//Maya ASCII 2016 scene
//Name: frd_deerWhite_rig_proxySkin.ma
//Last modified: Fri, Nov 18, 2016 10:59:00 AM
//Codeset: 1252
requires maya "2016";
requires -nodeType "mentalrayFramebuffer" -nodeType "mentalrayOptions" -nodeType "mentalrayGlobals"
		 -nodeType "mentalrayItemsList" -dataType "byteArray" "Mayatomr" "2016.0 - 3.13.1.10 ";
requires -nodeType "VRaySettingsNode" "vrayformaya" "3.10.01";
requires -nodeType "ilrOptionsNode" -nodeType "ilrUIOptionsNode" -nodeType "ilrBakeLayerManager"
		 -nodeType "ilrBakeLayer" "Turtle" "2016.0.0";
requires "stereoCamera" "10.0";
requires "pfOptions.py" "1.0";
requires "mtoa" "0.25.2";
requires "PO_Reader_v2012_64" "1.0";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2016";
fileInfo "version" "2016";
fileInfo "cutIdentifier" "201603180400-990260";
fileInfo "osv" "Microsoft Windows 8 Business Edition, 64-bit  (Build 9200)\n";
createNode transform -s -n "persp";
	rename -uid "F18AFA79-406D-275D-4355-1F8CE3BC8E70";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 13.571401119244523 7.8282156693437104 13.84090377861277 ;
	setAttr ".r" -type "double3" -5.7383526827588067 45.799999999896954 -359.99999999999073 ;
	setAttr ".rp" -type "double3" 2.2204460492503131e-016 2.2204460492503131e-016 4.4408920985006262e-016 ;
	setAttr ".rpt" -type "double3" 1.8683505729563207e-016 -3.0604473856722739e-017 
		-5.5425052618890054e-016 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "E32AB80A-4A2E-C35E-F353-438EE9C5B146";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 19.025738102452824;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" -1.6895909198400005e-006 5.9259120563808656 0.64329122529211369 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "34CD8A3C-4F18-68F1-6D12-BA8A3144B098";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "1EE3C972-4260-3C2A-AAC6-92866D30D216";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "B9C5D942-490B-EB8A-A998-6C8415060AF0";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1.0211468534413075 2.9983505924395373 100.12533145171344 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "8BC8565A-411E-C604-8665-BFB2FEADF9A5";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 4.3043502449597186;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "488B3068-4723-9472-A273-B9A635C43FE5";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 101.3620200296258 0.97923890754460008 1.1748130630957665 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "C316A18D-4832-624E-58D5-D59228D52E2C";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 3.7696663913106394;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "proxySkinJnt_grp";
	rename -uid "46BA2EB2-4B8F-6C98-DADD-A8915029A059";
	setAttr ".t" -type "double3" 0 7.1260128007052179 0.13611055408258466 ;
createNode joint -n "rootProxySkin_jnt" -p "proxySkinJnt_grp";
	rename -uid "49A362FA-4F18-CF80-1707-B5AD2D459662";
	setAttr ".t" -type "double3" 0 -2.0738460455046148 -0.11596790942412274 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 5.0521667552006031 0.020142644658461911 1;
	setAttr ".radi" 0.05;
createNode joint -n "spine1ProxySkin_jnt" -p "rootProxySkin_jnt";
	rename -uid "B37E10B4-4E9F-A9B3-A115-BEB253562E28";
	setAttr ".t" -type "double3" 0 0.35238390226155403 -0.39326695628170216 ;
	setAttr ".ro" 2;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 5.4045506574621571 -0.37312431162324022 1;
	setAttr ".radi" 0.05;
createNode joint -n "spine2ProxySkin_jnt" -p "spine1ProxySkin_jnt";
	rename -uid "C3C64B7D-48A4-AA62-9C15-F39FB1ACD855";
	setAttr ".t" -type "double3" 0 0.11046554462340996 1.1949362684419658 ;
	setAttr ".ro" 2;
	setAttr ".jo" -type "double3" 0.00049210753691731658 0 0 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 0.99999999996311539 8.5888967929811228e-006 0
		 0 -8.5888967929811228e-006 0.99999999996311539 0 0 5.5150162020855671 0.82181195681872554 1;
	setAttr ".radi" 0.05;
createNode joint -n "spine3ProxySkin_jnt" -p "spine2ProxySkin_jnt";
	rename -uid "9FFCAD5A-4230-9CC0-3955-12A38A2AB424";
	setAttr ".t" -type "double3" 7.4351863798988841e-017 0.33612439699619134 1.2862132045036274 ;
	setAttr ".ro" 2;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 0.99999999996311539 8.5888967929811228e-006 0
		 0 -8.5888967929811228e-006 0.99999999996311539 0 7.4351863798988841e-017 5.8511295519168938 2.1080280482126668 1;
	setAttr ".radi" 0.05;
createNode joint -n "neck1ProxySkin_jnt" -p "spine3ProxySkin_jnt";
	rename -uid "8E4F14E8-4483-8884-FB64-3DB013F3BBEC";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 2.0514079164197993e-023 8.8817841970012523e-016 -2.6645352591003757e-015 ;
	setAttr ".r" -type "double3" -5.7249984266343308e-014 0 0 ;
	setAttr ".jo" -type "double3" -7.7694829108118979 0 0 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 0.99082114634263529 -0.13517934738807602 0
		 0 0.13517934738807602 0.99082114634263529 0 7.435188431306803e-017 5.8511295519168964 2.1080280482126641 1;
	setAttr ".radi" 0.05;
createNode joint -n "neck2ProxySkin_jnt" -p "neck1ProxySkin_jnt";
	rename -uid "2485215C-441E-369B-58F1-53B81FE01B13";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -6.007167358800532e-018 2.9112513624173051 -2.2204460492503131e-015 ;
	setAttr ".r" -type "double3" 5.5659706925611529e-014 0 0 ;
	setAttr ".jo" -type "double3" 7.7689908032749821 0 0 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 2.7755575615628914e-017 0 0 -2.7755575615628914e-017 1 0
		 -2.6020852139652106e-017 10.856885927407339 -0.053788191692248979 1;
	setAttr ".radi" 0.05;
createNode joint -n "head1ProxySkin_jnt" -p "neck1ProxySkin_jnt";
	rename -uid "EE3B6F3C-4FA5-162F-E5F8-A295A9B11400";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -6.0071673588005197e-018 2.9112513624173033 -3.1086244689504383e-015 ;
	setAttr ".r" -type "double3" -1.590277340731758e-015 0 0 ;
	setAttr ".jo" -type "double3" 7.7689908032749821 0 0 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 2.7755575615628914e-017 0 0 -2.7755575615628914e-017 1 0
		 6.8344716954267511e-017 8.7356589641187661 1.714486988958444 1;
	setAttr ".radi" 0.05;
	setAttr ".liw" yes;
createNode joint -n "head2ProxySkin_jnt" -p "head1ProxySkin_jnt";
	rename -uid "FCEDC80E-4ABA-961E-0310-B4922EA5A123";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 5.1552460118565686e-016 1.9666167328839776 0.013000141164373646 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -2.6020852139652109e-017 12.64656436677493 -0.053788191692249028 1;
	setAttr ".radi" 0.05;
createNode joint -n "jaw1LWRProxySkin_jnt" -p "head1ProxySkin_jnt";
	rename -uid "7356A4F5-49E4-1A4B-CCEF-61BB8BC17293";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 2.6227362816030917e-016 -0.087474637798599986 0.881395214269034 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 2.7755575615628914e-017 0 0 -2.7755575615628914e-017 1 0
		 3.3061834511457666e-016 8.6481843263201696 2.5958822032274771 1;
	setAttr ".radi" 0.05;
	setAttr ".liw" yes;
createNode joint -n "jaw2LWRProxySkin_jnt" -p "jaw1LWRProxySkin_jnt";
	rename -uid "426FC7D2-4CEF-FB8A-B5B7-0A89C2902685";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -1.6544773752902978e-016 -0.69726001049487696 1.0383099917466088 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 -8.3266726846886741e-017 0 0 8.3266726846886741e-017 1 0
		 1.6517060758554688e-016 7.9509243158252909 3.6341921949740854 1;
	setAttr ".radi" 0.05;
createNode joint -n "jaw3LWRProxySkin_jnt" -p "jaw2LWRProxySkin_jnt";
	rename -uid "7D985632-40CF-849D-0FBD-18BF5D3D2462";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -4.3101173425501486e-017 -0.13859661243187738 0.79446715825441094 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -2.6020852139652109e-017 10.377191939779944 1.0562692302355619 1;
	setAttr ".radi" 0.05;
createNode joint -n "lowerTeethProxySkin_jnt" -p "jaw2LWRProxySkin_jnt";
	rename -uid "F629533B-411B-15B6-97BF-8D92AA330DEA";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 1.5050172804134427e-006 -0.010614044569884129 0.2455452468304764 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 -8.3266726846886741e-017 0 0 8.3266726846886741e-017 1 0
		 1.5050172805786133e-006 7.940310271255405 3.8797374418045623 1;
	setAttr ".radi" 0.05;
createNode joint -n "tongue1ProxySkin_jnt" -p "jaw1LWRProxySkin_jnt";
	rename -uid "F517EA4A-455A-4487-9133-8291673A035E";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -3.2183133669125231e-016 -0.67361749930797821 0.43452032040974897 ;
	setAttr ".jo" -type "double3" 89.593935536341803 0 0 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 0.97089208063779964 0.23951736420310818 0
		 0 -0.23951736420310818 0.97089208063779964 0 0 10.072923878713889 0.025607429179778735 1;
	setAttr ".radi" 0.05;
	setAttr ".liw" yes;
createNode joint -n "tongue2ProxySkin_jnt" -p "tongue1ProxySkin_jnt";
	rename -uid "7A42AB45-4019-6646-3034-2391C800527D";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" 8.4278694366385441e-031 0.41428369626046857 0 ;
	setAttr ".r" -type "double3" 3.180554681463516e-015 0 0 ;
	setAttr ".jo" -type "double3" 9.7105041597772832 0 0 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 0.73573807236441735 0.67726618760535473 0
		 0 -0.67726618760535473 0.73573807236441735 0 0 10.375278529070872 0.10019778824469508 1;
	setAttr ".radi" 0.05;
createNode joint -n "tongue3ProxySkin_jnt" -p "tongue2ProxySkin_jnt";
	rename -uid "FDE3D718-4E88-4C0F-2F65-348FBDD1D874";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 4.3213841687632052e-017 0.36884480093727046 -3.5527136788005009e-015 ;
	setAttr ".r" -type "double3" 3.4787316828507215e-016 0 0 ;
	setAttr ".jo" -type "double3" 0.15094790784301237 0 0 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 0.26717481468004534 0.96364807808695585 0
		 0 -0.96364807808695585 0.26717481468004534 0 0 10.449767044537673 0.16876641991609428 1;
	setAttr ".radi" 0.05;
createNode joint -n "tongue4ProxySkin_jnt" -p "tongue3ProxySkin_jnt";
	rename -uid "DFD1E262-42CC-B7EA-BCB2-C28A0251897D";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" -7.8565057962049812e-017 0.44431332996837991 1.9539925233402755e-014 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 -0.015026210923865246 0.99988710011944437 0
		 0 -0.99988710011944437 -0.015026210923865246 0 0 10.488728629764587 0.30929335082498055 1;
	setAttr ".radi" 0.05;
createNode joint -n "jaw1UPRProxySkin_jnt" -p "head1ProxySkin_jnt";
	rename -uid "BF89EDB3-400B-56E9-70A1-839A61199DCA";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 5.2297892687967057e-018 -0.64769048189728906 1.983328366548287 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 -8.3266726846886741e-017 0 0 8.3266726846886741e-017 1 0
		 7.3574506223064229e-017 8.0879684822214788 3.6978153555067301 1;
	setAttr ".radi" 0.05;
	setAttr ".liw" yes;
createNode joint -n "jaw2UPRProxySkin_jnt" -p "jaw1UPRProxySkin_jnt";
	rename -uid "43962534-433A-AC9C-5353-509419DC8C7C";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -1.2210812433126547e-016 -0.15969650088746867 0.78654545093687966 ;
	setAttr ".bps" -type "matrix" -1 1.2246467991473532e-016 0 0 -1.2246467991473532e-016 -1 0 0
		 0 0 1 0 -3.9875691153801112e-017 10.513451373935434 1.1067568396314549 1;
	setAttr ".radi" 0.05;
createNode joint -n "upperTeethProxySkin_jnt" -p "jaw1UPRProxySkin_jnt";
	rename -uid "569752A7-4D85-64BE-E9D9-F2864C5792B5";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 1.6551266331362613e-006 0.072627006599525146 0.14056262474791925 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 -8.3266726846886741e-017 0 0 8.3266726846886741e-017 1 0
		 1.6551266332098359e-006 8.1605954888210022 3.8383779802546494 1;
	setAttr ".radi" 0.05;
createNode joint -n "eyeProxySkinLFT_jnt" -p "head1ProxySkin_jnt";
	rename -uid "C179BBF5-457B-C9A4-7331-D0AB8885F55A";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.44669775685998897 0.44566046442773199 0.96867650212732137 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 2.7755575615628914e-017 0 0 -2.7755575615628914e-017 1 0
		 0.44669775685998903 9.1813194285465016 2.6831634910857649 1;
	setAttr ".radi" 0.05;
createNode joint -n "lidProxySkinLFT_jnt" -p "eyeProxySkinLFT_jnt";
	rename -uid "002156C6-4299-E097-6325-68A54611332C";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 2.7755575615628914e-017 0 0 -2.7755575615628914e-017 1 0
		 0.44669775685998903 9.1813194285465016 2.6831634910857649 1;
	setAttr ".radi" 0.05;
createNode joint -n "irisProxySkinLFT_jnt" -p "eyeProxySkinLFT_jnt";
	rename -uid "87DA3FEF-4786-8072-022A-2CA0E0F752E0";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 0.36163546123769774 -0.025147629840439834 0.41593852917790741 ;
	setAttr ".r" -type "double3" -3.5284278497485889e-015 -6.2989891543047e-015 -1.9878466759146963e-016 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 4.5769765547328003 40.862825727678285 2.9981346111977691 ;
	setAttr ".bps" -type "matrix" 0.75524295005004216 0.039555949761003233 -0.6542502680459632 0
		 2.8241298188902412e-015 0.99817729171867475 0.060349766338997413 0 0.65544495298973016 -0.045578735564696979 0.75386636247057326 0
		 0.80833321809768677 9.1561717987060618 3.0991020202636723 1;
	setAttr -k on -cb off ".radi" 0.05;
	setAttr ".liw" yes;
createNode joint -n "pupilProxySkinLFT_jnt" -p "irisProxySkinLFT_jnt";
	rename -uid "ABA387AF-4F55-D200-932D-2CB85C4D392F";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 8.8817841970012523e-016 0 1.3322676295501878e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 0.75524295005004216 0.039555949761003156 -0.6542502680459632 0
		 2.8241298188902412e-015 0.99817729171867475 0.060349766338997302 0 0.65544495298973016 -0.045578735564696896 0.75386636247057326 0
		 0.80833321809768732 9.15617179870606 3.0991020202636732 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "eyeSpecProxySkinLFT_jnt" -p "eyeProxySkinLFT_jnt";
	rename -uid "2005DA9F-46E4-B4DA-D284-CDAECBF1FED6";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 0.43752212801244794 0.11367111100428673 0.33773246686101244 ;
	setAttr ".r" -type "double3" 1.1927080055488187e-014 -3.6576378836830429e-014 -8.7465253740246719e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -28.055880766816301 47.295362577177237 -21.387876802356036 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0.54636822990099865 11.265977071776941 0.9290897048907325 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "eyeProxySkinRGT_jnt" -p "head1ProxySkin_jnt";
	rename -uid "70D0CB01-48DC-3AA7-8797-94B2633B89C4";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.44669800000000004 0.44566103588123696 0.9686730110415569 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 2.7755575615628914e-017 0 0 -2.7755575615628914e-017 1 0
		 -0.44669799999999998 9.1813200000000066 2.6831600000000004 1;
	setAttr ".radi" 0.05;
createNode joint -n "lidProxySkinRGT_jnt" -p "eyeProxySkinRGT_jnt";
	rename -uid "30EBC3C2-4A96-AA50-F919-AA92B265515D";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 2.7755575615628914e-017 0 0 -2.7755575615628914e-017 1 0
		 -0.44669799999999998 9.1813200000000066 2.6831600000000004 1;
	setAttr ".radi" 0.05;
createNode joint -n "irisProxySkinRGT_jnt" -p "eyeProxySkinRGT_jnt";
	rename -uid "3CEF3228-4C70-BAE3-7469-0EA6BA9A99F5";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" -0.361635 -0.025150000000000006 0.41594 ;
	setAttr ".r" -type "double3" -2.9817700138720464e-015 6.3735334046514988e-015 3.180554681463516e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 4.577379914687338 -40.862663976701903 -2.9983897073527861 ;
	setAttr ".bps" -type "matrix" 0.7552446184028202 -0.039559408917291962 0.6542481330043618 0
		 -2.4077961846558086e-015 0.99817696191951977 0.060355220926756568 0 -0.65544303060874676 -0.045582955797447748 0.75386777870339394 0
		 -0.80833299999999997 9.1561700000000066 3.0991000000000004 1;
	setAttr -k on -cb off ".radi" 0.05;
	setAttr ".liw" yes;
createNode joint -n "pupilProxySkinRGT_jnt" -p "irisProxySkinRGT_jnt";
	rename -uid "F84F2702-4C09-9CA8-707A-0B965671D9D2";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 4.4408920985006262e-016 -5.3290705182007514e-015 -4.4408920985006262e-016 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 0.7552446184028202 -0.039559408917291886 0.6542481330043618 0
		 -2.4077961846558086e-015 0.99817696191951977 0.060355220926756457 0 -0.65544303060874676 -0.045582955797447665 0.75386777870339394 0
		 -0.80833299999999975 9.156170000000003 3.0991000000000004 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "eyeSpecProxySkinRGT_jnt" -p "eyeProxySkinRGT_jnt";
	rename -uid "82EEE534-4E25-9E69-6F6C-B8AA5267C947";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" -0.22615403933715866 0.098747443847656768 0.50612909301757991 ;
	setAttr ".r" -type "double3" -9.9392333795734924e-017 2.4848083448933731e-017 -2.1552276845948051e-035 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -12.02657439644436 -23.216223099226358 4.8005132280081906 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -0.33581967186798489 11.265982736431209 0.96621025606127275 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "noseProxySkin_jnt" -p "head1ProxySkin_jnt";
	rename -uid "96674A5D-4ED4-7BC1-2600-CEA92D02CC95";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 1.3577605136289261e-008 -0.28676167644873196 2.7579905216605334 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 2.7755575615628914e-017 0 0 -2.7755575615628914e-017 1 0
		 1.3577605204633977e-008 8.4488972876700359 4.472477510618976 1;
	setAttr ".radi" 0.05;
	setAttr ".liw" yes;
createNode joint -n "ear1ProxySkinLFT_jnt" -p "head1ProxySkin_jnt";
	rename -uid "B79763E6-4847-1222-F2DC-3AB23B509F19";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.97394398848215724 1.0440426926466415 0.35140300534494218 ;
	setAttr ".r" -type "double3" -3.1805546814635294e-015 -2.2263882770244617e-014 6.2815954958904455e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -35.476506197811183 2.0310721746189491 -87.153012942710092 ;
	setAttr ".bps" -type "matrix" 0.049637648795777034 -0.9981382698896063 -0.035441472935116437 0
		 0.81232679507379968 0.060991755031180164 -0.58000446879601342 0 0.58108629464749584 -1.8912210545217768e-016 0.81384194913560692 0
		 0.97394398848215735 9.7797016567654094 2.0658899943033853 1;
	setAttr ".radi" 0.05;
	setAttr ".liw" yes;
createNode joint -n "ear2ProxySkinLFT_jnt" -p "ear1ProxySkinLFT_jnt";
	rename -uid "3A459334-42C6-1627-A085-3ABA62146849";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -7.1054273576010019e-015 0.40317751361615201 -2.2204460492503131e-015 ;
	setAttr ".r" -type "double3" 5.8144515270504926e-015 8.4731964560864007e-015 6.3009304108241486e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0.56508307025415305 -0.36258879507735803 29.188734419317623 ;
	setAttr ".bps" -type "matrix" 0.4431651049270941 -0.84163085910171564 -0.30864573021939895 0
		 0.69063826225340608 0.54005323534602423 -0.48100030530539928 0 0.57150982536309325 -2.7652058570032067e-014 0.82059522269718754 0
		 1.3014558859637884 9.8042921609099736 1.8320452346879501 1;
	setAttr ".radi" 0.05;
	setAttr ".liw" yes;
createNode joint -n "ear3ProxySkinLFT_jnt" -p "ear2ProxySkinLFT_jnt";
	rename -uid "91BEE279-458C-6ADB-8AA7-C99851A2451D";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 1.7763568394002505e-015 0.68275132475007094 -3.1086244689504383e-015 ;
	setAttr ".r" -type "double3" 8.7682674470424876e-015 -1.832546154358862e-015 2.0965570410037691e-017 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -0.45780896131551785 0.37149623041426849 6.3700859140171797 ;
	setAttr ".bps" -type "matrix" 0.51333894354918319 -0.7764993472469075 -0.36542015921529991 0
		 0.63259154562286513 0.63011805538734289 -0.45031008503180125 0 0.57992332720997108 7.7117434254461927e-015 0.81467105911387139 0
		 1.772990074440387 10.173014222778036 1.5036416390354974 1;
	setAttr ".radi" 0.05;
	setAttr ".liw" yes;
createNode joint -n "ear4ProxySkinLFT_jnt" -p "ear3ProxySkinLFT_jnt";
	rename -uid "6DDA4273-4D22-7C95-AFF5-5180E009DCDE";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 8.8817841970012523e-015 0.71786241172298038 4.4408920985006262e-015 ;
	setAttr ".r" -type "double3" -1.5061918145918703e-014 -5.4440597831398238e-015 -5.5638447891813588e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0.012178193092536956 -0.01177401969226847 4.9744401150131887 ;
	setAttr ".bps" -type "matrix" 0.56637745420854435 -0.71893625829462637 -0.40292336228316494 0
		 0.58581979983185883 0.69507600772097011 -0.41675473076575742 0 0.57968244883661035 1.7383428415882258e-013 0.81484247466046511 0
		 2.2271037670167924 10.625352289688578 1.1803809553713873 1;
	setAttr ".radi" 0.05;
	setAttr ".liw" yes;
createNode joint -n "ear5ProxySkinLFT_jnt" -p "ear4ProxySkinLFT_jnt";
	rename -uid "6A9A0021-4621-055F-2C32-6AA3083FB7B9";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 1.6875389974302379e-013 0.94657036555384444 -3.1086244689504383e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -6.361109362927032e-015 0 -1.5902773407317578e-014 ;
	setAttr ".bps" -type "matrix" 0.56637745420854435 -0.71893625829462637 -0.40292336228316494 0
		 0.58581979983185883 0.69507600772097011 -0.41675473076575742 0 0.57968244883661035 1.7383428415882258e-013 0.81484247466046511 0
		 2.7816234290924111 11.283290640404598 0.78589327752407989 1;
	setAttr ".radi" 0.05;
	setAttr ".liw" yes;
createNode joint -n "ear1ProxySkinRGT_jnt" -p "head1ProxySkin_jnt";
	rename -uid "25C0C295-4AD4-20E6-4479-C3B7C1630BB1";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.97394400000000014 1.044041035881234 0.35140301104155691 ;
	setAttr ".r" -type "double3" 3.1805546814635168e-015 3.1805546814635168e-015 8.8278125961003194e-032 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 144.52349380218882 -2.0310721746189624 87.153012942710049 ;
	setAttr ".bps" -type "matrix" 0.049637648795778144 0.99813826988960619 0.035441472935116715 0
		 0.81232679507379957 -0.060991755031181163 0.58000446879601342 0 0.58108629464749573 -1.9945595316662721e-016 -0.81384194913560681 0
		 -0.97394400000000003 9.7797000000000018 2.06589 1;
	setAttr ".radi" 0.05;
	setAttr ".liw" yes;
createNode joint -n "ear2ProxySkinRGT_jnt" -p "ear1ProxySkinRGT_jnt";
	rename -uid "562741A4-4061-2E2D-E233-6FB570353B93";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -5.381575576279829e-007 -0.40317805485486669 -6.2574918997526652e-006 ;
	setAttr ".r" -type "double3" -6.2430809665445994e-015 2.0872390097104338e-015 6.3708156455242735e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0.56508307025503979 -0.36258879507736219 29.18873441931764 ;
	setAttr ".bps" -type "matrix" 0.44316510492709527 0.84163085910171476 0.30864573021939923 0
		 0.69063826225341418 -0.54005323534602534 0.48100030530538634 0 0.57150982536308248 3.5576193666965811e-014 -0.82059522269719498 0
		 -1.3007263381847693 9.8056833236551828 1.8325609643881677 1;
	setAttr ".radi" 0.05;
	setAttr ".liw" yes;
createNode joint -n "ear3ProxySkinRGT_jnt" -p "ear2ProxySkinRGT_jnt";
	rename -uid "19EBDAD2-47D6-2524-2C41-38AFB4507A13";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -1.0272128230681687e-005 -0.68274499847012482 7.6491325451399916e-006 ;
	setAttr ".r" -type "double3" -3.9135731432070601e-016 -3.6433502356999077e-015 -4.7869444513066933e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -0.45780896131939491 0.37149623041437102 6.3700859140171389 ;
	setAttr ".bps" -type "matrix" 0.51333894354918386 0.77649934724690706 0.36542015921529997 0
		 0.63259154562283426 -0.63011805538734345 0.45031008503184378 0 0.57992332721000417 -4.1041486825235444e-014 -0.81467105911384785 0
		 -1.7722563381847645 10.174393323655178 1.5041509643881683 1;
	setAttr ".radi" 0.05;
	setAttr ".liw" yes;
createNode joint -n "ear4ProxySkinRGT_jnt" -p "ear3ProxySkinRGT_jnt";
	rename -uid "DA199669-459C-D77F-CAF6-9197DBBC4635";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 5.0236371444078998e-005 -0.71789879312741789 1.5844497935901813e-006 ;
	setAttr ".r" -type "double3" 9.0546027836610636e-015 5.3491323393388146e-016 7.9503995823016871e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0.012178193011807786 -0.011774019692539087 4.9744401150131816 ;
	setAttr ".bps" -type "matrix" 0.56637745420854502 0.71893625829462604 0.40292336228316478 0
		 0.58581979983101118 -0.69507600772097056 0.41675473076694797 0 0.57968244883746622 -1.1899145010672317e-012 -0.81484247465985626 0
		 -2.2263663381847647 10.62679332365518 1.1808909643881669 1;
	setAttr ".radi" 0.05;
	setAttr ".liw" yes;
createNode joint -n "ear5ProxySkinRGT_jnt" -p "ear4ProxySkinRGT_jnt";
	rename -uid "44C81753-4936-0B91-EA7A-8DBE5F8D6D97";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -2.7489992683094044e-005 -0.94654362435797523 -7.4822899609472415e-007 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 3.1805546814635152e-015 6.361109362927032e-015 -1.4312496066585827e-014 ;
	setAttr ".bps" -type "matrix" 0.56637745420854502 0.71893625829462604 0.40292336228316478 0
		 0.58581979983101118 -0.69507600772097056 0.41675473076694797 0 0.57968244883746622 -1.1899145010672317e-012 -0.81484247465985626 0
		 -2.7808863381847688 11.284693323655183 0.78640396438816507 1;
	setAttr ".radi" 0.05;
	setAttr ".liw" yes;
createNode joint -n "hornProxySkinLFT_jnt" -p "head1ProxySkin_jnt";
	rename -uid "AE96CB72-459E-561B-1315-92AD676888CE";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.57329688780009769 1.6785959396745689 0.34810368460742103 ;
	setAttr ".r" -type "double3" -2.5379961213788402e-031 -3.180554681463516e-015 9.1440947092076072e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -25.739226684744786 2.3811927796969467e-015 -21.983284828998734 ;
	setAttr ".bps" -type "matrix" 0.9272931008773091 -0.37433608571088178 3.1169740597820778e-017 0
		 0.33719442636924651 0.83528699786635263 -0.43427589159534369 0 0.16256513737840417 0.40270103815370445 0.90077991206457819 0
		 0.5732968878000978 10.414254903793335 2.0625906735658646 1;
	setAttr ".radi" 0.05;
createNode joint -n "hornProxySkinRGT_jnt" -p "head1ProxySkin_jnt";
	rename -uid "DB400605-4EEE-563B-420B-0288814AEE0E";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.57329688780009813 1.6785959396745689 0.34810368460742236 ;
	setAttr ".r" -type "double3" -9.9392333795734903e-015 7.9513867036587919e-016 1.9878466759146972e-016 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -25.739226684744786 -2.3811634564608323e-015 21.983 ;
	setAttr ".bps" -type "matrix" 0.92729496176683524 0.3743314759432389 -3.4137811419287182e-018 0
		 -0.33719027398315449 0.83528867411825636 -0.43427589159534369 0 -0.16256313546745099 0.40270184629316247 0.9007799120645783 0
		 -0.57329688780009802 10.414254903793335 2.0625906735658659 1;
	setAttr ".radi" 0.05;
createNode joint -n "neckRbnProxySkin_jnt" -p "neck1ProxySkin_jnt";
	rename -uid "DA4CE7D2-4241-084C-1EA5-CA83EC2B79AE";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -3.0035836794002598e-018 1.4556256812086517 -2.2204460492503131e-015 ;
	setAttr ".jo" -type "double3" -5.4069429584879788e-014 0 0 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 0.99082114634263529 -0.13517934738807602 0
		 0 0.13517934738807602 0.99082114634263529 0 7.1348300633667758e-017 7.2933942580178313 1.9112575185855538 1;
	setAttr ".radi" 0.05;
	setAttr ".liw" yes;
createNode joint -n "clav1ScapProxySkinLFT_jnt" -p "spine2ProxySkin_jnt";
	rename -uid "A9F4DC68-4EA6-2486-CAE3-BB9C05030153";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 1.1198304846200882 0.47504393654391119 0.96929197938932188 ;
	setAttr ".jo" -type "double3" 0 0 180 ;
	setAttr ".bps" -type "matrix" -1 1.2246467991021825e-016 1.0518364965731299e-021 0
		 -1.2246467991473532e-016 -0.99999999996311539 -8.5888967929811228e-006 0 0 -8.5888967929811228e-006 0.99999999996311539 0
		 1.1198304846200882 5.9900518134631833 1.7911080162756385 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode joint -n "clav2ScapProxySkinLFT_jnt" -p "clav1ScapProxySkinLFT_jnt";
	rename -uid "F7A8BDFE-4E1F-54E9-1FE5-C99FC5733256";
	setAttr ".t" -type "double3" 0.2433364340520392 0.48852644999036876 0.58211639177329721 ;
	setAttr ".r" -type "double3" -2.9118847791719204e-019 0 0 ;
	setAttr ".jo" -type "double3" 0.00049210753691731669 0 0 ;
	setAttr ".bps" -type "matrix" -1 1.2246467991021825e-016 1.0518364965731299e-021 0
		 -1.2246467991021825e-016 -0.99999999999999989 1.6940658945086007e-021 0 1.0518364965731301e-021 1.6940658945086007e-021 0.99999999999999989 0
		 0.87649405056804897 5.5015203637532233 2.3732202121242052 1;
	setAttr ".radi" 0.1;
createNode joint -n "upLegFrontProxySkinLFT_jnt" -p "clav2ScapProxySkinLFT_jnt";
	rename -uid "8338DB89-4C49-4327-C54E-3CA07730AE53";
	setAttr ".t" -type "double3" -2.2204460492503131e-016 -8.8817841970012523e-016 
		0 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" -9.706282597239735e-020 6.0265791991467028e-020 1.4033418596810944e-014 ;
	setAttr ".bps" -type "matrix" -1 1.2246467991021825e-016 1.0518364965731299e-021 0
		 -1.2246467991021825e-016 -0.99999999999999989 1.6940658945086007e-021 0 1.0518364965731301e-021 1.6940658945086007e-021 0.99999999999999989 0
		 0.87649405056804919 5.5015203637532242 2.3732202121242052 1;
	setAttr ".radi" 0.05;
createNode joint -n "midLegFrontProxySkinLFT_jnt" -p "upLegFrontProxySkinLFT_jnt";
	rename -uid "920E5E93-4DE0-3E71-1B14-C3A37613059F";
	setAttr ".t" -type "double3" -1.1102230246251565e-015 1.5741439905348713 -0.83081769408334427 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -1 1.2246467991021825e-016 1.0518364965731299e-021 0
		 -1.2246467991021825e-016 -0.99999999999999989 1.6940658945086007e-021 0 1.0518364965731301e-021 1.6940658945086007e-021 0.99999999999999989 0
		 0.87649405056804963 3.9273763732183529 1.5424025180408609 1;
	setAttr ".radi" 0.05;
createNode joint -n "lowLegFrontProxySkinLFT_jnt" -p "midLegFrontProxySkinLFT_jnt";
	rename -uid "2E9C03B0-4668-70FE-F24E-C680812EFD52";
	setAttr ".t" -type "double3" -6.6613381477509392e-015 1.5407045829345303 -0.025940506079254355 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -1 1.2246467991021825e-016 1.0518364965731299e-021 0
		 -1.2246467991021825e-016 -0.99999999999999989 1.6940658945086007e-021 0 1.0518364965731301e-021 1.6940658945086007e-021 0.99999999999999989 0
		 0.87649405056805607 2.386671790283823 1.516462011961607 1;
	setAttr ".radi" 0.05;
createNode joint -n "ankleFrontProxySkinLFT_jnt" -p "lowLegFrontProxySkinLFT_jnt";
	rename -uid "9610B8E2-460B-A858-DBE2-01B27BA81AB0";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 5.773159728050814e-015 1.1679456159333295 -0.13948179230042368 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -1 1.2246467991021825e-016 1.0518364965731299e-021 0
		 -1.2246467991021825e-016 -0.99999999999999989 1.6940658945086007e-021 0 1.0518364965731301e-021 1.6940658945086007e-021 0.99999999999999989 0
		 0.87649405056804908 1.2187261743504936 1.3769802196611833 1;
	setAttr ".radi" 0.05;
	setAttr ".liw" yes;
createNode joint -n "ballFrontProxySkinLFT_jnt" -p "ankleFrontProxySkinLFT_jnt";
	rename -uid "1BC64609-4E6C-BE65-B2BF-73AB2423BFF0";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -6.6613381477509392e-016 0.78938292374706076 0.37749784303153722 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 0 -180 ;
	setAttr ".bps" -type "matrix" 1 4.5170421951988018e-027 -1.0518364965731301e-021 0
		 -4.5170668471020899e-027 0.99999999999999989 -1.6940658945086005e-021 0 1.0518364965731301e-021 1.6940658945086007e-021 0.99999999999999989 0
		 0.87649405056804985 0.42934325060343281 1.7544780626927201 1;
	setAttr ".radi" 0.05;
	setAttr ".liw" yes;
createNode joint -n "toeFrontProxySkinLFT_jnt" -p "ballFrontProxySkinLFT_jnt";
	rename -uid "F40D821F-405A-4217-2F82-CF88C914EB08";
	setAttr ".t" -type "double3" 1.4432899320127035e-015 -0.42934325060342798 0.8550977803761175 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 89.999999999999972 0 -180 ;
	setAttr ".radi" 0.05;
createNode transform -n "ballFrontProxySkinBtwAllLFT_grp" -p "ankleFrontProxySkinLFT_jnt";
	rename -uid "7605C218-45A3-B5C6-5B6F-04A6AA558CA9";
	setAttr ".r" -type "double3" 9.7062825972397362e-020 -6.0265791991467028e-020 -180 ;
createNode transform -n "ballFrontProxySkinBtwAllMoveLFT_grp" -p "ballFrontProxySkinBtwAllLFT_grp";
	rename -uid "7727CEF3-49E3-FFD1-B954-22B57194FC42";
	setAttr ".t" -type "double3" 0 0.31958195163849346 0 ;
createNode transform -n "ballFrontProxySkinBtwHideLFT_grp" -p "ballFrontProxySkinBtwAllMoveLFT_grp";
	rename -uid "7A9DCD7A-4E40-DB64-644E-5997E751E6A6";
	setAttr ".v" no;
createNode transform -n "ballFrontProxySkinBtwUpLFT_grp" -p "ballFrontProxySkinBtwHideLFT_grp";
	rename -uid "6204B012-404C-3D5F-0EBF-8396FD30B58F";
createNode joint -n "ballFrontProxySkinBtwIkLFT_jnt" -p "ballFrontProxySkinBtwHideLFT_grp";
	rename -uid "3F9F2893-460C-89F2-67E7-D48B489F2908";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 0 -2.2204460492503131e-016 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" -24.393482760472477 0 0 ;
	setAttr ".bps" -type "matrix" 1 4.5170421951988018e-027 -1.0518364965731301e-021 0
		 -4.5170668471020899e-027 0.99999999999999989 -1.6940658945086005e-021 0 1.0518364965731301e-021 1.6940658945086007e-021 0.99999999999999989 0
		 0.87649405056804985 0.42934325060343281 1.7544780626927201 1;
	setAttr ".radi" 0.075000000000000011;
	setAttr ".liw" yes;
createNode joint -n "ballFrontProxySkinBtwEndIkLFT_jnt" -p "ballFrontProxySkinBtwIkLFT_jnt";
	rename -uid "8BC91B20-42ED-E3B2-46A8-E4B792A3105B";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 -0.63916390327698747 2.2204460492503131e-016 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" -1.0776138422974023e-035 1.077613842297402e-035 1.987881126606291e-041 ;
	setAttr ".bps" -type "matrix" 1 4.5170421951988018e-027 -1.0518364965731301e-021 0
		 -4.5170668471020899e-027 0.99999999999999989 -1.6940658945086005e-021 0 1.0518364965731301e-021 1.6940658945086007e-021 0.99999999999999989 0
		 0.87649405056804985 0.42934325060343281 1.7544780626927201 1;
	setAttr ".radi" 0.025;
	setAttr ".liw" yes;
createNode ikEffector -n "ballFrontProxySkinBtwEndLFT_eff" -p "ballFrontProxySkinBtwIkLFT_jnt";
	rename -uid "803DEDE4-482F-2BD7-1238-469B43D22D98";
	setAttr ".v" no;
	setAttr ".hd" yes;
createNode joint -n "ballFrontProxySkinBtwAimLFT_jnt" -p "ballFrontProxySkinBtwHideLFT_grp";
	rename -uid "0758C8FC-4536-E63B-DD70-98B4950A6A35";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" -9.7062825972397386e-020 6.026579199146704e-020 2.588074536673999e-025 ;
	setAttr ".bps" -type "matrix" 1 4.5170421951988018e-027 -1.0518364965731301e-021 0
		 -4.5170668471020899e-027 0.99999999999999989 -1.6940658945086005e-021 0 1.0518364965731301e-021 1.6940658945086007e-021 0.99999999999999989 0
		 0.87649405056804985 0.42934325060343281 1.7544780626927201 1;
	setAttr ".radi" 0.075000000000000011;
	setAttr ".liw" yes;
createNode joint -n "ballFrontProxySkinBtwAimEndLFT_jnt" -p "ballFrontProxySkinBtwAimLFT_jnt";
	rename -uid "2CBCE064-4403-1348-9E6D-13A6019DE20F";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 -0.63916390327698736 -2.2204460492503131e-016 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" -1.0776138422974023e-035 1.077613842297402e-035 1.987881126606291e-041 ;
	setAttr ".bps" -type "matrix" 1 4.5170421951988018e-027 -1.0518364965731301e-021 0
		 -4.5170668471020899e-027 0.99999999999999989 -1.6940658945086005e-021 0 1.0518364965731301e-021 1.6940658945086007e-021 0.99999999999999989 0
		 0.87649405056804985 0.42934325060343281 1.7544780626927201 1;
	setAttr ".radi" 0.025;
	setAttr ".liw" yes;
createNode aimConstraint -n "ballFrontProxySkinBtwAimLFT_jnt_aimConstraint1" -p "ballFrontProxySkinBtwAimLFT_jnt";
	rename -uid "FCBE94DA-4CFF-8B34-3792-B7AD38BB2BD8";
	addAttr -dcb 0 -ci true -sn "w0" -ln "toeFrontProxySkinLFT_jntW0" -dv 1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".a" -type "double3" 0 -1 0 ;
	setAttr ".u" -type "double3" 1 0 0 ;
	setAttr ".wut" 1;
	setAttr ".rsrr" -type "double3" -48.786965520944896 -3.2990758424310714e-014 7.2749718200695497e-014 ;
	setAttr -k on ".w0";
createNode joint -n "ballFrontProxySkinBtwVoidLFT_jnt" -p "ballFrontProxySkinBtwHideLFT_grp";
	rename -uid "05F02588-4710-84B4-E0A7-538D18205CE7";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" -9.7062825972397386e-020 6.026579199146704e-020 2.588074536673999e-025 ;
	setAttr ".bps" -type "matrix" 1 4.5170421951988018e-027 -1.0518364965731301e-021 0
		 -4.5170668471020899e-027 0.99999999999999989 -1.6940658945086005e-021 0 1.0518364965731301e-021 1.6940658945086007e-021 0.99999999999999989 0
		 0.87649405056804985 0.42934325060343281 1.7544780626927201 1;
	setAttr ".radi" 0.075000000000000011;
	setAttr ".liw" yes;
createNode joint -n "ballFrontProxySkinBtwVoidEndLFT_jnt" -p "ballFrontProxySkinBtwVoidLFT_jnt";
	rename -uid "E00EBAC8-4F9B-86E6-1778-8BB39CA1D7B5";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 -0.63916390327698736 -2.2204460492503131e-016 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" -1.0776138422974023e-035 1.077613842297402e-035 1.987881126606291e-041 ;
	setAttr ".bps" -type "matrix" 1 4.5170421951988018e-027 -1.0518364965731301e-021 0
		 -4.5170668471020899e-027 0.99999999999999989 -1.6940658945086005e-021 0 1.0518364965731301e-021 1.6940658945086007e-021 0.99999999999999989 0
		 0.87649405056804985 0.42934325060343281 1.7544780626927201 1;
	setAttr ".radi" 0.025;
	setAttr ".liw" yes;
createNode ikHandle -n "ballFrontProxySkinBtwLFT_ikHndl" -p "ballFrontProxySkinBtwHideLFT_grp";
	rename -uid "FB763897-430A-EE8C-05A1-8181A7371A1F";
	setAttr ".r" -type "double3" 9.7062825972397362e-020 -6.0265791991467028e-020 -2.5880745366739999e-025 ;
	setAttr ".hs" 1;
	setAttr -l on ".pv" -type "double3" 0 0 0 ;
	setAttr -l on ".pv";
	setAttr ".roc" yes;
createNode pointConstraint -n "ballFrontProxySkinBtwLFT_ikHndl_pointConstraint1" 
		-p "ballFrontProxySkinBtwLFT_ikHndl";
	rename -uid "0ED5E022-4416-67CE-D261-70B2830B20FB";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "ballFrontProxySkinBtwAimEndLFT_jntW0" 
		-dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "ballFrontProxySkinBtwVoidEndLFT_jntW1" 
		-dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" 0.87649405056805119 0.10976129896493958 1.7544780626927199 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode joint -n "ballFrontProxySkinBtwLFT_jnt" -p "ballFrontProxySkinBtwAllMoveLFT_grp";
	rename -uid "FDE3DF27-4E21-78A1-3E08-3AAA086074F0";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "aimBtw" -ln "aimBtw" -dv 0.5 -min 0 -max 1 -at "float";
	addAttr -ci true -sn "scaleBtw" -ln "scaleBtw" -dv 2 -at "float";
	setAttr -l on ".v";
	setAttr -l on ".t";
	setAttr -l on ".r";
	setAttr ".ro" 1;
	setAttr -l on ".s";
	setAttr ".jo" -type "double3" -24.393482760472477 0 0 ;
	setAttr ".bps" -type "matrix" 1 4.5170421951988018e-027 -1.0518364965731301e-021 0
		 -4.5170668471020899e-027 0.99999999999999989 -1.6940658945086005e-021 0 1.0518364965731301e-021 1.6940658945086007e-021 0.99999999999999989 0
		 0.87649405056804985 0.42934325060343281 1.7544780626927201 1;
	setAttr ".radi" 0.075000000000000011;
	setAttr ".liw" yes;
	setAttr -k on ".aimBtw";
	setAttr -k on ".scaleBtw";
createNode joint -n "ballFrontProxySkinBtwEndLFT_jnt" -p "ballFrontProxySkinBtwLFT_jnt";
	rename -uid "4C878D20-4FB9-570A-28AD-E4A973D52BBC";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 -0.63916390327698747 -2.2204460492503131e-016 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" -1.0776138422974023e-035 1.077613842297402e-035 1.987881126606291e-041 ;
	setAttr ".bps" -type "matrix" 1 4.5170421951988018e-027 -1.0518364965731301e-021 0
		 -4.5170668471020899e-027 0.99999999999999989 -1.6940658945086005e-021 0 1.0518364965731301e-021 1.6940658945086007e-021 0.99999999999999989 0
		 0.87649405056804985 0.42934325060343281 1.7544780626927201 1;
	setAttr ".radi" 0.025;
	setAttr ".liw" yes;
createNode parentConstraint -n "ballFrontProxySkinBtwLFT_jnt_parentConstraint1" -p
		 "ballFrontProxySkinBtwLFT_jnt";
	rename -uid "5C3895EB-4916-5EDA-011C-9497D561B769";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "ballFrontProxySkinBtwIkLFT_jntW0" 
		-dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 0 0 -4.4408920985006262e-016 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "ballFrontProxySkinBtwAllLFT_grp_pointConstraint1" 
		-p "ballFrontProxySkinBtwAllLFT_grp";
	rename -uid "FAB17927-40E4-C98E-4C39-70A979FFFFD1";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "ballFrontProxySkinLFT_jntW0" -dv 
		1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" -6.6613381477509392e-016 0.78938292374706054 0.37749784303153722 ;
	setAttr -k on ".w0";
createNode joint -n "lowLegRbnDtl1FrontProxySkinLFT_jnt" -p "lowLegFrontProxySkinLFT_jnt";
	rename -uid "D53AF6A1-4EEF-1C19-CC6F-A1A9A8159862";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 6.6613381477509392e-016 0.11679456159333235 -0.013948179230041768 ;
	setAttr ".r" -type "double3" -7.9513867036587899e-016 1.6641146162284123e-015 1.3934402034699522e-014 ;
	setAttr ".jo" -type "double3" 6.8102883483607366 0 180 ;
	setAttr ".bps" -type "matrix" 1 -2.4492935982495357e-016 -1.0518364965731297e-021 0
		 2.4320131952677674e-016 0.99294423082406247 0.11858226879770319 0 -2.9043234768230968e-017 -0.11858226879770319 0.99294423082406247 0
		 0.87649405056805496 2.2698772286904907 1.5025138327315652 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode joint -n "lowLegRbnDtl2FrontProxySkinLFT_jnt" -p "lowLegFrontProxySkinLFT_jnt";
	rename -uid "73D5FB2F-480B-A884-2476-B79E3C1D1343";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 1.7763568394002505e-015 0.35038368477999837 -0.04184453769012686 ;
	setAttr ".r" -type "double3" -7.9513867036587899e-016 1.6641146162284123e-015 1.3934402034699522e-014 ;
	setAttr ".jo" -type "double3" 6.8102883483607366 0 180 ;
	setAttr ".bps" -type "matrix" 1 -2.4492935982495357e-016 -1.0518364965731297e-021 0
		 2.4320131952677674e-016 0.99294423082406247 0.11858226879770319 0 -2.9043234768230968e-017 -0.11858226879770319 0.99294423082406247 0
		 0.87649405056805341 2.0362881055038247 1.4746174742714802 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode joint -n "lowLegRbnDtl3FrontProxySkinLFT_jnt" -p "lowLegFrontProxySkinLFT_jnt";
	rename -uid "5F583CBB-4C74-CF89-C636-1B947BAC379B";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 2.7755575615628914e-015 0.58397280796666484 -0.069740896150211285 ;
	setAttr ".r" -type "double3" -7.9513867036587899e-016 1.6641146162284123e-015 1.3934402034699522e-014 ;
	setAttr ".jo" -type "double3" 6.8102883483607366 0 180 ;
	setAttr ".bps" -type "matrix" 1 -2.4492935982495357e-016 -1.0518364965731297e-021 0
		 2.4320131952677674e-016 0.99294423082406247 0.11858226879770319 0 -2.9043234768230968e-017 -0.11858226879770319 0.99294423082406247 0
		 0.87649405056805252 1.8026989823171582 1.4467211158113957 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode joint -n "lowLegRbnDtl4FrontProxySkinLFT_jnt" -p "lowLegFrontProxySkinLFT_jnt";
	rename -uid "11B98ED9-455A-09D7-41FE-AEA29686F9B1";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 3.8857805861880479e-015 0.81756193115333042 -0.097637254610295932 ;
	setAttr ".r" -type "double3" -7.9513867036587899e-016 1.6641146162284123e-015 1.3934402034699522e-014 ;
	setAttr ".jo" -type "double3" 6.8102883483607366 0 180 ;
	setAttr ".bps" -type "matrix" 1 -2.4492935982495357e-016 -1.0518364965731297e-021 0
		 2.4320131952677674e-016 0.99294423082406247 0.11858226879770319 0 -2.9043234768230968e-017 -0.11858226879770319 0.99294423082406247 0
		 0.87649405056805141 1.5691098591304926 1.4188247573513111 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode joint -n "lowLegRbnDtl5FrontProxySkinLFT_jnt" -p "lowLegFrontProxySkinLFT_jnt";
	rename -uid "E3B4AC63-4412-E551-5898-8C96F7B5962F";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 4.9960036108132044e-015 1.0511510543399967 -0.12553361307038058 ;
	setAttr ".r" -type "double3" -7.9513867036587899e-016 1.6641146162284123e-015 1.3934402034699522e-014 ;
	setAttr ".jo" -type "double3" 6.8102883483607366 0 180 ;
	setAttr ".bps" -type "matrix" 1 -2.4492935982495357e-016 -1.0518364965731297e-021 0
		 2.4320131952677674e-016 0.99294423082406247 0.11858226879770319 0 -2.9043234768230968e-017 -0.11858226879770319 0.99294423082406247 0
		 0.87649405056804985 1.3355207359438264 1.3909283988912264 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode transform -n "ankleFrontProxySkinBtwAllLFT_grp" -p "lowLegRbnDtl5FrontProxySkinLFT_jnt";
	rename -uid "72BA29E7-4B02-39CF-38DE-05A78DABC2D0";
	setAttr ".r" -type "double3" 6.8102883483607375 -8.3199746751304182e-016 -180 ;
createNode transform -n "ankleFrontProxySkinBtwAllMoveLFT_grp" -p "ankleFrontProxySkinBtwAllLFT_grp";
	rename -uid "12915D4F-4403-9AFB-9FBE-DD8A694AD702";
	setAttr ".t" -type "double3" 0 -0.2922509584442407 0 ;
createNode transform -n "ankleFrontProxySkinBtwHideLFT_grp" -p "ankleFrontProxySkinBtwAllMoveLFT_grp";
	rename -uid "327BEB5E-4A69-72D8-12FC-A3A3EB493918";
	setAttr ".v" no;
createNode transform -n "ankleFrontProxySkinBtwUpLFT_grp" -p "ankleFrontProxySkinBtwHideLFT_grp";
	rename -uid "B03A1727-4F64-161C-E5DA-908678257322";
createNode joint -n "ankleFrontProxySkinBtwIkLFT_jnt" -p "ankleFrontProxySkinBtwHideLFT_grp";
	rename -uid "F7DCB20D-4B87-A5A7-56FA-139CBA83D06C";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 2.2204460492503131e-016 0 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 9.6196735150880368 0 0 ;
	setAttr ".bps" -type "matrix" -1 1.2246467991021825e-016 1.0518364965731299e-021 0
		 -1.2246467991021825e-016 -0.99999999999999989 1.6940658945086007e-021 0 1.0518364965731301e-021 1.6940658945086007e-021 0.99999999999999989 0
		 0.87649405056804908 1.2187261743504936 1.3769802196611833 1;
	setAttr ".radi" 0.075000000000000011;
	setAttr ".liw" yes;
createNode joint -n "ankleFrontProxySkinBtwEndIkLFT_jnt" -p "ankleFrontProxySkinBtwIkLFT_jnt";
	rename -uid "BADB4D63-49A1-8BD6-9F85-9DB446AB78EA";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 1.1102230246251565e-016 0.58450191688848174 0 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 0 1.0209422281829385e-040 ;
	setAttr ".bps" -type "matrix" -1 1.2246467991021825e-016 1.0518364965731299e-021 0
		 -1.2246467991021825e-016 -0.99999999999999989 1.6940658945086007e-021 0 1.0518364965731301e-021 1.6940658945086007e-021 0.99999999999999989 0
		 0.87649405056804908 1.2187261743504936 1.3769802196611833 1;
	setAttr ".radi" 0.025;
	setAttr ".liw" yes;
createNode ikEffector -n "ankleFrontProxySkinBtwEndLFT_eff" -p "ankleFrontProxySkinBtwIkLFT_jnt";
	rename -uid "0A39E4F3-49F2-5FBB-3AFF-08B0A3FBF644";
	setAttr ".v" no;
	setAttr ".hd" yes;
createNode joint -n "ankleFrontProxySkinBtwAimLFT_jnt" -p "ankleFrontProxySkinBtwHideLFT_grp";
	rename -uid "DF22C77F-490F-11F3-9E91-F5B0CE1BC47E";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 9.7062825972397386e-020 -6.0265791991467016e-020 2.5880886611741528e-025 ;
	setAttr ".bps" -type "matrix" -1 1.2246467991021825e-016 1.0518364965731299e-021 0
		 -1.2246467991021825e-016 -0.99999999999999989 1.6940658945086007e-021 0 1.0518364965731301e-021 1.6940658945086007e-021 0.99999999999999989 0
		 0.87649405056804908 1.2187261743504936 1.3769802196611833 1;
	setAttr ".radi" 0.075000000000000011;
	setAttr ".liw" yes;
createNode joint -n "ankleFrontProxySkinBtwAimEndLFT_jnt" -p "ankleFrontProxySkinBtwAimLFT_jnt";
	rename -uid "69734B5A-4BBC-614E-2D4E-B191AB3F6A4E";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 0.58450191688848196 0 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 0 1.0209422281829385e-040 ;
	setAttr ".bps" -type "matrix" -1 1.2246467991021825e-016 1.0518364965731299e-021 0
		 -1.2246467991021825e-016 -0.99999999999999989 1.6940658945086007e-021 0 1.0518364965731301e-021 1.6940658945086007e-021 0.99999999999999989 0
		 0.87649405056804908 1.2187261743504936 1.3769802196611833 1;
	setAttr ".radi" 0.025;
	setAttr ".liw" yes;
createNode aimConstraint -n "ankleFrontProxySkinBtwAimLFT_jnt_aimConstraint1" -p "ankleFrontProxySkinBtwAimLFT_jnt";
	rename -uid "BA756FD3-4936-E8B4-D052-768E3E167B62";
	addAttr -dcb 0 -ci true -sn "w0" -ln "ballFrontProxySkinLFT_jntW0" -dv 1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".a" -type "double3" 0 1 0 ;
	setAttr ".u" -type "double3" 1 0 0 ;
	setAttr ".wut" 1;
	setAttr ".rsrr" -type "double3" 19.239347030176045 5.6466446432638762e-015 3.3315394975756878e-014 ;
	setAttr -k on ".w0";
createNode joint -n "ankleFrontProxySkinBtwVoidLFT_jnt" -p "ankleFrontProxySkinBtwHideLFT_grp";
	rename -uid "6E6874EB-4443-C634-9B4F-3389162295E0";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 9.7062825972397386e-020 -6.0265791991467016e-020 2.5880886611741528e-025 ;
	setAttr ".bps" -type "matrix" -1 1.2246467991021825e-016 1.0518364965731299e-021 0
		 -1.2246467991021825e-016 -0.99999999999999989 1.6940658945086007e-021 0 1.0518364965731301e-021 1.6940658945086007e-021 0.99999999999999989 0
		 0.87649405056804908 1.2187261743504936 1.3769802196611833 1;
	setAttr ".radi" 0.075000000000000011;
	setAttr ".liw" yes;
createNode joint -n "ankleFrontProxySkinBtwVoidEndLFT_jnt" -p "ankleFrontProxySkinBtwVoidLFT_jnt";
	rename -uid "1E561CEF-4E63-DA79-38D7-64AE1731E081";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 0.58450191688848196 0 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 0 1.0209422281829385e-040 ;
	setAttr ".bps" -type "matrix" -1 1.2246467991021825e-016 1.0518364965731299e-021 0
		 -1.2246467991021825e-016 -0.99999999999999989 1.6940658945086007e-021 0 1.0518364965731301e-021 1.6940658945086007e-021 0.99999999999999989 0
		 0.87649405056804908 1.2187261743504936 1.3769802196611833 1;
	setAttr ".radi" 0.025;
	setAttr ".liw" yes;
createNode ikHandle -n "ankleFrontProxySkinBtwLFT_ikHndl" -p "ankleFrontProxySkinBtwHideLFT_grp";
	rename -uid "7C2DC530-4EC3-A2C5-5B31-739DB7C0EE46";
	setAttr ".r" -type "double3" 7.9513867036587959e-016 8.3199704232221437e-016 -180 ;
	setAttr ".hs" 1;
	setAttr -l on ".pv" -type "double3" 0 0 0 ;
	setAttr -l on ".pv";
	setAttr ".roc" yes;
createNode pointConstraint -n "ankleFrontProxySkinBtwLFT_ikHndl_pointConstraint1" 
		-p "ankleFrontProxySkinBtwLFT_ikHndl";
	rename -uid "98F1C3C8-41B7-D962-829D-608565713979";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "ankleFrontProxySkinBtwAimEndLFT_jntW0" 
		-dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "ankleFrontProxySkinBtwVoidEndLFT_jntW1" 
		-dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" 0.87649405056805063 0.92647521590625292 1.3769802196611829 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode joint -n "ankleFrontProxySkinBtwLFT_jnt" -p "ankleFrontProxySkinBtwAllMoveLFT_grp";
	rename -uid "DDFD7641-4F4F-8E71-0378-DC86D068FE08";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "aimBtw" -ln "aimBtw" -dv 0.5 -min 0 -max 1 -at "float";
	addAttr -ci true -sn "scaleBtw" -ln "scaleBtw" -dv 2 -at "float";
	setAttr -l on ".v";
	setAttr -l on ".t";
	setAttr -l on ".r";
	setAttr ".ro" 1;
	setAttr -l on ".s";
	setAttr ".jo" -type "double3" 9.6196735150880368 0 0 ;
	setAttr ".bps" -type "matrix" -1 1.2246467991021825e-016 1.0518364965731299e-021 0
		 -1.2246467991021825e-016 -0.99999999999999989 1.6940658945086007e-021 0 1.0518364965731301e-021 1.6940658945086007e-021 0.99999999999999989 0
		 0.87649405056804908 1.2187261743504936 1.3769802196611833 1;
	setAttr ".radi" 0.075000000000000011;
	setAttr ".liw" yes;
	setAttr -k on ".aimBtw";
	setAttr -k on ".scaleBtw";
createNode joint -n "ankleFrontProxySkinBtwEndLFT_jnt" -p "ankleFrontProxySkinBtwLFT_jnt";
	rename -uid "7AD72832-47EA-BBA5-4120-42BE68786C6A";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 1.1102230246251565e-016 0.58450191688848174 2.2204460492503131e-016 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 0 1.0209422281829385e-040 ;
	setAttr ".bps" -type "matrix" -1 1.2246467991021825e-016 1.0518364965731299e-021 0
		 -1.2246467991021825e-016 -0.99999999999999989 1.6940658945086007e-021 0 1.0518364965731301e-021 1.6940658945086007e-021 0.99999999999999989 0
		 0.87649405056804908 1.2187261743504936 1.3769802196611833 1;
	setAttr ".radi" 0.025;
	setAttr ".liw" yes;
createNode parentConstraint -n "ankleFrontProxySkinBtwLFT_jnt_parentConstraint1" 
		-p "ankleFrontProxySkinBtwLFT_jnt";
	rename -uid "C0649096-4BC1-34ED-AC9C-B5AC3AEA8BC4";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "ankleFrontProxySkinBtwIkLFT_jntW0" 
		-dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 0 4.4408920985006262e-016 0 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "ankleFrontProxySkinBtwAllLFT_grp_pointConstraint1" 
		-p "ankleFrontProxySkinBtwAllLFT_grp";
	rename -uid "2C67AF83-443B-375B-1384-269B6A45FA3F";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "ankleFrontProxySkinLFT_jntW0" -dv 
		1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" -6.6613381477509392e-016 -0.11762449286442078 -8.8817841970012523e-016 ;
	setAttr -k on ".w0";
createNode joint -n "midLegRbnDtl1FrontProxySkinLFT_jnt" -p "midLegFrontProxySkinLFT_jnt";
	rename -uid "230E5BCA-448E-2D27-949F-FAAFF73FAE32";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -8.8817841970012523e-016 0.14597987279603331 -0.0024578311894818405 ;
	setAttr ".r" -type "double3" 2.8927460321051447e-032 2.3624411698495623e-016 1.4031429942731181e-014 ;
	setAttr ".jo" -type "double3" 0.96458536880384449 -1.1034765745125397e-032 180 ;
	setAttr ".bps" -type "matrix" 1 -2.4492935982495357e-016 -1.0518364965729371e-021 0
		 2.4489466896609357e-016 0.99985829152570249 0.016834395364952993 0 -4.1221859923356757e-018 -0.016834395364952993 0.99985829152570227 0
		 0.87649405056805052 3.7813965004223205 1.5399446868513795 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode joint -n "midLegRbnDtl2FrontProxySkinLFT_jnt" -p "midLegFrontProxySkinLFT_jnt";
	rename -uid "A25AB03A-4E3F-CFF3-60BF-9FACE772DA03";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -2.2204460492503131e-015 0.45088455518397108 -0.0075914446379552469 ;
	setAttr ".r" -type "double3" 2.8927460321051447e-032 2.3624411698495623e-016 1.4031429942731181e-014 ;
	setAttr ".jo" -type "double3" 0.96458536880384449 -1.1034765745125397e-032 180 ;
	setAttr ".bps" -type "matrix" 1 -2.4492935982495357e-016 -1.0518364965729371e-021 0
		 2.4489466896609357e-016 0.99985829152570249 0.016834395364952993 0 -4.1221859923356757e-018 -0.016834395364952993 0.99985829152570227 0
		 0.87649405056805185 3.4764918180343827 1.5348110734029061 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode joint -n "midLegRbnDtl3FrontProxySkinLFT_jnt" -p "midLegFrontProxySkinLFT_jnt";
	rename -uid "701787F6-404C-CEE4-6D3B-BF890C5F7767";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -3.3306690738754696e-015 0.77035229146726536 -0.0129702530396274 ;
	setAttr ".r" -type "double3" 2.8927460321051447e-032 2.3624411698495623e-016 1.4031429942731181e-014 ;
	setAttr ".jo" -type "double3" 0.96458536880384449 -1.1034765745125397e-032 180 ;
	setAttr ".bps" -type "matrix" 1 -2.4492935982495357e-016 -1.0518364965729371e-021 0
		 2.4489466896609357e-016 0.99985829152570249 0.016834395364952993 0 -4.1221859923356757e-018 -0.016834395364952993 0.99985829152570227 0
		 0.87649405056805263 3.1570240817510884 1.529432265001234 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode joint -n "midLegRbnDtl4FrontProxySkinLFT_jnt" -p "midLegFrontProxySkinLFT_jnt";
	rename -uid "50C74B78-4BFB-82E7-0559-D4B67E81E96C";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -4.3298697960381105e-015 1.0898200277505592 -0.018349061441298664 ;
	setAttr ".r" -type "double3" 2.8927460321051447e-032 2.3624411698495623e-016 1.4031429942731181e-014 ;
	setAttr ".jo" -type "double3" 0.96458536880384449 -1.1034765745125397e-032 180 ;
	setAttr ".bps" -type "matrix" 1 -2.4492935982495357e-016 -1.0518364965729371e-021 0
		 2.4489466896609357e-016 0.99985829152570249 0.016834395364952993 0 -4.1221859923356757e-018 -0.016834395364952993 0.99985829152570227 0
		 0.87649405056805363 2.8375563454677941 1.5240534565995627 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode joint -n "midLegRbnDtl5FrontProxySkinLFT_jnt" -p "midLegFrontProxySkinLFT_jnt";
	rename -uid "5C8AF97B-4552-0B6F-480C-D38A830F5B3D";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -5.5511151231257827e-015 1.394724710138497 -0.023482674889772515 ;
	setAttr ".r" -type "double3" 2.8927460321051447e-032 2.3624411698495623e-016 1.4031429942731181e-014 ;
	setAttr ".jo" -type "double3" 0.96458536880384449 -1.1034765745125397e-032 180 ;
	setAttr ".bps" -type "matrix" 1 -2.4492935982495357e-016 -1.0518364965729371e-021 0
		 2.4489466896609357e-016 0.99985829152570249 0.016834395364952993 0 -4.1221859923356757e-018 -0.016834395364952993 0.99985829152570227 0
		 0.87649405056805496 2.5326516630798563 1.5189198431510889 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode transform -n "lowLegFrontProxySkinBtwAllLFT_grp" -p "midLegRbnDtl5FrontProxySkinLFT_jnt";
	rename -uid "0C33CC2E-460C-6EC6-3A4D-A38F6EAFE208";
	setAttr ".r" -type "double3" 0.96458536880384449 -1.1806180123630317e-016 -180 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1 ;
createNode transform -n "lowLegFrontProxySkinBtwAllMoveLFT_grp" -p "lowLegFrontProxySkinBtwAllLFT_grp";
	rename -uid "866CD26B-41F5-4C95-E4C1-96B256BF52CA";
	setAttr ".t" -type "double3" 0 -0.039286580616716395 0 ;
createNode transform -n "lowLegFrontProxySkinBtwHideLFT_grp" -p "lowLegFrontProxySkinBtwAllMoveLFT_grp";
	rename -uid "27845CFD-4943-DD5C-6CE9-98943F6EE8C1";
	setAttr ".v" no;
createNode transform -n "lowLegFrontProxySkinBtwUpLFT_grp" -p "lowLegFrontProxySkinBtwHideLFT_grp";
	rename -uid "7E7E96BE-473D-0748-920F-32885FE34A9D";
createNode joint -n "lowLegFrontProxySkinBtwIkLFT_jnt" -p "lowLegFrontProxySkinBtwHideLFT_grp";
	rename -uid "D71BE67D-4CB6-4B9A-187D-A3A17E4BD37B";
	setAttr ".t" -type "double3" 1.1102230246251565e-016 0 0 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" -2.5533338217290762 0 0 ;
	setAttr ".bps" -type "matrix" -1 1.2246467991021825e-016 1.0518364965731299e-021 0
		 -1.2246467991021825e-016 -0.99999999999999989 1.6940658945086007e-021 0 1.0518364965731301e-021 1.6940658945086007e-021 0.99999999999999989 0
		 0.87649405056805607 2.386671790283823 1.516462011961607 1;
	setAttr ".radi" 0.075000000000000011;
createNode joint -n "lowLegFrontProxySkinBtwEndIkLFT_jnt" -p "lowLegFrontProxySkinBtwIkLFT_jnt";
	rename -uid "6DD5C6A1-402D-0E4F-6B51-B58AABE2E916";
	setAttr ".t" -type "double3" -2.2204460492503131e-016 0.078573161233433275 -2.2204460492503131e-016 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 0 1.0209422281829385e-040 ;
	setAttr ".bps" -type "matrix" -1 1.2246467991021825e-016 1.0518364965731299e-021 0
		 -1.2246467991021825e-016 -0.99999999999999989 1.6940658945086007e-021 0 1.0518364965731301e-021 1.6940658945086007e-021 0.99999999999999989 0
		 0.87649405056805607 2.386671790283823 1.516462011961607 1;
	setAttr ".radi" 0.025;
createNode ikEffector -n "lowLegFrontProxySkinBtwEndLFT_eff" -p "lowLegFrontProxySkinBtwIkLFT_jnt";
	rename -uid "A41F9DE4-4696-8BB7-F8BC-AFACF24E5522";
	setAttr ".v" no;
	setAttr ".hd" yes;
createNode joint -n "lowLegFrontProxySkinBtwAimLFT_jnt" -p "lowLegFrontProxySkinBtwHideLFT_grp";
	rename -uid "82692047-445A-6780-32F3-14B2C7E34E02";
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 9.7062825972397386e-020 -6.0265791991467016e-020 2.5880886611741528e-025 ;
	setAttr ".bps" -type "matrix" -1 1.2246467991021825e-016 1.0518364965731299e-021 0
		 -1.2246467991021825e-016 -0.99999999999999989 1.6940658945086007e-021 0 1.0518364965731301e-021 1.6940658945086007e-021 0.99999999999999989 0
		 0.87649405056805607 2.386671790283823 1.516462011961607 1;
	setAttr ".radi" 0.075000000000000011;
createNode joint -n "lowLegFrontProxySkinBtwAimEndLFT_jnt" -p "lowLegFrontProxySkinBtwAimLFT_jnt";
	rename -uid "37583065-4091-5BA5-C845-30921FA5E558";
	setAttr ".t" -type "double3" 0 0.078573161233433275 -2.2204460492503131e-016 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 0 1.0209422281829385e-040 ;
	setAttr ".bps" -type "matrix" -1 1.2246467991021825e-016 1.0518364965731299e-021 0
		 -1.2246467991021825e-016 -0.99999999999999989 1.6940658945086007e-021 0 1.0518364965731301e-021 1.6940658945086007e-021 0.99999999999999989 0
		 0.87649405056805607 2.386671790283823 1.516462011961607 1;
	setAttr ".radi" 0.025;
createNode aimConstraint -n "lowLegFrontProxySkinBtwAimLFT_jnt_aimConstraint1" -p
		 "lowLegFrontProxySkinBtwAimLFT_jnt";
	rename -uid "4F70B251-4A49-237D-622D-E08D5A93A971";
	addAttr -dcb 0 -ci true -sn "w0" -ln "lowLegRbnDtl1FrontProxySkinLFT_jntW0" -dv 
		1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".a" -type "double3" 0 1 0 ;
	setAttr ".u" -type "double3" 1 0 0 ;
	setAttr ".wut" 1;
	setAttr ".rsrr" -type "double3" -5.1066676434585681 1.6291844766851675e-014 -3.6534037288635199e-013 ;
	setAttr -k on ".w0";
createNode joint -n "lowLegFrontProxySkinBtwVoidLFT_jnt" -p "lowLegFrontProxySkinBtwHideLFT_grp";
	rename -uid "598865D5-4B2A-35B4-1B70-CD97243A3599";
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 9.7062825972397386e-020 -6.0265791991467016e-020 2.5880886611741528e-025 ;
	setAttr ".bps" -type "matrix" -1 1.2246467991021825e-016 1.0518364965731299e-021 0
		 -1.2246467991021825e-016 -0.99999999999999989 1.6940658945086007e-021 0 1.0518364965731301e-021 1.6940658945086007e-021 0.99999999999999989 0
		 0.87649405056805607 2.386671790283823 1.516462011961607 1;
	setAttr ".radi" 0.075000000000000011;
createNode joint -n "lowLegFrontProxySkinBtwVoidEndLFT_jnt" -p "lowLegFrontProxySkinBtwVoidLFT_jnt";
	rename -uid "FD5AD470-47BC-7B80-B699-488C2BF3D0B1";
	setAttr ".t" -type "double3" 0 0.078573161233433275 -2.2204460492503131e-016 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 0 1.0209422281829385e-040 ;
	setAttr ".bps" -type "matrix" -1 1.2246467991021825e-016 1.0518364965731299e-021 0
		 -1.2246467991021825e-016 -0.99999999999999989 1.6940658945086007e-021 0 1.0518364965731301e-021 1.6940658945086007e-021 0.99999999999999989 0
		 0.87649405056805607 2.386671790283823 1.516462011961607 1;
	setAttr ".radi" 0.025;
createNode ikHandle -n "lowLegFrontProxySkinBtwLFT_ikHndl" -p "lowLegFrontProxySkinBtwHideLFT_grp";
	rename -uid "AD7155EF-4647-9275-6888-039CC31C0E3F";
	setAttr ".r" -type "double3" -1.987846675914698e-016 1.1806179270048667e-016 -180 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1.0000000000000002 ;
	setAttr ".hs" 1;
	setAttr -l on ".pv" -type "double3" 0 0 0 ;
	setAttr -l on ".pv";
	setAttr ".roc" yes;
createNode pointConstraint -n "lowLegFrontProxySkinBtwLFT_ikHndl_pointConstraint1" 
		-p "lowLegFrontProxySkinBtwLFT_ikHndl";
	rename -uid "6FAC52ED-4CB5-8254-C65E-A4B32003345A";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "lowLegFrontProxySkinBtwAimEndLFT_jntW0" 
		-dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "lowLegFrontProxySkinBtwVoidEndLFT_jntW1" 
		-dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" 0.87649405056805674 2.3473852096671064 1.5164620119616063 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode joint -n "lowLegFrontProxySkinBtwLFT_jnt" -p "lowLegFrontProxySkinBtwAllMoveLFT_grp";
	rename -uid "10212DE8-49B9-6700-1E3D-478673CCFC6D";
	addAttr -ci true -sn "aimBtw" -ln "aimBtw" -dv 0.5 -min 0 -max 1 -at "float";
	addAttr -ci true -sn "scaleBtw" -ln "scaleBtw" -dv 2 -at "float";
	setAttr -l on ".v";
	setAttr -l on ".t";
	setAttr -l on ".r";
	setAttr ".ro" 1;
	setAttr -l on ".s";
	setAttr ".jo" -type "double3" -2.5533338217290762 0 0 ;
	setAttr ".bps" -type "matrix" -1 1.2246467991021825e-016 1.0518364965731299e-021 0
		 -1.2246467991021825e-016 -0.99999999999999989 1.6940658945086007e-021 0 1.0518364965731301e-021 1.6940658945086007e-021 0.99999999999999989 0
		 0.87649405056805607 2.386671790283823 1.516462011961607 1;
	setAttr ".radi" 0.075000000000000011;
	setAttr -k on ".aimBtw";
	setAttr -k on ".scaleBtw";
createNode joint -n "lowLegFrontProxySkinBtwEndLFT_jnt" -p "lowLegFrontProxySkinBtwLFT_jnt";
	rename -uid "85B8BAF5-4947-5373-7EFB-DF84F47F5E5A";
	setAttr ".t" -type "double3" -2.2204460492503131e-016 0.078573161233433275 -2.2204460492503131e-016 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 0 1.0209422281829385e-040 ;
	setAttr ".bps" -type "matrix" -1 1.2246467991021825e-016 1.0518364965731299e-021 0
		 -1.2246467991021825e-016 -0.99999999999999989 1.6940658945086007e-021 0 1.0518364965731301e-021 1.6940658945086007e-021 0.99999999999999989 0
		 0.87649405056805607 2.386671790283823 1.516462011961607 1;
	setAttr ".radi" 0.025;
createNode parentConstraint -n "lowLegFrontProxySkinBtwLFT_jnt_parentConstraint1" 
		-p "lowLegFrontProxySkinBtwLFT_jnt";
	rename -uid "FD247F1E-43C5-6BAA-EC66-46A414BE5448";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "lowLegFrontProxySkinBtwIkLFT_jntW0" 
		-dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 2.2204460492503131e-016 0 0 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "lowLegFrontProxySkinBtwAllLFT_grp_pointConstraint1" 
		-p "lowLegFrontProxySkinBtwAllLFT_grp";
	rename -uid "146009D7-497A-6067-D424-179D7D598099";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "lowLegFrontProxySkinLFT_jntW0" -dv 
		1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 1.3322676295501878e-015 -0.14600056231296499 0 ;
	setAttr -k on ".w0";
createNode joint -n "upLegRbnDtl1FrontProxySkinLFT_jnt" -p "upLegFrontProxySkinLFT_jnt";
	rename -uid "D42EB98D-43A4-AD2D-C556-B8A02A1F579F";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 2.2204460492503131e-016 0.11398384262178585 -0.060159549481627828 ;
	setAttr ".r" -type "double3" 7.0943673017528823e-031 6.5503391962795923e-015 1.2410878048480956e-014 ;
	setAttr ".jo" -type "double3" 27.824650853315116 0 180 ;
	setAttr ".bps" -type "matrix" 1 -2.4492935982495357e-016 -1.0518364965731297e-021 0
		 2.1661117596823951e-016 0.88438023583736081 0.4667671780023247 0 -1.1432405587200067e-016 -0.4667671780023247 0.88438023583736081 0
		 0.87649405056804897 5.3875365211314383 2.3130606626425774 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode joint -n "upLegRbnDtl2FrontProxySkinLFT_jnt" -p "upLegFrontProxySkinLFT_jnt";
	rename -uid "63208074-4732-4DD9-4E10-8FA1CC5B8724";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 2.2204460492503131e-016 0.41144041815607757 -0.2171542003276139 ;
	setAttr ".r" -type "double3" 7.0943673017528823e-031 6.5503391962795923e-015 1.2410878048480956e-014 ;
	setAttr ".jo" -type "double3" 27.824650853315116 0 180 ;
	setAttr ".bps" -type "matrix" 1 -2.4492935982495357e-016 -1.0518364965731297e-021 0
		 2.1661117596823951e-016 0.88438023583736081 0.4667671780023247 0 -1.1432405587200067e-016 -0.4667671780023247 0.88438023583736081 0
		 0.87649405056804897 5.0900799455971466 2.1560660117965913 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode joint -n "upLegRbnDtl3FrontProxySkinLFT_jnt" -p "upLegFrontProxySkinLFT_jnt";
	rename -uid "4222AB80-47B6-40C3-4845-83ABE325AD13";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 1.1102230246251565e-016 0.78707199526743565 -0.41540884704167391 ;
	setAttr ".r" -type "double3" 7.0943673017528823e-031 6.5503391962795923e-015 1.2410878048480956e-014 ;
	setAttr ".jo" -type "double3" 27.824650853315116 0 180 ;
	setAttr ".bps" -type "matrix" 1 -2.4492935982495357e-016 -1.0518364965731297e-021 0
		 2.1661117596823951e-016 0.88438023583736081 0.4667671780023247 0 -1.1432405587200067e-016 -0.4667671780023247 0.88438023583736081 0
		 0.87649405056804874 4.7144483684857885 1.9578113650825313 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode joint -n "upLegRbnDtl4FrontProxySkinLFT_jnt" -p "upLegFrontProxySkinLFT_jnt";
	rename -uid "55856541-4DDF-E07F-F2BC-ECAA5B6F4968";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 1.1102230246251565e-016 1.1627035723787955 -0.61366349375573259 ;
	setAttr ".r" -type "double3" 7.0943673017528823e-031 6.5503391962795923e-015 1.2410878048480956e-014 ;
	setAttr ".jo" -type "double3" 27.824650853315116 0 180 ;
	setAttr ".bps" -type "matrix" 1 -2.4492935982495357e-016 -1.0518364965731297e-021 0
		 2.1661117596823951e-016 0.88438023583736081 0.4667671780023247 0 -1.1432405587200067e-016 -0.4667671780023247 0.88438023583736081 0
		 0.87649405056804874 4.3388167913744287 1.7595567183684726 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode joint -n "upLegRbnDtl5FrontProxySkinLFT_jnt" -p "upLegFrontProxySkinLFT_jnt";
	rename -uid "722799B7-44A0-A852-BD65-CBB46AEB75A0";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -4.4408920985006262e-016 1.4601601479130881 -0.77065814460171733 ;
	setAttr ".r" -type "double3" 7.0943673017528823e-031 6.5503391962795923e-015 1.2410878048480956e-014 ;
	setAttr ".jo" -type "double3" 27.824650853315116 0 180 ;
	setAttr ".bps" -type "matrix" 1 -2.4492935982495357e-016 -1.0518364965731297e-021 0
		 2.1661117596823951e-016 0.88438023583736081 0.4667671780023247 0 -1.1432405587200067e-016 -0.4667671780023247 0.88438023583736081 0
		 0.87649405056804897 4.041360215840136 1.6025620675224879 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode transform -n "midLegFrontProxySkinBtwAllLFT_grp" -p "upLegRbnDtl5FrontProxySkinLFT_jnt";
	rename -uid "17015D34-4E9F-75B5-B038-9FB494930575";
	setAttr ".t" -type "double3" 5.5511151231257827e-016 -0.12888556076093138 4.4408920985006262e-016 ;
	setAttr ".r" -type "double3" 27.824650853315116 -3.2751163001436584e-015 -180 ;
createNode transform -n "midLegFrontProxySkinBtwAllMoveLFT_grp" -p "midLegFrontProxySkinBtwAllLFT_grp";
	rename -uid "DFB6B6D7-42AB-57AA-DC55-9B8315AD287A";
	setAttr ".t" -type "double3" 0 -0.048764187812530405 0 ;
createNode transform -n "midLegFrontProxySkinBtwHideLFT_grp" -p "midLegFrontProxySkinBtwAllMoveLFT_grp";
	rename -uid "A5B32BCF-4FEF-2BC1-3D81-8280B3F17A3B";
	setAttr ".v" no;
createNode transform -n "midLegFrontProxySkinBtwUpLFT_grp" -p "midLegFrontProxySkinBtwHideLFT_grp";
	rename -uid "C9E534E4-48FF-A735-0B75-37AC8B21377F";
createNode joint -n "midLegFrontProxySkinBtwIkLFT_jnt" -p "midLegFrontProxySkinBtwHideLFT_grp";
	rename -uid "DC55ED09-4618-0F8E-F9D5-788BDD23DABE";
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" -0.36154087872573787 0 0 ;
	setAttr ".bps" -type "matrix" -1 1.2246467991021825e-016 1.0518364965731299e-021 0
		 -1.2246467991021825e-016 -0.99999999999999989 1.6940658945086007e-021 0 1.0518364965731301e-021 1.6940658945086007e-021 0.99999999999999989 0
		 0.87649405056804963 3.9273763732183529 1.5424025180408609 1;
	setAttr ".radi" 0.075000000000000011;
createNode joint -n "midLegFrontProxySkinBtwEndIkLFT_jnt" -p "midLegFrontProxySkinBtwIkLFT_jnt";
	rename -uid "17E2D2BC-417F-123C-18B7-AAA1B66C8E48";
	setAttr ".t" -type "double3" 0 0.097528375625060892 -2.2204460492503131e-016 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 0 1.0209422281829385e-040 ;
	setAttr ".bps" -type "matrix" -1 1.2246467991021825e-016 1.0518364965731299e-021 0
		 -1.2246467991021825e-016 -0.99999999999999989 1.6940658945086007e-021 0 1.0518364965731301e-021 1.6940658945086007e-021 0.99999999999999989 0
		 0.87649405056804963 3.9273763732183529 1.5424025180408609 1;
	setAttr ".radi" 0.025;
createNode ikEffector -n "midLegFrontProxySkinBtwEndLFT_eff" -p "midLegFrontProxySkinBtwIkLFT_jnt";
	rename -uid "7155FCCC-4B61-D5A1-621F-71B9CBFD62E2";
	setAttr ".v" no;
	setAttr ".hd" yes;
createNode joint -n "midLegFrontProxySkinBtwAimLFT_jnt" -p "midLegFrontProxySkinBtwHideLFT_grp";
	rename -uid "E0303838-4F8B-ACC8-C543-05B32DD1738A";
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 9.7062825972397386e-020 -6.0265791991467016e-020 2.5880886611741528e-025 ;
	setAttr ".bps" -type "matrix" -1 1.2246467991021825e-016 1.0518364965731299e-021 0
		 -1.2246467991021825e-016 -0.99999999999999989 1.6940658945086007e-021 0 1.0518364965731301e-021 1.6940658945086007e-021 0.99999999999999989 0
		 0.87649405056804963 3.9273763732183529 1.5424025180408609 1;
	setAttr ".radi" 0.075000000000000011;
createNode joint -n "midLegFrontProxySkinBtwAimEndLFT_jnt" -p "midLegFrontProxySkinBtwAimLFT_jnt";
	rename -uid "F50E29F4-40A0-F664-A608-189DBB1E5EDC";
	setAttr ".t" -type "double3" 0 0.097528375625060448 -2.2204460492503131e-016 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 0 1.0209422281829385e-040 ;
	setAttr ".bps" -type "matrix" -1 1.2246467991021825e-016 1.0518364965731299e-021 0
		 -1.2246467991021825e-016 -0.99999999999999989 1.6940658945086007e-021 0 1.0518364965731301e-021 1.6940658945086007e-021 0.99999999999999989 0
		 0.87649405056804963 3.9273763732183529 1.5424025180408609 1;
	setAttr ".radi" 0.025;
createNode aimConstraint -n "midLegFrontProxySkinBtwAimLFT_jnt_aimConstraint1" -p
		 "midLegFrontProxySkinBtwAimLFT_jnt";
	rename -uid "C93456E8-42B9-765B-457A-898052551E64";
	addAttr -dcb 0 -ci true -sn "w0" -ln "midLegRbnDtl1FrontProxySkinLFT_jntW0" -dv 
		1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".a" -type "double3" 0 1 0 ;
	setAttr ".u" -type "double3" 1 0 0 ;
	setAttr ".wut" 1;
	setAttr ".rsrr" -type "double3" -0.72308175745129122 -1.8548854980564977e-015 2.9395209842098428e-013 ;
	setAttr -k on ".w0";
createNode joint -n "midLegFrontProxySkinBtwVoidLFT_jnt" -p "midLegFrontProxySkinBtwHideLFT_grp";
	rename -uid "0363342A-4795-25F5-B86E-1A9B74468F9B";
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 9.7062825972397386e-020 -6.0265791991467016e-020 2.5880886611741528e-025 ;
	setAttr ".bps" -type "matrix" -1 1.2246467991021825e-016 1.0518364965731299e-021 0
		 -1.2246467991021825e-016 -0.99999999999999989 1.6940658945086007e-021 0 1.0518364965731301e-021 1.6940658945086007e-021 0.99999999999999989 0
		 0.87649405056804963 3.9273763732183529 1.5424025180408609 1;
	setAttr ".radi" 0.075000000000000011;
createNode joint -n "midLegFrontProxySkinBtwVoidEndLFT_jnt" -p "midLegFrontProxySkinBtwVoidLFT_jnt";
	rename -uid "0ADA2C33-4F41-E66F-3DE8-84A0F7F0A8D0";
	setAttr ".t" -type "double3" 0 0.097528375625060448 -2.2204460492503131e-016 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 0 1.0209422281829385e-040 ;
	setAttr ".bps" -type "matrix" -1 1.2246467991021825e-016 1.0518364965731299e-021 0
		 -1.2246467991021825e-016 -0.99999999999999989 1.6940658945086007e-021 0 1.0518364965731301e-021 1.6940658945086007e-021 0.99999999999999989 0
		 0.87649405056804963 3.9273763732183529 1.5424025180408609 1;
	setAttr ".radi" 0.025;
createNode ikHandle -n "midLegFrontProxySkinBtwLFT_ikHndl" -p "midLegFrontProxySkinBtwHideLFT_grp";
	rename -uid "09BA2956-4B5E-1837-3576-8EABBAB6EC6E";
	setAttr ".r" -type "double3" 4.4745859414827788e-031 3.2751093323478049e-015 -180 ;
	setAttr ".hs" 1;
	setAttr -l on ".pv" -type "double3" 0 0 0 ;
	setAttr -l on ".pv";
	setAttr ".roc" yes;
createNode pointConstraint -n "midLegFrontProxySkinBtwLFT_ikHndl_pointConstraint1" 
		-p "midLegFrontProxySkinBtwLFT_ikHndl";
	rename -uid "A4DC4B02-49BB-5DB2-83DC-3B83E88221A7";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "midLegFrontProxySkinBtwAimEndLFT_jntW0" 
		-dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "midLegFrontProxySkinBtwVoidEndLFT_jntW1" 
		-dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" 0.87649405056804996 3.8786121854058226 1.5424025180408609 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode joint -n "midLegFrontProxySkinBtwLFT_jnt" -p "midLegFrontProxySkinBtwAllMoveLFT_grp";
	rename -uid "879A3904-4CFC-8344-2646-488B6CAE2B74";
	addAttr -ci true -sn "aimBtw" -ln "aimBtw" -dv 0.5 -min 0 -max 1 -at "float";
	addAttr -ci true -sn "scaleBtw" -ln "scaleBtw" -dv 2 -at "float";
	setAttr -l on ".v";
	setAttr -l on ".t";
	setAttr -l on ".r";
	setAttr ".ro" 1;
	setAttr -l on ".s";
	setAttr ".jo" -type "double3" -0.36154087872573787 0 0 ;
	setAttr ".bps" -type "matrix" -1 1.2246467991021825e-016 1.0518364965731299e-021 0
		 -1.2246467991021825e-016 -0.99999999999999989 1.6940658945086007e-021 0 1.0518364965731301e-021 1.6940658945086007e-021 0.99999999999999989 0
		 0.87649405056804963 3.9273763732183529 1.5424025180408609 1;
	setAttr ".radi" 0.075000000000000011;
	setAttr -k on ".aimBtw";
	setAttr -k on ".scaleBtw";
createNode joint -n "midLegFrontProxySkinBtwEndLFT_jnt" -p "midLegFrontProxySkinBtwLFT_jnt";
	rename -uid "691783B1-44D9-C072-C906-4186DEEB6293";
	setAttr ".t" -type "double3" 0 0.097528375625060892 -2.2204460492503131e-016 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 0 1.0209422281829385e-040 ;
	setAttr ".bps" -type "matrix" -1 1.2246467991021825e-016 1.0518364965731299e-021 0
		 -1.2246467991021825e-016 -0.99999999999999989 1.6940658945086007e-021 0 1.0518364965731301e-021 1.6940658945086007e-021 0.99999999999999989 0
		 0.87649405056804963 3.9273763732183529 1.5424025180408609 1;
	setAttr ".radi" 0.025;
createNode parentConstraint -n "midLegFrontProxySkinBtwLFT_jnt_parentConstraint1" 
		-p "midLegFrontProxySkinBtwLFT_jnt";
	rename -uid "02122335-4AD9-F548-9228-C3B16C4CAA73";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "midLegFrontProxySkinBtwIkLFT_jntW0" 
		-dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode joint -n "clav1ScaScapProxySkinLFT_jnt" -p "clav1ScapProxySkinLFT_jnt";
	rename -uid "294E9F6D-42A2-423E-99B9-01AB4F86A350";
	setAttr ".t" -type "double3" 6.6613381477509392e-016 2.6645352591003757e-015 -2.2204460492503131e-016 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1 ;
	setAttr ".radi" 0.1;
createNode joint -n "clav1ScapProxySkinRGT_jnt" -p "spine2ProxySkin_jnt";
	rename -uid "D8F7D544-481F-9E72-0712-82B532A493BF";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -1.11983 0.47504212309777127 0.96929396312925886 ;
	setAttr ".jo" -type "double3" 0 180 0 ;
	setAttr ".bps" -type "matrix" -1 1.0518364965731299e-021 -1.2246467991021825e-016 0
		 0 0.99999999996311539 8.5888967929811228e-006 0 1.2246467991473532e-016 8.5888967929811228e-006 -0.99999999996311539 0
		 -1.1198300000000001 5.9900500000000054 1.79111 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode joint -n "clav2ScapProxySkinRGT_jnt" -p "clav1ScapProxySkinRGT_jnt";
	rename -uid "3999F724-414F-AD43-A7F5-93A7492E6B02";
	setAttr ".t" -type "double3" -0.24333600000000011 -0.48852500029927182 -0.58211419591227909 ;
	setAttr ".r" -type "double3" -6.7943978180678134e-019 0 0 ;
	setAttr ".jo" -type "double3" 0.00049210753691327814 0 0 ;
	setAttr ".bps" -type "matrix" -1 1.0518364965731299e-021 -1.2246467991021825e-016 0
		 1.0518364965644982e-021 0.99999999999999989 7.048330560692484e-017 0 1.2246467991021825e-016 7.048330560692484e-017 -0.99999999999999989 0
		 -0.87649400000000011 5.501520000000002 2.3732199999999999 1;
	setAttr ".radi" 0.1;
createNode joint -n "upLegFrontProxySkinRGT_jnt" -p "clav2ScapProxySkinRGT_jnt";
	rename -uid "5BEA242A-461D-951F-F5F1-75AFB83F6FBA";
	setAttr ".t" -type "double3" -1.1102230246251565e-016 1.7763568394002505e-015 0 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 4.0384930002335377e-015 -2.5880886611741528e-025 -6.0265791991467016e-020 ;
	setAttr ".bps" -type "matrix" -1 1.0518364965731299e-021 -1.2246467991021825e-016 0
		 1.0518364965644982e-021 0.99999999999999989 7.048330560692484e-017 0 1.2246467991021825e-016 7.048330560692484e-017 -0.99999999999999989 0
		 -0.876494 5.5015200000000037 2.3732199999999999 1;
	setAttr ".radi" 0.05;
createNode joint -n "midLegFrontProxySkinRGT_jnt" -p "upLegFrontProxySkinRGT_jnt";
	rename -uid "AB917E39-47AD-16FA-ACF4-FE87D6DA7283";
	setAttr ".t" -type "double3" 1.1102230246251565e-016 -1.5741399999999999 0.83081999999999923 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -1 1.0518364965731299e-021 -1.2246467991021825e-016 0
		 1.0518364965644982e-021 0.99999999999999989 7.048330560692484e-017 0 1.2246467991021825e-016 7.048330560692484e-017 -0.99999999999999989 0
		 -0.876494 3.9273800000000039 1.5424000000000002 1;
	setAttr ".radi" 0.05;
createNode joint -n "lowLegFrontProxySkinRGT_jnt" -p "midLegFrontProxySkinRGT_jnt";
	rename -uid "9CA10F96-4987-89EE-11F6-28BBA27063D8";
	setAttr ".t" -type "double3" 0 -1.5407100000000002 0.02594000000000074 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -1 1.0518364965731299e-021 -1.2246467991021825e-016 0
		 1.0518364965644982e-021 0.99999999999999989 7.048330560692484e-017 0 1.2246467991021825e-016 7.048330560692484e-017 -0.99999999999999989 0
		 -0.876494 2.3866700000000041 1.5164599999999999 1;
	setAttr ".radi" 0.05;
createNode joint -n "ankleFrontProxySkinRGT_jnt" -p "lowLegFrontProxySkinRGT_jnt";
	rename -uid "1561EB96-4422-9634-BE89-B58F6932444D";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 -1.1679399999999998 0.13947999999999938 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -1 1.0518364965731299e-021 -1.2246467991021825e-016 0
		 1.0518364965644982e-021 0.99999999999999989 7.048330560692484e-017 0 1.2246467991021825e-016 7.048330560692484e-017 -0.99999999999999989 0
		 -0.876494 1.2187300000000043 1.3769800000000005 1;
	setAttr ".radi" 0.05;
	setAttr ".liw" yes;
createNode joint -n "ballFrontProxySkinRGT_jnt" -p "ankleFrontProxySkinRGT_jnt";
	rename -uid "24742F26-48C7-F2E8-B17F-858415EFFB09";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 -0.78938700000000006 -0.3774999999999995 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 0 180 ;
	setAttr ".bps" -type "matrix" 1 1.2246362807823873e-016 1.2246467991021825e-016 0
		 1.2246362807823875e-016 -0.99999999999999989 -7.0483305606924827e-017 0 1.2246467991021825e-016 7.048330560692484e-017 -0.99999999999999989 0
		 -0.876494 0.42934300000000447 1.75448 1;
	setAttr ".radi" 0.05;
	setAttr ".liw" yes;
createNode joint -n "toeFrontProxySkinRGT_jnt" -p "ballFrontProxySkinRGT_jnt";
	rename -uid "11786354-4B64-A6C0-6030-5891DC36D18D";
	setAttr ".t" -type "double3" 3.3306690738754696e-016 0.42934299999999986 -0.85509999999999819 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 89.999999999999929 0 -179.99999999999994 ;
	setAttr ".radi" 0.05;
createNode transform -n "ballFrontProxySkinBtwAllRGT_grp" -p "ankleFrontProxySkinRGT_jnt";
	rename -uid "78E0E6F6-4531-04B6-4450-5D9D2C0FC435";
	setAttr ".r" -type "double3" -1.1055105235942442e-014 -7.0167092982760673e-015 
		180 ;
createNode transform -n "ballFrontProxySkinBtwAllMoveRGT_grp" -p "ballFrontProxySkinBtwAllRGT_grp";
	rename -uid "AD6A07AA-4F9F-EFF1-2728-918C495FF4B6";
	setAttr ".t" -type "double3" 0 -0.31958257661123435 0 ;
createNode transform -n "ballFrontProxySkinBtwHideRGT_grp" -p "ballFrontProxySkinBtwAllMoveRGT_grp";
	rename -uid "60839703-4233-433D-7504-43BFCFC7D300";
	setAttr ".v" no;
createNode transform -n "ballFrontProxySkinBtwUpRGT_grp" -p "ballFrontProxySkinBtwHideRGT_grp";
	rename -uid "91C97996-4EE8-28C9-B23F-04975C14AD4B";
createNode joint -n "ballFrontProxySkinBtwIkRGT_jnt" -p "ballFrontProxySkinBtwHideRGT_grp";
	rename -uid "080CF43D-41C3-54E0-D5A2-D2B4384E2FCF";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 0 2.2204460492503131e-016 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" -24.393512519760847 0 0 ;
	setAttr ".bps" -type "matrix" 1 1.2246362807823873e-016 1.2246467991021825e-016 0
		 1.2246362807823875e-016 -0.99999999999999989 -7.0483305606924827e-017 0 1.2246467991021825e-016 7.048330560692484e-017 -0.99999999999999989 0
		 -0.876494 0.42934300000000447 1.75448 1;
	setAttr ".radi" 0.075000000000000011;
	setAttr ".liw" yes;
createNode joint -n "ballFrontProxySkinBtwEndIkRGT_jnt" -p "ballFrontProxySkinBtwIkRGT_jnt";
	rename -uid "5620C1BF-4157-04D2-F9BB-57B20DDC15BC";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 0.63916515322246892 0 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" -7.0622500768802555e-031 0 -4.9456086582534319e-031 ;
	setAttr ".bps" -type "matrix" 1 1.2246362807823873e-016 1.2246467991021825e-016 0
		 1.2246362807823875e-016 -0.99999999999999989 -7.0483305606924827e-017 0 1.2246467991021825e-016 7.048330560692484e-017 -0.99999999999999989 0
		 -0.876494 0.42934300000000447 1.75448 1;
	setAttr ".radi" 0.025;
	setAttr ".liw" yes;
createNode ikEffector -n "ballFrontProxySkinBtwEndRGT_eff" -p "ballFrontProxySkinBtwIkRGT_jnt";
	rename -uid "6D73320F-4582-A2F6-1FC6-138DF48A9E45";
	setAttr ".v" no;
	setAttr ".hd" yes;
createNode joint -n "ballFrontProxySkinBtwAimRGT_jnt" -p "ballFrontProxySkinBtwHideRGT_grp";
	rename -uid "54597EF8-4CB7-0874-09D4-B18B6D0BB52F";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 1.105510523594244e-014 7.0167092982760688e-015 -7.0166490327428823e-015 ;
	setAttr ".bps" -type "matrix" 1 1.2246362807823873e-016 1.2246467991021825e-016 0
		 1.2246362807823875e-016 -0.99999999999999989 -7.0483305606924827e-017 0 1.2246467991021825e-016 7.048330560692484e-017 -0.99999999999999989 0
		 -0.876494 0.42934300000000447 1.75448 1;
	setAttr ".radi" 0.075000000000000011;
	setAttr ".liw" yes;
createNode joint -n "ballFrontProxySkinBtwAimEndRGT_jnt" -p "ballFrontProxySkinBtwAimRGT_jnt";
	rename -uid "61101341-4862-20FE-9128-7C819B80BFF8";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 0.63916515322246892 2.2204460492503131e-016 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" -7.0622500768802555e-031 0 -4.9456086582534319e-031 ;
	setAttr ".bps" -type "matrix" 1 1.2246362807823873e-016 1.2246467991021825e-016 0
		 1.2246362807823875e-016 -0.99999999999999989 -7.0483305606924827e-017 0 1.2246467991021825e-016 7.048330560692484e-017 -0.99999999999999989 0
		 -0.876494 0.42934300000000447 1.75448 1;
	setAttr ".radi" 0.025;
	setAttr ".liw" yes;
createNode aimConstraint -n "ballFrontProxySkinBtwAimRGT_jnt_aimConstraint1" -p "ballFrontProxySkinBtwAimRGT_jnt";
	rename -uid "513517C2-40A6-D8A8-6843-FC9EA1B6BE43";
	addAttr -dcb 0 -ci true -sn "w0" -ln "toeFrontProxySkinRGT_jntW0" -dv 1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".a" -type "double3" 0 1 0 ;
	setAttr ".u" -type "double3" 1 0 0 ;
	setAttr ".wut" 1;
	setAttr ".rsrr" -type "double3" -48.787025039521737 7.6132496216575124e-015 -1.6788368203678696e-014 ;
	setAttr -k on ".w0";
createNode joint -n "ballFrontProxySkinBtwVoidRGT_jnt" -p "ballFrontProxySkinBtwHideRGT_grp";
	rename -uid "07A36C8B-4964-0AAC-140A-F5A9D8CBF739";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 1.105510523594244e-014 7.0167092982760688e-015 -7.0166490327428823e-015 ;
	setAttr ".bps" -type "matrix" 1 1.2246362807823873e-016 1.2246467991021825e-016 0
		 1.2246362807823875e-016 -0.99999999999999989 -7.0483305606924827e-017 0 1.2246467991021825e-016 7.048330560692484e-017 -0.99999999999999989 0
		 -0.876494 0.42934300000000447 1.75448 1;
	setAttr ".radi" 0.075000000000000011;
	setAttr ".liw" yes;
createNode joint -n "ballFrontProxySkinBtwVoidEndRGT_jnt" -p "ballFrontProxySkinBtwVoidRGT_jnt";
	rename -uid "A11A4DAE-4699-39CA-7CCA-6B85F96D0290";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 0.63916515322246892 2.2204460492503131e-016 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" -7.0622500768802555e-031 0 -4.9456086582534319e-031 ;
	setAttr ".bps" -type "matrix" 1 1.2246362807823873e-016 1.2246467991021825e-016 0
		 1.2246362807823875e-016 -0.99999999999999989 -7.0483305606924827e-017 0 1.2246467991021825e-016 7.048330560692484e-017 -0.99999999999999989 0
		 -0.876494 0.42934300000000447 1.75448 1;
	setAttr ".radi" 0.025;
	setAttr ".liw" yes;
createNode ikHandle -n "ballFrontProxySkinBtwRGT_ikHndl" -p "ballFrontProxySkinBtwHideRGT_grp";
	rename -uid "FF572FFB-4562-1958-D822-849C3A486677";
	setAttr ".r" -type "double3" 180 -7.0167092982760673e-015 7.0166490327428839e-015 ;
	setAttr ".hs" 1;
	setAttr -l on ".pv" -type "double3" 0 0 0 ;
	setAttr -l on ".pv";
	setAttr ".roc" yes;
createNode pointConstraint -n "ballFrontProxySkinBtwRGT_ikHndl_pointConstraint1" 
		-p "ballFrontProxySkinBtwRGT_ikHndl";
	rename -uid "7CDF9B72-450F-2798-A67A-D0B7AA0A348B";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "ballFrontProxySkinBtwAimEndRGT_jntW0" 
		-dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "ballFrontProxySkinBtwVoidEndRGT_jntW1" 
		-dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" -0.87649399999999988 0.10976042338877023 1.7544799999999998 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode joint -n "ballFrontProxySkinBtwRGT_jnt" -p "ballFrontProxySkinBtwAllMoveRGT_grp";
	rename -uid "74BB5D3B-4FBD-D57E-B9E3-DABB527E9F5A";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "aimBtw" -ln "aimBtw" -dv 0.5 -min 0 -max 1 -at "float";
	addAttr -ci true -sn "scaleBtw" -ln "scaleBtw" -dv 2 -at "float";
	setAttr -l on ".v";
	setAttr -l on ".t";
	setAttr -l on ".r";
	setAttr ".ro" 1;
	setAttr -l on ".s";
	setAttr ".jo" -type "double3" -24.393512519760847 0 0 ;
	setAttr ".bps" -type "matrix" 1 1.2246362807823873e-016 1.2246467991021825e-016 0
		 1.2246362807823875e-016 -0.99999999999999989 -7.0483305606924827e-017 0 1.2246467991021825e-016 7.048330560692484e-017 -0.99999999999999989 0
		 -0.876494 0.42934300000000447 1.75448 1;
	setAttr ".radi" 0.075000000000000011;
	setAttr ".liw" yes;
	setAttr -k on ".aimBtw";
	setAttr -k on ".scaleBtw";
createNode joint -n "ballFrontProxySkinBtwEndRGT_jnt" -p "ballFrontProxySkinBtwRGT_jnt";
	rename -uid "9C2BD61E-4DF2-48BC-7073-8E91B129E90C";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 0.63916515322246903 -4.4408920985006262e-016 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" -7.0622500768802555e-031 0 -4.9456086582534319e-031 ;
	setAttr ".bps" -type "matrix" 1 1.2246362807823873e-016 1.2246467991021825e-016 0
		 1.2246362807823875e-016 -0.99999999999999989 -7.0483305606924827e-017 0 1.2246467991021825e-016 7.048330560692484e-017 -0.99999999999999989 0
		 -0.876494 0.42934300000000447 1.75448 1;
	setAttr ".radi" 0.025;
	setAttr ".liw" yes;
createNode parentConstraint -n "ballFrontProxySkinBtwRGT_jnt_parentConstraint1" -p
		 "ballFrontProxySkinBtwRGT_jnt";
	rename -uid "D0D6D7A0-475E-D7C4-240C-6AAF4374D86A";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "ballFrontProxySkinBtwIkRGT_jntW0" 
		-dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 0 0 4.4408920985006262e-016 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "ballFrontProxySkinBtwAllRGT_grp_pointConstraint1" 
		-p "ballFrontProxySkinBtwAllRGT_grp";
	rename -uid "2BBDA5E8-40E3-991A-E5E3-79B7DA3BF77C";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "ballFrontProxySkinRGT_jntW0" -dv 
		1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 0 -0.78938699999999984 -0.3774999999999995 ;
	setAttr -k on ".w0";
createNode joint -n "lowLegRbnDtl1FrontProxySkinRGT_jnt" -p "lowLegFrontProxySkinRGT_jnt";
	rename -uid "8F6C4B62-460C-47CA-1D83-FFA9A8FE9234";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 2.2204460492503131e-016 -0.11679400000000051 0.013947999999999627 ;
	setAttr ".r" -type "double3" 2.5444437451708128e-014 2.7586914362813485e-033 6.1255255007915905e-049 ;
	setAttr ".jo" -type "double3" 6.8102340987179213 180 0 ;
	setAttr ".bps" -type "matrix" 1 -1.0518364965817616e-021 2.4492935982495357e-016 0
		 -2.9043004496724517e-017 0.99294434310144619 0.11858132864341336 0 -2.4320134702581565e-016 -0.11858132864341336 0.99294434310144652 0
		 -0.87649400000000022 2.2698760000000036 1.5025120000000003 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode joint -n "lowLegRbnDtl2FrontProxySkinRGT_jnt" -p "lowLegFrontProxySkinRGT_jnt";
	rename -uid "87FFAF70-4F08-249B-1F63-2BA034EA63D6";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 4.4408920985006262e-016 -0.35038200000000108 0.04184399999999977 ;
	setAttr ".r" -type "double3" 2.5444437451708128e-014 2.7586914362813485e-033 6.1255255007915905e-049 ;
	setAttr ".jo" -type "double3" 6.8102340987179213 180 0 ;
	setAttr ".bps" -type "matrix" 1 -1.0518364965817616e-021 2.4492935982495357e-016 0
		 -2.9043004496724517e-017 0.99294434310144619 0.11858132864341336 0 -2.4320134702581565e-016 -0.11858132864341336 0.99294434310144652 0
		 -0.87649400000000044 2.036288000000003 1.4746160000000001 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode joint -n "lowLegRbnDtl3FrontProxySkinRGT_jnt" -p "lowLegFrontProxySkinRGT_jnt";
	rename -uid "81240421-4B63-7D08-F6C2-A68B8E816E5A";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 4.4408920985006262e-016 -0.58397000000000121 0.069739999999999913 ;
	setAttr ".r" -type "double3" 2.5444437451708128e-014 2.7586914362813485e-033 6.1255255007915905e-049 ;
	setAttr ".jo" -type "double3" 6.8102340987179213 180 0 ;
	setAttr ".bps" -type "matrix" 1 -1.0518364965817616e-021 2.4492935982495357e-016 0
		 -2.9043004496724517e-017 0.99294434310144619 0.11858132864341336 0 -2.4320134702581565e-016 -0.11858132864341336 0.99294434310144652 0
		 -0.87649400000000044 1.8027000000000029 1.44672 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode joint -n "lowLegRbnDtl4FrontProxySkinRGT_jnt" -p "lowLegFrontProxySkinRGT_jnt";
	rename -uid "AE104C69-4525-70CB-2FBF-D2AEDE1622A3";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 3.3306690738754696e-016 -0.81755800000000134 0.097635999999999612 ;
	setAttr ".r" -type "double3" 2.5444437451708128e-014 2.7586914362813485e-033 6.1255255007915905e-049 ;
	setAttr ".jo" -type "double3" 6.8102340987179213 180 0 ;
	setAttr ".bps" -type "matrix" 1 -1.0518364965817616e-021 2.4492935982495357e-016 0
		 -2.9043004496724517e-017 0.99294434310144619 0.11858132864341336 0 -2.4320134702581565e-016 -0.11858132864341336 0.99294434310144652 0
		 -0.87649400000000033 1.5691120000000027 1.4188240000000003 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode joint -n "lowLegRbnDtl5FrontProxySkinRGT_jnt" -p "lowLegFrontProxySkinRGT_jnt";
	rename -uid "DDF182A3-42B9-FF16-75DC-95B5CF76F1BD";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 2.2204460492503131e-016 -1.0511460000000008 0.12553199999999975 ;
	setAttr ".r" -type "double3" 2.5444437451708128e-014 2.7586914362813485e-033 6.1255255007915905e-049 ;
	setAttr ".jo" -type "double3" 6.8102340987179213 180 0 ;
	setAttr ".bps" -type "matrix" 1 -1.0518364965817616e-021 2.4492935982495357e-016 0
		 -2.9043004496724517e-017 0.99294434310144619 0.11858132864341336 0 -2.4320134702581565e-016 -0.11858132864341336 0.99294434310144652 0
		 -0.87649400000000022 1.3355240000000033 1.3909280000000002 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode transform -n "ankleFrontProxySkinBtwAllRGT_grp" -p "lowLegRbnDtl5FrontProxySkinRGT_jnt";
	rename -uid "37652C65-4418-15C4-9172-A2A0E2E18894";
	setAttr ".r" -type "double3" 6.8102340987179213 180 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 0.99999999999999978 ;
createNode transform -n "ankleFrontProxySkinBtwAllMoveRGT_grp" -p "ankleFrontProxySkinBtwAllRGT_grp";
	rename -uid "AD008C1A-41D1-7AFB-6DFD-50AD8F46D930";
	setAttr ".t" -type "double3" 0 0.29225249750181181 0 ;
createNode transform -n "ankleFrontProxySkinBtwHideRGT_grp" -p "ankleFrontProxySkinBtwAllMoveRGT_grp";
	rename -uid "1DCAC130-4AF0-7FAF-9B13-CB9ED80F600B";
	setAttr ".v" no;
createNode transform -n "ankleFrontProxySkinBtwUpRGT_grp" -p "ankleFrontProxySkinBtwHideRGT_grp";
	rename -uid "0FE7CB38-4BC1-12AE-04FB-70814FA7E568";
createNode joint -n "ankleFrontProxySkinBtwIkRGT_jnt" -p "ankleFrontProxySkinBtwHideRGT_grp";
	rename -uid "FB4D41C0-4684-8A3E-2FEF-CC972054CF5C";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 9.6196781705996184 0 0 ;
	setAttr ".bps" -type "matrix" -1 1.0518364965731299e-021 -1.2246467991021825e-016 0
		 1.0518364965644982e-021 0.99999999999999989 7.048330560692484e-017 0 1.2246467991021825e-016 7.048330560692484e-017 -0.99999999999999989 0
		 -0.876494 1.2187300000000043 1.3769800000000005 1;
	setAttr ".radi" 0.075000000000000011;
	setAttr ".liw" yes;
createNode joint -n "ankleFrontProxySkinBtwEndIkRGT_jnt" -p "ankleFrontProxySkinBtwIkRGT_jnt";
	rename -uid "7DE0E3F9-4E9E-73DE-6314-9F85B8113CBB";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -2.2204460492503131e-016 -0.58450499500362407 -2.2204460492503131e-016 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 0 -7.6904137356990004e-037 ;
	setAttr ".bps" -type "matrix" -1 1.0518364965731299e-021 -1.2246467991021825e-016 0
		 1.0518364965644982e-021 0.99999999999999989 7.048330560692484e-017 0 1.2246467991021825e-016 7.048330560692484e-017 -0.99999999999999989 0
		 -0.876494 1.2187300000000043 1.3769800000000005 1;
	setAttr ".radi" 0.025;
	setAttr ".liw" yes;
createNode ikEffector -n "ankleFrontProxySkinBtwEndRGT_eff" -p "ankleFrontProxySkinBtwIkRGT_jnt";
	rename -uid "951D7DD4-447F-E91C-A0D6-7BB3C242C882";
	setAttr ".v" no;
	setAttr ".hd" yes;
createNode joint -n "ankleFrontProxySkinBtwAimRGT_jnt" -p "ankleFrontProxySkinBtwHideRGT_grp";
	rename -uid "7F7F6BC9-4404-BA15-8ECD-15BE1E9D11A0";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" -4.0383959374075651e-015 2.5880886611741528e-025 6.0265791991467016e-020 ;
	setAttr ".bps" -type "matrix" -1 1.0518364965731299e-021 -1.2246467991021825e-016 0
		 1.0518364965644982e-021 0.99999999999999989 7.048330560692484e-017 0 1.2246467991021825e-016 7.048330560692484e-017 -0.99999999999999989 0
		 -0.876494 1.2187300000000043 1.3769800000000005 1;
	setAttr ".radi" 0.075000000000000011;
	setAttr ".liw" yes;
createNode joint -n "ankleFrontProxySkinBtwAimEndRGT_jnt" -p "ankleFrontProxySkinBtwAimRGT_jnt";
	rename -uid "5EC4BCF5-4A31-CE6C-68D5-D38F57C92AA0";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 -0.58450499500362385 0 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 0 -7.6904137356990004e-037 ;
	setAttr ".bps" -type "matrix" -1 1.0518364965731299e-021 -1.2246467991021825e-016 0
		 1.0518364965644982e-021 0.99999999999999989 7.048330560692484e-017 0 1.2246467991021825e-016 7.048330560692484e-017 -0.99999999999999989 0
		 -0.876494 1.2187300000000043 1.3769800000000005 1;
	setAttr ".radi" 0.025;
	setAttr ".liw" yes;
createNode aimConstraint -n "ankleFrontProxySkinBtwAimRGT_jnt_aimConstraint1" -p "ankleFrontProxySkinBtwAimRGT_jnt";
	rename -uid "0EFB3749-44F8-8C00-0A09-17A9EBA59F81";
	addAttr -dcb 0 -ci true -sn "w0" -ln "ballFrontProxySkinRGT_jntW0" -dv 1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".a" -type "double3" 0 -1 0 ;
	setAttr ".u" -type "double3" 1 0 0 ;
	setAttr ".wut" 1;
	setAttr ".rsrr" -type "double3" 19.239356341199425 -9.4110296555344756e-016 -5.552536688334597e-015 ;
	setAttr -k on ".w0";
createNode joint -n "ankleFrontProxySkinBtwVoidRGT_jnt" -p "ankleFrontProxySkinBtwHideRGT_grp";
	rename -uid "B134B632-4418-93A7-46C2-B3AB9DD9B10F";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" -4.0383959374075651e-015 2.5880886611741528e-025 6.0265791991467016e-020 ;
	setAttr ".bps" -type "matrix" -1 1.0518364965731299e-021 -1.2246467991021825e-016 0
		 1.0518364965644982e-021 0.99999999999999989 7.048330560692484e-017 0 1.2246467991021825e-016 7.048330560692484e-017 -0.99999999999999989 0
		 -0.876494 1.2187300000000043 1.3769800000000005 1;
	setAttr ".radi" 0.075000000000000011;
	setAttr ".liw" yes;
createNode joint -n "ankleFrontProxySkinBtwVoidEndRGT_jnt" -p "ankleFrontProxySkinBtwVoidRGT_jnt";
	rename -uid "6363DD56-45AD-5B36-AE06-5ABF0DFAC5B0";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 -0.58450499500362385 0 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 0 -7.6904137356990004e-037 ;
	setAttr ".bps" -type "matrix" -1 1.0518364965731299e-021 -1.2246467991021825e-016 0
		 1.0518364965644982e-021 0.99999999999999989 7.048330560692484e-017 0 1.2246467991021825e-016 7.048330560692484e-017 -0.99999999999999989 0
		 -0.876494 1.2187300000000043 1.3769800000000005 1;
	setAttr ".radi" 0.025;
	setAttr ".liw" yes;
createNode ikHandle -n "ankleFrontProxySkinBtwRGT_ikHndl" -p "ankleFrontProxySkinBtwHideRGT_grp";
	rename -uid "DA525AA6-4DB7-D699-92B9-998FBEB95351";
	setAttr ".r" -type "double3" 0 180.00000000000003 0 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999978 ;
	setAttr ".hs" 1;
	setAttr -l on ".pv" -type "double3" 0 0 0 ;
	setAttr -l on ".pv";
	setAttr ".roc" yes;
createNode pointConstraint -n "ankleFrontProxySkinBtwRGT_ikHndl_pointConstraint1" 
		-p "ankleFrontProxySkinBtwRGT_ikHndl";
	rename -uid "071166C7-434F-1851-6A02-21AF4E396B45";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "ankleFrontProxySkinBtwAimEndRGT_jntW0" 
		-dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "ankleFrontProxySkinBtwVoidEndRGT_jntW1" 
		-dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" -0.876494 0.92647750249819261 1.3769800000000008 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode joint -n "ankleFrontProxySkinBtwRGT_jnt" -p "ankleFrontProxySkinBtwAllMoveRGT_grp";
	rename -uid "F796E29F-4B80-FB3E-FCA5-F4A90D469F74";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "aimBtw" -ln "aimBtw" -dv 0.5 -min 0 -max 1 -at "float";
	addAttr -ci true -sn "scaleBtw" -ln "scaleBtw" -dv 2 -at "float";
	setAttr -l on ".v";
	setAttr -l on ".t";
	setAttr -l on ".r";
	setAttr ".ro" 1;
	setAttr -l on ".s";
	setAttr ".jo" -type "double3" 9.6196781705996184 0 0 ;
	setAttr ".bps" -type "matrix" -1 1.0518364965731299e-021 -1.2246467991021825e-016 0
		 1.0518364965644982e-021 0.99999999999999989 7.048330560692484e-017 0 1.2246467991021825e-016 7.048330560692484e-017 -0.99999999999999989 0
		 -0.876494 1.2187300000000043 1.3769800000000005 1;
	setAttr ".radi" 0.075000000000000011;
	setAttr ".liw" yes;
	setAttr -k on ".aimBtw";
	setAttr -k on ".scaleBtw";
createNode joint -n "ankleFrontProxySkinBtwEndRGT_jnt" -p "ankleFrontProxySkinBtwRGT_jnt";
	rename -uid "7679A5B1-454F-EFBC-2C60-E3B6A8D77668";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -2.2204460492503131e-016 -0.58450499500362407 -2.2204460492503131e-016 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 0 -7.6904137356990004e-037 ;
	setAttr ".bps" -type "matrix" -1 1.0518364965731299e-021 -1.2246467991021825e-016 0
		 1.0518364965644982e-021 0.99999999999999989 7.048330560692484e-017 0 1.2246467991021825e-016 7.048330560692484e-017 -0.99999999999999989 0
		 -0.876494 1.2187300000000043 1.3769800000000005 1;
	setAttr ".radi" 0.025;
	setAttr ".liw" yes;
createNode parentConstraint -n "ankleFrontProxySkinBtwRGT_jnt_parentConstraint1" 
		-p "ankleFrontProxySkinBtwRGT_jnt";
	rename -uid "85608B75-42B9-BCAF-07CD-56B1BBF51ADF";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "ankleFrontProxySkinBtwIkRGT_jntW0" 
		-dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode pointConstraint -n "ankleFrontProxySkinBtwAllRGT_grp_pointConstraint1" 
		-p "ankleFrontProxySkinBtwAllRGT_grp";
	rename -uid "E78477F0-48EC-15D4-5255-D4ADFB55428F";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "ankleFrontProxySkinRGT_jntW0" -dv 
		1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 2.2204460492503131e-016 -0.11762391398010763 2.2204460492503131e-016 ;
	setAttr -k on ".w0";
createNode joint -n "midLegRbnDtl1FrontProxySkinRGT_jnt" -p "midLegFrontProxySkinRGT_jnt";
	rename -uid "100F00BC-4407-70F1-A0C8-E4ADD1C3DB11";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 3.3306690738754696e-016 -0.14598068052607882 0.0024577881969016779 ;
	setAttr ".jo" -type "double3" 0.96456316331213798 180 0 ;
	setAttr ".bps" -type "matrix" 1 -1.0518364965817616e-021 2.4492935982495357e-016 0
		 -4.1220910812164938e-018 0.99985829804994764 0.016834007860929881 0 -2.4489467056366524e-016 -0.016834007860929881 0.99985829804994786 0
		 -0.87649400000000033 3.7813993194739259 1.5399422118030994 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode joint -n "midLegRbnDtl2FrontProxySkinRGT_jnt" -p "midLegFrontProxySkinRGT_jnt";
	rename -uid "FF8C89C6-41C4-1071-2466-208C27EE8802";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 5.5511151231257827e-016 -0.45088655273650957 0.0075913034756616149 ;
	setAttr ".jo" -type "double3" 0.96456316331213798 180 0 ;
	setAttr ".bps" -type "matrix" 1 -1.0518364965817616e-021 2.4492935982495357e-016 0
		 -4.1220910812164938e-018 0.99985829804994764 0.016834007860929881 0 -2.4489467056366524e-016 -0.016834007860929881 0.99985829804994786 0
		 -0.87649400000000055 3.4764934472634952 1.5348086965243395 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode joint -n "midLegRbnDtl3FrontProxySkinRGT_jnt" -p "midLegFrontProxySkinRGT_jnt";
	rename -uid "DEA4ADF5-49E4-A556-6B79-AE8A304EE27B";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 5.5511151231257827e-016 -0.77035500000000212 0.01297000000000148 ;
	setAttr ".jo" -type "double3" 0.96456316331213798 180 0 ;
	setAttr ".bps" -type "matrix" 1 -1.0518364965817616e-021 2.4492935982495357e-016 0
		 -4.1220910812164938e-018 0.99985829804994764 0.016834007860929881 0 -2.4489467056366524e-016 -0.016834007860929881 0.99985829804994786 0
		 -0.87649400000000055 3.1570250000000026 1.5294299999999996 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode joint -n "midLegRbnDtl4FrontProxySkinRGT_jnt" -p "midLegFrontProxySkinRGT_jnt";
	rename -uid "5E71E054-424B-A51D-AADD-00B97A548A9B";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 5.5511151231257827e-016 -1.0898234472634951 0.018348696524340014 ;
	setAttr ".jo" -type "double3" 0.96456316331213798 180 0 ;
	setAttr ".bps" -type "matrix" 1 -1.0518364965817616e-021 2.4492935982495357e-016 0
		 -4.1220910812164938e-018 0.99985829804994764 0.016834007860929881 0 -2.4489467056366524e-016 -0.016834007860929881 0.99985829804994786 0
		 -0.87649400000000055 2.8375565527365092 1.5240513034756606 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode joint -n "midLegRbnDtl5FrontProxySkinRGT_jnt" -p "midLegFrontProxySkinRGT_jnt";
	rename -uid "219098BC-4F15-05D8-9F10-DDB072A5702D";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 5.5511151231257827e-016 -1.394729319473925 0.02348221180309995 ;
	setAttr ".jo" -type "double3" 0.96456316331213798 180 0 ;
	setAttr ".bps" -type "matrix" 1 -1.0518364965817616e-021 2.4492935982495357e-016 0
		 -4.1220910812164938e-018 0.99985829804994764 0.016834007860929881 0 -2.4489467056366524e-016 -0.016834007860929881 0.99985829804994786 0
		 -0.87649400000000055 2.5326506805260793 1.5189177881969007 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode transform -n "lowLegFrontProxySkinBtwAllRGT_grp" -p "midLegRbnDtl5FrontProxySkinRGT_jnt";
	rename -uid "1C899FFF-43D9-2E17-BB5E-33949F0579A4";
	setAttr ".r" -type "double3" 0.96456316331213798 180 0 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
createNode transform -n "lowLegFrontProxySkinBtwAllMoveRGT_grp" -p "lowLegFrontProxySkinBtwAllRGT_grp";
	rename -uid "640DBE72-43F9-2DCC-EB0F-2388A89CB9B8";
	setAttr ".t" -type "double3" 0 0.039286387269356446 0 ;
createNode transform -n "lowLegFrontProxySkinBtwHideRGT_grp" -p "lowLegFrontProxySkinBtwAllMoveRGT_grp";
	rename -uid "8002EA03-4F07-B869-AC1A-7C9C1BD991FF";
	setAttr ".v" no;
createNode transform -n "lowLegFrontProxySkinBtwUpRGT_grp" -p "lowLegFrontProxySkinBtwHideRGT_grp";
	rename -uid "6F871413-4035-CA39-8AE9-4A983C1CC328";
createNode joint -n "lowLegFrontProxySkinBtwIkRGT_jnt" -p "lowLegFrontProxySkinBtwHideRGT_grp";
	rename -uid "32E16A02-43BB-2A21-24BD-E3A4863A0CE0";
	setAttr ".t" -type "double3" -1.1102230246251565e-016 0 -2.2204460492503131e-016 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" -2.5533134702987605 0 0 ;
	setAttr ".bps" -type "matrix" -1 1.0518364965731299e-021 -1.2246467991021825e-016 0
		 1.0518364965644982e-021 0.99999999999999989 7.048330560692484e-017 0 1.2246467991021825e-016 7.048330560692484e-017 -0.99999999999999989 0
		 -0.876494 2.3866700000000041 1.5164599999999999 1;
	setAttr ".radi" 0.075000000000000011;
createNode joint -n "lowLegFrontProxySkinBtwEndIkRGT_jnt" -p "lowLegFrontProxySkinBtwIkRGT_jnt";
	rename -uid "6F8F55EE-44EF-B3F6-ADA3-ACBE423773E7";
	setAttr ".t" -type "double3" 2.2204460492503131e-016 -0.078572774538712142 4.4408920985006262e-016 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 0 -7.6904137356990004e-037 ;
	setAttr ".bps" -type "matrix" -1 1.0518364965731299e-021 -1.2246467991021825e-016 0
		 1.0518364965644982e-021 0.99999999999999989 7.048330560692484e-017 0 1.2246467991021825e-016 7.048330560692484e-017 -0.99999999999999989 0
		 -0.876494 2.3866700000000041 1.5164599999999999 1;
	setAttr ".radi" 0.025;
createNode ikEffector -n "lowLegFrontProxySkinBtwEndRGT_eff" -p "lowLegFrontProxySkinBtwIkRGT_jnt";
	rename -uid "E5BA1F66-4586-2F9D-77C3-56877BEFAFC8";
	setAttr ".v" no;
	setAttr ".hd" yes;
createNode joint -n "lowLegFrontProxySkinBtwAimRGT_jnt" -p "lowLegFrontProxySkinBtwHideRGT_grp";
	rename -uid "1F41F2E9-4ABE-3E70-1046-F5A1462F1029";
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" -4.0383959374075651e-015 2.5880886611741528e-025 6.0265791991467016e-020 ;
	setAttr ".bps" -type "matrix" -1 1.0518364965731299e-021 -1.2246467991021825e-016 0
		 1.0518364965644982e-021 0.99999999999999989 7.048330560692484e-017 0 1.2246467991021825e-016 7.048330560692484e-017 -0.99999999999999989 0
		 -0.876494 2.3866700000000041 1.5164599999999999 1;
	setAttr ".radi" 0.075000000000000011;
createNode joint -n "lowLegFrontProxySkinBtwAimEndRGT_jnt" -p "lowLegFrontProxySkinBtwAimRGT_jnt";
	rename -uid "F25D8781-483E-0F28-0E14-B7BD01FD6155";
	setAttr ".t" -type "double3" 0 -0.078572774538712586 0 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 0 -7.6904137356990004e-037 ;
	setAttr ".bps" -type "matrix" -1 1.0518364965731299e-021 -1.2246467991021825e-016 0
		 1.0518364965644982e-021 0.99999999999999989 7.048330560692484e-017 0 1.2246467991021825e-016 7.048330560692484e-017 -0.99999999999999989 0
		 -0.876494 2.3866700000000041 1.5164599999999999 1;
	setAttr ".radi" 0.025;
createNode aimConstraint -n "lowLegFrontProxySkinBtwAimRGT_jnt_aimConstraint1" -p
		 "lowLegFrontProxySkinBtwAimRGT_jnt";
	rename -uid "C82CD7C1-464D-8238-1108-11B4D1334F30";
	addAttr -dcb 0 -ci true -sn "w0" -ln "lowLegRbnDtl1FrontProxySkinRGT_jntW0" -dv 
		1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".a" -type "double3" 0 -1 0 ;
	setAttr ".u" -type "double3" 1 0 0 ;
	setAttr ".wut" 1;
	setAttr ".rsrr" -type "double3" -5.1066269405980291 -1.8101993972544184e-015 4.0593573687127624e-014 ;
	setAttr -k on ".w0";
createNode joint -n "lowLegFrontProxySkinBtwVoidRGT_jnt" -p "lowLegFrontProxySkinBtwHideRGT_grp";
	rename -uid "A3AB6009-47CD-AA7F-C715-F0BA074C4F42";
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" -4.0383959374075651e-015 2.5880886611741528e-025 6.0265791991467016e-020 ;
	setAttr ".bps" -type "matrix" -1 1.0518364965731299e-021 -1.2246467991021825e-016 0
		 1.0518364965644982e-021 0.99999999999999989 7.048330560692484e-017 0 1.2246467991021825e-016 7.048330560692484e-017 -0.99999999999999989 0
		 -0.876494 2.3866700000000041 1.5164599999999999 1;
	setAttr ".radi" 0.075000000000000011;
createNode joint -n "lowLegFrontProxySkinBtwVoidEndRGT_jnt" -p "lowLegFrontProxySkinBtwVoidRGT_jnt";
	rename -uid "6716B771-4EFC-64C9-2D6F-469E2247A7B9";
	setAttr ".t" -type "double3" 0 -0.078572774538712586 0 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 0 -7.6904137356990004e-037 ;
	setAttr ".bps" -type "matrix" -1 1.0518364965731299e-021 -1.2246467991021825e-016 0
		 1.0518364965644982e-021 0.99999999999999989 7.048330560692484e-017 0 1.2246467991021825e-016 7.048330560692484e-017 -0.99999999999999989 0
		 -0.876494 2.3866700000000041 1.5164599999999999 1;
	setAttr ".radi" 0.025;
createNode ikHandle -n "lowLegFrontProxySkinBtwRGT_ikHndl" -p "lowLegFrontProxySkinBtwHideRGT_grp";
	rename -uid "CE9A31C3-42CE-FB73-3C13-43998B7E4F8D";
	setAttr ".r" -type "double3" 0 180.00000000000003 0 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 0.99999999999999989 ;
	setAttr ".hs" 1;
	setAttr -l on ".pv" -type "double3" 0 0 0 ;
	setAttr -l on ".pv";
	setAttr ".roc" yes;
createNode pointConstraint -n "lowLegFrontProxySkinBtwRGT_ikHndl_pointConstraint1" 
		-p "lowLegFrontProxySkinBtwRGT_ikHndl";
	rename -uid "815B3DBA-416C-C9FB-E4BB-8B82B0404F05";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "lowLegFrontProxySkinBtwAimEndRGT_jntW0" 
		-dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "lowLegFrontProxySkinBtwVoidEndRGT_jntW1" 
		-dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" -0.876494 2.3473836127306478 1.5164600000000001 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode joint -n "lowLegFrontProxySkinBtwRGT_jnt" -p "lowLegFrontProxySkinBtwAllMoveRGT_grp";
	rename -uid "D7DB64DE-4C86-B27C-9AEE-44924F87B21D";
	addAttr -ci true -sn "aimBtw" -ln "aimBtw" -dv 0.5 -min 0 -max 1 -at "float";
	addAttr -ci true -sn "scaleBtw" -ln "scaleBtw" -dv 2 -at "float";
	setAttr -l on ".v";
	setAttr -l on ".t";
	setAttr -l on ".r";
	setAttr ".ro" 1;
	setAttr -l on ".s";
	setAttr ".jo" -type "double3" -2.5533134702987623 0 0 ;
	setAttr ".bps" -type "matrix" -1 1.0518364965731299e-021 -1.2246467991021825e-016 0
		 1.0518364965644982e-021 0.99999999999999989 7.048330560692484e-017 0 1.2246467991021825e-016 7.048330560692484e-017 -0.99999999999999989 0
		 -0.876494 2.3866700000000041 1.5164599999999999 1;
	setAttr ".radi" 0.075000000000000011;
	setAttr -k on ".aimBtw";
	setAttr -k on ".scaleBtw";
createNode joint -n "lowLegFrontProxySkinBtwEndRGT_jnt" -p "lowLegFrontProxySkinBtwRGT_jnt";
	rename -uid "D073822C-4DA0-9169-9115-05AA1889BA09";
	setAttr ".t" -type "double3" 2.2204460492503131e-016 -0.078572774538712142 4.4408920985006262e-016 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 0 -7.6904137356990004e-037 ;
	setAttr ".bps" -type "matrix" -1 1.0518364965731299e-021 -1.2246467991021825e-016 0
		 1.0518364965644982e-021 0.99999999999999989 7.048330560692484e-017 0 1.2246467991021825e-016 7.048330560692484e-017 -0.99999999999999989 0
		 -0.876494 2.3866700000000041 1.5164599999999999 1;
	setAttr ".radi" 0.025;
createNode parentConstraint -n "lowLegFrontProxySkinBtwRGT_jnt_parentConstraint1" 
		-p "lowLegFrontProxySkinBtwRGT_jnt";
	rename -uid "03F27EE8-46B9-4F1E-2680-9CB65C16C0A2";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "lowLegFrontProxySkinBtwIkRGT_jntW0" 
		-dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 1.5902773407317584e-015 0 0 ;
	setAttr ".rst" -type "double3" -2.2204460492503131e-016 0 -4.4408920985006262e-016 ;
	setAttr ".rsrr" -type "double3" 1.5902773407317584e-015 0 0 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "lowLegFrontProxySkinBtwAllRGT_grp_pointConstraint1" 
		-p "lowLegFrontProxySkinBtwAllRGT_grp";
	rename -uid "8BC325BE-47F4-843D-85C2-FAAD0CEED526";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "lowLegFrontProxySkinRGT_jntW0" -dv 
		1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 5.5511151231257827e-016 -0.14600136920480189 -4.4408920985006262e-016 ;
	setAttr -k on ".w0";
createNode joint -n "upLegRbnDtl1FrontProxySkinRGT_jnt" -p "upLegFrontProxySkinRGT_jnt";
	rename -uid "196DE080-4231-193E-A8B2-6BAED3635E1E";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 -0.11398336490894767 0.060159616828014961 ;
	setAttr ".jo" -type "double3" 27.824776456419499 180 0 ;
	setAttr ".bps" -type "matrix" 1 -1.0518364965817616e-021 2.4492935982495357e-016 0
		 -1.143245307240864e-016 0.88437921259395902 0.46676911672869797 0 -2.1661092534792741e-016 -0.46676911672869797 0.88437921259395924 0
		 -0.876494 5.3875366350910561 2.3130603831719849 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode joint -n "upLegRbnDtl2FrontProxySkinRGT_jnt" -p "upLegFrontProxySkinRGT_jnt";
	rename -uid "99DCCE56-4AA0-E9D8-B713-3B8C521C2CD1";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 2.2204460492503131e-016 -0.4114391108725286 0.21715466355922164 ;
	setAttr ".jo" -type "double3" 27.824776456419499 180 0 ;
	setAttr ".bps" -type "matrix" 1 -1.0518364965817616e-021 2.4492935982495357e-016 0
		 -1.143245307240864e-016 0.88437921259395902 0.46676911672869797 0 -2.1661092534792741e-016 -0.46676911672869797 0.88437921259395924 0
		 -0.87649400000000022 5.0900808891274751 2.1560653364407782 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode joint -n "upLegRbnDtl3FrontProxySkinRGT_jnt" -p "upLegFrontProxySkinRGT_jnt";
	rename -uid "1277EAB9-472E-04E9-D080-2A88996E64B9";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 4.4408920985006262e-016 -0.78706999999999905 0.41540999999999961 ;
	setAttr ".jo" -type "double3" 27.824776456419499 180 0 ;
	setAttr ".bps" -type "matrix" 1 -1.0518364965817616e-021 2.4492935982495357e-016 0
		 -1.143245307240864e-016 0.88437921259395902 0.46676911672869797 0 -2.1661092534792741e-016 -0.46676911672869797 0.88437921259395924 0
		 -0.87649400000000044 4.7144500000000047 1.9578099999999998 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode joint -n "upLegRbnDtl4FrontProxySkinRGT_jnt" -p "upLegFrontProxySkinRGT_jnt";
	rename -uid "14220624-4A2C-6C6F-C1DB-BE83181FA972";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 3.3306690738754696e-016 -1.1627008891274704 0.61366533644077714 ;
	setAttr ".jo" -type "double3" 27.824776456419499 180 0 ;
	setAttr ".bps" -type "matrix" 1 -1.0518364965817616e-021 2.4492935982495357e-016 0
		 -1.143245307240864e-016 0.88437921259395902 0.46676911672869797 0 -2.1661092534792741e-016 -0.46676911672869797 0.88437921259395924 0
		 -0.87649400000000022 4.3388191108725334 1.7595546635592223 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode joint -n "upLegRbnDtl5FrontProxySkinRGT_jnt" -p "upLegFrontProxySkinRGT_jnt";
	rename -uid "D52DD250-440F-43D4-E8ED-0294962DD4B4";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 2.2204460492503131e-016 -1.4601566350910504 0.77066038317198204 ;
	setAttr ".jo" -type "double3" 27.824776456419499 180 0 ;
	setAttr ".bps" -type "matrix" 1 -1.0518364965817616e-021 2.4492935982495357e-016 0
		 -1.143245307240864e-016 0.88437921259395902 0.46676911672869797 0 -2.1661092534792741e-016 -0.46676911672869797 0.88437921259395924 0
		 -0.87649400000000011 4.0413633649089533 1.6025596168280174 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode transform -n "midLegFrontProxySkinBtwAllRGT_grp" -p "upLegRbnDtl5FrontProxySkinRGT_jnt";
	rename -uid "176190BF-4452-D54E-46EF-E881BEB494C7";
	setAttr ".t" -type "double3" 2.2204460492503131e-016 -0.12888516971653718 -8.3266726846886741e-016 ;
	setAttr ".r" -type "double3" 27.824776456419499 180 0 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
createNode transform -n "midLegFrontProxySkinBtwAllMoveRGT_grp" -p "midLegFrontProxySkinBtwAllRGT_grp";
	rename -uid "B5534620-459A-74F8-B946-25967B8ED332";
	setAttr ".t" -type "double3" 0 0.048764457314405027 0 ;
createNode transform -n "midLegFrontProxySkinBtwHideRGT_grp" -p "midLegFrontProxySkinBtwAllMoveRGT_grp";
	rename -uid "80333A13-4E6A-8179-687D-1F92BAC2736D";
	setAttr ".v" no;
createNode transform -n "midLegFrontProxySkinBtwUpRGT_grp" -p "midLegFrontProxySkinBtwHideRGT_grp";
	rename -uid "00D3E8E4-4C27-9CF0-4115-0D9C17288DF4";
createNode joint -n "midLegFrontProxySkinBtwIkRGT_jnt" -p "midLegFrontProxySkinBtwHideRGT_grp";
	rename -uid "6CE17151-4861-A213-47F9-86B0D0B03ED8";
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" -0.3615325556810578 0 0 ;
	setAttr ".bps" -type "matrix" -1 1.0518364965731299e-021 -1.2246467991021825e-016 0
		 1.0518364965644982e-021 0.99999999999999989 7.048330560692484e-017 0 1.2246467991021825e-016 7.048330560692484e-017 -0.99999999999999989 0
		 -0.876494 3.9273800000000039 1.5424000000000002 1;
	setAttr ".radi" 0.075000000000000011;
createNode joint -n "midLegFrontProxySkinBtwEndIkRGT_jnt" -p "midLegFrontProxySkinBtwIkRGT_jnt";
	rename -uid "DA9FB654-4727-7566-72D2-10845C458708";
	setAttr ".t" -type "double3" 0 -0.097528914628809638 -2.2204460492503131e-016 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 0 -7.6904137356990004e-037 ;
	setAttr ".bps" -type "matrix" -1 1.0518364965731299e-021 -1.2246467991021825e-016 0
		 1.0518364965644982e-021 0.99999999999999989 7.048330560692484e-017 0 1.2246467991021825e-016 7.048330560692484e-017 -0.99999999999999989 0
		 -0.876494 3.9273800000000039 1.5424000000000002 1;
	setAttr ".radi" 0.025;
createNode ikEffector -n "midLegFrontProxySkinBtwEndRGT_eff" -p "midLegFrontProxySkinBtwIkRGT_jnt";
	rename -uid "E32D1341-48A1-3E62-3ED5-69AAE5EA694A";
	setAttr ".v" no;
	setAttr ".hd" yes;
createNode joint -n "midLegFrontProxySkinBtwAimRGT_jnt" -p "midLegFrontProxySkinBtwHideRGT_grp";
	rename -uid "78E688A0-4C7D-AB1B-9AE8-628F19EB5485";
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" -4.0383959374075651e-015 2.5880886611741528e-025 6.0265791991467016e-020 ;
	setAttr ".bps" -type "matrix" -1 1.0518364965731299e-021 -1.2246467991021825e-016 0
		 1.0518364965644982e-021 0.99999999999999989 7.048330560692484e-017 0 1.2246467991021825e-016 7.048330560692484e-017 -0.99999999999999989 0
		 -0.876494 3.9273800000000039 1.5424000000000002 1;
	setAttr ".radi" 0.075000000000000011;
createNode joint -n "midLegFrontProxySkinBtwAimEndRGT_jnt" -p "midLegFrontProxySkinBtwAimRGT_jnt";
	rename -uid "2786955D-4C74-5DD0-A10B-94B0BB69292D";
	setAttr ".t" -type "double3" 0 -0.097528914628809638 2.2204460492503131e-016 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 0 -7.6904137356990004e-037 ;
	setAttr ".bps" -type "matrix" -1 1.0518364965731299e-021 -1.2246467991021825e-016 0
		 1.0518364965644982e-021 0.99999999999999989 7.048330560692484e-017 0 1.2246467991021825e-016 7.048330560692484e-017 -0.99999999999999989 0
		 -0.876494 3.9273800000000039 1.5424000000000002 1;
	setAttr ".radi" 0.025;
createNode aimConstraint -n "midLegFrontProxySkinBtwAimRGT_jnt_aimConstraint1" -p
		 "midLegFrontProxySkinBtwAimRGT_jnt";
	rename -uid "447D2D3D-496A-CDF2-8588-C98D170A3E65";
	addAttr -dcb 0 -ci true -sn "w0" -ln "midLegRbnDtl1FrontProxySkinRGT_jntW0" -dv 
		1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".a" -type "double3" 0 -1 0 ;
	setAttr ".u" -type "double3" 1 0 0 ;
	setAttr ".wut" 1;
	setAttr ".rsrr" -type "double3" -0.72306511136069496 -8.2437001879284966e-016 1.3064465489062163e-013 ;
	setAttr -k on ".w0";
createNode joint -n "midLegFrontProxySkinBtwVoidRGT_jnt" -p "midLegFrontProxySkinBtwHideRGT_grp";
	rename -uid "5D1A061C-4F43-1A3D-16D1-F08A8D20DE54";
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" -4.0383959374075651e-015 2.5880886611741528e-025 6.0265791991467016e-020 ;
	setAttr ".bps" -type "matrix" -1 1.0518364965731299e-021 -1.2246467991021825e-016 0
		 1.0518364965644982e-021 0.99999999999999989 7.048330560692484e-017 0 1.2246467991021825e-016 7.048330560692484e-017 -0.99999999999999989 0
		 -0.876494 3.9273800000000039 1.5424000000000002 1;
	setAttr ".radi" 0.075000000000000011;
createNode joint -n "midLegFrontProxySkinBtwVoidEndRGT_jnt" -p "midLegFrontProxySkinBtwVoidRGT_jnt";
	rename -uid "454F2B7C-4BAC-BED7-D696-4999623CDA8F";
	setAttr ".t" -type "double3" 0 -0.097528914628809638 2.2204460492503131e-016 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 0 -7.6904137356990004e-037 ;
	setAttr ".bps" -type "matrix" -1 1.0518364965731299e-021 -1.2246467991021825e-016 0
		 1.0518364965644982e-021 0.99999999999999989 7.048330560692484e-017 0 1.2246467991021825e-016 7.048330560692484e-017 -0.99999999999999989 0
		 -0.876494 3.9273800000000039 1.5424000000000002 1;
	setAttr ".radi" 0.025;
createNode ikHandle -n "midLegFrontProxySkinBtwRGT_ikHndl" -p "midLegFrontProxySkinBtwHideRGT_grp";
	rename -uid "6EFDE552-4BE3-060A-98A1-5F884E16B5A2";
	setAttr ".r" -type "double3" 0 180.00000000000003 0 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999978 ;
	setAttr ".hs" 1;
	setAttr -l on ".pv" -type "double3" 0 0 0 ;
	setAttr -l on ".pv";
	setAttr ".roc" yes;
createNode pointConstraint -n "midLegFrontProxySkinBtwRGT_ikHndl_pointConstraint1" 
		-p "midLegFrontProxySkinBtwRGT_ikHndl";
	rename -uid "BA029F7F-40F1-E54F-D707-D38E86C76EAD";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "midLegFrontProxySkinBtwAimEndRGT_jntW0" 
		-dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "midLegFrontProxySkinBtwVoidEndRGT_jntW1" 
		-dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" -0.87649399999999988 3.878615542685599 1.5424000000000004 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode joint -n "midLegFrontProxySkinBtwRGT_jnt" -p "midLegFrontProxySkinBtwAllMoveRGT_grp";
	rename -uid "087A170B-49BE-7012-514D-3798D38DDFFF";
	addAttr -ci true -sn "aimBtw" -ln "aimBtw" -dv 0.5 -min 0 -max 1 -at "float";
	addAttr -ci true -sn "scaleBtw" -ln "scaleBtw" -dv 2 -at "float";
	setAttr -l on ".v";
	setAttr -l on ".t";
	setAttr -l on ".r";
	setAttr ".ro" 1;
	setAttr -l on ".s";
	setAttr ".jo" -type "double3" -0.3615325556810578 0 0 ;
	setAttr ".bps" -type "matrix" -1 1.0518364965731299e-021 -1.2246467991021825e-016 0
		 1.0518364965644982e-021 0.99999999999999989 7.048330560692484e-017 0 1.2246467991021825e-016 7.048330560692484e-017 -0.99999999999999989 0
		 -0.876494 3.9273800000000039 1.5424000000000002 1;
	setAttr ".radi" 0.075000000000000011;
	setAttr -k on ".aimBtw";
	setAttr -k on ".scaleBtw";
createNode joint -n "midLegFrontProxySkinBtwEndRGT_jnt" -p "midLegFrontProxySkinBtwRGT_jnt";
	rename -uid "0BB5BB47-4932-A8E7-E6C5-90BB860E7E3D";
	setAttr ".t" -type "double3" 0 -0.097528914628809638 -2.2204460492503131e-016 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 0 -7.6904137356990004e-037 ;
	setAttr ".bps" -type "matrix" -1 1.0518364965731299e-021 -1.2246467991021825e-016 0
		 1.0518364965644982e-021 0.99999999999999989 7.048330560692484e-017 0 1.2246467991021825e-016 7.048330560692484e-017 -0.99999999999999989 0
		 -0.876494 3.9273800000000039 1.5424000000000002 1;
	setAttr ".radi" 0.025;
createNode parentConstraint -n "midLegFrontProxySkinBtwRGT_jnt_parentConstraint1" 
		-p "midLegFrontProxySkinBtwRGT_jnt";
	rename -uid "7EEB272E-4528-439D-5B4E-C485B53A32AB";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "midLegFrontProxySkinBtwIkRGT_jntW0" 
		-dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode joint -n "clav1ScaScapProxySkinRGT_jnt" -p "clav1ScapProxySkinRGT_jnt";
	rename -uid "1381C7E1-47C8-373D-D409-86A68EF51B7E";
	setAttr ".t" -type "double3" 0 -1.7763568394002505e-015 -2.2204460492503131e-016 ;
	setAttr ".s" -type "double3" 1 1 1.0000000000000002 ;
	setAttr ".radi" 0.1;
createNode joint -n "upSpineRbnProxySkin_jnt" -p "spine2ProxySkin_jnt";
	rename -uid "A45BF50F-4059-D0DD-D8A3-36BD1E340D51";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 3.7175931899494421e-017 0.16806219849809523 0.64310660225181293 ;
	setAttr ".r" -type "double3" -6.361109362927032e-015 0 0 ;
	setAttr ".jo" -type "double3" -14.645500391792886 0 0 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 0.9675108606695132 -0.25282945731567291 0
		 0 0.25282945731567291 0.9675108606695132 0 3.7175931899494421e-017 5.68307287700123 1.4649200025156954 1;
	setAttr ".radi" 0.05;
	setAttr ".liw" yes;
createNode joint -n "lowSpineRbnProxySkin_jnt" -p "spine1ProxySkin_jnt";
	rename -uid "36AA3253-4B1D-55D2-7D3F-3BBD1D192934";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 0.05523277231170276 0.59746813422098299 ;
	setAttr ".r" -type "double3" -2.3854160110976376e-015 0 0 ;
	setAttr ".jo" -type "double3" -5.281680439913913 0 0 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 0.99575418154821393 -0.092052213060016083 0
		 0 0.092052213060016083 0.99575418154821393 0 0 5.4597834297738599 0.22434382259774277 1;
	setAttr ".radi" 0.05;
	setAttr ".liw" yes;
createNode joint -n "pelvisProxySkin_jnt" -p "rootProxySkin_jnt";
	rename -uid "E4A567E4-47D1-DC88-8A55-D49EDA96FC9B";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 0.34561912555040131 -0.53161781510093764 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 5.3977858807510044 -0.5114751704424757 1;
	setAttr ".radi" 0.05;
	setAttr ".liw" yes;
createNode joint -n "pelvisScaProxySkin_jnt" -p "pelvisProxySkin_jnt";
	rename -uid "25994066-4E35-18E0-77DA-BFB15698EA8F";
	setAttr ".radi" 0.1;
createNode joint -n "clav1HipProxySkinLFT_jnt" -p "pelvisProxySkin_jnt";
	rename -uid "70ADAE54-481A-6EC3-85DB-36B35F60C96F";
	setAttr ".t" -type "double3" 0.54736376329656067 0.24592695606537696 -1.1978253804296024 ;
	setAttr ".jo" -type "double3" 0 0 179.99999999999994 ;
	setAttr ".bps" -type "matrix" -1 1.0106430996148606e-015 0 0 -1.0106430996148606e-015 -1 0 0
		 0 0 1 0 0.54736376329656067 5.6437128368163814 -1.709300550872078 1;
	setAttr ".radi" 0.05;
createNode joint -n "clav2HipProxySkinLFT_jnt" -p "clav1HipProxySkinLFT_jnt";
	rename -uid "9C6D6164-4875-780F-6A51-50B4531A9B0B";
	setAttr ".t" -type "double3" -0.17585051648345262 0.34345023030096833 -0.82516464891673458 ;
	setAttr ".bps" -type "matrix" -1 1.0106430996148606e-015 0 0 -1.0106430996148606e-015 -1 0 0
		 0 0 1 0 0.86161100828807746 5.300262606515413 -2.5344651997888126 1;
	setAttr ".radi" 0.05;
createNode joint -n "upLegBackProxySkinLFT_jnt" -p "clav2HipProxySkinLFT_jnt";
	rename -uid "AF2F6BC1-4D17-D049-33DE-CAA2A5B74F73";
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999164810399821 -0.00085875358477135685 0.0039957808410960374 0
		 0.0008578787839038784 -0.99999960768092577 -0.00022063994786323578 0 0.0039959687488211691 -0.00021721020949263151 0.99999199249468163 0
		 0.86161100828807746 5.300262606515413 -2.5344651997888126 1;
	setAttr ".radi" 0.05;
createNode joint -n "midLegBackProxySkinLFT_jnt" -p "upLegBackProxySkinLFT_jnt";
	rename -uid "08EEA101-47B9-6923-4B16-B9A8E81FACDD";
	setAttr ".t" -type "double3" 0 1.3681717115483503 0.27092214051060326 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999164810399821 -0.00085875358477135685 0.0039957808410960374 0
		 0.0008578787839038784 -0.99999960768092577 -0.00022063994786323578 0 0.0039959687488211691 -0.00021721020949263151 0.99999199249468163 0
		 0.72547175754601534 3.9319137359853009 -2.2632940990275512 1;
	setAttr ".radi" 0.05;
createNode joint -n "lowLegBackProxySkinLFT_jnt" -p "midLegBackProxySkinLFT_jnt";
	rename -uid "9ADD1E73-43D6-0380-6C01-4294B44CA2D4";
	setAttr ".t" -type "double3" -2.6645352591003757e-015 1.1578796777657709 -0.81351941924787408 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999164810399821 -0.00085875358477135685 0.0039957808410960374 0
		 0.0008578787839038784 -0.99999960768092577 -0.00022063994786323578 0 0.0039959687488211691 -0.00021721020949263151 0.99999199249468163 0
		 0.72321427978001307 2.7742112172012918 -3.0770624785260807 1;
	setAttr ".radi" 0.05;
createNode joint -n "ankleBackProxySkinLFT_jnt" -p "lowLegBackProxySkinLFT_jnt";
	rename -uid "A33B27EB-4950-37CC-9D2C-A8BCF0C634CD";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -1.6653345369377348e-015 1.9066778626801524 -0.52965109730339988 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999164810399821 -0.00085875358477135685 0.0039957808410960374 0
		 0.0008578787839038784 -0.99999960768092577 -0.00022063994786323578 0 0.0039959687488211691 -0.00021721020949263151 0.99999199249468163 0
		 0.72273350903354416 0.86764914817303662 -3.6071300239497184 1;
	setAttr ".radi" 0.05;
	setAttr ".liw" yes;
createNode joint -n "ballBackProxySkinLFT_jnt" -p "ankleBackProxySkinLFT_jnt";
	rename -uid "ED74E8F2-40E3-53AB-AA96-6E8BA4E0AFD4";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -1.4432899320127035e-015 0.48854008456475351 0.16068004365228505 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 0 -180 ;
	setAttr ".bps" -type "matrix" 0.99999981145095374 -0.00024635605073557317 0.00056250044770701085 0
		 0.00024643732485707203 0.9999999592053963 -0.00014442247344810747 0 -0.00056246484540983278 0.00014456106732295158 0.99999983136768356 0
		 0.72379468964017357 0.37907435392593442 -3.4465590584025247 1;
	setAttr ".radi" 0.05;
	setAttr ".liw" yes;
createNode joint -n "toeBackProxySkinLFT_jnt" -p "ballBackProxySkinLFT_jnt";
	rename -uid "51F60445-4C42-C789-5D99-95AE77F64CCF";
	setAttr ".t" -type "double3" 8.8817841970012523e-016 -0.37899326995638744 0.86529154454450907 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 89.999999999999972 0 -180 ;
	setAttr ".radi" 0.05;
createNode transform -n "ballBackProxySkinBtwAllLFT_grp" -p "ankleBackProxySkinLFT_jnt";
	rename -uid "C2377554-4475-9951-3EDC-3B8085D6A503";
	setAttr ".r" -type "double3" 0 0 -179.99999999999994 ;
createNode transform -n "ballBackProxySkinBtwAllMoveLFT_grp" -p "ballBackProxySkinBtwAllLFT_grp";
	rename -uid "6FCBF80C-43D2-EF9E-1B9E-D0B77BDEE178";
	setAttr ".t" -type "double3" 0 0.31551340640944248 0 ;
createNode transform -n "ballBackProxySkinBtwHideLFT_grp" -p "ballBackProxySkinBtwAllMoveLFT_grp";
	rename -uid "8BFAC488-48B9-41C8-81A1-91B6B3449F5A";
	setAttr ".v" no;
createNode transform -n "ballBackProxySkinBtwUpLFT_grp" -p "ballBackProxySkinBtwHideLFT_grp";
	rename -uid "300B3163-440A-3698-7CC6-3FB13CB646A1";
createNode joint -n "ballBackProxySkinBtwIkLFT_jnt" -p "ballBackProxySkinBtwHideLFT_grp";
	rename -uid "0174EE71-42B1-7A29-7CCB-5A992C47C3B4";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" -25.624259173270001 0 0 ;
	setAttr ".bps" -type "matrix" 0.99999981145095374 -0.00024635605073557317 0.00056250044770701085 0
		 0.00024643732485707203 0.9999999592053963 -0.00014442247344810747 0 -0.00056246484540983278 0.00014456106732295158 0.99999983136768356 0
		 0.72379468964017357 0.37907435392593442 -3.4465590584025247 1;
	setAttr ".radi" 0.075000000000000011;
	setAttr ".liw" yes;
createNode joint -n "ballBackProxySkinBtwEndIkLFT_jnt" -p "ballBackProxySkinBtwIkLFT_jnt";
	rename -uid "D0AE3247-4266-F6AA-E1A8-21BBA66A157C";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 1.1102230246251565e-016 -0.63102681281888473 -4.4408920985006262e-016 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 0 -1.1299600123008406e-029 ;
	setAttr ".bps" -type "matrix" 0.99999981145095374 -0.00024635605073557317 0.00056250044770701085 0
		 0.00024643732485707203 0.9999999592053963 -0.00014442247344810747 0 -0.00056246484540983278 0.00014456106732295158 0.99999983136768356 0
		 0.72379468964017357 0.37907435392593442 -3.4465590584025247 1;
	setAttr ".radi" 0.025;
	setAttr ".liw" yes;
createNode ikEffector -n "ballBackProxySkinBtwEndLFT_eff" -p "ballBackProxySkinBtwIkLFT_jnt";
	rename -uid "7E956DD7-45C4-6530-2BD5-A89A8B5D9594";
	setAttr ".v" no;
	setAttr ".hd" yes;
createNode joint -n "ballBackProxySkinBtwAimLFT_jnt" -p "ballBackProxySkinBtwHideLFT_grp";
	rename -uid "9BEAFA58-4967-76A4-5C09-DCAD7DCD48E2";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 0 -5.0888874903416281e-014 ;
	setAttr ".bps" -type "matrix" 0.99999981145095374 -0.00024635605073557317 0.00056250044770701085 0
		 0.00024643732485707203 0.9999999592053963 -0.00014442247344810747 0 -0.00056246484540983278 0.00014456106732295158 0.99999983136768356 0
		 0.72379468964017357 0.37907435392593442 -3.4465590584025247 1;
	setAttr ".radi" 0.075000000000000011;
	setAttr ".liw" yes;
createNode joint -n "ballBackProxySkinBtwAimEndLFT_jnt" -p "ballBackProxySkinBtwAimLFT_jnt";
	rename -uid "848180B2-48BF-F917-0B3C-1A9F15D26BE3";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 -0.63102681281888473 0 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 0 -1.1299600123008406e-029 ;
	setAttr ".bps" -type "matrix" 0.99999981145095374 -0.00024635605073557317 0.00056250044770701085 0
		 0.00024643732485707203 0.9999999592053963 -0.00014442247344810747 0 -0.00056246484540983278 0.00014456106732295158 0.99999983136768356 0
		 0.72379468964017357 0.37907435392593442 -3.4465590584025247 1;
	setAttr ".radi" 0.025;
	setAttr ".liw" yes;
createNode aimConstraint -n "ballBackProxySkinBtwAimLFT_jnt_aimConstraint1" -p "ballBackProxySkinBtwAimLFT_jnt";
	rename -uid "4818E5B0-42C0-9656-5AD4-F383DE7B70BD";
	addAttr -dcb 0 -ci true -sn "w0" -ln "toeBackProxySkinLFT_jntW0" -dv 1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".a" -type "double3" 0 -1 0 ;
	setAttr ".u" -type "double3" 1 0 0 ;
	setAttr ".wut" 1;
	setAttr ".rsrr" -type "double3" -51.248518346539967 -1.0999360534816672e-014 2.2932513577659691e-014 ;
	setAttr -k on ".w0";
createNode joint -n "ballBackProxySkinBtwVoidLFT_jnt" -p "ballBackProxySkinBtwHideLFT_grp";
	rename -uid "C02BC52D-47D3-FA9A-93E7-9F94660C41A9";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 0 -5.0888874903416281e-014 ;
	setAttr ".bps" -type "matrix" 0.99999981145095374 -0.00024635605073557317 0.00056250044770701085 0
		 0.00024643732485707203 0.9999999592053963 -0.00014442247344810747 0 -0.00056246484540983278 0.00014456106732295158 0.99999983136768356 0
		 0.72379468964017357 0.37907435392593442 -3.4465590584025247 1;
	setAttr ".radi" 0.075000000000000011;
	setAttr ".liw" yes;
createNode joint -n "ballBackProxySkinBtwVoidEndLFT_jnt" -p "ballBackProxySkinBtwVoidLFT_jnt";
	rename -uid "55102B77-4E3A-C34A-78E6-258A500AAC14";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 -0.63102681281888473 0 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 0 -1.1299600123008406e-029 ;
	setAttr ".bps" -type "matrix" 0.99999981145095374 -0.00024635605073557317 0.00056250044770701085 0
		 0.00024643732485707203 0.9999999592053963 -0.00014442247344810747 0 -0.00056246484540983278 0.00014456106732295158 0.99999983136768356 0
		 0.72379468964017357 0.37907435392593442 -3.4465590584025247 1;
	setAttr ".radi" 0.025;
	setAttr ".liw" yes;
createNode ikHandle -n "ballBackProxySkinBtwLFT_ikHndl" -p "ballBackProxySkinBtwHideLFT_grp";
	rename -uid "8E2634C7-429A-5E9B-6CE8-C99CE323A516";
	setAttr ".hs" 1;
	setAttr -l on ".pv" -type "double3" 0 0 0 ;
	setAttr -l on ".pv";
	setAttr ".roc" yes;
createNode pointConstraint -n "ballBackProxySkinBtwLFT_ikHndl_pointConstraint1" -p
		 "ballBackProxySkinBtwLFT_ikHndl";
	rename -uid "678B2447-40D0-0A4D-304E-EB8D32EB0431";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "ballBackProxySkinBtwAimEndLFT_jntW0" 
		-dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "ballBackProxySkinBtwVoidEndLFT_jntW1" 
		-dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" 0.72321427978001396 0.063479863546943571 -3.4460335321771982 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode joint -n "ballBackProxySkinBtwLFT_jnt" -p "ballBackProxySkinBtwAllMoveLFT_grp";
	rename -uid "5A2EF83C-48D3-45FD-A69F-F2BC933AD396";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "aimBtw" -ln "aimBtw" -dv 0.5 -min 0 -max 1 -at "float";
	addAttr -ci true -sn "scaleBtw" -ln "scaleBtw" -dv 2 -at "float";
	setAttr -l on ".v";
	setAttr -l on ".t";
	setAttr -l on ".r";
	setAttr ".ro" 1;
	setAttr -l on ".s";
	setAttr ".jo" -type "double3" -25.624259173270001 0 0 ;
	setAttr ".bps" -type "matrix" 0.99999981145095374 -0.00024635605073557317 0.00056250044770701085 0
		 0.00024643732485707203 0.9999999592053963 -0.00014442247344810747 0 -0.00056246484540983278 0.00014456106732295158 0.99999983136768356 0
		 0.72379468964017357 0.37907435392593442 -3.4465590584025247 1;
	setAttr ".radi" 0.075000000000000011;
	setAttr ".liw" yes;
	setAttr -k on ".aimBtw";
	setAttr -k on ".scaleBtw";
createNode joint -n "ballBackProxySkinBtwEndLFT_jnt" -p "ballBackProxySkinBtwLFT_jnt";
	rename -uid "156944D2-4953-3BF5-4E19-FFAB8F595137";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 1.1102230246251565e-016 -0.63102681281888473 -4.4408920985006262e-016 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 0 -1.1299600123008406e-029 ;
	setAttr ".bps" -type "matrix" 0.99999981145095374 -0.00024635605073557317 0.00056250044770701085 0
		 0.00024643732485707203 0.9999999592053963 -0.00014442247344810747 0 -0.00056246484540983278 0.00014456106732295158 0.99999983136768356 0
		 0.72379468964017357 0.37907435392593442 -3.4465590584025247 1;
	setAttr ".radi" 0.025;
	setAttr ".liw" yes;
createNode parentConstraint -n "ballBackProxySkinBtwLFT_jnt_parentConstraint1" -p
		 "ballBackProxySkinBtwLFT_jnt";
	rename -uid "B06AFE19-4B4D-8DFC-5520-80917E27222C";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "ballBackProxySkinBtwIkLFT_jntW0" 
		-dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode pointConstraint -n "ballBackProxySkinBtwAllLFT_grp_pointConstraint1" -p
		 "ballBackProxySkinBtwAllLFT_grp";
	rename -uid "E9D4ABDB-465D-71BB-D859-CF96E0831A13";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "ballBackProxySkinLFT_jntW0" -dv 1 
		-min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" -1.5543122344752192e-015 0.48854008456475356 0.16068004365228505 ;
	setAttr -k on ".w0";
createNode joint -n "lowLegRbnDtl1BackProxySkinLFT_jnt" -p "lowLegBackProxySkinLFT_jnt";
	rename -uid "6F161117-44CD-7DEB-18D8-B4B25F97102C";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -3.3306690738754696e-016 0.19066778626801506 -0.052965109730341187 ;
	setAttr ".r" -type "double3" 3.1805546814635183e-015 6.8102666859835328e-015 2.4516110313417791e-014 ;
	setAttr ".jo" -type "double3" 15.524623128116549 0 -179.99999999999997 ;
	setAttr ".pa" -type "double3" 6.361109362927032e-015 1.4124500153760504e-030 -1.7655625192200626e-031 ;
	setAttr ".bps" -type "matrix" 0.99999164810399821 0.00085875358477236754 -0.0039957808410960374 0
		 0.00024295140964730418 0.96345700243338728 0.26786292284802093 0 0.0040797912767664892 -0.26786165646533455 0.9634487471053067 0
		 0.7231662027053668 2.5835550102984652 -3.1300692330684483 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode joint -n "lowLegRbnDtl2BackProxySkinLFT_jnt" -p "lowLegBackProxySkinLFT_jnt";
	rename -uid "709181FC-4DD3-5219-1A5D-0FA86D48D10B";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -8.8817841970012523e-016 0.57200335880404563 -0.15889532919102045 ;
	setAttr ".r" -type "double3" 3.1805546814635183e-015 6.8102666859835328e-015 2.4516110313417791e-014 ;
	setAttr ".jo" -type "double3" 15.524623128116549 0 -179.99999999999997 ;
	setAttr ".pa" -type "double3" 6.361109362927032e-015 1.4124500153760504e-030 -1.7655625192200626e-031 ;
	setAttr ".bps" -type "matrix" 0.99999164810399821 0.00085875358477236754 -0.0039957808410960374 0
		 0.00024295140964730418 0.96345700243338728 0.26786292284802093 0 0.0040797912767664892 -0.26786165646533455 0.9634487471053067 0
		 0.72307004855607282 2.2022425964928143 -3.236082742153175 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode joint -n "lowLegRbnDtl3BackProxySkinLFT_jnt" -p "lowLegBackProxySkinLFT_jnt";
	rename -uid "7ED49CB3-45FB-CC5B-2AB5-92877630861A";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -8.8817841970012523e-016 0.95333893134007663 -0.26482554865170016 ;
	setAttr ".r" -type "double3" 3.1805546814635183e-015 6.8102666859835328e-015 2.4516110313417791e-014 ;
	setAttr ".jo" -type "double3" 15.524623128116549 0 -179.99999999999997 ;
	setAttr ".pa" -type "double3" 6.361109362927032e-015 1.4124500153760504e-030 -1.7655625192200626e-031 ;
	setAttr ".bps" -type "matrix" 0.99999164810399821 0.00085875358477236754 -0.0039957808410960374 0
		 0.00024295140964730418 0.96345700243338728 0.26786292284802093 0 0.0040797912767664892 -0.26786165646533455 0.9634487471053067 0
		 0.72297389440677884 1.8209301826871624 -3.3420962512379027 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode joint -n "lowLegRbnDtl4BackProxySkinLFT_jnt" -p "lowLegBackProxySkinLFT_jnt";
	rename -uid "EDA4EA8A-4A07-0CBF-D622-639D478932BF";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -1.4432899320127035e-015 1.3346745038761072 -0.37075576811237987 ;
	setAttr ".r" -type "double3" 3.1805546814635183e-015 6.8102666859835328e-015 2.4516110313417791e-014 ;
	setAttr ".jo" -type "double3" 15.524623128116549 0 -179.99999999999997 ;
	setAttr ".pa" -type "double3" 6.361109362927032e-015 1.4124500153760504e-030 -1.7655625192200626e-031 ;
	setAttr ".bps" -type "matrix" 0.99999164810399821 0.00085875358477236754 -0.0039957808410960374 0
		 0.00024295140964730418 0.96345700243338728 0.26786292284802093 0 0.0040797912767664892 -0.26786165646533455 0.9634487471053067 0
		 0.72287774025748464 1.4396177688815126 -3.4481097603226294 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode joint -n "lowLegRbnDtl5BackProxySkinLFT_jnt" -p "lowLegBackProxySkinLFT_jnt";
	rename -uid "EB86B431-4595-6BA6-A698-098A42B429D5";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -1.4432899320127035e-015 1.7160100764121378 -0.47668598757305913 ;
	setAttr ".r" -type "double3" 3.1805546814635183e-015 6.8102666859835328e-015 2.4516110313417791e-014 ;
	setAttr ".jo" -type "double3" 15.524623128116549 0 -179.99999999999997 ;
	setAttr ".pa" -type "double3" 6.361109362927032e-015 1.4124500153760504e-030 -1.7655625192200626e-031 ;
	setAttr ".bps" -type "matrix" 0.99999164810399821 0.00085875358477236754 -0.0039957808410960374 0
		 0.00024295140964730418 0.96345700243338728 0.26786292284802093 0 0.0040797912767664892 -0.26786165646533455 0.9634487471053067 0
		 0.72278158610819099 1.0583053550758603 -3.5541232694073557 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode transform -n "ankleBackProxySkinBtwAllLFT_grp" -p "lowLegRbnDtl5BackProxySkinLFT_jnt";
	rename -uid "FC203A64-4547-51D5-28A8-7C8BC9D8C231";
	setAttr ".r" -type "double3" 15.524623128116549 8.688306303551694e-015 179.99999999999997 ;
	setAttr ".s" -type "double3" 1 0.99999999999999978 1 ;
createNode transform -n "ankleBackProxySkinBtwAllMoveLFT_grp" -p "ankleBackProxySkinBtwAllLFT_grp";
	rename -uid "EAABF427-43DE-70D2-0A58-B59CCD486CA6";
	setAttr ".t" -type "double3" 0 -0.17177132944548301 0 ;
createNode transform -n "ankleBackProxySkinBtwHideLFT_grp" -p "ankleBackProxySkinBtwAllMoveLFT_grp";
	rename -uid "88A574CB-41C3-6402-0B3B-88B66FE07BCF";
	setAttr ".v" no;
createNode transform -n "ankleBackProxySkinBtwUpLFT_grp" -p "ankleBackProxySkinBtwHideLFT_grp";
	rename -uid "087107C5-4624-B0B7-51F6-29AEC95EBE16";
createNode joint -n "ankleBackProxySkinBtwIkLFT_jnt" -p "ankleBackProxySkinBtwHideLFT_grp";
	rename -uid "97947098-497C-19E9-A2F5-A483EC2418D0";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 6.8382649783430098 0 0 ;
	setAttr ".bps" -type "matrix" -0.99999164810399821 -0.00085875358477135685 0.0039957808410960374 0
		 0.0008578787839038784 -0.99999960768092577 -0.00022063994786323578 0 0.0039959687488211691 -0.00021721020949263151 0.99999199249468163 0
		 0.72273350903354416 0.86764914817303662 -3.6071300239497184 1;
	setAttr ".radi" 0.075000000000000011;
	setAttr ".liw" yes;
createNode joint -n "ankleBackProxySkinBtwEndIkLFT_jnt" -p "ankleBackProxySkinBtwIkLFT_jnt";
	rename -uid "924C9C92-4C39-B39C-A36D-D59B261D2971";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 1.1102230246251565e-016 0.34354265889096669 0 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999164810399821 -0.00085875358477135685 0.0039957808410960374 0
		 0.0008578787839038784 -0.99999960768092577 -0.00022063994786323578 0 0.0039959687488211691 -0.00021721020949263151 0.99999199249468163 0
		 0.72273350903354416 0.86764914817303662 -3.6071300239497184 1;
	setAttr ".radi" 0.025;
	setAttr ".liw" yes;
createNode ikEffector -n "ankleBackProxySkinBtwEndLFT_eff" -p "ankleBackProxySkinBtwIkLFT_jnt";
	rename -uid "8DE66D79-4D91-D8F8-48A8-71B41E64177E";
	setAttr ".v" no;
	setAttr ".hd" yes;
createNode joint -n "ankleBackProxySkinBtwAimLFT_jnt" -p "ankleBackProxySkinBtwHideLFT_grp";
	rename -uid "342D1C47-4AB8-ED24-CAD1-5D9BA00F0A0A";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999164810399821 -0.00085875358477135685 0.0039957808410960374 0
		 0.0008578787839038784 -0.99999960768092577 -0.00022063994786323578 0 0.0039959687488211691 -0.00021721020949263151 0.99999199249468163 0
		 0.72273350903354416 0.86764914817303662 -3.6071300239497184 1;
	setAttr ".radi" 0.075000000000000011;
	setAttr ".liw" yes;
createNode joint -n "ankleBackProxySkinBtwAimEndLFT_jnt" -p "ankleBackProxySkinBtwAimLFT_jnt";
	rename -uid "D4DAB5B0-49C7-AD69-0E1C-3BA2E846E86C";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 0.34354265889096647 0 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999164810399821 -0.00085875358477135685 0.0039957808410960374 0
		 0.0008578787839038784 -0.99999960768092577 -0.00022063994786323578 0 0.0039959687488211691 -0.00021721020949263151 0.99999199249468163 0
		 0.72273350903354416 0.86764914817303662 -3.6071300239497184 1;
	setAttr ".radi" 0.025;
	setAttr ".liw" yes;
createNode aimConstraint -n "ankleBackProxySkinBtwAimLFT_jnt_aimConstraint1" -p "ankleBackProxySkinBtwAimLFT_jnt";
	rename -uid "1DAB457F-403A-24E6-2996-B6A9D2680BC3";
	addAttr -dcb 0 -ci true -sn "w0" -ln "ballBackProxySkinLFT_jntW0" -dv 1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".a" -type "double3" 0 1 0 ;
	setAttr ".u" -type "double3" 1 0 0 ;
	setAttr ".wut" 1;
	setAttr ".rsrr" -type "double3" 13.676529956685853 1.4592435270529439e-014 1.2168456712538949e-013 ;
	setAttr -k on ".w0";
createNode joint -n "ankleBackProxySkinBtwVoidLFT_jnt" -p "ankleBackProxySkinBtwHideLFT_grp";
	rename -uid "7DB7F132-4CF9-FF78-7833-D5BBA1A77DFE";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999164810399821 -0.00085875358477135685 0.0039957808410960374 0
		 0.0008578787839038784 -0.99999960768092577 -0.00022063994786323578 0 0.0039959687488211691 -0.00021721020949263151 0.99999199249468163 0
		 0.72273350903354416 0.86764914817303662 -3.6071300239497184 1;
	setAttr ".radi" 0.075000000000000011;
	setAttr ".liw" yes;
createNode joint -n "ankleBackProxySkinBtwVoidEndLFT_jnt" -p "ankleBackProxySkinBtwVoidLFT_jnt";
	rename -uid "DD22A06E-40FB-F69B-6703-87A5DDD77C19";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 0.34354265889096647 0 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999164810399821 -0.00085875358477135685 0.0039957808410960374 0
		 0.0008578787839038784 -0.99999960768092577 -0.00022063994786323578 0 0.0039959687488211691 -0.00021721020949263151 0.99999199249468163 0
		 0.72273350903354416 0.86764914817303662 -3.6071300239497184 1;
	setAttr ".radi" 0.025;
	setAttr ".liw" yes;
createNode ikHandle -n "ankleBackProxySkinBtwLFT_ikHndl" -p "ankleBackProxySkinBtwHideLFT_grp";
	rename -uid "AB7F9F17-49F9-1D15-BEE1-EC9D67DD25AB";
	setAttr ".r" -type "double3" -3.1805546814635251e-015 -8.688306303551694e-015 -179.99999999999994 ;
	setAttr ".s" -type "double3" 1 0.99999999999999978 1 ;
	setAttr ".hs" 1;
	setAttr -l on ".pv" -type "double3" 0 0 0 ;
	setAttr -l on ".pv";
	setAttr ".roc" yes;
createNode pointConstraint -n "ankleBackProxySkinBtwLFT_ikHndl_pointConstraint1" 
		-p "ankleBackProxySkinBtwLFT_ikHndl";
	rename -uid "927D6581-465A-35AD-E123-0A8AFAA2E924";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "ankleBackProxySkinBtwAimEndLFT_jntW0" 
		-dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "ankleBackProxySkinBtwVoidEndLFT_jntW1" 
		-dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" 0.72321427978001274 0.69576202507565621 -3.6067135758294837 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode joint -n "ankleBackProxySkinBtwLFT_jnt" -p "ankleBackProxySkinBtwAllMoveLFT_grp";
	rename -uid "61DEC71D-45B0-37F5-A175-C085E12DCD8D";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "aimBtw" -ln "aimBtw" -dv 0.5 -min 0 -max 1 -at "float";
	addAttr -ci true -sn "scaleBtw" -ln "scaleBtw" -dv 2 -at "float";
	setAttr -l on ".v";
	setAttr -l on ".t";
	setAttr -l on ".r";
	setAttr ".ro" 1;
	setAttr -l on ".s";
	setAttr ".jo" -type "double3" 6.8382649783430098 0 0 ;
	setAttr ".bps" -type "matrix" -0.99999164810399821 -0.00085875358477135685 0.0039957808410960374 0
		 0.0008578787839038784 -0.99999960768092577 -0.00022063994786323578 0 0.0039959687488211691 -0.00021721020949263151 0.99999199249468163 0
		 0.72273350903354416 0.86764914817303662 -3.6071300239497184 1;
	setAttr ".radi" 0.075000000000000011;
	setAttr ".liw" yes;
	setAttr -k on ".aimBtw";
	setAttr -k on ".scaleBtw";
createNode joint -n "ankleBackProxySkinBtwEndLFT_jnt" -p "ankleBackProxySkinBtwLFT_jnt";
	rename -uid "8CF66EE4-498C-D14F-F8FB-CD98FF3FDB7B";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 1.1102230246251565e-016 0.34354265889096669 0 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999164810399821 -0.00085875358477135685 0.0039957808410960374 0
		 0.0008578787839038784 -0.99999960768092577 -0.00022063994786323578 0 0.0039959687488211691 -0.00021721020949263151 0.99999199249468163 0
		 0.72273350903354416 0.86764914817303662 -3.6071300239497184 1;
	setAttr ".radi" 0.025;
	setAttr ".liw" yes;
createNode parentConstraint -n "ankleBackProxySkinBtwLFT_jnt_parentConstraint1" -p
		 "ankleBackProxySkinBtwLFT_jnt";
	rename -uid "1E6C52F4-488B-3622-81FE-1EA102ADD0EC";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "ankleBackProxySkinBtwIkLFT_jntW0" 
		-dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode pointConstraint -n "ankleBackProxySkinBtwAllLFT_grp_pointConstraint1" 
		-p "ankleBackProxySkinBtwAllLFT_grp";
	rename -uid "2A8AC241-4212-931F-4F28-4B9E148FA885";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "ankleBackProxySkinLFT_jntW0" -dv 
		1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 2.2204460492503131e-016 -0.19788761348071354 -1.3322676295501878e-015 ;
	setAttr -k on ".w0";
createNode joint -n "midLegRbnDtl1BackProxySkinLFT_jnt" -p "midLegBackProxySkinLFT_jnt";
	rename -uid "60411271-487B-D8EB-13AF-F181C3781A7D";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -1.1102230246251565e-016 0.11635193577514924 -0.081748182507883183 ;
	setAttr ".r" -type "double3" 6.3611093629270367e-015 1.4627651434727033e-014 2.0819491125818029e-014 ;
	setAttr ".jo" -type "double3" 35.091650654348228 0 -179.99999999999997 ;
	setAttr ".bps" -type "matrix" 0.99999164810399821 0.00085875358477236754 -0.0039957808410960374 0
		 0.0015952814185938311 0.81810830843519355 0.57506195383323866 0 0.0037628180191073292 -0.57506352537053429 0.8181001057260463 0
		 0.72524528014546052 3.8157693109910054 -2.3449339494916135 1;
	setAttr ".radi" 0.1;
createNode joint -n "midLegRbnDtl2BackProxySkinLFT_jnt" -p "midLegBackProxySkinLFT_jnt";
	rename -uid "52F1AB26-43E9-CBBC-6D17-009357B48C88";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -7.7715611723760958e-016 0.34815345852773172 -0.24461056259069602 ;
	setAttr ".r" -type "double3" 6.3611093629270367e-015 1.4627651434727033e-014 2.0819491125818029e-014 ;
	setAttr ".jo" -type "double3" 35.091650654348228 0 -179.99999999999997 ;
	setAttr ".bps" -type "matrix" 0.99999164810399821 0.00085875358477236754 -0.0039957808410960374 0
		 0.0015952814185938311 0.81810830843519355 0.57506195383323866 0 0.0037628180191073292 -0.57506352537053429 0.8181001057260463 0
		 0.7247934927426789 3.5840791379878443 -2.5077928303970065 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode joint -n "midLegRbnDtl3BackProxySkinLFT_jnt" -p "midLegBackProxySkinLFT_jnt";
	rename -uid "D437C1F2-4FB1-B6FC-8F6B-84A93715670A";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -1.2212453270876722e-015 0.57893983888288503 -0.40675970962393704 ;
	setAttr ".r" -type "double3" 6.3611093629270367e-015 1.4627651434727033e-014 2.0819491125818029e-014 ;
	setAttr ".jo" -type "double3" 35.091650654348228 0 -179.99999999999997 ;
	setAttr ".bps" -type "matrix" 0.99999164810399821 0.00085875358477236754 -0.0039957808410960374 0
		 0.0015952814185938311 0.81810830843519355 0.57506195383323866 0 0.0037628180191073292 -0.57506352537053429 0.8181001057260463 0
		 0.72434301866301465 3.3530624765932959 -2.6701782887768157 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode joint -n "midLegRbnDtl4BackProxySkinLFT_jnt" -p "midLegBackProxySkinLFT_jnt";
	rename -uid "ED6B386B-426B-296D-F427-4AB0758D45A9";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -1.8873791418627661e-015 0.80972621923803967 -0.5689088566571785 ;
	setAttr ".r" -type "double3" 6.3611093629270367e-015 1.4627651434727033e-014 2.0819491125818029e-014 ;
	setAttr ".jo" -type "double3" 35.091650654348228 0 -179.99999999999997 ;
	setAttr ".bps" -type "matrix" 0.99999164810399821 0.00085875358477236754 -0.0039957808410960374 0
		 0.0015952814185938311 0.81810830843519355 0.57506195383323866 0 0.0037628180191073292 -0.57506352537053429 0.8181001057260463 0
		 0.72389254458334973 3.1220458151987485 -2.832563747156625 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode joint -n "midLegRbnDtl5BackProxySkinLFT_jnt" -p "midLegBackProxySkinLFT_jnt";
	rename -uid "1FCE203F-4A70-2084-665A-A3903ABD45B1";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -2.3314683517128287e-015 1.041527741990623 -0.73177123673999178 ;
	setAttr ".r" -type "double3" 6.3611093629270367e-015 1.4627651434727033e-014 2.0819491125818029e-014 ;
	setAttr ".jo" -type "double3" 35.091650654348228 0 -179.99999999999997 ;
	setAttr ".bps" -type "matrix" 0.99999164810399821 0.00085875358477236754 -0.0039957808410960374 0
		 0.0015952814185938311 0.81810830843519355 0.57506195383323866 0 0.0037628180191073292 -0.57506352537053429 0.8181001057260463 0
		 0.72344075718056788 2.8903556421955892 -2.9954226280620162 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode transform -n "lowLegBackProxySkinBtwAllLFT_grp" -p "midLegRbnDtl5BackProxySkinLFT_jnt";
	rename -uid "FDB2C1EE-4C42-3EB4-C4F5-F5B0A03E0C66";
	setAttr ".r" -type "double3" 35.091650654348221 1.8661459532570671e-014 179.99999999999997 ;
	setAttr ".s" -type "double3" 1 0.99999999999999978 0.99999999999999989 ;
createNode transform -n "lowLegBackProxySkinBtwAllMoveLFT_grp" -p "lowLegBackProxySkinBtwAllLFT_grp";
	rename -uid "036DED32-4DB7-725B-DA2D-249B52199226";
	setAttr ".t" -type "double3" 0 -0.066094462902558562 0 ;
createNode transform -n "lowLegBackProxySkinBtwHideLFT_grp" -p "lowLegBackProxySkinBtwAllMoveLFT_grp";
	rename -uid "B3D05C3E-4C8F-4423-7F16-E9B12430317C";
	setAttr ".v" no;
createNode transform -n "lowLegBackProxySkinBtwUpLFT_grp" -p "lowLegBackProxySkinBtwHideLFT_grp";
	rename -uid "C6A3C9BA-4EB2-58A3-866D-4994E6AF6889";
createNode joint -n "lowLegBackProxySkinBtwIkLFT_jnt" -p "lowLegBackProxySkinBtwHideLFT_grp";
	rename -uid "F40A2121-47C8-01EE-9C8F-D4A8D1C3BF18";
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" -5.8277653085941035 0 0 ;
	setAttr ".bps" -type "matrix" -0.99999164810399821 -0.00085875358477135685 0.0039957808410960374 0
		 0.0008578787839038784 -0.99999960768092577 -0.00022063994786323578 0 0.0039959687488211691 -0.00021721020949263151 0.99999199249468163 0
		 0.72321427978001307 2.7742112172012918 -3.0770624785260807 1;
	setAttr ".radi" 0.075000000000000011;
createNode joint -n "lowLegBackProxySkinBtwEndIkLFT_jnt" -p "lowLegBackProxySkinBtwIkLFT_jnt";
	rename -uid "CA19E1B4-4889-A7E0-3AF1-E597D178A8B5";
	setAttr ".t" -type "double3" 0 0.13218892580511721 -4.4408920985006262e-016 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999164810399821 -0.00085875358477135685 0.0039957808410960374 0
		 0.0008578787839038784 -0.99999960768092577 -0.00022063994786323578 0 0.0039959687488211691 -0.00021721020949263151 0.99999199249468163 0
		 0.72321427978001307 2.7742112172012918 -3.0770624785260807 1;
	setAttr ".radi" 0.025;
createNode ikEffector -n "lowLegBackProxySkinBtwEndLFT_eff" -p "lowLegBackProxySkinBtwIkLFT_jnt";
	rename -uid "D3CB113D-4B90-A581-F35C-2C858FC84F57";
	setAttr ".v" no;
	setAttr ".hd" yes;
createNode joint -n "lowLegBackProxySkinBtwAimLFT_jnt" -p "lowLegBackProxySkinBtwHideLFT_grp";
	rename -uid "81821956-45B6-30E5-DE20-F6A364193239";
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999164810399821 -0.00085875358477135685 0.0039957808410960374 0
		 0.0008578787839038784 -0.99999960768092577 -0.00022063994786323578 0 0.0039959687488211691 -0.00021721020949263151 0.99999199249468163 0
		 0.72321427978001307 2.7742112172012918 -3.0770624785260807 1;
	setAttr ".radi" 0.075000000000000011;
createNode joint -n "lowLegBackProxySkinBtwAimEndLFT_jnt" -p "lowLegBackProxySkinBtwAimLFT_jnt";
	rename -uid "E4C384FB-4D11-D6D0-AF29-F799BB93649D";
	setAttr ".t" -type "double3" 0 0.13218892580511721 0 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999164810399821 -0.00085875358477135685 0.0039957808410960374 0
		 0.0008578787839038784 -0.99999960768092577 -0.00022063994786323578 0 0.0039959687488211691 -0.00021721020949263151 0.99999199249468163 0
		 0.72321427978001307 2.7742112172012918 -3.0770624785260807 1;
	setAttr ".radi" 0.025;
createNode aimConstraint -n "lowLegBackProxySkinBtwAimLFT_jnt_aimConstraint1" -p "lowLegBackProxySkinBtwAimLFT_jnt";
	rename -uid "F4A100D9-417A-B4E8-1142-529193162BDF";
	addAttr -dcb 0 -ci true -sn "w0" -ln "lowLegRbnDtl1BackProxySkinLFT_jntW0" -dv 
		1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".a" -type "double3" 0 1 0 ;
	setAttr ".u" -type "double3" 1 0 0 ;
	setAttr ".wut" 1;
	setAttr ".rsrr" -type "double3" -11.65553061718815 -4.952946231569767e-015 4.8526936593911299e-014 ;
	setAttr -k on ".w0";
createNode joint -n "lowLegBackProxySkinBtwVoidLFT_jnt" -p "lowLegBackProxySkinBtwHideLFT_grp";
	rename -uid "6A390EE5-4B5C-C9F2-B8F6-10952FEC7DB0";
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999164810399821 -0.00085875358477135685 0.0039957808410960374 0
		 0.0008578787839038784 -0.99999960768092577 -0.00022063994786323578 0 0.0039959687488211691 -0.00021721020949263151 0.99999199249468163 0
		 0.72321427978001307 2.7742112172012918 -3.0770624785260807 1;
	setAttr ".radi" 0.075000000000000011;
createNode joint -n "lowLegBackProxySkinBtwVoidEndLFT_jnt" -p "lowLegBackProxySkinBtwVoidLFT_jnt";
	rename -uid "04CC79DF-44C4-BC56-FAB0-0EA83ECF666E";
	setAttr ".t" -type "double3" 0 0.13218892580511721 0 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999164810399821 -0.00085875358477135685 0.0039957808410960374 0
		 0.0008578787839038784 -0.99999960768092577 -0.00022063994786323578 0 0.0039959687488211691 -0.00021721020949263151 0.99999199249468163 0
		 0.72321427978001307 2.7742112172012918 -3.0770624785260807 1;
	setAttr ".radi" 0.025;
createNode ikHandle -n "lowLegBackProxySkinBtwLFT_ikHndl" -p "lowLegBackProxySkinBtwHideLFT_grp";
	rename -uid "1BC8F9AA-4404-11C2-6B72-BEB759D1E2DF";
	setAttr ".r" -type "double3" -6.3611093629270501e-015 -1.8661459532570664e-014 
		-179.99999999999994 ;
	setAttr ".hs" 1;
	setAttr -l on ".pv" -type "double3" 0 0 0 ;
	setAttr -l on ".pv";
	setAttr ".roc" yes;
createNode pointConstraint -n "lowLegBackProxySkinBtwLFT_ikHndl_pointConstraint1" 
		-p "lowLegBackProxySkinBtwLFT_ikHndl";
	rename -uid "13511023-4082-9764-96FF-76A4A6D3DD08";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "lowLegBackProxySkinBtwAimEndLFT_jntW0" 
		-dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "lowLegBackProxySkinBtwVoidEndLFT_jntW1" 
		-dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" 0.72321427978001307 2.7081167542987332 -3.0770624785260834 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode joint -n "lowLegBackProxySkinBtwLFT_jnt" -p "lowLegBackProxySkinBtwAllMoveLFT_grp";
	rename -uid "9335469D-41A9-3738-33E9-029A39BA93C6";
	addAttr -ci true -sn "aimBtw" -ln "aimBtw" -dv 0.5 -min 0 -max 1 -at "float";
	addAttr -ci true -sn "scaleBtw" -ln "scaleBtw" -dv 2 -at "float";
	setAttr -l on ".v";
	setAttr -l on ".t";
	setAttr -l on ".r";
	setAttr ".ro" 1;
	setAttr -l on ".s";
	setAttr ".jo" -type "double3" -5.8277653085941035 0 0 ;
	setAttr ".bps" -type "matrix" -0.99999164810399821 -0.00085875358477135685 0.0039957808410960374 0
		 0.0008578787839038784 -0.99999960768092577 -0.00022063994786323578 0 0.0039959687488211691 -0.00021721020949263151 0.99999199249468163 0
		 0.72321427978001307 2.7742112172012918 -3.0770624785260807 1;
	setAttr ".radi" 0.075000000000000011;
	setAttr -k on ".aimBtw";
	setAttr -k on ".scaleBtw";
createNode joint -n "lowLegBackProxySkinBtwEndLFT_jnt" -p "lowLegBackProxySkinBtwLFT_jnt";
	rename -uid "C0AA9DE0-487B-6D45-230C-D7862B66032D";
	setAttr ".t" -type "double3" 0 0.13218892580511721 -4.4408920985006262e-016 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999164810399821 -0.00085875358477135685 0.0039957808410960374 0
		 0.0008578787839038784 -0.99999960768092577 -0.00022063994786323578 0 0.0039959687488211691 -0.00021721020949263151 0.99999199249468163 0
		 0.72321427978001307 2.7742112172012918 -3.0770624785260807 1;
	setAttr ".radi" 0.025;
createNode parentConstraint -n "lowLegBackProxySkinBtwLFT_jnt_parentConstraint1" 
		-p "lowLegBackProxySkinBtwLFT_jnt";
	rename -uid "B2062630-4D04-1E6F-E882-DA9B6D0162ED";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "lowLegBackProxySkinBtwIkLFT_jntW0" 
		-dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode pointConstraint -n "lowLegBackProxySkinBtwAllLFT_grp_pointConstraint1" 
		-p "lowLegBackProxySkinBtwAllLFT_grp";
	rename -uid "D942F5C6-4D3C-7DEE-8F57-77B933312D71";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "lowLegBackProxySkinLFT_jntW0" -dv 
		1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 1.1102230246251565e-016 -0.14219893917313919 0 ;
	setAttr -k on ".w0";
createNode joint -n "upLegRbnDtl1BackProxySkinLFT_jnt" -p "upLegBackProxySkinLFT_jnt";
	rename -uid "B7DDAB1F-43E3-20A4-C980-2E96A6ACC232";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -2.2204460492503131e-016 0.097333526018832828 0.019273755618455457 ;
	setAttr ".r" -type "double3" -1.5902773407317598e-015 -4.9424795394656484e-015 2.4959793533518082e-014 ;
	setAttr ".jo" -type "double3" -11.200678874588055 0 -179.99999999999997 ;
	setAttr ".bps" -type "matrix" 0.99500084171789716 -0.099786488985164762 -0.0039976989243278747 0
		 0.097132211342723387 0.97628531785245898 -0.193474317844502 0 0.023209017651518542 0.19211880376927309 0.98109719535727702 0
		 0.85189634006885906 5.2026195299384153 -2.5151148856055281 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode joint -n "upLegRbnDtl2BackProxySkinLFT_jnt" -p "upLegBackProxySkinLFT_jnt";
	rename -uid "222E259C-45C3-920D-69CB-BDA6030DC8BD";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -1.1102230246251565e-016 0.35517441027410257 0.070330800347534694 ;
	setAttr ".r" -type "double3" -1.5902773407317598e-015 -4.9424795394656484e-015 2.4959793533518082e-014 ;
	setAttr ".jo" -type "double3" -11.200678874588055 0 -179.99999999999997 ;
	setAttr ".bps" -type "matrix" 0.99500084171789716 -0.099786488985164762 -0.0039976989243278747 0
		 0.097132211342723387 0.97628531785245898 -0.193474317844502 0 0.023209017651518542 0.19211880376927309 0.98109719535727702 0
		 0.82622819266244196 4.9446264800227988 -2.4639873838104114 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode joint -n "upLegRbnDtl3BackProxySkinLFT_jnt" -p "upLegBackProxySkinLFT_jnt";
	rename -uid "E97FCB08-42F6-2729-CB88-61BA399EBBC3";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -1.1102230246251565e-016 0.68408585577417913 0.13546107025530274 ;
	setAttr ".r" -type "double3" -1.5902773407317598e-015 -4.9424795394656484e-015 2.4959793533518082e-014 ;
	setAttr ".jo" -type "double3" -11.200678874588055 0 -179.99999999999997 ;
	setAttr ".bps" -type "matrix" 0.99500084171789716 -0.099786488985164762 -0.0039976989243278747 0
		 0.097132211342723387 0.97628531785245898 -0.193474317844502 0 0.023209017651518542 0.19211880376927309 0.98109719535727702 0
		 0.79354138291704657 4.6160881712503592 -2.398879649408181 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode joint -n "upLegRbnDtl4BackProxySkinLFT_jnt" -p "upLegBackProxySkinLFT_jnt";
	rename -uid "C5320C12-4843-2124-9ADD-92884E8D046E";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -2.2204460492503131e-016 1.0129973012742512 0.20059134016307034 ;
	setAttr ".r" -type "double3" -1.5902773407317598e-015 -4.9424795394656484e-015 2.4959793533518082e-014 ;
	setAttr ".jo" -type "double3" -11.200678874588055 0 -179.99999999999997 ;
	setAttr ".bps" -type "matrix" 0.99500084171789716 -0.099786488985164762 -0.0039976989243278747 0
		 0.097132211342723387 0.97628531785245898 -0.193474317844502 0 0.023209017651518542 0.19211880376927309 0.98109719535727702 0
		 0.76085457317165162 4.2875498624779196 -2.3337719150059528 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode joint -n "upLegRbnDtl5BackProxySkinLFT_jnt" -p "upLegBackProxySkinLFT_jnt";
	rename -uid "697A5559-45C1-45D6-35A0-8F8C1EE22846";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 1.2708381855295192 0.25164838489214869 ;
	setAttr ".r" -type "double3" -1.5902773407317598e-015 -4.9424795394656484e-015 2.4959793533518082e-014 ;
	setAttr ".jo" -type "double3" -11.200678874588055 0 -179.99999999999997 ;
	setAttr ".bps" -type "matrix" 0.99500084171789716 -0.099786488985164762 -0.0039976989243278747 0
		 0.097132211342723387 0.97628531785245898 -0.193474317844502 0 0.023209017651518542 0.19211880376927309 0.98109719535727702 0
		 0.73518642576523385 4.0295568125622996 -2.2826444132108374 1;
	setAttr ".radi" 0.1;
createNode transform -n "midLegBackProxySkinBtwAllLFT_grp" -p "upLegRbnDtl5BackProxySkinLFT_jnt";
	rename -uid "B8A6691A-4BFB-46A3-7C42-12A43AB9C17A";
	setAttr ".t" -type "double3" 1.1102230246251565e-016 -0.099223449561574917 -6.6613381477509392e-016 ;
	setAttr ".r" -type "double3" -11.200678874588055 -6.3054470724758499e-015 179.99999999999997 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1 ;
createNode transform -n "midLegBackProxySkinBtwAllMoveLFT_grp" -p "midLegBackProxySkinBtwAllLFT_grp";
	rename -uid "F603C3DE-4680-B87F-F176-BD88AEBDE761";
	setAttr ".t" -type "double3" 0 -0.047494445683829066 0 ;
createNode transform -n "midLegBackProxySkinBtwHideLFT_grp" -p "midLegBackProxySkinBtwAllMoveLFT_grp";
	rename -uid "0364E9FF-4E17-6CC2-AC8B-D7BB3C482524";
	setAttr ".v" no;
createNode transform -n "midLegBackProxySkinBtwUpLFT_grp" -p "midLegBackProxySkinBtwHideLFT_grp";
	rename -uid "76E33E28-4AFA-4A3B-14C3-39AECEBD8130";
createNode joint -n "midLegBackProxySkinBtwIkLFT_jnt" -p "midLegBackProxySkinBtwHideLFT_grp";
	rename -uid "FF221692-4A82-E5B0-FDAA-1D9418C480FD";
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" -13.258035536560637 0 0 ;
	setAttr ".bps" -type "matrix" -0.99999164810399821 -0.00085875358477135685 0.0039957808410960374 0
		 0.0008578787839038784 -0.99999960768092577 -0.00022063994786323578 0 0.0039959687488211691 -0.00021721020949263151 0.99999199249468163 0
		 0.72547175754601534 3.9319137359853009 -2.2632940990275512 1;
	setAttr ".radi" 0.075000000000000011;
createNode joint -n "midLegBackProxySkinBtwEndIkLFT_jnt" -p "midLegBackProxySkinBtwIkLFT_jnt";
	rename -uid "0DB4BA3E-4F13-5B87-061C-86A43C2474AD";
	setAttr ".t" -type "double3" -2.2204460492503131e-016 0.094988891367657757 -8.8817841970012523e-016 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999164810399821 -0.00085875358477135685 0.0039957808410960374 0
		 0.0008578787839038784 -0.99999960768092577 -0.00022063994786323578 0 0.0039959687488211691 -0.00021721020949263151 0.99999199249468163 0
		 0.72547175754601534 3.9319137359853009 -2.2632940990275512 1;
	setAttr ".radi" 0.025;
createNode ikEffector -n "midLegBackProxySkinBtwEndLFT_eff" -p "midLegBackProxySkinBtwIkLFT_jnt";
	rename -uid "12A0721A-4392-EF21-8CD0-9084106D2763";
	setAttr ".v" no;
	setAttr ".hd" yes;
createNode joint -n "midLegBackProxySkinBtwAimLFT_jnt" -p "midLegBackProxySkinBtwHideLFT_grp";
	rename -uid "65E9C0C2-4A5A-EEA7-5B14-82A7C60C13FC";
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999164810399821 -0.00085875358477135685 0.0039957808410960374 0
		 0.0008578787839038784 -0.99999960768092577 -0.00022063994786323578 0 0.0039959687488211691 -0.00021721020949263151 0.99999199249468163 0
		 0.72547175754601534 3.9319137359853009 -2.2632940990275512 1;
	setAttr ".radi" 0.075000000000000011;
createNode joint -n "midLegBackProxySkinBtwAimEndLFT_jnt" -p "midLegBackProxySkinBtwAimLFT_jnt";
	rename -uid "8ED927FC-4DC8-B223-9FCD-EEB737F0BE70";
	setAttr ".t" -type "double3" 0 0.094988891367657757 0 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999164810399821 -0.00085875358477135685 0.0039957808410960374 0
		 0.0008578787839038784 -0.99999960768092577 -0.00022063994786323578 0 0.0039959687488211691 -0.00021721020949263151 0.99999199249468163 0
		 0.72547175754601534 3.9319137359853009 -2.2632940990275512 1;
	setAttr ".radi" 0.025;
createNode aimConstraint -n "midLegBackProxySkinBtwAimLFT_jnt_aimConstraint1" -p "midLegBackProxySkinBtwAimLFT_jnt";
	rename -uid "6FCF294F-4595-CBF6-4875-CF92592AF195";
	addAttr -dcb 0 -ci true -sn "w0" -ln "midLegRbnDtl1BackProxySkinLFT_jntW0" -dv 
		1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".a" -type "double3" 0 1 0 ;
	setAttr ".u" -type "double3" 1 0 0 ;
	setAttr ".wut" 1;
	setAttr ".rsrr" -type "double3" -26.516071073121374 -8.1852629244041318e-015 3.4739729646900536e-014 ;
	setAttr -k on ".w0";
createNode joint -n "midLegBackProxySkinBtwVoidLFT_jnt" -p "midLegBackProxySkinBtwHideLFT_grp";
	rename -uid "EAEC361A-40B0-DCE8-F079-8FBA6303EAD8";
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999164810399821 -0.00085875358477135685 0.0039957808410960374 0
		 0.0008578787839038784 -0.99999960768092577 -0.00022063994786323578 0 0.0039959687488211691 -0.00021721020949263151 0.99999199249468163 0
		 0.72547175754601534 3.9319137359853009 -2.2632940990275512 1;
	setAttr ".radi" 0.075000000000000011;
createNode joint -n "midLegBackProxySkinBtwVoidEndLFT_jnt" -p "midLegBackProxySkinBtwVoidLFT_jnt";
	rename -uid "A261E0EB-44DA-2519-77E3-D6AF1EA2CD93";
	setAttr ".t" -type "double3" 0 0.094988891367657757 0 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999164810399821 -0.00085875358477135685 0.0039957808410960374 0
		 0.0008578787839038784 -0.99999960768092577 -0.00022063994786323578 0 0.0039959687488211691 -0.00021721020949263151 0.99999199249468163 0
		 0.72547175754601534 3.9319137359853009 -2.2632940990275512 1;
	setAttr ".radi" 0.025;
createNode ikHandle -n "midLegBackProxySkinBtwLFT_ikHndl" -p "midLegBackProxySkinBtwHideLFT_grp";
	rename -uid "2FBD8293-4E02-6C7E-6990-1CAEB9E54D41";
	setAttr ".r" -type "double3" -1.5902773407317521e-015 6.3054470724758491e-015 -179.99999999999994 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1 ;
	setAttr ".hs" 1;
	setAttr -l on ".pv" -type "double3" 0 0 0 ;
	setAttr -l on ".pv";
	setAttr ".roc" yes;
createNode pointConstraint -n "midLegBackProxySkinBtwLFT_ikHndl_pointConstraint1" 
		-p "midLegBackProxySkinBtwLFT_ikHndl";
	rename -uid "A2D42AD9-44D8-AB41-FF1E-06B2D7B08AA8";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "midLegBackProxySkinBtwAimEndLFT_jntW0" 
		-dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "midLegBackProxySkinBtwVoidEndLFT_jntW1" 
		-dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" 0.72321427978001163 3.8845964492832339 -2.2635430592782098 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode joint -n "midLegBackProxySkinBtwLFT_jnt" -p "midLegBackProxySkinBtwAllMoveLFT_grp";
	rename -uid "87D0FD90-470E-B458-CC21-84820F6B5B59";
	addAttr -ci true -sn "aimBtw" -ln "aimBtw" -dv 0.5 -min 0 -max 1 -at "float";
	addAttr -ci true -sn "scaleBtw" -ln "scaleBtw" -dv 2 -at "float";
	setAttr -l on ".v";
	setAttr -l on ".t";
	setAttr -l on ".r";
	setAttr ".ro" 1;
	setAttr -l on ".s";
	setAttr ".jo" -type "double3" -13.258035536560637 0 0 ;
	setAttr ".bps" -type "matrix" -0.99999164810399821 -0.00085875358477135685 0.0039957808410960374 0
		 0.0008578787839038784 -0.99999960768092577 -0.00022063994786323578 0 0.0039959687488211691 -0.00021721020949263151 0.99999199249468163 0
		 0.72547175754601534 3.9319137359853009 -2.2632940990275512 1;
	setAttr ".radi" 0.075000000000000011;
	setAttr -k on ".aimBtw";
	setAttr -k on ".scaleBtw";
createNode joint -n "midLegBackProxySkinBtwEndLFT_jnt" -p "midLegBackProxySkinBtwLFT_jnt";
	rename -uid "9F403782-406A-447F-5470-CCB5AA60F756";
	setAttr ".t" -type "double3" -2.2204460492503131e-016 0.094988891367657757 -8.8817841970012523e-016 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999164810399821 -0.00085875358477135685 0.0039957808410960374 0
		 0.0008578787839038784 -0.99999960768092577 -0.00022063994786323578 0 0.0039959687488211691 -0.00021721020949263151 0.99999199249468163 0
		 0.72547175754601534 3.9319137359853009 -2.2632940990275512 1;
	setAttr ".radi" 0.025;
createNode parentConstraint -n "midLegBackProxySkinBtwLFT_jnt_parentConstraint1" 
		-p "midLegBackProxySkinBtwLFT_jnt";
	rename -uid "36E3FAA2-4399-CABE-2CD5-DDB23E5B0D1E";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "midLegBackProxySkinBtwIkLFT_jntW0" 
		-dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode joint -n "clav1ScaHipProxySkinLFT_jnt" -p "clav1HipProxySkinLFT_jnt";
	rename -uid "D161923B-4F74-FF32-EA2E-90A5A766E099";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".bps" -type "matrix" -1 1.0106430996148606e-015 0 0 -1.0106430996148606e-015 -1 0 0
		 0 0 1 0 0.54736376329656067 5.6437128368163814 -1.709300550872078 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode joint -n "clav1HipProxySkinRGT_jnt" -p "pelvisProxySkin_jnt";
	rename -uid "1E1C8598-4586-D717-67A1-7FBFAD8D94C3";
	setAttr ".t" -type "double3" -0.547364 0.24592411924899515 -1.1978248295575242 ;
	setAttr ".jo" -type "double3" 0 180 0 ;
	setAttr ".bps" -type "matrix" -1 0 -1.2246467991473532e-016 0 0 1 0 0 1.2246467991473532e-016 0 -1 0
		 -0.54736399999999996 5.6437099999999996 -1.7092999999999998 1;
	setAttr ".radi" 0.05;
createNode joint -n "clav2HipProxySkinRGT_jnt" -p "clav1HipProxySkinRGT_jnt";
	rename -uid "58F7F4E1-4E3F-FA21-FE56-B19907C37867";
	setAttr ".t" -type "double3" 0.17585000000000017 -0.34344999999999981 0.82517 ;
	setAttr ".bps" -type "matrix" -1 0 -1.2246467991473532e-016 0 0 1 0 0 1.2246467991473532e-016 0 -1 0
		 -0.86161100000000002 5.3002599999999997 -2.5344699999999998 1;
	setAttr ".radi" 0.05;
createNode joint -n "upLegBackProxySkinRGT_jnt" -p "clav2HipProxySkinRGT_jnt";
	rename -uid "44F3D1A6-4ADB-ABC8-10DE-58A5F6B465BF";
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999165125565648 0.00085858090734481664 -0.0039950291377053061 0
		 0.00085770643340208686 0.99999960783787922 0.00022059864425942046 0 0.0039952169727903546 0.00021717024034469839 -0.99999199550717754 0
		 -0.86161100000000002 5.3002599999999997 -2.5344699999999998 1;
	setAttr ".radi" 0.05;
createNode joint -n "midLegBackProxySkinRGT_jnt" -p "upLegBackProxySkinRGT_jnt";
	rename -uid "36D0192F-4854-C9A8-13FF-D287E12DEEB6";
	setAttr ".t" -type "double3" -7.7715611723760958e-016 -1.3681699999999997 -0.27092999999999989 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999165125565648 0.00085858090734481664 -0.0039950291377053061 0
		 0.00085770643340208686 0.99999960783787922 0.00022059864425942046 0 0.0039952169727903546 0.00021717024034469839 -0.99999199550717754 0
		 -0.72547106778659631 3.931912873589396 -2.2632910850567871 1;
	setAttr ".radi" 0.05;
createNode joint -n "lowLegBackProxySkinRGT_jnt" -p "midLegBackProxySkinRGT_jnt";
	rename -uid "438723A2-47CE-138A-BBCF-649FAB6CE205";
	setAttr ".t" -type "double3" 7.7715611723760958e-016 -1.15788 0.81352 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999165125565648 0.00085858090734481664 -0.0039950291377053061 0
		 0.00085770643340208686 0.99999960783787922 0.00022059864425942046 0 0.0039952169727903546 0.00021717024034469839 -0.99999199550717754 0
		 -0.72321399999999991 2.7742100000000001 -3.0770599999999986 1;
	setAttr ".radi" 0.05;
createNode joint -n "ankleBackProxySkinRGT_jnt" -p "lowLegBackProxySkinRGT_jnt";
	rename -uid "44B26522-43B7-B716-0F36-1C9B60A38F60";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 1.1102230246251565e-016 -1.9066770000000002 0.52965000000000018 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999165125565648 0.00085858090734481664 -0.0039950291377053061 0
		 0.00085770643340208686 0.99999960783787922 0.00022059864425942046 0 0.0039952169727903546 0.00021717024034469839 -0.99999199550717754 0
		 -0.7227333024596817 0.86764877194429446 -3.6071263707816188 1;
	setAttr ".radi" 0.05;
	setAttr ".liw" yes;
createNode joint -n "ballBackProxySkinRGT_jnt" -p "ankleBackProxySkinRGT_jnt";
	rename -uid "43394F79-4CBB-3BA2-CD23-638DCCE90CC0";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 -0.48853999999999986 -0.16068000000000016 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 0 -180 ;
	setAttr ".bps" -type "matrix" 0.99999981153608242 0.00024631429833993845 -0.0005623673769295668 0
		 0.00024639553846109987 -0.99999995921948293 0.00014439623037953623 0 -0.00056233178713976581 -0.00014453476797870847 -0.99999983144631677 0
		 -0.72379427782384331 0.3790740686169583 -3.4465554282051891 1;
	setAttr ".radi" 0.05;
	setAttr ".liw" yes;
createNode joint -n "toeBackProxySkinRGT_jnt" -p "ballBackProxySkinRGT_jnt";
	rename -uid "CF07F69E-4227-3989-60E9-E0B819AD9936";
	setAttr ".t" -type "double3" -1.1102230246251565e-016 0.37899300000000163 -0.86528999999999989 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 89.999999999999972 0 -179.99999999999986 ;
	setAttr ".radi" 0.05;
createNode transform -n "ballBackProxySkinBtwAllRGT_grp" -p "ankleBackProxySkinRGT_jnt";
	rename -uid "609100EF-4893-0116-A7F1-13824C595F74";
	setAttr ".r" -type "double3" 7.016709298534876e-015 -7.016709298534876e-015 180 ;
createNode transform -n "ballBackProxySkinBtwAllMoveRGT_grp" -p "ballBackProxySkinBtwAllRGT_grp";
	rename -uid "F250F418-4EEA-C84F-9132-7E8AC5643708";
	setAttr ".t" -type "double3" 0 -0.31551289769578356 0 ;
createNode transform -n "ballBackProxySkinBtwHideRGT_grp" -p "ballBackProxySkinBtwAllMoveRGT_grp";
	rename -uid "9C0D2A56-4DAE-9721-D34D-8B8280F6B0A1";
	setAttr ".v" no;
createNode transform -n "ballBackProxySkinBtwUpRGT_grp" -p "ballBackProxySkinBtwHideRGT_grp";
	rename -uid "6606E080-41E5-14C2-4B65-A5B7E9F83837";
createNode joint -n "ballBackProxySkinBtwIkRGT_jnt" -p "ballBackProxySkinBtwHideRGT_grp";
	rename -uid "25A4805E-4B0D-DA5E-5CFE-F0A680EFE244";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" -25.624249890094092 0 0 ;
	setAttr ".bps" -type "matrix" 0.99999981153608242 0.00024631429833993845 -0.0005623673769295668 0
		 0.00024639553846109987 -0.99999995921948293 0.00014439623037953623 0 -0.00056233178713976581 -0.00014453476797870847 -0.99999983144631677 0
		 -0.72379427782384331 0.3790740686169583 -3.4465554282051891 1;
	setAttr ".radi" 0.075000000000000011;
	setAttr ".liw" yes;
createNode joint -n "ballBackProxySkinBtwEndIkRGT_jnt" -p "ballBackProxySkinBtwIkRGT_jnt";
	rename -uid "295614A6-445D-A605-11CE-3DA9451F806F";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -1.1102230246251565e-016 0.63102579539156745 0 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 0 -1.0523378412572102e-046 ;
	setAttr ".bps" -type "matrix" 0.99999981153608242 0.00024631429833993845 -0.0005623673769295668 0
		 0.00024639553846109987 -0.99999995921948293 0.00014439623037953623 0 -0.00056233178713976581 -0.00014453476797870847 -0.99999983144631677 0
		 -0.72379427782384331 0.3790740686169583 -3.4465554282051891 1;
	setAttr ".radi" 0.025;
	setAttr ".liw" yes;
createNode ikEffector -n "ballBackProxySkinBtwEndRGT_eff" -p "ballBackProxySkinBtwIkRGT_jnt";
	rename -uid "B5B345A0-4A34-10A2-5116-4D89F723109F";
	setAttr ".v" no;
	setAttr ".hd" yes;
createNode joint -n "ballBackProxySkinBtwAimRGT_jnt" -p "ballBackProxySkinBtwHideRGT_grp";
	rename -uid "C40527CD-4E66-075B-AD02-4EAC764B061D";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" -7.0167092985348752e-015 7.0167092985348775e-015 7.0167092985348752e-015 ;
	setAttr ".bps" -type "matrix" 0.99999981153608242 0.00024631429833993845 -0.0005623673769295668 0
		 0.00024639553846109987 -0.99999995921948293 0.00014439623037953623 0 -0.00056233178713976581 -0.00014453476797870847 -0.99999983144631677 0
		 -0.72379427782384331 0.3790740686169583 -3.4465554282051891 1;
	setAttr ".radi" 0.075000000000000011;
	setAttr ".liw" yes;
createNode joint -n "ballBackProxySkinBtwAimEndRGT_jnt" -p "ballBackProxySkinBtwAimRGT_jnt";
	rename -uid "960140FB-468A-7E6E-F96F-84AE1F57A328";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 0.63102579539156722 0 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 0 -1.0523378412572102e-046 ;
	setAttr ".bps" -type "matrix" 0.99999981153608242 0.00024631429833993845 -0.0005623673769295668 0
		 0.00024639553846109987 -0.99999995921948293 0.00014439623037953623 0 -0.00056233178713976581 -0.00014453476797870847 -0.99999983144631677 0
		 -0.72379427782384331 0.3790740686169583 -3.4465554282051891 1;
	setAttr ".radi" 0.025;
	setAttr ".liw" yes;
createNode aimConstraint -n "ballBackProxySkinBtwAimRGT_jnt_aimConstraint1" -p "ballBackProxySkinBtwAimRGT_jnt";
	rename -uid "06B66868-4495-3214-8A5E-6FBFE14715AD";
	addAttr -dcb 0 -ci true -sn "w0" -ln "toeBackProxySkinRGT_jntW0" -dv 1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".a" -type "double3" 0 1 0 ;
	setAttr ".u" -type "double3" 1 0 0 ;
	setAttr ".wut" 1;
	setAttr ".rsrr" -type "double3" -51.248499780188254 -5.4996863687719407e-015 1.1466274273897674e-014 ;
	setAttr -k on ".w0";
createNode joint -n "ballBackProxySkinBtwVoidRGT_jnt" -p "ballBackProxySkinBtwHideRGT_grp";
	rename -uid "F34FA135-49AB-8D66-FD55-519E4052926F";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" -7.0167092985348752e-015 7.0167092985348775e-015 7.0167092985348752e-015 ;
	setAttr ".bps" -type "matrix" 0.99999981153608242 0.00024631429833993845 -0.0005623673769295668 0
		 0.00024639553846109987 -0.99999995921948293 0.00014439623037953623 0 -0.00056233178713976581 -0.00014453476797870847 -0.99999983144631677 0
		 -0.72379427782384331 0.3790740686169583 -3.4465554282051891 1;
	setAttr ".radi" 0.075000000000000011;
	setAttr ".liw" yes;
createNode joint -n "ballBackProxySkinBtwVoidEndRGT_jnt" -p "ballBackProxySkinBtwVoidRGT_jnt";
	rename -uid "90017B02-47BA-7595-0779-DEB1090D110D";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 0.63102579539156722 0 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 0 -1.0523378412572102e-046 ;
	setAttr ".bps" -type "matrix" 0.99999981153608242 0.00024631429833993845 -0.0005623673769295668 0
		 0.00024639553846109987 -0.99999995921948293 0.00014439623037953623 0 -0.00056233178713976581 -0.00014453476797870847 -0.99999983144631677 0
		 -0.72379427782384331 0.3790740686169583 -3.4465554282051891 1;
	setAttr ".radi" 0.025;
	setAttr ".liw" yes;
createNode ikHandle -n "ballBackProxySkinBtwRGT_ikHndl" -p "ballBackProxySkinBtwHideRGT_grp";
	rename -uid "AE8AF82B-4455-4459-9278-CB8E805807F9";
	setAttr ".r" -type "double3" 180 -7.016709298534876e-015 7.016709298534876e-015 ;
	setAttr ".hs" 1;
	setAttr -l on ".pv" -type "double3" 0 0 0 ;
	setAttr -l on ".pv";
	setAttr ".roc" yes;
createNode pointConstraint -n "ballBackProxySkinBtwRGT_ikHndl_pointConstraint1" -p
		 "ballBackProxySkinBtwRGT_ikHndl";
	rename -uid "0D855060-45C3-E31C-0BC1-60855D900CC8";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "ballBackProxySkinBtwAimEndRGT_jntW0" 
		-dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "ballBackProxySkinBtwVoidEndRGT_jntW1" 
		-dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" -0.7232139999999998 0.063480102304216413 -3.44603 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode joint -n "ballBackProxySkinBtwRGT_jnt" -p "ballBackProxySkinBtwAllMoveRGT_grp";
	rename -uid "8082122F-4279-5E14-8485-E3A21884102C";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "aimBtw" -ln "aimBtw" -dv 0.5 -min 0 -max 1 -at "float";
	addAttr -ci true -sn "scaleBtw" -ln "scaleBtw" -dv 2 -at "float";
	setAttr -l on ".v";
	setAttr -l on ".t";
	setAttr -l on ".r";
	setAttr ".ro" 1;
	setAttr -l on ".s";
	setAttr ".jo" -type "double3" -25.624249890094092 0 0 ;
	setAttr ".bps" -type "matrix" 0.99999981153608242 0.00024631429833993845 -0.0005623673769295668 0
		 0.00024639553846109987 -0.99999995921948293 0.00014439623037953623 0 -0.00056233178713976581 -0.00014453476797870847 -0.99999983144631677 0
		 -0.72379427782384331 0.3790740686169583 -3.4465554282051891 1;
	setAttr ".radi" 0.075000000000000011;
	setAttr ".liw" yes;
	setAttr -k on ".aimBtw";
	setAttr -k on ".scaleBtw";
createNode joint -n "ballBackProxySkinBtwEndRGT_jnt" -p "ballBackProxySkinBtwRGT_jnt";
	rename -uid "0E0260CD-4311-881B-96FE-728E243068CF";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -1.1102230246251565e-016 0.63102579539156745 0 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 0 -1.0523378412572102e-046 ;
	setAttr ".bps" -type "matrix" 0.99999981153608242 0.00024631429833993845 -0.0005623673769295668 0
		 0.00024639553846109987 -0.99999995921948293 0.00014439623037953623 0 -0.00056233178713976581 -0.00014453476797870847 -0.99999983144631677 0
		 -0.72379427782384331 0.3790740686169583 -3.4465554282051891 1;
	setAttr ".radi" 0.025;
	setAttr ".liw" yes;
createNode parentConstraint -n "ballBackProxySkinBtwRGT_jnt_parentConstraint1" -p
		 "ballBackProxySkinBtwRGT_jnt";
	rename -uid "A3439352-4B11-C93D-B7EA-2B888BB3FACF";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "ballBackProxySkinBtwIkRGT_jntW0" 
		-dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode pointConstraint -n "ballBackProxySkinBtwAllRGT_grp_pointConstraint1" -p
		 "ballBackProxySkinBtwAllRGT_grp";
	rename -uid "959E8192-41A8-BE3C-D16D-F59F3A751A9B";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "ballBackProxySkinRGT_jntW0" -dv 1 
		-min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 0 -0.48853999999999986 -0.16068000000000016 ;
	setAttr -k on ".w0";
createNode joint -n "lowLegRbnDtl1BackProxySkinRGT_jnt" -p "lowLegBackProxySkinRGT_jnt";
	rename -uid "FDD96485-471D-B040-0D97-38A001D31BEC";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -2.2204460492503131e-016 -0.19066770000000055 0.052964999999999929 ;
	setAttr ".r" -type "double3" 3.4003497244536831e-065 2.2069531490250788e-032 1.7655625192200632e-031 ;
	setAttr ".jo" -type "double3" 15.524599201625735 180 0 ;
	setAttr ".pa" -type "double3" 3.4003497244536831e-065 2.2069531490250788e-032 1.7655625192200632e-031 ;
	setAttr ".bps" -type "matrix" 0.99999165125565648 -0.00085858090734481664 0.0039950291377054284 0
		 -0.00024291455393214017 0.96345712514032333 0.26786248152524594 0 -0.0040790209002970791 -0.2678612156605894 0.96344887292130177 0
		 -0.72316593024596831 2.5835538771944302 -3.1300666370781616 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode joint -n "lowLegRbnDtl2BackProxySkinRGT_jnt" -p "lowLegBackProxySkinRGT_jnt";
	rename -uid "6A0D79B4-450E-CCA8-CBFB-6BBC5ED9AEE6";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 -0.57200310000000076 0.15889500000000067 ;
	setAttr ".r" -type "double3" 3.4003497244536831e-065 2.2069531490250788e-032 1.7655625192200632e-031 ;
	setAttr ".jo" -type "double3" 15.524599201625735 180 0 ;
	setAttr ".pa" -type "double3" 3.4003497244536831e-065 2.2069531490250788e-032 1.7655625192200632e-031 ;
	setAttr ".bps" -type "matrix" 0.99999165125565648 -0.00085858090734481664 0.0039950291377054284 0
		 -0.00024291455393214017 0.96345712514032333 0.26786248152524594 0 -0.0040790209002970791 -0.2678612156605894 0.96344887292130177 0
		 -0.72306979073790434 2.2022416315832896 -3.2360799112344867 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode joint -n "lowLegRbnDtl3BackProxySkinRGT_jnt" -p "lowLegBackProxySkinRGT_jnt";
	rename -uid "C9EFF96A-4A1C-7A9D-0B09-8AB18F0AE7D3";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -1.1102230246251565e-016 -0.95333850000000053 0.26482500000000053 ;
	setAttr ".r" -type "double3" 3.4003497244536831e-065 2.2069531490250788e-032 1.7655625192200632e-031 ;
	setAttr ".jo" -type "double3" 15.524599201625735 180 0 ;
	setAttr ".pa" -type "double3" 3.4003497244536831e-065 2.2069531490250788e-032 1.7655625192200632e-031 ;
	setAttr ".bps" -type "matrix" 0.99999165125565648 -0.00085858090734481664 0.0039950291377054284 0
		 -0.00024291455393214017 0.96345712514032333 0.26786248152524594 0 -0.0040790209002970791 -0.2678612156605894 0.96344887292130177 0
		 -0.7229736512298407 1.8209293859721476 -3.3420931853908096 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode joint -n "lowLegRbnDtl4BackProxySkinRGT_jnt" -p "lowLegBackProxySkinRGT_jnt";
	rename -uid "8D66A09C-430C-4FCE-169F-64A1A7C6087F";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 -1.3346738999999996 0.37075499999999995 ;
	setAttr ".r" -type "double3" 3.4003497244536831e-065 2.2069531490250788e-032 1.7655625192200632e-031 ;
	setAttr ".jo" -type "double3" 15.524599201625735 180 0 ;
	setAttr ".pa" -type "double3" 3.4003497244536831e-065 2.2069531490250788e-032 1.7655625192200632e-031 ;
	setAttr ".bps" -type "matrix" 0.99999165125565648 -0.00085858090734481664 0.0039950291377054284 0
		 -0.00024291455393214017 0.96345712514032333 0.26786248152524594 0 -0.0040790209002970791 -0.2678612156605894 0.96344887292130177 0
		 -0.72287751172177683 1.4396171403610065 -3.4481064595471338 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode joint -n "lowLegRbnDtl5BackProxySkinRGT_jnt" -p "lowLegBackProxySkinRGT_jnt";
	rename -uid "E66B108B-46EA-5197-12D6-EA873A7968F2";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 1.1102230246251565e-016 -1.7160093000000007 0.47668500000000025 ;
	setAttr ".r" -type "double3" 3.4003497244536831e-065 2.2069531490250788e-032 1.7655625192200632e-031 ;
	setAttr ".jo" -type "double3" 15.524599201625735 180 0 ;
	setAttr ".pa" -type "double3" 3.4003497244536831e-065 2.2069531490250788e-032 1.7655625192200632e-031 ;
	setAttr ".bps" -type "matrix" 0.99999165125565648 -0.00085858090734481664 0.0039950291377054284 0
		 -0.00024291455393214017 0.96345712514032333 0.26786248152524594 0 -0.0040790209002970791 -0.2678612156605894 0.96344887292130177 0
		 -0.72278137221371397 1.0583048947498654 -3.5541197337034576 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode transform -n "ankleBackProxySkinBtwAllRGT_grp" -p "lowLegRbnDtl5BackProxySkinRGT_jnt";
	rename -uid "E30A5155-43A1-73B5-17F8-9DA215D7E8BA";
	setAttr ".r" -type "double3" 15.524599201625735 180 0 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999978 ;
createNode transform -n "ankleBackProxySkinBtwAllMoveRGT_grp" -p "ankleBackProxySkinBtwAllRGT_grp";
	rename -uid "178CFB9E-4F71-C17E-D71D-63B3F035F104";
	setAttr ".t" -type "double3" 0 0.17177129805955357 0 ;
createNode transform -n "ankleBackProxySkinBtwHideRGT_grp" -p "ankleBackProxySkinBtwAllMoveRGT_grp";
	rename -uid "97EBC604-4E6F-6775-59E5-778C41CAD57E";
	setAttr ".v" no;
createNode transform -n "ankleBackProxySkinBtwUpRGT_grp" -p "ankleBackProxySkinBtwHideRGT_grp";
	rename -uid "D85E703F-4DFC-B05F-FC52-8C9A9A82AB1B";
createNode joint -n "ankleBackProxySkinBtwIkRGT_jnt" -p "ankleBackProxySkinBtwHideRGT_grp";
	rename -uid "02CE64CE-4A6E-CE10-3160-01A313214642";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 6.8382643460484474 0 0 ;
	setAttr ".bps" -type "matrix" -0.99999165125565648 0.00085858090734481664 -0.0039950291377053061 0
		 0.00085770643340208686 0.99999960783787922 0.00022059864425942046 0 0.0039952169727903546 0.00021717024034469839 -0.99999199550717754 0
		 -0.7227333024596817 0.86764877194429446 -3.6071263707816188 1;
	setAttr ".radi" 0.075000000000000011;
	setAttr ".liw" yes;
createNode joint -n "ankleBackProxySkinBtwEndIkRGT_jnt" -p "ankleBackProxySkinBtwIkRGT_jnt";
	rename -uid "987D987B-4562-FAE3-EBF1-C88429B11856";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -1.1102230246251565e-016 -0.34354259611910698 0 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999165125565648 0.00085858090734481664 -0.0039950291377053061 0
		 0.00085770643340208686 0.99999960783787922 0.00022059864425942046 0 0.0039952169727903546 0.00021717024034469839 -0.99999199550717754 0
		 -0.7227333024596817 0.86764877194429446 -3.6071263707816188 1;
	setAttr ".radi" 0.025;
	setAttr ".liw" yes;
createNode ikEffector -n "ankleBackProxySkinBtwEndRGT_eff" -p "ankleBackProxySkinBtwIkRGT_jnt";
	rename -uid "C9D32FEF-4ABC-9D5E-5431-78BC6E3FFFFA";
	setAttr ".v" no;
	setAttr ".hd" yes;
createNode joint -n "ankleBackProxySkinBtwAimRGT_jnt" -p "ankleBackProxySkinBtwHideRGT_grp";
	rename -uid "5C543F3C-47B7-AA54-2C0E-8A8AA5618D46";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999165125565648 0.00085858090734481664 -0.0039950291377053061 0
		 0.00085770643340208686 0.99999960783787922 0.00022059864425942046 0 0.0039952169727903546 0.00021717024034469839 -0.99999199550717754 0
		 -0.7227333024596817 0.86764877194429446 -3.6071263707816188 1;
	setAttr ".radi" 0.075000000000000011;
	setAttr ".liw" yes;
createNode joint -n "ankleBackProxySkinBtwAimEndRGT_jnt" -p "ankleBackProxySkinBtwAimRGT_jnt";
	rename -uid "3ACCE748-48D9-D766-49B9-D9A2F786F761";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 -0.34354259611910676 0 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999165125565648 0.00085858090734481664 -0.0039950291377053061 0
		 0.00085770643340208686 0.99999960783787922 0.00022059864425942046 0 0.0039952169727903546 0.00021717024034469839 -0.99999199550717754 0
		 -0.7227333024596817 0.86764877194429446 -3.6071263707816188 1;
	setAttr ".radi" 0.025;
	setAttr ".liw" yes;
createNode aimConstraint -n "ankleBackProxySkinBtwAimRGT_jnt_aimConstraint1" -p "ankleBackProxySkinBtwAimRGT_jnt";
	rename -uid "E4CB7DED-468C-7983-7DDA-779ABCDEB3FC";
	addAttr -dcb 0 -ci true -sn "w0" -ln "ballBackProxySkinRGT_jntW0" -dv 1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".a" -type "double3" 0 -1 0 ;
	setAttr ".u" -type "double3" 1 0 0 ;
	setAttr ".wut" 1;
	setAttr ".rsrr" -type "double3" 13.676528692097012 1.1224951191660284e-015 9.3603530112883047e-015 ;
	setAttr -k on ".w0";
createNode joint -n "ankleBackProxySkinBtwVoidRGT_jnt" -p "ankleBackProxySkinBtwHideRGT_grp";
	rename -uid "9A56C7BE-4899-BBCD-14B1-89932885D766";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999165125565648 0.00085858090734481664 -0.0039950291377053061 0
		 0.00085770643340208686 0.99999960783787922 0.00022059864425942046 0 0.0039952169727903546 0.00021717024034469839 -0.99999199550717754 0
		 -0.7227333024596817 0.86764877194429446 -3.6071263707816188 1;
	setAttr ".radi" 0.075000000000000011;
	setAttr ".liw" yes;
createNode joint -n "ankleBackProxySkinBtwVoidEndRGT_jnt" -p "ankleBackProxySkinBtwVoidRGT_jnt";
	rename -uid "9CE8D424-401C-26BA-90B5-66AB21F758F0";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 -0.34354259611910676 0 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999165125565648 0.00085858090734481664 -0.0039950291377053061 0
		 0.00085770643340208686 0.99999960783787922 0.00022059864425942046 0 0.0039952169727903546 0.00021717024034469839 -0.99999199550717754 0
		 -0.7227333024596817 0.86764877194429446 -3.6071263707816188 1;
	setAttr ".radi" 0.025;
	setAttr ".liw" yes;
createNode ikHandle -n "ankleBackProxySkinBtwRGT_ikHndl" -p "ankleBackProxySkinBtwHideRGT_grp";
	rename -uid "7464A691-43A7-0065-D22C-FFA2E0217DF5";
	setAttr ".r" -type "double3" 0 180.00000000000003 0 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999956 ;
	setAttr ".hs" 1;
	setAttr -l on ".pv" -type "double3" 0 0 0 ;
	setAttr -l on ".pv";
	setAttr ".roc" yes;
createNode pointConstraint -n "ankleBackProxySkinBtwRGT_ikHndl_pointConstraint1" 
		-p "ankleBackProxySkinBtwRGT_ikHndl";
	rename -uid "61BBDE36-4804-0119-4DDD-6FB58A20B884";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "ankleBackProxySkinBtwAimEndRGT_jntW0" 
		-dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "ankleBackProxySkinBtwVoidEndRGT_jntW1" 
		-dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" -0.72321399999999969 0.69576170194044651 -3.60671 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode joint -n "ankleBackProxySkinBtwRGT_jnt" -p "ankleBackProxySkinBtwAllMoveRGT_grp";
	rename -uid "56C0A185-4DDE-B211-85C9-67A5A2095564";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "aimBtw" -ln "aimBtw" -dv 0.5 -min 0 -max 1 -at "float";
	addAttr -ci true -sn "scaleBtw" -ln "scaleBtw" -dv 2 -at "float";
	setAttr -l on ".v";
	setAttr -l on ".t";
	setAttr -l on ".r";
	setAttr ".ro" 1;
	setAttr -l on ".s";
	setAttr ".jo" -type "double3" 6.8382643460484474 0 0 ;
	setAttr ".bps" -type "matrix" -0.99999165125565648 0.00085858090734481664 -0.0039950291377053061 0
		 0.00085770643340208686 0.99999960783787922 0.00022059864425942046 0 0.0039952169727903546 0.00021717024034469839 -0.99999199550717754 0
		 -0.7227333024596817 0.86764877194429446 -3.6071263707816188 1;
	setAttr ".radi" 0.075000000000000011;
	setAttr ".liw" yes;
	setAttr -k on ".aimBtw";
	setAttr -k on ".scaleBtw";
createNode joint -n "ankleBackProxySkinBtwEndRGT_jnt" -p "ankleBackProxySkinBtwRGT_jnt";
	rename -uid "45347865-4B39-39EE-8261-6A96C8468A0F";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -1.1102230246251565e-016 -0.34354259611910698 0 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999165125565648 0.00085858090734481664 -0.0039950291377053061 0
		 0.00085770643340208686 0.99999960783787922 0.00022059864425942046 0 0.0039952169727903546 0.00021717024034469839 -0.99999199550717754 0
		 -0.7227333024596817 0.86764877194429446 -3.6071263707816188 1;
	setAttr ".radi" 0.025;
	setAttr ".liw" yes;
createNode parentConstraint -n "ankleBackProxySkinBtwRGT_jnt_parentConstraint1" -p
		 "ankleBackProxySkinBtwRGT_jnt";
	rename -uid "FF4C4323-4A89-0592-214C-D4B5FEFF0813";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "ankleBackProxySkinBtwIkRGT_jntW0" 
		-dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode pointConstraint -n "ankleBackProxySkinBtwAllRGT_grp_pointConstraint1" 
		-p "ankleBackProxySkinBtwAllRGT_grp";
	rename -uid "E61F3FC7-4111-32E1-42D9-59AF07A9EFC7";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "ankleBackProxySkinRGT_jntW0" -dv 
		1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 2.2204460492503131e-016 -0.19788750099056232 0 ;
	setAttr -k on ".w0";
createNode joint -n "midLegRbnDtl1BackProxySkinRGT_jnt" -p "midLegBackProxySkinRGT_jnt";
	rename -uid "E8D8C6DC-455E-ED5D-007F-2E80E1133512";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 6.6613381477509392e-016 -0.11635198868674923 0.081748255291086203 ;
	setAttr ".jo" -type "double3" 35.091662393824947 180 0 ;
	setAttr ".pa" -type "double3" 3.1805546814635168e-014 8.8278125961003085e-032 3.5311250384401278e-031 ;
	setAttr ".bps" -type "matrix" 0.99999165125565648 -0.00085858090734481664 0.0039950291377054284 0
		 -0.0015949910268440613 0.81810821371509657 0.57506208939167436 0 -0.0037621034820774606 -0.57506366038093493 0.81810001410993194 0
		 -0.72524463147779883 3.8157683931547859 -2.3449310031878365 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode joint -n "midLegRbnDtl2BackProxySkinRGT_jnt" -p "midLegBackProxySkinRGT_jnt";
	rename -uid "B00F5DA5-4269-C868-336D-36B719A41868";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 9.9920072216264089e-016 -0.34815358416144937 0.24461075740752136 ;
	setAttr ".jo" -type "double3" 35.091662393824947 180 0 ;
	setAttr ".pa" -type "double3" 3.1805546814635168e-014 8.8278125961003085e-032 3.5311250384401278e-031 ;
	setAttr ".bps" -type "matrix" 0.99999165125565648 -0.00085858090734481664 0.0039950291377054284 0
		 -0.0015949910268440613 0.81810821371509657 0.57506208939167436 0 -0.0037621034820774606 -0.57506366038093493 0.81810001410993194 0
		 -0.72479292610842405 3.5840781412066387 -2.5077899968311685 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode joint -n "midLegRbnDtl3BackProxySkinRGT_jnt" -p "midLegBackProxySkinRGT_jnt";
	rename -uid "203ECC7E-40BF-9B37-CD0A-1CA5FADCF194";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 1.1102230246251565e-015 -0.57894000000000156 0.4067599999999989 ;
	setAttr ".jo" -type "double3" 35.091662393824947 180 0 ;
	setAttr ".pa" -type "double3" 3.1805546814635168e-014 8.8278125961003085e-032 3.5311250384401278e-031 ;
	setAttr ".bps" -type "matrix" 0.99999165125565648 -0.00085858090734481664 0.0039950291377054284 0
		 -0.0015949910268440613 0.81810821371509657 0.57506208939167436 0 -0.0037621034820774606 -0.57506366038093493 0.81810001410993194 0
		 -0.72434253389329772 3.3530614367946989 -2.670175542528396 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode joint -n "midLegRbnDtl4BackProxySkinRGT_jnt" -p "midLegBackProxySkinRGT_jnt";
	rename -uid "2D9ACA1C-4A49-A482-E3F2-ADB118AFAB21";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 9.9920072216264089e-016 -0.80972641583855332 0.56890924259247599 ;
	setAttr ".jo" -type "double3" 35.091662393824947 180 0 ;
	setAttr ".pa" -type "double3" 3.1805546814635168e-014 8.8278125961003085e-032 3.5311250384401278e-031 ;
	setAttr ".bps" -type "matrix" 0.99999165125565648 -0.00085858090734481664 0.0039950291377054284 0
		 -0.0015949910268440613 0.81810821371509657 0.57506208939167436 0 -0.0037621034820774606 -0.57506366038093493 0.81810001410993194 0
		 -0.72389214167817151 3.1220447323827605 -2.832561088225622 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode joint -n "midLegRbnDtl5BackProxySkinRGT_jnt" -p "midLegBackProxySkinRGT_jnt";
	rename -uid "7E1F403F-4541-4851-A468-4E9CE6769AF9";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 1.1102230246251565e-015 -1.0415280113132535 0.73177174470891115 ;
	setAttr ".jo" -type "double3" 35.091662393824947 180 0 ;
	setAttr ".pa" -type "double3" 3.1805546814635168e-014 8.8278125961003085e-032 3.5311250384401278e-031 ;
	setAttr ".bps" -type "matrix" 0.99999165125565648 -0.00085858090734481664 0.0039950291377054284 0
		 -0.0015949910268440613 0.81810821371509657 0.57506208939167436 0 -0.0037621034820774606 -0.57506366038093493 0.81810001410993194 0
		 -0.72344043630879729 2.8903544804346097 -2.9954200818689545 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode transform -n "lowLegBackProxySkinBtwAllRGT_grp" -p "midLegRbnDtl5BackProxySkinRGT_jnt";
	rename -uid "D6C3FEB9-4A1D-FB24-8B08-979EE9554BC7";
	setAttr ".r" -type "double3" 35.091662393824947 180 0 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999978 ;
createNode transform -n "lowLegBackProxySkinBtwAllMoveRGT_grp" -p "lowLegBackProxySkinBtwAllRGT_grp";
	rename -uid "502126CC-4800-60B3-CF1F-6DA3AE80C57F";
	setAttr ".t" -type "double3" 0 0.066094425330848153 0 ;
createNode transform -n "lowLegBackProxySkinBtwHideRGT_grp" -p "lowLegBackProxySkinBtwAllMoveRGT_grp";
	rename -uid "C65A0A68-402C-7115-05C9-D4A6D47ADE9E";
	setAttr ".v" no;
createNode transform -n "lowLegBackProxySkinBtwUpRGT_grp" -p "lowLegBackProxySkinBtwHideRGT_grp";
	rename -uid "E536ED1D-4395-5EA2-0061-8589B0728BA9";
createNode joint -n "lowLegBackProxySkinBtwIkRGT_jnt" -p "lowLegBackProxySkinBtwHideRGT_grp";
	rename -uid "2693B26B-4B40-FC2A-919F-72A22AA1EB56";
	setAttr ".t" -type "double3" 0 0 4.4408920985006262e-016 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" -5.8277562991776177 0 0 ;
	setAttr ".bps" -type "matrix" -0.99999165125565648 0.00085858090734481664 -0.0039950291377053061 0
		 0.00085770643340208686 0.99999960783787922 0.00022059864425942046 0 0.0039952169727903546 0.00021717024034469839 -0.99999199550717754 0
		 -0.72321399999999991 2.7742100000000001 -3.0770599999999986 1;
	setAttr ".radi" 0.075000000000000011;
createNode joint -n "lowLegBackProxySkinBtwEndIkRGT_jnt" -p "lowLegBackProxySkinBtwIkRGT_jnt";
	rename -uid "4999AC48-47C7-518A-4327-48BE92D7CBF2";
	setAttr ".t" -type "double3" 0 -0.1321888506616955 -4.4408920985006262e-016 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999165125565648 0.00085858090734481664 -0.0039950291377053061 0
		 0.00085770643340208686 0.99999960783787922 0.00022059864425942046 0 0.0039952169727903546 0.00021717024034469839 -0.99999199550717754 0
		 -0.72321399999999991 2.7742100000000001 -3.0770599999999986 1;
	setAttr ".radi" 0.025;
createNode ikEffector -n "lowLegBackProxySkinBtwEndRGT_eff" -p "lowLegBackProxySkinBtwIkRGT_jnt";
	rename -uid "306E2AB4-459A-9412-A6DD-D09768DAEBA2";
	setAttr ".v" no;
	setAttr ".hd" yes;
createNode joint -n "lowLegBackProxySkinBtwAimRGT_jnt" -p "lowLegBackProxySkinBtwHideRGT_grp";
	rename -uid "03C9C42A-4F45-E39B-769F-6BB1923FA9BE";
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999165125565648 0.00085858090734481664 -0.0039950291377053061 0
		 0.00085770643340208686 0.99999960783787922 0.00022059864425942046 0 0.0039952169727903546 0.00021717024034469839 -0.99999199550717754 0
		 -0.72321399999999991 2.7742100000000001 -3.0770599999999986 1;
	setAttr ".radi" 0.075000000000000011;
createNode joint -n "lowLegBackProxySkinBtwAimEndRGT_jnt" -p "lowLegBackProxySkinBtwAimRGT_jnt";
	rename -uid "B224DDB3-4598-FF61-5913-B5A70D4A618A";
	setAttr ".t" -type "double3" 0 -0.13218885066169506 0 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999165125565648 0.00085858090734481664 -0.0039950291377053061 0
		 0.00085770643340208686 0.99999960783787922 0.00022059864425942046 0 0.0039952169727903546 0.00021717024034469839 -0.99999199550717754 0
		 -0.72321399999999991 2.7742100000000001 -3.0770599999999986 1;
	setAttr ".radi" 0.025;
createNode aimConstraint -n "lowLegBackProxySkinBtwAimRGT_jnt_aimConstraint1" -p "lowLegBackProxySkinBtwAimRGT_jnt";
	rename -uid "10C7C683-40B0-CF3B-30EC-A780911A14F8";
	addAttr -dcb 0 -ci true -sn "w0" -ln "lowLegRbnDtl1BackProxySkinRGT_jntW0" -dv 
		1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".a" -type "double3" 0 -1 0 ;
	setAttr ".u" -type "double3" 1 0 0 ;
	setAttr ".wut" 1;
	setAttr ".rsrr" -type "double3" -11.655512598354761 4.9529412316878201e-015 -4.8526963147164571e-014 ;
	setAttr -k on ".w0";
createNode joint -n "lowLegBackProxySkinBtwVoidRGT_jnt" -p "lowLegBackProxySkinBtwHideRGT_grp";
	rename -uid "392F9CC7-4125-8118-2295-C6B7EC5C5578";
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999165125565648 0.00085858090734481664 -0.0039950291377053061 0
		 0.00085770643340208686 0.99999960783787922 0.00022059864425942046 0 0.0039952169727903546 0.00021717024034469839 -0.99999199550717754 0
		 -0.72321399999999991 2.7742100000000001 -3.0770599999999986 1;
	setAttr ".radi" 0.075000000000000011;
createNode joint -n "lowLegBackProxySkinBtwVoidEndRGT_jnt" -p "lowLegBackProxySkinBtwVoidRGT_jnt";
	rename -uid "C8B2C9EC-4C23-CBFE-4EC2-44BC27AD9A1F";
	setAttr ".t" -type "double3" 0 -0.13218885066169506 0 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999165125565648 0.00085858090734481664 -0.0039950291377053061 0
		 0.00085770643340208686 0.99999960783787922 0.00022059864425942046 0 0.0039952169727903546 0.00021717024034469839 -0.99999199550717754 0
		 -0.72321399999999991 2.7742100000000001 -3.0770599999999986 1;
	setAttr ".radi" 0.025;
createNode ikHandle -n "lowLegBackProxySkinBtwRGT_ikHndl" -p "lowLegBackProxySkinBtwHideRGT_grp";
	rename -uid "05CD02AC-4B55-8F69-57E2-CAABAD9ADECD";
	setAttr ".r" -type "double3" 0 180.00000000000003 0 ;
	setAttr ".hs" 1;
	setAttr -l on ".pv" -type "double3" 0 0 0 ;
	setAttr -l on ".pv";
	setAttr ".roc" yes;
createNode pointConstraint -n "lowLegBackProxySkinBtwRGT_ikHndl_pointConstraint1" 
		-p "lowLegBackProxySkinBtwRGT_ikHndl";
	rename -uid "56D5DF13-42A2-0F03-BE9F-D69117CC66F1";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "lowLegBackProxySkinBtwAimEndRGT_jntW0" 
		-dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "lowLegBackProxySkinBtwVoidEndRGT_jntW1" 
		-dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" -0.72321399999999991 2.7081155746691525 -3.077059999999999 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode joint -n "lowLegBackProxySkinBtwRGT_jnt" -p "lowLegBackProxySkinBtwAllMoveRGT_grp";
	rename -uid "3296B775-4353-BAE2-2D9B-72BDB1B44234";
	addAttr -ci true -sn "aimBtw" -ln "aimBtw" -dv 0.5 -min 0 -max 1 -at "float";
	addAttr -ci true -sn "scaleBtw" -ln "scaleBtw" -dv 2 -at "float";
	setAttr -l on ".v";
	setAttr -l on ".t";
	setAttr -l on ".r";
	setAttr ".ro" 1;
	setAttr -l on ".s";
	setAttr ".jo" -type "double3" -5.8277562991776177 0 0 ;
	setAttr ".bps" -type "matrix" -0.99999165125565648 0.00085858090734481664 -0.0039950291377053061 0
		 0.00085770643340208686 0.99999960783787922 0.00022059864425942046 0 0.0039952169727903546 0.00021717024034469839 -0.99999199550717754 0
		 -0.72321399999999991 2.7742100000000001 -3.0770599999999986 1;
	setAttr ".radi" 0.075000000000000011;
	setAttr -k on ".aimBtw";
	setAttr -k on ".scaleBtw";
createNode joint -n "lowLegBackProxySkinBtwEndRGT_jnt" -p "lowLegBackProxySkinBtwRGT_jnt";
	rename -uid "3564CBED-434D-A647-DAAD-C58D14AA3285";
	setAttr ".t" -type "double3" 0 -0.13218885066169506 -4.4408920985006262e-016 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999165125565648 0.00085858090734481664 -0.0039950291377053061 0
		 0.00085770643340208686 0.99999960783787922 0.00022059864425942046 0 0.0039952169727903546 0.00021717024034469839 -0.99999199550717754 0
		 -0.72321399999999991 2.7742100000000001 -3.0770599999999986 1;
	setAttr ".radi" 0.025;
createNode parentConstraint -n "lowLegBackProxySkinBtwRGT_jnt_parentConstraint1" 
		-p "lowLegBackProxySkinBtwRGT_jnt";
	rename -uid "AA120DC2-4DA6-7909-44C1-5DBB957E8171";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "lowLegBackProxySkinBtwIkRGT_jntW0" 
		-dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 0 0 8.8817841970012523e-016 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "lowLegBackProxySkinBtwAllRGT_grp_pointConstraint1" 
		-p "lowLegBackProxySkinBtwAllRGT_grp";
	rename -uid "50651F54-4522-D10B-41C6-9D9A1F44A6AD";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "lowLegBackProxySkinRGT_jntW0" -dv 
		1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 3.3306690738754696e-016 -0.14219902430923304 -1.7763568394002505e-015 ;
	setAttr -k on ".w0";
createNode joint -n "upLegRbnDtl1BackProxySkinRGT_jnt" -p "upLegBackProxySkinRGT_jnt";
	rename -uid "1BF2B267-49ED-0893-93F6-868FE1216DBF";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 4.4408920985006262e-016 -0.097333490763841901 -0.019274331883207818 ;
	setAttr ".r" -type "double3" 0 -8.827812596100315e-032 0 ;
	setAttr ".jo" -type "double3" 168.79899074995731 8.8278125961003194e-032 180 ;
	setAttr ".pa" -type "double3" 6.8967285907033694e-034 -9.9392333795734874e-017 -7.9513867036587899e-016 ;
	setAttr ".bps" -type "matrix" 0.99500079540812614 0.09978698088077198 0.0039969468219077436 0
		 -0.097132706198663751 0.97628416032326071 -0.19347991029672812 0 -0.023208931981476007 0.19212443037939328 0.98109609556234278 0
		 -0.85189627447721106 5.2026169606885171 -2.5151191109301325 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode joint -n "upLegRbnDtl2BackProxySkinRGT_jnt" -p "upLegBackProxySkinRGT_jnt";
	rename -uid "1200A00B-408D-A666-5425-3AB8168480A4";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 1.1102230246251565e-016 -0.35517408706937559 -0.07033286463649091 ;
	setAttr ".r" -type "double3" 0 -8.827812596100315e-032 0 ;
	setAttr ".jo" -type "double3" 168.79899074995731 8.8278125961003194e-032 180 ;
	setAttr ".pa" -type "double3" 6.8967285907033694e-034 -9.9392333795734874e-017 -7.9513867036587899e-016 ;
	setAttr ".bps" -type "matrix" 0.99500079540812614 0.09978698088077198 0.0039969468219077436 0
		 -0.097132706198663751 0.97628416032326071 -0.19347991029672812 0 -0.023208931981476007 0.19212443037939328 0.98109609556234278 0
		 -0.82622799511395095 4.9446242047382274 -2.46399012891127 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode joint -n "upLegRbnDtl3BackProxySkinRGT_jnt" -p "upLegBackProxySkinRGT_jnt";
	rename -uid "CD9945B4-49F5-F783-2BC7-27A0A772ED17";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 2.2204460492503131e-016 -0.68408499999999961 -0.13546499999999995 ;
	setAttr ".r" -type "double3" 0 -8.827812596100315e-032 0 ;
	setAttr ".jo" -type "double3" 168.79899074995731 8.8278125961003194e-032 180 ;
	setAttr ".pa" -type "double3" 6.8967285907033694e-034 -9.9392333795734874e-017 -7.9513867036587899e-016 ;
	setAttr ".bps" -type "matrix" 0.99500079540812614 0.09978698088077198 0.0039969468219077436 0
		 -0.097132706198663751 0.97628416032326071 -0.19347991029672812 0 -0.023208931981476007 0.19212443037939328 0.98109609556234278 0
		 -0.793541033893298 4.6160864367946948 -2.3988805425283934 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode joint -n "upLegRbnDtl4BackProxySkinRGT_jnt" -p "upLegBackProxySkinRGT_jnt";
	rename -uid "FD5761E6-4470-F510-11AA-789088E83992";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 1.1102230246251565e-016 -1.012995912930621 -0.20059713536350987 ;
	setAttr ".r" -type "double3" 0 -8.827812596100315e-032 0 ;
	setAttr ".jo" -type "double3" 168.79899074995731 8.8278125961003194e-032 180 ;
	setAttr ".pa" -type "double3" 6.8967285907033694e-034 -9.9392333795734874e-017 -7.9513867036587899e-016 ;
	setAttr ".bps" -type "matrix" 0.99500079540812614 0.09978698088077198 0.0039969468219077436 0
		 -0.097132706198663751 0.97628416032326071 -0.19347991029672812 0 -0.023208931981476007 0.19212443037939328 0.98109609556234278 0
		 -0.76085407267264549 4.2875486688511657 -2.3337709561455178 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode joint -n "upLegRbnDtl5BackProxySkinRGT_jnt" -p "upLegBackProxySkinRGT_jnt";
	rename -uid "A6A16F57-4F22-A3C9-2226-5289F36C37B9";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 2.2204460492503131e-016 -1.2708365092361582 -0.25165566811679296 ;
	setAttr ".r" -type "double3" 0 -8.827812596100315e-032 0 ;
	setAttr ".jo" -type "double3" 168.79899074995731 8.8278125961003194e-032 180 ;
	setAttr ".pa" -type "double3" 6.8967285907033694e-034 -9.9392333795734874e-017 -7.9513867036587899e-016 ;
	setAttr ".bps" -type "matrix" 0.99500079540812614 0.09978698088077198 0.0039969468219077436 0
		 -0.097132706198663751 0.97628416032326071 -0.19347991029672812 0 -0.023208931981476007 0.19212443037939328 0.98109609556234278 0
		 -0.73518579330938505 4.0295559129008769 -2.2826419741266557 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode transform -n "midLegBackProxySkinBtwAllRGT_grp" -p "upLegRbnDtl5BackProxySkinRGT_jnt";
	rename -uid "CC38D6DD-46B9-456D-BFFD-FBA9A1A62180";
	setAttr ".t" -type "double3" 1.1102230246251565e-015 -0.099223526916850879 2.2204460492503131e-016 ;
	setAttr ".r" -type "double3" 168.79899074995731 -1.3630072216771845e-015 180 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999967 ;
createNode transform -n "midLegBackProxySkinBtwAllMoveRGT_grp" -p "midLegBackProxySkinBtwAllRGT_grp";
	rename -uid "2B4CB854-44C1-4305-F887-1E82D0F3293C";
	setAttr ".t" -type "double3" 0 0.047494474119284043 0 ;
createNode transform -n "midLegBackProxySkinBtwHideRGT_grp" -p "midLegBackProxySkinBtwAllMoveRGT_grp";
	rename -uid "6015892F-4F8A-F400-CBEF-008F6D42E526";
	setAttr ".v" no;
createNode transform -n "midLegBackProxySkinBtwUpRGT_grp" -p "midLegBackProxySkinBtwHideRGT_grp";
	rename -uid "18220CE9-4079-D951-A240-16840D020167";
createNode joint -n "midLegBackProxySkinBtwIkRGT_jnt" -p "midLegBackProxySkinBtwHideRGT_grp";
	rename -uid "8BD2152E-4796-3FB4-F1C7-9CAF9A45BD68";
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" -13.258040043959062 0 0 ;
	setAttr ".bps" -type "matrix" -0.99999165125565648 0.00085858090734481664 -0.0039950291377053061 0
		 0.00085770643340208686 0.99999960783787922 0.00022059864425942046 0 0.0039952169727903546 0.00021717024034469839 -0.99999199550717754 0
		 -0.72547106778659631 3.931912873589396 -2.2632910850567871 1;
	setAttr ".radi" 0.075000000000000011;
createNode joint -n "midLegBackProxySkinBtwEndIkRGT_jnt" -p "midLegBackProxySkinBtwIkRGT_jnt";
	rename -uid "4FC0175A-4336-1E47-FB24-E49A9A7E47B5";
	setAttr ".t" -type "double3" -1.1102230246251565e-016 -0.094988948238566628 0 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999165125565648 0.00085858090734481664 -0.0039950291377053061 0
		 0.00085770643340208686 0.99999960783787922 0.00022059864425942046 0 0.0039952169727903546 0.00021717024034469839 -0.99999199550717754 0
		 -0.72547106778659631 3.931912873589396 -2.2632910850567871 1;
	setAttr ".radi" 0.025;
createNode ikEffector -n "midLegBackProxySkinBtwEndRGT_eff" -p "midLegBackProxySkinBtwIkRGT_jnt";
	rename -uid "BF02B2B4-4EA0-07B4-CD96-7AA514F18264";
	setAttr ".v" no;
	setAttr ".hd" yes;
createNode joint -n "midLegBackProxySkinBtwAimRGT_jnt" -p "midLegBackProxySkinBtwHideRGT_grp";
	rename -uid "D054A3C1-4506-3552-51F9-FABD8099AD6E";
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999165125565648 0.00085858090734481664 -0.0039950291377053061 0
		 0.00085770643340208686 0.99999960783787922 0.00022059864425942046 0 0.0039952169727903546 0.00021717024034469839 -0.99999199550717754 0
		 -0.72547106778659631 3.931912873589396 -2.2632910850567871 1;
	setAttr ".radi" 0.075000000000000011;
createNode joint -n "midLegBackProxySkinBtwAimEndRGT_jnt" -p "midLegBackProxySkinBtwAimRGT_jnt";
	rename -uid "1BB116E4-4060-3269-42D4-3699F750C5F5";
	setAttr ".t" -type "double3" 0 -0.094988948238566628 0 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999165125565648 0.00085858090734481664 -0.0039950291377053061 0
		 0.00085770643340208686 0.99999960783787922 0.00022059864425942046 0 0.0039952169727903546 0.00021717024034469839 -0.99999199550717754 0
		 -0.72547106778659631 3.931912873589396 -2.2632910850567871 1;
	setAttr ".radi" 0.025;
createNode aimConstraint -n "midLegBackProxySkinBtwAimRGT_jnt_aimConstraint1" -p "midLegBackProxySkinBtwAimRGT_jnt";
	rename -uid "ECE4967D-4699-BFB1-F256-D0A38B8DCF21";
	addAttr -dcb 0 -ci true -sn "w0" -ln "midLegRbnDtl1BackProxySkinRGT_jntW0" -dv 
		1 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".a" -type "double3" 0 -1 0 ;
	setAttr ".u" -type "double3" 1 0 0 ;
	setAttr ".wut" 1;
	setAttr ".rsrr" -type "double3" -26.516080087917871 -5.7296827718636788e-014 2.4317796770471211e-013 ;
	setAttr -k on ".w0";
createNode joint -n "midLegBackProxySkinBtwVoidRGT_jnt" -p "midLegBackProxySkinBtwHideRGT_grp";
	rename -uid "D0A9273C-445E-E472-1CA4-1F8E54D65748";
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999165125565648 0.00085858090734481664 -0.0039950291377053061 0
		 0.00085770643340208686 0.99999960783787922 0.00022059864425942046 0 0.0039952169727903546 0.00021717024034469839 -0.99999199550717754 0
		 -0.72547106778659631 3.931912873589396 -2.2632910850567871 1;
	setAttr ".radi" 0.075000000000000011;
createNode joint -n "midLegBackProxySkinBtwVoidEndRGT_jnt" -p "midLegBackProxySkinBtwVoidRGT_jnt";
	rename -uid "8DAA98AA-4DB1-4789-19BF-9EADDE547E46";
	setAttr ".t" -type "double3" 0 -0.094988948238566628 0 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999165125565648 0.00085858090734481664 -0.0039950291377053061 0
		 0.00085770643340208686 0.99999960783787922 0.00022059864425942046 0 0.0039952169727903546 0.00021717024034469839 -0.99999199550717754 0
		 -0.72547106778659631 3.931912873589396 -2.2632910850567871 1;
	setAttr ".radi" 0.025;
createNode ikHandle -n "midLegBackProxySkinBtwRGT_ikHndl" -p "midLegBackProxySkinBtwHideRGT_grp";
	rename -uid "65FDC565-4CE3-143A-C936-5698F515E567";
	setAttr ".r" -type "double3" 0 180 0 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999978 ;
	setAttr ".hs" 1;
	setAttr -l on ".pv" -type "double3" 0 0 0 ;
	setAttr -l on ".pv";
	setAttr ".roc" yes;
createNode pointConstraint -n "midLegBackProxySkinBtwRGT_ikHndl_pointConstraint1" 
		-p "midLegBackProxySkinBtwRGT_ikHndl";
	rename -uid "07E1F02B-4782-E863-1C77-45B9E6435D88";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "midLegBackProxySkinBtwAimEndRGT_jntW0" 
		-dv 1 -min 0 -at "double";
	addAttr -dcb 0 -ci true -k true -sn "w1" -ln "midLegBackProxySkinBtwVoidEndRGT_jntW1" 
		-dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".rst" -type "double3" -0.72321399999999914 3.8845955258807168 -2.2635399999999994 ;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode joint -n "midLegBackProxySkinBtwRGT_jnt" -p "midLegBackProxySkinBtwAllMoveRGT_grp";
	rename -uid "FF67438D-43E9-25BB-D727-0BA4289310A5";
	addAttr -ci true -sn "aimBtw" -ln "aimBtw" -dv 0.5 -min 0 -max 1 -at "float";
	addAttr -ci true -sn "scaleBtw" -ln "scaleBtw" -dv 2 -at "float";
	setAttr -l on ".v";
	setAttr -l on ".t";
	setAttr -l on ".r";
	setAttr ".ro" 1;
	setAttr -l on ".s";
	setAttr ".jo" -type "double3" -13.258040043959062 0 0 ;
	setAttr ".bps" -type "matrix" -0.99999165125565648 0.00085858090734481664 -0.0039950291377053061 0
		 0.00085770643340208686 0.99999960783787922 0.00022059864425942046 0 0.0039952169727903546 0.00021717024034469839 -0.99999199550717754 0
		 -0.72547106778659631 3.931912873589396 -2.2632910850567871 1;
	setAttr ".radi" 0.075000000000000011;
	setAttr -k on ".aimBtw";
	setAttr -k on ".scaleBtw";
createNode joint -n "midLegBackProxySkinBtwEndRGT_jnt" -p "midLegBackProxySkinBtwRGT_jnt";
	rename -uid "DAC4427F-489F-883C-FBA9-699C6E7F004C";
	setAttr ".t" -type "double3" -1.1102230246251565e-016 -0.094988948238566628 0 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999165125565648 0.00085858090734481664 -0.0039950291377053061 0
		 0.00085770643340208686 0.99999960783787922 0.00022059864425942046 0 0.0039952169727903546 0.00021717024034469839 -0.99999199550717754 0
		 -0.72547106778659631 3.931912873589396 -2.2632910850567871 1;
	setAttr ".radi" 0.025;
createNode parentConstraint -n "midLegBackProxySkinBtwRGT_jnt_parentConstraint1" 
		-p "midLegBackProxySkinBtwRGT_jnt";
	rename -uid "09585EFA-41A9-0D38-15BF-A38DBAC482E2";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "midLegBackProxySkinBtwIkRGT_jntW0" 
		-dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode joint -n "clav1ScaHipProxySkinRGT_jnt" -p "clav1HipProxySkinRGT_jnt";
	rename -uid "2DB08F3D-474D-4614-751F-219247C3F9EA";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".bps" -type "matrix" -1 0 -1.2246467991473532e-016 0 0 1 0 0 1.2246467991473532e-016 0 -1 0
		 -0.54736399999999996 5.6437099999999996 -1.7092999999999998 1;
	setAttr ".radi" 0.1;
	setAttr ".liw" yes;
createNode joint -n "tail1ProxySkin_jnt" -p "pelvisProxySkin_jnt";
	rename -uid "335FB7F3-4B82-7E0E-C86E-C29E93FDEA24";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 0.42126780863956448 -1.7889919998699453 ;
	setAttr ".r" -type "double3" -1.2722218725854067e-014 0 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 14.328398229824984 0 0 ;
	setAttr ".pa" -type "double3" -1.2722218725854067e-014 0 0 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 0.96889318915103462 0.24747926785235466 0
		 0 -0.24747926785235466 0.96889318915103462 0 0 5.8190536893905689 -2.3004671703124209 1;
	setAttr ".radi" 0.05;
	setAttr ".liw" yes;
createNode joint -n "tail2ProxySkin_jnt" -p "tail1ProxySkin_jnt";
	rename -uid "C450161F-43DB-98E9-40DA-E984EA7CF1C4";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 9.6495480509629958e-017 -1.7763568394002505e-015 -0.21738818277964 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 43.265827795912386 0 0 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 0.53591187928805195 0.84427392334357254 0
		 0 -0.84427392334357254 0.53591187928805195 0 9.6495480509629958e-017 5.8728527577046261 -2.5110931000095351 1;
	setAttr ".radi" 0.05;
	setAttr ".liw" yes;
createNode joint -n "tail3ProxySkin_jnt" -p "tail2ProxySkin_jnt";
	rename -uid "7059A80E-471F-A233-2BD1-BCA6B91DA70A";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -6.0117103183496953e-017 1.9984014443252818e-015 -0.71994257197189082 ;
	setAttr ".r" -type "double3" 1.9083328088781101e-014 -1.220970436077321e-014 6.9180891106647965e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -60.463858257183475 0 180 ;
	setAttr ".pa" -type "double3" 1.9083328088781101e-014 -1.220970436077321e-014 6.9180891106647965e-015 ;
	setAttr ".bps" -type "matrix" -1 6.5630276759515564e-017 1.0339373578262842e-016 0
		 -6.0371716463733537e-017 0.47036642009202484 -0.88247120681063174 0 -1.0654977089530105e-016 -0.88247120681063185 -0.47036642009202478 0
		 3.6378377326132981e-017 6.4806814975253975 -2.8969188767344636 1;
	setAttr ".radi" 0.05;
	setAttr ".liw" yes;
createNode joint -n "tail4ProxySkin_jnt" -p "tail3ProxySkin_jnt";
	rename -uid "7A335CF4-4D34-822D-6977-D59A4667429A";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 1.6326406433464165e-016 -1.7763568394002505e-015 -0.51209314355052449 ;
	setAttr ".r" -type "double3" -5.0888874903416268e-014 0 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -41.82061684870645 0 0 ;
	setAttr ".pa" -type "double3" -5.0888874903416268e-014 0 0 ;
	setAttr ".bps" -type "matrix" -1 6.5630276759515564e-017 1.0339373578262842e-016 0
		 2.6056275715668317e-017 0.93896643674507729 -0.34400876539159975 0 -1.1966063814992651e-016 -0.34400876539159975 -0.93896643674507729 0
		 -7.2322279886145831e-017 6.9325889519138784 -2.6560474580489286 1;
	setAttr ".radi" 0.05;
	setAttr ".liw" yes;
createNode joint -n "tail5ProxySkin_jnt" -p "tail4ProxySkin_jnt";
	rename -uid "8EFDC72A-456C-5F78-7E9B-B6B3F16F83BC";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 1.3667267177591441e-016 0 -0.48397667963006219 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" -1 6.5630276759515564e-017 1.0339373578262842e-016 0
		 2.6056275715668317e-017 0.93896643674507729 -0.34400876539159975 0 -1.1966063814992651e-016 -0.34400876539159975 -0.93896643674507729 0
		 -1.5108199332784396e-016 7.0990811719517435 -2.2016095997089753 1;
	setAttr ".radi" 0.05;
	setAttr ".liw" yes;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "5E58CD63-4814-8FAC-206E-CE8CDBBF8031";
	setAttr -s 9 ".lnk";
	setAttr -s 9 ".slnk";
createNode displayLayerManager -n "layerManager";
	rename -uid "B5FD8805-40FB-CF2A-D223-109E4068ED03";
	setAttr ".cdl" 2;
	setAttr -s 2 ".dli[1]"  2;
createNode displayLayer -n "defaultLayer";
	rename -uid "C9F67B03-471F-2F48-18CC-B992DAB9CC5C";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "DCF82791-406C-A255-13D2-CEA5D16BAE67";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "0ED80896-4455-FBD0-1035-9593ED90E5FC";
	setAttr ".g" yes;
createNode shadingEngine -n "lambert2SG";
	rename -uid "5887D62D-492B-A952-40BE-7DA43A9D0391";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode shadingEngine -n "blinn1SG";
	rename -uid "ECFDAD9F-408F-AA5B-DA83-07B0075FA606";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode shadingEngine -n "blinn2SG";
	rename -uid "6B734BF2-4BE1-F896-589E-95903D2823F9";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode shadingEngine -n "blinn3SG";
	rename -uid "E5E6AA43-47F1-F662-9B50-F88521D70F4C";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode shadingEngine -n "blinn4SG";
	rename -uid "70CE1A20-496D-C0A5-600B-FCBB64D28BAD";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode shadingEngine -n "blinn5SG";
	rename -uid "02FEB926-419B-C892-21A5-92A6CB48579F";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode shadingEngine -n "lambert3SG";
	rename -uid "16568FAC-4477-BCA4-E1CC-70B74AB21706";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode ilrOptionsNode -s -n "TurtleRenderOptions";
	rename -uid "12AEC8EA-43A9-DE3C-0DFB-359FE675BB95";
lockNode -l 1 ;
createNode ilrUIOptionsNode -s -n "TurtleUIOptions";
	rename -uid "151C06CA-4610-EA21-C06B-1CBB2BF5752F";
lockNode -l 1 ;
createNode ilrBakeLayerManager -s -n "TurtleBakeLayerManager";
	rename -uid "628A5ADC-41EF-2A7C-DB4F-A2BC926198DE";
lockNode -l 1 ;
createNode ilrBakeLayer -s -n "TurtleDefaultBakeLayer";
	rename -uid "B4FE7F0C-4317-7A38-A4CB-E69D8887A3D5";
lockNode -l 1 ;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "10FB8B86-4894-A8F2-7155-A6A55E6355CB";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 24 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode mentalrayItemsList -s -n "mentalrayItemsList";
	rename -uid "4FDF5A7D-420B-3C46-073A-7CAE479D507B";
createNode mentalrayGlobals -s -n "mentalrayGlobals";
	rename -uid "850AE90C-4621-F3A8-E779-C88AAC79D4E3";
createNode mentalrayOptions -s -n "miDefaultOptions";
	rename -uid "B12598BA-41A1-33FF-C9AA-F8A632013979";
	addAttr -ci true -m -sn "stringOptions" -ln "stringOptions" -at "compound" -nc 
		3;
	addAttr -ci true -sn "name" -ln "name" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "value" -ln "value" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "type" -ln "type" -dt "string" -p "stringOptions";
	setAttr -s 82 ".stringOptions";
	setAttr ".stringOptions[0].name" -type "string" "rast motion factor";
	setAttr ".stringOptions[0].value" -type "string" "1.0";
	setAttr ".stringOptions[0].type" -type "string" "scalar";
	setAttr ".stringOptions[1].name" -type "string" "rast transparency depth";
	setAttr ".stringOptions[1].value" -type "string" "8";
	setAttr ".stringOptions[1].type" -type "string" "integer";
	setAttr ".stringOptions[2].name" -type "string" "rast useopacity";
	setAttr ".stringOptions[2].value" -type "string" "true";
	setAttr ".stringOptions[2].type" -type "string" "boolean";
	setAttr ".stringOptions[3].name" -type "string" "importon";
	setAttr ".stringOptions[3].value" -type "string" "false";
	setAttr ".stringOptions[3].type" -type "string" "boolean";
	setAttr ".stringOptions[4].name" -type "string" "importon density";
	setAttr ".stringOptions[4].value" -type "string" "1.0";
	setAttr ".stringOptions[4].type" -type "string" "scalar";
	setAttr ".stringOptions[5].name" -type "string" "importon merge";
	setAttr ".stringOptions[5].value" -type "string" "0.0";
	setAttr ".stringOptions[5].type" -type "string" "scalar";
	setAttr ".stringOptions[6].name" -type "string" "importon trace depth";
	setAttr ".stringOptions[6].value" -type "string" "0";
	setAttr ".stringOptions[6].type" -type "string" "integer";
	setAttr ".stringOptions[7].name" -type "string" "importon traverse";
	setAttr ".stringOptions[7].value" -type "string" "true";
	setAttr ".stringOptions[7].type" -type "string" "boolean";
	setAttr ".stringOptions[8].name" -type "string" "shadowmap pixel samples";
	setAttr ".stringOptions[8].value" -type "string" "3";
	setAttr ".stringOptions[8].type" -type "string" "integer";
	setAttr ".stringOptions[9].name" -type "string" "ambient occlusion";
	setAttr ".stringOptions[9].value" -type "string" "false";
	setAttr ".stringOptions[9].type" -type "string" "boolean";
	setAttr ".stringOptions[10].name" -type "string" "ambient occlusion rays";
	setAttr ".stringOptions[10].value" -type "string" "64";
	setAttr ".stringOptions[10].type" -type "string" "integer";
	setAttr ".stringOptions[11].name" -type "string" "ambient occlusion cache";
	setAttr ".stringOptions[11].value" -type "string" "false";
	setAttr ".stringOptions[11].type" -type "string" "boolean";
	setAttr ".stringOptions[12].name" -type "string" "ambient occlusion cache density";
	setAttr ".stringOptions[12].value" -type "string" "1.0";
	setAttr ".stringOptions[12].type" -type "string" "scalar";
	setAttr ".stringOptions[13].name" -type "string" "ambient occlusion cache points";
	setAttr ".stringOptions[13].value" -type "string" "64";
	setAttr ".stringOptions[13].type" -type "string" "integer";
	setAttr ".stringOptions[14].name" -type "string" "irradiance particles";
	setAttr ".stringOptions[14].value" -type "string" "false";
	setAttr ".stringOptions[14].type" -type "string" "boolean";
	setAttr ".stringOptions[15].name" -type "string" "irradiance particles rays";
	setAttr ".stringOptions[15].value" -type "string" "256";
	setAttr ".stringOptions[15].type" -type "string" "integer";
	setAttr ".stringOptions[16].name" -type "string" "irradiance particles interpolate";
	setAttr ".stringOptions[16].value" -type "string" "1";
	setAttr ".stringOptions[16].type" -type "string" "integer";
	setAttr ".stringOptions[17].name" -type "string" "irradiance particles interppoints";
	setAttr ".stringOptions[17].value" -type "string" "64";
	setAttr ".stringOptions[17].type" -type "string" "integer";
	setAttr ".stringOptions[18].name" -type "string" "irradiance particles indirect passes";
	setAttr ".stringOptions[18].value" -type "string" "0";
	setAttr ".stringOptions[18].type" -type "string" "integer";
	setAttr ".stringOptions[19].name" -type "string" "irradiance particles scale";
	setAttr ".stringOptions[19].value" -type "string" "1.0";
	setAttr ".stringOptions[19].type" -type "string" "scalar";
	setAttr ".stringOptions[20].name" -type "string" "irradiance particles env";
	setAttr ".stringOptions[20].value" -type "string" "true";
	setAttr ".stringOptions[20].type" -type "string" "boolean";
	setAttr ".stringOptions[21].name" -type "string" "irradiance particles env rays";
	setAttr ".stringOptions[21].value" -type "string" "256";
	setAttr ".stringOptions[21].type" -type "string" "integer";
	setAttr ".stringOptions[22].name" -type "string" "irradiance particles env scale";
	setAttr ".stringOptions[22].value" -type "string" "1";
	setAttr ".stringOptions[22].type" -type "string" "integer";
	setAttr ".stringOptions[23].name" -type "string" "irradiance particles rebuild";
	setAttr ".stringOptions[23].value" -type "string" "true";
	setAttr ".stringOptions[23].type" -type "string" "boolean";
	setAttr ".stringOptions[24].name" -type "string" "irradiance particles file";
	setAttr ".stringOptions[24].value" -type "string" "";
	setAttr ".stringOptions[24].type" -type "string" "string";
	setAttr ".stringOptions[25].name" -type "string" "geom displace motion factor";
	setAttr ".stringOptions[25].value" -type "string" "1.0";
	setAttr ".stringOptions[25].type" -type "string" "scalar";
	setAttr ".stringOptions[26].name" -type "string" "contrast all buffers";
	setAttr ".stringOptions[26].value" -type "string" "true";
	setAttr ".stringOptions[26].type" -type "string" "boolean";
	setAttr ".stringOptions[27].name" -type "string" "finalgather normal tolerance";
	setAttr ".stringOptions[27].value" -type "string" "25.842";
	setAttr ".stringOptions[27].type" -type "string" "scalar";
	setAttr ".stringOptions[28].name" -type "string" "trace camera clip";
	setAttr ".stringOptions[28].value" -type "string" "false";
	setAttr ".stringOptions[28].type" -type "string" "boolean";
	setAttr ".stringOptions[29].name" -type "string" "unified sampling";
	setAttr ".stringOptions[29].value" -type "string" "true";
	setAttr ".stringOptions[29].type" -type "string" "boolean";
	setAttr ".stringOptions[30].name" -type "string" "samples quality";
	setAttr ".stringOptions[30].value" -type "string" "0.25 0.25 0.25 0.25";
	setAttr ".stringOptions[30].type" -type "string" "color";
	setAttr ".stringOptions[31].name" -type "string" "samples min";
	setAttr ".stringOptions[31].value" -type "string" "1.0";
	setAttr ".stringOptions[31].type" -type "string" "scalar";
	setAttr ".stringOptions[32].name" -type "string" "samples max";
	setAttr ".stringOptions[32].value" -type "string" "100.0";
	setAttr ".stringOptions[32].type" -type "string" "scalar";
	setAttr ".stringOptions[33].name" -type "string" "samples error cutoff";
	setAttr ".stringOptions[33].value" -type "string" "0.0 0.0 0.0 0.0";
	setAttr ".stringOptions[33].type" -type "string" "color";
	setAttr ".stringOptions[34].name" -type "string" "samples per object";
	setAttr ".stringOptions[34].value" -type "string" "false";
	setAttr ".stringOptions[34].type" -type "string" "boolean";
	setAttr ".stringOptions[35].name" -type "string" "progressive";
	setAttr ".stringOptions[35].value" -type "string" "false";
	setAttr ".stringOptions[35].type" -type "string" "boolean";
	setAttr ".stringOptions[36].name" -type "string" "progressive max time";
	setAttr ".stringOptions[36].value" -type "string" "0";
	setAttr ".stringOptions[36].type" -type "string" "integer";
	setAttr ".stringOptions[37].name" -type "string" "progressive subsampling size";
	setAttr ".stringOptions[37].value" -type "string" "4";
	setAttr ".stringOptions[37].type" -type "string" "integer";
	setAttr ".stringOptions[38].name" -type "string" "iray";
	setAttr ".stringOptions[38].value" -type "string" "false";
	setAttr ".stringOptions[38].type" -type "string" "boolean";
	setAttr ".stringOptions[39].name" -type "string" "light relative scale";
	setAttr ".stringOptions[39].value" -type "string" "0.31831";
	setAttr ".stringOptions[39].type" -type "string" "scalar";
	setAttr ".stringOptions[40].name" -type "string" "trace camera motion vectors";
	setAttr ".stringOptions[40].value" -type "string" "false";
	setAttr ".stringOptions[40].type" -type "string" "boolean";
	setAttr ".stringOptions[41].name" -type "string" "ray differentials";
	setAttr ".stringOptions[41].value" -type "string" "true";
	setAttr ".stringOptions[41].type" -type "string" "boolean";
	setAttr ".stringOptions[42].name" -type "string" "environment lighting mode";
	setAttr ".stringOptions[42].value" -type "string" "off";
	setAttr ".stringOptions[42].type" -type "string" "string";
	setAttr ".stringOptions[43].name" -type "string" "environment lighting quality";
	setAttr ".stringOptions[43].value" -type "string" "0.2";
	setAttr ".stringOptions[43].type" -type "string" "scalar";
	setAttr ".stringOptions[44].name" -type "string" "environment lighting shadow";
	setAttr ".stringOptions[44].value" -type "string" "transparent";
	setAttr ".stringOptions[44].type" -type "string" "string";
	setAttr ".stringOptions[45].name" -type "string" "environment lighting resolution";
	setAttr ".stringOptions[45].value" -type "string" "512";
	setAttr ".stringOptions[45].type" -type "string" "integer";
	setAttr ".stringOptions[46].name" -type "string" "environment lighting shader samples";
	setAttr ".stringOptions[46].value" -type "string" "2";
	setAttr ".stringOptions[46].type" -type "string" "integer";
	setAttr ".stringOptions[47].name" -type "string" "environment lighting scale";
	setAttr ".stringOptions[47].value" -type "string" "1 1 1";
	setAttr ".stringOptions[47].type" -type "string" "color";
	setAttr ".stringOptions[48].name" -type "string" "environment lighting caustic photons";
	setAttr ".stringOptions[48].value" -type "string" "0";
	setAttr ".stringOptions[48].type" -type "string" "integer";
	setAttr ".stringOptions[49].name" -type "string" "environment lighting global illum photons";
	setAttr ".stringOptions[49].value" -type "string" "0";
	setAttr ".stringOptions[49].type" -type "string" "integer";
	setAttr ".stringOptions[50].name" -type "string" "light importance sampling";
	setAttr ".stringOptions[50].value" -type "string" "all";
	setAttr ".stringOptions[50].type" -type "string" "string";
	setAttr ".stringOptions[51].name" -type "string" "light importance sampling quality";
	setAttr ".stringOptions[51].value" -type "string" "1.0";
	setAttr ".stringOptions[51].type" -type "string" "scalar";
	setAttr ".stringOptions[52].name" -type "string" "light importance sampling samples";
	setAttr ".stringOptions[52].value" -type "string" "4";
	setAttr ".stringOptions[52].type" -type "string" "integer";
	setAttr ".stringOptions[53].name" -type "string" "light importance sampling resolution";
	setAttr ".stringOptions[53].value" -type "string" "1.0";
	setAttr ".stringOptions[53].type" -type "string" "scalar";
	setAttr ".stringOptions[54].name" -type "string" "light importance sampling precomputed";
	setAttr ".stringOptions[54].value" -type "string" "false";
	setAttr ".stringOptions[54].type" -type "string" "boolean";
	setAttr ".stringOptions[55].name" -type "string" "mila quality";
	setAttr ".stringOptions[55].value" -type "string" "1.0";
	setAttr ".stringOptions[55].type" -type "string" "scalar";
	setAttr ".stringOptions[56].name" -type "string" "mila glossy quality";
	setAttr ".stringOptions[56].value" -type "string" "1.0";
	setAttr ".stringOptions[56].type" -type "string" "scalar";
	setAttr ".stringOptions[57].name" -type "string" "mila scatter quality";
	setAttr ".stringOptions[57].value" -type "string" "1.0";
	setAttr ".stringOptions[57].type" -type "string" "scalar";
	setAttr ".stringOptions[58].name" -type "string" "mila scatter scale";
	setAttr ".stringOptions[58].value" -type "string" "1.0";
	setAttr ".stringOptions[58].type" -type "string" "scalar";
	setAttr ".stringOptions[59].name" -type "string" "mila diffuse quality";
	setAttr ".stringOptions[59].value" -type "string" "1.0";
	setAttr ".stringOptions[59].type" -type "string" "scalar";
	setAttr ".stringOptions[60].name" -type "string" "mila diffuse detail";
	setAttr ".stringOptions[60].value" -type "string" "false";
	setAttr ".stringOptions[60].type" -type "string" "boolean";
	setAttr ".stringOptions[61].name" -type "string" "mila diffuse detail distance";
	setAttr ".stringOptions[61].value" -type "string" "10.0";
	setAttr ".stringOptions[61].type" -type "string" "scalar";
	setAttr ".stringOptions[62].name" -type "string" "mila use max distance inside";
	setAttr ".stringOptions[62].value" -type "string" "true";
	setAttr ".stringOptions[62].type" -type "string" "boolean";
	setAttr ".stringOptions[63].name" -type "string" "mila clamp output";
	setAttr ".stringOptions[63].value" -type "string" "false";
	setAttr ".stringOptions[63].type" -type "string" "boolean";
	setAttr ".stringOptions[64].name" -type "string" "mila clamp level";
	setAttr ".stringOptions[64].value" -type "string" "1.0";
	setAttr ".stringOptions[64].type" -type "string" "scalar";
	setAttr ".stringOptions[65].name" -type "string" "gi gpu";
	setAttr ".stringOptions[65].value" -type "string" "off";
	setAttr ".stringOptions[65].type" -type "string" "string";
	setAttr ".stringOptions[66].name" -type "string" "gi gpu rays";
	setAttr ".stringOptions[66].value" -type "string" "34";
	setAttr ".stringOptions[66].type" -type "string" "integer";
	setAttr ".stringOptions[67].name" -type "string" "gi gpu passes";
	setAttr ".stringOptions[67].value" -type "string" "4";
	setAttr ".stringOptions[67].type" -type "string" "integer";
	setAttr ".stringOptions[68].name" -type "string" "gi gpu presample density";
	setAttr ".stringOptions[68].value" -type "string" "1.0";
	setAttr ".stringOptions[68].type" -type "string" "scalar";
	setAttr ".stringOptions[69].name" -type "string" "gi gpu presample depth";
	setAttr ".stringOptions[69].value" -type "string" "2";
	setAttr ".stringOptions[69].type" -type "string" "integer";
	setAttr ".stringOptions[70].name" -type "string" "gi gpu filter";
	setAttr ".stringOptions[70].value" -type "string" "1.0";
	setAttr ".stringOptions[70].type" -type "string" "integer";
	setAttr ".stringOptions[71].name" -type "string" "gi gpu depth";
	setAttr ".stringOptions[71].value" -type "string" "3";
	setAttr ".stringOptions[71].type" -type "string" "integer";
	setAttr ".stringOptions[72].name" -type "string" "gi gpu devices";
	setAttr ".stringOptions[72].value" -type "string" "0";
	setAttr ".stringOptions[72].type" -type "string" "integer";
	setAttr ".stringOptions[73].name" -type "string" "shutter shape function";
	setAttr ".stringOptions[73].value" -type "string" "none";
	setAttr ".stringOptions[73].type" -type "string" "string";
	setAttr ".stringOptions[74].name" -type "string" "shutter full open";
	setAttr ".stringOptions[74].value" -type "string" "0.2";
	setAttr ".stringOptions[74].type" -type "string" "scalar";
	setAttr ".stringOptions[75].name" -type "string" "shutter full close";
	setAttr ".stringOptions[75].value" -type "string" "0.8";
	setAttr ".stringOptions[75].type" -type "string" "scalar";
	setAttr ".stringOptions[76].name" -type "string" "gi";
	setAttr ".stringOptions[76].value" -type "string" "off";
	setAttr ".stringOptions[76].type" -type "string" "boolean";
	setAttr ".stringOptions[77].name" -type "string" "gi rays";
	setAttr ".stringOptions[77].value" -type "string" "100";
	setAttr ".stringOptions[77].type" -type "string" "integer";
	setAttr ".stringOptions[78].name" -type "string" "gi depth";
	setAttr ".stringOptions[78].value" -type "string" "0";
	setAttr ".stringOptions[78].type" -type "string" "integer";
	setAttr ".stringOptions[79].name" -type "string" "gi freeze";
	setAttr ".stringOptions[79].value" -type "string" "off";
	setAttr ".stringOptions[79].type" -type "string" "boolean";
	setAttr ".stringOptions[80].name" -type "string" "gi filter";
	setAttr ".stringOptions[80].value" -type "string" "1.0";
	setAttr ".stringOptions[80].type" -type "string" "scalar";
	setAttr ".stringOptions[81].name" -type "string" "environment lighting globillum photons";
	setAttr ".stringOptions[81].value" -type "string" "0";
	setAttr ".stringOptions[81].type" -type "string" "integer";
createNode mentalrayFramebuffer -s -n "miDefaultFramebuffer";
	rename -uid "B43471F5-4641-D655-386A-48A0A79F3138";
createNode VRaySettingsNode -s -n "vraySettings";
	rename -uid "6B97E5D0-4130-2C5A-335A-7A92EBF1BD83";
	setAttr ".cfile" -type "string" "";
	setAttr ".cfile2" -type "string" "";
	setAttr ".casf" -type "string" "";
	setAttr ".casf2" -type "string" "";
	setAttr ".msr" 2;
	setAttr ".aaft" 6;
	setAttr ".dma" 3;
	setAttr ".fsz" 0.06;
	setAttr ".pft" no;
	setAttr ".fnm" -type "string" "";
	setAttr ".lcfnm" -type "string" "";
	setAttr ".asf" -type "string" "";
	setAttr ".lcasf" -type "string" "";
	setAttr ".icits" 10;
	setAttr ".ifile" -type "string" "";
	setAttr ".ifile2" -type "string" "";
	setAttr ".iasf" -type "string" "";
	setAttr ".iasf2" -type "string" "";
	setAttr ".ofn" -type "string" "";
	setAttr ".pmfile" -type "string" "";
	setAttr ".pmfile2" -type "string" "";
	setAttr ".pmasf" -type "string" "";
	setAttr ".pmasf2" -type "string" "";
	setAttr ".dmcsaa" 0.367089;
	setAttr ".dmcsat" 0.2;
	setAttr ".dmcsams" 7;
	setAttr ".cmtp" 6;
	setAttr ".cmco" yes;
	setAttr ".cmcl" 5;
	setAttr ".cmao" 1;
	setAttr ".cmlw" yes;
	setAttr ".cg" 2.2000000476837158;
	setAttr ".cmas" yes;
	setAttr ".caet1" -type "float3" 0.50000799 0.50000799 0.50000799 ;
	setAttr ".caet3" -type "float3" 1 1 1 ;
	setAttr ".caoet" yes;
	setAttr ".vrscfile" -type "string" "";
	setAttr ".mtah" yes;
	setAttr ".mpp" -type "string" "";
	setAttr ".ddms" 2;
	setAttr ".srml" 60;
	setAttr ".srdml" 14400;
	setAttr ".srdfgt" 1;
	setAttr ".srgx" 16;
	setAttr ".srgy" 16;
	setAttr ".smth" 90;
	setAttr ".seu" yes;
	setAttr ".gorvs" yes;
	setAttr ".golddl" no;
	setAttr ".goldhl" 0;
	setAttr ".gormio" yes;
	setAttr ".wi" 1280;
	setAttr ".he" 720;
	setAttr ".aspr" 1.7777777910232544;
	setAttr ".fnprx" -type "string" "<Scene>";
	setAttr ".defd" -type "string" "";
	setAttr ".fnes" -type "string" "vraySettings.";
	setAttr ".exratr" -type "string" "";
	setAttr ".jpegq" 100;
	setAttr ".animtp" 1;
	setAttr ".animbo" yes;
	setAttr ".imgfs" -type "string" "exr (multichannel)";
	setAttr ".bkc" -type "string" "map1";
	setAttr ".vfbOn" yes;
	setAttr ".vfbSA" -type "Int32Array" 251 998 14 239 162 1316 814
		 1568 205 10 205 8471617 1 60 205 1 551 251 189
		 73 453 -858993459 1071353036 -95443718 1071009700 1717986918 1069737574 1240768330 1069151391 886 1
		 3 1 0 0 0 0 1 0 5 0 1065353216 3
		 1 0 0 0 0 1 0 5 0 1065353216 3 1
		 1065353216 0 0 0 1 0 5 0 1065353216 1 3 2
		 1065353216 1065353216 1065353216 1065353216 1 0 5 0 0 0 0 1
		 0 5 0 1065353216 1 137531 65536 1 1313131313 65536 944879383 0
		 -525502228 1065353216 1621981420 1034147594 1053609164 1065353216 2 0 0 -1097805629 -1097805629 1049678019
		 1049678019 0 2 1065353216 1065353216 -1097805629 -1097805629 1049678019 1049678019 0 2 1
		 2 -1 0 0 0 1869111636 24941 21432696 0 0 0 16
		 0 0 8 1343986960 0 1 0 1060818944 0 1060819007 1065353216 1065353216
		 0 99670469 0 99670457 0 21432696 16777215 0 70 1 32 53
		 1632775510 1868963961 1632444530 622879097 2036429430 1936876918 544108393 1701978236 1919247470 1835627552 1915035749 1701080677
		 1835627634 12901 1378702848 1713404257 1293972079 543258977 808529459 540094510 1701978236 1919247470 1835627552 807411813
		 807411816 858857581 7550766 16777216 16777216 0 0 0 0 1 1 0
		 0 0 0 1 1 0 0 11 1936614732 1701209669 7566435 1
		 0 1 0 1101004800 1101004800 1082130432 0 0 0 1077936128 0 0
		 0 1 0 1 1112014848 1101004800 1 0 0 0 0 82176
		 0 16576 0 0 0 0 16448 0 65536 65536 0 0
		 0 65536 0 0 0 0 0 0 0 0 0 0
		 0 0 65536 536870912 536888779 ;
	setAttr ".sRGBOn" yes;
	setAttr ".reur" yes;
	setAttr ".mSceneName" -type "string" "P:/Lego_FRDCG/asset/3D/character/animal/frd_deerWhite/rig/rig_md/scenes/frd_deerWhite_rig_proxySkin.ma";
	setAttr ".shMode" 0;
	setAttr ".shFileName" -type "string" "";
	setAttr ".shFileFormat" 0;
	setAttr ".shAdaptiveThresh" 0.10000000149011612;
	setAttr ".shAdaptiveEdge" 1;
	setAttr ".shrFileName" -type "string" "";
	setAttr ".rt_traceDepth" 3;
	setAttr ".rt_giDepth" 1;
	setAttr ".rt_maxNoise" 0.0010000000474974513;
createNode renderGlobals -s -n "swiffworksRenderGlobals";
	rename -uid "46237FF8-4F67-4E0B-773A-2E86A9150FF5";
	addAttr -ci true -sn "rm" -ln "renderMode" -at "short";
	addAttr -ci true -sn "twos" -ln "twoSided" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "ctc" -ln "convertToCurves" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "cqlty" -ln "curveQuality" -at "short";
	addAttr -ci true -sn "inttype" -ln "intersType" -at "short";
	addAttr -ci true -sn "dlt" -ln "defaultLineThickness" -dv 0.2 -at "float";
	addAttr -ci true -sn "dlo" -ln "defaultLineOpacity" -dv 1 -at "float";
	addAttr -ci true -sn "dlcr" -ln "defaultLineColorR" -at "float";
	addAttr -ci true -sn "dlcg" -ln "defaultLineColorG" -at "float";
	addAttr -ci true -sn "dlcb" -ln "defaultLineColorB" -at "float";
	addAttr -ci true -sn "bw" -ln "bandwidth" -dv 14400 -at "float";
	addAttr -ci true -sn "oswg" -ln "onlySWG" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "fa" -ln "defaultFoldAngle" -dv 45 -at "float";
	addAttr -ci true -sn "df" -ln "defaultDisplayFolds" -at "short";
createNode renderGlobals -s -n "inkPaintRenderGlobals";
	rename -uid "D7EA744F-486C-35D3-80F9-049640D3904C";
	addAttr -ci true -sn "regMode" -ln "regionMode" -at "short";
	addAttr -ci true -sn "dpi" -ln "dpiValue" -dv 100 -at "short";
createNode renderGlobals -s -n "inkworksRenderGlobals";
	rename -uid "8788FEF5-4FDD-6F2E-4422-5D990A1EE61D";
	addAttr -ci true -sn "bb" -ln "blendBackgr" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "rm" -ln "renderMode" -at "short";
	addAttr -ci true -sn "as" -ln "adaptiveSubdv" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "twos" -ln "twoSided" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "oiw" -ln "onlyIW" -min 0 -max 1 -at "bool";
createNode mentalrayOptions -s -n "miContourPreset";
	rename -uid "56206B09-435E-3C04-DC4B-758391EAD298";
	setAttr ".splck" yes;
	setAttr ".fil" 0;
	setAttr ".rflr" 1;
	setAttr ".rfrr" 1;
	setAttr ".maxr" 1;
	setAttr ".shrd" 2;
createNode mentalrayOptions -s -n "Draft";
	rename -uid "46CBC6FE-4781-F19F-D6FE-74A60E017D0C";
	setAttr ".splck" yes;
	setAttr ".fil" 0;
	setAttr ".rflr" 1;
	setAttr ".rfrr" 1;
	setAttr ".maxr" 2;
	setAttr ".shrd" 2;
createNode mentalrayOptions -s -n "DraftMotionBlur";
	rename -uid "4E050AC8-4A8E-A91B-31A2-8BB0D9F95CE9";
	setAttr ".splck" yes;
	setAttr ".fil" 0;
	setAttr ".rflr" 1;
	setAttr ".rfrr" 1;
	setAttr ".maxr" 2;
	setAttr ".shrd" 2;
	setAttr ".mb" 1;
	setAttr ".tconr" 1;
	setAttr ".tcong" 1;
	setAttr ".tconb" 1;
	setAttr ".tcona" 1;
createNode mentalrayOptions -s -n "DraftRapidMotion";
	rename -uid "333FE2DA-4A77-311A-D6DB-81AB239C0139";
	setAttr ".splck" yes;
	setAttr ".fil" 0;
	setAttr ".scan" 3;
	setAttr ".rapc" 1;
	setAttr ".raps" 0.25;
	setAttr ".rflr" 1;
	setAttr ".rfrr" 1;
	setAttr ".maxr" 2;
	setAttr ".shrd" 2;
	setAttr ".mb" 1;
	setAttr ".tconr" 1;
	setAttr ".tcong" 1;
	setAttr ".tconb" 1;
	setAttr ".tcona" 1;
createNode mentalrayOptions -s -n "Preview";
	rename -uid "270F3079-45D4-8E7C-A5F9-029F11F0203F";
	setAttr ".splck" yes;
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".shrd" 2;
createNode mentalrayOptions -s -n "PreviewMotionblur";
	rename -uid "9E473CA7-4773-05AC-0130-6EB42617A986";
	setAttr ".splck" yes;
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".shrd" 2;
	setAttr ".mb" 1;
	setAttr ".tconr" 0.5;
	setAttr ".tcong" 0.5;
	setAttr ".tconb" 0.5;
	setAttr ".tcona" 0.5;
createNode mentalrayOptions -s -n "PreviewRapidMotion";
	rename -uid "4049B1AE-4CB8-211A-1A09-0482636FE1C1";
	setAttr ".splck" yes;
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".scan" 3;
	setAttr ".rapc" 3;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".shrd" 2;
	setAttr ".mb" 1;
	setAttr ".tconr" 0.5;
	setAttr ".tcong" 0.5;
	setAttr ".tconb" 0.5;
	setAttr ".tcona" 0.5;
createNode mentalrayOptions -s -n "PreviewCaustics";
	rename -uid "45F443BD-4323-A641-C76B-5A8A8C235998";
	setAttr ".splck" yes;
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".shrd" 2;
	setAttr ".ca" yes;
	setAttr ".cc" 1;
	setAttr ".cr" 1;
createNode mentalrayOptions -s -n "PreviewGlobalIllum";
	rename -uid "B4C80210-4C92-9675-0BC8-799318D8C026";
	setAttr ".splck" yes;
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".shrd" 2;
	setAttr ".gi" yes;
	setAttr ".gc" 1;
	setAttr ".gr" 1;
createNode mentalrayOptions -s -n "PreviewFinalGather";
	rename -uid "6E66F6FF-4999-C985-C412-F691936FFBC3";
	setAttr ".splck" yes;
	setAttr ".minsp" -1;
	setAttr ".maxsp" 1;
	setAttr ".fil" 1;
	setAttr ".rflr" 2;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 4;
	setAttr ".shrd" 2;
	setAttr ".fg" yes;
createNode mentalrayOptions -s -n "Production";
	rename -uid "FBFD13D6-46CE-4062-EA8C-6CA264630D16";
	setAttr ".splck" yes;
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
	setAttr ".shrd" 2;
createNode mentalrayOptions -s -n "ProductionMotionblur";
	rename -uid "9F336CF4-49FC-BCF0-76B6-F18B63C098E2";
	setAttr ".splck" yes;
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
	setAttr ".shrd" 2;
	setAttr ".mb" 2;
createNode mentalrayOptions -s -n "ProductionRapidMotion";
	rename -uid "C4D97F43-4F8D-34FA-C1F5-5798BF386809";
	setAttr ".splck" yes;
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".scan" 3;
	setAttr ".rapc" 8;
	setAttr ".raps" 2;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
	setAttr ".shrd" 2;
	setAttr ".mb" 2;
createNode mentalrayOptions -s -n "ProductionFineTrace";
	rename -uid "6B5EFD30-4291-1A4C-4111-6C82C5674DB9";
	setAttr ".conr" 0.019999999552965164;
	setAttr ".cong" 0.019999999552965164;
	setAttr ".conb" 0.019999999552965164;
	setAttr ".splck" yes;
	setAttr ".minsp" 1;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 0.75;
	setAttr ".filh" 0.75;
	setAttr ".jit" yes;
	setAttr ".rflr" 1;
	setAttr ".rfrr" 1;
	setAttr ".maxr" 1;
	setAttr ".shrd" 2;
createNode mentalrayOptions -s -n "ProductionRapidFur";
	rename -uid "6064D456-4104-FA5A-9807-2F889B9A997B";
	setAttr ".conr" 0.039999999105930328;
	setAttr ".cong" 0.029999999329447746;
	setAttr ".conb" 0.070000000298023224;
	setAttr ".splck" yes;
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 1.1449999809265137;
	setAttr ".filh" 1.1449999809265137;
	setAttr ".jit" yes;
	setAttr ".scan" 3;
	setAttr ".rapc" 3;
	setAttr ".raps" 0.25;
	setAttr ".ray" no;
	setAttr ".rflr" 1;
	setAttr ".rfrr" 1;
	setAttr ".maxr" 1;
	setAttr ".shrd" 2;
	setAttr ".mbsm" no;
	setAttr ".bism" 0.019999999552965164;
createNode mentalrayOptions -s -n "ProductionRapidHair";
	rename -uid "7843B890-4DE6-5995-36D3-41BA6FC5F14C";
	setAttr ".conr" 0.039999999105930328;
	setAttr ".cong" 0.029999999329447746;
	setAttr ".conb" 0.070000000298023224;
	setAttr ".splck" yes;
	setAttr ".minsp" 0;
	setAttr ".maxsp" 2;
	setAttr ".fil" 1;
	setAttr ".filw" 1.1449999809265137;
	setAttr ".filh" 1.1449999809265137;
	setAttr ".jit" yes;
	setAttr ".scan" 3;
	setAttr ".rapc" 6;
	setAttr ".ray" no;
	setAttr ".rflr" 1;
	setAttr ".rfrr" 1;
	setAttr ".maxr" 1;
	setAttr ".shrd" 2;
	setAttr ".mbsm" no;
	setAttr ".bism" 0.019999999552965164;
createNode mentalrayOptions -s -n "PreviewImrRayTracyOff";
	rename -uid "15D77B92-4463-29D0-48AC-C299C6695CFF";
	setAttr ".minsp" 0;
	setAttr ".fil" 1;
	setAttr ".ray" no;
	setAttr ".rflr" 1;
	setAttr ".rfrr" 1;
	setAttr ".maxr" 1;
	setAttr ".shrd" 2;
createNode mentalrayOptions -s -n "PreviewImrRayTracyOn";
	rename -uid "8AD200AE-468A-5244-8CB8-5B9DF348CDBD";
	setAttr ".minsp" 0;
	setAttr ".fil" 1;
	setAttr ".rflr" 1;
	setAttr ".rfrr" 2;
	setAttr ".maxr" 3;
	setAttr ".shrd" 1;
createNode multiplyDivide -n "midLegFrontProxySkinMulScalePosLFT_mdv";
	rename -uid "A34AA0DC-410C-898D-C371-DA84C1E9E254";
	setAttr ".i2" -type "float3" -0.0099999998 -0.0099999998 -0.0099999998 ;
createNode multiplyDivide -n "midLegFrontProxySkinMulScaleNegLFT_mdv";
	rename -uid "A417CC58-4979-9459-D76B-12941FCD0C58";
	setAttr ".i2" -type "float3" 0.0099999998 0.0099999998 0.0099999998 ;
createNode clamp -n "midLegFrontProxySkinClampScalePosLFT_mdv";
	rename -uid "4F2256FA-4B5B-6413-BD6E-81A7BB692EA1";
	setAttr ".mx" -type "float3" 180 180 180 ;
createNode clamp -n "midLegFrontProxySkinClampScaleNegLFT_mdv";
	rename -uid "49CEEC9C-4DBE-4D2E-52BF-C9AAE221E729";
	setAttr ".mx" -type "float3" 180 180 180 ;
createNode plusMinusAverage -n "midLegFrontProxySkinPlusPosNegScaleLFT_pma";
	rename -uid "90512D98-43B6-BCA1-0CF0-03B7197D4CE3";
	setAttr -s 2 ".i3";
	setAttr -s 2 ".i3";
createNode multiplyDivide -n "midLegFrontProxySkinMulScalePlusLFT_mdv";
	rename -uid "192EA9E1-4172-034A-E7E7-AD9F29289D88";
createNode addDoubleLinear -n "midLegFrontProxySkinDoubleScaleALFT_adl";
	rename -uid "EF6F9B32-4231-A1FC-87ED-E6BFABF02FE5";
	setAttr ".i1" 1;
createNode addDoubleLinear -n "midLegFrontProxySkinDoubleScaleBLFT_adl";
	rename -uid "DC883174-49E8-5D19-2672-1D8BBC732905";
	setAttr ".i1" 1;
createNode reverse -n "midLegFrontProxySkinDriveVoidLFT_adl";
	rename -uid "5535CD0C-426F-B2AB-602D-4E983E87C6EB";
createNode ikRPsolver -n "ikRPsolver";
	rename -uid "FC337457-4E55-BD57-759D-98A8DB0EFFC8";
createNode unitConversion -n "unitConversion1";
	rename -uid "9A6419D0-4756-885F-AE57-FAAFDAA8F4AA";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion2";
	rename -uid "71876520-4027-E4C3-8744-F0A58187F438";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion3";
	rename -uid "D9FB542A-4C32-08FD-4C87-EAA7AD832931";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion4";
	rename -uid "0D6A46F6-4242-0756-E279-76BB728A4037";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion5";
	rename -uid "10905733-4CC5-FB3D-79AB-5ABDAB20041A";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion6";
	rename -uid "3D68FDB3-438E-32F1-D99F-AEA27FD884BE";
	setAttr ".cf" 57.295779513082323;
createNode multiplyDivide -n "midLegFrontProxySkinMulScalePosRGT_mdv";
	rename -uid "CE75D0DF-4593-C928-B7D1-65BC8E636178";
	setAttr ".i2" -type "float3" -0.0099999998 -0.0099999998 -0.0099999998 ;
createNode multiplyDivide -n "midLegFrontProxySkinMulScaleNegRGT_mdv";
	rename -uid "D0F9A46E-4FEF-BA94-DD46-E1BF0DBC3489";
	setAttr ".i2" -type "float3" 0.0099999998 0.0099999998 0.0099999998 ;
createNode clamp -n "midLegFrontProxySkinClampScalePosRGT_mdv";
	rename -uid "4BA1D73B-413E-186E-2A48-40A4F8EDAF4C";
	setAttr ".mx" -type "float3" 180 180 180 ;
createNode clamp -n "midLegFrontProxySkinClampScaleNegRGT_mdv";
	rename -uid "17197D6B-40D7-5ACF-FB5F-D793FF23F457";
	setAttr ".mx" -type "float3" 180 180 180 ;
createNode plusMinusAverage -n "midLegFrontProxySkinPlusPosNegScaleRGT_pma";
	rename -uid "FA36B0CB-413F-E0E0-C377-EFB7A4ECAD4E";
	setAttr -s 2 ".i3";
	setAttr -s 2 ".i3";
createNode multiplyDivide -n "midLegFrontProxySkinMulScalePlusRGT_mdv";
	rename -uid "CB123B30-4F95-490A-F0C9-1FAFD79D92BC";
createNode addDoubleLinear -n "midLegFrontProxySkinDoubleScaleARGT_adl";
	rename -uid "8F0F3AA0-4FE1-4EB4-1F6E-B289282150CF";
	setAttr ".i1" 1;
createNode addDoubleLinear -n "midLegFrontProxySkinDoubleScaleBRGT_adl";
	rename -uid "675C80B0-44F8-4D21-9D70-15A87BE085C2";
	setAttr ".i1" 1;
createNode reverse -n "midLegFrontProxySkinDriveVoidRGT_adl";
	rename -uid "8C9C9F2F-4D79-DD0C-0FAD-3EA67E718185";
createNode unitConversion -n "unitConversion7";
	rename -uid "8564A678-44A7-6ACA-60DC-4D9979CE87BE";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion8";
	rename -uid "FE37D99D-499A-DB0F-D854-AE8DAFAE1FEC";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion9";
	rename -uid "B4AC7BAC-47DF-D50E-2A92-1BAD61CD13C2";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion10";
	rename -uid "906EEAF9-4840-94EF-F6FC-0E9C69FBD665";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion11";
	rename -uid "6F9D4FE2-4BC7-4542-8F9C-40ABB8A6DDEB";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion12";
	rename -uid "5FDCACFA-4D91-8D33-FD93-87B1180591BD";
	setAttr ".cf" 57.295779513082323;
createNode multiplyDivide -n "midLegBackProxySkinMulScalePosLFT_mdv";
	rename -uid "62E5A687-48CA-8464-7CAA-1192FBD83987";
	setAttr ".i2" -type "float3" -0.0099999998 -0.0099999998 -0.0099999998 ;
createNode multiplyDivide -n "midLegBackProxySkinMulScaleNegLFT_mdv";
	rename -uid "9439C391-40E4-1DC4-AF0D-7A94A06424BB";
	setAttr ".i2" -type "float3" 0.0099999998 0.0099999998 0.0099999998 ;
createNode clamp -n "midLegBackProxySkinClampScalePosLFT_mdv";
	rename -uid "5A662155-4937-48B5-A709-C4B66C6610AD";
	setAttr ".mx" -type "float3" 180 180 180 ;
createNode clamp -n "midLegBackProxySkinClampScaleNegLFT_mdv";
	rename -uid "884E8FD0-496C-0B6A-E3DB-2B8260F36893";
	setAttr ".mx" -type "float3" 180 180 180 ;
createNode plusMinusAverage -n "midLegBackProxySkinPlusPosNegScaleLFT_pma";
	rename -uid "29E01E8B-4613-51E1-F0C8-C9B5F095D55E";
	setAttr -s 2 ".i3";
	setAttr -s 2 ".i3";
createNode multiplyDivide -n "midLegBackProxySkinMulScalePlusLFT_mdv";
	rename -uid "B17F7A67-4271-8559-1187-7B860DD36367";
createNode addDoubleLinear -n "midLegBackProxySkinDoubleScaleALFT_adl";
	rename -uid "DD2A9A76-4552-4A3D-1D83-33A456CE6125";
	setAttr ".i1" 1;
createNode addDoubleLinear -n "midLegBackProxySkinDoubleScaleBLFT_adl";
	rename -uid "21BCFBEA-4282-60C3-B85D-72B2C0E41042";
	setAttr ".i1" 1;
createNode reverse -n "midLegBackProxySkinDriveVoidLFT_adl";
	rename -uid "38622A59-41B9-13F3-0D9F-058D2AC2AC8E";
createNode unitConversion -n "unitConversion13";
	rename -uid "AEDFA804-4843-20E0-A238-A99DC8DD67DD";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion14";
	rename -uid "E98133C3-4B95-C194-71A4-4D9E801732A6";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion15";
	rename -uid "51FD4E0B-498C-9C45-559F-52803FDFC5D3";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion16";
	rename -uid "68EAA865-4002-6053-1C98-46ACF34AB2A4";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion17";
	rename -uid "EB2C20EA-4ACF-0B0D-E9CC-F6B9AC284F88";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion18";
	rename -uid "4051063C-476A-DC1D-569C-D39B68E46DA7";
	setAttr ".cf" 57.295779513082323;
createNode multiplyDivide -n "midLegBackProxySkinMulScalePosRGT_mdv";
	rename -uid "943C7D61-4E92-EF20-527A-D4BE50565B9A";
	setAttr ".i2" -type "float3" -0.0099999998 -0.0099999998 -0.0099999998 ;
createNode multiplyDivide -n "midLegBackProxySkinMulScaleNegRGT_mdv";
	rename -uid "AABB255C-4CA1-B38A-7DBC-DA8958774823";
	setAttr ".i2" -type "float3" 0.0099999998 0.0099999998 0.0099999998 ;
createNode clamp -n "midLegBackProxySkinClampScalePosRGT_mdv";
	rename -uid "3AB76783-4445-37E5-84AF-96B095E3D8D8";
	setAttr ".mx" -type "float3" 180 180 180 ;
createNode clamp -n "midLegBackProxySkinClampScaleNegRGT_mdv";
	rename -uid "D15EE8E2-494A-48E4-3DF7-1289AD6DD810";
	setAttr ".mx" -type "float3" 180 180 180 ;
createNode plusMinusAverage -n "midLegBackProxySkinPlusPosNegScaleRGT_pma";
	rename -uid "4D588B18-44BF-AD1C-9BD2-03AC379D7C1E";
	setAttr -s 2 ".i3";
	setAttr -s 2 ".i3";
createNode multiplyDivide -n "midLegBackProxySkinMulScalePlusRGT_mdv";
	rename -uid "6C23978E-4E33-6261-D535-2CB9B35A60D8";
createNode addDoubleLinear -n "midLegBackProxySkinDoubleScaleARGT_adl";
	rename -uid "A3AFF0EC-4B7B-42C4-B8C9-79BA312CB7AF";
	setAttr ".i1" 1;
createNode addDoubleLinear -n "midLegBackProxySkinDoubleScaleBRGT_adl";
	rename -uid "451620AE-4AF2-3C56-4295-A48A3963BC63";
	setAttr ".i1" 1;
createNode reverse -n "midLegBackProxySkinDriveVoidRGT_adl";
	rename -uid "6CAA88F2-49DE-B98D-CA5A-18A1F971C8D4";
createNode unitConversion -n "unitConversion19";
	rename -uid "4A693803-41B3-7142-9573-B9AFB5F3A0E9";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion20";
	rename -uid "F509DBCC-4D31-990E-7D85-6E9FC956D7F1";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion21";
	rename -uid "E1CDCE23-4940-237F-4998-62A9EB89555A";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion22";
	rename -uid "C6827C82-4DFA-DE55-0D2F-60BF2BD1A55A";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion23";
	rename -uid "F9113939-43B6-4C97-B78C-9CA09AB4DB33";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion24";
	rename -uid "5218F0A9-449F-173A-8898-07BF6922E445";
	setAttr ".cf" 57.295779513082323;
createNode multiplyDivide -n "lowLegBackProxySkinMulScalePosLFT_mdv";
	rename -uid "C85BF27C-41D2-BF08-8B55-7C8CEB105E04";
	setAttr ".i2" -type "float3" -0.0099999998 -0.0099999998 -0.0099999998 ;
createNode multiplyDivide -n "lowLegBackProxySkinMulScaleNegLFT_mdv";
	rename -uid "006867BE-4DA8-717B-370F-01BD8AD45683";
	setAttr ".i2" -type "float3" 0.0099999998 0.0099999998 0.0099999998 ;
createNode clamp -n "lowLegBackProxySkinClampScalePosLFT_mdv";
	rename -uid "6FAB3E57-472E-EA42-0C6F-959578867420";
	setAttr ".mx" -type "float3" 180 180 180 ;
createNode clamp -n "lowLegBackProxySkinClampScaleNegLFT_mdv";
	rename -uid "487A4AEB-4078-E952-9D8C-62950A432C42";
	setAttr ".mx" -type "float3" 180 180 180 ;
createNode plusMinusAverage -n "lowLegBackProxySkinPlusPosNegScaleLFT_pma";
	rename -uid "2D073203-45E6-745F-E772-2282A6BC7420";
	setAttr -s 2 ".i3";
	setAttr -s 2 ".i3";
createNode multiplyDivide -n "lowLegBackProxySkinMulScalePlusLFT_mdv";
	rename -uid "C40FB5F0-46F6-5FE3-9EE3-46A444DAA5F9";
createNode addDoubleLinear -n "lowLegBackProxySkinDoubleScaleALFT_adl";
	rename -uid "342B8AE2-4618-A212-A8BC-CFA3E9462154";
	setAttr ".i1" 1;
createNode addDoubleLinear -n "lowLegBackProxySkinDoubleScaleBLFT_adl";
	rename -uid "16CB982E-43EF-75C0-4650-90A19227AA71";
	setAttr ".i1" 1;
createNode reverse -n "lowLegBackProxySkinDriveVoidLFT_adl";
	rename -uid "9F892079-4850-2DE4-CB6B-AA82908A1AC4";
createNode unitConversion -n "unitConversion25";
	rename -uid "248B4FB0-41A8-FC1D-52CF-3899CC6DC64D";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion26";
	rename -uid "97225937-4DAC-CA3A-F95C-AFAC31753E15";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion27";
	rename -uid "92B0AFFF-44C6-C085-99B7-2EBF088535D8";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion28";
	rename -uid "A1BF06D3-4C3E-6845-AF15-5ABF3DAA23F5";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion29";
	rename -uid "B2F2465F-470C-4FE0-2BEE-FF8A0CF56D0E";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion30";
	rename -uid "A4E26622-43BF-4F59-DDFD-F9AE8944C06F";
	setAttr ".cf" 57.295779513082323;
createNode multiplyDivide -n "lowLegBackProxySkinMulScalePosRGT_mdv";
	rename -uid "24F87394-4EE4-7429-F213-559632344D1A";
	setAttr ".i2" -type "float3" -0.0099999998 -0.0099999998 -0.0099999998 ;
createNode multiplyDivide -n "lowLegBackProxySkinMulScaleNegRGT_mdv";
	rename -uid "BD7947BC-47CB-1D72-6BCA-77B04ADB9BC0";
	setAttr ".i2" -type "float3" 0.0099999998 0.0099999998 0.0099999998 ;
createNode clamp -n "lowLegBackProxySkinClampScalePosRGT_mdv";
	rename -uid "8EF8594B-45C4-EE88-30F0-88ADBBAFFCEC";
	setAttr ".mx" -type "float3" 180 180 180 ;
createNode clamp -n "lowLegBackProxySkinClampScaleNegRGT_mdv";
	rename -uid "2CCCF132-4113-6B3A-5E8C-C78E0E07F4EC";
	setAttr ".mx" -type "float3" 180 180 180 ;
createNode plusMinusAverage -n "lowLegBackProxySkinPlusPosNegScaleRGT_pma";
	rename -uid "BB86C06C-4B84-981A-EB52-899FD333EAE4";
	setAttr -s 2 ".i3";
	setAttr -s 2 ".i3";
createNode multiplyDivide -n "lowLegBackProxySkinMulScalePlusRGT_mdv";
	rename -uid "6BE40204-4D05-D731-03E4-F197AFCF72F5";
createNode addDoubleLinear -n "lowLegBackProxySkinDoubleScaleARGT_adl";
	rename -uid "1CBB1817-4AED-4511-FF74-BFA3FB462DD9";
	setAttr ".i1" 1;
createNode addDoubleLinear -n "lowLegBackProxySkinDoubleScaleBRGT_adl";
	rename -uid "9FFA3A88-4F93-8B0B-BDA5-39BD72B6B0EC";
	setAttr ".i1" 1;
createNode reverse -n "lowLegBackProxySkinDriveVoidRGT_adl";
	rename -uid "B98F88EC-4006-A441-67AB-43B9B3F3E045";
createNode unitConversion -n "unitConversion31";
	rename -uid "5E4EB99E-4C44-A0DA-8E46-509B4AFACB26";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion32";
	rename -uid "062BDCE1-4E68-A5D6-E4B7-FFA2377F7EFF";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion33";
	rename -uid "4BD1BF71-4B7D-4372-8E66-B1A2EC3ADBB6";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion34";
	rename -uid "2D48AB75-4D89-5424-9F80-EC99672B7E20";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion35";
	rename -uid "E150EAB9-4AED-DA2B-E7A9-EA81E954CEAB";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion36";
	rename -uid "BADB658C-4227-A5C7-6538-CCAE76D7C9C8";
	setAttr ".cf" 57.295779513082323;
createNode multiplyDivide -n "ankleBackProxySkinMulScalePosLFT_mdv";
	rename -uid "EB4FB7FB-48BB-48E3-B5D6-498A0CE4C599";
	setAttr ".i2" -type "float3" -0.0099999998 -0.0099999998 -0.0099999998 ;
createNode multiplyDivide -n "ankleBackProxySkinMulScaleNegLFT_mdv";
	rename -uid "EF370EDD-4F69-0961-47C8-2E83E9FB1401";
	setAttr ".i2" -type "float3" 0.0099999998 0.0099999998 0.0099999998 ;
createNode clamp -n "ankleBackProxySkinClampScalePosLFT_mdv";
	rename -uid "57CAD43C-4601-E8DF-F364-D587CE5B6937";
	setAttr ".mx" -type "float3" 180 180 180 ;
createNode clamp -n "ankleBackProxySkinClampScaleNegLFT_mdv";
	rename -uid "02A7D4BB-47B6-EB2F-D96A-D6AA4974E6E3";
	setAttr ".mx" -type "float3" 180 180 180 ;
createNode plusMinusAverage -n "ankleBackProxySkinPlusPosNegScaleLFT_pma";
	rename -uid "5D6BE05B-4E1F-42EE-1A59-87B93B3F2A20";
	setAttr -s 2 ".i3";
	setAttr -s 2 ".i3";
createNode multiplyDivide -n "ankleBackProxySkinMulScalePlusLFT_mdv";
	rename -uid "0105C95D-4F51-BC2B-5214-25A9C54DDE08";
createNode addDoubleLinear -n "ankleBackProxySkinDoubleScaleALFT_adl";
	rename -uid "299DE0B1-4192-8A8E-E245-D3AAEE880546";
	setAttr ".i1" 1;
createNode addDoubleLinear -n "ankleBackProxySkinDoubleScaleBLFT_adl";
	rename -uid "21FCFC13-4CE1-AA1E-BC43-F7940F45D8D4";
	setAttr ".i1" 1;
createNode reverse -n "ankleBackProxySkinDriveVoidLFT_adl";
	rename -uid "D137C8A7-46F8-54DD-DDAB-30ADF9D6C01E";
createNode unitConversion -n "unitConversion37";
	rename -uid "DBA5EAB8-489A-A190-D5ED-0C9AC6BAA3E8";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion38";
	rename -uid "6DBC2B1A-45B8-9AD5-4242-D3B42611D9AA";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion39";
	rename -uid "3D88A97A-49A3-7614-D5AF-0FAC15121A7A";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion40";
	rename -uid "C6C3AD9A-4240-26B2-505E-BEA81E79E7E7";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion41";
	rename -uid "A82F4713-4AEE-8D9F-0CE6-6CA565019764";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion42";
	rename -uid "CE31F9A4-4E5A-9A42-84CB-BEA9DAF6B835";
	setAttr ".cf" 57.295779513082323;
createNode multiplyDivide -n "ankleBackProxySkinMulScalePosRGT_mdv";
	rename -uid "9F324F0E-4241-6689-1C7A-BDA97FCA7810";
	setAttr ".i2" -type "float3" -0.0099999998 -0.0099999998 -0.0099999998 ;
createNode multiplyDivide -n "ankleBackProxySkinMulScaleNegRGT_mdv";
	rename -uid "E5126028-4385-305D-09CD-3F809C2F4635";
	setAttr ".i2" -type "float3" 0.0099999998 0.0099999998 0.0099999998 ;
createNode clamp -n "ankleBackProxySkinClampScalePosRGT_mdv";
	rename -uid "929D2EF1-46D0-1A3D-C0BC-A3BE6A231A5D";
	setAttr ".mx" -type "float3" 180 180 180 ;
createNode clamp -n "ankleBackProxySkinClampScaleNegRGT_mdv";
	rename -uid "A6CD9F31-480C-52FC-32EE-A7B0CC5A531E";
	setAttr ".mx" -type "float3" 180 180 180 ;
createNode plusMinusAverage -n "ankleBackProxySkinPlusPosNegScaleRGT_pma";
	rename -uid "5E07AD01-494A-91B7-0792-3D835F422632";
	setAttr -s 2 ".i3";
	setAttr -s 2 ".i3";
createNode multiplyDivide -n "ankleBackProxySkinMulScalePlusRGT_mdv";
	rename -uid "6FC624BE-44E8-ED76-1A03-E586106F4528";
createNode addDoubleLinear -n "ankleBackProxySkinDoubleScaleARGT_adl";
	rename -uid "B40C88CF-4057-17FA-BC31-CBA8D67AF920";
	setAttr ".i1" 1;
createNode addDoubleLinear -n "ankleBackProxySkinDoubleScaleBRGT_adl";
	rename -uid "9A3235C4-405B-BE69-75CC-179CEFB3BA8C";
	setAttr ".i1" 1;
createNode reverse -n "ankleBackProxySkinDriveVoidRGT_adl";
	rename -uid "88BA8E6F-46E2-4928-FCD2-31B27E1155A9";
createNode unitConversion -n "unitConversion43";
	rename -uid "B6FC7A3A-4E7E-75B4-3144-E09C89D61539";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion44";
	rename -uid "7D6D5008-46EA-71FE-A7F0-A6AE62D2D007";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion45";
	rename -uid "7FE7D198-45C2-517D-D96D-3482E3C56449";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion46";
	rename -uid "D3E26FDD-458F-20A9-66BB-9A9E1E5C6AC3";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion47";
	rename -uid "DF3ADAA3-423D-1496-3947-2DB561232BDA";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion48";
	rename -uid "14BD78DB-418A-61D8-8742-B2A677D2A9EA";
	setAttr ".cf" 57.295779513082323;
createNode multiplyDivide -n "ballBackProxySkinMulScalePosLFT_mdv";
	rename -uid "CED3B59D-484D-37AB-AB09-F58594D4C77C";
	setAttr ".i2" -type "float3" -0.0099999998 -0.0099999998 -0.0099999998 ;
createNode multiplyDivide -n "ballBackProxySkinMulScaleNegLFT_mdv";
	rename -uid "5CCAF926-4DD0-AFF0-B6B3-BDA978725B17";
	setAttr ".i2" -type "float3" 0.0099999998 0.0099999998 0.0099999998 ;
createNode clamp -n "ballBackProxySkinClampScalePosLFT_mdv";
	rename -uid "D77F70CC-43C2-159F-D61E-BA920450937F";
	setAttr ".mx" -type "float3" 180 180 180 ;
createNode clamp -n "ballBackProxySkinClampScaleNegLFT_mdv";
	rename -uid "2AC0EFD8-499C-A9DC-F086-CDB2F54C2198";
	setAttr ".mx" -type "float3" 180 180 180 ;
createNode plusMinusAverage -n "ballBackProxySkinPlusPosNegScaleLFT_pma";
	rename -uid "9F95743A-4E24-FE27-BC19-7F8DA274FAB8";
	setAttr -s 2 ".i3";
	setAttr -s 2 ".i3";
createNode multiplyDivide -n "ballBackProxySkinMulScalePlusLFT_mdv";
	rename -uid "2FD7687E-4012-B683-495D-EE9DB00E3B0A";
createNode addDoubleLinear -n "ballBackProxySkinDoubleScaleALFT_adl";
	rename -uid "E8AD9B6A-4B87-0FEE-B658-FE8AB4A05E2E";
	setAttr ".i1" 1;
createNode addDoubleLinear -n "ballBackProxySkinDoubleScaleBLFT_adl";
	rename -uid "BDBE87EB-4CE9-E6FA-05D2-8DBC8C587C81";
	setAttr ".i1" 1;
createNode reverse -n "ballBackProxySkinDriveVoidLFT_adl";
	rename -uid "BA81DDFD-4626-8240-9AC1-E2B116D69FE4";
createNode unitConversion -n "unitConversion49";
	rename -uid "6750DA69-4511-3EB4-6176-229BABEA71C9";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion50";
	rename -uid "4DAFD445-46EB-9DD1-E0EC-EA9CAF28CE07";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion51";
	rename -uid "3E7A329D-4369-443D-F16B-59BE5A8CF67C";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion52";
	rename -uid "1E1B01F9-49A9-C85C-B92D-F5A9479E1CFA";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion53";
	rename -uid "836C1144-44A8-C14D-AF1B-1099E10F68D5";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion54";
	rename -uid "6BA59B15-4568-F604-5DBA-92B8A7841138";
	setAttr ".cf" 57.295779513082323;
createNode multiplyDivide -n "ballBackProxySkinMulScalePosRGT_mdv";
	rename -uid "A78BD1B6-45D0-7E40-FC45-2DB561CF62EF";
	setAttr ".i2" -type "float3" -0.0099999998 -0.0099999998 -0.0099999998 ;
createNode multiplyDivide -n "ballBackProxySkinMulScaleNegRGT_mdv";
	rename -uid "31E54B05-41C9-EBA6-82CA-ABA958404CDB";
	setAttr ".i2" -type "float3" 0.0099999998 0.0099999998 0.0099999998 ;
createNode clamp -n "ballBackProxySkinClampScalePosRGT_mdv";
	rename -uid "DDE69DDF-454A-CDCE-3930-E1ABFB959DA9";
	setAttr ".mx" -type "float3" 180 180 180 ;
createNode clamp -n "ballBackProxySkinClampScaleNegRGT_mdv";
	rename -uid "6ABD7B58-4ABE-0161-CE70-2D9A57A43063";
	setAttr ".mx" -type "float3" 180 180 180 ;
createNode plusMinusAverage -n "ballBackProxySkinPlusPosNegScaleRGT_pma";
	rename -uid "A1522F14-406B-A20D-E7A3-778A413B8DC1";
	setAttr -s 2 ".i3";
	setAttr -s 2 ".i3";
createNode multiplyDivide -n "ballBackProxySkinMulScalePlusRGT_mdv";
	rename -uid "57C5AA93-4720-ED65-DB03-61BEB7161DC3";
createNode addDoubleLinear -n "ballBackProxySkinDoubleScaleARGT_adl";
	rename -uid "182B4119-4D5D-94B6-A11C-CFAE5D163419";
	setAttr ".i1" 1;
createNode addDoubleLinear -n "ballBackProxySkinDoubleScaleBRGT_adl";
	rename -uid "95B66532-43B8-EAD6-78BD-A3BA24413F08";
	setAttr ".i1" 1;
createNode reverse -n "ballBackProxySkinDriveVoidRGT_adl";
	rename -uid "CBEABB7E-40EF-D28A-073B-7798CA5ECC1C";
createNode unitConversion -n "unitConversion55";
	rename -uid "83C1E9A8-4BE7-A0B8-E17F-4F9D64D335FF";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion56";
	rename -uid "1F082CB9-42F1-A9F5-0908-658C057C0E8D";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion57";
	rename -uid "F99BA65D-482E-FD78-EF3F-A9998AEC8A4F";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion58";
	rename -uid "1318A1C7-4C1A-8FFB-67DC-FD8DB30FE39E";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion59";
	rename -uid "69A08E9B-42F6-794A-429A-40A6F64C6080";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion60";
	rename -uid "7490F8B7-49D9-DC14-7CBC-EAAF658CA524";
	setAttr ".cf" 57.295779513082323;
createNode multiplyDivide -n "ankleFrontProxySkinMulScalePosLFT_mdv";
	rename -uid "36DB4558-4441-5AD8-FD6D-A3AC1FEA33DF";
	setAttr ".i2" -type "float3" -0.0099999998 -0.0099999998 -0.0099999998 ;
createNode multiplyDivide -n "ankleFrontProxySkinMulScaleNegLFT_mdv";
	rename -uid "D4AAF44B-4DB6-CD0C-E035-C4A169F30964";
	setAttr ".i2" -type "float3" 0.0099999998 0.0099999998 0.0099999998 ;
createNode clamp -n "ankleFrontProxySkinClampScalePosLFT_mdv";
	rename -uid "0C29D721-421E-F9AE-266F-768954020CE2";
	setAttr ".mx" -type "float3" 180 180 180 ;
createNode clamp -n "ankleFrontProxySkinClampScaleNegLFT_mdv";
	rename -uid "F168D5A9-4127-E7F9-8072-EB81BAF80BDC";
	setAttr ".mx" -type "float3" 180 180 180 ;
createNode plusMinusAverage -n "ankleFrontProxySkinPlusPosNegScaleLFT_pma";
	rename -uid "9D5EB8D6-4056-5A7D-A611-2DBBC5C2899A";
	setAttr -s 2 ".i3";
	setAttr -s 2 ".i3";
createNode multiplyDivide -n "ankleFrontProxySkinMulScalePlusLFT_mdv";
	rename -uid "B0AFA20A-407B-FC3B-ECEB-E2A6654732B1";
createNode addDoubleLinear -n "ankleFrontProxySkinDoubleScaleALFT_adl";
	rename -uid "BFB3B03A-4F66-8320-CAE7-29AFAA55E1BD";
	setAttr ".i1" 1;
createNode addDoubleLinear -n "ankleFrontProxySkinDoubleScaleBLFT_adl";
	rename -uid "32E8D0FD-4A15-7113-265B-CD8DF6612D0B";
	setAttr ".i1" 1;
createNode reverse -n "ankleFrontProxySkinDriveVoidLFT_adl";
	rename -uid "FC0F8BBD-4AC5-8B18-150D-02B187AF5D26";
createNode unitConversion -n "unitConversion61";
	rename -uid "921E6A16-4483-B0F9-B058-ADBCCD7A23A0";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion62";
	rename -uid "F629C41F-4C12-A1C6-DAED-9E862C311874";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion63";
	rename -uid "588CC342-49CF-A0A6-4847-00909974A356";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion64";
	rename -uid "5F526517-4244-BDC9-0405-0886CA4753A3";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion65";
	rename -uid "75BE98BD-4B41-7018-CF3C-108B21E46470";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion66";
	rename -uid "33F12188-4E08-C20A-1168-308D83DDB720";
	setAttr ".cf" 57.295779513082323;
createNode multiplyDivide -n "ballFrontProxySkinMulScalePosLFT_mdv";
	rename -uid "81C13706-4356-1875-C07E-F1B4A3184E18";
	setAttr ".i2" -type "float3" -0.0099999998 -0.0099999998 -0.0099999998 ;
createNode multiplyDivide -n "ballFrontProxySkinMulScaleNegLFT_mdv";
	rename -uid "2CC223D3-4A06-F09C-AD41-8CBC70EC4A04";
	setAttr ".i2" -type "float3" 0.0099999998 0.0099999998 0.0099999998 ;
createNode clamp -n "ballFrontProxySkinClampScalePosLFT_mdv";
	rename -uid "8BD7A0CF-4D53-CAD4-8C03-50ACE546DC70";
	setAttr ".mx" -type "float3" 180 180 180 ;
createNode clamp -n "ballFrontProxySkinClampScaleNegLFT_mdv";
	rename -uid "8A73D720-43EF-AC94-1BB5-3B8ECC95130C";
	setAttr ".mx" -type "float3" 180 180 180 ;
createNode plusMinusAverage -n "ballFrontProxySkinPlusPosNegScaleLFT_pma";
	rename -uid "A503D8C9-4DCA-EC29-278F-B5837735DDD6";
	setAttr -s 2 ".i3";
	setAttr -s 2 ".i3";
createNode multiplyDivide -n "ballFrontProxySkinMulScalePlusLFT_mdv";
	rename -uid "A3BE96AE-4A9C-9885-9BEF-11A0A836524D";
createNode addDoubleLinear -n "ballFrontProxySkinDoubleScaleALFT_adl";
	rename -uid "01417445-4873-2D93-454D-45A5F11C8F9C";
	setAttr ".i1" 1;
createNode addDoubleLinear -n "ballFrontProxySkinDoubleScaleBLFT_adl";
	rename -uid "7B347AFE-4869-1BD0-F043-DBB7B0C3208E";
	setAttr ".i1" 1;
createNode reverse -n "ballFrontProxySkinDriveVoidLFT_adl";
	rename -uid "06D43327-47CB-B046-A5FD-9D8DEB0AAA0E";
createNode unitConversion -n "unitConversion67";
	rename -uid "4A9AA257-4740-94AB-D887-A0BA60FC6A72";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion68";
	rename -uid "E2570D70-428B-F67E-89ED-55A7839EB078";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion69";
	rename -uid "AA9983D5-405B-4DD7-9BB5-AD91FA266CBB";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion70";
	rename -uid "9FBC1722-4270-2F99-65CA-8A884F4C4849";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion71";
	rename -uid "302792E4-43AF-C941-3D0A-25871843FF2A";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion72";
	rename -uid "148DEBEF-4210-2F2C-7B4A-F69B4C23F914";
	setAttr ".cf" 57.295779513082323;
createNode multiplyDivide -n "ankleFrontProxySkinMulScalePosRGT_mdv";
	rename -uid "70027C2B-47B4-3CC5-FA4C-D79ECDEA282C";
	setAttr ".i2" -type "float3" -0.0099999998 -0.0099999998 -0.0099999998 ;
createNode multiplyDivide -n "ankleFrontProxySkinMulScaleNegRGT_mdv";
	rename -uid "E0AA9145-4325-E5B2-DE49-6A98AFB65D93";
	setAttr ".i2" -type "float3" 0.0099999998 0.0099999998 0.0099999998 ;
createNode clamp -n "ankleFrontProxySkinClampScalePosRGT_mdv";
	rename -uid "AF812CDF-461E-DB17-BE75-53ABC5AA2C99";
	setAttr ".mx" -type "float3" 180 180 180 ;
createNode clamp -n "ankleFrontProxySkinClampScaleNegRGT_mdv";
	rename -uid "FBCA1182-400B-2D17-790A-A2B151071086";
	setAttr ".mx" -type "float3" 180 180 180 ;
createNode plusMinusAverage -n "ankleFrontProxySkinPlusPosNegScaleRGT_pma";
	rename -uid "93772E10-43B6-5176-9C39-C8B0063AEC5B";
	setAttr -s 2 ".i3";
	setAttr -s 2 ".i3";
createNode multiplyDivide -n "ankleFrontProxySkinMulScalePlusRGT_mdv";
	rename -uid "39703CF6-4165-DAB3-BE33-DF8422559C72";
createNode addDoubleLinear -n "ankleFrontProxySkinDoubleScaleARGT_adl";
	rename -uid "36CCBD93-4E51-5AB7-5DE0-BDA8D2AD272F";
	setAttr ".i1" 1;
createNode addDoubleLinear -n "ankleFrontProxySkinDoubleScaleBRGT_adl";
	rename -uid "037E3BE3-4653-6471-2F49-9B982B8F9588";
	setAttr ".i1" 1;
createNode reverse -n "ankleFrontProxySkinDriveVoidRGT_adl";
	rename -uid "B210C86B-4887-5692-B620-FE980AAC1DE3";
createNode unitConversion -n "unitConversion73";
	rename -uid "9FB09779-475F-50D7-F675-7293FBA8392E";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion74";
	rename -uid "1F152C19-4883-274B-2215-E8BB8CE58EE7";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion75";
	rename -uid "0FCA7CF9-4AE5-0289-2CBD-0DA33E086392";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion76";
	rename -uid "6850E41E-45EB-2EE2-2E38-649E4498D12A";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion77";
	rename -uid "96609434-4B7B-B2EA-E7B7-A8A02D0BF1B6";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion78";
	rename -uid "8B3DBD17-4C2E-F629-006E-AAAC144E987D";
	setAttr ".cf" 57.295779513082323;
createNode multiplyDivide -n "ballFrontProxySkinMulScalePosRGT_mdv";
	rename -uid "737B9A71-4EB3-FE42-C9B5-979D08B258F5";
	setAttr ".i2" -type "float3" -0.0099999998 -0.0099999998 -0.0099999998 ;
createNode multiplyDivide -n "ballFrontProxySkinMulScaleNegRGT_mdv";
	rename -uid "2B3A783A-4008-3617-6E80-5EA85D2776EB";
	setAttr ".i2" -type "float3" 0.0099999998 0.0099999998 0.0099999998 ;
createNode clamp -n "ballFrontProxySkinClampScalePosRGT_mdv";
	rename -uid "D2E46A36-469E-948C-70E5-339EBA165DA5";
	setAttr ".mx" -type "float3" 180 180 180 ;
createNode clamp -n "ballFrontProxySkinClampScaleNegRGT_mdv";
	rename -uid "F0CA78CF-45E9-ABF1-CA85-76A23430BBE0";
	setAttr ".mx" -type "float3" 180 180 180 ;
createNode plusMinusAverage -n "ballFrontProxySkinPlusPosNegScaleRGT_pma";
	rename -uid "6120B242-4B62-15ED-0A6B-0BA7F0F9C32E";
	setAttr -s 2 ".i3";
	setAttr -s 2 ".i3";
createNode multiplyDivide -n "ballFrontProxySkinMulScalePlusRGT_mdv";
	rename -uid "F7FB0AF3-4FD4-0B0B-C50F-3B9CE23A4E18";
createNode addDoubleLinear -n "ballFrontProxySkinDoubleScaleARGT_adl";
	rename -uid "D0D0CF3B-4DD9-42B9-B5CB-EF9AEB3FB065";
	setAttr ".i1" 1;
createNode addDoubleLinear -n "ballFrontProxySkinDoubleScaleBRGT_adl";
	rename -uid "C8AE52A0-4970-8F30-10C7-80A095825494";
	setAttr ".i1" 1;
createNode reverse -n "ballFrontProxySkinDriveVoidRGT_adl";
	rename -uid "10DABFBF-4F00-6DAE-0C92-9CA155D0B341";
createNode unitConversion -n "unitConversion79";
	rename -uid "B59C83E2-4BA7-346B-177A-708F7B8B411F";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion80";
	rename -uid "DBC96235-4C3F-E742-A702-A1A7EC7AD70E";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion81";
	rename -uid "1E137565-4C7C-7D82-587E-E8B33837F154";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion82";
	rename -uid "1117B9F5-466A-189A-EDBE-80894CF2CF4A";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion83";
	rename -uid "0D759F16-4D29-F1FC-541C-6EB249851CD2";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion84";
	rename -uid "CB92740F-4822-27A7-E43B-6DAB4DE2A1BA";
	setAttr ".cf" 57.295779513082323;
createNode multiplyDivide -n "lowLegFrontProxySkinMulScalePosLFT_mdv";
	rename -uid "F0D6F171-4D6F-F4B0-463B-109792130C7D";
	setAttr ".i2" -type "float3" -0.0099999998 -0.0099999998 -0.0099999998 ;
createNode multiplyDivide -n "lowLegFrontProxySkinMulScaleNegLFT_mdv";
	rename -uid "8E5395EE-40B6-4A28-5FA1-B0AF6D80AA1C";
	setAttr ".i2" -type "float3" 0.0099999998 0.0099999998 0.0099999998 ;
createNode clamp -n "lowLegFrontProxySkinClampScalePosLFT_mdv";
	rename -uid "EA2292FF-4829-CB1E-B60D-419D99F91BCC";
	setAttr ".mx" -type "float3" 180 180 180 ;
createNode clamp -n "lowLegFrontProxySkinClampScaleNegLFT_mdv";
	rename -uid "D63E7EB1-4B5C-6C11-09B0-8E81F61F0DA0";
	setAttr ".mx" -type "float3" 180 180 180 ;
createNode plusMinusAverage -n "lowLegFrontProxySkinPlusPosNegScaleLFT_pma";
	rename -uid "BF140F94-46BC-C806-59BF-A08993425D8E";
	setAttr -s 2 ".i3";
	setAttr -s 2 ".i3";
createNode multiplyDivide -n "lowLegFrontProxySkinMulScalePlusLFT_mdv";
	rename -uid "DDF92E68-4FFF-EFDE-2E2B-ADB8AFF7828B";
createNode addDoubleLinear -n "lowLegFrontProxySkinDoubleScaleALFT_adl";
	rename -uid "7C6CD3DC-46F6-DE5A-B288-379EC4B14E83";
	setAttr ".i1" 1;
createNode addDoubleLinear -n "lowLegFrontProxySkinDoubleScaleBLFT_adl";
	rename -uid "70D739F7-4265-DD8A-E973-09BED533E139";
	setAttr ".i1" 1;
createNode reverse -n "lowLegFrontProxySkinDriveVoidLFT_adl";
	rename -uid "C06A79DD-4BA7-6736-4CF7-7592CD0955A4";
createNode unitConversion -n "unitConversion85";
	rename -uid "2B1BCFB2-4C75-79D6-7D5E-CD97F5B31691";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion86";
	rename -uid "C03C281A-4A91-F0D8-9561-159557E48E42";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion87";
	rename -uid "13386A5E-4D02-3F1B-B097-26B8CEAEF3F4";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion88";
	rename -uid "65474D3A-401D-7766-3C87-148933CA6149";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion89";
	rename -uid "25532E11-4C51-D6A1-77ED-87B75DB84B97";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion90";
	rename -uid "50BFCC4F-415B-1741-6FBC-BCAC7C1D0BB2";
	setAttr ".cf" 57.295779513082323;
createNode multiplyDivide -n "lowLegFrontProxySkinMulScalePosRGT_mdv";
	rename -uid "E27A8D46-4302-F231-9DA6-81A703FA469E";
	setAttr ".i2" -type "float3" -0.0099999998 -0.0099999998 -0.0099999998 ;
createNode multiplyDivide -n "lowLegFrontProxySkinMulScaleNegRGT_mdv";
	rename -uid "739EEAD6-4BD5-F619-EADB-33B000826421";
	setAttr ".i2" -type "float3" 0.0099999998 0.0099999998 0.0099999998 ;
createNode clamp -n "lowLegFrontProxySkinClampScalePosRGT_mdv";
	rename -uid "AB350F16-4F1D-7D8B-F959-BB8EFCA28847";
	setAttr ".mx" -type "float3" 180 180 180 ;
createNode clamp -n "lowLegFrontProxySkinClampScaleNegRGT_mdv";
	rename -uid "D86F187B-44D9-E59C-AC69-D1A1F80DF4D2";
	setAttr ".mx" -type "float3" 180 180 180 ;
createNode plusMinusAverage -n "lowLegFrontProxySkinPlusPosNegScaleRGT_pma";
	rename -uid "A5A98CA0-40AC-ADEC-07DA-E9BF99398503";
	setAttr -s 2 ".i3";
	setAttr -s 2 ".i3";
createNode multiplyDivide -n "lowLegFrontProxySkinMulScalePlusRGT_mdv";
	rename -uid "B3642F1C-40BE-E780-2C8F-49AF1D64C7D9";
createNode addDoubleLinear -n "lowLegFrontProxySkinDoubleScaleARGT_adl";
	rename -uid "3F0E8AF4-4B7A-CF54-B012-9883178C2B08";
	setAttr ".i1" 1;
createNode addDoubleLinear -n "lowLegFrontProxySkinDoubleScaleBRGT_adl";
	rename -uid "45E9FC26-4834-A246-541D-A8A7D3D4F21C";
	setAttr ".i1" 1;
createNode reverse -n "lowLegFrontProxySkinDriveVoidRGT_adl";
	rename -uid "00E76E53-4339-CC33-C185-D0A95BE3C0FE";
createNode unitConversion -n "unitConversion91";
	rename -uid "948BC57C-4E3D-5C91-B527-84832FC3513D";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion92";
	rename -uid "971AC9F8-4CD5-2FA7-F81F-009A46FCCDEB";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion93";
	rename -uid "33BE349D-47CE-75A1-B335-3590FCD72C10";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion94";
	rename -uid "2C2B4750-494A-70CE-A058-0FBF83427F87";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion95";
	rename -uid "774104D4-4DFF-2AB5-5DBC-6B81FBD4D398";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion96";
	rename -uid "C4F4CCA3-4C21-5186-EC20-13A89A7E190A";
	setAttr ".cf" 57.295779513082323;
select -ne :time1;
	setAttr -av -k on ".cch";
	setAttr -av -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 1;
	setAttr -av ".unw" 1;
	setAttr -k on ".etw";
	setAttr -k on ".tps";
	setAttr -av -k on ".tms";
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 9 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
select -ne :renderGlobalsList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
select -ne :defaultShaderList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
	setAttr -k on ".ihi";
select -ne :initialShadingGroup;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -k on ".mico";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -k on ".micr";
	setAttr -k on ".micg";
	setAttr -k on ".micb";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
select -ne :initialParticleSE;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -k on ".mico";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -k on ".micr";
	setAttr -k on ".micg";
	setAttr -k on ".micb";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
select -ne :defaultResolution;
	setAttr -av -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -av -k on ".w";
	setAttr -av -k on ".h";
	setAttr -av -k on ".pa" 1;
	setAttr -av -k on ".al";
	setAttr -av -k on ".dar";
	setAttr -av -k on ".ldar";
	setAttr -k on ".dpi";
	setAttr -av -k on ".off";
	setAttr -av -k on ".fld";
	setAttr -av -k on ".zsl";
	setAttr -k on ".isu";
	setAttr -k on ".pdu";
select -ne :hardwareRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k off -cb on ".ctrs" 256;
	setAttr -av -k off -cb on ".btrs" 512;
	setAttr -k off -cb on ".fbfm";
	setAttr -k off -cb on ".ehql";
	setAttr -k off -cb on ".eams";
	setAttr -k off -cb on ".eeaa";
	setAttr -k off -cb on ".engm";
	setAttr -k off -cb on ".mes";
	setAttr -k off -cb on ".emb";
	setAttr -av -k off -cb on ".mbbf";
	setAttr -k off -cb on ".mbs";
	setAttr -k off -cb on ".trm";
	setAttr -k off -cb on ".tshc";
	setAttr -k off -cb on ".enpt";
	setAttr -k off -cb on ".clmt";
	setAttr -k off -cb on ".tcov";
	setAttr -k off -cb on ".lith";
	setAttr -k off -cb on ".sobc";
	setAttr -k off -cb on ".cuth";
	setAttr -k off -cb on ".hgcd";
	setAttr -k off -cb on ".hgci";
	setAttr -k off -cb on ".mgcs";
	setAttr -k off -cb on ".twa";
	setAttr -k off -cb on ".twz";
	setAttr -k on ".hwcc";
	setAttr -k on ".hwdp";
	setAttr -k on ".hwql";
	setAttr -k on ".hwfr";
	setAttr -k on ".soll";
	setAttr -k on ".sosl";
	setAttr -k on ".bswa";
	setAttr -k on ".shml";
	setAttr -k on ".hwel";
select -ne :ikSystem;
	setAttr -s 4 ".sol";
connectAttr "rootProxySkin_jnt.s" "spine1ProxySkin_jnt.is";
connectAttr "spine1ProxySkin_jnt.s" "spine2ProxySkin_jnt.is";
connectAttr "spine2ProxySkin_jnt.s" "spine3ProxySkin_jnt.is";
connectAttr "spine3ProxySkin_jnt.s" "neck1ProxySkin_jnt.is";
connectAttr "neck1ProxySkin_jnt.s" "neck2ProxySkin_jnt.is";
connectAttr "neck1ProxySkin_jnt.s" "head1ProxySkin_jnt.is";
connectAttr "jaw1LWRProxySkin_jnt.s" "jaw2LWRProxySkin_jnt.is";
connectAttr "jaw2LWRProxySkin_jnt.s" "jaw3LWRProxySkin_jnt.is";
connectAttr "jaw2LWRProxySkin_jnt.s" "lowerTeethProxySkin_jnt.is";
connectAttr "jaw1LWRProxySkin_jnt.s" "tongue1ProxySkin_jnt.is";
connectAttr "tongue1ProxySkin_jnt.s" "tongue2ProxySkin_jnt.is";
connectAttr "tongue2ProxySkin_jnt.s" "tongue3ProxySkin_jnt.is";
connectAttr "tongue3ProxySkin_jnt.s" "tongue4ProxySkin_jnt.is";
connectAttr "jaw1UPRProxySkin_jnt.s" "jaw2UPRProxySkin_jnt.is";
connectAttr "jaw1UPRProxySkin_jnt.s" "upperTeethProxySkin_jnt.is";
connectAttr "eyeProxySkinLFT_jnt.s" "lidProxySkinLFT_jnt.is";
connectAttr "eyeProxySkinLFT_jnt.s" "irisProxySkinLFT_jnt.is";
connectAttr "eyeProxySkinLFT_jnt.s" "eyeSpecProxySkinLFT_jnt.is";
connectAttr "eyeProxySkinRGT_jnt.s" "lidProxySkinRGT_jnt.is";
connectAttr "eyeProxySkinRGT_jnt.s" "irisProxySkinRGT_jnt.is";
connectAttr "eyeProxySkinRGT_jnt.s" "eyeSpecProxySkinRGT_jnt.is";
connectAttr "head1ProxySkin_jnt.s" "ear1ProxySkinLFT_jnt.is";
connectAttr "ear1ProxySkinLFT_jnt.s" "ear2ProxySkinLFT_jnt.is";
connectAttr "ear2ProxySkinLFT_jnt.s" "ear3ProxySkinLFT_jnt.is";
connectAttr "ear3ProxySkinLFT_jnt.s" "ear4ProxySkinLFT_jnt.is";
connectAttr "ear4ProxySkinLFT_jnt.s" "ear5ProxySkinLFT_jnt.is";
connectAttr "head1ProxySkin_jnt.s" "ear1ProxySkinRGT_jnt.is";
connectAttr "ear1ProxySkinRGT_jnt.s" "ear2ProxySkinRGT_jnt.is";
connectAttr "ear2ProxySkinRGT_jnt.s" "ear3ProxySkinRGT_jnt.is";
connectAttr "ear3ProxySkinRGT_jnt.s" "ear4ProxySkinRGT_jnt.is";
connectAttr "ear4ProxySkinRGT_jnt.s" "ear5ProxySkinRGT_jnt.is";
connectAttr "head1ProxySkin_jnt.s" "hornProxySkinLFT_jnt.is";
connectAttr "head1ProxySkin_jnt.s" "hornProxySkinRGT_jnt.is";
connectAttr "neck1ProxySkin_jnt.s" "neckRbnProxySkin_jnt.is";
connectAttr "spine2ProxySkin_jnt.s" "clav1ScapProxySkinLFT_jnt.is";
connectAttr "clav1ScapProxySkinLFT_jnt.s" "clav2ScapProxySkinLFT_jnt.is";
connectAttr "clav2ScapProxySkinLFT_jnt.s" "upLegFrontProxySkinLFT_jnt.is";
connectAttr "upLegFrontProxySkinLFT_jnt.s" "midLegFrontProxySkinLFT_jnt.is";
connectAttr "midLegFrontProxySkinLFT_jnt.s" "lowLegFrontProxySkinLFT_jnt.is";
connectAttr "lowLegFrontProxySkinLFT_jnt.s" "ankleFrontProxySkinLFT_jnt.is";
connectAttr "ankleFrontProxySkinLFT_jnt.s" "ballFrontProxySkinLFT_jnt.is";
connectAttr "ballFrontProxySkinLFT_jnt.s" "toeFrontProxySkinLFT_jnt.is";
connectAttr "ballFrontProxySkinBtwAllLFT_grp_pointConstraint1.ctx" "ballFrontProxySkinBtwAllLFT_grp.tx"
		;
connectAttr "ballFrontProxySkinBtwAllLFT_grp_pointConstraint1.cty" "ballFrontProxySkinBtwAllLFT_grp.ty"
		;
connectAttr "ballFrontProxySkinBtwAllLFT_grp_pointConstraint1.ctz" "ballFrontProxySkinBtwAllLFT_grp.tz"
		;
connectAttr "ballFrontProxySkinBtwIkLFT_jnt.s" "ballFrontProxySkinBtwEndIkLFT_jnt.is"
		;
connectAttr "ballFrontProxySkinBtwEndIkLFT_jnt.tx" "ballFrontProxySkinBtwEndLFT_eff.tx"
		;
connectAttr "ballFrontProxySkinBtwEndIkLFT_jnt.ty" "ballFrontProxySkinBtwEndLFT_eff.ty"
		;
connectAttr "ballFrontProxySkinBtwEndIkLFT_jnt.tz" "ballFrontProxySkinBtwEndLFT_eff.tz"
		;
connectAttr "ballFrontProxySkinBtwAimLFT_jnt_aimConstraint1.crx" "ballFrontProxySkinBtwAimLFT_jnt.rx"
		;
connectAttr "ballFrontProxySkinBtwAimLFT_jnt_aimConstraint1.cry" "ballFrontProxySkinBtwAimLFT_jnt.ry"
		;
connectAttr "ballFrontProxySkinBtwAimLFT_jnt_aimConstraint1.crz" "ballFrontProxySkinBtwAimLFT_jnt.rz"
		;
connectAttr "ballFrontProxySkinBtwAimLFT_jnt.s" "ballFrontProxySkinBtwAimEndLFT_jnt.is"
		;
connectAttr "ballFrontProxySkinBtwAimLFT_jnt.pim" "ballFrontProxySkinBtwAimLFT_jnt_aimConstraint1.cpim"
		;
connectAttr "ballFrontProxySkinBtwAimLFT_jnt.t" "ballFrontProxySkinBtwAimLFT_jnt_aimConstraint1.ct"
		;
connectAttr "ballFrontProxySkinBtwAimLFT_jnt.rp" "ballFrontProxySkinBtwAimLFT_jnt_aimConstraint1.crp"
		;
connectAttr "ballFrontProxySkinBtwAimLFT_jnt.rpt" "ballFrontProxySkinBtwAimLFT_jnt_aimConstraint1.crt"
		;
connectAttr "ballFrontProxySkinBtwAimLFT_jnt.ro" "ballFrontProxySkinBtwAimLFT_jnt_aimConstraint1.cro"
		;
connectAttr "ballFrontProxySkinBtwAimLFT_jnt.jo" "ballFrontProxySkinBtwAimLFT_jnt_aimConstraint1.cjo"
		;
connectAttr "ballFrontProxySkinBtwAimLFT_jnt.is" "ballFrontProxySkinBtwAimLFT_jnt_aimConstraint1.is"
		;
connectAttr "toeFrontProxySkinLFT_jnt.t" "ballFrontProxySkinBtwAimLFT_jnt_aimConstraint1.tg[0].tt"
		;
connectAttr "toeFrontProxySkinLFT_jnt.rp" "ballFrontProxySkinBtwAimLFT_jnt_aimConstraint1.tg[0].trp"
		;
connectAttr "toeFrontProxySkinLFT_jnt.rpt" "ballFrontProxySkinBtwAimLFT_jnt_aimConstraint1.tg[0].trt"
		;
connectAttr "toeFrontProxySkinLFT_jnt.pm" "ballFrontProxySkinBtwAimLFT_jnt_aimConstraint1.tg[0].tpm"
		;
connectAttr "ballFrontProxySkinBtwAimLFT_jnt_aimConstraint1.w0" "ballFrontProxySkinBtwAimLFT_jnt_aimConstraint1.tg[0].tw"
		;
connectAttr "ballFrontProxySkinBtwUpLFT_grp.wm" "ballFrontProxySkinBtwAimLFT_jnt_aimConstraint1.wum"
		;
connectAttr "ballFrontProxySkinBtwVoidLFT_jnt.s" "ballFrontProxySkinBtwVoidEndLFT_jnt.is"
		;
connectAttr "ballFrontProxySkinBtwIkLFT_jnt.msg" "ballFrontProxySkinBtwLFT_ikHndl.hsj"
		;
connectAttr "ballFrontProxySkinBtwEndLFT_eff.hp" "ballFrontProxySkinBtwLFT_ikHndl.hee"
		;
connectAttr "ikRPsolver.msg" "ballFrontProxySkinBtwLFT_ikHndl.hsv";
connectAttr "ballFrontProxySkinBtwLFT_ikHndl_pointConstraint1.ctx" "ballFrontProxySkinBtwLFT_ikHndl.tx"
		;
connectAttr "ballFrontProxySkinBtwLFT_ikHndl_pointConstraint1.cty" "ballFrontProxySkinBtwLFT_ikHndl.ty"
		;
connectAttr "ballFrontProxySkinBtwLFT_ikHndl_pointConstraint1.ctz" "ballFrontProxySkinBtwLFT_ikHndl.tz"
		;
connectAttr "ballFrontProxySkinBtwLFT_ikHndl.pim" "ballFrontProxySkinBtwLFT_ikHndl_pointConstraint1.cpim"
		;
connectAttr "ballFrontProxySkinBtwLFT_ikHndl.rp" "ballFrontProxySkinBtwLFT_ikHndl_pointConstraint1.crp"
		;
connectAttr "ballFrontProxySkinBtwLFT_ikHndl.rpt" "ballFrontProxySkinBtwLFT_ikHndl_pointConstraint1.crt"
		;
connectAttr "ballFrontProxySkinBtwAimEndLFT_jnt.t" "ballFrontProxySkinBtwLFT_ikHndl_pointConstraint1.tg[0].tt"
		;
connectAttr "ballFrontProxySkinBtwAimEndLFT_jnt.rp" "ballFrontProxySkinBtwLFT_ikHndl_pointConstraint1.tg[0].trp"
		;
connectAttr "ballFrontProxySkinBtwAimEndLFT_jnt.rpt" "ballFrontProxySkinBtwLFT_ikHndl_pointConstraint1.tg[0].trt"
		;
connectAttr "ballFrontProxySkinBtwAimEndLFT_jnt.pm" "ballFrontProxySkinBtwLFT_ikHndl_pointConstraint1.tg[0].tpm"
		;
connectAttr "ballFrontProxySkinBtwLFT_ikHndl_pointConstraint1.w0" "ballFrontProxySkinBtwLFT_ikHndl_pointConstraint1.tg[0].tw"
		;
connectAttr "ballFrontProxySkinBtwVoidEndLFT_jnt.t" "ballFrontProxySkinBtwLFT_ikHndl_pointConstraint1.tg[1].tt"
		;
connectAttr "ballFrontProxySkinBtwVoidEndLFT_jnt.rp" "ballFrontProxySkinBtwLFT_ikHndl_pointConstraint1.tg[1].trp"
		;
connectAttr "ballFrontProxySkinBtwVoidEndLFT_jnt.rpt" "ballFrontProxySkinBtwLFT_ikHndl_pointConstraint1.tg[1].trt"
		;
connectAttr "ballFrontProxySkinBtwVoidEndLFT_jnt.pm" "ballFrontProxySkinBtwLFT_ikHndl_pointConstraint1.tg[1].tpm"
		;
connectAttr "ballFrontProxySkinBtwLFT_ikHndl_pointConstraint1.w1" "ballFrontProxySkinBtwLFT_ikHndl_pointConstraint1.tg[1].tw"
		;
connectAttr "ballFrontProxySkinBtwLFT_jnt.aimBtw" "ballFrontProxySkinBtwLFT_ikHndl_pointConstraint1.w0"
		;
connectAttr "ballFrontProxySkinDriveVoidLFT_adl.ox" "ballFrontProxySkinBtwLFT_ikHndl_pointConstraint1.w1"
		;
connectAttr "ballFrontProxySkinBtwLFT_jnt_parentConstraint1.ctx" "ballFrontProxySkinBtwLFT_jnt.tx"
		 -l on;
connectAttr "ballFrontProxySkinBtwLFT_jnt_parentConstraint1.cty" "ballFrontProxySkinBtwLFT_jnt.ty"
		 -l on;
connectAttr "ballFrontProxySkinBtwLFT_jnt_parentConstraint1.ctz" "ballFrontProxySkinBtwLFT_jnt.tz"
		 -l on;
connectAttr "ballFrontProxySkinBtwLFT_jnt_parentConstraint1.crx" "ballFrontProxySkinBtwLFT_jnt.rx"
		 -l on;
connectAttr "ballFrontProxySkinBtwLFT_jnt_parentConstraint1.cry" "ballFrontProxySkinBtwLFT_jnt.ry"
		 -l on;
connectAttr "ballFrontProxySkinBtwLFT_jnt_parentConstraint1.crz" "ballFrontProxySkinBtwLFT_jnt.rz"
		 -l on;
connectAttr "ballFrontProxySkinDoubleScaleALFT_adl.o" "ballFrontProxySkinBtwLFT_jnt.sx"
		 -l on;
connectAttr "ballFrontProxySkinDoubleScaleBLFT_adl.o" "ballFrontProxySkinBtwLFT_jnt.sz"
		 -l on;
connectAttr "ballFrontProxySkinBtwLFT_jnt.s" "ballFrontProxySkinBtwEndLFT_jnt.is"
		;
connectAttr "ballFrontProxySkinBtwLFT_jnt.ro" "ballFrontProxySkinBtwLFT_jnt_parentConstraint1.cro"
		;
connectAttr "ballFrontProxySkinBtwLFT_jnt.pim" "ballFrontProxySkinBtwLFT_jnt_parentConstraint1.cpim"
		;
connectAttr "ballFrontProxySkinBtwLFT_jnt.rp" "ballFrontProxySkinBtwLFT_jnt_parentConstraint1.crp"
		;
connectAttr "ballFrontProxySkinBtwLFT_jnt.rpt" "ballFrontProxySkinBtwLFT_jnt_parentConstraint1.crt"
		;
connectAttr "ballFrontProxySkinBtwLFT_jnt.jo" "ballFrontProxySkinBtwLFT_jnt_parentConstraint1.cjo"
		;
connectAttr "ballFrontProxySkinBtwIkLFT_jnt.t" "ballFrontProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tt"
		;
connectAttr "ballFrontProxySkinBtwIkLFT_jnt.rp" "ballFrontProxySkinBtwLFT_jnt_parentConstraint1.tg[0].trp"
		;
connectAttr "ballFrontProxySkinBtwIkLFT_jnt.rpt" "ballFrontProxySkinBtwLFT_jnt_parentConstraint1.tg[0].trt"
		;
connectAttr "ballFrontProxySkinBtwIkLFT_jnt.r" "ballFrontProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tr"
		;
connectAttr "ballFrontProxySkinBtwIkLFT_jnt.ro" "ballFrontProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tro"
		;
connectAttr "ballFrontProxySkinBtwIkLFT_jnt.s" "ballFrontProxySkinBtwLFT_jnt_parentConstraint1.tg[0].ts"
		;
connectAttr "ballFrontProxySkinBtwIkLFT_jnt.pm" "ballFrontProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tpm"
		;
connectAttr "ballFrontProxySkinBtwIkLFT_jnt.jo" "ballFrontProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tjo"
		;
connectAttr "ballFrontProxySkinBtwIkLFT_jnt.ssc" "ballFrontProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tsc"
		;
connectAttr "ballFrontProxySkinBtwIkLFT_jnt.is" "ballFrontProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tis"
		;
connectAttr "ballFrontProxySkinBtwLFT_jnt_parentConstraint1.w0" "ballFrontProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tw"
		;
connectAttr "ballFrontProxySkinBtwAllLFT_grp.pim" "ballFrontProxySkinBtwAllLFT_grp_pointConstraint1.cpim"
		;
connectAttr "ballFrontProxySkinBtwAllLFT_grp.rp" "ballFrontProxySkinBtwAllLFT_grp_pointConstraint1.crp"
		;
connectAttr "ballFrontProxySkinBtwAllLFT_grp.rpt" "ballFrontProxySkinBtwAllLFT_grp_pointConstraint1.crt"
		;
connectAttr "ballFrontProxySkinLFT_jnt.t" "ballFrontProxySkinBtwAllLFT_grp_pointConstraint1.tg[0].tt"
		;
connectAttr "ballFrontProxySkinLFT_jnt.rp" "ballFrontProxySkinBtwAllLFT_grp_pointConstraint1.tg[0].trp"
		;
connectAttr "ballFrontProxySkinLFT_jnt.rpt" "ballFrontProxySkinBtwAllLFT_grp_pointConstraint1.tg[0].trt"
		;
connectAttr "ballFrontProxySkinLFT_jnt.pm" "ballFrontProxySkinBtwAllLFT_grp_pointConstraint1.tg[0].tpm"
		;
connectAttr "ballFrontProxySkinBtwAllLFT_grp_pointConstraint1.w0" "ballFrontProxySkinBtwAllLFT_grp_pointConstraint1.tg[0].tw"
		;
connectAttr "lowLegFrontProxySkinLFT_jnt.s" "lowLegRbnDtl1FrontProxySkinLFT_jnt.is"
		;
connectAttr "lowLegFrontProxySkinLFT_jnt.s" "lowLegRbnDtl2FrontProxySkinLFT_jnt.is"
		;
connectAttr "lowLegFrontProxySkinLFT_jnt.s" "lowLegRbnDtl3FrontProxySkinLFT_jnt.is"
		;
connectAttr "lowLegFrontProxySkinLFT_jnt.s" "lowLegRbnDtl4FrontProxySkinLFT_jnt.is"
		;
connectAttr "lowLegFrontProxySkinLFT_jnt.s" "lowLegRbnDtl5FrontProxySkinLFT_jnt.is"
		;
connectAttr "ankleFrontProxySkinBtwAllLFT_grp_pointConstraint1.ctx" "ankleFrontProxySkinBtwAllLFT_grp.tx"
		;
connectAttr "ankleFrontProxySkinBtwAllLFT_grp_pointConstraint1.cty" "ankleFrontProxySkinBtwAllLFT_grp.ty"
		;
connectAttr "ankleFrontProxySkinBtwAllLFT_grp_pointConstraint1.ctz" "ankleFrontProxySkinBtwAllLFT_grp.tz"
		;
connectAttr "ankleFrontProxySkinBtwIkLFT_jnt.s" "ankleFrontProxySkinBtwEndIkLFT_jnt.is"
		;
connectAttr "ankleFrontProxySkinBtwEndIkLFT_jnt.tx" "ankleFrontProxySkinBtwEndLFT_eff.tx"
		;
connectAttr "ankleFrontProxySkinBtwEndIkLFT_jnt.ty" "ankleFrontProxySkinBtwEndLFT_eff.ty"
		;
connectAttr "ankleFrontProxySkinBtwEndIkLFT_jnt.tz" "ankleFrontProxySkinBtwEndLFT_eff.tz"
		;
connectAttr "ankleFrontProxySkinBtwAimLFT_jnt_aimConstraint1.crx" "ankleFrontProxySkinBtwAimLFT_jnt.rx"
		;
connectAttr "ankleFrontProxySkinBtwAimLFT_jnt_aimConstraint1.cry" "ankleFrontProxySkinBtwAimLFT_jnt.ry"
		;
connectAttr "ankleFrontProxySkinBtwAimLFT_jnt_aimConstraint1.crz" "ankleFrontProxySkinBtwAimLFT_jnt.rz"
		;
connectAttr "ankleFrontProxySkinBtwAimLFT_jnt.s" "ankleFrontProxySkinBtwAimEndLFT_jnt.is"
		;
connectAttr "ankleFrontProxySkinBtwAimLFT_jnt.pim" "ankleFrontProxySkinBtwAimLFT_jnt_aimConstraint1.cpim"
		;
connectAttr "ankleFrontProxySkinBtwAimLFT_jnt.t" "ankleFrontProxySkinBtwAimLFT_jnt_aimConstraint1.ct"
		;
connectAttr "ankleFrontProxySkinBtwAimLFT_jnt.rp" "ankleFrontProxySkinBtwAimLFT_jnt_aimConstraint1.crp"
		;
connectAttr "ankleFrontProxySkinBtwAimLFT_jnt.rpt" "ankleFrontProxySkinBtwAimLFT_jnt_aimConstraint1.crt"
		;
connectAttr "ankleFrontProxySkinBtwAimLFT_jnt.ro" "ankleFrontProxySkinBtwAimLFT_jnt_aimConstraint1.cro"
		;
connectAttr "ankleFrontProxySkinBtwAimLFT_jnt.jo" "ankleFrontProxySkinBtwAimLFT_jnt_aimConstraint1.cjo"
		;
connectAttr "ankleFrontProxySkinBtwAimLFT_jnt.is" "ankleFrontProxySkinBtwAimLFT_jnt_aimConstraint1.is"
		;
connectAttr "ballFrontProxySkinLFT_jnt.t" "ankleFrontProxySkinBtwAimLFT_jnt_aimConstraint1.tg[0].tt"
		;
connectAttr "ballFrontProxySkinLFT_jnt.rp" "ankleFrontProxySkinBtwAimLFT_jnt_aimConstraint1.tg[0].trp"
		;
connectAttr "ballFrontProxySkinLFT_jnt.rpt" "ankleFrontProxySkinBtwAimLFT_jnt_aimConstraint1.tg[0].trt"
		;
connectAttr "ballFrontProxySkinLFT_jnt.pm" "ankleFrontProxySkinBtwAimLFT_jnt_aimConstraint1.tg[0].tpm"
		;
connectAttr "ankleFrontProxySkinBtwAimLFT_jnt_aimConstraint1.w0" "ankleFrontProxySkinBtwAimLFT_jnt_aimConstraint1.tg[0].tw"
		;
connectAttr "ankleFrontProxySkinBtwUpLFT_grp.wm" "ankleFrontProxySkinBtwAimLFT_jnt_aimConstraint1.wum"
		;
connectAttr "ankleFrontProxySkinBtwVoidLFT_jnt.s" "ankleFrontProxySkinBtwVoidEndLFT_jnt.is"
		;
connectAttr "ankleFrontProxySkinBtwIkLFT_jnt.msg" "ankleFrontProxySkinBtwLFT_ikHndl.hsj"
		;
connectAttr "ankleFrontProxySkinBtwEndLFT_eff.hp" "ankleFrontProxySkinBtwLFT_ikHndl.hee"
		;
connectAttr "ikRPsolver.msg" "ankleFrontProxySkinBtwLFT_ikHndl.hsv";
connectAttr "ankleFrontProxySkinBtwLFT_ikHndl_pointConstraint1.ctx" "ankleFrontProxySkinBtwLFT_ikHndl.tx"
		;
connectAttr "ankleFrontProxySkinBtwLFT_ikHndl_pointConstraint1.cty" "ankleFrontProxySkinBtwLFT_ikHndl.ty"
		;
connectAttr "ankleFrontProxySkinBtwLFT_ikHndl_pointConstraint1.ctz" "ankleFrontProxySkinBtwLFT_ikHndl.tz"
		;
connectAttr "ankleFrontProxySkinBtwLFT_ikHndl.pim" "ankleFrontProxySkinBtwLFT_ikHndl_pointConstraint1.cpim"
		;
connectAttr "ankleFrontProxySkinBtwLFT_ikHndl.rp" "ankleFrontProxySkinBtwLFT_ikHndl_pointConstraint1.crp"
		;
connectAttr "ankleFrontProxySkinBtwLFT_ikHndl.rpt" "ankleFrontProxySkinBtwLFT_ikHndl_pointConstraint1.crt"
		;
connectAttr "ankleFrontProxySkinBtwAimEndLFT_jnt.t" "ankleFrontProxySkinBtwLFT_ikHndl_pointConstraint1.tg[0].tt"
		;
connectAttr "ankleFrontProxySkinBtwAimEndLFT_jnt.rp" "ankleFrontProxySkinBtwLFT_ikHndl_pointConstraint1.tg[0].trp"
		;
connectAttr "ankleFrontProxySkinBtwAimEndLFT_jnt.rpt" "ankleFrontProxySkinBtwLFT_ikHndl_pointConstraint1.tg[0].trt"
		;
connectAttr "ankleFrontProxySkinBtwAimEndLFT_jnt.pm" "ankleFrontProxySkinBtwLFT_ikHndl_pointConstraint1.tg[0].tpm"
		;
connectAttr "ankleFrontProxySkinBtwLFT_ikHndl_pointConstraint1.w0" "ankleFrontProxySkinBtwLFT_ikHndl_pointConstraint1.tg[0].tw"
		;
connectAttr "ankleFrontProxySkinBtwVoidEndLFT_jnt.t" "ankleFrontProxySkinBtwLFT_ikHndl_pointConstraint1.tg[1].tt"
		;
connectAttr "ankleFrontProxySkinBtwVoidEndLFT_jnt.rp" "ankleFrontProxySkinBtwLFT_ikHndl_pointConstraint1.tg[1].trp"
		;
connectAttr "ankleFrontProxySkinBtwVoidEndLFT_jnt.rpt" "ankleFrontProxySkinBtwLFT_ikHndl_pointConstraint1.tg[1].trt"
		;
connectAttr "ankleFrontProxySkinBtwVoidEndLFT_jnt.pm" "ankleFrontProxySkinBtwLFT_ikHndl_pointConstraint1.tg[1].tpm"
		;
connectAttr "ankleFrontProxySkinBtwLFT_ikHndl_pointConstraint1.w1" "ankleFrontProxySkinBtwLFT_ikHndl_pointConstraint1.tg[1].tw"
		;
connectAttr "ankleFrontProxySkinBtwLFT_jnt.aimBtw" "ankleFrontProxySkinBtwLFT_ikHndl_pointConstraint1.w0"
		;
connectAttr "ankleFrontProxySkinDriveVoidLFT_adl.ox" "ankleFrontProxySkinBtwLFT_ikHndl_pointConstraint1.w1"
		;
connectAttr "ankleFrontProxySkinBtwLFT_jnt_parentConstraint1.ctx" "ankleFrontProxySkinBtwLFT_jnt.tx"
		 -l on;
connectAttr "ankleFrontProxySkinBtwLFT_jnt_parentConstraint1.cty" "ankleFrontProxySkinBtwLFT_jnt.ty"
		 -l on;
connectAttr "ankleFrontProxySkinBtwLFT_jnt_parentConstraint1.ctz" "ankleFrontProxySkinBtwLFT_jnt.tz"
		 -l on;
connectAttr "ankleFrontProxySkinBtwLFT_jnt_parentConstraint1.crx" "ankleFrontProxySkinBtwLFT_jnt.rx"
		 -l on;
connectAttr "ankleFrontProxySkinBtwLFT_jnt_parentConstraint1.cry" "ankleFrontProxySkinBtwLFT_jnt.ry"
		 -l on;
connectAttr "ankleFrontProxySkinBtwLFT_jnt_parentConstraint1.crz" "ankleFrontProxySkinBtwLFT_jnt.rz"
		 -l on;
connectAttr "ankleFrontProxySkinDoubleScaleALFT_adl.o" "ankleFrontProxySkinBtwLFT_jnt.sx"
		 -l on;
connectAttr "ankleFrontProxySkinDoubleScaleBLFT_adl.o" "ankleFrontProxySkinBtwLFT_jnt.sz"
		 -l on;
connectAttr "ankleFrontProxySkinBtwLFT_jnt.s" "ankleFrontProxySkinBtwEndLFT_jnt.is"
		;
connectAttr "ankleFrontProxySkinBtwLFT_jnt.ro" "ankleFrontProxySkinBtwLFT_jnt_parentConstraint1.cro"
		;
connectAttr "ankleFrontProxySkinBtwLFT_jnt.pim" "ankleFrontProxySkinBtwLFT_jnt_parentConstraint1.cpim"
		;
connectAttr "ankleFrontProxySkinBtwLFT_jnt.rp" "ankleFrontProxySkinBtwLFT_jnt_parentConstraint1.crp"
		;
connectAttr "ankleFrontProxySkinBtwLFT_jnt.rpt" "ankleFrontProxySkinBtwLFT_jnt_parentConstraint1.crt"
		;
connectAttr "ankleFrontProxySkinBtwLFT_jnt.jo" "ankleFrontProxySkinBtwLFT_jnt_parentConstraint1.cjo"
		;
connectAttr "ankleFrontProxySkinBtwIkLFT_jnt.t" "ankleFrontProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tt"
		;
connectAttr "ankleFrontProxySkinBtwIkLFT_jnt.rp" "ankleFrontProxySkinBtwLFT_jnt_parentConstraint1.tg[0].trp"
		;
connectAttr "ankleFrontProxySkinBtwIkLFT_jnt.rpt" "ankleFrontProxySkinBtwLFT_jnt_parentConstraint1.tg[0].trt"
		;
connectAttr "ankleFrontProxySkinBtwIkLFT_jnt.r" "ankleFrontProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tr"
		;
connectAttr "ankleFrontProxySkinBtwIkLFT_jnt.ro" "ankleFrontProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tro"
		;
connectAttr "ankleFrontProxySkinBtwIkLFT_jnt.s" "ankleFrontProxySkinBtwLFT_jnt_parentConstraint1.tg[0].ts"
		;
connectAttr "ankleFrontProxySkinBtwIkLFT_jnt.pm" "ankleFrontProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tpm"
		;
connectAttr "ankleFrontProxySkinBtwIkLFT_jnt.jo" "ankleFrontProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tjo"
		;
connectAttr "ankleFrontProxySkinBtwIkLFT_jnt.ssc" "ankleFrontProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tsc"
		;
connectAttr "ankleFrontProxySkinBtwIkLFT_jnt.is" "ankleFrontProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tis"
		;
connectAttr "ankleFrontProxySkinBtwLFT_jnt_parentConstraint1.w0" "ankleFrontProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tw"
		;
connectAttr "ankleFrontProxySkinBtwAllLFT_grp.pim" "ankleFrontProxySkinBtwAllLFT_grp_pointConstraint1.cpim"
		;
connectAttr "ankleFrontProxySkinBtwAllLFT_grp.rp" "ankleFrontProxySkinBtwAllLFT_grp_pointConstraint1.crp"
		;
connectAttr "ankleFrontProxySkinBtwAllLFT_grp.rpt" "ankleFrontProxySkinBtwAllLFT_grp_pointConstraint1.crt"
		;
connectAttr "ankleFrontProxySkinLFT_jnt.t" "ankleFrontProxySkinBtwAllLFT_grp_pointConstraint1.tg[0].tt"
		;
connectAttr "ankleFrontProxySkinLFT_jnt.rp" "ankleFrontProxySkinBtwAllLFT_grp_pointConstraint1.tg[0].trp"
		;
connectAttr "ankleFrontProxySkinLFT_jnt.rpt" "ankleFrontProxySkinBtwAllLFT_grp_pointConstraint1.tg[0].trt"
		;
connectAttr "ankleFrontProxySkinLFT_jnt.pm" "ankleFrontProxySkinBtwAllLFT_grp_pointConstraint1.tg[0].tpm"
		;
connectAttr "ankleFrontProxySkinBtwAllLFT_grp_pointConstraint1.w0" "ankleFrontProxySkinBtwAllLFT_grp_pointConstraint1.tg[0].tw"
		;
connectAttr "midLegFrontProxySkinLFT_jnt.s" "midLegRbnDtl1FrontProxySkinLFT_jnt.is"
		;
connectAttr "midLegFrontProxySkinLFT_jnt.s" "midLegRbnDtl2FrontProxySkinLFT_jnt.is"
		;
connectAttr "midLegFrontProxySkinLFT_jnt.s" "midLegRbnDtl3FrontProxySkinLFT_jnt.is"
		;
connectAttr "midLegFrontProxySkinLFT_jnt.s" "midLegRbnDtl4FrontProxySkinLFT_jnt.is"
		;
connectAttr "midLegFrontProxySkinLFT_jnt.s" "midLegRbnDtl5FrontProxySkinLFT_jnt.is"
		;
connectAttr "lowLegFrontProxySkinBtwAllLFT_grp_pointConstraint1.ctx" "lowLegFrontProxySkinBtwAllLFT_grp.tx"
		;
connectAttr "lowLegFrontProxySkinBtwAllLFT_grp_pointConstraint1.cty" "lowLegFrontProxySkinBtwAllLFT_grp.ty"
		;
connectAttr "lowLegFrontProxySkinBtwAllLFT_grp_pointConstraint1.ctz" "lowLegFrontProxySkinBtwAllLFT_grp.tz"
		;
connectAttr "lowLegFrontProxySkinBtwIkLFT_jnt.s" "lowLegFrontProxySkinBtwEndIkLFT_jnt.is"
		;
connectAttr "lowLegFrontProxySkinBtwEndIkLFT_jnt.tx" "lowLegFrontProxySkinBtwEndLFT_eff.tx"
		;
connectAttr "lowLegFrontProxySkinBtwEndIkLFT_jnt.ty" "lowLegFrontProxySkinBtwEndLFT_eff.ty"
		;
connectAttr "lowLegFrontProxySkinBtwEndIkLFT_jnt.tz" "lowLegFrontProxySkinBtwEndLFT_eff.tz"
		;
connectAttr "lowLegFrontProxySkinBtwAimLFT_jnt_aimConstraint1.crx" "lowLegFrontProxySkinBtwAimLFT_jnt.rx"
		;
connectAttr "lowLegFrontProxySkinBtwAimLFT_jnt_aimConstraint1.cry" "lowLegFrontProxySkinBtwAimLFT_jnt.ry"
		;
connectAttr "lowLegFrontProxySkinBtwAimLFT_jnt_aimConstraint1.crz" "lowLegFrontProxySkinBtwAimLFT_jnt.rz"
		;
connectAttr "lowLegFrontProxySkinBtwAimLFT_jnt.s" "lowLegFrontProxySkinBtwAimEndLFT_jnt.is"
		;
connectAttr "lowLegFrontProxySkinBtwAimLFT_jnt.pim" "lowLegFrontProxySkinBtwAimLFT_jnt_aimConstraint1.cpim"
		;
connectAttr "lowLegFrontProxySkinBtwAimLFT_jnt.t" "lowLegFrontProxySkinBtwAimLFT_jnt_aimConstraint1.ct"
		;
connectAttr "lowLegFrontProxySkinBtwAimLFT_jnt.rp" "lowLegFrontProxySkinBtwAimLFT_jnt_aimConstraint1.crp"
		;
connectAttr "lowLegFrontProxySkinBtwAimLFT_jnt.rpt" "lowLegFrontProxySkinBtwAimLFT_jnt_aimConstraint1.crt"
		;
connectAttr "lowLegFrontProxySkinBtwAimLFT_jnt.ro" "lowLegFrontProxySkinBtwAimLFT_jnt_aimConstraint1.cro"
		;
connectAttr "lowLegFrontProxySkinBtwAimLFT_jnt.jo" "lowLegFrontProxySkinBtwAimLFT_jnt_aimConstraint1.cjo"
		;
connectAttr "lowLegFrontProxySkinBtwAimLFT_jnt.is" "lowLegFrontProxySkinBtwAimLFT_jnt_aimConstraint1.is"
		;
connectAttr "lowLegRbnDtl1FrontProxySkinLFT_jnt.t" "lowLegFrontProxySkinBtwAimLFT_jnt_aimConstraint1.tg[0].tt"
		;
connectAttr "lowLegRbnDtl1FrontProxySkinLFT_jnt.rp" "lowLegFrontProxySkinBtwAimLFT_jnt_aimConstraint1.tg[0].trp"
		;
connectAttr "lowLegRbnDtl1FrontProxySkinLFT_jnt.rpt" "lowLegFrontProxySkinBtwAimLFT_jnt_aimConstraint1.tg[0].trt"
		;
connectAttr "lowLegRbnDtl1FrontProxySkinLFT_jnt.pm" "lowLegFrontProxySkinBtwAimLFT_jnt_aimConstraint1.tg[0].tpm"
		;
connectAttr "lowLegFrontProxySkinBtwAimLFT_jnt_aimConstraint1.w0" "lowLegFrontProxySkinBtwAimLFT_jnt_aimConstraint1.tg[0].tw"
		;
connectAttr "lowLegFrontProxySkinBtwUpLFT_grp.wm" "lowLegFrontProxySkinBtwAimLFT_jnt_aimConstraint1.wum"
		;
connectAttr "lowLegFrontProxySkinBtwVoidLFT_jnt.s" "lowLegFrontProxySkinBtwVoidEndLFT_jnt.is"
		;
connectAttr "lowLegFrontProxySkinBtwIkLFT_jnt.msg" "lowLegFrontProxySkinBtwLFT_ikHndl.hsj"
		;
connectAttr "lowLegFrontProxySkinBtwEndLFT_eff.hp" "lowLegFrontProxySkinBtwLFT_ikHndl.hee"
		;
connectAttr "ikRPsolver.msg" "lowLegFrontProxySkinBtwLFT_ikHndl.hsv";
connectAttr "lowLegFrontProxySkinBtwLFT_ikHndl_pointConstraint1.ctx" "lowLegFrontProxySkinBtwLFT_ikHndl.tx"
		;
connectAttr "lowLegFrontProxySkinBtwLFT_ikHndl_pointConstraint1.cty" "lowLegFrontProxySkinBtwLFT_ikHndl.ty"
		;
connectAttr "lowLegFrontProxySkinBtwLFT_ikHndl_pointConstraint1.ctz" "lowLegFrontProxySkinBtwLFT_ikHndl.tz"
		;
connectAttr "lowLegFrontProxySkinBtwLFT_ikHndl.pim" "lowLegFrontProxySkinBtwLFT_ikHndl_pointConstraint1.cpim"
		;
connectAttr "lowLegFrontProxySkinBtwLFT_ikHndl.rp" "lowLegFrontProxySkinBtwLFT_ikHndl_pointConstraint1.crp"
		;
connectAttr "lowLegFrontProxySkinBtwLFT_ikHndl.rpt" "lowLegFrontProxySkinBtwLFT_ikHndl_pointConstraint1.crt"
		;
connectAttr "lowLegFrontProxySkinBtwAimEndLFT_jnt.t" "lowLegFrontProxySkinBtwLFT_ikHndl_pointConstraint1.tg[0].tt"
		;
connectAttr "lowLegFrontProxySkinBtwAimEndLFT_jnt.rp" "lowLegFrontProxySkinBtwLFT_ikHndl_pointConstraint1.tg[0].trp"
		;
connectAttr "lowLegFrontProxySkinBtwAimEndLFT_jnt.rpt" "lowLegFrontProxySkinBtwLFT_ikHndl_pointConstraint1.tg[0].trt"
		;
connectAttr "lowLegFrontProxySkinBtwAimEndLFT_jnt.pm" "lowLegFrontProxySkinBtwLFT_ikHndl_pointConstraint1.tg[0].tpm"
		;
connectAttr "lowLegFrontProxySkinBtwLFT_ikHndl_pointConstraint1.w0" "lowLegFrontProxySkinBtwLFT_ikHndl_pointConstraint1.tg[0].tw"
		;
connectAttr "lowLegFrontProxySkinBtwVoidEndLFT_jnt.t" "lowLegFrontProxySkinBtwLFT_ikHndl_pointConstraint1.tg[1].tt"
		;
connectAttr "lowLegFrontProxySkinBtwVoidEndLFT_jnt.rp" "lowLegFrontProxySkinBtwLFT_ikHndl_pointConstraint1.tg[1].trp"
		;
connectAttr "lowLegFrontProxySkinBtwVoidEndLFT_jnt.rpt" "lowLegFrontProxySkinBtwLFT_ikHndl_pointConstraint1.tg[1].trt"
		;
connectAttr "lowLegFrontProxySkinBtwVoidEndLFT_jnt.pm" "lowLegFrontProxySkinBtwLFT_ikHndl_pointConstraint1.tg[1].tpm"
		;
connectAttr "lowLegFrontProxySkinBtwLFT_ikHndl_pointConstraint1.w1" "lowLegFrontProxySkinBtwLFT_ikHndl_pointConstraint1.tg[1].tw"
		;
connectAttr "lowLegFrontProxySkinBtwLFT_jnt.aimBtw" "lowLegFrontProxySkinBtwLFT_ikHndl_pointConstraint1.w0"
		;
connectAttr "lowLegFrontProxySkinDriveVoidLFT_adl.ox" "lowLegFrontProxySkinBtwLFT_ikHndl_pointConstraint1.w1"
		;
connectAttr "lowLegFrontProxySkinDoubleScaleALFT_adl.o" "lowLegFrontProxySkinBtwLFT_jnt.sx"
		 -l on;
connectAttr "lowLegFrontProxySkinDoubleScaleBLFT_adl.o" "lowLegFrontProxySkinBtwLFT_jnt.sz"
		 -l on;
connectAttr "lowLegFrontProxySkinBtwLFT_jnt_parentConstraint1.ctx" "lowLegFrontProxySkinBtwLFT_jnt.tx"
		 -l on;
connectAttr "lowLegFrontProxySkinBtwLFT_jnt_parentConstraint1.cty" "lowLegFrontProxySkinBtwLFT_jnt.ty"
		 -l on;
connectAttr "lowLegFrontProxySkinBtwLFT_jnt_parentConstraint1.ctz" "lowLegFrontProxySkinBtwLFT_jnt.tz"
		 -l on;
connectAttr "lowLegFrontProxySkinBtwLFT_jnt_parentConstraint1.crx" "lowLegFrontProxySkinBtwLFT_jnt.rx"
		 -l on;
connectAttr "lowLegFrontProxySkinBtwLFT_jnt_parentConstraint1.cry" "lowLegFrontProxySkinBtwLFT_jnt.ry"
		 -l on;
connectAttr "lowLegFrontProxySkinBtwLFT_jnt_parentConstraint1.crz" "lowLegFrontProxySkinBtwLFT_jnt.rz"
		 -l on;
connectAttr "lowLegFrontProxySkinBtwLFT_jnt.s" "lowLegFrontProxySkinBtwEndLFT_jnt.is"
		;
connectAttr "lowLegFrontProxySkinBtwLFT_jnt.ro" "lowLegFrontProxySkinBtwLFT_jnt_parentConstraint1.cro"
		;
connectAttr "lowLegFrontProxySkinBtwLFT_jnt.pim" "lowLegFrontProxySkinBtwLFT_jnt_parentConstraint1.cpim"
		;
connectAttr "lowLegFrontProxySkinBtwLFT_jnt.rp" "lowLegFrontProxySkinBtwLFT_jnt_parentConstraint1.crp"
		;
connectAttr "lowLegFrontProxySkinBtwLFT_jnt.rpt" "lowLegFrontProxySkinBtwLFT_jnt_parentConstraint1.crt"
		;
connectAttr "lowLegFrontProxySkinBtwLFT_jnt.jo" "lowLegFrontProxySkinBtwLFT_jnt_parentConstraint1.cjo"
		;
connectAttr "lowLegFrontProxySkinBtwIkLFT_jnt.t" "lowLegFrontProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tt"
		;
connectAttr "lowLegFrontProxySkinBtwIkLFT_jnt.rp" "lowLegFrontProxySkinBtwLFT_jnt_parentConstraint1.tg[0].trp"
		;
connectAttr "lowLegFrontProxySkinBtwIkLFT_jnt.rpt" "lowLegFrontProxySkinBtwLFT_jnt_parentConstraint1.tg[0].trt"
		;
connectAttr "lowLegFrontProxySkinBtwIkLFT_jnt.r" "lowLegFrontProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tr"
		;
connectAttr "lowLegFrontProxySkinBtwIkLFT_jnt.ro" "lowLegFrontProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tro"
		;
connectAttr "lowLegFrontProxySkinBtwIkLFT_jnt.s" "lowLegFrontProxySkinBtwLFT_jnt_parentConstraint1.tg[0].ts"
		;
connectAttr "lowLegFrontProxySkinBtwIkLFT_jnt.pm" "lowLegFrontProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tpm"
		;
connectAttr "lowLegFrontProxySkinBtwIkLFT_jnt.jo" "lowLegFrontProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tjo"
		;
connectAttr "lowLegFrontProxySkinBtwIkLFT_jnt.ssc" "lowLegFrontProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tsc"
		;
connectAttr "lowLegFrontProxySkinBtwIkLFT_jnt.is" "lowLegFrontProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tis"
		;
connectAttr "lowLegFrontProxySkinBtwLFT_jnt_parentConstraint1.w0" "lowLegFrontProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tw"
		;
connectAttr "lowLegFrontProxySkinBtwAllLFT_grp.pim" "lowLegFrontProxySkinBtwAllLFT_grp_pointConstraint1.cpim"
		;
connectAttr "lowLegFrontProxySkinBtwAllLFT_grp.rp" "lowLegFrontProxySkinBtwAllLFT_grp_pointConstraint1.crp"
		;
connectAttr "lowLegFrontProxySkinBtwAllLFT_grp.rpt" "lowLegFrontProxySkinBtwAllLFT_grp_pointConstraint1.crt"
		;
connectAttr "lowLegFrontProxySkinLFT_jnt.t" "lowLegFrontProxySkinBtwAllLFT_grp_pointConstraint1.tg[0].tt"
		;
connectAttr "lowLegFrontProxySkinLFT_jnt.rp" "lowLegFrontProxySkinBtwAllLFT_grp_pointConstraint1.tg[0].trp"
		;
connectAttr "lowLegFrontProxySkinLFT_jnt.rpt" "lowLegFrontProxySkinBtwAllLFT_grp_pointConstraint1.tg[0].trt"
		;
connectAttr "lowLegFrontProxySkinLFT_jnt.pm" "lowLegFrontProxySkinBtwAllLFT_grp_pointConstraint1.tg[0].tpm"
		;
connectAttr "lowLegFrontProxySkinBtwAllLFT_grp_pointConstraint1.w0" "lowLegFrontProxySkinBtwAllLFT_grp_pointConstraint1.tg[0].tw"
		;
connectAttr "upLegFrontProxySkinLFT_jnt.s" "upLegRbnDtl1FrontProxySkinLFT_jnt.is"
		;
connectAttr "upLegFrontProxySkinLFT_jnt.s" "upLegRbnDtl2FrontProxySkinLFT_jnt.is"
		;
connectAttr "upLegFrontProxySkinLFT_jnt.s" "upLegRbnDtl3FrontProxySkinLFT_jnt.is"
		;
connectAttr "upLegFrontProxySkinLFT_jnt.s" "upLegRbnDtl4FrontProxySkinLFT_jnt.is"
		;
connectAttr "upLegFrontProxySkinLFT_jnt.s" "upLegRbnDtl5FrontProxySkinLFT_jnt.is"
		;
connectAttr "midLegFrontProxySkinBtwIkLFT_jnt.s" "midLegFrontProxySkinBtwEndIkLFT_jnt.is"
		;
connectAttr "midLegFrontProxySkinBtwEndIkLFT_jnt.tx" "midLegFrontProxySkinBtwEndLFT_eff.tx"
		;
connectAttr "midLegFrontProxySkinBtwEndIkLFT_jnt.ty" "midLegFrontProxySkinBtwEndLFT_eff.ty"
		;
connectAttr "midLegFrontProxySkinBtwEndIkLFT_jnt.tz" "midLegFrontProxySkinBtwEndLFT_eff.tz"
		;
connectAttr "midLegFrontProxySkinBtwAimLFT_jnt_aimConstraint1.crx" "midLegFrontProxySkinBtwAimLFT_jnt.rx"
		;
connectAttr "midLegFrontProxySkinBtwAimLFT_jnt_aimConstraint1.cry" "midLegFrontProxySkinBtwAimLFT_jnt.ry"
		;
connectAttr "midLegFrontProxySkinBtwAimLFT_jnt_aimConstraint1.crz" "midLegFrontProxySkinBtwAimLFT_jnt.rz"
		;
connectAttr "midLegFrontProxySkinBtwAimLFT_jnt.s" "midLegFrontProxySkinBtwAimEndLFT_jnt.is"
		;
connectAttr "midLegFrontProxySkinBtwAimLFT_jnt.pim" "midLegFrontProxySkinBtwAimLFT_jnt_aimConstraint1.cpim"
		;
connectAttr "midLegFrontProxySkinBtwAimLFT_jnt.t" "midLegFrontProxySkinBtwAimLFT_jnt_aimConstraint1.ct"
		;
connectAttr "midLegFrontProxySkinBtwAimLFT_jnt.rp" "midLegFrontProxySkinBtwAimLFT_jnt_aimConstraint1.crp"
		;
connectAttr "midLegFrontProxySkinBtwAimLFT_jnt.rpt" "midLegFrontProxySkinBtwAimLFT_jnt_aimConstraint1.crt"
		;
connectAttr "midLegFrontProxySkinBtwAimLFT_jnt.ro" "midLegFrontProxySkinBtwAimLFT_jnt_aimConstraint1.cro"
		;
connectAttr "midLegFrontProxySkinBtwAimLFT_jnt.jo" "midLegFrontProxySkinBtwAimLFT_jnt_aimConstraint1.cjo"
		;
connectAttr "midLegFrontProxySkinBtwAimLFT_jnt.is" "midLegFrontProxySkinBtwAimLFT_jnt_aimConstraint1.is"
		;
connectAttr "midLegRbnDtl1FrontProxySkinLFT_jnt.t" "midLegFrontProxySkinBtwAimLFT_jnt_aimConstraint1.tg[0].tt"
		;
connectAttr "midLegRbnDtl1FrontProxySkinLFT_jnt.rp" "midLegFrontProxySkinBtwAimLFT_jnt_aimConstraint1.tg[0].trp"
		;
connectAttr "midLegRbnDtl1FrontProxySkinLFT_jnt.rpt" "midLegFrontProxySkinBtwAimLFT_jnt_aimConstraint1.tg[0].trt"
		;
connectAttr "midLegRbnDtl1FrontProxySkinLFT_jnt.pm" "midLegFrontProxySkinBtwAimLFT_jnt_aimConstraint1.tg[0].tpm"
		;
connectAttr "midLegFrontProxySkinBtwAimLFT_jnt_aimConstraint1.w0" "midLegFrontProxySkinBtwAimLFT_jnt_aimConstraint1.tg[0].tw"
		;
connectAttr "midLegFrontProxySkinBtwUpLFT_grp.wm" "midLegFrontProxySkinBtwAimLFT_jnt_aimConstraint1.wum"
		;
connectAttr "midLegFrontProxySkinBtwVoidLFT_jnt.s" "midLegFrontProxySkinBtwVoidEndLFT_jnt.is"
		;
connectAttr "midLegFrontProxySkinBtwIkLFT_jnt.msg" "midLegFrontProxySkinBtwLFT_ikHndl.hsj"
		;
connectAttr "midLegFrontProxySkinBtwEndLFT_eff.hp" "midLegFrontProxySkinBtwLFT_ikHndl.hee"
		;
connectAttr "ikRPsolver.msg" "midLegFrontProxySkinBtwLFT_ikHndl.hsv";
connectAttr "midLegFrontProxySkinBtwLFT_ikHndl_pointConstraint1.ctx" "midLegFrontProxySkinBtwLFT_ikHndl.tx"
		;
connectAttr "midLegFrontProxySkinBtwLFT_ikHndl_pointConstraint1.cty" "midLegFrontProxySkinBtwLFT_ikHndl.ty"
		;
connectAttr "midLegFrontProxySkinBtwLFT_ikHndl_pointConstraint1.ctz" "midLegFrontProxySkinBtwLFT_ikHndl.tz"
		;
connectAttr "midLegFrontProxySkinBtwLFT_ikHndl.pim" "midLegFrontProxySkinBtwLFT_ikHndl_pointConstraint1.cpim"
		;
connectAttr "midLegFrontProxySkinBtwLFT_ikHndl.rp" "midLegFrontProxySkinBtwLFT_ikHndl_pointConstraint1.crp"
		;
connectAttr "midLegFrontProxySkinBtwLFT_ikHndl.rpt" "midLegFrontProxySkinBtwLFT_ikHndl_pointConstraint1.crt"
		;
connectAttr "midLegFrontProxySkinBtwAimEndLFT_jnt.t" "midLegFrontProxySkinBtwLFT_ikHndl_pointConstraint1.tg[0].tt"
		;
connectAttr "midLegFrontProxySkinBtwAimEndLFT_jnt.rp" "midLegFrontProxySkinBtwLFT_ikHndl_pointConstraint1.tg[0].trp"
		;
connectAttr "midLegFrontProxySkinBtwAimEndLFT_jnt.rpt" "midLegFrontProxySkinBtwLFT_ikHndl_pointConstraint1.tg[0].trt"
		;
connectAttr "midLegFrontProxySkinBtwAimEndLFT_jnt.pm" "midLegFrontProxySkinBtwLFT_ikHndl_pointConstraint1.tg[0].tpm"
		;
connectAttr "midLegFrontProxySkinBtwLFT_ikHndl_pointConstraint1.w0" "midLegFrontProxySkinBtwLFT_ikHndl_pointConstraint1.tg[0].tw"
		;
connectAttr "midLegFrontProxySkinBtwVoidEndLFT_jnt.t" "midLegFrontProxySkinBtwLFT_ikHndl_pointConstraint1.tg[1].tt"
		;
connectAttr "midLegFrontProxySkinBtwVoidEndLFT_jnt.rp" "midLegFrontProxySkinBtwLFT_ikHndl_pointConstraint1.tg[1].trp"
		;
connectAttr "midLegFrontProxySkinBtwVoidEndLFT_jnt.rpt" "midLegFrontProxySkinBtwLFT_ikHndl_pointConstraint1.tg[1].trt"
		;
connectAttr "midLegFrontProxySkinBtwVoidEndLFT_jnt.pm" "midLegFrontProxySkinBtwLFT_ikHndl_pointConstraint1.tg[1].tpm"
		;
connectAttr "midLegFrontProxySkinBtwLFT_ikHndl_pointConstraint1.w1" "midLegFrontProxySkinBtwLFT_ikHndl_pointConstraint1.tg[1].tw"
		;
connectAttr "midLegFrontProxySkinBtwLFT_jnt.aimBtw" "midLegFrontProxySkinBtwLFT_ikHndl_pointConstraint1.w0"
		;
connectAttr "midLegFrontProxySkinDriveVoidLFT_adl.ox" "midLegFrontProxySkinBtwLFT_ikHndl_pointConstraint1.w1"
		;
connectAttr "midLegFrontProxySkinBtwLFT_jnt_parentConstraint1.ctx" "midLegFrontProxySkinBtwLFT_jnt.tx"
		 -l on;
connectAttr "midLegFrontProxySkinBtwLFT_jnt_parentConstraint1.cty" "midLegFrontProxySkinBtwLFT_jnt.ty"
		 -l on;
connectAttr "midLegFrontProxySkinBtwLFT_jnt_parentConstraint1.ctz" "midLegFrontProxySkinBtwLFT_jnt.tz"
		 -l on;
connectAttr "midLegFrontProxySkinBtwLFT_jnt_parentConstraint1.crx" "midLegFrontProxySkinBtwLFT_jnt.rx"
		 -l on;
connectAttr "midLegFrontProxySkinBtwLFT_jnt_parentConstraint1.cry" "midLegFrontProxySkinBtwLFT_jnt.ry"
		 -l on;
connectAttr "midLegFrontProxySkinBtwLFT_jnt_parentConstraint1.crz" "midLegFrontProxySkinBtwLFT_jnt.rz"
		 -l on;
connectAttr "midLegFrontProxySkinDoubleScaleALFT_adl.o" "midLegFrontProxySkinBtwLFT_jnt.sx"
		 -l on;
connectAttr "midLegFrontProxySkinDoubleScaleBLFT_adl.o" "midLegFrontProxySkinBtwLFT_jnt.sz"
		 -l on;
connectAttr "midLegFrontProxySkinBtwLFT_jnt.s" "midLegFrontProxySkinBtwEndLFT_jnt.is"
		;
connectAttr "midLegFrontProxySkinBtwLFT_jnt.ro" "midLegFrontProxySkinBtwLFT_jnt_parentConstraint1.cro"
		;
connectAttr "midLegFrontProxySkinBtwLFT_jnt.pim" "midLegFrontProxySkinBtwLFT_jnt_parentConstraint1.cpim"
		;
connectAttr "midLegFrontProxySkinBtwLFT_jnt.rp" "midLegFrontProxySkinBtwLFT_jnt_parentConstraint1.crp"
		;
connectAttr "midLegFrontProxySkinBtwLFT_jnt.rpt" "midLegFrontProxySkinBtwLFT_jnt_parentConstraint1.crt"
		;
connectAttr "midLegFrontProxySkinBtwLFT_jnt.jo" "midLegFrontProxySkinBtwLFT_jnt_parentConstraint1.cjo"
		;
connectAttr "midLegFrontProxySkinBtwIkLFT_jnt.t" "midLegFrontProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tt"
		;
connectAttr "midLegFrontProxySkinBtwIkLFT_jnt.rp" "midLegFrontProxySkinBtwLFT_jnt_parentConstraint1.tg[0].trp"
		;
connectAttr "midLegFrontProxySkinBtwIkLFT_jnt.rpt" "midLegFrontProxySkinBtwLFT_jnt_parentConstraint1.tg[0].trt"
		;
connectAttr "midLegFrontProxySkinBtwIkLFT_jnt.r" "midLegFrontProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tr"
		;
connectAttr "midLegFrontProxySkinBtwIkLFT_jnt.ro" "midLegFrontProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tro"
		;
connectAttr "midLegFrontProxySkinBtwIkLFT_jnt.s" "midLegFrontProxySkinBtwLFT_jnt_parentConstraint1.tg[0].ts"
		;
connectAttr "midLegFrontProxySkinBtwIkLFT_jnt.pm" "midLegFrontProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tpm"
		;
connectAttr "midLegFrontProxySkinBtwIkLFT_jnt.jo" "midLegFrontProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tjo"
		;
connectAttr "midLegFrontProxySkinBtwIkLFT_jnt.ssc" "midLegFrontProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tsc"
		;
connectAttr "midLegFrontProxySkinBtwIkLFT_jnt.is" "midLegFrontProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tis"
		;
connectAttr "midLegFrontProxySkinBtwLFT_jnt_parentConstraint1.w0" "midLegFrontProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tw"
		;
connectAttr "clav1ScapProxySkinLFT_jnt.s" "clav1ScaScapProxySkinLFT_jnt.is";
connectAttr "spine2ProxySkin_jnt.s" "clav1ScapProxySkinRGT_jnt.is";
connectAttr "clav1ScapProxySkinRGT_jnt.s" "clav2ScapProxySkinRGT_jnt.is";
connectAttr "clav2ScapProxySkinRGT_jnt.s" "upLegFrontProxySkinRGT_jnt.is";
connectAttr "upLegFrontProxySkinRGT_jnt.s" "midLegFrontProxySkinRGT_jnt.is";
connectAttr "midLegFrontProxySkinRGT_jnt.s" "lowLegFrontProxySkinRGT_jnt.is";
connectAttr "lowLegFrontProxySkinRGT_jnt.s" "ankleFrontProxySkinRGT_jnt.is";
connectAttr "ankleFrontProxySkinRGT_jnt.s" "ballFrontProxySkinRGT_jnt.is";
connectAttr "ballFrontProxySkinRGT_jnt.s" "toeFrontProxySkinRGT_jnt.is";
connectAttr "ballFrontProxySkinBtwAllRGT_grp_pointConstraint1.ctx" "ballFrontProxySkinBtwAllRGT_grp.tx"
		;
connectAttr "ballFrontProxySkinBtwAllRGT_grp_pointConstraint1.cty" "ballFrontProxySkinBtwAllRGT_grp.ty"
		;
connectAttr "ballFrontProxySkinBtwAllRGT_grp_pointConstraint1.ctz" "ballFrontProxySkinBtwAllRGT_grp.tz"
		;
connectAttr "ballFrontProxySkinBtwIkRGT_jnt.s" "ballFrontProxySkinBtwEndIkRGT_jnt.is"
		;
connectAttr "ballFrontProxySkinBtwEndIkRGT_jnt.tx" "ballFrontProxySkinBtwEndRGT_eff.tx"
		;
connectAttr "ballFrontProxySkinBtwEndIkRGT_jnt.ty" "ballFrontProxySkinBtwEndRGT_eff.ty"
		;
connectAttr "ballFrontProxySkinBtwEndIkRGT_jnt.tz" "ballFrontProxySkinBtwEndRGT_eff.tz"
		;
connectAttr "ballFrontProxySkinBtwAimRGT_jnt_aimConstraint1.crx" "ballFrontProxySkinBtwAimRGT_jnt.rx"
		;
connectAttr "ballFrontProxySkinBtwAimRGT_jnt_aimConstraint1.cry" "ballFrontProxySkinBtwAimRGT_jnt.ry"
		;
connectAttr "ballFrontProxySkinBtwAimRGT_jnt_aimConstraint1.crz" "ballFrontProxySkinBtwAimRGT_jnt.rz"
		;
connectAttr "ballFrontProxySkinBtwAimRGT_jnt.s" "ballFrontProxySkinBtwAimEndRGT_jnt.is"
		;
connectAttr "ballFrontProxySkinBtwAimRGT_jnt.pim" "ballFrontProxySkinBtwAimRGT_jnt_aimConstraint1.cpim"
		;
connectAttr "ballFrontProxySkinBtwAimRGT_jnt.t" "ballFrontProxySkinBtwAimRGT_jnt_aimConstraint1.ct"
		;
connectAttr "ballFrontProxySkinBtwAimRGT_jnt.rp" "ballFrontProxySkinBtwAimRGT_jnt_aimConstraint1.crp"
		;
connectAttr "ballFrontProxySkinBtwAimRGT_jnt.rpt" "ballFrontProxySkinBtwAimRGT_jnt_aimConstraint1.crt"
		;
connectAttr "ballFrontProxySkinBtwAimRGT_jnt.ro" "ballFrontProxySkinBtwAimRGT_jnt_aimConstraint1.cro"
		;
connectAttr "ballFrontProxySkinBtwAimRGT_jnt.jo" "ballFrontProxySkinBtwAimRGT_jnt_aimConstraint1.cjo"
		;
connectAttr "ballFrontProxySkinBtwAimRGT_jnt.is" "ballFrontProxySkinBtwAimRGT_jnt_aimConstraint1.is"
		;
connectAttr "toeFrontProxySkinRGT_jnt.t" "ballFrontProxySkinBtwAimRGT_jnt_aimConstraint1.tg[0].tt"
		;
connectAttr "toeFrontProxySkinRGT_jnt.rp" "ballFrontProxySkinBtwAimRGT_jnt_aimConstraint1.tg[0].trp"
		;
connectAttr "toeFrontProxySkinRGT_jnt.rpt" "ballFrontProxySkinBtwAimRGT_jnt_aimConstraint1.tg[0].trt"
		;
connectAttr "toeFrontProxySkinRGT_jnt.pm" "ballFrontProxySkinBtwAimRGT_jnt_aimConstraint1.tg[0].tpm"
		;
connectAttr "ballFrontProxySkinBtwAimRGT_jnt_aimConstraint1.w0" "ballFrontProxySkinBtwAimRGT_jnt_aimConstraint1.tg[0].tw"
		;
connectAttr "ballFrontProxySkinBtwUpRGT_grp.wm" "ballFrontProxySkinBtwAimRGT_jnt_aimConstraint1.wum"
		;
connectAttr "ballFrontProxySkinBtwVoidRGT_jnt.s" "ballFrontProxySkinBtwVoidEndRGT_jnt.is"
		;
connectAttr "ballFrontProxySkinBtwIkRGT_jnt.msg" "ballFrontProxySkinBtwRGT_ikHndl.hsj"
		;
connectAttr "ballFrontProxySkinBtwEndRGT_eff.hp" "ballFrontProxySkinBtwRGT_ikHndl.hee"
		;
connectAttr "ikRPsolver.msg" "ballFrontProxySkinBtwRGT_ikHndl.hsv";
connectAttr "ballFrontProxySkinBtwRGT_ikHndl_pointConstraint1.ctx" "ballFrontProxySkinBtwRGT_ikHndl.tx"
		;
connectAttr "ballFrontProxySkinBtwRGT_ikHndl_pointConstraint1.cty" "ballFrontProxySkinBtwRGT_ikHndl.ty"
		;
connectAttr "ballFrontProxySkinBtwRGT_ikHndl_pointConstraint1.ctz" "ballFrontProxySkinBtwRGT_ikHndl.tz"
		;
connectAttr "ballFrontProxySkinBtwRGT_ikHndl.pim" "ballFrontProxySkinBtwRGT_ikHndl_pointConstraint1.cpim"
		;
connectAttr "ballFrontProxySkinBtwRGT_ikHndl.rp" "ballFrontProxySkinBtwRGT_ikHndl_pointConstraint1.crp"
		;
connectAttr "ballFrontProxySkinBtwRGT_ikHndl.rpt" "ballFrontProxySkinBtwRGT_ikHndl_pointConstraint1.crt"
		;
connectAttr "ballFrontProxySkinBtwAimEndRGT_jnt.t" "ballFrontProxySkinBtwRGT_ikHndl_pointConstraint1.tg[0].tt"
		;
connectAttr "ballFrontProxySkinBtwAimEndRGT_jnt.rp" "ballFrontProxySkinBtwRGT_ikHndl_pointConstraint1.tg[0].trp"
		;
connectAttr "ballFrontProxySkinBtwAimEndRGT_jnt.rpt" "ballFrontProxySkinBtwRGT_ikHndl_pointConstraint1.tg[0].trt"
		;
connectAttr "ballFrontProxySkinBtwAimEndRGT_jnt.pm" "ballFrontProxySkinBtwRGT_ikHndl_pointConstraint1.tg[0].tpm"
		;
connectAttr "ballFrontProxySkinBtwRGT_ikHndl_pointConstraint1.w0" "ballFrontProxySkinBtwRGT_ikHndl_pointConstraint1.tg[0].tw"
		;
connectAttr "ballFrontProxySkinBtwVoidEndRGT_jnt.t" "ballFrontProxySkinBtwRGT_ikHndl_pointConstraint1.tg[1].tt"
		;
connectAttr "ballFrontProxySkinBtwVoidEndRGT_jnt.rp" "ballFrontProxySkinBtwRGT_ikHndl_pointConstraint1.tg[1].trp"
		;
connectAttr "ballFrontProxySkinBtwVoidEndRGT_jnt.rpt" "ballFrontProxySkinBtwRGT_ikHndl_pointConstraint1.tg[1].trt"
		;
connectAttr "ballFrontProxySkinBtwVoidEndRGT_jnt.pm" "ballFrontProxySkinBtwRGT_ikHndl_pointConstraint1.tg[1].tpm"
		;
connectAttr "ballFrontProxySkinBtwRGT_ikHndl_pointConstraint1.w1" "ballFrontProxySkinBtwRGT_ikHndl_pointConstraint1.tg[1].tw"
		;
connectAttr "ballFrontProxySkinBtwRGT_jnt.aimBtw" "ballFrontProxySkinBtwRGT_ikHndl_pointConstraint1.w0"
		;
connectAttr "ballFrontProxySkinDriveVoidRGT_adl.ox" "ballFrontProxySkinBtwRGT_ikHndl_pointConstraint1.w1"
		;
connectAttr "ballFrontProxySkinBtwRGT_jnt_parentConstraint1.ctx" "ballFrontProxySkinBtwRGT_jnt.tx"
		 -l on;
connectAttr "ballFrontProxySkinBtwRGT_jnt_parentConstraint1.cty" "ballFrontProxySkinBtwRGT_jnt.ty"
		 -l on;
connectAttr "ballFrontProxySkinBtwRGT_jnt_parentConstraint1.ctz" "ballFrontProxySkinBtwRGT_jnt.tz"
		 -l on;
connectAttr "ballFrontProxySkinBtwRGT_jnt_parentConstraint1.crx" "ballFrontProxySkinBtwRGT_jnt.rx"
		 -l on;
connectAttr "ballFrontProxySkinBtwRGT_jnt_parentConstraint1.cry" "ballFrontProxySkinBtwRGT_jnt.ry"
		 -l on;
connectAttr "ballFrontProxySkinBtwRGT_jnt_parentConstraint1.crz" "ballFrontProxySkinBtwRGT_jnt.rz"
		 -l on;
connectAttr "ballFrontProxySkinDoubleScaleARGT_adl.o" "ballFrontProxySkinBtwRGT_jnt.sx"
		 -l on;
connectAttr "ballFrontProxySkinDoubleScaleBRGT_adl.o" "ballFrontProxySkinBtwRGT_jnt.sz"
		 -l on;
connectAttr "ballFrontProxySkinBtwRGT_jnt.s" "ballFrontProxySkinBtwEndRGT_jnt.is"
		;
connectAttr "ballFrontProxySkinBtwRGT_jnt.ro" "ballFrontProxySkinBtwRGT_jnt_parentConstraint1.cro"
		;
connectAttr "ballFrontProxySkinBtwRGT_jnt.pim" "ballFrontProxySkinBtwRGT_jnt_parentConstraint1.cpim"
		;
connectAttr "ballFrontProxySkinBtwRGT_jnt.rp" "ballFrontProxySkinBtwRGT_jnt_parentConstraint1.crp"
		;
connectAttr "ballFrontProxySkinBtwRGT_jnt.rpt" "ballFrontProxySkinBtwRGT_jnt_parentConstraint1.crt"
		;
connectAttr "ballFrontProxySkinBtwRGT_jnt.jo" "ballFrontProxySkinBtwRGT_jnt_parentConstraint1.cjo"
		;
connectAttr "ballFrontProxySkinBtwIkRGT_jnt.t" "ballFrontProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tt"
		;
connectAttr "ballFrontProxySkinBtwIkRGT_jnt.rp" "ballFrontProxySkinBtwRGT_jnt_parentConstraint1.tg[0].trp"
		;
connectAttr "ballFrontProxySkinBtwIkRGT_jnt.rpt" "ballFrontProxySkinBtwRGT_jnt_parentConstraint1.tg[0].trt"
		;
connectAttr "ballFrontProxySkinBtwIkRGT_jnt.r" "ballFrontProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tr"
		;
connectAttr "ballFrontProxySkinBtwIkRGT_jnt.ro" "ballFrontProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tro"
		;
connectAttr "ballFrontProxySkinBtwIkRGT_jnt.s" "ballFrontProxySkinBtwRGT_jnt_parentConstraint1.tg[0].ts"
		;
connectAttr "ballFrontProxySkinBtwIkRGT_jnt.pm" "ballFrontProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tpm"
		;
connectAttr "ballFrontProxySkinBtwIkRGT_jnt.jo" "ballFrontProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tjo"
		;
connectAttr "ballFrontProxySkinBtwIkRGT_jnt.ssc" "ballFrontProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tsc"
		;
connectAttr "ballFrontProxySkinBtwIkRGT_jnt.is" "ballFrontProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tis"
		;
connectAttr "ballFrontProxySkinBtwRGT_jnt_parentConstraint1.w0" "ballFrontProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tw"
		;
connectAttr "ballFrontProxySkinBtwAllRGT_grp.pim" "ballFrontProxySkinBtwAllRGT_grp_pointConstraint1.cpim"
		;
connectAttr "ballFrontProxySkinBtwAllRGT_grp.rp" "ballFrontProxySkinBtwAllRGT_grp_pointConstraint1.crp"
		;
connectAttr "ballFrontProxySkinBtwAllRGT_grp.rpt" "ballFrontProxySkinBtwAllRGT_grp_pointConstraint1.crt"
		;
connectAttr "ballFrontProxySkinRGT_jnt.t" "ballFrontProxySkinBtwAllRGT_grp_pointConstraint1.tg[0].tt"
		;
connectAttr "ballFrontProxySkinRGT_jnt.rp" "ballFrontProxySkinBtwAllRGT_grp_pointConstraint1.tg[0].trp"
		;
connectAttr "ballFrontProxySkinRGT_jnt.rpt" "ballFrontProxySkinBtwAllRGT_grp_pointConstraint1.tg[0].trt"
		;
connectAttr "ballFrontProxySkinRGT_jnt.pm" "ballFrontProxySkinBtwAllRGT_grp_pointConstraint1.tg[0].tpm"
		;
connectAttr "ballFrontProxySkinBtwAllRGT_grp_pointConstraint1.w0" "ballFrontProxySkinBtwAllRGT_grp_pointConstraint1.tg[0].tw"
		;
connectAttr "lowLegFrontProxySkinRGT_jnt.s" "lowLegRbnDtl1FrontProxySkinRGT_jnt.is"
		;
connectAttr "lowLegFrontProxySkinRGT_jnt.s" "lowLegRbnDtl2FrontProxySkinRGT_jnt.is"
		;
connectAttr "lowLegFrontProxySkinRGT_jnt.s" "lowLegRbnDtl3FrontProxySkinRGT_jnt.is"
		;
connectAttr "lowLegFrontProxySkinRGT_jnt.s" "lowLegRbnDtl4FrontProxySkinRGT_jnt.is"
		;
connectAttr "lowLegFrontProxySkinRGT_jnt.s" "lowLegRbnDtl5FrontProxySkinRGT_jnt.is"
		;
connectAttr "ankleFrontProxySkinBtwAllRGT_grp_pointConstraint1.ctx" "ankleFrontProxySkinBtwAllRGT_grp.tx"
		;
connectAttr "ankleFrontProxySkinBtwAllRGT_grp_pointConstraint1.cty" "ankleFrontProxySkinBtwAllRGT_grp.ty"
		;
connectAttr "ankleFrontProxySkinBtwAllRGT_grp_pointConstraint1.ctz" "ankleFrontProxySkinBtwAllRGT_grp.tz"
		;
connectAttr "ankleFrontProxySkinBtwIkRGT_jnt.s" "ankleFrontProxySkinBtwEndIkRGT_jnt.is"
		;
connectAttr "ankleFrontProxySkinBtwEndIkRGT_jnt.tx" "ankleFrontProxySkinBtwEndRGT_eff.tx"
		;
connectAttr "ankleFrontProxySkinBtwEndIkRGT_jnt.ty" "ankleFrontProxySkinBtwEndRGT_eff.ty"
		;
connectAttr "ankleFrontProxySkinBtwEndIkRGT_jnt.tz" "ankleFrontProxySkinBtwEndRGT_eff.tz"
		;
connectAttr "ankleFrontProxySkinBtwAimRGT_jnt_aimConstraint1.crx" "ankleFrontProxySkinBtwAimRGT_jnt.rx"
		;
connectAttr "ankleFrontProxySkinBtwAimRGT_jnt_aimConstraint1.cry" "ankleFrontProxySkinBtwAimRGT_jnt.ry"
		;
connectAttr "ankleFrontProxySkinBtwAimRGT_jnt_aimConstraint1.crz" "ankleFrontProxySkinBtwAimRGT_jnt.rz"
		;
connectAttr "ankleFrontProxySkinBtwAimRGT_jnt.s" "ankleFrontProxySkinBtwAimEndRGT_jnt.is"
		;
connectAttr "ankleFrontProxySkinBtwAimRGT_jnt.pim" "ankleFrontProxySkinBtwAimRGT_jnt_aimConstraint1.cpim"
		;
connectAttr "ankleFrontProxySkinBtwAimRGT_jnt.t" "ankleFrontProxySkinBtwAimRGT_jnt_aimConstraint1.ct"
		;
connectAttr "ankleFrontProxySkinBtwAimRGT_jnt.rp" "ankleFrontProxySkinBtwAimRGT_jnt_aimConstraint1.crp"
		;
connectAttr "ankleFrontProxySkinBtwAimRGT_jnt.rpt" "ankleFrontProxySkinBtwAimRGT_jnt_aimConstraint1.crt"
		;
connectAttr "ankleFrontProxySkinBtwAimRGT_jnt.ro" "ankleFrontProxySkinBtwAimRGT_jnt_aimConstraint1.cro"
		;
connectAttr "ankleFrontProxySkinBtwAimRGT_jnt.jo" "ankleFrontProxySkinBtwAimRGT_jnt_aimConstraint1.cjo"
		;
connectAttr "ankleFrontProxySkinBtwAimRGT_jnt.is" "ankleFrontProxySkinBtwAimRGT_jnt_aimConstraint1.is"
		;
connectAttr "ballFrontProxySkinRGT_jnt.t" "ankleFrontProxySkinBtwAimRGT_jnt_aimConstraint1.tg[0].tt"
		;
connectAttr "ballFrontProxySkinRGT_jnt.rp" "ankleFrontProxySkinBtwAimRGT_jnt_aimConstraint1.tg[0].trp"
		;
connectAttr "ballFrontProxySkinRGT_jnt.rpt" "ankleFrontProxySkinBtwAimRGT_jnt_aimConstraint1.tg[0].trt"
		;
connectAttr "ballFrontProxySkinRGT_jnt.pm" "ankleFrontProxySkinBtwAimRGT_jnt_aimConstraint1.tg[0].tpm"
		;
connectAttr "ankleFrontProxySkinBtwAimRGT_jnt_aimConstraint1.w0" "ankleFrontProxySkinBtwAimRGT_jnt_aimConstraint1.tg[0].tw"
		;
connectAttr "ankleFrontProxySkinBtwUpRGT_grp.wm" "ankleFrontProxySkinBtwAimRGT_jnt_aimConstraint1.wum"
		;
connectAttr "ankleFrontProxySkinBtwVoidRGT_jnt.s" "ankleFrontProxySkinBtwVoidEndRGT_jnt.is"
		;
connectAttr "ankleFrontProxySkinBtwIkRGT_jnt.msg" "ankleFrontProxySkinBtwRGT_ikHndl.hsj"
		;
connectAttr "ankleFrontProxySkinBtwEndRGT_eff.hp" "ankleFrontProxySkinBtwRGT_ikHndl.hee"
		;
connectAttr "ikRPsolver.msg" "ankleFrontProxySkinBtwRGT_ikHndl.hsv";
connectAttr "ankleFrontProxySkinBtwRGT_ikHndl_pointConstraint1.ctx" "ankleFrontProxySkinBtwRGT_ikHndl.tx"
		;
connectAttr "ankleFrontProxySkinBtwRGT_ikHndl_pointConstraint1.cty" "ankleFrontProxySkinBtwRGT_ikHndl.ty"
		;
connectAttr "ankleFrontProxySkinBtwRGT_ikHndl_pointConstraint1.ctz" "ankleFrontProxySkinBtwRGT_ikHndl.tz"
		;
connectAttr "ankleFrontProxySkinBtwRGT_ikHndl.pim" "ankleFrontProxySkinBtwRGT_ikHndl_pointConstraint1.cpim"
		;
connectAttr "ankleFrontProxySkinBtwRGT_ikHndl.rp" "ankleFrontProxySkinBtwRGT_ikHndl_pointConstraint1.crp"
		;
connectAttr "ankleFrontProxySkinBtwRGT_ikHndl.rpt" "ankleFrontProxySkinBtwRGT_ikHndl_pointConstraint1.crt"
		;
connectAttr "ankleFrontProxySkinBtwAimEndRGT_jnt.t" "ankleFrontProxySkinBtwRGT_ikHndl_pointConstraint1.tg[0].tt"
		;
connectAttr "ankleFrontProxySkinBtwAimEndRGT_jnt.rp" "ankleFrontProxySkinBtwRGT_ikHndl_pointConstraint1.tg[0].trp"
		;
connectAttr "ankleFrontProxySkinBtwAimEndRGT_jnt.rpt" "ankleFrontProxySkinBtwRGT_ikHndl_pointConstraint1.tg[0].trt"
		;
connectAttr "ankleFrontProxySkinBtwAimEndRGT_jnt.pm" "ankleFrontProxySkinBtwRGT_ikHndl_pointConstraint1.tg[0].tpm"
		;
connectAttr "ankleFrontProxySkinBtwRGT_ikHndl_pointConstraint1.w0" "ankleFrontProxySkinBtwRGT_ikHndl_pointConstraint1.tg[0].tw"
		;
connectAttr "ankleFrontProxySkinBtwVoidEndRGT_jnt.t" "ankleFrontProxySkinBtwRGT_ikHndl_pointConstraint1.tg[1].tt"
		;
connectAttr "ankleFrontProxySkinBtwVoidEndRGT_jnt.rp" "ankleFrontProxySkinBtwRGT_ikHndl_pointConstraint1.tg[1].trp"
		;
connectAttr "ankleFrontProxySkinBtwVoidEndRGT_jnt.rpt" "ankleFrontProxySkinBtwRGT_ikHndl_pointConstraint1.tg[1].trt"
		;
connectAttr "ankleFrontProxySkinBtwVoidEndRGT_jnt.pm" "ankleFrontProxySkinBtwRGT_ikHndl_pointConstraint1.tg[1].tpm"
		;
connectAttr "ankleFrontProxySkinBtwRGT_ikHndl_pointConstraint1.w1" "ankleFrontProxySkinBtwRGT_ikHndl_pointConstraint1.tg[1].tw"
		;
connectAttr "ankleFrontProxySkinBtwRGT_jnt.aimBtw" "ankleFrontProxySkinBtwRGT_ikHndl_pointConstraint1.w0"
		;
connectAttr "ankleFrontProxySkinDriveVoidRGT_adl.ox" "ankleFrontProxySkinBtwRGT_ikHndl_pointConstraint1.w1"
		;
connectAttr "ankleFrontProxySkinBtwRGT_jnt_parentConstraint1.ctx" "ankleFrontProxySkinBtwRGT_jnt.tx"
		 -l on;
connectAttr "ankleFrontProxySkinBtwRGT_jnt_parentConstraint1.cty" "ankleFrontProxySkinBtwRGT_jnt.ty"
		 -l on;
connectAttr "ankleFrontProxySkinBtwRGT_jnt_parentConstraint1.ctz" "ankleFrontProxySkinBtwRGT_jnt.tz"
		 -l on;
connectAttr "ankleFrontProxySkinBtwRGT_jnt_parentConstraint1.crx" "ankleFrontProxySkinBtwRGT_jnt.rx"
		 -l on;
connectAttr "ankleFrontProxySkinBtwRGT_jnt_parentConstraint1.cry" "ankleFrontProxySkinBtwRGT_jnt.ry"
		 -l on;
connectAttr "ankleFrontProxySkinBtwRGT_jnt_parentConstraint1.crz" "ankleFrontProxySkinBtwRGT_jnt.rz"
		 -l on;
connectAttr "ankleFrontProxySkinDoubleScaleARGT_adl.o" "ankleFrontProxySkinBtwRGT_jnt.sx"
		 -l on;
connectAttr "ankleFrontProxySkinDoubleScaleBRGT_adl.o" "ankleFrontProxySkinBtwRGT_jnt.sz"
		 -l on;
connectAttr "ankleFrontProxySkinBtwRGT_jnt.s" "ankleFrontProxySkinBtwEndRGT_jnt.is"
		;
connectAttr "ankleFrontProxySkinBtwRGT_jnt.ro" "ankleFrontProxySkinBtwRGT_jnt_parentConstraint1.cro"
		;
connectAttr "ankleFrontProxySkinBtwRGT_jnt.pim" "ankleFrontProxySkinBtwRGT_jnt_parentConstraint1.cpim"
		;
connectAttr "ankleFrontProxySkinBtwRGT_jnt.rp" "ankleFrontProxySkinBtwRGT_jnt_parentConstraint1.crp"
		;
connectAttr "ankleFrontProxySkinBtwRGT_jnt.rpt" "ankleFrontProxySkinBtwRGT_jnt_parentConstraint1.crt"
		;
connectAttr "ankleFrontProxySkinBtwRGT_jnt.jo" "ankleFrontProxySkinBtwRGT_jnt_parentConstraint1.cjo"
		;
connectAttr "ankleFrontProxySkinBtwIkRGT_jnt.t" "ankleFrontProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tt"
		;
connectAttr "ankleFrontProxySkinBtwIkRGT_jnt.rp" "ankleFrontProxySkinBtwRGT_jnt_parentConstraint1.tg[0].trp"
		;
connectAttr "ankleFrontProxySkinBtwIkRGT_jnt.rpt" "ankleFrontProxySkinBtwRGT_jnt_parentConstraint1.tg[0].trt"
		;
connectAttr "ankleFrontProxySkinBtwIkRGT_jnt.r" "ankleFrontProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tr"
		;
connectAttr "ankleFrontProxySkinBtwIkRGT_jnt.ro" "ankleFrontProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tro"
		;
connectAttr "ankleFrontProxySkinBtwIkRGT_jnt.s" "ankleFrontProxySkinBtwRGT_jnt_parentConstraint1.tg[0].ts"
		;
connectAttr "ankleFrontProxySkinBtwIkRGT_jnt.pm" "ankleFrontProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tpm"
		;
connectAttr "ankleFrontProxySkinBtwIkRGT_jnt.jo" "ankleFrontProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tjo"
		;
connectAttr "ankleFrontProxySkinBtwIkRGT_jnt.ssc" "ankleFrontProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tsc"
		;
connectAttr "ankleFrontProxySkinBtwIkRGT_jnt.is" "ankleFrontProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tis"
		;
connectAttr "ankleFrontProxySkinBtwRGT_jnt_parentConstraint1.w0" "ankleFrontProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tw"
		;
connectAttr "ankleFrontProxySkinBtwAllRGT_grp.pim" "ankleFrontProxySkinBtwAllRGT_grp_pointConstraint1.cpim"
		;
connectAttr "ankleFrontProxySkinBtwAllRGT_grp.rp" "ankleFrontProxySkinBtwAllRGT_grp_pointConstraint1.crp"
		;
connectAttr "ankleFrontProxySkinBtwAllRGT_grp.rpt" "ankleFrontProxySkinBtwAllRGT_grp_pointConstraint1.crt"
		;
connectAttr "ankleFrontProxySkinRGT_jnt.t" "ankleFrontProxySkinBtwAllRGT_grp_pointConstraint1.tg[0].tt"
		;
connectAttr "ankleFrontProxySkinRGT_jnt.rp" "ankleFrontProxySkinBtwAllRGT_grp_pointConstraint1.tg[0].trp"
		;
connectAttr "ankleFrontProxySkinRGT_jnt.rpt" "ankleFrontProxySkinBtwAllRGT_grp_pointConstraint1.tg[0].trt"
		;
connectAttr "ankleFrontProxySkinRGT_jnt.pm" "ankleFrontProxySkinBtwAllRGT_grp_pointConstraint1.tg[0].tpm"
		;
connectAttr "ankleFrontProxySkinBtwAllRGT_grp_pointConstraint1.w0" "ankleFrontProxySkinBtwAllRGT_grp_pointConstraint1.tg[0].tw"
		;
connectAttr "midLegFrontProxySkinRGT_jnt.s" "midLegRbnDtl1FrontProxySkinRGT_jnt.is"
		;
connectAttr "midLegFrontProxySkinRGT_jnt.s" "midLegRbnDtl2FrontProxySkinRGT_jnt.is"
		;
connectAttr "midLegFrontProxySkinRGT_jnt.s" "midLegRbnDtl3FrontProxySkinRGT_jnt.is"
		;
connectAttr "midLegFrontProxySkinRGT_jnt.s" "midLegRbnDtl4FrontProxySkinRGT_jnt.is"
		;
connectAttr "midLegFrontProxySkinRGT_jnt.s" "midLegRbnDtl5FrontProxySkinRGT_jnt.is"
		;
connectAttr "lowLegFrontProxySkinBtwAllRGT_grp_pointConstraint1.ctx" "lowLegFrontProxySkinBtwAllRGT_grp.tx"
		;
connectAttr "lowLegFrontProxySkinBtwAllRGT_grp_pointConstraint1.cty" "lowLegFrontProxySkinBtwAllRGT_grp.ty"
		;
connectAttr "lowLegFrontProxySkinBtwAllRGT_grp_pointConstraint1.ctz" "lowLegFrontProxySkinBtwAllRGT_grp.tz"
		;
connectAttr "lowLegFrontProxySkinBtwIkRGT_jnt.s" "lowLegFrontProxySkinBtwEndIkRGT_jnt.is"
		;
connectAttr "lowLegFrontProxySkinBtwEndIkRGT_jnt.tx" "lowLegFrontProxySkinBtwEndRGT_eff.tx"
		;
connectAttr "lowLegFrontProxySkinBtwEndIkRGT_jnt.ty" "lowLegFrontProxySkinBtwEndRGT_eff.ty"
		;
connectAttr "lowLegFrontProxySkinBtwEndIkRGT_jnt.tz" "lowLegFrontProxySkinBtwEndRGT_eff.tz"
		;
connectAttr "lowLegFrontProxySkinBtwAimRGT_jnt_aimConstraint1.crx" "lowLegFrontProxySkinBtwAimRGT_jnt.rx"
		;
connectAttr "lowLegFrontProxySkinBtwAimRGT_jnt_aimConstraint1.cry" "lowLegFrontProxySkinBtwAimRGT_jnt.ry"
		;
connectAttr "lowLegFrontProxySkinBtwAimRGT_jnt_aimConstraint1.crz" "lowLegFrontProxySkinBtwAimRGT_jnt.rz"
		;
connectAttr "lowLegFrontProxySkinBtwAimRGT_jnt.s" "lowLegFrontProxySkinBtwAimEndRGT_jnt.is"
		;
connectAttr "lowLegFrontProxySkinBtwAimRGT_jnt.pim" "lowLegFrontProxySkinBtwAimRGT_jnt_aimConstraint1.cpim"
		;
connectAttr "lowLegFrontProxySkinBtwAimRGT_jnt.t" "lowLegFrontProxySkinBtwAimRGT_jnt_aimConstraint1.ct"
		;
connectAttr "lowLegFrontProxySkinBtwAimRGT_jnt.rp" "lowLegFrontProxySkinBtwAimRGT_jnt_aimConstraint1.crp"
		;
connectAttr "lowLegFrontProxySkinBtwAimRGT_jnt.rpt" "lowLegFrontProxySkinBtwAimRGT_jnt_aimConstraint1.crt"
		;
connectAttr "lowLegFrontProxySkinBtwAimRGT_jnt.ro" "lowLegFrontProxySkinBtwAimRGT_jnt_aimConstraint1.cro"
		;
connectAttr "lowLegFrontProxySkinBtwAimRGT_jnt.jo" "lowLegFrontProxySkinBtwAimRGT_jnt_aimConstraint1.cjo"
		;
connectAttr "lowLegFrontProxySkinBtwAimRGT_jnt.is" "lowLegFrontProxySkinBtwAimRGT_jnt_aimConstraint1.is"
		;
connectAttr "lowLegRbnDtl1FrontProxySkinRGT_jnt.t" "lowLegFrontProxySkinBtwAimRGT_jnt_aimConstraint1.tg[0].tt"
		;
connectAttr "lowLegRbnDtl1FrontProxySkinRGT_jnt.rp" "lowLegFrontProxySkinBtwAimRGT_jnt_aimConstraint1.tg[0].trp"
		;
connectAttr "lowLegRbnDtl1FrontProxySkinRGT_jnt.rpt" "lowLegFrontProxySkinBtwAimRGT_jnt_aimConstraint1.tg[0].trt"
		;
connectAttr "lowLegRbnDtl1FrontProxySkinRGT_jnt.pm" "lowLegFrontProxySkinBtwAimRGT_jnt_aimConstraint1.tg[0].tpm"
		;
connectAttr "lowLegFrontProxySkinBtwAimRGT_jnt_aimConstraint1.w0" "lowLegFrontProxySkinBtwAimRGT_jnt_aimConstraint1.tg[0].tw"
		;
connectAttr "lowLegFrontProxySkinBtwUpRGT_grp.wm" "lowLegFrontProxySkinBtwAimRGT_jnt_aimConstraint1.wum"
		;
connectAttr "lowLegFrontProxySkinBtwVoidRGT_jnt.s" "lowLegFrontProxySkinBtwVoidEndRGT_jnt.is"
		;
connectAttr "lowLegFrontProxySkinBtwIkRGT_jnt.msg" "lowLegFrontProxySkinBtwRGT_ikHndl.hsj"
		;
connectAttr "lowLegFrontProxySkinBtwEndRGT_eff.hp" "lowLegFrontProxySkinBtwRGT_ikHndl.hee"
		;
connectAttr "ikRPsolver.msg" "lowLegFrontProxySkinBtwRGT_ikHndl.hsv";
connectAttr "lowLegFrontProxySkinBtwRGT_ikHndl_pointConstraint1.ctx" "lowLegFrontProxySkinBtwRGT_ikHndl.tx"
		;
connectAttr "lowLegFrontProxySkinBtwRGT_ikHndl_pointConstraint1.cty" "lowLegFrontProxySkinBtwRGT_ikHndl.ty"
		;
connectAttr "lowLegFrontProxySkinBtwRGT_ikHndl_pointConstraint1.ctz" "lowLegFrontProxySkinBtwRGT_ikHndl.tz"
		;
connectAttr "lowLegFrontProxySkinBtwRGT_ikHndl.pim" "lowLegFrontProxySkinBtwRGT_ikHndl_pointConstraint1.cpim"
		;
connectAttr "lowLegFrontProxySkinBtwRGT_ikHndl.rp" "lowLegFrontProxySkinBtwRGT_ikHndl_pointConstraint1.crp"
		;
connectAttr "lowLegFrontProxySkinBtwRGT_ikHndl.rpt" "lowLegFrontProxySkinBtwRGT_ikHndl_pointConstraint1.crt"
		;
connectAttr "lowLegFrontProxySkinBtwAimEndRGT_jnt.t" "lowLegFrontProxySkinBtwRGT_ikHndl_pointConstraint1.tg[0].tt"
		;
connectAttr "lowLegFrontProxySkinBtwAimEndRGT_jnt.rp" "lowLegFrontProxySkinBtwRGT_ikHndl_pointConstraint1.tg[0].trp"
		;
connectAttr "lowLegFrontProxySkinBtwAimEndRGT_jnt.rpt" "lowLegFrontProxySkinBtwRGT_ikHndl_pointConstraint1.tg[0].trt"
		;
connectAttr "lowLegFrontProxySkinBtwAimEndRGT_jnt.pm" "lowLegFrontProxySkinBtwRGT_ikHndl_pointConstraint1.tg[0].tpm"
		;
connectAttr "lowLegFrontProxySkinBtwRGT_ikHndl_pointConstraint1.w0" "lowLegFrontProxySkinBtwRGT_ikHndl_pointConstraint1.tg[0].tw"
		;
connectAttr "lowLegFrontProxySkinBtwVoidEndRGT_jnt.t" "lowLegFrontProxySkinBtwRGT_ikHndl_pointConstraint1.tg[1].tt"
		;
connectAttr "lowLegFrontProxySkinBtwVoidEndRGT_jnt.rp" "lowLegFrontProxySkinBtwRGT_ikHndl_pointConstraint1.tg[1].trp"
		;
connectAttr "lowLegFrontProxySkinBtwVoidEndRGT_jnt.rpt" "lowLegFrontProxySkinBtwRGT_ikHndl_pointConstraint1.tg[1].trt"
		;
connectAttr "lowLegFrontProxySkinBtwVoidEndRGT_jnt.pm" "lowLegFrontProxySkinBtwRGT_ikHndl_pointConstraint1.tg[1].tpm"
		;
connectAttr "lowLegFrontProxySkinBtwRGT_ikHndl_pointConstraint1.w1" "lowLegFrontProxySkinBtwRGT_ikHndl_pointConstraint1.tg[1].tw"
		;
connectAttr "lowLegFrontProxySkinBtwRGT_jnt.aimBtw" "lowLegFrontProxySkinBtwRGT_ikHndl_pointConstraint1.w0"
		;
connectAttr "lowLegFrontProxySkinDriveVoidRGT_adl.ox" "lowLegFrontProxySkinBtwRGT_ikHndl_pointConstraint1.w1"
		;
connectAttr "lowLegFrontProxySkinDoubleScaleARGT_adl.o" "lowLegFrontProxySkinBtwRGT_jnt.sx"
		 -l on;
connectAttr "lowLegFrontProxySkinDoubleScaleBRGT_adl.o" "lowLegFrontProxySkinBtwRGT_jnt.sz"
		 -l on;
connectAttr "lowLegFrontProxySkinBtwRGT_jnt_parentConstraint1.ctx" "lowLegFrontProxySkinBtwRGT_jnt.tx"
		 -l on;
connectAttr "lowLegFrontProxySkinBtwRGT_jnt_parentConstraint1.cty" "lowLegFrontProxySkinBtwRGT_jnt.ty"
		 -l on;
connectAttr "lowLegFrontProxySkinBtwRGT_jnt_parentConstraint1.ctz" "lowLegFrontProxySkinBtwRGT_jnt.tz"
		 -l on;
connectAttr "lowLegFrontProxySkinBtwRGT_jnt_parentConstraint1.crx" "lowLegFrontProxySkinBtwRGT_jnt.rx"
		 -l on;
connectAttr "lowLegFrontProxySkinBtwRGT_jnt_parentConstraint1.cry" "lowLegFrontProxySkinBtwRGT_jnt.ry"
		 -l on;
connectAttr "lowLegFrontProxySkinBtwRGT_jnt_parentConstraint1.crz" "lowLegFrontProxySkinBtwRGT_jnt.rz"
		 -l on;
connectAttr "lowLegFrontProxySkinBtwRGT_jnt.s" "lowLegFrontProxySkinBtwEndRGT_jnt.is"
		;
connectAttr "lowLegFrontProxySkinBtwRGT_jnt.ro" "lowLegFrontProxySkinBtwRGT_jnt_parentConstraint1.cro"
		;
connectAttr "lowLegFrontProxySkinBtwRGT_jnt.pim" "lowLegFrontProxySkinBtwRGT_jnt_parentConstraint1.cpim"
		;
connectAttr "lowLegFrontProxySkinBtwRGT_jnt.rp" "lowLegFrontProxySkinBtwRGT_jnt_parentConstraint1.crp"
		;
connectAttr "lowLegFrontProxySkinBtwRGT_jnt.rpt" "lowLegFrontProxySkinBtwRGT_jnt_parentConstraint1.crt"
		;
connectAttr "lowLegFrontProxySkinBtwRGT_jnt.jo" "lowLegFrontProxySkinBtwRGT_jnt_parentConstraint1.cjo"
		;
connectAttr "lowLegFrontProxySkinBtwIkRGT_jnt.t" "lowLegFrontProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tt"
		;
connectAttr "lowLegFrontProxySkinBtwIkRGT_jnt.rp" "lowLegFrontProxySkinBtwRGT_jnt_parentConstraint1.tg[0].trp"
		;
connectAttr "lowLegFrontProxySkinBtwIkRGT_jnt.rpt" "lowLegFrontProxySkinBtwRGT_jnt_parentConstraint1.tg[0].trt"
		;
connectAttr "lowLegFrontProxySkinBtwIkRGT_jnt.r" "lowLegFrontProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tr"
		;
connectAttr "lowLegFrontProxySkinBtwIkRGT_jnt.ro" "lowLegFrontProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tro"
		;
connectAttr "lowLegFrontProxySkinBtwIkRGT_jnt.s" "lowLegFrontProxySkinBtwRGT_jnt_parentConstraint1.tg[0].ts"
		;
connectAttr "lowLegFrontProxySkinBtwIkRGT_jnt.pm" "lowLegFrontProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tpm"
		;
connectAttr "lowLegFrontProxySkinBtwIkRGT_jnt.jo" "lowLegFrontProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tjo"
		;
connectAttr "lowLegFrontProxySkinBtwIkRGT_jnt.ssc" "lowLegFrontProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tsc"
		;
connectAttr "lowLegFrontProxySkinBtwIkRGT_jnt.is" "lowLegFrontProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tis"
		;
connectAttr "lowLegFrontProxySkinBtwRGT_jnt_parentConstraint1.w0" "lowLegFrontProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tw"
		;
connectAttr "lowLegFrontProxySkinBtwAllRGT_grp.pim" "lowLegFrontProxySkinBtwAllRGT_grp_pointConstraint1.cpim"
		;
connectAttr "lowLegFrontProxySkinBtwAllRGT_grp.rp" "lowLegFrontProxySkinBtwAllRGT_grp_pointConstraint1.crp"
		;
connectAttr "lowLegFrontProxySkinBtwAllRGT_grp.rpt" "lowLegFrontProxySkinBtwAllRGT_grp_pointConstraint1.crt"
		;
connectAttr "lowLegFrontProxySkinRGT_jnt.t" "lowLegFrontProxySkinBtwAllRGT_grp_pointConstraint1.tg[0].tt"
		;
connectAttr "lowLegFrontProxySkinRGT_jnt.rp" "lowLegFrontProxySkinBtwAllRGT_grp_pointConstraint1.tg[0].trp"
		;
connectAttr "lowLegFrontProxySkinRGT_jnt.rpt" "lowLegFrontProxySkinBtwAllRGT_grp_pointConstraint1.tg[0].trt"
		;
connectAttr "lowLegFrontProxySkinRGT_jnt.pm" "lowLegFrontProxySkinBtwAllRGT_grp_pointConstraint1.tg[0].tpm"
		;
connectAttr "lowLegFrontProxySkinBtwAllRGT_grp_pointConstraint1.w0" "lowLegFrontProxySkinBtwAllRGT_grp_pointConstraint1.tg[0].tw"
		;
connectAttr "upLegFrontProxySkinRGT_jnt.s" "upLegRbnDtl1FrontProxySkinRGT_jnt.is"
		;
connectAttr "upLegFrontProxySkinRGT_jnt.s" "upLegRbnDtl2FrontProxySkinRGT_jnt.is"
		;
connectAttr "upLegFrontProxySkinRGT_jnt.s" "upLegRbnDtl3FrontProxySkinRGT_jnt.is"
		;
connectAttr "upLegFrontProxySkinRGT_jnt.s" "upLegRbnDtl4FrontProxySkinRGT_jnt.is"
		;
connectAttr "upLegFrontProxySkinRGT_jnt.s" "upLegRbnDtl5FrontProxySkinRGT_jnt.is"
		;
connectAttr "midLegFrontProxySkinBtwIkRGT_jnt.s" "midLegFrontProxySkinBtwEndIkRGT_jnt.is"
		;
connectAttr "midLegFrontProxySkinBtwEndIkRGT_jnt.tx" "midLegFrontProxySkinBtwEndRGT_eff.tx"
		;
connectAttr "midLegFrontProxySkinBtwEndIkRGT_jnt.ty" "midLegFrontProxySkinBtwEndRGT_eff.ty"
		;
connectAttr "midLegFrontProxySkinBtwEndIkRGT_jnt.tz" "midLegFrontProxySkinBtwEndRGT_eff.tz"
		;
connectAttr "midLegFrontProxySkinBtwAimRGT_jnt_aimConstraint1.crx" "midLegFrontProxySkinBtwAimRGT_jnt.rx"
		;
connectAttr "midLegFrontProxySkinBtwAimRGT_jnt_aimConstraint1.cry" "midLegFrontProxySkinBtwAimRGT_jnt.ry"
		;
connectAttr "midLegFrontProxySkinBtwAimRGT_jnt_aimConstraint1.crz" "midLegFrontProxySkinBtwAimRGT_jnt.rz"
		;
connectAttr "midLegFrontProxySkinBtwAimRGT_jnt.s" "midLegFrontProxySkinBtwAimEndRGT_jnt.is"
		;
connectAttr "midLegFrontProxySkinBtwAimRGT_jnt.pim" "midLegFrontProxySkinBtwAimRGT_jnt_aimConstraint1.cpim"
		;
connectAttr "midLegFrontProxySkinBtwAimRGT_jnt.t" "midLegFrontProxySkinBtwAimRGT_jnt_aimConstraint1.ct"
		;
connectAttr "midLegFrontProxySkinBtwAimRGT_jnt.rp" "midLegFrontProxySkinBtwAimRGT_jnt_aimConstraint1.crp"
		;
connectAttr "midLegFrontProxySkinBtwAimRGT_jnt.rpt" "midLegFrontProxySkinBtwAimRGT_jnt_aimConstraint1.crt"
		;
connectAttr "midLegFrontProxySkinBtwAimRGT_jnt.ro" "midLegFrontProxySkinBtwAimRGT_jnt_aimConstraint1.cro"
		;
connectAttr "midLegFrontProxySkinBtwAimRGT_jnt.jo" "midLegFrontProxySkinBtwAimRGT_jnt_aimConstraint1.cjo"
		;
connectAttr "midLegFrontProxySkinBtwAimRGT_jnt.is" "midLegFrontProxySkinBtwAimRGT_jnt_aimConstraint1.is"
		;
connectAttr "midLegRbnDtl1FrontProxySkinRGT_jnt.t" "midLegFrontProxySkinBtwAimRGT_jnt_aimConstraint1.tg[0].tt"
		;
connectAttr "midLegRbnDtl1FrontProxySkinRGT_jnt.rp" "midLegFrontProxySkinBtwAimRGT_jnt_aimConstraint1.tg[0].trp"
		;
connectAttr "midLegRbnDtl1FrontProxySkinRGT_jnt.rpt" "midLegFrontProxySkinBtwAimRGT_jnt_aimConstraint1.tg[0].trt"
		;
connectAttr "midLegRbnDtl1FrontProxySkinRGT_jnt.pm" "midLegFrontProxySkinBtwAimRGT_jnt_aimConstraint1.tg[0].tpm"
		;
connectAttr "midLegFrontProxySkinBtwAimRGT_jnt_aimConstraint1.w0" "midLegFrontProxySkinBtwAimRGT_jnt_aimConstraint1.tg[0].tw"
		;
connectAttr "midLegFrontProxySkinBtwUpRGT_grp.wm" "midLegFrontProxySkinBtwAimRGT_jnt_aimConstraint1.wum"
		;
connectAttr "midLegFrontProxySkinBtwVoidRGT_jnt.s" "midLegFrontProxySkinBtwVoidEndRGT_jnt.is"
		;
connectAttr "midLegFrontProxySkinBtwIkRGT_jnt.msg" "midLegFrontProxySkinBtwRGT_ikHndl.hsj"
		;
connectAttr "midLegFrontProxySkinBtwEndRGT_eff.hp" "midLegFrontProxySkinBtwRGT_ikHndl.hee"
		;
connectAttr "ikRPsolver.msg" "midLegFrontProxySkinBtwRGT_ikHndl.hsv";
connectAttr "midLegFrontProxySkinBtwRGT_ikHndl_pointConstraint1.ctx" "midLegFrontProxySkinBtwRGT_ikHndl.tx"
		;
connectAttr "midLegFrontProxySkinBtwRGT_ikHndl_pointConstraint1.cty" "midLegFrontProxySkinBtwRGT_ikHndl.ty"
		;
connectAttr "midLegFrontProxySkinBtwRGT_ikHndl_pointConstraint1.ctz" "midLegFrontProxySkinBtwRGT_ikHndl.tz"
		;
connectAttr "midLegFrontProxySkinBtwRGT_ikHndl.pim" "midLegFrontProxySkinBtwRGT_ikHndl_pointConstraint1.cpim"
		;
connectAttr "midLegFrontProxySkinBtwRGT_ikHndl.rp" "midLegFrontProxySkinBtwRGT_ikHndl_pointConstraint1.crp"
		;
connectAttr "midLegFrontProxySkinBtwRGT_ikHndl.rpt" "midLegFrontProxySkinBtwRGT_ikHndl_pointConstraint1.crt"
		;
connectAttr "midLegFrontProxySkinBtwAimEndRGT_jnt.t" "midLegFrontProxySkinBtwRGT_ikHndl_pointConstraint1.tg[0].tt"
		;
connectAttr "midLegFrontProxySkinBtwAimEndRGT_jnt.rp" "midLegFrontProxySkinBtwRGT_ikHndl_pointConstraint1.tg[0].trp"
		;
connectAttr "midLegFrontProxySkinBtwAimEndRGT_jnt.rpt" "midLegFrontProxySkinBtwRGT_ikHndl_pointConstraint1.tg[0].trt"
		;
connectAttr "midLegFrontProxySkinBtwAimEndRGT_jnt.pm" "midLegFrontProxySkinBtwRGT_ikHndl_pointConstraint1.tg[0].tpm"
		;
connectAttr "midLegFrontProxySkinBtwRGT_ikHndl_pointConstraint1.w0" "midLegFrontProxySkinBtwRGT_ikHndl_pointConstraint1.tg[0].tw"
		;
connectAttr "midLegFrontProxySkinBtwVoidEndRGT_jnt.t" "midLegFrontProxySkinBtwRGT_ikHndl_pointConstraint1.tg[1].tt"
		;
connectAttr "midLegFrontProxySkinBtwVoidEndRGT_jnt.rp" "midLegFrontProxySkinBtwRGT_ikHndl_pointConstraint1.tg[1].trp"
		;
connectAttr "midLegFrontProxySkinBtwVoidEndRGT_jnt.rpt" "midLegFrontProxySkinBtwRGT_ikHndl_pointConstraint1.tg[1].trt"
		;
connectAttr "midLegFrontProxySkinBtwVoidEndRGT_jnt.pm" "midLegFrontProxySkinBtwRGT_ikHndl_pointConstraint1.tg[1].tpm"
		;
connectAttr "midLegFrontProxySkinBtwRGT_ikHndl_pointConstraint1.w1" "midLegFrontProxySkinBtwRGT_ikHndl_pointConstraint1.tg[1].tw"
		;
connectAttr "midLegFrontProxySkinBtwRGT_jnt.aimBtw" "midLegFrontProxySkinBtwRGT_ikHndl_pointConstraint1.w0"
		;
connectAttr "midLegFrontProxySkinDriveVoidRGT_adl.ox" "midLegFrontProxySkinBtwRGT_ikHndl_pointConstraint1.w1"
		;
connectAttr "midLegFrontProxySkinBtwRGT_jnt_parentConstraint1.ctx" "midLegFrontProxySkinBtwRGT_jnt.tx"
		 -l on;
connectAttr "midLegFrontProxySkinBtwRGT_jnt_parentConstraint1.cty" "midLegFrontProxySkinBtwRGT_jnt.ty"
		 -l on;
connectAttr "midLegFrontProxySkinBtwRGT_jnt_parentConstraint1.ctz" "midLegFrontProxySkinBtwRGT_jnt.tz"
		 -l on;
connectAttr "midLegFrontProxySkinBtwRGT_jnt_parentConstraint1.crx" "midLegFrontProxySkinBtwRGT_jnt.rx"
		 -l on;
connectAttr "midLegFrontProxySkinBtwRGT_jnt_parentConstraint1.cry" "midLegFrontProxySkinBtwRGT_jnt.ry"
		 -l on;
connectAttr "midLegFrontProxySkinBtwRGT_jnt_parentConstraint1.crz" "midLegFrontProxySkinBtwRGT_jnt.rz"
		 -l on;
connectAttr "midLegFrontProxySkinDoubleScaleARGT_adl.o" "midLegFrontProxySkinBtwRGT_jnt.sx"
		 -l on;
connectAttr "midLegFrontProxySkinDoubleScaleBRGT_adl.o" "midLegFrontProxySkinBtwRGT_jnt.sz"
		 -l on;
connectAttr "midLegFrontProxySkinBtwRGT_jnt.s" "midLegFrontProxySkinBtwEndRGT_jnt.is"
		;
connectAttr "midLegFrontProxySkinBtwRGT_jnt.ro" "midLegFrontProxySkinBtwRGT_jnt_parentConstraint1.cro"
		;
connectAttr "midLegFrontProxySkinBtwRGT_jnt.pim" "midLegFrontProxySkinBtwRGT_jnt_parentConstraint1.cpim"
		;
connectAttr "midLegFrontProxySkinBtwRGT_jnt.rp" "midLegFrontProxySkinBtwRGT_jnt_parentConstraint1.crp"
		;
connectAttr "midLegFrontProxySkinBtwRGT_jnt.rpt" "midLegFrontProxySkinBtwRGT_jnt_parentConstraint1.crt"
		;
connectAttr "midLegFrontProxySkinBtwRGT_jnt.jo" "midLegFrontProxySkinBtwRGT_jnt_parentConstraint1.cjo"
		;
connectAttr "midLegFrontProxySkinBtwIkRGT_jnt.t" "midLegFrontProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tt"
		;
connectAttr "midLegFrontProxySkinBtwIkRGT_jnt.rp" "midLegFrontProxySkinBtwRGT_jnt_parentConstraint1.tg[0].trp"
		;
connectAttr "midLegFrontProxySkinBtwIkRGT_jnt.rpt" "midLegFrontProxySkinBtwRGT_jnt_parentConstraint1.tg[0].trt"
		;
connectAttr "midLegFrontProxySkinBtwIkRGT_jnt.r" "midLegFrontProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tr"
		;
connectAttr "midLegFrontProxySkinBtwIkRGT_jnt.ro" "midLegFrontProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tro"
		;
connectAttr "midLegFrontProxySkinBtwIkRGT_jnt.s" "midLegFrontProxySkinBtwRGT_jnt_parentConstraint1.tg[0].ts"
		;
connectAttr "midLegFrontProxySkinBtwIkRGT_jnt.pm" "midLegFrontProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tpm"
		;
connectAttr "midLegFrontProxySkinBtwIkRGT_jnt.jo" "midLegFrontProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tjo"
		;
connectAttr "midLegFrontProxySkinBtwIkRGT_jnt.ssc" "midLegFrontProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tsc"
		;
connectAttr "midLegFrontProxySkinBtwIkRGT_jnt.is" "midLegFrontProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tis"
		;
connectAttr "midLegFrontProxySkinBtwRGT_jnt_parentConstraint1.w0" "midLegFrontProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tw"
		;
connectAttr "clav1ScapProxySkinRGT_jnt.s" "clav1ScaScapProxySkinRGT_jnt.is";
connectAttr "spine2ProxySkin_jnt.s" "upSpineRbnProxySkin_jnt.is";
connectAttr "spine1ProxySkin_jnt.s" "lowSpineRbnProxySkin_jnt.is";
connectAttr "rootProxySkin_jnt.s" "pelvisProxySkin_jnt.is";
connectAttr "pelvisProxySkin_jnt.s" "pelvisScaProxySkin_jnt.is";
connectAttr "pelvisProxySkin_jnt.s" "clav1HipProxySkinLFT_jnt.is";
connectAttr "clav1HipProxySkinLFT_jnt.s" "clav2HipProxySkinLFT_jnt.is";
connectAttr "clav2HipProxySkinLFT_jnt.s" "upLegBackProxySkinLFT_jnt.is";
connectAttr "upLegBackProxySkinLFT_jnt.s" "midLegBackProxySkinLFT_jnt.is";
connectAttr "midLegBackProxySkinLFT_jnt.s" "lowLegBackProxySkinLFT_jnt.is";
connectAttr "lowLegBackProxySkinLFT_jnt.s" "ankleBackProxySkinLFT_jnt.is";
connectAttr "ankleBackProxySkinLFT_jnt.s" "ballBackProxySkinLFT_jnt.is";
connectAttr "ballBackProxySkinLFT_jnt.s" "toeBackProxySkinLFT_jnt.is";
connectAttr "ballBackProxySkinBtwAllLFT_grp_pointConstraint1.ctx" "ballBackProxySkinBtwAllLFT_grp.tx"
		;
connectAttr "ballBackProxySkinBtwAllLFT_grp_pointConstraint1.cty" "ballBackProxySkinBtwAllLFT_grp.ty"
		;
connectAttr "ballBackProxySkinBtwAllLFT_grp_pointConstraint1.ctz" "ballBackProxySkinBtwAllLFT_grp.tz"
		;
connectAttr "ballBackProxySkinBtwIkLFT_jnt.s" "ballBackProxySkinBtwEndIkLFT_jnt.is"
		;
connectAttr "ballBackProxySkinBtwEndIkLFT_jnt.tx" "ballBackProxySkinBtwEndLFT_eff.tx"
		;
connectAttr "ballBackProxySkinBtwEndIkLFT_jnt.ty" "ballBackProxySkinBtwEndLFT_eff.ty"
		;
connectAttr "ballBackProxySkinBtwEndIkLFT_jnt.tz" "ballBackProxySkinBtwEndLFT_eff.tz"
		;
connectAttr "ballBackProxySkinBtwAimLFT_jnt_aimConstraint1.crx" "ballBackProxySkinBtwAimLFT_jnt.rx"
		;
connectAttr "ballBackProxySkinBtwAimLFT_jnt_aimConstraint1.cry" "ballBackProxySkinBtwAimLFT_jnt.ry"
		;
connectAttr "ballBackProxySkinBtwAimLFT_jnt_aimConstraint1.crz" "ballBackProxySkinBtwAimLFT_jnt.rz"
		;
connectAttr "ballBackProxySkinBtwAimLFT_jnt.s" "ballBackProxySkinBtwAimEndLFT_jnt.is"
		;
connectAttr "ballBackProxySkinBtwAimLFT_jnt.pim" "ballBackProxySkinBtwAimLFT_jnt_aimConstraint1.cpim"
		;
connectAttr "ballBackProxySkinBtwAimLFT_jnt.t" "ballBackProxySkinBtwAimLFT_jnt_aimConstraint1.ct"
		;
connectAttr "ballBackProxySkinBtwAimLFT_jnt.rp" "ballBackProxySkinBtwAimLFT_jnt_aimConstraint1.crp"
		;
connectAttr "ballBackProxySkinBtwAimLFT_jnt.rpt" "ballBackProxySkinBtwAimLFT_jnt_aimConstraint1.crt"
		;
connectAttr "ballBackProxySkinBtwAimLFT_jnt.ro" "ballBackProxySkinBtwAimLFT_jnt_aimConstraint1.cro"
		;
connectAttr "ballBackProxySkinBtwAimLFT_jnt.jo" "ballBackProxySkinBtwAimLFT_jnt_aimConstraint1.cjo"
		;
connectAttr "ballBackProxySkinBtwAimLFT_jnt.is" "ballBackProxySkinBtwAimLFT_jnt_aimConstraint1.is"
		;
connectAttr "toeBackProxySkinLFT_jnt.t" "ballBackProxySkinBtwAimLFT_jnt_aimConstraint1.tg[0].tt"
		;
connectAttr "toeBackProxySkinLFT_jnt.rp" "ballBackProxySkinBtwAimLFT_jnt_aimConstraint1.tg[0].trp"
		;
connectAttr "toeBackProxySkinLFT_jnt.rpt" "ballBackProxySkinBtwAimLFT_jnt_aimConstraint1.tg[0].trt"
		;
connectAttr "toeBackProxySkinLFT_jnt.pm" "ballBackProxySkinBtwAimLFT_jnt_aimConstraint1.tg[0].tpm"
		;
connectAttr "ballBackProxySkinBtwAimLFT_jnt_aimConstraint1.w0" "ballBackProxySkinBtwAimLFT_jnt_aimConstraint1.tg[0].tw"
		;
connectAttr "ballBackProxySkinBtwUpLFT_grp.wm" "ballBackProxySkinBtwAimLFT_jnt_aimConstraint1.wum"
		;
connectAttr "ballBackProxySkinBtwVoidLFT_jnt.s" "ballBackProxySkinBtwVoidEndLFT_jnt.is"
		;
connectAttr "ballBackProxySkinBtwIkLFT_jnt.msg" "ballBackProxySkinBtwLFT_ikHndl.hsj"
		;
connectAttr "ballBackProxySkinBtwEndLFT_eff.hp" "ballBackProxySkinBtwLFT_ikHndl.hee"
		;
connectAttr "ikRPsolver.msg" "ballBackProxySkinBtwLFT_ikHndl.hsv";
connectAttr "ballBackProxySkinBtwLFT_ikHndl_pointConstraint1.ctx" "ballBackProxySkinBtwLFT_ikHndl.tx"
		;
connectAttr "ballBackProxySkinBtwLFT_ikHndl_pointConstraint1.cty" "ballBackProxySkinBtwLFT_ikHndl.ty"
		;
connectAttr "ballBackProxySkinBtwLFT_ikHndl_pointConstraint1.ctz" "ballBackProxySkinBtwLFT_ikHndl.tz"
		;
connectAttr "ballBackProxySkinBtwLFT_ikHndl.pim" "ballBackProxySkinBtwLFT_ikHndl_pointConstraint1.cpim"
		;
connectAttr "ballBackProxySkinBtwLFT_ikHndl.rp" "ballBackProxySkinBtwLFT_ikHndl_pointConstraint1.crp"
		;
connectAttr "ballBackProxySkinBtwLFT_ikHndl.rpt" "ballBackProxySkinBtwLFT_ikHndl_pointConstraint1.crt"
		;
connectAttr "ballBackProxySkinBtwAimEndLFT_jnt.t" "ballBackProxySkinBtwLFT_ikHndl_pointConstraint1.tg[0].tt"
		;
connectAttr "ballBackProxySkinBtwAimEndLFT_jnt.rp" "ballBackProxySkinBtwLFT_ikHndl_pointConstraint1.tg[0].trp"
		;
connectAttr "ballBackProxySkinBtwAimEndLFT_jnt.rpt" "ballBackProxySkinBtwLFT_ikHndl_pointConstraint1.tg[0].trt"
		;
connectAttr "ballBackProxySkinBtwAimEndLFT_jnt.pm" "ballBackProxySkinBtwLFT_ikHndl_pointConstraint1.tg[0].tpm"
		;
connectAttr "ballBackProxySkinBtwLFT_ikHndl_pointConstraint1.w0" "ballBackProxySkinBtwLFT_ikHndl_pointConstraint1.tg[0].tw"
		;
connectAttr "ballBackProxySkinBtwVoidEndLFT_jnt.t" "ballBackProxySkinBtwLFT_ikHndl_pointConstraint1.tg[1].tt"
		;
connectAttr "ballBackProxySkinBtwVoidEndLFT_jnt.rp" "ballBackProxySkinBtwLFT_ikHndl_pointConstraint1.tg[1].trp"
		;
connectAttr "ballBackProxySkinBtwVoidEndLFT_jnt.rpt" "ballBackProxySkinBtwLFT_ikHndl_pointConstraint1.tg[1].trt"
		;
connectAttr "ballBackProxySkinBtwVoidEndLFT_jnt.pm" "ballBackProxySkinBtwLFT_ikHndl_pointConstraint1.tg[1].tpm"
		;
connectAttr "ballBackProxySkinBtwLFT_ikHndl_pointConstraint1.w1" "ballBackProxySkinBtwLFT_ikHndl_pointConstraint1.tg[1].tw"
		;
connectAttr "ballBackProxySkinBtwLFT_jnt.aimBtw" "ballBackProxySkinBtwLFT_ikHndl_pointConstraint1.w0"
		;
connectAttr "ballBackProxySkinDriveVoidLFT_adl.ox" "ballBackProxySkinBtwLFT_ikHndl_pointConstraint1.w1"
		;
connectAttr "ballBackProxySkinBtwLFT_jnt_parentConstraint1.ctx" "ballBackProxySkinBtwLFT_jnt.tx"
		 -l on;
connectAttr "ballBackProxySkinBtwLFT_jnt_parentConstraint1.cty" "ballBackProxySkinBtwLFT_jnt.ty"
		 -l on;
connectAttr "ballBackProxySkinBtwLFT_jnt_parentConstraint1.ctz" "ballBackProxySkinBtwLFT_jnt.tz"
		 -l on;
connectAttr "ballBackProxySkinBtwLFT_jnt_parentConstraint1.crx" "ballBackProxySkinBtwLFT_jnt.rx"
		 -l on;
connectAttr "ballBackProxySkinBtwLFT_jnt_parentConstraint1.cry" "ballBackProxySkinBtwLFT_jnt.ry"
		 -l on;
connectAttr "ballBackProxySkinBtwLFT_jnt_parentConstraint1.crz" "ballBackProxySkinBtwLFT_jnt.rz"
		 -l on;
connectAttr "ballBackProxySkinDoubleScaleALFT_adl.o" "ballBackProxySkinBtwLFT_jnt.sx"
		 -l on;
connectAttr "ballBackProxySkinDoubleScaleBLFT_adl.o" "ballBackProxySkinBtwLFT_jnt.sz"
		 -l on;
connectAttr "ballBackProxySkinBtwLFT_jnt.s" "ballBackProxySkinBtwEndLFT_jnt.is";
connectAttr "ballBackProxySkinBtwLFT_jnt.ro" "ballBackProxySkinBtwLFT_jnt_parentConstraint1.cro"
		;
connectAttr "ballBackProxySkinBtwLFT_jnt.pim" "ballBackProxySkinBtwLFT_jnt_parentConstraint1.cpim"
		;
connectAttr "ballBackProxySkinBtwLFT_jnt.rp" "ballBackProxySkinBtwLFT_jnt_parentConstraint1.crp"
		;
connectAttr "ballBackProxySkinBtwLFT_jnt.rpt" "ballBackProxySkinBtwLFT_jnt_parentConstraint1.crt"
		;
connectAttr "ballBackProxySkinBtwLFT_jnt.jo" "ballBackProxySkinBtwLFT_jnt_parentConstraint1.cjo"
		;
connectAttr "ballBackProxySkinBtwIkLFT_jnt.t" "ballBackProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tt"
		;
connectAttr "ballBackProxySkinBtwIkLFT_jnt.rp" "ballBackProxySkinBtwLFT_jnt_parentConstraint1.tg[0].trp"
		;
connectAttr "ballBackProxySkinBtwIkLFT_jnt.rpt" "ballBackProxySkinBtwLFT_jnt_parentConstraint1.tg[0].trt"
		;
connectAttr "ballBackProxySkinBtwIkLFT_jnt.r" "ballBackProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tr"
		;
connectAttr "ballBackProxySkinBtwIkLFT_jnt.ro" "ballBackProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tro"
		;
connectAttr "ballBackProxySkinBtwIkLFT_jnt.s" "ballBackProxySkinBtwLFT_jnt_parentConstraint1.tg[0].ts"
		;
connectAttr "ballBackProxySkinBtwIkLFT_jnt.pm" "ballBackProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tpm"
		;
connectAttr "ballBackProxySkinBtwIkLFT_jnt.jo" "ballBackProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tjo"
		;
connectAttr "ballBackProxySkinBtwIkLFT_jnt.ssc" "ballBackProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tsc"
		;
connectAttr "ballBackProxySkinBtwIkLFT_jnt.is" "ballBackProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tis"
		;
connectAttr "ballBackProxySkinBtwLFT_jnt_parentConstraint1.w0" "ballBackProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tw"
		;
connectAttr "ballBackProxySkinBtwAllLFT_grp.pim" "ballBackProxySkinBtwAllLFT_grp_pointConstraint1.cpim"
		;
connectAttr "ballBackProxySkinBtwAllLFT_grp.rp" "ballBackProxySkinBtwAllLFT_grp_pointConstraint1.crp"
		;
connectAttr "ballBackProxySkinBtwAllLFT_grp.rpt" "ballBackProxySkinBtwAllLFT_grp_pointConstraint1.crt"
		;
connectAttr "ballBackProxySkinLFT_jnt.t" "ballBackProxySkinBtwAllLFT_grp_pointConstraint1.tg[0].tt"
		;
connectAttr "ballBackProxySkinLFT_jnt.rp" "ballBackProxySkinBtwAllLFT_grp_pointConstraint1.tg[0].trp"
		;
connectAttr "ballBackProxySkinLFT_jnt.rpt" "ballBackProxySkinBtwAllLFT_grp_pointConstraint1.tg[0].trt"
		;
connectAttr "ballBackProxySkinLFT_jnt.pm" "ballBackProxySkinBtwAllLFT_grp_pointConstraint1.tg[0].tpm"
		;
connectAttr "ballBackProxySkinBtwAllLFT_grp_pointConstraint1.w0" "ballBackProxySkinBtwAllLFT_grp_pointConstraint1.tg[0].tw"
		;
connectAttr "lowLegBackProxySkinLFT_jnt.s" "lowLegRbnDtl1BackProxySkinLFT_jnt.is"
		;
connectAttr "lowLegBackProxySkinLFT_jnt.s" "lowLegRbnDtl2BackProxySkinLFT_jnt.is"
		;
connectAttr "lowLegBackProxySkinLFT_jnt.s" "lowLegRbnDtl3BackProxySkinLFT_jnt.is"
		;
connectAttr "lowLegBackProxySkinLFT_jnt.s" "lowLegRbnDtl4BackProxySkinLFT_jnt.is"
		;
connectAttr "lowLegBackProxySkinLFT_jnt.s" "lowLegRbnDtl5BackProxySkinLFT_jnt.is"
		;
connectAttr "ankleBackProxySkinBtwAllLFT_grp_pointConstraint1.ctx" "ankleBackProxySkinBtwAllLFT_grp.tx"
		;
connectAttr "ankleBackProxySkinBtwAllLFT_grp_pointConstraint1.cty" "ankleBackProxySkinBtwAllLFT_grp.ty"
		;
connectAttr "ankleBackProxySkinBtwAllLFT_grp_pointConstraint1.ctz" "ankleBackProxySkinBtwAllLFT_grp.tz"
		;
connectAttr "ankleBackProxySkinBtwIkLFT_jnt.s" "ankleBackProxySkinBtwEndIkLFT_jnt.is"
		;
connectAttr "ankleBackProxySkinBtwEndIkLFT_jnt.tx" "ankleBackProxySkinBtwEndLFT_eff.tx"
		;
connectAttr "ankleBackProxySkinBtwEndIkLFT_jnt.ty" "ankleBackProxySkinBtwEndLFT_eff.ty"
		;
connectAttr "ankleBackProxySkinBtwEndIkLFT_jnt.tz" "ankleBackProxySkinBtwEndLFT_eff.tz"
		;
connectAttr "ankleBackProxySkinBtwAimLFT_jnt_aimConstraint1.crx" "ankleBackProxySkinBtwAimLFT_jnt.rx"
		;
connectAttr "ankleBackProxySkinBtwAimLFT_jnt_aimConstraint1.cry" "ankleBackProxySkinBtwAimLFT_jnt.ry"
		;
connectAttr "ankleBackProxySkinBtwAimLFT_jnt_aimConstraint1.crz" "ankleBackProxySkinBtwAimLFT_jnt.rz"
		;
connectAttr "ankleBackProxySkinBtwAimLFT_jnt.s" "ankleBackProxySkinBtwAimEndLFT_jnt.is"
		;
connectAttr "ankleBackProxySkinBtwAimLFT_jnt.pim" "ankleBackProxySkinBtwAimLFT_jnt_aimConstraint1.cpim"
		;
connectAttr "ankleBackProxySkinBtwAimLFT_jnt.t" "ankleBackProxySkinBtwAimLFT_jnt_aimConstraint1.ct"
		;
connectAttr "ankleBackProxySkinBtwAimLFT_jnt.rp" "ankleBackProxySkinBtwAimLFT_jnt_aimConstraint1.crp"
		;
connectAttr "ankleBackProxySkinBtwAimLFT_jnt.rpt" "ankleBackProxySkinBtwAimLFT_jnt_aimConstraint1.crt"
		;
connectAttr "ankleBackProxySkinBtwAimLFT_jnt.ro" "ankleBackProxySkinBtwAimLFT_jnt_aimConstraint1.cro"
		;
connectAttr "ankleBackProxySkinBtwAimLFT_jnt.jo" "ankleBackProxySkinBtwAimLFT_jnt_aimConstraint1.cjo"
		;
connectAttr "ankleBackProxySkinBtwAimLFT_jnt.is" "ankleBackProxySkinBtwAimLFT_jnt_aimConstraint1.is"
		;
connectAttr "ballBackProxySkinLFT_jnt.t" "ankleBackProxySkinBtwAimLFT_jnt_aimConstraint1.tg[0].tt"
		;
connectAttr "ballBackProxySkinLFT_jnt.rp" "ankleBackProxySkinBtwAimLFT_jnt_aimConstraint1.tg[0].trp"
		;
connectAttr "ballBackProxySkinLFT_jnt.rpt" "ankleBackProxySkinBtwAimLFT_jnt_aimConstraint1.tg[0].trt"
		;
connectAttr "ballBackProxySkinLFT_jnt.pm" "ankleBackProxySkinBtwAimLFT_jnt_aimConstraint1.tg[0].tpm"
		;
connectAttr "ankleBackProxySkinBtwAimLFT_jnt_aimConstraint1.w0" "ankleBackProxySkinBtwAimLFT_jnt_aimConstraint1.tg[0].tw"
		;
connectAttr "ankleBackProxySkinBtwUpLFT_grp.wm" "ankleBackProxySkinBtwAimLFT_jnt_aimConstraint1.wum"
		;
connectAttr "ankleBackProxySkinBtwVoidLFT_jnt.s" "ankleBackProxySkinBtwVoidEndLFT_jnt.is"
		;
connectAttr "ankleBackProxySkinBtwIkLFT_jnt.msg" "ankleBackProxySkinBtwLFT_ikHndl.hsj"
		;
connectAttr "ankleBackProxySkinBtwEndLFT_eff.hp" "ankleBackProxySkinBtwLFT_ikHndl.hee"
		;
connectAttr "ikRPsolver.msg" "ankleBackProxySkinBtwLFT_ikHndl.hsv";
connectAttr "ankleBackProxySkinBtwLFT_ikHndl_pointConstraint1.ctx" "ankleBackProxySkinBtwLFT_ikHndl.tx"
		;
connectAttr "ankleBackProxySkinBtwLFT_ikHndl_pointConstraint1.cty" "ankleBackProxySkinBtwLFT_ikHndl.ty"
		;
connectAttr "ankleBackProxySkinBtwLFT_ikHndl_pointConstraint1.ctz" "ankleBackProxySkinBtwLFT_ikHndl.tz"
		;
connectAttr "ankleBackProxySkinBtwLFT_ikHndl.pim" "ankleBackProxySkinBtwLFT_ikHndl_pointConstraint1.cpim"
		;
connectAttr "ankleBackProxySkinBtwLFT_ikHndl.rp" "ankleBackProxySkinBtwLFT_ikHndl_pointConstraint1.crp"
		;
connectAttr "ankleBackProxySkinBtwLFT_ikHndl.rpt" "ankleBackProxySkinBtwLFT_ikHndl_pointConstraint1.crt"
		;
connectAttr "ankleBackProxySkinBtwAimEndLFT_jnt.t" "ankleBackProxySkinBtwLFT_ikHndl_pointConstraint1.tg[0].tt"
		;
connectAttr "ankleBackProxySkinBtwAimEndLFT_jnt.rp" "ankleBackProxySkinBtwLFT_ikHndl_pointConstraint1.tg[0].trp"
		;
connectAttr "ankleBackProxySkinBtwAimEndLFT_jnt.rpt" "ankleBackProxySkinBtwLFT_ikHndl_pointConstraint1.tg[0].trt"
		;
connectAttr "ankleBackProxySkinBtwAimEndLFT_jnt.pm" "ankleBackProxySkinBtwLFT_ikHndl_pointConstraint1.tg[0].tpm"
		;
connectAttr "ankleBackProxySkinBtwLFT_ikHndl_pointConstraint1.w0" "ankleBackProxySkinBtwLFT_ikHndl_pointConstraint1.tg[0].tw"
		;
connectAttr "ankleBackProxySkinBtwVoidEndLFT_jnt.t" "ankleBackProxySkinBtwLFT_ikHndl_pointConstraint1.tg[1].tt"
		;
connectAttr "ankleBackProxySkinBtwVoidEndLFT_jnt.rp" "ankleBackProxySkinBtwLFT_ikHndl_pointConstraint1.tg[1].trp"
		;
connectAttr "ankleBackProxySkinBtwVoidEndLFT_jnt.rpt" "ankleBackProxySkinBtwLFT_ikHndl_pointConstraint1.tg[1].trt"
		;
connectAttr "ankleBackProxySkinBtwVoidEndLFT_jnt.pm" "ankleBackProxySkinBtwLFT_ikHndl_pointConstraint1.tg[1].tpm"
		;
connectAttr "ankleBackProxySkinBtwLFT_ikHndl_pointConstraint1.w1" "ankleBackProxySkinBtwLFT_ikHndl_pointConstraint1.tg[1].tw"
		;
connectAttr "ankleBackProxySkinBtwLFT_jnt.aimBtw" "ankleBackProxySkinBtwLFT_ikHndl_pointConstraint1.w0"
		;
connectAttr "ankleBackProxySkinDriveVoidLFT_adl.ox" "ankleBackProxySkinBtwLFT_ikHndl_pointConstraint1.w1"
		;
connectAttr "ankleBackProxySkinBtwLFT_jnt_parentConstraint1.ctx" "ankleBackProxySkinBtwLFT_jnt.tx"
		 -l on;
connectAttr "ankleBackProxySkinBtwLFT_jnt_parentConstraint1.cty" "ankleBackProxySkinBtwLFT_jnt.ty"
		 -l on;
connectAttr "ankleBackProxySkinBtwLFT_jnt_parentConstraint1.ctz" "ankleBackProxySkinBtwLFT_jnt.tz"
		 -l on;
connectAttr "ankleBackProxySkinBtwLFT_jnt_parentConstraint1.crx" "ankleBackProxySkinBtwLFT_jnt.rx"
		 -l on;
connectAttr "ankleBackProxySkinBtwLFT_jnt_parentConstraint1.cry" "ankleBackProxySkinBtwLFT_jnt.ry"
		 -l on;
connectAttr "ankleBackProxySkinBtwLFT_jnt_parentConstraint1.crz" "ankleBackProxySkinBtwLFT_jnt.rz"
		 -l on;
connectAttr "ankleBackProxySkinDoubleScaleALFT_adl.o" "ankleBackProxySkinBtwLFT_jnt.sx"
		 -l on;
connectAttr "ankleBackProxySkinDoubleScaleBLFT_adl.o" "ankleBackProxySkinBtwLFT_jnt.sz"
		 -l on;
connectAttr "ankleBackProxySkinBtwLFT_jnt.s" "ankleBackProxySkinBtwEndLFT_jnt.is"
		;
connectAttr "ankleBackProxySkinBtwLFT_jnt.ro" "ankleBackProxySkinBtwLFT_jnt_parentConstraint1.cro"
		;
connectAttr "ankleBackProxySkinBtwLFT_jnt.pim" "ankleBackProxySkinBtwLFT_jnt_parentConstraint1.cpim"
		;
connectAttr "ankleBackProxySkinBtwLFT_jnt.rp" "ankleBackProxySkinBtwLFT_jnt_parentConstraint1.crp"
		;
connectAttr "ankleBackProxySkinBtwLFT_jnt.rpt" "ankleBackProxySkinBtwLFT_jnt_parentConstraint1.crt"
		;
connectAttr "ankleBackProxySkinBtwLFT_jnt.jo" "ankleBackProxySkinBtwLFT_jnt_parentConstraint1.cjo"
		;
connectAttr "ankleBackProxySkinBtwIkLFT_jnt.t" "ankleBackProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tt"
		;
connectAttr "ankleBackProxySkinBtwIkLFT_jnt.rp" "ankleBackProxySkinBtwLFT_jnt_parentConstraint1.tg[0].trp"
		;
connectAttr "ankleBackProxySkinBtwIkLFT_jnt.rpt" "ankleBackProxySkinBtwLFT_jnt_parentConstraint1.tg[0].trt"
		;
connectAttr "ankleBackProxySkinBtwIkLFT_jnt.r" "ankleBackProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tr"
		;
connectAttr "ankleBackProxySkinBtwIkLFT_jnt.ro" "ankleBackProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tro"
		;
connectAttr "ankleBackProxySkinBtwIkLFT_jnt.s" "ankleBackProxySkinBtwLFT_jnt_parentConstraint1.tg[0].ts"
		;
connectAttr "ankleBackProxySkinBtwIkLFT_jnt.pm" "ankleBackProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tpm"
		;
connectAttr "ankleBackProxySkinBtwIkLFT_jnt.jo" "ankleBackProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tjo"
		;
connectAttr "ankleBackProxySkinBtwIkLFT_jnt.ssc" "ankleBackProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tsc"
		;
connectAttr "ankleBackProxySkinBtwIkLFT_jnt.is" "ankleBackProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tis"
		;
connectAttr "ankleBackProxySkinBtwLFT_jnt_parentConstraint1.w0" "ankleBackProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tw"
		;
connectAttr "ankleBackProxySkinBtwAllLFT_grp.pim" "ankleBackProxySkinBtwAllLFT_grp_pointConstraint1.cpim"
		;
connectAttr "ankleBackProxySkinBtwAllLFT_grp.rp" "ankleBackProxySkinBtwAllLFT_grp_pointConstraint1.crp"
		;
connectAttr "ankleBackProxySkinBtwAllLFT_grp.rpt" "ankleBackProxySkinBtwAllLFT_grp_pointConstraint1.crt"
		;
connectAttr "ankleBackProxySkinLFT_jnt.t" "ankleBackProxySkinBtwAllLFT_grp_pointConstraint1.tg[0].tt"
		;
connectAttr "ankleBackProxySkinLFT_jnt.rp" "ankleBackProxySkinBtwAllLFT_grp_pointConstraint1.tg[0].trp"
		;
connectAttr "ankleBackProxySkinLFT_jnt.rpt" "ankleBackProxySkinBtwAllLFT_grp_pointConstraint1.tg[0].trt"
		;
connectAttr "ankleBackProxySkinLFT_jnt.pm" "ankleBackProxySkinBtwAllLFT_grp_pointConstraint1.tg[0].tpm"
		;
connectAttr "ankleBackProxySkinBtwAllLFT_grp_pointConstraint1.w0" "ankleBackProxySkinBtwAllLFT_grp_pointConstraint1.tg[0].tw"
		;
connectAttr "midLegBackProxySkinLFT_jnt.s" "midLegRbnDtl1BackProxySkinLFT_jnt.is"
		;
connectAttr "midLegBackProxySkinLFT_jnt.s" "midLegRbnDtl2BackProxySkinLFT_jnt.is"
		;
connectAttr "midLegBackProxySkinLFT_jnt.s" "midLegRbnDtl3BackProxySkinLFT_jnt.is"
		;
connectAttr "midLegBackProxySkinLFT_jnt.s" "midLegRbnDtl4BackProxySkinLFT_jnt.is"
		;
connectAttr "midLegBackProxySkinLFT_jnt.s" "midLegRbnDtl5BackProxySkinLFT_jnt.is"
		;
connectAttr "lowLegBackProxySkinBtwAllLFT_grp_pointConstraint1.ctx" "lowLegBackProxySkinBtwAllLFT_grp.tx"
		;
connectAttr "lowLegBackProxySkinBtwAllLFT_grp_pointConstraint1.cty" "lowLegBackProxySkinBtwAllLFT_grp.ty"
		;
connectAttr "lowLegBackProxySkinBtwAllLFT_grp_pointConstraint1.ctz" "lowLegBackProxySkinBtwAllLFT_grp.tz"
		;
connectAttr "lowLegBackProxySkinBtwIkLFT_jnt.s" "lowLegBackProxySkinBtwEndIkLFT_jnt.is"
		;
connectAttr "lowLegBackProxySkinBtwEndIkLFT_jnt.tx" "lowLegBackProxySkinBtwEndLFT_eff.tx"
		;
connectAttr "lowLegBackProxySkinBtwEndIkLFT_jnt.ty" "lowLegBackProxySkinBtwEndLFT_eff.ty"
		;
connectAttr "lowLegBackProxySkinBtwEndIkLFT_jnt.tz" "lowLegBackProxySkinBtwEndLFT_eff.tz"
		;
connectAttr "lowLegBackProxySkinBtwAimLFT_jnt_aimConstraint1.crx" "lowLegBackProxySkinBtwAimLFT_jnt.rx"
		;
connectAttr "lowLegBackProxySkinBtwAimLFT_jnt_aimConstraint1.cry" "lowLegBackProxySkinBtwAimLFT_jnt.ry"
		;
connectAttr "lowLegBackProxySkinBtwAimLFT_jnt_aimConstraint1.crz" "lowLegBackProxySkinBtwAimLFT_jnt.rz"
		;
connectAttr "lowLegBackProxySkinBtwAimLFT_jnt.s" "lowLegBackProxySkinBtwAimEndLFT_jnt.is"
		;
connectAttr "lowLegBackProxySkinBtwAimLFT_jnt.pim" "lowLegBackProxySkinBtwAimLFT_jnt_aimConstraint1.cpim"
		;
connectAttr "lowLegBackProxySkinBtwAimLFT_jnt.t" "lowLegBackProxySkinBtwAimLFT_jnt_aimConstraint1.ct"
		;
connectAttr "lowLegBackProxySkinBtwAimLFT_jnt.rp" "lowLegBackProxySkinBtwAimLFT_jnt_aimConstraint1.crp"
		;
connectAttr "lowLegBackProxySkinBtwAimLFT_jnt.rpt" "lowLegBackProxySkinBtwAimLFT_jnt_aimConstraint1.crt"
		;
connectAttr "lowLegBackProxySkinBtwAimLFT_jnt.ro" "lowLegBackProxySkinBtwAimLFT_jnt_aimConstraint1.cro"
		;
connectAttr "lowLegBackProxySkinBtwAimLFT_jnt.jo" "lowLegBackProxySkinBtwAimLFT_jnt_aimConstraint1.cjo"
		;
connectAttr "lowLegBackProxySkinBtwAimLFT_jnt.is" "lowLegBackProxySkinBtwAimLFT_jnt_aimConstraint1.is"
		;
connectAttr "lowLegRbnDtl1BackProxySkinLFT_jnt.t" "lowLegBackProxySkinBtwAimLFT_jnt_aimConstraint1.tg[0].tt"
		;
connectAttr "lowLegRbnDtl1BackProxySkinLFT_jnt.rp" "lowLegBackProxySkinBtwAimLFT_jnt_aimConstraint1.tg[0].trp"
		;
connectAttr "lowLegRbnDtl1BackProxySkinLFT_jnt.rpt" "lowLegBackProxySkinBtwAimLFT_jnt_aimConstraint1.tg[0].trt"
		;
connectAttr "lowLegRbnDtl1BackProxySkinLFT_jnt.pm" "lowLegBackProxySkinBtwAimLFT_jnt_aimConstraint1.tg[0].tpm"
		;
connectAttr "lowLegBackProxySkinBtwAimLFT_jnt_aimConstraint1.w0" "lowLegBackProxySkinBtwAimLFT_jnt_aimConstraint1.tg[0].tw"
		;
connectAttr "lowLegBackProxySkinBtwUpLFT_grp.wm" "lowLegBackProxySkinBtwAimLFT_jnt_aimConstraint1.wum"
		;
connectAttr "lowLegBackProxySkinBtwVoidLFT_jnt.s" "lowLegBackProxySkinBtwVoidEndLFT_jnt.is"
		;
connectAttr "lowLegBackProxySkinBtwIkLFT_jnt.msg" "lowLegBackProxySkinBtwLFT_ikHndl.hsj"
		;
connectAttr "lowLegBackProxySkinBtwEndLFT_eff.hp" "lowLegBackProxySkinBtwLFT_ikHndl.hee"
		;
connectAttr "ikRPsolver.msg" "lowLegBackProxySkinBtwLFT_ikHndl.hsv";
connectAttr "lowLegBackProxySkinBtwLFT_ikHndl_pointConstraint1.ctx" "lowLegBackProxySkinBtwLFT_ikHndl.tx"
		;
connectAttr "lowLegBackProxySkinBtwLFT_ikHndl_pointConstraint1.cty" "lowLegBackProxySkinBtwLFT_ikHndl.ty"
		;
connectAttr "lowLegBackProxySkinBtwLFT_ikHndl_pointConstraint1.ctz" "lowLegBackProxySkinBtwLFT_ikHndl.tz"
		;
connectAttr "lowLegBackProxySkinBtwLFT_ikHndl.pim" "lowLegBackProxySkinBtwLFT_ikHndl_pointConstraint1.cpim"
		;
connectAttr "lowLegBackProxySkinBtwLFT_ikHndl.rp" "lowLegBackProxySkinBtwLFT_ikHndl_pointConstraint1.crp"
		;
connectAttr "lowLegBackProxySkinBtwLFT_ikHndl.rpt" "lowLegBackProxySkinBtwLFT_ikHndl_pointConstraint1.crt"
		;
connectAttr "lowLegBackProxySkinBtwAimEndLFT_jnt.t" "lowLegBackProxySkinBtwLFT_ikHndl_pointConstraint1.tg[0].tt"
		;
connectAttr "lowLegBackProxySkinBtwAimEndLFT_jnt.rp" "lowLegBackProxySkinBtwLFT_ikHndl_pointConstraint1.tg[0].trp"
		;
connectAttr "lowLegBackProxySkinBtwAimEndLFT_jnt.rpt" "lowLegBackProxySkinBtwLFT_ikHndl_pointConstraint1.tg[0].trt"
		;
connectAttr "lowLegBackProxySkinBtwAimEndLFT_jnt.pm" "lowLegBackProxySkinBtwLFT_ikHndl_pointConstraint1.tg[0].tpm"
		;
connectAttr "lowLegBackProxySkinBtwLFT_ikHndl_pointConstraint1.w0" "lowLegBackProxySkinBtwLFT_ikHndl_pointConstraint1.tg[0].tw"
		;
connectAttr "lowLegBackProxySkinBtwVoidEndLFT_jnt.t" "lowLegBackProxySkinBtwLFT_ikHndl_pointConstraint1.tg[1].tt"
		;
connectAttr "lowLegBackProxySkinBtwVoidEndLFT_jnt.rp" "lowLegBackProxySkinBtwLFT_ikHndl_pointConstraint1.tg[1].trp"
		;
connectAttr "lowLegBackProxySkinBtwVoidEndLFT_jnt.rpt" "lowLegBackProxySkinBtwLFT_ikHndl_pointConstraint1.tg[1].trt"
		;
connectAttr "lowLegBackProxySkinBtwVoidEndLFT_jnt.pm" "lowLegBackProxySkinBtwLFT_ikHndl_pointConstraint1.tg[1].tpm"
		;
connectAttr "lowLegBackProxySkinBtwLFT_ikHndl_pointConstraint1.w1" "lowLegBackProxySkinBtwLFT_ikHndl_pointConstraint1.tg[1].tw"
		;
connectAttr "lowLegBackProxySkinBtwLFT_jnt.aimBtw" "lowLegBackProxySkinBtwLFT_ikHndl_pointConstraint1.w0"
		;
connectAttr "lowLegBackProxySkinDriveVoidLFT_adl.ox" "lowLegBackProxySkinBtwLFT_ikHndl_pointConstraint1.w1"
		;
connectAttr "lowLegBackProxySkinBtwLFT_jnt_parentConstraint1.ctx" "lowLegBackProxySkinBtwLFT_jnt.tx"
		 -l on;
connectAttr "lowLegBackProxySkinBtwLFT_jnt_parentConstraint1.cty" "lowLegBackProxySkinBtwLFT_jnt.ty"
		 -l on;
connectAttr "lowLegBackProxySkinBtwLFT_jnt_parentConstraint1.ctz" "lowLegBackProxySkinBtwLFT_jnt.tz"
		 -l on;
connectAttr "lowLegBackProxySkinBtwLFT_jnt_parentConstraint1.crx" "lowLegBackProxySkinBtwLFT_jnt.rx"
		 -l on;
connectAttr "lowLegBackProxySkinBtwLFT_jnt_parentConstraint1.cry" "lowLegBackProxySkinBtwLFT_jnt.ry"
		 -l on;
connectAttr "lowLegBackProxySkinBtwLFT_jnt_parentConstraint1.crz" "lowLegBackProxySkinBtwLFT_jnt.rz"
		 -l on;
connectAttr "lowLegBackProxySkinDoubleScaleALFT_adl.o" "lowLegBackProxySkinBtwLFT_jnt.sx"
		 -l on;
connectAttr "lowLegBackProxySkinDoubleScaleBLFT_adl.o" "lowLegBackProxySkinBtwLFT_jnt.sz"
		 -l on;
connectAttr "lowLegBackProxySkinBtwLFT_jnt.s" "lowLegBackProxySkinBtwEndLFT_jnt.is"
		;
connectAttr "lowLegBackProxySkinBtwLFT_jnt.ro" "lowLegBackProxySkinBtwLFT_jnt_parentConstraint1.cro"
		;
connectAttr "lowLegBackProxySkinBtwLFT_jnt.pim" "lowLegBackProxySkinBtwLFT_jnt_parentConstraint1.cpim"
		;
connectAttr "lowLegBackProxySkinBtwLFT_jnt.rp" "lowLegBackProxySkinBtwLFT_jnt_parentConstraint1.crp"
		;
connectAttr "lowLegBackProxySkinBtwLFT_jnt.rpt" "lowLegBackProxySkinBtwLFT_jnt_parentConstraint1.crt"
		;
connectAttr "lowLegBackProxySkinBtwLFT_jnt.jo" "lowLegBackProxySkinBtwLFT_jnt_parentConstraint1.cjo"
		;
connectAttr "lowLegBackProxySkinBtwIkLFT_jnt.t" "lowLegBackProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tt"
		;
connectAttr "lowLegBackProxySkinBtwIkLFT_jnt.rp" "lowLegBackProxySkinBtwLFT_jnt_parentConstraint1.tg[0].trp"
		;
connectAttr "lowLegBackProxySkinBtwIkLFT_jnt.rpt" "lowLegBackProxySkinBtwLFT_jnt_parentConstraint1.tg[0].trt"
		;
connectAttr "lowLegBackProxySkinBtwIkLFT_jnt.r" "lowLegBackProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tr"
		;
connectAttr "lowLegBackProxySkinBtwIkLFT_jnt.ro" "lowLegBackProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tro"
		;
connectAttr "lowLegBackProxySkinBtwIkLFT_jnt.s" "lowLegBackProxySkinBtwLFT_jnt_parentConstraint1.tg[0].ts"
		;
connectAttr "lowLegBackProxySkinBtwIkLFT_jnt.pm" "lowLegBackProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tpm"
		;
connectAttr "lowLegBackProxySkinBtwIkLFT_jnt.jo" "lowLegBackProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tjo"
		;
connectAttr "lowLegBackProxySkinBtwIkLFT_jnt.ssc" "lowLegBackProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tsc"
		;
connectAttr "lowLegBackProxySkinBtwIkLFT_jnt.is" "lowLegBackProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tis"
		;
connectAttr "lowLegBackProxySkinBtwLFT_jnt_parentConstraint1.w0" "lowLegBackProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tw"
		;
connectAttr "lowLegBackProxySkinBtwAllLFT_grp.pim" "lowLegBackProxySkinBtwAllLFT_grp_pointConstraint1.cpim"
		;
connectAttr "lowLegBackProxySkinBtwAllLFT_grp.rp" "lowLegBackProxySkinBtwAllLFT_grp_pointConstraint1.crp"
		;
connectAttr "lowLegBackProxySkinBtwAllLFT_grp.rpt" "lowLegBackProxySkinBtwAllLFT_grp_pointConstraint1.crt"
		;
connectAttr "lowLegBackProxySkinLFT_jnt.t" "lowLegBackProxySkinBtwAllLFT_grp_pointConstraint1.tg[0].tt"
		;
connectAttr "lowLegBackProxySkinLFT_jnt.rp" "lowLegBackProxySkinBtwAllLFT_grp_pointConstraint1.tg[0].trp"
		;
connectAttr "lowLegBackProxySkinLFT_jnt.rpt" "lowLegBackProxySkinBtwAllLFT_grp_pointConstraint1.tg[0].trt"
		;
connectAttr "lowLegBackProxySkinLFT_jnt.pm" "lowLegBackProxySkinBtwAllLFT_grp_pointConstraint1.tg[0].tpm"
		;
connectAttr "lowLegBackProxySkinBtwAllLFT_grp_pointConstraint1.w0" "lowLegBackProxySkinBtwAllLFT_grp_pointConstraint1.tg[0].tw"
		;
connectAttr "upLegBackProxySkinLFT_jnt.s" "upLegRbnDtl1BackProxySkinLFT_jnt.is";
connectAttr "upLegBackProxySkinLFT_jnt.s" "upLegRbnDtl2BackProxySkinLFT_jnt.is";
connectAttr "upLegBackProxySkinLFT_jnt.s" "upLegRbnDtl3BackProxySkinLFT_jnt.is";
connectAttr "upLegBackProxySkinLFT_jnt.s" "upLegRbnDtl4BackProxySkinLFT_jnt.is";
connectAttr "upLegBackProxySkinLFT_jnt.s" "upLegRbnDtl5BackProxySkinLFT_jnt.is";
connectAttr "midLegBackProxySkinBtwIkLFT_jnt.s" "midLegBackProxySkinBtwEndIkLFT_jnt.is"
		;
connectAttr "midLegBackProxySkinBtwEndIkLFT_jnt.tx" "midLegBackProxySkinBtwEndLFT_eff.tx"
		;
connectAttr "midLegBackProxySkinBtwEndIkLFT_jnt.ty" "midLegBackProxySkinBtwEndLFT_eff.ty"
		;
connectAttr "midLegBackProxySkinBtwEndIkLFT_jnt.tz" "midLegBackProxySkinBtwEndLFT_eff.tz"
		;
connectAttr "midLegBackProxySkinBtwAimLFT_jnt_aimConstraint1.crx" "midLegBackProxySkinBtwAimLFT_jnt.rx"
		;
connectAttr "midLegBackProxySkinBtwAimLFT_jnt_aimConstraint1.cry" "midLegBackProxySkinBtwAimLFT_jnt.ry"
		;
connectAttr "midLegBackProxySkinBtwAimLFT_jnt_aimConstraint1.crz" "midLegBackProxySkinBtwAimLFT_jnt.rz"
		;
connectAttr "midLegBackProxySkinBtwAimLFT_jnt.s" "midLegBackProxySkinBtwAimEndLFT_jnt.is"
		;
connectAttr "midLegBackProxySkinBtwAimLFT_jnt.pim" "midLegBackProxySkinBtwAimLFT_jnt_aimConstraint1.cpim"
		;
connectAttr "midLegBackProxySkinBtwAimLFT_jnt.t" "midLegBackProxySkinBtwAimLFT_jnt_aimConstraint1.ct"
		;
connectAttr "midLegBackProxySkinBtwAimLFT_jnt.rp" "midLegBackProxySkinBtwAimLFT_jnt_aimConstraint1.crp"
		;
connectAttr "midLegBackProxySkinBtwAimLFT_jnt.rpt" "midLegBackProxySkinBtwAimLFT_jnt_aimConstraint1.crt"
		;
connectAttr "midLegBackProxySkinBtwAimLFT_jnt.ro" "midLegBackProxySkinBtwAimLFT_jnt_aimConstraint1.cro"
		;
connectAttr "midLegBackProxySkinBtwAimLFT_jnt.jo" "midLegBackProxySkinBtwAimLFT_jnt_aimConstraint1.cjo"
		;
connectAttr "midLegBackProxySkinBtwAimLFT_jnt.is" "midLegBackProxySkinBtwAimLFT_jnt_aimConstraint1.is"
		;
connectAttr "midLegRbnDtl1BackProxySkinLFT_jnt.t" "midLegBackProxySkinBtwAimLFT_jnt_aimConstraint1.tg[0].tt"
		;
connectAttr "midLegRbnDtl1BackProxySkinLFT_jnt.rp" "midLegBackProxySkinBtwAimLFT_jnt_aimConstraint1.tg[0].trp"
		;
connectAttr "midLegRbnDtl1BackProxySkinLFT_jnt.rpt" "midLegBackProxySkinBtwAimLFT_jnt_aimConstraint1.tg[0].trt"
		;
connectAttr "midLegRbnDtl1BackProxySkinLFT_jnt.pm" "midLegBackProxySkinBtwAimLFT_jnt_aimConstraint1.tg[0].tpm"
		;
connectAttr "midLegBackProxySkinBtwAimLFT_jnt_aimConstraint1.w0" "midLegBackProxySkinBtwAimLFT_jnt_aimConstraint1.tg[0].tw"
		;
connectAttr "midLegBackProxySkinBtwUpLFT_grp.wm" "midLegBackProxySkinBtwAimLFT_jnt_aimConstraint1.wum"
		;
connectAttr "midLegBackProxySkinBtwVoidLFT_jnt.s" "midLegBackProxySkinBtwVoidEndLFT_jnt.is"
		;
connectAttr "midLegBackProxySkinBtwIkLFT_jnt.msg" "midLegBackProxySkinBtwLFT_ikHndl.hsj"
		;
connectAttr "midLegBackProxySkinBtwEndLFT_eff.hp" "midLegBackProxySkinBtwLFT_ikHndl.hee"
		;
connectAttr "ikRPsolver.msg" "midLegBackProxySkinBtwLFT_ikHndl.hsv";
connectAttr "midLegBackProxySkinBtwLFT_ikHndl_pointConstraint1.ctx" "midLegBackProxySkinBtwLFT_ikHndl.tx"
		;
connectAttr "midLegBackProxySkinBtwLFT_ikHndl_pointConstraint1.cty" "midLegBackProxySkinBtwLFT_ikHndl.ty"
		;
connectAttr "midLegBackProxySkinBtwLFT_ikHndl_pointConstraint1.ctz" "midLegBackProxySkinBtwLFT_ikHndl.tz"
		;
connectAttr "midLegBackProxySkinBtwLFT_ikHndl.pim" "midLegBackProxySkinBtwLFT_ikHndl_pointConstraint1.cpim"
		;
connectAttr "midLegBackProxySkinBtwLFT_ikHndl.rp" "midLegBackProxySkinBtwLFT_ikHndl_pointConstraint1.crp"
		;
connectAttr "midLegBackProxySkinBtwLFT_ikHndl.rpt" "midLegBackProxySkinBtwLFT_ikHndl_pointConstraint1.crt"
		;
connectAttr "midLegBackProxySkinBtwAimEndLFT_jnt.t" "midLegBackProxySkinBtwLFT_ikHndl_pointConstraint1.tg[0].tt"
		;
connectAttr "midLegBackProxySkinBtwAimEndLFT_jnt.rp" "midLegBackProxySkinBtwLFT_ikHndl_pointConstraint1.tg[0].trp"
		;
connectAttr "midLegBackProxySkinBtwAimEndLFT_jnt.rpt" "midLegBackProxySkinBtwLFT_ikHndl_pointConstraint1.tg[0].trt"
		;
connectAttr "midLegBackProxySkinBtwAimEndLFT_jnt.pm" "midLegBackProxySkinBtwLFT_ikHndl_pointConstraint1.tg[0].tpm"
		;
connectAttr "midLegBackProxySkinBtwLFT_ikHndl_pointConstraint1.w0" "midLegBackProxySkinBtwLFT_ikHndl_pointConstraint1.tg[0].tw"
		;
connectAttr "midLegBackProxySkinBtwVoidEndLFT_jnt.t" "midLegBackProxySkinBtwLFT_ikHndl_pointConstraint1.tg[1].tt"
		;
connectAttr "midLegBackProxySkinBtwVoidEndLFT_jnt.rp" "midLegBackProxySkinBtwLFT_ikHndl_pointConstraint1.tg[1].trp"
		;
connectAttr "midLegBackProxySkinBtwVoidEndLFT_jnt.rpt" "midLegBackProxySkinBtwLFT_ikHndl_pointConstraint1.tg[1].trt"
		;
connectAttr "midLegBackProxySkinBtwVoidEndLFT_jnt.pm" "midLegBackProxySkinBtwLFT_ikHndl_pointConstraint1.tg[1].tpm"
		;
connectAttr "midLegBackProxySkinBtwLFT_ikHndl_pointConstraint1.w1" "midLegBackProxySkinBtwLFT_ikHndl_pointConstraint1.tg[1].tw"
		;
connectAttr "midLegBackProxySkinBtwLFT_jnt.aimBtw" "midLegBackProxySkinBtwLFT_ikHndl_pointConstraint1.w0"
		;
connectAttr "midLegBackProxySkinDriveVoidLFT_adl.ox" "midLegBackProxySkinBtwLFT_ikHndl_pointConstraint1.w1"
		;
connectAttr "midLegBackProxySkinBtwLFT_jnt_parentConstraint1.ctx" "midLegBackProxySkinBtwLFT_jnt.tx"
		 -l on;
connectAttr "midLegBackProxySkinBtwLFT_jnt_parentConstraint1.cty" "midLegBackProxySkinBtwLFT_jnt.ty"
		 -l on;
connectAttr "midLegBackProxySkinBtwLFT_jnt_parentConstraint1.ctz" "midLegBackProxySkinBtwLFT_jnt.tz"
		 -l on;
connectAttr "midLegBackProxySkinBtwLFT_jnt_parentConstraint1.crx" "midLegBackProxySkinBtwLFT_jnt.rx"
		 -l on;
connectAttr "midLegBackProxySkinBtwLFT_jnt_parentConstraint1.cry" "midLegBackProxySkinBtwLFT_jnt.ry"
		 -l on;
connectAttr "midLegBackProxySkinBtwLFT_jnt_parentConstraint1.crz" "midLegBackProxySkinBtwLFT_jnt.rz"
		 -l on;
connectAttr "midLegBackProxySkinDoubleScaleALFT_adl.o" "midLegBackProxySkinBtwLFT_jnt.sx"
		 -l on;
connectAttr "midLegBackProxySkinDoubleScaleBLFT_adl.o" "midLegBackProxySkinBtwLFT_jnt.sz"
		 -l on;
connectAttr "midLegBackProxySkinBtwLFT_jnt.s" "midLegBackProxySkinBtwEndLFT_jnt.is"
		;
connectAttr "midLegBackProxySkinBtwLFT_jnt.ro" "midLegBackProxySkinBtwLFT_jnt_parentConstraint1.cro"
		;
connectAttr "midLegBackProxySkinBtwLFT_jnt.pim" "midLegBackProxySkinBtwLFT_jnt_parentConstraint1.cpim"
		;
connectAttr "midLegBackProxySkinBtwLFT_jnt.rp" "midLegBackProxySkinBtwLFT_jnt_parentConstraint1.crp"
		;
connectAttr "midLegBackProxySkinBtwLFT_jnt.rpt" "midLegBackProxySkinBtwLFT_jnt_parentConstraint1.crt"
		;
connectAttr "midLegBackProxySkinBtwLFT_jnt.jo" "midLegBackProxySkinBtwLFT_jnt_parentConstraint1.cjo"
		;
connectAttr "midLegBackProxySkinBtwIkLFT_jnt.t" "midLegBackProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tt"
		;
connectAttr "midLegBackProxySkinBtwIkLFT_jnt.rp" "midLegBackProxySkinBtwLFT_jnt_parentConstraint1.tg[0].trp"
		;
connectAttr "midLegBackProxySkinBtwIkLFT_jnt.rpt" "midLegBackProxySkinBtwLFT_jnt_parentConstraint1.tg[0].trt"
		;
connectAttr "midLegBackProxySkinBtwIkLFT_jnt.r" "midLegBackProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tr"
		;
connectAttr "midLegBackProxySkinBtwIkLFT_jnt.ro" "midLegBackProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tro"
		;
connectAttr "midLegBackProxySkinBtwIkLFT_jnt.s" "midLegBackProxySkinBtwLFT_jnt_parentConstraint1.tg[0].ts"
		;
connectAttr "midLegBackProxySkinBtwIkLFT_jnt.pm" "midLegBackProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tpm"
		;
connectAttr "midLegBackProxySkinBtwIkLFT_jnt.jo" "midLegBackProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tjo"
		;
connectAttr "midLegBackProxySkinBtwIkLFT_jnt.ssc" "midLegBackProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tsc"
		;
connectAttr "midLegBackProxySkinBtwIkLFT_jnt.is" "midLegBackProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tis"
		;
connectAttr "midLegBackProxySkinBtwLFT_jnt_parentConstraint1.w0" "midLegBackProxySkinBtwLFT_jnt_parentConstraint1.tg[0].tw"
		;
connectAttr "clav1HipProxySkinLFT_jnt.s" "clav1ScaHipProxySkinLFT_jnt.is";
connectAttr "pelvisProxySkin_jnt.s" "clav1HipProxySkinRGT_jnt.is";
connectAttr "clav1HipProxySkinRGT_jnt.s" "clav2HipProxySkinRGT_jnt.is";
connectAttr "clav2HipProxySkinRGT_jnt.s" "upLegBackProxySkinRGT_jnt.is";
connectAttr "upLegBackProxySkinRGT_jnt.s" "midLegBackProxySkinRGT_jnt.is";
connectAttr "midLegBackProxySkinRGT_jnt.s" "lowLegBackProxySkinRGT_jnt.is";
connectAttr "lowLegBackProxySkinRGT_jnt.s" "ankleBackProxySkinRGT_jnt.is";
connectAttr "ankleBackProxySkinRGT_jnt.s" "ballBackProxySkinRGT_jnt.is";
connectAttr "ballBackProxySkinRGT_jnt.s" "toeBackProxySkinRGT_jnt.is";
connectAttr "ballBackProxySkinBtwAllRGT_grp_pointConstraint1.ctx" "ballBackProxySkinBtwAllRGT_grp.tx"
		;
connectAttr "ballBackProxySkinBtwAllRGT_grp_pointConstraint1.cty" "ballBackProxySkinBtwAllRGT_grp.ty"
		;
connectAttr "ballBackProxySkinBtwAllRGT_grp_pointConstraint1.ctz" "ballBackProxySkinBtwAllRGT_grp.tz"
		;
connectAttr "ballBackProxySkinBtwIkRGT_jnt.s" "ballBackProxySkinBtwEndIkRGT_jnt.is"
		;
connectAttr "ballBackProxySkinBtwEndIkRGT_jnt.tx" "ballBackProxySkinBtwEndRGT_eff.tx"
		;
connectAttr "ballBackProxySkinBtwEndIkRGT_jnt.ty" "ballBackProxySkinBtwEndRGT_eff.ty"
		;
connectAttr "ballBackProxySkinBtwEndIkRGT_jnt.tz" "ballBackProxySkinBtwEndRGT_eff.tz"
		;
connectAttr "ballBackProxySkinBtwAimRGT_jnt_aimConstraint1.crx" "ballBackProxySkinBtwAimRGT_jnt.rx"
		;
connectAttr "ballBackProxySkinBtwAimRGT_jnt_aimConstraint1.cry" "ballBackProxySkinBtwAimRGT_jnt.ry"
		;
connectAttr "ballBackProxySkinBtwAimRGT_jnt_aimConstraint1.crz" "ballBackProxySkinBtwAimRGT_jnt.rz"
		;
connectAttr "ballBackProxySkinBtwAimRGT_jnt.s" "ballBackProxySkinBtwAimEndRGT_jnt.is"
		;
connectAttr "ballBackProxySkinBtwAimRGT_jnt.pim" "ballBackProxySkinBtwAimRGT_jnt_aimConstraint1.cpim"
		;
connectAttr "ballBackProxySkinBtwAimRGT_jnt.t" "ballBackProxySkinBtwAimRGT_jnt_aimConstraint1.ct"
		;
connectAttr "ballBackProxySkinBtwAimRGT_jnt.rp" "ballBackProxySkinBtwAimRGT_jnt_aimConstraint1.crp"
		;
connectAttr "ballBackProxySkinBtwAimRGT_jnt.rpt" "ballBackProxySkinBtwAimRGT_jnt_aimConstraint1.crt"
		;
connectAttr "ballBackProxySkinBtwAimRGT_jnt.ro" "ballBackProxySkinBtwAimRGT_jnt_aimConstraint1.cro"
		;
connectAttr "ballBackProxySkinBtwAimRGT_jnt.jo" "ballBackProxySkinBtwAimRGT_jnt_aimConstraint1.cjo"
		;
connectAttr "ballBackProxySkinBtwAimRGT_jnt.is" "ballBackProxySkinBtwAimRGT_jnt_aimConstraint1.is"
		;
connectAttr "toeBackProxySkinRGT_jnt.t" "ballBackProxySkinBtwAimRGT_jnt_aimConstraint1.tg[0].tt"
		;
connectAttr "toeBackProxySkinRGT_jnt.rp" "ballBackProxySkinBtwAimRGT_jnt_aimConstraint1.tg[0].trp"
		;
connectAttr "toeBackProxySkinRGT_jnt.rpt" "ballBackProxySkinBtwAimRGT_jnt_aimConstraint1.tg[0].trt"
		;
connectAttr "toeBackProxySkinRGT_jnt.pm" "ballBackProxySkinBtwAimRGT_jnt_aimConstraint1.tg[0].tpm"
		;
connectAttr "ballBackProxySkinBtwAimRGT_jnt_aimConstraint1.w0" "ballBackProxySkinBtwAimRGT_jnt_aimConstraint1.tg[0].tw"
		;
connectAttr "ballBackProxySkinBtwUpRGT_grp.wm" "ballBackProxySkinBtwAimRGT_jnt_aimConstraint1.wum"
		;
connectAttr "ballBackProxySkinBtwVoidRGT_jnt.s" "ballBackProxySkinBtwVoidEndRGT_jnt.is"
		;
connectAttr "ballBackProxySkinBtwIkRGT_jnt.msg" "ballBackProxySkinBtwRGT_ikHndl.hsj"
		;
connectAttr "ballBackProxySkinBtwEndRGT_eff.hp" "ballBackProxySkinBtwRGT_ikHndl.hee"
		;
connectAttr "ikRPsolver.msg" "ballBackProxySkinBtwRGT_ikHndl.hsv";
connectAttr "ballBackProxySkinBtwRGT_ikHndl_pointConstraint1.ctx" "ballBackProxySkinBtwRGT_ikHndl.tx"
		;
connectAttr "ballBackProxySkinBtwRGT_ikHndl_pointConstraint1.cty" "ballBackProxySkinBtwRGT_ikHndl.ty"
		;
connectAttr "ballBackProxySkinBtwRGT_ikHndl_pointConstraint1.ctz" "ballBackProxySkinBtwRGT_ikHndl.tz"
		;
connectAttr "ballBackProxySkinBtwRGT_ikHndl.pim" "ballBackProxySkinBtwRGT_ikHndl_pointConstraint1.cpim"
		;
connectAttr "ballBackProxySkinBtwRGT_ikHndl.rp" "ballBackProxySkinBtwRGT_ikHndl_pointConstraint1.crp"
		;
connectAttr "ballBackProxySkinBtwRGT_ikHndl.rpt" "ballBackProxySkinBtwRGT_ikHndl_pointConstraint1.crt"
		;
connectAttr "ballBackProxySkinBtwAimEndRGT_jnt.t" "ballBackProxySkinBtwRGT_ikHndl_pointConstraint1.tg[0].tt"
		;
connectAttr "ballBackProxySkinBtwAimEndRGT_jnt.rp" "ballBackProxySkinBtwRGT_ikHndl_pointConstraint1.tg[0].trp"
		;
connectAttr "ballBackProxySkinBtwAimEndRGT_jnt.rpt" "ballBackProxySkinBtwRGT_ikHndl_pointConstraint1.tg[0].trt"
		;
connectAttr "ballBackProxySkinBtwAimEndRGT_jnt.pm" "ballBackProxySkinBtwRGT_ikHndl_pointConstraint1.tg[0].tpm"
		;
connectAttr "ballBackProxySkinBtwRGT_ikHndl_pointConstraint1.w0" "ballBackProxySkinBtwRGT_ikHndl_pointConstraint1.tg[0].tw"
		;
connectAttr "ballBackProxySkinBtwVoidEndRGT_jnt.t" "ballBackProxySkinBtwRGT_ikHndl_pointConstraint1.tg[1].tt"
		;
connectAttr "ballBackProxySkinBtwVoidEndRGT_jnt.rp" "ballBackProxySkinBtwRGT_ikHndl_pointConstraint1.tg[1].trp"
		;
connectAttr "ballBackProxySkinBtwVoidEndRGT_jnt.rpt" "ballBackProxySkinBtwRGT_ikHndl_pointConstraint1.tg[1].trt"
		;
connectAttr "ballBackProxySkinBtwVoidEndRGT_jnt.pm" "ballBackProxySkinBtwRGT_ikHndl_pointConstraint1.tg[1].tpm"
		;
connectAttr "ballBackProxySkinBtwRGT_ikHndl_pointConstraint1.w1" "ballBackProxySkinBtwRGT_ikHndl_pointConstraint1.tg[1].tw"
		;
connectAttr "ballBackProxySkinBtwRGT_jnt.aimBtw" "ballBackProxySkinBtwRGT_ikHndl_pointConstraint1.w0"
		;
connectAttr "ballBackProxySkinDriveVoidRGT_adl.ox" "ballBackProxySkinBtwRGT_ikHndl_pointConstraint1.w1"
		;
connectAttr "ballBackProxySkinBtwRGT_jnt_parentConstraint1.ctx" "ballBackProxySkinBtwRGT_jnt.tx"
		 -l on;
connectAttr "ballBackProxySkinBtwRGT_jnt_parentConstraint1.cty" "ballBackProxySkinBtwRGT_jnt.ty"
		 -l on;
connectAttr "ballBackProxySkinBtwRGT_jnt_parentConstraint1.ctz" "ballBackProxySkinBtwRGT_jnt.tz"
		 -l on;
connectAttr "ballBackProxySkinBtwRGT_jnt_parentConstraint1.crx" "ballBackProxySkinBtwRGT_jnt.rx"
		 -l on;
connectAttr "ballBackProxySkinBtwRGT_jnt_parentConstraint1.cry" "ballBackProxySkinBtwRGT_jnt.ry"
		 -l on;
connectAttr "ballBackProxySkinBtwRGT_jnt_parentConstraint1.crz" "ballBackProxySkinBtwRGT_jnt.rz"
		 -l on;
connectAttr "ballBackProxySkinDoubleScaleARGT_adl.o" "ballBackProxySkinBtwRGT_jnt.sx"
		 -l on;
connectAttr "ballBackProxySkinDoubleScaleBRGT_adl.o" "ballBackProxySkinBtwRGT_jnt.sz"
		 -l on;
connectAttr "ballBackProxySkinBtwRGT_jnt.s" "ballBackProxySkinBtwEndRGT_jnt.is";
connectAttr "ballBackProxySkinBtwRGT_jnt.ro" "ballBackProxySkinBtwRGT_jnt_parentConstraint1.cro"
		;
connectAttr "ballBackProxySkinBtwRGT_jnt.pim" "ballBackProxySkinBtwRGT_jnt_parentConstraint1.cpim"
		;
connectAttr "ballBackProxySkinBtwRGT_jnt.rp" "ballBackProxySkinBtwRGT_jnt_parentConstraint1.crp"
		;
connectAttr "ballBackProxySkinBtwRGT_jnt.rpt" "ballBackProxySkinBtwRGT_jnt_parentConstraint1.crt"
		;
connectAttr "ballBackProxySkinBtwRGT_jnt.jo" "ballBackProxySkinBtwRGT_jnt_parentConstraint1.cjo"
		;
connectAttr "ballBackProxySkinBtwIkRGT_jnt.t" "ballBackProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tt"
		;
connectAttr "ballBackProxySkinBtwIkRGT_jnt.rp" "ballBackProxySkinBtwRGT_jnt_parentConstraint1.tg[0].trp"
		;
connectAttr "ballBackProxySkinBtwIkRGT_jnt.rpt" "ballBackProxySkinBtwRGT_jnt_parentConstraint1.tg[0].trt"
		;
connectAttr "ballBackProxySkinBtwIkRGT_jnt.r" "ballBackProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tr"
		;
connectAttr "ballBackProxySkinBtwIkRGT_jnt.ro" "ballBackProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tro"
		;
connectAttr "ballBackProxySkinBtwIkRGT_jnt.s" "ballBackProxySkinBtwRGT_jnt_parentConstraint1.tg[0].ts"
		;
connectAttr "ballBackProxySkinBtwIkRGT_jnt.pm" "ballBackProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tpm"
		;
connectAttr "ballBackProxySkinBtwIkRGT_jnt.jo" "ballBackProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tjo"
		;
connectAttr "ballBackProxySkinBtwIkRGT_jnt.ssc" "ballBackProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tsc"
		;
connectAttr "ballBackProxySkinBtwIkRGT_jnt.is" "ballBackProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tis"
		;
connectAttr "ballBackProxySkinBtwRGT_jnt_parentConstraint1.w0" "ballBackProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tw"
		;
connectAttr "ballBackProxySkinBtwAllRGT_grp.pim" "ballBackProxySkinBtwAllRGT_grp_pointConstraint1.cpim"
		;
connectAttr "ballBackProxySkinBtwAllRGT_grp.rp" "ballBackProxySkinBtwAllRGT_grp_pointConstraint1.crp"
		;
connectAttr "ballBackProxySkinBtwAllRGT_grp.rpt" "ballBackProxySkinBtwAllRGT_grp_pointConstraint1.crt"
		;
connectAttr "ballBackProxySkinRGT_jnt.t" "ballBackProxySkinBtwAllRGT_grp_pointConstraint1.tg[0].tt"
		;
connectAttr "ballBackProxySkinRGT_jnt.rp" "ballBackProxySkinBtwAllRGT_grp_pointConstraint1.tg[0].trp"
		;
connectAttr "ballBackProxySkinRGT_jnt.rpt" "ballBackProxySkinBtwAllRGT_grp_pointConstraint1.tg[0].trt"
		;
connectAttr "ballBackProxySkinRGT_jnt.pm" "ballBackProxySkinBtwAllRGT_grp_pointConstraint1.tg[0].tpm"
		;
connectAttr "ballBackProxySkinBtwAllRGT_grp_pointConstraint1.w0" "ballBackProxySkinBtwAllRGT_grp_pointConstraint1.tg[0].tw"
		;
connectAttr "lowLegBackProxySkinRGT_jnt.s" "lowLegRbnDtl1BackProxySkinRGT_jnt.is"
		;
connectAttr "lowLegBackProxySkinRGT_jnt.s" "lowLegRbnDtl2BackProxySkinRGT_jnt.is"
		;
connectAttr "lowLegBackProxySkinRGT_jnt.s" "lowLegRbnDtl3BackProxySkinRGT_jnt.is"
		;
connectAttr "lowLegBackProxySkinRGT_jnt.s" "lowLegRbnDtl4BackProxySkinRGT_jnt.is"
		;
connectAttr "lowLegBackProxySkinRGT_jnt.s" "lowLegRbnDtl5BackProxySkinRGT_jnt.is"
		;
connectAttr "ankleBackProxySkinBtwAllRGT_grp_pointConstraint1.ctx" "ankleBackProxySkinBtwAllRGT_grp.tx"
		;
connectAttr "ankleBackProxySkinBtwAllRGT_grp_pointConstraint1.cty" "ankleBackProxySkinBtwAllRGT_grp.ty"
		;
connectAttr "ankleBackProxySkinBtwAllRGT_grp_pointConstraint1.ctz" "ankleBackProxySkinBtwAllRGT_grp.tz"
		;
connectAttr "ankleBackProxySkinBtwIkRGT_jnt.s" "ankleBackProxySkinBtwEndIkRGT_jnt.is"
		;
connectAttr "ankleBackProxySkinBtwEndIkRGT_jnt.tx" "ankleBackProxySkinBtwEndRGT_eff.tx"
		;
connectAttr "ankleBackProxySkinBtwEndIkRGT_jnt.ty" "ankleBackProxySkinBtwEndRGT_eff.ty"
		;
connectAttr "ankleBackProxySkinBtwEndIkRGT_jnt.tz" "ankleBackProxySkinBtwEndRGT_eff.tz"
		;
connectAttr "ankleBackProxySkinBtwAimRGT_jnt_aimConstraint1.crx" "ankleBackProxySkinBtwAimRGT_jnt.rx"
		;
connectAttr "ankleBackProxySkinBtwAimRGT_jnt_aimConstraint1.cry" "ankleBackProxySkinBtwAimRGT_jnt.ry"
		;
connectAttr "ankleBackProxySkinBtwAimRGT_jnt_aimConstraint1.crz" "ankleBackProxySkinBtwAimRGT_jnt.rz"
		;
connectAttr "ankleBackProxySkinBtwAimRGT_jnt.s" "ankleBackProxySkinBtwAimEndRGT_jnt.is"
		;
connectAttr "ankleBackProxySkinBtwAimRGT_jnt.pim" "ankleBackProxySkinBtwAimRGT_jnt_aimConstraint1.cpim"
		;
connectAttr "ankleBackProxySkinBtwAimRGT_jnt.t" "ankleBackProxySkinBtwAimRGT_jnt_aimConstraint1.ct"
		;
connectAttr "ankleBackProxySkinBtwAimRGT_jnt.rp" "ankleBackProxySkinBtwAimRGT_jnt_aimConstraint1.crp"
		;
connectAttr "ankleBackProxySkinBtwAimRGT_jnt.rpt" "ankleBackProxySkinBtwAimRGT_jnt_aimConstraint1.crt"
		;
connectAttr "ankleBackProxySkinBtwAimRGT_jnt.ro" "ankleBackProxySkinBtwAimRGT_jnt_aimConstraint1.cro"
		;
connectAttr "ankleBackProxySkinBtwAimRGT_jnt.jo" "ankleBackProxySkinBtwAimRGT_jnt_aimConstraint1.cjo"
		;
connectAttr "ankleBackProxySkinBtwAimRGT_jnt.is" "ankleBackProxySkinBtwAimRGT_jnt_aimConstraint1.is"
		;
connectAttr "ballBackProxySkinRGT_jnt.t" "ankleBackProxySkinBtwAimRGT_jnt_aimConstraint1.tg[0].tt"
		;
connectAttr "ballBackProxySkinRGT_jnt.rp" "ankleBackProxySkinBtwAimRGT_jnt_aimConstraint1.tg[0].trp"
		;
connectAttr "ballBackProxySkinRGT_jnt.rpt" "ankleBackProxySkinBtwAimRGT_jnt_aimConstraint1.tg[0].trt"
		;
connectAttr "ballBackProxySkinRGT_jnt.pm" "ankleBackProxySkinBtwAimRGT_jnt_aimConstraint1.tg[0].tpm"
		;
connectAttr "ankleBackProxySkinBtwAimRGT_jnt_aimConstraint1.w0" "ankleBackProxySkinBtwAimRGT_jnt_aimConstraint1.tg[0].tw"
		;
connectAttr "ankleBackProxySkinBtwUpRGT_grp.wm" "ankleBackProxySkinBtwAimRGT_jnt_aimConstraint1.wum"
		;
connectAttr "ankleBackProxySkinBtwVoidRGT_jnt.s" "ankleBackProxySkinBtwVoidEndRGT_jnt.is"
		;
connectAttr "ankleBackProxySkinBtwIkRGT_jnt.msg" "ankleBackProxySkinBtwRGT_ikHndl.hsj"
		;
connectAttr "ankleBackProxySkinBtwEndRGT_eff.hp" "ankleBackProxySkinBtwRGT_ikHndl.hee"
		;
connectAttr "ikRPsolver.msg" "ankleBackProxySkinBtwRGT_ikHndl.hsv";
connectAttr "ankleBackProxySkinBtwRGT_ikHndl_pointConstraint1.ctx" "ankleBackProxySkinBtwRGT_ikHndl.tx"
		;
connectAttr "ankleBackProxySkinBtwRGT_ikHndl_pointConstraint1.cty" "ankleBackProxySkinBtwRGT_ikHndl.ty"
		;
connectAttr "ankleBackProxySkinBtwRGT_ikHndl_pointConstraint1.ctz" "ankleBackProxySkinBtwRGT_ikHndl.tz"
		;
connectAttr "ankleBackProxySkinBtwRGT_ikHndl.pim" "ankleBackProxySkinBtwRGT_ikHndl_pointConstraint1.cpim"
		;
connectAttr "ankleBackProxySkinBtwRGT_ikHndl.rp" "ankleBackProxySkinBtwRGT_ikHndl_pointConstraint1.crp"
		;
connectAttr "ankleBackProxySkinBtwRGT_ikHndl.rpt" "ankleBackProxySkinBtwRGT_ikHndl_pointConstraint1.crt"
		;
connectAttr "ankleBackProxySkinBtwAimEndRGT_jnt.t" "ankleBackProxySkinBtwRGT_ikHndl_pointConstraint1.tg[0].tt"
		;
connectAttr "ankleBackProxySkinBtwAimEndRGT_jnt.rp" "ankleBackProxySkinBtwRGT_ikHndl_pointConstraint1.tg[0].trp"
		;
connectAttr "ankleBackProxySkinBtwAimEndRGT_jnt.rpt" "ankleBackProxySkinBtwRGT_ikHndl_pointConstraint1.tg[0].trt"
		;
connectAttr "ankleBackProxySkinBtwAimEndRGT_jnt.pm" "ankleBackProxySkinBtwRGT_ikHndl_pointConstraint1.tg[0].tpm"
		;
connectAttr "ankleBackProxySkinBtwRGT_ikHndl_pointConstraint1.w0" "ankleBackProxySkinBtwRGT_ikHndl_pointConstraint1.tg[0].tw"
		;
connectAttr "ankleBackProxySkinBtwVoidEndRGT_jnt.t" "ankleBackProxySkinBtwRGT_ikHndl_pointConstraint1.tg[1].tt"
		;
connectAttr "ankleBackProxySkinBtwVoidEndRGT_jnt.rp" "ankleBackProxySkinBtwRGT_ikHndl_pointConstraint1.tg[1].trp"
		;
connectAttr "ankleBackProxySkinBtwVoidEndRGT_jnt.rpt" "ankleBackProxySkinBtwRGT_ikHndl_pointConstraint1.tg[1].trt"
		;
connectAttr "ankleBackProxySkinBtwVoidEndRGT_jnt.pm" "ankleBackProxySkinBtwRGT_ikHndl_pointConstraint1.tg[1].tpm"
		;
connectAttr "ankleBackProxySkinBtwRGT_ikHndl_pointConstraint1.w1" "ankleBackProxySkinBtwRGT_ikHndl_pointConstraint1.tg[1].tw"
		;
connectAttr "ankleBackProxySkinBtwRGT_jnt.aimBtw" "ankleBackProxySkinBtwRGT_ikHndl_pointConstraint1.w0"
		;
connectAttr "ankleBackProxySkinDriveVoidRGT_adl.ox" "ankleBackProxySkinBtwRGT_ikHndl_pointConstraint1.w1"
		;
connectAttr "ankleBackProxySkinBtwRGT_jnt_parentConstraint1.ctx" "ankleBackProxySkinBtwRGT_jnt.tx"
		 -l on;
connectAttr "ankleBackProxySkinBtwRGT_jnt_parentConstraint1.cty" "ankleBackProxySkinBtwRGT_jnt.ty"
		 -l on;
connectAttr "ankleBackProxySkinBtwRGT_jnt_parentConstraint1.ctz" "ankleBackProxySkinBtwRGT_jnt.tz"
		 -l on;
connectAttr "ankleBackProxySkinBtwRGT_jnt_parentConstraint1.crx" "ankleBackProxySkinBtwRGT_jnt.rx"
		 -l on;
connectAttr "ankleBackProxySkinBtwRGT_jnt_parentConstraint1.cry" "ankleBackProxySkinBtwRGT_jnt.ry"
		 -l on;
connectAttr "ankleBackProxySkinBtwRGT_jnt_parentConstraint1.crz" "ankleBackProxySkinBtwRGT_jnt.rz"
		 -l on;
connectAttr "ankleBackProxySkinDoubleScaleARGT_adl.o" "ankleBackProxySkinBtwRGT_jnt.sx"
		 -l on;
connectAttr "ankleBackProxySkinDoubleScaleBRGT_adl.o" "ankleBackProxySkinBtwRGT_jnt.sz"
		 -l on;
connectAttr "ankleBackProxySkinBtwRGT_jnt.s" "ankleBackProxySkinBtwEndRGT_jnt.is"
		;
connectAttr "ankleBackProxySkinBtwRGT_jnt.ro" "ankleBackProxySkinBtwRGT_jnt_parentConstraint1.cro"
		;
connectAttr "ankleBackProxySkinBtwRGT_jnt.pim" "ankleBackProxySkinBtwRGT_jnt_parentConstraint1.cpim"
		;
connectAttr "ankleBackProxySkinBtwRGT_jnt.rp" "ankleBackProxySkinBtwRGT_jnt_parentConstraint1.crp"
		;
connectAttr "ankleBackProxySkinBtwRGT_jnt.rpt" "ankleBackProxySkinBtwRGT_jnt_parentConstraint1.crt"
		;
connectAttr "ankleBackProxySkinBtwRGT_jnt.jo" "ankleBackProxySkinBtwRGT_jnt_parentConstraint1.cjo"
		;
connectAttr "ankleBackProxySkinBtwIkRGT_jnt.t" "ankleBackProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tt"
		;
connectAttr "ankleBackProxySkinBtwIkRGT_jnt.rp" "ankleBackProxySkinBtwRGT_jnt_parentConstraint1.tg[0].trp"
		;
connectAttr "ankleBackProxySkinBtwIkRGT_jnt.rpt" "ankleBackProxySkinBtwRGT_jnt_parentConstraint1.tg[0].trt"
		;
connectAttr "ankleBackProxySkinBtwIkRGT_jnt.r" "ankleBackProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tr"
		;
connectAttr "ankleBackProxySkinBtwIkRGT_jnt.ro" "ankleBackProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tro"
		;
connectAttr "ankleBackProxySkinBtwIkRGT_jnt.s" "ankleBackProxySkinBtwRGT_jnt_parentConstraint1.tg[0].ts"
		;
connectAttr "ankleBackProxySkinBtwIkRGT_jnt.pm" "ankleBackProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tpm"
		;
connectAttr "ankleBackProxySkinBtwIkRGT_jnt.jo" "ankleBackProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tjo"
		;
connectAttr "ankleBackProxySkinBtwIkRGT_jnt.ssc" "ankleBackProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tsc"
		;
connectAttr "ankleBackProxySkinBtwIkRGT_jnt.is" "ankleBackProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tis"
		;
connectAttr "ankleBackProxySkinBtwRGT_jnt_parentConstraint1.w0" "ankleBackProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tw"
		;
connectAttr "ankleBackProxySkinBtwAllRGT_grp.pim" "ankleBackProxySkinBtwAllRGT_grp_pointConstraint1.cpim"
		;
connectAttr "ankleBackProxySkinBtwAllRGT_grp.rp" "ankleBackProxySkinBtwAllRGT_grp_pointConstraint1.crp"
		;
connectAttr "ankleBackProxySkinBtwAllRGT_grp.rpt" "ankleBackProxySkinBtwAllRGT_grp_pointConstraint1.crt"
		;
connectAttr "ankleBackProxySkinRGT_jnt.t" "ankleBackProxySkinBtwAllRGT_grp_pointConstraint1.tg[0].tt"
		;
connectAttr "ankleBackProxySkinRGT_jnt.rp" "ankleBackProxySkinBtwAllRGT_grp_pointConstraint1.tg[0].trp"
		;
connectAttr "ankleBackProxySkinRGT_jnt.rpt" "ankleBackProxySkinBtwAllRGT_grp_pointConstraint1.tg[0].trt"
		;
connectAttr "ankleBackProxySkinRGT_jnt.pm" "ankleBackProxySkinBtwAllRGT_grp_pointConstraint1.tg[0].tpm"
		;
connectAttr "ankleBackProxySkinBtwAllRGT_grp_pointConstraint1.w0" "ankleBackProxySkinBtwAllRGT_grp_pointConstraint1.tg[0].tw"
		;
connectAttr "midLegBackProxySkinRGT_jnt.s" "midLegRbnDtl1BackProxySkinRGT_jnt.is"
		;
connectAttr "midLegBackProxySkinRGT_jnt.s" "midLegRbnDtl2BackProxySkinRGT_jnt.is"
		;
connectAttr "midLegBackProxySkinRGT_jnt.s" "midLegRbnDtl3BackProxySkinRGT_jnt.is"
		;
connectAttr "midLegBackProxySkinRGT_jnt.s" "midLegRbnDtl4BackProxySkinRGT_jnt.is"
		;
connectAttr "midLegBackProxySkinRGT_jnt.s" "midLegRbnDtl5BackProxySkinRGT_jnt.is"
		;
connectAttr "lowLegBackProxySkinBtwAllRGT_grp_pointConstraint1.ctx" "lowLegBackProxySkinBtwAllRGT_grp.tx"
		;
connectAttr "lowLegBackProxySkinBtwAllRGT_grp_pointConstraint1.cty" "lowLegBackProxySkinBtwAllRGT_grp.ty"
		;
connectAttr "lowLegBackProxySkinBtwAllRGT_grp_pointConstraint1.ctz" "lowLegBackProxySkinBtwAllRGT_grp.tz"
		;
connectAttr "lowLegBackProxySkinBtwIkRGT_jnt.s" "lowLegBackProxySkinBtwEndIkRGT_jnt.is"
		;
connectAttr "lowLegBackProxySkinBtwEndIkRGT_jnt.tx" "lowLegBackProxySkinBtwEndRGT_eff.tx"
		;
connectAttr "lowLegBackProxySkinBtwEndIkRGT_jnt.ty" "lowLegBackProxySkinBtwEndRGT_eff.ty"
		;
connectAttr "lowLegBackProxySkinBtwEndIkRGT_jnt.tz" "lowLegBackProxySkinBtwEndRGT_eff.tz"
		;
connectAttr "lowLegBackProxySkinBtwAimRGT_jnt_aimConstraint1.crx" "lowLegBackProxySkinBtwAimRGT_jnt.rx"
		;
connectAttr "lowLegBackProxySkinBtwAimRGT_jnt_aimConstraint1.cry" "lowLegBackProxySkinBtwAimRGT_jnt.ry"
		;
connectAttr "lowLegBackProxySkinBtwAimRGT_jnt_aimConstraint1.crz" "lowLegBackProxySkinBtwAimRGT_jnt.rz"
		;
connectAttr "lowLegBackProxySkinBtwAimRGT_jnt.s" "lowLegBackProxySkinBtwAimEndRGT_jnt.is"
		;
connectAttr "lowLegBackProxySkinBtwAimRGT_jnt.pim" "lowLegBackProxySkinBtwAimRGT_jnt_aimConstraint1.cpim"
		;
connectAttr "lowLegBackProxySkinBtwAimRGT_jnt.t" "lowLegBackProxySkinBtwAimRGT_jnt_aimConstraint1.ct"
		;
connectAttr "lowLegBackProxySkinBtwAimRGT_jnt.rp" "lowLegBackProxySkinBtwAimRGT_jnt_aimConstraint1.crp"
		;
connectAttr "lowLegBackProxySkinBtwAimRGT_jnt.rpt" "lowLegBackProxySkinBtwAimRGT_jnt_aimConstraint1.crt"
		;
connectAttr "lowLegBackProxySkinBtwAimRGT_jnt.ro" "lowLegBackProxySkinBtwAimRGT_jnt_aimConstraint1.cro"
		;
connectAttr "lowLegBackProxySkinBtwAimRGT_jnt.jo" "lowLegBackProxySkinBtwAimRGT_jnt_aimConstraint1.cjo"
		;
connectAttr "lowLegBackProxySkinBtwAimRGT_jnt.is" "lowLegBackProxySkinBtwAimRGT_jnt_aimConstraint1.is"
		;
connectAttr "lowLegRbnDtl1BackProxySkinRGT_jnt.t" "lowLegBackProxySkinBtwAimRGT_jnt_aimConstraint1.tg[0].tt"
		;
connectAttr "lowLegRbnDtl1BackProxySkinRGT_jnt.rp" "lowLegBackProxySkinBtwAimRGT_jnt_aimConstraint1.tg[0].trp"
		;
connectAttr "lowLegRbnDtl1BackProxySkinRGT_jnt.rpt" "lowLegBackProxySkinBtwAimRGT_jnt_aimConstraint1.tg[0].trt"
		;
connectAttr "lowLegRbnDtl1BackProxySkinRGT_jnt.pm" "lowLegBackProxySkinBtwAimRGT_jnt_aimConstraint1.tg[0].tpm"
		;
connectAttr "lowLegBackProxySkinBtwAimRGT_jnt_aimConstraint1.w0" "lowLegBackProxySkinBtwAimRGT_jnt_aimConstraint1.tg[0].tw"
		;
connectAttr "lowLegBackProxySkinBtwUpRGT_grp.wm" "lowLegBackProxySkinBtwAimRGT_jnt_aimConstraint1.wum"
		;
connectAttr "lowLegBackProxySkinBtwVoidRGT_jnt.s" "lowLegBackProxySkinBtwVoidEndRGT_jnt.is"
		;
connectAttr "lowLegBackProxySkinBtwIkRGT_jnt.msg" "lowLegBackProxySkinBtwRGT_ikHndl.hsj"
		;
connectAttr "lowLegBackProxySkinBtwEndRGT_eff.hp" "lowLegBackProxySkinBtwRGT_ikHndl.hee"
		;
connectAttr "ikRPsolver.msg" "lowLegBackProxySkinBtwRGT_ikHndl.hsv";
connectAttr "lowLegBackProxySkinBtwRGT_ikHndl_pointConstraint1.ctx" "lowLegBackProxySkinBtwRGT_ikHndl.tx"
		;
connectAttr "lowLegBackProxySkinBtwRGT_ikHndl_pointConstraint1.cty" "lowLegBackProxySkinBtwRGT_ikHndl.ty"
		;
connectAttr "lowLegBackProxySkinBtwRGT_ikHndl_pointConstraint1.ctz" "lowLegBackProxySkinBtwRGT_ikHndl.tz"
		;
connectAttr "lowLegBackProxySkinBtwRGT_ikHndl.pim" "lowLegBackProxySkinBtwRGT_ikHndl_pointConstraint1.cpim"
		;
connectAttr "lowLegBackProxySkinBtwRGT_ikHndl.rp" "lowLegBackProxySkinBtwRGT_ikHndl_pointConstraint1.crp"
		;
connectAttr "lowLegBackProxySkinBtwRGT_ikHndl.rpt" "lowLegBackProxySkinBtwRGT_ikHndl_pointConstraint1.crt"
		;
connectAttr "lowLegBackProxySkinBtwAimEndRGT_jnt.t" "lowLegBackProxySkinBtwRGT_ikHndl_pointConstraint1.tg[0].tt"
		;
connectAttr "lowLegBackProxySkinBtwAimEndRGT_jnt.rp" "lowLegBackProxySkinBtwRGT_ikHndl_pointConstraint1.tg[0].trp"
		;
connectAttr "lowLegBackProxySkinBtwAimEndRGT_jnt.rpt" "lowLegBackProxySkinBtwRGT_ikHndl_pointConstraint1.tg[0].trt"
		;
connectAttr "lowLegBackProxySkinBtwAimEndRGT_jnt.pm" "lowLegBackProxySkinBtwRGT_ikHndl_pointConstraint1.tg[0].tpm"
		;
connectAttr "lowLegBackProxySkinBtwRGT_ikHndl_pointConstraint1.w0" "lowLegBackProxySkinBtwRGT_ikHndl_pointConstraint1.tg[0].tw"
		;
connectAttr "lowLegBackProxySkinBtwVoidEndRGT_jnt.t" "lowLegBackProxySkinBtwRGT_ikHndl_pointConstraint1.tg[1].tt"
		;
connectAttr "lowLegBackProxySkinBtwVoidEndRGT_jnt.rp" "lowLegBackProxySkinBtwRGT_ikHndl_pointConstraint1.tg[1].trp"
		;
connectAttr "lowLegBackProxySkinBtwVoidEndRGT_jnt.rpt" "lowLegBackProxySkinBtwRGT_ikHndl_pointConstraint1.tg[1].trt"
		;
connectAttr "lowLegBackProxySkinBtwVoidEndRGT_jnt.pm" "lowLegBackProxySkinBtwRGT_ikHndl_pointConstraint1.tg[1].tpm"
		;
connectAttr "lowLegBackProxySkinBtwRGT_ikHndl_pointConstraint1.w1" "lowLegBackProxySkinBtwRGT_ikHndl_pointConstraint1.tg[1].tw"
		;
connectAttr "lowLegBackProxySkinBtwRGT_jnt.aimBtw" "lowLegBackProxySkinBtwRGT_ikHndl_pointConstraint1.w0"
		;
connectAttr "lowLegBackProxySkinDriveVoidRGT_adl.ox" "lowLegBackProxySkinBtwRGT_ikHndl_pointConstraint1.w1"
		;
connectAttr "lowLegBackProxySkinBtwRGT_jnt_parentConstraint1.ctx" "lowLegBackProxySkinBtwRGT_jnt.tx"
		 -l on;
connectAttr "lowLegBackProxySkinBtwRGT_jnt_parentConstraint1.cty" "lowLegBackProxySkinBtwRGT_jnt.ty"
		 -l on;
connectAttr "lowLegBackProxySkinBtwRGT_jnt_parentConstraint1.ctz" "lowLegBackProxySkinBtwRGT_jnt.tz"
		 -l on;
connectAttr "lowLegBackProxySkinBtwRGT_jnt_parentConstraint1.crx" "lowLegBackProxySkinBtwRGT_jnt.rx"
		 -l on;
connectAttr "lowLegBackProxySkinBtwRGT_jnt_parentConstraint1.cry" "lowLegBackProxySkinBtwRGT_jnt.ry"
		 -l on;
connectAttr "lowLegBackProxySkinBtwRGT_jnt_parentConstraint1.crz" "lowLegBackProxySkinBtwRGT_jnt.rz"
		 -l on;
connectAttr "lowLegBackProxySkinDoubleScaleARGT_adl.o" "lowLegBackProxySkinBtwRGT_jnt.sx"
		 -l on;
connectAttr "lowLegBackProxySkinDoubleScaleBRGT_adl.o" "lowLegBackProxySkinBtwRGT_jnt.sz"
		 -l on;
connectAttr "lowLegBackProxySkinBtwRGT_jnt.s" "lowLegBackProxySkinBtwEndRGT_jnt.is"
		;
connectAttr "lowLegBackProxySkinBtwRGT_jnt.ro" "lowLegBackProxySkinBtwRGT_jnt_parentConstraint1.cro"
		;
connectAttr "lowLegBackProxySkinBtwRGT_jnt.pim" "lowLegBackProxySkinBtwRGT_jnt_parentConstraint1.cpim"
		;
connectAttr "lowLegBackProxySkinBtwRGT_jnt.rp" "lowLegBackProxySkinBtwRGT_jnt_parentConstraint1.crp"
		;
connectAttr "lowLegBackProxySkinBtwRGT_jnt.rpt" "lowLegBackProxySkinBtwRGT_jnt_parentConstraint1.crt"
		;
connectAttr "lowLegBackProxySkinBtwRGT_jnt.jo" "lowLegBackProxySkinBtwRGT_jnt_parentConstraint1.cjo"
		;
connectAttr "lowLegBackProxySkinBtwIkRGT_jnt.t" "lowLegBackProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tt"
		;
connectAttr "lowLegBackProxySkinBtwIkRGT_jnt.rp" "lowLegBackProxySkinBtwRGT_jnt_parentConstraint1.tg[0].trp"
		;
connectAttr "lowLegBackProxySkinBtwIkRGT_jnt.rpt" "lowLegBackProxySkinBtwRGT_jnt_parentConstraint1.tg[0].trt"
		;
connectAttr "lowLegBackProxySkinBtwIkRGT_jnt.r" "lowLegBackProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tr"
		;
connectAttr "lowLegBackProxySkinBtwIkRGT_jnt.ro" "lowLegBackProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tro"
		;
connectAttr "lowLegBackProxySkinBtwIkRGT_jnt.s" "lowLegBackProxySkinBtwRGT_jnt_parentConstraint1.tg[0].ts"
		;
connectAttr "lowLegBackProxySkinBtwIkRGT_jnt.pm" "lowLegBackProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tpm"
		;
connectAttr "lowLegBackProxySkinBtwIkRGT_jnt.jo" "lowLegBackProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tjo"
		;
connectAttr "lowLegBackProxySkinBtwIkRGT_jnt.ssc" "lowLegBackProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tsc"
		;
connectAttr "lowLegBackProxySkinBtwIkRGT_jnt.is" "lowLegBackProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tis"
		;
connectAttr "lowLegBackProxySkinBtwRGT_jnt_parentConstraint1.w0" "lowLegBackProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tw"
		;
connectAttr "lowLegBackProxySkinBtwAllRGT_grp.pim" "lowLegBackProxySkinBtwAllRGT_grp_pointConstraint1.cpim"
		;
connectAttr "lowLegBackProxySkinBtwAllRGT_grp.rp" "lowLegBackProxySkinBtwAllRGT_grp_pointConstraint1.crp"
		;
connectAttr "lowLegBackProxySkinBtwAllRGT_grp.rpt" "lowLegBackProxySkinBtwAllRGT_grp_pointConstraint1.crt"
		;
connectAttr "lowLegBackProxySkinRGT_jnt.t" "lowLegBackProxySkinBtwAllRGT_grp_pointConstraint1.tg[0].tt"
		;
connectAttr "lowLegBackProxySkinRGT_jnt.rp" "lowLegBackProxySkinBtwAllRGT_grp_pointConstraint1.tg[0].trp"
		;
connectAttr "lowLegBackProxySkinRGT_jnt.rpt" "lowLegBackProxySkinBtwAllRGT_grp_pointConstraint1.tg[0].trt"
		;
connectAttr "lowLegBackProxySkinRGT_jnt.pm" "lowLegBackProxySkinBtwAllRGT_grp_pointConstraint1.tg[0].tpm"
		;
connectAttr "lowLegBackProxySkinBtwAllRGT_grp_pointConstraint1.w0" "lowLegBackProxySkinBtwAllRGT_grp_pointConstraint1.tg[0].tw"
		;
connectAttr "upLegBackProxySkinRGT_jnt.s" "upLegRbnDtl1BackProxySkinRGT_jnt.is";
connectAttr "upLegBackProxySkinRGT_jnt.s" "upLegRbnDtl2BackProxySkinRGT_jnt.is";
connectAttr "upLegBackProxySkinRGT_jnt.s" "upLegRbnDtl3BackProxySkinRGT_jnt.is";
connectAttr "upLegBackProxySkinRGT_jnt.s" "upLegRbnDtl4BackProxySkinRGT_jnt.is";
connectAttr "upLegBackProxySkinRGT_jnt.s" "upLegRbnDtl5BackProxySkinRGT_jnt.is";
connectAttr "midLegBackProxySkinBtwIkRGT_jnt.s" "midLegBackProxySkinBtwEndIkRGT_jnt.is"
		;
connectAttr "midLegBackProxySkinBtwEndIkRGT_jnt.tx" "midLegBackProxySkinBtwEndRGT_eff.tx"
		;
connectAttr "midLegBackProxySkinBtwEndIkRGT_jnt.ty" "midLegBackProxySkinBtwEndRGT_eff.ty"
		;
connectAttr "midLegBackProxySkinBtwEndIkRGT_jnt.tz" "midLegBackProxySkinBtwEndRGT_eff.tz"
		;
connectAttr "midLegBackProxySkinBtwAimRGT_jnt_aimConstraint1.crx" "midLegBackProxySkinBtwAimRGT_jnt.rx"
		;
connectAttr "midLegBackProxySkinBtwAimRGT_jnt_aimConstraint1.cry" "midLegBackProxySkinBtwAimRGT_jnt.ry"
		;
connectAttr "midLegBackProxySkinBtwAimRGT_jnt_aimConstraint1.crz" "midLegBackProxySkinBtwAimRGT_jnt.rz"
		;
connectAttr "midLegBackProxySkinBtwAimRGT_jnt.s" "midLegBackProxySkinBtwAimEndRGT_jnt.is"
		;
connectAttr "midLegBackProxySkinBtwAimRGT_jnt.pim" "midLegBackProxySkinBtwAimRGT_jnt_aimConstraint1.cpim"
		;
connectAttr "midLegBackProxySkinBtwAimRGT_jnt.t" "midLegBackProxySkinBtwAimRGT_jnt_aimConstraint1.ct"
		;
connectAttr "midLegBackProxySkinBtwAimRGT_jnt.rp" "midLegBackProxySkinBtwAimRGT_jnt_aimConstraint1.crp"
		;
connectAttr "midLegBackProxySkinBtwAimRGT_jnt.rpt" "midLegBackProxySkinBtwAimRGT_jnt_aimConstraint1.crt"
		;
connectAttr "midLegBackProxySkinBtwAimRGT_jnt.ro" "midLegBackProxySkinBtwAimRGT_jnt_aimConstraint1.cro"
		;
connectAttr "midLegBackProxySkinBtwAimRGT_jnt.jo" "midLegBackProxySkinBtwAimRGT_jnt_aimConstraint1.cjo"
		;
connectAttr "midLegBackProxySkinBtwAimRGT_jnt.is" "midLegBackProxySkinBtwAimRGT_jnt_aimConstraint1.is"
		;
connectAttr "midLegRbnDtl1BackProxySkinRGT_jnt.t" "midLegBackProxySkinBtwAimRGT_jnt_aimConstraint1.tg[0].tt"
		;
connectAttr "midLegRbnDtl1BackProxySkinRGT_jnt.rp" "midLegBackProxySkinBtwAimRGT_jnt_aimConstraint1.tg[0].trp"
		;
connectAttr "midLegRbnDtl1BackProxySkinRGT_jnt.rpt" "midLegBackProxySkinBtwAimRGT_jnt_aimConstraint1.tg[0].trt"
		;
connectAttr "midLegRbnDtl1BackProxySkinRGT_jnt.pm" "midLegBackProxySkinBtwAimRGT_jnt_aimConstraint1.tg[0].tpm"
		;
connectAttr "midLegBackProxySkinBtwAimRGT_jnt_aimConstraint1.w0" "midLegBackProxySkinBtwAimRGT_jnt_aimConstraint1.tg[0].tw"
		;
connectAttr "midLegBackProxySkinBtwUpRGT_grp.wm" "midLegBackProxySkinBtwAimRGT_jnt_aimConstraint1.wum"
		;
connectAttr "midLegBackProxySkinBtwVoidRGT_jnt.s" "midLegBackProxySkinBtwVoidEndRGT_jnt.is"
		;
connectAttr "midLegBackProxySkinBtwIkRGT_jnt.msg" "midLegBackProxySkinBtwRGT_ikHndl.hsj"
		;
connectAttr "midLegBackProxySkinBtwEndRGT_eff.hp" "midLegBackProxySkinBtwRGT_ikHndl.hee"
		;
connectAttr "ikRPsolver.msg" "midLegBackProxySkinBtwRGT_ikHndl.hsv";
connectAttr "midLegBackProxySkinBtwRGT_ikHndl_pointConstraint1.ctx" "midLegBackProxySkinBtwRGT_ikHndl.tx"
		;
connectAttr "midLegBackProxySkinBtwRGT_ikHndl_pointConstraint1.cty" "midLegBackProxySkinBtwRGT_ikHndl.ty"
		;
connectAttr "midLegBackProxySkinBtwRGT_ikHndl_pointConstraint1.ctz" "midLegBackProxySkinBtwRGT_ikHndl.tz"
		;
connectAttr "midLegBackProxySkinBtwRGT_ikHndl.pim" "midLegBackProxySkinBtwRGT_ikHndl_pointConstraint1.cpim"
		;
connectAttr "midLegBackProxySkinBtwRGT_ikHndl.rp" "midLegBackProxySkinBtwRGT_ikHndl_pointConstraint1.crp"
		;
connectAttr "midLegBackProxySkinBtwRGT_ikHndl.rpt" "midLegBackProxySkinBtwRGT_ikHndl_pointConstraint1.crt"
		;
connectAttr "midLegBackProxySkinBtwAimEndRGT_jnt.t" "midLegBackProxySkinBtwRGT_ikHndl_pointConstraint1.tg[0].tt"
		;
connectAttr "midLegBackProxySkinBtwAimEndRGT_jnt.rp" "midLegBackProxySkinBtwRGT_ikHndl_pointConstraint1.tg[0].trp"
		;
connectAttr "midLegBackProxySkinBtwAimEndRGT_jnt.rpt" "midLegBackProxySkinBtwRGT_ikHndl_pointConstraint1.tg[0].trt"
		;
connectAttr "midLegBackProxySkinBtwAimEndRGT_jnt.pm" "midLegBackProxySkinBtwRGT_ikHndl_pointConstraint1.tg[0].tpm"
		;
connectAttr "midLegBackProxySkinBtwRGT_ikHndl_pointConstraint1.w0" "midLegBackProxySkinBtwRGT_ikHndl_pointConstraint1.tg[0].tw"
		;
connectAttr "midLegBackProxySkinBtwVoidEndRGT_jnt.t" "midLegBackProxySkinBtwRGT_ikHndl_pointConstraint1.tg[1].tt"
		;
connectAttr "midLegBackProxySkinBtwVoidEndRGT_jnt.rp" "midLegBackProxySkinBtwRGT_ikHndl_pointConstraint1.tg[1].trp"
		;
connectAttr "midLegBackProxySkinBtwVoidEndRGT_jnt.rpt" "midLegBackProxySkinBtwRGT_ikHndl_pointConstraint1.tg[1].trt"
		;
connectAttr "midLegBackProxySkinBtwVoidEndRGT_jnt.pm" "midLegBackProxySkinBtwRGT_ikHndl_pointConstraint1.tg[1].tpm"
		;
connectAttr "midLegBackProxySkinBtwRGT_ikHndl_pointConstraint1.w1" "midLegBackProxySkinBtwRGT_ikHndl_pointConstraint1.tg[1].tw"
		;
connectAttr "midLegBackProxySkinBtwRGT_jnt.aimBtw" "midLegBackProxySkinBtwRGT_ikHndl_pointConstraint1.w0"
		;
connectAttr "midLegBackProxySkinDriveVoidRGT_adl.ox" "midLegBackProxySkinBtwRGT_ikHndl_pointConstraint1.w1"
		;
connectAttr "midLegBackProxySkinBtwRGT_jnt_parentConstraint1.ctx" "midLegBackProxySkinBtwRGT_jnt.tx"
		 -l on;
connectAttr "midLegBackProxySkinBtwRGT_jnt_parentConstraint1.cty" "midLegBackProxySkinBtwRGT_jnt.ty"
		 -l on;
connectAttr "midLegBackProxySkinBtwRGT_jnt_parentConstraint1.ctz" "midLegBackProxySkinBtwRGT_jnt.tz"
		 -l on;
connectAttr "midLegBackProxySkinBtwRGT_jnt_parentConstraint1.crx" "midLegBackProxySkinBtwRGT_jnt.rx"
		 -l on;
connectAttr "midLegBackProxySkinBtwRGT_jnt_parentConstraint1.cry" "midLegBackProxySkinBtwRGT_jnt.ry"
		 -l on;
connectAttr "midLegBackProxySkinBtwRGT_jnt_parentConstraint1.crz" "midLegBackProxySkinBtwRGT_jnt.rz"
		 -l on;
connectAttr "midLegBackProxySkinDoubleScaleARGT_adl.o" "midLegBackProxySkinBtwRGT_jnt.sx"
		 -l on;
connectAttr "midLegBackProxySkinDoubleScaleBRGT_adl.o" "midLegBackProxySkinBtwRGT_jnt.sz"
		 -l on;
connectAttr "midLegBackProxySkinBtwRGT_jnt.s" "midLegBackProxySkinBtwEndRGT_jnt.is"
		;
connectAttr "midLegBackProxySkinBtwRGT_jnt.ro" "midLegBackProxySkinBtwRGT_jnt_parentConstraint1.cro"
		;
connectAttr "midLegBackProxySkinBtwRGT_jnt.pim" "midLegBackProxySkinBtwRGT_jnt_parentConstraint1.cpim"
		;
connectAttr "midLegBackProxySkinBtwRGT_jnt.rp" "midLegBackProxySkinBtwRGT_jnt_parentConstraint1.crp"
		;
connectAttr "midLegBackProxySkinBtwRGT_jnt.rpt" "midLegBackProxySkinBtwRGT_jnt_parentConstraint1.crt"
		;
connectAttr "midLegBackProxySkinBtwRGT_jnt.jo" "midLegBackProxySkinBtwRGT_jnt_parentConstraint1.cjo"
		;
connectAttr "midLegBackProxySkinBtwIkRGT_jnt.t" "midLegBackProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tt"
		;
connectAttr "midLegBackProxySkinBtwIkRGT_jnt.rp" "midLegBackProxySkinBtwRGT_jnt_parentConstraint1.tg[0].trp"
		;
connectAttr "midLegBackProxySkinBtwIkRGT_jnt.rpt" "midLegBackProxySkinBtwRGT_jnt_parentConstraint1.tg[0].trt"
		;
connectAttr "midLegBackProxySkinBtwIkRGT_jnt.r" "midLegBackProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tr"
		;
connectAttr "midLegBackProxySkinBtwIkRGT_jnt.ro" "midLegBackProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tro"
		;
connectAttr "midLegBackProxySkinBtwIkRGT_jnt.s" "midLegBackProxySkinBtwRGT_jnt_parentConstraint1.tg[0].ts"
		;
connectAttr "midLegBackProxySkinBtwIkRGT_jnt.pm" "midLegBackProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tpm"
		;
connectAttr "midLegBackProxySkinBtwIkRGT_jnt.jo" "midLegBackProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tjo"
		;
connectAttr "midLegBackProxySkinBtwIkRGT_jnt.ssc" "midLegBackProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tsc"
		;
connectAttr "midLegBackProxySkinBtwIkRGT_jnt.is" "midLegBackProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tis"
		;
connectAttr "midLegBackProxySkinBtwRGT_jnt_parentConstraint1.w0" "midLegBackProxySkinBtwRGT_jnt_parentConstraint1.tg[0].tw"
		;
connectAttr "clav1HipProxySkinRGT_jnt.s" "clav1ScaHipProxySkinRGT_jnt.is";
connectAttr "pelvisProxySkin_jnt.s" "tail1ProxySkin_jnt.is";
connectAttr "tail1ProxySkin_jnt.s" "tail2ProxySkin_jnt.is";
connectAttr "tail2ProxySkin_jnt.s" "tail3ProxySkin_jnt.is";
connectAttr "tail3ProxySkin_jnt.s" "tail4ProxySkin_jnt.is";
connectAttr "tail4ProxySkin_jnt.s" "tail5ProxySkin_jnt.is";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert2SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "blinn1SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "blinn2SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "blinn3SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "blinn4SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "blinn5SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert3SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert2SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "blinn1SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "blinn2SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "blinn3SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "blinn4SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "blinn5SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert3SG.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr ":TurtleDefaultBakeLayer.idx" ":TurtleBakeLayerManager.bli[0]";
connectAttr ":TurtleRenderOptions.msg" ":TurtleDefaultBakeLayer.rset";
connectAttr "unitConversion1.o" "midLegFrontProxySkinMulScalePosLFT_mdv.i1x";
connectAttr "unitConversion3.o" "midLegFrontProxySkinMulScalePosLFT_mdv.i1y";
connectAttr "unitConversion5.o" "midLegFrontProxySkinMulScalePosLFT_mdv.i1z";
connectAttr "unitConversion2.o" "midLegFrontProxySkinMulScaleNegLFT_mdv.i1x";
connectAttr "unitConversion4.o" "midLegFrontProxySkinMulScaleNegLFT_mdv.i1y";
connectAttr "unitConversion6.o" "midLegFrontProxySkinMulScaleNegLFT_mdv.i1z";
connectAttr "midLegFrontProxySkinMulScalePosLFT_mdv.o" "midLegFrontProxySkinClampScalePosLFT_mdv.ip"
		;
connectAttr "midLegFrontProxySkinMulScaleNegLFT_mdv.o" "midLegFrontProxySkinClampScaleNegLFT_mdv.ip"
		;
connectAttr "midLegFrontProxySkinClampScalePosLFT_mdv.op" "midLegFrontProxySkinPlusPosNegScaleLFT_pma.i3[0]"
		;
connectAttr "midLegFrontProxySkinClampScaleNegLFT_mdv.op" "midLegFrontProxySkinPlusPosNegScaleLFT_pma.i3[1]"
		;
connectAttr "midLegFrontProxySkinBtwLFT_jnt.scaleBtw" "midLegFrontProxySkinMulScalePlusLFT_mdv.i2x"
		;
connectAttr "midLegFrontProxySkinBtwLFT_jnt.scaleBtw" "midLegFrontProxySkinMulScalePlusLFT_mdv.i2y"
		;
connectAttr "midLegFrontProxySkinBtwLFT_jnt.scaleBtw" "midLegFrontProxySkinMulScalePlusLFT_mdv.i2z"
		;
connectAttr "midLegFrontProxySkinPlusPosNegScaleLFT_pma.o3" "midLegFrontProxySkinMulScalePlusLFT_mdv.i1"
		;
connectAttr "midLegFrontProxySkinMulScalePlusLFT_mdv.oz" "midLegFrontProxySkinDoubleScaleALFT_adl.i2"
		;
connectAttr "midLegFrontProxySkinMulScalePlusLFT_mdv.ox" "midLegFrontProxySkinDoubleScaleBLFT_adl.i2"
		;
connectAttr "midLegFrontProxySkinBtwLFT_jnt.aimBtw" "midLegFrontProxySkinDriveVoidLFT_adl.ix"
		;
connectAttr "midLegFrontProxySkinBtwIkLFT_jnt.rx" "unitConversion1.i";
connectAttr "midLegFrontProxySkinBtwIkLFT_jnt.rx" "unitConversion2.i";
connectAttr "midLegFrontProxySkinBtwIkLFT_jnt.ry" "unitConversion3.i";
connectAttr "midLegFrontProxySkinBtwIkLFT_jnt.ry" "unitConversion4.i";
connectAttr "midLegFrontProxySkinBtwIkLFT_jnt.rz" "unitConversion5.i";
connectAttr "midLegFrontProxySkinBtwIkLFT_jnt.rz" "unitConversion6.i";
connectAttr "unitConversion7.o" "midLegFrontProxySkinMulScalePosRGT_mdv.i1x";
connectAttr "unitConversion9.o" "midLegFrontProxySkinMulScalePosRGT_mdv.i1y";
connectAttr "unitConversion11.o" "midLegFrontProxySkinMulScalePosRGT_mdv.i1z";
connectAttr "unitConversion8.o" "midLegFrontProxySkinMulScaleNegRGT_mdv.i1x";
connectAttr "unitConversion10.o" "midLegFrontProxySkinMulScaleNegRGT_mdv.i1y";
connectAttr "unitConversion12.o" "midLegFrontProxySkinMulScaleNegRGT_mdv.i1z";
connectAttr "midLegFrontProxySkinMulScalePosRGT_mdv.o" "midLegFrontProxySkinClampScalePosRGT_mdv.ip"
		;
connectAttr "midLegFrontProxySkinMulScaleNegRGT_mdv.o" "midLegFrontProxySkinClampScaleNegRGT_mdv.ip"
		;
connectAttr "midLegFrontProxySkinClampScalePosRGT_mdv.op" "midLegFrontProxySkinPlusPosNegScaleRGT_pma.i3[0]"
		;
connectAttr "midLegFrontProxySkinClampScaleNegRGT_mdv.op" "midLegFrontProxySkinPlusPosNegScaleRGT_pma.i3[1]"
		;
connectAttr "midLegFrontProxySkinBtwRGT_jnt.scaleBtw" "midLegFrontProxySkinMulScalePlusRGT_mdv.i2x"
		;
connectAttr "midLegFrontProxySkinBtwRGT_jnt.scaleBtw" "midLegFrontProxySkinMulScalePlusRGT_mdv.i2y"
		;
connectAttr "midLegFrontProxySkinBtwRGT_jnt.scaleBtw" "midLegFrontProxySkinMulScalePlusRGT_mdv.i2z"
		;
connectAttr "midLegFrontProxySkinPlusPosNegScaleRGT_pma.o3" "midLegFrontProxySkinMulScalePlusRGT_mdv.i1"
		;
connectAttr "midLegFrontProxySkinMulScalePlusRGT_mdv.oz" "midLegFrontProxySkinDoubleScaleARGT_adl.i2"
		;
connectAttr "midLegFrontProxySkinMulScalePlusRGT_mdv.ox" "midLegFrontProxySkinDoubleScaleBRGT_adl.i2"
		;
connectAttr "midLegFrontProxySkinBtwRGT_jnt.aimBtw" "midLegFrontProxySkinDriveVoidRGT_adl.ix"
		;
connectAttr "midLegFrontProxySkinBtwIkRGT_jnt.rx" "unitConversion7.i";
connectAttr "midLegFrontProxySkinBtwIkRGT_jnt.rx" "unitConversion8.i";
connectAttr "midLegFrontProxySkinBtwIkRGT_jnt.ry" "unitConversion9.i";
connectAttr "midLegFrontProxySkinBtwIkRGT_jnt.ry" "unitConversion10.i";
connectAttr "midLegFrontProxySkinBtwIkRGT_jnt.rz" "unitConversion11.i";
connectAttr "midLegFrontProxySkinBtwIkRGT_jnt.rz" "unitConversion12.i";
connectAttr "unitConversion13.o" "midLegBackProxySkinMulScalePosLFT_mdv.i1x";
connectAttr "unitConversion15.o" "midLegBackProxySkinMulScalePosLFT_mdv.i1y";
connectAttr "unitConversion17.o" "midLegBackProxySkinMulScalePosLFT_mdv.i1z";
connectAttr "unitConversion14.o" "midLegBackProxySkinMulScaleNegLFT_mdv.i1x";
connectAttr "unitConversion16.o" "midLegBackProxySkinMulScaleNegLFT_mdv.i1y";
connectAttr "unitConversion18.o" "midLegBackProxySkinMulScaleNegLFT_mdv.i1z";
connectAttr "midLegBackProxySkinMulScalePosLFT_mdv.o" "midLegBackProxySkinClampScalePosLFT_mdv.ip"
		;
connectAttr "midLegBackProxySkinMulScaleNegLFT_mdv.o" "midLegBackProxySkinClampScaleNegLFT_mdv.ip"
		;
connectAttr "midLegBackProxySkinClampScalePosLFT_mdv.op" "midLegBackProxySkinPlusPosNegScaleLFT_pma.i3[0]"
		;
connectAttr "midLegBackProxySkinClampScaleNegLFT_mdv.op" "midLegBackProxySkinPlusPosNegScaleLFT_pma.i3[1]"
		;
connectAttr "midLegBackProxySkinBtwLFT_jnt.scaleBtw" "midLegBackProxySkinMulScalePlusLFT_mdv.i2x"
		;
connectAttr "midLegBackProxySkinBtwLFT_jnt.scaleBtw" "midLegBackProxySkinMulScalePlusLFT_mdv.i2y"
		;
connectAttr "midLegBackProxySkinBtwLFT_jnt.scaleBtw" "midLegBackProxySkinMulScalePlusLFT_mdv.i2z"
		;
connectAttr "midLegBackProxySkinPlusPosNegScaleLFT_pma.o3" "midLegBackProxySkinMulScalePlusLFT_mdv.i1"
		;
connectAttr "midLegBackProxySkinMulScalePlusLFT_mdv.oz" "midLegBackProxySkinDoubleScaleALFT_adl.i2"
		;
connectAttr "midLegBackProxySkinMulScalePlusLFT_mdv.ox" "midLegBackProxySkinDoubleScaleBLFT_adl.i2"
		;
connectAttr "midLegBackProxySkinBtwLFT_jnt.aimBtw" "midLegBackProxySkinDriveVoidLFT_adl.ix"
		;
connectAttr "midLegBackProxySkinBtwIkLFT_jnt.rx" "unitConversion13.i";
connectAttr "midLegBackProxySkinBtwIkLFT_jnt.rx" "unitConversion14.i";
connectAttr "midLegBackProxySkinBtwIkLFT_jnt.ry" "unitConversion15.i";
connectAttr "midLegBackProxySkinBtwIkLFT_jnt.ry" "unitConversion16.i";
connectAttr "midLegBackProxySkinBtwIkLFT_jnt.rz" "unitConversion17.i";
connectAttr "midLegBackProxySkinBtwIkLFT_jnt.rz" "unitConversion18.i";
connectAttr "unitConversion19.o" "midLegBackProxySkinMulScalePosRGT_mdv.i1x";
connectAttr "unitConversion21.o" "midLegBackProxySkinMulScalePosRGT_mdv.i1y";
connectAttr "unitConversion23.o" "midLegBackProxySkinMulScalePosRGT_mdv.i1z";
connectAttr "unitConversion20.o" "midLegBackProxySkinMulScaleNegRGT_mdv.i1x";
connectAttr "unitConversion22.o" "midLegBackProxySkinMulScaleNegRGT_mdv.i1y";
connectAttr "unitConversion24.o" "midLegBackProxySkinMulScaleNegRGT_mdv.i1z";
connectAttr "midLegBackProxySkinMulScalePosRGT_mdv.o" "midLegBackProxySkinClampScalePosRGT_mdv.ip"
		;
connectAttr "midLegBackProxySkinMulScaleNegRGT_mdv.o" "midLegBackProxySkinClampScaleNegRGT_mdv.ip"
		;
connectAttr "midLegBackProxySkinClampScalePosRGT_mdv.op" "midLegBackProxySkinPlusPosNegScaleRGT_pma.i3[0]"
		;
connectAttr "midLegBackProxySkinClampScaleNegRGT_mdv.op" "midLegBackProxySkinPlusPosNegScaleRGT_pma.i3[1]"
		;
connectAttr "midLegBackProxySkinBtwRGT_jnt.scaleBtw" "midLegBackProxySkinMulScalePlusRGT_mdv.i2x"
		;
connectAttr "midLegBackProxySkinBtwRGT_jnt.scaleBtw" "midLegBackProxySkinMulScalePlusRGT_mdv.i2y"
		;
connectAttr "midLegBackProxySkinBtwRGT_jnt.scaleBtw" "midLegBackProxySkinMulScalePlusRGT_mdv.i2z"
		;
connectAttr "midLegBackProxySkinPlusPosNegScaleRGT_pma.o3" "midLegBackProxySkinMulScalePlusRGT_mdv.i1"
		;
connectAttr "midLegBackProxySkinMulScalePlusRGT_mdv.oz" "midLegBackProxySkinDoubleScaleARGT_adl.i2"
		;
connectAttr "midLegBackProxySkinMulScalePlusRGT_mdv.ox" "midLegBackProxySkinDoubleScaleBRGT_adl.i2"
		;
connectAttr "midLegBackProxySkinBtwRGT_jnt.aimBtw" "midLegBackProxySkinDriveVoidRGT_adl.ix"
		;
connectAttr "midLegBackProxySkinBtwIkRGT_jnt.rx" "unitConversion19.i";
connectAttr "midLegBackProxySkinBtwIkRGT_jnt.rx" "unitConversion20.i";
connectAttr "midLegBackProxySkinBtwIkRGT_jnt.ry" "unitConversion21.i";
connectAttr "midLegBackProxySkinBtwIkRGT_jnt.ry" "unitConversion22.i";
connectAttr "midLegBackProxySkinBtwIkRGT_jnt.rz" "unitConversion23.i";
connectAttr "midLegBackProxySkinBtwIkRGT_jnt.rz" "unitConversion24.i";
connectAttr "unitConversion25.o" "lowLegBackProxySkinMulScalePosLFT_mdv.i1x";
connectAttr "unitConversion27.o" "lowLegBackProxySkinMulScalePosLFT_mdv.i1y";
connectAttr "unitConversion29.o" "lowLegBackProxySkinMulScalePosLFT_mdv.i1z";
connectAttr "unitConversion26.o" "lowLegBackProxySkinMulScaleNegLFT_mdv.i1x";
connectAttr "unitConversion28.o" "lowLegBackProxySkinMulScaleNegLFT_mdv.i1y";
connectAttr "unitConversion30.o" "lowLegBackProxySkinMulScaleNegLFT_mdv.i1z";
connectAttr "lowLegBackProxySkinMulScalePosLFT_mdv.o" "lowLegBackProxySkinClampScalePosLFT_mdv.ip"
		;
connectAttr "lowLegBackProxySkinMulScaleNegLFT_mdv.o" "lowLegBackProxySkinClampScaleNegLFT_mdv.ip"
		;
connectAttr "lowLegBackProxySkinClampScalePosLFT_mdv.op" "lowLegBackProxySkinPlusPosNegScaleLFT_pma.i3[0]"
		;
connectAttr "lowLegBackProxySkinClampScaleNegLFT_mdv.op" "lowLegBackProxySkinPlusPosNegScaleLFT_pma.i3[1]"
		;
connectAttr "lowLegBackProxySkinBtwLFT_jnt.scaleBtw" "lowLegBackProxySkinMulScalePlusLFT_mdv.i2x"
		;
connectAttr "lowLegBackProxySkinBtwLFT_jnt.scaleBtw" "lowLegBackProxySkinMulScalePlusLFT_mdv.i2y"
		;
connectAttr "lowLegBackProxySkinBtwLFT_jnt.scaleBtw" "lowLegBackProxySkinMulScalePlusLFT_mdv.i2z"
		;
connectAttr "lowLegBackProxySkinPlusPosNegScaleLFT_pma.o3" "lowLegBackProxySkinMulScalePlusLFT_mdv.i1"
		;
connectAttr "lowLegBackProxySkinMulScalePlusLFT_mdv.oz" "lowLegBackProxySkinDoubleScaleALFT_adl.i2"
		;
connectAttr "lowLegBackProxySkinMulScalePlusLFT_mdv.ox" "lowLegBackProxySkinDoubleScaleBLFT_adl.i2"
		;
connectAttr "lowLegBackProxySkinBtwLFT_jnt.aimBtw" "lowLegBackProxySkinDriveVoidLFT_adl.ix"
		;
connectAttr "lowLegBackProxySkinBtwIkLFT_jnt.rx" "unitConversion25.i";
connectAttr "lowLegBackProxySkinBtwIkLFT_jnt.rx" "unitConversion26.i";
connectAttr "lowLegBackProxySkinBtwIkLFT_jnt.ry" "unitConversion27.i";
connectAttr "lowLegBackProxySkinBtwIkLFT_jnt.ry" "unitConversion28.i";
connectAttr "lowLegBackProxySkinBtwIkLFT_jnt.rz" "unitConversion29.i";
connectAttr "lowLegBackProxySkinBtwIkLFT_jnt.rz" "unitConversion30.i";
connectAttr "unitConversion31.o" "lowLegBackProxySkinMulScalePosRGT_mdv.i1x";
connectAttr "unitConversion33.o" "lowLegBackProxySkinMulScalePosRGT_mdv.i1y";
connectAttr "unitConversion35.o" "lowLegBackProxySkinMulScalePosRGT_mdv.i1z";
connectAttr "unitConversion32.o" "lowLegBackProxySkinMulScaleNegRGT_mdv.i1x";
connectAttr "unitConversion34.o" "lowLegBackProxySkinMulScaleNegRGT_mdv.i1y";
connectAttr "unitConversion36.o" "lowLegBackProxySkinMulScaleNegRGT_mdv.i1z";
connectAttr "lowLegBackProxySkinMulScalePosRGT_mdv.o" "lowLegBackProxySkinClampScalePosRGT_mdv.ip"
		;
connectAttr "lowLegBackProxySkinMulScaleNegRGT_mdv.o" "lowLegBackProxySkinClampScaleNegRGT_mdv.ip"
		;
connectAttr "lowLegBackProxySkinClampScalePosRGT_mdv.op" "lowLegBackProxySkinPlusPosNegScaleRGT_pma.i3[0]"
		;
connectAttr "lowLegBackProxySkinClampScaleNegRGT_mdv.op" "lowLegBackProxySkinPlusPosNegScaleRGT_pma.i3[1]"
		;
connectAttr "lowLegBackProxySkinBtwRGT_jnt.scaleBtw" "lowLegBackProxySkinMulScalePlusRGT_mdv.i2x"
		;
connectAttr "lowLegBackProxySkinBtwRGT_jnt.scaleBtw" "lowLegBackProxySkinMulScalePlusRGT_mdv.i2y"
		;
connectAttr "lowLegBackProxySkinBtwRGT_jnt.scaleBtw" "lowLegBackProxySkinMulScalePlusRGT_mdv.i2z"
		;
connectAttr "lowLegBackProxySkinPlusPosNegScaleRGT_pma.o3" "lowLegBackProxySkinMulScalePlusRGT_mdv.i1"
		;
connectAttr "lowLegBackProxySkinMulScalePlusRGT_mdv.oz" "lowLegBackProxySkinDoubleScaleARGT_adl.i2"
		;
connectAttr "lowLegBackProxySkinMulScalePlusRGT_mdv.ox" "lowLegBackProxySkinDoubleScaleBRGT_adl.i2"
		;
connectAttr "lowLegBackProxySkinBtwRGT_jnt.aimBtw" "lowLegBackProxySkinDriveVoidRGT_adl.ix"
		;
connectAttr "lowLegBackProxySkinBtwIkRGT_jnt.rx" "unitConversion31.i";
connectAttr "lowLegBackProxySkinBtwIkRGT_jnt.rx" "unitConversion32.i";
connectAttr "lowLegBackProxySkinBtwIkRGT_jnt.ry" "unitConversion33.i";
connectAttr "lowLegBackProxySkinBtwIkRGT_jnt.ry" "unitConversion34.i";
connectAttr "lowLegBackProxySkinBtwIkRGT_jnt.rz" "unitConversion35.i";
connectAttr "lowLegBackProxySkinBtwIkRGT_jnt.rz" "unitConversion36.i";
connectAttr "unitConversion37.o" "ankleBackProxySkinMulScalePosLFT_mdv.i1x";
connectAttr "unitConversion39.o" "ankleBackProxySkinMulScalePosLFT_mdv.i1y";
connectAttr "unitConversion41.o" "ankleBackProxySkinMulScalePosLFT_mdv.i1z";
connectAttr "unitConversion38.o" "ankleBackProxySkinMulScaleNegLFT_mdv.i1x";
connectAttr "unitConversion40.o" "ankleBackProxySkinMulScaleNegLFT_mdv.i1y";
connectAttr "unitConversion42.o" "ankleBackProxySkinMulScaleNegLFT_mdv.i1z";
connectAttr "ankleBackProxySkinMulScalePosLFT_mdv.o" "ankleBackProxySkinClampScalePosLFT_mdv.ip"
		;
connectAttr "ankleBackProxySkinMulScaleNegLFT_mdv.o" "ankleBackProxySkinClampScaleNegLFT_mdv.ip"
		;
connectAttr "ankleBackProxySkinClampScalePosLFT_mdv.op" "ankleBackProxySkinPlusPosNegScaleLFT_pma.i3[0]"
		;
connectAttr "ankleBackProxySkinClampScaleNegLFT_mdv.op" "ankleBackProxySkinPlusPosNegScaleLFT_pma.i3[1]"
		;
connectAttr "ankleBackProxySkinBtwLFT_jnt.scaleBtw" "ankleBackProxySkinMulScalePlusLFT_mdv.i2x"
		;
connectAttr "ankleBackProxySkinBtwLFT_jnt.scaleBtw" "ankleBackProxySkinMulScalePlusLFT_mdv.i2y"
		;
connectAttr "ankleBackProxySkinBtwLFT_jnt.scaleBtw" "ankleBackProxySkinMulScalePlusLFT_mdv.i2z"
		;
connectAttr "ankleBackProxySkinPlusPosNegScaleLFT_pma.o3" "ankleBackProxySkinMulScalePlusLFT_mdv.i1"
		;
connectAttr "ankleBackProxySkinMulScalePlusLFT_mdv.oz" "ankleBackProxySkinDoubleScaleALFT_adl.i2"
		;
connectAttr "ankleBackProxySkinMulScalePlusLFT_mdv.ox" "ankleBackProxySkinDoubleScaleBLFT_adl.i2"
		;
connectAttr "ankleBackProxySkinBtwLFT_jnt.aimBtw" "ankleBackProxySkinDriveVoidLFT_adl.ix"
		;
connectAttr "ankleBackProxySkinBtwIkLFT_jnt.rx" "unitConversion37.i";
connectAttr "ankleBackProxySkinBtwIkLFT_jnt.rx" "unitConversion38.i";
connectAttr "ankleBackProxySkinBtwIkLFT_jnt.ry" "unitConversion39.i";
connectAttr "ankleBackProxySkinBtwIkLFT_jnt.ry" "unitConversion40.i";
connectAttr "ankleBackProxySkinBtwIkLFT_jnt.rz" "unitConversion41.i";
connectAttr "ankleBackProxySkinBtwIkLFT_jnt.rz" "unitConversion42.i";
connectAttr "unitConversion43.o" "ankleBackProxySkinMulScalePosRGT_mdv.i1x";
connectAttr "unitConversion45.o" "ankleBackProxySkinMulScalePosRGT_mdv.i1y";
connectAttr "unitConversion47.o" "ankleBackProxySkinMulScalePosRGT_mdv.i1z";
connectAttr "unitConversion44.o" "ankleBackProxySkinMulScaleNegRGT_mdv.i1x";
connectAttr "unitConversion46.o" "ankleBackProxySkinMulScaleNegRGT_mdv.i1y";
connectAttr "unitConversion48.o" "ankleBackProxySkinMulScaleNegRGT_mdv.i1z";
connectAttr "ankleBackProxySkinMulScalePosRGT_mdv.o" "ankleBackProxySkinClampScalePosRGT_mdv.ip"
		;
connectAttr "ankleBackProxySkinMulScaleNegRGT_mdv.o" "ankleBackProxySkinClampScaleNegRGT_mdv.ip"
		;
connectAttr "ankleBackProxySkinClampScalePosRGT_mdv.op" "ankleBackProxySkinPlusPosNegScaleRGT_pma.i3[0]"
		;
connectAttr "ankleBackProxySkinClampScaleNegRGT_mdv.op" "ankleBackProxySkinPlusPosNegScaleRGT_pma.i3[1]"
		;
connectAttr "ankleBackProxySkinBtwRGT_jnt.scaleBtw" "ankleBackProxySkinMulScalePlusRGT_mdv.i2x"
		;
connectAttr "ankleBackProxySkinBtwRGT_jnt.scaleBtw" "ankleBackProxySkinMulScalePlusRGT_mdv.i2y"
		;
connectAttr "ankleBackProxySkinBtwRGT_jnt.scaleBtw" "ankleBackProxySkinMulScalePlusRGT_mdv.i2z"
		;
connectAttr "ankleBackProxySkinPlusPosNegScaleRGT_pma.o3" "ankleBackProxySkinMulScalePlusRGT_mdv.i1"
		;
connectAttr "ankleBackProxySkinMulScalePlusRGT_mdv.oz" "ankleBackProxySkinDoubleScaleARGT_adl.i2"
		;
connectAttr "ankleBackProxySkinMulScalePlusRGT_mdv.ox" "ankleBackProxySkinDoubleScaleBRGT_adl.i2"
		;
connectAttr "ankleBackProxySkinBtwRGT_jnt.aimBtw" "ankleBackProxySkinDriveVoidRGT_adl.ix"
		;
connectAttr "ankleBackProxySkinBtwIkRGT_jnt.rx" "unitConversion43.i";
connectAttr "ankleBackProxySkinBtwIkRGT_jnt.rx" "unitConversion44.i";
connectAttr "ankleBackProxySkinBtwIkRGT_jnt.ry" "unitConversion45.i";
connectAttr "ankleBackProxySkinBtwIkRGT_jnt.ry" "unitConversion46.i";
connectAttr "ankleBackProxySkinBtwIkRGT_jnt.rz" "unitConversion47.i";
connectAttr "ankleBackProxySkinBtwIkRGT_jnt.rz" "unitConversion48.i";
connectAttr "unitConversion49.o" "ballBackProxySkinMulScalePosLFT_mdv.i1x";
connectAttr "unitConversion51.o" "ballBackProxySkinMulScalePosLFT_mdv.i1y";
connectAttr "unitConversion53.o" "ballBackProxySkinMulScalePosLFT_mdv.i1z";
connectAttr "unitConversion50.o" "ballBackProxySkinMulScaleNegLFT_mdv.i1x";
connectAttr "unitConversion52.o" "ballBackProxySkinMulScaleNegLFT_mdv.i1y";
connectAttr "unitConversion54.o" "ballBackProxySkinMulScaleNegLFT_mdv.i1z";
connectAttr "ballBackProxySkinMulScalePosLFT_mdv.o" "ballBackProxySkinClampScalePosLFT_mdv.ip"
		;
connectAttr "ballBackProxySkinMulScaleNegLFT_mdv.o" "ballBackProxySkinClampScaleNegLFT_mdv.ip"
		;
connectAttr "ballBackProxySkinClampScalePosLFT_mdv.op" "ballBackProxySkinPlusPosNegScaleLFT_pma.i3[0]"
		;
connectAttr "ballBackProxySkinClampScaleNegLFT_mdv.op" "ballBackProxySkinPlusPosNegScaleLFT_pma.i3[1]"
		;
connectAttr "ballBackProxySkinBtwLFT_jnt.scaleBtw" "ballBackProxySkinMulScalePlusLFT_mdv.i2x"
		;
connectAttr "ballBackProxySkinBtwLFT_jnt.scaleBtw" "ballBackProxySkinMulScalePlusLFT_mdv.i2y"
		;
connectAttr "ballBackProxySkinBtwLFT_jnt.scaleBtw" "ballBackProxySkinMulScalePlusLFT_mdv.i2z"
		;
connectAttr "ballBackProxySkinPlusPosNegScaleLFT_pma.o3" "ballBackProxySkinMulScalePlusLFT_mdv.i1"
		;
connectAttr "ballBackProxySkinMulScalePlusLFT_mdv.oz" "ballBackProxySkinDoubleScaleALFT_adl.i2"
		;
connectAttr "ballBackProxySkinMulScalePlusLFT_mdv.ox" "ballBackProxySkinDoubleScaleBLFT_adl.i2"
		;
connectAttr "ballBackProxySkinBtwLFT_jnt.aimBtw" "ballBackProxySkinDriveVoidLFT_adl.ix"
		;
connectAttr "ballBackProxySkinBtwIkLFT_jnt.rx" "unitConversion49.i";
connectAttr "ballBackProxySkinBtwIkLFT_jnt.rx" "unitConversion50.i";
connectAttr "ballBackProxySkinBtwIkLFT_jnt.ry" "unitConversion51.i";
connectAttr "ballBackProxySkinBtwIkLFT_jnt.ry" "unitConversion52.i";
connectAttr "ballBackProxySkinBtwIkLFT_jnt.rz" "unitConversion53.i";
connectAttr "ballBackProxySkinBtwIkLFT_jnt.rz" "unitConversion54.i";
connectAttr "unitConversion55.o" "ballBackProxySkinMulScalePosRGT_mdv.i1x";
connectAttr "unitConversion57.o" "ballBackProxySkinMulScalePosRGT_mdv.i1y";
connectAttr "unitConversion59.o" "ballBackProxySkinMulScalePosRGT_mdv.i1z";
connectAttr "unitConversion56.o" "ballBackProxySkinMulScaleNegRGT_mdv.i1x";
connectAttr "unitConversion58.o" "ballBackProxySkinMulScaleNegRGT_mdv.i1y";
connectAttr "unitConversion60.o" "ballBackProxySkinMulScaleNegRGT_mdv.i1z";
connectAttr "ballBackProxySkinMulScalePosRGT_mdv.o" "ballBackProxySkinClampScalePosRGT_mdv.ip"
		;
connectAttr "ballBackProxySkinMulScaleNegRGT_mdv.o" "ballBackProxySkinClampScaleNegRGT_mdv.ip"
		;
connectAttr "ballBackProxySkinClampScalePosRGT_mdv.op" "ballBackProxySkinPlusPosNegScaleRGT_pma.i3[0]"
		;
connectAttr "ballBackProxySkinClampScaleNegRGT_mdv.op" "ballBackProxySkinPlusPosNegScaleRGT_pma.i3[1]"
		;
connectAttr "ballBackProxySkinBtwRGT_jnt.scaleBtw" "ballBackProxySkinMulScalePlusRGT_mdv.i2x"
		;
connectAttr "ballBackProxySkinBtwRGT_jnt.scaleBtw" "ballBackProxySkinMulScalePlusRGT_mdv.i2y"
		;
connectAttr "ballBackProxySkinBtwRGT_jnt.scaleBtw" "ballBackProxySkinMulScalePlusRGT_mdv.i2z"
		;
connectAttr "ballBackProxySkinPlusPosNegScaleRGT_pma.o3" "ballBackProxySkinMulScalePlusRGT_mdv.i1"
		;
connectAttr "ballBackProxySkinMulScalePlusRGT_mdv.oz" "ballBackProxySkinDoubleScaleARGT_adl.i2"
		;
connectAttr "ballBackProxySkinMulScalePlusRGT_mdv.ox" "ballBackProxySkinDoubleScaleBRGT_adl.i2"
		;
connectAttr "ballBackProxySkinBtwRGT_jnt.aimBtw" "ballBackProxySkinDriveVoidRGT_adl.ix"
		;
connectAttr "ballBackProxySkinBtwIkRGT_jnt.rx" "unitConversion55.i";
connectAttr "ballBackProxySkinBtwIkRGT_jnt.rx" "unitConversion56.i";
connectAttr "ballBackProxySkinBtwIkRGT_jnt.ry" "unitConversion57.i";
connectAttr "ballBackProxySkinBtwIkRGT_jnt.ry" "unitConversion58.i";
connectAttr "ballBackProxySkinBtwIkRGT_jnt.rz" "unitConversion59.i";
connectAttr "ballBackProxySkinBtwIkRGT_jnt.rz" "unitConversion60.i";
connectAttr "unitConversion61.o" "ankleFrontProxySkinMulScalePosLFT_mdv.i1x";
connectAttr "unitConversion63.o" "ankleFrontProxySkinMulScalePosLFT_mdv.i1y";
connectAttr "unitConversion65.o" "ankleFrontProxySkinMulScalePosLFT_mdv.i1z";
connectAttr "unitConversion62.o" "ankleFrontProxySkinMulScaleNegLFT_mdv.i1x";
connectAttr "unitConversion64.o" "ankleFrontProxySkinMulScaleNegLFT_mdv.i1y";
connectAttr "unitConversion66.o" "ankleFrontProxySkinMulScaleNegLFT_mdv.i1z";
connectAttr "ankleFrontProxySkinMulScalePosLFT_mdv.o" "ankleFrontProxySkinClampScalePosLFT_mdv.ip"
		;
connectAttr "ankleFrontProxySkinMulScaleNegLFT_mdv.o" "ankleFrontProxySkinClampScaleNegLFT_mdv.ip"
		;
connectAttr "ankleFrontProxySkinClampScalePosLFT_mdv.op" "ankleFrontProxySkinPlusPosNegScaleLFT_pma.i3[0]"
		;
connectAttr "ankleFrontProxySkinClampScaleNegLFT_mdv.op" "ankleFrontProxySkinPlusPosNegScaleLFT_pma.i3[1]"
		;
connectAttr "ankleFrontProxySkinBtwLFT_jnt.scaleBtw" "ankleFrontProxySkinMulScalePlusLFT_mdv.i2x"
		;
connectAttr "ankleFrontProxySkinBtwLFT_jnt.scaleBtw" "ankleFrontProxySkinMulScalePlusLFT_mdv.i2y"
		;
connectAttr "ankleFrontProxySkinBtwLFT_jnt.scaleBtw" "ankleFrontProxySkinMulScalePlusLFT_mdv.i2z"
		;
connectAttr "ankleFrontProxySkinPlusPosNegScaleLFT_pma.o3" "ankleFrontProxySkinMulScalePlusLFT_mdv.i1"
		;
connectAttr "ankleFrontProxySkinMulScalePlusLFT_mdv.oz" "ankleFrontProxySkinDoubleScaleALFT_adl.i2"
		;
connectAttr "ankleFrontProxySkinMulScalePlusLFT_mdv.ox" "ankleFrontProxySkinDoubleScaleBLFT_adl.i2"
		;
connectAttr "ankleFrontProxySkinBtwLFT_jnt.aimBtw" "ankleFrontProxySkinDriveVoidLFT_adl.ix"
		;
connectAttr "ankleFrontProxySkinBtwIkLFT_jnt.rx" "unitConversion61.i";
connectAttr "ankleFrontProxySkinBtwIkLFT_jnt.rx" "unitConversion62.i";
connectAttr "ankleFrontProxySkinBtwIkLFT_jnt.ry" "unitConversion63.i";
connectAttr "ankleFrontProxySkinBtwIkLFT_jnt.ry" "unitConversion64.i";
connectAttr "ankleFrontProxySkinBtwIkLFT_jnt.rz" "unitConversion65.i";
connectAttr "ankleFrontProxySkinBtwIkLFT_jnt.rz" "unitConversion66.i";
connectAttr "unitConversion67.o" "ballFrontProxySkinMulScalePosLFT_mdv.i1x";
connectAttr "unitConversion69.o" "ballFrontProxySkinMulScalePosLFT_mdv.i1y";
connectAttr "unitConversion71.o" "ballFrontProxySkinMulScalePosLFT_mdv.i1z";
connectAttr "unitConversion68.o" "ballFrontProxySkinMulScaleNegLFT_mdv.i1x";
connectAttr "unitConversion70.o" "ballFrontProxySkinMulScaleNegLFT_mdv.i1y";
connectAttr "unitConversion72.o" "ballFrontProxySkinMulScaleNegLFT_mdv.i1z";
connectAttr "ballFrontProxySkinMulScalePosLFT_mdv.o" "ballFrontProxySkinClampScalePosLFT_mdv.ip"
		;
connectAttr "ballFrontProxySkinMulScaleNegLFT_mdv.o" "ballFrontProxySkinClampScaleNegLFT_mdv.ip"
		;
connectAttr "ballFrontProxySkinClampScalePosLFT_mdv.op" "ballFrontProxySkinPlusPosNegScaleLFT_pma.i3[0]"
		;
connectAttr "ballFrontProxySkinClampScaleNegLFT_mdv.op" "ballFrontProxySkinPlusPosNegScaleLFT_pma.i3[1]"
		;
connectAttr "ballFrontProxySkinBtwLFT_jnt.scaleBtw" "ballFrontProxySkinMulScalePlusLFT_mdv.i2x"
		;
connectAttr "ballFrontProxySkinBtwLFT_jnt.scaleBtw" "ballFrontProxySkinMulScalePlusLFT_mdv.i2y"
		;
connectAttr "ballFrontProxySkinBtwLFT_jnt.scaleBtw" "ballFrontProxySkinMulScalePlusLFT_mdv.i2z"
		;
connectAttr "ballFrontProxySkinPlusPosNegScaleLFT_pma.o3" "ballFrontProxySkinMulScalePlusLFT_mdv.i1"
		;
connectAttr "ballFrontProxySkinMulScalePlusLFT_mdv.oz" "ballFrontProxySkinDoubleScaleALFT_adl.i2"
		;
connectAttr "ballFrontProxySkinMulScalePlusLFT_mdv.ox" "ballFrontProxySkinDoubleScaleBLFT_adl.i2"
		;
connectAttr "ballFrontProxySkinBtwLFT_jnt.aimBtw" "ballFrontProxySkinDriveVoidLFT_adl.ix"
		;
connectAttr "ballFrontProxySkinBtwIkLFT_jnt.rx" "unitConversion67.i";
connectAttr "ballFrontProxySkinBtwIkLFT_jnt.rx" "unitConversion68.i";
connectAttr "ballFrontProxySkinBtwIkLFT_jnt.ry" "unitConversion69.i";
connectAttr "ballFrontProxySkinBtwIkLFT_jnt.ry" "unitConversion70.i";
connectAttr "ballFrontProxySkinBtwIkLFT_jnt.rz" "unitConversion71.i";
connectAttr "ballFrontProxySkinBtwIkLFT_jnt.rz" "unitConversion72.i";
connectAttr "unitConversion73.o" "ankleFrontProxySkinMulScalePosRGT_mdv.i1x";
connectAttr "unitConversion75.o" "ankleFrontProxySkinMulScalePosRGT_mdv.i1y";
connectAttr "unitConversion77.o" "ankleFrontProxySkinMulScalePosRGT_mdv.i1z";
connectAttr "unitConversion74.o" "ankleFrontProxySkinMulScaleNegRGT_mdv.i1x";
connectAttr "unitConversion76.o" "ankleFrontProxySkinMulScaleNegRGT_mdv.i1y";
connectAttr "unitConversion78.o" "ankleFrontProxySkinMulScaleNegRGT_mdv.i1z";
connectAttr "ankleFrontProxySkinMulScalePosRGT_mdv.o" "ankleFrontProxySkinClampScalePosRGT_mdv.ip"
		;
connectAttr "ankleFrontProxySkinMulScaleNegRGT_mdv.o" "ankleFrontProxySkinClampScaleNegRGT_mdv.ip"
		;
connectAttr "ankleFrontProxySkinClampScalePosRGT_mdv.op" "ankleFrontProxySkinPlusPosNegScaleRGT_pma.i3[0]"
		;
connectAttr "ankleFrontProxySkinClampScaleNegRGT_mdv.op" "ankleFrontProxySkinPlusPosNegScaleRGT_pma.i3[1]"
		;
connectAttr "ankleFrontProxySkinBtwRGT_jnt.scaleBtw" "ankleFrontProxySkinMulScalePlusRGT_mdv.i2x"
		;
connectAttr "ankleFrontProxySkinBtwRGT_jnt.scaleBtw" "ankleFrontProxySkinMulScalePlusRGT_mdv.i2y"
		;
connectAttr "ankleFrontProxySkinBtwRGT_jnt.scaleBtw" "ankleFrontProxySkinMulScalePlusRGT_mdv.i2z"
		;
connectAttr "ankleFrontProxySkinPlusPosNegScaleRGT_pma.o3" "ankleFrontProxySkinMulScalePlusRGT_mdv.i1"
		;
connectAttr "ankleFrontProxySkinMulScalePlusRGT_mdv.oz" "ankleFrontProxySkinDoubleScaleARGT_adl.i2"
		;
connectAttr "ankleFrontProxySkinMulScalePlusRGT_mdv.ox" "ankleFrontProxySkinDoubleScaleBRGT_adl.i2"
		;
connectAttr "ankleFrontProxySkinBtwRGT_jnt.aimBtw" "ankleFrontProxySkinDriveVoidRGT_adl.ix"
		;
connectAttr "ankleFrontProxySkinBtwIkRGT_jnt.rx" "unitConversion73.i";
connectAttr "ankleFrontProxySkinBtwIkRGT_jnt.rx" "unitConversion74.i";
connectAttr "ankleFrontProxySkinBtwIkRGT_jnt.ry" "unitConversion75.i";
connectAttr "ankleFrontProxySkinBtwIkRGT_jnt.ry" "unitConversion76.i";
connectAttr "ankleFrontProxySkinBtwIkRGT_jnt.rz" "unitConversion77.i";
connectAttr "ankleFrontProxySkinBtwIkRGT_jnt.rz" "unitConversion78.i";
connectAttr "unitConversion79.o" "ballFrontProxySkinMulScalePosRGT_mdv.i1x";
connectAttr "unitConversion81.o" "ballFrontProxySkinMulScalePosRGT_mdv.i1y";
connectAttr "unitConversion83.o" "ballFrontProxySkinMulScalePosRGT_mdv.i1z";
connectAttr "unitConversion80.o" "ballFrontProxySkinMulScaleNegRGT_mdv.i1x";
connectAttr "unitConversion82.o" "ballFrontProxySkinMulScaleNegRGT_mdv.i1y";
connectAttr "unitConversion84.o" "ballFrontProxySkinMulScaleNegRGT_mdv.i1z";
connectAttr "ballFrontProxySkinMulScalePosRGT_mdv.o" "ballFrontProxySkinClampScalePosRGT_mdv.ip"
		;
connectAttr "ballFrontProxySkinMulScaleNegRGT_mdv.o" "ballFrontProxySkinClampScaleNegRGT_mdv.ip"
		;
connectAttr "ballFrontProxySkinClampScalePosRGT_mdv.op" "ballFrontProxySkinPlusPosNegScaleRGT_pma.i3[0]"
		;
connectAttr "ballFrontProxySkinClampScaleNegRGT_mdv.op" "ballFrontProxySkinPlusPosNegScaleRGT_pma.i3[1]"
		;
connectAttr "ballFrontProxySkinBtwRGT_jnt.scaleBtw" "ballFrontProxySkinMulScalePlusRGT_mdv.i2x"
		;
connectAttr "ballFrontProxySkinBtwRGT_jnt.scaleBtw" "ballFrontProxySkinMulScalePlusRGT_mdv.i2y"
		;
connectAttr "ballFrontProxySkinBtwRGT_jnt.scaleBtw" "ballFrontProxySkinMulScalePlusRGT_mdv.i2z"
		;
connectAttr "ballFrontProxySkinPlusPosNegScaleRGT_pma.o3" "ballFrontProxySkinMulScalePlusRGT_mdv.i1"
		;
connectAttr "ballFrontProxySkinMulScalePlusRGT_mdv.oz" "ballFrontProxySkinDoubleScaleARGT_adl.i2"
		;
connectAttr "ballFrontProxySkinMulScalePlusRGT_mdv.ox" "ballFrontProxySkinDoubleScaleBRGT_adl.i2"
		;
connectAttr "ballFrontProxySkinBtwRGT_jnt.aimBtw" "ballFrontProxySkinDriveVoidRGT_adl.ix"
		;
connectAttr "ballFrontProxySkinBtwIkRGT_jnt.rx" "unitConversion79.i";
connectAttr "ballFrontProxySkinBtwIkRGT_jnt.rx" "unitConversion80.i";
connectAttr "ballFrontProxySkinBtwIkRGT_jnt.ry" "unitConversion81.i";
connectAttr "ballFrontProxySkinBtwIkRGT_jnt.ry" "unitConversion82.i";
connectAttr "ballFrontProxySkinBtwIkRGT_jnt.rz" "unitConversion83.i";
connectAttr "ballFrontProxySkinBtwIkRGT_jnt.rz" "unitConversion84.i";
connectAttr "unitConversion85.o" "lowLegFrontProxySkinMulScalePosLFT_mdv.i1x";
connectAttr "unitConversion87.o" "lowLegFrontProxySkinMulScalePosLFT_mdv.i1y";
connectAttr "unitConversion89.o" "lowLegFrontProxySkinMulScalePosLFT_mdv.i1z";
connectAttr "unitConversion86.o" "lowLegFrontProxySkinMulScaleNegLFT_mdv.i1x";
connectAttr "unitConversion88.o" "lowLegFrontProxySkinMulScaleNegLFT_mdv.i1y";
connectAttr "unitConversion90.o" "lowLegFrontProxySkinMulScaleNegLFT_mdv.i1z";
connectAttr "lowLegFrontProxySkinMulScalePosLFT_mdv.o" "lowLegFrontProxySkinClampScalePosLFT_mdv.ip"
		;
connectAttr "lowLegFrontProxySkinMulScaleNegLFT_mdv.o" "lowLegFrontProxySkinClampScaleNegLFT_mdv.ip"
		;
connectAttr "lowLegFrontProxySkinClampScalePosLFT_mdv.op" "lowLegFrontProxySkinPlusPosNegScaleLFT_pma.i3[0]"
		;
connectAttr "lowLegFrontProxySkinClampScaleNegLFT_mdv.op" "lowLegFrontProxySkinPlusPosNegScaleLFT_pma.i3[1]"
		;
connectAttr "lowLegFrontProxySkinBtwLFT_jnt.scaleBtw" "lowLegFrontProxySkinMulScalePlusLFT_mdv.i2x"
		;
connectAttr "lowLegFrontProxySkinBtwLFT_jnt.scaleBtw" "lowLegFrontProxySkinMulScalePlusLFT_mdv.i2y"
		;
connectAttr "lowLegFrontProxySkinBtwLFT_jnt.scaleBtw" "lowLegFrontProxySkinMulScalePlusLFT_mdv.i2z"
		;
connectAttr "lowLegFrontProxySkinPlusPosNegScaleLFT_pma.o3" "lowLegFrontProxySkinMulScalePlusLFT_mdv.i1"
		;
connectAttr "lowLegFrontProxySkinMulScalePlusLFT_mdv.oz" "lowLegFrontProxySkinDoubleScaleALFT_adl.i2"
		;
connectAttr "lowLegFrontProxySkinMulScalePlusLFT_mdv.ox" "lowLegFrontProxySkinDoubleScaleBLFT_adl.i2"
		;
connectAttr "lowLegFrontProxySkinBtwLFT_jnt.aimBtw" "lowLegFrontProxySkinDriveVoidLFT_adl.ix"
		;
connectAttr "lowLegFrontProxySkinBtwIkLFT_jnt.rx" "unitConversion85.i";
connectAttr "lowLegFrontProxySkinBtwIkLFT_jnt.rx" "unitConversion86.i";
connectAttr "lowLegFrontProxySkinBtwIkLFT_jnt.ry" "unitConversion87.i";
connectAttr "lowLegFrontProxySkinBtwIkLFT_jnt.ry" "unitConversion88.i";
connectAttr "lowLegFrontProxySkinBtwIkLFT_jnt.rz" "unitConversion89.i";
connectAttr "lowLegFrontProxySkinBtwIkLFT_jnt.rz" "unitConversion90.i";
connectAttr "unitConversion91.o" "lowLegFrontProxySkinMulScalePosRGT_mdv.i1x";
connectAttr "unitConversion93.o" "lowLegFrontProxySkinMulScalePosRGT_mdv.i1y";
connectAttr "unitConversion95.o" "lowLegFrontProxySkinMulScalePosRGT_mdv.i1z";
connectAttr "unitConversion92.o" "lowLegFrontProxySkinMulScaleNegRGT_mdv.i1x";
connectAttr "unitConversion94.o" "lowLegFrontProxySkinMulScaleNegRGT_mdv.i1y";
connectAttr "unitConversion96.o" "lowLegFrontProxySkinMulScaleNegRGT_mdv.i1z";
connectAttr "lowLegFrontProxySkinMulScalePosRGT_mdv.o" "lowLegFrontProxySkinClampScalePosRGT_mdv.ip"
		;
connectAttr "lowLegFrontProxySkinMulScaleNegRGT_mdv.o" "lowLegFrontProxySkinClampScaleNegRGT_mdv.ip"
		;
connectAttr "lowLegFrontProxySkinClampScalePosRGT_mdv.op" "lowLegFrontProxySkinPlusPosNegScaleRGT_pma.i3[0]"
		;
connectAttr "lowLegFrontProxySkinClampScaleNegRGT_mdv.op" "lowLegFrontProxySkinPlusPosNegScaleRGT_pma.i3[1]"
		;
connectAttr "lowLegFrontProxySkinBtwRGT_jnt.scaleBtw" "lowLegFrontProxySkinMulScalePlusRGT_mdv.i2x"
		;
connectAttr "lowLegFrontProxySkinBtwRGT_jnt.scaleBtw" "lowLegFrontProxySkinMulScalePlusRGT_mdv.i2y"
		;
connectAttr "lowLegFrontProxySkinBtwRGT_jnt.scaleBtw" "lowLegFrontProxySkinMulScalePlusRGT_mdv.i2z"
		;
connectAttr "lowLegFrontProxySkinPlusPosNegScaleRGT_pma.o3" "lowLegFrontProxySkinMulScalePlusRGT_mdv.i1"
		;
connectAttr "lowLegFrontProxySkinMulScalePlusRGT_mdv.oz" "lowLegFrontProxySkinDoubleScaleARGT_adl.i2"
		;
connectAttr "lowLegFrontProxySkinMulScalePlusRGT_mdv.ox" "lowLegFrontProxySkinDoubleScaleBRGT_adl.i2"
		;
connectAttr "lowLegFrontProxySkinBtwRGT_jnt.aimBtw" "lowLegFrontProxySkinDriveVoidRGT_adl.ix"
		;
connectAttr "lowLegFrontProxySkinBtwIkRGT_jnt.rx" "unitConversion91.i";
connectAttr "lowLegFrontProxySkinBtwIkRGT_jnt.rx" "unitConversion92.i";
connectAttr "lowLegFrontProxySkinBtwIkRGT_jnt.ry" "unitConversion93.i";
connectAttr "lowLegFrontProxySkinBtwIkRGT_jnt.ry" "unitConversion94.i";
connectAttr "lowLegFrontProxySkinBtwIkRGT_jnt.rz" "unitConversion95.i";
connectAttr "lowLegFrontProxySkinBtwIkRGT_jnt.rz" "unitConversion96.i";
connectAttr "lambert2SG.pa" ":renderPartition.st" -na;
connectAttr "blinn1SG.pa" ":renderPartition.st" -na;
connectAttr "blinn2SG.pa" ":renderPartition.st" -na;
connectAttr "blinn3SG.pa" ":renderPartition.st" -na;
connectAttr "blinn4SG.pa" ":renderPartition.st" -na;
connectAttr "blinn5SG.pa" ":renderPartition.st" -na;
connectAttr "lambert3SG.pa" ":renderPartition.st" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "ikRPsolver.msg" ":ikSystem.sol" -na;
// End of frd_deerWhite_rig_proxySkin.ma
