//Maya ASCII 2012 scene
//Name: TEMPLATE_skirtPos_cluster01.ma
//Last modified: Thu, Nov 28, 2013 04:25:57 PM
//Codeset: 1252
requires maya "2012";
requires "Mayatomr" "2012.0m - 3.9.1.36 ";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2012";
fileInfo "version" "2012 x64";
fileInfo "cutIdentifier" "001200000000-796618";
fileInfo "osv" "Microsoft Windows 7 Business Edition, 64-bit Windows 7 Service Pack 1 (Build 7601)\n";
createNode transform -s -n "persp";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1.8387678480472591 4.689575367590769 8.4979792937064982 ;
	setAttr ".r" -type "double3" -24.338352729619395 7.7999999999999776 2.0064103041282314e-016 ;
createNode camera -s -n "perspShape" -p "persp";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 8.0177230273232247;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 0.24999999999999967 2.6276667611844418 0.2433455904736937 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0.087780912359665897 100.10003952158785 0.0096439053204650325 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 13.180658013390111;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0.95215700449221796 2.3202217813003334 100.17266742256591 ;
createNode camera -s -n "frontShape" -p "front";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 7.9813464183847707;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.10000000000002 0.43919102873622301 -0.086507323841961636 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 7.5128283551999333;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "TEMP_tempRoot_ctrl";
	addAttr -ci true -sn "_TYPE" -ln "_TYPE" -dt "string";
	addAttr -ci true -sn "shapeType" -ln "shapeType" -dt "string";
	addAttr -ci true -sn "nodes" -ln "nodes" -at "message";
	addAttr -ci true -sn "loftNodes" -ln "loftNodes" -at "message";
	addAttr -ci true -sn "surfaces" -ln "surfaces" -at "message";
	addAttr -ci true -sn "hip_loc" -ln "hip_loc" -at "message";
	addAttr -ci true -sn "upLegLFT_loc" -ln "upLegLFT_loc" -at "message";
	addAttr -ci true -sn "upLegRHT_loc" -ln "upLegRHT_loc" -at "message";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".rp" -type "double3" -5.5511151231257827e-016 2.3776667611844418 0.021088135040003064 ;
	setAttr ".sp" -type "double3" -5.5511151231257827e-016 2.3776667611844418 0.021088135040003064 ;
	setAttr -l on "._TYPE" -type "string" "rigTempRoot";
	setAttr -l on ".shapeType" -type "string" "crossCircle";
createNode nurbsCurve -n "TEMP_tempRoot_ctrlShape" -p "TEMP_tempRoot_ctrl";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		1 49 0 no 3
		50 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27
		 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49
		50
		-1.772028641110666 2.3776667611844418 0.021088135040003064
		-1.5746093592746542 2.3776667611844418 0.021088135040003064
		-1.5552185533516489 2.3776667611844418 -0.2252352021907354
		-1.4975364414420236 2.3776667611844418 -0.46548507329224509
		-1.4029864921698159 2.3776667611844418 -0.69376199163379182
		-1.2738778879695767 2.3776667611844418 -0.90443534799140357
		-1.1134213414044143 2.3776667611844418 -1.0923332063644091
		-0.92552348303140863 2.3776667611844418 -1.2527897529295706
		-0.71485012667379721 2.3776667611844418 -1.3818983571298102
		-0.48657320833225026 2.3776667611844418 -1.4764483064020166
		-0.24632333723073954 2.3776667611844418 -1.534130418311642
		-5.5511151231257827e-016 2.3776667611844418 -1.5535212242346459
		-5.5511151231257827e-016 2.3776667611844418 -1.7509405060706604
		-5.5511151231257827e-016 2.3776667611844418 -1.5535212242346459
		0.24632333723073843 2.3776667611844418 -1.534130418311642
		0.48657320833224904 2.3776667611844418 -1.4764483064020166
		0.7148501266737961 2.3776667611844418 -1.3818983571298102
		0.92552348303140752 2.3776667611844418 -1.2527897529295706
		1.1134213414044134 2.3776667611844418 -1.0923332063644091
		1.2738778879695758 2.3776667611844418 -0.90443534799140357
		1.402986492169815 2.3776667611844418 -0.69376199163379182
		1.4975364414420227 2.3776667611844418 -0.46548507329224509
		1.555218553351648 2.3776667611844418 -0.2252352021907354
		1.5746093592746528 2.3776667611844418 0.021088135040003064
		1.7720286411106652 2.3776667611844418 0.021088135040003064
		1.5746093592746528 2.3776667611844418 0.021088135040003064
		1.555218553351648 2.3776667611844418 0.26741147227074152
		1.4975364414420227 2.3776667611844418 0.50766134337225122
		1.402986492169815 2.3776667611844418 0.73593826171379795
		1.2738778879695758 2.3776667611844418 0.9466116180714097
		1.1134213414044134 2.3776667611844418 1.134509476444415
		0.92552348303140752 2.3776667611844418 1.2949660230095765
		0.7148501266737961 2.3776667611844418 1.4240746272098166
		0.48657320833224904 2.3776667611844418 1.518624576482023
		0.24632333723073843 2.3776667611844418 1.5763066883916483
		-5.5511151231257827e-016 2.3776667611844418 1.5956974943146522
		-5.5511151231257827e-016 2.3776667611844418 1.7931167761506663
		-5.5511151231257827e-016 2.3776667611844418 1.5956974943146522
		-0.24632333723073954 2.3776667611844418 1.5763066883916483
		-0.48657320833225026 2.3776667611844418 1.518624576482023
		-0.71485012667379721 2.3776667611844418 1.4240746272098166
		-0.92552348303140863 2.3776667611844418 1.2949660230095765
		-1.1134213414044143 2.3776667611844418 1.134509476444415
		-1.2738778879695767 2.3776667611844418 0.9466116180714097
		-1.4029864921698159 2.3776667611844418 0.73593826171379795
		-1.4975364414420236 2.3776667611844418 0.50766134337225122
		-1.5552185533516489 2.3776667611844418 0.26741147227074152
		-1.5746093592746542 2.3776667611844418 0.021088135040003064
		-1.772028641110666 2.3776667611844418 0.021088135040003064
		-1.5746093592746542 2.3776667611844418 0.021088135040003064
		;
createNode transform -n "TEMP_use_grp" -p "TEMP_tempRoot_ctrl";
	setAttr ".it" no;
createNode transform -n "TEMP_skirtPosSkin_nrbs" -p "TEMP_use_grp";
	addAttr -ci true -sn "tempRoot" -ln "tempRoot" -at "message";
	setAttr ".ovdt" 2;
	setAttr ".ove" yes;
createNode nurbsSurface -n "TEMP_skirtPosSkin_nrbsShape" -p "TEMP_skirtPosSkin_nrbs";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".tw" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
createNode transform -n "TEMP_tmp_grp" -p "TEMP_tempRoot_ctrl";
createNode transform -n "TEMP_ctrl_grp" -p "TEMP_tmp_grp";
createNode transform -n "TEMP_skirtLoftUP_tmpCtrl" -p "TEMP_ctrl_grp";
	setAttr -l on -k off ".v";
	setAttr ".rp" -type "double3" -5.5511151231257827e-016 2.3776667611844418 0.021088135040003064 ;
	setAttr ".sp" -type "double3" -5.5511151231257827e-016 2.3776667611844418 0.021088135040003064 ;
createNode nurbsCurve -n "TEMP_skirtLoftUP_tmpCtrlShape" -p "TEMP_skirtLoftUP_tmpCtrl";
	setAttr -k off ".v";
	setAttr -s 20 ".iog[0].og";
	setAttr ".iog[0].og[2].gcl" -type "componentList" 1 "cv[0]";
	setAttr ".iog[0].og[3].gcl" -type "componentList" 1 "cv[*]";
	setAttr ".iog[0].og[4].gcl" -type "componentList" 1 "cv[1]";
	setAttr ".iog[0].og[5].gcl" -type "componentList" 1 "cv[2]";
	setAttr ".iog[0].og[6].gcl" -type "componentList" 1 "cv[3]";
	setAttr ".iog[0].og[7].gcl" -type "componentList" 1 "cv[4]";
	setAttr ".iog[0].og[8].gcl" -type "componentList" 1 "cv[5]";
	setAttr ".iog[0].og[9].gcl" -type "componentList" 1 "cv[6]";
	setAttr ".iog[0].og[10].gcl" -type "componentList" 1 "cv[7]";
	setAttr ".iog[0].og[11].gcl" -type "componentList" 1 "cv[8]";
	setAttr ".iog[0].og[12].gcl" -type "componentList" 1 "cv[9]";
	setAttr ".iog[0].og[13].gcl" -type "componentList" 1 "cv[10]";
	setAttr ".iog[0].og[14].gcl" -type "componentList" 1 "cv[11]";
	setAttr ".iog[0].og[15].gcl" -type "componentList" 1 "cv[12]";
	setAttr ".iog[0].og[16].gcl" -type "componentList" 1 "cv[13]";
	setAttr ".iog[0].og[17].gcl" -type "componentList" 1 "cv[14]";
	setAttr ".iog[0].og[18].gcl" -type "componentList" 1 "cv[15]";
	setAttr ".iog[0].og[19].gcl" -type "componentList" 1 "cv[15]";
	setAttr ".iog[0].og[20].gcl" -type "componentList" 1 "cv[15]";
	setAttr ".iog[0].og[21].gcl" -type "componentList" 1 "cv[15]";
	setAttr ".ove" yes;
	setAttr ".ovc" 20;
	setAttr ".cc" -type "nurbsCurve" 
		3 16 2 no 3
		21 -2 -1 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18
		19
		0.43890188312182288 2.3776667611844418 -0.85253378687225712
		4.9200089451485364e-016 2.3776667611844418 -0.92451341924227837
		-0.43890188312182371 2.3776667611844418 -0.85253378687225712
		-0.81098493319382781 2.3776667611844418 -0.64755313629353717
		-1.0596028787837972 2.3776667611844418 -0.34077791340250402
		-1.1469058914029495 2.3776667611844418 0.021088135040002978
		-1.0596028787837972 2.3776667611844418 0.38295418348250998
		-0.81098493319382781 2.3776667611844418 0.68972940637354352
		-0.43890188312182365 2.3776667611844418 0.89471005695226347
		9.1647445455402744e-017 2.3776667611844418 0.9666896893222845
		0.43890188312182371 2.3776667611844418 0.89471005695226324
		0.81098493319382781 2.3776667611844418 0.68972940637354352
		1.0596028787837983 2.3776667611844418 0.38295418348250998
		1.1469058914029484 2.3776667611844418 0.02108813504000288
		1.0596028787837972 2.3776667611844418 -0.34077791340250457
		0.81098493319382747 2.3776667611844418 -0.64755313629353739
		0.43890188312182288 2.3776667611844418 -0.85253378687225712
		4.9200089451485364e-016 2.3776667611844418 -0.92451341924227837
		-0.43890188312182371 2.3776667611844418 -0.85253378687225712
		;
createNode nurbsCurve -n "TEMP_skirtLoftUP_tmpCtrlShapeOrig" -p "TEMP_skirtLoftUP_tmpCtrl";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".cc" -type "nurbsCurve" 
		3 16 2 no 3
		21 -2 -1 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18
		19
		0.35992571730626449 2.3776667611844418 -0.72307648241012534
		3.235604978257672e-016 2.3776667611844418 -0.78210407333306076
		-0.35992571730626516 2.3776667611844418 -0.72307648241012534
		-0.66505600688740363 2.3776667611844418 -0.55498012527289076
		-0.86893754816764968 2.3776667611844418 -0.30340614849823
		-0.94053122467786077 2.3776667611844418 -0.0066544095263063622
		-0.86893754816764968 2.3776667611844418 0.29009732944561728
		-0.66505600688740363 2.3776667611844418 0.54167130622027826
		-0.3599257173062651 2.3776667611844418 0.70976766335751273
		-4.7532047808898832e-018 2.3776667611844418 0.76879525428044804
		0.35992571730626516 2.3776667611844418 0.70976766335751251
		0.66505600688740363 2.3776667611844418 0.54167130622027826
		0.86893754816765034 2.3776667611844418 0.29009732944561728
		0.9405312246778601 2.3776667611844418 -0.0066544095263064438
		0.86893754816764968 2.3776667611844418 -0.3034061484982305
		0.66505600688740329 2.3776667611844418 -0.55498012527289109
		0.35992571730626449 2.3776667611844418 -0.72307648241012534
		3.235604978257672e-016 2.3776667611844418 -0.78210407333306076
		-0.35992571730626516 2.3776667611844418 -0.72307648241012534
		;
createNode transform -n "TEMP_skirtLoft01UP_clsHndl" -p "TEMP_skirtLoftUP_tmpCtrl";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 0.35992571730626449 2.3776667611844418 -0.72307648241012534 ;
	setAttr ".sp" -type "double3" 0.35992571730626449 2.3776667611844418 -0.72307648241012534 ;
createNode clusterHandle -n "TEMP_skirtLoft01UP_clsHndlShape" -p "TEMP_skirtLoft01UP_clsHndl";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".or" -type "double3" 0.35992571730626449 2.3776667611844418 -0.72307648241012534 ;
createNode transform -n "TEMP_skirtLoft02UP_clsHndl" -p "TEMP_skirtLoftUP_tmpCtrl";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 3.235604978257672e-016 2.3776667611844418 -0.78210407333306076 ;
	setAttr ".sp" -type "double3" 3.235604978257672e-016 2.3776667611844418 -0.78210407333306076 ;
createNode clusterHandle -n "TEMP_skirtLoft02UP_clsHndlShape" -p "TEMP_skirtLoft02UP_clsHndl";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".or" -type "double3" 3.235604978257672e-016 2.3776667611844418 -0.78210407333306076 ;
createNode transform -n "TEMP_skirtLoft03UP_clsHndl" -p "TEMP_skirtLoftUP_tmpCtrl";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" -0.35992571730626516 2.3776667611844418 -0.72307648241012534 ;
	setAttr ".sp" -type "double3" -0.35992571730626516 2.3776667611844418 -0.72307648241012534 ;
createNode clusterHandle -n "TEMP_skirtLoft03UP_clsHndlShape" -p "TEMP_skirtLoft03UP_clsHndl";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".or" -type "double3" -0.35992571730626516 2.3776667611844418 -0.72307648241012534 ;
createNode transform -n "TEMP_skirtLoft04UP_clsHndl" -p "TEMP_skirtLoftUP_tmpCtrl";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" -0.66505600688740363 2.3776667611844418 -0.55498012527289076 ;
	setAttr ".sp" -type "double3" -0.66505600688740363 2.3776667611844418 -0.55498012527289076 ;
createNode clusterHandle -n "TEMP_skirtLoft04UP_clsHndlShape" -p "TEMP_skirtLoft04UP_clsHndl";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".or" -type "double3" -0.66505600688740363 2.3776667611844418 -0.55498012527289076 ;
createNode transform -n "TEMP_skirtLoft05UP_clsHndl" -p "TEMP_skirtLoftUP_tmpCtrl";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" -0.86893754816764968 2.3776667611844418 -0.30340614849823 ;
	setAttr ".sp" -type "double3" -0.86893754816764968 2.3776667611844418 -0.30340614849823 ;
createNode clusterHandle -n "TEMP_skirtLoft05UP_clsHndlShape" -p "TEMP_skirtLoft05UP_clsHndl";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".or" -type "double3" -0.86893754816764968 2.3776667611844418 -0.30340614849823 ;
createNode transform -n "TEMP_skirtLoft06UP_clsHndl" -p "TEMP_skirtLoftUP_tmpCtrl";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" -0.94053122467786077 2.3776667611844418 -0.0066544095263063622 ;
	setAttr ".sp" -type "double3" -0.94053122467786077 2.3776667611844418 -0.0066544095263063622 ;
createNode clusterHandle -n "TEMP_skirtLoft06UP_clsHndlShape" -p "TEMP_skirtLoft06UP_clsHndl";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".or" -type "double3" -0.94053122467786077 2.3776667611844418 -0.0066544095263063622 ;
createNode transform -n "TEMP_skirtLoft07UP_clsHndl" -p "TEMP_skirtLoftUP_tmpCtrl";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" -0.86893754816764968 2.3776667611844418 0.29009732944561728 ;
	setAttr ".sp" -type "double3" -0.86893754816764968 2.3776667611844418 0.29009732944561728 ;
createNode clusterHandle -n "TEMP_skirtLoft07UP_clsHndlShape" -p "TEMP_skirtLoft07UP_clsHndl";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".or" -type "double3" -0.86893754816764968 2.3776667611844418 0.29009732944561728 ;
createNode transform -n "TEMP_skirtLoft08UP_clsHndl" -p "TEMP_skirtLoftUP_tmpCtrl";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" -0.66505600688740363 2.3776667611844418 0.54167130622027826 ;
	setAttr ".sp" -type "double3" -0.66505600688740363 2.3776667611844418 0.54167130622027826 ;
createNode clusterHandle -n "TEMP_skirtLoft08UP_clsHndlShape" -p "TEMP_skirtLoft08UP_clsHndl";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".or" -type "double3" -0.66505600688740363 2.3776667611844418 0.54167130622027826 ;
createNode transform -n "TEMP_skirtLoft09UP_clsHndl" -p "TEMP_skirtLoftUP_tmpCtrl";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" -0.3599257173062651 2.3776667611844418 0.70976766335751273 ;
	setAttr ".sp" -type "double3" -0.3599257173062651 2.3776667611844418 0.70976766335751273 ;
createNode clusterHandle -n "TEMP_skirtLoft09UP_clsHndlShape" -p "TEMP_skirtLoft09UP_clsHndl";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".or" -type "double3" -0.3599257173062651 2.3776667611844418 0.70976766335751273 ;
createNode transform -n "TEMP_skirtLoft10UP_clsHndl" -p "TEMP_skirtLoftUP_tmpCtrl";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" -4.7532047808898832e-018 2.3776667611844418 0.76879525428044804 ;
	setAttr ".sp" -type "double3" -4.7532047808898832e-018 2.3776667611844418 0.76879525428044804 ;
createNode clusterHandle -n "TEMP_skirtLoft10UP_clsHndlShape" -p "TEMP_skirtLoft10UP_clsHndl";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".or" -type "double3" -4.7532047808898832e-018 2.3776667611844418 0.76879525428044804 ;
createNode transform -n "TEMP_skirtLoft11UP_clsHndl" -p "TEMP_skirtLoftUP_tmpCtrl";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 0.35992571730626516 2.3776667611844418 0.70976766335751251 ;
	setAttr ".sp" -type "double3" 0.35992571730626516 2.3776667611844418 0.70976766335751251 ;
createNode clusterHandle -n "TEMP_skirtLoft11UP_clsHndlShape" -p "TEMP_skirtLoft11UP_clsHndl";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".or" -type "double3" 0.35992571730626516 2.3776667611844418 0.70976766335751251 ;
createNode transform -n "TEMP_skirtLoft12UP_clsHndl" -p "TEMP_skirtLoftUP_tmpCtrl";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 0.66505600688740363 2.3776667611844418 0.54167130622027826 ;
	setAttr ".sp" -type "double3" 0.66505600688740363 2.3776667611844418 0.54167130622027826 ;
createNode clusterHandle -n "TEMP_skirtLoft12UP_clsHndlShape" -p "TEMP_skirtLoft12UP_clsHndl";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".or" -type "double3" 0.66505600688740363 2.3776667611844418 0.54167130622027826 ;
createNode transform -n "TEMP_skirtLoft13UP_clsHndl" -p "TEMP_skirtLoftUP_tmpCtrl";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 0.86893754816765034 2.3776667611844418 0.29009732944561728 ;
	setAttr ".sp" -type "double3" 0.86893754816765034 2.3776667611844418 0.29009732944561728 ;
createNode clusterHandle -n "TEMP_skirtLoft13UP_clsHndlShape" -p "TEMP_skirtLoft13UP_clsHndl";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".or" -type "double3" 0.86893754816765034 2.3776667611844418 0.29009732944561728 ;
createNode transform -n "TEMP_skirtLoft14UP_clsHndl" -p "TEMP_skirtLoftUP_tmpCtrl";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 0.9405312246778601 2.3776667611844418 -0.0066544095263064438 ;
	setAttr ".sp" -type "double3" 0.9405312246778601 2.3776667611844418 -0.0066544095263064438 ;
createNode clusterHandle -n "TEMP_skirtLoft14UP_clsHndlShape" -p "TEMP_skirtLoft14UP_clsHndl";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".or" -type "double3" 0.9405312246778601 2.3776667611844418 -0.0066544095263064438 ;
createNode transform -n "TEMP_skirtLoft15UP_clsHndl" -p "TEMP_skirtLoftUP_tmpCtrl";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 0.86893754816764968 2.3776667611844418 -0.3034061484982305 ;
	setAttr ".sp" -type "double3" 0.86893754816764968 2.3776667611844418 -0.3034061484982305 ;
createNode clusterHandle -n "TEMP_skirtLoft15UP_clsHndlShape" -p "TEMP_skirtLoft15UP_clsHndl";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".or" -type "double3" 0.86893754816764968 2.3776667611844418 -0.3034061484982305 ;
createNode transform -n "TEMP_skirtLoft16UP_clsHndl" -p "TEMP_skirtLoftUP_tmpCtrl";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 0.66505600688740329 2.3776667611844418 -0.55498012527289109 ;
	setAttr ".sp" -type "double3" 0.66505600688740329 2.3776667611844418 -0.55498012527289109 ;
createNode clusterHandle -n "TEMP_skirtLoft16UP_clsHndlShape" -p "TEMP_skirtLoft16UP_clsHndl";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".or" -type "double3" 0.66505600688740329 2.3776667611844418 -0.55498012527289109 ;
createNode transform -n "TEMP_skirtLoftMID_tmpCtrl" -p "TEMP_ctrl_grp";
	setAttr -l on -k off ".v";
	setAttr ".rp" -type "double3" -7.7715611723760958e-016 1.1779886859071584 -0.056703236117644051 ;
	setAttr ".sp" -type "double3" -7.7715611723760958e-016 1.1779886859071584 -0.056703236117644051 ;
createNode nurbsCurve -n "TEMP_skirtLoftMID_tmpCtrlShape" -p "TEMP_skirtLoftMID_tmpCtrl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 20;
	setAttr ".cc" -type "nurbsCurve" 
		3 16 2 no 3
		21 -2 -1 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18
		19
		0.56325741790493744 1.1779886859071584 -1.2365206414764363
		4.9200089451485364e-016 1.1779886859071584 -1.3337283934472108
		-0.56325741790493844 1.1779886859071584 -1.2365206414764363
		-1.0407639998750591 1.1779886859071584 -0.95969638461119877
		-1.3598236974133537 1.1779886859071584 -0.54539960654109176
		-1.4718625638529803 1.1779886859071584 -0.056703236117644475
		-1.3598236974133537 1.1779886859071584 0.43199313430580327
		-1.0407639998750591 1.1779886859071584 0.84628991237591067
		-0.56325741790493833 1.1779886859071584 1.1231141692411482
		9.1647445455402744e-017 1.1779886859071584 1.2203219212119227
		0.56325741790493888 1.1779886859071584 1.1231141692411482
		1.0407639998750591 1.1779886859071584 0.84628991237590978
		1.3598236974133544 1.1779886859071584 0.4319931343058025
		1.4718625638529788 1.1779886859071584 -0.056703236117644579
		1.3598236974133537 1.1779886859071584 -0.54539960654109243
		1.0407639998750584 1.1779886859071584 -0.95969638461119922
		0.56325741790493744 1.1779886859071584 -1.2365206414764363
		4.9200089451485364e-016 1.1779886859071584 -1.3337283934472108
		-0.56325741790493844 1.1779886859071584 -1.2365206414764363
		;
createNode nurbsCurve -n "TEMP_skirtLoftMID_tmpCtrlShapeOrig" -p "TEMP_skirtLoftMID_tmpCtrl";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".cc" -type "nurbsCurve" 
		3 16 2 no 3
		21 -2 -1 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18
		19
		0.46190467155329712 1.1779886859071584 -1.037968601570217
		3.235604978257672e-016 1.1779886859071584 -1.1176847550005711
		-0.46190467155329784 1.1779886859071584 -1.037968601570217
		-0.8534885440388813 1.1779886859071584 -0.81095620301019711
		-1.1151365225874614 1.1779886859071584 -0.47120813912875664
		-1.2070150743098527 1.1779886859071584 -0.070447972827976255
		-1.1151365225874614 1.1779886859071584 0.33031219347280427
		-0.8534885440388813 1.1779886859071584 0.67006025735424524
		-0.46190467155329767 1.1779886859071584 0.89707265591426455
		-4.7532047808898832e-018 1.1779886859071584 0.97678880934461831
		0.46190467155329812 1.1779886859071584 0.89707265591426455
		0.8534885440388813 1.1779886859071584 0.67006025735424457
		1.1151365225874619 1.1779886859071584 0.33031219347280377
		1.2070150743098518 1.1779886859071584 -0.070447972827976338
		1.1151365225874614 1.1779886859071584 -0.47120813912875703
		0.85348854403888064 1.1779886859071584 -0.81095620301019766
		0.46190467155329712 1.1779886859071584 -1.037968601570217
		3.235604978257672e-016 1.1779886859071584 -1.1176847550005711
		-0.46190467155329784 1.1779886859071584 -1.037968601570217
		;
createNode transform -n "TEMP_skirtLoft01MID_clsHndl" -p "TEMP_skirtLoftMID_tmpCtrl";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 0.46190467155329712 1.1779886859071584 -1.037968601570217 ;
	setAttr ".sp" -type "double3" 0.46190467155329712 1.1779886859071584 -1.037968601570217 ;
createNode clusterHandle -n "TEMP_skirtLoft01MID_clsHndlShape" -p "TEMP_skirtLoft01MID_clsHndl";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".or" -type "double3" 0.46190467155329712 1.1779886859071584 -1.037968601570217 ;
createNode transform -n "TEMP_skirtLoft02MID_clsHndl" -p "TEMP_skirtLoftMID_tmpCtrl";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 3.235604978257672e-016 1.1779886859071584 -1.1176847550005711 ;
	setAttr ".sp" -type "double3" 3.235604978257672e-016 1.1779886859071584 -1.1176847550005711 ;
createNode clusterHandle -n "TEMP_skirtLoft02MID_clsHndlShape" -p "TEMP_skirtLoft02MID_clsHndl";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".or" -type "double3" 3.235604978257672e-016 1.1779886859071584 -1.1176847550005711 ;
createNode transform -n "TEMP_skirtLoft03MID_clsHndl" -p "TEMP_skirtLoftMID_tmpCtrl";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" -0.46190467155329784 1.1779886859071584 -1.037968601570217 ;
	setAttr ".sp" -type "double3" -0.46190467155329784 1.1779886859071584 -1.037968601570217 ;
createNode clusterHandle -n "TEMP_skirtLoft03MID_clsHndlShape" -p "TEMP_skirtLoft03MID_clsHndl";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".or" -type "double3" -0.46190467155329784 1.1779886859071584 -1.037968601570217 ;
createNode transform -n "TEMP_skirtLoft04MID_clsHndl" -p "TEMP_skirtLoftMID_tmpCtrl";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" -0.8534885440388813 1.1779886859071584 -0.81095620301019711 ;
	setAttr ".sp" -type "double3" -0.8534885440388813 1.1779886859071584 -0.81095620301019711 ;
createNode clusterHandle -n "TEMP_skirtLoft04MID_clsHndlShape" -p "TEMP_skirtLoft04MID_clsHndl";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".or" -type "double3" -0.8534885440388813 1.1779886859071584 -0.81095620301019711 ;
createNode transform -n "TEMP_skirtLoft05MID_clsHndl" -p "TEMP_skirtLoftMID_tmpCtrl";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" -1.1151365225874614 1.1779886859071584 -0.47120813912875664 ;
	setAttr ".sp" -type "double3" -1.1151365225874614 1.1779886859071584 -0.47120813912875664 ;
createNode clusterHandle -n "TEMP_skirtLoft05MID_clsHndlShape" -p "TEMP_skirtLoft05MID_clsHndl";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".or" -type "double3" -1.1151365225874614 1.1779886859071584 -0.47120813912875664 ;
createNode transform -n "TEMP_skirtLoft06MID_clsHndl" -p "TEMP_skirtLoftMID_tmpCtrl";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" -1.2070150743098527 1.1779886859071584 -0.070447972827976255 ;
	setAttr ".sp" -type "double3" -1.2070150743098527 1.1779886859071584 -0.070447972827976255 ;
createNode clusterHandle -n "TEMP_skirtLoft06MID_clsHndlShape" -p "TEMP_skirtLoft06MID_clsHndl";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".or" -type "double3" -1.2070150743098527 1.1779886859071584 -0.070447972827976255 ;
createNode transform -n "TEMP_skirtLoft07MID_clsHndl" -p "TEMP_skirtLoftMID_tmpCtrl";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" -1.1151365225874614 1.1779886859071584 0.33031219347280427 ;
	setAttr ".sp" -type "double3" -1.1151365225874614 1.1779886859071584 0.33031219347280427 ;
createNode clusterHandle -n "TEMP_skirtLoft07MID_clsHndlShape" -p "TEMP_skirtLoft07MID_clsHndl";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".or" -type "double3" -1.1151365225874614 1.1779886859071584 0.33031219347280427 ;
createNode transform -n "TEMP_skirtLoft08MID_clsHndl" -p "TEMP_skirtLoftMID_tmpCtrl";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" -0.8534885440388813 1.1779886859071584 0.67006025735424524 ;
	setAttr ".sp" -type "double3" -0.8534885440388813 1.1779886859071584 0.67006025735424524 ;
createNode clusterHandle -n "TEMP_skirtLoft08MID_clsHndlShape" -p "TEMP_skirtLoft08MID_clsHndl";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".or" -type "double3" -0.8534885440388813 1.1779886859071584 0.67006025735424524 ;
createNode transform -n "TEMP_skirtLoft09MID_clsHndl" -p "TEMP_skirtLoftMID_tmpCtrl";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" -0.46190467155329767 1.1779886859071584 0.89707265591426455 ;
	setAttr ".sp" -type "double3" -0.46190467155329767 1.1779886859071584 0.89707265591426455 ;
createNode clusterHandle -n "TEMP_skirtLoft09MID_clsHndlShape" -p "TEMP_skirtLoft09MID_clsHndl";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".or" -type "double3" -0.46190467155329767 1.1779886859071584 0.89707265591426455 ;
createNode transform -n "TEMP_skirtLoft10MID_clsHndl" -p "TEMP_skirtLoftMID_tmpCtrl";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" -4.7532047808898832e-018 1.1779886859071584 0.97678880934461831 ;
	setAttr ".sp" -type "double3" -4.7532047808898832e-018 1.1779886859071584 0.97678880934461831 ;
createNode clusterHandle -n "TEMP_skirtLoft10MID_clsHndlShape" -p "TEMP_skirtLoft10MID_clsHndl";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".or" -type "double3" -4.7532047808898832e-018 1.1779886859071584 0.97678880934461831 ;
createNode transform -n "TEMP_skirtLoft11MID_clsHndl" -p "TEMP_skirtLoftMID_tmpCtrl";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 0.46190467155329812 1.1779886859071584 0.89707265591426455 ;
	setAttr ".sp" -type "double3" 0.46190467155329812 1.1779886859071584 0.89707265591426455 ;
createNode clusterHandle -n "TEMP_skirtLoft11MID_clsHndlShape" -p "TEMP_skirtLoft11MID_clsHndl";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".or" -type "double3" 0.46190467155329812 1.1779886859071584 0.89707265591426455 ;
createNode transform -n "TEMP_skirtLoft12MID_clsHndl" -p "TEMP_skirtLoftMID_tmpCtrl";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 0.8534885440388813 1.1779886859071584 0.67006025735424457 ;
	setAttr ".sp" -type "double3" 0.8534885440388813 1.1779886859071584 0.67006025735424457 ;
createNode clusterHandle -n "TEMP_skirtLoft12MID_clsHndlShape" -p "TEMP_skirtLoft12MID_clsHndl";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".or" -type "double3" 0.8534885440388813 1.1779886859071584 0.67006025735424457 ;
createNode transform -n "TEMP_skirtLoft13MID_clsHndl" -p "TEMP_skirtLoftMID_tmpCtrl";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 1.1151365225874619 1.1779886859071584 0.33031219347280377 ;
	setAttr ".sp" -type "double3" 1.1151365225874619 1.1779886859071584 0.33031219347280377 ;
createNode clusterHandle -n "TEMP_skirtLoft13MID_clsHndlShape" -p "TEMP_skirtLoft13MID_clsHndl";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".or" -type "double3" 1.1151365225874619 1.1779886859071584 0.33031219347280377 ;
createNode transform -n "TEMP_skirtLoft14MID_clsHndl" -p "TEMP_skirtLoftMID_tmpCtrl";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 1.2070150743098518 1.1779886859071584 -0.070447972827976338 ;
	setAttr ".sp" -type "double3" 1.2070150743098518 1.1779886859071584 -0.070447972827976338 ;
createNode clusterHandle -n "TEMP_skirtLoft14MID_clsHndlShape" -p "TEMP_skirtLoft14MID_clsHndl";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".or" -type "double3" 1.2070150743098518 1.1779886859071584 -0.070447972827976338 ;
createNode transform -n "TEMP_skirtLoft15MID_clsHndl" -p "TEMP_skirtLoftMID_tmpCtrl";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 1.1151365225874614 1.1779886859071584 -0.47120813912875703 ;
	setAttr ".sp" -type "double3" 1.1151365225874614 1.1779886859071584 -0.47120813912875703 ;
createNode clusterHandle -n "TEMP_skirtLoft15MID_clsHndlShape" -p "TEMP_skirtLoft15MID_clsHndl";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".or" -type "double3" 1.1151365225874614 1.1779886859071584 -0.47120813912875703 ;
createNode transform -n "TEMP_skirtLoft16MID_clsHndl" -p "TEMP_skirtLoftMID_tmpCtrl";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 0.85348854403888064 1.1779886859071584 -0.81095620301019766 ;
	setAttr ".sp" -type "double3" 0.85348854403888064 1.1779886859071584 -0.81095620301019766 ;
createNode clusterHandle -n "TEMP_skirtLoft16MID_clsHndlShape" -p "TEMP_skirtLoft16MID_clsHndl";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".or" -type "double3" 0.85348854403888064 1.1779886859071584 -0.81095620301019766 ;
createNode transform -n "TEMP_skirtLoftLO_tmpCtrl" -p "TEMP_ctrl_grp";
	setAttr -l on -k off ".v";
	setAttr ".rp" -type "double3" -6.6613381477509392e-016 0 -0.1330881905261283 ;
	setAttr ".sp" -type "double3" -6.6613381477509392e-016 0 -0.1330881905261283 ;
createNode nurbsCurve -n "TEMP_skirtLoftLO_tmpCtrlShape" -p "TEMP_skirtLoftLO_tmpCtrl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 20;
	setAttr ".cc" -type "nurbsCurve" 
		3 16 2 no 3
		21 -2 -1 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18
		19
		0.68999033818903222 0 -1.6489350106902303
		4.9200089451485364e-016 0 -1.7738289655097801
		-0.68999033818903421 0 -1.6489350106902303
		-1.2749359021667792 0 -1.2932671186863394
		-1.6657840323623627 0 -0.76097250191822985
		-1.8030316440006364 0 -0.13308819052612839
		-1.6657840323623627 0 0.4947961208659728
		-1.2749359021667792 0 1.0270907376340834
		-0.68999033818903321 0 1.3827586296379748
		4.9200089451485364e-016 0 1.5076525844575235
		0.68999033818903455 0 1.3827586296379744
		1.2749359021667792 0 1.0270907376340828
		1.6657840323623634 0 0.4947961208659728
		1.8030316440006351 0 -0.13308819052612877
		1.6657840323623627 0 -0.76097250191823052
		1.2749359021667777 0 -1.2932671186863394
		0.68999033818903222 0 -1.6489350106902303
		4.9200089451485364e-016 0 -1.7738289655097801
		-0.68999033818903421 0 -1.6489350106902303
		;
createNode nurbsCurve -n "TEMP_skirtLoftLO_tmpCtrlShapeOrig" -p "TEMP_skirtLoftLO_tmpCtrl";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".cc" -type "nurbsCurve" 
		3 16 2 no 3
		21 -2 -1 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18
		19
		0.56583322368236011 0 -1.37617297793532
		3.235604978257672e-016 0 -1.4785934687199667
		-0.56583322368236177 0 -1.37617297793532
		-1.0455234683500296 0 -1.084504096859283
		-1.3660422426552454 0 -0.64799076865069127
		-1.4785934687199691 0 -0.13308819052612839
		-1.3660422426552454 0 0.38181438759843439
		-1.0455234683500296 0 0.81832771580702668
		-0.56583322368236089 0 1.1099965968830634
		3.235604978257672e-016 0 1.2124170876677098
		0.565833223682362 0 1.1099965968830632
		1.0455234683500296 0 0.81832771580702612
		1.3660422426552459 0 0.38181438759843439
		1.4785934687199682 0 -0.13308819052612872
		1.3660422426552454 0 -0.64799076865069183
		1.0455234683500283 0 -1.0845040968592832
		0.56583322368236011 0 -1.37617297793532
		3.235604978257672e-016 0 -1.4785934687199667
		-0.56583322368236177 0 -1.37617297793532
		;
createNode transform -n "TEMP_skirtLoft01LO_clsHndl" -p "TEMP_skirtLoftLO_tmpCtrl";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 0.56583322368236011 0 -1.37617297793532 ;
	setAttr ".sp" -type "double3" 0.56583322368236011 0 -1.37617297793532 ;
createNode clusterHandle -n "TEMP_skirtLoft01LO_clsHndlShape" -p "TEMP_skirtLoft01LO_clsHndl";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".or" -type "double3" 0.56583322368236011 0 -1.37617297793532 ;
createNode transform -n "TEMP_skirtLoft02LO_clsHndl" -p "TEMP_skirtLoftLO_tmpCtrl";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 3.235604978257672e-016 0 -1.4785934687199667 ;
	setAttr ".sp" -type "double3" 3.235604978257672e-016 0 -1.4785934687199667 ;
createNode clusterHandle -n "TEMP_skirtLoft02LO_clsHndlShape" -p "TEMP_skirtLoft02LO_clsHndl";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".or" -type "double3" 3.235604978257672e-016 0 -1.4785934687199667 ;
createNode transform -n "TEMP_skirtLoft03LO_clsHndl" -p "TEMP_skirtLoftLO_tmpCtrl";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" -0.56583322368236177 0 -1.37617297793532 ;
	setAttr ".sp" -type "double3" -0.56583322368236177 0 -1.37617297793532 ;
createNode clusterHandle -n "TEMP_skirtLoft03LO_clsHndlShape" -p "TEMP_skirtLoft03LO_clsHndl";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".or" -type "double3" -0.56583322368236177 0 -1.37617297793532 ;
createNode transform -n "TEMP_skirtLoft04LO_clsHndl" -p "TEMP_skirtLoftLO_tmpCtrl";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" -1.0455234683500296 0 -1.084504096859283 ;
	setAttr ".sp" -type "double3" -1.0455234683500296 0 -1.084504096859283 ;
createNode clusterHandle -n "TEMP_skirtLoft04LO_clsHndlShape" -p "TEMP_skirtLoft04LO_clsHndl";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".or" -type "double3" -1.0455234683500296 0 -1.084504096859283 ;
createNode transform -n "TEMP_skirtLoft05LO_clsHndl" -p "TEMP_skirtLoftLO_tmpCtrl";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" -1.3660422426552454 0 -0.64799076865069127 ;
	setAttr ".sp" -type "double3" -1.3660422426552454 0 -0.64799076865069127 ;
createNode clusterHandle -n "TEMP_skirtLoft05LO_clsHndlShape" -p "TEMP_skirtLoft05LO_clsHndl";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".or" -type "double3" -1.3660422426552454 0 -0.64799076865069127 ;
createNode transform -n "TEMP_skirtLoft06LO_clsHndl" -p "TEMP_skirtLoftLO_tmpCtrl";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" -1.4785934687199691 0 -0.13308819052612839 ;
	setAttr ".sp" -type "double3" -1.4785934687199691 0 -0.13308819052612839 ;
createNode clusterHandle -n "TEMP_skirtLoft06LO_clsHndlShape" -p "TEMP_skirtLoft06LO_clsHndl";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".or" -type "double3" -1.4785934687199691 0 -0.13308819052612839 ;
createNode transform -n "TEMP_skirtLoft07LO_clsHndl" -p "TEMP_skirtLoftLO_tmpCtrl";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" -1.3660422426552454 0 0.38181438759843439 ;
	setAttr ".sp" -type "double3" -1.3660422426552454 0 0.38181438759843439 ;
createNode clusterHandle -n "TEMP_skirtLoft07LO_clsHndlShape" -p "TEMP_skirtLoft07LO_clsHndl";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".or" -type "double3" -1.3660422426552454 0 0.38181438759843439 ;
createNode transform -n "TEMP_skirtLoft08LO_clsHndl" -p "TEMP_skirtLoftLO_tmpCtrl";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" -1.0455234683500296 0 0.81832771580702668 ;
	setAttr ".sp" -type "double3" -1.0455234683500296 0 0.81832771580702668 ;
createNode clusterHandle -n "TEMP_skirtLoft08LO_clsHndlShape" -p "TEMP_skirtLoft08LO_clsHndl";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".or" -type "double3" -1.0455234683500296 0 0.81832771580702668 ;
createNode transform -n "TEMP_skirtLoft09LO_clsHndl" -p "TEMP_skirtLoftLO_tmpCtrl";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" -0.56583322368236089 0 1.1099965968830634 ;
	setAttr ".sp" -type "double3" -0.56583322368236089 0 1.1099965968830634 ;
createNode clusterHandle -n "TEMP_skirtLoft09LO_clsHndlShape" -p "TEMP_skirtLoft09LO_clsHndl";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".or" -type "double3" -0.56583322368236089 0 1.1099965968830634 ;
createNode transform -n "TEMP_skirtLoft10LO_clsHndl" -p "TEMP_skirtLoftLO_tmpCtrl";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 3.235604978257672e-016 0 1.2124170876677098 ;
	setAttr ".sp" -type "double3" 3.235604978257672e-016 0 1.2124170876677098 ;
createNode clusterHandle -n "TEMP_skirtLoft10LO_clsHndlShape" -p "TEMP_skirtLoft10LO_clsHndl";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".or" -type "double3" 3.235604978257672e-016 0 1.2124170876677098 ;
createNode transform -n "TEMP_skirtLoft11LO_clsHndl" -p "TEMP_skirtLoftLO_tmpCtrl";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 0.565833223682362 0 1.1099965968830632 ;
	setAttr ".sp" -type "double3" 0.565833223682362 0 1.1099965968830632 ;
createNode clusterHandle -n "TEMP_skirtLoft11LO_clsHndlShape" -p "TEMP_skirtLoft11LO_clsHndl";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".or" -type "double3" 0.565833223682362 0 1.1099965968830632 ;
createNode transform -n "TEMP_skirtLoft12LO_clsHndl" -p "TEMP_skirtLoftLO_tmpCtrl";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 1.0455234683500296 0 0.81832771580702612 ;
	setAttr ".sp" -type "double3" 1.0455234683500296 0 0.81832771580702612 ;
createNode clusterHandle -n "TEMP_skirtLoft12LO_clsHndlShape" -p "TEMP_skirtLoft12LO_clsHndl";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".or" -type "double3" 1.0455234683500296 0 0.81832771580702612 ;
createNode transform -n "TEMP_skirtLoft13LO_clsHndl" -p "TEMP_skirtLoftLO_tmpCtrl";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 1.3660422426552459 0 0.38181438759843439 ;
	setAttr ".sp" -type "double3" 1.3660422426552459 0 0.38181438759843439 ;
createNode clusterHandle -n "TEMP_skirtLoft13LO_clsHndlShape" -p "TEMP_skirtLoft13LO_clsHndl";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".or" -type "double3" 1.3660422426552459 0 0.38181438759843439 ;
createNode transform -n "TEMP_skirtLoft14LO_clsHndl" -p "TEMP_skirtLoftLO_tmpCtrl";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 1.4785934687199682 0 -0.13308819052612872 ;
	setAttr ".sp" -type "double3" 1.4785934687199682 0 -0.13308819052612872 ;
createNode clusterHandle -n "TEMP_skirtLoft14LO_clsHndlShape" -p "TEMP_skirtLoft14LO_clsHndl";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".or" -type "double3" 1.4785934687199682 0 -0.13308819052612872 ;
createNode transform -n "TEMP_skirtLoft15LO_clsHndl" -p "TEMP_skirtLoftLO_tmpCtrl";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 1.3660422426552454 0 -0.64799076865069183 ;
	setAttr ".sp" -type "double3" 1.3660422426552454 0 -0.64799076865069183 ;
createNode clusterHandle -n "TEMP_skirtLoft15LO_clsHndlShape" -p "TEMP_skirtLoft15LO_clsHndl";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".or" -type "double3" 1.3660422426552454 0 -0.64799076865069183 ;
createNode transform -n "TEMP_skirtLoft16LO_clsHndl" -p "TEMP_skirtLoftLO_tmpCtrl";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 1.0455234683500283 0 -1.0845040968592832 ;
	setAttr ".sp" -type "double3" 1.0455234683500283 0 -1.0845040968592832 ;
createNode clusterHandle -n "TEMP_skirtLoft16LO_clsHndlShape" -p "TEMP_skirtLoft16LO_clsHndl";
	setAttr ".ihi" 0;
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".or" -type "double3" 1.0455234683500283 0 -1.0845040968592832 ;
createNode transform -n "TEMP_hip_tmpLoc" -p "TEMP_ctrl_grp";
	addAttr -ci true -sn "tempRoot" -ln "tempRoot" -at "message";
	setAttr -l on -k off ".v";
	setAttr -l on ".tx";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 1.2325951644078309e-032 2.4479657363983005 0.027059386924267757 ;
	setAttr ".sp" -type "double3" 1.2325951644078309e-032 2.4479657363983005 0.027059386924267757 ;
createNode locator -n "TEMP_hip_tmpLocShape" -p "TEMP_hip_tmpLoc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".lp" -type "double3" 1.2325951644078309e-032 2.4479657363983005 0.027059386924267761 ;
	setAttr ".los" -type "double3" 0.3 0.3 0.3 ;
createNode transform -n "TEMP_upLegLFT_tmpLoc" -p "TEMP_ctrl_grp";
	addAttr -ci true -sn "tempRoot" -ln "tempRoot" -at "message";
	setAttr -l on -k off ".v";
	setAttr ".r" -type "double3" 0 0 180 ;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 0.65052955929067524 1.7177713660101057 0.0018951183943218888 ;
	setAttr ".sp" -type "double3" 0.65052955929067524 1.7177713660101057 0.0018951183943218888 ;
createNode locator -n "TEMP_upLegLFT_tmpLocShape" -p "TEMP_upLegLFT_tmpLoc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".lp" -type "double3" 0.65052955929067524 1.7177713660101055 0.0018951183943218888 ;
	setAttr ".los" -type "double3" 0.1 0.1 0.1 ;
createNode transform -n "TEMP_upLegRHT_tmpLoc" -p "TEMP_ctrl_grp";
	addAttr -ci true -sn "tempRoot" -ln "tempRoot" -at "message";
	setAttr -l on -k off ".v";
	setAttr ".r" -type "double3" 0 180 0 ;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" -0.65053000000000016 1.7177674982481212 0.0018951200000008502 ;
	setAttr ".sp" -type "double3" -0.65053000000000016 1.7177674982481212 0.0018951200000008502 ;
createNode locator -n "TEMP_upLegRHT_tmpLocShape" -p "TEMP_upLegRHT_tmpLoc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".lp" -type "double3" -0.65053000000000016 1.7177674982481212 0.0018951200000008502 ;
	setAttr ".los" -type "double3" 0.1 0.1 0.1 ;
createNode transform -n "TEMP_still_grp" -p "TEMP_tmp_grp";
	setAttr ".it" no;
createNode transform -n "TEMP_skirtLoftUP_crv" -p "TEMP_still_grp";
	setAttr ".v" no;
createNode nurbsCurve -n "TEMP_skirtLoftUP_crvShape" -p "TEMP_skirtLoftUP_crv";
	setAttr -k off ".v";
	setAttr -s 34 ".iog[0].og";
	setAttr ".tw" yes;
createNode nurbsCurve -n "TEMP_skirtLoftUP_crvShapeOrig" -p "TEMP_skirtLoftUP_crv";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".cc" -type "nurbsCurve" 
		3 16 2 no 3
		21 -2 -1 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18
		19
		0.35992571730626449 2.3776667611844418 -0.72307648241012534
		3.235604978257672e-016 2.3776667611844418 -0.78210407333306076
		-0.35992571730626516 2.3776667611844418 -0.72307648241012534
		-0.66505600688740363 2.3776667611844418 -0.55498012527289076
		-0.86893754816764968 2.3776667611844418 -0.30340614849823
		-0.94053122467786077 2.3776667611844418 -0.0066544095263063622
		-0.86893754816764968 2.3776667611844418 0.29009732944561728
		-0.66505600688740363 2.3776667611844418 0.54167130622027826
		-0.3599257173062651 2.3776667611844418 0.70976766335751273
		-4.7532047808898832e-018 2.3776667611844418 0.76879525428044804
		0.35992571730626516 2.3776667611844418 0.70976766335751251
		0.66505600688740363 2.3776667611844418 0.54167130622027826
		0.86893754816765034 2.3776667611844418 0.29009732944561728
		0.9405312246778601 2.3776667611844418 -0.0066544095263064438
		0.86893754816764968 2.3776667611844418 -0.3034061484982305
		0.66505600688740329 2.3776667611844418 -0.55498012527289109
		0.35992571730626449 2.3776667611844418 -0.72307648241012534
		3.235604978257672e-016 2.3776667611844418 -0.78210407333306076
		-0.35992571730626516 2.3776667611844418 -0.72307648241012534
		;
createNode transform -n "TEMP_skirtLoftMID_crv" -p "TEMP_still_grp";
	setAttr ".v" no;
createNode nurbsCurve -n "TEMP_skirtLoftMID_crvShape" -p "TEMP_skirtLoftMID_crv";
	setAttr -k off ".v";
	setAttr -s 34 ".iog[0].og";
	setAttr ".tw" yes;
createNode nurbsCurve -n "TEMP_skirtLoftMID_crvShapeOrig" -p "TEMP_skirtLoftMID_crv";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".cc" -type "nurbsCurve" 
		3 16 2 no 3
		21 -2 -1 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18
		19
		0.46190467155329712 1.1779886859071584 -1.037968601570217
		3.235604978257672e-016 1.1779886859071584 -1.1176847550005711
		-0.46190467155329784 1.1779886859071584 -1.037968601570217
		-0.8534885440388813 1.1779886859071584 -0.81095620301019711
		-1.1151365225874614 1.1779886859071584 -0.47120813912875664
		-1.2070150743098527 1.1779886859071584 -0.070447972827976255
		-1.1151365225874614 1.1779886859071584 0.33031219347280427
		-0.8534885440388813 1.1779886859071584 0.67006025735424524
		-0.46190467155329767 1.1779886859071584 0.89707265591426455
		-4.7532047808898832e-018 1.1779886859071584 0.97678880934461831
		0.46190467155329812 1.1779886859071584 0.89707265591426455
		0.8534885440388813 1.1779886859071584 0.67006025735424457
		1.1151365225874619 1.1779886859071584 0.33031219347280377
		1.2070150743098518 1.1779886859071584 -0.070447972827976338
		1.1151365225874614 1.1779886859071584 -0.47120813912875703
		0.85348854403888064 1.1779886859071584 -0.81095620301019766
		0.46190467155329712 1.1779886859071584 -1.037968601570217
		3.235604978257672e-016 1.1779886859071584 -1.1176847550005711
		-0.46190467155329784 1.1779886859071584 -1.037968601570217
		;
createNode transform -n "TEMP_skirtLoftLO_crv" -p "TEMP_still_grp";
	setAttr ".v" no;
createNode nurbsCurve -n "TEMP_skirtLoftLO_crvShape" -p "TEMP_skirtLoftLO_crv";
	setAttr -k off ".v";
	setAttr -s 34 ".iog[0].og";
	setAttr ".tw" yes;
createNode nurbsCurve -n "TEMP_skirtLoftLO_crvShapeOrig" -p "TEMP_skirtLoftLO_crv";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".cc" -type "nurbsCurve" 
		3 16 2 no 3
		21 -2 -1 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18
		19
		0.56583322368236011 0 -1.37617297793532
		3.235604978257672e-016 0 -1.4785934687199667
		-0.56583322368236177 0 -1.37617297793532
		-1.0455234683500296 0 -1.084504096859283
		-1.3660422426552454 0 -0.64799076865069127
		-1.4785934687199691 0 -0.13308819052612839
		-1.3660422426552454 0 0.38181438759843439
		-1.0455234683500296 0 0.81832771580702668
		-0.56583322368236089 0 1.1099965968830634
		3.235604978257672e-016 0 1.2124170876677098
		0.565833223682362 0 1.1099965968830632
		1.0455234683500296 0 0.81832771580702612
		1.3660422426552459 0 0.38181438759843439
		1.4785934687199682 0 -0.13308819052612872
		1.3660422426552454 0 -0.64799076865069183
		1.0455234683500283 0 -1.0845040968592832
		0.56583322368236011 0 -1.37617297793532
		3.235604978257672e-016 0 -1.4785934687199667
		-0.56583322368236177 0 -1.37617297793532
		;
createNode transform -n "TEMP_ann_grp" -p "TEMP_still_grp";
createNode transform -n "TEMP_upLegLFTPointer_loc" -p "TEMP_ann_grp";
	setAttr ".v" no;
createNode locator -n "TEMP_upLegLFTPointer_locShape" -p "TEMP_upLegLFTPointer_loc";
	setAttr -k off ".v";
createNode parentConstraint -n "TEMP_upLegLFTPointer_loc_parentConstraint1" -p "TEMP_upLegLFTPointer_loc";
	addAttr -ci true -k true -sn "w0" -ln "upLegLFT_tmpLocW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 0 0 180 ;
	setAttr ".rst" -type "double3" 0.65052955929067524 1.7177713660101057 0.0018951183943218888 ;
	setAttr -k on ".w0";
createNode transform -n "TEMP_upLegLFTPointer_ann" -p "TEMP_ann_grp";
createNode annotationShape -n "TEMP_upLegLFTPointer_annShape" -p "TEMP_upLegLFTPointer_ann";
	setAttr -k off ".v";
	setAttr ".ovdt" 2;
	setAttr ".ove" yes;
	setAttr ".txt" -type "string" "";
createNode parentConstraint -n "TEMP_upLegLFTPointer_ann_parentConstraint1" -p "TEMP_upLegLFTPointer_ann";
	addAttr -ci true -k true -sn "w0" -ln "hip_tmpLocW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 1.2325951644078309e-032 2.4479657363983005 0.027059386924267757 ;
	setAttr -k on ".w0";
createNode transform -n "TEMP_upLegRHTPointer_loc" -p "TEMP_ann_grp";
	setAttr ".v" no;
createNode locator -n "TEMP_upLegRHTPointer_locShape" -p "TEMP_upLegRHTPointer_loc";
	setAttr -k off ".v";
createNode parentConstraint -n "TEMP_upLegRHTPointer_loc_parentConstraint1" -p "TEMP_upLegRHTPointer_loc";
	addAttr -ci true -k true -sn "w0" -ln "upLegRHT_tmpLocW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 0 180 0 ;
	setAttr ".rst" -type "double3" -0.65053000000000016 1.7177674982481212 0.0018951200000008502 ;
	setAttr -k on ".w0";
createNode transform -n "TEMP_upLegRHTPointer_ann" -p "TEMP_ann_grp";
createNode annotationShape -n "TEMP_upLegRHTPointer_annShape" -p "TEMP_upLegRHTPointer_ann";
	setAttr -k off ".v";
	setAttr ".ovdt" 2;
	setAttr ".ove" yes;
	setAttr ".txt" -type "string" "";
createNode parentConstraint -n "TEMP_upLegRHTPointer_ann_parentConstraint1" -p "TEMP_upLegRHTPointer_ann";
	addAttr -ci true -k true -sn "w0" -ln "hip_tmpLocW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 1.2325951644078309e-032 2.4479657363983005 0.027059386924267757 ;
	setAttr -k on ".w0";
createNode lightLinker -s -n "lightLinker1";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode displayLayerManager -n "layerManager";
createNode displayLayer -n "defaultLayer";
createNode renderLayerManager -n "renderLayerManager";
createNode renderLayer -n "defaultRenderLayer";
	setAttr ".g" yes;
createNode script -n "sceneConfigurationScriptNode";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 24 -ast 1 -aet 48 ";
	setAttr ".st" 6;
createNode mentalrayItemsList -s -n "mentalrayItemsList";
createNode mentalrayGlobals -s -n "mentalrayGlobals";
	setAttr ".rvb" 3;
	setAttr ".ivb" no;
createNode mentalrayOptions -s -n "miDefaultOptions";
	addAttr -ci true -m -sn "stringOptions" -ln "stringOptions" -at "compound" -nc 
		3;
	addAttr -ci true -sn "name" -ln "name" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "value" -ln "value" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "type" -ln "type" -dt "string" -p "stringOptions";
	setAttr ".minsp" 1;
	setAttr ".maxsp" 3;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
	setAttr -s 28 ".stringOptions";
	setAttr ".stringOptions[0].name" -type "string" "rast motion factor";
	setAttr ".stringOptions[0].value" -type "string" "1.0";
	setAttr ".stringOptions[0].type" -type "string" "scalar";
	setAttr ".stringOptions[1].name" -type "string" "rast transparency depth";
	setAttr ".stringOptions[1].value" -type "string" "8";
	setAttr ".stringOptions[1].type" -type "string" "integer";
	setAttr ".stringOptions[2].name" -type "string" "rast useopacity";
	setAttr ".stringOptions[2].value" -type "string" "true";
	setAttr ".stringOptions[2].type" -type "string" "boolean";
	setAttr ".stringOptions[3].name" -type "string" "importon";
	setAttr ".stringOptions[3].value" -type "string" "false";
	setAttr ".stringOptions[3].type" -type "string" "boolean";
	setAttr ".stringOptions[4].name" -type "string" "importon density";
	setAttr ".stringOptions[4].value" -type "string" "1.0";
	setAttr ".stringOptions[4].type" -type "string" "scalar";
	setAttr ".stringOptions[5].name" -type "string" "importon merge";
	setAttr ".stringOptions[5].value" -type "string" "0.0";
	setAttr ".stringOptions[5].type" -type "string" "scalar";
	setAttr ".stringOptions[6].name" -type "string" "importon trace depth";
	setAttr ".stringOptions[6].value" -type "string" "0";
	setAttr ".stringOptions[6].type" -type "string" "integer";
	setAttr ".stringOptions[7].name" -type "string" "importon traverse";
	setAttr ".stringOptions[7].value" -type "string" "true";
	setAttr ".stringOptions[7].type" -type "string" "boolean";
	setAttr ".stringOptions[8].name" -type "string" "shadowmap pixel samples";
	setAttr ".stringOptions[8].value" -type "string" "3";
	setAttr ".stringOptions[8].type" -type "string" "integer";
	setAttr ".stringOptions[9].name" -type "string" "ambient occlusion";
	setAttr ".stringOptions[9].value" -type "string" "false";
	setAttr ".stringOptions[9].type" -type "string" "boolean";
	setAttr ".stringOptions[10].name" -type "string" "ambient occlusion rays";
	setAttr ".stringOptions[10].value" -type "string" "256";
	setAttr ".stringOptions[10].type" -type "string" "integer";
	setAttr ".stringOptions[11].name" -type "string" "ambient occlusion cache";
	setAttr ".stringOptions[11].value" -type "string" "false";
	setAttr ".stringOptions[11].type" -type "string" "boolean";
	setAttr ".stringOptions[12].name" -type "string" "ambient occlusion cache density";
	setAttr ".stringOptions[12].value" -type "string" "1.0";
	setAttr ".stringOptions[12].type" -type "string" "scalar";
	setAttr ".stringOptions[13].name" -type "string" "ambient occlusion cache points";
	setAttr ".stringOptions[13].value" -type "string" "64";
	setAttr ".stringOptions[13].type" -type "string" "integer";
	setAttr ".stringOptions[14].name" -type "string" "irradiance particles";
	setAttr ".stringOptions[14].value" -type "string" "false";
	setAttr ".stringOptions[14].type" -type "string" "boolean";
	setAttr ".stringOptions[15].name" -type "string" "irradiance particles rays";
	setAttr ".stringOptions[15].value" -type "string" "256";
	setAttr ".stringOptions[15].type" -type "string" "integer";
	setAttr ".stringOptions[16].name" -type "string" "irradiance particles interpolate";
	setAttr ".stringOptions[16].value" -type "string" "1";
	setAttr ".stringOptions[16].type" -type "string" "integer";
	setAttr ".stringOptions[17].name" -type "string" "irradiance particles interppoints";
	setAttr ".stringOptions[17].value" -type "string" "64";
	setAttr ".stringOptions[17].type" -type "string" "integer";
	setAttr ".stringOptions[18].name" -type "string" "irradiance particles indirect passes";
	setAttr ".stringOptions[18].value" -type "string" "0";
	setAttr ".stringOptions[18].type" -type "string" "integer";
	setAttr ".stringOptions[19].name" -type "string" "irradiance particles scale";
	setAttr ".stringOptions[19].value" -type "string" "1.0";
	setAttr ".stringOptions[19].type" -type "string" "scalar";
	setAttr ".stringOptions[20].name" -type "string" "irradiance particles env";
	setAttr ".stringOptions[20].value" -type "string" "true";
	setAttr ".stringOptions[20].type" -type "string" "boolean";
	setAttr ".stringOptions[21].name" -type "string" "irradiance particles env rays";
	setAttr ".stringOptions[21].value" -type "string" "256";
	setAttr ".stringOptions[21].type" -type "string" "integer";
	setAttr ".stringOptions[22].name" -type "string" "irradiance particles env scale";
	setAttr ".stringOptions[22].value" -type "string" "1";
	setAttr ".stringOptions[22].type" -type "string" "integer";
	setAttr ".stringOptions[23].name" -type "string" "irradiance particles rebuild";
	setAttr ".stringOptions[23].value" -type "string" "true";
	setAttr ".stringOptions[23].type" -type "string" "boolean";
	setAttr ".stringOptions[24].name" -type "string" "irradiance particles file";
	setAttr ".stringOptions[24].value" -type "string" "";
	setAttr ".stringOptions[24].type" -type "string" "string";
	setAttr ".stringOptions[25].name" -type "string" "geom displace motion factor";
	setAttr ".stringOptions[25].value" -type "string" "1.0";
	setAttr ".stringOptions[25].type" -type "string" "scalar";
	setAttr ".stringOptions[26].name" -type "string" "contrast all buffers";
	setAttr ".stringOptions[26].value" -type "string" "true";
	setAttr ".stringOptions[26].type" -type "string" "boolean";
	setAttr ".stringOptions[27].name" -type "string" "finalgather normal tolerance";
	setAttr ".stringOptions[27].value" -type "string" "25.842";
	setAttr ".stringOptions[27].type" -type "string" "scalar";
createNode mentalrayFramebuffer -s -n "miDefaultFramebuffer";
	setAttr ".dat" 16;
createNode multDoubleLinear -n "TEMP_skirtPosLegMirror_mdl";
	addAttr -ci true -sn "tempRoot" -ln "tempRoot" -at "message";
	setAttr ".i2" -1;
createNode cluster -n "TEMP_skirtLoft16UP_clsHndlCluster";
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode groupParts -n "cluster16GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[15]";
createNode groupId -n "cluster16GroupId";
	setAttr ".ihi" 0;
createNode objectSet -n "cluster16Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode cluster -n "TEMP_skirtLoft15UP_clsHndlCluster";
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode groupParts -n "cluster15GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[14]";
createNode groupId -n "cluster15GroupId";
	setAttr ".ihi" 0;
createNode objectSet -n "cluster15Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode cluster -n "TEMP_skirtLoft14UP_clsHndlCluster";
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode groupParts -n "cluster14GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[13]";
createNode groupId -n "cluster14GroupId";
	setAttr ".ihi" 0;
createNode objectSet -n "cluster14Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode cluster -n "TEMP_skirtLoft13UP_clsHndlCluster";
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode groupParts -n "cluster13GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[12]";
createNode groupId -n "cluster13GroupId";
	setAttr ".ihi" 0;
createNode objectSet -n "cluster13Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode cluster -n "TEMP_skirtLoft12UP_clsHndlCluster";
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode groupParts -n "cluster12GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[11]";
createNode groupId -n "cluster12GroupId";
	setAttr ".ihi" 0;
createNode objectSet -n "cluster12Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode cluster -n "TEMP_skirtLoft11UP_clsHndlCluster";
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode groupParts -n "cluster11GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[10]";
createNode groupId -n "cluster11GroupId";
	setAttr ".ihi" 0;
createNode objectSet -n "cluster11Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode cluster -n "TEMP_skirtLoft10UP_clsHndlCluster";
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode groupParts -n "cluster10GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[9]";
createNode groupId -n "cluster10GroupId";
	setAttr ".ihi" 0;
createNode objectSet -n "cluster10Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode cluster -n "TEMP_skirtLoft09UP_clsHndlCluster";
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode groupParts -n "cluster9GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[8]";
createNode groupId -n "cluster9GroupId";
	setAttr ".ihi" 0;
createNode objectSet -n "cluster9Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode cluster -n "TEMP_skirtLoft08UP_clsHndlCluster";
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode groupParts -n "cluster8GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[7]";
createNode groupId -n "cluster8GroupId";
	setAttr ".ihi" 0;
createNode objectSet -n "cluster8Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode cluster -n "TEMP_skirtLoft07UP_clsHndlCluster";
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode groupParts -n "cluster7GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[6]";
createNode groupId -n "cluster7GroupId";
	setAttr ".ihi" 0;
createNode objectSet -n "cluster7Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode cluster -n "TEMP_skirtLoft06UP_clsHndlCluster";
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode groupParts -n "cluster6GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[5]";
createNode groupId -n "cluster6GroupId";
	setAttr ".ihi" 0;
createNode objectSet -n "cluster6Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode cluster -n "TEMP_skirtLoft05UP_clsHndlCluster";
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode groupParts -n "cluster5GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[4]";
createNode groupId -n "cluster5GroupId";
	setAttr ".ihi" 0;
createNode objectSet -n "cluster5Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode cluster -n "TEMP_skirtLoft04UP_clsHndlCluster";
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode groupParts -n "cluster4GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[3]";
createNode groupId -n "cluster4GroupId";
	setAttr ".ihi" 0;
createNode objectSet -n "cluster4Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode cluster -n "TEMP_skirtLoft03UP_clsHndlCluster";
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode groupParts -n "cluster3GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[2]";
createNode groupId -n "cluster3GroupId";
	setAttr ".ihi" 0;
createNode objectSet -n "cluster3Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode cluster -n "TEMP_skirtLoft02UP_clsHndlCluster";
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode groupParts -n "cluster2GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[1]";
createNode groupId -n "cluster2GroupId";
	setAttr ".ihi" 0;
createNode objectSet -n "cluster2Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode cluster -n "TEMP_skirtLoft01UP_clsHndlCluster";
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode objectSet -n "cluster1Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupParts -n "groupParts2";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode groupId -n "groupId2";
	setAttr ".ihi" 0;
createNode tweak -n "tweak1";
createNode objectSet -n "tweakSet1";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupParts -n "cluster1GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[0]";
createNode groupId -n "cluster1GroupId";
	setAttr ".ihi" 0;
createNode cluster -n "TEMP_skirtLoft16MID_clsHndlCluster";
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode groupParts -n "cluster35GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[15]";
createNode groupId -n "cluster35GroupId";
	setAttr ".ihi" 0;
createNode objectSet -n "cluster35Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode cluster -n "TEMP_skirtLoft15MID_clsHndlCluster";
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode groupParts -n "cluster34GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[14]";
createNode groupId -n "cluster34GroupId";
	setAttr ".ihi" 0;
createNode objectSet -n "cluster34Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode cluster -n "TEMP_skirtLoft14MID_clsHndlCluster";
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode groupParts -n "cluster33GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[13]";
createNode groupId -n "cluster33GroupId";
	setAttr ".ihi" 0;
createNode objectSet -n "cluster33Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode cluster -n "TEMP_skirtLoft13MID_clsHndlCluster";
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode groupParts -n "cluster32GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[12]";
createNode groupId -n "cluster32GroupId";
	setAttr ".ihi" 0;
createNode objectSet -n "cluster32Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode cluster -n "TEMP_skirtLoft12MID_clsHndlCluster";
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode groupParts -n "cluster31GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[11]";
createNode groupId -n "cluster31GroupId";
	setAttr ".ihi" 0;
createNode objectSet -n "cluster31Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode cluster -n "TEMP_skirtLoft11MID_clsHndlCluster";
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode groupParts -n "cluster30GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[10]";
createNode groupId -n "cluster30GroupId";
	setAttr ".ihi" 0;
createNode objectSet -n "cluster30Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode cluster -n "TEMP_skirtLoft10MID_clsHndlCluster";
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode groupParts -n "cluster29GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[9]";
createNode groupId -n "cluster29GroupId";
	setAttr ".ihi" 0;
createNode objectSet -n "cluster29Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode cluster -n "TEMP_skirtLoft09MID_clsHndlCluster";
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode groupParts -n "cluster28GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[8]";
createNode groupId -n "cluster28GroupId";
	setAttr ".ihi" 0;
createNode objectSet -n "cluster28Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode cluster -n "TEMP_skirtLoft08MID_clsHndlCluster";
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode groupParts -n "cluster27GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[7]";
createNode groupId -n "cluster27GroupId";
	setAttr ".ihi" 0;
createNode objectSet -n "cluster27Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode cluster -n "TEMP_skirtLoft07MID_clsHndlCluster";
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode groupParts -n "cluster26GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[6]";
createNode groupId -n "cluster26GroupId";
	setAttr ".ihi" 0;
createNode objectSet -n "cluster26Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode cluster -n "TEMP_skirtLoft06MID_clsHndlCluster";
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode groupParts -n "cluster25GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[5]";
createNode groupId -n "cluster25GroupId";
	setAttr ".ihi" 0;
createNode objectSet -n "cluster25Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode cluster -n "TEMP_skirtLoft05MID_clsHndlCluster";
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode groupParts -n "cluster24GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[4]";
createNode groupId -n "cluster24GroupId";
	setAttr ".ihi" 0;
createNode objectSet -n "cluster24Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode cluster -n "TEMP_skirtLoft04MID_clsHndlCluster";
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode groupParts -n "cluster23GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[3]";
createNode groupId -n "cluster23GroupId";
	setAttr ".ihi" 0;
createNode objectSet -n "cluster23Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode cluster -n "TEMP_skirtLoft03MID_clsHndlCluster";
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode groupParts -n "cluster22GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[2]";
createNode groupId -n "cluster22GroupId";
	setAttr ".ihi" 0;
createNode objectSet -n "cluster22Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode cluster -n "TEMP_skirtLoft02MID_clsHndlCluster";
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode groupParts -n "cluster21GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[1]";
createNode groupId -n "cluster21GroupId";
	setAttr ".ihi" 0;
createNode objectSet -n "cluster21Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode cluster -n "TEMP_skirtLoft01MID_clsHndlCluster";
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode objectSet -n "cluster20Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupParts -n "groupParts4";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode groupId -n "groupId4";
	setAttr ".ihi" 0;
createNode tweak -n "tweak2";
createNode objectSet -n "tweakSet2";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupParts -n "cluster20GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[0]";
createNode groupId -n "cluster20GroupId";
	setAttr ".ihi" 0;
createNode cluster -n "TEMP_skirtLoft16LO_clsHndlCluster";
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode groupParts -n "cluster54GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[15]";
createNode groupId -n "cluster54GroupId";
	setAttr ".ihi" 0;
createNode objectSet -n "cluster54Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode cluster -n "TEMP_skirtLoft15LO_clsHndlCluster";
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode groupParts -n "cluster53GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[14]";
createNode groupId -n "cluster53GroupId";
	setAttr ".ihi" 0;
createNode objectSet -n "cluster53Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode cluster -n "TEMP_skirtLoft14LO_clsHndlCluster";
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode groupParts -n "cluster52GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[13]";
createNode groupId -n "cluster52GroupId";
	setAttr ".ihi" 0;
createNode objectSet -n "cluster52Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode cluster -n "TEMP_skirtLoft13LO_clsHndlCluster";
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode groupParts -n "cluster51GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[12]";
createNode groupId -n "cluster51GroupId";
	setAttr ".ihi" 0;
createNode objectSet -n "cluster51Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode cluster -n "TEMP_skirtLoft12LO_clsHndlCluster";
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode groupParts -n "cluster50GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[11]";
createNode groupId -n "cluster50GroupId";
	setAttr ".ihi" 0;
createNode objectSet -n "cluster50Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode cluster -n "TEMP_skirtLoft11LO_clsHndlCluster";
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode groupParts -n "cluster49GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[10]";
createNode groupId -n "cluster49GroupId";
	setAttr ".ihi" 0;
createNode objectSet -n "cluster49Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode cluster -n "TEMP_skirtLoft10LO_clsHndlCluster";
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode groupParts -n "cluster48GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[9]";
createNode groupId -n "cluster48GroupId";
	setAttr ".ihi" 0;
createNode objectSet -n "cluster48Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode cluster -n "TEMP_skirtLoft09LO_clsHndlCluster";
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode groupParts -n "cluster47GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[8]";
createNode groupId -n "cluster47GroupId";
	setAttr ".ihi" 0;
createNode objectSet -n "cluster47Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode cluster -n "TEMP_skirtLoft08LO_clsHndlCluster";
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode groupParts -n "cluster46GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[7]";
createNode groupId -n "cluster46GroupId";
	setAttr ".ihi" 0;
createNode objectSet -n "cluster46Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode cluster -n "TEMP_skirtLoft07LO_clsHndlCluster";
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode groupParts -n "cluster45GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[6]";
createNode groupId -n "cluster45GroupId";
	setAttr ".ihi" 0;
createNode objectSet -n "cluster45Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode cluster -n "TEMP_skirtLoft06LO_clsHndlCluster";
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode groupParts -n "cluster44GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[5]";
createNode groupId -n "cluster44GroupId";
	setAttr ".ihi" 0;
createNode objectSet -n "cluster44Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode cluster -n "TEMP_skirtLoft05LO_clsHndlCluster";
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode groupParts -n "cluster43GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[4]";
createNode groupId -n "cluster43GroupId";
	setAttr ".ihi" 0;
createNode objectSet -n "cluster43Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode cluster -n "TEMP_skirtLoft04LO_clsHndlCluster";
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode groupParts -n "cluster42GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[3]";
createNode groupId -n "cluster42GroupId";
	setAttr ".ihi" 0;
createNode objectSet -n "cluster42Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode cluster -n "TEMP_skirtLoft03LO_clsHndlCluster";
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode groupParts -n "cluster41GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[2]";
createNode groupId -n "cluster41GroupId";
	setAttr ".ihi" 0;
createNode objectSet -n "cluster41Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode cluster -n "TEMP_skirtLoft02LO_clsHndlCluster";
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode groupParts -n "cluster40GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[1]";
createNode groupId -n "cluster40GroupId";
	setAttr ".ihi" 0;
createNode objectSet -n "cluster40Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode cluster -n "TEMP_skirtLoft01LO_clsHndlCluster";
	setAttr ".gm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode objectSet -n "cluster39Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupParts -n "groupParts6";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode groupId -n "groupId6";
	setAttr ".ihi" 0;
createNode tweak -n "tweak3";
createNode objectSet -n "tweakSet3";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupParts -n "cluster39GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[0]";
createNode groupId -n "cluster39GroupId";
	setAttr ".ihi" 0;
createNode loft -n "skirtPos_loft";
	addAttr -ci true -sn "tempRoot" -ln "tempRoot" -at "message";
	setAttr -s 3 ".ic";
	setAttr ".ss" 3;
select -ne :time1;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 1;
	setAttr -av ".unw" 1;
lockNode -l 1 ;
select -ne :renderPartition;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
lockNode -l 1 ;
select -ne :initialShadingGroup;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
lockNode -l 1 ;
select -ne :initialParticleSE;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
lockNode -l 1 ;
select -ne :defaultShaderList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".s";
lockNode -l 1 ;
select -ne :postProcessList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
lockNode -l 1 ;
select -ne :defaultRenderUtilityList1;
select -ne :defaultRenderingList1;
select -ne :renderGlobalsList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
lockNode -l 1 ;
select -ne :defaultLightSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr -k on ".ro" yes;
lockNode -l 1 ;
select -ne :defaultObjectSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr ".ro" yes;
lockNode -l 1 ;
select -ne :hardwareRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr ".ctrs" 256;
	setAttr -av ".btrs" 512;
	setAttr -k off ".fbfm";
	setAttr -k off -cb on ".ehql";
	setAttr -k off -cb on ".eams";
	setAttr -k off -cb on ".eeaa";
	setAttr -k off -cb on ".engm";
	setAttr -k off -cb on ".mes";
	setAttr -k off -cb on ".emb";
	setAttr -av -k off -cb on ".mbbf";
	setAttr -k off -cb on ".mbs";
	setAttr -k off -cb on ".trm";
	setAttr -k off -cb on ".tshc";
	setAttr -k off ".enpt";
	setAttr -k off -cb on ".clmt";
	setAttr -k off -cb on ".tcov";
	setAttr -k off -cb on ".lith";
	setAttr -k off -cb on ".sobc";
	setAttr -k off -cb on ".cuth";
	setAttr -k off -cb on ".hgcd";
	setAttr -k off -cb on ".hgci";
	setAttr -k off -cb on ".mgcs";
	setAttr -k off -cb on ".twa";
	setAttr -k off -cb on ".twz";
	setAttr -k on ".hwcc";
	setAttr -k on ".hwdp";
	setAttr -k on ".hwql";
	setAttr -k on ".hwfr";
lockNode -l 1 ;
select -ne :defaultHardwareRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -av -k on ".rp";
	setAttr -k on ".cai";
	setAttr -k on ".coi";
	setAttr -cb on ".bc";
	setAttr -av -k on ".bcb";
	setAttr -av -k on ".bcg";
	setAttr -av -k on ".bcr";
	setAttr -k on ".ei";
	setAttr -k on ".ex";
	setAttr -av -k on ".es";
	setAttr -av -k on ".ef";
	setAttr -av -k on ".bf";
	setAttr -k on ".fii";
	setAttr -av -k on ".sf";
	setAttr -k on ".gr";
	setAttr -k on ".li";
	setAttr -k on ".ls";
	setAttr -k on ".mb";
	setAttr -k on ".ti";
	setAttr -k on ".txt";
	setAttr -k on ".mpr";
	setAttr -k on ".wzd";
	setAttr -k on ".fn" -type "string" "im";
	setAttr -k on ".if";
	setAttr -k on ".res" -type "string" "ntsc_4d 646 485 1.333";
	setAttr -k on ".as";
	setAttr -k on ".ds";
	setAttr -k on ".lm";
	setAttr -k on ".fir";
	setAttr -k on ".aap";
	setAttr -k on ".gh";
	setAttr -cb on ".sd";
lockNode -l 1 ;
connectAttr "TEMP_tempRoot_ctrl.surfaces" "TEMP_skirtPosSkin_nrbs.tempRoot";
connectAttr "skirtPos_loft.os" "TEMP_skirtPosSkin_nrbsShape.cr";
connectAttr "TEMP_tempRoot_ctrl.hip_loc" "TEMP_hip_tmpLoc.tempRoot";
connectAttr "TEMP_tempRoot_ctrl.upLegLFT_loc" "TEMP_upLegLFT_tmpLoc.tempRoot";
connectAttr "TEMP_skirtPosLegMirror_mdl.o" "TEMP_upLegRHT_tmpLoc.tx";
connectAttr "TEMP_upLegLFT_tmpLoc.ty" "TEMP_upLegRHT_tmpLoc.ty";
connectAttr "TEMP_upLegLFT_tmpLoc.tz" "TEMP_upLegRHT_tmpLoc.tz";
connectAttr "TEMP_tempRoot_ctrl.upLegRHT_loc" "TEMP_upLegRHT_tmpLoc.tempRoot";
connectAttr "TEMP_skirtLoft16UP_clsHndlCluster.og[0]" "TEMP_skirtLoftUP_crvShape.cr"
		;
connectAttr "tweak1.pl[0].cp[0]" "TEMP_skirtLoftUP_crvShape.twl";
connectAttr "cluster1GroupId.id" "TEMP_skirtLoftUP_crvShape.iog.og[2].gid";
connectAttr "cluster1Set.mwc" "TEMP_skirtLoftUP_crvShape.iog.og[2].gco";
connectAttr "groupId2.id" "TEMP_skirtLoftUP_crvShape.iog.og[3].gid";
connectAttr "tweakSet1.mwc" "TEMP_skirtLoftUP_crvShape.iog.og[3].gco";
connectAttr "cluster2GroupId.id" "TEMP_skirtLoftUP_crvShape.iog.og[4].gid";
connectAttr "cluster2Set.mwc" "TEMP_skirtLoftUP_crvShape.iog.og[4].gco";
connectAttr "cluster3GroupId.id" "TEMP_skirtLoftUP_crvShape.iog.og[5].gid";
connectAttr "cluster3Set.mwc" "TEMP_skirtLoftUP_crvShape.iog.og[5].gco";
connectAttr "cluster4GroupId.id" "TEMP_skirtLoftUP_crvShape.iog.og[6].gid";
connectAttr "cluster4Set.mwc" "TEMP_skirtLoftUP_crvShape.iog.og[6].gco";
connectAttr "cluster5GroupId.id" "TEMP_skirtLoftUP_crvShape.iog.og[7].gid";
connectAttr "cluster5Set.mwc" "TEMP_skirtLoftUP_crvShape.iog.og[7].gco";
connectAttr "cluster6GroupId.id" "TEMP_skirtLoftUP_crvShape.iog.og[8].gid";
connectAttr "cluster6Set.mwc" "TEMP_skirtLoftUP_crvShape.iog.og[8].gco";
connectAttr "cluster7GroupId.id" "TEMP_skirtLoftUP_crvShape.iog.og[9].gid";
connectAttr "cluster7Set.mwc" "TEMP_skirtLoftUP_crvShape.iog.og[9].gco";
connectAttr "cluster8GroupId.id" "TEMP_skirtLoftUP_crvShape.iog.og[10].gid";
connectAttr "cluster8Set.mwc" "TEMP_skirtLoftUP_crvShape.iog.og[10].gco";
connectAttr "cluster9GroupId.id" "TEMP_skirtLoftUP_crvShape.iog.og[11].gid";
connectAttr "cluster9Set.mwc" "TEMP_skirtLoftUP_crvShape.iog.og[11].gco";
connectAttr "cluster10GroupId.id" "TEMP_skirtLoftUP_crvShape.iog.og[12].gid";
connectAttr "cluster10Set.mwc" "TEMP_skirtLoftUP_crvShape.iog.og[12].gco";
connectAttr "cluster11GroupId.id" "TEMP_skirtLoftUP_crvShape.iog.og[13].gid";
connectAttr "cluster11Set.mwc" "TEMP_skirtLoftUP_crvShape.iog.og[13].gco";
connectAttr "cluster12GroupId.id" "TEMP_skirtLoftUP_crvShape.iog.og[14].gid";
connectAttr "cluster12Set.mwc" "TEMP_skirtLoftUP_crvShape.iog.og[14].gco";
connectAttr "cluster13GroupId.id" "TEMP_skirtLoftUP_crvShape.iog.og[15].gid";
connectAttr "cluster13Set.mwc" "TEMP_skirtLoftUP_crvShape.iog.og[15].gco";
connectAttr "cluster14GroupId.id" "TEMP_skirtLoftUP_crvShape.iog.og[16].gid";
connectAttr "cluster14Set.mwc" "TEMP_skirtLoftUP_crvShape.iog.og[16].gco";
connectAttr "cluster15GroupId.id" "TEMP_skirtLoftUP_crvShape.iog.og[17].gid";
connectAttr "cluster15Set.mwc" "TEMP_skirtLoftUP_crvShape.iog.og[17].gco";
connectAttr "cluster16GroupId.id" "TEMP_skirtLoftUP_crvShape.iog.og[18].gid";
connectAttr "cluster16Set.mwc" "TEMP_skirtLoftUP_crvShape.iog.og[18].gco";
connectAttr "TEMP_skirtLoft16MID_clsHndlCluster.og[0]" "TEMP_skirtLoftMID_crvShape.cr"
		;
connectAttr "tweak2.pl[0].cp[0]" "TEMP_skirtLoftMID_crvShape.twl";
connectAttr "cluster20GroupId.id" "TEMP_skirtLoftMID_crvShape.iog.og[2].gid";
connectAttr "cluster20Set.mwc" "TEMP_skirtLoftMID_crvShape.iog.og[2].gco";
connectAttr "groupId4.id" "TEMP_skirtLoftMID_crvShape.iog.og[3].gid";
connectAttr "tweakSet2.mwc" "TEMP_skirtLoftMID_crvShape.iog.og[3].gco";
connectAttr "cluster21GroupId.id" "TEMP_skirtLoftMID_crvShape.iog.og[4].gid";
connectAttr "cluster21Set.mwc" "TEMP_skirtLoftMID_crvShape.iog.og[4].gco";
connectAttr "cluster22GroupId.id" "TEMP_skirtLoftMID_crvShape.iog.og[5].gid";
connectAttr "cluster22Set.mwc" "TEMP_skirtLoftMID_crvShape.iog.og[5].gco";
connectAttr "cluster23GroupId.id" "TEMP_skirtLoftMID_crvShape.iog.og[6].gid";
connectAttr "cluster23Set.mwc" "TEMP_skirtLoftMID_crvShape.iog.og[6].gco";
connectAttr "cluster24GroupId.id" "TEMP_skirtLoftMID_crvShape.iog.og[7].gid";
connectAttr "cluster24Set.mwc" "TEMP_skirtLoftMID_crvShape.iog.og[7].gco";
connectAttr "cluster25GroupId.id" "TEMP_skirtLoftMID_crvShape.iog.og[8].gid";
connectAttr "cluster25Set.mwc" "TEMP_skirtLoftMID_crvShape.iog.og[8].gco";
connectAttr "cluster26GroupId.id" "TEMP_skirtLoftMID_crvShape.iog.og[9].gid";
connectAttr "cluster26Set.mwc" "TEMP_skirtLoftMID_crvShape.iog.og[9].gco";
connectAttr "cluster27GroupId.id" "TEMP_skirtLoftMID_crvShape.iog.og[10].gid";
connectAttr "cluster27Set.mwc" "TEMP_skirtLoftMID_crvShape.iog.og[10].gco";
connectAttr "cluster28GroupId.id" "TEMP_skirtLoftMID_crvShape.iog.og[11].gid";
connectAttr "cluster28Set.mwc" "TEMP_skirtLoftMID_crvShape.iog.og[11].gco";
connectAttr "cluster29GroupId.id" "TEMP_skirtLoftMID_crvShape.iog.og[12].gid";
connectAttr "cluster29Set.mwc" "TEMP_skirtLoftMID_crvShape.iog.og[12].gco";
connectAttr "cluster30GroupId.id" "TEMP_skirtLoftMID_crvShape.iog.og[13].gid";
connectAttr "cluster30Set.mwc" "TEMP_skirtLoftMID_crvShape.iog.og[13].gco";
connectAttr "cluster31GroupId.id" "TEMP_skirtLoftMID_crvShape.iog.og[14].gid";
connectAttr "cluster31Set.mwc" "TEMP_skirtLoftMID_crvShape.iog.og[14].gco";
connectAttr "cluster32GroupId.id" "TEMP_skirtLoftMID_crvShape.iog.og[15].gid";
connectAttr "cluster32Set.mwc" "TEMP_skirtLoftMID_crvShape.iog.og[15].gco";
connectAttr "cluster33GroupId.id" "TEMP_skirtLoftMID_crvShape.iog.og[16].gid";
connectAttr "cluster33Set.mwc" "TEMP_skirtLoftMID_crvShape.iog.og[16].gco";
connectAttr "cluster34GroupId.id" "TEMP_skirtLoftMID_crvShape.iog.og[17].gid";
connectAttr "cluster34Set.mwc" "TEMP_skirtLoftMID_crvShape.iog.og[17].gco";
connectAttr "cluster35GroupId.id" "TEMP_skirtLoftMID_crvShape.iog.og[18].gid";
connectAttr "cluster35Set.mwc" "TEMP_skirtLoftMID_crvShape.iog.og[18].gco";
connectAttr "TEMP_skirtLoft16LO_clsHndlCluster.og[0]" "TEMP_skirtLoftLO_crvShape.cr"
		;
connectAttr "tweak3.pl[0].cp[0]" "TEMP_skirtLoftLO_crvShape.twl";
connectAttr "cluster39GroupId.id" "TEMP_skirtLoftLO_crvShape.iog.og[2].gid";
connectAttr "cluster39Set.mwc" "TEMP_skirtLoftLO_crvShape.iog.og[2].gco";
connectAttr "groupId6.id" "TEMP_skirtLoftLO_crvShape.iog.og[3].gid";
connectAttr "tweakSet3.mwc" "TEMP_skirtLoftLO_crvShape.iog.og[3].gco";
connectAttr "cluster40GroupId.id" "TEMP_skirtLoftLO_crvShape.iog.og[4].gid";
connectAttr "cluster40Set.mwc" "TEMP_skirtLoftLO_crvShape.iog.og[4].gco";
connectAttr "cluster41GroupId.id" "TEMP_skirtLoftLO_crvShape.iog.og[5].gid";
connectAttr "cluster41Set.mwc" "TEMP_skirtLoftLO_crvShape.iog.og[5].gco";
connectAttr "cluster42GroupId.id" "TEMP_skirtLoftLO_crvShape.iog.og[6].gid";
connectAttr "cluster42Set.mwc" "TEMP_skirtLoftLO_crvShape.iog.og[6].gco";
connectAttr "cluster43GroupId.id" "TEMP_skirtLoftLO_crvShape.iog.og[7].gid";
connectAttr "cluster43Set.mwc" "TEMP_skirtLoftLO_crvShape.iog.og[7].gco";
connectAttr "cluster44GroupId.id" "TEMP_skirtLoftLO_crvShape.iog.og[8].gid";
connectAttr "cluster44Set.mwc" "TEMP_skirtLoftLO_crvShape.iog.og[8].gco";
connectAttr "cluster45GroupId.id" "TEMP_skirtLoftLO_crvShape.iog.og[9].gid";
connectAttr "cluster45Set.mwc" "TEMP_skirtLoftLO_crvShape.iog.og[9].gco";
connectAttr "cluster46GroupId.id" "TEMP_skirtLoftLO_crvShape.iog.og[10].gid";
connectAttr "cluster46Set.mwc" "TEMP_skirtLoftLO_crvShape.iog.og[10].gco";
connectAttr "cluster47GroupId.id" "TEMP_skirtLoftLO_crvShape.iog.og[11].gid";
connectAttr "cluster47Set.mwc" "TEMP_skirtLoftLO_crvShape.iog.og[11].gco";
connectAttr "cluster48GroupId.id" "TEMP_skirtLoftLO_crvShape.iog.og[12].gid";
connectAttr "cluster48Set.mwc" "TEMP_skirtLoftLO_crvShape.iog.og[12].gco";
connectAttr "cluster49GroupId.id" "TEMP_skirtLoftLO_crvShape.iog.og[13].gid";
connectAttr "cluster49Set.mwc" "TEMP_skirtLoftLO_crvShape.iog.og[13].gco";
connectAttr "cluster50GroupId.id" "TEMP_skirtLoftLO_crvShape.iog.og[14].gid";
connectAttr "cluster50Set.mwc" "TEMP_skirtLoftLO_crvShape.iog.og[14].gco";
connectAttr "cluster51GroupId.id" "TEMP_skirtLoftLO_crvShape.iog.og[15].gid";
connectAttr "cluster51Set.mwc" "TEMP_skirtLoftLO_crvShape.iog.og[15].gco";
connectAttr "cluster52GroupId.id" "TEMP_skirtLoftLO_crvShape.iog.og[16].gid";
connectAttr "cluster52Set.mwc" "TEMP_skirtLoftLO_crvShape.iog.og[16].gco";
connectAttr "cluster53GroupId.id" "TEMP_skirtLoftLO_crvShape.iog.og[17].gid";
connectAttr "cluster53Set.mwc" "TEMP_skirtLoftLO_crvShape.iog.og[17].gco";
connectAttr "cluster54GroupId.id" "TEMP_skirtLoftLO_crvShape.iog.og[18].gid";
connectAttr "cluster54Set.mwc" "TEMP_skirtLoftLO_crvShape.iog.og[18].gco";
connectAttr "TEMP_upLegLFTPointer_loc_parentConstraint1.ctx" "TEMP_upLegLFTPointer_loc.tx"
		;
connectAttr "TEMP_upLegLFTPointer_loc_parentConstraint1.cty" "TEMP_upLegLFTPointer_loc.ty"
		;
connectAttr "TEMP_upLegLFTPointer_loc_parentConstraint1.ctz" "TEMP_upLegLFTPointer_loc.tz"
		;
connectAttr "TEMP_upLegLFTPointer_loc_parentConstraint1.crx" "TEMP_upLegLFTPointer_loc.rx"
		;
connectAttr "TEMP_upLegLFTPointer_loc_parentConstraint1.cry" "TEMP_upLegLFTPointer_loc.ry"
		;
connectAttr "TEMP_upLegLFTPointer_loc_parentConstraint1.crz" "TEMP_upLegLFTPointer_loc.rz"
		;
connectAttr "TEMP_upLegLFTPointer_loc.ro" "TEMP_upLegLFTPointer_loc_parentConstraint1.cro"
		;
connectAttr "TEMP_upLegLFTPointer_loc.pim" "TEMP_upLegLFTPointer_loc_parentConstraint1.cpim"
		;
connectAttr "TEMP_upLegLFTPointer_loc.rp" "TEMP_upLegLFTPointer_loc_parentConstraint1.crp"
		;
connectAttr "TEMP_upLegLFTPointer_loc.rpt" "TEMP_upLegLFTPointer_loc_parentConstraint1.crt"
		;
connectAttr "TEMP_upLegLFT_tmpLoc.t" "TEMP_upLegLFTPointer_loc_parentConstraint1.tg[0].tt"
		;
connectAttr "TEMP_upLegLFT_tmpLoc.rp" "TEMP_upLegLFTPointer_loc_parentConstraint1.tg[0].trp"
		;
connectAttr "TEMP_upLegLFT_tmpLoc.rpt" "TEMP_upLegLFTPointer_loc_parentConstraint1.tg[0].trt"
		;
connectAttr "TEMP_upLegLFT_tmpLoc.r" "TEMP_upLegLFTPointer_loc_parentConstraint1.tg[0].tr"
		;
connectAttr "TEMP_upLegLFT_tmpLoc.ro" "TEMP_upLegLFTPointer_loc_parentConstraint1.tg[0].tro"
		;
connectAttr "TEMP_upLegLFT_tmpLoc.s" "TEMP_upLegLFTPointer_loc_parentConstraint1.tg[0].ts"
		;
connectAttr "TEMP_upLegLFT_tmpLoc.pm" "TEMP_upLegLFTPointer_loc_parentConstraint1.tg[0].tpm"
		;
connectAttr "TEMP_upLegLFTPointer_loc_parentConstraint1.w0" "TEMP_upLegLFTPointer_loc_parentConstraint1.tg[0].tw"
		;
connectAttr "TEMP_upLegLFTPointer_ann_parentConstraint1.ctx" "TEMP_upLegLFTPointer_ann.tx"
		;
connectAttr "TEMP_upLegLFTPointer_ann_parentConstraint1.cty" "TEMP_upLegLFTPointer_ann.ty"
		;
connectAttr "TEMP_upLegLFTPointer_ann_parentConstraint1.ctz" "TEMP_upLegLFTPointer_ann.tz"
		;
connectAttr "TEMP_upLegLFTPointer_ann_parentConstraint1.crx" "TEMP_upLegLFTPointer_ann.rx"
		;
connectAttr "TEMP_upLegLFTPointer_ann_parentConstraint1.cry" "TEMP_upLegLFTPointer_ann.ry"
		;
connectAttr "TEMP_upLegLFTPointer_ann_parentConstraint1.crz" "TEMP_upLegLFTPointer_ann.rz"
		;
connectAttr "TEMP_upLegLFTPointer_locShape.wm" "TEMP_upLegLFTPointer_annShape.dom"
		 -na;
connectAttr "TEMP_upLegLFTPointer_ann.ro" "TEMP_upLegLFTPointer_ann_parentConstraint1.cro"
		;
connectAttr "TEMP_upLegLFTPointer_ann.pim" "TEMP_upLegLFTPointer_ann_parentConstraint1.cpim"
		;
connectAttr "TEMP_upLegLFTPointer_ann.rp" "TEMP_upLegLFTPointer_ann_parentConstraint1.crp"
		;
connectAttr "TEMP_upLegLFTPointer_ann.rpt" "TEMP_upLegLFTPointer_ann_parentConstraint1.crt"
		;
connectAttr "TEMP_hip_tmpLoc.t" "TEMP_upLegLFTPointer_ann_parentConstraint1.tg[0].tt"
		;
connectAttr "TEMP_hip_tmpLoc.rp" "TEMP_upLegLFTPointer_ann_parentConstraint1.tg[0].trp"
		;
connectAttr "TEMP_hip_tmpLoc.rpt" "TEMP_upLegLFTPointer_ann_parentConstraint1.tg[0].trt"
		;
connectAttr "TEMP_hip_tmpLoc.r" "TEMP_upLegLFTPointer_ann_parentConstraint1.tg[0].tr"
		;
connectAttr "TEMP_hip_tmpLoc.ro" "TEMP_upLegLFTPointer_ann_parentConstraint1.tg[0].tro"
		;
connectAttr "TEMP_hip_tmpLoc.s" "TEMP_upLegLFTPointer_ann_parentConstraint1.tg[0].ts"
		;
connectAttr "TEMP_hip_tmpLoc.pm" "TEMP_upLegLFTPointer_ann_parentConstraint1.tg[0].tpm"
		;
connectAttr "TEMP_upLegLFTPointer_ann_parentConstraint1.w0" "TEMP_upLegLFTPointer_ann_parentConstraint1.tg[0].tw"
		;
connectAttr "TEMP_upLegRHTPointer_loc_parentConstraint1.ctx" "TEMP_upLegRHTPointer_loc.tx"
		;
connectAttr "TEMP_upLegRHTPointer_loc_parentConstraint1.cty" "TEMP_upLegRHTPointer_loc.ty"
		;
connectAttr "TEMP_upLegRHTPointer_loc_parentConstraint1.ctz" "TEMP_upLegRHTPointer_loc.tz"
		;
connectAttr "TEMP_upLegRHTPointer_loc_parentConstraint1.crx" "TEMP_upLegRHTPointer_loc.rx"
		;
connectAttr "TEMP_upLegRHTPointer_loc_parentConstraint1.cry" "TEMP_upLegRHTPointer_loc.ry"
		;
connectAttr "TEMP_upLegRHTPointer_loc_parentConstraint1.crz" "TEMP_upLegRHTPointer_loc.rz"
		;
connectAttr "TEMP_upLegRHTPointer_loc.ro" "TEMP_upLegRHTPointer_loc_parentConstraint1.cro"
		;
connectAttr "TEMP_upLegRHTPointer_loc.pim" "TEMP_upLegRHTPointer_loc_parentConstraint1.cpim"
		;
connectAttr "TEMP_upLegRHTPointer_loc.rp" "TEMP_upLegRHTPointer_loc_parentConstraint1.crp"
		;
connectAttr "TEMP_upLegRHTPointer_loc.rpt" "TEMP_upLegRHTPointer_loc_parentConstraint1.crt"
		;
connectAttr "TEMP_upLegRHT_tmpLoc.t" "TEMP_upLegRHTPointer_loc_parentConstraint1.tg[0].tt"
		;
connectAttr "TEMP_upLegRHT_tmpLoc.rp" "TEMP_upLegRHTPointer_loc_parentConstraint1.tg[0].trp"
		;
connectAttr "TEMP_upLegRHT_tmpLoc.rpt" "TEMP_upLegRHTPointer_loc_parentConstraint1.tg[0].trt"
		;
connectAttr "TEMP_upLegRHT_tmpLoc.r" "TEMP_upLegRHTPointer_loc_parentConstraint1.tg[0].tr"
		;
connectAttr "TEMP_upLegRHT_tmpLoc.ro" "TEMP_upLegRHTPointer_loc_parentConstraint1.tg[0].tro"
		;
connectAttr "TEMP_upLegRHT_tmpLoc.s" "TEMP_upLegRHTPointer_loc_parentConstraint1.tg[0].ts"
		;
connectAttr "TEMP_upLegRHT_tmpLoc.pm" "TEMP_upLegRHTPointer_loc_parentConstraint1.tg[0].tpm"
		;
connectAttr "TEMP_upLegRHTPointer_loc_parentConstraint1.w0" "TEMP_upLegRHTPointer_loc_parentConstraint1.tg[0].tw"
		;
connectAttr "TEMP_upLegRHTPointer_ann_parentConstraint1.ctx" "TEMP_upLegRHTPointer_ann.tx"
		;
connectAttr "TEMP_upLegRHTPointer_ann_parentConstraint1.cty" "TEMP_upLegRHTPointer_ann.ty"
		;
connectAttr "TEMP_upLegRHTPointer_ann_parentConstraint1.ctz" "TEMP_upLegRHTPointer_ann.tz"
		;
connectAttr "TEMP_upLegRHTPointer_ann_parentConstraint1.crx" "TEMP_upLegRHTPointer_ann.rx"
		;
connectAttr "TEMP_upLegRHTPointer_ann_parentConstraint1.cry" "TEMP_upLegRHTPointer_ann.ry"
		;
connectAttr "TEMP_upLegRHTPointer_ann_parentConstraint1.crz" "TEMP_upLegRHTPointer_ann.rz"
		;
connectAttr "TEMP_upLegRHTPointer_locShape.wm" "TEMP_upLegRHTPointer_annShape.dom"
		 -na;
connectAttr "TEMP_upLegRHTPointer_ann.ro" "TEMP_upLegRHTPointer_ann_parentConstraint1.cro"
		;
connectAttr "TEMP_upLegRHTPointer_ann.pim" "TEMP_upLegRHTPointer_ann_parentConstraint1.cpim"
		;
connectAttr "TEMP_upLegRHTPointer_ann.rp" "TEMP_upLegRHTPointer_ann_parentConstraint1.crp"
		;
connectAttr "TEMP_upLegRHTPointer_ann.rpt" "TEMP_upLegRHTPointer_ann_parentConstraint1.crt"
		;
connectAttr "TEMP_hip_tmpLoc.t" "TEMP_upLegRHTPointer_ann_parentConstraint1.tg[0].tt"
		;
connectAttr "TEMP_hip_tmpLoc.rp" "TEMP_upLegRHTPointer_ann_parentConstraint1.tg[0].trp"
		;
connectAttr "TEMP_hip_tmpLoc.rpt" "TEMP_upLegRHTPointer_ann_parentConstraint1.tg[0].trt"
		;
connectAttr "TEMP_hip_tmpLoc.r" "TEMP_upLegRHTPointer_ann_parentConstraint1.tg[0].tr"
		;
connectAttr "TEMP_hip_tmpLoc.ro" "TEMP_upLegRHTPointer_ann_parentConstraint1.tg[0].tro"
		;
connectAttr "TEMP_hip_tmpLoc.s" "TEMP_upLegRHTPointer_ann_parentConstraint1.tg[0].ts"
		;
connectAttr "TEMP_hip_tmpLoc.pm" "TEMP_upLegRHTPointer_ann_parentConstraint1.tg[0].tpm"
		;
connectAttr "TEMP_upLegRHTPointer_ann_parentConstraint1.w0" "TEMP_upLegRHTPointer_ann_parentConstraint1.tg[0].tw"
		;
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "TEMP_upLegLFT_tmpLoc.tx" "TEMP_skirtPosLegMirror_mdl.i1";
connectAttr "TEMP_tempRoot_ctrl.nodes" "TEMP_skirtPosLegMirror_mdl.tempRoot";
connectAttr "cluster16GroupParts.og" "TEMP_skirtLoft16UP_clsHndlCluster.ip[0].ig"
		;
connectAttr "cluster16GroupId.id" "TEMP_skirtLoft16UP_clsHndlCluster.ip[0].gi";
connectAttr "TEMP_skirtLoft16UP_clsHndl.wm" "TEMP_skirtLoft16UP_clsHndlCluster.ma"
		;
connectAttr "TEMP_skirtLoft16UP_clsHndlShape.x" "TEMP_skirtLoft16UP_clsHndlCluster.x"
		;
connectAttr "TEMP_skirtLoft15UP_clsHndlCluster.og[0]" "cluster16GroupParts.ig";
connectAttr "cluster16GroupId.id" "cluster16GroupParts.gi";
connectAttr "cluster16GroupId.msg" "cluster16Set.gn" -na;
connectAttr "TEMP_skirtLoftUP_crvShape.iog.og[18]" "cluster16Set.dsm" -na;
connectAttr "TEMP_skirtLoft16UP_clsHndlCluster.msg" "cluster16Set.ub[0]";
connectAttr "cluster15GroupParts.og" "TEMP_skirtLoft15UP_clsHndlCluster.ip[0].ig"
		;
connectAttr "cluster15GroupId.id" "TEMP_skirtLoft15UP_clsHndlCluster.ip[0].gi";
connectAttr "TEMP_skirtLoft15UP_clsHndl.wm" "TEMP_skirtLoft15UP_clsHndlCluster.ma"
		;
connectAttr "TEMP_skirtLoft15UP_clsHndlShape.x" "TEMP_skirtLoft15UP_clsHndlCluster.x"
		;
connectAttr "TEMP_skirtLoft14UP_clsHndlCluster.og[0]" "cluster15GroupParts.ig";
connectAttr "cluster15GroupId.id" "cluster15GroupParts.gi";
connectAttr "cluster15GroupId.msg" "cluster15Set.gn" -na;
connectAttr "TEMP_skirtLoftUP_crvShape.iog.og[17]" "cluster15Set.dsm" -na;
connectAttr "TEMP_skirtLoft15UP_clsHndlCluster.msg" "cluster15Set.ub[0]";
connectAttr "cluster14GroupParts.og" "TEMP_skirtLoft14UP_clsHndlCluster.ip[0].ig"
		;
connectAttr "cluster14GroupId.id" "TEMP_skirtLoft14UP_clsHndlCluster.ip[0].gi";
connectAttr "TEMP_skirtLoft14UP_clsHndl.wm" "TEMP_skirtLoft14UP_clsHndlCluster.ma"
		;
connectAttr "TEMP_skirtLoft14UP_clsHndlShape.x" "TEMP_skirtLoft14UP_clsHndlCluster.x"
		;
connectAttr "TEMP_skirtLoft13UP_clsHndlCluster.og[0]" "cluster14GroupParts.ig";
connectAttr "cluster14GroupId.id" "cluster14GroupParts.gi";
connectAttr "cluster14GroupId.msg" "cluster14Set.gn" -na;
connectAttr "TEMP_skirtLoftUP_crvShape.iog.og[16]" "cluster14Set.dsm" -na;
connectAttr "TEMP_skirtLoft14UP_clsHndlCluster.msg" "cluster14Set.ub[0]";
connectAttr "cluster13GroupParts.og" "TEMP_skirtLoft13UP_clsHndlCluster.ip[0].ig"
		;
connectAttr "cluster13GroupId.id" "TEMP_skirtLoft13UP_clsHndlCluster.ip[0].gi";
connectAttr "TEMP_skirtLoft13UP_clsHndl.wm" "TEMP_skirtLoft13UP_clsHndlCluster.ma"
		;
connectAttr "TEMP_skirtLoft13UP_clsHndlShape.x" "TEMP_skirtLoft13UP_clsHndlCluster.x"
		;
connectAttr "TEMP_skirtLoft12UP_clsHndlCluster.og[0]" "cluster13GroupParts.ig";
connectAttr "cluster13GroupId.id" "cluster13GroupParts.gi";
connectAttr "cluster13GroupId.msg" "cluster13Set.gn" -na;
connectAttr "TEMP_skirtLoftUP_crvShape.iog.og[15]" "cluster13Set.dsm" -na;
connectAttr "TEMP_skirtLoft13UP_clsHndlCluster.msg" "cluster13Set.ub[0]";
connectAttr "cluster12GroupParts.og" "TEMP_skirtLoft12UP_clsHndlCluster.ip[0].ig"
		;
connectAttr "cluster12GroupId.id" "TEMP_skirtLoft12UP_clsHndlCluster.ip[0].gi";
connectAttr "TEMP_skirtLoft12UP_clsHndl.wm" "TEMP_skirtLoft12UP_clsHndlCluster.ma"
		;
connectAttr "TEMP_skirtLoft12UP_clsHndlShape.x" "TEMP_skirtLoft12UP_clsHndlCluster.x"
		;
connectAttr "TEMP_skirtLoft11UP_clsHndlCluster.og[0]" "cluster12GroupParts.ig";
connectAttr "cluster12GroupId.id" "cluster12GroupParts.gi";
connectAttr "cluster12GroupId.msg" "cluster12Set.gn" -na;
connectAttr "TEMP_skirtLoftUP_crvShape.iog.og[14]" "cluster12Set.dsm" -na;
connectAttr "TEMP_skirtLoft12UP_clsHndlCluster.msg" "cluster12Set.ub[0]";
connectAttr "cluster11GroupParts.og" "TEMP_skirtLoft11UP_clsHndlCluster.ip[0].ig"
		;
connectAttr "cluster11GroupId.id" "TEMP_skirtLoft11UP_clsHndlCluster.ip[0].gi";
connectAttr "TEMP_skirtLoft11UP_clsHndl.wm" "TEMP_skirtLoft11UP_clsHndlCluster.ma"
		;
connectAttr "TEMP_skirtLoft11UP_clsHndlShape.x" "TEMP_skirtLoft11UP_clsHndlCluster.x"
		;
connectAttr "TEMP_skirtLoft10UP_clsHndlCluster.og[0]" "cluster11GroupParts.ig";
connectAttr "cluster11GroupId.id" "cluster11GroupParts.gi";
connectAttr "cluster11GroupId.msg" "cluster11Set.gn" -na;
connectAttr "TEMP_skirtLoftUP_crvShape.iog.og[13]" "cluster11Set.dsm" -na;
connectAttr "TEMP_skirtLoft11UP_clsHndlCluster.msg" "cluster11Set.ub[0]";
connectAttr "cluster10GroupParts.og" "TEMP_skirtLoft10UP_clsHndlCluster.ip[0].ig"
		;
connectAttr "cluster10GroupId.id" "TEMP_skirtLoft10UP_clsHndlCluster.ip[0].gi";
connectAttr "TEMP_skirtLoft10UP_clsHndl.wm" "TEMP_skirtLoft10UP_clsHndlCluster.ma"
		;
connectAttr "TEMP_skirtLoft10UP_clsHndlShape.x" "TEMP_skirtLoft10UP_clsHndlCluster.x"
		;
connectAttr "TEMP_skirtLoft09UP_clsHndlCluster.og[0]" "cluster10GroupParts.ig";
connectAttr "cluster10GroupId.id" "cluster10GroupParts.gi";
connectAttr "cluster10GroupId.msg" "cluster10Set.gn" -na;
connectAttr "TEMP_skirtLoftUP_crvShape.iog.og[12]" "cluster10Set.dsm" -na;
connectAttr "TEMP_skirtLoft10UP_clsHndlCluster.msg" "cluster10Set.ub[0]";
connectAttr "cluster9GroupParts.og" "TEMP_skirtLoft09UP_clsHndlCluster.ip[0].ig"
		;
connectAttr "cluster9GroupId.id" "TEMP_skirtLoft09UP_clsHndlCluster.ip[0].gi";
connectAttr "TEMP_skirtLoft09UP_clsHndl.wm" "TEMP_skirtLoft09UP_clsHndlCluster.ma"
		;
connectAttr "TEMP_skirtLoft09UP_clsHndlShape.x" "TEMP_skirtLoft09UP_clsHndlCluster.x"
		;
connectAttr "TEMP_skirtLoft08UP_clsHndlCluster.og[0]" "cluster9GroupParts.ig";
connectAttr "cluster9GroupId.id" "cluster9GroupParts.gi";
connectAttr "cluster9GroupId.msg" "cluster9Set.gn" -na;
connectAttr "TEMP_skirtLoftUP_crvShape.iog.og[11]" "cluster9Set.dsm" -na;
connectAttr "TEMP_skirtLoft09UP_clsHndlCluster.msg" "cluster9Set.ub[0]";
connectAttr "cluster8GroupParts.og" "TEMP_skirtLoft08UP_clsHndlCluster.ip[0].ig"
		;
connectAttr "cluster8GroupId.id" "TEMP_skirtLoft08UP_clsHndlCluster.ip[0].gi";
connectAttr "TEMP_skirtLoft08UP_clsHndl.wm" "TEMP_skirtLoft08UP_clsHndlCluster.ma"
		;
connectAttr "TEMP_skirtLoft08UP_clsHndlShape.x" "TEMP_skirtLoft08UP_clsHndlCluster.x"
		;
connectAttr "TEMP_skirtLoft07UP_clsHndlCluster.og[0]" "cluster8GroupParts.ig";
connectAttr "cluster8GroupId.id" "cluster8GroupParts.gi";
connectAttr "cluster8GroupId.msg" "cluster8Set.gn" -na;
connectAttr "TEMP_skirtLoftUP_crvShape.iog.og[10]" "cluster8Set.dsm" -na;
connectAttr "TEMP_skirtLoft08UP_clsHndlCluster.msg" "cluster8Set.ub[0]";
connectAttr "cluster7GroupParts.og" "TEMP_skirtLoft07UP_clsHndlCluster.ip[0].ig"
		;
connectAttr "cluster7GroupId.id" "TEMP_skirtLoft07UP_clsHndlCluster.ip[0].gi";
connectAttr "TEMP_skirtLoft07UP_clsHndl.wm" "TEMP_skirtLoft07UP_clsHndlCluster.ma"
		;
connectAttr "TEMP_skirtLoft07UP_clsHndlShape.x" "TEMP_skirtLoft07UP_clsHndlCluster.x"
		;
connectAttr "TEMP_skirtLoft06UP_clsHndlCluster.og[0]" "cluster7GroupParts.ig";
connectAttr "cluster7GroupId.id" "cluster7GroupParts.gi";
connectAttr "cluster7GroupId.msg" "cluster7Set.gn" -na;
connectAttr "TEMP_skirtLoftUP_crvShape.iog.og[9]" "cluster7Set.dsm" -na;
connectAttr "TEMP_skirtLoft07UP_clsHndlCluster.msg" "cluster7Set.ub[0]";
connectAttr "cluster6GroupParts.og" "TEMP_skirtLoft06UP_clsHndlCluster.ip[0].ig"
		;
connectAttr "cluster6GroupId.id" "TEMP_skirtLoft06UP_clsHndlCluster.ip[0].gi";
connectAttr "TEMP_skirtLoft06UP_clsHndl.wm" "TEMP_skirtLoft06UP_clsHndlCluster.ma"
		;
connectAttr "TEMP_skirtLoft06UP_clsHndlShape.x" "TEMP_skirtLoft06UP_clsHndlCluster.x"
		;
connectAttr "TEMP_skirtLoft05UP_clsHndlCluster.og[0]" "cluster6GroupParts.ig";
connectAttr "cluster6GroupId.id" "cluster6GroupParts.gi";
connectAttr "cluster6GroupId.msg" "cluster6Set.gn" -na;
connectAttr "TEMP_skirtLoftUP_crvShape.iog.og[8]" "cluster6Set.dsm" -na;
connectAttr "TEMP_skirtLoft06UP_clsHndlCluster.msg" "cluster6Set.ub[0]";
connectAttr "cluster5GroupParts.og" "TEMP_skirtLoft05UP_clsHndlCluster.ip[0].ig"
		;
connectAttr "cluster5GroupId.id" "TEMP_skirtLoft05UP_clsHndlCluster.ip[0].gi";
connectAttr "TEMP_skirtLoft05UP_clsHndl.wm" "TEMP_skirtLoft05UP_clsHndlCluster.ma"
		;
connectAttr "TEMP_skirtLoft05UP_clsHndlShape.x" "TEMP_skirtLoft05UP_clsHndlCluster.x"
		;
connectAttr "TEMP_skirtLoft04UP_clsHndlCluster.og[0]" "cluster5GroupParts.ig";
connectAttr "cluster5GroupId.id" "cluster5GroupParts.gi";
connectAttr "cluster5GroupId.msg" "cluster5Set.gn" -na;
connectAttr "TEMP_skirtLoftUP_crvShape.iog.og[7]" "cluster5Set.dsm" -na;
connectAttr "TEMP_skirtLoft05UP_clsHndlCluster.msg" "cluster5Set.ub[0]";
connectAttr "cluster4GroupParts.og" "TEMP_skirtLoft04UP_clsHndlCluster.ip[0].ig"
		;
connectAttr "cluster4GroupId.id" "TEMP_skirtLoft04UP_clsHndlCluster.ip[0].gi";
connectAttr "TEMP_skirtLoft04UP_clsHndl.wm" "TEMP_skirtLoft04UP_clsHndlCluster.ma"
		;
connectAttr "TEMP_skirtLoft04UP_clsHndlShape.x" "TEMP_skirtLoft04UP_clsHndlCluster.x"
		;
connectAttr "TEMP_skirtLoft03UP_clsHndlCluster.og[0]" "cluster4GroupParts.ig";
connectAttr "cluster4GroupId.id" "cluster4GroupParts.gi";
connectAttr "cluster4GroupId.msg" "cluster4Set.gn" -na;
connectAttr "TEMP_skirtLoftUP_crvShape.iog.og[6]" "cluster4Set.dsm" -na;
connectAttr "TEMP_skirtLoft04UP_clsHndlCluster.msg" "cluster4Set.ub[0]";
connectAttr "cluster3GroupParts.og" "TEMP_skirtLoft03UP_clsHndlCluster.ip[0].ig"
		;
connectAttr "cluster3GroupId.id" "TEMP_skirtLoft03UP_clsHndlCluster.ip[0].gi";
connectAttr "TEMP_skirtLoft03UP_clsHndl.wm" "TEMP_skirtLoft03UP_clsHndlCluster.ma"
		;
connectAttr "TEMP_skirtLoft03UP_clsHndlShape.x" "TEMP_skirtLoft03UP_clsHndlCluster.x"
		;
connectAttr "TEMP_skirtLoft02UP_clsHndlCluster.og[0]" "cluster3GroupParts.ig";
connectAttr "cluster3GroupId.id" "cluster3GroupParts.gi";
connectAttr "cluster3GroupId.msg" "cluster3Set.gn" -na;
connectAttr "TEMP_skirtLoftUP_crvShape.iog.og[5]" "cluster3Set.dsm" -na;
connectAttr "TEMP_skirtLoft03UP_clsHndlCluster.msg" "cluster3Set.ub[0]";
connectAttr "cluster2GroupParts.og" "TEMP_skirtLoft02UP_clsHndlCluster.ip[0].ig"
		;
connectAttr "cluster2GroupId.id" "TEMP_skirtLoft02UP_clsHndlCluster.ip[0].gi";
connectAttr "TEMP_skirtLoft02UP_clsHndl.wm" "TEMP_skirtLoft02UP_clsHndlCluster.ma"
		;
connectAttr "TEMP_skirtLoft02UP_clsHndlShape.x" "TEMP_skirtLoft02UP_clsHndlCluster.x"
		;
connectAttr "TEMP_skirtLoft01UP_clsHndlCluster.og[0]" "cluster2GroupParts.ig";
connectAttr "cluster2GroupId.id" "cluster2GroupParts.gi";
connectAttr "cluster2GroupId.msg" "cluster2Set.gn" -na;
connectAttr "TEMP_skirtLoftUP_crvShape.iog.og[4]" "cluster2Set.dsm" -na;
connectAttr "TEMP_skirtLoft02UP_clsHndlCluster.msg" "cluster2Set.ub[0]";
connectAttr "cluster1GroupParts.og" "TEMP_skirtLoft01UP_clsHndlCluster.ip[0].ig"
		;
connectAttr "cluster1GroupId.id" "TEMP_skirtLoft01UP_clsHndlCluster.ip[0].gi";
connectAttr "TEMP_skirtLoft01UP_clsHndl.wm" "TEMP_skirtLoft01UP_clsHndlCluster.ma"
		;
connectAttr "TEMP_skirtLoft01UP_clsHndlShape.x" "TEMP_skirtLoft01UP_clsHndlCluster.x"
		;
connectAttr "cluster1GroupId.msg" "cluster1Set.gn" -na;
connectAttr "TEMP_skirtLoftUP_crvShape.iog.og[2]" "cluster1Set.dsm" -na;
connectAttr "TEMP_skirtLoft01UP_clsHndlCluster.msg" "cluster1Set.ub[0]";
connectAttr "TEMP_skirtLoftUP_crvShapeOrig.ws" "groupParts2.ig";
connectAttr "groupId2.id" "groupParts2.gi";
connectAttr "groupParts2.og" "tweak1.ip[0].ig";
connectAttr "groupId2.id" "tweak1.ip[0].gi";
connectAttr "groupId2.msg" "tweakSet1.gn" -na;
connectAttr "TEMP_skirtLoftUP_crvShape.iog.og[3]" "tweakSet1.dsm" -na;
connectAttr "tweak1.msg" "tweakSet1.ub[0]";
connectAttr "tweak1.og[0]" "cluster1GroupParts.ig";
connectAttr "cluster1GroupId.id" "cluster1GroupParts.gi";
connectAttr "cluster35GroupParts.og" "TEMP_skirtLoft16MID_clsHndlCluster.ip[0].ig"
		;
connectAttr "cluster35GroupId.id" "TEMP_skirtLoft16MID_clsHndlCluster.ip[0].gi";
connectAttr "TEMP_skirtLoft16MID_clsHndl.wm" "TEMP_skirtLoft16MID_clsHndlCluster.ma"
		;
connectAttr "TEMP_skirtLoft16MID_clsHndlShape.x" "TEMP_skirtLoft16MID_clsHndlCluster.x"
		;
connectAttr "TEMP_skirtLoft15MID_clsHndlCluster.og[0]" "cluster35GroupParts.ig";
connectAttr "cluster35GroupId.id" "cluster35GroupParts.gi";
connectAttr "cluster35GroupId.msg" "cluster35Set.gn" -na;
connectAttr "TEMP_skirtLoftMID_crvShape.iog.og[18]" "cluster35Set.dsm" -na;
connectAttr "TEMP_skirtLoft16MID_clsHndlCluster.msg" "cluster35Set.ub[0]";
connectAttr "cluster34GroupParts.og" "TEMP_skirtLoft15MID_clsHndlCluster.ip[0].ig"
		;
connectAttr "cluster34GroupId.id" "TEMP_skirtLoft15MID_clsHndlCluster.ip[0].gi";
connectAttr "TEMP_skirtLoft15MID_clsHndl.wm" "TEMP_skirtLoft15MID_clsHndlCluster.ma"
		;
connectAttr "TEMP_skirtLoft15MID_clsHndlShape.x" "TEMP_skirtLoft15MID_clsHndlCluster.x"
		;
connectAttr "TEMP_skirtLoft14MID_clsHndlCluster.og[0]" "cluster34GroupParts.ig";
connectAttr "cluster34GroupId.id" "cluster34GroupParts.gi";
connectAttr "cluster34GroupId.msg" "cluster34Set.gn" -na;
connectAttr "TEMP_skirtLoftMID_crvShape.iog.og[17]" "cluster34Set.dsm" -na;
connectAttr "TEMP_skirtLoft15MID_clsHndlCluster.msg" "cluster34Set.ub[0]";
connectAttr "cluster33GroupParts.og" "TEMP_skirtLoft14MID_clsHndlCluster.ip[0].ig"
		;
connectAttr "cluster33GroupId.id" "TEMP_skirtLoft14MID_clsHndlCluster.ip[0].gi";
connectAttr "TEMP_skirtLoft14MID_clsHndl.wm" "TEMP_skirtLoft14MID_clsHndlCluster.ma"
		;
connectAttr "TEMP_skirtLoft14MID_clsHndlShape.x" "TEMP_skirtLoft14MID_clsHndlCluster.x"
		;
connectAttr "TEMP_skirtLoft13MID_clsHndlCluster.og[0]" "cluster33GroupParts.ig";
connectAttr "cluster33GroupId.id" "cluster33GroupParts.gi";
connectAttr "cluster33GroupId.msg" "cluster33Set.gn" -na;
connectAttr "TEMP_skirtLoftMID_crvShape.iog.og[16]" "cluster33Set.dsm" -na;
connectAttr "TEMP_skirtLoft14MID_clsHndlCluster.msg" "cluster33Set.ub[0]";
connectAttr "cluster32GroupParts.og" "TEMP_skirtLoft13MID_clsHndlCluster.ip[0].ig"
		;
connectAttr "cluster32GroupId.id" "TEMP_skirtLoft13MID_clsHndlCluster.ip[0].gi";
connectAttr "TEMP_skirtLoft13MID_clsHndl.wm" "TEMP_skirtLoft13MID_clsHndlCluster.ma"
		;
connectAttr "TEMP_skirtLoft13MID_clsHndlShape.x" "TEMP_skirtLoft13MID_clsHndlCluster.x"
		;
connectAttr "TEMP_skirtLoft12MID_clsHndlCluster.og[0]" "cluster32GroupParts.ig";
connectAttr "cluster32GroupId.id" "cluster32GroupParts.gi";
connectAttr "cluster32GroupId.msg" "cluster32Set.gn" -na;
connectAttr "TEMP_skirtLoftMID_crvShape.iog.og[15]" "cluster32Set.dsm" -na;
connectAttr "TEMP_skirtLoft13MID_clsHndlCluster.msg" "cluster32Set.ub[0]";
connectAttr "cluster31GroupParts.og" "TEMP_skirtLoft12MID_clsHndlCluster.ip[0].ig"
		;
connectAttr "cluster31GroupId.id" "TEMP_skirtLoft12MID_clsHndlCluster.ip[0].gi";
connectAttr "TEMP_skirtLoft12MID_clsHndl.wm" "TEMP_skirtLoft12MID_clsHndlCluster.ma"
		;
connectAttr "TEMP_skirtLoft12MID_clsHndlShape.x" "TEMP_skirtLoft12MID_clsHndlCluster.x"
		;
connectAttr "TEMP_skirtLoft11MID_clsHndlCluster.og[0]" "cluster31GroupParts.ig";
connectAttr "cluster31GroupId.id" "cluster31GroupParts.gi";
connectAttr "cluster31GroupId.msg" "cluster31Set.gn" -na;
connectAttr "TEMP_skirtLoftMID_crvShape.iog.og[14]" "cluster31Set.dsm" -na;
connectAttr "TEMP_skirtLoft12MID_clsHndlCluster.msg" "cluster31Set.ub[0]";
connectAttr "cluster30GroupParts.og" "TEMP_skirtLoft11MID_clsHndlCluster.ip[0].ig"
		;
connectAttr "cluster30GroupId.id" "TEMP_skirtLoft11MID_clsHndlCluster.ip[0].gi";
connectAttr "TEMP_skirtLoft11MID_clsHndl.wm" "TEMP_skirtLoft11MID_clsHndlCluster.ma"
		;
connectAttr "TEMP_skirtLoft11MID_clsHndlShape.x" "TEMP_skirtLoft11MID_clsHndlCluster.x"
		;
connectAttr "TEMP_skirtLoft10MID_clsHndlCluster.og[0]" "cluster30GroupParts.ig";
connectAttr "cluster30GroupId.id" "cluster30GroupParts.gi";
connectAttr "cluster30GroupId.msg" "cluster30Set.gn" -na;
connectAttr "TEMP_skirtLoftMID_crvShape.iog.og[13]" "cluster30Set.dsm" -na;
connectAttr "TEMP_skirtLoft11MID_clsHndlCluster.msg" "cluster30Set.ub[0]";
connectAttr "cluster29GroupParts.og" "TEMP_skirtLoft10MID_clsHndlCluster.ip[0].ig"
		;
connectAttr "cluster29GroupId.id" "TEMP_skirtLoft10MID_clsHndlCluster.ip[0].gi";
connectAttr "TEMP_skirtLoft10MID_clsHndl.wm" "TEMP_skirtLoft10MID_clsHndlCluster.ma"
		;
connectAttr "TEMP_skirtLoft10MID_clsHndlShape.x" "TEMP_skirtLoft10MID_clsHndlCluster.x"
		;
connectAttr "TEMP_skirtLoft09MID_clsHndlCluster.og[0]" "cluster29GroupParts.ig";
connectAttr "cluster29GroupId.id" "cluster29GroupParts.gi";
connectAttr "cluster29GroupId.msg" "cluster29Set.gn" -na;
connectAttr "TEMP_skirtLoftMID_crvShape.iog.og[12]" "cluster29Set.dsm" -na;
connectAttr "TEMP_skirtLoft10MID_clsHndlCluster.msg" "cluster29Set.ub[0]";
connectAttr "cluster28GroupParts.og" "TEMP_skirtLoft09MID_clsHndlCluster.ip[0].ig"
		;
connectAttr "cluster28GroupId.id" "TEMP_skirtLoft09MID_clsHndlCluster.ip[0].gi";
connectAttr "TEMP_skirtLoft09MID_clsHndl.wm" "TEMP_skirtLoft09MID_clsHndlCluster.ma"
		;
connectAttr "TEMP_skirtLoft09MID_clsHndlShape.x" "TEMP_skirtLoft09MID_clsHndlCluster.x"
		;
connectAttr "TEMP_skirtLoft08MID_clsHndlCluster.og[0]" "cluster28GroupParts.ig";
connectAttr "cluster28GroupId.id" "cluster28GroupParts.gi";
connectAttr "cluster28GroupId.msg" "cluster28Set.gn" -na;
connectAttr "TEMP_skirtLoftMID_crvShape.iog.og[11]" "cluster28Set.dsm" -na;
connectAttr "TEMP_skirtLoft09MID_clsHndlCluster.msg" "cluster28Set.ub[0]";
connectAttr "cluster27GroupParts.og" "TEMP_skirtLoft08MID_clsHndlCluster.ip[0].ig"
		;
connectAttr "cluster27GroupId.id" "TEMP_skirtLoft08MID_clsHndlCluster.ip[0].gi";
connectAttr "TEMP_skirtLoft08MID_clsHndl.wm" "TEMP_skirtLoft08MID_clsHndlCluster.ma"
		;
connectAttr "TEMP_skirtLoft08MID_clsHndlShape.x" "TEMP_skirtLoft08MID_clsHndlCluster.x"
		;
connectAttr "TEMP_skirtLoft07MID_clsHndlCluster.og[0]" "cluster27GroupParts.ig";
connectAttr "cluster27GroupId.id" "cluster27GroupParts.gi";
connectAttr "cluster27GroupId.msg" "cluster27Set.gn" -na;
connectAttr "TEMP_skirtLoftMID_crvShape.iog.og[10]" "cluster27Set.dsm" -na;
connectAttr "TEMP_skirtLoft08MID_clsHndlCluster.msg" "cluster27Set.ub[0]";
connectAttr "cluster26GroupParts.og" "TEMP_skirtLoft07MID_clsHndlCluster.ip[0].ig"
		;
connectAttr "cluster26GroupId.id" "TEMP_skirtLoft07MID_clsHndlCluster.ip[0].gi";
connectAttr "TEMP_skirtLoft07MID_clsHndl.wm" "TEMP_skirtLoft07MID_clsHndlCluster.ma"
		;
connectAttr "TEMP_skirtLoft07MID_clsHndlShape.x" "TEMP_skirtLoft07MID_clsHndlCluster.x"
		;
connectAttr "TEMP_skirtLoft06MID_clsHndlCluster.og[0]" "cluster26GroupParts.ig";
connectAttr "cluster26GroupId.id" "cluster26GroupParts.gi";
connectAttr "cluster26GroupId.msg" "cluster26Set.gn" -na;
connectAttr "TEMP_skirtLoftMID_crvShape.iog.og[9]" "cluster26Set.dsm" -na;
connectAttr "TEMP_skirtLoft07MID_clsHndlCluster.msg" "cluster26Set.ub[0]";
connectAttr "cluster25GroupParts.og" "TEMP_skirtLoft06MID_clsHndlCluster.ip[0].ig"
		;
connectAttr "cluster25GroupId.id" "TEMP_skirtLoft06MID_clsHndlCluster.ip[0].gi";
connectAttr "TEMP_skirtLoft06MID_clsHndl.wm" "TEMP_skirtLoft06MID_clsHndlCluster.ma"
		;
connectAttr "TEMP_skirtLoft06MID_clsHndlShape.x" "TEMP_skirtLoft06MID_clsHndlCluster.x"
		;
connectAttr "TEMP_skirtLoft05MID_clsHndlCluster.og[0]" "cluster25GroupParts.ig";
connectAttr "cluster25GroupId.id" "cluster25GroupParts.gi";
connectAttr "cluster25GroupId.msg" "cluster25Set.gn" -na;
connectAttr "TEMP_skirtLoftMID_crvShape.iog.og[8]" "cluster25Set.dsm" -na;
connectAttr "TEMP_skirtLoft06MID_clsHndlCluster.msg" "cluster25Set.ub[0]";
connectAttr "cluster24GroupParts.og" "TEMP_skirtLoft05MID_clsHndlCluster.ip[0].ig"
		;
connectAttr "cluster24GroupId.id" "TEMP_skirtLoft05MID_clsHndlCluster.ip[0].gi";
connectAttr "TEMP_skirtLoft05MID_clsHndl.wm" "TEMP_skirtLoft05MID_clsHndlCluster.ma"
		;
connectAttr "TEMP_skirtLoft05MID_clsHndlShape.x" "TEMP_skirtLoft05MID_clsHndlCluster.x"
		;
connectAttr "TEMP_skirtLoft04MID_clsHndlCluster.og[0]" "cluster24GroupParts.ig";
connectAttr "cluster24GroupId.id" "cluster24GroupParts.gi";
connectAttr "cluster24GroupId.msg" "cluster24Set.gn" -na;
connectAttr "TEMP_skirtLoftMID_crvShape.iog.og[7]" "cluster24Set.dsm" -na;
connectAttr "TEMP_skirtLoft05MID_clsHndlCluster.msg" "cluster24Set.ub[0]";
connectAttr "cluster23GroupParts.og" "TEMP_skirtLoft04MID_clsHndlCluster.ip[0].ig"
		;
connectAttr "cluster23GroupId.id" "TEMP_skirtLoft04MID_clsHndlCluster.ip[0].gi";
connectAttr "TEMP_skirtLoft04MID_clsHndl.wm" "TEMP_skirtLoft04MID_clsHndlCluster.ma"
		;
connectAttr "TEMP_skirtLoft04MID_clsHndlShape.x" "TEMP_skirtLoft04MID_clsHndlCluster.x"
		;
connectAttr "TEMP_skirtLoft03MID_clsHndlCluster.og[0]" "cluster23GroupParts.ig";
connectAttr "cluster23GroupId.id" "cluster23GroupParts.gi";
connectAttr "cluster23GroupId.msg" "cluster23Set.gn" -na;
connectAttr "TEMP_skirtLoftMID_crvShape.iog.og[6]" "cluster23Set.dsm" -na;
connectAttr "TEMP_skirtLoft04MID_clsHndlCluster.msg" "cluster23Set.ub[0]";
connectAttr "cluster22GroupParts.og" "TEMP_skirtLoft03MID_clsHndlCluster.ip[0].ig"
		;
connectAttr "cluster22GroupId.id" "TEMP_skirtLoft03MID_clsHndlCluster.ip[0].gi";
connectAttr "TEMP_skirtLoft03MID_clsHndl.wm" "TEMP_skirtLoft03MID_clsHndlCluster.ma"
		;
connectAttr "TEMP_skirtLoft03MID_clsHndlShape.x" "TEMP_skirtLoft03MID_clsHndlCluster.x"
		;
connectAttr "TEMP_skirtLoft02MID_clsHndlCluster.og[0]" "cluster22GroupParts.ig";
connectAttr "cluster22GroupId.id" "cluster22GroupParts.gi";
connectAttr "cluster22GroupId.msg" "cluster22Set.gn" -na;
connectAttr "TEMP_skirtLoftMID_crvShape.iog.og[5]" "cluster22Set.dsm" -na;
connectAttr "TEMP_skirtLoft03MID_clsHndlCluster.msg" "cluster22Set.ub[0]";
connectAttr "cluster21GroupParts.og" "TEMP_skirtLoft02MID_clsHndlCluster.ip[0].ig"
		;
connectAttr "cluster21GroupId.id" "TEMP_skirtLoft02MID_clsHndlCluster.ip[0].gi";
connectAttr "TEMP_skirtLoft02MID_clsHndl.wm" "TEMP_skirtLoft02MID_clsHndlCluster.ma"
		;
connectAttr "TEMP_skirtLoft02MID_clsHndlShape.x" "TEMP_skirtLoft02MID_clsHndlCluster.x"
		;
connectAttr "TEMP_skirtLoft01MID_clsHndlCluster.og[0]" "cluster21GroupParts.ig";
connectAttr "cluster21GroupId.id" "cluster21GroupParts.gi";
connectAttr "cluster21GroupId.msg" "cluster21Set.gn" -na;
connectAttr "TEMP_skirtLoftMID_crvShape.iog.og[4]" "cluster21Set.dsm" -na;
connectAttr "TEMP_skirtLoft02MID_clsHndlCluster.msg" "cluster21Set.ub[0]";
connectAttr "cluster20GroupParts.og" "TEMP_skirtLoft01MID_clsHndlCluster.ip[0].ig"
		;
connectAttr "cluster20GroupId.id" "TEMP_skirtLoft01MID_clsHndlCluster.ip[0].gi";
connectAttr "TEMP_skirtLoft01MID_clsHndl.wm" "TEMP_skirtLoft01MID_clsHndlCluster.ma"
		;
connectAttr "TEMP_skirtLoft01MID_clsHndlShape.x" "TEMP_skirtLoft01MID_clsHndlCluster.x"
		;
connectAttr "cluster20GroupId.msg" "cluster20Set.gn" -na;
connectAttr "TEMP_skirtLoftMID_crvShape.iog.og[2]" "cluster20Set.dsm" -na;
connectAttr "TEMP_skirtLoft01MID_clsHndlCluster.msg" "cluster20Set.ub[0]";
connectAttr "TEMP_skirtLoftMID_crvShapeOrig.ws" "groupParts4.ig";
connectAttr "groupId4.id" "groupParts4.gi";
connectAttr "groupParts4.og" "tweak2.ip[0].ig";
connectAttr "groupId4.id" "tweak2.ip[0].gi";
connectAttr "groupId4.msg" "tweakSet2.gn" -na;
connectAttr "TEMP_skirtLoftMID_crvShape.iog.og[3]" "tweakSet2.dsm" -na;
connectAttr "tweak2.msg" "tweakSet2.ub[0]";
connectAttr "tweak2.og[0]" "cluster20GroupParts.ig";
connectAttr "cluster20GroupId.id" "cluster20GroupParts.gi";
connectAttr "cluster54GroupParts.og" "TEMP_skirtLoft16LO_clsHndlCluster.ip[0].ig"
		;
connectAttr "cluster54GroupId.id" "TEMP_skirtLoft16LO_clsHndlCluster.ip[0].gi";
connectAttr "TEMP_skirtLoft16LO_clsHndl.wm" "TEMP_skirtLoft16LO_clsHndlCluster.ma"
		;
connectAttr "TEMP_skirtLoft16LO_clsHndlShape.x" "TEMP_skirtLoft16LO_clsHndlCluster.x"
		;
connectAttr "TEMP_skirtLoft15LO_clsHndlCluster.og[0]" "cluster54GroupParts.ig";
connectAttr "cluster54GroupId.id" "cluster54GroupParts.gi";
connectAttr "cluster54GroupId.msg" "cluster54Set.gn" -na;
connectAttr "TEMP_skirtLoftLO_crvShape.iog.og[18]" "cluster54Set.dsm" -na;
connectAttr "TEMP_skirtLoft16LO_clsHndlCluster.msg" "cluster54Set.ub[0]";
connectAttr "cluster53GroupParts.og" "TEMP_skirtLoft15LO_clsHndlCluster.ip[0].ig"
		;
connectAttr "cluster53GroupId.id" "TEMP_skirtLoft15LO_clsHndlCluster.ip[0].gi";
connectAttr "TEMP_skirtLoft15LO_clsHndl.wm" "TEMP_skirtLoft15LO_clsHndlCluster.ma"
		;
connectAttr "TEMP_skirtLoft15LO_clsHndlShape.x" "TEMP_skirtLoft15LO_clsHndlCluster.x"
		;
connectAttr "TEMP_skirtLoft14LO_clsHndlCluster.og[0]" "cluster53GroupParts.ig";
connectAttr "cluster53GroupId.id" "cluster53GroupParts.gi";
connectAttr "cluster53GroupId.msg" "cluster53Set.gn" -na;
connectAttr "TEMP_skirtLoftLO_crvShape.iog.og[17]" "cluster53Set.dsm" -na;
connectAttr "TEMP_skirtLoft15LO_clsHndlCluster.msg" "cluster53Set.ub[0]";
connectAttr "cluster52GroupParts.og" "TEMP_skirtLoft14LO_clsHndlCluster.ip[0].ig"
		;
connectAttr "cluster52GroupId.id" "TEMP_skirtLoft14LO_clsHndlCluster.ip[0].gi";
connectAttr "TEMP_skirtLoft14LO_clsHndl.wm" "TEMP_skirtLoft14LO_clsHndlCluster.ma"
		;
connectAttr "TEMP_skirtLoft14LO_clsHndlShape.x" "TEMP_skirtLoft14LO_clsHndlCluster.x"
		;
connectAttr "TEMP_skirtLoft13LO_clsHndlCluster.og[0]" "cluster52GroupParts.ig";
connectAttr "cluster52GroupId.id" "cluster52GroupParts.gi";
connectAttr "cluster52GroupId.msg" "cluster52Set.gn" -na;
connectAttr "TEMP_skirtLoftLO_crvShape.iog.og[16]" "cluster52Set.dsm" -na;
connectAttr "TEMP_skirtLoft14LO_clsHndlCluster.msg" "cluster52Set.ub[0]";
connectAttr "cluster51GroupParts.og" "TEMP_skirtLoft13LO_clsHndlCluster.ip[0].ig"
		;
connectAttr "cluster51GroupId.id" "TEMP_skirtLoft13LO_clsHndlCluster.ip[0].gi";
connectAttr "TEMP_skirtLoft13LO_clsHndl.wm" "TEMP_skirtLoft13LO_clsHndlCluster.ma"
		;
connectAttr "TEMP_skirtLoft13LO_clsHndlShape.x" "TEMP_skirtLoft13LO_clsHndlCluster.x"
		;
connectAttr "TEMP_skirtLoft12LO_clsHndlCluster.og[0]" "cluster51GroupParts.ig";
connectAttr "cluster51GroupId.id" "cluster51GroupParts.gi";
connectAttr "cluster51GroupId.msg" "cluster51Set.gn" -na;
connectAttr "TEMP_skirtLoftLO_crvShape.iog.og[15]" "cluster51Set.dsm" -na;
connectAttr "TEMP_skirtLoft13LO_clsHndlCluster.msg" "cluster51Set.ub[0]";
connectAttr "cluster50GroupParts.og" "TEMP_skirtLoft12LO_clsHndlCluster.ip[0].ig"
		;
connectAttr "cluster50GroupId.id" "TEMP_skirtLoft12LO_clsHndlCluster.ip[0].gi";
connectAttr "TEMP_skirtLoft12LO_clsHndl.wm" "TEMP_skirtLoft12LO_clsHndlCluster.ma"
		;
connectAttr "TEMP_skirtLoft12LO_clsHndlShape.x" "TEMP_skirtLoft12LO_clsHndlCluster.x"
		;
connectAttr "TEMP_skirtLoft11LO_clsHndlCluster.og[0]" "cluster50GroupParts.ig";
connectAttr "cluster50GroupId.id" "cluster50GroupParts.gi";
connectAttr "cluster50GroupId.msg" "cluster50Set.gn" -na;
connectAttr "TEMP_skirtLoftLO_crvShape.iog.og[14]" "cluster50Set.dsm" -na;
connectAttr "TEMP_skirtLoft12LO_clsHndlCluster.msg" "cluster50Set.ub[0]";
connectAttr "cluster49GroupParts.og" "TEMP_skirtLoft11LO_clsHndlCluster.ip[0].ig"
		;
connectAttr "cluster49GroupId.id" "TEMP_skirtLoft11LO_clsHndlCluster.ip[0].gi";
connectAttr "TEMP_skirtLoft11LO_clsHndl.wm" "TEMP_skirtLoft11LO_clsHndlCluster.ma"
		;
connectAttr "TEMP_skirtLoft11LO_clsHndlShape.x" "TEMP_skirtLoft11LO_clsHndlCluster.x"
		;
connectAttr "TEMP_skirtLoft10LO_clsHndlCluster.og[0]" "cluster49GroupParts.ig";
connectAttr "cluster49GroupId.id" "cluster49GroupParts.gi";
connectAttr "cluster49GroupId.msg" "cluster49Set.gn" -na;
connectAttr "TEMP_skirtLoftLO_crvShape.iog.og[13]" "cluster49Set.dsm" -na;
connectAttr "TEMP_skirtLoft11LO_clsHndlCluster.msg" "cluster49Set.ub[0]";
connectAttr "cluster48GroupParts.og" "TEMP_skirtLoft10LO_clsHndlCluster.ip[0].ig"
		;
connectAttr "cluster48GroupId.id" "TEMP_skirtLoft10LO_clsHndlCluster.ip[0].gi";
connectAttr "TEMP_skirtLoft10LO_clsHndl.wm" "TEMP_skirtLoft10LO_clsHndlCluster.ma"
		;
connectAttr "TEMP_skirtLoft10LO_clsHndlShape.x" "TEMP_skirtLoft10LO_clsHndlCluster.x"
		;
connectAttr "TEMP_skirtLoft09LO_clsHndlCluster.og[0]" "cluster48GroupParts.ig";
connectAttr "cluster48GroupId.id" "cluster48GroupParts.gi";
connectAttr "cluster48GroupId.msg" "cluster48Set.gn" -na;
connectAttr "TEMP_skirtLoftLO_crvShape.iog.og[12]" "cluster48Set.dsm" -na;
connectAttr "TEMP_skirtLoft10LO_clsHndlCluster.msg" "cluster48Set.ub[0]";
connectAttr "cluster47GroupParts.og" "TEMP_skirtLoft09LO_clsHndlCluster.ip[0].ig"
		;
connectAttr "cluster47GroupId.id" "TEMP_skirtLoft09LO_clsHndlCluster.ip[0].gi";
connectAttr "TEMP_skirtLoft09LO_clsHndl.wm" "TEMP_skirtLoft09LO_clsHndlCluster.ma"
		;
connectAttr "TEMP_skirtLoft09LO_clsHndlShape.x" "TEMP_skirtLoft09LO_clsHndlCluster.x"
		;
connectAttr "TEMP_skirtLoft08LO_clsHndlCluster.og[0]" "cluster47GroupParts.ig";
connectAttr "cluster47GroupId.id" "cluster47GroupParts.gi";
connectAttr "cluster47GroupId.msg" "cluster47Set.gn" -na;
connectAttr "TEMP_skirtLoftLO_crvShape.iog.og[11]" "cluster47Set.dsm" -na;
connectAttr "TEMP_skirtLoft09LO_clsHndlCluster.msg" "cluster47Set.ub[0]";
connectAttr "cluster46GroupParts.og" "TEMP_skirtLoft08LO_clsHndlCluster.ip[0].ig"
		;
connectAttr "cluster46GroupId.id" "TEMP_skirtLoft08LO_clsHndlCluster.ip[0].gi";
connectAttr "TEMP_skirtLoft08LO_clsHndl.wm" "TEMP_skirtLoft08LO_clsHndlCluster.ma"
		;
connectAttr "TEMP_skirtLoft08LO_clsHndlShape.x" "TEMP_skirtLoft08LO_clsHndlCluster.x"
		;
connectAttr "TEMP_skirtLoft07LO_clsHndlCluster.og[0]" "cluster46GroupParts.ig";
connectAttr "cluster46GroupId.id" "cluster46GroupParts.gi";
connectAttr "cluster46GroupId.msg" "cluster46Set.gn" -na;
connectAttr "TEMP_skirtLoftLO_crvShape.iog.og[10]" "cluster46Set.dsm" -na;
connectAttr "TEMP_skirtLoft08LO_clsHndlCluster.msg" "cluster46Set.ub[0]";
connectAttr "cluster45GroupParts.og" "TEMP_skirtLoft07LO_clsHndlCluster.ip[0].ig"
		;
connectAttr "cluster45GroupId.id" "TEMP_skirtLoft07LO_clsHndlCluster.ip[0].gi";
connectAttr "TEMP_skirtLoft07LO_clsHndl.wm" "TEMP_skirtLoft07LO_clsHndlCluster.ma"
		;
connectAttr "TEMP_skirtLoft07LO_clsHndlShape.x" "TEMP_skirtLoft07LO_clsHndlCluster.x"
		;
connectAttr "TEMP_skirtLoft06LO_clsHndlCluster.og[0]" "cluster45GroupParts.ig";
connectAttr "cluster45GroupId.id" "cluster45GroupParts.gi";
connectAttr "cluster45GroupId.msg" "cluster45Set.gn" -na;
connectAttr "TEMP_skirtLoftLO_crvShape.iog.og[9]" "cluster45Set.dsm" -na;
connectAttr "TEMP_skirtLoft07LO_clsHndlCluster.msg" "cluster45Set.ub[0]";
connectAttr "cluster44GroupParts.og" "TEMP_skirtLoft06LO_clsHndlCluster.ip[0].ig"
		;
connectAttr "cluster44GroupId.id" "TEMP_skirtLoft06LO_clsHndlCluster.ip[0].gi";
connectAttr "TEMP_skirtLoft06LO_clsHndl.wm" "TEMP_skirtLoft06LO_clsHndlCluster.ma"
		;
connectAttr "TEMP_skirtLoft06LO_clsHndlShape.x" "TEMP_skirtLoft06LO_clsHndlCluster.x"
		;
connectAttr "TEMP_skirtLoft05LO_clsHndlCluster.og[0]" "cluster44GroupParts.ig";
connectAttr "cluster44GroupId.id" "cluster44GroupParts.gi";
connectAttr "cluster44GroupId.msg" "cluster44Set.gn" -na;
connectAttr "TEMP_skirtLoftLO_crvShape.iog.og[8]" "cluster44Set.dsm" -na;
connectAttr "TEMP_skirtLoft06LO_clsHndlCluster.msg" "cluster44Set.ub[0]";
connectAttr "cluster43GroupParts.og" "TEMP_skirtLoft05LO_clsHndlCluster.ip[0].ig"
		;
connectAttr "cluster43GroupId.id" "TEMP_skirtLoft05LO_clsHndlCluster.ip[0].gi";
connectAttr "TEMP_skirtLoft05LO_clsHndl.wm" "TEMP_skirtLoft05LO_clsHndlCluster.ma"
		;
connectAttr "TEMP_skirtLoft05LO_clsHndlShape.x" "TEMP_skirtLoft05LO_clsHndlCluster.x"
		;
connectAttr "TEMP_skirtLoft04LO_clsHndlCluster.og[0]" "cluster43GroupParts.ig";
connectAttr "cluster43GroupId.id" "cluster43GroupParts.gi";
connectAttr "cluster43GroupId.msg" "cluster43Set.gn" -na;
connectAttr "TEMP_skirtLoftLO_crvShape.iog.og[7]" "cluster43Set.dsm" -na;
connectAttr "TEMP_skirtLoft05LO_clsHndlCluster.msg" "cluster43Set.ub[0]";
connectAttr "cluster42GroupParts.og" "TEMP_skirtLoft04LO_clsHndlCluster.ip[0].ig"
		;
connectAttr "cluster42GroupId.id" "TEMP_skirtLoft04LO_clsHndlCluster.ip[0].gi";
connectAttr "TEMP_skirtLoft04LO_clsHndl.wm" "TEMP_skirtLoft04LO_clsHndlCluster.ma"
		;
connectAttr "TEMP_skirtLoft04LO_clsHndlShape.x" "TEMP_skirtLoft04LO_clsHndlCluster.x"
		;
connectAttr "TEMP_skirtLoft03LO_clsHndlCluster.og[0]" "cluster42GroupParts.ig";
connectAttr "cluster42GroupId.id" "cluster42GroupParts.gi";
connectAttr "cluster42GroupId.msg" "cluster42Set.gn" -na;
connectAttr "TEMP_skirtLoftLO_crvShape.iog.og[6]" "cluster42Set.dsm" -na;
connectAttr "TEMP_skirtLoft04LO_clsHndlCluster.msg" "cluster42Set.ub[0]";
connectAttr "cluster41GroupParts.og" "TEMP_skirtLoft03LO_clsHndlCluster.ip[0].ig"
		;
connectAttr "cluster41GroupId.id" "TEMP_skirtLoft03LO_clsHndlCluster.ip[0].gi";
connectAttr "TEMP_skirtLoft03LO_clsHndl.wm" "TEMP_skirtLoft03LO_clsHndlCluster.ma"
		;
connectAttr "TEMP_skirtLoft03LO_clsHndlShape.x" "TEMP_skirtLoft03LO_clsHndlCluster.x"
		;
connectAttr "TEMP_skirtLoft02LO_clsHndlCluster.og[0]" "cluster41GroupParts.ig";
connectAttr "cluster41GroupId.id" "cluster41GroupParts.gi";
connectAttr "cluster41GroupId.msg" "cluster41Set.gn" -na;
connectAttr "TEMP_skirtLoftLO_crvShape.iog.og[5]" "cluster41Set.dsm" -na;
connectAttr "TEMP_skirtLoft03LO_clsHndlCluster.msg" "cluster41Set.ub[0]";
connectAttr "cluster40GroupParts.og" "TEMP_skirtLoft02LO_clsHndlCluster.ip[0].ig"
		;
connectAttr "cluster40GroupId.id" "TEMP_skirtLoft02LO_clsHndlCluster.ip[0].gi";
connectAttr "TEMP_skirtLoft02LO_clsHndl.wm" "TEMP_skirtLoft02LO_clsHndlCluster.ma"
		;
connectAttr "TEMP_skirtLoft02LO_clsHndlShape.x" "TEMP_skirtLoft02LO_clsHndlCluster.x"
		;
connectAttr "TEMP_skirtLoft01LO_clsHndlCluster.og[0]" "cluster40GroupParts.ig";
connectAttr "cluster40GroupId.id" "cluster40GroupParts.gi";
connectAttr "cluster40GroupId.msg" "cluster40Set.gn" -na;
connectAttr "TEMP_skirtLoftLO_crvShape.iog.og[4]" "cluster40Set.dsm" -na;
connectAttr "TEMP_skirtLoft02LO_clsHndlCluster.msg" "cluster40Set.ub[0]";
connectAttr "cluster39GroupParts.og" "TEMP_skirtLoft01LO_clsHndlCluster.ip[0].ig"
		;
connectAttr "cluster39GroupId.id" "TEMP_skirtLoft01LO_clsHndlCluster.ip[0].gi";
connectAttr "TEMP_skirtLoft01LO_clsHndl.wm" "TEMP_skirtLoft01LO_clsHndlCluster.ma"
		;
connectAttr "TEMP_skirtLoft01LO_clsHndlShape.x" "TEMP_skirtLoft01LO_clsHndlCluster.x"
		;
connectAttr "cluster39GroupId.msg" "cluster39Set.gn" -na;
connectAttr "TEMP_skirtLoftLO_crvShape.iog.og[2]" "cluster39Set.dsm" -na;
connectAttr "TEMP_skirtLoft01LO_clsHndlCluster.msg" "cluster39Set.ub[0]";
connectAttr "TEMP_skirtLoftLO_crvShapeOrig.ws" "groupParts6.ig";
connectAttr "groupId6.id" "groupParts6.gi";
connectAttr "groupParts6.og" "tweak3.ip[0].ig";
connectAttr "groupId6.id" "tweak3.ip[0].gi";
connectAttr "groupId6.msg" "tweakSet3.gn" -na;
connectAttr "TEMP_skirtLoftLO_crvShape.iog.og[3]" "tweakSet3.dsm" -na;
connectAttr "tweak3.msg" "tweakSet3.ub[0]";
connectAttr "tweak3.og[0]" "cluster39GroupParts.ig";
connectAttr "cluster39GroupId.id" "cluster39GroupParts.gi";
connectAttr "TEMP_skirtLoftUP_crvShape.ws" "skirtPos_loft.ic[0]";
connectAttr "TEMP_skirtLoftMID_crvShape.ws" "skirtPos_loft.ic[1]";
connectAttr "TEMP_skirtLoftLO_crvShape.ws" "skirtPos_loft.ic[2]";
connectAttr "TEMP_tempRoot_ctrl.loftNodes" "skirtPos_loft.tempRoot";
connectAttr "TEMP_skirtPosSkin_nrbsShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "TEMP_skirtPosLegMirror_mdl.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of TEMPLATE_skirtPos_cluster01.ma
