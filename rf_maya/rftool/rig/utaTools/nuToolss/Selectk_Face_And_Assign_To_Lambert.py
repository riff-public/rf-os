# Select Face and Assign to lambert
nodeMth = mc.createNode ('lambert', n = 'Mouth_Lambert')
mc.setAttr ('%s.color' % nodeMth,0.0922722, 0.457611, 0.588235 )
nodeEye = mc.createNode ('lambert', n = 'Eye_Lambert')
mc.setAttr ('%s.color' % nodeEye,0.589317, 0.177624, 0.647059 )

import maya.cmds as mc
lists = mc.ls(sl=True)
for i in range(len(lists)):
    mc.hyperShade(lists[i], assign = nodeEye)