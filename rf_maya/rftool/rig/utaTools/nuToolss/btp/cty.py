from nuTools import btp

class AnimalExtra01(btp.BtpBase):

    def __init__(self, gap=5):
        btp.BtpBase.__init__(self, gap)

        self.mouthLFT = {'NAME':'mouthLFT', 'SIDE':'LFT',
                        '01upLipLFT_UD'        :       {'p':'upLipLFT_UP',         'n':'upLipLFT_DN'},
                        '02loLipLFT_UD'        :       {'p':'loLipLFT_UP',         'n':'loLipLFT_DN'},
                        '03crnrMouthLFT_UD'    :       {'p':'crnrMouthLFT_UP',     'n':'crnrMouthLFT_DN'},
                        '04crnrMouthLFT_IO'    :       {'p':'crnrMouthLFT_OUT',    'n':'crnrMouthLFT_IN'},
                        '05lipsPartLFT_IO'     :       {'p':'lipsPartLFT_OUT',     'n':'lipsPartLFT_IN'}}


        self.mouthRHT = {'NAME':'mouthRHT', 'SIDE':'RHT',
                        '01upLipRHT_UD'        :       {'p':'upLipRHT_UP',         'n':'upLipRHT_DN'},
                        '02loLipRHT_UD'        :       {'p':'loLipRHT_UP',         'n':'loLipRHT_DN'},
                        '03crnrMouthRHT_UD'    :       {'p':'crnrMouthRHT_UP',     'n':'crnrMouthRHT_DN'},
                        '04crnrMouthRHT_IO'    :       {'p':'crnrMouthRHT_OUT',    'n':'crnrMouthRHT_IN'},
                        '05lipsPartRHT_IO'     :       {'p':'lipsPartRHT_OUT',     'n':'lipsPartRHT_IN'}}


        self.mouthMID = {'NAME':'mouthMID', 'SIDE':'MID',
                        '01upLipMID_UD'        :       {'p':'upLipMID_UP',         'n':'upLipMID_DN'},
                        '02loLipMID_UD'        :       {'p':'loLipMID_UP',         'n':'loLipMID_DN'},
                        '03upLipAll_UD'     :       {'p':'upLipAll_UP',         'n':'upLipAll_DN'},
                        '04loLipAll_UD'     :       {'p':'loLipAll_UP',         'n':'loLipAll_DN'},
                        '05mouth_UD'        :       {'p':'mouth_UP',            'n':'mouth_DN'},
                        '06mouth_LR'        :       {'p':'mouth_LFT',           'n':'mouth_RHT'},
                        '07mouthTurn'       :       {'p':'mouthTurn_LFT',       'n':'mouthTurn_RHT'}}   

        self.parts = [self.mouthLFT, self.mouthRHT, self.mouthMID]



class AnimalExtra02(btp.BtpBase):

    def __init__(self, gap=5):
        btp.BtpBase.__init__(self, gap)

        self.mouthLFT = {'NAME':'mouthLFT', 'SIDE':'LFT',
                        '01crnrMouthLFT_UD'    :       {'p':'crnrMouthLFT_UP',     'n':'crnrMouthLFT_DN'},
                        '02crnrMouthLFT_IO'    :       {'p':'crnrMouthLFT_OUT',    'n':'crnrMouthLFT_IN'},
                        '03lipsPartLFT_IO'     :       {'p':'lipsPartLFT_OUT',     'n':'lipsPartLFT_IN'}}


        self.mouthRHT = {'NAME':'mouthRHT', 'SIDE':'RHT',
                        '01crnrMouthRHT_UD'    :       {'p':'crnrMouthRHT_UP',     'n':'crnrMouthRHT_DN'},
                        '02crnrMouthRHT_IO'    :       {'p':'crnrMouthRHT_OUT',    'n':'crnrMouthRHT_IN'},
                        '03lipsPartRHT_IO'     :       {'p':'lipsPartRHT_OUT',     'n':'lipsPartRHT_IN'}}


        self.mouthMID = {'NAME':'mouthMID', 'SIDE':'MID',
                        '01upLipAll_UD'     :       {'p':'upLipAll_UP',         'n':'upLipAll_DN'},
                        '02loLipAll_UD'     :       {'p':'loLipAll_UP',         'n':'loLipAll_DN'},
                        '03mouth_UD'        :       {'p':'mouth_UP',            'n':'mouth_DN'},
                        '04mouth_LR'        :       {'p':'mouth_LFT',           'n':'mouth_RHT'},
                        '05mouthTurn'       :       {'p':'mouthTurn_LFT',       'n':'mouthTurn_RHT'}}   

        self.parts = [self.mouthLFT, self.mouthRHT, self.mouthMID]


class AnimalExtra03(btp.BtpBase):

    def __init__(self, gap=5):
        btp.BtpBase.__init__(self, gap)

        self.mouthLFT = {'NAME':'mouthLFT', 'SIDE':'LFT',
                        '01crnrMouthLFT_UD'    :       {'p':'crnrMouthLFT_UP',     'n':'crnrMouthLFT_DN'},
                        '02crnrMouthLFT_IO'    :       {'p':'crnrMouthLFT_OUT',    'n':'crnrMouthLFT_IN'}}


        self.mouthRHT = {'NAME':'mouthRHT', 'SIDE':'RHT',
                        '01crnrMouthRHT_UD'    :       {'p':'crnrMouthRHT_UP',     'n':'crnrMouthRHT_DN'},
                        '02crnrMouthRHT_IO'    :       {'p':'crnrMouthRHT_OUT',    'n':'crnrMouthRHT_IN'}}


        self.mouthMID = {'NAME':'mouthMID', 'SIDE':'MID',
                        '01mouth_UD'        :       {'p':'mouth_UP',            'n':'mouth_DN'},
                        '02mouth_LR'        :       {'p':'mouth_LFT',           'n':'mouth_RHT'},
                        '03mouthTurn'       :       {'p':'mouthTurn_LFT',       'n':'mouthTurn_RHT'}}   

        self.parts = [self.mouthLFT, self.mouthRHT, self.mouthMID]