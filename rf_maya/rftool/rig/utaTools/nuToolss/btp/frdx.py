from nuTools import btp

class HumanMain01(btp.BtpBase):

	def __init__(self, gap=5):
		btp.BtpBase.__init__(self, gap)

		self.eyeBrowLFT = {'NAME':'eyeBrowLFT', 'SIDE':'LFT',
						'01eb_UD'			:		{'p':'ebLFT_UP',			'n':'ebLFT_DN'},
						'02eb_IO'			:		{'p':'ebLFT_IN',			'n':'ebLFT_OUT'},
						'03ebInner_UD'		:		{'p':'ebInnerLFT_UP',		'n':'ebInnerLFT_DN'},
						'04ebMid_UD'		:		{'p':'ebMidLFT_UP',			'n':'ebMidLFT_DN'},
						'05ebOuter_UD'		:		{'p':'ebOuterLFT_UP',		'n':'ebOuterLFT_DN'}}
		self.eyeBrowRHT = {'NAME':'eyeBrowRHT', 'SIDE':'RHT',
						'01eb_UD'			:		{'p':'ebRHT_UP',			'n':'ebRHT_DN'},
						'02eb_IO'			:		{'p':'ebRHT_IN',			'n':'ebRHT_OUT'},
						'03ebInner_UD'		:		{'p':'ebInnerRHT_UP',		'n':'ebInnerRHT_DN'},
						'04ebMid_UD'		:		{'p':'ebMidRHT_UP',			'n':'ebMidRHT_DN'},
						'05ebOuter_UD'		:		{'p':'ebOuterRHT_UP',		'n':'ebOuterRHT_DN'}}


		self.eyeLFT = 	{'NAME':' eyeLFT', 'SIDE':'LFT',
						'01upLid_UD'		:		{	'p':['upLidLFT_UP_inb01', 'upLidLFT_UP'],
														'n':['upLidLFT_DN_inb01', 'upLidLFT_DN_inb02', 'upLidLFT_DN_inb03', 'upLidLFT_DN']},
						'02loLid_UD'		:		{	'p':['loLidLFT_UP_inb01', 'loLidLFT_UP_inb02', 'loLidLFT_UP_inb03', 'loLidLFT_UP'],
														'n':['loLidLFT_DN_inb01', 'loLidLFT_DN']},
						'03lidTw_IO'		:		{	'p':'lidTwLFT_OUT',		'n':'lidTwLFT_IN'}}
		self.eyeRHT = 	{'NAME':'eyeRHT', 'SIDE':'RHT',
						'01upLid_UD'		:		{	'p':['upLidRHT_UP_inb01', 'upLidRHT_UP'],
														'n':['upLidRHT_DN_inb01', 'upLidRHT_DN_inb02', 'upLidRHT_DN_inb03', 'upLidRHT_DN']},
						'02loLid_UD'		:		{	'p':['loLidRHT_UP_inb01', 'loLidRHT_UP_inb02', 'loLidRHT_UP_inb03', 'loLidRHT_UP'],
														'n':['loLidRHT_DN_inb01', 'loLidRHT_DN']},
						'03lidTw_IO'		:		{	'p':'lidTwRHT_OUT',		'n':'lidTwRHT_IN'}}


		self.mouthLFT = {'NAME':'mouthLFT', 'SIDE':'LFT',
						'01upLip_UD'		:		{'p':'upLipLFT_UP',			'n':'upLipLFT_DN'},
						'02loLip_UD'		:		{'p':'loLipLFT_UP',			'n':'loLipLFT_DN'},
						'03crnrMouth_UD'	:		{'p':'crnrMouthLFT_UP',		'n':'crnrMouthLFT_DN'},
						'04crnrMouth_IO'	:		{'p':'crnrMouthLFT_OUT',	'n':'crnrMouthLFT_IN'},
						'05lipsPart_IO'		:		{'p':'lipsPartLFT_OUT',		'n':'lipsPartLFT_IN'},
						'06upCheek_IO'		:		{'p':'upCheekLFT_OUT',		'n':'upCheekLFT_IN'},
						'07loCheek_IO'		:		{'p':'loCheekLFT_OUT',		'n':'loCheekLFT_IN'},
						'08puff_IO'			:		{'p':'puffLFT_OUT',			'n':'puffLFT_IN'},
						'09cheek_IO'		:		{'p':'cheekLFT_OUT',		'n':'cheekLFT_IN'}}
		self.mouthRHT = {'NAME':'mouthRHT', 'SIDE':'RHT',
						'01upLip_UD'		:		{'p':'upLipRHT_UP',			'n':'upLipRHT_DN'},
						'02loLip_UD'		:		{'p':'loLipRHT_UP',			'n':'loLipRHT_DN'},
						'03crnrMouth_UD'	:		{'p':'crnrMouthRHT_UP',		'n':'crnrMouthRHT_DN'},
						'04crnrMouth_IO'	:		{'p':'crnrMouthRHT_OUT',	'n':'crnrMouthRHT_IN'},
						'05lipsPart_IO'		:		{'p':'lipsPartRHT_OUT',		'n':'lipsPartRHT_IN'},
						'06upCheek_IO'		:		{'p':'upCheekRHT_OUT',		'n':'upCheekRHT_IN'},
						'07loCheek_IO'		:		{'p':'loCheekRHT_OUT',		'n':'loCheekRHT_IN'},
						'08puff_IO'			:		{'p':'puffRHT_OUT',			'n':'puffRHT_IN'},
						'09cheek_IO'		:		{'p':'cheekRHT_OUT',		'n':'cheekRHT_IN'}}


		self.mouthMID = {'NAME':'mouthMID', 'SIDE':'MID',
						'01mouth_UD'		:		{'p':'mouth_UP',			'n':'mouth_DN'},
						'02mouth_LR'		:		{'p':'mouth_LFT',			'n':'mouth_RHT'},
						'03mouthTurn'		:		{'p':'mouthTurn_LFT',		'n':'mouthTurn_RHT'},
						'04upLip_UD'		:		{'p':'upLipMID_UP',			'n':'upLipMID_DN'},
						'05loLip_UD'		:		{'p':'loLipMID_UP',			'n':'loLipMID_DN'},		
						'06upLipCurl_IO'	:		{'p':'upLipCurl_OUT',		'n':'upLipCurl_IN'},
						'07loLipCurl_IO'	:		{'p':'loLipCurl_OUT',		'n':'loLipCurl_IN'},
						'08mouthClench'		:		{'p':'mouthClench_IN',		'n':'mouthClench_OUT'},
						'09mouthPull'		:		{'p':'mouthPull_OUT',		'n':'mouthPull_IN'},
						'10mouthU'			:		{'p':'mouthU',				'n':''},
						'11thickness'		:		{'p':'thickness_UP',		'n':'thickness_DN'}}

		self.parts = [self.mouthMID, self.mouthLFT, self.mouthRHT, self.eyeLFT, self.eyeRHT, self.eyeBrowLFT, self.eyeBrowRHT]


class AnimalExtra01(btp.BtpBase):

	def __init__(self, gap=5):
		btp.BtpBase.__init__(self, gap)

		self.eyeBrowLFT = {'NAME':'eyeBrowLFT', 'SIDE':'LFT',
						'01eb_UD'			:		{'p':'ebLFT_UP',			'n':'ebLFT_DN'},
						'02eb_IO'			:		{'p':'ebLFT_IN',			'n':'ebLFT_OUT'},
						'03ebInner_UD'		:		{'p':'ebInnerLFT_UP',		'n':'ebInnerLFT_DN'},
						'04ebMid_UD'		:		{'p':'ebMidLFT_UP',			'n':'ebMidLFT_DN'},
						'05ebOuter_UD'		:		{'p':'ebOuterLFT_UP',		'n':'ebOuterLFT_DN'}}
		self.eyeBrowRHT = {'NAME':'eyeBrowRHT', 'SIDE':'RHT',
						'01eb_UD'			:		{'p':'ebRHT_UP',			'n':'ebRHT_DN'},
						'02eb_IO'			:		{'p':'ebRHT_IN',			'n':'ebRHT_OUT'},
						'03ebInner_UD'		:		{'p':'ebInnerRHT_UP',		'n':'ebInnerRHT_DN'},
						'04ebMid_UD'		:		{'p':'ebMidRHT_UP',			'n':'ebMidRHT_DN'},
						'05ebOuter_UD'		:		{'p':'ebOuterRHT_UP',		'n':'ebOuterRHT_DN'}}


		self.eyeLFT = 	{'NAME':'eyeLFT', 'SIDE':'LFT',
						'01upLid_UD'		:		{	'p':'upLidLFT_UP',
														'n':['upLidLFT_DN_inb01', 'upLidLFT_DN_inb02', 'upLidLFT_DN_inb03', 'upLidLFT_DN']},
						'02loLid_UD'		:		{	'p':['loLidLFT_UP_inb01', 'loLidLFT_UP_inb02', 'loLidLFT_UP_inb03', 'loLidLFT_UP'],
														'n':'loLidLFT_DN'},
						'03lidTw_IO'		:		{	'p':'lidTwLFT_OUT',		'n':'lidTwLFT_IN'}}
		self.eyeRHT = 	{'NAME':'eyeRHT', 'SIDE':'RHT',
						'01upLid_UD'		:		{	'p':'upLidRHT_UP',
														'n':['upLidRHT_DN_inb01', 'upLidRHT_DN_inb02', 'upLidRHT_DN_inb03', 'upLidRHT_DN']},
						'02loLid_UD'		:		{	'p':['loLidRHT_UP_inb01', 'loLidRHT_UP_inb02', 'loLidRHT_UP_inb03', 'loLidRHT_UP'],
														'n':'loLidRHT_DN'},
						'03lidTw_IO'		:		{	'p':'lidTwRHT_OUT',		'n':'lidTwRHT_IN'}}


		self.mouthLFT = {'NAME':'mouthLFT', 'SIDE':'LFT',
						'01upLip_UD'		:		{'p':'upLipLFT_UP',			'n':'upLipLFT_DN'},
						'02loLip_UD'		:		{'p':'loLipLFT_UP',			'n':'loLipLFT_DN'},
						'03crnrMouth_UD'	:		{'p':'crnrMouthLFT_UP',		'n':'crnrMouthLFT_DN'},
						'04crnrMouth_IO'	:		{'p':'crnrMouthLFT_OUT',	'n':'crnrMouthLFT_IN'},
						'05lipsPart_IO'		:		{'p':'lipsPartLFT_OUT',		'n':'lipsPartLFT_IN'}}


		self.mouthRHT = {'NAME':'mouthRHT', 'SIDE':'RHT',
						'01upLip_UD'		:		{'p':'upLipRHT_UP',			'n':'upLipRHT_DN'},
						'02loLip_UD'		:		{'p':'loLipRHT_UP',			'n':'loLipRHT_DN'},
						'03crnrMouth_UD'	:		{'p':'crnrMouthRHT_UP',		'n':'crnrMouthRHT_DN'},
						'04crnrMouth_IO'	:		{'p':'crnrMouthRHT_OUT',	'n':'crnrMouthRHT_IN'},
						'05lipsPart_IO'		:		{'p':'lipsPartRHT_OUT',		'n':'lipsPartRHT_IN'}}


		self.mouthMID = {'NAME':'mouthMID', 'SIDE':'MID',
						'01upLip_UD'		:		{'p':'upLipMID_UP',			'n':'upLipMID_DN'},
						'02loLip_UD'		:		{'p':'loLipMID_UP',			'n':'loLipMID_DN'},
						'03upLipAll_UD'		:		{'p':'upLipAll_UP',			'n':'upLipAll_DN'},
						'04loLipAll_UD'		:		{'p':'loLipAll_UP',			'n':'loLipAll_DN'},
						'05mouth_UD'		:		{'p':'mouth_UP',			'n':'mouth_DN'},
						'06mouth_LR'		:		{'p':'mouth_LFT',			'n':'mouth_RHT'},
						'07mouthTurn'		:		{'p':'mouthTurn_LFT',		'n':'mouthTurn_RHT'}}	

		self.parts = [self.mouthMID, self.mouthLFT, self.mouthRHT, self.eyeLFT, self.eyeRHT, self.eyeBrowLFT, self.eyeBrowRHT]


class AnimalExtra02(btp.BtpBase):

	def __init__(self, gap=5):
		btp.BtpBase.__init__(self, gap)

		self.eyeBrowLFT = {'NAME':'eyeBrowLFT', 'SIDE':'LFT',
						'01eb_UD'			:		{'p':'ebLFT_UP',			'n':'ebLFT_DN'},
						'02eb_IO'			:		{'p':'ebLFT_IN',			'n':'ebLFT_OUT'},
						'03ebRot_IO'		:		{'p':'ebRotLFT_OUT',		'n':'ebRotLFT_IN'}}

		self.eyeBrowRHT = {'NAME':'eyeBrowRHT', 'SIDE':'RHT',
						'01eb_UD'			:		{'p':'ebRHT_UP',			'n':'ebRHT_DN'},
						'02eb_IO'			:		{'p':'ebRHT_IN',			'n':'ebRHT_OUT'},
						'03ebRot_IO'		:		{'p':'ebRotRHT_OUT',		'n':'ebRotRHT_IN'}}

		self.eyeLFT = 	{'NAME':'eyeLFT', 'SIDE':'LFT',
						'01upLid_UD'		:		{	'p':'upLidLFT_UP',
														'n':['upLidLFT_DN_inb01', 'upLidLFT_DN_inb02', 'upLidLFT_DN']},
						'02loLid_UD'		:		{	'p':['loLidLFT_UP_inb01', 'loLidLFT_UP_inb02', 'loLidLFT_UP'],
														'n':'loLidLFT_DN'},
						'03lidTw_IO'		:		{	'p':'lidTwLFT_OUT',		'n':'lidTwLFT_IN'}}
		self.eyeRHT = 	{'NAME':'eyeRHT', 'SIDE':'RHT',
						'01upLid_UD'		:		{	'p':'upLidRHT_UP',
														'n':['upLidRHT_DN_inb01', 'upLidRHT_DN_inb02', 'upLidRHT_DN']},
						'02loLid_UD'		:		{	'p':['loLidRHT_UP_inb01', 'loLidRHT_UP_inb02', 'loLidRHT_UP'],
														'n':'loLidRHT_DN'},
						'03lidTw_IO'		:		{	'p':'lidTwRHT_OUT',		'n':'lidTwRHT_IN'}}


		self.mouthLFT = {'NAME':'mouthLFT', 'SIDE':'LFT',
						'01upLip_UD'		:		{'p':'upLipLFT_UP',			'n':'upLipLFT_DN'},
						'02loLip_UD'		:		{'p':'loLipLFT_UP',			'n':'loLipLFT_DN'},
						'03crnrMouth_UD'	:		{'p':'crnrMouthLFT_UP',		'n':'crnrMouthLFT_DN'},
						'04crnrMouth_IO'	:		{'p':'crnrMouthLFT_OUT',	'n':'crnrMouthLFT_IN'}}


		self.mouthRHT = {'NAME':'mouthRHT', 'SIDE':'RHT',
						'01upLip_UD'		:		{'p':'upLipRHT_UP',			'n':'upLipRHT_DN'},
						'02loLip_UD'		:		{'p':'loLipRHT_UP',			'n':'loLipRHT_DN'},
						'03crnrMouth_UD'	:		{'p':'crnrMouthRHT_UP',		'n':'crnrMouthRHT_DN'},
						'04crnrMouth_IO'	:		{'p':'crnrMouthRHT_OUT',	'n':'crnrMouthRHT_IN'}}


		self.mouthMID = {'NAME':'mouthMID', 'SIDE':'MID',
						'01upLip_UD'		:		{'p':'upLipMID_UP',			'n':'upLipMID_DN'},
						'02loLip_UD'		:		{'p':'loLipMID_UP',			'n':'loLipMID_DN'},
						'03upLipAll_UD'		:		{'p':'upLipAll_UP',			'n':'upLipAll_DN'},
						'04loLipAll_UD'		:		{'p':'loLipAll_UP',			'n':'loLipAll_DN'},
						'05mouth_UD'		:		{'p':'mouth_UP',			'n':'mouth_DN'},
						'06mouth_LR'		:		{'p':'mouth_LFT',			'n':'mouth_RHT'},
						'07mouthTurn'		:		{'p':'mouthTurn_LFT',		'n':'mouthTurn_RHT'}}	

		self.parts = [self.mouthMID, self.mouthLFT, self.mouthRHT, self.eyeLFT, self.eyeRHT, self.eyeBrowLFT, self.eyeBrowRHT]


class AnimalExtra03(btp.BtpBase):

	def __init__(self, gap=5):
		btp.BtpBase.__init__(self, gap)

		self.eyeBrowLFT = {'NAME':'eyeBrowLFT', 'SIDE':'LFT',
						'01eb_UD'			:		{'p':'ebLFT_UP',			'n':'ebLFT_DN'},
						'02eb_IO'			:		{'p':'ebLFT_IN',			'n':'ebLFT_OUT'},
						'03ebRot_IO'		:		{'p':'ebRotLFT_OUT',		'n':'ebRotLFT_IN'}}

		self.eyeBrowRHT = {'NAME':'eyeBrowRHT', 'SIDE':'RHT',
						'01eb_UD'			:		{'p':'ebRHT_UP',			'n':'ebRHT_DN'},
						'02eb_IO'			:		{'p':'ebRHT_IN',			'n':'ebRHT_OUT'},
						'03ebRot_IO'		:		{'p':'ebRotRHT_OUT',		'n':'ebRotRHT_IN'}}

		self.eyeLFT = 	{'NAME':'eyeLFT', 'SIDE':'LFT',
						'01upLid_UD'		:		{	'p':'upLidLFT_UP',
													'n': ['upLidLFT_DN_inb01', 'upLidLFT_DN_inb02', 'upLidLFT_DN']},
						'02loLid_UD'		:		{	'p':['loLidLFT_UP_inb01', 'loLidLFT_UP_inb02', 'loLidLFT_UP'],
													'n':'loLidLFT_DN'}}
		self.eyeRHT = 	{'NAME':'eyeRHT', 'SIDE':'RHT',
						'01upLid_UD'		:		{	'p':'upLidRHT_UP',
													'n':['upLidRHT_DN_inb01', 'upLidRHT_DN_inb02', 'upLidRHT_DN']},
						'02loLid_UD'		:		{	'p':['loLidRHT_UP_inb01', 'loLidRHT_UP_inb02', 'loLidRHT_UP'],
													'n':'loLidRHT_DN'}}


		self.mouthLFT = {'NAME':'mouthLFT', 'SIDE':'LFT',
						'01crnrMouth_UD'	:		{'p':'crnrMouthLFT_UP',		'n':'crnrMouthLFT_DN'},
						'02crnrMouth_IO'	:		{'p':'crnrMouthLFT_OUT',	'n':'crnrMouthLFT_IN'}}


		self.mouthRHT = {'NAME':'mouthRHT', 'SIDE':'RHT',
						'01crnrMouth_UD'	:		{'p':'crnrMouthRHT_UP',		'n':'crnrMouthRHT_DN'},
						'02crnrMouth_IO'	:		{'p':'crnrMouthRHT_OUT',	'n':'crnrMouthRHT_IN'}}


		self.mouthMID = {'NAME':'mouthMID', 'SIDE':'MID',
						'01upLipAll_UD'		:		{'p':'upLipAll_UP',			'n':'upLipAll_DN'},
						'02loLipAll_UD'		:		{'p':'loLipAll_UP',			'n':'loLipAll_DN'},
						'03mouth_UD'		:		{'p':'mouth_UP',			'n':'mouth_DN'},
						'04mouth_LR'		:		{'p':'mouth_LFT',			'n':'mouth_RHT'},
						'05mouthTurn'		:		{'p':'mouthTurn_LFT',		'n':'mouthTurn_RHT'}}	

		self.parts = [self.mouthMID, self.mouthLFT, self.mouthRHT, self.eyeLFT, self.eyeRHT, self.eyeBrowLFT, self.eyeBrowRHT]


class WingAnimalExtra03(btp.BtpBase):

	def __init__(self, gap=5):
		btp.BtpBase.__init__(self, gap)

		self.eyeBrowLFT = {'NAME':'eyeBrowLFT', 'SIDE':'LFT',
						'01eb_UD'			:		{'p':'ebLFT_UP',			'n':'ebLFT_DN'},
						'02eb_IO'			:		{'p':'ebLFT_IN',			'n':'ebLFT_OUT'},
						'03ebRot_IO'		:		{'p':'ebRotLFT_OUT',		'n':'ebRotLFT_IN'}}

		self.eyeBrowRHT = {'NAME':'eyeBrowRHT', 'SIDE':'RHT',
						'01eb_UD'			:		{'p':'ebRHT_UP',			'n':'ebRHT_DN'},
						'02eb_IO'			:		{'p':'ebRHT_IN',			'n':'ebRHT_OUT'},
						'03ebRot_IO'		:		{'p':'ebRotRHT_OUT',		'n':'ebRotRHT_IN'}}

		self.eyeLFT = 	{'NAME':'eyeLFT', 'SIDE':'LFT',
						'01upLid_UD'		:		{	'p':'upLidLFT_UP',
														'n':['upLidLFT_DN_inb01', 'upLidLFT_DN_inb02', 'upLidLFT_DN']},
						'02loLid_UD'		:		{	'p':['loLidLFT_UP_inb01', 'loLidLFT_UP_inb02', 'loLidLFT_UP'],
														'n':'loLidLFT_DN'}}
		self.eyeRHT = 	{'NAME':'eyeRHT', 'SIDE':'RHT',
						'01upLid_UD'		:		{	'p':'upLidRHT_UP',
														'n':['upLidRHT_DN_inb01', 'upLidRHT_DN_inb02', 'upLidRHT_DN']},
						'02loLid_UD'		:		{	'p':['loLidRHT_UP_inb01', 'loLidRHT_UP_inb02', 'loLidRHT_UP'],
														'n':'loLidRHT_DN'}}


		self.wingLFT = {'NAME':'wingLFT', 'SIDE':'LFT',
						'01wing_FOLD'		:		{	'p':['wingLFT_FOLD_inb01', 'wingLFT_FOLD_inb02', 'wingLFT_FOLD_inb03', 'wingLFT_FOLD_inb04', 'wingLFT_FOLD'],
														'n':''}}


		self.wingRHT = {'NAME':'wingRHT', 'SIDE':'RHT',
						'01wing_FOLD'		:		{	'p':['wingRHT_FOLD_inb01', 'wingRHT_FOLD_inb02', 'wingRHT_FOLD_inb03', 'wingRHT_FOLD_inb04', 'wingRHT_FOLD'],
														'n':'' }}


		self.parts = [self.eyeLFT, self.eyeRHT, self.eyeBrowLFT, self.eyeBrowRHT, self.wingLFT, self.wingRHT]




class DugdigCha(btp.BtpBase):

	def __init__(self, gap=5):
		btp.BtpBase.__init__(self, gap)

		self.eyeBrowLFT = {'NAME':'eyeBrowLFT', 'SIDE':'LFT',
						'01eb_UD'			:		{'p':'ebLFT_UP',			'n':'ebLFT_DN'},
						'02eb_IO'			:		{'p':'ebLFT_IN',			'n':'ebLFT_OUT'},
						'03ebInner_UD'		:		{'p':'ebInnerLFT_UP',		'n':'ebInnerLFT_DN'},
						'04ebMid_UD'		:		{'p':'ebMidLFT_UP',			'n':'ebMidLFT_DN'},
						'05ebOuter_UD'		:		{'p':'ebOuterLFT_UP',		'n':'ebOuterLFT_DN'}}
		self.eyeBrowRHT = {'NAME':'eyeBrowRHT', 'SIDE':'RHT',
						'01eb_UD'			:		{'p':'ebRHT_UP',			'n':'ebRHT_DN'},
						'02eb_IO'			:		{'p':'ebRHT_IN',			'n':'ebRHT_OUT'},
						'03ebInner_UD'		:		{'p':'ebInnerRHT_UP',		'n':'ebInnerRHT_DN'},
						'04ebMid_UD'		:		{'p':'ebMidRHT_UP',			'n':'ebMidRHT_DN'},
						'05ebOuter_UD'		:		{'p':'ebOuterRHT_UP',		'n':''}}


		self.eyeLFT = 	{'NAME':'eyeLFT', 'SIDE':'LFT',
						'01upLid_UD'		:		{	'p':'upLidLFT_UP',
														'n':['upLidLFT_DN_inb01', 'upLidLFT_DN_inb02', 'upLidLFT_DN_inb03', 'upLidLFT_DN']},
						'02loLid_UD'		:		{	'p':['loLidLFT_UP_inb01', 'loLidLFT_UP_inb02', 'loLidLFT_UP_inb03', 'loLidLFT_UP'],
														'n':'loLidLFT_DN'},
						'03lidTw_IO'		:		{	'p':'lidTwLFT_OUT',		'n':'lidTwLFT_IN'}}
		self.eyeRHT = 	{'NAME':'eyeRHT', 'SIDE':'RHT',
						'01upLid_UD'		:		{	'p':'upLidRHT_UP',
														'n':['upLidRHT_DN_inb01', 'upLidRHT_DN_inb02', 'upLidRHT_DN_inb03', 'upLidRHT_DN']},
						'02loLid_UD'		:		{	'p':['loLidRHT_UP_inb01', 'loLidRHT_UP_inb02', 'loLidRHT_UP_inb03', 'loLidRHT_UP'],
														'n':'loLidRHT_DN'},
						'03lidTw_IO'		:		{	'p':'lidTwRHT_OUT',		'n':'lidTwRHT_IN'}}


		self.mouthLFT = {'NAME':'mouthLFT', 'SIDE':'LFT',
						'01upLip_UD'		:		{'p':'upLipLFT_UP',			'n':'upLipLFT_DN'},
						'02loLip_UD'		:		{'p':'loLipLFT_UP',			'n':'loLipLFT_DN'},
						'03crnrMouth_UD'	:		{'p':'crnrMouthLFT_UP',		'n':'crnrMouthLFT_DN'},
						'04crnrMouth_IO'	:		{'p':'crnrMouthLFT_OUT',	'n':'crnrMouthLFT_IN'},
						'05lipsPart_IO'		:		{'p':'lipsPartLFT_OUT',		'n':'lipsPartLFT_IN'}}


		self.mouthRHT = {'NAME':'mouthRHT', 'SIDE':'RHT',
						'01upLip_UD'		:		{'p':'upLipRHT_UP',			'n':'upLipRHT_DN'},
						'02loLip_UD'		:		{'p':'loLipRHT_UP',			'n':'loLipRHT_DN'},
						'03crnrMouth_UD'	:		{'p':'crnrMouthRHT_UP',		'n':'crnrMouthRHT_DN'},
						'04crnrMouth_IO'	:		{'p':'crnrMouthRHT_OUT',	'n':'crnrMouthRHT_IN'},
						'05lipsPart_IO'		:		{'p':'lipsPartRHT_OUT',		'n':'lipsPartRHT_IN'}}


		self.mouthMID = {'NAME':'mouthMID', 'SIDE':'MID',
						'01upLip_UD'		:		{'p':'upLipMID_UP',			'n':'upLipMID_DN'},
						'02loLip_UD'		:		{'p':'loLipMID_UP',			'n':'loLipMID_DN'},
						'03upLipAll_UD'		:		{'p':'upLipAll_UP',			'n':'upLipAll_DN'},
						'04loLipAll_UD'		:		{'p':'loLipAll_UP',			'n':'loLipAll_DN'},
						'05mouth_UD'		:		{'p':'mouth_UP',			'n':'mouth_DN'},
						'06mouth_LR'		:		{'p':'mouth_LFT',			'n':'mouth_RHT'},
						'07mouthTurn'		:		{'p':'mouthTurn_LFT',		'n':'mouthTurn_RHT'}}	

		self.parts = [self.mouthMID, self.mouthLFT, self.mouthRHT, self.eyeLFT, self.eyeRHT, self.eyeBrowLFT, self.eyeBrowRHT]
