from nuTools.manager import btp

class AnimalExtra01(btp.BtpBase):

    def __init__(self, gap=5):
        btp.BtpBase.__init__(self, gap)

        self.mouthLFT = {'NAME':'mouthLFT', 'SIDE':'LFT',
                        '01upLipLFT_UD'        :       {'p':'upLipLFT_UP',         'n':'upLipLFT_DN'},
                        '02loLipLFT_UD'        :       {'p':'loLipLFT_UP',         'n':'loLipLFT_DN'},
                        '03crnrMouthLFT_UD'    :       {'p':'crnrMouthLFT_UP',     'n':'crnrMouthLFT_DN'},
                        '04crnrMouthLFT_IO'    :       {'p':'crnrMouthLFT_OUT',    'n':'crnrMouthLFT_IN'},
                        '05lipsPartLFT_IO'     :       {'p':'lipsPartLFT_OUT',     'n':'lipsPartLFT_IN'}}


        self.mouthRGT = {'NAME':'mouthRGT', 'SIDE':'RGT',
                        '01upLipRGT_UD'        :       {'p':'upLipRGT_UP',         'n':'upLipRGT_DN'},
                        '02loLipRGT_UD'        :       {'p':'loLipRGT_UP',         'n':'loLipRGT_DN'},
                        '03crnrMouthRGT_UD'    :       {'p':'crnrMouthRGT_UP',     'n':'crnrMouthRGT_DN'},
                        '04crnrMouthRGT_IO'    :       {'p':'crnrMouthRGT_OUT',    'n':'crnrMouthRGT_IN'},
                        '05lipsPartRGT_IO'     :       {'p':'lipsPartRGT_OUT',     'n':'lipsPartRGT_IN'}}


        self.mouthMID = {'NAME':'mouthMID', 'SIDE':'MID',
                        '01upLipMID_UD'        :       {'p':'upLipMID_UP',         'n':'upLipMID_DN'},
                        '02loLipMID_UD'        :       {'p':'loLipMID_UP',         'n':'loLipMID_DN'},
                        '03upLipAll_UD'     :       {'p':'upLipAll_UP',         'n':'upLipAll_DN'},
                        '04loLipAll_UD'     :       {'p':'loLipAll_UP',         'n':'loLipAll_DN'},
                        '05mouth_UD'        :       {'p':'mouth_UP',            'n':'mouth_DN'},
                        '06mouth_LR'        :       {'p':'mouth_LFT',           'n':'mouth_RGT'},
                        '07mouthTurn'       :       {'p':'mouthTurn_LFT',       'n':'mouthTurn_RGT'}}   

        self.parts = [self.mouthLFT, self.mouthRGT, self.mouthMID]



class AnimalExtra02(btp.BtpBase):

    def __init__(self, gap=5):
        btp.BtpBase.__init__(self, gap)

        self.mouthLFT = {'NAME':'mouthLFT', 'SIDE':'LFT',
                        '01crnrMouthLFT_UD'    :       {'p':'crnrMouthLFT_UP',     'n':'crnrMouthLFT_DN'},
                        '02crnrMouthLFT_IO'    :       {'p':'crnrMouthLFT_OUT',    'n':'crnrMouthLFT_IN'},
                        '03lipsPartLFT_IO'     :       {'p':'lipsPartLFT_OUT',     'n':'lipsPartLFT_IN'}}


        self.mouthRGT = {'NAME':'mouthRGT', 'SIDE':'RGT',
                        '01crnrMouthRGT_UD'    :       {'p':'crnrMouthRGT_UP',     'n':'crnrMouthRGT_DN'},
                        '02crnrMouthRGT_IO'    :       {'p':'crnrMouthRGT_OUT',    'n':'crnrMouthRGT_IN'},
                        '03lipsPartRGT_IO'     :       {'p':'lipsPartRGT_OUT',     'n':'lipsPartRGT_IN'}}


        self.mouthMID = {'NAME':'mouthMID', 'SIDE':'MID',
                        '01upLipAll_UD'     :       {'p':'upLipAll_UP',         'n':'upLipAll_DN'},
                        '02loLipAll_UD'     :       {'p':'loLipAll_UP',         'n':'loLipAll_DN'},
                        '03mouth_UD'        :       {'p':'mouth_UP',            'n':'mouth_DN'},
                        '04mouth_LR'        :       {'p':'mouth_LFT',           'n':'mouth_RGT'},
                        '05mouthTurn'       :       {'p':'mouthTurn_LFT',       'n':'mouthTurn_RGT'}}   

        self.parts = [self.mouthLFT, self.mouthRGT, self.mouthMID]


class AnimalExtra03(btp.BtpBase):

    def __init__(self, gap=5):
        btp.BtpBase.__init__(self, gap)

        self.mouthLFT = {'NAME':'mouthLFT', 'SIDE':'LFT',
                        '01crnrMouthLFT_UD'    :       {'p':'crnrMouthLFT_UP',     'n':'crnrMouthLFT_DN'},
                        '02crnrMouthLFT_IO'    :       {'p':'crnrMouthLFT_OUT',    'n':'crnrMouthLFT_IN'}}


        self.mouthRGT = {'NAME':'mouthRGT', 'SIDE':'RGT',
                        '01crnrMouthRGT_UD'    :       {'p':'crnrMouthRGT_UP',     'n':'crnrMouthRGT_DN'},
                        '02crnrMouthRGT_IO'    :       {'p':'crnrMouthRGT_OUT',    'n':'crnrMouthRGT_IN'}}


        self.mouthMID = {'NAME':'mouthMID', 'SIDE':'MID',
                        '01mouth_UD'        :       {'p':'mouth_UP',            'n':'mouth_DN'},
                        '02mouth_LR'        :       {'p':'mouth_LFT',           'n':'mouth_RGT'},
                        '03mouthTurn'       :       {'p':'mouthTurn_LFT',       'n':'mouthTurn_RGT'}}   

        self.parts = [self.mouthLFT, self.mouthRGT, self.mouthMID]