from nuTools.manager import btp

p = 'p'
n = 'n'


class HumanMain01(btp.BtpBase):

	def __init__(self, gap=5):
		btp.BtpBase.__init__(self, gap)

		self.eyeBrowLFT = {'NAME':'eyeBrowLFT', 'SIDE':'LFT',
						'01ebIO'			:		{p:'ebLFT_OUT',			n:'ebLFT_IN'},
						'02ebUD'			:		{p:'ebLFT_UP',			n:'ebLFT_DN'},
						'03ebTurn'			:		{p:'ebTurnLFT_OUT',		n:'ebTurnLFT_IN'},
						'04ebInnerIO'		:		{p:'ebInnerLFT_IN',		n:'ebInnerLFT_OUT'},
						'05ebInnerTurn'		:		{p:'ebInnerTurnLFT_OUT',n:'ebInnerTurnLFT_IN'},
						'06ebInnerUD'		:		{p:'ebInnerLFT_UP',		n:'ebInnerLFT_DN'},
						'07ebMidUD'			:		{p:'ebMidLFT_UP',		n:'ebMidLFT_DN'},
						'08ebOuterUD'		:		{p:'ebOuterLFT_UP',		n:'ebOuterLFT_DN'}}

		self.eyeBrowRGT = {'NAME':'eyeBrowRGT', 'SIDE':'RGT',
						'01ebIO'			:		{p:'ebRGT_OUT',			n:'ebRGT_IN'},
						'02ebUD'			:		{p:'ebRGT_UP',			n:'ebRGT_DN'},
						'03ebTurn'			:		{p:'ebTurnRGT_OUT',		n:'ebTurnRGT_IN'},
						'04ebInnerIO'		:		{p:'ebInnerRGT_IN',		n:'ebInnerRGT_OUT'},
						'05ebInnerTurn'		:		{p:'ebInnerTurnRGT_OUT',n:'ebInnerTurnRGT_IN'},
						'06ebInnerUD'		:		{p:'ebInnerRGT_UP',		n:'ebInnerRGT_DN'},
						'07ebMidUD'			:		{p:'ebMidRGT_UP',		n:'ebMidRGT_DN'},
						'08ebOuterUD'		:		{p:'ebOuterRGT_UP',		n:'ebOuterRGT_DN'}}


		self.lidLFT = 	{'NAME':' lidLFT', 'SIDE':'LFT',
						'01upLidUD'			:		{p:'upLidLFT_UP',
													n:['upLidLFT_DN_inb01', 'upLidLFT_DN_inb02', 'upLidLFT_DN_inb03', 'upLidLFT_DN']},
						'02loLidUD'			:		{p:['loLidLFT_UP_inb01', 'loLidLFT_UP_inb02', 'loLidLFT_UP_inb03', 'loLidLFT_UP'],
													n:'loLidLFT_DN'},
						'03upLidTW'			:		{p:'upLidTwLFT_OUT',	n:'upLidTwLFT_IN'},
						'04loLidTW'			:		{p:'loLidTwLFT_OUT',	n:'loLidTwLFT_IN'}}

		self.lidRGT = 	{'NAME':' lidRGT', 'SIDE':'RGT',
						'01upLidUD'			:		{p:'upLidRGT_UP',
													n:['upLidRGT_DN_inb01', 'upLidRGT_DN_inb02', 'upLidRGT_DN_inb03', 'upLidRGT_DN']},
						'02loLidUD'			:		{p:['loLidRGT_UP_inb01', 'loLidRGT_UP_inb02', 'loLidRGT_UP_inb03', 'loLidRGT_UP'],
													n:'loLidRGT_DN'},
						'03upLidTW'			:		{p:'upLidTwRGT_OUT',	n:'upLidTwRGT_IN'},
						'04loLidTW'			:		{p:'loLidTwRGT_OUT',	n:'loLidTwRGT_IN'}}

		self.face = 	{'NAME':' face', 'SIDE':'',
						'01faceSquash'			:	{p:'faceSquash',	n:'faceStretch'}}

		self.nose = 	{'NAME':' nose', 'SIDE':'',
						'01noseSquash'			:	{p:'noseSquash',	n:'noseStretch'}}

		self.noseLFT = 	{'NAME':' noseLFT', 'SIDE':'LFT',
						'01leftUp'				:	{p:'noseLFT_UP',		n:'noseLFT_DN'},
						'02leftTurn'			:	{p:'noseTurnLFT_OUT',	n:'noseTurnLFT_IN'}}

		self.noseRGT = 	{'NAME':' noseRGT', 'SIDE':'RGT',
						'01rightUp'				:	{p:'noseRGT_UP',		n:'noseRGT_DN'},
						'02rightTurn'			:	{p:'noseTurnRGT_OUT',	n:'noseTurnRGT_IN'}}

		self.mouthLFT = {'NAME':'mouthLFT', 'SIDE':'LFT',
						'01crnrMouthIO'		:		{p:'crnrMouthLFT_OUT',		n:'crnrMouthLFT_IN'},
						'02crnrMouthUD'		:		{p:'crnrMouthLFT_UP',		n:'crnrMouthLFT_DN'},
						'03upLipsUD'		:		{p:'upLipLFT_UP',			n:'upLipLFT_DN'},
						'04loLipsUD'		:		{p:'loLipLFT_UP',			n:'loLipLFT_DN'},
						'05upCrnrLipsUD'	:		{p:'upCrnrLipLFT_UP',		n:'upCrnrLipLFT_DN'},
						'06loCrnrLipsUD'	:		{p:'loCrnrLipLFT_UP',		n:'loCrnrLipLFT_DN'},
						'07lipsPartIO'		:		{p:'lipsPartLFT_IN',		n:'lipsPartLFT_OUT'},
						'08cheekIO'			:		{p:'cheekLFT_OUT',			n:'cheekLFT_IN'},
						'09cheekUprIO'		:		{p:'cheekUprLFT_UP',		n:'cheekUprLFT_DN'},
						'10cheekLwrIO'		:		{p:'cheekLwrLFT_OUT',		n:'cheekLwrLFT_IN'},
						'11puffIO'			:		{p:'puffLFT_OUT',			n:'puffLFT_IN'}}

		self.mouthRGT = {'NAME':'mouthRGT', 'SIDE':'RGT',
						'01crnrMouthIO'		:		{p:'crnrMouthRGT_OUT',		n:'crnrMouthRGT_IN'},
						'02crnrMouthUD'		:		{p:'crnrMouthRGT_UP',		n:'crnrMouthRGT_DN'},
						'03upLipsUD'		:		{p:'upLipRGT_UP',			n:'upLipRGT_DN'},
						'04loLipsUD'		:		{p:'loLipRGT_UP',			n:'loLipRGT_DN'},
						'05upCrnrLipsUD'	:		{p:'upCrnrLipRGT_UP',		n:'upCrnrLipRGT_DN'},
						'06loCrnrLipsUD'	:		{p:'loCrnrLipRGT_UP',		n:'loCrnrLipRGT_DN'},
						'07lipsPartIO'		:		{p:'lipsPartRGT_IN',		n:'lipsPartRGT_OUT'},
						'08cheekIO'			:		{p:'cheekRGT_OUT',			n:'cheekRGT_IN'},
						'09cheekUprIO'		:		{p:'cheekUprRGT_UP',		n:'cheekUprRGT_DN'},
						'10cheekLwrIO'		:		{p:'cheekLwrRGT_OUT',		n:'cheekLwrRGT_IN'},
						'11puffIO'			:		{p:'puffRGT_OUT',			n:'puffRGT_IN'}}

		self.mouth = {'NAME':'mouth', 'SIDE':'',
						'01mouthLR'			:		{p:'mouth_LFT',			n:'mouth_RGT'},
						'02mouthUD'			:		{p:'mouth_UP',			n:'mouth_DN'},
						
						'03mouthTurn'		:		{p:'mouthTurn_LFT',		n:'mouthTurn_RGT'},

						'04upLipsUD'		:		{p:'upLipAll_UP',		n:'upLipAll_DN'},
						'05loLipsUD'		:		{p:'loLipAll_UP',		n:'loLipAll_DN'},
						'06upLipsMidUD'		:		{p:'upLipMid_UP',		n:'upLipMid_DN'},
						'07loLipsMidUD'		:		{p:'loLipMid_UP',		n:'loLipMid_DN'},

						'08upLipsCurlIO'	:		{p:'upLipCurl_OUT',		n:'upLipCurl_IN'},
						'09loLipsCurlIO'	:		{p:'loLipCurl_OUT',		n:'loLipCurl_IN'},

						'10mouthClench'		:		{p:'mouthClench_IN',	n:''},
						'11mouthPull'		:		{p:'mouthPull_OUT',		n:''},
						'12mouthU'			:		{p:'mouthU',			n:''},
						'13mouthM'		:			{p:'mouthM',			n:''}}

		self.getParts()

class HumanMain02(btp.BtpBase):

	def __init__(self, gap=5):
		btp.BtpBase.__init__(self, gap)

		self.eyeBrowLFT = {'NAME':'eyeBrowLFT', 'SIDE':'LFT',
						'01ebOI'			:		{p:'ebLFT_OUT',			n:'ebLFT_IN'},
						'02ebUD'			:		{p:'ebLFT_UP',			n:'ebLFT_DN'},

						'03ebTW'			:		{p:'ebTWLFT_OUT',		n:'ebTWLFT_IN'},
						'04ebInnerTW'		:		{p:'ebInnerTWLFT_OUT',	n:'ebInnerTWLFT_IN'},

						'05ebInnerUD'		:		{p:'ebInnerLFT_UP',		n:'ebInnerLFT_DN'},
						'06ebMidUD'			:		{p:'ebMidLFT_UP',		n:'ebMidLFT_DN'},
						'07ebOuterUD'		:		{p:'ebOuterLFT_UP',		n:'ebOuterLFT_DN'}}

		self.eyeBrowRGT = {'NAME':'eyeBrowRGT', 'SIDE':'RGT',
						'01ebOI'			:		{p:'ebRGT_OUT',			n:'ebRGT_IN'},
						'02ebUD'			:		{p:'ebRGT_UP',			n:'ebRGT_DN'},

						'03ebTW'			:		{p:'ebTWRGT_OUT',		n:'ebTWRGT_IN'},
						'04ebInnerTW'		:		{p:'ebInnerTWRGT_OUT',	n:'ebInnerTWRGT_IN'},
						
						'05ebInnerUD'		:		{p:'ebInnerRGT_UP',		n:'ebInnerRGT_DN'},
						'06ebMidUD'			:		{p:'ebMidRGT_UP',		n:'ebMidRGT_DN'},
						'07ebOuterUD'		:		{p:'ebOuterRGT_UP',		n:'ebOuterRGT_DN'}}


		self.lidLFT = 	{'NAME':' lidLFT', 'SIDE':'LFT',
						'01upLidUD'			:		{p:'upLidLFT_UP',
													n:['upLidLFT_DN_inb01', 'upLidLFT_DN_inb02', 'upLidLFT_DN_inb03', 'upLidLFT_DN']},
						'02loLidUD'			:		{p:['loLidLFT_UP_inb01', 'loLidLFT_UP_inb02', 'loLidLFT_UP_inb03', 'loLidLFT_UP'],
													n:'loLidLFT_DN'},
						'03upLidTW'			:		{p:'upLidTWLFT_LFT',	n:'upLidTWLFT_RGT'},
						'04loLidTW'			:		{p:'loLidTWLFT_LFT',	n:'loLidTWLFT_RGT'}}

		self.lidRGT = 	{'NAME':' lidRGT', 'SIDE':'RGT',
						'01upLidUD'			:		{p:'upLidRGT_UP',
													n:['upLidRGT_DN_inb01', 'upLidRGT_DN_inb02', 'upLidRGT_DN_inb03', 'upLidRGT_DN']},
						'02loLidUD'			:		{p:['loLidRGT_UP_inb01', 'loLidRGT_UP_inb02', 'loLidRGT_UP_inb03', 'loLidRGT_UP'],
													n:'loLidRGT_DN'},
						'03upLidTW'			:		{p:'upLidTWRGT_LFT',	n:'upLidTWRGT_RGT'},
						'04loLidTW'			:		{p:'loLidTWRGT_LFT',	n:'loLidTWRGT_RGT'}}

		self.face = 	{'NAME':' face', 'SIDE':'',
						'01faceSqSt'		:	{p:'faceSquash',	n:'faceStretch'}}

		self.mouthLFT = {'NAME':'mouthLFT', 'SIDE':'LFT',
						'01crnrMouthOI'		:		{p:'crnrMouthLFT_OUT',		n:'crnrMouthLFT_IN'},
						'02crnrMouthUD'		:		{p:'crnrMouthLFT_UP',		n:'crnrMouthLFT_DN'},
						'03puffOI'			:		{p:'puffLFT_OUT',			n:'puffLFT_IN'},

						'04upLipUD'			:		{p:'upLipLFT_UP',			n:'upLipLFT_DN'},
						'05loLipUD'			:		{p:'loLipLFT_UP',			n:'loLipLFT_DN'},
						'06upLipCrnrUD'		:		{p:'upLipCrnrLFT_UP',		n:'upLipCrnrLFT_DN'},
						'06loLipCrnrUD'		:		{p:'loLipCrnrLFT_UP',		n:'loLipCrnrLFT_DN'},

						'07lipsPartOI'		:		{p:'lipsPartLFT_OUT',		n:'lipsPartLFT_IN'},

						'08cheekUprUD'		:		{p:'cheekUprLFT_UP',		n:'cheekUprLFT_DN'},

						'09cheekUprOI'		:		{p:'cheekUprLFT_OUT',		n:'cheekUprLFT_IN'},
						'10cheekLwrOI'		:		{p:'cheekLwrLFT_OUT',		n:'cheekLwrLFT_IN'}}
						
		self.mouthRGT = {'NAME':'mouthRGT', 'SIDE':'RGT',
						'01crnrMouthOI'		:		{p:'crnrMouthRGT_OUT',		n:'crnrMouthRGT_IN'},
						'02crnrMouthUD'		:		{p:'crnrMouthRGT_UP',		n:'crnrMouthRGT_DN'},
						'03puffOI'			:		{p:'puffRGT_OUT',			n:'puffRGT_IN'},

						'04upLipUD'			:		{p:'upLipRGT_UP',			n:'upLipRGT_DN'},
						'05loLipUD'			:		{p:'loLipRGT_UP',			n:'loLipRGT_DN'},
						'06upLipCrnrUD'		:		{p:'upLipCrnrRGT_UP',		n:'upLipCrnrRGT_DN'},
						'06loLipCrnrUD'		:		{p:'loLipCrnrRGT_UP',		n:'loLipCrnrRGT_DN'},

						'07lipsPartOI'		:		{p:'lipsPartRGT_OUT',		n:'lipsPartRGT_IN'},

						'08cheekUprUD'		:		{p:'cheekUprRGT_UP',		n:'cheekUprRGT_DN'},
				
						'09cheekUprOI'		:		{p:'cheekUprRGT_OUT',		n:'cheekUprRGT_IN'},
						'10cheekLwrOI'		:		{p:'cheekLwrRGT_OUT',		n:'cheekLwrRGT_IN'}}

		self.mouth = {'NAME':'mouth', 'SIDE':'',
						'01mouthLR'			:		{p:'mouth_LFT',			n:'mouth_RGT'},
						'02mouthUD'			:		{p:'mouth_UP',			n:'mouth_DN'},
						'03mouthTW'			:		{p:'mouthTW_LFT',	n:'mouthTW_RGT'},

						'04upLipAllUD'		:		{p:'upLipAll_UP',		n:'upLipAll_DN'},
						'05loLipAllUD'		:		{p:'loLipAll_UP',		n:'loLipAll_DN'},
						'06upLipUD'			:		{p:'upLipMid_UP',		n:'upLipMid_DN'},
						'07loLipUD'			:		{p:'loLipMid_UP',		n:'loLipMid_DN'},

						'08upLipRollOI'		:		{p:'upLipRoll_OUT',		n:'upLipRoll_IN'},
						'09loLipRollOI'		:		{p:'loLipRoll_OUT',		n:'loLipRoll_IN'},

						'10upLipPullOI'		:		{p:'upLipPull_OUT',		n:'upLipPull_IN'},
						'11loLipPullOI'		:		{p:'loLipPull_OUT',		n:'loLipPull_IN'},

						'12upLipCleftUD'	:		{p:'upLipCleft_UP',		n:'upLipCleft_DN'},

						'13lipsPress'		:		{p:'mouthPress_IN',		n:'mouthPress_OUT'},

						'14mouthU'			:		{p:'mouthU',			n:''}}

		self.getParts()

class AnimalNoMouth(btp.BtpBase):

	def __init__(self, gap=5):
		btp.BtpBase.__init__(self, gap)

		self.eyeBrowLFT = {'NAME':'eyeBrowLFT', 'SIDE':'LFT',
						'01ebIO'			:		{p:'ebLFT_OUT',			n:'ebLFT_IN'},
						'02ebUD'			:		{p:'ebLFT_UP',			n:'ebLFT_DN'},
						'03ebTurn'			:		{p:'ebTurnLFT_OUT',		n:'ebTurnLFT_IN'},
						'04ebInnerIO'		:		{p:'ebInnerLFT_IN',		n:'ebInnerLFT_OUT'},
						'05ebInnerTurn'		:		{p:'ebInnerTurnLFT_OUT',n:'ebInnerTurnLFT_IN'},
						'06ebInnerUD'		:		{p:'ebInnerLFT_UP',		n:'ebInnerLFT_DN'},
						'07ebMidUD'			:		{p:'ebMidLFT_UP',		n:'ebMidLFT_DN'},
						'08ebOuterUD'		:		{p:'ebOuterLFT_UP',		n:'ebOuterLFT_DN'}}

		self.eyeBrowRGT = {'NAME':'eyeBrowRGT', 'SIDE':'RGT',
						'01ebIO'			:		{p:'ebRGT_OUT',			n:'ebRGT_IN'},
						'02ebUD'			:		{p:'ebRGT_UP',			n:'ebRGT_DN'},
						'03ebTurn'			:		{p:'ebTurnRGT_OUT',		n:'ebTurnRGT_IN'},
						'04ebInnerIO'		:		{p:'ebInnerRGT_IN',		n:'ebInnerRGT_OUT'},
						'05ebInnerTurn'		:		{p:'ebInnerTurnRGT_OUT',n:'ebInnerTurnRGT_IN'},
						'06ebInnerUD'		:		{p:'ebInnerRGT_UP',		n:'ebInnerRGT_DN'},
						'07ebMidUD'			:		{p:'ebMidRGT_UP',		n:'ebMidRGT_DN'},
						'08ebOuterUD'		:		{p:'ebOuterRGT_UP',		n:'ebOuterRGT_DN'}}


		self.lidLFT = 	{'NAME':' lidLFT', 'SIDE':'LFT',
						'01upLidUD'			:		{p:'upLidLFT_UP',
													n:['upLidLFT_DN_inb01', 'upLidLFT_DN_inb02', 'upLidLFT_DN_inb03', 'upLidLFT_DN']},
						'02loLidUD'			:		{p:['loLidLFT_UP_inb01', 'loLidLFT_UP_inb02', 'loLidLFT_UP_inb03', 'loLidLFT_UP'],
													n:'loLidLFT_DN'},
						'03upLidTW'			:		{p:'upLidTwLFT_OUT',	n:'upLidTwLFT_IN'},
						'04loLidTW'			:		{p:'loLidTwLFT_OUT',	n:'loLidTwLFT_IN'}}

		self.lidRGT = 	{'NAME':' lidRGT', 'SIDE':'RGT',
						'01upLidUD'			:		{p:'upLidRGT_UP',
													n:['upLidRGT_DN_inb01', 'upLidRGT_DN_inb02', 'upLidRGT_DN_inb03', 'upLidRGT_DN']},
						'02loLidUD'			:		{p:['loLidRGT_UP_inb01', 'loLidRGT_UP_inb02', 'loLidRGT_UP_inb03', 'loLidRGT_UP'],
													n:'loLidRGT_DN'},
						'03upLidTW'			:		{p:'upLidTwRGT_OUT',	n:'upLidTwRGT_IN'},
						'04loLidTW'			:		{p:'loLidTwRGT_OUT',	n:'loLidTwRGT_IN'}}

		self.getParts()

class ExtraEyeLid(btp.BtpBase):

	def __init__(self, gap=5):
		btp.BtpBase.__init__(self, gap)
		self.lidLFT = 	{'NAME':' lidLFT', 'SIDE':'LFT',
						'01upLidUD'			:		{p:'upLidLFT_UP',
													n:['upLidLFT_DN_inb01', 'upLidLFT_DN_inb02', 'upLidLFT_DN_inb03', 'upLidLFT_DN']},
						'02loLidUD'			:		{p:['loLidLFT_UP_inb01', 'loLidLFT_UP_inb02', 'loLidLFT_UP_inb03', 'loLidLFT_UP'],
													n:'loLidLFT_DN'},
						'03upLidTW'			:		{p:'upLidTwLFT_OUT',	n:'upLidTwLFT_IN'},
						'04loLidTW'			:		{p:'loLidTwLFT_OUT',	n:'loLidTwLFT_IN'}}

		self.lidRGT = 	{'NAME':' lidRGT', 'SIDE':'RGT',
						'01upLidUD'			:		{p:'upLidRGT_UP',
													n:['upLidRGT_DN_inb01', 'upLidRGT_DN_inb02', 'upLidRGT_DN_inb03', 'upLidRGT_DN']},
						'02loLidUD'			:		{p:['loLidRGT_UP_inb01', 'loLidRGT_UP_inb02', 'loLidRGT_UP_inb03', 'loLidRGT_UP'],
													n:'loLidRGT_DN'},
						'03upLidTW'			:		{p:'upLidTwRGT_OUT',	n:'upLidTwRGT_IN'},
						'04loLidTW'			:		{p:'loLidTwRGT_OUT',	n:'loLidTwRGT_IN'}}

		self.getParts()