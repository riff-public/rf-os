# import maya.OpenMaya as om
import maya.OpenMaya as om
import maya.cmds as mc
import os, struct, time

import nuTools.plugins.mddRead as mddRead
reload(mddRead)

mddReadPluginPath = '%s\\mddRead.py' %os.path.dirname(mddRead.__file__)
try:
	if not mc.pluginInfo('mddRead.py',q=1,loaded=1):
		mc.loadPlugin(mddReadPluginPath, quiet=1)
except Exception, e:
	om.MGlobal.displayError('Cannot load MDD read plugin from  %s' %mddReadPluginPath)
	raise e


def closeFileError(mddFile):
	'''
	If a file fails, this replaces it with 1 char, better not remove it?
	'''

	# get the path and close
	filePath = mddFile.name
	mddFile.close()

	# re-open and write blank
	file = open(filepath, 'w')
	file.write('\n')
	file.close()
	om.MGlobal.displayError('Number of verts has changed during animation cannot export.')


def getVertexPositions(mfnMesh):
	vertexPositionList = []
	mPoints = om.MPointArray()
	mfnMesh.getPoints(mPoints, om.MSpace.kWorld)
	# startTime = time.time()
	for i in xrange(mPoints.length()):
		vertexPositionList.extend([mPoints[i].x, mPoints[i].y, mPoints[i].z])

	# print time.time() - startTime   
	return vertexPositionList


def getMfnMesh(obj):
	''' 
		Return MFnMesh of the given object name.
		args :
			obj = Object name. (str)
		return:
			OpenMaya.MFnMesh of the object

	'''

	try:
		mSel = om.MSelectionList()
		mSel.add(obj)
		dMesh = om.MDagPath()
		mSel.getDagPath(0, dMesh) 
		dMesh.extendToShape()
		return om.MFnMesh(dMesh)
	except:
		om.MGlobal.displayError('OpenMaya.MDagPath() failed on %s.' % obj)
		return None


def mddWrite(filePath, meshShape, startFrame, endFrame, fps):

	# filePath = difName+"/"+basename+".mdd"

	mddFile = open(filePath, 'wb')

	# set time to start frame
	mc.currentTime(startFrame, edit=True)

	# get MfnMesh
	mfnMesh = getMfnMesh(meshShape)

	# get num verts. this value must be consistant
	numVerts = mfnMesh.numVertices()

	# get vert position
	vertexPositionList = getVertexPositions(mfnMesh)

	# get num frame and num verts
	numFrames = (endFrame - startFrame) + 1

	## write the header
	mddFile.write(struct.pack(">2i", numFrames, numVerts))

	# write the frame times, sec
	mddFile.write(struct.pack(">%df" % (numFrames), *[frame/float(fps) for frame in xrange(numFrames)]) ) # seconds	

	# write out referece model vertex position
	mddFile.write(struct.pack(">%df" % (numVerts*3), *[v for v in vertexPositionList]))


	vertexPositionList = []
	for frame in xrange(startFrame, endFrame+1):
		mc.currentTime(frame, edit=True)

		# Check vertex  count, its shouldnt be changed over time
		if mfnMesh.numVertices() != numVerts:
			closeFileError(mddFile)
			return

		vertexPositionList = getVertexPositions(mfnMesh)

		# Write the vertex data
		mddFile.write(struct.pack(">%df" % (numVerts*3), *[v for v in vertexPositionList]))

	vertexPositionList = None
	mddFile.close()



def mddRead(filePath, meshShape, cycle=False, offset=0, **kwargs):
	# apply deformer
	mddNode = mc.deformer(meshShape, type='mddRead', **kwargs)[0]

	# connect time1
	mc.connectAttr('time1.outTime', '%s.time' %mddNode, f=True)

	# set mdd file path
	mc.setAttr('%s.mddFile' %mddNode, filePath, type='string')
	mc.setAttr('%s.cycle' %mddNode, cycle)
	mc.setAttr('%s.offset' %mddNode, offset)


