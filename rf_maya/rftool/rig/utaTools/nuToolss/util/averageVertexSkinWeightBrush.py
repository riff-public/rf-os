import maya.mel as mel
from maya.cmds import pluginInfo as pluginInfo
from maya.cmds import loadPlugin as loadPlugin

pluginNames = ['averageVertexSkinWeightCmd.mll', 'averageVertexSkinWeightCmd.py']

for pluginName in pluginNames:
	try:
		if not pluginInfo(pluginName, q=True, l=True):
			loadPlugin(pluginName, qt=True)
		break
	except:
		continue


def initPaint():
	cmd = '''
	global proc averageVertexWeightBrush(string $context) {
		artUserPaintCtx -e 
		-ic "init_averageVertexWeightBrush"
		-svc "set_averageVertexWeightBrush"
		-sao "absolute"
		$context;
	}

	global proc init_averageVertexWeightBrush(string $name) {}

	global proc set_averageVertexWeightBrush(int $slot, int $index, float $val) {		
		string $selVtxs[] = `filterExpand -fp true -sm 31`;
		if(size($selVtxs) > 0) {
			int $i = 0;
			string $vtxIndexes[];
			for($vtx in $selVtxs) {
					string $buffer[];
					tokenize $vtx ".[]" $buffer;
					$vtxIndexes[$i] = $buffer[2];
					$i++;
			}
			if(!stringArrayContains($index, $vtxIndexes)) return; 

		}

		averageVertexSkinWeight -i $index -v $val;     
	}
	'''
	mel.eval(cmd)



def paint():
	cmd = '''
	ScriptPaintTool;
	artUserPaintCtx -e -tsc "averageVertexWeightBrush" `currentCtx`;
	'''
	mel.eval(cmd)


initPaint()

