# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'O:/studioTools/maya/python/tool/rig/nuTools/util/igPicker/ui/cmdDialog.ui'
#
# Created: Tue Apr 05 16:22:46 2016
#      by: pyside-uic 0.2.14 running on PySide 1.2.0
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_cmd_dialog(object):
    def setupUi(self, cmd_dialog):
        cmd_dialog.setObjectName("cmd_dialog")
        cmd_dialog.resize(450, 200)
        self.gridLayout = QtGui.QGridLayout(cmd_dialog)
        self.gridLayout.setObjectName("gridLayout")
        self.cmd_textEdit = QtGui.QTextEdit(cmd_dialog)
        self.cmd_textEdit.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))
        self.cmd_textEdit.setFrameShape(QtGui.QFrame.WinPanel)
        self.cmd_textEdit.setLineWrapMode(QtGui.QTextEdit.NoWrap)
        self.cmd_textEdit.setObjectName("cmd_textEdit")
        self.gridLayout.addWidget(self.cmd_textEdit, 0, 0, 1, 1)
        self.main_buttonBox = QtGui.QDialogButtonBox(cmd_dialog)
        self.main_buttonBox.setOrientation(QtCore.Qt.Vertical)
        self.main_buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.main_buttonBox.setObjectName("main_buttonBox")
        self.gridLayout.addWidget(self.main_buttonBox, 0, 1, 1, 1)
        self.language_horizontalLayout = QtGui.QHBoxLayout()
        self.language_horizontalLayout.setSpacing(50)
        self.language_horizontalLayout.setContentsMargins(50, -1, -1, -1)
        self.language_horizontalLayout.setObjectName("language_horizontalLayout")
        self.mel_radioButton = QtGui.QRadioButton(cmd_dialog)
        self.mel_radioButton.setChecked(True)
        self.mel_radioButton.setObjectName("mel_radioButton")
        self.language_horizontalLayout.addWidget(self.mel_radioButton)
        self.python_radioButton = QtGui.QRadioButton(cmd_dialog)
        self.python_radioButton.setObjectName("python_radioButton")
        self.language_horizontalLayout.addWidget(self.python_radioButton)
        self.gridLayout.addLayout(self.language_horizontalLayout, 1, 0, 1, 1)

        self.retranslateUi(cmd_dialog)
        QtCore.QObject.connect(self.main_buttonBox, QtCore.SIGNAL("accepted()"), cmd_dialog.accept)
        QtCore.QObject.connect(self.main_buttonBox, QtCore.SIGNAL("rejected()"), cmd_dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(cmd_dialog)

    def retranslateUi(self, cmd_dialog):
        cmd_dialog.setWindowTitle(QtGui.QApplication.translate("cmd_dialog", "Bind command button", None, QtGui.QApplication.UnicodeUTF8))
        self.mel_radioButton.setText(QtGui.QApplication.translate("cmd_dialog", "MEL", None, QtGui.QApplication.UnicodeUTF8))
        self.python_radioButton.setText(QtGui.QApplication.translate("cmd_dialog", "Python", None, QtGui.QApplication.UnicodeUTF8))

