import maya.OpenMaya as om
import maya.mel as mel
from functools import partial
import collections
import maya.cmds as mc
# import pymel.core as pm
import time

DEFAULT_TOL = 0.001

class SymMeshReflector(object):
    """
    Tool for mirroring mesh modification on one side to the other.
    By given a symmetry mesh once, the script can manipulate another geometry with the same 
    topology. 

    """

    def __init__(self, tol=DEFAULT_TOL):

        #UI vars
        self.WINDOW_TITLE = 'Symmetry Mesh Reflector v.1.0'
        # self.WINDOW_NAME = 'smrWin'

        #global vars
        self.tol = tol
        self.baseVtxNum = 0
        self.filterBaseGeo = True
        self.revertSliderDragging = False

        # base mesh vars
        self.baseMesh = None
        self.baseMeshData = []
        self.meshCenter = []
        
        # self.posIndx = []
        # self.negIndx = []
        # self.midIndx = []
        self.noneIndx = []
        self.filterIndx = {'pos':[], 'neg':[]}
        # self.midNegIndx = []

        self.midDict = {}
        self.pairDict = {}

        # multiple action vars
        # self.focusVtxList = []
        # self.focusIndxList = []
        self.focusVtx = collections.defaultdict(list)  #{mesh:[vtx1, vtx2, ...]}
        self.focusIndx = collections.defaultdict(list)  #{mesh:[indx1, indx2, ...]}

        # meshA vars
        self.meshA = None
        self.meshAData = []
        self.meshACenter = []

        # self.posAIndx = []
        # self.negAIndx = []
        # self.midAIndx = []
        self.filterAIndx = {'pos':[], 'neg':[]}
        self.noneAIndx = []
        self.midAPosIndx = []
        self.midANegIndx = []

        self.midADict = {}
        self.pairADict = {}

        # meshA action vars
        self.movedAVtxs = []
        self.movedAIndxs = []
        self.meshAFocusVtxs = []
        self.meshAFocusIndxs = []

        # ui
        self.statusTxt = None


    def UI(self):
        """
        The maya.cmds UI function. 

        """
        # if mc.window(self.WINDOW_NAME, ex=True):
        #     mc.deleteUI(self.WINDOW_NAME, window=True)

        # mc.window(self.WINDOW_NAME, title=self.WINDOW_TITLE, s=False, mnb=True, mxb=False)
        mc.window(title=self.WINDOW_TITLE, s=False, mnb=True, mxb=False)
        mc.columnLayout(adj=True, rs=0, co=['both', 0])
        mc.frameLayout(label='by Nuternativ : nuttynew@hotmail.com', bs='out', fn='smallObliqueLabelFont', mh=5, mw=5)

        mc.rowColumnLayout(nc=2, co=([1, 'left', 95], [2, 'left', 5]))
        mc.text(l='tolerance')
        self.tolFloatField = mc.floatField(w=58, v=DEFAULT_TOL, pre=6, min=0.0, cc=partial(self.changeTol))
        mc.setParent('..')

        mc.columnLayout(adj=True, rs=5, co=['both', 50])
        self.statusTxt = mc.text(l='No Base Mesh Data.', bgc=[0.4, 0, 0], h=25)
        mc.button(l='Get Base Mesh Data', c=partial(self.caller, 'analyzeBaseGeo'), h=35)
        mc.setParent('..')

        mc.columnLayout(adj=True, rs=5, co=['left', 105])
        self.filterBaseGeoChkBox = mc.checkBox(l='filter selection', v=True, cc=partial(self.filterBaseGeoChangeValue))
        mc.setParent('..')

        mc.rowColumnLayout(nc=2, co=([1, 'left', 50], [2, 'left', 10]))
        mc.button(l='Check Symmetry', h=20, w=100, c=partial(self.caller, 'checkSymmetry'))
        mc.button(l='Sel Moved', h=20, w=70, c=partial(self.caller, 'getMovedVtx'))
        mc.setParent('..')
            
        mc.rowColumnLayout(nc=2, co=([1, 'left', 10], [2, 'left', 3]))
        self.revertFloatFld = mc.floatField(v=100, max=100, min=0, pre=2, w=42, cc=partial(self.fillRevertMesh))
        self.revertSlider = mc.floatSliderGrp(v=100, max=100, min=0, cw2=[200, 10], f=False, pre=2, el='%',
                            dc=partial(self.dragRevertMesh), cc=partial(self.endDragRevertMesh))
        mc.setParent('..')

        mc.columnLayout(adj=True, rs=5, co=['both', 80])
        mc.button(l='Revert to Base', c=partial(self.caller, 'revertToBase'))
        mc.setParent('..')

        mc.frameLayout( label='Mirror Mesh', bs='etchedIn', mh=5, mw=5, cll=True, cl=False)
        mc.rowColumnLayout(nc=3, co=([1, 'left', 5], [2, 'left', 10], [3, 'left', 10]))
        mc.button(l='M >>', w=80, h=30, c=partial(self.caller, 'mirror_neg'))
        mc.button(l='Flip', w=80, h=30, c=partial(self.caller, 'flip'))
        mc.button(l='<< M', w=80, h=30, c=partial(self.caller, 'mirror_pos'))
        mc.setParent('..')
        mc.setParent('..')

        mc.frameLayout( label='Copy Mesh', bs='etchedIn', mh=5, mw=5, cll=True, cl=False)
        mc.rowColumnLayout(nc=3, co=([1, 'left', 5], [2, 'left', 10], [3, 'left', 10]))
        mc.button(l='Subtract', w=80, h=25, c=partial(self.caller, 'subtractFromMeshA'))
        mc.button(l='Copy', w=80, h=25, c=partial(self.caller, 'copyFromMeshA'))
        mc.button(l='Add', w=80, h=25, c=partial(self.caller, 'addFromMeshA'))
        mc.setParent('..')

        mc.showWindow()



    def addFromMeshA(self):
        """
        Add self.meshA modifications to each items in self.focusVtx.

        """
        mel.eval('undoInfo -ock;')
        for mesh in self.focusVtx:
            #get all vtx current position
            currTrans = self.getCurrOsTrans(mesh)

            for v, i in zip(self.focusVtx[mesh], self.focusIndx[mesh]):
                if self.checkEqualPoint(self.meshAData[i], self.baseMeshData[i]) == True:
                    continue

                relTrans = self.getRelativeTrans(self.meshAData[i], self.baseMeshData[i])
                mel.eval('move -ls %f %f %f %s;' %(currTrans[i][0]+relTrans[0], currTrans[i][1]+relTrans[1], currTrans[i][2]+relTrans[2], v))
        mel.eval('undoInfo -cck;')


    def getGeoData(self, mesh=None):
        """
        Get all vertices position for self.mesh (no mirroring or pairing data)

        """

        if not mesh:
            try: 
                mesh = mel.eval('ls -sl -l')[0]
            except:
                return

        self.baseMeshData = []

        #iterate thru each mesh 
        mfnMesh = self.getMFnMesh(obj=mesh)
        pArray = om.MPointArray()   
        mfnMesh.getPoints(pArray, om.MSpace.kObject)

        for i in xrange(0, pArray.length()):
            self.baseMeshData.append([pArray[i].x, pArray[i].y, pArray[i].z])



    def analyzeBaseGeo(self, mesh=None):
        """
        Analyze a mesh to be used as base geometry. Collect all data, pair vtxs, mids, etc.. 

        """

        #reset base mesh data
        self.baseMesh = None
        self.baseMeshData = []
        posIndx, negIndx, self.noneIndx = [], [], []
        self.filterIndx = {'pos':[], 'neg':[]}
        self.meshCenter = []
        self.pairDict = {}
        self.midDict = {}

        if not mesh:
            try: 
                mesh = mel.eval('ls -sl -l')[0]
            except:
                om.MGlobal.displayWarning('Please select base mesh.')
                return 

        self.baseMesh = mesh

        #iterate thru each mesh 
        try:
            mfnMesh = self.getMFnMesh(obj=self.baseMesh)
        except:
            return
        
        if not mfnMesh:
            return

        pArray = om.MPointArray()   
        mfnMesh.getPoints(pArray, om.MSpace.kObject)
        pos, neg = [], []

        #finding the mesh center
        self.meshCenter = mel.eval('objectCenter -l %s;' %self.baseMesh)

        #for each vertex, store all in orig and get those in the middle
        self.baseVtxNum = pArray.length()

        for i in xrange(0, self.baseVtxNum):
            self.baseMeshData.append([pArray[i].x, pArray[i].y, pArray[i].z])
            self.midDict[i] = False

            #found that it is in the mid
            if abs(pArray[i].x - self.meshCenter[0]) <= self.tol:
                self.filterIndx['pos'].append(i)
                self.filterIndx['neg'].append(i)
                self.midDict[i] = True
                continue

            if pArray[i].x > self.meshCenter[0]:
                pos.append(i)
            else:
                neg.append(i)

        #matching left and right vertex
        for p in pos:
            mirrorPos = om.MPoint((pArray[p].x*-1) + (2*self.meshCenter[0]), pArray[p].y, pArray[p].z)
            for n in neg:
                if pArray[n].distanceTo(mirrorPos) <= self.tol:
                    self.filterIndx['neg'].append(n)
                    self.filterIndx['pos'].append(p)
                    self.pairDict[p] = n
                    self.pairDict[n] = p
                    neg.pop(neg.index(n))
                    break
            else:
                self.noneIndx.append(p)
            # else:
                # cut out neg vert that has already matched with pos for more speed

        #add negative verts that'was left unmatched to none    
        self.noneIndx.extend(neg)

        # self.filterIndx['pos'] = midIndx.extend(posIndx)
        # self.filterIndx['neg'] = midIndx.extend(negIndx)

        if self.noneIndx:
            nonSymVtxs = self.getVtxFromIndx(self.baseMesh, self.noneIndx)
            toSelect = ' '.join('"{0}"'.format(v) for v in nonSymVtxs)
            mel.eval('select -r %s;' %toSelect)
            om.MGlobal.displayWarning('Mesh is NOT symmetrical. Not all vertex can be mirrored.')
        else:
            mel.eval('select %s;' %self.baseMesh)
            om.MGlobal.displayInfo('Mesh is symmetrical.')



    def analyzeAGeo(self, mesh=None):
        """
        Analyze a mesh to be used as self.meshA. Collect all data, pair vtxs, mids, etc.. 
        Warn user if the mesh is not all symmetry. Called by 'check symmetry' action.

        """

        if not mesh:
            try: 
                mesh = mel.eval('ls -sl -l')[0]
            except:
                return 

        #reset data
        self.meshAData = []
        posAIndx, negAIndx, midAIndx = [], [], []
        self.noneAIndx = []
        self.filterAIndx = {'pos':[], 'neg':[]}
        self.meshACenter = []
        self.pairADict = {}
        self.midADict = {}

        #iterate thru each mesh 
        try:
            mfnMesh = self.getMFnMesh(obj=mesh)
        except:
            return

        if not mfnMesh:
            return

        pArray = om.MPointArray()   
        mfnMesh.getPoints(pArray, om.MSpace.kObject)
        pos, neg = [], []

        #finding the mesh center
        # self.meshACenter = mel.eval('objectCenter -l %s;' %mesh)
        meshTrans = mel.eval('listRelatives -p -f %s' %mesh)[0]
        self.meshACenter = mel.eval('xform -q -rp %s' %meshTrans)

        #for each vertex, store all in orig and get those in the middle
        vtxNum = pArray.length()

        for i in xrange(0, vtxNum):
            self.meshAData.append([pArray[i].x, pArray[i].y, pArray[i].z])
            self.midADict[i] = False

            #found that it is in the mid
            if abs(pArray[i].x - self.meshACenter[0]) <= self.tol:
                self.filterAIndx['pos'].append(i)
                self.filterAIndx['neg'].append(i)
                self.midADict[i] = True
                continue

            if pArray[i].x > self.meshACenter[0]:
                pos.append(i)
            else:
                neg.append(i)

        #matching left and right vertex
        for p in pos:
            find = om.MPoint((pArray[p].x*-1)+(2*self.meshACenter[0]), pArray[p].y, pArray[p].z)
            for n in neg:
                if pArray[n].distanceTo(find) <= self.tol:
                    self.filterAIndx['neg'].append(n)
                    self.filterAIndx['pos'].append(p)
                    self.pairADict[p] = n
                    self.pairADict[n] = p
                    neg.pop(neg.index(n))
                    break
            else:
                self.noneAIndx.append(p)


        #add negative verts that'was left unmatched to none    
        self.noneAIndx.extend(neg)

        # self.midAPosIndx = self.midAIndx + self.posAIndx
        # self.midANegIndx = self.midAIndx + self.negAIndx

        if self.noneAIndx:
            nonSymVtxs = self.getVtxFromIndx(mesh, self.noneAIndx)
            toSelect = ' '.join('"{0}"'.format(v) for v in nonSymVtxs)
            mel.eval('select -r %s;' %toSelect)
            om.MGlobal.displayWarning('Mesh is NOT symmetrical. Not all vertex can be mirrored.')
        else:
            mel.eval('select %s;' %mesh)
            om.MGlobal.displayInfo('Mesh is symmetrical.')



    def caller(self, op, sels=[]):
        """
        Caller function for all the actions.

        """

        mel.eval('waitCursor -state on;')
        # startTime = time.time()

        if op in ['mirror_pos', 'mirror_neg']:
            if self.getUserSelection(sels) == True:
                self.mirrorVtx(filterIndxList=self.filterIndx[op.split('_')[-1]])
            else:
                om.MGlobal.displayError('Mirror : Select a mesh to mirror.')

        elif op == 'revertToBase':
            if self.getUserSelection(sels) == True:
                self.revertVtxToBase()

        elif op == 'flip':
            if self.getUserSelection(sels) == True:
                self.flipVtx(filterIndxList=self.filterIndx['pos'])

        elif op == 'flipApi':
            if self.getUserSelection(sels) == True:
                self.flipVtxApi()

        elif op == 'copyFromMeshA':
            if self.getMeshASelection(child=True) == True:
                self.getAGeoData(mesh=self.meshA)
                self.copyFromMeshA()
                mc.select(self.focusVtx.keys())
            else:
                om.MGlobal.displayError('Copy : Select the source mesh, then destination meshes with the same vertex count.')

        elif op == 'addFromMeshA':
            if self.getMeshASelection(child=True) == True:
                self.getAGeoData(mesh=self.meshA)
                self.addFromMeshA()
                mc.select(self.focusVtx.keys())
            else:
                om.MGlobal.displayError('Add : Select the source mesh, then destination meshes with the same vertex count.')

        elif op == 'subtractFromMeshA':
            if self.getMeshASelection(child=True) == True:
                self.getAGeoData(mesh=self.meshA)
                self.subtractFromMeshA()
                mc.select(self.focusVtx.keys())
            else:
                om.MGlobal.displayError('Subtract : Select the source mesh, then destination meshes with the same vertex count.')

        elif op == 'checkSymmetry':
            self.getMeshASelection(child=False)
            self.analyzeAGeo(mesh=self.meshA)
            # else:
                # om.MGlobal.displayError('Check symmetry : Invalid selection.')

        elif op == 'getMovedVtx':
            if self.getUserSelection(sels) == True:
                self.getMovedVtx(sel=True)

        elif op == 'analyzeBaseGeo':
            self.analyzeBaseGeo()
            try:
                if self.baseMesh and self.baseMeshData:
                    self.setStatusTxt(True)
                else:
                    self.setStatusTxt(False)
            except: pass
        

        mel.eval('waitCursor -state off;')


    def copyFromMeshA(self):
        """
        Copy self.meshA modifications to each items in self.focusVtx.

        """
        mel.eval('undoInfo -ock;')
        for mesh in self.focusVtx:
            #get all vtx current position
            currTrans = self.getCurrOsTrans(mesh)

            for v, i in zip(self.focusVtx[mesh], self.focusIndx[mesh]):
                if self.checkEqualPoint(currTrans[i], self.meshAData[i]) == True:
                    continue
                mel.eval('move -ls %f %f %f %s;' %(self.meshAData[i][0], self.meshAData[i][1], self.meshAData[i][2], v))
        mel.eval('undoInfo -cck;')


    def checkSymPoint(self, pointA, pointB):
        """
        Accept 2 point positions(A and B). Calculate if pointB is the mirror of point A under the tolerance.
        Return: boolean

        """

        mPointA = om.MPoint(pointA[0], pointA[1], pointA[2])
        mPointB = om.MPoint(pointB[0], pointB[1], pointB[2])
        mirMPoint = om.MPoint((pointA[0]*-1)+(2*self.meshCenter[0]), pointA[1], pointA[2])
        if mPointB.distanceTo(mirMPoint) <= self.tol:
            return True
        else:
            return False



    def checkEqualPoint(self, pointA, pointB):
        """
        Accept 2 points position, see if pointA is equal to point B under the tolerance.
        Return: boolean

        """

        mPointA = om.MPoint(pointA[0], pointA[1], pointA[2])
        mPointB = om.MPoint(pointB[0], pointB[1], pointB[2])
        if mPointA.distanceTo(mPointB) <= self.tol:
            return True
        else:
            return False



    def changeTol(self, *args):
        self.tol = mc.floatField(self.tolFloatField, q=True, v=True)



    def dragRevertMesh(self, *args):
        if self.revertSliderDragging == False:
            self.revertSliderDragging = True
            if self.getMeshASelection(child=False) == True:
                self.getAGeoData(self.meshA)
                self.getMovedVtxMeshA()
            mel.eval('undoInfo -ock;')

        percent = mc.floatSliderGrp(self.revertSlider, q=True, value=True)

        #set the float field
        mc.floatField(self.revertFloatFld, e=True, value=percent)

        self.revertVtxByPercent(percent=percent)



    def fillRevertMesh(self, *args):
        if self.getMeshASelection(child=False) == True:
            self.getAGeoData(self.meshA)
            self.getMovedVtxMeshA()

        percent = mc.floatField(self.revertFloatFld, q=True, value=True)

        #set the float slider
        # mc.floatSliderGrp(self.revertSlider, e=True, value=percent)   
        self.revertVtxByPercent(percent=percent)

        self.endDragRevertMesh()



    def endDragRevertMesh(self, *args):
        if self.revertSliderDragging == True:        
            mel.eval('undoInfo -cck;')

        #set the float field and float slider back to 100
        mc.floatSliderGrp(self.revertSlider, e=True, value=100)   
        mc.floatField(self.revertFloatFld, e=True, value=100)
        
        self.revertSliderDragging = False



    def filterSelection(self):
        """
        Filter all focus var to be only those that has the same vertex count as self.baseMesh.

        """
        res = True
        toDel = []
        for mesh in self.focusVtx:
            meshVtxNum = mel.eval('polyEvaluate -v %s;' %mesh)[0]
            if meshVtxNum != self.baseVtxNum:
                toDel.append(mesh)

        for mesh in toDel:
            del(self.focusVtx[mesh])
            del(self.focusIndx[mesh])
                # del(self.focusVtx[mesh])
                # del(self.focusIndx[mesh])
                # for vtx, indx in zip(self.focusVtxList, self.focusIndxList):
                #     if vtx.startswith(mesh):
                #         self.focusVtxList.remove(vtx)
                #         self.focusIndxList.remove(indx)

        if not self.focusIndx:
            res = False
        return res


    def filterBaseGeoChangeValue(self, *args):
        self.filterBaseGeo = mc.checkBox(self.filterBaseGeoChkBox, q=True, value=True)



    def flipVtx(self, filterIndxList):
        """
        Flip each focus mesh.
        Args: filterIndexList : List of vertex that will be use as filter for user selection to prevent
                                user from selecting vertex from both sides. 

        """
        # startTime = time.time()
        for mesh in self.focusVtx:
            #get all vtx current position
            currTrans = self.getCurrOsTrans(mesh)

            #filter out vtx
            filterIndxs = list(set(self.focusIndx[mesh]).intersection(filterIndxList))
            filterVtxs = self.getVtxFromIndx(mesh, filterIndxs)

            for v, i in zip(filterVtxs, filterIndxs):

                #if it's in the middle of the mesh
                if self.midDict[i] == True:
                    mel.eval('move -ls %f %f %f %s ' %(currTrans[i][0]*-1, currTrans[i][1], currTrans[i][2], v) )

                #it's on the left or right side
                else:
                    c = self.pairDict[i]

                    # if both verts are already sym, skip to increse speed of moving stady point
                    if self.checkSymPoint(currTrans[i], currTrans[c]) == True:
                        continue

                    cVtx = '%s.vtx[%i]' %(mesh, c)
                    mel.eval('move -ls %f %f %f %s' %(currTrans[i][0]*-1, currTrans[i][1], currTrans[i][2], cVtx))
                    mel.eval('move -ls %f %f %f %s' %(currTrans[c][0]*-1, currTrans[c][1], currTrans[c][2], v))
        # print 'time = %s' %(time.time() - startTime)


    def flipVtxApi(self):
        """
        Flip each focus mesh.
        Args: filterIndexList : List of vertex that will be use as filter for user selection to prevent
                                user from selecting vertex from both sides. 

        """

        for mesh in self.focusVtx:
            #get all vtx current position
            # currTrans = self.getCurrOsTrans(mesh)

            # get MFnMesh 
            mFnMesh = self.getMFnMesh(mesh)
            dagPath = self.getDagPath(mesh)

            # get all points
            vtxPointArray = om.MPointArray()
            mFnMesh.getPoints(vtxPointArray, om.MSpace.kObject)      

            resultPointArray = om.MPointArray()
            geoIterator = om.MItGeometry(dagPath)

            # filterIndxs = list(set(self.focusIndx[mesh]).intersection(filterIndxList))
            cPos = {}
            # i = 0

            while( not geoIterator.isDone()):
                pointPosition = geoIterator.position()
                
                vIndx = geoIterator.index()

                if self.midDict[vIndx] == True:
                    pointPosition.x = pointPosition.x*-1
                    
                else:
                    c = self.pairDict[vIndx]
                    
                    if vIndx in cPos.keys():
                        pointPosition.x = cPos[vIndx].x*-1
                        pointPosition.y = cPos[vIndx].y 
                        pointPosition.z = cPos[vIndx].z
                    else:
                        cPos[c] = vtxPointArray[vIndx]     
                        pointPosition.x = vtxPointArray[c].x*-1
                        pointPosition.y = vtxPointArray[c].y 
                        pointPosition.z = vtxPointArray[c].z 

                resultPointArray.append(pointPosition)    
                geoIterator.next()
                # i += 1

            geoIterator.setAllPositions(resultPointArray)


    def getAGeoData(self, mesh=None):
        """
        Get all vertices position for self.meshA

        """

        if not mesh:
            try: 
                mesh = mel.eval('ls -sl -l')[0]
            except:
                return

        self.meshAData = []

        #iterate thru each mesh 
        mfnMesh = self.getMFnMesh(obj=mesh)
        if not mfnMesh:
            return

        pArray = om.MPointArray()   
        mfnMesh.getPoints(pArray, om.MSpace.kObject)

        for i in xrange(0, pArray.length()):
            self.meshAData.append([pArray[i].x, pArray[i].y, pArray[i].z])



    def getUserSelection(self, sels=[]):
        """
        Get user selection for multiple mesh operations (mirror, flip).
        Stores data to : 
        self.focusVtx       : mesh:[vtx1, vtx2, vtx3, ...]
        self.focusIndx      : mesh:[0, 1, 2, ...]
        self.focusVtxList   : [vtx1, vtx2, vtx3, ...] 
        self.focusIndxList  : [0, 1, 2, ...]

        """

        self.resetFocusData()

        if not sels:
            sels = mel.eval('filterExpand -fp true -ex true -sm 31 -sm 12;')

            if not sels:
                # om.MGlobal.displayError('Select something!')
                return False

        for sel in sels:
            typ = mel.eval('objectType %s;' %sel)
            if typ == 'transform':
                shps = mel.eval('listRelatives -f -ni -shapes -type "mesh" %s;' %sel)
                if shps:
                    shp = shps[0]
                    # if shp not in self.focusVtx:
                    #     self.focusVtx[shp] = []
                    #     self.focusIndx[shp] = []
                    numVtx = mel.eval('polyEvaluate -v %s;' %shp)
                    numVtxRange = xrange(0, numVtx[0])
                    vtxs = self.getVtxFromIndx(shp, numVtxRange)
                    self.focusVtx[shp].extend(vtxs)
                    self.focusIndx[shp].extend(numVtxRange)
                    # self.focusVtxList.extend(vtxs)
                    # self.focusIndxList.extend(numVtxRange)

            elif typ == 'mesh':
                if '.vtx' in sel:
                    splits = sel.split('.vtx')
                    mesh = splits[0]
                    indx = int(splits[-1][1:-1])
                    # if mesh not in self.focusVtx.keys():
                    #     self.focusVtx[mesh] = []
                    #     self.focusIndx[mesh] = []
                    self.focusVtx[mesh].append(sel)
                    self.focusIndx[mesh].append(indx)
                    # self.focusVtxList.append(sel)
                    # self.focusIndxList.append(indx)
                else:
                    # if sel not in self.focusVtx.keys():
                    #     self.focusVtx[sel] = []
                    #     self.focusIndx[sel] = []
                    numVtx = mel.eval('polyEvaluate -v %s;' %sel)
                    # if numVtx: 
                    numVtxRange = xrange(0, numVtx[0])
                    vtxs = self.getVtxFromIndx(sel, numVtxRange)

                    self.focusVtx[sel].extend(vtxs)
                    self.focusIndx[sel].extend(numVtxRange)
                    # self.focusVtxList.extend(vtxs)
                    # self.focusIndxList.extend(numVtxRange)

        res = True
        if self.filterBaseGeo == True:
            res = self.filterSelection()

        return res



    def getMovedVtxMeshA(self):
        """
        Check if self.meshA vertices have moved from the base mesh and store those in self.moveAVtxs and self.moveAIndxs.

        """

        # Clear the vars
        self.movedAVtxs = []
        self.movedAIndxs = []

        for v, i in zip(self.meshAFocusVtxs, self.meshAFocusIndxs):
            if self.checkEqualPoint(self.meshAData[i], self.baseMeshData[i]) == False:
                self.movedAVtxs.append(v)
                self.movedAIndxs.append(i)




    def getMovedVtx(self, sel=True):
        """
        From focus data retrieved from function 'getUserSelection()'. Compare any of the vertices has moved from the original.
        Called by 'Select moved vertices' action.

        """

        toSel = []

        for mesh in self.focusVtx:
            #get all vtx current position
            currTrans = self.getCurrOsTrans(mesh)

            for v, i in zip(self.focusVtx[mesh], self.focusIndx[mesh]):
                if self.checkEqualPoint(currTrans[i], self.baseMeshData[i]) == False:
                    toSel.append(v)

        if sel == True:
            if toSel:
                movedVtxs = ' '.join('"{0}"'.format(v) for v in toSel)
                mel.eval('select -r %s;' %movedVtxs)
            else:
                mel.eval('select -cl;')
                om.MGlobal.displayInfo('No vertex was modified.')



    def getVtxFromIndx(self, mesh, indxs):
        """
        Accept mesh(str) and index(int) combine string to retrieve vertices name.
        Return: List

        """
        return ['%s.vtx[%i]' %(mesh, i) for i in indxs]
        # return map(lambda i: '%s.vtx[%i]' %(mesh, i), indx)



    def getRelativeTrans(self, pointA, pointB):
        """
        Accept 2 point positions(A and B). Subtract B from A to get relative transform.
        Return: list(position)

        """

        relTransX = pointA[0] - pointB[0]
        relTransY = pointA[1] - pointB[1]
        relTransZ = pointA[2] - pointB[2]
        return [relTransX, relTransY, relTransZ]



    def getCurrOsTrans(self, mesh):
        """
        Given mesh name as string, will iterate through each vertex and get its position.
        Return: list([vtx1.x, vtx1.y, vtx1.z], [...], ...)

        """
        mFnMesh = self.getMFnMesh(mesh)
        vtxPointArray = om.MPointArray()    
        mFnMesh.getPoints(vtxPointArray, om.MSpace.kObject)
            
        # return a list off all points positions
        return [[vtxPointArray[i][0], vtxPointArray[i][1], vtxPointArray[i][2]] for i in xrange(0, vtxPointArray.length())]


    def getMFnMesh(self, obj):
        """
        Given mesh name as string, return mfnMesh.

        """

        try:
            msl = om.MSelectionList()
            msl.add(obj)
            nodeDagPath = om.MDagPath()
            msl.getDagPath(0, nodeDagPath)
            return om.MFnMesh(nodeDagPath)
        except:
            return None

        

    def getDagPath(self, obj):
        """
        Given mesh name as string, return mfnMesh.

        """

        try:
            msl = om.MSelectionList()
            msl.add(obj)
            nodeDagPath = om.MDagPath()
            msl.getDagPath(0, nodeDagPath)
        except:
            return

        return nodeDagPath



    def getMeshASelection(self, sels=[], child=False):
        """
        Get user selection for operation that has other mesh involve in operation (subtract, add, copy).
        Stores data to : 
        self.meshA               : mesh 
        self.meshAFocusVtx       : mesh:[vtx1, vtx2, vtx3, ...]
        self.meshAFocusIndx      : mesh:[0, 1, 2, ...]

        """

        self.resetMeshAFocusData()

        if not sels:
            sels = mel.eval('ls -sl -l -fl')

            if not sels:
                om.MGlobal.displayError('Select something!')
                return False

            if len(sels) <= child:
                return False

            if child == True:
                res = self.getUserSelection(sels=sels[1:])
                if res == False:
                    self.resetFocusData()
                    return res

        meshA = sels[0]

        if '.vtx' in meshA:
            splits = meshA.split('.vtx')
            self.meshA = splits[0]
            vtxNum = mel.eval('polyEvaluate -v %s;' %self.meshA)[0]
            for s in sels:
                splits = s.split('.vtx')
                if splits[0] == self.meshA:
                    self.meshAFocusVtxs.append(s)
                    self.meshAFocusIndxs.append(int(splits[-1][1:-1]))
        else:
            typ = mel.eval('objectType %s' %meshA)
            if typ == 'transform':
                shps = mel.eval('listRelatives -ni -f -shapes -type "mesh" %s;' %meshA)
                if shps:
                    self.meshA = shps[0]
                    vtxNum = mel.eval('polyEvaluate -v %s;' %self.meshA)[0]
                    indxRange = xrange(0, vtxNum)
                    self.meshAFocusVtxs = self.getVtxFromIndx(self.meshA, indxRange)
                    self.meshAFocusIndxs = indxRange

            elif typ == 'mesh':
                self.meshA = meshA
                vtxNum = mel.eval('polyEvaluate -v %s;' %self.meshA)[0]
                
                indxRange = xrange(0, vtxNum)
                self.meshAFocusVtxs = self.getVtxFromIndx(self.meshA, indxRange)
                self.meshAFocusIndxs = indxRange

        # check
        # if vtxNum != self.baseVtxNum and check == False:
        #     return False

        return True
           


    def mirrorVtx(self, filterIndxList):
        """
        Mirror each focus mesh to the other side.
        Args: filterIndexList : List of vertex that will be use as filter for user selection to prevent
                                user from selecting vertex from both sides. 

        """

        for mesh in self.focusVtx:
            #get all vtx current position
            currTrans = self.getCurrOsTrans(mesh)

            #filter out vtx
            filterIndxs = list(set(self.focusIndx[mesh]).intersection(filterIndxList))
            filterVtxs = self.getVtxFromIndx(mesh, filterIndxs)

            for v, i in zip(filterVtxs, filterIndxs):

                #if it's in the middle of the mesh
                if self.midDict[i] == True:
                    mel.eval('move -ls %f %f %f %s' %(self.baseMeshData[i][0], currTrans[i][1], currTrans[i][2], v) )

                #it's on the left or right side
                else:
                    c = self.pairDict[i]

                    # if both verts are already sym, skip to increse speed of moving stady point
                    if self.checkSymPoint(currTrans[i], currTrans[c]) == True:
                        continue

                    cVtx = '%s.vtx[%i]' %(mesh, c)
                    mel.eval('move -ls %f %f %f %s' %(currTrans[i][0]*-1, currTrans[i][1], currTrans[i][2], cVtx))



    def revertVtxToBase(self):
        """
        Revert self.focusVtx(multiple objects) to base mesh.

        """

        for mesh in self.focusVtx:
            #get all vtx current position
            currTrans = self.getCurrOsTrans(mesh)

            for v, i in zip(self.focusVtx[mesh], self.focusIndx[mesh]):
                if self.checkEqualPoint(currTrans[i], self.baseMeshData[i]) == True:
                    continue
                mel.eval('move -ls %f %f %f %s;' %(self.baseMeshData[i][0], self.baseMeshData[i][1], self.baseMeshData[i][2], v))



    def resetFocusData(self):
        """
        Reset all focus variables for multiple operations(mirror, flip).

        """

        self.focusVtx = collections.defaultdict(list) 
        self.focusIndx = collections.defaultdict(list)
        # self.focusVtxList = []
        # self.focusIndxList = []



    def resetMeshAFocusData(self):
        """
        Reset all focus variables for meshA operations(subtract, copy, add).

        """

        self.movedAVtxs = []
        self.movedAIndxs = []
        self.meshAFocusVtxs = []
        self.meshAFocusIndxs = []
        self.meshA = None



    def revertVtxByPercent(self, percent):
        """
        Revert self.meshA(one object) to base mesh by the percent given.

        """

        #make percent range from 0 - 1
        percent = percent * 0.01
    
        for v, i in zip(self.movedAVtxs, self.movedAIndxs):
            relTrans = self.getRelativeTrans(self.meshAData[i], self.baseMeshData[i])
            mel.eval('move -ls %f %f %f %s;' %(self.baseMeshData[i][0]+relTrans[0]*percent, self.baseMeshData[i][1]+relTrans[1]*percent, self.baseMeshData[i][2]+relTrans[2]*percent, v))



    def subtractFromMeshA(self):
        """
        Subtract self.meshA modifications from each items in self.focusVtx.

        """
        mel.eval('undoInfo -ock;')
        for mesh in self.focusVtx.keys():
            #get all vtx current position
            currTrans = self.getCurrOsTrans(mesh)

            for v, i in zip(self.focusVtx[mesh], self.focusIndx[mesh]):
                if self.checkEqualPoint(self.meshAData[i], self.baseMeshData[i]) == True:
                    continue

                relTrans = self.getRelativeTrans(self.meshAData[i], self.baseMeshData[i])
                mel.eval('move -ls %f %f %f %s;' %(currTrans[i][0]-relTrans[0], currTrans[i][1]-relTrans[1], currTrans[i][2]-relTrans[2], v))
        mel.eval('undoInfo -cck;')


    def setStatusTxt(self, status):
        if status == True:
            # self.statusTxt.setBackgroundColor([0, 0.4, 0])
            # self.statusTxt.setLabel('Loaded.')
            mc.text(self.statusTxt, e=True, bgc=([0, 0.4, 0]), l='Loaded.')
        else:
            # self.statusTxt.setBackgroundColor([0.4, 0, 0])
            # self.statusTxt.setLabel('No Base Mesh Data.')
            mc.text(self.statusTxt, e=True, bgc=([0.4, 0, 0]), l='No Base Mesh Data.')



















