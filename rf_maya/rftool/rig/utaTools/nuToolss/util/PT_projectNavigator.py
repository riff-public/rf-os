import pymel.core as pm
import re, os, socket, subprocess
from pprint import pprint
from nuTools import misc
from nuTools import config

reload(config)
reload(misc)


class PT_ProjectNavigator(object):
	def __init__(self):
		self.WINDOW_NAME = 'PT_ProjectNavigatorWin'
		self.WINDOW_TITLE = 'PT_ProjectNavigator v1'
		self.projAssetDirDict = config.PROJ_ASSET_ROOT_DIR
		self.PROJECT_LOADED = False
		self.deptPath = {'hero':'maya', 'work':'maya/work'}
		self.modelType = 'anim'
		self.dept = 'rig'
		self.currentMode = 'work'
		self.currentModeIndx = 2
		self.prevTabIndx = None
 		self.defaultAssetPathText = '< Directory does not exists! >'
		self.dirExists = False

		#vars
		self.focusAsset = ''
		self.focusFile = ''
		self.focusAssetPath = ''
		self.focusDeptPath = ''
		self.focusWorkPath = ''
		self.focusHeroPath = ''
		self.focusFilePath = ''
		self.ref = []
		self.assetDict = {}
		self.prevDict = {}

		#get ip, and auto-fill user
		ipAddress = socket.gethostbyname(socket.gethostname())
		try :
			self.user = config.USER_IP[str(ipAddress)]
		except:
			self.user = 'User'

	def UI(self):
		if pm.window(self.WINDOW_NAME, ex=True):
			pm.deleteUI(self.WINDOW_NAME, window=True)
		with pm.window(self.WINDOW_NAME, title=self.WINDOW_TITLE, s=False, mnb=True, mxb=False) as self.mainWindow:
			with pm.columnLayout(adj=True, rs=5, co=['both', 5]):
				# with pm.rowColumnLayout(nc=2, rs=(1, 5), co=[(1, 'left', 3), (2, 'left', 5)]):
				# 	pm.text(l='Quick Open:')
				# 	self.quickOpenTxtFld = pm.textField(ed=True, w=750, tx='')
				with pm.frameLayout(lv=False, mh=5, mw=5) as self.headerFrame:
					with pm.rowColumnLayout(nc=2, rs=(1, 5), co=[(1, 'left', 0), (2, 'left', 0)]):
						with pm.columnLayout(adj=True, rs=10, co=['both', 0]):
							with pm.rowColumnLayout(nc=4, rs=(1, 5), co=[(1, 'left', 5), (2, 'left', 5), (3, 'left', 10), (4, 'left', 3)]):
								pm.text(l='Project: ')
								with pm.optionMenu(cc=pm.Callback(self.caller, 'setProject')) as self.projMenu:
									pm.menuItem(l='N/A')
									for k in sorted(self.projAssetDirDict.keys()):
										pm.menuItem(l=k)
								pm.text('User: ')
								self.userTxtFld = pm.textField(w=85)
						with pm.columnLayout(adj=True, rs=4, co=['both', 0]):
							with pm.rowColumnLayout(nc=4, rs=(1, 5), co=[(1, 'left', 40), (2, 'left', 5), (3, 'left', 5), (4, 'left', 5)]):
								pm.text(l='Path:')
								self.assetPathTextFld = pm.textField(ed=True, w=377, bgc=[0.3, 0, 0], tx=self.defaultAssetPathText, ec=pm.Callback(self.caller, 'quickOpenFile'))
								self.copyButt = pm.button(l='copy', c=pm.Callback(self.copyPath))
								self.openExplorerButt = pm.button(l='explore', c=pm.Callback(self.openExplorer))
							with pm.rowColumnLayout(nc=2, rs=(1, 5), co=[(1, 'left', 27), (2, 'left', 5)]):
								pm.text(l='Recent:')
								with pm.optionMenu(cc=pm.Callback(self.caller, 'openPrevFile'), w=377) as self.prevMenu:
									pm.menuItem(l='')

				with pm.tabLayout() as self.modeTabLayout:
					with pm.frameLayout(lv=False, mh=5, mw=5) as self.assetModeFrame:
						with pm.rowColumnLayout(nc=2, rs=(1, 5), co=[(1, 'left', 0), (2, 'left', 0)]):		
		
							with pm.rowColumnLayout(nc=2, rs=(1, 5), co=[(1, 'left', 65), (2, 'left', 5)]):
								pm.text(l='Filter: ')
								with pm.rowColumnLayout(nc=2, rs=((1, 0), (2, 0), (3, 0), (4, 0), (5, 0)), co=[(1, 'left', 5), (2, 'left', 15), (3, 'left', 5), (4, 'left', 5), 
									(5, 'left', 5), (6, 'left', 5), (7, 'left', 5), (8, 'left', 5), (9, 'left', 5)]):		
									self.fCharacterChkBox = pm.checkBox(l='character', v=True, cc=pm.Callback(self.caller, 'search'))
									self.fPropChkBox = pm.checkBox(l='prop', v=True, cc=pm.Callback(self.caller, 'search'))
									self.fSetChkBox = pm.checkBox(l='set', v=True, cc=pm.Callback(self.caller, 'search'))
									self.fSetDressChkBox = pm.checkBox(l='setDress', v=True, cc=pm.Callback(self.caller, 'search'))
									self.fVehicleChkBox = pm.checkBox(l='vehicle', v=True, cc=pm.Callback(self.caller, 'search'))
									self.fClothingChkBox = pm.checkBox(l='clothing', v=True, cc=pm.Callback(self.caller, 'search'))
									self.fEnvironmentChkBox = pm.checkBox(l='environment', v=True, cc=pm.Callback(self.caller, 'search'))
									self.fWeaponChkBox = pm.checkBox(l='weapon', v=False, cc=pm.Callback(self.caller, 'search'))
									self.fMiscChkBox = pm.checkBox(l='misc', v=False, cc=pm.Callback(self.caller, 'search'))
									with pm.rowColumnLayout(nc=2, rs=(1, 0), co=[(1, 'left', 0), (2, 'left', 3)]):
										self.fEtcChkBox = pm.checkBox(l='', v=False, cc=pm.Callback(self.caller, 'search'))
										self.fEtcTxtFld = pm.textField(w=73, cc=pm.Callback(self.caller, 'search'))


							with pm.rowColumnLayout(nc=6, rs=([1, 0]), co=[(1, 'left', 20), (2, 'left', 5), (3, 'left', 8), 
															(4, 'left', 8), (5, 'left', 8), (6, 'left', 8)]):
								self.deptRadioCol = pm.radioCollection()
								self.modelRadioButt = pm.radioButton(l='model', sl=False, onc=pm.Callback(self.caller, 'refreshAssetList'))
								self.rigRadioButt = pm.radioButton(l='rig', sl=True, onc=pm.Callback(self.caller, 'refreshAssetList'))
								self.uvRadioButt = pm.radioButton(l='uv', sl=False, onc=pm.Callback(self.caller, 'refreshAssetList'))
								self.shadeRadioButt = pm.radioButton(l='shade', sl=False, onc=pm.Callback(self.caller, 'refreshAssetList'))
								self.devRadioButt = pm.radioButton(l='dev', sl=False, onc=pm.Callback(self.caller, 'refreshAssetList'))
								self.refRadioButt = pm.radioButton(l='ref', sl=False, onc=pm.Callback(self.caller, 'refreshAssetList'))
								with pm.columnLayout(adj=True, rs=2, co=['left', 10]):
									self.modelTypeRadioCol = pm.radioCollection()
									self.animModelRadioButt = pm.radioButton(l='anim', sl=True, en=False, onc=pm.Callback(self.caller, 'setModelType'))
									self.proxyModelRadioButt = pm.radioButton(l='proxy', sl=False, en=False, onc=pm.Callback(self.caller, 'setModelType'))
									self.renderModelRadioButt = pm.radioButton(l='render', sl=False, en=False, onc=pm.Callback(self.caller, 'setModelType'))
									self.bshModelRadioButt = pm.radioButton(l='bsh', sl=False, en=False, onc=pm.Callback(self.caller, 'setModelType'))

							with pm.columnLayout(adj=True, rs=5, co=['left', 8]):
								with pm.rowColumnLayout(nc=2, rs=(1, 5), co=[(1, 'left', 15), (2, 'left', 5)]):
									pm.text(l='Search: ')
									self.searchTxtFld = pm.textField(w=226, cc=pm.Callback(self.caller, 'search'), ec=pm.Callback(self.caller, 'search'))
								self.assetTSL = pm.textScrollList(h=300, w=312, ams=False, sc=pm.Callback(self.caller, 'refreshAssetList'))
					
							
							with pm.rowColumnLayout(nc=2, co=[(1, 'left', 10), (2, 'left', 10)]):
								with pm.columnLayout(adj=True, co=['both', 0]):
									with pm.tabLayout(cc=pm.Callback(self.caller, 'selectWorkHero')) as self.workHeroTabLayout:
										self.heroTSL = pm.textScrollList(h=300, w=312, ams=False, sc=pm.Callback(self.caller, 'selectFile'))
										self.workTSL = pm.textScrollList(h=300, w=312, ams=False, sc=pm.Callback(self.caller, 'selectFile'))
										pm.tabLayout( self.workHeroTabLayout, edit=True, tabLabel=((self.heroTSL, '   Hero   '), (self.workTSL, '   Work   ')), selectTabIndex=1)
								with pm.columnLayout(adj=True, rs=5, co=['both', 3]):
									pm.text(l='', h=15)
									self.openButt = pm.button(l='Open', w=140, h=50, c=pm.Callback(self.caller, 'openFile'))
									self.savePPButt = pm.button(l='Save ++', w=140, h=50, c=pm.Callback(self.caller, 'incSave'))
									with pm.rowColumnLayout(nc=2, co=[(1, 'left', 0), (2, 'left', 5)]):
										self.referenceButt = pm.button(l='Create\nRef', w=80, h=35, c=pm.Callback(self.createRef))
										self.removeRefButt = pm.button(l='Remove\nRef', w=80, h=35, c=pm.Callback(self.removeRef))
									self.importRefButt = pm.button(l='Import Ref', h=40, c=pm.Callback(self.importRef))
									with pm.columnLayout(adj=True, co=['left', 30]):
										self.removeNameSpaceChkBox = pm.checkBox(l='Remove Namespace', v=True)
									pm.separator()	
									pm.text(l='', h=46)
									self.updateButt = pm.button(l='Update to Current', w=140, h=25, c=pm.Callback(self.updateUI))


					with pm.frameLayout(lv=False, mh=5, mw=5) as self.filmModeFrame:
						pass


					pm.tabLayout( self.modeTabLayout, edit=True, tabLabel=((self.assetModeFrame, '   Asset   '), (self.filmModeFrame, '   Film   ')), selectTabIndex=1)

		self.updateUI()
		self.userTxtFld.setText(self.user)
				


	def addToPrevOpen(self):
		rootDir = self.projAssetDirDict[self.currentProject]
		label = self.focusFilePath.split(rootDir)[1]
		if len(label) > 65:
			label = label[-65:]
		if self.focusFilePath not in self.prevDict.values():
			self.prevDict[label] = self.focusFilePath
			pm.menuItem(l=label, p=self.prevMenu, ia='')



	def caller(self, op):
		if op == 'setProject':
			self.setProject()
			if self.PROJECT_LOADED == False:
				return
			self.updateAssetTSL('setProject')
			self.updateWorkTSL()
			self.resetAssetPathTxtFld()
			self.workTSL.removeAll()
			self.resetVar()
		elif op == 'search':
			selectedAsset = self.getTSLFocus('assetTSL')
			self.setProject()
			self.updateAssetTSL('search')
			self.resetAssetPathTxtFld()
			self.resetVar()
			self.getHeroWork()
			self.updateWorkTSL()
			if selectedAsset in self.assetTSL.getAllItems():
				self.assetTSL.setSelectItem(selectedAsset)
				self.caller('refreshAssetList')
		elif op == 'refreshAssetList':
			self.selectDept()
			self.getHeroWork()
			self.updateWorkTSL()
			self.updateAssetPathTxtFld('selectAssetTSL')
			self.lockSaveButt()
		elif op == 'refreshToCurrent':
			self.getCurrentDept()
			self.selectDeptRadioButt()
			self.caller('refreshAssetList')
		elif op == 'setModelType':
			self.setModelType()
			self.getHeroWork()
			self.updateWorkTSL()
			self.updateAssetPathTxtFld('selectDept')		
		elif op == 'selectWorkHero':
			self.getHeroWork()
			self.updateWorkTSL()
			self.updateAssetPathTxtFld('selectWorkHero')
			self.lockSaveButt()
		elif op == 'selectFile':
			self.updateAssetPathTxtFld('selectFile')
		elif op == 'incSave':
			self.incSave()
			self.workHeroTabLayout.setSelectTabIndex(2)
			self.getHeroWork()
			self.updateWorkTSL()
			self.setWorkTSL()
			self.updateAssetPathTxtFld('selectFile')
		elif op == 'openFile':
			self.openFile()
			self.addToPrevOpen()
		elif op == 'openPrevFile':
			currentPrev = self.prevMenu.getValue()
			if not currentPrev in self.prevDict.keys():
				return
			self.focusFilePath = self.prevDict[currentPrev]
			self.openFile()
			self.prevMenu.setValue('')
			self.updateUI()
		elif op == 'quickOpenFile':
			if self.quickOpen() == True:
				self.assetPathTextFld.setText(self.focusFilePath)
				self.enableAssetPathTxtFld()
				self.openFile()
				self.updateUIFromPath()
			else:
				self.resetAssetPathTxtFld()



	# def hilightPath(self):
	# 	print 'hilight'


	def createRef(self):
		if self.focusFilePath == pm.sceneName() or not self.focusFilePath:
			return
		self.ref.append(pm.createReference(self.focusFilePath, defaultNamespace=True))



	def copyPath(self):
		if self.dirExists == False:
			return
		path = self.assetPathTextFld.getText()
		ctrlPress = misc.checkMod('ctrl')
		if ctrlPress == True:
			toCopyPath = '/'.join(path.split('/')[0:-1])
		else:
			toCopyPath = path
		misc.addToClipBoard(toCopyPath)



	def cleanPath(self, path):
		#remove unwanted characters
		path = path.replace('"', '')
		path = path.replace('\\', '/')
		return path


	def convertOsPath(self, path):
		#remove unwanted characters
		path = path.replace('"', '')
		path = path.replace('/', '\\')
		return path



	def enableAssetPathTxtFld(self):
		self.assetPathTextFld.setBackgroundColor([0,0.4,0])
		self.dirExists = True



	def getCurrentProject(self):
		currentPath = pm.sceneName()
		try:
			ks = {}
			for k, v in self.projAssetDirDict.iteritems():
				if v in currentPath:
					ks[len(v)] = k
			return ks[max(ks.keys())]
		except:
			return



	def getProject(self):
		currentPath = self.focusFilePath

		try:
			ks = {}
			for k, v in self.projAssetDirDict.iteritems():
				if v in currentPath:
					ks[len(v)] = k
			return ks[max(ks.keys())]
		except:
			return



	def getCurrentAsset(self):
		if not self.assetDict or self.PROJECT_LOADED == False:
			return
		currentPath = pm.sceneName()
		for asset in self.assetDict.keys():
			try:
				mname = re.search(r"((\w+))", asset)
				assetStrip = mname.group()
				subType = asset.replace(assetStrip, '').strip()[1:-1]
				if subType: 
					subType = '/%s' %subType
				toSearch = '%s/%s/' %(subType, assetStrip)
			except: pass
		
			if toSearch in currentPath:
				self.focusAsset = asset



	def getAsset(self):
		if not self.assetDict or self.PROJECT_LOADED == False:
			return
		currentPath = self.focusFilePath
		for asset in self.assetDict.keys():
			try:
				mname = re.search(r"((\w+))", asset)
				assetStrip = mname.group()
				subType = asset.replace(assetStrip, '').strip()[1:-1]
				if subType: 
					subType = '/%s' %subType
				toSearch = '%s/%s/' %(subType, assetStrip)
			except: pass
		
			if toSearch in currentPath:
				self.focusAsset = asset		



	def getCurrentDept(self):
		currentPath = pm.sceneName()
		depts = ['rig' ,'model', 'uv', 'shade', 'dev' ,'ref'] 
		modelTypes = ['anim', 'proxy', 'render', 'bsh']
		try:
			self.dept = [d for d in depts if '/%s/' %d in currentPath][0]
			currentDeptPath = ''
			if self.dept == 'model':
				currentModelType = [t for t in modelTypes if '/%s/' %t in currentPath][0]
				self.modelType = currentModelType
				currentDeptPath = '%s/%s' %(currentDeptPath, currentModelType)
			if '/work/' in currentPath:
				self.workHeroTabLayout.setSelectTabIndex(2)
			else:
				self.workHeroTabLayout.setSelectTabIndex(1)
	
		except:
			pass



	def genVersion(self, incSavePath):
		version = '001'
		try:
			ver = 0
			files = [ f for f in os.listdir(incSavePath) if os.path.isfile(os.path.join(incSavePath,f)) ]
			num = []
			for fName in files:
				if '_v' in fName:
					fName = fName.split('_v')[-1]
				v = ''
				for i in fName:
					if i.isdigit():
						v += str(i)
				if v:
					num.append(int(v))

			ver = max(num) + 1
			version = str(ver)
		except:
			pass

		return version.zfill(3)



	def getAllAsset(self, assetPathRoot, hasSubType=True):
		existingTypeFolder = os.walk(assetPathRoot).next()[1]
		typePaths = []
		try:
			for assetType in self.assetTypeFiltered:
				if assetType in existingTypeFolder:
					assetTypePath = '%s/%s' %(assetPathRoot, assetType)
					if os.path.exists(assetTypePath):
						typePaths.append(assetTypePath)

			stPaths, assets = [], {}
			for typ in typePaths:
				if hasSubType == False:
					assetFolders = os.walk(typ).next()[1]
					for a in assetFolders:
						assetPath = '%s/%s' %(typ, a)
						if os.path.exists(assetPath):
							assets[a] = assetPath

				else:
					subTypes = os.walk(typ).next()[1]
					for st in subTypes:
						stPath = '%s/%s' %(typ, st)
						if os.path.exists(stPath):
							assetFolders = os.walk(stPath).next()[1]
							for a in assetFolders:
								assetPath = '%s/%s' %(stPath, a)
								if os.path.exists(assetPath):
									if a in assets.keys():
										value = assets[a]
										oldSt = value.split('/')[-2]
										del(assets[a])

										oldKey = '%s (%s)' %(a, oldSt)
										assets[oldKey] = value

										key = '%s (%s)' %(a, st)
										assets[key] = assetPath 
									else:
										assets[a] = assetPath
			self.assetDict  = assets
		except:
			self.assetDict = {}



	def getTSLFocus(self, tsl):
		if tsl == 'assetTSL':
			TSL = self.assetTSL
		elif tsl == 'workHeroTSL':
			if self.currentMode == 'hero':
				TSL = self.heroTSL
			else:
				TSL = self.workTSL
		try:
			return TSL.getSelectItem()[0]
		except:
			pass



	def getHeroWork(self):
		selTab = self.workHeroTabLayout.getSelectTabIndex()

		if selTab == 1:
			self.currentMode = 'hero'
			self.currentModeIndx = 1
		else:
			self.currentMode = 'work'
			self.currentModeIndx = 2




	def importRef(self):
		removeNs = self.removeNameSpaceChkBox.getValue()
		try:
			for ref in self.ref:
				ref.importContents(removeNamespace=removeNs)
			self.ref = []
		except:
			pass



	def incSave(self):
		if self.dept in ['dev', 'ref']:
			return
		# incSavePath = '%s/%s/%s' %(self.focusAssetPath, self.dept, self.deptPath['work'])
		incSavePath = '%s/%s' %(self.focusDeptPath, self.deptPath['work'])

		if not os.path.exists(incSavePath):
			return

		self.user = self.userTxtFld.getText()
		if self.dept == 'model':
			deptPart = '%s_%s' %(self.dept, self.modelType)
		else: deptPart = self.dept

		mname = re.search(r"((\w+))", self.focusAsset)
		if mname:
			focusAssetRep = mname.group()
		else:
			focusAssetRep = self.focusAsset

		fileName = '%s_%s_v%s_%s.ma' %(focusAssetRep, deptPart, self.genVersion(incSavePath), self.user)
		pathToSave = '%s/%s' %(incSavePath, fileName)

		if os.path.exists(pathToSave):
			return
		else:
			pm.saveAs(pathToSave)
			self.focusFile = fileName
			print ('\nFile Saved to: %s' %pathToSave),



	def lockSaveButt(self):
		if self.dept in ['ref', 'dev'] and self.currentMode != 'work':
			self.savePPButt.setEnable(False)
		else:
			self.savePPButt.setEnable(True)



	def setModelType(self):
		if self.animModelRadioButt.getSelect() == True:
			self.modelType = 'anim'
		elif self.proxyModelRadioButt.getSelect() == True:
			self.modelType = 'proxy'
		elif self.renderModelRadioButt.getSelect() == True:
			self.modelType = 'render'
		elif self.bshModelRadioButt.getSelect() == True:
			self.modelType = 'bsh'
		self.focusDeptPath = '%s/%s/%s' %(self.assetDict[self.getTSLFocus('assetTSL')], 'model', self.modelType)



	def setModelTypeRadioButt(self):
		val = self.modelRadioButt.getSelect()		
		self.animModelRadioButt.setEnable(val)
		self.proxyModelRadioButt.setEnable(val)
		self.renderModelRadioButt.setEnable(val)
		self.bshModelRadioButt.setEnable(val)



	def setWorkTSL(self):
		try:
			if self.focusFile:
				self.workTSL.setSelectItem(self.focusFile)
		except: pass



	def selectDept(self):
		self.focusAsset = self.getTSLFocus('assetTSL')
		if self.PROJECT_LOADED == False or not self.focusAsset:
			return
		self.setModelTypeRadioButt()
		if self.modelRadioButt.getSelect() == True:
			self.dept = 'model'
			self.deptPath = {'hero':'', 'work':'work'}
		elif self.rigRadioButt.getSelect() == True:
			self.dept = 'rig'
			self.deptPath = {'hero':'maya', 'work':'maya/work'}
		elif self.uvRadioButt.getSelect() == True:
			self.dept = 'uv'
			self.deptPath = {'hero':'maya', 'work':'maya/work'}
		elif self.shadeRadioButt.getSelect() == True:
			self.dept = 'shade'
			self.deptPath = {'hero':'maya', 'work':'maya/work'}
		elif self.devRadioButt.getSelect() == True:
			self.dept = 'dev'
			self.deptPath = {'hero':'maya', 'work':'maya/wip'}		
		elif self.refRadioButt.getSelect() == True:
			self.dept = 'ref'
			self.deptPath = {'hero':'', 'work':''}

		if self.dept == 'model':
			self.focusDeptPath = '%s/%s/%s' %(self.assetDict[self.focusAsset], self.dept, self.modelType)
		else:
			self.focusDeptPath = '%s/%s' %(self.assetDict[self.focusAsset], self.dept)



	def selectDeptRadioButt(self):
		if self.dept == 'model':
			self.modelRadioButt.setSelect(True)
			if self.modelType == 'anim':
				self.animModelRadioButt.setSelect(True)
			elif self.modelType == 'proxy':
				self.proxyModelRadioButt.setSelect(True)
			elif self.modelType == 'render':
				self.renderModelRadioButt.setSelect(True)
			elif self.modelType == 'bsh':
				self.bshModelRadioButt.setSelect(True)
		elif self.dept == 'rig':
			self.rigRadioButt.setSelect(True) 
		elif self.dept == 'uv':
			self.uvRadioButt.setSelect(True) 
		elif self.dept == 'shade':
			self.shadeRadioButt.setSelect(True) 
		elif self.dept == 'dev':
			self.devRadioButt.setSelect(True) 		
		elif self.dept == 'ref':
			self.refRadioButt.setSelect(True) 



	def openFile(self):
		if not os.path.exists(self.focusFilePath):
			return

		if not os.path.isfile(self.focusFilePath):
			return

		result = ''
		if pm.cmds.file(q=True, anyModified=True) == True:
			result = pm.confirmDialog( title='Confirm Open File', 
				message='Scene has been modified',
				button=['Open', 'Cancel'], defaultButton='Open', cancelButton='Cancel', dismissString='Cancel' )
		
		if result == 'Cancel':
			return
		else:
			pm.openFile(self.focusFilePath, f=True)
			


	def openExplorer(self):
		currentPath = self.assetPathTextFld.getText()
		currentPath = self.convertOsPath(currentPath)

		if os.path.exists(currentPath) == False:
			return

		if os.path.isfile(currentPath) == True:
			toOpen = os.path.dirname(currentPath)
		else:
			toOpen = currentPath 

		try:
			subprocess.Popen(r'explorer "%s"' %toOpen)
		except:
			return




	def quickOpen(self):
		pastedPath = self.assetPathTextFld.getText()

		#remove unwanted characters
		pastedPath = self.cleanPath(pastedPath)


		self.focusFilePath = pastedPath

		if not os.path.exists(self.focusFilePath):
			return False

		else:
			return True



	def removeRef(self):
		if self.ref:
			for r in self.ref:
				r.remove()
			self.ref = []



	def resetVar(self):
		self.focusAsset = ''
		self.focusFile = ''
		self.focusAssetPath = ''
		self.focusDeptPath = ''
		self.focusWorkPath = ''
		self.focusHeroPath = ''
		self.focusFilePath = ''



	def resetAssetPathTxtFld(self):
		self.assetPathTextFld.setBackgroundColor([0.3,0,0])
		self.dirExists = False



	def setProject(self):
		self.PROJECT_LOADED = False
		self.currentProject = self.projMenu.getValue()
		if self.currentProject == 'N/A':
			self.workTSL.removeAll()
			self.heroTSL.removeAll()
			self.assetTSL.removeAll()
			# self.assetPathTextFld.setText('')
			return

		filterChkBox = [self.fCharacterChkBox, self.fPropChkBox, self.fSetChkBox, self.fSetDressChkBox, 
						self.fVehicleChkBox, self.fWeaponChkBox, self.fEnvironmentChkBox, self.fClothingChkBox,
						self.fMiscChkBox, self.fEtcChkBox]
		self.assetTypeFiltered = []
		self.assetDict = {}
		assetPathRoot = None
		for chkBox in filterChkBox:
			if chkBox.getValue() == True:
				if chkBox == self.fEtcChkBox:
					self.assetTypeFiltered.append(self.fEtcTxtFld.getText())
				else:
					self.assetTypeFiltered.append(chkBox.getLabel())
		if self.currentProject in self.projAssetDirDict.keys():
			assetPathRoot = self.projAssetDirDict[self.currentProject]

		aSubType = config.PROJ_ASSET_SUBTYPE[self.currentProject]

		if assetPathRoot:
			self.getAllAsset(assetPathRoot, aSubType)
			self.PROJECT_LOADED = True



	def updateAssetTSL(self, op):
		sortedAssets = sorted(self.assetDict.iterkeys(), key=lambda s: s.lower())
		if op == 'search':
			searchFor = self.searchTxtFld.getText()
			found = []
			for a in sortedAssets:
				if searchFor in a:
					found.append(a)
			self.assetTSL.removeAll()
			found = sorted(found, key=lambda s: s.lower())
			for f in found:
				self.assetTSL.append(f)

		elif op == 'setProject':
			self.assetTSL.removeAll()
			for a in sortedAssets:
				self.assetTSL.append(a)



	def updateUI(self):
		self.currentProject = self.getCurrentProject()
		if self.currentProject:
			self.projMenu.setValue(self.currentProject)
		else:
			return

		self.setProject()
		if self.PROJECT_LOADED == False:
			return

		self.updateAssetTSL('setProject')
		self.getCurrentAsset()

		if self.focusAsset:
			self.assetTSL.setSelectItem(self.focusAsset)
			self.caller('refreshToCurrent')
	


	def updateUIFromPath(self):
		self.currentProject = self.getProject()
		if self.currentProject in self.projAssetDirDict.keys():
			self.projMenu.setValue(self.currentProject)

			self.setProject()
			self.updateAssetTSL('setProject')
			self.getAsset()

			if self.focusAsset:
				self.assetTSL.setSelectItem(self.focusAsset)
				self.caller('refreshToCurrent')

		else:
			self.PROJECT_LOADED == False
			self.projMenu.setValue('N/A')
			self.workTSL.removeAll()
			self.heroTSL.removeAll()
			self.assetTSL.removeAll()
			self.resetVar()
			return





	def updateWorkTSL(self):
		if self.dept not in ['rig', 'model', 'uv', 'shade', 'dev', 'ref']:
			return
		try:
			if self.deptPath['hero']:
				self.focusHeroPath = '%s/%s' %(self.focusDeptPath, self.deptPath['hero'])
			else:
				self.focusHeroPath = self.focusDeptPath
			if self.deptPath['work']:
				self.focusWorkPath = '%s/%s' %(self.focusDeptPath, self.deptPath['work'])
			else:
				self.focusWorkPath = ''

			self.heroTSL.removeAll()
			self.workTSL.removeAll()

			heroFiles = [ f for f in os.listdir(self.focusHeroPath) if os.path.isfile(os.path.join(self.focusHeroPath,f)) ]
			if heroFiles:
				for h in heroFiles:
					self.heroTSL.append(h)

			workFiles = [ f for f in os.listdir(self.focusWorkPath) if os.path.isfile(os.path.join(self.focusWorkPath,f)) ]
			if workFiles:
				for w in workFiles:
					self.workTSL.append(w)

		except:
			pass



	def updateAssetPathTxtFld(self, op):
		if self.PROJECT_LOADED == False:
			return
		toset = ''
		self.focusFilePath = ''

		if op == 'selectAssetTSL':	
			selAsset = self.getTSLFocus('assetTSL')
			self.focusAssetPath = self.assetDict[selAsset]
			if self.currentMode == 'hero':
		 		toset = self.focusHeroPath
		 	else:
		 		toset = self.focusWorkPath
		elif op == 'selectDept':
			toset = self.focusDeptPath
		elif op == 'selectWorkHero':
			if self.currentMode == 'hero':
		 		toset = self.focusHeroPath
		 	else:
		 		toset = self.focusWorkPath
		elif op == 'selectFile':
			self.fileSel = self.getTSLFocus('workHeroTSL')
			if self.currentMode == 'hero':
				focusPath = self.focusHeroPath
			else:
				focusPath = self.focusWorkPath
			self.focusFilePath = '%s/%s' %(focusPath, self.fileSel)
			toset = self.focusFilePath

		if os.path.exists(toset):
			self.assetPathTextFld.setText(toset)
			self.assetPathTextFld.setBackgroundColor([0,0.4,0])
			self.dirExists = True
		else:
			self.resetAssetPathTxtFld()
			self.workTSL.removeAll()