import itertools

import pymel.core as pm
import maya.cmds as mc
import maya.mel as mel
import maya.OpenMaya as om

from nuTools import misc
reload(misc)

def doBoolean(srcObj, cutCube):
	boolObj = srcObj.duplicate()[0]
	pm.delete(boolObj, ch=True)

	boolObjShp = boolObj.getShape(ni=True)
	resultObjs = pm.polyCBoolOp(boolObj, cutCube, 
					op=3, ch=1, 
					preserveColor=0, 
					classification=1)
	if resultObjs:
		pm.delete(resultObjs[1])
		for i, d in boolObjShp.inputs(c=True):
			i.disconnect()
		pm.delete(boolObj)
		return resultObjs[0]

def getProxyGeoData(proxyGeo):
	proxyGeoName = proxyGeo.longName()

	# create get MfnMesh for proxyGeo and cutCube
	mSel = om.MSelectionList()
	mSel.add(proxyGeoName)

	proxyMDagPath = om.MDagPath()
	mSel.getDagPath(0, proxyMDagPath) 

	proxyMDagPath.extendToShape()
	proxyMfnMesh = om.MFnMesh(proxyMDagPath)

	# iterate thru each face of pxy, get face center
	faceIt = om.MItMeshPolygon(proxyMDagPath)
	numFace = proxyMfnMesh.numPolygons()
	faceCenters = om.MPointArray()
	faceCenterFs = om.MFloatPointArray()

	while not faceIt.isDone():
		centerMPt = faceIt.center(om.MSpace.kWorld)
		centerMFPt = om.MFloatPoint(centerMPt[0], centerMPt[1], centerMPt[2])

		faceCenters.append(centerMPt)
		faceCenterFs.append(centerMFPt)

		# go next
		faceIt.next()

	return proxyMDagPath, proxyMfnMesh, numFace, faceCenters, faceCenterFs

def getCutCubeData(cutCube):
	cutCubeName = cutCube.longName()

	mSel = om.MSelectionList()
	mSel.add(cutCubeName)

	cubeMDagPath = om.MDagPath()
	mSel.getDagPath(0, cubeMDagPath)
	cubeMDagPath.extendToShape()

	# get inclusive matrix of the cube full path
	cubeIncMatrix = cubeMDagPath.inclusiveMatrix()

	cubeMfnDagNode = om.MFnDagNode(cubeMDagPath)
	cubeMBB = cubeMfnDagNode.boundingBox()  # get bounding box in object space
	cubeMBB.transformUsing(cubeIncMatrix)  # transform the bounding box to world space

	
	cubeMfnMesh = om.MFnMesh(cubeMDagPath)

	# iterate thru each face of the cube, get face normal and invert it
	faceIt = om.MItMeshPolygon(cubeMDagPath)
	numFace = cubeMfnMesh.numPolygons()
	inverseNormals = []

	while not faceIt.isDone():
		normalMVector = om.MVector()
		faceIt.getNormal(normalMVector, om.MSpace.kWorld)
		# normalMVector *= -1

		# convert MVector to MFloat vector
		normalMFVector = om.MFloatVector(normalMVector)

		inverseNormals.append(normalMFVector)

		# go next
		faceIt.next()

	return cubeMDagPath, cubeMfnMesh, numFace, inverseNormals, cubeMBB


def rayIntersect(fnMesh, raySource, rayDir, accelParams=None):
	hitPoints = om.MFloatPointArray()
	hitRayParams = om.MFloatArray()
	hitFaces = om.MIntArray()
	hit = fnMesh.allIntersections(raySource,  # raySource 
			rayDir,  	# rayDirection 
			None,  		# faceIds 
			None,  		# triIds 
			True,  		# idsSorted 
			om.MSpace.kWorld,  # space
			100000, 	# maxParam 
			False,  	# testBothDirections 
			accelParams,  # accelParams
			False,  # sortHits
			hitPoints,  # hitPoints
			hitRayParams,  # hitRayParams 
			hitFaces,  # hitFaces
			None,  # hitTriangles
			None,  # hitBary1s
			None,  # hitBary2s
			0.0001)  # tolerance

	# result = int(fmod(len(hitFaces), 2))

	return hit

def faceRanges(geoName, inputList):
	result = []
	for a, b in itertools.groupby(enumerate(inputList), lambda (x, y): y - x):
		b = list(b)
		if b[0][1] == b[-1][1]:
			num = str(b[0][1])
		else:
			num = '%s:%s' %(b[0][1], b[-1][1])
		result.append('%s.f[%s]' %(geoName, num))

	return result

def createProxyRig(srcGeos, cutCubes,
				triangulate=False, 
				searchSuffix='_ply', 
				replaceSuffix='_jnt'):

	cutCubes = [cube for cube in cutCubes if misc.checkIfPly(cube)==True]
	if not cutCubes:
		om.MGlobal.displayError('No cut object found!')
		return

	# make sure all srcGeos are transform with mesh as child
	srcGeos = [geo for geo in srcGeos if misc.checkIfPly(geo)==True]
	if not srcGeos:
		om.MGlobal.displayError('No geometry found!')
		return

	# duplicate and delete history
	dups = pm.duplicate(srcGeos)
	pm.delete(dups, ch=True)

	# combine evertying
	cmbGeo = pm.polyUnite(dups, ch=False, mergeUVSets=True, name='tmpCombineGeo_ply')[0]
	
	try:
		pm.delete(dups)  # delete the left over transform
	except:
		pass

	if triangulate == True:
		pm.polyTriangulate(cmbGeo, ch=False)

	# get cmbGeo data
	proxyMDagPath, proxyMfnMesh, numFace, faceCenters, faceCenterFs = getProxyGeoData(cmbGeo)

	# --- loop over all cut objects
	proxyGeos = []
	timeUsed = 0.0
	for cutCube in cutCubes:
		cutCubeName = cutCube.nodeName()

		parent = None
		if cutCubeName.endswith(searchSuffix):
			jntName = cutCubeName.replace(searchSuffix, replaceSuffix)
			jntName = jntName.split(':')[-1]
			for s in ['*:%s' %jntName, jntName]:
				jnts = pm.ls(s, type='joint')
				if jnts:
					parent = jnts[0]
					break
			else:
				om.MGlobal.displayWarning('Skipping %s: could not find match.' %cutCubeName)
				continue

		# get cut cube data
		cubeMDagPath, cubeMfnMesh, numFace, inverseNormals, cubeMBB = getCutCubeData(cutCube)
		# cubeAccelParams = cubeMfnMesh.autoUniformGridParams()
		cubeAccelParams = None

		# duplicate the combinded geo
		proxyGeo = cmbGeo.duplicate()[0]
		proxyGeoShp = proxyGeo.getShape(ni=True)
		proxyGeoName = proxyGeoShp.longName()

		# get only points that's inside the cube boundingBox
		indxInBB, ptFInBB = [], []
		nonIsctFaceIndxs = []

		# local var for more speed in tight loop
		cubeMBB_contains = cubeMBB.contains
		indxInBB_append = indxInBB.append
		ptFInBB_append = ptFInBB.append
		nonIsctFaceIndxs_append = nonIsctFaceIndxs.append

		for fi in xrange(faceCenters.length()):
			if cubeMBB_contains(faceCenters[fi]) == True:
				indxInBB_append(fi)
				ptFInBB_append(faceCenterFs[fi])
			else:
				nonIsctFaceIndxs_append(fi)

		# do intersect, get face index that's inside the cube
		for fi, pt in zip(indxInBB, ptFInBB):
			for invNormal in inverseNormals:
				if not rayIntersect(cubeMfnMesh, pt, invNormal, cubeAccelParams):
					nonIsctFaceIndxs.append(fi)
					break

		# rearrange flat face num list into ranges for speed
		nonIsctFaces = faceRanges(proxyGeoName, sorted(nonIsctFaceIndxs))
		# delete them
		mc.delete(nonIsctFaces)

		# parent it to joint
		pm.parent(proxyGeo, parent)

		proxyGeo.rename(cutCubeName)
		proxyGeos.append(proxyGeo)

	pm.delete(cmbGeo)

def snapProxyCube(jnts=[], mult=1.0, parentToJnt=False):
	if not jnts:
		jnts = misc.getSel(num='inf', selType='joint')
		if not jnts:
			return

	cubes = []
	for j in jnts:
		dim = j.radius.get() * mult
		cube = pm.polyCube(ch=False, w=dim, h=dim, d=dim)[0]
		misc.snapTransform('parent', j, cube, False, True)

		nms = misc.nameSplit(j.nodeName())
		newName = '%s%s_ply' %(nms['elem'], nms['pos'])
		cube.rename(newName)

		if parentToJnt == True:
			pm.parent(cube, j)

		cubes.append(cube)

	pm.select(cubes)