import pymel.core as pm
import maya.OpenMaya as om
import re, os
from pprint import pprint
from nuTools import misc, config


reload(misc)
reload(config)

global gGeoReplacerRefs
# gGeoReplacerRefs = []

class GeoReplacer(object):
	"""
	Geometry Replacer Class helps you match geometries with the same name/position to identify them in as a pair.
	After each object is identified, you can choose a method for replacing old geometry with the new one.

	Instance Command :
		from nuTools import util as ut
		reload(ut)
		geoReplacer = ut.GeoReplacer()
		geoReplacer.UI()

	Methods are:
		1. copyUv : Will copy the UV from the new object to the old one. If the old object has deformer(s) applied,
					this method will not leave any history on the mesh.
		2. replace : Replace the old object with the new one by deleting the old one and parent the new one to the 
					same heirachy.
		3. copySkinWeight : Apply smooth skin and copy skin weight from the old one to the new one.
	"""		
	def __init__(self):
		self.WINDOW_NAME = 'geoReplacerMainWin'
		self.WINDOW_TITLE = 'Geometry Replacer v3.0'
		self.matchDict = {}
		self.partMatchDict = {}
		self.noMatch = []
		self.sels = []
		self.unSels = []

	def UI(self):
		if pm.window(self.WINDOW_NAME, ex=True):
			pm.deleteUI(self.WINDOW_NAME, window=True)
		with pm.window(self.WINDOW_NAME, title=self.WINDOW_TITLE, s=False, mnb=True, mxb=False) as self.mainWindow:
			with pm.frameLayout(lv=False, bs='etchedIn', mh=5, mw=5):
				with pm.frameLayout(l='Source File', bs='etchedIn', mh=5, mw=5, fn='smallObliqueLabelFont'):      
					with pm.columnLayout(adj=True, rs=5, co=['both', 5]):
						with pm.rowColumnLayout(nc=2, co=[(1, 'left', 55), (2, 'left', 20)]):
							pm.button(l='Create Ref', c=pm.Callback(self.createRef), w=75, h=30, bgc=(0,0.4,0.2))
							pm.button(l='Remove Ref', c=pm.Callback(self.removeRef), w=75, h=30, bgc=(0.4,0.0,0.0))


				with pm.frameLayout(l='Matching', bs='etchedIn', mh=5, mw=5, fn='smallObliqueLabelFont'):     		
					with pm.columnLayout(adj=True, rs=0, co=['both', 5]):
						self.feedBackTxt = pm.text(l='Select original objects and press "Match Selected".', h=20, bgc=[0.3,0,0])
						self.feedBackNoMatchTxt = pm.text(l='N/A', h=20, bgc=[0.3,0,0])
					with pm.columnLayout(adj=True, rs=5, co=['both', 5]):
						with pm.rowColumnLayout(nc=2, co=[(1, 'left', 5), (2, 'left', 5)]):
							prefixTxt = pm.text(l='Namespace')
							self.prefixTxtFld = pm.textField(w=150)

					with pm.columnLayout(adj=True, rs=5, co=['both', 65]):
						pm.button(l='Match by Name', w=100, h=25, bgc=(0,0.3,0.3), c=pm.Callback(self.matchObjCall, 'name'))
						pm.button(l='Match by Center and Volume', w=100, h=25, bgc=(0.4,0.0,0.0), c=pm.Callback(self.matchObjCall, 'centerAndVolume'))
						#pm.button(l='Match by Volume', w=100, h=25, bgc=(0.4,0.0,0.0), c=pm.Callback(self.matchObjCall, 'volume'))
						with pm.rowColumnLayout(nc=2, co=[(1, 'left', 0), (2, 'left', 5)]):
							pm.button(l='Select\nMatched', w=75, h=35, c=pm.Callback(self.selMatched))
							pm.button(l='Select\nNon Matched', w=75, h=35, c=pm.Callback(self.selNoMatched))

				with pm.frameLayout(l='Replaceing', bs='etchedIn', mh=5, mw=5, fn='smallObliqueLabelFont'):     		
					with pm.rowColumnLayout(nc=2, rs=(1, 5), co=[(1, 'left', 52), (2, 'left', 5)]):
						pm.text(l='Replace Methods')
						with pm.optionMenu() as self.metOptionMenu:
								pm.menuItem(l='copyUv')
								pm.menuItem(l='setUv')
								pm.menuItem(l='replace')
								pm.menuItem(l='replace(relative)')
								pm.menuItem(l='copySkinWeight')
								pm.menuItem(l='do nothing')
						pm.text(l='Shader Options')
						with pm.optionMenu() as self.shdOptionMenu:
								pm.menuItem(l='use original')
								pm.menuItem(l='use imported')
								pm.menuItem(l='do nothing')

					with pm.columnLayout(adj=True, rs=5, co=['both', 50]):
						pm.button(l='Replace Selected', w=100, h=30, bgc=(0,0.3,0.3), c=pm.Callback(self.replaceObj, 'selected'))
						pm.button(l='Replace All', w=100, h=40, bgc=(0,0.4,0.2), c=pm.Callback(self.replaceObj, 'all'))


	def createRef(self):
		currPath = pm.sceneName()
		try:
			assetPath = '/'.join(currPath.split('/')[0:7])
			refDirPath = '%s/publish' %(assetPath)
		except:
			om.MGlobal.displayWarning('Failed splitting path.')
			refDirPath = os.path.dirname(currPath)
		
		if not os.path.exists(refDirPath):
			om.MGlobal.displayError('Path do not exists: %s' %refDirPath)
			

		refFilePath = pm.fileDialog2(ff="Maya Files (*.ma *.mb)", ds=2, fm=1, 
				dir=refDirPath, cap='Create Reference', okc='Reference')
		ref = pm.createReference(refFilePath)

		global gGeoReplacerRefs
		ns = ref.namespace
		gGeoReplacerRefs.append(ref) 

		#reset prefix text field
		self.prefixTxtFld.setText(ns)
	

	def matchObjCall(self, method):
		prefix = self.prefixTxtFld.getText()
		
		#reset the status txt
		self.feedBackTxt.setBackgroundColor([0.3,0,0])
		self.feedBackNoMatchTxt.setBackgroundColor([0.3,0,0])
		self.feedBackTxt.setLabel('Select original objects and press "Match Selected".')
		self.feedBackNoMatchTxt.setLabel('N/A')

		self.getSelMesh()

		self.matchDict, self.noMatch = self.matchObj(prefix=prefix, method=method)

		matchNum = len(self.matchDict)
		noMatchNum = len(self.noMatch)
		self.feedBackTxt.setLabel('%s  object(s) matched.' %matchNum)
		self.feedBackNoMatchTxt.setLabel('%s object(s) unable to find match.' %noMatchNum)

		if matchNum > 0:
			self.feedBackTxt.setBackgroundColor([0,0.5,0])
		if noMatchNum == 0:
			self.feedBackNoMatchTxt.setBackgroundColor([0,0.5,0])

	def selMatched(self):
		sels = misc.getSel(num='inf')
		if not sels:
			return
		orig, new = [], []
		for s in sels:
			if s in self.matchDict.keys():
				orig.append(s)
				new.append(self.matchDict[s])
		if orig and new:
			pm.select([orig, new], r=True)

	def selNoMatched(self):
		if self.noMatch == []:
			return
		pm.select(self.noMatch, r=True)

	def replaceObj(self, scope):
		if not self.matchDict:
			return

		method = self.metOptionMenu.getValue()
		
		match = {}
		self.partMatchDict = {}
		if scope == 'selected':
			sels = misc.getSel(num='inf')
			for s in sels:
				if s in self.matchDict.keys():
					newObj = self.matchDict[s]
					self.partMatchDict[s] = newObj
			match = self.partMatchDict

		elif scope == 'all':
			match = self.matchDict

		self.batchOpOnPairObjs(objDict=match, op=method)

	def removeRef(self):
		global gGeoReplacerRefs
		refs = gGeoReplacerRefs
		for ref in refs:
			ref.remove()

	def importRef(self):
		if not self.ref:
			return
		self.ref.importContents(removeNamespace=True)

	def getSelMesh(self):
		self.sels = []
		sels = misc.getSel(num='inf')
		if not sels:
			pm.error('No selection! Select polygon(s) to find another with matching name.')

		filteredSel = []
		for s in sels:
			tran = None
			tran = self.checkIfPolygons(s)
			if tran:
				self.sels.append(tran)

	def checkIfPolygons(self, obj):
		shp = obj.getShape()
		if shp:
			meshShp = filter(lambda x: isinstance(x, (pm.nt.Mesh)), [shp])
			if meshShp:
				trans = meshShp[0].getParent()
				return trans

	def getUnselected(self):
		if not self.sels:
			return
		self.unSels = []
		allTrans = pm.ls(type='transform')
		for i in allTrans:
			ply = None
			ply = self.checkIfPolygons(i)
			if ply not in self.sels and ply:
				self.unSels.append(ply)


	def matchObj(self, prefix='', method='name'):
		if not self.sels:
			return

		retDict, noMatch = {}, []
		objNum = len(self.sels)

		#Progress bar
		pBarWin = pm.window('Progress', mnb=False, mxb=False, s=False)
		pm.columnLayout(adj=True, co=['both', 5])
		pm.text(l='Matching Geometries by %s...' %method)
		pBar = pm.progressBar(w=200, beginProgress=True, isInterruptable=True, maxValue=objNum, imp=False)
		pm.showWindow(pBarWin)

		for sel in self.sels:
			#if user hit cancel
			if pm.progressBar(pBar, q=True, isCancelled=True ) :
				break
			newObj = ''
			matchFound = False
			if method == 'name':
				splitNs = sel.nodeName().split(':')[-1]

				sameNames = pm.ls('%s%s' %(prefix, splitNs), r=True)

				for s in sameNames:
					if s == sel or s.isVisible() == False:
						sameNames.remove(s)	
				#we got more than one matching name. 
				if len(sameNames) > 1:
					#Will compare each object center and get the closest one.
					newObj, matchFound = self.compareCenter(sel, sameNames)
				else:
					if sameNames != []:
						newObj = sameNames[0]
						matchFound = True

			elif method == 'centerAndVolume':
				self.getUnselected()
				newObj, matchFound = self.compareCenterAndVolume(sel, self.unSels)
				try:
					if matchFound == True:
						self.unSels.remove(newObj)
				except: pass
				
			if matchFound == True and newObj not in retDict.values():
				newObj = pm.PyNode(newObj)
				retDict[sel] = newObj
			else:
				pm.warning('Cannot find match for  %s' %sel),
				noMatch.append(sel)

			#increment progress bar
			pm.progressBar(pBar, e=True, step=1)

		pm.progressBar(pBar, e=True, endProgress=True)
		pm.deleteUI(pBarWin)

		pprint(retDict)
		return retDict, noMatch



	def compareCenter(self, oldObj, newObjs):
		matchFound = False
		closeObjs = []
		closestObject = ''
		oldCenter = pm.objectCenter(oldObj, gl=True)
		for s in newObjs:
			center = pm.objectCenter(s, gl=True)
			if center == oldCenter:
				closeObjs.append(s)
		
		closeObjNum = len(closeObjs)
		if closeObjNum == 1:
			matchFound = True
			closestObject = closeObjs[0]

		return closestObject, matchFound



	def compareCenterAndVolume(self, oldObj, newObjs):
		matchFound = False
		closeObjs, dists = [], []
		closestObject = ''
		oldCenter = pm.objectCenter(oldObj, gl=True)
		for s in newObjs:
			center = pm.objectCenter(s, gl=True)
			dist = misc.getDistanceFromPosition(oldCenter, center)
			if not dists and not closeObjs:
				dists.append(dist)
				closeObjs.append(s)
				continue
			if dist < min(dists):
				dists = [dist]
				closeObjs = [s]
			elif dist == min(dists):
				dists.append(dist)
				closeObjs.append(s)
			
		if len(dists) == 1:
			closestObject = closeObjs[0]
			matchFound = True
		else:
			closestObject, matchFound = self.compareVolume(oldObj, closeObjs)

		return closestObject, matchFound

	def compareVolume(self, oldObj, newObjs):
		matchFound = False
		vols = []
		closestObject = ''

		currOldObjName = oldObj.nodeName()
		try:
			oldObj.rename('_TEMP%s' %currOldObjName)
		except: pass
		pm.select(oldObj, r=True)
		oldVol = pm.mel.eval('computePolysetVolume;')
		try:
			oldObj.rename(currOldObjName)
		except: pass
		
		for s in newObjs:
			pm.select(s, r=True)
			newVol = pm.mel.eval('computePolysetVolume;')
			vols.append(newVol)
		
		diff = [abs(i-oldVol) for i in vols]
		closest = min(diff)

		if diff.count(closest) == 1:
			idx = diff.index(closest)
			closestObject = newObjs[idx]
			matchFound = True

		return closestObject, matchFound

	def batchOpOnPairObjs(self, objDict, op):
		if not objDict:
			pm.error('/nThis function need dictionary of {oldObject:newObject} as input.'),
		result = []
		#Progress bar
		pBarWin = pm.window('Progress', mnb=False, mxb=False, s=False)
		pm.columnLayout(adj=True, co=['both', 5])
		pm.text(l='%s...' %op)
		objNum = len(objDict.keys())
		pBar = pm.progressBar(w=200, beginProgress=True, isInterruptable=True, maxValue=objNum, imp=False)
		pm.showWindow(pBarWin)

		shadeMethod = self.shdOptionMenu.getValue()

		for k in sorted(objDict.keys(), key=lambda k:len(objDict[k].split('|')), reverse=True):
			v = objDict[k]
			#if user hit cancel
			if pm.progressBar(pBar, q=True, isCancelled=True ) :
				break

			if op == 'do nothing':
				self.doApplyShade(old=k, new=v, shadeMethod=shadeMethod)
				result.append('nothing\t%s\t\t%s' %(v, k))

			elif op == 'copyUv':
				if not k.isReferenced():
					ret = misc.copyUv(parent=v, child=[k], rename=False, printRes=False)
					self.doApplyShade(old=k, new=v, shadeMethod=shadeMethod)
					result.append({ret['uv']:'%s\t\t%s' %(v, k)})
				else:
					pm.warning('%s is a referenced object.' %k.nodeName())

			elif op == 'setUv':
				ret = misc.setUv(source=v, destination=k)
				self.doApplyShade(old=k, new=v, shadeMethod=shadeMethod)
				result.append('%s : %s\t\t%s' %(ret, v, k))

			elif op == 'replace' or op == 'replace(relative)':
				if v.isReferenced() == True:
					om.MGlobal.displayWarning('Duplicated referenced object : %s' %v)
					v = v.duplicate()[0]
					# continue
				if op == 'replace':
					oldParent = k.getParent()

					self.doApplyShade(old=k, new=v, shadeMethod=shadeMethod)
					
					vChildren = [c for c in v.getChildren() if isinstance(c, pm.nt.Transform) == True]
					if vChildren:
						pm.delete(vChildren)

					oldName = k.nodeName()
					kchildren = k.getChildren()
					pm.parent(kchildren, v)

					pm.delete(k)
					pm.parent(v, oldParent)
					v.rename(oldName)
					result.append(v)
				else:
					oldParent = k.getParent()

					self.doApplyShade(old=k, new=v, shadeMethod=shadeMethod)

					vChildren = [c for c in v.getChildren() if isinstance(c, pm.nt.Transform) == True]
					if vChildren:
						pm.delete(vChildren)

					kchildren = k.getChildren()
					pm.parent(kchildren, v, r=True)

					pm.delete(k)
					# print 'parenting  %s  :  %s' %(v, oldParent)
					pm.parent(v, oldParent, r=True)
					result.append(v)
			elif op == 'copySkinWeight':
				try:
					scs = misc.copySkinWeight(parent=k, child=v)
				except:
					scs = None
					pass

				self.doApplyShade(old=k, new=v, shadeMethod=shadeMethod)
				result = scs

			#increment progress bar
			pm.progressBar(pBar, e=True, step=1)

		pm.progressBar(pBar, e=True, endProgress=True)
		pm.deleteUI(pBarWin)
		pprint(result)


	def doApplyShade(self, old, new, shadeMethod):
		if shadeMethod == 'use imported':
			misc.transferShadeAssign(parent=new, child=old)
		elif shadeMethod == 'use original':
			misc.transferShadeAssign(parent=old, child=new)


	def parentToGroup(self):
		sels = misc.getSel(num=2)
		newGrp = sels[0]
		oldGrp = sels[1]

		oldChilds = pm.listRelatives(oldGrp, children=True, type='transform')
		newChilds = pm.listRelatives(newGrp, children=True, type='transform')

		pm.delete(oldChilds)
		pm.parent(newChilds, oldGrp, r=True)
