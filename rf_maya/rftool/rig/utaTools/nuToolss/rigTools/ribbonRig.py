import maya.cmds as mc
import maya.OpenMaya as om
import pymel.core as pm

from nuTools import misc, controller
reload(misc)
reload(controller)
import nuTools.rigTools.baseRig as baseRig
reload(baseRig)

RADIUS_MULT = 0.1

class RibbonConnector(baseRig.BaseRig):
	def __init__(self, 
				jnts=None,
				ctrlShp='crossCircle',
				ctrlColor='yellow', 
				parent={},
				**kwargs):
		super(RibbonConnector, self).__init__(**kwargs)
		# temp joints
		self.tmpJnts = self.jntsArgs(jnts)

		# setting var
		self.ctrlShp = ctrlShp
		self.ctrlColor = ctrlColor

		self.ctrl = None
		self.ctrlZgrp = None
		self.parent = parent

	def rig(self):
		# --- create the controller
		self.ctrl = controller.Controller(name='%sRbn%s_ctrl' %(self.elem, self.side), 
					st=self.ctrlShp, scale=(self.size))
		self.ctrl.setColor(self.ctrlColor)
		self.ctrl.rotateOrder.set(self.rotateOrder)

		toLockHide = {'t':False, 'r':True, 's':True, 'v':True}
		misc.lockAttr(self.ctrl, **toLockHide)
		misc.hideAttr(self.ctrl, **toLockHide)

		self.ctrlZgrp = misc.zgrp(self.ctrl, element='CtrlZro', suffix='grp')[0]

		# snap zgrp
		misc.snapTransform('parent', self.tmpJnts, self.ctrlZgrp, False, True)

		# constraint to parent transform
		if self.parent:
			for cons, parent in self.parent.iteritems():
				misc.snapTransform(cons, parent, self.ctrlZgrp, True, False)

		# --- parent to main groups
		pm.parent(self.ctrlZgrp, self.animGrp)

class RibbonRig(baseRig.BaseRig):
	def __init__(self, 
				aimAxis='+x',
				upAxis='+z',
				**kwargs):
		super(RibbonRig, self).__init__(**kwargs)

		# handle basic ribbon attributes
		self.radius = self.size * RADIUS_MULT

		self.aimAxis = aimAxis
		self.aimVec = misc.vectorStr(self.aimAxis)

		self.upAxis = upAxis
		self.upVec = misc.vectorStr(self.upAxis)

		self.otherAxis = misc.crossAxis(self.aimAxis, self.upAxis)
		self.otherVec = misc.vectorStr(self.otherAxis)

	def createSurf(self, crvs):
		self.surf = pm.loft(crvs, d=1, ch=False)[0]
		self.surf.rename('%sRbn%s_nrbs' %(self.elem, self.side))
		pm.delete(crvs)
		
class RibbonIkRig(RibbonRig):
	def __init__(self, 
				numJnt=5,
				aimAxis='+x',
				upAxis='+z',
				ctrlShp='crossCircle',
				ctrlColor='yellow',
				dtlCtrlShp='circle',
				dtlCtrlColor='lightBlue',
				baseTipCrvShape=None,
				baseTipCtrlColor='yellow',
				baseTipJnt=False,
				**kwargs):
		if numJnt < 1 or not isinstance(numJnt, int):
			om.MGlobal.displayError('Invalid numJnt, using 5.')
			numJnt = 5
		super(RibbonIkRig, self).__init__(aimAxis=aimAxis,
										upAxis=upAxis,
										**kwargs)
		
		self.numJnt = numJnt
		self.ctrlShp = ctrlShp
		self.ctrlColor = ctrlColor
		self.dtlCtrlShp = dtlCtrlShp
		self.dtlCtrlColor = dtlCtrlColor
		self.baseTipCrvShape = baseTipCrvShape
		self.baseTipCtrlColor = baseTipCtrlColor

		self.baseTipJnt = baseTipJnt

		self.baseCtrl = None
		self.midCtrl = None
		self.tipCtrl = None

		# ctrls
		self.dtlCtrls = []
		self.dtlCtrlZgrps = []

		# jnts
		self.baseJnt = None
		self.midJnt = None
		self.tipJnt = None
		self.rbnJnts = []

	def rig(self):
		# --- figure out the axis
		self.rotateOrder = '%s%s%s' %(self.aimAxis[-1], self.otherAxis[-1], self.upAxis[-1]) 
		self.twstRoAttrName =  'r%s' %self.aimAxis[-1]
		self.sqshScAttrNames = ('s%s' %self.upAxis[-1], 's%s' %self.otherAxis[-1])

		# --- main grps
		_name = (self.elem, self.side)
		self.rigCtrlGrp = pm.group(em=True, n='%sRbnRig%s_grp' %_name)
		self.rigUtilGrp = pm.group(em=True, n='%sRbnUtil%s_grp' %_name)
		self.rigStillGrp = pm.group(em=True, n='%sRbnStill%s_grp' %_name)
		self.rigStillGrp.visibility.set(False)

		# parent main grps
		pm.parent(self.rigCtrlGrp, self.animGrp)
		pm.parent(self.rigUtilGrp, self.utilGrp)
		pm.parent(self.rigStillGrp, self.stillGrp)

		# tip position
		tipPos = self.aimVec * self.size

		# --- create nurbs
		aCrv = pm.curve(d=1, p=[(0, 0, 0), 
			(tipPos.x, tipPos.y, tipPos.z)], 
			k=[0, 1])
		bCrv = aCrv.duplicate()[0]
		pm.xform(aCrv, ws=True, t=self.otherVec*0.05)
		pm.xform(bCrv, ws=True, t=self.otherVec*-0.05)

		self.createSurf(crvs=[bCrv, aCrv])
		pm.rebuildSurface(self.surf, ch=0, rpo=1, rt=0, end=1, kr=0, kcp=0, 
			kc=0, su=1, du=1, sv=5, dv=3, tol=0.0001, fr=2, dir=2)
		pm.parent(self.surf, self.rigStillGrp)

		# --- rbn jnt
		if not self.baseTipCrvShape:
			baseTipCrvShape = 'locator'
		# base jnt
		self.baseJnt = pm.createNode('joint', n='%sRbnBase%s_jnt' %_name)
		self.baseJnt.radius.set(self.radius)
		self.baseJnt.visibility.set(False)
		self.baseCtrl = controller.JointController(name='%sRbnBase%s_ctrl' %_name,
												st=baseTipCrvShape, 
												color=self.baseTipCtrlColor, 
												scale=(self.size*0.334),
												draw=False)
		self.baseCtrl.lockAttr(r=True, s=True, v=True)
		self.baseCtrl.hideAttr(r=True, s=True, v=True)
		self.baseCtrl.rotateOrder.set(self.rotateOrder)

		self.baseOfstGrp = misc.zgrp(self.baseCtrl, element='Ofst', suffix='grp')[0]
		self.baseZroGrp = misc.zgrp(self.baseOfstGrp, element='Zro', remove='Ofst', suffix='grp')[0]
		pm.parent(self.baseZroGrp, self.rigCtrlGrp)

		self.baseAimGrp = pm.group(em=True, n='%sRbnBaseAim%s_grp' %_name)
		self.baseAimZroGrp = misc.zgrp(self.baseAimGrp, element='Zro', suffix='grp')[0]
		pm.parent(self.baseAimZroGrp, self.baseCtrl)

		misc.snapTransform('parent', self.baseAimGrp, self.baseJnt, False, False)
		
		# tipJnt
		self.tipJnt = pm.createNode('joint', n='%sRbnTip%s_jnt' %_name)
		self.tipJnt.radius.set(self.radius)
		self.tipJnt.visibility.set(False)
		self.tipCtrl = controller.JointController(name='%sRbnTip%s_ctrl' %_name,
												st=baseTipCrvShape, 
												color=self.baseTipCtrlColor, 
												scale=(self.size*0.334),
												draw=False)
		self.tipCtrl.lockAttr(r=True, s=True, v=True)
		self.tipCtrl.hideAttr(r=True, s=True, v=True)
		self.tipCtrl.rotateOrder.set(self.rotateOrder)

		self.tipOfstGrp = misc.zgrp(self.tipCtrl, element='Ofst', suffix='grp')[0]
		self.tipZroGrp = misc.zgrp(self.tipOfstGrp, element='Zro', remove='Ofst', suffix='grp')[0]
		pm.parent(self.tipZroGrp, self.rigCtrlGrp)

		self.tipAimGrp = pm.group(em=True, n='%sRbnTipAim%s_grp' %_name)
		self.tipAimZroGrp = pm.group(self.tipAimGrp, n='%sRbnTipAimZro%s_grp' %_name)
		pm.parent(self.tipAimZroGrp, self.tipCtrl)

		# position tip ctrl zgrp
		pm.xform(self.tipZroGrp, ws=True, t=tipPos)

		misc.snapTransform('parent', self.tipAimGrp, self.tipJnt, False, False)

		# if baseTipCrvShape is not passed, hide the shape
		if not self.baseTipCrvShape:
			for ctrl in [self.baseCtrl, self.tipCtrl]:
				ctrl.getShape().intermediateObject.set(True)

		# mid jnt
		self.midJnt = pm.createNode('joint', n='%sRbnMid%s_jnt' %_name)
		self.midJnt.radius.set(self.radius)
		self.midJnt.visibility.set(False)
		self.midCtrl = controller.JointController(name='%sRbnMid%s_ctrl' %_name,
												st=self.ctrlShp, 
												color=self.ctrlColor, 
												axis=self.aimAxis,
												scale=self.size,
												draw=False)
		self.midCtrl.lockAttr(s=True, v=True)
		self.midCtrl.hideAttr(s=True, v=True)
		self.midCtrl.rotateOrder.set(self.rotateOrder)

		twstDivAttr = misc.addNumAttr(self.midCtrl, '__twist__', 'double', hide=False, min=0, max=1, dv=0)
		baseTwstAttr = misc.addNumAttr(self.midCtrl, 'baseTwist', 'double')
		tipTwstAttr = misc.addNumAttr(self.midCtrl, 'tipTwist', 'double')
		autoTwstAttr = misc.addNumAttr(self.midCtrl, 'autoTwist', 'double', min=0.0, max=1.0)
		twstDivAttr.lock()

		sqstDivAttr = misc.addNumAttr(self.midCtrl, '__squash__', 'double', hide=False, min=0, max=1, dv=0)
		sqstDivAttr.lock()

		sqstAttr = misc.addNumAttr(self.midCtrl, 'squash', 'double')
		autoSqshAttr = misc.addNumAttr(self.midCtrl, 'autoSquash', 'double', min=0.0, max=1.0)

		midCtrlShp = self.midCtrl.getShape(ni=True)
		dtlCtrlVisAttr = misc.addNumAttr(midCtrlShp, 'detailCtrl_vis', 'long', min=0, max=1, dv=0)
		dtlCtrlVisAttr.setKeyable(False)
		dtlCtrlVisAttr.showInChannelBox(True)
		
		defLenAttr = misc.addNumAttr(midCtrlShp, 'defaultLen', 'double', v=self.size)
		defLenAttr.setKeyable(False)
		defLenAttr.showInChannelBox(False)
		# defLenAttr.setLocked(True)

		baseTwstMultAttr = misc.addNumAttr(midCtrlShp, 'baseTwist_mult', 'double', dv=1.0)
		baseTwstMultAttr.setKeyable(False)
		baseTwstMultAttr.showInChannelBox(False)
		tipTwstMultAttr = misc.addNumAttr(midCtrlShp, 'tipTwist_mult', 'double', dv=1.0)
		tipTwstMultAttr.setKeyable(False)
		tipTwstMultAttr.showInChannelBox(False)

		pm.addAttr(midCtrlShp, ln='aimVector', sn='av', at='double3')
		pm.addAttr(midCtrlShp, ln='aimVectorX', sn='avx', at='double', p='aimVector')
		pm.addAttr(midCtrlShp, ln='aimVectorY', sn='avy', at='double', p='aimVector')
		pm.addAttr(midCtrlShp, ln='aimVectorZ', sn='avz', at='double', p='aimVector')
		midCtrlShp.aimVector.set(self.aimVec)
		midCtrlShp.aimVector.setKeyable(False)
		midCtrlShp.aimVector.showInChannelBox(False)

		pm.addAttr(midCtrlShp, ln='upVector', sn='uv', at='double3')
		pm.addAttr(midCtrlShp, ln='upVectorX', sn='uvx', at='double', p='upVector')
		pm.addAttr(midCtrlShp, ln='upVectorY', sn='uvy', at='double', p='upVector')
		pm.addAttr(midCtrlShp, ln='upVectorZ', sn='uvz', at='double', p='upVector')
		midCtrlShp.upVector.set(self.upVec)
		midCtrlShp.upVector.setKeyable(False)
		midCtrlShp.upVector.showInChannelBox(False)
		
		self.midOfstGrp = misc.zgrp(self.midCtrl, element='Ofst', suffix='grp')[0]
		self.midAimGrp = misc.zgrp(self.midOfstGrp, element='Aim', remove='Ofst', suffix='grp')[0]
		self.midZroGrp = misc.zgrp(self.midAimGrp, element='Zro', remove='Aim', suffix='grp')[0]
		pm.parent(self.midZroGrp, self.rigCtrlGrp)

		self.midJntOfstGrp = pm.group(em=True, n='%sRbnMidJntOffset%s_grp' %_name)
		self.midJntZroGrp = misc.zgrp(self.midJntOfstGrp, element='Zro', remove='Offst', suffix='grp')[0]
		pm.parent(self.midJntZroGrp, self.midCtrl)

		misc.snapTransform('parent', self.midJntOfstGrp, self.midJnt, False, False)

		# parent 3 ribbon jnts to group
		pm.parent([self.baseJnt, self.midJnt, self.tipJnt], self.rigUtilGrp)

		# setup aim
		self.baseAimCons = pm.aimConstraint(self.tipCtrl, self.baseAimZroGrp,
										aim=self.aimVec, u=self.upVec,
										wut='objectrotation',
										wuo=self.baseCtrl,
										wu=self.upVec)
		self.tipAimCons = pm.aimConstraint(self.baseCtrl, self.tipAimZroGrp,
										aim=(self.aimVec*-1), u=self.upVec,
										wut='objectrotation',
										wuo=self.tipCtrl,
										wu=self.upVec)
		pm.pointConstraint([self.baseCtrl, self.tipCtrl], self.midZroGrp)
		self.midAimCons = pm.aimConstraint(self.tipCtrl, self.midAimGrp,
										aim=self.aimVec, u=self.upVec,
										wut='objectrotation',
										wuo=self.midZroGrp,
										wu=self.upVec)

		# connect upVec attr on midCtrl shape to aim cons vector
		pm.connectAttr(midCtrlShp.upVector, self.baseAimCons.upVector)
		pm.connectAttr(midCtrlShp.upVector, self.baseAimCons.worldUpVector)
		pm.connectAttr(midCtrlShp.upVector, self.tipAimCons.upVector)
		pm.connectAttr(midCtrlShp.upVector, self.tipAimCons.worldUpVector)
		pm.connectAttr(midCtrlShp.upVector, self.midAimCons.upVector)
		pm.connectAttr(midCtrlShp.upVector, self.midAimCons.worldUpVector)

		# point on surface group
		self.allPosGrp = pm.group(em=True, n='%sRbnPos%s_grp' %_name)
		pm.parent(self.allPosGrp, self.rigStillGrp)

		# detail ctrl group
		self.dtlCtrlGrp = pm.group(em=True, n='%sRbnDtlCtrl%s_grp' %_name)
		# detail joint group
		self.dtlJntGrp = pm.group(em=True, n='%sRbnDtlJnt%s_grp' %_name)
		
		pm.parent(self.dtlCtrlGrp, self.rigCtrlGrp)
		pm.parent(self.dtlJntGrp, self.rigUtilGrp)

		misc.snapTransform('parent', self.dtlCtrlGrp, self.dtlJntGrp, False, False)

		pm.connectAttr(midCtrlShp.detailCtrl_vis, self.dtlCtrlGrp.visibility)

		# attach pos groups
		if self.baseTipJnt:
			totalNumJnt = self.numJnt + 2    # include the base and tip rbn jnt
			step = float(1.0/(totalNumJnt-1))
			varyStep = float(6.0/(totalNumJnt-1))
		else:
			totalNumJnt = self.numJnt
			step = float(1.0/(totalNumJnt+1))
			try:
				varyStep = float(6.0/(totalNumJnt-1))
			except ZeroDivisionError:
				varyStep = 0

		posGrps, posNodes, aimNodes = [], [], []
		for i in xrange(totalNumJnt):
			posGrp = pm.group(em=True, n='%sRbnPos%s%s_grp' %(self.elem, (i+1), self.side))
			
			posNode = pm.createNode('pointOnSurfaceInfo', n='%sRbn%s%s_posi' %(self.elem, (i+1), self.side))
			posNode.parameterU.set(0.5)

			if self.baseTipJnt:
				paramV = (i)*step
			else:
				paramV = (i + 1)*step
			posNode.parameterV.set(paramV)
			posNode.turnOnPercentage.set(True)
			
			aimCon = pm.createNode('aimConstraint', n='%sRbn%s%s_aimConstraint' %(self.elem, (i+1), self.side))
			pm.connectAttr(self.surf.worldSpace[0], posNode.inputSurface)
			pm.connectAttr(midCtrlShp.aimVector , aimCon.aimVector)
			pm.connectAttr(midCtrlShp.upVector , aimCon.upVector)
			pm.connectAttr(posNode.normal, aimCon.worldUpVector)
			pm.connectAttr(posNode.tangentV, aimCon.target[0].targetTranslate)
			pm.connectAttr(posNode.position, posGrp.translate)
			pm.connectAttr(aimCon.constraintRotate, posGrp.rotate)

			pm.parent(posGrp, self.allPosGrp)
			pm.parent(aimCon, posGrp)

			posGrps.append(posGrp)

		# --- twist pre connections
		self.twstMultMdv = pm.createNode('multiplyDivide', n='%sRbnTwstMult%s_mdv' %(_name))
		self.autoTwstMdv = pm.createNode('multiplyDivide', n='%sRbnAutoTwst%s_mdv' %(_name))
		twstPma = pm.createNode('plusMinusAverage', n='%sRbnTwst%s_pma' %(_name))

		# pm.connectAttr(self.baseTwstGrp.attr(self.twstRoAttrName), self.twstMultMdv.input1X)
		pm.connectAttr(midCtrlShp.baseTwist_mult, self.twstMultMdv.input2X)
		# pm.connectAttr(self.tipTwstGrp.attr(self.twstRoAttrName), self.twstMultMdv.input1Y)
		pm.connectAttr(midCtrlShp.tipTwist_mult, self.twstMultMdv.input2Y)

		pm.connectAttr(self.twstMultMdv.outputX, self.autoTwstMdv.input1X)
		pm.connectAttr(self.twstMultMdv.outputY, self.autoTwstMdv.input1Y)
		pm.connectAttr(self.midCtrl.autoTwist, self.autoTwstMdv.input2X)
		pm.connectAttr(self.midCtrl.autoTwist, self.autoTwstMdv.input2Y)

		pm.connectAttr(self.autoTwstMdv.outputX, twstPma.input2D[0].input2Dx)
		pm.connectAttr(self.autoTwstMdv.outputY, twstPma.input2D[0].input2Dy)
		pm.connectAttr(self.midCtrl.baseTwist, twstPma.input2D[1].input2Dx)
		pm.connectAttr(self.midCtrl.tipTwist, twstPma.input2D[1].input2Dy)

		# --- squash pre connections
		# 3 distance grps
		basePosGrp = pm.group(em=True, n='%sRbnBasePos%s_grp' %_name)
		midPosGrp = pm.group(em=True, n='%sRbnMidPos%s_grp' %_name)
		tipPosGrp = pm.group(em=True, n='%sRbnTipPos%s_grp' %_name)
		misc.snapTransform('point', self.baseJnt, basePosGrp, False, False)
		misc.snapTransform('point', self.midJnt, midPosGrp, False, False)
		misc.snapTransform('point', self.tipJnt, tipPosGrp, False, False)
		pm.parent([basePosGrp, midPosGrp, tipPosGrp], self.rigCtrlGrp)

		# distanceBetween nodes
		uprDist = pm.createNode('distanceBetween', n='%sRbnUpr%s_dist' %_name)
		pm.connectAttr(basePosGrp.translate, uprDist.point1)
		pm.connectAttr(midPosGrp.translate, uprDist.point2)

		lwrDist = pm.createNode('distanceBetween', n='%sRbnLwr%s_dist' %_name)
		pm.connectAttr(midPosGrp.translate, lwrDist.point1)
		pm.connectAttr(tipPosGrp.translate, lwrDist.point2)

		self.lenAdl = pm.createNode('addDoubleLinear', n='%sRbnLen%s_adl' %_name)
		pm.connectAttr(uprDist.distance, self.lenAdl.input1)
		pm.connectAttr(lwrDist.distance, self.lenAdl.input2)

		sqshNormMdv = pm.createNode('multiplyDivide', n='%sRbnSqshNorm%s_mdv' %_name)
		sqshPowMdv = pm.createNode('multiplyDivide', n='%sRbnSqshPow%s_mdv' %_name)
		sqshDivMdv = pm.createNode('multiplyDivide', n='%sRbnSqshDiv%s_mdv' %_name)
		
		sqshNormMdv.operation.set(2)
		pm.connectAttr(self.lenAdl.output, sqshNormMdv.input1X)

		midCtrlShp.defaultLen.set(self.lenAdl.output.get())
		pm.connectAttr(midCtrlShp.defaultLen, sqshNormMdv.input2X)

		sqshPowMdv.operation.set(3)
		sqshPowMdv.input2X.set(2.0)
		pm.connectAttr(sqshNormMdv.outputX, sqshPowMdv.input1X)

		sqshDivMdv.operation.set(2)
		sqshDivMdv.input1X.set(1.0)
		pm.connectAttr(sqshPowMdv.outputX, sqshDivMdv.input2X)

		# --- create detail controller
		twstPmaBaseTipDict = {0:twstPma.output2Dx, (totalNumJnt-1):twstPma.output2Dy}

		# create tmp animCurve and frameCache to calculate squash mult values
		c = pm.createNode('animCurveTU')
		c.addKey(0, 0.0, tangentInType='linear', tangentOutType='linear')
		c.addKey(3, 1.0, tangentInType='flat', tangentOutType='flat')
		c.addKey(6, 0.0, tangentInType='linear', tangentOutType='linear')
		fc = pm.createNode('frameCache')
		pm.connectAttr(c.output, fc.stream)

		for i in xrange(totalNumJnt):
			# dtl ctrl
			dtlCtrl = controller.JointController(name='%sRbnDtl%s%s_ctrl' %(self.elem, (i+1), self.side),
												st=self.dtlCtrlShp, 
												color=self.dtlCtrlColor, 
												axis=self.aimAxis,
												scale=(self.size*0.75),
												draw=False)
			dtlCtrl.lockAttr(s=True, v=True)
			dtlCtrl.hideAttr(s=True, v=True)
			dtlCtrl.rotateOrder.set(self.rotateOrder)

			dtlSqshAttr = misc.addNumAttr(dtlCtrl, 'squash', 'double', min=0, max=1, dv=0.0)

			ofstGrp = misc.zgrp(dtlCtrl, element='Ofst', suffix='grp')[0]
			twstGrp = misc.zgrp(ofstGrp, element='Twst', remove='Ofst', suffix='grp')[0]
			zroGrp = misc.zgrp(twstGrp, element='Zro', remove='Twst', suffix='grp')[0]
			pm.parent(zroGrp, self.dtlCtrlGrp)

			# dtl jnt
			dtlJnt = pm.createNode('joint', n='%sRbnDtl%s%s_jnt' %(self.elem, (i+1), self.side))
			dtlJnt.radius.set(self.radius*0.75)
			pm.parent(dtlJnt, self.dtlJntGrp)

			# constraint joint to dtl ctrl
			misc.snapTransform('parent', posGrps[i], zroGrp, False, False)
			misc.snapTransform('parent', dtlCtrl, dtlJnt, False, True)
			pm.makeIdentity(dtlJnt, n=False, apply=True)
			misc.snapTransform('parent', dtlCtrl, dtlJnt, False, False)

			# connect twist
			if i in twstPmaBaseTipDict:    # if this is the first or the last jnt
				if totalNumJnt == 1:
					defSqshMultValue = 1.0
					# create mdv if it's the only joint, set mult value to 0.5
					dtlTwstMdv = pm.createNode('multiplyDivide', n='%sRbnDtl%sTwst%s_mdv' %(self.elem, (i+1), self.side))
					dtlTwstMdv.input2X.set(0.5)
					dtlTwstMdv.input2Y.set(0.5)

					pm.connectAttr(twstPma.output2Dx, dtlTwstMdv.input1X)
					pm.connectAttr(twstPma.output2Dy, dtlTwstMdv.input1Y)
					
					dtlTwstAdl = pm.createNode('addDoubleLinear', n='%sRbnDtl%sTwst%s_adl' %(self.elem, (i+1), self.side))
					pm.connectAttr(dtlTwstMdv.outputX, dtlTwstAdl.input1)
					pm.connectAttr(dtlTwstMdv.outputY, dtlTwstAdl.input2)
					pm.connectAttr(dtlTwstAdl.output, twstGrp.attr(self.twstRoAttrName))
				else:
					defSqshMultValue = 0.0
					pm.connectAttr(twstPmaBaseTipDict[i], twstGrp.attr(self.twstRoAttrName))
			else:
				if self.baseTipJnt:
					twstMultValue = step*i
				else:
					twstMultValue = float(1.0/(totalNumJnt-1)) * i
				twstMultInvValue = 1.0 - twstMultValue
				# create mdv if it's not the first or the last
				dtlTwstMdv = pm.createNode('multiplyDivide', n='%sRbnDtl%sTwst%s_mdv' %(self.elem, (i+1), self.side))
				dtlTwstMdv.input2X.set(twstMultInvValue)
				dtlTwstMdv.input2Y.set(twstMultValue)

				pm.connectAttr(twstPma.output2Dx, dtlTwstMdv.input1X)
				pm.connectAttr(twstPma.output2Dy, dtlTwstMdv.input1Y)
				
				dtlTwstAdl = pm.createNode('addDoubleLinear', n='%sRbnDtl%sTwst%s_adl' %(self.elem, (i+1), self.side))
				pm.connectAttr(dtlTwstMdv.outputX, dtlTwstAdl.input1)
				pm.connectAttr(dtlTwstMdv.outputY, dtlTwstAdl.input2)

				# get squash mult from frame cache
				fc.varyTime.set(varyStep * i)
				defSqshMultValue = round(fc.varying.get(), 3)

				pm.connectAttr(dtlTwstAdl.output, twstGrp.attr(self.twstRoAttrName))
				
			# connect squash
			squashAttrName = 'squash_mult%s' %(i+1)
			sqshMultAttr = misc.addNumAttr(midCtrlShp, squashAttrName, 'double', min=0, max=1, dv=defSqshMultValue)
			midCtrlShp.attr(squashAttrName).setKeyable(False)
			midCtrlShp.attr(squashAttrName).showInChannelBox(True)
			autoSqshMdl = pm.createNode('multDoubleLinear', n='%sRbnDtl%sAutoSqshMult%s_mdl' %(self.elem, (i+1), self.side))
			autoSqshSubPma = pm.createNode('plusMinusAverage', n='%sRbnDtl%sAutoSqshSub%s_pma' %(self.elem, (i+1), self.side))
			autoSqshAdl = pm.createNode('addDoubleLinear', n='%sRbnDtl%sAutoSqsh%s_adl' %(self.elem, (i+1), self.side))

			autoSqshSubPma.operation.set(2)
			autoSqshSubPma.input1D[0].set(1.0)
			pm.connectAttr(sqshMultAttr, autoSqshSubPma.input1D[1])
			pm.connectAttr(sqshDivMdv.outputX, autoSqshMdl.input1)
			pm.connectAttr(sqshMultAttr, autoSqshMdl.input2)
			pm.connectAttr(autoSqshSubPma.output1D, autoSqshAdl.input1)
			pm.connectAttr(autoSqshMdl.output, autoSqshAdl.input2)

			autoSqshBta = pm.createNode('blendTwoAttr', n='%sRbn%sAutoSqsh%s_bta' %(self.elem, (i+1), self.side))
			autoSqshBta.input[0].set(1.0)
			pm.connectAttr(self.midCtrl.autoSquash, autoSqshBta.attributesBlender)
			pm.connectAttr(autoSqshAdl.output, autoSqshBta.input[1])

			sqshPma = pm.createNode('plusMinusAverage', n='%sRbnDtl%sSqsh%s_adl' %(self.elem, (i+1), self.side))
			pm.connectAttr(dtlCtrl.squash, sqshPma.input1D[0])
			pm.connectAttr(self.midCtrl.squash, sqshPma.input1D[1])
			pm.connectAttr(autoSqshBta.output, sqshPma.input1D[2])

			# connect to joint scale
			for axis in self.sqshScAttrNames:
				pm.connectAttr(sqshPma.output1D, dtlJnt.attr(axis))

			# store to class var
			self.dtlCtrls.append(dtlCtrl)
			self.rbnJnts.append(dtlJnt)

		# delete tmp animCurve and frameCache
		pm.delete([c, fc])

		# --- skin the surf
		allRbnBindJnts = [self.baseJnt, self.midJnt, self.tipJnt]
		self.skinCluster = pm.skinCluster(allRbnBindJnts, self.surf, 
										tsb=True, dr=7, mi=2)
		
		pm.skinPercent(self.skinCluster, self.surf.cv[0:1][0], tv=[self.baseJnt, 1.0])

		pm.skinPercent(self.skinCluster, self.surf.cv[0:1][1], tv=[self.baseJnt, 0.8666])
		self.baseJnt.lockInfluenceWeights.set(True)
		pm.skinPercent(self.skinCluster, self.surf.cv[0:1][1], tv=[self.midJnt, 0.1334])

		self.lockInf(jnts=allRbnBindJnts, value=False)
		pm.skinPercent(self.skinCluster, self.surf.cv[0:1][2], tv=[self.baseJnt, 0.6])
		self.baseJnt.lockInfluenceWeights.set(True)
		pm.skinPercent(self.skinCluster, self.surf.cv[0:1][2], tv=[self.midJnt, 0.4])

		self.lockInf(jnts=allRbnBindJnts, value=False)
		pm.skinPercent(self.skinCluster, self.surf.cv[0:1][3], tv=[self.baseJnt, 0.2])
		self.baseJnt.lockInfluenceWeights.set(True)
		pm.skinPercent(self.skinCluster, self.surf.cv[0:1][3], tv=[self.midJnt, 0.8])

		self.lockInf(jnts=allRbnBindJnts, value=False)
		pm.skinPercent(self.skinCluster, self.surf.cv[0:1][4], tv=[self.tipJnt, 0.2])
		self.tipJnt.lockInfluenceWeights.set(True)
		pm.skinPercent(self.skinCluster, self.surf.cv[0:1][4], tv=[self.midJnt, 0.8])

		self.lockInf(jnts=allRbnBindJnts, value=False)
		pm.skinPercent(self.skinCluster, self.surf.cv[0:1][5], tv=[self.midJnt, 0.4])
		self.midJnt.lockInfluenceWeights.set(True)
		pm.skinPercent(self.skinCluster, self.surf.cv[0:1][5], tv=[self.tipJnt, 0.6])

		self.lockInf(jnts=allRbnBindJnts, value=False)
		pm.skinPercent(self.skinCluster, self.surf.cv[0:1][6], tv=[self.midJnt, 0.1334])
		self.midJnt.lockInfluenceWeights.set(True)
		pm.skinPercent(self.skinCluster, self.surf.cv[0:1][6], tv=[self.tipJnt, 0.8666])

		self.lockInf(jnts=allRbnBindJnts, value=False)
		pm.skinPercent(self.skinCluster, self.surf.cv[0:1][7], tv=[self.tipJnt, 1.0])


	def attach(self, base, tip, parent, tipParent, autoTwist=True, nonRoll=True, upAxis='+y', parentUpAxis='+y'):
		if isinstance(base, (str, unicode)):
			base = pm.PyNode(base)
		if isinstance(tip, (str, unicode)):
			tip = pm.PyNode(tip)
		if isinstance(parent, (str, unicode)):
			parent = pm.PyNode(parent)
		if isinstance(tipParent, (str, unicode)):
			tipParent = pm.PyNode(tipParent)

		upVec = misc.vectorStr(upAxis)
		parentUpVec = misc.vectorStr(parentUpAxis)

		misc.snapTransform('point', parent, self.rigCtrlGrp, False, True)
		pm.delete(pm.aimConstraint(tipParent, 
								self.rigCtrlGrp,
								aim=self.aimVec, u=upVec,
								wut='objectrotation',
								wuo=parent,
								wu=parentUpVec))

		pm.parentConstraint(parent, self.rigCtrlGrp, mo=True)
		pm.pointConstraint(base, self.baseCtrl)
		pm.pointConstraint(tip, self.tipCtrl)

		toLockHide = {'t':True, 'r':True, 's':True, 'v':True}
		misc.lockAttr(self.baseCtrl, **toLockHide)
		misc.lockAttr(self.tipCtrl, **toLockHide)
		
		midCtrlShp = self.midCtrl.getShape()

		# set default Len for autoSquash to work properly
		midCtrlShp.defaultLen.set(self.lenAdl.output.get())

		elemSide = (self.elem, self.side)
		if autoTwist == True:
			# auto twst grps
			self.autoTwstGrp = pm.group(em=True, n='%sRbnAutoTwst%s_grp' %elemSide)
			self.autoTwstGrp.visibility.set(False)
			misc.snapTransform('parent', parent, self.autoTwstGrp, False, True)
			
			self.autoTwstAimGrp = pm.group(em=True, n='%sRbnAutoTwstAim%s_grp' %elemSide)
			self.autoTwstZroGrp = pm.group(em=True, n='%sRbnAutoTwstZro%s_grp' %elemSide)
			pm.parent(self.autoTwstZroGrp, self.autoTwstAimGrp)
			misc.snapTransform('parent', self.baseJnt, self.autoTwstZroGrp, False, True)

			pm.parent(self.autoTwstAimGrp, self.autoTwstGrp)
			parentOfParent = parent.getParent()
			if parentOfParent:
				pm.parent(self.autoTwstGrp, parentOfParent)

			self.autoTwstPntCons = pm.pointConstraint(parent, self.autoTwstGrp)

			# fk auto twst joints
			self.baseAutoTwstConsJnt = pm.createNode('joint', n='%sRbnBaseAutoTwstCons%s_jnt' %elemSide)
			self.baseAutoTwstConsJnt.rotateOrder.set(self.rotateOrder)
			self.baseAutoTwstConsJnt.radius.set(self.radius)
			misc.snapTransform('parent', self.baseJnt, self.baseAutoTwstConsJnt, False, True)

			self.tipAutoTwstConsJnt = pm.createNode('joint', n='%sRbnTipAutoTwstCons%s_jnt' %elemSide)
			self.tipAutoTwstConsJnt.rotateOrder.set(self.rotateOrder)
			self.tipAutoTwstConsJnt.radius.set(self.radius)
			misc.snapTransform('parent', self.tipJnt, self.tipAutoTwstConsJnt, False, True)

			pm.parent(self.tipAutoTwstConsJnt, self.baseAutoTwstConsJnt)
			pm.parent(self.baseAutoTwstConsJnt, self.autoTwstZroGrp)
			
			pm.makeIdentity(self.baseAutoTwstConsJnt, n=False, apply=True)
			pm.makeIdentity(self.tipAutoTwstConsJnt, n=False, apply=True)

			pm.parentConstraint(parent, self.baseAutoTwstConsJnt, mo=True)
			pm.parentConstraint(tipParent, self.tipAutoTwstConsJnt, mo=True)

			# --- twist joint
			self.baseAutoTwstJnt = pm.createNode('joint', n='%sRbnBaseAutoTwst%s_jnt' %elemSide)
			self.baseAutoTwstJnt.rotateOrder.set(self.rotateOrder)
			self.baseAutoTwstJnt.radius.set(self.radius)
			misc.snapTransform('parent', self.baseAutoTwstConsJnt, self.baseAutoTwstJnt, False, True)
			
			self.tipAutoTwstJnt = pm.createNode('joint', n='%sRbnTipAutoTwst%s_jnt' %elemSide)
			self.tipAutoTwstJnt.rotateOrder.set(self.rotateOrder)
			self.tipAutoTwstJnt.radius.set(self.radius)
			misc.snapTransform('parent', self.tipAutoTwstConsJnt, self.tipAutoTwstJnt, False, True)

			pm.parent(self.baseAutoTwstJnt, self.baseAutoTwstConsJnt)
			pm.parent(self.tipAutoTwstJnt, self.baseAutoTwstConsJnt)

			pm.makeIdentity(self.baseAutoTwstJnt, n=False, apply=True)
			pm.makeIdentity(self.tipAutoTwstJnt, n=False, apply=True)

			# constraint auto twist
			pm.pointConstraint(self.tipAutoTwstConsJnt, self.tipAutoTwstJnt)
			self.tipUpVecGrp = pm.group(em=True, n='%sRbnTipAimUpVec%s_grp' %elemSide)
			misc.snapTransform('parent', self.tipAutoTwstConsJnt, self.tipUpVecGrp, False, True)
			pm.parent(self.tipUpVecGrp, self.tipAutoTwstConsJnt)

			upVecTrans = upVec * 0.1
			self.tipUpVecGrp.translate.set(upVecTrans)
			pm.aimConstraint(self.baseAutoTwstConsJnt, self.tipAutoTwstJnt,
								aim=self.aimVec*-1, u=upVec,
								wut='object',
								wuo=self.tipUpVecGrp)

			if nonRoll:
				# create non roll
				self.baseNonRollJnt = pm.createNode('joint', n='%sRbnBaseNonRoll%s_jnt' %elemSide)
				self.baseNonRollJnt.rotateOrder.set(self.rotateOrder)
				self.baseNonRollJnt.radius.set(self.radius*0.75)
				misc.snapTransform('parent', self.baseJnt, self.baseNonRollJnt, False, True)
				
				self.tipNonRollJnt = pm.createNode('joint', n='%sRbnTipNonRoll%s_jnt' %elemSide)
				self.tipNonRollJnt.rotateOrder.set(self.rotateOrder)
				self.tipNonRollJnt.radius.set(self.radius*0.75)
				misc.snapTransform('parent', self.tipJnt, self.tipNonRollJnt, False, True)

				pm.parent(self.tipNonRollJnt, self.baseNonRollJnt)
				pm.parent(self.baseNonRollJnt,  self.autoTwstZroGrp)

				pm.makeIdentity(self.baseNonRollJnt, n=False, apply=True)
				pm.makeIdentity(self.tipNonRollJnt, n=False, apply=True)

				# create non roll ik handle
				self.nonRollIkHndl, self.nonRollIkEff = pm.ikHandle(sj=self.baseNonRollJnt, 
												ee=self.tipNonRollJnt,
												sol='ikRPsolver')
				self.nonRollIkHndl.rename('%sRbnNonRoll%s_ikHndl' %elemSide)
				self.nonRollIkHndl.rename('%sRbnNonRoll%s_eff' %elemSide)
				self.nonRollIkHndl.poleVector.set([0, 0, 0])
				self.nonRollIkHndl.poleVector.lock()
				pm.parent(self.nonRollIkHndl, self.tipAutoTwstConsJnt)

				# non roll up vec grp
				self.baseUpVecGrp = pm.group(em=True, n='%sRbnBaseAimUpVec%s_grp' %elemSide)
				misc.snapTransform('parent', self.baseNonRollJnt, self.baseUpVecGrp, False, True)
				pm.parent(self.baseUpVecGrp, self.baseNonRollJnt)

				nonRollTrans = upVec * 0.1
				self.baseUpVecGrp.translate.set(nonRollTrans)

				pm.aimConstraint(self.tipAutoTwstConsJnt, self.baseAutoTwstJnt,
								aim=self.aimVec, u=upVec,
								wut='object',
								wuo=self.baseUpVecGrp)
			else:
				self.baseUpVecGrp = pm.group(em=True, n='%sRbnBaseAimUpVec%s_grp' %elemSide)
				misc.snapTransform('parent', self.baseJnt, self.baseUpVecGrp, False, True)
				pm.parent(self.baseUpVecGrp, self.baseAutoTwstConsJnt)
				self.baseUpVecGrp.translate.set(upVecTrans)

				pm.aimConstraint(self.tipAutoTwstConsJnt, self.baseAutoTwstJnt,
								aim=self.aimVec, u=upVec,
								wut='object',
								wuo=self.baseUpVecGrp)

			pm.connectAttr(self.baseAutoTwstJnt.attr(self.twstRoAttrName), self.twstMultMdv.input1X, f=True)
			pm.connectAttr(self.tipAutoTwstJnt.attr(self.twstRoAttrName), self.twstMultMdv.input1Y, f=True)

class RibbonFkRig(RibbonRig):
	def __init__(self, 
				jnts=[],
				ctrlJnts=[],
				aimAxis='+x',
				upAxis='+z',
				ctrlShp='crossCircle',
				ctrlColor='yellow',
				dtlCtrlShp='circle',
				dtlCtrlColor='lightBlue',
				localWorldRig=True,
				**kwargs):
		super(RibbonFkRig, self).__init__(aimAxis=aimAxis,
										upAxis=upAxis,
										**kwargs)

		self.tmpJnts = self.jntsArgs(jnts)
		self.ctrlTmpJnts = self.jntsArgs(ctrlJnts)

		self.ctrlShp = ctrlShp
		self.ctrlColor = ctrlColor
		self.dtlCtrlShp = dtlCtrlShp
		self.dtlCtrlColor = dtlCtrlColor
		self.localWorldRig = localWorldRig

		self.jnts = []
		self.ctrlJnts = []
		self.dtlCtrls = []
		self.dtlGCtrls = []
		self.ctrls = []
		self.gCtrls = []

		self.ctrlZgrps = []
		self.ctrlOffsetGrps = []
		self.ctrlSpaceGrps = []

	def rig(self):
		# --- figure out the axis
		# self.rotateOrder = '%s%s%s' %(self.aimAxis[-1], self.otherAxis[-1], self.upAxis[-1]) 
		# self.twstRoAttrName =  'r%s' %self.aimAxis[-1]
		# self.sqshScAttrNames = ('s%s' %self.upAxis[-1], 's%s' %self.otherAxis[-1])

		# --- main grps
		_name = (self.elem, self.side)
		self.rigCtrlGrp = pm.group(em=True, n='%sRbnRig%s_grp' %_name)
		self.rigUtilGrp = pm.group(em=True, n='%sRbnUtil%s_grp' %_name)
		self.rigStillGrp = pm.group(em=True, n='%sRbnStill%s_grp' %_name)
		self.rigStillGrp.visibility.set(False)

		# parent main grps
		pm.parent(self.rigCtrlGrp, self.animGrp)
		pm.parent(self.rigUtilGrp, self.utilGrp)
		pm.parent(self.rigStillGrp, self.stillGrp)

		# constraint to parent
		if self.parent:
			misc.snapTransform('parent', self.parent, self.rigCtrlGrp, True, False)
			misc.snapTransform('scale', self.parent, self.rigCtrlGrp, True, False)

		# detail ctrl group
		self.dtlCtrlGrp = pm.group(em=True, n='%sRbnDtlCtrl%s_grp' %_name)
		pm.parent(self.dtlCtrlGrp, self.rigCtrlGrp)

		# detail joint group
		self.dtlJntGrp = pm.group(em=True, n='%sRbnDtlJnt%s_grp' %_name)
		pm.parent(self.dtlJntGrp, self.rigUtilGrp)

		# create pos jnt
		self.allPosGrp = pm.group(em=True, n='%sRbnPos%s_grp' %_name)
		pm.parent(self.allPosGrp, self.rigStillGrp)

		# --- create curve
		tmpJntLen = len(self.tmpJnts)
		ctrlTmpJntLen = len(self.ctrlTmpJnts)

		cmd = 'aCrvTmp = pm.curve(d=2, p=['
		for i, tmpJnt in enumerate(self.ctrlTmpJnts):
			tr = pm.xform(tmpJnt, q=True, ws=True, t=True)
			cmd += '(%s, %s, %s)' %(tr[0], tr[1], tr[2])

			if i < tmpJntLen - 1:
				cmd += ','

		cmd += '])'
		
		exec(cmd)

		aCrv = pm.createNode('transform')
		misc.snapTransform('parent', self.ctrlTmpJnts[0], aCrv, False, True)
		pm.parent(aCrvTmp.getShape(), aCrv, s=True)
		pm.delete(aCrvTmp)
		
		bCrv = aCrv.duplicate()[0]
		pm.xform(aCrv, os=True, t=self.upVec*0.5)
		pm.xform(bCrv, os=True, t=self.upVec*-0.5)

		self.createSurf(crvs=[aCrv, bCrv])
		pm.parent(self.surf, self.rigStillGrp)

		# main surface loop
		posGrps, posNodes, aimNodes = [], [], []
		for i, tmpJnt in enumerate(self.tmpJnts):
			iName = (self.elem, (i + 1), self.side)
			
			# --- pos grp
			posGrp = pm.group(em=True, n='%sRbnPos%s%s_grp' %iName)
			posNode = pm.createNode('pointOnSurfaceInfo', n='%sRbn%s%s_posi' %iName)
			posGrps.append(posGrp)

			tr = pm.xform(tmpJnt, q=True, ws=True, t=True)
			uvs = misc.getClosestSurfUvFromPoint(self.surf.getShape(), pm.dt.Vector(tr))
			posNode.parameterU.set(uvs[0])
			posNode.parameterV.set(uvs[1])

			aimCon = pm.createNode('aimConstraint', n='%sRbn%s%s_aimConstraint' %iName)
			pm.connectAttr(self.surf.worldSpace[0], posNode.inputSurface)
			pm.connectAttr(posNode.normal, aimCon.worldUpVector)
			pm.connectAttr(posNode.tangentV, aimCon.target[0].targetTranslate)
			pm.connectAttr(posNode.position, posGrp.translate)
			pm.connectAttr(aimCon.constraintRotate, posGrp.rotate)

			aimCon.aimVector.set(self.aimVec)
			aimCon.upVector.set(self.otherVec)

			pm.parent(posGrp, self.allPosGrp)
			pm.parent(aimCon, posGrp)

			# --- dtl jnt
			dtlJnt = tmpJnt.duplicate(po=True, n='%sDtl%s%s_jnt' %iName)[0]
			dtlJntZroGrp = misc.zgrp(dtlJnt, element='Zro', suffix='grp')[0]


			pm.parent(dtlJntZroGrp, self.dtlJntGrp)
			self.jnts.append(dtlJnt)

			misc.snapTransform('parent', posGrp, dtlJntZroGrp, True, True)

			# --- dtl ctrl
			dtlCtrl = controller.JointController(name='%sDtl%s%s_ctrl' %(self.elem, (i+1), self.side),
												st=self.dtlCtrlShp, 
												color=self.dtlCtrlColor, 
												axis=self.aimAxis,
												scale=(self.size*0.75),
												draw=False)
			dtlCtrl.lockAttr(v=True)
			dtlCtrl.hideAttr(v=True)
			dtlCtrl.rotateOrder.set(self.rotateOrder)
			dtlCtrlGCtrl = dtlCtrl.addGimbal()
			dtlCtrlZroGrp = misc.zgrp(dtlCtrl, element='Zro', suffix='grp')[0]
			pm.parent(dtlCtrlZroGrp, self.dtlCtrlGrp)

			self.dtlCtrls.append(dtlCtrl)
			self.dtlGCtrls.append(dtlCtrlGCtrl)

			misc.snapTransform('parent', dtlJnt, dtlCtrlZroGrp, False, True)

			misc.snapTransform('parent', posGrp, dtlCtrlZroGrp, True, False)

			misc.snapTransform('parent', dtlCtrlGCtrl, dtlJntZroGrp, False, False)
			misc.snapTransform('scale', dtlCtrlGCtrl, dtlJntZroGrp, False, False)

		localObj = self.rigCtrlGrp
		worldObj = self.animGrp
		for i in xrange(ctrlTmpJntLen):
			tmpCtrlJnt = self.ctrlTmpJnts[i]
			iName = (self.elem, (i + 1), self.side)

			# --- dtl jnt
			ctrlJnt = tmpCtrlJnt.duplicate(po=True, n='%s%s%s_jnt' %iName)[0]
			self.ctrlJnts.append(ctrlJnt)

			if i > 0:
				pm.parent(ctrlJnt, self.ctrlJnts[i-1])
				localObj = self.gCtrls[i-1]
				worldObj = self.rigCtrlGrp
			else:
				pm.parent(ctrlJnt, self.rigUtilGrp)

			# if this is not the last joint
			if i < ctrlTmpJntLen - 1:
				# --- dtl ctrl
				ctrl = controller.JointController(name='%s%s%s_ctrl' %iName,
													st=self.ctrlShp, 
													color=self.ctrlColor, 
													axis=self.aimAxis,
													scale=(self.size),
													draw=False)
				ctrl.lockAttr(s=True, v=True)
				ctrl.hideAttr(s=True, v=True)
				ctrl.rotateOrder.set(self.rotateOrder)
				gCtrl = ctrl.addGimbal()

				ctrlOffsetGrp = misc.zgrp(ctrl, element='Ofst', suffix='grp', preserveHeirachy=True)[0]
				ctrlSpaceGrp = misc.zgrp(ctrlOffsetGrp, element='Space', remove='Ofst', suffix='grp', preserveHeirachy=True)[0]
				ctrlZgrp = misc.zgrp(ctrlSpaceGrp, element='Zro', remove='Space', suffix='grp')[0]
				
				misc.snapTransform('parent', ctrlJnt, ctrlZgrp, False, True)

				if i > 0:
					pm.parent(ctrlZgrp, self.gCtrls[i-1])
					localObj = self.gCtrls[i-1]
					worldObj = self.rigCtrlGrp
				else:
					pm.parent(ctrlZgrp, self.rigCtrlGrp)

				self.ctrls.append(ctrl)
				self.gCtrls.append(gCtrl)
				self.ctrlZgrps.append(ctrlZgrp)
				self.ctrlOffsetGrps.append(ctrlOffsetGrp)
				self.ctrlSpaceGrps.append(ctrlSpaceGrp)

				misc.snapTransform('parent', gCtrl, ctrlJnt, False, False)

				if self.localWorldRig == True:
					misc.createLocalWorld(objs=[self.ctrls[i], localObj, worldObj, self.ctrlSpaceGrps[i]], 
										constraintType='parent', attrName='localWorld')

		baseCtrlShp = self.ctrls[0].getShape()

		# dtl vis ctrl
		dtlCtrlVisAttr = misc.addNumAttr(baseCtrlShp, 'detailCtrl_vis', 'long', min=0, max=1, dv=0)
		pm.connectAttr(dtlCtrlVisAttr, self.dtlCtrlGrp.visibility)

		# bind the surface
		self.skinCluster = pm.skinCluster(self.ctrlJnts, self.surf, 
										tsb=True, dr=2, mi=2)

		# set skin weights
		self.lockInf(jnts=self.ctrlJnts, value=False)
		for u in xrange(2):
			for v in xrange(ctrlTmpJntLen):
				pm.skinPercent(self.skinCluster, self.surf.cv[u][v], tv=[self.ctrlJnts[v], 1.0])

		