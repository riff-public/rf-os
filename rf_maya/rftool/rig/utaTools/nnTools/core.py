#Import python modules
import os, sys

#Import GUI
from PySide import QtCore
from PySide import QtGui
from PySide import QtUiTools
from shiboken import wrapInstance

# Import Maya module
import maya.OpenMayaUI as mui
import maya.cmds as mc
import maya.mel as mm

from tool.rig.nnTools import nnTools as nn
reload( nn )

moduleFile 	= sys.modules[__name__].__file__
moduleDir 	= os.path.dirname( moduleFile )
sys.path.append( moduleDir )

def getMayaWindow():
	ptr = mui.MQtUtil.mainWindow()
	if ptr is not None :
		# ptr = mui.MQtUtil.mainWindow()
		return wrapInstance( long( ptr ) , QtGui.QMainWindow )

def deleteUI( ui ) : 
	if mc.window( ui , exists = True) : 
		mc.deleteUI( ui )

class MyForm( QtGui.QMainWindow ) :

	def __init__( self , parent = None ) :
		# Setup Window
		super( MyForm , self ).__init__( parent )

		self.runUI()
		self.initFacial()
		self.facialCtrl()

		self.ui.studAPushButton.clicked.connect( self.getFacialA )
		self.ui.studBPushButton.clicked.connect( self.getFacialB )
		self.ui.studCPushButton.clicked.connect( self.getFacialC )
		self.ui.studDPushButton.clicked.connect( self.getFacialD )

		# self.aa = 'ffff_ctrl'

	def runUI( self ) :
		self.mayaUI = 'minifigFacialUI'
		deleteUI( self.mayaUI )

		# read .ui directly
		loader = QtUiTools.QUiLoader()
		loader.setWorkingDirectory( moduleDir )

		f = QtCore.QFile( '%s/ui.ui' % moduleDir)
		f.open( QtCore.QFile.ReadOnly )

		self.myWidget = loader.load( f , self )
		self.ui = self.myWidget

		f.close()

		self.ui.show()

	def initFacial( self ) :
		iconA = QtGui.QPixmap( 'O:/studioTools/maya/python/tool/rig/nnTools/python/minifigFacial/icon/facialA.jpg' )
		self.ui.studAPushButton.setIcon( QtGui.QIcon( iconA ) )
		self.ui.studAPushButton.setIconSize( QtCore.QSize( 60 , 60 ) )

		iconB = QtGui.QPixmap( 'O:/studioTools/maya/python/tool/rig/nnTools/python/minifigFacial/icon/facialB.jpg' )
		self.ui.studBPushButton.setIcon(QtGui.QIcon( iconB ) )
		self.ui.studBPushButton.setIconSize( QtCore.QSize( 60 , 60 ) )

		iconC = QtGui.QPixmap( 'O:/studioTools/maya/python/tool/rig/nnTools/python/minifigFacial/icon/facialC.jpg' )
		self.ui.studCPushButton.setIcon( QtGui.QIcon( iconC ) )
		self.ui.studCPushButton.setIconSize( QtCore.QSize( 60 , 60 ) )

		iconD = QtGui.QPixmap( 'O:/studioTools/maya/python/tool/rig/nnTools/python/minifigFacial/icon/facialD.jpg' )
		self.ui.studDPushButton.setIcon( QtGui.QIcon( iconD ) )
		self.ui.studDPushButton.setIconSize( QtCore.QSize( 60 , 60 ) )

	def facialCtrl( self ) :
		self.browCtrlLFTGrp 		= '*:browLFT_ctrl_grp'
		self.browCtrlRGTGrp 		= '*:browRGT_ctrl_grp'
		self.squintCtrlLFTGrp 		= '*:squintLFT_ctrl_grp'
		self.squintCtrlRGTGrp 		= '*:squintRGT_ctrl_grp'
		self.eyeHilightCtrlLFTGrp 	= '*:eyeHilightLFT_ctrl_grp'
		self.eyeHilightCtrlRGTGrp 	= '*:eyeHilightRGT_ctrl_grp'
		self.facialSettingCtrl 		= '*:facialSetting_ctrl'
		self.eyeLFTCtrlGrp 			= '*:eyeLFT_ctrl_grp'
		self.eyeRGTCtrlGrp 			= '*:eyeRGT_ctrl_grp'
		self.browLFTCtrl 			= '*:browLFT_ctrl'
		self.browRGTCtrl 			= '*:browRGT_ctrl'
		self.eyeLFTCtrl 			= '*:eyeLFT_ctrl'
		self.eyeRGTCtrl 			= '*:eyeRGT_ctrl'
		self.facialCtrlLoc 			= '*:facialCtrl_loc'

	def getFacialA( self ) :
		self.facialCtrl()

		mc.setAttr( self.facialSettingCtrl + '.faceSet' , 0 )
		mc.setAttr( self.browCtrlLFTGrp + '.v' , 1 )
		mc.setAttr( self.browCtrlRGTGrp + '.v' , 1 )
		mc.setAttr( self.squintCtrlLFTGrp + '.v' , 1 )
		mc.setAttr( self.squintCtrlRGTGrp + '.v' , 1 )
		mc.setAttr( self.eyeHilightCtrlLFTGrp + '.v' , 1 )
		mc.setAttr( self.eyeHilightCtrlRGTGrp + '.v' , 1 )
		mc.setAttr( self.eyeLFTCtrlGrp + '.v' , 1 )
		mc.setAttr( self.eyeRGTCtrlGrp + '.v' , 1 )
		mc.setAttr( self.eyeLFTCtrlGrp + '.tx' , 0 )
		mc.setAttr( self.browLFTCtrl + '.visible' , 1 )
		mc.setAttr( self.browRGTCtrl + '.visible' , 1 )
		mc.setAttr( self.eyeLFTCtrl + '.visible' , 1 )
		mc.setAttr( self.eyeRGTCtrl + '.visible' , 1 )
		mc.setAttr( self.facialCtrlLoc + '.L_eyeMoveX_internalPos' , 0.137 )

	def getFacialB( self ) :
		self.facialCtrl()

		mc.setAttr( self.facialSettingCtrl + '.faceSet' , 0 )
		mc.setAttr( self.browCtrlLFTGrp + '.v' , 1 )
		mc.setAttr( self.browCtrlRGTGrp + '.v' , 1 )
		mc.setAttr( self.squintCtrlLFTGrp + '.v' , 0 )
		mc.setAttr( self.squintCtrlRGTGrp + '.v' , 0 )
		mc.setAttr( self.eyeHilightCtrlLFTGrp + '.v' , 0 )
		mc.setAttr( self.eyeHilightCtrlRGTGrp + '.v' , 0 )
		mc.setAttr( self.eyeLFTCtrlGrp + '.v' , 1 )
		mc.setAttr( self.eyeRGTCtrlGrp + '.v' , 1 )
		mc.setAttr( self.eyeLFTCtrlGrp + '.tx' , 0 )
		mc.setAttr( self.browLFTCtrl + '.visible' , 1 )
		mc.setAttr( self.browRGTCtrl + '.visible' , 1 )
		mc.setAttr( self.eyeLFTCtrl + '.visible' , 1 )
		mc.setAttr( self.eyeRGTCtrl + '.visible' , 1 )
		mc.setAttr( self.facialCtrlLoc + '.L_eyeMoveX_internalPos' , 0.137 )

	def getFacialC( self ) :
		self.facialCtrl()

		mc.setAttr( self.facialSettingCtrl + '.faceSet' , 0 )
		mc.setAttr( self.browCtrlLFTGrp + '.v' , 0 )
		mc.setAttr( self.browCtrlRGTGrp + '.v' , 0 )
		mc.setAttr( self.squintCtrlLFTGrp + '.v' , 0 )
		mc.setAttr( self.squintCtrlRGTGrp + '.v' , 0 )
		mc.setAttr( self.eyeHilightCtrlLFTGrp + '.v' , 0 )
		mc.setAttr( self.eyeHilightCtrlRGTGrp + '.v' , 0 )
		mc.setAttr( self.eyeLFTCtrlGrp + '.v' , 1 )
		mc.setAttr( self.eyeRGTCtrlGrp + '.v' , 0 )
		mc.setAttr( self.eyeLFTCtrlGrp + '.tx' , -0.125 )
		mc.setAttr( self.browLFTCtrl + '.visible' , 0 )
		mc.setAttr( self.browRGTCtrl + '.visible' , 0 )
		mc.setAttr( self.eyeLFTCtrl + '.visible' , 1 )
		mc.setAttr( self.eyeRGTCtrl + '.visible' , 0 )
		mc.setAttr( self.facialCtrlLoc + '.L_eyeMoveX_internalPos' , 0 )

	def getFacialD( self ) :
		self.facialCtrl()
		mc.setAttr( self.facialSettingCtrl + '.faceSet' , 1 )
