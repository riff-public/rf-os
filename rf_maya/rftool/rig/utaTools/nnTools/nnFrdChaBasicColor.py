import maya.cmds as mc
import maya.mel as mm

## body
nodeBody = mc.shadingNode( 'lambert' , asShader=True )
shdBody = mc.rename( nodeBody , 'body_lambert' )
mc.setAttr( '%s.color' % shdBody , 0.835443, 0.614889, 0.454735 , typ='double3' )
try:
	mc.select( '*:mesh_body' )
except:
	mc.select( '*:*:mesh_body' )
mm.eval('hyperShade -assign %s' % shdBody)

## nail
nodeNail = mc.shadingNode( 'lambert' , asShader=True )
shdNail = mc.rename( nodeNail , 'nail_lambert' )
mc.setAttr( '%s.color' % shdNail , 0.78, 0.78, 0.78 , typ='double3' )
try:
	mc.select( '*:mesh_nails' )
except:
	mc.select( '*:*:mesh_nails' )
mm.eval('hyperShade -assign %s' % shdNail)

## eyelash
nodeEyelash = mc.shadingNode( 'lambert' , asShader=True )
shdEyelash = mc.rename( nodeEyelash , 'eyelash_lambert' )
mc.setAttr( '%s.color' % shdEyelash , 0.139666, 0.139666, 0.139666 , typ='double3' )
try:
	mc.select( '*:mesh_eyelash??T' )
except:
	mc.select( '*:*:mesh_eyelash??T' )
mm.eval('hyperShade -assign %s' % shdEyelash)

## cornea
nodeCornea = mc.shadingNode( 'lambert' , asShader=True )
shdCornea = mc.rename( nodeCornea , 'cornea_lambert' )
mc.setAttr( '%s.color' % shdCornea , 0.139666, 0.139666, 0.139666 , typ='double3' )
mc.setAttr( '%s.transparency' % shdCornea , 0.84, 0.84, 0.84 , typ='double3' )
try:
	mc.select( '*:mesh_cornea??T' )
except:
	mc.select( '*:*:mesh_cornea??T' )
mm.eval('hyperShade -assign %s' % shdCornea)

## eyeSpec
nodeEyeSpec = mc.shadingNode( 'lambert' , asShader=True )
shdEyeSpec = mc.rename( nodeEyeSpec , 'eyeSpec_lambert' )
mc.setAttr( '%s.color' % shdEyeSpec , 0.78, 0.78, 0.78 , typ='double3' )
try:
	mc.select( '*:mesh_eyeSpec??T' )
except:
	mc.select( '*:*:mesh_eyeSpec??T' )
mm.eval('hyperShade -assign %s' % shdEyeSpec)

## pupil
nodePupil = mc.shadingNode( 'lambert' , asShader=True )
shdPupil = mc.rename( nodePupil , 'pupil_lambert' )
mc.setAttr( '%s.color' % shdPupil , 0.122, 0.122, 0.122 , typ='double3' )
try:
	mc.select( '*:mesh_pupil??T' )
except:
	mc.select( '*:*:mesh_pupil??T' )
mm.eval('hyperShade -assign %s' % shdPupil)

## iris
nodeIris = mc.shadingNode( 'lambert' , asShader=True )
shdIris = mc.rename( nodeIris , 'iris_lambert' )
mc.setAttr( '%s.color' % shdIris , 0.112, 0.316, 0.118 , typ='double3' )
try:
	mc.select( '*:mesh_iris??T' )
except:
	mc.select( '*:*:mesh_iris??T' )
mm.eval('hyperShade -assign %s' % shdIris)

## sclera
nodeSclera = mc.shadingNode( 'lambert' , asShader=True )
shdSclera = mc.rename( nodeSclera , 'sclera_lambert' )
mc.setAttr( '%s.color' % shdSclera , 1, 1, 1 , typ='double3' )
try:
	mc.select( '*:mesh_sclera??T' )
except:
	mc.select( '*:*:mesh_sclera??T' )
mm.eval('hyperShade -assign %s' % shdSclera)

## tongue
nodeTongue = mc.shadingNode( 'lambert' , asShader=True )
shdTongue = mc.rename( nodeTongue , 'tongue_lambert' )
mc.setAttr( '%s.color' % shdTongue , 0.557, 0.127, 0.127 , typ='double3' )
try:
	mc.select( '*:mesh_tongue' )
except:
	mc.select( '*:*:mesh_tongue' )
mm.eval('hyperShade -assign %s' % shdTongue)

## gum
nodeGum = mc.shadingNode( 'lambert' , asShader=True )
shdGum = mc.rename( nodeGum , 'gum_lambert' )
mc.setAttr( '%s.color' % shdGum , 0.557, 0.188, 0.127 , typ='double3' )
try:
	mc.select( '*:*Gum' )
except:
	mc.select( '*:*:*Gum' )
mm.eval('hyperShade -assign %s' % shdGum)

## teeth
nodeTeeth = mc.shadingNode( 'lambert' , asShader=True )
shdTeeth = mc.rename( nodeTeeth , 'teeth_lambert' )
mc.setAttr( '%s.color' % shdTeeth , 0.89, 0.89, 0.89 , typ='double3' )
try:
	mc.select( '*:*Teeth' )
except:
	mc.select( '*:*:*Teeth' )
mm.eval('hyperShade -assign %s' % shdTeeth)

## man eyelash
nodeManEyelash = mc.shadingNode( 'lambert' , asShader=True )
shdManEyelash = mc.rename( nodeManEyelash , 'manEyelash_lambert' )
mc.setAttr( '%s.color' % shdManEyelash , 0.139666, 0.139666, 0.139666 , typ='double3' )
try:
	mc.select( '*:mesh_manEyelash' )
except:
	mc.select( '*:*:mesh_manEyelash' )
mm.eval('hyperShade -assign %s' % shdManEyelash)

mc.select( cl=True )