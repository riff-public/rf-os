import maya.cmds as mc
import maya.mel as mm

def headSS( ns = 'Rig' , nameBsnNode = 'headSS_bsn' ) :
	'''
		for minifig projects
		add memberships geo to squash stertch head , add blendShape , parCon scaCon with hair_ctrl
		selected 	: group hair or hat
		return 		: None
	'''

	sel 		= mc.ls( sl=True )[0]
	listSels 	= mc.listRelatives( sel , ad=True )

	## check constraint
	for i in listSels :
		types =  mc.nodeType( i ) 
		if types == 'parentConstraint' or types == 'scaleConstraint' :
			mc.delete( i )

	## duplicate group and rename
	dupGroup 	= mc.duplicate( sel )[0]
	checkLast 	= dupGroup.split( '_' )[-1]
	listChilds 	= mc.listRelatives( dupGroup , ad=True , f=True )

	for child in listChilds :
		newName = child.split( '|' )[-1] + '_bsh'
		name 	= mc.rename( child , newName )

	if 'grp1' == checkLast :
		groupBsh = mc.rename( dupGroup , dupGroup.replace( '_grp1' , '_bsh' ) )

	else :
		groupBsh = mc.rename( dupGroup , dupGroup + '_bsh' )

	## add edit membership
	listMesh = []
	listAll = mc.listRelatives( groupBsh , ad=True , type='transform' , f=True )

	if listAll :
		for child in listAll:
			chShps = mc.listRelatives( child , shapes=True , f=True )
			if chShps:
				shapes = chShps[0]
				checkShapes = mc.nodeType( shapes )
				if checkShapes == 'mesh' :
					listMesh.append( child.split( '|' )[-1] )

	for mesh in listMesh :
		meshShp = mc.listRelatives( mesh , shapes=True )[0]
		selVtx 	= mc.polyEvaluate( mesh , v=True )
		vtx 	= '%s.vtx[0:%f]' %( meshShp , selVtx )
		mm.eval( 'sets -fe %s:squash1Set %s' %( ns , vtx ) )
		mm.eval( 'select -add %s' % vtx )

	## blend shape
	mc.blendShape( groupBsh , sel , foc=True , o='local' , tc=0 , n=nameBsnNode )
	mc.setAttr( '%s.%s' %( nameBsnNode , groupBsh ) , 1 )

	## constraint
	mc.parentConstraint( '%s:hair_ctrl' % ns , sel , mo=True , w=1 )
	mc.scaleConstraint( '%s:hair_ctrl' % ns , sel , mo=True , w=1 )

	## wrap group
	mc.setAttr( '%s.v' % groupBsh , 0 )
	mc.parent( groupBsh , '%s:headDefStill_grp' % ns )