import maya.cmds as mc
class misc :
	def checkAssets(self) : 
		ls = mc.ls(sl = True)
		allNameSpace = []
		for each in ls : 
			if ':' in each : 
				ns = each.split(':')[0]
				if not ns in allNameSpace : 
					allNameSpace.append(ns)
		
		ok = []
		out = []
		ref = self.allNamespaces()
		for each in allNameSpace : 
			if each in ref : 
				ok.append(each)
				
			else : 
				out.append(each)
		#print allNameSpace		
		#print ok
		#print out
		
		file = mc.file(q = True, sn = True).split('/')[-1]		
		print 'file name %s' % file
		print '-------------------'
		for each in ok : 
			print each
			
		print '-------------------'
				
	def allNamespaces(self) : 
		allNamespaces = mc.namespaceInfo(lon = True)
		allNamespaces.remove('UI')
		allNamespaces.remove('shared')
		
		ref = []
		for each in allNamespaces : 
			mc.select('%s:*' % each)
			ls = mc.ls(sl = True)
			if mc.referenceQuery( ls[0], isNodeReferenced=True ) : 
				ref.append(each) 
				
		return ref
		
	def checkAssets2(self) : 
		ls = mc.ls(sl = True)
		allNameSpace = []
		for each in ls : 
			if ':' in each : 
				ns = each.split(':')[0]
				if not ns in allNameSpace : 
					allNameSpace.append(ns)
							
		file = mc.file(q = True, sn = True).split('/')[-1]		
		print 'file name %s' % file
		print '-------------------'
		for each in allNameSpace : 
			print each
			
		print '-------------------'
		
