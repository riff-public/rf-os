import maya.cmds as mc 
from functools import partial
import sys
import os
import genScript 
#sys.path.append('P:\BKK\Animators_Playground\Ta\scripts')

def run() : 
	a = exportToPropRig()
	a.show()

#center prop
class exportToPropRig() : 

	def __init__(self) : 
		self.win = 'exportToPropRigUI'
		if sys.platform == 'win32' :
			self.path = 'P:/Lego_Friends/asset/3D/'
		if sys.platform == 'linux2' : 
			self.path = '/dsPipe/Lego_Friends/asset/3D/'
	
	def show(self) : 
		if(mc.window(self.win, exists = True)) : 
			mc.deleteUI(self.win)
			
		mc.window(self.win, t = 'export assets v2.2', wh = [300,200])
		mc.columnLayout(adj = 1, rs = 4)
		mc.text(l = 'name')
		mc.textField('%s_TX' % self.win)
		mc.text(l = 'output direcotry')
		mc.textField('%s_directoryTX' % self.win, tx = 'finish')
		mc.frameLayout(borderStyle = 'etchedIn', lv = False)
		mc.rowColumnLayout(nc = 2, cw = [(1, 150), (2, 150)])
		mc.checkBox('%s_MultipleObject_CB' % self.win, l = 'Multiply Objects')
		mc.checkBox('%s_ObjectName_CB' % self.win, l = 'Object Name')
		mc.checkBox('%s_MO_CB' % self.win, l = 'Maintain Offset')
		mc.checkBox('%s_AS_CB' % self.win, l = 'Use Asset Name')
		mc.checkBox('%s_OverrideOP_CB' % self.win, l = 'Override Output')

		mc.setParent('..')
		mc.setParent('..')
		
		mc.frameLayout(lv = 0, borderStyle = 'etchedIn')
		mc.rowColumnLayout(nc = 3, cw = [(1, 100), (2, 100), (3, 100)], cs = [(2, 4), (3, 4)])
		mc.textScrollList('%s_lv1' % self.win, sc = partial(self.browseLv2))
		mc.textScrollList('%s_lv2' % self.win, sc = partial(self.browseLv3))
		mc.textScrollList('%s_lv3' % self.win, sc = partial(self.setText))
		mc.textField('%s_lv1TX' % self.win)
		mc.textField('%s_lv2TX' % self.win)
		mc.textField('%s_lv3TX' % self.win)
		mc.setParent('..')
		mc.setParent('..')
		
		mc.button(l = 'Get Name', c = partial(self.fillUI))
		mc.button(l = 'Export', c = partial(self.exportToPropRig))
		mc.showWindow()
		mc.window(self.win, e = True, wh = [300, 416])
		self.fillUI()
		
	
	def exportToPropRig(self, arg = None) : 
	
		dir = mc.textField('%s_directoryTX' % self.win, q = True, tx = True)
		multipleCheck = mc.checkBox('%s_MultipleObject_CB' % self.win, q = True, v = True)
		getObjName = mc.checkBox('%s_ObjectName_CB' % self.win, q = True, v = True)
		mo = mc.checkBox('%s_MO_CB' % self.win, q = True, v = True)
	
		ls = mc.ls(sl = True)
		for i in range(len(ls)) : 
			loc = mc.spaceLocator(p = [0, 0, 0])
			if mc.checkBox('%s_MO_CB' % self.win, q = True, v = True) : 
				loc2 = mc.spaceLocator(p = [0, 0, 0])
				mc.delete(mc.parentConstraint(ls[i], loc2[0]))
			
			
			mc.delete(mc.parentConstraint(loc[0], ls[i]))
			mc.delete(loc)
			
			geoGrp = mc.group(ls[i], n = '%s_Grp' % ls[i])
			mc.xform(geoGrp, os = True, piv = [0, 0, 0])
			
			
			if not (mc.objExists('Rig_Grp')) : 
				genScript.importRigGrp()
				
			else : 
				mc.rename('Rig_Grp', 'Rig_Grp2')
				mc.rename('Root_Ctrl', 'Root_Ctrl2')
				mc.rename('SuperRoot_Ctrl', 'SuperRoot_Ctrl2')
				mc.rename('Geo_Grp', 'Geo_Grp2')
				genScript.importRigGrp()
			
			mc.parentConstraint('Root_Ctrl', geoGrp)
			mc.scaleConstraint('Root_Ctrl', geoGrp, mo = True)
			
			AnimGrp = mc.group(geoGrp, n = 'Anim_Grp')
			mc.parent(AnimGrp, 'Geo_Grp')
			
			if mo == 1 : 
				mc.delete(mc.parentConstraint(loc2[0], 'SuperRoot_Ctrl'))
				mc.delete(loc2)
			
			fileName = ''
			
			if getObjName == 1 : 				
				fileName = ls[i]
			
			else : 
				fileName = mc.textField('%s_TX' % self.win, q = True, tx = True)
				
			currentPath = mc.file(q = True, sn = True)
			currentPath = currentPath.replace(currentPath.split('/')[-1], '')
			
			path = ''
			
			if multipleCheck == 1 : 
				path =('%s%s%d' % (dir, fileName, i + 1))
				
			else : 
				path =('%s%s' % (dir, fileName))
		
			if not os.path.exists(dir) : 
				os.makedirs(dir)
				
			mc.select('Rig_Grp')
			mc.file(path, force = True, options = 'v=0', typ = 'mayaAscii', pr = True, es = True)
			
			mc.select('Anim_Grp', hi = True)
			mc.delete('Anim_Grp')
			mc.xform('SuperRoot_Ctrl', ws = True, t = [0, 0, 0])
			mc.select('Rig_Grp', hi = True)
			mc.lockNode(l = 0)
			mc.delete('Rig_Grp')
			
			if mc.objExists('Rig_Grp2') : 
				mc.rename('Rig_Grp2', 'Rig_Grp')
				mc.rename('Root_Ctrl2', 'Root_Ctrl')
				mc.rename('SuperRoot_Ctrl2', 'SuperRoot_Ctrl')
				mc.rename('Geo_Grp2', 'Geo_Grp')
			
		
	def fillUI(self, arg = None) : 

		currentPath = mc.file(q = True, sn = True)
		currentPath = currentPath.replace(currentPath.split('/')[-1], '')
		mc.textField('%s_directoryTX' % self.win, e = True, tx = '%soutput' % currentPath)
		self.browseLv1()


	def fillAttr(self, arg = None) : 
		if mc.objExists('Rig_Grp') : 
			assetType = mc.textField('%s_AssetTypeTX' % self.win, q = True, tx = True)
			assetSubType = mc.textField('%s_AssetSubTypeTX' % self.win, q = True, tx = True)
			assetName = mc.textField('%s_AssetNameTX' % self.win, q = True, tx = True)
			mc.setAttr('Rig_Grp.assetType', assetType, type = 'string')
			mc.setAttr('Rig_Grp.assetSubType', assetSubType, type = 'string')
			mc.setAttr('Rig_Grp.assetName', assetName, type = 'string')
			
				
	def browseLv1(self) : 
		project = self.path
		files = mc.getFileList(folder = project)
		mc.textScrollList('%s_lv1' % self.win, e = True, ra = True)
		
		for each in files : 
			selLv1 = mc.textScrollList('%s_lv1' % self.win, e = True, append = each)
		
		
		
	def browseLv2(self) : 
		project = self.path
		selLv1 = mc.textScrollList('%s_lv1' % self.win, q = True, si = True)
		
		files = mc.getFileList(folder = (project + selLv1[0] + '/'))
		
		mc.textScrollList('%s_lv2' % self.win, e = True, ra = True)
		
		for each in files : 
			mc.textScrollList('%s_lv2' % self.win, e = True, append = each)
			
		mc.textField('%s_lv1TX' % self.win, e = True, tx = selLv1[0])
		
		
		
	def browseLv3(self) : 
		project = self.path
		selLv1 = mc.textScrollList('%s_lv1' % self.win, q = True, si = True)
		selLv2 = mc.textScrollList('%s_lv2' % self.win, q = True, si = True)
		
		files = mc.getFileList(folder = (project + selLv1[0] + '/' + selLv2[0] + '/'))
		
		mc.textScrollList('%s_lv3' % self.win, e = True, ra = True)
		
		for each in files : 
			selLv1 = mc.textScrollList('%s_lv3' % self.win, e = True, append = each)
			
		mc.textField('%s_lv2TX' % self.win, e = True, tx = selLv2[0])
		
		
	def setText(self) : 
		project = self.path
		selLv1 = mc.textScrollList('%s_lv1' % self.win, q = True, si = True)
		selLv2 = mc.textScrollList('%s_lv2' % self.win, q = True, si = True)
		selLv3 = mc.textScrollList('%s_lv3' % self.win, q = True, si = True)		
		
		mc.textField('%s_lv3TX' % self.win, e = True, tx = selLv3[0])
		self.setExtraAttr()
		
		path = '%s%s/%s/%s/dev/maya/' % (project, selLv1[0], selLv2[0], selLv3[0])
		
		if mc.checkBox('%s_OverrideOP_CB' % self.win, q = True, v = True) : 
			mc.textField('%s_directoryTX' % self.win, e = True, tx = path)
			mc.textField('%s_TX' % self.win, e = True, tx = '%s_Rig.ma' % selLv3[0])
			
		if mc.checkBox('%s_AS_CB' % self.win, q = True, v = True) : 
			mc.textField('%s_TX' % self.win, e = True, tx = '%s_Rig.ma' % selLv3[0])
		
		
	def setExtraAttr(self) : 
		assetType = mc.textField('%s_lv1TX' % self.win, q = True, tx = True)
		subAssetType = mc.textField('%s_lv2TX' % self.win, q = True, tx = True)
		assetName = mc.textField('%s_lv3TX' % self.win, q = True, tx = True)
		
		if mc.objExists('Rig_Grp') : 
			mc.setAttr('Rig_Grp.assetType', assetType, type = 'string')
			mc.setAttr('Rig_Grp.assetSubType', subAssetType, type = 'string')
			mc.setAttr('Rig_Grp.assetName', assetName, type = 'string')
			mc.setAttr('Rig_Grp.project', 'Friends', type = 'string')

run()
