import maya.cmds as mc 
from functools import partial
import sys
import os
sys.path.append('Y:\USERS\Ta\scripts')
import genScript 


def run() : 
	a = exportProp()
	a.show()

#center prop
class exportProp() : 

	def __init__(self) : 
		self.win = 'exportPropUI'
		if sys.platform == 'win32' :
			self.path = 'P:/Lego_Friends/asset/3D/'
		if sys.platform == 'linux2' : 
			self.path = '/dsPipe/Lego_Friends/asset/3D/'
	
	def show(self) : 
		if(mc.window(self.win, exists = True)) : 
			mc.deleteUI(self.win)
			
		mc.window(self.win, t = 'export assets v2.2', wh = [300,200])
		mc.columnLayout(adj = 1, rs = 4)
		mc.textField('%s_pathTX' % self.win)
		
		mc.frameLayout(lv = 0, borderStyle = 'etchedIn')
		mc.rowColumnLayout(nc = 3, cw = [(1, 100), (2, 100), (3, 100)], cs = [(2, 4), (3, 4)])
		mc.textScrollList('%s_lv1' % self.win, sc = partial(self.browseLv2))
		mc.textScrollList('%s_lv2' % self.win, sc = partial(self.browseLv3))
		mc.textScrollList('%s_lv3' % self.win, sc = partial(self.setText))
		mc.textField('%s_lv1TX' % self.win)
		mc.textField('%s_lv2TX' % self.win)
		mc.textField('%s_lv3TX' % self.win)

		mc.setParent('..')
		mc.setParent('..')
		mc.checkBox('%s_MOCB' % self.win, l = 'Maintain Offset')
		mc.button(l = 'Export', h = 30, c = partial(self.exportCmd))
		mc.showWindow()
		mc.window(self.win, e = True, wh = [300, 300])
		self.browseLV1()
		
		
	def exportCmd(self, arg = None) : 
		sel = mc.ls(sl = True)
		output = mc.textField('%s_pathTX' % self.win, q = True, tx = True)
		exists = 0
		if not mc.objExists('Rig_Grp') : 
			genScript.importRigGrp()
			
		else : 
			mc.select('Rig_Grp', hi = True)
			mc.lockNode(l = False)
			mc.rename('Rig_Grp', 'Rig_Grp2')
			mc.rename('Root_Ctrl', 'Root_Ctrl2')
			mc.rename('SuperRoot_Ctrl', 'SuperRoot_Ctrl2')
			mc.rename('Proxy_Geo_Grp', 'Proxy_Geo_Grp2')
			genScript.importRigGrp()
			exists = 1
			
		loc1 = mc.spaceLocator(p = [0, 0, 0])
		loc2 = mc.spaceLocator(p = [0, 0, 0])
		mc.delete(mc.parentConstraint(sel[0], loc2[0]))
		mc.delete(mc.parentConstraint(loc1[0], sel[0]))
		
		if mc.objExists('Proxy_Geo_Grp') : 
			parent = 'Proxy_Geo_Grp'
			mc.parent(sel[0], parent)
			
		if mc.checkBox('%s_MOCB' % self.win, q = True, v = True) : 
			mc.delete(mc.parentConstraint(loc2[0], 'Rig_Grp'))
			
		self.setExtraAttr()
			
		mc.select('Rig_Grp')
		mc.file(output, force = True, options = 'v=0', typ = 'mayaAscii', pr = True, es = True)
		
		mc.select('Rig_Grp', hi = True)
		mc.lockNode(l = False)
		mc.delete('Rig_Grp')
		mc.delete(loc1[0])
		mc.delete(loc2[0])
		
		if exists == 1 : 
			mc.rename('Rig_Grp2', 'Rig_Grp')
			mc.rename('Root_Ctrl2', 'Root_Ctrl')
			mc.rename('SuperRoot_Ctrl2', 'SuperRoot_Ctrl')
			mc.rename('Proxy_Geo_Grp2', 'Proxy_Geo_Grp')
			

		
		
	def browseLV1(self) : 
		browse = '%s' % (self.path)
		files = mc.getFileList(folder = browse)
		mc.textScrollList('%s_lv1' % self.win, e = True, ra = True)
		for each in files : 
			mc.textScrollList('%s_lv1' % self.win, e = True, append = each)
			
		
	def browseLv2(self) : 
		sel1 = mc.textScrollList('%s_lv1' % self.win, q = True, si = True)
		browse = '%s%s/' % (self.path, sel1[0])
		files = mc.getFileList(folder = browse)
		mc.textScrollList('%s_lv2' % self.win, e = True, ra = True)
		for each in files : 
			mc.textScrollList('%s_lv2' % self.win, e = True, append = each)
			
	def browseLv3(self) : 
		sel1 = mc.textScrollList('%s_lv1' % self.win, q = True, si = True)
		sel2 = mc.textScrollList('%s_lv2' % self.win, q = True, si = True)
		browse = '%s%s/%s/' % (self.path, sel1[0], sel2[0])
		files = mc.getFileList(folder = browse)
		mc.textScrollList('%s_lv3' % self.win, e = True, ra = True)
		for each in files : 
			mc.textScrollList('%s_lv3' % self.win, e = True, append = each)
			
	def setText(self) : 
		sel1 = mc.textScrollList('%s_lv1' % self.win, q = True, si = True)
		sel2 = mc.textScrollList('%s_lv2' % self.win, q = True, si = True)
		sel3 = mc.textScrollList('%s_lv3' % self.win, q = True, si = True)
		mc.textField('%s_lv1TX' % self.win, e = True, tx = sel1[0])
		mc.textField('%s_lv2TX' % self.win, e = True, tx = sel2[0])
		mc.textField('%s_lv3TX' % self.win, e = True, tx = sel3[0])
		path = '%s%s/%s/%s/' % (self.path, sel1[0], sel2[0], sel3[0])
		file = '%s_Rig.ma' % sel3[0]
		mc.textField('%s_pathTX' % self.win, e = True, tx = '%sdev/maya/%s' % (path, file))

	
	def setExtraAttr(self) : 
		assetType = mc.textField('%s_lv1TX' % self.win, q = True, tx = True)
		subAssetType = mc.textField('%s_lv2TX' % self.win, q = True, tx = True)
		assetName = mc.textField('%s_lv3TX' % self.win, q = True, tx = True)
		
		if mc.objExists('Rig_Grp') : 
			mc.setAttr('Rig_Grp.assetType', assetType, type = 'string')
			mc.setAttr('Rig_Grp.assetSubType', subAssetType, type = 'string')
			mc.setAttr('Rig_Grp.assetName', assetName, type = 'string')
			mc.setAttr('Rig_Grp.project', 'Friends', type = 'string')

run()
