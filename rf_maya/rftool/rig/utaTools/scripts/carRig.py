import maya.cmds as mc
from functools import partial

def run() : 
	ui = carRig()
	ui.show()
	
class carRig :
	def __init__(self) : 
		self.source = 'D:/Ken_Pipeline/core/rf_maya/rftool/rig/utaTools/scripts/car_template.ma'
		self.carGrp = 'carGeo_Grp'
		self.allGrp = 'allCtrl_Grp'
		
	def show(self) : 
		if mc.window('carRigWin', exists = True) : 
			mc.deleteUI('carRigWin')
		mc.window('carRigWin', t = 'Car Rig')
		mc.columnLayout(adj = 1, rs = 4)
		
		mc.text(l = 'Front Left')
		mc.textField('FLTX')
		mc.text(l = 'Front Right')
		mc.textField('FRTX')
		mc.text(l = 'Rear Left')
		mc.textField('RLTX')
		mc.text(l = 'Rear Right')
		mc.textField('RRTX')
		mc.text(l = 'Chassis Grp')
		mc.textField('ChassisTX')

		mc.text(l = 'Combind Wheel')
		mc.frameLayout(borderStyle = 'etchedIn', lv = False) 
		mc.rowColumnLayout(nc = 2, cs = [(2, 0)], cw = [(1, 100), (2, 100)])
		mc.button(l = 'FL', h = 30, c = partial(self.combndWheel, 'FL'))
		mc.button(l = 'FR', h = 30, c = partial(self.combndWheel, 'FR'))
		mc.button(l = 'RL', h = 30, c = partial(self.combndWheel, 'RL'))
		mc.button(l = 'RR', h = 30, c = partial(self.combndWheel, 'RR'))
		mc.setParent('..')
		mc.button(l = 'Chassis Group', h = 30, c = partial(self.groupChassis))
		mc.setParent('..')

		mc.button(l = 'Import Template', h = 30, c = partial(self.importTemplate))
		mc.button(l = 'Set Position', h = 30, c = partial(self.setPosition))
		mc.button(l = 'Constraint', h = 30, c = partial(self.rigCmd))
		
		mc.showWindow()
		mc.window('carRigWin', e = True, wh = [200, 431])
		
	def rigCmd(self, arg = None) : 
		FL = mc.textField('FLTX', q = True, tx = True)
		FR = mc.textField('FRTX', q = True, tx = True)
		RL = mc.textField('RLTX', q = True, tx = True)
		RR = mc.textField('RRTX', q = True, tx = True)
		chassis = mc.textField('ChassisTX', q = True, tx = True)
		
		FL_jnt = 'L_F_wheel_jnt'
		FR_jnt = 'R_F_wheel_jnt'
		RL_jnt = 'L_R_wheel_jnt'
		RR_jnt = 'R_R_wheel_jnt'
		chassis_jnt = 'chassis_jnt'
		
		mc.delete(mc.pointConstraint(FL, FL_jnt))
		mc.delete(mc.pointConstraint(FR, FR_jnt))
		mc.delete(mc.pointConstraint(RL, RL_jnt))
		mc.delete(mc.pointConstraint(RR, RR_jnt))
		
		mc.parentConstraint(FL_jnt, FL, mo = True)
		mc.parentConstraint(FR_jnt, FR, mo = True)
		mc.parentConstraint(RL_jnt, RL, mo = True)
		mc.parentConstraint(RR_jnt, RR, mo = True)
		mc.parentConstraint(chassis_jnt, chassis, mo = True)

	def importTemplate(self, arg = None) : 
		mc.file(self.source, i = True, type = 'mayaAscii', options = 'v=0', pr = True, loadReferenceDepth = 'all')	
		
	def setPosition(self, arg = None) : 
		mc.delete('delete1', 'delete2', 'delete3', 'delete4', 'delete5', 'delete6', 'delete7')
		mc.delete('front_wheelShaft_move', 'chassis_pivot_move', 'rear_wheelShaft_move')
		
	def combndWheel(self, wheel, arg = None) : 
		selObj = mc.ls(sl = True)
		newName = mc.polyUnite(selObj)[0]
		newName = mc.rename(newName, '%s_wheel_geo' % wheel)
		mc.delete(newName, ch = True)
		mc.CenterPivot()
		newGrp = mc.group(newName, n = '%s_wheelGeo_Grp' % wheel)
		
		if not mc.objExists(self.carGrp) : 
			mc.group(n = self.carGrp, em = True)
					
		mc.parent(newGrp, self.carGrp)
		
		if wheel == 'FL' : 
			mc.textField('FLTX', e = True, tx = newGrp)
			
		if wheel == 'FR' : 
			mc.textField('FRTX', e = True, tx = newGrp)
			
		if wheel == 'RL' : 
			mc.textField('RLTX', e = True, tx = newGrp)
			
		if wheel == 'RR' : 
			mc.textField('RRTX', e = True, tx = newGrp)
			
			
		

	def groupChassis(self, arg = None) : 
		allSel = mc.ls(sl = True)
		chassisGrp = mc.group(allSel, n = '%s_Grp' % 'chassis')
		
		if not mc.objExists(self.carGrp) : 
			mc.group(n = self.carGrp, em = True)

		mc.parent(chassisGrp, self.carGrp)

		mc.textField('ChassisTX', e = True, tx = chassisGrp)
