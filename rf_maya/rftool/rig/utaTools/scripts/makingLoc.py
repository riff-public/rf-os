import maya.cmds as mc 
from functools import partial
import sys
import os
import genScript 
#sys.path.append('P:\BKK\Animators_Playground\Ta\scripts')

def run() : 
	a = makingLoc()
	a.show()

#center prop
class makingLoc() : 

	def __init__(self) : 
		self.win = 'makingLocUI'
		if sys.platform == 'win32' :
			self.path = 'P:/Lego_Friends/asset/3D/'
		if sys.platform == 'linux2' : 
			self.path = '/dsPipe/Lego_Friends/asset/3D/'
	
	def show(self) : 
		if(mc.window(self.win, exists = True)) : 
			mc.deleteUI(self.win)
			
		mc.window(self.win, t = 'export assets v2.2', wh = [300,200])
		mc.columnLayout(adj = 1, rs = 4)
		mc.text(l = 'Making Loc')
		mc.textField('%s_nameTX' % self.win)
		mc.button(l = 'Get Name', c = partial(self.setName))
		mc.button(l = 'Make Locator', c = partial(self.createLoc), h = 30)
		mc.showWindow()
		mc.window(self.win, edit = True, wh = [200, 100])
		
		
	def createLoc(self, arg = None) : 
		ls = mc.ls(sl = True) 
		name = mc.textField('%s_nameTX' % self.win, q = True, tx = True)
		for i in range(len(ls)) : 
			loc = mc.spaceLocator(p = (0, 0, 0), n = '%s_%d_loc' % (name, (i + 1)))
			mc.delete(mc.parentConstraint(ls[i], loc[0]))
			
			
	def setName(self, arg = None) : 
		ls = mc.ls(sl = True) 
		if len(ls) > 0 : 
			name = mc.textField('%s_nameTX' % self.win, e = True, tx = ls[0])
