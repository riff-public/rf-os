import maya.cmds as mc
from functools import partial
import sys
sys.path.append('Y:/USERS/Ta/scripts/python')
from sg import utils as ut
reload(ut)

class testUI(object) : 
	def __init__(self) : 
		self.win = 'test'
		self.ui = '%s_ui' % self.win
		self.show()
	def show(self) : 
		if mc.window(self.ui, exists = True) : 
			mc.deleteUI(self.ui)
			
		mc.window(self.ui, t = 'Create Assets in shotgun', wh = [300, 200])
		mc.columnLayout(adj = 1) 
		mc.rowColumnLayout(nc = 3, cw = [(1, 100), (2, 200), (3, 100)])
		mc.text(l = 'Project')
		mc.optionMenu('%s_project' % self.win)
		projs = self.allProject()
		for proj in projs : 
			mc.menuItem(l = proj)
		mc.text(l = '-')
		
		mc.text(l = 'Asset Type')
		mc.optionMenu('%s_type' % self.win, cc = partial(self.fillUI))
		types = ut.sgGetList('sg_asset_type')
		mc.menuItem(l = 'New')
		for type in types : 
			mc.menuItem(l = type)
		mc.textField('%s_typeTX' % self.win)
		
		mc.text(l = 'Asset SubType')
		mc.optionMenu('%s_subType' % self.win, cc = partial(self.fillUI))
		subtypes = ut.sgGetList('sg_subtype')
		mc.menuItem(l = 'New')
		for subtype in subtypes : 
			mc.menuItem(l = subtype)
		mc.textField('%s_subtypeTX' % self.win)
		
		mc.text(l = 'Task Template')
		mc.optionMenu('%s_template' % self.win)		
		tasks = self.allTaskTemplate()
		for task in tasks : 
			mc.menuItem(l = task)
		mc.text(l = '-')
		
		mc.text(l = 'Asset Name')
		mc.textField('%s_assetTX' % self.win)
		
		mc.setParent('..')
		
		mc.textScrollList('%s_assetTSL' % self.win, numberOfRows = 8)
		
		mc.button(l = 'Update', c = partial(self.fillUI))
		mc.button(l = 'Create', h = 30, c = partial(self.createAsset))
		mc.button(l = 'Remove Asset', h = 30, c = partial(self.removeAsset))
		
		mc.showWindow()
		mc.window(self.ui, e = True, wh = [430, 360])
		self.fillUI()
		
	def fillUI(self, arg = None) : 
		self.setListUI()
		
	def allTaskTemplate(self) : 
		taskTemplate = ut.sgGetAllTaskTemplate()
		codes = []
		ids = []
		
		for each in taskTemplate : 
			codes.append(each['code'])
			ids.append(each['id'])
			
		return codes
		
	def allProject(self) : 
		projs = []
		allProjs = ut.sgGetAllProject()
		for each in allProjs : 
			projs.append(each['name'])
			
		return projs
		
	def createAsset(self, arg = None) : 
		project = mc.optionMenu('%s_project' % self.win, q = True, v = True)
		type = mc.optionMenu('%s_type' % self.win, q = True, v = True)
		subType = mc.optionMenu('%s_subType' % self.win, q = True, v = True)
		template = mc.optionMenu('%s_template' % self.win, q = True, v = True)
		assetName = mc.textField('%s_assetTX' % self.win, q = True, tx = True)
		
		if type == 'New' : 
			type = mc.textField('%s_typeTX' % self.win, q = True, tx = True)
			ut.sgAddToList(type, 'sg_asset_type')
			
		if subType == 'New' : 
			subType = mc.textField('%s_subtypeTX' % self.win, q = True, tx = True)
			ut.sgAddToList(subType, 'sg_subtype')			
		
		ut.sgCreateAsset(project, template, type, subType, assetName)
		self.fillUI()
		
	def removeAsset(self, arg = None) : 
		project = mc.optionMenu('%s_project' % self.win, q = True, v = True)
		type = mc.optionMenu('%s_type' % self.win, q = True, v = True)
		sybType = mc.optionMenu('%s_subType' % self.win, q = True, v = True)
		template = mc.optionMenu('%s_template' % self.win, q = True, v = True)
		assetName = mc.textField('%s_assetTX' % self.win, q = True, tx = True)
		
		ut.sgRemoveAsset(project, type, sybType, assetName)
		
	def listAsset(self, arg = None) : 
		project = mc.optionMenu('%s_project' % self.win, q = True, v = True)
		type = mc.optionMenu('%s_type' % self.win, q = True, v = True)
		subType = mc.optionMenu('%s_subType' % self.win, q = True, v = True)
		assets = ut.sgListAsset(project, type, subType)
		
		return assets
		
	def setListUI(self) : 
		assets = []
		allAssets = self.listAsset()
		
		mc.textScrollList('%s_assetTSL' % self.win, e = True, ra = True)
		for each in allAssets :  
			mc.textScrollList('%s_assetTSL' % self.win, e = True, append = each['code'])
			
		
			
		


a = testUI()
a.fillUI()
#a.listAsset()
#assets = ut.sgListAsset('Lego Template', 'char', 'lion')