from shotgun_api3 import Shotgun
# connection to server
server = 'https://pts.shotgunstudio.com'
script = 'testScript'
id = '0935c4bd1693051e98a599cff9206aae72c7d4af'
#projName = 'Lego City'
sg = Shotgun(server, script, id)

def sgGetProjectInfo(projName) : 
	proj = sg.find_one("Project", [["name", "is", projName]])
	return proj

def sgGetAllTaskTemplate() : 
	fields = ['id', 'code']
	filters = []
	tasks = sg.find("TaskTemplate",filters,fields)
	# return format {'code': 'Shot Template', 'type': 'TaskTemplate', 'id': 21}
	return tasks

def sgGetAllProject() : 
	fields = ['name']
	filters = []
	projs = sg.find("Project",filters,fields)
	# return format {'type': 'Project', 'name': 'Demo Project', 'id': 4}
	return projs

def sgGetAssetID(projName, assetName, assetType, assetSubType) : 
	sg = Shotgun(server, script, id)
	filters = [['project.Project.name', 'is', str(projName)], ['code', 'is', str(assetName)], ['sg_asset_type', 'is', str(assetType)], ['sg_subtype', 'is', str(assetSubType)]]
	assetID = sg.find('Asset', filters = filters, fields = ['id'])
	return assetID

def sgGetAssetCode(projName, assetID) : 
	sg = Shotgun(server, script, id)
	filters = [['project.Project.name', 'is', str(projName)], ['id', 'is', assetID]]
	assetCode = sg.find('Asset', filters = filters, fields = ['code'])
	return assetCode

def sgGetEpisodeID(projName, assetName, assetType, assetSubType) : 
	sg = Shotgun(server, script, id)
	filters = [['project.Project.name', 'is', str(projName)], ['code', 'is', str(assetName)], ['sg_asset_type', 'is', str(assetType)], ['sg_subtype', 'is', str(assetSubType)]]
	assetID = sg.find('Asset', filters = filters, fields = ['id'])
	return assetID

def sgGetList(type) : 
	# name to add to the list
	#type = 'sg_subtype' for supType
	#type = 'sg_asset_type' for Type

	values = sg.schema_field_read(str('Asset'), str(type)) [str(type)]["properties"]["valid_values"]["value"]
	return values

def sgAddToList(name, type) : 
	# name to add to the list
	#type = 'sg_subtype' for supType
	#type = 'sg_asset_type' for Type

	values = sg.schema_field_read(str('Asset'), str(type)) [str(type)]["properties"]["valid_values"]["value"]
	if not name in values : 
		values.append(name)
		properties = {'valid_values' : values}
		sg.schema_field_update(str('Asset'), str(type), properties)
		print 'Add %s to %s' % (name, type)
		return True

def sgRemoveFromList(name, type) : 
	# name to add to the list
	#type = 'sg_subtype' for supType
	#type = 'sg_asset_type' for Type

	values = sg.schema_field_read(str('Asset'), str(type)) [str(type)]["properties"]["valid_values"]["value"]
	if name in values : 
		values.remove(name)
		properties = {'valid_values' : values}
		sg.schema_field_update(str('Asset'), str(type), properties)
		print 'Remove %s from %s' % (name, type)
		return True

def sgListSeason(projName) : 
	fields = ['id', 'code']
	filters = [['project.Project.name', 'is', str(projName)]]
	assetID = sg.find('Scene', filters = filters, fields = fields)

def sgListEpisode(projName, seasonName) :  
	fields = ['id', 'code']
	filters = [ ['project.Project.name', 'is', str(projName)]
				['project.Project.name', 'is', str(projName)]
			  ]
	assetID = sg.find('Scene', filters = filters, fields = fields)

def sgCreateAsset(projName, template, assetType, assetSubType, assetName) :
	proj = sg.find_one("Project", [["name", "is", str(projName)]])
	fields = ['id', 'code']
	filters = [ ['code','is', template ] ]
	
	if template : 
		template = sg.find_one('TaskTemplate', filters, fields)
		data = { 'project': proj,
	         	'code': assetName,
	         	'task_template': template }
	         	
	else : 
		data = { 'project': proj,
	         	'code': assetName }
	         	
	asset = sg.create('Asset', data)
	
	sg.update('Asset',asset['id'], {'sg_asset_type':str(assetType)})
	sg.update('Asset',asset['id'], {'sg_subtype':str(assetSubType)})
	
	return asset['id']

def sgRemoveAsset(projName, assetType, assetSubType, assetName) :
	proj = sg.find_one("Project", [["name", "is", str(projName)]])
	filters = [
    ['project','is',proj],
    ['sg_asset_type','is',assetType], 
    ['sg_subtype', 'is', assetSubType],
    ['code', 'is', assetName]
    ]
	asset= sg.find_one("Asset",filters,fields = [])
	id = asset['id']
	sg.delete('Asset', id) 

	return True

def sgListAsset(projName, assetType, assetSubType) : 
	proj = sg.find_one("Project", [["name", "is", str(projName)]])
	filters = [
	['project','is',proj],
	['sg_asset_type','is',assetType], 
	['sg_subtype', 'is', assetSubType]
	]

	asset= sg.find("Asset",filters,fields = ['code', 'id'])
	return asset

def sgUploadAssetIcon(projName, assetName, assetType, assetSubType, imgPath) : 
	sg = Shotgun(server, script, id)
	filters = [['project.Project.name', 'is', str(projName)], ['code', 'is', str(assetName)], ['sg_asset_type', 'is', str(assetType)], ['sg_subtype', 'is', str(assetSubType)]]
	assetID = sg.find('Asset', filters = filters, fields = ['id'])
	i = dict(assetID[0])['id']
	sg.upload_thumbnail('Asset', i, imgPath)

def sgGetStepID(pipeline = '') : 
	id = int()
	steps = sg.find('Step',filters = [],fields = ['id', 'code', 'content'])
	for eachSteps in steps :
		if eachSteps['code'] == pipeline : 
			id = eachSteps['id']

	return id

def sgGetAllAssetTask(projName, assetName, assetType, assetSubType, pipeline) :
	# example return value
	# Result: [{'content': 'HD model', 'type': 'Task', 'id': 12234}, {'content': 'LD model', 'type': 'Task', 'id': 12235}]

	proj = sgGetProjectInfo(projName)
	assetID = sgGetAssetID(projName, assetName, assetType, assetSubType)
	stepID = sgGetStepID(pipeline)

	fields = ['id', 'code', 'content']
	filters = [
    ['project','is', proj],
    ['entity', 'is', assetID[0]], 
    ['step', 'is', {'type':'Step', 'id':stepID}]
    ]
	tasks = sg.find('Task',filters, fields)

	return tasks

def sgGetAssetTaskID(projName, assetName, assetType, assetSubType, pipeline, taskName) : 
	
	tasks = sgGetAllAssetTask(projName, assetName, assetType, assetSubType, pipeline)
	id = int()
	for eachTask in tasks : 
		if eachTask['content'] == taskName : 
			id = eachTask['id']

	return id