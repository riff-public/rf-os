#Import python modules
import sys, os, re, shutil, random, sip
import subprocess
import getpass
from PyQt4 import QtCore, QtGui
sys.path.append('Y:/USERS/Ta/scripts/python')
#Import GUI
from PyQt4.QtCore import *
from PyQt4.QtGui import *
#import maya.OpenMayaUI as mui

#Custom import modules
from myUI import ui
reload(ui)

from functools import partial



# If inside Maya open Maya GUI
def getMayaWindow():
    ptr = mui.MQtUtil.mainWindow()
    return sip.wrapinstance(long(ptr), QObject)

if 'cmds' in dir() :
    import maya.OpenMayaUI as mui
    getMayaWindow()

class MyForm(QtGui.QMainWindow):

    def __init__(self, parent=None):
        self.count = 0
        #Setup Window
        QtGui.QWidget.__init__(self, parent)
        self.ui = ui.Ui_MainWindow()
        self.ui.setupUi(self)
        self.comboBoxInfo()
        self.connect( self.ui.pushButton , SIGNAL( 'clicked()' ) , self.clicked )
        self.connect( self.ui.optionComboBox, SIGNAL(  'currentIndexChanged(int)' ), self.test)
        
    def clicked(self) :
        #self.ui.pushButton.setText( '%d'%self.count)
        #self.count += 1
        self.ui.displayListWidget.clear()

    def comboBoxInfo(self) : 
        a = ['a', 'b', 'c', 'd']
        for i in a :
            self.ui.optionComboBox.addItem(i)
    
    def test( self ) :
        sender = self.sender()
        a = str(self.ui.optionComboBox.currentText())
        self.ui.displayListWidget.addItem(a)

# #IF not runned inside Maya
# if __name__ == "__main__":
#     app = QtGui.QApplication(sys.argv)
#     #myapp = MyForm(getMayaWindow())
#     myapp = MyForm()
#     myapp.show()
#     app.exec_()

if 'cmds' in dir() :
    #myapp = MyForm(getMayaWindow())
    myapp = MyForm(getMayaWindow())
    myapp.show()
else :
    app = QtGui.QApplication(sys.argv)
    #myapp = MyForm(getMayaWindow())
    myapp = MyForm()
    myapp.show()
    app.exec_()