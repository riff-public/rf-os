#Import python modules
import sys, os, re, shutil, random, sip
import subprocess
import getpass
from PyQt4 import QtCore, QtGui
sys.path.append('Y:/USERS/Ta/scripts/python/myUI')
#Import GUI
from PyQt4.QtCore import *
from PyQt4.QtGui import *

#Custom import modules
from ui import Ui_MainWindow


#If inside Maya open Maya GUI
def getMayaWindow():
    ptr = mui.MQtUtil.mainWindow()
    return sip.wrapinstance(long(ptr), QObject)

class MyForm(QtGui.QMainWindow):
    def __init__(self, parent=None):
        self.count = 0
        #Setup Window
        QtGui.QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.connect( self.ui.pushButton , SIGNAL( 'clicked()' ) , self.clicked )

    def clicked( self ) :
        self.ui.pushButton.setText( '%d'%self.count)
        self.count += 1


#IF not runned inside Maya
if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    myapp = MyForm()
    myapp.show()
    sys.exit(app.exec_())