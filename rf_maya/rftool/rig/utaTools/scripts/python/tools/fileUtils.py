# read write file
import os
import shutil

def createFile(file) : 
	f = open(file, 'w')
	f.close()
	return True

def writeFile(file, data) : 
	f = open(file, 'w')
	f.write(data)
	f.close()
	return True

def appendData(file, data) :
	f = open(file, 'a')
	f.write(data)
	f.close()
	return True

def readFile(file) : 
	f = open(file, 'r')
	data = f.read()
	return data

def eraseData(file) :
	f = open(file, 'w')
	f.close()
	return True

def copy(src, dst) : 
	if os.path.exists(src) : 
		dstDir = dst.replace(dst.split('/')[-1], '')
		if not os.path.exists(dstDir) :
			os.makedirs(dstDir)

		shutil.copy2(src, dst)
	return dst

# copy recursively every directory
#src = 'P:/BKK/systemTool/python/ptFileManager'
#dst = 'P:/BKK/systemTool/ptFileManager'
#copyTree(src, dst)

def copyTree(src, dst) : 
	if os.path.exists(src) :
		shutil.copytree(src, dst, symlinks=False, ignore=None)
	return dst