#Import python modules
import sys, os, re, shutil, random, sip
import subprocess
import getpass
from PyQt4 import QtCore, QtGui
import maya.app.general.createImageFormats as createImageFormats

#Import GUI
from PyQt4.QtCore import *
from PyQt4.QtGui import *

#Custom import modules
from dsAssetOpenUI import Ui_AssetWindow
import dsCommon.dsOsUtil as dsOsUtil
reload(dsOsUtil)
import dsCommon.dsProjectUtil as dsProjectUtil
reload(dsProjectUtil)
#import dsSgUtil as sgBridge
#reload(sgBridge)

if dsOsUtil.mayaRunning() == True:
    import maya.cmds as cmds
    import maya.mel as mel
    import maya.OpenMayaUI as mui

#If inside Maya open Maya GUI
def getMayaWindow():
    ptr = mui.MQtUtil.mainWindow()
    return sip.wrapinstance(long(ptr), QObject)

class MyForm(QtGui.QMainWindow):
    def __init__(self, parent=None):
        #Setup Window
        QtGui.QWidget.__init__(self, parent)
        self.ui = Ui_AssetWindow()
        self.ui.setupUi(self)

        #For action, run def
        QtCore.QObject.connect(self.ui.productionComboBox, QtCore.SIGNAL("activated(int)"), self.assetTypeUpdate);
        QtCore.QObject.connect(self.ui.productionComboBox, QtCore.SIGNAL("activated(int)"), self.minifigProduction);
        QtCore.QObject.connect(self.ui.assetTypeComboBox, QtCore.SIGNAL("activated(int)"), self.updateAssetSubTypeComboBox);
        QtCore.QObject.connect(self.ui.assetSubTypeComboBox, QtCore.SIGNAL("activated(int)"), self.assetListUpdate);
        QtCore.QObject.connect(self.ui.assetListWidget, SIGNAL("itemDoubleClicked(QListWidgetItem *)"), self.assetListView);
        QtCore.QObject.connect(self.ui.iconButtonGroup, QtCore.SIGNAL("buttonClicked(int)"), self.iconUpdate);
        QtCore.QObject.connect(self.ui.iconCheckBox, QtCore.SIGNAL("stateChanged(int)"), self.iconEnable);
        QtCore.QObject.connect(self.ui.createPushButton, QtCore.SIGNAL("clicked()"), self.createAsset);
        QtCore.QObject.connect(self.ui.assetNameTextEdit, QtCore.SIGNAL("returnPressed()"), self.createAsset);
        QtCore.QObject.connect(self.ui.createMinifigPushButton, QtCore.SIGNAL("clicked()"), self.createMinifigAsset);
        QtCore.QObject.connect(self.ui.removePushButton, QtCore.SIGNAL("clicked()"), self.removeAsset);
        QtCore.QObject.connect(self.ui.createIcon, QtCore.SIGNAL("clicked()"), self.createAssetIcon);
        QtCore.QObject.connect(self.ui.exportPart, QtCore.SIGNAL("clicked()"), self.exportPart);

        QtCore.QObject.connect(self.ui.incrPushButton, QtCore.SIGNAL("clicked()"), self.incrBackup);
        QtCore.QObject.connect(self.ui.actionAbout, QtCore.SIGNAL("activated(int)"), self.aboutMenu);

        QtCore.QObject.connect(self.ui.actionExit, QtCore.SIGNAL("activated(int)"), self.closeEvent);
        QtCore.QObject.connect(self.ui.actionExit, QtCore.SIGNAL("activated(int)"), self.exitWidget)

        #Init Widgets
        self.updateProjectComboBox()

        #Set Context Menu for list widget
        self.ui.assetListWidget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu);
        self.ui.assetListWidget.customContextMenuRequested.connect(self.openMenu);

        #Gui Settings
        self.settings = QtCore.QSettings("Duckling&Sonne", "dsAssetOpen")
        self.iconCheckBoxState = True
        self.iconSizeState = "small"
        self.productionState = ""
        self.assetTypeState = ""
        self.assetSubTypeState = ""
	try:
	  self.readSettings()
	except Exception as e:
	  print 'Read Settings fucked up! \nERROR:%s' % (e)
        self.asset = ""

        self.minifigProduction()

    def exportPart(self):
        cmds.delete(ch=True)
        project = self.ui.productionComboBox.currentText()
        assetType = self.ui.assetTypeComboBox.currentText()
        assetSubType = self.ui.assetSubTypeComboBox.currentText()
        assetName = self.ui.assetListWidget.currentItem().text()

        fileName = "%s%s_Model" % (dsProjectUtil.listAssetDevPath(project, assetType, assetSubType, assetName), assetName)
        print fileName

        cmds.file(fileName, f=True, options="V=0", type="mayaAscii", es=True)

        self.createAssetIcon(True)

    def createAssetIcon(self, isolateView=False):
        project = self.ui.productionComboBox.currentText()
        assetType = self.ui.assetTypeComboBox.currentText()
        assetSubType = self.ui.assetSubTypeComboBox.currentText()
        assetName = self.ui.assetListWidget.currentItem().text()

        filename = dsProjectUtil.listAssetIcon(project, assetType, assetSubType, assetName)
        self.renderIcon()
        self.saveIcon(filename)
        self.saveIconToShotgun(project, assetName, assetType, assetSubType, filename)

        self.assetListUpdate()

    def renderIcon(self):
        cmds.setAttr("defaultRenderGlobals.animation", 1)
        cmds.setAttr("vraySettings.animBatchOnly", 1)
        width = cmds.getAttr("vraySettings.width")
        height = cmds.getAttr("vraySettings.height")
        cmds.setAttr("vraySettings.width", 640)
        cmds.setAttr("vraySettings.height", 360)
        mel.eval("renderWindowRender redoPreviousRender renderView;")

        cmds.setAttr("vraySettings.width", width)
        cmds.setAttr("vraySettings.height", height)

    def saveIcon(self, filename):
        renderPanels = cmds.getPanel(scriptType="renderWindowPanel")
        formatManager = createImageFormats.ImageFormats()
        formatManager.pushRenderGlobalsForDesc("PNG")
        cmds.renderWindowEditor(renderPanels[0], e=True, writeImage=str(filename))
        formatManager.popRenderGlobals()

    def saveIconToShotgun(self, project, assetName, assetType, assetSubType, filename):
        sgBridge.sgCreateIcon(project, assetName, assetType, assetSubType, filename)


    def minifigProduction(self):
        project = self.ui.productionComboBox.currentText()
        path = dsProjectUtil.listMinifigTemplatePath(project)[0]

        if os.path.exists(path) == True:
            self.ui.createMinifigPushButton.setEnabled(True)
        else:
            self.ui.createMinifigPushButton.setEnabled(False)

    def updateProjectComboBox(self):
        '''This Def list all project in the project dir, with a config.xml file in the root'''
        self.ui.productionComboBox.clear()
        projects = dsProjectUtil.listProjects()
        i = 0
        if projects:
            for project in projects:
                self.ui.productionComboBox.addItem("")
                self.ui.productionComboBox.setItemText(i, QtGui.QApplication.translate("MainWindow", project, None, QtGui.QApplication.UnicodeUTF8))
                i = i+1
        self.assetTypeUpdate()

    def assetTypeUpdate(self):
        self.ui.assetTypeComboBox.clear()
        path = dsProjectUtil.listAssetTypes(str(self.ui.productionComboBox.currentText()))
        i=0
        for folder in dsOsUtil.listFolder(path):
            self.ui.assetTypeComboBox.addItem("")
            self.ui.assetTypeComboBox.setItemText(i, QtGui.QApplication.translate("AssetWindow", folder, None, QtGui.QApplication.UnicodeUTF8))
            i=i+1
        self.updateAssetSubTypeComboBox()

    def updateAssetSubTypeComboBox(self):
        '''Lists all Sub Asset Types for a given Asset Type'''
        self.ui.assetSubTypeComboBox.clear()
        project = self.ui.productionComboBox.currentText()
        assetType = self.ui.assetTypeComboBox.currentText()
        path = dsProjectUtil.listSubAssets(project, assetType)

        assetSubTypes = dsOsUtil.listFolder(str(path))
        i=0
        for assetSubType in assetSubTypes:
            self.ui.assetSubTypeComboBox.addItem("")
            self.ui.assetSubTypeComboBox.setItemText(i, QtGui.QApplication.translate("MainWindow", assetSubType, None, QtGui.QApplication.UnicodeUTF8))
            i = i+1
        self.assetListUpdate()

    def assetListUpdate(self):
        self.iconUpdate()

        #Listing Assets
        project = self.ui.productionComboBox.currentText()
        assetType = self.ui.assetTypeComboBox.currentText()
        assetSubType = self.ui.assetSubTypeComboBox.currentText()

        path = str(dsProjectUtil.listAssets(project, assetType, assetSubType))
        self.ui.assetListWidget.clear()
        i = 0
        for asset in dsOsUtil.listFolder(path):
            if self.ui.iconCheckBox.isChecked() == True:
                #Add Icon
                picPath = dsProjectUtil.listAssetIcon(project, assetType, assetSubType, asset)
                if os.path.exists(picPath) == True:
                    icon = QtGui.QIcon()
                    icon.addPixmap(QtGui.QPixmap(picPath), QtGui.QIcon.Normal, QtGui.QIcon.Off)
                    item = QtGui.QListWidgetItem(self.ui.assetListWidget)
                    item.setIcon(icon)
                else:
                    #Icon dosen't exist add new row
                    self.ui.assetListWidget.addItem("")
            else:
                self.ui.assetListWidget.addItem("")

            self.ui.assetListWidget.item(i).setText(QtGui.QApplication.translate("AssetWindow", asset, None, QtGui.QApplication.UnicodeUTF8))
            i=i+1

    def assetListView(self, item):
        if ".." in item.text():
            MyForm.assetListUpdate(self)
            self.ui.dirPathLabel.clear()
        elif ".ma" in item.text():
            MyForm.openMaFiles(self, item.text())
        else:
            self.asset = item.text()
            MyForm.listMaFiles(self, item)


    def listMaFiles(self, item="", fullPath=False):
        if fullPath == False:
            project = self.ui.productionComboBox.currentText()
            assetType = self.ui.assetTypeComboBox.currentText()
            assetSubType = self.ui.assetSubTypeComboBox.currentText()
            asset = item.text()

            maFolder = dsProjectUtil.listAssetDevPath(project, assetType, assetSubType, asset)
        if fullPath == True:
            maFolder = item

        if dsOsUtil.listMa(maFolder) != "ERROR":
            self.ui.dirPathLabel.setText(QtGui.QApplication.translate("AssetWindow", maFolder, None, QtGui.QApplication.UnicodeUTF8))

            i=0

            self.ui.assetListWidget.clear()
            self.ui.assetListWidget.addItem("")
            self.ui.assetListWidget.item(i).setText(QtGui.QApplication.translate("AssetWindow", "..", None, QtGui.QApplication.UnicodeUTF8))

            for asset in dsOsUtil.listMa(maFolder):
                i=i+1
                self.ui.assetListWidget.addItem("")
                self.ui.assetListWidget.item(i).setText(QtGui.QApplication.translate("AssetWindow", asset, None, QtGui.QApplication.UnicodeUTF8))
            self.ui.dirPathLabel.setText(maFolder)

        else:
            self.ui.statusbar.showMessage("Path dosen't exist: " + maFolder)

    def iconUpdate(self):
        #Icon Size
        iconSize = [100,500]
        if self.ui.iconMediumRadioButton.isChecked() == 1:
            iconSize[0] = 150
        elif self.ui.iconBigRadioButton.isChecked() == 1:
            iconSize[0] = 200

        self.ui.assetListWidget.setIconSize(QtCore.QSize(iconSize[0], iconSize[1]))

    def iconEnable(self):
        if self.ui.iconCheckBox.isChecked() == True:
            #Enable Buttons
            self.ui.iconSmallRadioButton.setEnabled(True)
            self.ui.iconMediumRadioButton.setEnabled(True)
            self.ui.iconBigRadioButton.setEnabled(True)

            self.iconUpdate()
            self.assetListUpdate()
        else:
            #Disable Buttons
            self.ui.iconSmallRadioButton.setEnabled(False)
            self.ui.iconMediumRadioButton.setEnabled(False)
            self.ui.iconBigRadioButton.setEnabled(False)
            self.ui.assetListWidget.setIconSize(QtCore.QSize(0, 0))

    def createMinifigAsset(self):
        if self.ui.assetNameTextEdit.text() == "":
            print "No name Typed"
        else:
            project = self.ui.productionComboBox.currentText()
            assetType = self.ui.assetTypeComboBox.currentText()
            assetSubType = self.ui.assetSubTypeComboBox.currentText()

            path = dsProjectUtil.listAssets(project, assetType, assetSubType)
            assetName = self.ui.assetNameTextEdit.text()
            templateVal = dsProjectUtil.listMinifigTemplatePath(project)
            print templateVal
            assetMinifigTemplate = templateVal[0]
            assetMinifigTemplateName = templateVal[1]

            newSubAsset = path + "/" + assetName
            i=0

            if os.path.exists(newSubAsset) == True:
                self.ui.statusbar.showMessage("Asset Already Exists")
            else:
                shutil.copytree(str(assetMinifigTemplate), str(newSubAsset))
                for root, dirs, files in os.walk(str(newSubAsset)):
                    for item in files:
                        if assetMinifigTemplateName in item:
                            nameSplit = item.split("_")

                            oldPath = os.path.join(root, item)
                            newPath = os.path.join(root, item.replace(assetMinifigTemplateName, assetName))

                            if item.endswith(".ma"):
                                oldArray = [("$AssetFilePath/%s" % (assetMinifigTemplateName)), ("%s_%s" % (assetMinifigTemplateName, nameSplit[1])), "{assetName}", "{assetType}", "{assetSubType}"]
                                newArray = [("$AssetFilePath/%s" % (assetName)), ("%s_%s" % (assetName, nameSplit[1])), assetName, assetType, assetSubType]
                                dsOsUtil.readReplaceMa(oldPath, oldArray, newArray)
                            shutil.move(oldPath, newPath)
                            i=i+1

        MyForm.assetListUpdate(self)
        self.ui.statusbar.showMessage("Asset created:  " + assetName)

        self.ui.assetNameTextEdit.clear()

    def createAsset(self):
        if self.ui.assetNameTextEdit.text() == "":
            print "No name Typed"
        else:
            project = self.ui.productionComboBox.currentText()
            assetType = self.ui.assetTypeComboBox.currentText()
            assetSubType = self.ui.assetSubTypeComboBox.currentText()

            path = dsProjectUtil.listAssets(project, assetType, assetSubType)
            assetName = self.ui.assetNameTextEdit.text()
            templateVal = dsProjectUtil.listTemplatePath(project)
            assetTemplate = templateVal[0]
            assetTemplateName = templateVal[1]

            newSubAsset = path + "/" + assetName
            i=0

            if os.path.exists(newSubAsset) == True:
                self.ui.statusbar.showMessage("Asset Already Exists")
            else:
                shutil.copytree(str(assetTemplate), str(newSubAsset))
                for root, dirs, files in os.walk(str(newSubAsset)):
                    for item in files:
                        if assetTemplateName in item:
                            nameSplit = item.split("_")

                            oldPath = os.path.join(root, item)
                            newPath = os.path.join(root, item.replace(assetTemplateName, assetName))

                            if item.endswith(".ma"):
                                oldArray = [("{$assetFilePath}/%s" % (assetTemplateName)), ("%s_%s" % (assetTemplateName, nameSplit[1])), "{assetName}", "{assetType}", "{assetSubType}"]
                                newArray = [("{$assetFilePath}/%s" % (assetName)), ("%s_%s" % (assetName, nameSplit[1])), assetName, assetType, assetSubType]
                                dsOsUtil.readReplaceMa(oldPath, oldArray, newArray)
                            shutil.move(oldPath, newPath)
                            i=i+1

        MyForm.assetListUpdate(self)
        self.ui.statusbar.showMessage("Asset created:  " + assetName)

        sgBridge.sgCreateAsset(project, assetName, assetType, assetSubType)


        self.ui.assetNameTextEdit.clear()

    def removeAsset(self):
        currentSelection = self.ui.assetListWidget.currentItem()

        if currentSelection == None:
            self.ui.statusbar.showMessage("No Asset Selected")
            mel.eval('print "Please select asset"')
        else:
            #Listing Assets
            project = self.ui.productionComboBox.currentText()
            assetType = self.ui.assetTypeComboBox.currentText()
            assetSubType = self.ui.assetSubTypeComboBox.currentText()

            path = str(dsProjectUtil.listAssets(project, assetType, assetSubType) + "/" + currentSelection.text())
            MyForm.deleteConfirmDialog(self, path)

        #Remove from shotgun too....
        sgBridge.sgRemoveAsset(project, currentSelection.text(), assetType, assetSubType)

    def openMaFiles(self, item):
        path = self.ui.dirPathLabel.text()
        asset = item
        devFile = str(path + asset)

        if dsOsUtil.mayaRunning() == True:
            if cmds.file(q=True, anyModified=True) == True:
                MyForm.saveConfirmDialog(self, devFile)
            else:
                cmds.file( devFile, o=True )
                mel.eval('print "Scene opened: %s"' % devFile)
        else:
            os.system("maya.exe -file %s" % devFile)

    def deleteConfirmDialog(self, itemPath):
        posetiveStatus = "Asset Removed"
        negativeStatus = "Abort"

        reply = QtGui.QMessageBox.question(self, 'Message',
            "Want to delete this Asset?", QtGui.QMessageBox.Yes |
            QtGui.QMessageBox.No, QtGui.QMessageBox.No)

        if reply == QtGui.QMessageBox.Yes:
            if os.path.isdir(itemPath) == True:
                shutil.rmtree(str(itemPath))
                self.ui.assetListWidget.takeItem(self.ui.assetListWidget.currentRow())
                self.ui.statusbar.showMessage(posetiveStatus)
            else:
                path = self.ui.dirPathLabel.text()
                os.remove(str(itemPath))
                MyForm.listMaFiles(self, str(path), True)
        else:
            self.ui.statusbar.showMessage(negativeStatus)

    def importAsset(self, item):
        path = self.ui.dirPathLabel.text()
        asset = item
        devFile = str(path + asset)

        if dsOsUtil.mayaRunning() == True:
            cmds.file(devFile, i=True )
            mel.eval('print "File Imported: %s"' % (devFile))
        else:
            os.system("maya.exe -file %s" % (devFile))

    def refAsset(self, item):
        project = self.ui.productionComboBox.currentText()
        assetType = self.ui.assetTypeComboBox.currentText()
        assetSubType = self.ui.assetSubTypeComboBox.currentText()
        asset = self.asset
        name = asset
        assetFile = self.ui.assetListWidget.currentItem().text()

        path = dsProjectUtil.listAssetDevPath(project, assetType, assetSubType, asset)
        asset = self.ui.assetListWidget.currentItem().text()
        devFile = str(path + assetFile)

        if dsOsUtil.mayaRunning() == True:
            cmds.file(devFile, r=True, namespace=str(name), options="v=0", shd="shadingNetworks")
            mel.eval('print "File Referenced: %s"' % (devFile))
        else:
            os.system("maya.exe -file %s" % (devFile))

    def incrBackup(self):
        posetiveStatus = "Backup created"
        negativeStatus = "No file selected - please select .ma file"

        assetPath = self.ui.dirPathLabel.text()
        asset = self.ui.assetListWidget.currentItem().text()

        filePath = (assetPath + asset)
        incrementFolder = assetPath + "/Increment"
        user = getpass.getuser()

        if str(user) == "administrator":
            user = "adm"

        files=[]
        if asset != None:
            if asset.split(".")[-1] == "ma":
                if not os.path.exists(incrementFolder):
                    os.mkdir(incrementFolder)
                for item in dsOsUtil.listMa(incrementFolder):
                    if str(asset[1:-3]) in str(item):
                        files.append(item.split("_")[-2])

                if len(files) == 0:
                    incrValue = 1
                else:
                    incrValue = int(sorted(files)[-1])+1

                newPath = incrementFolder + "/" + asset.split(".")[0] + "_" + "%.3d" % incrValue + "_" + user + ".ma"

                shutil.copy(filePath, newPath)

                self.ui.statusbar.showMessage(posetiveStatus)
            else:
                self.ui.statusbar.showMessage(negativeStatus)
        else:
            self.ui.statusbar.showMessage(negativeStatus)

    def exitWidget(self):
        if dsOsUtil.mayaRunning() == True:
            global assetOpenForm
            self.ui.statusbar.showMessage('Quitting')
            mel.eval('print "Asset Open Dialog closed"')
            assetOpenForm.close()
        else:
            self.close()

    def saveConfirmDialog(self, item):
        reply = QtGui.QMessageBox.question(self, 'Message',
            "Want to to Save, before closing?", QtGui.QMessageBox.Yes |
            QtGui.QMessageBox.No| QtGui.QMessageBox.Cancel, QtGui.QMessageBox.No)

        if reply == QtGui.QMessageBox.Yes:
            cmds.file( save=True, type='mayaAscii' )
            cmds.file(item, o=True, f=True)
            self.ui.statusbar.showMessage("File saved - File opened")
            mel.eval('print "File Saved - File Opened"')
        elif reply == QtGui.QMessageBox.No:
            print item
            cmds.file(item, o=True, f=True)
            self.ui.statusbar.showMessage("File opened")
            mel.eval('print "File Opened: %s"' % item)
        else:
            self.ui.statusbar.showMessage("Open Scene Cancled")
            mel.eval('print "Cancled"')

    def openAssetExplore(self):
        path = self.ui.dirPathLabel.text()
        if path == "":
            project = self.ui.productionComboBox.currentText()
            assetType = self.ui.assetTypeComboBox.currentText()
            assetSubType = self.ui.assetSubTypeComboBox.currentText()
            path = dsProjectUtil.listSubAssets(project, assetType) + "/" + assetSubType + "/"
        dsOsUtil.openInBrowser(path)

##        currentOs = dsOsUtil.listOS()
##        platform = str(currentOs)
##
##        if platform == "Windows":
##            print os.path.exists(path)
##            if os.path.exists(path):
##                path = path.split("/")
##                path = path.join("\\")
##                subprocess.Popen("explorer %s" % (path))
##            else:
##                self.ui.statusbar.showMessage("Path dosen't exist")
##        if platform == "Linux":
##            if os.path.exists(path):
##                subprocess.Popen(["nautilus", path])
##            else:
##                self.ui.statusbar.showMessage("Path dosen't exist")

    def aboutMenu(self):
        aboutDialog = QDialog()
        aboutDialog.setObjectName("About")
        aboutDialog.resize(450, 220)

        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(aboutDialog.sizePolicy().hasHeightForWidth())
        aboutDialog.setSizePolicy(sizePolicy)
        aboutDialog.textBrowser = QtGui.QTextBrowser(aboutDialog)
        aboutDialog.textBrowser.setEnabled(True)
        aboutDialog.textBrowser.setGeometry(QtCore.QRect(10, 10, 430, 200))
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(aboutDialog.textBrowser.sizePolicy().hasHeightForWidth())
        aboutDialog.textBrowser.setSizePolicy(sizePolicy)
        aboutDialog.textBrowser.setObjectName("textBrowser")

        aboutDialog.setWindowTitle(QtGui.QApplication.translate("About", "About", None, QtGui.QApplication.UnicodeUTF8))
        aboutDialog.textBrowser.setHtml(QtGui.QApplication.translate("About", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
        "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
        "p, li { white-space: pre-wrap; }\n"
        "</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
        "<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:16pt; font-weight:600;\">Asset Open Dialog</span></p>\n"
        "<p align=\"center\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p>\n"
        "<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">Updated: Sunday, 10th July 2011</span></p>\n"
        "<p align=\"center\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p>\n"
        "<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">By Karsten Friis Nielsen</span></p>\n"
        "<p align=\"center\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p>\n"
        "<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">karsten_friis_nielsen@hotmail.com</span></p>\n"
        "<p align=\"center\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p>\n"
        "<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">Duckling&Sonne (c)</span></p></body></html>", None, QtGui.QApplication.UnicodeUTF8))

        aboutDialog.exec_()
        aboutDialog.setVisible()

    def openMenu(self, position):
        menu = QMenu()
        selectedItem = self.ui.assetListWidget.currentItem()
        incrAction=""
        removeAction=""
        openAction = ""
        importAction = ""
        refAction = ""

        if selectedItem != None:
            #If Ma file selected
            if ".ma" in selectedItem.text():
                openAction = menu.addAction("Open")
                if dsOsUtil.mayaRunning() == True:
                    refAction = menu.addAction("Ref")
                    importAction = menu.addAction("Import")
                    removeAction = menu.addAction("Remove")
                incrAction = menu.addAction("Increment Backup")

            #If Folder Selected
            else:
                project = self.ui.productionComboBox.currentText()
                assetType = self.ui.assetTypeComboBox.currentText()
                assetSubType = self.ui.assetSubTypeComboBox.currentText()
                asset = selectedItem.text()

                maFolder = dsProjectUtil.listAssetDevPath(project, assetType, assetSubType, asset)
                for item in dsOsUtil.listMa(maFolder):
                    entry = menu.addAction(item)
                    self.connect(entry,QtCore.SIGNAL('triggered()'), lambda item=item: self.openMaFiles(item=item))
                    menu.addAction(entry)
                removeAction = menu.addAction("Remove Folder")

        quitAction = menu.addAction("Quit")
        exploreAction = menu.addAction("Open in Explore")
        action = menu.exec_(self.ui.assetListWidget.mapToGlobal(position))

        #Check Action
        if action == quitAction:
            self.exitWidget()
        if action == exploreAction:
            self.openAssetExplore()
        if action == incrAction:
            MyForm.incrBackup(self)
        if action == removeAction:
            MyForm.removeAsset(self)
        if action == openAction:
            MyForm.assetListView(self, selectedItem)
        if action == importAction:
            MyForm.importAsset(self, selectedItem.text())
        if action == refAction:
            MyForm.refAsset(self, selectedItem.text())

    def closeEvent(self, event):
        MyForm.writeSettings(self)

    def readSettings(self):
        '''Read application settings from the QT Settings object.'''
        settings = QtCore.QSettings("Duckling&Sonne", "dsAssetOpen")

        self.iconCheckBoxState = settings.value("iconCheckBoxState").toBool()
        self.ui.iconCheckBox.setChecked(self.iconCheckBoxState)

        self.productionState = settings.value("productionState").toString()
        if not self.ui.productionComboBox.findText(self.productionState) == -1:
            self.ui.productionComboBox.setCurrentIndex(self.ui.productionComboBox.findText(self.productionState))
            MyForm.assetTypeUpdate(self)

        self.assetTypeState = settings.value("assetTypeState").toString()
        if not self.ui.assetTypeComboBox.findText(self.assetTypeState) == -1:
            self.ui.assetTypeComboBox.setCurrentIndex(self.ui.assetTypeComboBox.findText(self.assetTypeState))
            MyForm.updateAssetSubTypeComboBox(self)

        self.assetSubTypeState = settings.value("assetSubTypeState").toString()
        assetSubValue = self.ui.assetSubTypeComboBox.findText(self.assetSubTypeState)
        if not assetSubValue == -1:
            self.ui.assetSubTypeComboBox.setCurrentIndex(assetSubValue)

        self.iconSizeState = settings.value("iconSizeState").toString()
        buttons = self.ui.iconButtonGroup.buttons()
        for button in buttons:
            if button.text() == self.iconSizeState:
                button.setChecked(True)

        MyForm.assetListUpdate(self)

    def writeSettings(self):
        '''Write settings'''
        settings = QtCore.QSettings("Duckling&Sonne", "dsAssetOpen")

        settings.setValue("iconCheckBoxState", self.ui.iconCheckBox.isChecked())

        settings.setValue("productionState", self.ui.productionComboBox.currentText())
        settings.setValue("assetTypeState", self.ui.assetTypeComboBox.currentText())
        settings.setValue("assetSubTypeState", self.ui.assetSubTypeComboBox.currentText())

        settings.setValue("iconSizeState", self.ui.iconButtonGroup.checkedButton().text())

#IF not runned inside Maya
if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    myapp = MyForm()
    myapp.show()
    sys.exit(app.exec_())

#If runned unside Maya
def UI():
    global assetOpenForm
    assetOpenForm = MyForm(getMayaWindow())
    assetOpenForm.show()
