import maya.cmds as mc
import maya.mel as mm
mm.eval('source taCurveMakingUI.mel')
mm.eval('source TA_lib.mel')
mm.eval('source TA_autorigLib.mel')

#animal leg rig
#define leg joint
sides = ['lf', 'rt']


upLeg_jnt = 'upLeg_jnt'
lowLeg_jnt = 'lowLeg_jnt'
ankle_jnt = 'ankle_jnt'
lowAnkle_jnt = 'lowAnkle_jnt'
ball_jnt = 'ball_jnt'
toe_jnt = 'toe_jnt'
heel_jnt = 'heel_jnt'
pelvis_jnt = 'cnt_pelvis_jnt'

outLoc = 'outFootRock_loc'
inLoc = 'inFootRock_loc'

for side in sides :

	#duplicate secondary joint

	actvJnt = [upLeg_jnt, lowLeg_jnt, ankle_jnt, lowAnkle_jnt]
	psvJnts = []

	for eachJnt in actvJnt :

		psvJnt = mc.createNode('joint', n = 'psv_%s_%s' % (side, eachJnt))
		mc.delete(mc.parentConstraint('%s_%s' % (side, eachJnt), psvJnt))
		if not eachJnt == upLeg_jnt :
			mc.parent(psvJnt, prevJnt) 
		
		prevJnt = psvJnt
		psvJnts.append(psvJnt)
		
	mc.parent(psvJnts[0], 'allJnt_grp')
	legJntOffsetGrp = mc.group('%s_%s' % (side, upLeg_jnt), psvJnts[0], n = '%s_legJnt_zGrp' % side)
	legJntGrp = mc.group(legJntOffsetGrp, n = '%s_legJnt_offsetGrp' % side)

	#follow pelvis_jnt
	mc.parentConstraint('%s' % pelvis_jnt, legJntGrp, mo = True)
		
	#ik for passiveJnt 

	psvIK = mc.ikHandle(n = '%s_psv_lowAnkle_ik' % side, sj = (psvJnts[0]), ee = (psvJnts[3]), sol = 'ikRPsolver')
	ankleIK = mc.ikHandle(n = '%s_ankle_ik' % side, sj = ('%s_%s' % (side, upLeg_jnt)), ee = ('%s_%s' % (side, ankle_jnt)), sol = 'ikRPsolver')
	lowAnkleIK = mc.ikHandle(n = '%s_lowAnkle_ik' % side, sj = ('%s_%s' % (side, ankle_jnt)), ee = ('%s_%s' % (side, lowAnkle_jnt)), sol = 'ikRPsolver')
	ballIK = mc.ikHandle(n = '%s_ball_ik' % side, sj = ('%s_%s' % (side, lowAnkle_jnt)), ee = ('%s_%s' % (side, ball_jnt)), sol = 'ikRPsolver')
	toeIK = mc.ikHandle(n = '%s_toe_ik' % side, sj = ('%s_%s' % (side, ball_jnt)), ee = ('%s_%s' % (side, toe_jnt)), sol = 'ikRPsolver')

	# grouping ik
	ankleIKPV_Grp = mc.group(ankleIK[0], n = '%s_%s' % (side, 'ankleIKPV_Grp'))
	ankleIK_Grp = mc.group(ankleIKPV_Grp, n = '%s_%s' % (side, 'ankleIK_Grp'))
	lowAnkleIK_Grp = mc.group(lowAnkleIK[0], n = '%s_%s' % (side, 'lowAnkleIK_Grp'))
	psvIKGrp = mc.group(psvIK[0], n = '%s_ankleIKpsvPV_Grp' % side)
	toeIKGrp = mc.group(toeIK[0], n = '%s_toeIK_Grp' % side)
	toePVGrp = mc.group(ballIK[0], psvIKGrp, toeIKGrp, n = '%s_toePV_Grp' % side)
	heelPVGrp = mc.group(toePVGrp, n = '%s_heelPV_Grp' % side)
	outRockGrp = mc.group(heelPVGrp, n = '%s_outRockPV_Grp' % side)
	inRockGrp = mc.group(outRockGrp, n = '%s_inRockPV_Grp' % side)
	ballPVGrp = mc.group(inRockGrp, n = '%s_ballPV_Grp' % side)
	footGrp = mc.group(ballPVGrp, n = '%s_%s' % (side, 'foot_Grp'))

	# group all ik
	allIKGrp = mc.group(ankleIK_Grp, lowAnkleIK_Grp, footGrp, n = '%s_legIk_zGrp' % side)
	mc.parent(allIKGrp, 'allIK_grp')

	# pivot group
	# ankle pivot
	anklePos = mc.xform('%s_%s' % (side, ankle_jnt), q = True, ws = True, t = True)

	# ball pivot
	ballPos = mc.xform('%s_%s' % (side, ball_jnt), q = True, ws = True, t = True)

	# lower ankle pivot
	lowAnklePos = mc.xform('%s_%s' % (side, lowAnkle_jnt), q = True, ws = True, t = True)

	# toe pivot
	toePos = mc.xform('%s_%s' % (side, toe_jnt), q = True, ws = True, t = True)

	# heel pivot
	heelPos = mc.xform('%s_%s' % (side, heel_jnt), q = True, ws = True, t = True)

	# in loc pivot
	inLocPos = mc.xform('%s' % inLoc, q = True, ws = True, t = True)

	# out loc pivot
	outLocPos = mc.xform('%s' % outLoc, q = True, ws = True, t = True)

	#  ----------------------------------------------------------------------------------

	# move foot top group pivot to lower ankle joint
	mc.xform(footGrp, os = True, piv = [lowAnklePos[0], lowAnklePos[1], lowAnklePos[2]])
	mc.xform(lowAnkleIK_Grp, os = True, piv = [lowAnklePos[0], lowAnklePos[1], lowAnklePos[2]])

	# move ankleIK_Grp pivot to ankle joint
	mc.xform(ankleIK_Grp, os = True, piv = [anklePos[0], anklePos[1], anklePos[2]])

	# move ankle ik passive group pivot to ball jnt
	mc.xform(psvIKGrp, os = True, piv = [ballPos[0], ballPos[1], ballPos[2]])

	# move toe ik group pivot to ball jnt
	mc.xform(toeIKGrp, os = True, piv = [ballPos[0], ballPos[1], ballPos[2]])

	# move toePVGrp pivot to toe joint
	mc.xform(toePVGrp, os = True, piv = [toePos[0], toePos[1], toePos[2]])

	# move heelPVGrp pivot to heel joint 
	mc.xform(heelPVGrp, os = True, piv = [heelPos[0], heelPos[1], heelPos[2]])

	# move ankleIKPV_Grp pivot to lower ankle joint
	mc.xform(ankleIKPV_Grp, os = True, piv = [lowAnklePos[0], lowAnklePos[1], lowAnklePos[2]])

	# move outRockGrp pivot to outRockPos
	if not side == 'rt' :
		mc.xform(outRockGrp, os = True, piv = [outLocPos[0], outLocPos[1], outLocPos[2]])
	else : 
		mc.xform(outRockGrp, os = True, piv = [-outLocPos[0], outLocPos[1], outLocPos[2]])

	# move inRockGrp pivot to inRockPos
	if not side == 'rt' : 
		mc.xform(inRockGrp, os = True, piv = [inLocPos[0], inLocPos[1], inLocPos[2]])
	else : 
		mc.xform(inRockGrp, os = True, piv = [-inLocPos[0], inLocPos[1], inLocPos[2]])

	# move ballPVGrp pivot to ballPos
	mc.xform(ballPVGrp, os = True, piv = [ballPos[0], ballPos[1], ballPos[2]])

	# constraint ik to passive joint
	mc.parentConstraint(psvJnts[2], ankleIK_Grp, mo = True)
	mc.parentConstraint(psvJnts[3], lowAnkleIK_Grp, mo = True)

	# make foot ctrl curve
	footCtrl = mm.eval('eval(taMakeCurve("box"));')
	footCtrl = mc.rename(footCtrl, '%s_foot_ctrl' % side)
	footCtrlGrp = mm.eval('tazGrp')
	mc.parent(footCtrlGrp, 'allCtrl_grp')

	# make upleg ctrl curve
	upLegCtrl = mm.eval('eval(taMakeCurve("box"));')
	upLegCtrl = mc.rename(upLegCtrl, '%s_upLeg_ctrl' % side)
	upLegCtrlGrp = mm.eval('tazGrp')
	mc.parent(upLegCtrlGrp, 'allCtrl_grp')
	mc.delete(mc.pointConstraint('%s_%s' % (side, upLeg_jnt), upLegCtrlGrp))

	# group foot ctrl and up leg ctrl to allCtrl_zGrp
	mc.group(footCtrlGrp, upLegCtrlGrp, n = '%s_allLegCtrl_zGrp' % side)

	mc.parentConstraint(upLegCtrl, legJntOffsetGrp, mo = True)

	# parent up leg ctrl to pelvis ctrl
	mc.parentConstraint('pelvis_ctrl', upLegCtrlGrp, mo = True)




	# move to lowAnkleJnt
	mc.delete(mc.pointConstraint('%s_%s' % (side, lowAnkle_jnt), footCtrlGrp))

	# add attributes
	foot_roll = mm.eval('TA_addAttr("%s", "roll", 0, 0, 0, 10);' % footCtrl)
	toe_roll = mm.eval('TA_addAttr("%s", "toe_roll", 0, 0, 0, 10);' % footCtrl)
	heel_roll = mm.eval('TA_addAttr("%s", "heel_roll", 0, 0, 0, 10);' % footCtrl)
	ankle_roll = mm.eval('TA_addAttr("%s", "ankle_roll", 0, 0, 0, 10);' % footCtrl)
	ball_twist = mm.eval('TA_addAttr("%s", "ball_twist", 0, 0, 0, 10);' % footCtrl)
	foot_rock = mm.eval('TA_addAttr("%s", "foot_rock", 0, 0, 0, 10);' % footCtrl)
	low_ankle = mm.eval('TA_addAttr("%s", "low_ankle", 0, 0, 0, 10);' % footCtrl)
	auto_stretch = mm.eval('TA_addAttr("%s", "auto_stretch", 1, 0, 0, 1);' % footCtrl)
	upLegStretch = mm.eval('TA_addAttr("%s", "upLeg_stretch", 0, 0, 0, 10);' % footCtrl)
	lowLegStretch = mm.eval('TA_addAttr("%s", "lowLeg_stretch", 0, 0, 0, 10);' % footCtrl)
	ankleStretch = mm.eval('TA_addAttr("%s", "ankle_stretch", 0, 0, 0, 10);' % footCtrl)

	# constraint foot ctrl to foot group
	mc.parentConstraint(footCtrl, footGrp)

	# connect nodes
	# add PMA
	toeRollPma = addPMA('%s.rotateX' % toePVGrp, '%s_toe_roll_pma' % side, 3)
	lowAnklePma = addPMA('%s.rotateX' % ankleIKPV_Grp, '%s_low_ankle_pma' % side, 3)
	heelRollPma = addPMA('%s.rotateX' % heelPVGrp, '%s_heel_roll_pma' % side, 3)
	ankleRollPma = addPMA('%s.rotateX' % psvIKGrp, '%s_annkle_roll_pma' % side, 3)
	ballTwistPma = addPMA('%s.rotateY' % ballPVGrp, '%s_ball_twist_pma' % side, 3)

	# connect attribute to group
	genConnect(toe_roll, '%s.input1' % toeRollPma, '%s_toePV' % side, 4)
	genConnect(ankle_roll, '%s.input1' % ankleRollPma, '%s_ankle_rollPV' % side, 4)
	genConnect(heel_roll, '%s.input1' % heelRollPma, '%s_heelPV' % side, -4)
	genConnect(ball_twist, '%s.input1' % ballTwistPma, '%s_ballPV' % side, 4)
	genConnect(low_ankle, '%s.input1' % lowAnklePma, '%s_low_anklePV' % side, 4)

	# foot rock
	inClamp = mc.createNode('clamp', n = '%s_in_clamp' % side)
	outClamp = mc.createNode('clamp', n = '%s_out_clamp' % side)
	if not side == 'rt' :
		mc.setAttr('%s.maxR' % inClamp, 360)
		mc.setAttr('%s.minR' % outClamp, -360)

	else : 
		mc.setAttr('%s.minR' % inClamp, -360)
		mc.setAttr('%s.maxR' % outClamp, 360)

	if side == 'lf' : 
		footRockMdv = genConnect(foot_rock, '%s.inputR' % inClamp, '%s_footRock' % side, -4)

	if side == 'rt' :
		footRockMdv = genConnect(foot_rock, '%s.inputR' % inClamp, '%s_footRock' % side, 4)

	mc.connectAttr('%s.outputR' % inClamp, '%s.rotateZ' % inRockGrp, f = True)
	mc.connectAttr('%s.outputX' % footRockMdv, '%s.inputR' % outClamp, f = True)
	mc.connectAttr('%s.outputR' % outClamp, '%s.rotateZ' % outRockGrp, f = True)

	# foot roll
	ankleClamp = mc.createNode('clamp', n = '%s_ankle_clamp' % side)
	toeClamp = mc.createNode('clamp', n = '%s_toe_clamp' % side)
	heelClamp = mc.createNode('clamp', n = '%s_heel_clamp' % side)
	offsetToePma = mm.eval('taPMA("%s", %s)' % ('offsetToe_pma', 3))
	reverseToeLimitMdv = mc.createNode('multiplyDivide', n = '%s_reverseToeLimit_mdv' % side)

	# connect mdv to clamp
	footRollMdv = genConnect(foot_roll, '%s.inputR' % heelClamp, '%s_footRoll' % side, 4)
	mc.connectAttr('%s.outputX' % footRollMdv, '%s.inputR' % ankleClamp, f = True)
	mc.connectAttr('%s.outputX' % footRollMdv, '%s.inputR' % toeClamp, f = True)

	# add clamp to ctrl shape
	footCtrlShape = mc.listRelatives(footCtrl, s = True)
	rollLimitAttr = mm.eval('TA_addAttr("%s", "%s_amp", 0, 0, 0, 10);' % (footCtrlShape[0], 'roll_limit'))

	# connect limit attr to secondTerm and toe clamp
	mc.connectAttr(rollLimitAttr, '%s.minR' % toeClamp, f = True)


	# connect toe clamp to offset pma
	mc.connectAttr('%s.outputR' % toeClamp, '%s.input1' % offsetToePma, f = True)
	mc.connectAttr(rollLimitAttr, '%s.input1X' % reverseToeLimitMdv, f = True)
	mc.setAttr('%s.input2X' % reverseToeLimitMdv, -1)
	mc.connectAttr('%s.outputX' % reverseToeLimitMdv, '%s.input2' % offsetToePma, f = True)


	# connect clamp to heel and toe Group
	mc.connectAttr('%s.outputR' % ankleClamp, '%s.input2' % ankleRollPma, f = True)
	mc.connectAttr('%s.outputR' % heelClamp, '%s.input2' % heelRollPma, f = True)
	mc.connectAttr('%s.output1D' % offsetToePma, '%s.input2' % toeRollPma, f = True)

	# set limit and clamp
	mc.setAttr(rollLimitAttr, 20)

	mc.setAttr('%s.maxR' % toeClamp, 360)
	mc.setAttr('%s.maxR' % ankleClamp, 360)
	mc.setAttr('%s.minR' % heelClamp, -360)



	# stretching ik part

	# add PMA
	upLegStretchPma = addPMA('%s_%s.scaleY' % (side, upLeg_jnt), '%s_upleg_stretch_pma' % side, 3)
	lowLegStretchPma = addPMA('%s_%s.scaleY' % (side, lowLeg_jnt), '%s_upleg_stretch_pma' % side, 3)
	ankleStretchPma = addPMA('%s_%s.scaleY' % (side, ankle_jnt), '%s_ankle_stretch_pma' %side, 3)

	upLegPvsStretchPma = addPMA('%s.scaleY' % psvJnts[0], '%s_pvs_upleg_stretch_pma' % side, 3)
	lowLegPvsStretchPma = addPMA('%s.scaleY' % psvJnts[1], '%s_pvs_upleg_stretch_pma' % side, 3)
	anklePvsStretchPma = addPMA('%s.scaleY' % psvJnts[2], '%s_pvs_ankle_stretch_pma' %side, 3)

	# add mdv 
	upLegStretchMdv = genConnect(upLegStretch, '%s.input1' % upLegStretchPma, 'upLegStretch_mdv', 0.1)
	lowLegStretchMdv = genConnect(lowLegStretch, '%s.input1' % lowLegStretchPma, 'lowLegStretch_mdv', 0.1)
	ankleStretchMdv = genConnect(ankleStretch, '%s.input1' % ankleStretchPma, 'ankleStretch_mdv', 0.1)

	mc.connectAttr('%s.outputX' % upLegStretchMdv, '%s.input1' % upLegPvsStretchPma, f = True)
	mc.connectAttr('%s.outputX' % lowLegStretchMdv, '%s.input1' % lowLegPvsStretchPma, f = True)
	mc.connectAttr('%s.outputX' % ankleStretchMdv, '%s.input1' % anklePvsStretchPma, f = True)

	# set PMA
	mc.setAttr('%s.input2' % upLegStretchPma, 0)
	mc.setAttr('%s.input2' % lowLegStretchPma, 0)
	mc.setAttr('%s.input2' % ankleStretchPma, 0)
	mc.setAttr('%s.input2' % upLegPvsStretchPma, 0)
	mc.setAttr('%s.input2' % lowLegPvsStretchPma, 0)
	mc.setAttr('%s.input2' % anklePvsStretchPma, 0)

	# autoStretch ik
	# distance between
	legStretchGrp = mc.group(em = True, n = ('%s_legStretch_Grp' % side))

	legLoc1 = mc.spaceLocator(n = '%s_upLeg_loc' % side, p = [0, 0, 0])
	legLoc2 = mc.spaceLocator(n = '%s_ankle_loc' % side, p = [0, 0, 0])
	legDist = mc.createNode('distanceBetween', n = '%s_leg_dist' % side)

	mc.parent(legLoc1, legStretchGrp)
	mc.parent(legLoc2, legStretchGrp)

	mc.pointConstraint(upLegCtrl, legLoc1)
	mc.pointConstraint (footCtrl, legLoc2)

	mc.parent(legStretchGrp, 'freeze_grp')

	mc.connectAttr('%s.translate' % legLoc1[0], '%s.point1' % legDist, f = True)
	mc.connectAttr('%s.translate' % legLoc2[0], '%s.point2' % legDist, f = True)

	# calculation
	# create mdv for leg calculation
	legStretchMdv = mc.createNode('multiplyDivide', n = '%s_legStretch_mdv' % side)

	# leg distance
	legDis = mc.getAttr('%s.distance' % legDist)
	# divide
	mc.setAttr('%s.operation' % legStretchMdv, 2)

	mc.connectAttr('%s.distance' % legDist, '%s.input1X' % legStretchMdv, f = True)
	mc.setAttr('%s.input2X' % legStretchMdv, legDis)

	# create condition
	legStretchCnd = mc.createNode('condition', n = '%s_legStretch_cnd' % side)
	mc.connectAttr('%s.outputX' % legStretchMdv, '%s.firstTerm' % legStretchCnd, f = True)
	mc.setAttr('%s.secondTerm' % legStretchCnd, 1)
	mc.setAttr('%s.operation' % legStretchCnd, 2)
	mc.connectAttr('%s.outputX' % legStretchMdv, '%s.colorIfTrueR' % legStretchCnd)
	mc.setAttr('%s.colorIfFalseR' % legStretchCnd, 1)

	# connect switch
	legSwitch_blc = mc.createNode('blendColors', n = '%s_legSwitch_blc' % side)
	mc.connectAttr(auto_stretch, '%s.blender' % legSwitch_blc, f = True)
	mc.connectAttr('%s.outColor' % legStretchCnd, '%s.color1' % legSwitch_blc, f = True)
	mc.setAttr('%s.color2R' % legSwitch_blc, 1)

	# connect to joints
	mc.connectAttr('%s.outputR' % legSwitch_blc, '%s.input3' % upLegStretchPma, f = True)
	mc.connectAttr('%s.outputR' % legSwitch_blc, '%s.input3' % lowLegStretchPma, f = True)
	mc.connectAttr('%s.outputR' % legSwitch_blc, '%s.input3' % ankleStretchPma, f = True)
	mc.connectAttr('%s.outputR' % legSwitch_blc, '%s.input3' % upLegPvsStretchPma, f = True)
	mc.connectAttr('%s.outputR' % legSwitch_blc, '%s.input3' % lowLegPvsStretchPma, f = True)
	mc.connectAttr('%s.outputR' % legSwitch_blc, '%s.input3' % anklePvsStretchPma, f = True)


def genConnect(src, dst, nodeName, default) : 
	srcShape = mc.listRelatives(src.split('.')[0], s = True)
	mdv = mc.createNode('multiplyDivide', n = '%s_mdv' % nodeName)
	mc.connectAttr(src, '%s.input1X' % mdv, f = True)
	mc.connectAttr('%s.outputX' % mdv, dst, f = True)
	attr = mm.eval('TA_addAttr("%s", "%s_amp", 0, 0, 0, 10);' % (srcShape[0], nodeName))
	mc.connectAttr(attr, '%s.input2X' % mdv, f = True)
	mc.setAttr(attr, default)
	return mdv
	
def addPMA(src, nodeName, channel) : 
	pma = mm.eval('taPMA("%s", %s)' % (nodeName, channel))
	mc.connectAttr('%s.output1D' % pma, src, f = True)
	return pma
	
