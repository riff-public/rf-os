import maya.cmds as mc 
from functools import partial
import sys
import os
sys.path.append('Y:\USERS\Ta\scripts')
import genScript 


def run() : 
	a = exportProp()
	a.show()

#center prop
class exportProp() : 

	def __init__(self) : 
		self.win = 'exportPropUI'
		self.os = ''
		if sys.platform == 'win32' :
			#self.path = 'P:/Lego_Friends/asset/3D/'
			self.path = 'P:/'
			self.os = 'win'
		if sys.platform == 'linux2' : 
			#self.path = '/dsPipe/Lego_Friends/asset/3D/'
			self.path = '/dsPipe/'
			self.os = 'linux'
	
	def show(self) : 
		if(mc.window(self.win, exists = True)) : 
			mc.deleteUI(self.win)
			
		mc.window(self.win, t = 'export assets v2.2', wh = [300,200])
		mc.columnLayout(adj = 1, rs = 4)
		mc.frameLayout(lv = 0, borderStyle = 'etchedIn')
		mc.rowColumnLayout(nc = 2, cw = [(1, 40), (2, 270)], cs = [(2, 4)])
		mc.text(l = 'path', al = 'left')
		mc.textField('%s_pathTX' % self.win)
		
		mc.text(l = 'project', al = 'left')
		project = self.browseProject()
		mc.optionMenu('%s_project_OP' % self.win, cc = partial(self.browseLV1))
		
		for each in project : 
			mc.menuItem(l = each)
						
		mc.setParent('..')
		mc.setParent('..')
		
		mc.checkBox('%s_CB' % self.win, l = 'Include Controller', v = 1)
		mc.checkBox('%s_expCB' % self.win, l = 'Export', v = 1)
		
		mc.frameLayout(lv = 0, borderStyle = 'etchedIn')
		mc.rowColumnLayout(nc = 3, cw = [(1, 100), (2, 100), (3, 100)], cs = [(2, 4), (3, 4)])
		mc.textScrollList('%s_lv1' % self.win, sc = partial(self.browseLv2))
		mc.textScrollList('%s_lv2' % self.win, sc = partial(self.browseLv3))
		mc.textScrollList('%s_lv3' % self.win, sc = partial(self.setText))
		mc.textField('%s_lv1TX' % self.win)
		mc.textField('%s_lv2TX' % self.win)
		mc.textField('%s_lv3TX' % self.win)

		mc.setParent('..')
		mc.setParent('..')
		mc.checkBox('%s_MOCB' % self.win, l = 'Maintain Offset')
		mc.button(l = 'Export', h = 30, c = partial(self.exportCmd))
		mc.showWindow()
		mc.window(self.win, e = True, wh = [320, 362])
		self.browseLV1()
		
		
	def exportCmd(self, arg = None) : 
		sel = mc.ls(sl = True)
		output = mc.textField('%s_pathTX' % self.win, q = True, tx = True)
		exists = 0
		
		if mc.checkBox('%s_CB' % self.win, q = True, v = True) : 
			if not mc.objExists('Rig_Grp') : 
				genScript.importRigGrp()
				
			else : 
				mc.select('Rig_Grp', hi = True)
				mc.lockNode(l = False)
				mc.rename('Rig_Grp', 'Rig_Grp2')
				mc.rename('Root_Ctrl', 'Root_Ctrl2')
				mc.rename('SuperRoot_Ctrl', 'SuperRoot_Ctrl2')
				mc.rename('Proxy_Geo_Grp', 'Proxy_Geo_Grp2')
				genScript.importRigGrp()
				exists = 1
				
			loc1 = mc.spaceLocator(p = [0, 0, 0])
			loc2 = mc.spaceLocator(p = [0, 0, 0])
			mc.delete(mc.parentConstraint(sel[0], loc2[0]))
			mc.delete(mc.parentConstraint(loc1[0], sel[0]))
			
			if mc.objExists('Proxy_Geo_Grp') : 
				parent = 'Proxy_Geo_Grp'
				mc.parent(sel[0], parent)
				
			if mc.checkBox('%s_MOCB' % self.win, q = True, v = True) : 
				mc.delete(mc.parentConstraint(loc2[0], 'Rig_Grp'))
				
			self.setExtraAttr()
				
			mc.select('Rig_Grp', 'Anim_Set', 'Blocking_Set', 'Proxy_Set', 'Render_Set')
			print ('sel all')
			if mc.checkBox('%s_expCB' % self.win, q = True, v = True) : 
				mc.file(output, force = True, options = 'v=0', typ = 'mayaAscii', pr = True, es = True)
			
			mc.select('Rig_Grp', hi = True)
			mc.lockNode(l = False)
			
			if mc.checkBox('%s_expCB' % self.win, q = True, v = True) : 
				mc.delete('Rig_Grp')
				mc.delete(loc1[0])
				mc.delete(loc2[0])
			
			if exists == 1 : 
				mc.rename('Rig_Grp2', 'Rig_Grp')
				mc.rename('Root_Ctrl2', 'Root_Ctrl')
				mc.rename('SuperRoot_Ctrl2', 'SuperRoot_Ctrl')
				mc.rename('Proxy_Geo_Grp2', 'Proxy_Geo_Grp')
				
		else : 
			if mc.checkBox('%s_expCB' % self.win, q = True, v = True) : 
				mc.file(output, force = True, options = 'v=0', typ = 'mayaAscii', pr = True, es = True)
			

		
		
	def browseProject(self) : 
		project = mc.getFileList(folder = self.path)
		returnValue = []
		for each in project :
			if each.split('_')[0] == 'Lego' : 
				returnValue.append(each)
		
		return returnValue
	
	def browseLV1(self, arg = None) :
		project = mc.optionMenu('%s_project_OP' % self.win, q = True, v = True) 
		browse = '%s%s/asset/3D/' % ((self.path), project)
		mc.textScrollList('%s_lv2' % self.win, e = True, ra = True)
		mc.textScrollList('%s_lv3' % self.win, e = True, ra = True)
		if os.path.exists(browse) : 
			files = sorted(mc.getFileList(folder = browse))
			mc.textScrollList('%s_lv1' % self.win, e = True, ra = True)
			for each in files : 
				mc.textScrollList('%s_lv1' % self.win, e = True, append = each)
			
		
	def browseLv2(self) : 
		sel1 = mc.textScrollList('%s_lv1' % self.win, q = True, si = True)
		project = mc.optionMenu('%s_project_OP' % self.win, q = True, v = True) 
		browse = '%s%s/asset/3D/%s/' % (self.path, project, sel1[0])
		if os.path.exists(browse) : 
			files = mc.getFileList(folder = browse)
			mc.textScrollList('%s_lv2' % self.win, e = True, ra = True)
			for each in files : 
				mc.textScrollList('%s_lv2' % self.win, e = True, append = each)
		else : 
			mc.textScrollList('%s_lv2' % self.win, e = True, ra = True)
			mc.textScrollList('%s_lv2' % self.win, e = True, append = 'Not in the system')
			
	def browseLv3(self) : 
		sel1 = mc.textScrollList('%s_lv1' % self.win, q = True, si = True)
		sel2 = mc.textScrollList('%s_lv2' % self.win, q = True, si = True)
		project = mc.optionMenu('%s_project_OP' % self.win, q = True, v = True) 
		browse = '%s%s/asset/3D/%s/%s/' % (self.path, project, sel1[0], sel2[0])
		if os.path.exists(browse) : 
			files = mc.getFileList(folder = browse)
			mc.textScrollList('%s_lv3' % self.win, e = True, ra = True)
			for each in files : 
				mc.textScrollList('%s_lv3' % self.win, e = True, append = each)
		else : 
			mc.textScrollList('%s_lv3' % self.win, e = True, ra = True)
			mc.textScrollList('%s_lv3' % self.win, e = True, append = 'Not in the system')
			
	def setText(self) : 
		sel1 = mc.textScrollList('%s_lv1' % self.win, q = True, si = True)
		sel2 = mc.textScrollList('%s_lv2' % self.win, q = True, si = True)
		sel3 = mc.textScrollList('%s_lv3' % self.win, q = True, si = True)
		mc.textField('%s_lv1TX' % self.win, e = True, tx = sel1[0])
		mc.textField('%s_lv2TX' % self.win, e = True, tx = sel2[0])
		mc.textField('%s_lv3TX' % self.win, e = True, tx = sel3[0])
		project = mc.optionMenu('%s_project_OP' % self.win, q = True, v = True) 
		path = '%s%s/asset/3D/%s/%s/%s/' % (self.path, project, sel1[0], sel2[0], sel3[0])
		file = '%s_Rig.ma' % sel3[0]
		mc.textField('%s_pathTX' % self.win, e = True, tx = '%sdev/maya/%s' % (path, file))

	
	def setExtraAttr(self) : 
		assetType = mc.textField('%s_lv1TX' % self.win, q = True, tx = True)
		subAssetType = mc.textField('%s_lv2TX' % self.win, q = True, tx = True)
		assetName = mc.textField('%s_lv3TX' % self.win, q = True, tx = True)
		projectName = ''
		if self.os == 'win' : 
			projectName = mc.file(q = True, sn = True).split('/')[1]
		if self.os == 'linux' : 
			projectName = mc.file(q = True, sn = True).split('/')[2]

		
		if mc.objExists('Rig_Grp') : 
			mc.setAttr('Rig_Grp.assetType', assetType, type = 'string')
			mc.setAttr('Rig_Grp.assetSubType', subAssetType, type = 'string')
			mc.setAttr('Rig_Grp.assetName', assetName, type = 'string')
			mc.setAttr('Rig_Grp.project', projectName, type = 'string')

run()