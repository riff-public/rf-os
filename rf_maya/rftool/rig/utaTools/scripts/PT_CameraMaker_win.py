import maya.cmds as mc 
import maya.mel as mm
from functools import partial
import os.path
import maya.mel as mm
import sys

def run() : 
	ui = PT_CameraMaker()
	ui.show()

class PT_CameraMaker : 
	def __init__(self) : 
		self.win = 'PT_CameraMaker'
		self.ui = '%s_Win' % self.win
		if sys.platform == 'win32' :
			self.fileName =  'N:/globalMaya/Mel/ptTools/LOCA00_K_camera.ma'
		if sys.platform == 'linux2' : 
			self.fileName =  '/dsGlobal/globalMaya/Mel/ptTools/LOCA00_K_camera.ma'
		
	def show(self) : 
		if mc.window(self.ui, exists = True) : 
			mc.deleteUI(self.ui)
			
		mc.window(self.ui, title = 'PT_CameraMaker win v.1.0', s = True, wh = [600, 500])
		layout = mc.columnLayout(adj = 1, rs = 4)
		mc.frameLayout(lv = 0, borderStyle = 'etchedIn')
		mc.rowColumnLayout(nc = 3, cw = [(1, 100), (2, 140), (3, 100)], cs = [(2, 10), (3, 20)])
		mc.text(l = 'Camera Name')
		mc.textField('%s_nameTX' % self.win, tx = 's0010')
		
		mc.optionMenu('%s_presetOP' % self.win, l = 'Name Preset', cc = partial(self.fillName))
		
		no = self.numberOfCam(40)
		for each in no : 			
			mc.menuItem(l = each)
			
		mc.setParent('..')
		mc.setParent('..')
		
		mc.frameLayout(lv = 0, borderStyle = 'etchedIn')
		mc.rowColumnLayout(nc = 4, cw = [(1, 100), (2, 60), (3, 100), (4, 60)], cs = [(2, 10)])
		mc.text(l = 'Timeline Start')
		mc.textField('%s_timeStartTX' % self.win, tx = '1', cc = partial(self.autoText))
		mc.text(l = 'Timeline End')
		mc.textField('%s_timeEndTX' % self.win, tx = '24', cc = partial(self.autoText))
		
		mc.text(l = 'Sequence Start')
		mc.textField('%s_SeqStartTX' % self.win, tx = '1')
		mc.text(l = 'Sequence End')
		mc.textField('%s_SeqEndTX' % self.win, tx = '24')
		
		mc.setParent('..')
		mc.setParent('..')
		
		mc.checkBox('%s_insertCB' % self.win, l = 'insert after last sequence', v = True)
		mc.button(l = 'Update', c = partial(self.updateTextField))
		mc.button(l = 'Create Camera', h = 30, c = partial(self.sequencerCmd))
		mc.setParent('..')
	
		mc.showWindow()
		mc.window(self.ui, e = True, wh = [410, 156])
		
		
	def numberOfCam(self, no) : 
		cam = []
		for i in range(no) : 
			if i < 9 : 
				cam.append('s00%d0' % (i + 1))
				
			else : 
				cam.append('s0%d0' % (i + 1))
				
		return cam
		
		
	def fillName(self, arg = None) : 
		sel = mc.optionMenu('%s_presetOP' % self.win, q = True, v = True)
		mc.textField('%s_nameTX' % self.win, e = True, tx = sel)
		

	def sequencerCmd(self, arg = None) : 
		name = mc.textField('%s_nameTX' % self.win, q = True, tx = True)
		TS = mc.textField('%s_timeStartTX' % self.win, q = True, tx = True)
		TE = mc.textField('%s_timeEndTX' % self.win, q = True, tx = True)
		SS = mc.textField('%s_SeqStartTX' % self.win, q = True, tx = True)
		SE = mc.textField('%s_SeqEndTX' % self.win, q = True, tx = True)
		
		if not self.checkCamExists() : 
			
			cam = self.importCamRig(name)
			#mc.rename(cam[1], '%s_camShape' % name)
			#cam[0] = mc.rename(cam[0], '%s_cam' % name)
			
			mc.shot(name, startTime = int(TS), endTime = int(TE), sequenceStartTime = int(SS), sequenceEndTime = int(SE), currentCamera = cam, clip = '')
			
			if mc.checkBox('%s_insertCB' % self.win, q = True, v = True) : 
				self.updateTextField()
				
			camShape = mc.listRelatives(cam, s = True)
			mc.setAttr('%s.displayResolution' % camShape[0], 1)
			mc.setAttr('%s.width' % 'defaultResolution', 1920)
			mc.setAttr('%s.height' % 'defaultResolution', 1080)
			mc.setAttr('%s.deviceAspectRatio' % 'defaultResolution', 1.777)
			mc.setAttr('%s.pixelAspect' % 'defaultResolution', 1.000)
			mc.setAttr('%s.displayGateMaskColor' % camShape[0], 0, 0, 0)
			mc.setAttr('%s.displayGateMaskOpacity' % camShape[0], 1)	
	
	def autoText(self, arg = None) : 
		TS = mc.textField('%s_timeStartTX' % self.win, q = True, tx = True) 
		TE = mc.textField('%s_timeEndTX' % self.win, q = True, tx = True)

		if int(TS) >= int(TE) : 
			mc.textField('%s_timeEndTX' % self.win, e = True, tx = str(int(TS) + 1))
			TE = mc.textField('%s_timeEndTX' % self.win, q = True, tx = True)
			
		SS = mc.textField('%s_SeqStartTX' % self.win, e = True, tx = (mc.textField('%s_timeStartTX' % self.win, q = True, tx = True)))
		SE = mc.textField('%s_SeqEndTX' % self.win, e = True, tx = (mc.textField('%s_timeEndTX' % self.win, q = True, tx = True)))


	def checkLastFrame(self) : 
		shots = mc.ls(type = 'shot')
		max = 0
		for each in shots : 
			seqEnd = int(mc.getAttr('%s.sequenceEndFrame' % each))
			if seqEnd > max : 
				max = seqEnd
				
		return max
				
	def updateTextField(self, arg = None) : 
		max = self.checkLastFrame()
		mc.textField('%s_timeStartTX' % self.win, e = True, tx = (max + 1)) 
		self.autoText()
		
		
	def checkCamExists(self, arg = None) : 
		cams = mc.listCameras()
		sel = mc.textField('%s_nameTX' % self.win, q = True, tx = True)
		for each in cams : 
			if each == ('%s_cam' % sel) :  
				mc.confirmDialog( title='Error', message='Camera already exists\n Choose the different name.' , button=['OK'])
				return 1
			
		else : 
			return 0
			
	def importCamRig(self, name, arg = None) : 
		fileName =  self.fileName
		mc.file(fileName, i = True, type = 'mayaAscii')
		newCam = []
		
		if mc.objExists('root_ctrl') : 
			mc.select('root_ctrl')
			mc.select(hi = True)
			allHierarchy = mc.ls(sl = True)
			for each in allHierarchy : 
				try : 
					newName = mc.rename(each, '%s_%s' % (name, each))
						
				except RuntimeError :
					print 'skip'
					
		return '%s_cam' % name