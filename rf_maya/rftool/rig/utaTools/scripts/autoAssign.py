import maya.cmds as mc
import maya.mel as mm

def assign() : 
    if mc.objExists('Rig_Grp') : 
    	check = ['assetType', 'assetSubType', 'assetName', 'project']
    	attr = mc.listAttr('Rig_Grp', ud = True)
    	
    	for each in check : 
    		if not each in attr : 
    			mc.addAttr('Rig_Grp', ln = each,  dt = 'string')
    			mc.setAttr('Rig_Grp.%s' % each, e = True, keyable = True)
    			mm.eval('print "create %s ";' % each)
				
    	file = mc.file(q = True, sn = True)
    	project = file.split('/')[1]
        assetType = file.split('/')[4]
        assetSubType = file.split('/')[5]
        assetName = file.split('/')[6]
        mc.setAttr('Rig_Grp.assetType', assetType, type = 'string')
        mc.setAttr('Rig_Grp.assetSubType', assetSubType, type = 'string')
        mc.setAttr('Rig_Grp.assetName', assetName, type = 'string')
        mc.setAttr('Rig_Grp.project', project, type = 'string')
        mm.eval('print "Set OK";')