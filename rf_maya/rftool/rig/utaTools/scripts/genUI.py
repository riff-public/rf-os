import maya.cmds as mc
import maya.mel as mm
import sys
sys.path.append(r"P:\lib\local\utaTools\scripts")

def genUI() : 
    if mc.window('genWin', exists = True) : 
        mc.deleteUI('genWin')
        
    mc.window('genWin', title = 'general Commands Window')
    mc.columnLayout(adj = True, rs = 2)
    mc.button(l = 'import Rig Grp', c = 'import genScript\ngenScript.importRigGrp()', h = 30)
    mc.button(l = 'Auto Assign', c = 'import autoAssign\nautoAssign.assign()', h = 30)
    mc.button(l = 'Export Assets', c = 'import exportProp\nexportProp.run()', h = 30)
    mc.button(l = 'Rig Selected', c = 'import rigSelected\nrigSelected.rigSelected()', h = 30)
    mc.button(l = 'Create Locator', c = 'import makingLoc\nmakingLoc.run()', h = 30)
    mc.button(l = 'Place Assets', c = 'import placeAsset\nplaceAsset.run()', h = 30)
    mc.button(l = 'Rig Car', c = 'import carRig\ncarRig.run()', h = 30)
    mc.button(l = 'Make Icon', c = 'import makeIcon\nmakeIcon.makeIcon()', h = 30)
    mc.button(l = 'Easy Copy Paste', c = 'import easyClipboard\neasyClipboard.run()', h = 30) 
    mc.button(l = 'Replace Sel', c = 'import PT_replaceSel\nPT_replaceSel.run()', h = 30)
    mc.button(l = 'Replace Vray', c = 'mm.eval("replaceVray");', h = 30)
    mc.frameLayout(borderStyle = 'etchedIn', l = 'Controls')
    mc.columnLayout(adj = 1, rs = 4)
    
    mc.button(l = 'Make Control', c = 'mm.eval("TACurveMakingUI");', h = 30)
    mc.button(l = 'Curve Color', c = 'mm.eval("TAccUI");', h = 30)
    mc.button(l = 'Rename Tool', c = 'mm.eval("taRenameUI")', h = 30)
    
    mc.setParent('..')
    mc.setParent('..')
    
    mc.showWindow()
    mc.window('genWin', e = True, wh = [100, 475])