import maya.cmds as cmds

def PT_IsolateCamUI():
    if cmds.window('isolateCamUI', exists = True):
        cmds.deleteUI('isolateCamUI')
    
    cmds.window('isolateCamUI', title = 'View Cam')
    cmds.columnLayout(adj = True, rs = 4)
    cmds.text(l = "List all Cameras")
    cmds.textScrollList('camTSL', numberOfRows = 8, allowMultiSelection = 0, sc = 'viewCam.PT_isolateCamCmd(1)')
    cmds.button(l = 'Visible All', c = 'viewCam.PT_isolateCamCmd(2)' )
    cmds.button(h = 30, l = 'Refresh', c = 'viewCam.PT_RefreshUI()')
    
    cmds.showWindow('isolateCamUI')
    cmds.window('isolateCamUI', edit = True, wh = [180, 270])
    
    #tricker command
    PT_RefreshUI()

#exclude lists (tupple)
def excludeList(input, exclude):
    
    for cam in exclude:
        for each in input:
            if cam == each:
                input.remove(cam)
                
    return input
    
def PT_RefreshUI():
    
    #get cam
    allCam = cmds.listCameras(p = True)
    exclude = ['persp', 'master']
    
    #remove master cam
    listCam = excludeList(allCam, exclude)
    listCam = PT_removeMasterCam(listCam)
    
    #update UI lists
    PT_refreshTSL(listCam)       
    

def PT_refreshTSL(inputList):
    #get list from UI
    camList = cmds.textScrollList('camTSL', q = True, si = True)
    
    cmds.textScrollList('camTSL', e = True, ra = True)
    
    for each in inputList:           
        cmds.textScrollList('camTSL', e = True, append = each)
        
def PT_removeMasterCam(camList):
    for cam in camList:
        if cam.split('_')[0] == 'master':
            camList.remove(cam)

    return camList
        
def PT_ChannelControl(object, name, lock, hide):
    cmds.setAttr('%s.%s' % (object, name), k = hide)
    cmds.setAttr('%s.%s' % (object, name), lock = lock)
    
def PT_isolateCamCmd(type,arg=None):
    allCam = cmds.textScrollList('camTSL', q = True, ai = True)
    selList = cmds.textScrollList('camTSL', q = True, si = True)
    
    if type == 1:
    
        for cam in allCam:
            camCtrl = cam.split(':')
            camCtrl = ('%s:root_ctrl' %camCtrl[0])
            
            if selList[0] == cam:            
                PT_ChannelControl(camCtrl, 'visibility', False, True)
                cmds.setAttr('%s.visibility' % camCtrl, 1)
                PT_ChannelControl(camCtrl, 'visibility', True, False)
                print ('select cam %s' % camCtrl)
            else:
                PT_ChannelControl(camCtrl, 'visibility', False, True)
                cmds.setAttr('%s.visibility' % camCtrl, 0)
                PT_ChannelControl(camCtrl, 'visibility', True, False)
                print ('deselect cam %s' % camCtrl)

    if type == 2:
        
        for cam in allCam:
            camCtrl = cam.split(':')
            camCtrl = ('%s:root_ctrl' %camCtrl[0])
            
            PT_ChannelControl(camCtrl, 'visibility', False, True)
            cmds.setAttr('%s.visibility' % camCtrl, 1)
            PT_ChannelControl(camCtrl, 'visibility', True, False)        
        
            
