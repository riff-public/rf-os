//source TA_lib.mel
source "TA_lib.mel";

global proc TA_ribbonUI()
{
	if (`window -exists TA_ribbonWin`)
	deleteUI TA_ribbonWin;
	
	window -t "TA Ribbon v.1.0" TA_ribbonWin;
	columnLayout -adj 1 -rs 4;
	
	text -l "prefix";
	textField -tx "Char" prefixTX;
	text -l "side";
	textField -tx "side" sideTX;
	text -l "element";
	textField -tx "element" elementTX;
	text -l "distance";
	textField -tx "10" distanceTX;
	button -l "Create" -c TA_ribbonUICmd -h 30;
	showWindow;
	window -e -wh 200 200 TA_ribbonWin;
	
}

global proc TA_ribbonUICmd() 
{
	string $prefix = `textField -q -tx prefixTX`;
	string $side = `textField -q -tx sideTX`;
	string $element = `textField -q -tx elementTX`;
	string $distance = `textField -q -tx distanceTX`;
	TA_ribbonCmd($prefix, $side, $element, $distance);
	
}

//RIBBON MAIN PROCEDURE ///////////////////////////////////////--------------------------------------------------------------


global proc string TA_ribbonCmd(string $prefix, string $side, string $element, float $distance)
{
	
string $ribbonName;
//create nurbsPlane //////////////////////////

$ribbonPlane = `nurbsPlane -n ($prefix+"_"+$side+"_"+$element+"_rb_plane") -p 0 0 0 -ax 0 1 0 -w 1 -lr 1 -d 3 -u 1 -v 5 -ch 1`;

xform -ws -ro 0 -90 0 $ribbonPlane;
xform -ws -s 1 1 $distance $ribbonPlane;

rebuildSurface -ch 1 -rpo 1 -rt 0 -end 1 -kr 2 -kcp 0 -kc 0 -su 1 -du 1 -sv 5 -dv 3 -tol 0.0001 -fr 0  -dir 2 $ribbonPlane;
makeIdentity -apply true -t 1 -r 1 -s 1 -n 0 $ribbonPlane;
delete -ch $ribbonPlane;

$ribbonPlaneGrp = `group -n ($ribbonPlane[0]+"_grp") $ribbonPlane[0]`;

//create group to organize hierarchy--------------------------------------------------------------

$ctrlGrp = `group -em -n ($prefix+"_"+$side+"_"+$element+"_ribbon_Grp")`;

$allJointGrp 	= `group -em -n ($prefix+"_"+$side+"_"+$element+"_ribbon_allJntGrp")`;
$allJntPlaneGrp = `group -em -n ($prefix+"_"+$side+"_"+$element+"_ribbon_allJntPlaneGrp")`;
$allSubJointGrp = `group -em -n ($prefix+"_"+$side+"_"+$element+"_ribbon_allSubJntGrp")`;

$allCtrlGrp 		= `group -em -n ($prefix+"_"+$side+"_"+$element+"_ribbon_allCtrlGrp")`;
$allCtrlPlaneGrp 	= `group -em -n ($prefix+"_"+$side+"_"+$element+"_ribbon_allCtrlPlaneGrp")`;
$allSubCtrlGrp 		= `group -em -n ($prefix+"_"+$side+"_"+$element+"_ribbon_allSubCtrlGrp")`;

parent $allSubJointGrp 	$allJointGrp;
parent $allJointGrp 	$ctrlGrp;
parent $allJntPlaneGrp 	$allJointGrp;

parent $allCtrlGrp 		$ctrlGrp;
parent $allSubCtrlGrp 	$allCtrlGrp;
parent $allCtrlPlaneGrp $allCtrlGrp;

parent $ribbonPlaneGrp	$ctrlGrp;


//loop for ribbon//////////////--------------------------------------------------------------

for($i = 0; $i < 5; $i++)
{
//create joint, pointOnSurfaceInfo and aimConstraint Node ////////////////////////////

	$poiNode 	= `createNode pointOnSurfaceInfo -n ($prefix+"_"+$side+"_"+$element+"_psi_"+$i)`;
	$aimNode 	= `createNode aimConstraint -n ($prefix+"_"+$side+"_"+$element+"_aim_"+$i)`;
	
	$joint 			= `createNode joint -n ($prefix+"_"+$side+"_"+$element+"_ribbon_"+$i+"_jnt")`;
	$jointaimGrp 	= `group -n ($joint+"aimGrp") $joint`;
	$jointGrp 		= `group -n ($joint+"Grp") $joint`;
	$jointTwistGrp 	= `group -n ($joint+"TwistGrp") $jointGrp`;
	
	$jointConstraintGrp = `group -n ($joint+"CPGrp") $jointTwistGrp`;
	

//group elements 

	parent $jointaimGrp 	$allSubJointGrp;
	parent $aimNode 		$jointaimGrp;

//connect attribute ////////////--------------------------------------------------------------

	//connect plane.worldSpace[0] to pointOnSurfaceInfo.inputSurface
	connectAttr -f ($ribbonPlane[0]+".worldSpace[0]") ($poiNode+".inputSurface");

	//connect pointOnSurfaceInfo.position to jointGrp.translate
	connectAttr -f ($poiNode+".position") ($jointaimGrp+".translate");

	//set joint to its position
	setAttr ($poiNode+".parameterU") 0.5;
	setAttr ($poiNode+".parameterV") (0.5+$i);

	//connect poiNode.rangentV to aimNode.worldUpVector
	connectAttr -f ($poiNode+".tangentV") ($aimNode+".worldUpVector");

	//connect poiNode.rusult.normal to aimNode.tg[0].tt
	connectAttr ($poiNode+".result.normal") ($aimNode+".tg[0].tt");

	//connect aimNode.constraintRotate to jointGrp.rotate
	connectAttr -f ($aimNode+".constraintRotate") ($jointaimGrp+".rotate");

	//set up axis
	setAttr ($aimNode+".aimVectorX") 0;
	setAttr ($aimNode+".aimVectorY") 1;
	setAttr ($aimNode+".aimVectorZ") 0;

	setAttr ($aimNode+".upVectorX") 1;
	setAttr ($aimNode+".upVectorY") 0;
	setAttr ($aimNode+".upVectorZ") 0;

	//create curve ctrl
	$curve = TAribbonCurve(($prefix+"_"+$side+"_"+$element+"_ribbon_"+$i+"_ctrl"),1,1);
	$curveGrp = `group -n ($curve+"Grp")`;

	//move ctrl to joint position
	delete(`parentConstraint ($jointaimGrp) ($curveGrp)`);
	
	//parentConstraint ctrl joint
	parentConstraint $curve $jointConstraintGrp;

	//ctrl move along with the group
	parentConstraint $jointaimGrp $curveGrp;

	//put curve Grp to ctrlGrp
	parent $curveGrp $allSubCtrlGrp;
}

//----------------------------------------------------------------------------------------------------------------------------
//create Control set for ribbon
//----------------------------------------------------------------------------------------------------------------------------

//create left joint group

$leftJoint = `createNode joint -n ($prefix+"_"+$side+"_"+$element+"_lf_RBB_jnt")`;
$leftJntGrp = `group -n ($leftJoint+"Grp") $leftJoint`;
$leftJntAimGrp = `group -n ($leftJoint+"AimGrp") $leftJntGrp`;
$leftJntTargetGrp = `group -n ($leftJoint+"TargetGrp") $leftJntAimGrp`;
$leftJntzGrp = `group -n ($leftJoint+"zGrp") $leftJntTargetGrp`;


//create up group

$leftJntUpGrp = `group -em -n ($prefix+"_"+$side+"_"+$element+"_lf_RBB_upGrp")`;
xform -ws -t 0 0 1 $leftJntUpGrp;
parent $leftJntUpGrp $leftJntTargetGrp;

//position to the left

xform -ws -t (-$distance/2) 0 0 $leftJntzGrp;


//create center joint group

$centerJoint = `createNode joint -n ($prefix+"_"+$side+"_"+$element+"_cnt_RBB_jnt")`;
$centerJntGrp = `group -n ($centerJoint+"Grp") $centerJoint`;
$centerJntAimGrp = `group -n ($centerJoint+"AimGrp") $centerJntGrp`;
$centerJntPointGrp = `group -n ($centerJoint+"PointGrp") $centerJntAimGrp`;
$centerJntTargetGrp = `group -n ($centerJoint+"TargetGrp") $centerJntPointGrp`;

//create up group

$centerJntUpGrp = `group -em -n ($prefix+"_"+$side+"_"+$element+"_cnt_RBB_upGrp")`;
xform -ws -t 0 0 1 $centerJntUpGrp;
parent $centerJntUpGrp $centerJntPointGrp;


//create right joint group

$rightJoint = `createNode joint -n ($prefix+"_"+$side+"_"+$element+"_rt_RBB_jnt")`;
$rightJntGrp = `group -n ($rightJoint+"Grp") $rightJoint`;
$rightJntAimGrp = `group -n ($rightJoint+"AimGrp") $rightJntGrp`;
$rightJntTargetGrp = `group -n ($rightJoint+"TargetGrp") $rightJntAimGrp`;
$rightJntzGrp = `group -n ($rightJoint+"zGrp") $rightJntTargetGrp`;

//create up group

$rightJntUpGrp = `group -em -n ($prefix+"_"+$side+"_"+$element+"_rt_RBB_upGrp")`;
xform -ws -t 0 0 1 $rightJntUpGrp;
parent $rightJntUpGrp $rightJntTargetGrp;

//position to the right

xform -ws -t ($distance/2) 0 0 $rightJntzGrp;

//organized joint group

parent $leftJntzGrp $allJntPlaneGrp;
parent $rightJntzGrp $allJntPlaneGrp;
parent $centerJntTargetGrp $allJntPlaneGrp;



////////////////////////////////////////////////////////--------------------------------------------------------------
//rig ctrl ribbon

////////////////////////////////////////////////////////--------------------------------------------------------------

//set aim constraint left joint to right group

$leftJntAimNode = `aimConstraint -offset 0 0 0 
					-weight 1 
					-aimVector 1 0 0 
					-upVector 0 0 1 
					-worldUpType "object" 
					-worldUpObject $leftJntUpGrp
					$rightJntTargetGrp $leftJntAimGrp`;
					

//set aim constraint right joint to left group

$rightJntAimNode = `aimConstraint -offset 0 0 0 
					-weight 1 
					-aimVector -1 0 0 
					-upVector 0 0 1 
					-worldUpType "object" 
					-worldUpObject $rightJntUpGrp
					$leftJntTargetGrp $rightJntAimGrp`;
					

//set aim constraint center joint to left group

$centerJntAimNode = `aimConstraint -offset 0 0 0 
					-weight 1 
					-aimVector -1 0 0 
					-upVector 0 0 1 
					-worldUpType "object" 
					-worldUpObject $centerJntUpGrp
					$leftJntTargetGrp $centerJntAimGrp`;
					

//set point constraint center joint and the other two joints

$centerJntPointNode = `pointConstraint $leftJntTargetGrp $rightJntTargetGrp $centerJntPointGrp`;



//create controls for all three joints //////////////////////////////////////////////////////////////////////////

//lf joint--------------------------------------------------------------

$leftJntCtrl = TAribbonCurve($prefix+"_"+$side+"_"+$element+"_lf_RBB_ctrl",1.4,6);
$leftJntCtrlGrp = `group -n ($leftJntCtrl+"Grp") $leftJntCtrl`;
$leftJntCtrlzGrp = `group -n ($leftJntCtrl+"zGrp") $leftJntCtrlGrp`;

delete(`parentConstraint $leftJntTargetGrp $leftJntCtrlzGrp`);

$leftJntCtrlConstraint = `parentConstraint $leftJntCtrl $leftJntTargetGrp`;


//rt joint--------------------------------------------------------------

$rightJntCtrl = TAribbonCurve($prefix+"_"+$side+"_"+$element+"_rt_RBB_ctrl",1.4,6);
$rightJntCtrlGrp = `group -n ($rightJntCtrl+"Grp") $rightJntCtrl`;
$rightJntCtrlzGrp = `group -n ($rightJntCtrl+"zGrp") $rightJntCtrlGrp`;

delete(`parentConstraint $rightJntTargetGrp $rightJntCtrlzGrp`);

$rightJntCtrlConstraint = `parentConstraint $rightJntCtrl $rightJntTargetGrp`;


//cnt joint--------------------------------------------------------------

$centerJntCtrl = TAribbonCurve($prefix+"_"+$side+"_"+$element+"_cnt_RBB_ctrl",1.4,6);
$centerJntCtrlGrp = `group -n ($centerJntCtrl+"Grp") $centerJntCtrl`;
$centerJntCtrlzGrp = `group -n ($centerJntCtrl+"zGrp") $centerJntCtrlGrp`;

delete(`parentConstraint $centerJntGrp $centerJntCtrlGrp`);

$centerJntCtrlConstraint = `parentConstraint $centerJntCtrl $centerJntGrp`;

parentConstraint ($centerJntAimGrp) ($centerJntCtrlGrp);


//organized controls--------------------------------------------------------------

parent $leftJntCtrlzGrp $allCtrlPlaneGrp;
parent $rightJntCtrlzGrp $allCtrlPlaneGrp;
parent $centerJntCtrlzGrp $allCtrlPlaneGrp;

//constraint joint plane most top group to ctrl grp

parentConstraint -mo $leftJntzGrp $leftJntCtrlzGrp;
parentConstraint -mo $rightJntzGrp $rightJntCtrlzGrp;
parentConstraint -mo $centerJntTargetGrp $centerJntCtrlzGrp;

//////////////////////////////////////////////////////////////////////////////////////////////

//bindskin ctrl plane joint to plane--------------------------------------------------------------

$skinNode = `skinCluster -tsb $leftJoint $rightJoint $centerJoint $ribbonPlane[0]`;


//set weight to vertices

skinPercent 	-transformValue $leftJoint 0.8 
				-transformValue $centerJoint 0.2 
				$skinNode[0] 
				($ribbonPlane[0]+".cv[0:1][1]") ;

skinPercent 	-transformValue $leftJoint 0.5 
				-transformValue $centerJoint 0.5 
				$skinNode[0]
				($ribbonPlane[0]+".cv[0:1][2]") ;

skinPercent 	-transformValue $leftJoint 0.1 
				-transformValue $rightJoint 0.1 
				-transformValue $centerJoint 0.8 		
				$skinNode[0]
				($ribbonPlane[0]+".cv[0:1][3]") ;

skinPercent 	-transformValue $leftJoint 0.1 
				-transformValue $rightJoint 0.1 
				-transformValue $centerJoint 0.8 
				$skinNode[0]
				($ribbonPlane[0]+".cv[0:1][4]") ;

skinPercent 	-transformValue $rightJoint 0.5 
				-transformValue $centerJoint 0.5 
				$skinNode[0]
				($ribbonPlane[0]+".cv[0:1][5]") ;

skinPercent 	-transformValue $rightJoint 0.8 
				-transformValue $centerJoint 0.2 
				$skinNode[0]
				($ribbonPlane[0]+".cv[0:1][6]") ;


//--------------------------------------------------------------
//add ribbon attributes
//TA_lib.mel
//--------------------------------------------------------------


TA_addAttr($centerJntCtrl, "up_twist",0,0,0,0);
TA_addAttr($centerJntCtrl, "mid_twist",0,0,0,0);
TA_addAttr($centerJntCtrl, "low_twist",0,0,0,0);
TA_addAttr($centerJntCtrl, "auto_squash",1,0,0,1);
TA_addAttr($centerJntCtrl, "squash",1,0,-10,10);
TA_addAttr($centerJntCtrl, "details",1,1,0,1);


return $ribbonName;
}


//-----------------------------------------------------------
//Create Ribbon Control Procedure
//TA_ribbonCurve("name",1,1);
//---------------------------------------------------------

global proc string TAribbonCurve(string $name, float $size, int $color)
{
$curve = `curve -n ($name) -d 1 -p 0 1 1 -p 0 1 -1 -p 0 -1 -1 -p 0 -1 1 -p 0 1 1 -k 0 -k 1 -k 2 -k 3 -k 4` ;
$curveShape = `listRelatives -s $curve`;

xform -ws -s $size $size $size ($curve+".cv[0:4]");

////////////////////////////////
//TA_cc from TA.lib.mel
///////////////////////////

TA_cc($curve, $color);
return $curve;
}
