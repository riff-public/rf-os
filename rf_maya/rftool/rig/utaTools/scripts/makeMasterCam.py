import maya.cmds as mc

allShots = mc.ls(type = 'shot')
cams = []
shots = []

camInfo = {}
masterCam = 'master_cam'

for i in range(len(allShots)) : 
	incomingConnection = mc.listConnections(allShots[i], p = True)
	camShape = incomingConnection[-1].split('.')[0]
	camTranform = mc.listRelatives(camShape, p = True)[0]
	cams.append(camTranform)
	shots.append(allShots[i])
	camInfo[camTranform] = allShots[i]

if not mc.objExists('master_cam') : 
	cam = mc.camera()
	masterCam = mc.rename(cam[0], 'master_cam')



if mc.objExists('masterConstraint') : 
	mc.delete('masterConstraint')

masterCamShape = mc.listRelatives(masterCam, s = True)	
exp = '%s.focalLength = ' % masterCamShape [0]
lastFrame = 0

for i in range(len(shots)) : 
	endFrame = mc.getAttr('%s.endFrame' % shots[i])
	if endFrame > lastFrame : 
		lastFrame = endFrame
	
for i in range(len(cams)) : 		
	constraint = mc.parentConstraint(cams[i], masterCam, n = 'masterConstraint')
	startFrame = mc.getAttr('%s.startFrame' % shots[i])
	endFrame = mc.getAttr('%s.endFrame' % shots[i])

	
	if not lastFrame == endFrame : 
		mc.setKeyframe(constraint, at = ('w%s' % i), v = 1, t = [startFrame, endFrame])
		mc.setKeyframe(constraint, at = ('w%s' % i), v = 0, t = [startFrame - 1, endFrame + 1])
	else : 
		mc.setKeyframe(constraint, at = ('w%s' % i), v = 1, t = [startFrame, endFrame])
		mc.setKeyframe(constraint, at = ('w%s' % i), v = 0, t = [startFrame - 1])		
	
	camShape = mc.listRelatives(cams[i])
	
	if i == len(cams) - 1 :
		exp += '(%s.focalLength * %s.w%s)' % (camShape[0], constraint[0], i)		
	else : 
		exp += '(%s.focalLength * %s.w%s) + ' % (camShape[0], constraint[0], i)
	
if mc.objExists('masterCam_exp') : 
	mc.delete('masterCam_exp')
	
mc.expression(s = '%s;' % exp, n = 'masterCam_exp')	