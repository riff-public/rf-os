import maya.cmds as mc
import maya.mel as mm
mm.eval('source TA_lib.mel')
mm.eval('source TA_autorigLib.mel')
# axis 
# +X
# -X
# +Y
# -Y
# +Z
# -Z


def ribbonInput(obj1, obj2, prefix, side, element, choiceAxis) : 
	loc1 = mc.spaceLocator(p = [0, 0, 0])
	loc2 = mc.spaceLocator(p = [0, 0, 0])
	distanceNode = mc.createNode('distanceBetween')
	mc.connectAttr('%s.translate' % loc1[0], '%s.point1' % distanceNode, f = True)
	mc.connectAttr('%s.translate' % loc2[0], '%s.point2' % distanceNode, f = True)
	
	mc.delete(mc.pointConstraint(obj1, loc1[0]))
	mc.delete(mc.pointConstraint(obj2, loc2[0]))
	distance = mc.getAttr('%s.distance' % distanceNode)

	ribbon = taRibbonCmd(prefix, side, element, choiceAxis, distance)
	
	mc.delete(mc.pointConstraint(obj1, ribbon[1]))
	mc.delete(mc.aimConstraint(obj2, ribbon[1]))
	mc.parentConstraint(obj1, ribbon[1], mo = True)
	
	

def taRibbonCmd(prefix = 'gen', side = 'cnt', element = 'gen', choiceAxis = '+x', distance = 5.0) : 
	#ribbonName = ''
	#prefix = 'char'
	#side = 'lf'
	#element = 'arm'
	#choiceAxis = '+x'
	#distance = 5.0
	subJointPma = []
	subCtrlTopGrp = []
	subSquashPma = []
	ribbonInfo = []
	
	if choiceAxis == '+x' : 
		axis = [0, 1, 0]
		rotValue = [-90, -90, 0]
		aimVector = [1, 0, 0]
		upVector = [0, 1, 0]
		upGrp = [0, 1, 0]
		lfJntPos = [-distance/2, 0, 0]
		rtJntPos = [distance/2, 0, 0]
		lfAimVector = [1, 0, 0]
		rtAimVector = [-1, 0, 0]
		twistAxis = 'rotateX'
		squashAxis = ['scaleY', 'scaleZ']
		curveAxis = [0, 0, 0]
		
	if choiceAxis == '+y' : 
		axis = [0, 0, 0]
		rotValue = [0, 0, 0]
		aimVector = [0, 1, 0]
		upVector = [0, 0, 1]
		upGrp = [0, 0, 1]
		lfJntPos = [0, -distance/2, 0]
		rtJntPos = [0, distance/2, 0]
		lfAimVector = [0, 1, 0]
		rtAimVector = [0, -1, 0]
		twistAxis = 'rotateY'
		squashAxis = ['scaleX', 'scaleZ']
		curveAxis = [0, 0, 90]
		
	if choiceAxis == '+z' : 
		axis = [0, 1, 0]
		rotValue = [-90, 180, 0]
		aimVector = [0, 0, 1]
		upVector = [0, 1, 0]
		upGrp = [0, 1, 0]
		lfJntPos = [0, 0, -distance/2]
		rtJntPos = [0, 0, distance/2,]
		lfAimVector = [0, 0, 1]
		rtAimVector = [0, 0, -1]
		twistAxis = 'rotateZ'
		squashAxis = ['scaleX', 'scaleY']
		curveAxis = [0, 90, 0]
		
	if choiceAxis == '-x' : 
		axis = [0, 1, 0]
		rotValue = [-90, 90, 0]
		aimVector = [-1, 0, 0]
		upVector = [0, 1, 0]
		upGrp = [0, 1, 0]
		lfJntPos = [distance/2, 0, 0]
		rtJntPos = [-distance/2, 0, 0]
		lfAimVector = [-1, 0, 0]
		rtAimVector = [1, 0, 0]
		twistAxis = 'rotateX'
		squashAxis = ['scaleY', 'scaleZ']
		curveAxis = [0, 0, 0]
		
	if choiceAxis == '-y' : 
		axis = [0, 0, 0]
		rotValue = [0, 0, 180]
		aimVector = [0, -1, 0]
		upVector = [0, 0, 1]
		upGrp = [0, 0, 1]
		lfJntPos = [0, distance/2, 0]
		rtJntPos = [0, -distance/2, 0]
		lfAimVector = [0, -1, 0]
		rtAimVector = [0, 1, 0]
		twistAxis = 'rotateY'
		squashAxis = ['scaleX', 'scaleZ']
		curveAxis = [0, 0, 90]

	if choiceAxis == '-z' : 
		axis = [0, 1, 0]
		rotValue = [-90, 0, 0]
		aimVector = [0, 0, -1]
		upVector = [0, 1, 0]
		upGrp = [0, 1, 0]
		lfJntPos = [0, 0, distance/2]
		rtJntPos = [0, 0, -distance/2]
		lfAimVector = [0, 0, -1]
		rtAimVector = [0, 0, 1]
		twistAxis = 'rotateZ'
		squashAxis = ['scaleX', 'scaleY']
		curveAxis = [0, 90, 0]

	# create nurbsPlane ================================================
	ribbonPlane = mc.nurbsPlane(n = '%s_%s_%sRibbonPlane_nrb' % (prefix, side, element), p = [0, 0, 0], ax = [0, 0, 0], w = 1, lr = 1, d = 3, u = 1, v = 5, ch = 0) 

	mc.xform(ribbonPlane[0], ws = True, ro = rotValue)
	mc.xform(ribbonPlane[0], ws = True, s  = [1, distance, 1])
	mc.makeIdentity(ribbonPlane[0], apply = True, t = 1, r = 1, s = 1, n = 0)
	mc.rebuildSurface(ribbonPlane[0], ch = 0, rpo = 1, rt = 0, end = 1, kr = 2, kcp = 0, kc = 0, su = 1, du = 1, sv = 5, dv = 3, tol = 0.0001, fr = 0,  dir = 2)

	# group
	ribbonPlaneGrp = mc.group(ribbonPlane[0], n = '%s_%s_ribbonPlaneNrb_Grp' % (prefix, element)) 
	ribbonSubGrp = mc.group(em = True, n = '%s_%s_ribbonElement_Grp' % (prefix, element))
	ribbonSubJntGrp = mc.group(em = True, n = '%s_%s_ribbonSubJnt_Grp' % (prefix, element))
	ribbonSubCtrlGrp = mc.group(em = True, n = '%s_%s_ribbonSubCtrl_Grp' % (prefix, element))

	ribbonMainGrp = mc.group(em = True, n = '%s_%s_mainRibbon_Grp' % (prefix, element))


	# parent subGrp 
	mc.parent(ribbonSubCtrlGrp, ribbonSubJntGrp, ribbonSubGrp)

	# constraint scale main group to sub ctrl group
	mc.scaleConstraint(ribbonMainGrp, ribbonSubCtrlGrp)
	
	# loop for ribbon ===========================================================================================
	for i in range(5) : 
	
		# create joint
		joint = mc.createNode('joint', n = '%s_%s_%sJoint%s_jnt' % (prefix, side, element, i))
		jointGrp = mc.group(joint, n = '%s_%s_%sJoint%s_Grp' % (prefix, side, element, i))
		jointTwistGrp = mc.group(jointGrp, n = '%s_%s_%sJoint%s_twistGrp' % (prefix, side, element, i))
		jointCtrlCSGrp = mc.group(jointTwistGrp, n = '%s_%s_%sJoint%s_ctrlGrp' % (prefix, side, element, i))
		jointAimGrp = mc.group(jointCtrlCSGrp, n = '%s_%s_%sJoint%s_aimGrp' % (prefix, side, element, i))

		# put in the group
		mc.parent(jointAimGrp, ribbonSubJntGrp)
		
		# make joint stick to the surface
		poiNode = mc.createNode('pointOnSurfaceInfo', n = '%s_%s_%sJoint%s_poi' % (prefix, side, i, element))
		
		# connect surface to node
		mc.connectAttr('%s.worldSpace[0]' % ribbonPlane[0], '%s.inputSurface' % poiNode, f = True)
	
		# connect node to joint translation
		mc.connectAttr('%s.position' % poiNode, '%s.translate' % jointAimGrp, f = True)
		
		# set poi node parameter to center each face
		mc.setAttr('%s.parameterU' % poiNode, 0.5)
		mc.setAttr('%s.parameterV' % poiNode, 0.5 + i)
		
		# make joint rotate along nurbs plane
		aimNode = mc.createNode('aimConstraint', n = '%s_%s_%sJoint%s_aimNode' % (prefix, side, element, i))

		# put aim node in the group
		mc.parent(aimNode, ribbonSubJntGrp)
		
		# connect up vector 
		mc.connectAttr('%s.result.normal' % poiNode, '%s.worldUpVector' % aimNode, f = True)
		
		# connect tangentV to aimNode.tg[0].tt
		mc.connectAttr('%s.tangentV' % poiNode, '%s.tg[0].tt' % aimNode, f = True)
		
		# connect aim node to joint.rotate
		mc.connectAttr('%s.constraintRotate' % aimNode, '%s.rotate' % jointAimGrp, f = True)
		
		# set up axis
		mc.setAttr('%s.aimVectorX' % aimNode, aimVector[0])
		mc.setAttr('%s.aimVectorY' % aimNode, aimVector[1])
		mc.setAttr('%s.aimVectorZ' % aimNode, aimVector[2])
	
		mc.setAttr('%s.upVectorX' % aimNode, upVector[0])
		mc.setAttr('%s.upVectorY' % aimNode, upVector[1])
		mc.setAttr('%s.upVectorZ' % aimNode, upVector[2])
		
		# create curve ctrl
		curve = makeCtrl(13, 1)
		curve = mc.rename(curve, '%s_%s_%sJoint%s_ctrl' % (prefix, side, element, i))
		xCurve(curve, 'rotate', curveAxis[0], curveAxis[1], curveAxis[2])
		curveGrp = mc.group(curve, n = '%s_%s_%sJoint%sCtrl_Grp' % (prefix, side, element, i))
		curveTopGrp = mc.group(curveGrp, n = '%s_%s_%sJoint%sCtrl_TopGrp' % (prefix, side, element, i))
		subCtrlTopGrp.append(curveTopGrp)

		# add squash attribute
		squashAttr = mm.eval('TA_addAttr("%s", "squash", 0, 0, 0, 10);' % curve)

		# add pma to scale joint
		squashPma = addPMA('%s.%s' % (jointGrp, squashAxis[0]), '%s_%s_%sSquash%s_pma' % (prefix, side, element, i), 3)
		mc.connectAttr('%s.output1D' % squashPma, '%s.%s' % (jointGrp, squashAxis[1]), f = True)
		mc.setAttr('%s.input3' % squashPma, 1)

		subSquashPma.append(squashPma)



		# add mdv to squash
		squashMdv = genConnect(squashAttr, '%s.input1' % squashPma, '%s_%s_%ssquash%s' % (prefix, side, element, i), 0.1, 'shape', '')


		# put ctrl in the group
		mc.parent(curveTopGrp, ribbonSubCtrlGrp)
		
		# move ctrl to each joint
		mc.delete(mc.parentConstraint(jointAimGrp, curveTopGrp))
		
		# constraint ctrl to joint
		mc.parentConstraint(curve, jointCtrlCSGrp)
		mc.scaleConstraint(curve, jointCtrlCSGrp)
		
		# make ctrl follow plane
		mc.parentConstraint(jointAimGrp, curveTopGrp)

		# add PMA 
		subJointPma.append(addPMA('%s.%s' % (jointTwistGrp, twistAxis), '%s_joint%sTwist_pma' % (side, i), 4))
		
		
	# create left joint =================================================
	lfJoint = mc.createNode('joint', n = '%s_%s_lf_ribbon_jnt' % (prefix, element))
	lfJointGrp = mc.group(lfJoint, n = '%s_%s_lf_ribbon_Grp' % (prefix, element))
	lfJointAimGrp = mc.group(lfJointGrp, n = '%s_%s_lf_ribbon_aimGrp' % (prefix, element))
	lfJointTargetGrp = mc.group(lfJointAimGrp, n = '%s_%s_lf_ribbon_targetGrp' % (prefix, element))
	lfJointTopGrp = mc.group(lfJointTargetGrp, n = '%s_%s_lf_ribbon_topGrp' % (prefix, element))
	
	# up group for left joint 
	lfJointUpGrp = mc.group(em = True, n = '%s_%s_lf_ribbonJnt_upGrp' % (prefix, element))
	mc.parent(lfJointUpGrp, lfJointTargetGrp)
	
	# move up group up
	mc.xform(lfJointUpGrp, ws = True, t = upGrp)
	
	# move left joint to left of the plane
	mc.xform(lfJointTopGrp, ws = True, t = lfJntPos)
	
	# create right joint =================================================
	rtJoint = mc.createNode('joint', n = '%s_%s_rt_ribbon_jnt' % (prefix, element))
	rtJointGrp = mc.group(rtJoint, n = '%s_%s_rt_ribbon_Grp' % (prefix, element))
	rtJointAimGrp = mc.group(rtJointGrp, n = '%s_%s_rt_ribbon_aimGrp' % (prefix, element))
	rtJointTargetGrp = mc.group(rtJointAimGrp, n = '%s_%s_rt_ribbon_targetGrp' % (prefix, element))
	rtJointTopGrp = mc.group(rtJointTargetGrp, n = '%s_%s_rt_ribbon_topGrp' % (prefix, element))	
	
	# up group for right joint 
	rtJointUpGrp = mc.group(em = True, n = '%s_%s_rt_ribbonJnt_upGrp' % (prefix, element))
	mc.parent(rtJointUpGrp, rtJointTargetGrp)
	
	# move up group up
	mc.xform(rtJointUpGrp, ws = True, t = upGrp)
	
	# move right joint to right of the plane
	mc.xform(rtJointTopGrp, ws = True, t = rtJntPos)
	
	# create center joint =================================================
	cntJoint = mc.createNode('joint', n = '%s_%s_cnt_ribbon_jnt' % (prefix, element))
	cntJointGrp = mc.group(cntJoint, n = '%s_%s_cnt_ribbon_Grp' % (prefix, element))
	cntJointCSGrp = mc.group(cntJointGrp, n = '%s_%s_cnt_ribbon_CSGrp' % (prefix, element))
	cntJointAimGrp = mc.group(cntJointCSGrp, n = '%s_%s_cnt_ribbon_aimGrp' % (prefix, element))
	cntJointPointGrp = mc.group(cntJointAimGrp, n = '%s_%s_cnt_ribbon_targetGrp' % (prefix, element))
	cntJointTopGrp = mc.group(cntJointPointGrp, n = '%s_%s_cnt_ribbon_topGrp' % (prefix, element))
	
	# up group for right joint 
	cntJointUpGrp = mc.group(em = True, n = '%s_%s_cnt_ribbonJnt_upGrp' % (prefix, element))
	mc.parent(cntJointUpGrp, cntJointPointGrp)
	
	# move up group up
	mc.xform(cntJointUpGrp, ws = True, t = upGrp)
	
	# aim ===============================================================================
	lfJntAimNode = mc.aimConstraint(rtJointTargetGrp, lfJointAimGrp, offset = [0, 0, 0], weight = 1, aimVector = lfAimVector, worldUpType = 'object', worldUpObject = lfJointUpGrp)
	rtJntAimNode = mc.aimConstraint(lfJointTargetGrp, rtJointAimGrp, offset = [0, 0, 0], weight = 1, aimVector = rtAimVector, worldUpType = 'object', worldUpObject = rtJointUpGrp)
	cntJntAimNode = mc.aimConstraint(lfJointTargetGrp, cntJointAimGrp, offset = [0, 0, 0], weight = 1, aimVector = rtAimVector, worldUpType = 'object', worldUpObject = cntJointUpGrp)
	
	
	# constraint point center joint between left and right joint ===========================
	cntJointPointNode = mc.pointConstraint(lfJointTargetGrp, rtJointTargetGrp, cntJointPointGrp)
	
	# create ctrl
	innerCtrl = makeCtrl(17, 1.5)
	innerCtrl = mc.rename(innerCtrl, '%s_%s_inner_ctrl' % (prefix, element))
	xCurve(innerCtrl, 'rotate', curveAxis[0], curveAxis[1], curveAxis[2])
	innerCtrlGrp = mc.group(innerCtrl, n = '%s_%s_innerCtrl_Grp' % (prefix, element))
	innerCtrlTopGrp = mc.group(innerCtrlGrp, n = '%s_%s_innerCtrl_topGrp' % (prefix, element))
	
	outterCtrl = makeCtrl(17, 1.5)
	outterCtrl = mc.rename(outterCtrl, '%s_%s_outter_ctrl' % (prefix, element))
	xCurve(outterCtrl, 'rotate', curveAxis[0], curveAxis[1], curveAxis[2])
	outterCtrlGrp = mc.group(outterCtrl, n = '%s_%s_outterCtrl_Grp' % (prefix, element))
	outterCtrlTopGrp = mc.group(outterCtrlGrp, n = '%s_%s_outterCtrl_topGrp' % (prefix, element))
	
	middleCtrl = makeCtrl(17, 1.5)
	middleCtrl = mc.rename(middleCtrl, '%s_%s_mid_ctrl' % (prefix, element))
	xCurve(middleCtrl, 'rotate', curveAxis[0], curveAxis[1], curveAxis[2])
	middleCtrlGrp = mc.group(middleCtrl, n = '%s_%s_midCtrl_Grp' % (prefix, element))
	middleCtrlTopGrp = mc.group(middleCtrlGrp, n = '%s_%s_midCtrl_topGrp' % (prefix, element))
	middleCtrlShape = mc.listRelatives(middleCtrl, s = True)
	
	# move all ctrl to each joint
	mc.delete(mc.parentConstraint(lfJointTopGrp, innerCtrlTopGrp))
	mc.delete(mc.parentConstraint(rtJointTopGrp, outterCtrlTopGrp))
	mc.delete(mc.parentConstraint(cntJointTopGrp, middleCtrlTopGrp))
	
	# constraint each ctrl
	mc.parentConstraint(innerCtrl, lfJointTopGrp)
	mc.parentConstraint(outterCtrl, rtJointTopGrp)
	mc.parentConstraint(middleCtrl, cntJointCSGrp)
	
	mc.parentConstraint(cntJointAimGrp, middleCtrlTopGrp)
	
	# bind skin nurbs plane
	skinNode = mc.skinCluster(lfJoint, rtJoint, cntJoint, ribbonPlane[0], tsb = True)

	# weight plane
	mc.skinPercent(skinNode[0], '%s.cv[0:1][1]' % ribbonPlane[0], transformValue = [(lfJoint, 0.8), (cntJoint, 0.2)])
	mc.skinPercent(skinNode[0], '%s.cv[0:1][2]' % ribbonPlane[0], transformValue = [(lfJoint, 0.5), (cntJoint, 0.5)])
	mc.skinPercent(skinNode[0], '%s.cv[0:1][3]' % ribbonPlane[0], transformValue = [(lfJoint, 0.1), (cntJoint, 0.8), (rtJoint, 0.1)])
	mc.skinPercent(skinNode[0], '%s.cv[0:1][4]' % ribbonPlane[0], transformValue = [(lfJoint, 0.1), (cntJoint, 0.8), (rtJoint, 0.1)])
	mc.skinPercent(skinNode[0], '%s.cv[0:1][5]' % ribbonPlane[0], transformValue = [(rtJoint, 0.5), (cntJoint, 0.5)])
	mc.skinPercent(skinNode[0], '%s.cv[0:1][6]' % ribbonPlane[0], transformValue = [(rtJoint, 0.8), (cntJoint, 0.2)])

	# organizing group

	mc.parent(lfJointTopGrp, ribbonMainGrp)
	mc.parent(rtJointTopGrp, ribbonMainGrp)
	mc.parent(cntJointTopGrp, ribbonMainGrp)

	mc.parent(innerCtrlTopGrp, ribbonMainGrp)
	mc.parent(outterCtrlTopGrp, ribbonMainGrp)
	mc.parent(middleCtrlTopGrp, ribbonMainGrp)

	# group all
	ribbonAllGrp = mc.group(em = True, n = '%s_%s_ribbonAll_Grp' % (prefix, element))

	mc.parent(ribbonSubGrp, ribbonAllGrp)
	mc.parent(ribbonPlaneGrp, ribbonAllGrp)
	mc.parent(ribbonMainGrp, ribbonAllGrp)

	# get inner joint pivot 
	innerJntPivot = mc.xform(lfJoint, q = True, ws = True, t = True)
	mc.xform(ribbonMainGrp, os = True, piv = [innerJntPivot[0], innerJntPivot[1], innerJntPivot[2]])

	# add attribute
	upTwist = mm.eval('TA_addAttr("%s", "up_twist", 0, 0, 0, 10);' % middleCtrl)
	midTwist = mm.eval('TA_addAttr("%s", "mid_twist", 0, 0, 0, 10);' % middleCtrl)
	lowTwist = mm.eval('TA_addAttr("%s", "low_twist", 0, 0, 0, 10);' % middleCtrl)
	autoTwist = mm.eval('TA_addAttr("%s", "auto_twist", 1, 0, 0, 1);' % middleCtrl)
	autoSquash = mm.eval('TA_addAttr("%s", "auto_squash", 1, 0, 0, 1);' % middleCtrl)
	squash = mm.eval('TA_addAttr("%s", "squash", 0, 0, 0, 10);' % middleCtrl)
	details = mm.eval('TA_addAttr("%s", "details", 1, 0, 0, 1);' % middleCtrl)

	twistValue = mm.eval('TA_addAttr("%s", "auto_twist", 0, 0, 1, 10);' % middleCtrlShape[0])

	# connect mdv
	upTwistMdv = addMdv(upTwist, '%s_%s_upTwist' % (prefix, element), 10, 'shape')
	midTwistMdv = addMdv(midTwist, '%s_%s_midTwist' % (prefix, element), 10, 'shape')
	lowTwistMdv = addMdv(lowTwist, '%s_%s_lowTwist' % (prefix, element), 10, 'shape')
	squashMdv = addMdv(squash, '%s_%s_squash' % (prefix, element), 10, 'shape')
	twistValueMdv = addMdv(twistValue, '%s_%s_twistValue' % (prefix, element), 10, 'self')

	# connect details to subCtrlTopGrp
	mc.connectAttr(details, '%s.visibility' % subCtrlTopGrp[0], f = True)
	mc.connectAttr(details, '%s.visibility' % subCtrlTopGrp[1], f = True)
	mc.connectAttr(details, '%s.visibility' % subCtrlTopGrp[2], f = True)
	mc.connectAttr(details, '%s.visibility' % subCtrlTopGrp[3], f = True)
	mc.connectAttr(details, '%s.visibility' % subCtrlTopGrp[4], f = True)

	# connect squash to sub squash
	mc.connectAttr('%s.outputX' % squashMdv, '%s.input2' % subSquashPma[0], f = True)
	mc.connectAttr('%s.outputX' % squashMdv, '%s.input2' % subSquashPma[1], f = True)
	mc.connectAttr('%s.outputX' % squashMdv, '%s.input2' % subSquashPma[2], f = True)
	mc.connectAttr('%s.outputX' % squashMdv, '%s.input2' % subSquashPma[3], f = True)
	mc.connectAttr('%s.outputX' % squashMdv, '%s.input2' % subSquashPma[4], f = True)


	

	# twist section ==================================================================
	# connect multiply divide

	genConnect('%s.outputX' % upTwistMdv, '%s.input1' % subJointPma[0], '%s_%s_upTwistJnt0' % (prefix, element), 0.0, 'custom', middleCtrlShape[0])
	genConnect('%s.outputX' % upTwistMdv, '%s.input1' % subJointPma[1], '%s_%s_upTwistJnt1' % (prefix, element), 0.25, 'custom', middleCtrlShape[0])
	genConnect('%s.outputX' % upTwistMdv, '%s.input1' % subJointPma[2], '%s_%s_upTwistJnt2' % (prefix, element), 0.5, 'custom', middleCtrlShape[0])
	genConnect('%s.outputX' % upTwistMdv, '%s.input1' % subJointPma[3], '%s_%s_upTwistJnt3' % (prefix, element), 0.75, 'custom', middleCtrlShape[0])
	genConnect('%s.outputX' % upTwistMdv, '%s.input1' % subJointPma[4], '%s_%s_upTwistJnt4' % (prefix, element), 1.0, 'custom', middleCtrlShape[0])

	genConnect('%s.outputX' % midTwistMdv, '%s.input2' % subJointPma[0], '%s_%s_midTwistJnt0' % (prefix, element), 0.1, 'custom', middleCtrlShape[0])
	genConnect('%s.outputX' % midTwistMdv, '%s.input2' % subJointPma[1], '%s_%s_midTwistJnt1' % (prefix, element), 0.5, 'custom', middleCtrlShape[0])
	genConnect('%s.outputX' % midTwistMdv, '%s.input2' % subJointPma[2], '%s_%s_midTwistJnt2' % (prefix, element), 1.0, 'custom', middleCtrlShape[0])
	genConnect('%s.outputX' % midTwistMdv, '%s.input2' % subJointPma[3], '%s_%s_midTwistJnt3' % (prefix, element), 0.5, 'custom', middleCtrlShape[0])
	genConnect('%s.outputX' % midTwistMdv, '%s.input2' % subJointPma[4], '%s_%s_midTwistJnt4' % (prefix, element), 0.1, 'custom', middleCtrlShape[0])

	genConnect('%s.outputX' % lowTwistMdv, '%s.input3' % subJointPma[0], '%s_%s_lowTwistJnt0' % (prefix, element), 1.0, 'custom', middleCtrlShape[0])
	genConnect('%s.outputX' % lowTwistMdv, '%s.input3' % subJointPma[1], '%s_%s_lowTwistJnt1' % (prefix, element), 0.75, 'custom', middleCtrlShape[0])
	genConnect('%s.outputX' % lowTwistMdv, '%s.input3' % subJointPma[2], '%s_%s_lowTwistJnt2' % (prefix, element), 0.5, 'custom', middleCtrlShape[0])
	genConnect('%s.outputX' % lowTwistMdv, '%s.input3' % subJointPma[3], '%s_%s_lowTwistJnt3' % (prefix, element), 0.25, 'custom', middleCtrlShape[0])
	genConnect('%s.outputX' % lowTwistMdv, '%s.input3' % subJointPma[4], '%s_%s_lowTwistJnt4' % (prefix, element), 0.0, 'custom', middleCtrlShape[0])

	genConnect('%s.outputX' % twistValueMdv, '%s.input4' % subJointPma[0], '%s_%s_autoTwistJnt0' % (prefix, element), 0.0, 'custom', middleCtrlShape[0])
	genConnect('%s.outputX' % twistValueMdv, '%s.input4' % subJointPma[1], '%s_%s_autoTwistJnt1' % (prefix, element), 0.25, 'custom', middleCtrlShape[0])
	genConnect('%s.outputX' % twistValueMdv, '%s.input4' % subJointPma[2], '%s_%s_autoTwistJnt2' % (prefix, element), 0.5, 'custom', middleCtrlShape[0])
	genConnect('%s.outputX' % twistValueMdv, '%s.input4' % subJointPma[3], '%s_%s_autoTwistJnt3' % (prefix, element), 0.75, 'custom', middleCtrlShape[0])
	genConnect('%s.outputX' % twistValueMdv, '%s.input4' % subJointPma[4], '%s_%s_autoTwistJnt4' % (prefix, element), 1.0, 'custom', middleCtrlShape[0])

	ribbonInfo.append(ribbonAllGrp)
	ribbonInfo.append(ribbonMainGrp)
	
	return ribbonInfo



def makeCtrl(color, size) : 
	curve = mc.curve(d = 1, p = [(0, -1, -1), (0, 1, -1), (0, 1, 1), (0, -1, 1), (0, -1, -1)], k = [0, 1, 2, 3, 4])
	curveShape = mc.listRelatives(curve, s = True)
	mc.setAttr('%s.overrideEnabled' % curveShape[0], 1)
	mc.setAttr('%s.overrideColor' % curveShape[0], color)
	
	spans = mc.getAttr('%s.spans' % curveShape[0])
	mc.xform('%s.cv[0:%s]' % (curveShape[0], spans), ws = True, s = [size, size, size])
	return curve


def genConnect(src, dst, nodeName, default, amp, custom) : 
	srcShape = mc.listRelatives(src.split('.')[0], s = True)
	srcTransform = src.split('.')[0]
	mdv = mc.createNode('multiplyDivide', n = '%s_mdv' % nodeName)
	mc.connectAttr(src, '%s.input1X' % mdv, f = True)
	mc.connectAttr('%s.outputX' % mdv, dst, f = True)

	if amp == 'shape' : 
		attr = mm.eval('TA_addAttr("%s", "%s_amp", 0, 0, 0, 10);' % (srcShape[0], nodeName))

	if amp == 'self' : 
		attr = mm.eval('TA_addAttr("%s", "%s_amp", 0, 0, 0, 10);' % (srcTransform, nodeName))

	if amp == 'custom' : 
		attr = mm.eval('TA_addAttr("%s", "%s_amp", 0, 0, 0, 10);' % (custom, nodeName))

	mc.connectAttr(attr, '%s.input2X' % mdv, f = True)
	mc.setAttr(attr, default)
	return mdv
	
def addPMA(src, nodeName, channel) : 
	pma = mm.eval('taPMA("%s", %s)' % (nodeName, channel))
	mc.connectAttr('%s.output1D' % pma, src, f = True)
	return pma
	

def addMdv(src, nodeName, default, amp) : 
	srcShape = mc.listRelatives(src.split('.')[0], s = True)
	srcTransform = src.split('.')[0]
	mdv = mc.createNode('multiplyDivide', n = '%s_mdv' % nodeName)
	mc.connectAttr(src, '%s.input1X' % mdv, f = True)

	if amp == 'shape' : 
		attr = mm.eval('TA_addAttr("%s", "%s_amp", 0, 0, 0, 10);' % (srcShape[0], nodeName))

	if amp == 'self' : 
		attr = mm.eval('TA_addAttr("%s", "%s_amp", 0, 0, 0, 10);' % (srcTransform, nodeName))

	mc.connectAttr(attr, '%s.input2X' % mdv, f = True)
	mc.setAttr(attr, default)

	return mdv


def xCurve(curve, type, x, y, z) : 
	curveShape = mc.listRelatives(curve, s = True)
	spans = mc.getAttr('%s.spans' % curveShape[0])

	if type == 'scale' : 
		mc.xform('%s.cv[0:%s]' % (curve, spans), ws = True, s = [x, y, z])

	if type == 'rotate' : 
		mc.xform('%s.cv[0:%s]' % (curve, spans), ro = [x, y, z])

	if type == 'translate' : 
		mc.xform('%s.cv[0:%s]' % (curve, spans), r = True, t = [x, y, z])
	
ribbonInput('lf_upArm_jnt', 'lf_loArm_jnt', 'gen', 'lf', 'upArm', '+x')