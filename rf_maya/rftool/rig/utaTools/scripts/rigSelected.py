import maya.cmds as mc 
def rigSelected() : 
	ls = mc.ls(sl = True)
	for each in ls : 
		ctrl = mc.circle(c = [0, 0, 0], nr = [0, 180, 0], r = 2)
		ctrl = mc.rename(ctrl[0], '%s_ctrl' % each)
		ctrlGrp = mc.group(ctrl, n = '%s_Grp' %each)
		mc.delete(mc.parentConstraint(each, ctrlGrp))
		mc.parentConstraint(ctrl, each)
		mc.scaleConstraint(ctrl, each, mo = True)
		mc.addAttr(ctrl, ln = 'viz', at = 'double', min = 0, max = 1, dv = 1)
		mc.setAttr('%s.viz' % ctrl, e = True, keyable = True)
		mc.connectAttr('%s.viz' % ctrl, '%s.visibility' % each)