import maya.cmds as mc
from functools import partial

def run() : 
	a = addBlendshapeTools()
	a.show()

#center prop
class addBlendshapeTools() : 

	def __init__(self) : 
		self.win = 'addBlendshapeToolsUI'
	
	def show(self) : 
		if(mc.window(self.win, exists = True)) : 
			mc.deleteUI(self.win)
			
		mc.window(self.win, t = 'export assets v2.2', wh = [300,200])
		mc.columnLayout(adj = 1, rs = 4)
		

		mc.optionMenu('%s_nodeOP' % self.win, l = 'Blendshape Node', cc = partial(self.updateTX))
		allNode = mc.ls(type = 'blendShape')
		for each in allNode : 
			mc.menuItem(l = each)
		
		mc.textField('%s_nodeTX' % self.win)
		mc.text(l = 'Index')
		mc.textField('%s_indexTX' % self.win)
		
		mc.checkBox('%s_inb_CB' % self.win, l = 'Inbetween', v = 0)
		#mc.button(l = 'Update', c = partial(self.findBlendshapeIndex))
		mc.button(l = 'Add', h = 30, c = partial(self.addBlendshapeCmd))
		mc.showWindow()
		mc.window(self.win, e = True, wh = [200, 148])
		self.updateTX()
		self.findBlendshapeIndex()

	def updateTX(self, arg = None) : 
		node = mc.optionMenu('%s_nodeOP' % self.win, q = True, v = True)
		mc.textField('%s_nodeTX' % self.win, e = True, tx = node)
		self.findBlendshapeIndex()
		
	def addBlendshapeCmd(self, arg = None) : 
		obj = mc.ls(sl = True) 
		base = obj[-1]
		bsNode = mc.textField('%s_nodeTX' % self.win, q = True, tx = True)
		index = int(mc.textField('%s_indexTX' % self.win, q = True, tx = True))
		inbetweenList = []
		
		if mc.checkBox('%s_inb_CB' % self.win, q = True, v = True) : 
			if len(obj) > 2 : 
				target = obj[-2]
				numObj = len(obj) 
				numTarget = numObj - 1 
				weight = (1 / float(numTarget))
				
				inbetweenList = obj[0:-2]
				mc.blendShape(bsNode, e = True, t = (base, index, target, 1))
				print 'base %s target %s \n' % (base, target)
				
				for i in range(len(inbetweenList)) : 
					mc.blendShape(bsNode, edit=True, ib=True, t=(base, index, inbetweenList[i], (weight*(i + 1))))
					print 'base %s target %s %s\n' % (base, inbetweenList[i], weight*(i + 1))
					
	def findBlendshapeIndex(self, arg = None) : 
		bsNode = mc.textField('%s_nodeTX' % self.win, q = True, tx = True)
		bsList = mc.listAttr('%s.w' % bsNode, m = True)
		mc.textField('%s_indexTX' % self.win, e = True, tx = len(bsList) + 1)
				
				
				


#a = addBlendshapeTools()
#a.findBlendshapeIndex()
#mc.ls(type = 'blendShape')

#mc.blendShape('chr_wolf_wonald01:Wonald_facial_bls', e = True, t = ('Wonald_head_geo', 52, 'Wonald_head_base', 1))

#mc.blendShape( 'chr_wolf_wonald01:Wonald_facial_bls', edit=True, ib=True, t=('Wonald_head_geo', 51, 'Wonald_head_base2', 0.4))

