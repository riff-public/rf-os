#peck make icon
import maya.cmds as mc
import maya.mel as mm


def makeIcon() :
	# Making icon for Chima
	width = 1280
	height = 720
	ratio = 16.0000/9.0000
	
	currWidth = mc.getAttr( 'defaultResolution.width' )
	currHeight = mc.getAttr( 'defaultResolution.height' )
	currRatio = mc.getAttr( 'defaultResolution.deviceAspectRatio' )
	
	mc.setAttr( 'defaultResolution.lockDeviceAspectRatio' , 1 )
	mc.setAttr('defaultRenderGlobals.currentRenderer', 'mentalRay', type = 'string')
	
	if currWidth == width and currHeight == height :
		
		mc.setAttr( 'defaultRenderQuality.shadingSamples' , 1 )
		mc.setAttr( 'defaultRenderGlobals.imageFormat' , 32 )
		
		currPath = mc.file( q=True , sn=True )
		charName = currPath.split( '/' )[ -1 ].split( '_' )[ 0 ]
		iconPath = '%simages/icon/%s.png' % ( currPath.split( 'dev' )[0] , charName )
		mm.eval('renderIntoNewWindow render;')
		mm.eval( 'renderWindowSaveImageCallback "renderView" "%s" "PNG"' % iconPath )
	else :
		mc.setAttr( 'defaultResolution.width' , width )
		mc.setAttr( 'defaultResolution.height' , height )
		mc.setAttr( 'defaultResolution.deviceAspectRatio' , ratio )