import maya.cmds as mc
import os 
import sys

def importRigGrp() : 
    if sys.platform == 'win32' : 
        # mc.file('Y:/USERS/Ta/rig/Rig_Grp.ma',  i = True, type = 'mayaAscii', options = 'v=0', pr = True, loadReferenceDepth = 'all')
        mc.file('O:/systemTool/template/maya/controls/Rig_Grp.ma',  i = True, type = 'mayaAscii', options = 'v=0', pr = True, loadReferenceDepth = 'all')


    if sys.platform == 'linux2' : 
        # mc.file('/playground/USERS/Ta/rig/Rig_Grp.ma',  i = True, type = 'mayaAscii', options = 'v=0', pr = True, loadReferenceDepth = 'all')
        mc.file('/dsPipe/systemTool/template/maya/controls/Rig_Grp.ma',  i = True, type = 'mayaAscii', options = 'v=0', pr = True, loadReferenceDepth = 'all')

    
def rigSelected() : 
	ls = mc.ls(sl = True)
	for each in ls : 
		ctrl = mc.circle(c = [0, 0, 0], nr = [0, 180, 0], r = 2)
		ctrl = mc.rename(ctrl[0], '%s_ctrl' % each)
		ctrlGrp = mc.group(ctrl, n = '%s_Grp' %each)
		mc.delete(mc.parentConstraint(each, ctrlGrp))
		mc.parentConstraint(ctrl, each)
		mc.scaleConstraint(ctrl, each, mo = True)