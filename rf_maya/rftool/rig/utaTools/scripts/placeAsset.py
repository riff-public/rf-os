import maya.cmds as mc 
from functools import partial
import sys
import os
import genScript 
#sys.path.append('P:\BKK\Animators_Playground\Ta\scripts')

def run() : 
	a = placeAsset()
	a.show()

#center prop
class placeAsset() : 

	def __init__(self) : 
		self.win = 'placeAssetUI'
		self.look = 'SuperRoot_Ctrl'
		self.replace = ''
	
	def show(self) : 
		if(mc.window(self.win, exists = True)) : 
			mc.deleteUI(self.win)
			
		mc.window(self.win, t = 'placeAssetUI v1.0', wh = [300,200])
		mc.columnLayout(adj = 1, rs = 4)
		mc.text(l = 'Looking for')
		mc.textField('%s_CtrlTX' % self.win)
		mc.text(l = 'Replace')
		mc.textField('%s_replaceTX' % self.win)
		mc.button(l = 'Move', c = partial(self.moveAsset), h = 30)
		mc.showWindow()
		mc.window(self.win, edit = True, wh = [200, 120])
		self.setName()
		
		
	def moveAsset(self, arg = None) : 
		ls = mc.ls(sl = True) 
		ctrlName = mc.textField('%s_CtrlTX' % self.win, q = True, tx = True)
		replace = mc.textField('%s_replaceTX' % self.win, q = True, tx = True)
		
		for i in range(len(ls)) : 
			ctrl = ('%s:%s' % (ls[i].split(':')[0], ctrlName))
			loc = '%s_%d_loc' % (replace, (i + 1))
			mc.delete(mc.parentConstraint(loc, ctrl))
			
			
	def setName(self, arg = None) : 
		mc.textField('%s_CtrlTX' % self.win, e = True, tx = self.look)
		mc.textField('%s_replaceTX' % self.win, e = True, tx = self.replace)
		
