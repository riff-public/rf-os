import maya.cmds as mc 
from functools import partial
import os
import sys
import shutil
import maya.mel as mm

def run() : 
	ui = PT_chimaJobCheck()
	ui.show()

class PT_chimaJobCheck : 
	def __init__(self) : 
		self.win = 'PT_chimaJobCheck'
		self.ui = '%s_Win' % self.win
		if sys.platform == 'win32' :
			self.projectPath = 'Z:/02_DESIGN/'
			self.drive = 'Z:/'
			self.browseList = ['01_CHARACTERS', '02_SETS', '03_PROPS', '04_VEHICLES']
			self.filter = ['03_MODEL', '04_RIG']
		if sys.platform == 'linux2' :
			self.template = '/dsPipe/Lego_Friends/film/LEGO_Friends_EP02_147762/sequenceTemplate/'
			self.projectPath = '/dsPipe/Lego_Friends/film/'
			self.drive = '/dsPipe/'
		
	def show(self) : 
		if mc.window(self.ui, exists = True) : 
			mc.deleteUI(self.ui)
			
		mc.window(self.ui, title = 'PT ChimaJobCheck v.1.0', s = True, wh = [600, 500])
		mc.columnLayout(adj = 1, rs = 4)
		
		mc.frameLayout(borderStyle = 'etchedIn', lv = 0)
		mc.rowColumnLayout(nc = 3, cw = [(1, 200), (2, 200), (3, 200)])
		mc.optionMenu('%s_projectOM' % self.win, cc = partial(self.browseModel))
		
		project = self.browseMenu()
		
		for each in project : 
			mc.menuItem(l = each)
			
		mc.optionMenu('%s_type' % self.win, cc = partial(self.browseModel))
		
		for each in self.browseList : 
			mc.menuItem(l = each)

		mc.text('%s_pathTX' % self.win, l = 'Project : %s' % self.projectPath, al = 'left') 
		mc.setParent('..')
		mc.setParent('..')

		mc.frameLayout(borderStyle = 'etchedIn', lv = 0)
		mc.rowColumnLayout(nc = 4, cw = [(1, 250), (2,250), (3,250), (4, 250)], cs = [(2, 20), (3, 20), (4, 20), (5, 20)])
		
		mc.text(l = 'All Lists')
		mc.text(l = 'Model')
		mc.text(l = 'Rig')
		mc.text(l = 'Job')
		mc.textScrollList('%s_allTSL' % self.win, numberOfRows = 20)
		mc.textScrollList('%s_modelTSL' % self.win, numberOfRows = 20)
		mc.textScrollList('%s_rigTSL' % self.win, numberOfRows = 20)
		mc.popupMenu()
		mc.menuItem(l = 'Open', c = partial(self.openRig))
		mc.textScrollList('%s_compareTSL' % self.win, numberOfRows = 20)
		mc.popupMenu()
		mc.menuItem(l = 'Open', c = partial(self.copyToRigWip, 'open'))
		mc.menuItem(l = 'Import', c = partial(self.copyToRigWip, 'import'))
		
		mc.text(l = '')
		mc.text(l = '')
		mc.text(l = '')
		mc.columnLayout(adj = 1, rs = 4)
		mc.button(l = 'Publish', c = partial(self.publishCurrent), h = 30)
		mc.button(l = 'Update', c = partial(self.browseModel), h = 30)
		mc.setParent('..')

		mc.setParent('..')
		mc.setParent('..')
		mc.showWindow()
		mc.window(self.ui, e = True, wh = [1070, 416])
		self.browseModel()
		
	def browseMenu(self, arg = None) : 
		file = mc.getFileList(folder = self.projectPath)
		episodes = []
		for each in file : 
			if each.split('_')[0].isdigit() : 
				episodes.append(each) 
		return sorted(episodes)
		
	def browseModel(self, arg = None) : 
		episode = mc.optionMenu('%s_projectOM' % self.win, q = True, v = True)
		#get type char, prop, veh
		files = sorted(mc.getFileList(folder = '%s%s/' % (self.projectPath, episode)))
		
		charType = mc.optionMenu('%s_type' % self.win, q = True, v = True)
		
		#Z:\02_DESIGN\07_LOCA07\03_PROPS\loca07_prp_gen_piranhasnake01\03_MODEL
		
		modelFiles = []
		
		mc.textScrollList('%s_allTSL' % self.win, e = True, ra = True)
		mc.textScrollList('%s_modelTSL' % self.win, e = True, ra = True)
		mc.textScrollList('%s_rigTSL' % self.win, e = True, ra = True)
		mc.textScrollList('%s_compareTSL' % self.win, e = True, ra = True)
		for each in files : 
			
			#check if it's in the list
			if each in charType : 
				#browse in asset types
				assets = sorted(mc.getFileList(folder = '%s%s/%s/' % (self.projectPath, episode, each)))
				#browse assets
				for i in range(len(assets)) : 
					count = 0
					model = ''
					assetType = sorted(mc.getFileList(folder = '%s%s/%s/%s/' % (self.projectPath, episode, each, assets[i])))
					mc.textScrollList('%s_allTSL' % self.win, e = True, append = '%s.%s' % (i, assets[i]))

					
					for eachType in assetType : 
						if eachType == self.filter[0] : 
							ma = sorted(mc.getFileList(folder = '%s%s/%s/%s/%s/' % (self.projectPath, episode, each, assets[i], eachType), filespec='*.ma'))
							if len(ma) > 0 : 
								for eachFile in ma : 
									mc.textScrollList('%s_modelTSL' % self.win, e = True, append = '%s.%s' % (i, eachFile))
									count = 1
									model = eachFile
									#print 'model %s' % eachFile
							else : 
								mc.textScrollList('%s_modelTSL' % self.win, e = True, append = '%s.%s' % (i, '-'))
							
						if eachType == self.filter[1] : 
							ma = sorted(mc.getFileList(folder = '%s%s/%s/%s/%s/' % (self.projectPath, episode, each, assets[i], eachType), filespec='*.ma'))
							if len(ma) > 0 : 
								for eachFile in ma : 
									mc.textScrollList('%s_rigTSL' % self.win, e = True, append = '%s.%s' % (i, eachFile))
									#print 'rig %s' % eachFile
									mc.textScrollList('%s_compareTSL' % self.win, e = True, append = '%s.%s' % (i, '-'))
									
							else : 
								mc.textScrollList('%s_rigTSL' % self.win, e = True, append = '%s.%s' % (i, '-'))	
								if count == 1 : 
									mc.textScrollList('%s_compareTSL' % self.win, e = True, append = '%s.%s' % (i, model))
									
								else : 
									mc.textScrollList('%s_compareTSL' % self.win, e = True, append = '%s.%s' % (i, '-'))
									
	def copyToRigWip(self, mode = 'open', arg = None) : 
		sel = mc.textScrollList('%s_compareTSL' % self.win, q = True, si = True)	
		selIndex = mc.textScrollList('%s_compareTSL' % self.win, q = True, sii = True)
		
		all = mc.textScrollList('%s_allTSL' % self.win, q = True, ai = True)
		allModels = mc.textScrollList('%s_modelTSL' % self.win, q = True, ai = True)
		allRigs = mc.textScrollList('%s_rigTSL' % self.win, q = True, ai = True)
		allJobs = mc.textScrollList('%s_compareTSL' % self.win, q = True, ai = True)	
		
		ep = mc.optionMenu('%s_projectOM' % self.win, q = True, v = True)
		charType = mc.optionMenu('%s_type' % self.win, q = True, v = True)
		
		if len(all) == len(allModels) == len(allRigs) == len(allJobs) : 
			path = '%s%s/%s/%s/03_MODEL/%s' % (self.projectPath, ep, charType, all[selIndex[0] - 1].split('.')[1], sel[0].replace('%s.' % sel[0].split('.')[0], ''))
			src = path
			dst = '%s%s/%s/%s/04_RIG/01_WIP/%s.ma' % (self.projectPath, ep, charType, all[selIndex[0] - 1].split('.')[1], all[selIndex[0] - 1].split('.')[1])
			dir = '%s%s/%s/%s/04_RIG/01_WIP' % (self.projectPath, ep, charType, all[selIndex[0] - 1].split('.')[1])
			if not os.path.exists(dir) : 
				os.makedirs(dir)
				
			if not os.path.exists(dst) : 
				shutil.copy2(src, dst)
				print 'Done Copying'
			
			if os.path.exists(dst) : 
				if mode == 'open' : 
					mc.file(dst, f = True, ignoreVersion = True, o = True)
				if mode == 'import' : 
					mc.file(dst, f = True, ignoreVersion = True, i = True)
		else : 
			mm.eval('print "List are not the same structure";')
			print('all %s, model %s, rig %s, job %s' % (len(all), len(allModels), len(allRigs), len(allJobs)))
				
	def publishCurrent(self, arg = None) : 
		currentFile = mc.file(q = True, sn = True)
		src = currentFile
		currentDir = currentFile.replace(currentFile.split('/')[-1], '')
		publishDir = currentDir.replace('%s/' % currentDir.split('/')[-2], '')
		dst = publishDir
		shutil.copy2(src, dst)
		print 'Done Publishing'
		self.browseModel()

	def openRig(self, arg = None) : 
		all = mc.textScrollList('%s_allTSL' % self.win, q = True, ai = True)
		selRig = mc.textScrollList('%s_rigTSL' % self.win, q = True, si = True)
		ep = mc.optionMenu('%s_projectOM' % self.win, q = True, v = True)
		selIndex = mc.textScrollList('%s_rigTSL' % self.win, q = True, sii = True)
		charType = mc.optionMenu('%s_type' % self.win, q = True, v = True)
		dst = '%s%s/%s/%s/04_RIG/%s.ma' % (self.projectPath, ep, charType, all[selIndex[0] - 1].split('.')[1], selRig[0].split('.')[1])
		mc.file(dst, f = True, ignoreVersion = True, o = True)
		#print dst

