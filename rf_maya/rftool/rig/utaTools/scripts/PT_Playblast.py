import maya.cmds as mc 
import maya.mel as mm
from functools import partial
import os
import os.path
import maya.mel as mm
import sys

def run() : 
	ui = PT_Playblast()
	#ui = ui.show()
	

class PT_Playblast : 
	def __init__(self) : 
		self.win = 'PT_Playblast'
		self.ui = '%s_Win' % self.win
		if sys.platform == 'win32' :
			self.fileName =  'N:/globalMaya/Mel/ptTools/LOCA00_K_camera.ma'
			self.os = 'win'
		if sys.platform == 'linux2' : 
			self.fileName =  '/dsGlobal/globalMaya/Mel/ptTools/LOCA00_K_camera.ma'
			self.os = 'linux'
		self.nameFormat = ['q0010_anim_hero', 'q0010_anim_preview', 'q0010_cam0010_anim_preview']
		self.hudList = ['File Name', 'User', 'Current Camera', 'Shot Frame', 'Overall Frame', 'Focal Length']
		self.show()
		self.width = int(mc.textField('%s_widthTX' % self.win, q = True, tx = True))
		self.height = int(mc.textField('%s_heightTX' % self.win, q = True, tx = True))
		
		self.audio = None
		audio = mc.ls(type = 'audio')
		if audio : 
			self.sound = audio[0]
			
		
		
		
	def show(self) : 
		if mc.window(self.ui, exists = True) : 
			mc.deleteUI(self.ui)
			
		mc.window(self.ui, title = 'PT_Playblast v.1.0', s = True, wh = [600, 500])

		form = mc.formLayout()
		tabs = mc.tabLayout(innerMarginWidth=5, innerMarginHeight=5)
		
		
		# start tablayout 1 ====================================
		
		tabLayout1 = mc.columnLayout(adj = 1, rs = 4)
		mc.formLayout( form, edit=True, attachForm=((tabs, 'top', 0), (tabs, 'left', 0), (tabs, 'bottom', 0), (tabs, 'right', 0)) )

		mc.frameLayout(lv = 0, borderStyle = 'etchedIn')
		mc.rowColumnLayout(nc = 2, cs = [(1, 2), (2, 4)], cw = [(1, 150), (2, 168)])
		
		mc.text(l = 'Platform : %s' % self.os , al = 'left')
		
		mc.setParent('..')
		mc.setParent('..')

		mc.frameLayout(lv = 0, borderStyle = 'etchedIn')
		mc.rowColumnLayout(nc = 2, cs = [(1, 2), (2, 4)], cw = [(1, 150), (2, 168)])

		mc.optionMenu('%s_chooserOP' % self.win, l = 'Cam Tools : ')
		mc.menuItem(l = 'ZooShots')
		mc.menuItem(l = 'Camera Sequencer')

		mc.button(l = 'Convert ZooShots to Sequencer', c = partial(self.zooShotToSequencer))

		mc.setParent('..')
		mc.setParent('..')

		mc.frameLayout(lv = 0, borderStyle = 'etchedIn')
		mc.columnLayout(adj = 1, rs = 4)
		mc.text(l = 'Camera List')
		mc.textScrollList('%s_cameraTSL' % self.win, numberOfRows = 16, allowMultiSelection = True)
		
		mc.button(l = 'Update', h = 30, c = partial(self.uiPlayblastSelector))
		
		mc.setParent('..')
		mc.setParent('..')
		
		mc.frameLayout(lv = 0, borderStyle = 'etchedIn')
		mc.rowColumnLayout(nc = 2, cs = [(1, 2), (2, 2)], cw = [(1, 180), (2, 180)])
		
		mc.button(l = 'Play Selected', h = 30, c = partial(self.playSelected))
		mc.button(l = 'Play All', h = 30, c = partial(self.playAll))
		
		mc.setParent('..')
		mc.setParent('..')
		
		mc.frameLayout(lv = 0, borderStyle = 'etchedIn')
		mc.rowColumnLayout(nc = 2, cs = [(2,4)], cw = [(1, 160), (2,160)])
		
		mc.checkBox('%s_previewCB' % self.win, l = 'Preview Only (Not Save)')
		mc.checkBox('%s_convertCB' % self.win, l = 'Convert to Mov', v = True)
		mc.checkBox('%s_openFileCB' % self.win, l = 'Open file after finish', v = False)
		mc.checkBox('%s_hudCB' % self.win, l = 'Show Hud', v = True)
		
		mc.setParent('..')
		mc.setParent('..')
		
		mc.frameLayout(l = 'Output Directory', borderStyle = 'etchedIn')
		mc.columnLayout(adj = 1, rs = 4)
		
		mc.textField('%s_outputTX' % self.win)
		mc.checkBox('%s_overrideOutputCB' % self.win, l = 'Override Output', onc = partial(self.updateUI), ofc = partial(self.updateUI))
		
		mc.frameLayout(lv = 0, borderStyle = 'etchedIn')
		mc.rowColumnLayout(nc = 3, cs = [(2, 2), (3, 2)], cw = [(1, 160), (2, 50), (3, 104)])
		
		mc.optionMenu('%s_fileNameOM' % self.win, l = 'Output Name', cc = partial(self.updateUI))
		mc.menuItem(l = 'File Name')
		mc.menuItem(l = 'Custom')
		
		mc.text(l = 'Custom')
		mc.textField('%s_fileNameTX' % self.win, tx = 'Playblast', cc = partial(self.updateUI))

		mc.setParent('..')
		mc.setParent('..')
		
		mc.frameLayout(lv = 0, borderStyle = 'etchedIn')
		mc.rowColumnLayout(nc = 4, cs = [(2, 2), (3, 2)], cw = [(1, 60), (2, 100), (3, 50), (4, 104)])
		
		mc.text(l = 'Dir Name')
		mc.textField('%s_dirNameTX' % self.win, tx = '01_BLASTS', cc = partial(self.updateUI))
		mc.text(l = 'Dir Level')
		mc.textField('%s_dirLocation' % self.win, tx = '0', cc = partial(self.updateUI))

		mc.setParent('..')
		mc.setParent('..')
		
		mc.setParent('..')
		mc.setParent('..')
		
		mc.frameLayout(l = 'Setting', borderStyle = 'etchedIn')
		mc.columnLayout(adj = 1, rs = 4)
		mc.frameLayout(lv = 0, borderStyle = 'etchedIn')
		mc.rowColumnLayout(nc = 2, cs = [(1, 2), (2, 8)], cw = [(1, 160), (2, 160)])

		mc.optionMenu('%s_sizeOM' % self.win, l = 'Resolution : ', cc = partial(self.updateUI))
		mc.menuItem(l = '720 x 405')
		mc.menuItem(l = '960 x 540')
		mc.menuItem(l = '1280 x 720')
		mc.menuItem(l = '1920 x 1080')
		mc.optionMenu('%s_sizeOM' % self.win, e = True, sl = 2)
		
		mc.checkBox(l = 'Override')
		
		mc.setParent('..')
		mc.rowColumnLayout(nc = 4, cs = [(2, 2), (3, 2)], cw = [(1, 60), (2, 100), (3, 50), (4, 104)])
		
		mc.text(l = 'Width')
		mc.textField('%s_widthTX' % self.win, tx = '960')
		mc.text(l = 'Height')
		mc.textField('%s_heightTX' % self.win, tx = '540')
		mc.setParent('..')
		
		mc.setParent('..')
		
		mc.frameLayout(lv = 0, borderStyle = 'etchedIn')
		mc.rowColumnLayout(nc = 3, cs = [(2, 2), (3, 2)], cw = [(1, 60), (2, 100), (3, 200)])
		
		mc.text(l = 'Codec')
		mc.textField('%s_codecTX' % self.win, tx = 'XVID', cc = partial(self.updateUI))

		mc.optionMenu('%s_extensionOM' % self.win, l = 'Extension', cc = partial(self.updateUI))
		mc.menuItem(l = 'avi')
		mc.menuItem(l = 'mov')
		mc.menuItem(l = 'iff')
		

		mc.setParent('..')
		mc.setParent('..')
		
		mc.frameLayout(lv = 0, borderStyle = 'etchedIn')
		mc.rowColumnLayout(nc = 4, cs = [(2, 2), (3, 2)], cw = [(1, 60), (2, 100), (3, 50), (4, 100)])
		
		mc.text(l = 'OverScan')
		mc.textField('%s_overScanTX' % self.win, tx = '1')

		mc.setParent('..')
		mc.setParent('..')
		

				
		mc.setParent('..')
		mc.setParent('..')
		
		mc.button(l = 'Save Setting (not working yet)')

		mc.setParent('..')

		# end tablayout 1 ===================================================
		# start tab layout 2 ================================================
		
		
		tabLayout2 = mc.columnLayout(adj = 1, rs = 4)
		
		mc.frameLayout(lv = 0, borderStyle = 'etchedIn')
		mc.rowColumnLayout(nc = 5, cs = [(1, 2), (2, 4)], cw = [(1, 140), (2, 50), (3, 50), (4, 50), (5, 50)])
		
		mc.optionMenu('%s_Hud1_OM' % self.win, l = 'HUD1 : ')
		for each in self.hudList : 
			mc.menuItem(l = each)
			
		mc.text(l = 'S') 
		mc.textField('%s_Hud1_sTX' % self.win)
		mc.text(l = 'B') 
		mc.textField('%s_Hud1_bTX' % self.win)
			
		mc.optionMenu('%s_Hud2_OM' % self.win, l = 'HUD2 : ')
		for each in self.hudList : 
			mc.menuItem(l = each)
			
		mc.text(l = 'S') 
		mc.textField('%s_Hud2_sTX' % self.win)
		mc.text(l = 'B') 
		mc.textField('%s_Hud2_bTX' % self.win)
			
		mc.optionMenu('%s_Hud3_OM' % self.win, l = 'HUD3 : ')
		for each in self.hudList : 
			mc.menuItem(l = each)
			
		mc.text(l = 'S') 
		mc.textField('%s_Hud3_sTX' % self.win)
		mc.text(l = 'B') 
		mc.textField('%s_Hud3_bTX' % self.win)
			
		mc.optionMenu('%s_Hud4_OM' % self.win, l = 'HUD4 : ')
		for each in self.hudList : 
			mc.menuItem(l = each)
			
		mc.text(l = 'S') 
		mc.textField('%s_Hud4_sTX' % self.win)
		mc.text(l = 'B') 
		mc.textField('%s_Hud4_bTX' % self.win)
		

		
		mc.setParent('..')
		mc.setParent('..')
		
		mc.frameLayout(lv = 0, borderStyle = 'etchedIn')
		mc.rowColumnLayout(nc = 3, cs = [(1, 2), (3, 60)], cw = [(1, 50), (2, 120), (3, 120)])
		
		mc.text(l = 'User : ', al = 'right')
		mc.textField('%s_UserTX' % self.win)
		
		mc.button(l = 'Save User', h = 20, c = partial(self.saveUser))
		
		mc.setParent('..')
		mc.setParent('..')

		mc.tabLayout( tabs, edit=True, tabLabel=((tabLayout1, 'Main'), (tabLayout2, 'HUD Setting')) )

		mc.showWindow()
		mc.window(self.ui, e = True, wh = [380, 720])
		self.uiPlayblastSelector()
		self.updateUI()
		
	def uiPlayblastSelector(self, arg = None) : 
		option = mc.optionMenu('%s_chooserOP' % self.win, q = True, v = True)
		playblstDir = mc.textField('%s_dirNameTX' % self.win, q = True, tx = True)
		dirLevel = int(mc.textField('%s_dirLocation' % self.win, q = True, tx = True))
		currentFile = mc.file(q = True, sn = True)


		if option == 'ZooShots' : 
			self.listByZooShots()

		if option == 'Camera Sequencer' :
			print 'Sequencer'


	def updateUI(self, arg = None) : 
		playblastDir = mc.textField('%s_dirNameTX' % self.win, q = True, tx = True)
		dirLevel = int(mc.textField('%s_dirLocation' % self.win, q = True, tx = True))
		currentFile = mc.file(q = True, sn = True)
		fileName = self.outputName()
		overrideOutput = mc.checkBox('%s_overrideOutputCB' % self.win, q = True, v = True)
		customName = mc.optionMenu('%s_fileNameOM' % self.win, q = True, v = True)
		
		if overrideOutput == 1 : 
			mc.textField('%s_outputTX' % self.win, e = True, ed = True)
		
		if overrideOutput == 0 : 
			mc.textField('%s_outputTX' % self.win, e = True, ed = False)	
			
		if customName == 'Custom' : 
			mc.textField('%s_fileNameTX' % self.win, e = True, ed = True)
		
		else : 
			mc.textField('%s_fileNameTX' % self.win, e = True, ed = False)
		
		if dirLevel > 0 : 
			mc.textField('%s_dirLocation' % self.win, e = True, tx = 0)
			dirLevel = 0
		
		if currentFile : 
			outputDirectory = self.outputDir(playblastDir, fileName, currentFile, dirLevel)
			mc.textField('%s_outputTX' % self.win, e = True, tx = outputDirectory)
			
		resolutionPreset = mc.optionMenu('%s_sizeOM' % self.win, q = True, v = True)
		resolution = resolutionPreset.split(' x ')
		
		mc.textField('%s_widthTX' % self.win, e = True, tx = resolution[0])
		mc.textField('%s_heightTX' % self.win, e = True, tx = resolution[1])
		
		self.getUser()
		

	def zooShotsCamInfo(self, type) : 
			shotsNode = 'shotsNode'
			if mc.objExists(shotsNode) : 
				try : 
					allZooshotCams = mc.listConnections(shotsNode)
					masterCam = mc.listConnections('%s.masterCamera' % shotsNode)
					exclude = masterCam
					startFrame = []
					endFrame = []
					disableCam = []
							
					for each in exclude : 
						if each in allZooshotCams : 
							allZooshotCams.remove(each)
							
					allZooshotCams = sorted(allZooshotCams)
					
					#print 'all cam %s' % len(allZooshotCams)
					
					for i in range(len(allZooshotCams)) : 
						shotsNodeAttr = mc.listConnections('%s.message' % allZooshotCams[i], p = True)
						
						for each in shotsNodeAttr : 
							if not each[-2].isdigit() : 
								shotNumber = each[-1]
							else : 
								shotNumber = '%s%s' % (each[-2], each[-1])
					
							rawShotInfo = mc.getAttr('%s.shotInfo%s' % (shotsNode, shotNumber))
							#print rawShotInfo
							
							if not '-disable 1' in rawShotInfo :
								start = rawShotInfo.replace('. -start ', '')
								startFrame.append(int(start))
								if not i == 0 : 
									endFrame.append(int(start) - 1)
						
								if i == len(allZooshotCams) - 1 : 
									rawEndFrame = mc.getAttr('%s.globalInfo' % shotsNode)
									endFrame.append(int(rawEndFrame.replace('. -end ', '')))
									
							else : 
								disableCam.append(allZooshotCams[i])
								if i == len(allZooshotCams) - 1 : 
									rawEndFrame = mc.getAttr('%s.globalInfo' % shotsNode)
									endFrame.append(int(rawEndFrame.replace('. -end ', '')))
					
					for removeCam in disableCam : 
						if removeCam in allZooshotCams : 
							allZooshotCams.remove(removeCam)
									
					if type == 'cam' : 
						return allZooshotCams 
						
					if type == 'start' : 
						return startFrame
						
					if type == 'end' : 
						return endFrame
						
				except AttributeError : 
					mm.eval('warning("shotsNode error. No end frame specify. List camera failed. Please update end frame value to fix this. ");')
					return 'none'
	
			else : 
				print 'No shotsNode found. Cannot playblast from zooshots.'
				return 'none'

	def listByZooShots(self) : 
		cam = self.zooShotsCamInfo('cam')
		start = self.zooShotsCamInfo('start')
		end = self.zooShotsCamInfo('end')
		
		if not cam == 'none' : 

			mc.textScrollList('%s_cameraTSL' % self.win, e = True, ra = True)
	
			for each in cam : 
				mc.textScrollList('%s_cameraTSL' % self.win, e = True, append = each)
				
		else : 
			print 'zooShotCamInfo return none'
				
	def playSelected(self, arg = None) : 
		selCams = mc.textScrollList('%s_cameraTSL' % self.win, q = True, si = True)
		playType = mc.optionMenu('%s_chooserOP' % self.win, q = True, v = True)
		width = int(mc.textField('%s_widthTX' % self.win, q = True, tx = True))
		height = int(mc.textField('%s_heightTX' % self.win, q = True, tx = True))
		outputDir = mc.textField('%s_outputTX' % self.win, q = True, tx = True)
		preview = mc.checkBox('%s_previewCB' % self.win, q = True, v = True)
		hudDisplay = mc.checkBox('%s_hudCB' % self.win, q = True, v = True)

		for each in selCams : 
			value = self.getCamInfo(each, playType)
			start = int(value.split('-')[0])
			end = int(value.split('-')[1])
			self.cleanViewPort(each)
			mainFile = outputDir.split('.')[0]
			ext = outputDir.split('.')[-1]
			shotName = each.split('_')[0]
			self.showAllHud(hudDisplay)
			output = '%s_%s.%s' % (mainFile, shotName, ext)
			self.playblastCmd(start, end, width, height, output, preview)
			self.showAllHud(0)
			print '%s %s %s' % (each, start, end)


	def playAll(self, arg = None) : 
		masterCam = self.getMasterCam()
		option = mc.optionMenu('%s_chooserOP' % self.win, q = True, v = True)
		
		startEnd = self.getTimeLength(option)
		start = startEnd.split('-')[0]
		end = startEnd.split('-')[-1]
		width = int(mc.textField('%s_widthTX' % self.win, q = True, tx = True))
		height = int(mc.textField('%s_heightTX' % self.win, q = True, tx = True))
		outputDir = mc.textField('%s_outputTX' % self.win, q = True, tx = True)
		preview = mc.checkBox('%s_previewCB' % self.win, q = True, v = True)
		hudDisplay = mc.checkBox('%s_hudCB' % self.win, q = True, v = True)

		self.showAllHud(hudDisplay)
		self.cleanViewPort(masterCam)
		self.playblastCmd(start, end, width, height, outputDir, preview)
		self.showAllHud(0)
		print '%s %s' % (start, end)



	def getCamInfo(self, cam, type, arg = None) : 
		if type == 'ZooShots' : 
			allCams = self.zooShotsCamInfo('cam')
			starts = self.zooShotsCamInfo('start')
			ends = self.zooShotsCamInfo('end')
			start = ''
			end = ''
			startEnd = ''

			if len(allCams) == len(starts) == len(ends) : 
				for i in range(len(allCams)) :
					if cam == allCams[i] : 
						start = starts[i]
						end = ends[i]
						startEnd = '%s-%s' % (start, end)	
		
			return startEnd

	def getTimeLength(self, type, arg = None) : 
		if type == 'ZooShots' : 
			allCams = self.zooShotsCamInfo('cam')
			starts = self.zooShotsCamInfo('start')
			ends = self.zooShotsCamInfo('end')
			min = starts[0]
			max = ends[-1]

			return '%s-%s' % (min, max)


			
	def playblastCmd(self, start, end, w, h, fileName, preview) : 
	
		# checkOutput
		outputDir = fileName.replace(fileName.split('/')[-1], '')
		
		if not os.path.exists(outputDir) : 
			os.makedirs(outputDir)
			
		fileFormat = mc.optionMenu('%s_extensionOM' % self.win, q = True, v = True)
		codec = mc.textField('%s_codecTX' % self.win, q = True, tx = True)
		
		fileName = fileName.replace('.%s' % fileName.split('.')[-1], '')
		showViewer = mc.checkBox('%s_openFileCB' % self.win, q = True, v = True)
			
		if preview == 1 :
			mc.playblast( format=fileFormat ,
							s=self.sound ,
							#filename=fileName ,
							st=start ,
							et=end ,
							forceOverwrite=True ,
							sequenceTime=0 ,
							clearCache=1 ,
							viewer=showViewer ,
							showOrnaments=1 ,
							fp=4 ,
							widthHeight=[w,h] ,
							percent=100 ,
							compression=codec ,
							offScreen=False ,
							quality=70
							)
							
		else : 
			if mc.checkBox('%s_convertCB' % self.win, q = True, v = True) : 
				showViewer = 0
			mc.playblast( format=fileFormat ,
							s=self.sound ,
							filename=fileName ,
							st=start ,
							et=end ,
							forceOverwrite=True ,
							sequenceTime=0 ,
							clearCache=1 ,
							viewer=showViewer ,
							showOrnaments=1 ,
							fp=4 ,
							widthHeight=[w,h] ,
							percent=100 ,
							compression=codec ,
							offScreen=False ,
							quality=70
							)

			if mc.checkBox('%s_convertCB' % self.win, q = True, v = True) : 
				self.convertFile('%s.%s' % (fileName, fileFormat), '%s.%s' % (fileName, 'mov'))
							
						
						
	def cleanViewPort(self, cam) : 
	
		overscan = int(mc.textField('%s_overScanTX' % self.win, q = True, tx = True))
		currentPanel = mc.getPanel(withFocus = True)
		mc.lookThru(cam)
		mc.modelEditor(currentPanel, e = True, displayAppearance = 'smoothShaded',dtx = True)
		mc.modelEditor(currentPanel, e = True, allObjects = 0)
		mc.modelEditor(currentPanel, e = True, nurbsSurfaces = True)
		mc.modelEditor(currentPanel, e = True, polymeshes = True)
		
		camShape = mc.listRelatives(cam, s = True)
		#mc.setAttr('%s.overscan' % camShape[0], overscan)
		mc.select(cl = True)
		

	def outputDir(self, playblastDir, fileName, path, dirLevel) : 
		#path = 'C:/test/test2/file.ma'
		#path = '/dsPipe/test/test2/file.ma'

		#playblastDir = 'playBlast/test'

		if playblastDir[0] == '/' : 
			playblastDir = playblastDir.replace('/', '')

		if not playblastDir[-1] == '/' : 
			playblastDir = '%s/' % playblastDir

		# check os
		if path[0] == '/' : 
			os = 'linux'
		else : 
			os = 'win'

		# find current dir
		currentDir = path.replace('/%s' % path.split('/')[-1], '')
		#dirLevel = -4
		allDir = currentDir.split('/')
		savingPath = ''

		# checking valid level
		if abs(dirLevel) > len(allDir) - 1 : 
			mm.eval('warning "level error. using root level";')
			dirLevel = -(len(allDir) - 1)

		#print len(allDir)

		if not dirLevel == 0 : 
			savingDir = allDir[dirLevel - 1]

			for each in allDir : 
				savingPath += '%s/' % each
				if savingDir == each : 
					break
					
			savingPath = '%s%s%s' % (savingPath, playblastDir, fileName)
			
		else : 
			savingPath = '%s/%s%s' % (currentDir, playblastDir, fileName)			

		return savingPath
		
	
	def outputName(self, arg = None) : 
		sceneFile = mc.file(q = True, sn = True) 
		fileName = 'UnDefined'
		customName = mc.textField('%s_fileNameTX' % self.win, q = True, tx = True)
		fileFormat = mc.optionMenu('%s_extensionOM' % self.win, q = True, v = True)
		
		if sceneFile : 
			fileName = sceneFile.split('/')[-1].split('.')[0]
		
		option = mc.optionMenu('%s_fileNameOM' % self.win, q = True, v = True)
		
		if option == 'File Name' : 
			return '%s.%s' % (fileName, fileFormat)
			
		if option == 'Custom' : 
			return '%s.%s' % (customName, fileFormat)


	def getMasterCam(self) : 

		option = mc.optionMenu('%s_chooserOP' % self.win, q = True, v = True)
		
		if option == 'ZooShots' : 
			shotsNode = 'shotsNode'
			if mc.objExists(shotsNode) : 
				masterCam = mc.listConnections('%s.masterCamera' % shotsNode)
				
			else : 
				print 'error shotsNode missing. cannot get masterCam'


		if option == 'Camera Sequencer' : 
			masterCam = 'masterCam'
		
		return masterCam

	def convertFile(self, currentFile, saveFile, arg = None) : 
		cmd = '"C:\Program Files (x86)\Pdplayer\pdplayer.exe" --attach --fps=24 --frame_base=1 --timeline_visible=0 ----pp_visible=0 %s --save_layer_as=%s,avc1 --exit' % ( currentFile , saveFile )
		os.system( cmd )
		os.remove( currentFile )
		saveFile = saveFile.replace('/', '\\')

		if mc.checkBox('%s_openFileCB' % self.win, q = True, v = True) : 
			os.system('"C:\Program Files (x86)\QuickTime\QuickTimePlayer.exe" %s' % saveFile)
		print 'convert done'


	def zooShotToSequencer(self, arg = None) : 
		shotsNode = 'shotsNode'
		if mc.objExists(shotsNode) : 
			cams = self.zooShotsCamInfo('cam')
			starts = self.zooShotsCamInfo('start')
			ends = self.zooShotsCamInfo('end')

			for i in range(len(cams)) :
				cam = cams[i]
				start = starts[i]
				end = ends[i]
				shot = cams[i].split('_')[0]
				mc.shot(shot, startTime = start, endTime = end, sequenceStartTime = start, sequenceEndTime = end, currentCamera = cam)
				print 'Create %s from %s - %s' % (cam, start, end)
				
	
	def saveUser(self, arg = None) : 
		user = mc.textField('%s_UserTX' % self.win, q = True, tx = True)
		mc.optionVar(sv=('PTuser', user) )		
		mm.eval('print "save user as %s ";' % user)
		
	
	def getUser(self, arg = None) : 
		user = mc.optionVar(q = 'PTuser')
		mc.textField('%s_UserTX' % self.win, e = True, tx = user)


	def showHudUser(self, s=5, b=2, show = 1, arg = None) : 
		mc.headsUpDisplay( rp = (s, b))
		if mc.headsUpDisplay('userHud', ex = True) : 
			mc.headsUpDisplay('userHud', rem = True)

		if show == 1 :
			mc.headsUpDisplay('userHud', s=s, b=b, ba='left', dw = 50, l = 'User : ', c = self.getHudUserName, attachToRefresh = True)
			

	def getHudFileName(self, arg = None) : 
		fileName = mc.file(q = True, sn = True)
		return fileName

	def showHudFileName(self, s=5, b=3, show = 1, arg = None) : 
		mc.headsUpDisplay( rp=(s, b) )
		if mc.headsUpDisplay('fileNameHud', ex = True) : 
			mc.headsUpDisplay( 'fileNameHud', rem=True )

		if show == 1 : 
			mc.headsUpDisplay( 'fileNameHud', s=s, b=b, ba='left', dw=50, l = 'File Name : ', c = self.getHudFileName, attachToRefresh = True)

	
	def getHudUserName(self, arg = None) : 
		user = mc.optionVar(q = 'PTuser')
		return user

	def getHudCamName(self, arg = None) : 
		return self.getCurrentCam()

	def getHudShotFrame(self, arg = None) : 
		currentCam = self.getCurrentCam()
		allCams = self.zooShotsCamInfo('cam')
		starts = self.zooShotsCamInfo('start')
		ends = self.zooShotsCamInfo('end')
		currentFrame = mc.currentTime(q = True)
		overallFrame = 0
		startFrame = 0

		for i in range(len(allCams)) : 
			if currentCam == allCams[i] : 
				overallFrame = ((ends[i] + 1) - starts[i])
				startFrame = currentFrame + 1 - starts[i]
				return '%s/%s' % (startFrame, overallFrame)

	def showHudShotFrame(self, s=7, b=2, show = 1, arg = None) : 
		mc.headsUpDisplay( rp = (s, b))
		if mc.headsUpDisplay('currentShotInfo', ex = True) : 
			mc.headsUpDisplay('currentShotInfo', rem = True)

		if show == 1 : 
			mc.headsUpDisplay('currentShotInfo', s=s, b=b, ba='left', dw = 50, l = 'Info : ', c = self.getHudShotFrame, attachToRefresh = True)

		
	def getHudOverAllFrame(self, arg = None) : 
		currentFrame = mc.currentTime(q = True)
		ends = self.zooShotsCamInfo('end')
		return '%s/%s' % (currentFrame, ends[-1])


	def showHudOverAllFrame(self, s=7, b=1, show = 1, arg = None) : 
		mc.headsUpDisplay( rp = (s, b))
		if mc.headsUpDisplay('overallFrameDisplay', ex = True) : 
			mc.headsUpDisplay('overallFrameDisplay', rem = True)

		if show == 1 : 
			mc.headsUpDisplay('overallFrameDisplay', s=s, b=b, ba='left', dw = 50, l = 'Over all : ', c = self.getHudOverAllFrame, attachToRefresh = True)
		
		
	def getCurrentCam(self, arg = None) : 
		currentFrame = mc.currentTime(q = True)
		cams = self.zooShotsCamInfo('cam')
		starts = self.zooShotsCamInfo('start')
		ends = self.zooShotsCamInfo('end')
		cam = 'No Cam'

		for i in range(len(cams)) : 
			cam = cams[i]
			start = float(starts[i])
			end = float(ends[i])
			
			if currentFrame in range(start, end + 1) : 
				return cam


	def showHudCurrentCam(self, s=7, b=3, show = 1, arg = None) : 
		mc.headsUpDisplay( rp = (s, b))
		if mc.headsUpDisplay('currentCamHud', ex = True) : 
			mc.headsUpDisplay('currentCamHud', rem = True)

		if show == 1 : 
			mc.headsUpDisplay('currentCamHud', s=s, b=b, ba='left', dw = 50, l = 'Cam : ', c = self.getCurrentCam, attachToRefresh = True)

	def showAllHud(self, show = 1, arg = None) : 
		
		if show == 1 : 
			# file name
			self.showHudFileName(5, 3, 1)

			# user
			self.showHudUser(5, 2, 1)

			# current camera
			self.showHudCurrentCam(7, 3, 1)
			

			# current shot display
			self.showHudShotFrame(7, 2, 1)
			

			# overall frame display
			self.showHudOverAllFrame(7, 1, 1)

		else : 
			# file name
			self.showHudFileName(5, 3, 0)

			# user
			self.showHudUser(5, 2, 0)

			# current camera
			self.showHudCurrentCam(7, 3, 0)
			

			# current shot display
			self.showHudShotFrame(7, 2, 0)
			

			# overall frame display
			self.showHudOverAllFrame(7, 1, 0)


	def prefSetting(self, arg = None) : 
		# 
		#user = user = mc.textField('%s_UserTX' % self.win, q = True, tx = True)
		#mc.optionVar(sv=('PTuser', user) )
		#mc.optionVar( remove='PTuser' )
		print 'OK'
		

a = PT_Playblast()