import maya.cmds as mc

def run():
    ui = test()
    ui.show()

class test(object):
    def __init__(self):
        self.ui = 'test'
        self.win = '%sWin' %self.ui
        
    def show(self):
        if mc.window(self.win, exists = True):
            mc.deleteUI(self.win)
            
        mc.window(self.win, title = 'test window')
        mc.columnLayout(adj = True)
        mc.button(l = 'OK')
        mc.showWindow(self.win)
        mc.window(self.win, edit = True, wh = [300, 200])