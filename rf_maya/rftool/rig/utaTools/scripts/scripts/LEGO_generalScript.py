import maya.cmds as mc

class LEGO_generalScript(object) :

    def __init__(self) :    
        self.ui = 'generalScript'
    
    def toggleCtrlVis(self) :
        panel = mc.getPanel(wf = True)
        
        if panel == 'modelPanel1' or 'modelPanel2' or 'modelPanel3' or 'modelPanel4' :
            currentState = mc.modelEditor(panel, query = True, nurbsCurves = True)
            
            if currentState == 0 : 
                mc.modelEditor(panel, edit = True, nurbsCurves = 1)
                
            if currentState == 1 :
                mc.modelEditor(panel, edit = True, nurbsCurves = 0)