#list all character for LEGO Project

import maya.cmds as mc
from functools import partial

def run():
	ui = LEGO_CharList()
	ui.show()

class LEGO_CharList(object):

	def __init__(self):
		
		self.ui = 'charList'
		self.win = '%sWin' %self.ui
		
		#declare ctrl sets
		
		self.superRootCtrl = ['placement_ctrl']
		self.rootCtrl 	= ['placement_ctrl', 'placementGmbl_ctrl']
		self.lf_legCtrl = ['ankleFkGmblLFT_ctrl','ankleFkLFT_ctrl','ankleRollIkLFT_ctrl','kneeIkLFT_ctrl','legIkGmblLFT_ctrl','legIkLFT_ctrl','legIkRootLFT_ctrl','legLFT_ctrl','legRbnLFT_ctrl','lowLegFkGmblLFT_ctrl','lowLegFkLFT_ctrl','lowLegRbnDtl1LFT_ctrl','lowLegRbnDtl2LFT_ctrl','lowLegRbnDtl3LFT_ctrl','lowLegRbnDtl4LFT_ctrl','lowLegRbnDtl5LFT_ctrl','lowLegRbnEndLFT_ctrl','lowLegRbnLFT_ctrl','lowLegRbnRootLFT_ctrl','toeFkGmblLFT_ctrl','toeFkLFT_ctrl','upLegFkGmblLFT_ctrl','upLegFkLFT_ctrl','upLegRbnDtl1LFT_ctrl','upLegRbnDtl2LFT_ctrl','upLegRbnDtl3LFT_ctrl','upLegRbnDtl4LFT_ctrl','upLegRbnDtl5LFT_ctrl','upLegRbnEndLFT_ctrl','upLegRbnLFT_ctrl','upLegRbnRootLFT_ctrl']	
		self.rt_legCtrl = ['ankleFkGmblRGT_ctrl','ankleFkRGT_ctrl','ankleRollIkRGT_ctrl','kneeIkRGT_ctrl','legIkGmblRGT_ctrl','legIkRGT_ctrl','legIkRootRGT_ctrl','legRGT_ctrl','legRbnRGT_ctrl','lowLegFkGmblRGT_ctrl','lowLegFkRGT_ctrl','lowLegRbnDtl1RGT_ctrl','lowLegRbnDtl2RGT_ctrl','lowLegRbnDtl3RGT_ctrl','lowLegRbnDtl4RGT_ctrl','lowLegRbnDtl5RGT_ctrl','lowLegRbnEndRGT_ctrl','lowLegRbnRGT_ctrl','lowLegRbnRootRGT_ctrl','toeFkGmblRGT_ctrl','toeFkRGT_ctrl','upLegFkGmblRGT_ctrl','upLegFkRGT_ctrl','upLegRbnDtl1RGT_ctrl','upLegRbnDtl2RGT_ctrl','upLegRbnDtl3RGT_ctrl','upLegRbnDtl4RGT_ctrl','upLegRbnDtl5RGT_ctrl','upLegRbnEndRGT_ctrl','upLegRbnRGT_ctrl','upLegRbnRootRGT_ctrl']	
		self.lf_armCtrl = ['armIkGmblLFT_ctrl','armIkLFT_ctrl','armIkRootLFT_ctrl','armLFT_ctrl','armRbnLFT_ctrl','elbowIkLFT_ctrl','forearmFkGmblLFT_ctrl','forearmFkLFT_ctrl','forearmRbnDtl1LFT_ctrl','forearmRbnDtl2LFT_ctrl','forearmRbnDtl3LFT_ctrl','forearmRbnDtl4LFT_ctrl','forearmRbnDtl5LFT_ctrl','forearmRbnEndLFT_ctrl','forearmRbnLFT_ctrl','forearmRbnRootLFT_ctrl','upArmFkGmblLFT_ctrl','upArmFkLFT_ctrl','upArmRbnDtl1LFT_ctrl','upArmRbnDtl2LFT_ctrl','upArmRbnDtl3LFT_ctrl','upArmRbnDtl4LFT_ctrl','upArmRbnDtl5LFT_ctrl','upArmRbnEndLFT_ctrl','upArmRbnLFT_ctrl','upArmRbnRootLFT_ctrl','wristFkGmblLFT_ctrl','wristFkLFT_ctrl']
		self.rt_armCtrl = ['armIkGmblRGT_ctrl','armIkRGT_ctrl','armIkRootRGT_ctrl','armRGT_ctrl','armRbnRGT_ctrl','elbowIkRGT_ctrl','forearmFkGmblRGT_ctrl','forearmFkRGT_ctrl','forearmRbnDtl1RGT_ctrl','forearmRbnDtl2RGT_ctrl','forearmRbnDtl3RGT_ctrl','forearmRbnDtl4RGT_ctrl','forearmRbnDtl5RGT_ctrl','forearmRbnEndRGT_ctrl','forearmRbnRGT_ctrl','forearmRbnRootRGT_ctrl','upArmFkGmblRGT_ctrl','upArmFkRGT_ctrl','upArmRbnDtl1RGT_ctrl','upArmRbnDtl2RGT_ctrl','upArmRbnDtl3RGT_ctrl','upArmRbnDtl4RGT_ctrl','upArmRbnDtl5RGT_ctrl','upArmRbnEndRGT_ctrl','upArmRbnRGT_ctrl','upArmRbnRootRGT_ctrl','wristFkGmblRGT_ctrl','wristFkRGT_ctrl']
		self.backCtrl 	= ['clavGmblLFT_ctrl','clavGmblRGT_ctrl','clavLFT_ctrl','clavRGT_ctrl','lowSpineRbnEnd_ctrl','lowSpineRbnRoot_ctrl','lowSpineRbn_ctrl','pelvisGmbl_ctrl','pelvis_ctrl','rootGmbl_ctrl','root_ctrl','spine1FkGmbl_ctrl','spine1Fk_ctrl','spine2FkGmbl_ctrl','spine2Fk_ctrl','spineIkGmbl_ctrl','spineIkOff_ctrl','spineIkRoot_ctrl','spineIk_ctrl','spineRbn_ctrl','spine_ctrl','upSpineRbnEnd_ctrl','upSpineRbnRoot_ctrl','upSpineRbn_ctrl']
		self.faceDtCtrl = ['cheekDtlLFT_ctrl','cheekDtlRGT_ctrl','cornerLipDtlLFT_ctrl','cornerLipDtlRGT_ctrl','eb1DtlLFT_ctrl','eb1DtlRGT_ctrl','eb2DtlLFT_ctrl','eb2DtlRGT_ctrl','eb3DtlLFT_ctrl','eb3DtlRGT_ctrl','eb4DtlLFT_ctrl','eb4DtlRGT_ctrl','ebDtl_ctrl','lid1DtlLFT_ctrl','lid1DtlRGT_ctrl','lid2DtlLFT_ctrl','lid2DtlRGT_ctrl','lid3DtlLFT_ctrl','lid3DtlRGT_ctrl','lid4DtlLFT_ctrl','lid4DtlRGT_ctrl','lid5DtlLFT_ctrl','lid5DtlRGT_ctrl','lid6DtlLFT_ctrl','lid6DtlRGT_ctrl','lid7DtlLFT_ctrl','lid7DtlRGT_ctrl','lid8DtlLFT_ctrl','lid8DtlRGT_ctrl','lowerLip1DtlLFT_ctrl','lowerLip1DtlRGT_ctrl','lowerLip2DtlLFT_ctrl','lowerLip2DtlRGT_ctrl','lowerLip3DtlLFT_ctrl','lowerLip3DtlRGT_ctrl','lowerLip4DtlLFT_ctrl','lowerLip4DtlRGT_ctrl','lowerLip5DtlLFT_ctrl','lowerLip5DtlRGT_ctrl','lowerLip6DtlLFT_ctrl','lowerLip6DtlRGT_ctrl','lowerLipDtl_ctrl','puffDtlLFT_ctrl','puffDtlRGT_ctrl','upperLip1DtlLFT_ctrl','upperLip1DtlRGT_ctrl','upperLip2DtlLFT_ctrl','upperLip2DtlRGT_ctrl','upperLip3DtlLFT_ctrl','upperLip3DtlRGT_ctrl','upperLip4DtlLFT_ctrl','upperLip4DtlRGT_ctrl','upperLip5DtlLFT_ctrl','upperLip5DtlRGT_ctrl','upperLip6DtlLFT_ctrl','upperLip6DtlRGT_ctrl','upperLipDtl_ctrl']
		self.hairCtrl 	= ['backHair1Gmbl_ctrl','backHair1_ctrl','backHair2Gmbl_ctrl','backHair2_ctrl','backHair3Gmbl_ctrl','backHair3_ctrl','frontHair1GmblLFT_ctrl','frontHair1GmblRGT_ctrl','frontHair1LFT_ctrl','frontHair1RGT_ctrl','frontHair2GmblLFT_ctrl','frontHair2GmblRGT_ctrl','frontHair2LFT_ctrl','frontHair2RGT_ctrl']
		self.blendshapeCtrl = ['eyeBsh_ctrl','mouthBsh_ctrl']
		self.headCtrl 	= ['eyeGmblLFT_ctrl','eyeGmblRGT_ctrl','eyeLFT_ctrl','eyeOfst_ctrl','eyeRGT_ctrl','eyeTrgtLFT_ctrl','eyeTrgtRGT_ctrl','eye_ctrl','headGmbl_ctrl','head_ctrl','jaw1GmblLWR_ctrl','jaw1LWR_ctrl','jaw2GmblLWR_ctrl','jaw2LWR_ctrl','lowerHead_ctrl','lowerTeeth_ctrl','mouthMove_ctrl','neck1FkGmbl_ctrl','neck1Fk_ctrl','neckRbnEnd_ctrl','neckRbnRoot_ctrl','neckRbn_ctrl','tongue1Gmbl_ctrl','tongue1_ctrl','tongue2Gmbl_ctrl','tongue2_ctrl','tongue3Gmbl_ctrl','tongue3_ctrl','tongue4Gmbl_ctrl','tongue4_ctrl','upperHead_ctrl','upperTeeth_ctrl']
		self.lf_fingerCtrl = ['index1FkLFT_ctrl','index2FkLFT_ctrl','index3FkLFT_ctrl','index4FkLFT_ctrl','indexLFT_ctrl','middle1FkLFT_ctrl','middle2FkLFT_ctrl','middle3FkLFT_ctrl','middle4FkLFT_ctrl','middleLFT_ctrl','pinky1FkLFT_ctrl','pinky2FkLFT_ctrl','pinky3FkLFT_ctrl','pinky4FkLFT_ctrl','pinkyLFT_ctrl','ring1FkLFT_ctrl','ring2FkLFT_ctrl','ring3FkLFT_ctrl','ring4FkLFT_ctrl','ringLFT_ctrl','thumb1FkLFT_ctrl','thumb2FkLFT_ctrl','thumb3FkLFT_ctrl','thumbLFT_ctrl']
		self.rt_fingerCtrl = ['index1FkRGT_ctrl','index2FkRGT_ctrl','index3FkRGT_ctrl','index4FkRGT_ctrl','indexRGT_ctrl','middle1FkRGT_ctrl','middle2FkRGT_ctrl','middle3FkRGT_ctrl','middle4FkRGT_ctrl','middleRGT_ctrl','pinky1FkRGT_ctrl','pinky2FkRGT_ctrl','pinky3FkRGT_ctrl','pinky4FkRGT_ctrl','pinkyRGT_ctrl','ring1FkRGT_ctrl','ring2FkRGT_ctrl','ring3FkRGT_ctrl','ring4FkRGT_ctrl','ringRGT_ctrl','thumb1FkRGT_ctrl','thumb2FkRGT_ctrl','thumb3FkRGT_ctrl','thumbRGT_ctrl']
		




		
		
	def show(self):
		
		if mc.window(self.win, exists = True):
			
			mc.deleteUI(self.win)
			
		mc.window(self.win, title = 'Char List')
		mc.columnLayout(adj = True, rs = 1)
		
		mc.text(l = 'Character lists')
		
		mc.textScrollList( '%sCharTSL' %self.ui, numberOfRows = 8, allowMultiSelection = True)
		
		mc.frameLayout(borderStyle = 'etchedOut', lv = False) 
		mc.rowColumnLayout(nc = 3, cw = [(1, 100), (2, 100)])
		
		mc.radioCollection()
		mc.radioButton('%sCharRB' % self.ui, label='Char' , sl = True)
		mc.radioButton('%sPropRB' % self.ui,  label='Prop' )
		
		mc.setParent('..')
		mc.setParent('..')
		
		mc.frameLayout(borderStyle = 'etchedOut', lv = False) 
		mc.rowColumnLayout(nc = 3, cw = [(1, 100), (2, 100)])
		
		mc.text(l = 'Selection', al = 'left')
		mc.checkBox('%sSelectionCB' % self.ui, l = 'Add')
		
		mc.setParent('..')
		mc.setParent('..')
		
		mc.frameLayout(borderStyle = 'etchedOut', lv = False) 
		mc.rowColumnLayout(nc = 3, cw = [(1, 100), (2, 100)])
		
		mc.text(l = 'Isolate Selected', al = 'left')
		mc.checkBox('%sIsolateCB' % self.ui, l = 'On', en = 0 )
		
		mc.setParent('..')
		mc.setParent('..')

		mc.button( l = 'Update' , h = 30, c = partial(self.getCharList))
		
		mc.showWindow(self.win)
		mc.window(self.win, edit = True, wh = [180, 310])
		self.getCharList()
		
	
	
	def getCharList(self, arg = None):
		
		# getting all references in the scene
		
		allNamespace = mc.namespaceInfo(lon = True)
		allNamespace.remove('UI')
		allNamespace.remove('shared')
		
		# update all list to the textScrollList
		
		#clear list
		
		mc.textScrollList('%sCharTSL' %self.ui, edit = True, ra = True)

		# get the filter selected by UI
		
		filter = 'Char'
		
		if mc.radioButton('%sCharRB' % self.ui, query = True, sl = True):
			filter = 'Char'
			
		if mc.radioButton('%sPropRB' % self.ui, query = True, sl = True):
			filter = 'Prop'
		
		#check if it char or not
		
		for i in allNamespace:
		
			allGrp = '%s:Rig_Grp' % i
			
			#check if Rig_Grp exists			
			if mc.objExists(allGrp):
			
				#add assets that match the filter to the lists
				
				if mc.getAttr('%s.assetType' % allGrp) == filter:
						
					mc.textScrollList('%sCharTSL' %self.ui, edit = True, append = i)
			
		print ('%s added' % str(int(len(allNamespace)) - 1))
		
		
	# Below this line are selection set functions ###===========================================================
	
	# Call function with these parameter 
	# Sel (Root, SuperRoot, LF_leg, RT_leg, back, Head, LF_arm, RT_arm, LF_finger, RT_finger, blendshape, FaceDetails, Hair)

		
	def Sel(self, Root = 1, SuperRoot = 1, LF_leg = 1, RT_leg = 1, back = 1, Head = 1, LF_arm = 1, RT_arm = 1, LF_finger = 1, RT_finger = 1, blendshape = 1, FaceDetails = 1, Hair = 1):
	
	
		if mc.window(self.win, exists = True):
		
			selObj = mc.textScrollList('%sCharTSL' %self.ui, query = True, si = True)
			add = True
			
			if mc.checkBox('%sSelectionCB' % self.ui, query = True, v = True) :
			    add = False
			
			if (selObj) != None :
			
				mc.select(cl = add)
				
				for i in selObj:
				
					#Select root ctrl only
					
					if SuperRoot == 1:
					
						self.SelSelection(i, self.superRootCtrl)
					
					if Root == 1 :
					
						self.SelSelection(i, self.rootCtrl)
						
					if LF_leg == 1 :
					
						self.SelSelection(i, self.lf_legCtrl)
						
					if RT_leg == 1 :
					
						self.SelSelection(i, self.rt_legCtrl)
						
					if back == 1 :
					
						self.SelSelection(i, self.backCtrl)
						
					if Head == 1 :
					
						self.SelSelection(i, self.headCtrl)
						
					if LF_arm == 1 :
					
						self.SelSelection(i, self.lf_armCtrl)
						
					if RT_arm == 1 :
					
						self.SelSelection(i, self.rt_armCtrl)
						
					if LF_finger == 1 :
					
						self.SelSelection(i, self.lf_fingerCtrl)
						
					if RT_finger == 1 :
					
						self.SelSelection(i, self.rt_fingerCtrl)
						
					if blendshape == 1 :
					
						self.SelSelection(i, self.blendshapeCtrl)
						
					if FaceDetails == 1 :
					
						self.SelSelection(i, self.faceDtCtrl)
						
					if Hair == 1 :
					
						self.SelSelection(i, self.hairCtrl)
						
					
						
					
	def SelSelection(self, namespace, ctrl) :
		
		for i in ctrl : 
			
			mc.select('%s:%s' % (namespace, i), add = True)
			
			
			
	# isolate selected
	
	def SelRigGrp(self) :
	
		if mc.window(self.win, exists = True):
		
			selObj = mc.textScrollList('%sCharTSL' %self.ui, query = True, si = True)
			
			if (selObj) != None :
			
				charSel = []
					
				for i in selObj:
					
					charSel.append('%s:Rig_Grp' % i)
					
				mc.select(charSel)