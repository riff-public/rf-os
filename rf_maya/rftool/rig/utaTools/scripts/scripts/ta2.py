import maya.cmds as mc

def run():
    ui = myClass()
    ui.show()

class myClass(object):
    def __init__(self):
        self.ui = 'testTool'
        self.win = '%sWin' %self.ui
        
    def show(self):
        if mc.window(self.win, exists = True):
            mc.deleteUI(self.win)
            
        mc.window(self.win, title = 'test')
        mc.columnLayout()
        mc.button(l = 'OK')
        mc.showWindow(self.win)
        mc.window(self.win, edit = True, wh = [300, 200])