/*
''	Originally scripted by: Kurt Rathjen 2007
''	Email: kurt.rathjen@gmail.com
''	www.kurtrathjen.com
''	---
''	Source "C:/path/al_animationLibraryUI";
''	Run: al_animationLibraryUI;
*/

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//ph_getExactNamespaceFromObject
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

global proc string al_shortName(
	string $object)
{
	string $buffer[];
	tokenize $object "|" $buffer;

	if (`size $buffer` > 1)
		return $buffer[`size $buffer`-1];
	else
		return $buffer[0];
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//ph_getExactNamespaceFromObject
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

global proc string[] ph_getExactNamespaceFromObject (
	string $objects[])
{
	string $return[];
	string $namespaceList[] = `namespaceInfo -listOnlyNamespaces`;

	for ($nameSpace in $namespaceList){
		for ($each in $objects) {
			if 	((`gmatch $each  ("|" + $nameSpace+":*")`) ||
				(`gmatch $each  ($nameSpace+":*")`))
				$return[`size($return)`] = $nameSpace;
		}
	}
	$return = `stringArrayRemoveDuplicates($return)`;
	return $return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//ph_getExactNamespaceFromReferencePath
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

global proc string[] ph_getExactNamespaceFromReferencePath (
	string $references[])
{
	string $return[];

	for ($reference in $references)
		$return[size($return)] = `file -q -ns $reference`;

	return $return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//ph_getNameSpaces
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

global proc string[] ph_getNameSpaces()
{
	string $array[], $return[], $buffer[], $tmp;

	string $selection[] = ph_getExactNamespaceFromObject(`ls -l -sl`);
	string $references[] = `file -q -r`;
	string $namespaces[] =  ph_getExactNamespaceFromReferencePath($references);
	string $sets[] = `ls -sets`;

	for ($set in $sets) {
		if (($tmp = `match "ControlSet$" $set`) != ""){
			$numTokens = `tokenize $set ":" $buffer`;
			if ($buffer[0] != "ControlSet")
				$buffer[0] = `substitute $tmp $buffer[0] ""`;
			$return[`size $return`] = $buffer[0];
		}
	}

	$return = stringArrayCatenate ($array, $return);
	return $return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//ph_getNameSpaces
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

global proc al_createControlSet() {

	string $selected[] = `ls -sl`;

	if (!`size $selected`)
		error "Nothig is selected";


	string $result = `promptDialog
 		-title ("Create New ControlSet")
 		-message ("Only Adds Selected Objects!\nEnter ControlSet Name:")
		-text ""
 		-button "OK" -button "Cancel"
 		-defaultButton "OK" -cancelButton "Cancel"
 		-dismissString "Cancel"`;

	if ($result != "OK")
		return;

	string $controlSet = (`promptDialog -query -text` + "_ControlSet");

	if (`objExists $controlSet`)
		error (`promptDialog -query -text` + " already Exists!");

	$controlSet = `createNode  -ss "objectSet" -n $controlSet`;

	print ($controlSet + "\n");
	sets -add $controlSet $selected;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//ph_loadCharacterControls
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

global proc ph_loadCharacterControls(
	string $namespace,
	string $textScrollList,
	string $optionVar,
	int $alt)
{
	string $controlSet, $tmp[], $list[], $result;

	if (!`gmatch $namespace "*ControlSet"`) {
		if (!`objExists ($controlSet = ($namespace + "ControlSet"))`)
			$controlSet = ($namespace + ":ControlSet");
	}
	else if ($namespace != "")
		$controlSet = $namespace;

	if ($namespace != "New..") {
		if (`objExists $controlSet`) {
			$list = `sets -q $controlSet`;
			if (`objExists ($namespace + ":face")`) {
				$result = `confirmDialog
					-title "Confirm"
					-message ("Add Blend Shape \""+$namespace+":face\" Controller?")
					-button "Yes"
					-button "No"
					-defaultButton "Yes"
					-cancelButton "No"
					-dismissString "No"`;

				if ($result == "Yes")
					$list[`size $list`] = ($namespace + ":face");
				else if ($result == "No")
					$list = stringArrayRemoveExact({($namespace + ":face")}, $list);
			}
			if ($alt) {
				optionVar -sv $optionVar "";
				al_autoMapObjects ($list, $namespace, 0);
			}
			else {
				al_sortOptionVarList({""}, $textScrollList, $optionVar, -1);
				al_sortOptionVarList($list, $textScrollList, $optionVar, 1);
			}
		}
	}
	else {
	}
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//al_substituteAllinString
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

global proc string al_substituteAllinString (
	string $search,
	string $string,
	string $replace)
{
	do {
		$string = `substitute $search $string $replace`;
	}
	while
		(`gmatch $string ("*" + $search + "*")`) ;

	return  $string;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//al_substituteAllinString
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

global proc string al_substituteAllString (
	string $search,
	string $str,
	string $replace,
	string $split,
	int $alt)
{
	string $return, $buffer[];
	int $numTokens = `tokenize $str $split $buffer`;

	for ($i=0;$i<`size $buffer`;$i++) {
		$buffer[$i] = `substitute $search $buffer[$i] $replace`;
		if (!`startsWith $str $split` && $i == 0) {
			if ($alt)
				$return += ($search + $buffer[$i]);
			else
				$return += ($buffer[$i]);
		} else
			if ($alt)
				$return += ($split + $search + $buffer[$i]);
			else
				$return += ($split + $buffer[$i]);
	}
	return $return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//al_tokenize2
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

global proc string al_tokenize2 (
	string $str,
	string $split1,
	string $split2)
{
	string $buffer1[], $buffer2[], $return;
	int $numtoken = `tokenize $str $split1 $buffer1`;

	for ($i=0;$i<`size $buffer1`;$i++) {
		int $numtoken = `tokenize $buffer1[$i] $split2 $buffer2`;
		if (!`startsWith $str $split1` && $i == 0) {
			$return += ($buffer2[$numtoken-1]);
		} else
			$return += ($split1 + $buffer2[$numtoken-1]);
	}
	return $return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//al_sortListAz
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

global proc string[]  al_sortListAz(
	string $list[])
{
	string $tmp[], $result[];
	int $alt;

	for ($item in $list) {
		$tmp[`size $tmp`] = (capitalizeString($item));
	}
	$tmp = sort($tmp);

	for ($i=0;$i<`size $tmp`; $i++) {
		$alt = 0;
		for ($x=0;$x<`size $list`; $x++) {
			$str = substitute(`match "[!-z]" $list[$x]`, $list[$x], toupper(`match "[!-z]" $list[$x]`));
			if ($tmp[$i] == $list[$x] || $tmp[$i] == $str) {
				$result[`size $result`] = $list[$x];
				$alt = 1;
				break;
			}
		}
	}
	return $result;
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// al_getDirectories (path, filter, mode,)
// eg: al_getDirectories ("C:/temp/animationLib", "al_", 0)
// Result: al_test01 al_test02 al_test03
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

global proc string[] al_getDirectories (
	string $path,
	string $filter,
	string $alt)
{
	string $return[];
	string $assets[] = sort(`getFileList -fld ($path)`);

	for ($asset in $assets) {
		if (`gmatch $asset($filter+"*")`) {
			if ($alt)
				$asset = `substitute $filter $asset ""`;
			$return[size($return)] = $asset;
		}
	}
	return $return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// al_substituteStringArray (search[], array[], replace)
// eg: al_substituteStringArray ({"klr_", "al_"}, {"klr_a","al_b","al_c"}, "ph_");
// Result: ph_a ph_b ph_c
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

global proc string[] al_substituteStringArray (
	string $searches[],
	string $array[],
	string $replace)
{
	string $tmpStr, $return[];

	for ($string in $array) {
		for($search in $searches) {
			$string = `strip $string`;
			$string = `substitute $search $string $replace`;
		}
		$return[size($return)] = $string;
	}
	return $return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// al_padding
// eg: al_padding ("9", 3, "0")
// Result: 009
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

global proc string al_padding (
	string $str,
	int $pad,
	string $chr)
{
	string $return;

	for ($i=0;$i<$pad;$i++) {
		if (size($str)<=$i)
			$return += $chr;
	}
	return ($return + $str);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// al_uniqueId
// Result: 21432420070428
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

global proc string al_uniqueId()
{
	string $version = "v002";
	string $time = `about -ct`;
	string $date = `about -cd`;

	string $return = substituteAllString($time, ":", "");
	$return += substituteAllString($date , "/", "");
	$return += $version;

	return $return;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// al_removeAnimationLibrary_UniqueId
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

global proc al_removeAnimationLibrary_UniqueId ()
{
	string $references[] = `file -q -r`;
	string $namespace;

	for ($reference in $references) {
		$namespace = `file -q -ns $reference`;
		print $namespace;
		if (`match "temp_animationLibrary" $namespace` != "")
			file -rr $reference;
	}
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// al_deleteAnimationLibrary_UniqueId
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

global proc al_deleteAnimationLibrary_UniqueId () {

	string $list[] = `ls -type "animCurve"`;
	string $tmp[];


	for ($item in $list) {
		if (`objExists ($item + ".animationLibrary_uniqueId")`)
			if (!size(`ls -ro $item`))
				$tmp[`size $tmp`] = $item;
	}

	if (`size $tmp`)
		catch (`delete $tmp`);

}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// al_duplicateRefereceNodes_UniqueId
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

global proc al_duplicateAnimationLibrary_UniqueId () {

	string $list[] = `ls -type "animCurve"`;

	for ($item in $list) {
		if (`objExists ($item + ".animationLibrary_uniqueId")`)
			if (size(`ls -ro $item`))
				duplicate -rr $item;
	}
}

