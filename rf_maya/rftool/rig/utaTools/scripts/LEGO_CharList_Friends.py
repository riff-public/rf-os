#list all character for LEGO Project

import maya.cmds as mc
import maya.mel as mm
from functools import partial


def run():
	ui = LEGO_CharList()
	ui.show()

class LEGO_CharList(object):

	def __init__(self):
		
		self.ui = 'charList'
		self.win = '%sWin' %self.ui
		
		#declare ctrl sets
		
		self.superRootCtrl = ['placement_ctrl']
		self.rootCtrl 	= ['placement_ctrl', 'placementGmbl_ctrl']
		self.lf_legCtrl = ['ankleFkGmblLFT_ctrl','ankleFkLFT_ctrl','ankleRollIkLFT_ctrl','kneeIkLFT_ctrl','legIkGmblLFT_ctrl','legIkLFT_ctrl','legIkRootLFT_ctrl','legLFT_ctrl','legRbnLFT_ctrl','lowLegFkGmblLFT_ctrl','lowLegFkLFT_ctrl','lowLegRbnDtl1LFT_ctrl','lowLegRbnDtl2LFT_ctrl','lowLegRbnDtl3LFT_ctrl','lowLegRbnDtl4LFT_ctrl','lowLegRbnDtl5LFT_ctrl','lowLegRbnEndLFT_ctrl','lowLegRbnLFT_ctrl','lowLegRbnRootLFT_ctrl','toeFkGmblLFT_ctrl','toeFkLFT_ctrl','upLegFkGmblLFT_ctrl','upLegFkLFT_ctrl','upLegRbnDtl1LFT_ctrl','upLegRbnDtl2LFT_ctrl','upLegRbnDtl3LFT_ctrl','upLegRbnDtl4LFT_ctrl','upLegRbnDtl5LFT_ctrl','upLegRbnEndLFT_ctrl','upLegRbnLFT_ctrl','upLegRbnRootLFT_ctrl']	
		self.rt_legCtrl = ['ankleFkGmblRGT_ctrl','ankleFkRGT_ctrl','ankleRollIkRGT_ctrl','kneeIkRGT_ctrl','legIkGmblRGT_ctrl','legIkRGT_ctrl','legIkRootRGT_ctrl','legRGT_ctrl','legRbnRGT_ctrl','lowLegFkGmblRGT_ctrl','lowLegFkRGT_ctrl','lowLegRbnDtl1RGT_ctrl','lowLegRbnDtl2RGT_ctrl','lowLegRbnDtl3RGT_ctrl','lowLegRbnDtl4RGT_ctrl','lowLegRbnDtl5RGT_ctrl','lowLegRbnEndRGT_ctrl','lowLegRbnRGT_ctrl','lowLegRbnRootRGT_ctrl','toeFkGmblRGT_ctrl','toeFkRGT_ctrl','upLegFkGmblRGT_ctrl','upLegFkRGT_ctrl','upLegRbnDtl1RGT_ctrl','upLegRbnDtl2RGT_ctrl','upLegRbnDtl3RGT_ctrl','upLegRbnDtl4RGT_ctrl','upLegRbnDtl5RGT_ctrl','upLegRbnEndRGT_ctrl','upLegRbnRGT_ctrl','upLegRbnRootRGT_ctrl']	
		self.lf_armCtrl = ['armIkGmblLFT_ctrl','armIkLFT_ctrl','armIkRootLFT_ctrl','armLFT_ctrl','armRbnLFT_ctrl','elbowIkLFT_ctrl','forearmFkGmblLFT_ctrl','forearmFkLFT_ctrl','forearmRbnDtl1LFT_ctrl','forearmRbnDtl2LFT_ctrl','forearmRbnDtl3LFT_ctrl','forearmRbnDtl4LFT_ctrl','forearmRbnDtl5LFT_ctrl','forearmRbnEndLFT_ctrl','forearmRbnLFT_ctrl','forearmRbnRootLFT_ctrl','upArmFkGmblLFT_ctrl','upArmFkLFT_ctrl','upArmRbnDtl1LFT_ctrl','upArmRbnDtl2LFT_ctrl','upArmRbnDtl3LFT_ctrl','upArmRbnDtl4LFT_ctrl','upArmRbnDtl5LFT_ctrl','upArmRbnEndLFT_ctrl','upArmRbnLFT_ctrl','upArmRbnRootLFT_ctrl','wristFkGmblLFT_ctrl','wristFkLFT_ctrl']
		self.rt_armCtrl = ['armIkGmblRGT_ctrl','armIkRGT_ctrl','armIkRootRGT_ctrl','armRGT_ctrl','armRbnRGT_ctrl','elbowIkRGT_ctrl','forearmFkGmblRGT_ctrl','forearmFkRGT_ctrl','forearmRbnDtl1RGT_ctrl','forearmRbnDtl2RGT_ctrl','forearmRbnDtl3RGT_ctrl','forearmRbnDtl4RGT_ctrl','forearmRbnDtl5RGT_ctrl','forearmRbnEndRGT_ctrl','forearmRbnRGT_ctrl','forearmRbnRootRGT_ctrl','upArmFkGmblRGT_ctrl','upArmFkRGT_ctrl','upArmRbnDtl1RGT_ctrl','upArmRbnDtl2RGT_ctrl','upArmRbnDtl3RGT_ctrl','upArmRbnDtl4RGT_ctrl','upArmRbnDtl5RGT_ctrl','upArmRbnEndRGT_ctrl','upArmRbnRGT_ctrl','upArmRbnRootRGT_ctrl','wristFkGmblRGT_ctrl','wristFkRGT_ctrl']
		self.backCtrl 	= ['clavGmblLFT_ctrl','clavGmblRGT_ctrl','clavLFT_ctrl','clavRGT_ctrl','lowSpineRbnEnd_ctrl','lowSpineRbnRoot_ctrl','lowSpineRbn_ctrl','pelvisGmbl_ctrl','pelvis_ctrl','rootGmbl_ctrl','root_ctrl','spine1FkGmbl_ctrl','spine1Fk_ctrl','spine2FkGmbl_ctrl','spine2Fk_ctrl','spineIkGmbl_ctrl','spineIkOff_ctrl','spineIkRoot_ctrl','spineIk_ctrl','spineRbn_ctrl','spine_ctrl','upSpineRbnEnd_ctrl','upSpineRbnRoot_ctrl','upSpineRbn_ctrl']
		self.faceDtCtrl = ['cheekDtlLFT_ctrl','cheekDtlRGT_ctrl','cornerLipDtlLFT_ctrl','cornerLipDtlRGT_ctrl','eb1DtlLFT_ctrl','eb1DtlRGT_ctrl','eb2DtlLFT_ctrl','eb2DtlRGT_ctrl','eb3DtlLFT_ctrl','eb3DtlRGT_ctrl','eb4DtlLFT_ctrl','eb4DtlRGT_ctrl','ebDtl_ctrl','lid1DtlLFT_ctrl','lid1DtlRGT_ctrl','lid2DtlLFT_ctrl','lid2DtlRGT_ctrl','lid3DtlLFT_ctrl','lid3DtlRGT_ctrl','lid4DtlLFT_ctrl','lid4DtlRGT_ctrl','lid5DtlLFT_ctrl','lid5DtlRGT_ctrl','lid6DtlLFT_ctrl','lid6DtlRGT_ctrl','lid7DtlLFT_ctrl','lid7DtlRGT_ctrl','lid8DtlLFT_ctrl','lid8DtlRGT_ctrl','lowerLip1DtlLFT_ctrl','lowerLip1DtlRGT_ctrl','lowerLip2DtlLFT_ctrl','lowerLip2DtlRGT_ctrl','lowerLip3DtlLFT_ctrl','lowerLip3DtlRGT_ctrl','lowerLip4DtlLFT_ctrl','lowerLip4DtlRGT_ctrl','lowerLip5DtlLFT_ctrl','lowerLip5DtlRGT_ctrl','lowerLip6DtlLFT_ctrl','lowerLip6DtlRGT_ctrl','lowerLipDtl_ctrl','puffDtlLFT_ctrl','puffDtlRGT_ctrl','upperLip1DtlLFT_ctrl','upperLip1DtlRGT_ctrl','upperLip2DtlLFT_ctrl','upperLip2DtlRGT_ctrl','upperLip3DtlLFT_ctrl','upperLip3DtlRGT_ctrl','upperLip4DtlLFT_ctrl','upperLip4DtlRGT_ctrl','upperLip5DtlLFT_ctrl','upperLip5DtlRGT_ctrl','upperLip6DtlLFT_ctrl','upperLip6DtlRGT_ctrl','upperLipDtl_ctrl']
		self.hairCtrl 	= ['backHair1Gmbl_ctrl','backHair1_ctrl','backHair2Gmbl_ctrl','backHair2_ctrl','backHair3Gmbl_ctrl','backHair3_ctrl','frontHair1GmblLFT_ctrl','frontHair1GmblRGT_ctrl','frontHair1LFT_ctrl','frontHair1RGT_ctrl','frontHair2GmblLFT_ctrl','frontHair2GmblRGT_ctrl','frontHair2LFT_ctrl','frontHair2RGT_ctrl']
		self.blendshapeCtrl = ['eyeBsh_ctrl','mouthBsh_ctrl']
		self.headCtrl 	= ['eyeGmblLFT_ctrl','eyeGmblRGT_ctrl','eyeLFT_ctrl','eyeOfst_ctrl','eyeRGT_ctrl','eyeTrgtLFT_ctrl','eyeTrgtRGT_ctrl','eye_ctrl','headGmbl_ctrl','head_ctrl','jaw1GmblLWR_ctrl','jaw1LWR_ctrl','jaw2GmblLWR_ctrl','jaw2LWR_ctrl','lowerHead_ctrl','lowerTeeth_ctrl','mouthMove_ctrl','neck1FkGmbl_ctrl','neck1Fk_ctrl','neckRbnEnd_ctrl','neckRbnRoot_ctrl','neckRbn_ctrl','tongue1Gmbl_ctrl','tongue1_ctrl','tongue2Gmbl_ctrl','tongue2_ctrl','tongue3Gmbl_ctrl','tongue3_ctrl','tongue4Gmbl_ctrl','tongue4_ctrl','upperHead_ctrl','upperTeeth_ctrl']
		self.lf_fingerCtrl = ['index1FkLFT_ctrl','index2FkLFT_ctrl','index3FkLFT_ctrl','index4FkLFT_ctrl','indexLFT_ctrl','middle1FkLFT_ctrl','middle2FkLFT_ctrl','middle3FkLFT_ctrl','middle4FkLFT_ctrl','middleLFT_ctrl','pinky1FkLFT_ctrl','pinky2FkLFT_ctrl','pinky3FkLFT_ctrl','pinky4FkLFT_ctrl','pinkyLFT_ctrl','ring1FkLFT_ctrl','ring2FkLFT_ctrl','ring3FkLFT_ctrl','ring4FkLFT_ctrl','ringLFT_ctrl','thumb1FkLFT_ctrl','thumb2FkLFT_ctrl','thumb3FkLFT_ctrl','thumbLFT_ctrl']
		self.rt_fingerCtrl = ['index1FkRGT_ctrl','index2FkRGT_ctrl','index3FkRGT_ctrl','index4FkRGT_ctrl','indexRGT_ctrl','middle1FkRGT_ctrl','middle2FkRGT_ctrl','middle3FkRGT_ctrl','middle4FkRGT_ctrl','middleRGT_ctrl','pinky1FkRGT_ctrl','pinky2FkRGT_ctrl','pinky3FkRGT_ctrl','pinky4FkRGT_ctrl','pinkyRGT_ctrl','ring1FkRGT_ctrl','ring2FkRGT_ctrl','ring3FkRGT_ctrl','ring4FkRGT_ctrl','ringRGT_ctrl','thumb1FkRGT_ctrl','thumb2FkRGT_ctrl','thumb3FkRGT_ctrl','thumbRGT_ctrl']
		




		
		
	def show(self):
		
		if mc.window(self.win, exists = True):
			
			mc.deleteUI(self.win)
			
		mc.window(self.win, title = 'Char List Friends')
		tab = mc.tabLayout()
		
		# layout for Char tab =====================================================================================================================================================
		
		form = mc.formLayout('%s_MainForm' % self.ui)
		col1 = mc.columnLayout(adj = True, rs = 1)

		mc.text(l = 'Character lists')
		
		mc.textScrollList( '%sCharTSL' %self.ui, numberOfRows = 8, allowMultiSelection = True, w = 204)
		
		mc.frameLayout(borderStyle = 'etchedOut', lv = False) 
		mc.rowColumnLayout(nc = 3, cw = [(1, 100), (2, 100)])
		
		mc.radioCollection()
		mc.radioButton('%sCharRB' % self.ui, label='Char' , sl = True)
		mc.radioButton('%sPropRB' % self.ui,  label='Prop' )
		
		mc.setParent('..')
		mc.setParent('..')
		
		mc.frameLayout(borderStyle = 'etchedOut', lv = False) 
		mc.rowColumnLayout(nc = 3, cw = [(1, 100), (2, 100)])
		
		mc.text(l = 'Selection', al = 'left')
		mc.checkBox('%sSelectionCB' % self.ui, l = 'Add')
		
		mc.setParent('..')
		mc.setParent('..')
		
		mc.frameLayout(borderStyle = 'etchedOut', lv = False) 
		mc.rowColumnLayout(nc = 3, cw = [(1, 100), (2, 100)])
		
		mc.text(l = 'Isolate Selected', al = 'left')
		mc.checkBox('%sIsolateCB' % self.ui, l = 'On', en = 0 )
		
		mc.setParent('..')
		mc.setParent('..')

		mc.button( l = 'Update' , h = 30, c = partial(self.getCharList))
		
		mc.setParent('..')
		
		col2 = mc.columnLayout(adj = True, rs =2)
		mc.text(l = 'Selection')
		mc.button(l = 'Super Root', h = 30, c = partial(self.SelAllCtrl, 1, 0, 0))
		mc.button(l = 'All Ctrl', h = 30, c = partial(self.SelAllCtrl, 0, 1, 1))
		mc.button(l = 'All Ctrl no Root', h = 30, c = partial(self.SelAllCtrl, 0, 1, 0))
		mc.button(l = 'top grp', h = 30, c = partial(self.SelRigGrp))
		mc.text(l = '\nPositioning')
		mc.button(l = 'Spread Selection', h = 30, c = partial(self.spreadSelection))
		mc.button(l = 'Center Screen', h = 30, c = partial(self.centerScreen))
		
		mc.setParent('..')
		
		mc.formLayout('%s_MainForm' % self.ui, edit = True, attachForm = [(col1, 'left', 5), (col2, 'right', 5)], attachControl=[(col1, 'right', 5, col2)], attachPosition=[(col1, 'right', 5, 30), (col2, 'left', 5, 70)])
		mc.setParent('..')
		
		#layout for the camera tab ====================================================================================================================================
		
		form2 = mc.formLayout('%s_MainForm2' % self.ui)
		col3 = mc.columnLayout(adj = True, rs = 1)

		mc.text(l = 'Camera lists')
		
		mc.textScrollList( '%sCameraTSL' %self.ui, numberOfRows = 8, allowMultiSelection = True, w = 204)
		
		mc.frameLayout(borderStyle = 'etchedOut', lv = False) 
		mc.rowColumnLayout(nc = 2, cw = [(1, 100), (2, 100)])
		
		mc.radioCollection()
		mc.radioButton('%sIsolateRB' % self.ui, label='Isolate' , sl = True)
		mc.radioButton('%sViewRB' % self.ui,  label='View' )
		mc.checkBox('%sViewCB' % self.ui, label = 'View')
		
		mc.setParent('..')
		mc.setParent('..')
		
		mc.frameLayout(borderStyle = 'etchedOut', lv = False) 
		mc.rowColumnLayout(nc = 3, cw = [(1, 100), (2, 100)])
		
		#mc.text(l = 'Selection', al = 'left')
		#mc.checkBox('%sSelectionCB' % self.ui, l = 'Add')
		
		mc.setParent('..')
		mc.setParent('..')
		
		mc.frameLayout(borderStyle = 'etchedOut', lv = False) 
		mc.rowColumnLayout(nc = 3, cw = [(1, 100), (2, 100)])
		
		#mc.text(l = 'Isolate Selected', al = 'left')
		#mc.checkBox('%sIsolateCB' % self.ui, l = 'On', en = 0 )
		
		mc.setParent('..')
		mc.setParent('..')

		mc.button( l = 'Update' , h = 30, c = partial(self.listCamUI))
		
		mc.setParent('..')
		
		col4 = mc.columnLayout(adj = True, rs =2)
		mc.text(l = 'Camera')
		mc.button(l = 'Show All Cams', h = 30, c = partial(self.showAllCam))
		mc.button(l = 'Select Camera', h = 30, c = partial(self.selActiveCamera))
		mc.button(l = 'Focus Camera', h = 30, c = partial(self.focusToSelCamera))
		mc.text(l = '----')
		mc.button(l = 'Zoo Shots', h = 30, c = 'import maya.mel as mm\nmm.eval("zooShots")')
		#mc.text(l = '\nPositioning')
		#mc.button(l = 'Spread Selection', h = 30, c = partial(self.spreadSelection))
		#mc.button(l = 'Center Screen', h = 30, c = partial(self.centerScreen))
		
		mc.setParent('..')
		
		mc.formLayout('%s_MainForm2' % self.ui, edit = True, attachForm = [(col3, 'left', 5), (col4, 'right', 5)], attachControl=[(col3, 'right', 5, col4)], attachPosition=[(col3, 'right', 5, 30), (col4, 'left', 5, 70)])
		mc.setParent('..')
		
		# end 2 tabs layout ===================================================================================================================================
		
		mc.tabLayout( tab, edit=True, tabLabel=((form, 'Assets'), (form2, 'Camera')))
		
		mc.showWindow(self.win)
		mc.window(self.win, edit = True, wh = [330, 330])
		self.getCharList()
		self.listCamUI()

		
	
	
	def getCharList(self, arg = None):
		
		# getting all references in the scene
		
		allNamespace = mc.namespaceInfo(lon = True)
		allNamespace.remove('UI')
		allNamespace.remove('shared')
		
		# update all list to the textScrollList
		
		#clear list
		
		mc.textScrollList('%sCharTSL' %self.ui, edit = True, ra = True)

		# get the filter selected by UI
		
		filter = 'Char'
		filter2 = 'char'
		
		if mc.radioButton('%sCharRB' % self.ui, query = True, sl = True):
			filter = 'Char'
			filter2 = 'char'
			
		if mc.radioButton('%sPropRB' % self.ui, query = True, sl = True):
			filter = 'Prop'
			filter2 = 'prop'
		
		#check if it char or not
		
		for i in allNamespace:
		
			allGrp = '%s:Rig_Grp' % i
			
			#check if Rig_Grp exists			
			if mc.objExists(allGrp):
			
				#add assets that match the filter to the lists
						
				mc.textScrollList('%sCharTSL' %self.ui, edit = True, append = i)
			
		print ('%s added' % str(int(len(allNamespace)) - 1))
		
		
	# Below this line are selection set functions ###===========================================================
	
	# Call function with these parameter 
	# Sel (Root, SuperRoot, LF_leg, RT_leg, back, Head, LF_arm, RT_arm, LF_finger, RT_finger, blendshape, FaceDetails, Hair)

		
	def Sel(self, Root = 1, SuperRoot = 1, LF_leg = 1, RT_leg = 1, back = 1, Head = 1, LF_arm = 1, RT_arm = 1, LF_finger = 1, RT_finger = 1, blendshape = 1, FaceDetails = 1, Hair = 1):
	
	
		if mc.window(self.win, exists = True):
		
			selObj = mc.textScrollList('%sCharTSL' %self.ui, query = True, si = True)
			add = True
			
			if mc.checkBox('%sSelectionCB' % self.ui, query = True, v = True) :
			    add = False
			
			if (selObj) != None :
			
				mc.select(cl = add)
				
				for i in selObj:
				
					#Select root ctrl only
					
					if SuperRoot == 1:
					
						self.SelSelection(i, self.superRootCtrl)
					
					if Root == 1 :
					
						self.SelSelection(i, self.rootCtrl)
						
					if LF_leg == 1 :
					
						self.SelSelection(i, self.lf_legCtrl)
						
					if RT_leg == 1 :
					
						self.SelSelection(i, self.rt_legCtrl)
						
					if back == 1 :
					
						self.SelSelection(i, self.backCtrl)
						
					if Head == 1 :
					
						self.SelSelection(i, self.headCtrl)
						
					if LF_arm == 1 :
					
						self.SelSelection(i, self.lf_armCtrl)
						
					if RT_arm == 1 :
					
						self.SelSelection(i, self.rt_armCtrl)
						
					if LF_finger == 1 :
					
						self.SelSelection(i, self.lf_fingerCtrl)
						
					if RT_finger == 1 :
					
						self.SelSelection(i, self.rt_fingerCtrl)
						
					if blendshape == 1 :
					
						self.SelSelection(i, self.blendshapeCtrl)
						
					if FaceDetails == 1 :
					
						self.SelSelection(i, self.faceDtCtrl)
						
					if Hair == 1 :
					
						self.SelSelection(i, self.hairCtrl)
						
					
						
					
	def SelSelection(self, namespace, ctrl) :
		
		for i in ctrl : 
			
			mc.select('%s:%s' % (namespace, i), add = True)
			
			
			
	# isolate selected
	
	def SelRigGrp(self, arg = None) :
	
		if mc.window(self.win, exists = True):
		
			selObj = mc.textScrollList('%sCharTSL' %self.ui, query = True, si = True)
			
			if (selObj) != None :
			
				charSel = []
					
				for i in selObj:
					
					charSel.append('%s:Rig_Grp' % i)
					
				mc.select(charSel)
				
				
    # selection all ctrl by *Ctrl
	
	def SelAllCtrl(self, superRoot = 1, allCtrl = 1, rootSel = 1, arg = None) :
	
		if mc.window(self.win, exists = True):
		
			selObj = mc.textScrollList('%sCharTSL' %self.ui, query = True, si = True)
			
			if (selObj) != None :
			
				mc.select(cl = True)
					
				for i in selObj:
					
					if allCtrl == 1 : 
					
						selCtrl = mc.select('%s:*_Ctrl' % i, add = True)
						mc.select('%s:*_ctrl' % i, add = True)
					
						if rootSel == 0 : 
							
							mc.select('%s:SuperRoot_Ctrl' % i, d = True)
							mc.select('%s:Root_Ctrl' % i, d = True)
							
					if superRoot == 1 : 
					
						mc.select('%s:SuperRoot_Ctrl' % i, add = True)
					
				#mc.select(charSel)
				
        
	def centerScreen(self, arg = None) :
	
		if mc.window(self.win, exists = True):
		
			selObj = mc.textScrollList('%sCharTSL' %self.ui, query = True, si = True)
			
			if (selObj) != None :
					
				for i in selObj:
					
					ctrl = ('%s:SuperRoot_Ctrl' % i)
					self.centerScreenCmd(ctrl, 100, -3)
					

					
				
	def centerScreenCmd(self, obj, sample, step) :

		#create Temp Locator
		
		tempLoc = mc.spaceLocator(p = (0, 0, 0))
		
		#get current camera in the viewport
		
		panel = mc.getPanel(wf = True)
		cam = mc.modelPanel(panel, q = True, cam = True)
		
		mc.delete(mc.parentConstraint(cam, tempLoc[0]))
		
		for i in range(sample) : 
		
			mc.xform(tempLoc, r = True, os = True, t = (0, 0, step))
			height = mc.xform(tempLoc, q = True, ws = True, t = True)
			if height[1] < 0 : 
				mc.setAttr('%s.translateY' % tempLoc[0], 0)
				break
				
		tempCon = mc.pointConstraint(tempLoc[0], obj)
		currentPos = mc.xform(tempLoc[0], q = True, ws = True, t = True) 		
		mc.delete(tempCon)
		
		tempLoc2 = mc.spaceLocator(p = (0, 0, 0))
		mc.delete(mc.pointConstraint(cam, tempLoc2[0]))
		mc.setAttr('%s.translateY' % tempLoc2[0], 0) 
		
		tempAimCon = mc.aimConstraint(tempLoc2[0], obj, aimVector = (0, 0, 1), upVector = (0, 1, 0), worldUpType = 'vector', worldUpVector = (0, 1, 0))
		currentRot = mc.xform(tempLoc[0], q = True, ws = True, ro = True) 
		mc.delete(tempAimCon)
		mc.delete(tempLoc2[0])
		
		
		mc.xform(obj, ws = True, t = (currentPos[0], currentPos[1], currentPos[2]))
		mc.xform(obj, ws = True, ro = (currentRot[0], currentRot[1], currentRot[2]))
		
		mc.delete(tempLoc[0])
		mc.setAttr('%s.rotateX' % obj, 0)
		mc.setAttr('%s.rotateZ' % obj, 0)
		
		
	def spreadSelection(self, arg = None) : 
	
		self.SelAllCtrl(1, 0, 0)
		
		sel = mc.ls(sl = True)
		
		for i in range(len(sel)) : 
			mc.setAttr('%s.translateX' % sel[i], (i*2))
			

	def listCamUI(self, arg = None) : 
		allCam = mc.listCameras(p = True)
		for cam in allCam : 	
			if len(cam.split(':')) < 2 : 
				allCam.remove(cam)
			
		exclude = ['persp', 'master']
		for each in exclude : 
			if each in allCam : 
				
				allCam.remove(each)
			
		# fill textScrollList 
		
		mc.textScrollList('%sCameraTSL' %self.ui, e = True, ra = True)
		
		for eachCam in allCam : 

			print eachCam 
			mc.textScrollList('%sCameraTSL' %self.ui, e = True, append = eachCam, sc = partial(self.camCmd))
			

	def PT_ChannelControl(self, object, name, lock, hide):
	    mc.setAttr('%s.%s' % (object, name), k = hide)
	    mc.setAttr('%s.%s' % (object, name), lock = lock)
	    
	 
	def isolateCamCmd(self) : 
		allCam = mc.textScrollList('%sCameraTSL' %self.ui, q = True, ai = True)
		selList = mc.textScrollList('%sCameraTSL' %self.ui, q = True, si = True)
		
		for each in allCam : 
			camCtrl = each.split(':')
			camCtrl = ('%s:root_ctrl' %camCtrl[0])
			
			if selList[0] == each : 
				self.PT_ChannelControl(camCtrl, 'visibility', False, True)
				mc.setAttr('%s.visibility' % camCtrl, 1)
				self.PT_ChannelControl(camCtrl, 'visibility', True, False)
				
			else : 
				self.PT_ChannelControl(camCtrl, 'visibility', False, True)
				mc.setAttr('%s.visibility' % camCtrl, 0)
				self.PT_ChannelControl(camCtrl, 'visibility', True, False)
		

	def showAllCam(self, arg = None) : 
		allCam = mc.textScrollList('%sCameraTSL' %self.ui, q = True, ai = True)
		
		for each in allCam : 
			camCtrl = each.split(':')
			camCtrl = ('%s:root_ctrl' %camCtrl[0])
			
			self.PT_ChannelControl(camCtrl, 'visibility', False, True)
			mc.setAttr('%s.visibility' % camCtrl, 1)
			self.PT_ChannelControl(camCtrl, 'visibility', True, False)
			

	def camCmd(self) : 
		filter = ''
		
		if mc.radioButton('%sIsolateRB' % self.ui, query = True, sl = True):
			filter = 'Isolate'	
		if mc.radioButton('%sViewRB' % self.ui, query = True, sl = True):
			filter = 'View'
			
		if filter == 'Isolate' : 
		
			self.isolateCamCmd()
			
			if mc.checkBox('%sViewCB' % self.ui, q = True, v = True) : 
				
				self.setPlaybackRange()
			
		if filter == 'View' : 
			self.setPlaybackRange()
			

	def camRangeInfo(self, index) : 
	
		info = []
	
		if mc.objExists('shotsNode') : 
	
			attr = mc.listAttr('shotsNode', ud = True)
			
			excludeList = ['zooShots', 'globalInfo', 'masterCamera', 'version']
			
			for each in excludeList : 
				if each in attr : 
					attr.remove(each)
			
			keyword = 'shotCamera'
			
			for each in attr : 
				if keyword in each[0 : len(keyword)] :  
					attr.remove(each)
					
			cameraSize = len(attr)
			shotInfo = attr
			
			for i in range(len(attr)) :
				temp = mc.getAttr('shotsNode.%s' % attr[i])
				start = (temp.replace('. -start ', ''))
				
				end = 0
				if not i == (len(attr)-1) : 
					temp = mc.getAttr('shotsNode.%s' % attr[i+1])
					end = int((temp.replace('. -start ', ''))) - 1
				
				if i == (len(attr) - 1) : 
					temp = mc.getAttr('shotsNode.globalInfo')
					end = (temp.replace('. -end ', ''))
			
				temp = ('%s-%s' % (start, end))
				info.append(temp)
				
		else : 
		
			mm.eval('error (\"No Shot Node in the scnene\");')
				
		return info[index]
			
		
	def setPlaybackRange(self) : 
		
		selIndex = mc.textScrollList('%sCameraTSL' %self.ui, q = True, sii = True)
		
		playbackRange = self.camRangeInfo(int(selIndex[0]) - 1)
		
		temp = playbackRange.split('-')
		startFrame = temp[0]
		endFrame = temp[-1]

		mc.playbackOptions(min = startFrame, max = endFrame)
		mc.currentTime(startFrame)
		
		
		
	def selActiveCamera(self, arg = None) : 
	
		selList = mc.textScrollList('%sCameraTSL' %self.ui, q = True, si = True)
		cam = selList[0].split(':')
		mc.select('%s:root_ctrl' % cam[0])
		
	
	def focusToSelCamera(self, arg = None) : 
		
		selList = mc.textScrollList('%sCameraTSL' %self.ui, q = True, si = True)
		self.selActiveCamera()
		mm.eval('FrameSelected;')
		mm.eval('fitPanel -selected;')

run()