import maya.cmds as mc 
import maya.mel as mm
from functools import partial
import os
import sys
import shutil


def run() : 
	ui = PT_miniFix()
	ui.show()

class PT_miniFix : 
	def __init__(self) : 
		self.win = 'PT_miniFix'
		self.ui = '%s_Win' % self.win
		if sys.platform == 'win32' :
			self.projectPath = 'Z:/02_DESIGN/'
			self.drive = 'Z:/'
			self.browseList = ['01_CHARACTERS', '02_SETS', '03_PROPS', '04_VEHICLES']
			self.filter = ['03_MODEL', '04_RIG']
		if sys.platform == 'linux2' :
			self.template = '/dsPipe/Lego_Friends/film/LEGO_Friends_EP02_147762/sequenceTemplate/'
			self.projectPath = '/dsPipe/Lego_Friends/film/'
			self.drive = '/dsPipe/'
		
	def show(self) : 
		if mc.window(self.ui, exists = True) : 
			mc.deleteUI(self.ui)
			
		mc.window(self.ui, title = 'PT MiniFix v.1.0', s = True, wh = [600, 500])
		mc.columnLayout(adj = 1, rs = 4)

		mc.text(l = 'miniFix')
		mc.button(l = 'Fix Scale', c = partial(self.minifigScale))
		mc.checkBox('%s_chiCB' % self.win, l = 'Chi')
		mc.button(l = 'Fix Head', c = partial(self.miniFigHead2))
		mc.text(l = 'head geo')
		mc.textField('%s_headGeoTX' % self.win, tx = 'Laval_MiniFigHead_geo')
		mc.text(l = 'skinCluster')
		mc.textField('%s_skinClusterTX' % self.win, tx = 'skinCluster20')
		mc.button(l = 'Head World', c = partial(self.miniFigHeadWorld))
		mc.button(l = 'Hand Fix', c = partial(self.miniFigHandFix))
		mc.button(l = 'Add Mount Point', c = partial(self.miniFigAddMountPoint))
		mc.button(l = 'Add Cone', c = partial(self.miniFigAddCone))
		mc.text(l = 'name')
		mc.textField('%s_nameTX' % self.win, tx = 'laval01')
		mc.button(l = 'Add Name Tag', c = partial(self.miniFigAddNameTag))
		
		mc.text(l = 'Char_Set')
		mc.button(l = 'Add Char_Set')
		mc.button(l = 'Add Extra', c = partial(self.miniFigCharSet, 'extra'))

		mc.optionMenu('%s_OP' % self.win)
		mc.menuItem(l = 'cape')
		mc.menuItem(l = 'tail')
		mc.menuItem(l = 'extra')
		mc.menuItem(l = 'cape2')
		mc.menuItem(l = 'wing')
		mc.menuItem(l = 'skunk')

		mc.button(l = 'Add Option', c = partial(self.miniFigCharSet, 'other'))

		mc.showWindow()
		mc.window(self.ui, e = True, wh = [200, 490])


	def minifigScale(self, arg = None) : 
		
		chi = 0 
		if mc.checkBox('%s_chiCB' % self.win, q = True, v = True) : 
			chi = 1
		
		rootCtrl = 'Root_Ctrl'
		mdvNode = mc.createNode('multiplyDivide', n = 'backStretchScalable_mdv')
		
		mc.connectAttr('%s.scaleX' % rootCtrl, '%s.input1X' % mdvNode, f = True) 
		mc.setAttr('%s.input2X' % mdvNode, 1.15)
		mc.connectAttr('%s.outputX' % mdvNode, 'multiplyDivide1.input2X', f = True)
		
		mdvNode2 = mc.createNode('multiplyDivide', n = 'neckStretchScalable_mdv')
		mc.connectAttr('%s.scaleX' % rootCtrl, '%s.input1X' % mdvNode2, f = True) 
		mc.setAttr('%s.input2X' % mdvNode2, 0.147)
		mc.connectAttr('%s.outputX' % mdvNode2, 'multiplyDivide7.input2X', f = True)
		
		mc.scaleConstraint(rootCtrl, 'Utility_GRP|Joint_GRP', mo = True)
		mc.setAttr('IKhandels_GRP.scaleX', k = True)
		mc.setAttr('IKhandels_GRP.scaleY', k = True)
		mc.setAttr('IKhandels_GRP.scaleZ', k = True)
		mc.setAttr('IKhandels_GRP.sx', lock = False)
		mc.setAttr('IKhandels_GRP.sy', lock = False)
		mc.setAttr('IKhandels_GRP.sz', lock = False)
		
		mc.scaleConstraint(rootCtrl, 'IKhandels_GRP', mo = True)
		
		mc.setAttr('Utility_GRP|Cluster_GRP.scaleX', k = True)
		mc.setAttr('Utility_GRP|Cluster_GRP.scaleY', k = True)
		mc.setAttr('Utility_GRP|Cluster_GRP.scaleZ', k = True)
		
		mc.setAttr('Utility_GRP|Cluster_GRP.sx', lock = False)
		mc.setAttr('Utility_GRP|Cluster_GRP.sy', lock = False)
		mc.setAttr('Utility_GRP|Cluster_GRP.sz', lock = False)
		
		mc.scaleConstraint(rootCtrl, 'Utility_GRP|Cluster_GRP', mo = True)
		
		if(chi == 1) : 
			mc.setAttr('chi_ctrl.scaleX', k = True)
			mc.setAttr('chi_ctrl.scaleY', k = True)
			mc.setAttr('chi_ctrl.scaleZ', k = True)
			
			mc.setAttr('chi_ctrl.sx', lock = False)
			mc.setAttr('chi_ctrl.sy', lock = False)
			mc.setAttr('chi_ctrl.sz', lock = False)
			
			mc.scaleConstraint(rootCtrl, 'chi_ctrl', mo = True)
			
			mc.setAttr ('chi_ctrl.sx', keyable = False, channelBox = False)
			mc.setAttr ('chi_ctrl.sy', keyable = False, channelBox = False)
			mc.setAttr ('chi_ctrl.sz', keyable = False, channelBox = False)
		
	def miniFigNeckFix(self, arg = None) : 

		skinClusterNode = mc.textField('%s_headGeoTX' % self.win, q = True, tx = True)
		head = mc.textField('%s_skinClusterTX' % self.win, q = True, tx = True)

		mc.disconnectAttr('multiplyDivide8.outputX', 'Head3BND.translateX')
		mc.disconnectAttr('multiplyDivide10.outputX', 'Head5BND.translateX')
		mc.disconnectAttr('multiplyDivide9.outputX', 'Head4BND.translateX')
		mc.delete('neck_IK')
		mc.parentConstraint('Head_Ctrl', 'Head2BND', mo = True)
		mc.skinPercent( skinClusterNode, ['%s.vtx[368:390]' % head, '%s.vtx[740]' % head, '%s.vtx[749]' % head, '%s.vtx[802]' % head], transformValue=[('Head1BND', 0.2), ('Head3BND', 0.8)])
		mc.skinPercent( skinClusterNode, ['%s.vtx[414:436]' % head, '%s.vtx[741]' % head, '%s.vtx[748]' % head, '%s.vtx[803]' % head], transformValue=[('Head1BND', 0.4), ('Head3BND', 0.6)])
		
	def miniFigHeadWorld(self, arg = None) : 
		mc.orientConstraint('Root_Ctrl', 'headWorldFKArm_LOC')
		
		
	def miniFigHandFix(self, arg = None) : 

		L_Pinky_Ctrl = mc.duplicate('L_Pinky_Ctrl')
		L_Thumb_Ctrl = mc.duplicate('L_Thumb_Ctrl')
		
		R_Pinky_Ctrl = mc.duplicate('R_Pinky_Ctrl')
		R_Thumb_Ctrl = mc.duplicate('R_Thumb_Ctrl')
		
		mc.connectAttr('plusMinusAverage2.output3D', '%s.rotate' % L_Pinky_Ctrl[0])
		mc.connectAttr('%s.rotate' % L_Pinky_Ctrl[0], 'LhandPinkyMDN.input1', f = True)
		
		mc.connectAttr('plusMinusAverage1.output3D', '%s.rotate' % L_Thumb_Ctrl[0])
		mc.connectAttr('%s.rotate' % L_Thumb_Ctrl[0], 'LhandThumbMDN.input1', f = True)
		
		mc.connectAttr('plusMinusAverage3.output3D', '%s.rotate' % R_Pinky_Ctrl[0])
		mc.connectAttr('%s.rotate' % R_Pinky_Ctrl[0], 'RhandPinkyMDN.input1', f = True)
		
		mc.connectAttr('plusMinusAverage4.output3D', '%s.rotate' % R_Thumb_Ctrl[0])
		mc.connectAttr('%s.rotate' % R_Thumb_Ctrl[0], 'RhandThumbMDN.input1', f = True)
		
		mc.setAttr('L_Thumb_Ctrl_Grp.visibility', 0)
		mc.setAttr('L_Pinky_Ctrl_Grp.visibility', 0)
		mc.setAttr('R_Thumb_Ctrl_Grp.visibility', 0)
		mc.setAttr('R_Pinky_Ctrl_Grp.visibility', 0)
		
	def miniFigAddMountPoint(self, arg = None) : 
		ctrl = 'Chest_Ctrl'
		ctrlGrp = 'Ctrl_Grp'
		self.customAddAttr(ctrl, 'Mount_Point', 'long', 0, 1 , 1)
		self.customAddAttr(ctrl, 'L_hand_mnt', 'long', 0, 1, 0)
		self.customAddAttr(ctrl, 'R_hand_mnt', 'long', 0, 1, 0)
		L_Ctrl = self.boxCurve('L_HandMount_Ctrl')
		R_Ctrl = self.boxCurve('R_HandMount_Ctrl')
		
		mc.setAttr('%s.Mount_Point' % ctrl, lock = True) 
		
		L_handCtrlGrp = mc.group(L_Ctrl, n = 'L_HandMountCtrlGr')
		R_handCtrlGrp = mc.group(R_Ctrl, n = 'R_HandMountCtrlGr')
		
		mc.xform(L_handCtrlGrp, ws = True, s = [0.6, 0.6, 0.6])
		mc.xform(R_handCtrlGrp, ws = True, s = [0.6, 0.6, 0.6])
		mc.makeIdentity(L_handCtrlGrp, apply = True, t = 1, r = 1, s = 1, n = 0)
		mc.makeIdentity(R_handCtrlGrp, apply = True, t = 1, r = 1, s = 1, n = 0)
		
		mc.xform(L_handCtrlGrp, ws = True, t = [1.889, 2.667, 0])
		mc.xform(L_handCtrlGrp, ws = True, ro = [0, 0, -15])
		
		mc.xform(R_handCtrlGrp, ws = True, t = [-1.889, 2.667, 0])
		mc.xform(R_handCtrlGrp, ws = True, ro = [0, 0, 15])
		
		mc.parentConstraint('L_Elbow_BND|L_Wrist_BND', L_handCtrlGrp, mo = True)
		mc.parentConstraint('R_Elbow_BND|R_Wrist_BND', R_handCtrlGrp, mo = True)
		
		mc.connectAttr('%s.L_hand_mnt' % ctrl, '%s.visibility' % L_handCtrlGrp)
		mc.connectAttr('%s.R_hand_mnt' % ctrl, '%s.visibility' % R_handCtrlGrp)
		
		mc.parent(L_handCtrlGrp, ctrlGrp)
		mc.parent(R_handCtrlGrp, ctrlGrp)

		
	def customAddAttr(self, Ctrl, attr, type, min, max, dv) : 
		mc.addAttr(Ctrl, ln = attr, min = min, max = max, dv = dv, at = type)
		mc.setAttr('%s.%s' % (Ctrl, attr), e = True, keyable = True)	
		
		
	def boxCurve(self, name) : 
		curve = mm.eval('curve -d 1 -p -0.5 0.5 0.5 -p -0.5 0.5 -0.5 -p 0.5 0.5 -0.5 -p 0.5 0.5 0.5 -p -0.5 0.5 0.5 -p -0.5 -0.5 0.5 -p -0.5 -0.5 -0.5 -p -0.5 0.5 -0.5 -p 0.5 0.5 -0.5 -p 0.5 -0.5 -0.5 -p 0.5 -0.5 0.5 -p 0.5 0.5 0.5 -p -0.5 0.5 0.5 -p -0.5 -0.5 0.5 -p 0.5 -0.5 0.5 -p 0.5 -0.5 -0.5 -p -0.5 -0.5 -0.5 -p -0.5 -0.5 0.5 -k 0 -k 1 -k 2 -k 3 -k 4 -k 5 -k 6 -k 7 -k 8 -k 9 -k 10 -k 11 -k 12 -k 13 -k 14 -k 15 -k 16 -k 17 ;')
		curve = mc.rename(curve, name)
		return curve


	def addSubCharSetCape(self) : 
		mm.eval('setCurrentCharacters( {"Char_Set"} );')

		mc.select('cape_top_ctrl', 'cape_mid_ctrl', 'cape_bottom_ctrl', 'cape_top_cl', 'cape_top_cl', 'cape_top_cl', 'L_cape_mid_cl', 'cape_mid_cl', 'R_cape_mid_cl', 'R_cape_mid_cl', 'L_cape_bottom_cl', 'cape_bottom_cl', 'R_cape_bottom_cl')
		mm.eval('doCreateSubcharacterArgList 2 { "Cape","0","0","1","1","0","0" };')
		mm.eval('setCurrentCharacters( {} );')
		

	def addSubCharSetCape2(self) : 
		mm.eval('setCurrentCharacters( {"Char_Set"} );')

		mc.select('cape_top_ctrl', 'cape_mid_ctrl', 'cape_bottom_ctrl', 'cape_top_cl', 'cape_top_cl', 'cape_top_cl', 'L_cape_mid_cl', 'cape_mid_cl', 'R_cape_mid_cl', 'R_cape_mid_cl', 'L_cape_bottom_cl', 'cape_bottom_cl', 'R_cape_bottom_cl')
		mm.eval('doCreateSubcharacterArgList 2 { "Cape","0","0","1","1","0","0" };')
		mm.eval('setCurrentCharacters( {} );')
		

	def addSubCharSetTail(self) : 
		mm.eval('setCurrentCharacters( {"Char_Set"} );')
		mc.select('tailIK_mid_ctrl', 'tailIK_ctrl', 'tail04_ctrl', 'tail03_ctrl', 'tail02_ctrl', 'tail01_ctrl', 'tailAttributes_ctrl')
		mm.eval('doCreateSubcharacterArgList 2 { "Tail","0","0","1","1","0","0" };')
		mm.eval('setCurrentCharacters( {} );')
		
		
	def addSubCharSetWing(self) : 
		mm.eval('setCurrentCharacters( {"Char_Set"} );')
		mc.select('L_wing01_ctrl', 'L_wing02_ctrl', 'L_wing03_ctrl', 'L_wing04_ctrl', 'R_wing01_ctrl', 'R_wing02_ctrl', 'R_wing03_ctrl', 'R_wing04_ctrl')
		mm.eval('doCreateSubcharacterArgList 2 { "Wing","0","0","1","1","0","0" };')
		mm.eval('setCurrentCharacters( {} );')
		mc.character('Pelvis_Ctrl.L_wingFold', 'Pelvis_Ctrl.R_wingFold', 'Pelvis_Ctrl.L_wingFoldB', 'Pelvis_Ctrl.R_wingFoldB', add = 'Wing')
		
		
	def addSubCharSetSkunk(self) : 
		mm.eval('setCurrentCharacters( {"Char_Set"} );')
		mc.select('tail01_ctrl', 'tail02_ctrl_zero', 'tail02_ctrl', 'tail03_ctrl', 'tail04_ctrl', 'tail05_ctrl', 'tail06_ctrl', 'tail07_ctrl')
		mm.eval('doCreateSubcharacterArgList 2 { "Skunk","0","0","1","1","0","0" };')
		mm.eval('setCurrentCharacters( {} );')
		
		
		

	def addExtraSubCharSet(self) : 
		mm.eval('setCurrentCharacters( {"Char_Set"} );')
		mc.select('face_ctrl', 'L_finger_ctrl', 'R_finger_ctrl', 'L_HandMount_Ctrl','R_HandMount_Ctrl')
		mm.eval('doCreateSubcharacterArgList 2 { "Extra","0","0","1","1","0","0" };')
		mm.eval('setCurrentCharacters( {} );')
		
		
	def miniFigCharSet(self, type, arg = None) : 
		mc.select(cl = True)

		choice = mc.optionMenu('%s_OP' % self.win, q = True, v = True)

		if type == 'extra' : 
			self.addExtraSubCharSet()

		if type == 'other' : 

			if choice == 'cape' : 
				self.addSubCharSetCape()
				
			if choice == 'tail' : 
				self.addSubCharSetTail()
					
			if choice == 'cape2' : 
				self.addSubCharSetCape2()
				
			if choice == 'wing' : 
				self.addSubCharSetWing()
				
			if choice == 'skunk' : 
			    self.addSubCharSetSkunk()
			
			
	def miniFigAddCone(self, arg = None) : 
		ctrl = 'SuperRoot_Ctrl'
		Proxy = 'Proxy_Grp'
		L_cone = mc.polyCone(n = 'L_footFloorCheck_Geo', r = 0.25, h = 0.5, sx = 20, sy = 1, sz = 0, ax = [0, 1, 0], rcp = 0, cuv = 3, ch = 1)
		R_cone = mc.polyCone(n = 'R_footFloorCheck_Geo', r = 0.25, h = 0.5, sx = 20, sy = 1, sz = 0, ax = [0, 1, 0], rcp = 0, cuv = 3, ch = 1)
		L_coneGrp = mc.group(L_cone, n = 'L_FootCheckGeoGrp')
		R_coneGrp = mc.group(R_cone, n = 'R_FootCheckGeoGrp')
		
		mc.parent(L_coneGrp, Proxy)
		mc.parent(R_coneGrp, Proxy)
		
		mc.xform(L_coneGrp, ws = True, t = [0.45, -0.2, 0])
		mc.xform(R_coneGrp, ws = True, t = [-0.45, -0.2, 0])
		
		
		mc.parentConstraint('L_Ankle_BND', L_coneGrp, mo = True)
		mc.parentConstraint('R_Ankle_BND', R_coneGrp, mo = True)
		
		node = mc.shadingNode('surfaceShader', asShader=True, n = 'tmp_Shd')
		mc.setAttr('%s.outColor' % node, 1, 0, 0)
		mc.select(L_cone, R_cone)
		mc.hyperShade(assign = node )
		
		self.customAddAttr(ctrl, 'Extras', 'long', 0, 1 , 1)
		self.customAddAttr(ctrl, 'IntersectionCheck', 'long', 0, 1 , 0)
		mc.setAttr('%s.Extras' % ctrl, lock = True) 
		
		mc.connectAttr('%s.IntersectionCheck' % ctrl, '%s.visibility' % L_coneGrp, f = True)
		mc.connectAttr('%s.IntersectionCheck' % ctrl, '%s.visibility' % R_coneGrp, f = True)
		
		
	def addFaceExtra(self, arg = None) : 
		mc.select()
		mc.character('jaw_ctrl', 'Razcal_L_Eye_Ctrl', 'Razcal_R_Eye_Ctrl', add = 'Extras')  
		
		
	def miniFigAddNameTag(self, arg = None) : 

		scaleV = 0.06
		name = mc.textField('%s_nameTX' % self.win, q = True, tx = True)
		Proxy = 'Proxy_Grp'
		ctrl = 'SuperRoot_Ctrl'
		text = mc.textCurves(ch = 0, f = 'Tahoma|w400|h-11', t = name)
		text2 = mc.textCurves(ch = 0, f = 'Tahoma|w400|h-11', t = name)
		insideElement = mc.listRelatives(text)
		insideElement2 = mc.listRelatives(text2)
		groupName = mc.group(n = '%sNameTagFront_Grp' % name, em = True)
		groupName2 = mc.group(n = '%sNameTagBack_Grp' % name, em = True)
		
		textNode = mc.shadingNode('surfaceShader', asShader=True, n = 'name_Shd')
		shape = []
		
		for i in range(len(insideElement)) : 
			geo = mc.planarSrf(insideElement[i], name = '%s_Trim' % insideElement[i], ch = 1, tol = 0.01, o = True, po = 0)
			geo2 = mc.planarSrf(insideElement2[i], name = '%s_Trim' % insideElement2[i], ch = 1, tol = 0.01, o = True, po = 0)
			mc.parent(geo, groupName)
			mc.parent(geo2, groupName2)
			mc.delete(insideElement[i], ch = True)
			mc.delete(insideElement2[i], ch = True)
			mc.select(insideElement[i])
			
			shape = mc.listRelatives(geo, s = True , ad=True )

			mc.setAttr('%s.castsShadows' % shape[0], 0)
			mc.setAttr('%s.receiveShadows' % shape[0], 0)
			mc.setAttr('%s.motionBlur' % shape[0], 0)
			mc.setAttr('%s.primaryVisibility' % shape[0], 0)
			mc.setAttr('%s.smoothShading' % shape[0], 0)
			mc.setAttr('%s.visibleInReflections' % shape[0], 0)
			mc.setAttr('%s.visibleInRefractions' % shape[0], 0)
			mc.setAttr('%s.doubleSided' % shape[0], 0)
			
			shape2 = mc.listRelatives(geo2, s = True , ad=True )

			mc.setAttr('%s.castsShadows' % shape2[0], 0)
			mc.setAttr('%s.receiveShadows' % shape2[0], 0)
			mc.setAttr('%s.motionBlur' % shape2[0], 0)
			mc.setAttr('%s.primaryVisibility' % shape2[0], 0)
			mc.setAttr('%s.smoothShading' % shape2[0], 0)
			mc.setAttr('%s.visibleInReflections' % shape2[0], 0)
			mc.setAttr('%s.visibleInRefractions' % shape2[0], 0)
			mc.setAttr('%s.doubleSided' % shape2[0], 0)
			
			
		mc.select(groupName, groupName2) 
		mc.hyperShade(assign = textNode)
		
		mc.select(groupName, groupName2)
		mm.eval('CenterPivot;')
		mc.delete(text)
		mc.delete(text2)
		mc.xform(groupName, groupName2, ws = True, s = [scaleV, scaleV, scaleV])
		mc.delete(mc.parentConstraint('Chest_Ctrl', groupName))
		mc.delete(mc.parentConstraint('Chest_Ctrl', groupName2))
		mc.xform(groupName, ws = True, r = True, t = [0, 0, 0.38])
		mc.xform(groupName2, ws = True, r = True, t = [0, 0, -0.41])
		mc.xform(groupName, ws = True, ro = [-8, 0, 0])
		mc.xform(groupName2, ws = True, ro = [-8, 180, 0])
		
		nameGrp = mc.group(n = 'nameTag_Grp', em = True)
		mc.setAttr('%s.overrideEnabled' % nameGrp, 1)
		mc.setAttr('%s.overrideDisplayType' % nameGrp, 2)
		
		mc.parent(groupName, nameGrp)
		mc.parent(groupName2, nameGrp)
		mc.parent(nameGrp, Proxy)
		mc.parentConstraint('Chest_Ctrl', groupName, mo = True)
		mc.scaleConstraint('Chest_Ctrl', groupName, mo = True)
		mc.parentConstraint('Chest_Ctrl', groupName2, mo = True)
		mc.scaleConstraint('Chest_Ctrl', groupName2, mo = True)
		
		self.customAddAttr(ctrl, 'nameTag', 'long', 0, 1 , 1)
		mc.connectAttr('%s.nameTag' % ctrl, '%s.visibility' % nameGrp, f = True)
		
		
	def miniFigHead2(self, arg = None) : 


		head = mc.textField('%s_headGeoTX' % self.win, q = True, tx = True)
		skinClusterNode = mc.textField('%s_skinClusterTX' % self.win, q = True, tx = True)

		oldJnt = ['Head2IK', 'Head3IK', 'Head4IK', 'Head5IK']
		BNDJnt = ['Head2BND', 'Head3BND', 'Head4BND', 'Head5BND']
		newJnt = ['Head2FK', 'Head3FK', 'Head4FK', 'Head5FK']
		for i in range(len(oldJnt)) : 
			ikJnt = mc.createNode('joint', n = oldJnt[i])
			fkJnt = mc.createNode('joint', n = newJnt[i])
			mc.xform(ikJnt, ws = True, ro = [0, 180, -90])
			mc.xform(fkJnt, ws = True, ro = [0, 180, -90])
			mc.makeIdentity(ikJnt, apply = True, t = True, r = True, s = True, n = False)
			mc.makeIdentity(fkJnt, apply = True, t = True, r = True, s = True, n = False)
			mc.setAttr('%s.radius' % ikJnt, 0.1)
			mc.setAttr('%s.radius' % fkJnt, 0.1)
			mc.delete(mc.parentConstraint(BNDJnt[i], ikJnt))
			mc.delete(mc.parentConstraint(BNDJnt[i], fkJnt))
			
			print mc.xform(ikJnt, q = True, ro = True)
			
			if not i == 0 : 
				mc.parent(oldJnt[i], oldJnt[i-1])
				mc.parent(newJnt[i], newJnt[i-1])
				
		
				
		mc.parent(oldJnt[0], 'Head1BND')
		mc.parent(newJnt[0], 'Head1BND')
		
		reverseNode = mc.createNode('reverse', n = 'headJntSwitch_rsv')
		
		#add attribute to head control
		mc.addAttr('Head_Ctrl', ln = 'fixHead', at = 'double', min = 0, max = 1, dv = 0)
		mc.setAttr('Head_Ctrl.fixHead', e = True, keyable = True)
		
		mc.connectAttr('Head_Ctrl.fixHead', '%s.inputX' % reverseNode)

		# disconnected old jnt
		mc.disconnectAttr('multiplyDivide8.outputX', '%s.translateX' % BNDJnt[1])
		mc.disconnectAttr('multiplyDivide10.outputX', '%s.translateX' % BNDJnt[3])
		mc.disconnectAttr('multiplyDivide9.outputX', '%s.translateX' % BNDJnt[2])
		
		mc.setAttr('%s.rotateZ' % newJnt[0], 0)
		
		for i in range(len(BNDJnt)) : 
			constraintNode = mc.parentConstraint(oldJnt[i], newJnt[i], BNDJnt[i])
			mc.connectAttr('%s.outputX' % reverseNode, '%s.w0' % constraintNode[0])
			mc.connectAttr('Head_Ctrl.fixHead', '%s.w1' % constraintNode[0])
		
		#make ikspline for oldJnt 
		spineIK = mc.ikHandle(sj=oldJnt[0], ee=oldJnt[3], c = 'neck_curve', sol = 'ikSplineSolver', ccv = False, pcv = False)
		mc.parent(spineIK[0], 'IKhandels_Grp')
		
		#reconnect spine IK
		mc.connectAttr('neck_twist_pma.output1D', '%s.twist' % spineIK[0], f = True)
		
		mc.connectAttr('multiplyDivide8.outputX', '%s.translateX' % oldJnt[1], f = True)
		mc.connectAttr('multiplyDivide9.outputX', '%s.translateX' % oldJnt[2], f = True)
		mc.connectAttr('multiplyDivide10.outputX', '%s.translateX' % oldJnt[3], f = True)
		
		mc.delete('neck_IK')
		mc.rename(spineIK[0], 'neck_IK')
		


		mc.parentConstraint('Head_Ctrl', newJnt[0], mo = True)
		
		# correct skin 
		mc.skinPercent( skinClusterNode, ['%s.vtx[368:390]' % head, '%s.vtx[740]' % head, '%s.vtx[749]' % head, '%s.vtx[802]' % head], transformValue=[('Head1BND', 0.2), ('Head3BND', 0.8)])
		mc.skinPercent( skinClusterNode, ['%s.vtx[414:436]' % head, '%s.vtx[741]' % head, '%s.vtx[748]' % head, '%s.vtx[803]' % head], transformValue=[('Head1BND', 0.4), ('Head3BND', 0.6)])
	



		


	

	
	

	
#minifigScale(1)
#miniFigNeckFix('mesh_head', 'skinCluster31')
#miniFigHeadWorld()
#miniFigHandFix()
#miniFigAddMountPoint()
#miniFigCharSet('extra')
#miniFigCharSet('cape')
#miniFigAddCone()
#miniFigAddNameTag('shadowind01', 0.06)
#miniFigHead2('Laval_MiniFigHead_geo', 'skinCluster20')