import maya.cmds as mc 
import maya.mel as mm
from functools import partial
import os.path
import maya.mel as mm
import sys

def run() : 
	ui = PT_replaceSel()
	ui.show()

class PT_replaceSel : 
	def __init__(self) : 
		self.win = 'PT_replaceSel'
		self.ui = '%s_Win' % self.win
		if sys.platform == 'win32' :
			self.path = 'Y:/USERS/Ta/clipBoard/'

		if sys.platform == 'linux2' :
			self.path = '/playground/USERS/Ta/clipBoard/'

		self.w = 260
		self.h = 384
		self.offset = 120
		

	def show(self) : 
		if mc.window(self.ui, exists = True) : 
			mc.deleteUI(self.ui)
			
		mc.window(self.ui, title = 'Easy Clip Board v.1.0', s = True, wh = [600, 500])
		mainForm = mc.columnLayout(adj = 1, rs = 4)
		mc.text(l = 'Original')
		mc.textField('%s_originalTX' % self.win)
		mc.button(l = 'Get Original', c = partial(self.getSelected))
		mc.text(l = 'name')
		mc.textField('%s_numberTX' % self.win)
		mc.button(l = 'Select', c = partial(self.selNumber))
		mc.checkBox('%s_deleteCB' % self.win, l = 'delete after copy', v = 1)
		mc.button(l = 'Replace', h = 30, c = partial(self.replaceSelected))
		mc.showWindow()
		mc.window(self.ui, e = True, wh = [200, 200])

	def getSelected(self, arg = None) :
		sel = mc.ls(sl = True)
		if sel :
			mc.textField('%s_originalTX' % self.win, e = True, tx = sel[0])

	def replaceSelected(self, arg = None) : 
		sels = mc.ls(sl = True, l = True)
		original = mc.textField('%s_originalTX' % self.win, q = True, tx = True)
		for each in sels : 
			dup = mc.duplicate(original)
			mc.delete(mc.parentConstraint(each, dup[0]))

			if mc.checkBox('%s_deleteCB' % self.win, q = True, v = True) : 
				mc.delete(each)
				
		mm.eval('print "%s object replaced";' % len(sels))
				
				
	def selNumber(self, arg = None) : 
		number = mc.textField('%s_numberTX' % self.win, q = True, tx = True)
		mc.select(number)
		sel = mc.ls(sl = True)
		transformList = []
		for each in sel : 
			if mc.nodeType(each) == 'transform' : 
				transformList.append(each)
				
		mc.select(transformList)
		mm.eval('print "%s object selected";' % len(transformList)) 