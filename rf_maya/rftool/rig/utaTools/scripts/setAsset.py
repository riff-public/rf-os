import maya.cmds as mc
from functools import partial
def run() :
    ui = SetAsset()
    ui.show()
    
    
class SetAsset :
    def __init__(self) : 
        self.win = 'genWin'
        self.ui = ('%s_ui' % self.win )
        
    def show(self) : 
        if mc.window(self.ui, exists = True) : 
            mc.deleteUI(self.ui)
        mc.window(self.ui, title = 'Asset Naming')
        mc.columnLayout(adj = True, rs = 2)
        mc.text(l = 'Asset Type')
        mc.textField('%s_AssetTypeTX' % self.win)
        mc.text(l = 'Asset Sub Type')
        mc.textField('%s_AssetSubTypeTX' % self.win)
        mc.text(l = 'Asset Name')
        mc.textField('%s_AssetNameTX' % self.win)
        mc.button(l = 'Get Setting', h = 30, c = partial(self.fillUI))
        mc.button(l = 'Assign Current', h = 30, c = partial(self.assignFromUI))
        
        mc.showWindow()
        mc.window('%s_ui' % self.win, edit = True, wh = [200, 180])
        
    def fillUI(self, arg = None) : 
        if mc.objExists('Rig_Grp') : 
            assetType = mc.getAttr('Rig_Grp.assetType')
            assetSubType = mc.getAttr('Rig_Grp.assetSubType')
            assetName = mc.getAttr('Rig_Grp.assetName')
            
            mc.textField(('%s_AssetTypeTX' % self.win), edit = True, tx = assetType)
            mc.textField(('%s_AssetSubTypeTX' % self.win), edit = True, tx = assetSubType)
            mc.textField(('%s_AssetNameTX' % self.win), edit = True, tx = assetName)
            

    def assignFromUI(self, arg = None) : 
        if mc.objExists('Rig_Grp') : 
            assetType = mc.textField('%s_AssetTypeTX' % self.win, q = True, tx = True)
            assetSubType = mc.textField('%s_AssetSubTypeTX' % self.win, q = True, tx = True)
            assetName = mc.textField('%s_AssetNameTX' % self.win, q = True, tx = True)
            mc.setAttr('Rig_Grp.assetType', assetType, type = 'string')
            mc.setAttr('Rig_Grp.assetSubType', assetSubType, type = 'string')
            mc.setAttr('Rig_Grp.assetName', assetName, type = 'string')
            