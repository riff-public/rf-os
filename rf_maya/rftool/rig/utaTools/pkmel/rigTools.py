import maya.cmds as mc
import maya.mel as mm

import os
import shutil
import sys
import re

import xml.etree.ElementTree as et
from xml.dom import minidom

from utaTools.pkmel import core as pc

reload(pc)

def getOrigShape(transform=''):
	
	'''
	Get origShape from given transform node
	'''

	origShp = None

	shps = mc.listRelatives(transform, s=True, f=True)

	if shps:
		for shp in shps:
			if mc.getAttr('%s.intermediateObject' % shp):
				connections = mc.listConnections(shp, s=True, d=False)
				if not connections:
					origShp = shp

	return origShp

def fixShapeNormal():

	sels = mc.ls(sl=True)

	for sel in sels:
		
		shps = mc.listRelatives(sel, s=True, f=True)
		currShp = ''
		isOrig = False
		
		if len(shps) > 1:
			for shp in shps:
				if mc.getAttr('%s.intermediateObject'%shp) and 'Orig' in shp:
					currShp = shp
					isOrig = True
		else:
			currShp = shps[0]
		
		if currShp:
			mc.setAttr('%s.intermediateObject' % currShp, 0)
			cmd = 'polyNormal -normalMode 0 -userNormalMode 0 -ch 1 %s;' % currShp
			mm.eval(cmd)
			mc.delete(currShp, ch=True)
			
			if isOrig:
				mc.setAttr('%s.intermediateObject' % currShp, 1)
			
		else:
			print 'There is no shape node for %s' % sel

	mc.select(sels, r=True)

def prettify( elem ):
	'''
	Return a pretty-printed XML string for the Element.
	'''
	rough_string = et.tostring( elem , 'utf-8' )
	reparsed = minidom.parseString( rough_string )
	return reparsed.toprettyxml( indent="  " )

def writeHierarchy( root='' ) :

	chdn = mc.listRelatives( root , ad=True , f=True , type='transform' )
	maxStep = 0

	topDownList = []
	for chd in chdn :
		currStep = chd.count( '|' )
		if currStep > maxStep :
			maxStep = currStep

	for ix in range( 1 , maxStep+1 ) :
		for chd in chdn :
			if chd.count( '|' ) == ix :
				topDownList.append( chd )

	rtElm = et.Element( root )
	for each in topDownList :

		nmList = each.split('|')
		currNm = nmList[-1]
		currPar = '|'.join( nmList[:-1] )

		chdElm = et.SubElement( rtElm , currNm )

		parElm = et.SubElement( chdElm , 'parent' )
		parElm.text = currPar

	print et.tostring( rtElm )
	print prettify( rtElm )

def transferUvToOrig() :
	
	'''
	Transfer UVs from first selected object to orig shape of last selected object.
	'''
	
	sels = mc.ls( sl=True )

	targetShapes = mc.listRelatives( sels[-1] , f=True , s=True )

	origShapes = []

	for targetShape in targetShapes :
		if mc.getAttr( '%s.intermediateObject' % targetShape ) and 'Orig' in targetShape :
			origShapes.append( targetShape )

	if len( origShapes ) == 1 :

		origShape = origShapes[0]
		mc.setAttr( '%s.intermediateObject' % origShape , 0 )

		mc.select( sels[0] , r=True )
		mc.select( origShape , add=True )

		cmd = 'transferAttributes -pos 0 -nml 0 -uvs 2 -col 2 -spa 4 -suv "map1" -tuv "map1" -sm 3 -fuv 0 -clb 1;'
		mm.eval( cmd )
		mc.delete( origShape , ch=True )
		mc.setAttr( '%s.intermediateObject' % origShape , 1 )
		mc.select( sels , r=True )
		
	else :
		print '%s has none or more than 1 orig shape.' % sels[1]

def getAllDeformationNode() :

	'''
	Return list of deformation nodes in current scene.
	'''
	defTools = (
					'skinCluster' ,
					'blendShape' ,
					'wire' ,
					'sculpt' ,
					'nonLinear' ,
					'softMod' ,
					'cluster' ,
					'wrap' ,
					'ffd'
				)

	defNodes = []	
	for defTool in defTools :
		currDefs = mc.ls( type=defTool )
		if currDefs :
			for currDef in currDefs :
				if not currDef in defNodes :
					defNodes.append( currDef )

	return defNodes

def dupAndCleanSelected() :
	for sel in mc.ls( sl=True , l=True ) :
		dupAndCleanDag( sel , '' , '' )

def dupAndCleanDag( dag='' , searchFor='' , replaceWith='' ) :

	'''
	Duplicate, cleanup and rename given 'dag' and its children.

	Disable all deformation tools in the current scene.
	Duplicate given DAG.
	Iterate through every child element then...
		1. Cleanup current geometry
		2. Rename current DAG with search and replace
	Cleanup and rename duplicated DAG
	Enable all deformation tools in the current scene.
	'''
	
	# Populate deformation nodes
	defNodes = getAllDeformationNode()	
	
	# Disable deformation nodes
	if defNodes :
		for defNode in defNodes :
			mc.setAttr( '%s.envelope' % defNode , 0 )
			print 'Disabled %s' % defNode

	dup = mc.duplicate( dag , rr=True )[0]
	chldn = mc.listRelatives(
								dup ,
								ad=True ,
								type='transform' ,
								f=True
							)

	# Cleanup and rename all child node.
	if chldn :
		for chld in sorted( chldn , reverse=True ) :

			cleanupModel( chld )
			currName = chld.split( '|' )[-1]

			if searchFor in currName :
				mc.rename( chld , currName.replace( searchFor , replaceWith ) )
			else :
				print '%s has no %s to replace' % ( currName , searchFor )

	# Cleanup and rename DAG node.
	cleanupModel( dup )
	dagName = dag.split( '|' )[-1]
	if searchFor in dagName :
		dup = mc.rename( dup , dagName.replace( searchFor , replaceWith ) )
	else :
		print '%s has no %s to replace' % ( dagName , searchFor )

	mc.select( dup , r=True )

	# Enable deformation nodes
	if defNodes :
		for defNode in defNodes :
			if mc.objExists( defNode ) :
				mc.setAttr( '%s.envelope' % defNode , 1 )
				print 'Enabled %s' % defNode

def findRelDefTools( shape='' ) :
	# Find deformation tools that are related to given shape.
	defTools = (
					'skinCluster' ,
					'blendShape' ,
					'wire' ,
					'sculpt' ,
					'nonLinear' ,
					'softMod' ,
					'cluster' ,
					'wrap' ,
					'ffd'
				)

	relDefTools = []
	objSets = mc.listConnections( shape , s=True , d=False , scn=True , type='objectSet' )

	for objSet in objSets :

		setConns = mc.listConnections( objSet , s=True , d=False , scn=True )

		for setConn in setConns :
			
			if ( mc.nodeType( setConn ) in defTools ) and ( not setConn in relDefTools ) :

				relDefTools.append( setConn )

	return relDefTools

def saveToHero() :
	'''
	Temporary script to save current version scene to hero scene.
	'''

	mayaFlPath = os.path.normpath( mc.file( q=True , sn=True ) )
	
	fldPath , flNmExt = os.path.split( mayaFlPath )
	flNm , flExt = os.path.splitext( flNmExt )

	exp = '(.*)(_v\d{3})'

	m = re.search( exp , flNm )
	pubFlNm = m.group(1)

	pubFlPath = os.path.join(
								fldPath ,
								'%s.ma' % pubFlNm
							)
	
	mc.file( rn=pubFlPath )

	importRigElement()
	removeAllNamespace()

	mc.file( s=True , f=True , type='mayaAscii' )
	removeUnusedNodeFromMaFile( pubFlPath )

def adjustRadius( default=1 ) :
	'''
	Adjust radius to each type of joint
	'''
	jnts = mc.ls( type='joint' )
	scaJnts = mc.ls( '*Sca*_jnt' )
	dtlJnts = mc.ls( '*RbnDtl*_jnt' )

	for jnt in jnts :
		rad = default
		if 'Sca' in jnt :
			rad = default*2
		elif 'RbnDtl' in jnt :
			rad = default*2
		elif 'RbnDtl3' in jnt :
			rad = rad*2
		else :
			rad = default

		try :
			mc.setAttr( '%s.radius' % jnt , rad )
		except :
			pass

def centerizedSelectedAndMirror() :
	
	'''
	Centerized selected mesh components and mirror the objects in X axis
	'''
	
	sels = mc.ls( sl=True , fl=True )
	objs = []
	
	for sel in sels :
		currObj = ''
		if '.' in sel :
			currObj = sel.split( '.' )[0]
		if currObj and not currObj in objs :
			objs.append( currObj )
	
	moveSelectedToCenter()
	
	for obj in objs :
		
		mirrorNode = mc.polyMirrorFace( obj , ws=True , p=[0,0,0] , direction=1 , mergeMode=1 , ch=True )[0]
		mc.setAttr( '%s.mergeThreshold' % mirrorNode , 0.0001 )
		
		mc.delete( obj , ch=True )

def centerizedSelected() :

	'''
	Centerized selected mesh components
	'''

	sels = mc.ls( sl=True , fl=True )

	mm.eval( 'PolySelectConvert 3;' )

	for sel in mc.ls( sl=True , fl=True ) :

		currPos = mc.xform( sel , q=True , t=True , ws=True )
		currPos[0] = 0
		mc.xform( sel , t=[currPos[0],currPos[1],currPos[2]] , ws=True )

	mc.select( sels , r=True )

def createMidJointFromSelected() :

	'''
	Duplicate the first selected joint
	and position it to the middle of selected nodes.
	'''

	sels = mc.ls( sl=True )

	dup = mc.duplicate( sels[0] , rr=True )[0]
	chldrn = mc.listRelatives( dup , f=True )

	if chldrn :
		for chld in chldrn :
			try :
				mc.delete( chld )
			except :
				pass

	mc.delete( mc.pointConstraint( sels , dup ) )
	mc.parent( dup , sels[0] )

	return dup

def createPreviewMaterial( mat='' , previewMaterialType='' ) :

	'''
	Create a preview material to given "mat" with "previewMaterialType"
	'''
	
	matOutColor = '%s.outColor' % mat
	matImgOut = None
	if mc.objExists( matOutColor ) :
		matImgOut = mc.listConnections( mat , s=True , d=False , p=True )[0]
	
	matInfo = mc.listConnections( mat , s=False , d=True , type='materialInfo' )[0]
	previewMat = mc.shadingNode( previewMaterialType , asShader=True )
	
	mc.connectAttr( '%s.message' % previewMat , '%s.texture' % matInfo , f=True )
	mc.connectAttr( '%s.message' % previewMat , '%s.material' % matInfo , f=True )
	
	if matImgOut :
		mc.connectAttr( matImgOut , '%s.color' % previewMat , f=True )
	
	mc.hyperShade( o=mat )
	objs = mc.ls( sl=True )
	
	if objs :
		cmd = 'hyperShade -assign %s;' % mat
		mm.eval( cmd )

def extendJoint( ax='y' , offset=1 ) :
	'''
	Create a child joint to selected joint(s)
	'''
	sels = mc.ls( sl=True , l=True )

	for sel in sels :

		dup = mc.duplicate( sel , rr=True )[0]
		chldrn = mc.listRelatives( dup , f=True )

		if chldrn :
			for chld in chldrn :
				try :
					mc.delete( chld )
				except :
					pass

		mc.parent( dup , sel )

		mc.setAttr( '%s.t%s' % ( dup , ax ) , offset )

def quickOrient( aimVec=[0,1,0] , upVec=[-1,0,0] ) :
	
	sels = mc.ls( sl=True )
	targ = sels[0]
	src = sels[1]
	
	chldrn = mc.listRelatives( src , s=False , c=True , p=False )

	if chldrn :
		for chld in chldrn :
			mc.parent( chld , w=True )
	
	localAim( targ , src , aimVec , upVec )
	
	if chldrn :
		for chld in chldrn :
			mc.parent( chld , src )
	
	mc.select( src , r=True )

def localAim( targ='' , src='' , aimVec=[0,1,0] , upVec=[-1,0,0] ) :

	tmpUp = mc.duplicate( src , rr=True )[0]
	tmpUpChldrn = mc.listRelatives( tmpUp , f=True )
	
	if tmpUpChldrn :
		mc.delete( tmpUpChldrn )

	aim = aimVec
	wu = upVec
	u = upVec
	wuo = tmpUp
	wut = 'objectrotation'

	aimCon = mc.aimConstraint( targ , src , aim=aim , wu=wu , wuo=wuo , wut=wut , u=u )
	mc.delete( aimCon )
	mc.delete( tmpUp )

def createBeforeBlendShape() :

	# Create blend shape node with 'before' option.
	# Basically, this is used for connectting animated geo to rendered geo.
	# Select source geo and target geo then run the procedure.

	sels = mc.ls( sl=True )

	localName = sels[-1].split( ':' )[-1]
	tmpNameList = localName.split( '_' )

	if len( tmpNameList ) > 1 :
		tmpNameList[-1] = 'bsn'
	else :
		tmpNameList.append( 'bsn' )

	bsn = '_'.join( tmpNameList )
	mc.blendShape( before=True , origin='world' , n=bsn )
	mc.setAttr( '%s.w[0]' % bsn , 1 )

def createFrontOfChainBlendShape() :

	# Create blend shape node with 'frontOfChain' option.
	# Select source geo and target geo then run the procedure.

	sels = mc.ls( sl=True )

	localName = sels[-1].split( ':' )[-1]
	tmpNameList = localName.split( '_' )

	if len( tmpNameList ) > 1 :
		tmpNameList[-1] = 'bsn'
	else :
		tmpNameList.append( 'bsn' )

	bsn = '_'.join( tmpNameList )
	mc.blendShape( frontOfChain=True , origin='local' , n=bsn )
	mc.setAttr( '%s.w[0]' % bsn , 1 )

def toLambert() :

	sels = mc.ls( sl=True )

	for sel in sels :

		lambert = mc.shadingNode( 'lambert' , asShader=True )
		sg = mc.listConnections( '%s.outColor' % sel )[0]
		fileNode = ''

		if mc.objExists( '%s.color' % sel ) :
			
			fileNodes = mc.listConnections( '%s.color' % sel )
			
			if fileNodes :
				
				fileNode = fileNodes[0]
		
		if fileNode :
			
			mc.connectAttr(
								'%s.outColor' % fileNode ,
								'%s.color' % lambert ,
								f=True
							)
		else :
			
			colorVal = mc.getAttr( '%s.color' % sel )[0]
			
			mc.setAttr( '%s.color' % lambert , colorVal[0] , colorVal[1] , colorVal[2] )

		mc.connectAttr(
								'%s.outColor' % lambert ,
								'%s.surfaceShader' % sg ,
								f=True
							)
		
		mc.delete( sel )
		mc.rename( lambert , sel )

def removeShadingNode() :

	types = (
				'file' ,
				'place2dTexture' ,
				'gammaCorrect' ,
				'sampleInfo' ,
				'renderLayer' ,
				'VRayLightMtl' ,
				'ramp' ,
				'VRayPlaceEnvTex' ,
				'VRayMtl' ,
				'VRayBlendMtl'
			)
	
	for type_ in types :
		
		nodes = mc.ls( type=type_ )
		
		if nodes :
			
			for node in nodes :
				try :
					mc.delete( node )
				except :
					pass

def createCleanedCamera() :

	# Select camera then run the script.
	# Script will duplicate selected camera.

	cleanedCam = 'cleanedNuke_cam'

	if mc.objExists( cleanedCam ) :
		mc.delete( cleanedCam )

	selectedCam = ''
	selectedCamShp = ''

	selected = mc.ls( sl=True , l=True )[0]
	
	if mc.nodeType( selected ) == 'camera' :
		selectedCamShp = selected
		selectedCam = mc.listRelatives( selected , p=True , f=True )[0]
	else :
		selectedCam = selected
		selectedCamShp = mc.listRelatives( selected , type='shape' , f=True )[0]

	mc.select( selectedCam )
	mc.duplicate( selectedCam , rr=True , n=cleanedCam )
	mc.parent( w=True )

	duppedCamShp = mc.listRelatives( cleanedCam , type='shape' , f=True )[0]

	for attr in ( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' ) :
		mc.setAttr( '%s.%s' % ( cleanedCam , attr ) , l=False )

	minTime = mc.playbackOptions( q=True , min=True )
	maxTime = mc.playbackOptions( q=True , max=True )

	animCurves = mc.listConnections( selectedCamShp , s=True , type='animCurve' )
	attrs = []
	if animCurves :
		for animCurve in animCurves :
			
			attr = mc.listConnections( animCurve , d=True , p=True )[0].split( '.' )[1]
			
			mc.copyKey( selectedCamShp , attribute=attr )
			mc.pasteKey( duppedCamShp , attribute=attr )
	
	parCons = mc.parentConstraint( selectedCam , cleanedCam )
	mc.bakeResults( cleanedCam , simulation=True , t=( minTime , maxTime ) )
	mc.delete( parCons )

def renameFileAndTextureNode( node='' , name='' , type_='' ) :

	currTxtPath = os.path.normpath( mc.getAttr( '%s.fileTextureName' % node ) )
	currTxtFldPath , currTxtNameExt = os.path.split( currTxtPath )
	currTxtName , currTxtExt = os.path.splitext( currTxtNameExt )

	newTxtName = '%s_%s' % ( name , type_ )
	newTxtPath = os.path.join(
								currTxtFldPath ,
								'%s%s' % ( newTxtName , currTxtExt )
							)
	os.rename( currTxtPath , newTxtPath )

	mc.setAttr( '%s.fileTextureName' % node , newTxtPath , type='string' )
	mc.rename( node , newTxtName )

def isKeywordInLine( kws=() , line='' ) :
	
	result = False
	for kw in kws :
		if kw in line :
			result = True
	
	return result

def removeUnusedNodeFromMaFile( filePath='' ) :
	
	kws = (
				'uiConfigurationScriptNode' ,
				'delight' ,
				'mentalray' ,
				'miDefaultOptions' ,
				'defaultRenderLayer' ,
				'renderLayerManager' ,
				'modelPanel4ViewSelectedSet' ,
				'lockNode'
			)
	
	filePath = os.path.normpath( filePath )
	folderPath , fileNameExt = os.path.split( filePath )
	
	tmpFilePath = '%s.tmp' % filePath
	
	print 'Fixing %s' % filePath
	
	fid = open( filePath , 'r' )
	
	tmpid = open( tmpFilePath , 'w' )
	foundKw = False
	write = True
	
	for line in fid :
		
		if isKeywordInLine( kws , line ) :
			foundKw = True
			write = False
		
		if foundKw and line.startswith( '	' ) :
			write = False
		
		if foundKw and not line.startswith( '	' ) and not isKeywordInLine( kws , line ) :
			foundKw = False
			write = True
		
		if write :
			tmpid.write( line )
	
	fid.close()
	tmpid.close()
	
	bakFilePath = '%s.bak' % filePath
	
	shutil.copy2( filePath , bakFilePath )
	shutil.copy2( tmpFilePath , filePath )
	os.remove( tmpFilePath )
	os.remove( bakFilePath )
	
	print 'Fixing %s done.' % filePath

def renameMatInfo() :

	exceptions = [ 'initialMaterialInfo' ]

	for each in mc.ls( type='materialInfo' ) :

		sg = mc.listConnections( '%s.shadingGroup' % each )
		if sg :
			if not each in exceptions :
				mc.rename( each , '%s_materialInfo' % sg[0] )

def doAddOriGrp() :

	sels = mc.ls( sl=True )

	for sel in sels :
		
		parent = mc.listRelatives( sel , p=True )[0]
		nodeName , nodeSide , nodeType = getElementName( sel )
		
		grp = pc.Null()
		grp.name = '%s%sOri%s_grp' % ( nodeName , capitalizeFirst( nodeType ) , nodeSide )
		grp.snap( parent )
		grp.parent( parent )
		mc.parent( sel , grp )

def doAddElem( elm='' ) :

	for each in sorted( mc.ls( sl=True , l=True ) , reverse=True ) :
		name = each.split( '|' )[-1]
		mc.rename( each , addElementToName( name , elm ) )

def groupOnCurve( crv='' ) :

	crvObj = pc.Dag( crv )
	crvShpObj = pc.Dag( crvObj.shape )

	grp = pc.Null()
	poci = pc.PointOnCurveInfo()

	grp.add( ln='parameter' , k=True , min=0 , max=1 )
	crvShpObj.attr('worldSpace[0]') >> poci.attr('ic')
	poci.attr( 'turnOnPercentage' ).v = True
	poci.attr('position') >> grp.attr('t')
	grp.attr( 'parameter' ) >> poci.attr( 'parameter' )
	grp.lockHideAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

	return grp , poci

def connectProxySkinJoint() :

	# Connect proxy skin joint to skin joint.

	proxyJnts = mc.ls( '*:*ProxySkin*_jnt' )

	for proxyJnt in proxyJnts :

		cnncs = mc.listConnections( proxyJnt , type='constraint' , d=False , s=True )

		if not cnncs :

			proxyNs , proxyName = proxyJnt.split( ':' )
			skinJntName = proxyName.replace( 'ProxySkin' , '' )
			
			skinJnts = mc.ls( '*:%s' % skinJntName )

			if skinJnts :

				skinJnt = skinJnts[0]

				try :
					mc.parentConstraint( skinJnt , proxyJnt , mo=True )
					mc.scaleConstraint( skinJnt , proxyJnt , mo=True )
				except :
					print '%s cannot be connected, please check.' % proxyJnt

				# Correct segment scale compensation
				ssc = mc.getAttr( '%s.segmentScaleCompensate' % skinJnt )
				mc.setAttr( '%s.segmentScaleCompensate' % proxyJnt , ssc )

			else :
				print 'Cannot find skin joint for %s' % proxyJnt

		else :

			print '%s already has connections.' % proxyJnt

def removeAllShader() :

	exceptions = ( 'initialShadingGroup' , 'initialParticleSE' )

	for each in mc.ls( type='shadingEngine' ) :
		if not each in exceptions :

			shaders = mc.listConnections( '%s.surfaceShader' % each )
			if shaders :
				try :
					mc.delete( shaders[0] )
					print '%s has been deleted.' % shaders[0]
				except :
					print '%s cannot be deleted.' % shaders[0]
			try :
				mc.delete( each )
				print '%s has been deleted.' % each
			except :
				print '%s cannot be deleted.' % each

def detachSelectedEdge() :

	objs = []

	for sel in mc.ls( sl=True , fl=True ) :

		obj = sel.split( '.' )[0]

		if not obj in objs :
			objs.append( obj )
	
	mm.eval( 'DetachComponent' )
	mc.select( objs , r=True )
	mm.eval( 'SeparatePolygon' )
	mc.delete( ch=True )

def selectBakeController() :
	sels = mc.ls( sl=True )
	nss = []
	ctrls = []

	for sel in sels :
		currNs = ''
		try :
			currNs = sel.replace( sel.split( ':' )[-1] , '' )
		except :
			pass

		if not currNs in nss :
			nss.append( currNs )

	for ns in nss :
		
		ctrlFilter = '%s*_ctrl' % ns
		currCtrls = mc.ls( ctrlFilter )

		for ctrl in currCtrls :
			
			ctrls.append( ctrl )

	mc.select( ctrls , r=True )


def selectNoPlacement() :

	selectAllController( [ 'master_ctrl' , 'placement_ctrl' , 'offset_ctrl' ] )

def selectAllController( exceptions=[] ) :

	sels = mc.ls( sl=True )
	nss = []
	ctrls = []

	for sel in sels :
		currNs = ''
		try :
			currNs = sel.replace( sel.split( ':' )[-1] , '' )
		except :
			pass

		if not currNs in nss :
			nss.append( currNs )

	for ns in nss :
		
		ctrlFilter = '%s*_ctrl' % ns
		currCtrls = mc.ls( ctrlFilter )

		for ctrl in currCtrls :

			canAppend = True

			connections = mc.listConnections( ctrl , s=True , d=False )
			if connections :
				for connection in connections :
					if 'Constraint' in mc.nodeType( connection ) :
						canAppend = False

			if exceptions :
				for exception in exceptions :
					if exception in ctrl :
						canAppend = False

			if canAppend :
				ctrls.append( ctrl )

	mc.select( ctrls , r=True )

def findConnectedContraint( dag='' ) :
	'''
	Find constraint nodes that connected to given dag.
	Return list of constraint nodes.
	'''
	cnstrs = []

	cons = mc.listConnections( dag , s=True , d=False , scn=True , type='constraint' )
	if cons :
		for con in cons :
			if not con in cnstrs :
				cnstrs.append( con )

	return cnstrs

def findTargetFromConstraintNode( constraintNode='' ) :
	'''
	Find constraint targets that connected to given constraintNode.
	Return list of constraint targets.
	'''
	targets = []
	# Get current source connections.
	cnstrSrcs = mc.listConnections( constraintNode , s=True , d=False , p=True )
	
	for cnstrSrc in cnstrSrcs :
		# Iterate to each source connection.
		# If source has attribute parentMatrix connected in,
		# append to 'targets'
		exp = r'(.*)(\.parentMatrix)'
		m = re.search( exp , cnstrSrc )
		if m :
			if not m.group(1) in targets :
				targets.append( m.group(1) )

	return targets

def findConstraintSource( dag='' ) :
	'''
	Find the constraint target of given DAG.
	Return a constraint target.
	'''
	cnstrs = findConnectedContraint( dag )
	
	# Make sure that given DAG has connected constraint node.
	if cnstrs :

		targets = []
		for cnstr in cnstrs :
			# Iterate to each constraint node
			# and get current constraint target.
			currTargets = findTargetFromConstraintNode( cnstr )
			for currTarget in currTargets :
				if not currTarget in targets :
					targets.append( currTarget )
		
		# Make sure that given DAG has only one target.
		if len( targets ) > 1 :
			print '%s has more than one target.' % dag
			return None
		else :
			return targets[0]
	else :
		print '%s has no related constraint node.' % dag
		return None

def removeConstraintTarget( target='' , dag='' ) :
	'''
	Remove constraint target from given dag.
	'''
	attrDict = {
					'tx' : None ,
					'ty' : None ,
					'tz' : None ,
					'rx' : None ,
					'ry' : None ,
					'rz' : None ,
					'sx' : None ,
					'sy' : None ,
					'sz' : None
				}
	# Collecting transform values before remove the constraint target.
	for attr in attrDict.keys() :
		currNodeAttr = '%s.%s' % ( dag , attr )
		if not mc.getAttr( currNodeAttr , l=True ) :
			attrDict[attr] = mc.getAttr( currNodeAttr )

	mc.select( target , r=True )
	mc.select( dag , add=True )
	mm.eval( 'performRemoveConstraintTarget 0;' )

	# Setting transform values back after constraint target has been removed.
	for attr in attrDict.keys() :
		currNodeAttr = '%s.%s' % ( dag , attr )
		if attrDict[attr] :
			mc.setAttr( currNodeAttr , attrDict[attr] )

def importRigElement() :

	'''
	Import all reference.
	Then iterate through each child parented to 'delete_grp'.
	Parent each child to related constraint target.
	Remove 'delete_grp'.
	'''
	
	# Import all referenced files
	importAllReferences()

	delGrp = 'delete_grp'
	
	if mc.objExists( delGrp ) :
		
		for each in mc.listRelatives( delGrp , type='transform' ) :
			
			target = findConstraintSource( each )
			
			if target :
				mc.parent( each , target )
				removeConstraintTarget( target , each )

		mc.delete( delGrp )

def importAllReferences() :
	
	'''
	Import all reference file in the current scene.
	'''
	
	rfns = mc.ls( type='reference' )

	if rfns :
		
		for rfn in rfns :
			
			if not rfn == 'sharedReferenceNode' :
				
				try :
					fn = mc.referenceQuery( rfn , filename=True )
					
					mc.file( fn , importReference=True )
					
					print '%s has been imported.' % rfn
				
				except RuntimeError :
					
					print '%s is not connected to reference file.' % rfn

def renameSelectedShadingGroup() :
	'''
	Rename selected shading group, regarding its material.
	'''

	for each in mc.ls( sl=True ) :
		
		mat = mc.listConnections(
									'%s.surfaceShader' % each ,
									s=True ,
									d=False
								)[0]

		currName , currSide , currType = getElementName( mat )
		sgName = '%s%s%s%s_sg' % (
										currName ,
										currType[0].capitalize() ,
										currType[1:] ,
										currSide
									)
	
	mc.rename( each , sgName )

def addElementToName( objName='' , element='' ) :

	nodeName , nodeSide , nodeType = getElementName( objName )

	return '%s%s%s_%s' % ( nodeName , capitalizeFirst( element ) , nodeSide , nodeType )

def getElementName( objName='' ) :

	nodeName = ''
	nodeSide = ''
	nodeType = ''

	sides = ( 'LFT' , 'RGT' )
	for side in sides :
		if side in objName :
			nodeSide = side
	try :
		nodeName , nodeType = objName.split( '%s_' % nodeSide )
	except ValueError :
		nodeName = objName
		nodeType = ''

	return nodeName , nodeSide , nodeType

def capitalizeFirst( inputName='' ) :
	
	kwDict = {
				'LFT_' : 'Left' ,
				'RGT_' : 'Right'
				}
	
	for key in kwDict.keys() :
		if key in inputName :
			inputName = inputName.replace( key , kwDict[key] )
	
	return '%s%s' % ( inputName[0].capitalize() , inputName[1:] )

def doAttrOffsetValue( sourceObjAttr='' , targetObjAttr='' , offsetValue=1 ) :
	
	targetObj , targetAttr = targetObjAttr.split( '.' )
	targetName , targetSide , targetType = getElementName( targetObj )
	
	add = attrOffsetValue( sourceObjAttr , targetObjAttr , offsetValue )
	
	add.name = '%s%s%sOffset%s_add' % (
										targetName ,
										capitalizeFirst( targetType ) ,
										capitalizeFirst( targetAttr ) ,
										targetSide
									)
	
	return add

def doAttrAmp( sourceObjAttr='' , targetObjAttr='' , ampAttr='' ) :
	
	# sourceObj , sourceAttr = sourceObjAttr.split( '.' )
	# sourceName , sourceSide , sourceType = getElementName( sourceObj )

	targetObj , targetAttr = targetObjAttr.split( '.' )
	targetName , targetSide , targetType = getElementName( targetObj )

	if '[' in targetAttr :
		targetAttr = targetAttr.replace( '[' , 'I' )

	if ']' in targetAttr :
		targetAttr = targetAttr.replace( ']' , 'I' )
	
	
	mul = attrAmper(
						ctrlAttr = sourceObjAttr ,
						targetAttr = targetObjAttr ,
						dv = 1 ,
						ampAttr = ampAttr
					)
	
	mul.name = '%s%s%sAmp%s_mul' % (
										targetName ,
										capitalizeFirst( targetType ) ,
										capitalizeFirst( targetAttr ) ,
										targetSide
									)
	
	return mul

def doAddParentLocWor() :
	
	ctrl , loc , wor , zro = mc.ls(sl=True)
	( locGrp ,
		worGrp ,
		worParcon ,
		zroParcon ,
		rev ) = parentLocalWorldCtrl( ctrl , loc , wor , zro )
	
	currName = ''
	currSide = '_'
	
	for side in ( 'LFT' , 'RGT' ) :
		if side in ctrl :
			currSide = '%s_' % side
	
	currName = ctrl.split( currSide )[0]
	locGrp.name = '%sCtrlLoc%sgrp' % ( currName , currSide )
	worGrp.name = '%sCtrlWor%sgrp' % ( currName , currSide )
	worParcon.name = '%s_parentConstraint1' % worGrp
	rev.name = '%sCtrlLocWor%srev' % ( currName , currSide )

def doAddOrientLocWor() :
	
	ctrl , loc , wor , zro = mc.ls(sl=True)
	( locGrp ,
		worGrp ,
		worParcon ,
		zroParcon ,
		rev ) = orientLocalWorldCtrl( ctrl , loc , wor , zro )
	
	currName = ''
	currSide = '_'
	
	for side in ( 'LFT' , 'RGT' ) :
		if side in ctrl :
			currSide = '%s_' % side
	
	currName = ctrl.split( currSide )[0]
	locGrp.name = '%sCtrlLoc%sgrp' % ( currName , currSide )
	worGrp.name = '%sCtrlWor%sgrp' % ( currName , currSide )
	worParcon.name = '%s_orientConstraint1' % worGrp
	rev.name = '%sCtrlLocWor%srev' % ( currName , currSide )

def doAddCtrl( shape='circle' ) :
	
	jnts = mc.ls( sl=True , l=True )
	ctrls = []
	gmbls = []
	zgrps = []
	
	for jnt in jnts :
		jntName = ''
		if ':' in jnt :
			jntName = jnt.split( ':' )[-1].split( '|' )[-1]
		else :
			jntName = jnt.split( '|' )[-1]
		
		currName = ''
		currSide = '_'
		
		sides = ( 'LFT' , 'RGT' , 'UPR' , 'LWR' , 'CEN', 'FNT', 'BCK')
		for side in sides :
			if side in jntName :
				currSide = '%s_' % side
		
		currName = jntName.split( currSide )[0]
		
		ctrl = pc.Control( shape )
		ctrl.lockHideAttrs( 'v' )
		ctrl.color = 'blue'
		ctrl.name = '%s%sCtrl' % ( currName , currSide )
		
		mc.select( ctrl , r=True )
		zgrp = doZeroGroup()[0]
		
		mc.select( ctrl , r=True )
		gmbl = doAddGimbal()[0]

		conCtrl = pc.addConCtrl( ctrl )
		conCtrl.name = '%sCon%sCtrl' % ( currName , currSide )
		
		zgrp.snap( jnt )
		mc.parentConstraint( gmbl , jnt )
		mc.scaleConstraint( gmbl , jnt )

		ctrls.append( ctrl )
		gmbls.append( gmbl )
		zgrps.append( zgrp )

	return ctrls , gmbls , zgrps

def doAddGimbal() :
	ctrls = mc.ls( sl=True )
	gmbls = []
	
	for ctrl in ctrls :
		
		gmbl = pc.addGimbal( ctrl )
		
		currName = ''
		currSide = '_'
		
		sides = ( 'LFT' , 'RGT' )
		for side in sides :
			if side in ctrl :
				currSide = '%s_' % side
		
		currName = ctrl.split( currSide )[0]
		
		gmbl.name = '%sGmbl%sCtrl' % ( currName , currSide )
		gmbls.append( gmbl )
		
	return gmbls

def hideControl() :
	# Scale selected controller to zero and remove all keyable attributes.
	sels = mc.ls( sl=True )
	for each in sels :
		
		ctrl = pc.Dag( each )
		ctrl.scaleShape( 0 )
		
		for attr in mc.listAttr( ctrl , k=True ) :
			
			ctrl.attr( attr ).lockHide()
		
		shp = pc.Dag( ctrl.shape )
		
		if mc.objExists( shp.attr( 'gimbalControl' ) ) :
			
			gmbl = pc.Dag( mc.listConnections( shp.attr( 'gimbalControl' ) , d=True , s=False )[0] )
			gmbl.scaleShape( 0 )
			for attr in mc.listAttr( gmbl , k=True ) :
				print gmbl , attr
				gmbl.attr( attr ).lockHide()
	
	mc.select( sels , r=True )

def skinLattice() :
	
	# Bind selected lattice to the selected joints
	# Select a lattice then joints
	
	sels = mc.ls( sl=True )
	lat = sels[-1]
	jnts = sels[:-1]
	
	# Bind lattice to joints
	skc = mc.skinCluster( jnts , lat , dr = 7 , mi = 2 , tsb=True )[0]
	jntNo = len( jnts )
	
	for ix in range( 0 , jntNo ) :
		# Edit the skin values
		currId = jntNo - ix - 1
		
		vtcsA = '%s.pt[0:1][%s][0]' % ( lat , currId )
		vtcsB = '%s.pt[0:1][%s][1]' % ( lat , currId )
		
		mc.skinPercent( skc , vtcsA , tv = [ jnts[ix] , 1 ] )
		mc.skinPercent( skc , vtcsB , tv = [ jnts[ix] , 1 ] )

def renameLattice( name='' ) :
	
	# Rename the lattice, lattice base, ffd and ffd set
	# Select lattice node the run the script
	
	lat = mc.ls( sl=True )[0]
	latShp = mc.listRelatives( lat , s=True )[0]
	ffd = mc.listConnections( '%s.worldMatrix[0]' % latShp , d=True , s=False )[0]
	ffdSet = mc.listConnections( '%s.message' % ffd , d=True , s=True )[0]
	latBase = mc.listConnections( '%s.baseLatticeMatrix' % ffd , d=False , s=True )[0]
	latBaseShp = mc.listRelatives( latBase , s=True )[0]
	
	mc.rename( lat , '%s_ffdLattice' % name )
	print 'Rename %s to %s' % ( lat , '%s_ffdLattice' % name )
	
	mc.rename( latBase , '%s_ffdBase' % name )
	print 'Rename %s to %s' % ( latBase , '%s_ffdBase' % name )
	
	mc.rename( ffd , '%s_ffd' % name )
	print 'Rename %s to %s' % ( ffd , '%s_ffd' % name )
	
	mc.rename( ffdSet , '%s_ffdSet' % name )
	print 'Rename %s to %s' % ( ffdSet , '%s_ffdSet' % name )

def resetJointOrient() :
	
	# Reset the orientation of selected joints to zero
	# Select joints the run the script
	
	for each in mc.ls( sl=True ) :
		
		prnts = mc.listRelatives( each , p=True )
		chldrn = mc.listRelatives( each , c=True )
		
		if prnts :
			mc.parent( each , w=True )
		
		if chldrn :
			mc.parent( chldrn[0] , w=True )
		
		mc.setAttr( '%s.jointOrient' % each , 0 , 0 , 0 )
		
		if prnts :
			mc.parent( each , prnts[0] )
		
		if chldrn :
			mc.parent( chldrn[0] , each )

def addOffsetGroup() :
	# Create offset group for selected controller
	sel = mc.ls( sl=True )[0]
	
	currSide = ''
	sides = ( 'LFT' , 'RGT' )
	
	for side in sides :
		
		if side in sel :
			
			currSide = side
	
	currName , currType = sel.split( '%s_' % currSide )
	
	offGrpName = '%s%s%sOfst%s_grp' % ( currName , currType[0].upper() , currType[1:] , currSide )
	
	prnt = mc.listRelatives( sel , p=True )[0]
	offGrp = mc.group( em=True , n=offGrpName )
	mc.delete( mc.parentConstraint( prnt , offGrp ) )
	mc.parent( offGrp , prnt , r=False )
	mc.parent( sel , offGrp )
	
	attrs = ( 'v' )
	# attrs = ( 'v' )
	
	for attr in attrs :
		mc.setAttr( '%s.%s' % ( offGrp , attr ) , k=False , l=True )
	
	return offGrpName

def renameClusterHandleShape() :
	'''
	Rename all clusterHandleShape in the current scene.
	'''
	clsHndls = mc.ls( 'clusterHandleShape*' )
	
	for clsHndl in clsHndls :
		
		clstr = mc.listConnections(  '%s.clusterTransforms[0]' % clsHndl , d=True , s=False )[0]
		mc.rename( clsHndl , '%sShape' % clstr )

def locatorOnMidPos() :
	'''
	Create locator at the middle of selected objects.
	'''
	sels = mc.ls( sl = True , fl = True , l = True )
	no = len( sels )
	posSum = [0,0,0]
	loc = pc.Locator()
	
	for sel in sels :
		
		currPos = mc.xform( sel , q = True , t = True , ws = True )
		posSum[0] += currPos[0]
		posSum[1] += currPos[1]
		posSum[2] += currPos[2]
	
	loc.attr('tx').v = posSum[0]/no
	loc.attr('ty').v = posSum[1]/no
	loc.attr('tz').v = posSum[2]/no
	
	return loc

def renameSelectedSkinSet() :
	
	for sel in mc.ls( sl=True ) :
		renameDeformerSet( sel , 'skinCluster' )
		renameDeformerSet( sel , 'tweak' )

def renameDeformerSet( obj='' , defType='' ) :
	
	suffix = '%s%s' % ( defType[0].upper() , defType[1:] )
	shape = mc.listRelatives( obj , s=True )[0]
	
	dfNodes = mc.listConnections( shape , s=True , d=False , type=defType )
	
	if dfNodes :
		currSkn = mc.rename( dfNodes[0] , '%s%s' % ( obj , suffix ) )
		dfSets = mc.listConnections( currSkn , s=False , d=True , type='objectSet' )
		if dfSets :
			mc.rename( dfSets[0] , '%s%sSet' % ( obj , suffix ) )

def alignJawCtrl() :
	# Align jaw control to jaw joint.
	# Select jaw control curve then jaw joint.
	sels = mc.ls( sl = True )
	
	ctrl = sels[0]
	jnt = sels[1]
	
	shp = mc.listRelatives( ctrl , shapes=True )[0]
	mc.select( shp , r=True )
	
	fstCv = 14
	scdCv = 2
	
	fstPos = mc.xform( '%s.cv[%s]' % (shp,fstCv) , q=True , ws=True , t=True )
	scdPos = mc.xform( '%s.cv[%s]' % (shp,scdCv) , q=True , ws=True , t=True )
	midPos = [ (fstPos[0]+scdPos[0])/2 , (fstPos[1]+scdPos[1])/2 , (fstPos[2]+scdPos[2])/2 ]
	
	clstr = mc.cluster()[1]
	mc.select( clstr , r=True )
	mc.move( midPos[0] , midPos[1] , midPos[2] , '%s.rotatePivot' % clstr )
	mc.move( midPos[0] , midPos[1] , midPos[2] , '%s.scalePivot' % clstr )
	
	mc.delete( mc.pointConstraint( jnt , clstr ) )
	
	tipJnt = mc.listRelatives( jnt , type = 'joint' )[0]
	
	if tipJnt :
		
		mc.delete( mc.aimConstraint(tipJnt,clstr,aim=(0,0,1),u=(1,0,0),wut='vector',wu=(1,0,0)))

def scaleGimbalControl( size=0.8 ) :
	
	gmblCtrls = mc.ls( '*Gmbl*_ctrl' )
	
	for each in gmblCtrls :
		
		ctrl = pc.Dag( each.replace( 'Gmbl' , '' ) )
		gmbl = pc.Dag( each )
		copyCurveShape( ctrl , gmbl )
		gmbl.scaleShape( size )

def scaleConControl( size=1.2 ) :
	
	gmblCtrls = mc.ls( '*Con*_ctrl' )
	
	for each in gmblCtrls :
		ctrl = pc.Dag( each.replace( 'Con' , '' ) )
		gmbl = pc.Dag( each )
		copyCurveShape( ctrl , gmbl )
		gmbl.scaleShape( size )

def mirrorCurveShape( obj = '' , search = '' , replace = '' ) :
	# Mirror NURBs curve shape
	crv = pc.Dag( obj )
	crvShape = pc.Dag( crv.shape )
	cv = crvShape.attr('spans').value + crvShape.attr('degree').value
	
	origCrv = pc.Dag( crv.name.replace( search , replace ) )
	origCrvShape = pc.Dag( origCrv.shape )
	
	for ix in range( 0 , cv ) :
		
		pos = mc.xform( '%s.cv[%s]' % ( origCrvShape , str(ix) ) , q = True , t = True , ws = True )
		mc.xform( '%s.cv[%s]' % ( crv.name , str(ix) ) , t = ( -pos[0] , pos[1] , pos[2] ) , ws = True )

def copyCurveShape( src = '' , target = '' ) :
	# Copy NURBs curve shape
	crv = pc.Dag( target )
	
	crvShape = pc.Dag( crv.shape )
	cv = crvShape.attr('spans').value + crvShape.attr('degree').value
	
	origCrv = pc.Dag( src )
	origCrvShape = pc.Dag( origCrv.shape )
	
	for ix in range( 0 , cv ) :
		
		pos = mc.xform( '%s.cv[%s]' % ( origCrvShape , str(ix) ) , q = True , t = True , ws = True )
		mc.xform( '%s.cv[%s]' % ( crv.name , str(ix) ) , t = ( pos[0] , pos[1] , pos[2] ) , ws = True )

def jointControl( crvType = '' ) :
	# Creating joint node with curve shape.
	jnt = pc.Joint()
	jnt.createCurve( crvType )
	jnt.attr('radius').v = 0
	jnt.attr('radius').lock = 1
	jnt.attr('radius').hide = 1
	
	return jnt

def jointAt( obj ) :
	# Create joint at postion of given object
	# Returns : joint object
	target = pc.Dag( obj )
	
	jnt = pc.Joint()
	
	jnt.snap( target )
	jnt.freeze( r = True , s = True )
	jnt.rotateOrder = target.rotateOrder
	if target.attr( 'radius' ).exists :
		jnt.attr( 'radius' ).v = target.attr( 'radius' ).v
	mc.select( cl = True )
	
	return jnt

def blend2Vectors( attr = '' , objA = '' , objB = '' , target = '' ) :
	# Blending 2 vector attributes
	blend = pc.BlendColors()
	mc.connectAttr( '%s.%s' % ( objA , attr ) , blend.attr( 'color1' ) )
	mc.connectAttr( '%s.%s' % ( objB , attr ) , blend.attr( 'color2' ) )
	
	if target :
		blend.attr( 'output' ) >> target.attr( attr )
	
	mc.select( cl = True )
	
	return blend

def crvGuide( ctrl = '' , target = '' ) :
	# Create NURBs curve between control and target
	# Returns : curve and two clusters
	crv = pc.Dag( mc.curve( d = 1 , p = [ ( 0 , 0 , 0 ) , ( 0 , 0 , 0 ) ] ) )
	clstr1 = pc.Dag( mc.cluster( '%s.cv[0]' % crv , wn = ( ctrl , ctrl ) )[ 0 ] )
	clstr2 = pc.Dag( mc.cluster( '%s.cv[1]' % crv , wn = ( target , target ) )[ 0 ] )
	mc.select( cl = True )
	
	return crv , clstr1 , clstr2

def visCtrlr( ctrl = '' , attrName = '' , target = '' ) :
	# Add visibility control
	mc.addAttr( ctrl , ln = attrName , at = 'bool' , k = True )
	mc.setAttr( '%s.%s' % ( ctrl , attrName ) , cb = True )
	mc.connectAttr( '%s.%s' % ( ctrl , attrName ) , '%s.v' % target )
	mc.select( cl = True )

def attrOffsetValue( ctrlAttr = '' , targetAttr = '' , offsetValue = 1 ) :

	add = pc.AddDoubleLinear()

	add.add( ln='offsetValue' , k=True , dv=offsetValue )

	mc.connectAttr( ctrlAttr , '%s.i1' % add , f=True )
	mc.connectAttr( '%s.offsetValue' % add , '%s.i2' % add , f=True )
	mc.connectAttr( '%s.o' % add , targetAttr , f=True )

	return add

def attrAmper( ctrlAttr = '' , targetAttr = '' , dv = 1 , ampAttr = '' ) :
	# Create attribute amplifier
	# Returns : multiDoubleLinear
	shape = pc.Dag( '' )
	try :
		shape = pc.Dag( mc.listRelatives( str( ctrlAttr ).split('.')[0] )[0] )
	except :
		pass
	mul = pc.MultDoubleLinear()
	
	mul.add( ln = 'amp' , k = True , dv = dv )
	
	if shape.exists and ampAttr :
		if shape.attr( ampAttr ).exists :
			shape.attr(ampAttr) >> mul.attr('amp')
		else :
			shape.add( ln = ampAttr , k = False , dv = dv )
			shape.attr(ampAttr) >> mul.attr('amp')
	
	mul.attr('amp') >> mul.attr('i1')
	mc.connectAttr( ctrlAttr , '%s.i2' % mul.name )
	mc.connectAttr( '%s.o' % mul.name , targetAttr , f = True )
	mc.select( cl = True )
	
	return mul

def fkStretch( ctrl = '' , attr = '' , target = '' , ax = 'y' ) :
	'''
	Create stretchy FK by using translation of its child
	Returns : addDoubleLinear and multiDoulbleLinear
	'''
	if attr :
		if not mc.objExists( '%s.%s' % ( ctrl , attr ) ) :
			mc.addAttr( ctrl , ln = attr , at = 'float' , k = True )		
	else :
		if mc.objExists( '%s.stretch' % ctrl ) :
			attr = 'stretch'
		else :
			mc.addAttr( ctrl , ln = 'stretch' , at = 'float' , k = True )
			attr = 'stretch'
	
	add = pc.AddDoubleLinear()
	
	div = mc.getAttr( '%s.t%s' % ( target , ax ) )
	mul = attrAmper( '%s.%s' % ( ctrl , attr ) , '%s.i2' % add , dv = div )
	add.add( ln = 'default' , k = True , dv = div )
	add.attr('default') >> add.attr('i1')
	add.attr('o') >> '%s.t%s' % ( target , ax )
	
	mc.select( cl = True )
	
	return add , mul

def fkSquash( ctrl = '' , attr = '' , target = '' , ax = 'y' ) :
	'''
	Create FK squash by connecting to the other two axes
	Returns : addDoubleLinear and multiDoulbleLinear
	'''
	if attr :
		if not mc.objExists( '%s.%s' % ( ctrl , attr ) ) :
			mc.addAttr( ctrl , ln = attr , at = 'float' , k = True )		
	else :
		if mc.objExists( '%s.squash' % ctrl ) :
			attr = 'squash'
		else :
			mc.addAttr( ctrl , ln = 'squash' , at = 'float' , k = True )
			attr = 'squash'
	
	add = pc.AddDoubleLinear()
	
	mul = attrAmper( '%s.%s' % ( ctrl , attr ) , '%s.i2' % add , dv = 0.1 )
	add.add( ln = 'default' , k = True , dv = 1 )
	add.attr('default') >> add.attr('i1')
	
	for eachAx in ( 'x' , 'y' , 'z' ) :
		if not eachAx == ax :
			add.attr('o') >> '%s.s%s' % ( target , eachAx )
	
	mc.select( cl = True )
	
	return add , mul

def orientLocalWorldCtrl( ctrl = '' , localObj = '' , worldObj = '' , oriGrp = '' ) :
	# Blending orientation between local and world object.
	# Returns : locGrp , worGrp , worGrpOriCons , oriGrpOriCons and oriGrpOriConsRev
	locGrp = pc.Null()
	worGrp = pc.Null()
	locGrp.snap( oriGrp )
	worGrp.snap( oriGrp )
	
	oLoc = str( locGrp.name )
	oWor = str( worGrp.name )
	
	locGrp.name = 'local'
	worGrp.name = 'world'
	
	worGrpOriCons = pc.orientConstraint( worldObj , worGrp , mo = True )
	oriGrpOriCons = pc.orientConstraint( locGrp , worGrp , oriGrp )
	oriGrpOriConsRev = pc.Reverse()
	
	locGrp.name = oLoc
	worGrp.name = oWor
	
	con = pc.Dag( ctrl )
	
	attr = 'localWorld'
	con.add( ln = attr , k = True, min = 0 , max = 1 )
	con.attr( attr ) >> oriGrpOriCons.attr( 'w1' )
	con.attr( attr ) >> oriGrpOriConsRev.attr( 'ix' )
	oriGrpOriConsRev.attr( 'ox' ) >> oriGrpOriCons.attr( 'w0' )
	
	oriGrpOriCons.attr('interpType').value = 2

	locGrp.parent( localObj )
	worGrp.parent( localObj )
	pc.clsl()
	
	return locGrp , worGrp , worGrpOriCons , oriGrpOriCons , oriGrpOriConsRev

def parentLocalWorldCtrl( ctrl = '' , localObj = '' , worldObj = '' , parGrp = '' ) :
	# Blending parent between local and world object.
	# Returns : locGrp , worGrp , worGrpParCons , parGrpParCons and parGrpParConsRev
	locGrp = pc.Null()
	worGrp = pc.Null()
	
	locGrp.snap( parGrp )
	worGrp.snap( parGrp )
	
	worGrpParCons = pc.parentConstraint( worldObj , worGrp , mo = True )
	parGrpParCons = pc.parentConstraint( locGrp , worGrp , parGrp )
	parGrpParConsRev = pc.Reverse()
	
	con = pc.Dag( ctrl )
	
	attr = 'localWorld'
	con.add( ln = attr , k = True, min = 0 , max = 1 )
	con.attr( attr ) >> parGrpParCons.attr( 'w1' )
	con.attr( attr ) >> parGrpParConsRev.attr( 'ix' )
	parGrpParConsRev.attr( 'ox' ) >> parGrpParCons.attr( 'w0' )
	
	parGrpParCons.attr('interpType').value = 2

	locGrp.parent( localObj )
	worGrp.parent( localObj )
	pc.clsl()
	
	return locGrp , worGrp , worGrpParCons , parGrpParCons , parGrpParConsRev

def zeroGroup( obj = '' ) :
	# Create zero group
	chld = pc.Dag( obj )
	grp = pc.Null()
	grp.snap( chld )
	chld.parent( grp )
	
	return grp

def doZeroGroup() :
	# Create zero group with naming
	sels = mc.ls( sl=True )
	sides = ( 'LFT' , 'RGT' , 'UPR' , 'LWR' , 'CEN', 'FNT', 'BCK')
	zroGrps = []
	
	for sel in sels :
		
		curr = pc.Dag( sel )
		prnt = curr.getParent()
		zGrp = zeroGroup( sel )
		
		nameLst = curr.name.split( '_' )
		currType = nameLst[-1]
		charName = ''
		midName = ''
		currSide = ''
		
		if len( nameLst ) == 3 :
			charName = '%s_' % nameLst[0]
			midName = nameLst[1]
		else :
			midName = nameLst[0]
		
		for side in sides :
			if side in midName :
				currSide = side
				midName = midName.replace( side , '' )
		
		zGrp.name = '%s%s%s%sZro%s_Grp' % ( charName ,
											midName ,
											currType[0].upper() ,
											currType[1:] ,
											currSide
											)
		
		if prnt :
			zGrp.parent( prnt )
			# print 'aaaa'
		
		zroGrps.append( zGrp )
	
	return zroGrps

def UtaDoZeroGroup() :
	# Create zero group with naming
	sels = mc.ls( sl=True )
	sides = ( '_L_' , '_R_' , 'Upr' , 'Lwr' ,'Cen', 'Fnt', 'Bck')
	zroGrps = []
	
	for sel in sels :
		
		curr = pc.Dag( sel )
		prnt = curr.getParent()
		zGrp = zeroGroup( sel )
		nameLst = curr.name.split( '_' )

		charName = ''
		midName = ''
		currSide = ''
		
		if len( nameLst ) == 3 :
			charName = '%s' % nameLst[0]
			currSide = '_' + nameLst[1] + '_'
			currType = nameLst[-1]
		else :
			charName = nameLst[0]
			currType = nameLst[-1]
			currSide = '_'
		for side in sides :
			if side in currSide :
				currSide = side
				midName = midName.replace( side , '' )
		zGrp.name = '%s%s%sGrp' % ( charName ,
										currType,
										currSide
										)
		
		if prnt :
			zGrp.parent( prnt )
			# print 'aaaa'
		
		zroGrps.append( zGrp )
	
	return zroGrps

def wireIkCurve() :
	# Ribbon curve for ribbon IK
	return pc.curve(
						d = 3 ,
						p = [
								( 0 , 0 , 0 ) ,
								( 0 , 0.0666667 , 0 ) ,
								( 0 , 0.2 , 0 ) ,
								( 0 , 0.4 , 0 ) ,
								( 0 , 0.6 , 0 ) ,
								( 0 , 0.8 , 0 ) ,
								( 0 , 0.933333 , 0 ) ,
								( 0 , 1 , 0 )
							]
					)

def ribbonCurve() :
	# Ribbon curve for ribbon IK
	return pc.curve( d = 1 , p = [ ( 0 , 0 , 0 ) , ( 0 , 1 , 0 ) ] )

def ribbonSurface() :
	# Ribbon geometry for ribbon IK
	nrb = pc.nurbsPlane( p = (0,0,0) ,
							ax = (0,0,1) ,
							w = True ,
							lr = 5 ,
							d = 3 , u = 1 ,
							v = 5 , ch = 0
						)
	nrb.attr('t').value = (0,2.5,0)
	nrb.freeze()
	nrb.attr('rp').value = (0,0,0)
	nrb.attr('sp').value = (0,0,0)
	nrb.scaleShape( 0.2 )
	
	return nrb

def constrainAttributeNaming( cnstrs ) :
	
	for cnstr in cnstrs :
		
		cnstrType = mc.nodeType( cnstr )

		dummyAttr = 'targetTranslate'

		if cnstrType == 'parentConstraint' :

			dummyAttr = 'target[000].targetTranslate'

		elif cnstrType == 'pointConstraint' :

			dummyAttr = 'target[000].targetTranslate'

		elif cnstrType == 'orientConstraint' :

			dummyAttr = 'target[000].targetRotate'

		elif cnstrType == 'scaleConstraint' :

			dummyAttr = 'target[000].targetScale'

		elif cnstrType == 'aimConstraint' :

			dummyAttr = 'target[000].targetTranslate'

		elif cnstrType == 'poleVectorConstraint' :

			dummyAttr = 'constraintRotatePivot'

		if mc.listAttr( cnstr , ud=True ) :

			udAttrs = sorted( mc.listAttr( cnstr , ud=True ) )

			for ix in range( len( udAttrs ) ) :

				currNodeAttrName = '%s.%s' % ( cnstr , udAttrs[ix] )

				outAttr = dummyAttr.replace( '000' , str( ix ) )
				target = mc.listConnections( '%s.%s' % ( cnstr , outAttr ) , s=True )[0]

				newAttrName = '%sW%s' % ( target , str( ix ) )
				print '%s -- %s' % ( udAttrs[ix] , newAttrName )

				lock = False

				if mc.getAttr( currNodeAttrName , l=True ) :
					lock = True
					mc.setAttr( currNodeAttrName , l=False )

				if not udAttrs[ix] == newAttrName :
					mc.renameAttr( currNodeAttrName , newAttrName )

				if lock :
					mc.setAttr( '%s.%s' % ( cnstr , newAttrName ) , l=True )

def nodeNaming( obj , charName = '' , elem = '' , side = '' ) :
	'''
	Auto-naming all attributes within the given object.
	'''
	if charName and ( not '_' in charName ) :
		charName = '%s_' % charName
	
	allAttrs = dir( obj )
	cnstrs = []
	
	for attr in allAttrs :

		if ( not '__' in attr ) and ( '_' in attr ) :
			
			nodeName , nodeType = attr.split( '_' )
			newName = '%s%s%s%s_%s' % ( charName , nodeName , elem , side , nodeType )
			cmd = 'obj.%s.name = "%s"' % ( attr , newName )
			exec( cmd )

			if 'Constraint' in mc.nodeType( newName ) :
				cnstrs.append( newName )

	constrainAttributeNaming( cnstrs )

def dummyNaming( obj , attr = '' , dummy = '' , charName = '' , elem = '' , side = '' ) :
	'''
	Auto-naming all attributes within the given object.
	Script will replace the "attr" with the "dummy".
	'''
	# if charName and ( not '_' in charName ) :
		# charName = '%s_' % charName
	# print charName, '....charName'
	allAttrs = dir( obj )
	cnstrs = []
	
	for eachAttr in allAttrs :
		if ( not '__' in eachAttr ) and ( '_' in eachAttr ) :
			
			nodeName , nodeType = eachAttr.split( '_' )
			nodeName = nodeName.replace( attr , dummy )
			nodeType = nodeType.capitalize()
			newName = '%s%s%s%s_%s' % ( charName , nodeName , elem , side , nodeType )
			cmd = 'obj.%s.name = "%s"' % ( eachAttr , newName )
			exec( cmd )

			if 'Constraint' in mc.nodeType( newName ) :
					cnstrs.append( newName )

	constrainAttributeNaming( cnstrs )

def lockUnusedAttrs( obj ) :
	# Lock unused attributes
	lckLst = [
				'grp' ,
				'parCons' ,
				'pntCons' ,
				'oriCons' ,
				'aimCons' ,
				'polCons' ,
				'scaCons'
			]
	
	nodes = dir( obj )
	
	for node in nodes :
		for lck in lckLst :
			attrLen = len( node )
			lckLen = len( lck )
			
			if attrLen > lckLen and node[ -lckLen : ] == lck :
				nodeName = eval( 'obj.%s' % node )
				kAttrs = mc.listAttr( nodeName , k=True )
				
				for kAttr in kAttrs :
					if not mc.attributeQuery( kAttr.split( '.' )[0] , n=nodeName , multi=True ) :
						mc.setAttr( '%s.%s' % ( nodeName , kAttr ) , l=True )

def cleanupAllModel() :

	# Clean all gemotry in the scene.
	removeAllNamespace()
	# Get poly shape node and NURBs shape node.
	shps = mc.ls( type='mesh' , l=True ) + mc.ls( type='nurbsSurface' , l=True )

	for shp in shps :

		if mc.objExists( shp ) :

			cleanupShape( shp )

	# Delete unused shading nodes.
	mm.eval( 'MLdeleteUnused' )

def cleanupSelectedModel() :

	# Clean slected models.
	sels = mc.ls( sl=True , l=True )

	for sel in sels :

		cleanupModel( sel )

def cleanupModel( dag='' ) :

	'''
	Cleanup given DAG node.
	Unlock its transform attributes.
	Remove related intermediate shapes and clean its shape.
	'''

	# Unlock transform attrs.
	for attr in ( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' ) :
		mc.setAttr( '%s.%s' % ( dag , attr ) , l=False , k=True )

	# Removing intermediate shapes
	shps = mc.listRelatives( dag , s=True , f=True )

	if shps :
		for shp in shps :
			if mc.objExists( shp ) :
				if mc.getAttr( '%s.intermediateObject' % shp ) :
					# Remove intermediate shapes
					mc.delete( shp )
					print '%s has been deleted.' % shp
				else :
					cleanupShape( shp )
	else :
		'%s has no related shape.' % dag

def cleanupShape( shp='' ) :
	
	'''
	Process to cleanup given shape.
	1. Soft edge.
	2. Enable all render attribute.
	3. Turn opposite attribute off.
	4. Delete construction history.
	5. Correct name of given shape.
	'''

	# Soft edge.
	mc.select( shp , r=True )
	cmd = 'polyNormalPerVertex -ufn true;'
	mm.eval( cmd )
	cmd = 'polySoftEdge -a 180 -ch 1;'
	mm.eval( cmd )
	
	# Enable all render attributes.
	attrs = (
				'castsShadows' ,
				'receiveShadows' ,
				'motionBlur' ,
				'primaryVisibility' ,
				'smoothShading' ,
				'visibleInReflections' ,
				'visibleInRefractions' ,
				'doubleSided'
			)
	for attr in attrs :
		nodeAttr = '%s.%s' % ( shp , attr ) 
		if not mc.getAttr( nodeAttr ) :
			mc.setAttr( nodeAttr , 1 )
	
	# Turn opposite attribute off.
	opposite = '%s.opposite' % shp
	if mc.getAttr( opposite ) :
		mc.setAttr( opposite , 0 )
	
	# Delete construction history.
	mc.delete( shp , ch=True )

	# Correct shape name
	tf = mc.listRelatives( shp , p=True , type='transform' )[0]
	mc.rename( shp , '%sShape' % tf.split( '|' )[-1] )

def importAllReference() :
	
	'''
	Import all reference node in current scene.
	Return imported reference nodes.
	'''
	rfns = mc.ls( type='reference' )
	impRfns = []
	
	for rfn in rfns :
		
		if not rfn == 'sharedReferenceNode' :
			
			try :
				
				fn = mc.referenceQuery( rfn , filename=True )
				mc.file( fn , importReference=True )
				print '%s has been imported.' % rfn
				impRfns.append( rfn )
				
			except RuntimeError :
				
				print '%s is not connected to reference file.' % rfn
				mc.lockNode( rfn , l=0 )
				mc.delete( rfn )
			
	return impRfns

def removeAllNodeInNamespace( ns='' ) :

	# Remove every nodes that belong to the given namespace.
	# Input		: Namespace
	# Output	: Empty namespace
	
	nodes = mc.ls( '%s:*' % ns , l=True )
	mc.namespace( set=':' )

	if nodes :
		# If current namespace contains nodes,
		# delete them.
		for node in nodes :

			if mc.objExists( node ) :

				lockState = mc.lockNode( node , q=True )[0]

				if lockState :
					mc.lockNode( node , l=False )
				newName = ''
				try :
					newName = mc.rename( node , node.split( ':' )[-1] )
					# print '%s has been renamed to %s' % ( node , newName )
				except :
					pass
				mc.lockNode( newName , l=lockState )

def renameAllNodeInNamespace( ns='' ) :

	# Rename every nodes that belong to the given namespace.
	# Input		: Namespace
	# Output	: Empty namespace
	
	nodes = sorted( mc.ls( '%s:*' % ns , l=True ) , reverse=True )
	mc.namespace( set=':' )

	if nodes :
		# If current namespace contains nodes,
		# delete them.
		for node in nodes :

			if mc.objExists( node ) :

				lockState = mc.lockNode( node , q=True )[0]

				if lockState :
					mc.lockNode( node , l=False )
				newName = addElementToName( node.split( ':' )[-1] , ns )
				print newName
				try :
					mc.rename( node , newName )
				except :
					pass

def removeLeafNamespace( parentNs=':' ) :

	# Recursively find leaf namespace of the given namespace,
	# then remove it.
	# Input		: Namespace
	# Output	: None
	
	nss = mc.namespaceInfo( lon=True )
	
	# If current namespace is root,
	# remove system namespaces from current namespaces.
	if parentNs == ':' :
		nss.remove( 'UI' )
		nss.remove( 'shared' )

	if nss :
		# If current namespace contains some namespaces,
		for ns in nss :
			# iterate through every child namespaces.
			
			mc.namespace( set=':' )
			mc.namespace( set=ns )
			# mc.namespace( set=currentNs )
			childNss = mc.namespaceInfo( lon=True )

			if childNss :
				# If current child namespace contains namespaces,
				# send itself to removeLeafNamespace.
				removeLeafNamespace( ns.split( ':' )[-1] )

			else :
				# If current namespace does not contain any namespace,
				# rename all node belong to the current namespace then
				# remove current namespace.
				removeAllNodeInNamespace( ns )
				try :
					mc.namespace( rm=ns )
				except :
					pass
				# print '%s has been removed.' % ns

def removeAllNamespace() :

	# Recursively remove all namespace in current scene.
	# Input		: None
	# Output	: Removed namespace.

	mc.namespace( set=':' )
	sysNss = [ 'UI' , 'shared' ]
	nss = mc.namespaceInfo( lon=True )

	if nss == sysNss :
		# If there are only 'UI' nad 'shared' namespaces exist in the scene,
		# process is done.
		print 'All namespaces have been removed.'

	else :
		# If there are some namespaces in the scene,
		# remove leaf namespace then call 'removeAllNamespace' again.
		removeLeafNamespace( ':' )
		removeAllNamespace()
	
	return [ ns for ns in nss if not ns in sysNss ]

def shaderAssigner() :
	# Assigning temporary shader to selected objects
	sels = mc.ls( sl=True )
	name = ''
	side = ''

	nameResult = mc.promptDialog(
									title='Shading Name',
									message='Enter Name:',
									button=['OK', 'Cancel'],
									defaultButton='OK',
									cancelButton='Cancel',
									dismissString='Cancel'
								)
	
	if nameResult == 'OK':
		name = mc.promptDialog(query=True, text=True)
	
	if name :
		
		shadingName = '%sTmp_lambert' % name
		
		if not mc.objExists( shadingName ) :
			
			mc.shadingNode( 'lambert' , asShader=True , n=shadingName )
		
		mc.select( sels , r=True )
		cmd = 'hyperShade -assign %s;' % shadingName
		mm.eval( cmd )
		
		mc.select( shadingName , r=True )

def fileNodeRenamer() :
	
	nameResult = mc.promptDialog(
										title='File node renamer',
										message='Enter Name:',
										button=['OK', 'Cancel'],
										defaultButton='OK',
										cancelButton='Cancel',
										dismissString='Cancel'
									)
	
	if nameResult == 'OK':
		name = mc.promptDialog( query=True , text=True )
	
	if name :
		
		sel = mc.ls( sl=True )[0]
		fileNodeName = '%s_file' % name
		mc.rename( sel , fileNodeName )

		p2d = mc.listConnections( fileNodeName , s=True , d=False )
		if p2d :
			mc.rename( p2d[0] , '%sFile_place2dTexture' % name )

def dupShade() :
	# Duplicating material from first selected to last selected object.
	sels = mc.ls( sl=True , l=True )
	
	mc.hyperShade( smn=True )
	
	refMat = mc.ls( sl=True )[0]
	
	if ':' in refMat :
		matName = refMat.split( ':' )[-1]
	else :
		matName = refMat
	
	if not mc.objExists( matName ) :
		mc.duplicate( upstreamNodes=True )
	
	for sel in sels :
		if not sel == sels[0] :
			mc.select( sel , r=True )
			mc.hyperShade( assign=matName )
	
	mc.select( sels , r=True )

def removeSelectedReference() :
	# Remove selected reference from the scene
	editSelectedReference(opr='remove')

def importSelectedReference() :
	# Import selected reference from the scene
	editSelectedReference(opr='import')

def reloadSelectedReference() :
	# Reload selected reference from the scene
	editSelectedReference(opr='reload')

def editSelectedReference(opr='reload') :

	# Edit selected reference from the scene, regarding given operation.
	# Valid operatoins are 'remove', 'import' and 'reload'.
	sels = mc.ls( sl=True )
	
	for sel in sels :
		
		if mc.objExists( sel ) and mc.referenceQuery( sel , isNodeReferenced=True ) :
			
			refNode = mc.referenceQuery( sel , referenceNode=True , topReference=True )
			fileName = mc.referenceQuery( refNode , filename=True )

			if opr == 'reload ' :
				mc.file( fileName , lr=True )
			elif opr == 'remove' :
				mc.file( fileName , rr=True )
			elif opr == 'import' :
				mc.file( fileName , i=True )

def addCheckRigKeyToSelected( valDict={} , dvs=[10,-10] ) :

	'''
	Adding check rig keyframes to selected objects and selected channel box attributes,
	regarding valDict and default values(dvs).
	'''
	
	objs = mc.ls( sl=True , l=True )
	attrs = mc.channelBox( 'mainChannelBox' , q=True , sma=True )

	if not valDict :
		valDict = {
						'tx' : [1,0] ,
						'ty' : [1,0] ,
						'tz' : [1,0] ,
						'rx' : [60,-60] ,
						'ry' : [60,-60] ,
						'rz' : [60,-60]
					}

	for obj in objs :
		for attr in attrs :
			currFrame = mc.currentTime( q=True )
			nodeAttr = '%s.%s' % ( obj , attr )
			if mc.objExists( nodeAttr ) :
				vals = dvs
				if attr in valDict.keys() :
					vals = valDict[attr]
				frameRange = 10
				if vals[1] :
					frameRange = 20
				addCheckRigKey( nodeAttr , vals , currFrame , frameRange )

	mc.playbackOptions( max=currFrame )

def addCheckRigKey( nodeAttr='' , vals=[10,-10] , startFrame=0 , frameRange=0 ) :

	'''
	Adding check rig keyframes to given nodeAttr with given vals at startFrame.
	'''

	currFrame = mc.currentTime( q=True )
	currVal = mc.getAttr( nodeAttr )
	
	frameStep = int( frameRange/2 )
	if vals[1] :
		frameStep = int( frameRange/4 )

	# Step 1 - Start frame
	mc.currentTime( startFrame , u=False )
	mc.setKeyframe( nodeAttr )

	# Step 2 - First value
	mc.currentTime( currFrame + frameStep , u=False )
	mc.setAttr( nodeAttr , currVal+vals[0] )
	mc.setKeyframe( nodeAttr , itt='linear' , ott='linear' )

	# Step 3 - Back to current value
	mc.currentTime( currFrame + 2*frameStep , u=False )
	mc.setAttr( nodeAttr , currVal )
	mc.setKeyframe( nodeAttr , itt='linear' , ott='linear' )

	if vals[1] :
		# Step 4 - Second value
		mc.currentTime( currFrame + 3*frameStep , u=False )
		mc.setAttr( nodeAttr , currVal+vals[1] )
		mc.setKeyframe( nodeAttr , itt='linear' , ott='linear' )

		# Step 5 - Back to current value
		mc.currentTime( currFrame + frameRange , u=False )
		mc.setAttr( nodeAttr , currVal )
		mc.setKeyframe( nodeAttr , itt='linear' , ott='linear' )

	mc.currentTime( mc.currentTime( q=True ) )