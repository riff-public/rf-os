import maya.cmds as mc
import pkmel.core as pc
import pkmel.rigTools as rigTools
reload( pc )
reload( rigTools )

import splineIkRig as splineIkRig
reload( splineIkRig )
import fkRig as fkRig
reload( fkRig )

'''
Example
butt = TailRig( 'buttBase_jnt' ,
				'anim_grp' ,
				size ,
				[ 'butt1_jnt' ,
					'butt2_jnt' ,
					'butt3_jnt' ,
					'butt4_jnt' ,
					'butt5_jnt' ,
					'butt6_jnt' ,
					'butt7_jnt'
				] ,
				'butt' ,
				'' ,
				'y' ,
				(0,1,0) ,
				(1,0,0) ,
				[ 'butt1IkUp_jnt' ,
					'butt2IkUp_jnt' ,
					'butt3IkUp_jnt' ,
					'butt4IkUp_jnt' ,
					'butt5IkUp_jnt' ,
					'butt6IkUp_jnt' ,
					'butt7IkUp_jnt'
				] ,
				[ 'butt1IkCtrl_jnt' ,
					'butt2IkCtrl_jnt' ,
					'butt3IkCtrl_jnt' ,
					'butt4IkCtrl_jnt'
				] ,
				'tailIk_crv' ,
				'tailIkUp_crv'
			)
'''



class TailRig( object ) :

	def __init__( self ,
					parent 		= '' ,
					animGrp 	= 'anim_grp' ,
					charSize 	= 1 ,
					tmpJnt 		= [] ,
					name 		= '' ,
					side 		= '' ,
					ax 			= 'y' ,
					aimVec 		= (0,1,0) ,
					upVec 		= (1,0,0 ) ,
					ikUpJnts	= [] ,
					ikCtrlJnts	= [] ,
					ikCrv		= '' ,
					ikUpCrv		= ''
				) :

		# Main group
		self.rigGrp = pc.Null()
		self.rigGrp.name = '%sRig%s_grp' % ( name , side )
		mc.parentConstraint( parent , self.rigGrp )

		self.ctrl = pc.Control( 'stick' )
		self.ctrl.name = '%s%s_ctrl' % ( name , side )
		self.ctrl.lockHideAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
		self.ctrl.color = 'green'
		self.ctrl.scaleShape( charSize )
		self.ctrlZgrp = pc.group( self.ctrl )
		self.ctrlZgrp.name = '%sCtrlZro%s_grp' % ( name , side )
		self.ctrl.add( ln='fkIk' , min=0 , max=1 , dv=0 , k=True )
		mc.parentConstraint( tmpJnt[0] , self.ctrlZgrp )
		self.ctrlZgrp.parent( self.rigGrp )

		# self.stillGrp = pc.Null()
		# self.stillGrp.name = '%sStill%s_grp' % ( name , side )

		self.jntGrp = pc.Null()
		self.jntGrp.name = '%sJnt%s_grp' % ( name , side )

		# self.ikhGrp = pc.Null()
		# self.ikhGrp.name = '%sIkh%s_grp' % ( name , side )

		# FK
		# FK - joints
		self.fkJntGrp = pc.Null()
		self.fkJntGrp.name = '%sFkJnt%s_grp' % ( name , side )
		fkJnts = []
		for jnt in tmpJnt :
			idx = tmpJnt.index( jnt )
			currJnt = rigTools.jointAt( jnt )
			currJnt.name = '%s%dFk%s_jnt' % ( name , (idx+1) , side )
			fkJnts.append( currJnt )

			if not idx == 0 :
				currJnt.parent( fkJnts[ idx-1 ] )
			else :
				zro = rigTools.zeroGroup( currJnt )
				zro.name = '%s%dFkJntZro%s_grp' % ( name , (idx+1) , side )
				zro.parent( self.fkJntGrp )

		self.fk = fkRig.FkRig(
								parent = parent ,
								animGrp = animGrp ,
								charSize = charSize ,
								tmpJnt = fkJnts ,
								name = name ,
								side = side ,
								ax = ax ,
								shape = 'circle'
							)

		# Adding local/world to the first FK joint
		# mc.select( fk.ctrls[0] , fk.zGrps[0] , animGrp , fk.ofstGrps[0] )
		# rigTools.doAddOrientLocWor()

		self.fk.rigGrp.parent( self.rigGrp )
		
		# IK
		ikJnts = []
		for jnt in tmpJnt :
			idx = tmpJnt.index( jnt )
			currJnt = rigTools.jointAt( jnt )
			currJnt.name = '%s%dIk%s_jnt' % ( name , (idx+1) , side )
			ikJnts.append( currJnt )

			if not idx == 0 :
				mc.parent( currJnt , ikJnts[ idx-1 ] )
		
		self.ik = splineIkRig.SplineIkRig(
											ikJnts = ikJnts ,
											ikUpJnts = ikUpJnts ,
											ikPosJnts = [] ,
											ikCtrlJnts = ikCtrlJnts ,
											ikCrv = ikCrv ,
											ikUpCrv = ikUpCrv ,
											ax = ax ,
											aimVec = aimVec ,
											upVec = upVec ,
											name = name ,
											side = side
										)

		for ix in range( 0 , len( self.ik.ctrls ) ) :
			self.ik.ctrls[ix].scaleShape( charSize )

		mc.parentConstraint( parent , self.ik.rig_grp , mo=True )
		mc.scaleConstraint( parent , self.ik.rig_grp , mo=True )
		mc.parentConstraint( animGrp , self.ik.worOri_grp , mo=True )

		# FK/IK
		self.rev = pc.Reverse()
		self.rev.name = '%sFkIk%s_rev' % ( name , side )

		self.ctrl.attr( 'fkIk' ) >> self.rev.attr( 'ix' )
		self.rev.attr( 'ox' ) >> self.fk.rigGrp.attr( 'v' )
		self.ctrl.attr( 'fkIk' ) >> self.ik.rig_grp.attr( 'v' )
		self.ctrl.attr( 'fkIk' ) >> self.ik.still_grp.attr( 'v' )

		self.fkIkParCons = []
		for ix in range( 0 , len( tmpJnt ) ) :
			con = pc.parentConstraint( fkJnts[ix] , ikJnts[ix] , tmpJnt[ix] )
			self.ctrl.attr( 'fkIk' ) >> con.attr( 'w1' )
			self.rev.attr( 'ox' ) >> con.attr( 'w0' )
			
			self.fkIkParCons.append( con )
		
		# Cleanup hierarchy
		self.fkJntGrp.parent( self.jntGrp )
		self.ik.jnt_grp.parent( self.jntGrp )
		self.ik.rig_grp.parent( self.rigGrp )
		# ik.ikh_grp.parent( self.ikhGrp )
		self.rigGrp.parent( animGrp )

		self.jntGrp.parent( 'jnt_grp' )
		self.ik.ikh_grp.parent( 'ikh_grp' )
		self.ik.still_grp.parent( 'still_grp' )
		self.ik.ik_crv.attr('v').v = 0
		ikUpShp = pc.Dag( self.ik.ikUp_crv.shape )
		ikUpShp.attr('overrideEnabled' ).v = 1
		ikUpShp.attr('overrideDisplayType' ).v = 2
