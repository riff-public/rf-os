import maya.cmds as mc
import pkmel.core as pc
reload( pc )
import pkmel.rigTools as rigTools
reload( rigTools )
import pkmel.mainGroup as pmain
reload( pmain )
import pkmel.rootRig as proot
reload( proot )
import pkmel.pelvisRig as ppelv
reload( ppelv )
import pkmel.spineRig as pspi
reload( pspi )
import pkmel.neckRig as pneck
reload( pneck )
import pkmel.headRig as phead
reload( phead )
import pkmel.clavicleRig as pclav
reload( pclav )
import pkmel.armRig as parm
reload( parm )
import pkmel.legRig as pleg
reload( pleg )
import pkmel.fingerRig as pfngr
reload( pfngr )
import pkmel.thumbRig as pthmb
reload( pthmb )
import pkmel.ribbon as prbn
reload( prbn )
import pkmel.fkRig as pfk
reload( pfk )
import pkmel.backLegRig as pbleg
reload( pbleg )
import pkmel.fkGroupRig as pfkg
reload( pfkg )
import pkmel.tailRig as ptail
reload( ptail )

def main() :

	# Naming
	charName = ''
	elem = ''

	# Rig
	mainGroup = pmain.MainGroup()
	rigTools.nodeNaming(
							mainGroup ,
							charName=charName ,
							elem=elem ,
							side=''
						)

	anim = 'anim_grp'
	jnt = 'jnt_grp'
	skin = 'skin_grp'
	ikh = 'ikh_grp'
	still = 'still_grp'
	size = 0.05
	
	# Arrange hierarchy for Friends!!!!
	mc.parent( mainGroup.placement_ctrl , w=True )
	mc.parent( mainGroup.still_grp , w=True )
	mainGroup.offset_ctrl.name = 'placementCtrlOfst_grp'
	mc.delete( mainGroup.offset_ctrl.shape )
	mc.select( mainGroup.placement_ctrl )
	placementGimbal = rigTools.doAddGimbal()
	mainGroup.offset_ctrl.parent( placementGimbal )
	mc.delete( mainGroup.rig_grp )

	# Root
	print 'Rigging Root'
	rootRig = proot.RootRig(
								animGrp=anim ,
								skinGrp=skin ,
								charSize=size ,
								tmpJnt='root_tmpJnt'
							)

	rigTools.nodeNaming(
							rootRig ,
							charName=charName ,
							elem=elem ,
							side=''
						)

	# Pelvis
	print 'Rigging pelvis'
	pelvisRig = ppelv.PelvisRig(
									parent=rootRig.root_jnt ,
									animGrp=anim ,
									skinGrp=skin ,
									charSize=size ,
									tmpJnt='pelvis_tmpJnt'
								)

	rigTools.nodeNaming(
							pelvisRig ,
							charName=charName ,
							elem=elem ,
							side=''
						)

	# Spine
	print 'Rigging Spine'
	spineRig = pspi.SpineRig(
								parent=rootRig.root_jnt ,
								animGrp=anim ,
								jntGrp=jnt ,
								skinGrp=skin ,
								stillGrp=still ,
								ribbon=False ,
								ax='z' ,
								charSize=size ,
								tmpJnt=(
											'spine1_tmpJnt' ,
											'spine2_tmpJnt' ,
											'spine3_tmpJnt' ,
											'chestIkPiv_jnt' ,
										)
							)

	rigTools.nodeNaming(
							spineRig ,
							charName=charName ,
							elem=elem ,
							side=''
						)

	rigTools.dummyNaming(
							obj=spineRig.lowSpineRbn ,
							attr='rbn' ,
							dummy='lowSpineRbn' ,
							charName=charName ,
							elem=elem ,
							side=''
						)

	rigTools.dummyNaming(
							obj=spineRig.upSpineRbn ,
							attr='rbn' ,
							dummy='upSpineRbn' ,
							charName=charName ,
							elem=elem ,
							side=''
						)

	# Spine fix
	spinePos = pc.Null()
	spinePos.name = 'spinePos_grp'
	spinePos.snap( spineRig.spineRig_grp )
	mc.pointConstraint( pelvisRig.pelvis_jnt , spinePos )
	spinePos.parent( spineRig.spineRig_grp )

	spineElems = [
					spineRig.spineCtrl_grp ,
					spineRig.spineFkCtrl_grp ,
					spineRig.spineIkCtrl_grp ,
					spineRig.spineRbnAnim_grp
					]
	for spineElem in spineElems :
		
		attrs = ( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' )
		for attr in attrs :
			spineElem.attr( attr ).lock = 0
		
		# spineElem.parent( spinePos )
		mc.parentConstraint( spinePos , spineElem , mo=True )

	attrs = ( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' )
	for attr in attrs :
		spineRig.spineIkCtrlLoc_grp.attr( attr ).lock = 0
	mc.parentConstraint( rootRig.root_jnt , spineRig.spineIkCtrlLoc_grp , mo=True )

	# Neck
	print 'Rigging Neck'
	neckRig = pneck.NeckRig(
								parent=spineRig.spine3_jnt ,
								animGrp=anim ,
								jntGrp=jnt ,
								skinGrp=skin ,
								stillGrp=still ,
								ribbon=False ,
								ax='z' ,
								charSize=size ,
								tmpJnt=( 'neck1_tmpJnt' , 'head1_tmpJnt' )
							)

	rigTools.nodeNaming(
							neckRig ,
							charName=charName ,
							elem=elem ,
							side=''
						)

	rigTools.dummyNaming(
							obj=neckRig.neckRbn ,
							attr='rbn' ,
							dummy='neckRbn' ,
							charName=charName ,
							elem=elem ,
							side=''
						)
	mc.setAttr( 'neckRbn_ctrlShape.upVector' , 1 , 0 , 0 )

	# Head
	print 'Rigging Head'
	headRig = phead.HeadRig(
								parent = neckRig.neck2_jnt ,
								animGrp = anim ,
								skinGrp = skin ,
								charSize = size ,
								tmpJnt = (
											'head1_tmpJnt' ,
											'head2_tmpJnt' ,
											'eyeLFT_tmpJnt' ,
											'eyeRGT_tmpJnt' ,
											'lowerJaw1_tmpJnt' ,
											'lowerJaw2_tmpJnt' ,
											'lowerJaw3_tmpJnt' ,
											'upperJaw1_tmpJnt' ,
											'upperJaw2_tmpJnt' ,
											'eyeTrgt_tmpJnt' ,
											'eyeTrgtLFT_tmpJnt' ,
											'eyeTrgtRGT_tmpJnt'
										)
							)

	rigTools.nodeNaming(
							headRig ,
							charName = '' ,
							elem = '' ,
							side = ''
						)

	attrs = ( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' )
	for attr in attrs :
		neckRig.neckRbn.rbnRootTwst_grp.attr( attr ).l = 0

	pc.parentConstraint( headRig.head1_jnt , neckRig.neckRbn.rbnRootTwst_grp )
	pc.parentConstraint( headRig.head1_jnt , neckRig.neckRbn.rbnEndCtrlZro_grp )

	# Left iris
	lIrisRig = phead.IrisRig(
								parent=headRig.eyeLFT_jnt ,
								eyeGmbl=headRig.eyeGmblLFT_ctrl ,
								charSize=size ,
								tmpJnt='irisLFT_tmpJnt'
							)

	rigTools.nodeNaming(
							lIrisRig ,
							charName = '' ,
							elem = '' ,
							side = 'LFT'
						)

	# Right iris
	rIrisRig = phead.IrisRig(
								parent=headRig.eyeRGT_jnt ,
								eyeGmbl=headRig.eyeGmblRGT_ctrl ,
								charSize=size ,
								tmpJnt='irisRGT_tmpJnt'
							)

	rigTools.nodeNaming(
							rIrisRig ,
							charName = '' ,
							elem = '' ,
							side = 'RGT'
						)

	# Left Eye Spec
	lEyeSpecRig = phead.EyeSpecRig(
										parent=headRig.eyeLFT_jnt ,
										worldSpace = headRig.head1_jnt ,
										eyeGmbl=headRig.eyeGmblLFT_ctrl ,
										charSize=size ,
										tmpJnt='eyeSpecLFT_tmpJnt'
									)

	rigTools.nodeNaming(
							lEyeSpecRig ,
							charName = '' ,
							elem = '' ,
							side = 'LFT'
						)

	# Right Eye Spec
	REyeSpecRig = phead.EyeSpecRig(
										parent=headRig.eyeRGT_jnt ,
										worldSpace = headRig.head1_jnt ,
										eyeGmbl=headRig.eyeGmblRGT_ctrl ,
										charSize=size ,
										tmpJnt='eyeSpecRGT_tmpJnt'
									)

	rigTools.nodeNaming(
							REyeSpecRig ,
							charName = '' ,
							elem = '' ,
							side = 'RGT'
						)

	# Left Scap
	print 'Rigging Left Scap'
	lftScapRig = pclav.ClavicleRig(
										parent=spineRig.spine2_jnt ,
										side='LFT' ,
										animGrp=anim ,
										skinGrp=skin ,
										charSize=size ,
										tmpJnt = (
													'scapLFT_tmpJnt' ,
													'upLegLFT_tmpJnt'
												)
									)

	rigTools.nodeNaming(
							lftScapRig ,
							charName = charName ,
							elem = 'Scap' ,
							side = 'LFT'
						)

	# Right Scap
	print 'Rigging Right Scap'
	rgtScapRig = pclav.ClavicleRig(
										parent=spineRig.spine2_jnt ,
										side='RGT' ,
										animGrp=anim ,
										skinGrp=skin ,
										charSize=size ,
										tmpJnt = (
													'scapRGT_tmpJnt' ,
													'upLegRGT_tmpJnt'
												)
									)

	rigTools.nodeNaming(
							rgtScapRig ,
							charName = charName ,
							elem = 'Scap' ,
							side = 'RGT'
						)

	# Left Front Leg
	print 'Rigging Left Front Leg'
	lftFrRig = pleg.LegRig(
								parent=lftScapRig.clav2_jnt ,
								side='LFT' ,
								animGrp=anim ,
								jntGrp=jnt ,
								skinGrp=skin ,
								stillGrp=still ,
								ribbon=True ,
								charSize=size ,
								tmpJnt=(
											'upLegLFT_tmpJnt' ,
											'lowLegLFT_tmpJnt' ,
											'ankleLFT_tmpJnt' ,
											'ballLFT_tmpJnt' ,
											'toeLFT_tmpJnt' ,
											'heelLFT_tmpJnt' ,
											'footInLFT_tmpJnt' ,
											'footOutLFT_tmpJnt' ,
											'kneeIkPivLFT_tmpJnt'
										)
							)

	rigTools.nodeNaming(
							lftFrRig ,
							charName=charName ,
							elem='Front' ,
							side='LFT'
						)

	rigTools.dummyNaming(
							obj=lftFrRig.upLegRbn ,
							attr='rbn' ,
							dummy='upArmRbn' ,
							charName=charName ,
							elem='' ,
							side='LFT'
						)

	rigTools.dummyNaming(
							obj=lftFrRig.lowLegRbn ,
							attr='rbn' ,
							dummy='forearmRbn' ,
							charName=charName ,
							elem='' ,
							side='LFT'
						)

	# Right Front Leg
	print 'Rigging Right Front Leg'
	rgtFrRig = pleg.LegRig(
								parent=rgtScapRig.clav2_jnt ,
								side='RGT' ,
								animGrp=anim ,
								jntGrp=jnt ,
								skinGrp=skin ,
								stillGrp=still ,
								ribbon=True ,
								charSize=size ,
								tmpJnt=(
											'upLegRGT_tmpJnt' ,
											'lowLegRGT_tmpJnt' ,
											'ankleRGT_tmpJnt' ,
											'ballRGT_tmpJnt' ,
											'toeRGT_tmpJnt' ,
											'heelRGT_tmpJnt' ,
											'footInRGT_tmpJnt' ,
											'footOutRGT_tmpJnt' ,
											'kneeIkPivRGT_tmpJnt'
										)
							)

	rigTools.nodeNaming(
							rgtFrRig ,
							charName=charName ,
							elem='Front' ,
							side='RGT'
						)

	rigTools.dummyNaming(
							obj=rgtFrRig.upLegRbn ,
							attr='rbn' ,
							dummy='upArmRbn' ,
							charName=charName ,
							elem='' ,
							side='RGT'
						)

	rigTools.dummyNaming(
							obj=rgtFrRig.lowLegRbn ,
							attr='rbn' ,
							dummy='forearmRbn' ,
							charName=charName ,
							elem='' ,
							side='RGT'
						)

	# Left Hip
	print 'Rigging Left Hip'
	lftHipRig = pclav.ClavicleRig(
										parent=pelvisRig.pelvis_jnt ,
										side='LFT' ,
										animGrp=anim ,
										skinGrp=skin ,
										charSize=size ,
										tmpJnt = (
													'hipLFT_tmpJnt' ,
													'upLegBackLFT_tmpJnt'
												)
									)

	rigTools.nodeNaming(
							lftHipRig ,
							charName = charName ,
							elem = 'Hip' ,
							side = 'LFT'
						)

	# Right Hip
	print 'Rigging Right Hip'
	rgtHipRig = pclav.ClavicleRig(
										parent=pelvisRig.pelvis_jnt ,
										side='RGT' ,
										animGrp=anim ,
										skinGrp=skin ,
										charSize=size ,
										tmpJnt = (
													'hipRGT_tmpJnt' ,
													'upLegBackRGT_tmpJnt'
												)
									)

	rigTools.nodeNaming(
							rgtHipRig ,
							charName = charName ,
							elem = 'Hip' ,
							side = 'RGT'
						)

	# Left Back Leg
	print 'Rigging Left Back Leg'
	lftBckLeg = pbleg.BackLegRig(
									parent=lftHipRig.clav2_jnt ,
									side='LFT' ,
									animGrp=anim ,
									jntGrp=jnt ,
									skinGrp=skin ,
									stillGrp=still ,
									ribbon=True ,
									charSize=size ,
									tmpJnt=(
												'upLegBackLFT_tmpJnt' ,
												'midLegBackLFT_tmpJnt' ,
												'lowLegBackLFT_tmpJnt' ,
												'ankleBackLFT_tmpJnt' ,
												'ballBackLFT_tmpJnt' ,
												'toeBackLFT_tmpJnt' ,
												'heelBackLFT_tmpJnt' ,
												'footInBackLFT_tmpJnt' ,
												'footOutBackLFT_tmpJnt' ,
												'kneeIkBackPivLFT_tmpJnt'
											)
								)

	rigTools.nodeNaming(
							lftBckLeg ,
							charName=charName ,
							elem='Back' ,
							side='LFT'
						)
	rigTools.dummyNaming(
							obj=lftBckLeg.upLegRbn ,
							attr='rbn' ,
							dummy='upLegRbn' ,
							charName=charName ,
							elem='Back' ,
							side='LFT'
						)
	rigTools.dummyNaming(
							obj=lftBckLeg.midLegRbn ,
							attr='rbn' ,
							dummy='midLegRbn' ,
							charName=charName ,
							elem='Back' ,
							side='LFT'
						)
	rigTools.dummyNaming(
							obj=lftBckLeg.lowLegRbn ,
							attr='rbn' ,
							dummy='lowLegRbn' ,
							charName=charName ,
							elem='Back' ,
							side='LFT'
						)

	# Right Back Leg
	print 'Rigging Right Back Leg'
	lftBckLeg = pbleg.BackLegRig(
									parent=rgtHipRig.clav2_jnt ,
									side='RGT' ,
									animGrp=anim ,
									jntGrp=jnt ,
									skinGrp=skin ,
									stillGrp=still ,
									ribbon=True ,
									charSize=size ,
									tmpJnt=(
												'upLegBackRGT_tmpJnt' ,
												'midLegBackRGT_tmpJnt' ,
												'lowLegBackRGT_tmpJnt' ,
												'ankleBackRGT_tmpJnt' ,
												'ballBackRGT_tmpJnt' ,
												'toeBackRGT_tmpJnt' ,
												'heelBackRGT_tmpJnt' ,
												'footInBackRGT_tmpJnt' ,
												'footOutBackRGT_tmpJnt' ,
												'kneeIkBackPivRGT_tmpJnt'
											)
								)

	rigTools.nodeNaming(
							lftBckLeg ,
							charName=charName ,
							elem='Back' ,
							side='RGT'
						)
	rigTools.dummyNaming(
							obj=lftBckLeg.upLegRbn ,
							attr='rbn' ,
							dummy='upLegRbn' ,
							charName=charName ,
							elem='Back' ,
							side='RGT'
						)
	rigTools.dummyNaming(
							obj=lftBckLeg.midLegRbn ,
							attr='rbn' ,
							dummy='midLegRbn' ,
							charName=charName ,
							elem='Back' ,
							side='RGT'
						)
	rigTools.dummyNaming(
							obj=lftBckLeg.lowLegRbn ,
							attr='rbn' ,
							dummy='lowLegRbn' ,
							charName=charName ,
							elem='Back' ,
							side='RGT'
						)

	###############################################################################################################
	# ADD JOINT

	# Left Ear
	print 'Rigging Left Ear'
	lftEarRig = pfk.FkRig(
								parent=headRig.head2_jnt ,
								animGrp=anim ,
								charSize=size ,
								tmpJnt=[
											'ear1LFT_jnt' ,
											'ear2LFT_jnt' ,
											'ear3LFT_jnt' ,
											'ear4LFT_jnt'
										] ,
								name='ear' ,
								side='LFT' ,
								ax='y' ,
								shape='circle'
							)

	# Right Ear
	print 'Rigging Right Ear'
	rgtEarRig = pfk.FkRig(
								parent=headRig.head2_jnt ,
								animGrp=anim ,
								charSize=size ,
								tmpJnt=[
											'ear1RGT_jnt' ,
											'ear2RGT_jnt' ,
											'ear3RGT_jnt' ,
											'ear4RGT_jnt'
										] ,
								name='ear' ,
								side='RGT' ,
								ax='y' ,
								shape='circle'
							)

	# Tail
	print 'Rigging Tail'
	tailRig = pfk.FkRig(
								parent=pelvisRig.pelvis_jnt ,
								animGrp=anim ,
								charSize=size ,
								tmpJnt=[
											'tail1_jnt' ,
											'tail2_jnt' ,
											'tail3_jnt' ,
											'tail4_jnt' ,
											'tail5_jnt'
										] ,
								name='tail' ,
								side='' ,
								ax='y' ,
								shape='circle'
							)

	# Tongue
	print 'Rigging Tongue'
	tongueRig = pfk.FkRig(
								parent = headRig.jaw1LWR_jnt ,
								animGrp = 'anim_grp' ,
								charSize = size ,
								tmpJnt = [
											'tongue1_jnt' ,
											'tongue2_jnt' ,
											'tongue3_jnt' ,
											'tongue4_jnt'
										] ,
								name = 'tongue' ,
								side = '' ,
								ax='y' ,
								shape='circle'
						)

	# Upper Teeth
	print 'Rigging Upper Teeth'
	upperTeethRig = pfkg.FkGroupRig(
										parent = headRig.head1_jnt ,
										animGrp = 'anim_grp' ,
										charSize = size ,
										tmpJnt = [ 'upperTeeth_jnt' ] ,
										name = 'upperTeeth' ,
										side = '' ,
										shape='cube'
									)

	# Lower Teeth
	print 'Rigging Lower Teeth'
	lowerTeethRig = pfkg.FkGroupRig(
										parent = headRig.jaw1LWR_jnt ,
										animGrp = 'anim_grp' ,
										charSize = size ,
										tmpJnt = [ 'lowerTeeth_jnt' ] ,
										name = 'lowerTeeth' ,
										side = '' ,
										shape='cube'
									)

	# Nose 
	print 'Rigging Nose'
	noseRig = pfkg.FkGroupRig(
								parent = headRig.head1_jnt  ,
								animGrp = 'anim_grp' ,
								charSize = size ,
								tmpJnt = [ 'nose_jnt' ] ,
								name = 'nose' ,
								side = '' ,
								shape='cube'
								)


	rigTools.adjustRadius( 0.05 )
	mc.delete( 'tmpJnt_grp' )
	mc.parent( 'addJnt_grp' , 'skin_grp' )
	mc.select( cl=True )