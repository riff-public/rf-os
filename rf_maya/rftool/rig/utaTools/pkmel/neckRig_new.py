# Neck rig module
import maya.cmds as mc
import pymel.core as pm
import pkmel.core as pc
import pkmel.rigTools as rigTools
import pkmel.ribbon as pr
reload( pc )
reload( rigTools )
reload( pr )

class NeckRig( object ) :
	
	def __init__(
						self ,
						parent 		= 'spine3_jnt' ,
						animGrp 	= 'anim_grp' ,
						jntGrp 		= 'jnt_grp' ,
						skinGrp 	= 'skin_grp' ,
						stillGrp 	= 'still_grp' ,
						ribbon 		= False ,
						ax 			= 'y' ,
						charSize 	= 1 ,
						tmpJnt 		= (
										'neck1_tmpJnt' ,
										'head1_tmpJnt'
										), 
						rotateOrder = 'zxy'
					) :
		
		# Checking parent
		spine3Jnt = pc.Dag( parent )
		if not spine3Jnt.exists :
			spine3Jnt = pc.Null()
			spine3Jnt.parent( skinGrp )
		
		# Template object
		neck1 = pc.Dag( tmpJnt[0] )
		neck2 = pc.Dag( tmpJnt[1] )
		
		# Skin joints
		self.neck1_jnt = rigTools.jointAt( neck1 )
		self.neck2_jnt = rigTools.jointAt( neck2 )
		
		self.neck2_jnt.parent( self.neck1_jnt )
		self.neck1_jnt.parent( spine3Jnt )
		
		mc.parentConstraint( self.neck1_jnt , neck1 )
		# mc.parentConstraint( self.neck2_jnt , neck2 )
		
		# Main group
		self.neckRig_grp = pc.Null()
		self.neckRigGrp_parCons = pc.parentConstraint( spine3Jnt , self.neckRig_grp )
		
		self.neckJnt_grp = pc.Null()
		self.neckJntGrp_parCons = pc.parentConstraint( spine3Jnt , self.neckJnt_grp )
		
		# Length
		self.neckLen = pc.distance( neck1 , neck2 )
		
		# ----- FK -----
		# FK main group
		self.neckFkCtrl_grp = pc.Null()
		self.neckFkCtrl_grp.snap( neck1 )
		self.neckFkCtrl_grp.parent( self.neckRig_grp )
		
		self.neckFkJnt_grp = pc.Null()
		self.neckFkJnt_grp.snap( neck1 )
		self.neckFkJnt_grp.parent( self.neckJnt_grp )
		
		# FK joints
		self.neck1Fk_jnt = rigTools.jointAt( neck1 )
		self.neck2Fk_jnt = rigTools.jointAt( neck2 )
		
		self.neck2Fk_jnt.parent( self.neck1Fk_jnt )
		self.neck1Fk_jnt.parent( self.neckFkJnt_grp )
		
		# FK controls
		self.neck1Fk_ctrl = rigTools.jointControl( 'circle' )
		self.neck1FkGmbl_ctrl = pc.addGimbal( self.neck1Fk_ctrl )
		self.neck1FkCtrlZro_grp = rigTools.zeroGroup( self.neck1Fk_ctrl )
		
		# FK control - parenting and positioning
		self.neck1FkCtrlZro_grp.snapPoint( neck1 )
		self.neck1Fk_ctrl.snapOrient( neck1 )
		self.neck1Fk_ctrl.freeze( r=True )
		self.neck1FkCtrlZro_grp.parent( self.neckFkCtrl_grp )
		
		# FK control - shape adjustment
		self.neck1Fk_ctrl.color = 'red'
		self.neck1Fk_ctrl.scaleShape( 3 * charSize )
		
		# FK control - rotate order adjustment
		self.neck1Fk_ctrl.rotateOrder = rotateOrder
		self.neck1FkGmbl_ctrl.rotateOrder = rotateOrder
		
		# FK control - stretch
		( self.neck1FkStretch_add ,
		self.neck1FkStretch_mul ) = rigTools.fkStretch( ctrl = self.neck1Fk_ctrl ,
									target = self.neck2Fk_jnt ,
									ax = ax )
		
		# FK control - adjusting stretch amplitude
		self.neck1FkStretchAmp_mul = rigTools.attrAmper( self.neck1Fk_ctrl.attr('stretch') ,
														self.neck1FkStretch_mul.attr('i2') ,
														dv = 0.1
														)
		
		# FK control - local/world setup
		( self.neck1FkCtrlLoc_grp ,
		self.neck1FkCtrlWor_grp ,
		self.neck1FkCtrlWorGrp_oriCons ,
		self.neck1FkCtrlZroGrp_oriCons ,
		self.neck1FkCtrlZroGrpOriCons_rev ) = rigTools.orientLocalWorldCtrl( self.neck1Fk_ctrl ,
																			self.neckFkCtrl_grp ,
																			animGrp ,
																			self.neck1FkCtrlZro_grp
																		)
		
		# self.neck1FkCtrlLoc_grp.parent( self.neckFkCtrl_grp )
		# self.neck1FkCtrlWor_grp.parent( self.neckFkCtrl_grp )
		
		# Connect to joint
		self.neck1FkJnt_oriCons = pc.parentConstraint( self.neck1FkGmbl_ctrl , self.neck1Fk_jnt )
		
		# Connect to joint
		self.neck1Jnt_parCons = pc.parentConstraint( self.neck1Fk_jnt , self.neck1_jnt )
		self.neck2Jnt_parCons = pc.parentConstraint( self.neck2Fk_jnt , self.neck2_jnt )
		
		# Group
		self.neckRig_grp.parent( animGrp )
		self.neckJnt_grp.parent( jntGrp )
		
		# Ribbon - ribbon
		if ribbon != None:
			if ribbon == True:
				self.neckRbn = pr.RibbonIkHi( size = self.neckLen , ax = '%s+' % ax )
			else :
				self.neckRbn = pr.RibbonIkLow( size = self.neckLen , ax = '%s+' % ax )
			
			self.neckRbn.rbnAnim_grp.snap( neck1 )
			mc.delete( pc.aimConstraint( neck2 ,
											self.neckRbn.rbnAnim_grp ,
											aim = (0,1,0) , u = (1,0,0) ,
											wut = 'vector' , wu = (1,0,0)
										)
					)
			self.neckRbn_parCons = pc.parentConstraint( self.neck1_jnt , self.neckRbn.rbnAnim_grp , mo = True )

			self.neckRbnRootCtrl_pntCons = pc.pointConstraint( self.neck1_jnt , self.neckRbn.rbnRoot_ctrl )
			mc.delete(mc.pointConstraint(neck2, self.neckRbn.rbnEnd_ctrl))  # **

			###

			# Ribbon upper leg - twist distributetion
			# --- create upLeg non roll
			self.neckNonRoll_grp = pc.Null()
			self.neckNonRoll_grp.snap(self.neck1_jnt)

			self.neckNonRoll_grp.attr('v').value = 0
			
			self.neckNonRollAim_grp = pc.Null()
			self.neckNonRollZro_grp = pc.Null()
		
			self.neckNonRollZro_grp.parent(self.neckNonRollAim_grp)
			self.neckNonRollZro_grp.snap(self.neckRbn.rbnRoot_jnt)

			self.neckNonRollAim_grp.parent(self.neckNonRoll_grp)
			self.neckNonRoll_grp.parent(parent)

			self.neckNonRoll_pntCons = pc.pointConstraint( self.neck1_jnt , self.neckNonRoll_grp )

			# fk non roll
			self.neckRbnRootNonRoll_jnt = pc.Joint()
			self.neckRbnRootNonRoll_jnt.snap(self.neckRbn.rbnRoot_jnt)

			self.neckRbnEndNonRoll_jnt = pc.Joint()
			self.neckRbnEndNonRoll_jnt.snap(self.neckRbn.rbnEnd_jnt)

			self.neckRbnEndNonRoll_jnt.parent(self.neckRbnRootNonRoll_jnt)
			self.neckRbnRootNonRoll_jnt.parent(self.neckNonRollZro_grp)

			self.neckRbnRootNonRoll_parCons = pc.parentConstraint(self.neck1_jnt, self.neckRbnRootNonRoll_jnt, mo=True)
			# # ** self.neckRbnEndNonRoll_parCons = pc.parentConstraint(self.lowLeg_jnt, self.neckRbnEndNonRoll_jnt, mo=True)

			# twist jnt
			self.neckRbnRootTwst_jnt 	= pc.Joint()
			self.neckRbnRootTwst_jnt.rotateOrder = 'zxy'
			self.neckRbnRootTwst_jnt.snap(self.neckRbnRootNonRoll_jnt)
			
			self.neckRbnEndTwst_jnt 	= pc.Joint()
			self.neckRbnEndTwst_jnt.rotateOrder = 'zxy'
			self.neckRbnEndTwst_jnt.snap(self.neckRbnEndNonRoll_jnt)

			self.neckRbnRootTwst_jnt.parent(self.neckRbnRootNonRoll_jnt)
			self.neckRbnEndTwst_jnt.parent(self.neckRbnRootNonRoll_jnt)	

			# ik non roll
			self.neckRbnRootNonRollIk_jnt = pc.Joint()
			self.neckRbnRootNonRollIk_jnt.snap(self.neckRbn.rbnRoot_jnt)

			self.neckRbnEndNonRollIk_jnt = pc.Joint()
			self.neckRbnEndNonRollIk_jnt.snap(self.neckRbn.rbnEnd_jnt)

			self.neckRbnEndNonRollIk_jnt.parent(self.neckRbnRootNonRollIk_jnt)
			self.neckRbnRootNonRollIk_jnt.parent(self.neckNonRollZro_grp)

			# create ik handle
			self.neckNonRoll_ikh = pc.IkRp( sj = self.neckRbnRootNonRollIk_jnt , ee = self.neckRbnEndNonRollIk_jnt )
			mc.setAttr('%s.poleVectorX' %self.neckNonRoll_ikh, 0.0)
			mc.setAttr('%s.poleVectorY' %self.neckNonRoll_ikh, 0.0)
			mc.setAttr('%s.poleVectorZ' %self.neckNonRoll_ikh, 0.0)
			self.neckNonRoll_ikh.parent(self.neckRbnEndNonRoll_jnt)
			for axis in 'XYZ':
				self.neckNonRoll_ikh.attr('poleVector%s' %axis).l = True

			# create upVec grp
			upV = pm.dt.Vector(self.neckRbn.upVec)
			aimV = pm.dt.Vector(self.neckRbn.aimVec)
			crossV = upV.cross(aimV)
			crossVTrans = crossV * 0.1
			# upVecTrans = [self.neckRbn.upVec[0]*0.1, self.neckRbn.upVec[1]*0.1, self.neckRbn.upVec[2]*0.1]

			self.neckRbnRootUpVec_grp = pc.Null()
			self.neckRbnRootUpVec_grp.snap(self.neckRbnRootNonRollIk_jnt)
			self.neckRbnRootUpVec_grp.parent(self.neckRbnRootNonRollIk_jnt)
			mc.setAttr('%s.t' %self.neckRbnRootUpVec_grp, crossVTrans[0], crossVTrans[1], crossVTrans[2])

			self.neckRbnEndUpVec_grp = pc.Null()
			self.neckRbnEndUpVec_grp.snap(self.neckRbnEndNonRoll_jnt)
			self.neckRbnEndUpVec_grp.parent(self.neckRbnEndNonRoll_jnt)
			mc.setAttr('%s.t' %self.neckRbnEndUpVec_grp,  crossVTrans[0], crossVTrans[1], crossVTrans[2])

			# self.neckRbnEndUpVec_grp.parent(self.neckRbnEndNonRoll_jnt)

			# constraint twist grps
			mc.delete( pc.aimConstraint(self.neckRbnEndNonRoll_jnt ,
										self.neckRbnRootTwst_jnt ,
										aim = self.neckRbn.aimVec , u = (crossV.x, crossV.y, crossV.z) ,
										wut = 'object' ,
										wuo = self.neckRbnRootUpVec_grp 
										)
					)
			self.neckRbnRootTwst_jnt.freeze()
			self.neckRbnRootTwst_aimCons = pc.aimConstraint(self.neckRbnEndNonRoll_jnt ,
										self.neckRbnRootTwst_jnt ,
										aim = self.neckRbn.aimVec , u = (crossV.x, crossV.y, crossV.z) ,
										wut = 'object' ,
										wuo = self.neckRbnRootUpVec_grp 
										)

			rbnAimInv = [self.neckRbn.aimVec[0]*-1, self.neckRbn.aimVec[1]*-1, self.neckRbn.aimVec[2]*-1]
			mc.delete( pc.aimConstraint(self.neckRbnRootNonRoll_jnt ,
										self.neckRbnEndTwst_jnt ,
										aim = rbnAimInv , u = (crossV.x, crossV.y, crossV.z) ,
										wut = 'object' ,
										wuo = self.neckRbnEndUpVec_grp 
										)
					)
			self.neckRbnEndTwst_jnt.freeze()
			self.neckRbnEndTwst_aimCons = pc.aimConstraint(self.neckRbnRootNonRoll_jnt ,
										self.neckRbnEndTwst_jnt ,
										aim = rbnAimInv , u = (crossV.x, crossV.y, crossV.z) ,
										wut = 'object' ,
										wuo = self.neckRbnEndUpVec_grp 
										)

			self.neckRbnEndTwst_pntCons = pc.pointConstraint(self.neckRbnEndNonRoll_jnt, self.neckRbnEndTwst_jnt)

			# connect twist grp to mul
			self.neckRbnRootTwst_jnt.attr(self.neckRbn.twstAx) >> self.neckRbn.rbnRootTwstAmp_mul.attr('i1')
			self.neckRbnEndTwst_jnt.attr(self.neckRbn.twstAx) >> self.neckRbn.rbnEndTwstAmp_mul.attr('i1')	

			# Ribbon upper arm - twist distributetion
			neckRbnShp = pc.Dag( self.neckRbn.rbn_ctrl.shape )
			neckRbnShp.attr('rootTwistAmp').value 	= 1
			neckRbnShp.attr('endTwistAmp').value 	= 1
			# neckRbnShp.attr('upVector').value = (1 , 0 , 0 )
			self.neckRbn.rbn_ctrl.attr('autoTwist').value = 1

			###
			self.neckRbn.rbnAnim_grp.parent( self.neckRig_grp )
			self.neckRbn.rbnSkin_grp.parent( skinGrp )
			self.neckRbn.rbnJnt_grp.parent( jntGrp )
			if ribbon == True:
				self.neckRbn.rbnStill_grp.parent( stillGrp )
			
			# Ribbon - cleanup
			for attr in ('tx','ty','tz') :
				self.neckRbn.rbnRoot_ctrl.attr( attr ).lockHide()
				self.neckRbn.rbnEnd_ctrl.attr( attr ).lockHide()
			
			self.neckRbn.rbnRoot_ctrl.hide()
			self.neckRbn.rbnEnd_ctrl.hide()
			# self.neckRbn.rbn_ctrl.attr('autoTwist').value = 1
			
		# Rig cleanup
		self.neck1Fk_ctrl.attr('localWorld').value = 0
				
		rigTools.lockUnusedAttrs( self )
		# self.neck1Fk_ctrl.lockHideAttrs( 'sx' , 'sy' , 'sz' , 'v' )
		for attr in ('sx','sy','sz','v') :
			self.neck1Fk_ctrl.attr( attr ).lockHide()

			