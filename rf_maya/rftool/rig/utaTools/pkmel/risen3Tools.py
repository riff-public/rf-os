import maya.cmds as mc
import maya.mel as mm
import pkmel.core as pc
import os

def selectBoneJoint() :

	jnts = mc.ls( 'BONE_*' , type='joint' ) + mc.ls( 'Slot_*' , type='joint' )
	skinnedJnts = []

	for jnt in jnts :
		if not '_END' in jnt :
			skinnedJnts.append( jnt )

	mc.select( skinnedJnts , r=True )

def bindToBone() :
	
	sels = mc.ls( sl=True )
	
	jnts = mc.ls( 'BONE_*' , type='joint' ) + mc.ls( 'Slot_*' , type='joint' )
	skinnedJnts = []
	
	for jnt in jnts :
		if not '_END' in jnt :
			skinnedJnts.append( jnt )
	
	for sel in sels :
		
		mc.skinCluster( skinnedJnts , sel , tsb=True )

def isInOffset( x=0 , y=0 , offset=0 ) :

	xMin = x - offset
	xMax = x + offset

	if xMin < y < xMax :
	    return True
	else :
	    return False

def findMirroredVertex() :
	offset = 1
	vtcs = mc.ls( sl=True , fl=True )
	mirroredVtcs = []
	geo = vtcs[0].split( '.' )[0]
	vtxNo = mc.polyEvaluate( geo , v = True )

	for vtx in vtcs :

		vtxPos = mc.xform( vtx , q=True , t=True , ws=True )
        
		for ix in range( 0 , vtxNo ) :
			currVtxName = '%s.vtx[%d]' % ( geo , ix )
			currVtxPos = mc.xform( currVtxName , q=True , t=True , ws=True )

			inX = isInOffset( ( vtxPos[0] * -1 ) , currVtxPos[0] , offset )
			inY = isInOffset( vtxPos[1] , currVtxPos[1] , offset )
			inZ = isInOffset( vtxPos[2] , currVtxPos[2] , offset )

			if inX and inY and inZ :
				mirroredVtcs.append( currVtxName )
    
	return mirroredVtcs

def renameEndJoint() :
	
	sels = mc.ls( sl=True )
	
	for sel in sels :
		
		parent = mc.listRelatives( sel , p=True )[0]
		mc.rename( sel , '%s_END' % parent )

def placementBaker() :
	# Set time range,
	# select placement controller, root controller,
	# controller that need to be baked then run the script
	
	# Get informations
	start = mc.playbackOptions( q=True , min=True )
	end = mc.playbackOptions( q=True , max=True )
	mc.currentTime( start )
	sels = mc.ls( sl=True )
	
	# Placement controller, root controller
	# and controllers that need to be baked.
	plc = sels[0]
	root = sels[1]
	ctrls = sels[2:]
	
	# Create template object for placement position.
	plcPos = mc.group( em=True )
	mc.delete( mc.pointConstraint( plc , plcPos ) )
	mc.pointConstraint( root , plcPos , skip='y' , mo=True )
	
	# Create template object for root position.
	rootPos = mc.group( em=True )
	mc.pointConstraint( root , rootPos )
	
	# Create template object for controller position.
	ctrlPoss = []
	for ctrl in ctrls :
		currPos = mc.group( em=True )
		mc.pointConstraint( ctrl , currPos )
		ctrlPoss.append( currPos )
	
	# Bake every constrained template object to animation key frames.
	nulls = [ plcPos , rootPos ]
	nulls.extend( ctrlPoss )
	
	mc.bakeResults( nulls , t = (start,end) )
	mc.currentTime( start )
	
	# Remove every keyframes from selected controllers
	attrs = ( 'tx' , 'ty' , 'tz' )
	for sel in sels :
		for attr in attrs :
			mc.cutKey ( sel , cl = True , at = attr )
	
	# Constrain controller to template object
	mc.pointConstraint( plcPos , plc )
	mc.pointConstraint( rootPos , root )
	
	if ctrls :
		for ix in range( len( ctrls ) ) :
			mc.pointConstraint( ctrlPoss[ix] , ctrls[ix] )
	
	# Bake every controller to animation key frames.
	mc.bakeResults( sels , t = (start,end) )
	mc.currentTime( start )
	
	mc.delete( nulls )

def tmpSknCtrl() :
	
	jnts = mc.ls( sl=True )
	
	for jnt in jnts :
		pars = mc.listRelatives( jnt , p=True )
		loc = mc.spaceLocator()[0]
		grp = mc.group( loc )
		
		mc.delete( mc.parentConstraint( jnt , grp ) )
		mc.parentConstraint( loc , jnt )
		
		if pars :
			mc.parentConstraint( pars[0] , grp , mo=True )
		
		mc.rename( loc , '%s_tmpCtrl' % jnt )
		mc.rename( grp , '%s_tmpCtrl_grp' % jnt )
		
		mc.select( '%s_tmpCtrl' % jnt , r=True )

def rearrangeHierarchy() :
	
	# importAllReferences()
	# removeNamespaces()
	
	rig = 'rig_grp'
	top = 'TOPCTRL'
	
	plc = 'placement_ctrl'
	mvr = 'CTRL_Mover_World'
	
	still = 'still_grp'
	traj = 'TRAJECTORY'
	
	trajCon = pc.Control( 'sphere' )
	trajCon.name = 'CTRL_Trajectory'
	trajCon.color = 'red'
	trajCon.lockHideAttrs( 'sx' , 'sy' , 'sz' , 'v' )
	
	if mc.objExists( still ) :
		mc.parent( still , plc )
		mc.setAttr( '%s.inheritsTransform' % still , 0 )
	
	mc.rename( rig , top )
	mc.rename( plc , mvr )
	mc.parentConstraint( trajCon , traj )
	
	mc.parent( trajCon , top )
	
	convertName()

def connectToBone() :
	# Connect rig joints to bone joints
	bones = mc.ls( '*:BONE_*' , type='joint')
	
	for bone in bones :
		
		print 'Connecting rig joint to %s' % bone

		try :
			jnt = mc.ls( '*:%s' % bone.split( ':' )[1].replace( 'BONE' , 'RIGJNT' ) )[0]
		
			mc.parentConstraint( jnt , bone , mo=True )
		except :
			pass

def disableScaleAttr() :
	# Lock scale and squash attributes
	ctrls = mc.ls( '*_ctrl' )
	
	for ctrl in ctrls :
		
		attrs = mc.listAttr( ctrl , k=True )
		
		if attrs :
		
			for attr in attrs :
				
				sclAttrs = ( 'scaleX' , 'scaleY' , 'scaleZ' , 'squash' , 'autoSquash' , 'stretch' )
				
				if attr in sclAttrs :
					
					mc.setAttr( '%s.%s' % ( ctrl , attr ) , l=True )

def convertName() :
	# Translate current naming format to risen3 format
	for node in mc.ls() :
		
		if '_' in node :
			
			if ( len( node.split( '_' ) ) == 2 ) and node[0].islower() :
				
				sides = ( 'LFT_' , 'RGT_' )
				currSide = '_'
				
				for side in sides :
					if side in node :
						currSide = side
				
				elem , nodeType = node.split( currSide )
				nodeType = nodeType.upper()
				elem = '%s%s' % ( elem[0].upper() , elem[1:] )
				
				noStr = ''
				
				for each in elem :
					if each.isdigit() :
						noStr = noStr + each
				
				elem = elem.replace( noStr , '' )
				
				if currSide == '_' :
					currSide = ''
				else :
					currSide = '_%s' % currSide.title()
				
				if noStr :
					noInt = int( noStr )
					if not currSide :
						currSide = '_'
					currSide = '%s%02d' % ( currSide , noInt )
				else :
					currSide = currSide[:-1]
				
				currName = '%s_%s%s' % ( nodeType , elem , currSide )
				print 'from %s to %s' % ( node , currName )
				
				try :
					mc.rename( node , currName )
				except RuntimeError :
					pass

def jointCounter() :
	# Count the node that has prefix "BONE_"
	jnts = mc.ls( 'BONE_*' , type='joint' ) + mc.ls( 'Slot_*' , type='joint' )
	skinnedJnts = []
	endJnts = []
	
	for jnt in jnts :
		if jnt.endswith( '_END' ) :
			endJnts.append( jnt )
		else :
			skinnedJnts.append( jnt )
			if 'Left' in jnt :
				rgtJnt = jnt.replace( 'Left' , 'Right' )
				if not mc.objExists( rgtJnt ) :
					skinnedJnts.append( rgtJnt )

	msg = '%s joints\n' % len( jnts )
	msg += '%s skinned joints\n' % len( skinnedJnts )
	msg += '%s end joints\n' % len( endJnts )
	
	mc.confirmDialog(
						title='Confirm' ,
						message=msg
					)

def findIllegalVtcs() :
	# Script for finding the vertex that has more than 4 influences.
	# Select geometry then run script.
	sel = mc.ls( sl=True )[0]
	skc = mm.eval( 'findRelatedSkinCluster %s' % sel )
	infs = mc.skinCluster( skc , q=True , inf=True )
	vtcDct = {}
	
	for ix in range( mc.polyEvaluate( sel , v=True ) ) :
		
		vtx = '%s.vtx[%s]' % ( sel , ix )
		sknVals = mc.skinPercent( skc , vtx , q=True , v=True )
		infDct = {}
		
		for iy in range( len( sknVals ) ) :
			if sknVals[iy] :
				infDct[ infs[iy] ] = sknVals[iy]
		
		if len( infDct.keys() ) > 4 :
			vtcDct[vtx] = infDct
	
	if vtcDct.keys() :
		
		for vtc in vtcDct.keys() :
			
			print '===================='
			print '\n%s has %s influences\n' % ( vtc , len( vtcDct[vtc] ) )
			
			for jnt in vtcDct[vtc].keys() :
				print '%s : %s ' % ( jnt , vtcDct[vtc][jnt] )
		
		mc.select( vtcDct.keys() , r=True )

def incrementalSave() :
	
	fp = mc.file( q=True , sn=True )
	sn = fp.split( '/' )[ -1 ]
	wd = fp.replace( sn , '' )
	
	elemName , cnt = sn.split( '_' )
	cnt = cnt.split( '.' )[0]
	currCnt = int( cnt ) + 1
	
	incrFl = '%s%s_%s' % ( wd , elemName , currCnt )
	
	mc.file( rename=incrFl )
	print 'Saving to %s' % incrFl
	mc.file( save=True , type='mayaAscii')

def findParentNode( node='' ) :
	# Find the node that given node has been constrained to.
	constrs = []
	parConstrs = mc.listConnections( node , s=True , type='parentConstraint' )
	scaConstrs = mc.listConnections( node , s=True , type='scaleConstraint' )
	
	if parConstrs :
		for each in parConstrs :
			constrs.append( each )
	
	if scaConstrs :
		for each in scaConstrs :
			constrs.append( each )
	
	if constrs :
		parent = mc.listConnections( '%s.target[0].targetTranslate' % constrs[0] , s=True )[0]
		mc.delete( constrs )
		return parent
	else :
		return None

def publishElement() :
	importRigElement()
	fp = mc.file( q=True , sn=True )
	sn = fp.split( '/' )[ -1 ]
	wd = fp.replace( sn , '' )
	
	elemName = sn.split( '_' )[0]
	
	# Get all published file
	pubFiles = []
	for each in os.listdir( wd ) :
		if '%s_pub' % elemName in each :
			pubFiles.append( each )
	
	latestPubNo = 0
	if pubFiles :
		
		pubFiles.sort()
		latestPubNo = int( pubFiles[-1].split( '_' )[-1].split( '.' )[0] )
	
	hero = '%s%s_pub_%s' % ( wd , elemName , str( latestPubNo + 1 ) )
	
	mc.file( rename=hero )
	print 'Saving to %s' % hero
	mc.file( save=True , type='mayaAscii' )