from nuTools.rigTools import proc
reload(proc)
def main() :
# Neck 
	proc.btwJnt(jnts=['spine5Sca_jnt', 'neck1_jnt', 'neckRbn_jnt'], pointConstraint=True)

# Arm LFT
	proc.btwJnt(jnts=['spine5Sca_jnt', 'clav1LFT_jnt', 'upArmLFT_jnt'], pointConstraint=True)
	proc.btwJnt(jnts=['clav1LFT_jnt', 'upArmLFT_jnt', 'upArmRbnDtl1LFT_jnt'], pointConstraint=True)
	proc.btwJnt(jnts=['upArmRbnDtl5LFT_jnt', 'forearmLFT_jnt', 'forearmRbnDtl1LFT_jnt'], pointConstraint=False)
	proc.btwJnt(jnts=['forearmRbnDtl5LFT_jnt', 'wristLFT_jnt', 'handLFT_jnt'], pointConstraint=True)

# Arm RGT
	proc.btwJnt(jnts=['spine5Sca_jnt', 'clav1RGT_jnt', 'upArmRGT_jnt'], pointConstraint=True)
	proc.btwJnt(jnts=['clav1RGT_jnt', 'upArmRGT_jnt', 'upArmRbnDtl1RGT_jnt'], pointConstraint=True)
	proc.btwJnt(jnts=['upArmRbnDtl5RGT_jnt', 'forearmRGT_jnt', 'forearmRbnDtl1RGT_jnt'], pointConstraint=False)
	proc.btwJnt(jnts=['forearmRbnDtl5RGT_jnt', 'wristRGT_jnt', 'handRGT_jnt'], pointConstraint=True)

# Leg LFT
	proc.btwJnt(jnts=['spine1Sca_jnt', 'upLegLFT_jnt', 'upLegRbnDtl1LFT_jnt'], pointConstraint=True)
	proc.btwJnt(jnts=['upLegRbnDtl5LFT_jnt', 'lowLegLFT_jnt', 'lowLegRbnDtl1LFT_jnt'], pointConstraint=False)
	proc.btwJnt(jnts=['lowLegRbnDtl5LFT_jnt', 'ankleLFT_jnt', 'ballLFT_jnt'], pointConstraint=True)

# Leg RGT
	proc.btwJnt(jnts=['spine1Sca_jnt', 'upLegRGT_jnt', 'upLegRbnDtl1RGT_jnt'], pointConstraint=True)
	proc.btwJnt(jnts=['upLegRbnDtl5RGT_jnt', 'lowLegRGT_jnt', 'lowLegRbnDtl1RGT_jnt'], pointConstraint=False)
	proc.btwJnt(jnts=['lowLegRbnDtl5RGT_jnt', 'ankleRGT_jnt', 'ballRGT_jnt'], pointConstraint=True)
