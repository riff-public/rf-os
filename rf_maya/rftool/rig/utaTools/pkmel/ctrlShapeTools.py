import maya.cmds as mc
import os, sys
import pickle
import core as pc
import maya.mel as mm

from utaTools.utapy import copyObjFile 
reload(copyObjFile)

from rf_utils.context import context_info
from rf_utils import file_utils

#path project config
rootScript = os.environ['RFSCRIPT']
pathProjectJson = r"{}\core\rf_maya\rftool\rig\export_config\project_config.json".format(rootScript)
projectData = file_utils.json_loader(pathProjectJson)


def writeAllCtrl() :
	# Project Name 
	asset = context_info.ContextPathInfo()
	projectName = asset.project  

	if projectName in projectData['project']:
		dataFld = copyObjFile.getDataFld()
		if not os.path.isdir( dataFld ) :
	   		 os.mkdir( dataFld )
		
		ctrls = mc.ls( "*trl" )
		fn = '%s/ctrlShape.txt' % dataFld

		writeCtrlShape( ctrls , fn )

		print "# Generate >> Path " , fn
		print '# Generate >> Exporting all control shape is done.'

	else:
		dataFld = pc.getDataFld()
		if not os.path.isdir( dataFld ) :
	   		 os.mkdir( dataFld )
		
		ctrls = mc.ls( "*trl" )
		fn = '%s/ctrlShape.txt' % dataFld

		print "# Generate >> Path " , fn

		writeCtrlShape( ctrls , fn )
		
		print '# Generate >> Exporting all control shape is done.'

def readAllCtrl( search = '' , replace = '' , dataFld = '') :

	# Project Name
	# if dataFld:
	# 	ctrls = mc.ls( "*trl" , '*:*trl')
	# 	fn = r'%s/ctrlShape.txt' % dataFld
	# 	print '# Generate >> From' , fn
	# 	readCtrlShape( ctrls , fn , search = search , replace = replace )
	# 	print '# Generate >> Importing all control shape is done.'

	if not dataFld:
		driveCheck = (mm.eval('file -q-loc;')).split(':')[0]
		if driveCheck == 'P':
			asset = context_info.ContextPathInfo()
			projectName = asset.project
		else:
			projectName = ''

		if projectName in projectData['project']:
			dataFld = copyObjFile.getDataFld()
		else:
			dataFld = pc.getDataFld()

		if not os.path.isdir( dataFld ) :
	   		 os.mkdir( dataFld )

	ctrls = mc.ls( "*trl" , '*:*trl')
	fn = r'%s/ctrlShape.txt' % dataFld
	print '# Generate >> From' , fn
	readCtrlShape( ctrls , fn , search = search , replace = replace )
	print '# Generate >> Importing all control shape is done.'

def writeCtrlShape( ctrls = [] , fn = '' ) :
	
	fid = open( fn , 'w' )
	ctrlDct = {}
	
	for ctrl in ctrls :
		
		shapes = mc.listRelatives( ctrl , s = True )
		
		if type( shapes ) == type( [] ) and mc.nodeType( shapes[0] ) == 'nurbsCurve' :
			
			cv = mc.getAttr( '%s.spans' % shapes[0] ) + mc.getAttr( '%s.degree' % shapes[0] )
		
			for ix in range( 0 , cv ) :
				
				cvName = '%s.cv[%s]' % ( shapes[0] , str( ix ) )
				ctrlDct[ cvName ] = mc.xform( cvName , q = True , os = True , t = True )

			# Write color property
			if mc.getAttr('%s.overrideEnabled' % shapes[0]):
				colVal = mc.getAttr('%s.overrideColor' % shapes[0])
				ctrlDct[shapes[0]] = colVal
			
	pickle.dump( ctrlDct , fid )
	fid.close()

def readCtrlShape( ctrls = [] , fn = '' , search = '' , replace = '' ) :
	# print fn
	fid = open( fn , 'r' )
	ctrlDct = pickle.load( fid )
	fid.close()
	
	for key in ctrlDct.keys() :
		# print key
		if search :
			currVtx = key.replace( search , replace )
		else :
			currVtx = key
				
		if '.' in currVtx:
			if mc.objExists( currVtx ):
				mc.xform( currVtx , os = True , t = ctrlDct[ currVtx ] )
		else:
			if mc.objExists( currVtx ):
				mc.setAttr( '%s.overrideEnabled' % currVtx , 1 )
				mc.setAttr( '%s.overrideColor' % currVtx , ctrlDct[ currVtx ] )