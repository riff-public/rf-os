import maya.cmds as mc
import maya.mel as mm
  
import core as pc

import rigTools
import os
import shutil


def weaponRig2(jnts=['Rig:wristLFT_jnt', 'Rig:wristRGT_jnt']) :
	# Rigging weapons
	sels = mc.ls( sl=True )
	
	lHandLoc, rHandLoc = createWeaponLocator2(jnts)
	
	for sel in sels :
		
		currName = sel.split( '_' )[0]
		currName2 = currName.split ('Geo') [0]
		# print currName2
		
		ctrl = rigTools.jointControl( 'sphere' )
		ctrl.name = '%s_ctrl' % currName2
		ctrl.color = 'yellow'

		ctrl.add( ln='geoVis' , min=0 , max=1 , dv=1 , k=True )
		ctrl.attr( 'v' ).lock = True
		ctrl.attr( 'v' ).hide = True

		mc.connectAttr( '%s.geoVis' % ctrl , '%s.v' % sel )
		
		ctrlGrp = pc.group( ctrl )
		ctrlGrp.name = '%sCtrlZero_grp' % currName2
		
		# mc.skinCluster( ctrl , sel , tsb=True )
		mc.parentConstraint( ctrl , sel , mo=True )
		mc.scaleConstraint ( ctrl , sel , mo=True ) 
		
		lftGrp , rgtGrp = parentLeftRight( ctrl , lHandLoc.name , rHandLoc.name , ctrlGrp )
		lftGrp.name = '%sCtrlLeft_grp' % currName2
		rgtGrp.name = '%sCtrlRgt_grp' % currName2

		print currName2
		
def createWeaponLocator2(jnts) :
	# Creating location for mounting weapons at left hand and right hand
	lHandJnt = jnts[0]
	rHandJnt = jnts[1]
	
	lHand = pc.Locator()
	rHand = pc.Locator()
	
	poses = []
	for i in [lHandJnt,rHandJnt]:
		position = ''
		nsSplit = i.split(':')[-1]
		revElement = nsSplit.split('_')[0][::-1]

		for a in range(len(revElement)):
			if revElement[a].isupper():
				position += revElement[a]
			else:
				break
		poses.append(position[::-1])

	lHand.name = 'weapon%s_loc' %poses[0]
	rHand.name = 'weapon%s_loc' %poses[1]
	
	# lHand.attr( 't' ).v = ( 1.92 , 0 , 0.015 )
	# lHand.attr( 'rz' ).v = -15
	# rHand.attr( 't' ).v = ( -1.92 , 0 , 0.015 )
	# rHand.attr( 'rz' ).v = 15
	
	mc.delete( mc.pointConstraint( lHandJnt , lHand  ) )
	mc.delete( mc.pointConstraint( rHandJnt , rHand  ) )
	
	mc.parentConstraint( lHandJnt , lHand , mo=True )
	mc.scaleConstraint(lHandJnt , lHand , mo=True)
	mc.parentConstraint( rHandJnt , rHand , mo=True )
	mc.scaleConstraint(rHandJnt , rHand , mo=True)

	return lHand, rHand

	
def parentLeftRight( ctrl = '' , localObj = '' , worldObj = '' , parGrp = '' ) :
	# Blending parent between left hand and right hand.
	# Returns : locGrp , worGrp , worGrpParCons , parGrpParCons and parGrpParConsRev
	locGrp = pc.Null()
	worGrp = pc.Null()
	
	locGrp.snap( localObj )
	worGrp.snap( worldObj )
	
	worGrpParCons = pc.parentConstraint( worldObj , worGrp )
	worGrpParCons = pc.scaleConstraint( worldObj , worGrp )

	locGrpParCons = pc.parentConstraint( localObj , locGrp )
	locGrpParCons = pc.scaleConstraint( localObj , locGrp )

	parGrpParCons = pc.parentConstraint( locGrp , worGrp , parGrp )
	parGrpParCons2 = pc.scaleConstraint( locGrp , worGrp , parGrp , mo=True)
	parGrpParConsRev = pc.Reverse()
	
	con = pc.Dag( ctrl )
	
	attr = 'leftRight'
	con.add( ln = attr , k = True, min = 0 , max = 1 )
	con.attr( attr ) >> parGrpParCons.attr( 'w1' )
	con.attr( attr ) >> parGrpParConsRev.attr( 'ix' )
	parGrpParConsRev.attr( 'ox' ) >> parGrpParCons.attr( 'w0' )
	
	pc.clsl()
	
	return locGrp , worGrp	
