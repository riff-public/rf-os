import maya.cmds as mc
import pkmel.core as pc
import pkmel.rigTools as rigTools
import pkmel.ribbon as pr
reload( pc )
reload( rigTools )
reload( pr )

class LatticeRig( object ) :

	def __init__(
					self ,
					parent='' ,
					lattice='' ,
					animGrp='anim_grp' ,
					stillGrp='still_grp' ,
					name='' ,
					side=''
				) :

		self.rigGrp = pc.Null()
		self.rigGrp.name = '%sLatRig%s_grp' % ( name , side )
		if parent :
			mc.parentConstraint( parent , self.rigGrp )
			mc.scaleConstraint( parent , self.rigGrp )

		if animGrp :
			self.rigGrp.parent( animGrp )

		self.stillGrp = pc.Null()
		self.stillGrp.name = '%sLatStill%s_grp' % ( name , side )

		if stillGrp :
			self.stillGrp.parent( stillGrp )

		self.lat = pc.Dag( lattice )
		self.ffd = pc.Node( mc.listConnections( '%s.latticeOutput' % self.lat , s=False , d=True )[0] )
		self.latBs = pc.Dag( mc.listConnections( '%s.baseLatticeMatrix' % self.ffd , s=True , d=False )[0] )

		self.lat.name = '%s%s_lat' % ( name , side )
		self.ffd.name = '%s%s_ffd' % ( name , side )
		self.latBs.name = '%s%s_bsLat' % ( name , side )

		latShp = pc.Dag( self.lat.shape )
		latBsShp = pc.Dag( self.latBs.shape )

		latShp.attr('worldMatrix[0]') // self.ffd.attr('deformedLatticeMatrix')
		latShp.attr('latticeOutput') // self.ffd.attr('deformedLatticePoints')

		self.latGuide = pc.Dag( mc.duplicate( self.lat , rr=True )[0] )
		self.latGuide.name = '%sGuide%s_lat' % ( name , side )

		latShp.attr('worldMatrix[0]') >> self.ffd.attr('deformedLatticeMatrix')
		latShp.attr('latticeOutput') >> self.ffd.attr('deformedLatticePoints')

		sRes = latShp.attr('sDivisions').v
		tRes = latShp.attr('tDivisions').v
		uRes = latShp.attr('uDivisions').v

		ctrls = []
		ctrlZgrps = []
		ix = 1

		for s in range( sRes ) :
			for t in range( tRes ) :
				for u in range( uRes ) :

					latPt = '%s.pt[%s][%s][%s]' % ( self.lat , t , s , u )
					latGuidePt = '%s.pt[%s][%s][%s]' % ( self.latGuide , t , s , u )

					latPtPos = mc.xform( latPt , q=True , t=True , ws=True )

					ctrl = pc.Control('cube')
					ctrl.color = 'yellow'
					ctrl.lockHideAttrs( 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
					ctrl.name = '%sLat%s%s_ctrl' % ( name , str( ix ) , side )

					ctrlZgrp = pc.group( ctrl )
					ctrlZgrp.name = '%sLat%sCtrlZro%s_grp' % ( name , str( ix ) , side )

					ctrlZgrp.attr('t').v = latPtPos

					latCl = mc.cluster( latPt , wn = ( ctrl , ctrl ) )
					latClSet = mc.listConnections( '%s.message' % latCl[0] , d=True , s=False )[0]
					latGuideCl = mc.cluster( latGuidePt , wn = ( ctrl , ctrl ) )
					latGuideClSet = mc.listConnections( '%s.message' % latGuideCl[0] , d=True , s=False )[0]

					mc.setAttr( '%s.relative' % latCl[0] , 1 )
					mc.setAttr( '%s.relative' % latGuideCl[0] , 1 )

					mc.rename( latCl[0] , '%sLat%s%s_cl' % ( name , str( ix ) , side ) )
					mc.rename( latGuideCl[0] , '%sLatGuide%s%s_cl' % ( name , str( ix ) , side ) )

					ctrlZgrp.parent( self.rigGrp )

					ix += 1

		self.lat.parent( self.stillGrp )
		self.latBs.parent( self.stillGrp )
		self.latGuide.parent( self.stillGrp )
		if parent :
			mc.parentConstraint( parent , self.latGuide , mo=True )

		self.lat.attr('v').v = 0
		self.latBs.attr('v').v = 0
		self.latGuide.attr('overrideEnabled').v = 1
		self.latGuide.attr('overrideDisplayType').v = 2