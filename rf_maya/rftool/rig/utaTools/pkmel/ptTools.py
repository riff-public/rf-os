import maya.cmds as mc
import maya.mel as mm
import os
import shutil
import pickle
import subprocess
import re

import pkmel.core as pc
reload( pc )
import pkmel.rigTools as rigTools
reload( rigTools )

def placementSnapper():

	sel = mc.ls(sl=True)[0]
	ns = ''

	if ':' in sel:
		elms = sel.split(':')
		ns = '%s:' % ':'.join(elms[:-1])

	placement = '%sSuperRoot_Ctrl' % ns
	placementGrp = '%sSuperRoot_Grp' % ns

	root = '%sroot_ctrl' % ns

	placementLoc = mc.spaceLocator()[0]
	placementLocGrp = mc.group(placementLoc)

	mc.delete(mc.parentConstraint(placementGrp, placementLocGrp))
	# mc.delete(mc.pointConstraint(placement, placementLoc))
	mc.delete(mc.parentConstraint(placement, placementLoc))
	mc.delete(mc.pointConstraint(root, placementLoc, skip='y'))

	ctrlDict = {}
	ctrls = [
				'root_ctrl', 'pelvis_ctrl',
				'legIkLFT_ctrl', 'legIkRGT_ctrl',
				'kneeIkLFT_ctrl', 'kneeIkRGT_ctrl',
				'spine1Fk_ctrl', 'spine2Fk_ctrl',
				'neck1Fk_ctrl', 'head_ctrl',
				'eye_ctrl',
				'upArmFkLFT_ctrl', 'upArmFkRGT_ctrl'
			]

	for ctrl in ctrls:

		currCtrl = '%s%s' % (ns, ctrl)

		loc = mc.spaceLocator(n=currCtrl.replace(':', '_'))[0]
		mc.delete(mc.parentConstraint(currCtrl, loc))

		ctrlDict[ctrl] = loc

	trAttrs = ('tx', 'ty', 'tz', 'rx', 'ry', 'rz')

	for attr in trAttrs:
		placementAttr = '%s.%s' % (placement, attr)
		placementLocVal = mc.getAttr('%s.%s' % (placementLoc, attr))
		mc.setAttr(placementAttr, placementLocVal)

	for ctrl in ctrls:

		currCtrl = '%s%s' % (ns, ctrl)
		loc = ctrlDict[ctrl]

		tmpCtrl = mc.duplicate(currCtrl, rr=True)[0]

		for attr in trAttrs:
			mc.setAttr('%s.%s' % (tmpCtrl, attr), l=False)
			mc.setAttr('%s.%s' % (tmpCtrl, attr), k=True)
		
		tmpCtrlChn = mc.listRelatives(tmpCtrl, type='transform', ad=True, f=True)
		if tmpCtrlChn:
			for tmpCtrlCh in tmpCtrlChn:
				if mc.objExists(tmpCtrlCh):
					mc.delete(tmpCtrlCh)

		mc.delete(mc.parentConstraint(loc, tmpCtrl))
		for attr in trAttrs:
			ctrlAttr = '%s.%s' % (currCtrl, attr)
			tmpVal = mc.getAttr('%s.%s' % (tmpCtrl, attr))
			try:
				mc.setAttr(ctrlAttr, tmpVal)
			except:
				pass

		mc.delete(tmpCtrl)
		mc.delete(loc)

	mc.delete(placementLocGrp)

	mc.select(sel, r=True)

def removeNeckRbnCtrlFromPoseLibData(flPath=''):

	fldPath, flNmExt = os.path.split(flPath)
	flNm, flExt = os.path.splitext(flNmExt)

	tmpFlPath = '%s.tmp' % flPath
	kw = '<Control name="neckRbn_ctrl"'
	exitKw = '</Control>'
	found = False

	flObj = open(flPath, 'r')
	tmpFlObj = open(tmpFlPath, 'w')

	for line in flObj:
		if kw in line:
			found = True

		if not found:
			tmpFlObj.write(line)

		if (found == True) and (exitKw in line):
			found = False

	flObj.close()
	tmpFlObj.close()
	
	os.remove(flPath)
	os.rename(tmpFlPath, flPath)

def compareAsset(assetPaths):

	# ptTools.compareAsset(
	# 					ptTools.getAnimAsset(
	# 											ptTools.getAllAssetInType('Lego_Friends2015', 'Friends_12', 'vehicle')
	# 										)
	# 				)
	missingGeos = []
	currWidth = 0

	for assetPath in assetPaths:
		fldPath, flNmExt = os.path.split(assetPath)
		flNm, flExt = os.path.splitext(flNmExt)
		refFlPath = mc.file(assetPath, r=True, ns=flNm, pmt=False)
		rfn = mc.file(refFlPath, q=True, rfn=True)

		# frd11_horse_Anim:SuperRoot_Grp|frd11_horse_Anim:SuperRoot_Ctrl
		ns = rfn.replace('RN', '')
		geoGrpNm = '%s:Geo_Grp' % ns
		# root = '%s:SuperRoot_Ctrl' % ns
		root = '%s:Rig_Grp|%s:SuperRoot_Grp|%s:SuperRoot_Ctrl' % (ns, ns, ns)
		bbox = [0, 0, 0, 0, 0, 0]

		geoGrps = mc.ls( '*%s*' % geoGrpNm, l=True)
		if geoGrps:
			geoGrp = geoGrps[0]
			bbox = mc.exactWorldBoundingBox(geoGrp, ii=True)
		else:
			missingGeos.append(assetPath)
		width = abs(bbox[0]-bbox[3])

		if not assetPath == assetPaths[0]:
			currWidth += width/2

		charName = flNm.replace('_Anim', '')
		txt = mc.textCurves(
								ch=False ,
								f='Tahoma|w400|h-3' ,
								t=charName
							)[0]
		mc.select(txt, r=True)
		mm.eval('CenterPivot')
		mc.delete(mc.pointConstraint(root, txt))
		currTy = mc.getAttr('%s.ty' % txt)
		mc.setAttr('%s.ty' % txt, (currTy-1))

		mc.parentConstraint(root, txt, mo=True)
		
		mc.setAttr('%s.tx' % root, currWidth)
		currWidth += width/2

	if missingGeos:
		for missingGeo in missingGeos:
			print "%s doesn't have Geo_Grp" % missingGeo

def getAnimAsset(assetDict):
	assetPaths = []
	for asset in assetDict:
		fldPath = os.path.join(
									'P:\\',
									asset['proj'],
									'asset',
									'3D',
									asset['sg_asset_type'],
									asset['sg_subtype'],
									asset['code'],
									'ref'
								)
		fls = [f for f in os.listdir(fldPath) if os.path.isfile(os.path.join(fldPath, f))]
		rigAnimPath = ''
		for fl in fls:
			if '_Anim' in fl:
				rigAnimPath = os.path.join(fldPath, fl)
				break

		if rigAnimPath:
			assetPaths.append(rigAnimPath)

	return assetPaths

def getAllAssetInType(projNm, ep, assetType, assetSubType):

	from shotgun_api3 import Shotgun
	server = 'https://pts.shotgunstudio.com'
	script = 'ptTools'
	_id = 'ec0b3324c1ab54cf4e21c68d18d70f2f347c3cbd'
	sg = Shotgun(server, script, _id)

	fields = ['code', 'sg_asset_type', 'sg_subtype', 'scenes']
	filters = [['project.Project.name', 'is', projNm]]

	typeAssets = []

	for asset in sg.find('Asset',filters, fields):
		if not ep:
			assetDict = {
							'code': asset['code'],
							'proj': projNm,
							'sg_asset_type': asset['sg_asset_type'],
							'sg_subtype': asset['sg_subtype']
						}
			typeAssets.append(assetDict)

		else:
			currEpDict = asset['scenes']
			if currEpDict:
				if (currEpDict[0]['name'] == ep) and (asset['sg_asset_type'] == assetType) and (asset['sg_subtype'] == assetSubType) :
					assetDict = {
									'code': asset['code'],
									'proj': projNm,
									'sg_asset_type': asset['sg_asset_type'],
									'sg_subtype': asset['sg_subtype']
								}
					typeAssets.append(assetDict)
		
	return typeAssets

def createFileTextureNode():
	flNode = mc.shadingNode('file', asTexture=True)
	p2dNode = mc.shadingNode('place2dTexture', asUtility=True)
	
	mc.connectAttr('%s.coverage' % p2dNode, '%s.coverage' % flNode)
	mc.connectAttr('%s.translateFrame' % p2dNode, '%s.translateFrame' % flNode)
	mc.connectAttr('%s.rotateFrame' % p2dNode, '%s.rotateFrame' % flNode)
	mc.connectAttr('%s.mirrorU' % p2dNode, '%s.mirrorU' % flNode)
	mc.connectAttr('%s.mirrorV' % p2dNode, '%s.mirrorV' % flNode)
	mc.connectAttr('%s.stagger' % p2dNode, '%s.stagger' % flNode)
	mc.connectAttr('%s.wrapU' % p2dNode, '%s.wrapU' % flNode)
	mc.connectAttr('%s.wrapV' % p2dNode, '%s.wrapV' % flNode)
	mc.connectAttr('%s.repeatUV' % p2dNode, '%s.repeatUV' % flNode)
	mc.connectAttr('%s.offset' % p2dNode, '%s.offset' % flNode)
	mc.connectAttr('%s.rotateUV' % p2dNode, '%s.rotateUV' % flNode)
	mc.connectAttr('%s.noiseUV' % p2dNode, '%s.noiseUV' % flNode)
	mc.connectAttr('%s.vertexUvOne' % p2dNode, '%s.vertexUvOne' % flNode)
	mc.connectAttr('%s.vertexUvTwo' % p2dNode, '%s.vertexUvTwo' % flNode)
	mc.connectAttr('%s.vertexUvThree' % p2dNode, '%s.vertexUvThree' % flNode)
	mc.connectAttr('%s.vertexCameraOne' % p2dNode, '%s.vertexCameraOne' % flNode)
	mc.connectAttr('%s.outUV' % p2dNode, '%s.uv' % flNode)
	mc.connectAttr('%s.outUvFilterSize' % p2dNode, '%s.uvFilterSize' % flNode)

	return flNode, p2dNode

def mvSetCard(prefix='', frame=0, scale=0, posNode='', cam='', imgPrefix='', offFrame=0, lifeFrame=0):

	imgExt= '.tif'
	imgPath = '%s.0%s%s' % (imgPrefix, frame, imgExt)

	pos = pc.Control('plus')
	pos.color = 'red'
	pos.name = '%s%sCardPos_ctrl' % (prefix, frame)
	pos.scaleShape(0.1)

	rot = pc.Control('circle')
	rot.color = 'blue'
	rot.name = '%s%sCardRot_ctrl' % (prefix, frame)
	rot.parent(pos)
	rot.scaleShape(0.1)

	plane = '%s%sCard_ply' % (prefix, frame)
	mc.polyPlane(n=plane, sx=1, sy=1,)
	mc.parent(plane, rot)
	mc.setAttr('%s.tz' % plane, -0.5)

	mc.setAttr('%s.s' % pos, scale, scale, scale)

	mc.currentTime(frame)
	pos.snap(posNode)
	
	rot.attr('rx').v = 90

	if cam:

		camPos = pc.Null()
		camPos.snapPoint(cam)
		camPos.attr('ty').v = pos.attr('ty').v
		
		mc.delete(
					mc.aimConstraint(
										camPos,
										pos,
										aimVector=(0, 0, 1),
										upVector=(0, 1, 0),
										worldUpType='scene'
									)
					)

		mc.delete(camPos)

	# Shader
	flNode, p2dNode = createFileTextureNode()
	flNode = mc.rename(flNode, '%s%sCard_file' % (prefix, frame))
	p2dNode = mc.rename(p2dNode, '%s%sCard_place2dTexture' % (prefix, frame))
	mc.setAttr('%s.fileTextureName' % flNode, imgPath, type='string')

	lambert = mc.shadingNode('lambert', asShader=True)
	lambert = mc.rename(lambert, '%s%sCard_lambert' % (prefix, frame))

	mc.connectAttr('%s.outColor' % flNode, '%s.color' % lambert )
	mc.connectAttr('%s.outTransparency' % flNode, '%s.transparency' % lambert )

	mc.select(plane, r=True)
	mc.hyperShade( assign=lambert )

	# mc.setAttr('%s.rx' % rot, 90)
	mc.setKeyframe(rot, attribute='v', v=0, t=frame-1)
	mc.setKeyframe(rot, attribute='v', v=1, t=frame)
	if lifeFrame:
		mc.setKeyframe(rot, attribute='v', v=1, t=frame-1+lifeFrame)
		mc.setKeyframe(rot, attribute='v', v=0, t=frame+lifeFrame)

	if offFrame:
		mc.setKeyframe(rot, attribute='v', v=0, t=frame+offFrame)

	# Delete key in shapes controller
	rotShp = mc.listRelatives( rot )[0]
	mc.cutKey( rotShp , attribute='v' )
	mc.setAttr( '%s.v' % rotShp , 1 )
	
	return pos, rot, plane

def removeBlendedKeyInRibbonControl() :

	ctrls = []

	print 'Searching ribbon end/root controllers.'
	for each in mc.ls(type='transform',l=True) :
		exp = r'(.*Rbn)(Root|End)(.*_ctrl$)'
		m = re.search( exp , each )
		if m :
			if not m.group(0) in ctrls :
				ctrls.append( each )

	if ctrls :

		for ctrl in ctrls :

			cons = mc.listConnections( ctrl , s=True , d=False , type='pairBlend' )

			if cons :

				pb = cons[0]
				aCrvs = mc.listConnections( pb , s=True , d=False , type='animCurveTL' )

				if aCrvs :
					for aCrv in aCrvs :
						print '%s has been removed.' % aCrv
						mc.delete( aCrv )
	else :
		print 'Ribbon controllers have not been found.'

def frdSetAllTextureRes( res='' ) :

	for fn in mc.ls( type='file' ) :
		frdChangeTextureRes( fn , res )

def frdChangeTextureRes( fn='' , res='' ) :

	txtPath = os.path.normpath( mc.getAttr( '%s.fileTextureName' % fn ) )
	txtFldPath , txtNmExt = os.path.split( txtPath )
	txtParentPath , txtFld = os.path.split( txtFldPath )
	txtNm , txtExt = os.path.splitext( txtNmExt )

	currRes = None
	if not txtFld == 'textures' :
		currRes = txtFld

	currTxtFldPath = ''
	if txtFld == 'textures' :
		currTxtFldPath = os.path.join( txtFldPath , res )
	else :
		if currRes :
			currTxtFldPath = os.path.join( txtParentPath , res )

	currTxtNm = txtNm
	if currRes :
		if res :
			currTxtNm = currTxtNm.replace( currRes , res )
		else :
			currTxtNm = currTxtNm.replace( '_%s' % currRes , '' )
	else :
		if res :
			currTxtNm = '%s_%s' % ( currTxtNm , res )

	currTxtPath = os.path.join( currTxtFldPath , '%s%s' % ( currTxtNm , txtExt ) )

	if os.path.exists( currTxtPath ) :

		print 'Set texture path in %s from %s to %s' % ( fn , txtPath , currTxtPath )
		mc.setAttr( '%s.fileTextureName' % fn , currTxtPath , type='string' )

	else :

		print '%s does not exists' % currTxtPath

def charNotation( charName='' , baseRig='' , charSize='' ) :
	
	# charName = 'noah'
	# baseRig = 'None'
	# charSize = '1.106'
	
	grp = mc.group( em=True , n='%sTxt_grp'%charName )
	
	charTxt = mc.textCurves(
								ch=False ,
								f='Times New Roman|h-13|w400|c0' ,
								t=charName
							)[0]
	baseTxt = mc.textCurves(
								ch=False ,
								f='Times New Roman|h-13|w400|c0' ,
								t=baseRig
							)[0]
	sizeTxt = mc.textCurves(
								ch=False ,
								f='Times New Roman|h-13|w400|c0' ,
								t=charSize
							)[0]
	
	mc.xform( charTxt , cp=True )
	mc.xform( baseTxt , cp=True )
	mc.xform( sizeTxt , cp=True )

	txtSca = 0.25
	mc.setAttr( '%s.s' % charTxt , txtSca , txtSca , txtSca )
	mc.setAttr( '%s.s' % baseTxt , txtSca , txtSca , txtSca )
	mc.setAttr( '%s.s' % sizeTxt , txtSca , txtSca , txtSca )
	txtH = -1
	mc.setAttr( '%s.ty' % charTxt , txtH )
	mc.setAttr( '%s.ty' % baseTxt , txtH*2 )
	mc.setAttr( '%s.ty' % sizeTxt , txtH*3 )
	mc.parent( charTxt , grp )
	mc.parent( baseTxt , grp )
	mc.parent( sizeTxt , grp )

def abbr( inputName='' ) :

	# Abbriviate input name

	ABBR_DICT = {
					'VRayMtl' : 'vrm' ,
					'layeredTexture' : 'layTex' ,
					'bumpMap' : 'bmp' ,
					'color' : 'col' ,
					'alpha' : 'alp' ,
					'VRayBlendMtl' : 'vrb' ,
					'base_material' : 'base' ,
					'reflectionColor' : 'rflcCol' ,
					'sampleInfo' : 'samp' ,
					'place2dTexture' : 'p2d' ,
					'coverage' : 'cov'
				}

	returnName = inputName

	if inputName in ABBR_DICT.keys() :
		returnName = ABBR_DICT[ inputName ]

	return returnName

def autoNameSelectedToTarget() :

	src , tar = mc.ls( sl=True )

	tarName , tarSide , tarType = rigTools.getElementName( tar )
	srcType = mc.nodeType( src )
	tarAttr = mc.listConnections( src , p=True , d=True , s=False )[0].split( '.' )[-1]

	srcName = '%s%s%s%s_%s' % (
									tarName ,
									rigTools.capitalizeFirst( abbr( tarType ) ) ,
									rigTools.capitalizeFirst( abbr( tarAttr ) ) ,
									tarSide ,
									abbr( srcType )
								)

	mc.rename( src , srcName )

def doChangeTextureExtensionTo( extension='jpg' ) :

	fns = mc.ls( type='file' )

	for fn in fns :

		currTxtPath = os.path.normpath( mc.getAttr( '%s.fileTextureName' % fn ) )
		currTxtFldPath , currTxtNameExt = os.path.split( currTxtPath )
		currTxtName , currTxtExt = os.path.splitext( currTxtNameExt )

		fixedTxtPath = os.path.join(
										currTxtFldPath ,
										'%s.%s' % ( currTxtName , extension )
									)
		print fixedTxtPath
		mc.setAttr( '%s.fileTextureName' % fn , fixedTxtPath , type='string' )

def doConvertExrTextureFromSelectedFileNode() :

	rvio = os.path.normpath( r'O:\systemTool\convertor\Tweak\RV-3.12.20-32\bin\rvio.exe' )

	fns = mc.ls( sl=True , type='file' )

	for fn in fns :

		currTxtPath = os.path.normpath( mc.getAttr( '%s.fileTextureName' % fn ) )
		currTxtFldPath , currTxtNameExt = os.path.split( currTxtPath )
		currTxtName , currTxtExt = os.path.splitext( currTxtNameExt )
		
		if currTxtExt == '.exr' :

			tifPath = os.path.join( currTxtFldPath , '%s.tif' % currTxtName )

			if not os.path.exists( tifPath ) :

				cmd = '%s %s -o %s' % ( rvio , currTxtPath , tifPath )
				subprocess.call( cmd )
				mc.setAttr( '%s.fileTextureName' % fn , tifPath , type='string' )
			else :

				mc.setAttr( '%s.fileTextureName' % fn , tifPath , type='string' )

		else :

			exrTxtPath = os.path.join( currTxtFldPath , '%s.exr' % currTxtName )
			mc.setAttr( '%s.fileTextureName' % fn , exrTxtPath , type='string' )

def createMov(
					tmpFilePath='' ,
					fps=24 ,
					outFilePath=''
				) :
	
	ffmpeg = os.path.normpath( r'O:/systemTool/toolPack/O/systemTool/python/playBlastPlus/convertor/ffmpeg_codec/bin/ffmpeg.exe' )

	cmd = '%s -y -i "%s" -r %s' % ( ffmpeg , os.path.normpath( tmpFilePath ) , fps )
	cmd += ' -vcodec libx264 -vprofile baseline -crf 22 -bf 0 -pix_fmt yuv420p'
	cmd += ' -f mov %s' % os.path.normpath( outFilePath )
	
	subprocess.call( cmd )
	
	return outFilePath

def pfAssetRelinkTexture() :
	prefix = r'P:\Lego_Template\asset\3D\friendHotelSetFromPf\source'

	for each in mc.ls( type='file' ) :
		
		mc.setAttr( '%s.fileTextureName' % each , l=False )
		currTexturePath = mc.getAttr( '%s.fileTextureName' % each )
		
		tifPath = os.path.join( prefix , currTexturePath ).replace( '.exr' , '.tif' )
		
		mc.setAttr(  '%s.fileTextureName' % each , tifPath , type='string' )

def pfAssetImportMatFile() :

	scenePath = os.path.normpath( mc.file( q=True , sn=True ) )
	sceneFolderPath , sceneNameExt = os.path.split( scenePath )

	matFilePath = os.path.join( sceneFolderPath , 'mat.ma' )

	mc.file( matFilePath , i=True )

def pfAssetReAssignShader( searchFor='' , replaceWith='' , prefix='', suffix='') :

	scenePath = os.path.normpath( mc.file( q=True , sn=True ) )
	sceneFolderPath , sceneNameExt = os.path.split( scenePath )
	
	shdFls = [ f for f in os.listdir( sceneFolderPath ) if f.endswith( '.shd' ) ]

	currShdFl = os.path.join( sceneFolderPath , shdFls[0] )

	geoToShader = pickle.load( open( currShdFl , 'rb' ) )
	
	for geo in geoToShader.keys() :
		
		print geo
		print geoToShader[ geo ]
		currShd = geoToShader[ geo ]
		
		if searchFor :
			currShd = currShd.replace( searchFor , replaceWith )

		if prefix:
			currShd = '%s%s' % (prefix, currShd)

		if suffix:
			currShd = '%s%s' % (currShd, suffix)
		
		assignShader( currShd , geo )

def getRelatedShader( node='' ) :

	# Get shader assigned to given node.

	shader = None
	
	mc.select( node , r=True )
	shapes = mc.ls( dag=True , o=True , s=True , sl=True , ni=True )
	
	sgs = mc.listConnections( shapes , type='shadingEngine' )
	if sgs :
		shader = mc.listConnections( '%s.surfaceShader' % sgs[0] , s=True , d=False )[0]

	return shader

def assignShader( shader='' , geo='' ) :
	
	for each in mc.ls( geo , l=True ) :

		cmd = 'assignSG %s %s' % ( shader , each )

		try :
			mm.eval( cmd )
		except :
			print 'Cannot assign %s to %s' % ( shader , geo )
	
	return True


def writeSelectedShadingData() :

	scenePath = os.path.normpath( mc.file( q=True , sn=True ) )
	sceneFolderPath , sceneNameExt = os.path.split( scenePath )
	sceneName , sceneExt = os.path.splitext( sceneNameExt )

	shadingDataName = '%sShadingData.shd' % sceneName

	shadingDataPath = os.path.join(
										sceneFolderPath ,
										shadingDataName
									)

	relatedShaderDict = {}

	for sel in mc.ls( sl=True , l=True ) :
		
		shader = getRelatedShader( sel )
		if shader :
			relatedShaderDict[ sel ] = shader

	pickle.dump( relatedShaderDict , open( shadingDataPath , 'wb' ) )
	
	print 'Shading data has been written to %s' % shadingDataPath
	return shadingDataPath

def batchRenameFiles( folderPath='' , search='' , replace='' ) :
	
	cleanedPath = os.path.normpath( folderPath )
	files = [ f for f in os.listdir( cleanedPath ) if os.path.isfile( os.path.join( cleanedPath , f ) ) ]
	
	for _file in files :
		
		currFileName = _file.replace( search , replace )
		oldFilePath = os.path.join( cleanedPath , _file )
		currFilePath = os.path.join( cleanedPath , currFileName )
		shutil.move( oldFilePath , currFilePath )

def frdAddEyeSpec() :

	lftSpecCtrl = 'eyeSpecLFT_ctrl'
	lftSpecCtrlShp = 'eyeSpecLFT_ctrlShape'
	lftSpecGrp = 'eyeSpecLFT_grp'
	lftSpecJnt = 'eyeSpecLFT_jnt'
	lftSpecAdd = 'eyeSpacLFT_add'

	rgtSpecCtrl = 'eyeSpecRGT_ctrl'
	rgtSpecCtrlShp = 'eyeSpecRGT_ctrlShape'
	rgtSpecGrp = 'eyeSpecRGT_grp'
	rgtSpecJnt = 'eyeSpecRGT_jnt'
	rgtSpecAdd = 'eyeSpacRGT_add'

	lftRp = mc.xform( lftSpecCtrl , q=True , rp=True , ws=True )
	lftSp = mc.xform( lftSpecCtrl , q=True , sp=True , ws=True )

	rgtRp = mc.xform( rgtSpecCtrl , q=True , rp=True , ws=True )
	rgtSp = mc.xform( rgtSpecCtrl , q=True , sp=True , ws=True )

	print lftRp , lftSp , rgtRp , rgtSp

	ctrls = ( lftSpecCtrl , rgtSpecCtrl )
	attrs = ( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' )

	for ctrl in ctrls :
		for attr in attrs :
			mc.setAttr( '%s.%s' % ( ctrl , attr ) , l=True )

	lftSubSpecCtrl = pc.Control( 'circle' )
	lftSubSpecCtrl.name = 'eyeSpecSubLFT_ctrl'
	lftSubSpecCtrl.color = 'white'
	lftSubSpecCtrl.rotateShape( ( 90 , 0 , 0 ) )
	lftSubSpecCtrl.scaleShape( 0.05 )
	lftSubSpecCtrl.attr( 'v' ).lockHide()

	lftSubSpecCtrlGrp = pc.group( lftSubSpecCtrl )
	lftSubSpecCtrlGrp.name = 'eyeSpecSubCtrlZroLFT_grp'

	lftSubSpecCtrlGrp.snap( lftSpecGrp )

	mc.addAttr( lftSpecCtrlShp , ln='subCtrlVis' , at='float' , min=0 , max=1 , k=True )
	mc.connectAttr( '%s.subCtrlVis' % lftSpecCtrlShp ,  lftSubSpecCtrlGrp.attr( 'v' ) )

	# mc.xform( lftSubSpecCtrl , rp=lftRp )
	mc.move( lftRp[0] , lftRp[1] , lftRp[2] , '%s.rotatePivot' % lftSubSpecCtrl , a=True )

	mc.parent( lftSubSpecCtrlGrp , lftSpecCtrl )
	mc.parent( lftSpecGrp , lftSubSpecCtrl )

	try :
		mc.setAttr( '%s.default' % lftSpecAdd , 0 )
	except :
		pass

	rgtSubSpecCtrl = pc.Control( 'circle' )
	rgtSubSpecCtrl.name = 'eyeSpecSubRGT_ctrl'
	rgtSubSpecCtrl.color = 'white'
	rgtSubSpecCtrl.rotateShape( ( 90 , 0 , 0 ) )
	rgtSubSpecCtrl.scaleShape( 0.05 )
	rgtSubSpecCtrl.attr( 'v' ).lockHide()

	rgtSubSpecCtrlGrp = pc.group( rgtSubSpecCtrl )
	rgtSubSpecCtrlGrp.name = 'eyeSpecSubCtrlZroRGT_grp'

	rgtSubSpecCtrlGrp.snap( rgtSpecGrp )

	mc.addAttr( rgtSpecCtrlShp , ln='subCtrlVis' , at='float' , min=0 , max=1 , k=True )
	mc.connectAttr( '%s.subCtrlVis' % rgtSpecCtrlShp ,  rgtSubSpecCtrlGrp.attr( 'v' ) )

	# mc.xform( rgtSubSpecCtrl , rp=rgtRp )
	mc.move( rgtRp[0] , rgtRp[1] , rgtRp[2] , '%s.rotatePivot' % rgtSubSpecCtrl , a=True )

	mc.parent( rgtSubSpecCtrlGrp , rgtSpecCtrl )
	mc.parent( rgtSpecGrp , rgtSubSpecCtrl )

	try :
		mc.setAttr( '%s.default' % rgtSpecAdd , 0 )
	except :
		pass

def city2DDoCreateNewRenderLayer() :
	
	result = mc.promptDialog(
								title='Render Layer Name',
								message='Enter Name:' ,
								button=[ 'OK' , 'Cancel' ] ,
								defaultButton='OK',
								cancelButton='Cancel',
								dismissString='Cancel'
							)
	
	if result == 'OK' :
		newLayerName = mc.promptDialog( query=True , text=True )
		city2DCreateNewRenderLayer( newLayerName )

def city2DCreateNewRenderLayer( newLayerName='' ) :

	sels = mc.ls( sl=True )
	currLayer = mc.editRenderLayerGlobals( query=True, currentRenderLayer=True )

	if not newLayerName in mc.ls( type='renderLayer' ) :
		mc.createRenderLayer( n=newLayerName )

	for sel in sels :
		
		if not currLayer == 'defaultRenderLayer' :
			mc.editRenderLayerMembers( currLayer , sel , r=True )

		mc.editRenderLayerMembers( newLayerName , sel )

	mc.editRenderLayerGlobals( currentRenderLayer=newLayerName )

def city2DImportFaceGeo() :

	facialFile = r'Y:/USERS/Peck/proj/testCity2D/assets/miniFigEye/hero/rig.mb'

	mc.file( facialFile , i=True )

def city2DParentFaceGeo() :

	faceGeo = 'eyeGuideGeo_grp'
	geoGrp = 'headGeo_grp'
	mc.parent( faceGeo , geoGrp )
	mc.parentConstraint( 'head2Skin_jnt' , faceGeo , mo=True )

def city2DGroupCharacterParts() :

	geoGrp = 'geo_grp'

	bodyGrp = mc.group( n='bodyGeo_grp' , em=True )
	headGrp = mc.group( n='headGeo_grp' , em=True )
	lHandGrp = mc.group( n='handGeoLFT_grp' , em=True )
	rHandGrp = mc.group( n='handGeoRGT_grp' , em=True )
	# lArmGrp = mc.group( n='armGeoLFT_grp' , em=True )
	# rArmGrp = mc.group( n='armGeoRGT_grp' , em=True )
	lLegGrp = mc.group( n='legGeoLFT_grp' , em=True )
	rLegGrp = mc.group( n='legGeoRGT_grp' , em=True )

	legGeoGrp = 'legGeo_grp'
	lArmGeoGrp = 'armGeoLFT_grp'
	rArmGeoGrp = 'armGeoRGT_grp'
	lLegGeo = 'legLFT_ply'
	rLegGeo = 'legRGT_ply'
	lHandGeoGrp = 'handPlyZroLFT_grp'
	rHandGeoGrp = 'handPlyZroRGT_grp'
	head = 'head_ply'
	body = 'body_ply'
	pelvis = 'pelvis_ply'
	hip = 'hip_ply'

	mc.parent( head , headGrp )
	mc.parent( lLegGeo , headGrp )
	mc.parent( lLegGeo , lLegGrp )
	mc.parent( rLegGeo , rLegGrp )
	mc.parent( lHandGeoGrp , lHandGrp )
	mc.parent( rHandGeoGrp , rHandGrp )
	mc.parent( body , hip , pelvis , bodyGrp )

	mc.parent( headGrp , geoGrp )
	mc.parent( lHandGrp , geoGrp )
	mc.parent( rHandGrp , geoGrp )
	mc.parent( lLegGrp , legGeoGrp )
	mc.parent( rLegGrp , legGeoGrp )
	mc.parent( bodyGrp , geoGrp )

def city2DTransferColorToIncan() :

	sels = mc.ls( sl=True )
	
	for sel in sels :
		
		mc.hyperShade( objects=sel )

		objs = mc.ls( sl=True )

		colSources = mc.listConnections( '%s.color' % sel , source=True , p=True )
		transSources = mc.listConnections( '%s.transparency' % sel , source=True , p=True )
		
		surfaceShd = mc.shadingNode( 'surfaceShader' , asShader=True )
		
		if colSources :
			mc.connectAttr( colSources[0] , '%s.outColor' % surfaceShd )
		else :
			colVals = mc.getAttr( '%s.color' % sel )
			mc.setAttr(
						'%s.outColor' % surfaceShd ,
						colVals[0][0] ,
						colVals[0][1] ,
						colVals[0][2]
						)

		if transSources :
			mc.connectAttr(
								transSources[0] ,
								'%s.outTransparency' % surfaceShd
							)

		if objs and surfaceShd :
			mc.select( objs , r=True )
			mc.hyperShade( assign=surfaceShd )

		if mc.objExists( sel ) :
			mc.delete( sel )

def city2DPublish( pubType='Render' ) :

	# Auto fill asset data
	from rigTool import genScript
	reload(genScript)

	genScript.autoAssign()

	mc.file( s=True )

	scenePath = os.path.normpath( mc.file( q=True , sn=True ) )
	sceneFolderPath , sceneNameExt = os.path.split( scenePath )
	sceneFolderPathElem = sceneFolderPath.split( os.sep )

	refFolderPath = os.path.join(
									sceneFolderPathElem[0] ,
									os.sep ,
									sceneFolderPathElem[1] ,
									sceneFolderPathElem[2] ,
									sceneFolderPathElem[3] ,
									sceneFolderPathElem[4] ,
									sceneFolderPathElem[5] ,
									sceneFolderPathElem[6] ,
									'ref'
								)
	pubFileName = '%s_%s.mb' % ( sceneFolderPathElem[6] , pubType )

	pubFilePath = os.path.join( refFolderPath , pubFileName )

	mc.file( rn=pubFilePath )
	mc.file( type='mayaBinary', s=True )

	print '\nScene has been published to %s\n' % pubFilePath

def city2DRef( pubType='Anim' ) :
	
	scenePath = os.path.normpath( mc.file( q=True , sn=True ) )
	sceneFolderPath , sceneNameExt = os.path.split( scenePath )
	sceneFolderPathElem = sceneFolderPath.split( os.sep )

	refFolderPath = os.path.join(
									sceneFolderPathElem[0] ,
									os.sep ,
									sceneFolderPathElem[1] ,
									sceneFolderPathElem[2] ,
									sceneFolderPathElem[3] ,
									sceneFolderPathElem[4] ,
									sceneFolderPathElem[5] ,
									sceneFolderPathElem[6] ,
									'ref'
								)
	pubFileName = '%s_%s' % ( sceneFolderPathElem[6] , pubType )
	pubFileNameExt = '%s.mb' % pubFileName
	
	pubFilePath = os.path.join( refFolderPath , pubFileNameExt )
	
	mc.file( pubFilePath , r=True , ns=pubFileName )

def createThumbnail( width=450 , height=300 ) :

	currentUserPath = os.path.normpath( os.path.expanduser('~') )
	tmpFilePath = os.path.join(
									currentUserPath ,
									'playblastTemp'
								)
	
	oWidth = mc.getAttr( 'defaultResolution.width' )
	oHeight = mc.getAttr( 'defaultResolution.height' )
	
	# width = 450
	# height = 300

	frame = mc.currentTime( q=True )

	args = {
				'format' : 'iff' ,
				'filename' : tmpFilePath ,
				'frame' : frame ,
				'forceOverwrite' : True ,
				'clearCache' : True ,
				'viewer' : 0 ,
				'showOrnaments' : 0 ,
				'fp' : 4 ,
				'percent' : 100 ,
				'quality' : 100 ,
				'width' : width ,
				'height' : height ,
				'offScreen' : True ,
				'indexFromZero' : True
			}

	mc.setAttr( 'defaultResolution.aspectLock' , False )
	mc.setAttr( 'defaultResolution.width' , width )
	mc.setAttr( 'defaultResolution.height' , height )
	
	mc.setAttr( 'defaultRenderGlobals.imageFormat' , 32 )
	mc.playblast( **args )

	# mc.setAttr( 'defaultResolution.width' , oWidth )
	# mc.setAttr( 'defaultResolution.height' , oHeight )
	
	blastFilePath = '%s%splayblastTemp.0000.png' % ( currentUserPath , os.sep )
	return blastFilePath

def city2DSetPlayblastCam( cam='persp' ) :

	camShp = mc.listRelatives( cam , s=True )[0]
	
	mc.setAttr( 'defaultResolution.width' , 450 )
	mc.setAttr( 'defaultResolution.height' , 300 )

	mc.setAttr( '%s.focalLength' % camShp , 60 )
	mc.camera(
				cam ,
				e=True ,
				displayFilmGate=False ,
				displayResolution=True ,
				filmFit='overscan'
			)

def city2DCreateThumbnail() :

	import sg.utils as sgUtil
	reload( sgUtil )

	scenePath = os.path.normpath( mc.file( q=True , sn=True ) )
	sceneData = scenePath.split( os.sep )

	projName = sceneData[1]
	assetName = sceneData[6]
	assetType = sceneData[4]
	assetSubType = sceneData[5]

	mc.camera(
				'persp' ,
				e=True ,
				displayFilmGate=False ,
				displayResolution=True ,
				filmFit='overscan'
			)

	imgPath = os.path.normpath( createThumbnail() )

	iconPath = os.path.join(
								sceneData[0] ,
								os.sep ,
								sceneData[1] ,
								sceneData[2] ,
								sceneData[3] ,
								sceneData[4] ,
								sceneData[5] ,
								sceneData[6] ,
								'images' ,
								'icon' ,
								'%s.png' % sceneData[6]
							)
	
	shutil.copy2( imgPath , iconPath )

	sgUtil.sgUploadAssetIcon(
								projName ,
								assetName ,
								assetType ,
								assetSubType ,
								imgPath
							)