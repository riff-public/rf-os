import maya.cmds as mc
import os
import pickle
import pkmel.core as pc
reload( pc )

def writeAllCtrl() :
	
	dataFld = pc.getDataFld()
	
	if not os.path.isdir( dataFld ) :
		 os.mkdir( dataFld )
	
	ctrls = mc.ls( "*_ctrl" )
	fn = '%s/ctrlAttr.pkr' % dataFld
	writeCtrlAttr( ctrls , fn )
	
	print 'Exporting all control attribute is done.'

def readAllCtrl( search = '' , replace = '' ) :
	
	dataFld = pc.getDataFld()
	
	if not os.path.isdir( dataFld ) :
		 os.mkdir( dataFld )
	
	ctrls = mc.ls( "*_ctrl" )
	fn = '%s/ctrlAttr.pkr' % dataFld
	readCtrlAttr( fn , search = search , replace = replace )
	
	print 'Importing all control attribute is done.'

def writeDagHrchy() :

	# dataFld = pc.getDataFld()
	
	# if not os.path.isdir( dataFld ) :
	# 	 os.mkdir( dataFld )
	
	# fn = '%s/ctrlHrchy.pkr' % dataFld
	# fid = open( fn , 'w' )

	hrchyDict = {}
	dags = []
	
	tfs = mc.ls( type='transform' , cameras=False )
	jnts = mc.ls( type='joint' )
	
	dags = tfs + jnts
	
	print dags

def writeCtrlAttr( ctrls = [] , fn = '' ) :
	
	fid = open( fn , 'w' )
	
	ctrlDct = {}
	
	for ctrl in ctrls :
		
		currCtrl = pc.Dag( ctrl )
		currShape = pc.Dag( currCtrl.shape )
		
		for each in ( currCtrl , currShape ) :
			
			if mc.objExists( each ) :

				attrs = []

				dfAttrs = [
								'tx' , 'ty' , 'tz' ,
								'rx' , 'ry' , 'rz' ,
								'sx' , 'sy' , 'sz' ,
								'v'
							]
				udAttrs = mc.listAttr( each , ud=True , lf=True )

				if not each == currShape :
					attrs += dfAttrs

				if udAttrs :
					attrs += udAttrs

				if attrs :
									
					for attr in attrs :

						currCtrlAttr = '%s.%s' % ( each , attr )
						val = mc.getAttr( currCtrlAttr )
						lockState = mc.getAttr( currCtrlAttr , l=True )
						keyState = mc.getAttr( currCtrlAttr , k=True )
						cbState = mc.getAttr( currCtrlAttr , cb=True )

						if not type( val ) == type( [] ) :

							resultDict = {
											'value' : val ,
											'lock' : lockState ,
											'keyable' : keyState ,
											'channelbox' : cbState
										}

							ctrlDct[ currCtrlAttr ] = resultDict
						
	pickle.dump( ctrlDct , fid )
	fid.close()

def readCtrlAttr( fn = '' , search = '' , replace = '' ) :
	
	fid = open( fn , 'r' )
	ctrlDct = pickle.load( fid )
	fid.close()
	
	for key in ctrlDct.keys() :
		
		if search :
			currCtrlAttr = key.replace( search , replace )
		else :
			currCtrlAttr = key
		
		if mc.objExists( currCtrlAttr ) :
			
			val = ctrlDct[currCtrlAttr]['value']
			lockState = ctrlDct[currCtrlAttr]['lock']
			keyState = ctrlDct[currCtrlAttr]['keyable']
			cbState = ctrlDct[currCtrlAttr]['channelbox']
			
			mc.setAttr( currCtrlAttr , l=lockState )
			
			if not lockState :
				mc.setAttr( currCtrlAttr , val )
				mc.setAttr( currCtrlAttr , k=keyState )
				if not keyState :
					mc.setAttr( currCtrlAttr , cb=cbState )





















