import maya.cmds as mc
import pymel.core as pm
import maya.mel as mm
import os
import pickle

from utaTools.utapy import copyObjFile 
reload(copyObjFile)

from rf_utils.context import context_info
from rf_utils import file_utils

#path project config
rootScript = os.environ['RFSCRIPT']
pathProjectJson = r"{}\core\rf_maya\rftool\rig\export_config\project_config.json".format(rootScript)
projectData = file_utils.json_loader(pathProjectJson)


def getDataFld() :
	
	wfn = os.path.normpath( pm.sceneName() )

	tmpAry = wfn.split( '\\' )
	tmpAry[-2] = 'data'
	
	dataFld = '\\'.join( tmpAry[0:-1] )
	
	if not os.path.isdir( dataFld ) :
		os.mkdir( dataFld )
	print dataFld
	return dataFld

def writeWeight( geo='' , fn='' ) :

	skn = mm.eval( 'findRelatedSkinCluster( "%s" )' % geo )

	if skn :

		sknMeth = mc.getAttr( '%s.skinningMethod' % skn )
		infs = mc.skinCluster( skn , q = True , inf = True )
		sknSet = mc.listConnections( '%s.message' % skn , d=True , s=False )[0]
		print fn
		fid = open( fn , 'w' )
		wDct = {}

		wDct[ 'influences' ] = infs
		wDct[ 'name' ] = skn
		wDct[ 'set' ] = sknSet
		wDct[ 'skinningMethod' ] = sknMeth

		for ix in xrange( mc.polyEvaluate( geo , v = True ) ) :

			currVtx = '%s.vtx[%d]' % ( geo , ix )
			skinVal = mc.skinPercent( skn , currVtx , q = True , v = True )
			wDct[ ix ] = skinVal

		pickle.dump( wDct , fid )
		fid.close()
	else :
		print '%s has no related skinCluster node.' % geo

def writeSelectedWeight() :

	# Project Name 
	asset = context_info.ContextPathInfo()
	projectName = asset.project if mc.file(q=True, sn=True) else ''

	# Export skin weight values into selected geometries
	suffix = 'Weight'
	if projectName in projectData['project']:
		for sel in mc.ls( sl=True ) :
			## Path "data"
			dataFld = copyObjFile.getDataFld()

			fls = os.listdir( dataFld )
			fn = '%s%s.txt' % ( sel , suffix )
			fPth = '%s/%s' % ( dataFld , fn )
			writeWeight( sel , fPth )

	else:
		for sel in mc.ls( sl=True ) :
			
			dataFld = getDataFld()
			fls = os.listdir( dataFld )
			fn = '%s%s.txt' % ( sel , suffix )

			fPth = '%s/%s' % ( dataFld , fn )
			writeWeight( sel , fPth )

	print "Project Name ::", projectName
	mc.confirmDialog( title='Progress' , message='Exporting weight has done.' )


def writeWeightBpm( geo='' , fn='' ,infs=None) :

	skn = mm.eval( 'findRelatedSkinCluster( "%s" )' % geo )

	if skn :

		sknMeth = mc.getAttr( '%s.skinningMethod' % skn )
		#infs = mc.skinCluster( skn , q = True , inf = True )
		sknSet = mc.listConnections( '%s.message' % skn , d=True , s=False )[0]
		print fn
		fid = open( fn , 'w' )
		wDct = {}

		infs2 = []
		for inf in infs:
			if ':' in inf:
				inf2 = inf.split(':')[1]
				infs2.append(inf2)

		wDct[ 'influences' ] = infs2
		wDct[ 'name' ] = skn
		wDct[ 'set' ] = sknSet
		wDct[ 'skinningMethod' ] = sknMeth

		for ix in xrange( mc.polyEvaluate( geo , v = True ) ) :

			currVtx = '%s.vtx[%d]' % ( geo , ix )
			skinVal = mc.skinPercent( skn , currVtx , q = True , v = True )
			wDct[ ix ] = skinVal

		pickle.dump( wDct , fid )
		fid.close()
	else :
		print '%s has no related skinCluster node.' % geo

def writeSelectedWeightBpm(infs = None) :

	# Project Name 
	asset = context_info.ContextPathInfo()
	projectName = asset.project if mc.file(q=True, sn=True) else ''

	# Export skin weight values into selected geometries
	suffix = 'Weight'
	if projectName in projectData['project']:
		for sel in mc.ls( sl=True ) :
			## Path "data"
			dataFld = copyObjFile.getDataFld()

			fls = os.listdir( dataFld )
			selName = sel

			if ':' in sel:
				selName = sel.split(':')[1]
				selNameSp = selName.split('_')
			fn = '%sBaseBpm_%sWeight.txt' % ( selNameSp[0] , selNameSp[-1] )
			fPth = '%s/%s' % ( dataFld , fn )
			writeWeightBpm( sel , fPth ,infs)

	else:
		for sel in mc.ls( sl=True ) :
			
			dataFld = getDataFld()
			fls = os.listdir( dataFld )
			selName = sel
			if ':' in sel:
				selName = sel.split(':')[1]
			fn = '%s%s.txt' % ( selName , suffix )

			fPth = '%s/%s' % ( dataFld , fn )
			writeWeightBpm( sel , fPth ,infs)

	print "Project Name ::", projectName
	mc.confirmDialog( title='Progress' , message='Exporting weight has done.' )


# def writeSelectedWeight() :
# 	# Export skin weight values into selected geometries
# 	suffix = 'Weight'
# 	for sel in mc.ls( sl=True ) :
		
# 		dataFld = getDataFld()
# 		fls = os.listdir( dataFld )
# 		fn = '%s%s.txt' % ( sel , suffix )
		
# 		fPth = '%s/%s' % ( dataFld , fn )
# 		writeWeight( sel , fPth )
	
# 	mc.confirmDialog( title='Progress' , message='Exporting weight has done.' )


def readWeight( geo = '' , fn = '' ) :
	
	print 'Loading %s' % fn
	fid = open( fn , 'r' )
	wDct = pickle.load( fid )
	fid.close()
	
	infs = wDct[ 'influences' ]
	
	for inf in infs :
		if not mc.objExists( inf ) :
			print 'Scene has no %s ' % inf

	oSkn = mm.eval( 'findRelatedSkinCluster "%s"' % geo )
	if oSkn :
		mc.skinCluster( oSkn , e = True , ub = True )

	tmpSkn = mc.skinCluster( infs , geo , tsb=True )[0]
	skn = mc.rename( tmpSkn , wDct[ 'name' ] )
	mc.setAttr( '%s.skinningMethod' % skn , wDct['skinningMethod'] )

	sknSet = mc.listConnections( '%s.message' % skn , d=True , s=False )[0]
	mc.rename( sknSet , wDct[ 'set' ] )
	
	for inf in infs :
		mc.setAttr( '%s.liw' % inf , False )
	
	mc.setAttr( '%s.normalizeWeights' % skn , False )
	mc.skinPercent( skn , geo , nrm=False , prw=100 )
	mc.setAttr( '%s.normalizeWeights' % skn , True )
	
	vtxNo = mc.polyEvaluate( geo , v = True )
	
	for ix in xrange( vtxNo ) :		
		for iy in xrange( len( infs ) ) :
			wVal = wDct[ ix ][ iy ]
			if wVal :
				wlAttr = '%s.weightList[%s].weights[%s]' % ( skn , ix , iy )
				mc.setAttr( wlAttr , wVal )
		
		# Percent calculation
		if ix == ( vtxNo - 1 ) :
			print '100%% done.'
		else :
			prePrcnt = 0
			if ix > 0 :
				prePrcnt = int( ( float( ix - 1 ) / vtxNo ) * 100.00 )
			
			prcnt = int( ( float( ix ) / vtxNo ) * 100.00 )

			if not prcnt == prePrcnt :
				print '%s%% done.' % str( prcnt )

def readWeight2( geo = '' , fn = '' , searchFor='' , replaceWith='' , prefix='' , suffix='' ) :
	
	print 'Loading %s' % fn
	fid = open( fn , 'r' )
	wDct = pickle.load( fid )
	fid.close()
	
	infs = wDct[ 'influences' ]

	print infs

	currInfs = []

	for inf in infs :
		currInf = inf
		if searchFor :
			currInf = currInf.replace( searchFor , replaceWith )
		print currInf
		currInfs.append( '%s%s%s' % ( prefix , currInf , suffix ) )
	
	for currInf in currInfs :
		if not mc.objExists( currInf ) :
			print 'Scene has no %s ' % currInf

	oSkn = mm.eval( 'findRelatedSkinCluster "%s"' % geo )
	if oSkn :
		mc.skinCluster( oSkn , e = True , ub = True )

	tmpSkn = mc.skinCluster( currInfs , geo , tsb=True )[0]
	skn = mc.rename( tmpSkn , wDct[ 'name' ] )
	mc.setAttr( '%s.skinningMethod' % skn , wDct['skinningMethod'] )

	sknSet = mc.listConnections( '%s.message' % skn , d=True , s=False )[0]
	mc.rename( sknSet , wDct[ 'set' ] )
	
	for currInf in currInfs :
		mc.setAttr( '%s.liw' % currInf , False )
	
	mc.setAttr( '%s.normalizeWeights' % skn , False )
	mc.skinPercent( skn , geo , nrm=False , prw=100 )
	mc.setAttr( '%s.normalizeWeights' % skn , True )
	
	vtxNo = mc.polyEvaluate( geo , v = True )
	
	for ix in xrange( vtxNo ) :		
		for iy in xrange( len( currInfs ) ) :
			wVal = wDct[ ix ][ iy ]
			if wVal :
				wlAttr = '%s.weightList[%s].weights[%s]' % ( skn , ix , iy )
				mc.setAttr( wlAttr , wVal )
		
		# Percent calculation
		if ix == ( vtxNo - 1 ) :
			print '100%% done.'
		else :
			prePrcnt = 0
			if ix > 0 :
				prePrcnt = int( ( float( ix - 1 ) / vtxNo ) * 100.00 )
			
			prcnt = int( ( float( ix ) / vtxNo ) * 100.00 )

			if not prcnt == prePrcnt :
				print '%s%% done.' % str( prcnt )

def readSelectedWeight2( weightFolderPath='' , searchFor='' , replaceWith='' , prefix='' , suffix='' ) :

	# Project Name 
	asset = context_info.ContextPathInfo()
	projectName = asset.project if mc.file(q=True, sn=True) else ''

	# Import skin weight values into selected geometries
	sels = 	mc.ls( sl=True )
	for sel in sels :
		
		if not weightFolderPath :
			dataFld = getDataFld()
		else :
			dataFld = os.path.normpath( weightFolderPath )
		fn = '%sWeight.txt' % sel
		
		try :
			print 'Importing %s.' % sel
			readWeight2( sel , os.path.join( dataFld , fn ) , searchFor , replaceWith , prefix , suffix )
			print 'Importing %s done!!.' % fn
		except :
			print 'Cannot find weight file for!! %s' % sel
	
	mc.select( sels )
	mc.confirmDialog( title='Progress' , message='Importing weight is done!!.' )

def readSelectedWeight( weightFolderPath='' ) :

	# Project Name 
	asset = context_info.ContextPathInfo()
	projectName = asset.project if mc.file(q=True, sn=True) else ''

	# Import skin weight values into selected geometries
	sels = 	mc.ls( sl=True )
	if projectName in projectData['project']:
		## Path "data"
		dataFld = copyObjFile.getDataFld()
		# print dataFld, 'l;lll'
		for sel in sels :
			fn = '%sWeight.txt' %  sel
			
			try :
				readWeight( sel , os.path.join( dataFld , fn ) )
				print 'Importing %s done!!.' % fn
			except :
				print 'Cannot find weight file for!! %s' % sel

	else:
		for sel in sels :
			
			if not weightFolderPath :
				dataFld = getDataFld()
			else :
				dataFld = os.path.normpath( weightFolderPath )
			fn = '%sWeight.txt' % sel
			
			try :
				readWeight( sel , os.path.join( dataFld , fn ) )
				print 'Importing %s done!!.' % fn
			except :pass
				# print 'Cannot find weight file for!! %s' % sel
				
	print "Project Name ::", projectName
	mc.select( sels )
	mc.confirmDialog( title='Progress' , message='Importing weight is done!!.' )

# def readSelectedWeight( weightFolderPath='' ) :
# 	# Import skin weight values into selected geometries
# 	sels = 	mc.ls( sl=True )
# 	for sel in sels :
		
# 		if not weightFolderPath :
# 			dataFld = getDataFld()
# 		else :
# 			dataFld = os.path.normpath( weightFolderPath )
# 		fn = '%sWeight.txt' % sel
		
# 		try :
# 			print 'Importing %s.' % sel
# 			readWeight( sel , os.path.join( dataFld , fn ) )
# 			print 'Importing %s done.' % fn
# 		except :
# 			print 'Cannot find weight file for %s' % sel
	
# 	mc.select( sels )
# 	mc.confirmDialog( title='Progress' , message='Importing weight is done.' )

	
def copySelectedWeight() :
	# Copy skin weights from source object to target object.
	# Select source geometry then target geometry then run script.
	sels = mc.ls( sl=True )
	selsMas = sels[0]
	seleChi = sels[1:]
	for sel in seleChi:
		jnts = mc.skinCluster( selsMas , q = True , inf = True )
		
		oSkn = mm.eval( 'findRelatedSkinCluster( "%s" )' % sel )
		if oSkn :
			mc.skinCluster( oSkn , e = True , ub = True )
		
		skn = mc.skinCluster( jnts , sel , tsb = True )[0]
		
		mc.select( selsMas , r=True )
		mc.select( sel , add=True )
		mm.eval( 'copySkinWeights  -noMirror -surfaceAssociation closestPoint -influenceAssociation oneToOne -influenceAssociation oneToOne;' )
		mc.select(cl=True)