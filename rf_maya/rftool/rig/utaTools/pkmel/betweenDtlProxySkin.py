from nuTools.rigTools import proc
reload(proc)
def main() :
# Neck 
	proc.btwJnt(jnts=['spine5ScaProxySkin_jnt', 'neck1ProxySkin_jnt', 'neckRbnProxySkin_jnt'], pointConstraint=True)

# Arm LFT
	proc.btwJnt(jnts=['spine5ScaProxySkin_jnt', 'clav1ProxySkinLFT_jnt', 'upArmProxySkinLFT_jnt'], pointConstraint=True)
	proc.btwJnt(jnts=['clav1ProxySkinLFT_jnt', 'upArmProxySkinLFT_jnt', 'upArmRbnDtl1ProxySkinLFT_jnt'], pointConstraint=True)
	proc.btwJnt(jnts=['upArmRbnDtl5ProxySkinLFT_jnt', 'forearmProxySkinLFT_jnt', 'forearmRbnDtl1ProxySkinLFT_jnt'], pointConstraint=False)
	proc.btwJnt(jnts=['forearmRbnDtl5ProxySkinLFT_jnt', 'wristProxySkinLFT_jnt', 'handProxySkinLFT_jnt'], pointConstraint=True)

# Arm RGT
	proc.btwJnt(jnts=['spine5ScaProxySkin_jnt', 'clav1ProxySkinRGT_jnt', 'upArmProxySkinRGT_jnt'], pointConstraint=True)
	proc.btwJnt(jnts=['clav1ProxySkinRGT_jnt', 'upArmProxySkinRGT_jnt', 'upArmRbnDtl1ProxySkinRGT_jnt'], pointConstraint=True)
	proc.btwJnt(jnts=['upArmRbnDtl5ProxySkinRGT_jnt', 'forearmProxySkinRGT_jnt', 'forearmRbnDtl1ProxySkinRGT_jnt'], pointConstraint=False)
	proc.btwJnt(jnts=['forearmRbnDtl5ProxySkinRGT_jnt', 'wristProxySkinRGT_jnt', 'handProxySkinRGT_jnt'], pointConstraint=True)

# Leg LFT
	proc.btwJnt(jnts=['spine1ScaProxySkin_jnt', 'upLegProxySkinLFT_jnt', 'upLegRbnDtl1ProxySkinLFT_jnt'], pointConstraint=True)
	proc.btwJnt(jnts=['upLegRbnDtl5ProxySkinLFT_jnt', 'lowLegProxySkinLFT_jnt', 'lowLegRbnDtl1ProxySkinLFT_jnt'], pointConstraint=False)
	proc.btwJnt(jnts=['lowLegRbnDtl5ProxySkinLFT_jnt', 'ankleProxySkinLFT_jnt', 'ballProxySkinLFT_jnt'], pointConstraint=True)

# Leg RGT
	proc.btwJnt(jnts=['spine1ScaProxySkin_jnt', 'upLegProxySkinRGT_jnt', 'upLegRbnDtl1ProxySkinRGT_jnt'], pointConstraint=True)
	proc.btwJnt(jnts=['upLegRbnDtl5ProxySkinRGT_jnt', 'lowLegProxySkinRGT_jnt', 'lowLegRbnDtl1ProxySkinRGT_jnt'], pointConstraint=False)
	proc.btwJnt(jnts=['lowLegRbnDtl5ProxySkinRGT_jnt', 'ankleProxySkinRGT_jnt', 'ballProxySkinRGT_jnt'], pointConstraint=True)
