import maya.cmds as mc
from utaTools.utapy import utaCore
reload(utaCore)

def autoRoll(ctrls = [], driven = True, target = [], axis = [],*args):
	time = 'time1.outTime'


	if not ctrls:
		ctrls = mc.ls(sl = True)
	for i in range(len(ctrls)):
	# for ctrl in ctrls:
		name, side, lastName = utaCore.splitName(sels = [ctrls[i]] , optionSide = True)
		## add Attribute
		utaCore.attrNameTitle(obj = ctrls[i], ln = 'Functions')
		utaCore.addAttrCtrl (ctrl = ctrls[i], elem = ['Auto'], min = 0, max = 1, at = 'long', dv = 0)
		utaCore.addAttrCtrl (ctrl = ctrls[i], elem = ['Speed', 'Offset'], min = -1000, max = 1000, at = 'float', dv = 0)

		## createNode
		bclNode = mc.createNode('blendColors', n = '{}{}Bcl'.format(name, side))
		mdvNode = mc.createNode('multiplyDivide', n = '{}{}Mdv'.format(name, side))
		mdvRevNode = mc.createNode('multiplyDivide', n = '{}Rev{}Mdv'.format(name, side))

		## connectAttr
		mc.connectAttr(time,'{}.color1R'.format(bclNode))
		mc.connectAttr('{}.Offset'.format(ctrls[i]), '{}.color2R'.format(bclNode))
		mc.connectAttr('{}.Auto'.format(ctrls[i]), '{}.blender'.format(bclNode))
		mc.connectAttr('{}.outputR'.format(bclNode), '{}.input1X'.format(mdvNode))
		mc.connectAttr('{}.Speed'.format(ctrls[i]),'{}.input2X'.format(mdvNode))
		mc.connectAttr('{}.outputX'.format(mdvNode), '{}.input1X'.format(mdvRevNode))
		mc.setAttr('{}.input2X'.format(mdvRevNode), -1)	
		if driven:
			if not target[i]:
				mc.connectAttr('{}.outputX'.format(mdvRevNode), '{}CtrlOfst{}Grp.{}'.format(name, side, axis[i]))
			else:
				mc.connectAttr('{}.outputX'.format(mdvRevNode), '{}.{}'.format(target[i], axis[i]))

		## setDefault Attr
		mc.setAttr('{}.Auto'.format(ctrls[i]), 1)
		mc.setAttr('{}.Speed'.format(ctrls[i]), 0.5)