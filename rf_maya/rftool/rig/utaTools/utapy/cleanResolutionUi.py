import sys
sys.path.append(r'P:\lib\local\utaTools\utapy')
import cleanResolution3 as crl 
reload(crl)
import pymel.core as pm 
reload(pm)
import eyelidFollowConnect as efc 
reload(efc)

# clean Skin
def cleanFile ( *args ) :
    crl.clean();
    # efc.lockMdv(listMdv = ['eyeLid_lft_mdv', 'eyeLid_rgt_mdv'], lock = False);
# clean Dtl
def cleanDtlFile( *args ) :
    crl.cleanDtl();
# clean Bsh
def cleanBshFile ( *args ) :
    crl.cleanBsh();
# clean FacialSq
def cleanFacialSqFile ( *args ) :
    crl.cleanFacialSq();
# connectDtl
def connectDtlFile ( *args ) :
    crl.connectDtl();
# connectBsn
def connectBsnFile ( *args ) :
    crl.connectBsn();
# connect FacialSQ
def connectFacialSqFile ( *args ) :
    # crl.connectFacialSq();
    crl.connectFacialSqRun()
# lock eyeLid Joint
def disconEyeLid( *args ) :
    efc.discon()
    # efc.lockEyeLid()
    # efc.unLockMdv(listMdv = ['eyeLid_lft_mdv', 'eyeLid_rgt_mdv'], lock = True);
def cleanFileAndLockEyeLid( *args ) :
    crl.clean();
    efc.lockAttrs(listObj = ['EyeLidsBsh_L_Jnt','EyeLidsBsh_R_Jnt'], lock = True);
    efc.lockMdv(listObj = ['eyeLid_lft_mdv', 'eyeLid_rgt_mdv', 'legIkStrtAmp_lft_mdv', 'legIkStrtAmp_rgt_mdv', 'toeIkStrt_lft_mdv', 'toeIkStrt_rgt_mdv'], lock = True);
    efc.lockBcl(listObj = ['legIkStrt_lft_bcl', 'legIkStrt_rgt_bcl', 'legIkLock_lft_bcl', 'legIkLock_rgt_bcl'], lock = True);
    efc.lockRev(listObj = ['ankleIkLocWor_lft_rev', 'ankleIkLocWor_rgt_rev'], lock = True);
    efc.lockPma(listObj = ['toeIkStrt_lft_pma', 'toeIkStrt_rgt_pma'], lock = True);
    efc.lockMidFacialSq_Ctrl(listObj = ['HdMidHdDfmRig_Ctrl'], lock = True);
    efc.lockUpLowFacialSq_Ctrl(listObj = ['HdUpHdDfmRig_Ctrl', 'HdLowHdDfmRig_Ctrl'], lock = True);
 
# # clean CombRig
# def cleanCombRig (*args):
#     crl.cleanCombRig()
    
def windowUI ( *args ) :
    
    # check if window exists
    if pm.window ( 'windowUI' , exists = True ) :
        pm.deleteUI ( 'windowUI' ) ;
    else : pass ;
   
    window = pm.window ( 'windowUI', title = "cleanResolution v. 1.0" ,
        mnb = False , mxb = False , sizeable = True , rtf = True ) ;
        
    pm.window ( 'windowUI' , e = True , h = 400, w = 220 ) ;
    
    mainLayout = pm.rowColumnLayout ( nc = 1 , p = window ) ;

    # 0. clean
    rowSkinLabel = pm.rowColumnLayout ( nc = 1 , p = mainLayout , h = 2, w = 220 ) ;
    # pm.text ( label = '1. Clean Skin File' , p = rowSkinLabel ) ;
    pm.button ( 'clean' , label = ' CLEAN ', h = 50 , w = 200, p = mainLayout , c = cleanFile , bgc = (140 , 100, 25)) ; 


    # 1. clean Skin File
    rowSkinLabel = pm.rowColumnLayout ( nc = 1 , p = mainLayout , h = 20, w = 220 ) ;
    # pm.text ( label = '1. Clean Skin File' , p = rowSkinLabel ) ;
    pm.button ( 'butSkin' , label = '1. Clean Skin File' , h = 50 , w = 220, p = mainLayout , c = cleanFile , bgc = (0.973 , 0.781 , 0.711 )) ; 

    # 2. clean Dtl File
    rowDtlLabel = pm.rowColumnLayout ( nc = 1 , p = mainLayout , h = 20, w = 220 ) ;
    # pm.text ( label = '2. Clean Dtl File' , p = rowDtlLabel ) ;
    pm.button ( 'butDtl' , label = '2. Clean Dtl File' , h = 50 , w = 220, p = mainLayout , c = cleanDtlFile , bgc = (0 , 122, 190 )) ; 

    # 3. clean Bsn File
    rowBshLabel = pm.rowColumnLayout ( nc = 1 , p = mainLayout , h = 20, w = 220) ;
    # pm.text ( label = '3. Clean Bsh File' , p = rowBshLabel ) ;
    pm.button ( 'butBsh' , label = '3. Clean Bsh File' , h = 50 , w = 220, p = mainLayout , c = cleanBshFile , bgc = (0.900 , 0.700, 0.252)) ; 

    # 4. clean Bsn File
    rowSqLabel = pm.rowColumnLayout ( nc = 1 , p = mainLayout , h = 20, w = 220) ;
    # pm.text ( label = '3. Clean Bsh File' , p = rowBshLabel ) ;
    pm.button ( 'butSq' , label = '4. Clean FacialSq File' , h = 50 , w = 220, p = mainLayout , c = cleanFacialSqFile , bgc = (0.960, 0.711, 0.973)) ; 

# roll 2 Column
    rowCombRigLabel1 = pm.rowColumnLayout ( nc = 1 , p = mainLayout , h = 20 ) ;
    rowColumn2 = pm.rowColumnLayout ( nc = 2 , p = mainLayout , w = 220 , cw = [ ( 1 , 110 ) , ( 2 , 110 )] ) ;

    # 4.4 Connect Dtl
    rowCombRigLabel2 = pm.rowColumnLayout ( nc = 1 , p = mainLayout , h = 2, w = 220 ) ;
    pm.button ( 'connectDtl' , label = '5.1 Connect Dtl' , h = 25 , w = 110, p = rowColumn2 , c = connectDtlFile , bgc = (0.700 , 0.500, 0.900)) ; 

    # 4.2. Connect Bsn
    rowCombRigLabel3 = pm.rowColumnLayout ( nc = 1 , p = mainLayout , h = 2, w = 220 ) ;
    pm.button ( 'connectBsn' , label = '5.2 Connect Bsn' , h = 25 , w = 110, p = rowColumn2 , c = connectBsnFile , bgc = (0.400 , 0.900, 0.500)) ; 
 
 # roll 4 Column  
    rowColumn3 = pm.rowColumnLayout ( nc = 2 , p = mainLayout , w = 220 , cw = [ ( 1 , 110 ) , ( 2 , 110 )] ) ;

    # 4.4 Connect Dtl
    rowCombRigLabel3 = pm.rowColumnLayout ( nc = 1 , p = mainLayout , h = 5, w = 220 ) ;
    pm.button ( 'connectFaceialSq' , label = '5.3 Connect FacialSq' , h = 25 , w = 110, p = rowColumn3 , c = connectFacialSqFile , bgc = (0.922 , 0.863, 0.044)) ; 

    # 4.2. Connect Bsn
    rowCombRigLabel4 = pm.rowColumnLayout ( nc = 1 , p = mainLayout , h = 5, w = 220 ) ;
    pm.button ( 'connectBsn' , label = '5.4 Connect EyeLid' , h = 25 , w = 110, p = rowColumn3 , c = disconEyeLid , bgc = (0.356 , 0.730, 0.882)) ;  
   
    rowCombRigLabelDn = pm.rowColumnLayout ( nc = 1 , p = mainLayout ) ;
    # 5 Clean CombRig File'
    rowCombRigLabel6 = pm.rowColumnLayout ( nc = 1 , p = rowCombRigLabelDn , h = 10, w = 220 ) ;
    pm.button ( 'cleanCombRig' , label = '6. Clean CombRig File' , h = 50 , w = 220, p = rowCombRigLabelDn , c = cleanFileAndLockEyeLid , bgc = (140 , 100, 25)) ; 
    
    window.show () ;

windowUI ( ) ;