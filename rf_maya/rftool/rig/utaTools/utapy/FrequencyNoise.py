import maya.cmds as mc

def frequencyNoise(ctrl = [],frequencyValuse = 0.1):
	if ctrl == '':
		sels = mc.ls(sl = True)
		ctrl = sels

	for i in range(len(ctrl)):
	    names = ctrl[i].split('_')[0]
	    mc.addAttr(ctrl[i] ,ln = 'frequency',at = 'double', dv = 0 , k = True )
	    CtrlOfstGrp = mc.group(ctrl[i], n = '%sOfst_Grp' %names)
	    NoiNs = mc.createNode('noise', n = '%s_Noi' %names)
	    mc.connectAttr('time1.outTime','%s.time' %NoiNs)
	    mc.setAttr('%s.frequency' %NoiNs, 0)
	    
	    RempNd = mc.createNode('remapValue', n = '%s_rmp' %names)
	    mc.connectAttr('%s.outColor' %NoiNs,'%s.value[0]' %RempNd)
	    
	    MdvNdv = mc.createNode('multiplyDivide', n = '%s_mdv' %names)
	    mc.connectAttr('%s.value[0].value_Position' %RempNd ,'%s.i1x' %MdvNdv)
	    
	    mc.connectAttr('%s.o.ox' %MdvNdv,'%s.tx' %CtrlOfstGrp)
	    mc.connectAttr('%s.frequency' %ctrl[i],'%s.i2x' %MdvNdv)
	    mc.setAttr('%s.frequency' %ctrl[i] , frequencyValuse)