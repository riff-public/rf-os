# import os,sys,traceback
# sys.path.append(r"O:\Pipeline\legacy\lib\local\utaTools\utapy")
import maya.cmds as mc;
import pymel.core as pm;
# from utaTools.utapy import jointOnCurve5 as joc 
# reload(joc)
# from utaTools.utapy import createJoint as cj
# reload(cj)
# from utaTools.utapy import snapPaths as sp 
# reload(sp)
# from utaTools.utapy import getNameSpace as gns 
# reload(gns)
# from utaTools.utapy import tailControlRotate as ctr 
# reload(ctr)
# from utaTools.utapy import locatorSnap as ls
# reload(ls)
# from rftool.utils import maya_utils as mu
# reload(mu)
from utaTools.utapy import importExportAnim as iea 
reload(iea)

def ExportAnimUi(*args):
    imExportNodeField = pm.textField ( "txtImExportNodeField" , q  = True , text = True  )
    imExportPathField = pm.textField ( "txtImExportPathField" , q  = True , text = True  )
    iea.export_anim(node =imExportNodeField, path=imExportPathField)
def ImportAnimUi(*args):
    imExportNodeField = pm.textField ( "txtImExportNodeField" , q  = True , text = True  )
    imExportPathField = pm.textField ( "txtImExportPathField" , q  = True , text = True  )
    iea.import_anim(node =imExportNodeField, path=imExportPathField)
   
# UI ---------------------------------------------------------------------------------------------------
# UI ---------------------------------------------------------------------------------------------------
# UI ---------------------------------------------------------------------------------------------------
# UI ---------------------------------------------------------------------------------------------------
# UI ---------------------------------------------------------------------------------------------------
def ImportExportanimUI2() :

    # check if window exists
    if pm.window ( 'ImExportUI' , exists = True ) :
        pm.deleteUI ( 'ImExportUI' ) ;
    title = "Im-ExportAnim v1.0 (23-02-2018)" ;
    # create window 
    window = pm.window ( 'ImExportUI', 
                            title = title , 
                            width = 100,
                            mnb = True , 
                            mxb = True , 
                            sizeable = True , 
                            rtf = True ) ;
    windowRowColumnLayout = pm.window ( 'ImExportUI', 
                            e = True , 
                            width = 150);

    # motionTab A-------------------------------------------------------------------------------------------------------------------------------    
    mainMotionTab = pm.tabLayout ( innerMarginWidth = 1 , innerMarginHeight = 1 , p = windowRowColumnLayout ) ;

    MotionTab_A = pm.rowColumnLayout ( "Import - Export", nc = 1 , p = mainMotionTab ) ;
    mainMotionPathTab = pm.rowColumnLayout ( nc = 1 , p = MotionTab_A , w = 200) ;

    motionPathsRowLine = pm.rowColumnLayout ( nc = 1 , p = mainMotionPathTab ) ;

    rowLine_TabA = pm.rowColumnLayout ( nc = 1 , p = motionPathsRowLine ) ;
    mc.tabLayout(p=rowLine_TabA, w= 192)

    # Control Tab ------------------------------------------------------
    # Control Tab ------------------------------------------------------

        # line 1 
    rowLine1 = pm.rowColumnLayout ( nc = 3 , p = motionPathsRowLine , h = 25 , cw = [(1, 10), (2, 180),(3, 10 ) ]);
    pm.rowColumnLayout (p = rowLine1 ,h=8) ;
    pm.text( label='Import And Export Animations' , p = rowLine1, al = "center")  
    pm.rowColumnLayout (p = rowLine1 ,h=8) ;

    rowLine2 = pm.rowColumnLayout ( nc = 7 , p = motionPathsRowLine , h = 30 , cw = [(1, 5), (2, 30),(3, 5),(4,120),(5,5),(6,30),(7,5) ]) ;
    pm.rowColumnLayout (p = rowLine2 ,h=8) ;
    pm.text( label='Name:' , p = rowLine2, al = "left")
    pm.rowColumnLayout (p = rowLine2 ,h=8) ;
    imExportNodeField = pm.textField( "txtImExportNodeField", text = "text", p = rowLine2 , editable = True )
    pm.rowColumnLayout (p = rowLine2 ,h=8) ;
    pm.text( label='.anim' , p = rowLine2, al = "left")
    pm.rowColumnLayout (p = rowLine2 ,h=8) ;


    rowLine3 = pm.rowColumnLayout ( nc = 7 , p = motionPathsRowLine , h = 30 , cw = [(1, 5), (2, 30),(3, 5),(4,120),(5,5),(6,30),(7,5) ]) ;
    pm.rowColumnLayout (p = rowLine3 ,h=8) ;
    pm.text( label='Path :' , p = rowLine3, al = "left")
    pm.rowColumnLayout (p = rowLine3 ,h=8) ;
    imExportPathField = pm.textField( "txtImExportPathField", text = "text", p = rowLine3 , editable = True )
    pm.rowColumnLayout (p = rowLine3 ,h=8) ;
    pm.text( label='File' , p = rowLine3, al = "left")
    pm.rowColumnLayout (p = rowLine3 ,h=8) ;
    
    rowLine4 = pm.rowColumnLayout ( nc = 4 , p = motionPathsRowLine , h = 25 , cw = [(1, 5), (2,35),(3,150),(4,5)]) ;
    pm.rowColumnLayout (p = rowLine4 ,h=8) ;
    pm.text( label='Library:' , p = rowLine4, al = "left")
    mc.radioButtonGrp(la2=('Off', 'On'), nrb=2,select=1, p = rowLine4,columnAttach=[(1, "right",60)]  )
    pm.rowColumnLayout (p = rowLine4 ,h=8) ;
    
    rowLine5 = pm.rowColumnLayout ( nc = 3 , p = motionPathsRowLine , h = 30 , cw = [(1, 10), (2, 180), (3, 10) ]) ;
    pm.rowColumnLayout (p = rowLine5 ,h=8) ;
    pm.button ('exportAnimTxt', label = " Exports" ,h=25, p = rowLine5 , c = ExportAnimUi, bgc = (0.411765, 0.411765, 0.411765)) ;
    pm.rowColumnLayout (p = rowLine5 ,h=8) ;

    rowLine6 = pm.rowColumnLayout ( nc = 3 , p = motionPathsRowLine , h = 30 , cw = [(1, 10), (2, 180), (3, 10) ]) ;
    pm.rowColumnLayout (p = rowLine6 ,h=8) ;
    pm.button ('importAnimTxt', label = " Import" ,h=25, p = rowLine6 , c = ImportAnimUi, bgc = (0.411765, 0.411765, 0.411765)) ;
    pm.rowColumnLayout (p = rowLine6 ,h=8) ;

    # TabB
    rowLine_TabB = pm.rowColumnLayout ( nc = 1 , p = motionPathsRowLine ) ;
    mc.tabLayout(p=rowLine_TabB, w= 200)

    rowLine7 = pm.rowColumnLayout ( nc = 3 , p = motionPathsRowLine , h = 20 , cw = [(1, 10), (2, 180),(3, 10 ) ]);
    pm.rowColumnLayout (p = rowLine7 ,h=8) ;
    pm.text( label='Key Animtion Check' , p = rowLine7, al = "center")  
    pm.rowColumnLayout (p = rowLine7 ,h=8) ;
    
    rowLine8 = pm.rowColumnLayout ( nc = 3 , p = motionPathsRowLine , h = 30 , cw = [(1, 10), (2, 180),(3, 10 ) ]);
    pm.rowColumnLayout (p = rowLine8 ,h=8) ;
    pm.optionMenu( label='' , p = rowLine8)
    pm.menuItem( label='Spine' )
    pm.menuItem( label='Head' )
    pm.menuItem( label='Arm' )
    pm.menuItem( label='Leg' )
    pm.rowColumnLayout (p = rowLine8 ,h=8) ;
    

    
    rowLine9 = pm.rowColumnLayout ( nc = 5 , p = motionPathsRowLine , h = 30 , cw = [(1, 10),(2, 85),(3, 10),(4,85),(5,10)]) ;
    pm.rowColumnLayout (p = rowLine9 ,h=8) ;
    pm.button ('InsertAnimatorLoop', label = "Insert" ,h=25, p = rowLine9 , c = ExportAnimUi, bgc = (0.411765, 0.411765, 0.411765)) ;
    pm.rowColumnLayout (p = rowLine9 ,h=8) ;
    pm.button ('exportAnimTxt3', label = "Clear" ,h=25, p = rowLine9 , c = ExportAnimUi, bgc = (0.411765, 0.411765, 0.411765)) ;
    pm.rowColumnLayout (p = rowLine9 ,h=8) ;
    
    # TabC
    rowLine_TabB = pm.rowColumnLayout ( nc = 1 , p = motionPathsRowLine ) ;
    mc.tabLayout(p=rowLine_TabB, w= 200)

    pm.setParent("..")
    pm.showWindow(window)
