import maya.cmds as mc
import maya.mel as mm

import lpRig.core as lrc
reload(lrc)
import lpRig.rigTools as lrr
reload(lrr)
import DtlGrpHiMidLowNakee as DGHML 
reload(DGHML)

def createDetailRigToSelected(part='DtlRig'):

	sels = mc.ls(sl=True)
	ctrlGrpNm = '%sCtrl_Grp' % part
	skinGrpNm = '%sSkin_Grp' % part

	ctrlGrp = None
	if not mc.objExists(ctrlGrpNm):
		ctrlGrp = lrc.Null()
		ctrlGrp.name = ctrlGrpNm
	else:
		ctrlGrp = lrc.Dag(ctrlGrpNm)

	skinGrp = None
	if not mc.objExists(skinGrpNm):
		skinGrp = lrc.Null()
		skinGrp.name = skinGrpNm
	else:
		skinGrp = lrc.Dag(skinGrpNm)
	
	for sel in sels:
		
		ndNm, ndIdx, ndSide, ndType = lrr.extractName(sel)

		dtlRigObj = DetailRig(sel, ndNm, ndIdx.replace('_', ''), ndSide.replace('_', ''))

		dtlRigObj.CtrlZrGrp.parent(ctrlGrp)
		dtlRigObj.JntZrGrp.parent(skinGrp)

	DGHML.DtlGrpHiMidLow()

class DetailRig(object):

	def __init__(
					self,
					tmpLoc='',
					part='',
					idx='',
					side=''
				):

		if side:
			side = '_%s_' % side
		else:
			side = '_'

		if idx:
			idx = '_%s' % idx

		col = 'yellow'
		if side == '_L_':
			col = 'red'
		elif side == '_R_':
			col = 'blue'

		self.Jnt = lrc.Joint()
		self.Jnt.name = '{part}{idx}{side}Jnt'.format(part=part, idx=idx, side=side)

		self.JntZrGrp = lrc.group(self.Jnt)
		self.JntZrGrp.name = '{part}JntZr{idx}{side}Grp'.format(part=part, idx=idx, side=side)

		self.JntZrGrp.snap(tmpLoc)

		self.Ctrl = lrc.Control('cube')
		self.Ctrl.name = '{part}{idx}{side}Ctrl'.format(part=part, idx=idx, side=side)
		self.Ctrl.lockHideAttrs('v')
		self.Ctrl.color = col

		self.CtrlInvGrp = lrc.group(self.Ctrl)
		self.CtrlInvGrp.name = '{part}CtrlInv{idx}{side}Grp'.format(part=part, idx=idx, side=side)

		self.CtrlZrGrp = lrc.group(self.CtrlInvGrp)
		self.CtrlZrGrp.name = '{part}CtrlZr{idx}{side}Grp'.format(part=part, idx=idx, side=side)

		self.CtrlZrGrp.snap(tmpLoc)

		for attr in ('t', 'r', 's'):
			self.Ctrl.attr(attr) >> self.Jnt.attr(attr)
