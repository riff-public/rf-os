import sys
sys.path.append( r'P:\lib\local')
sys.path.append( r'P:\SevenChicks\all\asset\char\main\BlueChick\rig\data')

import ppmel.core as core
import maya.cmds as mc
import maya.mel as mm

# reload(core)
# reload(mc)
# reload(mm)

#edit Dtl Group
def addGrpToRbn ( elem = '' , numberDtl = 1 ) :

#add Group at dtlCtrl
    dtlRbnCtrl = ('%s%sRbnDtl_ctrl' %(elem, numberDtl))
    dtlRbnTwist = ('%s%sRbnDtlCtrlTwist_grp' %(elem,numberDtl))
    dtlNull = mc.group(em=True)
    dtlRbnOfst = mc.rename(dtlNull,'%s%sRbnDtlCtrlOfst_grp' %(elem,numberDtl))
    core.setLock( dtlRbnOfst , False , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v')
    core.parent(dtlRbnOfst, '%s%sRbnDtlCtrlTwist_grp' %(elem,numberDtl))
    if dtlRbnCtrl :
        core.snap( dtlRbnCtrl , dtlRbnOfst) 
    core.parent('%s%sRbnDtl_ctrl' %(elem,numberDtl), dtlRbnOfst)

#unlock Group on mainRbn
    rbnRoot = core.setLock('%sRbn_grp' %elem, False ,'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v') 
    core.setLock('%sRbnRootCtrlZro_grp' %elem, False ,'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v') 
    core.setLock('%sRbnEndCtrlZro_grp' %elem, False ,'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v') 

#snap to tmpJnt
    core.snap('%s_tmpJnt' %elem , '%sRbn_grp' %elem)

#----------------------------------------------------------------------------
#edit Dtl Group
def addGrpToRbn2 ( elem = '', numberDtl = 1 , side = '') :

#add Group at dtlCtrl
    dtlRbnCtrl = ('%s_animaRbnDtl%s%s_ctrl' %(elem, numberDtl, side))
    dtlRbnTwist = ('%s_animaRbnDtl%sTwst%s_grp' %(elem,numberDtl, side))
    dtlNull = mc.group(em=True)
    dtlRbnOfst = mc.rename(dtlNull,'%s_animaRbnDtl%sOfst%s_grp' %(elem,numberDtl, side))
    core.setLock( dtlRbnOfst , False , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v')
    core.parent(dtlRbnOfst, dtlRbnTwist)
    if dtlRbnCtrl :
        core.snap( dtlRbnCtrl , dtlRbnOfst) 
    core.parent(dtlRbnCtrl, dtlRbnOfst)

#unlock Group on mainRbn
    rbnRoot = core.setLock('%s_animaRbnAnim%s_grp' %(elem, side), False ,'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v') 
    core.setLock('%s_animaRbnRootCtrlZro%s_grp' %(elem, side), False ,'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v') 
    core.setLock('%s_animaRbnEndCtrlZro%s_grp' %(elem, side), False ,'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v') 

#snap to tmpJnt
    # core.snap('%s_tmpJnt' %elem , '%sRbn_grp' %elem)

#----------------------------------------------------------------------------
#edit Dtl Group
def addGrpToRbnSpider ( elem = '', numberDtl = 5, side = '') :

#add Group at dtlCtrl

    for numberDtl in range(1, 6):
        # print "We're on time %d" % (x)
        dtlRbnCtrl = ('Rbn%sDtl_%s_%s_Ctrl' %(elem, numberDtl, side))
        dtlRbnTwist = ('Rbn%sDtl_%s_%s_Tws' %(elem,numberDtl, side))
        dtlNull = mc.group(em=True)
        dtlRbnOfst = mc.rename(dtlNull,'Rbn%sDtl_%s_%s_Ofst_Grp' %(elem,numberDtl, side))
        core.setLock( dtlRbnOfst , False , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v')
        core.parent(dtlRbnOfst, dtlRbnTwist)
        if dtlRbnCtrl :
            core.snap( dtlRbnCtrl , dtlRbnOfst) 
        core.parent(dtlRbnCtrl, dtlRbnOfst)

# #unlock Group on mainRbn
#     rbnRoot = core.setLock('%s_animaRbnAnim%s_grp' %(elem, side), False ,'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v') 
#     core.setLock('%s_animaRbnRootCtrlZro%s_grp' %(elem, side), False ,'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v') 
#     core.setLock('%s_animaRbnEndCtrlZro%s_grp' %(elem, side), False ,'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v') 

#snap to tmpJnt
    # core.snap('%s_tmpJnt' %elem , '%sRbn_grp' %elem)


def setAttrRbbSpider(*args):
# UpLegFnt L
    # UpLegFntDtl_2_L
    mc.setAttr('%s.tx' % 'RbnUpLegFntDtl_2_L_Ofst_Grp', 5.22)
    mc.setAttr('%s.tz' % 'RbnUpLegFntDtl_2_L_Ofst_Grp', 1.656)
    # UpLegFntDtl_3_L
    mc.setAttr('%s.tx' % 'RbnUpLegFntDtl_3_L_Ofst_Grp', 5.652)
    mc.setAttr('%s.tz' % 'RbnUpLegFntDtl_3_L_Ofst_Grp', 7.702)
    # UpLegFntDtl_4_L
    mc.setAttr('%s.tx' % 'RbnUpLegFntDtl_4_L_Ofst_Grp', 1.901)
    mc.setAttr('%s.tz' % 'RbnUpLegFntDtl_4_L_Ofst_Grp', 7.18)
# UpLegFnt R
    # UpLegFntDtl_2_R
    mc.setAttr('%s.tx' % 'RbnUpLegFntDtl_2_R_Ofst_Grp', 5.22)
    mc.setAttr('%s.tz' % 'RbnUpLegFntDtl_2_R_Ofst_Grp', -1.656)
    # UpLegFntDtl_3_R
    mc.setAttr('%s.tx' % 'RbnUpLegFntDtl_3_R_Ofst_Grp', 5.652)
    mc.setAttr('%s.tz' % 'RbnUpLegFntDtl_3_R_Ofst_Grp', -7.702)
    # UpLegFntDtl_4_R
    mc.setAttr('%s.tx' % 'RbnUpLegFntDtl_4_R_Ofst_Grp', 1.901)
    mc.setAttr('%s.tz' % 'RbnUpLegFntDtl_4_R_Ofst_Grp', -7.18)

# MidLegFnt L
    # MidLegFntDtl_2_L
    mc.setAttr('%s.tx' % 'RbnMidLegFntDtl_2_L_Ofst_Grp', 2.386)
    mc.setAttr('%s.tz' % 'RbnMidLegFntDtl_2_L_Ofst_Grp', -2.935)
    # MidLegFntDtl_3_L
    mc.setAttr('%s.tx' % 'RbnMidLegFntDtl_3_L_Ofst_Grp', 2.846)
    mc.setAttr('%s.tz' % 'RbnMidLegFntDtl_3_L_Ofst_Grp', -1.135)
    # MidLegFntDtl_4_L
    mc.setAttr('%s.tx' % 'RbnMidLegFntDtl_4_L_Ofst_Grp', 1.712)
    mc.setAttr('%s.tz' % 'RbnMidLegFntDtl_4_L_Ofst_Grp', -1.258)

# MidLegFnt R
    # MidLegFntDtl_2_R
    mc.setAttr('%s.tx' % 'RbnMidLegFntDtl_2_R_Ofst_Grp', 2.386)
    mc.setAttr('%s.tz' % 'RbnMidLegFntDtl_2_R_Ofst_Grp', 2.935)
    # MidLegFntDtl_3_L
    mc.setAttr('%s.tx' % 'RbnMidLegFntDtl_3_R_Ofst_Grp', 2.846)
    mc.setAttr('%s.tz' % 'RbnMidLegFntDtl_3_R_Ofst_Grp', 1.135)
    # MidLegFntDtl_4_L
    mc.setAttr('%s.tx' % 'RbnMidLegFntDtl_4_R_Ofst_Grp', 1.712)
    mc.setAttr('%s.tz' % 'RbnMidLegFntDtl_4_R_Ofst_Grp', 1.258)

# UpLegMidA L
    # UpLegMidADtl_2_L
    mc.setAttr('%s.tz' % 'RbnUpLegMidADtl_2_L_Ofst_Grp', -1.079)
    mc.setAttr('%s.tz' % 'RbnUpLegMidADtl_3_L_Ofst_Grp', -0.465)

# UpLegMidA R
    mc.setAttr('%s.tz' % 'RbnUpLegMidADtl_2_R_Ofst_Grp', 1.079)
    mc.setAttr('%s.tz' % 'RbnUpLegMidADtl_3_R_Ofst_Grp', 0.465)


# UpLegMidB L
    # UpLegMidBDtl_2_L
    mc.setAttr('%s.tz' % 'RbnUpLegMidBDtl_2_L_Ofst_Grp', -2.487)
    mc.setAttr('%s.tz' % 'RbnUpLegMidBDtl_3_L_Ofst_Grp', -1.777)
    mc.setAttr('%s.tz' % 'RbnUpLegMidBDtl_4_L_Ofst_Grp', -0.872)
# UpLegMidA R
    mc.setAttr('%s.tz' % 'RbnUpLegMidBDtl_2_R_Ofst_Grp', 2.487)
    mc.setAttr('%s.tz' % 'RbnUpLegMidBDtl_3_R_Ofst_Grp', 1.777)
    mc.setAttr('%s.tz' % 'RbnUpLegMidBDtl_4_R_Ofst_Grp', 0.872)


# UpLegBck L
    # UpLegBckDtl_2_L
    mc.setAttr('%s.tz' % 'RbnUpLegBckDtl_2_L_Ofst_Grp', -1.338)
    mc.setAttr('%s.tz' % 'RbnUpLegBckDtl_3_L_Ofst_Grp', -0.978)

# UpLegMidA R
    mc.setAttr('%s.tz' % 'RbnUpLegBckDtl_2_R_Ofst_Grp', 1.338)
    mc.setAttr('%s.tz' % 'RbnUpLegBckDtl_3_R_Ofst_Grp', 0.978)



# #unlock Group on mainRbn
#     # UpLegFnt L
#     core.setLock('RbnUpLegFntDtl_2_L_Ofst_Grp', False ,'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v') 
#     core.setLock('RbnUpLegFntDtl_3_L_Ofst_Grp', False ,'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v') 
#     core.setLock('RbnUpLegFntDtl_4_L_Ofst_Grp', False ,'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v') 
#     # UpLegFnt R
#     core.setLock('RbnUpLegFntDtl_2_R_Ofst_Grp', False ,'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v') 
#     core.setLock('RbnUpLegFntDtl_3_R_Ofst_Grp', False ,'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v') 
#     core.setLock('RbnUpLegFntDtl_4_R_Ofst_Grp', False ,'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v') 

#     # UpLegMidA L
#     core.setLock('RbnUpLegMidADtl_2_L_Ofst_Grp', False ,'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v') 
#     core.setLock('RbnUpLegMidADtl_3_L_Ofst_Grp', False ,'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v') 
#     # UpLegMidA R
#     core.setLock('RbnUpLegMidADtl_2_R_Ofst_Grp', False ,'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v') 
#     core.setLock('RbnUpLegMidADtl_3_R_Ofst_Grp', False ,'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v') 


#     # UpLegMidB L
#     core.setLock('RbnUpLegMidBDtl_2_L_Ofst_Grp', False ,'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v') 
#     core.setLock('RbnUpLegMidBDtl_3_L_Ofst_Grp', False ,'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v') 
#     # UpLegMidB R
#     core.setLock('RbnUpLegMidBDtl_2_R_Ofst_Grp', False ,'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v') 
#     core.setLock('RbnUpLegMidBDtl_3_R_Ofst_Grp', False ,'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v') 


#     # UpLegBck L
#     core.setLock('RbnUpLegBckDtl_2_L_Ofst_Grp', False ,'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v') 
#     core.setLock('RbnUpLegBckDtl_3_L_Ofst_Grp', False ,'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v') 
#     # UpLegBck R
#     core.setLock('RbnUpLegBckDtl_2_R_Ofst_Grp', False ,'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v') 
#     core.setLock('RbnUpLegBckDtl_3_R_Ofst_Grp', False ,'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v') 
