import maya.cmds as mc
from utaTools.pkmel import weightTools
reload(weightTools)
import maya.cmds as mc
from utaTools.utapy import utaCore
reload (utaCore)
from lpRig import rigTools as lrr
reload(lrr)


def MuscleConnectUI():

    windowID = 'MuscleConnectUI'
    
    
    if mc.window(windowID, exists=True):
        mc.deleteUI(windowID)
    winWidth = 230
    winHight = 100
    
    #SCAN SCENE FOR MscNode_Grp
    scanList=mc.ls('*:*MscNode*_Grp')
    Childs = []
    
    for each in scanList:
        Name  = each.split(':')[-1]
        Childs.append(Name)
    
    mc.window(windowID , title = 'Muscle Connect' , width = winWidth , height = winHight , sizeable = True )
    mainCL = mc.columnLayout('columnLayoutName01')
    utaCore.wintes(window = 'MuscleConnectUI' , path = 'D:/' , pngIcon = 'MuscleConnectIcon.png')
    mc.rowLayout(numberOfColumns=1 , h=35)
    mc.button('copyWeight',label = 'Copy Weights to Pos_Geo' , w=winWidth , h = 30 , c= copySkinPosGeo )
    mc.setParent('..')
    
    mc.rowLayout(numberOfColumns=1, h=35)
    mc.button('addBshCollide',label = 'Add Bsh From Collide_Geo' , w=winWidth , h = 30 , c = addBshCollideGeo)

    mc.setParent('..')
    mc.rowLayout(numberOfColumns=1 , h=20)
    mc.text(label = 'MscNode_Grp :')
    mc.setParent('..')
    mc.rowLayout(numberOfColumns=1 , h=140)
    mc.textScrollList( 'MscNodeInScene' ,w=winWidth  , numberOfRows=8, allowMultiSelection=True,
                append=Childs,
                showIndexedItem=4, 
                h=130)
    mc.setParent('..')              
    mc.rowLayout(numberOfColumns=2)
    mc.button('Parent+Scale Constraint'  , w=0.75*winWidth , h = 30 ,c=parentScaleNode)
    mc.button('Refresh'  , w=0.25*winWidth , h = 30,bgc=[1,.85,0] , c = refreshLayout)
    mc.setParent('..')
    
    mc.showWindow()


def copySkinPosGeo(*args):
    #CopySkinWeight
    PosGeo = mc.ls('*:*PosAddRig_Geo')
    for each in PosGeo:
        PosSplit = each.split(':')[-1]
        GeoName = PosSplit.replace('PosAddRig_Geo' , '_Geo')
        mc.select(['bodyRig_md:{}'.format(GeoName)] , each)
        weightTools.copySelectedWeight()

def addBshCollideGeo(*args):
    #BlendShape Stage
    CollideGeo = mc.ls('*:*CollideAddRig_Geo')
    for each in CollideGeo:
        CollideSplit = each.split(':')[-1]
        GeoName = CollideSplit.replace('CollideAddRig_Geo' , '_Geo')
        mc.select( each , ['bodyRig_md:{}'.format(GeoName)] )
        lrr.doAddBlendShape()

def parentScaleNode(*args):
    Leader = mc.ls(sl=True)
    if Leader == [] :
        print "! ! ! PLEASE SELECT AN OBJ TO PARENT & SCALE CONSTRAINT ! ! !"
    else:
        
        selectList = mc.textScrollList( 'MscNodeInScene' ,q=True ,si=True)

        Childs=[]
        for each in selectList:
            name = 'addRig_md:'+ each
            Childs.append(name)
        for each in Childs:
            mc.parentConstraint(Leader , each , mo=True)
            mc.scaleConstraint(Leader , each , mo=True)

def refreshLayout(*args):
    scanList=mc.ls('*:*MscNode*_Grp')
    if scanList == []:
        print "! ! ! No MscNode_Grp In This Scene ! ! !"
    else :
        Childs = []
        
        for each in scanList:
            Name  = each.split(':')[-1]
            Childs.append(Name)
    
        mc.textScrollList( 'MscNodeInScene' , e=True , ra=True)
        mc.textScrollList( 'MscNodeInScene' , e=True , append=Childs)
    

