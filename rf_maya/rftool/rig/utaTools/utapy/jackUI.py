import maya.cmds as mc

def jackUI(*args):
    windowID = 'Create_SmearLattice_UI'


    if mc.window(windowID, exists=True):
    	mc.deleteUI(windowID)

    winWidth = 250
    tmpWidth = [winWidth*0.4, winWidth*0.6]
    tmpWidth3 = [winWidth*0.2, winWidth*0.5, winWidth*0.3] 
    tmpWidth4 = [winWidth*0.4,winWidth*0.2,winWidth*0.2,winWidth*0.2]
    clmWidth4 = [winWidth*0.4,winWidth*0.2,winWidth*0.2,winWidth*0.2]
    mc.window(windowID , title = 'CreateLttSmear' , width = winWidth)
    mainCL = mc.columnLayout()
    mc.text(label = '1.Select Geo', align='left' )
    mc.button('MakeLatticefromSel' , c = Prepare_cmd)
    mc.text(label = '2.SelectLatticePointINORDER!', align='left' )
    mc.text(label = '  Name', align='left' )
    NameValue = mc.textField('Name' , ed = True )
    mc.text(label = 'Side', align='left' )
    SideValue = mc.textField('Side' , ed = True)

    mc.text(label = 'Example = [GeoName]Ltt[Name](i)_[Side]_Jnt', align='left' )
    mc.button('Create Jnt+Ctrl' , c = CreateJntCtrl_cmd)


    mc.text(label = '3.Select Lattice > Choose > Execute' , align='left')
    mc.radioCollection('WireOrBind')
    mc.radioButton('WireDeform' ,  label='WireDeform',select=True )
    mc.radioButton( 'BindSkin' , label='BindSkin' )
    mc.button('Execute' , c = WireBind_cmd)

    mc.text(label = '4.vvv This Button will create AddRig_Grp and organize all groups' , align='left')
    mc.text(label = '   vv' , align='left')
    mc.text(label = '   v ' , align='left')
    mc.text(label = '   v ' , align='left')
    mc.text(label = '   v ' , align='left')
    mc.button('Organize' , c=Organize_cmd)
    mc.showWindow()


def WireBind_cmd(*args):
    if mc.radioButton('WireDeform' , q=True , select=True):
        WireDeform_cmd()
    elif mc.radioButton( 'BindSkin' ,q=True , select=True  ):
        BindSkin_cmd()
        


def Prepare_cmd(*args):
    #===================================================================#
    #====================Create Lattice from Sel========================#
    #===================================================================#
    LttGeo = mc.ls(sl=True)

    nameAll = LttGeo[0].split('_')    
    name = nameAll[0].split('AddRig')
    GeoProcSp = nameAll[0].split(name[0])

    GeoName = name[0]
    GeoProc = GeoProcSp[-1]
    GeoType = nameAll[-1]
    # print GeoName,  GeoType,  GeoProc, 'll'

    Crv = mc.lattice(LttGeo[0],divisions=(4,3,4),oc=True,n='%sLtt%s_%s'%(GeoName, GeoProc, GeoType))
    GeoAddRigGrp = mc.createNode('transform',n='%s%sGeo_Grp'%(GeoName, GeoProc))
    mc.parent(LttGeo[0],GeoAddRigGrp)
    
    
    if mc.objExists('LatticeGeo_Grp'):
        mc.parent(GeoAddRigGrp,'LatticeGeo_Grp')
        
    else:
        AddRigGeoGrp = mc.createNode('transform',n='LatticeGeo_Grp')
        mc.parent(GeoAddRigGrp,AddRigGeoGrp)

    
    LttBaseGrp = mc.group(em=True , n='%sLatticeBase_Grp'%(GeoName))
    for each in Crv:
        if 'Lattice' in each:
            mc.parent(each, LttBaseGrp)
        elif 'Base' in each:
            mc.parent(each, LttBaseGrp)
            mc.setAttr('%s.v' % each, 0)
        else:
            pass

    #===========================================================#
    #====================Create Base Grp========================#
    #===========================================================#
    
    
    if mc.objExists('LatticeElems_Grp'):
        mc.parent(LttBaseGrp,'LatticeElems_Grp')
        
    else:
        mc.group(em=True, name = 'LatticeElems_Grp')
        mc.parent(LttBaseGrp,'LatticeElems_Grp')
    
    
    if mc.objExists('LatticeJnt_Grp'):
        pass
    else:
        mc.group( em=True , n='LatticeJnt_Grp')
    
    
    if mc.objExists('LatticeCtrl_Grp'):
        pass
    else:
        mc.group( em=True , n='LatticeCtrl_Grp')
    
    
    if mc.objExists('LatticeWireCrv_Grp'):
        pass
    else:
        CrvG = mc.group( em=True , n= 'LatticeWireCrv_Grp')
        

def CreateJntCtrl_cmd(*args):
    
    #============================================================================================#
    #====================Create Jnts / JntsZro / Ctrls / CtrlsZro / Crvs ========================#
    #============================================================================================#
    
    Jlist = []

    name = mc.textField('Name' , q=True , text=True)
    side = mc.textField('Side' , q=True , text=True)
    sel = mc.ls(sl=True , fl=True)

    size = len(sel)
    
    GeoName , GeoType = sel[0].split('_')    
    JGrpSide = mc.group( em=True , n='%s%s%sJnt_Grp'%(GeoName,name,side))
    JzrGrplist = []
    CzrGrplist = []

    

    for i in range(0,size):
        #Create Joints Stage
        JzrGrp = mc.group( em=True , n='%sLtt%s%sJntZero_%s_Grp'%(GeoName,name,i+1,side))
        Jnt = mc.createNode('joint' , n='%sLtt%s%s_%s_Jnt'%(GeoName,name,i+1,side))
        mc.parent(Jnt , JzrGrp)
        pos = mc.xform(sel[i], q=True, ws=True, t=True)
        mc.move(pos[0],pos[1],pos[2], JzrGrp, absolute=True)
        JzrGrplist.append(JzrGrp)

        
        #Create Ctrls Stage
        Hilist = []
        C = mc.group(em=True , n='%sLtt%sCtrl%sZro_%s_Grp'%(GeoName,name,i+1,side))
        B = mc.group(C, n='%sLtt%sCtrlOffs%s_%s_Grp'%(GeoName,name,i+1,side))
        A = mc.group(B , n='%sLtt%sCtrl%s_%s_Grp'%(GeoName,name,i+1,side))
        mc.curve( p =[(-0.5, 0.5, 0.5),( -0.5, -0.5, 0.5),( 0.5 ,-0.5, 0.5),( 0.5, 0.5, 0.5),( -0.5, 0.5, 0.5),( -0.5, 0.5, -0.5),(-0.5, -0.5, -0.5),(-0.5 ,-0.5, 0.5),( -0.5 ,0.5, 0.5),( -0.5, -0.5, 0.5),( 0.5,-0.5, 0.5),( 0.5, -0.5, -0.5),( 0.5, 0.5, -0.5),( 0.5, 0.5, 0.5),( 0.5, -0.5, 0.5),( 0.5, -0.5, -0.5), ( 0.5, 0.5, -0.5), (-0.5, 0.5, -0.5),( -0.5, -0.5, -0.5),( 0.5, -0.5, -0.5),( 0.5, 0.5, -0.5),(-0.5, 0.5, -0.5) ,(-0.5, 0.5, 0.5),(-0.5, -0.5, 0.5),(-0.5, -0.5, -0.5), (-0.5, 0.5, -0.5)] , d=1)
        mc.scale(.5,.5,.5)
        CT =mc.rename('%sLtt%s%s_%s_Ctrl'%(GeoName,name,i+1,side))
        mc.makeIdentity(apply = True)
        
        #========================Gimbal=============================
        
        #Gname , Gside , Gtype = CT.split('_')    
        #GB = mc.duplicate(CT,name='%s_%s_Gimbal'%(Gname,Gside) )
        #mc.scale( .75, .75, .75,GB )
        #mc.makeIdentity(apply = True)
        #mc.addAttr( '%sShape' %CT , sn = 'Gimbal' , k = True , at = 'float', dv=0 , min = 0 , max = 1)
        #shape = mc.listRelatives('%s'%CT) [0]
        #mc.connectAttr('%s.Gimbal'%shape , '%s.visibility'%GB[0])    
        #mc.parent(GB,CT)
        mc.parent(CT,C)
        mc.parentConstraint(Jnt,A , mo=False)
        mc.delete(A , cn=True)
        
        #===================Connect Jnt to Ctrl Attrs Stage===================
        
        #Translate
        mc.connectAttr('%s.translateX'%CT , '%s.translate.translateX'%Jnt)
        mc.connectAttr('%s.translateY'%CT , '%s.ty'%Jnt)
        mc.connectAttr('%s.translateZ'%CT, '%s.tz'%Jnt)
        #Rotate
        mc.connectAttr('%s.rotateX'%CT , '%s.rotate.rotateX'%Jnt)
        mc.connectAttr('%s.rotateY'%CT , '%s.ry'%Jnt)
        mc.connectAttr('%s.rotateZ'%CT, '%s.rz'%Jnt)
        #Scale
        mc.connectAttr('%s.scaleX'%CT , '%s.scale.scaleX'%Jnt)
        mc.connectAttr('%s.scaleY'%CT , '%s.sy'%Jnt)
        mc.connectAttr('%s.scaleZ'%CT, '%s.sz'%Jnt) 
        CzrGrplist.append(A)
    #=======================XForm Position To Create Curves=============================
    vertexPos= []
    crvDegree = 3
    pos = []
    #Get Positions
    for i in sel:
	    vertexPos = mc.xform (i ,query= True, worldSpace=True,translation=True  )
	    print vertexPos
	    pos.append(vertexPos) # add position of vtx selected ex. [(0,0,0),(1.545,2.322,3.10),(1,1,1)]

    #=======================Create Grp for Filter=============================
    ThisLttName , ThisLttType = sel[0].split('_')
    if mc.objExists('%sLttJnt_Grp'%(ThisLttName)):
        pass        
    else:
        mc.group(em=True ,n='%sLttJnt_Grp'%(ThisLttName))
    
    if mc.objExists('%sLttCtrl_Grp'%(ThisLttName)):
        pass        
    else:
        mc.group(em=True ,n='%sLttCtrl_Grp'%(ThisLttName))

    if mc.objExists('%sLttWireCrv_Grp'%(ThisLttName)):
        pass        
    else:
        mc.group(em=True ,n='%sLttWireCrv_Grp'%(ThisLttName))

    #create Curve
    VertCurve = mc.curve(d = crvDegree,p=pos , n='%s%s%sWire_Crv'%(GeoName,name,side))
    AllLttCtrlGrp = mc.group(em=True , n='%sLtt%sCtrl_Grp'%(ThisLttName,name))
    mc.parent(JzrGrplist,JGrpSide)
    mc.parent(CzrGrplist,AllLttCtrlGrp)
    mc.parent(AllLttCtrlGrp,'%sLttCtrl_Grp'%(ThisLttName))
    mc.parent(VertCurve , '%sLttWireCrv_Grp'%(ThisLttName))
    mc.parent(JGrpSide ,'%sLttJnt_Grp'%(ThisLttName) )

    #===================Check This Run Ctrl_Grp==========================#    


    listA = mc.listRelatives('LatticeCtrl_Grp')
    B= '%sLttCtrl_Grp'%(ThisLttName)

    if listA:
        if not B in listA:
            mc.parent(B,'LatticeCtrl_Grp')
        else:
            pass
    else:
        mc.parent(B,'LatticeCtrl_Grp')
        
        
    #===================Check This Run Jnt_Grp========================#


    listA = mc.listRelatives('LatticeJnt_Grp')
    B= '%sLttJnt_Grp'%(ThisLttName)

    if listA:
        if not B in listA:
            mc.parent(B,'LatticeJnt_Grp')
        else:
            pass
    else:
        mc.parent(B,'LatticeJnt_Grp')
    


    #===================Check This Run WireCrv_Grp========================#


    listA = mc.listRelatives('LatticeWireCrv_Grp')
    B= '%sLttWireCrv_Grp'%(ThisLttName)

    if listA:
        if not B in listA:
            mc.parent(B,'LatticeWireCrv_Grp')
        else:
            pass
    else:
        mc.parent(B,'LatticeWireCrv_Grp')

    #=================================================================#
    #=====================BindSkin Jnt To Crv=========================#
    #=================================================================#    
    JntThisRunList = []

    CheckGrp = mc.listRelatives ('%s%s%sJnt_Grp'%( GeoName,name,side) ,ad=True)
    print CheckGrp
    for i in CheckGrp:
        if '_Jnt' in i:
            JntThisRunList.append(i)
    print JntThisRunList
 

    mc.select(JntThisRunList)
    mc.select(VertCurve, add=True)

    mc.skinCluster()
    
def WireDeform_cmd(*args):
    #============================================#
    #=============Wire Deform Style==============#
    #============================================#
    sel = mc.ls(sl=True)
    LttName , LttType = sel[0].split('_')

    CrvList = []

    G = mc.listRelatives ('%sLttWireCrv_Grp'%(LttName) ,ad=True)
    print G
    for i in G:
        if '_Crv' in i:
            CrvList.append(i)

    mc.wire(sel, gw=0, en=1, li=0, w=CrvList)

 
def BindSkin_cmd(*args):
    #==========================================#
    #=============Bind Skin Style==============#
    #==========================================#
    sel = mc.ls(sl=True)
    LttName , LttType = sel[0].split('_')

    JntList = []

    G = mc.listRelatives ('%sLttJnt_Grp'%(LttName) ,ad=True)
    print G
    for i in G:
        if '_Jnt' in i:
            JntList.append(i)

    mc.select(sel)
    mc.select(JntList, add=True)

    mc.skinCluster()
    
    mc.delete('%sLttWireCrv_Grp'%(LttName))
    
def Organize_cmd(*args):
    #===================================================#
    #====================Check Grp======================#
    #===================================================#
    if mc.objExists('AddRigCtrl_Grp'):
        pass     
    else:
        mc.createNode('transform',n='AddRigCtrl_Grp')

    if mc.objExists('AddRigJnt_Grp'):
        pass     
    else:
        mc.createNode('transform',n='AddRigJnt_Grp')
    
    if mc.objExists('AddRigStill_Grp'):
        pass     
    else:
        mc.createNode('transform',n='AddRigStill_Grp')

    mc.parent( 'LatticeJnt_Grp' ,'AddRigJnt_Grp')
    mc.parent( 'LatticeCtrl_Grp' , 'AddRigCtrl_Grp')
    mc.parent( 'LatticeGeo_Grp' , 'AddRigStill_Grp')
    mc.parent( 'LatticeElems_Grp' , 'AddRigStill_Grp')
    mc.parent( 'LatticeWireCrv_Grp' , 'AddRigStill_Grp')
    mc.parent( 'AddRigJnt_Grp' , 'AddRigStill_Grp')