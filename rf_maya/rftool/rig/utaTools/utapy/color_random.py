import random ;

white = 16 ;
lightYellow = 21 ;
lightBlue = 18 ;
lightPink = 19 ;
skin = 20 ;
lightGreen = 22 ;
purple = 30 ;

color = [ white , lightYellow , lightBlue , lightPink , skin , lightGreen , purple ] ;

colorRandom = random.SystemRandom ( ) ;

import pymel.core as pm ;

selection = pm.ls ( sl = True ) ;

for each in selection : 
    curveShape = each + 'Shape' ;   
    curveShape = pm.general.PyNode ( curveShape ) ;
    curveShape.overrideEnabled.set ( 1 ) ;
    curveShape.overrideColor.set ( colorRandom.choice ( color ) ) ;