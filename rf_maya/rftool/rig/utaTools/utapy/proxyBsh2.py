import maya.cmds as mc
reload(mc)
geoSel = ("BodyMthRig_Geo")
mthSel = ("*:mouthBsh_ctrl")
mth_L_Sel = ("*:mouthBsh_lft_ctrl")
mth_R_Sel = ("*:mouthBsh_rgt_ctrl")
a = 10
b = -10 
d = 0
tx = -30
ty = 30
InOut = ("In", "Out")
UpDn = ("Up", "Dn")
# row A-----------------
# upLipsCurlIn
upLipsCurlIn10 = ("%s.upLipsCurlIO" % mthSel) #10
# upLipsCurlOut
upLipsCurlOut_10 = ("%s.upLipsCurlIO" % mthSel) #-10
# loLipsCurlIn
loLipsCurlIn10 = ("%s.loLipsCurlIO" % mthSel) #10  
# loLipsCurlOut
loLipsCurlOut_10 = ("%s.loLipsCurlIO" % mthSel) #-10 
# mouthU
mouthU10 = ("%s.mouthU" % mthSel) #10
# row B ----------------
# puffUpperUp
puffUpperUp_L10 = ("%s.cheekUprIO" % mth_L_Sel) #10
puffUpperUp_R10 = ("%s.cheekUprIO" % mth_R_Sel) #10
# puffUpperDn
puffUpperDn_L_10 = ("%s.cheekUprIO" % mth_L_Sel) #-10
puffUpperDn_R_10 = ("%s.cheekUprIO" % mth_R_Sel) #-10
# puffLowerOut
puffLowerIO_L10 = ("%s.cheekLwrIO" % mth_L_Sel) #10
puffLowerIO_R10 = ("%s.cheekLwrIO" % mth_L_Sel) #10
# puffLowerIn
puffLowerOut_L_10 = ("%s.cheekLwrIO" % mth_L_Sel) #-10
puffLowerOut_R_10 = ("%s.cheekLwrIO" % mth_L_Sel) #-10
# cornerUp
cornerUp_L10 = ("%s.cornerUD" % mth_L_Sel) #10
cornerUp_R10 = ("%s.cornerUD" % mth_R_Sel) #10
# cornerDn
cornerDn_L_10 = ("%s.cornerUD" % mth_L_Sel) #-10
cornerDn_R_10 = ("%s.cornerUD" % mth_R_Sel) #-10
# cornerIn
cornerIn_L10 = ("%s.cornerIO" % mth_L_Sel) #10
cornerIn_R10 = ("%s.cornerIO" % mth_R_Sel) #10
# cornerOut
cornerOut_L_10 = ("%s.cornerIO" % mth_L_Sel) #-10
cornerIn_R_10 = ("%s.cornerIO" % mth_R_Sel) #-10
# cornerInnerUp
cornerInnerUp_L10 = ("%s.cornerInUD" % mth_L_Sel) #10
cornerInnerUp_R10 = ("%s.cornerInUD" % mth_R_Sel) #10
# cornerInnerDn
cornerInnerDn_L_10 = ("%s.cornerInUD" % mth_L_Sel) #-10
cornerInnerDn_R_10 = ("%s.cornerInUD" % mth_R_Sel) #-10
# cornerOuterUp
cornerOuterUp_L10 = ("%s.cornerOutUD" % mth_L_Sel) #10 
cornerOuterUp_R10 = ("%s.cornerOutUD" % mth_R_Sel) #10 
# cornerOuterDn
cornerOuterDn_L_10 = ("%s.cornerOutUD" % mth_L_Sel) #-10 
cornerOuterDn_R_10 = ("%s.cornerOutUD" % mth_R_Sel) #-10 
# up_Lo_Lips 10
upLoLipsInOut10 = (upLipsCurlIn10, loLipsCurlIn10)
# mth 10   
mthU10 = mouthU10
# mth -10
up_Lo_Lips_10 = (upLipsCurlOut_10, loLipsCurlOut_10)
# mth 10   mthU10 = (mouthU)
inOut = ("In", "Out")
upDn = ("Up", "Dn")   
upList = "upLipsCurl"
def proxyBsh():
    

	for each in upLoLipsInOut10:
		for inOuts in inOut:
			if mc.objExists(each)==True:
				proxyBshZro = mc.ls('proxyBshZro_Grp')
				proxyBshZroAmount = len(proxyBshZro)
				mc.setAttr(each, int(a))
				geoDup = mc.duplicate(geoSel)
				geoDupName = mc.rename(geoDup, "upLipsCurl%s" % inOuts[i])
				mc.setAttr(each, int(d))
				if proxyBshZroAmount != 0:
					print "proxyBshZro_Grp is ready"
					mc.parent(geoDupName, proxyBshZro)
				else:
					proxyBshZro = mc.group(em= True, n = "proxyBshZro_Grp")
					mc.parent(geoDupName, proxyBshZro)

# mouth
	proxyBshZro = mc.ls('proxyBshZro_Grp')
	proxyBshZroAmount = len(proxyBshZro)
	mc.setAttr(mthU10, int(a))
	geoDup = mc.duplicate(geoSel, n = "mouth")
	mc.setAttr(mthU10, int(d))
	if proxyBshZroAmount != 0:
		print "proxyBshZro_Grp is ready"
		mc.parent(geoDup, proxyBshZro)
	else:
		proxyBshZro = mc.group(em= True, n = "proxyBshZro_Grp")
		mc.parent(geoDup, proxyBshZro)
    
# loLips
	for each in up_Lo_Lips_10:
		proxyBshZro = mc.ls('proxyBshZro_Grp')
		proxyBshZroAmount = len(proxyBshZro)
		mc.setAttr(each, int(a))
		geoDup = mc.duplicate(geoSel, n = "loLips")
		mc.setAttr(each, int(d))
    	if proxyBshZroAmount != 0:
        	print "proxyBshZro_Grp is ready"
        	mc.parent(geoDup, proxyBshZro)
    	else:
        	proxyBshZro = mc.group(em= True, n = "proxyBshZro_Grp")
        	mc.parent(geoDup, proxyBshZro)

# puffUpperUp_L10
# puffUpperUp_R10
	proxyBshZro = mc.ls('proxyBshZro_Grp')
	proxyBshZroAmount = len(proxyBshZro)
	mc.setAttr(puffUpperUp_L10, int(a))
	mc.setAttr(puffUpperUp_R10, int(a))
	geoDup = mc.duplicate(geoSel, n = "cheekUprIn")
	mc.setAttr(puffUpperUp_L10, int(d))
	mc.setAttr(puffUpperUp_R10, int(d))
	if proxyBshZroAmount != 0:
		print "proxyBshZro_Grp is ready"
		mc.parent(geoDup, proxyBshZro)
	else:
		proxyBshZro = mc.group(em= True, n = "proxyBshZro_Grp")
		mc.parent(geoDup, proxyBshZro)
    
# puffUpperDn_L_10
# puffUpperDn_R_10
	proxyBshZro = mc.ls('proxyBshZro_Grp')
	proxyBshZroAmount = len(proxyBshZro)
	mc.setAttr(puffUpperDn_L_10, int(a))
	mc.setAttr(puffUpperDn_R_10, int(a))
	geoDup = mc.duplicate(geoSel, n = "cheekUprOut")
	mc.setAttr(puffUpperDn_L_10, int(d))
	mc.setAttr(puffUpperDn_R_10, int(d))
	if proxyBshZroAmount != 0:
		print "proxyBshZro_Grp is ready"
		mc.parent(geoDup, proxyBshZro)
	else:
		proxyBshZro = mc.group(em= True, n = "proxyBshZro_Grp")
		mc.parent(geoDup, proxyBshZro)  

# puffLowerIO_L10
# puffLowerIO_R10
	proxyBshZro = mc.ls('proxyBshZro_Grp')
	proxyBshZroAmount = len(proxyBshZro)
	mc.setAttr(puffLowerIO_L10, int(a))
	mc.setAttr(puffLowerIO_R10, int(a))
	geoDup = mc.duplicate(geoSel, n = "puffLowerIn")
	mc.setAttr(puffLowerIO_L10, int(d))
	mc.setAttr(puffLowerIO_R10, int(d))
	if proxyBshZroAmount != 0:
		print "proxyBshZro_Grp is ready"
		mc.parent(geoDup, proxyBshZro)
	else:
		proxyBshZro = mc.group(em= True, n = "proxyBshZro_Grp")
		mc.parent(geoDup, proxyBshZro) 

# puffLowerOut_L_10
# puffLowerOut_R_10
	proxyBshZro = mc.ls('proxyBshZro_Grp')
	proxyBshZroAmount = len(proxyBshZro)
	mc.setAttr(puffLowerOut_L_10, int(a))
	mc.setAttr(puffLowerOut_R_10, int(a))
	geoDup = mc.duplicate(geoSel, n = "puffLowerOut")
	mc.setAttr(puffLowerOut_L_10, int(d))
	mc.setAttr(puffLowerOut_R_10, int(d))
	if proxyBshZroAmount != 0:
		print "proxyBshZro_Grp is ready"
		mc.parent(geoDup, proxyBshZro)
	else:
		proxyBshZro = mc.group(em= True, n = "proxyBshZro_Grp")
		mc.parent(geoDup, proxyBshZro) 

# cornerUp_L10
# cornerUp_R10
	proxyBshZro = mc.ls('proxyBshZro_Grp')
	proxyBshZroAmount = len(proxyBshZro)
	mc.setAttr(cornerUp_L10, int(a))
	mc.setAttr(cornerUp_R10, int(a))
	geoDup = mc.duplicate(geoSel, n = "cornerUp")
	mc.setAttr(cornerUp_L10, int(d))
	mc.setAttr(cornerUp_R10, int(d))
	if proxyBshZroAmount != 0:
		print "proxyBshZro_Grp is ready"
		mc.parent(geoDup, proxyBshZro)
	else:
		proxyBshZro = mc.group(em= True, n = "proxyBshZro_Grp")
		mc.parent(geoDup, proxyBshZro) 

# cornerDn_L_10
# cornerDn_R_10
	proxyBshZro = mc.ls('proxyBshZro_Grp')
	proxyBshZroAmount = len(proxyBshZro)
	mc.setAttr(cornerDn_L_10, int(a))
	mc.setAttr(cornerDn_R_10, int(a))
	geoDup = mc.duplicate(geoSel, n = "cornerDn")
	mc.setAttr(cornerDn_L_10, int(d))
	mc.setAttr(cornerDn_R_10, int(d))
	if proxyBshZroAmount != 0:
		print "proxyBshZro_Grp is ready"
		mc.parent(geoDup, proxyBshZro)
	else:
		proxyBshZro = mc.group(em= True, n = "proxyBshZro_Grp")
		mc.parent(geoDup, proxyBshZro) 

# cornerIn_L10
# cornerIn_R10
	proxyBshZro = mc.ls('proxyBshZro_Grp')
	proxyBshZroAmount = len(proxyBshZro)
	mc.setAttr(cornerIn_L10, int(a))
	mc.setAttr(cornerIn_R10, int(a))
	geoDup = mc.duplicate(geoSel, n = "cornerIn")
	mc.setAttr(cornerIn_L10, int(d))
	mc.setAttr(cornerIn_R10, int(d))
	if proxyBshZroAmount != 0:
		print "proxyBshZro_Grp is ready"
		mc.parent(geoDup, proxyBshZro)
	else:
		proxyBshZro = mc.group(em= True, n = "proxyBshZro_Grp")
		mc.parent(geoDup, proxyBshZro) 

# cornerOut_L_10
# cornerOut_R_10
	proxyBshZro = mc.ls('proxyBshZro_Grp')
	proxyBshZroAmount = len(proxyBshZro)
	mc.setAttr(cornerOut_L_10, int(a))
	mc.setAttr(cornerOut_R_10, int(a))
	geoDup = mc.duplicate(geoSel, n = "cornerOut")
	mc.setAttr(cornerOut_L_10, int(d))
	mc.setAttr(cornerOut_R_10, int(d))
	if proxyBshZroAmount != 0:
		print "proxyBshZro_Grp is ready"
		mc.parent(geoDup, proxyBshZro)
	else:
		proxyBshZro = mc.group(em= True, n = "proxyBshZro_Grp")
		mc.parent(geoDup, proxyBshZro)

# cornerInnerUp_L10
# cornerInnerUp_R10
	proxyBshZro = mc.ls('proxyBshZro_Grp')
	proxyBshZroAmount = len(proxyBshZro)
	mc.setAttr(cornerInnerUp_L10, int(a))
	mc.setAttr(cornerInnerUp_R10, int(a))
	geoDup = mc.duplicate(geoSel, n = "cornerInnerUp")
	mc.setAttr(cornerInnerUp_L10, int(d))
	mc.setAttr(cornerInnerUp_R10, int(d))
	if proxyBshZroAmount != 0:
		print "proxyBshZro_Grp is ready"
		mc.parent(geoDup, proxyBshZro)
	else:
		proxyBshZro = mc.group(em= True, n = "proxyBshZro_Grp")
		mc.parent(geoDup, proxyBshZro)

# cornerInnerDn_L_10
# cornerInnerDn_R_10
	proxyBshZro = mc.ls('proxyBshZro_Grp')
	proxyBshZroAmount = len(proxyBshZro)
	mc.setAttr(cornerInnerDn_L_10, int(a))
	mc.setAttr(cornerInnerDn_R_10, int(a))
	geoDup = mc.duplicate(geoSel, n = "cornerInnerDn")
	mc.setAttr(cornerInnerDn_L_10, int(d))
	mc.setAttr(cornerInnerDn_R_10, int(d))
	if proxyBshZroAmount != 0:
		print "proxyBshZro_Grp is ready"
		mc.parent(geoDup, proxyBshZro)
	else:
		proxyBshZro = mc.group(em= True, n = "proxyBshZro_Grp")
		mc.parent(geoDup, proxyBshZro)

# cornerOuterUp_L10
# cornerOuterUp_R10
	proxyBshZro = mc.ls('proxyBshZro_Grp')
	proxyBshZroAmount = len(proxyBshZro)
	mc.setAttr(cornerOuterUp_L10, int(a))
	mc.setAttr(cornerOuterUp_R10, int(a))
	geoDup = mc.duplicate(geoSel, n = "cornerOuterUp")
	mc.setAttr(cornerOuterUp_L10, int(d))
	mc.setAttr(cornerOuterUp_R10, int(d))
	if proxyBshZroAmount != 0:
		print "proxyBshZro_Grp is ready"
		mc.parent(geoDup, proxyBshZro)
	else:
		proxyBshZro = mc.group(em= True, n = "proxyBshZro_Grp")
		mc.parent(geoDup, proxyBshZro)

# cornerOuterDn_L_10
# cornerOuterDn_R_10
	proxyBshZro = mc.ls('proxyBshZro_Grp')
	proxyBshZroAmount = len(proxyBshZro)
	mc.setAttr(cornerOuterDn_L_10, int(a))
	mc.setAttr(cornerOuterDn_R_10, int(a))
	geoDup = mc.duplicate(geoSel, n = "cornerOuterDn")
	mc.setAttr(cornerOuterDn_L_10, int(d))
	mc.setAttr(cornerOuterDn_R_10, int(d))
	if proxyBshZroAmount != 0:
		print "proxyBshZro_Grp is ready"
		mc.parent(geoDup, proxyBshZro)
	else:
		proxyBshZro = mc.group(em= True, n = "proxyBshZro_Grp")
		mc.parent(geoDup, proxyBshZro)
#-------------------------------------------  




                 