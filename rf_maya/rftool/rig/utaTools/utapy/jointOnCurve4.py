import maya.cmds as mc
import re
#recuperation des curves et du nombre de joints voulut

def jointOnCurve(elem = ''):
    sel =mc.ls(sl=True)
    lists = [a.replace('_Crv', '') for a in sel]
    ken = lists[0]
    print ken

    selsList = []  
    for sels in sel:
        selsList.append(sels)
    # grpJnt = mc.createNode('transform',n='%sJntZro_Grp' % elem)
    # print grpJnt
    if sel == []:
        # pass
        mc.confirmDialog(title='No selection', message='you must select curve(s)', button=['OK'], defaultButton='OK' )
    else:
        chaine = ""
        expression = r"^[0-9]+$"
        while re.search(expression,chaine) is None:
            prompt = mc.promptDialog(
            title="Joint On Curve",
            message ="number of bones",
            button=['OK','Cancel'],
            defaultButton="OK",
            dismissString='Cancel')
            if prompt == "OK":
                chaine = mc.promptDialog(query=True, text=True)
                # pass
            elif prompt == "Cancel":
                chaine = "0"  
        if chaine == "0":
            print 'Operation Number'
        else:            
            chaine = int(chaine)      

    #Creation des CVs dup 

    for elt in sel:
        MtJntGrp = mc.createNode('transform',n=('%s%sJntRig_Grp' % (ken,elem)))
        MtCtrlGrp = mc.createNode('transform',n=('%s%sCtrlRig_Grp' % (ken,elem)))
        MtStlGrp = mc.createNode('transform',n=('%s%sStillRig_Grp' % (ken,elem)))

        mc.rebuildCurve(elt, ch=False, rpo=True, rt=False, end=True, kr=False, kcp=False, kep=True, kt=True, s=chaine, d=3, tol=0.01)
        mc.delete(elt, ch=True)
        DupCrv = mc.select(mc.duplicate(elt, n= elt+('%sUpTemp' % elem)))

        CrvUp = elt+('%sUpTemp' % elem)
        mc.delete(CrvUp, ch=True)
        mc.parent(elt,CrvUp,MtStlGrp)

        ep = chaine+1
        i=0
        SK = []
        CL = []
        JIK = []

        while ep>0:
            mc.select(elt+".ep["+str(i)+"]")
            mc.cluster(name="CL_Temp_"+str(i))
            CL.append("CL_Temp_"+str(i)+"Handle")
            pos = mc.xform("CL_Temp_"+str(i)+"Handle", query=True, rp=True, ws=True)
            mc.select(clear=True)
            mc.joint(p=(pos[0],pos[1],pos[2]), name=elt+str(i+1)+'_Jnt')  
            SK.append(elt+str(i+1)) 
            JIK.append(elt+str(i+1)+'_Jnt')
            ep-=1
            i+=1   
        i-=1
        NoJIK = len(JIK)
        NoSK = len(SK)
        mc.delete(CL)
        for sm in range(NoJIK):
            mc.parent(JIK[sm],MtJntGrp)

        #Crete Grps and Ctrls for Crv##    
        MtCtrl = []    
        for a in range(NoSK):
            ZGrp = mc.createNode('transform',n=SK[a]+'CtrlZro_Grp')
            CirCrv = mc.curve(d = 1,p=[(1.125,0,0),(1.004121,0,0),(0.991758,0,-0.157079),(0.954976,0,-0.31029),
                    (0.894678,0,-0.455861) ,(0.812351,0,-0.590207),(0.710021,0,-0.710021),(0.590207,0,-0.812351),
                    (0.455861,0,-0.894678),(0.31029,0,-0.954976),(0.157079,0,-0.991758),(0,0,-1.004121),(0,0,-1.125),
                    (0,0,-1.004121),(-0.157079,0,-0.991758),(-0.31029,0,-0.954976),(-0.455861,0,-0.894678),(-0.590207,0,-0.812351),
                    (-0.710021,0,-0.710021),(-0.812351,0,-0.590207),(-0.894678,0,-0.455861) ,(-0.954976,0,-0.31029),
                    (-0.991758,0,-0.157079),(-1.004121,0,0),(-1.125,0,0),(-1.004121,0,0),(-0.991758,0,0.157079),
                    (-0.954976,0,0.31029),(-0.894678,0,0.455861),(-0.812351,0,0.590207),(-0.710021,0,0.710021),
                    (-0.590207,0,0.812351),(-0.455861,0,0.894678),(-0.31029,0,0.954976),(-0.157079,0,0.991758),
                    (0,0,1.004121),(0,0,1.125) ,(0,0,1.004121),(0.157079,0,0.991758),(0.31029,0,0.954976),
                    (0.455861,0,0.894678),(0.590207,0,0.812351),(0.710021,0,0.710021),(0.812351,0,0.590207),
                    (0.894678,0,0.455861),(0.954976,0,0.31029),(0.991758,0,0.157079),(1.004121,0,0)])
        
            CirCrv = mc.rename(CirCrv,SK[a]+'_Ctrl')
            CirShape = mc.listRelatives( CirCrv , shapes = True )[0]
            mc.setAttr('%s.overrideEnabled'%CirShape,1)
            mc.setAttr('%s.overrideColor'%CirShape,18)
            Crv = CirCrv
            
            mc.parent(Crv,ZGrp)
            
            mc.delete(mc.parentConstraint(JIK[a],ZGrp))
            mc.parentConstraint(Crv,JIK[a])

            MtCtrl.append(ZGrp)

    ##Reverse Curve Direction##
    mc.reverseCurve(sel,ch =1,rpo=1)
    mc.reverseCurve(CrvUp,ch =1,rpo=1)

    for Mtc in range(NoJIK):
        mc.parent(MtCtrl[Mtc],MtCtrlGrp)

    ##Skin to Ctrl Crv Path##
    CtrlSkin2Crv = mc.skinCluster(sel,CrvUp, JIK, n='test', tsb=True, bm=0, sm=0, nw=1)[0]


    # CtrlSkin2Crv = mc.skinCluster(sel,elt+('%sUpTemp' % elem), JIK, n='test', tsb=True, bm=0, sm=0, nw=1)[0]
        
    axis = ['X', 'Y', 'Z']
    attrs = ['translate', 'rotate', 'scale']


    for ax in axis:
        for attr in attrs:
            mc.setAttr(elt+('%sUpTemp' % elem)+"."+attr+ax, lock=0)
            
    mc.setAttr(elt+('%sUpTemp' % elem)+'.translateY', 100)

    for ax in axis:
        for attr in attrs:
            mc.setAttr(elt+('%sUpTemp' % elem)+"."+attr+ax, lock=1)
            
        mc.setAttr(elt+('%sUpTemp' % elem)+'.v',0)   
    mc.select(clear=True)
