from nuTools import misc
reload(misc)
from utaTools.nuTools import misc
reload(misc)
import maya.cmds as mc


def copyNonlinearUI(*args):

    
    windowID = 'Copy_Nonlinear_Node_UI'
    
    
    if mc.window(windowID, exists=True):
        mc.deleteUI(windowID)
    
    
    mc.window(windowID , title = 'Copy Nonlinear Node' , width = 200 , height = 20,sizeable = False)
    mainCL = mc.columnLayout(adj=True)
    
    

    
    mc.rowLayout(numberOfColumns=3)
    mc.radioCollection('type')
    mc.radioButton('Deforms' ,  label='AllDeform')
    mc.radioButton( 'SkinCluster' , label='SkinCluster' )
    mc.radioButton( 'Both' , label='Both' )
    mc.setParent('..')
    NameValue = mc.textFieldGrp( 'Name' , label='Name',columnWidth2=[40,150] , ed = True , pht='>>>Ex: ffd1 , wire1 , wrap1 <<<')
    mc.button('Execute' , c = executeCommand)
    
    mc.showWindow()


#------------------------------Copy  nonlinear Node -----------------------------

def executeCommand(*args):
    if mc.radioButton('Deforms' , q=True , select=True):
        copyDeform()

    elif mc.radioButton('SkinCluster' , q=True , select=True):
        copySkin()

    elif mc.radioButton( 'Both' ,q=True , select=True  ):
        copyBoth()
    else:
        pass

def copyDeform(*args):
    objBase , objChild = selected()
    DeformName, SkinName, deformAll  = copyAllNonLinear(objBase= objBase, objChild = objChild)
    revList = DeformName[::-1]
    for each in objChild:
        for objSel in revList:
            mc.select(each)
            misc.addDeformerMember(deformerName=(objSel))

def copySkin(*args):
    misc.copySkinWeight(removeUnuse=True, cleanUnuseShp=True)

def copyBoth(*args):
    objBase , objChild = selected()
    DeformName, SkinName, deformAll  = copyAllNonLinear(objBase= objBase, objChild = objChild )
    revList = deformAll[::-1]
    for each in revList:
        if 'skinCluster' in each:
            for each in objChild:
                mc.select(objBase, r = True)
                mc.select(each, add = True)
                copySkin()
        elif 'wire' in each:
            for eachChild in objChild:
                mc.select(objBase, r = True)
                mc.select(eachChild, add = True)
                addDeformer(obj = each)
        elif 'ffd' in each:
            for eachChild in objChild:
                mc.select(objBase, r = True)
                mc.select(eachChild, add = True)
                addDeformer(obj = each)

def addDeformer(obj = '', *args):
    misc.addDeformerMember(deformerName=(obj))

def selected(*args):
    objBase = mc.ls(sl = True)[0]
    objChild =  mc.ls(sl = True)[1:]
    return objBase, objChild

def copyAllNonLinear(objBase= '', objChild = [] ,*args):
    Mom = mc.listRelatives(objBase)
    Child = objChild

    AllList = mc.listHistory(Mom , pdo =True) 
    DeformList = ['ffd' , 'wire','nonLinear']
    DeformName =[]
    SkinList ='skinCluster'
    SkinName = []
    deformAll = []
    for i in AllList:
        subj = mc.objectType(i)
        for a in DeformList:
            if subj == a:
                DeformName.append(i)
                deformAll.append(i)
            else:
                pass
        if subj == SkinList:
            SkinName.append(i)
            deformAll.append(i)
        else:
            pass
    return  DeformName, SkinName, deformAll
    
def copyNonLinearText(*args):
    name = mc.textFieldGrp('Name' , q=True , text=True)
    from nuTools import misc
    reload(misc)
    misc.addDeformerMember(deformerName='{}'.format(name))
    
