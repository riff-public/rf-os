import maya.cmds as mc
import maya.mel as mm 
import pymel.core as pm 
from utaTools.utapy import utaCore
reload(utaCore)

def copyShapeAllUi():
# check if window exists
    if pm.window ( 'copyShapeAllUi' , exists = True ) :
        pm.deleteUI ( 'copyShapeAllUi' ) ;
    else : pass ;
   
    window = pm.window ( 'copyShapeAllUi', title = "copyShapeAll v. 1.0" ,
        mnb = False , mxb = False , sizeable = True , rtf = True ) ;
        
    pm.window ( 'copyShapeAllUi' , e = True , h = 90, w = 300 ) ;
    mainLayout = pm.rowColumnLayout ( nc = 1 , p = window ) ;

    # 0. copyShape A
    rowLine1 = pm.rowColumnLayout ( nc = 3 , p = mainLayout , h = 10, w = 300 ) ;
    pm.button (label = "-"*100 ,h = 10, w = 300, p = rowLine1 , bgc = (0.333333, 0.333333, 0.333333)) ; 

    rowColumn1 = pm.rowColumnLayout ( nc = 7 , p = mainLayout , w = 300 , cw = [ (1, 5),  ( 2 , 110 ) , (3, 5),( 4 , 60 ), (5, 5),( 6 , 110 ), (7 , 5)] ) ;
    pm.rowColumnLayout (p = rowColumn1 ,h=8) ;
    textDataGrpA = pm.textField( "txtFieldGrpA", text = "Data Group A", p = rowColumn1 , editable = True , h = 28, w = 100) 
    pm.rowColumnLayout (p = rowColumn1 ,h=8) ;
    pm.rowColumnLayout (p = rowColumn1 ,h=8) ;
    pm.rowColumnLayout (p = rowColumn1 ,h=8) ;
    textDataGrpB = pm.textField( "txtFieldGrpB", text = "Data Group B", p = rowColumn1 , editable = True , h = 28, w = 100) 
    pm.rowColumnLayout (p = rowColumn1 ,h=8) ;


    rowColumn2 = pm.rowColumnLayout ( nc = 7 , p = mainLayout , w = 300 , cw = [ (1, 5),  ( 2 , 110 ) , (3, 5),( 4 , 60 ), (5, 5),( 6 , 110 ), (7 , 5)] ) ;
    pm.rowColumnLayout (p = rowColumn2 ,h=8) ;
    pm.button ( 'copyShapeToBsh_A' , label = ' Group "A" ', h = 30 , p = rowColumn2 , c = selectObjA,  bgc = (0, 0.74902, 1)) ; 
    pm.rowColumnLayout (p = rowColumn2 ,h=8) ;
    pm.text( label= ">> To >>" , p = rowColumn2, al = "center")
    pm.rowColumnLayout (p = rowColumn2 ,h=8) ;
    pm.button ( 'copyShapeToBsh_B' , label = ' Group "B"', h = 30 , p = rowColumn2 , c = selectObjB, bgc = (1, 0.0784314, 0.576471)) ;
    pm.rowColumnLayout (p = rowColumn2 ,h=8) ;

    rowCopyShape_A = pm.rowColumnLayout ( nc = 3 , p = mainLayout , h = 30, w = 300 , cw = [ (1, 5),  ( 2 , 290 ), (3 , 5)]) ;
    pm.rowColumnLayout (p = rowCopyShape_A ,h=8) ;
    pm.button ( 'copyShapeToBsh' , label = ' Copy Shape All ', h = 30 , p = rowCopyShape_A, c = run ,  bgc = (255 , 120, 120)) ; 
    pm.rowColumnLayout (p = rowCopyShape_A ,h=8) ;    

    # 0. copyShape B
    rowLine3 = pm.rowColumnLayout ( nc = 3 , p = mainLayout , h = 10, w = 300 ) ;
    pm.button (label = "-"*100 ,h = 10, w = 300, p = rowLine3 , bgc = (0.333333, 0.333333, 0.333333)) ; 

    window.show () ;

def selectObjA(*args):
    strObj = ''
    objA = mc.ls(sl = True)
    for obj in objA:
        if strObj == '':
            strObj = obj
        else:
            strObj = strObj + ',' + obj

    pm.textField( "txtFieldGrpA", e=True , text=strObj) 
    print "Group 'A'", objA
    # return objA

def selectObjB(*args):
    strObj = ''
    objB = mc.ls(sl = True)
    for obj in objB:
        if strObj == '':
            strObj = obj
        else:
            strObj = strObj + ',' + obj
    pm.textField( "txtFieldGrpB", e=True , text=strObj) 
    print "Group 'B'", objB
    # return objB

def run(*args):
    objA = pm.textField( "txtFieldGrpA", q=True , text=True)
    objA = objA.split(',')
    objB = pm.textField( "txtFieldGrpB", q=True , text=True)
    objB = objB.split(',')
    utaCore.copyShapeAll(objGrpA= objA, objGrpB = objB)