import maya.cmds as mc
import maya.mel as mm
import maya.api.OpenMaya as om

import os
import shutil
import sys
import re
from functools import partial

import lpRig.core as lrc
reload(lrc)

def copyWeightBasedOnWorldPosition(tolerance=0.01):
	'''
	Select vertices then object.
	Script will select the vertices of selected object that share the same position as selected vertices.
	'''
	sels = mc.ls(sl=True, fl=True)

	vtxDict = {}
	for ix in range(mc.polyEvaluate(sels[0], v=True)):
		currVtx = '{obj}.vtx[{no}]'.format(obj=sels[0], no=ix)
		currPos = mc.xform(currVtx, q=True, t=True, ws=True)

		vtxDict[currVtx] = currPos

	selVtcs = []
	for ix in range(mc.polyEvaluate(sels[1], v=True)):

		currVtx = '{obj}.vtx[{no}]'.format(obj=sels[1], no=ix)
		currPos = mc.xform(currVtx, q=True, t=True, ws=True)

		for vtx in vtxDict.keys():

			if not vtxDict[vtx][0]-tolerance <= currPos[0] <= vtxDict[vtx][0]+tolerance:
				continue

			if not vtxDict[vtx][1]-tolerance <= currPos[1] <= vtxDict[vtx][1]+tolerance:
				continue

			if not vtxDict[vtx][2]-tolerance <= currPos[2] <= vtxDict[vtx][2]+tolerance:
				continue

			selVtcs.append(currVtx)

	if selVtcs:
		mc.select(vtxDict.keys(), r=True)
		mc.select(selVtcs, add=True)
		mc.copySkinWeights(noMirror=True, surfaceAssociation='closestPoint', influenceAssociation='oneToOne')