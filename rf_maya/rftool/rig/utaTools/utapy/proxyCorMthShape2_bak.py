import maya.cmds as mc
# reload(mc)
# import pymel.core as pm 
# reload(pm)
#-----------------#
# Shapes = Shape  #
#-----------------#
def proxyCorCrtMthBsn(tyOffset, 
				jawMthRotateX, 
				bodyMthRig="BodyMthRig_Geo", 
				BodyMthLip="BodyMthLipRig_Geo", 
				BodyMthJaw="BodyMthJawRig_Geo",
				Obj01MthGeo="Obj01MthRig_Geo",
				Obj02MthGeo="Obj02MthRig_Geo",
				*args):

	#BodyMthRig_Geo
	#BodyMthLipRig_Geo
	#BodyMthJawRig_Geo
	#Obj01MthRig_Geo
	#Obj02MthRig_Geo

	headOrig = ["Orig", ("JawOpenRef%s" % jawMthRotateX)]
	crtMthShapes = ["JawOpenMthShapes", "JawOpenShapes"]
	crtBshShapes = ["CnrUpOpenRefShapes", "CnrDnOpenRefShapes", "CnrInOpenShapes", "CnrOutOpenShapes", "CnrInUpOpenShapes", "CnrInDnOpenShapes", "CnrOutUpOpenShapes", "CnrOutDnOpenShapes"]          
	crtBsh = ["JawOpenMth", "JawOpen", "CnrUpOpen", "CnrDnOpen", "CnrInOpen", "CnrOutOpen", "CnrInUpOpen", "CnrInDnOpen", "CnrOutUpOpen", "CnrOutDnOpen"]      
	crtPuff =  ["LipUpPuffMthRigShapes", "LipLowPuffMthRigShapes", "LipUpThickMthRigShapes", "LipLowThickMthRigShapes"]
	mthctrl = mc.ls("*:JawMthRig_Ctrl")
	# headJaw = mc.ls("BodyMthJawRig_Geo")
	# headMth = mc.ls("BodyMthRig_Geo")
	headGeo =(BodyMthLip+ BodyMthJaw+ Obj01MthGeo+ Obj02MthGeo)
	headGeoUnlock = (bodyMthRig+BodyMthLip+BodyMthJaw+Obj01MthGeo+Obj02MthGeo)
	print headGeo
	orig_Grp = mc.createNode("transform", n = "orig_Grp")
	origObj01_Grp = mc.createNode("transform", n = "origObj01_Grp")
	origObj02_Grp = mc.createNode("transform", n = "origObj02_Grp")

	crtBsh_Grp = mc.createNode('transform',n = 'crtBsh_Grp')
	# crtObj01Bsh_Grp = mc.createNode('transform',n = 'crtObj01Bsh_Grp')
	# crtObj02Bsh_Grp = mc.createNode('transform',n = 'crtObj02Bsh_Grp')

	crtBshShapes_Grps = mc.createNode('transform',n = 'crtBshShapes_Grp')
	crtObj01BshShapes_Grps = mc.createNode('transform',n = 'crtBshObj01Shapes_Grp')
	crtObj02BshShapes_Grps = mc.createNode('transform',n = 'crtBshObj02Shapes_Grp')

	crtBshCon_Grps = mc.createNode('transform',n = 'crtBshCon_Grp')
	crtObj01BshCon_Grps = mc.createNode('transform',n = 'crtObj01BshCon_Grp')
	crtObj02BshCon_Grps = mc.createNode('transform',n = 'crtObj02BshCon_Grp')

	crtPuff_Grps = mc.createNode('transform',n = 'crtPuff_Grp')
	crtObj01Puff_Grps = mc.createNode('transform',n = 'crtObj01Puff_Grp')
	crtObj02Puff_Grps = mc.createNode('transform',n = 'crtObj02Puff_Grp')
	attr = ["tx", "ty", "tz", "rx", "ry", "rz", "sx", "sy", "sz"]
	# unlock headJaw
	# print headGeo[0]
	i = 0 
	for each in headGeo:
		for attrs in attr:
			print each
			mc.setAttr("%s.%s" % (headGeo[i], attrs), l = False);
		i += 1
	# HeadOriShapes and JawShapes(valueRotate)
	headBshOrig = mc.duplicate(BodyMthJaw, n = "%s" % headOrig[0])
	headObj01Orig = mc.duplicate(Obj01MthGeo, n = "%s%s" % (headOrig[0], "Obj01"))
	headObj02Orig = mc.duplicate(Obj02MthGeo, n = "%s%s" % (headOrig[1], "Obj02"))
	listOrigObj01 = mc.listRelatives(origObj01_Grp)

	mc.setAttr("%s.rotateX" % (mthctrl[0]),jawMthRotateX)
	headJawOprnRef = mc.duplicate(BodyMthJaw, n = "%s" % headOrig[1])
	headObj01OprnRef = mc.duplicate(Obj01MthGeo, n = "%s%s" % (headOrig[0], "Obj01"))
	headObj02OprnRef = mc.duplicate(Obj02MthGeo, n = "%s%s" % (headOrig[1], "Obj02"))
	listOrigObj02 = mc.listRelatives(origObj02_Grp)

	# Parent 
	mc.parent(headObj01Orig, headObj01OprnRef, origObj01_Grp)
	mc.parent(headObj02Orig, headObj02OprnRef, origObj02_Grp)
	
	# Duplicate crtBshShapes
	crtMthShapesAmount = len(crtMthShapes)
	crtBshShapesAmount = len(crtBshShapes)
	crtBshAmount = len(crtBsh)
	crtPuffAmount = len(crtPuff)

	# move Orig, jawOpen
	# print listOrigObj01, headOrig
	i = 0 
	defaultTyEach = 0
	for obj in headOrig:
		for attrs in attr:
			mc.setAttr("%s.%s" % (headOrig[i], attrs), l = False);
			# mc.setAttr("%s.%s" % (listOrigObj01[each], attrs), l = False);
			# mc.setAttr("%s.%s" % (listOrigObj02[each], attrs), l = False);
			defaultTyEach += 3
		i += 1
		mc.parent (obj, orig_Grp)


	# for crtBshShapess in crtBshShapes:
	for i in range(crtMthShapesAmount):

		bshShapes = mc.duplicate(BodyMthJaw, n = "%s" % crtMthShapes[i])
		bshObj01Shapes = mc.duplicate(Obj01MthGeo, n = "%s%s" % (crtMthShapes[i], "Obj01"))
		bshObj02Shapes = mc.duplicate(Obj02MthGeo, n = "%s%s" % (crtMthShapes[i], "Obj02"))
		mc.parent(bshShapes, crtBshShapes_Grps)
		mc.parent(bshObj01Shapes, crtObj01BshShapes_Grps )
		mc.parent(bshObj02Shapes, crtObj02BshShapes_Grps )
	for i in range(crtBshAmount):
		bshShapes = mc.duplicate(headOrig[0], n = "%s" % crtBsh[i])
		bshObj01Shapes = mc.duplicate(Obj01MthGeo, n = "%s%s" % (crtBsh[i], "Obj01"))
		bshObj02Shapes = mc.duplicate(Obj02MthGeo, n = "%s%s" % (crtBsh[i], "Obj02"))
		mc.parent(bshShapes, crtBshCon_Grps)
		mc.parent(bshObj01Shapes, crtObj01BshCon_Grps )
		mc.parent(bshObj02Shapes, crtObj02BshCon_Grps )
	for i in range(crtBshShapesAmount):
		bshShapes = mc.duplicate(headOrig[0], n = "%s" % crtBshShapes[i])
		bshObj01Shapes = mc.duplicate(Obj01MthGeo, n = "%s%s" % (crtBshShapes[i], "Obj01"))
		bshObj02Shapes = mc.duplicate(Obj02MthGeo, n = "%s%s" % (crtBshShapes[i], "Obj02"))
		mc.parent(bshShapes, crtBshShapes_Grps)
		mc.parent(bshObj01Shapes, crtObj01BshShapes_Grps)
		mc.parent(bshObj02Shapes, crtObj02BshShapes_Grps)
	for i in range(crtPuffAmount):
		bshShapes = mc.duplicate(headOrig[0], n = "%s" % crtPuff[i])
		bshObj01Shapes = mc.duplicate(Obj01MthGeo, n = "%s%s" % (crtPuff[i], "Obj01"))
		bshObj02Shapes = mc.duplicate(Obj02MthGeo, n = "%s%s" % (crtPuff[i], "Obj02"))
		mc.parent(bshShapes, crtPuff_Grps)
		mc.parent(bshObj01Shapes, crtObj01Puff_Grps)
		mc.parent(bshObj02Shapes, crtObj02Puff_Grps)

	# setAttribute JawOpen Default
	mc.setAttr("%s.rotateX" % (mthctrl[0]),0)
	mc.setAttr ("%sShape.jawOpen" % mthctrl[0], jawMthRotateX);
	# parent to crtBsh_Grp
	mc.parent(crtBshShapes_Grps, 
				crtObj01BshShapes_Grps, 
				crtObj02BshShapes_Grps, 
				crtBshCon_Grps, 
				crtObj01BshCon_Grps, 
				crtObj02BshCon_Grps, 
				crtPuff_Grps, 
				crtObj01Puff_Grps, 
				crtObj02Puff_Grps, 
				orig_Grp, 
				origObj01_Grp, 
				origObj02_Grp, 
				crtBsh_Grp)

	# set TranslateX Obj
	mc.setAttr("%s.translateX" % (crtBshCon_Grps),-3.5*int(tyOffset))
	mc.setAttr("%s.translateX" % (crtObj01BshCon_Grps),-3.5*int(tyOffset))
	mc.setAttr("%s.translateX" % (crtObj02BshCon_Grps),-3.5*int(tyOffset))
	mc.setAttr("%s.translateX" % (crtBshShapes_Grps),-2.5*int(tyOffset))
	mc.setAttr("%s.translateX" % (crtObj01BshShapes_Grps),-2.5*int(tyOffset))
	mc.setAttr("%s.translateX" % (crtObj02BshShapes_Grps),-2.5*int(tyOffset))
	mc.setAttr("%s.translateX" % (crtPuff_Grps),-1.5*int(tyOffset))
	mc.setAttr("%s.translateX" % (crtObj01Puff_Grps),-1.5*int(tyOffset))
	mc.setAttr("%s.translateX" % (crtObj02Puff_Grps),-1.5*int(tyOffset))
	mc.setAttr("%s.translateX" % (headOrig[0]),0.5*jawMthRotateX)
	mc.setAttr("%s.translateX" % (origObj01_Grp),0.5*jawMthRotateX)
	mc.setAttr("%s.translateX" % (headOrig[1]),0.8*jawMthRotateX)
	mc.setAttr("%s.translateX" % (origObj02_Grp),0.8*jawMthRotateX)

	crtBshShapesTotal = (crtMthShapes+crtBshShapes)
	print '2', crtBshShapesTotal
	print '1', crtObj01BshShapes_Grps
	# translateY 
	crtBshShapes_GrpsAmount = len(crtBshShapesTotal)
	crtObj01BshShapes_GrpsAmount = mc.listRelatives(crtObj01BshShapes_Grps)
	crtObj02BshShapes_GrpsAmount = mc.listRelatives(crtObj02BshShapes_Grps)
	crtBshCon_GrpsAmount = len(crtBsh)
	crtObj01BshCon_GrpsAmount = mc.listRelatives(crtObj01BshCon_Grps)
	crtObj02BshCon_GrpsAmount = mc.listRelatives(crtObj02BshCon_Grps)
	crtPuff_GrpsAmount = len(crtPuff)
	crtObj01Puff_GrpsAmount = mc.listRelatives(crtObj01Puff_Grps)
	crtObj02Puff_GrpsAmount = mc.listRelatives(crtObj02Puff_Grps)
	defaultTy = 0
	for i in range(crtBshShapes_GrpsAmount):
		mc.setAttr("%s.translateY" % (crtBshShapesTotal[i]), defaultTy)
		mc.setAttr("%s.translateY" % (crtObj01BshShapes_GrpsAmount[i]), defaultTy)
		mc.setAttr("%s.translateY" % (crtObj02BshShapes_GrpsAmount[i]), defaultTy)
		defaultTy += tyOffset
	defaultTy = 0
	for i in range(crtBshCon_GrpsAmount):
		mc.setAttr("%s.translateY" % (crtBsh[i]), defaultTy)
		mc.setAttr("%s.translateY" % (crtObj01BshCon_GrpsAmount[i]), defaultTy)
		mc.setAttr("%s.translateY" % (crtObj02BshCon_GrpsAmount[i]), defaultTy)
		defaultTy += tyOffset
	defaultTy = 0
	for i in range(crtPuff_GrpsAmount):
		mc.setAttr("%s.translateY" % (crtPuff[i]), defaultTy)
		mc.setAttr("%s.translateY" % (crtObj01Puff_GrpsAmount[i]), defaultTy)
		mc.setAttr("%s.translateY" % (crtObj02Puff_GrpsAmount[i]), defaultTy)
		defaultTy += tyOffset
 	mc.select(cl=True)

	## create BlendShape Shapestive Mouth -------------------------------------------------------

	bshList = ["CnrOutDnOpen", "CnrOutUpOpen", "CnrInDnOpen", "CnrInUpOpen", "CnrOutOpen", "CnrInOpen", "CnrDnOpen", "CnrUpOpen", "JawOpen", "JawOpenMth"]
	bshObj01List = ["CnrOutDnOpenObj01", "CnrOutUpOpenObj01", "CnrInDnOpenObj01", "CnrInUpOpenObj01", "CnrOutOpenObj01", "CnrInOpenObj01", "CnrDnOpenObj01", "CnrUpOpenObj01", "JawOpenObj01", "JawOpenMthObj01"]
	bshObj02List = ["CnrOutDnOpenObj02", "CnrOutUpOpenObj02", "CnrInDnOpenObj02", "CnrInUpOpenObj02", "CnrOutOpenObj02", "CnrInOpenObj02", "CnrDnOpenObj02", "CnrUpOpenObj02", "JawOpenObj02", "JawOpenMthObj02"]

	# proxhList = ["cornerOuterDn" , "cornerOuterUp", "cornerInnerDn", "cornerInnerUp", "cornerOut", "cornerIn", "cornerDn", "cornerUp", ""]
	# ShapesList = ["CnrOutDnOpenShapes", "CnrOutUpOpenShapes", "CnrInDnOpenShapes", "CnrInUpOpenShapes", "CnrOutOpenShapes", "CnrInOpenShapes", "CnrDnOpenRefShapes", "CnrUpOpenRefShapes"]
	# jawOpenRefNum = ("JawOpenRef20")
	# jawOpenShapesList = ("JawOpenShapes")
	
	'''1 =====================================================================
	CnrOutDnOpen
		JawOpenShapes -1
		cornerOuterDn -1
		CnrOutDnOpenShapes 1
	'''
	# Bsh Corractive Body Geo
	CnrOutDnOpenBsn = mc.blendShape("JawOpenShapes", "cornerOuterDn", "CnrOutDnOpenShapes", "CnrOutDnOpen",n="%sBsn" % bshList[0])[0]
	mc.setAttr("%sBsn" % bshList[0]+"."+"JawOpenShapes",-1)
	mc.setAttr("%sBsn" % bshList[0]+"."+"cornerOuterDn",-1)
	mc.setAttr("%sBsn" % bshList[0]+"."+"CnrOutDnOpenShapes",1)

	CnrOutDnOpenShapesBsn = mc.blendShape("JawOpenShapes", "cornerOuterDn", "CnrOutDnOpenShapes", n="%sShapesBsn" % bshList[0])[0]
	mc.setAttr("%sShapesBsn" % bshList[0]+"."+"JawOpenShapes",1)
	mc.setAttr("%sShapesBsn" % bshList[0]+"."+"cornerOuterDn",1)

	# Bsh Corractive Obj01 Geo
	CnrOutDnOpenBsnObj01 = mc.blendShape("JawOpenShapesObj01", "cornerOuterDnObj01", "CnrOutDnOpenShapesObj01", "CnrOutDnOpenObj01",n="%sBsn" % bshObj01List[0])[0]
	mc.setAttr("%sBsn" % bshObj01List[0]+"."+"JawOpenShapesObj01",-1)
	mc.setAttr("%sBsn" % bshObj01List[0]+"."+"cornerOuterDnObj01",-1)
	mc.setAttr("%sBsn" % bshObj01List[0]+"."+"CnrOutDnOpenShapesObj01",1)

	CnrOutDnOpenShapesBsnObj01 = mc.blendShape("JawOpenShapesObj01", "cornerOuterDnObj01", "CnrOutDnOpenShapesObj01", n="%sShapesBsnObj01" % bshObj01List[0])[0]
	mc.setAttr("%sShapesBsnObj01" % bshObj01List[0]+"."+"JawOpenShapesObj01",1)
	mc.setAttr("%sShapesBsnObj01" % bshObj01List[0]+"."+"cornerOuterDnObj01",1)

	# Bsh Corractive Obj02 Geo
	CnrOutDnOpenBsnObj02 = mc.blendShape("JawOpenShapesObj02", "cornerOuterDnObj02", "CnrOutDnOpenShapesObj02", "CnrOutDnOpenObj02",n="%sBsn" % bshObj02List[0])[0]
	mc.setAttr("%sBsn" % bshObj02List[0]+"."+"JawOpenShapesObj02",-1)
	mc.setAttr("%sBsn" % bshObj02List[0]+"."+"cornerOuterDnObj02",-1)
	mc.setAttr("%sBsn" % bshObj02List[0]+"."+"CnrOutDnOpenShapesObj02",1)

	CnrOutDnOpenShapesBsnObj02 = mc.blendShape("JawOpenShapesObj02", "cornerOuterDnObj02", "CnrOutDnOpenShapesObj02", n="%sShapesBsnObj02" % bshObj02List[0])[0]
	mc.setAttr("%sShapesBsnObj02" % bshObj02List[0]+"."+"JawOpenShapesObj02",1)
	mc.setAttr("%sShapesBsnObj02" % bshObj02List[0]+"."+"cornerOuterDnObj02",1)


	'''2  =====================================================================
	CnrOutUpOpen
		JawOpenShapes -1
		cornerOuterUp -1
		CnrOutUpOpenShapes 1
	'''
	# Bsh Corractive Body Geo
	CnrOutUpOpenBsn = mc.blendShape("JawOpenShapes", "cornerOuterUp", "CnrOutUpOpenShapes", "CnrOutUpOpen",n="%sBsn" % bshList[1])[0]
	mc.setAttr("%sBsn" % bshList[1]+"."+"JawOpenShapes",-1)
	mc.setAttr("%sBsn" % bshList[1]+"."+"cornerOuterUp",-1)
	mc.setAttr("%sBsn" % bshList[1]+"."+"CnrOutUpOpenShapes",1)

	CnrOutUpOpenShapesBsn = mc.blendShape("JawOpenShapes", "cornerOuterUp", "CnrOutUpOpenShapes", n="%sShapesBsn" % bshList[1])[0]
	mc.setAttr("%sShapesBsn" % bshList[1]+"."+"JawOpenShapes",1)
	mc.setAttr("%sShapesBsn" % bshList[1]+"."+"cornerOuterUp",1)

	# Bsh Corractive Obj01 Geo
	CnrOutUpOpenBsnObj01 = mc.blendShape("JawOpenShapesObj01", "cornerOuterUpObj01", "CnrOutUpOpenShapesObj01", "CnrOutUpOpenObj01",n="%sBsn" % bshObj01List[1])[0]
	mc.setAttr("%sBsn" % bshObj01List[1]+"."+"JawOpenShapesObj01",-1)
	mc.setAttr("%sBsn" % bshObj01List[1]+"."+"cornerOuterUpObj01",-1)
	mc.setAttr("%sBsn" % bshObj01List[1]+"."+"CnrOutUpOpenShapesObj01",1)

	CnrOutUpOpenShapesBsnObj01 = mc.blendShape("JawOpenShapesObj01", "cornerOuterUpObj01", "CnrOutUpOpenShapesObj01", n="%sShapesBsn" % bshObj01List[1])[0]
	mc.setAttr("%sShapesBsn" % bshObj01List[1]+"."+"JawOpenShapesObj01",1)
	mc.setAttr("%sShapesBsn" % bshObj01List[1]+"."+"cornerOuterUpObj01",1)




	'''3
	CnrInDnOpen
		JawOpenShapes -1
		cornerInnerDn -1
		cnrInDnOpenShapes 1
	'''
	CnrInDnOpenBsn = mc.blendShape("JawOpenShapes", "cornerInnerDn", "CnrInDnOpenShapes", "CnrInDnOpen",n="%sBsn" % bshList[2])[0]
	mc.setAttr("%sBsn" % bshList[2]+"."+"JawOpenShapes",-1)
	mc.setAttr("%sBsn" % bshList[2]+"."+"cornerInnerDn",-1)
	mc.setAttr("%sBsn" % bshList[2]+"."+"CnrInDnOpenShapes",1)

	CnrInDnOpenShapesBsn = mc.blendShape("JawOpenShapes", "cornerInnerDn", "CnrInDnOpenShapes", n="%sShapesBsn" % bshList[2])[0]
	mc.setAttr("%sShapesBsn" % bshList[2]+"."+"JawOpenShapes",1)
	mc.setAttr("%sShapesBsn" % bshList[2]+"."+"cornerInnerDn",1)
	# mc.setAttr("%sShapesBsn" % bshList[2]+"."+"CnrInDnOpenShapes",1)
	'''4
	CnrInUpOpen
		JawOpenShapes -1
		cornerInnerUp -1
		CnrInUpOpenShapes 1
	'''
	CnrInUpOpenBsn = mc.blendShape("JawOpenShapes", "cornerInnerUp", "CnrInUpOpenShapes", "CnrInUpOpen",n="%sBsn" % bshList[3])[0]
	mc.setAttr("%sBsn" % bshList[3]+"."+"JawOpenShapes",-1)
	mc.setAttr("%sBsn" % bshList[3]+"."+"cornerInnerUp",-1)
	mc.setAttr("%sBsn" % bshList[3]+"."+"CnrInUpOpenShapes",1)

	CnrInUpOpenBsn = mc.blendShape("JawOpenShapes", "cornerInnerUp", "CnrInUpOpenShapes", n="%sShapesBsn" % bshList[3])[0]
	mc.setAttr("%sShapesBsn" % bshList[3]+"."+"JawOpenShapes",1)
	mc.setAttr("%sShapesBsn" % bshList[3]+"."+"cornerInnerUp",1)
	# mc.setAttr("%sShapesBsn" % bshList[3]+"."+"CnrInUpOpenShapes",1)
	'''5
	CnrOutOpen
		JawOpenShapes -1
		cornerOut -1
		CnrOutOpenShapes 1
	'''
	CnrOutOpenBsn = mc.blendShape("JawOpenShapes", "cornerOut", "CnrOutOpenShapes", "CnrOutOpen",n="%sBsn" % bshList[4])[0]
	mc.setAttr("%sBsn" % bshList[4]+"."+"JawOpenShapes",-1)
	mc.setAttr("%sBsn" % bshList[4]+"."+"cornerOut",-1)
	mc.setAttr("%sBsn" % bshList[4]+"."+"CnrOutOpenShapes",1)

	CnrOutOpenShapesBsn = mc.blendShape("JawOpenShapes", "cornerOut", "CnrOutOpenShapes", n="%sShapesBsn" % bshList[4])[0]
	mc.setAttr("%sShapesBsn" % bshList[4]+"."+"JawOpenShapes",1)
	mc.setAttr("%sShapesBsn" % bshList[4]+"."+"cornerOut",1)
	# mc.setAttr("%sShapessn" % bshList[4]+"."+"CnrOutOpenShapes",1)
	'''6
	CnrInOpen
		JawOpenShapes -1
		cornerIn -1
		CnrInOpenShapes 1
	'''
	CnrInOpenBsn = mc.blendShape("JawOpenShapes", "cornerIn", "CnrInOpenShapes", "CnrInOpen",n="%sBsn" % bshList[5])[0]
	mc.setAttr("%sBsn" % bshList[5]+"."+"JawOpenShapes",-1)
	mc.setAttr("%sBsn" % bshList[5]+"."+"cornerIn",-1)
	mc.setAttr("%sBsn" % bshList[5]+"."+"CnrInOpenShapes",1)

	CnrInOpenShapesBsn = mc.blendShape("JawOpenShapes", "cornerIn", "CnrInOpenShapes", n="%sShapesBsn" % bshList[5])[0]
	mc.setAttr("%sShapesBsn" % bshList[5]+"."+"JawOpenShapes",1)
	mc.setAttr("%sShapesBsn" % bshList[5]+"."+"cornerIn",1)
	# mc.setAttr("%sShapesBsn" % bshList[5]+"."+"CnrInOpenShapes",1)
	'''7
	CnrDnOpen
		JawOpenRef20 -1
		cornerDn -1
		CnrDnOpenRefShapes 1
	'''
	CnrDnOpenBsn = mc.blendShape(("JawOpenRef%s" % jawMthRotateX), "cornerDn", "CnrDnOpenRefShapes", "CnrDnOpen",n="%sBsn" % bshList[6])[0]
	mc.setAttr("%sBsn" % bshList[6]+"."+("JawOpenRef%s" % jawMthRotateX),-1)
	mc.setAttr("%sBsn" % bshList[6]+"."+"cornerDn",-1)
	mc.setAttr("%sBsn" % bshList[6]+"."+"CnrDnOpenRefShapes",1)

	CnrDnOpenRefShapesBsn = mc.blendShape(("JawOpenRef%s" % jawMthRotateX), "cornerDn", "CnrDnOpenRefShapes",n="%sBsn" % crtBshShapesTotal[3])[0]
	mc.setAttr("%sBsn" % crtBshShapesTotal[3]+"."+("JawOpenRef%s" % jawMthRotateX),1)
	mc.setAttr("%sBsn" % crtBshShapesTotal[3]+"."+"cornerDn",1)
	# mc.setAttr("%sBsn" % bshList[6]+"."+"CnrDnOpenRefShapes",1)
	'''8
	CnrUpOpen
		JawOpenRef20 -1
		cornerUp -1
		CnrUpOpenRefShapes 1
	'''
	CnrUpOpenBsn = mc.blendShape(("JawOpenRef%s" % jawMthRotateX), "cornerUp", "CnrUpOpenRefShapes", "CnrUpOpen",n="%sBsn" % bshList[7])[0]
	mc.setAttr("%sBsn" % bshList[7]+"."+("JawOpenRef%s" % jawMthRotateX),-1)
	mc.setAttr("%sBsn" % bshList[7]+"."+"cornerUp",-1)
	mc.setAttr("%sBsn" % bshList[7]+"."+"CnrUpOpenRefShapes",1)

	CnrUpOpenRefShapesBsn = mc.blendShape(("JawOpenRef%s" % jawMthRotateX), "cornerUp", "CnrUpOpenRefShapes",n="%sBsn" % crtBshShapesTotal[2])[0]
	mc.setAttr("%sBsn" % crtBshShapesTotal[2]+"."+("JawOpenRef%s" % jawMthRotateX),1)
	mc.setAttr("%sBsn" % crtBshShapesTotal[2]+"."+"cornerUp",1)
	# mc.setAttr("%sBsn" % bshList[7]+"."+"CnrUpOpenRefShapes",1)

	'''9
	JawOpen
		JawOpenRef20 -1
		JawOpenShapes 1
	'''
	JawOpenBsn = mc.blendShape(("JawOpenRef%s" % jawMthRotateX), "JawOpenShapes", "JawOpen",n="%sBsn" % bshList[8])[0]
	mc.setAttr("%sBsn" % bshList[8]+"."+("JawOpenRef%s" % jawMthRotateX),-1)
	mc.setAttr("%sBsn" % bshList[8]+"."+"JawOpenShapes",1)

	# JawOpenShapesBsn = mc.blendShape(("JawOpenRef%s" % jawMthRotateX), "JawOpenShapes", "JawOpen",n="%sBsn" % bshList[8])[0]
	# mc.setAttr("%sBsn" % bshList[8]+"."+("JawOpenRef%s" % jawMthRotateX),-1)
	# mc.setAttr("%sBsn" % bshList[8]+"."+"JawOpenShapes",1)
	# JawOpenShape
	'''10
	JawOpenMth
		JawOpenRef20 -1
		JawOpenMthShapes 1
	'''
	JawOpenMthBsn = mc.blendShape(("JawOpenRef%s" % jawMthRotateX), "JawOpenMthShapes", "JawOpenMth",n="%sBsn" % bshList[9])[0]
	mc.setAttr("%sBsn" % bshList[9]+"."+("JawOpenRef%s" % jawMthRotateX),-1)
	mc.setAttr("%sBsn" % bshList[9]+"."+"JawOpenMthShapes",1)
