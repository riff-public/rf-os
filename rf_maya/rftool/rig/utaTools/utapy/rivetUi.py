import maya.cmds as mc
import pymel.core as pm 
from lpRig import rigTools as lrr
reload(lrr)

def rivetDo(*args):
	# elemObj = pm.textField( "rivertNameTextField", text = 'AddRig', p = mainLayout , editable = True , h = 30)
	elemObj = pm.textField ( "elemTxtField" , q  = True , text = True  )
	lrr.rivetToSeletedEdge(part=elemObj)

def rivetUi (*args) :
    #check if window exists
	if pm.window ( 'rivetMainUI' , exists = True ) :
		pm.deleteUI ( 'rivetMainUI' ) ;

	#title Ui
	title = "Rivert Ui v.1 (05-07-2018)" ;

	#window
	window = pm.window ( 'rivetMainUI', title = title , w = 250, h = 32, mnb = True , mxb = True , sizeable = True , rtf = True ) ;

	#mainLayout
	mainLayout = pm.rowColumnLayout ( w = 250, h = 32, p = window , nc = 3 ,columnWidth = [ ( 1 , 75 ) , ( 2 , 100 ) , (3, 75)] );

	#textField,Button
	pm.text( label='# Select Edge' , p = mainLayout, al = "left")
	elemObj = pm.textField( "elemTxtField", text = 'AddRig', p = mainLayout , editable = True , h = 30)
	pm.button ("rivertButton",label = "Rivert" , p = mainLayout, c = rivetDo ,bgc = (0.411765, 0.411765, 0.411765)) ; 
	# return elemObj
	#showWindow
	pm.showWindow ( window ) ;

