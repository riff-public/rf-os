import sys
import maya.cmds as mc
# reload(mc)

def setAttrRbbSpider(*args):
# UpLegFnt L
	# UpLegFntDtl_2_L
	mc.setAttr('%s.tx' % 'RbnUpLegFntDtl_2_L_Ofst_Grp', 5.22)
	mc.setAttr('%s.tz' % 'RbnUpLegFntDtl_2_L_Ofst_Grp', 1.656)
	# UpLegFntDtl_3_L
	mc.setAttr('%s.tx' % 'RbnUpLegFntDtl_3_L_Ofst_Grp', 5.652)
	mc.setAttr('%s.tz' % 'RbnUpLegFntDtl_3_L_Ofst_Grp', 7.702)
	# UpLegFntDtl_4_L
	mc.setAttr('%s.tx' % 'RbnUpLegFntDtl_4_L_Ofst_Grp', 1.901)
	mc.setAttr('%s.tz' % 'RbnUpLegFntDtl_4_L_Ofst_Grp', 8.541)

# UpLegFnt R
	# UpLegFntDtl_2_R
	mc.setAttr('%s.tx' % 'RbnUpLegFntDtl_2_R_Ofst_Grp', -5.22)
	mc.setAttr('%s.tz' % 'RbnUpLegFntDtl_2_R_Ofst_Grp', 1.656)
	# UpLegFntDtl_3_R
	mc.setAttr('%s.tx' % 'RbnUpLegFntDtl_3_R_Ofst_Grp', -5.652)
	mc.setAttr('%s.tz' % 'RbnUpLegFntDtl_3_R_Ofst_Grp', 7.702)
	# UpLegFntDtl_4_R
	mc.setAttr('%s.tx' % 'RbnUpLegFntDtl_4_R_Ofst_Grp', -1.901)
	mc.setAttr('%s.tz' % 'RbnUpLegFntDtl_4_R_Ofst_Grp', 8.541)