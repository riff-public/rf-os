import maya.cmds as mc
from utaTools.utapy import utaCore
reload(utaCore)
import pymel.core as pm
from ncmel import fkRig
reload(fkRig)
from ncmel import subRig
reload(subRig)

##-------------------------
# from utaTools.utapy import ClavMuscleRig
# reload(ClavMuscleRig)
# ClavMuscleRig.ClavMuscleRig(	ClavMusSpineRoot  = 'SpineMusRoot_Jnt',
#             					elem 			= 'ClavMus',
#             					side 			= 'L',
								# men 			= True
#             					ctrlGrp      	= 'MainCtrl_Grp' ,
#             					skinGrp      	= 'Skin_Grp' ,
#             					jntGrp       	= 'Jnt_Grp' ,
#             					ikhGrp          = 'Ikh_Grp' ,
#             					stillGrp 		= 'Still_Grp' )
##-------------------------
def ClavMuscleRig(	ClavMusSpineRoot  = 'SpineMusRoot_Jnt',
					elem 			= 'ClavMus',
					side 			= 'L',
					men				= True,
					parent   		= '',
					ctrlGrp      	= '' ,
					skinGrp      	= '' ,
					jntGrp       	= '' ,
					ikhGrp 			= '' ,
					stillGrp 		= '' , *args):



	## Create Control for late1Skin_L R_Jnt
	late = subRig.Run(
						name       = 'Late' ,
						tmpJnt     = 'late1Skin_{}_Jnt'.format(side) ,
						parent     = 'Spine5_Jnt',
						ctrlGrp    = ctrlGrp ,
						shape      = 'cube' ,
						side       = side ,
						size       = 1  
						)
	utaCore.lockAttrObj(listObj = ['LateSubCtrlOfst_{}_Grp'.format(side)], lock = False,keyable = True)
	mc.delete('late1Skin_{}_Jnt'.format(side))

	## Name
	if 'L' in side:
		side = '_L_'
	elif 'R' in side:
		side = '_R_'
	else:
		side = '_'


	## ControlAll
	controlList = []
	if parent:
		parent = parent
	else:
		parent       	= 'Clav{}Ctrl'.format(side)

	upArmParentGrp 	= [	'ArmFkCtrl{}Grp'.format(side), 'ArmIkCtrl{}Grp'.format(side)]
	spineJnt 		= 'Spine5_Jnt'
	upArmJnt 		= 'UpArm{}Jnt'.format(side)
	neckJnt 		= 'Neck_Jnt'

	## Group
	ClavMusSkin= mc.group(n = '{}Skin{}Grp'.format(elem, side), em = True)
	ClavMusCtrl = mc.group(n = '{}Ctrl{}Grp'.format(elem, side), em = True)
	ClavMusIkh = mc.group(n = '{}Ikh{}Grp'.format(elem, side), em = True)
	ClavMusStill = mc.group(n = '{}Still{}Grp'.format(elem, side), em = True)

	## Parent SpinePos to Spine
	## Unlocak Ik and Fk Control Group
	for each in upArmParentGrp:
		if mc.objExists(each):
			utaCore.lockAttrObj(listObj = [each], lock = False,keyable = True)
			utaCore.parentScaleConstraintLoop(  obj = 'ArmPointSkin1{}Jnt'.format(side), lists = [each], par = True, sca = False, ori = False, poi = False)

	## Parent Constraint
	utaCore.parentScaleConstraintLoop(  obj = spineJnt, lists = [ClavMusSpineRoot],par = True, sca = False, ori = False, poi = False)
	# utaCore.parentScaleConstraintLoop(  obj = spineJnt, lists = ['pacSkinJnt_L_Grp'],par = True, sca = False, ori = False, poi = False)

	## Create Control
	Pec1BineJnt = 'Pec1Skin{}Jnt'.format(side)
	mc.select(Pec1BineJnt)
	Pec1BineGrpCtrlName, Pec1BineGrpCtrlOfsName, Pec1BineGrpCtrlParsName, Pec1BineCtrlName, Pec1BineGmblCtrlName = utaCore.createControlCurve(curveCtrl = 'circle', parentCon = True, connections = False, geoVis = False)
	mc.setAttr('{}.segmentScaleCompensate'.format(Pec1BineJnt), 0)
	controlList.append(Pec1BineGrpCtrlName)

	## Pioint Constraint 'y', 'z'
	mc.pointConstraint('ClavMus2{}Jnt'.format(side), spineJnt, Pec1BineGrpCtrlOfsName, mo = True, skip = 'x')
	mc.setAttr ("{}_pointConstraint1.{}W1".format(Pec1BineGrpCtrlOfsName, spineJnt), 0.5)
	mc.setAttr ("{}_pointConstraint1.ClavMus2{}JntW0".format(Pec1BineGrpCtrlOfsName, side), 0.5)


	## Pioint Constraint 'x' (old skip = ['y', 'z'])
	mc.pointConstraint('ClavMus2{}Jnt'.format(side), spineJnt, 'LateSubCtrlOfst{}Grp'.format(side), mo = True, skip = [])
	mc.setAttr ("LateSubCtrlOfst{}Grp_pointConstraint1.{}W1".format( side, spineJnt), 0.5)
	mc.setAttr ("LateSubCtrlOfst{}Grp_pointConstraint1.ClavMus2{}JntW0".format(side, side), 0.5)

	## Generate ikhandle
	clavIkh = '{}{}'.format(elem, side)
	clavIkh = utaCore.createIkh(strJnt = 'ClavMus1{}Jnt'.format(side), endJnt = 'ClavMus2{}Jnt'.format(side)  ,name = clavIkh, sol = 'ikSCsolver')
	clavIkhZro = mc.rename(utaCore.zeroGroup( obj = clavIkh), '{}IkhZro{}Grp'.format(elem, side))

 	## Generate control for upArm
 	clavUpArm = 'clavUpArm{}jnt'.format(side)
 	clavUpArmJnt = mc.createNode('joint', n = clavUpArm)
  	if side == '_L_':
 		mc.delete(mc.parentConstraint('ClavMus2{}Jnt'.format(side), clavUpArmJnt, mo = False))
 		# print clavUpArmJnt, '...L..'
 	elif side == '_R_':
		mc.delete(mc.parentConstraint('ClavMus2{}Jnt'.format(side), clavUpArmJnt, mo = False))
		mc.setAttr('{}.rx'.format(clavUpArmJnt), -360)
		mc.setAttr('{}.ry'.format(clavUpArmJnt), 180)
		pm.makeIdentity ( clavUpArmJnt , apply = True ) ;    

	## Generate Control
	mc.select(clavUpArm)
	clavUpArmGrpCtrlName, clavUpArmGrpCtrlOfsName, clavUpArmGrpCtrlParsName, clavUpArmCtrlName, clavUpArmGmblCtrlName = utaCore.createControlCurve(curveCtrl = 'plus', parentCon = False, connections = False, geoVis = False)
	controlList.append(clavUpArmGrpCtrlName)
	mc.delete(clavUpArm)
	utaCore.parentScaleConstraintLoop(  obj = clavUpArmGmblCtrlName, lists = [clavIkhZro],par = True, sca = True, ori = False, poi = False)	
	mc.parent(clavIkhZro , ClavMusIkh)

	## ParentConstraint clav to upArm Control
	utaCore.parentScaleConstraintLoop(  obj = parent, lists = [clavUpArmGrpCtrlName],par = True, sca = True, ori = False, poi = False)

	## LocakAttr
	utaCore.lockAttr(listObj= [clavUpArmCtrlName],attrs = ( 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v'), lock = True, keyable = False)

	## Generate locator
	spineLoc = mc.spaceLocator(n = 'shldrWuo{}loc'.format(side))[0]
	mc.delete(mc.parentConstraint(spineJnt, spineLoc, mo = False))
	utaCore.parentScaleConstraintLoop(  obj = spineJnt, lists = [spineLoc],par = True, sca = True, ori = False, poi = False)

	## Create Control
	Trap1Jnt = 'Trap1Skin{}Jnt'.format(side)
	trapName, trapSide, trapLastName = utaCore.splitName(sels = [Trap1Jnt])
	mc.select(Trap1Jnt)
	Trap1GrpCtrlName, Trap1GrpCtrlOfsName, Trap1GrpCtrlParsName, Trap1CtrlName, Trap1GmblCtrlName = utaCore.createControlCurve(curveCtrl = 'circle', parentCon = True, connections = False, geoVis = False)
	utaCore.parentScaleConstraintLoop(  obj = 'ArmPointSkin1{}Jnt'.format(side), lists = [Trap1GrpCtrlName],par = True, sca = False, ori = False, poi = False)
	controlList.append(Trap1GrpCtrlName)

	# ## Generate TrapBck Control Vis
	# ctrlVis = utaCore.addAttrCtrl (ctrl = clavUpArmCtrlName, elem = ['{}_CtrlVis'.format(trapName)], min = 0, max = 1, at = 'long',dv = 0)[0]
	# mc.connectAttr('{}.{}'.format(clavUpArmCtrlName, ctrlVis), '{}.v'.format(Trap1GrpCtrlName))

	## Generate spineLoc aimConstraint
	aimCon = mc.aimConstraint(neckJnt, Trap1GrpCtrlOfsName, aim=(1, 0, 0), u=(0, 1, 0), wut="objectrotation", wu=(0, 1, 0), wuo = spineLoc, mo=True)[0]
	aimCon = mc.aimConstraint(neckJnt, 'ScapRoot1{}Jnt'.format(side), aim=(1, 0, 0), u=(0, 1, 0), wut="objectrotation", wu=(0, 1, 0), wuo = spineLoc, mo=True)[0]

	# ## Create Control
	Scap1SkinJnt = 'Scap1Skin{}jnt'.format(side)
	## Generate spineLoc aimConstraint
	generateAim(	objAimBase = spineJnt, 
					objAim = Scap1SkinJnt, 
					objUp = spineLoc,
					aim=(1, 0, 0), 
					u=(0, 1, 0), 
					wu=(0, 1, 0))

	# aimCon = mc.aimConstraint(spineJnt, Scap1SkinJnt, aim=(1, 0, 0), u=(0, 1, 0), wut="objectrotation", wu=(0, 1, 0), wuo = spineLoc, mo=True)[0]

	## Generate UpArmRootAdjSkin Rig
	## Generate trapSkin Bck Rig
	trapSkinBck(	ctrl = clavUpArmCtrlName,
					side = side,
					trapSkinBckJnt = ['Trap1Bck{}Jnt'.format(side), 'Trap2Bck{}Jnt'.format(side), 'trap3SkinBck{}Jnt'.format(side)],
					parent1 = Scap1SkinJnt,
					parent2 = spineJnt,
					ClavMus1Jnt = 'ClavMus1{}Jnt'.format(side),
					# upArmJnt = 'UpArm_L_Jnt'
					valueXAmp = [0, -1.6, -0.8])

	# UpArmRootAdjSkin(	ctrl = 'clavUpArm{}Ctrl'.format(side),
	# 					side = side,
	# 					upArmJnt = 'UpArm{}Jnt'.format(side),
	# 					UparmRootAdjSkinJnt = 'UpArmRootAdjSkin{}jnt'.format(side))
							
	## Generate Setrange node for clav front to back
	generateSetRange(	ctrl = clavUpArmCtrlName,
						side = side,
						elem = 'Scap',
						driver = 'ClavMus1{}Jnt'.format(side), 
						driverAxis = 'rotateY', 
						driven = Scap1SkinJnt,
						drivenAxis = 'translateX')

	## Wrap Group
	mc.parent(spineLoc, ClavMusStill)
	mc.parent(controlList , ClavMusCtrl)
	if mc.objExists(ClavMusSpineRoot):
		mc.parent(ClavMusSpineRoot, ClavMusSkin)

	if mc.objExists(skinGrp):
		mc.parent(ClavMusSkin, skinGrp)
	if mc.objExists(ctrlGrp):
		mc.parent(ClavMusCtrl, ctrlGrp)
		utaCore.parentScaleConstraintLoop(  obj = spineJnt, lists = [ClavMusCtrl],par = True, sca = True, ori = False, poi = False)

	if mc.objExists(stillGrp):
		mc.parent(ClavMusStill, stillGrp)

	if mc.objExists(ClavMusIkh):
		mc.parent(ClavMusIkh, ikhGrp)

	if mc.objExists('SpineMusRootJnt_Grp'):
		mc.delete('SpineMusRootJnt_Grp')

	## setColor 
	greenCtrl = [clavUpArmCtrlName]
	yellowCtrl = [Pec1BineCtrlName, Trap1CtrlName]
	utaCore.assignColor(obj = greenCtrl, col = 'green')
	utaCore.assignColor(obj = yellowCtrl, col = 'yellow')


	if not men:
		generateChestRig(elem = elem,ctrlGrp = ctrlGrp, side = side, spineJnt = spineJnt, aimLoc = spineLoc)
	
	## Generate TrapBck Control Vis
	ctrlVis = utaCore.addAttrCtrl (ctrl = clavUpArmCtrlName, elem = ['{}_CtrlVis'.format(trapName)], min = 0, max = 1, at = 'long',dv = 0)[0]
	mc.connectAttr('{}.{}'.format(clavUpArmCtrlName, ctrlVis), '{}.v'.format(Trap1GrpCtrlName))

	## Set 	segmentScaleCompensate Joint
	segMentLists = [Trap1Jnt, Pec1BineJnt]
	for each in segMentLists:
		mc.setAttr('{}.segmentScaleCompensate'.format(each), 0)


	## parentConstraint 
	mc.parentConstraint(parent , 'ClavMus1{}Jnt'.format(side) , mo=1)
	mc.scaleConstraint(parent , 'ClavMus1{}Jnt'.format(side) , mo=1)

def generateChestRig(elem = '', ctrlGrp = '', side = '', spineJnt = '', aimLoc = '',*args):
		if side == '_L_':
			sides = 'L'
		elif side == '_R_':
			sides = 'R'

		print "# Generate >> Breast {}".format(side)
		BreastRRig = fkRig.Run(              
										name       = 'Breast' ,
										tmpJnt     = [ 	'Breast1{}TmpJnt'.format(side), 
														'Breast2{}TmpJnt'.format(side) , 
														'Breast3{}TmpJnt'.format(side) ] ,
										parent     = 'Pec1Skin{}Jnt'.format(side) ,
										ctrlGrp    = ctrlGrp ,
										axis       = 'y' ,
										shape      = 'circle' ,
										side       = sides ,
										size       = 1)
		mc.delete('Breast1{}TmpJnt'.format(side))
		# Generate >> Fix some node
		mc.setAttr("Pec1SkinCtrlOfst{}Grp_pointConstraint1.Spine5_JntW1".format(side), 0)
		mc.setAttr("Pec1SkinCtrlOfst{}Grp_pointConstraint1.ClavMus2{}JntW0".format(side, side), 0)
		# mc.delete('BreastCtrl{}Grp_scaleConstraint1'.format(side))

		generateSetRange(	ctrl = 'Pec1Skin{}Ctrl'.format(side),
	                                side = sides,
	                                elem = 'MilkInOut',
	                                driver = 'ClavMus1{}Jnt'.format(side), 
	                                driverAxis = 'rotateY', 
	                                driven = 'Pec1SkinCtrlZro{}Grp'.format(side),
	                                drivenAxis = 'translateX')

		generateSetRange(	ctrl = 'Breast1{}Ctrl'.format(side),
	                                side = sides,
	                                elem = 'MilkUpDn',
	                                driver = 'ClavMus1{}Jnt'.format(side), 
	                                driverAxis = 'rotateZ', 
	                                driven = 'Breast1CtrlOfst{}Grp'.format(side),
	                                drivenAxis = 'translateZ')

		# print 'c'
		generateAim(objAimBase = spineJnt, objAim = 'Pec1SkinCtrlZro{}Grp'.format(side), objUp = aimLoc,aim=(1, 0, 0), u=(0, 1, 0), wu=(0, 1, 0), skip = [])
		generateAim(objAimBase = spineJnt, objAim = 'Breast1CtrlOfst{}Grp'.format(side), objUp = aimLoc,aim=(1, 0, 0), u=(0, 1, 0), wu=(0, 1, 0), skip = ['y', 'z'])
		# fix aim Constraint Node
		aimRevNode = utaCore.checkConnect(object = 'Breast1CtrlOfst{}Grp'.format(side), attr = 'rx')[0]
		mc.disconnectAttr(aimRevNode, 'Breast1CtrlOfst{}Grp.rx'.format(side))
		aimRevMdv = mc.createNode('multiplyDivide', n = '{}AimRev{}Mdv'.format(elem, side))
		mc.setAttr('{}.input2X'.format(aimRevMdv), -2)
		mc.connectAttr(aimRevNode, '{}.input1X'.format(aimRevMdv))
		mc.connectAttr('{}.outputX'.format(aimRevMdv), 'Breast1CtrlOfst{}Grp.rotateX'.format(side))

		## setAttribute 'Breast', 'Pec1Skin'
		objLists = ['Breast1', 'Pec1Skin', 'clavUpArm']
		breastValue = [0.15, 1.5, 5, -100, 400]
		Pec1SkinValue = [0.25, -0.15, 20, -100, 400]
		clavUpArm = [-0.8, -0.9, 9.1, -100, 200,]
		attrLists = ['minZ', 'maxX', 'oldMinX', 'oldMinZ', 'oldMaxX']
		for each in objLists:
			if 'Breast' in each:
				for i in range(len(attrLists)):
					mc.setAttr("{}{}CtrlShape.{}".format(each, side, attrLists[i]), breastValue[i])
			elif 'Pec1Skin' in each:
				for i in range(len(attrLists)):
					mc.setAttr("{}{}CtrlShape.{}".format(each, side, attrLists[i]), Pec1SkinValue[i])
			elif 'clavUpArm' in each:
				for i in range(len(attrLists)):
					mc.setAttr("{}{}CtrlShape.{}".format(each, side, attrLists[i]), clavUpArm[i])
	

def generateAim(objAimBase = '', objAim = '', objUp = '', aim=(1, 0, 0), u=(0, 1, 0), wu=(0, 1, 0), skip=[],*args):
	aimCon = mc.aimConstraint(objAimBase, objAim, aim=aim, u=u, wut="objectrotation", wu=wu, wuo = objUp, mo=True, skip = skip)[0]
	return aimCon

def generateSetRange(	ctrl = '',
						side = '',
						elem = '',
						driver = 'ClavMus1_L_Jnt', 
						driverAxis = 'rotateY', 
						driven = 'Scap1Skin_L_jnt',
						drivenAxis = 'translateX',*args):
	## Split Name
	name, side, lastName = utaCore.splitName(sels = [ctrl])
	utaCore.lockAttr(listObj = [driven], attrs = ['tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v'],lock = False, keyable = True)

	## Add Attribute shape
	attrAmp = ['minX', 'minY', 'minZ', 'maxX', 'maxY', 'maxZ', 'oldMinX', 'oldMinY', 'oldMinZ', 'oldMaxX', 'oldMaxY', 'oldMaxZ']
	utaCore.addAttrCtrl (ctrl = '{}'.format(ctrl), elem = ['{}{}OnOff'.format(name, elem)], min = 0, max = 1, at = 'long',dv = 1)
	utaCore.attrNameTitle(obj = '{}Shape'.format(ctrl) , ln = '{}{}_Fun'.format(name, elem.capitalize()))
	utaCore.addAttrCtrl (ctrl = '{}Shape'.format(ctrl), elem = attrAmp, min = -1000, max = 1000, at = 'float',dv = 0)

	## Generate Setrange node for clav front to back
	ClavMusSrg = mc.createNode('setRange', n = '{}Mus{}Srg'.format(name, side))
	ClavMusCon = mc.createNode('condition', n = '{}Mus{}con'.format(name, side))

	## SetAttribute Default
	mc.setAttr ("{}.operation".format(ClavMusCon), 2)
	mc.setAttr ("{}Shape.oldMinX".format(ctrl), 5)
	mc.setAttr ("{}Shape.oldMinZ".format(ctrl), -100)
	mc.setAttr ("{}Shape.oldMaxX".format(ctrl), 200)

	drivenValue = mc.getAttr('{}.{}'.format(driven, drivenAxis))
	mc.setAttr ("{}Shape.minX".format(ctrl), drivenValue)
	mc.setAttr ("{}Shape.minZ".format(ctrl), -3)
	mc.setAttr ("{}Shape.maxX".format(ctrl), -2.8)
	mc.setAttr ("{}Shape.maxZ".format(ctrl), drivenValue)

	## Connect Attribute
	for each in attrAmp:
		if side == '_L_':
			if 'maxX' in each:
				mirrorValuePosiMdv = mc.createNode('multiplyDivide', n = '{}{}MusPosi{}Mdv'.format(name, each, side))
				mc.setAttr('{}.input2X'.format(mirrorValuePosiMdv), -1)
				mc.connectAttr('{}Shape.{}'.format(ctrl, each), '{}.input1X'.format(mirrorValuePosiMdv))
				mc.connectAttr('{}.outputX'.format(mirrorValuePosiMdv), '{}.{}'.format(ClavMusSrg, each))
			else:
				mc.connectAttr('{}Shape.{}'.format(ctrl, each), '{}.{}'.format(ClavMusSrg, each))

		elif side == '_R_' :
			if 'minZ' in each:
				mirrorValueMdv = mc.createNode('multiplyDivide', n = '{}{}Mus{}Mdv'.format(name, each, side))
				mc.setAttr('{}.input2X'.format(mirrorValueMdv), -1)
				mc.connectAttr('{}Shape.{}'.format(ctrl, each), '{}.input1X'.format(mirrorValueMdv))
				mc.connectAttr('{}.outputX'.format(mirrorValueMdv), '{}.{}'.format(ClavMusSrg, each))
			else:
				mc.connectAttr('{}Shape.{}'.format(ctrl, each), '{}.{}'.format(ClavMusSrg, each))

		## Lock Attribute minX, maxZ
		if 'minX' in each or 'maxZ' in each:
			utaCore.lockAttr(listObj = ['{}Shape'.format(ctrl)], attrs = [each],lock = True, keyable = True)

	mc.connectAttr ("{}.outValueX".format(ClavMusSrg), "{}.colorIfTrueR".format(ClavMusCon))
	mc.connectAttr ("{}.outValueZ".format(ClavMusSrg), "{}.colorIfFalseR".format(ClavMusCon))
	mc.connectAttr ('{}.{}'.format(driver, driverAxis), "{}.firstTerm".format(ClavMusCon))
	mc.connectAttr ('{}.{}'.format(driver, driverAxis), '{}.valueX'.format(ClavMusSrg))
	mc.connectAttr ('{}.{}'.format(driver, driverAxis), '{}.valueZ'.format(ClavMusSrg))

	onOffSwitchBcol = mc.createNode('blendColors', n = '{}{}OnOff{}Bcol'.format(name, elem, side))
	mc.setAttr('{}.color2R'.format(onOffSwitchBcol), drivenValue)
	mc.connectAttr ("{}.{}".format(ctrl, '{}{}OnOff'.format(name,elem)), "{}.blender".format(onOffSwitchBcol))

	mc.connectAttr ("{}.outColorR".format(ClavMusCon), "{}.color1R".format(onOffSwitchBcol))
	mc.connectAttr ("{}.outputR".format(onOffSwitchBcol), "{}.{}".format(driven, drivenAxis))
	

	attr = ['minX', 'minY', 'minZ', 'maxX', 'maxY', 'maxZ', 'oldMinX', 'oldMinY', 'oldMinZ', 'oldMaxX', 'oldMaxY', 'oldMaxZ']


def trapSkinBck(ctrl = '',
				side = '',
				trapSkinBckJnt = ['Trap1Bck_L_Jnt', 'Trap2Bck_L_Jnt', 'trap3SkinBck_L_Jnt'],
				parent1 = 'Scap1Skin_L_jnt',
				parent2 = '',
				ClavMus1Jnt = 'ClavMus1_L_Jnt',
				# upArmJnt = 'UpArm_L_Jnt'
				valueXAmp = [0, -1.6, 0.8],
				*args):

	## name
	name, side, lastName = utaCore.splitName(sels = [trapSkinBckJnt[0]])

	## Trap1Bck #1
	utaCore.parentScaleConstraintLoop(  obj = 'Spine5_Jnt', lists = [trapSkinBckJnt[0]],par = True, sca = False, ori = False, poi = False)
	mc.pointConstraint(parent1, parent2, trapSkinBckJnt[1], mo = True)
	mc.setAttr ("{}_pointConstraint1.{}W1".format(trapSkinBckJnt[1], parent2), 0.5)
	mc.setAttr ("{}_pointConstraint1.{}W0".format(trapSkinBckJnt[1], parent1), 0.5)

	## Trap2Bck #2
	attrAmp = ['translateX', 'translateY', 'translateZ']

	## Add Attribute shape
	utaCore.attrNameTitle(obj = '{}Shape'.format(ctrl) , ln = '{}_Fun'.format(name))
	for attrEach in attrAmp:
		utaCore.addAttrCtrl (ctrl = '{}Shape'.format(ctrl), elem = ['{}_Amp'.format(attrEach)], min = -200, max = 200, at = 'float',dv = 0)

	## Trap2Bck Mdv1
	Trap2BckMdv = mc.createNode('multiplyDivide', n = '{}{}Mdv'.format(name, side))
	mc.connectAttr('{}.rx'.format(ClavMus1Jnt), '{}.input1X'.format(Trap2BckMdv))
	mc.connectAttr('{}.ry'.format(ClavMus1Jnt), '{}.input1Y'.format(Trap2BckMdv))
	mc.connectAttr('{}.rz'.format(ClavMus1Jnt), '{}.input1Z'.format(Trap2BckMdv))

	## Trap2Bck Mdv2
	Trap2BckAvrMdv = mc.createNode('multiplyDivide', n = '{}Avr{}Mdv'.format(name, side))
	mc.connectAttr('{}.outputX'.format(Trap2BckMdv), '{}.input1X'.format(Trap2BckAvrMdv))
	mc.connectAttr('{}.outputX'.format(Trap2BckMdv), '{}.input1Y'.format(Trap2BckAvrMdv))
	mc.connectAttr('{}.outputZ'.format(Trap2BckMdv), '{}.input1Z'.format(Trap2BckAvrMdv))

	for (each, value) in zip(attrAmp, valueXAmp):
		if 'X' in each:
			mc.setAttr('{}Shape.{}_Amp'.format(ctrl, each), value)
			mc.connectAttr('{}Shape.{}_Amp'.format(ctrl, each), '{}.input2X'.format(Trap2BckMdv))
			mc.setAttr('{}.input2X'.format(Trap2BckAvrMdv), 0.01)
		elif 'Y' in each:
			mc.setAttr('{}Shape.{}_Amp'.format(ctrl, each), value)
			mc.connectAttr('{}Shape.{}_Amp'.format(ctrl, each), '{}.input2Y'.format(Trap2BckMdv))
			mc.setAttr('{}.input2Y'.format(Trap2BckAvrMdv), 0.01)
		elif 'Z' in each:
			mc.setAttr('{}Shape.{}_Amp'.format(ctrl, each), value)
			mc.connectAttr('{}Shape.{}_Amp'.format(ctrl, each), '{}.input2Z'.format(Trap2BckMdv))
			mc.setAttr('{}.input2Z'.format(Trap2BckAvrMdv), 0.01)

	## Generate ClampNode
	Trap2BckCmp = mc.createNode('clamp', n = '{}{}Cmp'.format(name, side))
	mc.setAttr('{}.maxR'.format(Trap2BckCmp), 0)
	mc.setAttr('{}.maxG'.format(Trap2BckCmp), 50)
	mc.setAttr('{}.maxB'.format(Trap2BckCmp), 50)
	mc.connectAttr('{}.outputX'.format(Trap2BckAvrMdv), '{}.inputR'.format(Trap2BckCmp))
	mc.connectAttr('{}.outputY'.format(Trap2BckAvrMdv), '{}.inputG'.format(Trap2BckCmp))
	mc.connectAttr('{}.outputZ'.format(Trap2BckAvrMdv), '{}.inputB'.format(Trap2BckCmp))

	## Generate plusMinusAverage
	Trap2BckPma = mc.createNode('plusMinusAverage', n = '{}{}Pma'.format(name, side))
	# mc.connectAttr('{}.outputR'.format(Trap2BckPma), '{}.input2D[0].input2Dx'.format(Trap2BckPma))
	mc.connectAttr('{}.outputG'.format(Trap2BckCmp), '{}.input2D[0].input2Dx'.format(Trap2BckPma))
	mc.connectAttr('{}.outputB'.format(Trap2BckCmp), '{}.input2D[1].input2Dx'.format(Trap2BckPma))
	## Connect Side L or R
	if side == '_R_':
		trapSideRMdv = mc.createNode('multiplyDivide', n = '{}Mirror{}Mdv'.format(name, side))
		mc.setAttr('{}.input2X'.format(trapSideRMdv), -1)
		mc.connectAttr('{}.output2Dx'.format(Trap2BckPma), '{}.input1X'.format(trapSideRMdv))
		mc.connectAttr('{}.outputX'.format(trapSideRMdv), '{}.ty'.format(trapSkinBckJnt[-1]))

	else:
		mc.connectAttr('{}.output2Dx'.format(Trap2BckPma), '{}.ty'.format(trapSkinBckJnt[-1]))

def UpArmRootAdjSkin(	ctrl = 'clavUpArm_L_Ctrl',
						side = '',
						upArmJnt = 'UpArm_L_Jnt',
						UparmRootAdjSkinJnt		 = 'UpArmRootAdjSkin_L_jnt',
						*args):
	## Split Name
	name, side, lastName = utaCore.splitName(sels = [UparmRootAdjSkinJnt])
	axis = ['X', 'Y', 'Z']

	## Generate armRootAdj
	# UparmRootAdjSkinJnt = 'UpArmRootAdjSkin_L_jnt'
	## snap
	mc.delete(mc.parentConstraint(upArmJnt, UparmRootAdjSkinJnt, mo = False))
	mc.makeIdentity (UparmRootAdjSkinJnt , apply = True , jointOrient = False, normal = 1, translate =False, rotate = True, scale = False) 
	mc.parent(UparmRootAdjSkinJnt, upArmJnt)

	## Add attribute rotate Amp
	utaCore.attrNameTitle(obj = '{}Shape'.format(ctrl) , ln = '{}Rot_Fun'.format(name))
	for axisEach in axis:
		utaCore.addAttrCtrl (ctrl = ctrl, elem = ['rotate{}Adj'.format(axisEach)], min = -1000, max = 1000, at = 'float', dv = 0)
		utaCore.addAttrCtrl (ctrl = '{}Shape'.format(ctrl), elem = ['rotate{}_Amp'.format(axisEach)], min = -1000, max = 1000, at = 'float', dv = -0.5)
	## Add attribute scale Amp
	utaCore.attrNameTitle(obj = '{}Shape'.format(ctrl) , ln = '{}Sca_Fun'.format(name))
	for axisEach in axis:
		utaCore.addAttrCtrl (ctrl = ctrl, elem = ['scale{}Adj'.format(axisEach)], min = -1000, max = 1000, at = 'float', dv = 0)
		utaCore.addAttrCtrl (ctrl = '{}Shape'.format(ctrl), elem = ['scale{}_Amp'.format(axisEach)], min = -1000, max = 1000, at = 'float', dv = 0.4)

	## Generate Mdv Node
	UpArmRootAdjShapeRotAmpMdv = mc.createNode('multiplyDivide', n = '{}Rot{}Mdv'.format(name, side))
	UpArmRootAdjShapeScaAmpMdv = mc.createNode('multiplyDivide', n = '{}Sca{}Mdv'.format(name, side))
	UpArmRootAdjShapeScaAvrAmpMdv = mc.createNode('multiplyDivide', n = '{}ScaAvrAmp{}Mdv'.format(name, side))
	UpArmRootAdjShapeScaAmpPma = mc.createNode('plusMinusAverage', n = '{}Sca{}Pma'.format(name, side))
	UpArmRootAdjShapeRotPma = mc.createNode('plusMinusAverage', n = '{}RotAdj{}Pma'.format(name, side))
	UpArmRootAdjRotAvrMdv = mc.createNode('multiplyDivide', n = '{}RotAvr{}Mdv'.format(name, side))
	UpArmRootAdjScaAvrMdv = mc.createNode('multiplyDivide', n = '{}ScaAvr{}Mdv'.format(name, side))
	UpArmRootAdjRotShapeAvrMdv = mc.createNode('multiplyDivide', n = '{}ShapeRotAvr{}Mdv'.format(name, side))

	## Connect Amp
	for axisEach in axis:
		mc.connectAttr('{}.r{}'.format(upArmJnt, axisEach.lower()), '{}.input1{}'.format(UpArmRootAdjShapeRotAmpMdv, axisEach))
		mc.connectAttr('{}.r{}'.format(upArmJnt, axisEach.lower()), '{}.input1{}'.format(UpArmRootAdjShapeScaAmpMdv, axisEach))
		mc.connectAttr('{}Shape.{}'.format(ctrl, 'rotate{}_Amp'.format(axisEach)), '{}.input2{}'.format(UpArmRootAdjShapeRotAmpMdv, axisEach))
		mc.connectAttr('{}Shape.{}'.format(ctrl, 'scale{}_Amp'.format(axisEach)), '{}.input2{}'.format(UpArmRootAdjShapeScaAmpMdv, axisEach))		
		mc.connectAttr('{}.output{}'.format(UpArmRootAdjShapeRotAmpMdv, axisEach), '{}.input1{}'.format(UpArmRootAdjRotShapeAvrMdv, axisEach))
		mc.setAttr('{}.input2{}'.format(UpArmRootAdjRotShapeAvrMdv, axisEach), 0.20)
		mc.connectAttr('{}.output{}'.format(UpArmRootAdjRotShapeAvrMdv, axisEach), '{}.input3D[0].input3D{}'.format(UpArmRootAdjShapeRotPma, axisEach.lower()))
		mc.connectAttr('{}.rotate{}Adj'.format(ctrl, axisEach), '{}.input1{}'.format(UpArmRootAdjRotAvrMdv, axisEach))
		mc.setAttr('{}.input2{}'.format(UpArmRootAdjRotAvrMdv, axisEach), 1)
		mc.connectAttr('{}.output{}'.format(UpArmRootAdjRotAvrMdv, axisEach), '{}.input3D[1].input3D{}'.format(UpArmRootAdjShapeRotPma, axisEach.lower()))
		mc.connectAttr('{}.output3D{}'.format(UpArmRootAdjShapeRotPma, axisEach.lower()), '{}.r{}'.format(UparmRootAdjSkinJnt, axisEach.lower()))
		mc.setAttr('{}.input3D[0].input3D{}'.format(UpArmRootAdjShapeScaAmpPma, axisEach.lower()),1)
		mc.setAttr('{}.input2{}'.format(UpArmRootAdjShapeScaAvrAmpMdv, axisEach),0.01)
		mc.connectAttr('{}.output{}'.format(UpArmRootAdjShapeScaAmpMdv, axisEach), '{}.input1{}'.format(UpArmRootAdjShapeScaAvrAmpMdv, axisEach))
		mc.connectAttr('{}.output{}'.format(UpArmRootAdjShapeScaAvrAmpMdv, axisEach), '{}.input3D[1].input3D{}'.format(UpArmRootAdjShapeScaAmpPma, axisEach.lower()))
		mc.connectAttr('{}.scale{}Adj'.format(ctrl, axisEach), '{}.input1{}'.format(UpArmRootAdjScaAvrMdv, axisEach))
		mc.setAttr('{}.input2{}'.format(UpArmRootAdjScaAvrMdv, axisEach), 0.01)
		mc.connectAttr('{}.output{}'.format(UpArmRootAdjScaAvrMdv, axisEach), '{}.input3D[2].input3D{}'.format(UpArmRootAdjShapeScaAmpPma, axisEach.lower()))
		mc.connectAttr('{}.output3D{}'.format(UpArmRootAdjShapeScaAmpPma, axisEach.lower()), '{}.scale{}'.format(UparmRootAdjSkinJnt, axisEach))

	mc.select( cl = True)