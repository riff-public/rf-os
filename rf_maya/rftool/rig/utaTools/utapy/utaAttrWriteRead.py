import maya.cmds as mc
import json
import os
import shutil
import re
from rf_maya.rftool.rig.rigScript import copyObjFile
reload(copyObjFile)
from rf_utils.context import context_info
reload(context_info)
from utaTools.utapy import utaCore
reload(utaCore)

asset = context_info.ContextPathInfo()

def attrWrite(*args):
    # get all ctrl from selected Char
    listAll = mc.ls(allPaths=True)
    if listAll:
        listCtrl = []
        listJnt = []
        for des in listAll:
            typ = mc.objectType(des)
            if '_Ctrl' in des:
                if typ == 'transform' or typ == 'joint' or typ == 'nurbsCurve':
                    listCtrl.append(des)
            elif '_Jnt' in des:
                if typ == 'joint':
                    listJnt.append(des)


        # get attr and value from all ctrl
        dictCtrl = {}
        dictAttr = {}
        listRemove = ['controlPoints.xValue', 'controlPoints.yValue', 'controlPoints.zValue', 'colorSet.clamped', 'colorSet.representation']
        for ctrl in listCtrl:
            listAttrs = mc.listAttr(ctrl, keyable=True, s=True, unlocked=True)
            if listAttrs:
                for rm in listRemove:
                    if rm in listAttrs:
                        listAttrs.remove(rm)
                for attr in listAttrs:
                    attr = ctrl + '.' + attr
                    value = mc.getAttr(attr)
                    if ':'in attr:
                        attr = attr.split(':')[1] # remove namespace
                    dictAttr[attr] = value
                if ':' in ctrl:
                    ctrl = ctrl.split(':')[1] # remove namespace
                dictCtrl[ctrl] = dictAttr
                dictAttr = {}

        listRemoveMainAttr = ['translateX','translateY','translateZ','rotateX','rotateY','rotateZ','scaleX','scaleY','scaleZ','visibility']
        for jnt in listJnt:
            listAttrs = mc.listAttr(jnt, keyable=True, s=True, unlocked=True)
            if listAttrs:
                for rm in listRemoveMainAttr:
                    if rm in listAttrs:
                        listAttrs.remove(rm)
                for attr in listAttrs:
                    attr = jnt + '.' + attr
                    value = mc.getAttr(attr)
                    if ':'in attr:
                        attr = attr.split(':')[1] # remove namespace
                    dictAttr[attr] = value
                if ':' in jnt:
                    jnt = jnt.split(':')[1] # remove namespace
                dictCtrl[jnt] = dictAttr
                dictAttr = {}

        # write file
        # getPath
        proPath = copyObjFile.getDataFld()
        print proPath, '...proPath..88'
        mainPath = os.path.join(proPath, 'attrCtrl.txt')
        bckupPath = os.path.join(proPath, 'backup_attrCtrl')
        
        with open(mainPath,"w") as file:
            json.dump(dictCtrl, file)
            
        # backup file
        if not os.path.exists(bckupPath):
            os.makedirs(bckupPath)
        listDir = os.listdir(bckupPath)
        if listDir:
            lastVer = re.findall('\\d+',listDir[-1])
            ver = int(lastVer[0])+1
        else:
            ver = 1
        bckupFile = os.path.join(bckupPath, 'attrCtrl_v%03d.txt'%(ver))
        shutil.copy2(mainPath, bckupFile)

        # print 'Backup attrCtrl file . . .'
            
        # print '>>>>>DONE<<<<<'


def attrRead(*args):

    asset = context_info.ContextPathInfo()
    projectPath = os.environ['RFPROJECT']
    fileWorkPath = (asset.path.name()).replace('$RFPROJECT',projectPath)
    # filePath = fileWorkPath.replace('work','publ')
    filePath = fileWorkPath
    path = filePath + '/rig/rigData/attrCtrl.txt'

    with open(path,"r") as file:
            dataCtrl = json.load(file)
    rigGrp = mc.ls('*:Rig_Grp')[0]
    ns = rigGrp.split(':')[0]

    for ctrl, dataAttr in dataCtrl.iteritems():
        for attr, value in dataAttr.iteritems():
            if ns:
                attr = '*:' + attr
            if mc.objExists(attr):
                if mc.listAttr(attr,u=True):
                    try:
                        mc.setAttr(attr, value)
                    except:
                        print 'Can not set {}'.format(attr)
    print '>> Copy Attr is Done!'



def attrReadSelected(*args):
    sel = mc.ls(sl=True)
    if sel:

        asset = context_info.ContextPathInfo()
        projectPath = os.environ['RFPROJECT']
        fileWorkPath = (asset.path.name()).replace('$RFPROJECT',projectPath)
        # filePath = fileWorkPath.replace('work','publ')
        filePath = fileWorkPath
        path = filePath + '/rig/rigData/attrCtrl.txt'

        with open(path,"r") as file:
            dataCtrl = json.load(file)

        ns = sel[0].split(':')[0]

        for ctrl, dataAttr in dataCtrl.iteritems():
            if ns:
                ctrlCheck = ns + ':' + ctrl # ex >> tiny:ALlMover_ctrl
            if ctrlCheck in sel: # add specify selected ctrl
                for attr, value in dataAttr.iteritems():
                    if ns:
                        attr = ns + ':' + attr
                    if mc.objExists(attr):
                        mc.setAttr(attr, value)
    print '>> Copy Attr Selected is Done!'


def attrReadProcess(process = False,*args):
    asset = context_info.ContextPathInfo()
    projectPath = os.environ['RFPROJECT']
    nameSp = utaCore.nameSpaceLists()

    if process:
        fileWorkPath = (asset.path.process()).replace('$RFPROJECT',projectPath)
        path = fileWorkPath + '/rigData/attrCtrl.txt'
        processName = fileWorkPath.split('/')[-1]
    else:
        fileWorkPath = (asset.path.name()).replace('$RFPROJECT',projectPath)
        filePath = fileWorkPath.replace('work','publ')
        path = filePath + '/rig/rigData/attrCtrl.txt'

    with open(path,"r") as file:
            dataCtrl = json.load(file)


    ctrlLists = mc.ls('*:*_Ctrl', '*:*_CtrlShape', '*_Ctrl', '*_CtrlShape')
    for eachCtrl in ctrlLists:
        ns = eachCtrl.split(':')[0]

        for ctrl, dataAttr in dataCtrl.iteritems():
            for attr, value in dataAttr.iteritems():
                if ns:
                    attr = '*:' + attr
                if mc.objExists(attr):
                    if mc.listAttr(attr,u=True):
                        try:
                            mc.setAttr(attr, value)
                        except:
                            print 'Can not set {}'.format(attr)
    print '>> Copy attribute Process : {} is Done!'.format(processName)