'''
#-----------------------------------------------------------------------
1. med
2. skel
3. ctrl
4. combRig and (ref)ctrl , paint skinWeight
5. hdRig 
	cut Bsh send model depatement
	1 select face and adsing  shade blue and red at geo
	2 scrips
		2.1 bshTools
			-dupBlendshape()
			-ref facial Control
			-connectBshCtrl( eyeBrowLFT = 'eyeBrowBsh_lft_ctrl' ,
                    eyeBrowRGT = 'eyeBrowBsh_rgt_ctrl' ,
                    eyeLFT = 'eyeBsh_lft_ctrl' ,
                    eyeRGT = 'eyeBsh_rgt_ctrl' ,
                    nose = 'noseBsh_ctrl' ,
                    mouth = 'mouthBsh_ctrl' ,
                    mouthLFT = 'mouthBsh_lft_ctrl' ,
                    mouthRGT = 'mouthBsh_rgt_ctrl' ,
                    faceUpr = 'faceUprBsh_ctrl' ,
                    faceLwr = 'faceLwrBsh_ctrl' ,
                    bshName = 'facialAll_bsh' )

6. hdRig
	# hdRigTmpLoc
	# hdRigCtrl
	# hdRig
7. eyeRig
	# eyeRigTmpLoc
	# eyeRigCtrl
	# eyeRig
8. EarRig
	# earRigLmpLoc
	# earRigCtrl
	# earRig
9. NsRig
	# nsRigLmpLoc
	# nsRigCtrl
	# nsRig
10. TtRig
	# ttRigLmpLoc
	# ttRigCtrl
	# ttRig
11. EbRig
	# ebRigLmpLoc
	# ebRigCtrl
	# ebRig
		# run scrip eyebrowRig.createBsh( sel , 'EbRig')
		# ref ebSkinJntTmp in scenes ebRig
		# rotate groupJnt
		# run scrip lrr.mirrorOrientSelected((0, 0 ,1), (0, 1, 0))
		# weightDivider.run()
		# wait model eyeBrowDn...

#-----------------------------------------------------------------------
	hdGeo 
		EricFclRig_Grp
	hdRig
		HdDfmRigCtrl_Grp		
		HdDfmRigJnt_Grp
		HdDfmRigSkin_Grp
		HdDfmRigStill_Grp
	ebRig
		EbRigCtrl_Grp
		EbRigStill_Grp
	mthRig 
		MthRigCtrl_Grp
		MthRigJnt_Grp
		MthRigSkin_Grp
		MthRigStill_Grp
	nsRig
		NsRigStill_Grp
		NsRigCtrl_Grp
		NsRigSkin_Grp
	earRig
		EarRigStill_Grp
		EarRigCtrl_Grp
		EarRigSkin_Grp
	ttRig
		TtRigCtrl_Grp
		TtRigJnt_Grp
		TtRigSkin_Grp
		TtRigStill_Grp
	dtlRig
		DtlRigStill_Grp
		DtlRigCtrlHi_Grp
		DtlRigCtrlMid_Grp
		DtlRigCtrlLow_Grp
		DtlRigSkin_Grp
	bsh
		LilySRUBshGeo_Grp		
		facailRig_grp


	#local Rig
	ChestArmorALocRig
	ChestArmorFntLocRig
	ChestArmorBakLocRig
	ChestArmor1LocRig_L
	ChestArmor2LocRig_L
	ChestArmor3LocRig_L
	ChestArmor1LocRig_R
	ChestArmor2LocRig_R
	ChestArmor3LocRig_R

	ChestArmorBLocRig_L
	ChestArmorBLocRig_R

	ChestArmorELocRig

	#local Rig Group
		localSkin_Grp
		localStill_Grp
		localCtrl_Grp
			 when ref to combRig, jus create new group "LocRigCtrlPar_Grp, LocRigStillPar_Grp, LocRigSkinPar_Grp"
			 Still an d Skin parent to Still_Grp


# create BlendShape Corrective Mouth
CnrOutDnOpen
	JawOpenShape -1
	cornerOuterDn -1
	CnrOutDnOpenShape 1
CnrOutUpOpen
	JawOpenShape -1
	cornerOuterUp -1
	CnrOutUpOpenShape 1
CnrInDnOpen
	JawOpenShape -1
	cornerInnerDn -1
	cnrInDnOpenShape 1
CnrInUpOpen
	JawOpenShape -1
	cornerInnerUp -1
	CnrInUpOpenShape 1
CnrOutOpen
	JawOpenShape -1
	cornerOut -1
	CnrOutOpenShape 1
CnrInOpen
	JawOpenShape -1
	cornerIn -1
	CnrInOpenShape 1
CnrDnOpen
	JawOpenRef20 -1
	cornerDn -1
	CnrDnOpenRefShape 1
CnrUpOpen
	JawOpenRef20 -1
	cornerUp -1
	CnrUpOpenRefShape 1
JawOpen
	JawOpenRef20 -1
	JawOpenShape 1
JawOpenMth
	JawOpenRef20 -1
	JawOpenMthShape 1
'''







