import maya.cmds as mc

def deleteGrp(obj = ''):
	if mc.objExists (obj):
		sel = obj
	else:
		sel = mc.ls(sl = True)
	if mc.objExists('Delete_Grp'):
	    mc.parent(sel,'Delete_Grp')

	else:
	    Grp = mc.createNode('transform', n = 'Delete_Grp')
	    mc.parent(sel,Grp)
