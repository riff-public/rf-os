#Import python modules
import os, sys
import sqlite3
from collections import Counter

#Import GUI
from PySide import QtCore
from PySide import QtGui
from PySide import QtUiTools
from shiboken import wrapInstance

# Import Maya module
import maya.OpenMayaUI as mui
import maya.cmds as mc
import maya.mel as mm

moduleFile = sys.modules[__name__].__file__
moduleDir = os.path.dirname(moduleFile)
print moduleFile
print moduleDir
sys.path.append(moduleDir)


def getMayaWindow():
    ptr = mui.MQtUtil.mainWindow()
    if ptr is not  None:
        # ptr = mui.MQtUtil.mainWindow()
        return wrapInstance(long(ptr), QtGui.QMainWindow)

class MyForm(QtGui.QMainWindow):

    def __init__(self, parent=None):
        self.count = 0
        #Setup Window
        super(MyForm, self).__init__(parent)
        
        self.mayaUI = 'FacialAndToolsUi'
        # deleteUI(self.mayaUI)
        
        # read .ui directly
        loader = QtUiTools.QUiLoader()
        loader.setWorkingDirectory(moduleDir)
        
        f = QtCore.QFile("%s/FacialAndToolsUi.ui" % moduleDir)
        f.open(QtCore.QFile.ReadOnly)
        
        self.myWidget = loader.load(f, self)
        self.ui = self.myWidget
        f.close()
        
        self.ui.show()
        self.ui.setWindowTitle('Facial And Utilities v 0.5')