import maya.cmds as mc 
reload(mc)
from lpRig import fkRig
reload(fkRig)

def controlCurve(crvColor='yellow', infl='',elem =''): 

    jointName = mc.ls(sl=True) 
    countCtrl = 1
    for joints in jointName: 

        ctrlCurve = mc.circle(ch=0, n=(elem+ctrlCurve+('_Ctrl'))) 

        mc.delete(mc.pointConstraint(joints, ctrlCurve)) 
        mc.delete(mc.orientConstraint(joints,ctrlCurve)) 

        ## create grpFreeze 

        grpFrz = mc.duplicate(ctrlCurve, n=(ctrlCurve[0]+('_Grp'))) 
        mc.delete(mc.listRelatives(grpFrz[0],c=1, shapes=1)) 
        mc.parent(ctrlCurve, grpFrz) 

        ## if orient is on 

        if infl == 'orient': 
            mc.orientConstraint(ctrlCurve, joints) 
            mc.scaleConstraint(ctrlCurve, joints)

        if infl == 'parent': 
            mc.parentConstraint(ctrlCurve,joints) 
            mc.scaleConstraint(ctrlCurve, joints)
        ## color override 

        if crvColor == 'yellow': 
            mc.setAttr((ctrlCurve[0]+(".overrideEnabled")), 1) 
            mc.setAttr((ctrlCurve[0]+(".overrideColor")), 17) 

        elif crvColor == 'blue': 
            mc.setAttr((ctrlCurve[0]+(".overrideEnabled")), 1) 
            mc.setAttr((ctrlCurve[0]+(".overrideColor")), 6) 

        elif crvColor == 'red': 
            mc.setAttr((ctrlCurve[0]+(".overrideEnabled")), 1) 
            mc.setAttr((ctrlCurve[0]+(".overrideColor")), 13)

        countCtrl += 1