import maya.cmds as mc
reload(mc)
sys.path.append(r'P:\lib\local\lpRig')
import rigTools as lrr
reload(lrr)


addBshFacial(
hdRigList = ["HeadHdRig_Geo", "CaruncleHdRig_Geo", "EyebrowHdRig_Geo", "EyelashHdRig_Geo", 
            "EyeballHdRig_L_Geo", "CorneaHdRig_L_Geo", "EyeballHdRig_R_Geo", "CorneaHdRig_R_Geo", 
            "TongueHdRig_Geo", "GumUpHdRig_Geo","GumDnHdRig_Geo", "TeethUpHdRig_Geo", 
            "TeethDnHdRig_Geo"],
fclRigList = ["HeadFclRig_Geo", "CaruncleFclRig_Geo", "EyebrowFclRig_Geo", "EyelashFclRig_Geo", 
            "EyeballFclRig_L_Geo", "CorneaFclRig_L_Geo", "EyeballFclRig_R_Geo", "CorneaFclRig_R_Geo", 
            "TongueFclRig_Geo", "GumUpFclRig_Geo","GumDnFclRig_Geo", "TeethUpFclRig_Geo", 
            "TeethDnFclRig_Geo"])

def addBshFacial (hdRigList = [], fclRigList = []):
    for each in hdRigList:
        for eachs in fclRigList:
            currHdRig = mc.ls("*:%s" % each)
            #currFclRig = mc.ls('{fclRig}'.format(eachs = eachs))
            #print currHdRig 
            #print currHdRig   
            mc.select(currHdRig[0])
            mc.select(eachs, add=True)
            lrr.doAddBlendShape()
       