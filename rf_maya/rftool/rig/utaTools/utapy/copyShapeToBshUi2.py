import pymel.core as pm 

def addTextGeoBase(*args):
    sel = pm.ls(sl=True) ;
    sel = sel [0] ;
    pm.textField ( "txtFieldGeoBase" , e = True  , text = sel ) ;
    
def windowUI ( *args ) :
    
    # check if window exists
    if pm.window ( 'copyShapeWinUi' , exists = True ) :
        pm.deleteUI ( 'copyShapeWinUi' ) ;
    else : pass ;
   
    window = pm.window ( 'copyShapeWinUi', title = "copyShapeToBsh v. 1.0" ,
        mnb = False , mxb = False , sizeable = True , rtf = True ) ;
        
    pm.window ( 'copyShapeWinUi' , e = True , w = 300 , h = 125 ) ;
    with window :
    
        mainLayout = pm.rowColumnLayout ( nc = 1 ) ;
        with mainLayout :
            
            rowColumnGeoBase = pm.rowColumnLayout ( nc = 2 , w = 300 , cw = [ ( 1 , 180 ) , ( 2 , 120 )] ) ;
            with rowColumnGeoBase :
                textGeoBase = pm.textField ( "txtFieldGeoBase" , text = "geoBase", h = 28, w = 200) 
                pm.button ( 'addGeoBaseToBsh' , label = '<< add ', h = 30 , w = 20 , c = addTextGeoBase , bgc = (0.745098, 0.745098, 0.745098)) ; 
            
            rowClear = pm.rowColumnLayout ( nc = 3 , h = 30, w = 300 ) ;
            with rowClear :
                pm.button ( 'copyShapeToBsh' , label = ' Revert to GeoBase ', h = 30 , w = 300, c = getGeoBase , bgc = (0, 1, 0)) ; 
            
            rowCopyShape_A = pm.rowColumnLayout ( nc = 3 , h = 30, w = 300 ) ;
            with rowCopyShape :
                pm.button ( 'copyShapeToBsh' , label = ' Mouth ', h = 30 , w = 300 , c = copyShapeMouths , bgc = (255 , 120, 120)) ; 
                
            rowColumn2 = pm.rowColumnLayout ( nc = 2 , w = 300 , cw = [ ( 1 , 150 ) , ( 2 , 150 )] ) ;
            with rowColumn2 :
                pm.button ( 'copyShapeToBsh_A' , label = ' Mouth LFT ', h = 30 , w = 150, c = copyShapeMouth_LFT, bgc = (0, 0.74902, 1)) ; 
                pm.button ( 'copyShapeToBsh_B' , label = ' Mouth RGT ', h = 30 , w = 150, c = copyShapeMouth_RGT, bgc = (1, 0.0784314, 0.576471)) ;

    window.show () ;

windowUI ( ) ;