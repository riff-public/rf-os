from utaTools.utapy import utaCore
reload(utaCore)
import pkmel.ribbon as pkr
reload( pkr )
import pkmel.rigTools as rigTools

def blackLine(*args):
	# help( rigTools )
	# create Ribbon ( distanceDimension)
	rbnObj = pkr.RibbonIkHi( 2.5 , 'y+' )  ## pkr.RibbonIkHi  or pkr.RibbonIkLow
	dir( rbnObj )
	# rename Ribbon 
	rigTools.dummyNaming( rbnObj, 
	                      attr='rbn', 
	                      dummy='animaRbn', 
	                      charName='rope', 
	                      elem='', 
	                      side='RGT')