import sys
# sys.path.append(r'C:\Program Files\Basefount\Miarmy\Maya2018\scripts')

import maya.cmds as cmds
from McdGeneral import *
from McdAgentManagerOldGUI import *

def renameMM(name, newCreatureName):
	globalNode = McdGetMcdGlobalNode()
	orgActiveAgentName = cmds.getAttr(globalNode + ".activeAgentName")
	cmds.select(clear = True)
	#get id and name
	agentNode = "Agent_" + name
	#name = cmds.textField("tf_am_agtn" + str(index), q = True, tx = True)
	#try:
		#cmds.select(agentNode)
	#except:
		# ----------- referencing pipeline tracker : block changing agent content name -------------------
		#if isReferenceScene():
			#cmds.confirmDialog(t = "Error", m = "Cannot change the names of referenced agent")
			#return
		#cmds.confirmDialog(t = "Error", m = "Incorrect for naming Agent Node, Miarmy > Miarmy Contents Check.")
		#return
	#result = cmds.promptDialog( title='Modify Name:', message='Enter New Creature Name:', \
							  #button=['OK', 'Cancel'], defaultButton='OK', cancelButton='Cancel', dismissString='Cancel')
	#newCreatureName = ""
	#if result == "OK":
		#newCreatureName = cmds.promptDialog(query=True, text=True)

	#if newCreatureName == "":
		#return

	newAgentNode = "Agent_" + newCreatureName;
	exist = cmds.ls(newAgentNode)
	if exist != [] and exist != None:
		cmds.confirmDialog(t = "Error", m = "New name exist, try another one.")
		raise Exception("New name exist, try another one.")


	allGrpsFullPath = cmds.listRelatives(agentNode, f = True, c = True)
	allGrps = cmds.listRelatives(agentNode, c = True)

	for i in range(len(allGrps)):
		if (allGrps[i].find('_' + name)>0):
			prefix = allGrps[i].split('_' + name)[0]
			newName = prefix + '_' + newCreatureName
			cmds.rename(allGrpsFullPath[i], newName)

	newAgentNode = "Agent_" + newCreatureName
	cmds.rename(agentNode, newAgentNode)

	# not chang setup file:
	allChild = cmds.listRelatives(newAgentNode, c = True)
	allChildFilter = []
	for i in range(len(allChild)):
		if allChild[i].find("Setup_")!=0:
			allChildFilter.append(allChild[i])

	geoGroup = ""
	tempList = []
	for i in range(len(allChildFilter)):
		if allChildFilter[i].find("Geometry_") < 0:
			tempList.append(allChildFilter[i])
		else:
			geoGroup = allChildFilter[i]
	allChildFilter = tempList

	cmds.select(allChildFilter)

	cmds.select(hi = True)
	cmds.select(allChildFilter, d = True)
	selObj = cmds.ls(sl = True, l = True)
	nbSelObj = len(selObj)
	
	maxHi = 0
	hiNameContainer = []
	appended = 0
	totalLevel = 0
	for i in range(100):
		thisLevelElement = []
		for j in range(len(selObj)):
			if selObj[j].count('|') == i:
				thisLevelElement.append(selObj[j])
				appended += 1
		hiNameContainer.append(thisLevelElement)
		if appended == nbSelObj:
			totalLevel = i
			break;

	for i in range(totalLevel):
		i = totalLevel -i;
		for j in range(len(hiNameContainer[i])):
			item = hiNameContainer[i][j];
			realName = item.split('|')[-1]
			if realName.find('_' + name) > 0:
				if realName.split('_' + name)[-1] == "":
					prefix = realName.split('_' + name)[0]
					newName = prefix + '_' + newCreatureName
					cmds.rename(item, newName)
				else:
					dealWithOtherPossible(item, newCreatureName)
			else:
				if realName.find("ActionShell_") < 0:
					dealWithOtherPossible(item, newCreatureName)

	#activate and refresh:
	cmds.setAttr(globalNode + ".activeAgentName", newCreatureName, type = "string")
	
	# find the replace name of geometries:
	allGeoNodes = cmds.listRelatives(geoGroup, c = True, p = False, ad = True, path = True)
	if MIsBlank(allGeoNodes):
		return;
	
	allGeoNodes.reverse()
	allGeoNodesTrans = []
	
	findPart = orgActiveAgentName + "Geo"
	replacePart = newCreatureName + "Geo"
	for i in range(len(allGeoNodes)):
		
		if cmds.nodeType(allGeoNodes[i]) == "mesh":
			fullName = allGeoNodes[i]
			nodeName = allGeoNodes[i].split("|")[-1]
			
			if nodeName.find(findPart) > 0:
				newName = nodeName.replace(findPart, replacePart)
				# print ""
				# print allGeoNodes[i]
				# print newName
				# print fullName
				# print newName
				try:
					cmds.rename(fullName, newName, ignoreShape = True)
				except:
					pass
		else:
			allGeoNodesTrans.append(allGeoNodes[i])
			
			
	allGeoNodes = allGeoNodesTrans
	# the 2nd time, only work with geo trans:
	for i in range(len(allGeoNodes)):
		
		fullName = allGeoNodes[i]
		nodeName = allGeoNodes[i].split("|")[-1]
		
		if nodeName.find(findPart) > 0:
			newName = nodeName.replace(findPart, replacePart)
			# print ""
			# print allGeoNodes[i]
			# print newName
			# print fullName
			# print newName
			try:
				cmds.rename(fullName, newName, ignoreShape = True)
			except:
				pass
	return newCreatureName
# renameMM('test','xxxx')