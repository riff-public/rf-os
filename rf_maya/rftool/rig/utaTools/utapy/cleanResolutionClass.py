import sys
sys.path.append(r'P:\lib\local\utaTools')
sys.path.append(r'P:\lib\local\utaTools\utapy')
import maya.cmds as mc
reload(mc)
import pkrig.rigTools  as rigTools
reload(rigTools)
import maya.mel as mel
import utapy.turnOffSSC  as  turnOffSSC
reload(turnOffSSC)
import utaCore as uc
reload(uc)

class CleanUp(object):
#     " clean Default all Asset "
    def __init__(self, turtleDefaultBakeLayerName):

        self.turtleDefaultBakeLayerName = turtleDefaultBakeLayerName

    # 1. delete unkownode
    def deleteUnkownode(self):
        try:
            mc.lockNode(self.turtleDefaultBakeLayerName,lock=False)
            mc.delete(self.turtleDefaultBakeLayerName)
            print " done "
        except:
            pass
    def imReElement(self):
        # # 2. import reference and delete nameSpace
        rigTools.importRigElementDeleteGrp()


# 1. Run delete unkownode
clean1 = CleanUp(turtleDefaultBakeLayerName='TurtleDefaultBakeLayer')
# clean1.deleteUnkownode()
# # 2. import reference and delete nameSpace
# rigTools.importRigElementDeleteGrp()


# obj1.deleteUnkownode()

    # obj.deleteUnkownode()



# def clean():
#     # ---------------------------------------------------------------
#     # ------------------- clean Default -----------------------------
#     # ---------------------------------------------------------------
#     # 1. delete unkownode
#     sel = mc.ls('TurtleDefaultBakeLayer')
#     for x in sel:
#         if x != 0:
#             mc.lockNode(x,lock=False)
#             mc.delete(x)

#     # 2.import reference and delete nameSpace
#     rigTools.importRigElementDeleteGrp()

#     # 3. clean unused Node 
#     mel.eval('MLdeleteUnused;')

#     # 4. TurnOff SSC
#     turnOffSSC.turnOff()

#     # 5. delete vraySettings
#     sel = mc.ls('vraySettings')
#     for x in sel:
#         if x != 0:
#             mc.lockNode(x,lock=False)
#             mc.delete(x)

#     # 6. clean Delete_grp or 
#     delGrp = mc.ls('Delete_Grp', 'Delete_grp')
#     for x in delGrp:
#         if x != 0:
#             mc.delete(x)

#     # 7. delete layer
#     try:
#         dispLayerStates = [] # Create array to store layer states
#         dispLayers = mc.ls(type = 'displayLayer')
#         # print dispLayers
#         for lyr in dispLayers:
#             dispLayerStates.append([lyr,mc.getAttr('%s.visibility' % lyr)])
#                 # Restore layer visibility
#                 #return dispLayerStates
#                 #loop = 0
#         for lyr in dispLayers:
#             if lyr != 'displayLayer':
#                 #loop += 1
#                 mc.delete(lyr)
#     except:
#         print "'displayLayer' can't Delete"

#     # 8. clean Camera
#     for x in delGrp:
#         if x != 0:
#             uc.cleanCamera()

#     # # ---------------------------------------------------------------
#     # # -------------------   clean Bsn   -----------------------------
#     # # ---------------------------------------------------------------     
#     # # 1.assign shader 'lambert1'
#     # lisObj = mc.ls('FacialBshStill_Grp','FacialBshStill_grp')
#     # for i in range(len(lisObj)):
#     #     mc.select( lisObj[i] )
#     #     mc.hyperShade(lisObj[i], assign ='lambert1')

#     # # ---------------------------------------------------------------
#     # # -------------------   clean Dtl   -----------------------------
#     # # ---------------------------------------------------------------     
#     # # 1. setAttr  inheritsTransform
#     # listInherCtrl = mc.ls('*DtlCtrlZro_lft_grp')
#     # listInherJnt = mc.ls('*DtlJntZro_lft_grp')
#     # for i in range(len(listInherCtrl)):
#     #     mc.setAttr('%s.inheritsTransform' % listInherCtrl[i],0)
#     # for i in range(len(listInherJnt)):    
#     #     mc.setAttr('%s.inheritsTransform' % listInherJnt[i],0)

#     # # 2. delete anather polygon
#     # listPly = mc.ls('eye_rgt_grp','eye_lft_grp','hair_ply','hairBand_ply','tongue_ply')
#     # mc.delete(listPly)
#     # mc.hyperShade(rss=True)

#     # # 3. assign shader 'lambert1'
#     # lisObj = mc.ls('allDtlGeo_grp','dtlGeo_grp','dtlGeo_Grp','headDtl_Grp','dtlGeol_Grp')
#     # for i in lisObj:
#     #     mc.select(lisObj)
#     #     mc.hyperShade(lisObj, assign ='lambert1')

#     print '------ Clean Success!! --------' 
#     print '--------    *(^O^)*    --------' 
#     print '-------------------------------'

# def cleanBsh():
#     # ---------------------------------------------------------------
#     # ------------------- clean Default -----------------------------
#     # ---------------------------------------------------------------
#     cleanResolution.clean()
#     # ---------------------------------------------------------------
#     # -------------------   clean Bsn   -----------------------------
#     # ---------------------------------------------------------------     
#     # 1.assign shader 'lambert1'
#     lisObj = mc.ls('FacialBshStill_Grp','FacialBshStill_grp')
#     for i in range(len(lisObj)):
#         mc.select( lisObj[i] )
#         mc.hyperShade(lisObj[i], assign ='lambert1')

#     # print '----- Clean Bsh Success!! -----' 
#     # print '--------    *(^O^)*    --------' 
#     # print '-------------------------------'
# def cleanDtl()
#     # ---------------------------------------------------------------
#     # ------------------- clean Default -----------------------------
#     # ---------------------------------------------------------------
#     cleanResolution.clean()
#     # ---------------------------------------------------------------
#     # -------------------   clean Dtl   -----------------------------
#     # ---------------------------------------------------------------     
#     # 1. setAttr  inheritsTransform
#     listInherCtrl = mc.ls('*DtlCtrlZro_lft_grp', '*DtlCtrlZro_rgt_grp')
#     listInherJnt = mc.ls('*DtlJntZro_lft_grp', '*DtlJntZro_rgt_grp')
#     for i in range(len(listInherCtrl)):
#         mc.setAttr('%s.inheritsTransform' % listInherCtrl[i],0)
#     for i in range(len(listInherJnt)):    
#         mc.setAttr('%s.inheritsTransform' % listInherJnt[i],0)

#     # 2. delete anather polygon
#     listPly = mc.ls('eye_rgt_grp','eye_lft_grp','hair_ply','hairBand_ply','tongue_ply')
#     mc.delete(listPly)
#     mc.hyperShade(rss=True)

#     # 3. assign shader 'lambert1'
#     lisObj = mc.ls('allDtlGeo_grp','dtlGeo_grp','dtlGeo_Grp','headDtl_Grp','dtlGeol_Grp')
#     for i in lisObj:
#         mc.select(lisObj)
#         mc.hyperShade(lisObj, assign ='lambert1')

# def cleanCombRig():
#     # delete unkownode
#     sel = mc.ls('TurtleDefaultBakeLayer')
#     for x in sel:
#         if x != 0:
#             mc.lockNode(x,lock=False)
#             mc.delete(x)

#     # delete vraySettings
#     sel = mc.ls('vraySettings')
#     for x in sel:
#         if x != 0:
#             mc.lockNode(x,lock=False)
#             mc.delete(x)

#     # delete layer
#     try:
#         dispLayerStates = [] # Create array to store layer states
#         dispLayers = mc.ls(type = 'displayLayer')
#         # print dispLayers
#         for lyr in dispLayers:
#             dispLayerStates.append([lyr,mc.getAttr('%s.visibility' % lyr)])
#                 # Restore layer visibility
#                 #return dispLayerStates
#                 #loop = 0
#         for lyr in dispLayers:
#             if lyr != 'displayLayer':
#                 #loop += 1
#                 mc.delete(lyr)
#     except:
#         print "'displayLayer' can't Delete"

#     # clean unused Node 
#     mel.eval('MLdeleteUnused;')

#     # TurnOff SSC
#     turnOffSSC.turnOff()

#     # clean Camera
#     uc.cleanCamera()

#     # create Delete_Grp
#     delGrp = mc.group(em=True)
#     delGrpNew = mc.rename(delGrp,'Delete_Grp') 

#     # List
#     headJnt = mc.ls('*:head1_jnt')
#     teethUp = mc.ls('*:teeth1_upr_jnt')
#     teethDn = mc.ls('*:teeth1_lwr_jnt')

#     eyeDtl =  mc.ls('*:eye*DtlCtrlZro_*_grp','*:eyeBrow*DtlCtrlZro_*_grp','*:dtlEyelashZro_grp','*:cheekLwrDtlCtrlZro_*_grp','*:cheekUpr*DtlCtrlZro_*_grp')
#     mouthUpDtl = mc.ls('*:mouth1DtlCtrlZro_*_grp','*:mouth2DtlCtrlZro_*_grp','*:mouth3DtlCtrlZro_*_grp','*:mouth4DtlCtrlZro_*_grp')
#     mouthDnDtl = mc.ls('*:mouth5DtlCtrlZro_*_grp','*:mouth6DtlCtrlZro_*_grp','*:mouth7DtlCtrlZro_*_grp')
    
#     # lis Skin Group
#     animGrp = mc.ls('*:anim_grp')
#     stillGrp = mc.ls('*:still_grp')
#     skinGrp = mc.ls('*:skin_grp')
#     jntGrp = mc.ls('*:jnt_grp')
#     ikhGrp = mc.ls('*:ikh_grp')
#     defGrp = mc.ls('*:def_grp')

#     # list Bsh
#     FacialBshStill = mc.ls('*:FacialBshStill_Grp')
#     allDefGeo = mc.ls('allDefGeo_grp')
#     FacialRig = mc.ls('*:FacialRig_Grp')
#     FacialBsh = mc.ls('*:FacialBshCtrlZro_Grp')
#     EyeLidsBsh =mc.ls('*:EyeLidsBshJntZro_Grp')

#     # list Dtl
#     dtlGeo = mc.ls('*:headDtl_Grp','*:dtlGeo_grp','*:allDtlGeo_grp')
#     allDtlCtrl = mc.ls('*:allDtlCtrl_grp')
#     allDtlJnt = mc.ls('|*:allDtlJnt_grp')
#     allDtlNrb = mc.ls('*:allDtlNrb_grp')

#     # connectEyelidFollow
#     uc.eyeLidFollow()

#     # parentConstraint and scaleConstraint Dtl  
#     if eyeDtl :
#         for i in range(len(eyeDtl)):
#             mc.orientConstraint(headJnt,'%s' %eyeDtl[i],mo=True)
#             mc.scaleConstraint(headJnt,'%s' %eyeDtl[i],mo=True)
#         for i in range(len(mouthUpDtl)):
#             mc.orientConstraint(teethUp,'%s' %mouthUpDtl[i],mo=True)
#             mc.scaleConstraint(teethUp,'%s' %mouthUpDtl[i],mo=True)
#         for i in range(len(mouthDnDtl)):
#             mc.orientConstraint(teethDn,'%s' %mouthDnDtl[i],mo=True)
#             mc.scaleConstraint(teethDn,'%s' %mouthDnDtl[i],mo=True)

#     # Parent to Rig
#     mc.parent (allDefGeo, allDtlCtrl, allDtlJnt, allDtlNrb, dtlGeo, FacialBshStill, FacialRig, delGrpNew)

#     # check Object
#     # FacialBshStill parent to Delete_grp and parent scale Constrant
#     for x in FacialBshStill:
#         if x != 0:

#             mc.parentConstraint (defGrp,x,mo=True)
#             mc.scaleConstraint (defGrp,x,mo=True)

#     # allDefGeo_grp parent to Delete_grp and parent scale Constrant
#     for x in allDefGeo:
#         if x != 0:
#             mc.parentConstraint (defGrp,x,mo=True)
#             mc.scaleConstraint (defGrp,x,mo=True)

#     # Bsh parent to Delete_grp and parent scale Constrant
#     for x in FacialRig :
#         if x != 0:
#             mc.parentConstraint (animGrp,x,mo=True)
#             mc.scaleConstraint (animGrp,x,mo=True)
#             for x in FacialBsh :
#                 if x != 0:
#                     mc.parentConstraint (headJnt,x,mo=True)
#                     mc.scaleConstraint (headJnt,x,mo=True)
#                     for x in EyeLidsBsh :
#                         if x != 0:      
#                             mc.parentConstraint (headJnt,x,mo=True)
#                             mc.scaleConstraint (headJnt,x,mo=True)
        
#     # Dtl parent to Delete_grp and parent scale Constrant
#     for x in dtlGeo :
#         if x != 0:
#             mc.parentConstraint (defGrp,x,mo=True)
#             mc.scaleConstraint (defGrp,x,mo=True)
#             for x in allDtlCtrl:
#                 if x != 0:
#                     mc.parentConstraint (animGrp,x,mo=True)
#                     mc.scaleConstraint (animGrp,x,mo=True)
#                     for x in allDtlJnt:
#                         if x != 0:
#                             mc.parentConstraint (defGrp,x,mo=True)
#                             mc.scaleConstraint (defGrp,x,mo=True)
#                             for x in allDtlNrb :
#                                 if x != 0:
#                                     mc.parentConstraint (defGrp,x,mo=True)
#                                     mc.scaleConstraint (defGrp,x,mo=True)

#     print '--- Clean CombRig Success!! ---' 
#     print '--------    *(^O^)*    --------' 
#     print '-------------------------------'
