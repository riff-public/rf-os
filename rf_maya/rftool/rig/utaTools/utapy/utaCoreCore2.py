import maya.cmds as mc
from utaTools.utapy import utaCore
reload(utaCore)
from utaTools.utapy import fkTipRig 
reload(fkTipRig)
import maya.cmds as mc
reload(mc)
from utaTools.utapy import rigTools 
reload(rigTools)
from utaTools.utapy import tailControl 
reload(tailControl)
# from utaTools.utapy.lpRig import core as lrc
# reload(lrc)

# from lpRig import mainGroup
# reload(mainGroup)
################################################################################################
'''
from utaTools.utapy import utaCoreCore
reload(utaCoreCore)
utaCoreCore.run(name = 'Tail' , FkSpansJnt = 20 , IkSpansJnt = 4 )
'''
################################################################################################
class CreateJoint( object ):
    def __init__( self ):
        ## Select Obj 
        self.strJnt = ''
        self.endJnt = ''
        ## Name Obj
        self.skin     = ''
        self.fk       = 'Fk'
        self.ik       = 'Ik'
        self.paths    = 'Paths'
        self.pathsUp  = 'PathsUp'
        ## Joint
        self.skinJntGrp = []
        self.fkJntGrp = []
        self.ikJntGrp = []
        self.pansJntGrp = []
        self.pansUpJntGrp = []
        ## Curve
        self.skinCrv = []
        self.fkCrv = []
        self.ikCrv = []
        self.pansCrv = []
        self.pansUpCrv = []
        ## Spans Crv
        self.skinSpansCrv = []
        self.fkSpansCrv = []
        self.ikSpansCrv = []
        self.pansSpansCrv = []
        self.pansUpSpansCrv = []
        ## mainGroup
        self.ctrlGrp = ''
        self.skinGrp = ''
        self.jntGrp = ''
        self.ikhGrp = ''
        self.stillGrp = ''

        # self.width    = 500.00;
        # self.title    = 'Cloth Setup Tools';
        # self.version  = 1.0;

    def run(self, name, FkSpansJnt, IkSpansJnt, motionsPaths):
        '''run( name = 'nakee', 
                FkSpansJnt = 5, 
                IkSpansJnt = 10, 
                motionsPaths = False)
            >> select strJnt, endJnt
            >> Run
        '''
        print '# Generate >> Select Object'
        self.strJnt, self.endJnt = self.selectObj()

        print '# Generate >> mainGroup'
        self.ctrlGrp , self.skinGrp, self.jntGrp, self.ikhGrp, self.stillGrp = self.mainGrp()

        print '# Generate >> Create joint'
        self.skinJntGrp, self.skinCrv, self.skinSpansCrv= self.createJntOnCrv(selA = self.strJnt, selB = self.endJnt, name = name, elem = self.skin ,spansJnt = IkSpansJnt, radius = 0.2)
        self.fkJntGrp, self.fkCrv, self.fkSpansCrv= self.createJntOnCrv(selA = self.strJnt, selB = self.endJnt, name = name, elem = self.fk ,spansJnt = FkSpansJnt, radius = 0.4)
        self.ikJntGrp, self.ikCrv, self.ikSpansCrv = self.createJntOnCrv(selA = self.strJnt, selB = self.endJnt, name = name, elem = self.ik ,spansJnt = IkSpansJnt, radius = 0.6)
        if motionsPaths:
            self.pansJntGrp, self.pansCrv, self.pansSpansCrv = self.createJntOnCrv(selA = self.strJnt, selB = self.endJnt, name = name, elem = self.paths ,spansJnt = FkSpansJnt, radius = 0.8)
            self.pansUpJntGrp, self.pansUpCrv, self.pansUpSpansCrv = self.createJntOnCrv(selA = self.strJnt, selB = self.endJnt, name = name, elem = self.pathsUp ,spansJnt = FkSpansJnt, radius = 1)

        print '# Generate >> Clean Jnt Select Guide '
        mc.delete(self.strJnt, self.endJnt)

        print '# Generate >> 21. Creating Ik SpindIK Handle'
        self.createIkSpine(name = name)

        print '# Generate >> 22. Creating Bind Skin'
        self.createBindSkin()

        print '# Generate >> 23. Creating Parent Constraint Joint Skin'
        self.parentSkinJoint(name = name, SpansJnt = IkSpansJnt)

        print '# Generate >> 23. Creating Fk Control (circle)'
        self.createCtrlJnt(name = name, tmpJnt = self.fkJntGrp, shape = 'circle', colors = 'yellow')

        # print '# Generate >> 23. Creating Fk Control (square)'
        # self.createCtrlJnt(name = '{}Fk'.format(name), shape = 'square', colors = 'red')


        # print '112. Creating Ik Control (circle)'
        # tailControl.tailControl( elem = '{}Ik'.format(name), 
        #                 zro2 ='Ofst',
        #                 crvType ='circle',
        #                 jntsCount = IkSpansJnt,
        #                 gimbal = True,
        #                 color = 'red')

        
        # ## Create Control and Group  for TailControl
        # print '# Generate >> 24. Creating Tail Ik Control (circle)'
        # self.tailControl(elem = name, zro2='Oft', crvType='circle', jntsCount= FkSpansJnt, gimbal = True)

        # ## Create jointIk_Jnt for Tail 
        # print '25. Creating JointSca at TailIk_Jnt'

        
    def mainGrp(self):
        self.ctrlGrp = mc.group(n = 'Ctrl_Grp', em = True)
        self.skinGrp = mc.group(n = 'Skin_Grp', em = True)
        self.jntGrp = mc.group(n = 'Jnt_Grp', em = True)
        self.ikhGrp = mc.group(n = 'Ikh_Grp', em = True)
        self.stillGrp = mc.group(n = 'Still_Grp', em = True)
        return self.ctrlGrp , self.skinGrp, self.jntGrp, self.ikhGrp, self.stillGrp

    def selectObj(self):
        sels = mc.ls(sl = True)

        if len(sels) == 2:
            strJnt = sels[0]
            endJnt = sels[-1]
        else:
            print 'Please select 2 object'
        return strJnt, endJnt

    def createJntOnCrv(self, selA, selB, name, elem ,spansJnt, radius):
        jntAll = []
        points = []
        cvObj = []

        sels = [selA, selB]
        ## Create Group
        self.crvGrp = mc.group(n = '%s%sCrv_Grp' % (name, elem), em = True)
        self.jntsGrp = mc.group(n = '%s%sJnt_Grp' % (name, elem), em = True)

        # for ix in range(len(countJnt)):
        for each in sels:
            pos = self.getWStransform(each)
            points.append(pos)
        curve = mc.curve(n = '%s%s_Crv' % (name, elem), d=3, ep=[points[0], points[1]])
        mc.parent(curve,self.crvGrp)

        ## ReBuild Curve
        utaCore.reBuildCurve(lists = [curve], spans = spansJnt , degree = 1) 

        for i in range(spansJnt):
            addCv =  '%s.cv[%s]' % (curve, i)
            cvObj.append(addCv)

        for x , each in enumerate(cvObj, 1):
            mc.select(each)
            jnt = utaCore.jointFromLocator()
            jntName = mc.rename(jnt, '%s%s%s_Jnt' % (name, elem,  x))
            mc.setAttr('%s.radius' % jntName, radius)
            jntAll.append(jntName)
            mc.select(cl = True)

        ## Parent Joint and Aim AxisJnt
        mc.select(jntAll)  
        utaCore.parentObj2()
        mc.parent(jntAll[0], self.jntsGrp)
        mc.parent(self.jntsGrp, self.jntGrp)
        mc.parent(self.crvGrp, self.stillGrp)
        # utaCore.parentObj(jntName = '', Side = '', lastName = '')
        # mc.parent('%s%s1_Jnt' % (name, elem), self.jntsGrp)

        return jntAll, curve, spansJnt


    def getWStransform(self, obj):
        transform = mc.xform( obj, q=True, ws=True, t=True )
        return transform


    def createIkSpine(self, name):
        ikhGrp = mc.group(em= True, n = ('%sIkhZro_Grp' % name))
        ## ReBuild Curve
        utaCore.reBuildCurve(   lists = [self.ikCrv], 
                                spans = self.ikSpansCrv , 
                                degree = 3)         
        strCurveIk = mc.ikHandle(   sj = self.ikJntGrp[0], 
                                    ee = self.ikJntGrp[-1], 
                                    c = self.ikCrv, 
                                    n = name + 'Ik' + '_Ikh', 
                                    sol = 'ikSplineSolver', 
                                    ccv = False, 
                                    rootOnCurve = True, 
                                    parentCurve = False)[0]
        mc.parent( strCurveIk, ikhGrp)
        mc.parent( ikhGrp, self.ikhGrp)

    def createBindSkin(self):
        mc.skinCluster(self.fkJntGrp , self.ikCrv, toSelectedBones = True , lockWeights = True, removeUnusedInfluence = False, nurbsSamples = 5)

    def createCtrlJnt(self, name, tmpJnt ,shape , colors):
        tailFkRigObj = fkTipRig.FkRig(  parent = '', 
                                        ctrlGrp = self.ctrlGrp, 
                                        tmpJnt = tmpJnt,
                                        part = name, 
                                        side = '', 
                                        ax = 'y', 
                                        shape = shape,
                                        colors = colors)


    def parentSkinJoint(self, name, SpansJnt):
        for i in range(SpansJnt):
            mc.parentConstraint('{}Ik{}_Jnt'.format(name, i+1), '{}{}_Jnt'.format(name, i+1), mo = True)
            mc.scaleConstraint('{}Ik{}_Jnt'.format(name, i+1), '{}{}_Jnt'.format(name, i+1), mo = True)


    def tailControl(self, elem = '', zro2='',crvType='',jntsCount='', gimbal =True, color = 'red'):
        jntsCounts = int(jntsCount)
        ix= 0
        controlAll = []
        for x in range(0, jntsCounts):

            # Create Control
            ctrls = rigTools.jointControl(crvType=crvType)
            print ctrls, 'ctrls..'
            ctrlsName = mc.rename(ctrls,('{}{}_Ctrl'.format(elem,ix+1)))
            ctrlsZro1 = mc.group(em=True,n=('{}{}Ctrl_Grp'.format(elem,ix+1)))
            ctrlsZro2 = mc.group(em=True,n=('{}{}Ctrl{}Zro_Grp'.format(elem,ix+1,zro2)))
            
            # Set Color Controler
            if color == 'red':
                mc.setAttr('{}.overrideEnabled'.format(ctrlsName), 1)
                mc.setAttr('{}.overrideColor'.format(ctrlsName), 13)
            if color == 'yellow':
                mc.setAttr('{}.overrideEnabled'.format(ctrlsName), 1)
                mc.setAttr('{}.overrideColor'.format(ctrlsName), 17)
            if color == 'green':
                mc.setAttr('{}.overrideEnabled'.format(ctrlsName), 1)
                mc.setAttr('{}.overrideColor'.format(ctrlsName), 14)
            if color == 'blue':
                mc.setAttr('{}.overrideEnabled'.format(ctrlsName), 1)
                mc.setAttr('{}.overrideColor'.format(ctrlsName), 18)

                mc.parent(ctrlsName, ctrlsZro1)
                mc.parent(ctrlsZro1, ctrlsZro2)

            '''if gimbal ==True:
                # Add Gimbal
                gmbl = lrc.addGimbal(ctrlsName)
                gmbl.name = '%s%sGmbl_Ctrl' % (elem,ix+1)
                gmbl.scaleShape(0.75)
                ix+=1
                controlAll.append(ctrlsZro2)'''
        return controlAll

        def addColorsCtrl(self, obj = '', colors = ''):
            # Set Color Controler
            if color == 'red':
                mc.setAttr('{}.overrideEnabled'.format(ctrlsName), 1)
                mc.setAttr('{}.overrideColor'.format(ctrlsName), 13)
            if color == 'yellow':
                mc.setAttr('{}.overrideEnabled'.format(ctrlsName), 1)
                mc.setAttr('{}.overrideColor'.format(ctrlsName), 17)
            if color == 'green':
                mc.setAttr('{}.overrideEnabled'.format(ctrlsName), 1)
                mc.setAttr('{}.overrideColor'.format(ctrlsName), 14)
            if color == 'blue':
                mc.setAttr('{}.overrideEnabled'.format(ctrlsName), 1)
                mc.setAttr('{}.overrideColor'.format(ctrlsName), 18)


# class CreateJoint2( object ):

#     def __init__( self ):
#         self.fk2       = 'Fk2';

#     def ken(self):
#         CreateJoint().jun()
