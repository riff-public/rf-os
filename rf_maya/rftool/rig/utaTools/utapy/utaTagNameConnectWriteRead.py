import maya.cmds as mc
import json
import os
from utaTools.utapy import copyObjFile
reload(copyObjFile)

def tagNameConnectionWrite(*args):

    dictConnect = {}
    # get charName
    sn = mc.file(q=True, sn=True)
    charName = sn.split('/')[5]
    crvName = '%s_Crv' %charName # ex : 'tiny_Crv' , canis_Crv

    # Check _Crv TagName
    crvNameObj = mc.ls(crvName)
    # print crvNameObj, 'crvNameObj'
    if crvNameObj:
        if not crvNameObj:
            rfCrvName = mc.ls('*:%s' % crvName)
            crvName = rfCrvName[0]
 
        # get attrs and destination connection 
        listAttrs = mc.listAttr(crvName , keyable=True, s=True, unlocked=True)
        for attr in listAttrs:
            attr = crvName + '.' + attr
            shades = mc.listConnections(attr, p=True , d=True)
            # print attr
            # print shades
            dictConnect[attr] = shades

        # write file
            # getPath
            proPath = copyObjFile.getDataFld()
            mainPath = os.path.join(proPath, 'attrCrv_connection.txt')
            # print mainPath, 'mainPath'

            #path = 'E:/chaiyachat/Pia_script/rw_attr/attrCrv.txt'
            with open(mainPath,"w") as file:
                json.dump(dictConnect, file)

def tagNameConnectionRead(*args):
    # get charName
    sn = mc.file(q=True, sn=True)
    charName = sn.split('/')[5]
    path = 'P:/SevenChickMovie/asset/publ/char/%s/rig/data/attrCrv_connection.txt' % (charName)
    with open(path,"r") as file:
        dataConnect = json.load(file)

    # get namespace
    ref = mc.ls(typ='reference')
    ns = ''
    # if ref:
    #     rn = ref[0]
    #     print ref[0], 'ref'
    #     ns = rn.replace('RN','')
    #     print ns ,'ns'

    for attr, shades in dataConnect.iteritems():
        con1 = mc.listConnections(attr, d=True ,s=False,p=True)
        con2 = mc.listConnections(attr, d=False ,s=True,p=True)
        if con1:
            for c1 in con1:
                mc.disconnectAttr (attr, c1)
        if con2:
            for c2 in con2:
                mc.disconnectAttr (c2, attr)
        if shades:
            for shade in shades:
                print '##', 'Name :', shade, 'shade'
                # if ns:
                #     shade = ns + ':' + shade 
                #     attr = ns + ':' + attr # plus namespace ex > tiny:tiny_Crv
                #     print attr, 'attr'
                #     print shade, 'shade'

                # con3 = mc.listConnections(shade, d=True ,s=True,p=True)
                # con4 = mc.listConnections(shade, d=False ,s=False,p=True)
                # if con3:
                #     for c3 in con3:
                #         mc.disconnectAttr (shade, c3)
                # if con4:
                #     for c4 in con4:
                #         mc.disconnectAttr (c4, shade)

                mc.connectAttr(attr, shade)
                print '##', attr ,' :: coonect to >> ', shade


