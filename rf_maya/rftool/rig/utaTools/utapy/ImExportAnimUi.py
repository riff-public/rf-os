import os,sys,traceback
# sys.path.append(r"O:\Pipeline\legacy\lib\local\utaTools\utapy")
import maya.cmds as mc;
import pymel.core as pm;
import jointOnCurve5 as joc 
reload(joc)
import createJoint as cj
reload(cj)
import snapPaths as sp 
reload(sp)
import getNameSpace as gns 
reload(gns)
import tailControlRotate as ctr 
reload(ctr)
import locatorSnap as ls
reload(ls)
# from rftool.utils import maya_utils as mu
# reload(mu)
import importExportAnim as iea 
reload(iea)

def ExportAnimUi(*args):
    imExportNodeField = pm.textField ( "txtImExportNodeField" , q  = True , text = True  )
    imExportPathField = pm.textField ( "txtImExportPathField" , q  = True , text = True  )
    iea.export_anim(node =imExportNodeField, path=imExportPathField)
def ImportAnimUi(*args):
    imExportNodeField = pm.textField ( "txtImExportNodeField" , q  = True , text = True  )
    imExportPathField = pm.textField ( "txtImExportPathField" , q  = True , text = True  )
    iea.import_anim(node =imExportNodeField, path=imExportPathField)
   
# UI ---------------------------------------------------------------------------------------------------
# UI ---------------------------------------------------------------------------------------------------
# UI ---------------------------------------------------------------------------------------------------
# UI ---------------------------------------------------------------------------------------------------
# UI ---------------------------------------------------------------------------------------------------
def ImportExportanimUI() :

    # check if window exists
    if pm.window ( 'ImExportUI' , exists = True ) :
        pm.deleteUI ( 'ImExportUI' ) ;
    title = "Im-ExportAnim v1.0 (23-02-2018)" ;
    # create window 
    window = pm.window ( 'ImExportUI', 
                            title = title , 
                            width = 100,
                            mnb = True , 
                            mxb = True , 
                            sizeable = True , 
                            rtf = True ) ;
    windowRowColumnLayout = pm.window ( 'ImExportUI', 
                            e = True , 
                            width = 150);

    # motionTab A-------------------------------------------------------------------------------------------------------------------------------    
    mainMotionTab = pm.tabLayout ( innerMarginWidth = 1 , innerMarginHeight = 1 , p = windowRowColumnLayout ) ;

    MotionTab_A = pm.rowColumnLayout ( "Import - Export", nc = 1 , p = mainMotionTab ) ;
    mainMotionPathTab = pm.rowColumnLayout ( nc = 1 , p = MotionTab_A , w = 200) ;

    motionPathsRowLine = pm.rowColumnLayout ( nc = 1 , p = mainMotionPathTab ) ;

    rowLine_TabA = pm.rowColumnLayout ( nc = 1 , p = motionPathsRowLine ) ;
    mc.tabLayout(p=rowLine_TabA, w= 192)

    # Control Tab ------------------------------------------------------
    # Control Tab ------------------------------------------------------

        # line 1 
    rowLine1 = pm.rowColumnLayout ( nc = 3 , p = motionPathsRowLine , h = 30 , cw = [(1, 10), (2, 180),(3, 10 ) ]);
    pm.rowColumnLayout (p = rowLine1 ,h=8) ;
    pm.text( label='Import And Export Animations' , p = rowLine1, al = "center")  
    pm.rowColumnLayout (p = rowLine1 ,h=8) ;

    rowLine2 = pm.rowColumnLayout ( nc = 3 , p = motionPathsRowLine , h = 30 , cw = [(1, 10), (2, 180), (3, 10) ]) ;
    pm.rowColumnLayout (p = rowLine2 ,h=8) ;
    imExportNodeField = pm.textField( "txtImExportNodeField", text = "Node", p = rowLine2 , editable = True )
    pm.rowColumnLayout (p = rowLine2 ,h=8) ;

    rowLine2 = pm.rowColumnLayout ( nc = 3 , p = motionPathsRowLine , h = 30 , cw = [(1, 10), (2, 180), (3, 10) ]) ;
    pm.rowColumnLayout (p = rowLine2 ,h=8) ;
    imExportPathField = pm.textField( "txtImExportPathField", text = "Path", p = rowLine2 , editable = True )
    pm.rowColumnLayout (p = rowLine2 ,h=8) ;
  
    rowLine2 = pm.rowColumnLayout ( nc = 3 , p = motionPathsRowLine , h = 30 , cw = [(1, 10), (2, 180), (3, 10) ]) ;
    pm.rowColumnLayout (p = rowLine2 ,h=8) ;
    pm.button ('exportAnimTxt', label = " Exports" ,h=25, p = rowLine2 , c = ExportAnimUi, bgc = (0.411765, 0.411765, 0.411765)) ;
    pm.rowColumnLayout (p = rowLine2 ,h=8) ;

    rowLine2 = pm.rowColumnLayout ( nc = 3 , p = motionPathsRowLine , h = 30 , cw = [(1, 10), (2, 180), (3, 10) ]) ;
    pm.rowColumnLayout (p = rowLine2 ,h=8) ;
    pm.button ('importAnimTxt', label = " Import" ,h=25, p = rowLine2 , c = ImportAnimUi, bgc = (0.411765, 0.411765, 0.411765)) ;
    pm.rowColumnLayout (p = rowLine2 ,h=8) ;

    # TabB
    rowLine_TabB = pm.rowColumnLayout ( nc = 1 , p = motionPathsRowLine ) ;
    mc.tabLayout(p=rowLine_TabB, w= 192)

    pm.setParent("..")
    pm.showWindow(window)