class Test(object):
    def __init__(self, driver, driven):
        self.driver = driver ;
        self.driven = driven ;

    def printSelf ( self ) :
    	print ( 'driver : {a} , driven : {b}'.format ( a = self.driver , b = self.driven ) ) ;

test1 = Test ( driver = 'driver_polygon' , driven = 'driven_polygon' ) ;

test1.printSelf () ;


class Employee:
	"ex sample class"
	empCount = 0
	def __init__(self, name, salary):
		self.name = name
		self.salary = salary
		Employee.empCount +=1

	def displayCount (self):
		print "Total Employee %d" % Employee.empCount

	def displayEmployee(self):
		print "Name :" ,self.name , ", salary : ", self.salary

obj1 = Employee ('mindphp', 2500)
obj2 = Employee ('python' , 3500)

obj1.displayCount()
obj1.displayEmployee()