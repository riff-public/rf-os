import maya.cmds as mc
from utaTools.utapy import utaCore
reload(utaCore)
'''	
	from utaTools.utapy import connectMocapXToFacial as cmtf
	reload(cmtf)
	cmtf.connectClip(lists = True, nameSp = '', connectPoselib = True)
	cmtf.connectClip(lists = True, nameSp = '', connectPoselib = False)

'''
def listPoselibs(*args):
	lists = [		'jawOpen',
					'browDown_L',
					'browDown_R',
					'browInnerUp',
					'browOuterUp_L',
					'browOuterUp_R',
					'cheekPuff',
					'cheekSquint_L',
					'cheekSquint_R',
					'eyeBlink_L',
					'eyeBlink_R',
					'eyeLookDown_L',
					'eyeLookDown_R',
					'eyeLookIn_L',
					'eyeLookIn_R',
					'eyeLookOut_L',
					'eyeLookOut_R',
					'eyeLookUp_L',
					'eyeLookUp_R',
					'eyeSquint_L',
					'eyeSquint_R',
					'eyeWide_L',
					'eyeWide_R',
					'jawForward',
					'jawLeft',
					'jawRight',
					'mouthClose',
					'mouthDimple_L',
					'mouthDimple_R',
					'mouthFrown_L',
					'mouthFrown_R',
					'mouthFunnel',
					'mouthLeft',
					'mouthRight',
					'mouthLowerDown_L',
					'mouthLowerDown_R',
					'mouthPress_L',
					'mouthPress_R',
					'mouthPucker',
					'mouthRollLower',
					'mouthRollUpper',
					'mouthShrugLower',
					'mouthShrugUpper',
					'mouthSmile_L',
					'mouthSmile_R',
					'mouthStretch_L',
					'mouthStretch_R',
					'mouthUpperUp_L',
					'mouthUpperUp_R',
					'noseSneer_L',
					'noseSneer_R',
					'tongueOut',
					'tongueOut',
					'transformRotateX',
					'transformRotateY',
					'transformRotateZ',]
	return lists

def connectClip(lists = True, nameSp = '', connectPoselib = True, *args):
	clipReaderNode = 'ClipReader'
	mocapXNode = 'MocapX'
	if lists:
		lists = listPoselibs()

	else:
		obj = mc.ls(sl = True)
		lists = obj

	for each in lists:
		## list Connections
		objCheck = mc.listConnections('{}.{}'.format('ClipReader', each), d = True)
		if objCheck:
			# print objCheck, '..objCheck'
			if len(objCheck) == 1:
				objCheck = objCheck[0]
			else:
				for a in objCheck:
					if 'unitConversion' in a:

						objCheck = a
			if objCheck == mocapXNode:
				ken = mocapXNode
			elif objCheck == clipReaderNode:
				ken = clipReaderNode
			else:
				ken = objCheck
			## Connect and Disconnect
			try:
				if connectPoselib:
					conCheck = mc.listConnections('{}.weight'.format(each), s = True)
					if conCheck:
						print 'Already Connected'
						pass
					else:
						mc.connectAttr ('{}.{}'.format(ken, each), '{}.weight'.format(each), f = True)
				else: 
					# print '{}.{}'.format(ken, each), '{}.weight'.format(each)
					if 'unitConversion' in ken:
						mc.disconnectAttr ('{}.{}'.format(ken, 'output'), '{}.weight'.format(each))
						mc.setAttr('{}.weight'.format(each), 0)
					else:
						mc.disconnectAttr ('{}.{}'.format(ken, each), '{}.weight'.format(each))
						mc.setAttr('{}.weight'.format(each), 0)
			except: pass
		else:
			print "'ClipReader' or 'MocapX' is not ready"
