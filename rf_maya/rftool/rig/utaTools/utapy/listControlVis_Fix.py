def layerControlAttr (*args):
    ## Tital Ctrl Attr -----------------------------------------------------
    titalListAttr = [   'Control', 
                        'Facial', 
                        'Geometry']

    controlListAttr = [ 'MasterVis', 
                        'BlockingVis', 
                        'SecondaryVis', 
                        'AddRigVis']

    facialListAttr = [  'FacialVis', 
                        'DetailVis']

    geometryListGeo = [ 'GeoVis']

    return  titalListAttr, controlListAttr, facialListAttr, geometryListGeo

def listControlVis (*args):
    listMasterAttr = ['Offset_Ctrl'] 

    listBlockAttr = [
                    'RootCtrl_Grp', 
                    'SpineCtrl_Grp',
                    'PelvisCtrl_Grp', 
                    'NeckCtrl_Grp', 
                    'HeadCtrl_Grp', 
                    'ClavCtrl_L_Grp', 
                    'ClavCtrl_R_Grp', 
                    'UpLegIkCtrlZro_L_Grp', 
                    'UpLegIkCtrlZro_R_Grp',
                    'SpineMainCtrl_Grp', 
                    'LegCtrl_L_Grp',
                    'LegCtrl_R_Grp',
                    'ArmCtrlZro_L_Grp',
                    'ArmCtrlZro_R_Grp',
                    'ArmFkCtrl_L_Grp',
                    'ArmFkCtrl_R_Grp,',
                    'UpArmFkCtrlZro_L_Grp',
                    'UpArmFkCtrlZro_R_Grp',
                    'LegCtrlZro_L_Grp',
                    'LegCtrlZro_R_Grp',
                    'LegIkCtrl_L_Grp',
                    'LegIkCtrl_R_Grp',
                ] 

    listSeconAttr = [
                    'EyeCtrl_Grp', 
                    'ArmRbnCtrl_L_Grp', 
                    'ArmRbnCtrl_R_Grp', 
                    'LegRbnCtrlZro_L_Grp', 
                    'UpLegRbnCtrl_L_Grp', 
                    'LowLegRbnCtrl_L_Grp', 
                    'LegRbnCtrlZro_R_Grp', 
                    'UpLegRbnCtrl_R_Grp', 
                    'LowLegRbnCtrl_R_Grp', 
                    'FingerCtrl_L_Grp', 
                    'FingerCtrl_R_Grp', 
                    'NeckRbnCtrl_Grp',
                    'EarCtrl_L_Grp',
                    'EarCtrl_R_Grp',
                    'ArmRbnCtrlZro_L_Grp',
                    'ArmRbnCtrlZro_R_Grp', 
                    'UpArmRbnCtrl_L_Grp', 
                    'UpArmRbnCtrl_R_Grp', 
                    'ForearmRbnCtrl_L_Grp',
                    'ForearmRbnCtrl_R_Grp',
                    'EyeDotSubCtrl_L_Grp',
                    'EyeDotSubCtrl_R_Grp',
                    ]
    listsAddRigAttr = ['AddRigCtrl_Grp']

    listFclAttr = [ 'FacialCtrl_Grp',
                    'JawCtrl_Grp',
                    'IrisSubCtrl_L_Grp', 
                    'IrisSubCtrl_R_Grp',
                    'PupilSubCtrl_L_Grp',
                    'PupilSubCtrl_R_Grp']
                    
    listsDtlAttr = []

    listGeoAttr = ['Geo_Grp']

    return  listMasterAttr ,listBlockAttr, listSeconAttr, listsAddRigAttr ,listFclAttr , listsDtlAttr , listGeoAttr 

