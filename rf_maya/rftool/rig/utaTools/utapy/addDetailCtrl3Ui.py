import maya.cmds as mc
import maya.mel as mm
import ppmel.core as core
import re
reload( core )
import sys
sys.path.append(r'P:\lib\local\utaTools\utapy')
import addDetailCtrl3 
reload(addDetailCtrl3)

def addDetailCtrl3File(*args):
  addDetailCtrl3.runAddDtlCtrlNurb()
    
def runUI() :
    if mc.window( 'dtlCtrlNurb' , exists = True ) :
        mc.deleteUI( 'dtlCtrlNurb' , window = True )
    
    dtlCtrlNurb = mc.window( 'dtlCtrlNurb' , t = 'detailCtrlNurb v.1' )
    form = mc.formLayout( w = 430 , h = 65 )
    nameTX = mc.text( l = 'Name :' , al = 'left' )
    nameTF = mc.textField( 'nameTF' , w = 80 )
    sideTX = mc.text( l = 'Side :' , al = 'left' )
    sideTF = mc.textField( 'sideTF' , w = 80 )
    mc.popupMenu()
    mc.menuItem( l = 'lft' , c = "mc.textField( 'sideTF' , e = True , tx = 'lft' )" )
    mc.menuItem( l = 'rgt' , c = "mc.textField( 'sideTF' , e = True , tx = 'rgt' )" )
    mc.menuItem( l = 'cen' , c = "mc.textField( 'sideTF' , e = True , tx = 'cen' )" )
    shapeTX = mc.text( l = 'Shape :' , al = 'left' )
    shapeTF = mc.textField( 'shapeTF' , w = 80 )
    mc.popupMenu()
    mc.menuItem( l = 'cube' , c = "mc.textField( 'shapeTF' , e = True , tx = 'cube' )" )
    mc.menuItem( l = 'sphere' , c = "mc.textField( 'shapeTF' , e = True , tx = 'sphere' )" )
    mc.menuItem( l = 'square' , c = "mc.textField( 'shapeTF' , e = True , tx = 'square' )" )
    mc.menuItem( l = 'circle' , c = "mc.textField( 'shapeTF' , e = True , tx = 'circle' )" )
    mirrorTX = mc.text( l = 'Mirror :' , al = 'left' )
    mirrorCB = mc.checkBox( 'mirrorCB' , l = '' , v = True )
    processBT = mc.button( 'processBT' , l = 'Process' , h = 35 , w = 426 , c = addDetailCtrl3File )
    
    mc.formLayout( form , e = True , attachForm = [ ( nameTX , 'top' , 7 ) ,
                                                    ( nameTX , 'left' , 5 ) ,
                                                    ( nameTF , 'top' , 3 ) ,
                                                    ( sideTX , 'top' , 7 ) ,
                                                    ( sideTF , 'top' , 3 ) ,
                                                    ( shapeTX , 'top' , 7 ) ,
                                                    ( shapeTF , 'top' , 3 ) ,
                                                    ( mirrorTX , 'top' , 7 ) ,
                                                    ( mirrorCB , 'top' , 8 ) ,
                                                    ( processBT , 'left' , 2 ) 
                                                  ]
                                   , attachControl = [ ( nameTF , 'left' , 3 , nameTX ) ,
                                                       ( sideTX , 'left' , 7 , nameTF ) ,
                                                       ( sideTF , 'left' , 3 , sideTX ) ,
                                                       ( shapeTX , 'left' , 7 , sideTF ) ,
                                                       ( shapeTF , 'left' , 3 , shapeTX ) ,
                                                       ( mirrorTX , 'left' , 7 , shapeTF ) ,
                                                       ( mirrorCB , 'left' , 3 , mirrorTX ) ,
                                                       ( processBT , 'top' , 7 , nameTX )
                                                      ] )
    
    
    mc.showWindow ( 'dtlCtrlNurb' )