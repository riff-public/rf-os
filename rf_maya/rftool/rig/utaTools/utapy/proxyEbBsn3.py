import maya.cmds as mc
import pymel.core as pm
    #---------------------------------------------#
    #               EyebrowBsn R L Ctrl             #
    #---------------------------------------------#
def proxyEbBsn(tyOffset,bodyGeoBsn,*args):    
    ctrlBsns = mc.ls('*:eyeBrowBsh_*_ctrl')
    #bodyGeoBsn = '*:BodyBsh_Geo'
    ebGeoBsn = '*:EyebrowBsh_Geo'
        
    for ctrlBsn in ctrlBsns:
        setCkUprIo = mc.setAttr('%s.allUD' %(ctrlBsn), 10)
    checkEbEachAmount = len(bodyGeoBsn)
    if mc.objExists(checkEbEachAmount) != 0:
        dupBdGeo = mc.duplicate(bodyGeoBsn, n = 'BodyAllEyebrowUp_Geo')
        doGrp = mc.createNode('transform',n = 'AllEyebrowUp')
        mc.parent('BodyAllEyebrowUp_Geo','AllEyebrowUp')
    
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.allUD' %(ctrlBsn), -10)
    dupBdGeo = mc.duplicate(bodyGeoBsn, n = 'BodyAllEyebrowDn_Geo')
    doGrp = mc.createNode('transform',n = 'AllEyebrowDn')
    mc.parent('BodyAllEyebrowDn_Geo','AllEyebrowDn')
    
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.allUD' %(ctrlBsn), 0)
    
    #------------------------------------------------------------------------------#
    for ctrlBsn in ctrlBsns:
        setCkUprIo = mc.setAttr('%s.allIO' %(ctrlBsn), 10)
    dupBdGeo = mc.duplicate(bodyGeoBsn, n = 'BodyAllEyebrowIn_Geo')
    doGrp = mc.createNode('transform',n = 'AllEyebrowIn')
    mc.parent('BodyAllEyebrowIn_Geo','AllEyebrowIn')
    
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.allIO' %(ctrlBsn), -10)
    dupBdGeo = mc.duplicate(bodyGeoBsn, n = 'BodyAllEyebrowOut_Geo')
    doGrp = mc.createNode('transform',n = 'AllEyebrowOut')
    mc.parent('BodyAllEyebrowOut_Geo','AllEyebrowOut')
    
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.allIO' %(ctrlBsn), 0)
        
    #------------------------------------------------------------------------------#
    for ctrlBsn in ctrlBsns:
        setCkUprIo = mc.setAttr('%s.turnUD' %(ctrlBsn), 10)
    dupBdGeo = mc.duplicate(bodyGeoBsn, n = 'BodyAllEyebrowRotUp_Geo')
    doGrp = mc.createNode('transform',n = 'AllEyebrowRotUp')
    mc.parent('BodyAllEyebrowRotUp_Geo','AllEyebrowRotUp')
    
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.turnUD' %(ctrlBsn), -10)
    dupBdGeo = mc.duplicate(bodyGeoBsn, n = 'BodyAllEyebrowRotDn_Geo')
    doGrp = mc.createNode('transform',n = 'AllEyebrowRotDn')
    mc.parent('BodyAllEyebrowRotDn_Geo','AllEyebrowRotDn')
    
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.turnUD' %(ctrlBsn), 0)

    
    #---------------------------------------------#
    
    #---------------------------------------------#
    #               object ot grp                 #
    #---------------------------------------------#
    
    #---------------------------------------------#
    
    prxGrp = mc.createNode('transform',n = 'proxyBsh_Grp')
    
    mc.parent('AllEyebrowUp','AllEyebrowDn','AllEyebrowIn','AllEyebrowOut','AllEyebrowRotUp','AllEyebrowRotDn','proxyBsh_Grp')
    mc.setAttr('%s.tx' %prxGrp, -100)
    
    prxBsnMd = ['AllEyebrowUp','AllEyebrowDn','AllEyebrowIn','AllEyebrowOut','AllEyebrowRotUp','AllEyebrowRotDn']   
    numHead = len(prxBsnMd)
    defaultTy = 0
    for i in range(numHead):
        mc.setAttr((prxBsnMd[i]+'.ty'),defaultTy)
        defaultTy += tyOffset    

