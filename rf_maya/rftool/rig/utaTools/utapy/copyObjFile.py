import shutil 
# import os
import maya.cmds as mc
import pymel.core as pm
import maya.mel as mm
import os, sys

# from rf_utils.context import context_info
from rf_utils.context import context_info
from rf_utils import file_utils

#path project config
rootScript = os.environ['RFSCRIPT']
pathProjectJson = r"{}\core\rf_maya\rftool\rig\export_config\project_config.json".format(rootScript)
projectData = file_utils.json_loader(pathProjectJson)


def getDataFld(*args) : 
	# Project Name 
	asset = context_info.ContextPathInfo()
	projectName = asset.project  

	if projectName in projectData['project']:
		asset.context.update(process='data')
		dataPath = asset.path.publish_hero()
		scn_data , scn_hero = os.path.split(dataPath)
		tmpAry = scn_data.split( '/' )
		tmpAry[-1] = 'data' 
		tmpAry[0] = 'P:'
		dataFld = '\\'.join( tmpAry) 
		if not os.path.isdir( dataFld ) :
			os.mkdir( dataFld ) 

		return dataFld  

 	else :
	    wfn = os.path.normpath(pm.sceneName() )
	    tmpAry = wfn.split( '\\' )
	    tmpAry[-2] = 'data' 
	    dataFld = '\\'.join( tmpAry[0:-1] ) 
	    if not os.path.isdir( dataFld ) :
	        os.mkdir( dataFld ) 
	    return dataFld  

def copyObjFile(src,*args):
    shutil.copy(src, getDataFld())