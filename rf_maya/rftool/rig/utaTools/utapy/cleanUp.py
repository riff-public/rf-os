import maya.cmds as mc
import maya.mel as mm
from utaTools.pkrig import rigTools
reload(rigTools)
from rf_maya.lib import maya_lib
reload(maya_lib)
from utaTools.utapy import utaCore
reload(utaCore)
from rf_utils.context import context_info
reload(context_info)
from utaTools.utapy import listsAsset
reload(listsAsset)


def clearDeleteGrp():
    if mc.objExists ('Delete_Grp'):
        rigTools.importRigElementDeleteGrp()
    else :
        rigTools.importAllReference()
        rigTools.removeAllNamespace()
        # maya_lib.import_ref()
        # maya_lib.remove_namespaces()     
def clearLayer() :
    layers = mc.ls( type = 'displayLayer' )
    if not mc.objExists (layers == []):
        for layer in layers :
            if not layer == 'defaultLayer' :
                attrs = ( 'identification' , 'drawInfo' )
                for attr in attrs :
                    objs = mc.listConnections( '%s.%s' %(layer , attr) , p = True )
                    if objs :
                        for obj in objs :
                            if 'layerManager' in obj :
                                mc.disconnectAttr( obj , '%s.%s' %(layer , attr) )
                            else :
                                mc.disconnectAttr( '%s.%s' %(layer , attr) , obj )                               
                mc.delete( layer )  

def clearUnusedAnim() :
    animCrvs = mc.ls( type='animCurve')
    animKeyTypes = ('animCurveTL', 'animCurveTA', 'animCurveTT', 'animCurveTU')
    if animCrvs:
        # filter only anim curve with time input type and with no connection to "input" channel
        to_delete = [crv for crv in animCrvs if mc.nodeType(crv) in animKeyTypes and not mc.listConnections('%s.input' %crv, s=True, d=False)]
        if to_delete:
            mc.delete( to_delete )   

def clearUnknowNode() :
    try : 
        lists = mc.ls(type= 'unknown')
        if not mc.objExists (lists = []):
            mc.delete(lists)
        else :
            pass
    except TypeError : pass

def clearUnusedNode() :
    mm.eval( 'MLdeleteUnused' )

def clearUnusedShape() :
    types = mc.ls( 'mesh' , 'nurbsSurface' )
    if not mc.objExists (types == []):
        try :
            for type in types :
                shape = mc.ls( type = type )
                
                if len(shape) > 1 :
                    shapes = shape 
                else :
                    shapes = []
                    shapes.append( shape )
                    
                for shp in shapes :
                    if not mc.listConnections( shp , s = False , d = True , type = 'shadingEngine') :
                        mc.delete( shp )
        
        except TypeError : pass
    
def clearVraySettings():
    try:
        sels = mc.ls('vraySettings')
        mc.lockNode(sels,lock=False)
        mc.delete(sels)
    except: 
        pass    

def clearTturnOff():
    sels = mc.ls('*:*_jnt','*:*_Jnt', '*_jnt', '*_Jnt')
    try:
        for i in range(len(sels)):
            turnOffObj = mc.setAttr ('%s.segmentScaleCompensate'%sels[i],0)
    except: pass

def clearInheritsTransform():
    listInherCtrlRef = mc.ls('*:cheek*DtlCtrlZro_*_grp','*:eye*DtlCtrlZro_*_grp','*:mouth*DtlCtrlZro_*_grp','*:nose*DtlCtrlZro_*_grp','*:cheekDtlCtrlZro_*_grp')
    listInherJntRef = mc.ls('*:cheek*DtlJntZro_*_grp','*:eye*DtlJntZro_*_grp','*:mouth*DtlJntZro_*_grp','*:nose*DtlJntZro_*_grp','*:cheekDtlJntZro_*_grp')
    listInherCtrl = mc.ls('cheek*DtlCtrlZro_*_grp','eye*DtlCtrlZro_*_grp','mouth*DtlCtrlZro_*_grp','nose*DtlCtrlZro_*_grp','cheekDtlCtrlZro_*_grp')
    listInherJnt = mc.ls('cheek*DtlJntZro_*_grp','eye*DtlJntZro_*_grp','mouth*DtlJntZro_*_grp','nose*DtlJntZro_*_grp','cheekDtlJntZro_*_grp')
    listsInherAll = []
    for each in listInherCtrlRef:
        if mc.objExists(each):
            mc.setAttr('%s.inheritsTransform' % each,0)
            listsInherAll.append(each)
        else: pass
    for each in listInherCtrl:
        if mc.objExists(each):
            mc.setAttr('%s.inheritsTransform' % each,0)
            listsInherAll.append(each)
        else: pass
    for each in listInherJntRef:
        if mc.objExists(each):
            mc.setAttr('%s.inheritsTransform' % each,0)
            listsInherAll.append(each)
        else: pass
    for each in listInherJnt:
        if mc.objExists(each):
            mc.setAttr('%s.inheritsTransform' % each,0)
            listsInherAll.append(each)
        else: pass

def clearInheritsTransformObj(*args):
    lists = mc.ls(sl = True)
    listsInherAll = []
    for each in lists:
        if mc.objExists(each):
            mc.setAttr('%s.inheritsTransform' % each,0)
            listsInherAll.append(each)
        else: pass

def clearCameraDefault():
    try:
        cams = mc.ls(sl=True)
        camList = []
        for cam in cams:
            mc.camera(cam,e=True,startupCamera=False)
            mc.delete(cam)
            camList.append(cam)
    except: pass

def OnOffVisibilityObj():
    Ctrl_Grp = mc.ls('Ctrl_Grp')
    Skin_Grp = mc.ls('Skin_Grp')
    Jnt_Grp = mc.ls('Jnt_Grp')
    Ikh_Grp = mc.ls('Ikh_Grp')
    Still_Grp = mc.ls('Still_Grp')
    if Ctrl_Grp :
        utaCore.OnOffVisibility(lists = [ Ctrl_Grp[0]], v = 1)
    if Skin_Grp:
        utaCore.OnOffVisibility(lists = [Skin_Grp[0]], v = 0)
    if Jnt_Grp:
        utaCore.OnOffVisibility(lists = [Jnt_Grp[0]], v = 0)
    if Ikh_Grp:
        utaCore.OnOffVisibility(lists = [Ikh_Grp[0]], v = 0)
    if Still_Grp:
        utaCore.OnOffVisibility(lists = [Still_Grp[0]], v = 0)

def cleanShade():
    listObjs = mc.ls(type='mesh')
    if listObjs:
        mc.select(listObjs, r=True)
        mc.hyperShade(assign='lambert1')

def cleanShade_batch():
    listObjs = mc.ls(type='mesh')
    mc.sets(listObjs, e=True, nw=True, fe='initialShadingGroup')

def lockAllMover(controlObj = '',listsObj = [], *args):
    ## Check Asset Name
    asset = context_info.ContextPathInfo() 

    charName = asset.name
    typeObj = asset.type
    projectName = asset.project

    if (projectName == 'Hanuman'):
        ## Set Attribute Scale Y
        controlObj, listsChar, listsProp = listsAsset.listsLockScale(projectName = projectName)
        # for char in unlockScale :
        if typeObj == 'char':
            utaCore.lockAttr(listObj = [controlObj], attrs = ['scaleY'],lock = True ,keyable = True)
        else:
            if listsProp:
                for each in listsProp:
                    if charName == each:
                        utaCore.lockAttr(listObj = [controlObj], attrs = ['scaleY'],lock = False ,keyable = True)

    elif (projectName == 'SevenChickMovie'):
        ## Set Attribute Scale Y
        listsCharName , controlObj, attrScale = listsAsset.listsCharScale()
        for each, value in listsCharName.iteritems():
            if each == charName:
                mc.setAttr('{}.{}'.format(controlObj, attrScale), value)

        ## Lock And Unlock Scale 
        controlObj, listsChar, listsProp = listsAsset.listsLockScale(projectName = projectName)
        # for char in unlockScale :
        if typeObj == 'char':
            utaCore.lockAttr(listObj = [controlObj], attrs = ['scaleY'],lock = True ,keyable = True)
        else:
            if listsProp:
                for each in listsProp:
                    if charName == each:
                        utaCore.lockAttr(listObj = [controlObj], attrs = ['scaleY'],lock = True ,keyable = True)   
                    else:
                        utaCore.lockAttr(listObj = [controlObj], attrs = ['scaleY'],lock = False ,keyable = True)   


        jawCtrlObj = 'JawLwr1_Ctrl.LipsSealL'
        if mc.objExists(jawCtrlObj):
            utaCore.lockAttr(listObj = ['JawLwr1_Ctrl'], attrs = ['LipsSealL'],lock = True,keyable = True)
            utaCore.lockAttr(listObj = ['JawLwr1_Ctrl'], attrs = ['LipsSealR'],lock = True,keyable = True)
            utaCore.lockAttr(listObj = ['JawUprRootSub_Ctrl'], attrs = ['tx', 'ty', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz'],lock = True,keyable = True)

def deleteGrp (DelGrp = [],*args):
    if DelGrp:
        for each in DelGrp:
            if mc.objExists(each):
                mc.delete(each)

def remove_unused_for_skin(skinnode=''):
    infs = mc.skinCluster(skinnode, q=True, inf=True)
    wtinfs = mc.skinCluster(skinnode, q=True, wi=True)
    if not infs:
        return True

    reminfs = []
    if infs:
        for inf in infs:
            if wtinfs:
                if not inf in wtinfs:
                    mc.skinCluster(skinnode, e=True, ri=inf)
                    reminfs.append(inf)
    if reminfs:
        print '%i influences have been removed from %s.' % (len(reminfs), skinnode)
    return True

def remove_all_unused_influences(grp):
    sel = selGeo(grp)
    skins = mc.ls(type='skinCluster')
    for skin in skins:
        if mc.referenceQuery(skin, inr=True):
            continue
        remove_unused_for_skin(skin)
    return True

def selGeo(grp = 'MdGeo_Grp'):
    GeoAll = []
    if mc.objExists(grp):
        obj = mc.listRelatives(grp, ad = True)
        for each in obj:
            if not '_Grp'in each:
                if not 'Orig'in each:          
                    if not 'Shape'in each:       
                        GeoAll.append(each)
    return GeoAll

def checkParameterUV(*args):
    shape = mc.ls('*ctrlShape','*CtrlShape')
    for each in shape:
        attrU = mc.attributeQuery('parameterU',node = each,ex = True)
        attrV = mc.attributeQuery('parameterV',node = each,ex = True)
        if attrU == True:
            utaCore.lockAttr(listObj = [each], attrs = ['parameterU'],lock = True,keyable = True)
        if attrV == True:
            utaCore.lockAttr(listObj = [each], attrs = ['parameterV'],lock = True,keyable = True)

def lockVisRigGrp(*args):
    ## Lock Attribute Visibility 'Rig_Grp'
    utaCore.lockAttr(listObj = ['Rig_Grp'], attrs = ['visibility'],lock = True, keyable = False)

def lockGeoUnselect(*args):
    lists = utaCore.listGeo(sels = 'Geo_Grp', nameing = '_Geo', namePass = '_Grp')
    for each in lists:
        geo = each.split('|')[-1]
        try:
            mc.setAttr("{}Shape.overrideEnabled".format(geo), 1)
            mc.setAttr("{}Shape.overrideDisplayType".format(geo), 2)
        except:pass

