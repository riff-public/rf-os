import maya.cmds as mc
def connectAddRig(*args):
    grpRefs = ['*:Wattle_Grp','*:Nose_Grp','*:HornRootCtrl_Grp','*:HornRootSkin_Grp']
    nGrpRefs = len(grpRefs)
    grpRef = []
    grpAddRig = []
    grpZrs = []
    spGrpZrs1 = []
    spGrpZrs2 = []

    #lists = [a.replace('*:', '') for a in grpRefs]
    #print lists
       
    for i in grpRefs:
        names = i.split(':')[-1]
        #name = mc.replace(names,'')
        grpRef.append(names)

    for a in grpRef:
        grpZr = mc.createNode('transform',n = a)
        grpZrs.append(grpZr)

    for b in range(nGrpRefs):
        mc.parent(grpRefs[b],grpZrs[b])
        
    for c in grpZrs:
        if 'BeltJnt_Grp' in c:pass
        elif 'HornRootSkin_Grp' in c:pass
        elif 'BeltStill_Grp' in c:pass
        elif 'Tube1RblCtrlZr_1_Grp' in c:pass
        elif 'Tube2RblCtrlZr_4_Grp' in c:pass
        elif 'Tube1RblCtrlZr_4_Grp' in c:pass    
        else:
            spGrpZrs1.append(c)

    for g in spGrpZrs1:
        spGrpZr = mc.createNode('transform',n = 'AR'+g)
        spGrpZrs2.append(spGrpZr)
        
    nSpGrpZrs = len(spGrpZrs1)
        
    for h in range(nSpGrpZrs):
        mc.parent(spGrpZrs1[h],spGrpZrs2[h])
        
    mc.parentConstraint('*:HeadUp_Jnt','HornRootCtrl_Grp',mo = True)
    mc.scaleConstraint('*:HeadUp_Jnt','HornRootCtrl_Grp',mo = True)
    mc.parentConstraint('*:HeadUp_Jnt','Nose_Grp',mo = True)
    mc.scaleConstraint('*:HeadUp_Jnt','Nose_Grp',mo = True)

    ##change Main Constraint
    mc.parentConstraint('ctrl:Neck_Jnt','*:Wattle3PntCtrl_L_Grp',mo = True)
    mc.scaleConstraint('ctrl:Neck_Jnt','*:Wattle3PntCtrl_L_Grp',mo = True)
    mc.parentConstraint('ctrl:Jaw1_Jnt','*:Wattle2PntCtrl_L_Grp',mo = True)
    mc.scaleConstraint('ctrl:Jaw1_Jnt','*:Wattle2PntCtrl_L_Grp',mo = True)
    mc.parentConstraint('ctrl:Jaw2_Jnt','*:Wattle1PntCtrl_L_Grp',mo = True)
    mc.scaleConstraint('ctrl:Jaw2_Jnt','*:Wattle1PntCtrl_L_Grp',mo = True)


    mc.setAttr('*:SmallHornBCtrl_L_Grp_parentConstraint1.HeadUp_JntW0',0)
    mc.setAttr('*:SmallHornBCtrl_L_Grp_scaleConstraint1.HeadUp_JntW0',0)
    mc.setAttr('*:SmallHornCCtrl_L_Grp_parentConstraint1.HeadUp_JntW0',0)
    mc.setAttr('*:SmallHornCCtrl_L_Grp_scaleConstraint1.HeadUp_JntW0',0)
    mc.setAttr('*:SmallHornDCtrl_L_Grp_parentConstraint1.HeadUp_JntW0',0)
    mc.setAttr('*:SmallHornDCtrl_L_Grp_scaleConstraint1.HeadUp_JntW0',0)

    mc.setAttr('*:SmallHornBCtrl_R_Grp_parentConstraint1.HeadUp_JntW0',0)
    mc.setAttr('*:SmallHornBCtrl_R_Grp_scaleConstraint1.HeadUp_JntW0',0)
    mc.setAttr('*:SmallHornCCtrl_R_Grp_parentConstraint1.HeadUp_JntW0',0)
    mc.setAttr('*:SmallHornCCtrl_R_Grp_scaleConstraint1.HeadUp_JntW0',0)
    mc.setAttr('*:SmallHornDCtrl_R_Grp_parentConstraint1.HeadUp_JntW0',0)
    mc.setAttr('*:SmallHornDCtrl_R_Grp_scaleConstraint1.HeadUp_JntW0',0)

    mc.parentConstraint('*:HornRootBFkGmbl_3_L_Ctrl','*:SmallHornBCtrl_L_Grp',mo=True)
    mc.parentConstraint('*:HornRootCFkGmbl_2_L_Ctrl','*:SmallHornCCtrl_L_Grp',mo=True)
    mc.parentConstraint('*:HornRootDFkGmbl_2_L_Ctrl','*:SmallHornDCtrl_L_Grp',mo=True)

    mc.parentConstraint('*:HornRootBFkGmbl_3_R_Ctrl','*:SmallHornBCtrl_R_Grp',mo=True)
    mc.parentConstraint('*:HornRootCFkGmbl_2_R_Ctrl','*:SmallHornCCtrl_R_Grp',mo=True)
    mc.parentConstraint('*:HornRootDFkGmbl_2_R_Ctrl','*:SmallHornDCtrl_R_Grp',mo=True)

    mc.parentConstraint('*:Ctrl_Grp','ARWattle_Grp',mo=True)
    mc.parentConstraint('*:Ctrl_Grp','ARNose_Grp',mo=True)
    mc.parentConstraint('*:Ctrl_Grp','ARHornRootCtrl_Grp',mo=True)

    mc.parent('HornRootSkin_Grp','ARWattle_Grp','ARNose_Grp','ARHornRootCtrl_Grp','Delete_Grp')

    print('----Finish----')

