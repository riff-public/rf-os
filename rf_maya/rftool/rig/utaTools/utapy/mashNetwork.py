import maya.cmds as mc
import pymel.core as pm
import MASH.api as mapi
from utaTools.utapy import utaCore
reload(utaCore)

'''

import maya.cmds as mc
from utaTools.utapy import mashNetwork
reload(mashNetwork)
from utaTools.utapy import utaCore
reload(utaCore)
mashNetwork.mashNetwork(	name = 'Kinetic',
                            elem = 'A',
                            side = '',
                            geo = '', 
                            axis = 'Z', 
                            arclength = -800, 
                            objectCount = 70,
                            startFrameSet = 1001, 
                            endFrameSet = 1225,
                            staggerFramesSet = 100)

'''
def mashNetworkUi(*args):

	# check if window exists
	if pm.window ( 'mashNetworkUi' , exists = True ) :
	    pm.deleteUI ( 'mashNetworkUi' ) ;
	title = "M-N-W : Ui v1.0 (22-04-2020)" ;
	# create window 
	window = pm.window ( 'mashNetworkUi', 
	                        title = title , 
	                        width = 100,
	                        mnb = True , 
	                        mxb = True , 
	                        sizeable = True , 
	                        rtf = True ) ;
	windowRowColumnLayout = pm.window ( 'mashNetworkUi', 
	                        e = True , 
	                        h = 100,
	                        width = 225);

	# motionTab A-------------------------------------------------------------------------------------------------------------------------------    
	mainWindow = pm.tabLayout ( innerMarginWidth = 1 , innerMarginHeight = 1 , p = windowRowColumnLayout ) ;
	MotionTab_A = pm.rowColumnLayout ( " Mash Network ", nc = 1 , p = mainWindow ) ;

	## Tab ----------------------------------------------------------------
	rowLine_TabA = pm.rowColumnLayout ( nc = 1 , p = MotionTab_A ) ;

    # line TabA
	rowLine_TabB = pm.rowColumnLayout ( nc = 1 , p = rowLine_TabA ) ;
	mc.tabLayout(p=rowLine_TabB, w= 248)

	rowLine1 = pm.rowColumnLayout ( nc = 7 , p = rowLine_TabA  , cw = [(1, 5), (2, 50),(3, 5 ), (4, 132) ,(5, 5), (6, 52), (7, 5)]);
	## GeoGrp
	pm.rowColumnLayout (p = rowLine1 ,h=8) ;
	pm.text( label='Geo_Grp :' , p = rowLine1, al = "left")  
	pm.rowColumnLayout (p = rowLine1 ,h=2) ;
	pm.textField( "txtFieldGeoGrp", text = "", p = rowLine1 , editable = True )
	pm.rowColumnLayout (p = rowLine1 ,h=8) ;
	mc.button("buttonGeoGrp", l="<<" , p = rowLine1 , c = getGeoGrp)
	pm.rowColumnLayout (p = rowLine1 ,h=8) ;

    # line TabA
	rowLine_TabB = pm.rowColumnLayout ( nc = 1 , p = rowLine_TabA ) ;
	mc.tabLayout(p=rowLine_TabB, w= 248)


	rowLine2 = pm.rowColumnLayout ( nc = 7 , p = rowLine_TabA  , cw = [(1, 5), (2, 50),(3, 5 ), (4, 132) ,(5, 5), (6, 52), (7, 5)]);
	## Name
	pm.rowColumnLayout (p = rowLine2 ,h=8) ;
	pm.text( label='Name :' , p = rowLine2, al = "left")  
	pm.rowColumnLayout (p = rowLine2 ,h=2) ;
	pm.textField( "txtFieldName", text = "", p = rowLine2 , editable = True )
	pm.rowColumnLayout (p = rowLine2 ,h=8) ;
	mc.button("buttonName", l="<<" , p = rowLine2 , c = getName)
	pm.rowColumnLayout (p = rowLine2 ,h=8) ;


	## Elem
	pm.rowColumnLayout (p = rowLine2 ,h=8) ;
	pm.text( label='Elem :' , p = rowLine2, al = "left")  
	pm.rowColumnLayout (p = rowLine2 ,h=2) ;
	pm.textField( "txtFieldElem", text = "", p = rowLine2 , editable = True )
	pm.rowColumnLayout (p = rowLine2 ,h=8) ;
	mc.button("buttonElem", l="<<" , p = rowLine2 , c = getElem)
	pm.rowColumnLayout (p = rowLine2 ,h=8) ;


	rowLine3 = pm.rowColumnLayout ( nc = 9 , p = rowLine_TabA  , cw = [(1, 5), (2, 50),(3, 5 ), (4, 60) ,(5, 20), (6, 50), (7, 5), (8, 54),(9, 5)]);
	## Side	and Axis
	pm.rowColumnLayout (p = rowLine3 ,h=8) ;
	pm.text( label='Side :' , p = rowLine3, al = "left")  
	pm.rowColumnLayout (p = rowLine3 ,h=2) ;

	mc.optionMenu("txtFieldSide", label='' , p = rowLine3)
	mc.menuItem( label='_' )
	mc.menuItem( label='L' )
	mc.menuItem( label='R' )


	# pm.textField( "txtFieldSide", text = "", p = rowLine3 , editable = True )
	pm.rowColumnLayout (p = rowLine3 ,h=8) ;
	pm.text( label='    Axis :' , p = rowLine3, al = "left")  
	pm.rowColumnLayout (p = rowLine3 ,h=2) ;

	mc.optionMenu("txtFieldAxis", label='' , p = rowLine3)
	mc.menuItem( label='X' )
	mc.menuItem( label='Y' )
	mc.menuItem( label='Z' )

	# pm.rowColumnLayout (p = rowLine3 ,h=8) ;

	# ## arclength and objectCount	
	# pm.rowColumnLayout (p = rowLine3 ,h=8) ;
	# pm.text( label='arclength :' , p = rowLine3, al = "left")  
	# pm.rowColumnLayout (p = rowLine3 ,h=2) ;
	# pm.textField( "txtFieldArcLength", text = "", p = rowLine3 , editable = True )
	# pm.rowColumnLayout (p = rowLine3 ,h=8) ;
	# pm.text( label='objectCount :' , p = rowLine3, al = "left")  
	# pm.rowColumnLayout (p = rowLine3 ,h=2) ;
	# pm.textField( "txtFieldObjectCount", text = "", p = rowLine3 , editable = True )
	# pm.rowColumnLayout (p = rowLine3 ,h=8) ;

    # line TabA
	rowLine_TabB = pm.rowColumnLayout ( nc = 1 , p = rowLine_TabA ) ;
	mc.tabLayout(p=rowLine_TabB, w= 248)


	rowLine4 = pm.rowColumnLayout ( nc = 7 , p = rowLine_TabA  , cw = [(1, 5), (2, 82),(3, 5 ), (4, 100) ,(5, 5), (6, 50), (7, 5)]);
	## StartFrame	
	pm.rowColumnLayout (p = rowLine4 ,h=8) ;
	pm.text( label='StartFrame :' , p = rowLine4, al = "left")  
	pm.rowColumnLayout (p = rowLine4 ,h=2) ;
	pm.textField( "txtFieldStartFrame", text = "", p = rowLine4 , editable = True )
	pm.rowColumnLayout (p = rowLine4 ,h=8) ;
	mc.button("buttonStartFrame", l="<<" , p = rowLine4 , c = getStartFrame)
	pm.rowColumnLayout (p = rowLine4 ,h=8) ;

	## EndFrame	
	pm.rowColumnLayout (p = rowLine4 ,h=8) ;	
	pm.text( label='EndFrame :' , p = rowLine4, al = "left")  
	pm.rowColumnLayout (p = rowLine4 ,h=2) ;
	pm.textField( "txtFieldEndFrame", text = "", p = rowLine4 , editable = True )
	pm.rowColumnLayout (p = rowLine4 ,h=8) ;
	mc.button("buttonEndFrame", l="<<" , p = rowLine4 , c = getEndFrame)
	pm.rowColumnLayout (p = rowLine4 ,h=8) ;

	## StaggerFrames	
	pm.rowColumnLayout (p = rowLine4 ,h=8) ;	
	pm.text( label='StaggerFrames :' , p = rowLine4, al = "left")  
	pm.rowColumnLayout (p = rowLine4 ,h=2) ;
	pm.textField( "txtFieldStaggerFrames", text = "30", p = rowLine4 , editable = True )
	pm.rowColumnLayout (p = rowLine4 ,h=8) ;
	mc.button("buttonStaggerFrames", l="*_____*" , p = rowLine4 )
	pm.rowColumnLayout (p = rowLine4 ,h=8) ;

	## TimeScale	
	pm.rowColumnLayout (p = rowLine4 ,h=8) ;	
	pm.text( label='TimeScale :' , p = rowLine4, al = "left")  
	pm.rowColumnLayout (p = rowLine4 ,h=2) ;
	pm.textField( "txtFieldTimeScale", text = "0.250", p = rowLine4 , editable = True )
	pm.rowColumnLayout (p = rowLine4 ,h=8) ;
	mc.button("buttonTimeScale", l="*_____*" , p = rowLine4 )
	pm.rowColumnLayout (p = rowLine4 ,h=8) ;

    # line TabA
	rowLine_TabB = pm.rowColumnLayout ( nc = 1 , p = rowLine_TabA ) ;
	mc.tabLayout(p=rowLine_TabB, w= 248)


	rowLine6 = pm.rowColumnLayout ( nc = 7 , p = rowLine_TabA  , cw = [(1, 5), (2, 52),(3, 5 ), (4, 130) ,(5, 5), (6, 50), (7, 5)]);
	## Curve
	pm.rowColumnLayout (p = rowLine6 ,h=8) ;
	pm.text( label='Curve :' , p = rowLine6, al = "left")  
	pm.rowColumnLayout (p = rowLine6 ,h=2) ;
	pm.textField( "txtFieldCurve", text = "", p = rowLine6 , editable = True )
	pm.rowColumnLayout (p = rowLine6 ,h=8) ;
	mc.button("buttonCurve", l="<<" , p = rowLine6 , c = getCurve)
	pm.rowColumnLayout (p = rowLine6 ,h=8) ;

	rowLine7 = pm.rowColumnLayout ( nc = 9 , p = rowLine_TabA  , cw = [(1, 5), (2, 82),(3, 5 ), (4, 100) ,(5, 5), (6, 22.5), (7, 5), (8, 22.5), (9, 5)]);

	## arcLength	
	pm.rowColumnLayout (p = rowLine7 ,h=8) ;
	pm.text( label='arcLength :' , p = rowLine7, al = "left")  
	pm.rowColumnLayout (p = rowLine7 ,h=2) ;
	pm.textField( "txtFieldArcLength", text = "", p = rowLine7 , editable = True )
	pm.rowColumnLayout (p = rowLine7 ,h=8) ;
	mc.button("buttonArcLengthPo", l="+" , p = rowLine7 , c = getArcLengthPo)
	pm.rowColumnLayout (p = rowLine7 ,h=8) ;
	mc.button("buttonArcLengthNe", l="-" , p = rowLine7 , c = getArcLengthNe)
	pm.rowColumnLayout (p = rowLine7 ,h=8) ;



	rowLine8 = pm.rowColumnLayout ( nc = 7 , p = rowLine_TabA  , cw = [(1, 5), (2, 82),(3, 5 ), (4, 100) ,(5, 5), (6, 50), (7, 5)]);
	## ObjectCount	
	pm.rowColumnLayout (p = rowLine8 ,h=8) ;	
	pm.text( label='ObjectCount :' , p = rowLine8, al = "left")  
	pm.rowColumnLayout (p = rowLine8 ,h=2) ;
	pm.textField( "txtFieldObjectCount", text = "", p = rowLine8 , editable = True )
	pm.rowColumnLayout (p = rowLine8 ,h=8) ;
	mc.button("buttonObjectCount", l="*_____*" , p = rowLine8 , c = getObjectCount)
	pm.rowColumnLayout (p = rowLine8 ,h=8) ;

    # line TabA
	rowLine_TabB = pm.rowColumnLayout ( nc = 1 , p = rowLine_TabA ) ;
	mc.tabLayout(p=rowLine_TabB, w= 248)


	## Create
	rowLine5 = pm.rowColumnLayout ( nc = 3 , p = rowLine_TabA , h = 40 , cw = [(1, 5), (2, 243), (3, 5) ]) ;
	pm.rowColumnLayout (p = rowLine5 ,h=8) ;
	pm.button ('createButton', label = " C R E A T E " ,h=40, p = rowLine5 , c = mashNetworkRun, bgc = (0.411765, 0.411765, 0.411765)) ;
	pm.rowColumnLayout (p = rowLine5 ,h=8) ;

    # line TabA
	rowLine_TabB = pm.rowColumnLayout ( nc = 1 , p = rowLine_TabA ) ;
	mc.tabLayout(p=rowLine_TabB, w= 248)

    # line TabA
	rowLine_TabB = pm.rowColumnLayout ( nc = 1 , p = rowLine_TabA ) ;
	mc.tabLayout(p=rowLine_TabB, w= 248)

	## MashSpineIk
	rowLine5 = pm.rowColumnLayout ( nc = 3 , p = rowLine_TabA , h = 40 , cw = [(1, 5), (2, 243), (3, 5) ]) ;
	pm.rowColumnLayout (p = rowLine5 ,h=8) ;
	pm.button ('spineIkJntButton', label = " S P I N E I K : J N T " ,h=40, p = rowLine5 , c = mashSpineIkRun, bgc = (0.411765, 0.411765, 0.411765)) ;
	pm.rowColumnLayout (p = rowLine5 ,h=8) ;

    # line TabA
	rowLine_TabB = pm.rowColumnLayout ( nc = 1 , p = rowLine_TabA ) ;
	mc.tabLayout(p=rowLine_TabB, w= 248)

    # line TabA
	rowLine_TabB = pm.rowColumnLayout ( nc = 1 , p = rowLine_TabA ) ;
	mc.tabLayout(p=rowLine_TabB, w= 248)

	## Create
	rowLine5 = pm.rowColumnLayout ( nc = 3 , p = rowLine_TabA , h = 40 , cw = [(1, 5), (2, 243), (3, 5) ]) ;
	pm.rowColumnLayout (p = rowLine5 ,h=8) ;
	pm.button ('motionButton', label = " M O T I O N " ,h=40, p = rowLine5 , c = motionsRun, bgc = (0.411765, 0.411765, 0.411765)) ;
	pm.rowColumnLayout (p = rowLine5 ,h=8) ;

    # line TabA
	rowLine_TabB = pm.rowColumnLayout ( nc = 1 , p = rowLine_TabA ) ;
	mc.tabLayout(p=rowLine_TabB, w= 248)

	pm.setParent("..")
	pm.showWindow(window)



def getGeoGrp(*args):
    
    sel = mc.ls(sl=True)
    if sel:
        jnt = sel[0]
        pm.textField( "txtFieldGeoGrp", e=True , text = '%s'%jnt )
    else:
        pm.textField( "txtFieldGeoGrp", e=True , text = '' )

def getName(*args):
    
    sel = mc.ls(sl=True)
    if sel:
        jnt = sel[0]
        pm.textField( "txtFieldName", e=True , text = '%s'%jnt )
    else:
        pm.textField( "txtFieldName", e=True , text = '' )

def getElem(*args):
    
    sel = mc.ls(sl=True)
    if sel:
        jnt = sel[0]
        pm.textField( "txtFieldElem", e=True , text = '%s'%jnt )
    else:
        pm.textField( "txtFieldElem", e=True , text = '' )

def getStartFrame(*args):
    
    sel = mc.playbackOptions(q = True , min = True )
    if sel:
        pm.textField( "txtFieldStartFrame", e=True , text = int(sel))

def getEndFrame(*args):
    
    sel = mc.playbackOptions(q = True , max = True )	
    if sel:
        pm.textField( "txtFieldEndFrame", e=True , text = int(sel))

def getCurve(*args):
    
    sel = mc.ls(sl=True)
    if sel:
        jnt = sel[0]
        pm.textField( "txtFieldCurve", e=True , text = '%s'%jnt )
    else:
        pm.textField( "txtFieldCurve", e=True , text = '' )

def getArcLengthPo(*args):

	txtFieldCurve = pm.textField("txtFieldCurve", q=True, text=True)
	answerArcLength = utaCore.curveArgLength(curve = txtFieldCurve)
	pm.textField( "txtFieldArcLength", e=True , text = '+{}'.format(answerArcLength ))

def getArcLengthNe(*args):

	txtFieldCurve = pm.textField("txtFieldCurve", q=True, text=True)
	answerArcLength = utaCore.curveArgLength(curve = txtFieldCurve)
	pm.textField( "txtFieldArcLength", e=True , text = '-{}'.format(answerArcLength ))

def getObjectCount(*args):

	sel = mc.ls(sl=True)
	if sel:
		jnt = sel[0]
		pm.textField( "txtFieldObjectCount", e=True , text = '%s'%jnt )
	else:
		pm.textField( "txtFieldObjectCount", e=True , text = '' )


def motionsRun(*args):
	txtFieldName = pm.textField("txtFieldName", q=True, text=True)
	txtFieldElem = pm.textField( "txtFieldElem", q=True, text=True)
	txtFieldSide = mc.optionMenu("txtFieldSide", query = True, value = True)
	txtFieldObjectCount = pm.textField( "txtFieldObjectCount", q=True, text=True)
	txtFieldCurve = pm.textField("txtFieldCurve", q=True, text=True) 

	motions(	name = txtFieldName, 
				elem = txtFieldElem, 
				side = txtFieldSide, 
				objectCount = int(txtFieldObjectCount), 
				curve = txtFieldCurve)

	print '>> Create Motion Is Done!!'

def mashNetworkRun(*args):

	txtFieldGeoGrp = pm.textField("txtFieldGeoGrp", q=True, text=True) 
	txtFieldName = pm.textField("txtFieldName", q=True, text=True)
	txtFieldElem = pm.textField( "txtFieldElem", q=True, text=True)
	txtFieldSide = mc.optionMenu("txtFieldSide", query = True, value = True)
	txtFieldAxis = mc.optionMenu("txtFieldAxis", query = True, value = True)
	txtFieldArcLength = pm.textField( "txtFieldArcLength", q=True, text=True)
	txtFieldObjectCount = pm.textField( "txtFieldObjectCount", q=True, text=True)
	txtFieldStartFrame = pm.textField( "txtFieldStartFrame", q=True, text=True)
	txtFieldEndFrame = pm.textField( "txtFieldEndFrame", q=True, text=True)
	txtFieldStaggerFrames = pm.textField( "txtFieldStaggerFrames", q=True, text=True)
	txtFieldTimeScale = pm.textField( "txtFieldTimeScale", q=True, text=True)


	allList = [txtFieldGeoGrp, txtFieldName, txtFieldElem, txtFieldSide, txtFieldAxis, txtFieldArcLength, txtFieldObjectCount, txtFieldStartFrame, txtFieldEndFrame, txtFieldStaggerFrames, txtFieldTimeScale]
	print 		'>> geo = ' , txtFieldGeoGrp
	print		'>> name = ' , txtFieldName
	print		'>> elem = ' , txtFieldElem
	print		'>> side = ' , txtFieldSide
	print		'>> axis = ' , txtFieldAxis 
	print		'>> arclength = ' , txtFieldArcLength
	print		'>> objectCount = ' , txtFieldObjectCount
	print		'>> startFrameSet = ' , txtFieldStartFrame
	print		'>> endFrameSet = ' , txtFieldEndFrame
	print		'>> staggerFramesSet = ' , txtFieldStaggerFrames
	print		'>> txtFieldTimeScale = ' , txtFieldTimeScale

	mashNetwork(	geo = txtFieldGeoGrp, 
					name = txtFieldName,
					elem = txtFieldElem,
					side = txtFieldSide,
					axis = txtFieldAxis, 
					arclength = float(txtFieldArcLength), 
					objectCount = int(txtFieldObjectCount),
					startFrameSet = int(txtFieldStartFrame), 
					endFrameSet = int(txtFieldEndFrame),
					staggerFramesSet = int(txtFieldStaggerFrames),
					timeScaleSet = float(txtFieldTimeScale))

	print '>> Create Mash Network Is Done!!'

def mashSpineIkRun(*args):
	txtFieldGeoGrp = pm.textField("txtFieldGeoGrp", q=True, text=True) 
	txtFieldName = pm.textField("txtFieldName", q=True, text=True)
	txtFieldElem = pm.textField( "txtFieldElem", q=True, text=True)
	txtFieldSide = mc.optionMenu("txtFieldSide", query = True, value = True)
	txtFieldAxis = mc.optionMenu("txtFieldAxis", query = True, value = True)
	txtFieldArcLength = pm.textField( "txtFieldArcLength", q=True, text=True)
	txtFieldObjectCount = pm.textField( "txtFieldObjectCount", q=True, text=True)

	mashSpineIk(	geo = txtFieldGeoGrp,
					name = txtFieldName,
					elem = txtFieldElem,
					side = txtFieldSide,
					axis = txtFieldAxis, 
					arclength = float(txtFieldArcLength), 
					objectCount = int(txtFieldObjectCount))
	print '>> Create SpineIk Joint Is Done!!'

def motions(	name = '', 
				elem = '', 
				side = '', 
				objectCount = 70, 
				curve = ''):

	## naming
	if side == '_':
		side = ''
	else:
		side = '_' + side 
	newName = '{}{}{}'.format(name, elem, side)
	## create motion spineIk
	countStartJnt = '{}1_Jnt'.format(newName)
	cuuntEndjnt = '{}{}_Jnt'.format(newName, objectCount)

	## bindSkin
	jntBindSkin = mc.listRelatives(countStartJnt, type = 'joint', ad = True)
	jntBindSkin.append(countStartJnt)

	geoBindSkin = '{}_Mapi_ReproMesh'.format(newName)
	utaCore.bindSkinCluster(jntObj = jntBindSkin, obj = [geoBindSkin])

	utaCore.createIkhSpine (strJnt = countStartJnt, endJnt = cuuntEndjnt, curve = curve, name = '{}_Ikh'.format(newName), sol = 'ikSplineSolver')
	## fix ikhandle for ikspineHandle
	utaCore.checkIkSplineSolver(name = name ,elem = elem,  side = side,ikHandleNode = '{}_Ikh'.format(newName))
	
	## add attribute ikspine
	locCtrl = '{}_Loc'.format(newName)
	utaCore.attrNameTitle(obj = locCtrl, ln = 'ControlSpineIk')
	utaCore.addAttrCtrl (ctrl = locCtrl, elem = ['Offset', 'Roll', 'Twist'], min = -10000, max = 10000, at = 'float',)
	utaCore.addAttrCtrl (ctrl = locCtrl, elem = ['IkBlend'], min = 0, max = 1, at = 'long',)

	## connect attribute ikspine
	mc.setAttr ("{}.IkBlend".format(locCtrl), 1);
	mc.connectAttr('{}.Offset'.format(locCtrl), "{}.offset".format('{}_Ikh'.format(newName)))
	mc.connectAttr('{}.Roll'.format(locCtrl), "{}.roll".format('{}_Ikh'.format(newName)))
	mc.connectAttr('{}.Twist'.format(locCtrl), "{}.twist".format('{}_Ikh'.format(newName)))
	mc.connectAttr('{}.IkBlend'.format(locCtrl), "{}.ikBlend".format('{}_Ikh'.format(newName)))

	## wrap group
	mc.parent('{}_Ikh'.format(newName), '{}{}Still{}_Grp'.format(name, elem,  side))

	mc.select(cl = True)

def mashNetwork(	geo = '',
					name = '',
					elem = '',
					side = '',
					axis = 'Z', 
					arclength = -800, 
					objectCount = 70,
					startFrameSet = 1001, 
					endFrameSet = 1225,
					staggerFramesSet = 100,
					timeScaleSet = 0.250):

	## naming
	if side == '_':
		side = ''
	else:
		side = '_' + side 
	newName = '{}{}{}'.format(name, elem, side)

	## crate mash network
	if not geo == '':
		mc.select(geo)

	else:
		sel = mc.ls(sl = True)
		geo = sel[0]
		if len(sel) == 0:
			print ">> Please Select Geo_Grp for create mashNetwork"
			return

	mashNetwork = mapi.Network();
	mashNetwork.createNetwork(name= '{}_Mapi'.format(newName));
	nodeName =  mashNetwork.waiter

	## crate control for move
	newGrpLoc = mc.group(n = '{}{}Loc{}_Grp'.format(name, elem,  side), em = True);
	newLoc  = mc.spaceLocator(n = '{}_Loc'.format(newName))[0]
	mc.parent(newLoc, newGrpLoc)
	utaCore.snapObj(objA = geo, objB = newGrpLoc)
	mc.setAttr( "{}Shape.localScaleX".format(newLoc), 20);
	mc.setAttr( "{}Shape.localScaleY".format(newLoc), 20);
	mc.setAttr( "{}Shape.localScaleZ".format(newLoc), 20);
	utaCore.attrNameTitle(obj = newLoc, ln = 'Finctions')
	utaCore.addAttrCtrl (ctrl = newLoc, elem = ['arclength', 'objectCount', 'startFrameSet', 'endFrameSet', 'staggerFramesSet'], min = -1000000, max = 1000000, at = 'long',)
	utaCore.addAttrCtrl (ctrl = newLoc, elem = ['timeScale'], min = -1000, max = 1000, at = 'float',)

	## add a Signal node
	timeNode = mashNetwork.addNode("MASH_Time")
	moveNode = mashNetwork.addNode("MASH_Transform")

	mc.setAttr('{}.arclength'.format(newLoc), arclength)
	mc.setAttr('{}.objectCount'.format(newLoc), objectCount)
	mc.setAttr('{}.startFrameSet'.format(newLoc), startFrameSet)
	mc.setAttr('{}.endFrameSet'.format(newLoc), endFrameSet)
	mc.setAttr('{}.staggerFramesSet'.format(newLoc), staggerFramesSet)
	mc.setAttr('{}.timeScale'.format(newLoc), timeScaleSet)



	## set value Default
	mc.setAttr( "{}.amplitudeX".format(mashNetwork.distribute), 0);
	mc.connectAttr('{}.arclength'.format(newLoc), "{}.amplitude{}".format(mashNetwork.distribute, axis))
	mc.connectAttr('{}.objectCount'.format(newLoc), "{}.pointCount".format(mashNetwork.distribute))


	## set value default timeNode
	mc.connectAttr('{}.startFrameSet'.format(newLoc), "{}_Time.animationStart".format(nodeName))
	mc.connectAttr('{}.endFrameSet'.format(newLoc),  "{}_Time.animationEnd".format(nodeName))
	mc.connectAttr('{}.staggerFramesSet'.format(newLoc), "{}_Time.staggerFrames".format(nodeName))
	mc.connectAttr('{}.timeScale'.format(newLoc), "{}_Time.timeScale".format(nodeName))


	## connect transform Node
	mc.connectAttr ('{}.translate'.format(newLoc), '{}_Transform.positionAmount'.format(nodeName));
	mc.connectAttr ('{}.rotate'.format(newLoc), '{}_Transform.rotationAmount'.format(nodeName));
	mc.connectAttr ('{}.scale'.format(newLoc), '{}_Transform.scaleAmount'.format(nodeName));

	## create group
	stillGrp = mc.group(n = '{}{}Still{}_Grp'.format(name, elem,  side), em = True);
	ctrlGrp = mc.group(n = '{}{}Ctrl{}_Grp'.format(name, elem, side), em = True);
	geoGrp = mc.group(n = '{}{}Geo{}_Grp'.format(name, elem, side), em = True);
	jntGrp = mc.group(n = '{}{}Jnt{}_Grp'.format(name, elem, side), em = True);

	# ## crate joint for spine ik
	# jntSpIkGrp = mc.group(n = '{}{}IkSpJnt{}_Grp'.format(name, elem, side), em = True);
	# utaCore.snapObj(objA = geo, objB = jntSpIkGrp)
	# translateJnt = '%.3f' % (float(arclength) / float(objectCount))

	# print '>> translateJnt : ', translateJnt
	
	# jntLists = []
	# for i in range(objectCount):
	# 	locPos  = mc.spaceLocator(n = '{}{}_Loc'.format(newName, i+1))[0]
	# 	mc.select(locPos)
	# 	jnt = utaCore.createJointFromLocator (name = '{}{}'.format(newName, i+1 ), side = side + '_', lastName = '_Jnt')
	# 	jntLists.append(jnt)
	# 	if not i == 0:
	# 		mc.parent(jntLists[i], jntLists[i-1])
	# 		mc.setAttr('{}.translate{}'.format(jntLists[i], axis), float(translateJnt))
	# utaCore.snapObj(objA = geo, objB = jntLists[0])
	# mc.parent(jntLists[0] , jntSpIkGrp)


	## wrap obj
	reproMeshGeo = '{}_ReproMesh'.format(nodeName)
	mc.parent(reproMeshGeo, geoGrp);
	mc.parent(newGrpLoc, ctrlGrp);
	# mc.parent(jntSpIkGrp, jntGrp)

	mc.select(cl = True)
	# return reproMeshGeo


def mashSpineIk(	geo = '',
					name = '',
					elem = '',
					side = '',
					axis = 'Z', 
					arclength = -800, 
					objectCount = 70):

	## naming
	if side == '_':
		side = ''
	else:
		side = '_' + side 
	newName = '{}{}{}'.format(name, elem, side)

	## crate joint for spine ik
	jntSpIkGrp = mc.group(n = '{}{}IkSpJnt{}_Grp'.format(name, elem, side), em = True);
	utaCore.snapObj(objA = geo, objB = jntSpIkGrp)
	valueTranslate = '%.3f' % (float(arclength) / float(objectCount))
	# print '>> valueTranslate : ', valueTranslate
	# translateJnt = ((float(valueTranslate)*4)/100+ float(valueTranslate))
	translateJnt = float(valueTranslate)
	print '>> translateJnt : ', translateJnt

	jntLists = []
	for i in range(objectCount):
		locPos  = mc.spaceLocator(n = '{}{}_Loc'.format(newName, i+1))[0]
		mc.select(locPos)
		jnt = utaCore.createJointFromLocator (name = '{}{}'.format(newName, i+1 ), side = side + '_', lastName = '_Jnt')
		jntLists.append(jnt)
		if not i == 0:
			mc.parent(jntLists[i], jntLists[i-1])
			mc.setAttr('{}.translate{}'.format(jntLists[i], axis), float(translateJnt))
	utaCore.snapObj(objA = geo, objB = jntLists[0])
	mc.parent(jntLists[0] , jntSpIkGrp)

	mc.parent(jntSpIkGrp, '{}{}Jnt{}_Grp'.format(name, elem, side))

