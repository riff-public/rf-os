import sys
sys.path.append( r'P:\lib\local')
sys.path.append( r'P:\SevenChicks\all\asset\char\main\BlueChick\rig\data')

import ppmel.core as core
import maya.cmds as mc
import maya.mel as mm

reload(core)
reload(mc)
reload(mm)

#edit Dtl Group
def addGrpToRbn ( elem = '' , numberDtl = 1 ) :

    # elem = 'beltA'
    # numberDtl = '1'
    dtlRbnCtrl = ('%s%sRbnDtl_ctrl' %(elem, numberDtl))
    dtlRbnTwist = ('%s%sRbnDtlCtrlTwist_grp' %(elem,numberDtl))
    dtlNull = mc.group(em=True)
    dtlRbnOfst = mc.rename(dtlNull,'%s%sRbnDtlCtrlOfst_grp' %(elem,numberDtl))
    core.setLock( dtlRbnOfst , False , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v')
    core.parent(dtlRbnOfst, '%s%sRbnDtlCtrlTwist_grp' %(elem,numberDtl))
    if dtlRbnCtrl :
        core.snap( dtlRbnCtrl , dtlRbnOfst) 
    core.parent('%s%sRbnDtl_ctrl' %(elem,numberDtl), dtlRbnOfst)

