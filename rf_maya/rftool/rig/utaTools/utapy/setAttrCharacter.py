import maya.cmds as mc
def setAttrChar(*args):
	ctrls = mc.ls(	"Pelvis_Ctrl", 
			        "RbnNeck_Ctrl", 
			        "RbnUpArm_*_Ctrl", 
			        "RbnForearm_*_Ctrl", 
			        "RbnUpLeg_*_Ctrl", 
			        "RbnLowLeg_*_Ctrl")
	attrs = (	"indepHip", 
	        	"autoTwist")

	for each in ctrls:
	    for attr in attrs:
	        try:
	            mc.setAttr('%s.%s' % (each , attr) , 1)
	        except: pass