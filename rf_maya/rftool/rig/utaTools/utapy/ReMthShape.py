import maya.cmds as mc
# reload(mc)
import pymel.core as pm  
# reload(pm)
def ReMthShape(jawMthRotateX, *args):

	sels = mc.ls(sl=True)
         
	bshList = ["CnrOutDnOpen", "CnrOutUpOpen", "CnrInDnOpen", "CnrInUpOpen", "CnrOutOpen", "CnrInOpen", "CnrDnOpen", "CnrUpOpen", "JawOpen", "JawOpenMth"]
	Amount = 0
	for each in sels:
		if "CnrOutDnOpen" in each:
			'''1
			CnrOutDnOpen
				JawOpenShapes -1
				cornerOuterDn -1
				CnrOutDnOpenShapes 1
			'''
			pm.delete ( sels , ch = True ) ; 
			CnrOutDnOpenShapesBsn = mc.blendShape("JawOpenShapes", "cornerOuterDn", each , n="%sShapesBsn%s" % (each, Amount))[0]
			mc.setAttr("%sShapesBsn%s" % (each, Amount)+"."+"JawOpenShapes",1)
			mc.setAttr("%sShapesBsn%s" % (each, Amount)+"."+"cornerOuterDn",1)
			Amount += 1
	for each in sels:
		if "CnrOutUpOpen" in each:
			'''2
			CnrOutUpOpen
				JawOpenShapes -1
				cornerOuterUp -1
				CnrOutUpOpenShapes 1
			'''
			pm.delete ( sels , ch = True ) ;  
			CnrOutUpOpenShapesBsn = mc.blendShape("JawOpenShapes", "cornerOuterUp", each, n="%sShapesBsn%s" % (each, Amount))[0]
			mc.setAttr("%sShapesBsn%s" % (each, Amount)+"."+"JawOpenShapes",1)
			mc.setAttr("%sShapesBsn%s" % (each, Amount)+"."+"cornerOuterUp",1)
			# mc.setAttr("%sShapesBsn" % bshList[1]+"."+"CnrOutUpOpenShapes",1)
			Amount += 1
	for each in sels:
		if "CnrInDnOpen" in each:
			'''3
			CnrInDnOpen
				JawOpenShapes -1
				cornerInnerDn -1
				cnrInDnOpenShapes 1
			'''
			pm.delete ( sels , ch = True ) ;  
			pm.delete ( sels , ch = True ) ;  			
			CnrInDnOpenShapesBsn = mc.blendShape("JawOpenShapes", "cornerInnerDn", each, n="%sShapesBsn%s" % (each, Amount))[0]
			mc.setAttr("%sShapesBsn%s" % (each, Amount)+"."+"JawOpenShapes",1)
			mc.setAttr("%sShapesBsn%s" % (each, Amount)+"."+"cornerInnerDn",1)
			# mc.setAttr("%sShapesBsn" % bshList[2]+"."+"CnrInDnOpenShapes",1)
			Amount += 1
	for each in sels:
		if "CnrInUpOpen" in each:
			'''4
			CnrInUpOpen
				JawOpenShapes -1
				cornerInnerUp -1
				CnrInUpOpenShapes 1
			'''
			pm.delete ( sels , ch = True ) ;  
			pm.delete ( sels , ch = True ) ;  
			CnrInUpOpenBsn = mc.blendShape("JawOpenShapes", "cornerInnerUp", sels, n="%sShapesBsn%s" % (each, Amount))[0]
			mc.setAttr("%sShapesBsn%s" % (each, Amount)+"."+"JawOpenShapes",1)
			mc.setAttr("%sShapesBsn%s" % (each, Amount)+"."+"cornerInnerUp",1)
			# mc.setAttr("%sShapesBsn" % bshList[3]+"."+"CnrInUpOpenShapes",1)
			Amount += 1
	for each in sels:
		if "CnrOutOpen" in each:		
			'''5
			CnrOutOpen
				JawOpenShapes -1
				cornerOut -1
				CnrOutOpenShapes 1
			'''
			pm.delete ( sels , ch = True ) ;  
			CnrOutOpenShapesBsn = mc.blendShape("JawOpenShapes", "cornerOut", sels, n="%sShapesBsn%s" % (each, Amount))[0]
			mc.setAttr("%sShapesBsn%s" % (each, Amount)+"."+"JawOpenShapes",1)
			mc.setAttr("%sShapesBsn%s" % (each, Amount)+"."+"cornerOut",1)
			# mc.setAttr("%sShapessn" % bshList[4]+"."+"CnrOutOpenShapes",1)
			Amount += 1
	for each in sels:
		if "CnrInOpen" in each:	
			'''6
			CnrInOpen
				JawOpenShapes -1
				cornerIn -1
				CnrInOpenShapes 1
			'''
			pm.delete ( sels , ch = True ) ;  
			CnrInOpenShapesBsn = mc.blendShape("JawOpenShapes", "cornerIn", sels, n="%sShapesBsn%s" % (each, Amount))[0]
			mc.setAttr("%sShapesBsn%s" % (each, Amount)+"."+"JawOpenShapes",1)
			mc.setAttr("%sShapesBsn%s" % (each, Amount)+"."+"cornerIn",1)
			# mc.setAttr("%sShapesBsn" % bshList[5]+"."+"CnrInOpenShapes",1)
			Amount += 1
	for each in sels:
		if "CnrDnOpen" in each:	
		# '''7
		# CnrDnOpen
		# JawOpenRef20 -1
		# cornerDn -1
		# CnrDnOpenRefShapes 1
		# '''
			pm.delete ( sels , ch = True ) ;  
			CnrDnOpenRefShapesBsn = mc.blendShape(("JawOpenRef%s" % int(jawMthRotateX)), "cornerDn", each,n="%sShapesBsn%s" % (each, Amount))[0]
			mc.setAttr("%sShapesBsn%s" % (each, Amount)+"."+("JawOpenRef%s" % int(jawMthRotateX)), 1)
			mc.setAttr("%sShapesBsn%s" % (each, Amount)+"."+"cornerDn",1)
			Amount += 1

	for each in sels:
		if "CnrUpOpen" in each:	
		# '''8
		# CnrUpOpen
		# 	JawOpenRef20 -1
		# 	cornerUp -1
		# 	CnrUpOpenRefShapes 1
		# '''
			pm.delete ( sels , ch = True ) ;  
			CnrUpOpenRefShapesBsn = mc.blendShape(("JawOpenRef%s" % int(jawMthRotateX)), "cornerUp", each,n="%sShapesBsn%s" % (each, Amount))[0]
			mc.setAttr("%sShapesBsn%s" % (each, Amount)+"."+("JawOpenRef%s" % int(jawMthRotateX)), 1)
			mc.setAttr("%sShapesBsn%s" % (each, Amount)+"."+"cornerUp",1)
			Amount += 1

	# '''9
	# JawOpen
	# 	JawOpenRef20 -1
	# 	JawOpenShapes 1
	# '''
	# JawOpenBsn = mc.blendShape(("JawOpenRef%s" % jawMthRotateX), "JawOpenShapes", "JawOpen",n="%sBsn" % bshList[8])[0]
	# mc.setAttr("%sBsn" % bshList[8]+"."+("JawOpenRef%s" % jawMthRotateX),-1)
	# mc.setAttr("%sBsn" % bshList[8]+"."+"JawOpenShapes",1)

	# # JawOpenShapesBsn = mc.blendShape(("JawOpenRef%s" % jawMthRotateX), "JawOpenShapes", "JawOpen",n="%sBsn" % bshList[8])[0]
	# # mc.setAttr("%sBsn" % bshList[8]+"."+("JawOpenRef%s" % jawMthRotateX),-1)
	# # mc.setAttr("%sBsn" % bshList[8]+"."+"JawOpenShapes",1)
	# # JawOpenShape
	# '''10
	# JawOpenMth
	# 	JawOpenRef20 -1
	# 	JawOpenMthShapes 1
	# '''
	# JawOpenMthBsn = mc.blendShape(("JawOpenRef%s" % jawMthRotateX), "JawOpenMthShapes", "JawOpenMth",n="%sBsn" % bshList[9])[0]
	# mc.setAttr("%sBsn" % bshList[9]+"."+("JawOpenRef%s" % jawMthRotateX),-1)
	# mc.setAttr("%sBsn" % bshList[9]+"."+"JawOpenMthShapes",1)