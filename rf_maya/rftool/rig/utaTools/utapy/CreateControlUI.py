import sys
sys.path.append(r'P:\lib\local\utaTools\utapy')
import copyShapeMouth as csb 
reload(csb)
import eyelidFollowConnect as efc 
reload(efc)
from utaTools.utapy import utaCore
reload(utaCore)
import sys, os
import pymel.core as pm ;
import maya.cmds as mc ;
import maya.mel as mm ;

from functools import partial
# Path ---------------------------------------
core = '%s/core' % os.environ.get('RFSCRIPT')
# if not core in sys.path: 
#     sys.path.append(core)
path = '{core}/rf_maya/rftool/rig/'.format(core=core)
# Path ---------------------------------------

def pngImags():
    imags = (path + "utaTools/utaicon/kk_icons/")
    return imags
# # copyShape
# def copyShapeMouths ( *args ) :
#     csb.createControlUI_A();
# def copyShapeMouth_LFT( *args ) :
#     csb.createControlUI_B();
# def copyShapeMouth_RGT ( *args ) :
#     csb.createControlUI_C();
# def getGeoBase(*args):
#     sels = mc.ls(sl=True)       
#     suffix = pm.textField ( "txtFieldGeoBase" , q  = True , text = True  )
#     csb.copyShapeToBase(geoBase= suffix, lists= sels)
# def addTextGeoBase(*args):
#     sel = pm.ls(sl=True) ;
#     sel = sel [0] ;
#     pm.textField("txtFieldGeoBase", e = True  , text = sel)
#     # crint sel
def createControlUI ( *args ) :
    
    # check if window exists
    if pm.window ( 'createControlUI' , exists = True ) :
        pm.deleteUI ( 'createControlUI' ) ;
    else : pass ;
   
    window = pm.window ( 'createControlUI', title = "CreateControlUI v. 3.0" ,
        mnb = False , mxb = False , sizeable = True , rtf = True ) ;
        
    pm.window ( 'createControlUI' , e = True , h = 350, w = 282 ) ;
    mainLayout = pm.rowColumnLayout ( nc = 1 , p = window ) ;



    # Line Control shape A ------------------------------------------------------------------------------------
    rowLine1 = pm.rowColumnLayout ( nc = 3 , p = mainLayout , h = 10, w = 282 ) ;
    pm.button (label = "-"*100 ,h = 10, w = 300, p = rowLine1 , bgc = (0.333333, 0.333333, 0.333333)) ; 

    ## Label Control Shape
    pm.text( label= ">> Select ParentCon Or Connection" , p = mainLayout, al = "left")


    rowLine1Sub = pm.rowColumnLayout ( nc = 5 , p = mainLayout , h = 20 , cw = [(1, 30), (2, 80), (3, 50) , (4, 80), (5, 30)]) ;
    mc.radioCollection()
    pm.rowColumnLayout (p = rowLine1Sub ,h=8) ;
    mc.radioButton('parentConRadio', p = rowLine1Sub ,label='Parent Constraint', align='left' )
    pm.rowColumnLayout (p = rowLine1Sub ,h=8) ;
    mc.radioButton('connectionRadio', p = rowLine1Sub ,label='Connection', align='left')
    pm.rowColumnLayout (p = rowLine1Sub ,h=8) ;
    rowLine2Sub = pm.rowColumnLayout ( nc = 5 , p = mainLayout , h = 20 , cw = [(1, 30), (2, 80), (3, 50) , (4, 80), (5, 30)]) ;
    mc.radioCollection()
    pm.rowColumnLayout (p = rowLine2Sub ,h=8) ;
    pm.checkBox( "geoVischeck", label = "GeoVis", p = rowLine2Sub ,  align='left', value= True)
    pm.rowColumnLayout (p = rowLine2Sub ,h=8) ;
    pm.checkBox( "fkChainCheck", label = "fkChain", p = rowLine2Sub ,  align='left', value= False)
    pm.rowColumnLayout (p = rowLine2Sub ,h=8) ;

    # Line Control shape A ------------------------------------------------------------------------------------
    rowLine1 = pm.rowColumnLayout ( nc = 3 , p = mainLayout , h = 10, w = 282 ) ;
    pm.button (label = "-"*100 ,h = 10, w = 300, p = rowLine1 , bgc = (0.333333, 0.333333, 0.333333)) ; 

    ## Label Control Shape
    pm.text( label= "Controls Shape" , p = mainLayout, al = "center")

    # Line Control shape A ------------------------------------------------------------------------------------
    rowLine1 = pm.rowColumnLayout ( nc = 3 , p = mainLayout , h = 10, w = 282 ) ;
    pm.button (label = "-"*100 ,h = 10, w = 300, p = rowLine1 , bgc = (0.333333, 0.333333, 0.333333)) ; 


    ## RolColumn 1
    rowColumn1 = pm.rowColumnLayout ( nc = 15 , p = mainLayout , w = 282 , cw = [ ( 1 , 2) , ( 2 , 38 ), ( 3 , 2), ( 4 , 38 ), ( 5 , 2), ( 6 , 38 ), ( 7 , 2), ( 8 , 38), ( 9 , 2 ), ( 10 , 38), ( 11 , 2 ), ( 12 ,37), ( 13 , 2 ), ( 14 , 38), ( 15 , 2 )] ) ;
    pm.rowColumnLayout (p = rowColumn1) ;
    pm.symbolButton ( 'toolsA1' , image = ("%s%s" % (pngImags(),"circle.png")), h = 40, p = rowColumn1  , c = partial(curveShape, 'circle'), bgc = (0.312, 0.312, 0.312)) ; 
    pm.rowColumnLayout (p = rowColumn1) ;
    pm.symbolButton ( 'toolsA2' , image = ("%s%s" % (pngImags(),"square.png")), h = 40, p = rowColumn1  , c = partial(curveShape, 'square'), bgc = (0.312, 0.312, 0.312)) ; 
    pm.rowColumnLayout (p = rowColumn1) ;
    pm.symbolButton ( 'toolsA3' , image = ("%s%s" % (pngImags(),"cube.png")), h = 40, p = rowColumn1  , c = partial(curveShape, 'cube'), bgc = (0.312, 0.312, 0.312)) ; 
    pm.rowColumnLayout (p = rowColumn1) ;
    pm.symbolButton ( 'toolsA4' , image = ("%s%s" % (pngImags(),"arrow.png")), h = 40, p = rowColumn1  , c = partial(curveShape, 'arrow'), bgc = (0.312, 0.312, 0.312)) ; 
    pm.rowColumnLayout (p = rowColumn1) ;
    pm.symbolButton ( 'toolsA5' , image = ("%s%s" % (pngImags(),"circleArrow.png")), h = 40, p = rowColumn1  , c = partial(curveShape, 'circleArrow'), bgc = (0.312, 0.312, 0.312)) ; 
    pm.rowColumnLayout (p = rowColumn1) ;
    pm.symbolButton ( 'toolsA6' , image = ("%s%s" % (pngImags(),"plusCircle.png")), h = 40, p = rowColumn1  , c = partial(curveShape, 'cornerArrow'), bgc = (0.312, 0.312, 0.312)) ; 
    pm.rowColumnLayout (p = rowColumn1) ;
    pm.symbolButton ( 'toolsA7' , image = ("%s%s" % (pngImags(),"lineArrow.png")), h = 40, p = rowColumn1  , c = partial(curveShape, 'lineArrow'), bgc = (0.312, 0.312, 0.312)) ; 

    ## RolColumn 2
    rowColumn2 = pm.rowColumnLayout ( nc = 15 , p = mainLayout , w = 280 , cw = [ ( 1 , 2) , ( 2 , 38 ), ( 3 , 2), ( 4 , 38 ), ( 5 , 2), ( 6 , 38 ), ( 7 , 2), ( 8 , 38), ( 9 , 2 ), ( 10 , 38), ( 11 , 2 ), ( 12 ,37), ( 13 , 2 ), ( 14 , 38), ( 15 , 2 )] ) ;
    pm.rowColumnLayout (p = rowColumn2) ;
    pm.symbolButton ( 'toolsB1' , image = ("%s%s" % (pngImags(),"ballLine.png")), h = 40, p = rowColumn2  , c = partial(curveShape, 'ball'), bgc = (0.312, 0.312, 0.312)) ; 
    pm.rowColumnLayout (p = rowColumn2) ;
    pm.symbolButton ( 'toolsB2' , image = ("%s%s" % (pngImags(),"arrowLR.png")), h = 40, p = rowColumn2  , c = partial(curveShape, 'arrowLR'), bgc = (0.312, 0.312, 0.312)) ; 
    pm.rowColumnLayout (p = rowColumn2) ;
    pm.symbolButton ( 'toolsB3' , image = ("%s%s" % (pngImags(),"horseshoe.png")), h = 40, p = rowColumn2  , c = partial(curveShape, 'horseshoe'), bgc = (0.312, 0.312, 0.312)) ; 
    pm.rowColumnLayout (p = rowColumn2) ;
    pm.symbolButton ( 'toolsB4' , image = ("%s%s" % (pngImags(),"arrowAll.png")), h = 40, p = rowColumn2  , c = partial(curveShape, 'arrowAll'), bgc = (0.312, 0.312, 0.312)) ; 
    pm.rowColumnLayout (p = rowColumn2) ;
    pm.symbolButton ( 'toolsB5' , image = ("%s%s" % (pngImags(),"hand.png")), h = 40, p = rowColumn2  , c = partial(curveShape, 'hand'), bgc = (0.312, 0.312, 0.312)) ; 
    pm.rowColumnLayout (p = rowColumn2) ;
    pm.symbolButton ( 'toolsB6' , image = ("%s%s" % (pngImags(),"placement.png")), h = 40, p = rowColumn2  , c = partial(curveShape, 'placement'), bgc = (0.312, 0.312, 0.312)) ; 
    pm.rowColumnLayout (p = rowColumn2) ;
    pm.symbolButton ( 'toolsB7' , image = ("%s%s" % (pngImags(),"halfBall.png")), h = 40, p = rowColumn2  , c = partial(curveShape, 'halfBall'), bgc = (0.312, 0.312, 0.312)) ; 

    # ## RolColumn 3
    rowColumn3 = pm.rowColumnLayout ( nc = 15 , p = mainLayout , w = 280 , cw = [ ( 1 , 2) , ( 2 , 38 ), ( 3 , 2), ( 4 , 38 ), ( 5 , 2), ( 6 , 38 ), ( 7 , 2), ( 8 , 38), ( 9 , 2 ), ( 10 , 38), ( 11 , 2 ), ( 12 ,37), ( 13 , 2 ), ( 14 , 38), ( 15 , 2 )] ) ;
    pm.rowColumnLayout (p = rowColumn3) ;
    pm.symbolButton ( 'toolsC1' , image = ("%s%s" % (pngImags(),"shoe.png")), h = 40, p = rowColumn3  , c = partial(curveShape, 'shoe'), bgc = (0.312, 0.312, 0.312)) ; 
    pm.rowColumnLayout (p = rowColumn3) ;
    pm.symbolButton ( 'toolsC2' , image = ("%s%s" % (pngImags(),"arrowUp.png")), h = 40, p = rowColumn3  , c = partial(curveShape, 'arrowUp'), bgc = (0.312, 0.312, 0.312)) ; 
    pm.rowColumnLayout (p = rowColumn3) ;
    pm.symbolButton ( 'toolsC3' , image = ("%s%s" % (pngImags(),"placementBow.png")), h = 40, p = rowColumn3  , c = partial(curveShape, 'placementBow'), bgc = (0.312, 0.312, 0.312)) ; 
    pm.rowColumnLayout (p = rowColumn3) ;
    pm.symbolButton ( 'toolsC4' , image = ("%s%s" % (pngImags(),"eye.png")), h = 40, p = rowColumn3  , c = partial(curveShape, 'eye'), bgc = (0.312, 0.312, 0.312)) ; 
    pm.rowColumnLayout (p = rowColumn3) ;
    pm.symbolButton ( 'toolsC5' , image = ("%s%s" % (pngImags(),"airplane.png")), h = 40, p = rowColumn3  , c = partial(curveShape, 'airplane'), bgc = (0.312, 0.312, 0.312)) ; 
    pm.rowColumnLayout (p = rowColumn3) ;
    pm.symbolButton ( 'toolsC6' , image = ("%s%s" % (pngImags(),"iceCream.png")), h = 40, p = rowColumn3  , c = partial(curveShape, 'iceCream'), bgc = (0.312, 0.312, 0.312)) ; 
    pm.rowColumnLayout (p = rowColumn3) ;
    pm.symbolButton ( 'toolsC7' , image = ("%s%s" % (pngImags(),"key.png")), h = 40, p = rowColumn3  , c = partial(curveShape, 'key'), bgc = (0.312, 0.312, 0.312)) ; 

    # ## RolColumn 3
    rowColumn4 = pm.rowColumnLayout ( nc = 15 , p = mainLayout , w = 280 , cw = [ ( 1 , 2) , ( 2 , 38 ), ( 3 , 2), ( 4 , 38 ), ( 5 , 2), ( 6 , 38 ), ( 7 , 2), ( 8 , 38), ( 9 , 2 ), ( 10 , 38), ( 11 , 2 ), ( 12 ,37), ( 13 , 2 ), ( 14 , 38), ( 15 , 2 )] ) ;
    pm.rowColumnLayout (p = rowColumn4) ;
    pm.symbolButton ( 'toolsD1' , image = ("%s%s" % (pngImags(),"sun.png")), h = 40, p = rowColumn4   , c = partial(curveShape, 'sun'), bgc = (0.312, 0.312, 0.312)) ; 
    pm.rowColumnLayout (p = rowColumn4) ;
    pm.symbolButton ( 'toolsD2' , image = ("%s%s" % (pngImags(),"plus.png")), h = 40, p = rowColumn4   , c = partial(curveShape, 'plus'), bgc = (0.312, 0.312, 0.312)) ; 
    pm.rowColumnLayout (p = rowColumn4) ;
    pm.symbolButton ( 'toolsD3' , image = ("%s%s" % (pngImags(),"pyramid.png")), h = 40, p = rowColumn4   , c = partial(curveShape, 'pyramid'), bgc = (0.312, 0.312, 0.312)) ; 
    pm.rowColumnLayout (p = rowColumn4) ;
    pm.symbolButton ( 'toolsD4' , image = ("%s%s" % (pngImags(),"cornerArrow2.png")), h = 40, p = rowColumn4   , c = partial(curveShape, 'cornerArrow2'), bgc = (0.312, 0.312, 0.312)) ; 
    pm.rowColumnLayout (p = rowColumn4) ;
    pm.symbolButton ( 'toolsD5' , image = ("%s%s" % (pngImags(),"arrow9.png")), h = 40, p = rowColumn4   , c = partial(curveShape, 'arrow9'), bgc = (0.312, 0.312, 0.312)) ; 
    pm.rowColumnLayout (p = rowColumn4) ;
    pm.symbolButton ( 'toolsD6' , image = ("%s%s" % (pngImags(),"arrowTurn.png")), h = 40, p = rowColumn4   , c = partial(curveShape, 'arrowTurn'), bgc = (0.312, 0.312, 0.312)) ; 
    pm.rowColumnLayout (p = rowColumn4) ;
    pm.symbolButton ( 'toolsD7' , image = ("%s%s" % (pngImags(),"bamboo.png")), h = 40, p = rowColumn4   , c = partial(curveShape, 'bamboo'), bgc = (0.312, 0.312, 0.312)) ; 


    # ## RolColumn 4
    rowColumn5 = pm.rowColumnLayout ( nc = 15 , p = mainLayout , w = 280 , cw = [ ( 1 , 2) , ( 2 , 38 ), ( 3 , 2), ( 4 , 38 ), ( 5 , 2), ( 6 , 38 ), ( 7 , 2), ( 8 , 38), ( 9 , 2 ), ( 10 , 38), ( 11 , 2 ), ( 12 ,37), ( 13 , 2 ), ( 14 , 38), ( 15 , 2 )] ) ;
    pm.rowColumnLayout (p = rowColumn5) ;
    pm.symbolButton ( 'toolsE1' , image = ("%s%s" % (pngImags(),"sungrasses.png")), h = 40, p = rowColumn5  , c = partial(curveShape, 'sungrasses'), bgc = (0.312, 0.312, 0.312)) ; 
    pm.rowColumnLayout (p = rowColumn5) ;
    pm.symbolButton ( 'toolsE2' , image = ("%s%s" % (pngImags(),"fish.png")), h = 40, p = rowColumn5  , c = partial(curveShape, 'fish'), bgc = (0.312, 0.312, 0.312)) ; 
    pm.rowColumnLayout (p = rowColumn5) ;
    pm.symbolButton ( 'toolsE3' , image = ("%s%s" % (pngImags(),"w.png")), h = 40, p = rowColumn5  , c = partial(curveShape, 'w'), bgc = (0.312, 0.312, 0.312)) ; 
    pm.rowColumnLayout (p = rowColumn5) ;
    pm.symbolButton ( 'toolsE4' , image = ("%s%s" % (pngImags(),"arrow2D.png")), h = 40, p = rowColumn5  , c = partial(curveShape, 'arrow2D'), bgc = (0.312, 0.312, 0.312)) ; 
    pm.rowColumnLayout (p = rowColumn5) ;
    pm.symbolButton ( 'toolsE5' , image = ("%s%s" % (pngImags(),"ballPlus.png")), h = 40, p = rowColumn5  , c = partial(curveShape, 'ballPlus'), bgc = (0.312, 0.312, 0.312)) ; 
    pm.rowColumnLayout (p = rowColumn5) ;
    pm.symbolButton ( 'toolsE6' , image = ("%s%s" % (pngImags(),"gear.png")), h = 40, p = rowColumn5  , c = partial(curveShape, 'gear'), bgc = (0.312, 0.312, 0.312)) ; 
    pm.rowColumnLayout (p = rowColumn5) ;
    pm.symbolButton ( 'toolsE7' , image = ("%s%s" % (pngImags(),"gear.png")), h = 40, p = rowColumn5  , c = partial(curveShape, 'gear'), bgc = (0.312, 0.312, 0.312)) ; 




    # Line Control shape B ------------------------------------------------------------------------------------
    rowLine2 = pm.rowColumnLayout ( nc = 3 , p = mainLayout , h = 10, w = 280 ) ;
    pm.button (label = "-"*100 ,h = 10, w = 300, p = rowLine2 , bgc = (0.333333, 0.333333, 0.333333)) ; 

    ## Label Colors
    pm.text( label= "Colors" , p = mainLayout, al = "center")

    # Line Control shape B ------------------------------------------------------------------------------------
    rowLine2 = pm.rowColumnLayout ( nc = 3 , p = mainLayout , h = 10, w = 280 ) ;
    pm.button (label = "-"*100 ,h = 10, w = 300, p = rowLine2 , bgc = (0.333333, 0.333333, 0.333333)) ; 

    # ## RolColumn 3
    rowColumn4 = pm.rowColumnLayout ( nc = 15 , p = mainLayout , h = 20, w = 280 , cw = [ ( 1 , 2) , ( 2 , 38 ), ( 3 , 2), ( 4 , 38 ), ( 5 , 2), ( 6 , 38 ), ( 7 , 2), ( 8 , 38), ( 9 , 2 ), ( 10 , 38), ( 11 , 2 ), ( 12 ,37), ( 13 , 2 ), ( 14 , 38), ( 15 , 2 )] ) ;
    pm.rowColumnLayout (p = rowColumn4) ;
    pm.button (label = "" , p = rowColumn4 , c = partial(setColor, 'none'  ))
    pm.rowColumnLayout (p = rowColumn4) ;
    pm.button (label = "" , p = rowColumn4 , c = partial(setColor, 'black'  ), bgc = (0, 0, 0))      
    pm.rowColumnLayout (p = rowColumn4) ;
    pm.button (label = "" , p = rowColumn4 , c = partial(setColor, 'gray'  ), bgc = (0.75 ,0.75 ,0.75))    
    pm.rowColumnLayout (p = rowColumn4) ;
    pm.button (label = "" , p = rowColumn4 , c = partial(setColor, 'softGray'  ), bgc = (0.5 ,0.5, 0.5))    
    pm.rowColumnLayout (p = rowColumn4) ;
    pm.button (label = "" , p = rowColumn4 , c = partial(setColor, 'darkRed'  ), bgc = (0.8, 0, 0.2))    
    pm.rowColumnLayout (p = rowColumn4) ;
    pm.button (label = "" , p = rowColumn4 , c = partial(setColor, 'darkBlue'  ), bgc = (0, 0, 0.4))    
    pm.rowColumnLayout (p = rowColumn4) ;
    pm.button (label = "" , p = rowColumn4 , c = partial(setColor, 'blue'  ), bgc = (0, 0, 1))    

    # ## RolColumn 3
    rowColumn5 = pm.rowColumnLayout ( nc = 15 , p = mainLayout , h = 20, w = 280 , cw = [ ( 1 , 2) , ( 2 , 38 ), ( 3 , 2), ( 4 , 38 ), ( 5 , 2), ( 6 , 38 ), ( 7 , 2), ( 8 , 38), ( 9 , 2 ), ( 10 , 38), ( 11 , 2 ), ( 12 ,37), ( 13 , 2 ), ( 14 , 38), ( 15 , 2 )] ) ;
    pm.rowColumnLayout (p = rowColumn5) ;
    pm.button (label = "" , p = rowColumn5 , c = partial(setColor, 'darkGreen'  ), bgc = (0, 0.3, 0))   ##(0.160,0.160,0.948))  
    pm.rowColumnLayout (p = rowColumn5) ;
    pm.button (label = "" , p = rowColumn5 , c = partial(setColor, 'purple'  ), bgc = (0.2, 0, 0.302))##(0, 0.3, 0))  
    pm.rowColumnLayout (p = rowColumn5) ;
    pm.button (label = "" , p = rowColumn5 , c = partial(setColor, 'pinky'  ), bgc = (0.8, 0, 0.8))  ##(0.2, 0, 0.302))  
    pm.rowColumnLayout (p = rowColumn5) ;
    pm.button (label = "" , p = rowColumn5 , c = partial(setColor, 'brown'  ), bgc = (0.6, 0.3, 0.2))  ##(0.8, 0, 0.8))  
    pm.rowColumnLayout (p = rowColumn5) ;
    pm.button (label = "" , p = rowColumn5 , c = partial(setColor, 'brownDark'  ), bgc = (0.25, 0.13, 0.13))  ##(0.6, 0.3, 0.2))  
    pm.rowColumnLayout (p = rowColumn5) ;
    pm.button (label = "" , p = rowColumn5 , c = partial(setColor, 'redDark'  ), bgc = (0.500, 0, 0))  ##(0.25, 0.13, 0.13))  
    pm.rowColumnLayout (p = rowColumn5) ;
    pm.button (label = "" , p = rowColumn5 , c = partial(setColor, 'red'  ), bgc = (1, 0, 0))  

    ## RolColumn 3
    rowColumn6 = pm.rowColumnLayout ( nc = 15 , p = mainLayout , h = 20, w = 280 , cw = [ ( 1 , 2) , ( 2 , 38 ), ( 3 , 2), ( 4 , 38 ), ( 5 , 2), ( 6 , 38 ), ( 7 , 2), ( 8 , 38), ( 9 , 2 ), ( 10 , 38), ( 11 , 2 ), ( 12 ,37), ( 13 , 2 ), ( 14 , 38), ( 15 , 2 )] ) ;
    pm.rowColumnLayout (p = rowColumn6) ;
    pm.button (label = "" , p = rowColumn6 , c = partial(setColor, 'green'  ), bgc = (0, 1, 0))  ##(1, 0, 0))  
    pm.rowColumnLayout (p = rowColumn6) ;
    pm.button (label = "" , p = rowColumn6 , c = partial(setColor, 'blueDark'  ), bgc = (0, 0.3, 0.6))   ##(0, 1, 0))   
    pm.rowColumnLayout (p = rowColumn6) ;
    pm.button (label = "" , p = rowColumn6 , c = partial(setColor, 'white'  ), bgc = (1, 1, 1))   ##(0, 0.3, 0.6))   
    pm.rowColumnLayout (p = rowColumn6) ;
    pm.button (label = "" , p = rowColumn6 , c = partial(setColor, 'yellow'  ), bgc = (1, 1, 0))  ##(1, 1, 1))   
    pm.rowColumnLayout (p = rowColumn6) ;
    pm.button (label = "" , p = rowColumn6 , c = partial(setColor, 'softBlue'  ), bgc = (0, 1, 1))  ##(1, 1, 0))   
    pm.rowColumnLayout (p = rowColumn6) ;
    pm.button (label = "" , p = rowColumn6 , c = partial(setColor, 'greenWhite'  ), bgc = (0, 1, 0.8))  ##(0, 1, 1))   
    pm.rowColumnLayout (p = rowColumn6) ;
    pm.button (label = "" , p = rowColumn6 , c = partial(setColor, 'pinkyWhite'  ), bgc = (1, 0.5, 0.75))   


    # Line Control shape B ------------------------------------------------------------------------------------
    rowLine2 = pm.rowColumnLayout ( nc = 3 , p = mainLayout , h = 10, w = 280 ) ;
    pm.button (label = "-"*100 ,h = 10, w = 300, p = rowLine2 , bgc = (0.333333, 0.333333, 0.333333)) ; 

    window.show () ;
# CreateControlUI () ;

def curveShape(curveCtrl = '',*args):
    curveCtrl = curveCtrl
    # checkMirror = pm.checkBox( "checkMirror", q=True, value=True )
    if pm.checkBox( "geoVischeck", q=True, value=True ) :
        geoVis = True
    else :
        geoVis = False     

    if mc.radioButton('connectionRadio',q = True, select = True) :
        utaCore.createControlCurve(curveCtrl, parentCon =False, connections = True, geoVis = geoVis)

    elif mc.radioButton('parentConRadio',q = True, select = True) :
        utaCore.createControlCurve(curveCtrl, parentCon =True, connections = False, geoVis = geoVis) 
    else:
        utaCore.createControlCurve(curveCtrl, parentCon =False, connections = False, geoVis = geoVis)  

    # if  mc.radioButton('fkChainCheck',q = True, select = True) :
    if  pm.checkBox( "fkChainCheck", q=True, value=True ) :
        print '8'
    else :
        print '88' 
        # listsGrpCtrlName = []
        # listsGmblCtrlName = []
        # grpCtrlName, grpCtrlOfsName, grpCtrlParsName, ctrlName, GmblCtrlName = utaCore.createControlCurve(curveCtrl, parentCon =False, connections = False, geoVis = geoVis) 
        # listsGrpCtrlName.append(grpCtrlName)
        # listsGmblCtrlName.append(GmblCtrlName)
        # print listsGrpCtrlName,'...listsGrpCtrlName..'
        # print listsGmblCtrlName, '...listsGmblCtrlName..'    
def setColor (cor = '',*args):
    # cor = cor
    # utaCore.colors(cor = cor)
    sel = mc.ls(sl = True)
    utaCore.assignColor(obj = [sel], col = cor)
    # print cor

