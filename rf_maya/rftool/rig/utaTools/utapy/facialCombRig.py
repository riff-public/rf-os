import sys
sys.path.append(r"P:\lip\lical\utaTools\utapy")
import utaRefElem as rf
reload(rf)
import maya.cmds as mc
reload(mc)
# FclRig
import fclRig 
reload(fclRig)
# blendShape Hd 2 Fcl
from lpRig import core as lrc
reload(lrc)
from lpRig import rigTools as lrr
reload(lrr)


#1. reference allObj
def mainGroup():
    fclRig.createMainGroup()
def refEyeRig():    
    rf.refElem("eyeRig")
def refEbRig():
    rf.refElem("ebRig")
def refMthRig():
    rf.refElem("mthRig")
def refNsRig():
    rf.refElem("nsRig")
def refEarRig():
    rf.refElem("earRig")
def refDtlRig():
    rf.refElem("dtlRig")
def refTtRig(): 
    rf.refElem("ttRig")
def refHdRig():
    rf.refElem("hdRig")

# duplicate BodyFclRigPos_Geo --------------
def dupHeadFclRig(headSel=''):
#2. duplicate "BodyFclRigPos_Geo" and parent to FclRigStill_Grp
    if mc.objExists (headSel)==True:
        pass
    else :
        headSel = mc.ls("HeadFclRig_Geo")
    headGeoPos = mc.duplicate(headSel)
    headName = mc.rename(headGeoPos, "BodyFclRigPost_Geo")
    mc.parent(headName,w=True)
    mc.parent(headName, "FclRigStill_Grp")

# connect Fcl ------------------------------
def connectHdRig():
    #3. FclRig HdRig
    fclRig.createHdRigSpaceGroup()
    fclRig.parentHdRig()

def connectEyeRig():
    #4. FclRig EyeRig
    fclRig.parentEyeRig()
    fclRig.stickEyeCtrl()

def connectEbRig():
    #5. FclRig EbRig
    fclRig.parentEbRig()
    fclRig.stickEbCtrl()

def connectMthRig():
    #6. FclRig MthRig
    fclRig.parentMthRig()
    fclRig.stickMthCtrl()

def connectTtRig():
    #7. FclRig TtRig
    fclRig.parentTtRig()

def connectNsRig():
    #8. FclRig NsRig
    fclRig.parentNsRig()

def connectEarRig():
    #9. FclRig EarRig
    fclRig.parentEarRig()

def connectDtlRig():
    #10 FclRig DtlRig
    fclRig.parentDtlRig()
    fclRig.stickDtlCtrl(
            postGeo ="BodyFclRigPost_Geo",
            ctrls = [
                    'NsDtlRig_L_Ctrl', 
                    'CheekADtlRig_L_Ctrl',
                    'CheekBDtlRig_L_Ctrl',
                    'CheekCDtlRig_L_Ctrl', 
                    'NsDtlRig_R_Ctrl', 
                    'CheekADtlRig_R_Ctrl',
                    'CheekBDtlRig_R_Ctrl', 
                    'CheekCDtlRig_R_Ctrl', 
                    'EyeSktADtlRig_L_Ctrl',
                    'EyeSktBDtlRig_L_Ctrl', 
                    'EyeSktADtlRig_R_Ctrl', 
                    'EyeSktBDtlRig_R_Ctrl',
                    'PuffDtlRig_L_Ctrl', 
                    'PuffDtlRig_R_Ctrl',
                    'NsDtlRig_Ctrl',
                    'EyeBrowDtlRig_Ctrl',
                    'EyeBrowADtlRig_L_Ctrl',
                    'EyeBrowBDtlRig_L_Ctrl',
                    'EyeBrowCDtlRig_L_Ctrl',
                    'EyeBrowADtlRig_R_Ctrl',
                    'EyeBrowBDtlRig_R_Ctrl',
                    'EyeBrowCDtlRig_R_Ctrl',])
            
def getHeadFcl():
    sel = mc.ls("*:HeadFclRig_Geo")
    return sel

#if you have onather dtlControl , can you add 
'''
from lpRig import detailControl
detailControl.attachSelectedControlsToSelectedMesh()
'''

#11. Add Blend Shape



def addBshFacial(hdRigList = [], fclRigList = []):
    for i in range(0, len(hdRigList)):
        currHdRig = []
        currFclRig = []
        #currHdRig = ("*:%s" % (hdRigList[i]));
        #currFclRig = fclRigList[i];
        #print currHdRig, currFclRig;
        
        
        currHdRig = mc.ls("*:%s" % hdRigList[i])
        currFclRig = (fclRigList[i])
        print currHdRig, currFclRig;
        
        mc.select(currHdRig)
        mc.select(currFclRig, add=True)
        lrr.doAddBlendShape()
        
        
def stickDtlCtrlAdd(postGeo="", ctrls= []):

    # ctrls = ['NsDtlRig_L_Ctrl', 'CheekADtlRig_L_Ctrl', 'CheekBDtlRig_L_Ctrl',
    #       'CheekCDtlRig_L_Ctrl', 'NsDtlRig_R_Ctrl', 'CheekADtlRig_R_Ctrl',
    #       'CheekBDtlRig_R_Ctrl', 'CheekCDtlRig_R_Ctrl', 'EyeSktADtlRig_L_Ctrl',
    #       'EyeSktBDtlRig_L_Ctrl', 'EyeSktADtlRig_R_Ctrl', 'EyeSktBDtlRig_R_Ctrl',
    #       'PuffDtlRig_L_Ctrl', 'PuffDtlRig_R_Ctrl']
    # mc.select(cl=True)
    for ctrl in ctrls:
        currCtrls = mc.ls('*:{ctrl}'.format(ctrl=ctrl))
        if not currCtrls:
            continue
        mc.select(currCtrls[0], add=True)

    mc.select(postGeo, add=True)
    rvts = detailControl.attachSelectedControlsToSelectedMesh()
    mc.parent(rvts, RVT_GRP)












# findMainGroup(part = "DtlRig")
# def findMainGroup(part=''):
#     '''
#     Find the main groups of the given facial rig part.
#     '''
#     ctrl = None
#     jnt = None
#     skin = None
#     still = None

#     for each in mc.ls():
#         if each.endswith(':{part}Ctrl_Grp'.format(part=part)):
#             ctrl = each
#         elif each.endswith(':{part}Jnt_Grp'.format(part=part)):
#             jnt = each
#         elif each.endswith(':{part}Skin_Grp'.format(part=part)):
#             skin = each
#         elif each.endswith(':{part}Still_Grp'.format(part=part)):
#             still = each
#         elif each.endswith(':{part}Hi_Grp'.format(part=part)):
#             ctrl = each
#         elif each.endswith(':{part}Mid_Grp'.format(part=part)):
#             ctrl = each
#         elif each.endswith(':{part}Low_Grp'.format(part=part)):
#             ctrl = each

#     return ctrl, jnt, skin, still

