import maya.cmds as mc
reload(mc)
import pymel.core 
def sinePosition (elem , obj,*args):
	#obj = OnOffSwitch1_Ctrl

	# ##Add Attr Sine Position
	# mc.addAttr(obj, ln = '____SinePosition____',at='double',dv = 0,k=True)
	# mc.setAttr('%s.____SinePosition____' % obj, lock = True)

	AttrSinePos = mc.addAttr(obj,ln='Position',at='float',dv = 0,k=True)

	valueSineTZ = mc.getAttr('%sSine_Handle.translateZ' % elem )
	SinePosMdv = mc.createNode('multiplyDivide',n = '%sSinePos_Mdv' % elem )
	SinePosPma = mc.createNode('plusMinusAverage',n = '%sSinePos_Pma' % elem )
	mc.connectAttr('%s.Position' % obj, '%s.input1X' % SinePosMdv)
	mc.connectAttr('%s.outputX' % SinePosMdv ,'%s.input2D[0].input2Dx' % SinePosPma )
	mc.setAttr('%s.input2D[1].input2Dx' % SinePosPma ,valueSineTZ)
	mc.connectAttr('%s.output2Dx' % SinePosPma ,'%sSine_Handle.translateZ' % elem )