import maya.cmds as mc
reload(mc)
import getNameSpace as gns 
reload(gns)

def tailNonFlip(obj,elem, *args):
	nsLists = gns.getNs(nodeName=obj)
	tailIk = mc.ls('%sTailIk*_Ctrl' % nsLists)
	ikhTail = ('%sNakeeTail_Ikh' % nsLists)
	tailIkCtrlObj = []
	for tailIkLst in tailIk:
	    tailIkCtrl = tailIkLst
	    if'Gmbl_' in tailIkCtrl:
	        pass
	    else:
	        tailIkCtrlObj.append(tailIkCtrl)
	print tailIkCtrlObj
	print len(tailIkCtrlObj)
	tailPma = mc.createNode('plusMinusAverage', n ='%s.pma' % elem)
	x = 1
	value = -1.1
	for i in range(len(tailIkCtrlObj)):
		tailLst = mc.addAttr(tailIkCtrlObj[i], ln= 'twist__________', at='double',dv = 0, k = True)
		mc.addAttr(tailIkCtrlObj[i], ln = 'Values',at='double',dv = 0,k=True)
		mc.setAttr('%s.Values' % tailIkCtrlObj[i], value)
		tailMdv = mc.createNode('multiplyDivide', n = '%s%s.mdv' % (elem, x+1))
		mc.connectAttr('%s.Values' % tailIkCtrlObj[i], '%s.input2X' % tailMdv)
		mc.connectAttr('%s.translateX' % tailIkCtrlObj[i], '%s.input1X' % tailMdv)
		mc.connectAttr('%s.outputX' % tailMdv, '%s.input1D[%s]' % (tailPma, x))

		value-=0.1
		x+=1

	mc.connectAttr('%s.output1D' % tailPma, '%s.twist' % ikhTail)