import pymel.core as pm
import maya.OpenMaya as om
from nuTools import misc 
from nuTools import controller

reload(misc)
reload(controller)

import math, random

def ribbonNonRollRig(rbnCtrl=None, elem='', side='', skinGrp='skin_grp', animGrp='anim_grp',
	rbnAttr='detailControl', 
	visAttrName='secDetailControl', 
	autoTwstAttrName='secAutoTwist',
	ctrlColor='pink'):
	if not rbnCtrl:
		rbnCtrl = misc.getSel(selType='transform', num=1)
		if not rbnCtrl:
			return
	if isinstance(rbnCtrl, (str, unicode)):
		rbnCtrl = pm.PyNode(rbnCtrl)

	dtlCtrlGrps = rbnCtrl.attr(rbnAttr).outputs()
	dtlCtrls = []
	dtlCons = []
	dtlJnts = []
	for grp in dtlCtrlGrps:
		dtlTwGrp = grp.getChildren(type='transform')[0]
		dtlCtrl = dtlTwGrp.getChildren(type='transform')[0]
		dtlCon = grp.getChildren(type='parentConstraint')[0]

		jntPar = dtlCtrl.outputs(type='parentConstraint')[0]
		dtlJntZro = jntPar.outputs(type='transform')[0]
		dtlJnt = dtlJntZro.getChildren(type='joint')[0]

		dtlCtrls.append(dtlCtrl)
		dtlCons.append(dtlCon)
		dtlJnts.append(dtlJnt)

	posGrps = [c.getTargetList()[0] for c in dtlCons]
	
	# sort list by names
	dtlCtrls = sorted(dtlCtrls, key=lambda x: x.nodeName())
	dtlCons = sorted(dtlCons, key=lambda x: x.nodeName())
	posGrps = sorted(posGrps, key=lambda x: x.nodeName())
	dtlJnts = sorted(dtlJnts, key=lambda x: x.nodeName())

	rigGrp = pm.group(em=True, n='%sRig%s_Grp' %(elem, side))
	jntGrp = pm.group(em=True, n='%sSkin%s_Grp' %(elem, side))
	ctrlGrp = pm.group(em=True, n='%sAnim%s_Grp' %(elem, side))
	pm.parent(ctrlGrp, rigGrp)
	try:
		pm.parent(jntGrp, pm.PyNode(skinGrp))
	except:
		pass

	try:
		pm.parent(rigGrp, pm.PyNode(animGrp))
	except:
		pass

	visAttr = misc.addNumAttr(rbnCtrl.getShape(), visAttrName, 'long', min=0, max=1, dv=0, hide=False, lock=False, key=True)
	pm.connectAttr(visAttr, ctrlGrp.visibility)

	autoTwstAttr = misc.addNumAttr(rbnCtrl, autoTwstAttrName, 'double', min=0, max=1, dv=1, hide=False, lock=False, key=True)
	baseTwstGrp = dtlCtrls[0].getParent()
	
	nrJnts, nrCtrls = [], []
	i = 0
	for j, c in zip(dtlJnts, dtlCtrls):
		nrJnt = j.duplicate(po=True, n='%s%s%s_Jnt' %(elem, (i+1), side))[0]

		nrJnt.radius.set(j.radius.get()*1.1)
		misc.lockAttr(obj=nrJnt, lock=False, t=True, r=True, s=True, v=False)
		misc.hideAttr(obj=nrJnt, hide=False, t=True, r=True, s=True, v=False)

		pm.parent(nrJnt, jntGrp)
		pm.makeIdentity(nrJnt, apply=True)
		nrJnts.append(nrJnt)

		nrCtrl = c.duplicate(n='%s%s%s_Ctrl'%(elem, (i+1), side))[0]
		misc.scaleCtrlVtx(inc=True, percent=10, obj=nrCtrl)
		
		ctrlZgrp = misc.zgrp(nrCtrl, element='Zro', suffix='Grp')[0]
		pm.parent(ctrlZgrp, ctrlGrp)

		nrCtrls.append(nrCtrl)

		misc.lockAttr(obj=nrCtrl, lock=False, t=True, r=True, s=True, v=False)
		misc.hideAttr(obj=nrCtrl, hide=False, t=True, r=True, s=True, v=False)
		unused_attrs = nrCtrl.listAttr( r=True, s=True, k=True, ud=True)
		for a in unused_attrs:
			a.lock()
			a.setKeyable(False)
			a.showInChannelBox(False)

		pm.parentConstraint(posGrps[i], ctrlZgrp)
		pm.parentConstraint(nrCtrl, nrJnt)
		# pm.scaleConstraint(nrCtrl, nrJnt, mo=True)

		
		twstGrp = None
		ro_input_attr = None
		ro_source_attr = None
		# if connectTwst == True:
		twstGrp = dtlCtrls[i].getParent()
		# else:
			# twstGrp = baseTwstGrp

		for rax in ['rx', 'ry', 'rz']:
			ro_attr = twstGrp.attr(rax)
			if ro_attr.isConnected():

				ro_input_attr = rax
				ro_source_attr = ro_attr.inputs(p=True, scn=True)[0]
				break

		if ro_input_attr and ro_source_attr:
			bTwoAttr = pm.createNode('blendTwoAttr', n='%s%sAutoTwst%s_bta' %(elem, (i+1), side))
			bTwoAttr.input[0].set(0.0)

			ctrlTwstGrp = pm.group(nrCtrl, n='%s%sTwst%s_Grp' %(elem, (i+1), side))

			pm.connectAttr(autoTwstAttr, bTwoAttr.attributesBlender)
			pm.connectAttr(ro_source_attr, bTwoAttr.input[1])
			pm.connectAttr(bTwoAttr.output, ctrlTwstGrp.attr(ro_input_attr))

		# scale
		scalePma = pm.createNode('plusMinusAverage', n='%s%sSquash%s_pma' %(elem, (i+1), side))
		for ax in 'xyz':
			pm.connectAttr(nrCtrl.attr('s%s' %ax), scalePma.input3D[0].attr('input3D%s' %ax))
			pm.connectAttr(j.attr('s%s' %ax), scalePma.input3D[1].attr('input3D%s' %ax))
		pm.connectAttr(scalePma.output3D, nrJnt.scale)

		i += 1

	misc.setWireFrameColor(color=ctrlColor, objs=nrCtrls)