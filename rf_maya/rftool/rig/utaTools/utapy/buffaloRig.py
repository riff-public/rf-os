import maya.cmds as mc
from utaTools.utapy import utaCore
reload(utaCore)


## Rope5 Ctrl
Rope5RblCtrlLoc = ['Rope5RblCtrl1_Loc', 
					'Rope5RblCtrl2_Loc', 
					'Rope5RblCtrl3_Loc', 
					'Rope5RblCtrl4_Loc', 
					'Rope5RblCtrl5_Loc', 
					'Rope5RblCtrl6_Loc', 
					'Rope5RblCtrl7_Loc', 
					'Rope5RblCtrl1_Loc']

Rope5RblCtrl = ['Rope5RblCtrlZr_1_Grp', 
				'Rope5RblCtrlZr_2_Grp', 
				'Rope5RblCtrlZr_3_Grp', 
				'Rope5RblCtrlZr_4_Grp', 
				'Rope5RblCtrlZr_5_Grp', 
				'Rope5RblCtrlZr_6_Grp', 
				'Rope5RblCtrlZr_7_Grp', 
				'Rope5RblCtrlZr_8_Grp']



## Rope5 Dtl

Rope5RblDtlLoc = [	'Rope5RblDtl1_Loc',
					'Rope5RblDtl2_Loc',
					'Rope5RblDtl3_Loc',
					'Rope5RblDtl4_Loc',
					'Rope5RblDtl5_Loc',
					'Rope5RblDtl6_Loc',
					'Rope5RblDtl7_Loc',
					'Rope5RblDtl8_Loc',
					'Rope5RblDtl9_Loc',
					'Rope5RblDtl10_Loc',
					'Rope5RblDtl11_Loc',
					'Rope5RblDtl12_Loc', 
					'Rope5RblDtl13_Loc', 
					'Rope5RblDtl1_Loc']

Rope5RblDtl = [	'Rope5RblDtlCtrlOfst_1_Grp', 
				'Rope5RblDtlCtrlOfst_2_Grp', 
				'Rope5RblDtlCtrlOfst_3_Grp', 
				'Rope5RblDtlCtrlOfst_4_Grp', 
				'Rope5RblDtlCtrlOfst_5_Grp', 
				'Rope5RblDtlCtrlOfst_6_Grp', 
				'Rope5RblDtlCtrlOfst_7_Grp', 
				'Rope5RblDtlCtrlOfst_8_Grp', 
				'Rope5RblDtlCtrlOfst_9_Grp', 
				'Rope5RblDtlCtrlOfst_10_Grp', 
				'Rope5RblDtlCtrlOfst_11_Grp', 
				'Rope5RblDtlCtrlOfst_12_Grp', 
				'Rope5RblDtlCtrlOfst_13_Grp', 
				'Rope5RblDtlCtrlOfst_14_Grp']

##---------------------------------------------------------------------



## Rope4 Ctrl
Rope4RblCtrlLoc = ['Rope4RblCtrl1_Loc', 
					'Rope4RblCtrl2_Loc', 
					'Rope4RblCtrl3_Loc', 
					'Rope4RblCtrl4_Loc', 
					'Rope4RblCtrl5_Loc', 
					'Rope4RblCtrl6_Loc', 
					'Rope4RblCtrl1_Loc']

Rope4RblCtrl = ['Rope4RblCtrlZr_1_Grp', 
				'Rope4RblCtrlZr_2_Grp', 
				'Rope4RblCtrlZr_3_Grp', 
				'Rope4RblCtrlZr_4_Grp', 
				'Rope4RblCtrlZr_5_Grp', 
				'Rope4RblCtrlZr_6_Grp', 
				'Rope4RblCtrlZr_7_Grp']

## Rope4 Dtl

Rope4RblDtlLoc = [	'Rope4RblDtl1_Loc',
					'Rope4RblDtl2_Loc',
					'Rope4RblDtl3_Loc',
					'Rope4RblDtl4_Loc',
					'Rope4RblDtl5_Loc',
					'Rope4RblDtl6_Loc',
					'Rope4RblDtl7_Loc',
					'Rope4RblDtl8_Loc',
					'Rope4RblDtl9_Loc',
					'Rope4RblDtl10_Loc',
					'Rope4RblDtl11_Loc',
					'Rope4RblDtl1_Loc']

Rope4RblDtl = [	'Rope4RblDtlCtrlOfst_1_Grp', 
				'Rope4RblDtlCtrlOfst_2_Grp', 
				'Rope4RblDtlCtrlOfst_3_Grp', 
				'Rope4RblDtlCtrlOfst_4_Grp', 
				'Rope4RblDtlCtrlOfst_5_Grp', 
				'Rope4RblDtlCtrlOfst_6_Grp', 
				'Rope4RblDtlCtrlOfst_7_Grp', 
				'Rope4RblDtlCtrlOfst_8_Grp', 
				'Rope4RblDtlCtrlOfst_9_Grp', 
				'Rope4RblDtlCtrlOfst_10_Grp', 
				'Rope4RblDtlCtrlOfst_11_Grp', 
				'Rope4RblDtlCtrlOfst_12_Grp']

##---------------------------------------------------------------------

## Rope3 Ctrl
Rope3RblCtrlLoc = ['Rope3RblCtrl1_Loc', 
					'Rope3RblCtrl2_Loc', 
					'Rope3RblCtrl3_Loc']

Rope3RblCtrl = ['Rope3RblCtrlZr_1_Grp', 
				'Rope3RblCtrlZr_2_Grp', 
				'Rope3RblCtrlZr_3_Grp']

## Rope3 Dtl

Rope3RblDtlLoc = [	'Rope3RblDtl1_Loc',
					'Rope3RblDtl2_Loc',
					'Rope3RblDtl3_Loc',
					'Rope3RblDtl4_Loc']

Rope3RblDtl = [	'Rope3RblDtlCtrlOfst_1_Grp', 
				'Rope3RblDtlCtrlOfst_2_Grp', 
				'Rope3RblDtlCtrlOfst_3_Grp', 
				'Rope3RblDtlCtrlOfst_4_Grp']


def buffaloRope(*args):
	print '>> Generate : Snap Rope5 Ctrl'
	for i in range(len(Rope5RblCtrlLoc)):
		utaCore.snapSel(obj1 = Rope5RblCtrlLoc[i], obj2= Rope5RblCtrl[i])

	print '>> Generate : Snap Rope5 Dtl'
	for i in range(len(Rope5RblDtlLoc)):
		utaCore.snapSel(obj1 = Rope5RblDtlLoc[i], obj2= Rope5RblDtl[i])

	print '>> Generate : Snap Rope4 Ctrl'
	for i in range(len(Rope4RblCtrlLoc)):
		utaCore.snapSel(obj1 = Rope4RblCtrlLoc[i], obj2= Rope4RblCtrl[i])

	print '>> Generate : Snap Rope4 Dtl'
	for i in range(len(Rope4RblDtlLoc)):
		utaCore.snapSel(obj1 = Rope4RblDtlLoc[i], obj2= Rope4RblDtl[i])

	print '>> Generate : Snap Rope3 Ctrl'
	for i in range(len(Rope3RblCtrlLoc)):
		utaCore.snapSel(obj1 = Rope3RblCtrlLoc[i], obj2= Rope3RblCtrl[i])

	print '>> Generate : Snap Rope3 Dtl'
	for i in range(len(Rope3RblDtlLoc)):
		utaCore.snapSel(obj1 = Rope3RblDtlLoc[i], obj2= Rope3RblDtl[i])

	print '>> Generate : parentConstraint'
	utaCore.parentScaleConstraintLoop(  obj = 'Rope5Rbl_5_Ctrl', lists = ['Rope3RblCtrlZr_3_Grp'],par = True, sca = True, ori = False, poi = False)
	utaCore.parentScaleConstraintLoop(  obj = 'Rope4Rbl_4_Ctrl', lists = ['Rope3RblCtrlZr_1_Grp'],par = True, sca = True, ori = False, poi = False)
	mc.setAttr('{}.v'.format('Rope3RblCtrlZr_3_Grp'),0)
	mc.setAttr('{}.v'.format('Rope3RblCtrlZr_1_Grp'),0)

def uplegFnt(*args):
	## upLegFnt Left Right
	mc.delete('UpLegRbnCtrlZrFnt_L_Grp_pointConstraint1', 'UpLegRbnCtrlZrFnt_R_Grp_pointConstraint1')
	mc.delete(mc.pointConstraint('MidLegFntPos_L_Jnt', 'UpLegRbnCtrlZrFnt_L_Grp', mo = False))
	mc.delete(mc.pointConstraint('MidLegFntPos_R_Jnt', 'UpLegRbnCtrlZrFnt_R_Grp', mo = False))
	mc.pointConstraint('MidLegFntPos_L_Jnt', 'UpLegRbnCtrlZrFnt_L_Grp', mo = True)
	mc.pointConstraint('MidLegFntPos_R_Jnt', 'UpLegRbnCtrlZrFnt_R_Grp', mo = True)

	## lowLegFnt Left Right
	mc.delete('LowLegRbnCtrlZrFnt_L_Grp_pointConstraint1', 'LowLegRbnCtrlZrFnt_R_Grp_pointConstraint1')
	mc.delete(mc.pointConstraint('LowLegFntPos_L_Jnt', 'LowLegRbnCtrlZrFnt_L_Grp', mo = False))
	mc.delete(mc.pointConstraint('LowLegFntPos_R_Jnt', 'LowLegRbnCtrlZrFnt_R_Grp', mo = False))
	mc.pointConstraint('LowLegFntPos_L_Jnt', 'LowLegRbnCtrlZrFnt_L_Grp', mo = True)
	mc.pointConstraint('LowLegFntPos_R_Jnt', 'LowLegRbnCtrlZrFnt_R_Grp', mo = True)


def lexFlexRoll(obj = '', attrName = 'lexFlex', target = 'LegFlexIkPivFnt_L_Grp', *args):
	if obj:
		objShape = '{}Shape'.format(obj)
		## splitName
		nameAll = utaCore.splitName([obj])
		name = nameAll[0]
		side = nameAll[1]
		lastName = nameAll[-1]

		## Add Attribute
		attrRoll = utaCore.addAttrCtrl (ctrl = obj, elem = ['{}Roll'.format(attrName)], min = 0, max = 1, at = 'long')[0]
		attrRollAmp = utaCore.addAttrCtrl (ctrl = (objShape) , elem = ['{}RollAmp'.format(attrName)], min = -100, max = 100, at = 'float')[0]
		mc.setAttr('{}.{}'.format(obj,'{}Roll'.format(attrName)), 0)
		mc.setAttr('{}Shape.{}'.format(obj,'{}RollAmp'.format(attrName)), 0.1)

		## CreateNode Bcl
		nodeBcl1 = mc.createNode('blendColors', n = '{}{}1{}Bcl'.format(name,attrName, side))
		mc.setAttr ('{}.color1R'.format(nodeBcl1), 0)
		mc.setAttr ('{}.color1G'.format(nodeBcl1), 0.5)
		mc.setAttr ('{}.color2R'.format(nodeBcl1), 0.5)
		mc.connectAttr('{}.{}'.format(obj, attrRoll), '{}.blender'.format(nodeBcl1))

		# createNode Mdv1 2 3
		nodeMdv1 = mc.createNode('multiplyDivide', n = '{}{}1{}Mdv'.format(name,attrName, side))
		nodeMdv2 = mc.createNode('multiplyDivide', n = '{}{}2{}Mdv'.format(name,attrName, side))
		nodeMdv3 = mc.createNode('multiplyDivide', n = '{}{}3{}Mdv'.format(name,attrName, side))

		mc.connectAttr('{}.{}'.format(obj, attrName), '{}.input1Y'.format(nodeMdv1))
		mc.connectAttr('{}.{}'.format(obj, attrName), '{}.input1X'.format(nodeMdv2))
		mc.setAttr('{}.input2Y'.format(nodeMdv1), -1)
		mc.connectAttr('{}.outputY'.format(nodeMdv1), '{}.input1Y'.format(nodeMdv2))
		mc.connectAttr('{}.outputR'.format(nodeBcl1), '{}.input2X'.format(nodeMdv2))
		mc.connectAttr('{}.outputG'.format(nodeBcl1), '{}.input2Y'.format(nodeMdv2))
		mc.connectAttr('{}.outputX'.format(nodeMdv2), '{}.input1X'.format(nodeMdv3))
		mc.connectAttr('{}.outputY'.format(nodeMdv2), '{}.input1Y'.format(nodeMdv3))
		mc.connectAttr('{}.{}'.format(objShape, attrRollAmp), '{}.input2Y'.format(nodeMdv3))

		## Connect Attr to Grp
		attrCon = ['tz', 'rx']
		for each in attrCon:
			conCheck = utaCore.checkConnect(object = target, attr = each)
			if not conCheck  == None:
				utaCore.disConToObj(objA = conCheck[0], objB = '{}.{}'.format(target, each), connectionsObj = False)
				mc.connectAttr('{}.outputX'.format(nodeMdv3), '{}.{}'.format(target, each), f = True)

			else:
				mc.connectAttr('{}.outputY'.format(nodeMdv3), '{}.{}'.format(target, each), f = True)


		