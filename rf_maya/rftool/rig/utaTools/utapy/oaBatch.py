import os
import maya.cmds as mc
from utaTools.utapy import utaRefElem
reload(utaRefElem)
from rftool import rig
from rf_app.save_plus import save_utils
reload(save_utils)
from utaTools.utapy import export_Batch
reload(export_Batch)
from rf_utils import publish_info
reload(publish_info)
from rf_utils.context import context_info
reload(context_info)
from rf_utils import file_utils
reload(file_utils)
from utaTools.utapy import runMainRig as rmr
reload(rmr)
from utaTools.utapy import cleanUp
reload(cleanUp)
from utaTools.pkrig import rigTools
reload(rigTools)
from rf_maya.nuTools.util import bshUtil
reload(bshUtil)
from utaTools.utapy import utaCore
reload(utaCore)

'''
from utaTools.utapy import oaBatch
reload(oaBatch)
oaBatch.run(    fromObj = 'hogGirlDefaultHairA', 
                toObj = 'hogGirlDefaultOa',
                batchProcess = ['skel', 
                                'ctrl', 
                                'bodyRig',
                                'main'] )
'''
def run(batchProcess = ['skel', 'ctrl', 'bodyRig', 'main'], fromObj = 'hogGirlDefaultHairA', toObj = 'hogGirlDefaultOa',*args):
	entityFirst = utaRefElem.entityFirst()
	'''P:/SevenChickMovie/asset/publ/char'''
	rigUser = os.environ.get('RFUSER')
	# print 'test'
	# print batchProcess, 'batchProcess'
	for process in batchProcess:
		print process, '........process_batch'
		## Check Folder process
		utaCore.createFolderPath(entityFirst = entityFirst, workOrPubl = 'work',  charName = toObj,createProcess = [process])
		if process == 'skel':
			print 'in skel'
			## Reference Hero File
			fldHePath = utaRefElem.refElemStepBatch(char = fromObj, stepDep = 'rig', elem = process, path = entityFirst)

			## Save As File Work
			elemPath = saveWorkStepBatch(char = toObj, stepDep = 'rig', elem = process,user = '{}Batch'.format(rigUser), count = 1)

			## increment version work
			save_utils.save_increment()

		elif process == 'ctrl':
			print 'in ctrl'
			## Reference Hero File
			fldHePath = utaRefElem.refElemStepBatch(char = toObj, stepDep = 'rig', elem = 'skel', path = entityFirst)

			## Save As File Work
			elemPath = saveWorkStepBatch(char = toObj, stepDep = 'rig', elem = process,user = 'Ken', count = 1)

			entity = context_info.ContextPathInfo()
			charName = entity.name
			publPathR = entity.path.publish()
			publPathP = publPathR.replace('$RFVERSION', 'P:')

			## Copy data Folder
			fldData = 'data'
			src = '{}/{}/{}/{}/{}'.format(publPathP, 'char', fromObj, 'rig' , fldData)
			dst = '{}/{}/{}/{}/{}'.format(publPathP, 'char', toObj, 'rig' , fldData)

			## Create Folder and copy file

			utaCore.createFolderPath(entityFirst = entityFirst, workOrPubl = 'publ',  charName = toObj,createProcess = ['data'])
			file_utils.xcopy_directory(src , dst)

			## CleanFile
			cleanFile()

			## Run MainRig
			rmr.runMainRig()
			if mc.objExists('Export_Grp'):
				mc.delete('Export_Grp')

			## increment version work
			save_utils.save_increment()

		elif process == 'bodyRig':
			print 'in bodyRig'
			## Reference Hero File
			fldHePath = utaRefElem.refElemStepBatch(char = fromObj, stepDep = 'rig', elem = process, path = entityFirst)

			## Save As File Work
			elemPath = saveWorkStepBatch(char = toObj, stepDep = 'rig', elem = process,user = 'Ken', count = 1)

			## RePlace ctrl hero File
			switchRefPath(entity , step = 'rig' , process = 'ctrl', res = 'md')

			## increment version work
			save_utils.save_increment()

		elif process == 'main':
			print 'in main'
			## Reference Hero File
			fldHePath = utaRefElem.refElemStepBatch(char = fromObj, stepDep = 'rig', elem = process, path = entityFirst)

			## Save As File Work
			elemPath = saveWorkStepBatch(char = toObj, stepDep = 'rig', elem = process,user = 'Ken', count = 1)

			## RePlace ctrl hero File
			entity = context_info.ContextPathInfo()
			switchRefPath(entity , step = 'rig' , process = 'bodyRig', res = 'md')

			## increment version work
			save_utils.save_increment()


		## Publihs bodyRig Hero File
		pathFile = mc.file(q = True, loc = True)
		publishBatch(path = pathFile)
		mc.file(new = True, f = True)
		print '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>', process, 'is DONE', '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<'
		print '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>', process, 'is DONE', '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<'
	## Dialog Show
	processFinish()

def processFinish(*args):
	dialogShow = mc.confirmDialog( title='Confirm', 
		message='Crowd Batch is Done!!')


def switchRefPath(entity,step = 'rig' , process = 'ctrl', res = 'md' , *args):
	asset = entity.copy()
	allRefPath =  mc.file(q=True,reference=True)
	for each in allRefPath:
		if 'ctrl' in each:
			refPath = each
		elif 'bodyRig' in each:
			refPath = each
		else:
			pass
	refNode = mc.file(refPath,q=True,rfn=True)
	asset.context.update(step=step,process=process, res=res)
	heroPath = asset.path.output_hero()
	heroFile = asset.publish_name(hero=True)
	rigFile = '{}/{}'.format(heroPath, heroFile)
	mc.file(rigFile,lr=refNode)

# switchRefPath(entity,step = 'rig' , process = 'bodyRig', res = 'md')


def saveWorkStepBatch(char = '', stepDep = '', elem='',user = '', count = 1, *args):

	pathGuide = 'P:/SevenChickMovie/asset/work/char'
	fldPath = '{}/{}/{}/{}/maya'.format(pathGuide, char, stepDep, elem)
	workName = '{}_{}_{}.v00{}.{}.ma'.format(char, stepDep, elem, count, user)
	## Hero Path
	fldHePath = '{}/{}'.format(fldPath, workName)
	elemPath = os.path.join(fldHePath)	

	mc.file(rename = elemPath)
	mc.file(save=True, type="mayaAscii")

	return elemPath

def publishBatch(path = '',*args):

	## CleanFile
	cleanFile()

	entityAsset = context_info.ContextPathInfo(path = path)
	entityAsset.context.update(res='md')

	## Publish
	publishInfo = publish_info.Register(entityAsset)
	export_Batch.run(publishInfo, runProcess=True)

def cleanFile(*args):
	## Clean up 
	cleanUp.clearDeleteGrp()
	cleanUp.clearLayer()
	cleanUp.clearUnusedAnim()
	cleanUp.clearUnknowNode()
	cleanUp.clearUnusedNode()
	cleanUp.clearVraySettings()
	cleanUp.clearCameraDefault()
	rigTools.scaleGimbalControl()
	cleanUp.clearInheritsTransform()
	bshUtil.turnOffBshEdits()

	objSets = mc.ls('set_*')
	for each in objSets:
		mc.delete(each)
	cleanUp.checkParameterUV()