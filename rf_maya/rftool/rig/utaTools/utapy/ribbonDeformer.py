import maya.cmds as mc
import maya.mel as mm
from ncmel import core
from ncmel import rigTools as rt
reload( core )
reload( rt )

# -------------------------------------------------------------------------------------------------------------
#
#  RIBBON RIGGING MODULE
#  
# -------------------------------------------------------------------------------------------------------------

def run( name = '' , posiA = '' , posiB = '' , axis = '' , side = '' , hi = True , numJnts = 5 , nurbValue = 5 ):

    dist = rt.getDist( posiA , posiB )

    if hi :
        ribbon = RibbonHi( name = name , axis = axis , side = side , dist = dist , numJnts = numJnts  , nurbValue = nurbValue)
    else :
        ribbon = RibbonLow( name = name , axis = axis , side = side , dist = dist )

    ribbon.rbnCtrlGrp.snapPoint(posiA)
    
    mc.delete( 
                   mc.aimConstraint( posiB , ribbon.rbnCtrlGrp , 
                                     aim = ribbon.aimVec , 
                                     u = ribbon.upVec , 
                                     wut='objectrotation' , 
                                     wuo = posiA , 
                                     wu= ribbon.upVec )
              )
    
    mc.parentConstraint( posiA , ribbon.rbnCtrlGrp , mo = True )
    mc.parentConstraint( posiA , ribbon.rbnRootCtrl)
    mc.parentConstraint( posiB , ribbon.rbnEndCtrl)

    for ctrl in ( ribbon.rbnRootCtrl , ribbon.rbnEndCtrl) :
        ctrl.lockHideAttrs('tx', 'ty', 'tz' , 'rx' , 'ry' , 'rz' )
        ctrl.hide()
    
    if not hi :
        ribbon.rbnStillGrp = ''

    return {    
             'rbnCtrlGrp'   : ribbon.rbnCtrlGrp , 
             'rbnJntGrp'    : ribbon.rbnJntGrp , 
             'rbnSkinGrp'   : ribbon.rbnSkinGrp , 
             'rbnStillGrp'  : ribbon.rbnStillGrp ,
             'rbnCtrl'      : ribbon.rbnCtrl ,
             'rbnRootCtrl'  : ribbon.rbnRootCtrl ,
             'rbnEndCtrl'   : ribbon.rbnEndCtrl
            }
  
class Ribbon( object ):

    def __init__( self , name   = '' ,
                         axis     = 'y+' ,
                         side   = 'L' ,
                         dist   = 1 ,
                         numJnts = 5,
                 ):

        ##-- Info
        rbnInfo = self.ribbonInfo( axis )
    
        self.postAx    = rbnInfo[ 'postAx' ]
        self.twstAx    = rbnInfo[ 'twstAx' ]
        self.sqshAx    = rbnInfo[ 'sqshAx' ]
        self.aimVec    = rbnInfo[ 'aimVec' ]
        self.invAimVec = rbnInfo[ 'invAimVec' ]
        self.upVec     = rbnInfo[ 'upVec' ]
        self.rotate    = rbnInfo[ 'rotate' ]
        self.rotateOrder    = rbnInfo[ 'rotateOrder' ]
        self.sineT     = rbnInfo['sineT']
        self.numJnts    = numJnts

        if side == '' :
            side = '_'
        else :
            side = '_%s_' %side

        #-- Create Main Group
        self.rbnCtrlGrp = rt.createNode( 'transform' , '%sRbnCtrl%sGrp' %( name , side ))
        self.rbnJntGrp = rt.createNode( 'transform' , '%sRbnJnt%sGrp' %( name , side ))
        self.rbnSkinGrp = rt.createNode( 'transform' , '%sRbnSkin%sGrp' %( name , side ))
        
        #-- Create Joint
        self.rbnRootJnt = rt.createJnt( '%sRbnRoot%sJnt' %( name , side ))
        self.rbnMidJnt = rt.createJnt( '%sRbnMid%sJnt' %( name , side ))
        self.rbnEndJnt = rt.createJnt( '%sRbnEnd%sJnt' %( name , side ))

        self.rbnRootJntAim = rt.createNode( 'transform' , '%sRbnRootJntAim%sGrp' %( name , side ))
        self.rbnMidJntAim = rt.createNode( 'transform' , '%sRbnMidJntAim%sGrp' %( name , side ))
        self.rbnEndJntAim = rt.createNode( 'transform' , '%sRbnEndJntAim%sGrp' %( name , side ))

        #-- Create Controls
        self.rbnRootCtrl = rt.createCtrl( '%sRbnRoot%sCtrl' %( name , side ) , 'locator' , 'yellow' )
        self.rbnRootCtrlZro = rt.addGrp( self.rbnRootCtrl )

        self.rbnEndCtrl = rt.createCtrl( '%sRbnEnd%sCtrl' %( name , side ) , 'locator' , 'yellow' )
        self.rbnEndCtrlZro = rt.addGrp( self.rbnEndCtrl )

        self.rbnCtrl = rt.createCtrl( '%sRbn%sCtrl' %( name , side ) , 'circle' , 'yellow' )
        self.rbnCtrlZro = rt.addGrp( self.rbnCtrl )
        self.rbnCtrlAim = rt.addGrp( self.rbnCtrl , 'Aim' )
        self.rbnCtrlPars = rt.addGrp( self.rbnCtrl , 'Pars' )

        mc.parent( self.rbnMidJntAim , self.rbnCtrl )
        mc.parent( self.rbnEndJntAim , self.rbnEndCtrl )
        mc.parent( self.rbnRootJntAim , self.rbnRootCtrl )

        #-- Adjust Shape Controls
        self.rbnCtrl.scaleShape( dist*0.25 )
        self.rbnCtrl.rotateShape( self.rotate[0] , self.rotate[1] , self.rotate[2] )
        
        #-- Adjust Rotate Order
        self.rbnCtrl.setRotateOrder( self.rotateOrder )

        #-- Rig Process
        mc.parentConstraint( self.rbnCtrlGrp , self.rbnSkinGrp , mo = False )
        mc.scaleConstraint( self.rbnCtrlGrp , self.rbnSkinGrp , mo = False )

        mc.parentConstraint( self.rbnRootJntAim , self.rbnRootJnt , mo = False )
        mc.parentConstraint( self.rbnMidJntAim , self.rbnMidJnt , mo = False )
        mc.parentConstraint( self.rbnEndJntAim , self.rbnEndJnt , mo = False )

        if '-' in axis :
            self.rbnCtrlZro.attr( '%s' %self.postAx ).value = -(dist/2)
            self.rbnEndCtrlZro.attr( '%s' %self.postAx ).value = -dist
        else :
            self.rbnCtrlZro.attr( '%s' %self.postAx ).value = dist/2
            self.rbnEndCtrlZro.attr( '%s' %self.postAx ).value = dist

        mc.pointConstraint( self.rbnRootCtrl , self.rbnEndCtrl , self.rbnCtrlZro , mo = False )

        self.aimRootCons = mc.aimConstraint( self.rbnEndCtrl , self.rbnRootJntAim , aim = self.aimVec , u = self.upVec , wut = "objectrotation" , wu = self.upVec , wuo = self.rbnRootCtrlZro )
        self.aimEndCons  = mc.aimConstraint( self.rbnRootCtrl , self.rbnEndJntAim , aim = self.invAimVec , u = self.upVec , wut = "objectrotation" , wu = self.upVec , wuo = self.rbnEndCtrlZro )
        self.aimMidCons  = mc.aimConstraint( self.rbnEndCtrl , self.rbnCtrlAim , aim = self.aimVec , u = self.upVec , wut = "objectrotation" , wu = self.upVec , wuo = self.rbnCtrlZro )
    
        #-- Twist Distribute
        self.rbnCtrl.addTitleAttr( 'Twist' )
        self.rbnCtrl.addAttr( 'autoTwist' , 0 , 1 )
        self.rbnCtrl.addAttr( 'rootTwist' )
        self.rbnCtrl.addAttr( 'endTwist' )

        self.rbnCtrlShape = core.Dag(self.rbnCtrl.shape)

        self.rbnCtrlShape.addAttr( 'rootTwistAmp' )
        self.rbnCtrlShape.addAttr( 'endTwistAmp' )

        self.rbnCtrlShape.addAttr( 'rootAbsoluteTwist' )
        self.rbnCtrlShape.addAttr( 'endAbsoluteTwist' )

        self.rbnCtrlShape.attr('rootTwistAmp').value = 1
        self.rbnCtrlShape.attr('endTwistAmp').value = 1

        self.rbnAmpTwMdv = rt.createNode( 'multiplyDivide' , '%sRbnAmpTwist%sMdv' %( name , side ))
        self.rbnAutoTwMdv = rt.createNode( 'multiplyDivide' , '%sRbnAutoTwist%sMdv' %( name , side ))
        self.rbnRootPma = rt.createNode( 'plusMinusAverage' , '%sRbnRootTwist%sPma' %( name , side ))
        self.rbnEndPma = rt.createNode( 'plusMinusAverage' , '%sRbnEndTwist%sPma' %( name , side ))

        self.rbnCtrlShape.attr( 'rootAbsoluteTwist' ) >> self.rbnAmpTwMdv.attr('i1x')
        self.rbnCtrlShape.attr( 'rootTwistAmp' ) >> self.rbnAmpTwMdv.attr('i2x')
        self.rbnCtrlShape.attr( 'endAbsoluteTwist' ) >> self.rbnAmpTwMdv.attr('i1y')
        self.rbnCtrlShape.attr( 'endTwistAmp' ) >> self.rbnAmpTwMdv.attr('i2y')

        self.rbnCtrl.attr( 'autoTwist' ) >> self.rbnAutoTwMdv.attr('i1x')
        self.rbnCtrl.attr( 'autoTwist' ) >> self.rbnAutoTwMdv.attr('i1y')

        self.rbnAmpTwMdv.attr( 'ox' ) >> self.rbnAutoTwMdv.attr('i2x')
        self.rbnAmpTwMdv.attr( 'oy' ) >> self.rbnAutoTwMdv.attr('i2y')

        self.rbnCtrl.attr( 'rootTwist' ) >> self.rbnRootPma.attr('i1[0]')
        self.rbnCtrl.attr( 'endTwist' ) >> self.rbnEndPma.attr('i1[0]')

        self.rbnAutoTwMdv.attr( 'ox' ) >> self.rbnRootPma.attr('i1[1]')
        self.rbnAutoTwMdv.attr( 'oy' ) >> self.rbnEndPma.attr('i1[1]')
        
        #-- Squash Atribute
        self.rbnCtrl.addTitleAttr( 'Squash' )
        self.rbnCtrl.addAttr( 'autoSquash' , 0 , 1 )
        self.rbnCtrl.addAttr( 'ampSquash' )
        self.rbnCtrl.addAttr( 'squash' )
        self.rbnCtrl.addAttr( 'squashPosition',-10,10 )
        self.rbnCtrl.attr( 'ampSquash').value = 1
        self.rbnCtrlShape.addAttr( 'length' )

        self.rbnPosGrp = rt.createNode( 'transform' , '%sRbnPos%sGrp' %( name , side ))
        self.rbnRootPosGrp = rt.createNode( 'transform' , '%sRbnRootPos%sGrp' %( name , side ))
        self.rbnMidPosGrp = rt.createNode( 'transform' , '%sRbnMidPos%sGrp' %( name , side ))
        self.rbnEndPosGrp = rt.createNode( 'transform' , '%sRbnEndPos%sGrp' %( name , side ))

        mc.pointConstraint(self.rbnRootJnt, self.rbnRootPosGrp)
        mc.pointConstraint(self.rbnMidJnt, self.rbnMidPosGrp)
        mc.pointConstraint(self.rbnEndJnt, self.rbnEndPosGrp)

        self.rootDist = rt.createNode( 'distanceBetween' , '%sRbnRoot%sDist' %( name , side ))
        self.endDist = rt.createNode( 'distanceBetween' , '%sRbnEnd%sDist' %( name , side ))

        self.rbnRootPosGrp.attr('t') >> self.rootDist.attr('p1')
        self.rbnMidPosGrp.attr('t') >> self.rootDist.attr('p2')
        self.rbnMidPosGrp.attr('t') >> self.endDist.attr('p1')
        self.rbnEndPosGrp.attr('t') >> self.endDist.attr('p2')

        self.rbnLenPma = rt.createNode( 'plusMinusAverage' , '%sRbnLen%sPma' %( name , side ))

        self.rootDist.attr('d') >> self.rbnLenPma.attr('i1[0]')
        self.endDist.attr('d') >> self.rbnLenPma.attr('i1[1]')

        self.rbnCtrlShape.attr('length').value = dist
        self.rbnCtrlShape.lockAttrs( 'length' )

        self.rbnSqshDivMdv = rt.createNode( 'multiplyDivide' , '%sRbnSqshDiv%sMdv' %( name , side ))
        self.rbnSqshPowMdv = rt.createNode( 'multiplyDivide' , '%sRbnSqshPow%sMdv' %( name , side ))
        self.rbnSqshNormMdv = rt.createNode( 'multiplyDivide' , '%sRbnSqshNorm%sMdv' %( name , side ))
        
        self.rbnSqshNormMdv.attr('operation').value = 2
        self.rbnLenPma.attr('o1') >> self.rbnSqshNormMdv.attr('i1x')
        self.rbnCtrlShape.attr('length') >> self.rbnSqshNormMdv.attr('i2x')
        
        self.rbnSqshPowMdv.attr('operation').value = 3
        self.rbnSqshNormMdv.attr('ox') >> self.rbnSqshPowMdv.attr('i1x')
        self.rbnSqshPowMdv.attr('i2x').value = 2
        
        self.rbnSqshDivMdv.attr('operation').value = 2
        self.rbnSqshDivMdv.attr('i1x').value = 1
        self.rbnSqshPowMdv.attr('ox') >> self.rbnSqshDivMdv.attr('i2x')


        #-- Adjust Hierarchy
        mc.parent( self.rbnRootJnt , self.rbnMidJnt , self.rbnEndJnt , self.rbnJntGrp )
        mc.parent( self.rbnRootPosGrp , self.rbnMidPosGrp , self.rbnEndPosGrp , self.rbnPosGrp )

        mc.parent( self.rbnCtrlZro , self.rbnRootCtrlZro , self.rbnEndCtrlZro , self.rbnPosGrp , self.rbnCtrlGrp )

        #-- Cleanup
        for obj in ( self.rbnJntGrp , self.rbnSkinGrp ) :
            obj.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' )

        for obj in ( self.rbnRootCtrl , self.rbnEndCtrl ) :
            obj.lockHideAttrs( 'sx' , 'sy' , 'sz' )

        for obj in ( self.rbnRootJnt , self.rbnMidJnt , self.rbnEndJnt ) :
            obj.attr('v').value = 0

        self.rbnCtrlShape.attr( 'rootTwistAmp' ).hide = 1
        self.rbnCtrlShape.attr( 'endTwistAmp' ).hide = 1

        self.rbnCtrlShape.attr( 'rootAbsoluteTwist' ).hide = 1
        self.rbnCtrlShape.attr( 'endAbsoluteTwist' ).hide = 1

        self.rbnCtrl.lockHideAttrs( 'sx' , 'sy' , 'sz' , 'v' )

    def ribbonInfo( self , axis = '' ) :

        if axis == 'x+' :
            postAx = 'tx'
            twstAx =  'rx'
            sqshAx = [ 'sy' , 'sz' ]
            aimVec = [ 1 , 0 , 0 ]
            invAimVec = [ -1 , 0 , 0 ]
            upVec = [ 0 , 0 , 1 ]
            rotate = [ 0 , 0 , -90 ]
            rotateOrder = 0
            sineT = ['ty','tz']
            
        elif axis == 'x-' :
            postAx = 'tx'
            twstAx =  'rx'
            sqshAx = [ 'sy' , 'sz' ]
            aimVec = [ -1 , 0 , 0 ]
            invAimVec = [ 1 , 0 , 0 ]
            upVec = [ 0 , 0 , 1 ]
            rotate = [ 0 , 0 , 90 ]
            rotateOrder = 0
            sineT = ['ty','tz']
        
        elif axis == 'y+' :
            postAx = 'ty'
            twstAx =  'ry'
            sqshAx = [ 'sx' , 'sz' ]
            aimVec =  [ 0 , 1 , 0 ]
            invAimVec = [ 0 , -1 , 0 ]
            upVec = [ 0 , 0 , 1 ]
            rotate = [ 0 , 0 , 0 ]
            rotateOrder = 1
            sineT = ['tx','tz']
            
        elif axis == 'y-' :
            postAx = 'ty'
            twstAx =  'ry'
            sqshAx = [ 'sx' , 'sz' ]
            aimVec = [ 0 , -1 , 0 ]
            invAimVec = [ 0 , 1 , 0 ]
            upVec = [ 0 , 0 , 1 ]
            rotate = [ 0 , 0 , 180 ]
            rotateOrder = 1
            sineT = ['tx','tz']
        
        elif axis == 'z+' :
            postAx = 'tz'
            twstAx =  'rz'
            sqshAx = [ 'sx' , 'sy' ]
            aimVec = [ 0 , 0 , 1 ]
            invAimVec = [ 0 , 0 , -1 ]
            upVec = [ 0 , 1 , 0 ]
            rotate = [ 90 , 0 , 180 ]
            sineT = ['tx','ty']
            rotateOrder = 2
        
        elif axis == 'z-' :
            postAx = 'tz'
            twstAx =  'rz'
            sqshAx = [ 'sx' , 'sy' ]
            aimVec = [ 0 , 0 , -1 ]
            invAimVec = [ 0 , 0 , 1 ]
            upVec = [ 0 , 1 , 0 ]
            rotate = [ -90 , 0 , 0 ]
            rotateOrder = 2
            sineT = ['tx','ty']

        return { 'postAx' : postAx , 
                 'twstAx' : twstAx , 
                 'sqshAx' : sqshAx , 
                 'aimVec' : aimVec , 
                 'invAimVec' : invAimVec , 
                 'upVec' : upVec , 
                 'rotate' : rotate , 
                 'rotateOrder' : rotateOrder ,
                 'sineT' : sineT }

    def nonLinearDeformer(self,name = None , side = None , obj = None , defType = None , *args):

        nonL = mc.nonLinear(obj , typ = defType)
        hndNonT = mc.rename(nonL[1],'{}Transform{}Hnd'.format(name,side))
        hndNon = mc.rename(nonL[0],'{}Node{}Hnd'.format(name,side))

        return { 'handleDag' : hndNonT , 
                 'handleNode' : hndNon  }

class RibbonHi( Ribbon ):

    def __init__( self , name   = '' ,
                         axis     = 'y+' ,
                         side   = 'L' ,
                         dist   = 1 ,
                         numJnts = 5 ,
                         nurbValue = 5 ,
                 ):
        
        super( RibbonHi , self ).__init__( name , axis , side , dist , numJnts)

        ##-- Info
        if side == '' :
            side = '_'
        else :
            side = '_%s_' %side

        #-- Create Main Group
        self.rbnDetailGrp = rt.createNode( 'transform' , '%sRbnDtl%sGrp' %( name , side ))
        self.rbnStillGrp = rt.createNode( 'transform' , '%sRbnStill%sGrp' %( name , side ))

        #-- Create Attribute
        self.rbnCtrlShape.addAttr( 'detailControl' , 0 , 1 )

        #-- Create Nurbs Surface
        self.rbnNrb = core.Dag(mc.nurbsPlane( p = ( 0 , dist/2 , 0 ) , ax = ( 0 , 0 , 1 ) , w = True , lr = dist , d = 3 , u = 1 , v = nurbValue , ch = 0 , n = '%sRbn%sNrb' % ( name , side ))[0])
        self.rbnNrb.attr('rx').value = self.rotate[0]
        self.rbnNrb.attr('ry').value = self.rotate[1]
        self.rbnNrb.attr('rz').value = self.rotate[2]
        self.rbnNrb.freeze()

        self.rbnSkc = mc.skinCluster( self.rbnRootJnt , self.rbnMidJnt , self.rbnEndJnt , self.rbnNrb , dr = 7 , mi = 2 , n = '%sRbn%sSkc' % ( name , side ))[0]
        
        # mc.skinPercent( self.rbnSkc , '%s.cv[0:3][7]' %self.rbnNrb , tv = [ self.rbnEndJnt , 1 ] )
        # mc.skinPercent( self.rbnSkc , '%s.cv[0:3][6]' %self.rbnNrb , tv = [ self.rbnMidJnt , 0.2 ] )
        # mc.skinPercent( self.rbnSkc , '%s.cv[0:3][5]' %self.rbnNrb , tv = [ self.rbnMidJnt , 0.5 ] )
        # mc.skinPercent( self.rbnSkc , '%s.cv[0:3][4]' %self.rbnNrb , tv = [ self.rbnEndJnt , 0.1 ] )
        # mc.skinPercent( self.rbnSkc , '%s.cv[0:3][3]' %self.rbnNrb , tv = [ self.rbnRootJnt , 0.1 ] )
        # mc.skinPercent( self.rbnSkc , '%s.cv[0:3][2]' %self.rbnNrb , tv = [ self.rbnRootJnt , 0.5 ] )
        # mc.skinPercent( self.rbnSkc , '%s.cv[0:3][1]' %self.rbnNrb , tv = [ self.rbnRootJnt , 0.8 ] )
        # mc.skinPercent( self.rbnSkc , '%s.cv[0:3][0]' %self.rbnNrb , tv = [ self.rbnRootJnt , 1 ] )

        cvValue = nurbValue + 2
        skinPart = cvValue / 2
        skinMiddle = nurbValue / 2.0

        # sknRoot = (((1.0 / (skinPart / 2 )) / 2 ) / 4 )
        # print sknRoot

        mc.skinPercent(  self.rbnSkc , '%s.cv[0:3][0]' % self.rbnNrb , tv = [ self.rbnRootJnt , 1 ] )
        mc.skinPercent(  self.rbnSkc , '%s.cv[0:3][%i]' %( self.rbnNrb,cvValue) , tv = [ self.rbnEndJnt , 1 ] )

        if skinPart == (cvValue+1) / 2:
            endRoot = skinPart
            mc.skinPercent(  self.rbnSkc , '%s.cv[0:3][%i]' %(self.rbnNrb,skinPart) , tv = [ self.rbnMidJnt , 1 ] )
        else:
            endRoot = skinPart+1

        self.animCrv = mc.createNode('animCurveTU' , n ='rbnWeightValue')
        mc.setKeyframe(self.animCrv , t=0 , v = 0)
        mc.setKeyframe(self.animCrv , t =endRoot , v =endRoot)
        mc.keyTangent(self.animCrv , e=True , a=True , t=(0,0) , outAngle=90 , outWeight=(endRoot/4)*0.2)
        
        mc.keyTangent(self.animCrv , e=True , a=True , t=(endRoot,endRoot) , inAngle=0 , inWeight=(endRoot/4)*3.5)
        for v in range(2,endRoot):
            val = (mc.keyframe(self.animCrv , t=(v-1,v-1) , q=True , ev=True)[0])/endRoot

            mc.skinPercent( self.rbnSkc , '%s.cv[0:3][%i]' %(self.rbnNrb,v) , tv = [ ( self.rbnRootJnt , 1.0-val), ( self.rbnMidJnt , val)] )
        value = 1
        for v in range(nurbValue,skinPart,-1):
            val = (mc.keyframe(self.animCrv , t=(value,value) , q=True , ev=True)[0])/endRoot

            mc.skinPercent( self.rbnSkc , '%s.cv[0:3][%i]' %(self.rbnNrb,v) , tv = [ ( self.rbnEndJnt , 1.0-val), ( self.rbnMidJnt , val)] )
            value += 1
        val = (mc.keyframe(self.animCrv , t=(1,1) , q=True , ev=True)[0])/endRoot*0.3
        mc.skinPercent(  self.rbnSkc , '%s.cv[0:3][1]' % self.rbnNrb , tv = [ (self.rbnRootJnt , 1 - val) , (self.rbnMidJnt , val) ] )
        mc.skinPercent(  self.rbnSkc , '%s.cv[0:3][%i]' %( self.rbnNrb,cvValue-1) , tv = [ (self.rbnEndJnt , 1 - val) , (self.rbnMidJnt , val) ] )
        mc.delete(self.animCrv)

        #-- Detail Controls
        self.rbnPosGrpDict = []
        self.posiDtlDict = []
        self.rbnDtlAimDict = []
        self.rbnDtlJntDict = []
        self.rbnDtlJntZroDict = []
        self.rbnDtlCtrlDict = []
        self.rbnDtlCtrlZroDict = []
        self.rbnDtlCtrlTwsDict = []
        self.rbnDtlTwsMdvDict = []
        self.rbnDtlTwsPmaDict = []
        self.rbnDtlSqshMdvDict = []
        self.rbnDtlSqshPmaDict = []

        for i in range(0,self.numJnts) :

            pV = ((0.5 / (self.numJnts)) * (i+1) * 2) - ((0.5 / ((self.numJnts) * 2)) * 2)

            #-- Posi Node
            self.rbnPosGrp = rt.createNode( 'transform' , '%s%sRbnDtlPos%sGrp' %( name , i+1 , side ))
            self.posi = rt.createNode( 'pointOnSurfaceInfo' , '%s%sRbnDtlPos%sPosi' %( name , i+1 , side ))
            
            self.posi.attr('turnOnPercentage').value = 1
            self.posi.attr('parameterU').value = 0.5

            self.rbnNrb.attr('worldSpace[0]') >> self.posi.attr('inputSurface')
            self.posi.attr('position') >> self.rbnPosGrp.attr('t')

            self.rbnPosGrpDict.append( self.rbnPosGrp )
            self.posiDtlDict.append( self.posi )
        
            #-- Aim Constraint Node
            self.rbnAim = rt.createNode( 'aimConstraint' , '%s%sRbnDtl%saimConstraint' %( name , i+1 , side ))

            self.rbnAim.attr('ax').value = self.aimVec[0]
            self.rbnAim.attr('ay').value = self.aimVec[1]
            self.rbnAim.attr('az').value = self.aimVec[2]
            self.rbnAim.attr('ux').value = self.upVec[0]
            self.rbnAim.attr('uy').value = self.upVec[1]
            self.rbnAim.attr('uz').value = self.upVec[2]

            self.posi.attr('n') >> self.rbnAim.attr('wu')
            self.posi.attr('tv') >> self.rbnAim.attr('tg[0].tt')
            self.rbnAim.attr('cr') >> self.rbnPosGrp.attr('r')
        
            self.rbnDtlAimDict.append( self.rbnAim )
            mc.parent( self.rbnAim , self.rbnPosGrp )
            
            mc.select( cl = True )
        
            #-- Create Joint
            self.rbnJnt = rt.createJnt( '%s%sRbnDtl%sJnt' %( name , i+1 , side ))
            self.rbnJntZro = rt.addGrp( self.rbnJnt )
            
            self.rbnDtlJntDict.append( self.rbnJnt )
            self.rbnDtlJntZroDict.append( self.rbnJntZro )

            #-- Create Controls
            self.rbnDtlCtrl = rt.createCtrl( '%s%sRbnDtl%sCtrl' %( name , i+1 , side ) , 'circle' , 'cyan' )
            self.rbnDtlCtrlZro = rt.addGrp( self.rbnDtlCtrl )
            self.rbnDtlCtrlTws = rt.addGrp( self.rbnDtlCtrl , 'Twist' )

            self.rbnDtlCtrl.addAttr( 'squash' )
        
            self.rbnDtlCtrlDict.append( self.rbnDtlCtrl )
            self.rbnDtlCtrlZroDict.append( self.rbnDtlCtrlZro )
            self.rbnDtlCtrlTwsDict.append( self.rbnDtlCtrlTws )

            #-- Adjust Shape Controls
            self.rbnDtlCtrl.scaleShape( dist*0.2 )
            self.rbnDtlCtrl.rotateShape( self.rotate[0] , self.rotate[1] , self.rotate[2] )

            #-- Rig Process
            #-- Twist
            mc.parentConstraint( self.rbnPosGrp , self.rbnDtlCtrlZro , mo = False )
            mc.parentConstraint( self.rbnDtlCtrl , self.rbnJntZro , mo = False )

            self.rbnDtlTwsMdv = rt.createNode( 'multiplyDivide' , '%s%sRbnDtlTws%sMdv' %( name , i+1 , side ))
            self.rbnDtlTwsPma = rt.createNode( 'plusMinusAverage' , '%s%sRbnDtlTws%sPma' %( name , i+1 , side ))

            self.rbnDtlTwsMdvDict.append( self.rbnDtlTwsMdv )
            self.rbnDtlTwsPmaDict.append( self.rbnDtlTwsPma )

            self.rbnRootPma.attr('o1') >> self.rbnDtlTwsMdv.attr('i2x')
            self.rbnEndPma.attr('o1') >> self.rbnDtlTwsMdv.attr('i2y')

            self.rbnDtlTwsMdv.attr('ox') >> self.rbnDtlTwsPma.attr('i1[0]')
            self.rbnDtlTwsMdv.attr('oy') >> self.rbnDtlTwsPma.attr('i1[1]')

            self.rbnDtlTwsPma.attr('o1') >> self.rbnDtlCtrlTws.attr(self.twstAx)

            self.rbnCtrlShape.attr('detailControl') >> self.rbnDtlCtrlZro.attr('v')

            #-- Adjust Detail Controls Position
            self.posiDtlDict[i].attr('parameterV').value = pV
            self.rbnDtlTwsMdvDict[i].attr('i1x').value = 1-pV
            self.rbnDtlTwsMdvDict[i].attr('i1y').value = pV


        #-- Squash
        nrbSq = mc.duplicate(self.rbnNrb,n='{}RbnSquash{}Nrb'.format(name,side))[0]
        hndSq = self.nonLinearDeformer(name = '{}RbnSquash'.format(name) , side = side, obj = nrbSq , defType = 'squash' )
        dGrp = mc.createNode('transform',n='{}RbnSquashDel{}Grp'.format(name,side))
        hndSqGrp = mc.group(dGrp,n='{}RbnSquash{}Grp'.format(name,side))
        mc.delete(mc.parentConstraint(hndSq['handleDag'],hndSqGrp))
        mc.parent(hndSq['handleDag'],hndSqGrp)
        mc.delete(dGrp)
        mc.setAttr(hndSqGrp+'.r' , self.rotate[0] , self.rotate[1] , self.rotate[2])
        sqPosMdv = mc.createNode('multiplyDivide',n='{}RbnSquashPos{}Mdv'.format(name,side))
        mc.setAttr(sqPosMdv+'.input2X',0.5)
        mc.connectAttr(self.rbnCtrl.attr('squashPosition'),sqPosMdv+'.input1X')
        mc.connectAttr(sqPosMdv+'.outputX',hndSq['handleDag']+'.ty')

        BclSq = mc.createNode('blendColors',n='{}RbnAutoSqsh{}Bcl'.format(name,side))
        normPma = mc.createNode('plusMinusAverage',n='{}RbnSqshNorm{}Pma'.format(name,side))
        facMdv = mc.createNode('multiplyDivide',n='{}RbnSqshFactor{}Mdv'.format(name,side))
        ampMdv = mc.createNode('multiplyDivide',n='{}RbnSqshAmp{}Mdv'.format(name,side))
        mc.setAttr(normPma+'.operation',2)
        mc.setAttr(BclSq+'.color2R',1)
        mc.setAttr(BclSq+'.color2R',0)
        mc.connectAttr(BclSq+'.outputR',facMdv+'.input1X')
        mc.connectAttr(BclSq+'.outputG',facMdv+'.input2X')
        mc.connectAttr(self.rbnLenPma.attr('o1'),normPma+'.input1D[0]')
        mc.setAttr(normPma+'.input1D[1]',dist)
        mc.connectAttr(self.rbnCtrl.attr('autoSquash'),BclSq+'.blender')
        mc.connectAttr(normPma+'.output1D',BclSq+'.color1R')
        mc.setAttr(ampMdv+'.input2X',0.1)
        mc.connectAttr(self.rbnCtrl.attr('ampSquash'),ampMdv+'.input1X')
        mc.connectAttr(ampMdv+'.outputX',BclSq+'.color1G')
        mc.connectAttr(facMdv+'.outputX',hndSq['handleNode']+'.factor')
        SqPosGrp = mc.createNode('transform',n='{}RbnSquashPos{}Grp'.format(name,side))
        mc.parent(SqPosGrp,self.rbnStillGrp)
        for posi in range(numJnts):
            pV = ((0.5 / (numJnts)) * (posi+1) * 2) - ((0.5 / ((numJnts) * 2)) * 2)
            posSq = mc.createNode('pointOnSurfaceInfo',n='{}{}RbnSquash{}pos'.format(name,posi+1,side))
            posGrp = mc.createNode('transform',n='{}{}RbnSquashPos{}Grp'.format(name,posi+1,side))
            mc.parent(posGrp,SqPosGrp)
            mc.connectAttr(nrbSq+'.worldSpace[0]',posSq+'.inputSurface')
            mc.setAttr(posSq+'.parameterU',1)
            mc.setAttr(posSq+'.parameterV',pV)
            mc.connectAttr(posSq+'.position',posGrp+'.translate')

            mdvSqAbsPos = mc.createNode('multiplyDivide',n='{}{}RbnSquashAbsPowPos{}Mdv'.format(name,posi+1,side))
            mc.setAttr(mdvSqAbsPos+'.op',3)
            mc.setAttr(mdvSqAbsPos+'.i2x',2)
            mdvSqSqrPos = mc.createNode('multiplyDivide',n='{}{}RbnSquashAbsSqrPos{}Mdv'.format(name,posi+1,side))
            mc.setAttr(mdvSqSqrPos+'.op',3)
            mc.setAttr(mdvSqSqrPos+'.i2x',0.5)
            mc.connectAttr(posGrp+'.tx',mdvSqAbsPos+'.i1x')
            mc.connectAttr(mdvSqAbsPos+'.ox',mdvSqSqrPos+'.i1x')
            posTx = mc.getAttr(mdvSqSqrPos+'.ox')

            pmaSqPos = mc.createNode('plusMinusAverage',n='{}{}RbnSquashPos{}Pma'.format(name,posi+1,side))
            # posTx = mc.getAttr(posGrp+'.tx')
            mc.addAttr(pmaSqPos,ln='default',at='double',dv=-posTx,k=True)
            mc.connectAttr(pmaSqPos+'.default',pmaSqPos+'.input1D[0]')
            mc.connectAttr(mdvSqSqrPos+'.ox',pmaSqPos+'.i1[1]')
            # mc.connectAttr(posGrp+'.tx',pmaSqPos+'.input1D[1]')
            pmaSq = mc.createNode('plusMinusAverage',n='{}{}RbnSquash{}Pma'.format(name,posi+1,side))
            mdvPosSq = mc.createNode('multiplyDivide',n='{}{}RbnPosSquash{}Mdv'.format(name,posi+1,side))
            pmaTotalSq = mc.createNode('plusMinusAverage',n='{}{}RbnTotalSquash{}Pma'.format(name,posi+1,side))
            mc.setAttr(mdvPosSq+'.input2X',10)
            mc.connectAttr(self.rbnCtrl.attr('squash'),pmaSq+'.input1D[0]')
            mc.connectAttr(pmaSqPos+'.output1D',mdvPosSq+'.input1X')
            mc.connectAttr(mdvPosSq+'.outputX',pmaSq+'.input1D[1]')
            mc.connectAttr(pmaSq+'.output1D',pmaTotalSq+'.input1D[0]')
            mc.connectAttr('{}{}RbnDtl{}Ctrl.squash'.format(name,posi+1,side),pmaTotalSq+'.input1D[1]')
            mc.setAttr(pmaSq+'.input1D[2]',1)
            mc.connectAttr(pmaTotalSq+'.output1D','{}{}RbnDtl{}Jnt.{}'.format(name,posi+1,side,self.sqshAx[0]))
            mc.connectAttr(pmaTotalSq+'.output1D','{}{}RbnDtl{}Jnt.{}'.format(name,posi+1,side,self.sqshAx[1]))

    

        ##------sine------##
        #-- Sine Atribute
        self.rbnCtrl.addTitleAttr( 'Sinewave' )
        self.rbnCtrl.addAttr( 'autoSine' , 0 , 1 )
        self.rbnCtrl.addAttr( 'amplitude' )
        self.rbnCtrl.addAttr( 'offset' )
        self.rbnCtrl.addAttr( 'width' , 0 )
        self.rbnSineWigthMdv = rt.createNode( 'multiplyDivide' , '%sRbnSineWidth%sMdv' %( name , side ))
        self.rbnCtrl.attr('width') >> self.rbnSineWigthMdv.attr('i1x')
        self.rbnCtrl.attr( 'width').value = 1
        self.rbnCtrl.addAttr( 'position' )
        self.rbnCtrl.addAttr( 'twist' )
        self.rbnCtrl.addAttr( 'sineLength' )
        self.rbnCtrl.attr( 'sineLength').value = 2
        self.rbnCtrl.addAttr( 'dropoff' , 0 , 1)
        self.rbnCtrl.attr( 'dropoff').value = 1

        nrbSn = mc.duplicate(self.rbnNrb,n='{}RbnSine{}Nrb'.format(name,side))[0]
        hndSn = self.nonLinearDeformer(name = '{}RbnSine'.format(name) , side = side , obj = nrbSn , defType = 'sine' )
        dGrp = mc.createNode('transform',n='{}RbnSineDel{}Grp'.format(name,side))
        hndSnGrp = mc.group(dGrp,n='{}RbnSine{}Grp'.format(name,side))
        mc.delete(mc.parentConstraint(hndSn['handleDag'],hndSnGrp))
        mc.parent(hndSn['handleDag'],hndSnGrp)
        mc.delete(dGrp)
        mc.setAttr(hndSnGrp+'.r' , self.rotate[0] , self.rotate[1] , self.rotate[2])
        scaleSn = mc.getAttr(hndSq['handleDag']+'.sx')
        self.rbnSineWigthMdv.attr('i2x').value = scaleSn
        mc.setAttr(hndSn['handleNode']+'.dropoff',1)
        autoAPma = mc.createNode('plusMinusAverage',n='{}RbnSineAutoAmp{}Pma'.format(name,side))
        autoOPma = mc.createNode('plusMinusAverage',n='{}RbnSineAutoOffset{}Pma'.format(name,side))
        autoCnd = mc.createNode('condition',n='{}RbnSineAutoTime{}Cnd'.format(name,side))
        autoMdv = mc.createNode('multiplyDivide',n='{}RbnSineAutoTime{}Mdv'.format(name,side))
        mc.connectAttr(self.rbnCtrl.attr('autoSine'),autoCnd+'.firstTerm')
        mc.setAttr(autoCnd+'.secondTerm',1)
        mc.setAttr(autoCnd+'.colorIfFalseR',0)
        mc.setAttr(autoMdv+'.input2X',0.1)

        mc.connectAttr('time1.outTime',autoMdv+'.input1X')
        mc.connectAttr(autoMdv+'.outputX',autoCnd+'.colorIfTrueR')
        mc.connectAttr(self.rbnCtrl.attr('autoSine'),autoAPma+'.input1D[0]')
        mc.connectAttr(self.rbnCtrl.attr('amplitude'),autoAPma+'.input1D[1]')
        mc.connectAttr(autoAPma+'.output1D',hndSn['handleNode']+'.amplitude')
        mc.connectAttr(self.rbnCtrl.attr('offset'),autoOPma+'.input1D[0]')
        mc.connectAttr(autoCnd+'.outColorR',autoOPma+'.input1D[1]')
        mc.connectAttr(autoOPma+'.output1D',hndSn['handleNode']+'.offset')

        mc.connectAttr(self.rbnSineWigthMdv.attr('ox'),hndSn['handleDag']+'.sy')
        mc.connectAttr(self.rbnCtrl.attr('position'),hndSn['handleDag']+'.ty')
        mc.connectAttr(self.rbnCtrl.attr('sineLength'),hndSn['handleNode']+'.wavelength')
        mc.connectAttr(self.rbnCtrl.attr('dropoff'),hndSn['handleNode']+'.dropoff')
        mc.connectAttr(self.rbnCtrl.attr('twist'),hndSn['handleDag']+'.ry')

        SnPosGrp = mc.createNode('transform',n='{}RbnSinePos{}Grp'.format(name,side))
        mc.parent(SnPosGrp,self.rbnStillGrp)
        for posi in range(numJnts):
            pV = ((0.5 / (numJnts)) * (posi+1) * 2) - ((0.5 / ((numJnts) * 2)) * 2)
            posSn = mc.createNode('pointOnSurfaceInfo',n='{}{}RbnSine{}pos'.format(name,posi+1,side))
            posGrp = mc.createNode('transform',n='{}{}RbnSinePos{}Grp'.format(name,posi+1,side))
            mc.parent(posGrp,SnPosGrp)
            mc.connectAttr(nrbSn+'.worldSpace[0]',posSn+'.inputSurface')
            mc.setAttr(posSn+'.parameterU',0.5)
            mc.setAttr(posSn+'.parameterV',pV)
            mc.connectAttr(posSn+'.position',posGrp+'.translate')
            mc.connectAttr('{}.{}'.format(posGrp , self.sineT[0]),'{}{}RbnDtlCtrlTwist{}Grp.{}'.format(name,posi+1,side,self.sineT[0]))
            mc.connectAttr('{}.{}'.format(posGrp , self.sineT[1]),'{}{}RbnDtlCtrlTwist{}Grp.{}'.format(name,posi+1,side,self.sineT[1]))



        #-- Adjust Hierarchy
        mc.parent( nrbSn , nrbSq , hndSnGrp , hndSqGrp , self.rbnStillGrp)
        mc.parent( self.rbnNrb , self.rbnPosGrpDict , self.rbnStillGrp )
        mc.parent( self.rbnDtlJntZroDict , self.rbnSkinGrp )
        mc.parent( self.rbnDtlCtrlZroDict , self.rbnDetailGrp )
        mc.parent( self.rbnDetailGrp , self.rbnCtrlGrp )

        #-- Cleanup
        for grp in ( self.rbnStillGrp , self.rbnDetailGrp ) :
            grp.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' )

        for dicts in ( self.rbnDtlCtrlZroDict , self.rbnDtlJntZroDict , self.rbnDtlCtrlTwsDict , self.rbnDtlJntDict ) :
            for each in dicts :
                each.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        for dtlCtrl in self.rbnDtlCtrlDict :
            dtlCtrl.lockHideAttrs( 'sx' , 'sy' , 'sz' , 'v' )

    mc.select( cl = True )


class RibbonLow( Ribbon ):

    def __init__( self , name   = '' ,
                         axis     = 'y+' ,
                         side   = 'L' ,
                         dist   = 1 
                 ):
        
        super( RibbonLow , self ).__init__( name , axis , side , dist )

        ##-- Info
        if side == '' :
            side = '_'
        else :
            side = '_%s_' %side
        
        #-- Create Joint
        self.rbnJnt = rt.createJnt( '%sRbn%sJnt' %( name , side ))
        self.rbnJntZro = rt.addGrp( self.rbnJnt )
        self.rbnJntOfst = rt.addGrp( self.rbnJnt , 'Ofst' )

        mc.parentConstraint( self.rbnCtrl , self.rbnJntZro )

        #-- Twist Distribute
        self.rbnTwsMdv = rt.createNode( 'multiplyDivide' , '%sRbnTws%sMdv' %( name , side ))
        self.rbnTwsPma = rt.createNode( 'plusMinusAverage' , '%sRbnTws%sPma' %( name , side ))
        
        self.rbnTwsMdv.attr('i1x').value = 0.5
        self.rbnTwsMdv.attr('i1y').value = 0.5
        
        self.rbnRootPma.attr('o1') >> self.rbnTwsMdv.attr('i2y')
        self.rbnEndPma.attr('o1') >> self.rbnTwsMdv.attr('i2x')
        self.rbnTwsMdv.attr('ox') >> self.rbnTwsPma.attr('i1[0]')
        self.rbnTwsMdv.attr('oy') >> self.rbnTwsPma.attr('i1[1]')
        self.rbnTwsPma.attr('output1D') >> self.rbnJntOfst.attr( self.twstAx )

        #-- Squash
        self.rbnSqshMdv = rt.createNode( 'multiplyDivide' , '%sRbnSqsh%sMdv' %( name , side ))
        self.rbnSqshBcl = rt.createNode( 'blendColors' , '%sRbnSqsh%sBcl' %( name , side ))
        self.rbnSqshPma = rt.createNode( 'plusMinusAverage' , '%sRbnSqsh%sPma' %( name , side ))

        self.rbnSqshMdv.attr('i2x').value = 0.5
        self.rbnCtrl.attr('squash') >> self.rbnSqshMdv.attr('i1x')
        self.rbnCtrl.attr('autoSquash') >> self.rbnSqshBcl.attr('b')
        self.rbnSqshDivMdv.attr('ox') >> self.rbnSqshBcl.attr('c1r')

        self.rbnSqshBcl.attr('c2r').value = 1
        
        self.rbnSqshMdv.attr('ox') >> self.rbnSqshPma.attr('i1[0]')
        self.rbnSqshBcl.attr('opr') >> self.rbnSqshPma.attr('i1[1]')
        self.rbnSqshPma.attr('o1') >> self.rbnJnt.attr( self.sqshAx[0] )
        self.rbnSqshPma.attr('o1') >> self.rbnJnt.attr( self.sqshAx[1] ) 
        

        #-- Adjust Hierarchy
        mc.parent( self.rbnJntZro , self.rbnSkinGrp )