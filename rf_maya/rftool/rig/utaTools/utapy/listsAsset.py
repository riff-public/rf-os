''' Unlock Scale Some Asset'''
from collections import OrderedDict

def listsLockScale(projectName = '' , *args):
    controlObj = 'AllMover_Ctrl'
    listsChar = []
    if projectName == 'SevenChickMovie':
        listsProp = ['woodenBucket',
                    'scissorTiny',
                    'mop',
                    'sackBig',
                    'sackBigHi',
                    'RopeLasso',
                    'scrissorArthurNew',
                    'FluteGold',
                    'VineStraight',
                    'bigLeafUmbrella',
                    'bustToad',
                    'barberChair',
                    'gong',
                    'comb',
                    'paraglider',
                    'clawSword',
                    'clawHalberd',
                    'clawShield',
                    'clawDagger',
                    'bow',
                    'arrows',
                    'quiver',
                    'jacobusStaff',
                    'cronusStaff',
                    'swordToad',
                    'eggCradle',
                    'raft',
                    'boatHippoNoodle',
                    'eggShellA',
                    'coinSilver',
                    'toothbrush',
                    'towel',
                    'potSoupProcupine',
                    'dishSteel',
                    'hammerA',
                    'stool',
                    'chesspiecesWhite',
                    'chessboard',
                    'canisWeapon',
                    'clawshred',
                    'chinesePan',
                    'JacobusTempleExt',
                    'fanA',
                    'fanB',
                    'fanC',
                    'Umbrella',
                    'musicErhu',
                    'musicHuluxiao',
                    'musicSheng',
                    'nusicGuzheng',
                    'bambooA',
                    'bambooBroke',
                    'bambooPole',
                    'MusicDrumstick',
                    'swordKing',
                    'eggTinyBroke',
                    'eggMightyBroke',
                    'eggHuffyBroke',
                    'eggLazyBroke',
                    'eggDizzyBroke',
                    'eggWaryBroke',
                    'eggPercyBroke',
                    'statueChick',
                    'statuePig',
                    'terrainZoneA',
                    'terrainZoneB',
                    'terrainZoneC',
                    'terrainZoneD',
                    'monasteryDock',
                    'monasterySmall',
                    'monasterySmallHallInt',
                    'monasterySmallUprInt',
                    'mstCoridorA',
                    'mstCoridorB',
                    'bambooGroupA',
                    'BambooBg',
                    'adeptCabinA',
                    'adeptCabinB',
                    'mstMainTerrain',
                    'monasterySmallUprTerrain',
                    'mstBackTerrain',
                    'mstSmallLwrTerrain',
                    'mstBarberTerrain',
                    'mstBarberShopArthurExt',
                    'mirrorTableA',
                    'cavinPond',
                    'chairFootRestC',
                    'bombSmoke',
                    'terrainZoneD_right',
                    'combBig',
                    'airballoon',
                    'scrissorArthurPouch',
                    'monasterySmallUprOut',
                    'eggshellB',
                    'combA',
                    'combB',
                    'combC',
                    'bambooC',
                    'scrollD']
    elif projectName == 'Hanuman':
        listsProp = ['rockB',
                    'rockC',
                    'rockE',
                    'rockJ',
                    'rockG',
                    'rockH',
                    'rockI',
                    'stoneA',
                    'stoneB',
                    'mvCaraMaskStg',
                    'mvCaraMaskStgBacklight',
                    'cystalA',

                    'rvShipsExB',
                    'ravanShipExA',
                    'ravanShipExB',
                    'lucksamanChariot', 
                    'ravanMainShip',
                    'raamShipT'
                    ]

    elif projectName == 'BubbleBee':
        listsProp = ['microphone']

    return controlObj, listsChar, listsProp

def listsCharScale(*args):
    listsCharName = OrderedDict()
    controlObj = 'AllMover_Ctrl'
    attrScale = 'scaleY'
    ## Female Scale
    listsCharName ['rhinoFemale']       = 0.9
    listsCharName ['elephantFemale']    = 0.9
    listsCharName ['hippoFemale']       = 1.2
    listsCharName ['craneFemale']       = 0.9
    listsCharName ['deerFemale']        = 0.9
    listsCharName ['turtleFemale']      = 1
    listsCharName ['duckFemale']        = 0.8
    listsCharName ['shibaFemale']       = 1
    listsCharName ['otterFemale']       = 0.9
    listsCharName ['catFemale']         = 0.9
    listsCharName ['gibbonFemale']      = 0.9
    listsCharName ['rabbitFemale']      = 0.9
    listsCharName ['guineaPigFemale']   = 0.9
    listsCharName ['owlFemale']         = 0.9
    ## Girl Scale
    listsCharName ['elephantGirl']      = 1
    listsCharName ['rhinoGirl']         = 1.1
    listsCharName ['duckGirl']          = 0.9
    listsCharName ['otterGirl']         = 1
    listsCharName ['catGirl']           = 1
    listsCharName ['gibbonGirl']        = 1.1
    listsCharName ['boarGirl']          = 0.9
    listsCharName ['rabbitGirl']        = 0.9
    listsCharName ['shibaGirl']         = 1

    return listsCharName , controlObj, attrScale

def listsReamCharScale(*args):
    listsCharName = OrderedDict()
    controlObj = 'AllMover_Ctrl'
    attrScale = 'scaleY'
    ## Female Scale
    listsCharName ['ken']           = 0.1
    listsCharName ['buffalo']       = 1
    listsCharName ['crocodileMom']  = 1
    listsCharName ['crocodileSon']  = 1

    return listsCharName , controlObj, attrScale

