import maya.cmds as mc
reload(mc)

addBshFacial(
hdRigList = ["HeadHdRig_Geo", "CaruncleHdRig_Geo", "EyebrowHdRig_Geo", "EyelashHdRig_Geo", 
            "EyeballHdRig_L_Geo", "CorneaHdRig_L_Geo", "EyeballHdRig_R_Geo", "CorneaHdRig_R_Geo", 
            "TongueHdRig_Geo", "GumUpHdRig_Geo","GumDnHdRig_Geo", "TeethUpHdRig_Geo", 
            "TeethDnHdRig_Geo"],
fclRigList = ["HeadFclRig_Geo", "CaruncleFclRig_Geo", "EyebrowFclRig_Geo", "EyelashFclRig_Geo", 
            "EyeballFclRig_L_Geo", "CorneaFclRig_L_Geo", "EyeballFclRig_R_Geo", "CorneaFclRig_R_Geo", 
            "TongueFclRig_Geo", "GumUpFclRig_Geo","GumDnFclRig_Geo", "TeethUpFclRig_Geo", 
            "TeethDnFclRig_Geo"])

def addBshFacial (hdRigList = [], fclRigList = []):
    for hdRig in hdRigList:
        for fclRig in fclRigList:
            for i in range(len(hdRigList)):
                currHdRig = mc.ls('*:{hdRig}'.format(hdRig = hdRig))
                currFclRig = mc.ls('{fclRig}'.format(fclRig = fclRig))
                print currHdRig
                print currFclRig
    
                if not currHdRig or currFclRig:
                    continue
                mc.select(currHdRig)
                mc.select(currFclRig, add=True)
                lrr.doAddBlendShape()


