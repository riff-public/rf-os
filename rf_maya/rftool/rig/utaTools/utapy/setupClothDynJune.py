import pymel.core as pm;
import maya.mel as mel;
import maya.cmds as mc

class Class( object ):

    def browseRef( self,*args ):
        # print '1'
        # try : 
        #     print '2'
        pathFile = pm.textField( 'inputPath' , q = True , text = True );
        print pathFile, 'k3en'

        if pathFile:
            print 'A'
            browseRef = [pathFile]
        else:
            print 'B'

            startPath = 'P:/SevenChickMovie/asset/publ/char/dizzy/hero'

            browseRef = mc.fileDialog2( fm = 4, ff = '*',dir = startPath, okc = 'reference' );

        pm.textField( 'inputPath', e = True, text = browseRef[0] );
        mc.file( browseRef[0], r = True , ns = 'Sim')


    def __init__( self ):
        self.ui       = 'ClothSetupUI';
        self.width    = 500.00;
        self.title    = 'Cloth Setup Tools';
        self.version  = 1.0;

    def clothSetupUI( self ):
        if pm.window ( 'ClothSetupUI' , exists = True ) :
            pm.deleteUI ( 'ClothSetupUI' ) ;

        w = self.width ;

        window = pm.window ( 'ClothSetupUI' , 
            title     = '{title} {version}'.format( title = self.title, version = self.version ),
            mnb       = True ,
            mxb       = False,
            sizeable  = True ,
            rtf       = True ,
            );

        pm.window( window , e = True , w = w , h = 10.00 );
        with window:
    
            browseRefLayout = pm.rowColumnLayout( h = 25,w = w, nc = 4, columnAlign = (1, 'left'), columnWidth = [ ( 1, 95 ), ( 2, 320 ), ( 3, 5 ), ( 4, 75 )]);
            with browseRefLayout:
                pm.text( label = ' Reference Path :' );
                pm.textField( 'inputPath', en = True, text = '' );
                pm.separator( vis = False );
                pm.button( label = 'Ref', h = 5, c = self.browseRef );

        window.show();
