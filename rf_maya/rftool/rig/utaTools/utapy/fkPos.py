import maya.cmds as mc
import pymel.core as pm


# def fkPos(*args):
# 		MtCtrl = []    
# 		for a in range(NoSK):
#             ZGrp = mc.createNode('transform',n=SK[a]+'CtrlZro_Grp')
#             CirCrv = mc.curve(d = 1,n = 'IkPath_Ctrl',p=[(-99.056548,0,0),(-88.41312,0,0),(-87.324554,0,-13.830848),(-84.08589,0,-27.321117),(-78.776635,0,-40.138682),(-71.527721,0,-51.967883),(-62.517537,0,-62.517537),(-51.967883,0,-71.527721),(-40.138682,0,-78.776635),(-27.321117,0,-84.08589),(-13.830848,0,-87.324554),(0,0,-88.41312),(0,0,-99.056548),(0,0,-88.41312),(13.830848,0,-87.324554),(27.321117,0,-84.08589),(40.138682,0,-78.776635),(51.967883,0,-71.527721),(62.517537,0,-62.517537),(71.527721,0,-51.967883),(78.776635,0,-40.138682),(84.08589,0,-27.321117),(87.324554,0,-13.830848),(88.41312,0,0),(99.056548,0,0),(88.41312,0,0),(87.324554,0,13.830848),(84.08589,0,27.321117),(78.776635,0,40.138682),(71.527721,0,51.967883),(62.517537,0,62.517537),(51.967883,0,71.527721),(40.138682,0,78.776635),(27.321117,0,84.08589),(13.830848,0,87.324554),(0,0,88.41312),(0,0,99.056548),(0,0,88.41312),(-13.830848,0,87.324554),(-27.321117,0,84.08589),(-40.138682,0,78.776635),(-51.967883,0,71.527721),(-62.517537,0,62.517537),(-71.527721,0,51.967883),(-78.776635,0,40.138682),(-84.08589,0,27.321117),(-87.324554,0,13.830848),(-88.41312,0,0)])
#             CirCrv = mc.rename(CirCrv,SK[a]+'_Ctrl')
#             CirShape = mc.listRelatives( CirCrv , shapes = True )[0]
#             mc.setAttr('%s.overrideEnabled'%CirShape,1)
#             mc.setAttr('%s.overrideColor'%CirShape,18)
#             Crv = CirCrv
            
#             mc.parent(Crv,ZGrp)
            
#             mc.delete(mc.parentConstraint(JIK[a],ZGrp))
#             mc.parentConstraint(Crv,JIK[a])

#             MtCtrl.append(ZGrp)

def fkPos(elem = '', grpCon = True, listsCouth = '', *args):

    lists = mc.ls('TailFk*_Jnt')
    if pm.objExists == True:
        listsCouth = len(lists)

	print listsCouth
    ix= 0
    CirCrvAll = []
  

    # Create Group
    if grpCon == True:
    	for x in range(0, listsCouth):
        # Create Control
		    CirCrv = mc.curve(d = 1,n = '%s%s_Ctrl' % (elem, x+1) ,p=[(-99.056548,0,0),(-88.41312,0,0),(-87.324554,0,-13.830848),(-84.08589,0,-27.321117),(-78.776635,0,-40.138682),(-71.527721,0,-51.967883),(-62.517537,0,-62.517537),(-51.967883,0,-71.527721),(-40.138682,0,-78.776635),(-27.321117,0,-84.08589),(-13.830848,0,-87.324554),(0,0,-88.41312),(0,0,-99.056548),(0,0,-88.41312),(13.830848,0,-87.324554),(27.321117,0,-84.08589),(40.138682,0,-78.776635),(51.967883,0,-71.527721),(62.517537,0,-62.517537),(71.527721,0,-51.967883),(78.776635,0,-40.138682),(84.08589,0,-27.321117),(87.324554,0,-13.830848),(88.41312,0,0),(99.056548,0,0),(88.41312,0,0),(87.324554,0,13.830848),(84.08589,0,27.321117),(78.776635,0,40.138682),(71.527721,0,51.967883),(62.517537,0,62.517537),(51.967883,0,71.527721),(40.138682,0,78.776635),(27.321117,0,84.08589),(13.830848,0,87.324554),(0,0,88.41312),(0,0,99.056548),(0,0,88.41312),(-13.830848,0,87.324554),(-27.321117,0,84.08589),(-40.138682,0,78.776635),(-51.967883,0,71.527721),(-62.517537,0,62.517537),(-71.527721,0,51.967883),(-78.776635,0,40.138682),(-84.08589,0,27.321117),(-87.324554,0,13.830848),(-88.41312,0,0)])
		    # mc.delete(mc.parentConstraint(('TailFk%s_Jnt'% listsCouth ,CirCrv ), mo=False))
		    # CirCrvAll.append(CirCrv) 


		    groupCons = mc.group(em=True, n = ('%s%sJntZro_Grp' % (elem, (x+1))))
		    mc.parent(CirCrv, groupCons)
		    # mc.delete(mc.parentConstraint(CirCrv, groupCons,mo=False))
		    # mc.parent(CirCrv , groupCons)
		    mc.delete(mc.parentConstraint('TailFk%s_Jnt' % listsCouth, groupCons,mo=False))
        # listsCouth-=1  
        ix+=1
        # for xx in range(0, listsCouth):
        # 	mc.delete(mc.parentConstraint('TailFk%s_Jnt' % listsCouth, groupCons,mo=False))
	# ix+=1


        # # Parent Joint to Joint        
        # elif grpCon == False:
        #     if elem+str(ix)+'_Ctrl' != elem+"0"+'_Ctrl':
        #         mc.parent(elem+str(ix+1)+'_Ctrl',elem+str(ix)+'_Ctrl')


    # #Parent Joint to Group
    # if elem+str(listsCouth)+'JntZro_Grp' != elem+"25"+'JntZro_Grp':
    #     mc.parent(elem+str(listsCouth-ix)+'JntZro_Grp',elem+str(listsCouth)+'_Ctrl')


    mc.select(clear=True)
    return CirCrvAll