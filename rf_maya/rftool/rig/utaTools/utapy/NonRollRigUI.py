import maya.cmds as mc
# import pkmel.aimRig as aimRig
# reload( aimRig )
from utaTools.utapy import utaSkirtRig
reload(utaSkirtRig)

def openUI(*args):

	Sel = mc.ls(sl=1)
	windowID = 'NonRollRig_UI'
	if mc.window(windowID, exists=True):
		mc.deleteUI(windowID)

	winWidth = 300


	window = mc.window( windowID  , title="NonRollRig_UI ", iconName='ANIM', width = winWidth, height = 450 , s=False )

	mc.columnLayout( 'nonRollRigUILayout' , adjustableColumn=False )

	mc.rowLayout(numberOfColumns=2)
	mc.textFieldGrp( 'parentTxt' ,ed=True, label='Parent',columnWidth2=[70,180] , cal=[1,'left']  , pht = 'Ex : pelvisNnRlProxySkin_jnt' )
	mc.button(label = 'Add' , c=addParent,align='center', w = 0.2*winWidth, h = 20)

	mc.setParent('..')

	mc.rowLayout(numberOfColumns=2)
	mc.textFieldGrp( 'animGrpTxt' ,ed=True, label='AnimGrp',columnWidth2=[70,180] , cal=[1,'left']  , pht = 'Ex : legNnrlRig_grp' )
	mc.button(label = 'Add' , c=addAnimGrp,align='center', w = 0.2*winWidth, h = 20)

	mc.setParent('..')

	mc.rowLayout(numberOfColumns=2)
	mc.textFieldGrp( 'rootJntTxt' ,ed=True, label='RootJnt',columnWidth2=[70,180] , cal=[1,'left']  , pht = 'Ex : upLegLFT_jnt' )
	mc.button(label = 'Add' , c=addRootJnt,align='center', w = 0.2*winWidth, h = 20)

	mc.setParent('..')
	
	mc.rowLayout(numberOfColumns=2)
	mc.textFieldGrp( 'endJntTxt' ,ed=True, label='EndJnt',columnWidth2=[70,180] , cal=[1,'left']  , pht = 'Ex : lowLegLFT_jnt' )
	mc.button(label = 'Add' , c=addEndJnt,align='center', w = 0.2*winWidth, h = 20)

	mc.setParent('..')

	mc.rowLayout(numberOfColumns=2)
	mc.textFieldGrp( 'parentRootTxt' ,ed=True, label='ParentRoot',columnWidth2=[70,180] , cal=[1,'left']  , pht = 'Ex : upLegNnRlProxySkinLFT_jnt' )
	mc.button(label = 'Add' , c=addParentRoot,align='center', w = 0.2*winWidth, h = 20)

	mc.setParent('..')

	mc.rowLayout(numberOfColumns=2)
	mc.textFieldGrp( 'parentEndTxt' ,ed=True, label='ParentEnd',columnWidth2=[70,180] , cal=[1,'left']  , pht = 'Ex : lowLegNnRlProxySkinLFT_jnt' )
	mc.button(label = 'Add' , c=addParentEnd,align='center', w = 0.2*winWidth, h = 20)

	mc.setParent('..')

	mc.rowLayout(numberOfColumns=2)
	mc.textFieldGrp( 'nameTxt' ,ed=True, label='Name',columnWidth2=[70,180] , cal=[1,'left']  , pht = 'Ex : skirt' )
	mc.button(label = 'Add' , c=addName,align='center', w = 0.2*winWidth, h = 20)

	mc.setParent('..')

	mc.rowLayout(numberOfColumns=2)
	mc.textFieldGrp( 'sideTxt' ,ed=True, label='Side',columnWidth2=[70,180] , cal=[1,'left']  , pht = 'Ex : LFT / RGT' )
	mc.button(label = 'Add' , c=addSide,align='center', w = 0.2*winWidth, h = 20)

	mc.setParent('..')

	mc.rowLayout(numberOfColumns=2)
	mc.textFieldGrp( 'aimAxTxt' ,ed=True, label='AimAx',columnWidth2=[70,180] , cal=[1,'left']  , pht = 'Ex : y+' )
	mc.button(label = 'Add' , c=addAimAx,align='center', w = 0.2*winWidth, h = 20)

	mc.setParent('..')

	mc.rowLayout(numberOfColumns=2)
	mc.textFieldGrp( 'upAxTxt' ,ed=True, label='UpAx',columnWidth2=[70,180] , cal=[1,'left']  , pht = 'Ex : x-' )
	mc.button(label = 'Add' , c=addUpAx,align='center', w = 0.2*winWidth, h = 20)

	mc.setParent('..')

	mc.button(label = 'Run' , c=run , align = 'center' , w= 321 , h=40)
	mc.showWindow()

def addParent(*args):
	selSet = mc.ls(sl=True)[0]
	text = str(selSet)
	mc.textFieldGrp( 'parentTxt' ,edit=True , tx='{}'.format(text))    

def addAnimGrp(*args):
	selSet = mc.ls(sl=True)[0]
	text = str(selSet)
	mc.textFieldGrp( 'animGrpTxt' ,edit=True , tx='{}'.format(text))    

def addRootJnt(*args):
	selSet = mc.ls(sl=True)[0]
	text = str(selSet)
	mc.textFieldGrp( 'rootJntTxt' ,edit=True , tx='{}'.format(text))   

def addEndJnt(*args):
	selSet = mc.ls(sl=True)[0]
	text = str(selSet)
	mc.textFieldGrp( 'endJntTxt' ,edit=True , tx='{}'.format(text))  

def addParentRoot(*args):
	selSet = mc.ls(sl=True)[0]
	text = str(selSet)
	mc.textFieldGrp( 'parentRootTxt' ,edit=True , tx='{}'.format(text))  

def addParentEnd(*args):
	selSet = mc.ls(sl=True)[0]
	text = str(selSet)
	mc.textFieldGrp( 'parentEndTxt' ,edit=True , tx='{}'.format(text))  

def addName(*args):
	selSet = mc.ls(sl=True)[0]
	text = str(selSet)
	mc.textFieldGrp( 'nameTxt' ,edit=True , tx='{}'.format(text))  

def addSide(*args):
	selSet = mc.ls(sl=True)[0]
	text = str(selSet)
	mc.textFieldGrp( 'sideTxt' ,edit=True , tx='{}'.format(text)) 

def addAimAx(*args):
	selSet = mc.ls(sl=True)[0]
	text = str(selSet)
	mc.textFieldGrp( 'aimAxTxt' ,edit=True , tx='{}'.format(text))  

def addUpAx(*args):
	selSet = mc.ls(sl=True)[0]
	text = str(selSet)
	mc.textFieldGrp( 'upAxTxt' ,edit=True , tx='{}'.format(text))  


def run(*args):

	parent = mc.textFieldGrp( 'parentTxt' ,q=True , tx=True)
	animGrp = mc.textFieldGrp( 'animGrpTxt' ,q=True , tx=True)
	rootJnt = mc.textFieldGrp( 'rootJntTxt' ,q=True , tx=True)
	endJnt = mc.textFieldGrp( 'endJntTxt' ,q=True , tx=True)
	parentRoot = mc.textFieldGrp( 'parentRootTxt' ,q=True , tx=True)
	parentEnd = mc.textFieldGrp( 'parentEndTxt' ,q=True , tx=True)
	name = mc.textFieldGrp( 'nameTxt' ,q=True , tx=True)
	side = mc.textFieldGrp( 'sideTxt' ,q=True , tx=True)
	aimAx = mc.textFieldGrp( 'aimAxTxt' ,q=True , tx=True)
	upAx = mc.textFieldGrp( 'upAxTxt' ,q=True , tx=True)

	a = utaSkirtRig.NonRollRig(
					   parent = parent ,
					   animGrp = animGrp,
					   rootJnt = rootJnt ,
					   endJnt = endJnt ,
					   parentRoot = parentRoot,
					   parentEnd = parentEnd ,
					   name = name ,
					   side = side,
					   aimAx = aimAx ,
					   upAx = upAx
				   )
	print '#=== DONE ===#'
