mouthRig					corrective			proxy	

LipUpRollOutMthRig_Bsh						upLipsCurlOut	
LipUpRollInMthRig_Bsh						upLipsCurlIn
LipUpPuffMthRig_Bsh
LipLowRollOutMthRig_Bsh						loLipsCurlOut
LipLowRollInMthRig_Bsh						loLipsCurlIn
LipLowPuffMthRig_Bsh
UMthRig_Bsh					mouthU
LipUpThickMthRig_Bsh
LipLowThickMthRig_Bsh
JawOpenMthRig_Bsh			JawOpenMth


CnrOutMthRig_L_Bsh							cornerOut
CnrInMthRig_L_Bsh							cornerIn
CnrInUpMthRig_L_Bsh							cornerInnerUp
CnrUpMthRig_L_Bsh							cornerUp
CnrOutUpMthRig_L_Bsh						cornerOuterUp
CnrInDnMthRig_L_Bsh							cornerInnerDn
CnrDnMthRig_L_Bsh							cornerDn
CnrOutDnMthRig_L_Bsh						cornerOuterDn
CnrDnOpenMthRig_L_Bsh		CnrDnOpen
CnrOpenMthRig_L_Bsh			JawOpen
CnrUpOpenMthRig_L_Bsh		CnrUpOpen
CnrInDnOpenMthRig_L_Bsh		CnrInDnOpen
CnrInOpenMthRig_L_Bsh		CnrInOpen
CnrInUpOpenMthRig_L_Bsh		CnrInUpOpen
CnrOutDnOpenMthRig_L_Bsh	CnrOutDnOpen
CnrOutOpenMthRig_L_Bsh		CnrOutOpen
CnrOutUpOpenMthRig_L_Bsh	CnrOutUpOpen
CheekMthRig_L_Bsh							puffUpperUp
PuffMthRig_L_Bsh							puffLowerOut


import maya.cmds as mc
reload(mc)
bsh1 = ("upLipsCurlOut", 	"HeadMthRig_Geo_LipUpRollOutMthRig_Bsh" )
bsh2 = ("upLipsCurlIn", 	"HeadMthRig_Geo_LipUpRollInMthRig_Bsh" )
bsh3 = ("loLipsCurlOut", 	"HeadMthRig_Geo_LipLowRollOutMthRig_Bsh" )
bsh4 = ("loLipsCurlIn", 	"HeadMthRig_Geo_LipLowRollInMthRig_Bsh" )
bsh5 = ("mouthU", 			"HeadMthRig_Geo_UMthRig_Bsh" )
bsh6 = ("JawOpenMth", 		"HeadMthRig_Geo_JawOpenMthRig_Bsh" )
bsh7 = ("cornerOut", 		"HeadMthRig_Geo_CnrOutMthRig_L_Bsh" )
bsh8 = ("cornerIn", 		"HeadMthRig_Geo_CnrInMthRig_L_Bsh" )
bsh9 = ("cornerInnerUp", 	"HeadMthRig_Geo_CnrInUpMthRig_L_Bsh" )
bsh10 = ("cornerUp", 		"HeadMthRig_Geo_CnrUpMthRig_L_Bsh" )
bsh11 = ("cornerOuterUp", 	"HeadMthRig_Geo_CnrOutUpMthRig_L_Bsh" )
bsh12 = ("cornerInnerDn", 	"HeadMthRig_Geo_CnrInDnMthRig_L_Bsh" )
bsh13 = ("cornerDn", 		"HeadMthRig_Geo_CnrDnMthRig_L_Bsh" )
bsh14 = ("cornerOuterDn", 	"HeadMthRig_Geo_CnrOutDnMthRig_L_Bsh" )
bsh15 = ("CnrDnOpen", 		"HeadMthRig_Geo_CnrDnOpenMthRig_L_Bsh" )
bsh16 = ("JawOpen", 		"HeadMthRig_Geo_CnrOpenMthRig_L_Bsh" )
bsh17 = ("CnrUpOpen", 		"HeadMthRig_Geo_CnrUpOpenMthRig_L_Bsh" )
bsh18 = ("CnrInDnOpen", 	"HeadMthRig_Geo_CnrInDnOpenMthRig_L_Bsh" )
bsh19 = ("CnrInOpen", 		"HeadMthRig_Geo_CnrInOpenMthRig_L_Bsh" )
bsh20 = ("CnrInUpOpen", 	"HeadMthRig_Geo_CnrInUpOpenMthRig_L_Bsh" )
bsh21 = ("CnrOutDnOpen", 	"HeadMthRig_Geo_CnrOutDnOpenMthRig_L_Bsh" )
bsh22 = ("CnrOutOpen", 		"HeadMthRig_Geo_CnrOutOpenMthRig_L_Bsh" )
bsh23 = ("CnrOutUpOpen", 	"HeadMthRig_Geo_CnrOutUpOpenMthRig_L_Bsh" )
bsh24 = ("puffUpperUp", 	"HeadMthRig_Geo_CheekMthRig_L_Bsh" )
bsh25 = ("puffLowerOut", 	"HeadMthRig_Geo_PuffMthRig_L_Bsh" )
mc.select(a)

mc.select(bsh1)
mc.select(bsh2)
mc.select(bsh3)
mc.select(bsh4)
mc.select(bsh5)
mc.select(bsh6)
mc.select(bsh7)
mc.select(bsh8)
mc.select(bsh9)
mc.select(bsh10)
mc.select(bsh11)
mc.select(bsh12)
mc.select(bsh13)
mc.select(bsh14)
mc.select(bsh15)
mc.select(bsh16)
mc.select(bsh17)
mc.select(bsh18)
mc.select(bsh19)
mc.select(bsh20)
mc.select(bsh21)
mc.select(bsh22)
mc.select(bsh23)
mc.select(bsh24)
mc.select(bsh25)
mc.select(bsh26)


