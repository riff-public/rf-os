import maya.cmds as mc
from rftool.rig.utaTools.pkmel import weightTools
reload(weightTools)
from lpRig import rigTools 
reload(rigTools)
from utaTools.utapy import utaCore
reload(utaCore)
from utaTools.utapy import utaRefElem 
reload(utaRefElem)
from lpRig import rigTools as lrr
reload(lrr)

def get_nodes_in_history_by_type(typ , selection):
    nodes = None
    for node in mc.listHistory(selection):
        if mc.nodeType(node) == typ:
            nodes = node
    return nodes

def copy_geo_skin():
    newJntList = []
    newGeoList = [] ### Update ###
    geoGrp = None ### Update ###
    bpmJntGrp = None ### Update ###
    sels = mc.ls(sl=True, fl=True)
    for sel in sels:
        mc.select(sel) ### Update ###
        vtxnum = mc.polyEvaluate(sel,v=True)
        skin = get_nodes_in_history_by_type('skinCluster', sel)
        # print skin
        
        jnts = mc.skinCluster(skin,query=True,inf=True)
        
        for jnt in jnts:
            vtxList = []
            # print jnt
            for num in range(vtxnum):
                vtx = '%s.vtx[%s]'%(sel,num)
                weight = mc.skinPercent(skin , vtx, transform=jnt, query=True )
                if weight > 0.0:
                    vtxList.append(vtx)
            # print vtxList
            if vtxList:
                newJntList.append(jnt)if not jnt in newJntList else None ### Update ###
                
        weightTools.writeSelectedWeightBpm(newJntList)
                
        newName = sel
        if ':' in sel:
            newName = sel.split(':')[1]
            nameSp = newName.split('_')
        newGeo = mc.duplicate(sel, name='%sBaseBpm_%s'%(nameSp[0],nameSp[-1]))
        newGeoList.append(newGeo) ### Update ###
        if not geoGrp: ### Update ###
            geoGrp = mc.group(name = '%sBaseBpmRig%s_Grp'%(nameSp[0], nameSp[-1]),empty=True)
        if not bpmJntGrp: ### Update ###
            bpmJntGrp = mc.group(name = '%sBaseBpmRigJnt_Grp'%nameSp[0],empty=True)
        mc.parent (newGeo, geoGrp)
    for newJnt in newJntList:
        jntName = newJnt
        if ':' in newJnt:
            jntName = newJnt.split(':')[1]
        jntNewName = '%s'%jntName
        mc.select(cl=True)
        mc.joint(name = jntNewName)
        mc.delete(mc.parentConstraint(newJnt ,jntNewName ,mo=False))
        mc.parent (jntNewName, bpmJntGrp)
            
    for geo in newGeoList:
        mc.select(cl=True)
        mc.select(geo)
        weightTools.readSelectedWeight()

    # ## Re name for Bpm Jnt
    jntBpm = mc.listRelatives([bpmJntGrp], c = True)
    for each in jntBpm:
        eachSp = each.split('_')
        if len(eachSp) == 2:
            if 'Sca' in eachSp[0]:
                scaSp = eachSp[0].split('Sca')
                # print eachSp, 'eachSp'
                # print scaSp, 'scaSp'
                mc.rename(each, '%sBaseBpmSca_%s' % (scaSp[0], eachSp[-1]))
            else:
                # print 'else','1'
                mc.rename(each, '%sBaseBpm_%s' % (eachSp[0], eachSp[-1]))
        else:
            # print 'else2', '2'
            mc.rename(each, '%sBaseBpm_%s_%s' % (eachSp[0], eachSp[1], eachSp[-1]))


    ## Remove Reference
    mc.select(sels[0])
    lrr.removeSelectedReference()
    ## Dlete NameSpace
    rigTools.removeAllNamespace()   


