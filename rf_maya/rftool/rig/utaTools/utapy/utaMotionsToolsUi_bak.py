import os,sys,traceback
import maya.cmds as mc;
import pymel.core as pm;
from utaTools.utapy import jointOnCurve5 as joc 
reload(joc)
from utaTools.utapy import createJoint as cj
reload(cj)
from utaTools.utapy import snapPaths as sp 
reload(sp)
from utaTools.utapy import getNameSpace as gns 
reload(gns)
# import tailControlRotate as ctr 
# reload(ctr)
from utaTools.utapy import locatorSnap as lss
reload(lss)

def jointOnCurveAdd(*args):
    # lists = mc.ls(sl = True)
    # spLists = lists[0].split('_Crv')
    # elem = spLists[0]
    joc.jointOnCurve(elem = '')
    pm.button("createControTxt", e=True, bgc= (0.498039, 1, 0))
def createMotionsAdd( elem = 'Motions',*args):
    objSel = mc.ls(sl=True)

    obj1 = objSel[0]
    obj2 = objSel[-1]
    # Lists NameSpace 
    nsLists = gns.getNs(nodeName=obj2)


    PathsMJnt = cj.createMotionsJnt(elem = 'PathsM', grpCon = False, ns= nsLists)
    PathsUpJnt = cj.createMotionsJnt(elem = 'PathsUp', grpCon = False, ns= nsLists)
    snapPathss = sp.snapPaths(objSel = objSel, elem = 'Motions', ns = nsLists, obj1 = obj1, obj2 = nsLists )
    # tailRotate = ctr.tailControlRotate (elem = 'TailRotate', ns = nsLists, switchCtrl = obj2)
    locatorSnaps = lss.locatorSnap(ns = nsLists)
    pm.button("snapMotionsTxt", e=True, bgc= (0.498039, 1, 0))

def createMotionsAddObj( elem = '',*args):
    objSel = mc.ls(sl = True)
    obj1 = objSel[0]
    obj2 = objSel[-1]

    # elem = obj2.split('_Crv', '_crv')
    # print elem , '....................'
    # Lists NameSpace 
    nsLists = gns.getNs(nodeName=obj1)


    # PathsMJnt = cj.createMotionsJnt(elem = 'PathsM', grpCon = False, ns= nsLists)
    # PathsUpJnt = cj.createMotionsJnt(elem = 'PathsUp', grpCon = False, ns= nsLists)
    snapPathss = sp.snapPathsObj(elem = elem, ns = nsLists, obj1 = obj1, obj2 = obj2 )
    # tailRotate = ctr.tailControlRotate (elem = 'TailRotate', ns = nsLists, switchCtrl = obj2)
    # locatorSnaps = lss.locatorSnapObj(ns = nsLists, obj = obj1)
    pm.button("snapMotions1ObjTxt", e=True, bgc= (0.498039, 1, 0))

# UI ---------------------------------------------------------------------------------------------------
# UI ---------------------------------------------------------------------------------------------------
# UI ---------------------------------------------------------------------------------------------------
# UI ---------------------------------------------------------------------------------------------------
# UI ---------------------------------------------------------------------------------------------------
def utaMotionsToolsUi () :

    # check if window exists
    if pm.window ( 'utaPathToolsUI' , exists = True ) :
        pm.deleteUI ( 'utaPathToolsUI' ) ;
    title = "Motion Tools v1.0 (01-02-2018)" ;
    # create window 
    window = pm.window ( 'utaPathToolsUI', 
                            title = title , 
                            width = 100,
                            mnb = True , 
                            mxb = True , 
                            sizeable = True , 
                            rtf = True ) ;
    windowRowColumnLayout = pm.window ( 'utaPathToolsUI', 
                            e = True , 
                            width = 150);

    # motionTab A-------------------------------------------------------------------------------------------------------------------------------    
    mainMotionTab = pm.tabLayout ( innerMarginWidth = 1 , innerMarginHeight = 1 , p = windowRowColumnLayout ) ;

    MotionTab_A = pm.rowColumnLayout ( "Motions", nc = 1 , p = mainMotionTab ) ;
    mainMotionPathTab = pm.rowColumnLayout ( nc = 1 , p = MotionTab_A , w = 200) ;

    motionPathsRowLine = pm.rowColumnLayout ( nc = 1 , p = mainMotionPathTab ) ;

    rowLine_TabA = pm.rowColumnLayout ( nc = 1 , p = motionPathsRowLine ) ;
    mc.tabLayout(p=rowLine_TabA, w= 192)

    # Control Tab ------------------------------------------------------
    # Control Tab ------------------------------------------------------

        # line 1 
    rowLine1 = pm.rowColumnLayout ( nc = 3 , p = motionPathsRowLine , h = 30 , cw = [(1, 10), (2, 180),(3, 10 ) ]);
    pm.rowColumnLayout (p = rowLine1 ,h=8) ;
    pm.text( label='1. Create Control Paths' , p = rowLine1, al = "left")  
    pm.rowColumnLayout (p = rowLine1 ,h=8) ;

    rowLine2 = pm.rowColumnLayout ( nc = 3 , p = motionPathsRowLine , h = 30 , cw = [(1, 10), (2, 180), (3, 10) ]) ;
    pm.rowColumnLayout (p = rowLine2 ,h=8) ;
    pm.button ('createControTxt', label = " Create Control" ,h=25, p = rowLine2 , c = jointOnCurveAdd, bgc = (0.411765, 0.411765, 0.411765)) ;
    pm.rowColumnLayout (p = rowLine2 ,h=8) ;

    # # TabA
    # rowLine_TabA = pm.rowColumnLayout ( nc = 1 , p = motionPathsRowLine ) ;
    # mc.tabLayout(p=rowLine_TabA, w= 192)

    # # Motions Tab ------------------------------------------------------
    # # Motions Tab ------------------------------------------------------

    # pm.rowColumnLayout (p = motionPathsRowLine ) ;
    # pm.text( label='    2. Create Motions Paths' , p = motionPathsRowLine, al = "left")
    # pm.rowColumnLayout (p = motionPathsRowLine,h=8 ) ;

    #     # line 2 
    # rowLine2 = pm.rowColumnLayout ( nc = 3 , p = motionPathsRowLine , h = 30 , cw = [(1, 10), (2,180 ),(3, 10 )] ) ;
    # pm.rowColumnLayout (p = rowLine2 ,h=8) ;
    # pm.button ('snapMotionsTxt', label = " Create Motions (For Nakee2)" ,h=25, p = rowLine2 , c = createMotionsAdd, bgc = (0.411765, 0.411765, 0.411765)) ;
    # pm.rowColumnLayout (p = rowLine2 ,h=8) ;

    #     # TabB
    # rowLine_TabB = pm.rowColumnLayout ( nc = 1 , p = motionPathsRowLine ) ;
    # mc.tabLayout(p=rowLine_TabB, w= 192)


    # Motions Tab ------------------------------------------------------
    # Motions Tab ------------------------------------------------------

    pm.rowColumnLayout (p = motionPathsRowLine ) ;
    pm.text( label='    3. Create Motions Paths' , p = motionPathsRowLine, al = "left")
    pm.rowColumnLayout (p = motionPathsRowLine,h=8 ) ;

        # line 2 
    rowLine2 = pm.rowColumnLayout ( nc = 3 , p = motionPathsRowLine , h = 30 , cw = [(1, 10), (2,180 ),(3, 10 )] ) ;
    pm.rowColumnLayout (p = rowLine2 ,h=8) ;
    pm.button ('snapMotions1ObjTxt', label = " Create Motions " ,h=25, p = rowLine2 , c = createMotionsAddObj, bgc = (0.411765, 0.411765, 0.411765)) ;
    pm.rowColumnLayout (p = rowLine2 ,h=8) ;

        # TabB
    rowLine_TabC = pm.rowColumnLayout ( nc = 1 , p = mainMotionPathTab ) ;
    mc.tabLayout(p=rowLine_TabC, w= 192)

    pm.setParent("..")
    pm.showWindow(window)