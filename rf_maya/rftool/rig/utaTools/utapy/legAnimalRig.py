import maya.cmds as mc
import maya.mel as mm
import maya.OpenMaya as om
from ncmel import core
from ncmel import rigTools as rt
from ncmel import ribbonRig
reload( core )
reload( rt )
reload( ribbonRig )

# -------------------------------------------------------------------------------------------------------------
#
#  LEG ANIMAL RIGGING MODULE
#  
# -------------------------------------------------------------------------------------------------------------

class Run( object ):

    def __init__( self , upLegTmpJnt   = 'UpLeg_L_TmpJnt' ,
                         midLegTmpJnt  = 'MidLeg_L_TmpJnt' ,
                         lowLegTmpJnt  = 'LowLeg_L_TmpJnt' ,
                         ankleTmpJnt   = 'Ankle_L_TmpJnt' ,
                         ballTmpJnt    = 'Ball_L_TmpJnt' ,
                         toeTmpJnt     = 'Toe_L_TmpJnt' ,
                         heelTmpJnt    = 'Heel_L_TmpJnt' ,
                         footInTmpJnt  = 'FootIn_L_TmpJnt' ,
                         footOutTmpJnt = 'FootOut_L_TmpJnt' ,
                         kneeTmpJnt    = 'Knee_L_TmpJnt' ,
                         heelIkTwistPivTmpJnt = 'HeelIkTwistPiv_L_TmpJnt',
                         parent        = 'Pelvis_Jnt' , 
                         ctrlGrp       = 'Ctrl_Grp' ,
                         jntGrp        = 'Jnt_Grp' ,
                         skinGrp       = 'Skin_Grp' ,
                         stillGrp      = 'Still_Grp' ,
                         ikhGrp        = 'Ikh_Grp' ,
                         elem          = '' ,
                         side          = 'L' ,
                         ribbon        = True ,
                         hi            = True ,
                         foot          = True ,
                         size          = 1 
                 ):

        ##-- Info
        elem = elem.capitalize()
        if side == '' :
            sideRbn = ''
            side = '_'
        else :
            sideRbn = '%s' %side
            side = '_%s_' %side

        if 'L' in side  :
            valueSide = 1
            axis = 'y+'
            upVec = [ 0 , 0 , 1 ]
            rootTwsAmp = 1
            endTwsAmp = -1
        elif 'R' in side :
            valueSide = -1
            axis = 'y-'
            upVec = [ 0 , 0 , -1 ]
            rootTwsAmp = -1
            endTwsAmp = 1


        #-- Create Main Group
        self.legCtrlGrp = rt.createNode( 'transform' , 'Leg%sCtrl%sGrp' %( elem , side ))
        self.legJntGrp = rt.createNode( 'transform' , 'Leg%sJnt%sGrp' %( elem , side ))
        mc.parentConstraint( parent , self.legCtrlGrp , mo = False )
        self.legJntGrp.snap( parent )
        
        #-- Create Joint
        self.upLegJnt = rt.createJnt( 'UpLeg%s%sJnt' %( elem , side ) , upLegTmpJnt )
        self.midLegJnt = rt.createJnt( 'MidLeg%s%sJnt' %( elem , side ) , midLegTmpJnt )
        self.lowLegJnt = rt.createJnt( 'LowLeg%s%sJnt' %( elem , side ) , lowLegTmpJnt )
        self.ankleJnt = rt.createJnt( 'Ankle%s%sJnt' %( elem , side ) , ankleTmpJnt )
        self.ankleJnt.parent( self.lowLegJnt )
        self.lowLegJnt.parent( self.midLegJnt )
        self.midLegJnt.parent( self.upLegJnt )
        self.upLegJnt.parent( parent )

        if foot == True :
            self.ballJnt = rt.createJnt( 'Ball%s%sJnt' %( elem , side ) , ballTmpJnt )
            self.toeJnt = rt.createJnt( 'Toe%s%sJnt' %( elem , side ) , toeTmpJnt )
            self.toeJnt.parent( self.ballJnt )
            self.ballJnt.parent( self.ankleJnt )

        #-- Create Controls
        self.legCtrl = rt.createCtrl( 'Leg%s%sCtrl' %( elem , side ) , 'stick' , 'green' )
        self.legZro = rt.addGrp( self.legCtrl )

        #-- Adjust Shape Controls
        self.legCtrl.scaleShape( size )

        if 'L' in side :
            self.legCtrl.rotateShape( -90 , 0 , 0 )
        else :
            self.legCtrl.rotateShape( 90 , 0 , 0 )
        
        #-- Adjust Rotate Order
        for obj in ( self.upLegJnt , self.midLegJnt , self.lowLegJnt , self.ankleJnt ) :
            obj.setRotateOrder( 'yzx' )

        if foot == True :
            for obj in ( self.ballJnt , self.toeJnt ) :
                obj.setRotateOrder( 'yzx' )

        #-- Rig process
        mc.parentConstraint( self.ankleJnt , self.legZro , mo = False )
        upLegNonRollJnt = rt.addNonRollJnt( 'UpLeg' , elem , self.upLegJnt , self.midLegJnt , parent , axis )
        midLegNonRollJnt = rt.addNonRollJnt( 'MidLeg' , elem , self.midLegJnt , self.lowLegJnt , self.upLegJnt , axis )
        lowLegNonRollJnt = rt.addNonRollJnt( 'LowLeg' , elem , self.lowLegJnt , self.ankleJnt , self.midLegJnt , axis )

        self.upLegNonRollJntGrp = upLegNonRollJnt['jntGrp']
        self.upLegNonRollRootNr = upLegNonRollJnt['rootNr']
        self.upLegNonRollRootNrZro = upLegNonRollJnt['rootNrZro']
        self.upLegNonRollEndNr = upLegNonRollJnt['endNr']
        self.upLegNonRollIkhNr = upLegNonRollJnt['ikhNr']
        self.upLegNonRollIkhNrZro = upLegNonRollJnt['ikhNrZro']
        self.upLegNonRollTwistGrp = upLegNonRollJnt['twistGrp']

        self.midLegNonRollJntGrp = midLegNonRollJnt['jntGrp']
        self.midLegNonRollRootNr = midLegNonRollJnt['rootNr']
        self.midLegNonRollRootNrZro = midLegNonRollJnt['rootNrZro']
        self.midLegNonRollEndNr = midLegNonRollJnt['endNr']
        self.midLegNonRollIkhNr = midLegNonRollJnt['ikhNr']
        self.midLegNonRollIkhNrZro = midLegNonRollJnt['ikhNrZro']
        self.midLegNonRollTwistGrp = midLegNonRollJnt['twistGrp']

        self.lowLegNonRollJntGrp = lowLegNonRollJnt['jntGrp']
        self.lowLegNonRollRootNr = lowLegNonRollJnt['rootNr']
        self.lowLegNonRollRootNrZro = lowLegNonRollJnt['rootNrZro']
        self.lowLegNonRollEndNr = lowLegNonRollJnt['endNr']
        self.lowLegNonRollIkhNr = lowLegNonRollJnt['ikhNr']
        self.lowLegNonRollIkhNrZro = lowLegNonRollJnt['ikhNrZro']
        self.lowLegNonRollTwistGrp = lowLegNonRollJnt['twistGrp']

        #-- Adjust Hierarchy
        mc.parent( self.legZro , self.legCtrlGrp )
        mc.parent( self.legCtrlGrp , ctrlGrp )
        mc.parent( self.upLegNonRollJntGrp , self.midLegNonRollJntGrp , self.lowLegNonRollJntGrp , self.legJntGrp )
        mc.parent( self.legJntGrp , jntGrp )

        #-- Cleanup
        for obj in ( self.legZro , self.legJntGrp , self.legCtrlGrp ) :
            obj.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        self.legCtrl.lockHideAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        #-- Fk
        #-- Create Main Group Fk
        self.legFkCtrlGrp = rt.createNode( 'transform' , 'Leg%sFkCtrl%sGrp' %( elem , side ))
        self.legFkJntGrp = rt.createNode( 'transform' , 'Leg%sFkJnt%sGrp' %( elem , side ))

        self.legFkCtrlGrp.snap( self.legCtrlGrp )
        self.legFkJntGrp.snap( self.legCtrlGrp )
    
        #-- Create Joint Fk
        self.upLegFkJnt = rt.createJnt( 'UpLeg%sFk%sJnt' %( elem , side ) , upLegTmpJnt )
        self.midLegFkJnt = rt.createJnt( 'MidLeg%sFk%sJnt' %( elem , side ) , midLegTmpJnt )
        self.lowLegFkJnt = rt.createJnt( 'LowLeg%sFk%sJnt' %( elem , side ) , lowLegTmpJnt )
        self.ankleFkJnt = rt.createJnt( 'Ankle%sFk%sJnt' %( elem , side ) , ankleTmpJnt )
        self.ankleFkJnt.parent( self.lowLegFkJnt )
        self.lowLegFkJnt.parent( self.midLegFkJnt )
        self.midLegFkJnt.parent( self.upLegFkJnt )
        self.upLegFkJnt.parent( self.legFkJntGrp )
        
        if foot == True :
            self.ballFkJnt = rt.createJnt( 'Ball%sFk%sJnt' %( elem , side ) , ballTmpJnt )
            self.toeFkJnt = rt.createJnt( 'Toe%sFk%sJnt' %( elem , side ) , toeTmpJnt )
            self.toeFkJnt.parent( self.ballFkJnt )
            self.ballFkJnt.parent( self.ankleFkJnt )

        #-- Create Controls Fk
        self.upLegFkCtrl = rt.createCtrl( 'UpLeg%sFk%sCtrl' %( elem , side ) , 'cylinder' , 'red' , jnt = True )
        self.upLegFkGmbl = rt.addGimbal( self.upLegFkCtrl )
        self.upLegFkZro = rt.addGrp( self.upLegFkCtrl )
        self.upLegFkOfst = rt.addGrp( self.upLegFkCtrl , 'Ofst' )
        self.upLegFkZro.snapPoint( self.upLegFkJnt )
        self.upLegFkCtrl.snapJntOrient( self.upLegFkJnt )

        self.midLegFkCtrl = rt.createCtrl( 'MidLeg%sFk%sCtrl' %( elem , side ) , 'cylinder' , 'red' , jnt = True )
        self.midLegFkGmbl = rt.addGimbal( self.midLegFkCtrl )
        self.midLegFkZro = rt.addGrp( self.midLegFkCtrl )
        self.midLegFkOfst = rt.addGrp( self.midLegFkCtrl )
        self.midLegFkZro.snapPoint( self.midLegFkJnt )
        self.midLegFkCtrl.snapJntOrient( self.midLegFkJnt )

        self.lowLegFkCtrl = rt.createCtrl( 'LowLeg%sFk%sCtrl' %( elem , side ) , 'cylinder' , 'red' , jnt = True )
        self.lowLegFkGmbl = rt.addGimbal( self.lowLegFkCtrl )
        self.lowLegFkZro = rt.addGrp( self.lowLegFkCtrl )
        self.lowLegFkOfst = rt.addGrp( self.lowLegFkCtrl )
        self.lowLegFkZro.snapPoint( self.lowLegFkJnt )
        self.lowLegFkCtrl.snapJntOrient( self.lowLegFkJnt )

        self.ankleFkCtrl = rt.createCtrl( 'Ankle%sFk%sCtrl' %( elem , side ) , 'cylinder' , 'red' , jnt = True )
        self.ankleFkGmbl = rt.addGimbal( self.ankleFkCtrl )
        self.ankleFkZro = rt.addGrp( self.ankleFkCtrl )
        self.ankleFkOfst = rt.addGrp( self.ankleFkCtrl )
        self.ankleFkZro.snapPoint( self.ankleFkJnt )
        self.ankleFkCtrl.snapJntOrient( self.ankleFkJnt )

        if foot == True :
            self.ballFkCtrl = rt.createCtrl( 'Ball%sFk%sCtrl' %( elem , side ) , 'cylinder' , 'red' , jnt = True )
            self.ballFkGmbl = rt.addGimbal( self.ballFkCtrl )
            self.ballFkZro = rt.addGrp( self.ballFkCtrl )
            self.ballFkOfst = rt.addGrp( self.ballFkCtrl )
            self.ballFkSclOfst = rt.createNode( 'transform' , 'Ball%sFkCtrlSclOfst%sGrp' %( elem , side ) )
            self.ballFkSclOfst.snap( self.ankleFkJnt )
            self.ballFkZro.snapPoint( self.ballFkJnt )
            self.ballFkCtrl.snapJntOrient( self.ballFkJnt )
            self.ballFkZro.parent( self.ballFkSclOfst )

        #-- Adjust Shape Controls Fk
        for ctrl in ( self.upLegFkCtrl , self.upLegFkGmbl , self.midLegFkCtrl , self.midLegFkGmbl  , self.lowLegFkCtrl , self.lowLegFkGmbl , self.ankleFkCtrl , self.ankleFkGmbl ) :
            ctrl.scaleShape( size )
            
        if foot == True :
            for ctrl in ( self.ballFkCtrl , self.ballFkGmbl ) :
                ctrl.scaleShape( size )
    
        #-- Adjust Rotate Order Fk
        for obj in ( self.upLegFkJnt , self.midLegFkJnt , self.lowLegFkJnt , self.ankleFkJnt , self.upLegFkCtrl , self.midLegFkCtrl , self.lowLegFkCtrl , self.ankleFkCtrl ) :
            obj.setRotateOrder( 'yzx' )

        if foot == True :
            for obj in ( self.ballFkJnt , self.toeFkJnt , self.ballFkCtrl ) :
                obj.setRotateOrder( 'yzx' )

        #-- Rig process Fk
        mc.parentConstraint( self.upLegFkGmbl , self.upLegFkJnt , mo = False )
        mc.parentConstraint( self.midLegFkGmbl , self.midLegFkJnt , mo = False )
        mc.parentConstraint( self.lowLegFkGmbl , self.lowLegFkJnt , mo = False )
        mc.parentConstraint( self.ankleFkGmbl , self.ankleFkJnt , mo = False )

        rt.addFkStretch( self.upLegFkCtrl , self.midLegFkOfst , 'y' , -1 )
        rt.addFkStretch( self.midLegFkCtrl , self.lowLegFkOfst , 'y' , -1 )
        rt.addFkStretch( self.lowLegFkCtrl , self.ankleFkOfst , 'y' , -1 )
        
        if foot == True :
            mc.parentConstraint( self.ballFkGmbl , self.ballFkJnt , mo = False )
            rt.addFkStretch( self.ballFkCtrl , self.toeFkJnt , 'y' , valueSide )

        rt.localWorld( self.upLegFkCtrl , ctrlGrp , self.legFkCtrlGrp , self.upLegFkZro , 'orient' )

        self.upLegFkCtrl.attr('localWorld').value = 1
    
        #-- Adjust Hierarchy Fk
        mc.parent( self.legFkCtrlGrp , self.legCtrlGrp )
        mc.parent( self.legFkJntGrp , self.legJntGrp )
        mc.parent( self.upLegFkZro , self.legFkCtrlGrp )
        mc.parent( self.midLegFkZro , self.upLegFkGmbl )
        mc.parent( self.lowLegFkZro , self.midLegFkGmbl )
        mc.parent( self.ankleFkZro , self.lowLegFkGmbl )

        if foot == True :
            mc.parent( self.ballFkSclOfst , self.ankleFkGmbl )

        #-- Ik
        #-- Create Main Group Ik
        self.legIkCtrlGrp = rt.createNode( 'transform' , 'Leg%sIkCtrl%sGrp' %( elem , side ))
        self.legIkJntGrp = rt.createNode( 'transform' , 'Leg%sIkJnt%sGrp' %( elem , side ))
        self.legIkIkhGrp = rt.createNode( 'transform' , 'Leg%sIkh%sGrp' %( elem , side ))

        self.legIkCtrlGrp.snap( self.legCtrlGrp )
        self.legIkJntGrp.snap( self.legCtrlGrp )
        self.legIkIkhGrp.snap( self.legCtrlGrp )
        
        if foot == True :
            #-- Create Pivot Group Ik
            self.footPivGrp = rt.createNode( 'transform' , 'Foot%sIkPiv%sGrp' %( elem , side ))
            self.ballPivGrp = rt.createNode( 'transform' , 'Ball%sIkPiv%sGrp' %( elem , side ))
            self.ballPivCtrl = rt.createCtrl( 'Ball%sIkPiv%sCtrl' %( elem , side ) , 'arcCapsule' , 'blue' , jnt = 0 )

            self.bendPivGrp = rt.createNode( 'transform' , 'Bend%sIkPiv%sGrp' %( elem , side ))
            self.bendPivCtrl = rt.createCtrl( 'Bend%sIkPiv%sCtrl' %( elem , side ) , 'arcCapsule' , 'blue' , jnt = 0 )

            self.heelPivGrp = rt.createNode( 'transform' , 'Heel%sIkPiv%sGrp' %( elem , side ))
            self.heelPivCtrl = rt.createCtrl( 'Heel%sIkPiv%sCtrl' %( elem , side ) , 'sphere' , 'yellow' , jnt = 0 )

            self.heelTwistPivGrp = rt.createNode( 'transform' , 'Heel%sIkTwistPiv%sGrp' %( elem , side ))
            self.heelTwistPivCtrl = rt.createCtrl( 'Heel%sIkTwistPiv%sCtrl' %( elem , side ) , 'rotateCircle' , 'yellow' , jnt = 0 )

            self.toePivGrp = rt.createNode( 'transform' , 'Toe%sIkPiv%sGrp' %( elem , side ))
            self.toePivCtrl = rt.createCtrl( 'Toe%sIkPiv%sCtrl' %( elem , side ) , 'sphere' , 'yellow' , jnt = 0 )

            self.toeTwistPivGrp = rt.createNode( 'transform' , 'Toe%sIkTwistPiv%sGrp' %( elem , side ))
            self.toeTwistPivCtrl = rt.createCtrl( 'Toe%sIkTwistPiv%sCtrl' %( elem , side ) , 'rotateCircle' , 'yellow' , jnt = 0 )

            self.inPivGrp = rt.createNode( 'transform' , 'In%sIkPiv%sGrp' %( elem , side ))
            self.inPivCtrl  = rt.createCtrl( 'In%sIkPiv%sCtrl' %( elem , side ) , 'sphere' , 'yellow' , jnt = 0 )

            self.outPivGrp = rt.createNode( 'transform' , 'Out%sIkPiv%sGrp' %( elem , side ))
            self.outPivCtrl = rt.createCtrl( 'Out%sIkPiv%sCtrl' %( elem , side ) , 'sphere' , 'yellow' , jnt = 0 )

            self.anklePivGrp = rt.createNode( 'transform' , 'Ankle%sIkPiv%sGrp' %( elem , side ))

            
            self.footPivGrp.snapPoint( ankleTmpJnt )
            self.ballPivGrp.snapPoint( ballTmpJnt )
            self.bendPivGrp.snapPoint( ballTmpJnt )
            self.toePivGrp.snapPoint( toeTmpJnt )
            self.heelPivGrp.snapPoint( heelTmpJnt )
            self.inPivGrp.snapPoint( footInTmpJnt )
            self.outPivGrp.snapPoint( footOutTmpJnt )
            self.anklePivGrp.snapPoint( ankleTmpJnt )

            mc.delete(mc.pointConstraint( toeTmpJnt , ballTmpJnt , self.toeTwistPivGrp , mo = False ))
            mc.delete(mc.pointConstraint( heelIkTwistPivTmpJnt , self.heelTwistPivGrp , mo = False ))

            for obj in ( self.footPivGrp , self.ballPivGrp , self.bendPivGrp , self.toePivGrp , self.toeTwistPivGrp , self.heelPivGrp , self.heelTwistPivGrp , self.inPivGrp , self.outPivGrp ) :
                obj.snapOrient( self.ballFkJnt )
            
            self.ballPivCtrl.snap( self.ballPivGrp )
            self.bendPivCtrl.snap( self.bendPivGrp )
            self.toePivCtrl.snap( self.toePivGrp )
            self.heelPivCtrl.snap( self.heelPivGrp )
            self.inPivCtrl.snap( self.inPivGrp )
            self.outPivCtrl.snap( self.outPivGrp )          
            self.toeTwistPivCtrl.snap( self.toeTwistPivGrp )
            self.heelTwistPivCtrl.snap( self.heelTwistPivGrp )

            self.anklePivGrp.parent(self.ballPivCtrl)
            self.ballPivCtrl.parent(self.ballPivGrp)
            self.bendPivCtrl.parent(self.bendPivGrp)
            mc.parent(self.bendPivGrp,self.ballPivGrp,self.outPivCtrl)
            self.outPivCtrl.parent(self.outPivGrp)
            self.outPivGrp.parent(self.inPivCtrl)
            self.inPivCtrl.parent(self.inPivGrp)
            self.inPivGrp.parent( self.heelTwistPivCtrl)
            self.heelTwistPivCtrl.parent( self.heelTwistPivGrp)
            self.heelTwistPivGrp.parent( self.heelPivCtrl)
            self.heelPivCtrl.parent( self.heelPivGrp)
            self.heelPivGrp.parent( self.toeTwistPivCtrl)
            self.toeTwistPivCtrl.parent( self.toeTwistPivGrp)
            self.toeTwistPivGrp.parent( self.toePivCtrl)
            self.toePivCtrl.parent(self.toePivGrp)
            self.toePivGrp.parent(self.footPivGrp )


            # mc.parent( self.ballPivGrp , self.bendPivGrp , self.outPivGrp )
            # self.outPivGrp.parent( self.inPivGrp )
            # self.inPivGrp.parent( self.heelTwistPivGrp )
            # self.heelTwistPivGrp.parent( self.heelPivGrp )
            # self.heelPivGrp.parent( self.toeTwistPivGrp )
            # self.toeTwistPivGrp.parent( self.toePivGrp )
            # self.toePivGrp.parent( self.footPivGrp )
            for ctrl in ( self.ballPivCtrl , self.bendPivCtrl , self.toePivCtrl , self.heelPivCtrl,self.inPivCtrl,self.outPivCtrl,self.heelTwistPivCtrl,self.toeTwistPivCtrl) :
                ctrl.scaleShape( size )
                for ax in 'xyz':
                    mc.setAttr(ctrl.pynode()+'.s%s'%(ax),l=1,k=0)
                    mc.setAttr(ctrl.pynode()+'.t%s'%(ax),l=1,k=0)
                mc.setAttr(ctrl.pynode()+'.v',l=1,k=0)

        #-- Create Joint Ik
        self.upLegIkJnt = rt.createJnt( 'UpLeg%sIk%sJnt' %( elem , side ) , upLegTmpJnt )
        self.midLegIkJnt = rt.createJnt( 'MidLeg%sIk%sJnt' %( elem , side ) , midLegTmpJnt )
        self.lowLegIkJnt = rt.createJnt( 'LowLeg%sIk%sJnt' %( elem , side ) , lowLegTmpJnt )
        self.ankleIkJnt = rt.createJnt( 'Ankle%sIk%sJnt' %( elem , side ) , ankleTmpJnt )
        self.ankleIkJnt.parent( self.lowLegIkJnt )
        self.lowLegIkJnt.parent( self.midLegIkJnt )
        self.midLegIkJnt.parent( self.upLegIkJnt )
        self.upLegIkJnt.parent( self.legIkJntGrp )

        self.upLegIkSecJnt = rt.createJnt( 'UpLeg%sIkSec%sJnt' %( elem , side ) , upLegTmpJnt )
        self.midLegIkSecJnt = rt.createJnt( 'MidLeg%sIkSec%sJnt' %( elem , side ) , midLegTmpJnt )
        self.lowLegIkSecJnt = rt.createJnt( 'LowLeg%sIkSec%sJnt' %( elem , side ) , lowLegTmpJnt )
        self.ankleIkSecJnt = rt.createJnt( 'Ankle%sIkSec%sJnt' %( elem , side ) , ankleTmpJnt )
        self.ankleIkSecJnt.parent( self.lowLegIkSecJnt )
        self.lowLegIkSecJnt.parent( self.midLegIkSecJnt )
        self.midLegIkSecJnt.parent( self.upLegIkSecJnt )
        self.upLegIkSecJnt.parent( self.legIkJntGrp )
        
        if foot == True :
            self.ballIkJnt = rt.createJnt( 'Ball%sIk%sJnt' %( elem , side ) , ballTmpJnt )
            self.toeIkJnt = rt.createJnt( 'Toe%sIk%sJnt' %( elem , side ) , toeTmpJnt )
            self.toeIkJnt.parent( self.ballIkJnt )
            self.ballIkJnt.parent( self.ankleIkJnt )

        #-- Adjust Position Ik Second Joint 
        for ikSecJnt in ( self.midLegIkSecJnt , self.lowLegIkSecJnt , self.ankleIkSecJnt ):
            if not ikSecJnt.attr('tx').value == 0 :
                ikSecJnt.attr('tx').value = 0

        self.upVecLoc = rt.createNode( 'transform' , 'UpVecLoc_Grp' )
        self.upVecLocZro = rt.addGrp( self.upVecLoc )
        self.upVecLocZro.snap( self.upLegIkSecJnt )
        self.upVecLoc.attr('tx').value = 2
        
        mc.delete( mc.aimConstraint ( ankleTmpJnt , self.upLegIkSecJnt , mo = False  , aim = (0,1,0)  , u = (1,0,0) , wut = 'object' , wuo = self.upVecLoc ))
        
        self.upLegIkSecJnt.attr('rx').value = 0
        self.upLegIkSecJnt.attr('ry').value = 0
        self.ankleIkSecJnt.snapPoint( ankleTmpJnt )
        mc.delete( self.upVecLocZro )

        #-- Create Controls Ik
        self.upLegIkCtrl = rt.createCtrl( 'UpLeg%sIk%sCtrl' %( elem , side ) , 'cube' , 'blue' , jnt = True )
        self.upLegIkGmbl = rt.addGimbal( self.upLegIkCtrl )
        self.upLegIkZro = rt.addGrp( self.upLegIkCtrl )
        self.upLegIkOfst = rt.addGrp( self.upLegIkCtrl , 'Ofst' )
        self.upLegIkZro.snapPoint( self.upLegIkJnt )
        self.upLegIkCtrl.snapJntOrient( self.upLegIkJnt )

        self.ankleIkCtrl = rt.createCtrl( 'Ankle%sIk%sCtrl' %( elem , side ) , 'cube' , 'blue' , jnt = True )
        self.ankleIkGmbl = rt.addGimbal( self.ankleIkCtrl )
        self.ankleIkZro = rt.addGrp( self.ankleIkCtrl )
        self.ankleIkOfst = rt.addGrp( self.ankleIkCtrl )
        self.ankleIkZro.snapPoint( self.ankleIkJnt )
        self.ankleIkCtrl.snapJntOrient( self.ankleIkJnt )

        self.kneeIkCtrl = rt.createCtrl( 'Knee%sIk%sCtrl' %( elem , side ) , 'sphere' , 'blue' , jnt = True )
        self.kneeIkZro = rt.addGrp( self.kneeIkCtrl )
        self.kneeIkZro.snapPoint( kneeTmpJnt )
        
        self.midLegJntTx = mc.getAttr( '%s.tx' %self.midLegJnt )
        
        if not int(self.midLegJntTx) == 0 :
            mc.delete( mc.pointConstraint ( self.midLegIkSecJnt , self.kneeIkZro , mo = False , sk = 'z' ))

        #-- Adjust Shape Controls Ik
        for ctrl in ( self.upLegIkCtrl , self.upLegIkGmbl , self.ankleIkCtrl , self.ankleIkGmbl , self.kneeIkCtrl ) :
            ctrl.scaleShape( size )
    
        #-- Adjust Rotate Order Ik
        for obj in ( self.upLegIkJnt , self.lowLegIkJnt , self.ankleIkJnt , self.upLegIkCtrl , self.ankleIkCtrl ) :
            obj.setRotateOrder( 'yzx' )
        
        if foot == True :
            for obj in ( self.ballIkJnt , self.toeIkJnt ) :
                obj.setRotateOrder( 'yzx' )

        #-- Rig process Ik
        self.ankleIkLocWor = rt.localWorld( self.ankleIkCtrl , ctrlGrp , self.upLegIkGmbl , self.ankleIkZro , 'parent' )
        self.kneeIkLocWor = rt.localWorld( self.kneeIkCtrl , ctrlGrp , self.ankleIkCtrl , self.kneeIkZro , 'parent' )
        
        self.crv = rt.drawLine( self.kneeIkCtrl , self.midLegIkJnt )

        self.lowLegIkhDict = rt.addIkh( 'LowLeg%s' %elem , 'ikRPsolver' , self.upLegIkJnt , self.lowLegIkJnt )
        self.ankleIkhDict = rt.addIkh( 'Ankle%s' %elem , 'ikRPsolver' , self.lowLegIkJnt , self.ankleIkJnt )
        self.ankleIkhSecDict = rt.addIkh( 'Ankle%sSec' %elem , 'ikRPsolver' , self.upLegIkSecJnt , self.ankleIkSecJnt )
        
        self.lowLegIkh = self.lowLegIkhDict['ikh']
        self.lowLegIkhZro = self.lowLegIkhDict['ikhZro']
        self.ankleIkh = self.ankleIkhDict['ikh']
        self.ankleIkhZro = self.ankleIkhDict['ikhZro']
        self.ankleIkhSec = self.ankleIkhSecDict['ikh']
        self.ankleIkhSecZro = self.ankleIkhSecDict['ikhZro']

        self.polevector = mc.poleVectorConstraint( self.kneeIkCtrl , self.ankleIkhSec )[0]

        self.ankleIkCtrl.addAttr( 'twistUpLeg' )
        self.ankleIkCtrl.addAttr( 'twistLowLeg' )

        self.ankleIkCtrl.attr( 'twistUpLeg' ) >> self.lowLegIkh.attr('twist')
        self.ankleIkCtrl.attr( 'twistLowLeg' ) >> self.ankleIkhSec.attr('twist')

        self.ankleIkCtrl.addAttr( 'legFlex' )
        self.ankleIkCtrl.addAttr( 'legBend' )
        
        self.anklePivot = rt.createNode( 'transform' , 'Ankle%sIkhPiv%sGrp' %( elem , side ))
        self.anklePivot.snapPoint( self.ankleIkJnt )
        self.anklePivot.snapOrient( kneeTmpJnt )
        
        self.ankleIkCtrl.attr( 'legFlex' ) >> self.anklePivot.attr('rx')
        self.ankleIkCtrl.attr( 'legBend' ) >> self.anklePivot.attr('rz')

        mc.pointConstraint( self.upLegIkGmbl , self.upLegIkJnt , mo = False )
        mc.pointConstraint( self.upLegIkGmbl , self.upLegIkSecJnt , mo = False )
        mc.parentConstraint( self.lowLegIkSecJnt , self.lowLegIkhZro , mo = True )

        self.ikStrt = rt.addIkStretch( self.ankleIkCtrl , '' , 'ty' , 'Leg%s' %elem , self.upLegIkJnt , self.midLegIkJnt , self.lowLegIkJnt , self.ankleIkJnt )
        mc.connectAttr( 'Leg%sIkAutoStrt%sPma.o3x' %(elem,side) , '%s.ty' %self.midLegIkSecJnt )
        mc.connectAttr( 'Leg%sIkAutoStrt%sPma.o3y' %(elem,side) , '%s.ty' %self.lowLegIkSecJnt )
        mc.connectAttr( 'Leg%sIkAutoStrt%sPma.o3z' %(elem,side) , '%s.ty' %self.ankleIkSecJnt )

        self.ankleIkCtrl.attr('localWorld').value = 1
        self.ankleIkCtrl.attr('autoStretch').value = 1

        if foot == True :
            self.ankleIkCtrl.addAttr( 'toeStretch' )
            self.toeStrtMdv = rt.createNode( 'multiplyDivide' , 'Toe%sIkStrt%sMdv' %( elem , side ))
            self.toeStrtMdv.attr('i2x').value = valueSide

            self.toeStrtPma = rt.createNode( 'plusMinusAverage' , 'Toe%sIkStrt%sPma' %( elem , side ))
            self.toeStrtPma.addAttr( 'default' )
            self.toeStrtPma.attr('default').value = self.toeIkJnt.attr('ty').value

            self.ankleIkCtrl.attr( 'toeStretch' ) >> self.toeStrtMdv.attr('i1x')
            self.toeStrtPma.attr( 'default' ) >> self.toeStrtPma.attr('i1[0]')
            self.toeStrtMdv.attr( 'ox' ) >> self.toeStrtPma.attr('i1[1]')
            self.toeStrtPma.attr( 'o1' ) >> self.toeIkJnt.attr('ty')

            self.ballIkhDict = rt.addIkh( 'Ball%s' %elem , 'ikSCsolver' , self.ankleIkJnt , self.ballIkJnt )
            self.toeIkhDict = rt.addIkh( 'Toe%s' %elem , 'ikSCsolver' , self.ballIkJnt , self.toeIkJnt )
        
            self.ballIkh = self.ballIkhDict['ikh']
            self.ballIkhZro = self.ballIkhDict['ikhZro']

            self.toeIkh = self.toeIkhDict['ikh']
            self.toeIkhZro = self.toeIkhDict['ikhZro']

            self.ankleIkhZro.clearCons()
            self.ikStrt[2].clearCons()

            mc.parentConstraint( self.ballPivCtrl , self.ankleIkhZro , mo = True )
            mc.parentConstraint( self.ballPivCtrl , self.ankleIkhSecZro , mo = True )
            mc.parentConstraint( self.ballPivCtrl , self.ballIkhZro , mo = 1 )
            mc.parentConstraint( self.bendPivCtrl , self.toeIkhZro , mo = True )
            mc.parentConstraint( self.anklePivGrp , self.ikStrt[2] , mo = True )

            self.ankleIkCtrlShape = core.Dag(self.ankleIkCtrl.shape)

            self.ankleIkCtrl.addTitleAttr( 'Foot' )
            self.ankleIkCtrl.addAttr( 'ballRoll' )
            self.ankleIkCtrl.addAttr( 'heelRoll' )
            self.ankleIkCtrl.addAttr( 'toeRoll' )
            self.ankleIkCtrl.addAttr( 'heelTwist' )
            self.ankleIkCtrl.addAttr( 'toeTwist' )
            self.ankleIkCtrl.addAttr( 'toeBend' )
            self.ankleIkCtrl.addAttr( 'toeSide' )
            self.ankleIkCtrl.addAttr( 'toeSwirl' )
            self.ankleIkCtrl.addAttr( 'footRock' )
            self.ankleIkCtrlShape.addAttr( 'toeBreak' )
            self.ankleIkCtrlShape.attr('toeBreak').value = -25

            self.ballBreakCmp = rt.createNode( 'clamp' , 'Ball%sIkBreak%sCmp' %( elem , side ))

            self.toeBreakCnd = rt.createNode( 'condition' , 'Toe%sIkBreak%sCnd' %( elem , side ))
            self.toeBreakAmpMdv = rt.createNode( 'multiplyDivide' , 'Toe%sIkBreakAmp%sMdv' %( elem , side ))
            self.toeBreakAmpPma = rt.createNode( 'plusMinusAverage' , 'Toe%sIkBreakAmp%sPma' %( elem , side ))
            self.toeBreakPma = rt.createNode( 'plusMinusAverage' , 'Toe%sIkBreak%sPma' %( elem , side ))
            self.toeBreakCnd.attr('operation').value = 2
            self.toeBreakAmpMdv.attr('i2x').value = -1
            self.toeBreakCnd.attr('cfr').value = 0

            self.heelBreakCmp = rt.createNode( 'clamp' , 'Heel%sIkBreak%sCmp' %( elem , side ))
            self.heelBreakPma = rt.createNode( 'plusMinusAverage' , 'Heel%sIkBreak%sPma' %( elem , side ))
            self.heelBreakCmp.attr('maxR').value = 100

            self.ankleIkCtrl.attr( 'ballRoll' ) >> self.ballBreakCmp.attr('inputR')
            self.ankleIkCtrlShape.attr( 'toeBreak' ) >> self.ballBreakCmp.attr('minR')
            self.ankleIkCtrlShape.attr( 'toeBreak' ) >> self.toeBreakAmpMdv.attr('i1x')
            self.toeBreakAmpMdv.attr( 'ox' ) >> self.toeBreakAmpPma.attr('i1[0]')
            self.toeBreakAmpPma.attr( 'o1' ) >> self.toeBreakCnd.attr('ctr')
            self.ankleIkCtrlShape.attr( 'toeBreak' ) >> self.toeBreakCnd.attr('ft')
            self.toeBreakCnd.attr( 'ocr' ) >> self.toeBreakPma.attr('i1[0]')
            self.toeBreakPma.attr( 'o1' ) >> self.toePivGrp.attr('rx')
            self.ankleIkCtrl.attr( 'ballRoll' ) >> self.heelBreakCmp.attr('inputR')
            self.heelBreakCmp.attr( 'outputR' ) >> self.heelBreakPma.attr('i1[0]')
            self.heelBreakPma.attr( 'o1' ) >> self.heelPivGrp.attr('rx')



            #self.ankleIkCtrl.attr( 'ballRoll' ) >> self.ballPivGrp.attr('rx')
            self.ankleIkCtrl.attr( 'heelRoll' ) >> self.heelPivGrp.attr('rx')
            self.ankleIkCtrl.attr( 'toeRoll' ) >> self.toePivGrp.attr('rx')
            self.ankleIkCtrl.attr( 'heelTwist' ) >> self.heelTwistPivGrp.attr('rz')
            self.ankleIkCtrl.attr( 'toeTwist' ) >> self.toeTwistPivGrp.attr('rz')
            self.ankleIkCtrl.attr( 'toeBend' ) >> self.bendPivGrp.attr('rx')
            self.ankleIkCtrl.attr( 'toeSide' ) >> self.bendPivGrp.attr('rz')
            self.ankleIkCtrl.attr( 'toeSwirl' ) >> self.bendPivGrp.attr('ry')
            # self.ankleIkCtrl.attr( 'footRock' ) >> self.inPivGrp.attr('ry')
            # self.ankleIkCtrl.attr( 'footRock' ) >> self.outPivGrp.attr('ry')
            
            mc.transformLimits( self.inPivGrp , ery = ( True , False ) , ry = ( 0 , 45 ))
            mc.transformLimits( self.outPivGrp , ery = ( False , True ) , ry = ( -45 , 0 ))

            # create smart control
            self.smartToeCtrl = rt.createCtrl( 'Foot%sSmartIk%sCtrl' %( elem , side ) , 'cube' , 'red' , jnt = 0 )
            self.smartToeZro = rt.addGrp( self.smartToeCtrl )
            self.smartToeOfst = rt.addGrp( self.smartToeCtrl , 'Ofst' )
            
            self.smartToeZro.snapOrient(self.ballFkJnt)
            if side =='_R_':
                self.smartToeZro.pynode().sx.set(-1)
                self.smartToeZro.pynode().sy.set(-1)
            self.smartToeCtrl.pynode().tz.set(l=1,k=0)
            self.smartToeCtrl.pynode().v.set(l=1,k=0)
            for ax in 'xyz':
                self.smartToeCtrl.pynode().attr('r%s'%ax).set(l=1,k=0)
                self.smartToeCtrl.pynode().attr('s%s'%ax).set(l=1,k=0)

            # create math utility node for smart toe system
            self.smartAmp_mdv = rt.createNode('multiplyDivide', 'Foot%sSmartIkAmp%sMdv' %( elem , side ) )
            self.smartToeCtrl.getShape().addAttr('footSmartAmp',k=1,dv=5)

            self.ball_pma =  rt.createNode( 'plusMinusAverage' , 'Ball%sSmartIk%sPma' %( elem , side ))
            self.footRock_pma = rt.createNode( 'plusMinusAverage' , 'FootRock%sSmartIk%sPma' %( elem , side ))
            self.smart_mdv = rt.createNode( 'multiplyDivide' , 'Foot%sSmartIk%sMdv' %( elem , side ))

            # do smart toe rock
            # connect smart control to smart mdv
            self.smartToeCtrl.attr('ty') >> self.smart_mdv.attr('input1X')
            # set smart mdv attr
            self.smartToeCtrl.getShape().attr('footSmartAmp') >> self.smartAmp_mdv.pynode().input1X
            self.smartAmp_mdv.pynode().input2X.set(1)
            self.smartAmp_mdv.pynode().outputX >> self.smart_mdv.pynode().attr('input2X')
            #self.smart_mdv.pynode().attr('input2X').set(9)
            
            # connect to smart ball
            self.smart_mdv.attr('outputX') >>  self.ball_pma.attr('input1D[0]')
            self.ankleIkCtrl.attr('ballRoll') >>  self.ball_pma.attr('input1D[1]')

            # create ball utility pma
            self.ball_mdv = rt.createNode( 'multiplyDivide' , 'Ball%sSmartIk%sMdv' %( elem , side ))
            self.ball_pma.attr('output1D') >>  self.ball_mdv.attr('input1X')
            self.ball_mdv.pynode().attr('input2X').set(-1)

            # connect to ballPivGrp and Toe Break system
            self.ball_mdv.attr('outputX')  >> self.ballPivGrp.attr('rx')
            self.ball_mdv.attr('outputX')  >> self.heelBreakPma.attr('input1D[2]')
            self.ball_mdv.attr('outputX')  >> self.toeBreakAmpPma.attr('input1D[1]')
            self.ball_mdv.attr('outputX')  >> self.toeBreakCnd.attr ('secondTerm')
            

            # do smart foot rock
            #connect tx of smart ctrl to footRock_pma
            self.smartToeCtrl.getShape().attr('footSmartAmp') >> self.smartAmp_mdv.pynode().input1Y
            self.smartAmp_mdv.pynode().input2Y.set(1)


            self.smartAmp_mdv.pynode().outputY >> self.smart_mdv.pynode().attr('input2Y')
            #self.smart_mdv.pynode().attr('input2Y').set(-9)
            self.smartToeCtrl.attr('tx') >>  self.footRock_pma.attr('input1D[0]')
            self.ankleIkCtrl.attr('footRock') >>  self.footRock_pma.attr('input1D[1]')
            self.footRock_pma.attr('output1D') >> self.smart_mdv.attr('input1Y')
            self.smart_mdv.attr('outputY') >>  self.inPivGrp.attr('ry')
            self.smart_mdv.attr('outputY') >>  self.outPivGrp.attr('ry')

            mc.transformLimits( self.inPivGrp , ery = ( True , False ) , ry = ( 0 , 0 ))
            mc.transformLimits( self.outPivGrp , ery = ( False , True ) , ry = ( 0 , 0 ))

            mc.transformLimits( self.toePivGrp , erx = ( False , True ) , rx = ( 0 , 0 ))
            mc.transformLimits( self.heelPivGrp , erx = ( True , False ) , rx = ( 0 , 0 ))

            ankle_t = mc.xform(ankleTmpJnt,ws=1,q=1,t=1)
            ball_t = mc.xform(ballTmpJnt,ws=1,q=1,t=1)

            ankleVec = om.MVector(ankle_t[0],ankle_t[1],ankle_t[2])

            ballVec = om.MVector(ball_t[0],ball_t[1],ball_t[2])

            newVec = ballVec- ankleVec

            mc.xform(self.smartToeZro.name,ws=1,t=[newVec[0]+ankle_t[0],-newVec[1],newVec[2]])

            self.smartToeZro.parent(self.ankleIkGmbl)

        else :
            mc.parentConstraint( self.ankleIkGmbl , self.ankleIkhSecZro , mo = True )

        #-- Adjust Hierarchy Ik
        mc.parent( self.legIkCtrlGrp , self.legCtrlGrp )
        mc.parent( self.legIkJntGrp , self.legJntGrp )
        mc.parent( self.legIkIkhGrp , ikhGrp )
        mc.parent( self.upLegIkZro , self.ikStrt , self.crv , self.legIkCtrlGrp )
        mc.parent( self.ankleIkZro , self.kneeIkZro , self.upLegIkGmbl )
        mc.parent( self.ankleIkhZro , self.ankleIkhSecZro , self.legIkIkhGrp )
        mc.parent( self.lowLegIkh , self.anklePivot )
        mc.parent( self.anklePivot , self.lowLegIkhZro )
        mc.parent( self.lowLegIkhZro , self.legIkIkhGrp )

        if foot == True :
            mc.parent( self.footPivGrp , self.ankleIkGmbl )
            mc.parent( self.ballIkhZro , self.toeIkhZro , self.legIkIkhGrp )

        #-- Blending Fk Ik 
        rt.blendFkIk( self.legCtrl , self.upLegFkJnt , self.upLegIkJnt , self.upLegJnt )
        rt.blendFkIk( self.legCtrl , self.midLegFkJnt , self.midLegIkJnt , self.midLegJnt )
        rt.blendFkIk( self.legCtrl , self.lowLegFkJnt , self.lowLegIkJnt , self.lowLegJnt )
        rt.blendFkIk( self.legCtrl , self.ankleFkJnt , self.ankleIkJnt , self.ankleJnt )
        
        if foot == True :
            rt.blendFkIk( self.legCtrl , self.ballFkJnt , self.ballIkJnt , self.ballJnt )
            rt.blendFkIk( self.legCtrl , self.toeFkJnt , self.toeIkJnt , self.toeJnt )
        
        if mc.objExists( 'Leg%sFkIk%sRev' %( elem , side )) :
            self.revFkIk = core.Dag('Leg%sFkIk%sRev' %( elem , side ))

            self.legCtrl.attr( 'fkIk' ) >> self.legIkCtrlGrp.attr('v')
            self.revFkIk.attr( 'ox' ) >> self.legFkCtrlGrp.attr('v')

            self.legCtrl.attr('fkIk').value = 1

        #-- Scale Foot
        if foot == True :
            self.legCtrl.addAttr('footScale')
            self.legCtrl.attr('footScale').value = 1
            self.ballJnt.attr('segmentScaleCompensate').value = 0
            self.ballFkJnt.attr('segmentScaleCompensate').value = 0
            self.ballIkJnt.attr('segmentScaleCompensate').value = 0

            self.legCtrl.attr('footScale') >> self.ballFkSclOfst.attr('sx')
            self.legCtrl.attr('footScale') >> self.ballFkSclOfst.attr('sy')
            self.legCtrl.attr('footScale') >> self.ballFkSclOfst.attr('sz')

            self.legCtrl.attr('footScale') >> self.ankleFkJnt.attr('sx')
            self.legCtrl.attr('footScale') >> self.ankleFkJnt.attr('sy')
            self.legCtrl.attr('footScale') >> self.ankleFkJnt.attr('sz')

            self.legCtrl.attr('footScale') >> self.footPivGrp.attr('sx')
            self.legCtrl.attr('footScale') >> self.footPivGrp.attr('sy')
            self.legCtrl.attr('footScale') >> self.footPivGrp.attr('sz')

            self.legCtrl.attr('footScale') >> self.ankleIkJnt.attr('sx')
            self.legCtrl.attr('footScale') >> self.ankleIkJnt.attr('sy')
            self.legCtrl.attr('footScale') >> self.ankleIkJnt.attr('sz')

        #-- Cleanup Fk
        for grp in ( self.legFkCtrlGrp , self.legFkJntGrp , self.upLegFkZro , self.upLegFkOfst , self.lowLegFkZro , self.lowLegFkOfst , self.ankleFkZro , self.ankleFkOfst ) :
            grp.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
        
        for ctrl in ( self.upLegFkCtrl , self.upLegFkGmbl , self.lowLegFkCtrl , self.lowLegFkGmbl , self.ankleFkCtrl , self.ankleFkGmbl ) :
            ctrl.lockHideAttrs( 'sx' , 'sy' , 'sz' , 'v' )
            
        if foot == True :
            for grp in ( self.ballFkZro , self.ballFkOfst , self.ballFkSclOfst ) :
                grp.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
                
            for ctrl in ( self.ballFkCtrl , self.ballFkGmbl ) :
                ctrl.lockHideAttrs( 'sx' , 'sy' , 'sz' , 'v' )

        #-- Cleanup Ik
        for grp in ( self.legIkCtrlGrp , self.legIkJntGrp , self.legIkIkhGrp , self.upLegIkZro , self.ankleIkZro ) :
            grp.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
        
        for ctrl in ( self.upLegIkCtrl , self.upLegIkGmbl , self.kneeIkCtrl , self.ankleIkCtrl , self.ankleIkGmbl ) :
            ctrl.lockHideAttrs( 'sx' , 'sy' , 'sz' , 'v' )
            
        if foot == True :
            for grp in ( self.footPivGrp , self.ballPivGrp , self.bendPivGrp , self.heelPivGrp , self.heelTwistPivGrp , self.toePivGrp , self.toeTwistPivGrp , self.inPivGrp , self.outPivGrp ) :
                grp.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        #-- Ribbon
        if ribbon == True :
            self.upLegPar   = self.upLegJnt.getParent()
            self.midLegPar  = self.midLegJnt.getParent()
            self.lowLegPar  = self.lowLegJnt.getParent()
            self.anklePar   = self.ankleJnt.getParent()

            #-- Create Controls Ribbon
            self.upKneeRbnCtrl = rt.createCtrl( 'UpKnee%sRbn%sCtrl' %( elem , side ) , 'locator' , 'yellow' , jnt = True )
            self.upKneeRbnZro = rt.addGrp( self.upKneeRbnCtrl )
            self.upKneeRbnZro.snapPoint( self.midLegJnt )
            self.upKneeRbnCtrl.snapJntOrient( self.midLegJnt )
            mc.parent( self.upKneeRbnZro , self.legCtrlGrp )

            self.lowKneeRbnCtrl = rt.createCtrl( 'LowKnee%sRbn%sCtrl' %( elem , side ) , 'locator' , 'yellow' , jnt = True )
            self.lowKneeRbnZro = rt.addGrp( self.lowKneeRbnCtrl )
            self.lowKneeRbnZro.snapPoint( self.lowLegJnt )
            self.lowKneeRbnCtrl.snapJntOrient( self.lowLegJnt )
            mc.parent( self.lowKneeRbnZro , self.legCtrlGrp )

            #-- Adjust Shape Controls Ribbon
            self.upKneeRbnCtrl.scaleShape( size )
            self.lowKneeRbnCtrl.scaleShape( size )

            #-- Rig process Ribbon
            mc.parentConstraint( self.midLegJnt , self.upKneeRbnZro , mo = True )
            mc.parentConstraint( self.lowLegJnt , self.lowKneeRbnZro , mo = True )

            distRbnUpLeg = rt.getDist( self.upLegJnt , self.midLegJnt )
            distRbnMidLeg = rt.getDist( self.midLegJnt , self.lowLegJnt )
            distRbnLowLeg = rt.getDist( self.lowLegJnt , self.ankleJnt )

            if hi:
                self.rbnUpLeg = ribbonRig.RibbonHi( name = 'UpLeg%s' %elem , 
                                                    axis = 'y-' ,
                                                    side = sideRbn ,
                                                    dist = distRbnUpLeg )

                self.rbnMidLeg = ribbonRig.RibbonHi( name = 'MidLeg%s' %elem , 
                                                     axis = 'y-' ,
                                                     side = sideRbn ,
                                                     dist = distRbnMidLeg )
                
                self.rbnLowLeg = ribbonRig.RibbonHi( name = 'LowLeg%s' %elem , 
                                                     axis = 'y-' ,
                                                     side = sideRbn ,
                                                     dist = distRbnLowLeg )
            else:
                self.rbnUpLeg = ribbonRig.RibbonMid( name = 'UpLeg%s' %elem , 
                                                    axis = 'y-' ,
                                                    side = sideRbn ,
                                                    dist = distRbnUpLeg )

                self.rbnMidLeg = ribbonRig.RibbonMid( name = 'MidLeg%s' %elem , 
                                                     axis = 'y-' ,
                                                     side = sideRbn ,
                                                     dist = distRbnMidLeg )
                
                self.rbnLowLeg = ribbonRig.RibbonMid( name = 'LowLeg%s' %elem , 
                                                     axis = 'y-' ,
                                                     side = sideRbn ,
                                                     dist = distRbnLowLeg )

            self.rbnUpLeg.rbnCtrlGrp.snapPoint(self.upLegJnt)
    
            mc.delete( 
                           mc.aimConstraint( self.midLegJnt , self.rbnUpLeg.rbnCtrlGrp , 
                                             aim = (0,-1,0) ,
                                             u = upVec ,
                                             wut='objectrotation' ,
                                             wuo = self.upLegJnt ,
                                             wu= (0,0,1))
                     )
    
            mc.parentConstraint( self.upLegJnt , self.rbnUpLeg.rbnCtrlGrp , mo = True )
            mc.parentConstraint( self.upLegJnt , self.rbnUpLeg.rbnRootCtrl , mo = True )
            mc.parentConstraint( self.upKneeRbnCtrl , self.rbnUpLeg.rbnEndCtrl , mo = True )

            for ctrl in ( self.rbnUpLeg.rbnRootCtrl , self.rbnUpLeg.rbnEndCtrl) :
                ctrl.lockHideAttrs('tx', 'ty', 'tz' , 'rx' , 'ry' , 'rz' )
                ctrl.hide()

            self.rbnMidLeg.rbnCtrlGrp.snapPoint(self.midLegJnt)
    
            mc.delete( 
                           mc.aimConstraint( self.lowLegJnt , self.rbnMidLeg.rbnCtrlGrp , 
                                             aim = (0,-1,0) ,
                                             u = upVec ,
                                             wut='objectrotation' ,
                                             wuo = self.midLegJnt ,
                                             wu= (0,0,1))
                     )
    
            mc.parentConstraint( self.midLegJnt , self.rbnMidLeg.rbnCtrlGrp , mo = True )
            mc.parentConstraint( self.upKneeRbnCtrl , self.rbnMidLeg.rbnRootCtrl , mo = True )
            mc.parentConstraint( self.lowKneeRbnCtrl , self.rbnMidLeg.rbnEndCtrl , mo = True )

            for ctrl in ( self.rbnMidLeg.rbnRootCtrl , self.rbnMidLeg.rbnEndCtrl) :
                ctrl.lockHideAttrs('tx', 'ty', 'tz' , 'rx' , 'ry' , 'rz' )
                ctrl.hide()

            self.rbnLowLeg.rbnCtrlGrp.snapPoint(self.lowLegJnt)
    
            mc.delete( 
                           mc.aimConstraint( self.ankleJnt , self.rbnLowLeg.rbnCtrlGrp , 
                                             aim = (0,-1,0) ,
                                             u = upVec ,
                                             wut='objectrotation' ,
                                             wuo = self.lowLegJnt ,
                                             wu= (0,0,1))
                     )
    
            mc.parentConstraint( self.lowLegJnt , self.rbnLowLeg.rbnCtrlGrp , mo = True )
            mc.parentConstraint( self.lowKneeRbnCtrl , self.rbnLowLeg.rbnRootCtrl , mo = True )
            mc.parentConstraint( self.ankleJnt , self.rbnLowLeg.rbnEndCtrl , mo = True )

            for ctrl in ( self.rbnLowLeg.rbnRootCtrl , self.rbnLowLeg.rbnEndCtrl) :
                ctrl.lockHideAttrs('tx', 'ty', 'tz' , 'rx' , 'ry' , 'rz' )
                ctrl.hide()
    
            #-- Adjust Ribbon
            self.rbnUpLegCtrlShape = core.Dag(self.rbnUpLeg.rbnCtrl.shape)
            self.rbnMidLegCtrlShape = core.Dag(self.rbnMidLeg.rbnCtrl.shape)
            self.rbnLowLegCtrlShape = core.Dag(self.rbnLowLeg.rbnCtrl.shape)

            self.rbnUpLegCtrlShape.attr('rootTwistAmp').value = rootTwsAmp
            self.rbnUpLegCtrlShape.attr('endTwistAmp').value = endTwsAmp
            self.rbnMidLegCtrlShape.attr('rootTwistAmp').value = rootTwsAmp
            self.rbnMidLegCtrlShape.attr('endTwistAmp').value = endTwsAmp
            self.rbnLowLegCtrlShape.attr('rootTwistAmp').value = rootTwsAmp
            self.rbnLowLegCtrlShape.attr('endTwistAmp').value = endTwsAmp
                           
            #-- Twist Distribute Ribbon
            self.upLegNonRollTwistGrp.attr('ry') >> self.rbnUpLegCtrlShape.attr('rootAbsoluteTwist')
            self.lowLegJnt.attr('ry') >> self.rbnMidLegCtrlShape.attr('endAbsoluteTwist')
            self.ankleJnt.attr('ry') >> self.rbnLowLegCtrlShape.attr('endAbsoluteTwist')

            #-- Adjust Hierarchy Ribbon
            mc.parent( self.rbnUpLeg.rbnCtrlGrp , self.rbnMidLeg.rbnCtrlGrp , self.rbnLowLeg.rbnCtrlGrp , self.legCtrlGrp )
            mc.parent( self.rbnUpLeg.rbnJntGrp , self.rbnMidLeg.rbnJntGrp , self.rbnLowLeg.rbnJntGrp , self.legJntGrp )
            mc.parent( self.rbnUpLeg.rbnSkinGrp , self.rbnMidLeg.rbnSkinGrp , self.rbnLowLeg.rbnSkinGrp , skinGrp )
            mc.parent( self.rbnUpLeg.rbnStillGrp , self.rbnMidLeg.rbnStillGrp , self.rbnLowLeg.rbnStillGrp , stillGrp )
        
            self.rbnUpLeg.rbnCtrl.attr('autoTwist').value = 1
            self.rbnMidLeg.rbnCtrl.attr('autoTwist').value = 1
            self.rbnLowLeg.rbnCtrl.attr('autoTwist').value = 1

        mc.select( cl = True )