from nuTools.util import matrixConstraint as mtcon
reload(mtcon)
import pymel.core as pm

# convert constraints into matrix constraint
old_constraints = mtcon.convertToMatrixConstraint(objs=pm.ls(type='transform'))

# delete old constraints
pm.delete(old_constraints)