import maya.cmds as mc
import maya.mel as mm 
from utaTools.pkmel import core as pc
reload(pc)
import shutil 
import os
import pymel.core as pm
from rf_utils.context import context_info
# reload(context_info)
from lpRig import mainGroup
reload(mainGroup)
from utaTools.utapy import DeleteGrp
from utaTools.pkmel import rigTools
from utaTools.utapy import listControlVis as lcv
reload(lcv)
from utaTools.utapy import listControlAmpFcl as lcaf
reload(lcaf)
from lpRig import core as lrc
reload(lrc)
from utaTools.pkmel import weightTools
reload(weightTools)
from lpRig import rigTools as lrr
reload(lrr)

#snap obj to obj
def snap(*args):
    sels =mc.ls(sl=True)
    mean =mc.parentConstraint(sels[0],sels[1],mo=False)
    means=mc.delete (mean)

def snapSel(obj1 = '', obj2= '',*args):
    mean =mc.parentConstraint(obj1,obj2,mo=False)
    means=mc.delete (mean)    

def jointFromLocator (*args):
    #poseLocator = locator
    #sels = obj
    sels =mc.ls(sl=True)                                # select object selected
    for each in sels:                                   # search in selected
        poseLocator = mc.xform(each, q=True, t=True)    # find xform (x,y,z) 
        jntFrLoc = mc.joint (each, p=(poseLocator[0],poseLocator[1],poseLocator[2]))  # create joint from poseLocator(x,y,z)
        mean = mc.parent (jntFrLoc, w=True) 
        changName = each.split('_')                     # split '_'
        mc.rename(jntFrLoc, '%s_Jnt' % changName[0])    # chang new name obj               
        deleats = mc.delete (each)                      # delete object from selected each

        # print changName 

        # print mean  

def function(*args):
    # passdynamicHair ():
    #poseLocator = locator
    #sels = obj
    sels =mc.ls(sl=True)                                # select object selected
    for each in sels:                                   # search in selected
        poseLocator = mc.xform(each, q=True, t=True)    # find xform (x,y,z) 
        jntFrLoc = mc.joint (each, p=(poseLocator[0],poseLocator[1],poseLocator[2]))  # create joint from poseLocator(x,y,z)
        mean = mc.parent (jntFrLoc, w=True)             # unparent from w=world  mean unparent
        deleats = mc.delete (each)                      # delete object from selected each
        print mean 

def zeroGroup( obj = '' ,*args ) :
    # Create zero group
    chld = pc.Dag( obj )
    grp = pc.Null()
    grp.snap( chld )
    chld.parent( grp )
    return grp

def doZroGroup (*args):
    # Create zero group with naming
    sels = mc.ls( sl=True )
    sides = ( 'LFT' , 'RGT' , 'UPR' , 'LWR' )
    zroGrps = []
    
    for sel in sels :
        curr = pc.Dag( sel )
        prnt = curr.getParent()
        zGrp = zeroGroup( sel )
        
        nameLst = curr.name.split( '_' )
        currType = nameLst[-1]
        charName = ''
        midName = ''
        currSide = ''
        
        if len( nameLst ) == 3 :
            charName = '%s_' % nameLst[0]
            midName = nameLst[1]
        else :
            midName = nameLst[0]
        
        for side in sides :
            if side in midName :
                currSide = side
                midName = midName.replace( side , '' )
        
        zGrp.name = '%s%s%s%sZro%s_Grp' % ( charName ,
                                            midName ,
                                            currType[0].upper() ,
                                            currType[1:] ,
                                            currSide
                                        )
        
        if prnt :
                zGrp.parent( prnt )
            # print 'aaaa'
        
        zroGrps.append( zGrp )
    
        return zroGrps

def shadeCoppy (*args):
    sels =mc.ls(sl=True,l=True,nt=True,ap=True)
    print sels
  #for each in sels:
      #posi = mc.group (each,em=True)

def selected (*args):
    sels =mc.ls(sl=True,l=True,visible=True)
    print sels
    
def gmbl(obj,*args):
    sels = mc.ls(sl=True)
    for each in sels:   
        each.add( ln='gimbalControl' , min=0 , max=1 , k=True )
      # print (nameFun)
      # tag = mc.attributeInfo( 'geoVis', ui=True, h=True, i=True )
# def geoVis(obj):
#     sels = mc.ls(sl=True)
#     if sels = '' :
#         sels = obj
#     else:
#     for each in sels:   
#         each.add( ln='geoVis' , min=0 , max=1 , k=True )
#         print (nameFun)
#         tag = mc.attributeInfo( 'geoVis', ui=True, h=True, i=True )
# def dupu():
#     sels =mc.ls(sl=True)
#     for sel in sels:
#         let = mc.listConnections(sel)
#         for make in let:
#             nameList = mc.setAttr (let[0]) 10
#             print let
def attrEnum(obj = '', ln = '', en ='',*args):
    mc.addAttr( obj , ln = ln , at = 'enum' , en = en , k = True )

def attrNameTitle(obj = '', ln = '',*args):
    mc.addAttr( obj , ln = ln , at = 'enum' , k = True )
    mc.setAttr('%s.%s' % (obj, ln), l = True)

def attrName(obj = '', elem = '', *args):

    if not obj:
        sels = mc.ls(sl=True)
        obj = sels
        if mc.objExists(elem == ''):
            mc.addAttr(obj,ln = 'gimbalControl' , k = True)
        else:
            mc.addAttr(obj,ln = elem , k = True)

    else:

        if mc.objExists(elem == ''):
            mc.addAttr(obj, ln= 'gimbalControl', k=True)
        else:
            mc.addAttr(obj,ln = elem , k = True)

def attrNameMinMax (obj = '', elem = '', min = 0, max = 1, *args):

    if not mc.objExists(obj):
        sels = mc.ls(sl=True)
        obj = sels
        if not mc.objExists(elem):
            mc.addAttr(obj,ln = 'gimbalControl' , at = 'long', min = min, max = max , k = True)
        else:
            mc.addAttr(obj,ln = elem  , at = 'long', min = min, max = max, k = True)

    else:

        if mc.objExists(elem):
            mc.addAttr(obj, ln= 'gimbalControl', at = 'long', min = min, max = max, k=True)
        else:
            mc.addAttr(obj,ln = elem ,at = 'long', min = min , max = max, k = True)
    # if mc.objExists(ln):
    #     for each in sels:   
    #         each.add( ln='gimbalControl' , min=0 , max=1 , k=True )
    # else:
    #     for each in sels:
            
    #         each.add( ln= elem , min=0, max=1, k=True )
    #         print each

#find and delete keyFrame and vraySetting      
def clearKey(*args):
# select target1
    cur = mc.select(all=True)
# currentTime
    mc.currentTime (0)
# delete keyFrame from 0 to 2000
    mc.cutKey(time=(-200,2000)) 
    mc.delete ('vraySettings')
    mc.select(cl=True)


def cleanCamera(*args):  
    # clean Camera fot default
    cams = mc.ls(sl=True)
    for cam in cams:
        mc.camera(cam,e=True,startupCamera=False)
        mc.delete(cam)
        print 'delete %s' %(cam)
    
def chang4Kto1K(*args):
    #P-----------------------------------Find Texture 4K chang to 1K----------------------------
    files = mc.ls('*_aFile')
    for each in files : 
        path = mc.getAttr('%s.fileTextureName' % each)
        print path
    if '/4K/' in path : 
        newPath = path.replace('/4K/', '/1K/')
        mc.setAttr('%s.fileTextureName' % each, newPath, type = 'string')
        print 'set %s' % each

#Expression
#     #ES--------------------------------code  loop trainStaion -------------------------------------------------
#     railwayMoveIk_ikh.offset = trainStationOffset_ctrl.trainOffset*0.001;
#     int $round = railwayMoveIk_ikh.offset;
#     if (railwayMoveIk_ikh.offset > 1)
#         railwayMoveIk_ikh.offset = ((trainStationOffset_ctrl.trainOffset*0.001)-$round);

#     railwayMoveIk_ikh.offset = trainStationbogie1_loc.offset*0.001;
#     int $round = railwayMoveIk_ikh.offset;
#     if (railwayMoveIk_ikh.offset > 1)
#         railwayMoveIk_ikh.offset = ((trainStationbogie1_loc.offset*0.001)-$round);

def eyeLidFollow(*args):
    eyeList = mc.ls('*:EyeLFT_Jnt','*:EyeRGT_Jnt')
    eyeLidsList = mc.ls('*:EyeLidsBsh_L_Jnt', '*:EyeLidsBsh_R_Jnt')
    eyeCmp = mc.ls('*:EyeBallFollowIO_L_Cmp', '*:EyeBallFollowIO_R_Cmp',)
    eyelidFol =mc.ls('*:EyeLFT_Ctrl', '*:EyeRGT_Ctrl')

    mc.parentConstraint(eyeList[0],eyeLidsList[0], mo=True)
    mc.parentConstraint(eyeList[1],eyeLidsList[1], mo=True)

    # set eyeLidsBsh
    mc.setAttr('%s.eyelidsFollow' % eyeLidsList[0], 1)
    mc.setAttr('%s.eyelidsFollow' % eyeLidsList[1], 1)

    # set lidFollow
    mc.setAttr('%s.lidFollow' % eyelidFol[0], 0)
    mc.setAttr('%s.lidFollow' % eyelidFol[1], 0)

    # L
    #mc.setAttr('%s.minG' % eyeCmp[0], -300)
    #mc.setAttr('%s.maxR' % eyeCmp[0], 300)
    # R
    #mc.setAttr('%s.minR' % eyeCmp[1], -300)
    #mc.setAttr('%s.maxG' % eyeCmp[1], 300)

def getUserFromIp(*args):
    """
    Get user name from data base (config) using the current machine ip.
        return: User name(str)
    """

    ipAddress = socket.gethostbyname(socket.gethostname())
    try :
        user = config.USER_IP[str(ipAddress)]
    except:
        user = 'ken'
    return "%s : %s" % (user, ipAddress)

def lockAttrs(listObj, lock,*args):
    # listEyes = ['EyeLidsBsh_L_Jnt','EyeLidsBsh_R_Jnt']
    attrs = ['t', 'r', 's' , 'eyelidsFollow']
#attrEyeLids = ["eyelidsFollow"]
    for each in listObj:
        if mc.objExists(each):
            for eyeJnt in listObj:
                for attr in attrs:
                    mc.setAttr( "%s.%s" %(eyeJnt, attr), lock = True)
                    #mc.setAttr('%s.v' %eyeJnt ,lock = 0)

def lockHideAttr(listObj, lock, keyable,*args):
    # listEyes = ['EyeLidsBsh_L_Jnt','EyeLidsBsh_R_Jnt']
    attrs = ['t', 'r', 's', 'v', 'squash', 'stretch', 'eyelidsFollow']
#attrEyeLids = ["eyelidsFollow"]
    for each in listObj:
        if mc.objExists(each):
            for eyeJnt in listObj:
                for each in attrs:
                    if mc.objExists(each):
                        for attr in attrs:
                            mc.setAttr( "%s.%s" %(eyeJnt, attr), lock = lock, keyable = keyable)
                            #mc.setAttr('%s.v' %eyeJnt ,lock = 0)

# unlock mdv Node
def lockMdv(listObj, lock,*args):
    attrsMdv = ['i1x', 'i1y', 'i1z', 'i2x' , 'i2y', 'i2z']
    for each in listObj:
        if mc.objExists(each):
            for listObjs in listObj:
                for attrsMdvs in attrsMdv:
                    mc.setAttr( "%s.%s" %(listObjs, attrsMdvs), lock = lock)
                    #mc.setAttr('%s.v' %listMdvs ,lock = 0)

def lockAttrObj(listObj, lock = True,*args):
    # listBow = ("bowARig_lft_grp","bowBRig_rgt_grp")
    i = 0 
    for each in listObj:
        for attr in ( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v') :
            for i in range(len(listObj)):
                    mc.setAttr("%s.%s" % (each, attr) , lock = lock, k = False)
        i += 1  

def lockAttr(listObj, attrs = [],lock = True,keyable = True,*args):
    # listBow = ("bowARig_lft_grp","bowBRig_rgt_grp")
    # attrs = ( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v')
    i = 0 
    for each in listObj:
        for attr in  attrs:
            for i in range(len(listObj)):
                mc.setAttr("%s.%s" % (each, attr) , lock = lock, keyable = keyable)
        i += 1  

# def attrFunction(listObj = [], attrs = [], lock , hide , *args):
#     # listBow = ("bowARig_lft_grp","bowBRig_rgt_grp")
#     # attrs = ( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v')
#     i = 0 
#     for each in listObj:
#         for attr in  attrs:
#             for i in range(len(listObj)):
#                 mc.setAttr("%s.%s" % (each, attr) , lock = lock, hide = hide)
#         i += 1  

def unlockAttrs(listObj = [], lock = True, attrs = '', *args):
    # listBow = ("bowARig_lft_grp","bowBRig_rgt_grp")
    # #attr = ( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v')
    for each in listObj:
        for attr in attrs :
            for i in range(len(listObj)):
                mc.setAttr("%s.%s" % (each, attr) , lock = lock)

def unInfluences (listObj,*args):
    # listObj = mc.ls("*:*_ply", "*:*_geo")
    # listObj = mc.ls("*_ply", "*_geo")
    # print listObj
    for each in listObj:
        if mc.objExists(each):
            showInfluence = mc.skinCluster(each, removeInfluence=True)
            print showInfluence
            # skinList = 
            # skinJntList = 
            for each in skinList:
                for skinJntLists in skinJntList:
                    mc.skinCluster (each , ri= skinJntLists , e=True )
    print ">>> clear <<<"

def getDataFld(*args) :      
    wfn = os.path.normpath( pc.sceneName() )
    tmpAry = wfn.split( '\\' )
    tmpAry[-2] = 'data' 
    dataFld = '\\'.join( tmpAry[0:-1] ) 
    if not os.path.isdir( dataFld ) :
        os.mkdir( dataFld ) 
        #print (dataFld)
    return dataFld     
def copyObjFile(src,*args):
    shutil.copy(src, getDataFld())

def clean ( target = '' , *args ) :
    sels = mc.ls(sl=True)
    selsList = len(sels)
    if mc.objExists (target):
        # freeze transform , delete history , center pivot
        pm.makeIdentity ( target , apply = True ) ;    
        pm.delete ( target , ch = True ) ;
        pm.xform ( target , cp = True ) ;
    else:
        if selsList >= 1:
            target = sels
            # freeze transform , delete history , center pivot
            pm.makeIdentity ( target , apply = True ) ;    
            pm.delete ( target , ch = True ) ;
            pm.xform ( target , cp = True ) ;
        else:
            print "Don't Freeze Tranform. Please Select Object.!!"  

def fixJawMthRigTZ(*args):
    lists = mc.ls("JawMthRig_Ctrl", "JawMthRigJntTzColl_Pma")
    try:
        mc.disconnectAttr("%s.translate.translateY" % lists[0], "%s.input1D[0]" % lists[1])
        mc.connectAttr("%s.translate.translateZ" % lists[0], "%s.input1D[0]" % lists[1])
    except: pass

def parentScaleConstraint(*args):
    sels = mc.ls(sl=True)
    mc.parentConstraint(sels[0], sels[1], mo=True)
    mc.scaleConstraint(sels[0], sels[1], mo=True)

def parentScaleConstraintLoop(obj = '', lists = [],par = True, sca = True, ori = True, poi = True, *args):
    if obj:
        if par == True:
            for each in lists :
                mc.parentConstraint(obj, each, mo=True)
        if sca == True:
            for each in lists :
                mc.scaleConstraint(obj, each, mo=True)
        if ori == True:
            for each in lists :
                mc.orientConstraint(obj, each, mo=True)
        if poi == True:
            for each in lists :
                mc.pointConstraint(obj, each, mo=True)

def upperFnt(*args):
    sels = mc.ls(sl=True)
    x=0
    for i in range(len(sels)):
        ken = mc.rename(sels[i].upper())
        x+=1

def scaleObj(*args):
    sels = mc.ls(sl=True)
    addList =['scaleX', 'scaleY', 'scaleZ']

    for each in sels: 
        obj = ('%sShape' % each)  

# SearchNaming for select
def searchName(elem = '', *args):
    sels = mc.ls(sl=True)
    obj = []
    for sel in sels:
        if elem in sel:
            obj.append(sel)
            mc.select(obj)
        else:
            pass
# Run            
# searchName(elem = 'R')

def rivetToSeletedEdge(part=''):

    sels = mc.ls(sl=True, fl=True)
    rivet(part=part, fstEdge=sels[0], secEdge=sels[1])

#ConnectAttr Translate Rotate Scale
def connectTRS(trs = True, translate = True, rotate = True, scale = True, *args):
    sels = mc.ls(sl=True)
    lists = len(sels)

    if lists == 0:
        print 'Please Select Object 1 and Object 2'
    else:
        obj1 = sels[0]
        obj2 = sels[1]
        if (trs == True):
            mc.connectAttr('%s.translate' % obj1, '%s.translate' % obj2)
            mc.connectAttr('%s.rotate' % obj1, '%s.rotate' % obj2)
            mc.connectAttr('%s.scale' % obj1, '%s.scale' % obj2) 
            print obj1,'ConnectAttr T, R, S :' , obj2
        else:
            if (translate ==True):
                mc.connectAttr('%s.translate' % obj1, '%s.translate' % obj2) 
                print obj1,'ConnectAttr Translate:' , obj2
            elif (rotate ==True):
                mc.connectAttr('%s.rotate' % obj1, '%s.rotate' % obj2)  
                print obj1,'ConnectAttr Rotate:' , obj2
            elif (scale == True):
                mc.connectAttr('%s.scale' % obj1, '%s.scale' % obj2)  
                print obj1,'ConnectAttr Scale:' , obj2           


def assetName (*args):
    asset = context_info.ContextPathInfo()
    projectName = asset.path.name() if mc.file(q=True, sn=True) else ''
    nameObjs =  projectName.split('/')
    subType = nameObjs[-1]
    return subType
    # print 'SubType : ',subType

def mainGroupName (*args):
    # asset = context_info.ContextPathInfo()
    # projectName = asset.path.name() if mc.file(q=True, sn=True) else ''
    # # if maya is not projectName to pass
    #   #Exsample a = 1 if (1<0) else 2
    # print 'Creating Main Groups'
    # nameObjs =  projectName.split('/')
    # subType = nameObjs[-1]
    subType = 'Rig'
    mgObj = mainGroup.MainGroup(subType)
    print 'SubType : ',subType


def weaponRig2(jnts=['Rig:wristLFT_jnt', 'Rig:wristRGT_jnt']) :
    # Rigging weapons
    sels = mc.ls( sl=True )
    
    lHandLoc, rHandLoc = createWeaponLocator2(jnts)
    
    for sel in sels :
        
        currName = sel.split( '_' )[0]
        currName2 = currName.split ('Geo') [0]
        # print currName2
        
        ctrl = rigTools.jointControl( 'sphere' )
        ctrl.name = '%s_ctrl' % currName2
        ctrl.color = 'yellow'

        ctrl.add( ln='geoVis' , min=0 , max=1 , dv=1 , k=True )
        ctrl.attr( 'v' ).lock = True
        ctrl.attr( 'v' ).hide = True

        mc.connectAttr( '%s.geoVis' % ctrl , '%s.v' % sel )
        
        ctrlGrp = pc.group( ctrl )
        ctrlGrp.name = '%sCtrlZero_grp' % currName2
        
        # mc.skinCluster( ctrl , sel , tsb=True )
        mc.parentConstraint( ctrl , sel , mo=True )
        mc.scaleConstraint ( ctrl , sel , mo=True ) 
        
        lftGrp , rgtGrp = parentLeftRight( ctrl , lHandLoc.name , rHandLoc.name , ctrlGrp )
        lftGrp.name = '%sCtrlLeft_grp' % currName2
        rgtGrp.name = '%sCtrlRgt_grp' % currName2

        print currName2
        
def createWeaponLocator2(jnts) :
    # Creating location for mounting weapons at left hand and right hand
    lHandJnt = jnts[0]
    rHandJnt = jnts[1]

    lHand = pc.Locator()
    rHand = pc.Locator()

    poses = []
    for i in [lHandJnt,rHandJnt]:
        position = ''
        nsSplit = i.split(':')[-1]
        revElement = nsSplit.split('_')[0][::-1]

        for a in range(len(revElement)):
            if revElement[a].isupper():
                position += revElement[a]
            else:
                break
        poses.append(position[::-1])

    lHand.name = 'weapon%s_loc' %poses[0]
    rHand.name = 'weapon%s_loc' %poses[1]

    mc.delete( mc.pointConstraint( lHandJnt , lHand  ) )
    mc.delete( mc.pointConstraint( rHandJnt , rHand  ) )

    mc.parentConstraint( lHandJnt , lHand , mo=True )
    mc.scaleConstraint(lHandJnt , lHand , mo=True)
    mc.parentConstraint( rHandJnt , rHand , mo=True )
    mc.scaleConstraint(rHandJnt , rHand , mo=True)

    return lHand, rHand

    
def parentLeftRight( ctrl = '' , localObj = '' , worldObj = '' , parGrp = '' ) :
    # Blending parent between left hand and right hand.
    # Returns : locGrp , worGrp , worGrpParCons , parGrpParCons and parGrpParConsRev
    locGrp = pc.Null()
    worGrp = pc.Null()

    locGrp.snap( localObj )
    worGrp.snap( worldObj )

    worGrpParCons = pc.parentConstraint( worldObj , worGrp )
    worGrpParCons = pc.scaleConstraint( worldObj , worGrp )

    locGrpParCons = pc.parentConstraint( localObj , locGrp )
    locGrpParCons = pc.scaleConstraint( localObj , locGrp )

    parGrpParCons = pc.parentConstraint( locGrp , worGrp , parGrp )
    parGrpParCons2 = pc.scaleConstraint( locGrp , worGrp , parGrp , mo=True)
    parGrpParConsRev = pc.Reverse()

    con = pc.Dag( ctrl )

    attr = 'leftRight'
    con.add( ln = attr , k = True, min = 0 , max = 1 )
    con.attr( attr ) >> parGrpParCons.attr( 'w1' )
    con.attr( attr ) >> parGrpParConsRev.attr( 'ix' )
    parGrpParConsRev.attr( 'ox' ) >> parGrpParCons.attr( 'w0' )

    pc.clsl()

    return locGrp , worGrp  


def parentJnt (*args):
    lists = mc.ls(sl=True)
    check = lists[-1]
    objLists = len(lists)
    for idx, each in enumerate(lists):   #idx , enumerate(lists) : check index (i=0, i+=1)
        if idx+1 >= objLists:
            break
        mc.parent(lists[idx+1], each)

# Exsample  enumerate
    # my_list = ['apple', 'banana', 'grapes', 'pear']
    # for c, value in enumerate(my_list, 1):
    #     print(c, value)

# get NameSpace in scenes
def nameSpaceLists(*args):
    nsList = mc.namespaceInfo(lon = True)
    nameSp = []
    for lists in nsList:
        if lists in ['UI' , 'shared']:
            pass

        else:
            nameSp.append(lists)
    return nameSp

def getNs(nodeName=''):
    """
    Get namespace from given name
    Ex. aaa:bbb:ccc:ddd_grp > aaa:bbb:ccc:
    """
    ns = ''

    if '|' in nodeName:
        nodeName = nodeName.split('|')[-1]

    if ':' in nodeName:

        nmElms = nodeName.split(':')

        if nmElms:
            ns = '%s:' % ':'.join(nmElms[0:-1])

    return ns

def parentExportGrp(*args):
    exportGrp = 'Export_Grp'
    DeleteGrp.deleteGrp(obj = exportGrp)

def reNameCapitalize(*args):
    lists = mc.ls(sl=True)
    objLen = len(lists)
    reNameObj = []
    sameObj = []
    i = 0 
    if i <= objLen:
        for each in lists:
            spLists = each.split('_')
            if len(spLists) == 1:
                nameObj = spLists[0]  
                nameCapObj = nameObj.capitalize()
                obj = mc.rename(each,'%s_Geo' % nameCapObj)
                reNameObj.append(each)
            elif len(spLists) == 2:
                nameObj = spLists[0]
                lastNameObj = spLists[1]
                nameCapObj = nameObj.capitalize()
                lastNameCapObj = lastNameObj.capitalize()
                obj = mc.rename(each,'%s_%s' % (nameCapObj , lastNameCapObj))
                reNameObj.append(each)
            elif len(spLists) == 3:
                nameObj = spLists[0]
                sideObj = spLists[1]
                lastNameObj = spLists[2]
                nameCapObj = nameObj.capitalize()
                sideCapObj = sideObj.capitalize()
                lastNameCapObj = lastNameObj.capitalize()
                obj = mc.rename(each,'%s_%s_%s' % (nameCapObj , sideCapObj, lastNameCapObj)) 
                reNameObj.append(each)    
            else:
                sameObj.append(each)
    print "All Obj :      ", objLen
    print "Don't Re-Name :", len(sameObj)   
    print 'Re-Name :      ', len(reNameObj)

def openBook(name = 'BookUp1', valueBook1 = 1, valueBook2 = 0.7,valueBook3 = 0.5,valueBook4 = 0.3, rotateObj = 'rotateX',*args):
    lists = mc.ls(sl=True)
    obj1 = lists[0]
    obj2 = lists[1]
    splObj2 = obj2.split('_')
    numbers = (int(splObj2[1]))
    attrName(obj = obj1, elem = name)

    attrs = 'X'
    valueBook = [valueBook1, valueBook2, valueBook3, valueBook4]
    i = 0
    u = 0
    for each in valueBook:
        # attrName(obj = (obj1+'Shape'), elem = (name+'Amp'+(u)))
        elemMdv = mc.createNode('multiplyDivide', n = ('%s%s_Mdv' % (name, u+1)))
        attrName(obj = ('%sShape' % obj1), elem = ('%s%s%s' % (name,'Amp',u+1)))
        ampListt = mc.setAttr('%sShape.%s%s%s' % (obj1 , name, 'Amp', u+1), valueBook[u] )
        print '%sShape.%s%s%s' % (obj1 , name, 'Amp', u+1)
        print '%s.input2X' % elemMdv
        mc.connectAttr('%sShape.%s%s%s' % (obj1 , name, 'Amp', u+1), ('%s.input2X' % elemMdv))
        mc.connectAttr('%s.%s' % (obj1, name), ('%s.input1%s' % (elemMdv, attrs)))
        mc.connectAttr('%s.output%s' % (elemMdv, attrs), ('%s%sOfst_%s_Grp.%s' % (splObj2[0], splObj2[2], (numbers + u), rotateObj)))

        u += 1

def connectBsh(baseBsh = 'Body_Geo', bufferGeo = 'BodyBffr_Geo', eyeBshGrp = 'EyeBshGeo_Grp', mthBshGrp = 'MthBshGeo_Grp', ebBshGrp = 'EbBshGeo_Grp', eyeCtrl = 'Eye_Ctrl',*args):
    objGeo = [eyeBshGrp, mthBshGrp, ebBshGrp]
    eyeBshGrpLists = mc.listRelatives(mc.ls(eyeBshGrp))
    mthBshGrpLists = mc.listRelatives(mc.ls(mthBshGrp))
    ebBshGrpLists = mc.listRelatives(mc.ls(ebBshGrp))
    numbersEyeBsh = len(eyeBshGrpLists)

    eyeObj = eyeBshGrp.split('BshGeo_Grp')
  

    if mc.objExists(bufferGeo):
        bffrSplit = bufferGeo.split('_')
        newNameBffr = ('%s%s%s' % (bffrSplit[0], 'Bsh', '_Geo'))
        mc.blendShape(bufferGeo, baseBsh, frontOfChain = True, name = newNameBffr)

    if mc.objExists(eyeBshGrp):
        bshListsObj = []
        if mc.objExists(eyeBshGrp):
            mc.select(eyeBshGrpLists)   
            mc.select(bufferGeo, add= True)
            bshLists = mc.blendShape(name = '%sBsh_Geo' % eyeObj[0])
        if mc.objExists(eyeCtrl):
            attrName(obj = ('%sShape' % eyeCtrl), elem = ('%s%s' % (eyeObj[0],'Amp')))
            mc.setAttr('%sShape.%sAmp' % (eyeCtrl,eyeObj[0]), l = True)
            for each in eyeBshGrpLists:
                eachSplit = each.split('_')
                newNameBsh = ('%s%s' % (eachSplit[0], 'Bsh'))
                bshListsObj.append(newNameBsh)
                attrName(obj = eyeCtrl, elem = newNameBsh)

                attrName(obj = ('%sShape' % eyeCtrl), elem = ('%s%s' % (newNameBsh,'Amp')))
                elemMdv = mc.createNode('multiplyDivide', n = ('%s_Mdv' % each))
                mc.setAttr('%sShape.%sAmp' % (eyeCtrl,newNameBsh), 0.1)
                mc.connectAttr('%sShape.%sAmp' % (eyeCtrl,newNameBsh), '%s.input2X' % elemMdv)
                mc.connectAttr('%s.outputX' % elemMdv, '%s.%s' % ('%sBsh_Geo' % eyeObj[0],each))
                mc.connectAttr('%s.%s' % (eyeCtrl, newNameBsh), '%s.input1X' % elemMdv)
            mc.select(clear=True)

def ExFaceProxy(paths = '', side = '', count = ''):
    
    mc.ExtractFace(mc.ls(sl=True))
    listObj = mc.ls(sl=True)
    if side != '':
        findObj = mc.ls('%s*_ProxyGeo_%s_Grp' % (paths, side))
    else:
        findObj = mc.ls('%s*_ProxyGeo_Grp' % (paths))
    # Thumb1_ProxyGeo_L_Grp
    if not findObj:
        checkList = mc.listRelatives(listObj[-2], p = True)
        mc.parent(listObj[-2], w = True)

        objLisRe = mc.listRelatives(listObj, p = True)
        if not len(objLisRe) == 1:
            if side != '':
                objGeo = mc.polyUnite(objLisRe, n='%s%s_Proxy_%s_Geo' % (paths, count , side))
                grpObj = mc.group (n = '%s%s_ProxyGeo_%s_Grp' % (paths, count, side), em = True)
            else:
                objGeo = mc.polyUnite(objLisRe, n='%s%s_Proxy_Geo' % (paths, count))    
                grpObj = mc.group (n = '%s%s_ProxyGeo_Grp' % (paths, count), em = True)            
            objGeo = mc.ls(objGeo[0])
            pm.delete ( objGeo , ch = True ) ; 
            pm.delete ( listObj[-2] , ch = True ) ; 
            mc.parent(objGeo, grpObj)
        else:
            objGeo = mc.ls(listObj[0])
            pm.delete ( objGeo , ch = True ) ; 
            pm.delete ( listObj[-2] , ch = True ) ; 

            if side != '':
                grpObj = mc.group (n = '%s%s_ProxyGeo_%s_Grp' % (paths, count, side), em = True)
                nameObj = mc.rename(objGeo,'%s%s_Proxy_%s_Geo' % (paths, count , side)) 
            else:
                grpObj = mc.group (n = '%s%s_ProxyGeo_Grp' % (paths, count), em = True)
                nameObj = mc.rename(objGeo,'%s%s_Proxy_Geo' % (paths, count))    
            mc.parent(nameObj, grpObj)     
            mc.delete(checkList)

        if mc.objExists('%sProxyGeo_%s_Grp' % (paths, side)):
            mc.parent(grpObj, '%sProxyGeo_%s_Grp' % (paths, side))
        else:
            if side != '':
                grpMain = mc.group(n = '%sProxyGeo_%s_Grp' % (paths, side), em = True)
            
            else:
                grpMain = mc.group(n = '%sProxyGeo_Grp' % paths, em = True)
            mc.parent(grpObj, grpMain)

    else:
        objGrp = findObj[-1]
        spObj = objGrp.split('_')
        countObj =  int(spObj[0].split(paths)[1])  # split Paths/Number
        objGeo = mc.ls(sl=True)
        mc.parent (objGeo[-2], w = True)
        pm.delete (objGeo[-2], ch = True ) ; 
        objLisRe = mc.listRelatives(objGeo, p = True)

        if len(objLisRe) > 1:
            if side != '':
                grpObj = mc.group (n = '%s%s_ProxyGeo_%s_Grp' % (paths, countObj+1 , side), em = True)
                comObj = mc.polyUnite(objLisRe, n='%s%s_Proxy_%s_Geo' % (paths, countObj+1 , side))
            else:
                grpObj = mc.group (n = '%s%s_ProxyGeo_Grp' % (paths, countObj+1 ), em = True)
                comObj = mc.polyUnite(objLisRe, n='%s%s_Proxy_Geo' % (paths, countObj+1))  
                              
            geoObj = mc.ls(comObj[-2])   
            pm.delete (geoObj , ch = True ) ; 
            mc.parent(geoObj, grpObj)
        else:
            checkList = mc.listRelatives(listObj[0], p =True)
            objGeo = mc.ls(listObj[0])
            pm.delete ( objGeo , ch = True ) ; 
            if side != '':
                grpObj = mc.group (n = '%s%s_ProxyGeo_%s_Grp' % (paths, countObj+1 , side), em = True)
                nameObj = mc.rename(objGeo,'%s%s_Proxy_%s_Geo' % (paths, countObj+1 , side))

            else:
                grpObj = mc.group (n = '%s%s_ProxyGeo_Grp' % (paths, countObj+1), em = True)
                nameObj = mc.rename(objGeo,'%s%s_Proxy_Geo' % (paths, countObj+1))

            mc.parent(nameObj, grpObj)
            mc.delete(checkList)
        if side != '':    
            if mc.objExists('%sProxyGeo_%s_Grp' % (paths, side)):
                mc.parent(grpObj, '%sProxyGeo_%s_Grp' % (paths, side))
            else:
                grpMain = mc.group(n = '%sProxyGeo_%s_Grp' % (paths, side), em = True)
                mc.parent(grpObj, grpMain)
        else:
            if mc.objExists('%sProxyGeo_Grp' % paths):
                mc.parent(grpObj, '%sProxyGeo_Grp' % paths)
            else:
                grpMain = mc.group(n = '%sProxyGeo_Grp' % paths, em = True)
                mc.parent(grpObj, grpMain)

        if len(objLisRe) < 1:
            objGeo = mc.ls(sl=True)
            pm.delete ( objGeo , ch = True ) ; 
            grpObj = mc.group (n = '%s%s_ProxyGeo_%s_Grp' % (paths, countObj+1 , side), em = True)
            mc.parent(objGeo, grpObj)
            if mc.objExists('%sProxyGeo_%s_Grp' % (paths, side)):
                mc.parent(grpObj, '%sProxyGeo_%s_Grp' % (paths, side))
            else:
                grpMain = mc.group(n = '%sProxyGeo_%s_Grp' % (paths, side), em = True)
                mc.parent(grpObj, grpMain)

def extractFace (*args):
    mc.ExtractFace(mc.ls(sl=True))
    obj = mc.listRelatives(mc.ls(sl=True), p = True)
    objxx = mc.listRelatives(obj,c = True)
    mc.parent(objxx[-2], w = True)

def layerControlRun(ns = '', obj = '', shape = True, *args):
    mainAttrLayerVis = lcv.layerControlAttr()
    mins = 0 
    maxs = 1
    shapes = 'Shape'
    ctrlShape = '%s%s%s' % (ns, obj, shapes)
    ctrl = '%s%s' % (ns, obj)
    if shape  == True:
        if obj:
            for each in mainAttrLayerVis:
                ## Create Attrs Control Visibility
                attrNameMinMax(obj = ctrlShape, elem = each, min = mins, max = maxs )
                mc.setAttr('%s%s.%s' % (ns, obj, each), l = True)
                for attrs in mainAttrLayerVis[each]:
                    attrNameMinMax(obj = ctrlShape, elem = attrs, min = mins, max = maxs )
                    mc.setAttr('%s%s.%s' % (obj, shapes, attrs), 1)

    else:
        if obj:
            for each in mainAttrLayerVis:
                ## Create Attrs Control Visibility
                attrNameMinMax(obj = ctrl, elem = each, min = mins, max = maxs )
                mc.setAttr('%s%s.%s' % (ns, obj, each), l = True)
                for attrs in mainAttrLayerVis[each]:
                    attrNameMinMax(obj = ctrl, elem = attrs, min = mins, max = maxs )
                    mc.setAttr('%s.%s' % (obj, attrs), 1)

    ## Unlock Group for SetAttr layerControl
    ## Generate >> Unlock Group For SetAttr layerControl
    layerControlUnlock()

def createAmpEye(ns = '', process = '', obj = '', shape = True, min = 0, max = 50, *args):
    titalListAttr, listAttrL, listAttrR, eyeRigAmp= lcaf.selectControlAmp(process = process)
    mins = min 
    maxs = max
    shapes = 'Shape'
    ctrlShape = '%s%s%s' % (ns, obj, shapes)
    ctrl = '%s%s' % (ns, obj)
    i = 0 
    if shape  == True:
        if obj:
            ## Create Attrs Control Visibility
            attrNameMinMax(obj = ctrlShape, elem = titalListAttr, min = mins, max = maxs )
            mc.setAttr('%s%s%s.%s' % (ns, obj, shapes, titalListAttr), l = True)
            for each in listAttrL:
                attrNameMinMax(obj = ctrlShape, elem = each, min = mins, max = maxs )
                mc.setAttr('%s%s.%s' % (obj, shapes, each), eyeRigAmp[each])
            i += 1
    else:
        if obj:
            ## Create Attrs Control Visibility
            attrNameMinMax(obj = ctrl, elem = titalListAttr, min = mins, max = maxs )
            mc.setAttr('%s%s.%s' % (ns, obj, titalListAttr), l = True)
            for each in listAttrL:
                attrNameMinMax(obj = ctrl, elem = each, min = mins, max = maxs )
                mc.setAttr('%s.%s' % (obj, each), eyeRigAmp[each])
            i += 1

## Get Path File and Open File
def openFileWorldSpace(*args):
    path = mc.file(q=True, sn=True)
    mc.file(path, o=True, f=True)


def connectLayerControlVis(obj = '', shape = True, *args):
    mainAttrLayerVis = lcv.layerControlAttr()
    objLayerVis = lcv.listControlVis()
    shapes = 'Shape'
    if shape:
        objCtrlShape = '%s%s' % (obj, shapes)
    else:
        objCtrl = obj

    i = 0
    for each in mainAttrLayerVis:
        for sel in mainAttrLayerVis[each]:
            for objLists in objLayerVis[sel]:
                try:
                    mc.connectAttr('%s.%s' % (obj, sel), '%s.v' %  (objLists) )
                except: pass

def layerControlUnlock(*args):
    objLayerVis= lcv.listControlVis()
    for each in objLayerVis:
        for sel in objLayerVis[each]:
            try:
                mc.setAttr('%s.visibility' % sel, l = False)
            except:
                pass

def nonRollFk(      elem = '',
                    side = '',
                    objAttrs = '', 
                    source = '', 
                    target = '',
                    valueAmp = 0.8,
                    mins = 0,
                    maxs = 90,
                    axisSource = '',
                    axisTarget = '',
                    rotate = True,
                    translate = False,
                    scale = False,
                    # selected = False,
                    # selsObj = '',
                    shape = False,
                     *args):
    try:
        lockAttrObj(listObj = [target],lock = False)
    except:
        pass
    if rotate == True:
        attsObj  = 'rotate'
        parentList = mc.listConnections('%s.%s%s' % (target, attsObj, axisTarget), s = True, type = 'unitConversion')

    if translate == True:
        attsObj = 'translate'
        parentList = mc.listConnections('%s.%s%s' % (target, attsObj, axisTarget), s = True)

    if scale == True:
        attsObj = 'scale'
        parentList = mc.listConnections('%s.%s%s' % (target, attsObj, axisTarget), s = True)

    # if selected == True:
    #     attsObj = selsObj
    #     parentList = mc.listConnections('%s.%s%s' % (target, attsObj, axisTarget), s = True)

    if side:
        objAttrsList = '%s_%s' % (elem, side)
    else:
        side = 'C'
        objAttrsList = '%s_%s' % (elem, side)

    onOffAtr = 'AutoFk'

    if shape:
        objAttrss = objAttrs + 'Shape'
        # print '1'
    else:
        objAttrss = objAttrs
        # print'2'

    # print objAttrss, 'objAttrss'
    listAttrObj = mc.listAttr(objAttrs, s = True, r = True, k = True)
    ## Create MultiplyDivice 1
    elemMdv = mc.createNode('multiplyDivide', n = '%s_%s_NonRoll_Mdv' % (elem, side))
    mc.connectAttr('%s.rotate%s' % (source, axisSource), '%s.input1X' % elemMdv)
    if onOffAtr in listAttrObj:
        ## Connect Attr OnOff
        mc.connectAttr('%s.%s' % (objAttrs, onOffAtr), '%s.input2X' % elemMdv)
    else:
        ## Create OnOff
        attrNameMinMax (obj = objAttrs, elem = onOffAtr, min = 0, max = 1)
        mc.setAttr("%s.%s" % (objAttrs, onOffAtr), 1)
        ## Connect Attr OnOff
        mc.connectAttr('%s.%s' % (objAttrs, onOffAtr), '%s.input2X' % elemMdv)

    if objAttrss:
        # listAttrObj = mc.listAttr(objAttrss, s = True, r = True, k = True)

        ## Create Attr
        attrName(obj = objAttrss, elem = objAttrsList)   
        mc.setAttr("%s.%s" % (objAttrss, objAttrsList), valueAmp)

        ## Create Clamp
        elemClm = mc.createNode('clamp', n = '%s_%s_NonRoll_Clm' % (elem, side))
        mc.setAttr ('%s.minR' % elemClm, mins)
        mc.setAttr ('%s.maxR' % elemClm, maxs)
        mc.connectAttr('%s.outputX' % elemMdv, '%s.inputR' % elemClm)



        # # print parentList, 'parentList'
        if rotate :
            ## Create MultiplyDivice 2 Rotate
            elemDvMdv = mc.createNode('multiplyDivide', n = '%s_%s_DvNonRoll_Mdv' % (elem, side))
            # elemDvAttrMdv = mc.createNode('multiplyDivide', n = '%s_%s_DvAttrNonRoll_Mdv' % (elem, side))
            # mc.setAttr('%s.input2X' % elemDvAttrMdv, 0.1)
            mc.connectAttr('%s.%s' % (objAttrss,objAttrsList), '%s.input2X' % elemDvMdv)
            # mc.connectAttr('%s.%s' % (objAttrss,objAttrsList), '%s.input1X' % elemDvAttrMdv)
            # mc.connectAttr('%s.outputX' % elemDvAttrMdv, '%s.input2X' % elemDvMdv)
            mc.connectAttr('%s.outputR' % elemClm,  '%s.input1X' % elemDvMdv)
            if parentList :
                ll = mc.listConnections('%s.%s%s' % (target, attsObj, axisTarget), s = True, type = 'unitConversion')
                ken = mc.listConnections( ll, s = True, type = 'plusMinusAverage')
                zz = mc.listConnections(ken[0],d = False, c = True )

                ## fine input2D
                ken2 = zz[-2].split('.')
                objInput = ken2[-2]
                num = objInput.split('[')[1].replace(']','')
                ken3 = int(num)+1
                renameKen4 = 'input2D[%s]' % ken3
                # print objInput,'num1'
                # print ken3,'nummmmmmmmmmm'
                # print zz, 'zz'

                mc.connectAttr('%s.outputX' % elemDvMdv, '%s.%s.input2Dx' % (ken2[0], renameKen4))

            else:
                ## Create PlusMinusAverage 
                elemPma = mc.createNode( 'plusMinusAverage', n = '%s_%s_NonRoll_Pma' % (elem, side))
                mc.connectAttr('%s.outputX' % elemDvMdv, '%s.input2D[0].input2Dx' % elemPma)
                mc.connectAttr('%s.output2Dx' % elemPma, '%s.%s%s' % (target, attsObj, axisTarget))

        if translate:
            ## Create MultiplyDivice 2 Translate
            elemDvMdv = mc.createNode('multiplyDivide', n = '%s_%s_DvNonRoll_Mdv' % (elem, side))
            elemDvAttrMdv = mc.createNode('multiplyDivide', n = '%s_%s_DvAttrNonRoll_Mdv' % (elem, side))
            mc.setAttr('%s.input2X' % elemDvAttrMdv, 0.1)
            # mc.connectAttr('%s.%s' % (objAttrss,objAttrsList), '%s.input2X' % elemDvMdv)
            mc.connectAttr('%s.%s' % (objAttrss,objAttrsList), '%s.input1X' % elemDvAttrMdv)
            mc.connectAttr('%s.outputX' % elemDvAttrMdv, '%s.input2X' % elemDvMdv)
            mc.connectAttr('%s.outputR' % elemClm,  '%s.input1X' % elemDvMdv)
            if parentList:
                zz = mc.listConnections(parentList[0],d = False, c = True )

                ## fine input2D
                ken2 = zz[-2].split('.')
                objInput = ken2[-2]
                ken3 = int(objInput[-2])+1
                renameKen4 = 'input2D[%s]' % ken3

                mc.connectAttr('%s.outputX' % elemDvMdv, '%s.%s.input2Dx' % (ken2[0], renameKen4))

            else:
                ## Create PlusMinusAverage 
                elemPma = mc.createNode( 'plusMinusAverage', n = '%s_%s_NonRoll_Pma' % (elem, side))
                mc.connectAttr('%s.outputX' % elemDvMdv, '%s.input2D[0].input2Dx' % elemPma)
                mc.connectAttr('%s.output2Dx' % elemPma, '%s.%s%s' % (target, attsObj, axisTarget))

        if scale:
            ## Create MultiplyDivice 2 Translate
            elemDvMdv = mc.createNode('multiplyDivide', n = '%s_%s_DvNonRoll_Mdv' % (elem, side))
            elemDvAttrMdv = mc.createNode('multiplyDivide', n = '%s_%s_DvAttrNonRoll_Mdv' % (elem, side))
            mc.setAttr('%s.input2X' % elemDvAttrMdv, 0.1)
            # mc.connectAttr('%s.%s' % (objAttrss,objAttrsList), '%s.input2X' % elemDvMdv)
            mc.connectAttr('%s.%s' % (objAttrss,objAttrsList), '%s.input1X' % elemDvAttrMdv)
            mc.connectAttr('%s.outputX' % elemDvAttrMdv, '%s.input2X' % elemDvMdv)
            mc.connectAttr('%s.outputR' % elemClm,  '%s.input1X' % elemDvMdv)
            if parentList:
                zz = mc.listConnections(parentList[0],d = False, c = True )

                ## fine input2D
                ken2 = zz[-2].split('.')
                objInput = ken2[-2]
                ken3 = int(objInput[-2])+1
                renameKen4 = 'input2D[%s]' % ken3

                mc.connectAttr('%s.outputX' % elemDvMdv, '%s.%s.input2Dx' % (ken2[0], renameKen4))

            else:
                ## Create PlusMinusAverage 
                elemPma = mc.createNode( 'plusMinusAverage', n = '%s_%s_NonRoll_Pma' % (elem, side))
                mc.connectAttr('%s.outputX' % elemDvMdv, '%s.input2D[0].input2Dx' % elemPma)
                mc.connectAttr('%s.output2Dx' % elemPma, '%s.%s%s' % (target, attsObj, axisTarget))


        # if selected:
        #     ## Create MultiplyDivice 2 Translate
        #     elemDvMdv = mc.createNode('multiplyDivide', n = '%s_%s_DvNonRoll_Mdv' % (elem, side))
        #     elemDvAttrMdv = mc.createNode('multiplyDivide', n = '%s_%s_DvAttrNonRoll_Mdv' % (elem, side))
        #     mc.setAttr('%s.input2X' % elemDvAttrMdv, 0.1)
        #     # mc.connectAttr('%s.%s' % (objAttrss,objAttrsList), '%s.input2X' % elemDvMdv)
        #     mc.connectAttr('%s.%s' % (objAttrss,objAttrsList), '%s.input1X' % elemDvAttrMdv)
        #     mc.connectAttr('%s.outputX' % elemDvAttrMdv, '%s.input2X' % elemDvMdv)
        #     mc.connectAttr('%s.outputR' % elemClm,  '%s.input1X' % elemDvMdv)
        #     if parentList:
        #         zz = mc.listConnections(parentList[0],d = False, c = True )

        #         ## fine input2D
        #         ken2 = zz[-2].split('.')
        #         objInput = ken2[-2]
        #         ken3 = int(objInput[-2])+1
        #         renameKen4 = 'input2D[%s]' % ken3

        #         mc.connectAttr('%s.outputX' % elemDvMdv, '%s.%s.input2Dx' % (ken2[0], renameKen4))

        #     else:
        #         ## Create PlusMinusAverage 
        #         elemPma = mc.createNode( 'plusMinusAverage', n = '%s_%s_NonRoll_Pma' % (elem, side))
        #         mc.connectAttr('%s.outputX' % elemDvMdv, '%s.input2D[0].input2Dx' % elemPma)
        #         mc.connectAttr('%s.output2Dx' % elemPma, '%s.%s%s' % (target, attsObj, axisTarget))

def fixBagArthur (elem = '', side = '', *args):
    elemPma = mc.createNode( 'plusMinusAverage', n = '%s_%s_NonRoll_Pma' % (elem, side))
    mc.connectAttr('%s.translateY' % 'BagScissor4_R_Ctrl',  '%s.input2D[0].input2Dx' % elemPma)
    mc.connectAttr('%s.output2Dx' % elemPma, '%s.input1D[2]' % 'BagScissor3Sqsh_R_Pma')

def setDefaultControl(char = '', *args):
    ## SetAttr
    ArmList = ['Arm_L_Ctrl', 'Arm_R_Ctrl']
    SpineList = ['Spine_Ctrl']
    LegList = ['Leg_L_Ctrl', 'Leg_R_Ctrl']
    GrpList = ['Ctrl_Grp', 'Skin_Grp', 'Jnt_Grp', 'Ikh_Grp', 'Still_Grp', ]
    ## Arm
    for eachArm in ArmList:
        mc.setAttr('%s.fkIk' % eachArm, 0)

    ## Spine
    for eachSpine in SpineList:
        mc.setAttr('%s.fkIk' % eachSpine, 1)

    ## Leg
    for eachLeg in LegList:
        mc.setAttr('%s.fkIk' % eachLeg, 1)

    ## SetAttr AllMover Vis
    attrCtrlVis = lcv.layerControlAttr()
    AllMoverObj = '%s_Crv' % char
    for each in attrCtrlVis:
        for sels in attrCtrlVis[each]:
            # print sels, 'sels1'
            if sels == 'MasterVis':
                # print sels, 'sels2'
                mc.setAttr('%s.%s' % (AllMoverObj,'MasterVis'), 1)
            elif sels == 'GeoVis':
                # print sels, 'sels3'
                mc.setAttr('%s.%s' % (AllMoverObj,'GeoVis'), 1)                
            else:
                # print sels, 'sels4'
                mc.setAttr('%s.%s' % (AllMoverObj,sels), 0)

    ## SetAttr Visibility
    for each in GrpList[0]:
        lockAttr(listObj = each, attrs = ['v'],lock = False)
    for each in GrpList[1], GrpList[-1]:
        lockAttr(listObj = each, attrs = ['v'],lock = True)

def combineCurves(char = '', curves = []):
    crvGrp = mc.group(n = '%s_Crv' % char, em=True)
    for crv in curves:
        crvShape = mc.listRelatives(crv, shapes = True)
        mc.parent(crvShape,crvGrp,s=True,r=True, relative=True)
        if curves[-1]:
            mc.parent(crv, crvGrp)
        mc.delete(crv) 
    ## Rename CurveShape1 >> CharNameShape
    mc.rename ("curveShape1","%sShape" % crvGrp)   
    ## LockAttr 
    lockAttrObj(listObj = [crvGrp], lock = True) 
    mc.setAttr(crvGrp + '.overrideEnabled', 1)
    mc.setAttr(crvGrp + '.overrideColor', 19)
    pm.delete ( crvGrp , ch = True )
    return  crvGrp

def textName(char = '', size = '',*args):
    if char == '':
        ## find charName
        asset = context_info.ContextPathInfo()         
        char = asset.name
        if char == '':
            print 'Please Check File Path'
            # char = 'TEST'

    fontText = 'MS Shell Dlg 2'
    txtCrv = mc.textCurves( n = '%s_' %char , f = fontText , t = char )
    listChi = mc.listRelatives(txtCrv[0], type = 'transform', c = True, ad = True, s = False)
    crvGrp = mc.createNode ('transform', n = '%sCrv_Grp' % char, )
    mc.select(cl = True)
    listGrp =[]
    listCrvObj = []
    for each in listChi:
        if '_1' in each :
            listConObj = mc.listConnections(each, s = True)
            listParObj = mc.listConnections(each,d = False, c = True ,p  = True)
            listSplitObj = listParObj[-1].split('.')
            objConnect = listSplitObj[-1]
            listNum = int(objConnect[-2])
            positionCon = 'position[%s]' % listNum

            listGrp.append(each)
        if 'curve' in each:
            listCrvObj.append(each)        

    mc.parent(listCrvObj, w = True)
    mc.select(crvGrp, add = True)
    mc.parent(listCrvObj, crvGrp)   
    for each in listCrvObj:       
        if 'curve' in each:
            ## Freeztransform
            mc.makeIdentity (each , apply = True , jointOrient = False, normal = 1, translate =True, rotate = True, scale = True) 
            ## Reset Transform
            mc.move("%s.scalePivot" % each,"%s.rotatePivot" % each, absolute=True) 
    ## Combine Curve
    crvUse = combineCurves(char = char, curves = listCrvObj)
    mc.parent(crvUse, crvGrp)
    mc.setAttr('%s.s' % crvGrp, float(size),float(size),float(size))
    mc.xform(crvGrp, cp=1)
    mc.delete('%s_Shape' % char)


def tyHeadEnd(char = '', value = '', *args):
    headEnd = 'HeadEnd_Jnt'
    textName = '%sCrv_Grp' % char
    mc.delete(mc.parentConstraint(headEnd, textName))
    lists = mc.xform(textName, q=True, t=True) 
    values =  lists[1]+float(value)
    mc.setAttr('%s.ty' % textName, values)
    # mc.setAttr('%s.tx' % textName, -1)
    # return values

def copyOneToTwoSkinWeight(*args):
    listaA = mc.ls(sl=True)
    listsB = mc.ls(sl=True)
    i = 0
    for i in range(len(listsB)):
        #print i
        mc.select(listaA[i])
        mc.select(listsB[i], add = True)
        weightTools.copySelectedWeight()
        print listaA[i], '>>' , listsB[i]
    i += 1

def copySkinWeightToPosition(*args):
    lists = mc.ls(sl=True)
    for each in lists:
        mc.select(each)
        mc.select('Fur1_Geo_Proxy3' , add = True)
        lrr.copyWeightBasedOnWorldPosition()

def addFurGuide(BodyGeo = '', BodyFurGeo = '', BodyFclRigWrap = '', *args):
    # Bsh Body_Geo to BodyDtlCtrl  
    stillRigGrp = mc.ls("*:Still_Grp")

    if BodyGeo == '':
        BodyGeo = mc.ls("*:Body_Geo")
        BodyFurGeo = mc.ls("*:BodyFur_Geo")
        BodyFclRigWrap = mc.ls("BodyFclRigWrapped_Geo")

    # Duplicate BodyFurFcl_Geo, BodyFurGuide_Geo
    dupBodyGeo = mc.duplicate(BodyGeo, n = 'BodyFurFcl_Geo')
    dupBodyFurGuideGeo = mc.duplicate(BodyFurGeo, n = 'BodyFurGuide_Geo')
    mc.parent( dupBodyGeo[0], dupBodyFurGuideGeo[0], stillRigGrp)
    mc.setAttr('%s.v' % dupBodyGeo[0], 0)
    mc.setAttr('%s.v' % dupBodyFurGuideGeo[0], 0)
    # Bsh 
    mc.select(dupBodyFurGuideGeo[0], r = True)
    mc.select(dupBodyGeo[0], add = True)
    lrr.doAddBlendShape()

    mc.select(BodyFclRigWrap, r = True)
    mc.select(dupBodyGeo[0], add = True)
    lrr.doAddBlendShape()

    mc.select(dupBodyGeo[0], r = True)
    mc.select(BodyFurGeo, add = True)
    lrr.doAddBlendShape()

def connectObjMirror(translate = True, rotate = True, scale = True, valueA = -1, valueB = 1,*args):
    lists = mc.ls(sl = True)
    if translate == True:
        for each in lists:
            objSp =each.split('_')
            mdvNode = mc.createNode ('multiplyDivide', n = ('%sTr_%s_Mdv' % (objSp[0], objSp[1])))
            mc.setAttr('%s.input2.input2X' % mdvNode, valueA)
            #mc.setAttr('%s.input2.input2X' % mdvNode, valueB)
            #mc.setAttr('%s.input2.input2X' % mdvNode, valueB)
            mc.connectAttr('%s.translate.translateX' % each, '%s.input1.input1X' % mdvNode)
            mc.connectAttr('%s.translate.translateY' % each, '%s.input1.input1Y' % mdvNode)
            mc.connectAttr('%s.translate.translateZ' % each, '%s.input1.input1Z' % mdvNode)
            mc.connectAttr('%s.output.outputX' % mdvNode, '%s_%s_%s.translate.translateX' % (objSp[0], 'R', objSp[2]))
            mc.connectAttr('%s.output.outputY' % mdvNode, '%s_%s_%s.translate.translateY' % (objSp[0], 'R', objSp[2]))
            mc.connectAttr('%s.output.outputZ' % mdvNode, '%s_%s_%s.translate.translateZ' % (objSp[0], 'R', objSp[2]))

    if rotate == True:  
        for each in lists:
            objSp =each.split('_')
            mdvNode = mc.createNode ('multiplyDivide', n = ('%sRo_%s_Mdv' % (objSp[0], objSp[1])))
            #mc.setAttr('%s.input2.input2X' % mdvNode, valueA)
            mc.setAttr('%s.input2.input2Y' % mdvNode, valueA)
            mc.setAttr('%s.input2.input2Z' % mdvNode, valueA)
            mc.connectAttr('%s.rotateX' % each, '%s.input1X' % mdvNode)
            mc.connectAttr('%s.rotateY' % each, '%s.input1Y' % mdvNode)
            mc.connectAttr('%s.rotateZ' % each, '%s.input1Z' % mdvNode)
            mc.connectAttr('%s.outputX' % mdvNode, '%s_%s_%s.rotateX' % (objSp[0], 'R', objSp[2]))
            mc.connectAttr('%s.outputY' % mdvNode, '%s_%s_%s.rotateY' % (objSp[0], 'R', objSp[2]))
            mc.connectAttr('%s.outputZ' % mdvNode, '%s_%s_%s.rotateZ' % (objSp[0], 'R', objSp[2]))  

    if scale == True:
        for each in lists:
            objSp =each.split('_')
            mdvNode = mc.createNode ('multiplyDivide', n = ('%sSc_%s_Mdv' % (objSp[0], objSp[1])))
            mc.setAttr('%s.input2.input2X' % mdvNode, valueA)
            #mc.setAttr('%s.input2.input2X' % mdvNode, valueB)
            #mc.setAttr('%s.input2.input2X' % mdvNode, valueB)
            mc.connectAttr('%s.scaleX' % each, '%s.input1.input1X' % mdvNode)
            mc.connectAttr('%s.scaleY' % each, '%s.input1.input1Y' % mdvNode)
            mc.connectAttr('%s.scaleZ' % each, '%s.input1.input1Z' % mdvNode)
            mc.connectAttr('%s.output.outputX' % mdvNode, '%s_%s_%s.scaleX' % (objSp[0], 'R', objSp[2]))
            mc.connectAttr('%s.output.outputY' % mdvNode, '%s_%s_%s.scaleY' % (objSp[0], 'R', objSp[2]))
            mc.connectAttr('%s.output.outputZ' % mdvNode, '%s_%s_%s.scaleZ' % (objSp[0], 'R', objSp[2]))
            
def duplicateMthJawJnt(*args):
    jaw1Obj = mc.ls('*:JawLwr1_Jnt')
    jaw2Obj = mc.ls('*:JawLwr2_Jnt')
    HeadJntObj = mc.ls('*:Head_Jnt')
    paList = mc.listRelatives(jaw1Obj[0], p = True)
    jawGrp = mc.createNode ('transform' , n = 'mthJawJnt_Grp')
    mc.delete(mc.parentConstraint(paList[0], jawGrp, mo = False))
    spJnt1Name = jaw1Obj[0].split(':')
    spJnt2Name = jaw2Obj[0].split(':')
    spHeadJntName = HeadJntObj[0].split(':')
    mthJaw1Obj = mc.createNode('joint', n = 'mth%s' % spJnt1Name[-1])
    mthJaw2Obj = mc.createNode('joint', n = 'mth%s' % spJnt2Name[-1])
    HeadJnt = mc.createNode('joint', n = 'mth%s' % spHeadJntName[-1])
    mc.setAttr('%s.radius' % mthJaw1Obj, 0.025)
    mc.setAttr('%s.radius' % mthJaw2Obj, 0.025)
    mc.setAttr('%s.radius' % HeadJnt, 0.025)
    mc.delete(mc.parentConstraint(jaw1Obj[0], mthJaw1Obj, mo =False))
    mc.delete(mc.parentConstraint(jaw2Obj[0], mthJaw2Obj, mo =False))
    mc.delete(mc.parentConstraint(HeadJntObj[0], HeadJnt, mo =False))
    mc.makeIdentity (mthJaw1Obj , apply = True)
    mc.makeIdentity (mthJaw2Obj , apply = True)
    mc.makeIdentity (HeadJnt , apply = True)
    mc.parent(mthJaw2Obj, mthJaw1Obj)
    mc.parent(mthJaw1Obj, jawGrp)

def reDuildCurve(lists = [], spans = '', *args):
    if lists:
        for each in lists:
            lists = each
            mc.rebuildCurve(lists, rt = 0, s = int(spans))
    else:
        lists = mc.ls(sl = True)
        for each in lists:
            # obj = each.split('|')
            lists = each
            print lists, 'lists'
            mc.rebuildCurve(lists, rt = 0, s = int(spans))

def OnOffVisibility(lists = [], v = 1):
    # Ctrl_Grp = mc.ls('*:Ctrl_Grp')
    # Skin_Grp = mc.ls('*:Skin_Grp')
    # Jnt_Grp = mc.ls('*:Jnt_Grp')
    # Ikh_Grp = mc.ls('*:Ikh_Grp')
    # lists = [Skin_Grp[0], Jnt_Grp[0], Ikh_Grp[0]]
    # mc.setAttr('%s.v' % Ctrl_Grp, v)
    for each in lists:
        mc.setAttr('%s.v' % each, v)

def ballRollAutoToeBend(*args):
    #Left Ankle BallRoll
    cdnL = mc.shadingNode('condition', au = True , n ='BallRollFix_L_Cdn')
    pmaL = mc.shadingNode('plusMinusAverage', au = True , n ='BallRollFix_L_Pma')

    mc.connectAttr('AnkleIk_L_Ctrl.ballRoll',cdnL +'.firstTerm')
    mc.connectAttr('AnkleIk_L_Ctrl.ballRoll',cdnL +'.colorIfTrueR')
    mc.setAttr(cdnL +'.operation',3)
    mc.setAttr(cdnL +'.colorIfFalseR',0)

    mc.connectAttr('BallRollFix_L_Cdn.outColorR',pmaL +'.input1D[0]')
    mc.connectAttr('AnkleIk_L_Ctrl.toeBend',pmaL +'.input1D[1]')

    mc.setAttr('BendIkPiv_L_Grp.rotateX',l = False)
    mc.connectAttr(pmaL +'.output1D','BendIkPiv_L_Grp.rotateX',f=True,l = True)


    #Right Ankle BallRoll
    cdnR = mc.shadingNode('condition', au = True , n ='BallRollFix_R_Cdn')
    pmaR = mc.shadingNode('plusMinusAverage', au = True , n ='BallRollFix_R_Pma')

    mc.connectAttr('AnkleIk_R_Ctrl.ballRoll',cdnR +'.firstTerm')
    mc.connectAttr('AnkleIk_R_Ctrl.ballRoll',cdnR +'.colorIfTrueR')
    mc.setAttr(cdnR +'.operation',3)
    mc.setAttr(cdnR +'.colorIfFalseR',0)

    mc.connectAttr('BallRollFix_R_Cdn.outColorR',pmaR +'.input1D[0]')
    mc.connectAttr('AnkleIk_R_Ctrl.toeBend',pmaR +'.input1D[1]')

    mc.setAttr('BendIkPiv_R_Grp.rotateX',l = False)
    mc.connectAttr(pmaR +'.output1D','BendIkPiv_R_Grp.rotateX',f=True,l = True)

def BendIkPivRoll(elem = '',*args):
    bendIK = mc.ls('BallIkPiv_L_Grp', 'BallIkPiv_R_Grp')
    ankleIkCtrl = mc.ls('AnkleIk_L_Ctrl', 'AnkleIk_R_Ctrl')
    i = 0
    for each in bendIK:
        lockAttr(listObj= [each],attrs = ( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v'), lock = False)
        # for sels in ankleIkCtrl:
        # print each, '>>' , ankleIkCtrl[i]
        spBendIk = each.split('_')
        # spBendIkName = spBendIk[0].lower()
        spBendIkSide = spBendIk[1]
        newBendIK = mc.createNode('transform', n = '%s_%s_Geo' % (elem, spBendIkSide))
        mc.delete(mc.parentConstraint(each, newBendIK, mo = False))
        childrenPar = mc.listRelatives(each, p = True)
        mc.parent(newBendIK, childrenPar, )
        mc.parent(each, newBendIK)


        # print childrenPar
        bendIkMdv = mc.createNode('multiplyDivide', n = '%s_%s_Mdv' % (elem, spBendIkSide))
        mc.setAttr('%s.input2X' % bendIkMdv, 1)
        attrName(obj = ankleIkCtrl[i], elem = elem)
        mc.connectAttr('%s.%s' % (ankleIkCtrl[i], elem), '%s.input1X' % bendIkMdv)
        mc.connectAttr('%s.outputX' % bendIkMdv, '%s.rotateX' % newBendIK)

        lockAttr(listObj= [each, newBendIK],attrs = ( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v'), lock = True)
        i += 1

def heelRoll(jntTmp = [],*args):
    ## list Obj
    skinGrp = 'Skin_Grp'
    ankleIkCtrl = mc.ls('AnkleIk_L_Ctrl', 'AnkleIk_R_Ctrl')
    ballJnt = mc.ls('Ball_L_Jnt', 'Ball_R_Jnt')
    i = 0
    for each in ballJnt:
        ## Create Joint
        spName = jntTmp[i].split('_')
        jntName = spName[0]
        jntSide = spName[1]
        lastName = spName[-1]
        jntHeel = mc.createNode('joint', n = '%sRoll_%s_Jnt' % (jntName, jntSide))
        zroGrpCon = mc.createNode('transform', n = '%sRollCon_%s_Grp' % (jntName, jntSide))
        zroGrpPar = mc.createNode('transform', n = '%sRollPar_%s_Grp' % (jntName, jntSide))

        ## Snap and parent Group
        snapSel(obj1 = jntTmp[i], obj2= jntHeel)
        snapSel(obj1 = each, obj2= zroGrpCon)
        snapSel(obj1 = each, obj2= zroGrpPar)
        pm.makeIdentity ( jntHeel , apply = True ) ;   
        mc.parent(jntHeel, zroGrpCon)
        mc.parent(zroGrpCon, zroGrpPar)
        mc.parent(zroGrpPar, skinGrp)

        ## parentConstraint
        parentScaleConstraintLoop(  obj = each, lists = [zroGrpPar],par = True, sca = True, ori = False, poi = False)
        ## Create Node 
        toeBendMdv = mc.createNode('multiplyDivide', n = '%stoeBend_%s_Mdv' % (jntName, jntSide))
        toeBendClm = mc.createNode('clamp', n = '%stoeBend_%s_Clm' % (jntName, jntSide))
        ballIkPivRollClm = mc.createNode('clamp', n = '%sballIkPivRoll_%s_Clm' % (jntName, jntSide))
        ballRollClm = mc.createNode('clamp', n = '%sballRoll_%s_Clm' % (jntName, jntSide))
        ballRollPma = mc.createNode('plusMinusAverage', n= '%sballRoll_%s_Pma' % (jntName, jntSide))
        ballRollRevMdv = mc.createNode('multiplyDivide', n = '%sRev_%s_Mdv' % (jntName, jntSide))

        ## Setattr Node
        mc.setAttr('%s.input2X' % toeBendMdv, -1)
        mc.setAttr('%s.minR' % toeBendClm, -90)
        mc.setAttr('%s.maxR' % toeBendClm, 90)
        mc.setAttr('%s.minR' % ballIkPivRollClm, -90)
        mc.setAttr('%s.minR' % ballRollClm, -90)
        mc.setAttr('%s.input2X' % ballRollRevMdv, 1)

        ## connectAttr AnkleIk_L_Ctrl to Node
        mc.connectAttr('%s.toeBend' % ankleIkCtrl[i], '%s.input1X' % toeBendMdv)
        mc.connectAttr('%s.outputX' % toeBendMdv, '%s.inputR' % toeBendClm)
        mc.connectAttr('%s.ballIkPvRoll' % ankleIkCtrl[i], '%s.inputR' % ballIkPivRollClm)
        mc.connectAttr('%s.ballRoll' % ankleIkCtrl[i], '%s.inputR' % ballRollClm)

        ## connectAttr to plusMinusAverage
        mc.connectAttr('%s.outputR' % toeBendClm ,'%s.input1D[0]' % ballRollPma)
        mc.connectAttr('%s.outputR' % ballIkPivRollClm ,'%s.input1D[1]' % ballRollPma)
        mc.connectAttr('%s.outputR' % ballRollClm ,'%s.input1D[2]' % ballRollPma)

        ## connectAttr to Obj
        mc.connectAttr('%s.output1D' % ballRollPma, '%s.input1X' % ballRollRevMdv)
        mc.connectAttr('%s.outputX' % ballRollRevMdv, '%s.rotateX' % zroGrpCon)

        i += 1


# def jawUprRoot (jawUprRootTmpJnt = 'JawUprRoot_TmpJnt'):
#     #-- Create Joint Jaw 
#     self.jawUprRootJnt = rt.createJnt( 'Jaw%sUprRoot%sJnt' %( elem , side ) , jawUprRootTmpJnt )
#     mc.parent( self.jawUpr1Jnt , self.jawUprRootJnt)
#     mc.parent( self.jawUprRootJnt , self.parent )