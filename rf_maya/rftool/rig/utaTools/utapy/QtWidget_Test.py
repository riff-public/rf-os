from PySide2 import QtWidgets, QtCore
from shiboken2 import wrapInstance
import maya.OpenMayaUI as mui  
from functools import partial

import sys
from PySide2.QtWidgets import QApplication, QWidget, QPushButton, QHBoxLayout, QGroupBox, QDialog, QVBoxLayout, QGridLayout, QLineEdit
from PySide2.QtGui import QIcon
# from PySide2.QtCore import pyqtSlot

ptr = mui.MQtUtil.mainWindow()
mayaWidget = wrapInstance(long(ptr), QtWidgets.QWidget)
_uiName = 'CalculatorUi'

class Ui(QtWidgets.QMainWindow):
	def __init__(self, parent=None):
		super(Ui, self).__init__(parent)
	        self.title = 'Calculator v.01'
	        self.left = 600
	        self.top = 250
	        self.width = 200
	        self.height = 250
	        self.mainUi()
	        self.input_signal()

	def mainUi(self):
		self.setWindowTitle(self.title)
		self.setGeometry(self.left, self.top, self.width, self.height)
		## Main Layout
		self.windowLayout = QVBoxLayout()
		self.horizontalGroupBox = QGroupBox("Calculator v 0.1 : ")

		self.displayProcessLayout = QVBoxLayout()
		self.displayProcess = QLineEdit()
		self.displayProcess.setReadOnly(True)
		self.displayProcess.setAlignment(QtCore.Qt.AlignRight)
		self.displayProcessLayout.addWidget(self.displayProcess)
		self.windowLayout.addLayout(self.displayProcessLayout) 

		self.displayLayout = QVBoxLayout()
		self.display = QLineEdit()
		self.display.setReadOnly(True)
		self.display.setAlignment(QtCore.Qt.AlignRight)
		self.displayLayout.addWidget(self.display)
		self.windowLayout.addLayout(self.displayLayout) 

		self.layoutClearLayout = QVBoxLayout()
		self.buttonClear = QtWidgets.QPushButton('Clear')
		self.layoutClearLayout.addWidget(self.buttonClear)
		self.windowLayout.addLayout(self.layoutClearLayout)

        ## Button
		self.layoutButton = QGridLayout()
		## Row 1
		self.button7 = QtWidgets.QPushButton('7')
		self.layoutButton.addWidget(self.button7,0,0)
		self.button8 = QtWidgets.QPushButton('8')
		self.layoutButton.addWidget(self.button8,0,1)
		self.button9 = QtWidgets.QPushButton('9')
		self.layoutButton.addWidget(self.button9,0,2)
		self.buttonDivice = QtWidgets.QPushButton('/')
		self.layoutButton.addWidget(self.buttonDivice,0,3)
		self.buttonBack = QtWidgets.QPushButton('c')
		self.layoutButton.addWidget(self.buttonBack,0,4)
		## Row 2
		self.button4 = QtWidgets.QPushButton('4')
		self.layoutButton.addWidget(self.button4,1,0)
		self.button5 = QtWidgets.QPushButton('5')
		self.layoutButton.addWidget(self.button5,1,1)
		self.button6 = QtWidgets.QPushButton('6')
		self.layoutButton.addWidget(self.button6,1,2)
		self.buttonMulti = QtWidgets.QPushButton('x')
		self.layoutButton.addWidget(self.buttonMulti,1,3)
		self.buttonTubleIn = QtWidgets.QPushButton('(')
		self.layoutButton.addWidget(self.buttonTubleIn,1,4)
		## Row 3
		self.button1 = QtWidgets.QPushButton('1')
		self.layoutButton.addWidget(self.button1,2,0)
		self.button2 = QtWidgets.QPushButton('2')
		self.layoutButton.addWidget(self.button2,2,1)
		self.button3 = QtWidgets.QPushButton('3')
		self.layoutButton.addWidget(self.button3,2,2)
		self.buttonDel = QtWidgets.QPushButton('-')
		self.layoutButton.addWidget(self.buttonDel,2,3)
		self.buttonTubleOut = QtWidgets.QPushButton(')')
		self.layoutButton.addWidget(self.buttonTubleOut,2,4)
		## Row 4
		self.buttonZero = QtWidgets.QPushButton('0')
		self.layoutButton.addWidget(self.buttonZero,3,0)
		self.buttonZero2 = QtWidgets.QPushButton('00')
		self.layoutButton.addWidget(self.buttonZero2,3,1)
		self.buttonDot = QtWidgets.QPushButton('.')
		self.layoutButton.addWidget(self.buttonDot,3,2)
		self.buttonPlus = QtWidgets.QPushButton('+')
		self.layoutButton.addWidget(self.buttonPlus,3,3)
		self.buttonAnswer = QtWidgets.QPushButton('=')
		self.layoutButton.addWidget(self.buttonAnswer,3,4)

		self.windowLayout.addLayout(self.layoutButton) 

		self.horizontalGroupBox.setLayout(self.windowLayout)
		self.setCentralWidget(self.horizontalGroupBox)


	def input_signal(self):
		self.button1.clicked.connect(partial(self.sent_signal, '1'))
		self.button2.clicked.connect(partial(self.sent_signal, '2'))
		self.button3.clicked.connect(partial(self.sent_signal, '3'))
		self.button4.clicked.connect(partial(self.sent_signal, '4'))
		self.button5.clicked.connect(partial(self.sent_signal, '5'))
		self.button6.clicked.connect(partial(self.sent_signal, '6'))
		self.button7.clicked.connect(partial(self.sent_signal, '7'))
		self.button8.clicked.connect(partial(self.sent_signal, '8'))
		self.button9.clicked.connect(partial(self.sent_signal, '9'))
		self.buttonZero.clicked.connect(partial(self.sent_signal, '0'))
		self.buttonZero2.clicked.connect(partial(self.sent_signal, '00'))
		self.buttonTubleIn.clicked.connect(partial(self.sent_signal, '('))
		self.buttonTubleOut.clicked.connect(partial(self.sent_signal, ')'))
		self.buttonPlus.clicked.connect(partial(self.sent_signal, '+'))
		self.buttonDel.clicked.connect(partial(self.sent_signal, '-'))
		self.buttonMulti.clicked.connect(partial(self.sent_signal, 'x'))
		self.buttonDivice.clicked.connect(partial(self.sent_signal, '/'))
		self.buttonBack.clicked.connect(partial(self.cancle_signal))
		self.buttonDot.clicked.connect(partial(self.sent_signal, '.'))
		self.buttonAnswer.clicked.connect(partial(self.result))
		self.buttonClear.clicked.connect(partial(self.sent_clear))

		# self.display.returnPressed.connect(self.display_enter)

	def sent_signal(self, inputValue):
		value = self.display.text()
		value += inputValue
		self.display.setText(value)
		return value

	def cancle_signal(self):
		obj = []
		value = self.display.text()
		for each in value:
			obj += each
		obj.pop(-1)
		resultCancle = ''
		for each in obj:
			resultCancle += str(each)
		value =  resultCancle
		self.display.setText(value)
		return value

	def checkDisplay(self):
		value = self.display.text()
		math = ['+', '-', 'x', '/']
		for each in math:
			if each == '+':
				obj = value.split(each)
				if len(obj):
					answer = objA + objB
		print answer
		
	def display_enter(self):
		text = self.display.text()
		print text

	def sent_clear(self):
		self.display.setText('')
		self.displayProcess.setText('')

	def result2(self, inputa, math, inputb):
		## clean display	
		#self.displayProcess.setText('')
		if math == '+':
			resultWip = float(inputa) + float(inputb)
		print resultWip 

	def result(self):
		## clean display	
		self.displayProcess.setText('')

		objAll = []
		resultFinal = []		
		value = self.display.text()
		math = ['+', '-','x', '/']
		mathObj = []
		for i in range(len(math)):
			if math[i] in value:
				mathObj.append(math[i])
		for each in mathObj:
			objAll = value.split(each)
			if each == '+':
				resultFinal = float(objAll[0]) + float(objAll[-1])
			elif each == '-':
				resultFinal = float(objAll[0]) - float(objAll[-1])
			elif each == 'x':
				resultFinal = float(objAll[0]) * float(objAll[-1])
			elif each == '/':
				resultFinal = float(objAll[0]) % float(objAll[-1])
		volue = float(resultFinal)

		self.displayProcess.setText(str(volue))
		return volue


def show(self):
	self.deleteUi(_uiName)
	ui = Ui(mayaWidget)
	ui.show()


def deleteUi(self, uiName):
	import pymel.core as pm
	if pm.window ( _uiName , exists = True ) :
		pm.deleteUI ( _uiName ) ;