import maya.cmds as mc
import maya.mel as mm
import os,sys,traceback
# from PyQt4 import QtGui, QtCore
# from PyQt4.QtGui import*
# from PyQt4.QtCore import*
# from qtshim import QtCore, QtGui
# from qtshim import Signal as SIGNAL
# from qtshim import wrapinstance

def cShade():
    fileExt  = ".mb"
    fileType = "mayaBinary"
    tmpFile = "/maya_sceneClipBoard" + fileExt
    tmpdir = mm.eval("getenv TMPDIR")
    shadeFile = (tmpdir + tmpFile)
    edlFile = (tmpdir + "/maya_sceneClipBoard.txt")
    shadeGrp  = [n for n in mc.ls(type='shadingEngine')if not n == 'initialParticleSE' and not n =='initialShadingGroup' and not mc.referenceQuery(n,inr=1)]
    #shadeFile = os.path.join(shadeFilePath,'%s.ma' % publishVer.replace('uv','uv_shadeFile'))
    edlDict = ''

    materials = []
    if shadeGrp:
        for grp in shadeGrp:
            if  mc.listConnections('%s.surfaceShader' % grp):
                material =  mc.listConnections('%s.surfaceShader' % grp)[0]
                edlDict += '%s' % material # get materail
                materials.append('%s' % material)
                mc.hyperShade(o= material)
                for sl in mc.ls(sl=True):
                    if ':' in sl:
                        sl = sl.replace(sl.split(":")[0]+':','')
                    edlDict += ' %s' % sl # get object
                edlDict += '\r\n'


        f = open(edlFile,'w')
        f.write(edlDict)
        f.close                    

        if shadeGrp:
            mc.select(shadeGrp,noExpand=True)

        mc.file(shadeFile,force=1,
                exportSelected =1,
                constructionHistory=1,
                channels=1,
                expressions=1,
                constraints=1,
                shader=1,
                type =fileType)

        mm.eval('print "Exported shade"')


def pShade():
    fileExt  = ".mb"
    fileType = "mayaBinary"
    tmpFile = "/maya_sceneClipBoard" + fileExt
    tmpdir = mm.eval("getenv TMPDIR")
    shadeFile = (tmpdir + tmpFile)
    edlFile = (tmpdir + "/maya_sceneClipBoard.txt")

    missGeo = ''
    namespace = ''
    sel = mc.ls(sl=True)
    if sel:
        if ':' in sel[0]:
            namespace = sel[0].split(':')[0]
        if os.path.exists(edlFile):

            mc.file(shadeFile,i=True, loadReferenceDepth="all",pr=True,options ="v=0")

            f = open(edlFile,'r')
            txt = f.read()
            f.close()
            for t in txt.split('\r\n'):
                geo = []
                if t.split(' ')[1:]:
                    mc.select(cl=True)
                    for face in t.split(' ')[1:]:
                        if namespace:
                            if not face[:len(namespace)] == ('%s:' % namespace):
                                face = face.split('|')
                                face = ('|%s:'%namespace).join(face)
                                face = '%s:%s' % (namespace,face)
                        try:
                            mc.select(face ,r=True)
                            geo.append(face)

                        except:
                            # face
                            missGeo += '%s\n' % (face)
                    if geo: 
                        mc.select(geo,r=True)
                        mc.hyperShade(assign=t.split(' ')[0] )  
                    mc.select(cl=True)
            print '\n___________________________________\nGeo is missing.\n___________________________________\n%s' % missGeo
        else:
            QtGui.QMessageBox.warning(None,'Assign Shade Window','Hasn''t edl file.',QMessageBox.AcceptRole, QMessageBox.NoButton)