######Setup
from utaTools.utapy import utaCore
reload(utaCore)
import maya.cmds as mc
reload(mc)
import pymel.core as pm
from utaTools.pkmel import rigTools
reload( rigTools )
from utaTools.utapy import renameMiarmy
reload(renameMiarmy)
'''
## Help Miarmy
## Create Original Agent
	McdParseRootBoneCreateOAgent(0)
## Save Original Agent Setup
	McdSaveOAAttrSetup()
## sent Active Geo to Original Agent
	CopyGeoFromSetupAndSkinToOA(0)


'''

## -------------------------------------------------- Create Prep ---------------------------------------------------------------------------
## -------------------------------------------------- Create Prep ---------------------------------------------------------------------------

# def prepMiarmy(*args):
# 	## 01.Create Miarmy Ready
# 	McdSelectMcdGlobal(0)
# 	## 02.Create Parent SkinGrp and GeoGrp to miarmy_Contents
# 	setupGrp = mc.ls('Setup_*')
# 	geometryGrp = mc.ls('Geometry_*')
# 	mc.parent('SkinJnt_Grp', setupGrp)
# 	mc.parent('CharGeo_Grp', setupGrp)
# 	## 03.Create Original Agent
# 	McdParseRootBoneCreateOAgent(0)


# 	## 04. Create Scale DummyShape Cube
# 	from rf_maya. rftool.rig.miarmyTool.utils import mmAgentUtils
# 	reload(mmAgentUtils)
# 	import re
# 	mmAgentUtils.mulSnapMMBox(locoName = 'loco')
# 	mmAgentUtils.mulLimbMMBox('loco')


##------------------------------------------------------ Create OA -------------------------------------------------------------------------------------------
##------------------------------------------------------ Create OA -------------------------------------------------------------------------------------------

def miarmyProxyShapeRun(*args):
	baseJnt, countBaseJnt, proxyGrp = checkBaseConstraint(nameGrp = 'PrGeo_Grp', elem = '_Grp', namePass = '_Geo')
	geoNew = combineGeo(nameGrp = proxyGrp)
	dummyShapeAll, countDummyShapeAll = checkDummyShape(baseJnt = baseJnt)

	proxyGeoAll = []
	for each in proxyGrp:
		proxyGeoList = utaCore.listGeo(sels = each, nameing = '_Geo', namePass = '_Grp')
		proxyGeo = proxyGeoList[0].split('|')[-1]
		proxyGeoAll.append(proxyGeo)
	print len(proxyGeoAll)
	transferProxyGeoToDummyShape(dummyShape = dummyShapeAll, proxyGrp = proxyGeoAll)
	# scaleDummyShape(dummyShape = dummyShapeAll, proxyGrp = proxyGeoAll)

def checkBaseConstraint(nameGrp = 'PrGeo_Grp', elem = '_Grp', namePass = '_Geo', *args):
	grp = utaCore.listGeo(sels = nameGrp, nameing = elem, namePass = namePass)
	baseJnt = []
	proxyGrp = []

	for each in grp:
		if '_parentConstraint' in each:
			pass
		elif '_scaleConstraint' in each:
			pass
		else:
			nodeCon = mc.listConnections(each, d = True) 

			if nodeCon == None:
				pass
			else:
				nodeCheckAll = mc.listAttr(nodeCon[0], s = True)

				## Check Group Constraint
				grpCheck = each.split('|')[-1]
				proxyGrp.append(grpCheck)

				## Check BaseJoint Parent Constraint
				for each in nodeCheckAll:
					if '_JntW0' in each: 
						joint = each.replace('W0', '') 
						baseJnt.append(joint)

	countBaseJnt =  len(baseJnt)
	# print len(proxyGrp)
	# print proxyGrp, '...conGrp'
	return baseJnt, countBaseJnt, proxyGrp


def combineGeo(nameGrp = [], *args):

	for combineEach in nameGrp:
		## Split Name
		nameSp = utaCore.splitName(sels = [combineEach])
		name = nameSp[0]
		side = nameSp[1]
		lastName = nameSp[-1]

		## List Geometry 
		geoAll = utaCore.listGeo(sels = combineEach, nameing = '_Geo' , namePass = '_Grp')
		if len(geoAll) >= 2:
			mc.select(geoAll)
			geoNew = mc.polyUnite( n='{}{}Geo'.format(name, side))[0]

			## Clear Grp empty
			emptyGrp = utaCore.listGeo(sels = combineEach, nameing = '_Geo' , namePass = '_Grp')
			mc.select(emptyGrp)
			mc.delete()

			## Parent 
			mc.parent(geoNew, combineEach)
			utaCore.centerPivotObj (obj = [geoNew])
		else:
			geoNew = True
	mc.select(cl = True)

	return geoNew

def checkDummyShape(baseJnt = [], *args):
	dummyShapeAll = []
	for eachBaseJnt in baseJnt:
		## Split Name
		nameSp = utaCore.splitName(sels = [eachBaseJnt])
		name = nameSp[0]
		side = nameSp[1]
		lastName = nameSp[-1]

		dymmyShapeName = '{}{}{}_dummyShape_loco'.format(name, side, lastName)
		dummyShapeAll.append(dymmyShapeName)

	# mc.select(dummyShapeAll)
	countDummyShapeAll = len(dummyShapeAll)
	return dummyShapeAll, countDummyShapeAll


def transferProxyGeoToDummyShape(dummyShape = [], proxyGrp = [], *args):
	jointPosAll = []
	for i in range(len(dummyShape)):
		## Create Guide Transform Joint of proxy
		utaCore.centerPivotObj (obj = [proxyGrp[i]])
		poseLocator = mc.xform(proxyGrp[i], q=True, t=True)    # find xform (x,y,z) 
		jntFrLoc = mc.joint (proxyGrp[i], p=(poseLocator[0],poseLocator[1],poseLocator[2]), n = '{}_Jnt'.format(proxyGrp[i]))
		print jntFrLoc,'...jntFrLoc'
		mc.parent(w = True)
		utaCore.snapObj(objA = proxyGrp[i], objB = jntFrLoc)
		## Create connection inMesh and outMash
		mc.connectAttr('{}Shape.outMesh'.format(proxyGrp[i]), '{}Shape.inMesh'.format(dummyShape[i]))
		mc.setAttr('{}.v'.format(dummyShape[i]), 1)
		utaCore.centerPivotObj (obj = [dummyShape[i]])
		utaCore.snapObj(objA = jntFrLoc, objB = dummyShape[i])

		## addLambert lambert 1
		utaCore.addLambert(lisObj = [dummyShape[i]])

		jointPosAll.append(jntFrLoc)

	## Delete joint Position
	mc.delete(jointPosAll)


def scaleDummyShape(dummyShape = [], proxyGrp = [], *args):
	jointPosAll = []
	for i in range(len(dummyShape)):

		mc.setAttr('{}.v'.format(dummyShape[i]), 1)

		utaCore.centerPivotObj (obj = [dummyShape[i]])
		poseLocator = mc.xform(dummyShape[i], q=True, t=True)    # find xform (x,y,z) 
		jntFrLoc = mc.joint (dummyShape[i], p=(poseLocator[0],poseLocator[1],poseLocator[2]), n = '{}_Jnt'.format(proxyGrp[i]))
		mc.parent(w = True)
		utaCore.snapObj(objA = dummyShape[i], objB = jntFrLoc)


		bb = mc.xform(proxyGrp[i], q=True, ws=True, bb=True)
		center = mc.objectCenter(proxyGrp[i], gl=True)

		# sy = bb[3] - bb[0]
		# sx = bb[4] - bb[1]
		# sz = bb[5] - bb[2]

		w = bb[3] - bb[0]
		sy = bb[4] - bb[1]
		sz = bb[5] - bb[2]


		# cube = mc.polyCube(ch=False)[0]
		mc.setAttr('{}.tx'.format(dummyShape[i]), center[0])
		mc.setAttr('{}.ty'.format(dummyShape[i]), center[1])
		mc.setAttr('{}.tz'.format(dummyShape[i]), center[2])
		mc.setAttr('{}.sx'.format(dummyShape[i]), sx)
		mc.setAttr('{}.sy'.format(dummyShape[i]), sy)
		mc.setAttr('{}.sz'.format(dummyShape[i]), sz)

		utaCore.snapObj(objA = jntFrLoc, objB = dummyShape[i])

		jointPosAll.append(jntFrLoc)

	## Delete joint Position
	mc.delete(jointPosAll)



def createDetailJnt(detailGrp = 'deerBuckDefaultHairA_001:DtlRigSkin_Grp', parentJnt = 'ctrl_md:Head_Jnt', *args):

	detailJntAll = []
	dtlJnt = utaCore.listGeo(sels = detailGrp, nameing = '_Jnt', namePass = '_Grp')
	for eachDtlJnt in dtlJnt:
		jntSp = eachDtlJnt.split('|')[-1]
		detailJntAll.append(jntSp)

	for each in detailJntAll:
		if not 'Dtl_Jnt' in each:
			nameSpaceObj = each.split(':')
			refSp = nameSpaceObj[0]
			nameObj = nameSpaceObj[-1]
			nameSp = utaCore.splitName(sels = [nameObj])

			## nameSpace
			name = nameSp[0]
			side = nameSp[1]
			lastName = nameSp[-1]
			refSp = nameSpaceObj[0]

			## duplicate Joint
			dtlPosJnt = mc.duplicate(each, n = '{}{}{}'.format(name, side, lastName))
			mc.select(dtlPosJnt)
			dtlPosJntGrp = mc.rename(rigTools.doZeroGroup(),'{}Jnt{}Grp'.format(name, side))
			mc.parent(dtlPosJntGrp, parentJnt)
			mc.parentConstraint('{}:{}{}ctrl'.format(refSp, name, side), dtlPosJntGrp)
			mc.scaleConstraint('{}:{}{}ctrl'.format(refSp, name, side), dtlPosJntGrp)
    

def createRealTimeMiarmy():
	sels = mc.ls('MdGeo_Grp')
	objLists = utaCore.listGeo(sels = sels, nameing = '_Geo', namePass = '_Grp')
	for x in range(len(objLists)):

	    geo = objLists[x].split('|')[-1]
	    checkAttr = mc.ls('{}Shape.McdRTDisplay'.format(geo))[0]

	    if len(checkAttr) != 0:
	        print 'Mark Selected Geo Real Time Displayable is Already : {}'.format(x+1)
	        mc.select(cl = True)
	    else:
	        mc.select(each)
	        McdAddRTDisplayFlag()


def checkXform():
	geo = ['pCube3', 'pSphere2']
	sels = mc.ls(sl=True, l=True)
	#valutAObj = []
	#valueBobj = []
	for sel in sels:
	    bb = mc.xform(sel, q=True, ws=True, bb=True)
	    center = mc.objectCenter(sel, gl=True)    
	    sx = bb[3] - bb[0]
	    sy = bb[4] - bb[1]
	    sz = bb[5] - bb[2]   
	    valutAObj = sx + sy + sz
	    for each in geo:
	        bb = mc.xform(each, q=True, ws=True, bb=True)
	        center = mc.objectCenter(each, gl=True)        
	        sx = bb[3] - bb[0]
	        sy = bb[4] - bb[1]
	        sz = bb[5] - bb[2]        
	        valutBObj = sx + sy + sz
	        if valutAObj == valutBObj:
	            print 'True'
	        else:
	            print 'else'


def miarmyParentPrep():
	skinGrp = mc.ls('*:SkinJnt_Grp')[0]
	charGeo = mc.ls('*:CharGeo_Grp')[0]
	rigGrp = mc.ls('*:Rig_Grp_*')[0]
	setupLoco = 'Setup_loco'
	grpAll = [skinGrp, charGeo, rigGrp]
	for each in grpAll:
		if each:
			if setupLoco:
				mc.parent(each, setupLoco)
	# mc.parent(skinGrp, charGeo, rigGrp, setupLoco)
	mc.select(cl = True)
	return charGeo


def oaBlendShapeAuto():
    agentObj = (mc.ls('Agent_*')[0]).split('Agent_')[-1]

    newCreatureNameText = (mc.ls('*:Rig_Grp_*')[0]).split('Rig_Grp_')[-1]
    nameSpaceObj = (mc.ls('*:Rig_Grp_*')[0]).split(':')[0]
    mdGeoGrp = mc.ls('*:MdGeo_Grp')[0]
    newCreatureName = renameMiarmy.renameMM(name = agentObj,newCreatureName = newCreatureNameText)
    
    attr = ['tx','ty','tz','rx','ry','rz','sx','sy','sz']
    objBsh = ['Head_Jnt','EyeLFT_Jnt','EyeRGT_Jnt','Eye_L_Jnt','Eye_R_Jnt','JawLwr1_Jnt','JawLwr2_Jnt']
    for each in range(len(objBsh)):
        obj = '{}_ogb_{}'.format(objBsh[each],newCreatureNameText)
        if mc.objExists(each):
	        for cnnt in attr:
	            mc.connectAttr(obj+'.{}'.format(cnnt) , '{}:{}.{}'.format(nameSpaceObj, objBsh[each], cnnt))

    utaCore.checkBlendShape(selGrp = mdGeoGrp, locoName = newCreatureName)

    return nameSpaceObj, newCreatureNameText




## -------------------------------------------------------------- Spare ---------------------------------------------------------------------
## -------------------------------------------------------------- Spare ---------------------------------------------------------------------

# def proxyParent(*args):
# 	jntAll = utaCore.listGeo(sels = 'OriginalAgent_loco', nameing = '_loco')
# 	for each in jntAll:
# 	    obj = each.split('|')[-1]
# 	    if '_Jnt_' in obj:
# 	        nameJnt = utaCore.splitName(sels = [obj])
# 	        proxyGeo = '{}_Proxy{}Geo'.format(nameJnt[0], nameJnt[1]) 
# 	        if mc.objExists(proxyGeo):
# 	            print proxyGeo, 'proxyGeo'
# 	            mc.parent(proxyGeo, each)
# 	        else:
# 	            pass
# 	            print 'False'

# def checkDummyShape(selsGrp = 'OriginalAgent_loco', elemSp = '_dummyShape_loco', *args):
# 	jntAll = utaCore.listGeo(sels = selsGrp, nameing = elemSp)
# 	dummyShapeAll = []
# 	for each in jntAll:
# 	    obj = each.split('|')[-1]
# 	    if '_Jnt_' in obj:
# 	        nameJnt = utaCore.splitName(sels = [obj])
# 	        name = nameJnt[0]
# 	        side = nameJnt[1]
# 	        if side == '_Jnt_':
# 	            side = '_'
# 	        dummyShape = '{}{}Jnt_dummyShape_loco'.format(name, side)
# 	        if mc.objExists(dummyShape):
          
# 	            mc.setAttr('{}.v'.format(dummyShape), 1)
# 	            dummyShapeAll.append(dummyShape)
# 	        else:
# 	            pass
# 	countDummyShape = len(dummyShapeAll)
# 	return dummyShapeAll, countDummyShape

def customPlace(placeName):
	allLoc = mc.ls(sl = True)
	mc.setAttr(placeName + 'Shape.numOfAgent', len(allLoc))
	for i in range(0, len(allLoc)):
		tdata = mc.xform(allLoc[i], q = True, ws = True, piv = True)
		# print tdata, '..tda6ta'
		mc.setAttr('{}.placement[{}].agentPlace[1]'.format(placeName, str(i), tdata[0]))
		mc.setAttr('{}.placement[{}].agentPlace[2]'.format(placeName, str(i), tdata[1]))
		mc.setAttr('{}.placement[{}].agentPlace[3]'.format(placeName, str(i), tdata[2]))
		# mc.setAttr(placeName + '.placement[' + str(i) + '].agentPlace[1]',tdata[0])
		# mc.setAttr(placeName + '.placement[' + str(i) + '].agentPlace[2]',tdata[1])
		# mc.setAttr(placeName + '.placement[' + str(i) + '].agentPlace[3]',tdata[2])
		mc.setAttr('{}.placement[{}].agentPlace[5]'.format(placement, str(i)) ,mc.getAttr('{}.rotateY'.format(allLoc[i])) + 180)

		mc.setAttr(placeName + '.placement[' + str(i) + '].agentPlace[5]',mc.getAttr(allLoc[i] + '.rotateY') + 180)
		# print placeName + '.placement[' + str(i) + '].agentPlace[1]',tdata[0]
		# print placeName + '.placement[' + str(i) + '].agentPlace[2]',tdata[1]
		# print placeName + '.placement[' + str(i) + '].agentPlace[3]',tdata[2]
		# print placeName + '.placement[' + str(i) + '].agentPlace[5]'
		# print allLoc[i] + '.rotateY'
# customPlace('McdPlace0')