import maya.cmds as mc
import random
import math
 
def lockHideAttrs(obj = '', attrArray = [], lock = False, hide = True):
    sels = mc.ls(sl=True)
    if mc.objExists (obj==True):
        for each in obj :
            for attr in attrArray :
                mc.setAttr('%s.%s' % (each, attr), l = lock, k = hide)
    else: 
        obj = sels
        for each in obj :
            for attr in attrArray :
                mc.setAttr('%s.%s' % (each, attr), l = lock, k = hide)
#Run               
# lockHideAttrs(obj = '', attrArray = ['tx', 'ty', 'tz','rx', 'ry', 'rz', 'sx', 'sy', 'sz', 'v'], lock = 0)