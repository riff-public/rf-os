# System modules
import sys
import os
import re
import subprocess

# QT modules
from PySide import QtCore , QtGui
from functools import partial

# UI modules
import ui.ui as ui
reload( ui )

import cmd
reload( cmd )

MAYA_PYTHON = r'C:\Program Files\Autodesk\Maya2015\bin\mayapy.exe'

def getTaskFromFilePath( filePath='' ) :

	# Get task name from file path
	pathElms = filePath.split( os.sep )
	flNm , flExt = os.path.splitext( pathElms[-1] )
	stepName = pathElms[-4]
	fldName = pathElms[-5]
	
	task = None
	
	exp = r'%s_%s_(.+?)(_v\d{3}|$)' % ( fldName , stepName )
	m = re.search( exp , flNm )
	if m :
		task = str( m.group(1) )

		exp = r'v\d{3}'
		m = re.search( exp , task )

		if m :
			task = None

	return task

def getNextTaskVersion( folderPath='' , taskName='' ) :

	# Get the next version of ".ma" file with given "taskName" in given folderPath.
	# Return v001 if folder has no given taskName.
	if taskName :
		currTask = '_%s_' % taskName
	else :
		currTask = '_'

	currPath = os.path.normpath( folderPath )
	pathElms = currPath.split( os.sep )
	currFldName = pathElms[-4]
	currStepName = pathElms[-3]

	fls = [ f for f in os.listdir( currPath ) if os.path.isfile( os.path.join( currPath , f ) ) ]

	taskFiles = []

	for fl in fls :
		exp = r'%s_%s%sv(\d{3})' % ( currFldName , currStepName , currTask )
		m = re.search( exp , fl )
		if m :
			taskFiles.append( int( m.group(1) ) )

	nextVer = 1

	if taskFiles :
		nextVer = sorted( taskFiles )[-1] + 1

	nextVerPath = os.path.join(
									folderPath ,
									'%s_%s%sv%03d.ma' % ( currFldName , currStepName , currTask , nextVer )
								)

	return nextVerPath

class RigFileListWidget( QtGui.QListWidget ) :

	def __init__( self , type , parent=None ) :
		
		super(DragDropListWidget, self).__init__(parent)
		self.setAcceptDrops(True)

	def dragEnterEvent( self , event ) :
		
		if event.mimeData().hasUrls :
			event.accept()
		else:
			event.ignore()

	def dragMoveEvent( self , event ) :
		
		if event.mimeData().hasUrls :
			event.setDropAction( Qt.CopyAction )
			event.accept()
		else:
			event.ignore()

	def dropEvent(self, event):
		
		if event.mimeData().hasUrls :
			event.setDropAction( Qt.CopyAction )
			event.accept()
			self.emit( SIGNAL("dropped") , event.mimeData().urls()[0] )
		else:
			event.ignore()

class RigBrowserUi( QtGui.QMainWindow , ui.Ui_rigBrowserMainWindow ) :
	
	def __init__( self , parent=None ) :
		
		super( RigBrowserUi , self ).__init__( parent )
		self.setupUi( self )

class RigBrowser( object ) :

	def __init__( self , parent=None ) :

		self.ui = RigBrowserUi( parent )

		# File menu
		# File menu - Open
		fileOpenAction = QtGui.QAction( 'Open' , self.ui )
		fileOpenAction.setShortcut( 'Alt+O' )
		fileOpenAction.triggered.connect( self.openFile )
		self.ui.menuFile.addAction( fileOpenAction )

		# File menu - Save
		self.fileSaveAction = QtGui.QAction( 'Save' , self.ui )
		self.fileSaveAction.setShortcut( 'Alt+S' )
		self.ui.menuFile.addAction( self.fileSaveAction )
		self.fileSaveAction.setEnabled( False )
		
		# File menu 
		self.ui.menuFile.addSeparator()

		# File menu - Publish
		self.pubAction = QtGui.QAction( 'Publish' , self.ui )
		self.pubAction.setShortcut( 'Alt+P' )
		self.pubAction.triggered.connect( self.pubFile )
		self.ui.menuFile.addAction( self.pubAction )

		self.ui.menuFile.addSeparator()

		# File menu - Exit
		exitAction = QtGui.QAction( 'Exit' , self.ui )
		exitAction.triggered.connect( self.ui.close )
		self.ui.menuFile.addAction( exitAction )

		# Filter popup menu
		self.ui.filterLineEdit.setContextMenuPolicy( QtCore.Qt.CustomContextMenu )
		self.ui.filterLineEdit.customContextMenuRequested.connect( self.taskPopMenu )
		
		self.taskPopMenuWdgt = QtGui.QMenu( self.ui )
		
		# Connections
		self.ui.cwdLineEdit.editingFinished.connect( self.cwdChanged )
		self.ui.filterLineEdit.textChanged.connect( self.popFile )
		
		self.ui.fileListWidget.itemDoubleClicked.connect( self.openFile )
		
		# tmpPath = os.path.normpath( r'P:\Lego_Friends\asset\3D\character\main\frd_oliviaSchool\rig\maya\work' )
		# self.ui.cwdLineEdit.setText( tmpPath )
		# self.popFile()
		# self.popTask()
	
	def cwdChanged( self ) :
		
		self.popFile()
		self.popTask()
		self.ui.filterLineEdit.setText( '' )
	
	def pubFile( self ) :
		
		currPath = os.path.normpath( str( self.ui.cwdLineEdit.text() ) )
		selItems = [ str( x.text() ) for x in self.ui.fileListWidget.selectedItems() ]
		
		for selItem in selItems :

			currFlExt = selItem

			filePath = os.path.join( currPath , currFlExt )
			mdlFldPath , mdlFl = os.path.split( __file__ )
			mayaCmdFlPath = os.path.join(
											mdlFldPath ,
											'mayapyPub.py'
										)
			
			subprocess.call(
								[
									MAYA_PYTHON ,
									mayaCmdFlPath ,
									filePath
								]
							)
		
		self.popFile()
	
	def openFile( self ) :

		currPath = os.path.normpath( str( self.ui.cwdLineEdit.text() ) )
		currFile = str( self.ui.fileListWidget.currentItem().text() )

		filePath = os.path.join( currPath , currFile )
		os.startfile( filePath )

	def popFile( self ) :
		
		# Populate files in current folder.
		self.ui.fileListWidget.clear()
		
		# fltrState = self.ui.filterCheckBox.checkState()
		fltr = str( self.ui.filterLineEdit.text() )
		currPath = os.path.normpath( str( self.ui.cwdLineEdit.text() ) )
		
		if os.path.exists( currPath ) :
			# All files in "currPath" folder
			fls = [ os.path.splitext( f )[0] for f in os.listdir( currPath ) if os.path.join( currPath , f ).endswith( '.ma' ) ]
			
			for fl in sorted( fls ) :
				
				flPath = os.path.join( currPath , fl )
				currTask = getTaskFromFilePath( flPath )
								
				if fltr :
					if fltr == currTask :
						self.ui.fileListWidget.addItem( '%s.ma' % fl )
				else :
					self.ui.fileListWidget.addItem( '%s.ma' % fl )
	
	def popTask( self ) :

		self.taskPopMenuWdgt.clear()

		# Populate all task in current folder.
		fltr = str( self.ui.filterLineEdit.text() )
		currPath = os.path.normpath( str( self.ui.cwdLineEdit.text() ) )
		tasks = []

		# Get all task names from file names
		if os.path.exists( currPath ) :
			fls = [ f for f in os.listdir( currPath ) if os.path.isfile( os.path.join( currPath , f ) ) ]
			if fls :
				for fl in fls :
					
					flPath = os.path.join( currPath , fl )
					currTask = getTaskFromFilePath( flPath )
					
					if not currTask in tasks : tasks.append( currTask )

		# Add all collected tasks to filter popup menu
		if tasks :
			self.taskPopMenuWdgt.addAction( QtGui.QAction( '...' , self.ui ) )
			for task in tasks :
				if task :
					self.taskPopMenuWdgt.addAction( QtGui.QAction( str( task ) , self.ui ) )

	def taskPopMenu( self , pos ) :

		action = self.taskPopMenuWdgt.exec_( self.ui.filterLineEdit.mapToGlobal( pos ) )
		
		if action :
			if action.text() == '...' :
				self.ui.filterLineEdit.setText( '' )
			else :
				self.ui.filterLineEdit.setText( action.text() )

class RigBrowserMaya( RigBrowser ) :

	def __init__( self , parent=None ) :

		super( RigBrowserMaya , self ).__init__( parent )

		# Custom maya module
		global rmc
		import mayaCmd as rmc

		# Maya module
		global mc
		import maya.cmds as mc
		global mm
		import maya.mel as mm

		self.user = mm.eval( 'optionVar -q "PTuser"' )

		# Startup cwd
		mayaFilePath = mc.file( q=True , location=True )
		
		if mayaFilePath :
			cleanedPath = os.path.normpath( mayaFilePath )
			fldPath , sceneNmExt = os.path.split( cleanedPath )
			self.ui.cwdLineEdit.setText( fldPath )
		
		# Enable Save menu
		self.fileSaveAction.triggered.connect( self.saveTask )
		self.fileSaveAction.setEnabled( True )
		
		# Maya menu
		self.menuMaya = QtGui.QMenu( self.ui.menubar )
		self.menuMaya.setTitle( 'Maya' )
		self.ui.menubar.addAction( self.menuMaya.menuAction() )
		
		# Maya menu- Save To Project
		self.saveToProjectAction = QtGui.QAction( 'Save to project' , self.ui )
		self.saveToProjectAction.setShortcut( 'Alt+Shift+S' )
		self.menuMaya.addAction( self.saveToProjectAction )
		
		# Maya menu - Save to hero file
		sv2HrAction = QtGui.QAction( 'Save to Hero' , self.ui )
		sv2HrAction.setShortcut( 'Alt+Shift+P' )
		sv2HrAction.triggered.connect( self.sv2Hr )
		self.menuMaya.addAction( sv2HrAction )

		# Maya menu - Reference
		fileRefAction = QtGui.QAction( 'Reference' , self.ui )
		fileRefAction.setShortcut( 'Alt+R' )
		fileRefAction.triggered.connect( self.refFile )
		self.menuMaya.addAction( fileRefAction )

		# Maya menu - Import
		fileImportAction = QtGui.QAction( 'Import' , self.ui )
		fileImportAction.setShortcut( 'Alt+I' )
		fileImportAction.triggered.connect( self.importFile )
		self.menuMaya.addAction( fileImportAction )

		self.saveToProjectAction.triggered.connect( self.saveToProject )

		if mayaFilePath :
			self.popFile()

	def sv2Hr( self ) :

		fldPath , flNmExt = os.path.split( os.path.normpath( mc.file( q=True , location=True ) ) )
		fldElms = fldPath.split( os.sep )
		fldNm = fldElms[-4]
		task = fldElms[-3]
		flNm , flExt = os.path.splitext( flNmExt )

		exp = '(%s_%s_.*)(_v\d{3})' % ( fldNm , task )

		m = re.search( exp , flNm )
		pubFlNm = m.group(1)

		pubFlPath = os.path.join(
									fldPath ,
									'%s.ma' % pubFlNm
								)

		rmc.importRigElement()
		rmc.removeAllNamespace()
		mc.file( rn=pubFlPath )
		mc.file( s=True , f=True , type='mayaAscii' )
		rmc.removeUnusedNodeFromMaFile( pubFlPath )

		self.popFile()

	def refFile( self ) :

		currPath = os.path.normpath( str( self.ui.cwdLineEdit.text() ) )
		currFlExt = str( self.ui.fileListWidget.currentItem().text() )
		currFl , currExt = os.path.splitext( currFlExt )
		# currTask = getTaskFromFilePath( currPath )

		filePath = os.path.join( currPath , currFlExt )

		reply = QtGui.QMessageBox.question(
												self.ui ,
												'Confirm' ,
												'Do you want to reference %s ?' %  filePath ,
												QtGui.QMessageBox.Yes | QtGui.QMessageBox.No , QtGui.QMessageBox.No
											)

		if reply == QtGui.QMessageBox.Yes :
			mc.file( filePath , r=True , ns=currFl )

	def importFile( self ) :

		currPath = os.path.normpath( str( self.ui.cwdLineEdit.text() ) )
		currFlExt = str( self.ui.fileListWidget.currentItem().text() )
		currFl , currExt = os.path.splitext( currFlExt )

		filePath = os.path.join( currPath , currFlExt )

		reply = QtGui.QMessageBox.question(
												self.ui ,
												'Confirm' ,
												'Do you want to import %s ?' %  filePath ,
												QtGui.QMessageBox.Yes | QtGui.QMessageBox.No , QtGui.QMessageBox.No
											)

		if reply == QtGui.QMessageBox.Yes :
			mc.file( filePath , i=True )

	def saveTask( self ) :

		currTask = str( self.ui.filterLineEdit.text() )

		if currTask :

			self.saveFile( currTask )

		else :

			QtGui.QMessageBox.question(
											self.ui ,
											'Confirm' ,
											'Task name is not defined' ,
											QtGui.QMessageBox.Ok
										)

	def saveToProject( self ) :
		self.saveFile( '' )

	def saveFile( self , currTask='' ) :

		duppedDags = []

		for each in mc.ls(type='mesh') :
			if '|' in each :
				duppedDags.append( each )

		if duppedDags :

			msg = 'Current scene has more than one mesh shape with the same name, please fix.\n'
			
			for duppedDag in duppedDags :
				msg = '%s%s\n' % ( msg , duppedDag )

			QtGui.QMessageBox.question(
											self.ui ,
											'Confirm' ,
											msg ,
											QtGui.QMessageBox.Ok
										)

		else :

			currPath = os.path.normpath( str( self.ui.cwdLineEdit.text() ) )

			nxFlPath = getNextTaskVersion( currPath , currTask )

			if self.user :
				nxFlPath = nxFlPath.replace( '.ma' , '_%s.ma' % self.user )

			msg = ''
			if currTask :
				msg = 'Current task is set to "%s". Do you want to save?' % currTask
			else :
				msg = 'Current scene will be saved to project. Do you want to save?'

			reply = QtGui.QMessageBox.question(
													self.ui ,
													'Confirm' ,
													msg ,
													QtGui.QMessageBox.Yes | QtGui.QMessageBox.No
												)

			if reply == QtGui.QMessageBox.Yes :

				mc.file( rn=nxFlPath )
				mc.file( s=True , f=True , type='mayaAscii' )
				print 'Current scene has been saved to %s' % nxFlPath

			else :
				return

			self.popFile()
			self.popTask()

	def openFile( self ) :

		currPath = os.path.normpath( str( self.ui.cwdLineEdit.text() ) )
		currFile = str( self.ui.fileListWidget.currentItem().text() )

		filePath = os.path.join( currPath , currFile )

		if mc.file( query = True , anyModified=True ):

			reply = QtGui.QMessageBox.question(
													self.ui ,
													'Unsave chganges' ,
													'Current file has unsaved changes. Do you want to save?' ,
													QtGui.QMessageBox.Save | QtGui.QMessageBox.No | QtGui.QMessageBox.Cancel, QtGui.QMessageBox.No
												)

			if reply == QtGui.QMessageBox.Save :
				mc.file( save=True , type='mayaAscii' )

			if reply == QtGui.QMessageBox.Cancel :
				return

		mc.file( filePath , o=True , f=True)
		

