# System modules
import sys
import os
import re
import shutil

# Maya modules
import maya.standalone
import maya.cmds as mc
import maya.mel as mm

def main() :

	mdlFldPath , mdlFl = os.path.split( __file__ )
	sys.path.append( mdlFldPath )
	import mayaCmd as rmc

	mayaFlPath = sys.argv[1]

	fldPath , flNmExt = os.path.split( mayaFlPath )
	fldElms = fldPath.split( os.sep )
	fldNm = fldElms[-4]
	step = fldElms[-3]
	flNm , flExt = os.path.splitext( flNmExt )

	exp = '(%s_%s_.*)(_v\d{3})' % ( fldNm , step )

	m = re.search( exp , flNm )
	pubFlNm = m.group(1)

	pubFlPath = os.path.join(
								fldPath ,
								'%s.ma' % pubFlNm
							)
	user = None

	maya.standalone.initialize( name='python' )
	mc.file( mayaFlPath , force=True , open=True )
	user = mm.eval( 'optionVar -q "PTuser"' )
	rmc.importRigElement()
	rmc.removeAllNamespace()
	mc.file( rn=pubFlPath )
	mc.file( s=True , f=True , type='mayaAscii' )

	rmc.removeUnusedNodeFromMaFile( pubFlPath )

	if user :
		user = '_%s' % user
	else :
		user = ''
	
	# if 'anim' in flNm :
		
	# 	projFls = []
	# 	fls = [ f for f in os.listdir( fldPath ) if os.path.isfile( os.path.join( fldPath , f ) ) ]

	# 	for fl in fls :

	# 		exp = r'%s_%sv(\d{3})' % ( fldNm , step )
	# 		m = re.search( exp , fl )
	# 		if m :
	# 			projFls.append( int( m.group(1) ) )

	# 	nextVer = 1
	# 	if projFls :
	# 		nextVer = sorted( projFls )[-1] + 1

	# 	projFlPath = os.path.join(
	# 									fldPath ,
	# 									'%s_%s_v%03d%s.ma' % ( fldNm , step , nextVer , user )
	# 								)

	# 	shutil.copy2( pubFlPath , projFlPath )

main()