# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'O:/globalMaya/python/ptTools/rigBrowserPySide/ui/ui.ui'
#
# Created: Thu Aug 20 16:24:01 2015
#      by: pyside-uic 0.2.14 running on PySide 1.2.0
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_rigBrowserMainWindow(object):
    def setupUi(self, rigBrowserMainWindow):
        rigBrowserMainWindow.setObjectName("rigBrowserMainWindow")
        rigBrowserMainWindow.resize(326, 440)
        self.centralwidget = QtGui.QWidget(rigBrowserMainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QtGui.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.cwdLineEdit = QtGui.QLineEdit(self.centralwidget)
        self.cwdLineEdit.setObjectName("cwdLineEdit")
        self.verticalLayout.addWidget(self.cwdLineEdit)
        self.fileListWidget = QtGui.QListWidget(self.centralwidget)
        self.fileListWidget.setSelectionMode(QtGui.QAbstractItemView.ExtendedSelection)
        self.fileListWidget.setObjectName("fileListWidget")
        self.verticalLayout.addWidget(self.fileListWidget)
        self.filterHLayout = QtGui.QHBoxLayout()
        self.filterHLayout.setSpacing(4)
        self.filterHLayout.setObjectName("filterHLayout")
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.filterHLayout.addItem(spacerItem)
        self.filterLineEdit = QtGui.QLineEdit(self.centralwidget)
        self.filterLineEdit.setObjectName("filterLineEdit")
        self.filterHLayout.addWidget(self.filterLineEdit)
        self.verticalLayout.addLayout(self.filterHLayout)
        rigBrowserMainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(rigBrowserMainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 326, 21))
        self.menubar.setObjectName("menubar")
        self.menuFile = QtGui.QMenu(self.menubar)
        self.menuFile.setObjectName("menuFile")
        rigBrowserMainWindow.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(rigBrowserMainWindow)
        self.statusbar.setObjectName("statusbar")
        rigBrowserMainWindow.setStatusBar(self.statusbar)
        self.menubar.addAction(self.menuFile.menuAction())

        self.retranslateUi(rigBrowserMainWindow)
        QtCore.QMetaObject.connectSlotsByName(rigBrowserMainWindow)

    def retranslateUi(self, rigBrowserMainWindow):
        rigBrowserMainWindow.setWindowTitle(QtGui.QApplication.translate("rigBrowserMainWindow", "rigBrowser", None, QtGui.QApplication.UnicodeUTF8))
        self.menuFile.setTitle(QtGui.QApplication.translate("rigBrowserMainWindow", "File", None, QtGui.QApplication.UnicodeUTF8))

