
# Maya modules
import maya.cmds as mc
import maya.mel as mm

# System modules
import os
import shutil
import sys
import re

def isKeywordInLine( kws=() , line='' ) :
	
	result = False
	for kw in kws :
		if kw in line :
			result = True
	
	return result

def removeUnusedNodeFromMaFile( filePath='' ) :
	
	kws = (
				'uiConfigurationScriptNode' ,
				'delight' ,
				'mentalray' ,
				'miDefaultOptions' ,
				'defaultRenderLayer' ,
				'renderLayerManager' ,
				'modelPanel4ViewSelectedSet' ,
				'lockNode'
			)
	
	filePath = os.path.normpath( filePath )
	folderPath , fileNameExt = os.path.split( filePath )
	
	tmpFilePath = '%s.tmp' % filePath
	
	print 'Fixing %s' % filePath
	
	fid = open( filePath , 'r' )
	
	tmpid = open( tmpFilePath , 'w' )
	foundKw = False
	write = True
	
	for line in fid :
		
		if isKeywordInLine( kws , line ) :
			foundKw = True
			write = False
		
		if foundKw and line.startswith( '	' ) :
			write = False
		
		if foundKw and not line.startswith( '	' ) and not isKeywordInLine( kws , line ) :
			foundKw = False
			write = True
		
		if write :
			tmpid.write( line )
	
	fid.close()
	tmpid.close()
	
	bakFilePath = '%s.bak' % filePath
	
	shutil.copy2( filePath , bakFilePath )
	shutil.copy2( tmpFilePath , filePath )
	os.remove( tmpFilePath )
	os.remove( bakFilePath )
	
	print 'Fixing %s done.' % filePath

def importAllReferences() :
	
	# Import all reference.
	
	rfns = mc.ls( type='reference' )

	if rfns :
		
		for rfn in rfns :
			
			if not rfn == 'sharedReferenceNode' :
				
				try :
					fn = mc.referenceQuery( rfn , filename=True )
					
					mc.file( fn , importReference=True )
					
					print '%s has been imported.' % rfn
				
				except RuntimeError :
					
					print '%s is not connected to reference file.' % rfn

def removeAllNodeInNamespace( ns='' ) :

	# Remove every nodes that belong to the given namespace.
	# Input		: Namespace
	# Output	: Empty namespace
	
	nodes = mc.ls( '%s:*' % ns , l=True )
	mc.namespace( set=':' )

	if nodes :
		# If current namespace contains nodes,
		# delete them.
		for node in nodes :

			if mc.objExists( node ) :

				lockState = mc.lockNode( node , q=True )[0]

				if lockState :
					mc.lockNode( node , l=False )
				newName = ''
				try :
					newName = mc.rename( node , node.split( ':' )[-1] )
					# print '%s has been renamed to %s' % ( node , newName )
				except :
					pass
				mc.lockNode( newName , l=lockState )

def renameAllNodeInNamespace( ns='' ) :

	# Remove every nodes that belong to the given namespace.
	# Input		: Namespace
	# Output	: Empty namespace
	
	nodes = mc.ls( '%s:*' % ns , l=True )
	mc.namespace( set=':' )

	if nodes :
		# If current namespace contains nodes,
		# delete them.
		for node in nodes :

			if mc.objExists( node ) :

				lockState = mc.lockNode( node , q=True )[0]

				if lockState :
					mc.lockNode( node , l=False )
				newName = addElementToName( node.split( ':' )[-1] , ns )
				print newName
				try :
					mc.rename( node , newName )
				except :
					pass

def findConnectedContraint( dag='' ) :
	'''
	Find constraint nodes that connected to given dag.
	Return list of constraint nodes.
	'''
	cnstrs = []

	cons = mc.listConnections( dag , s=True , d=False , scn=True , type='constraint' )
	if cons :
		for con in cons :
			if not con in cnstrs :
				cnstrs.append( con )

	return cnstrs

def findTargetFromConstraintNode( constraintNode='' ) :
	'''
	Find constraint targets that connected to given constraintNode.
	Return list of constraint targets.
	'''
	targets = []
	# Get current source connections.
	cnstrSrcs = mc.listConnections( constraintNode , s=True , d=False , p=True )
	
	for cnstrSrc in cnstrSrcs :
		# Iterate to each source connection.
		# If source has attribute parentMatrix connected in,
		# append to 'targets'
		exp = r'(.*)(\.parentMatrix)'
		m = re.search( exp , cnstrSrc )
		if m :
			if not m.group(1) in targets :
				targets.append( m.group(1) )

	return targets

def findConstraintSource( dag='' ) :
	'''
	Find the constraint target of given DAG.
	Return a constraint target.
	'''
	cnstrs = findConnectedContraint( dag )
	
	# Make sure that given DAG has connected constraint node.
	if cnstrs :

		targets = []
		for cnstr in cnstrs :
			# Iterate to each constraint node
			# and get current constraint target.
			currTargets = findTargetFromConstraintNode( cnstr )
			for currTarget in currTargets :
				if not currTarget in targets :
					targets.append( currTarget )
		
		# Make sure that given DAG has only one target.
		if len( targets ) > 1 :
			print '%s has more than one target.' % dag
			return None
		else :
			return targets[0]
	else :
		print '%s has no related constraint node.' % dag
		return None

def removeConstraintTarget( target='' , dag='' ) :
	'''
	Remove constraint target from given dag.
	'''
	attrDict = {
					'tx' : None ,
					'ty' : None ,
					'tz' : None ,
					'rx' : None ,
					'ry' : None ,
					'rz' : None ,
					'sx' : None ,
					'sy' : None ,
					'sz' : None
				}
	# Collecting transform values before remove the constraint target.
	for attr in attrDict.keys() :
		currNodeAttr = '%s.%s' % ( dag , attr )
		if not mc.getAttr( currNodeAttr , l=True ) :
			attrDict[attr] = mc.getAttr( currNodeAttr )

	mc.select( target , r=True )
	mc.select( dag , add=True )
	mm.eval( 'performRemoveConstraintTarget 0;' )

	# Setting transform values back after constraint target has been removed.
	for attr in attrDict.keys() :
		currNodeAttr = '%s.%s' % ( dag , attr )
		if attrDict[attr] :
			mc.setAttr( currNodeAttr , attrDict[attr] )

def importRigElement() :

	'''
	Import all reference.
	Then iterate through each child parented to 'delete_grp'.
	Parent each child to related constraint target.
	Remove 'delete_grp'.
	'''
	
	# Import all referenced files
	importAllReferences()

	delGrp = 'delete_grp'
	
	if mc.objExists( delGrp ) :
		
		for each in mc.listRelatives( delGrp , type='transform' ) :
			
			target = findConstraintSource( each )
			
			if target :
				mc.parent( each , target )
				removeConstraintTarget( target , each )

		mc.delete( delGrp )

# def findConstraintSource( node='' ) :

# 	# Find the constrained target of given node.

# 	constrs = []

# 	# parConstrs = mc.listConnections( node , s=True , type='parentConstraint' )
# 	# scaConstrs = mc.listConnections( node , s=True , type='scaleConstraint' )
	
# 	parConstrs = mc.listConnections( '%s.tx' % node , s=True , type='parentConstraint' )
# 	scaConstrs = mc.listConnections( '%s.sx' % node , s=True , type='scaleConstraint' )
	
# 	if parConstrs :
# 		for each in parConstrs :
# 			constrs.append( each )
	
# 	if scaConstrs :
# 		for each in scaConstrs :
# 			constrs.append( each )
	
# 	if constrs :
# 		parent = mc.listConnections( '%s.target[0].targetTranslate' % constrs[0] , s=True )[0]
# 		mc.delete( constrs )
# 		return parent
# 	else :
# 		return None

# def importRigElement() :
# 	# Import all rig element.
# 	# Find children of 'addRig_grp' node
# 	# and parent to the constraint source.
	
# 	# Import all referenced files
# 	importAllReferences()
	
# 	addRig = 'delete_grp'
	
# 	if mc.objExists( addRig ) :
		
# 		for each in mc.listRelatives( addRig , type='transform' ) :
			
# 			parent = findConstraintSource( each )
			
# 			try :
# 				mc.parent( each , parent )
# 			except :
# 				pass
		
# 		mc.delete( addRig )

def removeAllNamespace() :

	# Recursively remove all namespace in current scene.
	# Input		: None
	# Output	: Removed namespace.

	mc.namespace( set=':' )
	sysNss = [ 'UI' , 'shared' ]
	nss = mc.namespaceInfo( lon=True )

	if nss == sysNss :
		# If there are only 'UI' nad 'shared' namespaces exist in the scene,
		# process is done.
		print 'All namespaces have been removed.'

	else :
		# If there are some namespaces in the scene,
		# remove leaf namespace then call 'removeAllNamespace' again.
		removeLeafNamespace( ':' )
		removeAllNamespace()
	
	return [ ns for ns in nss if not ns in sysNss ]

def removeLeafNamespace( parentNs=':' ) :

	# Recursively find leaf namespace of the given namespace,
	# then remove it.
	# Input		: Namespace
	# Output	: None
	
	nss = mc.namespaceInfo( lon=True )
	
	# If current namespace is root,
	# remove system namespaces from current namespaces.
	if parentNs == ':' :
		nss.remove( 'UI' )
		nss.remove( 'shared' )

	if nss :
		# If current namespace contains some namespaces,
		for ns in nss :
			# iterate through every child namespaces.
			
			mc.namespace( set=':' )
			mc.namespace( set=ns )
			# mc.namespace( set=currentNs )
			childNss = mc.namespaceInfo( lon=True )

			if childNss :
				# If current child namespace contains namespaces,
				# send itself to removeLeafNamespace.
				removeLeafNamespace( ns.split( ':' )[-1] )

			else :
				# If current namespace does not contain any namespace,
				# rename all node belong to the current namespace then
				# remove current namespace.
				removeAllNodeInNamespace( ns )
				try :
					mc.namespace( rm=ns )
				except :
					pass
				# print '%s has been removed.' % ns