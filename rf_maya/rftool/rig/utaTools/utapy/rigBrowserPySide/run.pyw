# System module
import sys

# QT module
from PySide import QtCore , QtGui
from functools import partial

# UI module
import app
reload( app )

def mayaRun() :
	
	import maya.OpenMayaUI as mui
	import shiboken

	global APP

	try:
		APP.close()
	except:
		pass

	ptr = mui.MQtUtil.mainWindow()
	mayaWindow = shiboken.wrapInstance(long(ptr), QtGui.QMainWindow)
	APP = app.RigBrowserMaya(mayaWindow)
	APP.ui.show()

	return APP

def run() :

	a = QtGui.QApplication( sys.argv )
	form = app.RigBrowser()
	form.ui.show()
	a.exec_()

if __name__ == '__main__' :
	
	run()