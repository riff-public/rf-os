import maya.cmds as mc
import maya.mel as mm
import ppmel.core as core
reload(core)


# --------------------------------------------------------------------------------------------
#
# Duplicate Blendshape
#
# description :  - Cut head geomety [ eye , eye brow , eyelash , in mouth ] 
#                - Rename Group "headBshGeo_grp"
#                - Select Group and run script "dupBlendshape()"
#
# --------------------------------------------------------------------------------------------

def dupBlendshape() :
    
    bshBase =         [ ]
    
    bshNode =         [ ]
    
    bshInBtw =        [ ]
    
    side =            [ 'lft' ,
                        'rgt' ]
    
    eyebrow_side =    [ 'eyebrowUp' ,
                        'eyebrowDn' ,
                        'eyebrowIn' ,
                        'eyebrowOut' ,
                        'eyebrowTurnF' ,
                        'eyebrowTurnC' ]
    
    lip =             [ 'mouthUp' ,
                        'mouthDn' ,
                        'mouthL' ,
                        'mouthR' ,
                        'mouthTurnF' ,
                        'mouthTurnC' ,
                        'upLipsCurlIn' ,
                        'upLipsCurlOut' ,
                        'loLipsCurlIn' ,
                        'loLipsCurlOut' ,
                        'mouthU' ]
    
    lip_side =         [ 'cornerInUp' ,
                         'cornerInDn' ,
                         'cornerOutUp' ,
                         'cornerOutDn' ,
                         'cornerUp' ,
                         'cornerDn' ,
                         'cornerIn' ,
                         'cornerOut' ,
                         'lipsPartIn' ,
                         'lipsPartOut' ,
                         'cheekUpper' ,
                         'cheekLower' ,
                         'cornerPuffIn' ,
                         'cornerPuffOut' ]

    sel = mc.ls( sl = True )[0]
    bshBase.append( sel )
    
    shapeOrig = mc.ls( '%sShapeOrig*' %sel )
    
    if shapeOrig :
        mc.delete(shapeOrig)
    
    bb = mc.exactWorldBoundingBox()
    yOffset = float( abs( bb[1] - bb[4] ) * 1.1 )
    xOffset = float( abs( bb[0] - bb[3] ) * 1.2 )
    xVal = 0
    
    for sides in side :
        xVal += xOffset
        yVal = 0
        
        for eyebrows in eyebrow_side :
            if not mc.objExists( 'eyebrowBsh_%s_grp' %sides ) :
                bshGrp = mc.createNode( 'transform' , n = 'eyebrowBsh_%s_grp' %sides )
            
            mc.select( sel , r = True )
            duppedNode = mc.duplicate( sel , rr = True )[0]
            bshNodes = mc.rename( duppedNode , '%s_%s' %(eyebrows , sides) )
            mc.parent( bshNodes , bshGrp )
            mc.move( 0 , yVal , 0 , bshNodes , r = True )
            
            bshNode.append( bshNodes )
            
            yVal += yOffset
        
        mc.move( xVal , 0 , 0 , bshGrp , r=True )
    
    yVal = 0
    
    for lips in lip :
        if not mc.objExists( 'lipBsh_grp' ) :
            bshGrp = mc.createNode( 'transform' , n = 'lipBsh_grp' )
        
        mc.select( sel , r = True )
        duppedNode = mc.duplicate( sel , rr = True )[0]
        bshNodes = mc.rename( duppedNode , '%s' %lips )
        mc.parent( bshNodes , bshGrp )
        mc.move( 0 , yVal , 0 , bshNodes , r = True )
        
        bshNode.append( bshNodes )
            
        yVal += yOffset
    
    xVal += xOffset
    mc.move( xVal , 0 , 0 , bshGrp , r=True )
    
    for sides in side :
        xVal += xOffset
        yVal = 0
        
        for lips in lip_side :
            if not mc.objExists( 'lipBsh_%s_grp' %sides ) :
                bshGrp = mc.createNode( 'transform' , n = 'lipBsh_%s_grp' %sides )
            
            mc.select( sel , r = True )
            duppedNode = mc.duplicate( sel , rr = True )[0]
            bshNodes = mc.rename( duppedNode , '%s_%s' %(lips , sides) )
            mc.parent( bshNodes , bshGrp )
            mc.move( 0 , yVal , 0 , bshNodes , r = True )
            
            bshNode.append( bshNodes )
            
            yVal += yOffset
        
        mc.move( xVal , 0 , 0 , bshGrp , r=True )
    
    yVal = 0
    
    
    #--- Create blendshape
    bsnName = mc.blendShape( bshNode , bshBase , n = 'facialAll_bsh' )
    
    mc.select( cl = True )


# --------------------------------------------------------------------------------------------
#
# Add Attribute
#
# --------------------------------------------------------------------------------------------

def addAttrBshCtrl() :
    attrBrow( 'eyeBrowBsh_lft_ctrl' )
    attrBrow( 'eyeBrowBsh_rgt_ctrl' )
    attrEye( 'eyeBsh_lft_ctrl' )
    attrEye( 'eyeBsh_rgt_ctrl' )
    attrNose( 'noseBsh_ctrl' )
    attrMouth( 'mouthBsh_ctrl' )
    attrMouthSide( 'mouthBsh_lft_ctrl' )
    attrMouthSide( 'mouthBsh_rgt_ctrl' )
    
def attrBrow( obj ):
    #obj = mc.ls( sl = True )[0]
    
    listAttr = [ 'eyebrow' , 
                 'allUD' , 
                 'allIO' , 
                 'innerUD' , 
                 'innerIO' , 
                 'middleUD' , 
                 'middleIO' , 
                 'outerUD' ,
                 'outerIO' ,
                 'turnUD' ]
    
    for attr in listAttr :
        mc.addAttr( obj , ln = '%s' %attr , at = 'double' , k = True , min = -10 , max = 10 )
        
    core.setLock( obj , True , 'eyebrow' )
    
def attrEye( obj ):
    #obj = mc.ls( sl = True )[0]
    
    listAttr = [ 'lid' ,
                 'upLidUD' , 
                 'loLidUD' , 
                 'upLidTW' , 
                 'loLidTW' , 
                 'upInnerUD' , 
                 'upMidUD' , 
                 'upOuterUD' ,
                 'loInnerUD' ,
                 'loMidUD' ,
                 'loOuterUD' ,
                 'ball' ,
                 'eyeBallUD' ,
                 'eyeBallIO' ,
                 'eyeBallTW' ,
                 'iris' ,
                 'scaleIris' ]
    
    for attr in listAttr :
        mc.addAttr( obj , ln = '%s' %attr , at = 'double' , k = True , min = -10 , max = 10 )
    
    core.setLock( obj , True , 'lid' , 'ball' , 'iris' )
    
def attrNose( obj ):
    #obj = mc.ls( sl = True )[0]
    
    listAttr = [ 'nose' ,
                 'noseUD' ,
                 'noseLR' ,
                 'noseSTQ' , 
                 'noseLft' , 
                 'noseLftUD' , 
                 'noseLftTurn' , 
                 'noseRgt' , 
                 'noseRgtUD' , 
                 'noseRgtTurn' ]
    
    for attr in listAttr :
        mc.addAttr( obj , ln = '%s' %attr , at = 'double' , k = True , min = -10 , max = 10 )
    
    core.setLock( obj , True , 'nose' , 'noseLft' , 'noseRgt' )
    
def attrMouth( obj ):
    #obj = mc.ls( sl = True )[0]
    
    listAttr = [ 'mouth' ,
                 'mouthUD' ,
                 'mouthLR' ,
                 'mouthTurn' , 
                 'upLipsUD' , 
                 'loLipsUD' , 
                 'upLipsCurlIO' , 
                 'loLipsCurlIO' , 
                 'mouthClench' , 
                 'mouthPull' ,
                 'mouthU' ,
                 'face' ,
                 'faceSquash' ,
                 'eyebrowPullIO' ]
    
    for attr in listAttr :
        mc.addAttr( obj , ln = '%s' %attr , at = 'double' , k = True , min = -10 , max = 10 )
    
    core.setLock( obj , True , 'mouth' , 'face' )
    
def attrMouthSide( obj ):
    #obj = mc.ls( sl = True )[0]
    
    listAttr = [ 'lip' ,
                 'upLipUD' ,
                 'loLipUD' ,
                 'cornerUD' , 
                 'cornerIO' , 
                 'lipPartIO' , 
                 'cheekUprIO' , 
                 'cheekLwrIO' , 
                 'puffIO' ]
    
    for attr in listAttr :
        mc.addAttr( obj , ln = '%s' %attr , at = 'double' , k = True , min = -10 , max = 10 )
    
    core.setLock( obj , True , 'lip' )



# --------------------------------------------------------------------------------------------
#
# Connect Blendshape to Control
#
# --------------------------------------------------------------------------------------------

def connectBshCtrl( eyeBrowLFT = 'eyeBrowBsh_lft_ctrl' ,
                    eyeBrowRGT = 'eyeBrowBsh_rgt_ctrl' ,
                    eyeLFT = 'eyeBsh_lft_ctrl' ,
                    eyeRGT = 'eyeBsh_rgt_ctrl' ,
                    nose = 'noseBsh_ctrl' ,
                    mouth = 'mouthBsh_ctrl' ,
                    mouthLFT = 'mouthBsh_lft_ctrl' ,
                    mouthRGT = 'mouthBsh_rgt_ctrl' ,
                    faceUpr = 'faceUprBsh_ctrl' ,
                    faceLwr = 'faceLwrBsh_ctrl' ,
                    bshName = 'facialAll_bsh' ) :
    
    
    #-- eyeBrow
    for side in ( 'lft' , 'rgt' ) :
        
        if side == 'lft' :
            ctrl = eyeBrowLFT
        else :
            ctrl = eyeBrowRGT
            
        attrDict = { 'allUD' : ( 'eyebrowUp' , 'eyebrowDn' ) , 
                     'allIO' : ( 'eyebrowIn' , 'eyebrowOut' ) ,
                     'turnUD' : ( 'eyebrowTurnF' , 'eyebrowTurnC' ) }

        for attr in attrDict :
            if mc.objExists ('%s.%s' %(ctrl,attr))==True:
                mdv = core.multiplyDivide( '%s_%s_mdv' %(attr , side))
                core.setVal( '%s.i2x' %mdv , 0.1 )
                core.setVal( '%s.i2y' %mdv , -0.1 )
                
                cmp = core.clamp( '%s_%s_cmp' %(attr , side))
                core.setVal( '%s.mng' %cmp , -10 )
                core.setVal( '%s.mxr' %cmp , 10 )
                        
                mc.connectAttr( '%s.%s' %(ctrl , attr) , '%s.ipr' %cmp )
                mc.connectAttr( '%s.%s' %(ctrl , attr) , '%s.ipg' %cmp )
                
                mc.connectAttr( '%s.opr' %cmp , '%s.i1x' %mdv )
                mc.connectAttr( '%s.opg' %cmp , '%s.i1y' %mdv )
                
                mc.connectAttr( '%s.ox' %mdv , '%s.%s_%s' %(bshName , attrDict[attr][0] , side))
                mc.connectAttr( '%s.oy' %mdv , '%s.%s_%s' %(bshName , attrDict[attr][1] , side))
            else: 
                print "##====================  PLEASE IMPORT 'FacialControl' ====================##"
        
    '''        
    #-- eye
    for side in ( 'lft' , 'rgt' ) :
        
        if side == 'lft' :
            ctrl = eyeLFT
        else :
            ctrl = eyeRGT
        
        attrDict = { 'upLidUD' : ( 'upLidUp' , 'upLidDn' ) , 
                     'loLidUD' : ( 'loLidUp' , 'loLidDn' ) , 
                     'upLidTW' : ( 'upLidTurnF' , 'upLidTurnC' ) , 
                     'loLidTW' : ( 'loLidTurnF' , 'loLidTurnC' ) , 
                     'upInnerUD' : 'upLidInUD'  , 
                     'upMidUD' : 'upLidMidUD' , 
                     'upOuterUD' : 'upLidOutUD' , 
                     'loInnerUD' : 'loLidInUD' , 
                     'loMidUD' : 'loLidMidUD' , 
                     'loOuterUD' : 'loLidOutUD' , 
                     'eyeBallUD' : ( 'eyeBallUp' , 'eyeBallDn' ) , 
                     'eyeBallIO' : ( 'eyeBallIn' , 'eyeBallOut' ) , 
                     'eyeBallTW' : ( 'eyeBallTurnF' , 'eyeBallTurnC' ) }
        
        for attr in attrDict :
                
            mdv = core.multiplyDivide( '%s_%s_mdv' %(attr , side))
            if not attr == 'loLidUD' :
                core.setVal( '%s.i2x' %mdv , 0.1 )
                core.setVal( '%s.i2y' %mdv , -0.1 )
            else :
                core.setVal( '%s.i2x' %mdv , -0.1 )
                core.setVal( '%s.i2y' %mdv , 0.1 )
            
            if not attr in ( 'upInnerUD' , 'upMidUD' , 'upOuterUD' , 'loInnerUD' , 'loMidUD' , 'loOuterUD' ) :
                cmp = core.clamp( '%s_%s_cmp' %(attr , side))
                
                if not attr == 'loLidUD' :
                    core.setVal( '%s.mng' %cmp , -10 )
                    core.setVal( '%s.mxr' %cmp , 10 )
                else :
                    core.setVal( '%s.mnr' %cmp , -10 )
                    core.setVal( '%s.mxg' %cmp , 10 )
                
                mc.connectAttr( '%s.%s' %(ctrl , attr) , '%s.ipr' %cmp )
                mc.connectAttr( '%s.%s' %(ctrl , attr) , '%s.ipg' %cmp )
                
                mc.connectAttr( '%s.opr' %cmp , '%s.i1x' %mdv )
                mc.connectAttr( '%s.opg' %cmp , '%s.i1y' %mdv )
                
                mc.connectAttr( '%s.ox' %mdv , '%s.%s_%s' %(bshName , attrDict[attr][0] , side))
                mc.connectAttr( '%s.oy' %mdv , '%s.%s_%s' %(bshName , attrDict[attr][1] , side))
            
            else :
                mc.connectAttr( '%s.%s' %(ctrl , attr) , '%s.i1x' %mdv )
                mc.connectAttr( '%s.ox' %mdv , '%s.%s_%s' %(bshName , attrDict[attr] , side))
    
    
    #-- nose
    attrDict = { 'noseUD' : ( 'noseUp' , 'noseDn' ) , 
                 'noseLR' : ( 'noseL' , 'noseR' ) , 
                 'noseSTQ' : ( 'noseStretch' , 'noseSquash' ) , 
                 'noseLftUD' : ( 'noseUp_lft' , 'noseDn_lft' ) , 
                 'noseLftTurn' : ( 'noseTurnF_lft' , 'noseTurnC_lft' ) , 
                 'noseRgtUD' : ( 'noseUp_rgt' , 'noseDn_rgt' ) , 
                 'noseRgtTurn' : ( 'noseTurnF_rgt' , 'noseTurnC_rgt' ) }
        
    for attr in attrDict :
            mdv = core.multiplyDivide( '%s_mdv' %attr )
            core.setVal( '%s.i2x' %mdv , 0.1 )
            core.setVal( '%s.i2y' %mdv , -0.1 )
            
            cmp = core.clamp( '%s_cmp' %attr )
            core.setVal( '%s.mng' %cmp , -10 )
            core.setVal( '%s.mxr' %cmp , 10 )
            
            mc.connectAttr( '%s.%s' %(nose , attr) , '%s.ipr' %cmp )
            mc.connectAttr( '%s.%s' %(nose , attr) , '%s.ipg' %cmp )
            
            mc.connectAttr( '%s.opr' %cmp , '%s.i1x' %mdv )
            mc.connectAttr( '%s.opg' %cmp , '%s.i1y' %mdv )
            
            mc.connectAttr( '%s.ox' %mdv , '%s.%s' %(bshName , attrDict[attr][0] ))
            mc.connectAttr( '%s.oy' %mdv , '%s.%s' %(bshName , attrDict[attr][1] ))
    
    '''
    #-- mouth
    attrDict = { 'mouthUD' : ( 'mouthUp' , 'mouthDn' ) ,
                 'mouthLR' : ( 'mouthL' , 'mouthR' ) ,
                 'mouthTurn' : ( 'mouthTurnF' , 'mouthTurnC' ) ,
                 'upLipsCurlIO' : ( 'upLipsCurlIn' , 'upLipsCurlOut' ) ,
                 'loLipsCurlIO' : ( 'loLipsCurlIn' , 'loLipsCurlOut' ) ,
                 'mouthU' : 'mouthU' }
        
    for attr in attrDict :
                
            mdv = core.multiplyDivide( '%s_mdv' %attr )
            core.setVal( '%s.i2x' %mdv , 0.1 )
            core.setVal( '%s.i2y' %mdv , -0.1 )
            
            if not attr in ( 'mouthClench' , 'mouthPull' , 'mouthU' , 'eyebrowPullIO' ) :
                cmp = core.clamp( '%s_cmp' %attr )
                core.setVal( '%s.mng' %cmp , -10 )
                core.setVal( '%s.mxr' %cmp , 10 )
                
                mc.connectAttr( '%s.%s' %(mouth , attr) , '%s.ipr' %cmp )
                mc.connectAttr( '%s.%s' %(mouth , attr) , '%s.ipg' %cmp )
                
                mc.connectAttr( '%s.opr' %cmp , '%s.i1x' %mdv )
                mc.connectAttr( '%s.opg' %cmp , '%s.i1y' %mdv )
                
                mc.connectAttr( '%s.ox' %mdv , '%s.%s' %(bshName , attrDict[attr][0] ))
                mc.connectAttr( '%s.oy' %mdv , '%s.%s' %(bshName , attrDict[attr][1] ))
            
            else :
                mc.connectAttr( '%s.%s' %(mouth , attr) , '%s.i1x' %mdv )
                mc.connectAttr( '%s.ox' %mdv , '%s.%s' %(bshName , attrDict[attr] ))
    
    
    #-- mouthSide
    for side in ( 'lft' , 'rgt' ) :
        
        if side == 'lft' :
            ctrl = mouthLFT
        else :
            ctrl = mouthRGT
            
        attrDict = { 'cornerInUD' : ( 'cornerInUp' , 'cornerInDn' ) ,
                     'cornerOutUD' : ( 'cornerOutUp' , 'cornerOutDn' ) ,
                     'cornerUD' : ( 'cornerUp' , 'cornerDn' ) ,
                     'cornerIO' : ( 'cornerIn' , 'cornerOut' ) ,
                     'lipPartIO' : ( 'lipsPartIn' , 'lipsPartOut' ) ,
                     'cheekUprIO' : 'cheekUpper' ,
                     'cheekLwrIO' : 'cheekLower' ,
                     'puffIO' : ( 'cornerPuffIn' , 'cornerPuffOut' ) }
        
        for attr in attrDict :
                
            mdv = core.multiplyDivide( '%s_%s_mdv' %(attr , side))
            core.setVal( '%s.i2x' %mdv , 0.1 )
            core.setVal( '%s.i2y' %mdv , -0.1 )
            
            if not attr in ( 'cheekUprIO' , 'cheekLwrIO' ) :
                cmp = core.clamp( '%s_%s_cmp' %(attr , side))
                core.setVal( '%s.mng' %cmp , -10 )
                core.setVal( '%s.mxr' %cmp , 10 )
                
                mc.connectAttr( '%s.%s' %(ctrl , attr) , '%s.ipr' %cmp )
                mc.connectAttr( '%s.%s' %(ctrl , attr) , '%s.ipg' %cmp )
                
                mc.connectAttr( '%s.opr' %cmp , '%s.i1x' %mdv )
                mc.connectAttr( '%s.opg' %cmp , '%s.i1y' %mdv )
                
                mc.connectAttr( '%s.ox' %mdv , '%s.%s_%s' %(bshName , attrDict[attr][0] , side))
                mc.connectAttr( '%s.oy' %mdv , '%s.%s_%s' %(bshName , attrDict[attr][1] , side))
            
            else :
                mc.connectAttr( '%s.%s' %(ctrl , attr) , '%s.i1x' %mdv )
                mc.connectAttr( '%s.ox' %mdv , '%s.%s_%s' %(bshName , attrDict[attr] , side))
    
    '''
    #-- faceUpr
    attrDict = { 'tx' : ( 'faceUprLft' , 'faceUprRgt' ) , 
                 'ty' : ( 'faceUprStretch' , 'faceUprSquash' ) , 
                 'tz' : ( 'faceUprFront' , 'faceUprBack' ) }
        
    for attr in attrDict :
            mdv = core.multiplyDivide( 'faceUpr%s_mdv' %attr )
            core.setVal( '%s.i2x' %mdv , 1 )
            core.setVal( '%s.i2y' %mdv , -1 )
            
            cmp = core.clamp( 'faceUpr%s_cmp' %attr )
            core.setVal( '%s.mng' %cmp , -100 )
            core.setVal( '%s.mxr' %cmp , 100 )
            
            mc.connectAttr( '%s.%s' %(faceUpr , attr) , '%s.ipr' %cmp )
            mc.connectAttr( '%s.%s' %(faceUpr , attr) , '%s.ipg' %cmp )
            
            mc.connectAttr( '%s.opr' %cmp , '%s.i1x' %mdv )
            mc.connectAttr( '%s.opg' %cmp , '%s.i1y' %mdv )
            
            mc.connectAttr( '%s.ox' %mdv , '%s.%s' %(bshName , attrDict[attr][0] ))
            mc.connectAttr( '%s.oy' %mdv , '%s.%s' %(bshName , attrDict[attr][1] ))
    
    
    #-- faceLwr
    attrDict = { 'tx' : ( 'faceLwrLft' , 'faceLwrRgt' ) , 
                 'ty' : ( 'faceLwrStretch' , 'faceLwrSquash' ) , 
                 'tz' : ( 'faceLwrFront' , 'faceLwrBack' ) }
        
    for attr in attrDict :
            mdv = core.multiplyDivide( 'faceLwr%s_mdv' %attr )
            
            if not attr == 'ty' :
                core.setVal( '%s.i2x' %mdv , 1 )
                core.setVal( '%s.i2y' %mdv , -1 )
            else :
                core.setVal( '%s.i2x' %mdv , -1 )
                core.setVal( '%s.i2y' %mdv , 1 )
            
            cmp = core.clamp( 'faceLwr%s_cmp' %attr )
            
            if not attr == 'ty' :
                core.setVal( '%s.mng' %cmp , -100 )
                core.setVal( '%s.mxr' %cmp , 100 )
            else :
                core.setVal( '%s.mnr' %cmp , -100 )
                core.setVal( '%s.mxg' %cmp , 100 )
            
            mc.connectAttr( '%s.%s' %(faceLwr , attr) , '%s.ipr' %cmp )
            mc.connectAttr( '%s.%s' %(faceLwr , attr) , '%s.ipg' %cmp )
            
            mc.connectAttr( '%s.opr' %cmp , '%s.i1x' %mdv )
            mc.connectAttr( '%s.opg' %cmp , '%s.i1y' %mdv )
            
            mc.connectAttr( '%s.ox' %mdv , '%s.%s' %(bshName , attrDict[attr][0] ))
            mc.connectAttr( '%s.oy' %mdv , '%s.%s' %(bshName , attrDict[attr][1] ))
    
    mc.select( cl = True )
    '''