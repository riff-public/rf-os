import sys
sys.path.append( r'O:\globalMaya\python\ptTools' )

import pkmel.ptTools as ptTools
reload( ptTools )

#1. ----------------------------------------------------------------------------
	ptTools.pfAssetRelinkTexture()

#2. ----------------------------------------------------------------------------
	# export model.fbx and mat.maya
	#select Object for write
	ptTools.writeSelectedShadingData()

#3. ----------------------------------------------------------------------------
	# import model.fbx and mat.maya
	ptTools.pfAssetImportMatFile()

#----------------------------------------------------------------------------
	ptTools.pfAssetReAssignShader()

