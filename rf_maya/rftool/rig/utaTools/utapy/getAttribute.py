import maya.cmds as mc
import pymel.core as pm
import maya.mel as mm
import os
#----------------------------------------------
'''
import sys
sys.path.append(r'P:\lib\local\utaTools\utapy')
import getAttributeClass as acc
reload(acc)
#writeAttribute
obj = acc.AttributeData()
obj5 = obj.allCtrl()
obj2 = obj.listAttributeKeyableAll(obj=obj5)
print obj2
obj3 = obj.getDataFld()
obj4= obj.save_file(name ="attrData", attrData = obj2, path= obj3)

#reedAttribute
obj6= obj.read_file(name="attrData", path= obj3)
exec obj6
#print obj5[1]
#print obj6
setValues = obj.setAttrToCtrl()
#print setAttrs 
'''
class AttributeData ( object ) :
    def __init__(self):
        pass
        # self.obj = obj ;
        # self.name = name  ;

    def allCtrl(self):
        ctrlInFile = mc.ls("*:*_ctrl", "*_ctrl")
        print ("allControl on file : %s" % ctrlInFile)
        print ("Amount : %s Control" % len(ctrlInFile))
        return ctrlInFile     

    def listAttributeKeyableAll(self, obj):   
        ctrlListGlobal = '' ;         
        for asset in obj:

            ctrlList = '%s = {};' % str ( asset )

            for attr in mc.listAttr( asset, r=True ,k=True):

                try:
                    ctrlAttrList = '%s["%s"] = %s;' % ( str(asset) , str(attr) , mc.getAttr("%s.%s" %(str(asset),attr)) )  
                    ctrlList += ctrlAttrList 

                except:
                    print "Error reading data" 

            ctrlListGlobal += ctrlList ;

        return ctrlListGlobal

    def keyable ( self , obj ) :
        attrList = mc.listAttr( obj, r=True ,k=True) ;
        return attrList ;
# -----------------------------------------------------------------------------------

def save_file(name, attrData, path ):       
    # if not attrData:
    #     mc.error("no file name sujpplied ")
    
    fileName = '%s.txt' % (name )
    pathFile = "%s/%s" % (path , fileName)
    print pathFile
    fileWrite = open(pathFile, 'w+')
    fileWrite.write(str(attrData))
    fileWrite.close()
    return pathFile
    #print fileWrite    
def read_file(name,  path ):       
    # if not attrData:
    #     mc.error("no file name sujpplied ")
    
    fileName = '%s.txt' % (name )
    pathFile = "%s/%s" % (path , fileName)
    # print pathFile
    fileWrite = open(pathFile, 'r+')
    data = fileWrite.read()
    fileWrite.close()
    return data
    #print fileWrite    
                
def setAttrToCtrl ():

    path = getDataFld() ;
    textCmd = read_file(name="attrData", path= path)
    # print textCmd
    exec textCmd
    allCtrlVal = self.allCtrl()
    for ctrl in allCtrlVal :
        attrLists = self.keyable(obj=ctrl)
        for attr in attrLists : 
            #exec ( 'val =' + ' ' + ctrl + '["' + attr + '"]' ) ;
            try :
                exec ( 'mc.setAttr("%s.%s" , %s ["%s"])' % ( ctrl , attr , ctrl , attr))
            except : pass
            #     mc.setAttr ( '%s.%s' % ( ctrl , attr ) , lock = False ) ;
            #     exec ( 'mc.setAttr("%s.%s" , %s ["%s"])' % ( ctrl , attr , ctrl , attr))
            #     mc.setAttr ( '%s.%s' % ( ctrl , attr ) , lock = True ) ;
            # else : pass ;

    # print '------ Clean Success!! --------' 
    # print '--------    *(^O^)*    --------' 
    # print '-------------------------------'
        
def getDataFld() :      
    wfn = os.path.normpath( pm.sceneName() )
    tmpAry = wfn.split( '\\' )
    tmpAry[-2] = 'data' 
    dataFld = '\\'.join( tmpAry[0:-1] ) 
    if not os.path.isdir( dataFld ) :
        os.mkdir( dataFld ) 
        #print (dataFld)
    return dataFld          
        
