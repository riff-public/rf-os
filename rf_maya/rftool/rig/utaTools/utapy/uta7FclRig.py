import os
from functools import partial

import maya.cmds as mc
import maya.mel as mm
import re

from lpRig import core as lrc
reload(lrc)
from lpRig import rigTools as lrr
reload(lrr)
from lpRig import detailControl
reload(detailControl)
from lpRig import blendShapeAdder
reload(blendShapeAdder)

from lpRig import core as lrc
reload(lrc)
from lpRig import rigTools as lrr
reload(lrr)
# FclRig DtlRig
# fclRig.parentDtlRig()
# fclRig.stickDtlCtrl()
# FclRig TtRig
# fclRig.refElem('TtRig')
# fclRig.parentTtRig()

def refElem(elem=''):

    scnLoc = os.path.normpath(mc.file(q=True, l=True)[0])

    fldPath, flNmExt = os.path.split(scnLoc)
    assetPath, taskFld = os.path.split(fldPath)
    catPath, assetNm = os.path.split(assetPath)

    elemPath = os.path.join(
                                fldPath,
                                '%s_%s.ma' % (assetNm, elem)
                            )

    mc.file(elemPath, r=True, ns=elem)
# TtRig
def parentTtRig():

    ctrl, jnt, skin, still = findMainGroup('TtRig')
    mc.parent(ctrl, HD_LOW_GRP)
    mc.parent(jnt, JNT_GRP)
    mc.parent(skin, SKIN_GRP)
    mc.parent(still, STILL_GRP)

    jawPiv = None
    jawJnt = None
    for each in mc.ls():
        if each.endswith(':TtRigCtrlJawPost_Grp'):
            jawPiv = each
        elif each.endswith(':JawMthRig_Jnt'):
            jawJnt = each

    for attr in ('t', 'r', 's'):
        mc.connectAttr('{jawJnt}.{attr}'.format(jawJnt=jawJnt, attr=attr), 
                        '{jawPiv}.{attr}'.format(jawPiv=jawPiv, attr=attr))
# DtlRig
def parentDtlRig():
    
    ctrl, jnt, skin, still = findMainGroup('DtlRig')
    mc.parent(skin, SKIN_GRP)
    mc.parent(still, STILL_GRP)

    mc.parent(mc.ls('*:DtlRigCtrlLow_Grp')[0], HD_LOW_GRP)
    mc.parent(mc.ls('*:DtlRigCtrlMid_Grp')[0], HD_MID_GRP)
    mc.parent(mc.ls('*:DtlRigCtrlHi_Grp')[0], HD_HI_GRP)

def stickDtlCtrl(postGeo='BodyFclRigPost_Geo'):

    ctrls =[
            'NsDtlRig_L_Ctrl', 'CheekADtlRig_L_Ctrl','CheekBDtlRig_L_Ctrl','CheekCDtlRig_L_Ctrl', 
            'NsDtlRig_R_Ctrl', 'CheekADtlRig_R_Ctrl','CheekBDtlRig_R_Ctrl', 'CheekCDtlRig_R_Ctrl', 
            'EyeSktADtlRig_L_Ctrl','EyeSktBDtlRig_L_Ctrl', 'EyeSktADtlRig_R_Ctrl', 'EyeSktBDtlRig_R_Ctrl',
            'PuffDtlRig_L_Ctrl', 'PuffDtlRig_R_Ctrl','NsDtlRig_Ctrl','EyeBrowDtlRig_Ctrl',
            'EyeBrowADtlRig_L_Ctrl','EyeBrowBDtlRig_L_Ctrl','EyeBrowCDtlRig_L_Ctrl','EyeBrowADtlRig_R_Ctrl',
            'EyeBrowBDtlRig_R_Ctrl','EyeBrowCDtlRig_R_Ctrl',]
    mc.select(cl=True)
    for ctrl in ctrls:
        currCtrls = mc.ls('*:{ctrl}'.format(ctrl=ctrl))
        if not currCtrls:
            continue
        mc.select(currCtrls[0], add=True)

    mc.select(postGeo, add=True)
    rvts = detailControl.attachSelectedControlsToSelectedMesh()
    mc.parent(rvts, RVT_GRP)  

def stickDtlCtrl(postGeo='BodyFclRigPost_Geo'):

    ctrls =[
            'NsDtlRig_L_Ctrl', 'CheekADtlRig_L_Ctrl','CheekBDtlRig_L_Ctrl','CheekCDtlRig_L_Ctrl', 
            'NsDtlRig_R_Ctrl', 'CheekADtlRig_R_Ctrl','CheekBDtlRig_R_Ctrl', 'CheekCDtlRig_R_Ctrl', 
            'EyeSktADtlRig_L_Ctrl','EyeSktBDtlRig_L_Ctrl', 'EyeSktADtlRig_R_Ctrl', 'EyeSktBDtlRig_R_Ctrl',
            'PuffDtlRig_L_Ctrl', 'PuffDtlRig_R_Ctrl','NsDtlRig_Ctrl','EyeBrowDtlRig_Ctrl',
            'EyeBrowADtlRig_L_Ctrl','EyeBrowBDtlRig_L_Ctrl','EyeBrowCDtlRig_L_Ctrl','EyeBrowADtlRig_R_Ctrl',
            'EyeBrowBDtlRig_R_Ctrl','EyeBrowCDtlRig_R_Ctrl',]
    mc.select(cl=True)
    for ctrl in ctrls:
        currCtrls = mc.ls('*:{ctrl}'.format(ctrl=ctrl))
        if not currCtrls:
            continue
        mc.select(currCtrls[0], add=True)

    mc.select(postGeo, add=True)
    rvts = detailControl.attachSelectedControlsToSelectedMesh()
    mc.parent(rvts, RVT_GRP)  

# def stickDtlCtrlAdd(postGeo="", ctrls= []):

#     # ctrls = ['NsDtlRig_L_Ctrl', 'CheekADtlRig_L_Ctrl', 'CheekBDtlRig_L_Ctrl',
#     #       'CheekCDtlRig_L_Ctrl', 'NsDtlRig_R_Ctrl', 'CheekADtlRig_R_Ctrl',
#     #       'CheekBDtlRig_R_Ctrl', 'CheekCDtlRig_R_Ctrl', 'EyeSktADtlRig_L_Ctrl',
#     #       'EyeSktBDtlRig_L_Ctrl', 'EyeSktADtlRig_R_Ctrl', 'EyeSktBDtlRig_R_Ctrl',
#     #       'PuffDtlRig_L_Ctrl', 'PuffDtlRig_R_Ctrl']
#     # mc.select(cl=True)
#     for ctrl in ctrls:
#         currCtrls = mc.ls('*:{ctrl}'.format(ctrl=ctrl))
#         if not currCtrls:
#             continue
#         mc.select(currCtrls[0], add=True)

#     mc.select(postGeo, add=True)
#     rvts = detailControl.attachSelectedControlsToSelectedMesh()
#     mc.parent(rvts, RVT_GRP)

def addBshFacial(hdRigList = [], fclRigList = []):
    for i in range(0, len(hdRigList)):
        currHdRig = []
        currFclRig = []        
        currHdRig = mc.ls("*:%s" % hdRigList[i])
        currFclRig = (fclRigList[i])
        # print currHdRig, currFclRig;
        listAmount = len(currHdRig)
        # print listAmount
        if listAmount != 0:
            mc.select(currHdRig)
            mc.select(currFclRig, add=True)
            lrr.doAddBlendShape()
        else :
            print hdRigList[i], "This Object is not Ready"  

    print "Create BlendShape Hd 2 Fcl is Done!!"

def addBshHd2Fcl(hdRigGeo ='', fclRigGeo = '',*args):
    # lists = mc.ls(hdRigGeo,fclRigGeo)
    mc.select(hdRigGeo)
    # print hdRigGeo, fclRigGeo
    mc.select(fclRigGeo, add=True)
    lrr.doAddBlendShape()
    listsPos = mc.ls("BodyFclRig_Geo", "BodyFclRigPost_Geo")
    mc.select(listsPos[0])
    mc.select(listsPos[1], add=True)
    lrr.doAddBlendShape()        


def fixAttrBsh(*args):
    utaCore.renameAttrs(object = 'NoseBsh_Ctrl', 
                    oldAttr = ['noseLftTwist','noseRgtTwist'], 
                    newAttr = ['eyebrowLftTwist','eyebrowRgtTwist'])    
        
    utaCore.addAttrCtrl(ctrl = 'MouthBsh_Ctrl', 
                    elem = ['LipsSealL', 'LipsSealR'], 
                    min = 0, 
                    max = 20,
                    at = 'float')
    