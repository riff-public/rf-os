import maya.cmds as mc
import pymel.core as pm
from utaTools.utapy import utaCore
reload(utaCore)

'''
************************************************************************

from utaTools.utapy import utaMuscleControl_002 as utaMuscleControl
reload(utaMuscleControl)
ken = utaMuscleControl.UtaMuscleControl()
ken.runEach(positiveObj = 'BodyPos_Geo', 
        negativeObj = ['NegaA01_Jnt', 'NegaA02_Jnt', 'NegaA03_Jnt'],  
        name = 'Beard', 
        elem = 'Msc',
        side = '',
        parent = '',
        detailCtrl = True)
************************************************************************
ken.generateSpineIkCrv( cur_name = 'Armor_Crv', 
                        span_curve = 20, 
                        name = 'Armor', 
                        elem = '', 
                        side = '_L_' , 
                        localPar = 'UpArm_L_Jnt', 
                        bindSkinGeo = 'Suit_Geo_Proxy', 
                        )
************************************************************************
    import sys
    sys.path.append(r'P:/SevenChickMovie/asset/publ/char/kingDamian/rig/data')
    import addRig
    reload(addRig)
    addRig.runUpArm_L()
************************************************************************
'''

class UtaMuscleControl( object ):
    def __init__( self ):

        self.positiveObj = []
        self.negativeObj = []
        self.negativeObjAll = []
        self.ctrlRigGrp = ''
        self.grpObjAll = []
        self.cMscNode = []

        self.IkhGrp = ''
        self.JntGrp = ''
        self.MscNodeGrp = ''
        self.TransFormGrp = ''
        self.crvPN = []
        self.crvSkinFB = []
        self.crvBshFB = []
        self.crvAll = ''
        self.ikhAll = []

        self.JntIkSpineGrp = ''
        self.ikhGrpObj = ''
        self.nodeKeepOut = ''
        self.nodeKeepOutAll = []
        self.keepOutNodeAll = []

        self.jointSpFntBck = []
        self.jointFnt = []
        self.jointBck = []

        self.dtlCtrl = []
        self.arcLengthAll = []
        self.ctrlMain = ''

        self.addRigGeoGrp = ''
        self.addRigStillGrp = ''
        self.addRigCtrlGrp = ''
    def run(self, positiveObj = '', negativeObj = [], name = '', elem = '', side = '', parent = '', *args):

        if side:
            if side == 'L':
                side = '_' + side + '_'
            elif side == 'R':
                side = '_' + side + '_'
            elif side == '_':
                pass
        else:
            side = '_'

        ## Create group
        self.addRigGeoGrp = 'AddRigGeo_Grp'
        self.addRigStillGrp = 'AddRigStill_Grp'
        self.addRigCtrlGrp = 'AddRigCtrl_Grp'

        self.IkhGrp = '{}Ikh{}Grp'.format(name, side)
        self.JntGrp = '{}Jnt{}Grp'.format(name, side)
        self.MscNodeGrp = '{}MscNode{}Grp'.format(name, side)
        self.TransFormGrp = '{}TransForm{}Grp'.format(name, side)
        self.crvAll = '{}Crv{}Grp'.format(name, side)
        self.ctrlRigGrp = '{}CtrlRig{}Grp'.format(name, side)


        if not mc.objExists(self.addRigGeoGrp):
            self.addRigGeoGrp = mc.group(n = 'AddRigGeo_Grp')
        if not mc.objExists(self.addRigStillGrp):
            self.addRigStillGrp = mc.group(n = 'AddRigStill_Grp')
        if not mc.objExists(self.addRigGeoGrp):
            self.addRigCtrlGrp = mc.group(n = 'AddRigCtrl_Grp')

        if not mc.objExists(self.IkhGrp):   
            self.IkhGrp = mc.group(n = '{}Ikh{}Grp'.format(name, side) , em = True)
            chainObj = mc.listRelatives(self.IkhGrp, p = True)
            if not chainObj:
                mc.parent(self.IkhGrp, self.addRigStillGrp) 
        if not mc.objExists(self.JntGrp):
            self.JntGrp = mc.group(n = '{}Jnt{}Grp'.format(name, side) , em = True)
            chainObj = mc.listRelatives(self.JntGrp, p = True)
            if not chainObj:
                mc.parent(self.JntGrp, self.addRigStillGrp) 
        if not mc.objExists(self.MscNodeGrp):
            self.MscNodeGrp = mc.group(n = '{}MscNode{}Grp'.format(name, side) , em = True)
            chainObj = mc.listRelatives(self.MscNodeGrp, p = True)
            if not chainObj:
                mc.parent(self.MscNodeGrp, self.addRigStillGrp) 
        if not mc.objExists(self.TransFormGrp):
            self.TransFormGrp = mc.group(n = '{}TransForm{}Grp'.format(name, side) , em = True)
            chainObj = mc.listRelatives(self.TransFormGrp, p = True)
            if not chainObj:
                mc.parent(self.TransFormGrp, self.addRigStillGrp) 
        if not mc.objExists(self.crvAll):
            self.crvAll = mc.group(n = '{}Crv{}Grp'.format(name, side) , em = True)
            chainObj = mc.listRelatives(self.crvAll, p = True)
            if not chainObj:
                mc.parent(self.crvAll, self.addRigStillGrp) 
        if not mc.objExists(self.ctrlRigGrp):
            self.ctrlRigGrp = mc.group(n = '{}CtrlRig{}Grp'.format(name, side) , em = True)
            chainObj = mc.listRelatives(self.ctrlRigGrp, p = True)
            if not chainObj:
                mc.parent(self.ctrlRigGrp, self.addRigCtrlGrp) 

        ## Generate parent GeoGrp
        mc.select(positiveObj, r = True)
        positiveObjSel = mc.pickWalk(d = 'Up')
        chainObj = mc.listRelatives(positiveObjSel, p = True)
        if not chainObj:
            mc.parent( positiveObjSel, self.addRigGeoGrp) 

        ## Generate Negative Object
        self.generateNegativeObj( negativeObj = negativeObj, name = name, elem = elem , side = side, parent = parent )

        ## Generate Positive Object
        self.generatePositiveObj( positiveObj = positiveObj ,name = name , elem = elem, side = side)

        mc.parent(self.nodeKeepOut, self.MscNodeGrp)
        jntGrpCheck = mc.listRelatives(parent, p = True)

        ## Generate local joint
        if not jntGrpCheck == None:
            if jntGrpCheck != self.JntGrp: 
                jntGrpCheck = mc.listRelatives(parent, p = True)[0]
                if not jntGrpCheck == self.JntGrp:
                    mc.parent(parent, self.JntGrp)
        mc.select(cl = True)


    def runEach(self, positiveObj = '', negativeObj = [], name = '', elem = '', side = '', parent = '', detailCtrl = False, *args):

        if side:
            if side == 'L':
                side = '_' + side + '_'
            elif side == 'R':
                side = '_' + side + '_'
            elif side == '_':
                pass
        else:
            side = '_'

        ## Create group
        self.addRigGeoGrp = 'AddRigGeo_Grp'
        self.addRigStillGrp = 'AddRigStill_Grp'
        self.addRigCtrlGrp = 'AddRigCtrl_Grp'

        self.IkhGrp = '{}Ikh{}Grp'.format(name, side)
        self.JntGrp = '{}Jnt{}Grp'.format(name, side)
        self.MscNodeGrp = '{}MscNode{}Grp'.format(name, side)
        self.TransFormGrp = '{}TransForm{}Grp'.format(name, side)
        self.crvAll = '{}Crv{}Grp'.format(name, side)
        self.ctrlRigGrp = '{}CtrlRig{}Grp'.format(name, side)


        if not mc.objExists(self.addRigGeoGrp):
            self.addRigGeoGrp = mc.group(n = 'AddRigGeo_Grp' , em = True)
        if not mc.objExists(self.addRigStillGrp):
            self.addRigStillGrp = mc.group(n = 'AddRigStill_Grp' , em = True)
        if not mc.objExists(self.addRigGeoGrp):
            self.addRigCtrlGrp = mc.group(n = 'AddRigCtrl_Grp' , em = True)
        print self.addRigCtrlGrp, '..addRig'
        if not mc.objExists(self.IkhGrp):   
            self.IkhGrp = mc.group(n = '{}Ikh{}Grp'.format(name, side) , em = True)
            chainObj = mc.listRelatives(self.IkhGrp, p = True)
            if not chainObj:
                mc.parent(self.IkhGrp, self.addRigStillGrp) 
        if not mc.objExists(self.JntGrp):
            self.JntGrp = mc.group(n = '{}Jnt{}Grp'.format(name, side) , em = True)
            chainObj = mc.listRelatives(self.JntGrp, p = True)
            if not chainObj:
                mc.parent(self.JntGrp, self.addRigStillGrp) 
        if not mc.objExists(self.MscNodeGrp):
            self.MscNodeGrp = mc.group(n = '{}MscNode{}Grp'.format(name, side) , em = True)
            chainObj = mc.listRelatives(self.MscNodeGrp, p = True)
            if not chainObj:
                mc.parent(self.MscNodeGrp, self.addRigStillGrp) 
        if not mc.objExists(self.TransFormGrp):
            self.TransFormGrp = mc.group(n = '{}TransForm{}Grp'.format(name, side) , em = True)
            chainObj = mc.listRelatives(self.TransFormGrp, p = True)
            if not chainObj:
                mc.parent(self.TransFormGrp, self.addRigStillGrp) 
        if not mc.objExists(self.crvAll):
            self.crvAll = mc.group(n = '{}Crv{}Grp'.format(name, side) , em = True)
            chainObj = mc.listRelatives(self.crvAll, p = True)
            if not chainObj:
                mc.parent(self.crvAll, self.addRigStillGrp) 
        if not mc.objExists(self.ctrlRigGrp):
            self.ctrlRigGrp = mc.group(n = '{}CtrlRig{}Grp'.format(name, side) , em = True)
            chainObj = mc.listRelatives(self.ctrlRigGrp, p = True)
            print chainObj, '...chainObj'
            if chainObj == None:
                print self.addRigCtrlGrp,'...self.addRigCtrlGrp'
                if mc.objExists(self.addRigCtrlGrp):
                    mc.parent(self.ctrlRigGrp, self.addRigCtrlGrp) 

        ## Generate parent GeoGrp
        mc.select(positiveObj, r = True)
        chainObj = mc.listRelatives(positiveObj, p = True)
        if chainObj == None:
            mc.parent( positiveObj, self.addRigGeoGrp) 

        ## Generate Negative Object
        self.generateNegativeObj( negativeObj = negativeObj, name = name, elem = elem , side = side, parent = parent , detailCtrl = detailCtrl)

        ## Generate Positive Object
        self.generatePositiveObj( positiveObj = positiveObj ,negativeObj = negativeObj, name = name , elem = elem, side = side)

        if parent:
            jntGrpCheck = mc.listRelatives(parent, p = True)
            if not jntGrpCheck == None:
                if jntGrpCheck != self.JntGrp: 
                    jntGrpCheck = mc.listRelatives(parent, p = True)[0]
                    if not jntGrpCheck == self.JntGrp:
                        mc.parent(parent, self.JntGrp)
        mc.select(cl = True)


    def generateNegativeObj(self, negativeObj = [], name = '', elem = '', side = '', parent = '', detailCtrl = True, *args):

        if side:
            if side == 'L':
                side = '_' + side + '_'
            elif side == 'R':
                side = '_' + side + '_'
            elif side == '_':
                pass
        else:
            side = '_'

        if not negativeObj:
            negativeObj = mc.ls(sl = True)

        for sel in negativeObj:
            name, side, lastName = utaCore.splitName(sels = [sel])
            # if not side == '_':
            #     side = '_' + side + '_'
            nodeShape = mc.createNode('cMuscleKeepOut' ,n = '{}KeepOutShape{}Node'.format(name , side))

            mc.setAttr("{}.inDirectionX".format(nodeShape), 0)
            mc.setAttr("{}.inDirectionY".format(nodeShape), 1)
            mc.setAttr("{}.inDirectionZ".format(nodeShape), 0)

            nodeKeepOuts = nodeShape.split('Shape')
            self.nodeKeepOut = nodeKeepOuts[0] + nodeKeepOuts[-1]
            self.keepOutNodeAll.append(self.nodeKeepOut)


            msgKeepOutJnt = '{}.msgKeepOut'.format(sel)
            if not mc.objExists(msgKeepOutJnt):
                attrObj = utaCore.addAttrCtrl (ctrl = sel, elem = ['msgKeepOut','msgKeepOutDriven','msgKeepOutXForm'], min = 0, max = 1, at = 'float')
            mc.connectAttr('{}.worldMatrix[0]'.format(self.nodeKeepOut), '{}.worldMatrixAim'.format(nodeShape))
            mc.connectAttr('{}.message'.format(self.nodeKeepOut), '{}.msgKeepOutXForm'.format(sel))

            grpObj = mc.group(n = '{}{}{}Node'.format(name, elem , side) , em = True)

            self.grpObjAll.append(grpObj)

            mc.connectAttr('{}.outTranslateLocal'.format(nodeShape), '{}.translate'.format(grpObj))
            mc.connectAttr('{}.message'.format(grpObj), '{}.msgKeepOutDriven'.format(sel))
            mc.connectAttr('{}.message'.format(nodeShape), '{}.msgKeepOut'.format(sel))

            utaCore.snapObj(objA = negativeObj[0], objB = self.nodeKeepOut)
            mc.parent(grpObj, self.nodeKeepOut)
            pm.makeIdentity ( grpObj , apply = True, r = True )

            grpJnt = mc.rename(utaCore.zeroGroup( obj = sel), '{}{}{}Grp'.format(name, lastName , side))

            mscGrp = mc.group(em = True, n = '{}KeepOut{}Grp'.format(name , side))
            utaCore.snapObj(objA = sel, objB = self.nodeKeepOut)
            utaCore.snapObj(objA = self.nodeKeepOut, objB = mscGrp)
            mc.parent(self.nodeKeepOut, mscGrp)

            ## Parent ..KeepOut_Node at ..MscNode_Grp
            parentObj = mc.listRelatives(mscGrp, p = True)
            if parentObj == None:
                mc.parent(mscGrp, self.MscNodeGrp)

            mc.select(sel, r = True)
            if detailCtrl:
                grpCtrlName, grpCtrlOfsName, grpCtrlParsName, ctrlName, GmblCtrlName = utaCore.createControlCurve(curveCtrl = 'cube', parentCon = False, connections = False, geoVis = False)
                mc.parent(grpCtrlName, self.ctrlRigGrp)
                mc.parentConstraint(mscGrp, grpCtrlName, mo = True)
                mc.scaleConstraint(mscGrp, grpCtrlName, mo = True)   
                mc.parentConstraint(GmblCtrlName, self.nodeKeepOut, mo = True)
                mc.scaleConstraint(GmblCtrlName, self.nodeKeepOut, mo = True)    

                self.dtlCtrl.append(grpCtrlName)

            ## Connection 
            mc.parent(grpJnt, self.JntGrp)
            mc.select(grpObj, r = True)
            mc.select(sel, add = True)
            utaCore.connectTRS(trs = True, translate = False, rotate = False, scale = False)    

            self.nodeKeepOutAll.append(self.nodeKeepOut)
            self.negativeObjAll.append(sel)
        print self.negativeObjAll, '..self.negativeObjAll'

    def generatePositiveObj(self, positiveObj = '',negativeObj = [], name = '', elem = '', side = '', *args):

        if side:
            if side == 'L':
                side = '_' + side + '_'
            elif side == 'R':
                side = '_' + side + '_'
            elif side == '_':
                pass
        else:
            side = '_'

        if not positiveObj:
            positiveObj = mc.ls(sl = True)[0]

        for i in range(len(negativeObj)):
            ## Generate cMuscleObject
            self.cMscNode = mc.createNode ('cMuscleObject', n = '{}{}{}ConObj{}Cms'.format(name ,i+1 , elem, side))
            mc.connectAttr('{}.worldMatrix[0]'.format(positiveObj), '{}.worldMatrixStart'.format(self.cMscNode))
            mc.connectAttr('{}.worldMesh[0]'.format(positiveObj + 'Shape'), '{}.meshIn'.format(self.cMscNode))

            # ## Generate Connect Muscle to KeepOut
            print self.nodeKeepOutAll[i], '....self.nodeKeepOutAll[i]...'
            print i , '...i'
            mc.connectAttr('{}.muscleData'.format(self.cMscNode), '{}.muscleData[0]'.format(self.nodeKeepOutAll[i]))
            self.cMscNode = []

            ## Parent Tranform
            mc.select('{}{}{}ConObj{}Cms'.format(name, i+1,  elem, side ))
            transObj = mc.pickWalk( d = 'Up')[0]
            mc.setAttr('{}.visibi lity'.format(transObj), 0)
            mc.parent(transObj, self.TransFormGrp)

    def generateSpineIkCrv(self, cur_name = '', span_curve = 20, name = '', elem = '', side = '', localPar = '',  localDrive = '', bindSkinGeo = '', *args):

        if side:
            if side == 'L':
                side = '_' + side + '_'
            elif side == 'R':
                side = '_' + side + '_'
            elif side == '_':
                pass
        else:
            side = '_'

        ## Generate LocalDrive
        localDriveJnt = mc.createNode('joint', n = '{}LocalDrive{}Jnt'.format(name, side))
        localDriveJntGrp = mc.group(em = True, n = '{}LocalDriveJnt{}Grp'.format(name, side))
        mc.parent(localDriveJnt, localDriveJntGrp)
        utaCore.snapObj(objA = localDrive, objB = localDriveJntGrp)
        mc.parent(localDriveJntGrp, self.JntGrp)

        # mc.select(localDrive, r = True)
        # mc.select(localDriveJnt, add = True)
        # utaCore.connectTRS(trs = True, translate = False, rotate = False, scale = False)


        self.JntIkSpineGrp = '{}JntIkSpine{}Grp'.format(name , side)
        if not mc.objExists(self.JntIkSpineGrp):   
            self.JntIkSpineGrp = mc.group(n = '{}JntIkSpine{}Grp'.format(name, side) , em = True)

        ## Duplicate Curve 
        crvPosi = mc.rename(cur_name, '{}Fnt{}Crv'.format(name, side))
        crvNega = mc.duplicate(crvPosi, n = '{}Bck{}Crv'.format(name, side))[0]
        crvSkinFnt = mc.duplicate(crvPosi, n = '{}SkinFnt{}Crv'.format(name, side))[0]
        crvSkinBck = mc.duplicate(crvPosi, n = '{}SkinBck{}Crv'.format(name, side))[0]
        crvBshFnt = mc.duplicate(crvPosi, n = '{}BshFnt{}Crv'.format(name, side))[0]
        crvBshBck = mc.duplicate(crvPosi, n = '{}BshBck{}Crv'.format(name, side))[0]
        self.crvSkinFB = [crvSkinFnt, crvSkinBck]
        self.crvBshFB = [crvBshFnt, crvBshBck]

        ## ReverseCurve 
        mc.reverseCurve(crvNega, ch = False, rpo = True)
        pm.makeIdentity ( crvNega , apply = True ) 
        mc.reverseCurve(crvSkinBck, ch = False, rpo = True)
        pm.makeIdentity ( crvSkinBck , apply = True ) 
        mc.reverseCurve(crvBshBck, ch = False, rpo = True)
        pm.makeIdentity ( crvBshBck , apply = True ) 

        ## Create Joint 
        self.jointFnt, self.jointBck = utaCore.createJointOnCurve(cur_name = crvPosi, name = name, elem = elem, side = side,  span_curve = span_curve)

        # print localDrive, '...localDrive'
        mc.select(localDrive, r = True)

        grpCtrlName, grpCtrlOfsName, grpCtrlParsName, ctrlName, GmblCtrlName = utaCore.createControlCurve(curveCtrl = 'circle', parentCon = False, connections = False, geoVis = False)
        self.ctrlMain = ctrlName
        mc.parent(grpCtrlName, self.ctrlRigGrp)

        ## Generate Attribute Amp
        utaCore.attrNameTitle(obj = self.ctrlMain, ln = 'Functions')

        ## Create ik spine
        jointFBAll = [self.jointFnt, self.jointBck]
        self.crvPN = [crvPosi, crvNega]
        offsetIkSpine = [-16, -0.68]
        spineIkMdvAll = []
        for i in range(len(jointFBAll)):
            ## Split Name Curve
            name, side, lastName = utaCore.splitName(sels = [self.crvPN[i]])
            # if not side == '_':
            #     side = '_' + side + '_'
            mc.select(jointFBAll[i])
            utaCore.parentObj(jntName = '', Side = '', lastName = '')
            ikhName = '{}{}Ikh'.format(name, side)
            utaCore.createIkhSpine (strJnt = jointFBAll[i][0], endJnt = jointFBAll[i][-1] ,curve = self.crvPN[i], name = ikhName, sol = 'ikSplineSolver')
            self.ikhAll.append(ikhName)
            mc.parent(jointFBAll[i][0], self.JntIkSpineGrp)
            mc.parent('{}{}Ikh'.format(name, side), self.IkhGrp)

            ## Generate Attribute Amp
            attrSpineIk = utaCore.addAttrCtrl (ctrl = self.ctrlMain, elem = ['{}{}PositonSpineIk'.format(name, side)], min = -200, max = 200, at = 'float')[0]
            mc.setAttr('{}.{}{}PositonSpineIk'.format(self.ctrlMain, name, side), offsetIkSpine[i])
            spineIkMdv = mc.createNode('multiplyDivide', n = '{}{}_Mdv'.format(name, side))
            spineIkMdvAll.append(spineIkMdv)
            mc.setAttr('{}.input2X'.format(spineIkMdv), 0.025)
            mc.connectAttr('{}.{}'.format(self.ctrlMain, attrSpineIk), '{}.input1X'.format(spineIkMdv))


        ## BlendShape crvBshFB to crvPN
        for i in range(len(self.crvBshFB)):
            bshNode = mc.blendShape(self.crvBshFB[i], self.crvPN[i], frontOfChain = True, name = '{}{}Bsh'.format(self.crvBshFB[i], side))[0]
            mc.setAttr ('{}.{}'.format(bshNode, self.crvBshFB[i]),  0.1)

        ## Generate SpineIk joint stretch 
        self.generateSpineIkJntStretch (jointFnt = self.jointFnt, jointBck = self.jointBck,  crvPN = self.crvPN, name = name, elem = elem ,side = side ,bindSkinGeo = bindSkinGeo,)

        offsetIkh = [0, 0.45]
        ## Generate Connect Amp Default of Stretch Spine Ik Joint
        for i in range(len(self.crvPN)):
            name, side, lastName = utaCore.splitName(sels = [self.crvPN[i]])
            # if not side == '_':
            #     side = '_' + side + '_'
            mc.connectAttr('{}.outputX'.format(spineIkMdvAll[i]), '{}D{}Adl.default'.format(name, side))
            ## Generate Set Attribute ikh position
            offsetIkhAttr = utaCore.addAttrCtrl (ctrl = self.ctrlMain, elem = ['{}{}OffsetIkh'.format(name, side)], min = -200, max = 200, at = 'float')[0]
            mc.setAttr ('{}.{}'.format(self.ctrlMain, offsetIkhAttr), offsetIkh[i])
            offsetIkhMdv = mc.createNode('multiplyDivide', n = '{}{}offsetIkh_Mdv'.format(name, side))
            mc.setAttr ('{}.input2X'.format(offsetIkhMdv), 0.1)
            mc.connectAttr ('{}.{}{}OffsetIkh'.format(self.ctrlMain, name, side), '{}.input1X'.format(offsetIkhMdv))
            mc.connectAttr ('{}.outputX'.format(offsetIkhMdv), '{}.offset'.format(self.ikhAll[i]))


        for i in range(len(jointFBAll)):
            for each in jointFBAll[i]:
                self.jointSpFntBck.append(each)

        print self.jointSpFntBck, '..self.jointSpFntBck'
        utaCore.bindSkinCluster(jntObj = self.jointSpFntBck, obj = [bindSkinGeo])

        ## BindSkin Curve
        print localDriveJnt, '...localDriveJnt'
        utaCore.bindSkinCluster(jntObj = [localDriveJnt], obj = self.crvBshFB)

        ## BindSkin NegativePos to CrvPN
        print self.negativeObjAll, '...self.negativeObjAll'
        utaCore.bindSkinCluster(jntObj = self.negativeObjAll, obj = self.crvPN)

        ## Generate ParentConstraint  localParent and MscNode
        utaCore.parentScaleConstraintLoop(obj = localPar, lists = [ self.MscNodeGrp],par = True, sca = True, ori = False, poi = False)


        ## Generate Wrap Joint
        mc.select(localDrive, r = True)
        obj = mc.pickWalk(d =  'up')
        mc.parent(obj, self.JntGrp)

        ## Generate NegativeGeo to GeoGrp
        mc.select(bindSkinGeo, r = True)
        bindSkinGeoSel = mc.pickWalk(d = 'Up')
        chainObj = mc.listRelatives(bindSkinGeoSel, p = True)
        if not chainObj:
            mc.parent( bindSkinGeoSel, self.addRigGeoGrp) 

        mc.parent(self.JntIkSpineGrp, self.JntGrp)
        mc.parent(crvPosi, crvNega, crvSkinFnt, crvSkinBck, crvBshFnt, crvBshBck, self.crvAll)

        # return self.jointSpFntBck, self.jointFnt, self.jointBck,  self.crvPN, self.crvSkinFB, self.crvBshFB


    def generateSpineIkJntStretch (self, jointFnt = [], jointBck = [], crvPN = [],name = '', elem = '',side = '', bindSkinGeo = '', *args):

        if side:
            if side == 'L':
                side = '_' + side + '_'
            elif side == 'R':
                side = '_' + side + '_'
            elif side == '_':
                pass
        else:
            side = '_'

        jointAll = [jointFnt, jointBck]
        for i in range(len(jointAll)):

            name, side, lastName = utaCore.splitName(sels = [self.crvPN[i]])
            # if not side == '_':
            #     side = '_' + side + '_'
            # shadingNode -asUtility curveInfo
            crvName = crvPN[i].split('_Crv')[0]
            crvInfoNode = mc.createNode('curveInfo', n = '{}CrvInfoNode{}Crv'.format(name, side))
            clampNode = mc.createNode('clamp', n = '{}limit{}Cmp'.format(name, side))
            mc.connectAttr('{}Shape.worldSpace[0]'.format(crvPN[i]),  '{}.inputCurve'.format(crvInfoNode))
            mdvANode = mc.createNode ('multiplyDivide', n = '{}A{}Mdv'.format(name, side))
            mdvBNode = mc.createNode ('multiplyDivide', n = '{}B{}Mdv'.format(name, side))
            adlNode = mc.createNode('addDoubleLinear', n = '{}D{}Adl'.format(name, side))
            mdvCNode = mc.createNode ('multiplyDivide', n = '{}E{}Mdv'.format(name, side))
            mdvDNode = mc.createNode ('multiplyDivide', n = '{}C{}Mdv'.format(name, side))

            mc.addAttr( adlNode , ln = 'default' , at = 'float' , k = True )

            mc.setAttr('{}.default'.format(adlNode), 0.12)
            mc.setAttr ('{}.operation'.format(mdvANode), 2)
            mc.setAttr ('{}.operation'.format(mdvBNode), 2)
            mc.setAttr ('{}.operation'.format(mdvCNode), 3)
            mc.setAttr ('{}.operation'.format(mdvDNode), 1)

            mc.connectAttr ('{}.arcLength'.format(crvInfoNode),  '{}.inputR'.format(clampNode))

            ## Get arcLength
            arcLength = mc.getAttr('{}.inputR'.format(clampNode))
            limitArc = 13
            mc.setAttr ('{}.input2X'.format(mdvANode), arcLength)
            mc.setAttr ('{}.minR'.format(clampNode), arcLength)

            mc.connectAttr ('{}.outputR'.format(clampNode),  '{}.input1X'.format(mdvANode))
            mc.connectAttr ('{}.outputX'.format(mdvANode), '{}.input1X'.format(mdvBNode))

            mc.connectAttr ('{}.default'.format(adlNode), '{}.input1'.format(adlNode))
            mc.connectAttr ('{}.outputX'.format(mdvBNode), '{}.input2'.format(adlNode))
            mc.connectAttr ('{}.outputX'.format(mdvBNode), '{}.input1X'.format(mdvCNode))
            mc.connectAttr ('{}.output'.format(adlNode), '{}.input2X'.format(mdvCNode))
            mc.connectAttr ('{}.outputX'.format(mdvCNode), '{}.input1X'.format(mdvDNode))

            ## Generate Attribute Limit arcLength
            limitCrv = utaCore.addAttrCtrl (ctrl = self.ctrlMain, elem = ['{}{}limitArcLength'.format(name, side)], min = -200, max = 200, at = 'float')[0]
            mc.setAttr ('{}.{}'.format(self.ctrlMain, limitCrv), limitArc)
            mc.connectAttr ('{}.{}'.format(self.ctrlMain, limitCrv), '{}.maxR'.format(clampNode))
            
            ## Generate Connect arcLength to spineIk joint (scaleY)
            for each in jointAll[i]:
                mc.connectAttr('{}.outputX'.format(mdvDNode), '{}.sy'.format(each))

        # ## Generate Wire curve at object
        # wireNode = utaCore.wireObj (bindSkinGeo = bindSkinGeo, wireObj = self.crvSkinFB,name = name, elem = elem, side = side)

        # mc.setAttr ('{}.dropoffDistance[1]'.format(wireNode[0]), 1)
        # mc.setAttr ('{}.dropoffDistance[0]'.format(wireNode[0]), 1)
        # mc.setAttr ('{}.rotation'.format(wireNode[0]), 0)
        mc.select(cl = True)
        # wire = mc.wire(bindSkinGeo,w=[self.crvSkinFB[0],self.crvSkinFB[1]],n='wire_test')
