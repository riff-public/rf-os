import os
import sys
import maya.cmds as mc
from utaTools.utapy import copyObjFile
reload(copyObjFile)
from rf_utils.context import context_info
import shutil
import re
from utaTools.utapy import utaCore
reload(utaCore)
from rf_utils import file_utils
from utils.crvScript import ctrlData
reload(ctrlData)

#path project config
rootScript = os.environ['RFSCRIPT']
pathProjectJson = r"{}\core\rf_maya\rftool\rig\export_config\project_config.json".format(rootScript)
projectData = file_utils.json_loader(pathProjectJson)


def runMainRig():
	asset = context_info.ContextPathInfo() 

	scn_path = os.path.normpath( mc.file( q = True , l = True)[0])
	scn_fld_path , scn_nm_ext = os.path.split(scn_path)
	scn_fld_data , scn_he_ext = os.path.split(scn_fld_path)
	rd_path = os.path.join(scn_fld_data, 'data')
	mr_path = os.path.join(rd_path, 'mainRig.py')
	bpath = rd_path

	projectName = asset.project if mc.file(q=True, loc=True) else ''
	# getPath
	proPath = copyObjFile.getDataFld()
	mainPath = os.path.join(proPath, 'mainRig.py')
	if projectName in projectData['project'] :
		if not 'Oa' in asset.name:
			CheckBackUpScrip()
		if os.path.exists(mainPath):
			sys.path.insert(0, copyObjFile.getDataFld())
			import mainRig
			reload(mainRig)

			try:
				mainRig.main()
			except:
				pass

			del(mainRig)
			print 'PROJECT NAME :', projectName

	else:
		if os.path.exists(mr_path):
			sys.path.insert(0, bpath)
			import mainRig
			reload(mainRig)
			try :
				mainRig.main()
			except :
				pass
			del(mainRig)
			print 'PROJECT NAME :', projectName

def runPrevisRig():
	# Path ---------------------------------------
	corePath = '%s/core' % os.environ.get('RFSCRIPT')
	previsPath = '{}/rf_maya/rftool/rig/template/templatePrevis/data'.format(corePath)
	asset = context_info.ContextPathInfo() 
	projectName = asset.project if mc.file(q=True, sn=True) else ''

	sys.path.insert(0, previsPath)
	import mainRig
	reload(mainRig)
	mainRig.main()


	print '# Generate >> PROJECT NAME :', projectName

#backup RunMainRig
def backupMainRig():
    proPath = copyObjFile.getDataFld()
    mainPath = os.path.join(proPath, 'mainRig.py')
    dstDir = os.path.join(proPath, 'backup_mainRig')

    if os.path.exists(mainPath) :
        if not os.path.exists(dstDir) :
            os.makedirs(dstDir)
        listDir = os.listdir(dstDir)

        if listDir:
            lastVer = re.findall('\\d+', listDir[-1])
            newVer = int(lastVer[0])+1
        else :
            newVer = 1

        newName = 'mainRig_v%03d.py' % newVer
        dst = os.path.join(dstDir, newName)

        shutil.copy2(mainPath, dst)
        print 'mainRig already Backup to : ' + dst 

	if os.path.exists(dst):
		return dst

def CheckBackUpScrip(*args):
    checkMessage = mc.confirmDialog( 	title='Confirm', 
						                message='Do you want backup Scrip?', 
						                button=['Yes','No'], 
						                defaultButton='Yes', 
						                cancelButton='No', 
						                dismissString='No' )
    if checkMessage == 'Yes':
        backupMainRig()
        print 'Backup "mainRig.py" Done!!'
    if checkMessage == 'No':
        print 'Thank you'


def runBpmRig():
	asset = context_info.ContextPathInfo() 

	scn_path = os.path.normpath( mc.file( q = True , l = True)[0])
	scn_fld_path , scn_nm_ext = os.path.split(scn_path)
	scn_fld_data , scn_he_ext = os.path.split(scn_fld_path)
	rd_path = os.path.join(scn_fld_data, 'data')
	mr_path = os.path.join(rd_path, 'bpmRig.py')
	bpath = rd_path

	# project = asset.project
	projectName = asset.project if mc.file(q=True, sn=True) else ''
	# if maya is not projectName to pass
		#Exsample a = 1 if (1<0) else 2


	# getPath
	proPath = copyObjFile.getDataFld()
	mainPath = os.path.join(proPath, 'bpmRig.py')
	# print mainPath, 'mainPath'
	if projectName in projectData['project'] :
		CheckBackUpBpmScrip()
		if os.path.exists(mainPath):

			sys.path.insert(0, copyObjFile.getDataFld())

			import bpmRig
			reload(bpmRig)

			try:
				bpmRig.BpmRig()
			except :
				pass

			del(bpmRig)
			print 'PROJECT NAME :', projectName

#backup RunMainRig
def backupBpmRig():
    proPath = copyObjFile.getDataFld()
    bpmPath = os.path.join(proPath, 'bpmRig.py')
    dstDir = os.path.join(proPath, 'backup_bpmRig')

    if os.path.exists(bpmPath) :
        if not os.path.exists(dstDir) :
            os.makedirs(dstDir)
        listDir = os.listdir(dstDir)

        if listDir:
            lastVer = re.findall('\\d+', listDir[-1])
            newVer = int(lastVer[0])+1
        else :
            newVer = 1

        newName = 'bpmRig_v%03d.py' % newVer
        dst = os.path.join(dstDir, newName)

        shutil.copy2(bpmPath, dst)
        print 'bpmRig File already Backup to : ' + dst 

	if os.path.exists(dst):
		return dst

def CheckBackUpBpmScrip(*args):
    checkMessage = mc.confirmDialog( title='Confirm', 
                message='Do you want backup Scrip?', 
                button=['Yes','No'], 
                defaultButton='Yes', 
                cancelButton='No', 
                dismissString='No' )
    if checkMessage == 'Yes':
        backupBpmRig()
        print 'Backup "bpmRig.py" Done!!'
    if checkMessage == 'No':
        print 'Thank you'
