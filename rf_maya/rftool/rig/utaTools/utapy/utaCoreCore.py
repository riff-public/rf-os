import maya.cmds as mc
from utaTools.utapy import utaCore
reload(utaCore)
import pymel.core as pm;
import maya.mel as mel;

################################################################################################
# from utaTools.utapy import utaCoreCore
# reload(utaCoreCore)
# utaCoreCore.run(name = 'Tail' , FkSpansJnt = 20 , IkSpansJnt = 4 )
################################################################################################
class Class( object ):
    def __init__( self):
        self.fk = 'Fk'
        self.ik = 'Ik'
        self.paths = 'Paths'
        self.pathsUp = 'PathsUp'

        self.jntAll = []
        self.points = []
    # self.ui       = 'ClothSetupUI';
    # self.width    = 500.00;
    # self.title    = 'Cloth Setup Tools';
    # self.version  = 1.0;

    def run(self, name, FkSpansJnt, IkSpansJnt):
        print 'a'
        ## Select
        sels = mc.ls(sl = True)

        ## Create joint 
        self.createJntOnCrv(selA = sels[0], selB = sels[1], name = name, elem = self.fk ,spansJnt = FkSpansJnt, radius = 0.8)
        self.createJntOnCrv(selA = sels[0], selB = sels[1], name = name, elem = self,ik ,spansJnt = IkSpansJnt, radius = 1)
        self.createJntOnCrv(selA = sels[0], selB = sels[1], name = name, elem = self.paths ,spansJnt = FkSpansJnt, radius = 0.4)
        self.createJntOnCrv(selA = sels[0], selB = sels[1], name = name, elem = self.pathsUp ,spansJnt = FkSpansJnt, radius = 0.6)

        ## Clean Jnt Select Guide
        mc.delete(sels)


    def createJntOnCrv(self, selA, selB, name, elem ,spansJnt, radius):



        sels = [selA, selB]
        ## Create Group
        crvGrp = mc.group(n = '%s%sCrv_Grp' % (name, elem), em = True)
        jntGrp = mc.group(n = '%s%sJnt_Grp' % (name, elem), em = True)

        # for ix in range(len(countJnt)):
        for each in sels:
            pos = self.getWStransform(each)
            self.points.append(pos)
        mc.curve(d=3, ep=[self.points[0], self.points[1]])
        curve = mc.ls(selection=True)
        crvRename = mc.rename(curve,'%s%s_Crv' % (name, elem))
        mc.parent(crvRename,crvGrp)

        ## ReBuild Curve
        utaCore.reBuildCurve(lists = [crvRename], spans = spansJnt , degree = 1) 

        for i in range(spansJnt):
            addCv =  '%s.cv[%s]' % (crvRename, i)
            mc.select (addCv)
            jnt = utaCore.jointFromLocator()
            jntName = mc.rename(jnt, '%s%s%s_Jnt' % (name, elem,  i+1))
            mc.setAttr('%s.radius' % jntName, radius)
            self.jntAll.append(jntName)

        ## Parent Joint and Aim AxisJnt
        mc.select(self.jntAll)  
        utaCore.parentObj(jntName = '', Side = '', lastName = '')
        mc.parent('%s%s1_Jnt' % (name, elem), jntGrp)





    def getWStransform(self, obj):
        transform = mc.xform( obj, q=True, ws=True, t=True )
        return transform


    def createIkSpine(self):
        print '21. Creating Ik Spind Handle'
        ikhGrp = mc.group(em= True, n = ('%sTailIkhZro_Grp' % elem))
        strCurveIk = mc.ikHandle(sj=IkJnt[0], ee=IkJnt[-1], c=crvGuide, n= elem + 'Tail_Ikh', sol='ikSplineSolver', ccv=False, rootOnCurve=True, parentCurve=False)[0]
        mc.parent( strCurveIk, ikhGrp)
        mc.parent( ikhGrp, mgObj.Ikh_Grp)