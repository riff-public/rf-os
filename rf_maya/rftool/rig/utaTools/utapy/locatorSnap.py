import maya.cmds as mc
import getNameSpace as gns 
reload(gns)

def locatorSnap (ns= '', *args):
    # naemSpace = 'nakee_combRig:'
    lstPthMTmp = mc.ls('%sPathsM*_Jnt' % ns)
    lstPthUpJnt = mc.ls('%sPathsUp*_Jnt' % ns)
    lstTailPthTmp = mc.ls('%sTailPathsTmp*_Jnt' % ns)
    rootLocTmp = ('%sRootCtrlZr_Grp' % ns)

    lstOfstGrpCtrl = mc.ls('%sTailFkCtrlOfst_*_Grp' % ns)
    lstLoc = []

    NoLstPthTmp = len(lstPthMTmp)

    counter = 1 ;
    b = 0
    grpLocMn = mc.createNode('transform',n='%sLocatorSnap_Grp' % ns)
    mc.hide(grpLocMn)

    for a in range(0,1):
        # First Control
        grpLoc = mc.createNode('transform',n='%slocSnp%s_Grp' % (ns, str(a+1).zfill(2)))
        locSnp = mc.spaceLocator(n='%slocSnp%s_Loc' % (ns, str(a+1).zfill(2)))
        counter += 1
        
        mc.parent(locSnp,grpLoc)
        mc.parent(grpLoc,grpLocMn)
        pointCon = mc.pointConstraint(lstPthMTmp[a],grpLoc)
        mc.delete ( pointCon ) 
        mc.pointConstraint ( lstPthUpJnt[a] , locSnp , mo = True )
        mc.orientConstraint(locSnp,lstOfstGrpCtrl[a] , mo = False)
        mc.aimConstraint(lstTailPthTmp[a+1],locSnp, mo = False, aim = (0.0,0.0,-1.0) , u = (0.0,1.0,0.0), wut= 'vector')
        
        # Root Control
        grpLoc = mc.createNode('transform',n='%srootLocSnp%s_Grp' % (ns, str(a+1).zfill(2)))
        locSnp = mc.spaceLocator(n='%srootLocSnp%s_Loc' % (ns, str(a+1).zfill(2)))
        counter += 1

        mc.parent(locSnp,grpLoc)
        mc.parent(grpLoc,grpLocMn)

        pointCon = mc.pointConstraint(lstPthMTmp[a],grpLoc)
        mc.delete ( pointCon ) 
        mc.pointConstraint ( lstPthUpJnt[a] , locSnp , mo = True )
        mc.orientConstraint(locSnp,rootLocTmp , mo = False)
        mc.aimConstraint(lstTailPthTmp[a+1],locSnp, mo = False, aim = (0.0,0.0,-1.0) , u = (0.0,1.0,0.0), wut= 'vector')

        a += 1

        a = 1
        for i in range(NoLstPthTmp):
            grpLoc = mc.createNode('transform',n='%slocSnp%s_Grp' %(ns, str(a+1).zfill(2)))
            locSnp = mc.spaceLocator(n = '%slocSnp%s_Loc' % (ns, str(a+1).zfill(2)))
            # counter += 1

            mc.parent(locSnp,grpLoc)
            mc.parent(grpLoc,grpLocMn)
            # print i
            pointCon = mc.pointConstraint(lstPthMTmp[i],grpLoc)
            mc.delete ( pointCon ) 
            
            mc.pointConstraint ( lstPthUpJnt[i] , locSnp , mo = True )
            
            lstLoc.append(locSnp)
            
            if i != 0 :
                mc.orientConstraint(locSnp,lstOfstGrpCtrl[i] , mo = False)
        
            if i != (NoLstPthTmp - 1) :
                mc.aimConstraint(lstTailPthTmp[i+1],locSnp, mo = False, aim = (0.0,1.0,0.0) , u = (0.0,0.0,1.0), wut= 'vector')
            a+=1

def locatorSnapObj (ns= '', obj = '', *args):
    # naemSpace = 'nakee_combRig:'
    lstPthMTmp = mc.ls('%sPathsM*_Jnt' % ns)
    lstPthUpJnt = mc.ls('%sPathsUp*_Jnt' % ns)
    lstTailPthTmp = mc.ls('%sTailPathsTmp*_Jnt' % ns)
    rootLocTmp = ('%sRootCtrlZr_Grp' % ns)

    lstOfstGrpCtrl = mc.ls(obj)
    print lstOfstGrpCtrl, 'lstOfstGrpCtrl'
    lstLoc = []

    NoLstPthTmp = len(lstPthMTmp)

    counter = 1 ;
    b = 0
    grpLocMn = mc.createNode('transform',n='%sLocatorSnap_Grp' % ns)
    mc.hide(grpLocMn)

    for a in range(0,1):
        # First Control
        grpLoc = mc.createNode('transform',n='%slocSnp%s_Grp' % (ns, str(a+1).zfill(2)))
        locSnp = mc.spaceLocator(n='%slocSnp%s_Loc' % (ns, str(a+1).zfill(2)))
        counter += 1
        
        mc.parent(locSnp,grpLoc)
        mc.parent(grpLoc,grpLocMn)
        pointCon = mc.pointConstraint(lstPthMTmp[a],grpLoc)
        mc.delete ( pointCon ) 
        mc.pointConstraint ( lstPthUpJnt[a] , locSnp , mo = True )
        mc.orientConstraint(locSnp,lstOfstGrpCtrl[a] , mo = False)
        mc.aimConstraint(lstTailPthTmp[a],locSnp, mo = False, aim = (0.0,0.0,-1.0) , u = (0.0,1.0,0.0), wut= 'vector')
        
        # Root Control
        grpLoc = mc.createNode('transform',n='%srootLocSnp%s_Grp' % (ns, str(a+1).zfill(2)))
        locSnp = mc.spaceLocator(n='%srootLocSnp%s_Loc' % (ns, str(a+1).zfill(2)))
        counter += 1

        mc.parent(locSnp,grpLoc)
        mc.parent(grpLoc,grpLocMn)

        pointCon = mc.pointConstraint(lstPthMTmp[a],grpLoc)
        mc.delete ( pointCon ) 
        mc.pointConstraint ( lstPthUpJnt[a] , locSnp , mo = True )
        mc.orientConstraint(locSnp,rootLocTmp , mo = False)
        mc.aimConstraint(lstTailPthTmp[a+1],locSnp, mo = False, aim = (0.0,0.0,-1.0) , u = (0.0,1.0,0.0), wut= 'vector')

        a += 1

        a = 1
        for i in range(NoLstPthTmp):
            grpLoc = mc.createNode('transform',n='%slocSnp%s_Grp' %(ns, str(a+1).zfill(2)))
            locSnp = mc.spaceLocator(n = '%slocSnp%s_Loc' % (ns, str(a+1).zfill(2)))
            # counter += 1

            mc.parent(locSnp,grpLoc)
            mc.parent(grpLoc,grpLocMn)
            # print i
            pointCon = mc.pointConstraint(lstPthMTmp[i],grpLoc)
            mc.delete ( pointCon ) 
            
            mc.pointConstraint ( lstPthUpJnt[i] , locSnp , mo = True )
            
            lstLoc.append(locSnp)
            
            if i != 0 :
                mc.orientConstraint(locSnp,lstOfstGrpCtrl[i] , mo = False)
        
            if i != (NoLstPthTmp - 1) :
                mc.aimConstraint(lstTailPthTmp[i+1],locSnp, mo = False, aim = (0.0,1.0,0.0) , u = (0.0,0.0,1.0), wut= 'vector')
            a+=1            