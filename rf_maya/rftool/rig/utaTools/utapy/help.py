#---------------------------------------------------------------------------------------------------------
#(py)scrip write controlShape  
import pkmel.ctrlShapeTools as pct
pct.writeAllCtrl()
#---------------------------------------------------------------------------------------------------------

#(py)scrip read controlShape  
import pkmel.ctrlShapeTools as pct
pct.readAllCtrl()
#---------------------------------------------------------------------------------------------------------


#(py)scrip write and read (chang) skinWeight
import pkmel.weightTools as weightTools
weightTools.readSelectedWeight()

help( weightTools.readWeight )

weightTools.readWeight( 'body_ply' , r'Y:\SHARE\nun\baseRigFriends\data\body_plyWeight.txt' )

#---------------------------------------------------------------------------------------------------------
#(py)gen  ribbon

import pkmel.ribbon as pkr
reload( pkr )

import pkmel.rigTools as rigTools

help( rigTools )
# create Ribbon ( distanceDimension)
rbnObj = pkr.RibbonIkHi( 48.940 , 'z+' )  ## pkr.RibbonIkHi  or pkr.RibbonIkLow

dir( rbnObj )
# rename Ribbon 
rigTools.dummyNaming(rbnObj, attr='rbn', dummy='windRbn', charName='', elem='', side='')

#---------------------------------------------------------------------------------------------------------
reverse Curve  >>|<<  Edit Curves> Reverse Curve Direction

#scrip run Rig dog
sys.path.append(r'O:\systemTool\python\UTAModul')
import UTAModul.rig_dog  as dog
reload(dog)

dog.main()

#----------------------publish combRig  of Rig------------------------------------------------------
import pkmel.rigTools as rigTools
reload( rigTools )

rigTools.importRigElement()
rigTools.importAllReference()  #import Reference
rigTools.removeAllNamespace()  # remove NameSpace
rigTools.connectProxySkinJoint()  #connect ProxySkinJoint
#---------------------------------------------------------------------------------------------------


#----------------------Cut and Connect BS-----------------------------------------------------------
import pkmel.rigTools as rigTools
reload( rigTools )
import pkmel.blendShapeTools as blendShapeTools
reload( blendShapeTools )

rigTools.detachSelectedEdge()# cut HeadBody  @ bsh
blendShapeTools.dupObjectForBlendShape()# Duplicate Head to blendShape @ bsh
#importControl BSH "O:\globalMaya\python\ptTools\pkmel\bshCtrlFriends.ma"  , "C:\Users\ken\Dropbox\scrip\Template Joint & Scrip\mouth Control.ma"
blendShapeTools.connectBlendShape() # connect 1 blendshape @ bsh
blendShapeTools.frdConnectControl() # connect 2 Control @ bsh
blendShapeTools.connectAutoLid() # connect 3 eyelid @ bsh
#-----------------connect at Rig-------------------
blendShapeTools.connectEyeRigToBshCtrl()  # connect eyeLid to Control @ Anim


#----------------------Create Control Dtl-----------------------------------------------------------
import pkmel.detailControl as dtlCtrl
reload( dtlCtrl )

dtlCtrl.controlOnSelectedEdge()
dtlCtrl.autoName(
                    charName='' ,
                    elem='eyebrow' ,
                    side='LFT'
                )
dtlCtrl.autoNameList(
                        charname='' ,
                        elem='' ,
                        side=''
                    )

#---------------------------------------------------------------------------------------------------


##############################################    Teacher Trick

###################################################################### dicts

dicts = {}

dicts['Wrinkle'] = 1.2
dicts['Eye'] = 2.0

tz_value = dicts['Eye']
dicts.keys()

import json
# write data
f_path = r"C:/Users/pornpavis.t/Desktop/xxxx.dat"
f = open(f_path,'w')
json.dump(dicts,f)
f.close()

with open(f_path,'w') as f:
    json.dump(dicts,f)

# read data
with open(f_path,'r') as f:
    value_dicts = json.load(f)

value_dicts['Eye']


def test():
    dict1 = {}
    dict1['Wrinkle']= 11.0
    dict1['Eye'] = 3.0
    return dict1


def test2(dict1={}):
    for k in dict1.keys():
        print dict1[k]
x = test()
test2(x)   


dicts.keys()
for part in ['Wrinkle','Eye','Cheek','Mouth']:
    if part in dicts.keys():
        print part , dicts[part]


###################################################################### copy folder
from rf_utils import admin

dir(admin)

admin.win_copy_directory(r"D:/gitLocal/core/rf_maya/rftool/rig/template/caTemplate/texture/main","C:/Users/pornpavis.t/Desktop/main")



from rf_maya.rftool.rig.rigScript import crvFunc
reload(crvFunc)

# add new shape Name
crvFunc.add_ctrl_to_dict('ShapeName','SelectedCrv')


# set Ctrl Shape
crvFunc.set_shape('selectedCrv','ShapeName')

from rf_maya.rftool.rig.rigScript import crvFunc
reload(crvFunc)
crvFunc.add_ctrl_to_dict('CAEyebrow','EyebrowUiRig_L_Ctrl')
crvFunc.set_shape('EyebrowUiRig_L_Ctrl','CAEyebrow')