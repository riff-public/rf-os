import maya.cmds as mc
reload(mc)
import getNameSpace as gns 
reload(gns)

def tailNonFlip(obj,elem, *args):
	# if obj =='':
	# 	obj = mc.ls(sl=True)
	# 	print obj
	nsLists = gns.getNs(nodeName=obj)
	tailIk = mc.ls('%sTailFk_*_Ctrl' % nsLists)
	# print tailIk
	ikhTail = ('%sLumjekTail_Ikh' % nsLists)
	tailIkCtrlObj = []
	for tailIkLst in tailIk:
	    tailIkCtrl = tailIkLst
	    if'Gmbl_' in tailIkCtrl:
	        pass
	    else:
	        tailIkCtrlObj.append(tailIkCtrl)
	print tailIkCtrlObj
	print len(tailIkCtrlObj)
	tailPma = mc.createNode('plusMinusAverage', n ='%s.Pma' % elem)
	x = 1
	# value = -1.1
	for i in range(len(tailIkCtrlObj)):
		tailLst = mc.addAttr(tailIkCtrlObj[i], ln= 'twist__________', at='double',dv = 0, k = True)
		mc.addAttr(tailIkCtrlObj[i], ln = 'Values',at='double',dv = 0,k=True)
		mc.setAttr('%s.Values' % tailIkCtrlObj[i], -1)
		tailMdv = mc.createNode('multiplyDivide', n = '%s%s.Mdv' % (elem, x+1))
		mc.setAttr ('%s.operation' % tailMdv, 2)
		tailNagMdv = mc.createNode('multiplyDivide', n = '%sNag%s.Mdv' % (elem , x+1))
		mc.connectAttr('%s.Values' % tailIkCtrlObj[i], '%s.input2X' % tailNagMdv)
		mc.connectAttr('%s.translateX' % tailIkCtrlObj[i], '%s.input1X' % tailMdv)
		mc.connectAttr('%s.translateX' % tailIkCtrlObj[i], '%s.input1X' % tailNagMdv)
		# mc.setAttr('%s.input2X' % tailNagMdv, -1)
		mc.connectAttr('%s.outputX' % tailNagMdv, '%s.input2X' % tailMdv)
		mc.connectAttr('%s.outputX' % tailMdv, '%s.input1D[%s]' % (tailPma, x))

		# value-=0.1
		x+=1

	mc.connectAttr('%s.output1D' % tailPma, '%s.twist' % ikhTail)