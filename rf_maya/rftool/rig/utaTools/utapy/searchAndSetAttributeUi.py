import pymel.core as pm
import maya.cmds as mc
import maya.mel as mm


def searchAndSetAttributeUi(*args):
	# check if window exists
	if pm.window ( 'DefaultAttrUi' , exists = True ) :
	    pm.deleteUI ( 'DefaultAttrUi' ) ;
	title = "DefaultAttr v1.0 (6-06-2019)" ;
	# create window 
	window = pm.window ( 'DefaultAttrUi', 
	                        title = title , 
	                        width = 100,
	                        mnb = True , 
	                        mxb = True , 
	                        sizeable = True , 
	                        rtf = True ) ;
	windowRowColumnLayout = pm.window ( 'DefaultAttrUi', 
	                        e = True , 
	                        h = 100,
	                        width = 225);

	# motionTab A-------------------------------------------------------------------------------------------------------------------------------    
	mainWindow = pm.tabLayout ( innerMarginWidth = 1 , innerMarginHeight = 1 , p = windowRowColumnLayout ) ;
	MotionTab_A = pm.rowColumnLayout ( "Search And Set Default Attribute", nc = 1 , p = mainWindow ) ;

	## Tab ----------------------------------------------------------------
	rowLine_TabA = pm.rowColumnLayout ( nc = 1 , p = MotionTab_A ) ;
	mc.tabLayout(p=rowLine_TabA, w= 225)

	# Control Tab ------------------------------------------------------
	# pm.rowColumnLayout (p = rowLine_TabA ,h=8) ;
	rowLine1 = pm.rowColumnLayout ( nc = 5 , p = rowLine_TabA  , cw = [(1, 10), (2, 60),(3, 10 ), (4, 145) ,(5, 10)]);


    ## Search
	pm.rowColumnLayout (p = rowLine1 ,h=33) ;
	pm.text( label='1. Search' , p = rowLine1, al = "left")  
	pm.rowColumnLayout (p = rowLine1 ,h=8) ;
	pm.checkBox( "checkSearch", label = "Control Name", p = rowLine1 ,  align='left', value= True)
	pm.rowColumnLayout (p = rowLine1 ) ;

	## Controller
	pm.rowColumnLayout (p = rowLine1 ,h=8) ;
	pm.text( label='2. Control' , p = rowLine1, al = "left")  
	pm.rowColumnLayout (p = rowLine1 ,h=8) ;
	rowLine0Sub = pm.rowColumnLayout ( nc = 3 , p = rowLine1 , h = 30 , cw = [(1, 120), (2, 25), (3, 25) ]) ;
	pm.textField( "txtFieldController", text = "*:Jaw*_Ctrl", p = rowLine0Sub , editable = True )
	mc.button("buttonController", l="<<" , p = rowLine0Sub , c = getControl)
	# mc.button("buttonSw", l="O/C" , p = rowLine0Sub , )
	pm.rowColumnLayout (p = rowLine1 ,h=8) ;

	## Attribute
	pm.rowColumnLayout (p = rowLine1 ,h=8) ;
	pm.text( label='3. Attribute' , p = rowLine1, al = "left")  
	pm.rowColumnLayout (p = rowLine1 ,h=8) ;
	rowLine0Sub = pm.rowColumnLayout ( nc = 3 , p = rowLine1 , h = 30 , cw = [(1, 120), (2, 25), (3, 25) ]) ;
	pm.textField( "txtFieldAttribute", text = "sx , sy , sz", p = rowLine0Sub , editable = True )
	mc.button("buttonAttribute", l="<<" , p = rowLine0Sub , c = getAttribute)
	pm.rowColumnLayout (p = rowLine1 ,h=8) ;	

	## Value
	pm.rowColumnLayout (p = rowLine1 ,h=8) ;
	pm.text( label='4. Value' , p = rowLine1, al = "left")  
	pm.rowColumnLayout (p = rowLine1 ,h=8) ;
	rowLine0Sub = pm.rowColumnLayout ( nc = 3 , p = rowLine1 , h = 30 , cw = [(1, 40), (2, 125), (3, 25) ]) ;
	pm.textField( "txtFieldValue", text = "1", p = rowLine0Sub , editable = True )
	mc.button("buttonValue", l="<<" , p = rowLine0Sub , c = getValue)
	pm.rowColumnLayout (p = rowLine1 ,h=8) ;	

	## Run
	rowLine2 = pm.rowColumnLayout ( nc = 3 , p = rowLine_TabA , h = 40 , cw = [(1, 10), (2, 215), (3, 10) ]) ;
	pm.rowColumnLayout (p = rowLine2 ,h=8) ;
	# pm.rowColumnLayout (p = rowLine1 ,h=8) ;
	pm.button ('buttonRun', label = " R U N" ,h=40, p = rowLine2 , c = run , bgc = (0.411765, 0.411765, 0.411765)) ;
	pm.rowColumnLayout (p = rowLine2 ,h=8) ;

	## Tab ----------------------------------------------------------------
	rowLine_TabA = pm.rowColumnLayout ( nc = 1 , p = MotionTab_A ) ;
	mc.tabLayout(p=rowLine_TabA, w= 240)


	pm.setParent("..")
	pm.showWindow(window)
	
def getControl(*args):
	strObj = ''
	sel = mc.ls(sl=True)
	for each in sel:
		if sel:
			if strObj == '':
				strObj = each.split('|')[0]
			else:
				strObj2 = each.split('|')[0]
				strObj = strObj + ' , ' + strObj2	    	
		pm.textField( "txtFieldController", e=True , text = '%s'% strObj )

def getAttribute(*args):

	strObj = ''
	sel = mc.ls(sl=True)
	for each in sel:
		if sel:
			if strObj == '':
				strObj = each.split('|')[0]
			else:
				strObj2 = each.split('|')[0]
				strObj = strObj + ' , ' + strObj2	    	
		pm.textField( "txtFieldAttribute", e=True , text = '%s'% strObj ) 

def getValue(*args):
	strObj = ''
	sel = mc.ls(sl=True)
	for each in sel:
		if sel:
			if strObj == '':
				strObj = each.split('|')[0]
			else:
				strObj2 = each.split('|')[0]
				strObj = strObj + ' , ' + strObj2	    	
		pm.textField( "txtFieldValue", e=True , text = '%s'% strObj )      
    	
def run(*args):
	from utaTools.utapy import utaCore
	reload(utaCore)

	searchValue = pm.checkBox( "checkSearch", q=True, value = True)
	txtFieldCtrl = pm.textField("txtFieldController", q=True, text=True) 
	txtFieldAttrs = pm.textField("txtFieldAttribute", q=True, text=True)
	txtFieldValue = pm.textField( "txtFieldValue", q=True, text=True)

	## Split txtField
	attrsLists = txtFieldAttrs.split(',')
	values = int(txtFieldValue)

	utaCore.defaultAttr(search = searchValue, lists = [txtFieldCtrl], attr = attrsLists, value = values)