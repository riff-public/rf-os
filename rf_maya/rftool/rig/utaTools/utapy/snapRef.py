import maya.cmds as mc
reload(mc)
from utaTools.utapy import getNameSpace as gns 
reload(gns)
import importExportAnim as iea
reload(iea)

def snapRef():
    paths = 'P:\NaKee\RD\Anim'
    sels = mc.ls(sl = True)
    nsLists1 = gns.getNs(nodeName=sels[0])
    nsLists2 = gns.getNs(nodeName=sels[1])
    selA = mc.ls('%sTailFk_*_Ctrl' % nsLists1)
    selB = mc.ls('%sTailFk*Pos_Ctrl' % nsLists2)
    selC = mc.ls('%sTailFk_*_Ctrl' % nsLists2)

    mc.setAttr('%sOnOffSwitch1_Ctrl.FkPosition' % nsLists2,19)
    mc.setAttr('%sOnOffSwitch1_Ctrl.autoStretch' % nsLists2,4)
    
    for i in range(19):
        mc.parentConstraint(selA[i],selB[i],mo=False)
        
    mc.parentConstraint('%sHead_Ctrl' % nsLists1,'%sHead_Ctrl' % nsLists2,mo=False)
    mc.parentConstraint('%sMove_Ctrl' % nsLists1,'%sMove_Ctrl' % nsLists2,mo=False)
    
    for a in range(19,25):
        mc.parentConstraint(selA[a],selC[a],mo=False)
        
    lstIEAEx = ['group3','HeadCtrl_Grp','TongueCtrl_Grp','NeckCtrlNeck_Grp','HeadUp_1_Ctrl','JawA_Ctrl','JawB_Ctrl','TailIk2_Ctrl','TailIk3_Ctrl','TailIk4_Ctrl','TailIk5_Ctrl']
    lstIEAIn = ['group3','HeadCtrl_Grp','TongueCtrl_Grp','NeckCtrlNeck_Grp','LumjekTail_Ikh','HeadUp_1_Ctrl','JawA_Ctrl','JawB_Ctrl','TailIk2_Ctrl','TailIk3_Ctrl','TailIk4_Ctrl','TailIk5_Ctrl']
    noLstIEAEx = len(lstIEAEx)
    noLstIEAIn = len(lstIEAIn)

    for b in range(noLstIEAEx):
        iea.export_anim(node = ('%s%s' % (nsLists1,lstIEAEx[b])),path =  ('%s\%s.anim' % (paths,lstIEAEx[b])))
    for c in range(noLstIEAIn):
        iea.import_anim(node = ('%s%s' % (nsLists2,lstIEAIn[c])),path =  ('%s\%s.anim' % (paths,lstIEAIn[c])))

    print'^____DONE_____^'

# snapRef()