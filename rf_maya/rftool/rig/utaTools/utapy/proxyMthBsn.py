import maya.cmds as mc
    #---------------------------------------------#
    #               MouthBsn R L Ctrl             #
    #---------------------------------------------#
def proxyMthBsn(tyOffset= '', paths = '' , geoBsn= ''):    

    ctrlBsns = mc.ls('*:mouthBsh_*_ctrl')

  
    for ctrlBsn in ctrlBsns:
        setCkUprIo = mc.setAttr('%s.cheekUprIO' %(ctrlBsn), 10)
    dupPfUpUp = mc.duplicate(geoBsn, n = 'puffUpperUp%s' % paths)
    
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.cheekUprIO' %(ctrlBsn), -10)
    dupPfUpDn = mc.duplicate(geoBsn, n = 'puffUpperDn%s' % paths)
    
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.cheekUprIO' %(ctrlBsn), 0)
    #----------------------------------------------------#
        
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.cheekLwrIO' %(ctrlBsn), 10)
    dupCnrInUp = mc.duplicate(geoBsn, n = 'puffLowerOut%s' % paths)
    
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.cheekLwrIO' %(ctrlBsn), -10)
    dupCnrInUp = mc.duplicate(geoBsn, n = 'puffLowerIn%s' % paths)
    
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.cheekLwrIO' %(ctrlBsn), 0)
    #----------------------------------------------------#
    
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.cornerUD' %(ctrlBsn), 10)
    dupCnrInUp = mc.duplicate(geoBsn, n = 'cornerUp%s' % paths)
    
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.cornerUD' %(ctrlBsn), -10)
    dupCnrInUp = mc.duplicate(geoBsn, n = 'cornerDn%s' % paths)
    
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.cornerUD' %(ctrlBsn), 0)
    #----------------------------------------------------#
    
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.cornerIO' %(ctrlBsn), 10)
    dupCnrInUp = mc.duplicate(geoBsn, n = 'cornerIn%s' % paths)
    
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.cornerIO' %(ctrlBsn), -10)
    dupCnrInUp = mc.duplicate(geoBsn, n = 'cornerOut%s' % paths)
    
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.cornerIO' %(ctrlBsn), 0)
    #----------------------------------------------------#
    
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.cornerInUD' %(ctrlBsn), 10)
    dupCnrInUp = mc.duplicate(geoBsn, n = 'cornerInnerUp%s' % paths)
    
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.cornerInUD' %(ctrlBsn), -10)
    dupCnrInUp = mc.duplicate(geoBsn, n = 'cornerInnerDn%s' % paths)
    
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.cornerInUD' %(ctrlBsn), 0)
    #----------------------------------------------------#
    
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.cornerOutUD' %(ctrlBsn), 10)
    dupCnrInUp = mc.duplicate(geoBsn, n = 'cornerOuterUp%s' % paths)
    
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.cornerOutUD' %(ctrlBsn), -10)
    dupCnrInUp = mc.duplicate(geoBsn, n = 'cornerOuterDn%s' % paths)
    
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.cornerOutUD' %(ctrlBsn), 0)
    #---------------------------------------------#
    
    #---------------------------------------------#
    #               MouthBsn Ctrl                 #
    #---------------------------------------------#
    
    #---------------------------------------------#
    ctrlMthBsn = mc.ls('*:mouthBsh_ctrl')
    
    for ctrlMBsn in ctrlMthBsn:
        mc.setAttr('%s.upLipsCurlIO' %(ctrlMBsn), 10)
    dupCnrInUp = mc.duplicate(geoBsn, n = 'upLipsCurlIn%s' % paths)
    
    for ctrlMBsn in ctrlMthBsn:
        mc.setAttr('%s.upLipsCurlIO' %(ctrlMBsn), -10)
    dupCnrInUp = mc.duplicate(geoBsn, n = 'upLipsCurlOut%s' % paths)
    
    for ctrlMBsn in ctrlMthBsn:
        mc.setAttr('%s.upLipsCurlIO' %(ctrlMBsn), 0)
    #----------------------------------------------------#
    
    for ctrlMBsn in ctrlMthBsn:
        mc.setAttr('%s.loLipsCurlIO' %(ctrlMBsn), 10)
    dupCnrInUp = mc.duplicate(geoBsn, n = 'loLipsCurlIn%s' % paths)
    
    for ctrlMBsn in ctrlMthBsn:
        mc.setAttr('%s.loLipsCurlIO' %(ctrlMBsn), -10)
    dupCnrInUp = mc.duplicate(geoBsn, n = 'loLipsCurlOut%s' % paths)
    
    for ctrlMBsn in ctrlMthBsn:
        mc.setAttr('%s.loLipsCurlIO' %(ctrlMBsn), 0)
    #----------------------------------------------------#
    
    for ctrlMBsn in ctrlMthBsn:
        mc.setAttr('%s.mouthU' %(ctrlMBsn), 10)
    dupCnrInUp = mc.duplicate(geoBsn, n = 'mouthU%s' % paths)
    
    for ctrlMBsn in ctrlMthBsn:
        mc.setAttr('%s.mouthU' %(ctrlMBsn), 0)
    #---------------------------------------------#
    
    #---------------------------------------------#
    #               object ot grp                 #
    #---------------------------------------------#
    
    #---------------------------------------------#
    
    prxGrp = mc.createNode('transform',n = 'proxy%sBsh_Grps' % paths)
    
    mc.parent('upLipsCurlIn%s' % paths,'upLipsCurlOut%s' % paths,'loLipsCurlIn%s' % paths,'loLipsCurlOut%s' % paths,'mouthU%s' % paths,'puffUpperUp%s' % paths,'puffUpperDn%s' % paths,'puffLowerOut%s' % paths,'puffLowerIn%s' % paths,'cornerUp%s' % paths,'cornerDn%s' % paths,'cornerIn%s' % paths,'cornerOut%s' % paths,'cornerInnerUp%s' % paths,'cornerInnerDn%s' % paths,'cornerOuterUp%s' % paths,'cornerOuterDn%s' % paths,prxGrp)
    mc.setAttr('%s.tx' %prxGrp, -200)
    
    prxBsnMd = ['upLipsCurlIn%s' % paths,'upLipsCurlOut%s' % paths,'loLipsCurlIn%s' % paths,'loLipsCurlOut%s' % paths,'mouthU%s' % paths,'puffUpperUp%s' % paths,'puffUpperDn%s' % paths,'puffLowerOut%s' % paths,'puffLowerIn%s' % paths,'cornerUp%s' % paths,'cornerDn%s' % paths,'cornerIn%s' % paths,'cornerOut%s' % paths,'cornerInnerUp%s' % paths,'cornerInnerDn%s' % paths,'cornerOuterUp%s' % paths,'cornerOuterDn%s' % paths]
   
    numHead = len(prxBsnMd)
    defaultTy = 0
    defaultTy2 = 0
    for i in range(numHead):
        if i < 5:
            mc.setAttr((prxBsnMd[i]+'.ty'),defaultTy)
            defaultTy += tyOffset
            mc.setAttr((prxBsnMd[i]+'.tx'),40)    
        elif i >= 5:
            mc.setAttr((prxBsnMd[i]+'.ty'),defaultTy2)
            defaultTy2 += tyOffset



    #---------------------------------------------#
    #               MouthBsn Obj01 Ctrl             #
    #---------------------------------------------#
def proxyMthObj01Bsn(tyOffset= '', geoObj01Bsn= ''):    

    # # listsGeoBsn = len(geoBsn)
    # sels = mc.ls(sl=True, fl=True)
    # selsLists = len(sels)
    # # print selsLists
    # if selsLists == 0:
    #     print 'Please Select "BodyBshRig_Geo"'
    # else: 
    #     geoBsn = sels
    ctrlBsns = mc.ls('*:mouthBsh_*_ctrl')
    #     grpBsns = ('*:facailRig_grp')
    #     # print geoBsn

  
    for ctrlBsn in ctrlBsns:
        setCkUprIo = mc.setAttr('%s.cheekUprIO' %(ctrlBsn), 10)
    # mc.duplicate(geoBsn, n = 'puffUpperUp')
    mc.duplicate(geoObj01Bsn, n = 'puffUpperUpObj01')
    # mc.duplicate(geoObj02Bsn, n = 'puffUpperUpObj02') 
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.cheekUprIO' %(ctrlBsn), -10)
    # mc.duplicate(geoBsn, n = 'puffUpperDn')
    mc.duplicate(geoObj01Bsn, n = 'puffUpperDnObj01')    
    # mc.duplicate(geoObj02Bsn, n = 'puffUpperDnObj02')  
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.cheekUprIO' %(ctrlBsn), 0)
    #----------------------------------------------------#
        
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.cheekLwrIO' %(ctrlBsn), 10)
    # mc.duplicate(geoBsn, n = 'puffLowerOut')
    mc.duplicate(geoObj01Bsn, n = 'puffLowerOutObj01')   
    # mc.duplicate(geoObj02Bsn, n = 'puffLowerOutObj02')  
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.cheekLwrIO' %(ctrlBsn), -10)
    # mc.duplicate(geoBsn, n = 'puffLowerIn')
    mc.duplicate(geoObj01Bsn, n = 'puffLowerInObj01')    
    # mc.duplicate(geoObj02Bsn, n = 'puffLowerInObj02')    
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.cheekLwrIO' %(ctrlBsn), 0)
    #----------------------------------------------------#
    
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.cornerUD' %(ctrlBsn), 10)
    # mc.duplicate(geoBsn, n = 'cornerUp')
    mc.duplicate(geoObj01Bsn, n = 'cornerUpObj01')    
    # mc.duplicate(geoObj02Bsn, n = 'cornerUpObj02')    
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.cornerUD' %(ctrlBsn), -10)
    # mc.duplicate(geoBsn, n = 'cornerDn')
    mc.duplicate(geoObj01Bsn, n = 'cornerDnObj01')    
    # mc.duplicate(geoObj02Bsn, n = 'cornerDnObj02')   
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.cornerUD' %(ctrlBsn), 0)
    #----------------------------------------------------#
    
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.cornerIO' %(ctrlBsn), 10)
    # mc.duplicate(geoBsn, n = 'cornerIn')
    mc.duplicate(geoObj01Bsn, n = 'cornerInObj01') 
    # mc.duplicate(geoObj02Bsn, n = 'cornerInObj02') 
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.cornerIO' %(ctrlBsn), -10)
    # mc.duplicate(geoBsn, n = 'cornerOut')
    mc.duplicate(geoObj01Bsn, n = 'cornerOutObj01')    
    # mc.duplicate(geoObj02Bsn, n = 'cornerOutObj02')    
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.cornerIO' %(ctrlBsn), 0)
    #----------------------------------------------------#
    
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.cornerInUD' %(ctrlBsn), 10)
    # mc.duplicate(geoBsn, n = 'cornerInnerUp')
    mc.duplicate(geoObj01Bsn, n = 'cornerInnerUpObj01')    
    # mc.duplicate(geoObj02Bsn, n = 'cornerInnerUpObj02')  
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.cornerInUD' %(ctrlBsn), -10)
    # mc.duplicate(geoBsn, n = 'cornerInnerDn')
    mc.duplicate(geoObj01Bsn, n = 'cornerInnerDnObj01')    
    # mc.duplicate(geoObj02Bsn, n = 'cornerInnerDnObj02') 
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.cornerInUD' %(ctrlBsn), 0)
    #----------------------------------------------------#
    
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.cornerOutUD' %(ctrlBsn), 10)
    # mc.duplicate(geoBsn, n = 'cornerOuterUp')
    mc.duplicate(geoObj01Bsn, n = 'cornerOuterUpObj01')   
    # mc.duplicate(geoObj02Bsn, n = 'cornerOuterUpObj02')   
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.cornerOutUD' %(ctrlBsn), -10)
    # mc.duplicate(geoBsn, n = 'cornerOuterDn')
    mc.duplicate(geoObj01Bsn, n = 'cornerOuterDnObj01')    
    # mc.duplicate(geoObj02Bsn, n = 'cornerOuterDnObj02')  
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.cornerOutUD' %(ctrlBsn), 0)
    #---------------------------------------------#
    
    #---------------------------------------------#
    #               MouthBsn Ctrl                 #
    #---------------------------------------------#
    
    #---------------------------------------------#
    ctrlMthBsn = mc.ls('*:mouthBsh_ctrl')
    
    for ctrlMBsn in ctrlMthBsn:
        mc.setAttr('%s.upLipsCurlIO' %(ctrlMBsn), 10)
    # mc.duplicate(geoBsn, n = 'upLipsCurlIn')
    mc.duplicate(geoObj01Bsn, n = 'upLipsCurlInObj01')  
    # mc.duplicate(geoObj02Bsn, n = 'upLipsCurlInObj02')  
    for ctrlMBsn in ctrlMthBsn:
        mc.setAttr('%s.upLipsCurlIO' %(ctrlMBsn), -10)
    # mc.duplicate(geoBsn, n = 'upLipsCurlOut')
    mc.duplicate(geoObj01Bsn, n = 'upLipsCurlOutObj01')    
    # mc.duplicate(geoObj02Bsn, n = 'upLipsCurlOutObj02')  
    for ctrlMBsn in ctrlMthBsn:
        mc.setAttr('%s.upLipsCurlIO' %(ctrlMBsn), 0)
    #----------------------------------------------------#
    
    for ctrlMBsn in ctrlMthBsn:
        mc.setAttr('%s.loLipsCurlIO' %(ctrlMBsn), 10)
    # mc.duplicate(geoBsn, n = 'loLipsCurlIn')
    mc.duplicate(geoObj01Bsn, n = 'loLipsCurlInObj01')
    # mc.duplicate(geoObj02Bsn, n = 'loLipsCurlInObj02')
    for ctrlMBsn in ctrlMthBsn:
        mc.setAttr('%s.loLipsCurlIO' %(ctrlMBsn), -10)
    # mc.duplicate(geoBsn, n = 'loLipsCurlOut')
    mc.duplicate(geoObj01Bsn, n = 'loLipsCurlOutObj01') 
    # mc.duplicate(geoObj02Bsn, n = 'loLipsCurlOutObj02') 
    for ctrlMBsn in ctrlMthBsn:
        mc.setAttr('%s.loLipsCurlIO' %(ctrlMBsn), 0)
    #----------------------------------------------------#
    
    for ctrlMBsn in ctrlMthBsn:
        mc.setAttr('%s.mouthU' %(ctrlMBsn), 10)
    # mc.duplicate(geoBsn, n = 'mouthU')
    mc.duplicate(geoObj01Bsn, n = 'mouthUObj01') 
    # mc.duplicate(geoObj02Bsn, n = 'mouthUObj02') 
    for ctrlMBsn in ctrlMthBsn:
        mc.setAttr('%s.mouthU' %(ctrlMBsn), 0)
    #---------------------------------------------#
    
    #---------------------------------------------#
    #               object ot grp                 #
    #---------------------------------------------#
    
    #---------------------------------------------#
    
    # prxGrp = mc.createNode('transform',n = 'proxyBsh_Grps')
    prxObj01Grp = mc.createNode('transform',n = 'proxyObj01Bsh_Grps')  
    # prxObj02Grp = mc.createNode('transform',n = 'proxyObj02Bsh_Grps')  
    # mc.parent('upLipsCurlIn','upLipsCurlOut','loLipsCurlIn','loLipsCurlOut','mouthU','puffUpperUp','puffUpperDn','puffLowerOut','puffLowerIn','cornerUp','cornerDn','cornerIn','cornerOut','cornerInnerUp','cornerInnerDn','cornerOuterUp','cornerOuterDn',prxGrp)
    mc.parent('upLipsCurlInObj01','upLipsCurlOutObj01','loLipsCurlInObj01','loLipsCurlOutObj01','mouthUObj01','puffUpperUpObj01','puffUpperDnObj01','puffLowerOutObj01','puffLowerInObj01','cornerUpObj01','cornerDnObj01','cornerInObj01','cornerOutObj01','cornerInnerUpObj01','cornerInnerDnObj01','cornerOuterUpObj01','cornerOuterDnObj01',prxObj01Grp)
    # mc.parent('upLipsCurlInObj02','upLipsCurlOutObj02','loLipsCurlInObj02','loLipsCurlOutObj02','mouthUObj02','puffUpperUpObj02','puffUpperDnObj02','puffLowerOutObj02','puffLowerInObj02','cornerUpObj02','cornerDnObj02','cornerInObj02','cornerOutObj02','cornerInnerUpObj02','cornerInnerDnObj02','cornerOuterUpObj02','cornerOuterDnObj02',prxObj02Grp)

    # mc.setAttr('%s.tx' %prxGrp, -200)
    mc.setAttr('%s.tx' %prxObj01Grp, -200)
    # mc.setAttr('%s.tx' %prxObj02Grp, -200)

    # prxBsnMd = ['upLipsCurlIn','upLipsCurlOut','loLipsCurlIn','loLipsCurlOut','mouthU','puffUpperUp','puffUpperDn','puffLowerOut','puffLowerIn','cornerUp','cornerDn','cornerIn','cornerOut','cornerInnerUp','cornerInnerDn','cornerOuterUp','cornerOuterDn']
    prxBshObj01 = ['upLipsCurlInObj01','upLipsCurlOutObj01','loLipsCurlInObj01','loLipsCurlOutObj01','mouthUObj01','puffUpperUpObj01','puffUpperDnObj01','puffLowerOutObj01','puffLowerInObj01','cornerUpObj01','cornerDnObj01','cornerInObj01','cornerOutObj01','cornerInnerUpObj01','cornerInnerDnObj01','cornerOuterUpObj01','cornerOuterDnObj01']
    # prxBshObj02 = ['upLipsCurlInObj02','upLipsCurlOutObj02','loLipsCurlInObj02','loLipsCurlOutObj02','mouthUObj02','puffUpperUpObj02','puffUpperDnObj02','puffLowerOutObj02','puffLowerInObj02','cornerUpObj02','cornerDnObj02','cornerInObj02','cornerOutObj02','cornerInnerUpObj02','cornerInnerDnObj02','cornerOuterUpObj02','cornerOuterDnObj02']
   
    # numHead = len(prxBsnMd)
    numobj01 = len(prxBshObj01)
    # numobj02 = len(prxBshObj02)

    defaultTy = 0
    defaultTy2 = 0
    for i in range(numobj01):
        if i < 5:
            # mc.setAttr((prxBsnMd[i]+'.ty'),defaultTy)
            mc.setAttr((prxBshObj01[i]+'.ty'),defaultTy)
            # mc.setAttr((prxBshObj02[i]+'.ty'),defaultTy)
            defaultTy += tyOffset

            # mc.setAttr((prxBsnMd[i]+'.tx'),40)    
            mc.setAttr((prxBshObj01[i]+'.tx'),40)  
            # mc.setAttr((prxBshObj02[i]+'.tx'),40) 
        elif i >= 5:
            # mc.setAttr((prxBsnMd[i]+'.ty'),defaultTy2)
            mc.setAttr((prxBshObj01[i]+'.ty'),defaultTy2)
            # mc.setAttr((prxBshObj02[i]+'.ty'),defaultTy2)
            defaultTy2 += tyOffset

    #---------------------------------------------#
    #               MouthBsn Obj02 Ctrl             #
    #---------------------------------------------#
def proxyMthObj02Bsn(tyOffset= '', geoObj02Bsn= ''):    

    # # listsGeoBsn = len(geoBsn)
    # sels = mc.ls(sl=True, fl=True)
    # selsLists = len(sels)
    # # print selsLists
    # if selsLists == 0:
    #     print 'Please Select "BodyBshRig_Geo"'
    # else: 
    #     geoBsn = sels
    ctrlBsns = mc.ls('*:mouthBsh_*_ctrl')
    #     grpBsns = ('*:facailRig_grp')
    #     # print geoBsn

  
    for ctrlBsn in ctrlBsns:
        setCkUprIo = mc.setAttr('%s.cheekUprIO' %(ctrlBsn), 10)
    # mc.duplicate(geoBsn, n = 'puffUpperUp')
    mc.duplicate(geoObj02Bsn, n = 'puffUpperUpObj02')
    # mc.duplicate(geoObj02Bsn, n = 'puffUpperUpObj02') 
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.cheekUprIO' %(ctrlBsn), -10)
    # mc.duplicate(geoBsn, n = 'puffUpperDn')
    mc.duplicate(geoObj02Bsn, n = 'puffUpperDnObj02')    
    # mc.duplicate(geoObj02Bsn, n = 'puffUpperDnObj02')  
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.cheekUprIO' %(ctrlBsn), 0)
    #----------------------------------------------------#
        
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.cheekLwrIO' %(ctrlBsn), 10)
    # mc.duplicate(geoBsn, n = 'puffLowerOut')
    mc.duplicate(geoObj02Bsn, n = 'puffLowerOutObj02')   
    # mc.duplicate(geoObj02Bsn, n = 'puffLowerOutObj02')  
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.cheekLwrIO' %(ctrlBsn), -10)
    # mc.duplicate(geoBsn, n = 'puffLowerIn')
    mc.duplicate(geoObj02Bsn, n = 'puffLowerInObj02')    
    # mc.duplicate(geoObj02Bsn, n = 'puffLowerInObj02')    
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.cheekLwrIO' %(ctrlBsn), 0)
    #----------------------------------------------------#
    
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.cornerUD' %(ctrlBsn), 10)
    # mc.duplicate(geoBsn, n = 'cornerUp')
    mc.duplicate(geoObj02Bsn, n = 'cornerUpObj02')    
    # mc.duplicate(geoObj02Bsn, n = 'cornerUpObj02')    
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.cornerUD' %(ctrlBsn), -10)
    # mc.duplicate(geoBsn, n = 'cornerDn')
    mc.duplicate(geoObj02Bsn, n = 'cornerDnObj02')    
    # mc.duplicate(geoObj02Bsn, n = 'cornerDnObj02')   
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.cornerUD' %(ctrlBsn), 0)
    #----------------------------------------------------#
    
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.cornerIO' %(ctrlBsn), 10)
    # mc.duplicate(geoBsn, n = 'cornerIn')
    mc.duplicate(geoObj02Bsn, n = 'cornerInObj02') 
    # mc.duplicate(geoObj02Bsn, n = 'cornerInObj02') 
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.cornerIO' %(ctrlBsn), -10)
    # mc.duplicate(geoBsn, n = 'cornerOut')
    mc.duplicate(geoObj02Bsn, n = 'cornerOutObj02')    
    # mc.duplicate(geoObj02Bsn, n = 'cornerOutObj02')    
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.cornerIO' %(ctrlBsn), 0)
    #----------------------------------------------------#
    
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.cornerInUD' %(ctrlBsn), 10)
    # mc.duplicate(geoBsn, n = 'cornerInnerUp')
    mc.duplicate(geoObj02Bsn, n = 'cornerInnerUpObj02')    
    # mc.duplicate(geoObj02Bsn, n = 'cornerInnerUpObj02')  
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.cornerInUD' %(ctrlBsn), -10)
    # mc.duplicate(geoBsn, n = 'cornerInnerDn')
    mc.duplicate(geoObj02Bsn, n = 'cornerInnerDnObj02')    
    # mc.duplicate(geoObj02Bsn, n = 'cornerInnerDnObj02') 
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.cornerInUD' %(ctrlBsn), 0)
    #----------------------------------------------------#
    
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.cornerOutUD' %(ctrlBsn), 10)
    # mc.duplicate(geoBsn, n = 'cornerOuterUp')
    mc.duplicate(geoObj02Bsn, n = 'cornerOuterUpObj02')   
    # mc.duplicate(geoObj02Bsn, n = 'cornerOuterUpObj02')   
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.cornerOutUD' %(ctrlBsn), -10)
    # mc.duplicate(geoBsn, n = 'cornerOuterDn')
    mc.duplicate(geoObj02Bsn, n = 'cornerOuterDnObj02')    
    # mc.duplicate(geoObj02Bsn, n = 'cornerOuterDnObj02')  
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.cornerOutUD' %(ctrlBsn), 0)
    #---------------------------------------------#
    
    #---------------------------------------------#
    #               MouthBsn Ctrl                 #
    #---------------------------------------------#
    
    #---------------------------------------------#
    ctrlMthBsn = mc.ls('*:mouthBsh_ctrl')
    
    for ctrlMBsn in ctrlMthBsn:
        mc.setAttr('%s.upLipsCurlIO' %(ctrlMBsn), 10)
    # mc.duplicate(geoBsn, n = 'upLipsCurlIn')
    mc.duplicate(geoObj02Bsn, n = 'upLipsCurlInObj02')  
    # mc.duplicate(geoObj02Bsn, n = 'upLipsCurlInObj02')  
    for ctrlMBsn in ctrlMthBsn:
        mc.setAttr('%s.upLipsCurlIO' %(ctrlMBsn), -10)
    # mc.duplicate(geoBsn, n = 'upLipsCurlOut')
    mc.duplicate(geoObj02Bsn, n = 'upLipsCurlOutObj02')    
    # mc.duplicate(geoObj02Bsn, n = 'upLipsCurlOutObj02')  
    for ctrlMBsn in ctrlMthBsn:
        mc.setAttr('%s.upLipsCurlIO' %(ctrlMBsn), 0)
    #----------------------------------------------------#
    
    for ctrlMBsn in ctrlMthBsn:
        mc.setAttr('%s.loLipsCurlIO' %(ctrlMBsn), 10)
    # mc.duplicate(geoBsn, n = 'loLipsCurlIn')
    mc.duplicate(geoObj02Bsn, n = 'loLipsCurlInObj02')
    # mc.duplicate(geoObj02Bsn, n = 'loLipsCurlInObj02')
    for ctrlMBsn in ctrlMthBsn:
        mc.setAttr('%s.loLipsCurlIO' %(ctrlMBsn), -10)
    # mc.duplicate(geoBsn, n = 'loLipsCurlOut')
    mc.duplicate(geoObj02Bsn, n = 'loLipsCurlOutObj02') 
    # mc.duplicate(geoObj02Bsn, n = 'loLipsCurlOutObj02') 
    for ctrlMBsn in ctrlMthBsn:
        mc.setAttr('%s.loLipsCurlIO' %(ctrlMBsn), 0)
    #----------------------------------------------------#
    
    for ctrlMBsn in ctrlMthBsn:
        mc.setAttr('%s.mouthU' %(ctrlMBsn), 10)
    # mc.duplicate(geoBsn, n = 'mouthU')
    mc.duplicate(geoObj02Bsn, n = 'mouthUObj02') 
    # mc.duplicate(geoObj02Bsn, n = 'mouthUObj02') 
    for ctrlMBsn in ctrlMthBsn:
        mc.setAttr('%s.mouthU' %(ctrlMBsn), 0)
    #---------------------------------------------#
    
    #---------------------------------------------#
    #               object ot grp                 #
    #---------------------------------------------#
    
    #---------------------------------------------#
    
    # prxGrp = mc.createNode('transform',n = 'proxyBsh_Grps')
    prxObj02Grp = mc.createNode('transform',n = 'proxyObj02Bsh_Grps')  
    # prxObj02Grp = mc.createNode('transform',n = 'proxyObj02Bsh_Grps')  
    # mc.parent('upLipsCurlIn','upLipsCurlOut','loLipsCurlIn','loLipsCurlOut','mouthU','puffUpperUp','puffUpperDn','puffLowerOut','puffLowerIn','cornerUp','cornerDn','cornerIn','cornerOut','cornerInnerUp','cornerInnerDn','cornerOuterUp','cornerOuterDn',prxGrp)
    mc.parent('upLipsCurlInObj02','upLipsCurlOutObj02','loLipsCurlInObj02','loLipsCurlOutObj02','mouthUObj02','puffUpperUpObj02','puffUpperDnObj02','puffLowerOutObj02','puffLowerInObj02','cornerUpObj02','cornerDnObj02','cornerInObj02','cornerOutObj02','cornerInnerUpObj02','cornerInnerDnObj02','cornerOuterUpObj02','cornerOuterDnObj02',prxObj02Grp)
    # mc.parent('upLipsCurlInObj02','upLipsCurlOutObj02','loLipsCurlInObj02','loLipsCurlOutObj02','mouthUObj02','puffUpperUpObj02','puffUpperDnObj02','puffLowerOutObj02','puffLowerInObj02','cornerUpObj02','cornerDnObj02','cornerInObj02','cornerOutObj02','cornerInnerUpObj02','cornerInnerDnObj02','cornerOuterUpObj02','cornerOuterDnObj02',prxObj02Grp)

    # mc.setAttr('%s.tx' %prxGrp, -200)
    mc.setAttr('%s.tx' %prxObj02Grp, -200)
    # mc.setAttr('%s.tx' %prxObj02Grp, -200)

    # prxBsnMd = ['upLipsCurlIn','upLipsCurlOut','loLipsCurlIn','loLipsCurlOut','mouthU','puffUpperUp','puffUpperDn','puffLowerOut','puffLowerIn','cornerUp','cornerDn','cornerIn','cornerOut','cornerInnerUp','cornerInnerDn','cornerOuterUp','cornerOuterDn']
    prxBshObj02 = ['upLipsCurlInObj02','upLipsCurlOutObj02','loLipsCurlInObj02','loLipsCurlOutObj02','mouthUObj02','puffUpperUpObj02','puffUpperDnObj02','puffLowerOutObj02','puffLowerInObj02','cornerUpObj02','cornerDnObj02','cornerInObj02','cornerOutObj02','cornerInnerUpObj02','cornerInnerDnObj02','cornerOuterUpObj02','cornerOuterDnObj02']
    # prxBshObj02 = ['upLipsCurlInObj02','upLipsCurlOutObj02','loLipsCurlInObj02','loLipsCurlOutObj02','mouthUObj02','puffUpperUpObj02','puffUpperDnObj02','puffLowerOutObj02','puffLowerInObj02','cornerUpObj02','cornerDnObj02','cornerInObj02','cornerOutObj02','cornerInnerUpObj02','cornerInnerDnObj02','cornerOuterUpObj02','cornerOuterDnObj02']
   
    # numHead = len(prxBsnMd)
    numObj02 = len(prxBshObj02)
    # numobj02 = len(prxBshObj02)

    defaultTy = 0
    defaultTy2 = 0
    for i in range(numObj02):
        if i < 5:
            # mc.setAttr((prxBsnMd[i]+'.ty'),defaultTy)
            mc.setAttr((prxBshObj02[i]+'.ty'),defaultTy)
            # mc.setAttr((prxBshObj02[i]+'.ty'),defaultTy)
            defaultTy += tyOffset

            # mc.setAttr((prxBsnMd[i]+'.tx'),40)    
            mc.setAttr((prxBshObj02[i]+'.tx'),40)  
            # mc.setAttr((prxBshObj02[i]+'.tx'),40) 
        elif i >= 5:
            # mc.setAttr((prxBsnMd[i]+'.ty'),defaultTy2)
            mc.setAttr((prxBshObj02[i]+'.ty'),defaultTy2)
            # mc.setAttr((prxBshObj02[i]+'.ty'),defaultTy2)
            defaultTy2 += tyOffset

# proxyMthBsn(20)