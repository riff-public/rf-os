import sys, os
import re
import maya.cmds as mc
import maya.mel as mm 
from utaTools.pkmel import core as pc
reload(pc)
import shutil 
import pymel.core as pm
import math

from lpRig import mainGroup
reload(mainGroup)
from utaTools.utapy import DeleteGrp
from utaTools.pkmel import rigTools
reload(rigTools)
from utaTools.utapy import listControlVis 
reload(listControlVis)
from utaTools.utapy import listControlAmpFcl as lcaf
reload(lcaf)
from lpRig import core as lrc
reload(lrc)
from utaTools.pkmel import weightTools
reload(weightTools)
from lpRig import rigTools as lrr
reload(lrr)
from nuTools.rigTools import bpmRig as bpmRig
reload(bpmRig)
from ncmel import mainRig
reload(mainRig)
from ncmel import subRig
reload(subRig)
import lpRig.core as lrc
reload(lrc)
from rf_utils.context import context_info
reload(context_info)
import math 

from ncscript2.pymel import bshTools
reload(bshTools)
import pymel.core as pmc
from rftool.rig.rigScript import core 
reload(core)
cn = core.Node()


# -*- coding: utf-8 -*-
import unicodedata
########## setWinDowIcon ############################
from maya import OpenMayaUI as omui
# Special cases for different Maya versions
try:
    from shiboken2 import wrapInstance
except ImportError:
    from shiboken import wrapInstance
try:
    from PySide2.QtGui import QIcon
    from PySide2.QtWidgets import QWidget
except ImportError:
    from PySide.QtGui import QIcon, QWidget
######################################################
from utaTools.utapy import listBaseBsh
reload(listBaseBsh)
from utaTools.utapy import listsAsset
reload(listsAsset)


## Check Project Name
def checkProject(*args):
    from rf_utils.context import context_info
    reload(context_info)

    asset = context_info.ContextPathInfo()
    projectName = asset.project 
    return projectName

## Check Character Name
def checkCharacterName(*args):
    from rf_utils.context import context_info
    reload(context_info)
    asset = context_info.ContextPathInfo() 
    charName = asset.name
    return charName
#snap obj to obj
def snap(*args):
    sels =mc.ls(sl=True)
    first = sels[0]
    sels.remove(first)
    for each in sels:
        mean =mc.parentConstraint(first,each,mo=False)
        means=mc.delete (mean)

def snapObj(objA = '', objB = '', *args):
    if objA :
        first = objA
        second = objB
    else:
        sels =mc.ls(sl=True)
        first = sels[0]
        second = sels[-1]

    mc.delete(mc.parentConstraint(first,second,mo=False))

def snapSel(obj1 = '', obj2 = '', mode = 'parent',*args):
    if mode == 'parent' :
        mean =mc.parentConstraint(obj1,obj2,mo=False)
        means=mc.delete (mean)  
    if mode == 'point' :
        mean =mc.pointConstraint(obj1,obj2,mo=False)
        means=mc.delete (mean)        



# def jointFromLocator (*args):
#     #poseLocator = locator
#     #sels = obj
#     sels =mc.ls(sl=True)                                # select object selected
#     for each in sels:                                   # search in selected
#         poseLocator = mc.xform(each, q=True, t=True)    # find xform (x,y,z) 
#         jntFrLoc = mc.joint (each, p=(poseLocator[0],poseLocator[1],poseLocator[2]))  # create joint from poseLocator(x,y,z)
#         mean = mc.parent (jntFrLoc, w=True) 
#         changeName = each.split('_')  
#         mc.rename(jntFrLoc, '%s_Jnt'.format( changeName[0])    # chang new name obj               
#         # deleats = mc.delete (each)                      # delete object from selected each


def jointFromLocator (sels = [], clear = True,*args): ## Jackport
    if not sels:
        sels =mc.ls(sl=True)                                # select object selected
    for each in sels:                                   # search in selected
        poseLocator = mc.xform(each, q=True, t=True)    # find xform (x,y,z) 
        jntFrLoc = mc.joint (each, p=(poseLocator[0],poseLocator[1],poseLocator[2]))  # create joint from poseLocator(x,y,z)
        mean = mc.parent (jntFrLoc, w=True) 
        changeName = each.split('_')  
        JntName = mc.rename(jntFrLoc, '%s_Jnt' % changeName[0])    # chang new name obj              
        if clear:
            deleats = mc.delete (each)                      # delete object from selected each
    return JntName


def createJointFromLocator (name = '', side = '', lastName = '', *args):
    #poseLocator = locator
    #sels = obj
    sels =mc.ls(sl=True)     
    for each in sels:
        if name == '':   
            name, side, lastName = splitName(sels = [each])  
        else:  
            name , side , lastName 
                          # select object selected                         
        poseLocator = mc.xform(each, q=True, t=True)    # find xform (x,y,z) 
        jntFrLoc = mc.joint (each, p=(poseLocator[0],poseLocator[1],poseLocator[2]))  # create joint from poseLocator(x,y,z)
        mean = mc.parent (jntFrLoc, w=True)                    # split '_'
        newJnt = mc.rename(jntFrLoc, '%s%sJnt' % (name, side))    # chang new name obj               
        deleats = mc.delete (each)                      # delete object from selected each
    return newJnt
def function(*args):
    # passdynamicHair ():
    #poseLocator = locator
    #sels = obj
    sels =mc.ls(sl=True)                                # select object selected
    for each in sels:                                   # search in selected
        poseLocator = mc.xform(each, q=True, t=True)    # find xform (x,y,z) 
        jntFrLoc = mc.joint (each, p=(poseLocator[0],poseLocator[1],poseLocator[2]))  # create joint from poseLocator(x,y,z)
        mean = mc.parent (jntFrLoc, w=True)             # unparent from w=world  mean unparent
        deleats = mc.delete (each)                      # delete object from selected each

def zeroGroup( obj = '' ,*args ) :
    # Create zero group
    chld = pc.Dag( obj )
    grp = pc.Null()
    grp.snap( chld )
    chld.parent( grp )
    return grp

def doZroGroup (*args):
    # Create zero group with naming
    sels = mc.ls( sl=True )
    sides = ( 'LFT' , 'RGT' , 'UPR' , 'LWR' )
    zroGrps = []
    
    for sel in sels :
        curr = pc.Dag( sel )
        prnt = curr.getParent()
        zGrp = zeroGroup( sel )
        
        nameLst = curr.name.split( '_' )
        currType = nameLst[-1]
        charName = ''
        midName = ''
        currSide = ''
        
        if len( nameLst ) == 3 :
            charName = '%s_' % nameLst[0]
            midName = nameLst[1]
        else :
            midName = nameLst[0]
        
        for side in sides :
            if side in midName :
                currSide = side
                midName = midName.replace( side , '' )
        
        zGrp.name = '%s%s%s%sZro%s_Grp' % ( charName ,
                                            midName ,
                                            currType[0].upper() ,
                                            currType[1:] ,
                                            currSide
                                        )
        
        if prnt :
                zGrp.parent( prnt )
        
        zroGrps.append( zGrp )
    
        return zroGrps

def shadeCoppy (*args):
    sels =mc.ls(sl=True,l=True,nt=True,ap=True)
    
def gmbl(obj,*args):
    sels = mc.ls(sl=True)
    if sels:
        sels = sels
    else:
        sels = obj
    for each in sels:   
        each.add( ln='gimbalControl' , min=0 , max=1 , k=True )

def setRGBColor(ctrl, color = (1,1,1),*args):
    
    rgb = ("R","G","B")

    for each in ctrl:
        mc.setAttr(each + ".overrideEnabled",1)
        mc.setAttr(each + ".overrideRGBColors",1)
        
        for channel, color in zip(rgb, color):
            
            mc.setAttr(each + ".overrideColor%s" %channel, color)

def colors(cor = '',ctrl = [],*args):
    if ctrl:
        sel = ctrl
    else:
        sel = mc.ls(sl = True)
        
    for each in sel:
        if cor == 'none':
            setRGBColor(ctrl = [each], color = (0.451,0.451,0.451))
        if cor == 'black':
            setRGBColor(ctrl = [each], color = (0,0,0))
        if cor == 'gray1':
            setRGBColor(ctrl = [each], color = (0.89,0.89,0.89))
        if cor == 'gray2':
            setRGBColor(ctrl = [each], color = (0.561,0.561,0.561))      
        if cor == 'red2':
            setRGBColor(ctrl = [each], color = (0.800,0.44,0.200))          
        if cor == 'blue1':
            setRGBColor(ctrl = [each], color = (0,0,0.400))
        if cor == 'blue2':
            setRGBColor(ctrl = [each], color = (0,0,1))

        if cor == 'blue3':
            setRGBColor(ctrl = [each], color = (0.160,0.160,0.948))
        if cor == 'green2':
            setRGBColor(ctrl = [each], color = (0,0.302,0))
        if cor == 'purple':
            setRGBColor(ctrl = [each], color = (0.490,0,0.740))
        if cor == 'punky':
            setRGBColor(ctrl = [each], color = (1,0,1))
        if cor == 'brown1':
            setRGBColor(ctrl = [each], color = (0.600,0.302,0.200))
        if cor == 'brown2':
            setRGBColor(ctrl = [each], color = (0.251,0.129,0.129))
        if cor == 'orange':
            setRGBColor(ctrl = [each], color = (1,0.285,0))

        if cor == 'red':
            setRGBColor(ctrl = [each], color = (1,0,0))
        if cor == 'green':
            setRGBColor(ctrl = [each], color = (0,1,0))
        if cor == 'blue3':
            setRGBColor(ctrl = [each], color = (0,0.302,0.600))
        if cor == 'white':
            setRGBColor(ctrl = [each], color = (1,1,1))
        if cor == 'yellow':
            setRGBColor(ctrl = [each], color = (1,1,0))
        if cor == 'blue4':
            setRGBColor(ctrl = [each], color = (0,1,1))
        if cor == 'blue5':
            setRGBColor(ctrl = [each], color = (0,0.973,0.793))

def assignColor(obj = [], col = '',*args):
    colDic = {

        'none' : 0,
        'black' : 1 ,
        'gray' : 2 ,
        'softGray' : 3 ,
        'darkRed' : 4 ,
        'darkBlue' : 5 ,
        'blue' : 6 ,
        'darkGreen' : 7 ,
        'purple' : 8,     ## purple
        'pinky' : 9,     ## pinky
        'brown' : 10,   ##Brown
        'brownDark' : 11 ,  ##BrownDark
        'redDark' : 12,   ## RedDark
        'red' : 13 ,
        'green' : 14 ,
        'blueDark' : 15,   ## BlueDark
        'white' : 16 ,
        'yellow' : 17 , 
        'softBlue' : 18 ,
        'greenWhite' : 19,   # GreenWhite
        'pinkyWhite' : 20,   # PinkyWhite
        'yellowDark' : 21,   # YellowDark
        'yellowWhite' : 22,   # YellowWhite



    }

    cid = colDic[ col ]

    if obj:
        sels = obj 
    else:
        sels = mc.ls( sl=True )

    for sel in sels :
        
        shps = mc.listRelatives( sel , s=True )

        if shps :
            
            for shp in shps :
                
                if col == 'none' :
                    mc.setAttr( '%s.overrideColor' % shp , cid )
                    mc.setAttr( '%s.overrideEnabled' % shp , 0 )
                else :
                    mc.setAttr( '%s.overrideEnabled' % shp , 1 )
                    mc.setAttr( '%s.overrideColor' % shp , cid )

def addGimbalDtl(gimbal = True,*args):
    sel = mc.ls(sl = True)
    for each in sel:
        jntName = each.split('_')
        if len(jntName) == 3:
            name = jntName[0]
            side = '_' + jntName[1] + '_'
            lastName = jntName[-1]

        elif len(jntName) ==2:
            name = jntName[0] 
            side = '_'
            lastName = jntName[-1]
       
        jntCon = '%s%sJnt' % (name, side)

        ## Add Gimbal Control ---------------------------------------------------------------------------------------------------------------------------------
        ## Duplicate Gimbal Control
        gimBalGrp = mc.group (n = '%sGimbalCtrl%sGrp' % (name, side), em = True)
        mdvCounterGimbalNode = mc.createNode( 'multiplyDivide',n = '%sCounter%sMdv' % (name, side))  
        mc.setAttr('%s.input2X' % mdvCounterGimbalNode, -1)    
        mc.setAttr('%s.input2Y' % mdvCounterGimbalNode, -1) 
        mc.setAttr('%s.input2Z' % mdvCounterGimbalNode, -1)   
        mc.delete(mc.parentConstraint(each, gimBalGrp, mo = False))
        dupGmblCurve = mc.duplicate(each, n = '%sGimbal%s%s' % (name, side, jntName[-1]))
        mc.parent(gimBalGrp, each)
        pm.makeIdentity ( gimBalGrp , apply = True ) 

        mc.setAttr('%s.sx' % dupGmblCurve[0], 0.741)
        mc.setAttr('%s.sy' % dupGmblCurve[0], 0.741)
        mc.setAttr('%s.sz' % dupGmblCurve[0], 0.741)
        pm.makeIdentity ( dupGmblCurve[0] , apply = True ) 

        ## Counter Gimbal Control
        mc.connectAttr('%s.translate'% dupGmblCurve[0], '%s.input1' % mdvCounterGimbalNode)
        mc.connectAttr('%s.output'% mdvCounterGimbalNode, '%s.translate' % gimBalGrp)
        ## Add Attribute GeoVis
        attrNameMinMax (obj = '%sShape' % each, elem = 'gimbalControl', min = 0, max = 1,)
        if gimbal:
            mc.setAttr ("%sShape.gimbalControl" % each,  1)

        mc.parent(dupGmblCurve[0], each)
        mc.connectAttr('%sShape.gimbalControl' % each, '%sShape.v' % dupGmblCurve[0])

        ## Set Color Gimbal (White)
        assignColor(obj = [dupGmblCurve[0]], col = 'white')
        ## DisConnect Attribute -----------------------------------------------------------------------------------------------------------------------------
        ## ctrl con
        ctrlTranslate = '%s.translate' % each          
        ctrlRotate = '%s.rotate' % each
        ctrlScale = '%s.scale' % each

        ## jnt con
        jntTranslate = '%s.translate' % jntCon
        jntRotate = '%s.rotate' % jntCon
        jntScale = '%s.scale' % jntCon

        ## DisconnectAttr
        mc.disconnectAttr(ctrlTranslate, jntTranslate)
        mc.disconnectAttr(ctrlRotate, jntRotate)
        mc.disconnectAttr(ctrlScale, jntScale)

        ## Connect plusMinusAverage Translate-----------------------------------------------------------------------------------------------------------------
        ## createNode
        pmaTranslateNode = mc.createNode( 'plusMinusAverage', n = '%sTranslate%sPma' % (name, side))

        ## Connect Ctrl Translate PmaTranslateNode 
        mc.connectAttr ('%s.translateX' % each,  '%s.input3D[0].input3Dx' % pmaTranslateNode)
        mc.connectAttr ('%s.translateY' % each,  '%s.input3D[0].input3Dy' % pmaTranslateNode)
        mc.connectAttr ('%s.translateZ' % each,  '%s.input3D[0].input3Dz' % pmaTranslateNode)

        ## Connect CtrlGimbal Translate PmaTranslateNode 
        mc.connectAttr ('%sGimbal%s%s.translateX' % (name, side, jntName[-1]),  '%s.input3D[1].input3Dx' % pmaTranslateNode)
        mc.connectAttr ('%sGimbal%s%s.translateY' % (name, side, jntName[-1]),  '%s.input3D[1].input3Dy' % pmaTranslateNode)
        mc.connectAttr ('%sGimbal%s%s.translateZ' % (name, side, jntName[-1]),  '%s.input3D[1].input3Dz' % pmaTranslateNode)

        ## Connect Translate Pma to Joint
        mc.connectAttr ('%s.output3Dx' % pmaTranslateNode,  '%s.translateX' % jntCon)
        mc.connectAttr ('%s.output3Dy' % pmaTranslateNode,  '%s.translateY' % jntCon)
        mc.connectAttr ('%s.output3Dz' % pmaTranslateNode,  '%s.translateZ' % jntCon)

        ## Connect plusMinusAverage Rotate-----------------------------------------------------------------------------------------------------------------
        ## createNode
        pmaRotateNode = mc.createNode( 'plusMinusAverage', n = '%sRotate%sPma' % (name, side))

        ## Connect Ctrl Rotate PmaRotateNode 
        mc.connectAttr ('%s.rotateX' % each,  '%s.input3D[0].input3Dx' % pmaRotateNode)
        mc.connectAttr ('%s.rotateY' % each,  '%s.input3D[0].input3Dy' % pmaRotateNode)
        mc.connectAttr ('%s.rotateZ' % each,  '%s.input3D[0].input3Dz' % pmaRotateNode)

        ## Connect CtrlGimbal Rotate PmaRotateNode 
        mc.connectAttr ('%sGimbal%s%s.rotateX' % (name, side, jntName[-1]),  '%s.input3D[1].input3Dx' % pmaRotateNode)
        mc.connectAttr ('%sGimbal%s%s.rotateY' % (name, side, jntName[-1]),  '%s.input3D[1].input3Dy' % pmaRotateNode)
        mc.connectAttr ('%sGimbal%s%s.rotateZ' % (name, side, jntName[-1]),  '%s.input3D[1].input3Dz' % pmaRotateNode)

        ## Connect Rotate  Pma to Joint
        mc.connectAttr ('%s.output3Dx' % pmaRotateNode,  '%s.rotateX' % jntCon)
        mc.connectAttr ('%s.output3Dy' % pmaRotateNode,  '%s.rotateY' % jntCon)
        mc.connectAttr ('%s.output3Dz' % pmaRotateNode,  '%s.rotateZ' % jntCon)

        ## Connect plusMinusAverage Rotate-----------------------------------------------------------------------------------------------------------------
        ## createNode
        pmaScaleNode = mc.createNode( 'plusMinusAverage', n = '%sScale%sPma' % (name, side))
        addDoubleLinearXNode = mc.createNode('addDoubleLinear', n = '%sScale%sAdl' % (name, side))
        addDoubleLinearYNode = mc.createNode('addDoubleLinear', n = '%sScale%sAdl' % (name, side))
        addDoubleLinearZNode = mc.createNode('addDoubleLinear', n = '%sScale%sAdl' % (name, side))
        mc.addAttr( addDoubleLinearXNode , ln = 'default' , at = 'float' , k = True )
        mc.connectAttr('%s.default' % addDoubleLinearXNode, '%s.input1' % addDoubleLinearXNode)
        mc.addAttr( addDoubleLinearYNode , ln = 'default' , at = 'float' , k = True )
        mc.connectAttr('%s.default' % addDoubleLinearYNode, '%s.input1' % addDoubleLinearYNode)
        mc.addAttr( addDoubleLinearZNode , ln = 'default' , at = 'float' , k = True )
        mc.connectAttr('%s.default' % addDoubleLinearZNode, '%s.input1' % addDoubleLinearZNode)
        mc.setAttr('%s.default' % addDoubleLinearXNode, -1)
        mc.setAttr('%s.default' % addDoubleLinearYNode, -1)
        mc.setAttr('%s.default' % addDoubleLinearZNode, -1)

        ## Connect Ctrl Scale PmaScaleNode 
        mc.connectAttr ('%s.scaleX' % each,  '%s.input3D[0].input3Dx' % pmaScaleNode)
        mc.connectAttr ('%s.scaleY' % each,  '%s.input3D[0].input3Dy' % pmaScaleNode)
        mc.connectAttr ('%s.scaleZ' % each,  '%s.input3D[0].input3Dz' % pmaScaleNode)

        ## Connect CtrlGimbal Rotate PmaScaleNode 
        mc.connectAttr ('%sGimbal%s%s.scaleX' % (name, side, jntName[-1]),  '%s.input3D[1].input3Dx' % pmaScaleNode)
        mc.connectAttr ('%sGimbal%s%s.scaleY' % (name, side, jntName[-1]),  '%s.input3D[1].input3Dy' % pmaScaleNode)
        mc.connectAttr ('%sGimbal%s%s.scaleZ' % (name, side, jntName[-1]),  '%s.input3D[1].input3Dz' % pmaScaleNode)

        ## Connect Pma to addDoubleLinearNode
        mc.connectAttr ('%s.output3Dx' % pmaScaleNode,  '%s.input2' % addDoubleLinearXNode)
        mc.connectAttr ('%s.output3Dy' % pmaScaleNode,  '%s.input2' % addDoubleLinearYNode)
        mc.connectAttr ('%s.output3Dz' % pmaScaleNode,  '%s.input2' % addDoubleLinearZNode)

        # ## Connect Scale  Pma to Joint
        mc.connectAttr ('%s.output'% addDoubleLinearXNode, '%s.scaleX' % jntCon)
        mc.connectAttr ('%s.output'% addDoubleLinearYNode, '%s.scaleY' % jntCon)
        mc.connectAttr ('%s.output'% addDoubleLinearZNode, '%s.scaleZ' % jntCon)

        ## Parent GimbalCurve at gimBalGrp
        mc.parent(dupGmblCurve, gimBalGrp)



def connection2Obj(objA = '',target = '',*args):
    nameSp = objA.split('_')
    if len(nameSp) == 3:
        name = nameSp[0]
        side = '_' + nameSp[1] + '_'
        lastName = nameSp[-1]
    elif len(nameSp) ==2:
        name = nameSp[0] 
        side = '_'
        lastName = nameSp[-1]
   
    ## Connect plusMinusAverage Translate-----------------------------------------------------------------------------------------------------------------
    ## createNode
    pmaTranslateNode = mc.createNode( 'plusMinusAverage', n = '%sTranslate%sPma' % (name, side))

    ## Connect Ctrl Translate PmaTranslateNode 
    mc.connectAttr ('%s.translateX' % objA,  '%s.input3D[0].input3Dx' % pmaTranslateNode)
    mc.connectAttr ('%s.translateY' % objA,  '%s.input3D[0].input3Dy' % pmaTranslateNode)
    mc.connectAttr ('%s.translateZ' % objA,  '%s.input3D[0].input3Dz' % pmaTranslateNode)

    ## Connect CtrlGimbal Translate PmaTranslateNode 
    mc.connectAttr ('%sGmbl%s%s.translateX' % (name, side, nameSp[-1]),  '%s.input3D[1].input3Dx' % pmaTranslateNode)
    mc.connectAttr ('%sGmbl%s%s.translateY' % (name, side, nameSp[-1]),  '%s.input3D[1].input3Dy' % pmaTranslateNode)
    mc.connectAttr ('%sGmbl%s%s.translateZ' % (name, side, nameSp[-1]),  '%s.input3D[1].input3Dz' % pmaTranslateNode)

    ## Connect Translate Pma to Joint
    mc.connectAttr ('%s.output3Dx' % pmaTranslateNode,  '%s.translateX' % target)
    mc.connectAttr ('%s.output3Dy' % pmaTranslateNode,  '%s.translateY' % target)
    mc.connectAttr ('%s.output3Dz' % pmaTranslateNode,  '%s.translateZ' % target)

    ## Connect plusMinusAverage Rotate-----------------------------------------------------------------------------------------------------------------
    ## createNode
    pmaRotateNode = mc.createNode( 'plusMinusAverage', n = '%sRotate%sPma' % (name, side))

    ## Connect Ctrl Rotate PmaRotateNode 
    mc.connectAttr ('%s.rotateX' % objA,  '%s.input3D[0].input3Dx' % pmaRotateNode)
    mc.connectAttr ('%s.rotateY' % objA,  '%s.input3D[0].input3Dy' % pmaRotateNode)
    mc.connectAttr ('%s.rotateZ' % objA,  '%s.input3D[0].input3Dz' % pmaRotateNode)

    ## Connect CtrlGimbal Rotate PmaRotateNode 
    mc.connectAttr ('%sGmbl%s%s.rotateX' % (name, side, nameSp[-1]),  '%s.input3D[1].input3Dx' % pmaRotateNode)
    mc.connectAttr ('%sGmbl%s%s.rotateY' % (name, side, nameSp[-1]),  '%s.input3D[1].input3Dy' % pmaRotateNode)
    mc.connectAttr ('%sGmbl%s%s.rotateZ' % (name, side, nameSp[-1]),  '%s.input3D[1].input3Dz' % pmaRotateNode)

    ## Connect Rotate  Pma to Joint
    mc.connectAttr ('%s.output3Dx' % pmaRotateNode,  '%s.rotateX' % target)
    mc.connectAttr ('%s.output3Dy' % pmaRotateNode,  '%s.rotateY' % target)
    mc.connectAttr ('%s.output3Dz' % pmaRotateNode,  '%s.rotateZ' % target)

    ## Connect plusMinusAverage Rotate-----------------------------------------------------------------------------------------------------------------
    ## createNode
    pmaScaleNode = mc.createNode( 'plusMinusAverage', n = '%sScale%sPma' % (name, side))
    addDoubleLinearXNode = mc.createNode('addDoubleLinear', n = '%sScale%sAdl' % (name, side))
    addDoubleLinearYNode = mc.createNode('addDoubleLinear', n = '%sScale%sAdl' % (name, side))
    addDoubleLinearZNode = mc.createNode('addDoubleLinear', n = '%sScale%sAdl' % (name, side))
    mc.addAttr( addDoubleLinearXNode , ln = 'default' , at = 'float' , k = True )
    mc.connectAttr('%s.default' % addDoubleLinearXNode, '%s.input1' % addDoubleLinearXNode)
    mc.addAttr( addDoubleLinearYNode , ln = 'default' , at = 'float' , k = True )
    mc.connectAttr('%s.default' % addDoubleLinearYNode, '%s.input1' % addDoubleLinearYNode)
    mc.addAttr( addDoubleLinearZNode , ln = 'default' , at = 'float' , k = True )
    mc.connectAttr('%s.default' % addDoubleLinearZNode, '%s.input1' % addDoubleLinearZNode)
    mc.setAttr('%s.default' % addDoubleLinearXNode, -1)
    mc.setAttr('%s.default' % addDoubleLinearYNode, -1)
    mc.setAttr('%s.default' % addDoubleLinearZNode, -1)

    ## Connect Ctrl Scale PmaScaleNode 
    mc.connectAttr ('%s.scaleX' % objA,  '%s.input3D[0].input3Dx' % pmaScaleNode)
    mc.connectAttr ('%s.scaleY' % objA,  '%s.input3D[0].input3Dy' % pmaScaleNode)
    mc.connectAttr ('%s.scaleZ' % objA,  '%s.input3D[0].input3Dz' % pmaScaleNode)

    ## Connect CtrlGimbal Rotate PmaScaleNode 
    mc.connectAttr ('%sGmbl%s%s.scaleX' % (name, side, nameSp[-1]),  '%s.input3D[1].input3Dx' % pmaScaleNode)
    mc.connectAttr ('%sGmbl%s%s.scaleY' % (name, side, nameSp[-1]),  '%s.input3D[1].input3Dy' % pmaScaleNode)
    mc.connectAttr ('%sGmbl%s%s.scaleZ' % (name, side, nameSp[-1]),  '%s.input3D[1].input3Dz' % pmaScaleNode)

    ## Connect Pma to addDoubleLinearNode
    mc.connectAttr ('%s.output3Dx' % pmaScaleNode,  '%s.input2' % addDoubleLinearXNode)
    mc.connectAttr ('%s.output3Dy' % pmaScaleNode,  '%s.input2' % addDoubleLinearYNode)
    mc.connectAttr ('%s.output3Dz' % pmaScaleNode,  '%s.input2' % addDoubleLinearZNode)

    # ## Connect Scale  Pma to Joint
    mc.connectAttr ('%s.output'% addDoubleLinearXNode, '%s.scaleX' % target)
    mc.connectAttr ('%s.output'% addDoubleLinearYNode, '%s.scaleY' % target)
    mc.connectAttr ('%s.output'% addDoubleLinearZNode, '%s.scaleZ' % target)




def attrEnum(obj = '', ln = '', en ='',*args):
    mc.addAttr( obj , ln = ln , at = 'enum' , en = en , k = True )

def attrNameTitle(obj = '', ln = '',*args):
    mc.addAttr( obj , ln = ln , at = 'enum' , k = True ,min = 0, max = 0 ,dv = 0)
    # mc.setAttr('%s.%s' % (obj, ln), l = True)

def attrName(obj = '', elem = '', *args):

    if not obj:
        sels = mc.ls(sl=True)
        obj = sels
        if mc.objExists(elem == ''):
            mc.addAttr(obj,ln = 'gimbalControl' , k = True)
        else:
            mc.addAttr(obj,ln = elem , k = True)

    else:

        if mc.objExists(elem == ''):
            mc.addAttr(obj, ln= 'gimbalControl', k=True)
        else:
            mc.addAttr(obj,ln = elem , k = True)

def attrNameMinMax (obj = '', elem = '', min = 0, max = 1, *args):

    if not mc.objExists(obj):
        sels = mc.ls(sl=True)
        obj = sels
        if not mc.objExists(elem):
            mc.addAttr(obj,ln = 'gimbalControl' , at = 'long', min = min, max = max , k = True)
        else:
            mc.addAttr(obj,ln = elem  , at = 'long', min = min, max = max, k = True)

    else:

        if mc.objExists(elem):
            mc.addAttr(obj, ln= 'gimbalControl', at = 'long', min = min, max = max, k=True)
        else:
            mc.addAttr(obj,ln = elem ,at = 'long', min = min , max = max, k = True)

def addAttrCtrl (ctrl = '', elem = [], min = 0, max = 1, at = 'long', dv = 0,*args):

    for each in elem:
        mc.addAttr(ctrl, ln = each , at = at, min = min , max = max, k = True, dv = dv)
    return elem
        
#find and delete keyFrame and vraySetting      
def clearKey(*args):
# select target1
    cur = mc.select(all=True)
# currentTime
    mc.currentTime (0)
# delete keyFrame from 0 to 2000
    mc.cutKey(time=(-200,2000)) 
    mc.delete ('vraySettings')
    mc.select(cl=True)


def cleanCamera(*args):  
    # clean Camera fot default
    cams = mc.ls(sl=True)
    for cam in cams:
        mc.camera(cam,e=True,startupCamera=False)
        mc.delete(cam)
    
def chang4Kto1K(*args):
    #P-----------------------------------Find Texture 4K chang to 1K----------------------------
    files = mc.ls('*_aFile')
    for each in files : 
        path = mc.getAttr('%s.fileTextureName' % each)
    if '/4K/' in path : 
        newPath = path.replace('/4K/', '/1K/')
        mc.setAttr('%s.fileTextureName' % each, newPath, type = 'string')

#Expression
#     #ES--------------------------------code  loop trainStaion -------------------------------------------------
#     railwayMoveIk_ikh.offset = trainStationOffset_ctrl.trainOffset*0.001;
#     int $round = railwayMoveIk_ikh.offset;
#     if (railwayMoveIk_ikh.offset > 1)
#         railwayMoveIk_ikh.offset = ((trainStationOffset_ctrl.trainOffset*0.001)-$round);

#     railwayMoveIk_ikh.offset = trainStationbogie1_loc.offset*0.001;
#     int $round = railwayMoveIk_ikh.offset;
#     if (railwayMoveIk_ikh.offset > 1)
#         railwayMoveIk_ikh.offset = ((trainStationbogie1_loc.offset*0.001)-$round);

def eyeLidFollow(*args):
    eyeList = mc.ls('*:eye_lft_jnt', '*:eye_rgt_jnt')
    eyeLidsList = mc.ls('*:EyeLidsBsh_L_Jnt', '*:EyeLidsBsh_R_Jnt')
    eyeCmp = mc.ls('*:EyeBallFollowIO_L_Cmp', '*:EyeBallFollowIO_R_Cmp',)
    eyelidFol =mc.ls('*:eye_rgt_ctrl', '*:eye_lft_ctrl')

    mc.parentConstraint(eyeList[0],eyeLidsList[0], mo=True)
    mc.parentConstraint(eyeList[1],eyeLidsList[1], mo=True)

    # set eyeLidsBsh
    mc.setAttr('%s.eyelidsFollow' % eyeLidsList[0], 1)
    mc.setAttr('%s.eyelidsFollow' % eyeLidsList[1], 1)

    # set lidFollow
    mc.setAttr('%s.lidFollow' % eyelidFol[0], 0)
    mc.setAttr('%s.lidFollow' % eyelidFol[1], 0)

    # L
    #mc.setAttr('%s.minG' % eyeCmp[0], -300)
    #mc.setAttr('%s.maxR' % eyeCmp[0], 300)
    # R
    #mc.setAttr('%s.minR' % eyeCmp[1], -300)
    #mc.setAttr('%s.maxG' % eyeCmp[1], 300)

def getUserFromIp(*args):
    """
    Get user name from data base (config) using the current machine ip.
        return: User name(str)
    """

    ipAddress = socket.gethostbyname(socket.gethostname())
    try :
        user = config.USER_IP[str(ipAddress)]
    except:
        user = 'ken'
    return "%s : %s" % (user, ipAddress)

def lockAttrs(listObj, lock,*args):
    # listEyes = ['EyeLidsBsh_L_Jnt','EyeLidsBsh_R_Jnt']
    attrs = ['t', 'r', 's' , 'eyelidsFollow']
#attrEyeLids = ["eyelidsFollow"]
    for each in listObj:
        if mc.objExists(each):
            for eyeJnt in listObj:
                for attr in attrs:
                    mc.setAttr( "%s.%s" %(eyeJnt, attr), lock = True)
                    #mc.setAttr('%s.v' %eyeJnt ,lock = 0)

def lockHideAttr(listObj, lock, keyable,*args):
    # listEyes = ['EyeLidsBsh_L_Jnt','EyeLidsBsh_R_Jnt']
    attrs = ['t', 'r', 's', 'v', 'squash', 'stretch', 'eyelidsFollow']
#attrEyeLids = ["eyelidsFollow"]
    for each in listObj:
        if mc.objExists(each):
            for eyeJnt in listObj:
                for each in attrs:
                    if mc.objExists(each):
                        for attr in attrs:
                            mc.setAttr( "%s.%s" %(eyeJnt, attr), lock = lock, keyable = keyable)
                            #mc.setAttr('%s.v' %eyeJnt ,lock = 0)

# unlock mdv Node
def lockMdv(listObj, lock,*args):
    attrsMdv = ['i1x', 'i1y', 'i1z', 'i2x' , 'i2y', 'i2z']
    for each in listObj:
        if mc.objExists(each):
            for listObjs in listObj:
                for attrsMdvs in attrsMdv:
                    mc.setAttr( "%s.%s" %(listObjs, attrsMdvs), lock = lock)
                    #mc.setAttr('%s.v' %listMdvs ,lock = 0)

def lockAttrObj(listObj, lock = True,keyable = False, *args):
    # listBow = ("bowARig_lft_grp","bowBRig_rgt_grp")
    i = 0 
    for each in listObj:
        for attr in ( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v') :
            for i in range(len(listObj)):
                    mc.setAttr("%s.%s" % (each, attr) , lock = lock, keyable = keyable)
        i += 1  

def lockAttr(listObj = [], attrs = [],lock = True, keyable = True,*args):
    # listBow = ("bowARig_lft_grp","bowBRig_rgt_grp")
    # attrs = ( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v')

    for i in range(len(listObj)):
        for x in range(len(attrs)):
            mc.setAttr("%s.%s" % (listObj[i], attrs[x]) , lock = lock, keyable = keyable)


# def attrFunction(listObj = [], attrs = [], lock , hide , *args):
#     # listBow = ("bowARig_lft_grp","bowBRig_rgt_grp")
#     # attrs = ( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v')
#     i = 0 
#     for each in listObj:
#         for attr in  attrs:
#             for i in range(len(listObj)):
#                 mc.setAttr("%s.%s" % (each, attr) , lock = lock, hide = hide)
#         i += 1  

def unlockAttrs(listObj = [], lock = True, attrs = '', *args):
    # listBow = ("bowARig_lft_grp","bowBRig_rgt_grp")
    # #attr = ( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v')
    for each in listObj:
        for attr in attrs :
            for i in range(len(listObj)):
                mc.setAttr("%s.%s" % (each, attr) , lock = lock)

def getSkinCluster(mesh):
    
    shapes = mc.listRelatives(mesh, shapes=True, path=True)

    for shape in shapes:
        history = mc.listHistory(shape, groupLevels=True, pruneDagObjects=True)
        if not history:
            continue
        skins = mc.ls(history, type='skinCluster')
        if skins:
            return skins[0]
    
    return None 

def unInfluences (listObj,*args):
    for each in listObj:
        skinNode = getSkinCluster(mesh = each)

def getInfluences(skinNode = '', *args):
    # skinNode = utl.getSkinCluster(model[0])
    
    if not skinNode:
        raise RuntimeError('Mesh must have an existing skinCluster')

    influences = mc.skinCluster(skinNode, query=True, influence=True)
    for influence in influences:
        mc.skinCluster(skinNode, edit=True, removeUnusedInfluence = True, influence=influence, lockWeights=False)

    return influences


def remove_unused_influences(mesh = [], targetInfluences=[]):

    # remove_unused_influences(mesh = [ken_Geo], targetInfluences=['ken1_Jnt', 'ken2_Jnt']

    '''
    Snippet to removeUnusedInfluences in Autodesk Maya using Python.
    The MEL version runs slowly, over every influence one at a time.
    "targetInfluences" allows you to directly specify which influences to remove.
    This will only remove targets which are not currently being used.
    '''
    for each in mesh:
        skinCls = getSkinCluster(mesh = [each])
        influence = getInfluences(skinNode = skinCls)
        if not targetInfluences:
            targetInfluences = influence
        allInfluences = mc.skinCluster(skinCls, q=True, inf=True)
        weightedInfluences = mc.skinCluster(skinCls, q=True, wi=True)
        unusedInfluences = [inf for inf in allInfluences if inf not in weightedInfluences]
        if targetInfluences:
            unusedInfluences = [
                    inf for inf in allInfluences
                    if inf in targetInfluences
                    if inf not in weightedInfluences
                    ]
        mc.skinCluster(skinCls, e=True, removeInfluence=unusedInfluences)

def getDataFld(*args) :      
    wfn = os.path.normpath( pc.sceneName() )
    tmpAry = wfn.split( '\\' )
    tmpAry[-2] = 'data' 
    dataFld = '\\'.join( tmpAry[0:-1] ) 
    if not os.path.isdir( dataFld ) :
        os.mkdir( dataFld ) 
    return dataFld     
def copyObjFile(src,*args):
    shutil.copy(src, getDataFld())

def clean ( target = '' , *args ) :
    sels = mc.ls(sl=True)
    selsList = len(sels)
    if mc.objExists (target):
        # freeze transform , delete history , center pivot
        pm.makeIdentity ( target , apply = True ) ;    
        pm.delete ( target , ch = True ) ;
        pm.xform ( target , cp = True ) ;
    else:
        if selsList >= 1:
            target = sels
            # freeze transform , delete history , center pivot
            pm.makeIdentity ( target , apply = True ) ;    
            pm.delete ( target , ch = True ) ;
            pm.xform ( target , cp = True ) ;
        else:
            print "Don't Freeze Tranform. Please Select Object.!!"  

def fixJawMthRigTZ(*args):
    lists = mc.ls("JawMthRig_Ctrl", "JawMthRigJntTzColl_Pma")
    try:
        mc.disconnectAttr("%s.translate.translateY" % lists[0], "%s.input1D[0]" % lists[1])
        mc.connectAttr("%s.translate.translateZ" % lists[0], "%s.input1D[0]" % lists[1])
    except: pass

def parentScaleConstraint(*args):
    sels = mc.ls(sl=True)
    mc.parentConstraint(sels[0], sels[1], mo=True)
    mc.scaleConstraint(sels[0], sels[1], mo=True)

def parentScaleConstraintLoop(obj = '', lists = [],par = True, sca = True, ori = True, poi = True, *args):
    if obj:
        if par == True:
            for each in lists :
                mc.parentConstraint(obj, each, mo=True)
        if sca == True:
            for each in lists :
                mc.scaleConstraint(obj, each, mo=True)
        if ori == True:
            for each in lists :
                mc.orientConstraint(obj, each, mo=True)
        if poi == True:
            for each in lists :
                mc.pointConstraint(obj, each, mo=True)

def upperFnt(*args):
    sels = mc.ls(sl=True)
    x=0
    for i in range(len(sels)):
        ken = mc.rename(sels[i].upper())
        x+=1

def scaleObj(*args):
    sels = mc.ls(sl=True)
    addList =['scaleX', 'scaleY', 'scaleZ']

    for each in sels: 
        obj = ('%sShape' % each)  

# SearchNaming for select
def searchName(elem = '', *args):
    sels = mc.ls(sl=True)
    obj = []
    for sel in sels:
        if elem in sel:
            obj.append(sel)
            mc.select(obj)
        else:
            pass
# Run            
# searchName(elem = 'R')

def rivetToSeletedEdge(part=''):

    sels = mc.ls(sl=True, fl=True)
    rivet(part=part, fstEdge=sels[0], secEdge=sels[1])

#ConnectAttr Translate Rotate Scale
def connectTRS(trs = True, translate = True, rotate = True, scale = True, *args):
    sels = mc.ls(sl=True)
    lists = len(sels)

    if lists == 0:
        print 'Please Select Object 1 and Object 2'
    else:
        obj1 = sels[0]
        obj2 = sels[1]
        if (trs == True):
            mc.connectAttr('%s.translate' % obj1, '%s.translate' % obj2)
            mc.connectAttr('%s.rotate' % obj1, '%s.rotate' % obj2)
            mc.connectAttr('%s.scale' % obj1, '%s.scale' % obj2) 
        else:
            if (translate ==True):
                mc.connectAttr('%s.translate' % obj1, '%s.translate' % obj2) 
            elif (rotate ==True):
                mc.connectAttr('%s.rotate' % obj1, '%s.rotate' % obj2)  
            elif (scale == True):
                mc.connectAttr('%s.scale' % obj1, '%s.scale' % obj2)  

def assetName (*args):
    asset = context_info.ContextPathInfo()
    projectName = asset.path.name() if mc.file(q=True, sn=True) else ''
    nameObjs =  projectName.split('/')
    subType = nameObjs[-1]
    return subType

def mainGroupName (*args):

    subType = 'Rig'
    mgObj = mainGroup.MainGroup(subType)

def weaponRig2(jnts=['Rig:wristLFT_jnt', 'Rig:wristRGT_jnt']) :
    # Rigging weapons
    sels = mc.ls( sl=True )
    
    lHandLoc, rHandLoc = createWeaponLocator2(jnts)
    
    for sel in sels :
        
        currName = sel.split( '_' )[0]
        currName2 = currName.split ('Geo') [0]
        
        ctrl = rigTools.jointControl( 'sphere' )
        ctrl.name = '%s_ctrl' % currName2
        ctrl.color = 'yellow'

        ctrl.add( ln='geoVis' , min=0 , max=1 , dv=1 , k=True )
        ctrl.attr( 'v' ).lock = True
        ctrl.attr( 'v' ).hide = True

        mc.connectAttr( '%s.geoVis' % ctrl , '%s.v' % sel )
        
        ctrlGrp = pc.group( ctrl )
        ctrlGrp.name = '%sCtrlZero_grp' % currName2
        
        # mc.skinCluster( ctrl , sel , tsb=True )
        mc.parentConstraint( ctrl , sel , mo=True )
        mc.scaleConstraint ( ctrl , sel , mo=True ) 
        
        lftGrp , rgtGrp = parentLeftRight( ctrl , lHandLoc.name , rHandLoc.name , ctrlGrp )
        lftGrp.name = '%sCtrlLeft_grp' % currName2
        rgtGrp.name = '%sCtrlRgt_grp' % currName2
        
def createWeaponLocator2(jnts) :
    # Creating location for mounting weapons at left hand and right hand
    lHandJnt = jnts[0]
    rHandJnt = jnts[1]

    lHand = pc.Locator()
    rHand = pc.Locator()

    poses = []
    for i in [lHandJnt,rHandJnt]:
        position = ''
        nsSplit = i.split(':')[-1]
        revElement = nsSplit.split('_')[0][::-1]

        for a in range(len(revElement)):
            if revElement[a].isupper():
                position += revElement[a]
            else:
                break
        poses.append(position[::-1])

    lHand.name = 'weapon%s_loc' %poses[0]
    rHand.name = 'weapon%s_loc' %poses[1]

    mc.delete( mc.pointConstraint( lHandJnt , lHand  ) )
    mc.delete( mc.pointConstraint( rHandJnt , rHand  ) )

    mc.parentConstraint( lHandJnt , lHand , mo=True )
    mc.scaleConstraint(lHandJnt , lHand , mo=True)
    mc.parentConstraint( rHandJnt , rHand , mo=True )
    mc.scaleConstraint(rHandJnt , rHand , mo=True)

    return lHand, rHand

    
def parentLeftRight( ctrl = '' , localObj = '' , worldObj = '' , parGrp = '' ) :
    # Blending parent between left hand and right hand.
    # Returns : locGrp , worGrp , worGrpParCons , parGrpParCons and parGrpParConsRev
    locGrp = pc.Null()
    worGrp = pc.Null()

    locGrp.snap( localObj )
    worGrp.snap( worldObj )

    worGrpParCons = pc.parentConstraint( worldObj , worGrp )
    worGrpParCons = pc.scaleConstraint( worldObj , worGrp )

    locGrpParCons = pc.parentConstraint( localObj , locGrp )
    locGrpParCons = pc.scaleConstraint( localObj , locGrp )

    parGrpParCons = pc.parentConstraint( locGrp , worGrp , parGrp )
    parGrpParCons2 = pc.scaleConstraint( locGrp , worGrp , parGrp , mo=True)
    parGrpParConsRev = pc.Reverse()

    con = pc.Dag( ctrl )

    attr = 'leftRight'
    con.add( ln = attr , k = True, min = 0 , max = 1 )
    con.attr( attr ) >> parGrpParCons.attr( 'w1' )
    con.attr( attr ) >> parGrpParConsRev.attr( 'ix' )
    parGrpParConsRev.attr( 'ox' ) >> parGrpParCons.attr( 'w0' )

    pc.clsl()

    return locGrp , worGrp  


def parentJnt (*args):
    lists = mc.ls(sl=True)
    check = lists[-1]
    objLists = len(lists)
    for idx, each in enumerate(lists):   #idx , enumerate(lists) : check index (i=0, i+=1)
        if idx+1 >= objLists:
            break
        mc.parent(lists[idx+1], each)


# get NameSpace in scenes
def nameSpaceLists(*args):
    nsList = mc.namespaceInfo(lon = True)
    nameSp = []
    for lists in nsList:
        if lists in ['UI' , 'shared']:
            pass

        else:
            nameSp.append(lists)
    return nameSp

def getNs(nodeName=''):
    """
    Get namespace from given name
    Ex. aaa:bbb:ccc:ddd_grp > aaa:bbb:ccc:
    """
    ns = ''

    if '|' in nodeName:
        nodeName = nodeName.split('|')[-1]

    if ':' in nodeName:

        nmElms = nodeName.split(':')

        if nmElms:
            ns = '%s:' % ':'.join(nmElms[0:-1])
    return ns

def parentExportGrp(*args):
    exportGrp = 'Export_Grp'
    if mc.objExists(exportGrp):
        DeleteGrp.deleteGrp(obj = exportGrp)

def reNameCapitalize(*args):
    lists = mc.ls(sl=True)
    objLen = len(lists)
    reNameObj = []
    sameObj = []
    i = 0 
    if i <= objLen:
        for each in lists:
            spLists = each.split('_')
            if len(spLists) == 1:
                nameObj = spLists[0]  
                nameCapObj = nameObj.capitalize()
                obj = mc.rename(each,'%s_Geo' % nameCapObj)
                reNameObj.append(each)
            elif len(spLists) == 2:
                nameObj = spLists[0]
                lastNameObj = spLists[1]
                nameCapObj = nameObj.capitalize()
                lastNameCapObj = lastNameObj.capitalize()
                obj = mc.rename(each,'%s_%s' % (nameCapObj , lastNameCapObj))
                reNameObj.append(each)
            elif len(spLists) == 3:
                nameObj = spLists[0]
                sideObj = spLists[1]
                lastNameObj = spLists[2]
                nameCapObj = nameObj.capitalize()
                sideCapObj = sideObj.capitalize()
                lastNameCapObj = lastNameObj.capitalize()
                obj = mc.rename(each,'%s_%s_%s' % (nameCapObj , sideCapObj, lastNameCapObj)) 
                reNameObj.append(each)    
            else:
                sameObj.append(each)

def openBook(name = 'BookUp1', valueBook1 = 1, valueBook2 = 0.7,valueBook3 = 0.5,valueBook4 = 0.3, rotateObj = 'rotateX',*args):
    lists = mc.ls(sl=True)
    obj1 = lists[0]
    obj2 = lists[1]
    splObj2 = obj2.split('_')
    numbers = (int(splObj2[1]))
    attrName(obj = obj1, elem = name)

    attrs = 'X'
    valueBook = [valueBook1, valueBook2, valueBook3, valueBook4]
    i = 0
    u = 0
    for each in valueBook:
        elemMdv = mc.createNode('multiplyDivide', n = ('%s%s_Mdv' % (name, u+1)))
        attrName(obj = ('%sShape' % obj1), elem = ('%s%s%s' % (name,'Amp',u+1)))
        ampListt = mc.setAttr('%sShape.%s%s%s' % (obj1 , name, 'Amp', u+1), valueBook[u] )
        mc.connectAttr('%sShape.%s%s%s' % (obj1 , name, 'Amp', u+1), ('%s.input2X' % elemMdv))
        mc.connectAttr('%s.%s' % (obj1, name), ('%s.input1%s' % (elemMdv, attrs)))
        mc.connectAttr('%s.output%s' % (elemMdv, attrs), ('%s%sOfst_%s_Grp.%s' % (splObj2[0], splObj2[2], (numbers + u), rotateObj)))

        u += 1

def connectBsh(baseBsh = 'Body_Geo', bufferGeo = 'BodyBffr_Geo', eyeBshGrp = 'EyeBshGeo_Grp', mthBshGrp = 'MthBshGeo_Grp', ebBshGrp = 'EbBshGeo_Grp', eyeCtrl = 'Eye_Ctrl',*args):
    objGeo = [eyeBshGrp, mthBshGrp, ebBshGrp]
    eyeBshGrpLists = mc.listRelatives(mc.ls(eyeBshGrp))
    mthBshGrpLists = mc.listRelatives(mc.ls(mthBshGrp))
    ebBshGrpLists = mc.listRelatives(mc.ls(ebBshGrp))
    numbersEyeBsh = len(eyeBshGrpLists)

    eyeObj = eyeBshGrp.split('BshGeo_Grp')
  

    if mc.objExists(bufferGeo):
        bffrSplit = bufferGeo.split('_')
        newNameBffr = ('%s%s%s' % (bffrSplit[0], 'Bsh', '_Geo'))
        mc.blendShape(bufferGeo, baseBsh, frontOfChain = True, name = newNameBffr)

    if mc.objExists(eyeBshGrp):
        bshListsObj = []
        if mc.objExists(eyeBshGrp):
            mc.select(eyeBshGrpLists)   
            mc.select(bufferGeo, add= True)
            bshLists = mc.blendShape(name = '%sBsh_Geo' % eyeObj[0])
        if mc.objExists(eyeCtrl):
            attrName(obj = ('%sShape' % eyeCtrl), elem = ('%s%s' % (eyeObj[0],'Amp')))
            mc.setAttr('%sShape.%sAmp' % (eyeCtrl,eyeObj[0]), l = True)
            for each in eyeBshGrpLists:
                eachSplit = each.split('_')
                newNameBsh = ('%s%s' % (eachSplit[0], 'Bsh'))
                bshListsObj.append(newNameBsh)
                attrName(obj = eyeCtrl, elem = newNameBsh)

                attrName(obj = ('%sShape' % eyeCtrl), elem = ('%s%s' % (newNameBsh,'Amp')))
                elemMdv = mc.createNode('multiplyDivide', n = ('%s_Mdv' % each))
                mc.setAttr('%sShape.%sAmp' % (eyeCtrl,newNameBsh), 0.1)
                mc.connectAttr('%sShape.%sAmp' % (eyeCtrl,newNameBsh), '%s.input2X' % elemMdv)
                mc.connectAttr('%s.outputX' % elemMdv, '%s.%s' % ('%sBsh_Geo' % eyeObj[0],each))
                mc.connectAttr('%s.%s' % (eyeCtrl, newNameBsh), '%s.input1X' % elemMdv)
            mc.select(clear=True)

def ExFaceProxy(part = '', side = '', count = '', jnt= '',mrr = False, *args):

    sel = mc.ls(sl=True,fl=True)
    countCheck = 0
    if '[0]' in sel[0]:
        countCheck = 1

    if jnt:
        if ':' in jnt:
            rn = jnt.split(':')[1]
            part = rn.split('_')[0]
        elif '_' in jnt:
            parts = jnt.split('_')[0]
            part = parts.replace(parts, part)
        if re.findall('\d+', part):
            count = ''
    mc.ExtractFace(mc.ls(sl=True))
    listObj = mc.ls(sl=True)
    objLisRe = mc.listRelatives(listObj, p = True)

    otherObj = listObj[-2]
    objSel = listObj[0]
    if len(objLisRe) == 2:
        if countCheck == 1:
            otherObj = listObj[-2]
            objSel = listObj[0]
        else:
            otherObj = listObj[-2]
            objSel = listObj[0]


    objPos = []
    objNeg = []
    if mrr == True:
        if not side == 'C':
            for obj in listObj:
                if obj == listObj[-2]:
                    break
                pos = mc.xform(obj, q=True ,boundingBox=True)
                if pos[0] < 0:
                    objNeg.append(obj)
                else:
                    objPos.append(obj)
        # else:

    if not objPos:
        objPos = []
        objNeg = []
    if not objNeg:
        objPos = []
        objNeg = []

    if objPos:
        findObjPos = mc.ls('%s*_ProxyGeo_%s_Grp' % (part, 'L'))
    if objNeg:
        findObjNeg = mc.ls('%s*_ProxyGeo_%s_Grp' % (part, 'R'))
    if side != '':
        findObj = mc.ls('%s*_ProxyGeo_%s_Grp' % (part, side))
    else:
        findObj = mc.ls('%s*_ProxyGeo_Grp' % (part))

    count = count
    # countObj = count + 1
    if objPos:
        if not findObjPos:
            checkList = mc.listRelatives(otherObj, p = True)
            mc.parent(otherObj, w = True)
            objLisRe = mc.listRelatives(objPos, p = True)
            if not len(objLisRe) == 1:
                objGeo = mc.polyUnite(objPos, n='%s_%s_Proxy_%s_Geo' % (part, count , 'L'))
                grpObj = mc.group (n = '%s_%s_ProxyGeo_%s_Grp' % (part, count, 'L'), em = True)
        
                objGeo = mc.ls(objGeo[0])
                pm.delete ( objGeo , ch = True ) ; 
                pm.delete ( otherObj , ch = True ) ; 
                mc.parent(objGeo, grpObj)
            else:
                objGeo = mc.ls(objPos[0])
                pm.delete ( objGeo , ch = True ) ; 
                pm.delete ( otherObj , ch = True ) ; 

                grpObj = mc.group (n = '%s_%s_ProxyGeo_%s_Grp' % (part, count, 'L'), em = True)
                nameObj = mc.rename(objGeo,'%s_%s_Proxy_%s_Geo' % (part, count , 'L')) 
                objGeo = nameObj

                mc.parent(nameObj, grpObj)     
                #mc.delete(checkList)

            if mc.objExists('%sProxyGeo_%s_Grp' % (part, 'L')):
                mc.parent(grpObj, '%sProxyGeo_%s_Grp' % (part, 'L'))
            else:
                grpMain = mc.group(n = '%sProxyGeo_%s_Grp' % (part, 'L'), em = True)
                mc.hide(grpMain)

                mc.parent(grpObj, grpMain)

        else:
            objGrp = findObjPos[-1]
            spObj = objGrp.split('_')[1]
            count =  int(spObj)   # split part/Number


            objGeo = objPos
            mc.parent (otherObj, w = True)
            pm.delete (otherObj, ch = True ) ; 
            objLisRe = mc.listRelatives(objGeo, p = True)

            if len(objLisRe) > 1:
                grpObj = mc.group (n = '%s_%s_ProxyGeo_%s_Grp' % (part, count+1 , 'L'), em = True)
                comObj = mc.polyUnite(objPos, n='%s_%s_Proxy_%s_Geo' % (part, count+1 , 'L'))
                                  
                geoObj = mc.ls(comObj[-2])   
                pm.delete (geoObj , ch = True ) ; 
                mc.parent(geoObj, grpObj)
            else:
                checkList = mc.listRelatives(objPos[0], p =True)
                objGeo = mc.ls(objPos[0])
                pm.delete ( objGeo , ch = True ) ; 

                grpObj = mc.group (n = '%s_%s_ProxyGeo_%s_Grp' % (part, count+1 , 'L'), em = True)
                nameObj = mc.rename(objGeo,'%s_%s_Proxy_%s_Geo' % (part, count+1 , 'L'))

                mc.parent(nameObj, grpObj)


            if mc.objExists('%sProxyGeo_%s_Grp' % (part, 'L')):
                mc.parent(grpObj, '%sProxyGeo_%s_Grp' % (part, 'L'))
            else:
                grpMain = mc.group(n = '%sProxyGeo_%s_Grp' % (part, 'L'), em = True)
                mc.hide(grpMain)
                mc.parent(grpObj, grpMain)

            if len(objLisRe) < 1:
                objGeo = objPos
                pm.delete ( objGeo , ch = True ) ; 
                grpObj = mc.group (n = '%s_%s_ProxyGeo_%s_Grp' % (part, count+1 , 'L'), em = True)
                mc.parent(objGeo, grpObj)
                if mc.objExists('%sProxyGeo_%s_Grp' % (part, 'L')):
                    mc.parent(grpObj, '%sProxyGeo_%s_Grp' % (part, 'L'))
                else:
                    grpMain = mc.group(n = '%sProxyGeo_%s_Grp' % (part, 'L'), em = True)
                    mc.hide(grpMain)
                    mc.parent(grpObj, grpMain)
        objPos = objGeo
        grpPos = grpObj
        grpPosMain = grpMain

    if objNeg:
        if not findObjNeg:
            #checkList = mc.listRelatives(otherObj, p = True)
            #mc.parent(otherObj, w = True)
            objLisRe = mc.listRelatives(objNeg, p = True)
            if not len(objLisRe) == 1:
                objGeo = mc.polyUnite(objNeg, n='%s_%s_Proxy_%s_Geo' % (part, count , 'R'))
                grpObj = mc.group (n = '%s_%s_ProxyGeo_%s_Grp' % (part, count, 'R'), em = True)
        
                objGeo = mc.ls(objGeo[0])
                pm.delete ( objGeo , ch = True ) ; 
                pm.delete ( otherObj , ch = True ) ; 
                mc.parent(objGeo, grpObj)
            else:
                objGeo = mc.ls(objNeg[0])
                pm.delete ( objGeo , ch = True ) ; 
                pm.delete ( otherObj , ch = True ) ; 

                grpObj = mc.group (n = '%s_%s_ProxyGeo_%s_Grp' % (part, count, 'R'), em = True)
                nameObj = mc.rename(objGeo,'%s_%s_Proxy_%s_Geo' % (part, count , 'R')) 
                objGeo = nameObj

                mc.parent(nameObj, grpObj)     
                mc.delete(checkList)

            if mc.objExists('%sProxyGeo_%s_Grp' % (part, 'R')):
                mc.parent(grpObj, '%sProxyGeo_%s_Grp' % (part, 'R'))
            else:
                grpMain = mc.group(n = '%sProxyGeo_%s_Grp' % (part, 'R'), em = True)
                mc.hide(grpMain)

                mc.parent(grpObj, grpMain)

        else:
            objGrp = findObjNeg[-1]
            spObj = objGrp.split('_')[1]
            count =  int(spObj)   # split part/Number 

            objGeo = objNeg
            pm.delete (otherObj, ch = True ) ; 
            objLisRe = mc.listRelatives(objGeo, p = True)

            if len(objLisRe) > 1:
                grpObj = mc.group (n = '%s_%s_ProxyGeo_%s_Grp' % (part, count+1 , 'R'), em = True)
                comObj = mc.polyUnite(objNeg, n='%s_%s_Proxy_%s_Geo' % (part, count+1 , 'R'))
                                  
                geoObj = mc.ls(comObj[-2])   
                pm.delete (geoObj , ch = True ) ; 
                mc.parent(geoObj, grpObj)
            else:
                checkList = mc.listRelatives(objNeg[0], p =True)
                objGeo = mc.ls(objNeg[0])
                pm.delete ( objGeo , ch = True ) ; 

                grpObj = mc.group (n = '%s_%s_ProxyGeo_%s_Grp' % (part, count+1 , 'R'), em = True)
                nameObj = mc.rename(objGeo,'%s_%s_Proxy_%s_Geo' % (part, count+1 , 'R'))

                mc.parent(nameObj, grpObj)
                mc.delete(checkList)

            if mc.objExists('%sProxyGeo_%s_Grp' % (part, 'R')):
                mc.parent(grpObj, '%sProxyGeo_%s_Grp' % (part, 'R'))
            else:
                grpMain = mc.group(n = '%sProxyGeo_%s_Grp' % (part, 'R'), em = True)
                mc.hide(grpMain)
                mc.parent(grpObj, grpMain)

            if len(objLisRe) < 1:
                objGeo = objNeg
                pm.delete ( objGeo , ch = True ) ; 
                grpObj = mc.group (n = '%s_%s_ProxyGeo_%s_Grp' % (part, count+1 , 'R'), em = True)
                mc.parent(objGeo, grpObj)
                if mc.objExists('%sProxyGeo_%s_Grp' % (part, 'R')):
                    mc.parent(grpObj, '%sProxyGeo_%s_Grp' % (part, 'R'))
                else:
                    grpMain = mc.group(n = '%sProxyGeo_%s_Grp' % (part, 'R'), em = True)
                    mc.hide(grpMain)
                    mc.parent(grpObj, grpMain)
        objNeg = objGeo
        grpNeg = grpObj
        grpNegMain = grpMain


    else:
        if not findObj:
            checkList = mc.listRelatives(otherObj, p = True)
            mc.parent(otherObj, w = True)
            objLisRe = mc.listRelatives(listObj, p = True)
            if not len(objLisRe) == 1:
                if side != '':
                    objGeo = mc.polyUnite(objLisRe, n='%s_%s_Proxy_%s_Geo' % (part, count , side))
                    grpObj = mc.group (n = '%s_%s_ProxyGeo_%s_Grp' % (part, count, side), em = True)
                else:
                    objGeo = mc.polyUnite(objLisRe, n='%s_%s_Proxy_Geo' % (part, count))    
                    grpObj = mc.group (n = '%s_%s_ProxyGeo_Grp' % (part, count), em = True)            
                objGeo = mc.ls(objGeo[0])
                pm.delete ( objGeo , ch = True ) ; 
                pm.delete ( otherObj , ch = True ) ; 
                mc.parent(objGeo, grpObj)
            else:
                objGeo = mc.ls(objSel)
                pm.delete ( objGeo , ch = True ) ; 
                pm.delete ( otherObj , ch = True ) ; 

                if side != '':
                    grpObj = mc.group (n = '%s_%s_ProxyGeo_%s_Grp' % (part, count, side), em = True)
                    nameObj = mc.rename(objGeo,'%s_%s_Proxy_%s_Geo' % (part, count , side)) 
                    objGeo = nameObj
                else:
                    grpObj = mc.group (n = '%s_%s_ProxyGeo_Grp' % (part, count), em = True)
                    nameObj = mc.rename(objGeo,'%s_%s_Proxy_Geo' % (part, count))    
                    objGeo = nameObj
                mc.parent(nameObj, grpObj)     
                mc.delete(checkList)

            if mc.objExists('%sProxyGeo_%s_Grp' % (part, side)):
                mc.parent(grpObj, '%sProxyGeo_%s_Grp' % (part, side))
            else:
                if side != '':
                    grpMain = mc.group(n = '%sProxyGeo_%s_Grp' % (part, side), em = True)
                    mc.hide(grpMain)
                
                else:
                    grpMain = mc.group(n = '%sProxyGeo_Grp' % part, em = True)
                    mc.hide(grpMain)
                mc.parent(grpObj, grpMain)

        else:
            objGrp = findObj[-1]
            spObj = objGrp.split('_')[1]
            counts = spObj.replace(part, '')
            if not count:
                count = 1
            count =  int(counts) # split part/Number
            objGeo = mc.ls(sl=True)
            mc.parent (otherObj, w = True)
            pm.delete (otherObj, ch = True ) ; 
            objLisRe = mc.listRelatives(objGeo, p = True)

            if len(objLisRe) > 1:
                if side != '':
                    grpObj = mc.group (n = '%s_%s_ProxyGeo_%s_Grp' % (part, count+1 , side), em = True)
                    comObj = mc.polyUnite(objLisRe, n='%s_%s_Proxy_%s_Geo' % (part, count+1 , side))
                else:
                    grpObj = mc.group (n = '%s_%s_ProxyGeo_Grp' % (part, count+1 ), em = True)
                    comObj = mc.polyUnite(objLisRe, n='%s_%s_Proxy_Geo' % (part, count+1))  
                                  
                geoObj = mc.ls(comObj[-2])   
                pm.delete (geoObj , ch = True ) ; 
                mc.parent(geoObj, grpObj)
            else:
                checkList = mc.listRelatives(objSel, p =True)
                objGeo = mc.ls(objSel)
                pm.delete ( objGeo , ch = True ) ; 
                if side != '':
                    grpObj = mc.group (n = '%s_%s_ProxyGeo_%s_Grp' % (part, count+1 , side), em = True)
                    nameObj = mc.rename(objGeo,'%s_%s_Proxy_%s_Geo' % (part, count+1 , side))

                else:
                    grpObj = mc.group (n = '%s_%s_ProxyGeo_Grp' % (part, count+1), em = True)
                    nameObj = mc.rename(objGeo,'%s_%s_Proxy_Geo' % (part, count+1))

                mc.parent(nameObj, grpObj)
                mc.delete(checkList)
            if side != '':   
                grpMain = '%sProxyGeo_%s_Grp' % (part, side)
                if mc.objExists(grpMain):
                    mc.parent(grpObj, '%sProxyGeo_%s_Grp' % (part, side))
                else:
                    grpMain = mc.group(n = '%sProxyGeo_%s_Grp' % (part, side), em = True)
                    mc.hide(grpMain)
                    mc.parent(grpObj, grpMain)
            else:
                if mc.objExists('%sProxyGeo_Grp' % part):
                    mc.parent(grpObj, '%sProxyGeo_Grp' % part)
                else:
                    grpMain = mc.group(n = '%sProxyGeo_Grp' % part, em = True)
                    mc.hide(grpMain)
                    mc.parent(grpObj, grpMain)

            if len(objLisRe) < 1:
                objGeo = mc.ls(sl=True)
                pm.delete ( objGeo , ch = True ) ; 
                grpObj = mc.group (n = '%s_%s_ProxyGeo_%s_Grp' % (part, count+1 , side), em = True)
                mc.parent(objGeo, grpObj)
                if mc.objExists('%sProxyGeo_%s_Grp' % (part, side)):
                    mc.parent(grpObj, '%sProxyGeo_%s_Grp' % (part, side))
                else:
                    grpMain = mc.group(n = '%sProxyGeo_%s_Grp' % (part, side), em = True)
                    mc.hide(grpMain)
                    mc.parent(grpObj, grpMain)

    ## Mirror Geo
    if mrr == True:
        if not side == 'C':
            name, side, lastName = splitName(sels = [grpMain])
            if side == '_L_':
                side = side.replace(side, '_R_')
            elif side == '_R_':
                side = side.replace(side, '_L_')

            newGrpSide = '{}{}{}'.format(name, side, lastName)

            ## Duplicate Obj for mirror
            countNewGrpMrr = grpObj.split('_')
            nameNew = countNewGrpMrr[0]
            countNew = countNewGrpMrr[1]
            proxyGeoNew = countNewGrpMrr[2]
            sideNew = countNewGrpMrr[3]
            lastNameNew = (countNewGrpMrr[-1]).replace('1', '')

            new = '%s_%s_ProxyGeo%sGrp' % (part, countNew , side)
            newGrpMrr = mc.duplicate(grpObj,n = new)[0]

            geoMrr = mc.listRelatives(newGrpMrr, c = True, f = True)
            for each in geoMrr:
                eachObj = each.split('|')[-1]
                if '_L_' in eachObj:
                    newName = eachObj.replace('_L_', '_R_')
                    mc.rename(each, newName)
                elif '_R_' in eachObj:
                    newName = eachObj.replace('_R_', '_L_')
                    mc.rename(each, newName)

            mc.setAttr('{}.sx'.format(newGrpMrr), -1)
            mc.makeIdentity (newGrpMrr , apply = True , jointOrient = False, normal = 1, translate =False, rotate = False, scale = True) 
            geoAll = mc.listRelatives(newGrpMrr, c = True)
            for each in geoAll:
                pass

            if not mc.objExists(newGrpSide):
                newGrpSide = mc.group(n = newGrpSide, em = True)

            mc.parent(newGrpMrr, newGrpSide)
            mc.setAttr('{}.v'.format(newGrpSide), 0)

        else:
            name, side, lastName = splitName(sels = [grpMain])

            newGrpSide = '{}{}{}'.format(name, side, lastName)

            ## Duplicate Obj for mirror
            countNewGrpMrr = grpObj.split('_')
            nameNew = countNewGrpMrr[0]
            countNew = countNewGrpMrr[1]
            proxyGeoNew = countNewGrpMrr[2]
            sideNew = countNewGrpMrr[3]
            lastNameNew = (countNewGrpMrr[-1]).replace('1', '')

            new = '%s_%s_ProxyGeo%sGrp' % (part, int(countNew) + 1 , side)
            newGrpMrr = mc.duplicate(grpObj,n = new)[0]

            mc.setAttr('{}.sx'.format(newGrpMrr), -1)
            mc.makeIdentity (newGrpMrr , apply = True , jointOrient = False, normal = 1, translate =False, rotate = False, scale = True) 
            geoAll = mc.listRelatives(newGrpMrr, c = True)
            for each in geoAll:
                pass

            if not mc.objExists(newGrpSide):
                newGrpSide = mc.group(n = newGrpSide, em = True)

            mc.setAttr('{}.v'.format(newGrpSide), 0)

    if jnt != '':
        if mrr:
            if not side == '_C_':
                if '_L_' in jnt:
                    jntL = jnt
                    jntR = jnt.replace('_L_','_R_')
                elif '_R_' in jnt:
                    jntR = jnt
                    jntL = jnt.replace('_R_','_L_')
                mc.parentConstraint(jntL, grpObj, mo=True)
                mc.scaleConstraint(jntL, grpObj, mo=True)
                mc.parentConstraint(jntR, newGrpMrr, mo=True)
                mc.scaleConstraint(jntR, newGrpMrr, mo=True)
            else:
                mc.parentConstraint(jnt, grpObj, mo=True)
                mc.scaleConstraint(jnt, grpObj, mo=True)
                mc.parentConstraint(jnt, newGrpMrr, mo=True)
                mc.scaleConstraint(jnt, newGrpMrr, mo=True)       
        else:
            mc.parentConstraint(jnt, grpObj, mo=True)
            mc.scaleConstraint(jnt, grpObj, mo=True)

    otherObj = mc.rename(otherObj,'cut_Geo')

    print '::::::::::::::::::::::::::::::::::::::: CUT GEO FINISH :::::::::::::::::::::::::::::::::::::::'
    print '::::::::::::::::::::::::::::::::::::::: CUT GEO FINISH :::::::::::::::::::::::::::::::::::::::'

def extractFace (*args):
    mc.ExtractFace(mc.ls(sl=True))
    obj = mc.listRelatives(mc.ls(sl=True), p = True)
    objxx = mc.listRelatives(obj,c = True)
    mc.parent(objxx[-2], w = True)

def layerControlRun(ns = '', obj = '', shape = True, *args):
    mainAttrLayerVis = listControlVis.layerControlAttr()
    mins = 0 
    maxs = 1
    shapes = 'Shape'
    ctrlShape = '%s%s_Crv%s' % (ns, obj, shapes)
    ctrl = '%s%s_Crv' % (ns, obj)
    if shape  == True:
        if obj:
            for each in mainAttrLayerVis:
                ## Create Attrs Control Visibility
                attrNameMinMax(obj = ctrlShape, elem = each, min = mins, max = maxs )
                mc.setAttr('%s%s_Crv.%s' % (ns, obj, each), l = True)
                for attrs in mainAttrLayerVis[each]:
                    attrNameMinMax(obj = ctrlShape, elem = attrs, min = mins, max = maxs )
                    mc.setAttr('%s_Crv%s.%s' % (obj, shapes, attrs), 1)

    else:
        if obj:
            for each in mainAttrLayerVis:
                ## Create Attrs Control Visibility
                attrNameMinMax(obj = ctrl, elem = each, min = mins, max = maxs )
                mc.setAttr('%s%s_Crv.%s' % (ns, obj, each), l = True)
                for attrs in mainAttrLayerVis[each]:
                    attrNameMinMax(obj = ctrl, elem = attrs, min = mins, max = maxs )
                    mc.setAttr('%s_Crv.%s' % (obj, attrs), 1)

    ## Unlock Group for SetAttr layerControl
    ## Generate >> Unlock Group For SetAttr layerControl
    layerControlUnlock()

def createAmpEye(ns = '', process = '', obj = '', shape = True, min = 0, max = 50, *args):
    titalListAttr, listAttrL, listAttrR, eyeRigAmp= lcaf.selectControlAmp(process = process)
    mins = min 
    maxs = max
    shapes = 'Shape'
    ctrlShape = '%s%s%s' % (ns, obj, shapes)
    ctrl = '%s%s' % (ns, obj)
    i = 0 
    if shape  == True:
        if obj:
            ## Create Attrs Control Visibility
            attrNameMinMax(obj = ctrlShape, elem = titalListAttr, min = mins, max = maxs )
            mc.setAttr('%s%s%s.%s' % (ns, obj, shapes, titalListAttr), l = True)
            for each in listAttrL:
                attrNameMinMax(obj = ctrlShape, elem = each, min = mins, max = maxs )
                mc.setAttr('%s%s.%s' % (obj, shapes, each), eyeRigAmp[each])
            i += 1
    else:
        if obj:
            ## Create Attrs Control Visibility
            attrNameMinMax(obj = ctrl, elem = titalListAttr, min = mins, max = maxs )
            mc.setAttr('%s%s.%s' % (ns, obj, titalListAttr), l = True)
            for each in listAttrL:
                attrNameMinMax(obj = ctrl, elem = each, min = mins, max = maxs )
                mc.setAttr('%s.%s' % (obj, each), eyeRigAmp[each])
            i += 1

## Get Path File and Open File
def openFileWorldSpace(*args):
    path = mc.file(q=True, sn=True)
    mc.file(path, o=True, f=True)

def connectLayerControlVis(obj = '', shape = True, *args):    
    mainAttrLayerVis = listControlVis.layerControlAttr()
    objLayerVis = listControlVis.listControlVis()
    shapes = 'Shape'
    if shape == True:
        objCtrl = '%s_Crv%s' % (obj, shapes)
    else:
        objCtrl = '%s_Crv' % obj
    for each in mainAttrLayerVis:
        for sel in mainAttrLayerVis[each]:
            for objLists in objLayerVis[sel]:
                RfObj = mc.objExists('{}'.format(objLists))
                if RfObj == True:
                    objLists = '{}'.format(objLists)
                else:
                    objLists = objLists   
                
                checkObj = mc.objExists(objLists)
                if checkObj == True:
                    attrC = mc.listConnections('{}.v'.format(objLists),s=True)
                    if mc.objExists(objCtrl):
                        if not attrC == None:
                            if 'RN' in attrC[0]:
                                mc.connectAttr('{}.{}'.format(objCtrl, sel), '{}.v'.format(objLists) ,f=True )
                            else:
                                pass
                        else:
                            mc.connectAttr('{}.{}'.format(objCtrl, sel), '{}.v'.format(objLists) )
                else:
                    pass


def checkListConnectionVis(*args):
    fileName = mc.file(q=True,sn=True)
    charName = fileName.split('/')[5]
    dtlGrp = ['DtlRigCtrlHi_Grp','DtlRigCtrlMid_Grp','DtlRigCtrlLow_Grp']
    checkDtlGrp = mc.objExists('fclRig:{}'.format(dtlGrp[0]))
    if checkDtlGrp == True:
        for a in dtlGrp:
            attrC = mc.listConnections('*:{}.v'.format(a),s=True)
            if not attrC == None:
                print '{}.v has already connected'.format(a)
            else:
                mc.connectAttr('*:{}_Crv.DetailVis'.format(charName),'*:{}.v'.format(a))
                print 'connect {}.v'.format(a)
    else:
        print 'cannot found Dtl Grp >> pass connect'

def layerControlUnlock(*args):
    objLayerVis= listControlVis.listControlVis()
    for each in objLayerVis:
        for sel in objLayerVis[each]:
            try:
                mc.setAttr('%s.visibility' % sel, l = False)
            except:
                pass

def nonRollFk(      elem = '',
                    side = '',
                    objAttrs = '', 
                    source = '', 
                    target = '',
                    valueAmp = 0.8,
                    mins = 0,
                    maxs = 90,
                    axisSource = '',
                    axisTarget = '',
                    rotate = True,
                    translate = False,
                    scale = False,
                    # selected = False,
                    # selsObj = '',
                    shape = False,
                    scaleProject = 0.1 , 
                     *args):
    try:
        lockAttrObj(listObj = [target],lock = False)
    except:
        pass
    if rotate == True:
        attsObj  = 'rotate'
        parentList = mc.listConnections('%s.%s%s' % (target, attsObj, axisTarget), s = True, type = 'unitConversion')

    if translate == True:
        attsObj = 'translate'
        parentList = mc.listConnections('%s.%s%s' % (target, attsObj, axisTarget), s = True)

    if scale == True:
        attsObj = 'scale'
        parentList = mc.listConnections('%s.%s%s' % (target, attsObj, axisTarget), s = True)

    # if selected == True:
    #     attsObj = selsObj
    #     parentList = mc.listConnections('%s.%s%s' % (target, attsObj, axisTarget), s = True)

    if side:
        objAttrsList = '%s_%s' % (elem, side)
    else:
        side = 'C'
        objAttrsList = '%s_%s' % (elem, side)

    onOffAtr = 'AutoFk'

    if shape:
        objAttrss = objAttrs + 'Shape'
    else:
        objAttrss = objAttrs

    listAttrObj = mc.listAttr(objAttrs, s = True, r = True, k = True)
    ## Create MultiplyDivice 1
    elemMdv = mc.createNode('multiplyDivide', n = '%s_%s_%s_NonRoll_Mdv' % (elem, side,attsObj))
    mc.connectAttr('%s.rotate%s' % (source, axisSource), '%s.input1X' % elemMdv)
    if onOffAtr in listAttrObj:
        ## Connect Attr OnOff
        mc.connectAttr('%s.%s' % (objAttrs, onOffAtr), '%s.input2X' % elemMdv)
    else:
        ## Create OnOff
        attrNameMinMax (obj = objAttrs, elem = onOffAtr, min = 0, max = 1)
        mc.setAttr("%s.%s" % (objAttrs, onOffAtr), 1)
        ## Connect Attr OnOff
        mc.connectAttr('%s.%s' % (objAttrs, onOffAtr), '%s.input2X' % elemMdv)

    if objAttrss:
        # listAttrObj = mc.listAttr(objAttrss, s = True, r = True, k = True)

        ## Create Attr
        attrName(obj = objAttrss, elem = objAttrsList)   
        mc.setAttr("%s.%s" % (objAttrss, objAttrsList), valueAmp)

        ## Create Clamp
        elemClm = mc.createNode('clamp', n = '%s_%s_%s_NonRoll_Clm' % (elem, side,attsObj))
        mc.setAttr ('%s.minR' % elemClm, mins)
        mc.setAttr ('%s.maxR' % elemClm, maxs)
        mc.connectAttr('%s.outputX' % elemMdv, '%s.inputR' % elemClm)

        if rotate :
            ## Create MultiplyDivice 2 Rotate
            elemDvMdv = mc.createNode('multiplyDivide', n = '%s_%s_%s_DvNonRoll_Mdv' % (elem, side , attsObj))
            # elemDvAmpMdv = mc.createNode('multiplyDivide', n = '%s_%s_AmpAttrNonRoll_Mdv' % (elem, side))
            # mc.setAttr('%s.input2X' % elemDvAmpMdv, 0.01)
            mc.connectAttr('%s.%s' % (objAttrss,objAttrsList), '%s.input2X' % elemDvMdv)
            #mc.connectAttr('%s.%s' % (objAttrss,objAttrsList), '%s.input1X' % elemDvAmpMdv)
            #mc.connectAttr('%s.outputX' % (elemDvAmpMdv), '%s.input2X' % elemDvMdv)
            # mc.connectAttr('%s.%s' % (objAttrss,objAttrsList), '%s.input1X' % elemDvAttrMdv)
            # mc.connectAttr('%s.outputX' % elemDvAttrMdv, '%s.input2X' % elemDvMdv)
            mc.connectAttr('%s.outputR' % elemClm,  '%s.input1X' % elemDvMdv)
            if parentList :
                ll = mc.listConnections('%s.%s%s' % (target, attsObj, axisTarget), s = True, type = 'unitConversion')
                ken = mc.listConnections( ll, s = True, type = 'plusMinusAverage')
                zz = mc.listConnections(ken[0],d = False, c = True )

                ## fine input2D
                ken2 = zz[-2].split('.')
                objInput = ken2[-2]
                num = objInput.split('[')[1].replace(']','')
                 
                ken3 = int(num)+1
                renameKen4 = 'input2D[%s]' % ken3
   

                mc.connectAttr('%s.outputX' % elemDvMdv, '%s.%s.input2Dx' % (ken2[0], renameKen4))

            else:
                ## Create PlusMinusAverage 
                elemPma = mc.createNode( 'plusMinusAverage', n = '%s_%s_%s_NonRoll_Pma' % (elem, side , attsObj))
                mc.connectAttr('%s.outputX' % elemDvMdv, '%s.input2D[0].input2Dx' % elemPma)
                mc.connectAttr('%s.output2Dx' % elemPma, '%s.%s%s' % (target, attsObj, axisTarget))

        if translate:
            ## Create MultiplyDivice 2 Translate
            elemDvMdv = mc.createNode('multiplyDivide', n = '%s_%s_%s_DvNonRoll_Mdv' % (elem, side , attsObj))
            elemDvAttrMdv = mc.createNode('multiplyDivide', n = '%s_%s_%s_DvAttrNonRoll_Mdv' % (elem, side , attsObj))
            mc.setAttr('%s.input2X' % elemDvAttrMdv, scaleProject)
            # mc.connectAttr('%s.%s' % (objAttrss,objAttrsList), '%s.input2X' % elemDvMdv)
            mc.connectAttr('%s.%s' % (objAttrss,objAttrsList), '%s.input1X' % elemDvAttrMdv)
            mc.connectAttr('%s.outputX' % elemDvAttrMdv, '%s.input2X' % elemDvMdv)
            mc.connectAttr('%s.outputR' % elemClm,  '%s.input1X' % elemDvMdv)
            if parentList:
                cnnctList = mc.listConnections(parentList[0],d = False, c = True )

                ## fine input2D
                pmaInput = cnnctList[-2].split('.')

                objInput = ''
                for each in pmaInput:
                    if 'input2D[' in each:
                        objInput = each
                    else:
                        pass
                if not objInput:
                    objInput = 'input2D[1]'

                num = objInput.split('[')[1].replace(']','')              
                numInput = int(num)+1
                inputSource = 'input2D[%s]' % numInput

                mc.connectAttr('%s.outputX' % elemDvMdv, '%s.%s.input2Dx' % (pmaInput[0], inputSource))

            else:
                ## Create PlusMinusAverage 
                elemPma = mc.createNode( 'plusMinusAverage', n = '%s_%s_%s_NonRoll_Pma' % (elem, side , attsObj))
                mc.connectAttr('%s.outputX' % elemDvMdv, '%s.input2D[0].input2Dx' % elemPma)
                mc.connectAttr('%s.output2Dx' % elemPma, '%s.%s%s' % (target, attsObj, axisTarget))

        if scale:
            ## Create MultiplyDivice 2 Translate
            elemDvMdv = mc.createNode('multiplyDivide', n = '%s_%s_%s_DvNonRoll_Mdv' % (elem, side , attsObj))
            elemDvAttrMdv = mc.createNode('multiplyDivide', n = '%s_%s_%s_DvAttrNonRoll_Mdv' % (elem, side , attsObj))
            mc.setAttr('%s.input2X' % elemDvAttrMdv, 0.1)
            # mc.connectAttr('%s.%s' % (objAttrss,objAttrsList), '%s.input2X' % elemDvMdv)
            mc.connectAttr('%s.%s' % (objAttrss,objAttrsList), '%s.input1X' % elemDvAttrMdv)
            mc.connectAttr('%s.outputX' % elemDvAttrMdv, '%s.input2X' % elemDvMdv)
            mc.connectAttr('%s.outputR' % elemClm,  '%s.input1X' % elemDvMdv)
            if parentList:
                zz = mc.listConnections(parentList[0],d = False, c = True )

                ## fine input2D
                ken2 = zz[-2].split('.')
                objInput = ken2[-2]
                ken3 = int(objInput[-2])+1
                renameKen4 = 'input2D[%s]' % ken3
                mc.addAttr(ken2[0], ln='default')
                mc.setAttr(ken2[0]+'.default' , 1)
                mc.connectAttr(ken2[0]+'.default' , '%s.%s.input2Dx' % (ken2[0], renameKen4))

                mc.connectAttr('%s.outputX' % elemDvMdv, '%s.%s.input2Dx' % (ken2[0], renameKen4+1))

            else:
                ## Create PlusMinusAverage 
                elemPma = mc.createNode( 'plusMinusAverage', n = '%s_%s_%s_NonRoll_Pma' % (elem, side , attsObj))
                mc.addAttr(elemPma, ln='default')
                mc.setAttr(elemPma+'.default' , 1)
                mc.connectAttr(elemPma+'.default', '%s.input2D[0].input2Dx' % elemPma)

                mc.connectAttr('%s.outputX' % elemDvMdv, '%s.input2D[1].input2Dx' % elemPma)
                mc.connectAttr('%s.output2Dx' % elemPma, '%s.%s%s' % (target, attsObj, axisTarget))


        # if selected:
        #     ## Create MultiplyDivice 2 Translate
        #     elemDvMdv = mc.createNode('multiplyDivide', n = '%s_%s_DvNonRoll_Mdv' % (elem, side))
        #     elemDvAttrMdv = mc.createNode('multiplyDivide', n = '%s_%s_DvAttrNonRoll_Mdv' % (elem, side))
        #     mc.setAttr('%s.input2X' % elemDvAttrMdv, 0.1)
        #     # mc.connectAttr('%s.%s' % (objAttrss,objAttrsList), '%s.input2X' % elemDvMdv)
        #     mc.connectAttr('%s.%s' % (objAttrss,objAttrsList), '%s.input1X' % elemDvAttrMdv)
        #     mc.connectAttr('%s.outputX' % elemDvAttrMdv, '%s.input2X' % elemDvMdv)
        #     mc.connectAttr('%s.outputR' % elemClm,  '%s.input1X' % elemDvMdv)
        #     if parentList:
        #         zz = mc.listConnections(parentList[0],d = False, c = True )

        #         ## fine input2D
        #         ken2 = zz[-2].split('.')
        #         objInput = ken2[-2]
        #         ken3 = int(objInput[-2])+1
        #         renameKen4 = 'input2D[%s]' % ken3

        #         mc.connectAttr('%s.outputX' % elemDvMdv, '%s.%s.input2Dx' % (ken2[0], renameKen4))

        #     else:
        #         ## Create PlusMinusAverage 
        #         elemPma = mc.createNode( 'plusMinusAverage', n = '%s_%s_NonRoll_Pma' % (elem, side))
        #         mc.connectAttr('%s.outputX' % elemDvMdv, '%s.input2D[0].input2Dx' % elemPma)
        #         mc.connectAttr('%s.output2Dx' % elemPma, '%s.%s%s' % (target, attsObj, axisTarget))

def nonRollFkAll(   elem = [],
                target = [],
                source = '', 
                objAttrs = '', 
                shape = True,
                side = '',
                valueAmp = 0,
                mins = 0,
                maxs = 90,
                axisSource = '',
                rotate = True,
                translate = False,
                scale = False,
                scaleProject = 0.1 ,
                *args):

    if rotate == True:
        for each in range(len(target)):
            nonRollFk(          elem = '{}_rotateX'.format(elem[each]),
                            side = side,
                            objAttrs = objAttrs, 
                            source = source, 
                            target = target[each],
                            valueAmp = 0,
                            mins = mins,
                            maxs = maxs,
                            axisSource = axisSource,
                            axisTarget = 'X',
                            rotate = True,
                            translate = False,
                            scale = False,
                            shape = True,
                            scaleProject = scaleProject ,
                            )

            nonRollFk(          elem = '{}_rotateY'.format(elem[each]),
                            side = side,
                            objAttrs = objAttrs, 
                            source = source, 
                            target = target[each],
                            valueAmp = 0,
                            mins = mins,
                            maxs = maxs,
                            axisSource = axisSource,
                            axisTarget = 'Y',
                            rotate = True,
                            translate = False,
                            scale = False,
                            shape = True,
                            scaleProject = scaleProject ,
                            )

            nonRollFk(          elem = '{}_rotateZ'.format(elem[each]),
                            side = side,
                            objAttrs = objAttrs, 
                            source = source, 
                            target = target[each],
                            valueAmp = 0,
                            mins = mins,
                            maxs = maxs,
                            axisSource = axisSource,
                            axisTarget = 'Z',
                            rotate = True,
                            translate = False,
                            scale = False,
                            shape = True,
                            scaleProject = scaleProject ,
                            )

    if translate == True:
        for each in range(len(target)):
            nonRollFk(          elem = '{}_translateX'.format(elem[each]),
                            side = side,
                            objAttrs = objAttrs, 
                            source = source, 
                            target = target[each],
                            valueAmp = 0,
                            mins = mins,
                            maxs = maxs,
                            axisSource = axisSource,
                            axisTarget = 'X',
                            rotate = False,
                            translate = True,
                            scale = False,
                            shape = True,
                            scaleProject = scaleProject ,
                            )

            nonRollFk(          elem = '{}_translateY'.format(elem[each]),
                            side = side,
                            objAttrs = objAttrs, 
                            source = source, 
                            target = target[each],
                            valueAmp = 0,
                            mins = mins,
                            maxs = maxs,
                            axisSource = axisSource,
                            axisTarget = 'Y',
                            rotate = False,
                            translate = True,
                            scale = False,
                            shape = True,
                            scaleProject = scaleProject ,
                            )

            nonRollFk(          elem = '{}_translateZ'.format(elem[each]),
                            side = side,
                            objAttrs = objAttrs, 
                            source = source, 
                            target = target[each],
                            valueAmp = 0,
                            mins = mins,
                            maxs = maxs,
                            axisSource = axisSource,
                            axisTarget = 'Z',
                            rotate = False,
                            translate = True,
                            scale = False,
                            shape = True,
                            scaleProject = scaleProject ,
                            )

    if scale == True:
        for each in range(len(target)):
            nonRollFk(          elem = '{}_scaleX'.format(elem[each]),
                            side = side,
                            objAttrs = objAttrs, 
                            source = source, 
                            target = target[each],
                            valueAmp = 0,
                            mins = mins,
                            maxs = maxs,
                            axisSource = axisSource,
                            axisTarget = 'X',
                            rotate = False,
                            translate = False,
                            scale = True,
                            shape = True,
                            scaleProject = scaleProject ,
                            )

            nonRollFk(          elem = '{}_scaleY'.format(elem[each]),
                            side = side,
                            objAttrs = objAttrs, 
                            source = source, 
                            target = target[each],
                            valueAmp = 0,
                            mins = mins,
                            maxs = maxs,
                            axisSource = axisSource,
                            axisTarget = 'Y',
                            rotate = False,
                            translate = False,
                            scale = True,
                            shape = True,
                            scaleProject = scaleProject ,
                            )

            nonRollFk(          elem = '{}_scaleZ'.format(elem[each]),
                            side = side,
                            objAttrs = objAttrs, 
                            source = source, 
                            target = target[each],
                            valueAmp = 0,
                            mins = mins,
                            maxs = maxs,
                            axisSource = axisSource,
                            axisTarget = 'Z',
                            rotate = False,
                            translate = False,
                            scale = True,
                            shape = True,
                            scaleProject = scaleProject ,
                            )

# example
# nonRollFkAll(   elem = ['Cover_Fnt','CoverA_Fnt','CoverB_Fnt','CoverC_Fnt','CoverD_Fnt','CoverBck_Fnt'],
#                 target = ['CoverFnt2CtrlOfst_Grp','CoverA2CtrlOfst_Grp','CoverB2CtrlOfst_Grp','CoverC2CtrlOfst_Grp','CoverD2CtrlOfst_Grp','CoverBck2CtrlOfst_Grp'],
#                 source = 'UpArmNonRoll_L_Jnt', 
#                 objAttrs = 'CoverFnt1_Ctrl', 
#                 shape = True,
#                 side = 'L',
#                 valueAmp = 0,
#                 mins = 0,
#                 maxs = 90,
#                 axisSource = 'X',
#                 rotate = True,
#                 translate = False)

def fixBagArthur (elem = '', side = '', *args):
    elemPma = mc.createNode( 'plusMinusAverage', n = '%s_%s_NonRoll_Pma' % (elem, side))
    mc.connectAttr('%s.translateY' % 'BagScissor4_R_Ctrl',  '%s.input2D[0].input2Dx' % elemPma)
    mc.connectAttr('%s.output2Dx' % elemPma, '%s.input1D[2]' % 'BagScissor3Sqsh_R_Pma')

def setReamCharScale(char = '', *args):
    placeMent = 'Place_Ctrl'
    listsCharName , controlObj, attrScale = listsAsset.listsReamCharScale()
    for each in listsCharName:
        if each == char:
            mc.setAttr('{}.{}'.format(placeMent, attrScale), listsCharName[each])
            lockAttr(listObj = [placeMent], attrs = [attrScale],lock = True, keyable = False)

def setDefaultControl(char = '', *args):
    asset = context_info.ContextPathInfo() 
    charName = asset.name
    lists = listControlVis.checkListsDefaultAttr()
    ## SetAttr
    ArmList = ['Arm_L_Ctrl', 'Arm_R_Ctrl']
    SpineList = ['Spine_Ctrl']
    LegList = ['Leg_L_Ctrl', 'Leg_R_Ctrl']
    GrpHideList = ['Skin_Grp', 'Jnt_Grp', 'Ikh_Grp', 'Still_Grp', 'TechGeo_Grp']
    GrpShowList = ['Ctrl_Grp']
    localWorldAttr = ['Head_Ctrl', 'UpArmFk_L_Ctrl', 'UpArmFk_R_Ctrl']
    MdPrGeo = ['MdGeo_Grp', 'PrGeo_Grp']


    for each in lists:
        if not charName == each:

            ## Arm
            for eachArm in ArmList:
                if mc.objExists('%s.fkIk' % eachArm):
                    mc.setAttr('%s.fkIk' % eachArm, 0)

            ## Spine
            for eachSpine in SpineList:
                if mc.objExists('%s.fkIk' % eachSpine):
                    mc.setAttr('%s.fkIk' % eachSpine, 0)

            ## Leg
            for eachLeg in LegList:
                if mc.objExists('%s.fkIk' % eachLeg):
                    mc.setAttr('%s.fkIk' % eachLeg, 1)

    ## SetAttr AllMover Vis
    attrCtrlVis = listControlVis.layerControlAttr()
    AllMoverObj = '%s_Crv' % char
    if mc.objExists(AllMoverObj):
        for each in attrCtrlVis:
            for sels in attrCtrlVis[each]:
                if sels:
                    if sels == 'MasterVis':
                        mc.setAttr('%s.%s' % (AllMoverObj,'MasterVis'), 1)
                    elif sels == 'GeoVis':
                        mc.setAttr('%s.%s' % (AllMoverObj,'GeoVis'), 1)                
                    else:
                        mc.setAttr('%s.%s' % (AllMoverObj,sels), 0)

    ## SetAttr Visibility
    for each in GrpHideList:
        if mc.objExists(each):
            mc.setAttr('%s.v' % each, 0)
    for each in GrpShowList:
        if mc.objExists(each):
            mc.setAttr('%s.v' % each, 1)
    for each in MdPrGeo:
        if mc.objExists(each):
            if mc.objExists('PrGeo_Grp'):
                if mc.listConnections('PrGeo_Grp.v',s=True):
                    pass
            else:
                mc.setAttr('%s.v' % each, 1)     
               
    ## SetAttr LocalWorld
    # except dragon from ThreeEyes
    for each in localWorldAttr:
        if mc.objExists('%s.localWorld' % each):
            mc.setAttr('%s.localWorld' % each, 1)

    # if charName == 'dragon':
    #     if asset.project == 'ThreeEyes'
    #         if mc.objExists(each):
    #             mc.setAttr('%s.localWorld' % each, 0)

    ## Lock Attribute Visibility 'Rig_Grp'
    lockAttr(listObj = ['Rig_Grp'], attrs = ['visibility'],lock = True, keyable = False)

    ## turn off visibility wrinkledRig
    allMidWrinkle = mc.ls("*MidWrinkledRig*", typ="nurbsCurve")
    # allMidWrinkle = mc.ls("*:*MidWrinkledRig*", typ="nurbsCurve")
    for shape in allMidWrinkle:
        if "Gmbl" not in shape:
            wrinkledCrv = mc.listRelatives(shape, ap=True)[0]
            mc.setAttr(wrinkledCrv + ".wrinkledVis", 0)

    ## lock Atribute Amp Facial
    projectName = checkProject()
    # print projectName, '...projectName'
    if projectName in 'CA':
        lockAttrAmpCharacter()

def combineCurves(char = '', curves = []):
    crvGrp = mc.group(n = '%s_Crv' % char, em=True)
    for crv in curves:
        crvShape = mc.listRelatives(crv, shapes = True)
        mc.parent(crvShape,crvGrp,s=True,r=True, relative=True)
        if curves[-1]:
            mc.parent(crv, crvGrp)
        mc.delete(crv) 
    ## Rename CurveShape1 >> CharNameShape
    mc.rename ("curveShape1","%sShape" % crvGrp)   
    ## LockAttr 
    lockAttrObj(listObj = [crvGrp], lock = True) 
    mc.setAttr(crvGrp + '.overrideEnabled', 1)
    mc.setAttr(crvGrp + '.overrideColor', 19)
    pm.delete ( crvGrp , ch = True )
    return  crvGrp

def textName(char = '', size = '',*args):
    if char == '':
        ## find charName
        asset = context_info.ContextPathInfo()         
        char = asset.name
        if char == '':
            print 'Please Check File Path'


    fontText = 'MS Shell Dlg 2, 27.8pt'
    txtCrv = mc.textCurves( n = '%s_' %char , f = fontText , t = char )
    listChi = mc.listRelatives(txtCrv[0], type = 'transform', c = True, ad = True, s = False)
    crvGrp = mc.createNode ('transform', n = '%sCrv_Grp' % char, )
    mc.select(cl = True)
    listGrp =[]
    listCrvObj = []
    for each in listChi:
        if '_1' in each :
            listConObj = mc.listConnections(each, s = True)
            listParObj = mc.listConnections(each,d = False, c = True ,p  = True)
            listSplitObj = listParObj[-1].split('.')
            objConnect = listSplitObj[-1]
            listNum = int(objConnect[-2])
            positionCon = 'position[%s]' % listNum

            listGrp.append(each)
        if 'curve' in each:
            listCrvObj.append(each)        

    mc.parent(listCrvObj, w = True)
    mc.select(crvGrp, add = True)
    mc.parent(listCrvObj, crvGrp)   
    for each in listCrvObj:       
        if 'curve' in each:
            ## Freeztransform
            mc.makeIdentity (each , apply = True , jointOrient = False, normal = 1, translate =True, rotate = True, scale = True) 
            ## Reset Transform
            mc.move("%s.scalePivot" % each,"%s.rotatePivot" % each, absolute=True) 
    ## Combine Curve
    crvUse = combineCurves(char = char, curves = listCrvObj)
    mc.parent(crvUse, crvGrp)
    mc.setAttr('%s.s' % crvGrp, float(size),float(size),float(size))
    mc.xform(crvGrp, cp=1)
    mc.delete('%s_Shape' % char)
    return crvGrp

def tyHeadEnd(char = '', value = '', objPar = 'HeadEnd_Jnt', *args):
    textName = '%sCrv_Grp' % char
    if objPar:
        mc.delete(mc.parentConstraint(objPar, textName))
    lists = mc.xform(textName, q=True, t=True) 
    values =  lists[1]+float(value)
    mc.setAttr('%s.ty' % textName, values)
    # mc.setAttr('%s.tx' % textName, -1)
    # return values

def copyOneToTwoSkinWeight(*args):
    listaA = mc.ls(sl=True)
    listsB = mc.ls(sl=True)
    i = 0
    for i in range(len(listsB)):
        mc.select(listaA[i])
        mc.select(listsB[i], add = True)
        weightTools.copySelectedWeight()
        # print listaA[i], '>>' , listsB[i]
    i += 1

def copySkinWeightToPosition(*args):
    lists = mc.ls(sl=True)
    for each in lists:
        mc.select(each)
        mc.select('Fur1_Geo_Proxy3' , add = True)
        lrr.copyWeightBasedOnWorldPosition()

def addFurGuide(BodyGeo = '', BodyFurGeo = '', BodyFclRigWrap = '', *args):
    # Bsh Body_Geo to BodyDtlCtrl  
    stillRigGrp = mc.ls("*:Still_Grp")

    if BodyGeo == '':
        BodyGeo = mc.ls("*:Body_Geo")
        BodyFurGeo = mc.ls("*:BodyFur_Geo")
        BodyFclRigWrap = mc.ls("BodyFclRigWrapped_Geo")

    # Duplicate BodyFurFcl_Geo, BodyFurGuide_Geo
    dupBodyGeo = mc.duplicate(BodyGeo, n = 'BodyFurFcl_Geo')
    dupBodyFurGuideGeo = mc.duplicate(BodyFurGeo, n = 'BodyFurGuide_Geo')
    mc.parent( dupBodyGeo[0], dupBodyFurGuideGeo[0], stillRigGrp)
    mc.setAttr('%s.v' % dupBodyGeo[0], 0)
    mc.setAttr('%s.v' % dupBodyFurGuideGeo[0], 0)
    # Bsh 
    mc.select(dupBodyFurGuideGeo[0], r = True)
    mc.select(dupBodyGeo[0], add = True)
    lrr.doAddBlendShape()

    mc.select(BodyFclRigWrap, r = True)
    mc.select(dupBodyGeo[0], add = True)
    lrr.doAddBlendShape()

    mc.select(dupBodyGeo[0], r = True)
    mc.select(BodyFurGeo, add = True)
    lrr.doAddBlendShape()

def connectObjMirror(translate = True, rotate = True, scale = True, valueA = -1, valueB = 1,*args):
    lists = mc.ls(sl = True)
    if translate == True:
        for each in lists:
            objSp =each.split('_')
            mdvNode = mc.createNode ('multiplyDivide', n = ('%sTr_%s_Mdv' % (objSp[0], objSp[1])))
            mc.setAttr('%s.input2.input2X' % mdvNode, valueA)
            #mc.setAttr('%s.input2.input2X' % mdvNode, valueB)
            #mc.setAttr('%s.input2.input2X' % mdvNode, valueB)
            mc.connectAttr('%s.translate.translateX' % each, '%s.input1.input1X' % mdvNode)
            mc.connectAttr('%s.translate.translateY' % each, '%s.input1.input1Y' % mdvNode)
            mc.connectAttr('%s.translate.translateZ' % each, '%s.input1.input1Z' % mdvNode)
            mc.connectAttr('%s.output.outputX' % mdvNode, '%s_%s_%s.translate.translateX' % (objSp[0], 'R', objSp[2]))
            mc.connectAttr('%s.output.outputY' % mdvNode, '%s_%s_%s.translate.translateY' % (objSp[0], 'R', objSp[2]))
            mc.connectAttr('%s.output.outputZ' % mdvNode, '%s_%s_%s.translate.translateZ' % (objSp[0], 'R', objSp[2]))

    if rotate == True:  
        for each in lists:
            objSp =each.split('_')
            mdvNode = mc.createNode ('multiplyDivide',asShader=True, n = ('%sRo_%s_Mdv' % (objSp[0], objSp[1])))
            #mc.setAttr('%s.input2.input2X' % mdvNode, valueA)
            mc.setAttr('%s.input2.input2Y' % mdvNode, valueA)
            mc.setAttr('%s.input2.input2Z' % mdvNode, valueA)
            mc.connectAttr('%s.rotateX' % each, '%s.input1X' % mdvNode)
            mc.connectAttr('%s.rotateY' % each, '%s.input1Y' % mdvNode)
            mc.connectAttr('%s.rotateZ' % each, '%s.input1Z' % mdvNode)
            mc.connectAttr('%s.outputX' % mdvNode, '%s_%s_%s.rotateX' % (objSp[0], 'R', objSp[2]))
            mc.connectAttr('%s.outputY' % mdvNode, '%s_%s_%s.rotateY' % (objSp[0], 'R', objSp[2]))
            mc.connectAttr('%s.outputZ' % mdvNode, '%s_%s_%s.rotateZ' % (objSp[0], 'R', objSp[2]))  

    if scale == True:
        for each in lists:
            objSp =each.split('_')
            mdvNode = mc.createNode ('multiplyDivide',asShader=True, n = ('%sSc_%s_Mdv' % (objSp[0], objSp[1])))
            mc.setAttr('%s.input2.input2X' % mdvNode, valueA)
            #mc.setAttr('%s.input2.input2X' % mdvNode, valueB)
            #mc.setAttr('%s.input2.input2X' % mdvNode, valueB)
            mc.connectAttr('%s.scaleX' % each, '%s.input1.input1X' % mdvNode)
            mc.connectAttr('%s.scaleY' % each, '%s.input1.input1Y' % mdvNode)
            mc.connectAttr('%s.scaleZ' % each, '%s.input1.input1Z' % mdvNode)
            mc.connectAttr('%s.output.outputX' % mdvNode, '%s_%s_%s.scaleX' % (objSp[0], 'R', objSp[2]))
            mc.connectAttr('%s.output.outputY' % mdvNode, '%s_%s_%s.scaleY' % (objSp[0], 'R', objSp[2]))
            mc.connectAttr('%s.output.outputZ' % mdvNode, '%s_%s_%s.scaleZ' % (objSp[0], 'R', objSp[2]))
            
def duplicateMthJawJnt(*args):
    jaw1Obj = mc.ls('*:JawLwr1_Jnt')
    jaw2Obj = mc.ls('*:JawLwr2_Jnt')
    HeadJntObj = mc.ls('*:Head_Jnt')
    paList = mc.listRelatives(jaw1Obj[0], p = True)
    jawGrp = mc.createNode ('transform' ,asShader=True, n = 'mthJawJnt_Grp')
    mc.delete(mc.parentConstraint(paList[0], jawGrp, mo = False))
    spJnt1Name = jaw1Obj[0].split(':')
    spJnt2Name = jaw2Obj[0].split(':')
    spHeadJntName = HeadJntObj[0].split(':')
    mthJaw1Obj = mc.createNode('joint',asShader=True, n = 'mth%s' % spJnt1Name[-1])
    mthJaw2Obj = mc.createNode('joint',asShader=True, n = 'mth%s' % spJnt2Name[-1])
    HeadJnt = mc.createNode('joint',asShader=True, n = 'mth%s' % spHeadJntName[-1])
    mc.setAttr('%s.radius' % mthJaw1Obj, 0.025)
    mc.setAttr('%s.radius' % mthJaw2Obj, 0.025)
    mc.setAttr('%s.radius' % HeadJnt, 0.025)
    mc.delete(mc.parentConstraint(jaw1Obj[0], mthJaw1Obj, mo =False))
    mc.delete(mc.parentConstraint(jaw2Obj[0], mthJaw2Obj, mo =False))
    mc.delete(mc.parentConstraint(HeadJntObj[0], HeadJnt, mo =False))
    mc.makeIdentity (mthJaw1Obj , apply = True)
    mc.makeIdentity (mthJaw2Obj , apply = True)
    mc.makeIdentity (HeadJnt , apply = True)
    mc.parent(mthJaw2Obj, mthJaw1Obj)
    mc.parent(mthJaw1Obj, jawGrp)



def reBuildCurve(lists = [], spans = 8, degree = 3,*args):
    if lists:
        for each in lists:
            # lists = each
            mc.rebuildCurve(each, rt = 0, spans = spans, degree = degree)
    else:
        lists = mc.ls(sl = True)
        for each in lists:
            # lists = each
            mc.rebuildCurve(each, rt = 0, spans = spans, degree = degree)

def OnOffVisibility(lists = [], v = 1):
    for each in lists:
        mc.setAttr('%s.v' % each, v)

def ballRollAutoToeBend(*args):
    #Left Ankle BallRoll
    cdnL = mc.createNode('condition', au = True , n ='BallRollFix_L_Cdn')
    pmaL = mc.createNode('plusMinusAverage', au = True , n ='BallRollFix_L_Pma')

    mc.connectAttr('AnkleIk_L_Ctrl.ballRoll',cdnL +'.firstTerm')
    mc.connectAttr('AnkleIk_L_Ctrl.ballRoll',cdnL +'.colorIfTrueR')
    mc.setAttr(cdnL +'.operation',3)
    mc.setAttr(cdnL +'.colorIfFalseR',0)

    mc.connectAttr('BallRollFix_L_Cdn.outColorR',pmaL +'.input1D[0]')
    mc.connectAttr('AnkleIk_L_Ctrl.toeBend',pmaL +'.input1D[1]')

    mc.setAttr('BendIkPiv_L_Grp.rotateX',l = False)
    mc.connectAttr(pmaL +'.output1D','BendIkPiv_L_Grp.rotateX',f=True,l = True)


    #Right Ankle BallRoll
    cdnR = mc.createNode('condition', au = True , n ='BallRollFix_R_Cdn')
    pmaR = mc.createNode('plusMinusAverage', au = True , n ='BallRollFix_R_Pma')

    mc.connectAttr('AnkleIk_R_Ctrl.ballRoll',cdnR +'.firstTerm')
    mc.connectAttr('AnkleIk_R_Ctrl.ballRoll',cdnR +'.colorIfTrueR')
    mc.setAttr(cdnR +'.operation',3)
    mc.setAttr(cdnR +'.colorIfFalseR',0)

    mc.connectAttr('BallRollFix_R_Cdn.outColorR',pmaR +'.input1D[0]')
    mc.connectAttr('AnkleIk_R_Ctrl.toeBend',pmaR +'.input1D[1]')

    mc.setAttr('BendIkPiv_R_Grp.rotateX',l = False)
    mc.connectAttr(pmaR +'.output1D','BendIkPiv_R_Grp.rotateX',f=True,l = True)

def BendIkPivRoll(elem = '',*args):
    bendIK = mc.ls('BallIkPiv_L_Grp', 'BallIkPiv_R_Grp')
    ankleIkCtrl = mc.ls('AnkleIk_L_Ctrl', 'AnkleIk_R_Ctrl')
    i = 0
    for each in bendIK:
        lockAttr(listObj= [each],attrs = ( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v'), lock = False)

        spBendIk = each.split('_')
        spBendIkSide = spBendIk[1]
        newBendIK = mc.createNode('transform',asShader=True, n = '%s_%s_Geo' % (elem, spBendIkSide))
        mc.delete(mc.parentConstraint(each, newBendIK, mo = False))
        childrenPar = mc.listRelatives(each, p = True)
        mc.parent(newBendIK, childrenPar, )
        mc.parent(each, newBendIK)

        bendIkMdv = mc.createNode('multiplyDivide',asShader=True, n = '%s_%s_Mdv' % (elem, spBendIkSide))
        mc.setAttr('%s.input2X' % bendIkMdv, 1)
        attrName(obj = ankleIkCtrl[i], elem = elem)
        mc.connectAttr('%s.%s' % (ankleIkCtrl[i], elem), '%s.input1X' % bendIkMdv)
        mc.connectAttr('%s.outputX' % bendIkMdv, '%s.rotateX' % newBendIK)

        lockAttr(listObj= [each, newBendIK],attrs = ( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v'), lock = True)
        i += 1

def heelRoll(jntTmp = [],*args):
    skinGrp = 'Skin_Grp'
    ankleIkCtrl = mc.ls('AnkleIk_L_Ctrl', 'AnkleIk_R_Ctrl')
    ballJnt = mc.ls('Ball_L_Jnt', 'Ball_R_Jnt')
    i = 0
    for each in ballJnt:
        ## Create Joint
        spName = jntTmp[i].split('_')
        jntName = spName[0]
        jntSide = spName[1]
        lastName = spName[-1]
        jntHeel = mc.createNode('joint',asShader=True, n = '%sRoll_%s_Jnt' % (jntName, jntSide))
        zroGrpCon = mc.createNode('transform',asShader=True, n = '%sRollCon_%s_Grp' % (jntName, jntSide))
        zroGrpPar = mc.createNode('transform',asShader=True, n = '%sRollPar_%s_Grp' % (jntName, jntSide))

        ## Snap and parent Group
        snapSel(obj1 = jntTmp[i], obj2= jntHeel)
        snapSel(obj1 = each, obj2= zroGrpCon)
        snapSel(obj1 = each, obj2= zroGrpPar)
        pm.makeIdentity ( jntHeel , apply = True ) ;   
        mc.parent(jntHeel, zroGrpCon)
        mc.parent(zroGrpCon, zroGrpPar)
        mc.parent(zroGrpPar, skinGrp)

        ## parentConstraint
        parentScaleConstraintLoop(  obj = each, lists = [zroGrpPar],par = True, sca = True, ori = False, poi = False)
        ## Create Node 
        toeBendMdv = mc.createNode('multiplyDivide',asShader=True, n = '%stoeBend_%s_Mdv' % (jntName, jntSide))
        toeBendClm = mc.createNode('clamp',asShader=True, n = '%stoeBend_%s_Clm' % (jntName, jntSide))
        ballIkPivRollClm = mc.createNode('clamp',asShader=True, n = '%sballIkPivRoll_%s_Clm' % (jntName, jntSide))
        ballRollClm = mc.createNode('clamp',asShader=True, n = '%sballRoll_%s_Clm' % (jntName, jntSide))
        ballRollPma = mc.createNode('plusMinusAverage', n= '%sballRoll_%s_Pma' % (jntName, jntSide))
        ballRollRevMdv = mc.createNode('multiplyDivide',asShader=True, n = '%sRev_%s_Mdv' % (jntName, jntSide))

        ## Setattr Node
        mc.setAttr('%s.input2X' % toeBendMdv, -1)
        mc.setAttr('%s.minR' % toeBendClm, -90)
        mc.setAttr('%s.maxR' % toeBendClm, 90)
        mc.setAttr('%s.minR' % ballIkPivRollClm, -90)
        mc.setAttr('%s.minR' % ballRollClm, -90)
        mc.setAttr('%s.input2X' % ballRollRevMdv, 1)

        ## connectAttr AnkleIk_L_Ctrl to Node
        mc.connectAttr('%s.toeBend' % ankleIkCtrl[i], '%s.input1X' % toeBendMdv)
        mc.connectAttr('%s.outputX' % toeBendMdv, '%s.inputR' % toeBendClm)
        mc.connectAttr('%s.ballIkPvRoll' % ankleIkCtrl[i], '%s.inputR' % ballIkPivRollClm)
        mc.connectAttr('%s.ballRoll' % ankleIkCtrl[i], '%s.inputR' % ballRollClm)

        ## connectAttr to plusMinusAverage
        mc.connectAttr('%s.outputR' % toeBendClm ,'%s.input1D[0]' % ballRollPma)
        mc.connectAttr('%s.outputR' % ballIkPivRollClm ,'%s.input1D[1]' % ballRollPma)
        mc.connectAttr('%s.outputR' % ballRollClm ,'%s.input1D[2]' % ballRollPma)

        ## connectAttr to Obj
        mc.connectAttr('%s.output1D' % ballRollPma, '%s.input1X' % ballRollRevMdv)
        mc.connectAttr('%s.outputX' % ballRollRevMdv, '%s.rotateX' % zroGrpCon)

        i += 1


def newBshDuplicate (process = 'Nose',elem = ['CrushIn', 'CrushOut'], side = ''):
    selected = mc.ls(sl = True)

    # ## Move Obj
    # bb = mc.exactWorldBoundingBox()
    # yOffset = float( abs( bb[1] - bb[4] ) * 1.2 )
    # xOffset = float( abs( bb[0] - bb[3] ) * 1.3 )
    # xVal = 0
    # yVal = 0

    # if process == 'Eyebrow':
    #     xVal = xOffset*(1+index)
    #     yVal = 0
    # elif process == 'Eye':
    #     xVal = xOffset*(3+index)
    #     yVal = 0
    # elif process == 'Mouth':
    #     xVal = xOffset*6
    #     yVal = 0
    # elif process == 'Nose':
    #     xVal = xOffset*(8+index)
    #     yVal = 0
    # elif process == 'Head':
    #     xVal = xOffset*(11+index)
    #     yVal = 0

    ## Unlock TranslateZ
    lockAttr(listObj= ['NoseBsh_Ctrl'], attrs = ['translateZ'],lock = False,keyable = True)
    
    ## Crate Node 
    ClmNode = mc.createNode('clamp',asShader=True, n = '%s%s_Clm' % (process, elem[0]))
    MdvNode = mc.createNode('multiplyDivide',asShader=True, n = '%s%s_Mdv' % (process, elem[0]))

    ## Setattr Node
    mc.setAttr('%s.minG' % ClmNode, -0.35)
    mc.setAttr('%s.maxR' % ClmNode, 0.35)
    mc.setAttr('%s.input2X' % MdvNode, -2.856)
    mc.setAttr('%s.input2Y' % MdvNode, 2.856)
    ## Limit Control
    mc.setAttr ("NoseBsh_Ctrl.translateZ", 0)
    mc.transformLimits('NoseBsh_Ctrl', tz=(-0.35, 0.35), etz = (1, 1) )

    ## Connect Node
    mc.connectAttr('%s.translateZ' % 'NoseBsh_Ctrl', '%s.inputG' % ClmNode)
    mc.connectAttr('%s.translateZ' % 'NoseBsh_Ctrl', '%s.inputR' % ClmNode)
    mc.connectAttr('%s.outputG' % ClmNode, '%s.input1X' % MdvNode)
    mc.connectAttr('%s.outputR' % ClmNode, '%s.input1Y' % MdvNode)

    i = 0 
    targerXY = ['X', 'Y']
    for each in elem:
        if not side:
            obj = mc.duplicate (selected, n = '%s%s' % (process, each))
            mc.parent('%s%s' % (process, each), '%sBsh_Grp' % process)
            mc.delete(mc.parentConstraint('%sBsh_Grp' % process, '%s%s' % (process, each), mo = False))

            ## Text
            if not mc.objExists( '%s%sText_Grp'  % (process, each)) :
                textGrp = mc.createNode( 'transform' ,asShader=True, n = '%s%sText_Grp'  % (process, each))
                mc.parent( textGrp , '%sText_Grp'  % process)
                mc.delete(mc.parentConstraint('%sText_Grp' % process, textGrp, mo = False))
            ## Add Bsh
            rigTools.addBlendShape(bsn='FacialAll_Bsn', bshObj='%s%s' % (process, each), targetObj='BshRigGeo_Grp', bshVal=1)
            ## Connect Node
            mc.connectAttr('%s.output%s' % (MdvNode, targerXY[i]), '%s.%s%s' % ('FacialAll_Bsn', process, each))
        else:
            obj = mc.duplicate (selected, n = '%s%s_%s' % (process, each, side))
            mc.parent('%s%s_%s' % (process, each, side), '%sBsh_%s_Grp' % (process, side))
            ## Add Bsh
            rigTools.addBlendShape(bsn='FacialAll_Bsn', bshObj='%s%s_%s' % (process, each, side), targetObj='BshRigGeo_Grp', bshVal=1)
            ## Connect Node
            mc.connectAttr('%s.output%s' % (MdvNode, targerXY[i]), '%s.%s%s_%s' % ('FacialAll_Bsn', process, each, side))
            mc.delete(mc.parentConstraint('NoseBsh_Grp', '%s%s_%s' % (process, each, side), mo = False))

            
        i += 1



def selectSkelForProxy ():
    mc.select('ctrl:Pelvis_Jnt',
                'ctrl:Spine1_Jnt',
                'ctrl:Spine2_Jnt',
                'ctrl:Spine3_Jnt',
                'ctrl:Spine4_Jnt',
                'ctrl:Spine5_Jnt',
                'ctrl:Neck3RbnDtl_Jnt',
                'ctrl:Head_Jnt',
                'ctrl:UpLeg1RbnDtl_L_Jnt',
                'ctrl:UpLeg2RbnDtl_L_Jnt',
                'ctrl:UpLeg3RbnDtl_L_Jnt',
                'ctrl:UpLeg4RbnDtl_L_Jnt',
                'ctrl:UpLeg5RbnDtl_L_Jnt',
                'ctrl:LowLeg1RbnDtl_L_Jnt',
                'ctrl:LowLeg2RbnDtl_L_Jnt',
                'ctrl:LowLeg3RbnDtl_L_Jnt',
                'ctrl:LowLeg4RbnDtl_L_Jnt',
                'ctrl:LowLeg5RbnDtl_L_Jnt',
                'ctrl:Ankle_L_Jnt',
                'ctrl:Ball_L_Jnt',
                'ctrl:Clav_L_Jnt',
                'ctrl:UpArm1RbnDtl_L_Jnt',
                'ctrl:UpArm2RbnDtl_L_Jnt',
                'ctrl:UpArm3RbnDtl_L_Jnt',
                'ctrl:UpArm4RbnDtl_L_Jnt',
                'ctrl:UpArm5RbnDtl_L_Jnt',
                'ctrl:Forearm1RbnDtl_L_Jnt',
                'ctrl:Forearm2RbnDtl_L_Jnt',
                'ctrl:Forearm3RbnDtl_L_Jnt',
                'ctrl:Forearm4RbnDtl_L_Jnt',
                'ctrl:Forearm5RbnDtl_L_Jnt',
                'ctrl:Wrist_L_Jnt',
                'ctrl:UpLeg1RbnDtl_R_Jnt',
                'ctrl:UpLeg2RbnDtl_R_Jnt',
                'ctrl:UpLeg3RbnDtl_R_Jnt',
                'ctrl:UpLeg4RbnDtl_R_Jnt',
                'ctrl:UpLeg5RbnDtl_R_Jnt',
                'ctrl:LowLeg1RbnDtl_R_Jnt',                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
                'ctrl:LowLeg2RbnDtl_R_Jnt',
                'ctrl:LowLeg3RbnDtl_R_Jnt',
                'ctrl:LowLeg4RbnDtl_R_Jnt',
                'ctrl:LowLeg5RbnDtl_R_Jnt',
                'ctrl:Ankle_R_Jnt',
                'ctrl:Ball_R_Jnt',
                'ctrl:Clav_R_Jnt',
                'ctrl:UpArm1RbnDtl_R_Jnt',
                'ctrl:UpArm2RbnDtl_R_Jnt',
                'ctrl:UpArm3RbnDtl_R_Jnt',
                'ctrl:UpArm4RbnDtl_R_Jnt',
                'ctrl:UpArm5RbnDtl_R_Jnt',
                'ctrl:Forearm1RbnDtl_R_Jnt',
                'ctrl:Forearm2RbnDtl_R_Jnt',
                'ctrl:Forearm3RbnDtl_R_Jnt',
                'ctrl:Forearm4RbnDtl_R_Jnt',
                'ctrl:Forearm5RbnDtl_R_Jnt',
                'ctrl:Wrist_R_Jnt',


                )


def parentObj(jntName = '', Side = '', lastName = '',*args):

    sels = mc.ls(sl=True)

    if sels:
        for i in range(len(sels)):
            if sels[i] == sels[0]:
                continue
            else:
                mc.aimConstraint( sels[i], sels[i-1], aim = [0, 1, 0])
                mc.delete('%s_aimConstraint1' % sels[i-1])
                pm.makeIdentity ( sels[i-1] , apply = True , rotate = True) 
                mc.parent (sels[i],sels[i-1])

                if sels[i] == sels[-1]:
                    mc.aimConstraint( sels[i-1], sels[i], aim = [0, -1, 0])
                    mc.delete('%s_aimConstraint1' % sels[i])
                    pm.makeIdentity ( sels[i] , apply = True , rotate = True) 
    if jntName:
        for i in range(len(sels)):
            if Side:
                mc.rename(sels[i],'%s%s_%s%s' % (jntName, i+1, Side, lastName))
            else:
                mc.rename(sels[i],'%s%s%s' % (jntName, i+1,lastName))    
    # else:
    #     mc.warning("Nothing selected")

def parentObj2(obj = [], *args):
    if obj:
        sels = obj
    else:
            sels = mc.ls(sl=True)

    if sels:               
        for i in range(len(sels)):      
            if sels[i] == sels[0]:      
                continue              
            else:                  
                mc.parent(sels[i], sels[i-1])   

def BpmFkRig(*args):
    bpmFk = bpmRig.BpmFkRig(jnts=['joint1', 'joint2'],
            parent='bodyRig:Spine5_Jnt',
            rootJnt='bpmRoot',
            inputGeometry='bodyRig:ChestArmor_Geo',
            skinCluster='skinCluster8',
            ctrlShp='crossCircle',
            ctrlColor='red')
    # bpmFk.rig()


def parentProxyDizzy(*args):
    lists = mc.ls(sl= True)
    prGrp = 'PrGeo_Grp'
    if not mc.objExists( prGrp ) :
        prGrp  = mc.createNode ('transform',asShader=True, n = 'PrGeo_Grp' )
        for each in lists:
            jnt = each.replace('_Geo', '_Jnt')
            parentScaleConstraintLoop(obj = jnt, lists = [each] ,par = True, sca = True, ori = False, poi = False)
            mc.parent( each, prGrp)


def renameAttrs(object = '', oldAttr = [], newAttr = []):
    i = 0 
    for each in oldAttr:
        mc.renameAttr('%s.%s' % (object, each) , newAttr[i])  
        i =+ 1

def con(objA = '',objB = '', attrA = [], attrB = []):                   
    i = 0
    for each in attrA:
        if mc.objExists('%s.%s' % (objA, each)):
            mc.connectAttr('%s.%s' % (objA,each), '%s.%s' % (objB,attrB[i]), f = True)
            i = + 1

def deleteAttribute(obj = '', attrs = []):
    for each in attrs:
        mc.deleteAttr( obj, at='%s' % each )


def createTechGeo_Grp(*args):
    techGeo = 'TechGeo_Grp'
    if not mc.objExists(techGeo):
        techGeoGrp = mc.group(n = 'TechGeo_Grp', em = True)
        mc.parent(techGeoGrp, 'Rig_Grp')
        lockAttr(listObj= [techGeoGrp], attrs = ['tx', 'ty', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz'],lock = True,keyable = False)

    else:
        print '# Generate >>', techGeo , 'is Ready'

def publishProxy(*args):
    unparentNode = ['fclRigRNfosterParent1', 'addRigRNfosterParent1']


def utaRemoveSelectedReference() :
    # Remove selected reference from the scene
    editSelectedReference(opr='remove', sels = '')

def utaImportSelectedReference() :
    # Import selected reference from the scene
    editSelectedReference(opr='import', sels = '')

def utaReloadSelectedReference() :
    # Reload selected reference from the scene
    editSelectedReference(opr='reload', sels = '')

def utaEeditSelectedReference(opr='reload', sels = []) :
    if sels:
        sel = sels
    else:
        sel = mc.ls(sl = True)
        if sel:
            sel = sel   
        else:
            print '## Please Select or Typing Object Name'

    if sel:
        for each in sel:
            if mc.objExists(each):
                if mc.objExists( each ) and mc.referenceQuery( each , isNodeReferenced=True ) :  
                    refNode = mc.referenceQuery( each , referenceNode=True , topReference=True )
                    fileName = mc.referenceQuery( refNode , filename=True )
                    if opr == 'reload ' :
                        mc.file( fileName , lr=True )
                    elif opr == 'remove' :
                        mc.file( fileName , rr=True )
                    elif opr == 'import' :
                        mc.file( fileName , i=True )

def cleanFileForProxy(*args):
    utaEeditSelectedReference(opr='remove', sels = ['fclRig:FclRigSkin_Grp', 'addRig:AddRigJnt_Grp','addRig:AddRigCtrl_Grp', 'clothRig:ClothRigStill_Grp'])
    RnNode = mc.ls('*RNfoster*1')
    if RnNode:
        for each in RnNode:
            mc.delete(each)   


def cleanUpReferenceFileForProxy(*args):
    from rf_utils.context import context_info
    asset = context_info.ContextPathInfo() 
    charName = asset.type
    # for each in objPass:
    if charName == 'prop':
        return

    namespaces = mc.namespaceInfo(listOnlyNamespaces=True, recurse=True)
    if namespaces:
        sysNss = [ 'UI' , 'shared']

        i = 0
        for each in namespaces:
            if each == sysNss[i] :
                pass
            else:
                if each == 'bodyRig_md':
                    pass
                else:
                    ref_all = mc.ls(type='reference')
                    for ix in ref_all:
                        ## Query File Reference Paths and Delete Reference File
                        try:
                            name_space = mc.referenceQuery(ix, ns=True).split(':')[1]
                            if "_md" in name_space:
                                if name_space == each:
                                    rfFile = mc.referenceQuery(ix, filename=True)
                        except:
                            pass
                    try:
                        ## Un Reference FilePath
                        mc.file( rfFile , rr=True )
                        ## Delete *RnFosterParent Node
                        RnNode = mc.ls('*RNfoster*')
                        if RnNode:
                            for each in RnNode:
                                mc.delete(each)  

                        print "## CleanUp Reference File For Proxy",
                    except:
                        pass
            i=+1

def mainProxyRig( GeoVis = True , size = 0.75 ):
    GeoGrp = mc.ls('Geo_Grp')
    GeoGuideGrp = mc.rename(GeoGrp[0], 'GeoGuide_Grp')
    MdGeoGrp = mc.ls('MdGeo_Grp', 'PrGeo_Grp')
    ctrlObj = 'Root_Ctrl'
    mainGrpCtrl = 'MainCtrl_Grp'

    obj = mc.ls(sl = True)
    for each in MdGeoGrp:
        mc.parent(each, world=True)
    ## Create Pivot Jnt
    rootTmpJnt = mc.joint( n = 'Root_TmpJnt')
    mc.delete(mc.parentConstraint(obj[0], rootTmpJnt, mo = False))

    mc.select(cl = True)

    ## Run mainRig
    main = mainRig.Run( 
                                    assetName = '' ,
                                    size      = size
                       )

    ## Split name
    Name, Side, LaseName, GrpZro, GrpOfst, GrpPars, CtrlName, CtrlGmblName = nameSp(sel = [ctrlObj])
    ctrlCurve = curveCtrlShape(curveCtrl = 'circle', elem  = CtrlName)
    main.rootCtrl = ctrlCurve

    ## Add Gimbal Control
    main.rootGmblCtrl = addGimbal(ctrlCurve = ctrlCurve, GmblCtrlName = CtrlGmblName)

    ## Create Group
    GrpsZro = createGroup ()
    GrpsOfst = createGroup ()
    GrpsPars = createGroup ()

    GrpZro = mc.rename( GrpsZro, GrpZro)
    GrpOfst = mc.rename( GrpsOfst, GrpOfst)
    GrpPars = mc.rename( GrpsPars, GrpPars)

    mc.parent(GrpOfst,  GrpZro)
    mc.parent(GrpPars, GrpOfst)
    mc.parent(ctrlCurve, GrpPars)

    ## Snape GrpZro to rootTmpJnt
    mc.delete(mc.parentConstraint(rootTmpJnt, GrpZro, mo = False))

    ## Add GeoVis Attribute
    if GeoVis:
        ## Set Colors
        assignColor(obj = [ctrlCurve], col = 'green')
        addAttrCtrl (ctrl =ctrlCurve, elem = ['GeoVis'], min = 0, max = 1, at = 'long')
        mc.setAttr('%s.GeoVis' %ctrlCurve, 1)
        for each in MdGeoGrp:
            mc.connectAttr('%s.GeoVis' %ctrlCurve, '%s.v' % each)

    ## ParentCon
    for each in MdGeoGrp:
        parentScaleConstraintLoop(obj=main.rootGmblCtrl, lists = [each],par = True, sca = True, ori = False, poi = False)

    ## CleanUp
    lockAttrObj(listObj = ['RootCtrlOfst_Grp'], lock = False, keyable = True)
    for each in MdGeoGrp:
        mc.parent(each, 'Geo_Grp')
    mc.parent(GrpZro, mainGrpCtrl )
    mc.delete(rootTmpJnt)
    mc.delete(GeoGuideGrp)
    mc.select(cl = True)




    return main.rootCtrl, main.rootGmblCtrl

## Copy Skin Weight (One to All)
def copySkinWeightObj (objB = '', objAll = [], *args):

    # if not objB:
    #     objB = mc.ls(sl = True)
    #     objAll = mc.ls(sl = True)

        for each in objAll:

            ## split objAA 
            objAA = objAll[0].split('_Geo')
            objAACtrl = 'ctrl:%sRootSub_Ctrl'% objAA[0]
            
            ## split ObjBB
            objBB = objB.split('_Geo_Proxy1')
            objBCtrl = 'ctrl:%sRootSub_Ctrl'% objBB[0]
            
            for node in mc.listHistory(each):
                if mc.nodeType(node) == 'skinCluster':
                    nodes = node
            
                    
            mc.select(objB, each)
            mc.copySkinWeights(ss='skinCluster1115' , ds = nodes, smooth = True)

## Copy SkinWeight (One to One)
def copySkinWeight(*args):
    obj = mc.ls(sl = True)
    for each in obj:
        lists = each.split('_')
        new = '%sPr_%s' % (lists[0], lists[-1])
        mc.select(each, new)
        weightTools.copySelectedWeight()
        print 'Copy SkinWeight', each, 'To', new 



def copyShapeOneToOne() :
    
    sel = mc.ls( sl = True )
    
    grpA = mc.listRelatives( sel[0] , f = True , ad = True , type = 'transform' )
    grpB = mc.listRelatives( sel[1] , f = True , ad = True , type = 'transform' )
    
    if grpA :
        for i in range(len(grpA)) :
            if not mc.listRelatives( grpA[i] , s = True ) : continue
            mc.select( grpA[i] , grpB[i] )
            mm.eval('source "O:/Pipeline/core/rf_maya/rftool/rig/utaTools/utamel/copyShape.mel"')
            mm.eval( 'copyShape ;' )
    else :
        mm.eval( 'copyShape ;' )
    
    mc.select( sel[0] , sel[1] )


def copyShapeAll(objGrpA= [], objGrpB = [],*args) :
    
    if len(objGrpA) == len(objGrpB):
        for i in range(len(objGrpA)):
        # for each in objGrpA:
            grpA = mc.listRelatives( objGrpA[i] , f = True , ad = True , type = 'transform' )
            grpB = mc.listRelatives(objGrpB[i] , f = True , ad = True , type = 'transform' )
            if grpA :
                for x in range(len(grpA)) :
                    if not mc.listRelatives( grpA[x] , s = True ) : continue
                    mc.select( grpA[x] , grpB[x] )
                    mm.eval('source "O:/Pipeline/core/rf_maya/rftool/rig/utaTools/utamel/copyShape.mel"')
                    mm.eval("copyShape ()")

    mc.select(cl = True)

# #------------------------------Copy  nonlinear Node -----------------------------

def convertSelToVtx(geometry=''):
    ''' 
        Convert selection from transform, mesh, vertex, face, edge to vertices.  *User Selection
            return:
                The vertices. list(PyNode)
    '''
    # if not sels:
    #     sels = None
    if not geometry:
        sels = pm.ls(sl=True, fl=True)
    else:
        sels = pm.ls(geometry)
    # for each in sels:
    typesToAdd = (pm.nt.Transform, pm.nt.Mesh, pm.MeshVertex, pm.MeshFace, pm.MeshEdge)
    result = []
    for sel in [s for s in sels if isinstance(s, typesToAdd)]:
        if isinstance(sel, pm.nt.Transform):
            shp = sel.getShape(ni=True)
            result.append(shp.vtx)
        elif isinstance(sel, pm.nt.Mesh):
            result.append(sel.vtx)
        elif isinstance(sel, (pm.MeshFace ,pm.MeshEdge)):
            vert = [pm.MeshVertex(v) for v in pm.polyListComponentConversion(sel, tv=True)]
            result.extend(vert)
        elif isinstance(sel, pm.MeshVertex):
            result.append(sel)

    return result


def addDeformerMemberObj(deformerName = '', geometry = '', orderAddOrRemove = True):
    ''' 
        Add selected component(vertex, face, edge)/object(mesh/transform) to the deformer membership. *User Selection
            args :
                deformerName = The name of deformer to add.
            return:
                Nothing
    '''
    # misc.addDeformerMember(deformerName=each)

    sels = convertSelToVtx(geometry = geometry)
    if not sels:
        return

    deformerNode = pm.PyNode(deformerName)
    memberSet = deformerNode.message.outputs(type='objectSet')[0]
        
    for sel in sels:
        if orderAddOrRemove:
            try:
                # pm.sets(memberSet, sel, fe=True)
                memberSet.add(sel)
            except: pass
        else:
            try:
                # for defs in defList:
                mc.deformer(deformerName,e=1,g=geometry,rm=1)
            except: pass

def addDeformerObj(geometry = [], deformerNameLists = [], orderAddOrRemove = True):
    for geoEach in geometry:
        for deformEach in deformerNameLists:
            addDeformerMemberObj(deformerName = deformEach, geometry = geoEach, orderAddOrRemove = orderAddOrRemove)


def parOrCon(obj = '', lists = [] , parentCon = True, connections = False, *args):
    for each in lists:
        if parentCon:
            parentScaleConstraintLoop(obj = obj, lists = [each] ,par = True, sca = True, ori = False, poi = False,)

        if connections:
            connection2Obj(objA = obj, target = each)




def createControlCurve(curveCtrl = '', parentCon = True, connections = True, geoVis = True, fkChain = True,*args): 
    sels = mc.ls(sl = True)

    for each in sels:
    # for each in sel:
        spNameObj = each.split("_")
        nameSub = spNameObj[0].split("Geo")

        if len(spNameObj) == 2:
            ## Splite Name
            name = nameSub[0]
            lastName = spNameObj[-1]
            ## Name Group and Control
            grpCtrlName = '%sCtrlZro_Grp' % name  
            grpCtrlOfsName = '%sCtrlOfst_Grp' % name 
            grpCtrlParsName = '%sCtrlPars_Grp' % name    
            ctrlName = '%s_Ctrl' % name   
            GmblCtrlName = '%sGmbl_Ctrl' % name 
        else:
            ## Splite Name Side 
            name = nameSub[0]
            side = spNameObj[1]
            lastName = spNameObj[-1]
            ## Name Group and Control
            grpCtrlName = '%sCtrlZro_%s_Grp' % (name, side) 
            grpCtrlOfsName = '%sCtrlOfst_%s_Grp' % (name, side)
            grpCtrlParsName = '%sCtrlPars_%s_Grp' % (name, side)
            ctrlName = '%s_%s_Ctrl' % (name, side)
            GmblCtrlName = '%sGmbl_%s_Ctrl' % (name, side)
        ## Create Group
        mc.group(n = grpCtrlName, em = True)
        mc.group(n = grpCtrlOfsName, em = True)
        mc.group(n = grpCtrlParsName, em = True)

        ## Parent
        mc.parent(grpCtrlParsName, grpCtrlOfsName)
        mc.parent(grpCtrlOfsName, grpCtrlName)

        ## Create Control Shape Curve
        ctrlName = curveCtrlShape(curveCtrl = curveCtrl, elem  = ctrlName,)


        mc.parent( ctrlName, grpCtrlParsName)
        mc.delete(mc.parentConstraint(each, grpCtrlName))

        ## Add Gimbal Control
        dupGmblCurve = addGimbal(ctrlCurve = ctrlName,GmblCtrlName = GmblCtrlName)
        # ## Add GeoVis
        if geoVis:
            ctrlName, elem = addGeoVis(ctrlName = ctrlName, elem = 'GeoVis')
            mc.connectAttr('%s.%s' % (ctrlName, elem), '%s.v' % each)


        if parentCon:
            parentScaleConstraintLoop(obj = dupGmblCurve, lists = [each] ,par = True, sca = True, ori = False, poi = False)

        elif connections:
            connection2Obj(objA = ctrlName, target = each)

        else:
            pass
        # if fkChain:
        #     if not range(len(sels)) == 0:
        #         mc.parent(GmblCtrlName)
        # parOrCon(obj = ctrlName, lists = [each] , parentCon = parentCon, connections = connections)
    return grpCtrlName, grpCtrlOfsName, grpCtrlParsName, ctrlName, GmblCtrlName

def fkChainControl(lists = [],*args):
    for i in range(len(lists)):
        if not i == 0:
            mc.parent(lits[i], lits[i-1])

def addGimbal(ctrlCurve = '',GmblCtrlName = '',*args):

    ## Duplicate Gimbal Control
    dupGmblCurve = mc.duplicate(ctrlCurve, n = GmblCtrlName)
    mc.setAttr('%s.sx' % dupGmblCurve[0], 0.741)
    mc.setAttr('%s.sy' % dupGmblCurve[0], 0.741)
    mc.setAttr('%s.sz' % dupGmblCurve[0], 0.741)
    pm.makeIdentity ( dupGmblCurve , apply = True ) 
    attrNameMinMax (obj = (ctrlCurve + 'Shape'), elem = 'gimbalControl', min = 0, max = 1,)

    mc.parent(dupGmblCurve, ctrlCurve)
    mc.connectAttr('%sShape.gimbalControl' % ctrlCurve, '%sShape.v' % dupGmblCurve[0])    

    ## lockAttribute
    objCtrl = (ctrlCurve, dupGmblCurve[0])

    for each in objCtrl:
        lockAttr(listObj = [each], attrs = ['v'],lock = True,keyable = False)

    ## Set Colors
    assignColor(obj = [ctrlCurve], col = 'red')
    assignColor(obj = [GmblCtrlName], col = 'white')
    return dupGmblCurve[0]

def addGimbalCtrl(ctrl = [], nameGmbl = 'Shadow', shapeAttr = True, *args):
    if ctrl:
        curveAll = ctrl
    else:
        curveAll =  mc.ls(sl = True)
    for each in curveAll:
        name, side, lastName = splitName(sels = [each], optionSide = True)
        ctrlCurve = name + side + lastName

        unparentObj = []
        lists = mc.listRelatives(ctrlCurve, f = True, c = True)
        for eachLists in lists:
            obj = eachLists.split('|')[-1]
            if not 'Shape' in obj:
                mc.parent (obj, w=True)  
                unparentObj.append(eachLists) 


        ## Duplicate Gimbal Control
        shaDowName = name.split('UiRig')[0]
        dupGmblCurve = mc.duplicate(ctrlCurve, n = '{}{}UiRig{}{}'.format(shaDowName, nameGmbl, side, lastName))
        mc.setAttr('{}.sx'.format(dupGmblCurve[0]), 0.741)
        mc.setAttr('{}.sy'.format(dupGmblCurve[0]), 0.741)
        mc.setAttr('{}.sz'.format(dupGmblCurve[0]), 0.741)
        pm.makeIdentity ( dupGmblCurve , apply = True ) 
        if shapeAttr:
            GmblCtrl = ctrlCurve + 'Shape'
            nameGmblVis = nameGmbl 
        else:
            GmblCtrl = ctrlCurve
            nameGmblVis = nameGmbl + 'Vis'

        attrNameMinMax (obj = (GmblCtrl), elem = nameGmblVis, min = 0, max = 1,)
        mc.parent(dupGmblCurve, ctrlCurve)
        mc.connectAttr('{}.{}'.format(GmblCtrl, nameGmblVis), '{}Shape.v'.format(dupGmblCurve[0]))    

        ## lockAttribute
        objCtrl = (ctrlCurve, dupGmblCurve[0])

        for eachCtrl in objCtrl:
            lockAttr(listObj = [eachCtrl], attrs = ['v'],lock = True,keyable = False)
        ## Set Colors
        # assignColor(obj = [ctrlCurve], col = 'red')
        assignColor(obj = [dupGmblCurve], col = 'white')
        
        for eachObj in unparentObj:
            obj = eachObj.split('|')[-1]
            mc.parent(obj, each)

    return dupGmblCurve[0]


def addGeoVis(ctrlName = '', elem = 'GeoVis',*args):
    ## Add Attribute GeoVis
    attrNameMinMax(obj = ctrlName, elem = elem, min = 0, max = 1,)
    mc.setAttr ("{}.{}".format(ctrlName, elem),  1)
    return ctrlName, elem

def addGeoVisObj(ctrlName = '', elem = 'GeoVis', obj = '', *args):
    ## Add Attribute GeoVis
    if not mc.objExists('{}.{}'.format(ctrlName, elem)):
        ctrlName, elem = addGeoVis(ctrlName = ctrlName, elem = elem)

    mc.setAttr ("{}.{}".format(ctrlName, elem),  0)
    mc.connectAttr('{}.{}'.format(ctrlName, elem), '{}.v'.format(obj), f = True)

    return ctrlName, elem, obj

def curveCtrlShape(curveCtrl = '', elem  = '',*args):
    ## Create Curve Shape
    mm.eval('source "O:/Pipeline/core/rf_maya/rftool/rig/utaTools/utamel/CurveShape.mel"')
    if curveCtrl == 'circle':
        curveShape = mm.eval("utaCurve "+curveCtrl+";")
    if curveCtrl == 'square':
        curveShape = mm.eval("utaCurve "+curveCtrl+";")
    if curveCtrl == 'cube':
        curveShape = mm.eval("utaCurve "+curveCtrl+";")
    if curveCtrl == 'arrow':
        curveShape = mm.eval("utaCurve "+curveCtrl+";")
    if curveCtrl == 'circleArrow':
        curveShape = mm.eval("utaCurve "+curveCtrl+";")
    if curveCtrl == 'cornerArrow':
        curveShape = mm.eval("utaCurve "+curveCtrl+";")
    if curveCtrl == 'lineArrow':
        curveShape = mm.eval("utaCurve "+curveCtrl+";")

    if curveCtrl == 'circleLine':
        curveShape = mm.eval("utaCurve "+curveCtrl+";")
    if curveCtrl == 'airplane':
        curveShape = mm.eval("utaCurve "+curveCtrl+";")
    if curveCtrl == 'horseshoe':
        curveShape = mm.eval("utaCurve "+curveCtrl+";")   
    if curveCtrl == 'arrowLR':
        curveShape = mm.eval("utaCurve "+curveCtrl+";")
    if curveCtrl == 'hand':
        curveShape = mm.eval("utaCurve "+curveCtrl+";")
    if curveCtrl == 'arrowAll':
        curveShape = mm.eval("utaCurve "+curveCtrl+";")
    if curveCtrl == 'ball':
        curveShape = mm.eval("utaCurve "+curveCtrl+";")     

    if curveCtrl == 'placement':
        curveShape = mm.eval("utaCurve "+curveCtrl+";")
    if curveCtrl == 'halfBall':
        curveShape = mm.eval("utaCurve "+curveCtrl+";")
    if curveCtrl == 'shoe':
        curveShape = mm.eval("utaCurve "+curveCtrl+";")   
    if curveCtrl == 'eye':
        curveShape = mm.eval("utaCurve "+curveCtrl+";")
    if curveCtrl == 'placementBow':
        curveShape = mm.eval("utaCurve "+curveCtrl+";")
    if curveCtrl == 'arrowUp':
        curveShape = mm.eval("utaCurve "+curveCtrl+";")
    if curveCtrl == 'key':
        curveShape = mm.eval("utaCurve "+curveCtrl+";")   

    if curveCtrl == 'arrowCircle':
        curveShape = mm.eval("utaCurve "+curveCtrl+";")
    if curveCtrl == 'iceCream':
        curveShape = mm.eval("utaCurve "+curveCtrl+";")
    if curveCtrl == 'sun':
        curveShape = mm.eval("utaCurve "+curveCtrl+";")   
    if curveCtrl == 'plus':
        curveShape = mm.eval("utaCurve "+curveCtrl+";")
    if curveCtrl == 'pyramid':
        curveShape = mm.eval("utaCurve "+curveCtrl+";")
    if curveCtrl == 'cornerArrow2':
        curveShape = mm.eval("utaCurve "+curveCtrl+";")
    if curveCtrl == 'arrow9':
        curveShape = mm.eval("utaCurve "+curveCtrl+";")   

    if curveCtrl == 'arrowTurn':
        curveShape = mm.eval("utaCurve "+curveCtrl+";")
    if curveCtrl == 'bamboo':
        curveShape = mm.eval("utaCurve "+curveCtrl+";")
    if curveCtrl == 'sungrasses':
        curveShape = mm.eval("utaCurve "+curveCtrl+";")   
    if curveCtrl == 'w':
        curveShape = mm.eval("utaCurve "+curveCtrl+";")
    if curveCtrl == 'fish':
        curveShape = mm.eval("utaCurve "+curveCtrl+";")
    if curveCtrl == 'arrow2D':
        curveShape = mm.eval("utaCurve "+curveCtrl+";")
    if curveCtrl == 'gear':
        curveShape = mm.eval("utaCurve "+curveCtrl+";")   
    if curveCtrl == 'ballPlus':
        curveShape = mm.eval("utaCurve "+curveCtrl+";")  
    controlName = mc.rename( curveShape, elem)
    return controlName

def generateCurveType(nameCurve = '', side = '', curveType = '', *args):
    side = findSide(side = side)

    if 'circle' in curveType:
        curveName = mc.circle( n = '{}{}Crv'.format(curveType, side),nr=(0, 0, 1), c=(0, 0, 0) )[0]
        curveName = mc.rename(curveName, '{}{}Crv'.format(nameCurve, side))
    elif 'square' in curveType:
        mc.curve(d=1, p=[(1,-1,1), (-1,-1,1),(-1,-1,-1),(1,-1,-1),(1,-1,1),])
        curveName = mc.square( n = '{}{}Crv'.format(curveType, side),nr=(0, 0, 1), c=(0, 0, 0) )[0]
        curveName = mc.rename(curveName, '{}{}Crv'.format(nameCurve, side))
    elif 'blink' in curveType:
        curveName = mc.circle( n = '{}{}Crv'.format(curveType, side),nr=(0, 0, 1), c=(0, 0, 0) )[0]


        mm.eval('select -r blink_Crv.cv[4:6] ;')
        mm.eval('move -r -os -wd 0 0.873698 0 ;')
        mm.eval('select -r blink_Crv.cv[5] ;')
        mm.eval('move -r -os -wd 0 0.743517 0 ;')
        curveName = mc.rename(curveName, '{}{}Crv'.format(nameCurve, side))

    return curveName

def ttRigConnectScale():
    ctrls = mc.ls('*MainUpTthRig_Ctrl', '*MainLowTthRig_Ctrl')


    # ctrls = ['MainUpTthRig_Ctrl','MainLowTthRig_Ctrl']
    
    jntUprs = [ 'RbnSkinUpTthRig_1_Jnt',
                'RbnSkinUpTthRig_2_Jnt',
                'RbnSkinUpTthRig_3_Jnt',
                'RbnSkinUpTthRig_4_Jnt',
                'RbnSkinUpTthRig_5_Jnt',
                'RbnSkinUpTthRig_6_Jnt',
                'RbnSkinUpTthRig_7_Jnt',
                'RbnSkinUpTthRig_8_Jnt',
                'RbnSkinUpTthRig_9_Jnt']
                
    jntLwrs = [ 'RbnSkinLowTthRig_1_Jnt',
                'RbnSkinLowTthRig_2_Jnt',
                'RbnSkinLowTthRig_3_Jnt',
                'RbnSkinLowTthRig_4_Jnt',
                'RbnSkinLowTthRig_5_Jnt',
                'RbnSkinLowTthRig_6_Jnt',
                'RbnSkinLowTthRig_7_Jnt',
                'RbnSkinLowTthRig_8_Jnt',
                'RbnSkinLowTthRig_9_Jnt']
    
    for i in range(len(jntUprs)):
        mc.connectAttr('%s.sx' % ctrls[0],'%s.sz' % jntUprs[i])
        mc.connectAttr('%s.sy' % ctrls[0],'%s.sx' % jntUprs[i])
        mc.connectAttr('%s.sz' % ctrls[0],'%s.sy' % jntUprs[i])
        
    for i in range(len(jntLwrs)):
        mc.connectAttr('%s.sx' % ctrls[1],'%s.sz' % jntLwrs[i])
        mc.connectAttr('%s.sy' % ctrls[1],'%s.sx' % jntLwrs[i])
        mc.connectAttr('%s.sz' % ctrls[1],'%s.sy' % jntLwrs[i])

def matrixObj (parentObj = '', childObj = '', translate = True, rotate = True,*args):
    ## select
    if not parentObj:
        sel = mc.ls(sl = True)
        if sel:
            parentObj = sel[0]
            childObj = sel[-1]
        else:
            print "Please Select Obj A , B"


    ## createNode
    worldMultiMatrixNode = mc.createNode('multMatrix',asShader=True, n = 'worldMultiMatrix')
    decomposeMatrixTxNode = mc.createNode('decomposeMatrix',asShader=True, n = 'decomposeMatrixTx')

    locatorObj = lrc.Locator()
    positionObj = mc.rename(locatorObj , 'Matrix_Loc')
    # ## connect
    # mc.connectAttr ('%s.matrix' % childObj, worldMatrix_test.matrixIn[0];
    mc.connectAttr ('%s.matrix' % childObj,  worldMultiMatrixNode + '.matrixIn[0]' )
    mc.connectAttr('%s.matrix' % parentObj, '%s.matrixIn[1]' % worldMultiMatrixNode)
    mc.connectAttr('%s.matrixSum' % worldMultiMatrixNode, '%s.inputMatrix' % decomposeMatrixTxNode)
    if translate or rotate :
        if translate:
            ## Node for Translate
            matrixDivideTranslate = mc.createNode('multiplyDivide',asShader=True, n = 'matrixTranslate_Mdv')

            mc.connectAttr('%s.outputTranslate' % decomposeMatrixTxNode, '%s.input1' % matrixDivideTranslate)
            mc.connectAttr('%s.outputX' % matrixDivideTranslate, '%s.tx' % positionObj)
            mc.connectAttr('%s.outputY' % matrixDivideTranslate, '%s.ty' % positionObj)
            mc.connectAttr('%s.outputZ' % matrixDivideTranslate, '%s.tz' % positionObj)
        if rotate:
            ## Node for Translate
            matrixDivideRotate = mc.createNode('multiplyDivide',asShader=True, n = 'matrixTranslate_Mdv')

            mc.connectAttr('%s.outputRotate' % decomposeMatrixTxNode, '%s.input1' % matrixDivideRotate)
            mc.connectAttr('%s.outputX' % matrixDivideRotate, '%s.rx' % positionObj)
            mc.connectAttr('%s.outputY' % matrixDivideRotate, '%s.ry' % positionObj)
            mc.connectAttr('%s.outputZ' % matrixDivideRotate, '%s.rz' % positionObj)


# def connectBpmJnt(*args):
#     selObj = mc.ls(sl = True)
#     jntBpm = mc.listRelatives(selObj[0], c = True)
#     for Jnt in jntBpm:
#         bpmJntSp = Jnt.split('_J')
#         if len(bpmJntSp) == 2:
#             name = bpmJntSp[0].split('BaseBpm')
#             nameSp = name[0].split(':')
#             mainJnt = 'bodyRig_md:%s%s_%s' % (nameSp[-1], name[-1], bpmJntSp[-1]) 
#             parentScaleConstraintLoop(obj = mainJnt, lists = [Jnt],par = True, sca = True, ori = False, poi = False)
#         else:
#             name = bpmJntSp[0].split('BaseBpm')
#             nameSp = name[0].split(':')
#             mainJnt = 'bodyRig_md:%s%s_%s_%s' % (nameSp[-1], name[-1], bpmJntSp[1], bpmJntSp[-1]) 
#             parentScaleConstraintLoop(obj = mainJnt, lists = [Jnt],par = True, sca = True, ori = False, poi = False)

def connectBpmJnt(*args):
    selObj = mc.ls('*:*BaseBpmRigJnt_Grp')[0]
    AddCtrl = mc.ls('*:AddCtrl_Grp')[0]
    jntBpm = mc.listRelatives(selObj, c = True)
    for Jnt in jntBpm:
        bpmJntNp = Jnt.split(':')
        bpmJntSp = bpmJntNp[-1].split('_')
        if len(bpmJntSp) == 2:
            name = bpmJntSp[0].split('BaseBpm')
            mainJnt = 'bodyRig_md:%s%s_%s' % (name[0], name[1], bpmJntSp[-1]) 
            parentScaleConstraintLoop(obj = mainJnt, lists = [Jnt],par = True, sca = True, ori = False, poi = False)
        else:
            name = bpmJntSp[0].split('BaseBpm')
            subName = name[-1].split('_')
            mainJnt = 'bodyRig_md:%s%s_%s_%s' % (name[0], name[1], bpmJntSp[-2], bpmJntSp[-1]) 
            parentScaleConstraintLoop(obj = mainJnt, lists = [Jnt],par = True, sca = True, ori = False, poi = False)

    bpmCtrlGrp = mc.group('*:BpmRigCtrl_Grp',n='BpmRigCtrlPar_Grp')

    bpmStillGrp = mc.group(mc.ls('*:BpmRigStill_Grp'),mc.ls('*:BpmRigGeo_Grp'),n='BpmRigStillPar_Grp')
    mc.parent(bpmStillGrp,'*:Still_Grp')
    mc.parent(bpmCtrlGrp, AddCtrl)


def get_nodes_in_history_by_type(typ , selection):
    nodes = None
    for node in mc.listHistory(selection):
        if mc.nodeType(node) == typ:
            nodes = node
            break
    return nodes

def getSkinClusterMel(geo = '', *args):
    import maya.mel as mm

    skinClusterNode = mm.eval('findRelatedSkinCluster {}'.format(geo))
    return skinClusterNode

def delete_unused_skin():
# <<<<<<< HEAD
    
    mdGeoGrp = mc.listRelatives('*MdGeo_Grp',allDescendents=True)
    for geo in mdGeoGrp:
        if '_Geo' in geo:
            if not '_GeoShape' in geo:
                skin = get_nodes_in_history_by_type('skinCluster', geo)
                if skin:
                    value = mc.getAttr('%s.envelope' % skin)
                    if value == 0:
                        mc.delete(skin)
                        print 'Delete >> %s SkinCluster' % skin


def nameSp(sel = [],*args):
    if not sel:
        sel = mc.ls(sl = True)
    for each in sel:

        spNameObj = each.split("_")
        NameSub = spNameObj[0].split("Geo")

        if len(spNameObj) == 2:

            Name = NameSub[0]
            Side = '_'
            LaseName = spNameObj[-1]
            GrpZro = '%sCtrlZro_Grp' % Name  
            GrpOfst = '%sCtrlOfst_Grp' % Name 
            GrpPars = '%sCtrlPars_Grp' % Name    
            CtrlName = '%s_Ctrl' % Name   
            CtrlGmblName = '%sGmbl_Ctrl' % Name 

        else:

            Name = NameSub[0]
            Side = spNameObj[1]
            LaseName = spNameObj[-1]
            GrpZro = '%sCtrlZro_%s_Grp' % (Name, Side) 
            GrpOfst = '%sCtrlOfst_%s_Grp' % (Name, Side)
            GrpPars = '%sCtrlPars_%s_Grp' % (Name, Side)
            CtrlName = '%s_%s_Ctrl' % (Name, Side)
            CtrlGmblName = '%sGmbl_%s_Ctrl' % (Name, Side)

    return Name, Side, LaseName, GrpZro, GrpOfst, GrpPars, CtrlName, CtrlGmblName

def noise(attr = '', axis = '', *args):
    sel = mc.ls(sl = True)
    ctrlObj = sel[0]
    targetObj = sel[-1]

    ## Split name
    Name, Side, LaseName, GrpZro, GrpOfst, GrpPars, CtrlName, CtrlGmblName = nameSp(sel = [ctrlObj])

    mc.addAttr( ctrlObj , ln = attr , at = 'float' , k = True )

    mc.setAttr ('%s.%s' % (ctrlObj, attr),0.5)
    NoiseNode = mc.createNode('noise',asShader=True, n = '%s%sNoi' % (Name, Side, ))
    mc.setAttr ( '%s.%s' % (NoiseNode, 'frequency'), 0 )
    TimeNode = mc.ls('time1')

    mc.connectAttr ('%s.outTime' % TimeNode[0] , '%s.time' % NoiseNode)
    RemapNode = mc.createNode( 'remapValue' ,asShader=True, n =  '%s%sRem' % (Name, Side ))
    mc.connectAttr(NoiseNode + '.outColor' , RemapNode + '.color[0]' + '.color_Color')

    MdvNode = mc.createNode('multiplyDivide' ,asShader=True, n = '%s%sMdv' % (Name, Side))
    MdvSetNode = mc.createNode('multiplyDivide' ,asShader=True, n = '%sSet%sMdv' % (Name, Side))
    mc.connectAttr(RemapNode + '.outColor', MdvNode + '.input1')
    mc.connectAttr(MdvNode + '.output', MdvSetNode + '.input1')
    mc.connectAttr( '%s.%s' % (ctrlObj, attr), MdvSetNode + '.input2X')
    mc.connectAttr(MdvSetNode + '.outputX', targetObj + '.' + axis)

def createGroup (*args):
    Group = mc.group ( em = True)
    return  Group

def addLambert(lisObj = [],*args):
    if not lisObj :
        lisObj = mc.ls('*BaseBpm_Geo')
    for each in lisObj:
        mc.select(each)
        mc.hyperShade(each, assign ='lambert1')

def preferredAnkleLegIk(obj = [], pfx = 45, pfy = 0, pfz = 0, *args):
    for each in obj:
        if each:
            mc.setAttr ('%s.preferredAngleX' % each, pfx)
            mc.setAttr ('%s.preferredAngleY' % each, pfy)
            mc.setAttr ('%s.preferredAngleZ' % each, pfz)

            mc.dgdirty('%s.rx' % each)
            mc.dgdirty('%s.ry' % each)               
            mc.dgdirty('%s.rz' % each)

def btwJntFnt(objDri = '', btwJnt = '', *args):

    ## split Name 
    namesSel = btwJnt.split('_')

    if len(namesSel) == 2:
        name = namesSel[0]
        side = '_'
        lastName = namesSel[-1]

    else:
        name = namesSel[0]
        side = '_' + namesSel[1] + '_'
        lastName = namesSel[-1]

    ## Create Joint 
    btwFnt = mc.createNode ('joint',n = '%sFnt%s%s' % (name, side, lastName))
    mc.delete(mc.parentConstraint( btwJnt, btwFnt, mo = False))
    mc.parent( btwFnt, btwJnt )

    ## Add Attribute on joint Sel
    addAttrCtrl (ctrl = btwJnt, elem = ['scaleBtwFntZP'], min = -200, max = 200, at = 'float')
    mc.setAttr('%s.scaleBtwFntZP' % btwJnt, 0.25)
    addAttrCtrl (ctrl = btwJnt, elem = ['scaleBtwFntZN'], min = -200, max = 200, at = 'float')
    mc.setAttr('%s.scaleBtwFntZN' % btwJnt, 0.4)

    ## Create Node
    clampNode = mc.createNode('clamp',n = '%s%sClm' % (name, side))
    mc.setAttr ('%s.minR' % clampNode,-180)
    mc.setAttr ('%s.maxG' % clampNode, 180)
    mdvAmpNode = mc.createNode('multiplyDivide',n = '%sAmp%sMdv' % (name, side))
    mc.setAttr ('%s.input2Y' % mdvAmpNode,  0.01)
    mdvPlusNode = mc.createNode('multiplyDivide',n = '%sPlus%sMdv' % (name, side))
    mdvTotalNode = mc.createNode('multiplyDivide',n = '%sTotal%sMdv' % (name, side))
    mc.setAttr('%s.input2X' % mdvTotalNode, -0.01)
    mc.setAttr('%s.input2Y' % mdvTotalNode, 0.05)
    plsMiNode = mc.createNode('plusMinusAverage',n = '%sTotal%sPls' % (name, side))
    adlNode = mc.createNode('addDoubleLinear',n = '%sDefault%sAdl' % (name, side))
    addAttrCtrl (ctrl = adlNode, elem = ['default'], min = -200, max = 200, at = 'float')
    mc.setAttr( '%s.default' % adlNode, 1)
    MdvAnswerNode = mc.createNode('multiplyDivide',n = '%sAnswer%sMdv' % (name, side))

    ## Connect Attribute
    mc.connectAttr('%s.rx' % objDri, '%s.inputR' % clampNode)
    mc.connectAttr('%s.rx' % objDri, '%s.inputG' % clampNode)

    mc.connectAttr('%s.outputR' % clampNode, '%s.input1X' % mdvAmpNode)
    mc.connectAttr('%s.outputG' % clampNode, '%s.input1Y' % mdvAmpNode)
    mc.connectAttr('%s.outputX' % mdvAmpNode, '%s.input1X' % mdvPlusNode)
    mc.connectAttr('%s.outputY' % mdvAmpNode, '%s.input1Y' % mdvPlusNode)
    mc.connectAttr('%s.sz' % btwJnt, '%s.input2X' % mdvPlusNode)
    mc.connectAttr('%s.sz' % btwJnt, '%s.input2Y' % mdvPlusNode)
    mc.connectAttr('%s.outputX' % mdvPlusNode, '%s.input1X' % mdvTotalNode)
    mc.connectAttr('%s.outputY' % mdvPlusNode, '%s.input1Y' % mdvTotalNode)
    mc.connectAttr('%s.outputX' % mdvTotalNode, '%s.input2D[0].input2Dx' % plsMiNode)
    mc.connectAttr('%s.outputY' % mdvTotalNode, '%s.input2D[1].input2Dx' % plsMiNode)
    mc.connectAttr('%s.output2Dx' % plsMiNode, '%s.input2' % adlNode)
    mc.connectAttr('%s.default' % adlNode, '%s.input1' % adlNode)
    mc.connectAttr('%s.output' % adlNode, '%s.input1X' % MdvAnswerNode) 
    mc.connectAttr('%s.scaleBtwFntZP' % btwJnt, '%s.input2X' % mdvAmpNode)
    mc.connectAttr('%s.scaleBtwFntZN' % btwJnt, '%s.input2Y' % mdvAmpNode)
    mc.connectAttr('%s.outputX' % MdvAnswerNode, '%s.sz' % btwFnt)  
    mc.connectAttr('%s.sx' % btwJnt, '%s.sx' % btwFnt)


def btwJntFnt2(objDri = '', btwJnt = '', *args):

    ## split Name 
    namesSel = btwJnt.split('_')

    if len(namesSel) == 2:
        name = namesSel[0]
        side = '_'
        lastName = namesSel[-1]

    else:
        name = namesSel[0]
        side = '_' + namesSel[1] + '_'
        lastName = namesSel[-1]

    ## Create Joint 
    btwFnt = mc.createNode ('joint',n = '%sFnt%s%s' % (name, side, lastName))
    mc.delete(mc.parentConstraint( btwJnt, btwFnt, mo = False))
    mc.parent( btwFnt, btwJnt )

    ## Add Attribute on joint Sel
    addAttrCtrl (ctrl = btwJnt, elem = ['scaleBtwFnt_X'], min = -200, max = 200, at = 'float')
    mc.setAttr('%s.scaleBtwFnt_X' % btwJnt, 0.01)
    addAttrCtrl (ctrl = btwJnt, elem = ['scaleBtwFnt_Z'], min = -200, max = 200, at = 'float')
    mc.setAttr('%s.scaleBtwFnt_Z' % btwJnt, 0.01)
    addAttrCtrl (ctrl = btwJnt, elem = ['scaleBtwFnt_Y'], min = -200, max = 200, at = 'float')
    mc.setAttr('%s.scaleBtwFnt_Y' % btwJnt, 0.01)

    ## Create Node
    mdvAmpNode = mc.createNode('multiplyDivide',n = '%sAmp%sMdv' % (name, side))
    adlXNode = mc.createNode('addDoubleLinear',n = '%sDefaultX%sAdl' % (name, side))
    addAttrCtrl (ctrl = adlXNode, elem = ['default'], min = -200, max = 200, at = 'float')
    mc.setAttr( '%s.default' % adlXNode, 1)
    adlZNode = mc.createNode('addDoubleLinear',n = '%sDefaultZ%sAdl' % (name, side))
    addAttrCtrl (ctrl = adlZNode, elem = ['default'], min = -200, max = 200, at = 'float')
    mc.setAttr( '%s.default' % adlZNode, 1)
    adlYNode = mc.createNode('addDoubleLinear',n = '%sDefaultY%sAdl' % (name, side))
    addAttrCtrl (ctrl = adlYNode, elem = ['default'], min = -200, max = 200, at = 'float')
    mc.setAttr( '%s.default' % adlYNode, 1)

    ## Connect Attribute
    ## adl X
    mc.connectAttr('%s.outputX' % mdvAmpNode, '%s.input2' % adlXNode)
    mc.connectAttr('%s.default' % adlXNode, '%s.input1' % adlXNode)
    mc.connectAttr('%s.output' % adlXNode, '%s.sx' % btwFnt) 

    ## adl Z
    mc.connectAttr('%s.outputX' % mdvAmpNode, '%s.input2' % adlZNode)
    mc.connectAttr('%s.default' % adlZNode, '%s.input1' % adlZNode)
    mc.connectAttr('%s.output' % adlZNode, '%s.sz' % btwFnt) 

    ## adl Y
    mc.connectAttr('%s.outputX' % mdvAmpNode, '%s.input2' % adlYNode)
    mc.connectAttr('%s.default' % adlYNode, '%s.input1' % adlYNode)
    mc.connectAttr('%s.output' % adlYNode, '%s.sy' % btwFnt) 

 
    mc.connectAttr('%s.sx' % btwJnt, '%s.input1X' % mdvAmpNode)
    mc.connectAttr('%s.scaleBtwFnt_X' % btwJnt, '%s.input2X' % mdvAmpNode)
    # mc.connectAttr('%s.outputX' % mdvAmpNode, '%s.sx' % btwFnt) 
    mc.connectAttr('%s.sz' % btwJnt, '%s.input1Z' % mdvAmpNode)
    mc.connectAttr('%s.scaleBtwFnt_Z' % btwJnt, '%s.input2Z' % mdvAmpNode)
    # mc.connectAttr('%s.outputZ' % mdvAmpNode, '%s.sz' % btwFnt) 
    mc.connectAttr('%s.sy' % btwJnt, '%s.input1Y' % mdvAmpNode)
    mc.connectAttr('%s.scaleBtwFnt_Y' % btwJnt, '%s.input2Y' % mdvAmpNode)
    # mc.connectAttr('%s.outputY' % mdvAmpNode, '%s.sy' % btwFnt) 


############################################################# New Scrips #######################################################
############################################################# New Scrips #######################################################
############################################################# New Scrips #######################################################
############################################################# New Scrips #######################################################

def selected (*args):
    sels =mc.ls(sl=True,visible=True)
    return sels

def splitName(sels = [], optionSide = True, *args):
    for each in sels:
        if ":" in each:
            objSp = each.split( ":" )
            sp = objSp[0]
            obj = objSp[-1]
        else:
            obj = each
        nameSp = obj.split( "_" )
        if len(nameSp) == 1:
            name = nameSp[0]
            side = ''
            lastName = ''      

        elif len(nameSp) == 2:
            name = nameSp[0]
            if nameSp[-1] == 'L':
                side = '_' + nameSp[-1] + '_'
                lastName = ''
            elif nameSp[-1] == 'R':
                side = '_' + nameSp[-1] + '_'
                lastName = ''
            else:
                side = '_'
                lastName = nameSp[-1]

        else:
            name = nameSp[0]
            side = '_' + nameSp[1] + '_'
            lastName = nameSp[-1]

    if optionSide == True:
        if side == 'L':
            side = side.replace(side, '_L_')
        elif side == 'R':
            side = side.replace(side, '_R_')
        if side == '':
            side == side.replace(side, '_')
    else:
        if side == '_L_':
            side = side.replace(side, 'L')
        elif side == '_R_':
            side = side.replace(side, 'R')
        elif side == '_':
            side = side.replace(side, '')

    return name, side, lastName

def duplicate(obj, name, side, elem , *args):

    geoLttGrp = mc.group( n = '%s%sGeo%sGrp' % (name, elem, side), em = True)
    objDup = mc.duplicate(obj, n = '%s%s%sGrp' % (name, elem, side))
    lists  = mc.listRelatives(objDup[0], type = 'transform', c = True, ad = True, s = False, f = True)

    for each in lists:
        nameSp = each.split("|")
        obj = nameSp[-1]
        if 'Grp' in obj:
            name, side, lastName = splitName([obj])
            newObj = mc.rename(each, '%s%s%sGrp' % (name, elem, side))
            lockAttrObj([newObj], lock = False, keyable = True)

        elif 'Geo' in obj:
            name, side, lastName = splitName([obj])
            newObj = mc.rename(each, '%s%s%sGeo' % (name, elem, side))      
            lockAttrObj([newObj], lock = False, keyable = True)

    mc.parent(objDup[0], geoLttGrp)

    return  objDup, geoLttGrp

def addLattice(sels , name, side, elem, divisions = (4,3,4) ,*args):

    LttBaseGrp = mc.group( n = '%s%sBase%sGrp' % (name, elem, side), em = True)
    lttObj = mc.lattice(sels, divisions = divisions, oc= True, n='%s%s%s%s'%(name, elem , side, elem))
    for each in lttObj:
        if 'Lattice' in each:
            mc.parent(each, LttBaseGrp)

        elif 'Base' in each:
            mc.parent(each, LttBaseGrp)
            mc.setAttr('%s.v' % each, 0)

        else:
            mc.setAttr('%s.outsideLattice' % each, 1)

    return lttObj, LttBaseGrp

def copySkinWeight(sels , *args):

    objListsA = listGeo(sels[0])
    objListsB = listGeo(sels[-1])

    i = 0
    for each in objListsA:
        mc.select(each, r = True)
        mc.select(objListsB[i], add = True)
        weightTools.copySelectedWeight()
        i += 1

def listGeo(sels = '', nameing = '_Geo', namePass = '_Grp', *args):
    if not sels:
        sels = mc.ls(sl = True)[0]
    objLists = []
    objAlls = mc.listRelatives(sels , c = True, ad = True , type = 'transform' , f = True)
    objAll = []
    if objAlls:
        for each in objAlls:
            if nameing in each:
                objAll.append(each)
        if objAll:
            for each in objAll:
                obj = each.split('|')[-1]
                if namePass in obj:
                    pass
                else:
                    objLists.append(each)
    return objLists

def listGeo3(sels = '', nameing = '_Geo', namePass = '_Grp', *args):
    if not sels:
        sels = mc.ls(sl = True)[0]
    objLists = []
    objAlls = mc.listRelatives(sels , c = True, ad = True , type = 'transform' , f = True)
    objAll = []
    if objAlls:
        for each in objAlls:
            if nameing in each:
                objAll.append(each)
        if objAll:
            for each in objAll:
                obj = each.split('|')[-1]
                if not namePass in obj:
                    if not '_Crv' in obj:
                        if not '_Geo' in obj:
                            if not '_Ikh' in obj:
                                if not '_parentC' in obj:
                                    if not '_pointC' in obj:
                                        if not '_Jnt' in obj:
                                            if not '_aimC' in obj:
                                                if not '_orientC' in obj:
                                                    if not '_scaleC' in obj:
                                                        if not '_IkHndl' in obj:
                                                            if not '_Eff' in obj:
                                                                if not '_loc' in obj:
                                                                    objLists.append(each)
    return objLists

def listGeo2(sel = '', lastName = '',*args):
    geoAll = []
    if not sel:
        sel = mc.ls(sl = True)[0]
    geo = mc.listRelatives(sel, ad = True, type = 'shape')
    for each in geo:
        if not 'Orig' in each:
            geoAll.append(each)

            ## not select on viewport 
            if lastName in each:
                mc.setAttr(each + '.overrideEnabled', 1)
                mc.setAttr(each + '.overrideDisplayType', 0)
    return geoAll

def addBlendShape(sels, *args):
    objListsA = listGeo(sels[-1])
    objListsB = listGeo(sels[0])

    i = 0
    for each in objListsA:
        mc.select(each, r = True)
        mc.select(objListsB[i], add = True)
        lrr.doAddBlendShape()
        i += 1


def setAttrSkinWeight(selsGrp, sels, value, clear, *args):
    for each in selsGrp:
        if mc.objExists(each):
            objListsB = listGeo(each)
            selsObj = objListsB
        elif mc.objExists(sels):
            selsObj = sels

        for geo in selsObj:
            if '_Geo' in geo:
                if not '_GeoShape' in geo:
                    skin = get_nodes_in_history_by_type('skinCluster', geo)
                    if skin:
                        mc.setAttr('%s.envelope' % skin, value)
                        if clear:
                            value = mc.getAttr('%s.envelope' % skin)

                            if value == 0:
                                mc.delete(skin)
                                print 'Delete >> %s SkinCluster' % skin

def createGrp(lists, *args):
    grpAll  = []
    for each in lists:
        name, side, lastName = splitName([each])
        grp = mc.group(em = True, n = '%s%s%s' % (name, side, lastName))
        grpAll.append(grp)

    return grpAll

def listNode(sels, type = 'blendShape', clear = False, *args):
    for each in sels:
        nodeCon = mc.ls(mc.listHistory(each) or [], type= type)
        if nodeCon:
            if clear:
                mc.delete(nodeCon[0])
    return nodeCon

########## setWinDowIcon ###########################
def wintes(window, path , pngIcon ,*args):
    qw = omui.MQtUtil.findWindow(window)
    widget = wrapInstance(long(qw), QWidget)
    
    # Create a QIcon object
    icon = QIcon('%s%s' % (path, pngIcon))
    
    # Assign the icon
    widget.setWindowIcon(icon)        

def createTextChar(sels, char, tyVal, *args):
    import math

    if not sels:
        sels = mc.ls(sl = True)

    bb = mc.exactWorldBoundingBox()

    yOffset = float( abs( bb[1] - bb[4] ) / 1.2 )
    xOffset = float( abs( bb[0] - bb[3] ) / 1.3 )
    xVal = 0
    yVal = 0

    txtCrv = textName(char = char, size = 1)
    mc.select( txtCrv , r = True )
    txtBb = mc.exactWorldBoundingBox()
    mc.xform( txtCrv , cp = True )
                
    objWidth = abs(bb[0] - bb[3])
    txtWidth = abs(txtBb[0] - txtBb[3])

    tmpLoc = mc.spaceLocator()
    sclVal = objWidth/txtWidth
    mc.scale( sclVal , sclVal , sclVal , tmpLoc , r = True )
    mc.move( 0 , bb[1]+yVal , bb[5] , tmpLoc , r = True )

def createJnt(name, side, lastName, nums):
    name = name
    side = side
    lastName = lastName
    nums = nums
    grpAll = []
    ctrlAll = []
    jntAll = []
    jntGrp = mc.group(n = '%s%s_Grp' % (name, lastName), em = True)
    
    for i in range(nums):
        jnt = mc.createNode ('joint', n = '%s%s%s%s' % (name, i+1, side, lastName))
        mc.parent(jnt, jntGrp)
        mc.setAttr('%s.tz' % jnt, nums)
        grp = mc.group(n = '%s%s%sGrp' % (name, i+1, side), em = True)
        mc.delete(mc.parentConstraint(jnt, grp, mo = False))
        ctrl = mc.circle( nr=(0, 0, 1), c=(0, 0, 0) ,n = '%s%s%sCtrl' % (name, i+1, side))[0]
        mc.makeIdentity (ctrl , apply = True)
        mc.delete(mc.parentConstraint(jnt, ctrl, mo = False))
        mc.parent(ctrl, grp)
        grpAll.append(grp)
        ctrlAll.append(ctrl)
        mc.parentConstraint(ctrl, jnt, mo = True)
        mc.scaleConstraint(ctrl, jnt, mo = True)
        if len(ctrlAll) > 1:
            mc.parent(grpAll[i], ctrlAll[i-1])
        nums += 1
        

## Search and Select Delete key Attribute
# defaultAttr(search = False, lists = ['*:Jaw*_Ctrl'], attr = ['sx', 'sy', 'sz'], value = 1)
def defaultAttr(search = True, lists = [], attr = [], value = 1):
    if search:
        for each in lists: 
            objLists = mc.ls(each)
    else:
        objLists = lists

    for each in objLists:
        if not 'Root' in each:
            for i in attr:
                nodes = mc.keyframe('%s.%s' % (each, i), query = True,n = True)
                if nodes:
                    mc.delete(nodes)
                mc.setAttr('%s.%s' % (each, i), value)

def addFacialAllLoc(attrName ,min , max, at, *args):
    facialLoc = 'FacialAll_Loc'
    if not mc.objExists(facialLoc):
        FacialAllGrp = mc.group( em = True, n = 'FacialAllLoc_Grp')
        facialLoc = mc.spaceLocator(n = facialLoc)[0]
        mc.parent(facialLoc, FacialAllGrp)
    else:
        FacialAllGrp = mc.listRelatives(facialLoc, p = True)
        
    mc.setAttr('%sShape.localPositionX' % facialLoc, l = True, k = False , cb = False)
    mc.setAttr('%sShape.localPositionY' % facialLoc, l = True, k = False , cb = False)
    mc.setAttr('%sShape.localPositionZ' % facialLoc, l = True, k = False , cb = False)
    mc.setAttr('%sShape.localScaleX' % facialLoc, l = True, k = False , cb = False)
    mc.setAttr('%sShape.localScaleY' % facialLoc, l = True, k = False , cb = False)
    mc.setAttr('%sShape.localScaleZ' % facialLoc, l = True, k = False , cb = False)

    for each in attrName:
        addAttrCtrl (ctrl = '{}Shape'.format (facialLoc), elem = [each], min = min, max = max, at = at)
        lockAttrObj(listObj = [facialLoc], lock = True, keyable = False)
    return FacialAllGrp , facialLoc
def listsAttrAdd(obj = [], *args):
    attrUd = mc.listAttr('EyeMaskUpUiRig_L_CtrlShape', k = True, ud = True)
    return attrUd

def listAttribute(obj = [], *args):
    # listsAttr = []
    for each in obj :
        listsAttr = mc.listAttr( each, k = True, v = True, l = False, ud = True)
    return listsAttr
def listAttributeOptions(obj = [], k = True, v = True, l = False, *args):
    for each in obj :
        listsAttr = mc.listAttr( each, k = k, v = v, l = l)
    return listsAttr
def createIkh(strJnt, endJnt ,name, sol = 'ikSplineSolver', *args):
    ''' ikSplineSolver
        ikRPsolver
        ikSCsolver'''

    mc.ikHandle(sj= strJnt, ee= endJnt, n= '{}Ikh'.format(name), sol= sol, ccv = True, rootOnCurve = True, parentCurve= False)[0]
    ikHandle = '{}Ikh'.format(name)
    return ikHandle

def createIkhSpine (strJnt, endJnt ,curve , name, sol = 'ikSplineSolver', *args):
    ikhSpine = mc.ikHandle(n = name, sj = strJnt, ee = endJnt, sol = sol, ccv = False, pcv = False, c = curve, parentCurve= False)
    return name
def checkIkSplineSolver(name = '', elem = '' , side = '', ikHandleNode = '',*args):
    ## naming
    if side == '_':
        side = ''
    else:
        side = '_' + side 
    newName = '{}{}{}'.format(name, elem, side)
    ## Check Connection Node
    connectionNode = checkConnect(object = ikHandleNode, attr = 'ikSolver')
    if mc.objExists(connectionNode[0]) :
        mc.disconnectAttr(connectionNode[0], '{}.ikSolver'.format(ikHandleNode))

    ikSolverNode = mc.createNode('ikSplineSolver',  n = '{}IkSpineSolver_Ikh'.format(newName))
    mc.connectAttr( '{}.message'.format(ikSolverNode), 'ikSystem.ikSolver',na = True)
    mc.connectAttr( '{}.message'.format(ikSolverNode), '{}.ikSolver'.format(ikHandleNode))


def cleanOrig(lists = [], *args):
    if not lists:
        lists = mc.ls(sl = True)
    for each in lists:
        objPath = mc.listRelatives(each, f = True, p = True)
        geoLists = listGeo(sels = objPath)
        mc.select(geoLists, r = True)
        mc.pickWalk( d= 'down')
        shapeOrigObj = mc.pickWalk(d = 'right')
        if shapeOrigObj:
            if 'Orig' in shapeOrigObj:
                mc.delete(shapeOrigObj)

def checkConnectFacialBsnNode(facialBshNode = '', *args):
    outputChecks = []
    inputChecks = []
    if facialBshNode:
        obj = mc.listAttr(facialBshNode + '.w', m = True)
        for each in obj:
            if 'Btw' in each:
                pass
            elif 'Geo' in each:
                pass
            else:
                outputObj = mc.listConnections('{}.{}'.format(facialBshNode, each), p = True)
                if not outputObj:
                    outputObj = None
                else:
                    if 'output' in outputObj[0]:
                        outputObj = outputObj[0].replace('.output', '.output.output')
                    else:
                        outputObj = outputObj[0]
                b = facialBshNode + '.' + each 
                outputChecks.append(outputObj)
                inputChecks.append(b)

    return outputChecks, inputChecks

def fixConnectionfacialNode(bsnNode = '', *args):
    obj = mc.listAttr(bsnNode + '.w', m = True)
    for each in obj:
        if 'Btw' in each:
            pass
        elif 'Geo' in each:
            pass
        else:
            lists = mc.listConnections('{}.{}'.format(bsnNode, each), p = True)[0]
            mc.disconnectAttr(lists, '{}.{}'.format(bsnNode,each))


def disConToObj(objA = '', objB = '', connectionsObj = True, *args):
    if connectionsObj:
        mc.connectAttr(objA, objB)
    else:
        mc.disconnectAttr(objA, objB)


def listsConnectBshNode(outputChecks = [], inputChecks = [], connectionsObj = False, *args):
    for i in range(len(inputChecks)):       
        inputBsnNodeName = inputChecks[i].split('.')[-1]
        inputBsnName = inputBsnNodeName.split('_')[0]
        inputSideName = inputBsnNodeName.split('_')[-1]

        if not outputChecks[i] == None:
            objA =outputChecks[i]
        else:
            objA = 'FacialAll_LocShape' + '.' + inputBsnName
        if connectionsObj:
            if not outputChecks == None:
                conCheck = mc.listConnections(inputChecks[i], s = True)
                if conCheck:
                    print 'Already is Connected'
                else: 
                    if 'L' in inputSideName:
                        objA = 'FacialAll_LocShape' + '.' + inputBsnNodeName      
                    elif 'R' in inputSideName:
                        objA = 'FacialAll_LocShape' + '.' + inputBsnNodeName   
                    else:
                        objA = 'FacialAll_LocShape' + '.' + inputBsnName      
            else:
                if 'L' in inputSideName:
                    objA = 'FacialAll_LocShape' + '.' + inputBsnNodeName      
                elif 'R' in inputSideName:
                    objA = 'FacialAll_LocShape' + '.' + inputBsnNodeName      
                else:
                    objA = 'FacialAll_LocShape' + '.' + inputBsnName     
                objA = 'FacialAll_LocShape' + '.' + inputBsnNodeName
            conCheck = mc.listConnections(inputChecks[i], s = True)
            if conCheck:
                print 'Already is Connected'  
            else:    
                disConToObj(objA = objA , objB = inputChecks[i], connectionsObj = True)                      
        else:
            conCheck = mc.listConnections(inputChecks[i], s = True)
            if not conCheck:
                print 'Already is Disconnected', 
            else:
                disConToObj(objA = outputChecks[i], objB = inputChecks[i], connectionsObj = False)  


def conAndDisconBshNode (   facialLoc = 'FacialAll_Loc',
                            bshName = 'FacialAll_Bsn',
                            connection = True,*args):

    BshRigStillGrp = 'BshRigStill_Grp'
    bshNameRefObj = mc.ls('Facial*_Bsn')
    # print bshNameRefObj, 'bsh'
    i = 0
    addAttrLoc = []
    attrListAll = []

    if not connection:
        for eachFclNode in bshNameRefObj:        
            if not mc.objExists(facialLoc):
                outputChecks, inputChecks = checkConnectFacialBsnNode(facialBshNode = eachFclNode)
                if eachFclNode == bshName:
                    bshNode = mc.listAttr(eachFclNode + '.w', m = True)
                    for eachBshNode in bshNode:
                        if 'Btw' in eachBshNode:
                            pass
                        elif 'Geo' in eachBshNode:
                            pass
                        else:
                            attrLists = mc.listConnections('%s.%s' % (eachFclNode, eachBshNode) ,p  = True)
                            if len(attrLists) == 1:
                                attrList = attrLists[0]
                            else:
                                attrList = attrLists[-1]
                            attrListAll.append(attrList)
                            mc.disconnectAttr(attrList, '%s.%s' %(eachFclNode ,eachBshNode))
                            addAttrLoc.append(eachBshNode)

                else:
                    listsConnectBshNode(outputChecks = outputChecks, inputChecks = inputChecks, connectionsObj = False )   

                FacialAllGrp , FacialAllLoc = addFacialAllLoc(addAttrLoc ,min = 0 , max = 10, at = 'float')

                if not mc.objExists(BshRigStillGrp):
                    BshRigStillGrp = mc.createNode('transform', n = 'BshRigStill_Grp', em = True)
                    mc.parent(FacialAllGrp, BshRigStillGrp)
                    mc.select(cl =True)
                else:
                    mc.parent(FacialAllGrp, BshRigStillGrp)
                    mc.select(cl =True)

                for eachAttrListAll in attrListAll:
                    mc.connectAttr(eachAttrListAll, '%sShape.%s' % (FacialAllLoc, addAttrLoc[i]))
                    i += 1
            else:
                outputChecks, inputChecks = checkConnectFacialBsnNode(facialBshNode = eachFclNode)
                listsConnectBshNode(outputChecks = outputChecks, inputChecks = inputChecks, connectionsObj = False )   

    else: ### True
        FacialLocShape = '%sShape' % facialLoc
        attrLoc = listAttribute(obj = [FacialLocShape])
        for eachBshNameRefObj in bshNameRefObj:
            outputChecks, inputChecks = checkConnectFacialBsnNode(facialBshNode = eachBshNameRefObj)

            listsConnectBshNode(outputChecks = outputChecks, inputChecks = inputChecks, connectionsObj = True )   


def renameNumberOrString(countObj = True, intObj = False, startNum = 34,nameObj = 'Kenxx' , lastName = '_Jnt'):
    obj = mc.ls(sl = True)
    n = len(obj)
    F = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H','I', 'J', 'K', 'L', 'M', 'N', 'O', 'P']
    jt = []        
    for i in range(n):
        if countObj == True:
            start = str(i + startNum)
        if intObj == True:
            start = str(F[i])
        a = mc.rename(obj[i],'{}{}{}'.format(nameObj, start ,lastName))
        jt.insert(i, a)
# renameNumberOrString(countObj = True, intObj = False, startNum = 1,nameObj = 'Ken' , lastName = '_Grp')


def createFacialAllLoc(facialLoc = 'FacialAll_Loc', baseBshNode = [],  *args):
    ## Create FacialAll_Loc For Base Attribute Bsh 
    if not mc.objExists(facialLoc):
        FacialAllGrp , FacialAllLoc = addFacialAllLoc(baseBshNode ,min = 0 , max = 10, at = 'float')
        print FacialAllGrp ,'>>',  FacialAllLoc
    if not mc.objExists('BshRigStill_Grp'):
        BshRigStillGrp = mc.createNode('transform', n = 'BshRigStill_Grp', em = True)
        mc.parent(FacialAllGrp, BshRigStillGrp)
        mc.select(cl =True)
    else:
        BshRigStillGrp = 'BshRigStill_Grp'
        checkFacialLoc = mc.objExists(facialLoc)
        if checkFacialLoc:
            checkPar = mc.listRelatives(facialLoc, p = True, f = True)[0].split('|')[1]
        if not checkPar == BshRigStillGrp:
            mc.parent(FacialAllGrp, BshRigStillGrp)
            mc.select(cl =True)   
    return facialLoc

def checkConnect(object = '', attr = '', *args):
    ## Check Connect of FacialBshNode
    conCheck = mc.listConnections(object + '.' + attr , p = True, s = True)
    if conCheck:
        conCheck = [item for item in conCheck if mc.objectType(item) != 'combinationShape' ]
    return conCheck

def checkNumberLayerTexture(layterTexture = 'EyeSeqAll_L_Lyt', *args):
    chanelCheck = ''
    lists = mc.listConnections(layterTexture)
    if lists:
        for eachFile in lists:
            if eachFile:
                for i in range(0, 20):
                    checkConnecAttr = checkConnect(object = layterTexture, attr = 'inputs[{}].color'.format(i))
                    if checkConnecAttr:
                        indexNumber = i
                        break
                    else:
                        indexNumber = 0
    else:
        indexNumber = 0
    return indexNumber


def checkIndexOrderLayerTexture(layterTexture = 'EyeSeqAll_L_Lyt', *args):
    orderAll =  mc.getAttr('{}.inputs'.format(layterTexture), mi = 1)
    if orderAll:
        if int(orderAll[-1]) == 0:
            indexOrder = 0
        else:
            indexOrder = int(orderAll[-1])

    return indexOrder

def removeMultiInstanceIndexOrderLayerTexture(layterTexture = ['EyeSeqAll_L_Lyt'], removeMultiInstance = True, *args):

    objConnect = []
    if layterTexture:
        layterTexture = layterTexture
    else:
        layterTexture = mc.ls('*_Lyt')

    for eachLyt in layterTexture:
        orderAll =  mc.getAttr('{}.inputs'.format(eachLyt), mi = 1)
        if removeMultiInstance:
            for i in range(len(orderAll)):
                colorAlpha = ['color', 'alpha']
                for x in range(len(colorAlpha)):
                    connectLists = checkConnectLists(object = eachLyt, attr = ['inputs[{}].{}'.format(orderAll[i], colorAlpha[x])])
                    
                    if not connectLists:
                        mc.removeMultiInstance ('{}.inputs[{}]'.format(eachLyt, orderAll[i]), b = True)
                    else:
                        connectLists = connectLists
                        objConnect.append(connectLists[0])
            orderAll =  mc.getAttr('{}.inputs'.format(eachLyt), mi = 1)

            connectLists = orderAll

        else:
            for i in range(len(orderAll)):
                for colorAlpha in ['color', 'alpha']:
                    connectLists = checkConnectLists(object = eachLyt, attr = ['inputs[{}].{}'.format(orderAll[i], colorAlpha)])
                    if connectLists:
                        objConnect.append(connectLists[0])
                    else:
                        objConnect.append('None')

            connectLists = orderAll
    # print layterTexture, '::', connectLists
    return layterTexture, connectLists, objConnect

# def changeIndexOrder(lytNode = '', curIndex = [], changeIndex = [], *args):

#     layterTexture, connectLists, objConnect = removeMultiInstanceIndexOrderLayerTexture(layterTexture = [lytNode], removeMultiInstance = True)
#     if not curIndex:
#         curIndex = connectLists
#     #[0L, 2L, 3L, 4L, 5L, 6L]
#     #[0L, 4L, 5L, 6L, 2L, 3L]
#     numberOdd = []
#     numberEven = []
#     blendModeOption = []

#     for u in range(len(objConnect)):

#         if u % 2 == 0:
#             ## odd 1
#             numberOdd.append(u)
#         else:
#             ## even 2
#             numberEven.append(u)
#     for i in range(len(curIndex)):

#         # print objConnect[numberOdd[i]], '...disCur..', '{}.inputs[{}].{}'.format(lytNode, curIndex[i], 'color')     
#         # print objConnect[numberEven[i]], '...disCur..', '{}.inputs[{}].{}'.format(lytNode, curIndex[i], 'alpha')        

#         mc.disconnectAttr(objConnect[numberOdd[i]], '{}.inputs[{}].{}'.format(lytNode, curIndex[i], 'color'))
#         mc.disconnectAttr(objConnect[numberEven[i]], '{}.inputs[{}].{}'.format(lytNode, curIndex[i], 'alpha'))
#         blendModeOption.append(mc.getAttr('{}.inputs[{}].blendMode'.format(lytNode, curIndex[i])))

#     for i in range(len(changeIndex)):

#         # print objConnect[numberOdd[i]], '...conChange..', '{}.inputs[{}].{}'.format(lytNode, changeIndex[i], 'color')     
#         # print objConnect[numberEven[i]], '...conChange..', '{}.inputs[{}].{}'.format(lytNode, changeIndex[i], 'alpha')        

#         mc.connectAttr(objConnect[numberOdd[i]], '{}.inputs[{}].{}'.format(lytNode, changeIndex[i], 'color'))
#         mc.connectAttr(objConnect[numberEven[i]], '{}.inputs[{}].{}'.format(lytNode, changeIndex[i], 'alpha'))
#         mc.setAttr('{}.inputs[{}].blendMode'.format(lytNode, changeIndex[i]), blendModeOption[i])


def changeIndexOrder(lytNode = '', curIndex = [],changeIndex = [], *args):
    orderAll = mc.getAttr('{}.inputs'.format(lytNode), mi = 1)
    # print orderAll
    allConList = []
    for num in orderAll:
        color = mc.listConnections('{}.inputs[{}].color'.format(lytNode, num),s=1,d=1,p=1,scn=1,c=1)
        alpha = mc.listConnections('{}.inputs[{}].alpha'.format(lytNode, num),s=1,d=1,p=1,scn=1,c=1)
        isVisible = mc.listConnections('{}.inputs[{}].isVisible'.format(lytNode, num),s=1,d=1,p=1,scn=1,c=1)
        blndMode = mc.getAttr('{}.inputs[{}].blendMode'.format(lytNode, num))
        allConList.append([color,alpha,blndMode,isVisible])
    #     print color
    #     print alpha
    # print allConList
    #[0L, 2L, 3L, 4L, 5L, 6L]
    #[0L, 4L, 5L, 6L, 2L, 3L]

    for inputs in allConList:
        if inputs[0]:
            mc.disconnectAttr(inputs[0][1],inputs[0][0])
        if inputs[1]:
            mc.disconnectAttr(inputs[1][1],inputs[1][0])
        if inputs[3]:
            mc.disconnectAttr(inputs[3][1],inputs[3][0])

    for idx,num in enumerate(orderAll):
        inputs = allConList[idx]
        nIdx = changeIndex.index(num)
        # print idx,nIdx,changeIndex[nIdx]
        if inputs[0]:
            mc.connectAttr(inputs[0][1],'{}.inputs[{}].color'.format(lytNode,orderAll[nIdx]))
        if inputs[1]:
            mc.connectAttr(inputs[1][1],'{}.inputs[{}].alpha'.format(lytNode,orderAll[nIdx]))
        if inputs[3]:
            mc.connectAttr(inputs[3][1],'{}.inputs[{}].isVisible'.format(lytNode,orderAll[nIdx]))

        mc.setAttr('{}.inputs[{}].blendMode'.format(lytNode, orderAll[nIdx]), inputs[2])


def checkConnectLists(object = '', attr = [], *args):
    attrLists = []
    ## Check Connect of FacialBshNode
    for eachAttr in attr:
        conCheck = mc.listConnections(object + '.' + eachAttr , p = True, s = True, d = False, scn = True)
        # if conCheck:
            # for eachConCheck in conCheck:
            #     if 'unit' in eachConCheck:
            #         newObject = eachConCheck.replace('.output', '')
            #         conCheck = mc.listConnections(newObject + '.input' , p = True, s = True, d = False)

        if conCheck:
            conCheck = [item for item in conCheck if mc.objectType(item) != 'combinationShape' ]
            attrLists.append(conCheck[0])

    return attrLists

def clear_attr(eachBshNode):
    bshAttrNode = mc.listAttr(eachBshNode + '.w', m = True)
    data = []
    dataLast = []
    for ix in bshAttrNode:
        attr = ix.replace('_%s'%(ix.split('_')[-1]), '')
        attrBase = ix.replace(attr, '')
        data.append(attr)
        dataLast.append(attrBase)

    return data, dataLast
    
def connectOrDisconnect(facialLoc = 'FacialAll_Loc', bshNode = [], connectBsh = True):
    baseBshNode = listBaseBsh.listBaseBshNode()
    facialLoc = createFacialAllLoc(facialLoc = facialLoc, baseBshNode = baseBshNode)

    ## Check Connect of FacialBshNode
    for eachBshNode in bshNode:
        if eachBshNode == 'FacialAll_Bsn':
            bshAttrBshNode= mc.listAttr(eachBshNode + '.w', m = True)
            dataLast = ''

        if not eachBshNode == 'FacialAll_Bsn':
            bshAttrBshNode, dataLast = clear_attr(eachBshNode)
            dataLast = dataLast[0]

        for checkBaseBshNode in bshAttrBshNode:
            conObj = checkConnect(object = eachBshNode, attr = checkBaseBshNode + dataLast)

            if conObj :
                conCheck = conObj[0]
                mc.disconnectAttr(conCheck, '{}.{}{}'.format(eachBshNode ,checkBaseBshNode , dataLast))


                conCheckObj = conCheck.split('.')[0]
                if not conCheckObj == facialLoc:
                    try:
                        mc.connectAttr(conCheck, '{}Shape.{}'.format(facialLoc, checkBaseBshNode))
                    except:pass
                else:
                    conCheck = mc.listConnections(facialLoc + 'Shape.' + checkBaseBshNode , d = True, s = True)
                    if conCheck:
                        conCheck = [item for item in conCheck if mc.objectType(item) != 'combinationShape' ]
                        if not conCheck:
                            try:
                                mc.connectAttr(conCheck, '{}Shape.{}'.format(facialLoc, checkBaseBshNode))
                            except: pass
                        else:
                            print 'Already is Connected'

        ## FacialAll_Bsn Node
        for eachAttrBshNode in bshAttrBshNode:
            for i in range(len(bshAttrBshNode)):
                if eachAttrBshNode == bshAttrBshNode[i]:

                    ## Connect Bsh Node------------------------------------------------------------------
                    if connectBsh == True:
                        conCheck = mc.listConnections(eachBshNode + '.' + bshAttrBshNode[i] + dataLast , d = True, s = True)
                        if conCheck:
                            conCheck = [item for item in conCheck if mc.objectType(item) != 'combinationShape' ]
                            if not conCheck:
                                try:
                                    mc.connectAttr('{}Shape.{}'.format(facialLoc, eachAttrBshNode), '{}.{}{}'.format(eachBshNode, bshAttrBshNode[i], dataLast))
                                except:pass
                            else:
                                print 'Already is Connected'

                        else:
                            try:
                                # print '{}Shape.{}'.format(facialLoc, eachAttrBshNode), '{}.{}{}'.format(eachBshNode, bshAttrBshNode[i], dataLast)
                                mc.connectAttr('{}Shape.{}'.format(facialLoc, eachAttrBshNode), '{}.{}{}'.format(eachBshNode, bshAttrBshNode[i], dataLast))
                            except: pass

                    ## Disconnect Bsh Node------------------------------------------------------------------
                    if connectBsh == False:
                        conCheck = mc.listConnections(eachBshNode + '.' + bshAttrBshNode[i] + dataLast, d = True, s = True)
                        if conCheck:
                            conCheck = [item for item in conCheck if mc.objectType(item) != 'combinationShape' ]
                            if not conCheck:
                                print 'Already is Disconnected'
                            else:
                         
                                try:
                                    mc.disconnectAttr('{}Shape.{}'.format(facialLoc, eachAttrBshNode), '{}.{}{}'.format(eachBshNode, bshAttrBshNode[i], dataLast))
                                except: pass


def createJointOnCurve(cur_name = '', name = '', elem = '', side = '',  span_curve = 20, degree = 3,*args):

    if side:
        if side == 'L':
            side = '_' + side + '_'
        elif side == 'R':
            side = '_' + side + '_'
        elif side == '_':
            pass
    else:
        side = '_'
        
    # cru = mc.polyToCurve(form =2, degree=3) 
    mc.rebuildCurve(cur_name, ch = 0, rpo =1 ,end = 1, rt=0, keepRange =0, keepEndPoints=True,kt=0 ,spans = span_curve, degree=degree, tol=0.01) 
    pm.makeIdentity ( cur_name , apply = True ) 

    jointAll = []
    jointFnt = []
    jointBck = []
    if not elem:
        elems = ['Fnt', 'Bck']
    else:
        elems = [elem]

    halfSpan = span_curve // 2
    for cv in range(0, span_curve): 
        if cv < halfSpan:
            elem = elems[0]

        cv_curve = '%s.cv[%d]'%(cur_name, cv)
        poseLocator = mc.xform(cv_curve, q=True, t=True) 
        jnt = mc.joint (cv_curve, p=(poseLocator[0],poseLocator[1],poseLocator[2]), name= '{}{}{}{}Jnt'.format(name, elem , cv + 1 , side)) 
        mc.parent (jnt, w=True) 
        if not elem:
            if cv < halfSpan:
                jointFnt.append(jnt)
            else:
                jointBck.append(jnt)
        else:
            jointFnt = None
            jointBck = None
            jointAll.append(jnt)

    return jointFnt, jointBck, jointAll


def createJointOnCurveNeedJoint(cur_name = '', name = '', elem = '', side = '',  span_curve = 20, degree = 3, needJoint = 3,*args):

    if side:
        if side == 'L':
            side = '_' + side + '_'
        elif side == 'R':
            side = '_' + side + '_'
        elif side == '_':
            pass
    else:
        side = '_'
        
    # cru = mc.polyToCurve(form =2, degree=3) 
    # mc.rebuildCurve(cur_name, ch = 0, rpo =1 ,end = 1, rt=0, keepRange =0, keepEndPoints=True,kt=0 ,spans = span_curve, degree=degree, tol=0.01) 
    # pm.makeIdentity ( cur_name , apply = True ) 

    jointAll = []
    jointFnt = []
    jointBck = []
    if not elem:
        elems = ['Fnt', 'Bck']
    else:
        elems = [elem]
 
    
    # halfSpan = int(math.ceil(span_curve / needJoint))
    lenCv = []
    halfSpan = int(span_curve / needJoint) + (span_curve % needJoint > 0)

    for cv in range(0, span_curve, halfSpan): 
        if '%s.cv[%d]'%(cur_name, cv):
            lenCv.append('%s.cv[%d]'%(cur_name, cv))

    if len(lenCv) < needJoint:
        span_curve = (needJoint * halfSpan)-2
        mc.rebuildCurve(cur_name, ch = 0, rpo =1 ,end = 1, rt=0, keepRange =0, keepEndPoints=True,kt=0 ,spans = span_curve, degree=degree, tol=0.01) 
        pm.makeIdentity ( cur_name , apply = True ) 
        span_curve = span_curve + 2
    for cv in range(0, span_curve , halfSpan): 
        if cv < halfSpan:
            elem = elems[0]
        cv_curve = '%s.cv[%d]'%(cur_name, cv)
        poseLocator = mc.xform(cv_curve, q=True, t=True) 
        jnt = mc.joint (cv_curve, p=(poseLocator[0],poseLocator[1],poseLocator[2]), name= '{}{}{}{}Jnt'.format(name, elem , cv + 1 , side)) 
        mc.parent (jnt, w=True) 
        if not elem:
            if cv < halfSpan:
                jointFnt.append(jnt)
            else:
                jointBck.append(jnt)
        else:
            jointFnt = None
            jointBck = None
            jointAll.append(jnt)

    return jointFnt, jointBck, jointAll

def bindSkinCluster(jntObj = [], obj = [], *args):
    for each in obj:
        skinNode = getSkinCluster(mesh = [each])
        if not skinNode:
            scls = mc.skinCluster(jntObj, each, name='{}_skinCluster'.format(each), toSelectedBones=True, bindMethod=0, skinMethod=0, normalizeWeights=1)[0]
        else:
            mc.select(each)
            scls = mc.skinCluster(edit = True, addInfluence = jntObj, weight = 0)


def wireObj (wireGeoObj = '', wireObj = [],name = '', elem = '', side = '', *args):
    if side:
        if side == 'L':
            side = '_' + side + '_'
        elif side == 'R':
            side = '_' + side + '_'
        elif side == '_':
            pass
    else:
        side = '_'
    wireNode = mc.wire(wireGeoObj,w = wireObj, n='{}{}{}Wire'.format(name , elem , side))
    return wireNode

def rebuildCrv(cur_name = [], span_curve = 10 , delHis = True, *args):
    for eachCrv in cur_name:
        a = mc.rebuildCurve(eachCrv, ch = 0, rpo =1 ,end = 0, rt=0, keepRange =0, keepEndPoints=True,kt=0 ,spans = span_curve, degree=3, tol=0.01) 
        if delHis:
            pm.makeIdentity ( eachCrv , apply = True ) 


def centerPivotObj (obj = [], *args):
    for each in obj:
        lockAttrObj(listObj = [each], lock = False,keyable = True)
        pm.makeIdentity ( each , apply = True ) ;    
        pm.delete ( each , ch = True ) ;
        pm.xform ( each , cp = True ) ;
    # return centerPivotObj

def checkBlendShape(selGrp = 'MdGeo_Grp', locoName = '', *args):
    nameSpObj = selGrp.split(':')[0]
    listBshGeo = dict()
    geoAll = listGeo(sels = selGrp, nameing = '_Geo', namePass = '_Grp')
    for each in geoAll:
        bshNode = get_nodes_in_history_by_type('blendShape', each)
        if bshNode:
            geoBsh = mc.listAttr(bshNode + '.w', m = True)

            # print geoBsh,'>>>>', 'geoBsh'
            if len(geoBsh) != 0:
                geoBase = (each.split('|')[-1]).split(':')[-1]

                if not locoName:
                    locoName = 'testOa'

                ## Connect BlendShape
                bshGeo = []
                for eachBshGeo in geoBsh:
                    geoName = '{}:{}_{}'.format(nameSpObj, eachBshGeo , locoName)
                    bshGeo.append(geoName)

                newGeo = '{}_{}Geo'.format(geoBase , locoName)

                bshName = geoBase.replace('_Geo', '')
                mc.select(bshGeo, r = True)   
                mc.select(newGeo, add= True)
                lrr.doAddBlendShape()

                ## Check Bpm Node
                if 'Bpm' in bshGeo:
                    skinNode = get_nodes_in_history_by_type('skinCluster', newGeo)
                    mc.setAttr ('{}.envelope'.format(skinNode), 0)
                # bshLists = mc.blendShape(name = '{}AgentBsh_Geo'.format(bshName))

            listBshGeo[geoBase] = [bshNode, geoBsh[0]]

    return listBshGeo
'''
listBsh = checkBlendShape(selGrp = 'MdGeo_Grp')

for key,value in listBsh.iteritems():
    print key,'::', value

    TeethUpr_Geo :{ 'bsh': TeethUpr_Bsn,
                    'Fcl': TeethUprFclRig_Geo }

'''

'''
from rf_utils import publish_info
from rf_utils.context import context_info

asset = context_info.ContextPathInfo()
asset.context.update(res='md')
publishInfo = publish_info.PreRegister(asset)
'''
def proceessLists (publishInfo):
    process = publishInfo.entity.process
    processSp  = process.split('_')

    if len(processSp) == 1:
        processName = processSp[0]
        processSubName = ''
        processNameSp = processName.split('Rig')[0]
        processObj = processNameSp.capitalize()

    else:
        processName = processSp[0]
        processSubName = '_' + processSp[-1]
        processNameSp = processName.split('Rig')[0]
        processObj = processNameSp.capitalize()

    return processName, processNameSp, processObj, processSubName


def renameLocoAgen(*args):

    fileName = mc.file(q=True,sn=True)
    if fileName:
        charName = fileName.split('/')[5]
        
    rigGrp = mc.ls('Rig_Grp')[0]
    children = cmds.listRelatives(rigGrp, ad = True)
    children.append(rigGrp)
    for each in children:
        obj = each.split('|')[-1]
        newObj = mc.rename(obj,'{}_{}_Loco'.format(obj, charName))


def cleanGroupEmpty(*args):
    rigGrp = mc.ls('Skin_Grp')[0]
    jntAll = []
    children = mc.listRelatives(rigGrp, c = True)
    for each in children:
        if '_Grp' in each:
            jntList = mc.listRelatives(each, ad = True, type = 'joint')
            if not jntList == None:
                for jntEach in jntList:
                    mc.parent(jntEach, 'Root_Jnt')
                    jntAll.append(jntEach)
            ## Delete Group empty
            mc.delete(each)

def createFolderSource(path = '', fldName = '',*args):
    '''P:/SevenChickMovie/asset/work/char'''
    # hero = entityFirst.split('/')[3]
    # entityFirstHero = entityFirst.replace(hero, workOrPubl)
    charPath = r'{}'.format(path)
    heroPath = r'{}/{}'.format(charPath, fldName)
    os.mkdir(heroPath)

def createFolder(entityFirst = '', workOrPubl = '',  charName = '',fldName = '',*args):
    '''P:/SevenChickMovie/asset/work/char'''
    hero = entityFirst.split('/')[3]
    entityFirstHero = entityFirst.replace(hero, workOrPubl)
    charPath = r'{}/{}/rig'.format(entityFirstHero, charName)
    heroPath = r'{}/{}'.format(charPath, fldName)
    os.mkdir(heroPath)

def createFolderPath(entityFirst = '', workOrPubl = '',  charName = '',createProcess = ['skel', 'ctrl', 'bodyRig'], *args):
    a = context_info.ContextPathInfo()

    hero = entityFirst.split('/')[3]
    entityFirstHero = entityFirst.replace(hero, workOrPubl)
    path = entityFirstHero
    assetPath = '%s/%s/rig' % (path, charName)
    
    for process in createProcess: 
        processPath = '%s/%s' % (assetPath, process)
        processMayaPath = '%s/%s/maya' % (assetPath, process)
        if not os.path.exists(processPath): 
            if process == 'data':
                os.makedirs(processPath)
            else:
                os.makedirs(processPath)
                os.makedirs(processMayaPath)



def maximAToe():
    ## Generate Control Ball of MaximA
    # from utaTools.utapy import utaCore
    # reload(utaCore)
    toeBend = ['ToeBendIkPivZr_L_Grp', 'ToeBendIkPivZr_R_Grp']
    toeIkCtrl = ['ToeIkCtrlZr_L_Grp', 'ToeIkCtrlZr_R_Grp']
    newParent = ['ToeBendIkPiv_L_Grp', 'ToeBendIkPiv_R_Grp']
    newPivOffs = ['ToeBendIkPivOffs_L_Grp', 'ToeBendIkPivOffs_R_Grp']
    for i in range(len(toeBend)):
        newPivOffsGrp = mc.group(n = newPivOffs[i], em = True)
        ctrlObj = mc.listRelatives(toeIkCtrl[i], c = True)[0]
        
        snapObj(objA = toeBend[i], objB = newPivOffsGrp)
        mc.parent(newPivOffsGrp, toeBend[i])
        mc.parent(newParent[i], newPivOffsGrp)
        mc.connectAttr('{}.rotateX'.format(ctrlObj), '{}.rotateX'.format(newPivOffsGrp))

        mc.parentConstraint(toeBend[i], toeIkCtrl[i], mo = True)
        mc.scaleConstraint(toeBend[i], toeIkCtrl[i], mo = True)
        
        # utaCore.lockAttr(listObj = [ctrlObj], attrs = ['tx', 'ty', 'tz', 'ry', 'rz','sx', 'sy', 'sz'],lock = True, keyable = False)

def reConstraint():
    obj = mc.ls(sl = True)
    attrs = ['tx', 'ty', 'tz', 'rx', 'ry', 'rz']
    parScaNode = []
    for each in obj:
        parGrp = mc.listRelatives(each, p = True)[0]
        parGrpTop = mc.listRelatives(parGrp, p = True)[0]
        basePar = mc.listRelatives(parGrp, ad = True, type = 'constraint')
        baseGrpPar = mc.listRelatives(parGrpTop, ad = True, type = 'constraint')
        if basePar:
            baseParJnt = mc.listConnections(basePar[0], type = 'joint')[0]
            mc.delete(basePar)
            for i in range(len(attrs)):
                mc.setAttr('{}.{}'.format(parGrp, attrs[i]),0)

            mc.parentConstraint(baseParJnt, parGrp, mo = True)
            # mc.scaleConstraint(baseParJnt, parGrp, mo = True)
        elif baseGrpPar:
            baseParJnt = mc.listConnections(baseGrpPar[0], type = 'joint')[0]
            mc.delete(baseGrpPar)
            for i in range(len(attrs)):
                mc.setAttr('{}.{}'.format(parGrpTop, attrs[i]),0)

            mc.parentConstraint(baseParJnt, parGrpTop, mo = True)
            # mc.scaleConstraint(baseParJnt, parGrpTop, mo = True)


def addToeBendRollLegFnt(   attrName = 'toeBend',
                            obj = [], 
                            target = [], 
                            attr = '',
                            amp = -30):
    if not obj:
        obj = mc.ls(sl = True)
    for i in range(len(obj)):
        objCtrl = obj[i]
        objCtrlShape = '{}Shape'.format(obj[i])

        ## splitName
        nameAll = splitName(sels = [objCtrl])
        name = nameAll[0]
        side = nameAll[1]
        lastName = nameAll[-1]

        ## Create Attribute
        onOffAttr = addAttrCtrl (ctrl = objCtrl, elem = ['{}RollOnOff'.format(attrName)], min = 0, max = 1, at = 'long')
        mc.setAttr('{}.{}RollOnOff'.format(objCtrl, attrName),1)
        onOffAttrShape = addAttrCtrl (ctrl = '{}'.format(objCtrlShape ), elem = ['{}RollAmp'.format(attrName)], min = -100, max = 100, at = 'float')
        mc.setAttr('{}.{}RollAmp'.format(objCtrlShape,attrName), amp)

        ## createNode Cmp
        toeBendCmp = mc.createNode('clamp', n = '{}Roll{}Cmp'.format(attrName, side))
        mc.setAttr("{}.maxR".format(toeBendCmp), 3)
        mc.connectAttr('{}.ty'.format(objCtrl), '{}.inputR'.format(toeBendCmp))

        ## createNode Mdv
        toeBendMdv = mc.createNode('multiplyDivide', n = '{}Roll{}Mdv'.format(attrName, side))
        mc.connectAttr('{}.outputR'.format(toeBendCmp), '{}.input1X'.format(toeBendMdv))
        mc.connectAttr('{}.{}RollAmp'.format(objCtrlShape, attrName), '{}.input2X'.format(toeBendMdv))

        ## createNode BlendColor
        toeBendBlc = mc.createNode('blendColors', n = '{}Roll{}Bcl'.format(attrName, side))
        mc.connectAttr('{}.{}RollOnOff'.format(objCtrl, attrName), '{}.blender'.format(toeBendBlc))
        mc.connectAttr('{}.outputX'.format(toeBendMdv), '{}.color1R'.format(toeBendBlc))

        ## createNode plusMinusAverage
        nodePmaObj = mc.createNode('plusMinusAverage', n = '{}Roll{}Pma'.format(attrName, side))

        mc.connectAttr('{}.{}'.format(objCtrl, attrName), '{}.input2D[0].input2Dx'.format(nodePmaObj))
        mc.connectAttr('{}.outputR'.format(toeBendBlc), '{}.input2D[1].input2Dx'.format(nodePmaObj))

        ## Connect Force ToeBendIkPiv
        ## Check Pma Node
        nodePma, indexPma = checkPma(obj = target[i], attrTarget = attr)
        if nodePma:
            mc.connectAttr ('{}.output2Dx'.format(nodePmaObj), '{}.input1D[{}]'.format(nodePma, indexPma), f = True)
        else:
            mc.connectAttr ('{}.output2Dx'.format(nodePmaObj), '{}.rotateX'.format(target[i]), f = True)


def checkPma(obj = 'HeelIkPivFnt_L_Grp', attrTarget = 'rotateX'):
    objAttr = mc.listConnections('{}.{}'.format(obj, attrTarget))[0]
    if 'unitConversion' in objAttr:
        lists = mc.listConnections(objAttr)
        for nodePmaEach in lists:
            if '_Pma' in nodePmaEach:
                nodePma = nodePmaEach
                for i in range(0, 10):
                    checkConnecAttr = checkConnect(object = nodePma, attr = 'input1D[{}]'.format(i))
                    if not checkConnecAttr:
                        indexPma = i
                        break
            else:
                nodePma = None
                indexPma = None
    return nodePma, indexPma

def toeBendFk(  ctrlGrp = '',
                objFk = ['ToeBendIkPivFnt_L_Grp', 'ToeBendIkPivFnt_R_Grp'],
                parent = '', 
                shape = 'square',
                side = '',
                size = 1,
                col = ''):
    for each in objFk:
        ## splitName
        nameAll = splitName(sels = [each])
        name = nameAll[0]
        side = nameAll[1].replace('_','')
        lastName = nameAll[-1]

        locJnt = mc.createNode ('joint', n = '{}{}Jnt'.format(name, side))
        snapObj(objA = each, objB = locJnt)
        # pm.makeIdentity ( locJnt , apply = True )
        # mc.makeIdentity (locJnt , apply = True , jointOrient = False, normal = 1, rotate = False) 

        subRig.Run(              
                    name       = name ,
                    tmpJnt     = locJnt,
                    parent     = parent ,
                    ctrlGrp    = ctrlGrp ,
                    shape      = shape ,
                    side       = side ,
                    size       = size )
        # controlName = '{}Sub_{}_Ctrl'.format(name, side)
        # assignColor(obj = [controlName], col = col)

        lockAttrObj(listObj = ['{}SubCtrlOfst_{}_Grp'.format(name, side)], lock = False,keyable = True)
        lockAttr(listObj = ['{}Sub_{}_Ctrl'.format(name, side)], attrs = ['tx', 'ty', 'tz', 'sx', 'sy', 'sz','localWorld'],lock = True, keyable = False)
        lockAttr(listObj = ['{}SubGmbl_{}_Ctrl'.format(name, side)], attrs = ['tx', 'ty', 'tz', 'sx', 'sy', 'sz'],lock = True, keyable = False)
        objParent = mc.listRelatives(each, p = True)[0]
        parentScaleConstraintLoop(obj = objParent, lists = ['{}SubCtrlOfst_{}_Grp'.format(name, side)],par = True, sca = True, ori = False, poi = False)
        parentScaleConstraintLoop(obj = '{}SubGmbl_{}_Ctrl'.format(name, side), lists = [each],par = True, sca = True, ori = False, poi = False)

        mc.delete(locJnt)


def curveArgLength(curve = ''):
    curveInfo = mc.createNode('curveInfo')
    curveShape = '{}Shape'.format(curve)
    mc.connectAttr('{}.worldSpace[0]'.format(curveShape), '{}.inputCurve'.format(curveInfo))
    answerArcLength = '%.3f' %(mc.getAttr('{}.arcLength'.format(curveInfo)))
    # print '>> ArdLength Curve :' , answerArcLength
    mc.delete(curveInfo)

    return answerArcLength


## ------------------------------------------
#from utaTools.utapy import utaCore
# reload(utaCore)
# utaCore.fixMouthNose()
## Run at bsh
## ------------------------------------------
def fixMouthNose(*args):
    elem = ['MouthNoseLR', 'MouthNoseUp', 'MouthNoseDn']
    MouthBshCtrl = 'MouthBsh_Ctrl'
    FacialLoc = 'FacialAll_Loc'
    MdvLRNode = mc.createNode('multiplyDivide', n = '{}LR_Mdv'.format('mouthNose'))
    PmaLRNode = mc.createNode('plusMinusAverage', n = '{}LR_Pma'.format('MouthNose'))
    PmaUpNode = mc.createNode('plusMinusAverage', n = '{}Up_Pma'.format('MouthNose'))
    PmaDnNode = mc.createNode('plusMinusAverage', n = '{}Dn_Pma'.format('MouthNose'))

    MdvUpDnNode = mc.createNode('multiplyDivide', n = '{}UpDn_Mdv'.format('MouthNose'))

    ## Add Attribute and Connect
    if mc.objExists(MouthBshCtrl):
        for each in elem:
            check = listAttribute(obj = ['{}Shape.{}'.format(MouthBshCtrl, each)])
            if not check:
                addAttrCtrl (ctrl = '{}Shape'.format(MouthBshCtrl), elem = elem, min = -200, max = 200, at = 'float')
        Amp = [0.5, 0.25, 0.15]
        for i in range(len(Amp)):
            mc.setAttr('{}Shape.{}'.format(MouthBshCtrl, elem[i]), Amp[i])
            if i == 0:
                mc.connectAttr('{}Shape.{}'.format(MouthBshCtrl, elem[i]), '{}.input2X'.format(MdvLRNode))
                mc.connectAttr('{}Shape.{}'.format(MouthBshCtrl, elem[i]), '{}.input2Y'.format(MdvLRNode))
                mc.connectAttr('{}.outputX'.format(MdvLRNode),'{}.input2D[1].input2Dx'.format(PmaLRNode))
                mc.connectAttr('{}.outputY'.format(MdvLRNode),'{}.input2D[1].input2Dy'.format(PmaLRNode))
                mc.connectAttr('{}.outputX'.format('NoseLR_Mdv'), '{}.input2D[0].input2Dx'.format(PmaLRNode))
                mc.connectAttr('{}.outputY'.format('NoseLR_Mdv'), '{}.input2D[0].input2Dy'.format(PmaLRNode))
                mc.connectAttr('{}.outputX'.format('MouthLR_Mdv'), '{}.input1X'.format(MdvLRNode))
                mc.connectAttr('{}.outputY'.format('MouthLR_Mdv'), '{}.input1Y'.format(MdvLRNode))
                
                if mc.objExists(FacialLoc):
                    mc.connectAttr('{}.output2Dx'.format(PmaLRNode), '{}Shape.NoseLeft'.format(FacialLoc), f = True)
                    mc.connectAttr('{}.output2Dy'.format(PmaLRNode), '{}Shape.NoseRight'.format(FacialLoc), f = True)

            elif i == 1:
                mc.connectAttr('{}Shape.{}'.format(MouthBshCtrl, elem[i]), '{}.input2X'.format(MdvUpDnNode))
                mc.connectAttr('{}.outputX'.format('MouthUD_Mdv'), '{}.input1X'.format(MdvUpDnNode))
                mc.connectAttr('{}.outputX'.format(MdvUpDnNode), '{}.input2D[0].input2Dx'.format(PmaUpNode))       
                mc.connectAttr('{}.outputX'.format('NoseUD_Mdv'), '{}.input2D[1].input2Dx'.format(PmaUpNode))

                if mc.objExists(FacialLoc):
                    mc.connectAttr('{}.output2Dx'.format(PmaUpNode), '{}Shape.NoseUp'.format(FacialLoc), f = True)

            else:
                mc.connectAttr('{}Shape.{}'.format(MouthBshCtrl, elem[i]), '{}.input2Y'.format(MdvUpDnNode))
                mc.connectAttr('{}.outputY'.format('MouthUD_Mdv'), '{}.input1Y'.format(MdvUpDnNode))
                mc.connectAttr('{}.outputY'.format(MdvUpDnNode), '{}.input2D[0].input2Dx'.format(PmaDnNode))       
                mc.connectAttr('{}.outputY'.format('NoseUD_Mdv'), '{}.input2D[1].input2Dx'.format(PmaDnNode))

                if mc.objExists(FacialLoc):
                    mc.connectAttr('{}.output2Dx'.format(PmaDnNode), '{}Shape.NoseDn'.format(FacialLoc), f = True)



####---------------------------------------
# from utaTools.utapy import utaCore
# reload(utaCore)
# utaCore.fixMouthCtrl(sels = ['MouthCnrBsh_L_Ctrl','MouthCnrBsh_R_Ctrl'],
#                     term = ['Up', 'Lo'],
#                     selsCen = ['MouthUprBsh_Ctrl', 'MouthLwrBsh_Ctrl'])
####---------------------------------------
def fixMouthCtrl(sels = [],term = [], selsCen = [],*args):
    bshNode = 'FacialAll_Loc'
    UprLwr = ['Upr', 'Lwr']
    sides = ['_L_', '_R_']
    valueUprLwr = [-1, 1]
    LftRgt = ['Lft', 'Rgt']

    for each in sels:
        for i in range(len(term)):
            name, side, lastName = splitName(sels = [each])
            if not side == '_':
                sideCon = '_' + side.split('_')[1]
                lockAttr(listObj = [each], attrs = ['rz'],lock = False, keyable = True)
                ClmNode = '{}{}{}Clm'.format (name, UprLwr[i], side)
                if not mc.objExists(ClmNode):
                    ClmNode = mc.createNode('clamp',n = ClmNode)
                MdvNode = '{}{}{}Mdv'.format(name, UprLwr[i], side)        
                if not  mc.objExists(MdvNode):
                    MdvNode = mc.createNode('multiplyDivide', n = MdvNode)
                MdvMirNode = '{}{}Mir{}Mdv'.format(name, UprLwr[i], side)
                if not  mc.objExists(MdvMirNode):
                    MdvMirNode = mc.createNode('multiplyDivide', n = MdvMirNode)
                MdvRevNode = '{}{}Rev{}Mdv'.format(name, UprLwr[i], side)
                if not  mc.objExists(MdvRevNode):
                    MdvRevNode = mc.createNode('multiplyDivide', n = MdvRevNode)
                PmaUprUpNode = '{}{}Up{}Pma'.format(name,UprLwr[i], side)
                if not  mc.objExists(PmaUprUpNode):
                    PmaUprUpNode = mc.createNode('plusMinusAverage', n = PmaUprUpNode)
                else:
                    ListConnectionAndDis(obj = PmaUprUpNode, dis = True, con = False)
                PmaUprDnNode = '{}{}Dn{}Pma'.format(name,UprLwr[i], side)                    
                if not  mc.objExists(PmaUprDnNode):
                    PmaUprDnNode = mc.createNode('plusMinusAverage', n = '{}{}Dn{}Pma'.format(name,UprLwr[i], side))
                else:
                    ListConnectionAndDis(obj = PmaUprDnNode, dis = True, con = False)

                ## SetAttr
                mc.setAttr('{}.minR'.format(ClmNode),-90)
                mc.setAttr('{}.maxR'.format(ClmNode), 0)
                mc.setAttr('{}.minG'.format(ClmNode), 0)
                mc.setAttr('{}.maxG'.format(ClmNode), 90)

                mc.setAttr('{}.input2X'.format(MdvNode), 0.1)
                mc.setAttr('{}.input2Y'.format(MdvNode), 0.1)

                mc.setAttr('{}.input2X'.format(MdvMirNode), -1)
                mc.setAttr('{}.input2X'.format(MdvRevNode), -1)


                # ## ConnectAttr
                mc.connectAttr('{}.rz'.format(each), '{}.input1X'.format(MdvNode), f = True)
                mc.connectAttr('{}.rz'.format(each), '{}.input1Y'.format(MdvNode), f = True)

                mc.connectAttr('{}.outputX'.format(MdvNode), '{}.inputR'.format(ClmNode), f = True)
                mc.connectAttr('{}.outputY'.format(MdvNode), '{}.inputG'.format(ClmNode), f = True)

                ## check Input1D
                PmaUprUpNum = findInputPma(obj = PmaUprUpNode)
                PmaUprDnNum = findInputPma(obj = PmaUprDnNode)
                mc.connectAttr('{}.outputR'.format(ClmNode), '{}.input1D[{}]'.format(PmaUprUpNode, PmaUprUpNum), f = True)
                mc.connectAttr('{}.outputG'.format(ClmNode), '{}.input1D[{}]'.format(PmaUprDnNode, PmaUprDnNum), f = True)

                if side == '_L_':
                    LftRgtObj = LftRgt[0]
                elif side == '_R_':
                    LftRgtObj = LftRgt[1]

                ## check Input1D
                PmaUprUpNum = findInputPma(obj = PmaUprUpNode)
                PmaUprDnNum = findInputPma(obj = PmaUprDnNode)
                mc.connectAttr('{}.outputX'.format('Lips{}{}AttrUD_Mdv'.format(LftRgtObj, UprLwr[i])), '{}.input1X'.format(MdvRevNode), f = True)
                mc.connectAttr('{}.outputX'.format(MdvRevNode), '{}.input1D[{}]'.format(PmaUprUpNode, PmaUprUpNum), f = True)
                mc.connectAttr('{}.outputY'.format('Lips{}{}AttrUD_Mdv'.format(LftRgtObj, UprLwr[i])), '{}.input1D[{}]'.format(PmaUprDnNode, PmaUprDnNum), f = True)

                mc.connectAttr('{}.output1D'.format(PmaUprUpNode), '{}.input1X'.format(MdvMirNode), f = True)

                if mc.objExists(bshNode):
                    mc.connectAttr('{}.outputX'.format(MdvMirNode), '{}Shape.{}LipsUp{}'.format(bshNode, term[i], sideCon), f = True)
                    mc.connectAttr('{}.output1D'.format(PmaUprDnNode), '{}Shape.{}LipsDn{}'.format(bshNode, term[i], sideCon), f = True)

    for sel in selsCen:     
        for i in range(0,2):  
            name, side, lastName = splitName(sels = [sel]) 

            ## Upr Lwr
            lockAttr(listObj = [sel], attrs = ['rz'],lock = False, keyable = True)
            ClmNode = '{}{}{}Clm'.format (name, UprLwr[i], side)            
            if not  mc.objExists(ClmNode):
                ClmNode = mc.createNode('clamp', n = ClmNode)
            MdvNode = '{}{}{}Mdv'.format(name, UprLwr[i], side)                
            if not  mc.objExists(MdvNode):
                MdvNode = mc.createNode('multiplyDivide', n = MdvNode)
            MdvMirRNode = '{}{}MirR{}Mdv'.format(name, UprLwr[i], side)
            if not  mc.objExists(MdvMirRNode):
                MdvMirRNode = mc.createNode('multiplyDivide', n = MdvMirRNode)
            MdvMirGNode = '{}{}MirG{}Mdv'.format(name, UprLwr[i], side)
            if not  mc.objExists(MdvMirGNode):
                MdvMirGNode = mc.createNode('multiplyDivide', n = MdvMirGNode)

            ## SetAttr
            mc.setAttr('{}.minR'.format(ClmNode),-90)
            mc.setAttr('{}.maxR'.format(ClmNode), 0)
            mc.setAttr('{}.minG'.format(ClmNode), 0)
            mc.setAttr('{}.maxG'.format(ClmNode), 90)

            mc.setAttr('{}.input2X'.format(MdvMirRNode), valueUprLwr[i])
            mc.setAttr('{}.input2X'.format(MdvMirGNode), valueUprLwr[i])
            mc.setAttr('{}.input2X'.format(MdvNode), 0.1)
            mc.setAttr('{}.input2Y'.format(MdvNode), 0.1)
            ## ConnectAttr
            mc.connectAttr('{}.rz'.format(sel), '{}.input1X'.format(MdvNode), f = True)
            mc.connectAttr('{}.rz'.format(sel), '{}.input1Y'.format(MdvNode), f = True)

            if 'Upr' in sel:
                mc.connectAttr('{}.outputX'.format(MdvNode), '{}.inputR'.format(ClmNode), f = True)
                mc.connectAttr('{}.outputY'.format(MdvNode), '{}.inputG'.format(ClmNode), f = True)
                mc.connectAttr('{}.outputR'.format(ClmNode), '{}.input1X'.format(MdvMirRNode), f = True)
                mc.connectAttr('{}.outputG'.format(ClmNode), '{}.input1X'.format(MdvMirGNode), f = True)

                ## check Input1D
                MouthCnrDnBshNum = findInputPma(obj = 'MouthCnrBsh{}Dn{}Pma'.format(UprLwr[0], sides[i]))
                MouthCnrUpBshNum = findInputPma(obj = 'MouthCnrBsh{}Up{}Pma'.format(UprLwr[0], sides[i]))
                mc.connectAttr('{}.outputX'.format(MdvMirGNode), '{}.input1D[{}]'.format('MouthCnrBsh{}Dn{}Pma'.format(UprLwr[0], sides[i]), MouthCnrDnBshNum), f = True)
                mc.connectAttr('{}.outputX'.format(MdvMirRNode), '{}.input1D[{}]'.format('MouthCnrBsh{}Up{}Pma'.format(UprLwr[0], sides[i]), MouthCnrUpBshNum), f = True)

            if 'Lwr' in sel:
                mc.setAttr('{}.input2X'.format(MdvMirRNode), valueUprLwr[i])
                mc.setAttr('{}.input2X'.format(MdvMirGNode), valueUprLwr[i])

                mc.connectAttr('{}.outputX'.format(MdvNode), '{}.inputR'.format(ClmNode), f = True)
                mc.connectAttr('{}.outputY'.format(MdvNode), '{}.inputG'.format(ClmNode), f = True)
                mc.connectAttr('{}.outputR'.format(ClmNode), '{}.input1X'.format(MdvMirRNode), f = True)
                mc.connectAttr('{}.outputG'.format(ClmNode), '{}.input1X'.format(MdvMirGNode), f = True)

                MouthCnrDnBshNum = findInputPma(obj = 'MouthCnrBsh{}Dn{}Pma'.format(UprLwr[1], sides[i]))
                MouthCnrUpBshNum = findInputPma(obj = 'MouthCnrBsh{}Up{}Pma'.format(UprLwr[1], sides[i]))
                mc.connectAttr('{}.outputX'.format(MdvMirGNode), '{}.input1D[{}]'.format('MouthCnrBsh{}Dn{}Pma'.format(UprLwr[1], sides[i]), MouthCnrDnBshNum), f = True)
                mc.connectAttr('{}.outputX'.format(MdvMirRNode), '{}.input1D[{}]'.format('MouthCnrBsh{}Up{}Pma'.format(UprLwr[1], sides[i]), MouthCnrUpBshNum), f = True)

    
def findInputPma(obj = '',*args):
    listAll = mc.listConnections(obj,d = False, c = True )
    ## fine input2D
    objCheck = []
    if not listAll == None:
        for each in listAll:
            if 'input' in each:
                splitA = each.split('.')[-1]
                splitB = splitA.split('[')[-1]
                objCheck.append(splitB.split(']')[0])
                numberPma = int(objCheck[-1])+1 
    else:
        numberPma = 0

    return numberPma


def ListConnectionAndDis(obj = '', dis = True, con = False, *args):
    conAll= []
    if obj:
        listAll = mc.listConnections(obj, d = True, c = True )
        if listAll:
            for each in listAll:
                if 'input' in each:
                    obj = mc.listConnections(each,p = True)[0]
                    conAll.append(obj + ' : ' + each)
                    if dis:
                        mc.disconnectAttr(obj, each)
        for each in conAll:    
            objSp = each.split(':')   
            objA = objSp[0]
            objB = objSp[-1]    
            if con: 
                mc.connectAttr(objA, objB, f = True) 
    else:
        print 'Please input object in "obj = ...."'

    return conAll


def weightNurb3Jnt_Tan(nurbs="", weightStyle=4, nurbSub=10, skinName="", jntRoot="", jntMid="", jntEnd="", *args):
    rampTxtr = mc.shadingNode('ramp', asShader=True)
    mc.setAttr("ramp1.colorEntryList[0].position", 0)
    mc.setAttr("ramp1.colorEntryList[0].color", 0, 0, 0)
    mc.setAttr("ramp1.colorEntryList[1].position", 0.5)
    mc.setAttr("ramp1.colorEntryList[1].color", 1, 1, 1)
    mc.setAttr("ramp1.colorEntryList[2].position", 1)
    mc.setAttr("ramp1.colorEntryList[2].color", 0, 0, 0)
    mc.setAttr("ramp1.interpolation", weightStyle)
    
    sdu = mc.getAttr("{}.degreeU".format(nurbs))
    sdv = mc.getAttr("{}.degreeU".format(nurbs)) + 1
    
    spu = nurbSub + sdu
    num = 1.0 / spu
    strt = 0.0
    if spu % 2 == 0:
        hspu = (spu / 2)
        loop = hspu + 1
    else:
        hspu = (spu + 1) / 2
        loop = hspu
    
    get_weight = []
    for x in range(loop):
        mid_wgh = mc.colorAtPoint(rampTxtr, u=0.5, v=strt)
        noMid_wgh = 1.0 - mid_wgh[0]
        strt += num
        
        get_weight.append(noMid_wgh)
    mc.delete(rampTxtr)

    mc.skinPercent(skinName, "{}.cv[*]".format(nurbs), tv=[jntMid, 1])
    
    for u in range(loop):
        for v in range(sdv):
            mc.skinPercent(skinName, "{}.cv[{}][{}]".format(nurbs, u, v), tv=[jntRoot, get_weight[u]])

    count = 0
    for u in range(hspu, spu)[::-1]:
        for v in range(sdv):
            mc.skinPercent(skinName, "{}.cv[{}][{}]".format(nurbs, u, v), tv=[jntEnd, get_weight[count]])
        count += 1

def createWrinkledRig(elem = '', axis="y", *args):

    sel = mc.ls(sl = True)
    geoShape = mc.listRelatives(sel[0], ap=True)
    geo = mc.listRelatives(geoShape, ap=True)[0]
    name, last_name = geo.split("_", 1)
    if "_" in last_name:
        if "L_" in last_name:
            side = "_L_"
        elif "R_" in last_name:
            side = "_R_"
    else:
        side = "_"

    span_crv = 6
    jnt_list = []
    gmblCtrl = []
    ctrl_ofst = []
    ctrlNameCrv = []
    ctrl_zro = []
    ctrl_par = []
    sub = ['Low', 'Mid', 'Hight']
    jntCv = [0, 3, 6]

    ## Get Cv curve
    crv_name = mc.polyToCurve(n = '{}WrinkledRig{}Crv'.format(name, side),form =2, degree=1)
    crv_weight = mc.duplicate(crv_name[0], n=crv_name[0].replace("WrinkledRig", "WeightWrinkledRig"))[0]
    nurb_sub = mc.getAttr(crv_weight + ".spans")

    ## Create Group
    if not mc.objExists("WrinkledRigCtrl_Grp"):
        ctrlGrp = mc.group(em=True, n="WrinkledRigCtrl_Grp")
    else: ctrlGrp = "WrinkledRigCtrl_Grp"

    if not mc.objExists("WrinkledRigJnt_Grp"):
        jntGrp = mc.group(em=True, n="WrinkledRigJnt_Grp")
    else: jntGrp = "WrinkledRigJnt_Grp"

    if not mc.objExists("WrinkledRigStill_Grp"):
        stillGrp = mc.group(em=True, n="WrinkledRigStill_Grp")
    else: stillGrp = "WrinkledRigStill_Grp"

    if not mc.objExists("WrinkledRigGeo_Grp"):
        geoGrp = mc.group(em=True, n="WrinkledRigGeo_Grp")
    else: geoGrp = "WrinkledRigGeo_Grp"

    nameCtrlGrp = mc.group(em=True, n='{}{}WrinkledRigCtrl{}{}'.format(elem, name, side, 'Grp'))
    nameJntGrp = mc.group(em=True, n='{}{}WrinkledRigJnt{}{}'.format(elem, name, side, 'Grp'))
    nameStillGrp = mc.group(em=True, n='{}{}WrinkledRigStill{}{}'.format(elem, name, side, 'Grp'))
    nameGeoGrp = mc.group(em=True, n='{}{}WrinkledRigGeo{}{}'.format(elem, name, side, 'Grp'))

    mc.parent(nameCtrlGrp, ctrlGrp)
    mc.parent(nameJntGrp, jntGrp)
    mc.parent(nameStillGrp, stillGrp)
    mc.parent(nameGeoGrp, geoGrp)

    ## Create curve
    for each in crv_name:
        if 'Crv' in each:
            clean ( target = each)
            crv_name = each
            mc.rebuildCurve(crv_name, ch = 0, rpo =1 ,end = 1, rt=0, keepRange =0, keepEndPoints=True,kt=0 ,spans = span_crv, degree=3, tol=0.01) 
    ## Create Joint from cv
    for i in range(len(jntCv)):
        mc.select('{}.cv[{}]'.format(crv_name, jntCv[i]))
        jnt_list.append(createJointFromLocator (name = '{}{}{}WrinkledRig'.format(elem, name, sub[i]), side = side, lastName = 'Jnt'))
        mc.parent(jnt_list[i], nameJntGrp)
        mc.setAttr(jnt_list[i] + ".radius", 0.005)

    ## Create Group and parent
    for i in range(len(jnt_list)):
            mc.select(jnt_list[i])
            zroGrp = rigTools.UtaDoZeroGroup()

            mc.select(jnt_list[i])
            grpCtrlName, grpCtrlOfsName, grpCtrlParsName, ctrlName, GmblCtrlName = createControlCurve(curveCtrl = 'ball', parentCon = True, connections = False, geoVis = False)
            gmblCtrl.append(GmblCtrlName)
            ctrl_ofst.append(grpCtrlOfsName)
            ctrlNameCrv.append(ctrlName)
            ctrl_zro.append(grpCtrlName)
            ctrl_par.append(grpCtrlParsName)
            mc.parent(grpCtrlName, nameCtrlGrp)
            mc.select(cl =True)

            mc.scale(0.005, 0.005, 0.005, ctrlName + ".cp[:]")
            mc.scale(0.005, 0.005, 0.005, GmblCtrlName + ".cp[:]")

    allJnt = mc.createNode("joint", n='{}{}AllMoverWrinkledRig{}{}'.format(elem, name, side, 'Jnt'))
    mc.delete(mc.parentConstraint(jnt_list[1], allJnt, mo=False))

    allCtrlName, allCtrlOfsName, allCtrlParsName, allCtrl, allGmblCtrl = createControlCurve(curveCtrl = 'cube', parentCon = False, connections = False, geoVis = False)
    mc.scale(0.01, 0.01, 0.01, allCtrl + ".cp[:]")
    mc.scale(0.01, 0.01, 0.01, allGmblCtrl + ".cp[:]")
    mc.delete(allJnt)
    mc.parent(allCtrlName, nameCtrlGrp)

    ## Create ParentConstraint and set color
    assignColor(obj = [ctrlNameCrv[0]], col = 'yellow')
    assignColor(obj = [ctrlNameCrv[1]], col = 'red')
    assignColor(obj = [ctrlNameCrv[-1]], col = 'blue')

    # parentScaleConstraintLoop(obj = gmblCtrl[0], lists = [ctrl_ofst[1]],par = False, sca = False, ori = False, poi = True)
    # parentScaleConstraintLoop(obj = gmblCtrl[-1], lists = [ctrl_ofst[1]],par = False, sca = False, ori = False, poi = True)
    lowWs = mc.xform(ctrlNameCrv[0], q=1, ws=1, t=1)
    hightWs = mc.xform(ctrlNameCrv[2], q=1, ws=1, t=1)

    get_max = []
    for L, H in zip(lowWs, hightWs):
        get_max.append(H - L)
        
    if max(get_max) == get_max[0]:
        aim = (1, 0, 0)
        up = (0, 1, 0)
        wu = (0, 1, 0)
    elif max(get_max) == get_max[1]:
        aim = (0, 1, 0)
        up = (0, 0, 1)
        wu = (0, 0, 1)
    elif max(get_max) == get_max[2]:
        aim = (0, 0, 1)
        up = (1, 0, 0)
        wu = (1, 0, 0)

    parCon = mc.parentConstraint(gmblCtrl[0], gmblCtrl[-1], ctrl_par[1], sr=("x", "y", "z"), mo=True)[0]
    mc.setAttr ('{}_parentConstraint1.interpType'.format(ctrl_par[1]), 2)
    aimCon = mc.aimConstraint(gmblCtrl[-1], ctrl_par[1], aim=aim, u=up, wut="vector", wu=wu, mo=True)[0]

    lowCon = mc.parentConstraint(allGmblCtrl, ctrl_ofst[0], mo=True)[0]
    midCon = mc.parentConstraint(allGmblCtrl, ctrl_ofst[1], mo=True)[0]
    higthCon = mc.parentConstraint(allGmblCtrl, ctrl_ofst[2], mo=True)[0]

    ## tan loft aand bindSkin
    crvA_loft = mc.duplicate(crv_weight)[0]
    crvB_loft = mc.duplicate(crv_weight)[0]
    if axis == "x":
        mc.move(-0.15, 0, 0, crvA_loft)
        mc.move(0.15, 0, 0, crvB_loft)
    elif axis == "x":
        mc.move(0, -0.15, 0, crvA_loft)
        mc.move(0, 0.15, 0, crvB_loft)
    elif axis == "z":
        mc.move(0, 0, -0.15, crvA_loft)
        mc.move(0, 0, 0.15, crvB_loft)

    loft_name = mc.loft(crvA_loft, crvB_loft, d=1, rsn=True, ch=False)[0]
    loft_name = mc.rename(loft_name, crv_name.replace("_Crv", "_Nrb"))
    mc.delete(crv_name, crv_weight, crvA_loft, crvB_loft)

    skin_name = mc.skinCluster(jnt_list, loft_name, tsb=True)[0]
    weightNurb3Jnt_Tan(nurbs=loft_name, nurbSub=nurb_sub, skinName=skin_name, weightStyle=4, jntRoot=jnt_list[0], jntMid=jnt_list[1], jntEnd=jnt_list[2])

    mc.skinCluster(jnt_list, geo, tsb=True)[0]
    mc.select(loft_name + ".cv[:]", geo + ".vtx[:]")
    mc.copySkinWeights(nm=True, sa="closestPoint", ia="closestJoint")
    #weightTools.copySelectedWeight()


    ## tan create Follicle
    get_nrb = []
    get_fol = []
    get_folCon = []
    for jnt, zro, ofst, par in zip(jnt_list, ctrl_zro, ctrl_ofst, ctrl_par):
        nrb = mc.nurbsPlane(n=jnt.replace("_Jnt", "_Nrb"), w=0.005, ax=(0,0,1), ch=False)[0]
        mc.delete(mc.parentConstraint(jnt, nrb, mo=False))
        get_nrb.append(nrb)    #----> create nurbsSurface

        folShape = mc.createNode("follicle", n=jnt.replace("_Jnt", "_FolShape"))
        mc.setAttr(folShape + ".pu", 0.5)
        mc.setAttr(folShape + ".pv", 0.5)
        fol = mc.listRelatives(folShape, ap=True)[0]

        mc.connectAttr("{}.local".format(nrb), "{}.inputSurface".format(folShape))
        mc.connectAttr("{}.worldMatrix[0]".format(nrb), "{}.inputWorldMatrix".format(folShape))
        mc.connectAttr("{}.outRotate".format(folShape), "{}.rotate".format(fol))
        mc.connectAttr("{}.outTranslate".format(folShape), "{}.translate".format(fol))
        get_fol.append(fol)    #----> create follicle

        folCon = mc.parentConstraint(fol, zro, mo=True)[0]
        get_folCon.append(folCon)
    allCon = mc.parentConstraint(get_fol[1], allCtrlName, mo=True)[0]

    ## create visibility
    mc.addAttr(ctrlNameCrv[1], ln="wrinkledVis", dv=1, min=0, max=1, k=True, at="long")
    mc.addAttr(ctrlNameCrv[1], ln="midFollow", dv=1, min=0, max=1, k=True, at="long")
    mc.addAttr(ctrlNameCrv[1], ln="allmoverFollow", dv=1, min=0, max=1, k=True, at="long")
    mc.setAttr(ctrlNameCrv[1] + ".allmoverFollow", 0)

    mc.connectAttr(ctrlNameCrv[1] + ".wrinkledVis", ctrl_zro[0] + ".v")
    mc.connectAttr(ctrlNameCrv[1] + ".wrinkledVis", ctrl_zro[2] + ".v")

    mc.connectAttr(ctrlNameCrv[1] + ".midFollow", "{}.{}W0".format(parCon, gmblCtrl[0]))
    mc.connectAttr(ctrlNameCrv[1] + ".midFollow", "{}.{}W1".format(parCon, gmblCtrl[-1]))
    mc.connectAttr(ctrlNameCrv[1] + ".midFollow", "{}.{}W0".format(aimCon, gmblCtrl[-1]))

    revCon = mc.createNode("reverse", n='{}{}WrinkledRig{}{}'.format(elem, name, side, 'Rev'))
    mc.connectAttr(ctrlNameCrv[1] + ".allmoverFollow", revCon + ".inputX")
    for folCnst, fol, allCnst  in zip(get_folCon, get_fol, [lowCon, midCon, higthCon]):
        mc.connectAttr(revCon + ".outputX", "{}.{}W0".format(folCnst, fol))
        mc.connectAttr(ctrlNameCrv[1] + ".allmoverFollow", "{}.{}W0".format(allCnst, allGmblCtrl))
    mc.connectAttr(ctrlNameCrv[1] + ".allmoverFollow", allCtrlName + ".v")


    ## change geo name
    geo = mc.rename(geo, "{}{}{}Geo".format(name, "WrinkledRig", side))
    geoShape = mc.listRelatives(geo, c=True, typ="mesh")[0]

    ## Parent Suface at GrpStill:
    mc.parent(get_nrb, nameStillGrp)
    mc.parent(get_fol, nameStillGrp)
    mc.parent(loft_name, nameStillGrp)
    mc.parent(geo, nameGeoGrp)

    mc.delete(loft_name)

def groupWrinkledRig(motherGeo='', geoGrp='', ctrlGrp='', stillGrp='', *args):
    mc.select(ado=True)
    sel = mc.ls(sl=True)
    for still in sel:
        if "WnkRig" in still:
            mrk_stillGrp = mc.listRelatives(still, c=True)

    mc.select(cl=True)
    for still_grp in mrk_stillGrp:
        nrb_shape = mc.listRelatives(still_grp, ad=True, typ="nurbsSurface")
        sel_nrb = mc.listRelatives(nrb_shape, ap=True)
        for still in sel_nrb:
            mc.select(still, add=True)
    mc.select(motherGeo, add=True)
    mc.CreateWrap()
    mc.parent(motherGeo + "Base", geoGrp)

    mc.select(cl=True)

def fixWrinkledRig(elem = "WrinkledRig", process = "fclRig_md"):

    targetMesh = mc.ls(sl=True)
    rawName = [i.split(":")[-1] for i in targetMesh]
    
    bsnNode = [i.replace("_Geo", "_Bsn") for i in rawName]
    
    no_match = []
    for mesh, name, bsn in zip(targetMesh, rawName, bsnNode):
        wnkRigGeo = process + ":" + name.replace("_", "{}_".format(elem), 1)
        if not mc.objExists(wnkRigGeo):
            no_match.append(wnkRigGeo)
        
        else:
            if not mc.objExists(bsn):
                bs = mc.blendShape(wnkRigGeo, mesh, foc=True, n=bsn)[0]
                mc.setAttr("{}.w[0]".format(bs), 1)
    
        # do connection
        isKey = mc.getAttr("{}.v".format(mesh), se=True)

        if isKey:
                
            parent = mc.listRelatives(wnkRigGeo, ap=True)[0]
            if "_L_" in parent:
                new = parent.replace("WrinkledRigGeo_L_Grp", "MidWrinkledRig_L_Ctrl")
            
            elif "_R_" in parent:
                new = parent.replace("WrinkledRigGeo_R_Grp", "MidWrinkledRig_R_Ctrl")
                
            else:
                new = parent.replace("WrinkledRigGeo_Grp", "MidWrinkledRig_Ctrl")

            mc.connectAttr("{}.wrinkledVis".format(new), "{}.v".format(mesh))
    
    if no_match:
        print "No match object : {}".format(no_match)

def checkSameObject(objA = '', objB = '',*args):
    if not objA:
        mc.select(cl = True)
        lists = mc.ls(sl = True)
        if not len(lists) == 2:
            print 'Please select 2 object'
            return False
    else:
        if objB:
            lists = [objA, objB]
        else:
            print 'Please input "objA and objB"'
            return False

    answer = []
    for each in lists:
        poseLocator = mc.xform(each, q=True, boundingBox=True)
        for i in range(len(poseLocator)):
            if i == 0:
                answerNum = poseLocator[i]

            else:
                answerNum = answerNum + poseLocator[i-1] 
        answer.append(answerNum)
    if answer:
        if answer[0] == answer[1]:
            return True
        else:
            return False




# ## Insert Loop Eage
# mm.eval('PolyConvertToRingAndSplit;')
# sel = mc.ls(sl = True)
# obj = sel[0].split('.e')[0]
# clean ( target = obj )

# ## Eage Edit Flow
# mm.eval('polyEditEdgeFlow -constructionHistory 1 -adjustEdgeFlow 1')
# sel = mc.ls(sl = True)
# obj = sel[0].split('.e')[0]
# clean ( target = obj )

## parent curve shape
    # select curve 1
    # select curve 2
    # select group new 
    # mel : parent -s-r






def lockAttrAmpCharacter( *artgs):

    ctrlAll = 'FacialCtrl_Grp'
    if mc.objExists(ctrlAll):
        allLists = listGeo(sels = ctrlAll, nameing = '_Ctrl', namePass = '_Grp')
    else:
        return

    allCtrl = []
    for each in allLists:
        lists = each.split('|')[-1]
        if '_Ctrl' in lists:
            if not 'Gmbl_Ctrl' in lists:
                allCtrl.append(lists)
                eachShape = '{}Shape'.format(lists)
                attrs = listAttribute(obj = [eachShape])
                for obj in attrs:
                    if '_Min' in obj or '_Max' in obj:
                        lockAttr(listObj = [eachShape], attrs = [obj],lock = True, keyable = True)
                    # elif '_Max' in obj:
                        # lockAttr(listObj = [eachShape], attrs = [obj],lock = True, keyable = True)



def checkOptionBoxWrite(labelA = '', labelB = '', *args):
    checkMessage = mc.confirmDialog( title='Confirm', 
                message='Please Select Option!!', 
                button=[labelA,labelB], 
                defaultButton=labelA, 
                cancelButton=labelB, 
                dismissString='No' )
    if checkMessage == labelA:
        return labelA
    if checkMessage == labelB:
        return labelB

def checkOptionBoxRead(labelA = '', labelB = '', labelC = '',*args):
    checkMessage = mc.confirmDialog( title='Confirm', 
                message='Please Select Option!!', 
                button=[labelA, labelB, labelC], 
                defaultButton=labelA, 
                cancelButton=labelB, 
                dismissString='No')
    if checkMessage == labelA:
        return labelA
    if checkMessage == labelB:
        return labelB
    if checkMessage == labelC:
        return labelC


def jointOnNrb(nrb = '', jnt = '', control = '', name = '', elem = '', side = '', *args):

    norGrp = mc.group(n= '{}{}Nor{}Grp'.format(name, elem, side), em = True)
    orinGrp = mc.group(n = '{}{}Pio{}Grp'.format(name, elem, side), em = True)

    mc.parent(orinGrp, norGrp)
    mc.delete(mc.parentConstraint(jnt, norGrp, mo = False))
    mc.normalConstraint (nrb, norGrp, w =  1, aimVector= ( 0, 0, 1),upVector=( 0, 1, 0),worldUpType ="vector",worldUpVector =(0, 1, 0,))
    mc.geometryConstraint (nrb, norGrp,weight= 1)
    mc.pointConstraint(control, norGrp, mo = True)
    mc.scaleConstraint(control, norGrp, mo = True)
    mc.orientConstraint(control, orinGrp, mo = False)

    mc.parentConstraint(orinGrp, jnt, mo = True)
    mc.scaleConstraint(orinGrp, jnt, mo = True)

    return norGrp

    # mc.parent(jnt, orinGrp)


def direcObj(driven = '',drivenAxis = '', driver = [], driverAxis = '' , shape = True, *args):
    if not driven:
        sels = mc.ls(sl = True)
        driven = sels[0]
    drivenName, drivenSide, drivenLastName = splitName(sels = [driven], optionSide = True)
    print drivenName, drivenSide, drivenLastName

    if shape:
        shape = 'Shape'
    else:
        shape = ''

    for each in driver:
        driverName, driverSide, driverLastName = splitName(sels = [each], optionSide = True)
        addAttrCtrl (ctrl = '{}{}'.format(driven, shape) , elem = ['{}_Amp'.format(driverName)], min = -200, max = 200, at = 'float')
        driverMdv = mc.createNode('multiplyDivide', n = '{}{}Mdv'.format(driverName, driverSide))

        conCheck = checkConnect(object = each, attr = driverAxis)
        # if not conCheck:
        #     print '..conCheck...'
        # else:
        #     print conCheck
        # mc.connectattr('{}.{}'.format(driven, drivenAxis), '{}.{}'.format(each, driverAxis))



def remove_all_unused_influences(grp):
    sel = selGeo(grp)
    skins = mc.ls(type='skinCluster')
    for skin in skins:
        if mc.referenceQuery(skin, inr=True):
            continue
        remove_unused_for_skin(skin)
    return True

def selGeo(grp = 'MdGeo_Grp'):
    GeoAll = []
    if mc.objExists(grp):
        obj = mc.listRelatives(grp, ad = True)
        for each in obj:
            if not '_Grp'in each:
                if not 'Orig'in each:          
                    if not 'Shape'in each:       
                        GeoAll.append(each)
    return GeoAll

def createCrvOnEdge(name = '', elem = '',side = '',degree = 3, *args):
    listAll = mc.ls(os=1, fl=1) 
    if listAll:
        sel = listAll[0]
    if not side:
        side = '_'
    elif 'L':
        side = '_L_'
    elif 'R':
        side = '_R_'

    pathCrv = mc.polyToCurve( form = 2, degree =degree, conformToSmoothMeshPreview = 1, n = '{}{}{}Crv'.format(name,elem.capitalize(),side))[0]
    return pathCrv

def setSelectPref(*args):
    mc.selectPref(tso=1)
    if mc.selectPref(tso=1, q = 1):
        print 'Set SelectPref Done'
    # mc.button('setPrefButt', edit = True, bgc = [0.4,1,0.6])


def creatCtrl(name = '', elem = '',side = '', degree = 3, reverseCv = True,*args):
    listAll = mc.ls(os=1, fl=1) 
    if listAll:
        sel = listAll[0]
    if not side:
        side = '_'
    elif 'L':
        side = '_L_'
    elif 'R':
        side = '_R_'

    pathCrv = mc.polyToCurve( form = 2, degree =degree, conformToSmoothMeshPreview = 1, n = '{}{}{}Crv'.format(name,elem.capitalize(),side))[0]

    if reverseCv:
        ## getPosi
        ## Reverse of not math value
        edgePosi = mc.xform(sel,q=True,t=True,ws = 1)
        
        edgePoint = [(edgePosi[0] + edgePosi[3])/2, (edgePosi[1]+edgePosi[4])/2,(edgePosi[2]+edgePosi[5])/2]    
        cvs = mc.getAttr(pathCrv + '.spans')+mc.getAttr(pathCrv + '.degree')
        startCv = mc.xform(pathCrv + '.cv[{}]'.format(0),q=True,t=True,ws = 1)
        endCv = mc.xform(pathCrv + '.cv[{}]'.format(cvs-1),q=True,t=True,ws = 1)
        
        disStart = math.sqrt(math.pow(edgePoint[0]-startCv[0],2) + math.pow(edgePoint[1]-startCv[1],2) + math.pow(edgePoint[2]-startCv[2],2))
        disEnd = math.sqrt(math.pow(edgePoint[0]-endCv[0],2) + math.pow(edgePoint[1]-endCv[1],2) + math.pow(edgePoint[2]-endCv[2],2))
        
        if disStart > disEnd:
            mc.reverseCurve(pathCrv, ch = 1)
        else:
            pass
        mc.delete(pathCrv,ch = True)

    return pathCrv 



# import maya.cmds as mc
# import math
def positionBoundingBox(curve = '', *args):
    # print curve, '...curve'
    ## splitName 
    nameSp, side, lastName = splitName(sels = [curve])  
    # curve = 'upperLipBoundSine_Crv'
    bb = mc.exactWorldBoundingBox(curve)

    ## group position
    positionGrp = []
    for i in range(1, 6):
        positionGrp.append(mc.group(n = '{}Posi{}{}Grp'.format(nameSp, i, side), em = True))
    disX = math.sqrt(math.pow((bb[0]-bb[3]),2))
    disY = math.sqrt(math.pow((bb[1]-bb[4]),2))
    disZ = math.sqrt(math.pow((bb[2]-bb[5]),2))
    if bb[0] >= bb[3]:
        centerX = bb[3] + (disX/2)
        mc.setAttr('{}.tx'.format(positionGrp[3]),bb[0])
        mc.setAttr('{}.tx'.format(positionGrp[4]),bb[3])
    else:
        centerX = bb[0] + (disX/2)
        mc.setAttr('{}.tx'.format(positionGrp[3]),bb[3])
        mc.setAttr('{}.tx'.format(positionGrp[4]),bb[0])
    # print centerX

    if bb[1] >= bb[4]:
        centerY = bb[4] + (disY/2)
    else:
        centerY = bb[1] + (disY/2)
    # print centerY

    if bb[2] >= bb[5]:
        centerZ = bb[5] + (disZ/2)
        mc.setAttr('{}.tz'.format(positionGrp[1]),bb[2])
        mc.setAttr('{}.tz'.format(positionGrp[2]),bb[5])
    else:
        centerZ = bb[2] + (disZ/2)
        mc.setAttr('{}.tz'.format(positionGrp[1]),bb[5])
        mc.setAttr('{}.tz'.format(positionGrp[2]),bb[2])
    # print centerZ

    mc.setAttr('{}.t'.format(positionGrp[0]),centerX,centerY,centerZ)
    for set in [positionGrp[1],positionGrp[2]]:
        mc.setAttr(set+'.tx',centerX)
        mc.setAttr(set+'.ty',centerY)
    for set in [positionGrp[3],positionGrp[4]]:
        mc.setAttr(set+'.ty',centerY)
        mc.setAttr(set+'.tz',centerZ)

    return positionGrp


def generateControlFollowAxis(grid = True, bbox = False, fk = True, horiOrVertical = True, horiLFTorRGT = True, world = True, *args):
    ## set pin pivot
    mc.manipPivot(pinPivot = True)

    lists = mc.ls(sl = True)
    if lists:
        fkGrp = []
        fkGmblGrp = []
        if fk:
            if horiOrVertical:
                for each in lists:
                    ## check Geo or Grp
                    if not '_Geo' in each:
                        geo = mc.listRelatives(each,c = True)
                        if geo:
                            each = geo[0]

                    nameSp, side, lastName = splitName(sels = [each])
                    mc.select(each)

                    ## create joint Start 
                    clust = mc.cluster()
                    trans = mc.createNode('transform', n = '{}Pos{}Grp'.format(nameSp, side))
                    mc.delete(mc.parentConstraint(clust[-1],trans,mo=0))
                    face = each+'.f[:]'
                    mc.delete(mc.normalConstraint(face,trans))
                    attrRY = mc.getAttr('{}.ry'.format(trans))
                    jntStart = jointFromLocator()

                    if grid:
                        mc.setAttr('{}.ty'.format(jntStart),0)

                    if bbox:
                        minY = mc.xform(each, q=1, bb=1)[1]
                        mc.setAttr('{}.ty'.format(jntStart), minY)

                    positionGrp = positionBoundingBox(curve = each)
                    if horiLFTorRGT:
                        posSide = positionGrp[3]
                    else:
                        posSide = positionGrp[4]

                    mc.delete(mc.parentConstraint(posSide, jntStart, mo = False))

                    mc.setAttr(jntStart + '.displayLocalAxis',1)

                    grp = mc.pickWalk(each, d = 'up')[0]


                    # # create Control curve
                    mc.select(jntStart, r = True)
                    grpCtrlName, grpCtrlOfsName, grpCtrlParsName, ctrlName, GmblCtrlName = createControlCurve(curveCtrl = 'cube', parentCon = False, connections = False, geoVis = True)

                    mc.parentConstraint(GmblCtrlName, grp, mo = True)
                    mc.scaleConstraint(GmblCtrlName, grp, mo = True)
                    mc.connectAttr('{}.GeoVis'.format(ctrlName), '{}.v'.format(grp))
                    mc.select(cl =True)

                    ##fkGrp 
                    fkGrp.append(grpCtrlName)
                    fkGmblGrp.append(GmblCtrlName)

                    ## clear
                    mc.select(clust[-1], jntStart, positionGrp)
                    mc.delete()

                # if fk:
                for i in range(len(fkGrp)):
                    if not i == 0:
                        mc.parent(fkGrp[i], fkGmblGrp[i-1])

            else :
                generateControlFollowAxisChain()
        else:
            for each in lists:
                ## check Geo or Grp
                if not '_Geo' in each:
                    geo = mc.listRelatives(each,c = True)
                    if geo:
                        each = geo[0]

                        nameSp, side, lastName = splitName(sels = [each])
                        mc.select(each)

                        ## create joint Start 
                        clust = mc.cluster()
                        trans = mc.createNode('transform', n = '{}Pos{}Grp'.format(nameSp, side))
                        mc.delete(mc.parentConstraint(clust[-1],trans,mo=0))
                        face = each+'.f[:]'
                        mc.delete(mc.normalConstraint(face,trans))
                        attrRY = mc.getAttr('{}.ry'.format(trans))
                        jntStart = jointFromLocator()

                        if grid:
                            mc.setAttr('{}.ty'.format(jntStart),0)

                        if bbox:
                            minY = mc.xform(each, q=1, bb=1)[1]
                            mc.setAttr('{}.ty'.format(jntStart), minY)

                        # mc.setAttr(jntStart + '.displayLocalAxis',1)

                        grp = mc.pickWalk(each, d = 'up')[0]

                        # # create Control curve
                        mc.select(jntStart, r = True)
                        grpCtrlName, grpCtrlOfsName, grpCtrlParsName, ctrlName, GmblCtrlName = createControlCurve(curveCtrl = 'cube', parentCon = False, connections = False, geoVis = True)

                        mc.parentConstraint(GmblCtrlName, grp, mo = True)
                        mc.scaleConstraint(GmblCtrlName, grp, mo = True)
                        mc.connectAttr('{}.GeoVis'.format(ctrlName), '{}.v'.format(grp))
                        mc.select(cl =True)

                        ##fkGrp 
                        fkGrp.append(grpCtrlName)

                        ## fix world Axis
                        jntGuide = mc.createNode('joint', n = 'jntGuide_Jnt')
                        mc.delete(mc.parentConstraint(ctrlName, jntGuide, mo = False))
                        mc.makeIdentity(jntGuide, a = True, jo = True)
                        mc.move(0, 5, 0, jntGuide, r = True )
                        mc.delete(mc.aimConstraint(jntGuide, ctrlName, mo = False, aim = (0, 1 , 0), u = (0, 1 , 0), wu = (0, 1, 0), wut = 'vector'))
                        mc.select(ctrlName, r = True)
                        if world:
                            fixAxisControl(world = True)
                        mc.setAttr('{}.r'.format(ctrlName), 0, 0, 0)

                        ## clear
                        mc.select(clust[-1], jntStart, jntGuide)
                        mc.delete()
                    else:
                        # print '>>', each
                        print "Selected don't have Geo or object"

                                
            print 'Generate is DONE'
    else:
        print '!! Please Select Object'

def generateControlFollowAxisChain(*args):
    geo_list = mc.ls(sl=True)
    jnt_start_list = []
    clust_list = []
    
    for num,each in enumerate(geo_list):
        ## check Geo or Grp
        if not '_Geo' in each:
            geo = mc.listRelatives(each,c = True)
            if geo:
                each = geo[0]

        nameSp, side , lastName = splitName(sels = [each])
        mc.select(each)

        clust = mc.cluster()
        trans = mc.createNode('transform', n = '{}Pos{}Grp'.format(nameSp, side))
        mc.delete(mc.parentConstraint(clust[-1],trans,mo=0))
        face = each+'.f[:]'
        #mc.delete(mc.normalConstraint(face,trans))
        mc.delete(mc.normalConstraint(face,trans,wut='objectRotation',wuo = each,aim=[0,-1,0],u=[0,0,1],wu=[0,0,1]))
        attrRY = mc.getAttr('{}.ry'.format(trans))
        attrRY = mc.getAttr('{}.ry'.format(trans))
        jntStart = jointFromLocator()

        if num ==0:
            mc.setAttr('{}.ty'.format(jntStart),0)

        else:
            bb = pm.PyNode(each).getBoundingBox()
            ty = bb[0][1]
            mc.setAttr('{}.ty'.format(jntStart),ty)

        jnt_start_list.append(jntStart)
        clust_list.append(clust[-1])
    zro_list = []
    gmb_ctrl_list = []
    grp_list = []
    ctrl_list = []
    
    for num,each in enumerate(geo_list):
        ## check Geo or Grp
        if not '_Geo' in each:
            geo = mc.listRelatives(each,c = True)
            if geo:
                each = geo[0]

        grp = mc.pickWalk(each,d='up')[0]

        mc.select(jnt_start_list[num],r=True)
        grpCtrlName, grpCtrlOfsName, grpCtrlParsName, ctrlName, GmblCtrlName = createControlCurve(curveCtrl = 'cube', parentCon = False, connections = False, geoVis = True)
        zro_list.append(grpCtrlName)
        gmb_ctrl_list.append(GmblCtrlName)
        grp_list.append(grp)
        ctrl_list.append(ctrlName)

    for num in range(len(gmb_ctrl_list)-1):
        mc.parent(zro_list[num+1],gmb_ctrl_list[num])

    for num,grp in enumerate(grp_list):
        mc.parentConstraint(gmb_ctrl_list[num], grp, mo = True)
        mc.scaleConstraint(gmb_ctrl_list[num], grp, mo = True)
        mc.connectAttr('{}.GeoVis'.format(ctrl_list[num]), '{}.v'.format(grp))

    mc.delete(jnt_start_list)
    mc.delete(clust_list)

    return zro_list[0]


def fixAxisControl(world = False, *args):
    lists = mc.ls(sl = True)
    jntPos = []
    if lists:
        for each in lists:
            loPos = lrr.locatorOnMidPos()
            mc.delete(mc.parentConstraint(each, loPos, mo = False))
            mc.select(loPos)
            jntGuide = jointFromLocator()
            mc.select(jntGuide)
            mc.BakeCustomPivot()
            nameSp, side, lastName = splitName(sels = [each])
            name = each.split('Pos')[0]
            grpTop = nameSp + 'CtrlZro_Grp'
            gmblGrp = name + 'PosGmbl_Ctrl'
            geo = nameSp.replace('Pos', '') + '_Geo'
            geoGrp = mc.listRelatives(geo,p=True)
            # lists = mc.listConnections(gmblGrp , d = True, s = True)
            listsPar = mc.listRelatives(gmblGrp , c = True)

            if len(listsPar) == 2:
                if mc.objExists(listsPar[-1]):
                    mc.parent(listsPar[-1], w = True)

            # freeze transform , delete history , center pivot
            pm.xform ( each , cp = True ) ;

            ## fix position
            mc.delete(mc.listRelatives(geoGrp, c = True, type = 'constraint'))
            mc.delete(mc.parentConstraint(jntGuide, grpTop, mo = False))

            if world:
                pm.makeIdentity(jntGuide, a=True, jo=True)
                pm.move(0, 5, 0, jntGuide, r=True)
                mc.delete(mc.aimConstraint(jntGuide, grpTop, aim=(0, 1, 0), u=(0, 1, 0), wut="vector", wu=(0, 1, 0), mo=False))

            mc.parentConstraint(gmblGrp, geoGrp, mo = True)
            mc.scaleConstraint(gmblGrp, geoGrp, mo = True)
            if len(listsPar) == 2:
                if mc.objExists(listsPar[-1]):
                    mc.parent(listsPar[-1], gmblGrp)

            jntPos.append(jntGuide)
    
        mc.delete(jntPos)

        
    else:
        print '!! Please Select Object and Fix Axis Control New'



# sel = mc.ls(sl=True)
# for i in sel:
#     mc.select(i)
#     clust = mc.cluster()
#     trans = mc.createNode('transform')
#     mc.delete(mc.parentConstraint(clust[-1],trans,mo=0))
#     face = i+'.f[:]'
#     mc.delete(mc.normalConstraint(face,trans))
#     mc.setAttr(trans+'.displayLocalAxis',1)
    
#     newJnt = utaCore.createJointFromLocator (name = 'test', side = 'xx', lastName = 'Jnt')

#     jntPos = mc.createNode('joint', n = 'pose_Jnt')
#     mc.delete(mc.parentConstraint(i, jntPos, mo = False))
#     mc.setAttr('{}.rx'.format(jntPos), 0)
#     mc.setAttr('{}.ry'.format(jntPos), 0)
#     mc.setAttr('{}.rz'.format(jntPos), 0)

def templateGrp (name = '', *args):
    geoGrp = mc.group(n = '{}Geo_Grp'.format(name), em = True)
    jntGrp = mc.group(n = '{}Jnt_Grp'.format(name), em = True)
    skinGrp = mc.group(n = '{}Skin_Grp'.format(name), em = True)
    ctrlGrp = mc.group(n = '{}Ctrl_Grp'.format(name), em = True)
    stillGrp = mc.group(n = '{}Still_Grp'.format(name), em = True)


def posiReverseNode(name = '', elem = '', side = '', posiLoc = '', ctrl = '', zroGrp = '', zroOfstGrp = '', *args):
    addAttrCtrl (ctrl = '{}Shape'.format(ctrl), elem = ['parameterU', 'parameterV'], min = 0, max = 1, at = 'float',dv = 0.5)
    posiNode = mc.createNode('pointOnSurfaceInfo', n = '{}{}{}Posi'.format(name, elem, side))
    posiMdvNode = mc.createNode('multiplyDivide', n = '{}{}{}Mdv'.format(name, elem, side))
    mc.setAttr('{}.input2X'.format(posiMdvNode), -1)
    mc.setAttr('{}.input2Y'.format(posiMdvNode), -1)
    mc.setAttr('{}.input2Z'.format(posiMdvNode), -1)
    mc.connectAttr('{}Shape.parameterU'.format(ctrl), '{}.parameterU'.format(posiNode))
    mc.connectAttr('{}Shape.parameterV'.format(ctrl), '{}.parameterV'.format(posiNode))

    posi = mc.xform(posiLoc, q=True, t=True, ws = 1)
    nurb = mc.nurbsPlane( axis = [0, 1, 0],w = 0.1, n = '{}{}{}Nrb'.format(name, elem, side))[0]
    mc.delete(mc.parentConstraint(ctrl, nurb, mo = False))
    mc.connectAttr('{}Shape.worldSpace[0]'.format(nurb), '{}.inputSurface'.format(posiNode))

    # mc.connectAttr('{}Shape.parameterU'.format(ctrl), '{}.parameterU'.format(posiNode))
    # mc.connectAttr('{}Shape.parameterV'.format(ctrl), '{}.parameterV'.format(posiNode))
    mc.connectAttr('{}.position'.format(posiNode), '{}.translate'.format(zroGrp))
    mc.connectAttr('{}.translate'.format(ctrl), '{}.input1'.format(posiMdvNode))
    mc.connectAttr('{}.output'.format(posiMdvNode), '{}.translate'.format(zroOfstGrp))

    return nurb

def addNewGeo(*args):

    newGeoLists = mc.ls('Geo_Grp')
    rigGrpoLists = mc.ls('Rig_Grp')
    ctrls = mc.ls(sl = True)
    if ctrls:
        ctrl = ctrls[0]
        ctrlGmbl = '{}Gmbl_Ctrl'.format(ctrl.replace('_Ctrl',''))

        if rigGrpoLists:
            checkRigGrpOld = mc.listRelatives(rigGrpoLists, f = True, c = True)
            geoGrpOld = mc.rename(checkRigGrpOld[0], 'Geo_OldDelGrp')
            checkGeoGrpOld = mc.listRelatives(geoGrpOld, f = True, c = True)
        checkGeo = newGeoLists[-1]
        if checkGeo:
            newGeoGrp = mc.listRelatives((checkGeo.split('|')[-1]), f = True, c = True)[0]
        #check
        for old in checkGeoGrpOld:
            oldGeo = old.split('|')[-1]
            newGeo = newGeoGrp.split('|')[-1]
            if newGeo in oldGeo:
                mc.delete(old)

        mc.parentConstraint(ctrlGmbl, newGeoGrp.split('|')[-1], mo = True)
        mc.scaleConstraint(ctrlGmbl, newGeoGrp.split('|')[-1], mo = True)

        mc.parent(newGeoGrp, geoGrpOld)
        
        if mc.objExists('{}.geoVis'.format(ctrl)):
            mc.connectAttr('{}.geoVis'.format(ctrl), '{}.v'.format(newGeoGrp.split('|')[-1]))
        else:
            mc.connectAttr('{}.GeoVis'.format(ctrl), '{}.v'.format(newGeoGrp.split('|')[-1]))                

        mc.delete(newGeoLists[0])
        mc.rename(geoGrpOld, 'Geo_Grp')
        
    else:

        print 'Please Select Some Control'


def createCvAxis(axisCheck = '', *args):
    ## check axis for project and fix axis control
    if axisCheck:
        axisCheck = axisCheck
    else:
        axisCheck = mc.upAxis(q = True, axis = True);

    if 'z' == axisCheck:
        allCtrl = mc.ls('*:*_Ctrl', '*_Ctrl')
        for ctrl in allCtrl:
            ctrlShape = ctrl + 'Shape'
            allcv = mc.ls('{}.cv[:]'.format(ctrlShape), fl = True)
            mc.select(allcv)    
            mm.eval('rotate -r -ocp -os -fo 90 0 0 ;')
    mc.select(cl = True)

def replaceFileTexture( source = 'Y:/XBB/prod/01_asset/ep00/01_ch/YJ/txr/', 
                        target = 'P:/Donut/ftp/IN/20221103/20221103/character02_For_Maya2020/XBB/prod/01_asset/ep00/01_ch/ForRiff/txr',*args):
    sels = mc.ls('file*')
    for each in sels:
        #print type(each[-1])
        if not 'SG' in each:
            a = mc.getAttr('{}.fileTextureName'.format(each))
            if source in a:
                c = a.replace(source , target)
                # setAttr -type "string" file22.fileTextureName "P:/Donut/ftp/IN/20221103/20221103/character02_For_Maya2020/XBB/prod/01_asset/ep00/01_ch/ForRiff/txr/hi_mouth_alpha2.jpg";
                # mc.setAttr('{}.fileTextureName'.format(each),type = "str", c )
                mm.eval('setAttr -type "string" {}.fileTextureName "{}";'.format(each, c))
                # mc.setAttr(t = "string",'{}.fileTextureName'.format(each), c)
                # mc.setAttr('{}.fileTextureName'.format(each), c)
                # print 'done'
                # setAttr -type "string" file2.fileTextureName "P:/Donut/ftp/IN/20221103/20221103/character02_For_Maya2020/XBB/prod/01_asset/ep00/01_ch/YJ/txr/Low_Diaper_Col.jpg";
                # mc.setAttr( '%s.overrideColor' % shp , cid )


def findSide(side = '',*args):
    if side == 'L':
        side = '_' + side + '_'
    
    elif side == 'R':
        side = '_' + side + '_'
    elif side == 'C':
        side = '_' + side + '_'
    else:
        side = '_'
    return side


def GenerateAnimCurve(*args):
    k =mc.createNode('animCurveUU')
    mc.setKeyframe(k,f= 0.5,v=-0.5,itt='auto',ott = 'auto')
    mc.setKeyframe(k,f= 1,  v= 0,  itt='auto',ott = 'auto')
    mc.setKeyframe(k,f= 2,  v=0.25,itt='auto',ott = 'auto')
    mc.setAttr(k+'.preInfinity',0)
    mc.setAttr(k+'.postInfinity',0)
    mc.keyTangent(k, wt=0)

def GenerateFilePlace2DTexture(name = '', side = '', elem = '', *args):
    fileNode = mc.shadingNode('file', asShader=True, n = '{}{}{}File'.format(name, elem, side))
    place2DTexture = mc.createNode('place2dTexture', n = '{}{}{}place2DTexture'.format(name, elem, side))

    mc.connectAttr('{}.coverage'.format(place2DTexture), '{}.coverage'.format(fileNode))
    mc.connectAttr('{}.translateFrame'.format(place2DTexture), '{}.translateFrame'.format(fileNode))
    mc.connectAttr('{}.rotateFrame'.format(place2DTexture), '{}.rotateFrame'.format(fileNode))
    mc.connectAttr('{}.mirrorU'.format(place2DTexture), '{}.mirrorU'.format(fileNode))
    mc.connectAttr('{}.mirrorV'.format(place2DTexture), '{}.mirrorV'.format(fileNode))
    mc.connectAttr('{}.stagger'.format(place2DTexture), '{}.stagger'.format(fileNode))
    mc.connectAttr('{}.wrapU'.format(place2DTexture), '{}.wrapU'.format(fileNode))
    mc.connectAttr('{}.wrapV'.format(place2DTexture), '{}.wrapV'.format(fileNode))
    mc.connectAttr('{}.repeatUV'.format(place2DTexture), '{}.repeatUV'.format(fileNode))
    mc.connectAttr('{}.offset'.format(place2DTexture), '{}.offset'.format(fileNode))
    mc.connectAttr('{}.rotateUV'.format(place2DTexture), '{}.rotateUV'.format(fileNode))
    mc.connectAttr('{}.noiseUV'.format(place2DTexture), '{}.noiseUV'.format(fileNode))
    mc.connectAttr('{}.vertexUvOne'.format(place2DTexture), '{}.vertexUvOne'.format(fileNode))
    mc.connectAttr('{}.vertexUvTwo'.format(place2DTexture), '{}.vertexUvTwo'.format(fileNode))
    mc.connectAttr('{}.vertexUvThree'.format(place2DTexture), '{}.vertexUvThree'.format(fileNode))
    mc.connectAttr('{}.vertexCameraOne'.format(place2DTexture), '{}.vertexCameraOne'.format(fileNode))
    mc.connectAttr('{}.outUV'.format(place2DTexture),'{}.uv'.format(fileNode))
    mc.connectAttr('{}.outUvFilterSize'.format(place2DTexture),'{}.uvFilterSize'.format(fileNode))


    return fileNode, place2DTexture

def generateMaterial(materialName = '', name = '', side = '', shortName = '',*args):

    shd = mc.shadingNode(materialName, name="{}{}{}".format(name, side, shortName), asShader=True)
    shdSG = mc.sets(name='{}SG'.format(shd), empty=True, renderable=True, noSurfaceShader=True)
    mc.connectAttr('{}.outColor'.format(shd), '{}.surfaceShader'.format(shdSG))
    # mc.sets(shd, e=True, forceElement=shdSG)
    return shd, shdSG

def applyMaterial(node,*args):
    if mc.objExists(node):
        shd = mc.shadingNode('lambert', name="%s_lambert" % node, asShader=True)
        shdSG = mc.sets(name='{}SG'.format(shd), empty=True, renderable=True, noSurfaceShader=True)
        mc.connectAttr('{}.outColor'.format(shd), '{}.surfaceShader'.format(shdSG))
        mc.sets(node, e=True, forceElement=shdSG)


def create_shader(name, node_type="lambert",*args):
    material = mc.shadingNode(node_type, name=name, asShader=True)
    sg = mc.sets(name="{}SG".format(name), empty=True, renderable=True, noSurfaceShader=True)
    mc.connectAttr("{}.outColor".format(material), "{}.surfaceShader".format(sg))
    return material, sg

def colorShade(*args):
    color = [1, 0, 0]
    meshes = mc.ls(selection=True, dag=True, type="mesh", noIntermediate=True)
    mtl, sg = create_shader("helloworld_MTL")
    mc.setAttr(mtl + ".color", color[0], color[1], color[2], type="double3")
    mc.sets(meshes, forceElement=sg)



def getSGfromShader(shader=None):
    if shader:
        if mc.objExists(shader):
            sgq = mc.listConnections(shader, d=True, et=True, t='shadingEngine')
            if sgq:
                return sgq[0]

    return None

def assignObjectListToShader(objList=None, shader=None):
    """
    Assign the shader to the object list
    arguments:
        objList: list of objects or faces
    """
    # assign selection to the shader
    shaderSG = getSGfromShader(shader)
    if objList:
        if not shaderSG == 'initialParticleSE':
            if shaderSG:
                
                mc.sets(objList, e=True, forceElement=shaderSG)
            else:
                print 'The provided shader didn\'t returned a shaderSG'
    else:
        print 'Please select one or more objects'

def assignSelectionToShader(shader=None):
    sel = mc.ls(sl=True, l=True)
    if sel:
        for each in sel:
            assignObjectListToShader(each, shader)


def parentShapeCurve(source = [], target = '', *args):
    if not source:
        sels = mc.ls(sl = True)
        mc.select(sels)
    else:
        for each in source:
            mc.select(each + 'Shape', add = True)
        mc.select(target, add = True)
    mm.eval("parent -r -s;")



def setUnableSelect(*args):
    sels = mc.ls(sl = True)
    for sel in sels:
        mc.select(sel)
        mc.setAttr("{}.overrideEnabled".format(each), 1)
        mc.setAttr("{}.overrideDisplayType".format(each), 2)
        mc.setAttr("{}.selectionChildHighlighting".format(each), 0)

def getMaterialformGeo(geometry = '',*args):
    if geometry:
        theNodes = mc.ls(geometry, dag = True, s = True)
    else:
        theNodes = mc.ls(sl = True, dag = True, s = True)

    shadeEng = mc.listConnections(theNodes, type = 'shadingEngine')
    materials = mc.ls(mc.listConnections(shadeEng ), materials = True)

    return materials

def confirm_dialog(msg = '', *args):
    pmc.confirmDialog(title = 'Confirm', message = msg)


def addBshSwRigToFclRig(source = '', target = '',*args):
    mc.select(source)
    mc.select(target,sdd = True)
    bshTools.add_blendShape(source, target)


def geneteateCurveType(menuName = ['Vis'], elem = 'UiRig', side = '', color = '', template = True, *args):
    side = findSide(side = side)
    fontText = 'MS Shell Dlg 2, 27.8pt'

    menuNameLists = []
    for i in range(len(menuName)):
        attrLists = mc.textCurves(n = '{}{}{}Ctrl'.format(menuName[i], elem, side), f = fontText, t = menuName[i])[0]

        curveLists = listGeo(sels = attrLists, nameing = 'curve', namePass = 'Char')
        mc.parent(curveLists, attrLists)

        ## clean shape of menuName
        targetCtrl = ''
        sourceCtrlShape = []        
        for x in range(len(curveLists)):
            if x == 0:
                splitNameTarget = curveLists[x].split('|')[-1]
                clean ( target = splitNameTarget)  
                targetCtrl = mc.rename(splitNameTarget, '{}View{}{}Ctrl'.format(menuName[i], elem, side))                       
            else:
                splitNameSource = curveLists[x].split('|')[-1]
                clean ( target = splitNameTarget)  
                sourceCtrlShape.append(mc.rename(splitNameSource, '{}{}View{}{}Ctrl'.format(menuName[i], elem, x+1, side)))

        ## parent Shape
        parentShapeCurve(source = sourceCtrlShape, target = targetCtrl)

        if color:
            # assignColor color
            mc.setAttr('{}.overrideEnabled'.format(targetCtrl), 1)
            assignColor(obj = [targetCtrl], col = color)
        if template:
            mc.setAttr("{}.template".format(targetCtrl), 1)
        mc.delete(sourceCtrlShape)


        ## rename
        newObj = mc.rename(attrLists, '{}View{}Ctrl{}Grp'.format(menuName[i], elem, side))

        ## delete char empty 
        charEmpty = listGeo(sels = newObj, nameing = 'Char', namePass = '_Ctrl')
        mc.delete(charEmpty)

        ## freez transform
        pm.makeIdentity(newObj, apply = True)
        menuNameLists.append(newObj)


    return menuNameLists

def checkProject(*args):
    from rf_utils.context import context_info
    reload(context_info)

    asset = context_info.ContextPathInfo()
    projectName = asset.project 
    return projectName

def generateGroup(sels = [], elem = '', *args):
    if not sels:
        sels = mc.ls(sl = True)
    for sel in sels:
        oldName, side, lastName = cn.splitName(sels = [sel])
        # print lastName, '...lastName'
        if 'Ctrl' in lastName:
            lastName = 'Ctrl'
        elif 'Grp' in lastName:
            lastName = 'Zro'
        name = '{}{}{}{}Grp'.format(oldName, elem, lastName, side)
        newGrp = mc.group(n = name, em = True)
        mc.parent(sel, newGrp)
    return newGrp

def setAndCleanAttr(obj = '', translate =  [0,0,0] , rotate = [0,0,0], scale = [1,1,1], color = '', *args):
    ## set attribute
    mc.setAttr('{}.tx'.format(obj), translate[0])
    mc.setAttr('{}.ty'.format(obj), translate[1])
    mc.setAttr('{}.tz'.format(obj), translate[2])
    mc.setAttr('{}.rx'.format(obj), rotate[0])
    mc.setAttr('{}.ry'.format(obj), rotate[1])
    mc.setAttr('{}.rz'.format(obj), rotate[2])
    mc.setAttr('{}.sx'.format(obj), scale[0])
    mc.setAttr('{}.sy'.format(obj), scale[1])
    mc.setAttr('{}.sz'.format(obj), scale[2])
    clean ( target = obj)

    ## color 
    ctrl = listGeo(sels = obj, nameing = '_Ctrl', namePass = '_Grp')
    assignColor(obj = [ctrl], col = color)


def snapPosition(selSource = '', selTarget = '', *args):
    ## generate position of locator
    # centerPivotObj (obj = [selTarget])
    mc.select(selTarget)
    select = lrr.locatorOnMidPos()
    mc.delete(mc.pointConstraint(selTarget, select, mo = False))
    mc.select(select)
    jntLocPos = jointFromLocator()
    snapObj(objA = jntLocPos, objB = selSource)
    mc.delete(jntLocPos)
    # pm.makeIdentity(selSource, apply = True)


def controlLimitTRS (obj = [], translate = True, rotate = True, ValueX = [], ValueY = [], ValueZ = [],*args):
    for each in obj:
        if translate:
            if ValueX:
                mc.transformLimits(each, tx = ValueX, etx = [1, 1])
            if ValueY:
                mc.transformLimits(each, ty = ValueY, ety = [1, 1])
            if ValueZ:
                mc.transformLimits(each, tz = ValueZ, etz = [1, 1])   

        if rotate:
            if ValueX:
                mc.transformLimits(each, rx = ValueX, erx = [1, 1])
            if ValueY:
                mc.transformLimits(each, ry = ValueY, ery = [1, 1])
            if ValueZ:
                mc.transformLimits(each, rz = ValueZ, erz = [1, 1])          


def parentLocalWorldCtrl( ctrl = '' , localObj = '' , worldObj = '' , parGrp = '' ) :
    # Blending parent between local and world object.
    # Returns : locGrp , worGrp , worGrpParCons , parGrpParCons and parGrpParConsRev
    ctrlName, ctrlSide, ctrlLastName = cn.splitName(sels = [ctrl])
    localGrp = mc.group(n = '{}Local{}Grp'.format(ctrlName, ctrlSide), em = True)
    WorldGrp = mc.group(n = '{}World{}Grp'.format(ctrlName, ctrlSide), em = True)

    snapObj(parGrp, localGrp)
    snapObj(parGrp, WorldGrp)

    mc.parent(localGrp, localObj)
    mc.parent(WorldGrp, localObj)
    pm.makeIdentity ( localGrp , apply = True ) ;    
    pm.makeIdentity ( WorldGrp , apply = True ) ;   

    worGrpParCons = pc.parentConstraint( worldObj , WorldGrp , mo = True )
    parGrpParCons = pc.parentConstraint( localGrp , WorldGrp , parGrp , mo = True)
    parGrpParConsRev = pc.Reverse()
    
    con = pc.Dag( ctrl )
    
    attr = 'localWorld'
    con.add( ln = attr , k = True, min = 0 , max = 1 , dv = 0)
    con.attr( attr ) >> parGrpParCons.attr( 'w1' )
    con.attr( attr ) >> parGrpParConsRev.attr( 'ix' )
    parGrpParConsRev.attr( 'ox' ) >> parGrpParCons.attr( 'w0' )
    
    parGrpParCons.attr('interpType').value = 2


    pc.clsl()
    
    return localGrp , WorldGrp , worGrpParCons , parGrpParCons , parGrpParConsRev

# def set_hidden(*args): 
#     from rf_app.model.geo import geo_vis as app
#     reload(app)
#     app.show()
    
# def init_functions(*args): 
#     if entity.step == Step.rig: 
#         ui.hiddenPly_pushButton.clicked.connect(set_hidden)
#         ui.setupLookLayers_pushButton.clicked.connect(setup_looks)

# def setup_ui( entity, parent, *args):
#     if entity.step == Step.rig: 
#         title = 'Rig Setup Ui'
#         size = [200, 100]
#         insUi = ui.RigUi(parent)

# def setup_looks(*args):
#     import os
#     from rf_maya.lib import material_layer 
#     reload(material_layer)
#     from rf_utils.sg import sg_process
#     from rf_maya.rftool.shade import shade_utils
#     from rf_utils.context import context_info

#     entity = []
#     # ui = []
#     entity = context_info.ContextPathInfo() if not entity else entity
#     print entity,' ....entity'
#     entity.context.use_sg(sg_process.sg, project= entity.project, entityType=entity.entity_type, entityName=entity.name)
#     ui = setup_ui(entity, parent)
#     initObj = init_functions()

#     # self.entity = context_info.ContextPathInfo() if not entity else entity
#     # self.entity.context.use_sg(sg_process.sg, project=self.entity.project, entityType=self.entity.entity_type, entityName=self.entity.name)
#     # self.ui = self.setup_ui(self.entity, parent)
#     # self.init_functions()

#     tasks = sg_process.list_looks(self.entity.context.sgEntity)
#     looks = [a.get('sg_name') for a in tasks]
    
#     for look in looks: 
#         material_layer.create_layer(look, mode='rig')

#     layers = material_layer.list_layer_info(self.entity)
#     if layers:
#         # assign look to each layer
#         for lyr, data in layers.iteritems():
#             # set current renderlayer 
#             mc.editRenderLayerGlobals(crl=lyr)

#             shadeFile = data['path']
#             # print shadeFile
#             if not os.path.exists(shadeFile):
#                 continue

#             shadeFileExt = os.path.basename(shadeFile)
#             shadeFileName, ext = os.path.splitext(shadeFileExt)
#             outputList = entity.guess_outputKey(shadeFileExt)
#             if outputList:
#                 # --- assign shaders
#                 outputKey, step = outputList[0]
#                 entity.context.update(step=step)
#                 entity.extract_name(os.path.basename(shadeFile), outputKey=outputKey)
#                 shadeNamespace = entity.mtr_namespace
#                 geoGrpName = entity.projectInfo.asset.geo_grp()
#                 geoGrps = mc.ls('*:%s' %(geoGrpName), type='transform')
#                 if not geoGrps:
#                     geoNamespace = ''
#                 else:
#                     geoGrpNsSplits = geoGrps[0].split(':')
#                     geoNamespace = ':'.join(geoGrpNsSplits[:-1])

#                 shade_utils.apply_ref_shade(shadeFile=shadeFile, 
#                                 shadeNamespace=shadeNamespace, 
#                                 targetNamespace=geoNamespace,
#                                 connectionInfo=[])
                
#         # get back to masterLayer
#         mc.editRenderLayerGlobals(crl='defaultRenderLayer')

def readCtrlShape_cmd(*args):
    import ctrlData
    reload(ctrlData)
    ctrlData.read_ctrl()

def getBlendShapeNode(*args):
    ##Get the Mesh
    my_mesh = mc.ls(sl=True)

    ##Get the blendShape
    my_history = mc.listHistory(my_mesh)

    my_blendShape_node = mc.ls( my_history, type='blendShape')

    ##Get the blend Shape Names
    mc.select(my_blendShape_node)

    my_blendShape_names = mc.listAttr( my_blendShape_node[0] + '.w' , m=True )

    ##print(my_blendShape_names)

def fixPullUV(*args):
    import maya.cmds as mc
    mc.addAttr('Head_Geo', ln = 'ignorePullUV', at = 'bool', k =1 )
    mc.setAttr('Head_Geo.ignorePullUV',1)

def duplicateRename(dupGrp = '', dupGeo = [], oldName = 'Fcl', newName = 'FclMrr', *args):
    if dupGrp:
        ## duplicate fclGeoGrp
        fclGeoGrpDup = mc.duplicate(dupGrp, rc = True, )
    elif dupGeo:
        ## split nameSplite
        nameSp = dupGeo[0].split(':')[0]
        grp = []
        dupGrpName = 'dupBsh{}Geo_Grp'.format(nameSp.split('_')[-1])
        if not mc.objExists(dupGrpName):
            dupBshGrp = mc.group(n = dupGrpName, em = True)
            for geo in dupGeo:
                geo = mc.duplicate(geo, rc = True, )
                mc.parent(geo, dupBshGrp)
                grp.append(geo[0])
        else:
            dupBshGrp = dupGrpName
        fclGeoGrpDup = grp

    if fclGeoGrpDup:
        fclMrrGeoGrp = []
        for eachObj in fclGeoGrpDup:
            subName = eachObj.replace(oldName, newName)
            eachObj = mc.rename(eachObj, subName)
            if '1' in eachObj:
                name, side, lastName = cn.splitName(sels = [eachObj])
                if 'Geo1' in lastName:
                    lastNameSp = lastName.split('1')[0]

                elif 'Grp1' in lastName:
                    lastNameSp = lastName.split('1')[0]
                newReName  = '{}{}{}'.format(name, side, lastNameSp)

                newNameObj = mc.rename(eachObj, newReName)
                fclMrrGeoGrp.append(newNameObj)
            else:
                fclMrrGeoGrp.append(eachObj)
    else:
        fclMrrGeoGrp = ''
        dupBshGrp = dupGrpName 
    return fclMrrGeoGrp, dupBshGrp

def ignorePullUV(geoGrp  = '', *args):

    geoLists = listGeo(sels = geoGrp, nameing = '_Geo', namePass = '_Grp')
    for geo in geoLists:
        obj = geo.split('|')[-1]
        mc.addAttr(obj, ln = 'ignorePullUV', at = 'bool', k =1 )
        mc.setAttr('{}.ignorePullUV'.format(obj), 1)

def clearRenderLayer(*args):
    renderlayers = sorted(mc.ls(type='renderLayer'), reverse=True, key=lambda r:mc.getAttr(r + ".displayOrder"))
    for renderlayer in renderlayers:
        if ':' not in renderlayer:
            if not 'defaultRenderLayer' in renderlayer:
                mc.editRenderLayerGlobals(currentRenderLayer = 'defaultRenderLayer')
                mc.delete(renderlayer)