# สคลิปย้าย attr 'eyebrowLftTwist', 'eyebrowRgtTwist' จาก NoseBsh_Ctrl ไปไว้ที่ 'EyebrowInBsh_L_Ctrl', 'EyebrowInBsh_R_Ctrl'
# รันที่ BshRig/FclRig ก็ได้
# Run at bsh Task
from apTools import misc
reload(misc)

misc.moveAttr('NoseBsh_Ctrl', 'EyebrowInBsh_L_Ctrl', 'eyebrowLftTwist')
misc.attrRename('EyebrowInBsh_L_Ctrl','eyebrowLftTwist','eyebrowTwist')

misc.moveAttr('NoseBsh_Ctrl', 'EyebrowInBsh_R_Ctrl', 'eyebrowRgtTwist')
misc.attrRename('EyebrowInBsh_R_Ctrl','eyebrowRgtTwist','eyebrowTwist')

## Run at bsh Task
## fixAmpFacial(nookr)
from utaTools.utapy import fixAmpFacial
reload(fixAmpFacial)
fixAmpFacial.fixAmpFacial()

## FixRotateFacialAtMouth(kenr)
from utaTools.utapy import utaCore
reload(utaCore)
utaCore.fixMouthCtrl(sels = ['MouthCnrBsh_L_Ctrl','MouthCnrBsh_R_Ctrl'],
                    term = ['Up', 'Lo'],
                    selsCen = ['MouthUprBsh_Ctrl', 'MouthLwrBsh_Ctrl'])