import maya.cmds as mc
import maya.mel as mel
import re

class SnapMotuhLoop(object):
	def __init__(self):
		if mc.window('snapMouthLoop', q=True, ex=True):
			mc.deleteUI('snapMouthLoop', window=True)
		
		mc.window('snapMouthLoop' , t='snapMouthLoop')
		mc.columnLayout (adj=True,cal='left')
		mc.button( label='Ply Edge to Crv' ,w=150, h=30, c=self.plyToCrv)
		mc.rowLayout(numberOfColumns=2 , adj=True)
		mc.text(label='Start : ' , bgc=[0,0,0], w=150, h=30)
		mc.text(label='End : ' , bgc=[0,0,0], w=150, h=30)
		mc.setParent('..')
		mc.rowLayout(numberOfColumns=2 , adj=True)
		self.start = mc.text(label='' , bgc=[0,0,0], w=150, h=30)
		self.end = mc.text(label='' , bgc=[0,0,0], w=150, h=30)
		mc.setParent('..')
		mc.rowLayout(numberOfColumns=2 , adj=True)
		mc.button( label='Add Start' ,w=150, h=30, c=self.addStart)
		mc.button( label='Add End' ,w=150, h=30, c=self.addEnd)
		mc.setParent('..')
		mc.button( label='Snap' ,w=150, h=30, c=self.snapCurve)

		mc.showWindow('snapMouthLoop')
		
		mc.window('snapMouthLoop' ,e=True)

	def snapCurve(self, *args):
		sel =mc.ls(sl=True)
		if sel:
			sortedList = []

			start = mc.text(self.start , q=True, label=True)
			end = mc.text(self.end , q=True, label=True)
			startNum = int(re.findall(r'\d+', start)[-1])
			endNum = int(re.findall(r'\d+', end)[-1])

			mas = mc.listRelatives(sel[0] , c=True , type='nurbsCurve')[0]
			chi = mc.listRelatives(sel[1] , c=True , type='nurbsCurve')[0]
			spans = mc.getAttr(mas+'.spans')
			#degree = mc.getAttr(mas+'.degree')
			mc.rebuildCurve(sel[1],s=spans,d=3,ch=1,rpo=1,rt=0,end=1,kr=0,kcp=0,kep=1,kt=0,tol=0.01)
			mc.delete(chi , ch=True)

			sortList = []
			if endNum > startNum:
				for iA in range(startNum,-1,-1):
					sortList.append(iA)
				for iB in range(spans-1,startNum,-1):
					sortList.append(iB)
			elif endNum < startNum:
				for iA in range(startNum,spans):
					sortList.append(iA)
				for iB in range(startNum):
					sortList.append(iB)
			else:
				print 'Start vertex and end vertex are same cv.'
				return
				
			for i in range(spans):
				pos = mc.xform(sel[0]+'.cv[{}]'.format(sortList[i]),t=True,ws=True,q=True)
				mc.xform(sel[1]+'.cv[{}]'.format(i),t=pos,ws=True)

			mc.delete(sel[0])

	def addStart(self, *args):
		sel = mc.ls(sl=True,flatten=True)
		if sel:
			mc.text(self.start , e=True, label = '%s' % sel[0])

	def addEnd(self, *args):
		sel = mc.ls(sl=True,flatten=True)
		if sel:
			mc.text(self.end , e=True, label = '%s' % sel[0])

	def plyToCrv(self, *args):
		mel.eval('polyToCurve -form 2 -degree 1;')
