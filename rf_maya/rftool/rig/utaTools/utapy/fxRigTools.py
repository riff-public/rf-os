import maya.cmds as mc
import maya.mel as mm
import pymel.core as pm 
import os , sys
import pickle
import math
import maya.mel as mm

from lpRig import rigTools as lrr
reload(lrr)

from rf_utils.context import context_info

from rf_maya.rftool.rig.ncmel import core
reload(core)

from rf_maya.rftool.rig.ncmel import rigTools as rt
reload(rt)

def setSelectPref(*args):
    mc.selectPref(tso=1)
    if mc.selectPref(tso=1, q = 1):
        print 'Set SelectPref Done'
    mc.button('setPrefButt', edit = True, bgc = [0.4,1,0.6])

def getParent(*args):
    selPar = pm.ls(sl=True)
    pm.textField('fillParentName' , edit=True, text = selPar[0])    

def creatCtrl(*args):
    part        = pm.textField('fillPartName', q = True, text= True)
    parent      = pm.textField('fillParentName', q = True, text= True)

    sel = mc.ls(os=1, fl=1)  
    refName = sel[0].split('.')

    if ':' in refName[0]:
        ns,geoname = refName[0].split(':')
    else:
        ns = ''
        geoname = refName[0]  
    
    fullName = geoname.split('_')
    name = fullName[0]
    if len(fullName) == 2:
        side = '_'
    elif len(fullName) == 3:
        side = '_'+fullName[1]+'_'
     
    pathCrv = mc.polyToCurve( form = 2, degree =3, conformToSmoothMeshPreview = 1, n = '{}Path{}Crv'.format(name,side))[0]
    
    ######################getPosi
    edgePosi = mc.xform(sel[0],q=True,t=True,ws = 1)
    
    edgePoint = [(edgePosi[0] + edgePosi[3])/2, (edgePosi[1]+edgePosi[4])/2,(edgePosi[2]+edgePosi[5])/2]    
    cvs = mc.getAttr(pathCrv + '.spans')+mc.getAttr(pathCrv + '.degree')
    startCv = mc.xform(pathCrv + '.cv[{}]'.format(0),q=True,t=True,ws = 1)
    endCv = mc.xform(pathCrv + '.cv[{}]'.format(cvs-1),q=True,t=True,ws = 1)
    
    disStart = math.sqrt(math.pow(edgePoint[0]-startCv[0],2) + math.pow(edgePoint[1]-startCv[1],2) + math.pow(edgePoint[2]-startCv[2],2))
    disEnd = math.sqrt(math.pow(edgePoint[0]-endCv[0],2) + math.pow(edgePoint[1]-endCv[1],2) + math.pow(edgePoint[2]-endCv[2],2))
    
    if disStart > disEnd:
        mc.reverseCurve(pathCrv, ch = 1)
        mc.delete(pathCrv,ch = True)
    else:
        pass
        
    ####################################### 
       
    stillGrp = 'FxRigStill_Grp'
    if not mc.objExists(stillGrp):
        mc.createNode('transform', name = stillGrp)
    
    ctrlGrp = 'FxRigCtrl_Grp'
    if not mc.objExists(ctrlGrp):
        mc.createNode('transform', name = ctrlGrp)

    jntGrp = 'FxRigJnt_Grp'
    if not mc.objExists(jntGrp):
        mc.createNode('transform', name = jntGrp)
        mc.parent(jntGrp,stillGrp)
    
    loc = 'FxRig_Loc'
    if not mc.objExists(loc):
        mc.createNode('transform', name = loc)
        mc.parent(loc,stillGrp)
        attr = ('tx', 'ty', 'tz','rx','ry','rz','sx','sy','sz','visibility')
        for at in attr:
            mc.setAttr(loc + '.' + at, l = True, k = False)

    visLoc = 'VisFxRig_Loc'
    if not mc.objExists(visLoc):
        mc.createNode('transform', name = visLoc)
        mc.parent(visLoc,stillGrp)
        attr = ('tx', 'ty', 'tz','rx','ry','rz','sx','sy','sz','visibility')
        for at in attr:
            mc.setAttr(visLoc + '.' + at, l = True, k = False)     

    ########################################   
    
    listCrv = [pathCrv]
    if pm.checkBox('checkMirror', v = True, q = True) == True:
        print 'ya na i sus'
        if side == '_L_':
            xside = '_R_'
            
        else:
            xside = '_L_'
        xpathCrv = mc.duplicate(pathCrv, name = '{}Path{}Crv'.format(name,xside))[0]
        mc.scale(-1, 1, 1, xpathCrv)
        mc.makeIdentity(xpathCrv, a = True)
        mc.delete(xpathCrv,ch = True)
        listCrv.append(xpathCrv)
    else:
        pass

    for obj in listCrv:
        pathGrp = part + 'PathCrv_Grp'
        if not mc.objExists(pathGrp):
            pathGrp = mc.createNode( 'transform' , name = pathGrp )
            mc.parent(pathGrp,ctrlGrp)
            #mc.parentConstraint(parent,pathGrp, mo =False)
    
        objName = obj.split('_')
        name = objName[0]
        type = objName[-1]
        if len(objName) == 2:
            objSide = '_'
        else:
            objSide = '_' + objName[1] + '_'
     
        zro = mc.createNode( 'transform' , name =  '{}{}Zro{}Grp'.format(name,type,objSide))
        mc.parent(obj,zro)
        mc.parent(zro,pathGrp) 

        ###########################Ctrl

        nameCtrl = name.split('Path')[0]
        ctrlRigGrp = part+'Ctrl_Grp'
        if not mc.objExists(ctrlRigGrp):
            mc.createNode('transform', name = ctrlRigGrp)
            mc.parent(ctrlRigGrp,ctrlGrp)

        parNs,parFullname = parent.split(':')
        parname = parFullname.split('_')
        if len(parname) == 2:
            parSide = '_'
        else:
            parSide = objSide  

        jnt = '{}FxRig{}Jnt'.format(parname[0],parSide)
        if not mc.objExists(jnt):
            jnt = mc.createNode('joint', name = jnt)
            mc.delete(mc.parentConstraint('{}:{}{}Jnt'.format(parNs,parname[0],parSide),jnt))
            mc.parent(jnt,jntGrp) 

            
        ctrl = rt.createCtrl(name = ('{}{}Ctrl').format(nameCtrl,objSide), shape = 'sphere' , col = 'magenta' , traget = '' , jnt = False )
        mc.xform('{}Shape.cv[*]'.format(ctrl), s = [0.05,0.05,0.05])

        zro = mc.createNode( 'transform' , name =  '{}CtrlZro{}Grp'.format(nameCtrl,objSide))
        ofst = mc.createNode( 'transform' , name =  '{}CtrlOfst{}Grp'.format(nameCtrl,objSide))
        mc.parent(ctrl,ofst)
        mc.parent(ofst,zro)
        mc.parent(zro,ctrlRigGrp)

        mc.setAttr(zro + '.inheritsTransform',0)
        mc.scaleConstraint(jnt,zro, mo = True)

        ############################MotionPath
        mtpNode = mc.createNode('motionPath', name =  '{}{}Mtp'.format(nameCtrl,objSide))
        mc.setAttr(mtpNode + '.fractionMode', True)
        mc.setAttr(mtpNode + '.follow', True)
        mc.setAttr(mtpNode + '.frontAxis', 1)
        mc.setAttr(mtpNode + '.upAxis', 2)
        mc.setAttr(mtpNode + '.worldUpType', 0)
        
        mult = mc.createNode('multiplyDivide', name = '{}Mult{}Mdv'.format(nameCtrl,objSide))
        div = mc.createNode('multiplyDivide', name = '{}Div{}Mdv'.format(nameCtrl,objSide)) 
        crvInfo = mc.createNode('curveInfo', name = '{}{}Cif'.format(nameCtrl,objSide))
       
        mc.connectAttr(obj+'Shape.worldSpace[0]',crvInfo + '.inputCurve')

        realArcLength = mc.getAttr(crvInfo + '.arcLength')
        if realArcLength >= 0.05:
            arcLength = round(mc.getAttr(crvInfo + '.arcLength'),1)
        else:
            arcLength = round(mc.getAttr(crvInfo + '.arcLength'),2)
                
        mc.delete(crvInfo)
       
        mc.setAttr(mult + '.input2.input2X', -1)
        mc.setAttr(div + '.operation', 2)
        mc.setAttr(div + '.input2.input2X', arcLength)
        
        mc.connectAttr(obj+'Shape.worldSpace[0]',mtpNode + '.geometryPath')
        mc.connectAttr(mtpNode+'.allCoordinates',zro + '.translate')
        mc.connectAttr(mtpNode+'.rotate',zro + '.rotate')
        mc.connectAttr(ctrl.getName() + '.translateY', mult + '.input1.input1X')
        mc.connectAttr(ctrl.getName() + '.translateY', div + '.input1.input1X') 
        mc.connectAttr(mult + '.output.outputX', ofst + '.ty')
        mc.connectAttr(div + '.output.outputX', mtpNode + '.uValue')
        
        mc.transformLimits(ctrl, ety=(1, 1), ty=(0, arcLength))

        ############################LockCtrl
        mc.addAttr(ctrl, longName = 'reverse', at = 'long', k = True, min = 0, max = 1)
        attr = ('tx','tz','rx','ry','rz','sx','sy','sz','visibility')
        for at in attr:
            mc.setAttr(ctrl.getName() + '.' + at, l = True, k = False)
            
        mc.setAttr(obj + '.overrideEnabled', 1)
        mc.setAttr(obj + '.overrideDisplayType', 2)

        ############################ConnectLocGeovis 
        visCnd = mc.createNode('condition', name = '{}Vis{}Cnd'.format(nameCtrl,objSide))
        revVisCnd = mc.createNode('condition', name = '{}RevVis{}Cnd'.format(nameCtrl,objSide))
        mc.connectAttr(ctrl.getName() + '.reverse', revVisCnd + '.firstTerm')
        mc.setAttr(revVisCnd + '.colorIfTrue.colorIfTrueR', 0)
        mc.setAttr(revVisCnd + '.colorIfFalse.colorIfFalseR', arcLength)
        mc.setAttr(visCnd + '.operation', 0)
        mc.setAttr(visCnd + '.colorIfTrue.colorIfTrueR', 0)
        mc.setAttr(visCnd + '.colorIfFalse.colorIfFalseR', 1)
        mc.connectAttr(ctrl.getName() + '.ty', visCnd + '.firstTerm')
        mc.connectAttr(revVisCnd + '.outColor.outColorR', visCnd + '.secondTerm')
        
        mc.addAttr(visLoc, longName = '{}{}Geo'.format(nameCtrl,objSide), at = 'long', k = True, min = 0, max = 1)
        mc.connectAttr(visCnd + '.outColor.outColorR', visLoc + '.{}{}Geo'.format(nameCtrl,objSide))

        ############################ConnectLoc
        fxName = 'sc__{}{}Rmp'.format(nameCtrl,objSide)
        revName = 'sc__{}{}Cnd'.format(nameCtrl,objSide)
        mc.addAttr(loc, longName = fxName, at = 'float', k = True, min = 0, max = 1)
        mc.addAttr(loc, longName = revName, at = 'long', k = True, min = 0, max = 1)
        mc.connectAttr(mtpNode + '.uValue',loc + '.' + fxName)
        mc.connectAttr(ctrl.getName() + '.reverse',loc + '.' + revName) 
        print '--------------- DONE ---------------'

def connectFxRig(*args):

    ########################################## Create ParsGrp

    stillParsGrp = 'FxRigStillPars_Grp'
    if not mc.objExists(stillParsGrp):
        mc.createNode('transform', name = stillParsGrp)
        mc.parent(stillParsGrp, 'bodyRig_md:Still_Grp')
    
    ctrlParsGrp = 'FxRigCtrlPars_Grp'
    if not mc.objExists(ctrlParsGrp):
        mc.createNode('transform', name = ctrlParsGrp)
        mc.parent(ctrlParsGrp, 'bodyRig_md:AddCtrl_Grp')
        mc.setAttr(ctrlParsGrp + '.inheritsTransform',0)

    wrapGeoGrp = 'FxRigWrapGeo_Grp'
    if not mc.objExists(wrapGeoGrp):
        mc.createNode('transform', name = wrapGeoGrp)
        mc.parent(wrapGeoGrp, stillParsGrp)

    ########################################## Connect Jnt

    fxRingJnt = mc.listRelatives('fxRig_md:FxRigJnt_Grp', c = True, type = 'joint')
    for fxJnt in fxRingJnt:
        name = fxJnt.split(':')
        nameJnt = name[1].split('FxRig')
        jnt = 'bodyRig_md:' + nameJnt[0]+nameJnt[-1]

        mc.parentConstraint(jnt,fxJnt, mo = True)
        mc.scaleConstraint(jnt,fxJnt, mo = True)

    ########################################## Connect Geo

    listGeo = []
    locVisAttr = mc.listAttr('fxRig_md:VisFxRig_Loc', k = True)
    for visAttr in locVisAttr:
        geoName = visAttr.split('sc__')[-1]
        geo = 'bodyRig_md:' +  geoName
        mc.connectAttr('fxRig_md:VisFxRig_Loc.{}'.format(visAttr),geo + '.visibility')
        listGeo.append(geo)
        
    ########################################## Connect Shade

    locAttr = mc.listAttr('fxRig_md:FxRig_Loc', k = True)

    asset = context_info.ContextPathInfo() 
    charName = asset.name

    for attr in locAttr:
        mc.addAttr('bodyRig_md:Geo_Grp', ln = attr, at = 'float', k =True)
        mc.connectAttr('fxRig_md:FxRig_Loc.{}'.format(attr),'bodyRig_md:Geo_Grp.{}'.format(attr))
        
        nodeName = attr.split('sc__')[-1]
        nsShade = charName + '_mdlMainMtr'
        ShadeNode = '{}:{}'.format(nsShade,nodeName)
        typeNode = nodeName.split('_')[-1]
        
        if typeNode == 'Rmp':
            mc.connectAttr('bodyRig_md:Geo_Grp.{}'.format(attr),'{}.colorEntryList[0].position'.format(ShadeNode))
        elif typeNode == 'Cnd':
            mc.connectAttr('bodyRig_md:Geo_Grp.{}'.format(attr),'{}.firstTerm'.format(ShadeNode))

    ########################################### CopyWeight

    headPathShape = mc.listRelatives('fxRig_md:HeadPathCrv_Grp',ad= True, type = 'nurbsCurve')
    headPath = mc.listRelatives(headPathShape,p= True)

    for each in headPath:
        eachFullName = each.split(':')[1]
        eName = eachFullName.split('_')
        eachName,x = eName[0].split('Path')

        if len(eName) == 2:
            eachSide = '_'
        else:
            eachSide = '_{}_'.format(eName[1])

        bodyGeo = 'bodyRig_md:{}{}Geo'.format(eachName,eachSide)
        fxGeo = mc.duplicate(bodyGeo,name = '{}FxRigWrap{}Geo'.format(eachName,eachSide))
        mc.parent (fxGeo,wrapGeoGrp)

        mc.select(bodyGeo)
        mc.select(fxGeo, add =True)
        lrr.doAddBlendShape()

        headPathName = 'fxRig_md:{}Path{}Crv'.format(eachName,eachSide)
        mc.select(headPathName)
        mc.select(fxGeo, add= True)
        wrap = mm.eval('doWrapArgList "7"{"1","0","10","1","0","1","1","0"};')[0]
        #mc.CreateWrap()


    ########################################### CopyWeight
    for obj in listGeo:
        ns,objFullName = obj.split(':')
        objName = objFullName.split('_')
        if len(objName) == 2:
            objSide = '_'
        else:
            objSide = '_{}_'.format(objName[1])
        pathName = 'fxRig_md:{}Path{}Crv'.format(objName[0],objSide)

        if not  pathName in headPath:
            sels = [obj,pathName]
            selsMas = sels[0]
            seleChi = sels[1:]
            for sel in seleChi:
                jnts = mc.skinCluster( selsMas , q = True , inf = True )
            
            oSkn = mm.eval('findRelatedSkinCluster %s' %sels[-1])
            if oSkn :
                mc.skinCluster( oSkn , e = True , ub = True )
            
            skn = mc.skinCluster( jnts , sel , tsb = True )[0]
            
            mc.select( selsMas , r=True )
            mc.select( sel , add=True )
            mm.eval( 'copySkinWeights  -noMirror -surfaceAssociation closestPoint -influenceAssociation closestJoint;' )
            mc.select(cl=True)

    ########################################### Keep Grp

    mc.parent('fxRig_md:FxRigStill_Grp',stillParsGrp)
    mc.parent('fxRig_md:FxRigCtrl_Grp',ctrlParsGrp)

    print ' - C O N N E CT  D O N E -'


def Run():
    windowname = 'FxRigTools'
    if mc.window(windowname, exists = True):
        mc.deleteUI(windowname)

    width = 230
    highth = 250

    ctrlWin = pm.window(windowname,widthHeight=(width, highth), sizeable = False)
    allColumn = pm.columnLayout(adj = True, parent = ctrlWin) 

    #setPref
    pm.separator(h = 5, st = 'none') 
    setPref= pm.rowColumnLayout( numberOfColumns=1, cw = [(1,width)], p = allColumn)
    pm.button('setPrefButt', label = 'SET PREF',command = setSelectPref,p = setPref)
    pm.separator(h = 10)

    #setup 
    setup = pm.columnLayout(cal = 'center', adj = True, parent = allColumn)
    #pm.separator(h = 5, st = 'none')  
    pm.text(label = '-  S E T U P  -', fn = 'boldLabelFont',bgc = [0,0,0] )

    pm.separator(h = 10)

    partRow= pm.rowColumnLayout( numberOfColumns=3, cw = [(1,width*0.25),(2,width*0.55),(3,width*0.2)], p = setup)
    pm.text('  Part', p = partRow, al = 'left')
    part = pm.textField('fillPartName', p = partRow)
    pm.rowColumnLayout (p = partRow) 

    parentRow = pm.rowColumnLayout( numberOfColumns=3, cw = [(1,width*0.25),(2,width*0.55),(3,width*0.2)], p = setup)
    pm.text('  Parent', p = parentRow, al = 'left')
    parent = pm.textField('fillParentName',p = parentRow)
    pm.button(label = u'\u23F4',command = getParent,p = parentRow, bgc = [0.4,0.5,0.5])

    mirrorRow= pm.rowColumnLayout( numberOfColumns=2, cw = [(1,width*0.25),(2,width*0.75)], p = setup)
    pm.text('  Mirror', p = mirrorRow, al = 'left')
    pm.checkBox('checkMirror', label = '', p = mirrorRow, v = False)

    pm.separator(h = 5, st = 'none', p = setup)  
    pm.separator(h = 5, p = setup)

    ############################################################################

    #Path
    createCtrl= pm.rowColumnLayout( numberOfColumns=1, cw = [(1,width)], p = allColumn)
    #createCtrl = pm.columnLayout(cal = 'center', adj = True, cw = [(1,width)] ,parent = allColumn)
      
    pathRow = pm.columnLayout(adj = True, parent = createCtrl)
    pm.button(command = creatCtrl, bgc = [0.3,0.5,0.5] ,label = u'\u25E6' + '  ' + u'\u0E2A\u0E23\u0E49\u0E32\u0E07\u0E04\u0E2D\u0E19\u0E42\u0E17\u0E23\u0E25'+ '  ' + u'\u25E6')
    #pm.button(command = 'creatCtrl()',label = u'\u25A0' + '   CREATE CTRL   ' + u'\u25A0')
    pm.separator(h = 10)

    #main 
    main = pm.columnLayout(cal = 'center', adj = True, parent = allColumn)
    #pm.separator(h = 5, st = 'none')  
    pm.text(label = '-  M A I N  -', fn = 'boldLabelFont',bgc = [0,0,0] )

    pm.separator(h = 10)

    connectMain= pm.rowColumnLayout( numberOfColumns=1, cw = [(1,width)], p = allColumn)
    pm.button('setPrefButt', bgc = [0.3,0.5,0.5] , label = 'Connect FxRig',command = connectFxRig,p = connectMain)

    pm.separator(h = 3, st = 'none')

    warn= pm.rowColumnLayout( numberOfColumns=1, cw = [(1,width)], p = allColumn)
    #pm.text('*Please Ref Mdl Shade Before Run', p = warn)
    pm.text(label = u'\u203C\u0020\u0E42\u0E1B\u0E23\u0E14\u0E40\u0E23\u0E1F\u0E40\u0E17\u0E01\u0E40\u0E08\u0E2D\u0E23\u0E4C\u0E42\u0E21\u0E40\u0E14\u0E25\u0E01\u0E48\u0E2D\u0E19\u0E01\u0E14\u0E1B\u0E38\u0E48\u0E21\u0020\u203C',fn = 'boldLabelFont', p = warn)
    pm.separator(h = 10)

    ############################################################################
        
    mc.showWindow(windowname)