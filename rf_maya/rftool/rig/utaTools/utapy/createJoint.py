# sys.path.append(r'O:\Pipeline\legacy\lib\local\lpRig')
# sys.path.append(r'O:\Pipeline\legacy\lib\local\utaTools\utapy')
import maya.cmds as mc
reload(mc)
import rigTools 
reload(rigTools)
import getNameSpace as gns 
reload(gns)

def createJoint(elem = '',jntsCount='', paths = ''):
    jntsCounts = int(jntsCount)
    ix= 0
    jointAll = []
    for x in range(0, jntsCounts):
        # Create Control
        joints = mc.createNode ('joint', n = ('%s%s%s_Jnt' % (elem, paths, (ix+1))))
        mc.delete(mc.parentConstraint(('TailIk%s_Jnt' % (ix+1),joints ), mo=False))
        mc.makeIdentity( joints, apply=True, translate = True, rotate = True, scale=True )
        mc.parent(joints, ('TailIk%s_Jnt' % (ix+1)))
        ix+=1
        jointAll.append(joints)
    mc.select(clear=True)
    return jointAll

def createMotionsJnt(elem = '', grpCon= True, ns = ''):

    lists = mc.ls('%sTailPathsTmp*_Jnt' % ns)
    listsCouth = len(lists)
 
    ix= 0
    jointAll = []
    for x in range(0, listsCouth):
        # Create Control
        joints = mc.createNode ('joint', n = ('%s%s%s_Jnt' % (ns, elem, (ix+1))))
        mc.delete(mc.parentConstraint((lists[x],joints ), mo=False))
        jointAll.append(joints)   
        # Create Group
        if grpCon == True:
            groupCons = mc.group(em=True, n = ('%s%s%sCon_Grp' % (ns, elem, (ix+1))))
            mc.delete(mc.parentConstraint(joints, groupCons,mo=False))
            mc.parent(joints , groupCons)
            # Parent Joint to Group
            if ns+elem+str(ix)+'Con_Grp' != ns+elem+"0"+'Con_Grp':
                mc.parent(ns+elem+str(ix+1)+'Con_Grp',ns+elem+str(ix)+'_Jnt')

        # Parent Joint to Joint        
        elif grpCon == False:
            if ns+elem+str(ix)+'_Jnt' != ns+elem+"0"+'_Jnt':
                mc.parent(ns+elem+str(ix+1)+'_Jnt',ns+elem+str(ix)+'_Jnt')
        ix+=1  
    mc.select(clear=True)
    return jointAll





