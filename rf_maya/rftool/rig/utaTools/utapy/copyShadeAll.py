import maya.cmds as mc
import maya.mel as mm
import os,sys,traceback
import pymel.core as pm

def cShade(*args):
    fileExt  = ".mb"
    fileType = "mayaBinary"
    tmpFile = "/maya_sceneClipBoard" + fileExt
    tmpdir = mm.eval("getenv TMPDIR")
    shadeFile = (tmpdir + tmpFile)
    edlFile = (tmpdir + "/maya_sceneClipBoard.txt")
    shadeGrp  = [n for n in mc.ls(type='shadingEngine')if not n == 'initialParticleSE' and not n =='initialShadingGroup' and not mc.referenceQuery(n,inr=1)]
    #shadeFile = os.path.join(shadeFilePath,'%s.ma' % publishVer.replace('uv','uv_shadeFile'))
    edlDict = ''
    print edlFile
    materials = []
    if shadeGrp:
        for grp in shadeGrp:
            if  mc.listConnections('%s.surfaceShader' % grp):
                material =  mc.listConnections('%s.surfaceShader' % grp)[0]
                edlDict += '%s' % material # get materail
                materials.append('%s' % material)
                mc.hyperShade(o= material)
                for sl in mc.ls(sl=True):
                    if ':' in sl:
                        sl = sl.replace(sl.split(":")[0]+':','')
                    edlDict += ' %s' % sl # get object
                edlDict += '\r\n'


        f = open(edlFile,'w')
        f.write(edlDict)
        f.close                    

        if shadeGrp:
            mc.select(shadeGrp,noExpand=True)

        mc.file(shadeFile,force=1,
                exportSelected =1,
                constructionHistory=1,
                channels=1,
                expressions=1,
                constraints=1,
                shader=1,
                type =fileType)

        mm.eval('print "Exported shade"')


def pShade(*args):
    fileExt  = ".mb"
    fileType = "mayaBinary"
    tmpFile = "/maya_sceneClipBoard" + fileExt
    tmpdir = mm.eval("getenv TMPDIR")
    shadeFile = (tmpdir + tmpFile)
    edlFile = (tmpdir + "/maya_sceneClipBoard.txt")

    missGeo = ''
    namespace = ''
    sel = mc.ls(sl=True)
    if sel:
        if ':' in sel[0]:
            namespace = sel[0].split(':')[0]
        if os.path.exists(edlFile):

            mc.file(shadeFile,i=True, loadReferenceDepth="all",pr=True,options ="v=0")

            f = open(edlFile,'r')
            txt = f.read()
            f.close()
            for t in txt.split('\r\n'):
                geo = []
                if t.split(' ')[1:]:
                    mc.select(cl=True)
                    for face in t.split(' ')[1:]:
                        if namespace:
                            if not face[:len(namespace)] == ('%s:' % namespace):
                                face = face.split('|')
                                face = ('|%s:'%namespace).join(face)
                                face = '%s:%s' % (namespace,face)
                        try:
                            mc.select(face ,r=True)
                            geo.append(face)

                        except:
                            # face
                            missGeo += '%s\n' % (face)
                    if geo: 
                        mc.select(geo,r=True)
                        mc.hyperShade(assign=t.split(' ')[0] )  
                    mc.select(cl=True)
            print '\n___________________________________\nGeo is missing.\n___________________________________\n%s' % missGeo
        else:
            QtGui.QMessageBox.warning(None,'Assign Shade Window','Hasn''t edl file.',QMessageBox.AcceptRole, QMessageBox.NoButton)

# def copyShadeAllUi(*args):
#     # check if window exists
#     if pm.window ( 'copyShapeWinUi' , exists = True ) :
#         pm.deleteUI ( 'copyShapeWinUi' ) ;
#     else : pass ;
   
#     window = pm.window ( 'copyShapeWinUi', title = "copyShade v. 1.0" ,
#         mnb = False , mxb = False , sizeable = True , rtf = True ) ;
        
#     pm.window ( 'copyShapeWinUi' , e = True , h = 60, w = 200) ;
#     mainLayout = pm.rowColumnLayout ( nc = 1 , p = window , bgc = (0, 0, 0)) ;

#     # 0. copyShape A
#     rowLine1 = pm.rowColumnLayout ( nc = 3 , p = mainLayout , h = 10, w = 200 ) ;
#     pm.button (label = "-"*80 ,h = 10, w = 300, p = rowLine1 , bgc = (0.498039, 1, 0.831373)) ; 

    
#     rowColumnGeoBase = pm.rowColumnLayout ( nc = 2 , p = mainLayout , h = 50 , w = 100 , cw = [ ( 1 , 100 ) , ( 2 , 100 )] ) ;
#     #button copyShade
#     pm.button ( 'copyShadeButton' , label = 'Copy Shade ', h = 50 , w = 100, p = rowColumnGeoBase , c = cShade , bgc = (0.498039, 1, 0.831373)) ;

#     #button paseShade
#     pm.button ( 'paseShadeButton' , label = 'Pase Shade ', h = 50, w = 100, p = rowColumnGeoBase , c = pShade , bgc = (1, 0.411765, 0.705882)) ;

#     # 1. copyShape B
#     rowLine2 = pm.rowColumnLayout ( nc = 3 , p = mainLayout , h = 10, w = 200 ) ;
#     pm.button (label = "-"*80 ,h = 10, w = 300, p = rowLine2 , bgc = (1, 0.411765, 0.705882)) ; 

#     window.show () ;