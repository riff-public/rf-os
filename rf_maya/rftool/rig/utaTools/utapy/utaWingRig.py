from utaTools.utapy import utaCore
reload(utaCore)
import maya.cmds as mc
from lpRig import rigTools as lrr
reload(lrr)
 
#------------------------------------------
# from utaTools.utapy import utaWingRig
# reload(utaWingRig)
# utaWingRig.utaWingRig(geo = 'wingWrap_Geo',
#                         wingMoveATmp = ['wingAMove1_L_Jnt',
#                                         'wingAMove2_L_Jnt'],
#                         wingMoveBTmp = ['wingBMove1_L_Jnt',
#                                         'wingBMove2_L_Jnt'],
#                         wingMoveCTmp = ['wingCMove1_L_Jnt',
#                                         'wingCMove2_L_Jnt'],
#                         parent = ['uparm_L_Jnt', 'forarm_L_Jnt', 'wrist_L_Jnt'])
#--------------------------------------------
def utaWingRig(geo = '', wingMoveATmp = [], wingMoveBTmp = [], wingMoveCTmp = [],parent = [],*args):

	print '# Generate >> split Name'
	name, side, lastName = utaCore.splitName(sels = [geo])
	
	print '# Generate >> gorup'
	geoGrp = mc.group(n = 'wingGeo_Grp', em = True)
	jntGrp = mc.group(n = 'wingJnt_Grp', em = True)
	# skinGrp = mc.group(n = 'wingSkin_Grp', em = True)
	ctrlGrp = mc.group(n = 'wingCtrl_Grp', em = True)
	stillGrp = mc.group(n = 'wingStill_Grp', em = True)

	print '# Generate >> duplicate geo'
	wingBendGeo = mc.group(n = 'wingBendGeo_Grp', em = True)
	wingName = ['wingABend_Geo', 'wingBBend_Geo', 'wingCBend_Geo']
	wingBendObj =[]
	for each in wingName:
		wingBendObj.append(mc.duplicate(geo, n = each)[0])
		mc.select()

	mc.parent(wingBendObj, wingBendGeo)
	mc.parent(wingBendGeo, geo, geoGrp)

	print '# Generate >> add Bend'
	ctrlMove = []
	ctrlGrpMove = []
	wingMoveTipTmp = [wingMoveATmp[-1], wingMoveBTmp[-1], wingMoveCTmp[-1]]
	wingMoveBaseTmp = [wingMoveATmp[0], wingMoveBTmp[0], wingMoveCTmp[0]]
	for i in range(len(wingMoveTipTmp)):
		mc.select(wingMoveTipTmp[i])
		grpCtrlName, grpCtrlOfsName, grpCtrlParsName, ctrlName, GmblCtrlName = utaCore.createControlCurve(curveCtrl = 'cube', parentCon = True, connections = False, geoVis = False)
		ctrlMove.append(ctrlName)
		ctrlGrpMove.append(grpCtrlName)
		## parent joint
		utaCore.parentScaleConstraintLoop(obj = parent[i], lists = [wingMoveBaseTmp[i]],par = True, sca = True, ori = False, poi = False)
		## parent control Grp
		utaCore.parentScaleConstraintLoop(obj = parent[i], lists = [grpCtrlName],par = True, sca = True, ori = False, poi = False)
		print parent[i], '...parent[i]'
		# print parent[i+1], '...[i+1]'
		print len(parent)-1, '...parent..', i
		if not len(parent)-1 == i:
			utaCore.parentScaleConstraintLoop(obj = parent[i+1], lists = [grpCtrlName],par = True, sca = True, ori = False, poi = False)


	print '# Generate >> add nonlineer'
	bendNode = []
	bendHandle = []
	for i in range(len(wingBendObj)):
		mc.select(wingBendObj[i])
		node = mc.nonLinear( type='bend', curvature=0.5)
		nameNode = wingName[i].replace('_Geo', '')
		mc.setAttr('{}.lowBound'.format(node[0]), 0)
		objHandle = mc.rename(node[-1], '{}{}Bnd'.format(nameNode, side))
		bendHandle.append(objHandle)
		objNode = mc.rename(node[0], '{}{}Node'.format(nameNode, side))
		bendNode.append(objNode)
		## connect Attr
		wingNodeMdv = mc.createNode('multiplyDivide', n = '{}{}Mdv'.format(nameNode, side))
		mc.setAttr('{}.input2X'.format(wingNodeMdv), 10)
		mc.connectAttr('{}.tx'.format(ctrlMove[i]), '{}.input1X'.format(wingNodeMdv))
		mc.connectAttr('{}.outputX'.format(wingNodeMdv), '{}.curvature'.format(objNode))



	print '# Generate >> snap bendHandle'
	for i in range(len(wingMoveBaseTmp)):
		mc.select(wingMoveBaseTmp[i])	
		mc.select(bendHandle[i],add = True)
		utaCore.snap()		

	print '# Generate >> add BlendShape'
	mc.select(wingBendObj)
	mc.select(geo,add = True)
	lrr.doAddBlendShape()


	print '# Generate >> wrap group'
	mc.parent(wingMoveATmp, wingMoveBTmp, wingMoveCTmp, jntGrp)
	mc.parent(ctrlGrpMove, ctrlGrp)
	mc.parent(bendHandle, stillGrp)