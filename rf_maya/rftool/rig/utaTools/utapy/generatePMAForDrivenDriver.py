import maya.cmds as mc
import maya.mel as mm 
from utaTools.utapy import utaCore
reload(utaCore)

########################################################################
# utaCore.generatePMAForDrivenDriver(   
#                                 driverCtrl = 'EyeTrgt_R_Ctrl', 
#                                 driverAxis = 'rx', 
#                                 drivenGrp = ['noseSubCtrlOfst_Grp'], 
#                                 drivenAxis = 'tz',
#                                 elem = '2'
#                                 clampMin = 0,
#                                 clampMax = 0,
#                                 )
########################################################################

def generatePMAForDrivenDriver(   
                                driverCtrl = '', 
                                driverAxis = '', 
                                drivenGrp = [], 
                                drivenAxis = '',
                                elem = '',
                                clamp = True,
                                clampMin = -90,
                                clampMax = 0,
                                *args):

    for each in drivenGrp:
        ## unlock Attribute
        utaCore.lockAttrObj(listObj = [each], lock = False,keyable = True,)
        name, side, lastName = utaCore.splitName(sels = [each], optionSide = True)
        ## clear Name Amp
        spName = ['CtrlZro', 'CtrlOfst', 'CtrlPars']
        for nameEach in spName:
            if nameEach in name:
                name = name.replace(nameEach, '')

        ## Add Attribute Name
        attrName = utaCore.addAttrCtrl (ctrl = '{}Shape'.format(driverCtrl) , elem = ['{}{}{}_Amp'.format(name, elem, drivenAxis.capitalize())], min = -200, max = 200, at = 'float')

        ## check connection attribute
        unitConversionNode = mc.listConnections('{}.{}'.format(each, drivenAxis), s = True, type = 'unitConversion')
        if unitConversionNode:
            conCheck = utaCore.checkConnect(object = each, attr = drivenAxis)
            node = mc.listConnections(unitConversionNode, d = False, c = True)

            nameNode = node[0].split('.')[0]
            attrNode = node[0].split('.')[1]
            conCheck = utaCore.checkConnect(object = nameNode, attr = attrNode)
            if conCheck:
                splitAll = conCheck[0].split('.')
                nameNode = splitAll[-2]
                if mc.objectType(nameNode, i = 'plusMinusAverage'):
                    attrNode = mc.listConnections(nameNode, d = False, c = True)

                    # fine input2D
                    splitDot = attrNode[-2].split('.')
                    objInput = splitDot[-2]
                    number = objInput.split('[')[1].replace(']','')
                    numberberPma = int(number)+1
                    pmaChannel = 'input2D[{}]'.format(numberberPma)

                    ## create Amp multiplyDivide 
                    ampMdvNode = mc.createNode('multiplyDivide', n = '{}{}{}{}Mdv'.format(name, elem, drivenAxis.capitalize(), side))

                    mc.connectAttr('{}Shape.{}'.format(driverCtrl, attrName[0]), '{}.input2X'.format(ampMdvNode))
                    mc.connectAttr('{}.outputX'.format(ampMdvNode), '{}.{}.input2Dx'.format(nameNode, pmaChannel))

                    ## Generate ClampNode
                    if clamp:
                        utaCore.addAttrCtrl (ctrl = '{}Shape'.format(driverCtrl) , elem = ['{}{}{}_Min'.format(name, elem, drivenAxis.capitalize()), '{}{}{}_Max'.format(name, elem, drivenAxis.capitalize())], min = -200, max = 200, at = 'float')

                        clampNode = mc.createNode('clamp', n = '{}{}{}{}Cmp'.format(name, elem, drivenAxis.capitalize(), side))

                        mc.connectAttr('{}Shape.{}'.format(driverCtrl, '{}{}{}_Min'.format(name, elem, drivenAxis.capitalize())), '{}.minR'.format(clampNode))
                        mc.connectAttr('{}Shape.{}'.format(driverCtrl, '{}{}{}_Max'.format(name, elem, drivenAxis.capitalize())), '{}.maxR'.format(clampNode))
                        mc.setAttr('{}Shape.{}{}{}_Min'.format(driverCtrl, name, elem, drivenAxis.capitalize()), clampMin)
                        mc.setAttr('{}Shape.{}{}{}_Max'.format(driverCtrl, name, elem, drivenAxis.capitalize()), clampMax)

                        mc.connectAttr('{}.{}'.format(driverCtrl, driverAxis), '{}.inputR'.format(clampNode))
                        mc.connectAttr('{}.outputR'.format(clampNode), '{}.input1X'.format(ampMdvNode))

                    else:
                        mc.connectAttr('{}.{}'.format(driverCtrl, driverAxis), '{}.input1X'.format(ampMdvNode))


                else:
                    pmaNode = mc.createNode( 'plusMinusAverage', n = '{}{}{}{}Pma'.format(name, elem, drivenAxis.capitalize(), side))
                    pmaChannel0 = 'input2D[0]'
                    pmaChannel1 = 'input2D[1]'

                    ## create Amp multiplyDivide 
                    ampMdvNode = mc.createNode('multiplyDivide', n = '{}{}{}{}Mdv'.format(name, elem, drivenAxis.capitalize(), side))

                    mc.connectAttr('{}.{}'.format(nameNode, splitAll[-1]), '{}.{}.input2Dx'.format(pmaNode, pmaChannel0))
                    mc.connectAttr('{}Shape.{}'.format(driverCtrl, attrName[0]), '{}.input2X'.format(ampMdvNode))
                    mc.connectAttr('{}.outputX'.format(ampMdvNode), '{}.{}.input2Dx'.format(pmaNode, pmaChannel1))
                    mc.connectAttr('{}.output2Dx'.format(pmaNode), '{}.{}'.format(each, drivenAxis), f = True)

                    ## Generate ClampNode
                    if clamp:
                        utaCore.addAttrCtrl (ctrl = '{}Shape'.format(driverCtrl) , elem = ['{}{}{}_Min'.format(name, elem, drivenAxis.capitalize()), '{}{}{}_Max'.format(name, elem, drivenAxis.capitalize())], min = -200, max = 200, at = 'float')

                        clampNode = mc.createNode('clamp', n = '{}{}{}{}Cmp'.format(name, elem, drivenAxis.capitalize(), side))
                        mc.connectAttr('{}Shape.{}'.format(driverCtrl, '{}{}{}_Min'.format(name, elem, drivenAxis.capitalize())), '{}.minR'.format(clampNode))
                        mc.connectAttr('{}Shape.{}'.format(driverCtrl, '{}{}{}_Max'.format(name, elem, drivenAxis.capitalize())), '{}.maxR'.format(clampNode))
                        mc.setAttr('{}Shape.{}{}{}_Min'.format(driverCtrl, name, elem, drivenAxis.capitalize()), clampMin)
                        mc.setAttr('{}Shape.{}{}{}_Max'.format(driverCtrl, name, elem, drivenAxis.capitalize()), clampMax)
                        mc.connectAttr('{}.{}'.format(driverCtrl, driverAxis), '{}.inputR'.format(clampNode))
                        mc.connectAttr('{}.outputR'.format(clampNode), '{}.input1X'.format(ampMdvNode))
                    else:
                        mc.connectAttr('{}.{}'.format(driverCtrl, driverAxis), '{}.input1X'.format(ampMdvNode))



        else:
            conCheck = utaCore.checkConnect(object = each, attr = drivenAxis)
            if conCheck:
                nameNode = conCheck[0].split('.')[0]
                attrNode = conCheck[0].split('.')[1]

                if mc.objectType(nameNode, i = 'plusMinusAverage'):
                    attrNode = mc.listConnections(nameNode, d = False, c = True)

                    # fine input2D
                    splitDot = attrNode[-2].split('.')
                    objInput = splitDot[-2]
                    number = objInput.split('[')[1].replace(']','')
                    numberberPma = int(number)+1
                    pmaChannel = 'input2D[{}]'.format(numberberPma)

                    ## create Amp multiplyDivide 
                    ampMdvNode = mc.createNode('multiplyDivide', n = '{}{}{}{}Mdv'.format(name, elem, drivenAxis.capitalize(), side))

                    mc.connectAttr('{}Shape.{}'.format(driverCtrl, attrName[0]), '{}.input2X'.format(ampMdvNode))
                    mc.connectAttr('{}.outputX'.format(ampMdvNode), '{}.{}.input2Dx'.format(nameNode, pmaChannel))

                    ## Generate ClampNode
                    if clamp:
                        utaCore.addAttrCtrl (ctrl = '{}Shape'.format(driverCtrl) , elem = ['{}{}{}_Min'.format(name, elem, drivenAxis.capitalize()), '{}{}{}_Max'.format(name, elem, drivenAxis.capitalize())], min = -200, max = 200, at = 'float')

                        clampNode = mc.createNode('clamp', n = '{}{}{}{}Cmp'.format(name, elem, drivenAxis.capitalize(), side))
                        mc.connectAttr('{}Shape.{}'.format(driverCtrl, '{}{}{}_Min'.format(name, elem, drivenAxis.capitalize())), '{}.minR'.format(clampNode))
                        mc.connectAttr('{}Shape.{}'.format(driverCtrl, '{}{}{}_Max'.format(name, elem, drivenAxis.capitalize())), '{}.maxR'.format(clampNode))
                        mc.setAttr('{}Shape.{}{}{}_Min'.format(driverCtrl, name, elem, drivenAxis.capitalize()), clampMin)
                        mc.setAttr('{}Shape.{}{}{}_Max'.format(driverCtrl, name, elem, drivenAxis.capitalize()), clampMax)
                        mc.connectAttr('{}.{}'.format(driverCtrl, driverAxis), '{}.inputR'.format(clampNode))
                        mc.connectAttr('{}.outputR'.format(clampNode), '{}.input1X'.format(ampMdvNode))
                    else:
                        mc.connectAttr('{}.{}'.format(driverCtrl, driverAxis), '{}.input1X'.format(ampMdvNode))


                else:
                    pmaNode = mc.createNode( 'plusMinusAverage', n = '{}{}{}{}Pma'.format(name, elem, drivenAxis.capitalize(), side))
                    pmaChannel0 = 'input2D[0]'
                    pmaChannel1 = 'input2D[1]'

                   ## create Amp multiplyDivide 
                    ampMdvNode = mc.createNode('multiplyDivide', n = '{}{}{}{}Mdv'.format(name, elem, drivenAxis.capitalize(), side))

                    mc.connectAttr('{}.{}'.format(nameNode, attrNode), '{}.{}.input2Dx'.format(pmaNode, pmaChannel0))
                    mc.connectAttr('{}Shape.{}'.format(driverCtrl, attrName[0]), '{}.input2X'.format(ampMdvNode))
                    mc.connectAttr('{}.outputX'.format(ampMdvNode), '{}.{}.input2Dx'.format(pmaNode, pmaChannel1))
                    mc.connectAttr('{}.output2Dx'.format(pmaNode), '{}.{}'.format(each, drivenAxis), f = True)

                    ## Generate ClampNode
                    if clamp:
                        utaCore.addAttrCtrl (ctrl = '{}Shape'.format(driverCtrl) , elem = ['{}{}{}_Min'.format(name, elem, drivenAxis.capitalize()), '{}{}{}_Max'.format(name, elem, drivenAxis.capitalize())], min = -200, max = 200, at = 'float')

                        clampNode = mc.createNode('clamp', n = '{}{}{}{}Cmp'.format(name, elem, drivenAxis.capitalize(), side))
                        mc.connectAttr('{}Shape.{}'.format(driverCtrl, '{}{}{}_Min'.format(name, elem, drivenAxis.capitalize())), '{}.minR'.format(clampNode))
                        mc.connectAttr('{}Shape.{}'.format(driverCtrl, '{}{}{}_Max'.format(name, elem, drivenAxis.capitalize())), '{}.maxR'.format(clampNode))
                        mc.setAttr('{}Shape.{}{}{}_Min'.format(driverCtrl, name, elem, drivenAxis.capitalize()), clampMin)
                        mc.setAttr('{}Shape.{}{}{}_Max'.format(driverCtrl, name, elem, drivenAxis.capitalize()), clampMax)
                        mc.connectAttr('{}.{}'.format(driverCtrl, driverAxis), '{}.inputR'.format(clampNode))
                        mc.connectAttr('{}.outputR'.format(clampNode), '{}.input1X'.format(ampMdvNode))
                    else:
                        mc.connectAttr('{}.{}'.format(driverCtrl, driverAxis), '{}.input1X'.format(ampMdvNode))

            else:
                pmaNode = mc.createNode( 'plusMinusAverage', n = '{}{}{}{}Pma'.format(name, elem, drivenAxis.capitalize(), side))
                pmaChannel = 'input2D[0]'
          
                ## create Amp multiplyDivide 
                ampMdvNode = mc.createNode('multiplyDivide', n = '{}{}{}{}Mdv'.format(name, elem, drivenAxis.capitalize(), side))


                mc.connectAttr('{}Shape.{}'.format(driverCtrl, attrName[0]), '{}.input2X'.format(ampMdvNode))
                mc.connectAttr('{}.outputX'.format(ampMdvNode), '{}.{}.input2Dx'.format(pmaNode, pmaChannel))
                mc.connectAttr('{}.output2Dx'.format(pmaNode), '{}.{}'.format(each, drivenAxis), f = True)

                ## Generate ClampNode
                if clamp:
                    utaCore.addAttrCtrl (ctrl = '{}Shape'.format(driverCtrl) , elem = ['{}{}{}_Min'.format(name, elem, drivenAxis.capitalize()), '{}{}{}_Max'.format(name, elem, drivenAxis.capitalize())], min = -200, max = 200, at = 'float')

                    clampNode = mc.createNode('clamp', n = '{}{}{}{}Cmp'.format(name, elem, drivenAxis.capitalize(), side))
                    mc.connectAttr('{}Shape.{}'.format(driverCtrl, '{}{}{}_Min'.format(name, elem, drivenAxis.capitalize())), '{}.minR'.format(clampNode))
                    mc.connectAttr('{}Shape.{}'.format(driverCtrl, '{}{}{}_Max'.format(name, elem, drivenAxis.capitalize())), '{}.maxR'.format(clampNode))
                    mc.setAttr('{}Shape.{}{}{}_Min'.format(driverCtrl, name, elem, drivenAxis.capitalize()), clampMin)
                    mc.setAttr('{}Shape.{}{}{}_Max'.format(driverCtrl, name, elem, drivenAxis.capitalize()), clampMax)
                    mc.connectAttr('{}.{}'.format(driverCtrl, driverAxis), '{}.inputR'.format(clampNode))
                    mc.connectAttr('{}.outputR'.format(clampNode), '{}.input1X'.format(ampMdvNode))
                else:
                    mc.connectAttr('{}.{}'.format(driverCtrl, driverAxis), '{}.input1X'.format(ampMdvNode))

        utaCore.lockAttrObj(listObj = [each], lock = True,keyable = False,)