import pymel.core as pm ;

def printYay ( *args ) :
    print ( 'yay' ) ;

def changeColor ( *args ) :
    pm.button ( 'furBtn' , e = True , bgc = ( 1 , 0 , 0 ) ) ;

def printTextF ( *args ) :
    text = pm.textField ( 'testTextField' , q = True , text = True ) ;
    print ( text ) ;
    
def windowUI ( *args ) :
    
    # check if window exists
    if pm.window ( 'windowUI' , exists = True ) :
        pm.deleteUI ( 'windowUI' ) ;
    else : pass ;
   
    window = pm.window ( 'windowUI', title = "Window" ,
        mnb = False , mxb = False , sizeable = True , rtf = True ) ;
        
    pm.window ( 'windowUI' , e = True , w = 310 ) ;
    
    mainLayout = pm.rowColumnLayout ( nc = 1 , p = window ) ;
    
    rowColumn = pm.rowColumnLayout ( nc = 2 , p = mainLayout , w = 310 , cw = [ ( 1 , 155 ) , ( 2 , 155 )] ) ;
    
    pm.button ( label = 'colour' , p = rowColumn , w = 10 , c = changeColor ) ;
    pm.button ( label = 'yayyyyyy' , p = rowColumn , w = 10 , c = printYay ) ;
    
    pm.text ( label = 'lol' , p = rowColumn ) ;
    
    rowColumn2 = pm.rowColumnLayout ( nc = 1 , w = 310 , p = mainLayout ) ;
    
    pm.button ( 'furBtn' , label = 'furrrrr seuy' , w = 310 , p = mainLayout  ) ; 
    
    textFieldYay = pm.textField ( 'testTextField' , w = 310 , p = mainLayout ) ;
    
    pm.button ( label = 'print texField' , p = mainLayout , c = printTextF ) ;
    
    window.show () ;

windowUI ( ) ;