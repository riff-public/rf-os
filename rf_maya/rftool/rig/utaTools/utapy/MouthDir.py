import sys, os
import maya.cmds as mc
import pymel.core as pm
from ncmel import subRig
reload(subRig)
from ncmel import fkRig
reload(fkRig)
from utaTools.pkmel import ctrlShapeTools
reload(ctrlShapeTools)
from utaTools.utapy import utaCore
reload(utaCore)
from ncmel import rigTools
reload(rigTools)

def Run(    
            lipUpCenTmpJnt = ['lipUp_TmpJnt'],
            lipDnCenTmpJnt = ['lipDn_TmpJnt'],
            lipCnrTmpJnt = ['lipCnr_L_TmpJnt', 'lipCnr_R_TmpJnt', 'lipCnrUp_L_TmpJnt', 'lipCnrDn_L_TmpJnt', 'lipCnrUp_R_TmpJnt', 'lipCnrDn_R_TmpJnt'],
            lipUpLTmpJnt = ['lipUp1_L_TmpJnt', 'lipUp2_L_TmpJnt'], 
            lipUpRTmpJnt = ['lipUp1_R_TmpJnt', 'lipUp2_R_TmpJnt'], 
            lipDnLTmpJnt = ['lipDn1_L_TmpJnt', 'lipDn2_L_TmpJnt'], 
            lipDnRTmpJnt = ['lipDn1_R_TmpJnt', 'lipDn2_R_TmpJnt'], 
            jawTmpJnt = ['jawDn1_TmpJnt', 'jawDn2_TmpJnt', 'jawDn3_TmpJnt'],
            tongueTmpJnt = ['tongue01_TmpJnt', 'tongue02_TmpJnt', 'tongue03_TmpJnt', 'tongue04_TmpJnt', 'tongue05_TmpJnt', 'tongue06_TmpJnt'],
            teethMainUpTmpJnt = ['teethMainUp_TmpJnt'],
            teethUpTmpJnt = ['teethUp_TmpJnt', 'teethUp_L_TmpJnt', 'teethUp_R_TmpJnt' ,'teethUpPos_L_TmpJnt', 'teethUpPos_R_TmpJnt'],
            teethMainDnTmpJnt = ['teethMainDn_TmpJnt'],
            teethDnTmpJnt = ['teethDn_TmpJnt', 'teethDn_L_TmpJnt', 'teethDn_R_TmpJnt','teethDnPos_L_TmpJnt', 'teethDnPos_R_TmpJnt' ],
            noseTmpJnt = ['nose_TmpJnt', 'nose_L_TmpJnt', 'nose_R_TmpJnt'],
            dtlOnFace = ['cheekU_L_TmpJnt', 'cheekD_L_TmpJnt', 'cheekU_R_TmpJnt', 'cheekD_R_TmpJnt'],
            teethRig = True, tongueRig = True, detailRig = True, noseRig = True,
            size = 0.75):

    mainLipCenTmpJnt = []
    ## Generate lipMain Up
    lipMainUpJnt = mc.joint(n = 'lipMainUp_TmpJnt')
    mc.delete(mc.parentConstraint(lipUpCenTmpJnt[0], lipMainUpJnt, mo = False))

    ## Check headPos_Jnt 
    if mc.objExists('HeadPos_TmpJnt'):
        HeadPosTmpJnt = 'HeadPos_TmpJnt'
    else:
        HeadPosTmpJnt = mc.joint(n = 'HeadPos_TmpJnt')

    mc.parent(lipMainUpJnt, HeadPosTmpJnt)
    mainLipCenTmpJnt.append(lipMainUpJnt)

    ## Generate lipMain Dn
    lipMainDnJnt = mc.joint(n = 'lipMainDn_TmpJnt')
    mc.delete(mc.parentConstraint(lipDnCenTmpJnt[0], lipMainDnJnt, mo = False))
    mc.parent(lipMainDnJnt, HeadPosTmpJnt)
    mainLipCenTmpJnt.append(lipMainDnJnt)

    tmpJnt = [mainLipCenTmpJnt, lipCnrTmpJnt, lipUpCenTmpJnt, lipUpLTmpJnt, lipUpRTmpJnt, lipDnCenTmpJnt, lipDnLTmpJnt, lipDnRTmpJnt]

    ## Group
    FacialDirRigStillGrp = mc.group(n = 'FacialDirRigStill_Grp', em = True)
    FacialDirRigGrp = mc.group(n = 'FacialDirRigCtrl_Grp', em = True)
    FacialDirSkinGrp = mc.group(n = 'FacialDirRigSkin_Grp', em = True)
    TongueCtrlGrp = mc.group(n = 'TongueRigCtrl_Grp', em = True)
    MouthCtrlGrp = mc.group(n = 'MouthRigCtrl_Grp', em = True)
    TeethCtrlGrp = mc.group(n = 'TeethRigCtrl_Grp', em = True)
    DetailCtrlGrp = mc.group(n = 'DetailRigCtrl_Grp', em = True)
    JawCtrlGrp = mc.group(n = 'JawRigCtrl_Grp', em = True)

    mc.parent(TongueCtrlGrp, MouthCtrlGrp, TeethCtrlGrp, DetailCtrlGrp, JawCtrlGrp, FacialDirRigGrp)

    ## Color Control Group
    mouthColorCtrl = []
    teethColorCtrl = []
    lipColorCtrl = []
    lipMainColorCtrl = []
    greenColorCtrl = []
    sofrlipMainColorCtrl = []
    detailColorCtrl = []
    noseColorCtrl = []
    jawColorCtrl = []
    lipCnrLRCtrl = []
    cnrRotGrp = []
    dtlOfstGrp = []
    mainCnrNewCtrl = []
    mainCnrNewGmbl = []
    mainCnrNewGrp = []

    ## Generate HeadPos Jnt
    HeadJnt = mc.createNode('joint', n = 'HeadPos_Jnt')
    mc.delete(mc.parentConstraint(HeadPosTmpJnt, HeadJnt, mo = False))
    mc.parent(HeadJnt, FacialDirSkinGrp)
    utaCore.parentScaleConstraintLoop(obj = HeadJnt, lists = [ TeethCtrlGrp],par = True, sca = True, ori = False, poi = False)



    ## Create Mouth Control
    mouthJnt = mc.createNode('joint', n = 'Mouth_Jnt')
    mc.delete(mc.parentConstraint(HeadJnt, mouthJnt, mo = False))
    mc.select(mouthJnt)
    MouthCtrlName, MouthCtrlOfsName, MouthCtrlParsName, MouthCtrl, MouthGmblCtrl = utaCore.createControlCurve(curveCtrl = 'circle', parentCon = False, connections = False, geoVis = False)
    mc.parent(MouthCtrlName, MouthCtrlGrp)
    mc.parent(mouthJnt, HeadJnt)
    utaCore.parentScaleConstraintLoop(obj = MouthGmblCtrl, lists = [mouthJnt],par = True, sca = True, ori = False, poi = False)
    mouthColorCtrl.append(MouthCtrl)


    print "# Generate Jaw Rig"
    name = 'Jaw'
    fkRig.Run (     name       = name ,
                    tmpJnt     = jawTmpJnt ,
                    parent     = 'Head_Jnt' ,
                    ctrlGrp    = JawCtrlGrp  ,
                    axis       = 'y' ,
                    shape      = 'square' ,
                    side       = '' ,
                    size       = size 
                    )
    mc.parent('Jaw1Pos_Jnt', mouthJnt)
    jawColorCtrl = ['Jaw1_Ctrl', 'Jaw2_Ctrl']
    utaCore.parentScaleConstraintLoop(obj = HeadJnt, lists = [JawCtrlGrp],par = True, sca = True, ori = False, poi = False)
    utaCore.parentScaleConstraintLoop(obj = 'Jaw1Gmbl_Ctrl', lists = [MouthCtrlGrp],par = True, sca = True, ori = False, poi = False)
    mc.setAttr("Jaw2CtrlPars_Grp.visibility", 0)
    ##-----------------------------------------------------------------------------------------------------------------
    ##-----------------------------------------------------------------------------------------------------------------

    if detailRig:
        print "# Generate detail control"
        for each in dtlOnFace:
            name, side, lastName = utaCore.splitName(sels = [each], optionSide = False)
            subRig.Run  (   name       = name ,
                            tmpJnt     = each,
                            parent     = 'Head_Jnt',
                            ctrlGrp    = DetailCtrlGrp ,
                            shape      = 'cube' ,
                            side       = side ,
                            size       = size
                            )

            if side == 'L':
                side = side.replace(side, '_L_')
            elif side == 'R':
                side = side.replace(side, '_R_')
            elif side == '':
                side = '_'

            utaCore.lockAttrObj(listObj = ['{}SubCtrl{}Grp'.format(name, side), '{}SubCtrlOfst{}Grp'.format(name, side)], lock = False,keyable = True)
            utaCore.parentScaleConstraintLoop(obj = HeadJnt, lists = ['{}SubCtrl{}Grp'.format(name, side)],par = True, sca = True, ori = False, poi = False)
            dtlOfstGrp.append('{}SubCtrlOfst{}Grp'.format(name, side))

            mc.parent('{}Sub{}Jnt'.format(name, side), HeadJnt)
            detailColorCtrl.append('{}Sub{}Ctrl'.format(name, side))


    ##-----------------------------------------------------------------------------------------------------------------
    ##----------------------------------------------------------------------------------------------------------------- 

    print "# Generate Lip Cnr L R Rig"


    for i in range(len(tmpJnt)):
        for each in tmpJnt[i]:
            name, side, lastName = utaCore.splitName(sels = [each], optionSide = False)  
            ## Create new mainControl corner

            if 'Cnr' in each:

                subRig.Run (name= name + 'MainCnr' ,
                            tmpJnt     = each,
                            parent     = '',
                            ctrlGrp    = 'MouthCtrlGrp' ,
                            shape      = 'circle' ,
                            side       = side ,
                            size       = size
                            )
                mainCnrNewCtrl.append('{}Sub{}Ctrl'.format(name + 'MainCnr', '_'+side+'_'))
                mainCnrNewGmbl.append('{}SubGmbl{}Ctrl'.format(name + 'MainCnr', '_'+side+'_'))
                mainCnrNewGrp.append('{}SubCtrl{}Grp'.format(name + 'MainCnr', '_'+side+'_'))
                mc.delete('{}Sub{}Jnt'.format(name + 'MainCnr', '_'+side+'_'))
                utaCore.lockAttrObj(listObj = ['{}SubCtrl{}Grp'.format(name + 'MainCnr', '_'+side+'_')], lock = False,keyable = True)


            ## Create lip control
            lip_uLRig = subRig.Run  (       name       = name ,
                                            tmpJnt     = each,
                                            parent     = '',
                                            ctrlGrp    = 'MouthCtrlGrp' ,
                                            shape      = 'circle' ,
                                            side       = side ,
                                            size       = size
                                            )

            if side == 'L':
                side = side.replace(side, '_L_')
            elif side == 'R':
                side = side.replace(side, '_R_')
            elif side == '':
                side = '_'

            ## delete Node Constraint
            mc.delete('{}Sub{}Jnt_parentConstraint1'.format(name, side), '{}Sub{}Jnt_scaleConstraint1'.format(name, side))
            utaCore.lockAttrObj(listObj = ['{}SubCtrl{}Grp'.format(name, side)], lock = False,keyable = True)
            lipColorCtrl.append('{}Sub{}Ctrl'.format(name, side))
            mc.parent('{}Sub{}Jnt'.format(name, side), mouthJnt)

            ## Joint On Nrb
            norGrp = utaCore.jointOnNrb(    nrb = 'Mouth_Nrb', 
                                            jnt = '{}Sub{}Jnt'.format(name, side), 
                                            control = '{}SubGmbl{}Ctrl'.format(name, side), 
                                            name = name, elem = '', side = side)
            mc.parent(norGrp, FacialDirRigStillGrp)


            ## Generate parent Main Group
            if 'MainUp' in name:
                mainUpGrpGmblCtrl = '{}SubGmbl{}Ctrl'.format(name, side)
                lipMainColorCtrl.append('{}Sub{}Ctrl'.format(name, side))
            elif 'MainDn' in name:
                mainDnGrpGmblCtrl = '{}SubGmbl{}Ctrl'.format(name, side)
                lipMainColorCtrl.append('{}Sub{}Ctrl'.format(name, side))

            if tmpJnt[i] == lipUpCenTmpJnt:
                mc.parent('{}SubCtrl{}Grp'.format(name, side), mainUpGrpGmblCtrl)
            if tmpJnt[i] == lipDnCenTmpJnt:
                mc.parent('{}SubCtrl{}Grp'.format(name, side), mainDnGrpGmblCtrl)       
 
            ## Generate Parent Cnr Group
            # print '.......................////', mainCnrNewGrp
            if 'Cnr' in name:
                if '_L_' in side:
                    cnrGrpGmblLCtrl = '{}SubGmbl{}Ctrl'.format(name, side)
                    lipMainColorCtrl.append('{}Sub{}Ctrl'.format(name, side))
                    cnrPosRot_L_Grp = mc.group(n = 'cnrPosRotate{}Grp'.format('_L_'), em = True)
                    mc.delete(mc.parentConstraint(cnrGrpGmblLCtrl, cnrPosRot_L_Grp, mo = False))
                    mc.parent(cnrPosRot_L_Grp, mainCnrNewGrp[0], cnrGrpGmblLCtrl)
                    cnrRotGrp.append(cnrPosRot_L_Grp)

                    ## fix subDtlCnr Control _L_
                    mc.delete('lipCnrPio{}Grp_orientConstraint1'.format(side), 'lipCnrNor{}Grp_pointConstraint1'.format(side), 'lipCnrNor{}Grp_scaleConstraint1'.format(side))
                    mc.orientConstraint(mainCnrNewGmbl[0], 'lipCnrPio{}Grp'.format(side), mo = True)
                    mc.parentConstraint(mainCnrNewGmbl[0], 'lipCnrNor{}Grp'.format(side), mo = True)
                    mc.scaleConstraint(mainCnrNewGmbl[0], 'lipCnrNor{}Grp'.format(side), mo = True)



                elif '_R_' in side:
                    cnrGrpGmblRCtrl = '{}SubGmbl{}Ctrl'.format(name, side)
                    lipMainColorCtrl.append('{}Sub{}Ctrl'.format(name, side))
                    cnrPosRot_R_Grp = mc.group(n = 'cnrPosRotate{}Grp'.format('_R_'), em = True)
                    mc.delete(mc.parentConstraint(cnrGrpGmblRCtrl, cnrPosRot_R_Grp, mo = False))
                    mc.parent(cnrPosRot_R_Grp, mainCnrNewGrp[-1], cnrGrpGmblRCtrl)
                    cnrRotGrp.append(cnrPosRot_R_Grp)

                    ## fix subDtlCnr Control _R_
                    mc.delete('lipCnrPio{}Grp_orientConstraint1'.format(side), 'lipCnrNor{}Grp_pointConstraint1'.format(side), 'lipCnrNor{}Grp_scaleConstraint1'.format(side))
                    mc.orientConstraint(mainCnrNewGmbl[-1], 'lipCnrPio{}Grp'.format(side), mo = True)
                    mc.parentConstraint(mainCnrNewGmbl[-1], 'lipCnrNor{}Grp'.format(side), mo = True)
                    mc.scaleConstraint(mainCnrNewGmbl[-1], 'lipCnrNor{}Grp'.format(side), mo = True)

            if 'Up2' in name:
                utaCore.lockAttrObj(listObj = ['{}SubCtrlOfst{}Grp'.format(name, side)], lock = False,keyable = True)
                if '_L_' in side:
                    objNameL = '{}Sub{}Ctrl'.format(name, side)
                    mc.parent('{}SubCtrl{}Grp'.format(name, side), cnrPosRot_L_Grp)
                    rigTools.localWorld(    obj = objNameL , 
                                            worldObj = mainUpGrpGmblCtrl , 
                                            localObj = 'cnrPosRotate_L_Grp' , 
                                            consGrp = '{}SubCtrlOfst{}Grp'.format(name, side) , 
                                            type = typ )
                    mc.setAttr("{}SubCtrlOfst{}Grp_{}Constraint1.interpType".format(name, side, typ) ,2)
                    mc.setAttr("{}Sub{}Ctrl.localWorld".format(name, side), 0.3)

                    ## Generate localWorld_Amp
                    newAttr = utaCore.addAttrCtrl('{}Shape'.format(objNameL), elem = ['lipUpLoWo_Amp'], min = -200, max = 200, at = 'float')
                    loWoMdv = mc.createNode('multiplyDivide', n = '{}LoWo{}Mdv'.format(name, side))
                    loWoCmp = mc.createNode('clamp', n = '{}LoWo{}Cmp'.format(name, side))
                    loWoPma = mc.createNode('plusMinusAverage', n = '{}LoWo{}Pma'.format(name, side))
                    mc.setAttr('{}Shape.{}'.format(objNameL, newAttr[0]), 0.7)
                    mc.setAttr('{}.minR'.format(loWoCmp), -0.4)
                    mc.connectAttr('{}.tx'.format('lipCnrSub{}Ctrl'.format(side)), '{}.input1X'.format(loWoMdv))
                    mc.connectAttr('{}Shape.{}'.format(objNameL, newAttr[0]), '{}.input2X'.format(loWoMdv))
                    mc.connectAttr('{}.outputX'.format(loWoMdv), '{}.inputR'.format(loWoCmp))
                    mc.connectAttr('{}.localWorld'.format(objNameL), '{}.input2D[0].input2Dx'.format(loWoPma))
                    mc.connectAttr('{}.outputR'.format(loWoCmp), '{}.input2D[1].input2Dx'.format(loWoPma))
                    mc.connectAttr('{}.output2Dx'.format(loWoPma), '{}SubCtrlOfst{}Grp_parentConstraint1.{}SubWorldSpace{}GrpW1'.format(name, side, name, side), f = True)
                    mc.connectAttr('{}.output2Dx'.format(loWoPma), '{}SubLocWor{}Rev.inputX'.format(name, side), f = True)


                elif '_R_' in side:
                    objNameR = '{}Sub{}Ctrl'.format(name, side)
                    mc.parent('{}SubCtrl{}Grp'.format(name, side), cnrPosRot_R_Grp)
                    rigTools.localWorld(    obj = objNameR , 
                                            worldObj = mainUpGrpGmblCtrl , 
                                            localObj = 'cnrPosRotate_R_Grp' , 
                                            consGrp = '{}SubCtrlOfst{}Grp'.format(name, side) , 
                                            type = typ )
                    mc.setAttr("{}SubCtrlOfst{}Grp_{}Constraint1.interpType".format(name, side, typ) ,2)
                    mc.setAttr("{}Sub{}Ctrl.localWorld".format(name, side), 0.3)

                    ## Generate localWorld_Amp
                    newAttr = utaCore.addAttrCtrl('{}Shape'.format(objNameR), elem = ['lipUpLoWo_Amp'], min = -200, max = 200, at = 'float')
                    loWoMdv = mc.createNode('multiplyDivide', n = '{}LoWo{}Mdv'.format(name, side))
                    loWoCmp = mc.createNode('clamp', n = '{}LoWo{}Cmp'.format(name, side))
                    loWoPma = mc.createNode('plusMinusAverage', n = '{}LoWo{}Pma'.format(name, side))
                    mc.setAttr('{}Shape.{}'.format(objNameR, newAttr[0]), 0.7)
                    mc.setAttr('{}.minR'.format(loWoCmp), -0.4)
                    mc.connectAttr('{}.tx'.format('lipCnrSub{}Ctrl'.format(side)), '{}.input1X'.format(loWoMdv))
                    mc.connectAttr('{}Shape.{}'.format(objNameR, newAttr[0]), '{}.input2X'.format(loWoMdv))
                    mc.connectAttr('{}.outputX'.format(loWoMdv), '{}.inputR'.format(loWoCmp))
                    mc.connectAttr('{}.localWorld'.format(objNameR), '{}.input2D[0].input2Dx'.format(loWoPma))
                    mc.connectAttr('{}.outputR'.format(loWoCmp), '{}.input2D[1].input2Dx'.format(loWoPma))
                    mc.connectAttr('{}.output2Dx'.format(loWoPma), '{}SubCtrlOfst{}Grp_parentConstraint1.{}SubWorldSpace{}GrpW1'.format(name, side, name, side), f = True)
                    mc.connectAttr('{}.output2Dx'.format(loWoPma), '{}SubLocWor{}Rev.inputX'.format(name, side), f = True)

                lipCnrLRCtrl.append('{}SubCtrlOfst{}Grp'.format(name, side))

            elif 'Dn2' in name:
                utaCore.lockAttrObj(listObj = ['{}SubCtrlOfst{}Grp'.format(name, side)], lock = False,keyable = True)
                if '_L_' in side:
                    objNameL = '{}Sub{}Ctrl'.format(name, side)
                    mc.parent('{}SubCtrl{}Grp'.format(name, side), cnrPosRot_L_Grp)
                    rigTools.localWorld(    obj = objNameL , 
                                            worldObj = mainDnGrpGmblCtrl , 
                                            localObj = 'cnrPosRotate_L_Grp' , 
                                            consGrp = '{}SubCtrlOfst{}Grp'.format(name, side) , 
                                            type = typ )
                    mc.setAttr("{}SubCtrlOfst{}Grp_{}Constraint1.interpType".format(name, side, typ) ,2)
                    mc.setAttr("{}Sub{}Ctrl.localWorld".format(name, side), 0.3)

                    ## Generate localWorld_Amp
                    newAttr = utaCore.addAttrCtrl('{}Shape'.format(objNameL), elem = ['lipUpLoWo_Amp'], min = -200, max = 200, at = 'float')
                    loWoMdv = mc.createNode('multiplyDivide', n = '{}LoWo{}Mdv'.format(name, side))
                    loWoCmp = mc.createNode('clamp', n = '{}LoWo{}Cmp'.format(name, side))
                    loWoPma = mc.createNode('plusMinusAverage', n = '{}LoWo{}Pma'.format(name, side))
                    mc.setAttr('{}Shape.{}'.format(objNameL, newAttr[0]), 0.7)
                    mc.setAttr('{}.minR'.format(loWoCmp), -0.4)
                    mc.connectAttr('{}.tx'.format('lipCnrSub{}Ctrl'.format(side)), '{}.input1X'.format(loWoMdv))
                    mc.connectAttr('{}Shape.{}'.format(objNameL, newAttr[0]), '{}.input2X'.format(loWoMdv))
                    mc.connectAttr('{}.outputX'.format(loWoMdv), '{}.inputR'.format(loWoCmp))
                    mc.connectAttr('{}.localWorld'.format(objNameL), '{}.input2D[0].input2Dx'.format(loWoPma))
                    mc.connectAttr('{}.outputR'.format(loWoCmp), '{}.input2D[1].input2Dx'.format(loWoPma))
                    mc.connectAttr('{}.output2Dx'.format(loWoPma), '{}SubCtrlOfst{}Grp_parentConstraint1.{}SubWorldSpace{}GrpW1'.format(name, side, name, side), f = True)
                    mc.connectAttr('{}.output2Dx'.format(loWoPma), '{}SubLocWor{}Rev.inputX'.format(name, side), f = True)


                elif '_R_' in side:
                    objNameR = '{}Sub{}Ctrl'.format(name, side)
                    mc.parent('{}SubCtrl{}Grp'.format(name, side), cnrPosRot_R_Grp)
                    rigTools.localWorld(    obj = objNameR , 
                                            worldObj = mainDnGrpGmblCtrl , 
                                            localObj = 'cnrPosRotate_R_Grp' , 
                                            consGrp = '{}SubCtrlOfst{}Grp'.format(name, side) , 
                                            type = typ )
                    mc.setAttr("{}SubCtrlOfst{}Grp_{}Constraint1.interpType".format(name, side, typ) ,2)
                    mc.setAttr("{}Sub{}Ctrl.localWorld".format(name, side), 0.3)

                    ## Generate localWorld_Amp
                    newAttr = utaCore.addAttrCtrl('{}Shape'.format(objNameR), elem = ['lipUpLoWo_Amp'], min = -200, max = 200, at = 'float')
                    loWoMdv = mc.createNode('multiplyDivide', n = '{}LoWo{}Mdv'.format(name, side))
                    loWoCmp = mc.createNode('clamp', n = '{}LoWo {}Cmp'.format(name, side))
                    loWoPma = mc.createNode('plusMinusAverage', n = '{}LoWo{}Pma'.format(name, side))
                    mc.setAttr('{}Shape.{}'.format(objNameR, newAttr[0]), 0.7)
                    mc.setAttr('{}.minR'.format(loWoCmp), -0.4)
                    mc.connectAttr('{}.tx'.format('lipCnrSub{}Ctrl'.format(side)), '{}.input1X'.format(loWoMdv))
                    mc.connectAttr('{}Shape.{}'.format(objNameR, newAttr[0]), '{}.input2X'.format(loWoMdv))
                    mc.connectAttr('{}.outputX'.format(loWoMdv), '{}.inputR'.format(loWoCmp))
                    mc.connectAttr('{}.localWorld'.format(objNameR), '{}.input2D[0].input2Dx'.format(loWoPma))
                    mc.connectAttr('{}.outputR'.format(loWoCmp), '{}.input2D[1].input2Dx'.format(loWoPma))
                    mc.connectAttr('{}.output2Dx'.format(loWoPma), '{}SubCtrlOfst{}Grp_parentConstraint1.{}SubWorldSpace{}GrpW1'.format(name, side, name, side), f = True)
                    mc.connectAttr('{}.output2Dx'.format(loWoPma), '{}SubLocWor{}Rev.inputX'.format(name, side), f = True)


                lipCnrLRCtrl.append('{}SubCtrlOfst{}Grp'.format(name, side))

            ## Generate ParentConstraint
            ## localWorld
            typ = 'parent'
            if 'Up1' in name:
                utaCore.lockAttrObj(listObj = ['{}SubCtrlOfst{}Grp'.format(name, side)], lock = False,keyable = True)
                if '_L_' in side:
                    objNameL = '{}Sub{}Ctrl'.format(name, side)
                    rigTools.localWorld(    obj = objNameL , 
                                            worldObj = mainUpGrpGmblCtrl , 
                                            localObj = cnrGrpGmblLCtrl , 
                                            consGrp = '{}SubCtrlOfst{}Grp'.format(name, side) , 
                                            type = typ )
                    mc.setAttr("{}SubCtrlOfst{}Grp_{}Constraint1.interpType".format(name, side, typ) ,2)
                    mc.setAttr("{}Sub{}Ctrl.localWorld".format(name, side), 0.6)

                    ## Generate localWorld_Amp
                    newAttr = utaCore.addAttrCtrl('{}Shape'.format(objNameL), elem = ['lipUpLoWo_Amp'], min = -200, max = 200, at = 'float')
                    loWoMdv = mc.createNode('multiplyDivide', n = '{}LoWo{}Mdv'.format(name, side))
                    loWoCmp = mc.createNode('clamp', n = '{}LoWo{}Cmp'.format(name, side))
                    loWoPma = mc.createNode('plusMinusAverage', n = '{}LoWo{}Pma'.format(name, side))
                    mc.setAttr('{}Shape.{}'.format(objNameL, newAttr[0]), 0.4)
                    mc.setAttr('{}.minR'.format(loWoCmp), -0.4)
                    mc.connectAttr('{}.tx'.format('lipCnrSub{}Ctrl'.format(side)), '{}.input1X'.format(loWoMdv))
                    mc.connectAttr('{}Shape.{}'.format(objNameL, newAttr[0]), '{}.input2X'.format(loWoMdv))
                    mc.connectAttr('{}.outputX'.format(loWoMdv), '{}.inputR'.format(loWoCmp))
                    mc.connectAttr('{}.localWorld'.format(objNameL), '{}.input2D[0].input2Dx'.format(loWoPma))
                    mc.connectAttr('{}.outputR'.format(loWoCmp), '{}.input2D[1].input2Dx'.format(loWoPma))
                    mc.connectAttr('{}.output2Dx'.format(loWoPma), '{}SubCtrlOfst{}Grp_parentConstraint1.{}SubWorldSpace{}GrpW1'.format(name, side, name, side), f = True)
                    mc.connectAttr('{}.output2Dx'.format(loWoPma), '{}SubLocWor{}Rev.inputX'.format(name, side), f = True)


                elif '_R_' in side:
                    objNameR = '{}Sub{}Ctrl'.format(name, side)
                    rigTools.localWorld(    obj = objNameR , 
                                            worldObj = mainUpGrpGmblCtrl , 
                                            localObj = cnrGrpGmblRCtrl , 
                                            consGrp = '{}SubCtrlOfst{}Grp'.format(name, side) , 
                                            type = typ )
                    mc.setAttr("{}SubCtrlOfst{}Grp_{}Constraint1.interpType".format(name, side, typ) ,2)
                    mc.setAttr("{}Sub{}Ctrl.localWorld".format(name, side), 0.6)

                    ## Generate localWorld_Amp
                    newAttr = utaCore.addAttrCtrl('{}Shape'.format(objNameR), elem = ['lipUpLoWo_Amp'], min = -200, max = 200, at = 'float')
                    loWoMdv = mc.createNode('multiplyDivide', n = '{}LoWo{}Mdv'.format(name, side))
                    loWoCmp = mc.createNode('clamp', n = '{}LoWo{}Cmp'.format(name, side))
                    loWoPma = mc.createNode('plusMinusAverage', n = '{}LoWo{}Pma'.format(name, side))
                    mc.setAttr('{}Shape.{}'.format(objNameR, newAttr[0]), 0.4)
                    mc.setAttr('{}.minR'.format(loWoCmp), -0.4)
                    mc.connectAttr('{}.tx'.format('lipCnrSub{}Ctrl'.format(side)), '{}.input1X'.format(loWoMdv))
                    mc.connectAttr('{}Shape.{}'.format(objNameR, newAttr[0]), '{}.input2X'.format(loWoMdv))
                    mc.connectAttr('{}.outputX'.format(loWoMdv), '{}.inputR'.format(loWoCmp))
                    mc.connectAttr('{}.localWorld'.format(objNameR), '{}.input2D[0].input2Dx'.format(loWoPma))
                    mc.connectAttr('{}.outputR'.format(loWoCmp), '{}.input2D[1].input2Dx'.format(loWoPma))
                    mc.connectAttr('{}.output2Dx'.format(loWoPma), '{}SubCtrlOfst{}Grp_parentConstraint1.{}SubWorldSpace{}GrpW1'.format(name, side, name, side), f = True)
                    mc.connectAttr('{}.output2Dx'.format(loWoPma), '{}SubLocWor{}Rev.inputX'.format(name, side), f = True)


            elif 'Dn1' in name:
                utaCore.lockAttrObj(listObj = ['{}SubCtrlOfst{}Grp'.format(name, side)], lock = False,keyable = True)
                if '_L_' in side:
                    objNameL = '{}Sub{}Ctrl'.format(name, side)
                    rigTools.localWorld(    obj = objNameL , 
                                            worldObj = mainDnGrpGmblCtrl , 
                                            localObj = cnrGrpGmblLCtrl , 
                                            consGrp = '{}SubCtrlOfst{}Grp'.format(name, side) , 
                                            type = typ )
                    mc.setAttr("{}SubCtrlOfst{}Grp_{}Constraint1.interpType".format(name, side, typ) ,2)
                    mc.setAttr("{}Sub{}Ctrl.localWorld".format(name, side), 0.6)

                    ## Generate localWorld_Amp
                    newAttr = utaCore.addAttrCtrl('{}Shape'.format(objNameL), elem = ['lipUpLoWo_Amp'], min = -200, max = 200, at = 'float')
                    loWoMdv = mc.createNode('multiplyDivide', n = '{}LoWo{}Mdv'.format(name, side))
                    loWoCmp = mc.createNode('clamp', n = '{}LoWo{}Cmp'.format(name, side))
                    loWoPma = mc.createNode('plusMinusAverage', n = '{}LoWo{}Pma'.format(name, side))
                    mc.setAttr('{}Shape.{}'.format(objNameL, newAttr[0]), 0.4)
                    mc.setAttr('{}.minR'.format(loWoCmp), -0.4)
                    mc.connectAttr('{}.tx'.format('lipCnrSub{}Ctrl'.format(side)), '{}.input1X'.format(loWoMdv))
                    mc.connectAttr('{}Shape.{}'.format(objNameL, newAttr[0]), '{}.input2X'.format(loWoMdv))
                    mc.connectAttr('{}.outputX'.format(loWoMdv), '{}.inputR'.format(loWoCmp))
                    mc.connectAttr('{}.localWorld'.format(objNameL), '{}.input2D[0].input2Dx'.format(loWoPma))
                    mc.connectAttr('{}.outputR'.format(loWoCmp), '{}.input2D[1].input2Dx'.format(loWoPma))
                    mc.connectAttr('{}.output2Dx'.format(loWoPma), '{}SubCtrlOfst{}Grp_parentConstraint1.{}SubWorldSpace{}GrpW1'.format(name, side, name, side), f = True)
                    mc.connectAttr('{}.output2Dx'.format(loWoPma), '{}SubLocWor{}Rev.inputX'.format(name, side), f = True)

                elif '_R_' in side:
                    objNameR = '{}Sub{}Ctrl'.format(name, side)
                    rigTools.localWorld(    obj = objNameR , 
                                            worldObj = mainDnGrpGmblCtrl , 
                                            localObj = cnrGrpGmblRCtrl , 
                                            consGrp = '{}SubCtrlOfst{}Grp'.format(name, side) , 
                                            type = typ )
                    mc.setAttr("{}SubCtrlOfst{}Grp_{}Constraint1.interpType".format(name, side, typ) ,2)
                    mc.setAttr("{}Sub{}Ctrl.localWorld".format(name, side), 0.6)

                    ## Generate localWorld_Amp
                    newAttr = utaCore.addAttrCtrl('{}Shape'.format(objNameR), elem = ['lipUpLoWo_Amp'], min = -200, max = 200, at = 'float')
                    loWoMdv = mc.createNode('multiplyDivide', n = '{}LoWo{}Mdv'.format(name, side))
                    loWoCmp = mc.createNode('clamp', n = '{}LoWo{}Cmp'.format(name, side))
                    loWoPma = mc.createNode('plusMinusAverage', n = '{}LoWo{}Pma'.format(name, side))
                    mc.setAttr('{}Shape.{}'.format(objNameR, newAttr[0]), 0.4)
                    mc.setAttr('{}.minR'.format(loWoCmp), -0.4)
                    mc.connectAttr('{}.tx'.format('lipCnrSub{}Ctrl'.format(side)), '{}.input1X'.format(loWoMdv))
                    mc.connectAttr('{}Shape.{}'.format(objNameR, newAttr[0]), '{}.input2X'.format(loWoMdv))
                    mc.connectAttr('{}.outputX'.format(loWoMdv), '{}.inputR'.format(loWoCmp))
                    mc.connectAttr('{}.localWorld'.format(objNameR), '{}.input2D[0].input2Dx'.format(loWoPma))
                    mc.connectAttr('{}.outputR'.format(loWoCmp), '{}.input2D[1].input2Dx'.format(loWoPma))
                    mc.connectAttr('{}.output2Dx'.format(loWoPma), '{}SubCtrlOfst{}Grp_parentConstraint1.{}SubWorldSpace{}GrpW1'.format(name, side, name, side), f = True)
                    mc.connectAttr('{}.output2Dx'.format(loWoPma), '{}SubLocWor{}Rev.inputX'.format(name, side), f = True)


            ## Generate wrap Group

            if 'Cnr' in name:
                mc.parent('{}SubCtrl{}Grp'.format(name, side), MouthGmblCtrl)
                # print '{}SubCtrl{}Grp'.format(name, side), '...chec,.....................................'
            elif 'Main' in name:
                mc.parent('{}SubCtrl{}Grp'.format(name, side), MouthGmblCtrl)
            elif 'Up1' in name:
                mc.parent('{}SubCtrl{}Grp'.format(name, side), MouthGmblCtrl)   
            elif 'Dn1' in name:
                mc.parent('{}SubCtrl{}Grp'.format(name, side), MouthGmblCtrl)

            if 'MainDn' in name:
                mainDnGrp = '{}SubCtrl{}Grp'.format(name, side)
                mainDnPosGrp = mc.group(n = '{}Pos{}Grp'.format(name, side), em = True)
                mc.delete(mc.parentConstraint(mainDnGrp, mainDnPosGrp, mo = False))
                mc.parent(mainDnPosGrp, FacialDirRigStillGrp)
                mc.parent(mainDnGrp, FacialDirRigGrp)
                mainDnPosZroGrp = mc.createNode('transform' , n = '{}PosZro{}Grp'.format(name, side))
                mc.delete(mc.parentConstraint(mainDnPosGrp, mainDnPosZroGrp, mo = False))
                mc.parent(mainDnPosGrp,mainDnPosZroGrp)

                #-- create position grp for follow head
                mainfollowHeadGrp = mc.createNode('transform' , n = '{}HeadPos{}Grp'.format(name, side))
                mc.parent(mainDnPosZroGrp , mainfollowHeadGrp)
                mc.parent(mainfollowHeadGrp , FacialDirRigStillGrp)
                mc.parentConstraint(HeadJnt , mainfollowHeadGrp , mo=True)
                mc.scaleConstraint(HeadJnt , mainfollowHeadGrp , mo=True)

                utaCore.parentScaleConstraintLoop(obj = MouthGmblCtrl, lists = [mainDnPosGrp],par = True, sca = True, ori = False, poi = False)
                mc.select(mainDnPosGrp, r = True)
                mc.select('{}SubCtrlOfst{}Grp'.format(name, side), add = True)
                utaCore.connectTRS(trs = True, translate = False, rotate = False, scale = False)
                utaCore.lockAttrObj(listObj = ['{}SubCtrlOfst{}Grp'.format(name, side)], lock = False, keyable = True)

    ## Create New main corner Control

    ## lip Direc auto
    for each in lipCnrLRCtrl:
        utaCore.lockAttr(listObj = [each], attrs = ['rx', 'ry', 'rz'],lock = False, keyable = True)

    attrAmp = ['cnr_Amp', 'dtlU_Amp', 'dtlD_Amp', 'eyeLidDn_Amp']
    valueAmp = [5, 2, 4, 2]
    sides = ['_L_', '_R_']
    for each in lipMainColorCtrl:
        for sideSel in sides:
            if sideSel in each:
                utaCore.addAttrCtrl (ctrl = '{}Shape'.format(each), elem = attrAmp, min = -200, max = 200, at = 'float')

                for i in range(len(attrAmp)):
                    mc.setAttr('{}Shape.{}'.format(each, attrAmp[i]), valueAmp[i])
                    name, side, lastName = utaCore.splitName(sels = [each], optionSide = False)
                    nameSp = attrAmp[i].split('_')[0]
                    clampNode = mc.createNode('clamp',n = '{}{}{}Cmp'.format(name, nameSp.capitalize(), sideSel))
                    mc.setAttr('{}.minR'.format(clampNode), -5)
                    mc.setAttr('{}.maxR'.format(clampNode), 5)
                    mdvNode = mc.createNode('multiplyDivide',n = '{}{}{}Mdv'.format(name, nameSp.capitalize(), sideSel))
                    mdvRevNode = mc.createNode('multiplyDivide',n = '{}Rev{}{}Mdv'.format(name, nameSp.capitalize(), sideSel))

                    ## Set Attr MultiplyDivice Reverse
                    if 'cnr_Amp' in attrAmp[i]:
                        mc.setAttr('{}.input2X'.format(mdvRevNode), 5)
                        mc.connectAttr('{}.ty'.format(each), '{}.inputR'.format(clampNode))
                    else:
                        if 'dtlU_Amp' in attrAmp[i]:
                            if sideSel == '_L_':
                                mc.setAttr('{}.input2X'.format(mdvRevNode), 0.1)    
                            if sideSel == '_R_':
                                mc.setAttr('{}.input2X'.format(mdvRevNode), -0.1)                               
                            mc.connectAttr('{}.tx'.format(each), '{}.inputR'.format(clampNode))
                        elif 'eyeLid' in attrAmp[i]:
                            mc.setAttr('{}.input2X'.format(mdvRevNode), 10)
                            mc.connectAttr('{}.tx'.format(each), '{}.inputR'.format(clampNode))
                        elif 'dtlD_Amp' in attrAmp[i]:
                            if sideSel == '_L_':
                                mc.setAttr('{}.input2X'.format(mdvRevNode), 0.05)
                            if sideSel == '_R_':
                                mc.setAttr('{}.input2X'.format(mdvRevNode), -0.05)                              
                            mc.connectAttr('{}.tx'.format(each), '{}.inputR'.format(clampNode))
                        else:
                            mc.setAttr('{}.input2X'.format(mdvRevNode), 0.1)
                            mc.connectAttr('{}.ty'.format(each), '{}.inputR'.format(clampNode))

                    mc.connectAttr('{}Shape.{}'.format(each, attrAmp[i]), '{}.input2X'.format(mdvNode))
                    mc.connectAttr('{}.outputR'.format(clampNode), '{}.input1X'.format(mdvNode))
                    mc.connectAttr('{}.outputX'.format(mdvNode), '{}.input1X'.format(mdvRevNode))

                    ## Connect to Object
                    if 'cnr_Amp' in attrAmp[i]:
                        mc.connectAttr('{}.outputX'.format(mdvRevNode), '{}.rz'.format('cnrPosRotate{}Grp'.format(sideSel)))
                    elif 'dtlU_Amp' in attrAmp[i]:
                        mc.connectAttr('{}.outputX'.format(mdvRevNode), '{}.ty'.format('cheekUSubCtrlOfst{}Grp'.format(sideSel)))
                    elif 'dtlD_Amp' in attrAmp[i]:
                        mc.connectAttr('{}.outputX'.format(mdvRevNode), '{}.tx'.format('cheekDSubCtrlOfst{}Grp'.format(sideSel)))

    ##-----------------------------------------------------------------------------------------------------------------
    ##-----------------------------------------------------------------------------------------------------------------

    if tongueRig:
        print "# Generate Tongue Rig"
        fkRig.Run (     name       = 'Tongue' ,
                        tmpJnt     = tongueTmpJnt ,
                        parent     = 'Head_Jnt' ,
                        ctrlGrp    = TongueCtrlGrp  ,
                        axis       = 'y' ,
                        shape      = 'circle' ,
                        side       = '' ,
                        size       = size 
                        )
        mc.parent('Tongue1Pos_Jnt', 'Jaw2Pos_Jnt')
        utaCore.parentScaleConstraintLoop(obj = 'Jaw2Gmbl_Ctrl', lists = [TongueCtrlGrp],par = True, sca = True, ori = False, poi = False)

    ##-----------------------------------------------------------------------------------------------------------------
    ##-----------------------------------------------------------------------------------------------------------------

    if teethRig :

        print "# Generate Teeth Up Rig"
        teethUpJntGrp = [teethMainUpTmpJnt, teethUpTmpJnt]
        teethUpGrp = []
        teethMainUpGmblCtrl = []
        teethUpJnt = []
        teethMainUpJnt = []
        for i in range(len(teethUpJntGrp)):
            for each in teethUpJntGrp[i]:
                name, side, lastName = utaCore.splitName(sels = [each], optionSide = False)
                subRig.Run  (   name       = name ,
                                tmpJnt     = each,
                                parent     = '',
                                ctrlGrp    = TeethCtrlGrp ,
                                shape      = 'cube' ,
                                side       = side ,
                                size       = size
                                )

                if side == 'L':
                    side = side.replace(side, '_L_')
                elif side == 'R':
                    side = side.replace(side, '_R_')
                elif side == '':
                    side = '_'

                utaCore.lockAttrObj(listObj = ['{}SubCtrl{}Grp'.format(name, side)], lock = False,keyable = True)
                teethColorCtrl.append('{}Sub{}Ctrl'.format(name, side))

                if 'teethUp' in each:
                    teethUpGrp.append('{}SubCtrl{}Grp'.format(name, side))
                    teethUpJnt.append('{}Sub{}Jnt'.format(name, side))

                if 'teethMainUp' in each:
                    teethMainUpGmblCtrl.append('{}SubGmbl{}Ctrl'.format(name, side))
                    teethMainUpJnt.append('{}Sub{}Jnt'.format(name, side))      

        for each in teethUpGrp:
            mc.parent(each, teethMainUpGmblCtrl[0]) 
        for each in teethUpJnt:
            mc.parent(each, teethMainUpJnt[0])  
        mc.parent(teethMainUpJnt[0], mouthJnt)  



        print "# Generate Teeth Dn Rig"
        teethDnJntGrp = [teethMainDnTmpJnt, teethDnTmpJnt]
        teethDnGrp = []
        teethMainDnGmblCtrl = []
        teethDnJnt = []
        teethMainDnJnt = []
        for i in range(len(teethDnJntGrp)):
            for each in teethDnJntGrp[i]:
                name, side, lastName = utaCore.splitName(sels = [each], optionSide = False)
                subRig.Run  (   name       = name ,
                                tmpJnt     = each,
                                parent     = '',
                                ctrlGrp    = TeethCtrlGrp ,
                                shape      = 'cube' ,
                                side       = side ,
                                size       = size
                                )

                if side == 'L':
                    side = side.replace(side, '_L_')
                elif side == 'R':
                    side = side.replace(side, '_R_')
                elif side == '':
                    side = '_'

                utaCore.lockAttrObj(listObj = ['{}SubCtrl{}Grp'.format(name, side)], lock = False,keyable = True)
                teethColorCtrl.append('{}Sub{}Ctrl'.format(name, side))

                if 'teethDn' in each:
                    teethDnGrp.append('{}SubCtrl{}Grp'.format(name, side))
                    teethDnJnt.append('{}Sub{}Jnt'.format(name, side))

                if 'teethMainDn' in each:
                    teethMainDnGmblCtrl.append('{}SubGmbl{}Ctrl'.format(name, side))
                    teethMainDnJnt.append('{}Sub{}Jnt'.format(name, side))
                    utaCore.parentScaleConstraintLoop(obj = 'Jaw2Gmbl_Ctrl', lists = ['{}SubCtrl{}Grp'.format(name, side)],par = True, sca = True, ori = False, poi = False)

        for each in teethDnGrp:
            mc.parent(each, teethMainDnGmblCtrl[0]) 
        for each in teethDnJnt:
            mc.parent(each, teethMainDnJnt[0])  

        mc.parent(teethMainDnJnt[0], mouthJnt)  

    if noseRig:
        print "# Generate Nose Rig"
        # noseJntGrp = [noseTmpJnt]
        noseGrp = []
        noseCenGmblCtrl = []
        noseCenJntGrp = []
        noseJnt = []

        for each in noseTmpJnt:
            name, side, lastName = utaCore.splitName(sels = [each], optionSide = False)
            subRig.Run  (   name       = name ,
                            tmpJnt     = each,
                            parent     = '',
                            ctrlGrp    = TeethCtrlGrp ,
                            shape      = 'cube' ,
                            side       = side ,
                            size       = size
                            )

            if side == 'L':
                side = side.replace(side, '_L_')
            elif side == 'R':
                side = side.replace(side, '_R_')
            elif side == '':
                side = '_'

            utaCore.lockAttrObj(listObj = ['{}SubCtrl{}Grp'.format(name, side)], lock = False,keyable = True)
            noseColorCtrl.append('{}Sub{}Ctrl'.format(name, side))

            if not side == '_':
                noseGrp.append('{}SubCtrl{}Grp'.format(name, side))
                noseJnt.append('{}Sub{}Jnt'.format(name, side))

            if side == '_':
                noseCenGmblCtrl.append('{}SubGmbl{}Ctrl'.format(name, side))
                noseCenJntGrp.append('{}Sub{}Jnt'.format(name, side))
                utaCore.parentScaleConstraintLoop(obj = HeadJnt, lists = ['{}SubCtrl{}Grp'.format(name, side)],par = True, sca = True, ori = False, poi = False)
                

        for each in noseGrp:
            mc.parent(each, noseCenGmblCtrl[0]) 
        for each in noseJnt:
            mc.parent(each, noseCenJntGrp[0])   
        mc.parent(noseCenJntGrp[0], mouthJnt)   

    ## Generate Color fo Control----------------------------------------------------------------------------------------
    utaCore.assignColor(obj = lipColorCtrl, col = 'softBlue')
    utaCore.assignColor(obj = teethColorCtrl, col = 'yellow')
    utaCore.assignColor(obj = lipMainColorCtrl, col = 'red')
    utaCore.assignColor(obj = mouthColorCtrl, col = 'yellow')
    utaCore.assignColor(obj = detailColorCtrl, col = 'green')
    utaCore.assignColor(obj = noseColorCtrl, col = 'red')
    utaCore.assignColor(obj = jawColorCtrl, col = 'blue')

    mc.parent('Mouth_Nrb', FacialDirRigStillGrp)
    mc.setAttr ("{}.drawStyle".format(mouthJnt), 2)
    # # mc.delete('TmpJnt_Grp')

    #-- skin nrb
    mc.skinCluster( HeadJnt , 'Mouth_Nrb' , tsb = True , mi = 1 , n = 'MouthNrb_Skc')


    print 'D O N E'