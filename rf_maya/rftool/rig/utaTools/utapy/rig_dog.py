import maya.cmds as mc
import pkmel.core as pc
reload( pc )
import pkmel.rigTools as rigTools
reload( rigTools )
import pkmel.mainGroup as pmain
reload( pmain )
import pkmel.rootRig as proot
reload( proot )
import pkmel.pelvisRig as ppelv
reload( ppelv )
import pkmel.spineRig as pspi
reload( pspi )
import pkmel.humanSpineRig as phspi
reload( phspi )
import pkmel.neckRig as pneck
reload( pneck )
import pkmel.headRig as phead
reload( phead )
import pkmel.clavicleRig as pclav
reload( pclav )
import pkmel.armRig as parm
reload( parm )
import pkmel.legRig as pleg
reload( pleg )
import pkmel.backLegRig as pbleg
reload( pbleg )
import pkmel.fingerRig as pfngr
reload( pfngr )
import pkmel.thumbRig as pthmb
reload( pthmb )
import pkmel.ribbon as prbn
reload( prbn )

def main() :
	placement = pc.Dag( 'placement_tmpCtrl' )
# Naming
	charName = ''
	elem = ''

# Rig
	mainGroup = pmain.MainGroup()
	rigTools.nodeNaming( mainGroup , charName = charName , elem = elem , side = '' )

	anim = mainGroup.anim_grp
	jnt = mainGroup.jnt_grp
	skin = mainGroup.skin_grp
	ikh = mainGroup.ikh_grp
	still = mainGroup.still_grp
	size = 0.2
	
# Arrange hierarchy for Friends!!!!
	mc.parent( mainGroup.placement_ctrl , w=True )
	mc.parent( mainGroup.still_grp , w=True )
	mainGroup.offset_ctrl.name = 'placementCtrlOfst_grp'
	mc.delete( mainGroup.offset_ctrl.shape )
	mc.select( mainGroup.placement_ctrl )
	placementGimbal = rigTools.doAddGimbal()
	mainGroup.offset_ctrl.parent( placementGimbal )
	mc.delete( mainGroup.rig_grp )

# Root
	rootRig = proot.RootRig( animGrp = anim ,
			skinGrp = skin ,
			charSize = size ,
			tmpJnt = 'root_tmpJnt' )

	rigTools.nodeNaming( rootRig ,
			charName = charName ,
			elem = elem ,
			side = '' )
# Pelvis
	pelvisRig = ppelv.PelvisRig( parent = rootRig.root_jnt.name ,
				animGrp = anim ,
				skinGrp = skin ,
				charSize = size ,
				tmpJnt = 'pelvis_tmpJnt' )

	rigTools.nodeNaming( pelvisRig ,
			charName = charName ,
			elem = elem ,
			side = '' )
# Spine	
	spineRig = pspi.SpineRig( parent = rootRig.root_jnt.name ,
				animGrp = anim ,
				jntGrp = jnt ,
				ikhGrp = ikh ,
				skinGrp = skin ,
				stillGrp = still ,
				ax = 'z',
				ribbon = False,
				charSize = size ,
				tmpJnt = ( 
						'spline01_tmpJnt' , 
						'spline02_tmpJnt' , 
						'neck_tmpJnt',
						'chestPic_loc' 
					) 
				)

	rigTools.nodeNaming( spineRig ,
			charName = charName ,
			elem = elem ,
			side = '' )

	rigTools.dummyNaming( obj = spineRig.lowSpineRbn ,
			attr = 'rbn' ,
			dummy = 'lowSpineRbn' ,
			charName = charName ,
			elem = elem ,
			side = '' )

	rigTools.dummyNaming( obj = spineRig.upSpineRbn ,
			attr = 'rbn' ,
			dummy = 'upSpineRbn' ,
			charName = charName ,
			elem = elem ,
			side = '' )

# Spine fix
	spinePos = pc.Null()
	spinePos.name = 'spinePos_grp'
	spinePos.snap( spineRig.spineRig_grp )
	mc.pointConstraint( pelvisRig.pelvis_jnt , spinePos )
	spinePos.parent( spineRig.spineRig_grp )

	spineElems = [
					spineRig.spineCtrl_grp ,
					spineRig.spineFkCtrl_grp ,
					spineRig.spineIkCtrl_grp ,
					spineRig.spineRbnAnim_grp
					]
	for spineElem in spineElems :
		
		attrs = ( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' )
		for attr in attrs :
			spineElem.attr( attr ).lock = 0
		
		# spineElem.parent( spinePos )
		mc.parentConstraint( spinePos , spineElem , mo=True )

	attrs = ( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' )
	for attr in attrs :
		spineRig.spineIkCtrlLoc_grp.attr( attr ).lock = 0
	mc.parentConstraint( rootRig.root_jnt , spineRig.spineIkCtrlLoc_grp , mo=True )
			
# Neck
	neckRig = pneck.NeckRig( parent = spineRig.spine2_jnt ,
			animGrp = anim ,
			jntGrp = jnt ,
			skinGrp = skin ,
			stillGrp = still ,
			ribbon = False ,
			ax = 'y' ,
			charSize = size ,
			tmpJnt = ( 'neck_tmpJnt' , 'head01_tmpJnt' ) )

	rigTools.nodeNaming( neckRig ,
			charName = charName ,
			elem = elem ,
			side = '' )

	rigTools.dummyNaming( obj = neckRig.neckRbn ,
			attr = 'rbn' ,
			dummy = 'neckRbn' ,
			charName = charName ,
			elem = elem ,
			side = '' )

# Head
	headRig = phead.HeadRig( parent = neckRig.neck2_jnt ,
			animGrp = anim ,
			skinGrp = skin ,
			charSize = size ,
			tmpJnt = ( 'head01_tmpJnt' , 'head02_tmpJnt' , 'eyeLFT_tmpJnt' , 'eyeRGT_tmpJnt' , 'jaw01LO_tmpJnt' , 'jaw02LO_tmpJnt' , 'jaw03LO_tmpJnt' , 'jaw01UP_tmpJnt' , 'jaw02UP_tmpJnt' , 'eyeTrgt_tmpJnt' , 'eyeTrgtLFT_tmpJnt' , 'eyeTrgtRGT_tmpJnt' ) )

	rigTools.nodeNaming( headRig ,
			charName = charName ,
			elem = elem ,
			side = '' )

	# attrs = ( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' )
	# for attr in attrs :
	# 	neckRig.headSpc_grp.attr( attr ).l = 0

	# headRigeadPosGrp_parCons = pc.parentConstraint( headRig.head1_jnt , neckRig.headSpc_grp )

# Clavicle left
	clavLRig = pclav.ClavicleRig( parent = spineRig.spine2_jnt ,
				side = 'LFT' ,
				animGrp = anim ,
				skinGrp = skin ,
				charSize = size ,
				tmpJnt = ( 'clavFRLFT_tmpJnt' , 'upLegFRLFT_tmpJnt' ) )

	rigTools.nodeNaming( clavLRig ,
			charName = charName ,
			elem = elem ,
			side = 'LFT' )

# Clavicle right:
	clavRRig = pclav.ClavicleRig( parent = spineRig.spine2_jnt ,
				side = 'RGT' ,
				animGrp = anim ,
				skinGrp = skin ,
				charSize = size ,
				tmpJnt = ( 'clavFRRGT_tmpJnt' , 'upLegFRRGT_tmpJnt' ) )

	rigTools.nodeNaming( clavRRig ,
			charName = charName ,
			elem = elem ,
			side = 'RGT' )

# Hip left
	hipLRig = pclav.ClavicleRig( parent = pelvisRig.pelvis_jnt ,
				side = 'LFT' ,
				animGrp = anim ,
				skinGrp = skin ,
				charSize = size ,
				tmpJnt = ( 'hipBKLFT_tmpJnt' , 'upLegBKLFT_tmpJnt' ) )

	rigTools.nodeNaming( hipLRig ,
			charName = charName ,
			elem = 'Hip' ,
			side = 'LFT' )

# Hip right:
	hipRRig = pclav.ClavicleRig( parent = pelvisRig.pelvis_jnt ,
				side = 'RGT' ,
				animGrp = anim ,
				skinGrp = skin ,
				charSize = size ,
				tmpJnt = ( 'hipBKRGT_tmpJnt' , 'upLegBKRGT_tmpJnt' ) )

	rigTools.nodeNaming( hipRRig ,
			charName = charName ,
			elem = 'Hip' ,
			side = 'RGT' )

# Front leg left
	bLegLRig = pbleg.BackLegRig( parent = clavLRig.clav2_jnt,
			side = 'LFT' ,
			animGrp = anim ,
			jntGrp = jnt ,
			ikhGrp = ikh ,
			skinGrp = skin ,
			stillGrp = still ,
			ribbon = True ,
			charSize = size ,
			tmpJnt = ( 	'upLegFRLFT_tmpJnt' , 
						'lowLegFRLFT_tmpJnt' , 
						'kneeFRLFT_tmpJnt' ,
						'ankleFRLFT_tmpJnt' , 
						'ballFRLFT_tmpJnt' ,
				  		'toeFRLFT_tmpJnt' ,
				  		'heelFRLFT_tmpJnt' ,
				   		'footInFRLFT_tmpJnt' ,
				    	'footOutFRLFT_tmpJnt' , 
				   		'kneeIkFRLFT_tmpJnt' ) )

	rigTools.nodeNaming( bLegLRig ,
			charName = charName ,
			elem = 'Font' ,
			side = 'LFT' ) 

	rigTools.dummyNaming( obj = bLegLRig.upLegRbn ,
			attr = 'rbn' ,
			dummy = 'upLegFontRbn' ,
			charName = charName ,
			elem = '' ,
			side = 'LFT' )

	rigTools.dummyNaming( obj = bLegLRig.midLegRbn ,
			attr = 'rbn' ,
			dummy = 'midLegFontRbn' ,
			charName = charName ,
			elem = '' ,
			side = 'LFT' )

	rigTools.dummyNaming( obj = bLegLRig.lowLegRbn ,
			attr = 'rbn' ,
			dummy = 'lowLegFontRbn' ,
			charName = charName ,
			elem = '' ,
			side = 'LFT' )

# Font  leg right
	bLegRRig = pbleg.BackLegRig( parent = clavRRig.clav2_jnt ,
			side = 'RGT' ,
			animGrp = anim ,
			jntGrp = jnt ,
			ikhGrp = ikh ,
			skinGrp = skin ,
			stillGrp = still ,
			ribbon = True ,
			charSize = size ,
			tmpJnt = ( 	'upLegFRRGT_tmpJnt' , 
						'lowLegFRRGT_tmpJnt' , 
						'kneeFRRGT_tmpJnt' , 
						'ankleFRRGT_tmpJnt' , 
						'ballFRRGT_tmpJnt' , 
						'toeFRRGT_tmpJnt' , 
						'heelFRRGT_tmpJnt' , 
						'footInFRRGT_tmpJnt' , 
						'footOutFRRGT_tmpJnt' , 
						'kneeIkFRRGT_tmpJnt' ) )

	rigTools.nodeNaming( bLegRRig ,
			charName = charName ,
			elem = 'Font' ,
			side = 'RGT' )

	rigTools.dummyNaming( obj = bLegRRig.upLegRbn ,
			attr = 'rbn' ,
			dummy = 'upLegFontRbn' ,
			charName = charName ,
			elem = '' ,
			side = 'RGT' )

	rigTools.dummyNaming( obj = bLegRRig.midLegRbn ,
			attr = 'rbn' ,
			dummy = 'midLegFontRbn' ,
			charName = charName ,
			elem = '' ,
			side = 'RGT' )

	rigTools.dummyNaming( obj = bLegRRig.lowLegRbn ,
			attr = 'rbn' ,
			dummy = 'lowLegFontRbn' ,
			charName = charName ,
			elem = '' ,
			side = 'RGT' )

# Back leg left
	bLegLRig = pbleg.BackLegRig( parent = hipLRig.clav2_jnt ,
			side = 'LFT' ,
			animGrp = anim ,
			jntGrp = jnt ,
			ikhGrp = ikh ,
			skinGrp = skin ,
			stillGrp = still ,
			ribbon = True ,
			charSize = size ,
			tmpJnt = ( 	'upLegBKLFT_tmpJnt' , 
						'lowLegBKLFT_tmpJnt' , 
						'kneeBKLFT_tmpJnt' ,
						'ankleBKLFT_tmpJnt' , 
						'ballBKLFT_tmpJnt' ,
				  		'toeBKLFT_tmpJnt' ,
				  		'heelBKLFT_tmpJnt' ,
				   		'footInBKLFT_tmpJnt' ,
				    	'footOutBKLFT_tmpJnt' , 
				   		'kneeIkBKLFT_tmpJnt' ) )

	rigTools.nodeNaming( bLegLRig ,
			charName = charName ,
			elem = 'Back' ,
			side = 'LFT' ) 

	rigTools.dummyNaming( obj = bLegLRig.upLegRbn ,
			attr = 'rbn' ,
			dummy = 'upLegBackRbn' ,
			charName = charName ,
			elem = '' ,
			side = 'LFT' )

	rigTools.dummyNaming( obj = bLegLRig.midLegRbn ,
			attr = 'rbn' ,
			dummy = 'midLegBackRbn' ,
			charName = charName ,
			elem = '' ,
			side = 'LFT' )

	rigTools.dummyNaming( obj = bLegLRig.lowLegRbn ,
			attr = 'rbn' ,
			dummy = 'lowLegBackRbn' ,
			charName = charName ,
			elem = '' ,
			side = 'LFT' )

# Back leg right
	bLegRRig = pbleg.BackLegRig( parent = hipRRig.clav2_jnt ,
			side = 'RGT' ,
			animGrp = anim ,
			jntGrp = jnt ,
			ikhGrp = ikh ,
			skinGrp = skin ,
			stillGrp = still ,
			ribbon = True ,
			charSize = size ,
			tmpJnt = ( 	'upLegBKRGT_tmpJnt' , 
						'lowLegBKRGT_tmpJnt' , 
						'kneeBKRGT_tmpJnt' , 
						'ankleBKRGT_tmpJnt' , 
						'ballBKRGT_tmpJnt' , 
						'toeBKRGT_tmpJnt' , 
						'heelBKRGT_tmpJnt' , 
						'footInBKRGT_tmpJnt' , 
						'footOutBKRGT_tmpJnt' , 
						'kneeIkBKRGT_tmpJnt' ) )

	rigTools.nodeNaming( bLegRRig ,
			charName = charName ,
			elem = 'Back' ,
			side = 'RGT' )

	rigTools.dummyNaming( obj = bLegRRig.upLegRbn ,
			attr = 'rbn' ,
			dummy = 'upLegBackRbn' ,
			charName = charName ,
			elem = '' ,
			side = 'RGT' )

	rigTools.dummyNaming( obj = bLegRRig.midLegRbn ,
			attr = 'rbn' ,
			dummy = 'midLegBackRbn' ,
			charName = charName ,
			elem = '' ,
			side = 'RGT' )

	rigTools.dummyNaming( obj = bLegRRig.lowLegRbn ,
			attr = 'rbn' ,
			dummy = 'lowLegBackRbn' ,
			charName = charName ,
			elem = '' ,
			side = 'RGT' )

#	if placement.exists :
#	mc.delete( placement )
