import maya.cmds as mc
from utaTools.utapy import utaCore
reload(utaCore)
import pymel.core as pm
##-------------------------
# from utaTools.utapy import clavMuscleRig
# reload(clavMuscleRig)
# clavMuscleRig.clavMuscleRig(parent    = 'clav_L_ctrl' ,
# 						upArmParentGrp 	= [	'armctrlIkRoot_L_Grp', 
# 											'upArmctrlFk_L_Grp'],
# 						spineJnt 		= 'spine5_jnt',
# 						upArmJnt 		= 'upArm_L_jnt',
# 						neckJnt 		= 'neck1_jnt',
# 						clavMusJntGrp  = 'clavMusJnt_L_Grp',
# 						elem 			= 'clavMus',
# 						side 			= 'L',
# 						ctrlGrp      	= '' ,
# 						skinGrp      	= '' ,
# 						jntGrp       	= '' ,
# 						stillGrp 		= '' )
##-------------------------

class ClavisMuscle( object ):
    def __init__( self ):
		self.parent       	= 'clav_L_ctrl' 
		self.upArmParentGrp = [	'armctrlIkRoot_L_Grp'
								'upArmctrlFk_L_Grp']
		self.spineJnt 		= 'spine5_jnt'
		self.upArmJnt 		= 'upArm_L_jnt'
		self.neckJnt 		= 'neck1_jnt'
		self.clavMusJntGrp  = 'clavMusJnt_L_Grp'
		self.elem 			= 'clavMus'
		self.side 			= 'L'
		self.ctrlGrp      	= '' 
		self.skinGrp      	= '' 
		self.jntGrp       	= '' 
		self.stillGrp 		= '' 

	def clavMuscleRig(	parent       	= 'clav_L_ctrl' ,
						upArmParentGrp 	= [	'armctrlIkRoot_L_Grp', 
											'upArmctrlFk_L_Grp'],
						spineJnt 		= 'spine5_jnt',
						upArmJnt 		= 'upArm_L_jnt',
						neckJnt 		= 'neck1_jnt',
						clavMusJntGrp  = 'clavMusJnt_L_Grp',
						elem 			= 'clavMus',
						side 			= 'L',
						ctrlGrp      	= '' ,
						skinGrp      	= '' ,
						jntGrp       	= '' ,
						stillGrp 		= '' , *args):
		## Name
		if 'L' in side:
			side = '_L_'
		elif 'R' in side:
			side = '_R_'
		else:
			side = '_'

		## ControlAll
		controlList = []

		## Group
		clavMusSkin= mc.group(n = '{}Skin{}Grp'.format(elem, side), em = True)
		clavMusCtrl = mc.group(n = '{}Ctrl{}Grp'.format(elem, side), em = True)
		clavMusStill = mc.group(n = '{}Still{}Grp'.format(elem, side), em = True)

		## Unlocak Ik and Fk Control Group
		utaCore.lockAttrObj(listObj = upArmParentGrp, lock = False,keyable = True)

		## Parent Constraint
		utaCore.parentScaleConstraintLoop(  obj = spineJnt, lists = [clavMusJntGrp],par = True, sca = False, ori = False, poi = False)
		utaCore.parentScaleConstraintLoop(  obj = 'armPoint1Trash_L_Jnt', lists = upArmParentGrp, par = True, sca = False, ori = False, poi = False)
		utaCore.parentScaleConstraintLoop(  obj = spineJnt, lists = ['pacBindJnt_L_Grp'],par = True, sca = False, ori = False, poi = False)


		## Create Control
		pec1BineJnt = 'pec1Bind_L_Jnt'
		mc.select(pec1BineJnt)
		pec1BineGrpCtrlName, pec1BineGrpCtrlOfsName, pec1BineGrpCtrlParsName, pec1BineCtrlName, pec1BineGmblCtrlName = utaCore.createControlCurve(curveCtrl = 'circle', parentCon = True, connections = False, geoVis = False)
		controlList.append(pec1BineGrpCtrlName)

		## Pioint Constraint 'y', 'z'
		mc.pointConstraint('clavMus2Trash_L_Jnt', spineJnt, pec1BineGrpCtrlOfsName, mo = True, skip = 'x')
		mc.setAttr ("{}_pointConstraint1.{}W1".format(pec1BineGrpCtrlOfsName, spineJnt), 0.5)
		mc.setAttr ("{}_pointConstraint1.clavMus2Trash_L_JntW0".format(pec1BineGrpCtrlOfsName), 0.5)

		## Pioint Constraint 'x' (old skip = ['y', 'z'])
		mc.pointConstraint('clavMus2Trash_L_Jnt', spineJnt, 'lat1Bind_L_Jnt', mo = True, skip = ['y'])
		mc.setAttr ("lat1Bind_L_Jnt_pointConstraint1.{}W1".format(spineJnt), 0.5)
		mc.setAttr ("lat1Bind_L_Jnt_pointConstraint1.clavMus2Trash_L_JntW0", 0.5)

		## Generate ikhandle
		clavIkh = '{}{}'.format(elem, side)
		clavIkh = utaCore.createIkh(strJnt = 'clavMus1Trash_L_Jnt', endJnt = 'clavMus2Trash_L_Jnt'  ,name = clavIkh, sol = 'ikSCsolver')
		print clavIkh, '...clavIkh'
	 	## Generate control for upArm
	 	clavUpArm = 'clavUpArm_L_jnt'
	 	mc.delete(mc.parentConstraint('clavMus2Trash_L_Jnt', mc.createNode('joint', n = clavUpArm), mo = False))
		mc.makeIdentity (clavUpArm , apply = False , jointOrient = True, normal = 1, translate =False, rotate = True, scale = False) 

		## Generate Control
		mc.select(clavUpArm)
		clavUpArmGrpCtrlName, clavUpArmGrpCtrlOfsName, clavUpArmGrpCtrlParsName, clavUpArmCtrlName, clavUpArmGmblCtrlName = utaCore.createControlCurve(curveCtrl = 'plus', parentCon = False, connections = False, geoVis = False)
		controlList.append(clavUpArmGrpCtrlName)
		mc.delete(clavUpArm)
		print clavUpArmCtrlName, '...clavUpArmCtrlName'
		print clavUpArmGmblCtrlName, ',,clavUpArmGmblCtrlName'
		mc.parent(clavIkh , clavUpArmGmblCtrlName)

		## ParentConstraint clav to upArm Control
		utaCore.parentScaleConstraintLoop(  obj = parent, lists = [clavUpArmGrpCtrlName],par = True, sca = True, ori = False, poi = False)

		## LocakAttr
		utaCore.lockAttr(listObj= [clavUpArmCtrlName],attrs = ( 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v'), lock = True, keyable = False)

		## Generate locator
		spineLoc = mc.spaceLocator(n = 'shldrWuo_L_loc')[0]
		mc.delete(mc.parentConstraint(spineJnt, spineLoc, mo = False))
		utaCore.parentScaleConstraintLoop(  obj = spineJnt, lists = [spineLoc],par = True, sca = False, ori = False, poi = False)

		## Create Control
		trap1BindJnt = 'trap1Bind_L_Jnt'
		mc.select(trap1BindJnt)
		trap1BindGrpCtrlName, trap1BindGrpCtrlOfsName, trap1BindGrpCtrlParsName, trap1BindCtrlName, trap1BindGmblCtrlName = utaCore.createControlCurve(curveCtrl = 'circle', parentCon = True, connections = False, geoVis = False)
		utaCore.parentScaleConstraintLoop(  obj = 'armPoint1Trash_L_Jnt', lists = [trap1BindGrpCtrlName],par = True, sca = False, ori = False, poi = False)
		controlList.append(trap1BindGrpCtrlName)

		## Generate spineLoc aimConstraint
		aimCon = mc.aimConstraint(neckJnt, trap1BindGrpCtrlOfsName, aim=(1, 0, 0), u=(0, 1, 0), wut="objectrotation", wu=(0, 1, 0), wuo = spineLoc, mo=True)[0]
		aimCon = mc.aimConstraint(neckJnt, 'scapRot1Trash_L_Jnt', aim=(1, 0, 0), u=(0, 1, 0), wut="objectrotation", wu=(0, 1, 0), wuo = spineLoc, mo=True)[0]





		# ## Create Control
		scap1BindJnt = 'scap1Bind_L_jnt'
		# mc.select(scap1BindJnt)
		# grpCtrlName, grpCtrlOfsName, grpCtrlParsName, ctrlName, GmblCtrlName = utaCore.createControlCurve(curveCtrl = 'circle', parentCon = True, connections = False, geoVis = False)
		# utaCore.parentScaleConstraintLoop(  obj = parent, lists = [grpCtrlName],par = True, sca = False, ori = False, poi = False)
		# controlList.append(grpCtrlName)

		## Generate spineLoc aimConstraint
		aimCon = mc.aimConstraint(spineJnt, scap1BindJnt, aim=(1, 0, 0), u=(0, 1, 0), wut="objectrotation", wu=(0, 1, 0), wuo = spineLoc, mo=True)[0]


		## trap1BindBck
		trapBindBckJnt = ['trap1BindBck_L_Jnt', 'trap2BindBck_L_Jnt']
		mc.pointConstraint(scap1BindJnt, spineLoc, trapBindBckJnt[0], mo = True)
		mc.setAttr ("{}_pointConstraint1.{}W1".format(trapBindBckJnt[0], spineLoc), 0.5)
		mc.setAttr ("{}_pointConstraint1.{}W0".format(trapBindBckJnt[0], scap1BindJnt), 0.5)


		## Generate armRootAdj
		upArmJntBind = 'uparmRootAdjBind_L_jnt'
		mc.delete(mc.parentConstraint(upArmJnt, upArmJntBind, mo = False))
		mc.makeIdentity (upArmJntBind , apply = True , jointOrient = False, normal = 1, translate =False, rotate = True, scale = False) 
		mc.parent(upArmJntBind, upArmJnt)
		upArmRoojAdjMdv = mc.createNode('multiplyDivide', n = 'upArmRootAdj_l_Mdv')
		mc.connectAttr('{}.ry'.format(upArmJnt), '{}.input1.input1Y'.format(upArmRoojAdjMdv))
		mc.connectAttr('{}.rz'.format(upArmJnt), '{}.input1.input1Z'.format(upArmRoojAdjMdv))
		mc.connectAttr('{}.outputY'.format(upArmRoojAdjMdv), '{}.ry'.format(upArmJntBind))
		mc.connectAttr('{}.outputZ'.format(upArmRoojAdjMdv), '{}.rz'.format(upArmJntBind))
		utaCore.addAttrCtrl (ctrl = '{}Shape'.format(clavUpArmCtrlName), elem = ['upArmRoorAdj_Amp'], min = -100, max = 100, at = 'float', dv = -1)
		mc.connectAttr('{}Shape.upArmRoorAdj_Amp'.format(clavUpArmCtrlName), '{}.input2Y'.format(upArmRoojAdjMdv))
		mc.connectAttr('{}Shape.upArmRoorAdj_Amp'.format(clavUpArmCtrlName), '{}.input2Z'.format(upArmRoojAdjMdv))

		## Generate Setrange node for clav front to back
		generateSetRange(	ctrl = clavUpArmCtrlName,
							elem = 'scap',
							driver = 'clavMus1Trash_L_Jnt', 
							driverAxis = 'rotateY', 
							driven = scap1BindJnt,
							drivenAxis = 'translateX')

		## Wrap Group
		mc.parent(spineLoc, clavMusStill)
		mc.parent(controlList , clavMusCtrl)
		if mc.objExists(clavMusJntGrp):
			mc.parent(clavMusJntGrp, clavMusSkin)

		if mc.objExists(skinGrp):
			mc.parent(clavMusSkin, skinGrp)
		if mc.objExists(ctrlGrp):
			mc.parent(clavMusCtrl, ctrlGrp)
		if mc.objExists(stillGrp):
			mc.parent(clavMusStill, stillGrp)

	def generateSetRange(	ctrl = '',
							elem = '',
							driver = 'clavMus1Trash_L_Jnt', 
							driverAxis = 'rotateY', 
							driven = 'scap1Bind_L_jnt',
							drivenAxis = 'translateX',*args):
		## Split Name
		name, side, lastName = utaCore.splitName(sels = [ctrl])

		## Add Attribute shape
		attrAmp = ['minX', 'minY', 'minZ', 'maxX', 'maxY', 'maxZ', 'oldMinX', 'oldMinY', 'oldMinZ', 'oldMaxX', 'oldMaxY', 'oldMaxZ']
		utaCore.attrNameTitle(obj = '{}Shape'.format(ctrl) , ln = '{}{}{}Fun'.format(name, elem.capitalize(), side))
		utaCore.addAttrCtrl (ctrl = '{}Shape'.format(ctrl), elem = attrAmp, min = -200, max = 200, at = 'float',dv = 0)

		## Generate Setrange node for clav front to back
		clavMusSrg = mc.createNode('setRange', n = '{}Mus{}Srg'.format(name, side))
		clavMusCon = mc.createNode('condition', n = '{}Mus{}con'.format(name, side))

		## SetAttribute Default
		mc.setAttr ("{}.operation".format(clavMusCon), 2)
		mc.setAttr ("{}Shape.oldMinX".format(ctrl), 5)
		mc.setAttr ("{}Shape.oldMaxX".format(ctrl), 50)
		mc.setAttr ("{}Shape.oldMinZ".format(ctrl), -50)

		drivenValue = mc.getAttr('{}.tx'.format(driven))
		mc.setAttr ("{}Shape.minX".format(ctrl), drivenValue)
		mc.setAttr ("{}Shape.minZ".format(ctrl), -1)
		mc.setAttr ("{}Shape.maxX".format(ctrl), -0.4)
		mc.setAttr ("{}Shape.maxZ".format(ctrl), drivenValue)

		## Connect Attribute
		for each in attrAmp:
			mc.connectAttr('{}Shape.{}'.format(ctrl, each), '{}.{}'.format(clavMusSrg, each))

			## Lock Attribute minX, maxZ
			if 'minX' in each or 'maxZ' in each:
				utaCore.lockAttr(listObj = ['{}Shape'.format(ctrl)], attrs = [each],lock = True, keyable = True)


		mc.connectAttr ("{}.outValueX".format(clavMusSrg), "{}.colorIfTrueR".format(clavMusCon))
		mc.connectAttr ("{}.outValueZ".format(clavMusSrg), "{}.colorIfFalseR".format(clavMusCon))
		mc.connectAttr ('{}.{}'.format(driver, driverAxis), "{}.firstTerm".format(clavMusCon))
		mc.connectAttr ('{}.{}'.format(driver, driverAxis), '{}.valueX'.format(clavMusSrg))
		mc.connectAttr ('{}.{}'.format(driver, driverAxis), '{}.valueZ'.format(clavMusSrg))

		mc.connectAttr ("{}.outColorR".format(clavMusCon), "{}.{}".format(driven, drivenAxis))
		

		attr = ['minX', 'minY', 'minZ', 'maxX', 'maxY', 'maxZ', 'oldMinX', 'oldMinY', 'oldMinZ', 'oldMaxX', 'oldMaxY', 'oldMaxZ']