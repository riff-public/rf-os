import maya.cmds as mc
from utaTools.utapy import utaCore
reload(utaCore)

def openUI(*args):
	windowID = 'NonRollRig_UI_V2'
	if mc.window(windowID, exists=True):
		mc.deleteUI(windowID)

	window = mc.window( windowID  , title='Controler at Grid [4-12-20]', iconName='ANIM', wh = (200, 320) , s=False , mxb = False , mnb = False )

	mc.columnLayout( 'gridControllerRigUILayout' , cal='center', cat=['both', 18] )
	mc.separator(st='none', h=5)
	mc.rowLayout(nc=10, w=180)
	mc.text('    # Select Group or Object  ', al = 'center')

	mc.setParent('..')

	mc.separator(st='none', h=5)
	mc.separator(st='in', w=160)
	mc.separator(st='none', h=5)

	mc.setParent('..')

	mc.rowLayout(nc=10, w=180)
	mc.radioCollection('gridController_pivotColloection')
	mc.text('Grid : ')
	mc.radioButton('gridController_gridRadioButton', l='')
	mc.separator(st='none', w=10)
	mc.text('Mid : ')
	mc.radioButton('gridController_midRadioButton', l='')
	mc.separator(st='none', w=10)
	mc.text('BB : ')
	mc.radioButton('gridController_bbRadioButton', l='')
	mc.radioCollection('gridController_pivotColloection', e=True, sl='gridController_bbRadioButton')

	mc.setParent('..')

	mc.separator(st='none', h=10, w=160)

	mc.rowLayout(nc=10, w=180)
	mc.radioCollection('gridController_WoObColloection')
	mc.text('World : ')
	mc.radioButton('gridController_worldRadioButton', l='')
	mc.separator(st='none', w=10)
	mc.text('Object : ')
	mc.radioButton('gridController_objectRadioButton', l='')
	mc.radioCollection('gridController_WoObColloection', e=True, sl='gridController_objectRadioButton')

	mc.setParent('..')

	mc.separator(st='in', h=20, w=160)

	mc.rowLayout(nc=3, w=180)
	mc.checkBox('gridController_fkCheckBox', l='')
	mc.separator(st='none', w=10)
	mc.text('Fk :')
	mc.setParent('..')

	mc.separator(st='none', h=5)
	mc.separator(st='in', w=160)
	mc.separator(st='none', h=5)

	mc.columnLayout(w=180)
	mc.rowLayout(nc=4, w=180)
	mc.radioCollection('gridController_horiVertiColloection')
	mc.radioButton('gridController_horiRadioButton', l='Horizontal', w=80)
	mc.separator(st="shelf", hr=False, w=10, h=20)
	mc.radioButton('gridController_vertiRadioButton', l='Vertical', w=80)
	mc.radioCollection('gridController_horiVertiColloection', e=True, sl='gridController_horiRadioButton')

	mc.setParent('..')

	mc.rowLayout(nc=4, w=180)
	mc.radioCollection('gridController_lrColloection')
	mc.radioButton('gridController_lRadioButton', l='L', w=39)
	mc.radioButton('gridController_rRadioButton', l='R', w=39)
	mc.radioCollection('gridController_lrColloection', e=True, sl='gridController_lRadioButton')
	mc.separator(st="shelf", hr=False, w=10, h=20)

	mc.setParent('..')

	mc.separator(st='none', h=5)
	mc.separator(st='in', w=160)
	mc.separator(st='none', h=5)

	mc.button('gridController_runButton', l='Run', w=160, h=50, c=runController)

	mc.separator(st='in', h = 10, w=160)
	mc.text('Fix Pivot :')
	mc.separator(st='in', h = 10, w=160)

	# mc.columnLayout(w=180)
	mc.rowLayout(nc=4, w=160)
	mc.button('gridController_fixWorldAxisButton', l='World', w=75, h=30, c = fixAxisControlWorld)
	mc.separator(st="shelf", hr=False, w=10, h=40)
	mc.button('gridController_fixObjectAxisButton', l='Object', w=75, h=30, c = fixAxisControlObject)
	mc.setParent('..')
	mc.separator(st='in', h = 10, w=160)

	mc.showWindow(windowID)

def runController(*args):
	isGrid = mc.radioButton('gridController_gridRadioButton', q=True, sl=True)
	isMid = mc.radioButton('gridController_midRadioButton', q=True, sl=True)
	isBb = mc.radioButton('gridController_bbRadioButton', q=True, sl=True)

	if isGrid:
		pv = True
	elif isMid or isBb: 
		pv = False
		
	isWorld = mc.radioButton('gridController_worldRadioButton', q=True, sl=True)
	isObject = mc.radioButton('gridController_objectRadioButton', q=True, sl=True)
	if isWorld:
		woOb = True
	elif isObject: woOb = False

	isFk = mc.checkBox('gridController_fkCheckBox', q=True, v=True)

	isHori = mc.radioButton('gridController_horiRadioButton', q=True, sl=True)
	isVerti = mc.radioButton('gridController_vertiRadioButton', q=True, sl=True)
	if isHori == True:
		  hv = True
	else: hv = False

	isL = mc.radioButton('gridController_lRadioButton', q=True, sl=True)
	isR = mc.radioButton('gridController_rRadioButton', q=True, sl=True)
	if isL == True:
		  lr = True
	else: lr = False

	utaCore.generateControlFollowAxis(grid = pv, bbox = isBb, fk = isFk, horiOrVertical = hv, horiLFTorRGT = lr, world = woOb)

def fixAxisControlWorld(*args):
	utaCore.fixAxisControl(world = True)

def fixAxisControlObject(*args):
	utaCore.fixAxisControl(world = False)