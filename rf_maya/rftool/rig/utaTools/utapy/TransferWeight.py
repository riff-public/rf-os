import maya.cmds as mc
#--------------------------------------------
# from utaTools.utapy import TransferWeight
# reload(TransferWeight)
# TransferWeight.doTransferWeights(geo = ['pSphere1'], a = ['a_jnt', 'b_jnt'], b = ['aa_jnt', 'bb_jnt'])
#--------------------------------------------

def doTransferWeights(geo = [], a = [], b = [], *args):
    allSource = a
    allTarget = b

    if len(allSource)==len(allTarget):
        # print 'yes ysyes'

        jntDict = {}

        for i in range(len(allSource)):
            jntDict[allSource[i]] = allTarget[i]
            
        if not geo:
            geo=mc.ls(sl=True)

        for each in geo:
            sp = mc.listRelatives(each , c=True , type='mesh')[0]
            
            Conlist = []
            inputList = mc.listHistory(each , pdo =True) 
            for i in inputList:
                subj = mc.objectType(i)
                if subj == 'skinCluster':
                    Conlist.append(i)
                else : pass

            infs = mc.skinCluster(sp,inf=True ,q=True)

            for a in infs:

                if not a in allSource:
                    return

                # print jntDict[a] , infs[a]
                if jntDict[a] == a:
                    pass
                else:
                    mc.skinCluster(Conlist[0] , ai = jntDict[a] , lw=True , edit=True , wt=False)


            jntList = mc.listConnections( '{}.influenceColor'.format(Conlist[0]), d=False, s=True )
        
            for e in jntList:
                mc.setAttr('{}.liw'.format(e) , 1)


            for i in range(len(infs)):
                mc.setAttr('{}.liw'.format(infs[i]) , 0)
                mc.setAttr('{}.liw'.format(jntDict[infs[i]]) , 0)

                mc.skinPercent( '{}'.format(Conlist[0]), '{}.vtx[:]'.format(each), transformValue=[('{}'.format(jntDict[infs[i]]), 1)])

                mc.skinCluster(Conlist[0] , inf=infs[i] , e=True , lw=True)
                mc.skinCluster(Conlist[0] , inf=jntDict[infs[i]] , e=True , lw=True)

    else:
        print 'nononononono'