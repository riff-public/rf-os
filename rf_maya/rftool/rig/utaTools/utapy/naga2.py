import maya.cmds as mc

import sys
sys.path.append(r'D:\tools')

from lpRig import rigTools as lrr
from lpRig import core as lrc

from lpRig import rigData
reload(rigData)

from lpRig import mainGroup
reload(mainGroup)
from lpRig import rootRig
reload(rootRig)
from lpRig import torsoRig
reload(torsoRig)
from lpRig import neckRig
reload(neckRig)
from lpRig import headRig
reload(headRig)
from lpRig import clavRig
reload(clavRig)
from lpRig import armRig
reload(armRig)
from lpRig import thumbRig
reload(thumbRig)
from lpRig import fingerRig
reload(fingerRig)
from lpRig import legRig
reload(legRig)
from lpRig import aimRig
reload(aimRig)
from lpRig import fkRig
reload(fkRig)
from lpRig import pointRig
reload(pointRig)
from lpRig import nonRollRig
reload(nonRollRig)
from lpRig import ribbon
reload (ribbon)
import pkmel.ctrlShapeTools as ctrlShapeTools
reload(ctrlShapeTools)
import setAttrCharacter as sac 
reload(sac)

def main():
	print 'Character Name'
	elem = 'naga'

	print 'Creating Main Groups'
	mgObj = mainGroup.MainGroup(elem)

	print 'Creating Root Rig'
	rootObj = rootRig.RootRig(
			ctrlGrp=mgObj.Ctrl_Grp,
			skinGrp=mgObj.Skin_Grp,
			tmpJnt='Root_Jnt',
			part=''
			)

	print 'Crete Neck Fk'
	neckRigObj = fkRig.FkRig(
			parent=rootObj.Root_Jnt, 
			ctrlGrp=mgObj.Ctrl_Grp, 
			tmpJnt=['Neck1_Jnt', 'Neck2_Jnt', 'Neck3_Jnt', 'Neck4_Jnt', 'Neck5_Jnt', 'NeckTip_Jnt'],
			part='Neck', 
			side='', 
			ax='y', 
			shape='circle')

	print 'Creating Head Rig'
	headObj = headRig.HeadRig(
	            parent=neckObj.NeckTip_Jnt,
	            ctrlGrp=mgObj.Ctrl_Grp,
	            tmpJnt=[
	                        'Head_Jnt',
	                        'HeadTip_Jnt',
	                        'Eye_L_Jnt',
	                        'Eye_R_Jnt',
	                        '',
	                        '',
	                        '',
	                        'EyeTar_Jnt',
	                        'EyeTar_L_Jnt',
	                        'EyeTar_R_Jnt'
	                    ],
	            part=''
	        )


	print 'Joint Guide'
	fkJnt=[
			'TailFk1_Jnt','TailFk2_Jnt','TailFk3_Jnt','TailFk4_Jnt','TailFk5_Jnt','TailFk6_Jnt','TailFk7_Jnt','TailFk8_Jnt','TailFk9_Jnt','TailFk10_Jnt',
			'TailFk11_Jnt','TailFk12_Jnt','TailFk13_Jnt','TailFk14_Jnt','TailFk15_Jnt','TailFk16_Jnt','TailFk17_Jnt','TailFk18_Jnt',
			'TailFk19_Jnt','TailFk20_Jnt','TailFk21_Jnt','TailFk22_Jnt','TailFk23_Jnt','TailFk24_Jnt','TailFk25_Jnt','TailFk26_Jnt'
			]
	IkJnt=[
			'TailIk1_Jnt','TailIk2_Jnt','TailIk3_Jnt','TailIk4_Jnt','TailIk5_Jnt','TailIk6_Jnt','TailIk7_Jnt','TailIk8_Jnt','TailIk9_Jnt','TailIk10_Jnt',
			'TailIk11_Jnt','TailIk12_Jnt','TailIk13_Jnt','TailIk14_Jnt','TailIk15_Jnt','TailIk16_Jnt','TailIk17_Jnt','TailIk18_Jnt','TailIk19_Jnt','TailIk20_Jnt',
			'TailIk21_Jnt','TailIk22_Jnt','TailIk23_Jnt','TailIk24_Jnt','TailIk25_Jnt','TailIk26_Jnt','TailIk27_Jnt','TailIk28_Jnt','TailIk29_Jnt','TailIk30_Jnt',
			'TailIk31_Jnt','TailIk32_Jnt','TailIk33_Jnt','TailIk34_Jnt','TailIk35_Jnt','TailIk36_Jnt','TailIk37_Jnt','TailIk38_Jnt','TailIk39_Jnt','TailIk40_Jnt',
			'TailIk41_Jnt','TailIk42_Jnt','TailIk43_Jnt','TailIk44_Jnt','TailIk45_Jnt','TailIk46_Jnt','TailIk47_Jnt','TailIk48_Jnt','TailIk49_Jnt','TailIk50_Jnt',
			'TailIk51_Jnt','TailIk52_Jnt','TailIk53_Jnt','TailIk54_Jnt','TailIk55_Jnt','TailIk56_Jnt','TailIk57_Jnt','TailIk58_Jnt','TailIk59_Jnt','TailIk60_Jnt',
			'TailIk61_Jnt','TailIk62_Jnt','TailIk63_Jnt','TailIk64_Jnt','TailIk65_Jnt','TailIk66_Jnt','TailIk67_Jnt','TailIk68_Jnt','TailIk69_Jnt','TailIk70_Jnt',
			'TailIk71_Jnt','TailIk72_Jnt','TailIk73_Jnt','TailIk74_Jnt','TailIk75_Jnt','TailIk76_Jnt','TailIk77_Jnt','TailIk78_Jnt','TailIk79_Jnt','TailIk80_Jnt',
			'TailIk81_Jnt','TailIk82_Jnt','TailIk83_Jnt','TailIk84_Jnt','TailIk85_Jnt','TailIk86_Jnt','TailIk87_Jnt','TailIk88_Jnt','TailIk89_Jnt','TailIk90_Jnt',
			'TailIk91_Jnt','TailIk92_Jnt','TailIk93_Jnt','TailIk94_Jnt','TailIk95_Jnt','TailIk96_Jnt','TailIk97_Jnt','TailIk98_Jnt','TailIk99_Jnt','TailIk100_Jnt'		
			'TailIk101_Jnt'
			]
	crvGuide ='ikSpine_Crv'

	print 'Ik Spind Handle'
	strCurveIk = mc.ikHandle(sj=IkJnt[0], ee=IkJnt[-1], c=crvGuide, n= elem + '_ik', sol='ikSplineSolver', ccv=False, rootOnCurve=True, parentCurve=False)[0]

	print 'Bind Skin'
	mc.skinCluster(fkJnt, crvGuide, toSelectedBones = True , lockWeights = True, removeUnusedInfluence = False, nurbsSamples = 5)

	# print 'Create naga GroupStill'
	# rigGrp = mc.group(em=True, n = ('%sRig_Grp' % elem))
	# elemGrp = mc.group(em= True, n = ('%sStill_Grp' % elem))
	# mc.parent(elemGrp, mgObj.Still_Grp)
	# mc.parent(strCurveIk,crvGuide,elemGrp)
	# mc.parent(rigGrp, mgObj.Ctrl_Grp)
	# mc.parent('rootJntZro_grp',mgObj.Skin_Grp)

	# print 'Crete Control Fk'
	# nagaRigObj = fkRig.FkRig(
	# 		parent=rootObj.Root_Jnt, 
	# 		ctrlGrp=mgObj.Ctrl_Grp, 
	# 		tmpJnt=fkJnt,
	# 		part='naga', 
	# 		side='L', 
	# 		ax='y', 
	# 		shape='circle')


	# print 'Creating Neck Rig'
	# neckObj = neckRig.NeckRig(
	#             parent=rootObj.Root_Jnt,
	#             ctrlGrp=mgObj.Ctrl_Grp,
	#             skinGrp=mgObj.Skin_Grp,
	#             jntGrp=mgObj.Jnt_Grp,
	#             stillGrp=mgObj.Still_Grp,
	#             ax='y',
	#             tmpJnt=[
	#                     'Neck_Jnt',
	#                     'NeckTip_Jnt'
	#                     ],
	#             part='',
	#             headJnt='Head_Jnt'
	#         )

	# print 'Creating Head Rig'
	# headObj = headRig.HeadRig(
	#             parent=neckObj.NeckTip_Jnt,
	#             ctrlGrp=mgObj.Ctrl_Grp,
	#             tmpJnt=[
	#                         'Head_Jnt',
	#                         'HeadTip_Jnt',
	#                         'Eye_L_Jnt',
	#                         'Eye_R_Jnt',
	#                         '',
	#                         '',
	#                         '',
	#                         'EyeTar_Jnt',
	#                         'EyeTar_L_Jnt',
	#                         'EyeTar_R_Jnt'
	#                     ],
	#             part=''
	#         )

	try:
		ctrlShapeTools.readAllCtrl()
		mc.delete('TmpJnt_Grp')
		mc.delete('PivJnt_Grp')
	except : pass




























