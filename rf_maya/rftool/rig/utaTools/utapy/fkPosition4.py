import maya.cmds as mc

def fkPos(*args):
    lstFkJnt = mc.ls('TailFk*_Jnt')
    lstCtrlOfstZro = mc.ls('TailIk*CtrlOfstZro_Grp')
    noLstFkJnt = len(lstFkJnt)
    lstCtrlFkPos = []
    lstGrpFkPos = []
    lstCtrlOfstFkPos = []
    lstCondNd = []
    lstWorldGrp = []
    lstLocalGrp = []
    lstRevNd = []
    
    
    FkZGrp = mc.createNode('transform',n = 'FkPosCtrlZro_Grp')
    for i in range(noLstFkJnt+1):
        num = '%d' %(int(i))
        nameFkPos = 'TailFk'+str(num)
        if num == '0':pass
        else:
            ZGrp = mc.createNode('transform',n = nameFkPos+'PosCtrlZro_Grp')
            OfstGrp = mc.createNode('transform',n = nameFkPos+'PosOfstCtrl_Grp')
            LocalGrp = mc.createNode('transform',n = nameFkPos+'Local_Grp')
            WorldGrp = mc.createNode('transform',n = nameFkPos+'World_Grp')
            CbCrv = mc.curve(d = 1,n = nameFkPos+'Pos_Ctrl',p=[(0,0,0),(0,27.643159,0),(1.942594,28.029564,0),(3.589451,29.129957,0),(4.689845,30.776814,0),(5.076249,32.719408,0),(6.345636,32.719408,0),(5.076249,32.719408,0),(4.689845,34.662003,0),(3.589451,36.30886,0),(1.942594,37.409253,0),(0,37.795657,0),(-1.942594,37.409253,-6.94725e-007),(-3.589451,36.30886,-1.28369e-006),(-4.689845,34.662003,-1.67722e-006),(-5.076249,32.719408,-1.81541e-006),(-6.327855,32.719408,0),(-5.076249,32.719408,-1.81541e-006),(-4.689845,30.776814,0),(-3.589451,29.129957,-1.28369e-006),(-1.942594,28.029564,-6.94725e-007),(0,27.643159,0),(-2.8947e-007,28.029564,1.942594),(-5.34869e-007,29.129957,3.589451),(-6.98842e-007,30.776814,4.689845),(-7.56422e-007,32.719408,5.076249),(0,32.719408,6.345636),(-7.56422e-007,32.719408,5.076249),(-6.98842e-007,34.662003,4.689845),(-5.34869e-007,36.30886,3.589451),(-2.8947e-007,37.409253,1.942594),(0,37.795657,0),(0,39.065045,0),(0,37.795657,0),(9.84198e-007,37.409253,-1.942594),(1.81856e-006,36.30886,-3.589451),(2.37606e-006,34.662003,-4.68984),(2.57183e-006,32.719408,-5.076249),(0,32.719408,-6.345636),(0,32.719408,-5.076249),(2.37606e-006,30.776814,-4.68984),(1.81856e-006,29.129957,-3.589451),(9.84198e-007,28.029564,-1.942594),(0,27.643159,0),(-2.8947e-007,28.029564,1.942594),(-5.34869e-007,29.129957,3.589451),(-6.98842e-007,30.776814,4.689845),(-7.56422e-007,32.719408,5.076249),(-1.942599,32.719408,4.68984),(-3.589451,32.719408,3.589451),(-4.689845,32.719408,1.942594),(-5.076249,32.719408,-1.81541e-006),(-4.68984,32.719408,-1.942599),(-3.589446,32.719408,-3.589451),(-1.942594,32.719408,-4.689845),(2.57183e-006,32.719408,-5.076249),(1.942599,32.719408,-4.68984),(3.589451,32.719408,-3.589446),(4.689845,32.719408,-1.942594),(5.076249,32.719408,0),(4.689845,32.719408,1.942594),(3.589451,32.719408,3.589451),(1.942594,32.719408,4.689845),(-7.56422e-007,32.719408,5.076249)])

            #SHOW AND UNLOCK ATTRIBUTES
            mc.setAttr('%s.scaleX' % CbCrv, lock = True , k = False )
            mc.setAttr('%s.scaleY' % CbCrv, lock = True , k = False )
            mc.setAttr('%s.scaleZ' % CbCrv, lock = True , k = False )

            mc.addAttr(CbCrv,ln='localWorld',at='long',min = 0,max = 1,dv = 0,k=True)
            
            revNd = mc.createNode('reverse',n = nameFkPos+'_Rev')
            
            CbShape = mc.listRelatives( CbCrv , shapes = True )[0]
            mc.setAttr('%s.overrideEnabled'%CbShape,1)
            mc.setAttr('%s.overrideColor'%CbShape,16)
            

            
            mc.parent(CbCrv,OfstGrp)
            mc.parent(OfstGrp,LocalGrp,WorldGrp,ZGrp)
            mc.parent(ZGrp,FkZGrp)
            
            
            lstCtrlFkPos.append(CbCrv)
            lstCtrlOfstFkPos.append(OfstGrp)
            lstGrpFkPos.append(ZGrp)
            lstLocalGrp.append(LocalGrp)
            lstWorldGrp.append(WorldGrp)
            lstRevNd.append(revNd)
    
    ##Local World Oftion for FkPos
    for g in range(noLstFkJnt):
    
        mc.orientConstraint('Ctrl_Grp',lstWorldGrp[g],mo = True)
        mc.orientConstraint(lstLocalGrp[g],lstWorldGrp[g],lstCtrlOfstFkPos[g],mo = True)
        
        mc.connectAttr(lstCtrlFkPos[g]+'.localWorld',lstCtrlOfstFkPos[g]+'_orientConstraint1.'+lstWorldGrp[g]+'W1')
        mc.connectAttr(lstCtrlFkPos[g]+'.localWorld',lstRevNd[g]+'.inputX')
        mc.connectAttr(lstRevNd[g]+'.outputX',lstCtrlOfstFkPos[g]+'_orientConstraint1.'+lstLocalGrp[g]+'W0')
    
        
    
    for b in range(noLstFkJnt):
        snapCtrlFkPos = mc.delete(mc.pointConstraint(lstFkJnt[b],lstGrpFkPos[b]))
        
    for a in range(noLstFkJnt-1):
        mc.parentConstraint(lstCtrlFkPos[a+1],lstGrpFkPos[a],mo = True)
        mc.scaleConstraint(lstCtrlFkPos[a+1],lstGrpFkPos[a],mo = True)
        
            
    for c in range(noLstFkJnt-1):
        mc.parentConstraint(lstCtrlFkPos[c+1],lstCtrlOfstZro[c],mo = True)
        mc.scaleConstraint(lstCtrlFkPos[c+1],lstCtrlOfstZro[c],mo = True)
        
    mc.parentConstraint('TailFk1Pos_Ctrl','MoveCtrlOfstZro_Grp',mo = True)
    mc.scaleConstraint('TailFk1Pos_Ctrl','MoveCtrlOfstZro_Grp',mo = True)


    #Add Atrribute FkPosition
    mc.addAttr('OnOffSwitch1_Ctrl',ln='FkPos________',at='long',dv = 0,k=True)
    mc.setAttr('OnOffSwitch1_Ctrl.FkPos________',lock = 1)
    mc.addAttr('OnOffSwitch1_Ctrl',ln='FkPosition',at='long',min = 0,max = 25,dv = 0,k=True)
    
    for d in range(noLstFkJnt+1):
        num = '%d' %(int(d))
        nameFkPos2 = 'TailFk'+str(num)
        
        condNd = mc.createNode('condition',n=nameFkPos2+'_Cond')
        mc.setAttr(condNd+'.operation',3)
        mc.setAttr(condNd+'.secondTerm',d)
        mc.setAttr(condNd+'.colorIfTrueR',1)
        mc.setAttr(condNd+'.colorIfFalseR',0)
        lstCondNd.append(condNd)
        
        mc.connectAttr('OnOffSwitch1_Ctrl.FkPosition',condNd+'.firstTerm')
        
    # Connect Attr FkPos at MoveCtrlOftsZro_Grp
    mc.connectAttr('%s.outColorR' % lstCondNd[1], 'MoveCtrlOfstZro_Grp'+'_scaleConstraint1.TailFk1Pos_CtrlW0')
    mc.connectAttr('%s.outColorR' % lstCondNd[1], 'MoveCtrlOfstZro_Grp'+'_parentConstraint1.TailFk1Pos_CtrlW0')   

    for f in range(noLstFkJnt-1):
        mc.connectAttr(lstCondNd[f+2]+'.outColorR',lstCtrlOfstZro[f]+'_scaleConstraint1.'+lstCtrlFkPos[f+1]+'W0')
        mc.connectAttr(lstCondNd[f+2]+'.outColorR',lstCtrlOfstZro[f]+'_parentConstraint1.'+lstCtrlFkPos[f+1]+'W0')
        mc.connectAttr(lstCondNd[f+1]+'.outColorR',lstGrpFkPos[f]+'.visibility')
        
    mc.connectAttr('TailFk25_Cond.outColorR','TailFk25PosCtrlZro_Grp.visibility')
    mc.parent(FkZGrp,'Ctrl_Grp')


    
    print('====done====')