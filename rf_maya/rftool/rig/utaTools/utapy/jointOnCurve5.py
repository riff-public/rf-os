import maya.cmds as mc
import re
from utaTools.utapy import utaCore
reload(utaCore)main
#recuperation des curves et du nombre de joints voulut

def jointOnCurve(elem = ''):
    sel =mc.ls(sl=True)
    lists = [a.replace('_Crv', '') for a in sel]
    if lists:
        newName = lists[0]


    selsList = []  
    for sels in sel:
        selsList.append(sels)
    # grpJnt = mc.createNode('transform',n='%sJntZro_Grp' % elem)
    # print grpJnt
    if sel == []:
        # pass
        mc.confirmDialog(title='No selection', message='you must select curve(s)', button=['OK'], defaultButton='OK' )
        
    else:
        textName = ""
        chaine = ""
        expression = r"^[0-9]+$"
        while re.search(expression,chaine) is None:
            prompt = mc.promptDialog(
            title="Joint On Curve",
            message ="number of bones",
            button=['OK','Cancel'],
            defaultButton="OK",
            dismissString='Cancel')
            if prompt == "OK":
                textName = mc.promptDialog(query=True, text=True)
                chaine = mc.promptDialog(query=True, text=True)
                # pass
            elif prompt == "Cancel":
                chaine = "0"  
        if chaine == "0":
            print 'Operation Number'
        else:            
            chaine = int(chaine) - 1     
    # print chaine, '..numbr'
    #Creation des CVs dup 

    for elt in sel:
        MtJntGrp = mc.createNode('transform',n=('%s%sJntRig_Grp' % (newName,elem)))
        MtCtrlGrp = mc.createNode('transform',n=('%s%sCtrlRig_Grp' % (newName,elem)))
        MtStlGrp = mc.createNode('transform',n=('%s%sStillRig_Grp' % (newName,elem)))

        mc.hide(MtJntGrp)

        motionCrv = mc.rebuildCurve(elt, ch=False, rpo=True, rt=False, end=True, kr=False, kcp=False, kep=True, kt=True, s=chaine, d=3, tol=0.01)
        mc.delete(motionCrv, ch=1)


        DupCrv = mc.select(mc.duplicate(elt, n= elt+('%sUpTemp' % elem)))

        CrvUp = elt+('%sUpTemp' % elem)
        ##Function Delete Histrory
        # selection = mc.ls(sl = 1)
        # for obj in selection:
            # mc.delete(obj , ch = 1)
        mc.delete(CrvUp, ch=1)
        mc.parent(elt,CrvUp,MtStlGrp)

        ep = chaine+1
        i=0
        SK = []
        CL = []
        JIK = []

        while ep>0:
            mc.select(elt+".ep["+str(i)+"]")
            mc.cluster(name="CL_Temp_"+str(i))
            CL.append("CL_Temp_"+str(i)+"Handle")
            pos = mc.xform("CL_Temp_"+str(i)+"Handle", query=True, rp=True, ws=True)
            mc.select(clear=True)
            mc.joint(p=(pos[0],pos[1],pos[2]), name=elt+str(i+1)+'_Jnt')  
            SK.append(elt+str(i+1)) 
            JIK.append(elt+str(i+1)+'_Jnt')
            ep-=1
            i+=1   
        i-=1
        NoJIK = len(JIK)
        NoSK = len(SK)
        mc.delete(CL)
        for sm in range(NoJIK):
            mc.parent(JIK[sm],MtJntGrp)

        #Crete Grps and Ctrls for Crv##    
        MtCtrl = []    
        for a in range(NoSK):
            ZGrp = mc.createNode('transform',n=SK[a]+'CtrlZro_Grp')
            CirCrv = utaCore.curveCtrlShape(curveCtrl = 'ballPlus', elem  = elem)

            # CirCrv = mc.curve(d = 1,n = 'IkPath_Ctrl',p=[(-99.056548,0,0),(-88.41312,0,0),(-87.324554,0,-13.830848),(-84.08589,0,-27.321117),(-78.776635,0,-40.138682),(-71.527721,0,-51.967883),(-62.517537,0,-62.517537),(-51.967883,0,-71.527721),(-40.138682,0,-78.776635),(-27.321117,0,-84.08589),(-13.830848,0,-87.324554),(0,0,-88.41312),(0,0,-99.056548),(0,0,-88.41312),(13.830848,0,-87.324554),(27.321117,0,-84.08589),(40.138682,0,-78.776635),(51.967883,0,-71.527721),(62.517537,0,-62.517537),(71.527721,0,-51.967883),(78.776635,0,-40.138682),(84.08589,0,-27.321117),(87.324554,0,-13.830848),(88.41312,0,0),(99.056548,0,0),(88.41312,0,0),(87.324554,0,13.830848),(84.08589,0,27.321117),(78.776635,0,40.138682),(71.527721,0,51.967883),(62.517537,0,62.517537),(51.967883,0,71.527721),(40.138682,0,78.776635),(27.321117,0,84.08589),(13.830848,0,87.324554),(0,0,88.41312),(0,0,99.056548),(0,0,88.41312),(-13.830848,0,87.324554),(-27.321117,0,84.08589),(-40.138682,0,78.776635),(-51.967883,0,71.527721),(-62.517537,0,62.517537),(-71.527721,0,51.967883),(-78.776635,0,40.138682),(-84.08589,0,27.321117),(-87.324554,0,13.830848),(-88.41312,0,0)])
            CirCrv = mc.rename(CirCrv,SK[a]+'_Ctrl')
            CirShape = mc.listRelatives( CirCrv , shapes = True )[0]
            mc.setAttr('%s.overrideEnabled'%CirShape,1)
            mc.setAttr('%s.overrideColor'%CirShape,18)
            Crv = CirCrv
            
            mc.parent(Crv,ZGrp)
            
            mc.delete(mc.parentConstraint(JIK[a],ZGrp))
            mc.parentConstraint(Crv,JIK[a])

            MtCtrl.append(ZGrp)
    ## Check selected
    if not sel:
        return True
    ##Reverse Curve Direction##
    mc.reverseCurve(sel,ch =1,rpo=1)
    mc.reverseCurve(CrvUp,ch =1,rpo=1)

    for Mtc in range(NoJIK):
        mc.parent(MtCtrl[Mtc],MtCtrlGrp)

    ##Skin to Ctrl Crv Path##
    CtrlSkin2Crv = mc.skinCluster(sel, JIK, n='%s_Skin' % elem, tsb=True, bm=0, sm=0, nw=1)[0]


    # CtrlSkin2Crv = mc.skinCluster(sel, JIK, n='%s_Skin' % elem, tsb=True, bm=0, sm=0, nw=1)[0]
    # CtrlSkin2Crv = mc.skinCluster(CrvUp, JIK, n='%sUpTemp_Skin' % elem, tsb=True, bm=0, sm=0, nw=1)[0]

    # CtrlSkin2Crv = mc.skinCluster(sel,elt+('%sUpTemp' % elem), JIK, n='test', tsb=True, bm=0, sm=0, nw=1)[0]
        
    axis = ['X', 'Y', 'Z']
    attrs = ['translate', 'rotate', 'scale']


    for ax in axis:
        for attr in attrs:
            mc.setAttr(elt+('%sUpTemp' % elem)+"."+attr+ax, lock=0)
            
    mc.setAttr(elt+('%sUpTemp' % elem)+'.translateY', 100)

    for ax in axis:
        for attr in attrs:
            mc.setAttr(elt+('%sUpTemp' % elem)+"."+attr+ax, lock=1)
            
        mc.setAttr(elt+('%sUpTemp' % elem)+'.v',0)   
    mc.select(clear=True)
