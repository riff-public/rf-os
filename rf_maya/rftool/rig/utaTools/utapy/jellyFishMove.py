# import maya.cmds as mc
# import maya.mel as mm 
# from utaTools.utapy import utaCore
# reload(utaCore)
# from utaTools.pkmel import rigTools as rigTools
# reload( rigTools )

# #--------------------------------------------------------------------
# # import maya.cmds as mc
# # from utaTools.utapy import jellyFishMove
# # reload(jellyFishMove)
# # jellyFishMove.jellyFishMove(obj = 'jellyFishTest_Geo')
# #--------------------------------------------------------------------

# def jellyFishMove(obj = '',*args):
    
#     name, side, lastName = utaCore.splitName([obj])

#     ## Generate Group for geo
#     mc.select(obj)
#     objGrp = rigTools.UtaDoZeroGroup()[0]

#     ## Generate Control 
#     mc.select(objGrp)
#     grpCtrlName, grpCtrlOfsName, grpCtrlParsName, ctrlName, GmblCtrlName = utaCore.createControlCurve(curveCtrl = 'circle', parentCon = True, connections = False, geoVis = True)
#     mc.parentConstraint(GmblCtrlName, objGrp, mo = True)
#     mc.scaleConstraint(GmblCtrlName, objGrp, mo = True)

#     ## Generate add Attribute
#     attrs = ['strength', 'dynSpeed']
#     values = [1, 0.1]
#     ctrlMdv = mc.createNode('multiplyDivide', n = '{}{}Mdv'.format(name, side))
#     dynSpeedAttr = utaCore.addAttrCtrl (ctrl = ctrlName, elem = attrs, min = -100, max = 100, at = 'float',dv = 0)
#     print dynSpeedAttr, '...dynSpeedAttr..'
#     for i in range(len(dynSpeedAttr)):
#         mc.setAttr('{}.{}'.format(ctrlName, dynSpeedAttr[i]), values[i])
         
#     print dynSpeedAttr , '..dynSpeedAttr.'
#     mc.connectAttr('{}.ty'.format(ctrlName), '{}.input1X'.format(ctrlMdv))
#     mc.connectAttr('{}.{}'.format(ctrlName, dynSpeedAttr[0]), '{}.input1Y'.format(ctrlMdv))
#     mc.connectAttr('{}.{}'.format(ctrlName, dynSpeedAttr[1]),'{}.input2X'.format(ctrlMdv))
#     # mc.setAttr('{}.input2Y'.format(ctrlMdv), 0.1)
#     ctrlDyn = '{}.outputX'.format(ctrlMdv)

#     ## Generate lambert Node
#     lambertNode = applyMaterial(obj = obj,  node = 'lambert', elem = '', addShade = True)
#     ## Generate ramp
#     rampNode = applyMaterial(obj = obj,  node = 'ramp', elem = '', addShade = False)
#     mc.connectAttr ('{}.outColor'.format(rampNode), '{}.color'.format(lambertNode), f = True)

#     ## Generate set Panar Geo
#     mc.select('{}.vtx[0:1000000]'.format(obj),  d = True) ;
#     mc.select('{}.f[0:1000000]'.format(obj),  add = True) ;
#     mc.polyProjection('{}.f[0:1000000]'.format(obj), ch = True,  type =  'Planar',  ibd = True, md = 'x');
#     mc.select(cl = True)

#     ## Generate nCloth and nucleus
#     mc.select(obj)
#     # nclothNodeShape = mc.createNode('nCloth', n = '{}{}nClothShape'.format(name, side))
#     # nucleus = mc.createNode ("nucleus", n = '{}{}nucleus'.format(name, side))
#     # mc.connectAttr ('{}Shape.worldMesh[0]'.format(obj), '{}.inputMesh'.format(nclothNodeShape))
#     # mc.connectAttr ('{}.currentState'.format(nclothNodeShape), '{}.inputActive[0]'.format(nucleus))
#     # mc.connectAttr ('{}.startState'.format(nclothNodeShape), '{}.inputActiveStart[0]'.format(nucleus))
#     # mc.connectAttr ('{}.startFrame'.format(nucleus), '{}.startFrame'.format(nclothNodeShape))
#     # mc.connectAttr ('{}.outputObjects[0]'.format(nucleus), '{}.nextState'.format(nclothNodeShape))
#     # mc.connectAttr ('{}.outputMesh'.format(nclothNodeShape), '{}Shape1.inMesh'.format(obj), f = True)
#     # mc.connectAttr ('time1.outTime', '{}.currentTime'.format(nucleus))
#     # mc.connectAttr ('time1.outTime', '{}.currentTime'.format(nclothNodeShape))

#     mm.eval ('createNCloth 0;')
#     if mc.objExists('nCloth1'):
#         nclothNode = mc.rename('nCloth1', '{}{}nCloth'.format(name, side))
#     if mc.objExists('nucleus1'):
#         nucleusNode = mc.rename('nucleus1', '{}{}nucleus'.format(name, side))




#     ## Generate Tranform Constraint
#     mc.select(obj, r = True)
#     mc.select('{}.vtx[0:1000000]'.format(obj),  r = True) ;
#     # dynConNode = mc.createNode ("dynamicConstraint", n = '{}{}dynConShape'.format(name, side))
#     # mc.connectAttr('{}.evalStart[0]'.format(dynConNode), '{}.inputStart[0]'.format(nucleus))
#     # nComPoNode = mc.createNode ("nComponent", n = '{}{}nComponent'.format(name, side))
#     # mc.connectAttr('{}.outComponent'.format(nComPoNode),  '{}.componentIds[0]'.format(dynConNode))
#     # mc.connectAttr('{}.nucleusId'.format(nclothNodeShape), '{}.objectId'.format(nComPoNode))
#     # mc.connectAttr('{}.evalCurrent[0]'.format(dynConNode), '{}.inputCurrent[1]'.format(nucleus))
#     # mc.connectAttr('time1.outTime',  '{}.currentTime'.format(dynConNode))
#     dynConNode = mm.eval('createNConstraint transform 0;')[0]
#     nCompoName = 'nComponent1'
#     if mc.objExists(nCompoName):
#         nComPoNode = mc.rename(nCompoName, '{}{}nComPo'.format(name, side))
#     mm.eval('createNConstraint transform 0;')
#     dynConNode = mc.rename('dynamicConstraint1', '{}{}dynConNode'.format(name, side))
#     mc.connectAttr(ctrlDyn, '{}Shape.currentTime'.format(dynConNode), f = True)
#     mc.connectAttr('{}.outputY'.format(ctrlMdv), '{}.{}'.format(dynConNode, dynSpeedAttr[0]))

#     ## Generate Expression Translate Y
#     mc.expression( s='{}.translateY=abs(sin({}*2)*cos({}*2))*5+{}*6'.format(dynConNode, ctrlDyn, ctrlDyn, ctrlDyn))
#     # mc.setAttr('{}.strength'.format(dynConNode), 1)
#     mc.connectAttr('{}.message'.format(dynConNode), 'expression1.objectMsg')

#     ## Generate ramp2
#     rampStrengthMapNode = applyMaterial(obj = obj,  node = 'ramp', elem = 'StrMap', addShade = False)
#     mc.connectAttr ('{}.outAlpha'.format(rampStrengthMapNode),  '{}.strengthMap'.format(nComPoNode))

#     ## Generate Expression
#     mc.expression( s = '{}.colorEntryList[0].position=abs(sin({}*2)*cos({}*2))*0.9'.format(rampStrengthMapNode, ctrlDyn, ctrlDyn))
#     mc.setAttr("{}.colorEntryList[0].color".format(rampStrengthMapNode), 0, 0, 0)
#     mc.setAttr("{}.colorEntryList[1].color".format(rampStrengthMapNode), 1, 1, 1)
#     mc.setAttr("{}.colorEntryList[1].position".format(rampStrengthMapNode), 1)

#     ## Generate Expression
#     mc.expression( s = '{}.damp=abs(sin({}*2)*cos({}*2))*2'.format(dynConNode, ctrlDyn, ctrlDyn))
#     mc.setAttr("{}Shape.compressionResistance".format(nclothNode), 0)
#     mc.setAttr("{}Shape.bendResistance".format(nclothNode), 0)
#     mc.setAttr("{}Shape.stretchDamp".format(nclothNode), 4.8)

#     ## Generate Wrap Group
#     mc.parent(dynConNode, nucleusNode, objGrp)
#     mc.setAttr('{}.visibility'.format(dynConNode), 0)
#     mc.setAttr('{}.visibility'.format(nucleusNode), 0)

# def applyMaterial(obj, node = 'lambert', elem = '', addShade = True):

#     name, side, lastName = utaCore.splitName([obj])

#     if mc.objExists(obj):
#         shd = mc.shadingNode(node, name="{}{}{}{}".format(name, elem, side, node), asShader=True)
#         shdSG = mc.sets(name='{}{}SG'.format(name, side), empty=True, renderable=True, noSurfaceShader=True)
#         if addShade:
#             mc.connectAttr('{}.outColor'.format(shd), '{}.surfaceShader'.format(shdSG))
#             mc.sets(obj, e=True, forceElement=shdSG)
#     return shd







import maya.cmds as mc
import maya.mel as mm 
import math
from utaTools.utapy import utaCore
reload(utaCore)
from utaTools.pkmel import rigTools as rigTools
reload( rigTools )
from lpRig import rigTools as lrr
reload(lrr)
#--------------------------------------------------------------------
# import maya.cmds as mc
# from utaTools.utapy import jellyFishMove
# reload(jellyFishMove)
# jellyFishMove.jellyFishMove(obj = 'jellyFishTest_Geo')
#--------------------------------------------------------------------

def jellyFishMove2(obj = '',elem = '', *args):
    ## Generate Splite name
    name, side, lastName = utaCore.splitName([obj])
    objName = '{}{}Dyn{}Geo'.format(name, elem, side)
    objNewName = mc.rename(obj, objName)


    ## Generate Group for geo
    mc.select(objNewName)
    objGrp = rigTools.UtaDoZeroGroup()[0]


    ## Generate Control 
    mc.select(objGrp)
    grpCtrlName, grpCtrlOfsName, grpCtrlParsName, ctrlName, GmblCtrlName = utaCore.createControlCurve(curveCtrl = 'circle', parentCon = True, connections = False, geoVis = True)


    ## Generate add Attribute
    attrs = ['strength', 'ctrlSpeed', 'timeSpeed']
    timeAttrs = ['time']
    timeAttr = utaCore.addAttrCtrl (ctrl = ctrlName, elem = timeAttrs, min = 0, max = 1, at = 'long',dv = 1)
    dynSpeedAttr = utaCore.addAttrCtrl (ctrl = ctrlName, elem = attrs, min = -100, max = 100, at = 'float',dv = 0)

    ## Generate Node
    values = [0.5, 0.01, 0.02]
    ctrlMdv = mc.createNode('multiplyDivide', n = '{}{}Ctrl{}Mdv'.format(name, elem, side))
    switchBcl = mc.createNode('blendColors', n = '{}{}Switch{}Bcl'.format(name, elem, side))
    switchMdv = mc.createNode('multiplyDivide', n = '{}{}Switch{}Mdv'.format(name, elem, side))


    ## Generate Connect Node
    for i in range(len(dynSpeedAttr)):
        mc.setAttr('{}.{}'.format(ctrlName, dynSpeedAttr[i]), values[i])
        
    mc.connectAttr('{}.ty'.format(ctrlName), '{}.input1X'.format(ctrlMdv))
    # mc.connectAttr('{}.{}'.format(ctrlName, dynSpeedAttr[0]), '{}.input1Y'.format(ctrlMdv))
    mc.connectAttr('{}.{}'.format(ctrlName, dynSpeedAttr[1]),'{}.input2X'.format(ctrlMdv))
    mc.connectAttr('{}.{}'.format(ctrlName, timeAttr[0]), '{}.blender'.format(switchBcl))
    mc.connectAttr('time1.outTime', '{}.input1Y'.format(ctrlMdv))
    mc.connectAttr('{}.outputX'.format(ctrlMdv), '{}.color2R'.format(switchBcl))
    mc.connectAttr('{}.outputY'.format(ctrlMdv), '{}.color1R'.format(switchBcl))

    mc.connectAttr('{}.outputR'.format(switchBcl), '{}.input1X'.format(switchMdv))
    mc.connectAttr('{}.{}'.format(ctrlName, dynSpeedAttr[2]),'{}.input2Y'.format(ctrlMdv))

    ## Generate String for Expression
    ctrlDyn = '{}.outputX'.format(switchMdv)

    ## Generate lambert Node
    lambertNode = applyMaterial(obj = objNewName,  node = 'lambert', elem = elem, addShade = True)
    ## Generate ramp
    rampNode = applyMaterial(obj = objNewName,  node = 'ramp', elem = elem, addShade = False)
    mc.connectAttr ('{}.outColor'.format(rampNode), '{}.color'.format(lambertNode), f = True)

    ## Generate set Panar Geo
    mc.select('{}.vtx[0:1000000]'.format(objNewName),  d = True) ;
    mc.select('{}.f[0:1000000]'.format(objNewName),  add = True) ;
    mc.polyProjection('{}.f[0:1000000]'.format(objNewName), ch = True,  type =  'Planar',  ibd = True, md = 'x');
    mc.select(cl = True)
    print 'A'
    ## Generate nCloth and nucleus
    mc.select(objNewName)
    # nclothNodeShape = mc.createNode('nCloth', n = '{}{}nClothShape'.format(name, side))
    # nucleus = mc.createNode ("nucleus", n = '{}{}nucleus'.format(name, side))
    # mc.connectAttr ('{}Shape.worldMesh[0]'.format(objNewName), '{}.inputMesh'.format(nclothNodeShape))
    # mc.connectAttr ('{}.currentState'.format(nclothNodeShape), '{}.inputActive[0]'.format(nucleus))
    # mc.connectAttr ('{}.startState'.format(nclothNodeShape), '{}.inputActiveStart[0]'.format(nucleus))
    # mc.connectAttr ('{}.startFrame'.format(nucleus), '{}.startFrame'.format(nclothNodeShape))
    # mc.connectAttr ('{}.outputObjects[0]'.format(nucleus), '{}.nextState'.format(nclothNodeShape))
    # mc.connectAttr ('{}.outputMesh'.format(nclothNodeShape), '{}Shape1.inMesh'.format(objNewName), f = True)
    # mc.connectAttr ('time1.outTime', '{}.currentTime'.format(nucleus))
    # mc.connectAttr ('time1.outTime', '{}.currentTime'.format(nclothNodeShape))

    mm.eval ('createNCloth 0;')

    if mc.objExists('nCloth1'):
        nclothNode = mc.rename('nCloth1', '{}{}{}nCloth'.format(name, elem, side))
    if mc.objExists('nucleus1'):
        nucleusNode = mc.rename('nucleus1', '{}{}{}nucleus'.format(name, elem, side))
    print 'B'



    ## Generate Tranform Constraint
    mc.select(objNewName, r = True)
    mc.select('{}.vtx[0:100000000]'.format(objNewName),  r = True) ;
    # dynConNode = mc.createNode ("dynamicConstraint", n = '{}{}dynConShape'.format(name, side))
    # mc.connectAttr('{}.evalStart[0]'.format(dynConNode), '{}.inputStart[0]'.format(nucleus))
    # nComPoNode = mc.createNode ("nComponent", n = '{}{}nComponent'.format(name, side))
    # mc.connectAttr('{}.outComponent'.format(nComPoNode),  '{}.componentIds[0]'.format(dynConNode))
    # mc.connectAttr('{}.nucleusId'.format(nclothNodeShape), '{}.objectId'.format(nComPoNode))
    # mc.connectAttr('{}.evalCurrent[0]'.format(dynConNode), '{}.inputCurrent[1]'.format(nucleus))
    # mc.connectAttr('time1.outTime',  '{}.currentTime'.format(dynConNode))
    dynConNode = mm.eval('createNConstraint transform 0;')[0]


    nCompoName = 'nComponent1'
    if mc.objExists(nCompoName):
        nComPoNode = mc.rename(nCompoName, '{}{}{}nComPo'.format(name, elem, side))
    dynConNode = mc.rename('dynamicConstraint1', '{}{}{}dynConNode'.format(name, elem, side))
    mc.connectAttr(ctrlDyn, '{}Shape.currentTime'.format(dynConNode), f = True)
    mc.connectAttr('{}.{}'.format(ctrlName, dynSpeedAttr[0]), '{}.{}'.format(dynConNode, dynSpeedAttr[0]))
    print 'C'
    ## Generate Expression Translate Y
    mc.expression( s='{}.translateY=abs(sin({}*2)*cos({}*2))*5+{}*6'.format(dynConNode, ctrlDyn, ctrlDyn, ctrlDyn))
    print 'D'

    # mc.setAttr('{}.strength'.format(dynConNode), 1)
    mc.connectAttr('{}.message'.format(dynConNode), 'expression1.objectMsg')

    ## Generate ramp2
    rampStrengthMapNode = applyMaterial(obj = objNewName,  node = 'ramp', elem = elem , addShade = False)
    mc.connectAttr ('{}.outAlpha'.format(rampStrengthMapNode),  '{}.strengthMap'.format(nComPoNode))

    ## Generate Expression
    mc.expression( s = '{}.colorEntryList[0].position=abs(sin({}*2)*cos({}*2))*0.9'.format(rampStrengthMapNode, ctrlDyn, ctrlDyn))

    mc.setAttr("{}.colorEntryList[0].color".format(rampStrengthMapNode), 0, 0, 0)
    mc.setAttr("{}.colorEntryList[1].color".format(rampStrengthMapNode), 1, 1, 1)
    mc.setAttr("{}.colorEntryList[1].position".format(rampStrengthMapNode), 1)

    ## Generate Expression
    mc.expression( s = '{}.damp=abs(sin({}*2)*cos({}*2))*2'.format(dynConNode, ctrlDyn, ctrlDyn))

    mc.setAttr("{}Shape.compressionResistance".format(nclothNode), 0)
    mc.setAttr("{}Shape.bendResistance".format(nclothNode), 0)
    mc.setAttr("{}Shape.stretchDamp".format(nclothNode), 4.8)

    ## Generate Wrap Group
    mc.parent(dynConNode, nucleusNode, objGrp)
    mc.setAttr('{}.visibility'.format(dynConNode), 0)
    mc.setAttr('{}.visibility'.format(nucleusNode), 0)

def applyMaterial(obj, node = 'lambert', elem = '', addShade = True):

    name, side, lastName = utaCore.splitName([obj])

    if mc.objExists(obj):
        shd = mc.shadingNode(node, name="{}{}{}{}".format(name, elem, side, node), asShader=True)
        shdSG = mc.sets(name='{}{}{}SG'.format(name, elem, side), empty=True, renderable=True, noSurfaceShader=True)
        if addShade:
            mc.connectAttr('{}.outColor'.format(shd), '{}.surfaceShader'.format(shdSG))
            mc.sets(obj, e=True, forceElement=shdSG)
    return shd













# from utaTools.utapy import jellyFishMove
# reload(jellyFishMove)
# jellyFishMove.jellyFishMove2(obj = 'jellyFishTest_Geo', elem = 'A', posiLoc = 'Posi_Loc')

def jellyFishMove3(obj = '',elem = '', posiLoc = '',*args):
    ## Generate Splite name
    name, side, lastName = utaCore.splitName([obj])

    ## Generate Group
    stillGrp = mc.group( n = '{}{}Still{}Grp'.format(name, elem, side), em = True)
    ctrlGrp = mc.group( n = '{}{}Ctrl{}Grp'.format(name, elem, side), em = True)
    geoGrp = mc.group( n = '{}{}Geo{}Grp'.format(name, elem, side), em = True)

    ## Generate Duplicate Geo for Skin and Dyn
    objNameSkin = mc.rename(mc.duplicate(obj), '{}{}Skin{}Geo'.format(name, elem, side))
    objNameDyn = mc.rename(mc.duplicate(obj), '{}{}Dyn{}Geo'.format(name, elem, side))
    objNameWrap = mc.rename(mc.duplicate(obj), '{}{}Wrap{}Geo'.format(name, elem, side))
    objNameNew = mc.rename(obj,'{}{}{}Geo'.format(name, elem, side))

    ## Generate Group for geo
    mc.select(objNameWrap)
    objNameWrapGrp = rigTools.UtaDoZeroGroup()[0]
    mc.select(objNameDyn)
    objNameDynGrp = rigTools.UtaDoZeroGroup()[0]
    mc.select(objNameSkin)
    objNameSkinGrp = rigTools.UtaDoZeroGroup()[0]


    ## Generate Control 
    mc.select(objNameNew)
    grpCtrlName, grpCtrlOfsName, grpCtrlParsName, ctrlName, GmblCtrlName = utaCore.createControlCurve(curveCtrl = 'circle', parentCon = False, connections = False, geoVis = True)
    mc.delete(mc.parentConstraint(posiLoc, grpCtrlName, mo = False))
    utaCore.lockAttrObj([ctrlName], lock = True,keyable = False)
    mc.connectAttr('{}.GeoVis'.format(ctrlName), '{}.v'.format(geoGrp))

    ## Generate add Attribute
    attrs = ['strength', 'ctrlSpeed', 'timeSpeed']
    timeAttrs = ['time']
    timeAttr = utaCore.addAttrCtrl (ctrl = ctrlName, elem = timeAttrs, min = 0, max = 1, at = 'long',dv = 1)
    dynSpeedAttr = utaCore.addAttrCtrl (ctrl = ctrlName, elem = attrs, min = -100, max = 100, at = 'float',dv = 0)

    ## Generate Node
    values = [0.5, 0.01, 0.02]
    ctrlMdv = mc.createNode('multiplyDivide', n = '{}{}Ctrl{}Mdv'.format(name, elem, side))
    switchBcl = mc.createNode('blendColors', n = '{}{}Switch{}Bcl'.format(name, elem, side))
    switchMdv = mc.createNode('multiplyDivide', n = '{}{}Switch{}Mdv'.format(name, elem, side))


    ## Generate Connect Node
    for i in range(len(dynSpeedAttr)):
        mc.setAttr('{}.{}'.format(ctrlName, dynSpeedAttr[i]), values[i])
        
    mc.connectAttr('{}.ty'.format(ctrlName), '{}.input1X'.format(ctrlMdv))
    # mc.connectAttr('{}.{}'.format(ctrlName, dynSpeedAttr[0]), '{}.input1Y'.format(ctrlMdv))
    mc.connectAttr('{}.{}'.format(ctrlName, dynSpeedAttr[1]),'{}.input2X'.format(ctrlMdv))
    mc.connectAttr('{}.{}'.format(ctrlName, timeAttr[0]), '{}.blender'.format(switchBcl))
    mc.connectAttr('time1.outTime', '{}.input1Y'.format(ctrlMdv))
    mc.connectAttr('{}.outputX'.format(ctrlMdv), '{}.color2R'.format(switchBcl))
    mc.connectAttr('{}.outputY'.format(ctrlMdv), '{}.color1R'.format(switchBcl))

    mc.connectAttr('{}.outputR'.format(switchBcl), '{}.input1X'.format(switchMdv))
    mc.connectAttr('{}.{}'.format(ctrlName, dynSpeedAttr[2]),'{}.input2Y'.format(ctrlMdv))

    ## Generate String for Expression
    ctrlDyn = '{}.outputX'.format(switchMdv)

    ## Generate lambert Node
    lambertNode = applyMaterial(obj = objNameDyn,  node = 'lambert', elem = elem, addShade = True)
    ## Generate ramp
    rampNode = applyMaterial(obj = objNameDyn,  node = 'ramp', elem = elem, addShade = False)
    mc.connectAttr ('{}.outColor'.format(rampNode), '{}.color'.format(lambertNode), f = True)

    ## Generate set Panar Geo
    mc.select('{}.vtx[0:1000000]'.format(objNameDyn),  d = True) ;
    mc.select('{}.f[0:1000000]'.format(objNameDyn),  add = True) ;
    mc.polyProjection('{}.f[0:1000000]'.format(objNameDyn), ch = True,  type =  'Planar',  ibd = True, md = 'x');
    mc.select(cl = True)
    print 'A'
    ## Generate nCloth and nucleus
    mc.select(objNameDyn)
    # nclothNodeShape = mc.createNode('nCloth', n = '{}{}nClothShape'.format(name, side))
    # nucleus = mc.createNode ("nucleus", n = '{}{}nucleus'.format(name, side))
    # mc.connectAttr ('{}Shape.worldMesh[0]'.format(objNameDyn), '{}.inputMesh'.format(nclothNodeShape))
    # mc.connectAttr ('{}.currentState'.format(nclothNodeShape), '{}.inputActive[0]'.format(nucleus))
    # mc.connectAttr ('{}.startState'.format(nclothNodeShape), '{}.inputActiveStart[0]'.format(nucleus))
    # mc.connectAttr ('{}.startFrame'.format(nucleus), '{}.startFrame'.format(nclothNodeShape))
    # mc.connectAttr ('{}.outputObjects[0]'.format(nucleus), '{}.nextState'.format(nclothNodeShape))
    # mc.connectAttr ('{}.outputMesh'.format(nclothNodeShape), '{}Shape1.inMesh'.format(objNameDyn), f = True)
    # mc.connectAttr ('time1.outTime', '{}.currentTime'.format(nucleus))
    # mc.connectAttr ('time1.outTime', '{}.currentTime'.format(nclothNodeShape))

    mm.eval ('createNCloth 0;')

    if mc.objExists('nCloth1'):
        nclothNode = mc.rename('nCloth1', '{}{}{}nCloth'.format(name, elem, side))
    if mc.objExists('nucleus1'):
        nucleusNode = mc.rename('nucleus1', '{}{}{}nucleus'.format(name, elem, side))
    print 'B'



    ## Generate Tranform Constraint
    mc.select(objNameDyn, r = True)
    mc.select('{}.vtx[0:100000000]'.format(objNameDyn),  r = True) ;
    # dynConNode = mc.createNode ("dynamicConstraint", n = '{}{}dynConShape'.format(name, side))
    # mc.connectAttr('{}.evalStart[0]'.format(dynConNode), '{}.inputStart[0]'.format(nucleus))
    # nComPoNode = mc.createNode ("nComponent", n = '{}{}nComponent'.format(name, side))
    # mc.connectAttr('{}.outComponent'.format(nComPoNode),  '{}.componentIds[0]'.format(dynConNode))
    # mc.connectAttr('{}.nucleusId'.format(nclothNodeShape), '{}.objectId'.format(nComPoNode))
    # mc.connectAttr('{}.evalCurrent[0]'.format(dynConNode), '{}.inputCurrent[1]'.format(nucleus))
    # mc.connectAttr('time1.outTime',  '{}.currentTime'.format(dynConNode))
    dynConNode = mm.eval('createNConstraint transform 0;')[0]


    nCompoName = 'nComponent1'
    if mc.objExists(nCompoName):
        nComPoNode = mc.rename(nCompoName, '{}{}{}nComPo'.format(name, elem, side))
    dynConNode = mc.rename('dynamicConstraint1', '{}{}{}dynConNode'.format(name, elem, side))
    mc.connectAttr(ctrlDyn, '{}Shape.currentTime'.format(dynConNode), f = True)
    mc.connectAttr('{}.{}'.format(ctrlName, dynSpeedAttr[0]), '{}.{}'.format(dynConNode, dynSpeedAttr[0]))
    print 'C'
    ## Generate Expression Translate Y
    mc.expression( s='{}.translateY=abs(sin({}*2)*cos({}*2))*5+{}*6'.format(dynConNode, ctrlDyn, ctrlDyn, ctrlDyn))
    print 'D'

    # mc.setAttr('{}.strength'.format(dynConNode), 1)
    mc.connectAttr('{}.message'.format(dynConNode), 'expression1.objectMsg')

    ## Generate ramp2
    rampStrengthMapNode = applyMaterial(obj = objNameDyn,  node = 'ramp', elem = elem , addShade = False)
    mc.connectAttr ('{}.outAlpha'.format(rampStrengthMapNode),  '{}.strengthMap'.format(nComPoNode))

    ## Generate Expression
    mc.expression( s = '{}.colorEntryList[0].position=abs(sin({}*2)*cos({}*2))*0.9'.format(rampStrengthMapNode, ctrlDyn, ctrlDyn))

    mc.setAttr("{}.colorEntryList[0].color".format(rampStrengthMapNode), 0, 0, 0)
    mc.setAttr("{}.colorEntryList[1].color".format(rampStrengthMapNode), 1, 1, 1)
    mc.setAttr("{}.colorEntryList[1].position".format(rampStrengthMapNode), 1)

    ## Generate Expression
    mc.expression( s = '{}.damp=abs(sin({}*2)*cos({}*2))*2'.format(dynConNode, ctrlDyn, ctrlDyn))

    mc.setAttr("{}Shape.compressionResistance".format(nclothNode), 0)
    mc.setAttr("{}Shape.bendResistance".format(nclothNode), 0)
    mc.setAttr("{}Shape.stretchDamp".format(nclothNode), 4.8)

    ## Generate Head Jnt-----------------------------------------------------------------------------------
    headJnt = mc.createNode('joint', n = '{}Head{}{}Jnt'.format(name, elem, side))

    # mc.delete(mc.parentConstraint(ctrlName, headJnt, mo = False))
    mc.select(headJnt)
    headJntGrp = rigTools.UtaDoZeroGroup()[0]

    ## Genarate bindSkin headJnt to geoSkin
    utaCore.bindSkinCluster(jntObj = [headJnt], obj = [objNameSkin])

    ## Generate BlendShape skinGeo to wrapGeo
    mc.select(objNameSkin, r = True)   
    mc.select(objNameWrap, add= True)
    lrr.doAddBlendShape()

    ## Generate BlendShape dynGeo to skinGeo
    mc.select(objNameDyn, r = True)   
    mc.select(objNameSkin, add= True)
    lrr.doAddBlendShape()

    ## Generate HeadControl and connect Node
    mc.select(headJnt)
    grpCtrlHeadName, grpCtrlOfsHeadName, grpCtrlParsHeadName, ctrlHeadName, GmblCtrlHeadName = utaCore.createControlCurve(curveCtrl = 'cube', parentCon = False, connections = False, geoVis = True)
    mc.delete(mc.parentConstraint(posiLoc , grpCtrlHeadName, mo = False))
    utaCore.lockAttr(listObj = [ctrlHeadName], attrs = ['GeoVis'],lock = True, keyable = False)

    nurb = utaCore.posiReverseNode(name = name, elem = elem, side = side, posiLoc = posiLoc, ctrl = ctrlHeadName, zroGrp = grpCtrlHeadName, zroOfstGrp = grpCtrlOfsHeadName)
    mc.select(ctrlHeadName, r = True)
    mc.select(headJntGrp, add = True)
    utaCore.connectTRS(trs = True, translate = False, rotate = False, scale = False)
    mc.select(nurb, r = True)
    mc.select(objNameWrap, add = True)
    mc.CreateWrap()
    mc.parentConstraint(GmblCtrlHeadName, grpCtrlName, mo = True)
    mc.scaleConstraint(GmblCtrlHeadName, grpCtrlName, mo = True)

    ## Generate Wrap Group
    mc.parent(dynConNode, nucleusNode, geoGrp)
    mc.parent(objNameSkinGrp, geoGrp)
    mc.parent(objNameDynGrp, nclothNode, objNameWrapGrp, headJntGrp, nurb, stillGrp)
    mc.parent(grpCtrlName, grpCtrlHeadName, ctrlGrp)
    listsVis = [dynConNode, nucleusNode, stillGrp]
    for each in listsVis:
        mc.setAttr('{}.visibility'.format(each), 0)

    ## Generate assign color
    utaCore.assignColor(obj = [ctrlName], col = 'green')
    
    ## Generate Delete Tmp
    mc.delete(obj, posiLoc)



    mc.select(cl = True)