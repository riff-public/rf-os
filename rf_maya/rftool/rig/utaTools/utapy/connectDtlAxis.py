#-----------------------------------------------------#
#        import utapy.connectDtlAxis as cde           #
#        reload(cde)                                  #
#        cde.conDtl(elem='test', valueConY=180)       #
#        cde.deleteConDtl()                           #
#        @Riff                                        #
#-----------------------------------------------------#

import sys
sys.path.append( r'P:\lib\local' )
import maya.cmds as mc
import maya.mel as mm
import ppmel.core as core
reload(core)

def conDtl(elem = '', valueConY = 180) :  #, objA = '', objB = ''
    #string 
    # elem = 'Test'
    sideLft = 'lft'
    sideRgt = 'rgt'

    #list
    listLft = mc.ls('*DtlCtrlZro_lft_grp')
    listRgt = mc.ls('*DtlCtrlZro_rgt_grp')
    listCen = mc.ls('*DtlCtrlZro_cen_grp')
    listJntLft = mc.ls('*DtlJntZro_lft_grp')
    listJntRgt = mc.ls('*DtlJntZro_rgt_grp')
    listJntCen = mc.ls('*DtlJntZro_cen_grp')
    listNrbLft = mc.ls('*Dtl_lft_nrb')
    listNrbRgt = mc.ls('*Dtl_rgt_nrb')
    listNrbCen = mc.ls('*Dtl_cen_nrb')
    # listGeo = mc.ls('hat*_ply','head*_ply','google_ply','eyelash*_ply','tongue*_ply','eye_lft_grp','eye_rgt_grp')



    #check Amount 
    listLftAmount = len(listLft)
    listRgtAmount = len(listRgt)
    listJntLftAmount = len(listJntLft) 
    listJntRgtAmount = len(listJntRgt)
    listNrbLftAmount = len(listNrbLft)
    listNrbRgtAmount = len(listNrbRgt)
    listNrbCenAmount = len(listNrbCen)
    # listGeoAmount = len(listGeo)

    print 'ctrl_lft =', listLftAmount
    print 'ctrl_rgt =', listRgtAmount
    print 'jnt_lft =', listJntLftAmount
    print 'jnt_rgt =', listJntRgtAmount
    print 'nrb_lft =', listNrbLftAmount
    print 'nrt_rgt =', listNrbRgtAmount
    print 'nrb_cen =', listNrbCenAmount
    # print '_ply =', listGeoAmount

    for i in range(0,len(listLft)) :

        #addDoubleLinear  rotateXYZ
        # print (lisLft,lisRgt)
        nodeAdlX = core.addDoubleLinear('%s%sX_adl' % (elem,i))#'%s%s_mdv',%(elem,i)
        nodeAdlY = core.addDoubleLinear('%s%sY_adl' % (elem,i))#'%s%s_mdv',%(elem,i)
        nodeAdlZ = core.addDoubleLinear('%s%sZ_adl' % (elem,i))#'%s%s_mdv',%(elem,i)

        # node revest
        nodeMulti = core.multiplyDivide('%s%sXYZ_mdv' % (elem,i))
        nodeMulti2 = core.multiplyDivide('%s%sCtrlJnt_mdv' % (elem,i))
        # setAttr Multi
        mc.setAttr('%s.input2X' %nodeMulti,-1)
        mc.setAttr('%s.input2Y' %nodeMulti,-1)
        mc.setAttr('%s.input2Z' %nodeMulti,-1)

        #connect in addDou...
        # mc.addAttr(nodeAdlY,'ken',0,valueConY,0)
        mc.addAttr(nodeAdlX, longName='default')
        mc.addAttr(nodeAdlY, longName='default')
        mc.addAttr(nodeAdlZ, longName='default')
        mc.setAttr('%s.default'%nodeAdlX,0)
        mc.setAttr('%s.default'%nodeAdlY,valueConY)
        mc.setAttr('%s.default'%nodeAdlZ,0)
        mc.connectAttr('%s.default'%nodeAdlX, '%s.input1'%nodeAdlX)
        mc.connectAttr('%s.default'%nodeAdlY, '%s.input1'%nodeAdlY)
        mc.connectAttr('%s.default'%nodeAdlZ, '%s.input1'%nodeAdlZ)

        #connect listLft to nodeMulti...
        mc.connectAttr( '%s.rotateX' % listLft[i] , '%s.input1X' % nodeMulti) 
        mc.connectAttr( '%s.rotateY' % listLft[i] , '%s.input1Y' % nodeMulti)
        mc.connectAttr( '%s.rotateZ' % listLft[i] , '%s.input1Z' % nodeMulti)

        #connect listLft to nodeMulti...
        mc.connectAttr( '%s.rotateX' % listLft[i] , '%s.input1X' % nodeMulti2) 
        mc.connectAttr( '%s.rotateY' % listLft[i] , '%s.input1Y' % nodeMulti2)
        mc.connectAttr( '%s.rotateZ' % listLft[i] , '%s.input1Z' % nodeMulti2)

        # #connect addDou... to listRgt
        mc.connectAttr( '%s.outputX' % nodeMulti , '%s.input2' % nodeAdlX)
        mc.connectAttr( '%s.outputY' % nodeMulti , '%s.input2' % nodeAdlY)
        mc.connectAttr( '%s.outputZ' % nodeMulti , '%s.input2' % nodeAdlZ)

        # connectAttr addDoubleLinear to listRgt
        mc.connectAttr('%s.output' % nodeAdlX , '%s.rotateX' % listRgt[i])    
        mc.connectAttr('%s.output' % nodeAdlY , '%s.rotateY' % listRgt[i])  
        mc.connectAttr('%s.output' % nodeAdlZ , '%s.rotateZ' % listRgt[i]) 

        # connect addDoubleLinear to JntLft 
        mc.connectAttr('%s.output' % nodeAdlX , '%s.rotateX' % listJntRgt[i])    
        mc.connectAttr('%s.output' % nodeAdlY , '%s.rotateY' % listJntRgt[i])  
        mc.connectAttr('%s.output' % nodeAdlZ , '%s.rotateZ' % listJntRgt[i]) 

        # connect addDoubleLinear to JntLft 
        mc.connectAttr('%s.outputX' % nodeMulti2 , '%s.rotateX' % listJntLft[i])    
        mc.connectAttr('%s.outputY' % nodeMulti2 , '%s.rotateY' % listJntLft[i])  
        mc.connectAttr('%s.outputZ' % nodeMulti2 , '%s.rotateZ' % listJntLft[i]) 

    #create Group
    # grpGeo = mc.group(em = True)
    # grpGeoRe = mc.rename(grpGeo,'dtlGeo_Grp')
    grpNrb = mc.group(em = True)
    grpNrbRe = mc.rename(grpNrb,'allDtlNrb_grp')
    grpCtrl = mc.group(em = True)
    grpCtrlRe = mc.rename(grpCtrl,'allDtlCtrl_grp')
    grpJnt = mc.group(em = True)
    grpJntRe = mc.rename(grpJnt,'allDtlJnt_grp')
    mc.parent(listLft, grpCtrlRe)
    mc.parent(listRgt, grpCtrlRe)
    mc.parent(listCen, grpCtrlRe)
    mc.parent(listJntLft, grpJntRe)
    mc.parent(listJntRgt, grpJntRe)
    mc.parent(listJntCen, grpJntRe)
    mc.parent(listNrbLft, grpNrbRe)
    mc.parent(listNrbRgt, grpNrbRe)
    mc.parent(listNrbCen, grpNrbRe)
    # mc.parent(listGeo, grpGeoRe)

    print '---- Create Node Test Success!! ----'  
    print '----  "Love me Love my Script"  ----'   
    # return elem   
         
def deleteConDtl ():
    listAdl = mc.ls('*X_adl*')
    listMdv = mc.ls('*XYZ_mdv*')
    listCtrlJnt = mc.ls('*CtrlJnt_mdv*')

    mc.delete(listAdl, listMdv, listCtrlJnt)

    print '---- Clean Node Test is Success!! ----' 
    print '-----  "Love me Love my Script"  -----'  



