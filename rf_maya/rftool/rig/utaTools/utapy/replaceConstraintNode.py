import maya.cmds as mc
from utaTools.pkmel import core 
reload(core)
from utaTools.utapy import utaCore
reload(utaCore)

#############################################################
#replaceConstrainntNode(meshGeo = 'BodyGeo_Grp', nameSpNew = 'ctrl_md')
#############################################################

def replaceConstrainntNode(meshGeo = '', nameSpNew = '',*args):
objListsBase = []
objListsTip = []
lists = meshGeo
#obj = mc.listRelatives(lists ,c = True, ad = True, typ = 'transform')
obj = utaCore.listGeo(sels = lists, nameing = '_Grp', namePass = '_Geo')
objAll = []
for each in obj:
    if not 'Constraint1' in each:
        objAll.append(each)
for each in objAll:
    if not 'Shape' in each:
        sel = mc.listRelatives(each, typ="constraint")
        if sel:
            nameSpOld = (mc.listConnections(sel[0] + ".target")[1]).split(':')[0]
            jntOld = (mc.listConnections(sel[0] + ".target")[1]).split(':')[-1]
        if mc.objExists('{}:{}'.format(nameSpNew, jntOld)):
            mc.delete(sel)
            utaCore.parentScaleConstraintLoop(obj = '{}:{}'.format(nameSpNew, jntOld), lists = [each],par = True, sca = True, ori = False, poi = False)
            print '----DONE----'