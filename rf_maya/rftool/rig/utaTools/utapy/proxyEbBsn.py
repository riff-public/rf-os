import maya.cmds as mc
import pymel.core as pm
    #---------------------------------------------#
    #               EyebrowBsn R L Ctrl             #
    #---------------------------------------------#
def proxyEbBsn(tyOffset,bodyGeoBsn, eyeBrowGeoBsh, *args):    
    ctrlBsns = mc.ls('*:eyeBrowBsh_*_ctrl')
    lenBody = len(bodyGeoBsn)
    lenEb = len(eyeBrowGeoBsh)
    print "Find Obj :" , ctrlBsns


    doGrp = mc.createNode('transform',n = 'AllEyebrowUp')
    try:
        for ctrlBsn in ctrlBsns:
            setCkUprIo = mc.setAttr('%s.allUD' %(ctrlBsn), 10)
        dupBdGeo = mc.duplicate(bodyGeoBsn, n = 'BodyPrEyebrowUp_Geo')
        mc.parent(dupBdGeo,doGrp)
        dupEbGeo = mc.duplicate(eyeBrowGeoBsh, n = 'EyebrowPrEyebrowUp_Geo')
        mc.parent(dupEbGeo,doGrp)
        print "Creating >>", doGrp, ":: 1. ", dupBdGeo, ":: 2.", dupEbGeo
    except: pass

    doGrp = mc.createNode('transform',n = 'AllEyebrowDn')
    try:
        for ctrlBsn in ctrlBsns:
            setCnInAttr = mc.setAttr('%s.allUD' %(ctrlBsn), -10)
        dupBdGeo = mc.duplicate(bodyGeoBsn, n = 'BodyPrEyebrowDn_Geo')
        mc.parent(dupBdGeo,doGrp)
        dupEbGeo = mc.duplicate(eyeBrowGeoBsh, n = 'EyebrowPrEyebrowDn_Geo')
        mc.parent(dupEbGeo,doGrp)
        print "Creating >>", doGrp, ":: 1. ", dupBdGeo, ":: 2.", dupEbGeo

    except: pass
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.allUD' %(ctrlBsn), 0)
    
    # #------------------------------------------------------------------------------#
    doGrp = mc.createNode('transform',n = 'AllEyebrowIn')
    try:
        for ctrlBsn in ctrlBsns:
            setCkUprIo = mc.setAttr('%s.allIO' %(ctrlBsn), 10)
        dupBdGeo = mc.duplicate(bodyGeoBsn, n = 'BodyPrEyebrowIn_Geo')
        mc.parent(dupBdGeo,doGrp)
        dupEbGeo = mc.duplicate(eyeBrowGeoBsh, n = 'EyebrowPrEyebrowIn_Geo')
        mc.parent(dupEbGeo,doGrp)
        print "Creating >>", doGrp, ":: 1. ", dupBdGeo, ":: 2.", dupEbGeo

    except: pass
    doGrp = mc.createNode('transform',n = 'AllEyebrowOut')
    try:
        for ctrlBsn in ctrlBsns:
            setCnInAttr = mc.setAttr('%s.allIO' %(ctrlBsn), -10)
        dupBdGeo = mc.duplicate(bodyGeoBsn, n = 'BodyPrEyebrowOut_Geo')
        mc.parent(dupBdGeo,doGrp)
        dupEbGeo = mc.duplicate(eyeBrowGeoBsh, n = 'EyebrowPrEyebrowOut_Geo')
        mc.parent(dupEbGeo,doGrp)
        print "Creating >>", doGrp, ":: 1. ", dupBdGeo, ":: 2.", dupEbGeo

    except: pass
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.allIO' %(ctrlBsn), 0)
        
    # #------------------------------------------------------------------------------#
    doGrp = mc.createNode('transform',n = 'AllEyebrowRotUp')
    try: 
        for ctrlBsn in ctrlBsns:
            setCkUprIo = mc.setAttr('%s.turnUD' %(ctrlBsn), 10)
        dupBdGeo = mc.duplicate(bodyGeoBsn, n = 'BodyPrEyebrowRotUp_Geo')
        mc.parent(dupBdGeo,doGrp)
        dupEbGeo = mc.duplicate(eyeBrowGeoBsh, n = 'EyebrowPrEyebrowRotUp_Geo')
        mc.parent(dupEbGeo,doGrp)
        print "Creating >>", doGrp, ":: 1. ", dupBdGeo, ":: 2.", dupEbGeo

    except: pass

    doGrp = mc.createNode('transform',n = 'AllEyebrowRotDn')
    try:
        for ctrlBsn in ctrlBsns:
            setCnInAttr = mc.setAttr('%s.turnUD' %(ctrlBsn), -10)
        dupBdGeo = mc.duplicate(bodyGeoBsn, n = 'BodyPrEyebrowRotDn_Geo')
        mc.parent(dupBdGeo,doGrp)
        dupEbGeo = mc.duplicate(eyeBrowGeoBsh, n = 'EyebrowPrEyebrowRotDn_Geo')
        mc.parent(dupEbGeo,doGrp)
        print "Creating >>", doGrp, ":: 1. ", dupBdGeo, ":: 2.", dupEbGeo

    except: pass
    for ctrlBsn in ctrlBsns:
        setCnInAttr = mc.setAttr('%s.turnUD' %(ctrlBsn), 0)

    
    # #---------------------------------------------#
    
    # #---------------------------------------------#
    # #               object ot grp                 #
    # #---------------------------------------------#
    
    # #---------------------------------------------#
    
    prxGrp = mc.createNode('transform',n = 'proxyBsh_Grp')
    
    mc.parent('AllEyebrowUp','AllEyebrowDn','AllEyebrowIn','AllEyebrowOut','AllEyebrowRotUp','AllEyebrowRotDn','proxyBsh_Grp')
    mc.setAttr('%s.tx' %prxGrp, -(tyOffset))
    
    prxBsnMd = ['AllEyebrowUp','AllEyebrowDn','AllEyebrowIn','AllEyebrowOut','AllEyebrowRotUp','AllEyebrowRotDn']   
    # print prxBsnMd
    numHead = len(prxBsnMd)
    defaultTy = 0
    for i in range(numHead):
        mc.setAttr((prxBsnMd[i]+'.ty'),defaultTy)
        defaultTy += tyOffset 



