import maya.cmds as mc
from utaTools.utapy import utaCore
reload(utaCore)

def unlockScale(*args):
	lists = ['AllMover_Ctrl']
	attrs = ['sx', 'sz']
	for each in lists:
		if mc.objExists(each):
			for i in range(len(attrs)):
				mc.setAttr('{}.{}'.format(each, attrs[i]),k = True, l = False)
				conCheck = utaCore.checkConnect(object = each, attr = attrs[i])
				mc.disconnectAttr(conCheck[0], '{}.{}'.format(each, attrs[i]))
