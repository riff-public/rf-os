import maya.cmds as mc
reload(mc)
def connectRZ(elem = ''):
	ikList = ['PathIkRZ1_Jnt',
				'PathIkRZ2_Jnt',
				'PathIkRZ3_Jnt',
				'PathIkRZ4_Jnt',
				'PathIkRZ5_Jnt',
				'PathIkRZ6_Jnt',
				'PathIkRZ7_Jnt',
				'PathIkRZ8_Jnt',
				'PathIkRZ9_Jnt',
				'PathIkRZ10_Jnt'
				]
	fkList = ['PathFk1_ctrl',
				'PathFk2_ctrl',
				'PathFk3_ctrl'
				]
	ikLists = len(ikList)
	fkLists = len(fkList)

	ix = 0
	for each in fkList:
		# for x in range(0,fkLists):
			# print each
			# for x in range(0, ikLists):

		listsPma = mc.createNode('plusMinusAverage', n = ('%s%s_pma' % (elem , ix+1)))
		for each in range(0, fkLists)
			mc.connectAttr('%s.rotateX' % each , ('%s.input1D[%s]' % (listsPma, ix+1)))
			ix+=1
		ix+=1
	# ix+=1
		# print listsPma

