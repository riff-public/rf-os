##ptun
import maya.cmds as mc
def addCnrUpDn(*args):
	mc.parent('facDirec_AddCnrUpDn:lipCnrUp_L_Jnt', 'facDirec_AddCnrUpDn:lipCnrDn_L_Jnt', 'lipCnrSub_L_Jnt')
	mc.parent('facDirec_AddCnrUpDn:lipCnrUp_R_Jnt', 'facDirec_AddCnrUpDn:lipCnrDn_R_Jnt', 'lipCnrSub_R_Jnt')
	mc.parent('facDirec_AddCnrUpDn:lipCnrUpMainCnrUpSubCtrl_L_Grp','facDirec_AddCnrUpDn:lipCnrDnMainCnrDnSubCtrl_L_Grp', 'facDirec_AddCnrUpDn:lipCnrUpMainCnrUpSubCtrl_R_Grp','facDirec_AddCnrUpDn:lipCnrDnMainCnrDnSubCtrl_R_Grp', 'FacialDirRigCtrl_Grp')
	## L
	mc.delete(mc.parentConstraint('lipCnrPio_L_Grp', mc.group(n = 'lipCnrUp_L_Grp',em = True), mo = False))
	mc.delete(mc.parentConstraint('lipCnrPio_L_Grp', mc.group(n = 'lipCnrDn_L_Grp',em = True), mo = False))
	mc.parent('lipCnrUp_L_Grp', 'lipCnrDn_L_Grp', 'lipCnrPio_L_Grp')
	## R
	mc.delete(mc.parentConstraint('lipCnrPio_R_Grp', mc.group(n = 'lipCnrUp_R_Grp',em = True), mo = False))
	mc.delete(mc.parentConstraint('lipCnrPio_R_Grp', mc.group(n = 'lipCnrDn_R_Grp',em = True), mo = False))
	mc.parent('lipCnrUp_R_Grp', 'lipCnrDn_R_Grp', 'lipCnrPio_R_Grp')
	## parent L
	mc.parentConstraint('lipCnrUp_L_Grp', 'facDirec_AddCnrUpDn:lipCnrUpMainCnrUpSubCtrl_L_Grp', mo = True)
	mc.parentConstraint('lipCnrDn_L_Grp', 'facDirec_AddCnrUpDn:lipCnrDnMainCnrDnSubCtrl_L_Grp', mo = True)
	mc.scaleConstraint('lipCnrUp_L_Grp', 'facDirec_AddCnrUpDn:lipCnrUpMainCnrUpSubCtrl_L_Grp', mo = True)
	mc.scaleConstraint('lipCnrDn_L_Grp', 'facDirec_AddCnrUpDn:lipCnrDnMainCnrDnSubCtrl_L_Grp', mo = True)
	## parent R
	mc.parentConstraint('lipCnrUp_R_Grp', 'facDirec_AddCnrUpDn:lipCnrUpMainCnrUpSubCtrl_R_Grp', mo = True)
	mc.parentConstraint('lipCnrDn_R_Grp', 'facDirec_AddCnrUpDn:lipCnrDnMainCnrDnSubCtrl_R_Grp', mo = True)
	mc.scaleConstraint('lipCnrUp_R_Grp', 'facDirec_AddCnrUpDn:lipCnrUpMainCnrUpSubCtrl_R_Grp', mo = True)
	mc.scaleConstraint('lipCnrDn_R_Grp', 'facDirec_AddCnrUpDn:lipCnrDnMainCnrDnSubCtrl_R_Grp', mo = True)