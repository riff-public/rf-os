import maya.cmds as mc
import maya.mel as mm
import maya.OpenMaya as om
from ncmel import core
from ncmel import rigTools as rt
from ncmel import ribbonRig
from utaTools.utapy import utaCore
from lpRig import core as lrc
reload(lrc)
reload( core )
reload( rt )
reload( ribbonRig )
reload(utaCore)
# -------------------------------------------------------------------------------------------------------------
#
#  LEG RIGGING MODULE
#  
# -------------------------------------------------------------------------------------------------------------

class Run( object ):

	def __init__( self , 	upLegTmpJnt   = 'UpLeg_L_TmpJnt' ,
							lowLegTmpJnt  = 'LowLeg_L_TmpJnt' ,
							ankleTmpJnt   = 'Ankle_L_TmpJnt' ,
							ballTmpJnt    = 'Ball_L_TmpJnt' ,
							toeTmpJnt     = 'Toe_L_TmpJnt' ,
							heelTmpJnt    = 'Heel_L_TmpJnt' ,
							footInTmpJnt  = 'FootIn_L_TmpJnt' ,
							footOutTmpJnt = 'FootOut_L_TmpJnt' ,
							kneeTmpJnt    = 'Knee_L_TmpJnt' ,
							heelIkTwistPivTmpJnt = 'HeelIkTwistPiv_L_TmpJnt',
							parent        = 'Pelvis_Jnt' , 
							ctrlGrp       = 'Ctrl_Grp' ,
							jntGrp        = 'Jnt_Grp' ,
							skinGrp       = 'Skin_Grp' ,
							stillGrp      = 'Still_Grp' ,
							ikhGrp        = 'Ikh_Grp' ,
							elem          = '' ,
							side          = 'L' ,
							ribbon        = True ,
							hi            = True ,
							foot          = True ,
							kneePos	   = '' ,
							size          = 1 
				 ):

		##-- Info
		elem = elem.capitalize()
		rootJnt = 'Root_Jnt'

		if side == '' :
			sideRbn = ''
			side = '_'
		else :
			sideRbn = '%s' %side
			side = '_%s_' %side

		if 'L' in side  :
			valueSide = 1
			axis = 'y+'
			upVec = [ 0 , 0 , 1 ]
			kneeAim = [ 0 , -1 , 0 ]
			kneeUpVec = [ 1 , 0 , 0 ]
			rootTwsAmp = 1
			endTwsAmp = -1
		elif 'R' in side :
			valueSide = -1
			axis = 'y-'
			upVec = [ 0 , 0 , -1 ]
			kneeAim = [ 0 , 1 , 0 ]
			kneeUpVec = [ -1 , 0 , 0 ]
			rootTwsAmp = -1
			endTwsAmp = 1


		#-- Create Main Group
		self.legCtrlGrp = rt.createNode( 'transform' , 'Leg%sCtrl%sGrp' %( elem , side ))
		self.legJntGrp = rt.createNode( 'transform' , 'Leg%sJnt%sGrp' %( elem , side ))
		mc.parentConstraint( parent , self.legCtrlGrp , mo = False )
		self.legJntGrp.snap( parent )
		
		#-- Create Joint
		self.upLegJnt = rt.createJnt( 'UpLeg%s%sJnt' %( elem , side ) , upLegTmpJnt )
		self.lowLegJnt = rt.createJnt( 'LowLeg%s%sJnt' %( elem , side ) , lowLegTmpJnt )
		self.ankleJnt = rt.createJnt( 'Ankle%s%sJnt' %( elem , side ) , ankleTmpJnt )
		self.ankleJnt.parent( self.lowLegJnt )
		self.lowLegJnt.parent( self.upLegJnt )
		self.upLegJnt.parent( rootJnt )

		if foot == True :
			self.ballJnt = rt.createJnt( 'Ball%s%sJnt' %( elem , side ) , ballTmpJnt )
			self.toeJnt = rt.createJnt( 'Toe%s%sJnt' %( elem , side ) , toeTmpJnt )
			self.toeJnt.parent( self.ballJnt )
			self.ballJnt.parent( self.ankleJnt )

		#-- Create Controls
		self.legCtrl = rt.createCtrl( 'Leg%s%sCtrl' %( elem , side ) , 'stick' , 'green' )
		self.legZro = rt.addGrp( self.legCtrl )

		#-- Adjust Shape Controls
		self.legCtrl.scaleShape( size )

		if 'L' in side :
			self.legCtrl.rotateShape( -90 , 0 , 0 )
		else :
			self.legCtrl.rotateShape( 90 , 0 , 0 )
		
		#-- Adjust Rotate Order
		for obj in ( self.upLegJnt , self.lowLegJnt , self.ankleJnt ) :
			obj.setRotateOrder( 'yzx' )

		if foot == True :
			for obj in ( self.ballJnt , self.toeJnt ) :
				obj.setRotateOrder( 'yzx' )

		#-- Rig process
		mc.parentConstraint( self.ankleJnt , self.legZro , mo = False )
		upLegNonRollJnt = rt.addNonRollJnt( 'UpLeg' , elem , self.upLegJnt , self.lowLegJnt , parent , axis )
		lowLegNonRollJnt = rt.addNonRollJnt( 'LowLeg' , elem , self.lowLegJnt , self.ankleJnt , self.upLegJnt , axis )

		self.upLegNonRollJntGrp = upLegNonRollJnt['jntGrp']
		self.upLegNonRollRootNr = upLegNonRollJnt['rootNr']
		self.upLegNonRollRootNrZro = upLegNonRollJnt['rootNrZro']
		self.upLegNonRollEndNr = upLegNonRollJnt['endNr']
		self.upLegNonRollIkhNr = upLegNonRollJnt['ikhNr']
		self.upLegNonRollIkhNrZro = upLegNonRollJnt['ikhNrZro']
		self.upLegNonRollTwistGrp = upLegNonRollJnt['twistGrp']

		self.lowLegNonRollJntGrp = lowLegNonRollJnt['jntGrp']
		self.lowLegNonRollRootNr = lowLegNonRollJnt['rootNr']
		self.lowLegNonRollRootNrZro = lowLegNonRollJnt['rootNrZro']
		self.lowLegNonRollEndNr = lowLegNonRollJnt['endNr']
		self.lowLegNonRollIkhNr = lowLegNonRollJnt['ikhNr']
		self.lowLegNonRollIkhNrZro = lowLegNonRollJnt['ikhNrZro']
		self.lowLegNonRollTwistGrp = lowLegNonRollJnt['twistGrp']
		
		#-- Adjust Hierarchy
		mc.parent( self.legZro , self.legCtrlGrp )
		mc.parent( self.legCtrlGrp , ctrlGrp )
		mc.parent( self.upLegNonRollJntGrp , self.lowLegNonRollJntGrp , self.legJntGrp )
		mc.parent( self.legJntGrp , jntGrp )

		#-- Cleanup
		for obj in ( self.legZro , self.legJntGrp , self.legCtrlGrp ) :
			obj.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

		self.legCtrl.lockHideAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

		#-- Fk
		#-- Create Main Group Fk
		self.legFkCtrlGrp = rt.createNode( 'transform' , 'Leg%sFkCtrl%sGrp' %( elem , side ))
		self.legFkJntGrp = rt.createNode( 'transform' , 'Leg%sFkJnt%sGrp' %( elem , side ))

		self.legFkCtrlGrp.snap( self.legCtrlGrp )
		self.legFkJntGrp.snap( self.legCtrlGrp )
	
		#-- Create Joint Fk
		self.upLegFkJnt = rt.createJnt( 'UpLeg%sFk%sJnt' %( elem , side ) , upLegTmpJnt )
		self.lowLegFkJnt = rt.createJnt( 'LowLeg%sFk%sJnt' %( elem , side ) , lowLegTmpJnt )
		self.ankleFkJnt = rt.createJnt( 'Ankle%sFk%sJnt' %( elem , side ) , ankleTmpJnt )
		self.ankleFkJnt.parent( self.lowLegFkJnt )
		self.lowLegFkJnt.parent( self.upLegFkJnt )
		self.upLegFkJnt.parent( self.legFkJntGrp )
		
		if foot == True :
			self.ballFkJnt = rt.createJnt( 'Ball%sFk%sJnt' %( elem , side ) , ballTmpJnt )
			self.toeFkJnt = rt.createJnt( 'Toe%sFk%sJnt' %( elem , side ) , toeTmpJnt )
			self.toeFkJnt.parent( self.ballFkJnt )
			self.ballFkJnt.parent( self.ankleFkJnt )

		#-- Create Controls Fk
		self.upLegFkCtrl = rt.createCtrl( 'UpLeg%sFk%sCtrl' %( elem , side ) , 'cylinder' , 'red' , jnt = True )
		self.upLegFkGmbl = rt.addGimbal( self.upLegFkCtrl )
		self.upLegFkZro = rt.addGrp( self.upLegFkCtrl )
		self.upLegFkOfst = rt.addGrp( self.upLegFkCtrl , 'Ofst' )
		self.upLegFkPars = rt.addGrp( self.upLegFkCtrl , 'Pars' )
		self.upLegFkZro.snapPoint( self.upLegFkJnt )
		self.upLegFkCtrl.snapJntOrient( self.upLegFkJnt )

		self.lowLegFkCtrl = rt.createCtrl( 'LowLeg%sFk%sCtrl' %( elem , side ) , 'cylinder' , 'red' , jnt = True )
		self.lowLegFkGmbl = rt.addGimbal( self.lowLegFkCtrl )
		self.lowLegFkZro = rt.addGrp( self.lowLegFkCtrl )
		self.lowLegFkOfst = rt.addGrp( self.lowLegFkCtrl , 'Ofst' )
		self.lowLegFkPars = rt.addGrp( self.lowLegFkCtrl , 'Pars' )
		self.lowLegFkZro.snapPoint( self.lowLegFkJnt )
		self.lowLegFkCtrl.snapJntOrient( self.lowLegFkJnt )

		self.ankleFkCtrl = rt.createCtrl( 'Ankle%sFk%sCtrl' %( elem , side ) , 'cylinder' , 'red' , jnt = True )
		self.ankleFkGmbl = rt.addGimbal( self.ankleFkCtrl )
		self.ankleFkZro = rt.addGrp( self.ankleFkCtrl )
		self.ankleFkOfst = rt.addGrp( self.ankleFkCtrl , 'Ofst' )
		self.ankleFkPars = rt.addGrp( self.ankleFkCtrl , 'Pars' )
		self.ankleFkZro.snapPoint( self.ankleFkJnt )
		self.ankleFkCtrl.snapJntOrient( self.ankleFkJnt )

		if foot == True :
			self.ballFkCtrl = rt.createCtrl( 'Ball%sFk%sCtrl' %( elem , side ) , 'cylinder' , 'red' , jnt = True )
			self.ballFkGmbl = rt.addGimbal( self.ballFkCtrl )
			self.ballFkZro = rt.addGrp( self.ballFkCtrl )
			self.ballFkOfst = rt.addGrp( self.ballFkCtrl , 'Ofst' )
			self.ballFkPars = rt.addGrp( self.ballFkCtrl , 'Pars' )
			self.ballFkSclOfst = rt.createNode( 'transform' , 'Ball%sFkCtrlSclOfst%sGrp' %( elem , side ) )
			self.ballFkSclOfst.snap( self.ankleFkJnt )
			self.ballFkZro.snapPoint( self.ballFkJnt )
			self.ballFkCtrl.snapJntOrient( self.ballFkJnt )
			self.ballFkZro.parent( self.ballFkSclOfst )

		#-- Adjust Shape Controls Fk
		for ctrl in ( self.upLegFkCtrl , self.upLegFkGmbl , self.lowLegFkCtrl , self.lowLegFkGmbl , self.ankleFkCtrl , self.ankleFkGmbl ) :
			ctrl.scaleShape( size )
			
		if foot == True :
			for ctrl in ( self.ballFkCtrl , self.ballFkGmbl ) :
				ctrl.scaleShape( size )
	
		#-- Adjust Rotate Order Fk
		for obj in ( self.upLegFkJnt , self.lowLegFkJnt , self.ankleFkJnt , self.upLegFkCtrl , self.lowLegFkCtrl , self.ankleFkCtrl ) :
			obj.setRotateOrder( 'yzx' )

		if foot == True :
			for obj in ( self.ballFkJnt , self.toeFkJnt , self.ballFkCtrl ) :
				obj.setRotateOrder( 'yzx' )

		#-- Rig process Fk
		mc.parentConstraint( self.upLegFkGmbl , self.upLegFkJnt , mo = False )
		mc.parentConstraint( self.lowLegFkGmbl , self.lowLegFkJnt , mo = False )
		mc.parentConstraint( self.ankleFkGmbl , self.ankleFkJnt , mo = False )

		rt.addFkStretch( self.upLegFkCtrl , self.lowLegFkOfst , 'y' , -1 )
		rt.addFkStretch( self.lowLegFkCtrl , self.ankleFkOfst , 'y' , -1 )
		
		if foot == True :
			mc.parentConstraint( self.ballFkGmbl , self.ballFkJnt , mo = False )
			rt.addFkStretch( self.ballFkCtrl , self.toeFkJnt , 'y' , valueSide )

		self.upLegFkLocWor = rt.localWorld( self.upLegFkCtrl , ctrlGrp , self.legFkCtrlGrp , self.upLegFkZro , 'orient' )

		self.upLegFkCtrl.attr('localWorld').value = 1
	
		#-- Adjust Hierarchy Fk
		mc.parent( self.legFkCtrlGrp , self.legCtrlGrp )
		mc.parent( self.legFkJntGrp , self.legJntGrp )
		mc.parent( self.upLegFkZro , self.legFkCtrlGrp )
		mc.parent( self.lowLegFkZro , self.upLegFkGmbl )
		mc.parent( self.ankleFkZro , self.lowLegFkGmbl )

		if foot == True :
			mc.parent( self.ballFkSclOfst , self.ankleFkGmbl )

		#-- Ik
		#-- Create Main Group Ik
		self.legIkCtrlGrp = rt.createNode( 'transform' , 'Leg%sIkCtrl%sGrp' %( elem , side ))
		self.legIkJntGrp = rt.createNode( 'transform' , 'Leg%sIkJnt%sGrp' %( elem , side ))
		self.legIkIkhGrp = rt.createNode( 'transform' , 'Leg%sIkh%sGrp' %( elem , side ))

		self.legIkCtrlGrp.snap( self.legCtrlGrp )
		self.legIkJntGrp.snap( self.legCtrlGrp )
		self.legIkIkhGrp.snap( self.legCtrlGrp )
		
		if foot == True :
			#-- Create Pivot Group Ik
			self.footPivGrp = rt.createNode( 'transform' , 'Foot%sIkPiv%sGrp' %( elem , side ))
			self.ballPivGrp = rt.createNode( 'transform' , 'Ball%sIkPiv%sGrp' %( elem , side ))
			self.ballPivCtrl = rt.createCtrl( 'Ball%sIkPiv%sCtrl' %( elem , side ) , 'arcCapsule' , 'blue' , jnt = 0 )
			
			self.bendPivGrp = rt.createNode( 'transform' , 'Bend%sIkPiv%sGrp' %( elem , side ))
			self.bendPivCtrl = rt.createCtrl( 'Bend%sIkPiv%sCtrl' %( elem , side ) , 'arcCapsule' , 'blue' , jnt = 0 )

			
			self.heelPivGrp = rt.createNode( 'transform' , 'Heel%sIkPiv%sGrp' %( elem , side ))
			self.heelPivCtrl = rt.createCtrl( 'Heel%sIkPiv%sCtrl' %( elem , side ) , 'sphere' , 'yellow' , jnt = 0 )
			
			
			self.heelTwistPivGrp = rt.createNode( 'transform' , 'Heel%sIkTwistPiv%sGrp' %( elem , side ))
			self.heelTwistPivCtrl = rt.createCtrl( 'Heel%sIkTwistPiv%sCtrl' %( elem , side ) , 'rotateCircle' , 'yellow' , jnt = 0 )

			self.toePivGrp = rt.createNode( 'transform' , 'Toe%sIkPiv%sGrp' %( elem , side ))
			self.toePivCtrl = rt.createCtrl( 'Toe%sIkPiv%sCtrl' %( elem , side ) , 'sphere' , 'yellow' , jnt = 0 )
			
			self.toeTwistPivGrp = rt.createNode( 'transform' , 'Toe%sIkTwistPiv%sGrp' %( elem , side ))
			self.toeTwistPivCtrl = rt.createCtrl( 'Toe%sIkTwistPiv%sCtrl' %( elem , side ) , 'rotateCircle' , 'yellow' , jnt = 0 )


			self.inPivGrp = rt.createNode( 'transform' , 'In%sIkPiv%sGrp' %( elem , side ))
			self.inPivCtrl  = rt.createCtrl( 'In%sIkPiv%sCtrl' %( elem , side ) , 'sphere' , 'yellow' , jnt = 0 )

			self.outPivGrp = rt.createNode( 'transform' , 'Out%sIkPiv%sGrp' %( elem , side ))
			self.outPivCtrl = rt.createCtrl( 'Out%sIkPiv%sCtrl' %( elem , side ) , 'sphere' , 'yellow' , jnt = 0 )
		   
			self.anklePivGrp = rt.createNode( 'transform' , 'Ankle%sIkPiv%sGrp' %( elem , side ))
			
			self.footPivGrp.snapPoint( ankleTmpJnt )
			self.ballPivGrp.snapPoint( ballTmpJnt )
			self.bendPivGrp.snapPoint( ballTmpJnt )
			self.toePivGrp.snapPoint( toeTmpJnt )
			self.heelPivGrp.snapPoint( heelTmpJnt )
			self.inPivGrp.snapPoint( footInTmpJnt )
			self.outPivGrp.snapPoint( footOutTmpJnt )
			self.anklePivGrp.snapPoint( ankleTmpJnt )

			mc.delete(mc.pointConstraint( toeTmpJnt , ballTmpJnt , self.toeTwistPivGrp , mo = False ))
			mc.delete(mc.pointConstraint( heelIkTwistPivTmpJnt , self.heelTwistPivGrp , mo = False ))


			for obj in ( self.footPivGrp , self.ballPivGrp , self.bendPivGrp , self.toePivGrp , self.toeTwistPivGrp , self.heelPivGrp , self.heelTwistPivGrp , self.inPivGrp , self.outPivGrp ) :
				obj.snapOrient( self.ballFkJnt )
			
			self.ballPivCtrl.snap( self.ballPivGrp )
			self.bendPivCtrl.snap( self.bendPivGrp )
			self.toePivCtrl.snap( self.toePivGrp )
			self.heelPivCtrl.snap( self.heelPivGrp )
			self.inPivCtrl.snap( self.inPivGrp )
			self.outPivCtrl.snap( self.outPivGrp )          
			self.toeTwistPivCtrl.snap( self.toeTwistPivGrp )
			self.heelTwistPivCtrl.snap( self.heelTwistPivGrp )
	 

			self.anklePivGrp.parent(self.ballPivCtrl)
			self.ballPivCtrl.parent(self.ballPivGrp)
			self.bendPivCtrl.parent(self.bendPivGrp)
			mc.parent(self.bendPivGrp,self.ballPivGrp,self.outPivCtrl)
			self.outPivCtrl.parent(self.outPivGrp)
			self.outPivGrp.parent(self.inPivCtrl)
			self.inPivCtrl.parent(self.inPivGrp)
			self.inPivGrp.parent( self.heelTwistPivCtrl)
			self.heelTwistPivCtrl.parent( self.heelTwistPivGrp)
			self.heelTwistPivGrp.parent( self.heelPivCtrl)
			self.heelPivCtrl.parent( self.heelPivGrp)
			self.heelPivGrp.parent( self.toeTwistPivCtrl)
			self.toeTwistPivCtrl.parent( self.toeTwistPivGrp)
			self.toeTwistPivGrp.parent( self.toePivCtrl)
			self.toePivCtrl.parent(self.toePivGrp)
			self.toePivGrp.parent(self.footPivGrp )
			
			#mc.parent( self.ballPivGrp , self.bendPivGrp , self.outPivGrp )
			#self.outPivGrp.parent( self.inPivGrp )
			#self.inPivGrp.parent( self.heelTwistPivGrp )
			#self.heelTwistPivGrp.parent( self.heelPivGrp )
			#self.heelPivGrp.parent( self.toeTwistPivGrp )
			#self.toeTwistPivGrp.parent( self.toePivGrp )
			#self.toePivGrp.parent( self.footPivGrp )
			#self.anklePivGrp.parent( self.ballPivGrp )
			for ctrl in ( self.ballPivCtrl , self.bendPivCtrl , self.toePivCtrl , self.heelPivCtrl,self.inPivCtrl,self.outPivCtrl,self.heelTwistPivCtrl,self.toeTwistPivCtrl) :
				ctrl.scaleShape( size )
				for ax in 'xyz':
					mc.setAttr(ctrl.pynode()+'.s%s'%(ax),l=1,k=0)
					mc.setAttr(ctrl.pynode()+'.t%s'%(ax),l=1,k=0)
				mc.setAttr(ctrl.pynode()+'.v',l=1,k=0)

		#-- Create Joint Ik
		self.upLegIkJnt = rt.createJnt( 'UpLeg%sIk%sJnt' %( elem , side ) , upLegTmpJnt )
		self.lowLegIkJnt = rt.createJnt( 'LowLeg%sIk%sJnt' %( elem , side ) , lowLegTmpJnt )
		self.ankleIkJnt = rt.createJnt( 'Ankle%sIk%sJnt' %( elem , side ) , ankleTmpJnt )
		self.ankleIkJnt.parent( self.lowLegIkJnt )
		self.lowLegIkJnt.parent( self.upLegIkJnt )
		self.upLegIkJnt.parent( self.legIkJntGrp )
		
		if foot == True :
			self.ballIkJnt = rt.createJnt( 'Ball%sIk%sJnt' %( elem , side ) , ballTmpJnt )
			self.toeIkJnt = rt.createJnt( 'Toe%sIk%sJnt' %( elem , side ) , toeTmpJnt )
			self.toeIkJnt.parent( self.ballIkJnt )
			self.ballIkJnt.parent( self.ankleIkJnt )

		#-- Create Controls Ik
		self.upLegIkCtrl = rt.createCtrl( 'UpLeg%sIk%sCtrl' %( elem , side ) , 'cube' , 'blue' , jnt = True )
		self.upLegIkGmbl = rt.addGimbal( self.upLegIkCtrl )
		self.upLegIkZro = rt.addGrp( self.upLegIkCtrl )
		self.upLegIkOfst = rt.addGrp( self.upLegIkCtrl , 'Ofst' )
		self.upLegIkPars = rt.addGrp( self.upLegIkCtrl , 'Pars' )
		self.upLegIkZro.snapPoint( self.upLegIkJnt )
		self.upLegIkCtrl.snapJntOrient( self.upLegIkJnt )

		self.ankleIkCtrl = rt.createCtrl( 'Ankle%sIk%sCtrl' %( elem , side ) , 'cube' , 'blue' , jnt = True )
		self.ankleIkGmbl = rt.addGimbal( self.ankleIkCtrl )
		self.ankleIkZro = rt.addGrp( self.ankleIkCtrl )
		self.ankleIkOfst = rt.addGrp( self.ankleIkCtrl , 'Ofst' )
		self.ankleIkPars = rt.addGrp( self.ankleIkCtrl , 'Pars' )
		self.ankleIkZro.snapPoint( self.ankleIkJnt )
		self.ankleIkCtrl.snapJntOrient( self.ankleIkJnt )

		self.kneeIkCtrl = rt.createCtrl( 'Knee%sIk%sCtrl' %( elem , side ) , 'sphere' , 'blue' , jnt = True )
		self.kneeIkZro = rt.addGrp( self.kneeIkCtrl )
		self.kneeIkZro.snapPoint( kneeTmpJnt )

		#-- Adjust Shape Controls Ik
		for ctrl in ( self.upLegIkCtrl , self.upLegIkGmbl , self.ankleIkCtrl , self.ankleIkGmbl) :
			ctrl.scaleShape( size )
		
		self.kneeIkCtrl .scaleShape( size*0.25 )

		#-- Adjust Rotate Order Ik
		for obj in ( self.upLegIkJnt , self.lowLegIkJnt , self.ankleIkJnt , self.upLegIkCtrl , self.ankleIkCtrl ) :
			obj.setRotateOrder( 'yzx' )
		
		if foot == True :
			for obj in ( self.ballIkJnt , self.toeIkJnt ) :
				obj.setRotateOrder( 'yzx' )

		#-- Rig process Ik
		self.ankleIkLocWor = rt.localWorld( self.ankleIkCtrl , ctrlGrp , self.upLegIkGmbl , self.ankleIkZro , 'parent' )
		self.kneeIkLocWor = rt.localWorld( self.kneeIkCtrl , ctrlGrp , self.ankleIkCtrl , self.kneeIkZro , 'parent' )

		#-- Prefered Ankle
		# utaCore.preferredAnkleLegIk(obj = [self.lowLegIkJnt], pfx = -45, pfy = 0, pfz = 0)
		# mc.setAttr ('%s.preferredAngleX' % self.lowLegIkJnt, -45) 

		self.crv = rt.drawLine( self.kneeIkCtrl , self.lowLegIkJnt )

		self.ankleIkhDict = rt.addIkh( 'Ankle%s' %elem , 'ikRPsolver' , self.upLegIkJnt , self.ankleIkJnt )
		
		self.ankleIkh = self.ankleIkhDict['ikh']
		self.ankleIkhZro = self.ankleIkhDict['ikhZro']

		self.polevector = mc.poleVectorConstraint( self.kneeIkCtrl , self.ankleIkh )[0]

		self.ankleIkCtrl.addAttr( 'twist' )

		self.ankleIkCtrl.attr( 'twist' ) >> self.ankleIkh.attr('twist')

		mc.pointConstraint( self.upLegIkGmbl , self.upLegIkJnt , mo = False )
		
		if not foot == True :
			mc.parentConstraint( self.ankleIkGmbl , self.ankleIkhZro , mo = True )

		self.ikStrt = rt.addIkStretch( self.ankleIkCtrl , self.kneeIkCtrl ,'ty' , 'Leg%s' %elem , self.upLegIkJnt , self.lowLegIkJnt , self.ankleIkJnt )
		
		self.ankleIkCtrl.attr('localWorld').value = 1
		self.ankleIkCtrl.attr('autoStretch').value = 1

		if foot == True :
			self.ankleIkCtrl.addAttr( 'toeStretch' )
			self.toeStrtMdv = rt.createNode( 'multiplyDivide' , 'Toe%sIkStrt%sMdv' %( elem , side ))
			self.toeStrtMdv.attr('i2x').value = valueSide

			self.toeStrtPma = rt.createNode( 'plusMinusAverage' , 'Toe%sIkStrt%sPma' %( elem , side ))
			self.toeStrtPma.addAttr( 'default' )
			self.toeStrtPma.attr('default').value = self.toeIkJnt.attr('ty').value

			self.ankleIkCtrl.attr( 'toeStretch' ) >> self.toeStrtMdv.attr('i1x')
			self.toeStrtPma.attr( 'default' ) >> self.toeStrtPma.attr('i1[0]')
			self.toeStrtMdv.attr( 'ox' ) >> self.toeStrtPma.attr('i1[1]')
			self.toeStrtPma.attr( 'o1' ) >> self.toeIkJnt.attr('ty')

			self.ballIkhDict = rt.addIkh( 'Ball%s' %elem , 'ikSCsolver' , self.ankleIkJnt , self.ballIkJnt )
			self.toeIkhDict = rt.addIkh( 'Toe%s' %elem , 'ikSCsolver' , self.ballIkJnt , self.toeIkJnt )
		
			self.ballIkh = self.ballIkhDict['ikh']
			self.ballIkhZro = self.ballIkhDict['ikhZro']

			self.toeIkh = self.toeIkhDict['ikh']
			self.toeIkhZro = self.toeIkhDict['ikhZro']

			self.ankleIkhZro.clearCons()
			self.ikStrt[2].clearCons()

			self.ankleIkCtrlShape = core.Dag(self.ankleIkCtrl.shape)

			mc.parentConstraint( self.ballPivCtrl , self.ankleIkhZro , mo = 1 )
			mc.parentConstraint( self.ballPivCtrl , self.ballIkhZro , mo = 1 )
			mc.parentConstraint( self.bendPivCtrl , self.toeIkhZro , mo = 1 )
			mc.parentConstraint( self.anklePivGrp , self.ikStrt[2] , mo = 1 )

			self.ankleIkCtrl.addTitleAttr( 'Foot' )
			self.ankleIkCtrl.addAttr( 'ballRoll' )
			self.ankleIkCtrl.addAttr( 'toeHeelRoll' )
			self.ankleIkCtrl.addAttr( 'heelTwist' )
			self.ankleIkCtrl.addAttr( 'toeTwist' )
			self.ankleIkCtrl.addAttr( 'toeBend' )
			self.ankleIkCtrl.addAttr( 'toeSide' )
			self.ankleIkCtrl.addAttr( 'toeSwirl' )
			self.ankleIkCtrl.addAttr( 'footRock' )
			self.ankleIkCtrlShape.addAttr( 'toeBreak' )
			self.ankleIkCtrlShape.attr('toeBreak').value = -90

			self.ballBreakCmp = rt.createNode( 'clamp' , 'Ball%sIkBreak%sCmp' %( elem , side ))

			self.toeBreakCnd = rt.createNode( 'condition' , 'Toe%sIkBreak%sCnd' %( elem , side ))
			self.toeBreakAmpMdv = rt.createNode( 'multiplyDivide' , 'Toe%sIkBreakAmp%sMdv' %( elem , side ))
			self.toeBreakAmpPma = rt.createNode( 'plusMinusAverage' , 'Toe%sIkBreakAmp%sPma' %( elem , side ))
			self.toeBreakPma = rt.createNode( 'plusMinusAverage' , 'Toe%sIkBreak%sPma' %( elem , side ))
			self.toeBreakCnd.attr('operation').value = 2
			self.toeBreakAmpMdv.attr('i2x').value = -1
			self.toeBreakCnd.attr('cfr').value = 0

			self.heelBreakCmp = rt.createNode( 'clamp' , 'Heel%sIkBreak%sCmp' %( elem , side ))
			self.heelBreakPma = rt.createNode( 'plusMinusAverage' , 'Heel%sIkBreak%sPma' %( elem , side ))
			self.heelBreakCmp.attr('maxR').value = 100

			self.ankleIkCtrl.attr( 'ballRoll' ) >> self.ballBreakCmp.attr('inputR')
			self.ankleIkCtrlShape.attr( 'toeBreak' ) >> self.ballBreakCmp.attr('minR')
			#self.ballBreakCmp.attr( 'outputR' ) >> self.ballPivGrp.attr('rx')


			self.ankleIkCtrlShape.attr( 'toeBreak' ) >> self.toeBreakAmpMdv.attr('i1x')
			self.toeBreakAmpMdv.attr( 'ox' ) >> self.toeBreakAmpPma.attr('i1[0]')
			#self.ankleIkCtrl.attr( 'ballRoll' ) >> self.toeBreakAmpPma.attr('i1[1]')
			self.toeBreakAmpPma.attr( 'o1' ) >> self.toeBreakCnd.attr('ctr')
			self.ankleIkCtrlShape.attr( 'toeBreak' ) >> self.toeBreakCnd.attr('ft')
			#self.ankleIkCtrl.attr( 'ballRoll' ) >> self.toeBreakCnd.attr('st')
			self.toeBreakCnd.attr( 'ocr' ) >> self.toeBreakPma.attr('i1[0]')
			self.toeBreakPma.attr( 'o1' ) >> self.toePivGrp.attr('rx')

			self.ankleIkCtrl.attr( 'ballRoll' ) >> self.heelBreakCmp.attr('inputR')
			self.heelBreakCmp.attr( 'outputR' ) >> self.heelBreakPma.attr('i1[0]')
			self.heelBreakPma.attr( 'o1' ) >> self.heelPivGrp.attr('rx')

			#self.ankleIkCtrl.attr( 'ballRoll' ) >> self.ballPivGrp.attr('rx')
			self.ankleIkCtrl.attr( 'toeHeelRoll' ) >> self.heelBreakPma.attr('i1[1]')
			self.ankleIkCtrl.attr( 'toeHeelRoll' ) >> self.toeBreakPma.attr('i1[1]')

			self.ankleIkCtrl.attr( 'heelTwist' ) >> self.heelTwistPivGrp.attr('rz')
			self.ankleIkCtrl.attr( 'toeTwist' ) >> self.toeTwistPivGrp.attr('rz')
			self.ankleIkCtrl.attr( 'toeBend' ) >> self.bendPivGrp.attr('rx')
			self.ankleIkCtrl.attr( 'toeSide' ) >> self.bendPivGrp.attr('rz')
			self.ankleIkCtrl.attr( 'toeSwirl' ) >> self.bendPivGrp.attr('ry')
			#self.ankleIkCtrl.attr( 'footRock' ) >> self.inPivGrp.attr('ry')
			#self.ankleIkCtrl.attr( 'footRock' ) >> self.outPivGrp.attr('ry')
			

			# create smart control
			self.smartToeCtrl = rt.createCtrl( 'Foot%sSmartIk%sCtrl' %( elem , side ) , 'cube' , 'red' , jnt = 0 )
			self.smartToeZro = rt.addGrp( self.smartToeCtrl )
			self.smartToeOfst = rt.addGrp( self.smartToeCtrl , 'Ofst' )
			
			self.smartToeZro.snapOrient(self.ballFkJnt)
			if side =='_R_':
				self.smartToeZro.pynode().sx.set(-1)
				self.smartToeZro.pynode().sy.set(-1)
			self.smartToeCtrl.pynode().tz.set(l=1,k=0)
			self.smartToeCtrl.pynode().v.set(l=1,k=0)
			for ax in 'xyz':
				self.smartToeCtrl.pynode().attr('r%s'%ax).set(l=1,k=0)
				self.smartToeCtrl.pynode().attr('s%s'%ax).set(l=1,k=0)

			# create math utility node for smart toe system
			self.smartAmp_mdv = rt.createNode('multiplyDivide', 'Foot%sSmartIkAmp%sMdv' %( elem , side ) )
			self.smartToeCtrl.getShape().addAttr('footSmartAmp',k=1,dv=30)

			self.ball_pma =  rt.createNode( 'plusMinusAverage' , 'Ball%sSmartIk%sPma' %( elem , side ))
			self.footRock_pma = rt.createNode( 'plusMinusAverage' , 'FootRock%sSmartIk%sPma' %( elem , side ))
			self.smart_mdv = rt.createNode( 'multiplyDivide' , 'Foot%sSmartIk%sMdv' %( elem , side ))

			# do smart toe rock
			# connect smart control to smart mdv
			self.smartToeCtrl.attr('ty') >> self.smart_mdv.attr('input1X')
			# set smart mdv attr
			self.smartToeCtrl.getShape().attr('footSmartAmp') >> self.smartAmp_mdv.pynode().input1X
			self.smartAmp_mdv.pynode().input2X.set(1)
			self.smartAmp_mdv.pynode().outputX >> self.smart_mdv.pynode().attr('input2X')
			#self.smart_mdv.pynode().attr('input2X').set(9)
			
			# connect to smart ball
			self.smart_mdv.attr('outputX') >>  self.ball_pma.attr('input1D[0]')
			self.ankleIkCtrl.attr('ballRoll') >>  self.ball_pma.attr('input1D[1]')

			# create ball utility pma
			self.ball_mdv = rt.createNode( 'multiplyDivide' , 'Ball%sSmartIk%sMdv' %( elem , side ))
			self.ball_pma.attr('output1D') >>  self.ball_mdv.attr('input1X')
			self.ball_mdv.pynode().attr('input2X').set(-1)

			# connect clamp to prevent ball ik negative rot
			self.ball_cmp = rt.createNode( 'clamp', 'Ball%sSmartIk%sCmp'%(elem, side))
			self.ball_mdv.attr('outputX') >> self.ball_cmp.attr('inputR')
			self.ball_mdv.attr('outputX') >> self.ball_cmp.attr('minR')

			# add ballPiv Limit to Ctrl and connect to cmp node.
			self.ankleIkCtrlShape.addAttr('BallPivLimit')
			self.ankleIkCtrlShape.attr('BallPivLimit') >> self.ball_cmp.attr('maxR')

			# connect to ballPivGrp and Toe Break system
			self.ball_cmp.attr('outputR')  >> self.ballPivGrp.attr('rx')
			self.ball_mdv.attr('outputX')  >> self.heelBreakPma.attr('input1D[2]')
			self.ball_mdv.attr('outputX')  >> self.toeBreakAmpPma.attr('input1D[1]')
			self.ball_mdv.attr('outputX')  >> self.toeBreakCnd.attr ('secondTerm')
			
			# do smart foot rock
			#connect tx of smart ctrl to footRock_pma
			self.smartToeCtrl.getShape().attr('footSmartAmp') >> self.smartAmp_mdv.pynode().input1Y
			self.smartAmp_mdv.pynode().input2Y.set(1)
			self.smartAmp_mdv.pynode().outputY >> self.smart_mdv.pynode().attr('input2Y')
			#self.smart_mdv.pynode().attr('input2Y').set(-9)
			self.smartToeCtrl.attr('tx') >>  self.smart_mdv.pynode().input1Y
			self.smart_mdv.attr('outputY') >> self.footRock_pma.attr('input1D[0]')
			self.ankleIkCtrl.attr('footRock') >>  self.footRock_pma.attr('input1D[1]')
			self.footRock_pma.attr('output1D') >>  self.inPivGrp.attr('ry')
			self.footRock_pma.attr('output1D') >>  self.outPivGrp.attr('ry')

			mc.transformLimits( self.inPivGrp , ery = ( True , False ) , ry = ( 0 , 0 ))
			mc.transformLimits( self.outPivGrp , ery = ( False , True ) , ry = ( 0 , 0 ))

			mc.transformLimits( self.toePivGrp , erx = ( False , True ) , rx = ( 0 , 0 ))
			mc.transformLimits( self.heelPivGrp , erx = ( True , False ) , rx = ( 0 , 0 ))

			ankle_t = mc.xform(ankleTmpJnt,ws=1,q=1,t=1)
			ball_t = mc.xform(ballTmpJnt,ws=1,q=1,t=1)

			ankleVec = om.MVector(ankle_t[0],ankle_t[1],ankle_t[2])

			ballVec = om.MVector(ball_t[0],ball_t[1],ball_t[2])

			newVec = ballVec- ankleVec

			mc.xform(self.smartToeZro.name,ws=1,t=[newVec[0]+ankle_t[0],-newVec[1],newVec[2]])

			self.smartToeZro.parent(self.ankleIkGmbl)


		#-- Adjust Hierarchy Ik
		mc.parent( self.legIkCtrlGrp , self.legCtrlGrp )
		mc.parent( self.legIkJntGrp , self.legJntGrp )
		mc.parent( self.legIkIkhGrp , ikhGrp )
		mc.parent( self.upLegIkZro , self.ikStrt[0] , self.crv , self.legIkCtrlGrp )
		mc.parent( self.ankleIkZro , self.kneeIkZro , self.upLegIkGmbl )
		mc.parent( self.ankleIkhZro , self.legIkIkhGrp )

		if foot == True :
			mc.parent( self.footPivGrp , self.ankleIkGmbl )
			mc.parent( self.ballIkhZro , self.toeIkhZro , self.legIkIkhGrp )

		#-- Blending Fk Ik 
		rt.blendFkIk( self.legCtrl , self.upLegFkJnt , self.upLegIkJnt , self.upLegJnt )
		rt.blendFkIk( self.legCtrl , self.lowLegFkJnt , self.lowLegIkJnt , self.lowLegJnt )
		rt.blendFkIk( self.legCtrl , self.ankleFkJnt , self.ankleIkJnt , self.ankleJnt )
		
		if foot == True :
			rt.blendFkIk( self.legCtrl , self.ballFkJnt , self.ballIkJnt , self.ballJnt )
			rt.blendFkIk( self.legCtrl , self.toeFkJnt , self.toeIkJnt , self.toeJnt )
		
		if mc.objExists( 'Leg%sFkIk%sRev' %( elem , side )) :
			self.revFkIk = core.Dag('Leg%sFkIk%sRev' %( elem , side ))

			self.legCtrl.attr( 'fkIk' ) >> self.legIkCtrlGrp.attr('v')
			self.revFkIk.attr( 'ox' ) >> self.legFkCtrlGrp.attr('v')

			self.legCtrl.attr('fkIk').value = 1

		#-- Scale Foot
		if foot == True :
			sclAnkle = core.Dag(mc.scaleConstraint( self.ankleFkJnt , self.ankleIkJnt , self.ankleJnt , mo = 0 )[0])
			sclBall = core.Dag(mc.scaleConstraint( self.ballFkJnt , self.ballIkJnt , self.ballJnt , mo = 0 )[0])
			sclToe = core.Dag(mc.scaleConstraint( self.toeFkJnt , self.toeIkJnt , self.toeJnt , mo = 0 )[0])
			
			self.legCtrl.attr('fkIk') >> sclAnkle.attr('%sW1' %self.ankleIkJnt)
			self.revFkIk.attr('ox') >> sclAnkle.attr('%sW0' %self.ankleFkJnt)
			self.legCtrl.attr('fkIk') >> sclBall.attr('%sW1' %self.ballIkJnt)
			self.revFkIk.attr('ox') >> sclBall.attr('%sW0' %self.ballFkJnt)
			self.legCtrl.attr('fkIk') >> sclToe.attr('%sW1' %self.toeIkJnt)
			self.revFkIk.attr('ox') >> sclToe.attr('%sW0' %self.toeFkJnt)

			self.legCtrl.addAttr('footScale')
			self.legCtrl.attr('footScale').value = 1
			self.ankleJnt.attr('segmentScaleCompensate').value = 0
			self.ballJnt.attr('segmentScaleCompensate').value = 0
			self.ballFkJnt.attr('segmentScaleCompensate').value = 0
			self.ballIkJnt.attr('segmentScaleCompensate').value = 0
			self.toeJnt.attr('segmentScaleCompensate').value = 0
			self.toeFkJnt.attr('segmentScaleCompensate').value = 0
			self.toeIkJnt.attr('segmentScaleCompensate').value = 0

			self.legCtrl.attr('footScale') >> self.ballFkSclOfst.attr('sx')
			self.legCtrl.attr('footScale') >> self.ballFkSclOfst.attr('sy')
			self.legCtrl.attr('footScale') >> self.ballFkSclOfst.attr('sz')

			self.legCtrl.attr('footScale') >> self.ankleFkJnt.attr('sx')
			self.legCtrl.attr('footScale') >> self.ankleFkJnt.attr('sy')
			self.legCtrl.attr('footScale') >> self.ankleFkJnt.attr('sz')

			self.legCtrl.attr('footScale') >> self.footPivGrp.attr('sx')
			self.legCtrl.attr('footScale') >> self.footPivGrp.attr('sy')
			self.legCtrl.attr('footScale') >> self.footPivGrp.attr('sz')

			self.legCtrl.attr('footScale') >> self.ankleIkJnt.attr('sx')
			self.legCtrl.attr('footScale') >> self.ankleIkJnt.attr('sy')
			self.legCtrl.attr('footScale') >> self.ankleIkJnt.attr('sz')

		#-- Cleanup Fk
		for grp in ( self.legFkCtrlGrp , self.legFkJntGrp , self.upLegFkZro , self.upLegFkOfst , self.lowLegFkZro , self.lowLegFkOfst , self.ankleFkZro , self.ankleFkOfst ) :
			grp.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
		
		for ctrl in ( self.upLegFkCtrl , self.upLegFkGmbl , self.lowLegFkCtrl , self.lowLegFkGmbl , self.ankleFkCtrl , self.ankleFkGmbl ) :
			ctrl.lockHideAttrs( 'sx' , 'sy' , 'sz' , 'v' )
			
		if foot == True :
			for grp in ( self.ballFkZro , self.ballFkOfst , self.ballFkSclOfst ) :
				grp.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
				
			for ctrl in ( self.ballFkCtrl , self.ballFkGmbl ) :
				ctrl.lockHideAttrs( 'sx' , 'sy' , 'sz' , 'v' )

		#-- Cleanup Ik
		for grp in ( self.legIkCtrlGrp , self.legIkJntGrp , self.legIkIkhGrp , self.upLegIkZro , self.ankleIkZro ) :
			grp.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
		
		for ctrl in ( self.upLegIkCtrl , self.upLegIkGmbl , self.kneeIkCtrl , self.ankleIkCtrl , self.ankleIkGmbl ) :
			ctrl.lockHideAttrs( 'sx' , 'sy' , 'sz' , 'v' )
			
		if foot == True :
			for grp in ( self.footPivGrp , self.ballPivGrp , self.bendPivGrp , self.heelPivGrp , self.heelTwistPivGrp , self.toePivGrp , self.toeTwistPivGrp , self.inPivGrp , self.outPivGrp ) :
				grp.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

		#-- Ribbon
		if ribbon == True :
			self.upLegPar   = self.upLegJnt.getParent()
			self.lowLegPar  = self.lowLegJnt.getParent()
			self.anklePar   = self.ankleJnt.getParent()

			#-- Create Controls Ribbon
			self.legRbnCtrl = rt.createCtrl( 'Leg%sRbn%sCtrl' %( elem , side ) , 'locator' , 'yellow' , jnt = True )
			self.legRbnZro = rt.addGrp( self.legRbnCtrl )
			self.legRbnPars = rt.addGrp( self.legRbnCtrl , 'Pars' )
			self.legRbnZro.snapPoint( self.lowLegJnt )
			self.legRbnCtrl.snapJntOrient( self.lowLegJnt )
			mc.parent( self.legRbnZro , self.legCtrlGrp )

			#-- Adjust Shape Controls Ribbon
			self.legRbnCtrl.scaleShape( size )

			#-- Rig process Ribbon
			mc.parentConstraint( self.lowLegJnt , self.legRbnZro , mo = True )

			distRbnUpLeg = rt.getDist( self.upLegJnt , self.lowLegJnt )
			distRbnLowLeg = rt.getDist( self.lowLegJnt , self.ankleJnt )

			if hi:
				self.rbnUpLeg = ribbonRig.RibbonHi(  name = 'UpLeg%s' %elem , 
													 axis = 'y-' ,
													 side = sideRbn ,
													 dist = distRbnUpLeg )
				
				self.rbnLowLeg = ribbonRig.RibbonHi(  name = 'LowLeg%s' %elem , 
													  axis = 'y-' ,
													  side = sideRbn ,
													  dist = distRbnLowLeg )
			else:
				self.rbnUpLeg = ribbonRig.RibbonMid(  name = 'UpLeg%s' %elem , 
													 axis = 'y-' ,
													 side = sideRbn ,
													 dist = distRbnUpLeg )
				
				self.rbnLowLeg = ribbonRig.RibbonMid(  name = 'LowLeg%s' %elem , 
													  axis = 'y-' ,
													  side = sideRbn ,
													  dist = distRbnLowLeg )

			self.rbnUpLeg.rbnCtrlGrp.snapPoint(self.upLegJnt)
	
			mc.delete( 
						   mc.aimConstraint( self.lowLegJnt , self.rbnUpLeg.rbnCtrlGrp , 
											 aim = (0,-1,0) ,
											 u = upVec ,
											 wut='objectrotation' ,
											 wuo = self.upLegJnt ,
											 wu= (0,0,1))
					 )
	
			mc.parentConstraint( self.upLegJnt , self.rbnUpLeg.rbnCtrlGrp , mo = True )
			mc.parentConstraint( self.upLegJnt , self.rbnUpLeg.rbnRootCtrl , mo = True )
			mc.parentConstraint( self.legRbnCtrl , self.rbnUpLeg.rbnEndCtrl , mo = True )

			for ctrl in ( self.rbnUpLeg.rbnRootCtrl , self.rbnUpLeg.rbnEndCtrl) :
				ctrl.lockHideAttrs('tx', 'ty', 'tz' , 'rx' , 'ry' , 'rz' )
				ctrl.hide()

			self.rbnLowLeg.rbnCtrlGrp.snapPoint(self.lowLegJnt)
	
			mc.delete( 
						   mc.aimConstraint( self.ankleJnt , self.rbnLowLeg.rbnCtrlGrp , 
											 aim = (0,-1,0) ,
											 u = upVec ,
											 wut='objectrotation' ,
											 wuo = self.lowLegJnt ,
											 wu= (0,0,1))
					 )
	
			mc.parentConstraint( self.lowLegJnt , self.rbnLowLeg.rbnCtrlGrp , mo = True )
			mc.parentConstraint( self.legRbnCtrl , self.rbnLowLeg.rbnRootCtrl , mo = True )
			mc.parentConstraint( self.ankleJnt , self.rbnLowLeg.rbnEndCtrl , mo = True )

			for ctrl in ( self.rbnLowLeg.rbnRootCtrl , self.rbnLowLeg.rbnEndCtrl) :
				ctrl.lockHideAttrs('tx', 'ty', 'tz' , 'rx' , 'ry' , 'rz' )
				ctrl.hide()
	
			#-- Adjust Ribbon
			self.rbnUpLegCtrlShape = core.Dag(self.rbnUpLeg.rbnCtrl.shape)
			self.rbnLowLegCtrlShape = core.Dag(self.rbnLowLeg.rbnCtrl.shape)

			self.rbnUpLegCtrlShape.attr('rootTwistAmp').value = rootTwsAmp
			self.rbnUpLegCtrlShape.attr('endTwistAmp').value = endTwsAmp
			self.rbnLowLegCtrlShape.attr('rootTwistAmp').value = rootTwsAmp
			self.rbnLowLegCtrlShape.attr('endTwistAmp').value = endTwsAmp
						   
			#-- Twist Distribute Ribbon
			self.upLegNonRollTwistGrp.attr('ry') >> self.rbnUpLegCtrlShape.attr('rootAbsoluteTwist')
			self.ankleJnt.attr('ry') >> self.rbnLowLegCtrlShape.attr('endAbsoluteTwist')

			#-- Adjust Hierarchy Ribbon
			mc.parent( self.rbnUpLeg.rbnCtrlGrp , self.rbnLowLeg.rbnCtrlGrp , self.legCtrlGrp )
			mc.parent( self.rbnUpLeg.rbnJntGrp , self.rbnLowLeg.rbnJntGrp , self.legJntGrp )
			mc.parent( self.rbnUpLeg.rbnSkinGrp , self.rbnLowLeg.rbnSkinGrp , skinGrp )
			mc.parent( self.rbnUpLeg.rbnStillGrp , self.rbnLowLeg.rbnStillGrp , stillGrp )
		
			self.rbnUpLeg.rbnCtrl.attr('autoTwist').value = 1
			self.rbnLowLeg.rbnCtrl.attr('autoTwist').value = 1
		
		if kneePos:	
			locatorObj = lrc.Locator()
			kneePosLoc = mc.rename(locatorObj , 'KneePos{}{}loc'.format(elem,side))
			kneePosGrp = mc.group(n = 'KneePos{}{}Grp'.format(elem,side), em = True)
			mc.parent(kneePosLoc, kneePosGrp)
			mc.parent(kneePosGrp, stillGrp)
			utaCore.snapObj(kneePos, kneePosGrp)

			## clear old Node and Attribute
			utaCore.lockAttrObj(listObj =['LowLeg{}RbnRoot{}Ctrl'.format(elem,side)], lock = False,keyable = True)
			mc.delete('LowLeg{}RbnRoot{}Ctrl_parentConstraint1'.format(elem,side))

			## create Node 
			clampNode = mc.createNode ('clamp', n = '{}{}Cmp'.format('KneePos', elem,side))
			reverseNode = mc.createNode('reverse', n = '{}{}{}Rev'.format('KneePos',elem, side))
			multiANode = mc.createNode('multiplyDivide', n = '{}{}{}Mdv'.format('KneePosA',elem, side))
			multiAmpNode = mc.createNode('multiplyDivide', n = '{}{}{}Mdv'.format('KneePosAmp',elem, side))

			## setAttr Node
			mc.setAttr('{}.minR'.format(clampNode), -135)
			mc.setAttr('{}.maxR'.format(clampNode), -80)
			mc.setAttr('{}.operation'.format(multiANode),2)

			## new parent
			if mc.objExists('UpLeg5{}RbnDtl{}Jnt'.format(elem,side)):
				mc.parentConstraint('UpLeg5{}RbnDtl{}Jnt'.format(elem,side), kneePosGrp, mo = True)
			else:
				mc.parentConstraint('UpLeg{}{}Jnt'.format(elem,side), kneePosGrp, mo = True)
			mc.parentConstraint('Leg{}Rbn{}Ctrl'.format(elem,side), kneePosLoc, 'LowLeg{}RbnRoot{}Ctrl'.format(elem,side), mo = False)

			## addAttr kneePosAmp
			AnkleCtrl = 'Leg{}Rbn{}Ctrl'.format(elem,side)
			utaCore.addAttrCtrl (ctrl = '{}Shape'.format(AnkleCtrl), elem = ['KneePosAmp'], min = -200, max = 200, at = 'float')
			mc.setAttr('{}Shape.KneePosAmp'.format(AnkleCtrl),0.5)

			## connect Attr
			mc.connectAttr('LowLeg{}{}Jnt.rx'.format(elem,side), '{}.inputR'.format(clampNode))
			mc.connectAttr('{}.outputR'.format(clampNode), '{}.input2X'.format(multiANode))
			mc.connectAttr('LowLeg{}{}Jnt.rx'.format(elem,side), '{}.input1X'.format(multiANode))
			mc.connectAttr('{}Shape.KneePosAmp'.format(AnkleCtrl), '{}.input2X'.format(multiAmpNode))
			mc.connectAttr('{}.outputX'.format(multiANode), '{}.input1X'.format(multiAmpNode))
			mc.connectAttr('{}.outputX'.format(multiAmpNode), 'LowLeg{}RbnRoot{}Ctrl_parentConstraint1.KneePos{}{}locW1'.format(elem,side,elem, side))
			mc.connectAttr('{}.outputX'.format(multiAmpNode), '{}.inputX'.format(reverseNode))
			mc.connectAttr('{}.outputX'.format(reverseNode), 'LowLeg{}RbnRoot{}Ctrl_parentConstraint1.Leg{}Rbn{}CtrlW0'.format(elem,side,elem, side))
			
			utaCore.lockAttrObj(listObj =['LowLeg{}RbnRoot{}Ctrl'.format(elem,side)], lock = True,keyable = False)


		mc.select( cl = True )