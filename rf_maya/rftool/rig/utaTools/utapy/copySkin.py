import maya.cmds as mc
import maya.mel as mm 
from utaTools.utapy import utaCore
reload(utaCore)
from utaTools.pkmel import weightTools
reload(weightTools)


# from utaTools.utapy import copySkin
# reload(copySkin)
# copySkin.copySkinFromTo(	geoGrp = 'headLeftGeo_Grp',
							# fromSideGeo = 'Left', 
							# toSideGeo = 'Right', 
							# fromSideJnt = 'Lft', 
							# toSideJnt = 'Rht',
							# influnce = True)



def copySkinFromTo(geoGrp = '',fromSideGeo = '', toSideGeo = '', fromSideJnt = '', toSideJnt = '', influnce = True,*args):
	listsGeo = utaCore.listGeo(sels = geoGrp, nameing = '_Geo', namePass = '_Grp')
	
	listJntSideRht = []
	fromSideObj = []
	nonFromSideGeo =[]
	nonSkinWeight = []
	for i in range(len(listsGeo)):

		selObj = listsGeo[i].split('|')[-1]
		## check side in obj:
		if fromSideGeo in selObj:
			fromSideObj.append(selObj)
		else:
			nonFromSideGeo.append(listsGeo[i])
	## replace side of geo
	for eachObj in fromSideObj:
		name, side, lastName = utaCore.splitName(sels = [eachObj])
		objA = eachObj
		objB = '{}{}{}'.format((name.replace(fromSideGeo, toSideGeo)),side, lastName)

		## check and addInfluence joint
		skinNode = utaCore.getSkinCluster(mesh = [eachObj])

		if skinNode:
			listsJnt = mc.skinCluster(skinNode, q = True, inf = True)
			for eachJnt in listsJnt:
				if fromSideJnt in eachJnt:
					## replace name of jnt
					jnt = eachJnt.replace(fromSideJnt, toSideJnt)
					listJntSideRht.append(jnt)
			try:
				# if not toSideJnt in jnt:
				utaCore.bindSkinCluster(jntObj = listJntSideRht, obj = [eachObj])
			except:pass

			## copySkin L to R
			mc.select(objA)
			mc.select(objB, add = True)
			weightTools.copySelectedWeight()
			## mirror Skin L to R
			mc.select(objA)
			mc.select(objB, add = True)
			mm.eval('doMirrorSkinWeightsArgList( 2, { " -mirrorMode YZ -surfaceAssociation closestPoint -influenceAssociation closestJoint" })')

			## remove unuse influnce 
			if influnce:
				geoObj = [objA, objB]
				utaCore.remove_unused_influences(mesh = geoObj, targetInfluences=[])
			
		else:
			nonSkinWeight.append(eachObj)




	print 'Object All :', len(listsGeo)
	print 'None Skinweight :', len(nonSkinWeight), ':', nonSkinWeight