import maya.cmds as mc
from utaTools.utapy import utaCore
reload(utaCore)
import pymel.core as pm
import math
from utaTools.pkmel import rigTools as rigTools
reload( rigTools )
##--------------------------------------------------------------------------------
# import maya.cmds as mc
# from utaTools.utapy import dynFkRig
# reload(dynFkRig)
# dynFkRig.dynFkRig(tmpJnt = ['dynIkHair1_Jnt', 'dynIkHair2_Jnt', 'dynIkHair3_Jnt', 'dynIkHair4_Jnt', 'dynIkHair5_Jnt', 'dynIkHair6_Jnt'],
#                     curve = '', 
#                     parent = 'ctrl:HeadEnd_Jnt',
#                     name = 'Dyn', 
#                     elem = 'Hair',
#                     side = '',
#                     ctrlGrp = '',
#                     skinGrp = '',
#                     ikhGrp = '',
#                     jntGrp = '',
#                     stillGrp = '')
##--------------------------------------------------------------------------------

def dynFkRig(tmpJnt = [],curve = '', parent = '', name = '', elem = '', side = '', ctrlGrp = '', skinGrp = '', ikhGrp = '', jntGrp = '', stillGrp = '',*args):
    fkCtrlGrp = []
    fkGmblCtrlGrp = []
    positionJnt = []
    lenPositionJnt = len(positionJnt)
    objDrvJnt = []
    objCtrl = []

    if side == 'L':
        side = '_L_'
    elif side == 'R':
        side = '_R_'
    elif side == '':
        side = '_'

    ## Group
    # objJntGrp= mc.group(n = '{}{}Jnt{}Grp'.format(name, elem, side), em = True)
    objCtrlGrp = mc.group(n = '{}{}Ctrl{}Grp'.format(name, elem, side), em = True)
    objIkhGrp= mc.group(n = '{}{}Ikh{}Grp'.format(name, elem, side), em = True)
    objStillGrp = mc.group(n = '{}{}Still{}Grp'.format(name, elem, side), em = True)

    ## Generate Curve spine Ik
    if not curve:
        value = []
        for jnt in tmpJnt:
            positionJnt = mc.xform( jnt , q = True, t = True , ws = True)
            value.append(tuple(positionJnt))
        curve = mc.curve( p=value,d = 3, n = '{}{}{}Crv'.format(name, elem, side))

    ## duplicate curve
    crvGoal= mc.duplicate(curve, n= '{}{}Goal{}Crv'.format(name,elem,  side))[0]
    crvParticle= mc.duplicate(curve, n= '{}{}Particle{}Crv'.format(name,elem,  side))[0]
    ## blendShape
    mc.select(crvGoal, r = True)   
    mc.select(crvParticle, add= True)
    mc.select(curve, add= True)
    bshLists = mc.blendShape(name = '{}{}{}Bsh'.format(name, elem, side))

    ## Create spineIk
    spineIkName = '{}Ik_Ikh'.format(elem.lower())
    utaCore.createIkhSpine (strJnt = tmpJnt[0], endJnt = tmpJnt[-1] ,curve = curve , name = spineIkName, sol = 'ikSplineSolver')
    ''' ikSplineSolver
        ikRPsolver
        ikSCsolver'''

    ## generate ik Control
    crvFkCtrlPos= mc.duplicate(curve, n= '{}fkCtrlPos{}Crv'.format(name, side))[0]
    fkCtrlCount = len(tmpJnt)
    print fkCtrlCount, '..fkCtrlCount..In'
    jointFnt, jointBck, jointAll = utaCore.createJointOnCurveNeedJoint(cur_name = crvFkCtrlPos, name = '{}{}{}Jnt'.format(name, elem, side), elem = '', side = side,  span_curve = fkCtrlCount, degree = 3, needJoint = 3)
    for i in range(len(jointAll)):
        nameNew, sideNew, lastNameNew = utaCore.splitName(sels = [jointAll[i]])
        if sideNew == '_L_':
            sideNew == 'L'
        elif sideNew == '_R_':
            sideNew == 'R'
        else:
            sideNew = '_'
        objNew = mc.rename(jointAll[i], '{}{}Drv{}{}Jnt'.format(nameNew, elem, i+1, sideNew))
        mc.select(objNew)
        grpCtrlName, grpCtrlOfsName, grpCtrlParsName, ctrlName, GmblCtrlName = utaCore.createControlCurve(curveCtrl = 'circle', parentCon = False, connections = False, geoVis = False)
        fkCtrlGrp.append(grpCtrlName)
        fkGmblCtrlGrp.append(GmblCtrlName)
        objDrvJnt.append(objNew)
        objCtrl.append(ctrlName)
    mc.delete(crvFkCtrlPos)

    ## parent joint Drv
    for i in range(len(fkCtrlGrp)):
        if not i == 0:
            mc.parent(fkCtrlGrp[i], fkGmblCtrlGrp[i-1])
        mc.parent(objDrvJnt[i], fkGmblCtrlGrp[i])  
    ## bindSkin obj Drv jnt
    utaCore.bindSkinCluster(jntObj = objDrvJnt, obj = [crvGoal])

    ## Create Attribute
    onOffAttr = utaCore.addAttrCtrl (ctrl = objCtrl[0], elem = ['dynamicBlendOnOff'], min = 0, max = 1, at = 'long', dv = 0)[0]
    mc.connectAttr('{}.{}'.format(objCtrl[0], onOffAttr),  '{}.{}'.format(bshLists[0], crvParticle))
    particleRev = mc.createNode('reverse', n = '{}{}Rev'.format(name, side))
    mc.connectAttr('{}.{}'.format(objCtrl[0], onOffAttr),  '{}.inputX'.format(particleRev))
    mc.connectAttr('{}.outputX'.format(particleRev), '{}.{}'.format(bshLists[0], crvGoal))

    ## Create soft body 
    CrvParticleNode = mc.soft(crvParticle, c = True )[0]
    mc.select(CrvParticleNode)

    ## Create turbulenceNode
    # turbulenceNode = mc.turbulence(pos= (0, 0 , 0), m= 5 ,att= 1 ,f= 1,phaseX= 0 ,phaseY =0 ,phaseZ= 0 ,noiseLevel= 0 ,noiseRatio= 0.707,mxd= -1,  vsh= 'none' ,vex= 0 ,vof = (0, 0, 0),vsw =360 ,tsr= 0.5 )[0]
    # mc.connectDynamic (CrvParticle, f = turbulenceNode)
    # mc.setAttr('{}.magnitude'.format(turbulenceNode), 2000)

    ## Generate attribute conserve
    goalSmoothnessAttr = utaCore.addAttrCtrl (ctrl = objCtrl[0], elem = ['goalSmoothness'], min = 0, max = 100, at = 'float', dv = 10)[0]
    goalSmoothnessMdv = mc.createNode('multiplyDivide', n = '{}{}GoalSmoothness{}Mdv'.format(name, elem, side))
    mc.connectAttr('{}.{}'.format(objCtrl[0], goalSmoothnessAttr), '{}.input1X'.format(goalSmoothnessMdv))
    mc.setAttr('{}.input2X'.format(goalSmoothnessMdv), 0.1)
    mc.connectAttr('{}.outputX'.format(goalSmoothnessMdv), '{}Shape.goalSmoothness'.format(CrvParticleNode))


    ## Create Goal
    mc.goal(CrvParticleNode, g = crvGoal, w= 0.5 ,utr= 0)
		
    ## Create remap Node
    arrayMapperNode = mc.arrayMapper(target = '{}Shape'.format(CrvParticleNode), destAttr = 'goalPP' , inputV = 'ageNormalized', type = 'ramp')[0]
    mc.disconnectAttr('{}Shape.ageNormalized'.format(CrvParticleNode), '{}.vCoordPP'.format(arrayMapperNode))
    mc.setAttr('{}Shape.lifespanMode'.format(CrvParticleNode), 0)
    ## reverse map ramp1 left = white, right = black
    mc.setAttr('ramp1.colorEntryList[1].position', 0)
    mc.setAttr('ramp1.colorEntryList[0].position', 1)
    mc.setAttr("ramp1.colorEntryList[1].color", 1 ,1 ,1,type = 'double3')
    mc.setAttr("ramp1.colorEntryList[0].color", 0 ,0 ,0,type = 'double3')
    mc.connectAttr('{}Shape.mass'.format(CrvParticleNode), '{}.vCoordPP'.format(arrayMapperNode))

    ## SetAttribute pt particle
    cvPoint = len(mc.getAttr('{}.cv[*]'.format(crvParticle)))
    for i in range(0,cvPoint):
        if i == 0:
            pointFirst = 0.001
            mc.particle('{}Shape'.format(CrvParticleNode), e = True, order = i , at = "mass" , fv= pointFirst)
        else:
            value = cvPoint - 1
            answer = float(i)/value
            mc.particle('{}Shape'.format(CrvParticleNode),e = True,order = i ,at = "mass", fv= answer)

    ## Generate attribute goal min weight
    goalMinweightAttr = utaCore.addAttrCtrl (ctrl = objCtrl[0], elem = ['goalMinWeight'], min = 0, max = 100, at = 'float', dv = 20)[0]
    goalMinWeightMdv = mc.createNode('multiplyDivide', n = '{}{}GoalMinWeight{}Mdv'.format(name, elem, side))
    mc.connectAttr('{}.{}'.format(objCtrl[0], goalMinweightAttr), '{}.input1X'.format(goalMinWeightMdv))
    mc.setAttr('{}.input2X'.format(goalMinWeightMdv), 0.01)
    mc.connectAttr('{}.outputX'.format(goalMinWeightMdv), '{}.minValue'.format(arrayMapperNode))
    mc.connectAttr('{}.{}'.format(objCtrl[0], onOffAttr), '{}Shape.isDynamic'.format(CrvParticleNode))

    ## Generate attribute conserve
    conserveAttr = utaCore.addAttrCtrl (ctrl = objCtrl[0], elem = ['conserve'], min = 0, max = 100, at = 'float', dv = 75)[0]
    conserveMdv = mc.createNode('multiplyDivide', n = '{}{}Conserve{}Mdv'.format(name, elem, side))
    mc.connectAttr('{}.{}'.format(objCtrl[0], conserveAttr), '{}.input1X'.format(conserveMdv))
    mc.setAttr('{}.input2X'.format(conserveMdv), 0.01)
    mc.connectAttr('{}.outputX'.format(conserveMdv), '{}Shape.conserve'.format(CrvParticleNode))

    ## wrap group
    if parent:
        utaCore.parentScaleConstraintLoop(obj = parent, lists = [objCtrlGrp],par = True, sca = True, ori = False, poi = False)
        mc.parent(tmpJnt[0], parent)
    mc.parent(fkCtrlGrp[0], objCtrlGrp)
    mc.parent(crvGoal, crvParticle, spineIkName, curve, objStillGrp)
    mc.parent(spineIkName, objIkhGrp)

    ## wrap to base grp 
    if ctrlGrp or ikhGrp or stillGrp:
        mc.parent(objCtrlGrp, ctrlGrp)
        mc.parent(objIkhGrp, ikhGrp)
        mc.parent(objStillGrp, stillGrp)

    ## set inheritsTransform
    listsInher = [objStillGrp]
    for each in listsInher:
        mc.setAttr('{}.inheritsTransform'.format(each), 0)
    mc.select(cl = True)



def dynFkRig2(tmpJnt = [],curve = '', parent = '', name = '', elem = '', side = '', ctrlGrp = '', skinGrp = '', ikhGrp = '', jntGrp = '', stillGrp = '',*args):
    fkCtrlGrp = []
    fkGmblCtrlGrp = []
    positionJnt = []
    lenPositionJnt = len(positionJnt)
    objDrvJnt = []
    objCtrl = []

    if side == 'L':
        side = '_L_'
    elif side == 'R':
        side = '_R_'
    elif side == '':
        side = '_'

    ## Group
    objSkinGrp= mc.group(n = '{}{}Skin{}Grp'.format(name, elem, side), em = True)
    objJntGrp= mc.group(n = '{}{}Jnt{}Grp'.format(name, elem, side), em = True)
    objCtrlGrp = mc.group(n = '{}{}Ctrl{}Grp'.format(name, elem, side), em = True)
    objIkhGrp= mc.group(n = '{}{}Ikh{}Grp'.format(name, elem, side), em = True)
    objStillGrp = mc.group(n = '{}{}Still{}Grp'.format(name, elem, side), em = True)

    ## Duplicate Joint 'Fk', 'Dyn'
    jntList = ['Fk', 'Dyn']

    fkJnt = []
    dynJnt = []
    for i in range(len(jntList)) :
        for x in range(len(tmpJnt)):
            nameNew, sideNew, lastNameNew = utaCore.splitName(sels = [tmpJnt[x]])
            if jntList[i] == 'Fk':
                fkJnt.append(mc.createNode('joint', n = '{}{}{}Jnt'.format(nameNew, jntList[i], sideNew)))
                mc.delete(mc.parentConstraint(tmpJnt[x], fkJnt[x], mo = False))
                if not x == 0:
                    mc.parent(fkJnt[x], fkJnt[x-1])

            elif jntList[i] == 'Dyn':
                dynJnt.append(mc.createNode('joint', n = '{}{}{}Jnt'.format(nameNew, jntList[i], sideNew)))
                mc.delete(mc.parentConstraint(tmpJnt[x], dynJnt[x], mo = False))
                if not x == 0:
                    mc.parent(dynJnt[x], dynJnt[x-1])

                mc.parentConstraint(dynJnt[x], tmpJnt[x], mo = True)
                mc.scaleConstraint(dynJnt[x], tmpJnt[x], mo = True)


    ## Generate Dynamic 
    if not curve:
        value = []
        for jnt in dynJnt:
            positionJnt = mc.xform( jnt , q = True, t = True , ws = True)
            value.append(tuple(positionJnt))
        curveDyn = mc.curve( p=value,d = 3, n = '{}{}{}Crv'.format(jntList[-1], elem, side))
        curveFk = mc.curve( p=value,d = 3, n = '{}{}{}Crv'.format(jntList[0], elem, side))

        ## Generate Dynamic curve
        pm.select(curveDyn)
        pm.mel.eval("MakeCurvesDynamic")
        if 'hairSystem1Follicles':
            system1Follicles = mc.rename('hairSystem1Follicles', '{}{}Folicles{}Grp'.format(name, elem, side))
            obj = mc.listRelatives(system1Follicles, c = True)
            if 'follicle1' in obj[0]:
                follicleObj = mc.rename('follicle1', '{}{}{}Flc'.format(name, elem, side))
                mc.setAttr('{}.pointLock'.format(follicleObj), 1)
        if 'hairSystem1OutputCurves':
            hairSymObj = mc.rename('hairSystem1OutputCurves', '{}{}OutputCrv{}Grp'.format(name, elem, side))
            crvDynOutputName = '{}{}Output{}Crv'.format(name, elem, side)
            objCrv = mc.rename(mc.listRelatives(hairSymObj, c = True)[0], crvDynOutputName)
        if 'hairSystem1':
            hairSym = mc.rename('hairSystem1', '{}{}System{}Syt'.format(name, elem, side))
        if 'nucleus1':
            nucleus = mc.rename('nucleus1', '{}{}Nucleus{}Ncl'.format(name, elem, side)) 

    ## generate ik Control
    crvFkCtrlPos= mc.duplicate(curveDyn, n= '{}fkCtrlPos{}Crv'.format(name, side))[0]
    mc.parent(crvFkCtrlPos, w = True)
    fkCtrlCount = len(tmpJnt)
    jointFnt, jointBck, jointAll = utaCore.createJointOnCurveNeedJoint(cur_name = crvFkCtrlPos, name = '{}{}{}Jnt'.format(name, elem, side), elem = '', side = side,  span_curve = fkCtrlCount, degree = 3, needJoint = 3)
    for i in range(len(jointAll)):
        nameNew, sideNew, lastNameNew = utaCore.splitName(sels = [jointAll[i]])
        if sideNew == '_L_':
            sideNew == 'L'
        elif sideNew == '_R_':
            sideNew == 'R'
        else:
            sideNew = '_'
        objNew = mc.rename(jointAll[i], '{}Drv{}{}Jnt'.format(nameNew, i+1, sideNew))
        mc.select(objNew)
        if not len(jointAll)-1 == i:
            grpCtrlName, grpCtrlOfsName, grpCtrlParsName, ctrlName, GmblCtrlName = utaCore.createControlCurve(curveCtrl = 'circle', parentCon = False, connections = False, geoVis = False)
        else:
            grpCtrlName, grpCtrlOfsName, grpCtrlParsName, ctrlName, GmblCtrlName = utaCore.createControlCurve(curveCtrl = 'cube', parentCon = False, connections = False, geoVis = False)

        fkCtrlGrp.append(grpCtrlName)
        fkGmblCtrlGrp.append(GmblCtrlName)
        objDrvJnt.append(objNew)
        objCtrl.append(ctrlName)
    mc.delete(crvFkCtrlPos)

    ## Colors
    utaCore.assignColor(obj = [objCtrl[-1]], col = 'green')

    ## Create Attribute
    utaCore.attrNameTitle(obj = objCtrl[-1], ln = 'dynFunctions')
    onOffAttr = utaCore.addAttrCtrl (ctrl = objCtrl[-1], elem = ['dynOnOff'], min = 0, max = 1, at = 'long', dv = 0)[0]
    # stretchResistanceAttr = utaCore.addAttrCtrl (ctrl = objCtrl[-1], elem = ['stretchResistance'], min = -1000, max = 1000, at = 'float', dv = 100)[0]
    # compressionResistanceAttr = utaCore.addAttrCtrl (ctrl = objCtrl[-1], elem = ['compressionResistance'], min = -1000, max = 1000, at = 'float', dv = 10)[0]
    startCurveAttractAttr = utaCore.addAttrCtrl (ctrl = objCtrl[-1], elem = ['startCurveAttract'], min = -1000, max = 1000, at = 'float', dv = 2)[0]
    attractionDampAttr = utaCore.addAttrCtrl (ctrl = objCtrl[-1], elem = ['attractionDamp'], min = -1000, max = 1000, at = 'float', dv = 0.35)[0]
    massAttr = utaCore.addAttrCtrl (ctrl = objCtrl[-1], elem = ['mass'], min = -1000, max = 1000, at = 'float', dv = 0.5)[0]
    dragAttr = utaCore.addAttrCtrl (ctrl = objCtrl[-1], elem = ['drag'], min = -1000, max = 1000, at = 'float', dv = 0.05)[0]
    # tangentialDragAttr = utaCore.addAttrCtrl (ctrl = objCtrl[-1], elem = ['tangentialDrag'], min = -1000, max = 1000, at = 'float', dv = 0.1)[0]
    motionDragAttr = utaCore.addAttrCtrl (ctrl = objCtrl[-1], elem = ['motionDrag'], min = -1000, max = 1000, at = 'float', dv = 0.8)[0]


    startFrameAttr = utaCore.addAttrCtrl (ctrl = objCtrl[-1], elem = ['startFrame'], min = -1000, max = 1000, at = 'float', dv = 0)[0]
    spaceScaleAttr = utaCore.addAttrCtrl (ctrl = objCtrl[-1], elem = ['spaceScale'], min = -1000, max = 1000, at = 'float', dv = 0.1)[0]
    gravityAttr = utaCore.addAttrCtrl (ctrl = objCtrl[-1], elem = ['gravity'], min = -1000, max = 1000, at = 'float', dv = 2)[0]
    airDensityAttr = utaCore.addAttrCtrl (ctrl = objCtrl[-1], elem = ['airDensity'], min = -1000, max = 1000, at = 'float', dv = 0)[0]
    windSpeedAttr = utaCore.addAttrCtrl (ctrl = objCtrl[-1], elem = ['windSpeed'], min = -1000, max = 1000, at = 'float', dv = 0)[0]
    windNoiseAttr = utaCore.addAttrCtrl (ctrl = objCtrl[-1], elem = ['windNoise'], min = -1000, max = 1000, at = 'float', dv = 0)[0]




    ## connect Visibility OnOff
    onOffCondition = mc.createNode('condition', n = '{}{}{}Con'.format(name, elem, side))
    mc.setAttr("{}.secondTerm".format(onOffCondition), 1)
    mc.setAttr("{}.colorIfTrueR".format(onOffCondition), 3)
    mc.setAttr("{}.colorIfFalseR".format(onOffCondition), 1)
    mc.connectAttr('{}.{}'.format(objCtrl[-1], onOffAttr),  '{}.firstTerm'.format(onOffCondition))
    mc.connectAttr('{}.outColorR'.format(onOffCondition),  '{}.simulationMethod'.format(hairSym))

    ## Connect Node Dynamic
    # mc.connectAttr('{}.{}'.format(objCtrl[-1], stretchResistanceAttr), '{}Shape.stretchResistance'.format(hairSym))
    # mc.connectAttr('{}.{}'.format(objCtrl[-1], compressionResistanceAttr), '{}Shape.compressionResistance'.format(hairSym))
    mc.connectAttr('{}.{}'.format(objCtrl[-1], startCurveAttractAttr), '{}Shape.startCurveAttract'.format(hairSym))
    mc.connectAttr('{}.{}'.format(objCtrl[-1], attractionDampAttr), '{}Shape.attractionDamp'.format(hairSym))
    mc.connectAttr('{}.{}'.format(objCtrl[-1], massAttr), '{}Shape.mass'.format(hairSym))
    mc.connectAttr('{}.{}'.format(objCtrl[-1], dragAttr), '{}Shape.drag'.format(hairSym))
    # mc.connectAttr('{}.{}'.format(objCtrl[-1], tangentialDragAttr), '{}Shape.tangentialDrag'.format(hairSym))
    mc.connectAttr('{}.{}'.format(objCtrl[-1], motionDragAttr), '{}Shape.motionDrag'.format(hairSym))

    mc.connectAttr('{}.{}'.format(objCtrl[-1], startFrameAttr), '{}.startFrame'.format(nucleus))
    mc.connectAttr('{}.{}'.format(objCtrl[-1], spaceScaleAttr), '{}.spaceScale'.format(nucleus))
    mc.connectAttr('{}.{}'.format(objCtrl[-1], gravityAttr), '{}.gravity'.format(nucleus))
    mc.connectAttr('{}.{}'.format(objCtrl[-1], airDensityAttr), '{}.airDensity'.format(nucleus))
    mc.connectAttr('{}.{}'.format(objCtrl[-1], windSpeedAttr), '{}.windSpeed'.format(nucleus))
    mc.connectAttr('{}.{}'.format(objCtrl[-1], windNoiseAttr), '{}.windNoise'.format(nucleus))
    ## parent joint Drv
    for i in range(len(fkCtrlGrp)):
        if not i == 0:
            mc.parent(fkCtrlGrp[i], fkGmblCtrlGrp[i-1])
        if not i == 0:
            mc.parent(objDrvJnt[i], objDrvJnt[i-1])  
        utaCore.parentScaleConstraintLoop(obj = [fkGmblCtrlGrp[i]], lists = [objDrvJnt[i]],par = True, sca = True, ori = False, poi = False)

    ## bindSkin obj Drv jnt and fkJnt
    utaCore.bindSkinCluster(jntObj = objDrvJnt, obj = [curveDyn])
    utaCore.bindSkinCluster(jntObj = objDrvJnt, obj = [curveFk])

    ## Create spineIk
    spineIkName = '{}{}Ik{}Ikh'.format(name, elem.lower(), side)
    spineIkName = utaCore.createIkhSpine (strJnt = dynJnt[0], endJnt = dynJnt[-1] ,curve = crvDynOutputName , name = spineIkName, sol = 'ikSplineSolver')
    ## Create spineIk Fk
    spineFkName = '{}{}Fk{}Ikh'.format(name, elem.lower(), side)
    spineFkName = utaCore.createIkhSpine (strJnt = fkJnt[0], endJnt = fkJnt[-1] ,curve = curveFk , name = spineFkName, sol = 'ikSplineSolver')
    ''' ikSplineSolver
        ikRPsolver
        ikSCsolver'''

    ## Generate Group dynJnt
    dynFkJntGrp = []
    listsJnt = [dynJnt[0], fkJnt[0]]
    for jnt in listsJnt:
        mc.select(jnt)
        dynFkJntGrp.append(rigTools.UtaDoZeroGroup())

    for i in range(len(tmpJnt)):
        mc.parentConstraint(fkJnt[i], dynJnt[i], tmpJnt[i], mo = True)
        mc.scaleConstraint(fkJnt[i], dynJnt[i], tmpJnt[i], mo = True)
        mc.connectAttr('{}.{}'.format(objCtrl[-1], onOffAttr), "{}_parentConstraint1.{}W0".format(tmpJnt[i], dynJnt[i]))
        mc.connectAttr('{}.{}'.format(objCtrl[-1], onOffAttr), "{}_scaleConstraint1.{}W0".format(tmpJnt[i], dynJnt[i]))
        switchRev = mc.createNode('reverse', n = '{}Rev'.format(objCtrl[-1]))
        mc.connectAttr('{}.{}'.format(objCtrl[-1], onOffAttr), '{}.inputX'.format(switchRev))
        mc.connectAttr('{}.outputX'.format(switchRev), "{}_parentConstraint1.{}W1".format(tmpJnt[i], fkJnt[i]))
        mc.connectAttr('{}.outputX'.format(switchRev), "{}_scaleConstraint1.{}W1".format(tmpJnt[i], fkJnt[i]))

    ## Generate Grp
    mc.select(spineIkName)
    spineIkGrp = rigTools.UtaDoZeroGroup()
    ## Generate spineFkGrp
    mc.select(spineFkName)
    spineFkGrp = rigTools.UtaDoZeroGroup()
    mc.select(curveFk)
    curveFkGrp = rigTools.UtaDoZeroGroup()
    mc.select(tmpJnt[0])
    tmpJntGrp = rigTools.UtaDoZeroGroup()
    mc.select(objDrvJnt[0])
    dynHairJnt = rigTools.UtaDoZeroGroup()

    ## Wrap Group
    mc.parent(hairSym, nucleus, system1Follicles, hairSymObj, curveFkGrp, objStillGrp)
    mc.parent(fkCtrlGrp[0], objCtrlGrp)
    mc.parent(dynHairJnt, dynFkJntGrp[0], dynFkJntGrp[-1], objJntGrp)
    mc.parent(spineIkGrp, spineFkGrp, objIkhGrp)
    mc.parent(tmpJntGrp, objSkinGrp)

    # Generate parentConstraint for scale
    listsCon = [dynFkJntGrp[0], dynFkJntGrp[-1]]
    for jntGrp in listsCon:
        utaCore.parentScaleConstraintLoop(obj = [fkGmblCtrlGrp[0]], lists = [jntGrp],par = True, sca = True, ori = False, poi = False)

    ## Generate vis group
    listsVis = [objIkhGrp, objJntGrp, objStillGrp]
    for each in listsVis:
        mc.setAttr('{}.v'.format(each), 0)

    ## Generate inheritsTransform
    listsInher = [objSkinGrp, objJntGrp]
    for each in listsInher:
        mc.setAttr('{}.inheritsTransform'.format(each), 0)
    mc.select(cl = True)

    ## set inheritsTransform
    listsInher = [objStillGrp]
    for each in listsInher:
        mc.setAttr('{}.inheritsTransform'.format(each), 0)
    mc.select(cl = True)