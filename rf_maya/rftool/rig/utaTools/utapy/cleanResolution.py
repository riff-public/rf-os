import sys
# sys.path.append(r'P:\lib\local\utaTools\pkmel')
sys.path.append(r'P:\lib\local\utaTools')
sys.path.append(r'P:\lib\local\utaTools\utapy')
# import utapy.utaCore as uc 
# reload(uc)
import maya.cmds as mc
reload(mc)
import maya.mel as mm
reload(mm)
import pkrig.rigTools  as rigTools
reload(rigTools)
import maya.mel as mel
import utapy.turnOffSSC  as  turnOffSSC
reload(turnOffSSC)
import utaCore as uc
reload(uc)

def clean():
    # delete unkownode
    try:
        sel = mc.ls('TurtleDefaultBakeLayer')
        mc.lockNode(sel,lock=False)
        mc.delete(sel)
    except: pass

    # import reference and delete nameSpace
    rigTools.importRigElementDeleteGrp()

    # clean unused Node 
    mel.eval('MLdeleteUnused;')

    # TurnOff SSC
    turnOffSSC.turnOff()

    # delete vraySettings
    try:
        sel = mc.ls('vraySettings')
        mc.lockNode(sel,lock=False)
        mc.delete(sel)
    except: pass

    # clean Delete_grp or 
    try:
        delGrp = mc.ls('Delete_Grp', 'Delete_grp')
        mc.delete(delGrp)
    except: pass

    # delete layer
    try:
        dispLayerStates = [] # Create array to store layer states
        dispLayers = mc.ls(type = 'displayLayer')
        # print dispLayers
        for lyr in dispLayers:
            dispLayerStates.append([lyr,mc.getAttr('%s.visibility' % lyr)])
                # Restore layer visibility
                #return dispLayerStates
                #loop = 0
        for lyr in dispLayers:
            if lyr != 'displayLayer':
                #loop += 1
                mc.delete(lyr)
    except:
        print "'displayLayer' can't Delete"

    # clean Camera
    try:
        for cam in cams:
            mc.camera(cam,e=True,startupCamera=False)
            mc.delete(cam)
            print 'delete %s' %(cam)
    except: pass

    # delete layer
    # cleanUp.clearLayer()
    print '------ Clean Success!! --------' 
    print '--------    *(^O^)*    --------' 
    print '-------------------------------'

def cleanBsh():
    # delete unkownode
    try: 
        sel = mc.ls('TurtleDefaultBakeLayer')
        mc.lockNode(sel,lock=False)
        mc.delete(sel)
    except: pass

    # delete vraySettings
    try:
        sel = mc.ls('vraySettings')
        mc.lockNode(sel,lock=False)
        mc.delete(sel)
    except: pass

    # delete layer
    try:
        dispLayerStates = [] # Create array to store layer states
        dispLayers = mc.ls(type = 'displayLayer')
        # print dispLayers
        for lyr in dispLayers:
            dispLayerStates.append([lyr,mc.getAttr('%s.visibility' % lyr)])
                # Restore layer visibility
                #return dispLayerStates
                #loop = 0
        for lyr in dispLayers:
            if lyr != 'displayLayer':
                #loop += 1
                mc.delete(lyr)
    except:
        print "'displayLayer' can't Delete"

    # import reference and delete nameSpace
    rigTools.importRigElementDeleteGrp()

    # assign shader 'lambert1'
    lisObj = mc.ls('FacialBshStill_Grp','FacialBshStill_grp')
    for i in range(len(lisObj)):
        mc.select( lisObj[i] )
        mc.hyperShade(lisObj[i], assign ='lambert1')

    # clean unused Node 
    mel.eval('MLdeleteUnused;')

    # TurnOff SSC
    turnOffSSC.turnOff()

    # clean Delete_grp or 
    try:
        delGrp = mc.ls('Delete_Grp', 'Delete_grp')
        mc.delete(x)
    except: pass

    # clean Camera
    try:
        for cam in cams:
            mc.camera(cam,e=True,startupCamera=False)
            mc.delete(cam)
            print 'delete %s' %(cam)
    except: pass

    print '----- Clean Bsh Success!! -----' 
    print '--------    *(^O^)*    --------' 
    print '-------------------------------'

# cleanDtl()
def cleanDtl():

    # delete unkownode
    try:
        sel = mc.ls('TurtleDefaultBakeLayer')
        mc.lockNode(sel,lock=False)
        mc.delete(sel)
    except: pass

    # delete vraySettings
    try: 
        sel = mc.ls('vraySettings')
        mc.lockNode(sel,lock=False)
        mc.delete(sel)
    except: pass

    # delete layer
    try:
        dispLayerStates = [] # Create array to store layer states
        dispLayers = mc.ls(type = 'displayLayer')
        # print dispLayers
        # for lyr in dispLayers:
        #     dispLayerStates.append([lyr,mc.getAttr('%s.visibility' % lyr)])
        #         # Restore layer visibility
        #         #return dispLayerStates
        #         #loop = 0
        for lyr in dispLayers[1:]:
            if lyr != 'displayLayer':
                #loop += 1
                mc.delete(lyr)
    except:
        print "'displayLayer' can't Delete"

    # import reference and delete nameSpace
    # rigTools.importRigElementDeleteGrp()

    # delete  inheritsTransform
    listInherCtrl = mc.ls('*DtlCtrlZro_lft_grp', '*DtlCtrlZro_rgt_grp')
    listInherJnt = mc.ls('*DtlJntZro_lft_grp', '*DtlJntZro_rgt_grp')
    for i in range(len(listInherCtrl)):
        mc.setAttr('%s.inheritsTransform' % listInherCtrl[i],0)
    for i in range(len(listInherJnt)):    
        mc.setAttr('%s.inheritsTransform' % listInherJnt[i],0)

    # delete anather polygon
    listPly = mc.ls('eye_rgt_grp','eye_lft_grp','hairBand_ply','tongue_ply')
    mc.delete(listPly)

    mc.hyperShade(rss=True)

    # #assign shader 'lambert1'
    # lisObj = mc.ls('headDtl_Grp', 'dtlGeol_Grp')
    # mc.select( lisObj )
    # mc.hyperShade( assign ='lambert1')

    # clean unused Node 
    mel.eval('MLdeleteUnused;')

    # clean Delete_grp or 
    delGrp = mc.ls('Delete_Grp', 'Delete_grp')
    mc.delete(delGrp)

    # assign shader 'lambert1'
    lisObj = mc.ls('allDtlGeo_grp','dtlGeo_grp','dtlGeo_Grp','headDtl_Grp','dtlGeol_Grp')
    for i in lisObj:
        mc.select(lisObj)
        mc.hyperShade(lisObj, assign ='lambert1')

    # TurnOff SSC
    turnOffSSC.turnOff()

    # clean Camera
    try:
        uc.cleanCamera()
    except: pass

    # clean Delete_grp or 
    try:
        delGrp = mc.ls('Delete_Grp', 'Delete_grp')
        mc.delete(delGrp)
    except: pass

    print '----- Clean Dtl Success!! -----' 
    print '--------    *(^O^)*    --------' 
    print '-------------------------------'

def cleanCombRig():
    # delete unkownode
    try:
        sel = mc.ls('TurtleDefaultBakeLayer')
        mc.lockNode(sel,lock=False)
        mc.delete(sel)
    except: pass

    # delete vraySettings
    try:
        sel = mc.ls('vraySettings')
        mc.lockNode(sel,lock=False)
        mc.delete(sel)
    except: pass

    # delete layer
    try:
        dispLayerStates = [] # Create array to store layer states
        dispLayers = mc.ls(type = 'displayLayer')
        # print dispLayers
        for lyr in dispLayers:
            dispLayerStates.append([lyr,mc.getAttr('%s.visibility' % lyr)])
                # Restore layer visibility
                #return dispLayerStates
                #loop = 0
        for lyr in dispLayers:
            if lyr != 'displayLayer':
                #loop += 1
                mc.delete(lyr)
    except:
        print "'displayLayer' can't Delete"

    # clean unused Node 
    mel.eval('MLdeleteUnused;')

    # TurnOff SSC
    turnOffSSC.turnOff()

    # clean Camera
    try:
        uc.cleanCamera()
    except: pass

    # create Delete_Grp
    delGrp = mc.group(em=True)
    delGrpNew = mc.rename(delGrp,'Delete_Grp') 

    # List
    headJnt = mc.ls('*:head1_jnt')
    teethUp = mc.ls('*:teeth1_upr_jnt')
    teethDn = mc.ls('*:teeth1_lwr_jnt')

    eyeDtl =  mc.ls('*:eye*DtlCtrlZro_*_grp','*:eyeBrow*DtlCtrlZro_*_grp','*:dtlEyelashZro_grp','*:cheekLwrDtlCtrlZro_*_grp','*:cheekUpr*DtlCtrlZro_*_grp')
    mouthUpDtl = mc.ls('*:mouth1DtlCtrlZro_*_grp','*:mouth2DtlCtrlZro_*_grp','*:mouth3DtlCtrlZro_*_grp','*:mouth4DtlCtrlZro_*_grp')
    mouthDnDtl = mc.ls('*:mouth5DtlCtrlZro_*_grp','*:mouth6DtlCtrlZro_*_grp','*:mouth7DtlCtrlZro_*_grp')
    
    # lis Skin Group
    animGrp = mc.ls('*:anim_grp')
    stillGrp = mc.ls('*:still_grp')
    skinGrp = mc.ls('*:skin_grp')
    jntGrp = mc.ls('*:jnt_grp')
    ikhGrp = mc.ls('*:ikh_grp')
    defGrp = mc.ls('*:def_grp')

    # list Bsh
    FacialBshStill = mc.ls('*:FacialBshStill_Grp')
    allDefGeo = mc.ls('allDefGeo_grp')
    FacialRig = mc.ls('*:FacialRig_Grp')
    FacialBsh = mc.ls('*:FacialBshCtrlZro_Grp')
    EyeLidsBsh =mc.ls('*:EyeLidsBshJntZro_Grp')

    # list Dtl
    dtlGeo = mc.ls('*:headDtl_Grp','*:dtlGeo_grp','*:allDtlGeo_grp')
    allDtlCtrl = mc.ls('*:allDtlCtrl_grp')
    allDtlJnt = mc.ls('|*:allDtlJnt_grp')
    allDtlNrb = mc.ls('*:allDtlNrb_grp')

    # connectEyelidFollow
    uc.eyeLidFollow()

    # parentConstraint and scaleConstraint Dtl  
    if eyeDtl :
        for i in range(len(eyeDtl)):
            mc.orientConstraint(headJnt,'%s' %eyeDtl[i],mo=True)
            mc.scaleConstraint(headJnt,'%s' %eyeDtl[i],mo=True)
        for i in range(len(mouthUpDtl)):
            mc.orientConstraint(teethUp,'%s' %mouthUpDtl[i],mo=True)
            mc.scaleConstraint(teethUp,'%s' %mouthUpDtl[i],mo=True)
        for i in range(len(mouthDnDtl)):
            mc.orientConstraint(teethDn,'%s' %mouthDnDtl[i],mo=True)
            mc.scaleConstraint(teethDn,'%s' %mouthDnDtl[i],mo=True)

    # Parent to Rig
    mc.parent (allDefGeo, allDtlCtrl, allDtlJnt, allDtlNrb, dtlGeo, FacialBshStill, FacialRig, delGrpNew)

    # check Object
    # FacialBshStill parent to Delete_grp and parent scale Constrant
    for x in FacialBshStill:
        if x != 0:
            mc.parentConstraint (defGrp,x,mo=True)
            mc.scaleConstraint (defGrp,x,mo=True)

    # allDefGeo_grp parent to Delete_grp and parent scale Constrant
    for x in allDefGeo:
        if x != 0:
            mc.parentConstraint (defGrp,x,mo=True)
            mc.scaleConstraint (defGrp,x,mo=True)

    # Bsh parent to Delete_grp and parent scale Constrant
    for x in FacialRig :
        if x != 0:
            mc.parentConstraint (animGrp,x,mo=True)
            mc.scaleConstraint (animGrp,x,mo=True)
            for x in FacialBsh :
                if x != 0:
                    mc.parentConstraint (headJnt,x,mo=True)
                    mc.scaleConstraint (headJnt,x,mo=True)
                    for x in EyeLidsBsh :
                        if x != 0:      
                            mc.parentConstraint (headJnt,x,mo=True)
                            mc.scaleConstraint (headJnt,x,mo=True)
        
    # Dtl parent to Delete_grp and parent scale Constrant
    for x in dtlGeo :
        if x != 0:
            mc.parentConstraint (defGrp,x,mo=True)
            mc.scaleConstraint (defGrp,x,mo=True)
            for x in allDtlCtrl:
                if x != 0:
                    mc.parentConstraint (animGrp,x,mo=True)
                    mc.scaleConstraint (animGrp,x,mo=True)
                    for x in allDtlJnt:
                        if x != 0:
                            mc.parentConstraint (defGrp,x,mo=True)
                            mc.scaleConstraint (defGrp,x,mo=True)
                            for x in allDtlNrb :
                                if x != 0:
                                    mc.parentConstraint (defGrp,x,mo=True)
                                    mc.scaleConstraint (defGrp,x,mo=True)

    print '--- Clean CombRig Success!! ---' 
    print '--------    *(^O^)*    --------' 
    print '-------------------------------'
