import maya.cmds as mc
import pymel.core as pm
from utaTools.utapy import utaCore
reload(utaCore)

'''
from utaTools.utapy import utaMuscleControl
reload(utaMuscleControl)
ken = utaMuscleControl.UtaMuscleControl()
ken.run(positiveObj = 'ShirtPas_Geo', 
        negativeObj = ['PosA_Jnt'], 
        tmpJnt = ['ArmorA1_Jnt',
                  'ArmorA2_Jnt'], 
        name = 'ArmorA')
'''

class UtaMuscleControl( object ):
    def __init__( self ):
        tmpJnt = []

        # positiveObj = []
        negativeObj = []
        ## Generate cMuscle
        self.grpObjAll = []
        self.cMscAll = []
        self.cMscNode = []

        self.IkhGrp = ''
        self.JntGrp = ''
        self.MusNodeGrp = ''
        self.TransFormGrp = ''

        self.JntIkSpineGrp = ''
        self.ikhGrpObj = ''
        self.nodeKeepOut = ''
    def run(self, positiveObj = '', negativeObj = [], tmpJnt = [], name = '', elem = ''):

        ## Create group
        self.IkhGrp = '{}Ikh_Grp'.format(name)
        self.JntGrp = '{}Jnt_Grp'.format(name)
        self.MusNodeGrp = '{}MusNode_Grp'.format(name)
        self.TransFormGrp = '{}TransForm_Grp'.format(name)
        if not mc.objExists(self.IkhGrp):   
            self.IkhGrp = mc.group(n = '{}Ikh_Grp'.format(name) , em = True)
        if not mc.objExists(self.JntGrp):
            self.JntGrp = mc.group(n = '{}Jnt_Grp'.format(name) , em = True)
        if not mc.objExists(self.MusNodeGrp):
            self.MusNodeGrp = mc.group(n = '{}MusNode_Grp'.format(name) , em = True)
        if not mc.objExists(self.TransFormGrp):
            self.TransFormGrp = mc.group(n = '{}TransForm_Grp'.format(name) , em = True)

        strJnt = tmpJnt[0]
        endJnt = tmpJnt[-1]
        ikh = '{}_Ikh'.format(name + elem)
        utaCore.createIkh(strJnt = strJnt, endJnt = endJnt ,name = name + elem, sol = 'ikRPsolver')
        self.ikhGrpObj = mc.group(n = '{}Ikh_Grp'.format(name + elem), em = True)
        mc.parent(self.ikhGrpObj, self.IkhGrp)
        utaCore.snapObj(objA = ikh, objB = self.ikhGrpObj)
        pm.makeIdentity ( self.ikhGrpObj , apply = True ) 
        mc.parent(ikh, self.ikhGrpObj)

        ## Generate cMuscle
        # self.grpObjAll = []
        # self.cMscAll = []
        # for sel in negativeObj:
        #     name, side, lastName = utaCore.splitName(sels = [sel])
        #     nodeShape = mc.createNode('cMuscleKeepOut' ,n = '{}KeepOutShape'.format(name))
        #     # utaCore.snapObj(negativeObj[0], nodeShape)
        #     mc.setAttr("{}.inDirectionX".format(nodeShape), 0)
        #     mc.setAttr("{}.inDirectionY".format(nodeShape), 1)
        #     mc.setAttr("{}.inDirectionZ".format(nodeShape), 0)

        #     self.nodeKeepOut = nodeShape.split('Shape')[0]
        #     self.cMscAll.append(self.nodeKeepOut)
        #     ## check addAttribute
        #     msgKeepOutJnt = '{}.msgKeepOut'.format(sel)
        #     if not msgKeepOutJnt:
        #         attrObj = utaCore.addAttrCtrl (ctrl = sel, elem = ['msgKeepOut','msgKeepOutDriven','msgKeepOutXForm'], min = 0, max = 1, at = 'float')
        #     mc.connectAttr('{}.worldMatrix[0]'.format(self.nodeKeepOut), '{}.worldMatrixAim'.format(nodeShape))
        #     mc.connectAttr('{}.message'.format(self.nodeKeepOut), '{}.msgKeepOutXForm'.format(sel))

        #     grpObj = mc.group(n = '{}Node'.format(name) , em = True)

        #     self.grpObjAll.append(grpObj)

        #     mc.connectAttr('{}.outTranslateLocal'.format(nodeShape), '{}.translate'.format(grpObj))
        #     mc.connectAttr('{}.message'.format(grpObj), '{}.msgKeepOutDriven'.format(sel))
        #     mc.connectAttr('{}.message'.format(nodeShape), '{}.msgKeepOut'.format(sel))
        #     # utaCore.lockAttr(listObj = [sel] , attrs = ['msgKeepOut','msgKeepOutDriven','msgKeepOutXForm'] ,lock = True, keyable = False)

        #     utaCore.snapObj(objA = negativeObj[0], objB = self.nodeKeepOut)
        #     mc.parent(grpObj, self.nodeKeepOut)
        #     pm.makeIdentity ( grpObj , apply = True )
        #     mc.parent(sel, grpObj)

        #     mc.parentConstraint(sel, ikhGrpObj, mo = True)
        #     mc.scaleConstraint(sel, ikhGrpObj, mo = True)


        self.generateNegativeObj( negativeObj = negativeObj)

        ## Generate cMuscleObject
        self.cMscNode = mc.createNode ('cMuscleObject', n = '{}ConObj_Cms'.format(name + elem))

        mc.connectAttr('{}.worldMatrix[0]'.format(positiveObj), '{}.worldMatrixStart'.format(self.cMscNode))
        mc.connectAttr('{}Shape.worldMesh[0]'.format(positiveObj), '{}.meshIn'.format(self.cMscNode))

        # ## Generate Connect Muscle to KeepOut
        for each in self.cMscAll:
            mc.connectAttr('{}.muscleData'.format(self.cMscNode), '{}.muscleData[0]'.format(each))

        print self.nodeKeepOut, '...'
        print self.MusNodeGrp , '..2'
        mc.parent(self.nodeKeepOut, self.MusNodeGrp)
        mc.parent(tmpJnt[0], self.JntGrp)
        ## Parent Tranform
        mc.select('{}{}ConObj_Cms'.format(name, elem ))
        transObj = mc.pickWalk( d = 'Up')[0]
        mc.setAttr('{}.visibility'.format(transObj), 0)
        mc.parent(transObj, self.TransFormGrp)
        mc.select(cl = True)

    def generateNegativeObj(self, negativeObj = [],*args):

        if not negativeObj:
            negativeObj = mc.ls(sl = True)

        for sel in negativeObj:
            name, side, lastName = utaCore.splitName(sels = [sel])
            nodeShape = mc.createNode('cMuscleKeepOut' ,n = '{}KeepOutShape'.format(name))
            # utaCore.snapObj(negativeObj[0], nodeShape)
            mc.setAttr("{}.inDirectionX".format(nodeShape), 0)
            mc.setAttr("{}.inDirectionY".format(nodeShape), 1)
            mc.setAttr("{}.inDirectionZ".format(nodeShape), 0)

            self.nodeKeepOut = nodeShape.split('Shape')[0]
            self.cMscAll.append(self.nodeKeepOut)
            ## check addAttribute
            msgKeepOutJnt = '{}.msgKeepOut'.format(sel)
            if not msgKeepOutJnt:
                attrObj = utaCore.addAttrCtrl (ctrl = sel, elem = ['msgKeepOut','msgKeepOutDriven','msgKeepOutXForm'], min = 0, max = 1, at = 'float')
            mc.connectAttr('{}.worldMatrix[0]'.format(self.nodeKeepOut), '{}.worldMatrixAim'.format(nodeShape))
            mc.connectAttr('{}.message'.format(self.nodeKeepOut), '{}.msgKeepOutXForm'.format(sel))

            grpObj = mc.group(n = '{}Node'.format(name) , em = True)

            self.grpObjAll.append(grpObj)

            mc.connectAttr('{}.outTranslateLocal'.format(nodeShape), '{}.translate'.format(grpObj))
            mc.connectAttr('{}.message'.format(grpObj), '{}.msgKeepOutDriven'.format(sel))
            mc.connectAttr('{}.message'.format(nodeShape), '{}.msgKeepOut'.format(sel))
            # utaCore.lockAttr(listObj = [sel] , attrs = ['msgKeepOut','msgKeepOutDriven','msgKeepOutXForm'] ,lock = True, keyable = False)

            utaCore.snapObj(objA = negativeObj[0], objB = self.nodeKeepOut)
            mc.parent(grpObj, self.nodeKeepOut)
            pm.makeIdentity ( grpObj , apply = True )
            mc.parent(sel, grpObj)

            mc.parentConstraint(sel, self.ikhGrpObj, mo = True)
            mc.scaleConstraint(sel, self.ikhGrpObj, mo = True)

    def generatePositiveObj(self, positiveObj = '',name = '', elem = '', *args):

        if not positiveObj:
            positiveObj = mc.ls(sl = True)[0]

        ## Generate cMuscleObject
        self.cMscNode = mc.createNode ('cMuscleObject', n = '{}ConObj_Cms'.format(name + elem))
        mc.connectAttr('{}.worldMatrix[0]'.format(positiveObj), '{}.worldMatrixStart'.format(self.cMscNode))
        mc.connectAttr('{}.worldMesh[0]'.format(positiveObj + 'Shape'), '{}.meshIn'.format(self.cMscNode))

    def generatePositiveNegativeObj(self,negativeObj = '', positiveObj = '', *args):
        ## Generate Connect Muscle to KeepOut
        
        for each in self.cMscAll:
            print each, '>>', self.cMscNode
        mc.connectAttr('{}.muscleData'.format(self.cMscNode), '{}.muscleData[0]'.format(each))

    def generateSpineIkCrv(self, cur_name = '', span_curve = 20, elem = '', *args):
        ikSpineAll = []
        self.JntIkSpineGrp = '{}JntIkSpine_Grp'.format(elem)
        if not mc.objExists(self.JntIkSpineGrp):   
            self.JntIkSpineGrp = mc.group(n = '{}JntIkSpine_Grp'.format(elem) , em = True)

        ## Duplicate Curve 
        crvPosi = mc.rename(cur_name, '{}Fnt_Crv'.format(elem))
        crvNega = mc.duplicate(crvPosi, n = '{}Bck_Crv'.format(elem))[0]
        crvSkinFnt = mc.duplicate(crvPosi, n = '{}SkinFnt_Crv'.format(elem))[0]
        crvSkinBck = mc.duplicate(crvPosi, n = '{}SkinBck_Crv'.format(elem))[0]
        crvBshFnt = mc.duplicate(crvPosi, n = '{}BshFnt_Crv'.format(elem))[0]
        crvBshBck = mc.duplicate(crvPosi, n = '{}BshBck_Crv'.format(elem))[0]

        ## ReverseCurve 
        mc.reverseCurve(crvNega, ch = True, rpo = True)

        ## Create Joint 
        jointFnt, jointBck = utaCore.createJointOnCurve(cur_name = crvPosi, elem = elem, span_curve = span_curve)

        ## Create ik spine
        jointAll = [jointFnt, jointBck]
        crvAll = [crvPosi, crvNega]
        offsetIkSpine = [0.94, 0.1]
        for i in range(len(jointAll)):
            ## Split Name Curve
            name, side, lastName = utaCore.splitName(sels = [crvAll[i]])
            mc.select(jointAll[i])
            utaCore.parentObj(jntName = '', Side = '', lastName = '')
            utaCore.createIkhSpine (strJnt = jointAll[i][0], endJnt = jointAll[i][-1] ,curve = crvAll[i], name = '{}{}Ikh'.format(name, side), sol = 'ikSplineSolver')
            mc.setAttr('{}.offset'.format('{}{}Ikh'.format(name, side)), offsetIkSpine[i])
            mc.parent(jointAll[i][0], self.JntIkSpineGrp)

        mc.parent(self.JntIkSpineGrp, self.JntGrp)