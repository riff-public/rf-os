import pymel.core as pm
import maya.cmds as mc
import maya.mel as mm
from utaTools.utapy import utaCore
reload(utaCore)

def ExFaceProxyUi(*args):

	# check if window exists
	if pm.window ( 'ExFaceProxyUi' , exists = True ) :
	    pm.deleteUI ( 'ExFaceProxyUi' ) ;
	title = "ExFaceProxy v1.0 (29-01-2019)" ;
	# create window 
	window = pm.window ( 'ExFaceProxyUi', 
	                        title = title , 
	                        width = 100,
	                        mnb = True , 
	                        mxb = True , 
	                        sizeable = True , 
	                        rtf = True ) ;
	windowRowColumnLayout = pm.window ( 'ExFaceProxyUi', 
	                        e = True , 
	                        h = 100,
	                        width = 225);

	# motionTab A-------------------------------------------------------------------------------------------------------------------------------    
	mainWindow = pm.tabLayout ( innerMarginWidth = 1 , innerMarginHeight = 1 , p = windowRowColumnLayout ) ;
	MotionTab_A = pm.rowColumnLayout ( "Cut Face Proxy", nc = 1 , p = mainWindow ) ;

	## Tab ----------------------------------------------------------------
	rowLine_TabA = pm.rowColumnLayout ( nc = 1 , p = MotionTab_A ) ;
	mc.tabLayout(p=rowLine_TabA, w= 225)

	# Control Tab ------------------------------------------------------
	pm.rowColumnLayout (p = rowLine_TabA ,h=8) ;

	rowLine1 = pm.rowColumnLayout ( nc = 5 , p = rowLine_TabA  , cw = [(1, 10), (2, 60),(3, 10 ), (4, 145) ,(5, 10)]);
	## Joint
	pm.rowColumnLayout (p = rowLine1 ,h=8) ;
	pm.text( label='1. Add Joint' , p = rowLine1, al = "left")  
	pm.rowColumnLayout (p = rowLine1 ,h=8) ;
	rowLine0Sub = pm.rowColumnLayout ( nc = 3 , p = rowLine1 , h = 30 , cw = [(1, 93), (2, 25), (3, 25) ]) ;
	pm.textField( "txtFieldJnt", text = "", p = rowLine0Sub , editable = True )
	mc.button("buttonJnt", l="<<" , p = rowLine0Sub , c = getJnt)
	mc.button("buttonSw", l="O/C" , p = rowLine0Sub , c = switchJnt)
	pm.rowColumnLayout (p = rowLine1 ,h=8) ;

	
	
	## Part
	pm.rowColumnLayout (p = rowLine1 ,h=8) ;
	pm.text( label='2. Part Name' , p = rowLine1, al = "left")  
	pm.rowColumnLayout (p = rowLine1 ,h=8) ;
	pm.textField( "txtFieldPart", text = "", p = rowLine1 , editable = True )
	pm.rowColumnLayout (p = rowLine1 ,h=8) ;
	
    ## mirror
	pm.rowColumnLayout (p = rowLine1 ,h=33) ;
	pm.text( label='3. Mirror' , p = rowLine1, al = "left")  
	pm.rowColumnLayout (p = rowLine1 ,h=8) ;
	pm.checkBox( "checkMirror", label = "Mirror", p = rowLine1 ,  align='left', value= False)
	pm.rowColumnLayout (p = rowLine1 ) ;
	
	## Side

	pm.rowColumnLayout (p = rowLine1 ) ;
	pm.text( label='4. Side' , p = rowLine1, al = "left")  
	pm.rowColumnLayout (p = rowLine1 ,h=8) ;
	rowLine1Sub = pm.rowColumnLayout ( nc = 3 , p = rowLine1 , h = 30 , cw = [(1, 30), (2, 30), (3, 30) ]) ;
	mc.radioCollection()
	mc.radioButton('radioL', p = rowLine1Sub ,label='L', align='left' )
	mc.radioButton('radioR', p = rowLine1Sub ,label='R', align='left')
	mc.radioButton('radioC', p = rowLine1Sub ,label='C', align='left', select = True)
	pm.rowColumnLayout (p = rowLine1 ,h=8) ;

	## count
	pm.rowColumnLayout (p = rowLine1 ,h=8) ;
	pm.text( label='5. Count' , p = rowLine1, al = "left")  
	pm.rowColumnLayout (p = rowLine1 ,h=8) ;
	pm.textField( "txtFieldCount", text = "1", p = rowLine1 , editable = True )
	pm.rowColumnLayout (p = rowLine1 ,h=8) ;

	## Create
	rowLine2 = pm.rowColumnLayout ( nc = 3 , p = rowLine_TabA , h = 40 , cw = [(1, 10), (2, 214), (3, 10) ]) ;
	pm.rowColumnLayout (p = rowLine2 ,h=8) ;
	# pm.rowColumnLayout (p = rowLine1 ,h=8) ;
	pm.button ('cutFaceButton', label = " Cut Face" ,h=40, p = rowLine2 , c = run, bgc = (0.411765, 0.411765, 0.411765)) ;
	pm.rowColumnLayout (p = rowLine2 ,h=8) ;

	## Tab ----------------------------------------------------------------
	rowLine_TabA = pm.rowColumnLayout ( nc = 1 , p = MotionTab_A ) ;
	mc.tabLayout(p=rowLine_TabA, w= 240)


	pm.setParent("..")
	pm.showWindow(window)
	
def getJnt(*args):
    
    sel = mc.ls(sl=True)
    if sel:
        jnt = sel[0]
        pm.textField( "txtFieldJnt", e=True , text = '%s'%jnt )
    else:
        pm.textField( "txtFieldJnt", e=True , text = '' )
    ## Un select Joint
    mm.eval('setObjectPickMask "Joint" false;')
        
def switchJnt(*args):
    
    if mc.selectType(q=True, j=True):
        mm.eval('setObjectPickMask "Joint" false;')
    else:
        mm.eval('setObjectPickMask "Joint" true;')
    	
	
def run(*args):

	txtFieldPartObj = pm.textField("txtFieldPart", q=True, text=True) 
	txtFieldCountObj = pm.textField("txtFieldCount", q=True, text=True)
	txtFieldJntObj = pm.textField( "txtFieldJnt", q=True, text=True)
	checkMirror = pm.checkBox( "checkMirror", q=True, value=True )
	
	side = ''
	if mc.radioButton('radioL',q = True, select = True) :
		side = 'L'
	elif mc.radioButton('radioR',q = True, select = True):
		side = 'R'
	elif mc.radioButton('radioC',q = True, select = True) :
		side = 'C'
	utaCore.ExFaceProxy(part = txtFieldPartObj, side = side, count = txtFieldCountObj, jnt = txtFieldJntObj, mrr = checkMirror)
	## On Joint Select
	mm.eval('setObjectPickMask "Joint" true;')
	## Object Mode
	mc.selectMode( object=True )