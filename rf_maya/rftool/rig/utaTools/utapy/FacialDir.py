def Run(*args):
	from utaTools.utapy import MouthDir, EyeDir, lipRig
	reload(MouthDir)
	reload(EyeDir)
	reload(lipRig)
	## mouthRig
	MouthDir.Run()
	## eyeRig
	EyeDir.Run()
	## lipSealRig
	lipRig.run(	geo = 'Body_Geo1', 
				curve = [	'upperLipLinear_Crv', 
							'lowerLipLinear_Crv'], 
				control = [	'MouthCnr_L_Ctrl', 
							'MouthCnr_R_Ctrl'])

	# Eyebrow.run()

