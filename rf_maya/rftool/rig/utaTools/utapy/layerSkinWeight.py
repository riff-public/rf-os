########################################################################################################################## skin matrix
########################################################################################################################## skin matrix
########################################################################################################################## skin matrix
########################################################################################################################## skin matrix
########################################################################################################################## skin matrix



###### FOR LEFT #######


selGeo = 'Hand_L_Geo'
mc.select(cl=1)
mc.select(selGeo)

skn = mc.deformer(type='skinCluster')[0]
mc.connectAttr('ctrl_md:UpArmSpr_L_Jnt.worldMatrix',skn+'.matrix[0]')
mc.connectAttr('ctrl_md:ForearmSpr_L_Jnt.worldMatrix',skn+'.matrix[1]')
mc.connectAttr('ctrl_md:UpArmSprJntZro_L_Grp.worldInverseMatrix' , skn+'.bindPreMatrix[0]')
# mulMat = mc.createNode('multMatrix',n = 'ForearmSprInvMatrix_L_MulMat')
# mc.connectAttr('ctrl_md:UpArmSprJntZro_L_Grp.worldInverseMatrix',mulMat+'.matrixIn[0]')
# mc.connectAttr('ctrl_md:ForearmSprJntZro_L_Grp.inverseMatrix' , mulMat+'.matrixIn[1]')
mc.connectAttr('ctrl_md:ForearmSprJntZro_L_Grp.worldInverseMatrix' , skn+'.bindPreMatrix[1]')
# mc.connectAttr(mulMat + '.matrixSum' , skn+'.bindPreMatrix[1]')
mc.skinPercent(skn,'Hand_L_Geo.vtx[:]',tv = ['ctrl_md:UpArmSpr_L_Jnt',1])
#foreVtx = mc.ls(sl=True)
#mc.skinPercent(skn,foreVtx,tv = ['joint2',1])

###### FOR RIGHT #######

selGeo = 'Hand_R_Geo'
mc.select(cl=1)
mc.select(selGeo)

skn = mc.deformer(type='skinCluster')[0]
mc.connectAttr('ctrl_md:UpArmSpr_R_Jnt.worldMatrix',skn+'.matrix[0]')
mc.connectAttr('ctrl_md:ForearmSpr_R_Jnt.worldMatrix',skn+'.matrix[1]')
mc.connectAttr('ctrl_md:UpArmSprJntZro_R_Grp.worldInverseMatrix' , skn+'.bindPreMatrix[0]')
# mulMat = mc.createNode('multMatrix',n = 'ForearmSprInvMatrix_R_MulMat')
# mc.connectAttr('ctrl_md:UpArmSprJntZro_R_Grp.worldInverseMatrix',mulMat+'.matrixIn[0]')
# mc.connectAttr('ctrl_md:ForearmSprJntZro_R_Grp.inverseMatrix' , mulMat+'.matrixIn[1]')
mc.connectAttr('ctrl_md:ForearmSprJntZro_R_Grp.worldInverseMatrix' , skn+'.bindPreMatrix[1]')
# mc.connectAttr(mulMat + '.matrixSum' , skn+'.bindPreMatrix[1]')
#mc.connectAttr('ctrl_md:ForearmSprJntZro_R_Grp.worldInverseMatrix' , skn+'.bindPreMatrix[1]')
mc.skinPercent(skn,'Hand_R_Geo.vtx[:]',tv = ['ctrl_md:UpArmSpr_R_Jnt',1])



###### FOR All SLEEVES #######



selGeo = 'ClothArm_L_Geo'
mc.select(cl=1)
mc.select(selGeo)

skn = mc.deformer(type='skinCluster')[0]
mc.connectAttr('ctrl_md:AllSleeve_L_Jnt.worldMatrix',skn+'.matrix[0]')
mc.connectAttr('ctrl_md:AllSleeveJntZro_L_Grp.worldInverseMatrix' , skn+'.bindPreMatrix[0]')
mulMat = mc.createNode('multMatrix',n = 'AllSleeveInvMatrix_L_MulMat')
mc.connectAttr('ctrl_md:AllSleeveJntZro_L_Grp.worldInverseMatrix',mulMat+'.matrixIn[0]')
mc.connectAttr(mulMat + '.matrixSum' , skn+'.bindPreMatrix[1]')
mc.skinPercent(skn,'{}.vtx[:]'.format(selGeo),tv = ['ctrl_md:AllSleeve_L_Jnt',1])



selGeo = 'ClothArm_R_Geo'
mc.select(cl=1)
mc.select(selGeo)

skn = mc.deformer(type='skinCluster')[0]
mc.connectAttr('ctrl_md:AllSleeve_R_Jnt.worldMatrix',skn+'.matrix[0]')
mc.connectAttr('ctrl_md:AllSleeveJntZro_R_Grp.worldInverseMatrix' , skn+'.bindPreMatrix[0]')
mulMat = mc.createNode('multMatrix',n = 'AllSleeveInvMatrix_R_MulMat')
mc.connectAttr('ctrl_md:AllSleeveJntZro_R_Grp.worldInverseMatrix',mulMat+'.matrixIn[0]')
mc.connectAttr(mulMat + '.matrixSum' , skn+'.bindPreMatrix[1]')
mc.skinPercent(skn,'{}.vtx[:]'.format(selGeo),tv = ['ctrl_md:AllSleeve_R_Jnt',1])

