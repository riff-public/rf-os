#range
listA = [ 4 , 3 , 2 , 1 ] ;
listB = [ 'A' , 'B' , 'C' , 'D' ] ;

for i in range ( 0 , len ( listA ) ) :
    print listA [ i ] , listB [ i ] ;

#scrip  Exsample py to mel
'''
global proc taTools()
{
  python("import sys\nsys.path.append('P:\lib\local\utaTools\scripts')\nimport maya.mel as mm\nimport genUI\ngenUI.genUI()");
}
taTools();
'''

#Windows UI Exsample  button to explorer
window = pm.window ( 'birdMainUI', title = "technical animation utilities v2.4", width = width ,
mnb = True , mxb = False , sizeable = True , rtf = True ) ;
mnb = True , mxb = False

# Open Exporer
os.startfile ( r'P:\_bird\birdScript' ) ;

# open webbrowser
import webbrowser
webbrowser.open('http://google.co.kr', new=2)

# select Object in layer
pm.mel.eval ( 'layerEditorSelectObjects hairOri1_layer;' ) ;

# Exsample setwidowicon
def __init__(self, *args, **kwargs):

        super(AssetnamingUI, self).__init__(*args, **kwargs)
        self.setupUi(self)

        self.qs = LocalProjectDB()

        self.setWindowIcon(QtGui.QIcon(":ico/fukured.png"))
        self.fileDeleteBtn.setIcon(QtGui.QIcon(":ico/skull.png"))
        self.fileSaveVersionBtn.setIcon(QtGui.QIcon(":ico/version.png"))
        self.showAllRes.setIcon(QtGui.QIcon(":ico/version.png"))
        self.showHero.setIcon(QtGui.QIcon(":ico/hero.png"))
        self.fileCommitLocalBtn.setIcon(QtGui.QIcon(":ico/localhero.png"))
        self.fileCommitHeroBtn.setIcon(QtGui.QIcon(":ico/hero.png"))
        self.fileAddName.setIcon(QtGui.QIcon(":ico/trumpet.png"))
        self.versionIcon.setPixmap(QtGui.QPixmap(":ico/version.png"))
        self.localIcon.setPixmap(QtGui.QPixmap(":ico/localhero.png"))
        self.heroIcon.setPixmap(QtGui.QPixmap(":ico/hero.png"))

        self.setStyleSheet("QPushButton, QListWidget, QLabel, QLineEdit{ \
                            font-size: 10pt;}")

#Exsample CheckBox
import pymel.core as pm
reload(pm)
win = pm.window(title="My Window")
layout = pm.columnLayout()
chkBox = pm.checkBox(label = "My Checkbox", value=True, parent=layout)
btn = pm.button(label="My Button", parent=layout)
def buttonPressed(*args):
    if chkBox.getValue():
        print "Check box is CHECKED!"
        btn.setLabel("Uncheck")
    else:
        print "Check box is UNCHECKED!"
        btn.setLabel("Check")
btn.setCommand(buttonPressed)
win.show()