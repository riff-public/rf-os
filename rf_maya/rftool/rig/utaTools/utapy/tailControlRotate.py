import maya.cmds as mc
reload(mc)
from utaTools.nuTools import controller 
reload(controller)

def tailControlRotate (elem, ns, switchCtrl,*args):
	# print crvName
	tmpJnt = mc.listRelatives('%sTailPathsTmp1_Jnt' % ns ,ad = True)
	rtZro = mc.createNode('transform', n ='%s%sZro_Grp' % (ns, elem))
	rtAimZro = mc.createNode('transform', n= '%s%sAimZro_Grp' % (ns, elem))
	tailRotateCtrl = controller.drawCurve(shapeType = 'arrow270', name = '%s%s_Ctrl' % (ns, elem))
	mc.setAttr('%s.overrideEnabled'%tailRotateCtrl,1)
	mc.setAttr('%s.overrideColor'%tailRotateCtrl,17)
	mc.connectAttr('%s.OnOffPath' % switchCtrl, '%s.visibility' % rtZro)
	mc.parent(rtAimZro, rtZro)
	mc.parent(tailRotateCtrl, rtAimZro)
	mc.delete(mc.parentConstraint('%sTailPathsTmp10JntZro_grp' % ns , rtZro, mo=False))
	mc.parentConstraint('%sTailPathsTmp10JntZro_grp' %  ns, rtZro, mo = True)
	mc.scaleConstraint('%sTailPathsTmp10JntZro_grp' %  ns, rtZro, mo = True)
	# mc.connectAttr('%s.translate' % tailRotateCtrl, '%s.translate' % '%sTailPathsTmp1_Jnt' % ns )
	mc.connectAttr('%s.rotate' % tailRotateCtrl, '%s.rotate' % '%sTailPathsTmp1_Jnt' % ns )
	mc.aimConstraint('%sTailPathsTmp11JntZro_grp' % ns, rtAimZro, aim = [0, 1, 0], u = [0, 0, 1])
