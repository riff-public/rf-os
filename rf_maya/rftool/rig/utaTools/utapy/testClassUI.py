import pymel.core as pm
import maya.cmds as mc

class AA( object ) :
    def __init__( self , version = '1.0' ) :
        
        if pm.window( 'TestAA' , exists = True ) :
            pm.deleteUI( 'TestAA' )
        
        width = 240
        window = pm.window( 'TestAA' , title = "AA" , width = width , mnb = True , mxb = False , sizeable = True , rtf = True )
        
        refLayout = pm.rowColumnLayout ( w = width , nc = 1 , columnWidth = [ ( 1 , width-4 ) ] )
        
        with refLayout :
            self.m = pm.button( 'Test' , label = 'TestA' , h = 35 )
                    
        pm.showWindow( window )

class BB( object ) :
    def __init__( self , version = '1.0' ) :
        
        if pm.window( 'TestBB' , exists = True ) :
            pm.deleteUI( 'TestBB' )
        
        width = 240
        window = pm.window( 'TestBB' , title = "BB" , width = width , mnb = True , mxb = False , sizeable = True , rtf = True )
        
        refLayout = pm.rowColumnLayout ( w = width , nc = 1 , columnWidth = [ ( 1 , width-4 ) ] )
        
        with refLayout :
            self.m = pm.button( 'Test' , label = 'TestB' , h = 35 )
                    
        pm.showWindow( window )
        
aa = AA()
bb = BB()

pm.button( aa.m , q=True , label = True )
pm.button( bb.m , q=True , label = True )