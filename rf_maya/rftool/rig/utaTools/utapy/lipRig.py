import pymel.core as pm
import maya.cmds as mc
import maya.mel as mm
from utaTools.utapy import utaCore
reload(utaCore)
from lpRig import rigTools 
reload(rigTools)
from utaTools.pkmel import weightTools
reload(weightTools)
from lpRig import rigTools as lrr
reload(lrr)

#---------------------------------------------------------------------
# from utaTools.utapy import lipRig
# reload(lipRig)
## generate curveOnEdge
# lipRig.createLipCrv(name = '', elem = '', side = '', degree = 1, reverseCv = True)
## genetate StickyLips
# lipRig.run( geo = 'Body_Geo1', 
#             curve = ['upperLipLinear_Crv', 'lowerLipLinear_Crv'], 
#             control = ['MouthCnr_L_Ctrl', 'MouthCnr_R_Ctrl'])
#---------------------------------------------------------------------


def run(geo = '', curve = [], control = [],*args):
	## duplicate curve
	upperCrv, lowerCrv = duplicateCrv(crvObj = curve)

	## get spans
	spans = mc.getAttr( '{}.spans'.format(curve[0]))
	## copySkinWeightGeoToCrv
	bindSkinGeoToCrv(geo = geo, curve = curve)
	## copySkinWeight Upper Crv
	copySkinWeightCrvToCrv(crvA = geo, crvBList = [upperCrv[0], upperCrv[1]])
	## copySkinWeight Lower Crv
	copySkinWeightCrvToCrv(crvA = geo, crvBList = [lowerCrv[0], lowerCrv[1]])

	## set skinWeight to cvCurve
	setSkinPercent(curve = [upperCrv[1], lowerCrv[1]], jnt = 'Head_Jnt', spans = spans)

	## add blendShape
	## upper bsh
	mc.select(upperCrv[0], r = True)   
	mc.select(upperCrv[1], add= True)
	mc.select(upperCrv[2], add= True)
	rigTools.doAddBlendShape()
	## lower bsh
	mc.select(lowerCrv[0], r = True)   
	mc.select(lowerCrv[1], add= True)
	mc.select(lowerCrv[2], add= True)
	rigTools.doAddBlendShape()
	mc.select(cl = True)

	## add Attr
	# control = ['MouthCnr_L_Ctrl', 'MouthCnr_R_Ctrl']
	for each in control:
		if not mc.objExists(each):
			print 'Control', '::', "Don't have in scene"
			return 
	controlAttr = addAttrToCtrl(control = control, 
								attr = ['sticky_Lips', 
										'SL_On_Off', 
										'SL_ManualDyn_Switch', 
										'SL_Manual'],
								value = [0, 4.5, 2.4])


	## create lip control and connect bsh
	generatelipRig(control = controlAttr, spans = spans,)

	##------------------------------------------------------------------------------------
	## generate Loft
	loftNrb = duplicateForLoft(curveObj = [upperCrv[0], lowerCrv[0]])

	## Loft Nrb
	## get spans For nHair
	count = mc.getAttr( '{}.spans'.format(upperCrv[0]))
	folliclesObjGrp, folliclesGrp = generateFolliclesOnNrb(objGrp = loftNrb, count = count-1)
	jntName = generateJntOnFollicles(objGrp = folliclesObjGrp)
	
	## wire Nrb
	wireBase = wireNrb(nrbObj = loftNrb, curveObj = [upperCrv[-1], lowerCrv[-1]])

	## generateStickyLipsNode
	stickLipsStillGrp = generateStickyLipsNode(control = control)

	## generateStickyLipsFunction
	sineObj, sineCrv, sineCrvGrp, stickLipsCtrlGrp = generateStickyLipsFunction(control = control, curve = [upperCrv[0], lowerCrv[0]])

	## add blendShape nonlinear to wireCrv
	addBlendShapeCrv(curveBase = [upperCrv[-1], lowerCrv[-1]], curveFun = sineCrv)

	## parent obj to group
	mc.parent(curve, wireBase, sineObj, upperCrv, lowerCrv, folliclesGrp, loftNrb, sineCrvGrp, stickLipsStillGrp)



	print 'DONE'

def wireNrb(nrbObj = [], curveObj = [], *args):
	wireBase = []
	for i in range(len(curveObj)):
		nameSp , side, lastName = utaCore.splitName(sels = [curveObj[i]])
		wireNode = utaCore.wireObj (wireGeoObj = nrbObj[i], wireObj = [curveObj[i]],name = nameSp, elem = '', side = side)
		wireBase.append('{}BaseWire'.format(wireNode[-1]))
	return wireBase

def duplicateForLoft(curveObj = [],*args):
	nameObj = ['in', 'out']
	valueIn = [-0.08, 0.98]
	valueOut = [0.08, 1.02]
	upperCrv = []
	lowerCrv = []
	for i in range(len(curveObj)):
		for x in range(0,2):
			nameSp , side, lastName = utaCore.splitName(sels = [curveObj[i]])
			crvNew = mc.duplicate(curveObj[i], n = '{}Loft{}{}Crv'.format(nameSp, nameObj[x].capitalize(), side))[0]
			utaCore.lockAttrObj(listObj = [crvNew], lock = False, keyable = True)
			pm.xform ( crvNew , cp = True )

			if 'LoftIn' in crvNew:
				mc.setAttr('{}.tz'.format(crvNew), valueIn[0])
				mc.setAttr('{}.rx'.format(crvNew), valueIn[1])
				if 'upperLip' in crvNew:
					upperCrv.append(crvNew)
				if 'lowerLip' in crvNew:
					lowerCrv.append(crvNew)
			elif 'LoftOut' in crvNew:
				mc.setAttr('{}.tz'.format(crvNew), valueOut[0])
				mc.setAttr('{}.rx'.format(crvNew), valueOut[1])
				if 'upperLip' in crvNew:
					upperCrv.append(crvNew)
				if 'lowerLip' in crvNew:
					lowerCrv.append(crvNew)

			pm.makeIdentity ( crvNew , apply = True ) ;    
			pm.delete ( crvNew , ch = True ) ;

	## loft upper Nrb
	mc.select(upperCrv[0], r = True)
	mc.select(upperCrv[1], add = True)
	upperNrb = mc.loft(n = 'upperLip_Nrb')[0]
	pm.xform ( upperNrb , cp = True )

	## loft lower Nrb
	mc.select(lowerCrv[0], r = True)
	mc.select(lowerCrv[1], add = True)
	lowerNrb = mc.loft(n = 'lowerLip_Nrb')[0]
	pm.xform ( lowerNrb , cp = True )

	pm.delete ( upperNrb, lowerNrb , ch = True )
	mc.delete(upperCrv, lowerCrv)
	nrbLoft = [upperNrb, lowerNrb]

	return nrbLoft

def generateJntOnFollicles(objGrp = [], *args):
	flcAll = []
	jntAll = []
	headJnt = 'Head_Jnt'
	# lipsJntGrp = mc.group(n = 'stickLipsJnt_Grp', em = True)
	if mc.objExists(headJnt):
		headLipJnt = mc.joint(n = 'headLip_Jnt')
		mc.delete(mc.parentConstraint(headJnt, headLipJnt, mo = False))
		mc.setAttr("{}.drawStyle".format(headLipJnt), 2)

	## generate Follicles
	for each in objGrp:
		objAll = mc.listRelatives(each, c = True)
		for each in objAll:
			flcObj = mc.listRelatives(each, c = True)
			flcAll.append(flcObj[0].split('Shape')[0])
			if 'curve' in flcObj[-1]:
				mc.delete(flcObj[-1])

	## generate joint
	for each in flcAll:
		jnt = utaCore.jointFromLocator (sels = [each], clear = False)
		jntAll.append(jnt)
		mc.parent(jnt, headLipJnt)
		mc.parentConstraint(each, jnt, mo = True)
		mc.scaleConstraint(each, jnt, mo = True)
	mc.select(cl = True)
	if mc.objExists('Head_Jnt'):
		mc.parent(headLipJnt, 'Head_Jnt')
		mc.setAttr ('{}.segmentScaleCompensate'.format(headLipJnt), 0 )

	return jntAll      

def generateFolliclesOnNrb(objGrp = [], count = 31, *args):
	folliclesObjGrp = []
	## generate Follicles
	folliclesGrp = mc.group(n = 'folliclesStill_Grp', em = True)
	for each in objGrp:
		nameSp , side, lastName = utaCore.splitName(sels = [each])

		mc.select(each, r = True)
		mm.eval('createHair 1 {} 10 0 0 0 0 5 0 2 1 1'.format(count))
		flcObj = mc.rename("hairSystem1Follicles", "{}Follicles{}Grp".format(nameSp, side))
		mc.parent(flcObj, folliclesGrp)
		mc.delete("hairSystem1OutputCurves", "hairSystem1")


		flcCount = mc.listRelatives(flcObj, c = True)
		for i in range(len(flcCount)):
			mc.rename(flcCount[i], '{}{}{}Flc'.format(nameSp, i+1, side))

		folliclesObjGrp.append(flcObj)
	mc.delete('nucleus1')
	mc.select(cl = True)
	return folliclesObjGrp, folliclesGrp

def setSkinPercent(curve = [], jnt = '', spans = 34,*args):
	for each in curve:
		skinNode = utaCore.getSkinCluster(mesh = [each])
		velueSkinWeight = mc.skinPercent( skinNode, '{}.cv[0]'.format(each), query=True, value=True )[0]
		mc.skinPercent(skinNode, '{}.cv[0:{}]'.format(each, spans) , tv = [jnt, velueSkinWeight])


def bindSkinGeoToCrv(geo = '', curve = [], *args):
	for each in curve:
		mc.select(geo, r = True)
		mc.select(each, add = True)
		utaCore.copyOneToTwoSkinWeight()
		mc.select(cl = True)

def copySkinWeightCrvToCrv(crvA = '', crvBList = [],*args):
	for each in crvBList:
		mc.select(crvA, r = True)
		mc.select(each, add = True)
		weightTools.copySelectedWeight()
		mc.select(cl = True)
		mc.select(crvA, r = True)
		mc.select(each, add = True)
		mc.copySkinWeights (noMirror = True,surfaceAssociation = 'closestComponent', influenceAssociation = 'closestJoint')
		# mm.eval('copySkinWeights  -noMirror -surfaceAssociation closestComponent -influenceAssociation closestJoint;')
		mc.select(cl = True)

def createLipCrv(name = '', elem = '', side = '', degree = 1, reverseCv = True, *args):
	## setPref
	utaCore.setSelectPref()
	## createCureve on Edge
	crv = utaCore.creatCtrl(name = name, elem = elem,side = side, degree = degree, reverseCv = reverseCv)
	return crv

def duplicateCrv(crvObj = ['upperLipLinear_Crv', 'lowerLipLinear_Crv'], *args):
	upperCrv = []
	lowerCrv = []
	nameCrv = ['base', 'sticky', 'wire']

	## select curve
	if not crvObj:
		crvObj = mc.ls(sl = True)
		if not crvObj:
			print 'Please Curve "upperLip" and "lowerLip"'
			return
	for each in crvObj:
		name, side, lastName = utaCore.splitName(sels = [each])
		for i in range(len(nameCrv)):
			newName = name.split('Linear')[0]
			obj = mc.duplicate(each, n = '{}{}{}Crv'.format(newName, nameCrv[i].capitalize(), side))
			newObj = mc.rebuildCurve(obj, ch = 0, rpo =1 ,end = 1, kcp = 1, rt=0, keepRange =0, keepEndPoints=True,kt=0, degree=3, tol=0.01) 
			mc.delete(newObj,ch = True)
			if 'upper' in newObj[0]:
				upperCrv.append(newObj[0])
			elif 'lower' in newObj[0]:
				lowerCrv.append(newObj[0])
	return upperCrv, lowerCrv

def generateStickyLipsFunction(control = [], curve = [],*args):
	sineCtrlAttr = []
	ctrlAttrL = []
	ctrlAttrR = []
	sineCrv = []
	sineListNode = []
	sineNode = []
	sineObj = []
	attrCtrl = []
	ctrlNameSine = []
	sineCrvGrp = mc.group(n = 'sineCrv_Grp', em = True)
	guideNameCrv = ['upper', 'lower']
	funcCtrlGrp = mc.group(n = 'lipsFuncCtrl_Grp', em = True)
	stickLipsCtrlGrp = mc.group(n = 'stickLipsCtrl_Grp', em = True)
	mc.parent(funcCtrlGrp, stickLipsCtrlGrp)
	mc.delete(mc.parentConstraint('Head_Jnt', funcCtrlGrp, mo = False))

	## generate control stickyLips 
	for i in range(len(guideNameCrv)):
		ctrlPosGrp = mc.group(n = '{}SineFunc_Grp'.format(guideNameCrv[i]), em = True)
		mc.delete(mc.parentConstraint(control[0], ctrlPosGrp, mo = False))

		if 'upper' in ctrlPosGrp:
			mc.setAttr('{}.tx'.format(ctrlPosGrp), 2.5)
		elif 'lower' in ctrlPosGrp:
			mc.setAttr('{}.tx'.format(ctrlPosGrp), 2.5)
			syValue = (mc.getAttr('{}.ty'.format(ctrlPosGrp))-0.4)
			mc.setAttr('{}.ty'.format(ctrlPosGrp), syValue)

		mc.select(ctrlPosGrp)
		grpCtrlName, grpCtrlOfsName, grpCtrlParsName, ctrlName, GmblCtrlName = utaCore.createControlCurve(curveCtrl = 'square', parentCon = True, connections = False, geoVis = False)
		utaCore.lockAttrObj([ctrlName], lock = True, keyable = False)
		ctrlNameSine.append(ctrlName)
		mc.setAttr('{}.rx'.format(grpCtrlName), 90)
		mc.setAttr('{}.sx'.format(grpCtrlName), 0.45)
		mc.setAttr('{}.sz'.format(grpCtrlName), 0.15)
		mc.parent(grpCtrlName, funcCtrlGrp)

		## Clear Obj 
		mc.delete(ctrlPosGrp)


		# for each in control:
		utaCore.attrNameTitle(obj = ctrlName, ln = 'Functions')
		OnOffAttr = 'OnOff_Switch'
		utaCore.attrEnum(obj = ctrlName, ln = OnOffAttr, en ='Off : On')
		attrs = utaCore.addAttrCtrl (ctrl = ctrlName, elem = ['Amplitude', 'Wavelength', 'Offset', 'PositionTZ', 'PositionSY'], min = -1000, max = 1000, at = 'float')
		sineCtrlAttr.append('{}.{}'.format(ctrlName, OnOffAttr))
		for attr in attrs:
			sineCtrlAttr.append('{}.{}'.format(ctrlName, attr))


	for each in curve:
		nameSp, side, lastName = utaCore.splitName(sels = [each])
		if 'Base' in nameSp:
			nameSp = nameSp.replace('Base', '')
		newCrv = mc.duplicate(each, n = '{}Sine{}Crv'.format(nameSp, side))
		mc.parent(newCrv, sineCrvGrp)
		sineCrv.append(newCrv[0])

	mc.setAttr('{}.translateX'.format(sineCrvGrp), 5)

	## generate Sine
	for each in sineCrv:
		nameSp, side, lastName = utaCore.splitName(sels = [each])
		sineListNode = mc.nonLinear(each, type =  'sine', lowBound = -1, highBound = 0, amplitude = 0.1)
		newSineNode = mc.rename(sineListNode[0], '{}{}Node'.format(nameSp, side))
		sineHnd = mc.rename(sineListNode[1], '{}Hnd{}Sine'.format(nameSp, side))
		sineObj.append(sineHnd)
		mc.setAttr('{}.dropoff'.format(newSineNode), 1)
		mc.setAttr('{}.wavelength'.format(newSineNode), 0.2)

		posJnt = utaCore.positionBoundingBox(curve = each)
		mc.delete(mc.parentConstraint(posJnt[1], sineHnd, mo = False))
		mc.setAttr('{}.rx'.format(sineHnd), 90)
		mc.setAttr('{}.rz'.format(sineHnd), -90)
		mc.delete(posJnt)

		syValue = (mc.getAttr('{}.sy'.format(sineHnd))*3)
		mc.setAttr('{}.sy'.format(sineHnd), syValue)
		sineNode.append(newSineNode)

	## Set Attribute for Wire
	for i in range(len(sineObj)):
		getTZ = mc.getAttr('{}.tz'.format(sineObj[i]))
		getSY = mc.getAttr('{}.sy'.format(sineObj[i]))

	for i in range(len(sineCtrlAttr)):
		sineNode[0] = sineNode[0]
		sineNode[1] = sineNode[1]
		if 'upper' in sineCtrlAttr[i]:
			attr = sineCtrlAttr[i].split('.')[-1]
			if 'OnOff_Switch' in attr:
				mc.connectAttr(sineCtrlAttr[i], '{}.envelope'.format(sineNode[0]))
			else:
				if 'Offset' in attr:
					ofsetMdv = mc.createNode('multiplyDivide', n = 'upperOffset_Mdv')
					mc.setAttr('{}.input2X'.format(ofsetMdv),-0.1)
					mc.connectAttr(sineCtrlAttr[i], '{}.input1X'.format(ofsetMdv))
					mc.connectAttr('{}.outputX'.format(ofsetMdv),'{}.{}'.format(sineNode[0], attr.lower()))
				elif 'PositionTZ'in attr:
					mc.connectAttr(sineCtrlAttr[i], '{}.tz'.format(sineObj[0]))
				elif 'PositionSY'in attr:
					mc.connectAttr(sineCtrlAttr[i], '{}.sy'.format(sineObj[0]))
				else:
					mc.connectAttr(sineCtrlAttr[i], '{}.{}'.format(sineNode[0], attr.lower()))

		elif 'lower' in sineCtrlAttr[i]:
			attr = sineCtrlAttr[i].split('.')[-1]
			if 'OnOff_Switch' in attr:
				mc.connectAttr(sineCtrlAttr[i], '{}.envelope'.format(sineNode[1]))
			else:
				if 'Offset' in attr:
					ofsetMdv = mc.createNode('multiplyDivide', n = 'lowerOffset_Mdv')
					mc.setAttr('{}.input2X'.format(ofsetMdv),-0.1)
					mc.connectAttr(sineCtrlAttr[i], '{}.input1X'.format(ofsetMdv))
					mc.connectAttr('{}.outputX'.format(ofsetMdv),'{}.{}'.format(sineNode[1], attr.lower()))
				elif 'PositionTZ'in attr:
					mc.connectAttr(sineCtrlAttr[i], '{}.tz'.format(sineObj[1]))
				elif 'PositionSY'in attr:
					mc.connectAttr(sineCtrlAttr[i], '{}.sy'.format(sineObj[1]))
				else:
					mc.connectAttr(sineCtrlAttr[i], '{}.{}'.format(sineNode[1], attr.lower()))


	for i in range(len(ctrlNameSine)):
		for attr in attrs:
			if 'Amplitude' in attr:
				mc.setAttr('{}.{}'.format(ctrlNameSine[i], attr), 0.1)
			elif 'Wavelength' in attr:
				mc.setAttr('{}.{}'.format(ctrlNameSine[i], attr), 0.2)
			elif 'PositionTZ' in attr:
				mc.setAttr('{}.{}'.format(ctrlNameSine[i], attr), getTZ)
			elif 'PositionSY' in attr:
				mc.setAttr('{}.{}'.format(ctrlNameSine[i], attr), getSY)
			sineCtrlAttr.append('{}.{}'.format(ctrlNameSine[i], attr))


	return sineObj, sineCrv, sineCrvGrp, stickLipsCtrlGrp

def addBlendShapeCrv(curveBase = [], curveFun = [],*args):
	for i in range(len(curveBase)):
		mc.select(curveFun[i], r = True)
		mc.select(curveBase[i], add = True)
		lrr.doAddBlendShape()

def generateStickyLipsNode(control = [], *args):
	stickLipsStillGrp = mc.group(n = 'stickLipsStill_Grp', em = True)
	## generate distance
	distanceNode = mc.rename(mc.distanceDimension( sp=[0,0,0], ep=[0,0,1]).split('Shape1')[0]+'1', 'stickyLips_Dtn')
	locatorDis = ['locator1' , 'locator2']
	mc.parent(distanceNode, locatorDis, stickLipsStillGrp)
	newLoc = []
	for each in locatorDis:
		if mc.objExists(each):
			if '1' in each:
				stickyLipsRootName = mc.rename(each, 'stickyLipsRoot_Loc')
				mc.delete(mc.parentConstraint('Jaw3_Jnt', stickyLipsRootName, mo = False))
				utaCore.parentScaleConstraintLoop(obj = 'Head_Jnt', lists = [stickyLipsRootName],par = True, sca = True, ori = False, poi = False)
				newLoc.append(stickyLipsRootName)
			else: 
				stickyLipsEndName = mc.rename(each, 'stickyLipsEnd_Loc')
				mc.delete(mc.parentConstraint('Jaw3_Jnt', stickyLipsEndName, mo = False))
				utaCore.parentScaleConstraintLoop(obj = 'Jaw3_Jnt', lists = [stickyLipsEndName],par = True, sca = True, ori = False, poi = False)
				newLoc.append(stickyLipsEndName)

	for each in control:
		nameSp , side, lastName = utaCore.splitName(sels = [each])

		## generateNode
		setRangeNode = mc.createNode('setRange', n = '{}StickyLips{}Srn'.format(nameSp, side))
		stickyLipsOnOff_Node = mc.createNode('blendColors', n = '{}StickyLipsOnOff{}Bcl'.format(nameSp, side))
		stickyLipsSwitchNode = mc.createNode('blendColors', n = '{}StickyLipsSwitch{}Bcl'.format(nameSp, side))

		setRangeDynNode = mc.createNode('setRange', n = '{}StickyLipsDyn{}Srn'.format(nameSp, side))

		## setAttribute
		mc.setAttr('{}.oldMaxX'.format(setRangeNode), 10)
		mc.setAttr('{}.oldMaxZ'.format(setRangeNode), 10)
		mc.setAttr('{}.minZ'.format(setRangeNode), 1)
		mc.setAttr('{}.maxX'.format(setRangeNode), 1)

		mc.setAttr('{}.oldMaxX'.format(setRangeDynNode), 1)
		mc.setAttr('{}.minX'.format(setRangeDynNode), 10)

		## connectNode
		mc.connectAttr('{}.SL_On_Off'.format(each), '{}.valueX'.format(setRangeNode))
		mc.connectAttr('{}.SL_ManualDyn_Switch'.format(each), '{}.valueZ'.format(setRangeNode))
		mc.connectAttr('{}.outValueX'.format(setRangeNode), '{}.blender'.format(stickyLipsOnOff_Node))
		mc.connectAttr('{}.outValueZ'.format(setRangeNode), '{}.blender'.format(stickyLipsSwitchNode))
		mc.connectAttr('{}.outputR'.format(stickyLipsSwitchNode), '{}.color1R'.format(stickyLipsOnOff_Node))
		mc.connectAttr('{}.SL_Manual'.format(each), '{}.color1R'.format(stickyLipsSwitchNode))
		mc.connectAttr('{}.outputR'.format(stickyLipsOnOff_Node), '{}.sticky_Lips'.format(each))

		## connect Distance
		mc.connectAttr('{}.distance'.format(distanceNode), '{}.valueX'.format(setRangeDynNode))
		mc.connectAttr('{}.outValueX'.format(setRangeDynNode), '{}.color2R'.format(stickyLipsSwitchNode))

	return stickLipsStillGrp

def addAttrToCtrl(control = [], attr = [], value = [],*args):
	# value = [0, 4.5, 2.4]
	controlAttr = []
	for each in control:
		attr = utaCore.addAttrCtrl (ctrl = each, elem = attr, min = 0, max = 10, at = 'float')
		for eachAttr in attr:
			attrCtrl = '{}.{}'.format(each, eachAttr)
			# if not 'sticky_Lips' in eachAttr:
			controlAttr.append(attrCtrl)

					# pass
					# controlAttr.append(attrCtrl)
	# print controlAttr, '...ken'
	# for i in range(len(controlAttr)):
	# 	if '_L_' in controlAttr[i]:
	# 		if not 'sticky_Lips' in controlAttr[i]:
	# 			print controlAttr[i], '...controlAttr[i]'
	# 			print value[i], '..value[i]'
	# 			for x in range(len(value)):
	# 				mc.setAttr(controlAttr[i], value[x])
	# 				print controlAttr[i]
	return controlAttr

def generatelipRig(control = [], spans = 32, *args):

	for each in control:
		if '_L_' in each:
			if 'sticky_Lips' in each:
				lf_main_attr = each
		elif '_R_' in each:
			if 'sticky_Lips' in each:
				rt_main_attr = each

	## Geo Cv on curve
	# spans = mc.getAttr( 'upperLipBound_Crv.spans' )
	lip_val_list = [spans, spans]
	lip_name_list = ['upperLip', 'lowerLip']

	name_counter = 0
	for each in lip_val_list:
		half_val = (each / 2) + 1
		total_val = each + 1
		div_val = 10.0 / half_val
		counter = 0
		while(counter<half_val):
			lip_sr = pm.shadingNode( 'setRange', asUtility=True, n='lf_' + lip_name_list[name_counter] + str(counter+1) + '_setRange')
			pm.setAttr(lip_sr + '.oldMaxX', (div_val * (counter+1)))
			pm.setAttr(lip_sr + '.oldMinX', (div_val * counter))
			pm.setAttr(lip_sr + '.maxX', 0)
			pm.setAttr(lip_sr + '.minX', 1)
			if counter == (half_val - 1):
				pm.setAttr(lip_sr + '.minX', 0.5)
			pm.connectAttr(lf_main_attr, lip_sr + '.valueX', f=True)
			
			lip_flip_sr = pm.shadingNode( 'setRange', asUtility=True, n='lf_' + lip_name_list[name_counter] + '_flip' + str(counter+1) + '_setRange')
			pm.setAttr(lip_flip_sr + '.oldMaxX', 1)
			if counter == (half_val - 1):
				pm.setAttr(lip_flip_sr + '.oldMaxX', 0.5)
			pm.setAttr(lip_flip_sr + '.oldMinX', 0)
			pm.setAttr(lip_flip_sr + '.maxX', 0)
			pm.setAttr(lip_flip_sr + '.minX', 1)
			if counter == (half_val - 1):
				pm.setAttr(lip_flip_sr + '.minX', 0.5)
			pm.connectAttr(lip_sr + '.outValueX', lip_flip_sr + '.valueX', f=True)
				
			if counter == (half_val - 1):
				mid_pma = pm.shadingNode( 'plusMinusAverage', asUtility=True, n='ct_' + lip_name_list[name_counter] + str(counter+1) + '_plusMinusAverage')
				pm.connectAttr(lip_sr + '.outValueX', mid_pma + '.input2D[0].input2Dx', f=True)
				pm.connectAttr(lip_flip_sr + '.outValueX', mid_pma + '.input2D[0].input2Dy', f=True)
			else:
				pm.connectAttr(lip_sr + '.outValueX', lip_name_list[name_counter] + 'Wire_Bsn.inputTarget[0].inputTargetGroup[0].targetWeights[' + str(counter) + ']', f=True)
				pm.connectAttr(lip_flip_sr + '.outValueX', lip_name_list[name_counter] + 'Wire_Bsn.inputTarget[0].inputTargetGroup[1].targetWeights[' + str(counter) + ']', f=True)
			
			counter = counter + 1
			
		#div_val = 10.0 / 39
		counter = half_val - 1
		rev_counter = half_val
		while(counter<total_val):
			lip_sr = pm.shadingNode( 'setRange', asUtility=True, n='rt_' + lip_name_list[name_counter] + str(counter+1) + '_setRange')
			pm.setAttr(lip_sr + '.oldMaxX', (div_val * rev_counter))
			pm.setAttr(lip_sr + '.oldMinX', (div_val * (rev_counter-1)))
			pm.setAttr(lip_sr + '.maxX', 0)
			pm.setAttr(lip_sr + '.minX', 1)
			if counter == (half_val - 1):
				pm.setAttr(lip_sr + '.minX', 0.5)
			pm.connectAttr(rt_main_attr, lip_sr + '.valueX', f=True)
			
			lip_flip_sr = pm.shadingNode( 'setRange', asUtility=True, n='rt_' + lip_name_list[name_counter] + '_flip' + str(counter+1) + '_setRange')
			pm.setAttr(lip_flip_sr + '.oldMaxX', 1)
			if counter == (half_val - 1):
				pm.setAttr(lip_flip_sr + '.oldMaxX', 0.5)
			pm.setAttr(lip_flip_sr + '.oldMinX', 0)
			pm.setAttr(lip_flip_sr + '.maxX', 0)
			pm.setAttr(lip_flip_sr + '.minX', 1)
			if counter == (half_val - 1):
				pm.setAttr(lip_flip_sr + '.minX', 0.5)
			pm.connectAttr(lip_sr + '.outValueX', lip_flip_sr + '.valueX', f=True)
			
			if counter == (half_val - 1):
				pm.connectAttr(lip_sr + '.outValueX', mid_pma + '.input2D[1].input2Dx', f=True)
				pm.connectAttr(lip_flip_sr + '.outValueX', mid_pma + '.input2D[1].input2Dy', f=True)
				pm.connectAttr(mid_pma + '.output2Dx', lip_name_list[name_counter] + 'Wire_Bsn.inputTarget[0].inputTargetGroup[0].targetWeights[' + str(counter) + ']', f=True)
				pm.connectAttr(mid_pma + '.output2Dy', lip_name_list[name_counter] + 'Wire_Bsn.inputTarget[0].inputTargetGroup[1].targetWeights[' + str(counter) + ']', f=True)
			else:
				pm.connectAttr(lip_sr + '.outValueX', lip_name_list[name_counter] + 'Wire_Bsn.inputTarget[0].inputTargetGroup[0].targetWeights[' + str(counter) + ']', f=True)
				pm.connectAttr(lip_flip_sr + '.outValueX', lip_name_list[name_counter] + 'Wire_Bsn.inputTarget[0].inputTargetGroup[1].targetWeights[' + str(counter) + ']', f=True)
			
			counter = counter + 1
			rev_counter = rev_counter - 1
		name_counter = name_counter + 1