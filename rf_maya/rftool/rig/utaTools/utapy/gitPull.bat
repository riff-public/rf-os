#echo off

D:
set localgit=D:\Ken_Pipeline\core

cd %localgit%\rf_app
git pull

cd %localgit%\rf_config
git pull

cd %localgit%\rf_env
git pull

cd %localgit%\rf_launcher
git pull

cd %localgit%\rf_launcher_apps
git pull

cd %localgit%\rf_maya
git pull

cd %localgit%\rf_qc
git pull

cd %localgit%\rf_utils
git pull

cd %localgit%\rf_lib
git pull