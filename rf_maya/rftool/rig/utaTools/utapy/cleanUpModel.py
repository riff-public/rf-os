import maya.cmds as mc
reload(mc)
from utaTools.utapy import utaCore
reload(utaCore)
from utaTools.utapy import cleanUp
reload(cleanUp)

def cleanUpModel(*args):
    print "## FreeaTransForm"
    utaCore.clean ( target = '')
    print "## Delete 'Delete_Grp'"
    cleanUp.clearDeleteGrp()
    print "## Delete 'Layer'"
    cleanUp.clearLayer();
    print "## Delete 'UnuseAnim'"
    cleanUp.clearUnusedAnim();
    print "## Delete 'Unknownode'"
    cleanUp.clearUnknowNode();
    print "## Delete 'UnusedNode'"
    cleanUp.clearUnusedNode();
    print "## Delete 'VraySettings'"
    cleanUp.clearVraySettings()
    print "## Delete 'Camera'"
    cleanUp.clearCameraDefault()
