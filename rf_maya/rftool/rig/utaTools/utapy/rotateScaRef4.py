import maya.cmds as mc
reload(mc)
import lpRig.core as lrc
reload(lrc)

def rotateSca (elem = 'RotateSca', nameSp = 'ctrl:', *args):
	listsIkSca = mc.ls('ctrl:TailIkRZ*_Jnt')
	mainCtrl = mc.ls('ctrl:NakeeMove_Ctrl')
	listsIkScaCount =  len(listsIkSca)

	# ctrl 1 Connect
	fkCtrlLists = mc.ls('ctrl:TailFk_*_Ctrl')
	# pmaNodeLists = mc.ls('RotateSca*_Pma')
	# print pmaNodeLists

# Ctrl 1 ==========================================================================================================================
# Ctrl 1 ==========================================================================================================================
	ix = 0
	for each in range(0, 1):
		pmaNode = mc.createNode ('plusMinusAverage', n = ('%s%s_Pma' % (elem , ix+1)))
		mdvNode = mc.createNode ('multiplyDivide', n= ('%s%s_Mdv' % (elem , ix+1)))
		mc.connectAttr ('%s.rotateZ' % mainCtrl[0], '%s.input1X' % mdvNode)
		mc.connectAttr ('%s.outputX' % mdvNode, '%s.input1D[%s]'% (pmaNode,ix))
		mc.setAttr ('%s.input2X' % mdvNode, -1)
		mc.connectAttr ('%s.output1D' % pmaNode , '%s.rotateY' % listsIkSca[0])

		mdvNodeCha = mc.createNode ('multiplyDivide', n= ('%sCha%s_Mdv' % (elem , ix+1)))
		mc.setAttr ('%s.input1X' % mdvNodeCha, 0.75)
		mc.setAttr ('%s.input1Y' % mdvNodeCha, 0.5)
		mc.setAttr ('%s.input1Z' % mdvNodeCha, 0.25)
		mc.connectAttr('%s.rotateY' % listsIkSca[0], '%s.input2X' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[0], '%s.input2Y' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[0], '%s.input2Z' % mdvNodeCha)


		# Connect Scale  XYZ----------------------------------------------------------------------------
		# Connect Scale  XYZ----------------------------------------------------------------------------
		mc.connectAttr ('%s.scaleX' % mainCtrl[0], 'TailIkRZ%s_Jnt.scaleX' % ix+1)
		mc.connectAttr ('%s.scaleY' % mainCtrl[0], 'TailIkRZ%s_Jnt.scaleY' % ix+1)
		mc.connectAttr ('%s.scaleZ' % mainCtrl[0], 'TailIkRZ%s_Jnt.scaleZ' % ix+1)

		# mc.connectAttr ('%s.output3Dx' % pmaNode, '%sTailIkRZ%s_Jnt.scaleX' % (nameSp, ix+1))
		# mc.connectAttr ('%s.output3Dy' % pmaNode, '%sTailIkRZ%s_Jnt.scaleY' % (nameSp, ix+1))
		# mc.connectAttr ('%s.output3Dz' % pmaNode, '%sTailIkRZ%s_Jnt.scaleZ' % (nameSp, ix+1))
		# # Connect Scale  XYZ----------------------------------------------------------------------------
		# # Connect Scale  XYZ----------------------------------------------------------------------------


		# # Connect Mdv Node for Rotate
		# # Loop Example
		# iiimd = -1
		# bck =25
		# imd = 0
		# iimd = 1
		# value = [0.75, 0.5, 0.25]
		# attrSca = ['X', 'Y', 'Z']
		# for each in range(0, 3):
		# 	mdvNodeSca = mc.createNode ('multiplyDivide', n= ('%sCon%s_Mdv' % (elem , imd+1)))
		# 	for each in attrSca:
		# 		mc.connectAttr ('%sTailIkRZ%s_Jnt.scale%s' % (nameSp, ix+1, each) , '%s.input1%s' % (mdvNodeSca,each))
		# 		mc.setAttr ('%s.input2%s' % (mdvNodeSca, each), value[imd])	
				
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		addScale = mc.createNode ('plusMinusAverage', n = ('%sA%s%s_Pma' % (elem , imd+1, each)))			
		# 		mc.addAttr(ln='default', k=True, dv=value[iiimd])
		# 		addScaleBck = mc.createNode ('plusMinusAverage', n = ('%sAB%s%s%s_Pma' % (elem , bck-1,imd+1, each)))			
		# 		mc.addAttr(ln='defaultBck', k=True, dv=-2)
		# 		mc.connectAttr('%s.output%s' % (mdvNodeSca, each), '%s.input1D[%s]' % (addScale, 0))
		# 		mc.connectAttr('%s.default' % addScale, '%s.input1D[%s]' % (addScale, 1))


		# 		mc.connectAttr('%s.output1D' % addScale, '%s.input1D[%s]' % (addScaleBck, 0))


		# 		mc.connectAttr('%s.defaultBck' % addScaleBck, '%s.input1D[%s]' % (addScaleBck, 1))
		# 		mc.connectAttr('%s.output1D' % addScaleBck,'ctrl:TailIkRZ%s_Jnt.scale%s' % (iimd+1, each)) 
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 	bck-=1
		# 	iiimd-=1	
		# 	iimd+=1
		# 	imd+=1		
	
		# Connect Control 1 Rotate 
		mc.connectAttr ('%s.rotateZ' % fkCtrlLists[0], '%s.input1Y' % mdvNode)
		mc.connectAttr ('%s.outputY' % mdvNode , '%s.input1D[%s]' % (pmaNode,ix+1))
		mc.setAttr ('%s.input2Y' % mdvNode, -1)

		attr = ['X', 'Y', 'Z']
		iix = 1
		for attrs in attr:
			pmaNodeCha = mc.createNode ('plusMinusAverage', n = ('%sCha%s_Pma' % (elem , iix+1)))
			mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs) , '%s.input1D[0]' % pmaNodeCha)
			mc.connectAttr ('%s.output1D' % pmaNodeCha, 'ctrl:TailIkRZ%s_Jnt.rotateY' % (iix+1))
			iix+=1
		ix+=1


# Ctrl 2 ==========================================================================================================================
# Ctrl 2 ==========================================================================================================================
	ix = 4
	ikCtrl = mc.ls('ctrl:TailIk2_Ctrl')
	for each in range(0, 1):
		pmaNode = mc.createNode ('plusMinusAverage', n = ('%s%s_Pma' % (elem , ix+1)))
		mc.connectAttr ('%s.rotateY' % ikCtrl[0], '%s.input1D[0]' % pmaNode)
		mc.connectAttr ('%s.output1D' % pmaNode , '%s.rotateY' % listsIkSca[4])
		mdvNodeCha = mc.createNode ('multiplyDivide', n= ('%sCha%s_Mdv' % (elem , ix+1)))
		mc.setAttr ('%s.input1X' % mdvNodeCha, 0.75)
		mc.setAttr ('%s.input1Y' % mdvNodeCha, 0.5)
		mc.setAttr ('%s.input1Z' % mdvNodeCha, 0.25)
		mc.connectAttr('%s.rotateY' % listsIkSca[4], '%s.input2X' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[4], '%s.input2Y' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[4], '%s.input2Z' % mdvNodeCha)

		# # Connect Control 2 Rotate 
		# mdvRotNode = mc.createNode ('multiplyDivide', n= ('%s%sRot_Mdv' % (elem , ix+1)))
		# mc.connectAttr ('%s.rotateZ' % fkCtrlLists[0], '%s.input1X' % mdvRotNode)
		# mc.connectAttr ('%s.outputX' % mdvRotNode , '%s.input1D[1]' % pmaNode)
		# mc.setAttr ('%s.input2X' % mdvRotNode, -1)

		# # Connect Scale  XYZ----------------------------------------------------------------------------
		# # Connect Scale  XYZ----------------------------------------------------------------------------
		# mc.connectAttr ('%s.scaleX' % ikCtrl[0], '%s.input3D[%s].input3Dx'% (pmaNode,ix))
		# mc.connectAttr ('%s.scaleY' % ikCtrl[0], '%s.input3D[%s].input3Dy'% (pmaNode,ix))
		# mc.connectAttr ('%s.scaleZ' % ikCtrl[0], '%s.input3D[%s].input3Dz'% (pmaNode,ix))

		# mc.connectAttr ('%s.output3Dx' % pmaNode, '%sTailIkRZ%s_Jnt.scaleX' % (nameSp, ix+1))
		# mc.connectAttr ('%s.output3Dy' % pmaNode, '%sTailIkRZ%s_Jnt.scaleY' % (nameSp, ix+1))
		# mc.connectAttr ('%s.output3Dz' % pmaNode, '%sTailIkRZ%s_Jnt.scaleZ' % (nameSp, ix+1))
		# # Connect Scale  XYZ----------------------------------------------------------------------------
		# # Connect Scale  XYZ----------------------------------------------------------------------------


		# Lists All Control Rotate ------------------------------
		# Lists All Control Rotate ------------------------------
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[1], '%s.input1D[2]' % pmaNode)






		# # Connect Mdv Node for Rotate
		# # Loop Example
		# iiimd = -1
		# bck = 25
		# imd = 0
		# iimd = 5
		# iiiimd = 4
		# value = [0.75, 0.5, 0.25]
		# attrSca = ['X', 'Y', 'Z']
		# for each in range(0, 3):
		# 	mdvNodeSca = mc.createNode ('multiplyDivide', n= ('%sCon%s_Mdv' % (elem , imd+1)))
		# 	for each in attrSca:
		# 		mc.connectAttr ('%sTailIkRZ%s_Jnt.scale%s' % (nameSp, ix+1, each) , '%s.input1%s' % (mdvNodeSca,each))
		# 		mc.setAttr ('%s.input2%s' % (mdvNodeSca, each), value[imd])	
				
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		addScale = mc.createNode ('plusMinusAverage', n = ('%sB%s%s_Pma' % (elem , imd+1, each)))
		# 		mc.addAttr(ln='default', k=True, dv=value[iiimd])	
		# 		addScaleBck = mc.createNode ('plusMinusAverage', n = ('%sBB%s%s%s_Pma' % (elem , bck-1, imd+1, each)))			
		# 		mc.addAttr(ln='defaultBck', k=True, dv=-1)
		# 		mc.connectAttr('%s.output%s' % (mdvNodeSca, each), '%s.input1D[%s]' % (addScale, 0))
		# 		mc.connectAttr('%s.default' % addScale, '%s.input1D[%s]' % (addScale, 1))
		# 		mc.connectAttr('%s.output1D' % addScale, '%s.input1D[%s]' % (addScaleBck, 0))

		# 		# Connect Scale Back-----------------------------------
		# 		mc.connectAttr('%s.defaultBck' % addScaleBck, '%s.input1D[%s]' % (addScaleBck, 1))
		# 		mc.connectAttr('%s.output1D' % addScaleBck, '%sA%s%s_Pma.input1D[%s]' % (elem, iiiimd-1, each, 2))
		# 		mc.connectAttr('%s.output1D' % addScaleBck, 'ctrl:TailIkRZ%s_Jnt.scale%s' % (iimd+1, each)) 

		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 	bck-=1
		# 	iiiimd-=1
		# 	iiimd-=1	
		# 	iimd+=1
		# 	imd+=1	










		attr = ['X', 'Y', 'Z']
		iix = 5
		for attrs in attr:
			pmaNodeCha = mc.createNode ('plusMinusAverage', n = ('%sCha%s_Pma' % (elem , iix+1)))
			mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs) , '%s.input1D[0]' % pmaNodeCha)
			mc.connectAttr ('%s.output1D' % pmaNodeCha, 'ctrl:TailIkRZ%s_Jnt.rotateY' % (iix+1))
			iix+=1
		ix+=1

	# Ctrl 2 Bck----------------------------------------
	attr = ['X', 'Y', 'Z']
	iix = 5
	for attrs in attr:
		mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs), '%sCha%s_Pma.input1D[1]' % (elem,iix-1))
		iix-=1



# Ctrl 3 ==========================================================================================================================
# Ctrl 3 ==========================================================================================================================
	ix = 8
	ikCtrl = mc.ls('ctrl:TailIk3_Ctrl')
	for each in range(0, 1):
		pmaNode = mc.createNode ('plusMinusAverage', n = ('%s%s_Pma' % (elem , ix+1)))
		mc.connectAttr ('%s.rotateY' % ikCtrl[0], '%s.input1D[0]' % pmaNode)
		mc.connectAttr ('%s.output1D' % pmaNode , '%s.rotateY' % listsIkSca[8])
		mdvNodeCha = mc.createNode ('multiplyDivide', n= ('%sCha%s_Mdv' % (elem , ix+1)))
		mc.setAttr ('%s.input2X' % mdvNodeCha, 0.75)
		mc.setAttr ('%s.input2Y' % mdvNodeCha, 0.5)
		mc.setAttr ('%s.input2Z' % mdvNodeCha, 0.25)
		mc.connectAttr('%s.rotateY' % listsIkSca[8], '%s.input1X' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[8], '%s.input1Y' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[8], '%s.input1Z' % mdvNodeCha)

		# Connect Control 3 Rotate 
		mdvRotNode = mc.createNode ('multiplyDivide', n= ('%s%sRot_Mdv' % (elem , ix+1)))
		mc.connectAttr ('%s.rotateZ' % fkCtrlLists[0], '%s.input1X' % mdvRotNode)
		mc.connectAttr ('%s.outputX' % mdvRotNode , '%s.input1D[1]' % pmaNode)
		mc.setAttr ('%s.input2X' % mdvRotNode, -1)

		# # Connect Scale  XYZ----------------------------------------------------------------------------
		# # Connect Scale  XYZ----------------------------------------------------------------------------
		# mc.connectAttr ('%s.scaleX' % ikCtrl[0], '%s.input3D[%s].input3Dx'% (pmaNode,ix))
		# mc.connectAttr ('%s.scaleY' % ikCtrl[0], '%s.input3D[%s].input3Dy'% (pmaNode,ix))
		# mc.connectAttr ('%s.scaleZ' % ikCtrl[0], '%s.input3D[%s].input3Dz'% (pmaNode,ix))

		# mc.connectAttr ('%s.output3Dx' % pmaNode, '%sTailIkRZ%s_Jnt.scaleX' % (nameSp, ix+1))
		# mc.connectAttr ('%s.output3Dy' % pmaNode, '%sTailIkRZ%s_Jnt.scaleY' % (nameSp, ix+1))
		# mc.connectAttr ('%s.output3Dz' % pmaNode, '%sTailIkRZ%s_Jnt.scaleZ' % (nameSp, ix+1))
		# # Connect Scale  XYZ----------------------------------------------------------------------------
		# # Connect Scale  XYZ----------------------------------------------------------------------------

		# Lists All Control Rotate ------------------------------
		# Lists All Control Rotate ------------------------------
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[1], '%s.input1D[2]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[2], '%s.input1D[3]' % pmaNode)



		# # Connect Mdv Node for Rotate
		# # Loop Example
		# iiimd = -1
		# bck = 25
		# imd = 0
		# iimd = 9
		# iiiimd = 4
		# value = [0.75, 0.5, 0.25]
		# attrSca = ['X', 'Y', 'Z']
		# for each in range(0, 3):
		# 	mdvNodeSca = mc.createNode ('multiplyDivide', n= ('%sCon%s_Mdv' % (elem , imd+1)))
		# 	for each in attrSca:
		# 		mc.connectAttr ('%sTailIkRZ%s_Jnt.scale%s' % (nameSp, ix+1, each) , '%s.input1%s' % (mdvNodeSca,each))
		# 		mc.setAttr ('%s.input2%s' % (mdvNodeSca, each), value[imd])	
				
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		addScale = mc.createNode ('plusMinusAverage', n = ('%sC%s%s_Pma' % (elem , imd+1, each)))
		# 		mc.addAttr(ln='default', k=True, dv=value[iiimd])	
		# 		addScaleBck = mc.createNode ('plusMinusAverage', n = ('%sCB%s%s%s_Pma' % (elem , bck-1, imd+1, each)))			
		# 		mc.addAttr(ln='defaultBck', k=True, dv=0)
		# 		mc.connectAttr('%s.output%s' % (mdvNodeSca, each), '%s.input1D[%s]' % (addScale, 0))
		# 		mc.connectAttr('%s.default' % addScale, '%s.input1D[%s]' % (addScale, 1))
		# 		mc.connectAttr('%s.output1D' % addScale, '%s.input1D[%s]' % (addScaleBck, 0))

		# 		# Connect Scale Back-----------------------------------
		# 		mc.connectAttr('%s.defaultBck' % addScaleBck, '%s.input1D[%s]' % (addScaleBck, 1))
		# 		# mc.connectAttr('%s.output1D' % addScaleBck, '%sB%s%s_Pma.input1D[%s]' % (elem, iiiimd-1, each, 2))
		# 		mc.connectAttr('%s.output1D' % addScaleBck, 'ctrl:TailIkRZ%s_Jnt.scale%s' % (iimd+1, each)) 

		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 	bck-=1
		# 	iiiimd-=1
		# 	iiimd-=1	
		# 	iimd+=1
		# 	imd+=1	




		attr = ['X', 'Y', 'Z']
		iix = 9
		for attrs in attr:
			pmaNodeCha = mc.createNode ('plusMinusAverage', n = ('%sCha%s_Pma' % (elem , iix+1)))
			mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs) , '%s.input1D[0]' % pmaNodeCha)
			mc.connectAttr ('%s.output1D' % pmaNodeCha, 'ctrl:TailIkRZ%s_Jnt.rotateY' % (iix+1))
			iix+=1
		ix+=1

	# Ctrl 3 Bck----------------------------------------
	attr = ['X', 'Y', 'Z']
	iix = 9
	for attrs in attr:
		mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs), '%sCha%s_Pma.input1D[1]' % (elem,iix-1))
		iix-=1

# Ctrl 4 ==========================================================================================================================
# Ctrl 4 ==========================================================================================================================
	ix = 12
	ikCtrl = mc.ls('ctrl:TailIk4_Ctrl')
	for each in range(0, 1):
		pmaNode = mc.createNode ('plusMinusAverage', n = ('%s%s_Pma' % (elem , ix+1)))
		mc.connectAttr ('%s.rotateY' % ikCtrl[0], '%s.input1D[0]' % pmaNode)
		mc.connectAttr ('%s.output1D' % pmaNode , '%s.rotateY' % listsIkSca[12])
		mdvNodeCha = mc.createNode ('multiplyDivide', n= ('%sCha%s_Mdv' % (elem , ix+1)))
		mc.setAttr ('%s.input2X' % mdvNodeCha, 0.75)
		mc.setAttr ('%s.input2Y' % mdvNodeCha, 0.5)
		mc.setAttr ('%s.input2Z' % mdvNodeCha, 0.25)
		mc.connectAttr('%s.rotateY' % listsIkSca[12], '%s.input1X' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[12], '%s.input1Y' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[12], '%s.input1Z' % mdvNodeCha)

		# Connect Control 4 Rotate 
		mdvRotNode = mc.createNode ('multiplyDivide', n= ('%s%sRot_Mdv' % (elem , ix+1)))
		mc.connectAttr ('%s.rotateZ' % fkCtrlLists[0], '%s.input1X' % mdvRotNode)
		mc.connectAttr ('%s.outputX' % mdvRotNode , '%s.input1D[1]' % pmaNode)
		mc.setAttr ('%s.input2X' % mdvRotNode, -1)

		# # Connect Scale  XYZ----------------------------------------------------------------------------
		# # Connect Scale  XYZ----------------------------------------------------------------------------
		# mc.connectAttr ('%s.scaleX' % ikCtrl[0], '%s.input3D[%s].input3Dx'% (pmaNode,ix))
		# mc.connectAttr ('%s.scaleY' % ikCtrl[0], '%s.input3D[%s].input3Dy'% (pmaNode,ix))
		# mc.connectAttr ('%s.scaleZ' % ikCtrl[0], '%s.input3D[%s].input3Dz'% (pmaNode,ix))

		# mc.connectAttr ('%s.output3Dx' % pmaNode, '%sTailIkRZ%s_Jnt.scaleX' % (nameSp, ix+1))
		# mc.connectAttr ('%s.output3Dy' % pmaNode, '%sTailIkRZ%s_Jnt.scaleY' % (nameSp, ix+1))
		# mc.connectAttr ('%s.output3Dz' % pmaNode, '%sTailIkRZ%s_Jnt.scaleZ' % (nameSp, ix+1))
		# # Connect Scale  XYZ----------------------------------------------------------------------------
		# # Connect Scale  XYZ----------------------------------------------------------------------------

		# Lists All Control Rotate ------------------------------
		# Lists All Control Rotate ------------------------------
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[1], '%s.input1D[2]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[2], '%s.input1D[3]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[3], '%s.input1D[4]' % pmaNode)



		# # Connect Mdv Node for Rotate
		# # Loop Example
		# iiimd = -1
		# imd = 0
		# iimd = 13
		# iiiimd = 4
		# value = [0.75, 0.5, 0.25]
		# attrSca = ['X', 'Y', 'Z']
		# for each in range(0, 3):
		# 	mdvNodeSca = mc.createNode ('multiplyDivide', n= ('%sCon%s_Mdv' % (elem , imd+1)))
		# 	for each in attrSca:
		# 		mc.connectAttr ('%sTailIkRZ%s_Jnt.scale%s' % (nameSp, ix+1, each) , '%s.input1%s' % (mdvNodeSca,each))
		# 		mc.setAttr ('%s.input2%s' % (mdvNodeSca, each), value[imd])	
				
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		addScale = mc.createNode ('plusMinusAverage', n = ('%sD%s%s_Pma' % (elem , imd+1, each)))
		# 		mc.addAttr(ln='default', k=True, dv=value[iiimd])	
		# 		addScaleBck = mc.createNode ('plusMinusAverage', n = ('%sDB%s%s_Pma' % (elem , imd+1, each)))			
		# 		mc.addAttr(ln='defaultBck', k=True, dv=-1)
		# 		mc.connectAttr('%s.output%s' % (mdvNodeSca, each), '%s.input1D[%s]' % (addScale, 0))
		# 		mc.connectAttr('%s.default' % addScale, '%s.input1D[%s]' % (addScale, 1))
		# 		mc.connectAttr('%s.output1D' % addScale, '%s.input1D[%s]' % (addScaleBck, 0))

		# 		# Connect Scale Back-----------------------------------
		# 		# mc.connectAttr('%s.output1D' % addScale, '%s.input1D[%s]' % (addScaleBck, 0))
		# 		mc.connectAttr('%s.defaultBck' % addScaleBck, '%s.input1D[%s]' % (addScaleBck, 1))
		# 		mc.connectAttr('%s.output1D' % addScaleBck, '%sC%s%s_Pma.input1D[%s]' % (elem, iiiimd-1, each, 2))
		# 													# '%sAB%s%s_Pma' % (elem , imd+1, each)))
		# 		mc.connectAttr('%s.output1D' % addScaleBck, 'ctrl:TailIkRZ%s_Jnt.scale%s' % (iimd+1, each)) 

		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 	iiiimd-=1
		# 	iiimd-=1	
		# 	iimd+=1
		# 	imd+=1




		attr = ['X', 'Y', 'Z']
		iix = 13
		for attrs in attr:
			pmaNodeCha = mc.createNode ('plusMinusAverage', n = ('%sCha%s_Pma' % (elem , iix+1)))
			mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs) , '%s.input1D[0]' % pmaNodeCha)
			mc.connectAttr ('%s.output1D' % pmaNodeCha, 'ctrl:TailIkRZ%s_Jnt.rotateY' % (iix+1))
			iix+=1
		ix+=1
	# Ctrl 4 Bck----------------------------------------
	attr = ['X', 'Y', 'Z']
	iix = 13
	for attrs in attr:
		mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs), '%sCha%s_Pma.input1D[1]' % (elem,iix-1))
		iix-=1

# Ctrl 5 ==========================================================================================================================
# Ctrl 5 ==========================================================================================================================
	ix = 16
	ikCtrl = mc.ls('ctrl:TailIk5_Ctrl')
	for each in range(0, 1):
		pmaNode = mc.createNode ('plusMinusAverage', n = ('%s%s_Pma' % (elem , ix+1)))
		mc.connectAttr ('%s.rotateY' % ikCtrl[0], '%s.input1D[0]' % pmaNode)
		mc.connectAttr ('%s.output1D' % pmaNode , '%s.rotateY' % listsIkSca[16])
		mdvNodeCha = mc.createNode ('multiplyDivide', n= ('%sCha%s_Mdv' % (elem , ix+1)))
		mc.setAttr ('%s.input2X' % mdvNodeCha, 0.75)
		mc.setAttr ('%s.input2Y' % mdvNodeCha, 0.5)
		mc.setAttr ('%s.input2Z' % mdvNodeCha, 0.25)
		mc.connectAttr('%s.rotateY' % listsIkSca[16], '%s.input1X' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[16], '%s.input1Y' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[16], '%s.input1Z' % mdvNodeCha)

		# Connect Control 5 Rotate 
		mdvRotNode = mc.createNode ('multiplyDivide', n= ('%s%sRot_Mdv' % (elem , ix+1)))
		mc.connectAttr ('%s.rotateZ' % fkCtrlLists[0], '%s.input1X' % mdvRotNode)
		mc.connectAttr ('%s.outputX' % mdvRotNode , '%s.input1D[1]' % pmaNode)
		mc.setAttr ('%s.input2X' % mdvRotNode, -1)

		# Connect Scale  XYZ----------------------------------------------------------------------------
		# Connect Scale  XYZ----------------------------------------------------------------------------
		mc.connectAttr ('%s.scaleX' % ikCtrl[0], '%s.input3D[%s].input3Dx'% (pmaNode,ix))
		mc.connectAttr ('%s.scaleY' % ikCtrl[0], '%s.input3D[%s].input3Dy'% (pmaNode,ix))
		mc.connectAttr ('%s.scaleZ' % ikCtrl[0], '%s.input3D[%s].input3Dz'% (pmaNode,ix))

		mc.connectAttr ('%s.output3Dx' % pmaNode, '%sTailIkRZ%s_Jnt.scaleX' % (nameSp, ix+1))
		mc.connectAttr ('%s.output3Dy' % pmaNode, '%sTailIkRZ%s_Jnt.scaleY' % (nameSp, ix+1))
		mc.connectAttr ('%s.output3Dz' % pmaNode, '%sTailIkRZ%s_Jnt.scaleZ' % (nameSp, ix+1))
		# Connect Scale  XYZ----------------------------------------------------------------------------
		# Connect Scale  XYZ----------------------------------------------------------------------------

		# Lists All Control Rotate ------------------------------
		# Lists All Control Rotate ------------------------------
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[1], '%s.input1D[2]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[2], '%s.input1D[3]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[3], '%s.input1D[4]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[4], '%s.input1D[5]' % pmaNode)


		# # Connect Mdv Node for Rotate
		# # Loop Example
		# iimd = 17
		# iiimd = -1
		# imd = 0
		# iiiimd = 4
		# value = [0.75, 0.5, 0.25]
		# attrSca = ['X', 'Y', 'Z']
		# for each in range(0, 3):
		# 	mdvNodeSca = mc.createNode ('multiplyDivide', n= ('%sCon%s_Mdv' % (elem , imd+1)))
		# 	for each in attrSca:
		# 		mc.connectAttr ('%sTailIkRZ%s_Jnt.scale%s' % (nameSp, ix+1, each) , '%s.input1%s' % (mdvNodeSca,each))
		# 		mc.setAttr ('%s.input2%s' % (mdvNodeSca, each), value[imd])	
				
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		addScale = mc.createNode ('plusMinusAverage', n = ('%sE%s%s_Pma' % (elem , imd+1, each)))
		# 		mc.addAttr(ln='default', k=True, dv=value[iiimd])	
		# 		addScaleBck = mc.createNode ('plusMinusAverage', n = ('%sEB%s%s_Pma' % (elem , imd+1, each)))			
		# 		mc.addAttr(ln='defaultBck', k=True, dv=-1)
		# 		mc.connectAttr('%s.output%s' % (mdvNodeSca, each), '%s.input1D[%s]' % (addScale, 0))
		# 		mc.connectAttr('%s.default' % addScale, '%s.input1D[%s]' % (addScale, 1))
		# 		mc.connectAttr('%s.output1D' % addScale, '%s.input1D[%s]' % (addScaleBck, 0))

		# 		# Connect Scale Back-----------------------------------
		# 		# mc.connectAttr('%s.output1D' % addScale, '%s.input1D[%s]' % (addScaleBck, 0))
		# 		mc.connectAttr('%s.defaultBck' % addScaleBck, '%s.input1D[%s]' % (addScaleBck, 1))
		# 		mc.connectAttr('%s.output1D' % addScaleBck, '%sD%s%s_Pma.input1D[%s]' % (elem, iiiimd-1, each, 2))
		# 													# '%sAB%s%s_Pma' % (elem , imd+1, each)))
		# 		mc.connectAttr('%s.output1D' % addScaleBck, 'ctrl:TailIkRZ%s_Jnt.scale%s' % (iimd+1, each)) 

		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 	iiiimd-=1
		# 	iiimd-=1	
		# 	iimd+=1
		# 	imd+=1



		attr = ['X', 'Y', 'Z']
		iix = 17
		for attrs in attr:
			pmaNodeCha = mc.createNode ('plusMinusAverage', n = ('%sCha%s_Pma' % (elem , iix+1)))
			mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs) , '%s.input1D[0]' % pmaNodeCha)
			mc.connectAttr ('%s.output1D' % pmaNodeCha, 'ctrl:TailIkRZ%s_Jnt.rotateY' % (iix+1))
			iix+=1
		ix+=1
	# Ctrl 5 Bck----------------------------------------
	attr = ['X', 'Y', 'Z']
	iix = 17
	for attrs in attr:
		mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs), '%sCha%s_Pma.input1D[1]' % (elem,iix-1))
		iix-=1

# Ctrl 6 ==========================================================================================================================
# Ctrl 6 ==========================================================================================================================
	ix = 20
	ikCtrl = mc.ls('ctrl:TailIk6_Ctrl')
	for each in range(0, 1):
		pmaNode = mc.createNode ('plusMinusAverage', n = ('%s%s_Pma' % (elem , ix+1)))
		mc.connectAttr ('%s.rotateY' % ikCtrl[0], '%s.input1D[0]' % pmaNode)
		mc.connectAttr ('%s.output1D' % pmaNode , '%s.rotateY' % listsIkSca[20])
		mdvNodeCha = mc.createNode ('multiplyDivide', n= ('%sCha%s_Mdv' % (elem , ix+1)))
		mc.setAttr ('%s.input2X' % mdvNodeCha, 0.75)
		mc.setAttr ('%s.input2Y' % mdvNodeCha, 0.5)
		mc.setAttr ('%s.input2Z' % mdvNodeCha, 0.25)
		mc.connectAttr('%s.rotateY' % listsIkSca[20], '%s.input1X' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[20], '%s.input1Y' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[20], '%s.input1Z' % mdvNodeCha)

		# Connect Control 6 Rotate 
		mdvRotNode = mc.createNode ('multiplyDivide', n= ('%s%sRot_Mdv' % (elem , ix+1)))
		mc.connectAttr ('%s.rotateZ' % fkCtrlLists[0], '%s.input1X' % mdvRotNode)
		mc.connectAttr ('%s.outputX' % mdvRotNode , '%s.input1D[1]' % pmaNode)
		mc.setAttr ('%s.input2X' % mdvRotNode, -1)

		# Connect Scale  XYZ----------------------------------------------------------------------------
		# Connect Scale  XYZ----------------------------------------------------------------------------
		mc.connectAttr ('%s.scaleX' % ikCtrl[0], '%s.input3D[%s].input3Dx'% (pmaNode,ix))
		mc.connectAttr ('%s.scaleY' % ikCtrl[0], '%s.input3D[%s].input3Dy'% (pmaNode,ix))
		mc.connectAttr ('%s.scaleZ' % ikCtrl[0], '%s.input3D[%s].input3Dz'% (pmaNode,ix))

		mc.connectAttr ('%s.output3Dx' % pmaNode, '%sTailIkRZ%s_Jnt.scaleX' % (nameSp, ix+1))
		mc.connectAttr ('%s.output3Dy' % pmaNode, '%sTailIkRZ%s_Jnt.scaleY' % (nameSp, ix+1))
		mc.connectAttr ('%s.output3Dz' % pmaNode, '%sTailIkRZ%s_Jnt.scaleZ' % (nameSp, ix+1))
		# Connect Scale  XYZ----------------------------------------------------------------------------
		# Connect Scale  XYZ----------------------------------------------------------------------------

		# Lists All Control Rotate ------------------------------
		# Lists All Control Rotate ------------------------------
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[1], '%s.input1D[2]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[2], '%s.input1D[3]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[3], '%s.input1D[4]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[4], '%s.input1D[5]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[5], '%s.input1D[6]' % pmaNode)


		# # Connect Mdv Node for Rotate
		# # Loop Example
		# iimd = 21
		# iiimd = -1
		# imd = 0
		# iiiimd = 4
		# value = [0.75, 0.5, 0.25]
		# attrSca = ['X', 'Y', 'Z']
		# for each in range(0, 3):
		# 	mdvNodeSca = mc.createNode ('multiplyDivide', n= ('%sCon%s_Mdv' % (elem , imd+1)))
		# 	for each in attrSca:
		# 		mc.connectAttr ('%sTailIkRZ%s_Jnt.scale%s' % (nameSp, ix+1, each) , '%s.input1%s' % (mdvNodeSca,each))
		# 		mc.setAttr ('%s.input2%s' % (mdvNodeSca, each), value[imd])	
				
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		addScale = mc.createNode ('plusMinusAverage', n = ('%sF%s%s_Pma' % (elem , imd+1, each)))
		# 		mc.addAttr(ln='default', k=True, dv=value[iiimd])	
		# 		addScaleBck = mc.createNode ('plusMinusAverage', n = ('%sFB%s%s_Pma' % (elem , imd+1, each)))			
		# 		mc.addAttr(ln='defaultBck', k=True, dv=-1)
		# 		mc.connectAttr('%s.output%s' % (mdvNodeSca, each), '%s.input1D[%s]' % (addScale, 0))
		# 		mc.connectAttr('%s.default' % addScale, '%s.input1D[%s]' % (addScale, 1))
		# 		mc.connectAttr('%s.output1D' % addScale, '%s.input1D[%s]' % (addScaleBck, 0))

		# 		# Connect Scale Back-----------------------------------
		# 		# mc.connectAttr('%s.output1D' % addScale, '%s.input1D[%s]' % (addScaleBck, 0))
		# 		mc.connectAttr('%s.defaultBck' % addScaleBck, '%s.input1D[%s]' % (addScaleBck, 1))
		# 		mc.connectAttr('%s.output1D' % addScaleBck, '%sE%s%s_Pma.input1D[%s]' % (elem, iiiimd-1, each, 2))
		# 													# '%sAB%s%s_Pma' % (elem , imd+1, each)))
		# 		mc.connectAttr('%s.output1D' % addScaleBck, 'ctrl:TailIkRZ%s_Jnt.scale%s' % (iimd+1, each)) 

		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 	iiiimd-=1
		# 	iiimd-=1	
		# 	iimd+=1
		# 	imd+=1


		attr = ['X', 'Y', 'Z']
		iix = 21
		for attrs in attr:
			pmaNodeCha = mc.createNode ('plusMinusAverage', n = ('%sCha%s_Pma' % (elem , iix+1)))
			mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs) , '%s.input1D[0]' % pmaNodeCha)
			mc.connectAttr ('%s.output1D' % pmaNodeCha, 'ctrl:TailIkRZ%s_Jnt.rotateY' % (iix+1))
			iix+=1
		ix+=1
	# Ctrl 6 Bck----------------------------------------
	attr = ['X', 'Y', 'Z']
	iix = 21
	for attrs in attr:
		mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs), '%sCha%s_Pma.input1D[1]' % (elem,iix-1))
		iix-=1

# Ctrl 7 ==========================================================================================================================
# Ctrl 7 ==========================================================================================================================
	ix = 24
	ikCtrl = mc.ls('ctrl:TailIk7_Ctrl')
	for each in range(0, 1):
		pmaNode = mc.createNode ('plusMinusAverage', n = ('%s%s_Pma' % (elem , ix+1)))
		mc.connectAttr ('%s.rotateY' % ikCtrl[0], '%s.input1D[0]' % pmaNode)
		mc.connectAttr ('%s.output1D' % pmaNode , '%s.rotateY' % listsIkSca[24])
		mdvNodeCha = mc.createNode ('multiplyDivide', n= ('%sCha%s_Mdv' % (elem , ix+1)))
		mc.setAttr ('%s.input2X' % mdvNodeCha, 0.75)
		mc.setAttr ('%s.input2Y' % mdvNodeCha, 0.5)
		mc.setAttr ('%s.input2Z' % mdvNodeCha, 0.24)
		mc.connectAttr('%s.rotateY' % listsIkSca[24], '%s.input1X' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[24], '%s.input1Y' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[24], '%s.input1Z' % mdvNodeCha)

		# Connect Control 7 Rotate 
		mdvRotNode = mc.createNode ('multiplyDivide', n= ('%s%sRot_Mdv' % (elem , ix+1)))
		mc.connectAttr ('%s.rotateZ' % fkCtrlLists[0], '%s.input1X' % mdvRotNode)
		mc.connectAttr ('%s.outputX' % mdvRotNode , '%s.input1D[1]' % pmaNode)
		mc.setAttr ('%s.input2X' % mdvRotNode, -1)

		# Connect Scale  XYZ----------------------------------------------------------------------------
		# Connect Scale  XYZ----------------------------------------------------------------------------
		mc.connectAttr ('%s.scaleX' % ikCtrl[0], '%s.input3D[%s].input3Dx'% (pmaNode,ix))
		mc.connectAttr ('%s.scaleY' % ikCtrl[0], '%s.input3D[%s].input3Dy'% (pmaNode,ix))
		mc.connectAttr ('%s.scaleZ' % ikCtrl[0], '%s.input3D[%s].input3Dz'% (pmaNode,ix))

		mc.connectAttr ('%s.output3Dx' % pmaNode, '%sTailIkRZ%s_Jnt.scaleX' % (nameSp, ix+1))
		mc.connectAttr ('%s.output3Dy' % pmaNode, '%sTailIkRZ%s_Jnt.scaleY' % (nameSp, ix+1))
		mc.connectAttr ('%s.output3Dz' % pmaNode, '%sTailIkRZ%s_Jnt.scaleZ' % (nameSp, ix+1))
		# Connect Scale  XYZ----------------------------------------------------------------------------
		# Connect Scale  XYZ----------------------------------------------------------------------------

		# Lists All Control Rotate ------------------------------
		# Lists All Control Rotate ------------------------------
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[1], '%s.input1D[2]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[2], '%s.input1D[3]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[3], '%s.input1D[4]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[4], '%s.input1D[5]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[5], '%s.input1D[6]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[6], '%s.input1D[7]' % pmaNode)


		# # Connect Mdv Node for Rotate
		# # Loop Example
		# iimd = 25
		# iiimd = -1
		# imd = 0
		# iiiimd = 4
		# value = [0.75, 0.5, 0.25]
		# attrSca = ['X', 'Y', 'Z']
		# for each in range(0, 3):
		# 	mdvNodeSca = mc.createNode ('multiplyDivide', n= ('%sCon%s_Mdv' % (elem , imd+1)))
		# 	for each in attrSca:
		# 		mc.connectAttr ('%sTailIkRZ%s_Jnt.scale%s' % (nameSp, ix+1, each) , '%s.input1%s' % (mdvNodeSca,each))
		# 		mc.setAttr ('%s.input2%s' % (mdvNodeSca, each), value[imd])	
				
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		addScale = mc.createNode ('plusMinusAverage', n = ('%sG%s%s_Pma' % (elem , imd+1, each)))
		# 		mc.addAttr(ln='default', k=True, dv=value[iiimd])	
		# 		addScaleBck = mc.createNode ('plusMinusAverage', n = ('%sGB%s%s_Pma' % (elem , imd+1, each)))			
		# 		mc.addAttr(ln='defaultBck', k=True, dv=-1)
		# 		mc.connectAttr('%s.output%s' % (mdvNodeSca, each), '%s.input1D[%s]' % (addScale, 0))
		# 		mc.connectAttr('%s.default' % addScale, '%s.input1D[%s]' % (addScale, 1))
		# 		mc.connectAttr('%s.output1D' % addScale, '%s.input1D[%s]' % (addScaleBck, 0))

		# 		# Connect Scale Back-----------------------------------
		# 		# mc.connectAttr('%s.output1D' % addScale, '%s.input1D[%s]' % (addScaleBck, 0))
		# 		mc.connectAttr('%s.defaultBck' % addScaleBck, '%s.input1D[%s]' % (addScaleBck, 1))
		# 		mc.connectAttr('%s.output1D' % addScaleBck, '%sF%s%s_Pma.input1D[%s]' % (elem, iiiimd-1, each, 2))
		# 													# '%sAB%s%s_Pma' % (elem , imd+1, each)))
		# 		mc.connectAttr('%s.output1D' % addScaleBck, 'ctrl:TailIkRZ%s_Jnt.scale%s' % (iimd+1, each)) 

		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 	iiiimd-=1
		# 	iiimd-=1	
		# 	iimd+=1
		# 	imd+=1



		attr = ['X', 'Y', 'Z']
		iix = 25
		for attrs in attr:
			pmaNodeCha = mc.createNode ('plusMinusAverage', n = ('%sCha%s_Pma' % (elem , iix+1)))
			mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs) , '%s.input1D[0]' % pmaNodeCha)
			mc.connectAttr ('%s.output1D' % pmaNodeCha, 'ctrl:TailIkRZ%s_Jnt.rotateY' % (iix+1))
			iix+=1
		ix+=1
	# Ctrl 7 Bck----------------------------------------
	attr = ['X', 'Y', 'Z']
	iix = 25
	for attrs in attr:
		mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs), '%sCha%s_Pma.input1D[1]' % (elem,iix-1))
		iix-=1

# Ctrl 8 ==========================================================================================================================
# Ctrl 8 ==========================================================================================================================
	ix = 28
	ikCtrl = mc.ls('ctrl:TailIk8_Ctrl')
	for each in range(0, 1):
		pmaNode = mc.createNode ('plusMinusAverage', n = ('%s%s_Pma' % (elem , ix+1)))
		mc.connectAttr ('%s.rotateY' % ikCtrl[0], '%s.input1D[0]' % pmaNode)
		mc.connectAttr ('%s.output1D' % pmaNode , '%s.rotateY' % listsIkSca[28])
		mdvNodeCha = mc.createNode ('multiplyDivide', n= ('%sCha%s_Mdv' % (elem , ix+1)))
		mc.setAttr ('%s.input2X' % mdvNodeCha, 0.75)
		mc.setAttr ('%s.input2Y' % mdvNodeCha, 0.5)
		mc.setAttr ('%s.input2Z' % mdvNodeCha, 0.25)
		mc.connectAttr('%s.rotateY' % listsIkSca[28], '%s.input1X' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[28], '%s.input1Y' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[28], '%s.input1Z' % mdvNodeCha)

		# Connect Control 8 Rotate 
		mdvRotNode = mc.createNode ('multiplyDivide', n= ('%s%sRot_Mdv' % (elem , ix+1)))
		mc.connectAttr ('%s.rotateZ' % fkCtrlLists[0], '%s.input1X' % mdvRotNode)
		mc.connectAttr ('%s.outputX' % mdvRotNode , '%s.input1D[1]' % pmaNode)
		mc.setAttr ('%s.input2X' % mdvRotNode, -1)

		# Connect Scale  XYZ----------------------------------------------------------------------------
		# Connect Scale  XYZ----------------------------------------------------------------------------
		mc.connectAttr ('%s.scaleX' % ikCtrl[0], '%s.input3D[%s].input3Dx'% (pmaNode,ix))
		mc.connectAttr ('%s.scaleY' % ikCtrl[0], '%s.input3D[%s].input3Dy'% (pmaNode,ix))
		mc.connectAttr ('%s.scaleZ' % ikCtrl[0], '%s.input3D[%s].input3Dz'% (pmaNode,ix))

		mc.connectAttr ('%s.output3Dx' % pmaNode, '%sTailIkRZ%s_Jnt.scaleX' % (nameSp, ix+1))
		mc.connectAttr ('%s.output3Dy' % pmaNode, '%sTailIkRZ%s_Jnt.scaleY' % (nameSp, ix+1))
		mc.connectAttr ('%s.output3Dz' % pmaNode, '%sTailIkRZ%s_Jnt.scaleZ' % (nameSp, ix+1))
		# Connect Scale  XYZ----------------------------------------------------------------------------
		# Connect Scale  XYZ----------------------------------------------------------------------------

		# Lists All Control Rotate ------------------------------
		# Lists All Control Rotate ------------------------------
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[1], '%s.input1D[2]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[2], '%s.input1D[3]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[3], '%s.input1D[4]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[4], '%s.input1D[5]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[5], '%s.input1D[6]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[6], '%s.input1D[7]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[7], '%s.input1D[8]' % pmaNode)


		# # Connect Mdv Node for Rotate
		# # Loop Example
		# iimd = 29
		# iiimd = -1
		# imd = 0
		# iiiimd = 4
		# value = [0.75, 0.5, 0.25]
		# attrSca = ['X', 'Y', 'Z']
		# for each in range(0, 3):
		# 	mdvNodeSca = mc.createNode ('multiplyDivide', n= ('%sCon%s_Mdv' % (elem , imd+1)))
		# 	for each in attrSca:
		# 		mc.connectAttr ('%sTailIkRZ%s_Jnt.scale%s' % (nameSp, ix+1, each) , '%s.input1%s' % (mdvNodeSca,each))
		# 		mc.setAttr ('%s.input2%s' % (mdvNodeSca, each), value[imd])	
				
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		addScale = mc.createNode ('plusMinusAverage', n = ('%sH%s%s_Pma' % (elem , imd+1, each)))
		# 		mc.addAttr(ln='default', k=True, dv=value[iiimd])	
		# 		addScaleBck = mc.createNode ('plusMinusAverage', n = ('%sHB%s%s_Pma' % (elem , imd+1, each)))			
		# 		mc.addAttr(ln='defaultBck', k=True, dv=-1)
		# 		mc.connectAttr('%s.output%s' % (mdvNodeSca, each), '%s.input1D[%s]' % (addScale, 0))
		# 		mc.connectAttr('%s.default' % addScale, '%s.input1D[%s]' % (addScale, 1))
		# 		mc.connectAttr('%s.output1D' % addScale, '%s.input1D[%s]' % (addScaleBck, 0))

		# 		# Connect Scale Back-----------------------------------
		# 		# mc.connectAttr('%s.output1D' % addScale, '%s.input1D[%s]' % (addScaleBck, 0))
		# 		mc.connectAttr('%s.defaultBck' % addScaleBck, '%s.input1D[%s]' % (addScaleBck, 1))
		# 		mc.connectAttr('%s.output1D' % addScaleBck, '%sG%s%s_Pma.input1D[%s]' % (elem, iiiimd-1, each, 2))
		# 													# '%sAB%s%s_Pma' % (elem , imd+1, each)))
		# 		mc.connectAttr('%s.output1D' % addScaleBck, 'ctrl:TailIkRZ%s_Jnt.scale%s' % (iimd+1, each)) 

		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 	iiiimd-=1
		# 	iiimd-=1	
		# 	iimd+=1
		# 	imd+=1


		attr = ['X', 'Y', 'Z']
		iix = 29
		for attrs in attr:
			pmaNodeCha = mc.createNode ('plusMinusAverage', n = ('%sCha%s_Pma' % (elem , iix+1)))
			mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs) , '%s.input1D[0]' % pmaNodeCha)
			mc.connectAttr ('%s.output1D' % pmaNodeCha, 'ctrl:TailIkRZ%s_Jnt.rotateY' % (iix+1))
			iix+=1
		ix+=1
	# Ctrl 8 Bck----------------------------------------
	attr = ['X', 'Y', 'Z']
	iix = 29
	for attrs in attr:
		mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs), '%sCha%s_Pma.input1D[1]' % (elem,iix-1))
		iix-=1

# Ctrl 9 ==========================================================================================================================
# Ctrl 9 ==========================================================================================================================
	ix = 32
	ikCtrl = mc.ls('ctrl:TailIk9_Ctrl')
	for each in range(0, 1):
		pmaNode = mc.createNode ('plusMinusAverage', n = ('%s%s_Pma' % (elem , ix+1)))
		mc.connectAttr ('%s.rotateY' % ikCtrl[0], '%s.input1D[0]' % pmaNode)
		mc.connectAttr ('%s.output1D' % pmaNode , '%s.rotateY' % listsIkSca[32])
		mdvNodeCha = mc.createNode ('multiplyDivide', n= ('%sCha%s_Mdv' % (elem , ix+1)))
		mc.setAttr ('%s.input2X' % mdvNodeCha, 0.75)
		mc.setAttr ('%s.input2Y' % mdvNodeCha, 0.5)
		mc.setAttr ('%s.input2Z' % mdvNodeCha, 0.25)
		mc.connectAttr('%s.rotateY' % listsIkSca[32], '%s.input1X' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[32], '%s.input1Y' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[32], '%s.input1Z' % mdvNodeCha)

		# Connect Control 9 Rotate 
		mdvRotNode = mc.createNode ('multiplyDivide', n= ('%s%sRot_Mdv' % (elem , ix+1)))
		mc.connectAttr ('%s.rotateZ' % fkCtrlLists[0], '%s.input1X' % mdvRotNode)
		mc.connectAttr ('%s.outputX' % mdvRotNode , '%s.input1D[1]' % pmaNode)
		mc.setAttr ('%s.input2X' % mdvRotNode, -1)

		# Connect Scale  XYZ----------------------------------------------------------------------------
		# Connect Scale  XYZ----------------------------------------------------------------------------
		mc.connectAttr ('%s.scaleX' % ikCtrl[0], '%s.input3D[%s].input3Dx'% (pmaNode,ix))
		mc.connectAttr ('%s.scaleY' % ikCtrl[0], '%s.input3D[%s].input3Dy'% (pmaNode,ix))
		mc.connectAttr ('%s.scaleZ' % ikCtrl[0], '%s.input3D[%s].input3Dz'% (pmaNode,ix))

		mc.connectAttr ('%s.output3Dx' % pmaNode, '%sTailIkRZ%s_Jnt.scaleX' % (nameSp, ix+1))
		mc.connectAttr ('%s.output3Dy' % pmaNode, '%sTailIkRZ%s_Jnt.scaleY' % (nameSp, ix+1))
		mc.connectAttr ('%s.output3Dz' % pmaNode, '%sTailIkRZ%s_Jnt.scaleZ' % (nameSp, ix+1))
		# Connect Scale  XYZ----------------------------------------------------------------------------
		# Connect Scale  XYZ----------------------------------------------------------------------------

		# Lists All Control Rotate ------------------------------
		# Lists All Control Rotate ------------------------------
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[1], '%s.input1D[2]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[2], '%s.input1D[3]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[3], '%s.input1D[4]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[4], '%s.input1D[5]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[5], '%s.input1D[6]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[6], '%s.input1D[7]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[7], '%s.input1D[8]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[8], '%s.input1D[9]' % pmaNode)



		# # Connect Mdv Node for Rotate
		# # Loop Example
		# iimd = 33
		# iiimd = -1
		# imd = 0
		# iiiimd = 4
		# value = [0.75, 0.5, 0.25]
		# attrSca = ['X', 'Y', 'Z']
		# for each in range(0, 3):
		# 	mdvNodeSca = mc.createNode ('multiplyDivide', n= ('%sCon%s_Mdv' % (elem , imd+1)))
		# 	for each in attrSca:
		# 		mc.connectAttr ('%sTailIkRZ%s_Jnt.scale%s' % (nameSp, ix+1, each) , '%s.input1%s' % (mdvNodeSca,each))
		# 		mc.setAttr ('%s.input2%s' % (mdvNodeSca, each), value[imd])	
				
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		addScale = mc.createNode ('plusMinusAverage', n = ('%sI%s%s_Pma' % (elem , imd+1, each)))
		# 		mc.addAttr(ln='default', k=True, dv=value[iiimd])	
		# 		addScaleBck = mc.createNode ('plusMinusAverage', n = ('%sIB%s%s_Pma' % (elem , imd+1, each)))			
		# 		mc.addAttr(ln='defaultBck', k=True, dv=-1)
		# 		mc.connectAttr('%s.output%s' % (mdvNodeSca, each), '%s.input1D[%s]' % (addScale, 0))
		# 		mc.connectAttr('%s.default' % addScale, '%s.input1D[%s]' % (addScale, 1))
		# 		mc.connectAttr('%s.output1D' % addScale, '%s.input1D[%s]' % (addScaleBck, 0))

		# 		# Connect Scale Back-----------------------------------
		# 		# mc.connectAttr('%s.output1D' % addScale, '%s.input1D[%s]' % (addScaleBck, 0))
		# 		mc.connectAttr('%s.defaultBck' % addScaleBck, '%s.input1D[%s]' % (addScaleBck, 1))
		# 		mc.connectAttr('%s.output1D' % addScaleBck, '%sH%s%s_Pma.input1D[%s]' % (elem, iiiimd-1, each, 2))
		# 													# '%sAB%s%s_Pma' % (elem , imd+1, each)))
		# 		mc.connectAttr('%s.output1D' % addScaleBck, 'ctrl:TailIkRZ%s_Jnt.scale%s' % (iimd+1, each)) 

		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 	iiiimd-=1
		# 	iiimd-=1	
		# 	iimd+=1
		# 	imd+=1



		attr = ['X', 'Y', 'Z']
		iix = 33
		for attrs in attr:
			pmaNodeCha = mc.createNode ('plusMinusAverage', n = ('%sCha%s_Pma' % (elem , iix+1)))
			mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs) , '%s.input1D[0]' % pmaNodeCha)
			mc.connectAttr ('%s.output1D' % pmaNodeCha, 'ctrl:TailIkRZ%s_Jnt.rotateY' % (iix+1))
			iix+=1
		ix+=1
	# Ctrl 9 Bck----------------------------------------
	attr = ['X', 'Y', 'Z']
	iix = 33
	for attrs in attr:
		mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs), '%sCha%s_Pma.input1D[1]' % (elem,iix-1))
		iix-=1

# Ctrl 10 ==========================================================================================================================
# Ctrl 10 ==========================================================================================================================
	ix = 36
	ikCtrl = mc.ls('ctrl:TailIk10_Ctrl')
	for each in range(0, 1):
		pmaNode = mc.createNode ('plusMinusAverage', n = ('%s%s_Pma' % (elem , ix+1)))
		mc.connectAttr ('%s.rotateY' % ikCtrl[0], '%s.input1D[0]' % pmaNode)
		mc.connectAttr ('%s.output1D' % pmaNode , '%s.rotateY' % listsIkSca[36])
		mdvNodeCha = mc.createNode ('multiplyDivide', n= ('%sCha%s_Mdv' % (elem , ix+1)))
		mc.setAttr ('%s.input2X' % mdvNodeCha, 0.75)
		mc.setAttr ('%s.input2Y' % mdvNodeCha, 0.5)
		mc.setAttr ('%s.input2Z' % mdvNodeCha, 0.25)
		mc.connectAttr('%s.rotateY' % listsIkSca[36], '%s.input1X' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[36], '%s.input1Y' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[36], '%s.input1Z' % mdvNodeCha)

		# Connect Control 10 Rotate 
		mdvRotNode = mc.createNode ('multiplyDivide', n= ('%s%sRot_Mdv' % (elem , ix+1)))
		mc.connectAttr ('%s.rotateZ' % fkCtrlLists[0], '%s.input1X' % mdvRotNode)
		mc.connectAttr ('%s.outputX' % mdvRotNode , '%s.input1D[1]' % pmaNode)
		mc.setAttr ('%s.input2X' % mdvRotNode, -1)

		# Connect Scale  XYZ----------------------------------------------------------------------------
		# Connect Scale  XYZ----------------------------------------------------------------------------
		mc.connectAttr ('%s.scaleX' % ikCtrl[0], '%s.input3D[%s].input3Dx'% (pmaNode,ix))
		mc.connectAttr ('%s.scaleY' % ikCtrl[0], '%s.input3D[%s].input3Dy'% (pmaNode,ix))
		mc.connectAttr ('%s.scaleZ' % ikCtrl[0], '%s.input3D[%s].input3Dz'% (pmaNode,ix))

		mc.connectAttr ('%s.output3Dx' % pmaNode, '%sTailIkRZ%s_Jnt.scaleX' % (nameSp, ix+1))
		mc.connectAttr ('%s.output3Dy' % pmaNode, '%sTailIkRZ%s_Jnt.scaleY' % (nameSp, ix+1))
		mc.connectAttr ('%s.output3Dz' % pmaNode, '%sTailIkRZ%s_Jnt.scaleZ' % (nameSp, ix+1))
		# Connect Scale  XYZ----------------------------------------------------------------------------
		# Connect Scale  XYZ----------------------------------------------------------------------------

		# Lists All Control Rotate ------------------------------
		# Lists All Control Rotate ------------------------------
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[1], '%s.input1D[2]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[2], '%s.input1D[3]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[3], '%s.input1D[4]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[4], '%s.input1D[5]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[5], '%s.input1D[6]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[6], '%s.input1D[7]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[7], '%s.input1D[8]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[8], '%s.input1D[9]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[9], '%s.input1D[10]' % pmaNode)


		# # Connect Mdv Node for Rotate
		# # Loop Example
		# iimd = 37
		# iiimd = -1
		# imd = 0
		# iiiimd = 4
		# value = [0.75, 0.5, 0.25]
		# attrSca = ['X', 'Y', 'Z']
		# for each in range(0, 3):
		# 	mdvNodeSca = mc.createNode ('multiplyDivide', n= ('%sCon%s_Mdv' % (elem , imd+1)))
		# 	for each in attrSca:
		# 		mc.connectAttr ('%sTailIkRZ%s_Jnt.scale%s' % (nameSp, ix+1, each) , '%s.input1%s' % (mdvNodeSca,each))
		# 		mc.setAttr ('%s.input2%s' % (mdvNodeSca, each), value[imd])	
				
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		addScale = mc.createNode ('plusMinusAverage', n = ('%sJ%s%s_Pma' % (elem , imd+1, each)))
		# 		mc.addAttr(ln='default', k=True, dv=value[iiimd])	
		# 		addScaleBck = mc.createNode ('plusMinusAverage', n = ('%sJB%s%s_Pma' % (elem , imd+1, each)))			
		# 		mc.addAttr(ln='defaultBck', k=True, dv=0)
		# 		mc.connectAttr('%s.output%s' % (mdvNodeSca, each), '%s.input1D[%s]' % (addScale, 0))
		# 		mc.connectAttr('%s.default' % addScale, '%s.input1D[%s]' % (addScale, 1))
		# 		mc.connectAttr('%s.output1D' % addScale, '%s.input1D[%s]' % (addScaleBck, 0))

		# 		# Connect Scale Back-----------------------------------
		# 		# mc.connectAttr('%s.output1D' % addScale, '%s.input1D[%s]' % (addScaleBck, 0))
		# 		mc.connectAttr('%s.defaultBck' % addScaleBck, '%s.input1D[%s]' % (addScaleBck, 1))
		# 		mc.connectAttr('%s.output1D' % addScaleBck, '%sI%s%s_Pma.input1D[%s]' % (elem, iiiimd-1, each, 2))
		# 													# '%sAB%s%s_Pma' % (elem , imd+1, each)))
		# 		mc.connectAttr('%s.output1D' % addScaleBck, 'ctrl:TailIkRZ%s_Jnt.scale%s' % (iimd+1, each)) 

		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 	iiiimd-=1
		# 	iiimd-=1	
		# 	iimd+=1
		# 	imd+=1



		attr = ['X', 'Y', 'Z']
		iix = 37
		for attrs in attr:
			pmaNodeCha = mc.createNode ('plusMinusAverage', n = ('%sCha%s_Pma' % (elem , iix+1)))
			mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs) , '%s.input1D[0]' % pmaNodeCha)
			mc.connectAttr ('%s.output1D' % pmaNodeCha, 'ctrl:TailIkRZ%s_Jnt.rotateY' % (iix+1))
			iix+=1
		ix+=1
	# Ctrl 11 Bck----------------------------------------
	attr = ['X', 'Y', 'Z']
	iix = 37
	for attrs in attr:
		mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs), '%sCha%s_Pma.input1D[1]' % (elem,iix-1))
		iix-=1

# Ctrl 11 ==========================================================================================================================
# Ctrl 11 ==========================================================================================================================
	ix = 40
	ikCtrl = mc.ls('ctrl:TailIk11_Ctrl')
	for each in range(0, 1):
		pmaNode = mc.createNode ('plusMinusAverage', n = ('%s%s_Pma' % (elem , ix+1)))
		mc.connectAttr ('%s.rotateY' % ikCtrl[0], '%s.input1D[0]' % pmaNode)
		mc.connectAttr ('%s.output1D' % pmaNode , '%s.rotateY' % listsIkSca[40])
		mdvNodeCha = mc.createNode ('multiplyDivide', n= ('%sCha%s_Mdv' % (elem , ix+1)))
		mc.setAttr ('%s.input2X' % mdvNodeCha, 0.75)
		mc.setAttr ('%s.input2Y' % mdvNodeCha, 0.5)
		mc.setAttr ('%s.input2Z' % mdvNodeCha, 0.25)
		mc.connectAttr('%s.rotateY' % listsIkSca[40], '%s.input1X' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[40], '%s.input1Y' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[40], '%s.input1Z' % mdvNodeCha)

		# Connect Control 11 Rotate 
		mdvRotNode = mc.createNode ('multiplyDivide', n= ('%s%sRot_Mdv' % (elem , ix+1)))
		mc.connectAttr ('%s.rotateZ' % fkCtrlLists[0], '%s.input1X' % mdvRotNode)
		mc.connectAttr ('%s.outputX' % mdvRotNode , '%s.input1D[1]' % pmaNode)
		mc.setAttr ('%s.input2X' % mdvRotNode, -1)

		# Connect Scale  XYZ----------------------------------------------------------------------------
		# Connect Scale  XYZ----------------------------------------------------------------------------
		mc.connectAttr ('%s.scaleX' % ikCtrl[0], '%s.input3D[%s].input3Dx'% (pmaNode,ix))
		mc.connectAttr ('%s.scaleY' % ikCtrl[0], '%s.input3D[%s].input3Dy'% (pmaNode,ix))
		mc.connectAttr ('%s.scaleZ' % ikCtrl[0], '%s.input3D[%s].input3Dz'% (pmaNode,ix))

		mc.connectAttr ('%s.output3Dx' % pmaNode, '%sTailIkRZ%s_Jnt.scaleX' % (nameSp, ix+1))
		mc.connectAttr ('%s.output3Dy' % pmaNode, '%sTailIkRZ%s_Jnt.scaleY' % (nameSp, ix+1))
		mc.connectAttr ('%s.output3Dz' % pmaNode, '%sTailIkRZ%s_Jnt.scaleZ' % (nameSp, ix+1))
		# Connect Scale  XYZ----------------------------------------------------------------------------
		# Connect Scale  XYZ----------------------------------------------------------------------------

		# Lists All Control Rotate ------------------------------
		# Lists All Control Rotate ------------------------------
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[1], '%s.input1D[2]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[2], '%s.input1D[3]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[3], '%s.input1D[4]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[4], '%s.input1D[5]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[5], '%s.input1D[6]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[6], '%s.input1D[7]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[7], '%s.input1D[8]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[8], '%s.input1D[9]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[9], '%s.input1D[10]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[10], '%s.input1D[11]' % pmaNode)


		# # Connect Mdv Node for Rotate
		# # Loop Example
		# iimd = 41
		# iiimd = -1
		# imd = 0
		# iiiimd = 4
		# value = [0.75, 0.5, 0.25]
		# attrSca = ['X', 'Y', 'Z']
		# for each in range(0, 3):
		# 	mdvNodeSca = mc.createNode ('multiplyDivide', n= ('%sCon%s_Mdv' % (elem , imd+1)))
		# 	for each in attrSca:
		# 		mc.connectAttr ('%sTailIkRZ%s_Jnt.scale%s' % (nameSp, ix+1, each) , '%s.input1%s' % (mdvNodeSca,each))
		# 		mc.setAttr ('%s.input2%s' % (mdvNodeSca, each), value[imd])	
				
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		addScale = mc.createNode ('plusMinusAverage', n = ('%sK%s%s_Pma' % (elem , imd+1, each)))
		# 		mc.addAttr(ln='default', k=True, dv=value[iiimd])	
		# 		addScaleBck = mc.createNode ('plusMinusAverage', n = ('%sKB%s%s_Pma' % (elem , imd+1, each)))			
		# 		mc.addAttr(ln='defaultBck', k=True, dv=-1)
		# 		mc.connectAttr('%s.output%s' % (mdvNodeSca, each), '%s.input1D[%s]' % (addScale, 0))
		# 		mc.connectAttr('%s.default' % addScale, '%s.input1D[%s]' % (addScale, 1))
		# 		mc.connectAttr('%s.output1D' % addScale, '%s.input1D[%s]' % (addScaleBck, 0))

		# 		# Connect Scale Back-----------------------------------
		# 		# mc.connectAttr('%s.output1D' % addScale, '%s.input1D[%s]' % (addScaleBck, 0))
		# 		mc.connectAttr('%s.defaultBck' % addScaleBck, '%s.input1D[%s]' % (addScaleBck, 1))
		# 		mc.connectAttr('%s.output1D' % addScaleBck, '%sJB%s%s_Pma.input1D[%s]' % (elem, iiiimd-1, each, 2))
		# 													# '%sAB%s%s_Pma' % (elem , imd+1, each)))
		# 		mc.connectAttr('%s.output1D' % addScaleBck, 'ctrl:TailIkRZ%s_Jnt.scale%s' % (iimd+1, each)) 

		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 	iiiimd-=1
		# 	iiimd-=1	
		# 	iimd+=1
		# 	imd+=1


		attr = ['X', 'Y', 'Z']
		iix = 41
		for attrs in attr:
			pmaNodeCha = mc.createNode ('plusMinusAverage', n = ('%sCha%s_Pma' % (elem , iix+1)))
			mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs) , '%s.input1D[0]' % pmaNodeCha)
			mc.connectAttr ('%s.output1D' % pmaNodeCha, 'ctrl:TailIkRZ%s_Jnt.rotateY' % (iix+1))
			iix+=1
		ix+=1
	# Ctrl 11 Bck----------------------------------------
	attr = ['X', 'Y', 'Z']
	iix = 41
	for attrs in attr:
		mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs), '%sCha%s_Pma.input1D[1]' % (elem,iix-1))
		iix-=1

# Ctrl 12 ==========================================================================================================================
# Ctrl 12 ==========================================================================================================================
	ix = 44
	ikCtrl = mc.ls('ctrl:TailIk12_Ctrl')
	for each in range(0, 1):
		pmaNode = mc.createNode ('plusMinusAverage', n = ('%s%s_Pma' % (elem , ix+1)))
		mc.connectAttr ('%s.rotateY' % ikCtrl[0], '%s.input1D[0]' % pmaNode)
		mc.connectAttr ('%s.output1D' % pmaNode , '%s.rotateY' % listsIkSca[44])
		mdvNodeCha = mc.createNode ('multiplyDivide', n= ('%sCha%s_Mdv' % (elem , ix+1)))
		mc.setAttr ('%s.input2X' % mdvNodeCha, 0.75)
		mc.setAttr ('%s.input2Y' % mdvNodeCha, 0.5)
		mc.setAttr ('%s.input2Z' % mdvNodeCha, 0.25)
		mc.connectAttr('%s.rotateY' % listsIkSca[44], '%s.input1X' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[44], '%s.input1Y' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[44], '%s.input1Z' % mdvNodeCha)

		# Connect Control 12 Rotate 
		mdvRotNode = mc.createNode ('multiplyDivide', n= ('%s%sRot_Mdv' % (elem , ix+1)))
		mc.connectAttr ('%s.rotateZ' % fkCtrlLists[0], '%s.input1X' % mdvRotNode)
		mc.connectAttr ('%s.outputX' % mdvRotNode , '%s.input1D[1]' % pmaNode)
		mc.setAttr ('%s.input2X' % mdvRotNode, -1)

		# Connect Scale  XYZ----------------------------------------------------------------------------
		# Connect Scale  XYZ----------------------------------------------------------------------------
		mc.connectAttr ('%s.scaleX' % ikCtrl[0], '%s.input3D[%s].input3Dx'% (pmaNode,ix))
		mc.connectAttr ('%s.scaleY' % ikCtrl[0], '%s.input3D[%s].input3Dy'% (pmaNode,ix))
		mc.connectAttr ('%s.scaleZ' % ikCtrl[0], '%s.input3D[%s].input3Dz'% (pmaNode,ix))

		mc.connectAttr ('%s.output3Dx' % pmaNode, '%sTailIkRZ%s_Jnt.scaleX' % (nameSp, ix+1))
		mc.connectAttr ('%s.output3Dy' % pmaNode, '%sTailIkRZ%s_Jnt.scaleY' % (nameSp, ix+1))
		mc.connectAttr ('%s.output3Dz' % pmaNode, '%sTailIkRZ%s_Jnt.scaleZ' % (nameSp, ix+1))
		# Connect Scale  XYZ----------------------------------------------------------------------------
		# Connect Scale  XYZ----------------------------------------------------------------------------

		# Lists All Control Rotate ------------------------------
		# Lists All Control Rotate ------------------------------
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[1], '%s.input1D[2]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[2], '%s.input1D[3]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[3], '%s.input1D[4]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[4], '%s.input1D[5]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[5], '%s.input1D[6]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[6], '%s.input1D[7]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[7], '%s.input1D[8]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[8], '%s.input1D[9]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[9], '%s.input1D[10]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[10], '%s.input1D[11]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[11], '%s.input1D[12]' % pmaNode)


		# # Connect Mdv Node for Rotate
		# # Loop Example
		# iimd = 45
		# iiimd = -1
		# imd = 0
		# iiiimd = 4
		# value = [0.75, 0.5, 0.25]
		# attrSca = ['X', 'Y', 'Z']
		# for each in range(0, 3):
		# 	mdvNodeSca = mc.createNode ('multiplyDivide', n= ('%sCon%s_Mdv' % (elem , imd+1)))
		# 	for each in attrSca:
		# 		mc.connectAttr ('%sTailIkRZ%s_Jnt.scale%s' % (nameSp, ix+1, each) , '%s.input1%s' % (mdvNodeSca,each))
		# 		mc.setAttr ('%s.input2%s' % (mdvNodeSca, each), value[imd])	
				
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		addScale = mc.createNode ('plusMinusAverage', n = ('%sL%s%s_Pma' % (elem , imd+1, each)))
		# 		mc.addAttr(ln='default', k=True, dv=value[iiimd])	
		# 		addScaleBck = mc.createNode ('plusMinusAverage', n = ('%sLB%s%s_Pma' % (elem , imd+1, each)))			
		# 		mc.addAttr(ln='defaultBck', k=True, dv=-1)
		# 		mc.connectAttr('%s.output%s' % (mdvNodeSca, each), '%s.input1D[%s]' % (addScale, 0))
		# 		mc.connectAttr('%s.default' % addScale, '%s.input1D[%s]' % (addScale, 1))
		# 		mc.connectAttr('%s.output1D' % addScale, '%s.input1D[%s]' % (addScaleBck, 0))

		# 		# Connect Scale Back-----------------------------------
		# 		# mc.connectAttr('%s.output1D' % addScale, '%s.input1D[%s]' % (addScaleBck, 0))
		# 		mc.connectAttr('%s.defaultBck' % addScaleBck, '%s.input1D[%s]' % (addScaleBck, 1))
		# 		mc.connectAttr('%s.output1D' % addScaleBck, '%sKB%s%s_Pma.input1D[%s]' % (elem, iiiimd-1, each, 2))
		# 													# '%sAB%s%s_Pma' % (elem , imd+1, each)))
		# 		mc.connectAttr('%s.output1D' % addScaleBck, 'ctrl:TailIkRZ%s_Jnt.scale%s' % (iimd+1, each)) 

		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 	iiiimd-=1
		# 	iiimd-=1	
		# 	iimd+=1
		# 	imd+=1


		attr = ['X', 'Y', 'Z']
		iix = 45
		for attrs in attr:
			pmaNodeCha = mc.createNode ('plusMinusAverage', n = ('%sCha%s_Pma' % (elem , iix+1)))
			mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs) , '%s.input1D[0]' % pmaNodeCha)
			mc.connectAttr ('%s.output1D' % pmaNodeCha, 'ctrl:TailIkRZ%s_Jnt.rotateY' % (iix+1))
			iix+=1
		ix+=1
	# Ctrl 12 Bck----------------------------------------
	attr = ['X', 'Y', 'Z']
	iix = 45
	for attrs in attr:
		mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs), '%sCha%s_Pma.input1D[1]' % (elem,iix-1))
		iix-=1

# Ctrl 13 ==========================================================================================================================
# Ctrl 13 ==========================================================================================================================
	ix = 48
	ikCtrl = mc.ls('ctrl:TailIk13_Ctrl')
	for each in range(0, 1):
		pmaNode = mc.createNode ('plusMinusAverage', n = ('%s%s_Pma' % (elem , ix+1)))
		mc.connectAttr ('%s.rotateY' % ikCtrl[0], '%s.input1D[0]' % pmaNode)
		mc.connectAttr ('%s.output1D' % pmaNode , '%s.rotateY' % listsIkSca[48])
		mdvNodeCha = mc.createNode ('multiplyDivide', n= ('%sCha%s_Mdv' % (elem , ix+1)))
		mc.setAttr ('%s.input2X' % mdvNodeCha, 0.75)
		mc.setAttr ('%s.input2Y' % mdvNodeCha, 0.5)
		mc.setAttr ('%s.input2Z' % mdvNodeCha, 0.25)
		mc.connectAttr('%s.rotateY' % listsIkSca[48], '%s.input1X' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[48], '%s.input1Y' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[48], '%s.input1Z' % mdvNodeCha)

		# Connect Control 13 Rotate 
		mdvRotNode = mc.createNode ('multiplyDivide', n= ('%s%sRot_Mdv' % (elem , ix+1)))
		mc.connectAttr ('%s.rotateZ' % fkCtrlLists[0], '%s.input1X' % mdvRotNode)
		mc.connectAttr ('%s.outputX' % mdvRotNode , '%s.input1D[1]' % pmaNode)
		mc.setAttr ('%s.input2X' % mdvRotNode, -1)

		# Connect Scale  XYZ----------------------------------------------------------------------------
		# Connect Scale  XYZ----------------------------------------------------------------------------
		mc.connectAttr ('%s.scaleX' % ikCtrl[0], '%s.input3D[%s].input3Dx'% (pmaNode,ix))
		mc.connectAttr ('%s.scaleY' % ikCtrl[0], '%s.input3D[%s].input3Dy'% (pmaNode,ix))
		mc.connectAttr ('%s.scaleZ' % ikCtrl[0], '%s.input3D[%s].input3Dz'% (pmaNode,ix))

		mc.connectAttr ('%s.output3Dx' % pmaNode, '%sTailIkRZ%s_Jnt.scaleX' % (nameSp, ix+1))
		mc.connectAttr ('%s.output3Dy' % pmaNode, '%sTailIkRZ%s_Jnt.scaleY' % (nameSp, ix+1))
		mc.connectAttr ('%s.output3Dz' % pmaNode, '%sTailIkRZ%s_Jnt.scaleZ' % (nameSp, ix+1))
		# Connect Scale  XYZ----------------------------------------------------------------------------
		# Connect Scale  XYZ----------------------------------------------------------------------------

		# Lists All Control Rotate ------------------------------
		# Lists All Control Rotate ------------------------------
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[1], '%s.input1D[2]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[2], '%s.input1D[3]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[3], '%s.input1D[4]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[4], '%s.input1D[5]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[5], '%s.input1D[6]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[6], '%s.input1D[7]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[7], '%s.input1D[8]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[8], '%s.input1D[9]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[9], '%s.input1D[10]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[10], '%s.input1D[11]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[11], '%s.input1D[12]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[12], '%s.input1D[13]' % pmaNode)



		# # Connect Mdv Node for Rotate
		# # Loop Example
		# iimd = 49
		# iiimd = -1
		# imd = 0
		# iiiimd = 4
		# value = [0.75, 0.5, 0.25]
		# attrSca = ['X', 'Y', 'Z']
		# for each in range(0, 3):
		# 	mdvNodeSca = mc.createNode ('multiplyDivide', n= ('%sCon%s_Mdv' % (elem , imd+1)))
		# 	for each in attrSca:
		# 		mc.connectAttr ('%sTailIkRZ%s_Jnt.scale%s' % (nameSp, ix+1, each) , '%s.input1%s' % (mdvNodeSca,each))
		# 		mc.setAttr ('%s.input2%s' % (mdvNodeSca, each), value[imd])	
				
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		addScale = mc.createNode ('plusMinusAverage', n = ('%sM%s%s_Pma' % (elem , imd+1, each)))
		# 		mc.addAttr(ln='default', k=True, dv=value[iiimd])	
		# 		addScaleBck = mc.createNode ('plusMinusAverage', n = ('%sMB%s%s_Pma' % (elem , imd+1, each)))			
		# 		mc.addAttr(ln='defaultBck', k=True, dv=-1)
		# 		mc.connectAttr('%s.output%s' % (mdvNodeSca, each), '%s.input1D[%s]' % (addScale, 0))
		# 		mc.connectAttr('%s.default' % addScale, '%s.input1D[%s]' % (addScale, 1))
		# 		mc.connectAttr('%s.output1D' % addScale, '%s.input1D[%s]' % (addScaleBck, 0))

		# 		# Connect Scale Back-----------------------------------
		# 		# mc.connectAttr('%s.output1D' % addScale, '%s.input1D[%s]' % (addScaleBck, 0))
		# 		mc.connectAttr('%s.defaultBck' % addScaleBck, '%s.input1D[%s]' % (addScaleBck, 1))
		# 		mc.connectAttr('%s.output1D' % addScaleBck, '%sLB%s%s_Pma.input1D[%s]' % (elem, iiiimd-1, each, 2))
		# 													# '%sAB%s%s_Pma' % (elem , imd+1, each)))
		# 		mc.connectAttr('%s.output1D' % addScaleBck, 'ctrl:TailIkRZ%s_Jnt.scale%s' % (iimd+1, each)) 

		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 	iiiimd-=1
		# 	iiimd-=1	
		# 	iimd+=1
		# 	imd+=1



		attr = ['X', 'Y', 'Z']
		iix = 49
		for attrs in attr:
			pmaNodeCha = mc.createNode ('plusMinusAverage', n = ('%sCha%s_Pma' % (elem , iix+1)))
			mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs) , '%s.input1D[0]' % pmaNodeCha)
			mc.connectAttr ('%s.output1D' % pmaNodeCha, 'ctrl:TailIkRZ%s_Jnt.rotateY' % (iix+1))
			iix+=1
		ix+=1
	# Ctrl 13 Bck----------------------------------------
	attr = ['X', 'Y', 'Z']
	iix = 49
	for attrs in attr:
		mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs), '%sCha%s_Pma.input1D[1]' % (elem,iix-1))
		iix-=1

# Ctrl 14 ==========================================================================================================================
# Ctrl 14 ==========================================================================================================================
	ix = 52
	ikCtrl = mc.ls('ctrl:TailIk14_Ctrl')
	for each in range(0, 1):
		pmaNode = mc.createNode ('plusMinusAverage', n = ('%s%s_Pma' % (elem , ix+1)))
		mc.connectAttr ('%s.rotateY' % ikCtrl[0], '%s.input1D[0]' % pmaNode)
		mc.connectAttr ('%s.output1D' % pmaNode , '%s.rotateY' % listsIkSca[52])
		mdvNodeCha = mc.createNode ('multiplyDivide', n= ('%sCha%s_Mdv' % (elem , ix+1)))
		mc.setAttr ('%s.input2X' % mdvNodeCha, 0.75)
		mc.setAttr ('%s.input2Y' % mdvNodeCha, 0.5)
		mc.setAttr ('%s.input2Z' % mdvNodeCha, 0.25)
		mc.connectAttr('%s.rotateY' % listsIkSca[52], '%s.input1X' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[52], '%s.input1Y' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[52], '%s.input1Z' % mdvNodeCha)

		# Connect Control 14 Rotate 
		mdvRotNode = mc.createNode ('multiplyDivide', n= ('%s%sRot_Mdv' % (elem , ix+1)))
		mc.connectAttr ('%s.rotateZ' % fkCtrlLists[0], '%s.input1X' % mdvRotNode)
		mc.connectAttr ('%s.outputX' % mdvRotNode , '%s.input1D[1]' % pmaNode)
		mc.setAttr ('%s.input2X' % mdvRotNode, -1)

		# Connect Scale  XYZ----------------------------------------------------------------------------
		# Connect Scale  XYZ----------------------------------------------------------------------------
		mc.connectAttr ('%s.scaleX' % ikCtrl[0], '%s.input3D[%s].input3Dx'% (pmaNode,ix))
		mc.connectAttr ('%s.scaleY' % ikCtrl[0], '%s.input3D[%s].input3Dy'% (pmaNode,ix))
		mc.connectAttr ('%s.scaleZ' % ikCtrl[0], '%s.input3D[%s].input3Dz'% (pmaNode,ix))

		mc.connectAttr ('%s.output3Dx' % pmaNode, '%sTailIkRZ%s_Jnt.scaleX' % (nameSp, ix+1))
		mc.connectAttr ('%s.output3Dy' % pmaNode, '%sTailIkRZ%s_Jnt.scaleY' % (nameSp, ix+1))
		mc.connectAttr ('%s.output3Dz' % pmaNode, '%sTailIkRZ%s_Jnt.scaleZ' % (nameSp, ix+1))
		# Connect Scale  XYZ----------------------------------------------------------------------------
		# Connect Scale  XYZ----------------------------------------------------------------------------

		# Lists All Control Rotate ------------------------------
		# Lists All Control Rotate ------------------------------
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[1], '%s.input1D[2]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[2], '%s.input1D[3]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[3], '%s.input1D[4]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[4], '%s.input1D[5]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[5], '%s.input1D[6]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[6], '%s.input1D[7]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[7], '%s.input1D[8]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[8], '%s.input1D[9]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[9], '%s.input1D[10]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[10], '%s.input1D[11]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[11], '%s.input1D[12]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[12], '%s.input1D[13]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[13], '%s.input1D[14]' % pmaNode)



		# # Connect Mdv Node for Rotate
		# # Loop Example
		# iimd = 53
		# iiimd = -1
		# imd = 0
		# iiiimd = 4
		# value = [0.75, 0.5, 0.25]
		# attrSca = ['X', 'Y', 'Z']
		# for each in range(0, 3):
		# 	mdvNodeSca = mc.createNode ('multiplyDivide', n= ('%sCon%s_Mdv' % (elem , imd+1)))
		# 	for each in attrSca:
		# 		mc.connectAttr ('%sTailIkRZ%s_Jnt.scale%s' % (nameSp, ix+1, each) , '%s.input1%s' % (mdvNodeSca,each))
		# 		mc.setAttr ('%s.input2%s' % (mdvNodeSca, each), value[imd])	
				
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		addScale = mc.createNode ('plusMinusAverage', n = ('%sN%s%s_Pma' % (elem , imd+1, each)))
		# 		mc.addAttr(ln='default', k=True, dv=value[iiimd])	
		# 		addScaleBck = mc.createNode ('plusMinusAverage', n = ('%sNB%s%s_Pma' % (elem , imd+1, each)))			
		# 		mc.addAttr(ln='defaultBck', k=True, dv=-1)
		# 		mc.connectAttr('%s.output%s' % (mdvNodeSca, each), '%s.input1D[%s]' % (addScale, 0))
		# 		mc.connectAttr('%s.default' % addScale, '%s.input1D[%s]' % (addScale, 1))
		# 		mc.connectAttr('%s.output1D' % addScale, '%s.input1D[%s]' % (addScaleBck, 0))

		# 		# Connect Scale Back-----------------------------------
		# 		# mc.connectAttr('%s.output1D' % addScale, '%s.input1D[%s]' % (addScaleBck, 0))
		# 		mc.connectAttr('%s.defaultBck' % addScaleBck, '%s.input1D[%s]' % (addScaleBck, 1))
		# 		mc.connectAttr('%s.output1D' % addScaleBck, '%sMB%s%s_Pma.input1D[%s]' % (elem, iiiimd-1, each, 2))
		# 													# '%sAB%s%s_Pma' % (elem , imd+1, each)))
		# 		mc.connectAttr('%s.output1D' % addScaleBck, 'ctrl:TailIkRZ%s_Jnt.scale%s' % (iimd+1, each)) 

		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 	iiiimd-=1
		# 	iiimd-=1	
		# 	iimd+=1
		# 	imd+=1



		attr = ['X', 'Y', 'Z']
		iix = 53
		for attrs in attr:
			pmaNodeCha = mc.createNode ('plusMinusAverage', n = ('%sCha%s_Pma' % (elem , iix+1)))
			mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs) , '%s.input1D[0]' % pmaNodeCha)
			mc.connectAttr ('%s.output1D' % pmaNodeCha, 'ctrl:TailIkRZ%s_Jnt.rotateY' % (iix+1))
			iix+=1
		ix+=1
	# Ctrl 14 Bck----------------------------------------
	attr = ['X', 'Y', 'Z']
	iix = 53
	for attrs in attr:
		mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs), '%sCha%s_Pma.input1D[1]' % (elem,iix-1))
		iix-=1

# Ctrl 15 ==========================================================================================================================
# Ctrl 15 ==========================================================================================================================
	ix = 56
	ikCtrl = mc.ls('ctrl:TailIk15_Ctrl')
	for each in range(0, 1):
		pmaNode = mc.createNode ('plusMinusAverage', n = ('%s%s_Pma' % (elem , ix+1)))
		mc.connectAttr ('%s.rotateY' % ikCtrl[0], '%s.input1D[0]' % pmaNode)
		mc.connectAttr ('%s.output1D' % pmaNode , '%s.rotateY' % listsIkSca[56])
		mdvNodeCha = mc.createNode ('multiplyDivide', n= ('%sCha%s_Mdv' % (elem , ix+1)))
		mc.setAttr ('%s.input2X' % mdvNodeCha, 0.75)
		mc.setAttr ('%s.input2Y' % mdvNodeCha, 0.5)
		mc.setAttr ('%s.input2Z' % mdvNodeCha, 0.25)
		mc.connectAttr('%s.rotateY' % listsIkSca[56], '%s.input1X' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[56], '%s.input1Y' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[56], '%s.input1Z' % mdvNodeCha)

		# Connect Control 15 Rotate 
		mdvRotNode = mc.createNode ('multiplyDivide', n= ('%s%sRot_Mdv' % (elem , ix+1)))
		mc.connectAttr ('%s.rotateZ' % fkCtrlLists[0], '%s.input1X' % mdvRotNode)
		mc.connectAttr ('%s.outputX' % mdvRotNode , '%s.input1D[1]' % pmaNode)
		mc.setAttr ('%s.input2X' % mdvRotNode, -1)

		# Connect Scale  XYZ----------------------------------------------------------------------------
		# Connect Scale  XYZ----------------------------------------------------------------------------
		mc.connectAttr ('%s.scaleX' % ikCtrl[0], '%s.input3D[%s].input3Dx'% (pmaNode,ix))
		mc.connectAttr ('%s.scaleY' % ikCtrl[0], '%s.input3D[%s].input3Dy'% (pmaNode,ix))
		mc.connectAttr ('%s.scaleZ' % ikCtrl[0], '%s.input3D[%s].input3Dz'% (pmaNode,ix))

		mc.connectAttr ('%s.output3Dx' % pmaNode, '%sTailIkRZ%s_Jnt.scaleX' % (nameSp, ix+1))
		mc.connectAttr ('%s.output3Dy' % pmaNode, '%sTailIkRZ%s_Jnt.scaleY' % (nameSp, ix+1))
		mc.connectAttr ('%s.output3Dz' % pmaNode, '%sTailIkRZ%s_Jnt.scaleZ' % (nameSp, ix+1))
		# Connect Scale  XYZ----------------------------------------------------------------------------
		# Connect Scale  XYZ----------------------------------------------------------------------------

		# Lists All Control Rotate ------------------------------
		# Lists All Control Rotate ------------------------------
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[1], '%s.input1D[2]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[2], '%s.input1D[3]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[3], '%s.input1D[4]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[4], '%s.input1D[5]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[5], '%s.input1D[6]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[6], '%s.input1D[7]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[7], '%s.input1D[8]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[8], '%s.input1D[9]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[9], '%s.input1D[10]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[10], '%s.input1D[11]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[11], '%s.input1D[12]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[12], '%s.input1D[13]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[13], '%s.input1D[14]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[14], '%s.input1D[15]' % pmaNode)


		# # Connect Mdv Node for Rotate
		# # Loop Example
		# iimd = 57
		# iiimd = -1
		# imd = 0
		# iiiimd = 4
		# value = [0.75, 0.5, 0.25]
		# attrSca = ['X', 'Y', 'Z']
		# for each in range(0, 3):
		# 	mdvNodeSca = mc.createNode ('multiplyDivide', n= ('%sCon%s_Mdv' % (elem , imd+1)))
		# 	for each in attrSca:
		# 		mc.connectAttr ('%sTailIkRZ%s_Jnt.scale%s' % (nameSp, ix+1, each) , '%s.input1%s' % (mdvNodeSca,each))
		# 		mc.setAttr ('%s.input2%s' % (mdvNodeSca, each), value[imd])	
				
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		addScale = mc.createNode ('plusMinusAverage', n = ('%sO%s%s_Pma' % (elem , imd+1, each)))
		# 		mc.addAttr(ln='default', k=True, dv=value[iiimd])	
		# 		addScaleBck = mc.createNode ('plusMinusAverage', n = ('%sOB%s%s_Pma' % (elem , imd+1, each)))			
		# 		mc.addAttr(ln='defaultBck', k=True, dv=-1)
		# 		mc.connectAttr('%s.output%s' % (mdvNodeSca, each), '%s.input1D[%s]' % (addScale, 0))
		# 		mc.connectAttr('%s.default' % addScale, '%s.input1D[%s]' % (addScale, 1))
		# 		mc.connectAttr('%s.output1D' % addScale, '%s.input1D[%s]' % (addScaleBck, 0))

		# 		# Connect Scale Back-----------------------------------
		# 		# mc.connectAttr('%s.output1D' % addScale, '%s.input1D[%s]' % (addScaleBck, 0))
		# 		mc.connectAttr('%s.defaultBck' % addScaleBck, '%s.input1D[%s]' % (addScaleBck, 1))
		# 		mc.connectAttr('%s.output1D' % addScaleBck, '%sNB%s%s_Pma.input1D[%s]' % (elem, iiiimd-1, each, 2))
		# 													# '%sAB%s%s_Pma' % (elem , imd+1, each)))
		# 		mc.connectAttr('%s.output1D' % addScaleBck, 'ctrl:TailIkRZ%s_Jnt.scale%s' % (iimd+1, each)) 

		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 	iiiimd-=1
		# 	iiimd-=1	
		# 	iimd+=1
		# 	imd+=1


		attr = ['X', 'Y', 'Z']
		iix = 57
		for attrs in attr:
			pmaNodeCha = mc.createNode ('plusMinusAverage', n = ('%sCha%s_Pma' % (elem , iix+1)))
			mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs) , '%s.input1D[0]' % pmaNodeCha)
			mc.connectAttr ('%s.output1D' % pmaNodeCha, 'ctrl:TailIkRZ%s_Jnt.rotateY' % (iix+1))
			iix+=1
		ix+=1
	# Ctrl 15 Bck----------------------------------------
	attr = ['X', 'Y', 'Z']
	iix = 57
	for attrs in attr:
		mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs), '%sCha%s_Pma.input1D[1]' % (elem,iix-1))
		iix-=1

# Ctrl 16 ==========================================================================================================================
# Ctrl 16 ==========================================================================================================================
	ix = 60
	ikCtrl = mc.ls('ctrl:TailIk16_Ctrl')
	for each in range(0, 1):
		pmaNode = mc.createNode ('plusMinusAverage', n = ('%s%s_Pma' % (elem , ix+1)))
		mc.connectAttr ('%s.rotateY' % ikCtrl[0], '%s.input1D[0]' % pmaNode)
		mc.connectAttr ('%s.output1D' % pmaNode , '%s.rotateY' % listsIkSca[60])
		mdvNodeCha = mc.createNode ('multiplyDivide', n= ('%sCha%s_Mdv' % (elem , ix+1)))
		mc.setAttr ('%s.input2X' % mdvNodeCha, 0.75)
		mc.setAttr ('%s.input2Y' % mdvNodeCha, 0.5)
		mc.setAttr ('%s.input2Z' % mdvNodeCha, 0.25)
		mc.connectAttr('%s.rotateY' % listsIkSca[60], '%s.input1X' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[60], '%s.input1Y' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[60], '%s.input1Z' % mdvNodeCha)

		# Connect Control 16 Rotate 
		mdvRotNode = mc.createNode ('multiplyDivide', n= ('%s%sRot_Mdv' % (elem , ix+1)))
		mc.connectAttr ('%s.rotateZ' % fkCtrlLists[0], '%s.input1X' % mdvRotNode)
		mc.connectAttr ('%s.outputX' % mdvRotNode , '%s.input1D[1]' % pmaNode)
		mc.setAttr ('%s.input2X' % mdvRotNode, -1)

		# Connect Scale  XYZ----------------------------------------------------------------------------
		# Connect Scale  XYZ----------------------------------------------------------------------------
		mc.connectAttr ('%s.scaleX' % ikCtrl[0], '%s.input3D[%s].input3Dx'% (pmaNode,ix))
		mc.connectAttr ('%s.scaleY' % ikCtrl[0], '%s.input3D[%s].input3Dy'% (pmaNode,ix))
		mc.connectAttr ('%s.scaleZ' % ikCtrl[0], '%s.input3D[%s].input3Dz'% (pmaNode,ix))

		mc.connectAttr ('%s.output3Dx' % pmaNode, '%sTailIkRZ%s_Jnt.scaleX' % (nameSp, ix+1))
		mc.connectAttr ('%s.output3Dy' % pmaNode, '%sTailIkRZ%s_Jnt.scaleY' % (nameSp, ix+1))
		mc.connectAttr ('%s.output3Dz' % pmaNode, '%sTailIkRZ%s_Jnt.scaleZ' % (nameSp, ix+1))
		# Connect Scale  XYZ----------------------------------------------------------------------------
		# Connect Scale  XYZ----------------------------------------------------------------------------

		# Lists All Control Rotate ------------------------------
		# Lists All Control Rotate ------------------------------
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[1], '%s.input1D[2]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[2], '%s.input1D[3]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[3], '%s.input1D[4]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[4], '%s.input1D[5]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[5], '%s.input1D[6]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[6], '%s.input1D[7]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[7], '%s.input1D[8]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[8], '%s.input1D[9]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[9], '%s.input1D[10]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[10], '%s.input1D[11]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[11], '%s.input1D[12]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[12], '%s.input1D[13]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[13], '%s.input1D[14]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[14], '%s.input1D[15]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[15], '%s.input1D[16]' % pmaNode)



		# # Connect Mdv Node for Rotate
		# # Loop Example
		# iimd = 61
		# iiimd = -1
		# imd = 0
		# iiiimd = 4
		# value = [0.75, 0.5, 0.25]
		# attrSca = ['X', 'Y', 'Z']
		# for each in range(0, 3):
		# 	mdvNodeSca = mc.createNode ('multiplyDivide', n= ('%sCon%s_Mdv' % (elem , imd+1)))
		# 	for each in attrSca:
		# 		mc.connectAttr ('%sTailIkRZ%s_Jnt.scale%s' % (nameSp, ix+1, each) , '%s.input1%s' % (mdvNodeSca,each))
		# 		mc.setAttr ('%s.input2%s' % (mdvNodeSca, each), value[imd])	
				
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		addScale = mc.createNode ('plusMinusAverage', n = ('%sP%s%s_Pma' % (elem , imd+1, each)))
		# 		mc.addAttr(ln='default', k=True, dv=value[iiimd])	
		# 		addScaleBck = mc.createNode ('plusMinusAverage', n = ('%sPB%s%s_Pma' % (elem , imd+1, each)))			
		# 		mc.addAttr(ln='defaultBck', k=True, dv=-1)
		# 		mc.connectAttr('%s.output%s' % (mdvNodeSca, each), '%s.input1D[%s]' % (addScale, 0))
		# 		mc.connectAttr('%s.default' % addScale, '%s.input1D[%s]' % (addScale, 1))
		# 		mc.connectAttr('%s.output1D' % addScale, '%s.input1D[%s]' % (addScaleBck, 0))

		# 		# Connect Scale Back-----------------------------------
		# 		# mc.connectAttr('%s.output1D' % addScale, '%s.input1D[%s]' % (addScaleBck, 0))
		# 		mc.connectAttr('%s.defaultBck' % addScaleBck, '%s.input1D[%s]' % (addScaleBck, 1))
		# 		mc.connectAttr('%s.output1D' % addScaleBck, '%sOB%s%s_Pma.input1D[%s]' % (elem, iiiimd-1, each, 2))
		# 													# '%sAB%s%s_Pma' % (elem , imd+1, each)))
		# 		mc.connectAttr('%s.output1D' % addScaleBck, 'ctrl:TailIkRZ%s_Jnt.scale%s' % (iimd+1, each)) 

		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 	iiiimd-=1
		# 	iiimd-=1	
		# 	iimd+=1
		# 	imd+=1



		attr = ['X', 'Y', 'Z']
		iix = 61
		for attrs in attr:
			pmaNodeCha = mc.createNode ('plusMinusAverage', n = ('%sCha%s_Pma' % (elem , iix+1)))
			mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs) , '%s.input1D[0]' % pmaNodeCha)
			mc.connectAttr ('%s.output1D' % pmaNodeCha, 'ctrl:TailIkRZ%s_Jnt.rotateY' % (iix+1))
			iix+=1
		ix+=1
	# Ctrl 16 Bck----------------------------------------
	attr = ['X', 'Y', 'Z']
	iix = 61
	for attrs in attr:
		mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs), '%sCha%s_Pma.input1D[1]' % (elem,iix-1))
		iix-=1

# Ctrl 17 ==========================================================================================================================
# Ctrl 17 ==========================================================================================================================
	ix = 64
	ikCtrl = mc.ls('ctrl:TailIk17_Ctrl')
	for each in range(0, 1):
		pmaNode = mc.createNode ('plusMinusAverage', n = ('%s%s_Pma' % (elem , ix+1)))
		mc.connectAttr ('%s.rotateY' % ikCtrl[0], '%s.input1D[0]' % pmaNode)
		mc.connectAttr ('%s.output1D' % pmaNode , '%s.rotateY' % listsIkSca[64])
		mdvNodeCha = mc.createNode ('multiplyDivide', n= ('%sCha%s_Mdv' % (elem , ix+1)))
		mc.setAttr ('%s.input2X' % mdvNodeCha, 0.75)
		mc.setAttr ('%s.input2Y' % mdvNodeCha, 0.5)
		mc.setAttr ('%s.input2Z' % mdvNodeCha, 0.25)
		mc.connectAttr('%s.rotateY' % listsIkSca[64], '%s.input1X' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[64], '%s.input1Y' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[64], '%s.input1Z' % mdvNodeCha)

		# Connect Control 17 Rotate 
		mdvRotNode = mc.createNode ('multiplyDivide', n= ('%s%sRot_Mdv' % (elem , ix+1)))
		mc.connectAttr ('%s.rotateZ' % fkCtrlLists[0], '%s.input1X' % mdvRotNode)
		mc.connectAttr ('%s.outputX' % mdvRotNode , '%s.input1D[1]' % pmaNode)
		mc.setAttr ('%s.input2X' % mdvRotNode, -1)

		# Connect Scale  XYZ----------------------------------------------------------------------------
		# Connect Scale  XYZ----------------------------------------------------------------------------
		mc.connectAttr ('%s.scaleX' % ikCtrl[0], '%s.input3D[%s].input3Dx'% (pmaNode,ix))
		mc.connectAttr ('%s.scaleY' % ikCtrl[0], '%s.input3D[%s].input3Dy'% (pmaNode,ix))
		mc.connectAttr ('%s.scaleZ' % ikCtrl[0], '%s.input3D[%s].input3Dz'% (pmaNode,ix))

		mc.connectAttr ('%s.output3Dx' % pmaNode, '%sTailIkRZ%s_Jnt.scaleX' % (nameSp, ix+1))
		mc.connectAttr ('%s.output3Dy' % pmaNode, '%sTailIkRZ%s_Jnt.scaleY' % (nameSp, ix+1))
		mc.connectAttr ('%s.output3Dz' % pmaNode, '%sTailIkRZ%s_Jnt.scaleZ' % (nameSp, ix+1))
		# Connect Scale  XYZ----------------------------------------------------------------------------
		# Connect Scale  XYZ----------------------------------------------------------------------------

		# Lists All Control Rotate ------------------------------
		# Lists All Control Rotate ------------------------------
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[1], '%s.input1D[2]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[2], '%s.input1D[3]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[3], '%s.input1D[4]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[4], '%s.input1D[5]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[5], '%s.input1D[6]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[6], '%s.input1D[7]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[7], '%s.input1D[8]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[8], '%s.input1D[9]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[9], '%s.input1D[10]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[10], '%s.input1D[11]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[11], '%s.input1D[12]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[12], '%s.input1D[13]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[13], '%s.input1D[14]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[14], '%s.input1D[15]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[15], '%s.input1D[16]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[16], '%s.input1D[17]' % pmaNode)



		# # Connect Mdv Node for Rotate
		# # Loop Example
		# iimd = 65
		# iiimd = -1
		# imd = 0
		# iiiimd = 4
		# value = [0.75, 0.5, 0.25]
		# attrSca = ['X', 'Y', 'Z']
		# for each in range(0, 3):
		# 	mdvNodeSca = mc.createNode ('multiplyDivide', n= ('%sCon%s_Mdv' % (elem , imd+1)))
		# 	for each in attrSca:
		# 		mc.connectAttr ('%sTailIkRZ%s_Jnt.scale%s' % (nameSp, ix+1, each) , '%s.input1%s' % (mdvNodeSca,each))
		# 		mc.setAttr ('%s.input2%s' % (mdvNodeSca, each), value[imd])	
				
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		addScale = mc.createNode ('plusMinusAverage', n = ('%sQ%s%s_Pma' % (elem , imd+1, each)))
		# 		mc.addAttr(ln='default', k=True, dv=value[iiimd])	
		# 		addScaleBck = mc.createNode ('plusMinusAverage', n = ('%sQB%s%s_Pma' % (elem , imd+1, each)))			
		# 		mc.addAttr(ln='defaultBck', k=True, dv=-1)
		# 		mc.connectAttr('%s.output%s' % (mdvNodeSca, each), '%s.input1D[%s]' % (addScale, 0))
		# 		mc.connectAttr('%s.default' % addScale, '%s.input1D[%s]' % (addScale, 1))
		# 		mc.connectAttr('%s.output1D' % addScale, '%s.input1D[%s]' % (addScaleBck, 0))

		# 		# Connect Scale Back-----------------------------------
		# 		# mc.connectAttr('%s.output1D' % addScale, '%s.input1D[%s]' % (addScaleBck, 0))
		# 		mc.connectAttr('%s.defaultBck' % addScaleBck, '%s.input1D[%s]' % (addScaleBck, 1))
		# 		mc.connectAttr('%s.output1D' % addScaleBck, '%sPB%s%s_Pma.input1D[%s]' % (elem, iiiimd-1, each, 2))
		# 													# '%sAB%s%s_Pma' % (elem , imd+1, each)))
		# 		mc.connectAttr('%s.output1D' % addScaleBck, 'ctrl:TailIkRZ%s_Jnt.scale%s' % (iimd+1, each)) 

		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 	iiiimd-=1
		# 	iiimd-=1	
		# 	iimd+=1
		# 	imd+=1



		attr = ['X', 'Y', 'Z']
		iix = 65
		for attrs in attr:
			pmaNodeCha = mc.createNode ('plusMinusAverage', n = ('%sCha%s_Pma' % (elem , iix+1)))
			mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs) , '%s.input1D[0]' % pmaNodeCha)
			mc.connectAttr ('%s.output1D' % pmaNodeCha, 'ctrl:TailIkRZ%s_Jnt.rotateY' % (iix+1))
			iix+=1
		ix+=1
	# Ctrl 17 Bck----------------------------------------
	attr = ['X', 'Y', 'Z']
	iix = 65
	for attrs in attr:
		mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs), '%sCha%s_Pma.input1D[1]' % (elem,iix-1))
		iix-=1

# Ctrl 18 ==========================================================================================================================
# Ctrl 18 ==========================================================================================================================
	ix = 68
	ikCtrl = mc.ls('ctrl:TailIk18_Ctrl')
	for each in range(0, 1):
		pmaNode = mc.createNode ('plusMinusAverage', n = ('%s%s_Pma' % (elem , ix+1)))
		mc.connectAttr ('%s.rotateY' % ikCtrl[0], '%s.input1D[0]' % pmaNode)
		mc.connectAttr ('%s.output1D' % pmaNode , '%s.rotateY' % listsIkSca[68])
		mdvNodeCha = mc.createNode ('multiplyDivide', n= ('%sCha%s_Mdv' % (elem , ix+1)))
		mc.setAttr ('%s.input2X' % mdvNodeCha, 0.75)
		mc.setAttr ('%s.input2Y' % mdvNodeCha, 0.5)
		mc.setAttr ('%s.input2Z' % mdvNodeCha, 0.25)
		mc.connectAttr('%s.rotateY' % listsIkSca[68], '%s.input1X' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[68], '%s.input1Y' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[68], '%s.input1Z' % mdvNodeCha)

		# Connect Control 18 Rotate 
		mdvRotNode = mc.createNode ('multiplyDivide', n= ('%s%sRot_Mdv' % (elem , ix+1)))
		mc.connectAttr ('%s.rotateZ' % fkCtrlLists[0], '%s.input1X' % mdvRotNode)
		mc.connectAttr ('%s.outputX' % mdvRotNode , '%s.input1D[1]' % pmaNode)
		mc.setAttr ('%s.input2X' % mdvRotNode, -1)

		# Connect Scale  XYZ----------------------------------------------------------------------------
		# Connect Scale  XYZ----------------------------------------------------------------------------
		mc.connectAttr ('%s.scaleX' % ikCtrl[0], '%s.input3D[%s].input3Dx'% (pmaNode,ix))
		mc.connectAttr ('%s.scaleY' % ikCtrl[0], '%s.input3D[%s].input3Dy'% (pmaNode,ix))
		mc.connectAttr ('%s.scaleZ' % ikCtrl[0], '%s.input3D[%s].input3Dz'% (pmaNode,ix))

		mc.connectAttr ('%s.output3Dx' % pmaNode, '%sTailIkRZ%s_Jnt.scaleX' % (nameSp, ix+1))
		mc.connectAttr ('%s.output3Dy' % pmaNode, '%sTailIkRZ%s_Jnt.scaleY' % (nameSp, ix+1))
		mc.connectAttr ('%s.output3Dz' % pmaNode, '%sTailIkRZ%s_Jnt.scaleZ' % (nameSp, ix+1))
		# Connect Scale  XYZ----------------------------------------------------------------------------
		# Connect Scale  XYZ----------------------------------------------------------------------------

		# Lists All Control Rotate ------------------------------
		# Lists All Control Rotate ------------------------------
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[1], '%s.input1D[2]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[2], '%s.input1D[3]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[3], '%s.input1D[4]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[4], '%s.input1D[5]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[5], '%s.input1D[6]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[6], '%s.input1D[7]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[7], '%s.input1D[8]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[8], '%s.input1D[9]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[9], '%s.input1D[10]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[10], '%s.input1D[11]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[11], '%s.input1D[12]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[12], '%s.input1D[13]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[13], '%s.input1D[14]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[14], '%s.input1D[15]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[15], '%s.input1D[16]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[16], '%s.input1D[17]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[17], '%s.input1D[18]' % pmaNode)


		# # Connect Mdv Node for Rotate
		# # Loop Example
		# iimd = 69
		# iiimd = -1
		# imd = 0
		# iiiimd = 4
		# value = [0.75, 0.5, 0.25]
		# attrSca = ['X', 'Y', 'Z']
		# for each in range(0, 3):
		# 	mdvNodeSca = mc.createNode ('multiplyDivide', n= ('%sCon%s_Mdv' % (elem , imd+1)))
		# 	for each in attrSca:
		# 		mc.connectAttr ('%sTailIkRZ%s_Jnt.scale%s' % (nameSp, ix+1, each) , '%s.input1%s' % (mdvNodeSca,each))
		# 		mc.setAttr ('%s.input2%s' % (mdvNodeSca, each), value[imd])	
				
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		addScale = mc.createNode ('plusMinusAverage', n = ('%sR%s%s_Pma' % (elem , imd+1, each)))
		# 		mc.addAttr(ln='default', k=True, dv=value[iiimd])	
		# 		addScaleBck = mc.createNode ('plusMinusAverage', n = ('%sRB%s%s_Pma' % (elem , imd+1, each)))			
		# 		mc.addAttr(ln='defaultBck', k=True, dv=-1)
		# 		mc.connectAttr('%s.output%s' % (mdvNodeSca, each), '%s.input1D[%s]' % (addScale, 0))
		# 		mc.connectAttr('%s.default' % addScale, '%s.input1D[%s]' % (addScale, 1))
		# 		mc.connectAttr('%s.output1D' % addScale, '%s.input1D[%s]' % (addScaleBck, 0))

		# 		# Connect Scale Back-----------------------------------
		# 		# mc.connectAttr('%s.output1D' % addScale, '%s.input1D[%s]' % (addScaleBck, 0))
		# 		mc.connectAttr('%s.defaultBck' % addScaleBck, '%s.input1D[%s]' % (addScaleBck, 1))
		# 		mc.connectAttr('%s.output1D' % addScaleBck, '%sQB%s%s_Pma.input1D[%s]' % (elem, iiiimd-1, each, 2))
		# 													# '%sAB%s%s_Pma' % (elem , imd+1, each)))
		# 		mc.connectAttr('%s.output1D' % addScaleBck, 'ctrl:TailIkRZ%s_Jnt.scale%s' % (iimd+1, each)) 

		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 	iiiimd-=1
		# 	iiimd-=1	
		# 	iimd+=1
		# 	imd+=1


		attr = ['X', 'Y', 'Z']
		iix = 69
		for attrs in attr:
			pmaNodeCha = mc.createNode ('plusMinusAverage', n = ('%sCha%s_Pma' % (elem , iix+1)))
			mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs) , '%s.input1D[0]' % pmaNodeCha)
			mc.connectAttr ('%s.output1D' % pmaNodeCha, 'ctrl:TailIkRZ%s_Jnt.rotateY' % (iix+1))
			iix+=1
		ix+=1
	# Ctrl 18 Bck----------------------------------------
	attr = ['X', 'Y', 'Z']
	iix = 69
	for attrs in attr:
		mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs), '%sCha%s_Pma.input1D[1]' % (elem,iix-1))
		iix-=1

# Ctrl 19 ==========================================================================================================================
# Ctrl 19 ==========================================================================================================================
	ix = 72
	ikCtrl = mc.ls('ctrl:TailIk19_Ctrl')
	for each in range(0, 1):
		pmaNode = mc.createNode ('plusMinusAverage', n = ('%s%s_Pma' % (elem , ix+1)))
		mc.connectAttr ('%s.rotateY' % ikCtrl[0], '%s.input1D[0]' % pmaNode)
		mc.connectAttr ('%s.output1D' % pmaNode , '%s.rotateY' % listsIkSca[72])
		mdvNodeCha = mc.createNode ('multiplyDivide', n= ('%sCha%s_Mdv' % (elem , ix+1)))
		mc.setAttr ('%s.input2X' % mdvNodeCha, 0.75)
		mc.setAttr ('%s.input2Y' % mdvNodeCha, 0.5)
		mc.setAttr ('%s.input2Z' % mdvNodeCha, 0.25)
		mc.connectAttr('%s.rotateY' % listsIkSca[72], '%s.input1X' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[72], '%s.input1Y' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[72], '%s.input1Z' % mdvNodeCha)

		# Connect Control 19 Rotate 
		mdvRotNode = mc.createNode ('multiplyDivide', n= ('%s%sRot_Mdv' % (elem , ix+1)))
		mc.connectAttr ('%s.rotateZ' % fkCtrlLists[0], '%s.input1X' % mdvRotNode)
		mc.connectAttr ('%s.outputX' % mdvRotNode , '%s.input1D[1]' % pmaNode)
		mc.setAttr ('%s.input2X' % mdvRotNode, -1)

		# Connect Scale  XYZ----------------------------------------------------------------------------
		# Connect Scale  XYZ----------------------------------------------------------------------------
		mc.connectAttr ('%s.scaleX' % ikCtrl[0], '%s.input3D[%s].input3Dx'% (pmaNode,ix))
		mc.connectAttr ('%s.scaleY' % ikCtrl[0], '%s.input3D[%s].input3Dy'% (pmaNode,ix))
		mc.connectAttr ('%s.scaleZ' % ikCtrl[0], '%s.input3D[%s].input3Dz'% (pmaNode,ix))

		mc.connectAttr ('%s.output3Dx' % pmaNode, '%sTailIkRZ%s_Jnt.scaleX' % (nameSp, ix+1))
		mc.connectAttr ('%s.output3Dy' % pmaNode, '%sTailIkRZ%s_Jnt.scaleY' % (nameSp, ix+1))
		mc.connectAttr ('%s.output3Dz' % pmaNode, '%sTailIkRZ%s_Jnt.scaleZ' % (nameSp, ix+1))
		# Connect Scale  XYZ----------------------------------------------------------------------------
		# Connect Scale  XYZ----------------------------------------------------------------------------

		# Lists All Control Rotate ------------------------------
		# Lists All Control Rotate ------------------------------
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[1], '%s.input1D[2]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[2], '%s.input1D[3]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[3], '%s.input1D[4]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[4], '%s.input1D[5]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[5], '%s.input1D[6]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[6], '%s.input1D[7]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[7], '%s.input1D[8]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[8], '%s.input1D[9]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[9], '%s.input1D[10]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[10], '%s.input1D[11]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[11], '%s.input1D[12]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[12], '%s.input1D[13]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[13], '%s.input1D[14]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[14], '%s.input1D[15]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[15], '%s.input1D[16]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[16], '%s.input1D[17]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[17], '%s.input1D[18]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[18], '%s.input1D[19]' % pmaNode)


		# # Connect Mdv Node for Rotate
		# # Loop Example
		# iimd = 73
		# iiimd = -1
		# imd = 0
		# iiiimd = 4
		# value = [0.75, 0.5, 0.25]
		# attrSca = ['X', 'Y', 'Z']
		# for each in range(0, 3):
		# 	mdvNodeSca = mc.createNode ('multiplyDivide', n= ('%sCon%s_Mdv' % (elem , imd+1)))
		# 	for each in attrSca:
		# 		mc.connectAttr ('%sTailIkRZ%s_Jnt.scale%s' % (nameSp, ix+1, each) , '%s.input1%s' % (mdvNodeSca,each))
		# 		mc.setAttr ('%s.input2%s' % (mdvNodeSca, each), value[imd])	
				
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		addScale = mc.createNode ('plusMinusAverage', n = ('%sS%s%s_Pma' % (elem , imd+1, each)))
		# 		mc.addAttr(ln='default', k=True, dv=value[iiimd])	
		# 		addScaleBck = mc.createNode ('plusMinusAverage', n = ('%sSB%s%s_Pma' % (elem , imd+1, each)))			
		# 		mc.addAttr(ln='defaultBck', k=True, dv=-1)
		# 		mc.connectAttr('%s.output%s' % (mdvNodeSca, each), '%s.input1D[%s]' % (addScale, 0))
		# 		mc.connectAttr('%s.default' % addScale, '%s.input1D[%s]' % (addScale, 1))
		# 		mc.connectAttr('%s.output1D' % addScale, '%s.input1D[%s]' % (addScaleBck, 0))

		# 		# Connect Scale Back-----------------------------------
		# 		# mc.connectAttr('%s.output1D' % addScale, '%s.input1D[%s]' % (addScaleBck, 0))
		# 		mc.connectAttr('%s.defaultBck' % addScaleBck, '%s.input1D[%s]' % (addScaleBck, 1))
		# 		mc.connectAttr('%s.output1D' % addScaleBck, '%sRB%s%s_Pma.input1D[%s]' % (elem, iiiimd-1, each, 2))
		# 													# '%sAB%s%s_Pma' % (elem , imd+1, each)))
		# 		mc.connectAttr('%s.output1D' % addScaleBck, 'ctrl:TailIkRZ%s_Jnt.scale%s' % (iimd+1, each)) 

		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 	iiiimd-=1
		# 	iiimd-=1	
		# 	iimd+=1
		# 	imd+=1


		attr = ['X', 'Y', 'Z']
		iix = 73
		for attrs in attr:
			pmaNodeCha = mc.createNode ('plusMinusAverage', n = ('%sCha%s_Pma' % (elem , iix+1)))
			mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs) , '%s.input1D[0]' % pmaNodeCha)
			mc.connectAttr ('%s.output1D' % pmaNodeCha, 'ctrl:TailIkRZ%s_Jnt.rotateY' % (iix+1))
			iix+=1
		ix+=1
	# Ctrl 19 Bck----------------------------------------
	attr = ['X', 'Y', 'Z']
	iix = 73
	for attrs in attr:
		mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs), '%sCha%s_Pma.input1D[1]' % (elem,iix-1))
		iix-=1

# Ctrl 20 ==========================================================================================================================
# Ctrl 20 ==========================================================================================================================
	ix = 76
	ikCtrl = mc.ls('ctrl:TailIk20_Ctrl')
	for each in range(0, 1):
		pmaNode = mc.createNode ('plusMinusAverage', n = ('%s%s_Pma' % (elem , ix+1)))
		mc.connectAttr ('%s.rotateY' % ikCtrl[0], '%s.input1D[0]' % pmaNode)
		mc.connectAttr ('%s.output1D' % pmaNode , '%s.rotateY' % listsIkSca[76])
		mdvNodeCha = mc.createNode ('multiplyDivide', n= ('%sCha%s_Mdv' % (elem , ix+1)))
		mc.setAttr ('%s.input2X' % mdvNodeCha, 0.75)
		mc.setAttr ('%s.input2Y' % mdvNodeCha, 0.5)
		mc.setAttr ('%s.input2Z' % mdvNodeCha, 0.25)
		mc.connectAttr('%s.rotateY' % listsIkSca[76], '%s.input1X' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[76], '%s.input1Y' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[76], '%s.input1Z' % mdvNodeCha)

		# Connect Control 20 Rotate 
		mdvRotNode = mc.createNode ('multiplyDivide', n= ('%s%sRot_Mdv' % (elem , ix+1)))
		mc.connectAttr ('%s.rotateZ' % fkCtrlLists[0], '%s.input1X' % mdvRotNode)
		mc.connectAttr ('%s.outputX' % mdvRotNode , '%s.input1D[1]' % pmaNode)
		mc.setAttr ('%s.input2X' % mdvRotNode, -1)

		# Connect Scale  XYZ----------------------------------------------------------------------------
		# Connect Scale  XYZ----------------------------------------------------------------------------
		mc.connectAttr ('%s.scaleX' % ikCtrl[0], '%s.input3D[%s].input3Dx'% (pmaNode,ix))
		mc.connectAttr ('%s.scaleY' % ikCtrl[0], '%s.input3D[%s].input3Dy'% (pmaNode,ix))
		mc.connectAttr ('%s.scaleZ' % ikCtrl[0], '%s.input3D[%s].input3Dz'% (pmaNode,ix))

		mc.connectAttr ('%s.output3Dx' % pmaNode, '%sTailIkRZ%s_Jnt.scaleX' % (nameSp, ix+1))
		mc.connectAttr ('%s.output3Dy' % pmaNode, '%sTailIkRZ%s_Jnt.scaleY' % (nameSp, ix+1))
		mc.connectAttr ('%s.output3Dz' % pmaNode, '%sTailIkRZ%s_Jnt.scaleZ' % (nameSp, ix+1))
		# Connect Scale  XYZ----------------------------------------------------------------------------
		# Connect Scale  XYZ----------------------------------------------------------------------------

		# Lists All Control Rotate ------------------------------
		# Lists All Control Rotate ------------------------------
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[1], '%s.input1D[2]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[2], '%s.input1D[3]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[3], '%s.input1D[4]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[4], '%s.input1D[5]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[5], '%s.input1D[6]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[6], '%s.input1D[7]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[7], '%s.input1D[8]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[8], '%s.input1D[9]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[9], '%s.input1D[10]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[10], '%s.input1D[11]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[11], '%s.input1D[12]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[12], '%s.input1D[13]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[13], '%s.input1D[14]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[14], '%s.input1D[15]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[15], '%s.input1D[16]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[16], '%s.input1D[17]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[17], '%s.input1D[18]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[18], '%s.input1D[19]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[19], '%s.input1D[20]' % pmaNode)


		# # Connect Mdv Node for Rotate
		# # Loop Example
		# iimd = 77
		# iiimd = -1
		# imd = 0
		# iiiimd = 4
		# value = [0.75, 0.5, 0.25]
		# attrSca = ['X', 'Y', 'Z']
		# for each in range(0, 3):
		# 	mdvNodeSca = mc.createNode ('multiplyDivide', n= ('%sCon%s_Mdv' % (elem , imd+1)))
		# 	for each in attrSca:
		# 		mc.connectAttr ('%sTailIkRZ%s_Jnt.scale%s' % (nameSp, ix+1, each) , '%s.input1%s' % (mdvNodeSca,each))
		# 		mc.setAttr ('%s.input2%s' % (mdvNodeSca, each), value[imd])	
				
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		addScale = mc.createNode ('plusMinusAverage', n = ('%sT%s%s_Pma' % (elem , imd+1, each)))
		# 		mc.addAttr(ln='default', k=True, dv=value[iiimd])	
		# 		addScaleBck = mc.createNode ('plusMinusAverage', n = ('%sTB%s%s_Pma' % (elem , imd+1, each)))			
		# 		mc.addAttr(ln='defaultBck', k=True, dv=-1)
		# 		mc.connectAttr('%s.output%s' % (mdvNodeSca, each), '%s.input1D[%s]' % (addScale, 0))
		# 		mc.connectAttr('%s.default' % addScale, '%s.input1D[%s]' % (addScale, 1))
		# 		mc.connectAttr('%s.output1D' % addScale, '%s.input1D[%s]' % (addScaleBck, 0))

		# 		# Connect Scale Back-----------------------------------
		# 		# mc.connectAttr('%s.output1D' % addScale, '%s.input1D[%s]' % (addScaleBck, 0))
		# 		mc.connectAttr('%s.defaultBck' % addScaleBck, '%s.input1D[%s]' % (addScaleBck, 1))
		# 		mc.connectAttr('%s.output1D' % addScaleBck, '%sSB%s%s_Pma.input1D[%s]' % (elem, iiiimd-1, each, 2))
		# 													# '%sAB%s%s_Pma' % (elem , imd+1, each)))
		# 		mc.connectAttr('%s.output1D' % addScaleBck, 'ctrl:TailIkRZ%s_Jnt.scale%s' % (iimd+1, each)) 

		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 	iiiimd-=1
		# 	iiimd-=1	
		# 	iimd+=1
		# 	imd+=1


		attr = ['X', 'Y', 'Z']
		iix = 77
		for attrs in attr:
			pmaNodeCha = mc.createNode ('plusMinusAverage', n = ('%sCha%s_Pma' % (elem , iix+1)))
			mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs) , '%s.input1D[0]' % pmaNodeCha)
			mc.connectAttr ('%s.output1D' % pmaNodeCha, 'ctrl:TailIkRZ%s_Jnt.rotateY' % (iix+1))
			iix+=1
		ix+=1
	# Ctrl 20 Bck----------------------------------------
	attr = ['X', 'Y', 'Z']
	iix = 77
	for attrs in attr:
		mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs), '%sCha%s_Pma.input1D[1]' % (elem,iix-1))
		iix-=1

# Ctrl 21 ==========================================================================================================================
# Ctrl 21 ==========================================================================================================================
	ix = 80
	ikCtrl = mc.ls('ctrl:TailIk21_Ctrl')
	for each in range(0, 1):
		pmaNode = mc.createNode ('plusMinusAverage', n = ('%s%s_Pma' % (elem , ix+1)))
		mc.connectAttr ('%s.rotateY' % ikCtrl[0], '%s.input1D[0]' % pmaNode)
		mc.connectAttr ('%s.output1D' % pmaNode , '%s.rotateY' % listsIkSca[80])
		mdvNodeCha = mc.createNode ('multiplyDivide', n= ('%sCha%s_Mdv' % (elem , ix+1)))
		mc.setAttr ('%s.input2X' % mdvNodeCha, 0.75)
		mc.setAttr ('%s.input2Y' % mdvNodeCha, 0.5)
		mc.setAttr ('%s.input2Z' % mdvNodeCha, 0.25)
		mc.connectAttr('%s.rotateY' % listsIkSca[80], '%s.input1X' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[80], '%s.input1Y' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[80], '%s.input1Z' % mdvNodeCha)

		# Connect Control 21 Rotate 
		mdvRotNode = mc.createNode ('multiplyDivide', n= ('%s%sRot_Mdv' % (elem , ix+1)))
		mc.connectAttr ('%s.rotateZ' % fkCtrlLists[0], '%s.input1X' % mdvRotNode)
		mc.connectAttr ('%s.outputX' % mdvRotNode , '%s.input1D[1]' % pmaNode)
		mc.setAttr ('%s.input2X' % mdvRotNode, -1)

		# Connect Scale  XYZ----------------------------------------------------------------------------
		# Connect Scale  XYZ----------------------------------------------------------------------------
		mc.connectAttr ('%s.scaleX' % ikCtrl[0], '%s.input3D[%s].input3Dx'% (pmaNode,ix))
		mc.connectAttr ('%s.scaleY' % ikCtrl[0], '%s.input3D[%s].input3Dy'% (pmaNode,ix))
		mc.connectAttr ('%s.scaleZ' % ikCtrl[0], '%s.input3D[%s].input3Dz'% (pmaNode,ix))

		mc.connectAttr ('%s.output3Dx' % pmaNode, '%sTailIkRZ%s_Jnt.scaleX' % (nameSp, ix+1))
		mc.connectAttr ('%s.output3Dy' % pmaNode, '%sTailIkRZ%s_Jnt.scaleY' % (nameSp, ix+1))
		mc.connectAttr ('%s.output3Dz' % pmaNode, '%sTailIkRZ%s_Jnt.scaleZ' % (nameSp, ix+1))
		# Connect Scale  XYZ----------------------------------------------------------------------------
		# Connect Scale  XYZ----------------------------------------------------------------------------

		# Lists All Control Rotate ------------------------------
		# Lists All Control Rotate ------------------------------
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[1], '%s.input1D[2]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[2], '%s.input1D[3]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[3], '%s.input1D[4]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[4], '%s.input1D[5]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[5], '%s.input1D[6]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[6], '%s.input1D[7]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[7], '%s.input1D[8]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[8], '%s.input1D[9]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[9], '%s.input1D[10]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[10], '%s.input1D[11]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[11], '%s.input1D[12]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[12], '%s.input1D[13]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[13], '%s.input1D[14]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[14], '%s.input1D[15]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[15], '%s.input1D[16]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[16], '%s.input1D[17]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[17], '%s.input1D[18]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[18], '%s.input1D[19]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[19], '%s.input1D[20]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[20], '%s.input1D[21]' % pmaNode)


		# # Connect Mdv Node for Rotate
		# # Loop Example
		# iimd = 81
		# iiimd = -1
		# imd = 0
		# iiiimd = 4
		# value = [0.75, 0.5, 0.25]
		# attrSca = ['X', 'Y', 'Z']
		# for each in range(0, 3):
		# 	mdvNodeSca = mc.createNode ('multiplyDivide', n= ('%sCon%s_Mdv' % (elem , imd+1)))
		# 	for each in attrSca:
		# 		mc.connectAttr ('%sTailIkRZ%s_Jnt.scale%s' % (nameSp, ix+1, each) , '%s.input1%s' % (mdvNodeSca,each))
		# 		mc.setAttr ('%s.input2%s' % (mdvNodeSca, each), value[imd])	
				
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		addScale = mc.createNode ('plusMinusAverage', n = ('%sU%s%s_Pma' % (elem , imd+1, each)))
		# 		mc.addAttr(ln='default', k=True, dv=value[iiimd])	
		# 		addScaleBck = mc.createNode ('plusMinusAverage', n = ('%sUB%s%s_Pma' % (elem , imd+1, each)))			
		# 		mc.addAttr(ln='defaultBck', k=True, dv=-1)
		# 		mc.connectAttr('%s.output%s' % (mdvNodeSca, each), '%s.input1D[%s]' % (addScale, 0))
		# 		mc.connectAttr('%s.default' % addScale, '%s.input1D[%s]' % (addScale, 1))
		# 		mc.connectAttr('%s.output1D' % addScale, '%s.input1D[%s]' % (addScaleBck, 0))

		# 		# Connect Scale Back-----------------------------------
		# 		# mc.connectAttr('%s.output1D' % addScale, '%s.input1D[%s]' % (addScaleBck, 0))
		# 		mc.connectAttr('%s.defaultBck' % addScaleBck, '%s.input1D[%s]' % (addScaleBck, 1))
		# 		mc.connectAttr('%s.output1D' % addScaleBck, '%sTB%s%s_Pma.input1D[%s]' % (elem, iiiimd-1, each, 2))
		# 													# '%sAB%s%s_Pma' % (elem , imd+1, each)))
		# 		mc.connectAttr('%s.output1D' % addScaleBck, 'ctrl:TailIkRZ%s_Jnt.scale%s' % (iimd+1, each)) 

		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 	iiiimd-=1
		# 	iiimd-=1	
		# 	iimd+=1
		# 	imd+=1


		attr = ['X', 'Y', 'Z']
		iix = 81
		for attrs in attr:
			pmaNodeCha = mc.createNode ('plusMinusAverage', n = ('%sCha%s_Pma' % (elem , iix+1)))
			mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs) , '%s.input1D[0]' % pmaNodeCha)
			mc.connectAttr ('%s.output1D' % pmaNodeCha, 'ctrl:TailIkRZ%s_Jnt.rotateY' % (iix+1))
			iix+=1
		ix+=1
	# Ctrl 21 Bck----------------------------------------
	attr = ['X', 'Y', 'Z']
	iix = 81
	for attrs in attr:
		mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs), '%sCha%s_Pma.input1D[1]' % (elem,iix-1))
		iix-=1

# Ctrl 22 ==========================================================================================================================
# Ctrl 22 ==========================================================================================================================
	ix = 84
	ikCtrl = mc.ls('ctrl:TailIk22_Ctrl')
	for each in range(0, 1):
		pmaNode = mc.createNode ('plusMinusAverage', n = ('%s%s_Pma' % (elem , ix+1)))
		mc.connectAttr ('%s.rotateY' % ikCtrl[0], '%s.input1D[0]' % pmaNode)
		mc.connectAttr ('%s.output1D' % pmaNode , '%s.rotateY' % listsIkSca[84])
		mdvNodeCha = mc.createNode ('multiplyDivide', n= ('%sCha%s_Mdv' % (elem , ix+1)))
		mc.setAttr ('%s.input2X' % mdvNodeCha, 0.75)
		mc.setAttr ('%s.input2Y' % mdvNodeCha, 0.5)
		mc.setAttr ('%s.input2Z' % mdvNodeCha, 0.25)
		mc.connectAttr('%s.rotateY' % listsIkSca[84], '%s.input1X' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[84], '%s.input1Y' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[84], '%s.input1Z' % mdvNodeCha)

		# Connect Control 22 Rotate 
		mdvRotNode = mc.createNode ('multiplyDivide', n= ('%s%sRot_Mdv' % (elem , ix+1)))
		mc.connectAttr ('%s.rotateZ' % fkCtrlLists[0], '%s.input1X' % mdvRotNode)
		mc.connectAttr ('%s.outputX' % mdvRotNode , '%s.input1D[1]' % pmaNode)
		mc.setAttr ('%s.input2X' % mdvRotNode, -1)

		# Connect Scale  XYZ----------------------------------------------------------------------------
		# Connect Scale  XYZ----------------------------------------------------------------------------
		mc.connectAttr ('%s.scaleX' % ikCtrl[0], '%s.input3D[%s].input3Dx'% (pmaNode,ix))
		mc.connectAttr ('%s.scaleY' % ikCtrl[0], '%s.input3D[%s].input3Dy'% (pmaNode,ix))
		mc.connectAttr ('%s.scaleZ' % ikCtrl[0], '%s.input3D[%s].input3Dz'% (pmaNode,ix))

		mc.connectAttr ('%s.output3Dx' % pmaNode, '%sTailIkRZ%s_Jnt.scaleX' % (nameSp, ix+1))
		mc.connectAttr ('%s.output3Dy' % pmaNode, '%sTailIkRZ%s_Jnt.scaleY' % (nameSp, ix+1))
		mc.connectAttr ('%s.output3Dz' % pmaNode, '%sTailIkRZ%s_Jnt.scaleZ' % (nameSp, ix+1))
		# Connect Scale  XYZ----------------------------------------------------------------------------
		# Connect Scale  XYZ----------------------------------------------------------------------------

		# Lists All Control Rotate ------------------------------
		# Lists All Control Rotate ------------------------------
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[1], '%s.input1D[2]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[2], '%s.input1D[3]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[3], '%s.input1D[4]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[4], '%s.input1D[5]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[5], '%s.input1D[6]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[6], '%s.input1D[7]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[7], '%s.input1D[8]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[8], '%s.input1D[9]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[9], '%s.input1D[10]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[10], '%s.input1D[11]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[11], '%s.input1D[12]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[12], '%s.input1D[13]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[13], '%s.input1D[14]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[14], '%s.input1D[15]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[15], '%s.input1D[16]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[16], '%s.input1D[17]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[17], '%s.input1D[18]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[18], '%s.input1D[19]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[19], '%s.input1D[20]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[20], '%s.input1D[21]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[21], '%s.input1D[22]' % pmaNode)


		# # Connect Mdv Node for Rotate
		# # Loop Example
		# iimd = 85
		# iiimd = -1
		# imd = 0
		# iiiimd = 4
		# value = [0.75, 0.5, 0.25]
		# attrSca = ['X', 'Y', 'Z']
		# for each in range(0, 3):
		# 	mdvNodeSca = mc.createNode ('multiplyDivide', n= ('%sCon%s_Mdv' % (elem , imd+1)))
		# 	for each in attrSca:
		# 		mc.connectAttr ('%sTailIkRZ%s_Jnt.scale%s' % (nameSp, ix+1, each) , '%s.input1%s' % (mdvNodeSca,each))
		# 		mc.setAttr ('%s.input2%s' % (mdvNodeSca, each), value[imd])	
				
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		addScale = mc.createNode ('plusMinusAverage', n = ('%sV%s%s_Pma' % (elem , imd+1, each)))
		# 		mc.addAttr(ln='default', k=True, dv=value[iiimd])	
		# 		addScaleBck = mc.createNode ('plusMinusAverage', n = ('%sVB%s%s_Pma' % (elem , imd+1, each)))			
		# 		mc.addAttr(ln='defaultBck', k=True, dv=-1)
		# 		mc.connectAttr('%s.output%s' % (mdvNodeSca, each), '%s.input1D[%s]' % (addScale, 0))
		# 		mc.connectAttr('%s.default' % addScale, '%s.input1D[%s]' % (addScale, 1))
		# 		mc.connectAttr('%s.output1D' % addScale, '%s.input1D[%s]' % (addScaleBck, 0))

		# 		# Connect Scale Back-----------------------------------
		# 		# mc.connectAttr('%s.output1D' % addScale, '%s.input1D[%s]' % (addScaleBck, 0))
		# 		mc.connectAttr('%s.defaultBck' % addScaleBck, '%s.input1D[%s]' % (addScaleBck, 1))
		# 		mc.connectAttr('%s.output1D' % addScaleBck, '%sUB%s%s_Pma.input1D[%s]' % (elem, iiiimd-1, each, 2))
		# 													# '%sAB%s%s_Pma' % (elem , imd+1, each)))
		# 		mc.connectAttr('%s.output1D' % addScaleBck, 'ctrl:TailIkRZ%s_Jnt.scale%s' % (iimd+1, each)) 

		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 	iiiimd-=1
		# 	iiimd-=1	
		# 	iimd+=1
		# 	imd+=1


		attr = ['X', 'Y', 'Z']
		iix = 85
		for attrs in attr:
			pmaNodeCha = mc.createNode ('plusMinusAverage', n = ('%sCha%s_Pma' % (elem , iix+1)))
			mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs) , '%s.input1D[0]' % pmaNodeCha)
			mc.connectAttr ('%s.output1D' % pmaNodeCha, 'ctrl:TailIkRZ%s_Jnt.rotateY' % (iix+1))
			iix+=1
		ix+=1
	# Ctrl 22 Bck----------------------------------------
	attr = ['X', 'Y', 'Z']
	iix = 85
	for attrs in attr:
		mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs), '%sCha%s_Pma.input1D[1]' % (elem,iix-1))
		iix-=1

# Ctrl 23 ==========================================================================================================================
# Ctrl 23 ==========================================================================================================================
	ix = 88
	ikCtrl = mc.ls('ctrl:TailIk23_Ctrl')
	for each in range(0, 1):
		pmaNode = mc.createNode ('plusMinusAverage', n = ('%s%s_Pma' % (elem , ix+1)))
		mc.connectAttr ('%s.rotateY' % ikCtrl[0], '%s.input1D[0]' % pmaNode)
		mc.connectAttr ('%s.output1D' % pmaNode , '%s.rotateY' % listsIkSca[88])
		mdvNodeCha = mc.createNode ('multiplyDivide', n= ('%sCha%s_Mdv' % (elem , ix+1)))
		mc.setAttr ('%s.input2X' % mdvNodeCha, 0.75)
		mc.setAttr ('%s.input2Y' % mdvNodeCha, 0.5)
		mc.setAttr ('%s.input2Z' % mdvNodeCha, 0.25)
		mc.connectAttr('%s.rotateY' % listsIkSca[88], '%s.input1X' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[88], '%s.input1Y' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[88], '%s.input1Z' % mdvNodeCha)

		# Connect Control 23 Rotate 
		mdvRotNode = mc.createNode ('multiplyDivide', n= ('%s%sRot_Mdv' % (elem , ix+1)))
		mc.connectAttr ('%s.rotateZ' % fkCtrlLists[0], '%s.input1X' % mdvRotNode)
		mc.connectAttr ('%s.outputX' % mdvRotNode , '%s.input1D[1]' % pmaNode)
		mc.setAttr ('%s.input2X' % mdvRotNode, -1)

		# Connect Scale  XYZ----------------------------------------------------------------------------
		# Connect Scale  XYZ----------------------------------------------------------------------------
		mc.connectAttr ('%s.scaleX' % ikCtrl[0], '%s.input3D[%s].input3Dx'% (pmaNode,ix))
		mc.connectAttr ('%s.scaleY' % ikCtrl[0], '%s.input3D[%s].input3Dy'% (pmaNode,ix))
		mc.connectAttr ('%s.scaleZ' % ikCtrl[0], '%s.input3D[%s].input3Dz'% (pmaNode,ix))

		mc.connectAttr ('%s.output3Dx' % pmaNode, '%sTailIkRZ%s_Jnt.scaleX' % (nameSp, ix+1))
		mc.connectAttr ('%s.output3Dy' % pmaNode, '%sTailIkRZ%s_Jnt.scaleY' % (nameSp, ix+1))
		mc.connectAttr ('%s.output3Dz' % pmaNode, '%sTailIkRZ%s_Jnt.scaleZ' % (nameSp, ix+1))
		# Connect Scale  XYZ----------------------------------------------------------------------------
		# Connect Scale  XYZ----------------------------------------------------------------------------

		# Lists All Control Rotate ------------------------------
		# Lists All Control Rotate ------------------------------
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[1], '%s.input1D[2]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[2], '%s.input1D[3]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[3], '%s.input1D[4]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[4], '%s.input1D[5]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[5], '%s.input1D[6]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[6], '%s.input1D[7]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[7], '%s.input1D[8]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[8], '%s.input1D[9]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[9], '%s.input1D[10]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[10], '%s.input1D[11]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[11], '%s.input1D[12]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[12], '%s.input1D[13]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[13], '%s.input1D[14]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[14], '%s.input1D[15]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[15], '%s.input1D[16]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[16], '%s.input1D[17]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[17], '%s.input1D[18]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[18], '%s.input1D[19]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[19], '%s.input1D[20]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[20], '%s.input1D[21]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[21], '%s.input1D[22]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[22], '%s.input1D[23]' % pmaNode)


		# # Connect Mdv Node for Rotate
		# # Loop Example
		# iimd = 89
		# iiimd = -1
		# imd = 0
		# iiiimd = 4
		# value = [0.75, 0.5, 0.25]
		# attrSca = ['X', 'Y', 'Z']
		# for each in range(0, 3):
		# 	mdvNodeSca = mc.createNode ('multiplyDivide', n= ('%sCon%s_Mdv' % (elem , imd+1)))
		# 	for each in attrSca:
		# 		mc.connectAttr ('%sTailIkRZ%s_Jnt.scale%s' % (nameSp, ix+1, each) , '%s.input1%s' % (mdvNodeSca,each))
		# 		mc.setAttr ('%s.input2%s' % (mdvNodeSca, each), value[imd])	
				
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		addScale = mc.createNode ('plusMinusAverage', n = ('%sW%s%s_Pma' % (elem , imd+1, each)))
		# 		mc.addAttr(ln='default', k=True, dv=value[iiimd])	
		# 		addScaleBck = mc.createNode ('plusMinusAverage', n = ('%sWB%s%s_Pma' % (elem , imd+1, each)))			
		# 		mc.addAttr(ln='defaultBck', k=True, dv=-1)
		# 		mc.connectAttr('%s.output%s' % (mdvNodeSca, each), '%s.input1D[%s]' % (addScale, 0))
		# 		mc.connectAttr('%s.default' % addScale, '%s.input1D[%s]' % (addScale, 1))
		# 		mc.connectAttr('%s.output1D' % addScale, '%s.input1D[%s]' % (addScaleBck, 0))

		# 		# Connect Scale Back-----------------------------------
		# 		# mc.connectAttr('%s.output1D' % addScale, '%s.input1D[%s]' % (addScaleBck, 0))
		# 		mc.connectAttr('%s.defaultBck' % addScaleBck, '%s.input1D[%s]' % (addScaleBck, 1))
		# 		mc.connectAttr('%s.output1D' % addScaleBck, '%sVB%s%s_Pma.input1D[%s]' % (elem, iiiimd-1, each, 2))
		# 													# '%sAB%s%s_Pma' % (elem , imd+1, each)))
		# 		mc.connectAttr('%s.output1D' % addScaleBck, 'ctrl:TailIkRZ%s_Jnt.scale%s' % (iimd+1, each)) 

		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 	iiiimd-=1
		# 	iiimd-=1	
		# 	iimd+=1
		# 	imd+=1


		attr = ['X', 'Y', 'Z']
		iix = 89
		for attrs in attr:
			pmaNodeCha = mc.createNode ('plusMinusAverage', n = ('%sCha%s_Pma' % (elem , iix+1)))
			mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs) , '%s.input1D[0]' % pmaNodeCha)
			mc.connectAttr ('%s.output1D' % pmaNodeCha, 'ctrl:TailIkRZ%s_Jnt.rotateY' % (iix+1))
			iix+=1
		ix+=1
	# Ctrl 23 Bck----------------------------------------
	attr = ['X', 'Y', 'Z']
	iix = 89
	for attrs in attr:
		mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs), '%sCha%s_Pma.input1D[1]' % (elem,iix-1))
		iix-=1

# Ctrl 24 ==========================================================================================================================
# Ctrl 24 ==========================================================================================================================
	ix = 92
	ikCtrl = mc.ls('ctrl:TailIk24_Ctrl')
	for each in range(0, 1):
		pmaNode = mc.createNode ('plusMinusAverage', n = ('%s%s_Pma' % (elem , ix+1)))
		mc.connectAttr ('%s.rotateY' % ikCtrl[0], '%s.input1D[0]' % pmaNode)
		mc.connectAttr ('%s.output1D' % pmaNode , '%s.rotateY' % listsIkSca[92])
		mdvNodeCha = mc.createNode ('multiplyDivide', n= ('%sCha%s_Mdv' % (elem , ix+1)))
		mc.setAttr ('%s.input2X' % mdvNodeCha, 0.75)
		mc.setAttr ('%s.input2Y' % mdvNodeCha, 0.5)
		mc.setAttr ('%s.input2Z' % mdvNodeCha, 0.25)
		mc.connectAttr('%s.rotateY' % listsIkSca[92], '%s.input1X' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[92], '%s.input1Y' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[92], '%s.input1Z' % mdvNodeCha)

		# Connect Control 24 Rotate 
		mdvRotNode = mc.createNode ('multiplyDivide', n= ('%s%sRot_Mdv' % (elem , ix+1)))
		mc.connectAttr ('%s.rotateZ' % fkCtrlLists[0], '%s.input1X' % mdvRotNode)
		mc.connectAttr ('%s.outputX' % mdvRotNode , '%s.input1D[1]' % pmaNode)
		mc.setAttr ('%s.input2X' % mdvRotNode, -1)

		# Connect Scale  XYZ----------------------------------------------------------------------------
		# Connect Scale  XYZ----------------------------------------------------------------------------
		mc.connectAttr ('%s.scaleX' % ikCtrl[0], '%s.input3D[%s].input3Dx'% (pmaNode,ix))
		mc.connectAttr ('%s.scaleY' % ikCtrl[0], '%s.input3D[%s].input3Dy'% (pmaNode,ix))
		mc.connectAttr ('%s.scaleZ' % ikCtrl[0], '%s.input3D[%s].input3Dz'% (pmaNode,ix))

		mc.connectAttr ('%s.output3Dx' % pmaNode, '%sTailIkRZ%s_Jnt.scaleX' % (nameSp, ix+1))
		mc.connectAttr ('%s.output3Dy' % pmaNode, '%sTailIkRZ%s_Jnt.scaleY' % (nameSp, ix+1))
		mc.connectAttr ('%s.output3Dz' % pmaNode, '%sTailIkRZ%s_Jnt.scaleZ' % (nameSp, ix+1))
		# Connect Scale  XYZ----------------------------------------------------------------------------
		# Connect Scale  XYZ----------------------------------------------------------------------------

		# Lists All Control Rotate ------------------------------
		# Lists All Control Rotate ------------------------------
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[1], '%s.input1D[2]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[2], '%s.input1D[3]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[3], '%s.input1D[4]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[4], '%s.input1D[5]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[5], '%s.input1D[6]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[6], '%s.input1D[7]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[7], '%s.input1D[8]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[8], '%s.input1D[9]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[9], '%s.input1D[10]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[10], '%s.input1D[11]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[11], '%s.input1D[12]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[12], '%s.input1D[13]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[13], '%s.input1D[14]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[14], '%s.input1D[15]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[15], '%s.input1D[16]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[16], '%s.input1D[17]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[17], '%s.input1D[18]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[18], '%s.input1D[19]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[19], '%s.input1D[20]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[20], '%s.input1D[21]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[21], '%s.input1D[22]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[22], '%s.input1D[23]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[23], '%s.input1D[24]' % pmaNode)


		# # Connect Mdv Node for Rotate
		# # Loop Example
		# iimd = 93
		# iiimd = -1
		# imd = 0
		# iiiimd = 4
		# value = [0.75, 0.5, 0.25]
		# attrSca = ['X', 'Y', 'Z']
		# for each in range(0, 3):
		# 	mdvNodeSca = mc.createNode ('multiplyDivide', n= ('%sCon%s_Mdv' % (elem , imd+1)))
		# 	for each in attrSca:
		# 		mc.connectAttr ('%sTailIkRZ%s_Jnt.scale%s' % (nameSp, ix+1, each) , '%s.input1%s' % (mdvNodeSca,each))
		# 		mc.setAttr ('%s.input2%s' % (mdvNodeSca, each), value[imd])	
				
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		addScale = mc.createNode ('plusMinusAverage', n = ('%sX%s%s_Pma' % (elem , imd+1, each)))
		# 		mc.addAttr(ln='default', k=True, dv=value[iiimd])	
		# 		addScaleBck = mc.createNode ('plusMinusAverage', n = ('%sXB%s%s_Pma' % (elem , imd+1, each)))			
		# 		mc.addAttr(ln='defaultBck', k=True, dv=0)
		# 		mc.connectAttr('%s.output%s' % (mdvNodeSca, each), '%s.input1D[%s]' % (addScale, 0))
		# 		mc.connectAttr('%s.default' % addScale, '%s.input1D[%s]' % (addScale, 1))
		# 		mc.connectAttr('%s.output1D' % addScale, '%s.input1D[%s]' % (addScaleBck, 0))

		# 		# Connect Scale Back-----------------------------------
		# 		# mc.connectAttr('%s.output1D' % addScale, '%s.input1D[%s]' % (addScaleBck, 0))
		# 		mc.connectAttr('%s.defaultBck' % addScaleBck, '%s.input1D[%s]' % (addScaleBck, 1))
		# 		mc.connectAttr('%s.output1D' % addScaleBck, '%sWB%s%s_Pma.input1D[%s]' % (elem, iiiimd-1, each, 2))
		# 													# '%sAB%s%s_Pma' % (elem , imd+1, each)))
		# 		mc.connectAttr('%s.output1D' % addScaleBck, 'ctrl:TailIkRZ%s_Jnt.scale%s' % (iimd+1, each)) 

		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 	iiiimd-=1
		# 	iiimd-=1	
		# 	iimd+=1
		# 	imd+=1


		attr = ['X', 'Y', 'Z']
		iix = 93
		for attrs in attr:
			pmaNodeCha = mc.createNode ('plusMinusAverage', n = ('%sCha%s_Pma' % (elem , iix+1)))
			mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs) , '%s.input1D[0]' % pmaNodeCha)
			mc.connectAttr ('%s.output1D' % pmaNodeCha, 'ctrl:TailIkRZ%s_Jnt.rotateY' % (iix+1))
			iix+=1
		ix+=1
	# Ctrl 24 Bck----------------------------------------
	attr = ['X', 'Y', 'Z']
	iix = 93
	for attrs in attr:
		mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs), '%sCha%s_Pma.input1D[1]' % (elem,iix-1))
		iix-=1

# Ctrl 25 ==========================================================================================================================
# Ctrl 25 ==========================================================================================================================
	ix = 96
	ikCtrl = mc.ls('ctrl:TailIk25_Ctrl')
	for each in range(0, 1):
		pmaNode = mc.createNode ('plusMinusAverage', n = ('%s%s_Pma' % (elem , ix+1)))
		mc.connectAttr ('%s.rotateY' % ikCtrl[0], '%s.input1D[0]' % pmaNode)
		mc.connectAttr ('%s.output1D' % pmaNode , '%s.rotateY' % listsIkSca[96])
		mdvNodeCha = mc.createNode ('multiplyDivide', n= ('%sCha%s_Mdv' % (elem , ix+1)))
		mc.setAttr ('%s.input2X' % mdvNodeCha, 0.75)
		mc.setAttr ('%s.input2Y' % mdvNodeCha, 0.5)
		mc.setAttr ('%s.input2Z' % mdvNodeCha, 0.25)
		mc.connectAttr('%s.rotateY' % listsIkSca[96], '%s.input1X' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[96], '%s.input1Y' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[96], '%s.input1Z' % mdvNodeCha)

		# Connect Control 25 Rotate 
		mdvRotNode = mc.createNode ('multiplyDivide', n= ('%s%sRot_Mdv' % (elem , ix+1)))
		mc.connectAttr ('%s.rotateZ' % fkCtrlLists[0], '%s.input1X' % mdvRotNode)
		mc.connectAttr ('%s.outputX' % mdvRotNode , '%s.input1D[1]' % pmaNode)
		mc.setAttr ('%s.input2X' % mdvRotNode, -1)

		# Connect Scale  XYZ----------------------------------------------------------------------------
		# Connect Scale  XYZ----------------------------------------------------------------------------
		mc.connectAttr ('%s.scaleX' % ikCtrl[0], '%s.input3D[%s].input3Dx'% (pmaNode,ix))
		mc.connectAttr ('%s.scaleY' % ikCtrl[0], '%s.input3D[%s].input3Dy'% (pmaNode,ix))
		mc.connectAttr ('%s.scaleZ' % ikCtrl[0], '%s.input3D[%s].input3Dz'% (pmaNode,ix))

		mc.connectAttr ('%s.output3Dx' % pmaNode, '%sTailIkRZ%s_Jnt.scaleX' % (nameSp, ix+1))
		mc.connectAttr ('%s.output3Dy' % pmaNode, '%sTailIkRZ%s_Jnt.scaleY' % (nameSp, ix+1))
		mc.connectAttr ('%s.output3Dz' % pmaNode, '%sTailIkRZ%s_Jnt.scaleZ' % (nameSp, ix+1))
		# Connect Scale  XYZ----------------------------------------------------------------------------
		# Connect Scale  XYZ----------------------------------------------------------------------------

		# Lists All Control Rotate ------------------------------
		# Lists All Control Rotate ------------------------------
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[1], '%s.input1D[2]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[2], '%s.input1D[3]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[3], '%s.input1D[4]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[4], '%s.input1D[5]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[5], '%s.input1D[6]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[6], '%s.input1D[7]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[7], '%s.input1D[8]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[8], '%s.input1D[9]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[9], '%s.input1D[10]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[10], '%s.input1D[11]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[11], '%s.input1D[12]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[12], '%s.input1D[13]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[13], '%s.input1D[14]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[14], '%s.input1D[15]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[15], '%s.input1D[16]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[16], '%s.input1D[17]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[17], '%s.input1D[18]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[18], '%s.input1D[19]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[19], '%s.input1D[20]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[20], '%s.input1D[21]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[21], '%s.input1D[22]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[22], '%s.input1D[23]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[23], '%s.input1D[24]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[24], '%s.input1D[25]' % pmaNode)


		# # Connect Mdv Node for Rotate
		# # Loop Example
		# iimd = 97
		# iiimd = -1
		# imd = 0
		# iiiimd = 4
		# value = [0.75, 0.5, 0.25]
		# attrSca = ['X', 'Y', 'Z']
		# for each in range(0, 3):
		# 	mdvNodeSca = mc.createNode ('multiplyDivide', n= ('%sCon%s_Mdv' % (elem , imd+1)))
		# 	for each in attrSca:
		# 		mc.connectAttr ('%sTailIkRZ%s_Jnt.scale%s' % (nameSp, ix+1, each) , '%s.input1%s' % (mdvNodeSca,each))
		# 		mc.setAttr ('%s.input2%s' % (mdvNodeSca, each), value[imd])	
				
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		addScale = mc.createNode ('plusMinusAverage', n = ('%sY%s%s_Pma' % (elem , imd+1, each)))
		# 		mc.addAttr(ln='default', k=True, dv=value[iiimd])	
		# 		addScaleBck = mc.createNode ('plusMinusAverage', n = ('%sYB%s%s_Pma' % (elem , imd+1, each)))			
		# 		mc.addAttr(ln='defaultBck', k=True, dv=0)
		# 		mc.connectAttr('%s.output%s' % (mdvNodeSca, each), '%s.input1D[%s]' % (addScale, 0))
		# 		mc.connectAttr('%s.default' % addScale, '%s.input1D[%s]' % (addScale, 1))
		# 		mc.connectAttr('%s.output1D' % addScale, '%s.input1D[%s]' % (addScaleBck, 0))

		# 		# Connect Scale Back-----------------------------------
		# 		# mc.connectAttr('%s.output1D' % addScale, '%s.input1D[%s]' % (addScaleBck, 0))
		# 		mc.connectAttr('%s.defaultBck' % addScaleBck, '%s.input1D[%s]' % (addScaleBck, 1))
		# 		mc.connectAttr('%s.output1D' % addScaleBck, '%sXB%s%s_Pma.input1D[%s]' % (elem, iiiimd-1, each, 2))
		# 													# '%sAB%s%s_Pma' % (elem , imd+1, each)))
		# 		mc.connectAttr('%s.output1D' % addScaleBck, 'ctrl:TailIkRZ%s_Jnt.scale%s' % (iimd+1, each)) 

		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 		# Createing Loop For Scale XYZ ---------------------------------------------------------------------
		# 	iiiimd-=1
		# 	iiimd-=1	
		# 	iimd+=1
		# 	imd+=1


	#Ctrl 25 Bck----------------------------------------
	attr = ['X', 'Y', 'Z']
	iix = 96
	ixx = 97
	for attrs in attr:
		pmaNodeCha = mc.createNode ('plusMinusAverage', n = ('%sCha%s_Pma' % (elem , iix+1)))
		mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs), '%sCha%s_Pma.input1D[0]' % (elem,iix+1))
		mc.connectAttr ('%s.output1D' % pmaNodeCha, '%sCha%s_Pma.input1D[1]' % (elem,ixx-1))
		iix+=1
		ixx-=1

