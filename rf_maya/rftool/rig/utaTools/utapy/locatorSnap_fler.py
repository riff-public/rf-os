import maya.cmds as mc
naemSpace = 'nakee_combRig:'
lstPthMTmp = mc.ls('PathsM*_Jnt')
lstPthUpJnt = mc.ls('PathsUp*_Jnt')
lstTailPthTmp = mc.ls('*:TailPathsTmp*_Jnt')
lstOfstGrpCtrl = mc.ls('*:TailFkCtrlOfst_*_Grp')
lstLoc = []

NoLstPthTmp = len(lstPthMTmp)

counter = 1 ;
b = 0
grpLocMn = mc.createNode('transform',n='LocatorSnap_Grp')
for a in range(0,1):
    grpLoc = mc.createNode('transform',n='locSnp' + str(counter).zfill(2)+'_Grp')
    locSnp = mc.spaceLocator(n = 'locSnp' + str(counter).zfill(2))
    counter += 1
    
    mc.parent(locSnp,grpLoc)
    mc.parent(grpLoc,grpLocMn)
  
    pointCon = mc.pointConstraint(lstPthMTmp[a],grpLoc)
    mc.delete ( pointCon ) 
    
    mc.pointConstraint ( lstPthUpJnt[a] , locSnp , mo = True )
    
    mc.orientConstraint(locSnp,lstOfstGrpCtrl[a] , mo = False)
    mc.aimConstraint(lstTailPthTmp[a+1],locSnp, mo = False, aim = (0.0,0.0,-1.0) , u = (0.0,1.0,0.0), wut= 'vector')
    a += 1
    
    for i in range(NoLstPthTmp):
        grpLoc = mc.createNode('transform',n='locSnp' + str(counter).zfill(2)+'_Grp')
        locSnp = mc.spaceLocator(n = 'locSnp' + str(counter).zfill(2))
        counter += 1
        
        mc.parent(locSnp,grpLoc)
        mc.parent(grpLoc,grpLocMn)
      
        pointCon = mc.pointConstraint(lstPthMTmp[i],grpLoc)
        mc.delete ( pointCon ) 
        
        mc.pointConstraint ( lstPthUpJnt[i] , locSnp , mo = True )
        
        lstLoc.append(locSnp)
        
        if i != 0 :
            mc.orientConstraint(locSnp,lstOfstGrpCtrl[i] , mo = False)
    
        if i != (NoLstPthTmp - 1) :
            mc.aimConstraint(lstTailPthTmp[i+1],locSnp, mo = False, aim = (0.0,1.0,0.0) , u = (0.0,0.0,1.0), wut= 'vector')