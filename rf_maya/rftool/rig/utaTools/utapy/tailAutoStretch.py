import maya.cmds as mc
reload(mc)


def tailAutoStretch(elem ,value, jntTyValue , tailAutoSt, *args):
	sels = ('OnOffSwitch1_Ctrl')
	tailIkCrv = ('TailIk_Crv')
	placeMentCtrl = ('Place_Ctrl')
	ikJnt = mc.ls('TailIk*_Jnt')

	# add Attribute
	mc.addAttr(sels, ln = 'Stretch________',at='double',dv = 0,k=True)
	mc.setAttr('%s.Stretch________' % sels, lock = True)
	mc.addAttr(sels, ln = 'autoStretch',at='double',dv = 0,k=True)
	mc.setAttr('%s.autoStretch' % sels, tailAutoSt)
	# mc.addAttr(sels, ln = 'tyValue',at='double',dv = 0,k=True)
	# mc.setAttr('%s.tyValue' % sels, jntTyValue)
	# Advice Value = 8.3

	# CreateNode
	crvInfoNode = mc.createNode ('curveInfo', n = ('%sAutoStr_CrvInfo' % elem))
	mdvASD = mc.createNode ('multiplyDivide', n = ('%sAutoStrDef_Mdv' % elem))
	mc.setAttr('%s.input2X' % mdvASD, value)
	mdvASDiv = mc.createNode ('multiplyDivide', n = ('%sAutoStrDiv_Mdv' % elem))
	mc.setAttr ('%s.operation' % mdvASDiv,2)
	mdvASMOut = mc.createNode ('multiplyDivide', n = ('%sAutoStrMOut_Mdv' % elem))
	blendCorNode = mc.createNode ('blendColors', n = ('%sAutoStrBlendCor_BCol' % elem))
	pmaNode = mc.createNode ('plusMinusAverage', n = ('%sAutoStrPma_Pma' % elem))
	mc.setAttr('%s.input2D[1].input2Dx' % pmaNode, jntTyValue)

	# ConnectAttribute
	mc.connectAttr('%sShape.worldSpace[0]' % tailIkCrv,  '%s.inputCurve' % crvInfoNode)
	mc.connectAttr('%s.arcLength' %  crvInfoNode, '%s.color1R' % blendCorNode)
	mc.connectAttr('%s.autoStretch' % sels, '%s.blender' % blendCorNode)
	mc.connectAttr('%s.scaleY' % placeMentCtrl, '%s.input1X' % mdvASD)
	mc.connectAttr('%s.outputR' % blendCorNode, '%s.input1X' % mdvASDiv)
	mc.connectAttr('%s.outputX' % mdvASD, '%s.color2R' % blendCorNode)
	mc.connectAttr('%s.outputX' % mdvASD, '%s.input2X' % mdvASDiv)
	mc.connectAttr('%s.outputX' % mdvASDiv, '%s.input2D[0].input2Dx' % pmaNode)
	mc.connectAttr('%s.output2Dx' % pmaNode, '%s.input1X' % mdvASMOut)
	# mc.connectAttr('%s.tyValue' % sels,'%s.input2D[1].input2Dx' % pmaNode)
	# Connect AutoStretch to Joint
	a = 1
	for a in range(8,91):
		mc.connectAttr('%s.outputX' % mdvASMOut, 'TailIk%s_Jnt.translateY' % a)
		a += 1

	# mdvASMOut = mc.createNode('multiplyDivide' , n = 'test_mdv')
	# for i in range(8,91):
	#     num = '%d' %(int(i))
	#     mc.connectAttr(mdvASMOut+'.output.outputX','TailIk'+str(num) +'_Jnt'+'.translate.translateY')