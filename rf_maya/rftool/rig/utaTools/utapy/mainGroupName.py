import maya.cmds as mc
reload(mc)

from lpRig import mainGroup
reload(mainGroup)

from rf_utils.context import context_info
reload(context_info)



def mainGroupName (*args):
	# asset = context_info.ContextPathInfo()
	# projectName = asset.path.name() if mc.file(q=True, sn=True) else ''
	# # if maya is not projectName to pass
	# 	#Exsample a = 1 if (1<0) else 2
	# print 'Creating Main Groups'
	# nameObjs =  projectName.split('/')
	# subType = nameObjs[-1]
	subType = 'Rig'
	mgObj = mainGroup.MainGroup(subType)
	print 'SubType : ',subType