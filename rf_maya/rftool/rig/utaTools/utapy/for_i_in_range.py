#range
listA = [ 4 , 3 , 2 , 1 ] ;
listB = [ 'A' , 'B' , 'C' , 'D' ] ;

for i in range ( 0 , len ( listA ) ) :
    print listA [ i ] , listB [ i ] ;

# blendShapeAdder
from lpRig import blendShapeAdder
blendShapeAdder.run()

# localControlMaker
from lpRig import localControlMaker
localControlMaker.run()