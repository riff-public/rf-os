import maya.cmds as mc
def fixAmpFacial(*args):
    ctrl_list = []
    for ctrl in mc.ls('*_Ctrl'):
        for trans in 'trs':
            for ax in 'xyz':
                if not mc.getAttr(ctrl+'.%s%s'%(trans,ax),l=1):
                    if "FaceLwrBsh_Ctrl" in '%s.%s%s'%(ctrl,trans,ax) or "FaceUprBsh_Ctrl" in '%s.%s%s'%(ctrl,trans,ax): pass
                    else: ctrl_list.append('%s.%s%s'%(ctrl,trans,ax))
                    
    for ctrl in ctrl_list:
        node = list(set(mc.listConnections(ctrl,s=0,d=1, p=0,scn=1)))
        for n in node:
            type = mc.nodeType(n)
            if type == 'clamp':
                ctrlName,at = ctrl.split('.')   
                ctrlShape = ctrlName + 'Shape'
                maxAt = '.x' + at + 'l'
                max = mc.getAttr(ctrlName+maxAt )
                minAt = '.m' + at + 'l'
                min = mc.getAttr(ctrlName+minAt )

                ctrlMaxAt = at + '_Max'
                ctrlMinAt = at + '_Min'
                
                if not mc.objExists(ctrlShape + '.maxMinAmp'):
                    mc.addAttr(ctrlShape, longName = 'maxMinAmp' , at = 'float', k = True, dv = 0)
                    mc.setAttr(ctrlShape + '.maxMinAmp', l = True)
                    
                if not mc.objExists(ctrlShape + '.' + ctrlMaxAt):
                    mc.addAttr(ctrlShape, longName = ctrlMaxAt , at = 'float', k = True, dv = max)
                    mc.addAttr(ctrlShape, longName = ctrlMinAt, at = 'float', k = True, dv = min)
                    mc.connectAttr(ctrlShape + '.' + ctrlMaxAt, ctrlName + maxAt) #max
                    mc.connectAttr(ctrlShape + '.' + ctrlMinAt, ctrlName + minAt) #min
                else: pass 
                
                nodeName = n[:-4]
                minRv = mc.getAttr(nodeName + '_Cmp.minR')
                maxRv = mc.getAttr(nodeName + '_Cmp.maxR')

                if minRv == 0 and maxRv != 0 :
                    mc.connectAttr(ctrlShape + '.' + ctrlMaxAt, nodeName + '_Cmp.maxR')
                    mc.connectAttr(ctrlShape + '.' + ctrlMinAt, nodeName + '_Cmp.minG')
                
                    div = mc.shadingNode('multiplyDivide',asUtility = True, name = nodeName + '_Div')
                    mc.setAttr(div+'.operation', 2)
                    mc.setAttr(div+'.input1X', 1)
                    mc.setAttr(div+'.input1Y', 1)
                
                    mc.connectAttr(ctrlShape + '.' + ctrlMaxAt, div+'.input2X')
                    mc.connectAttr(ctrlShape + '.' + ctrlMinAt, div+'.input2Y')
                    mc.connectAttr(div + '.outputX', nodeName + '_Mdv.input2X')
                    mc.connectAttr(div + '.outputY', nodeName + '_Mdv.input2Y')

                elif maxRv == 0 and minRv != 0 :
                    mc.connectAttr(ctrlShape + '.' + ctrlMaxAt, nodeName + '_Cmp.maxG')
                    mc.connectAttr(ctrlShape + '.' + ctrlMinAt, nodeName + '_Cmp.minR')
                
                    div = mc.shadingNode('multiplyDivide',asUtility = True, name = nodeName + '_Div')
                    mc.setAttr(div+'.operation', 2)
                    mc.setAttr(div+'.input1Y', 1)
                    mc.setAttr(div+'.input1X', 1)
                
                    mc.connectAttr(ctrlShape + '.' + ctrlMaxAt, div+'.input2Y')
                    mc.connectAttr(ctrlShape + '.' + ctrlMinAt, div+'.input2X')
                    mc.connectAttr(div + '.outputY', nodeName + '_Mdv.input2Y')
                    mc.connectAttr(div + '.outputX', nodeName + '_Mdv.input2X')

                elif maxRv != 0 and minRv != 0 :
                    mc.connectAttr(ctrlShape + '.' + ctrlMaxAt, nodeName + '_Cmp.maxR')
                    mc.connectAttr(ctrlShape + '.' + ctrlMinAt, nodeName + '_Cmp.minR')
                
                    div = mc.shadingNode('multiplyDivide',asUtility = True, name = nodeName + '_Div')
                    mc.setAttr(div+'.operation', 2)
                    mc.setAttr(div+'.input1X', 1)
                    mc.setAttr(div+'.input1Y', 1)
                
                    mc.connectAttr(ctrlShape + '.' + ctrlMaxAt, div+'.input2X')
                    mc.connectAttr(ctrlShape + '.' + ctrlMinAt, div+'.input2Y')
                    mc.connectAttr(div + '.outputX', nodeName + '_Mdv.input2X')
                    mc.connectAttr(div + '.outputY', nodeName + '_Mdv.input2Y')


                    
            elif type == 'blendColors':
                nodeBc = list(set(mc.listConnections(n,t = 'clamp')))[0]
                
                ctrlName,at = ctrl.split('.')   
                ctrlShape = ctrlName + 'Shape'
                maxAt = '.x' + at + 'l'
                max = mc.getAttr(ctrlName+maxAt )
                minAt = '.m' + at + 'l'
                min = mc.getAttr(ctrlName+minAt )

                ctrlMaxAt = at + '_Max'
                ctrlMinAt = at + '_Min'
                
                if not mc.objExists(ctrlShape + '.maxMinAmp'):
                    mc.addAttr(ctrlShape, longName = 'maxMinAmp' , at = 'float', k = True, dv = 0)
                    mc.setAttr(ctrlShape + '.maxMinAmp', l = True)
                    
                if not mc.objExists(ctrlShape + '.' + ctrlMaxAt):
                    mc.addAttr(ctrlShape, longName = ctrlMaxAt , at = 'float', k = True, dv = max)
                    mc.addAttr(ctrlShape, longName = ctrlMinAt, at = 'float', k = True, dv = min)
                    mc.connectAttr(ctrlShape + '.' + ctrlMaxAt, ctrlName + maxAt) #max
                    mc.connectAttr(ctrlShape + '.' + ctrlMinAt, ctrlName + minAt) #min
                else: pass 
                
                nodeName = nodeBc[:-4]
                minRv = mc.getAttr(nodeName + '_Cmp.minR')            
                if minRv == 0 :
                    mc.connectAttr(ctrlShape + '.' + ctrlMaxAt, nodeName + '_Cmp.maxR')
                    mc.connectAttr(ctrlShape + '.' + ctrlMinAt, nodeName + '_Cmp.minG')
                
                    div = mc.shadingNode('multiplyDivide',asUtility = True, name = nodeName + '_Div')
                    mc.setAttr(div+'.operation', 2)
                    mc.connectAttr(ctrlShape + '.' + ctrlMaxAt, div + '.input1X')
                    mc.setAttr(div+'.input2X', max)
                    mc.connectAttr(ctrlShape + '.' + ctrlMinAt, div + '.input1Y')
                    mc.setAttr(div+'.input2Y', min)
                                    
                    cal = mc.shadingNode('multiplyDivide',asUtility = True, name = nodeName + '_Cal')
                    mc.setAttr(cal+'.operation', 2)
                    calXV = mc.getAttr(nodeName + '_Mdv.input2X' )
                    calYV = mc.getAttr(nodeName + '_Mdv.input2Y' )
                    mc.setAttr(cal+'.input1X', calXV)
                    mc.setAttr(cal+'.input1Y', calYV) 
                    mc.connectAttr(div + '.outputX', cal + '.input2X')
                    mc.connectAttr(div + '.outputY', cal + '.input2Y') 
                            
                    mc.connectAttr(cal + '.outputX', nodeName + '_Mdv.input2X')
                    mc.connectAttr(cal + '.outputY', nodeName + '_Mdv.input2Y')            

                else:
                    mc.connectAttr(ctrlShape + '.' + ctrlMaxAt, nodeName + '_Cmp.maxG')
                    mc.connectAttr(ctrlShape + '.' + ctrlMinAt, nodeName + '_Cmp.minR')
                
                    div = mc.shadingNode('multiplyDivide',asUtility = True, name = nodeName + '_Div')
                    mc.setAttr(div+'.operation', 2)
                    mc.connectAttr(ctrlShape + '.' + ctrlMaxAt, div + '.input1Y')
                    mc.setAttr(div+'.input2Y', max)
                    mc.connectAttr(ctrlShape + '.' + ctrlMinAt, div + '.input1X')
                    mc.setAttr(div+'.input2X', min)
                                    
                    cal = mc.shadingNode('multiplyDivide',asUtility = True, name = nodeName + '_Cal')
                    mc.setAttr(cal+'.operation', 2)
                    calXV = mc.getAttr(nodeName + '_Mdv.input2X' )
                    calYV = mc.getAttr(nodeName + '_Mdv.input2Y' )
                    mc.setAttr(cal+'.input1X', calXV)
                    mc.setAttr(cal+'.input1Y', calYV) 
                    mc.connectAttr(div + '.outputX', cal + '.input2X')
                    mc.connectAttr(div + '.outputY', cal + '.input2Y') 
                            
                    mc.connectAttr(cal + '.outputX', nodeName + '_Mdv.input2X')
                    mc.connectAttr(cal + '.outputY', nodeName + '_Mdv.input2Y') 

            elif type == 'remapValue':
                ctrlName,at = ctrl.split('.')   
                ctrlShape = ctrlName + 'Shape'
                maxAt = '.x' + at + 'l'
                max = mc.getAttr(ctrlName+maxAt )
                minAt = '.m' + at + 'l'
                min = mc.getAttr(ctrlName+minAt )

                ctrlMaxAt = at + '_Max'
                ctrlMinAt = at + '_Min'
                
                if not mc.objExists(ctrlShape + '.maxMinAmp'):
                    mc.addAttr(ctrlShape, longName = 'maxMinAmp' , at = 'float', k = True, dv = 0)
                    mc.setAttr(ctrlShape + '.maxMinAmp', l = True)
                    
                if not mc.objExists(ctrlShape + '.' + ctrlMaxAt):
                    mc.addAttr(ctrlShape, longName = ctrlMaxAt , at = 'float', k = True, dv = max)
                    mc.addAttr(ctrlShape, longName = ctrlMinAt, at = 'float', k = True, dv = min)
                    mc.connectAttr(ctrlShape + '.' + ctrlMaxAt, ctrlName + maxAt) #max
                    mc.connectAttr(ctrlShape + '.' + ctrlMinAt, ctrlName + minAt) #min
                else: pass
                
                remap = mc.getAttr(n + '.inputMax')
                if remap > 0:
                    mc.connectAttr(ctrlShape + '.' + ctrlMaxAt,n + '.inputMax')
                elif remap < 0:
                    mc.connectAttr(ctrlShape + '.' + ctrlMinAt,n + '.inputMax')
                else: pass   