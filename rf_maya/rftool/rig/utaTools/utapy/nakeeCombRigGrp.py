import sys
import maya.cmds as mc
reload(mc)
def nakeeCombHdRig (*args):

	# Create Grp
	HdHiPosZro = mc.createNode ('transform' , n = 'HdHiPosZro_Grp')
	HdHiPosSpc = mc.createNode ('transform' , n = 'HdHiPosSpc_Grp')
	HdMidPosZro = mc.createNode ('transform' , n = 'HdMidPosZro_Grp')
	HdMidPosSpc = mc.createNode ('transform' , n = 'HdMidPosSpc_Grp')
	HdLowPosZro = mc.createNode ('transform' , n = 'HdLowPosZro_Grp')
	HdLowPosSpc = mc.createNode ('transform' , n = 'HdLowPosSpc_Grp')
	mc.parent(HdHiPosSpc, HdHiPosZro)
	mc.parent(HdMidPosSpc, HdMidPosZro)
	mc.parent(HdLowPosSpc, HdLowPosZro)

	# Snap Grp
	mc.delete(mc.parentConstraint('hdRig:RbnHdUpHdDfmRigJntZr_Grp', HdHiPosZro, mo=False))
	mc.delete(mc.parentConstraint('hdRig:HdMidHdDfmRig_Jnt', HdMidPosZro, mo=False))
	mc.delete(mc.parentConstraint('hdRig:RbnHdLowHdDfmRigJntZr_Grp', HdLowPosZro, mo=False))

	# Freez Transform Grp
	mc.setAttr('%s.ty' % HdHiPosSpc, -3.963)
	mc.makeIdentity(HdHiPosSpc,apply=True, t=1, r=1, s=1, n=0) 
	mc.setAttr('%s.ty' % HdLowPosSpc, 3.581)
	mc.makeIdentity(HdLowPosSpc,apply=True, t=1, r=1, s=1, n=0) 

	# connect HdRigGrp to PosGrp
	mc.connectAttr ('%s.translate' % 'hdRig:RbnHdUpHdDfmRigJntZr_Grp', '%s.translate' % HdHiPosSpc)
	mc.connectAttr ('%s.rotate' % 'hdRig:RbnHdUpHdDfmRigJntZr_Grp', '%s.rotate' % HdHiPosSpc)
	mc.connectAttr ('%s.scale' % 'hdRig:RbnHdUpHdDfmRigJntZr_Grp', '%s.scale' % HdHiPosSpc)
	mc.connectAttr ('%s.translate' % 'hdRig:HdMidHdDfmRig_Jnt', '%s.translate' % HdMidPosSpc)
	mc.connectAttr ('%s.rotate' % 'hdRig:HdMidHdDfmRig_Jnt', '%s.rotate' % HdMidPosSpc)
	mc.connectAttr ('%s.scale' % 'hdRig:HdMidHdDfmRig_Jnt', '%s.scale' % HdMidPosSpc)
	mc.connectAttr ('%s.translate' % 'hdRig:RbnHdLowHdDfmRigJntZr_Grp', '%s.translate' % HdLowPosSpc)
	mc.connectAttr ('%s.rotate' % 'hdRig:RbnHdLowHdDfmRigJntZr_Grp', '%s.rotate' % HdLowPosSpc)
	mc.connectAttr ('%s.scale' % 'hdRig:RbnHdLowHdDfmRigJntZr_Grp', '%s.scale' % HdLowPosSpc)

	# Create Group Sill Skin Jnt 
	HdDfmRigCtrlGrp = mc.createNode ('transform', n = 'HdDfmRigCtrlGrpZro_grp')
	HdDfmRigJntGrpGrp = mc.createNode ('transform', n = 'HdDfmRigJntGrpZro_grp')
	HdDfmRigSkinGrp = mc.createNode ('transform', n = 'HdDfmRigSkinGrpZro_grp')
	HdDfmRigStillGrp = mc.createNode ('transform', n = 'HdDfmRigStillGrpZro_grp')


	# Parent HdRig to Grp 
	mc.parent('hdRig:HdDfmRigCtrl_Grp', HdDfmRigCtrlGrp)
	mc.parent('hdRig:HdDfmRigJnt_Grp', HdDfmRigJntGrpGrp)
	mc.parent('hdRig:HdDfmRigSkin_Grp', HdDfmRigSkinGrp)
	mc.parent('hdRig:HdDfmRigStill_Grp', HdDfmRigStillGrp)

	# Parent To MainGrp
	mc.parent(HdDfmRigCtrlGrp, HdHiPosZro, HdMidPosZro, HdLowPosZro , 'ctrl:Ctrl_Grp')
	mc.parent(HdDfmRigJntGrpGrp, HdDfmRigSkinGrp, HdDfmRigSkinGrp, HdDfmRigStillGrp,  'ctrl:Still_Grp')
	# mc.parent(HdDfmRigCtrlGrp, 'ctrl:Ctrl_Grp')

	# PosGrp ParentConstraint Control Grp
	mc.parentConstraint('ctrl:Head_Jnt', HdDfmRigCtrlGrp, mo=True)
	mc.scaleConstraint('ctrl:Head_Jnt', HdDfmRigCtrlGrp, mo=True)
	mc.parentConstraint('ctrl:Head_Jnt', HdHiPosZro, mo=True)
	mc.scaleConstraint('ctrl:Head_Jnt', HdHiPosZro, mo=True)
	mc.parentConstraint('ctrl:Head_Jnt', HdMidPosZro, mo=True)
	mc.scaleConstraint('ctrl:Head_Jnt', HdMidPosZro, mo=True)
	mc.parentConstraint('ctrl:Head_Jnt', HdLowPosZro, mo=True)
	mc.scaleConstraint('ctrl:Head_Jnt', HdLowPosZro, mo=True)
	mc.parentConstraint(HdHiPosSpc, 'ctrl:HeadUpCtrlZr_1_Grp', mo=True)
	mc.parentConstraint(HdLowPosSpc, 'ctrl:JawACtrlZr_Grp', mo=True)

def nakeeCombDtlRig (*args):

	# Create DtlGrp
	DtlRigSkinGrp = mc.createNode ('transform', n = 'DtlRigSkinGrpZro_grp')
	DtlRigCtrlGrp = mc.createNode ('transform', n = 'DtlRigCtrlGrpZro_grp')
	DtlRigStillGrp = mc.createNode ('transform', n = 'DtlRigStillGrpZro_grp')

	# Parent DtlGrp
	mc.parent('dtlRig:DtlRigSkin_Grp', DtlRigSkinGrp)
	mc.parent('dtlRig:DtlRigCtrl_Grp', DtlRigCtrlGrp)
	mc.parent('dtlRig:DtlRigStill_Grp', DtlRigStillGrp)

	# Move Pivot
	mc.move(0, 16.139422, 41.950951, "dtlRig:CtrlDtlHiZroGrp.scalePivot", 'dtlRig:CtrlDtlHiZroGrp.rotatePivot', absolute=True)
	mc.move(0, 12.076551, 41.950951 ,'dtlRig:CtrlDtlMidZroGrp.scalePivot', 'dtlRig:CtrlDtlMidZroGrp.rotatePivot', absolute =True)
	mc.move (0, 7.469102, 37.772479,'dtlRig:CtrlDtlLowZroGrp.scalePivot', 'dtlRig:CtrlDtlLowZroGrp.rotatePivot',absolute= True)

	# Parent Constraint DtlRig Control
	mc.parentConstraint('ctrl:HeadUp_Jnt', 'dtlRig:CtrlDtlHiZroGrp', mo =True)
	mc.scaleConstraint('ctrl:HeadUp_Jnt', 'dtlRig:CtrlDtlHiZroGrp', mo =True)
	mc.parentConstraint('ctrl:Head_Jnt', 'dtlRig:CtrlDtlMidZroGrp', mo =True)
	mc.scaleConstraint('ctrl:Head_Jnt', 'dtlRig:CtrlDtlMidZroGrp', mo =True)
	mc.parentConstraint('ctrl:Jaw2_Jnt', 'dtlRig:CtrlDtlLowZroGrp', mo =True)
	mc.scaleConstraint('ctrl:Jaw2_Jnt', 'dtlRig:CtrlDtlLowZroGrp', mo =True)

	# Parent DtlRig to MainGroup
	mc.parent(DtlRigSkinGrp, DtlRigStillGrp, 'ctrl:Still_Grp')
	mc.parent(DtlRigCtrlGrp, 'ctrl:Ctrl_Grp')
