from maya import cmds as mc

def setAttributeDefault():
	grp = 'Char_Grp'
	attrs = ['scaleX', 'scaleY', 'scaleZ']
	ctrlAll = []
	value = 1
	charGrp = mc.listRelatives(grp, children = True)
	DelKey = []
	if not charGrp:
		return
	for obj in charGrp:
		shortName = obj.split("|")[-1]
		children = mc.listRelatives(shortName, fullPath=True, ad = True, type = 'transform')
		for each in children:
			childName = each.split("|")[-1]
			if '_Ctrl' in childName:
				## Get Attribute show on attribute editor
				childNameAttr = mc.listAttr(childName, v=True, c=True, k=True, s = True, u = True)
				# childNameAttr = mc.listAttr( childName, cfo = True)
				if childNameAttr:
					for eachAttr in childNameAttr:
						for i in attrs:
							if eachAttr == i:
								nodes = mc.keyframe('%s.%s' % (each, eachAttr), query = True,n = True)
								if nodes:
									mc.delete(nodes)
									DelKey.append(nodes)
								mc.setAttr('%s.%s' % (each, i), value)
								ctrlAll.append(eachAttr)

	# print len(ctrlAll)
	# print len	DelKey)
	print '>> D O N E <<'
