import maya.cmds as mc
import pymel.core as pm 
from lpRig import rigTools as lrr
reload(lrr)

def numberCtrlPathDo(*args):
	# elemObj = pm.textField( "rivertNameTextField", text = 'AddRig', p = mainLayout , editable = True , h = 30)
	elemObj = pm.textField ( "ctrlNameTxtField" , q  = True , text = True  )
	elemObj = pm.textField ( "ctrlNumberTxtField" , q  = True , text = True  )
	lrr.rivetToSeletedEdge(part=elemObj)

def numberCtrlPathUi (*args) :
    #check if window exists
	if pm.window ( 'numberCtrlPathUi' , exists = True ) :
		pm.deleteUI ( 'numberCtrlPathUi' ) ;

	#title Ui
	title = "numberCtrlPath Ui v.1 (28-01-2022)" ;

	#window
	window = pm.window ( 'numberCtrlPathUi', title = title , w = 250, h = 32, mnb = True , mxb = True , sizeable = True , rtf = True ) ;
	# mainTab_A = pm.tabLayout ( innerMarginWidth = 5 , innerMarginHeight = 5 , p = mainLayout ) ;



	#mainLayout
	mainLayout = pm.rowColumnLayout ( w = 250, h = 25, p = window , nc = 3 ,columnWidth = [ ( 1 , 75 ) , ( 2 , 100 ) , (3, 75)] );
	pm.rowColumnLayout (p = mainLayout ) ;

	#textField,Button
	pm.text( label='Curve Name >>' , p = mainLayout, al = "left")
	elemObj = pm.textField( "ctrlNameTxtField", text = 'AddRig', p = mainLayout , editable = True , h = 25)
	pm.button ("OkButton",label = "Ok" , p = mainLayout, c = numberCtrlPathDo ,bgc = (0.411765, 0.411765, 0.411765)) ; 

	pm.text( label='Control Number >>' , p = mainLayout, al = "left")
	elemObj = pm.textField( "ctrlNumberTxtField", text = 'AddRig', p = mainLayout , editable = True , h = 25)
	pm.button ("CancelButton",label = "Cancel" , p = mainLayout, c = numberCtrlPathDo ,bgc = (0.411765, 0.411765, 0.411765)) ; 
	# return elemObj
	#showWindow
	pm.showWindow ( window ) ;

