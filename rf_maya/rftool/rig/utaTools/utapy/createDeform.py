import maya.cmds as mc
from utaTools.utapy import utaCore
reload(utaCore)
from lpRig import rigTools as lrr
reload(lrr)


class CreateDeform( object ):
	def __init__( self ):
		self.objSel = []
		self.name = ''
		self.side = ''
		self.lastName = ''
		self.dupGeo = ''
		self.dupGeoGrp = ''
		self.objLtt = []
		self.LttBaseGrp = []

		self.poiJnt = [mc.ls('*:EyeAdjLFT_Jnt'), mc.ls('*:EyeAdjRGT_Jnt')]
		self.addRigPar = 'AddRigStillPar_Grp'
		self.fclRigPar = 'FclRigStillPar_Grp'
		self.LttAddRigGrp = 'LttAddRig_Grp'
		self.fclRigGeo = [mc.ls('*:EyeFclRig_L_Grp'), mc.ls('*:EyeFclRig_R_Grp')]
		self.stillGrp = mc.ls('*:Still_Grp')[0]

	def run(self, elem ):

		## select nameSplit
		self.objSel = utaCore.selected()
		i = 0

		for each in self.objSel:
			## 1. Delete BlendShape on Skin Geo	
			objLists = utaCore.listGeo(sels = each)
			utaCore.listNode(sels = objLists, type = 'blendShape', clear = True)
			## 2. Duplicate Geo for Lattice
			self.name, self.side, self.lastName = utaCore.splitName(sels = [each])
			self.dupGeo , self.dupGeoGrp = utaCore.duplicate(obj = each, 
															name = self.name, 
															side = self.side , 
															elem = elem)

			## 3. Add BlnedShape from lttGeo to geoSkin
			skinObj = [each, self.dupGeo[0]]
			utaCore.addBlendShape(skinObj)	
			## 4. Copy SkinWeight 
			utaCore.copySkinWeight(skinObj)


			## 5. Add Lattice 
			self.objLtt , self.LttBaseGrp = utaCore.addLattice(sels = each, 
																name= self.name, 
																side = self.side, 
																elem = elem , 
																divisions = (6,6,6))

			## 6. Connect FclRigGeo to lttAddRig_Grp
			for eachs in self.fclRigGeo:
			# if self.fclRigGeo[i] :
				fclGeo = [self.dupGeo[0], eachs]
				utaCore.addBlendShape(fclGeo)	

			## 7. Set Attribute SkinWeight 0, 1, and Delete SkinWeight
			utaCore.setAttrSkinWeight(selsGrp = [each], 
										sels = '', 
										value = 0, 
										clear = False)

			## 8. Parent Constraint Lattice 
			utaCore.parentScaleConstraintLoop(obj = self.poiJnt[i], 
											lists = [self.LttBaseGrp],
											par = True, 
											sca = True, 
											ori = False, 
											poi = False)

			## 9. Group Zip
			if mc.objExists( self.LttAddRigGrp):
				pass
			else:
				self.LttAddRigGrp = utaCore.createGrp([self.LttAddRigGrp])[0]

			mc.parent(self.dupGeoGrp, self.LttBaseGrp, self.LttAddRigGrp)

			i += 1

		## 10. Zip FclRigGrp
		if mc.objExists( self.addRigPar ) :
			pass
		else:
			addRigPar = utaCore.createGrp([self.addRigPar])
			self.addRigPar = addRigPar[0]
			mc.parent(self.addRigPar, self.stillGrp)
		checkPar = mc.listRelatives(self.LttAddRigGrp, p = True)

		if not checkPar == None:
			if checkPar[0] == self.addRigPar:
				pass
		else:
			mc.parent(self.LttAddRigGrp	, self.addRigPar)

	print 'Generate >>  Done !'
		