import os,sys,traceback
sys.path.append(r"P:\lib\local\utaTools\utapy")
sys.path.append(r"P:\lib\local\lpRig")
sys.path.append(r"P:\lib\local")
sys.path.append(r"P:\lib\local\utaTools\nuTools\util")
sys.path.append(r"P:\lib\local\utaTools\scripts")
import pymel.core as pm
# reload(pm)
from lpRig import rigTools as lrr
reload(lrr)
import maya.cmds as mc ;
reload(mc)

def splitEbShape(*args):
    #fix Body or Head
    
    # sels = mc.ls(sl=True)       
    suffixBodySide = pm.textField ( "txtFieldBodySide" , q  = True , text = True  )
    suffixEbSide = pm.textField ( "txtFieldEbSide" , q  = True , text = True  )  
    suffixBodyEach = pm.textField ( "txtFieldBodyEach" , q  = True , text = True  )
    suffixEbEach = pm.textField ( "txtFieldEbEach" , q  = True , text = True  )
    suffixEbJntL = "eb_L_jnt"
    suffixEbJntR = "eb_R_jnt"
    suffixEbInJntL = "ebIn_L_jnt"
    suffixEbInJntR = "ebIn_R_jnt"
    suffixEbMidJntL = "ebMid_L_jnt"
    suffixEbMidJntR = "ebMid_R_jnt"
    suffixEbOutJntL = "ebOut_L_jnt"
    suffixEbOutJntR = "ebOut_R_jnt"
    suffixBodyPrEyebrowUp = "BodyPrEyebrowUp_Geo"
    suffixEbAllEyebrowUp = "EyebrowPrEyebrowUp_Geo"
    suffixBodyPrEyebrowDn = "BodyPrEyebrowDn_Geo"
    suffixEbAllEyebrowDn = "EyebrowPrEyebrowDn_Geo"
    suffixBodyPrEyebrowIn = "BodyPrEyebrowIn_Geo"
    suffixEbAllEyebrowIn = "EyebrowPrEyebrowIn_Geo"
    suffixBodyPrEyebrowOut = "BodyPrEyebrowOut_Geo"
    suffixEbAllEyebrowOut = "EyebrowPrEyebrowOut_Geo"
    suffixBodyPrEyebrowRotUp = "BodyPrEyebrowRotUp_Geo"
    suffixEyebrowPrEyebrowRotUp = "EyebrowPrEyebrowRotUp_Geo"
    suffixBodyPrEyebrowRotDn = "BodyPrEyebrowRotDn_Geo"
    suffixEyebrowPrEyebrowRotDn = "EyebrowPrEyebrowRotDn_Geo"

    checkEbSide = mc.ls(suffixEbSide, sl=True)
    checkEbSideAmount = len(checkEbSide)
    checkEbEach = mc.ls(suffixEbEach, sl=True)
    checkEbEachAmount = len(checkEbEach)

    try:
        lrr.applyVertexMoveWithSkinWeight(
                                            baseObj= suffixEbEach ,
                                            srcObj=  suffixEbAllEyebrowUp ,
                                            tarObj=  'EyebrowEbRig_Geo_UpInEbRig_L_Bsh' ,
                                            skinGeo= suffixEbEach ,
                                            jnt= suffixEbInJntL
                                            )

        lrr.applyVertexMoveWithSkinWeight(
                                            baseObj= suffixEbEach ,
                                            srcObj=  suffixEbAllEyebrowUp ,
                                            tarObj=  'EyebrowEbRig_Geo_UpInEbRig_R_Bsh' ,
                                            skinGeo= suffixEbEach ,
                                            jnt= suffixEbInJntR
                                            )
        lrr.applyVertexMoveWithSkinWeight(
                                            baseObj= suffixEbEach ,
                                            srcObj=  suffixEbAllEyebrowUp ,
                                            tarObj=  'EyebrowEbRig_Geo_UpMidEbRig_L_Bsh' ,
                                            skinGeo= suffixEbEach ,
                                            jnt= suffixEbMidJntL
                                            ) 
        lrr.applyVertexMoveWithSkinWeight(
                                            baseObj= suffixEbEach ,
                                            srcObj=  suffixEbAllEyebrowUp ,
                                            tarObj=  'EyebrowEbRig_Geo_UpMidEbRig_R_Bsh' ,
                                            skinGeo= suffixEbEach ,
                                            jnt= suffixEbMidJntR
                                            )
        lrr.applyVertexMoveWithSkinWeight(
                                            baseObj= suffixEbEach ,
                                            srcObj=  suffixEbAllEyebrowUp ,
                                            tarObj=  'EyebrowEbRig_Geo_UpOutEbRig_L_Bsh' ,
                                            skinGeo= suffixEbEach ,
                                            jnt= suffixEbOutJntL
                                            )
        lrr.applyVertexMoveWithSkinWeight(
                                            baseObj= suffixEbEach ,
                                            srcObj=  suffixEbAllEyebrowUp ,
                                            tarObj=  'EyebrowEbRig_Geo_UpOutEbRig_R_Bsh' ,
                                            skinGeo= suffixEbEach ,
                                            jnt= suffixEbOutJntR
                                            )
        lrr.applyVertexMoveWithSkinWeight(
                                            baseObj= suffixEbEach ,
                                            srcObj=  suffixEbAllEyebrowDn ,
                                            tarObj=  'EyebrowEbRig_Geo_DnInEbRig_L_Bsh' ,
                                            skinGeo= suffixEbEach ,
                                            jnt= suffixEbInJntL
                                            ) 
        lrr.applyVertexMoveWithSkinWeight(
                                            baseObj= suffixEbEach ,
                                            srcObj=  suffixEbAllEyebrowDn ,
                                            tarObj=  'EyebrowEbRig_Geo_DnInEbRig_R_Bsh' ,
                                            skinGeo= suffixEbEach ,
                                            jnt= suffixEbInJntR
                                            )
        lrr.applyVertexMoveWithSkinWeight(
                                            baseObj= suffixEbEach ,
                                            srcObj=  suffixEbAllEyebrowDn ,
                                            tarObj=  'EyebrowEbRig_Geo_DnMidEbRig_L_Bsh' ,
                                            skinGeo= suffixEbEach ,
                                            jnt= suffixEbMidJntL
                                            )
        lrr.applyVertexMoveWithSkinWeight(
                                            baseObj= suffixEbEach ,
                                            srcObj=  suffixEbAllEyebrowDn ,
                                            tarObj=  'EyebrowEbRig_Geo_DnMidEbRig_R_Bsh' ,
                                            skinGeo= suffixEbEach ,
                                            jnt= suffixEbMidJntR
                                            )
        lrr.applyVertexMoveWithSkinWeight(
                                            baseObj= suffixEbEach ,
                                            srcObj=  suffixEbAllEyebrowDn ,
                                            tarObj=  'EyebrowEbRig_Geo_DnOutEbRig_L_Bsh' ,
                                            skinGeo= suffixEbEach ,
                                            jnt= suffixEbOutJntL
                                            ) 
        lrr.applyVertexMoveWithSkinWeight(
                                            baseObj= suffixEbEach ,
                                            srcObj=  suffixEbAllEyebrowDn ,
                                            tarObj=  'EyebrowEbRig_Geo_DnOutEbRig_R_Bsh' ,
                                            skinGeo= suffixEbEach ,
                                            jnt= suffixEbOutJntR
                                            )
        lrr.applyVertexMoveWithSkinWeight(
                                            baseObj= suffixEbEach ,
                                            srcObj=  suffixEbAllEyebrowIn ,
                                            tarObj=  'EyebrowEbRig_Geo_InInEbRig_L_Bsh' ,
                                            skinGeo= suffixEbEach ,
                                            jnt= suffixEbInJntL
                                            )
        lrr.applyVertexMoveWithSkinWeight(
                                            baseObj= suffixEbEach ,
                                            srcObj=  suffixEbAllEyebrowIn ,
                                            tarObj=  'EyebrowEbRig_Geo_InInEbRig_R_Bsh' ,
                                            skinGeo= suffixEbEach ,
                                            jnt= suffixEbInJntR
                                            )
        lrr.applyVertexMoveWithSkinWeight(
                                            baseObj= suffixEbEach ,
                                            srcObj=  suffixEbAllEyebrowIn ,
                                            tarObj=  'EyebrowEbRig_Geo_InMidEbRig_L_Bsh' ,
                                            skinGeo= suffixEbEach ,
                                            jnt= suffixEbMidJntL
                                            )    
        lrr.applyVertexMoveWithSkinWeight(
                                            baseObj= suffixEbEach ,
                                            srcObj=  suffixEbAllEyebrowIn ,
                                            tarObj=  'EyebrowEbRig_Geo_InMidEbRig_R_Bsh' ,
                                            skinGeo= suffixEbEach ,
                                            jnt= suffixEbMidJntR
                                            )
        lrr.applyVertexMoveWithSkinWeight(
                                            baseObj= suffixEbEach ,
                                            srcObj=  suffixEbAllEyebrowIn ,
                                            tarObj=  'EyebrowEbRig_Geo_InOutEbRig_L_Bsh' ,
                                            skinGeo= suffixEbEach ,
                                            jnt= suffixEbOutJntL
                                            )
        lrr.applyVertexMoveWithSkinWeight(
                                            baseObj= suffixEbEach ,
                                            srcObj=  suffixEbAllEyebrowIn ,
                                            tarObj=  'EyebrowEbRig_Geo_InOutEbRig_R_Bsh' ,
                                            skinGeo= suffixEbEach ,
                                            jnt= suffixEbOutJntR
                                            )
        lrr.applyVertexMoveWithSkinWeight(
                                            baseObj= suffixEbEach ,
                                            srcObj=  suffixEbAllEyebrowOut ,
                                            tarObj=  'EyebrowEbRig_Geo_OutInEbRig_L_Bsh' ,
                                            skinGeo= suffixEbEach ,
                                            jnt= suffixEbInJntL
                                            )
        lrr.applyVertexMoveWithSkinWeight(
                                            baseObj= suffixEbEach ,
                                            srcObj=  suffixEbAllEyebrowOut ,
                                            tarObj=  'EyebrowEbRig_Geo_OutInEbRig_R_Bsh' ,
                                            skinGeo= suffixEbEach ,
                                            jnt= suffixEbInJntR
                                            )    
        lrr.applyVertexMoveWithSkinWeight(
                                            baseObj= suffixEbEach ,
                                            srcObj=  suffixEbAllEyebrowOut ,
                                            tarObj=  'EyebrowEbRig_Geo_OutMidEbRig_L_Bsh' ,
                                            skinGeo= suffixEbEach ,
                                            jnt= suffixEbMidJntL
                                            ) 
        lrr.applyVertexMoveWithSkinWeight(
                                            baseObj= suffixEbEach ,
                                            srcObj=  suffixEbAllEyebrowOut ,
                                            tarObj=  'EyebrowEbRig_Geo_OutMidEbRig_R_Bsh' ,
                                            skinGeo= suffixEbEach ,
                                            jnt= suffixEbMidJntR
                                            )

        lrr.applyVertexMoveWithSkinWeight(
                                            baseObj= suffixEbEach ,
                                            srcObj=  suffixEbAllEyebrowOut ,
                                            tarObj=  'EyebrowEbRig_Geo_OutOutEbRig_L_Bsh' ,
                                            skinGeo= suffixEbEach ,
                                            jnt= suffixEbOutJntL
                                            )
        lrr.applyVertexMoveWithSkinWeight(
                                            baseObj= suffixEbEach ,
                                            srcObj=  suffixEbAllEyebrowOut ,
                                            tarObj=  'EyebrowEbRig_Geo_OutOutEbRig_R_Bsh' ,
                                            skinGeo= suffixEbEach ,
                                            jnt= suffixEbOutJntR
                                            )
    except : pass

    try:
        lrr.applyVertexMoveWithSkinWeight(
                                            baseObj= suffixEbSide ,
                                            srcObj=  suffixEbAllEyebrowUp ,
                                            tarObj=  'EyebrowEbRig_Geo_UpEbRig_L_Bsh' ,
                                            skinGeo= suffixEbSide ,
                                            jnt= suffixEbJntL
                                            )

        lrr.applyVertexMoveWithSkinWeight(
                                            baseObj= suffixEbSide ,
                                            srcObj=  suffixEbAllEyebrowUp ,
                                            tarObj=  'EyebrowEbRig_Geo_UpEbRig_R_Bsh' ,
                                            skinGeo= suffixEbSide ,
                                            jnt= suffixEbJntR
                                            )

        lrr.applyVertexMoveWithSkinWeight(
                                            baseObj= suffixEbSide ,
                                            srcObj=  suffixEbAllEyebrowDn ,
                                            tarObj=  'EyebrowEbRig_Geo_DnEbRig_L_Bsh' ,
                                            skinGeo= suffixEbSide ,
                                            jnt= suffixEbJntL
                                            ) 

        lrr.applyVertexMoveWithSkinWeight(
                                            baseObj= suffixEbSide ,
                                            srcObj=  suffixEbAllEyebrowDn ,
                                            tarObj=  'EyebrowEbRig_Geo_DnEbRig_R_Bsh' ,
                                            skinGeo= suffixEbSide ,
                                            jnt= suffixEbJntR
                                            )

        lrr.applyVertexMoveWithSkinWeight(
                                            baseObj= suffixEbSide ,
                                            srcObj=  suffixEbAllEyebrowIn ,
                                            tarObj=  'EyebrowEbRig_Geo_InEbRig_L_Bsh' ,
                                            skinGeo= suffixEbSide ,
                                            jnt= suffixEbJntL
                                            )

        lrr.applyVertexMoveWithSkinWeight(
                                            baseObj= suffixEbSide ,
                                            srcObj=  suffixEbAllEyebrowIn ,
                                            tarObj=  'EyebrowEbRig_Geo_InEbRig_R_Bsh' ,
                                            skinGeo= suffixEbSide ,
                                            jnt= suffixEbJntR
                                            )

        lrr.applyVertexMoveWithSkinWeight(
                                            baseObj= suffixEbSide ,
                                            srcObj=  suffixEbAllEyebrowOut ,
                                            tarObj=  'EyebrowEbRig_Geo_OutEbRig_L_Bsh' ,
                                            skinGeo= suffixEbSide ,
                                            jnt= suffixEbJntL
                                            ) 

        lrr.applyVertexMoveWithSkinWeight(
                                            baseObj= suffixEbSide ,
                                            srcObj=  suffixEbAllEyebrowOut ,
                                            tarObj=  'EyebrowEbRig_Geo_OutEbRig_R_Bsh' ,
                                            skinGeo= suffixEbSide ,
                                            jnt= suffixEbJntR
                                            )
        lrr.applyVertexMoveWithSkinWeight(
                                            baseObj= suffixEbSide ,
                                            srcObj=  suffixEyebrowPrEyebrowRotUp ,
                                            tarObj=  'EyebrowEbRig_Geo_RotUpEbRig_L_Bsh' ,
                                            skinGeo= suffixEbSide ,
                                            jnt= suffixEbJntL
                                            )
        lrr.applyVertexMoveWithSkinWeight(
                                            baseObj= suffixEbSide ,
                                            srcObj=  suffixEyebrowPrEyebrowRotDn ,
                                            tarObj=  'EyebrowEbRig_Geo_RotDnEbRig_L_Bsh' ,
                                            skinGeo= suffixEbSide ,
                                            jnt= suffixEbJntL
                                            )
        lrr.applyVertexMoveWithSkinWeight(
                                            baseObj= suffixEbSide ,
                                            srcObj=  suffixEyebrowPrEyebrowRotUp ,
                                            tarObj=  'EyebrowEbRig_Geo_RotUpEbRig_R_Bsh' ,
                                            skinGeo= suffixEbSide ,
                                            jnt= suffixEbJntR
                                            )
        lrr.applyVertexMoveWithSkinWeight(
                                            baseObj= suffixEbSide ,
                                            srcObj=  suffixEyebrowPrEyebrowRotDn ,
                                            tarObj=  'EyebrowEbRig_Geo_RotDnEbRig_R_Bsh' ,
                                            skinGeo= suffixEbSide ,
                                            jnt= suffixEbJntR
                                            )
    except : pass
# No yes Eb or Body Side    
    lrr.applyVertexMoveWithSkinWeight(  
                                        baseObj= suffixBodySide ,
                                        srcObj=  suffixBodyPrEyebrowUp ,
                                        tarObj=  'BodyEbRig_Geo_UpEbRig_L_Bsh' ,
                                        skinGeo= suffixBodySide ,
                                        jnt= suffixEbJntL
                                        )
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= suffixBodySide ,
                                        srcObj=  suffixBodyPrEyebrowUp ,
                                        tarObj=  'BodyEbRig_Geo_UpEbRig_R_Bsh' ,
                                        skinGeo= suffixBodySide ,
                                        jnt= suffixEbJntR
                                        )
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= suffixBodySide ,
                                        srcObj=  suffixBodyPrEyebrowDn ,
                                        tarObj=  'BodyEbRig_Geo_DnEbRig_L_Bsh' ,
                                        skinGeo= suffixBodySide ,
                                        jnt= suffixEbJntL
                                        )
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= suffixBodySide ,
                                        srcObj=  suffixBodyPrEyebrowDn ,
                                        tarObj=  'BodyEbRig_Geo_DnEbRig_R_Bsh' ,
                                        skinGeo= suffixBodySide ,
                                        jnt= suffixEbJntR
                                        )
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= suffixBodySide ,
                                        srcObj=  suffixBodyPrEyebrowIn ,
                                        tarObj=  'BodyEbRig_Geo_InEbRig_L_Bsh' ,
                                        skinGeo= suffixBodySide ,
                                        jnt= suffixEbJntL
                                        )
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= suffixBodySide ,
                                        srcObj=  suffixBodyPrEyebrowIn ,
                                        tarObj=  'BodyEbRig_Geo_InEbRig_R_Bsh' ,
                                        skinGeo= suffixBodySide ,
                                        jnt= suffixEbJntR
                                        )
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= suffixBodySide ,
                                        srcObj=  suffixBodyPrEyebrowOut ,
                                        tarObj=  'BodyEbRig_Geo_OutEbRig_L_Bsh' ,
                                        skinGeo= suffixBodySide ,
                                        jnt= suffixEbJntL
                                        )
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= suffixBodySide ,
                                        srcObj=  suffixBodyPrEyebrowOut ,
                                        tarObj=  'BodyEbRig_Geo_OutEbRig_R_Bsh' ,
                                        skinGeo= suffixBodySide ,
                                        jnt= suffixEbJntR
                                        )
#-------
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= suffixBodySide ,
                                        srcObj=  suffixBodyPrEyebrowRotUp ,
                                        tarObj=  'BodyEbRig_Geo_RotUpEbRig_L_Bsh' ,
                                        skinGeo= suffixBodySide ,
                                        jnt= suffixEbJntL
                                        )
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= suffixBodySide ,
                                        srcObj=  suffixBodyPrEyebrowRotDn ,
                                        tarObj=  'BodyEbRig_Geo_RotDnEbRig_L_Bsh' ,
                                        skinGeo= suffixBodySide ,
                                        jnt= suffixEbJntL
                                        )
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= suffixBodySide ,
                                        srcObj=  suffixBodyPrEyebrowRotUp ,
                                        tarObj=  'BodyEbRig_Geo_RotUpEbRig_R_Bsh' ,
                                        skinGeo= suffixBodySide ,
                                        jnt= suffixEbJntR
                                        )
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= suffixBodySide ,
                                        srcObj=  suffixBodyPrEyebrowRotDn ,
                                        tarObj=  'BodyEbRig_Geo_RotDnEbRig_R_Bsh' ,
                                        skinGeo= suffixBodySide ,
                                        jnt= suffixEbJntR
                                        )   
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= suffixBodyEach ,
                                        srcObj=  suffixBodyPrEyebrowUp ,
                                        tarObj=  'BodyEbRig_Geo_UpInEbRig_L_Bsh' ,
                                        skinGeo= suffixBodyEach ,
                                        jnt= suffixEbInJntL
                                        )

    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= suffixBodyEach ,
                                        srcObj=  suffixBodyPrEyebrowUp ,
                                        tarObj=  'BodyEbRig_Geo_UpInEbRig_R_Bsh' ,
                                        skinGeo= suffixBodyEach ,
                                        jnt= suffixEbInJntR
                                        )

    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= suffixBodyEach ,
                                        srcObj=  suffixBodyPrEyebrowUp ,
                                        tarObj=  'BodyEbRig_Geo_UpMidEbRig_L_Bsh' ,
                                        skinGeo= suffixBodyEach ,
                                        jnt= suffixEbMidJntL
                                        )

    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= suffixBodyEach ,
                                        srcObj=  suffixBodyPrEyebrowUp ,
                                        tarObj=  'BodyEbRig_Geo_UpMidEbRig_R_Bsh' ,
                                        skinGeo= suffixBodyEach ,
                                        jnt= suffixEbMidJntR
                                        )

    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= suffixBodyEach ,
                                        srcObj=  suffixBodyPrEyebrowUp ,
                                        tarObj=  'BodyEbRig_Geo_UpOutEbRig_L_Bsh' ,
                                        skinGeo= suffixBodyEach ,
                                        jnt= suffixEbOutJntL
                                        )

    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= suffixBodyEach ,
                                        srcObj=  suffixBodyPrEyebrowUp ,
                                        tarObj=  'BodyEbRig_Geo_UpOutEbRig_R_Bsh' ,
                                        skinGeo= suffixBodyEach ,
                                        jnt= suffixEbOutJntR
                                        )
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= suffixBodyEach ,
                                        srcObj=  suffixBodyPrEyebrowDn ,
                                        tarObj=  'BodyEbRig_Geo_DnInEbRig_L_Bsh' ,
                                        skinGeo= suffixBodyEach ,
                                        jnt= suffixEbInJntL
                                        )

    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= suffixBodyEach ,
                                        srcObj=  suffixBodyPrEyebrowDn ,
                                        tarObj=  'BodyEbRig_Geo_DnInEbRig_R_Bsh' ,
                                        skinGeo= suffixBodyEach ,
                                        jnt= suffixEbInJntR
                                        )

    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= suffixBodyEach ,
                                        srcObj=  suffixBodyPrEyebrowDn ,
                                        tarObj=  'BodyEbRig_Geo_DnMidEbRig_L_Bsh' ,
                                        skinGeo= suffixBodyEach ,
                                        jnt= suffixEbMidJntL
                                        )

    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= suffixBodyEach ,
                                        srcObj=  suffixBodyPrEyebrowDn ,
                                        tarObj=  'BodyEbRig_Geo_DnMidEbRig_R_Bsh' ,
                                        skinGeo= suffixBodyEach ,
                                        jnt= suffixEbMidJntR
                                        )

    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= suffixBodyEach ,
                                        srcObj=  suffixBodyPrEyebrowDn ,
                                        tarObj=  'BodyEbRig_Geo_DnOutEbRig_L_Bsh' ,
                                        skinGeo= suffixBodyEach ,
                                        jnt= suffixEbOutJntL
                                        )

    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= suffixBodyEach ,
                                        srcObj=  suffixBodyPrEyebrowDn ,
                                        tarObj=  'BodyEbRig_Geo_DnOutEbRig_R_Bsh' ,
                                        skinGeo= suffixBodyEach ,
                                        jnt= suffixEbOutJntR
                                        )

    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= suffixBodyEach ,
                                        srcObj=  suffixBodyPrEyebrowIn ,
                                        tarObj=  'BodyEbRig_Geo_InInEbRig_L_Bsh' ,
                                        skinGeo= suffixBodyEach ,
                                        jnt= suffixEbInJntL
                                        )
 
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= suffixBodyEach ,
                                        srcObj=  suffixBodyPrEyebrowIn ,
                                        tarObj=  'BodyEbRig_Geo_InInEbRig_R_Bsh' ,
                                        skinGeo= suffixBodyEach ,
                                        jnt= suffixEbInJntR
                                        )

    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= suffixBodyEach ,
                                        srcObj=  suffixBodyPrEyebrowIn ,
                                        tarObj=  'BodyEbRig_Geo_InMidEbRig_L_Bsh' ,
                                        skinGeo= suffixBodyEach ,
                                        jnt= suffixEbMidJntL
                                        )

    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= suffixBodyEach ,
                                        srcObj=  suffixBodyPrEyebrowIn ,
                                        tarObj=  'BodyEbRig_Geo_InMidEbRig_R_Bsh' ,
                                        skinGeo= suffixBodyEach ,
                                        jnt= suffixEbMidJntR
                                        ) 
                                        
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= suffixBodyEach ,
                                        srcObj=  suffixBodyPrEyebrowIn ,
                                        tarObj=  'BodyEbRig_Geo_InOutEbRig_L_Bsh' ,
                                        skinGeo= suffixBodyEach ,
                                        jnt= suffixEbOutJntL
                                        )

    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= suffixBodyEach ,
                                        srcObj=  suffixBodyPrEyebrowIn ,
                                        tarObj=  'BodyEbRig_Geo_InOutEbRig_R_Bsh' ,
                                        skinGeo= suffixBodyEach ,
                                        jnt= suffixEbOutJntR
                                        ) 

    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= suffixBodyEach ,
                                        srcObj=  suffixBodyPrEyebrowOut ,
                                        tarObj=  'BodyEbRig_Geo_OutInEbRig_L_Bsh' ,
                                        skinGeo= suffixBodyEach ,
                                        jnt= suffixEbInJntL
                                        )

    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= suffixBodyEach ,
                                        srcObj=  suffixBodyPrEyebrowOut ,
                                        tarObj=  'BodyEbRig_Geo_OutInEbRig_R_Bsh' ,
                                        skinGeo= suffixBodyEach ,
                                        jnt= suffixEbInJntR
                                        )

    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= suffixBodyEach ,
                                        srcObj=  suffixBodyPrEyebrowOut ,
                                        tarObj=  'BodyEbRig_Geo_OutMidEbRig_L_Bsh' ,
                                        skinGeo= suffixBodyEach ,
                                        jnt= suffixEbMidJntL
                                        )

    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= suffixBodyEach ,
                                        srcObj=  suffixBodyPrEyebrowOut ,
                                        tarObj=  'BodyEbRig_Geo_OutMidEbRig_R_Bsh' ,
                                        skinGeo= suffixBodyEach ,
                                        jnt= suffixEbMidJntR
                                        )


    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= suffixBodyEach ,
                                        srcObj=  suffixBodyPrEyebrowOut ,
                                        tarObj=  'BodyEbRig_Geo_OutOutEbRig_L_Bsh' ,
                                        skinGeo= suffixBodyEach ,
                                        jnt= suffixEbOutJntL
                                        )

    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= suffixBodyEach ,
                                        srcObj=  suffixBodyPrEyebrowOut ,
                                        tarObj=  'BodyEbRig_Geo_OutOutEbRig_R_Bsh' ,
                                        skinGeo= suffixBodyEach ,
                                        jnt= suffixEbOutJntR
                                        )

    print "Creating >> -- splitEbShape is Done!! --"