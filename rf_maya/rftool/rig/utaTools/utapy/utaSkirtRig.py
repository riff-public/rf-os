import maya.cmds as mc
import pkmel.core as pc
import pkmel.rigTools as rigTools
import pkmel.ribbon as pr
reload( pc )
reload( rigTools )
reload( pr )

from nuTools import misc
reload(misc)

#------------------------------------------------------------------------
# from utaTools.utapy import utaSkirtRig
# reload(utaSkirtRig)

# a = utaSkirtRig.NonRollRig(
#                        parent='pelvisNnRlSkin_Jnt' ,
#                        animGrp='legNnrlRig_grp' ,
#                        rootJnt='upLeg_L_jnt' ,
#                        endJnt='lowLeg_L_jnt' ,
#                        parentRoot='upLegNnRl_L_Jnt' ,
#                        parentEnd='lowLegNnRl_L_Jnt' ,
#                        name='skirt' ,
#                        side='_L' ,
#                        aimAx='y+' ,
#                        upAx='x-'
#                    )
#------------------------------------------------------------------------
class AimRig( object ) :

	def __init__(
					self ,
					parent='' ,
					animGrp='anim_grp' ,
					rootJnt='' ,
					endJnt='' ,
					parentRoot='' ,
					parentEnd='' ,
					name='' ,
					side='' ,
					aimAx='y+' ,
					upAx='x+'
				) :

		'''
		Base class for aim rig.
		Controller will be created at the position of 'rootJnt'
		and will be parented to 'parentRoot'.
		Target will be created at the position of 'endJnt'
		and will be parented to 'parentEnd'.
		'rigGrp' will be parented to 'parent'.

		'''
		
		self.vecDict = {
							'x+' : ( 1 , 0 , 0 ) ,
							'x-' : ( -1 , 0 , 0 ) ,
							'y+' : ( 0 , 1 , 0 ) ,
							'y-' : ( 0 , -1 , 0 ) ,
							'z+' : ( 0 , 0 , 1 ) ,
							'z-' : ( 0 , 0 , -1 )
						}

		self.jntLen = pc.distance( rootJnt , endJnt )

		# Main group
		self.rigGrp = pc.Null()
		self.rigGrp.name = '%sAimRig%s_grp' % ( name , side )
		if parent :
			mc.parentConstraint( parent , self.rigGrp )
			mc.scaleConstraint( parent , self.rigGrp )

		if animGrp :
			self.rigGrp.parent( animGrp )

		# Root controller
		self.rootCtrl = rigTools.jointControl( 'sphere' )
		self.rootGmbl = pc.addGimbal( self.rootCtrl )
		self.rootAimGrp = pc.group( self.rootCtrl )
		self.rootZGrp = pc.group( self.rootAimGrp )
		self.rootAimOriGrp = pc.Null()

		self.rootAimOriGrp.parent( self.rootZGrp )

		self.rootCtrl.name = '%sAimRoot%s_ctrl' % ( name , side )
		self.rootCtrl .color = 'blue'
		self.rootGmbl.name = '%sAimRootGmbl%s_ctrl' % ( name , side )
		self.rootAimGrp.name = '%sAimRootCtrlAim%s_grp' % ( name , side )
		self.rootZGrp.name = '%sAimRootCtrlZro%s_grp' % ( name , side )
		self.rootAimOriGrp.name = '%sAimRootCtrlAimOri%s_grp' % ( name , side )

		self.rootZGrp.snap( rootJnt )
		mc.parentConstraint( parentRoot , self.rootZGrp , mo=True )
		mc.scaleConstraint( parentRoot , self.rootZGrp , mo=True )
		self.rootCtrl.lockHideAttrs( 'v' )
		self.rootZGrp.parent( self.rigGrp )

		# Ori loc/wor
		self.rootCtrl.add( ln='localWorld' , min=0 , max=1 , dv=1 , k=True )
		oriCon = pc.Dag( mc.orientConstraint( self.rootZGrp , self.rootAimOriGrp , self.rootAimGrp )[0] )

		# mc.connectAttr( self.rootCtrl.attr('localWorld') , '%s.w1' % oriCon )
		self.rootCtrl.attr('localWorld') >> oriCon.attr('w1')

		self.locWorRev = pc.Reverse()
		self.locWorRev.name = '%sAimRootCtrlAimOriLocWor%s_rev' % ( name , side )

		self.rootCtrl.attr('localWorld') >> self.locWorRev.attr('ix')
		# mc.connectAttr( self.locWorRev.attr('ox') , '%s.w0' % oriCon )
		self.locWorRev.attr('ox') >> oriCon.attr('w0')

		# Target
		self.trg = pc.Null()
		self.trgZroGrp = pc.group( self.trg )

		self.trg.name = '%sAimTrg%s_grp' % ( name , side )
		self.trgZroGrp.name = '%sAimTrgZro%s_grp' % ( name , side )

		self.trgZroGrp.snap( endJnt )
		self.trgZroGrp.parent( self.rigGrp )
		mc.parentConstraint( parentEnd , self.trgZroGrp , mo=True )

		# Up Controller
		self.upCtrl = pc.Control( 'plus' )
		self.upCtrlZroGrp = pc.group( self.upCtrl )

		self.upCtrl.name = '%sAimUp%s_ctrl' % ( name , side )
		self.upCtrlZroGrp.name = '%sAimUpCtrlZro%s_grp' % ( name , side )

		self.upCtrl.lockHideAttrs( 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
		self.upCtrl.color = 'blue'

		self.upCtrlZroGrp.parent( rootJnt )
		self.upCtrlZroGrp.attr( 't' ).v = ( 0 , 0 , 0 )

		if upAx[-1] == '+' :
			self.upCtrlZroGrp.attr( 't%s' % upAx[0] ).v = self.jntLen
		else :
			self.upCtrlZroGrp.attr( 't%s' % upAx[0] ).v = -self.jntLen

		self.upCtrlZroGrp.parent( self.rigGrp )

		# Up Curve
		( self.upCrv ,
		self.rootUpCl ,
		self.endUpUpCl ) = rigTools.crvGuide( self.upCtrl , rootJnt )

		self.upCrv.name = '%sAimUp%s_crv' % ( name , side )
		self.rootUpCl.name = '%sAimRootUp%s_cl' % ( name , side )
		self.endUpUpCl.name = '%sAimEndUp%s_cl' % ( name , side )

		self.upCrv.attr('inheritsTransform').value = 0
		self.upCrv.attr('overrideEnabled').value = 1
		self.upCrv.attr('overrideDisplayType').value = 2

		self.upCrv.parent( self.upCtrlZroGrp )
		self.upCrv.attr('t').value = ( 0 , 0 , 0 )

		rootCtrlShp = pc.Dag( self.rootCtrl.shape )
		rootCtrlShp.add( ln='upCtrlVis' , min=0 , max=1 , k=True )
		rootCtrlShp.attr( 'upCtrlVis' ) >> self.upCtrlZroGrp.attr( 'v' )

		pc.aimConstraint(
							self.trg ,
							self.rootAimOriGrp ,
							aim = self.vecDict[ aimAx ] ,
							u = self.vecDict[ upAx ] ,
							wut = 'object' ,
							wuo = self.upCtrl ,
						)

		# Connect to join
		self.rootParCons = mc.parentConstraint( self.rootGmbl , rootJnt , mo=True )
		self.rootScaCons = mc.scaleConstraint( self.rootGmbl , rootJnt , mo=True )

class SuspensionRig( object ) :

	def __init__(
					self ,
					animGrp='anim_grp' ,
					rootJnt='' ,
					endJnt='' ,
					parentRoot='' ,
					parentEnd='' ,
					name='' ,
					side='' ,
					aimAx='y+' ,
					upAx='x+'
				) :

		'''
		Instance two AimRigs to create the suspension rig.
		This module doesn't need 'parent' like the others.
		'parentRoot' will become 'parent' to 'rootRig'
		and 'parentEnd' will become 'parent' to 'endRig'.

		'''

		self.inverseVecDict = {
									'x+' : 'x-' ,
									'x-' : 'x+' ,
									'y+' : 'y-' ,
									'y-' : 'y+' ,
									'z+' : 'z-' ,
									'z-' : 'z+'
								}

		# Main group
		self.rigGrp = pc.Null()
		self.rigGrp.name = '%sSuspRig%s_grp' % ( name , side )
		if animGrp :
			self.rigGrp.parent( animGrp )

		self.rootRig = AimRig(
								parentRoot,
								animGrp,
								rootJnt,
								endJnt,
								parentRoot,
								parentEnd,
								'%sSuspRoot' % name,
								side,
								aimAx,
								upAx
							)

		self.endRig = AimRig(
								parentEnd,
								animGrp,
								endJnt,
								rootJnt,
								parentEnd,
								parentRoot,
								'%sSuspEnd' % name,
								side,
								self.inverseVecDict[aimAx] ,
								upAx
							)
		
		self.rootRig.rigGrp.parent( self.rigGrp )
		self.endRig.rigGrp.parent( self.rigGrp )

class NonRollRig( AimRig ) :
	
	def __init__(
					self ,
					parent='' ,
					animGrp='anim_grp' ,
					rootJnt='' ,
					endJnt='' ,
					parentRoot='' ,
					parentEnd='' ,
					name='' ,
					side='' ,
					aimAx='y+' ,
					upAx='x+'
				) :

		'''
		Inherit AimRig to create non-roll rig.
		upVec will be parented to 'parent' in this module.
		Auto-stretch and stretch rig will be added.

		'''

		AimRig.__init__(
							self ,
							parent,
							animGrp,
							rootJnt,
							endJnt,
							parentRoot,
							parentEnd,
							name,
							side,
							aimAx,
							upAx
						)

		mc.parentConstraint( parent , self.upCtrlZroGrp , mo=True )

		# End controller
		self.endCtrl = rigTools.jointControl( 'sphere' )
		self.endGmbl = pc.addGimbal( self.endCtrl )
		self.endOffGrp = pc.group( self.endCtrl )
		self.endZGrp = pc.group( self.endOffGrp )

		self.endCtrl.name = '%sAimEnd%s_ctrl' % ( name , side )
		self.endCtrl .color = 'blue'
		self.endGmbl.name = '%sAimEndGmbl%s_ctrl' % ( name , side )
		self.endOffGrp.name = '%sAimEndCtrlOff%s_grp' % ( name , side )
		self.endZGrp.name = '%sAimEndCtrlZro%s_grp' % ( name , side )

		self.endZGrp.snap( rootJnt )
		self.endOffGrp.snap( endJnt )
		self.endZGrp.parent( self.rigGrp)
		mc.pointConstraint( self.rootGmbl , self.endZGrp )
		mc.orientConstraint( self.rootGmbl , self.endZGrp )
		mc.scaleConstraint( self.rootGmbl , self.endZGrp )
		self.endCtrl.lockHideAttrs( 'v' )

		# Connect to joint
		self.endParCons = mc.parentConstraint( self.endGmbl , endJnt , mo=True )
		self.endScaCons = mc.scaleConstraint( self.endGmbl , endJnt , mo=True )

		# Auto Stretch
		parLen = pc.distance( parentRoot , parentEnd )
		self.rootCtrl.add( ln='autoStretch' , min=0 , max=1 , k=True )

		self.rootDistGrp = pc.Null()
		self.endDistGrp = pc.Null()

		self.rootDistGrp.name = '%sNonRlRootDist%s_grp' % ( name , side )
		self.endDistGrp.name = '%sNonRlEndDist%s_grp' % ( name , side )

		self.rootDistGrp.parent( self.rigGrp )
		self.endDistGrp.parent( self.rigGrp )

		mc.pointConstraint( parentRoot , self.rootDistGrp )
		mc.pointConstraint( parentEnd , self.endDistGrp )
		
		self.autoStrDist = pc.DistanceBetween()
		self.autoStrDist.name = '%sNonRlAutoStr%s_dist' % ( name , side )

		self.rootDistGrp.attr('t') >> self.autoStrDist.attr('p1')
		self.endDistGrp.attr('t') >> self.autoStrDist.attr('p2')

		self.autoStrMdv = pc.MultiplyDivide()
		self.autoStrMdv.name = '%sNonRlAutoStr%s_mdv' % ( name , side )

		self.autoStrMdv.attr('op').v = 2
		self.autoStrMdv.attr('i2x').v = parLen
		self.autoStrDist.attr('d') >> self.autoStrMdv.attr('i1x')

		self.strMul = pc.MultDoubleLinear()
		self.strMul.name = '%sNonRlAutoStr%s_mul' % ( name , side )

		ofstVal = mc.getAttr( '%s.t%s' % ( endJnt , aimAx[0] ) )
		self.autoStrMdv.attr('ox') >> self.strMul.attr('i1')
		self.strMul.attr('i2').v = ofstVal

		self.autoStrBln = pc.BlendTwoAttr()
		self.autoStrBln.name = '%sNonRlAutoStr%s_bln' % ( name , side )

		self.rootCtrl.attr('autoStretch') >> self.autoStrBln.attr('ab')
		self.autoStrBln.add( ln='default' , k=True )
		self.autoStrBln.attr('default').v = ofstVal
		self.autoStrBln.attr('default') >> self.autoStrBln.last()
		self.strMul.attr('o') >> self.autoStrBln.last()
		
		# Stretch
		self.strAdd = pc.AddDoubleLinear()
		self.strAdd.name = '%sNonRlStr%s_add' % ( name , side )
		self.strAdd.add( ln='default' , k=True )
		self.autoStrBln.attr('o') >> self.strAdd.attr('default')
		self.strAdd.attr('default') >> self.strAdd.attr('i1')
		
		self.strMul = pc.MultDoubleLinear()
		self.strMul.name = '%sNonRlStr%s_mul' % ( name , side )
		self.strMul.add( ln='default' , dv=ofstVal , k=True )
		self.strMul.attr('default') >> self.strMul.attr('i1')
		self.strMul.attr('o') >> self.strAdd.attr('i2')
		
		self.strAmpMul = pc.MultDoubleLinear()
		self.strAmpMul.name = '%sNonRlStrAmp%s_mul' % ( name , side )
		self.strAmpMul.add( ln='amp' , dv=0.1 , k=True )
		self.strAmpMul.attr('amp') >> self.strAmpMul.attr('i1')
		
		self.rootCtrl.add( ln='stretch' , k=True )
		self.rootCtrl.attr('stretch') >> self.strAmpMul.attr('i2')
		self.strAmpMul.attr('o') >> self.strMul.attr('i2')

		self.strAdd.attr('o') >> self.endOffGrp.attr('t%s'%aimAx[0])

		# Disable segment scale compensation
		mc.setAttr( '%s.segmentScaleCompensate' % rootJnt , 0 )
		mc.setAttr( '%s.segmentScaleCompensate' % endJnt , 0 )

class NonRollRig2( AimRig ) :
	
	def __init__(
					self ,
					parent='' ,
					animGrp='anim_grp' ,
					rootJnt='' ,
					endJnt='' ,
					parentRoot='' ,
					parentEnd='' ,
					name='' ,
					side='' ,
					aimAx='y+' ,
					upAx='x+'
				) :

		'''
		Inherit AimRig to create non-roll rig.
		upVec will be parented to 'parent' in this module.
		Auto-stretch and stretch rig will be added.

		'''

		self.vecDict = {
							'x+' : ( 1 , 0 , 0 ) ,
							'x-' : ( -1 , 0 , 0 ) ,
							'y+' : ( 0 , 1 , 0 ) ,
							'y-' : ( 0 , -1 , 0 ) ,
							'z+' : ( 0 , 0 , 1 ) ,
							'z-' : ( 0 , 0 , -1 )
						}

		self.jntLen = pc.distance( rootJnt , endJnt )

		# Main group
		self.rigGrp = pc.Null()
		self.rigGrp.name = '%sAimRig%s_grp' % ( name , side )
		if parent :
			mc.parentConstraint( parent , self.rigGrp )
			mc.scaleConstraint( parent , self.rigGrp )

		if animGrp :
			self.rigGrp.parent( animGrp )

		# Root controller
		self.rootCtrl = rigTools.jointControl( 'stick' )
		self.rootGmbl = pc.addGimbal( self.rootCtrl )
		self.rootAimGrp = pc.group( self.rootCtrl )
		self.rootZGrp = pc.group( self.rootAimGrp )
		self.rootAimOriGrp = pc.Null()

		self.rootAimOriGrp.parent( self.rootZGrp )

		self.rootCtrl.name = '%sAimRoot%s_ctrl' % ( name , side )
		self.rootCtrl .color = 'blue'
		self.rootGmbl.name = '%sAimRootGmbl%s_ctrl' % ( name , side )
		self.rootAimGrp.name = '%sAimRootCtrlAim%s_grp' % ( name , side )
		self.rootZGrp.name = '%sAimRootCtrlZro%s_grp' % ( name , side )
		self.rootAimOriGrp.name = '%sAimRootCtrlAimOri%s_grp' % ( name , side )

		self.rootZGrp.snap( rootJnt )
		mc.parentConstraint( parentRoot , self.rootZGrp , mo=True )
		mc.scaleConstraint( parentRoot , self.rootZGrp , mo=True )
		self.rootCtrl.lockHideAttrs( 'v' )
		self.rootZGrp.parent( self.rigGrp )

		# Ori loc/wor
		self.rootCtrl.add( ln='nonRoll' , min=0 , max=1 , dv=1 , k=True )
		oriCon = pc.Dag( mc.orientConstraint( self.rootZGrp , self.rootAimOriGrp , self.rootAimGrp )[0] )

		self.rootCtrl.attr('nonRoll') >> oriCon.attr('w1')

		self.nonRollRev = pc.Reverse()
		self.nonRollRev.name = '%sAimRootCtrlAimOriNonRoll%s_rev' % ( name , side )

		self.rootCtrl.attr('nonRoll') >> self.nonRollRev.attr('ix')
		self.nonRollRev.attr('ox') >> oriCon.attr('w0')

		# Target
		self.trg = pc.Null()
		self.trgZroGrp = pc.group( self.trg )

		self.trg.name = '%sAimTrg%s_grp' % ( name , side )
		self.trgZroGrp.name = '%sAimTrgZro%s_grp' % ( name , side )

		self.trgZroGrp.snap( endJnt )
		self.trgZroGrp.parent( self.rigGrp )
		# mc.parentConstraint( parentEnd , self.trgZroGrp , mo=True )
		
		# localWorld for trg
		mc.select(self.rootCtrl, parentRoot, parent, self.trgZroGrp)
		misc.createLocalWorld(constraintType='parent', 
			attrName='localWorld')

		# Up Controller
		self.upCtrl = pc.Control( 'plus' )
		self.upCtrlZroGrp = pc.group( self.upCtrl )

		self.upCtrl.name = '%sAimUp%s_ctrl' % ( name , side )
		self.upCtrlZroGrp.name = '%sAimUpCtrlZro%s_grp' % ( name , side )

		self.upCtrl.lockHideAttrs( 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
		self.upCtrl.color = 'blue'

		self.upCtrlZroGrp.parent( rootJnt )
		self.upCtrlZroGrp.attr( 't' ).v = ( 0 , 0 , 0 )

		if upAx[-1] == '+' :
			self.upCtrlZroGrp.attr( 't%s' % upAx[0] ).v = self.jntLen
		else :
			self.upCtrlZroGrp.attr( 't%s' % upAx[0] ).v = -self.jntLen

		self.upCtrlZroGrp.parent( self.rigGrp )

		# Up Curve
		( self.upCrv ,
		self.rootUpCl ,
		self.endUpUpCl ) = rigTools.crvGuide( self.upCtrl , rootJnt )

		self.upCrv.name = '%sAimUp%s_crv' % ( name , side )
		self.rootUpCl.name = '%sAimRootUp%s_cl' % ( name , side )
		self.endUpUpCl.name = '%sAimEndUp%s_cl' % ( name , side )

		self.upCrv.attr('inheritsTransform').value = 0
		self.upCrv.attr('overrideEnabled').value = 1
		self.upCrv.attr('overrideDisplayType').value = 2

		self.upCrv.parent( self.upCtrlZroGrp )
		self.upCrv.attr('t').value = ( 0 , 0 , 0 )

		rootCtrlShp = pc.Dag( self.rootCtrl.shape )
		rootCtrlShp.add( ln='upCtrlVis' , min=0 , max=1 , k=True )
		rootCtrlShp.attr( 'upCtrlVis' ) >> self.upCtrlZroGrp.attr( 'v' )

		pc.aimConstraint(
							self.trg ,
							self.rootAimOriGrp ,
							aim = self.vecDict[ aimAx ] ,
							u = self.vecDict[ upAx ] ,
							wut = 'object' ,
							wuo = self.upCtrl ,
						)

		# Connect to joint
		self.rootParCons = mc.parentConstraint( self.rootGmbl , rootJnt , mo=True )
		self.rootScaCons = mc.scaleConstraint( self.rootGmbl , rootJnt , mo=True )

		# ----------------------------

		mc.parentConstraint( parent , self.upCtrlZroGrp , mo=True )

		# End controller
		# self.endCtrl = rigTools.jointControl( 'sphere' )
		# self.endGmbl = pc.addGimbal( self.endCtrl )
		# self.endOffGrp = pc.group( self.endCtrl )
		# self.endZGrp = pc.group( self.endOffGrp )

		# self.endCtrl.name = '%sAimEnd%s_ctrl' % ( name , side )
		# self.endCtrl .color = 'blue'
		# self.endGmbl.name = '%sAimEndGmbl%s_ctrl' % ( name , side )
		# self.endOffGrp.name = '%sAimEndCtrlOff%s_grp' % ( name , side )
		# self.endZGrp.name = '%sAimEndCtrlZro%s_grp' % ( name , side )

		# self.endZGrp.snap( rootJnt )
		# self.endOffGrp.snap( endJnt )
		# self.endZGrp.parent( self.rigGrp)
		# mc.pointConstraint( self.rootGmbl , self.endZGrp )
		# mc.orientConstraint( self.rootGmbl , self.endZGrp )
		# mc.scaleConstraint( self.rootGmbl , self.endZGrp )
		# self.endCtrl.lockHideAttrs( 'v' )

		# Connect to joint
		# self.endParCons = mc.parentConstraint( self.endGmbl , endJnt , mo=True )
		# self.endScaCons = mc.scaleConstraint( self.endGmbl , endJnt , mo=True )

		# Auto Stretch
		# parLen = pc.distance( parentRoot , parentEnd )
		# self.rootCtrl.add( ln='autoStretch' , min=0 , max=1 , k=True )

		# self.rootDistGrp = pc.Null()
		# self.endDistGrp = pc.Null()

		# self.rootDistGrp.name = '%sNonRlRootDist%s_grp' % ( name , side )
		# self.endDistGrp.name = '%sNonRlEndDist%s_grp' % ( name , side )

		# self.rootDistGrp.parent( self.rigGrp )
		# self.endDistGrp.parent( self.rigGrp )

		# mc.pointConstraint( parentRoot , self.rootDistGrp )
		# mc.pointConstraint( parentEnd , self.endDistGrp )
		
		# self.autoStrDist = pc.DistanceBetween()
		# self.autoStrDist.name = '%sNonRlAutoStr%s_dist' % ( name , side )

		# self.rootDistGrp.attr('t') >> self.autoStrDist.attr('p1')
		# self.endDistGrp.attr('t') >> self.autoStrDist.attr('p2')

		# self.autoStrMdv = pc.MultiplyDivide()
		# self.autoStrMdv.name = '%sNonRlAutoStr%s_mdv' % ( name , side )

		# self.autoStrMdv.attr('op').v = 2
		# self.autoStrMdv.attr('i2x').v = parLen
		# self.autoStrDist.attr('d') >> self.autoStrMdv.attr('i1x')

		# self.strMul = pc.MultDoubleLinear()
		# self.strMul.name = '%sNonRlAutoStr%s_mul' % ( name , side )

		# ofstVal = mc.getAttr( '%s.t%s' % ( endJnt , aimAx[0] ) )
		# self.autoStrMdv.attr('ox') >> self.strMul.attr('i1')
		# self.strMul.attr('i2').v = ofstVal

		# self.autoStrBln = pc.BlendTwoAttr()
		# self.autoStrBln.name = '%sNonRlAutoStr%s_bln' % ( name , side )

		# self.rootCtrl.attr('autoStretch') >> self.autoStrBln.attr('ab')
		# self.autoStrBln.add( ln='default' , k=True )
		# self.autoStrBln.attr('default').v = ofstVal
		# self.autoStrBln.attr('default') >> self.autoStrBln.last()
		# self.strMul.attr('o') >> self.autoStrBln.last()
		
		# # Stretch
		# self.strAdd = pc.AddDoubleLinear()
		# self.strAdd.name = '%sNonRlStr%s_add' % ( name , side )
		# self.strAdd.add( ln='default' , k=True )
		# self.autoStrBln.attr('o') >> self.strAdd.attr('default')
		# self.strAdd.attr('default') >> self.strAdd.attr('i1')
		
		# self.strMul = pc.MultDoubleLinear()
		# self.strMul.name = '%sNonRlStr%s_mul' % ( name , side )
		# self.strMul.add( ln='default' , dv=ofstVal , k=True )
		# self.strMul.attr('default') >> self.strMul.attr('i1')
		# self.strMul.attr('o') >> self.strAdd.attr('i2')
		
		# self.strAmpMul = pc.MultDoubleLinear()
		# self.strAmpMul.name = '%sNonRlStrAmp%s_mul' % ( name , side )
		# self.strAmpMul.add( ln='amp' , dv=0.1 , k=True )
		# self.strAmpMul.attr('amp') >> self.strAmpMul.attr('i1')
		
		# self.rootCtrl.add( ln='stretch' , k=True )
		# self.rootCtrl.attr('stretch') >> self.strAmpMul.attr('i2')
		# self.strAmpMul.attr('o') >> self.strMul.attr('i2')

		# self.strAdd.attr('o') >> self.endOffGrp.attr('t%s'%aimAx[0])

		# Disable segment scale compensation
		mc.setAttr( '%s.segmentScaleCompensate' % rootJnt , 0 )
		mc.setAttr( '%s.segmentScaleCompensate' % endJnt , 0 )