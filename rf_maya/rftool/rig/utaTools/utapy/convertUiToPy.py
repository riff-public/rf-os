import sys, pprint
from pyside2uic import compileUi
##############################################################
# from utaTools.utapy import convertUiToPy
# reload(convertUiToPy)
# convertUiToPy.convert(inFile = "D:/Ken_RND/KenR_DesignGUI/test_001.ui")
##############################################################

def convert(inFile):
	outFile = inFile.replace(".ui", ".py")
	pyFile = open(outFile, "w")
	compileUi(inFile, pyFile, False, 4, False)
	pyFile.close()
	return outFile
