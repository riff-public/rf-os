import maya.cmds as mc
from utaTools.utapy import utaCore
reload(utaCore)
import pymel.core as pm 

def buildCopyShapeNewHairUI():
    # check if window exists
    if pm.window ( 'CopyShapeNewHairUi' , exists = True ) :
        pm.deleteUI ( 'CopyShapeNewHairUi' ) ;
    else : pass ;

    window = pm.window ( 'CopyShapeNewHairUi', title = "copyShapeAll v. 1.0" ,
        mnb = False , mxb = False , sizeable = True , rtf = True ) ;

    # winWidth = 250
    # tmpWidth = [winWidth*0.8, winWidth*0.2]
    # mc.window(title = 'CopyShapeNewHair' , width = winWidth)
    # mainCL = mc.columnLayout()

    pm.window ( 'CopyShapeNewHairUi' , e = True ,  w = 250 ) ;
    mainLayout = pm.rowColumnLayout ( nc = 1 , p = window , w = 250) ;

    ## Bsh_Grp
    rowLine1 = pm.rowColumnLayout ( nc = 5 , p = mainLayout , w = 250 ,cw = [ (1, 5),  ( 2 , 180 ) , (3, 5),( 4 , 53 ), (5, 5) ]);
    pm.rowColumnLayout (p = rowLine1 ) ;    
    mc.textField('BshGrp', text ='Bsh_Grp'  , ed = True , p = rowLine1 )
    pm.rowColumnLayout (p = rowLine1 ) ;    
    mc.button('BshGrpBotton' , label = ' << ', c = selectBshGrp_cmd, p = rowLine1)
    pm.rowColumnLayout (p = rowLine1 ) ;    

    ## New Bsh Group
    rowLine2 = pm.rowColumnLayout ( nc = 5 , p = mainLayout , w = 250 ,cw = [ (1, 5),  ( 2 , 180 ) , (3, 5),( 4 , 53 ), (5, 5) ]);
    pm.rowColumnLayout (p = rowLine2 ) ;    
    mc.textField('NewObj', text ='New Bsh Group'  , ed = True , p = rowLine2 )
    pm.rowColumnLayout (p = rowLine2 ) ;    
    mc.button('NewObjBotton' , label = ' << ', c = selectObj_cmd, p = rowLine2)
    pm.rowColumnLayout (p = rowLine2 ) ;    

    ## Run copyShapeNewhair_cmd
    rowLine3 = pm.rowColumnLayout ( nc = 3 , p = mainLayout , w = 250 ,cw = [ (1, 5),  ( 2 , 238 ) , (3, 5)]);
    pm.rowColumnLayout (p = rowLine3 ) ;    
    mc.button('Run Copy Shape', c = run, p = rowLine3, h  = 45)
    pm.rowColumnLayout (p = rowLine3 ) ;    


    mc.setParent('..')
    mc.showWindow()
    # buildCopyShapeNewHairUI()

def selectObj_cmd(obj = [],*args):
    strObj = ''
    obj = mc.ls(sl = True)
    for each in obj:
        if strObj == '':
            NewObj1=each.split('|')[-1]
            strObj = NewObj1
        else:
            NewObj2=each.split('|')[-1]
            strObj = strObj + ',' + NewObj2
    mc.textField("NewObj", e=True , text=strObj) 
    pm.button("NewObjBotton", e=True, bgc= (0.498039, 1, 0))

def selectBshGrp_cmd(obj = [],*args):
    strObj = ''
    obj = mc.ls(sl = True)
    for each in obj:
        if strObj == '':
            NewObj1=each.split('|')[-1]
            strObj = NewObj1
        else:
            NewObj2=each.split('|')[-1]
            strObj = strObj + ',' + NewObj2
    mc.textField("BshGrp", e=True , text=strObj) 
    pm.button("BshGrpBotton", e=True, bgc= (0.498039, 1, 0))

def run (*args):
    NewObj = mc.textField('NewObj', text =True, q = True)
    bshGrp = mc.textField('BshGrp', text =True, q = True)
    copyShapeNewhair_cmd(bshGrp  = bshGrp, NewObj =  NewObj)

def copyShapeNewhair_cmd(bshGrp , NewObj , *args):
    bshGrpList = bshGrp.split(',')
    print 'CopyShape :', len(bshGrpList) ,'Group'
    for each in bshGrpList:
        bshList = each
        refBshGrp = mc.ls('*:%s' % each)

        if NewObj:
            NewHairGrp = NewObj
        else:
            NewHairGrp = ''

        if NewHairGrp == '':
            objA = mc.listRelatives(refBshGrp , ad=True)
            objB = mc.listRelatives( bshList , ad=True)
            if len(objA) == len(objB):
                print "-------------------Copy Shape If-------------------"
                utaCore.copyShapeAll(objGrpA= [refBshGrp ], objGrpB = [bshList])
            else:
                print "Amount do not match"
                print 'Ref Bsh_Grp ' + "%i"%len(objA)
                print 'Bsh_Grp ' + '%i'%len(objB)
        else:

            mc.select('*' + NewHairGrp , add = True)
            AllNewobjs = mc.ls(sl=True)
            moveGrp = mc.createNode('transform',n='MoveStillPar_Grp')
            AllGrps = []
            for eachGrpOut in AllNewobjs:
                trueName = eachGrpOut.split('|')[-1]
                grpPN = mc.listRelatives(eachGrpOut,p=True)[0]
                Newname = mc.rename(eachGrpOut,'%s_PN_%s'%(grpPN,trueName))
                mc.parent(Newname,moveGrp)
                AllGrps.append(Newname)

            objA = mc.listRelatives( refBshGrp , ad=True)
            objB = mc.listRelatives( bshList , ad=True)
            if len(objA) == len(objB):
                print "-------------------Copy Shape Else-------------------"
                utaCore.copyShapeAll(objGrpA= [refBshGrp], objGrpB = [bshList])
            else:
                print "Amount do not match"
                print 'Ref Bsh_Grp ' + "%i"%len(objA)
                print 'Bsh_Grp ' + '%i'%len(objB)
            
            for eachGrpIn in AllGrps:
                namePN = eachGrpIn.split('_PN_')
                mc.parent(eachGrpIn,namePN[0])
                mc.rename(eachGrpIn,namePN[-1])
            mc.delete(moveGrp)
        mc.select(cl = True)

    # pm.button("RunCopyShapeButton", e=True, bgc= (0.498039, 1, 0))
