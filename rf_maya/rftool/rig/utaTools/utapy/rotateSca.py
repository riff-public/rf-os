import maya.cmds as mc
reload(mc)

def rotateSca (elem = '', part = '', *args):
	listsIkSca = mc.ls('TailIkRZ*_Jnt')
	mainCtrl = mc.ls('Move_Ctrl')
	listsIkScaCount =  len(listsIkSca)

	# ctrl 1 Connect
	fkCtrlLists = mc.ls('TailFk_*_Ctrl')
	# pmaNodeLists = mc.ls('RotateSca*_Pma')
	# print pmaNodeLists

# Ctrl 1 ----------------------------------------
	ix = 0
	for each in range(0, 1):
		pmaNode = mc.createNode ('plusMinusAverage', n = ('%s%s_Pma' % (part , ix+1)))
		mdvNode = mc.createNode ('multiplyDivide', n= ('%s%s_Mdv' % (part , ix+1)))
		mc.connectAttr ('%s.rotateZ' % mainCtrl[0], '%s.input1X' % mdvNode)
		mc.connectAttr ('%s.outputX' % mdvNode, '%s.input1D[%s]'% (pmaNode,ix))
		mc.setAttr ('%s.input2X' % mdvNode, -1)
		mc.connectAttr ('%s.output1D' % pmaNode , '%s.rotateY' % listsIkSca[0])

		mdvNodeCha = mc.createNode ('multiplyDivide', n= ('%sCha%s_Mdv' % (part , ix+1)))
		mc.setAttr ('%s.input1X' % mdvNodeCha, 0.75)
		mc.setAttr ('%s.input1Y' % mdvNodeCha, 0.5)
		mc.setAttr ('%s.input1Z' % mdvNodeCha, 0.25)
		mc.connectAttr('%s.rotateY' % listsIkSca[0], '%s.input2X' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[0], '%s.input2Y' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[0], '%s.input2Z' % mdvNodeCha)

		# Connect Control 1 Rotate 
		mc.connectAttr ('%s.rotateZ' % fkCtrlLists[0], '%s.input1Y' % mdvNode)
		mc.connectAttr ('%s.outputY' % mdvNode , '%s.input1D[%s]' % (pmaNode,ix+1))
		mc.setAttr ('%s.input2Y' % mdvNode, -1)

		attr = ['X', 'Y', 'Z']
		iix = 1
		for attrs in attr:
			pmaNodeCha = mc.createNode ('plusMinusAverage', n = ('%sCha%s_Pma' % (part , iix+1)))
			mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs) , '%s.input1D[0]' % pmaNodeCha)
			mc.connectAttr ('%s.output1D' % pmaNodeCha, 'TailIkRZ%s_Jnt.rotateY' % (iix+1))
			iix+=1
		ix+=1

# Ctrl 2 Fnt----------------------------------------
	ix = 4
	ikCtrl = mc.ls('TailIk2_Ctrl')
	for each in range(0, 1):
		pmaNode = mc.createNode ('plusMinusAverage', n = ('%s%s_Pma' % (part , ix+1)))
		mc.connectAttr ('%s.rotateY' % ikCtrl[0], '%s.input1D[0]' % pmaNode)
		mc.connectAttr ('%s.output1D' % pmaNode , '%s.rotateY' % listsIkSca[4])
		mdvNodeCha = mc.createNode ('multiplyDivide', n= ('%sCha%s_Mdv' % (part , ix+1)))
		mc.setAttr ('%s.input1X' % mdvNodeCha, 0.75)
		mc.setAttr ('%s.input1Y' % mdvNodeCha, 0.5)
		mc.setAttr ('%s.input1Z' % mdvNodeCha, 0.25)
		mc.connectAttr('%s.rotateY' % listsIkSca[4], '%s.input2X' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[4], '%s.input2Y' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[4], '%s.input2Z' % mdvNodeCha)

		# Connect Control 2 Rotate 
		mdvRotNode = mc.createNode ('multiplyDivide', n= ('%s%sRot_Mdv' % (part , ix+1)))
		mc.connectAttr ('%s.rotateZ' % fkCtrlLists[0], '%s.input1X' % mdvRotNode)
		mc.connectAttr ('%s.outputX' % mdvRotNode , '%s.input1D[1]' % pmaNode)
		mc.setAttr ('%s.input2X' % mdvRotNode, -1)

		mc.connectAttr ('%s.rotateY' % fkCtrlLists[1], '%s.input1D[2]' % pmaNode)

		mc.connectAttr ('%s.rotateZ' % 'Root_Ctrl', '%s.input1Y' % mdvRotNode)
		mc.setAttr ('%s.input2Y' % mdvRotNode, -1)
		mc.connectAttr ('%s.outputY' % mdvRotNode , '%s.input1D[3]' % pmaNode)


		attr = ['X', 'Y', 'Z']
		iix = 5
		for attrs in attr:
			pmaNodeCha = mc.createNode ('plusMinusAverage', n = ('%sCha%s_Pma' % (part , iix+1)))
			mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs) , '%s.input1D[0]' % pmaNodeCha)
			mc.connectAttr ('%s.output1D' % pmaNodeCha, 'TailIkRZ%s_Jnt.rotateY' % (iix+1))
			iix+=1
		ix+=1

# Ctrl 2 Bck----------------------------------------
	attr = ['X', 'Y', 'Z']
	iix = 5
	for attrs in attr:
		mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs), '%sCha%s_Pma.input1D[1]' % (part,iix-1))
		iix-=1

# Ctrl 3 Fnt----------------------------------------
	ix = 8
	ikCtrl = mc.ls('TailIk3_Ctrl')
	for each in range(0, 1):
		pmaNode = mc.createNode ('plusMinusAverage', n = ('%s%s_Pma' % (part , ix+1)))
		mc.connectAttr ('%s.rotateY' % ikCtrl[0], '%s.input1D[0]' % pmaNode)
		mc.connectAttr ('%s.output1D' % pmaNode , '%s.rotateY' % listsIkSca[8])
		mdvNodeCha = mc.createNode ('multiplyDivide', n= ('%sCha%s_Mdv' % (part , ix+1)))
		mc.setAttr ('%s.input2X' % mdvNodeCha, 0.75)
		mc.setAttr ('%s.input2Y' % mdvNodeCha, 0.5)
		mc.setAttr ('%s.input2Z' % mdvNodeCha, 0.25)
		mc.connectAttr('%s.rotateY' % listsIkSca[8], '%s.input1X' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[8], '%s.input1Y' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[8], '%s.input1Z' % mdvNodeCha)

		# Connect Control 3 Rotate 
		mdvRotNode = mc.createNode ('multiplyDivide', n= ('%s%sRot_Mdv' % (part , ix+1)))
		mc.connectAttr ('%s.rotateZ' % fkCtrlLists[0], '%s.input1X' % mdvRotNode)
		mc.connectAttr ('%s.outputX' % mdvRotNode , '%s.input1D[1]' % pmaNode)
		mc.setAttr ('%s.input2X' % mdvRotNode, -1)

		mc.connectAttr ('%s.rotateY' % fkCtrlLists[1], '%s.input1D[2]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[2], '%s.input1D[3]' % pmaNode)

		mc.connectAttr ('%s.rotateZ' % 'Root_Ctrl', '%s.input1Y' % mdvRotNode)
		mc.setAttr ('%s.input2Y' % mdvRotNode, -1)
		mc.connectAttr ('%s.outputY' % mdvRotNode , '%s.input1D[4]' % pmaNode)

		attr = ['X', 'Y', 'Z']
		iix = 9
		for attrs in attr:
			pmaNodeCha = mc.createNode ('plusMinusAverage', n = ('%sCha%s_Pma' % (part , iix+1)))
			mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs) , '%s.input1D[0]' % pmaNodeCha)
			mc.connectAttr ('%s.output1D' % pmaNodeCha, 'TailIkRZ%s_Jnt.rotateY' % (iix+1))
			iix+=1
		ix+=1

# Ctrl 3 Bck----------------------------------------
	attr = ['X', 'Y', 'Z']
	iix = 9
	for attrs in attr:
		mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs), '%sCha%s_Pma.input1D[1]' % (part,iix-1))
		iix-=1

#Ctrl 4 Fnt----------------------------------------
	ix = 12
	ikCtrl = mc.ls('TailIk4_Ctrl')
	for each in range(0, 1):
		pmaNode = mc.createNode ('plusMinusAverage', n = ('%s%s_Pma' % (part , ix+1)))
		mc.connectAttr ('%s.rotateY' % ikCtrl[0], '%s.input1D[0]' % pmaNode)
		mc.connectAttr ('%s.output1D' % pmaNode , '%s.rotateY' % listsIkSca[12])
		mdvNodeCha = mc.createNode ('multiplyDivide', n= ('%sCha%s_Mdv' % (part , ix+1)))
		mc.setAttr ('%s.input2X' % mdvNodeCha, 0.75)
		mc.setAttr ('%s.input2Y' % mdvNodeCha, 0.5)
		mc.setAttr ('%s.input2Z' % mdvNodeCha, 0.25)
		mc.connectAttr('%s.rotateY' % listsIkSca[12], '%s.input1X' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[12], '%s.input1Y' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[12], '%s.input1Z' % mdvNodeCha)

		# Connect Control 4 Rotate 
		mdvRotNode = mc.createNode ('multiplyDivide', n= ('%s%sRot_Mdv' % (part , ix+1)))
		mc.connectAttr ('%s.rotateZ' % fkCtrlLists[0], '%s.input1X' % mdvRotNode)
		mc.connectAttr ('%s.outputX' % mdvRotNode , '%s.input1D[1]' % pmaNode)
		mc.setAttr ('%s.input2X' % mdvRotNode, -1)

		mc.connectAttr ('%s.rotateY' % fkCtrlLists[1], '%s.input1D[2]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[2], '%s.input1D[3]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[3], '%s.input1D[4]' % pmaNode)

		mc.connectAttr ('%s.rotateZ' % 'Root_Ctrl', '%s.input1Y' % mdvRotNode)
		mc.setAttr ('%s.input2Y' % mdvRotNode, -1)
		mc.connectAttr ('%s.outputY' % mdvRotNode , '%s.input1D[5]' % pmaNode)

		attr = ['X', 'Y', 'Z']
		iix = 13
		for attrs in attr:
			pmaNodeCha = mc.createNode ('plusMinusAverage', n = ('%sCha%s_Pma' % (part , iix+1)))
			mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs) , '%s.input1D[0]' % pmaNodeCha)
			mc.connectAttr ('%s.output1D' % pmaNodeCha, 'TailIkRZ%s_Jnt.rotateY' % (iix+1))
			iix+=1
		ix+=1
# Ctrl 4 Bck----------------------------------------
	attr = ['X', 'Y', 'Z']
	iix = 13
	for attrs in attr:
		mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs), '%sCha%s_Pma.input1D[1]' % (part,iix-1))
		iix-=1

#Ctrl 5 Fnt----------------------------------------
	ix = 16
	ikCtrl = mc.ls('TailIk5_Ctrl')
	for each in range(0, 1):
		pmaNode = mc.createNode ('plusMinusAverage', n = ('%s%s_Pma' % (part , ix+1)))
		mc.connectAttr ('%s.rotateY' % ikCtrl[0], '%s.input1D[0]' % pmaNode)
		mc.connectAttr ('%s.output1D' % pmaNode , '%s.rotateY' % listsIkSca[16])
		mdvNodeCha = mc.createNode ('multiplyDivide', n= ('%sCha%s_Mdv' % (part , ix+1)))
		mc.setAttr ('%s.input2X' % mdvNodeCha, 0.75)
		mc.setAttr ('%s.input2Y' % mdvNodeCha, 0.5)
		mc.setAttr ('%s.input2Z' % mdvNodeCha, 0.25)
		mc.connectAttr('%s.rotateY' % listsIkSca[16], '%s.input1X' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[16], '%s.input1Y' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[16], '%s.input1Z' % mdvNodeCha)

		# Connect Control 5 Rotate 
		mdvRotNode = mc.createNode ('multiplyDivide', n= ('%s%sRot_Mdv' % (part , ix+1)))
		mc.connectAttr ('%s.rotateZ' % fkCtrlLists[0], '%s.input1X' % mdvRotNode)
		mc.connectAttr ('%s.outputX' % mdvRotNode , '%s.input1D[1]' % pmaNode)
		mc.setAttr ('%s.input2X' % mdvRotNode, -1)

		mc.connectAttr ('%s.rotateY' % fkCtrlLists[1], '%s.input1D[2]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[2], '%s.input1D[3]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[3], '%s.input1D[4]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[4], '%s.input1D[5]' % pmaNode)

		mc.connectAttr ('%s.rotateZ' % 'Root_Ctrl', '%s.input1Y' % mdvRotNode)
		mc.setAttr ('%s.input2Y' % mdvRotNode, -1)
		mc.connectAttr ('%s.outputY' % mdvRotNode , '%s.input1D[6]' % pmaNode)

		attr = ['X', 'Y', 'Z']
		iix = 17
		for attrs in attr:
			pmaNodeCha = mc.createNode ('plusMinusAverage', n = ('%sCha%s_Pma' % (part , iix+1)))
			mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs) , '%s.input1D[0]' % pmaNodeCha)
			mc.connectAttr ('%s.output1D' % pmaNodeCha, 'TailIkRZ%s_Jnt.rotateY' % (iix+1))
			iix+=1
		ix+=1
# Ctrl 5 Bck----------------------------------------
	attr = ['X', 'Y', 'Z']
	iix = 17
	for attrs in attr:
		mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs), '%sCha%s_Pma.input1D[1]' % (part,iix-1))
		iix-=1
#Ctrl 6 Fnt----------------------------------------
	ix = 20
	ikCtrl = mc.ls('TailIk6_Ctrl')
	for each in range(0, 1):
		pmaNode = mc.createNode ('plusMinusAverage', n = ('%s%s_Pma' % (part , ix+1)))
		mc.connectAttr ('%s.rotateY' % ikCtrl[0], '%s.input1D[0]' % pmaNode)
		mc.connectAttr ('%s.output1D' % pmaNode , '%s.rotateY' % listsIkSca[20])
		mdvNodeCha = mc.createNode ('multiplyDivide', n= ('%sCha%s_Mdv' % (part , ix+1)))
		mc.setAttr ('%s.input2X' % mdvNodeCha, 0.75)
		mc.setAttr ('%s.input2Y' % mdvNodeCha, 0.5)
		mc.setAttr ('%s.input2Z' % mdvNodeCha, 0.25)
		mc.connectAttr('%s.rotateY' % listsIkSca[20], '%s.input1X' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[20], '%s.input1Y' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[20], '%s.input1Z' % mdvNodeCha)

		# Connect Control 6 Rotate 
		mdvRotNode = mc.createNode ('multiplyDivide', n= ('%s%sRot_Mdv' % (part , ix+1)))
		mc.connectAttr ('%s.rotateZ' % fkCtrlLists[0], '%s.input1X' % mdvRotNode)
		mc.connectAttr ('%s.outputX' % mdvRotNode , '%s.input1D[1]' % pmaNode)
		mc.setAttr ('%s.input2X' % mdvRotNode, -1)

		mc.connectAttr ('%s.rotateY' % fkCtrlLists[1], '%s.input1D[2]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[2], '%s.input1D[3]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[3], '%s.input1D[4]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[4], '%s.input1D[5]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[5], '%s.input1D[6]' % pmaNode)

		mc.connectAttr ('%s.rotateZ' % 'Root_Ctrl', '%s.input1Y' % mdvRotNode)
		mc.setAttr ('%s.input2Y' % mdvRotNode, -1)
		mc.connectAttr ('%s.outputY' % mdvRotNode , '%s.input1D[7]' % pmaNode)

		attr = ['X', 'Y', 'Z']
		iix = 21
		for attrs in attr:
			pmaNodeCha = mc.createNode ('plusMinusAverage', n = ('%sCha%s_Pma' % (part , iix+1)))
			mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs) , '%s.input1D[0]' % pmaNodeCha)
			mc.connectAttr ('%s.output1D' % pmaNodeCha, 'TailIkRZ%s_Jnt.rotateY' % (iix+1))
			iix+=1
		ix+=1
# Ctrl 6 Bck----------------------------------------
	attr = ['X', 'Y', 'Z']
	iix = 21
	for attrs in attr:
		mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs), '%sCha%s_Pma.input1D[1]' % (part,iix-1))
		iix-=1
#Ctrl 7 Fnt----------------------------------------
	ix = 24
	ikCtrl = mc.ls('TailIk7_Ctrl')
	for each in range(0, 1):
		pmaNode = mc.createNode ('plusMinusAverage', n = ('%s%s_Pma' % (part , ix+1)))
		mc.connectAttr ('%s.rotateY' % ikCtrl[0], '%s.input1D[0]' % pmaNode)
		mc.connectAttr ('%s.output1D' % pmaNode , '%s.rotateY' % listsIkSca[24])
		mdvNodeCha = mc.createNode ('multiplyDivide', n= ('%sCha%s_Mdv' % (part , ix+1)))
		mc.setAttr ('%s.input2X' % mdvNodeCha, 0.75)
		mc.setAttr ('%s.input2Y' % mdvNodeCha, 0.5)
		mc.setAttr ('%s.input2Z' % mdvNodeCha, 0.24)
		mc.connectAttr('%s.rotateY' % listsIkSca[24], '%s.input1X' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[24], '%s.input1Y' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[24], '%s.input1Z' % mdvNodeCha)

		# Connect Control 7 Rotate 
		mdvRotNode = mc.createNode ('multiplyDivide', n= ('%s%sRot_Mdv' % (part , ix+1)))
		mc.connectAttr ('%s.rotateZ' % fkCtrlLists[0], '%s.input1X' % mdvRotNode)
		mc.connectAttr ('%s.outputX' % mdvRotNode , '%s.input1D[1]' % pmaNode)
		mc.setAttr ('%s.input2X' % mdvRotNode, -1)

		mc.connectAttr ('%s.rotateY' % fkCtrlLists[1], '%s.input1D[2]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[2], '%s.input1D[3]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[3], '%s.input1D[4]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[4], '%s.input1D[5]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[5], '%s.input1D[6]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[6], '%s.input1D[7]' % pmaNode)

		mc.connectAttr ('%s.rotateZ' % 'Root_Ctrl', '%s.input1Y' % mdvRotNode)
		mc.setAttr ('%s.input2Y' % mdvRotNode, -1)
		mc.connectAttr ('%s.outputY' % mdvRotNode , '%s.input1D[8]' % pmaNode)

		attr = ['X', 'Y', 'Z']
		iix = 25
		for attrs in attr:
			pmaNodeCha = mc.createNode ('plusMinusAverage', n = ('%sCha%s_Pma' % (part , iix+1)))
			mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs) , '%s.input1D[0]' % pmaNodeCha)
			mc.connectAttr ('%s.output1D' % pmaNodeCha, 'TailIkRZ%s_Jnt.rotateY' % (iix+1))
			iix+=1
		ix+=1
# Ctrl 7 Bck----------------------------------------
	attr = ['X', 'Y', 'Z']
	iix = 25
	for attrs in attr:
		mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs), '%sCha%s_Pma.input1D[1]' % (part,iix-1))
		iix-=1

#Ctrl 8 Fnt----------------------------------------
	ix = 28
	ikCtrl = mc.ls('TailIk8_Ctrl')
	for each in range(0, 1):
		pmaNode = mc.createNode ('plusMinusAverage', n = ('%s%s_Pma' % (part , ix+1)))
		mc.connectAttr ('%s.rotateY' % ikCtrl[0], '%s.input1D[0]' % pmaNode)
		mc.connectAttr ('%s.output1D' % pmaNode , '%s.rotateY' % listsIkSca[28])
		mdvNodeCha = mc.createNode ('multiplyDivide', n= ('%sCha%s_Mdv' % (part , ix+1)))
		mc.setAttr ('%s.input2X' % mdvNodeCha, 0.75)
		mc.setAttr ('%s.input2Y' % mdvNodeCha, 0.5)
		mc.setAttr ('%s.input2Z' % mdvNodeCha, 0.25)
		mc.connectAttr('%s.rotateY' % listsIkSca[28], '%s.input1X' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[28], '%s.input1Y' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[28], '%s.input1Z' % mdvNodeCha)

		# Connect Control 8 Rotate 
		mdvRotNode = mc.createNode ('multiplyDivide', n= ('%s%sRot_Mdv' % (part , ix+1)))
		mc.connectAttr ('%s.rotateZ' % fkCtrlLists[0], '%s.input1X' % mdvRotNode)
		mc.connectAttr ('%s.outputX' % mdvRotNode , '%s.input1D[1]' % pmaNode)
		mc.setAttr ('%s.input2X' % mdvRotNode, -1)

		mc.connectAttr ('%s.rotateY' % fkCtrlLists[1], '%s.input1D[2]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[2], '%s.input1D[3]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[3], '%s.input1D[4]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[4], '%s.input1D[5]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[5], '%s.input1D[6]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[6], '%s.input1D[7]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[7], '%s.input1D[8]' % pmaNode)

		mc.connectAttr ('%s.rotateZ' % 'Root_Ctrl', '%s.input1Y' % mdvRotNode)
		mc.setAttr ('%s.input2Y' % mdvRotNode, -1)
		mc.connectAttr ('%s.outputY' % mdvRotNode , '%s.input1D[9]' % pmaNode)

		attr = ['X', 'Y', 'Z']
		iix = 29
		for attrs in attr:
			pmaNodeCha = mc.createNode ('plusMinusAverage', n = ('%sCha%s_Pma' % (part , iix+1)))
			mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs) , '%s.input1D[0]' % pmaNodeCha)
			mc.connectAttr ('%s.output1D' % pmaNodeCha, 'TailIkRZ%s_Jnt.rotateY' % (iix+1))
			iix+=1
		ix+=1
# Ctrl 8 Bck----------------------------------------
	attr = ['X', 'Y', 'Z']
	iix = 29
	for attrs in attr:
		mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs), '%sCha%s_Pma.input1D[1]' % (part,iix-1))
		iix-=1
#Ctrl 9 Fnt----------------------------------------
	ix = 32
	ikCtrl = mc.ls('TailIk9_Ctrl')
	for each in range(0, 1):
		pmaNode = mc.createNode ('plusMinusAverage', n = ('%s%s_Pma' % (part , ix+1)))
		mc.connectAttr ('%s.rotateY' % ikCtrl[0], '%s.input1D[0]' % pmaNode)
		mc.connectAttr ('%s.output1D' % pmaNode , '%s.rotateY' % listsIkSca[32])
		mdvNodeCha = mc.createNode ('multiplyDivide', n= ('%sCha%s_Mdv' % (part , ix+1)))
		mc.setAttr ('%s.input2X' % mdvNodeCha, 0.75)
		mc.setAttr ('%s.input2Y' % mdvNodeCha, 0.5)
		mc.setAttr ('%s.input2Z' % mdvNodeCha, 0.25)
		mc.connectAttr('%s.rotateY' % listsIkSca[32], '%s.input1X' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[32], '%s.input1Y' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[32], '%s.input1Z' % mdvNodeCha)

		# Connect Control 9 Rotate 
		mdvRotNode = mc.createNode ('multiplyDivide', n= ('%s%sRot_Mdv' % (part , ix+1)))
		mc.connectAttr ('%s.rotateZ' % fkCtrlLists[0], '%s.input1X' % mdvRotNode)
		mc.connectAttr ('%s.outputX' % mdvRotNode , '%s.input1D[1]' % pmaNode)
		mc.setAttr ('%s.input2X' % mdvRotNode, -1)

		mc.connectAttr ('%s.rotateY' % fkCtrlLists[1], '%s.input1D[2]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[2], '%s.input1D[3]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[3], '%s.input1D[4]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[4], '%s.input1D[5]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[5], '%s.input1D[6]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[6], '%s.input1D[7]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[7], '%s.input1D[8]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[8], '%s.input1D[9]' % pmaNode)

		mc.connectAttr ('%s.rotateZ' % 'Root_Ctrl', '%s.input1Y' % mdvRotNode)
		mc.setAttr ('%s.input2Y' % mdvRotNode, -1)
		mc.connectAttr ('%s.outputY' % mdvRotNode , '%s.input1D[10]' % pmaNode)

		attr = ['X', 'Y', 'Z']
		iix = 33
		for attrs in attr:
			pmaNodeCha = mc.createNode ('plusMinusAverage', n = ('%sCha%s_Pma' % (part , iix+1)))
			mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs) , '%s.input1D[0]' % pmaNodeCha)
			mc.connectAttr ('%s.output1D' % pmaNodeCha, 'TailIkRZ%s_Jnt.rotateY' % (iix+1))
			iix+=1
		ix+=1
# Ctrl 9 Bck----------------------------------------
	attr = ['X', 'Y', 'Z']
	iix = 33
	for attrs in attr:
		mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs), '%sCha%s_Pma.input1D[1]' % (part,iix-1))
		iix-=1
#Ctrl 10 Fnt----------------------------------------
	ix = 36
	ikCtrl = mc.ls('TailIk10_Ctrl')
	for each in range(0, 1):
		pmaNode = mc.createNode ('plusMinusAverage', n = ('%s%s_Pma' % (part , ix+1)))
		mc.connectAttr ('%s.rotateY' % ikCtrl[0], '%s.input1D[0]' % pmaNode)
		mc.connectAttr ('%s.output1D' % pmaNode , '%s.rotateY' % listsIkSca[36])
		mdvNodeCha = mc.createNode ('multiplyDivide', n= ('%sCha%s_Mdv' % (part , ix+1)))
		mc.setAttr ('%s.input2X' % mdvNodeCha, 0.75)
		mc.setAttr ('%s.input2Y' % mdvNodeCha, 0.5)
		mc.setAttr ('%s.input2Z' % mdvNodeCha, 0.25)
		mc.connectAttr('%s.rotateY' % listsIkSca[36], '%s.input1X' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[36], '%s.input1Y' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[36], '%s.input1Z' % mdvNodeCha)

		# Connect Control 10 Rotate 
		mdvRotNode = mc.createNode ('multiplyDivide', n= ('%s%sRot_Mdv' % (part , ix+1)))
		mc.connectAttr ('%s.rotateZ' % fkCtrlLists[0], '%s.input1X' % mdvRotNode)
		mc.connectAttr ('%s.outputX' % mdvRotNode , '%s.input1D[1]' % pmaNode)
		mc.setAttr ('%s.input2X' % mdvRotNode, -1)

		mc.connectAttr ('%s.rotateY' % fkCtrlLists[1], '%s.input1D[2]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[2], '%s.input1D[3]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[3], '%s.input1D[4]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[4], '%s.input1D[5]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[5], '%s.input1D[6]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[6], '%s.input1D[7]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[7], '%s.input1D[8]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[8], '%s.input1D[9]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[9], '%s.input1D[10]' % pmaNode)

		mc.connectAttr ('%s.rotateZ' % 'Root_Ctrl', '%s.input1Y' % mdvRotNode)
		mc.setAttr ('%s.input2Y' % mdvRotNode, -1)
		mc.connectAttr ('%s.outputY' % mdvRotNode , '%s.input1D[11]' % pmaNode)

		attr = ['X', 'Y', 'Z']
		iix = 37
		for attrs in attr:
			pmaNodeCha = mc.createNode ('plusMinusAverage', n = ('%sCha%s_Pma' % (part , iix+1)))
			mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs) , '%s.input1D[0]' % pmaNodeCha)
			mc.connectAttr ('%s.output1D' % pmaNodeCha, 'TailIkRZ%s_Jnt.rotateY' % (iix+1))
			iix+=1
		ix+=1
# Ctrl 11 Bck----------------------------------------
	attr = ['X', 'Y', 'Z']
	iix = 37
	for attrs in attr:
		mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs), '%sCha%s_Pma.input1D[1]' % (part,iix-1))
		iix-=1

#Ctrl 11 Fnt----------------------------------------
	ix = 40
	ikCtrl = mc.ls('TailIk11_Ctrl')
	for each in range(0, 1):
		pmaNode = mc.createNode ('plusMinusAverage', n = ('%s%s_Pma' % (part , ix+1)))
		mc.connectAttr ('%s.rotateY' % ikCtrl[0], '%s.input1D[0]' % pmaNode)
		mc.connectAttr ('%s.output1D' % pmaNode , '%s.rotateY' % listsIkSca[40])
		mdvNodeCha = mc.createNode ('multiplyDivide', n= ('%sCha%s_Mdv' % (part , ix+1)))
		mc.setAttr ('%s.input2X' % mdvNodeCha, 0.75)
		mc.setAttr ('%s.input2Y' % mdvNodeCha, 0.5)
		mc.setAttr ('%s.input2Z' % mdvNodeCha, 0.25)
		mc.connectAttr('%s.rotateY' % listsIkSca[40], '%s.input1X' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[40], '%s.input1Y' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[40], '%s.input1Z' % mdvNodeCha)

		# Connect Control 11 Rotate 
		mdvRotNode = mc.createNode ('multiplyDivide', n= ('%s%sRot_Mdv' % (part , ix+1)))
		mc.connectAttr ('%s.rotateZ' % fkCtrlLists[0], '%s.input1X' % mdvRotNode)
		mc.connectAttr ('%s.outputX' % mdvRotNode , '%s.input1D[1]' % pmaNode)
		mc.setAttr ('%s.input2X' % mdvRotNode, -1)

		mc.connectAttr ('%s.rotateY' % fkCtrlLists[1], '%s.input1D[2]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[2], '%s.input1D[3]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[3], '%s.input1D[4]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[4], '%s.input1D[5]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[5], '%s.input1D[6]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[6], '%s.input1D[7]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[7], '%s.input1D[8]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[8], '%s.input1D[9]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[9], '%s.input1D[10]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[10], '%s.input1D[11]' % pmaNode)

		mc.connectAttr ('%s.rotateZ' % 'Root_Ctrl', '%s.input1Y' % mdvRotNode)
		mc.setAttr ('%s.input2Y' % mdvRotNode, -1)
		mc.connectAttr ('%s.outputY' % mdvRotNode , '%s.input1D[12]' % pmaNode)

		attr = ['X', 'Y', 'Z']
		iix = 41
		for attrs in attr:
			pmaNodeCha = mc.createNode ('plusMinusAverage', n = ('%sCha%s_Pma' % (part , iix+1)))
			mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs) , '%s.input1D[0]' % pmaNodeCha)
			mc.connectAttr ('%s.output1D' % pmaNodeCha, 'TailIkRZ%s_Jnt.rotateY' % (iix+1))
			iix+=1
		ix+=1
# Ctrl 11 Bck----------------------------------------
	attr = ['X', 'Y', 'Z']
	iix = 41
	for attrs in attr:
		mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs), '%sCha%s_Pma.input1D[1]' % (part,iix-1))
		iix-=1
#Ctrl 12 Fnt----------------------------------------
	ix = 44
	ikCtrl = mc.ls('TailIk12_Ctrl')
	for each in range(0, 1):
		pmaNode = mc.createNode ('plusMinusAverage', n = ('%s%s_Pma' % (part , ix+1)))
		mc.connectAttr ('%s.rotateY' % ikCtrl[0], '%s.input1D[0]' % pmaNode)
		mc.connectAttr ('%s.output1D' % pmaNode , '%s.rotateY' % listsIkSca[44])
		mdvNodeCha = mc.createNode ('multiplyDivide', n= ('%sCha%s_Mdv' % (part , ix+1)))
		mc.setAttr ('%s.input2X' % mdvNodeCha, 0.75)
		mc.setAttr ('%s.input2Y' % mdvNodeCha, 0.5)
		mc.setAttr ('%s.input2Z' % mdvNodeCha, 0.25)
		mc.connectAttr('%s.rotateY' % listsIkSca[44], '%s.input1X' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[44], '%s.input1Y' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[44], '%s.input1Z' % mdvNodeCha)

		# Connect Control 12 Rotate 
		mdvRotNode = mc.createNode ('multiplyDivide', n= ('%s%sRot_Mdv' % (part , ix+1)))
		mc.connectAttr ('%s.rotateZ' % fkCtrlLists[0], '%s.input1X' % mdvRotNode)
		mc.connectAttr ('%s.outputX' % mdvRotNode , '%s.input1D[1]' % pmaNode)
		mc.setAttr ('%s.input2X' % mdvRotNode, -1)

		mc.connectAttr ('%s.rotateY' % fkCtrlLists[1], '%s.input1D[2]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[2], '%s.input1D[3]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[3], '%s.input1D[4]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[4], '%s.input1D[5]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[5], '%s.input1D[6]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[6], '%s.input1D[7]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[7], '%s.input1D[8]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[8], '%s.input1D[9]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[9], '%s.input1D[10]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[10], '%s.input1D[11]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[11], '%s.input1D[12]' % pmaNode)

		mc.connectAttr ('%s.rotateZ' % 'Root_Ctrl', '%s.input1Y' % mdvRotNode)
		mc.setAttr ('%s.input2Y' % mdvRotNode, -1)
		mc.connectAttr ('%s.outputY' % mdvRotNode , '%s.input1D[13]' % pmaNode)

		attr = ['X', 'Y', 'Z']
		iix = 45
		for attrs in attr:
			pmaNodeCha = mc.createNode ('plusMinusAverage', n = ('%sCha%s_Pma' % (part , iix+1)))
			mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs) , '%s.input1D[0]' % pmaNodeCha)
			mc.connectAttr ('%s.output1D' % pmaNodeCha, 'TailIkRZ%s_Jnt.rotateY' % (iix+1))
			iix+=1
		ix+=1
# Ctrl 12 Bck----------------------------------------
	attr = ['X', 'Y', 'Z']
	iix = 45
	for attrs in attr:
		mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs), '%sCha%s_Pma.input1D[1]' % (part,iix-1))
		iix-=1
#Ctrl 13 Fnt----------------------------------------
	ix = 48
	ikCtrl = mc.ls('TailIk13_Ctrl')
	for each in range(0, 1):
		pmaNode = mc.createNode ('plusMinusAverage', n = ('%s%s_Pma' % (part , ix+1)))
		mc.connectAttr ('%s.rotateY' % ikCtrl[0], '%s.input1D[0]' % pmaNode)
		mc.connectAttr ('%s.output1D' % pmaNode , '%s.rotateY' % listsIkSca[48])
		mdvNodeCha = mc.createNode ('multiplyDivide', n= ('%sCha%s_Mdv' % (part , ix+1)))
		mc.setAttr ('%s.input2X' % mdvNodeCha, 0.75)
		mc.setAttr ('%s.input2Y' % mdvNodeCha, 0.5)
		mc.setAttr ('%s.input2Z' % mdvNodeCha, 0.25)
		mc.connectAttr('%s.rotateY' % listsIkSca[48], '%s.input1X' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[48], '%s.input1Y' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[48], '%s.input1Z' % mdvNodeCha)

		# Connect Control 13 Rotate 
		mdvRotNode = mc.createNode ('multiplyDivide', n= ('%s%sRot_Mdv' % (part , ix+1)))
		mc.connectAttr ('%s.rotateZ' % fkCtrlLists[0], '%s.input1X' % mdvRotNode)
		mc.connectAttr ('%s.outputX' % mdvRotNode , '%s.input1D[1]' % pmaNode)
		mc.setAttr ('%s.input2X' % mdvRotNode, -1)

		mc.connectAttr ('%s.rotateY' % fkCtrlLists[1], '%s.input1D[2]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[2], '%s.input1D[3]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[3], '%s.input1D[4]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[4], '%s.input1D[5]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[5], '%s.input1D[6]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[6], '%s.input1D[7]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[7], '%s.input1D[8]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[8], '%s.input1D[9]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[9], '%s.input1D[10]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[10], '%s.input1D[11]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[11], '%s.input1D[12]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[12], '%s.input1D[13]' % pmaNode)

		mc.connectAttr ('%s.rotateZ' % 'Root_Ctrl', '%s.input1Y' % mdvRotNode)
		mc.setAttr ('%s.input2Y' % mdvRotNode, -1)
		mc.connectAttr ('%s.outputY' % mdvRotNode , '%s.input1D[14]' % pmaNode)

		attr = ['X', 'Y', 'Z']
		iix = 49
		for attrs in attr:
			pmaNodeCha = mc.createNode ('plusMinusAverage', n = ('%sCha%s_Pma' % (part , iix+1)))
			mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs) , '%s.input1D[0]' % pmaNodeCha)
			mc.connectAttr ('%s.output1D' % pmaNodeCha, 'TailIkRZ%s_Jnt.rotateY' % (iix+1))
			iix+=1
		ix+=1
# Ctrl 13 Bck----------------------------------------
	attr = ['X', 'Y', 'Z']
	iix = 49
	for attrs in attr:
		mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs), '%sCha%s_Pma.input1D[1]' % (part,iix-1))
		iix-=1
#Ctrl 14 Fnt----------------------------------------
	ix = 52
	ikCtrl = mc.ls('TailIk14_Ctrl')
	for each in range(0, 1):
		pmaNode = mc.createNode ('plusMinusAverage', n = ('%s%s_Pma' % (part , ix+1)))
		mc.connectAttr ('%s.rotateY' % ikCtrl[0], '%s.input1D[0]' % pmaNode)
		mc.connectAttr ('%s.output1D' % pmaNode , '%s.rotateY' % listsIkSca[52])
		mdvNodeCha = mc.createNode ('multiplyDivide', n= ('%sCha%s_Mdv' % (part , ix+1)))
		mc.setAttr ('%s.input2X' % mdvNodeCha, 0.75)
		mc.setAttr ('%s.input2Y' % mdvNodeCha, 0.5)
		mc.setAttr ('%s.input2Z' % mdvNodeCha, 0.25)
		mc.connectAttr('%s.rotateY' % listsIkSca[52], '%s.input1X' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[52], '%s.input1Y' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[52], '%s.input1Z' % mdvNodeCha)

		# Connect Control 14 Rotate 
		mdvRotNode = mc.createNode ('multiplyDivide', n= ('%s%sRot_Mdv' % (part , ix+1)))
		mc.connectAttr ('%s.rotateZ' % fkCtrlLists[0], '%s.input1X' % mdvRotNode)
		mc.connectAttr ('%s.outputX' % mdvRotNode , '%s.input1D[1]' % pmaNode)
		mc.setAttr ('%s.input2X' % mdvRotNode, -1)

		mc.connectAttr ('%s.rotateY' % fkCtrlLists[1], '%s.input1D[2]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[2], '%s.input1D[3]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[3], '%s.input1D[4]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[4], '%s.input1D[5]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[5], '%s.input1D[6]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[6], '%s.input1D[7]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[7], '%s.input1D[8]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[8], '%s.input1D[9]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[9], '%s.input1D[10]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[10], '%s.input1D[11]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[11], '%s.input1D[12]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[12], '%s.input1D[13]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[13], '%s.input1D[14]' % pmaNode)

		mc.connectAttr ('%s.rotateZ' % 'Root_Ctrl', '%s.input1Y' % mdvRotNode)
		mc.setAttr ('%s.input2Y' % mdvRotNode, -1)
		mc.connectAttr ('%s.outputY' % mdvRotNode , '%s.input1D[15]' % pmaNode)

		attr = ['X', 'Y', 'Z']
		iix = 53
		for attrs in attr:
			pmaNodeCha = mc.createNode ('plusMinusAverage', n = ('%sCha%s_Pma' % (part , iix+1)))
			mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs) , '%s.input1D[0]' % pmaNodeCha)
			mc.connectAttr ('%s.output1D' % pmaNodeCha, 'TailIkRZ%s_Jnt.rotateY' % (iix+1))
			iix+=1
		ix+=1
# Ctrl 14 Bck----------------------------------------
	attr = ['X', 'Y', 'Z']
	iix = 53
	for attrs in attr:
		mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs), '%sCha%s_Pma.input1D[1]' % (part,iix-1))
		iix-=1
#Ctrl 15 Fnt----------------------------------------
	ix = 56
	ikCtrl = mc.ls('TailIk15_Ctrl')
	for each in range(0, 1):
		pmaNode = mc.createNode ('plusMinusAverage', n = ('%s%s_Pma' % (part , ix+1)))
		mc.connectAttr ('%s.rotateY' % ikCtrl[0], '%s.input1D[0]' % pmaNode)
		mc.connectAttr ('%s.output1D' % pmaNode , '%s.rotateY' % listsIkSca[56])
		mdvNodeCha = mc.createNode ('multiplyDivide', n= ('%sCha%s_Mdv' % (part , ix+1)))
		mc.setAttr ('%s.input2X' % mdvNodeCha, 0.75)
		mc.setAttr ('%s.input2Y' % mdvNodeCha, 0.5)
		mc.setAttr ('%s.input2Z' % mdvNodeCha, 0.25)
		mc.connectAttr('%s.rotateY' % listsIkSca[56], '%s.input1X' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[56], '%s.input1Y' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[56], '%s.input1Z' % mdvNodeCha)

		# Connect Control 15 Rotate 
		mdvRotNode = mc.createNode ('multiplyDivide', n= ('%s%sRot_Mdv' % (part , ix+1)))
		mc.connectAttr ('%s.rotateZ' % fkCtrlLists[0], '%s.input1X' % mdvRotNode)
		mc.connectAttr ('%s.outputX' % mdvRotNode , '%s.input1D[1]' % pmaNode)
		mc.setAttr ('%s.input2X' % mdvRotNode, -1)

		mc.connectAttr ('%s.rotateY' % fkCtrlLists[1], '%s.input1D[2]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[2], '%s.input1D[3]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[3], '%s.input1D[4]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[4], '%s.input1D[5]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[5], '%s.input1D[6]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[6], '%s.input1D[7]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[7], '%s.input1D[8]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[8], '%s.input1D[9]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[9], '%s.input1D[10]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[10], '%s.input1D[11]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[11], '%s.input1D[12]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[12], '%s.input1D[13]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[13], '%s.input1D[14]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[14], '%s.input1D[15]' % pmaNode)

		mc.connectAttr ('%s.rotateZ' % 'Root_Ctrl', '%s.input1Y' % mdvRotNode)
		mc.setAttr ('%s.input2Y' % mdvRotNode, -1)
		mc.connectAttr ('%s.outputY' % mdvRotNode , '%s.input1D[16]' % pmaNode)

		attr = ['X', 'Y', 'Z']
		iix = 57
		for attrs in attr:
			pmaNodeCha = mc.createNode ('plusMinusAverage', n = ('%sCha%s_Pma' % (part , iix+1)))
			mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs) , '%s.input1D[0]' % pmaNodeCha)
			mc.connectAttr ('%s.output1D' % pmaNodeCha, 'TailIkRZ%s_Jnt.rotateY' % (iix+1))
			iix+=1
		ix+=1
# Ctrl 15 Bck----------------------------------------
	attr = ['X', 'Y', 'Z']
	iix = 57
	for attrs in attr:
		mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs), '%sCha%s_Pma.input1D[1]' % (part,iix-1))
		iix-=1
#Ctrl 16 Fnt----------------------------------------
	ix = 60
	ikCtrl = mc.ls('TailIk16_Ctrl')
	for each in range(0, 1):
		pmaNode = mc.createNode ('plusMinusAverage', n = ('%s%s_Pma' % (part , ix+1)))
		mc.connectAttr ('%s.rotateY' % ikCtrl[0], '%s.input1D[0]' % pmaNode)
		mc.connectAttr ('%s.output1D' % pmaNode , '%s.rotateY' % listsIkSca[60])
		mdvNodeCha = mc.createNode ('multiplyDivide', n= ('%sCha%s_Mdv' % (part , ix+1)))
		mc.setAttr ('%s.input2X' % mdvNodeCha, 0.75)
		mc.setAttr ('%s.input2Y' % mdvNodeCha, 0.5)
		mc.setAttr ('%s.input2Z' % mdvNodeCha, 0.25)
		mc.connectAttr('%s.rotateY' % listsIkSca[60], '%s.input1X' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[60], '%s.input1Y' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[60], '%s.input1Z' % mdvNodeCha)

		# Connect Control 16 Rotate 
		mdvRotNode = mc.createNode ('multiplyDivide', n= ('%s%sRot_Mdv' % (part , ix+1)))
		mc.connectAttr ('%s.rotateZ' % fkCtrlLists[0], '%s.input1X' % mdvRotNode)
		mc.connectAttr ('%s.outputX' % mdvRotNode , '%s.input1D[1]' % pmaNode)
		mc.setAttr ('%s.input2X' % mdvRotNode, -1)

		mc.connectAttr ('%s.rotateY' % fkCtrlLists[1], '%s.input1D[2]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[2], '%s.input1D[3]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[3], '%s.input1D[4]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[4], '%s.input1D[5]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[5], '%s.input1D[6]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[6], '%s.input1D[7]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[7], '%s.input1D[8]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[8], '%s.input1D[9]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[9], '%s.input1D[10]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[10], '%s.input1D[11]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[11], '%s.input1D[12]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[12], '%s.input1D[13]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[13], '%s.input1D[14]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[14], '%s.input1D[15]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[15], '%s.input1D[16]' % pmaNode)

		mc.connectAttr ('%s.rotateZ' % 'Root_Ctrl', '%s.input1Y' % mdvRotNode)
		mc.setAttr ('%s.input2Y' % mdvRotNode, -1)
		mc.connectAttr ('%s.outputY' % mdvRotNode , '%s.input1D[17]' % pmaNode)

		attr = ['X', 'Y', 'Z']
		iix = 61
		for attrs in attr:
			pmaNodeCha = mc.createNode ('plusMinusAverage', n = ('%sCha%s_Pma' % (part , iix+1)))
			mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs) , '%s.input1D[0]' % pmaNodeCha)
			mc.connectAttr ('%s.output1D' % pmaNodeCha, 'TailIkRZ%s_Jnt.rotateY' % (iix+1))
			iix+=1
		ix+=1
# Ctrl 16 Bck----------------------------------------
	attr = ['X', 'Y', 'Z']
	iix = 61
	for attrs in attr:
		mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs), '%sCha%s_Pma.input1D[1]' % (part,iix-1))
		iix-=1
#Ctrl 17 Fnt----------------------------------------
	ix = 64
	ikCtrl = mc.ls('TailIk17_Ctrl')
	for each in range(0, 1):
		pmaNode = mc.createNode ('plusMinusAverage', n = ('%s%s_Pma' % (part , ix+1)))
		mc.connectAttr ('%s.rotateY' % ikCtrl[0], '%s.input1D[0]' % pmaNode)
		mc.connectAttr ('%s.output1D' % pmaNode , '%s.rotateY' % listsIkSca[64])
		mdvNodeCha = mc.createNode ('multiplyDivide', n= ('%sCha%s_Mdv' % (part , ix+1)))
		mc.setAttr ('%s.input2X' % mdvNodeCha, 0.75)
		mc.setAttr ('%s.input2Y' % mdvNodeCha, 0.5)
		mc.setAttr ('%s.input2Z' % mdvNodeCha, 0.25)
		mc.connectAttr('%s.rotateY' % listsIkSca[64], '%s.input1X' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[64], '%s.input1Y' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[64], '%s.input1Z' % mdvNodeCha)

		# Connect Control 17 Rotate 
		mdvRotNode = mc.createNode ('multiplyDivide', n= ('%s%sRot_Mdv' % (part , ix+1)))
		mc.connectAttr ('%s.rotateZ' % fkCtrlLists[0], '%s.input1X' % mdvRotNode)
		mc.connectAttr ('%s.outputX' % mdvRotNode , '%s.input1D[1]' % pmaNode)
		mc.setAttr ('%s.input2X' % mdvRotNode, -1)

		mc.connectAttr ('%s.rotateY' % fkCtrlLists[1], '%s.input1D[2]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[2], '%s.input1D[3]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[3], '%s.input1D[4]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[4], '%s.input1D[5]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[5], '%s.input1D[6]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[6], '%s.input1D[7]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[7], '%s.input1D[8]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[8], '%s.input1D[9]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[9], '%s.input1D[10]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[10], '%s.input1D[11]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[11], '%s.input1D[12]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[12], '%s.input1D[13]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[13], '%s.input1D[14]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[14], '%s.input1D[15]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[15], '%s.input1D[16]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[16], '%s.input1D[17]' % pmaNode)

		mc.connectAttr ('%s.rotateZ' % 'Root_Ctrl', '%s.input1Y' % mdvRotNode)
		mc.setAttr ('%s.input2Y' % mdvRotNode, -1)
		mc.connectAttr ('%s.outputY' % mdvRotNode , '%s.input1D[18]' % pmaNode)

		attr = ['X', 'Y', 'Z']
		iix = 65
		for attrs in attr:
			pmaNodeCha = mc.createNode ('plusMinusAverage', n = ('%sCha%s_Pma' % (part , iix+1)))
			mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs) , '%s.input1D[0]' % pmaNodeCha)
			mc.connectAttr ('%s.output1D' % pmaNodeCha, 'TailIkRZ%s_Jnt.rotateY' % (iix+1))
			iix+=1
		ix+=1
# Ctrl 17 Bck----------------------------------------
	attr = ['X', 'Y', 'Z']
	iix = 65
	for attrs in attr:
		mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs), '%sCha%s_Pma.input1D[1]' % (part,iix-1))
		iix-=1

#Ctrl 18 Fnt----------------------------------------
	ix = 68
	ikCtrl = mc.ls('TailIk18_Ctrl')
	for each in range(0, 1):
		pmaNode = mc.createNode ('plusMinusAverage', n = ('%s%s_Pma' % (part , ix+1)))
		mc.connectAttr ('%s.rotateY' % ikCtrl[0], '%s.input1D[0]' % pmaNode)
		mc.connectAttr ('%s.output1D' % pmaNode , '%s.rotateY' % listsIkSca[68])
		mdvNodeCha = mc.createNode ('multiplyDivide', n= ('%sCha%s_Mdv' % (part , ix+1)))
		mc.setAttr ('%s.input2X' % mdvNodeCha, 0.75)
		mc.setAttr ('%s.input2Y' % mdvNodeCha, 0.5)
		mc.setAttr ('%s.input2Z' % mdvNodeCha, 0.25)
		mc.connectAttr('%s.rotateY' % listsIkSca[68], '%s.input1X' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[68], '%s.input1Y' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[68], '%s.input1Z' % mdvNodeCha)

		# Connect Control 18 Rotate 
		mdvRotNode = mc.createNode ('multiplyDivide', n= ('%s%sRot_Mdv' % (part , ix+1)))
		mc.connectAttr ('%s.rotateZ' % fkCtrlLists[0], '%s.input1X' % mdvRotNode)
		mc.connectAttr ('%s.outputX' % mdvRotNode , '%s.input1D[1]' % pmaNode)
		mc.setAttr ('%s.input2X' % mdvRotNode, -1)

		mc.connectAttr ('%s.rotateY' % fkCtrlLists[1], '%s.input1D[2]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[2], '%s.input1D[3]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[3], '%s.input1D[4]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[4], '%s.input1D[5]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[5], '%s.input1D[6]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[6], '%s.input1D[7]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[7], '%s.input1D[8]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[8], '%s.input1D[9]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[9], '%s.input1D[10]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[10], '%s.input1D[11]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[11], '%s.input1D[12]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[12], '%s.input1D[13]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[13], '%s.input1D[14]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[14], '%s.input1D[15]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[15], '%s.input1D[16]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[16], '%s.input1D[17]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[17], '%s.input1D[18]' % pmaNode)

		mc.connectAttr ('%s.rotateZ' % 'Root_Ctrl', '%s.input1Y' % mdvRotNode)
		mc.setAttr ('%s.input2Y' % mdvRotNode, -1)
		mc.connectAttr ('%s.outputY' % mdvRotNode , '%s.input1D[19]' % pmaNode)

		attr = ['X', 'Y', 'Z']
		iix = 69
		for attrs in attr:
			pmaNodeCha = mc.createNode ('plusMinusAverage', n = ('%sCha%s_Pma' % (part , iix+1)))
			mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs) , '%s.input1D[0]' % pmaNodeCha)
			mc.connectAttr ('%s.output1D' % pmaNodeCha, 'TailIkRZ%s_Jnt.rotateY' % (iix+1))
			iix+=1
		ix+=1
# Ctrl 18 Bck----------------------------------------
	attr = ['X', 'Y', 'Z']
	iix = 69
	for attrs in attr:
		mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs), '%sCha%s_Pma.input1D[1]' % (part,iix-1))
		iix-=1
#Ctrl 19 Fnt----------------------------------------
	ix = 72
	ikCtrl = mc.ls('TailIk19_Ctrl')
	for each in range(0, 1):
		pmaNode = mc.createNode ('plusMinusAverage', n = ('%s%s_Pma' % (part , ix+1)))
		mc.connectAttr ('%s.rotateY' % ikCtrl[0], '%s.input1D[0]' % pmaNode)
		mc.connectAttr ('%s.output1D' % pmaNode , '%s.rotateY' % listsIkSca[72])
		mdvNodeCha = mc.createNode ('multiplyDivide', n= ('%sCha%s_Mdv' % (part , ix+1)))
		mc.setAttr ('%s.input2X' % mdvNodeCha, 0.75)
		mc.setAttr ('%s.input2Y' % mdvNodeCha, 0.5)
		mc.setAttr ('%s.input2Z' % mdvNodeCha, 0.25)
		mc.connectAttr('%s.rotateY' % listsIkSca[72], '%s.input1X' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[72], '%s.input1Y' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[72], '%s.input1Z' % mdvNodeCha)

		# Connect Control 19 Rotate 
		mdvRotNode = mc.createNode ('multiplyDivide', n= ('%s%sRot_Mdv' % (part , ix+1)))
		mc.connectAttr ('%s.rotateZ' % fkCtrlLists[0], '%s.input1X' % mdvRotNode)
		mc.connectAttr ('%s.outputX' % mdvRotNode , '%s.input1D[1]' % pmaNode)
		mc.setAttr ('%s.input2X' % mdvRotNode, -1)

		mc.connectAttr ('%s.rotateY' % fkCtrlLists[1], '%s.input1D[2]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[2], '%s.input1D[3]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[3], '%s.input1D[4]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[4], '%s.input1D[5]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[5], '%s.input1D[6]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[6], '%s.input1D[7]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[7], '%s.input1D[8]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[8], '%s.input1D[9]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[9], '%s.input1D[10]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[10], '%s.input1D[11]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[11], '%s.input1D[12]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[12], '%s.input1D[13]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[13], '%s.input1D[14]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[14], '%s.input1D[15]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[15], '%s.input1D[16]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[16], '%s.input1D[17]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[17], '%s.input1D[18]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[18], '%s.input1D[19]' % pmaNode)

		mc.connectAttr ('%s.rotateZ' % 'Root_Ctrl', '%s.input1Y' % mdvRotNode)
		mc.setAttr ('%s.input2Y' % mdvRotNode, -1)
		mc.connectAttr ('%s.outputY' % mdvRotNode , '%s.input1D[20]' % pmaNode)

		attr = ['X', 'Y', 'Z']
		iix = 73
		for attrs in attr:
			pmaNodeCha = mc.createNode ('plusMinusAverage', n = ('%sCha%s_Pma' % (part , iix+1)))
			mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs) , '%s.input1D[0]' % pmaNodeCha)
			mc.connectAttr ('%s.output1D' % pmaNodeCha, 'TailIkRZ%s_Jnt.rotateY' % (iix+1))
			iix+=1
		ix+=1
# Ctrl 19 Bck----------------------------------------
	attr = ['X', 'Y', 'Z']
	iix = 73
	for attrs in attr:
		mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs), '%sCha%s_Pma.input1D[1]' % (part,iix-1))
		iix-=1
#Ctrl 20 Fnt----------------------------------------
	ix = 76
	ikCtrl = mc.ls('TailIk20_Ctrl')
	for each in range(0, 1):
		pmaNode = mc.createNode ('plusMinusAverage', n = ('%s%s_Pma' % (part , ix+1)))
		mc.connectAttr ('%s.rotateY' % ikCtrl[0], '%s.input1D[0]' % pmaNode)
		mc.connectAttr ('%s.output1D' % pmaNode , '%s.rotateY' % listsIkSca[76])
		mdvNodeCha = mc.createNode ('multiplyDivide', n= ('%sCha%s_Mdv' % (part , ix+1)))
		mc.setAttr ('%s.input2X' % mdvNodeCha, 0.75)
		mc.setAttr ('%s.input2Y' % mdvNodeCha, 0.5)
		mc.setAttr ('%s.input2Z' % mdvNodeCha, 0.25)
		mc.connectAttr('%s.rotateY' % listsIkSca[76], '%s.input1X' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[76], '%s.input1Y' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[76], '%s.input1Z' % mdvNodeCha)

		# Connect Control 20 Rotate 
		mdvRotNode = mc.createNode ('multiplyDivide', n= ('%s%sRot_Mdv' % (part , ix+1)))
		mc.connectAttr ('%s.rotateZ' % fkCtrlLists[0], '%s.input1X' % mdvRotNode)
		mc.connectAttr ('%s.outputX' % mdvRotNode , '%s.input1D[1]' % pmaNode)
		mc.setAttr ('%s.input2X' % mdvRotNode, -1)

		mc.connectAttr ('%s.rotateY' % fkCtrlLists[1], '%s.input1D[2]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[2], '%s.input1D[3]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[3], '%s.input1D[4]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[4], '%s.input1D[5]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[5], '%s.input1D[6]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[6], '%s.input1D[7]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[7], '%s.input1D[8]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[8], '%s.input1D[9]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[9], '%s.input1D[10]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[10], '%s.input1D[11]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[11], '%s.input1D[12]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[12], '%s.input1D[13]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[13], '%s.input1D[14]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[14], '%s.input1D[15]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[15], '%s.input1D[16]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[16], '%s.input1D[17]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[17], '%s.input1D[18]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[18], '%s.input1D[19]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[19], '%s.input1D[20]' % pmaNode)

		mc.connectAttr ('%s.rotateZ' % 'Root_Ctrl', '%s.input1Y' % mdvRotNode)
		mc.setAttr ('%s.input2Y' % mdvRotNode, -1)
		mc.connectAttr ('%s.outputY' % mdvRotNode , '%s.input1D[21]' % pmaNode)

		attr = ['X', 'Y', 'Z']
		iix = 77
		for attrs in attr:
			pmaNodeCha = mc.createNode ('plusMinusAverage', n = ('%sCha%s_Pma' % (part , iix+1)))
			mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs) , '%s.input1D[0]' % pmaNodeCha)
			mc.connectAttr ('%s.output1D' % pmaNodeCha, 'TailIkRZ%s_Jnt.rotateY' % (iix+1))
			iix+=1
		ix+=1
# Ctrl 20 Bck----------------------------------------
	attr = ['X', 'Y', 'Z']
	iix = 77
	for attrs in attr:
		mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs), '%sCha%s_Pma.input1D[1]' % (part,iix-1))
		iix-=1
#Ctrl 21 Fnt----------------------------------------
	ix = 80
	ikCtrl = mc.ls('TailIk21_Ctrl')
	for each in range(0, 1):
		pmaNode = mc.createNode ('plusMinusAverage', n = ('%s%s_Pma' % (part , ix+1)))
		mc.connectAttr ('%s.rotateY' % ikCtrl[0], '%s.input1D[0]' % pmaNode)
		mc.connectAttr ('%s.output1D' % pmaNode , '%s.rotateY' % listsIkSca[80])
		mdvNodeCha = mc.createNode ('multiplyDivide', n= ('%sCha%s_Mdv' % (part , ix+1)))
		mc.setAttr ('%s.input2X' % mdvNodeCha, 0.75)
		mc.setAttr ('%s.input2Y' % mdvNodeCha, 0.5)
		mc.setAttr ('%s.input2Z' % mdvNodeCha, 0.25)
		mc.connectAttr('%s.rotateY' % listsIkSca[80], '%s.input1X' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[80], '%s.input1Y' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[80], '%s.input1Z' % mdvNodeCha)

		# Connect Control 21 Rotate 
		mdvRotNode = mc.createNode ('multiplyDivide', n= ('%s%sRot_Mdv' % (part , ix+1)))
		mc.connectAttr ('%s.rotateZ' % fkCtrlLists[0], '%s.input1X' % mdvRotNode)
		mc.connectAttr ('%s.outputX' % mdvRotNode , '%s.input1D[1]' % pmaNode)
		mc.setAttr ('%s.input2X' % mdvRotNode, -1)

		mc.connectAttr ('%s.rotateY' % fkCtrlLists[1], '%s.input1D[2]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[2], '%s.input1D[3]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[3], '%s.input1D[4]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[4], '%s.input1D[5]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[5], '%s.input1D[6]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[6], '%s.input1D[7]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[7], '%s.input1D[8]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[8], '%s.input1D[9]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[9], '%s.input1D[10]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[10], '%s.input1D[11]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[11], '%s.input1D[12]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[12], '%s.input1D[13]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[13], '%s.input1D[14]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[14], '%s.input1D[15]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[15], '%s.input1D[16]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[16], '%s.input1D[17]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[17], '%s.input1D[18]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[18], '%s.input1D[19]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[19], '%s.input1D[20]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[20], '%s.input1D[21]' % pmaNode)

		mc.connectAttr ('%s.rotateZ' % 'Root_Ctrl', '%s.input1Y' % mdvRotNode)
		mc.setAttr ('%s.input2Y' % mdvRotNode, -1)
		mc.connectAttr ('%s.outputY' % mdvRotNode , '%s.input1D[22]' % pmaNode)

		attr = ['X', 'Y', 'Z']
		iix = 81
		for attrs in attr:
			pmaNodeCha = mc.createNode ('plusMinusAverage', n = ('%sCha%s_Pma' % (part , iix+1)))
			mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs) , '%s.input1D[0]' % pmaNodeCha)
			mc.connectAttr ('%s.output1D' % pmaNodeCha, 'TailIkRZ%s_Jnt.rotateY' % (iix+1))
			iix+=1
		ix+=1
# Ctrl 21 Bck----------------------------------------
	attr = ['X', 'Y', 'Z']
	iix = 81
	for attrs in attr:
		mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs), '%sCha%s_Pma.input1D[1]' % (part,iix-1))
		iix-=1
#Ctrl 22 Fnt----------------------------------------
	ix = 84
	ikCtrl = mc.ls('TailIk22_Ctrl')
	for each in range(0, 1):
		pmaNode = mc.createNode ('plusMinusAverage', n = ('%s%s_Pma' % (part , ix+1)))
		mc.connectAttr ('%s.rotateY' % ikCtrl[0], '%s.input1D[0]' % pmaNode)
		mc.connectAttr ('%s.output1D' % pmaNode , '%s.rotateY' % listsIkSca[84])
		mdvNodeCha = mc.createNode ('multiplyDivide', n= ('%sCha%s_Mdv' % (part , ix+1)))
		mc.setAttr ('%s.input2X' % mdvNodeCha, 0.75)
		mc.setAttr ('%s.input2Y' % mdvNodeCha, 0.5)
		mc.setAttr ('%s.input2Z' % mdvNodeCha, 0.25)
		mc.connectAttr('%s.rotateY' % listsIkSca[84], '%s.input1X' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[84], '%s.input1Y' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[84], '%s.input1Z' % mdvNodeCha)

		# Connect Control 22 Rotate 
		mdvRotNode = mc.createNode ('multiplyDivide', n= ('%s%sRot_Mdv' % (part , ix+1)))
		mc.connectAttr ('%s.rotateZ' % fkCtrlLists[0], '%s.input1X' % mdvRotNode)
		mc.connectAttr ('%s.outputX' % mdvRotNode , '%s.input1D[1]' % pmaNode)
		mc.setAttr ('%s.input2X' % mdvRotNode, -1)

		mc.connectAttr ('%s.rotateY' % fkCtrlLists[1], '%s.input1D[2]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[2], '%s.input1D[3]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[3], '%s.input1D[4]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[4], '%s.input1D[5]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[5], '%s.input1D[6]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[6], '%s.input1D[7]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[7], '%s.input1D[8]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[8], '%s.input1D[9]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[9], '%s.input1D[10]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[10], '%s.input1D[11]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[11], '%s.input1D[12]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[12], '%s.input1D[13]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[13], '%s.input1D[14]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[14], '%s.input1D[15]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[15], '%s.input1D[16]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[16], '%s.input1D[17]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[17], '%s.input1D[18]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[18], '%s.input1D[19]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[19], '%s.input1D[20]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[20], '%s.input1D[21]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[21], '%s.input1D[22]' % pmaNode)

		mc.connectAttr ('%s.rotateZ' % 'Root_Ctrl', '%s.input1Y' % mdvRotNode)
		mc.setAttr ('%s.input2Y' % mdvRotNode, -1)
		mc.connectAttr ('%s.outputY' % mdvRotNode , '%s.input1D[23]' % pmaNode)

		attr = ['X', 'Y', 'Z']
		iix = 85
		for attrs in attr:
			pmaNodeCha = mc.createNode ('plusMinusAverage', n = ('%sCha%s_Pma' % (part , iix+1)))
			mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs) , '%s.input1D[0]' % pmaNodeCha)
			mc.connectAttr ('%s.output1D' % pmaNodeCha, 'TailIkRZ%s_Jnt.rotateY' % (iix+1))
			iix+=1
		ix+=1
# Ctrl 22 Bck----------------------------------------
	attr = ['X', 'Y', 'Z']
	iix = 85
	for attrs in attr:
		mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs), '%sCha%s_Pma.input1D[1]' % (part,iix-1))
		iix-=1
#Ctrl 23 Fnt----------------------------------------
	ix = 88
	ikCtrl = mc.ls('TailIk23_Ctrl')
	for each in range(0, 1):
		pmaNode = mc.createNode ('plusMinusAverage', n = ('%s%s_Pma' % (part , ix+1)))
		mc.connectAttr ('%s.rotateY' % ikCtrl[0], '%s.input1D[0]' % pmaNode)
		mc.connectAttr ('%s.output1D' % pmaNode , '%s.rotateY' % listsIkSca[88])
		mdvNodeCha = mc.createNode ('multiplyDivide', n= ('%sCha%s_Mdv' % (part , ix+1)))
		mc.setAttr ('%s.input2X' % mdvNodeCha, 0.75)
		mc.setAttr ('%s.input2Y' % mdvNodeCha, 0.5)
		mc.setAttr ('%s.input2Z' % mdvNodeCha, 0.25)
		mc.connectAttr('%s.rotateY' % listsIkSca[88], '%s.input1X' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[88], '%s.input1Y' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[88], '%s.input1Z' % mdvNodeCha)

		# Connect Control 23 Rotate 
		mdvRotNode = mc.createNode ('multiplyDivide', n= ('%s%sRot_Mdv' % (part , ix+1)))
		mc.connectAttr ('%s.rotateZ' % fkCtrlLists[0], '%s.input1X' % mdvRotNode)
		mc.connectAttr ('%s.outputX' % mdvRotNode , '%s.input1D[1]' % pmaNode)
		mc.setAttr ('%s.input2X' % mdvRotNode, -1)

		mc.connectAttr ('%s.rotateY' % fkCtrlLists[1], '%s.input1D[2]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[2], '%s.input1D[3]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[3], '%s.input1D[4]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[4], '%s.input1D[5]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[5], '%s.input1D[6]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[6], '%s.input1D[7]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[7], '%s.input1D[8]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[8], '%s.input1D[9]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[9], '%s.input1D[10]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[10], '%s.input1D[11]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[11], '%s.input1D[12]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[12], '%s.input1D[13]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[13], '%s.input1D[14]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[14], '%s.input1D[15]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[15], '%s.input1D[16]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[16], '%s.input1D[17]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[17], '%s.input1D[18]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[18], '%s.input1D[19]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[19], '%s.input1D[20]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[20], '%s.input1D[21]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[21], '%s.input1D[22]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[22], '%s.input1D[23]' % pmaNode)


		mc.connectAttr ('%s.rotateZ' % 'Root_Ctrl', '%s.input1Y' % mdvRotNode)
		mc.setAttr ('%s.input2Y' % mdvRotNode, -1)
		mc.connectAttr ('%s.outputY' % mdvRotNode , '%s.input1D[24]' % pmaNode)

		attr = ['X', 'Y', 'Z']
		iix = 89
		for attrs in attr:
			pmaNodeCha = mc.createNode ('plusMinusAverage', n = ('%sCha%s_Pma' % (part , iix+1)))
			mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs) , '%s.input1D[0]' % pmaNodeCha)
			mc.connectAttr ('%s.output1D' % pmaNodeCha, 'TailIkRZ%s_Jnt.rotateY' % (iix+1))
			iix+=1
		ix+=1
# Ctrl 23 Bck----------------------------------------
	attr = ['X', 'Y', 'Z']
	iix = 89
	for attrs in attr:
		mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs), '%sCha%s_Pma.input1D[1]' % (part,iix-1))
		iix-=1
#Ctrl 24 Fnt----------------------------------------
	ix = 92
	ikCtrl = mc.ls('TailIk24_Ctrl')
	for each in range(0, 1):
		pmaNode = mc.createNode ('plusMinusAverage', n = ('%s%s_Pma' % (part , ix+1)))
		mc.connectAttr ('%s.rotateY' % ikCtrl[0], '%s.input1D[0]' % pmaNode)
		mc.connectAttr ('%s.output1D' % pmaNode , '%s.rotateY' % listsIkSca[92])
		mdvNodeCha = mc.createNode ('multiplyDivide', n= ('%sCha%s_Mdv' % (part , ix+1)))
		mc.setAttr ('%s.input2X' % mdvNodeCha, 0.75)
		mc.setAttr ('%s.input2Y' % mdvNodeCha, 0.5)
		mc.setAttr ('%s.input2Z' % mdvNodeCha, 0.25)
		mc.connectAttr('%s.rotateY' % listsIkSca[92], '%s.input1X' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[92], '%s.input1Y' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[92], '%s.input1Z' % mdvNodeCha)

		# Connect Control 24 Rotate 
		mdvRotNode = mc.createNode ('multiplyDivide', n= ('%s%sRot_Mdv' % (part , ix+1)))
		mc.connectAttr ('%s.rotateZ' % fkCtrlLists[0], '%s.input1X' % mdvRotNode)
		mc.connectAttr ('%s.outputX' % mdvRotNode , '%s.input1D[1]' % pmaNode)
		mc.setAttr ('%s.input2X' % mdvRotNode, -1)



		mc.connectAttr ('%s.rotateY' % fkCtrlLists[1], '%s.input1D[2]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[2], '%s.input1D[3]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[3], '%s.input1D[4]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[4], '%s.input1D[5]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[5], '%s.input1D[6]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[6], '%s.input1D[7]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[7], '%s.input1D[8]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[8], '%s.input1D[9]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[9], '%s.input1D[10]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[10], '%s.input1D[11]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[11], '%s.input1D[12]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[12], '%s.input1D[13]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[13], '%s.input1D[14]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[14], '%s.input1D[15]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[15], '%s.input1D[16]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[16], '%s.input1D[17]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[17], '%s.input1D[18]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[18], '%s.input1D[19]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[19], '%s.input1D[20]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[20], '%s.input1D[21]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[21], '%s.input1D[22]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[22], '%s.input1D[23]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[23], '%s.input1D[24]' % pmaNode)

		mc.connectAttr ('%s.rotateZ' % 'Root_Ctrl', '%s.input1Y' % mdvRotNode)
		mc.setAttr ('%s.input2Y' % mdvRotNode, -1)
		mc.connectAttr ('%s.outputY' % mdvRotNode , '%s.input1D[25]' % pmaNode)

		attr = ['X', 'Y', 'Z']
		iix = 93
		for attrs in attr:
			pmaNodeCha = mc.createNode ('plusMinusAverage', n = ('%sCha%s_Pma' % (part , iix+1)))
			mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs) , '%s.input1D[0]' % pmaNodeCha)
			mc.connectAttr ('%s.output1D' % pmaNodeCha, 'TailIkRZ%s_Jnt.rotateY' % (iix+1))
			iix+=1
		ix+=1
# Ctrl 24 Bck----------------------------------------
	attr = ['X', 'Y', 'Z']
	iix = 93
	for attrs in attr:
		mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs), '%sCha%s_Pma.input1D[1]' % (part,iix-1))
		iix-=1
#Ctrl 25 Fnt----------------------------------------
	ix = 96
	ikCtrl = mc.ls('TailIk25_Ctrl')
	for each in range(0, 1):
		pmaNode = mc.createNode ('plusMinusAverage', n = ('%s%s_Pma' % (part , ix+1)))
		mc.connectAttr ('%s.rotateY' % ikCtrl[0], '%s.input1D[0]' % pmaNode)
		mc.connectAttr ('%s.output1D' % pmaNode , '%s.rotateY' % listsIkSca[96])
		mdvNodeCha = mc.createNode ('multiplyDivide', n= ('%sCha%s_Mdv' % (part , ix+1)))
		mc.setAttr ('%s.input2X' % mdvNodeCha, 0.75)
		mc.setAttr ('%s.input2Y' % mdvNodeCha, 0.5)
		mc.setAttr ('%s.input2Z' % mdvNodeCha, 0.25)
		mc.connectAttr('%s.rotateY' % listsIkSca[96], '%s.input1X' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[96], '%s.input1Y' % mdvNodeCha)
		mc.connectAttr('%s.rotateY' % listsIkSca[96], '%s.input1Z' % mdvNodeCha)

		# Connect Control 25 Rotate 
		mdvRotNode = mc.createNode ('multiplyDivide', n= ('%s%sRot_Mdv' % (part , ix+1)))
		mc.connectAttr ('%s.rotateZ' % fkCtrlLists[0], '%s.input1X' % mdvRotNode)
		mc.connectAttr ('%s.outputX' % mdvRotNode , '%s.input1D[1]' % pmaNode)
		mc.setAttr ('%s.input2X' % mdvRotNode, -1)

		mc.connectAttr ('%s.rotateY' % fkCtrlLists[1], '%s.input1D[2]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[2], '%s.input1D[3]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[3], '%s.input1D[4]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[4], '%s.input1D[5]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[5], '%s.input1D[6]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[6], '%s.input1D[7]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[7], '%s.input1D[8]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[8], '%s.input1D[9]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[9], '%s.input1D[10]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[10], '%s.input1D[11]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[11], '%s.input1D[12]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[12], '%s.input1D[13]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[13], '%s.input1D[14]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[14], '%s.input1D[15]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[15], '%s.input1D[16]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[16], '%s.input1D[17]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[17], '%s.input1D[18]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[18], '%s.input1D[19]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[19], '%s.input1D[20]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[20], '%s.input1D[21]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[21], '%s.input1D[22]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[22], '%s.input1D[23]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[23], '%s.input1D[24]' % pmaNode)
		mc.connectAttr ('%s.rotateY' % fkCtrlLists[24], '%s.input1D[25]' % pmaNode)

		mc.connectAttr ('%s.rotateZ' % 'Root_Ctrl', '%s.input1Y' % mdvRotNode)
		mc.setAttr ('%s.input2Y' % mdvRotNode, -1)
		mc.connectAttr ('%s.outputY' % mdvRotNode , '%s.input1D[26]' % pmaNode)




#Ctrl 25 Bck----------------------------------------
	attr = ['X', 'Y', 'Z']
	iix = 96
	ixx = 97
	for attrs in attr:
		pmaNodeCha = mc.createNode ('plusMinusAverage', n = ('%sCha%s_Pma' % (part , iix+1)))
		mc.connectAttr ('%s.output%s' % (mdvNodeCha, attrs), '%sCha%s_Pma.input1D[0]' % (part,iix+1))
		mc.connectAttr ('%s.output1D' % pmaNodeCha, '%sCha%s_Pma.input1D[1]' % (part,ixx-1))
		iix+=1
		ixx-=1

