import shutil 
# import os
import maya.cmds as mc
import pymel.core as pm
import maya.mel as mm
import os, sys

# from rf_utils.context import context_info
from rf_utils.context import context_info
# reload(context_info)


def getDataFld(*args) : 
	asset = context_info.ContextPathInfo()
	# Project Name 
	projectName = asset.project if mc.file(q=True, sn=True) else ''
   
	if (projectName == 'SevenChickMovie'):
		asset.context.update(process='data')
		dataPath = asset.path.publish_hero()
		scn_data , scn_hero = os.path.split(dataPath)
		scn_rig , scn_data2 = os.path.split(scn_data)

		tmpAry = scn_data.split( '/' )
		tmpAry[-1] = 'data' 
		tmpAry[0] = 'P:'
		dataFld = '\\'.join( tmpAry) 

		tmpAryRig = scn_rig.split( '/' )
		tmpAryRig[-1] = 'rig' 
		tmpAryRig[0] = 'P:'
		dataFldRig = '\\'.join( tmpAryRig) 

		print dataFldRig
		if not os.path.isdir(dataFldRig)
			os.mkdir( dataFldRig ) 
		return dataFldRig  
		
		if not os.path.isdir( dataFld ) :
			os.mkdir( dataFld ) 
		return dataFld  

 	else :
	    wfn = os.path.normpath(pm.sceneName() )
	    tmpAry = wfn.split( '\\' )
	    tmpAry[-2] = 'data' 
	    dataFld = '\\'.join( tmpAry[0:-1] ) 
	    if not os.path.isdir( dataFld ) :
	        os.mkdir( dataFld ) 
	    return dataFld  

def copyObjFile(src,*args):
    shutil.copy(src, getDataFld())