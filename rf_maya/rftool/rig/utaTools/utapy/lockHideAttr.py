import maya.cmds as mc
import random
import math
 
def lockHideAttr(obj='',attrArray='',lock=True,hide= False):
	# TailFkLists = []
	if mc.objExists(obj==True):
		for each in obj:
			for a in attrArray:
				mc.setAttr(each + '.' + a, k=hide,l=lock)
	else:
		obj = mc.ls('TailFk_*_Ctrl')
		# TailFkLists.append(obj)
		for each in obj:
			for a in attrArray:
				mc.setAttr(each + '.' + a, k=hide,l=lock)