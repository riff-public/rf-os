import maya.cmds as mc
def updateModelMd(*args):
	listsMdGrp = mc.ls('MdGeo_Grp')
	listsGeoGrp = mc.ls('Geo_Grp')
	gmblCtrl = mc.ls('RootSubGmbl_Ctrl')
	ctrl = mc.ls('RootSub_Ctrl')
	mc.parent(listsMdGrp[0], w = True)
	for each in listsGeoGrp:
	    check = mc.listRelatives(each, p = True)
	    if check == None:
	        mc.delete(each)
	    else:
	        geoGrp = each.split('|')[-1]
	mc.parent(listsMdGrp[0], geoGrp)
	if mc.objExists(gmblCtrl[0]):
	    mc.parentConstraint(gmblCtrl[0], listsMdGrp[0],mo =True) 
	    mc.scaleConstraint(gmblCtrl[0], listsMdGrp[0],mo =True) 
	if mc.objExists(ctrl[0]):
	    mc.connectAttr('{}.GeoVis'.format(ctrl[0]), '{}.v'.format(listsMdGrp[0]))