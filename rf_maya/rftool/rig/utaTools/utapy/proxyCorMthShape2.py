import maya.cmds as mc
# reload(mc)
# import pymel.core as pm 
# reload(pm)
#-----------------#
# Shapes = Shape  #
# Mth Geo         #
#-----------------#


def proxyCorCrtMthBsn(	tyOffset, 
						jawMthRotateX, 
						bodyMthRig=["BodyMthRig_Geo"], 
						BodyMthLip=["BodyMthLipRig_Geo"], 
						BodyMthJaw=["BodyMthJawRig_Geo"],
						elem = '',
						*args):
	#BodyMthRig_Geo
	#BodyMthLipRig_Geo
	#BodyMthJawRig_Geo
	
	headOrig = ["Orig%s" % elem, ("JawOpenRef%s%s" % (jawMthRotateX, elem))]
	crtMthShapes = ["JawOpenMthShapes%s" % elem, "JawOpenShapes%s" % elem]
	crtBshShapes = ["CnrUpOpenRefShapes%s" % elem, "CnrDnOpenRefShapes%s" % elem, "CnrInOpenShapes%s" % elem, "CnrOutOpenShapes%s" % elem, "CnrInUpOpenShapes%s" % elem, "CnrInDnOpenShapes%s" % elem, "CnrOutUpOpenShapes%s" % elem, "CnrOutDnOpenShapes%s" % elem]          
	crtBsh = ["JawOpenMth%s" % elem, "JawOpen%s" % elem, "CnrUpOpen%s" % elem, "CnrDnOpen%s" % elem, "CnrInOpen%s" % elem, "CnrOutOpen%s" % elem, "CnrInUpOpen%s" % elem, "CnrInDnOpen%s" % elem, "CnrOutUpOpen%s" % elem, "CnrOutDnOpen%s" % elem]      
	crtPuff =  ["LipUpPuffMthRigShapes%s" % elem, "LipLowPuffMthRigShapes%s" % elem, "LipUpThickMthRigShapes%s" % elem, "LipLowThickMthRigShapes%s" % elem]
	mthctrl = mc.ls("*:JawMthRig_Ctrl")
	# headJaw = mc.ls("BodyMthJawRig_Geo")
	# headMth = mc.ls("BodyMthRig_Geo")
	headGeo =[BodyMthLip,BodyMthJaw]
	headGeoUnlock = (bodyMthRig+BodyMthLip+BodyMthJaw)
	print headGeo
	orig_Grp = mc.createNode("transform", n = "orig%s_Grp" % elem)
	crtBsh_Grp = mc.createNode('transform',n = 'crtBsh%s_Grp' % elem )
	crtBshShapes_Grps = mc.createNode('transform',n = 'crtBshShapes%s_Grp' % elem)
	crtBshCon_Grps = mc.createNode('transform',n = 'crtBshCon%s_Grp' % elem)
	crtPuff_Grps = mc.createNode('transform',n = 'crtPuff%s_Grp' % elem)
	attr = ["tx", "ty", "tz", "rx", "ry", "rz", "sx", "sy", "sz"]
	# unlock headJaw
	for each in headGeo:
		for attrs in attr:
			mc.setAttr("%s.%s" % (each, attrs), l = False);
	# HeadOriShapes and JawShapes(valueRotate)
	headBshOrig = mc.duplicate(BodyMthJaw, n = "%s" % headOrig[0])
	mc.setAttr("%s.rotateX" % (mthctrl[0]),jawMthRotateX)
	headJawOprnRef = mc.duplicate(BodyMthJaw, n = "%s" % headOrig[1])


	# Duplicate crtBshShapes
	crtMthShapesAmount = len(crtMthShapes)
	crtBshShapesAmount = len(crtBshShapes)
	crtBshAmount = len(crtBsh)
	crtPuffAmount = len(crtPuff)

	# move Orig, jawOpen
	defaultTyEach = 0
	
	for each in headOrig:
		for attrs in attr:
			mc.setAttr("%s.%s" % (each, attrs), l = False);
			# mc.setAttr("%s.translateX" % (each),defaultTyEach)
			defaultTyEach += 3
		mc.parent (each, orig_Grp)


	# for crtBshShapess in crtBshShapes:
	for i in range(crtMthShapesAmount):

		bshShapes = mc.duplicate(BodyMthJaw, n = "%s" % crtMthShapes[i])
		mc.parent(crtMthShapes[i], crtBshShapes_Grps)
	for i in range(crtBshShapesAmount):
		bshShapes = mc.duplicate(headOrig[0], n = "%s" % crtBshShapes[i])
		mc.parent(crtBshShapes[i], crtBshShapes_Grps)
	for i in range(crtBshAmount):
		bshShapes = mc.duplicate(headOrig[0], n = "%s" % crtBsh[i])
		mc.parent(crtBsh[i], crtBshCon_Grps)
	for i in range(crtPuffAmount):
		bshShapes = mc.duplicate(headOrig[0], n = "%s" % crtPuff[i])
		mc.parent(crtPuff[i], crtPuff_Grps)



	# setAttribute JawOpen Default
	mc.setAttr("%s.rotateX" % (mthctrl[0]),0)
	mc.setAttr ("%sShape.jawOpen" % mthctrl[0], jawMthRotateX);
	# parent to crtBsh_Grp
	mc.parent(crtBshShapes_Grps, crtBshCon_Grps, crtPuff_Grps, orig_Grp, crtBsh_Grp)
	mc.setAttr("%s.translateX" % (crtBshCon_Grps),-3.5*int(tyOffset))
	mc.setAttr("%s.translateX" % (crtBshShapes_Grps),-2.5*int(tyOffset))
	mc.setAttr("%s.translateX" % (crtPuff_Grps),-1.5*int(tyOffset))

	mc.setAttr("%s.translateX" % (headOrig[0]),1.5*jawMthRotateX)
	mc.setAttr("%s.translateX" % (headOrig[1]),3.5*jawMthRotateX)


	crtBshShapesTotal = (crtMthShapes+crtBshShapes)
	# print crtBshShapesTotal
	# translateY 
	crtBshShapes_GrpsAmount = len(crtBshShapesTotal)
	crtBshCon_GrpsAmount = len(crtBsh)
	crtPuff_GrpsAmount = len(crtPuff)
	defaultTy = 0
	for i in range(crtBshShapes_GrpsAmount):
		mc.setAttr("%s.translateY" % (crtBshShapesTotal[i]), defaultTy)
		defaultTy += tyOffset
	defaultTy = 0
	for i in range(crtBshCon_GrpsAmount):
		mc.setAttr("%s.translateY" % (crtBsh[i]), defaultTy)
		defaultTy += tyOffset
	defaultTy = 0
	for i in range(crtPuff_GrpsAmount):
		mc.setAttr("%s.translateY" % (crtPuff[i]), defaultTy)
		defaultTy += tyOffset
 	mc.select(cl=True)
	# create BlendShape Shapestive Mouth

	
	bshList = ["CnrOutDnOpen%s" % elem, "CnrOutUpOpen%s" % elem, "CnrInDnOpen%s" % elem, "CnrInUpOpen%s" % elem, "CnrOutOpen%s" % elem, "CnrInOpen%s" % elem, "CnrDnOpen%s" % elem, "CnrUpOpen%s" % elem, "JawOpen%s" % elem, "JawOpenMth%s" % elem]
	# proxhList = ["cornerOuterDn" , "cornerOuterUp", "cornerInnerDn", "cornerInnerUp", "cornerOut", "cornerIn", "cornerDn", "cornerUp", ""]
	# ShapesList = ["CnrOutDnOpenShapes", "CnrOutUpOpenShapes", "CnrInDnOpenShapes", "CnrInUpOpenShapes", "CnrOutOpenShapes", "CnrInOpenShapes", "CnrDnOpenRefShapes", "CnrUpOpenRefShapes"]
	# jawOpenRefNum = ("JawOpenRef20")
	# jawOpenShapesList = ("JawOpenShapes")
	
	'''1
	CnrOutDnOpen
		JawOpenShapes -1
		cornerOuterDn -1
		CnrOutDnOpenShapes 1
	'''
	print bshList
	CnrOutDnOpenBsn = mc.blendShape("JawOpenShapes%s" % elem, "cornerOuterDn%s" % elem, "CnrOutDnOpenShapes%s" % elem, "CnrOutDnOpen%s" % elem,n="%sBsn" % bshList[0])[0]
	mc.setAttr("%sBsn" % bshList[0]+"."+"JawOpenShapes%s" % elem,-1)
	mc.setAttr("%sBsn" % bshList[0]+"."+"cornerOuterDn%s" % elem,-1)
	mc.setAttr("%sBsn" % bshList[0]+"."+"CnrOutDnOpenShapes%s" % elem,1)

	CnrOutDnOpenShapesBsn = mc.blendShape("JawOpenShapes%s" % elem, "cornerOuterDn%s" % elem, "CnrOutDnOpenShapes%s" % elem, n="%sShapesBsn" % bshList[0])[0]
	mc.setAttr("%sShapesBsn" % bshList[0]+"."+"JawOpenShapes%s" % elem,1)
	mc.setAttr("%sShapesBsn" % bshList[0]+"."+"cornerOuterDn%s" % elem,1)
	# mc.setAttr("%sBsn" % bshList[0]+"."+"CnrOutDnOpenShapes",1)
	'''2
	CnrOutUpOpen
		JawOpenShapes -1
		cornerOuterUp -1
		CnrOutUpOpenShapes 1
	'''
	CnrOutUpOpenBsn = mc.blendShape("JawOpenShapes%s" % elem, "cornerOuterUp%s" % elem, "CnrOutUpOpenShapes%s" % elem, "CnrOutUpOpen%s" % elem,n="%sBsn" % bshList[1])[0]
	mc.setAttr("%sBsn" % bshList[1]+"."+"JawOpenShapes%s" % elem,-1)
	mc.setAttr("%sBsn" % bshList[1]+"."+"cornerOuterUp%s" % elem,-1)
	mc.setAttr("%sBsn" % bshList[1]+"."+"CnrOutUpOpenShapes%s" % elem,1)

	CnrOutUpOpenShapesBsn = mc.blendShape("JawOpenShapes%s" % elem, "cornerOuterUp%s" % elem, "CnrOutUpOpenShapes%s" % elem, n="%sShapesBsn" % bshList[1])[0]
	mc.setAttr("%sShapesBsn" % bshList[1]+"."+"JawOpenShapes%s" % elem,1)
	mc.setAttr("%sShapesBsn" % bshList[1]+"."+"cornerOuterUp%s" % elem,1)
	# mc.setAttr("%sShapesBsn" % bshList[1]+"."+"CnrOutUpOpenShapes",1)
	'''3
	CnrInDnOpen
		JawOpenShapes -1
		cornerInnerDn -1
		cnrInDnOpenShapes 1
	'''
	CnrInDnOpenBsn = mc.blendShape("JawOpenShapes%s" % elem, "cornerInnerDn%s" % elem, "CnrInDnOpenShapes%s" % elem, "CnrInDnOpen%s" % elem,n="%sBsn" % bshList[2])[0]
	mc.setAttr("%sBsn" % bshList[2]+"."+"JawOpenShapes%s" % elem,-1)
	mc.setAttr("%sBsn" % bshList[2]+"."+"cornerInnerDn%s" % elem,-1)
	mc.setAttr("%sBsn" % bshList[2]+"."+"CnrInDnOpenShapes%s" % elem,1)

	CnrInDnOpenShapesBsn = mc.blendShape("JawOpenShapes%s" % elem, "cornerInnerDn%s" % elem, "CnrInDnOpenShapes%s" % elem, n="%sShapesBsn" % bshList[2])[0]
	mc.setAttr("%sShapesBsn" % bshList[2]+"."+"JawOpenShapes%s" % elem,1)
	mc.setAttr("%sShapesBsn" % bshList[2]+"."+"cornerInnerDn%s" % elem,1)
	# mc.setAttr("%sShapesBsn" % bshList[2]+"."+"CnrInDnOpenShapes",1)
	'''4
	CnrInUpOpen
		JawOpenShapes -1
		cornerInnerUp -1
		CnrInUpOpenShapes 1
	'''
	CnrInUpOpenBsn = mc.blendShape("JawOpenShapes%s" % elem, "cornerInnerUp%s" % elem, "CnrInUpOpenShapes%s" % elem, "CnrInUpOpen%s" % elem,n="%sBsn" % bshList[3])[0]
	mc.setAttr("%sBsn" % bshList[3]+"."+"JawOpenShapes%s" % elem,-1)
	mc.setAttr("%sBsn" % bshList[3]+"."+"cornerInnerUp%s" % elem,-1)
	mc.setAttr("%sBsn" % bshList[3]+"."+"CnrInUpOpenShapes%s" % elem,1)

	CnrInUpOpenBsn = mc.blendShape("JawOpenShapes%s" % elem, "cornerInnerUp%s" % elem, "CnrInUpOpenShapes%s" % elem, n="%sShapesBsn" % bshList[3])[0]
	mc.setAttr("%sShapesBsn" % bshList[3]+"."+"JawOpenShapes%s" % elem,1)
	mc.setAttr("%sShapesBsn" % bshList[3]+"."+"cornerInnerUp%s" % elem,1)
	# mc.setAttr("%sShapesBsn" % bshList[3]+"."+"CnrInUpOpenShapes",1)
	'''5
	CnrOutOpen
		JawOpenShapes -1
		cornerOut -1
		CnrOutOpenShapes 1
	'''
	CnrOutOpenBsn = mc.blendShape("JawOpenShapes%s" % elem, "cornerOut%s" % elem, "CnrOutOpenShapes%s" % elem, "CnrOutOpen%s" % elem,n="%sBsn" % bshList[4])[0]
	mc.setAttr("%sBsn" % bshList[4]+"."+"JawOpenShapes%s" % elem,-1)
	mc.setAttr("%sBsn" % bshList[4]+"."+"cornerOut%s" % elem,-1)
	mc.setAttr("%sBsn" % bshList[4]+"."+"CnrOutOpenShapes%s" % elem,1)

	CnrOutOpenShapesBsn = mc.blendShape("JawOpenShapes%s" % elem, "cornerOut%s" % elem, "CnrOutOpenShapes%s" % elem, n="%sShapesBsn" % bshList[4])[0]
	mc.setAttr("%sShapesBsn" % bshList[4]+"."+"JawOpenShapes%s" % elem,1)
	mc.setAttr("%sShapesBsn" % bshList[4]+"."+"cornerOut%s" % elem,1)
	# mc.setAttr("%sShapessn" % bshList[4]+"."+"CnrOutOpenShapes",1)
	'''6
	CnrInOpen
		JawOpenShapes -1
		cornerIn -1
		CnrInOpenShapes 1
	'''
	CnrInOpenBsn = mc.blendShape("JawOpenShapes%s" % elem, "cornerIn%s" % elem, "CnrInOpenShapes%s" % elem, "CnrInOpen%s" % elem,n="%sBsn" % bshList[5])[0]
	mc.setAttr("%sBsn" % bshList[5]+"."+"JawOpenShapes%s" % elem,-1)
	mc.setAttr("%sBsn" % bshList[5]+"."+"cornerIn%s" % elem,-1)
	mc.setAttr("%sBsn" % bshList[5]+"."+"CnrInOpenShapes%s" % elem,1)

	CnrInOpenShapesBsn = mc.blendShape("JawOpenShapes%s" % elem, "cornerIn%s" % elem, "CnrInOpenShapes%s" % elem, n="%sShapesBsn" % bshList[5])[0]
	mc.setAttr("%sShapesBsn" % bshList[5]+"."+"JawOpenShapes%s" % elem,1)
	mc.setAttr("%sShapesBsn" % bshList[5]+"."+"cornerIn%s" % elem,1)
	# mc.setAttr("%sShapesBsn" % bshList[5]+"."+"CnrInOpenShapes",1)
	'''7
	CnrDnOpen
		JawOpenRef20 -1
		cornerDn -1
		CnrDnOpenRefShapes 1
	'''
	CnrDnOpenBsn = mc.blendShape(("JawOpenRef%s%s" % (jawMthRotateX, elem)), "cornerDn%s" % elem, "CnrDnOpenRefShapes%s" % elem, "CnrDnOpen%s" % elem,n="%sBsn" % bshList[6])[0]
	mc.setAttr("%sBsn" % bshList[6]+"."+("JawOpenRef%s%s" % (jawMthRotateX, elem)),-1)
	mc.setAttr("%sBsn" % bshList[6]+"."+"cornerDn%s" % elem,-1)
	mc.setAttr("%sBsn" % bshList[6]+"."+"CnrDnOpenRefShapes%s" % elem,1)

	CnrDnOpenRefShapesBsn = mc.blendShape(("JawOpenRef%s%s" % (jawMthRotateX, elem)), "cornerDn%s" % elem, "CnrDnOpenRefShapes%s" % elem,n="%sBsn" % crtBshShapesTotal[3])[0]
	mc.setAttr("%sBsn" % crtBshShapesTotal[3]+"."+("JawOpenRef%s%s" % (jawMthRotateX, elem)),1)
	mc.setAttr("%sBsn" % crtBshShapesTotal[3]+"."+"cornerDn%s" % elem,1)
	# mc.setAttr("%sBsn" % bshList[6]+"."+"CnrDnOpenRefShapes",1)
	'''8
	CnrUpOpen
		JawOpenRef20 -1
		cornerUp -1
		CnrUpOpenRefShapes 1
	'''
	CnrUpOpenBsn = mc.blendShape(("JawOpenRef%s%s" % (jawMthRotateX, elem)), "cornerUp%s" % elem, "CnrUpOpenRefShapes%s" % elem, "CnrUpOpen%s" % elem,n="%sBsn" % bshList[7])[0]
	mc.setAttr("%sBsn" % bshList[7]+"."+("JawOpenRef%s%s" % (jawMthRotateX, elem)),-1)
	mc.setAttr("%sBsn" % bshList[7]+"."+"cornerUp%s" % elem,-1)
	mc.setAttr("%sBsn" % bshList[7]+"."+"CnrUpOpenRefShapes%s" % elem,1)

	CnrUpOpenRefShapesBsn = mc.blendShape(("JawOpenRef%s%s" % (jawMthRotateX, elem)), "cornerUp%s" % elem, "CnrUpOpenRefShapes%s" % elem,n="%sBsn" % crtBshShapesTotal[2])[0]
	mc.setAttr("%sBsn" % crtBshShapesTotal[2]+"."+("JawOpenRef%s%s" % (jawMthRotateX, elem)),1)
	mc.setAttr("%sBsn" % crtBshShapesTotal[2]+"."+"cornerUp%s" % elem,1)
	# mc.setAttr("%sBsn" % bshList[7]+"."+"CnrUpOpenRefShapes",1)

	'''9
	JawOpen
		JawOpenRef20 -1
		JawOpenShapes 1
	'''
	JawOpenBsn = mc.blendShape(("JawOpenRef%s%s" % (jawMthRotateX, elem)), "JawOpenShapes%s" % elem, "JawOpen%s" % elem,n="%sBsn" % bshList[8])[0]
	mc.setAttr("%sBsn" % bshList[8]+"."+("JawOpenRef%s%s" % (jawMthRotateX, elem)),-1)
	mc.setAttr("%sBsn" % bshList[8]+"."+"JawOpenShapes%s" % elem,1)

	# JawOpenShapesBsn = mc.blendShape(("JawOpenRef%s" % jawMthRotateX), "JawOpenShapes", "JawOpen",n="%sBsn" % bshList[8])[0]
	# mc.setAttr("%sBsn" % bshList[8]+"."+("JawOpenRef%s" % jawMthRotateX),-1)
	# mc.setAttr("%sBsn" % bshList[8]+"."+"JawOpenShapes",1)
	# JawOpenShape
	'''10
	JawOpenMth
		JawOpenRef20 -1
		JawOpenMthShapes 1
	'''
	JawOpenMthBsn = mc.blendShape(("JawOpenRef%s%s" % (jawMthRotateX, elem)), "JawOpenMthShapes%s" % elem, "JawOpenMth%s" % elem,n="%sBsn" % bshList[9])[0]
	mc.setAttr("%sBsn" % bshList[9]+"."+("JawOpenRef%s%s" % (jawMthRotateX, elem)),-1)
	mc.setAttr("%sBsn" % bshList[9]+"."+"JawOpenMthShapes%s" % elem,1)


	
# def proxyCorCrtMthBsn(	tyOffset, 
# 							jawMthRotateX, 
# 							bodyMthRig=["BodyMthRig_Geo"], 
# 							BodyMthLip=["BodyMthLipRig_Geo"], 
# 							BodyMthJaw=["BodyMthJawRig_Geo"],
# 							*args):
# 	#BodyMthRig_Geo
# 	#BodyMthObj01LipRig_Geo
# 	#BodyMthObj02JawRig_Geo
	
# 	headOrig = ["Orig", ("JawOpenRef%s" % jawMthRotateX)]
# 	crtMthShapes = ["JawOpenMthShapes", "JawOpenShapes"]
# 	crtBshShapes = ["CnrUpOpenRefShapes", "CnrDnOpenRefShapes", "CnrInOpenShapes", "CnrOutOpenShapes", "CnrInUpOpenShapes", "CnrInDnOpenShapes", "CnrOutUpOpenShapes", "CnrOutDnOpenShapes"]          
# 	crtBsh = ["JawOpenMth", "JawOpen", "CnrUpOpen", "CnrDnOpen", "CnrInOpen", "CnrOutOpen", "CnrInUpOpen", "CnrInDnOpen", "CnrOutUpOpen", "CnrOutDnOpen"]      
# 	crtPuff =  ["LipUpPuffMthRigShapes", "LipLowPuffMthRigShapes", "LipUpThickMthRigShapes", "LipLowThickMthRigShapes"]
# 	mthctrl = mc.ls("*:JawMthRig_Ctrl")
# 	# headJaw = mc.ls("BodyMthJawRig_Geo")
# 	# headMth = mc.ls("BodyMthRig_Geo")
# 	headGeo =[BodyMthLip,BodyMthJaw]
# 	headGeoUnlock = (bodyMthRig+BodyMthLip+BodyMthJaw)
# 	print headGeo
# 	orig_Grp = mc.createNode("transform", n = "orig_Grp")
# 	crtBsh_Grp = mc.createNode('transform',n = 'crtBsh_Grp')
# 	crtBshShapes_Grps = mc.createNode('transform',n = 'crtBshShapes_Grp')
# 	crtBshCon_Grps = mc.createNode('transform',n = 'crtBshCon_Grp')
# 	crtPuff_Grps = mc.createNode('transform',n = 'crtPuff_Grp')
# 	attr = ["tx", "ty", "tz", "rx", "ry", "rz", "sx", "sy", "sz"]
# 	# unlock headJaw
# 	for each in headGeo:
# 		for attrs in attr:
# 			mc.setAttr("%s.%s" % (each, attrs), l = False);
# 	# HeadOriShapes and JawShapes(valueRotate)
# 	headBshOrig = mc.duplicate(BodyMthJaw, n = "%s" % headOrig[0])
# 	mc.setAttr("%s.rotateX" % (mthctrl[0]),jawMthRotateX)
# 	headJawOprnRef = mc.duplicate(BodyMthJaw, n = "%s" % headOrig[1])


# 	# Duplicate crtBshShapes
# 	crtMthShapesAmount = len(crtMthShapes)
# 	crtBshShapesAmount = len(crtBshShapes)
# 	crtBshAmount = len(crtBsh)
# 	crtPuffAmount = len(crtPuff)

# 	# move Orig, jawOpen
# 	defaultTyEach = 0
	
# 	for each in headOrig:
# 		for attrs in attr:
# 			mc.setAttr("%s.%s" % (each, attrs), l = False);
# 			# mc.setAttr("%s.translateX" % (each),defaultTyEach)
# 			defaultTyEach += 3
# 		mc.parent (each, orig_Grp)


# 	# for crtBshShapess in crtBshShapes:
# 	for i in range(crtMthShapesAmount):

# 		bshShapes = mc.duplicate(BodyMthJaw, n = "%s" % crtMthShapes[i])
# 		mc.parent(crtMthShapes[i], crtBshShapes_Grps)
# 	for i in range(crtBshShapesAmount):
# 		bshShapes = mc.duplicate(headOrig[0], n = "%s" % crtBshShapes[i])
# 		mc.parent(crtBshShapes[i], crtBshShapes_Grps)
# 	for i in range(crtBshAmount):
# 		bshShapes = mc.duplicate(headOrig[0], n = "%s" % crtBsh[i])
# 		mc.parent(crtBsh[i], crtBshCon_Grps)
# 	for i in range(crtPuffAmount):
# 		bshShapes = mc.duplicate(headOrig[0], n = "%s" % crtPuff[i])
# 		mc.parent(crtPuff[i], crtPuff_Grps)



# 	# setAttribute JawOpen Default
# 	mc.setAttr("%s.rotateX" % (mthctrl[0]),0)
# 	mc.setAttr ("%sShape.jawOpen" % mthctrl[0], jawMthRotateX);
# 	# parent to crtBsh_Grp
# 	mc.parent(crtBshShapes_Grps, crtBshCon_Grps, crtPuff_Grps, orig_Grp, crtBsh_Grp)
# 	mc.setAttr("%s.translateX" % (crtBshCon_Grps),-3.5*int(tyOffset))
# 	mc.setAttr("%s.translateX" % (crtBshShapes_Grps),-2.5*int(tyOffset))
# 	mc.setAttr("%s.translateX" % (crtPuff_Grps),-1.5*int(tyOffset))

# 	mc.setAttr("%s.translateX" % (headOrig[0]),1.5*jawMthRotateX)
# 	mc.setAttr("%s.translateX" % (headOrig[1]),3.5*jawMthRotateX)


# 	crtBshShapesTotal = (crtMthShapes+crtBshShapes)
# 	# print crtBshShapesTotal
# 	# translateY 
# 	crtBshShapes_GrpsAmount = len(crtBshShapesTotal)
# 	crtBshCon_GrpsAmount = len(crtBsh)
# 	crtPuff_GrpsAmount = len(crtPuff)
# 	defaultTy = 0
# 	for i in range(crtBshShapes_GrpsAmount):
# 		mc.setAttr("%s.translateY" % (crtBshShapesTotal[i]), defaultTy)
# 		defaultTy += tyOffset
# 	defaultTy = 0
# 	for i in range(crtBshCon_GrpsAmount):
# 		mc.setAttr("%s.translateY" % (crtBsh[i]), defaultTy)
# 		defaultTy += tyOffset
# 	defaultTy = 0
# 	for i in range(crtPuff_GrpsAmount):
# 		mc.setAttr("%s.translateY" % (crtPuff[i]), defaultTy)
# 		defaultTy += tyOffset
#  	mc.select(cl=True)
# 	# create BlendShape Shapestive Mouth

	
# 	bshList = ["CnrOutDnOpen", "CnrOutUpOpen", "CnrInDnOpen", "CnrInUpOpen", "CnrOutOpen", "CnrInOpen", "CnrDnOpen", "CnrUpOpen", "JawOpen", "JawOpenMth"]
# 	# proxhList = ["cornerOuterDn" , "cornerOuterUp", "cornerInnerDn", "cornerInnerUp", "cornerOut", "cornerIn", "cornerDn", "cornerUp", ""]
# 	# ShapesList = ["CnrOutDnOpenShapes", "CnrOutUpOpenShapes", "CnrInDnOpenShapes", "CnrInUpOpenShapes", "CnrOutOpenShapes", "CnrInOpenShapes", "CnrDnOpenRefShapes", "CnrUpOpenRefShapes"]
# 	# jawOpenRefNum = ("JawOpenRef20")
# 	# jawOpenShapesList = ("JawOpenShapes")
	
# 	'''1
# 	CnrOutDnOpen
# 		JawOpenShapes -1
# 		cornerOuterDn -1
# 		CnrOutDnOpenShapes 1
# 	'''
# 	print bshList
# 	CnrOutDnOpenBsn = mc.blendShape("JawOpenShapes", "cornerOuterDn", "CnrOutDnOpenShapes", "CnrOutDnOpen",n="%sBsn" % bshList[0])[0]
# 	mc.setAttr("%sBsn" % bshList[0]+"."+"JawOpenShapes",-1)
# 	mc.setAttr("%sBsn" % bshList[0]+"."+"cornerOuterDn",-1)
# 	mc.setAttr("%sBsn" % bshList[0]+"."+"CnrOutDnOpenShapes",1)

# 	CnrOutDnOpenShapesBsn = mc.blendShape("JawOpenShapes", "cornerOuterDn", "CnrOutDnOpenShapes", n="%sShapesBsn" % bshList[0])[0]
# 	mc.setAttr("%sShapesBsn" % bshList[0]+"."+"JawOpenShapes",1)
# 	mc.setAttr("%sShapesBsn" % bshList[0]+"."+"cornerOuterDn",1)
# 	# mc.setAttr("%sBsn" % bshList[0]+"."+"CnrOutDnOpenShapes",1)
# 	'''2
# 	CnrOutUpOpen
# 		JawOpenShapes -1
# 		cornerOuterUp -1
# 		CnrOutUpOpenShapes 1
# 	'''
# 	CnrOutUpOpenBsn = mc.blendShape("JawOpenShapes", "cornerOuterUp", "CnrOutUpOpenShapes", "CnrOutUpOpen",n="%sBsn" % bshList[1])[0]
# 	mc.setAttr("%sBsn" % bshList[1]+"."+"JawOpenShapes",-1)
# 	mc.setAttr("%sBsn" % bshList[1]+"."+"cornerOuterUp",-1)
# 	mc.setAttr("%sBsn" % bshList[1]+"."+"CnrOutUpOpenShapes",1)

# 	CnrOutUpOpenShapesBsn = mc.blendShape("JawOpenShapes", "cornerOuterUp", "CnrOutUpOpenShapes", n="%sShapesBsn" % bshList[1])[0]
# 	mc.setAttr("%sShapesBsn" % bshList[1]+"."+"JawOpenShapes",1)
# 	mc.setAttr("%sShapesBsn" % bshList[1]+"."+"cornerOuterUp",1)
# 	# mc.setAttr("%sShapesBsn" % bshList[1]+"."+"CnrOutUpOpenShapes",1)
# 	'''3
# 	CnrInDnOpen
# 		JawOpenShapes -1
# 		cornerInnerDn -1
# 		cnrInDnOpenShapes 1
# 	'''
# 	CnrInDnOpenBsn = mc.blendShape("JawOpenShapes", "cornerInnerDn", "CnrInDnOpenShapes", "CnrInDnOpen",n="%sBsn" % bshList[2])[0]
# 	mc.setAttr("%sBsn" % bshList[2]+"."+"JawOpenShapes",-1)
# 	mc.setAttr("%sBsn" % bshList[2]+"."+"cornerInnerDn",-1)
# 	mc.setAttr("%sBsn" % bshList[2]+"."+"CnrInDnOpenShapes",1)

# 	CnrInDnOpenShapesBsn = mc.blendShape("JawOpenShapes", "cornerInnerDn", "CnrInDnOpenShapes", n="%sShapesBsn" % bshList[2])[0]
# 	mc.setAttr("%sShapesBsn" % bshList[2]+"."+"JawOpenShapes",1)
# 	mc.setAttr("%sShapesBsn" % bshList[2]+"."+"cornerInnerDn",1)
# 	# mc.setAttr("%sShapesBsn" % bshList[2]+"."+"CnrInDnOpenShapes",1)
# 	'''4
# 	CnrInUpOpen
# 		JawOpenShapes -1
# 		cornerInnerUp -1
# 		CnrInUpOpenShapes 1
# 	'''
# 	CnrInUpOpenBsn = mc.blendShape("JawOpenShapes", "cornerInnerUp", "CnrInUpOpenShapes", "CnrInUpOpen",n="%sBsn" % bshList[3])[0]
# 	mc.setAttr("%sBsn" % bshList[3]+"."+"JawOpenShapes",-1)
# 	mc.setAttr("%sBsn" % bshList[3]+"."+"cornerInnerUp",-1)
# 	mc.setAttr("%sBsn" % bshList[3]+"."+"CnrInUpOpenShapes",1)

# 	CnrInUpOpenBsn = mc.blendShape("JawOpenShapes", "cornerInnerUp", "CnrInUpOpenShapes", n="%sShapesBsn" % bshList[3])[0]
# 	mc.setAttr("%sShapesBsn" % bshList[3]+"."+"JawOpenShapes",1)
# 	mc.setAttr("%sShapesBsn" % bshList[3]+"."+"cornerInnerUp",1)
# 	# mc.setAttr("%sShapesBsn" % bshList[3]+"."+"CnrInUpOpenShapes",1)
# 	'''5
# 	CnrOutOpen
# 		JawOpenShapes -1
# 		cornerOut -1
# 		CnrOutOpenShapes 1
# 	'''
# 	CnrOutOpenBsn = mc.blendShape("JawOpenShapes", "cornerOut", "CnrOutOpenShapes", "CnrOutOpen",n="%sBsn" % bshList[4])[0]
# 	mc.setAttr("%sBsn" % bshList[4]+"."+"JawOpenShapes",-1)
# 	mc.setAttr("%sBsn" % bshList[4]+"."+"cornerOut",-1)
# 	mc.setAttr("%sBsn" % bshList[4]+"."+"CnrOutOpenShapes",1)

# 	CnrOutOpenShapesBsn = mc.blendShape("JawOpenShapes", "cornerOut", "CnrOutOpenShapes", n="%sShapesBsn" % bshList[4])[0]
# 	mc.setAttr("%sShapesBsn" % bshList[4]+"."+"JawOpenShapes",1)
# 	mc.setAttr("%sShapesBsn" % bshList[4]+"."+"cornerOut",1)
# 	# mc.setAttr("%sShapessn" % bshList[4]+"."+"CnrOutOpenShapes",1)
# 	'''6
# 	CnrInOpen
# 		JawOpenShapes -1
# 		cornerIn -1
# 		CnrInOpenShapes 1
# 	'''
# 	CnrInOpenBsn = mc.blendShape("JawOpenShapes", "cornerIn", "CnrInOpenShapes", "CnrInOpen",n="%sBsn" % bshList[5])[0]
# 	mc.setAttr("%sBsn" % bshList[5]+"."+"JawOpenShapes",-1)
# 	mc.setAttr("%sBsn" % bshList[5]+"."+"cornerIn",-1)
# 	mc.setAttr("%sBsn" % bshList[5]+"."+"CnrInOpenShapes",1)

# 	CnrInOpenShapesBsn = mc.blendShape("JawOpenShapes", "cornerIn", "CnrInOpenShapes", n="%sShapesBsn" % bshList[5])[0]
# 	mc.setAttr("%sShapesBsn" % bshList[5]+"."+"JawOpenShapes",1)
# 	mc.setAttr("%sShapesBsn" % bshList[5]+"."+"cornerIn",1)
# 	# mc.setAttr("%sShapesBsn" % bshList[5]+"."+"CnrInOpenShapes",1)
# 	'''7
# 	CnrDnOpen
# 		JawOpenRef20 -1
# 		cornerDn -1
# 		CnrDnOpenRefShapes 1
# 	'''
# 	CnrDnOpenBsn = mc.blendShape(("JawOpenRef%s" % jawMthRotateX), "cornerDn", "CnrDnOpenRefShapes", "CnrDnOpen",n="%sBsn" % bshList[6])[0]
# 	mc.setAttr("%sBsn" % bshList[6]+"."+("JawOpenRef%s" % jawMthRotateX),-1)
# 	mc.setAttr("%sBsn" % bshList[6]+"."+"cornerDn",-1)
# 	mc.setAttr("%sBsn" % bshList[6]+"."+"CnrDnOpenRefShapes",1)

# 	CnrDnOpenRefShapesBsn = mc.blendShape(("JawOpenRef%s" % jawMthRotateX), "cornerDn", "CnrDnOpenRefShapes",n="%sBsn" % crtBshShapesTotal[3])[0]
# 	mc.setAttr("%sBsn" % crtBshShapesTotal[3]+"."+("JawOpenRef%s" % jawMthRotateX),1)
# 	mc.setAttr("%sBsn" % crtBshShapesTotal[3]+"."+"cornerDn",1)
# 	# mc.setAttr("%sBsn" % bshList[6]+"."+"CnrDnOpenRefShapes",1)
# 	'''8
# 	CnrUpOpen
# 		JawOpenRef20 -1
# 		cornerUp -1
# 		CnrUpOpenRefShapes 1
# 	'''
# 	CnrUpOpenBsn = mc.blendShape(("JawOpenRef%s" % jawMthRotateX), "cornerUp", "CnrUpOpenRefShapes", "CnrUpOpen",n="%sBsn" % bshList[7])[0]
# 	mc.setAttr("%sBsn" % bshList[7]+"."+("JawOpenRef%s" % jawMthRotateX),-1)
# 	mc.setAttr("%sBsn" % bshList[7]+"."+"cornerUp",-1)
# 	mc.setAttr("%sBsn" % bshList[7]+"."+"CnrUpOpenRefShapes",1)

# 	CnrUpOpenRefShapesBsn = mc.blendShape(("JawOpenRef%s" % jawMthRotateX), "cornerUp", "CnrUpOpenRefShapes",n="%sBsn" % crtBshShapesTotal[2])[0]
# 	mc.setAttr("%sBsn" % crtBshShapesTotal[2]+"."+("JawOpenRef%s" % jawMthRotateX),1)
# 	mc.setAttr("%sBsn" % crtBshShapesTotal[2]+"."+"cornerUp",1)
# 	# mc.setAttr("%sBsn" % bshList[7]+"."+"CnrUpOpenRefShapes",1)

# 	'''9
# 	JawOpen
# 		JawOpenRef20 -1
# 		JawOpenShapes 1
# 	'''
# 	JawOpenBsn = mc.blendShape(("JawOpenRef%s" % jawMthRotateX), "JawOpenShapes", "JawOpen",n="%sBsn" % bshList[8])[0]
# 	mc.setAttr("%sBsn" % bshList[8]+"."+("JawOpenRef%s" % jawMthRotateX),-1)
# 	mc.setAttr("%sBsn" % bshList[8]+"."+"JawOpenShapes",1)

# 	# JawOpenShapesBsn = mc.blendShape(("JawOpenRef%s" % jawMthRotateX), "JawOpenShapes", "JawOpen",n="%sBsn" % bshList[8])[0]
# 	# mc.setAttr("%sBsn" % bshList[8]+"."+("JawOpenRef%s" % jawMthRotateX),-1)
# 	# mc.setAttr("%sBsn" % bshList[8]+"."+"JawOpenShapes",1)
# 	# JawOpenShape
# 	'''10
# 	JawOpenMth
# 		JawOpenRef20 -1
# 		JawOpenMthShapes 1
# 	'''
# 	JawOpenMthBsn = mc.blendShape(("JawOpenRef%s" % jawMthRotateX), "JawOpenMthShapes", "JawOpenMth",n="%sBsn" % bshList[9])[0]
# 	mc.setAttr("%sBsn" % bshList[9]+"."+("JawOpenRef%s" % jawMthRotateX),-1)
# 	mc.setAttr("%sBsn" % bshList[9]+"."+"JawOpenMthShapes",1)



#-----------------#
# Shapes = Shape  #
# Obj01 Geo         #
#-----------------#
def proxyCorCrtMthObj01Bsn(	tyOffset, 
						jawMthRotateX, 
						bodyMthRig=["BodyMthRig_Geo"], 
						BodyMthLip=["BodyMthLipRig_Geo"], 
						BodyMthJaw=["BodyMthJawRig_Geo"],
						elem = '',
						*args):
	#BodyMthRig_Geo
	#BodyMthLipRig_Geo
	#BodyMthJawRig_Geo
	
	headOrig = ["Orig%s" % elem, ("JawOpenRef%s%s" % (jawMthRotateX, elem))]
	crtMthShapes = ["JawOpenMthShapes%s" % elem, "JawOpenShapes%s" % elem]
	crtBshShapes = ["CnrUpOpenRefShapes%s" % elem, "CnrDnOpenRefShapes%s" % elem, "CnrInOpenShapes%s" % elem, "CnrOutOpenShapes%s" % elem, "CnrInUpOpenShapes%s" % elem, "CnrInDnOpenShapes%s" % elem, "CnrOutUpOpenShapes%s" % elem, "CnrOutDnOpenShapes%s" % elem]          
	crtBsh = ["JawOpenMth%s" % elem, "JawOpen%s" % elem, "CnrUpOpen%s" % elem, "CnrDnOpen%s" % elem, "CnrInOpen%s" % elem, "CnrOutOpen%s" % elem, "CnrInUpOpen%s" % elem, "CnrInDnOpen%s" % elem, "CnrOutUpOpen%s" % elem, "CnrOutDnOpen%s" % elem]      
	crtPuff =  ["LipUpPuffMthRigShapes%s" % elem, "LipLowPuffMthRigShapes%s" % elem, "LipUpThickMthRigShapes%s" % elem, "LipLowThickMthRigShapes%s" % elem]
	mthctrl = mc.ls("*:JawMthRig_Ctrl")
	# headJaw = mc.ls("BodyMthJawRig_Geo")
	# headMth = mc.ls("BodyMthRig_Geo")
	headGeo =[BodyMthLip,BodyMthJaw]
	headGeoUnlock = (bodyMthRig+BodyMthLip+BodyMthJaw)
	print headGeo
	orig_Grp = mc.createNode("transform", n = "orig%s_Grp" % elem)
	crtBsh_Grp = mc.createNode('transform',n = 'crtBsh%s_Grp' % elem )
	crtBshShapes_Grps = mc.createNode('transform',n = 'crtBshShapes%s_Grp' % elem)
	crtBshCon_Grps = mc.createNode('transform',n = 'crtBshCon%s_Grp' % elem)
	crtPuff_Grps = mc.createNode('transform',n = 'crtPuff%s_Grp' % elem)
	attr = ["tx", "ty", "tz", "rx", "ry", "rz", "sx", "sy", "sz"]
	# unlock headJaw
	for each in headGeo:
		for attrs in attr:
			mc.setAttr("%s.%s" % (each, attrs), l = False);
	# HeadOriShapes and JawShapes(valueRotate)
	headBshOrig = mc.duplicate(BodyMthJaw, n = "%s" % headOrig[0])
	mc.setAttr("%s.rotateX" % (mthctrl[0]),jawMthRotateX)
	headJawOprnRef = mc.duplicate(BodyMthJaw, n = "%s" % headOrig[1])


	# Duplicate crtBshShapes
	crtMthShapesAmount = len(crtMthShapes)
	crtBshShapesAmount = len(crtBshShapes)
	crtBshAmount = len(crtBsh)
	crtPuffAmount = len(crtPuff)

	# move Orig, jawOpen
	defaultTyEach = 0
	
	for each in headOrig:
		for attrs in attr:
			mc.setAttr("%s.%s" % (each, attrs), l = False);
			# mc.setAttr("%s.translateX" % (each),defaultTyEach)
			defaultTyEach += 3
		mc.parent (each, orig_Grp)


	# for crtBshShapess in crtBshShapes:
	for i in range(crtMthShapesAmount):

		bshShapes = mc.duplicate(BodyMthJaw, n = "%s" % crtMthShapes[i])
		mc.parent(crtMthShapes[i], crtBshShapes_Grps)
	for i in range(crtBshShapesAmount):
		bshShapes = mc.duplicate(headOrig[0], n = "%s" % crtBshShapes[i])
		mc.parent(crtBshShapes[i], crtBshShapes_Grps)
	for i in range(crtBshAmount):
		bshShapes = mc.duplicate(headOrig[0], n = "%s" % crtBsh[i])
		mc.parent(crtBsh[i], crtBshCon_Grps)
	for i in range(crtPuffAmount):
		bshShapes = mc.duplicate(headOrig[0], n = "%s" % crtPuff[i])
		mc.parent(crtPuff[i], crtPuff_Grps)



	# setAttribute JawOpen Default
	mc.setAttr("%s.rotateX" % (mthctrl[0]),0)
	mc.setAttr ("%sShape.jawOpen" % mthctrl[0], jawMthRotateX);
	# parent to crtBsh_Grp
	mc.parent(crtBshShapes_Grps, crtBshCon_Grps, crtPuff_Grps, orig_Grp, crtBsh_Grp)
	mc.setAttr("%s.translateX" % (crtBshCon_Grps),-3.5*int(tyOffset))
	mc.setAttr("%s.translateX" % (crtBshShapes_Grps),-2.5*int(tyOffset))
	mc.setAttr("%s.translateX" % (crtPuff_Grps),-1.5*int(tyOffset))

	mc.setAttr("%s.translateX" % (headOrig[0]),1.5*jawMthRotateX)
	mc.setAttr("%s.translateX" % (headOrig[1]),3.5*jawMthRotateX)


	crtBshShapesTotal = (crtMthShapes+crtBshShapes)
	# print crtBshShapesTotal
	# translateY 
	crtBshShapes_GrpsAmount = len(crtBshShapesTotal)
	crtBshCon_GrpsAmount = len(crtBsh)
	crtPuff_GrpsAmount = len(crtPuff)
	defaultTy = 0
	for i in range(crtBshShapes_GrpsAmount):
		mc.setAttr("%s.translateY" % (crtBshShapesTotal[i]), defaultTy)
		defaultTy += tyOffset
	defaultTy = 0
	for i in range(crtBshCon_GrpsAmount):
		mc.setAttr("%s.translateY" % (crtBsh[i]), defaultTy)
		defaultTy += tyOffset
	defaultTy = 0
	for i in range(crtPuff_GrpsAmount):
		mc.setAttr("%s.translateY" % (crtPuff[i]), defaultTy)
		defaultTy += tyOffset
 	mc.select(cl=True)
	# create BlendShape Shapestive Mouth

	
	bshList = ["CnrOutDnOpen%s" % elem, "CnrOutUpOpen%s" % elem, "CnrInDnOpen%s" % elem, "CnrInUpOpen%s" % elem, "CnrOutOpen%s" % elem, "CnrInOpen%s" % elem, "CnrDnOpen%s" % elem, "CnrUpOpen%s" % elem, "JawOpen%s" % elem, "JawOpenMth%s" % elem]
	# proxhList = ["cornerOuterDn" , "cornerOuterUp", "cornerInnerDn", "cornerInnerUp", "cornerOut", "cornerIn", "cornerDn", "cornerUp", ""]
	# ShapesList = ["CnrOutDnOpenShapes", "CnrOutUpOpenShapes", "CnrInDnOpenShapes", "CnrInUpOpenShapes", "CnrOutOpenShapes", "CnrInOpenShapes", "CnrDnOpenRefShapes", "CnrUpOpenRefShapes"]
	# jawOpenRefNum = ("JawOpenRef20")
	# jawOpenShapesList = ("JawOpenShapes")
	
	'''1
	CnrOutDnOpen
		JawOpenShapes -1
		cornerOuterDn -1
		CnrOutDnOpenShapes 1
	'''
	print bshList
	CnrOutDnOpenBsn = mc.blendShape("JawOpenShapes%s" % elem, "cornerOuterDn%s" % elem, "CnrOutDnOpenShapes%s" % elem, "CnrOutDnOpen%s" % elem,n="%sBsn" % bshList[0])[0]
	mc.setAttr("%sBsn" % bshList[0]+"."+"JawOpenShapes%s" % elem,-1)
	mc.setAttr("%sBsn" % bshList[0]+"."+"cornerOuterDn%s" % elem,-1)
	mc.setAttr("%sBsn" % bshList[0]+"."+"CnrOutDnOpenShapes%s" % elem,1)

	CnrOutDnOpenShapesBsn = mc.blendShape("JawOpenShapes%s" % elem, "cornerOuterDn%s" % elem, "CnrOutDnOpenShapes%s" % elem, n="%sShapesBsn" % bshList[0])[0]
	mc.setAttr("%sShapesBsn" % bshList[0]+"."+"JawOpenShapes%s" % elem,1)
	mc.setAttr("%sShapesBsn" % bshList[0]+"."+"cornerOuterDn%s" % elem,1)
	# mc.setAttr("%sBsn" % bshList[0]+"."+"CnrOutDnOpenShapes",1)
	'''2
	CnrOutUpOpen
		JawOpenShapes -1
		cornerOuterUp -1
		CnrOutUpOpenShapes 1
	'''
	CnrOutUpOpenBsn = mc.blendShape("JawOpenShapes%s" % elem, "cornerOuterUp%s" % elem, "CnrOutUpOpenShapes%s" % elem, "CnrOutUpOpen%s" % elem,n="%sBsn" % bshList[1])[0]
	mc.setAttr("%sBsn" % bshList[1]+"."+"JawOpenShapes%s" % elem,-1)
	mc.setAttr("%sBsn" % bshList[1]+"."+"cornerOuterUp%s" % elem,-1)
	mc.setAttr("%sBsn" % bshList[1]+"."+"CnrOutUpOpenShapes%s" % elem,1)

	CnrOutUpOpenShapesBsn = mc.blendShape("JawOpenShapes%s" % elem, "cornerOuterUp%s" % elem, "CnrOutUpOpenShapes%s" % elem, n="%sShapesBsn" % bshList[1])[0]
	mc.setAttr("%sShapesBsn" % bshList[1]+"."+"JawOpenShapes%s" % elem,1)
	mc.setAttr("%sShapesBsn" % bshList[1]+"."+"cornerOuterUp%s" % elem,1)
	# mc.setAttr("%sShapesBsn" % bshList[1]+"."+"CnrOutUpOpenShapes",1)
	'''3
	CnrInDnOpen
		JawOpenShapes -1
		cornerInnerDn -1
		cnrInDnOpenShapes 1
	'''
	CnrInDnOpenBsn = mc.blendShape("JawOpenShapes%s" % elem, "cornerInnerDn%s" % elem, "CnrInDnOpenShapes%s" % elem, "CnrInDnOpen%s" % elem,n="%sBsn" % bshList[2])[0]
	mc.setAttr("%sBsn" % bshList[2]+"."+"JawOpenShapes%s" % elem,-1)
	mc.setAttr("%sBsn" % bshList[2]+"."+"cornerInnerDn%s" % elem,-1)
	mc.setAttr("%sBsn" % bshList[2]+"."+"CnrInDnOpenShapes%s" % elem,1)

	CnrInDnOpenShapesBsn = mc.blendShape("JawOpenShapes%s" % elem, "cornerInnerDn%s" % elem, "CnrInDnOpenShapes%s" % elem, n="%sShapesBsn" % bshList[2])[0]
	mc.setAttr("%sShapesBsn" % bshList[2]+"."+"JawOpenShapes%s" % elem,1)
	mc.setAttr("%sShapesBsn" % bshList[2]+"."+"cornerInnerDn%s" % elem,1)
	# mc.setAttr("%sShapesBsn" % bshList[2]+"."+"CnrInDnOpenShapes",1)
	'''4
	CnrInUpOpen
		JawOpenShapes -1
		cornerInnerUp -1
		CnrInUpOpenShapes 1
	'''
	CnrInUpOpenBsn = mc.blendShape("JawOpenShapes%s" % elem, "cornerInnerUp%s" % elem, "CnrInUpOpenShapes%s" % elem, "CnrInUpOpen%s" % elem,n="%sBsn" % bshList[3])[0]
	mc.setAttr("%sBsn" % bshList[3]+"."+"JawOpenShapes%s" % elem,-1)
	mc.setAttr("%sBsn" % bshList[3]+"."+"cornerInnerUp%s" % elem,-1)
	mc.setAttr("%sBsn" % bshList[3]+"."+"CnrInUpOpenShapes%s" % elem,1)

	CnrInUpOpenBsn = mc.blendShape("JawOpenShapes%s" % elem, "cornerInnerUp%s" % elem, "CnrInUpOpenShapes%s" % elem, n="%sShapesBsn" % bshList[3])[0]
	mc.setAttr("%sShapesBsn" % bshList[3]+"."+"JawOpenShapes%s" % elem,1)
	mc.setAttr("%sShapesBsn" % bshList[3]+"."+"cornerInnerUp%s" % elem,1)
	# mc.setAttr("%sShapesBsn" % bshList[3]+"."+"CnrInUpOpenShapes",1)
	'''5
	CnrOutOpen
		JawOpenShapes -1
		cornerOut -1
		CnrOutOpenShapes 1
	'''
	CnrOutOpenBsn = mc.blendShape("JawOpenShapes%s" % elem, "cornerOut%s" % elem, "CnrOutOpenShapes%s" % elem, "CnrOutOpen%s" % elem,n="%sBsn" % bshList[4])[0]
	mc.setAttr("%sBsn" % bshList[4]+"."+"JawOpenShapes%s" % elem,-1)
	mc.setAttr("%sBsn" % bshList[4]+"."+"cornerOut%s" % elem,-1)
	mc.setAttr("%sBsn" % bshList[4]+"."+"CnrOutOpenShapes%s" % elem,1)

	CnrOutOpenShapesBsn = mc.blendShape("JawOpenShapes%s" % elem, "cornerOut%s" % elem, "CnrOutOpenShapes%s" % elem, n="%sShapesBsn" % bshList[4])[0]
	mc.setAttr("%sShapesBsn" % bshList[4]+"."+"JawOpenShapes%s" % elem,1)
	mc.setAttr("%sShapesBsn" % bshList[4]+"."+"cornerOut%s" % elem,1)
	# mc.setAttr("%sShapessn" % bshList[4]+"."+"CnrOutOpenShapes",1)
	'''6
	CnrInOpen
		JawOpenShapes -1
		cornerIn -1
		CnrInOpenShapes 1
	'''
	CnrInOpenBsn = mc.blendShape("JawOpenShapes%s" % elem, "cornerIn%s" % elem, "CnrInOpenShapes%s" % elem, "CnrInOpen%s" % elem,n="%sBsn" % bshList[5])[0]
	mc.setAttr("%sBsn" % bshList[5]+"."+"JawOpenShapes%s" % elem,-1)
	mc.setAttr("%sBsn" % bshList[5]+"."+"cornerIn%s" % elem,-1)
	mc.setAttr("%sBsn" % bshList[5]+"."+"CnrInOpenShapes%s" % elem,1)

	CnrInOpenShapesBsn = mc.blendShape("JawOpenShapes%s" % elem, "cornerIn%s" % elem, "CnrInOpenShapes%s" % elem, n="%sShapesBsn" % bshList[5])[0]
	mc.setAttr("%sShapesBsn" % bshList[5]+"."+"JawOpenShapes%s" % elem,1)
	mc.setAttr("%sShapesBsn" % bshList[5]+"."+"cornerIn%s" % elem,1)
	# mc.setAttr("%sShapesBsn" % bshList[5]+"."+"CnrInOpenShapes",1)
	'''7
	CnrDnOpen
		JawOpenRef20 -1
		cornerDn -1
		CnrDnOpenRefShapes 1
	'''
	CnrDnOpenBsn = mc.blendShape(("JawOpenRef%s%s" % (jawMthRotateX, elem)), "cornerDn%s" % elem, "CnrDnOpenRefShapes%s" % elem, "CnrDnOpen%s" % elem,n="%sBsn" % bshList[6])[0]
	mc.setAttr("%sBsn" % bshList[6]+"."+("JawOpenRef%s%s" % (jawMthRotateX, elem)),-1)
	mc.setAttr("%sBsn" % bshList[6]+"."+"cornerDn%s" % elem,-1)
	mc.setAttr("%sBsn" % bshList[6]+"."+"CnrDnOpenRefShapes%s" % elem,1)

	CnrDnOpenRefShapesBsn = mc.blendShape(("JawOpenRef%s%s" % (jawMthRotateX, elem)), "cornerDn%s" % elem, "CnrDnOpenRefShapes%s" % elem,n="%sBsn" % crtBshShapesTotal[3])[0]
	mc.setAttr("%sBsn" % crtBshShapesTotal[3]+"."+("JawOpenRef%s%s" % (jawMthRotateX, elem)),1)
	mc.setAttr("%sBsn" % crtBshShapesTotal[3]+"."+"cornerDn%s" % elem,1)
	# mc.setAttr("%sBsn" % bshList[6]+"."+"CnrDnOpenRefShapes",1)
	'''8
	CnrUpOpen
		JawOpenRef20 -1
		cornerUp -1
		CnrUpOpenRefShapes 1
	'''
	CnrUpOpenBsn = mc.blendShape(("JawOpenRef%s%s" % (jawMthRotateX, elem)), "cornerUp%s" % elem, "CnrUpOpenRefShapes%s" % elem, "CnrUpOpen%s" % elem,n="%sBsn" % bshList[7])[0]
	mc.setAttr("%sBsn" % bshList[7]+"."+("JawOpenRef%s%s" % (jawMthRotateX, elem)),-1)
	mc.setAttr("%sBsn" % bshList[7]+"."+"cornerUp%s" % elem,-1)
	mc.setAttr("%sBsn" % bshList[7]+"."+"CnrUpOpenRefShapes%s" % elem,1)

	CnrUpOpenRefShapesBsn = mc.blendShape(("JawOpenRef%s%s" % (jawMthRotateX, elem)), "cornerUp%s" % elem, "CnrUpOpenRefShapes%s" % elem,n="%sBsn" % crtBshShapesTotal[2])[0]
	mc.setAttr("%sBsn" % crtBshShapesTotal[2]+"."+("JawOpenRef%s%s" % (jawMthRotateX, elem)),1)
	mc.setAttr("%sBsn" % crtBshShapesTotal[2]+"."+"cornerUp%s" % elem,1)
	# mc.setAttr("%sBsn" % bshList[7]+"."+"CnrUpOpenRefShapes",1)

	'''9
	JawOpen
		JawOpenRef20 -1
		JawOpenShapes 1
	'''
	JawOpenBsn = mc.blendShape(("JawOpenRef%s%s" % (jawMthRotateX, elem)), "JawOpenShapes%s" % elem, "JawOpen%s" % elem,n="%sBsn" % bshList[8])[0]
	mc.setAttr("%sBsn" % bshList[8]+"."+("JawOpenRef%s%s" % (jawMthRotateX, elem)),-1)
	mc.setAttr("%sBsn" % bshList[8]+"."+"JawOpenShapes%s" % elem,1)

	# JawOpenShapesBsn = mc.blendShape(("JawOpenRef%s" % jawMthRotateX), "JawOpenShapes", "JawOpen",n="%sBsn" % bshList[8])[0]
	# mc.setAttr("%sBsn" % bshList[8]+"."+("JawOpenRef%s" % jawMthRotateX),-1)
	# mc.setAttr("%sBsn" % bshList[8]+"."+"JawOpenShapes",1)
	# JawOpenShape
	'''10
	JawOpenMth
		JawOpenRef20 -1
		JawOpenMthShapes 1
	'''
	JawOpenMthBsn = mc.blendShape(("JawOpenRef%s%s" % (jawMthRotateX, elem)), "JawOpenMthShapes%s" % elem, "JawOpenMth%s" % elem,n="%sBsn" % bshList[9])[0]
	mc.setAttr("%sBsn" % bshList[9]+"."+("JawOpenRef%s%s" % (jawMthRotateX, elem)),-1)
	mc.setAttr("%sBsn" % bshList[9]+"."+"JawOpenMthShapes%s" % elem,1)



#-----------------#
# Shapes = Shape  #
# Obj02 Geo         #
#-----------------#
def proxyCorCrtMthObj02Bsn(	tyOffset, 
						jawMthRotateX, 
						bodyMthRig=["BodyMthRig_Geo"], 
						BodyMthLip=["BodyMthLipRig_Geo"], 
						BodyMthJaw=["BodyMthJawRig_Geo"],
						elem = '',
						*args):
	#BodyMthRig_Geo
	#BodyMthLipRig_Geo
	#BodyMthJawRig_Geo

	headOrig = ["Orig%s" % elem, ("JawOpenRef%s%s" % (jawMthRotateX, elem))]
	crtMthShapes = ["JawOpenMthShapes%s" % elem, "JawOpenShapes%s" % elem]
	crtBshShapes = ["CnrUpOpenRefShapes%s" % elem, "CnrDnOpenRefShapes%s" % elem, "CnrInOpenShapes%s" % elem, "CnrOutOpenShapes%s" % elem, "CnrInUpOpenShapes%s" % elem, "CnrInDnOpenShapes%s" % elem, "CnrOutUpOpenShapes%s" % elem, "CnrOutDnOpenShapes%s" % elem]          
	crtBsh = ["JawOpenMth%s" % elem, "JawOpen%s" % elem, "CnrUpOpen%s" % elem, "CnrDnOpen%s" % elem, "CnrInOpen%s" % elem, "CnrOutOpen%s" % elem, "CnrInUpOpen%s" % elem, "CnrInDnOpen%s" % elem, "CnrOutUpOpen%s" % elem, "CnrOutDnOpen%s" % elem]      
	crtPuff =  ["LipUpPuffMthRigShapes%s" % elem, "LipLowPuffMthRigShapes%s" % elem, "LipUpThickMthRigShapes%s" % elem, "LipLowThickMthRigShapes%s" % elem]
	mthctrl = mc.ls("*:JawMthRig_Ctrl")
	# headJaw = mc.ls("BodyMthJawRig_Geo")
	# headMth = mc.ls("BodyMthRig_Geo")
	headGeo =[BodyMthLip,BodyMthJaw]
	headGeoUnlock = (bodyMthRig+BodyMthLip+BodyMthJaw)
	print headGeo
	orig_Grp = mc.createNode("transform", n = "orig%s_Grp" % elem)
	crtBsh_Grp = mc.createNode('transform',n = 'crtBsh%s_Grp' % elem )
	crtBshShapes_Grps = mc.createNode('transform',n = 'crtBshShapes%s_Grp' % elem)
	crtBshCon_Grps = mc.createNode('transform',n = 'crtBshCon%s_Grp' % elem)
	crtPuff_Grps = mc.createNode('transform',n = 'crtPuff%s_Grp' % elem)
	attr = ["tx", "ty", "tz", "rx", "ry", "rz", "sx", "sy", "sz"]
	# unlock headJaw
	for each in headGeo:
		for attrs in attr:
			mc.setAttr("%s.%s" % (each, attrs), l = False);
	# HeadOriShapes and JawShapes(valueRotate)
	headBshOrig = mc.duplicate(BodyMthJaw, n = "%s" % headOrig[0])
	mc.setAttr("%s.rotateX" % (mthctrl[0]),jawMthRotateX)
	headJawOprnRef = mc.duplicate(BodyMthJaw, n = "%s" % headOrig[1])


	# Duplicate crtBshShapes
	crtMthShapesAmount = len(crtMthShapes)
	crtBshShapesAmount = len(crtBshShapes)
	crtBshAmount = len(crtBsh)
	crtPuffAmount = len(crtPuff)

	# move Orig, jawOpen
	defaultTyEach = 0
	
	for each in headOrig:
		for attrs in attr:
			mc.setAttr("%s.%s" % (each, attrs), l = False);
			# mc.setAttr("%s.translateX" % (each),defaultTyEach)
			defaultTyEach += 3
		mc.parent (each, orig_Grp)


	# for crtBshShapess in crtBshShapes:
	for i in range(crtMthShapesAmount):

		bshShapes = mc.duplicate(BodyMthJaw, n = "%s" % crtMthShapes[i])
		mc.parent(crtMthShapes[i], crtBshShapes_Grps)
	for i in range(crtBshShapesAmount):
		bshShapes = mc.duplicate(headOrig[0], n = "%s" % crtBshShapes[i])
		mc.parent(crtBshShapes[i], crtBshShapes_Grps)
	for i in range(crtBshAmount):
		bshShapes = mc.duplicate(headOrig[0], n = "%s" % crtBsh[i])
		mc.parent(crtBsh[i], crtBshCon_Grps)
	for i in range(crtPuffAmount):
		bshShapes = mc.duplicate(headOrig[0], n = "%s" % crtPuff[i])
		mc.parent(crtPuff[i], crtPuff_Grps)



	# setAttribute JawOpen Default
	mc.setAttr("%s.rotateX" % (mthctrl[0]),0)
	mc.setAttr ("%sShape.jawOpen" % mthctrl[0], jawMthRotateX);
	# parent to crtBsh_Grp
	mc.parent(crtBshShapes_Grps, crtBshCon_Grps, crtPuff_Grps, orig_Grp, crtBsh_Grp)
	mc.setAttr("%s.translateX" % (crtBshCon_Grps),-3.5*int(tyOffset))
	mc.setAttr("%s.translateX" % (crtBshShapes_Grps),-2.5*int(tyOffset))
	mc.setAttr("%s.translateX" % (crtPuff_Grps),-1.5*int(tyOffset))

	mc.setAttr("%s.translateX" % (headOrig[0]),1.5*jawMthRotateX)
	mc.setAttr("%s.translateX" % (headOrig[1]),3.5*jawMthRotateX)


	crtBshShapesTotal = (crtMthShapes+crtBshShapes)
	# print crtBshShapesTotal
	# translateY 
	crtBshShapes_GrpsAmount = len(crtBshShapesTotal)
	crtBshCon_GrpsAmount = len(crtBsh)
	crtPuff_GrpsAmount = len(crtPuff)
	defaultTy = 0
	for i in range(crtBshShapes_GrpsAmount):
		mc.setAttr("%s.translateY" % (crtBshShapesTotal[i]), defaultTy)
		defaultTy += tyOffset
	defaultTy = 0
	for i in range(crtBshCon_GrpsAmount):
		mc.setAttr("%s.translateY" % (crtBsh[i]), defaultTy)
		defaultTy += tyOffset
	defaultTy = 0
	for i in range(crtPuff_GrpsAmount):
		mc.setAttr("%s.translateY" % (crtPuff[i]), defaultTy)
		defaultTy += tyOffset
 	mc.select(cl=True)
	# create BlendShape Shapestive Mouth

	
	bshList = ["CnrOutDnOpen%s" % elem, "CnrOutUpOpen%s" % elem, "CnrInDnOpen%s" % elem, "CnrInUpOpen%s" % elem, "CnrOutOpen%s" % elem, "CnrInOpen%s" % elem, "CnrDnOpen%s" % elem, "CnrUpOpen%s" % elem, "JawOpen%s" % elem, "JawOpenMth%s" % elem]
	# proxhList = ["cornerOuterDn" , "cornerOuterUp", "cornerInnerDn", "cornerInnerUp", "cornerOut", "cornerIn", "cornerDn", "cornerUp", ""]
	# ShapesList = ["CnrOutDnOpenShapes", "CnrOutUpOpenShapes", "CnrInDnOpenShapes", "CnrInUpOpenShapes", "CnrOutOpenShapes", "CnrInOpenShapes", "CnrDnOpenRefShapes", "CnrUpOpenRefShapes"]
	# jawOpenRefNum = ("JawOpenRef20")
	# jawOpenShapesList = ("JawOpenShapes")
	
	'''1
	CnrOutDnOpen
		JawOpenShapes -1
		cornerOuterDn -1
		CnrOutDnOpenShapes 1
	'''
	print bshList
	CnrOutDnOpenBsn = mc.blendShape("JawOpenShapes%s" % elem, "cornerOuterDn%s" % elem, "CnrOutDnOpenShapes%s" % elem, "CnrOutDnOpen%s" % elem,n="%sBsn" % bshList[0])[0]
	mc.setAttr("%sBsn" % bshList[0]+"."+"JawOpenShapes%s" % elem,-1)
	mc.setAttr("%sBsn" % bshList[0]+"."+"cornerOuterDn%s" % elem,-1)
	mc.setAttr("%sBsn" % bshList[0]+"."+"CnrOutDnOpenShapes%s" % elem,1)

	CnrOutDnOpenShapesBsn = mc.blendShape("JawOpenShapes%s" % elem, "cornerOuterDn%s" % elem, "CnrOutDnOpenShapes%s" % elem, n="%sShapesBsn" % bshList[0])[0]
	mc.setAttr("%sShapesBsn" % bshList[0]+"."+"JawOpenShapes%s" % elem,1)
	mc.setAttr("%sShapesBsn" % bshList[0]+"."+"cornerOuterDn%s" % elem,1)
	# mc.setAttr("%sBsn" % bshList[0]+"."+"CnrOutDnOpenShapes",1)
	'''2
	CnrOutUpOpen
		JawOpenShapes -1
		cornerOuterUp -1
		CnrOutUpOpenShapes 1
	'''
	CnrOutUpOpenBsn = mc.blendShape("JawOpenShapes%s" % elem, "cornerOuterUp%s" % elem, "CnrOutUpOpenShapes%s" % elem, "CnrOutUpOpen%s" % elem,n="%sBsn" % bshList[1])[0]
	mc.setAttr("%sBsn" % bshList[1]+"."+"JawOpenShapes%s" % elem,-1)
	mc.setAttr("%sBsn" % bshList[1]+"."+"cornerOuterUp%s" % elem,-1)
	mc.setAttr("%sBsn" % bshList[1]+"."+"CnrOutUpOpenShapes%s" % elem,1)

	CnrOutUpOpenShapesBsn = mc.blendShape("JawOpenShapes%s" % elem, "cornerOuterUp%s" % elem, "CnrOutUpOpenShapes%s" % elem, n="%sShapesBsn" % bshList[1])[0]
	mc.setAttr("%sShapesBsn" % bshList[1]+"."+"JawOpenShapes%s" % elem,1)
	mc.setAttr("%sShapesBsn" % bshList[1]+"."+"cornerOuterUp%s" % elem,1)
	# mc.setAttr("%sShapesBsn" % bshList[1]+"."+"CnrOutUpOpenShapes",1)
	'''3
	CnrInDnOpen
		JawOpenShapes -1
		cornerInnerDn -1
		cnrInDnOpenShapes 1
	'''
	CnrInDnOpenBsn = mc.blendShape("JawOpenShapes%s" % elem, "cornerInnerDn%s" % elem, "CnrInDnOpenShapes%s" % elem, "CnrInDnOpen%s" % elem,n="%sBsn" % bshList[2])[0]
	mc.setAttr("%sBsn" % bshList[2]+"."+"JawOpenShapes%s" % elem,-1)
	mc.setAttr("%sBsn" % bshList[2]+"."+"cornerInnerDn%s" % elem,-1)
	mc.setAttr("%sBsn" % bshList[2]+"."+"CnrInDnOpenShapes%s" % elem,1)

	CnrInDnOpenShapesBsn = mc.blendShape("JawOpenShapes%s" % elem, "cornerInnerDn%s" % elem, "CnrInDnOpenShapes%s" % elem, n="%sShapesBsn" % bshList[2])[0]
	mc.setAttr("%sShapesBsn" % bshList[2]+"."+"JawOpenShapes%s" % elem,1)
	mc.setAttr("%sShapesBsn" % bshList[2]+"."+"cornerInnerDn%s" % elem,1)
	# mc.setAttr("%sShapesBsn" % bshList[2]+"."+"CnrInDnOpenShapes",1)
	'''4
	CnrInUpOpen
		JawOpenShapes -1
		cornerInnerUp -1
		CnrInUpOpenShapes 1
	'''
	CnrInUpOpenBsn = mc.blendShape("JawOpenShapes%s" % elem, "cornerInnerUp%s" % elem, "CnrInUpOpenShapes%s" % elem, "CnrInUpOpen%s" % elem,n="%sBsn" % bshList[3])[0]
	mc.setAttr("%sBsn" % bshList[3]+"."+"JawOpenShapes%s" % elem,-1)
	mc.setAttr("%sBsn" % bshList[3]+"."+"cornerInnerUp%s" % elem,-1)
	mc.setAttr("%sBsn" % bshList[3]+"."+"CnrInUpOpenShapes%s" % elem,1)

	CnrInUpOpenBsn = mc.blendShape("JawOpenShapes%s" % elem, "cornerInnerUp%s" % elem, "CnrInUpOpenShapes%s" % elem, n="%sShapesBsn" % bshList[3])[0]
	mc.setAttr("%sShapesBsn" % bshList[3]+"."+"JawOpenShapes%s" % elem,1)
	mc.setAttr("%sShapesBsn" % bshList[3]+"."+"cornerInnerUp%s" % elem,1)
	# mc.setAttr("%sShapesBsn" % bshList[3]+"."+"CnrInUpOpenShapes",1)
	'''5
	CnrOutOpen
		JawOpenShapes -1
		cornerOut -1
		CnrOutOpenShapes 1
	'''
	CnrOutOpenBsn = mc.blendShape("JawOpenShapes%s" % elem, "cornerOut%s" % elem, "CnrOutOpenShapes%s" % elem, "CnrOutOpen%s" % elem,n="%sBsn" % bshList[4])[0]
	mc.setAttr("%sBsn" % bshList[4]+"."+"JawOpenShapes%s" % elem,-1)
	mc.setAttr("%sBsn" % bshList[4]+"."+"cornerOut%s" % elem,-1)
	mc.setAttr("%sBsn" % bshList[4]+"."+"CnrOutOpenShapes%s" % elem,1)

	CnrOutOpenShapesBsn = mc.blendShape("JawOpenShapes%s" % elem, "cornerOut%s" % elem, "CnrOutOpenShapes%s" % elem, n="%sShapesBsn" % bshList[4])[0]
	mc.setAttr("%sShapesBsn" % bshList[4]+"."+"JawOpenShapes%s" % elem,1)
	mc.setAttr("%sShapesBsn" % bshList[4]+"."+"cornerOut%s" % elem,1)
	# mc.setAttr("%sShapessn" % bshList[4]+"."+"CnrOutOpenShapes",1)
	'''6
	CnrInOpen
		JawOpenShapes -1
		cornerIn -1
		CnrInOpenShapes 1
	'''
	CnrInOpenBsn = mc.blendShape("JawOpenShapes%s" % elem, "cornerIn%s" % elem, "CnrInOpenShapes%s" % elem, "CnrInOpen%s" % elem,n="%sBsn" % bshList[5])[0]
	mc.setAttr("%sBsn" % bshList[5]+"."+"JawOpenShapes%s" % elem,-1)
	mc.setAttr("%sBsn" % bshList[5]+"."+"cornerIn%s" % elem,-1)
	mc.setAttr("%sBsn" % bshList[5]+"."+"CnrInOpenShapes%s" % elem,1)

	CnrInOpenShapesBsn = mc.blendShape("JawOpenShapes%s" % elem, "cornerIn%s" % elem, "CnrInOpenShapes%s" % elem, n="%sShapesBsn" % bshList[5])[0]
	mc.setAttr("%sShapesBsn" % bshList[5]+"."+"JawOpenShapes%s" % elem,1)
	mc.setAttr("%sShapesBsn" % bshList[5]+"."+"cornerIn%s" % elem,1)
	# mc.setAttr("%sShapesBsn" % bshList[5]+"."+"CnrInOpenShapes",1)
	'''7
	CnrDnOpen
		JawOpenRef20 -1
		cornerDn -1
		CnrDnOpenRefShapes 1
	'''
	CnrDnOpenBsn = mc.blendShape(("JawOpenRef%s%s" % (jawMthRotateX, elem)), "cornerDn%s" % elem, "CnrDnOpenRefShapes%s" % elem, "CnrDnOpen%s" % elem,n="%sBsn" % bshList[6])[0]
	mc.setAttr("%sBsn" % bshList[6]+"."+("JawOpenRef%s%s" % (jawMthRotateX, elem)),-1)
	mc.setAttr("%sBsn" % bshList[6]+"."+"cornerDn%s" % elem,-1)
	mc.setAttr("%sBsn" % bshList[6]+"."+"CnrDnOpenRefShapes%s" % elem,1)

	CnrDnOpenRefShapesBsn = mc.blendShape(("JawOpenRef%s%s" % (jawMthRotateX, elem)), "cornerDn%s" % elem, "CnrDnOpenRefShapes%s" % elem,n="%sBsn" % crtBshShapesTotal[3])[0]
	mc.setAttr("%sBsn" % crtBshShapesTotal[3]+"."+("JawOpenRef%s%s" % (jawMthRotateX, elem)),1)
	mc.setAttr("%sBsn" % crtBshShapesTotal[3]+"."+"cornerDn%s" % elem,1)
	# mc.setAttr("%sBsn" % bshList[6]+"."+"CnrDnOpenRefShapes",1)
	'''8
	CnrUpOpen
		JawOpenRef20 -1
		cornerUp -1
		CnrUpOpenRefShapes 1
	'''
	CnrUpOpenBsn = mc.blendShape(("JawOpenRef%s%s" % (jawMthRotateX, elem)), "cornerUp%s" % elem, "CnrUpOpenRefShapes%s" % elem, "CnrUpOpen%s" % elem,n="%sBsn" % bshList[7])[0]
	mc.setAttr("%sBsn" % bshList[7]+"."+("JawOpenRef%s%s" % (jawMthRotateX, elem)),-1)
	mc.setAttr("%sBsn" % bshList[7]+"."+"cornerUp%s" % elem,-1)
	mc.setAttr("%sBsn" % bshList[7]+"."+"CnrUpOpenRefShapes%s" % elem,1)

	CnrUpOpenRefShapesBsn = mc.blendShape(("JawOpenRef%s%s" % (jawMthRotateX, elem)), "cornerUp%s" % elem, "CnrUpOpenRefShapes%s" % elem,n="%sBsn" % crtBshShapesTotal[2])[0]
	mc.setAttr("%sBsn" % crtBshShapesTotal[2]+"."+("JawOpenRef%s%s" % (jawMthRotateX, elem)),1)
	mc.setAttr("%sBsn" % crtBshShapesTotal[2]+"."+"cornerUp%s" % elem,1)
	# mc.setAttr("%sBsn" % bshList[7]+"."+"CnrUpOpenRefShapes",1)

	'''9
	JawOpen
		JawOpenRef20 -1
		JawOpenShapes 1
	'''
	JawOpenBsn = mc.blendShape(("JawOpenRef%s%s" % (jawMthRotateX, elem)), "JawOpenShapes%s" % elem, "JawOpen%s" % elem,n="%sBsn" % bshList[8])[0]
	mc.setAttr("%sBsn" % bshList[8]+"."+("JawOpenRef%s%s" % (jawMthRotateX, elem)),-1)
	mc.setAttr("%sBsn" % bshList[8]+"."+"JawOpenShapes%s" % elem,1)

	# JawOpenShapesBsn = mc.blendShape(("JawOpenRef%s" % jawMthRotateX), "JawOpenShapes", "JawOpen",n="%sBsn" % bshList[8])[0]
	# mc.setAttr("%sBsn" % bshList[8]+"."+("JawOpenRef%s" % jawMthRotateX),-1)
	# mc.setAttr("%sBsn" % bshList[8]+"."+"JawOpenShapes",1)
	# JawOpenShape
	'''10
	JawOpenMth
		JawOpenRef20 -1
		JawOpenMthShapes 1
	'''
	JawOpenMthBsn = mc.blendShape(("JawOpenRef%s%s" % (jawMthRotateX, elem)), "JawOpenMthShapes%s" % elem, "JawOpenMth%s" % elem,n="%sBsn" % bshList[9])[0]
	mc.setAttr("%sBsn" % bshList[9]+"."+("JawOpenRef%s%s" % (jawMthRotateX, elem)),-1)
	mc.setAttr("%sBsn" % bshList[9]+"."+"JawOpenMthShapes%s" % elem,1)