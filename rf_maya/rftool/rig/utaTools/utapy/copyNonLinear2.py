from nuTools import misc
reload(misc)
from utaTools.nuTools import misc
reload(misc)
import maya.cmds as mc
from utaTools.utapy import utaCore
reload (utaCore)


def copyNonlinearUI(*args):

    
    windowID = 'Copy_Nonlinear_Node_UI'
    
    
    if mc.window(windowID, exists=True):
        mc.deleteUI(windowID)
    winWidth = 300
    winHight = 100
    
    mc.window(windowID , title = 'Copy Nonlinear Node' , width = winWidth , height = winHight,sizeable = True , bgc =[.25,.25,.25])
    mainCL = mc.columnLayout('columnLayoutName01',adj=True)
    
    utaCore.wintes(window = 'Copy_Nonlinear_Node_UI' , path = 'D:/' , pngIcon = 'cropped-finger-icon.png')

    mc.frameLayout ("layoutFrame01", l = "How To Use ? ? ?", collapse = True, collapsable = True, p = "columnLayoutName01" , bgc =[.18,.18,.18] , w=winWidth)
    mc.text('1.> Select A MasterObject' , align = 'left')
    mc.text('2.> Selct ChildObjects(Can Select Multiple Childs)' , align = 'left')
    mc.text('3.> Press A Button Down Below ' , align = 'left') 
    mc.text('       Deforms = Copy Deforms Only' , align = 'left' , fn='obliqueLabelFont'  , bgc=[1,.5,.5])
    mc.text('       SkinCluster = Copy SkinCluster Only', align = 'left' , fn='obliqueLabelFont', bgc=[.5,1,.5])
    mc.text('       All = Copy Both, Deforms And SkinCluster ' , align = 'left' , fn='obliqueLabelFont', bgc=[.5,.5,1])
    mc.text('       ')
    mc.setParent('..')


    mc.rowLayout(numberOfColumns=3)    
    mc.button('Deforms' , c = copyDeform , w=0.333*winWidth , h = 50, bgc=[1,.5,.5])
    mc.button('SkinCluster' , c = copySkin , w=0.333*winWidth , h = 50, bgc=[.5,1,.5])    
    mc.button('All' , c = copyAll , w=0.333*winWidth , h = 50, bgc=[.5,.5,1])
    mc.setParent('..')

    mc.frameLayout ("layoutFrame02", l = "Add Specify Deform", collapse = True, collapsable = True, p = "columnLayoutName01" , bgc =[.18,.18,.18] , w=winWidth)
    NameValue = mc.textFieldGrp( 'Name' , label='Name',columnWidth2=[40,250] , ed = True , pht='>>>Ex: ffd1,wire1,bend1,wave1,wire32, ...<<<')
    mc.button('Copy From TextField' , c = copyNonLinearText, h = 50, bgc=[0.41,.75,.82] , hlc = [1,0,0])
    mc.setParent('..')

    mc.frameLayout ("layoutFrame03", l = "Copy Deform By Type", collapse = True, collapsable = True, p = "columnLayoutName01",bgc =[.18,.18,.18], w=winWidth)
    mc.rowLayout(numberOfColumns=2)
    mc.button('Wire' , c = copyWire , w=0.5*winWidth , h = 30,bgc=[1,.85,0])   
    mc.button('Lattice' , c = copyFfd ,  w=0.5*winWidth, h = 30,bgc=[1,.85,0])
    mc.setParent('..')

    mc.rowLayout(numberOfColumns=3)
    mc.button('Bend' , c = copyBend , w=0.333*winWidth , h = 30,bgc=[1,.85,0])
    mc.button('Flare' , c = copyFlare, w=0.333*winWidth , h = 30,bgc=[1,.85,0])
    mc.button('Sine' , c = copySine, w=0.333*winWidth , h = 30,bgc=[1,.85,0])
    mc.setParent('..')

    mc.rowLayout(numberOfColumns=3)
    mc.button('Squash' , c = copyBend, w=0.333*winWidth , h = 30,bgc=[1,.85,0])
    mc.button('Twist' , c = copyFlare, w=0.333*winWidth , h = 30,bgc=[1,.85,0])
    mc.button('Wave' , c = copySine, w=0.333*winWidth , h = 30,bgc=[1,.85,0])
    mc.setParent('..')

    mc.showWindow()


#=====================================================================================#
#==================================CALCULATE STAGE DEF================================#
#=====================================================================================#


def selected(*args):
    objBase = mc.ls(sl = True )[0]
    objChild =  mc.ls(sl = True )[1:]
    return objBase, objChild


def copyDeform(*args):
    objBase , objChild = selected()
    DeformName, SkinName, deformAll  = copyAllNonLinear(objBase= objBase, objChild = objChild)
    revList = DeformName[::-1]
    for each in objChild:
        for objSel in revList:
            mc.select(each)
            misc.addDeformerMember(deformerName=(objSel))
    print '>>> COPY ALL DEFORMS COMPLETE <<<'

def copyAllNonLinear(objBase= '', objChild = [] ,*args):
    Mom = mc.listRelatives(objBase , f=True)
    Child = objChild

    AllList = mc.listHistory(Mom , pdo =True) 
    DeformList = ['ffd' , 'wire','nonLinear']
    DeformName =[]
    SkinList ='skinCluster'
    SkinName = []
    deformAll = []
    for i in AllList:
        subj = mc.objectType(i)
        for a in DeformList:
            if subj == a:
                DeformName.append(i)
                deformAll.append(i)
            else:
                pass
        if subj == SkinList:
            SkinName.append(i)
            deformAll.append(i)
        else:
            pass
    return  DeformName, SkinName, deformAll

def addDeformer(obj = '', *args):
    misc.addDeformerMember(deformerName=(obj))


#=====================================================================================#
#=================================MAIN BUTTON DEF=====================================#
#=====================================================================================#


def copySkin(*args):
    misc.copySkinWeight(removeUnuse=True, cleanUnuseShp=True)
    print '>>> COPY SKIN WEIGHT COMPLETE <<<'

def copyAll(*args):
    objBase , objChild = selected()
    DeformName, SkinName, deformAll  = copyAllNonLinear(objBase= objBase, objChild = objChild )
    revList = deformAll[::-1]
    for each in revList:
        eachNode = mc.objectType(each)
        if eachNode =='skinCluster':
            for eachChild in objChild:
                mc.select(objBase, r = True)
                mc.select(eachChild, add = True)
                copySkin()
        elif eachNode =='wire':
            for eachChild in objChild:
                mc.select(objBase, r = True)
                mc.select(eachChild, add = True)
                addDeformer(obj = each)
        elif eachNode =='ffd':
            for eachChild in objChild:
                mc.select(objBase, r = True)
                mc.select(eachChild, add = True)
                addDeformer(obj = each)
        elif eachNode =='nonLinear':
            for eachChild in objChild:

                nlNameFull = mc.nonLinear(each,q=True,dt=True)[0]
                nlName = nlNameFull.split('|')[-1]
                nlType = mc.objectType('{}Shape'.format(nlName))
                if nlType =='deformBend':
                    mc.select(objBase, r = True)
                    mc.select(eachChild, add = True)
                    addDeformer(obj = each)
                elif nlType =='deformFlare':
                    mc.select(objBase, r = True)
                    mc.select(eachChild, add = True)
                    addDeformer(obj = each)            
                elif nlType =='deformSine':
                    mc.select(objBase, r = True)
                    mc.select(eachChild, add = True)
                    addDeformer(obj = each) 
                elif nlType =='deformSquash':
                    mc.select(objBase, r = True)
                    mc.select(eachChild, add = True)
                    addDeformer(obj = each) 
                elif nlType =='deformTwist':
                    mc.select(objBase, r = True)
                    mc.select(eachChild, add = True)
                    addDeformer(obj = each) 
                elif nlType =='deformWave':
                    mc.select(objBase, r = True)
                    mc.select(eachChild, add = True)
                    addDeformer(obj = each)


#=====================================================================================#
#=================================BY NAME BUTTON DEF==================================#
#=====================================================================================#    


def copyNonLinearText(*args):
    name = mc.textFieldGrp('Name' , q=True , text=True)
    from nuTools import misc
    reload(misc)
    misc.addDeformerMember(deformerName='{}'.format(name))
    print '>>> COPY SPECIFY DEFORM COMPLETE <<<'


#=====================================================================================#
#=================================BY DEFORMS BUTTON DEF===============================#
#=====================================================================================#


def copyWire(*args):
    objBase , objChild = selected()
    DeformName, SkinName, deformAll  = copyAllNonLinear(objBase= objBase, objChild = objChild )
    revList = deformAll[::-1]
    for each in revList:
        eachNode = mc.objectType(each)
        if eachNode =='wire':
            for eachChild in objChild:
                mc.select(objBase, r = True)
                mc.select(eachChild, add = True)
                addDeformer(obj = each)
        else:
            pass
    print '>>> COPY ALL WIRE DEFROMS COMPLETE <<<'

def copyFfd(*args):
    # ffd = Lattice
    objBase , objChild = selected()
    DeformName, SkinName, deformAll  = copyAllNonLinear(objBase= objBase, objChild = objChild )
    revList = deformAll[::-1]
    for each in revList:
        eachNode = mc.objectType(each)
        if eachNode =='ffd':
            for eachChild in objChild:
                mc.select(objBase, r = True)
                mc.select(eachChild, add = True)
                addDeformer(obj = each)
        else:
            pass
    print '>>> COPY ALL FFD DEFROMS COMPLETE <<<'

def copyBend(*args):
    objBase , objChild = selected()
    DeformName, SkinName, deformAll  = copyAllNonLinear(objBase= objBase, objChild = objChild )
    revList = deformAll[::-1]
    for each in revList:
        eachNode = mc.objectType(each)
        if eachNode == 'nonLinear':
            for eachChild in objChild:             
                nlNameFull = mc.nonLinear(each,q=True,dt=True)[0]
                nlName = nlNameFull.split('|')[-1]
                nlType = mc.objectType('{}Shape'.format(nlName))
                if nlType =='deformBend':
                    mc.select(objBase, r = True)
                    mc.select(eachChild, add = True)
                    addDeformer(obj = each)            

def copyFlare(*args):
    objBase , objChild = selected()
    DeformName, SkinName, deformAll  = copyAllNonLinear(objBase= objBase, objChild = objChild )
    revList = deformAll[::-1]
    for each in revList:
        eachNode = mc.objectType(each)
        if eachNode == 'nonLinear':
            for eachChild in objChild:             
                nlNameFull = mc.nonLinear(each,q=True,dt=True)[0]
                nlName = nlNameFull.split('|')[-1]
                nlType = mc.objectType('{}Shape'.format(nlName))
                if nlType =='deformFlare':
                    mc.select(objBase, r = True)
                    mc.select(eachChild, add = True)
                    addDeformer(obj = each)            

def copySine(*args):
    objBase , objChild = selected()
    DeformName, SkinName, deformAll  = copyAllNonLinear(objBase= objBase, objChild = objChild )
    revList = deformAll[::-1]
    for each in revList:
        eachNode = mc.objectType(each)
        if eachNode == 'nonLinear':
            for eachChild in objChild:             
                nlNameFull = mc.nonLinear(each,q=True,dt=True)[0]
                nlName = nlNameFull.split('|')[-1]
                nlType = mc.objectType('{}Shape'.format(nlName))
                if nlType =='deformSine':
                    mc.select(objBase, r = True)
                    mc.select(eachChild, add = True)
                    addDeformer(obj = each)   

def copySquash(*args):
    objBase , objChild = selected()
    DeformName, SkinName, deformAll  = copyAllNonLinear(objBase= objBase, objChild = objChild )
    revList = deformAll[::-1]
    for each in revList:
        eachNode = mc.objectType(each)
        if eachNode == 'nonLinear':
            for eachChild in objChild:             
                nlNameFull = mc.nonLinear(each,q=True,dt=True)[0]
                nlName = nlNameFull.split('|')[-1]
                nlType = mc.objectType('{}Shape'.format(nlName))
                if nlType =='deformSquash':
                    mc.select(objBase, r = True)
                    mc.select(eachChild, add = True)
                    addDeformer(obj = each)   

def copyTwist(*args):
    objBase , objChild = selected()
    DeformName, SkinName, deformAll  = copyAllNonLinear(objBase= objBase, objChild = objChild )
    revList = deformAll[::-1]
    for each in revList:
        eachNode = mc.objectType(each)
        if eachNode == 'nonLinear':
            for eachChild in objChild:             
                nlNameFull = mc.nonLinear(each,q=True,dt=True)[0]
                nlName = nlNameFull.split('|')[-1]
                nlType = mc.objectType('{}Shape'.format(nlName))
                if nlType =='deformTwist':
                    mc.select(objBase, r = True)
                    mc.select(eachChild, add = True)
                    addDeformer(obj = each)   

def copyWave(*args):
    objBase , objChild = selected()
    DeformName, SkinName, deformAll  = copyAllNonLinear(objBase= objBase, objChild = objChild )
    revList = deformAll[::-1]
    for each in revList:
        eachNode = mc.objectType(each)
        if eachNode == 'nonLinear':
            for eachChild in objChild:             
                nlNameFull = mc.nonLinear(each,q=True,dt=True)[0]
                nlName = nlNameFull.split('|')[-1]
                nlType = mc.objectType('{}Shape'.format(nlName))
                if nlType =='deformWave':
                    mc.select(objBase, r = True)
                    mc.select(eachChild, add = True)
                    addDeformer(obj = each)   


#===================================================================================================V-ZER CREATOR=========#