# GetPath GitLab
# import rftool.rig

#!/usr/bin/python
#-*-coding: utf-8 -*-
import sys, os
import pymel.core as pm ;
import maya.cmds as mc ;
import maya.mel as mm ;
from rf_utils import file_utils
from functools import partial
reload(file_utils)

from rf_utils.context import context_info
from rf_utils.sg import sg_utils
from rf_utils.sg import sg_process
from rf_utils import admin
reload(context_info)
reload(sg_utils)
reload(sg_process)
reload(admin)

# from rigScript import core
# reload(core)
# cn = core.Node()

#path project config
rootScript = os.environ['RFSCRIPT']
pathProjectJson = r"{}\core\rf_maya\rftool\rig\export_config\project_config.json".format(rootScript)
projectData = file_utils.json_loader(pathProjectJson)


## Check Project Name
def checkProject(*args):
    from rf_utils.context import context_info
    reload(context_info)

    asset = context_info.ContextPathInfo()
    projectName = asset.project 
    return projectName

# Path ---------------------------------------
corePath = '%s/core' % os.environ.get('RFSCRIPT')
path = '{}/rf_maya/rftool/rig/'.format(corePath)
#path project config
pathProjectJson = '{}/export_config/project_config.json'.format(path)
projectData = file_utils.json_loader(pathProjectJson)

# Path ---------------------------------------

def pngImags():
    imags = (path + "utaTools/utaicon/uiIcon/")
    return imags

# open Exporer
def openExporerFile(*args):
    os.startfile ( path + 'utaTools');
def openWebbrowserFile(*args):
    import webbrowser
    reload(webbrowser)

    webbrowser.open('http://riffcg.com', new=2)
def openTuterialFile(*args):
    os.startfile(r'P:\Doublemonkeys\all\research\Rig\tutorRig\facial_rigs')
def twoHerosDeatlineFile(*args):
    import webbrowser
    reload(webbrowser)

    webbrowser.open('https://docs.google.com/spreadsheets/d/1P3XekMIa595Vk2fc8Th8G6Q1WpQCaBqE1LwXEXKUNKA/edit#gid=1404888389', new=2)
def nakee2DeatlineFile(*args):
    reload(webbrowser)


    webbrowser.open('https://docs.google.com/spreadsheets/d/17Fl9ASC0S_LdYnfQQ-KaU0hBUNCAD_9mnpXp8P9txZQ/edit#gid=0', new=2)

# copyShape --------------------------------
# copyShape --------------------------------
def copyShapeMouths ( *args ) :
    from utaTools.utapy import copyShapeMouth 
    reload(copyShapeMouth)

    copyShapeMouth.copyMthShapeToBsh();
    pm.button("MouthcopyShapeToBsh", e=True, bgc= (0.498039, 1, 0))
def copyShapeMouth_LFT( *args ) :
    from utaTools.utapy import copyShapeMouth 
    reload(copyShapeMouth)

    values = pm.textField ( "txtFieldValueLFT" , q =True , text = True)
    copyShapeMouth.copyMthShapeToBshLFT(value = values);
    pm.button("MouthLFTcopyShapeToBsh", e=True, bgc= (0, 0.74902, 1))

def copyShapeMouth_RGT ( *args ) :
    from utaTools.utapy import copyShapeMouth 
    reload(copyShapeMouth)

    values = pm.textField ( "txtFieldValueRGT" , q  = True , text = True  )
    copyShapeMouth.copyMthShapeToBshRGT(value = values);
    pm.button("MouthRGTcopyShapeToBsh", e=True, bgc= (1, 0.0784314, 0.576471))
    
def getGeoBase(*args):
    from utaTools.utapy import copyShapeMouth 
    reload(copyShapeMouth)

    sels = mc.ls(sl=True)       
    suffix = pm.textField ( "txtFieldGeoBase" , q  = True , text = True  )
    copyShapeMouth.copyShapeToBase(geoBase= suffix, lists= sels)
    pm.button("copyShapeToBsh", e=True, bgc= (0.498039, 1, 0))

def ReMthShapeFile (*args):
    from utaTools.utapy import copyShapeMouth 
    reload(copyShapeMouth)
    from utaTools.utapy import ReMthShape as rms 
    reload(rms)

    sels = mc.ls(sl=True) 
    for each in sels:
        pm.delete ( each , ch = True ) ;   
    geoBaseSuffix = pm.textField ( "txtFieldGeoBase" , q  = True , text = True  )
    txtFieldJawMthRotateXSuffix = pm.textField ( "txtFieldJawMthRX" , q  = True , text = True  )
    copyShapeMouth.copyShapeToBase(geoBase= geoBaseSuffix, lists= sels)
    rms.ReMthShape(jawMthRotateX = txtFieldJawMthRotateXSuffix)
    pm.button("RevertToDefaultShape", e=True, bgc= (0.498039, 1, 0))

def addTextGeoBase(*args):
    sel = pm.ls(sl=True) ;
    if sel != []:
        sel = sel [0] ;
        pm.textField("txtFieldGeoBase", e = True  , text = sel)
    else:
        pm.textField("txtFieldGeoBase", e = True  , text = "")  
        print "Please select Object !! : %s" % "BodyMthRig_Geo"
    pm.button("addGeoBaseToBsh", e=True, bgc= (0.498039, 1, 0))
  

def addTextPathsMthRig_Geo(*args):
    sel = pm.ls(sl=True) ;
    if sel != []:
        sel = sel[0] ;
        pm.textField("txtFieldPathsRig_Geo", e = True  , text = sel)
    else:
        pm.textField("txtFieldPathsRig_Geo", e = True  , text = "")  
        print "Please select Object !! : %s" % "Paths Body Rig"
    pm.button("addPathRig_Geo", e=True, bgc= (0.498039, 1, 0)) 


def addTextBodyMthRig_Geo(*args):
    sel = pm.ls(sl=True) ;
    if sel != []:
        sel = sel[0] ;
        pm.textField("txtFieldBodyMthRig_Geo", e = True  , text = sel)
    else:
        pm.textField("txtFieldBodyMthRig_Geo", e = True  , text = "")  
        print "Please select Object !! : %s" % "BodyMthRig_Geo"
    pm.button("addBodyMthRig_Geo", e=True, bgc= (0.498039, 1, 0))  

def addTextPathsObjMthRig_Geo(*args):
    sel = pm.ls(sl=True) ;
    if sel != []:
        sel = sel[0] ;
        pm.textField("txtFieldPathsObjRig_Geo", e = True  , text = sel)
    else:
        pm.textField("txtFieldPathsObjRig_Geo", e = True  , text = "")  
        print "Please select Object !! : %s" % "Paths Rig"
    pm.button("addPathObjRig_Geo", e=True, bgc= (0.498039, 1, 0))  

def addTextBodyMthJawRig_Geo(*args):
    sel = pm.ls(sl=True) ;
    if sel != []:
        sel = sel [0] ;
        pm.textField("txtFieldBodyMthJawRig_Geo", e = True  , text = sel)
    else:
        pm.textField("txtFieldBodyMthJawRig_Geo", e = True  , text = "")  
        print "Please select Object !! : %s" % "BodyMthJawRig_Geo"
    pm.button("addBodyMthJawRig_Geo", e=True, bgc= (0.498039, 1, 0))  
def addTextBodyMthLipRig_Geo(*args):
    sel = pm.ls(sl=True) ;
    if sel != []:
        sel = sel [0] ;
        pm.textField("txtFieldBodyMthLipRig_Geo", e = True  , text = sel)
    else:
        pm.textField("txtFieldBodyMthLipRig_Geo", e = True  , text = "")  
        print "Please select Object !! : %s" % "BodyMthLipRig_Geo"
    pm.button("addBodyMthLipRig_Geo", e=True, bgc= (0.498039, 1, 0))  
def addTextBodyBshRig_Geo(*args):
    sel = pm.ls(sl=True) ;
    if sel != []:
        sel = sel [0] ;
        pm.textField("txtFieldBodyBshRig_Geo2", e = True  , text = sel)
    else:
        pm.textField("txtFieldBodyBshRig_Geo2", e = True  , text = "")  
        print "Please select Object !! : %s" % "BodyBshRig"
    pm.button("addBodyBshRig_Geo", e=True, bgc= (0.498039, 1, 0))  

def addTextInputBody_Geo(*args):
    sel = pm.ls(sl=True) ;
    if sel != []:
        sel = sel [0] ;
        pm.textField("txtFieldInputBody_Geo", e = True  , text = sel)
    else:
        pm.textField("txtFieldInputBody_Geo", e = True  , text = "")  
        print "Please select Object !! : %s" % "Body_Geo"
    pm.button("addInputBody_Geo", e=True, bgc= (0.498039, 1, 0))  

def addTextInputBodyFur_Geo(*args):
    sel = pm.ls(sl=True) ;
    if sel != []:
        sel = sel [0] ;
        pm.textField("txtFieldInputBodyFur_Geo", e = True  , text = sel)
    else:
        pm.textField("txtFieldInputBodyFur_Geo", e = True  , text = "")  
        print "Please select Object !! : %s" % "BodyFur_Geo"
    pm.button("addInputBodyFur_Geo", e=True, bgc= (0.498039, 1, 0))  

def addTextBodyFclRigWrapped_Geo(*args):
    sel = pm.ls(sl=True) ;
    if sel != []:
        sel = sel [0] ;
        pm.textField("txtFieldBodyFclRigWrapped_Geo", e = True  , text = sel)
    else:
        pm.textField("txtFieldBodyFclRigWrapped_Geo", e = True  , text = "")  
        print "Please select Object !! : %s" % "BodyFclRigWrapped_Geo"
    pm.button("addBodyFclRigWrapped_Geo", e=True, bgc= (0.498039, 1, 0))  

def addTextObj01BshRig_Geo(*args):
    sel = pm.ls(sl=True) ;
    if sel != []:
        sel = sel [0] ;
        pm.textField("txtFieldObj01BshRig_Geo", e = True  , text = sel)
    else:
        pm.textField("txtFieldObj01BshRig_Geo", e = True  , text = "")  
        print "Please select Object !! : %s" % "Obj01BshRig"
    pm.button("addObj01BshRig_Geo", e=True, bgc= (0.498039, 1, 0))  

def addTextObj02BshRig_Geo(*args):
    sel = pm.ls(sl=True) ;
    if sel != []:
        sel = sel [0] ;
        pm.textField("txtFieldObj02BshRig_Geo", e = True  , text = sel)
    else:
        pm.textField("txtFieldObj02BshRig_Geo", e = True  , text = "")  
        print "Please select Object !! : %s" % "Obj02BshRig"
    pm.button("addObj02BshRig_Geo", e=True, bgc= (0.498039, 1, 0))  

def addTextObj01MthRig_Geo(*args):
    sel = pm.ls(sl=True) ;
    if sel != []:
        sel = sel [0] ;
        pm.textField("txtFieldObj01MthRig_Geo", e = True  , text = sel)
    else:
        pm.textField("txtFieldObj01MthRig_Geo", e = True  , text = "")  
        print "Please select Object !! : %s" % "Obj01MthRig"
    pm.button("addObj01MthRig_Geo", e=True, bgc= (0.498039, 1, 0))  

def addTextObj02MthRig_Geo(*args):
    sel = pm.ls(sl=True) ;
    if sel != []:
        sel = sel [0] ;
        pm.textField("txtFieldObj02MthRig_Geo", e = True  , text = sel)
    else:
        pm.textField("txtFieldObj02MthRig_Geo", e = True  , text = "")  
        print "Please select Object !! : %s" % "Obj02MthRig"
    pm.button("addObj02MthRig_Geo", e=True, bgc= (0.498039, 1, 0))  

def addTextBodyObj01MthRig_Geo(*args):
    sel = pm.ls(sl=True) ;
    if sel != []:
        sel = sel [0] ;
        tetObj = pm.textField("txtFieldBodyMthObj01Rig_Geo", e = True  , text = sel)
    else:
        tetObj = pm.textField("txtFieldBodyMthObj01Rig_Geo", e = True  , text = "")  
        print "Please select Object !! : %s" % "Obj Geo"
    pm.button("addBodyMthObj01Rig_Geo", e=True, bgc= (0.498039, 1, 0))  

def addTextBodyObj01MthJawRig_Geo(*args):
    sel = pm.ls(sl=True) ;
    if sel != []:
        sel = sel [0] ;
        pm.textField("txtFieldObj01MthJawRig_Geo", e = True  , text = sel)
    else:
        pm.textField("txtFieldObj01MthJawRig_Geo", e = True  , text = "")  
        print "Please select Object !! : %s" % "Obj Jaw Geo"
    pm.button("addObj01MthJawRig_Geo", e=True, bgc= (0.498039, 1, 0))  



def addTextBodyObj01MthLipRig_Geo(*args):
    sel = pm.ls(sl=True) ;
    if sel != []:
        sel = sel [0] ;
        pm.textField("txtFieldObj01MthLipRig_Geo", e = True  , text = sel)
    else:
        pm.textField("txtFieldObj01MthLipRig_Geo", e = True  , text = "")  
        print "Please select Object !! : %s" % "Obj Lip Geo"
    pm.button("addObj01MthLipRig_Geo", e=True, bgc= (0.498039, 1, 0))  


def addTextBodyObj02MthRig_Geo(*args):
    sel = pm.ls(sl=True) ;
    if sel != []:
        sel = sel [0] ;
        pm.textField("txtFieldBodyMthObj02Rig_Geo", e = True  , text = sel)
    else:
        pm.textField("txtFieldBodyMthObj02Rig_Geo", e = True  , text = "")  
        print "Please select Object !! : %s" % "MthObj02Rig_Geo"
    pm.button("addBodyMthObj02Rig_Geo", e=True, bgc= (0.498039, 1, 0))  
def addTextBodyObj02MthJawRig_Geo(*args):
    sel = pm.ls(sl=True) ;
    if sel != []:
        sel = sel [0] ;
        pm.textField("txtFieldObj02MthJawRig_Geo", e = True  , text = sel)
    else:
        pm.textField("txtFieldObj02MthJawRig_Geo", e = True  , text = "")  
        print "Please select Object !! : %s" % "MthObj02JawRig"
    pm.button("addObj02MthJawRig_Geo", e=True, bgc= (0.498039, 1, 0))  
def addTextBodyObj02MthLipRig_Geo(*args):
    sel = pm.ls(sl=True) ;
    if sel != []:
        sel = sel [0] ;
        pm.textField("txtFieldObj02MthLipRig_Geo", e = True  , text = sel)
    else:
        pm.textField("txtFieldObj02MthLipRig_Geo", e = True  , text = "")  
        print "Please select Object !! : %s" % "MthObj02JawRig"
    pm.button("addObj02MthLipRig_Geo", e=True, bgc= (0.498039, 1, 0))

def get_project_from_sg():
    return sorted(projectData["project"])

def get_char_from_proj(proj, char, parent, *args):
    project_name = mc.optionMenu(proj, q=True, v=True)
    asset_list = []
    for asset in sg_process.get_assets(project_name,[['sg_asset_type', 'is', 'char']]):
        asset_list.append(asset['code'])
    if mc.optionMenu(char, ex=True):
        parent = mc.optionMenu(char,q=1,p=1)
        mc.deleteUI(char)
    projectToSort = mc.optionMenu(char, h=20, w=110, p=parent)
    for obj in sorted(asset_list):
        pm.menuItem(l=obj, p=projectToSort)

def get_rig_data_path(project,asset):
    context = context_info.Context()
    context.use_sg(sg_utils.sg,project , 'asset', entityName = asset)
    asset_context = context_info.ContextPathInfo(context = context)
    asset_context.context.update(step='rig',process = 'data')
    return asset_context.path.scheme(key='publishHeroPath').abs_path().replace('/hero','').split("/rig/data")[0]

def findProjectPath(*args):
    project_sort = mc.optionMenu("projectFromSorceMenu", q=True, v=True)
    project_dest = mc.optionMenu("projectToDestiMenu", q=True, v=True)
    char_sort = mc.optionMenu("charFromSorceMenu", q=True, v=True)
    char_dest = mc.optionMenu("charToDestiMenu", q=True, v=True)

    sort_path = get_rig_data_path(project_sort, char_sort)
    to_path = get_rig_data_path(project_dest, char_dest)

    if not os.path.isfile(sort_path + "/rig/data/mainRig.py"):
        mc.confirmDialog(m="Sorce has not file ( mainRig.py )")
        return

    if not os.path.isdir(to_path + "/rig"):
        admin.makedirs(to_path + "/rig")

    if not os.path.isdir(to_path + "/rig/data"):
        admin.makedirs(to_path + "/rig/data")

    if not os.path.isfile(to_path + "/rig/data/mainRig.py"):
        # admin.copyfile(sort_path + "/rig/data/mainRig.py", to_path + "/rig/data/mainRig.py")
        copy_module = mc.confirmDialog(m="Copy module", b=["all", "main", "cancel"], cb="cancel")
        if copy_module == "main":
            admin.copyfile(sort_path + "/rig/data/mainRig.py", to_path + "/rig/data/mainRig.py")
            admin.copyfile(sort_path + "/rig/data/ctrl_shape.dat", to_path + "/rig/data/ctrl_shape.dat")
            admin.copyfile(sort_path + "/rig/data/attrCtrl.txt", to_path + "/rig/data/attrCtrl.txt")
            admin.copyfile(sort_path + "/rig/data/attrCrv_connection.txt", to_path + "/rig/data/attrCrv_connection.txt")
            admin.win_copy_directory(sort_path + "/rig/data/backup_attrCtrl", to_path + "/rig/data/backup_attrCtrl")
            mc.confirmDialog(m="Copy module success")

        elif copy_module == "all":
            admin.copyfile(sort_path + "/rig/data/mainRig.py", to_path + "/rig/data/mainRig.py")
            admin.copyfile(sort_path + "/rig/data/ctrl_shape.dat", to_path + "/rig/data/ctrl_shape.dat")
            admin.copyfile(sort_path + "/rig/data/attrCtrl.txt", to_path + "/rig/data/attrCtrl.txt")
            admin.copyfile(sort_path + "/rig/data/attrCrv_connection.txt", to_path + "/rig/data/attrCrv_connection.txt")
            admin.win_copy_directory(sort_path + "/rig/data/backup_attrCtrl", to_path + "/rig/data/backup_attrCtrl")

            weight = []
            [weight.append(x) for x in os.listdir(sort_path + "/rig/data/") if "Weight.txt" in x]
            if weight:
                for x in weight:
                    admin.copyfile(sort_path + "/rig/data/{}".format(x), to_path + "/rig/data/{}".format(x))
            mc.confirmDialog(m="Copy module success")

    else:
        choice_copy = mc.confirmDialog(m="There is a module ( mainRig.py )\nDo you want to copy ?", b=["replace", "new version", "cancel"], cb="cancel")
        if choice_copy == "replace":
            check_sure = mc.confirmDialog(m="Sure na ja ?", b=["Yes", "No"], cb="No")

            if check_sure == "Yes":
                admin.copyfile(sort_path + "/rig/data/mainRig.py", to_path + "/rig/data/mainRig.py")
                print "replace module success"

        elif choice_copy == "new version":
            if not os.path.isdir(to_path + "/rig/data/backup_mainRig"):
                admin.makedirs(to_path + "/rig/data/backup_mainRig")

            dir_file = os.listdir(to_path + "/rig/data/backup_mainRig")
            if not dir_file:
                admin.copyfile(sort_path + "/rig/data/mainRig.py", to_path + "/rig/data/backup_mainRig/mainRig_v001.py")
            else:
                num_ver = dir_file[-1].split("_v")[1].split(".py")[0]
                num = num_ver.split("0")
                if len(num) == 1:
                    ver = str(eval(num) + 1)

                elif len(num) > 1:
                    num = eval(num[-1]) + 1
                    if len(str(num)) == 1:
                        ver = "00" + str(num)
                    elif len(str(num)) == 2:
                        ver = "0" + str(num)
                    elif len(str(num)) == 3:
                        ver = str(num)
   
                admin.copyfile(sort_path + "/rig/data/mainRig.py", to_path + "/rig/data/backup_mainRig/mainRig_v{}.py".format(ver))
            print "add new version success"

    pm.button("runFacialUiRigButton", e=True, bgc= (0.498039, 1, 0))

# cleanResolutions --------------------------
# cleanResolutions --------------------------
    # clean Skin
def cleanFile ( *args ) :
    from utaTools.utapy import cleanResolution3 as crl 
    reload(crl)

    crl.clean();
    # efc.lockMdv(listMdv = ['eyeLid_lft_mdv', 'eyeLid_rgt_mdv'], lock = False);
    # clean Dtl
def cleanDtlFile( *args ) :
    from utaTools.utapy import cleanResolution3 as crl 
    reload(crl)

    crl.cleanDtl();
    # clean Bsh
def cleanBshFile ( *args ) :
    from utaTools.utapy import cleanResolution3 as crl 
    reload(crl)

    crl.cleanBsh();
    # clean FacialSq
def cleanFacialSqFile ( *args ) :
    from utaTools.utapy import cleanResolution3 as crl 
    reload(crl)

    crl.cleanFacialSq();
    # connectDtl
def connectDtlFile ( *args ) :
    from utaTools.utapy import cleanResolution3 as crl 
    reload(crl)

    crl.connectDtl();
    # connectBsn
def connectBsnFile ( *args ) :
    from utaTools.utapy import cleanResolution3 as crl 
    reload(crl)

    crl.connectBsn();
    # connect FacialSQ
def connectFacialSqFile ( *args ) :
    from utaTools.utapy import cleanResolution3 as crl 
    reload(crl)

    # crl.connectFacialSq();
    crl.connectFacialSqRun()
    # lock eyeLid Joint
def disconEyeLid( *args ) :
    from utaTools.utapy import eyelidFollowConnect as efc
    reload(efc)

    efc.discon()
    # efc.lockEyeLid()
def cleanFileAndLockEyeLid( *args ) :
    from utaTools.utapy import eyelidFollowConnect as efc
    reload(efc)
    from utaTools.utapy import cleanResolution3 as crl 
    reload(crl)

    crl.clean();
    efc.lockAttrs(listObj = ['EyeLidsBsh_L_Jnt','EyeLidsBsh_R_Jnt'], lock = True);
    efc.lockMdv(listObj = ['eyeLid_lft_mdv', 'eyeLid_rgt_mdv', 'legIkStrtAmp_lft_mdv', 'legIkStrtAmp_rgt_mdv', 'toeIkStrt_lft_mdv', 'toeIkStrt_rgt_mdv'], lock = True);
    efc.lockBcl(listObj = ['legIkStrt_lft_bcl', 'legIkStrt_rgt_bcl', 'legIkLock_lft_bcl', 'legIkLock_rgt_bcl'], lock = True);
    efc.lockRev(listObj = ['ankleIkLocWor_lft_rev', 'ankleIkLocWor_rgt_rev'], lock = True);
    efc.lockPma(listObj = ['toeIkStrt_lft_pma', 'toeIkStrt_rgt_pma'], lock = True);
    efc.lockMidFacialSq_Ctrl(listObj = ['HdMidHdDfmRig_Ctrl'], lock = True);
    efc.lockUpLowFacialSq_Ctrl(listObj = ['HdUpHdDfmRig_Ctrl', 'HdLowHdDfmRig_Ctrl'], lock = True);

# add ebRig------------------------------
# add ebRig------------------------------
def addTxtFieldBodySide(*args):
    sel = pm.ls(sl=True) ;
    if sel != []:
        sel = sel [0] ;
        pm.textField("txtFieldBodySide", e = True  , text = sel)
    else:
        pm.textField("txtFieldBodySide", e = True  , text = "")  
        print "Please select Object !!"
        
def addTxtFieldEbSide(*args):
    sel = pm.ls(sl=True) ;
    if sel != []:
        sel = sel [0] ;
        pm.textField("txtFieldEbSide", e = True  , text = sel)
    else:
        pm.textField("txtFieldEbSide", e = True  , text = "")
        print "Please select Object !!"

def addTxtFieldBodyEach(*args):
    sel = pm.ls(sl=True) ;
    if sel != []:
        sel = sel [0] ;
        pm.textField("txtFieldBodyEach", e = True  , text = sel)   
    else:
        pm.textField("txtFieldBodyEach", e = True  , text = "") 
        print "Please select Object !!"

def addTxtFieldEbEach(*args):
    sel = pm.ls(sl=True) ;
    if sel != []:
        sel = sel [0] ;
        pm.textField("txtFieldEbEach", e = True  , text = sel)
    else:
        pm.textField("txtFieldEbEach", e = True  , text = "")
        print "Please select Object !!"

def addTxtFieldEbJntL(*args):
    sel = pm.ls(sl=True) ;
    if sel != []:
        sel = sel [0] ;
        pm.textField("txtFieldEbJntL", e = True  , text = sel)
    else: 
        pm.textField("txtFieldEbJntL", e = True  , text = "")
        print "Please select Object !!"

def addTxtFieldEbJntR(*args):
    sel = pm.ls(sl=True) ;
    if sel != []:
        sel = sel [0] ;
        pm.textField("txtFieldEbJntR", e = True  , text = sel)
    else:
        pm.textField("txtFieldEbJntR", e = True  , text = "")
        print "Please select Object !!"

def addTxtFieldEbInJntL(*args):
    sel = pm.ls(sl=True) ;
    if sel != []:
        sel = sel [0] ;
        pm.textField("txtFieldEbInJntL", e = True  , text = sel)
    else:
        pm.textField("txtFieldEbInJntL", e = True  , text = "")
        print "Please select Object !!"

def addTxtFieldEbInJntR(*args):
    sel = pm.ls(sl=True) ;
    if sel != []:
        sel = sel [0] ;
        pm.textField("txtFieldEbInJntR", e = True  , text = sel)
    else:
        pm.textField("txtFieldEbInJntR", e = True  , text = "")
        print "Please select Object !!"

def addTxtFieldEbMidJntL(*args):
    sel = pm.ls(sl=True) ;
    if sel != []:
        sel = sel [0] ;
        pm.textField("txtFieldEbMidJntL", e = True  , text = sel)
    else:
        pm.textField("txtFieldEbMidJntL", e = True  , text = "")
        print "Please select Object !!"

def addTxtFieldEbMidJntR(*args):
    sel = pm.ls(sl=True) ;
    if sel != []:
        sel = sel [0] ;
        pm.textField("txtFieldEbMidJntR", e = True  , text = sel)
    else:
        pm.textField("txtFieldEbMidJntR", e = True  , text = "")
        print "Please select Object !!"

def addTxtFieldEbOutJntL(*args):
    sel = pm.ls(sl=True) ;
    if sel != []:
        sel = sel [0] ;
        pm.textField("txtFieldEbOutJntL", e = True  , text = sel)
    else:
        pm.textField("txtFieldEbOutJntL", e = True  , text = "")
        print "Please select Object !!"

def addTxtFieldEbOutJntR(*args):
    sel = pm.ls(sl=True) ;
    if sel != []:
        sel = sel [0] ;
        pm.textField("txtFieldEbOutJntR", e = True  , text = sel)
    else:
        pm.textField("txtFieldEbOutJntR", e = True  , text = "")
        print "Please select Object !!"

def addTxtFieldBodyAllEyebrowUp(*args):
    sel = pm.ls(sl=True) ;
    if sel != []:
        sel = sel [0] ;
        pm.textField("txtFieldBodyAllEyebrowUp", e = True  , text = sel)
    else:
        pm.textField("txtFieldBodyAllEyebrowUp", e = True  , text = "")
        print "Please select Object !!"

def addTxtFieldEbAllEyebrowUp(*args):
    sel = pm.ls(sl=True) ;
    if sel != []:
        sel = sel [0] ;
        pm.textField("txtFieldEbAllEyebrowUp", e = True  , text = sel)
    else:
        pm.textField("txtFieldEbAllEyebrowUp", e = True  , text = "")
        print "Please select Object !!"

def addTxtFieldBodyAllEyebrowDn(*args):
    sel = pm.ls(sl=True) ;
    if sel != []:
        sel = sel [0] ;
        pm.textField("txtFieldBodyAllEyebrowDn", e = True  , text = sel)
    else:
        pm.textField("txtFieldBodyAllEyebrowDn", e = True  , text = "")
        print "Please select Object !!"

def addTxtFieldEbAllEyebrowDn(*args):
    sel = pm.ls(sl=True) ;
    if sel != []:
        sel = sel [0] ;
        pm.textField("txtFieldEbAllEyebrowDn", e = True  , text = sel)
    else:
        pm.textField("txtFieldEbAllEyebrowDn", e = True  , text = "")
        print "Please select Object !!"

def addTxtFieldBodyAllEyebrowIn(*args):
    sel = pm.ls(sl=True) ;
    if sel != []:
        sel = sel [0] ;
        pm.textField("txtFieldBodyAllEyebrowIn", e = True  , text = sel)
    else:
        pm.textField("txtFieldBodyAllEyebrowIn", e = True  , text = "")
        print "Please select Object !!"

def addTxtFieldEbAllEyebrowIn(*args):
    sel = pm.ls(sl=True) ;
    if sel != []:
        sel = sel [0] ;
        pm.textField("txtFieldEbAllEyebrowIn", e = True  , text = sel)
    else:
        pm.textField("txtFieldEbAllEyebrowIn", e = True  , text = "")
        print "Please select Object !!"

def addTxtFieldBodyAllEyebrowOut(*args):
    sel = pm.ls(sl=True) ;
    if sel != []:
        sel = sel [0] ;
        pm.textField("txtFieldBodyAllEyebrowOut", e = True  , text = sel)
    else:
        pm.textField("txtFieldBodyAllEyebrowOut", e = True  , text = "")
        print "Please select Object !!"

def addTxtFieldBodyAllEyebrowRotUp(*args):
    sel = pm.ls(sl=True) ;
    if sel != []:
        sel = sel [0] ;
        pm.textField("txtFieldBodyAllEyebrowRotUp", e = True  , text = sel)
    else:
        pm.textField("txtFieldBodyAllEyebrowRotUp", e = True  , text = "")
        print "Please select Object !!"

def addTxtFieldEyebrowsAllEyebrowRotUp(*args):
    sel = pm.ls(sl=True) ;
    if sel != []:
        sel = sel [0] ;
        pm.textField("txtFieldEyebrowsAllEyebrowRotUp", e = True  , text = sel)
    else:
        pm.textField("txtFieldEyebrowsAllEyebrowRotUp", e = True  , text = "")
        print "Please select Object !!"

def addTxtFieldBodyAllEyebrowRotDn(*args):
    sel = pm.ls(sl=True) ;
    if sel != []:
        sel = sel [0] ;
        pm.textField("txtFieldBodyAllEyebrowRotDn", e = True  , text = sel)
    else:
        pm.textField("txtFieldBodyAllEyebrowRotDn", e = True  , text = "")
        print "Please select Object !!"

def addTxtFieldEyebrowsAllEyebrowRotDn(*args):
    sel = pm.ls(sl=True) ;
    if sel != []:
        sel = sel [0] ;
        pm.textField("txtFieldEyebrowsAllEyebrowRotDn", e = True  , text = sel)
    else:
        pm.textField("txtFieldEyebrowsAllEyebrowRotDn", e = True  , text = "")
        print "Please select Object !!"


def addTxtFieldEbAllEyebrowOut(*args):
    sel = pm.ls(sl=True) ;
    if sel != []:
        sel = sel [0] ;
        pm.textField("txtFieldEbAllEyebrowOut", e = True  , text = sel)
    else:
        pm.textField("txtFieldEbAllEyebrowOut", e = True  , text = "")
        print "Please select Object !!"


def splitEbShapeFile(*args):
    from utaTools.utapy import splitEbShape as ses 
    reload(ses)

    ses.splitEbShape()
    print '....Split Eb Shape is Done!!....'
    pm.button("copyShapeToBsh", e=True, bgc= (0.498039, 1, 0))

def addSelectionOnField(field='', butt='', *args):
    sel = pm.selected()
    if sel:
        pm.textField(field, e=True, tx=sel[0])
        pm.button(butt, e=True, bgc=[0.498039, 1, 0])

# facial CombRig----------------------------------------------------------------------
# facial CombRig----------------------------------------------------------------------
#1. reference allObj
def mainGroup(*args):
    from utaTools.utapy import utaFclRig as fclRig 
    reload(fclRig)

    projectName = checkProject()
    if projectName in projectData['project']:
        fclRig.createMainGroup()
        mc.delete('FclRigRvt_Grp')
        pm.button("cteateMainGroupButton", e=True, bgc= (0.498039, 1, 0))
    else: 
        fclRig.createMainGroup()
        pm.button("cteateMainGroupButton", e=True, bgc= (0.498039, 1, 0))        
def refBshRig(*args):
    from utaTools.utapy import utaRefElem 
    reload(utaRefElem)

    projectName = checkProject()
    if projectName in projectData['project']:
        textFieldBshRig = pm.textField("txtField7BshRig", q=True, text=True) 
        utaRefElem.refElem(elem = textFieldBshRig)
        pm.button("refBsh7RigButton", e=True, bgc= (0.498039, 1, 0))
    else:
        txtFieldTyOffsets = pm.textField ( "txtFieldBodyBshRig_Geo" , q  = True , text = True  )
        utaRefElem.refElem(elem = txtFieldTyOffsets)
        pm.button("refBshRigButton", e=True, bgc= (0.498039, 1, 0))

def refHdRig(*args):
    from utaTools.utapy import utaRefElem 
    reload(utaRefElem)

    projectName = checkProject()
    if projectName in projectData['project']:
        textFieldHdDfmRig = pm.textField("txtField7HdDfmRig", q=True, text=True) 
        utaRefElem.refElem(elem = textFieldHdDfmRig)
        pm.button("refHd7RigButton", e=True, bgc= (0.498039, 1, 0))
    else:
        txtFieldTyOffsets = pm.textField ( "txtFieldBodyBshRig_Geo" , q  = True , text = True  )
        utaRefElem.refElem(elem = txtFieldTyOffsets)
        pm.button("refHdRigButton", e=True, bgc= (0.498039, 1, 0))

# def refHdRig(*args):
#     from utaTools.utapy import utaRefElem 
#     reload(utaRefElem)

#     texFieldHdRig = pm.textField("txtFieldHdRig", q=True, text=True)
#     utaRefElem.refElem(elem = texFieldHdRig)
#     pm.button("refHdRigButton", e=True, bgc= (0.498039, 1, 0))

def refEyeRig(*args):   
    from utaTools.utapy import utaRefElem 
    reload(utaRefElem)

    textFieldEyeRig = pm.textField("txtFieldEyeRig", q=True, text=True) 
    utaRefElem.refElem(elem = textFieldEyeRig)
    pm.button("refEyeRigButton", e=True, bgc= (0.498039, 1, 0))

def refEbRig(*args):
    from utaTools.utapy import utaRefElem 
    reload(utaRefElem)

    textFieldEbRig = pm.textField("txtFieldEbRig", q=True, text=True) 
    utaRefElem.refElem(elem= textFieldEbRig)
    pm.button("refEbRigButton", e=True, bgc= (0.498039, 1, 0))

def refMthRig(*args):
    from utaTools.utapy import utaRefElem 
    reload(utaRefElem)

    projectName = checkProject()
    if projectName in projectData['project']:
        textFieldMthRig = pm.textField("txtField7MthRig", q=True, text=True) 
        utaRefElem.refElem(elem = textFieldMthRig)
        pm.button("refMth7RigButton", e=True, bgc= (0.498039, 1, 0))
    else:
        textFieldMthRig = pm.textField("txtFieldMthRig", q=True, text=True) 
        utaRefElem.refElem(elem = textFieldMthRig)
        pm.button("refMthRigButton", e=True, bgc= (0.498039, 1, 0))

def refNsRig(*args):
    from utaTools.utapy import utaRefElem 
    reload(utaRefElem)

    textFieldNsRig = pm.textField("txtFieldNsRig", q=True, text=True) 
    utaRefElem.refElem(elem = textFieldNsRig)
    pm.button("refNsRigButton", e=True, bgc= (0.498039, 1, 0))

def refEarRig(*args):
    from utaTools.utapy import utaRefElem 
    reload(utaRefElem)

    textFieldEarRig = pm.textField("txtFieldEarRig", q=True, text=True) 
    utaRefElem.refElem(elem = textFieldEarRig)
    pm.button("refEarRigButton", e=True, bgc= (0.498039, 1, 0))

def refTtRig(*args): 
    from utaTools.utapy import utaRefElem 
    reload(utaRefElem)

    projectName = checkProject()
    if projectName in projectData['project']:
        textField7TtRig = pm.textField("txtField7TtRig", q=True, text=True) 
        utaRefElem.refElem(elem = textField7TtRig)
        pm.button("refTt7RigButton", e=True, bgc= (0.498039, 1, 0))
    else:
        textFieldTtRig = pm.textField("txtFieldTtRig", q=True, text=True) 
        utaRefElem.refElem(elem = textFieldTtRig)
        pm.button("refTtRigButton", e=True, bgc= (0.498039, 1, 0))        

def refDtlRig(*args):
    from utaTools.utapy import utaRefElem 
    reload(utaRefElem)

    projectName = checkProject()
    if projectName in projectData['project']:
        textField7DtlRig = pm.textField("txtField7DtlRig", q=True, text=True) 
        utaRefElem.refElem(elem = textField7DtlRig)
        pm.button("refDtl7RigButton", e=True, bgc= (0.498039, 1, 0))
    else:
        textFieldDtlRig = pm.textField("txtFieldDtlRig", q=True, text=True) 
        utaRefElem.refElem(elem = textFieldDtlRig)
        pm.button("refDtlRigButton", e=True, bgc= (0.498039, 1, 0))

def refWrinkledRig(*args):
    from utaTools.utapy import utaRefElem 
    reload(utaRefElem)

    projectName = checkProject()
    if projectName in projectData['project']:
        textField7WrinkledRig = pm.textField("txtField7WrinkledRig", q=True, text=True) 
        utaRefElem.refElem(elem = textField7WrinkledRig)
        pm.button("refWrinkled7RigButton", e=True, bgc= (0.498039, 1, 0))
    # else:
    #     textFieldDtlRig = pm.textField("txtFieldWrinkledRig", q=True, text=True) 
    #     utaRefElem.refElem(elem = textFieldDtlRig)
    #     pm.button("refWrinkledRigButton", e=True, bgc= (0.498039, 1, 0))

# connect Fcl ------------------------------
def connectHdRig(*args):
    from utaTools.utapy import utaFclRig as fclRig 
    reload(fclRig)
    projectName = checkProject()
    print projectData, '...projectData...'
    print projectName, '....projectName'
    if projectName in projectData['project']:
        fclRig.coonect7HdRig()
        print 'xx1'
        pm.button("con7TtRigButton", e=True, bgc= (0.498039, 1, 0))
    else:
        #3. FclRig HdRig
        fclRig.createHdRigSpaceGroup()
        fclRig.parentHdRig()
        pm.button("conHdRigButton", e=True, bgc= (0.498039, 1, 0))

def connectEyeRig(*args):
    from utaTools.utapy import utaFclRig as fclRig 
    reload(fclRig)

    #4. FclRig EyeRig
    fclRig.parentEyeRig()
    fclRig.stickEyeCtrl()
    pm.button("conEyeRigButton", e=True, bgc= (0.498039, 1, 0))

def connectEbRig(*args):
    from utaTools.utapy import utaFclRig as fclRig 
    reload(fclRig)

    #5. FclRig EbRig
    fclRig.parentEbRig()
    fclRig.stickEbCtrl()
    pm.button("conEbRigButton", e=True, bgc= (0.498039, 1, 0))

def connectMthRig(*args):
    from utaTools.utapy import utaFclRig as fclRig 
    reload(fclRig)

    #6. FclRig MthRig
    projectName = checkProject()
    if projectName in projectData['project']:
        fclRig.parent7MthRig()
        pm.button("con7MthRigButton", e=True, bgc= (0.498039, 1, 0))
    else:
        fclRig.parentMthRig()
        fclRig.stickMthCtrl()
        pm.button("conMthRigButton", e=True, bgc= (0.498039, 1, 0))

def connectTtRig(*args):
    from utaTools.utapy import utaFclRig as fclRig 
    reload(fclRig)

    projectName = checkProject()
    if projectName in projectData['project']:
        fclRig.parent7TtRig()
        pm.button("con7TtRigButton", e=True, bgc= (0.498039, 1, 0))
    else:
        fclRig.parentTtRig()
        pm.button("conTtRigButton", e=True, bgc= (0.498039, 1, 0))

def connectNsRig(*args):
    from utaTools.utapy import utaFclRig as fclRig 
    reload(fclRig)

    #8. FclRig NsRig
    fclRig.parentNsRig()
    pm.button("conNsRigButton", e=True, bgc= (0.498039, 1, 0))

def connectEarRig(*args):
    from utaTools.utapy import utaFclRig as fclRig 
    reload(fclRig)

    #9. FclRig EarRig
    fclRig.parentEarRig()
    pm.button("conEarRigButton", e=True, bgc= (0.498039, 1, 0))


def connectDtlRig(*args):
    from utaTools.utapy import utaFclRig as fclRig 
    reload(fclRig)

    projectName = checkProject()
    if projectName in projectData['project']:
        fclRig.parent7DtlRig()
        fclRig.stick7DtlCtrl(postGeo='BodyFclRigPost_Geo')
        pm.button("con7DtlRigButton", e=True, bgc= (0.498039, 1, 0))
    else:
        fclRig.parentDtlRig()
        fclRig.stickDtlCtrl(postGeo='BodyFclRigPost_Geo')
        pm.button("conDtlRigButton", e=True, bgc= (0.498039, 1, 0))

def connectBshRig(*args):
    from utaTools.utapy import utaFclRig as fclRig 
    reload(fclRig)

    projectName = checkProject()
    if projectName in projectData['project']:
        fclRig.coonect7BshRig()
        pm.button("con7BshRigButton", e=True, bgc= (0.498039, 1, 0))
    else:
        fclRig.connectBshRig()
        pm.button("conBshRigButton", e=True, bgc= (0.498039, 1, 0))        

def connectWrinkledRig(butt='', *args):
    from utaTools.utapy import utaCore
    reload(utaCore)

    motherGeo = pm.textField("motherGeo_Txt", q=True, tx=True)
    geoGrp = pm.textField("geoGrp_Txt", q=True, tx=True)
    ctrlGrp = pm.textField("ctrlGrp_Txt", q=True, tx=True)
    stillGrp = pm.textField("stillGrp_Txt", q=True, tx=True)

    utaCore.groupWrinkledRig(motherGeo=motherGeo, geoGrp=geoGrp, ctrlGrp=ctrlGrp, stillGrp=stillGrp)
    pm.button(butt, e=True, bgc=[0.498039, 1, 0])

def connectWrinkledRigAuto(butt='', *args):
    from utaTools.utapy import utaFclRig
    reload(utaFclRig)
    utaFclRig.conect7WrinkledRig()
    # motherGeo = pm.textField("motherGeo_Txt", q=True, tx=True)
    # geoGrp = pm.textField("geoGrp_Txt", q=True, tx=True)
    # ctrlGrp = pm.textField("ctrlGrp_Txt", q=True, tx=True)
    # stillGrp = pm.textField("stillGrp_Txt", q=True, tx=True)

    # utaCore.groupWrinkledRig(motherGeo=motherGeo, geoGrp=geoGrp, ctrlGrp=ctrlGrp, stillGrp=stillGrp)
    pm.button('con7WrinkledRigButton', e=True, bgc=[0.498039, 1, 0])

# ReColor 
# ReColor
def reColorCombRig(*args):
    pm.button("hdRigButton", e=True, bgc= (0.411765, 0.411765, 0.411765))
    pm.button("eyeRigButton", e=True, bgc= (0.411765, 0.411765, 0.411765))
    pm.button("ebRigButton", e=True, bgc= (0.411765, 0.411765, 0.411765))
    pm.button("mthRigButton", e=True, bgc= (0.411765, 0.411765, 0.411765))
    pm.button("ttRigButton", e=True, bgc= (0.411765, 0.411765, 0.411765))
    pm.button("nsRigButton", e=True, bgc= (0.411765, 0.411765, 0.411765))
    pm.button("earRigButton", e=True, bgc= (0.411765, 0.411765, 0.411765))
    pm.button("dtlRigButton", e=True, bgc= (0.411765, 0.411765, 0.411765))
    pm.button("conHdRigButton", e=True, bgc= (0.411765, 0.411765, 0.411765))
    pm.button("conEyeRigButton", e=True, bgc= (0.411765, 0.411765, 0.411765))
    pm.button("conEbRigButton", e=True, bgc= (0.411765, 0.411765, 0.411765))
    pm.button("conMthRigButton", e=True, bgc= (0.411765, 0.411765, 0.411765))
    pm.button("conTtRigButton", e=True, bgc= (0.411765, 0.411765, 0.411765))
    pm.button("conNsRigButton", e=True, bgc= (0.411765, 0.411765, 0.411765))
    pm.button("conEarRigButton", e=True, bgc= (0.411765, 0.411765, 0.411765))
    pm.button("conDtlRigButton", e=True, bgc= (0.411765, 0.411765, 0.411765))
    pm.button("conBshRigButton", e=True, bgc= (0.411765, 0.411765, 0.411765))

def reStstusTmpRig(*args):

    pm.button("HdRigTmpLocButton", e=True, bgc= (0.941176, 0.972549, 1))
    pm.button("EyeRigTmpLocButton", e=True, bgc= (0.941176, 0.972549, 1))
    pm.button("EarRigTmpLocButton", e=True, bgc= (0.941176, 0.972549, 1))
    pm.button("NsRigTmpLocButton", e=True, bgc= (0.941176, 0.972549, 1))
    pm.button("TtRigTmpLocButton", e=True, bgc= (0.941176, 0.972549, 1))
    pm.button("EbRigTmpLocButton", e=True, bgc= (0.941176, 0.972549, 1))
    pm.button("MthRigTmpLocButton", e=True, bgc= (0.941176, 0.972549, 1))
    pm.button("DtlRigTmpLocButton", e=True, bgc= (0.941176, 0.972549, 1))

    pm.button("RemoveHdRigTmpLocButton", e=True, bgc= (0.411765, 0.411765, 0.411765))
    pm.button("RemoveEyeRigTmpLocButton", e=True, bgc= (0.411765, 0.411765, 0.411765))
    pm.button("RemoveEarRigTmpLocButton", e=True, bgc= (0.411765, 0.411765, 0.411765))
    pm.button("RemoveNsRigTmpLocButton", e=True, bgc= (0.411765, 0.411765, 0.411765))
    pm.button("RemoveTtRigTmpLocButton", e=True, bgc= (0.411765, 0.411765, 0.411765))
    pm.button("RemoveEbRigTmpLocButton", e=True, bgc= (0.411765, 0.411765, 0.411765))
    pm.button("RemoveMthRigTmpLocButton", e=True, bgc= (0.411765, 0.411765, 0.411765))
    pm.button("RemoveDtlRigTmpLocButton", e=True, bgc= (0.411765, 0.411765, 0.411765))

def reStatusMth(*args):
    from utaTools.utapy import checkStatusUi as csui 
    reload(csui)

    # checkStatusUi------------------------------------------------------------
    csui.checkStatusMthRig()
    # checkStatusUi------------------------------------------------------------
    # pm.button("CreateMthBshButton", e=True, bgc= (0.411765, 0.411765, 0.411765))
    pm.button("addGeoBaseToBsh", e=True, bgc= (0.411765, 0.411765, 0.411765))
    # pm.button("copyShapeToBsh", e=True, bgc= (0.411765, 0.411765, 0.411765))

    pm.button("MouthcopyShapeToBsh", e=True, bgc= (0.411765, 0.411765, 0.411765))
    pm.button("MouthRGTcopyShapeToBsh", e=True, bgc= (0.411765, 0.411765, 0.411765))
    pm.button("MouthLFTcopyShapeToBsh", e=True, bgc= (0.411765, 0.411765, 0.411765))
    pm.button("Create12CVButton", e=True, bgc= (0.411765, 0.411765, 0.411765))
    pm.button("CreateLoftButton", e=True, bgc= (0.411765, 0.411765, 0.411765))
    # pm.button("DuplicateProxyMthBshButton", e=True, bgc= (0.411765, 0.411765, 0.411765))

# fc Rig
def reStatusFc(*args):
    from utaTools.utapy import checkStatusUi as csui 
    reload(csui)

    # checkStatusUi------------------------------------------------------------
    csui.checkStatusFCRig()
    # checkStatusUi------------------------------------------------------------
def reStatusHdGeo(*args):
    from utaTools.utapy import checkStatusUi as csui 
    reload(csui)

    # checkStatusUi------------------------------------------------------------
    csui.checkStatusHdGeoRig()
    # checkStatusUi------------------------------------------------------------

#eb Rig
def reStatusEb(*args):
    from utaTools.utapy import checkStatusUi as csui 
    reload(csui)

    # checkStatusUi------------------------------------------------------------
    csui.checkStatusEbRig()
    # checkStatusUi------------------------------------------------------------
    pm.button("MirrorOrientationButton", e=True, bgc= (0.411765, 0.411765, 0.411765))
    pm.button("EbWeightDividerButton", e=True, bgc= (0.411765, 0.411765, 0.411765))
    pm.button("DuplicateEbFromBshButton", e=True, bgc= (0.411765, 0.411765, 0.411765))

# duplicate BodyFclRigPos_Geo --------------
def dupHeadFclRig(headSel='',*args):
#2. duplicate "BodyFclRigPos_Geo" and parent to FclRigStill_Grp
    if mc.objExists (headSel)==True:
        pass
    else :
        headSel = mc.ls("HeadFclRig_Geo")
    headGeoPos = mc.duplicate(headSel)
    headName = mc.rename(headGeoPos, "BodyFclRigPost_Geo")
    mc.parent(headName,w=True)
    mc.parent(headName, "FclRigStill_Grp")

def dupHeadFclRigFile(*args):
    from utaTools.utapy import utaFclRig as fclRig 
    reload(fclRig)

    suffix = pm.textField ( "txtFieldBodyFclRigPos" , q  = True , text = True  )
    fclRig.dupHeadFclRig(headSel = suffix)
    mc.parent('FclRigGeo_Grp', 'FclRigStill_Grp')
    pm.button("CreateBodyFclRigPos_GeoButton", e=True, bgc= (0.498039, 1, 0))
    
def addDupHeadFclRigFile(*args):
    sel = pm.ls(sl=True) ;
    if sel != []:
        sel = sel [0] ;
        pm.textField("txtFieldBodyFclRigPos", e = True  , text = sel)
    else:
        pm.textField("txtFieldBodyFclRigPos", e = True  , text = "")  
        print "Please select Object !!"
        pm.button("addBodyFclRigPos", e=True, bgc= (0.498039, 1, 0))

# create BlendShape Hd 2 Fcl
def createHd2Fcl(*args):
    from utaTools.utapy import utaFclRig as fclRig 
    reload(fclRig)

    hdRigGeoSuffix = pm.textField ( "txtFieldHdRigGeo" , q  = True , text = True  )
    fclRigGeoSuffix = pm.textField ( "txtFieldFclRigGeo" , q  = True , text = True  )
    fclRig.addBshHd2Fcl(hdRigGeo = hdRigGeoSuffix, fclRigGeo = fclRigGeoSuffix)

    pm.button("createBS2HdAutoButton", e=True, bgc= (0.498039, 1, 0))

def addCreateFclFile(*args):
    sel = pm.ls(sl=True) ;
    if sel != []:
        sel = sel [0] ;
        pm.textField("txtFieldFclRigGeo", e = True  , text = sel)
    else:
        pm.textField("txtFieldFclRigGeo", e = True  , text = "")  
        print "Please select Object !!"
        pm.button("addFclRigGeo", e=True, bgc= (0.498039, 1, 0))
def addCreateHdFile(*args):
    sel = pm.ls(sl=True) ;
    if sel != []:
        sel = sel [0] ;
        pm.textField("txtFieldHdRigGeo", e = True  , text = sel)
    else:
        pm.textField("txtFieldHdRigGeo", e = True  , text = "")  
        print "Please select Object !!"
        pm.button("addHdRigGeo", e=True, bgc= (0.498039, 1, 0))

def createBs2Head(*args):
    from utaTools.utapy import utaFclRig as fclRig 
    reload(fclRig)

    projectName = checkProject()
    if projectName in projectData['project']:
        fclRig.connect7SelectedToHeadDeform()
    else:
        fclRig.connectSelectedToHeadDeform()
    pm.button("createBS2HdButton", e=True, bgc= (0.498039, 1, 0))

def doAddBSH(*args):
    from lpRig import rigTools as lrr
    reload(lrr)

    lrr.doAddBlendShape()
    pm.button("addBSHButton", e=True, bgc= (0.498039, 1, 0))
    pm.button("BshBpmButton", e=True, bgc= (0.498039, 1, 0))


def doJawSqSt(*args):
    # import rftool.rig       
    from utaTools.utapy import utaFclRig        
    reload(utaFclRig)  

    utaFclRig.connectJawSquashStretchBsh()      

    pm.button("addJawMouthSqStButton", e=True, bgc= (0.498039, 1, 0))
    # pm.button("addEyelidUpDnButton", e=True, bgc= (0.498039, 1, 0))


def doAddBSHMthFile(*args):
    from lpRig import rigTools as lrr
    reload(lrr)

    lists01 = mc.ls('BodyMthJawRig_Geo', 'BodyMthLipRig_Geo', 'BodyMthRig_Geo')
    if lists01:
        mc.select(lists01[0])
        mc.select(lists01[1], add=True)
        mc.select(lists01[2], add=True)
        lrr.doAddBlendShape()
    lists02 = mc.ls('TeethMthJawRig_Geo', 'TeethMthLipRig_Geo', 'TeethMthRig_Geo')
    if lists02:
        mc.select(lists02[0])
        mc.select(lists02[1], add=True)
        mc.select(lists02[2], add=True)
        lrr.doAddBlendShape()
    lists03 = mc.ls('TongueMthJawRig_Geo', 'TongueMthLipRig_Geo', 'TongueMthRig_Geo')
    if lists03:
        mc.select(lists03[0])
        mc.select(lists03[1], add=True)
        mc.select(lists03[2], add=True)
        lrr.doAddBlendShape()

    pm.button("addBSHButton", e=True, bgc= (0.498039, 1, 0))

# def doAddBSHMthObj01File(*args):
#     mc.select("TeethMthJawRig_Geo")
#     mc.select("TeethMthLipRig_Geo", add=True)
#     mc.select("TeethMthRig_Geo", add=True)
#     lrr.doAddBlendShape()
#     pm.button("addBSHButton", e=True, bgc= (0.498039, 1, 0))
# def doAddBSHMthObj02File(*args):
#     mc.select("TongueMthJawRig_Geo")
#     mc.select("TongueMthLipRig_Geo", add=True)
#     mc.select("TongueMthRig_Geo", add=True)
#     lrr.doAddBlendShape()
#     pm.button("addBSHButton", e=True, bgc= (0.498039, 1, 0))  








def assemBler(*args):
    from utaTools.utapy import utaFclRig as fclRig 
    reload(fclRig)

    fclRig.fclRigAssembler()
    pm.button("assemBlerButton", e=True, bgc= (0.498039, 1, 0))


def doEyelidUpDn(*args):
    from ncmel import bshTools  
    reload(bshTools)    
    bshTools.connectLoLidUpDnLR()   

    pm.button("addEyelidUpDnButton", e=True, bgc= (0.498039, 1, 0))

def doEyeLidFollow(*args):
    ## connect EyeLidFollow (Run at main)   
    from utaTools.utapy import utaFclRig    
    reload(utaFclRig)   
    utaFclRig.connectEyeLidFollow() 

    pm.button("addEyelidFollowButton", e=True, bgc= (0.498039, 1, 0))


def doAttrForLipsealLR(*args):
    ## Connect  New Attribute LipsSeal L R      
    ## Connect Attr LipsSealL- R (Run At Main)      
    from utaTools.utapy import utaCore      
    reload(utaCore)     
    ## Con LipsSeal L (at main)     
    utaCore.con(objA = 'fclRig_md:MouthCnrBsh_L_Ctrl',       
                objB = 'bodyRig_md:JawLwr1_Ctrl',       
                attrA = ['LipsSeal'],       
                attrB = ['LipsSealL'])   

    ## Con LipsSeal R (at main)     
    utaCore.con(objA = 'fclRig_md:MouthCnrBsh_R_Ctrl',      
                objB = 'bodyRig_md:JawLwr1_Ctrl',       
                attrA = ['LipsSeal'],       
                attrB = ['LipsSealR'])     

    pm.button("addAttrForLipsealLRButton", e=True, bgc= (0.498039, 1, 0))

def doTtRigConnectScale(*args):   
    from utaTools.utapy import utaCore      
    reload(utaCore)   

    ## Connect  Scale For Mouth Control 
    utaCore.ttRigConnectScale() 
    pm.button("addTtRigConnectScaleButton", e=True, bgc= (0.498039, 1, 0))


def connectMthLipsSealFile(*args):
    from utaTools.utapy import utaFclRig as fclRig 
    reload(fclRig)

    fclRig.connectMth7Fcl()
    pm.button("connectLipsSealButton", e=True, bgc= (0.498039, 1, 0))



def doLatticeForEye(*args):
    ## Add Lattice for Eye      
    from utaTools.utapy import createDeform     
    reload(createDeform)    

    createDeform.CreateDeform().run(elem = 'Ltt')       
    pm.button("addLatticeForeyeButton", e=True, bgc= (0.498039, 1, 0))

def doTeethUpLowVis(*args):
    #print"# Generate >> Create Attribute TeethUpVis, TeethLowerVis"  
    from utaTools.utapy import utaCore      
    reload(utaCore)     

    utaCore.addAttrCtrl (   ctrl = 'bodyRig_md:JawLwr1_Ctrl',         
                            elem = ['TeethUpVis','TeethLowerVis'],          
                            min = 0,            
                            max = 1)       
    pm.button("addTeethUpLowVisButton", e=True, bgc= (0.498039, 1, 0))
         
def sqHairPaScaConstraintFile(*args):
    from utaTools.utapy import utaFclRig as fclRig 
    reload(fclRig)

    fclRig.sqHairPaScaConstraint()
    pm.button("sqHairButton", e =True, bgc= (0.498039, 1, 0))

def addFurGuideFile(*args):
    from utaTools.utapy import utaCore
    reload(utaCore)

    txtFieldBodyGeo = pm.textField ( "txtFieldInputBody_Geo" , q  = True , text = True  )
    txtFieldBodyFurGeo = pm.textField ( "txtFieldInputBodyFur_Geo" , q  = True , text = True  )
    txtFieldBodyFclRigWrap = pm.textField ( "txtFieldBodyFclRigWrapped_Geo" , q  = True , text = True  )
    utaCore.addFurGuide(BodyGeo = txtFieldBodyGeo, 
                        BodyFurGeo = txtFieldBodyFurGeo, 
                        BodyFclRigWrap = txtFieldBodyFclRigWrap)
    pm.button("addFurGuideButton", e =True, bgc= (0.498039, 1, 0))
#hdRig
def addRefElemPathFile (*args):
    sel = pm.ls(sl=True) ;
    if sel != []:
        sel = sel [0] ;
        pm.textField("txtFieldfacialCtrlAttr", e = True  , text = sel)
    else:
        pm.textField("txtFieldfacialCtrlAttr", e = True  , text = "")  
        print "Please select Object !!"
    pm.button("addFacialCtrlAttr2_0", e=True, bgc= (0.498039, 1, 0))

def add7RefElemPathFile (*args):
    sel = pm.ls(sl=True) ;
    if sel != []:
        sel = sel [0] ;
        pm.textField("txt7FieldfacialCtrlAttr", e = True  , text = sel)
    else:
        pm.textField("txt7FieldfacialCtrlAttr", e = True  , text = "")  
        print "Please select Object !!"
    pm.button("add7FacialCtrlAttr2_0", e=True, bgc= (0.498039, 1, 0))



def add7GroupNewHairFile (*args):
    strObj = ''
    obj = mc.ls(sl = True)
    for each in obj:
        if strObj == '':
            NewObj1=each.split('|')[-1]
            strObj = NewObj1
        else:
            NewObj2=each.split('|')[-1]
            strObj = strObj + ',' + NewObj2
    mc.textField("txt7FieldGroupNewHair", e=True , text=strObj) 
    pm.button("add7GroupNewHairButton", e=True, bgc= (0.498039, 1, 0))

def add7GroupNewHairBshFile (*args):

    strObj = ''
    sel = pm.ls(sl=True) ;
    for each in sel:
        if strObj == '':
            newObj1 = each.split('|') [-1]
            strObj = newObj1
        else:
            newObj2 = each.split('|') [-1]
            strObj = strObj + ',' + newObj2
    pm.textField("txt7FieldGroupNewHairBsh", e = True  , text = strObj)  

    pm.button("add7GroupNewHairButtonBsh", e=True, bgc= (0.498039, 1, 0))


def refElemPathFile(*args):
    from utaTools.utapy import utaRefElem 
    reload(utaRefElem)

    projectName = checkProject()
    if projectName in projectData['project']:
        txtFieldfacialCtrlAttr = pm.textField ( "txt7FieldfacialCtrlAttr" , q  = True , text = True  )
        utaRefElem.refElemPath(path = path + "ncmel/Template", elem = txtFieldfacialCtrlAttr)
        pm.button("add7FacialCtrlAttr3_0", e=True, bgc= (0.498039, 1, 0))
    else:
        txtFieldfacialCtrlAttr = pm.textField ( "txtFieldfacialCtrlAttr" , q  = True , text = True  )
        utaRefElem.refElemPath(path = path + "ppmel/template", elem = txtFieldfacialCtrlAttr)
        pm.button("addFacialCtrlAttr2_1", e=True, bgc= (0.498039, 1, 0))

def duplicateCheckBox(*args):
    eyebrowCheck =  pm.checkBox('eyebrowCheckBox', query=True, value=True)
    eyeCheck =  pm.checkBox('eyeCheckBox', query=True, value=True)
    mouthCheck =  pm.checkBox('mouthCheckBox', query=True, value=True)
    noseCheck =  pm.checkBox('noseCheckBox', query=True, value=True)
    headCheck =  pm.checkBox('headCheckBox', query=True, value=True)
    threeEyeCheck =  pm.checkBox('threeEyeCheckBox', query=True, value=True)

    return eyebrowCheck, eyeCheck, mouthCheck, noseCheck, headCheck, threeEyeCheck

def dupBlendShapeFile(*args):
    from utaTools.utapy import bshTools
    reload(bshTools)
    from ncmel import bshTools as bshTools7
    reload(bshTools7)
    from utaTools.utapy import utaCore
    reload(utaCore)
    projectName = checkProject()

    eyebrowCheck, eyeCheck, mouthCheck, noseCheck, headCheck, threeEyeCheck = duplicateCheckBox()
    if projectName in projectData['project']:
        bshTools7.dupBlendshape(            eyebrow = eyebrowCheck ,
                                            eye     = eyeCheck ,
                                            mouth   = mouthCheck ,
                                            nose    = noseCheck ,
                                            head    = headCheck ,
                                            threeEye = threeEyeCheck)
        bshTools7.connectBshNodeToLocAttr(  eyebrow = eyebrowCheck ,
                                            eye     = eyeCheck ,
                                            mouth   = mouthCheck ,
                                            nose    = noseCheck ,
                                            head    = headCheck ,
                                            threeEye = threeEyeCheck,
                                            bshName = 'FacialAll_Loc')
        pm.button("DuplicateBsh7Button2", e=True, bgc= (0.498039, 1, 0))
    else:
        bshTools.dupBlendshape()
        pm.button("DuplicateBsh7Button2", e=True, bgc= (0.498039, 1, 0))
    ##  Fix Rotate at mouth Control
    utaCore.fixMouthCtrl(   sels = ['MouthCnrBsh_L_Ctrl','MouthCnrBsh_R_Ctrl'],
                            term = ['Up', 'Lo'],
                            selsCen = ['MouthUprBsh_Ctrl', 'MouthLwrBsh_Ctrl'])
    ## Fix Nose Follow Mouth when move Left Right
    utaCore.fixMouthNose()   
    
def connectBshCtrlFile (*args):
    from utaTools.utapy import bshTools
    reload(bshTools)

    bshTools.connectBshCtrl(                eyeBrowLFT = 'eyeBrowBsh_lft_ctrl' ,
                                            eyeBrowRGT = 'eyeBrowBsh_rgt_ctrl' ,
                                            eyeLFT = 'eyeBsh_lft_ctrl' ,
                                            eyeRGT = 'eyeBsh_rgt_ctrl' ,
                                            nose = 'noseBsh_ctrl' ,
                                            mouth = 'mouthBsh_ctrl' ,
                                            mouthLFT = 'mouthBsh_lft_ctrl' ,
                                            mouthRGT = 'mouthBsh_rgt_ctrl' ,
                                            faceUpr = 'faceUprBsh_ctrl' ,
                                            faceLwr = 'faceLwrBsh_ctrl' ,
                                            bshName = 'facialAll_bsh' )

    pm.button("connectBshButton", e=True, bgc= (0.498039, 1, 0))

def connect7BshCtrlFile (*args):
    from ncmel import bshTools as bshTools7
    reload(bshTools7)    
    bshTools7.connectBshMoverCtrl( facialLoc = 'FacialAll_Loc',
                                    bshName = 'Facial*_Bsn',
                                    connection = True)

    pm.button("connect7BshButton", e=True, bgc= (0.498039, 1, 0))
    pm.button("disconnect7BshButton", e=True, bgc= (0.411765, 0.411765, 0.411765))



def disconnect7BshCtrlFile (*args):
    from ncmel import bshTools as bshTools7
    reload(bshTools7)

    bshTools7.connectBshMoverCtrl( facialLoc = 'FacialAll_Loc',
                                    bshName = 'Facial*_Bsn',
                                    connection = False)
    pm.button("disconnect7BshButton", e=True, bgc= (0.498039, 1, 0))
    pm.button("connect7BshButton", e=True, bgc= (0.411765, 0.411765, 0.411765))
    
def copyShapeBshButtonFile (*args):
    from utaTools.utapy import buildCopyShapeNewHair 
    reload(buildCopyShapeNewHair)

    GroupNewHairTxt7FieldBsh = pm.textField ( "txt7FieldGroupNewHairBsh" , q  = True , text = True  )
    GroupNewHairTxt7Field = pm.textField ( "txt7FieldGroupNewHair" , q  = True , text = True  )
    buildCopyShapeNewHair.copyShapeNewhair_cmd(NewObj = GroupNewHairTxt7Field, bshGrp = GroupNewHairTxt7FieldBsh)
    pm.button("copyShapeBshButton", e=True, bgc= (0.498039, 1, 0))

    # copyShapeBshButton
def duplicateBsh7UiFile(*args):
    from ncmel.add_blendshape import app
    reload(app)

    app.show()
    pm.button("buttonDuplicateBshUI", e=True, bgc= (0.498039, 1, 0))

def hdRigTmpLocFile(*args):
    from utaTools.utapy import utaRefElem 
    reload(utaRefElem)

    utaRefElem.refElemPath(path = path + "lpRig/lib/tmpLocs", elem = "HdRigTmpLocs")
    pm.button("HdRigTmpLocButton", e=True, bgc= (0.498039, 1, 0))
    pm.button("RemoveHdRigTmpLocButton", e=True, bgc= (1, 0.0784314, 0.576471))

def eyeRigTmpLocFile(*args):
    from utaTools.utapy import utaRefElem 
    reload(utaRefElem)

    utaRefElem.refElemPath(path = path + "lpRig/lib/tmpLocs", elem = "EyeRigTmpLocs")
    pm.button("EyeRigTmpLocButton", e=True, bgc= (0.498039, 1, 0))
    pm.button("RemoveEyeRigTmpLocButton", e=True, bgc= (1, 0.0784314, 0.576471))

def earRigTmpLocFile(*args):
    from utaTools.utapy import utaRefElem 
    reload(utaRefElem)

    utaRefElem.refElemPath(path = path + "lpRig/lib/tmpLocs", elem = "EarRigTmpLocs")
    pm.button("EarRigTmpLocButton", e=True, bgc= (0.498039, 1, 0))
    pm.button("RemoveEarRigTmpLocButton", e=True, bgc= (1, 0.0784314, 0.576471))

def nsRigTmpLocFile(*args):
    from utaTools.utapy import utaRefElem 
    reload(utaRefElem)

    utaRefElem.refElemPath(path = path + "lpRig/lib/tmpLocs", elem = "NsRigTmpLocs")
    pm.button("NsRigTmpLocButton", e=True, bgc= (0.498039, 1, 0))
    pm.button("RemoveNsRigTmpLocButton", e=True, bgc= (1, 0.0784314, 0.576471))

def ttRigTmpLocFile(*args):
    from utaTools.utapy import utaRefElem 
    reload(utaRefElem)

    utaRefElem.refElemPath(path = path + "lpRig/lib/tmpLocs", elem = "TtRigTmpLocs")
    pm.button("TtRigTmpLocButton", e=True, bgc= (0.498039, 1, 0))
    pm.button("RemoveTtRigTmpLocButton", e=True, bgc= (1, 0.0784314, 0.576471))

def ebRigTmpLocFile(*args):
    from utaTools.utapy import utaRefElem 
    reload(utaRefElem)

    utaRefElem.refElemPath(path = path + "lpRig/lib/tmpLocs", elem = "EbRigTmpLocs")
    pm.button("EbRigTmpLocButton", e=True, bgc= (0.498039, 1, 0))
    pm.button("RemoveEbRigTmpLocButton", e=True, bgc= (1, 0.0784314, 0.576471))

def mthRigTmpLocFile(*args):
    from utaTools.utapy import utaRefElem 
    reload(utaRefElem)

    projectName = checkProject()
    print projectName, 'projectName'
    if projectName in projectData['project']:
        utaRefElem.refElemPath(path = path + "ncmel/Template", elem = "MouthTmpCrv")
        pm.button("CreateMthTmpButton", e=True, bgc= (0.498039, 1, 0))
    else:
        utaRefElem.refElemPath(path = path + "lpRig/lib/tmpLocs", elem = "MthRigTmpLocs")
    pm.button("MthRigTmpLocButton", e=True, bgc= (0.498039, 1, 0))
    pm.button("RemoveMthRigTmpLocButton", e=True, bgc= (1, 0.0784314, 0.576471))

def dtlRigTmpLocFile(*args):
    from utaTools.utapy import utaRefElem 
    reload(utaRefElem)

    projectName = checkProject()
    if projectName in projectData['project']:
        utaRefElem.refElemPath(path = path + "ncmel/Template", elem = "DetailNrb")
        pm.button("DtlRigTmpLocButton", e=True, bgc= (0.498039, 1, 0))
    else:
        utaRefElem.refElemPath(path = path + "lpRig/lib/tmpLocs", elem = "DtlRigTmpLocs")
        pm.button("DtlRigTmpLocButton", e=True, bgc= (0.498039, 1, 0))
    pm.button("RemoveDtlRigTmpLocButton", e=True, bgc= (1, 0.0784314, 0.576471))

def removeHdRig(*args): 
    lists = mc.ls("*:NeckHdDfmRigTmp_Loc") 
    rf.removeSelectedReference(sels = lists)
    pm.button("HdRigTmpLocButton", e=True, bgc= (0.411765, 0.411765, 0.411765))
    pm.button("RemoveHdRigTmpLocButton", e=True, bgc= (0.941176, 0.972549, 1))

def removeEyeRig(*args):
    lists = mc.ls("*:LidRigTmpLoc_L_Grp") 
    rf.removeSelectedReference(sels = lists)
    pm.button("EyeRigTmpLocButton", e=True, bgc= (0.411765, 0.411765, 0.411765))
    pm.button("RemoveEyeRigTmpLocButton", e=True, bgc= (0.941176, 0.972549, 1))
def removeEarRig(*args): 
    lists = mc.ls("*:EarTmpJnt_Grp")
    rf.removeSelectedReference(sels = lists)
    pm.button("EarRigTmpLocButton", e=True, bgc= (0.411765, 0.411765, 0.411765))
    pm.button("RemoveEarRigTmpLocButton", e=True, bgc= (0.941176, 0.972549, 1))
def removeNsRig(*args): 
    lists = mc.ls("*:NsRigTmpLoc_Grp")
    rf.removeSelectedReference(sels = lists)
    pm.button("NsRigTmpLocButton", e=True, bgc= (0.411765, 0.411765, 0.411765))
    pm.button("RemoveNsRigTmpLocButton", e=True, bgc= (0.941176, 0.972549, 1))
def removeTtRig(*args): 
    lists = mc.ls("*:TtRigTmpLoc_Grp")
    rf.removeSelectedReference(sels = lists)
    pm.button("TtRigTmpLocButton", e=True, bgc= (0.411765, 0.411765, 0.411765))
    pm.button("RemoveTtRigTmpLocButton", e=True, bgc= (0.941176, 0.972549, 1))
def removeEbRig(*args): 
    lists = mc.ls("*:EbRbnTmpLoc_Grp")
    rf.removeSelectedReference(sels = lists)
    pm.button("EbRigTmpLocButton", e=True, bgc= (0.411765, 0.411765, 0.411765))
    pm.button("RemoveEbRigTmpLocButton", e=True, bgc= (0.941176, 0.972549, 1))
def removeMthRig(*args): 
    lists = mc.ls("*:LipRigTmpLoc_Grp")
    rf.removeSelectedReference(sels = lists)
    pm.button("MthRigTmpLocButton", e=True, bgc= (0.411765, 0.411765, 0.411765))
    pm.button("RemoveMthRigTmpLocButton", e=True, bgc= (0.941176, 0.972549, 1))
def removeDtlRig(*args): 
    lists = mc.ls("*:NsDtlRig_L_Loc")
    rf.removeSelectedReference(sels = lists)
    pm.button("DtlRigTmpLocButton", e=True, bgc= (0.411765, 0.411765, 0.411765))
    pm.button("RemoveDtlRigTmpLocButton", e=True, bgc= (0.941176, 0.972549, 1))

# import hdGeo file
def importHdGeoFile(*args):
    from utaTools.utapy import utaFclRig as fclRig 
    reload(fclRig)
    fclRig.importHdGeo()
    pm.button("ImportHdGeo", e=True, bgc= (0.498039, 1, 0))

# ebCreateBs
def ebCreateBsh(*args):
    from lpRig import eyebrowRig as eyebrowRig
    reload(eyebrowRig)

    sel = mc.ls(sl = True)[0]
    eyebrowRig.createBsh( sel , 'EbRig')  
    print '....Create BlendShape is Done!!....'
    pm.button("EbCreateBshButton", e=True, bgc= (0.498039, 1, 0))  
def ebmirrorOrientSelected(*args):
    from lpRig import rigTools as lrr
    reload(lrr)

    lrr.mirrorOrientSelected((0, 0 ,1), (0, 1, 0))
    pm.button("MirrorOrientationButton", e=True, bgc= (0.498039, 1, 0))
def ebWeightDivider(*args):
    from lpRig import weightDivider as weightDivider
    reload(weightDivider)

    weightDivider.run()
    pm.button("EbWeightDividerButton", e=True, bgc= (0.498039, 1, 0))

#Tmp Rig
def hdRigCtrl(*args):
    from utaTools.utapy import utaCore
    reload(utaCore)

    projectName = checkProject()
    if projectName in projectData['project']:
        from utaTools.utapy import headDeformRig as hdrUta
        reload(hdrUta)

        hdrUta.main() 
        utaCore.parentExportGrp()
        pm.symbolCheckBox("HdRigButton", e=True, bgc= (0.498039, 1, 0)) 


    else:
        from lpRig import headDeformRig as hdrlp
        reload(hdrlp)

        hdrlp.main() 
        utaCore.parentExportGrp()
        pm.symbolCheckBox("HdRigButton", e=True, bgc= (0.498039, 1, 0)) 

def eyeRigCtrl(*args):
    from lpRig import eyeRig as eyeRig
    reload(eyeRig)
    from utaTools.utapy import utaCore
    reload(utaCore)

    eyeRig.main() 
    utaCore.parentExportGrp()
    pm.symbolCheckBox("EyeRigButton", e=True, bgc= (0.498039, 1, 0))
def ebRigCtrl(*args):
    from lpRig import eyebrowRig as eyebrowRig
    reload(eyebrowRig)
    from utaTools.utapy import utaCore
    reload(utaCore)

    eyebrowRig.main(detail = False , lite = False)
    utaCore.parentExportGrp()
    pm.symbolCheckBox("EbRigButton", e=True, bgc= (0.498039, 1, 0))
def mthRigCtrl(*args):
    from lpRig import mouthRig as mouthRig
    reload(mouthRig)
    from ncmel import mouthRig as ncmelMouthRig
    reload(ncmelMouthRig)
    from utaTools.utapy import utaCore
    reload(utaCore)

    projectName = checkProject()
    if projectName in projectData['project']:
        fieldObjNumMthJnt = pm.textField ( "txtFieldNumMthJnt" , q  = True , text = True  )
        fieldObjSkipMthJntCen = pm.textField ( "txtFieldSkipMthJntCen" , q  = True , text = True  )
        ncmelMouthRig.Run(  lipsInTmpCuv     = 'LipsIn_CuvTemp' ,
                            lipsOutTmpCuv    = 'LipsOut_CuvTemp' ,
                            jawTmpJnt        = 'mthJawLwr2_Jnt' ,
                            headTmpJnt       = 'mthHead_Jnt' ,
                            jawCtrl          = 'mthJawLwr1_Loc' ,
                            numDtlJnt        =  int(fieldObjNumMthJnt) ,
                            skipJntCen       =  int(fieldObjSkipMthJntCen) ,
                            skinGrp          = '' ,
                            elem             = 'mth' ,
                            side             = '' ,
                        )
        mc.createNode('transform',n='MthRigGeo_Grp')
        utaCore.parentExportGrp()
        # pm.symbolCheckBox("CreateMthRigCtrlButton", e=True, bgc= (0.498039, 1, 0))

        ## Connect  Scale For Mouth Control


    else:
        mouthRig.main(lipRig=True)
        utaCore.parentExportGrp()
        pm.symbolCheckBox("MthRigButton", e=True, bgc= (0.498039, 1, 0))

def nsRigCtrl(*args):
    from lpRig import noseRig as nsRig
    reload(nsRig)
    from utaTools.utapy import utaCore
    reload(utaCore)

    nsRig.main()
    utaCore.parentExportGrp()
    pm.symbolCheckBox("NsRigButton", e=True, bgc= (0.498039, 1, 0))
def earRigCtrl(*args):
    from lpRig import earRig as earRig 
    reload(earRig)
    from utaTools.utapy import utaCore
    reload(utaCore)

    earRig.main()
    utaCore.parentExportGrp()
    pm.symbolCheckBox("EarRigButton", e=True, bgc= (0.498039, 1, 0))
def ttRigCtrl(*args):
    from lpRig import ttRig as ttRig
    reload(ttRig)
    from utaTools.utapy import utaCore
    reload(utaCore)

    ttRig.main()
    utaCore.parentExportGrp()
    utaCore.ttRigConnectScale()
    mc.createNode('transform',n='TtRigGeo_Grp')
    pm.symbolCheckBox("TtRigButton", e=True, bgc= (0.498039, 1, 0))
    # ss
def dtlRigCtrl(*args):
    from lpRig import detailRig as detailRig
    reload(detailRig)
    from utaTools.utapy import utaCore
    reload(utaCore)
    from utaTools.utapy import utaDetailRig
    reload(utaDetailRig)

    projectName = checkProject()
    if projectName in projectData['project']:
        utaDetailRig.runDetailRig(  shape = 'cube',
                                    mirror = 'OFF', 
                                    charSize = 1)
        pm.symbolCheckBox("DtlRigButton", e=True, bgc= (0.498039, 1, 0))
        mc.createNode('transform',n='DtlRigGeo_Grp')
    else:
        lists = mc.ls('*_Loc')
        mc.select(lists)
        detailRig.createDetailRigToSelected()
        utaCore.parentExportGrp()
        pm.symbolCheckBox("DtlRigButton", e=True, bgc= (0.498039, 1, 0))

def ebJntTmp(*args):
    # Eyebrow Joint
    mc.file( "%s/%s" % (path ,"lpRig/lib/tmpLocs/EbLocalJnts.ma") , r = True , ns = 'ebLocalJnt' )
    pm.button("ebJntTmpButton", e=True, bgc= (0.498039, 1, 0))

def removeSelectRef(*args):
    from lpRig import rigTools as lrr
    reload(lrr)

    lrr.removeSelectedReference()
    pm.button("removeSelectRefButton", e=True, bgc= (1, 0.0784314, 0.576471))
    pm.button("ebJntTmpButton", e=True, bgc= (0.411765, 0.411765, 0.411765))

def removeSelectRefFile(*args):
    from lpRig import rigTools as lrr
    reload(lrr)

    lrr.removeSelectedReference()
def importSelectRefFile(*args):
    from lpRig import rigTools as lrr
    reload(lrr)

    # lrr.importRigElement()
    lrr.importSelectedReference()  #import Reference
    # lrr.removeAllNamespace() 
    # lrr.removeSelectedReference()
# m
def createMthBsh(*args):
    from lpRig import mouthRig as mouthRig
    reload(mouthRig)

    sel = mc.ls(sl = True)[0]
    mouthRig.createBsh( sel , 'MthRig' )
    pm.button("CreateMthBshButton", e=True, bgc= (0.498039, 1, 0))

def createWrinkleRig(butt, *args):
    from utaTools.utapy import utaCore
    reload(utaCore)

    elem = pm.textField("elem_txt", q=True, tx=True)
    axis = pm.radioCollection("axis_radioCol", q=1, sl=1)

    if elem:
        if axis == "axis_x": isAxis = "x"
        elif axis == "axis_y": isAxis = "y"
        elif axis == "axis_z": isAxis = "z"

        utaCore.createWrinkledRig(elem, axis)
        pm.button(butt, e=True, bgc=[0.498039, 1, 0])

def fixWrinkleRig(*args):
    from utaTools.utapy import utaCore
    reload(utaCore)

    utaCore.fixWrinkledRig(elem = "WrinkledRig", process = "fclRig_md")

# copyShapde
def copyShadeFile(*args):
    from utaTools.utapy import copyShadeAll as cs 
    reload(cs)
    cs.cShade()
    pm.button("copyShadeButton", e=True, bgc= (0.498039, 1, 0))
    pm.button("paseShadeButton", e=True, bgc= (0.411765, 0.411765, 0.411765))
def paseShadeFile(*args): 
    from utaTools.utapy import copyShadeAll as cs 
    reload(cs)
    cs.pShade()
    pm.button("paseShadeButton", e=True, bgc= (0.498039, 1, 0))
    pm.button("copyShadeButton", e=True, bgc= (0.411765, 0.411765, 0.411765))

# Tools
def writeSelectedWeightFile(*args):
    from utaTools.pkmel import weightTools
    reload(weightTools)

    # write skinWeight--------------------------
    weightTools.writeSelectedWeight()
def readSelectedWeightFile(*args):
    from utaTools.pkmel import weightTools
    reload(weightTools)

    # read skinWeight----------------------------
    weightTools.readSelectedWeight()


def writeAllCtrlOutProjectFile(*args):
    from utaTools.pkmel import ctrlShapeTools
    reload(ctrlShapeTools)

    ctrlShapeTools.writeAllCtrl()

def readAllCtrlTmpDataFldFile(*args):
    from utaTools.pkmel import ctrlShapeTools
    reload(ctrlShapeTools)

    corePath = '%s/core' % os.environ.get('RFSCRIPT')
    path = '{}/rf_maya/rftool/rig/template/templatePrevis/data'.format(corePath)
    ctrlShapeTools.readAllCtrl(dataFld = path)

def readAllCtrlOutProjectFile(*args):
    from utaTools.pkmel import ctrlShapeTools
    reload(ctrlShapeTools)

    ctrlShapeTools.readAllCtrl(dataFld = '')

def writeAllCtrlFile(*args):
    from rf_maya.rftool.rig.utils.crvScript import ctrlData
    reload(ctrlData)
    from utaTools.utapy import utaCore
    reload(utaCore)
    import os
    from rf_utils.context import context_info
    reload(context_info)

    ## Check In Of Out Project path
    optionPath = utaCore.checkOptionBoxWrite(labelA = 'In Project', labelB = 'Out Project')
    if optionPath == 'In Project':
        ## check path path file
        asset = context_info.ContextPathInfo()
        projectPath = os.environ['RFPROJECT']
        if 'P:' in projectPath:
            fileWorkPath = (asset.path.name()).replace('$RFPROJECT',projectPath)
            filePath = fileWorkPath.replace('work','publ')
            charName = filePath.split('/')[-1]
            charPath = filePath.replace(charName, '')
            ## check and create Folder
            utaCore.createFolderPath(entityFirst = charPath, workOrPubl = 'publ',  charName = charName,createProcess = ['data'])
            ## write data ctrl to data folder
            ctrlData.write_ctrl_dataFld()

    elif optionPath == 'Out Project':
        writeAllCtrlOutProjectFile()


def readAllCtrlFile(*args):
    from rf_maya.rftool.rig.utils.crvScript import ctrlData
    reload(ctrlData)
    from utaTools.utapy import utaCore
    reload(utaCore)

    ## Check In Of Out Project path
    optionPath = utaCore.checkOptionBoxRead(labelA = 'In Project', labelB = 'Template "data" Fld', labelC = 'Out Project')
    if optionPath == 'In Project':
        ctrlData.read_ctrl_dataFld()
    elif optionPath == 'Template "data" Fld':
        readAllCtrlTmpDataFldFile()
    elif optionPath == 'Out Project':
        readAllCtrlOutProjectFile()


def controlFile(*args):
    from utaTools.utapy import CreateControlUI
    reload(CreateControlUI)

    # mm.eval('source "D:/Ken_Pipeline/core/rf_maya/rftool/rig/utaTools/utamel/utaControl.mel"')
    # mm.eval("utaControl()")
    CreateControlUI.createControlUI()

# def createControlFile(*args):
#     mm.eval('source "D:/Ken_Pipeline/core/rf_maya/rftool/rig/utaTools/utamel/kenQuadCore.mel"')
#     mm.eval("utaControl()")
    
    
def paintSkinWeightAFile(*args):
    mm.eval("ArtPaintSkinWeightsTool")

def paintSkinWeightBFile(*args):
    from utaTools.utapy import averageVertexSkinWeightBrush as AVSWBrush
    reload(AVSWBrush)

    try:
        AVSWBrush.paint()
    except:pass
def locatorOnMidPosFile(*args):
    from lpRig import rigTools as lrr
    reload(lrr)

    lrr.locatorOnMidPos()
def snapFile(*args):
    from utaTools.utapy import utaCore
    reload(utaCore)

    utaCore.snap()
def dlraFile(*args):
    nodes = mc.ls( type='transform' ) + mc.ls( type='joint' )
    for node in nodes :
        if mc.getAttr( '%s.displayLocalAxis' % node ) :
            mc.setAttr( '%s.displayLocalAxis' % node , 0 )

def lraFile(*args):
    mm.eval("ToggleLocalRotationAxes")

def symMesfile(*args):
    from utaTools.nuTools.util import symMeshReflector2 as smr
    reload(smr)

    smrObj = smr.SymMeshReflectors()
    smrObj.UI()

def abSymMesfile(*args):
    mm.eval('source "O:/Pipeline/core/rf_maya/rftool/rig/utaTools/utamel/abSymMesh.mel"')
    mm.eval("abSymMesh()")
def createJntFile(*args):
    mm.eval("createNode joint");
def localWorldFile(*args):
    mm.eval('source "O:/Pipeline/core/rf_maya/rftool/rig/utaTools/utamel/aLocalWorld.mel"')
    mm.eval("aLocalWorld()")
def keyFrameFile(*args):
    mm.eval('source "O:/Pipeline/core/rf_maya/rftool/rig/utaTools/utamel/keyOffset.mel"')
    mm.eval("keyOffset()")
def copySkinWeightAFile(*args):
    from utaTools.nuTools import misc
    reload(misc)

    misc.copySkinWeight(removeUnuse=True, cleanUnuseShp=True)
def copySkinWeightBFile(*args):
    from utaTools.pkmel import weightTools
    reload(weightTools)

    weightTools.copySelectedWeight()
def cometRenameFile(*args):
    mm.eval('source "O:/Pipeline/core/rf_maya/rftool/rig/utaTools/utamel/cometRename.mel"')
    mm.eval("cometRename()")
def nuRenameFile(*args):
    from utaTools.nuTools.util import renamer
    reload(renamer)

    renamer.Renamer()
def pkCopyCrvShapeUiFile(*args):
    mm.eval('source "O:/Pipeline/core/rf_maya/rftool/rig/utaTools/utamel/pkCopyCrvShapeUi.mel"')
    mm.eval("pkCopyCrvShapeUi()")
def scaleGmblFile(*args):
    from utaTools.pkrig import rigTools as upr 
    reload(upr)

    upr.scaleGimbalControl()
def createGroupFile(*args):
    from utaTools.pkmel import rigTools as rigTools
    reload( rigTools )

    rigTools.UtaDoZeroGroup()
def deleteUnlockNodeFile(*args):
    mm.eval("lockNode -l-0;delete;")
def unlockNodeFile(*args):
    mm.eval("lockNode -l-0;")
def fileTextureManagerFile(*args):
    mm.eval('source "O:/Pipeline/core/rf_maya/rftool/rig/utaTools/utamel/FileTextureManager.mel"')
    mm.eval("FileTextureManager()")
def taToolsFile(*args):
    from utaTools.scripts import genUI as genUI
    reload(genUI)

    genUI.genUI()
def peckToolsFile(*args):
    from utaTools.pkmel import controlMaker
    reload(controlMaker)

    controlMaker.run()
def skinToolsFile(*args):
    from utaTools.pkmel import skinTools
    reload(skinTools)

    skinTools.run()
def addDtlFile(*args):
    from utaTools.utapy import addDetailCtrl3Ui as adtUi
    reload(adtUi)

    adtUi.runUI()
def weightPullerFile(*args):
    from lpRig import weightPuller
    reload(weightPuller)

    weightPuller.run()
def deleteSelectEdgeFile(*args):
    from lpRig import rigTools as lrr
    reload(lrr)

    lrr.detachSelectedEdge()# cut HeadBody  @ bsh
def LRAFile(*args):
    mm.eval('source "O:/Pipeline/core/rf_maya/rftool/rig/utaTools/utamel/FileTextureManager.mel"')
    mm.eval("ToggleLocalRotationAxes")
def DLRAFile(*args):
    # import maya.cmds as mc
    nodes = mc.ls( type='transform' ) + mc.ls( type='joint' )
    for node in nodes :
        if mc.getAttr( '%s.displayLocalAxis' % node ) :
            mc.setAttr( '%s.displayLocalAxis' % node , 0 )
def selectJntFile(*args):
    mc.select(mc.ls(typ="joint"))
def createEyeBshFile(*args):
    from lpRig import eyeRig as eyeRig
    reload(eyeRig)

    eyeRig.doCreateBsh()
def copyShapeDHFile(*args):
    sels = mc.ls(sl=True) 
    for each in sels:
        pm.delete ( each , ch = True ) ;  
    mm.eval('source "O:/Pipeline/core/rf_maya/rftool/rig/utaTools/utamel/copyShape.mel"')
    mm.eval("copyShape()")
def copyShapeFile(*args):
    sels = mc.ls(sl=True) 
    mm.eval('source "O:/Pipeline/core/rf_maya/rftool/rig/utaTools/utamel/copyShape.mel"')
    mm.eval("copyShape ()")
def mirrorShapeFile(*args):
    mm.eval('source "O:/Pipeline/core/rf_maya/rftool/rig/utaTools/utamel/mirrorShape.mel"')
    mm.eval('mirrorShape ("YZ" , "0" , "0" , "1" , "0.001" )')
def copyShapePositionFile(*args):
    from lpRig import rigTools as lrr
    reload(lrr)

    # CopyWeight
    lrr.copyWeightBasedOnWorldPosition()
def transferShadeAssignFile(*args):
    from utaTools.nuTools import misc
    reload(misc)

    misc.transferShadeAssign()
def snapFile(*args):
    from utaTools.utapy import utaCore
    reload(utaCore)

    utaCore.snap()

def copyUVFile(*args):
    from utaTools.nuTools.util import geoReplacer
    reload(geoReplacer)

    geoReplacerObject = geoReplacer.GeoReplacer() 
    geoReplacerObject.UI()
def create12CVFile(*args):
    mm.eval("circle -ch on -o on -nr 0 1 0 -r 3.519638 -s 12 ;")
    pm.button("Create12CVButton", e=True, bgc= (0.498039, 1, 0))
def createLoftFile(*args):
    sels = mc.ls( selection = True )
    pm.loft(sels[0], sels[1], ch = 1, u = 1, c = 0, ar = 1, d = 3, ss = 1, rn = 0, po = 0, rsn = True)
    # loft -ch 1 -u 1 -c 0 -ar 1 -d 3 -ss 1 -rn 0 -po 0 -rsn true "nurbsCircle4" "nurbsCircle5";
    pm.button("CreateLoftButton", e=True, bgc= (0.498039, 1, 0))
def dupProxyMthBshFile(*args):
    from utaTools.utapy import proxyMthBsn as proxyMB
    reload(proxyMB)

    valuesField = pm.textField ( "txtFieldTyOffsetMB" , q  = True , text = True  )
    fieldBodyBshRig_Geo = pm.textField ( "txtFieldBodyBshRig_Geo2" , q  = True , text = True  )
    fieldObj01BshRig_Geo = pm.textField ( "txtFieldObj01BshRig_Geo" , q  = True , text = True  )
    fieldObj02BshRig_Geo = pm.textField ( "txtFieldObj02BshRig_Geo" , q  = True , text = True  )
    # proxyMB.proxyMthBsn(tyOffset = int(valuesField), geoBsn = fieldBodyBshRig_Geo, geoObj01 = fieldObj01BshRig_Geo , geoObj02 = fieldObj02BshRig_Geo)
    proxyMB.proxyMthBsn(tyOffset = int(valuesField), paths = '', geoBsn = fieldBodyBshRig_Geo)
    proxyMB.proxyMthBsn(tyOffset= int(valuesField), paths = 'Obj01', geoBsn= fieldObj01BshRig_Geo)
    proxyMB.proxyMthBsn(tyOffset= int(valuesField), paths = 'Obj02', geoBsn= fieldObj02BshRig_Geo)    
    pm.button("DuplicateProxyMthBshButton", e=True, bgc= (0.498039, 1, 0))

def dupProxyMthObjBshFile(*args):
    from utaTools.utapy import proxyMthBsn as proxyMB
    reload(proxyMB)

    valuesField = pm.textField ( "txtFieldTyOffsetMB" , q  = True , text = True  )
    valuesPathsField = pm.textField ( "txtFieldPathsRig_Geo" , q  = True , text = True  )
    fieldBodyBshRig_Geo = pm.textField ( "txtFieldBodyBshRig_Geo2" , q  = True , text = True  )
    fieldObj01BshRig_Geo = pm.textField ( "txtFieldObj01BshRig_Geo" , q  = True , text = True  )
    # fieldObj02BshRig_Geo = pm.textField ( "txtFieldObj02BshRig_Geo" , q  = True , text = True  )
    # proxyMB.proxyMthBsn(tyOffset = int(valuesField), geoBsn = fieldBodyBshRig_Geo, geoObj01 = fieldObj01BshRig_Geo , geoObj02 = fieldObj02BshRig_Geo)
    # proxyMB.proxyMthBsn(tyOffset = int(valuesField), paths = '', geoBsn = fieldBodyBshRig_Geo)
    proxyMB.proxyMthBsn(tyOffset= int(valuesField), paths = valuesPathsField, geoObj01Bsn= fieldObj01BshRig_Geo)
    proxyMB.proxyMthObj02Bsn(tyOffset= int(valuesField), paths = '', geoObj02Bsn= fieldObj02BshRig_Geo)    
    pm.button("DuplicateProxyMthBshButton", e=True, bgc= (0.498039, 1, 0))


def dupProxyEbBshFile(*args):
    from utaTools.utapy import proxyEbBsn as proxyEB
    reload(proxyEB)

    bodyGeoBsnField = pm.textField ( "txtFieldBodyGeoBsn" , q  = True , text = True  )
    eyeBrowGeoBsnField = pm.textField ( "txtFieldEyebrowGeoBsn" , q  = True , text = True  )
    values = pm.textField ( "txtFieldTyOffset" , q  = True , text = True  )
    proxyEB.proxyEbBsn(tyOffset = int(values), bodyGeoBsn = bodyGeoBsnField, eyeBrowGeoBsh = eyeBrowGeoBsnField)
    print "Creating >>  -- ProxyEbBsh is Done!! --"   
    pm.button("DuplicateEbFromBshButton", e=True, bgc= (0.498039, 1, 0))

def AddBodyBshEbFile(*args):
    sel = pm.ls(sl=True) ;
    if sel != []:
        sel = sel [0] ;
        pm.textField("txtFieldBodyGeoBsn", e = True  , text = sel)
    else:
        pm.textField("txtFieldBodyGeoBsn", e = True  , text = "")  
        print "Please select Object !!"
    pm.button("AddBodyGeoBshEb", e=True, bgc= (0.498039, 1, 0))

def AddEyebrowBshEbFile(*args):
    sel = pm.ls(sl=True) ;
    if sel != []:
        sel = sel [0] ;
        pm.textField("txtFieldEyebrowGeoBsn", e = True  , text = sel)
    else:
        pm.textField("txtFieldEyebrowGeoBsn", e = True  , text = "")  
        print "Please select Object !!"
    pm.button("AddEyeBrowGeoBshEb", e=True, bgc= (0.498039, 1, 0))

def runFacialUiCtrl(*args):
    from rf_maya.rftool.rig.rigScript import runFacialRig as rfr
    reload(rfr)

    rfr.runFacialUiCtrl()
    # rmr.backupMainRig()
    pm.button("runFacialUiCtrlButton", e=True, bgc= (0.498039, 1, 0))

def runFacialUiRig(*args):
    from rf_maya.rftool.rig.rigScript import runFacialRig as rfr
    reload(rfr)

    rfr.runFacialUiRig()
    # rmr.backupMainRig()
    pm.button("runFacialUiRigButton", e=True, bgc= (0.498039, 1, 0))

def runPrevisRigFile(*args):
    from utaTools.utapy import runMainRig as rmr
    reload(rmr)
    rmr.runPrevisRig()
    # rmr.backupMainRig()
    pm.button("runPrevisRigButton", e=True, bgc= (0.498039, 1, 0))


def runBpmRigFile(*args):
    from utaTools.utapy import runMainRig as rmr
    reload(rmr)

    rmr.runBpmRig()
    rmr.backupBpmRig()
    pm.button("runBpmRigButton", e=True, bgc= (0.498039, 1, 0))

def refBodyRigFile(*args):
    from utaTools.utapy import utaRefElem 
    reload(utaRefElem)

    utaRefElem.refElem(elem = 'bodyRig')
    print 'Reference BodyRig'
    pm.button("refBodyRigButton", e=True, bgc= (0.498039, 1, 0))

def connectBpmFile(*args):
    from utaTools.utapy import utaCore
    reload(utaCore)

    utaCore.connectBpmJnt()
    pm.button("connectBpmButton", e=True, bgc= (0.498039, 1, 0))
    
def duplicateGeoSkinFile(*args):
    from utaTools.utapy import utaCore
    reload(utaCore)
    from utaTools.utapy import bpmTools
    reload(bpmTools)

    print 'Duplicate Geo SkinWeight'
    bpmTools.copy_geo_skin()
    sels = mc.ls('*BaseBpm_Geo')
    print sels, 'sels'
    utaCore.addLambert(lisObj = sels)

    pm.button("duplicateGeoSkinButton", e=True, bgc= (0.498039, 1, 0))


def copyBpmRigFile(*args):
    from utaTools.utapy import copyObjFile 
    reload(copyObjFile)

    projectName = checkProject()
    if projectName in projectData['project']:
        checkMessage = mc.confirmDialog( title='Confirm', 
                        message='Are you sure?', 
                        button=['Yes','No'], 
                        defaultButton='Yes', 
                        cancelButton='No', 
                        dismissString='No' )
        if checkMessage == 'Yes':
            copyObjFile.copyObjFile(src=path + 'template/sevenChickMovie/data/bpmRig.py')
            # copyObjFile.copyObjFile(src=path + 'template/sevenChickMovie/data/ctrlShape.txt')
            print 'bpmRig Scrip copy is Done!!'
        if checkMessage == 'No':
            print 'Thank you'
    # else:
    #     checkMessage = mc.confirmDialog( title='Confirm', 
    #             message='Are you sure?', 
    #             button=['Yes','No'], 
    #             defaultButton='Yes', 
    #             cancelButton='No', 
    #             dismissString='No' )
    #     if checkMessage == 'Yes':
    #         copyObjFile.copyObjFile(src=path + 'template/data/dataChar/mainRig.py')
    #         copyObjFile.copyObjFile(src=path + 'template/data/dataChar/ctrlShape.txt')
    #         print 'Scrip copy is Done!!'
    #     if checkMessage == 'No':
    #         print 'Thank you'            
    # pm.button("dataScripCharButton", e=True, bgc= (0.498039, 1, 0))


def copyTxrFldFile(*args):
    from utaTools.utapy import utaRefElem 
    reload(utaRefElem)

    # projectName = checkProject()
    # if projectName in projectData['project']:
    #     utaRefElem.refElemPath(path = (path + "template/caTemplate/texture"), elem = "main")
    # else:
    #     utaRefElem.refElemPath(path = (path + "template/caTemplate/texture"), elem = "main")
    # pm.button("templateCharJntButton", e=True, bgc= (0.498039, 1, 0))

def templatePrevisJntFile(*args):
    from utaTools.utapy import utaRefElem 
    reload(utaRefElem)
    utaRefElem.refElemPath(path = (path + "template/templatePrevis/skel"), elem = "previsCharTmp")

def templateFacialDirTmpJntFile(*args):
    from utaTools.utapy import utaRefElem 
    reload(utaRefElem)

    utaRefElem.refElemPath(path = (path + "template/facialDirTemplate/skel"), elem = "facialDirTmp")


def templateQuadJntFile(*args):
    from utaTools.utapy import utaRefElem 
    reload(utaRefElem)

    utaRefElem.refElemPath(path = path + "template/skel", elem = "quadrupedTmp")
    pm.button("templateQuadJntButton", e=True, bgc= (0.498039, 1, 0))


def copyScripCharFile(*args):
    from utaTools.utapy import copyObjFile 
    reload(copyObjFile)

    projectName = checkProject()
    if projectName in projectData['project']:
        checkMessage = mc.confirmDialog( title='Confirm', 
                        message='Are you sure?', 
                        button=['Yes','No'], 
                        defaultButton='Yes', 
                        cancelButton='No', 
                        dismissString='No' )
        if checkMessage == 'Yes':
            copyObjFile.copyObjFile(src=path + 'template/sevenChickMovie/data/mainRig.py')
            copyObjFile.copyObjFile(src=path + 'template/sevenChickMovie/data/ctrlShape.txt')
            print 'Scrip copy is Done!!'
        if checkMessage == 'No':
            print 'Thank you'
    else:
        checkMessage = mc.confirmDialog( title='Confirm', 
                message='Are you sure?', 
                button=['Yes','No'], 
                defaultButton='Yes', 
                cancelButton='No', 
                dismissString='No' )
        if checkMessage == 'Yes':
            copyObjFile.copyObjFile(src=path + 'template/data/dataChar/mainRig.py')
            copyObjFile.copyObjFile(src=path + 'template/data/dataChar/ctrlShape.txt')
            print 'Scrip copy is Done!!'
        if checkMessage == 'No':
            print 'Thank you'            
    pm.button("dataScripCharButton", e=True, bgc= (0.498039, 1, 0))
def copyScripQuadFile(*args):
    from utaTools.utapy import copyObjFile 
    reload(copyObjFile)

    projectName = checkProject()
    if projectName in projectData['project']:
        checkMessage = mc.confirmDialog( title='Confirm', 
                        message='Are you sure?', 
                        button=['Yes','No'], 
                        defaultButton='Yes', 
                        cancelButton='No', 
                        dismissString='No' )
        if checkMessage == 'Yes':
            copyObjFile.copyObjFile(src=path + "template/data/dataQuad/mainRig.py")
            copyObjFile.copyObjFile(src=path + "template/data/dataQuad/ctrlShape.txt")
            print 'Scrip copy is Done!!'
        if checkMessage == 'No':
            print 'Thank you'
    else:
        checkMessage = mc.confirmDialog( title='Confirm', 
                message='Are you sure?', 
                button=['Yes','No'], 
                defaultButton='Yes', 
                cancelButton='No', 
                dismissString='No' )
        if checkMessage == 'Yes':
            copyObjFile.copyObjFile(src=path + "template/data/dataQuad/mainRig.py")
            copyObjFile.copyObjFile(src=path + "template/data/dataQuad/ctrlShape.txt")
            print 'Scrip copy is Done!!'
        if checkMessage == 'No':
            print 'Thank you'            
    pm.button("dataScripQuadButton", e=True, bgc= (0.498039, 1, 0))


def copyScripBpmFile(*args):
    from utaTools.utapy import copyObjFile 
    reload(copyObjFile)

    projectName = checkProject()
    if projectName in projectData['project']:
        checkMessage = mc.confirmDialog( title='Confirm', 
                        message='Are you sure?', 
                        button=['Yes','No'], 
                        defaultButton='Yes', 
                        cancelButton='No', 
                        dismissString='No' )
        if checkMessage == 'Yes':
            copyObjFile.copyObjFile(src=path + 'template/sevenChickMovie/data/bpmRig.py')
            # copyObjFile.copyObjFile(src=path + 'template/sevenChickMovie/data/ctrlShape.txt')
            print 'Scrip copy is Done!!'
        if checkMessage == 'No':
            print 'Thank you'
    # else:
    #     checkMessage = mc.confirmDialog( title='Confirm', 
    #             message='Are you sure?', 
    #             button=['Yes','No'], 
    #             defaultButton='Yes', 
    #             cancelButton='No', 
    #             dismissString='No' )
    #     if checkMessage == 'Yes':
    #         copyObjFile.copyObjFile(src=path + 'template/data/dataChar/mainRig.py')
    #         copyObjFile.copyObjFile(src=path + 'template/data/dataChar/ctrlShape.txt')
    #         print 'Scrip copy is Done!!'
    #     if checkMessage == 'No':
    #         print 'Thank you'            
    pm.button("copyScripBpmFileButton", e=True, bgc= (0.498039, 1, 0))



def proxyCrtMthBsnFile(*args):
    from utaTools.utapy import proxyCorMthShape2
    reload(proxyCorMthShape2)

    txtFieldTyOffsets = pm.textField ( "txtFieldTyOffsetMB" , q  = True , text = True  )
    txtFieldJawMthRotateXs = pm.textField ( "txtFieldJawMthRX" , q  = True , text = True  )
    txtFieldPathsMthRig = pm.textField ( "txtFieldPathsRig_Geo" , q  = True , text = True  )
    txtFieldBodyMthRig = pm.textField ( "txtFieldBodyMthRig_Geo" , q  = True , text = True  )
    txtFieldBodyMthLipRig = pm.textField ( "txtFieldBodyMthLipRig_Geo" , q  = True , text = True  )
    txtFieldBodyMthJawRig = pm.textField ( "txtFieldBodyMthJawRig_Geo" , q  = True , text = True  )

    # txtFieldObj01MthRig = pm.textField ( "txtFieldBodyMthObj01Rig_Geo" , q  = True , text = True  )
    # txtFieldObj01MthLipRig = pm.textField ( "txtFieldObj01MthLipRig_Geo" , q  = True , text = True  )
    # txtFieldObj01MthJawRig = pm.textField ( "txtFieldObj01MthJawRig_Geo" , q  = True , text = True  )

    # txtFieldObj02MthRig = pm.textField ( "txtFieldBodyMthObj02Rig_Geo" , q  = True , text = True  )
    # txtFieldObj02MthLipRig = pm.textField ( "txtFieldObj02MthLipRig_Geo" , q  = True , text = True  )
    # txtFieldObj02MthJawRig = pm.textField ( "txtFieldObj02MthJawRig_Geo" , q  = True , text = True  )


    # proxyCorMthShape2.proxyCorCrtMthBsn(tyOffset = int(txtFieldTyOffsets), 
    #                                     jawMthRotateX = int(txtFieldJawMthRotateXs), 
    #                                     bodyMthRig=txtFieldBodyMthRig, 
    #                                     BodyMthLip=txtFieldBodyMthLipRig, 
    #                                     BodyMthJaw=txtFieldBodyMthJawRig,
    #                                     Obj01MthGeo=txtFieldObj01MthLipRig,
    #                                     Obj02MthGeo=txtFieldObj02MthJawRig,)

    proxyCorMthShape2.proxyCorCrtMthBsn(tyOffset = int(txtFieldTyOffsets), 
                                        jawMthRotateX = int(txtFieldJawMthRotateXs), 
                                        bodyMthRig=txtFieldBodyMthRig, 
                                        BodyMthLip=txtFieldBodyMthLipRig, 
                                        BodyMthJaw=txtFieldBodyMthJawRig,
                                        elem = txtFieldPathsMthRig
                                    )
    # proxyCorMthShape2.proxyCorCrtMthBsn(tyOffset = int(txtFieldTyOffsets), 
    #                                     jawMthRotateX = int(txtFieldJawMthRotateXs), 
    #                                     bodyMthRig=txtFieldObj01MthRig, 
    #                                     BodyMthLip=txtFieldObj01MthLipRig, 
    #                                     BodyMthJaw=txtFieldObj01MthJawRig,
    #                                     elem = 'Obj01',
    #                                 )
    # proxyCorMthShape2.proxyCorCrtMthBsn(tyOffset = int(txtFieldTyOffsets), 
    #                                     jawMthRotateX = int(txtFieldJawMthRotateXs), 
    #                                     bodyMthRig=txtFieldObj02MthRig, 
    #                                     BodyMthLip=txtFieldObj02MthLipRig, 
    #                                     BodyMthJaw=txtFieldObj02MthJawRig,
    #                                     elem = 'Obj02',
                                    # )
    pm.button("DuplicateCorrecMthShapeButton", e=True, bgc= (0.498039, 1, 0))
    print  "Create Proxy Corrective Mth Bsn is Success!!"

def con2HeadFile(*args):
    from utaTools.utapy import con2Head 
    reload(con2Head)

    projectName = checkProject()
    if projectName in projectData['project']:
        con2Head.connectGeoToBsh()
    else:
        print "Sorry This Function is not Ready!!"

    pm.button("createBSHd2FclButton", e=True, bgc= (0.498039, 1, 0))

def selJntBindSkin(*args):
    lists = mc.ls('*:Rbn*Dtl_*_*_Jnt','*:SpineSkin_*_Jnt',
        '*:RbnNeckDtl_*_Jnt','*:Head_Jnt',
        '*:Clav_*_Jnt','*:Pelvis_Jnt',
        '*:Ankle_*_Jnt','*:Ball_*_Jnt',
        '*:Index*_*_Jnt','*:Middle*_*_Jnt',
        '*:Pinky*_*_Jnt','*:Wrist_*_Jnt','*:Hand_*_Jnt',
        '*:Thumb_*_*_Jnt','*:Index_*_*_Jnt','*:Middle_*_*_Jnt',
        '*:Ring_*_*_Jnt','*:Pinky_*_*_Jnt')
    try:
        mc.select(lists)
    except: pass

def utaMotionsToolsUi(*args):
    from utaTools.utapy import utaMotionsToolsUi as motionsUi
    reload(motionsUi)

    motionsUi.utaMotionsToolsUi()
    # umtu.utaMotionsToolsUi()

def ImExportAnimUi(*args):
    from utaTools.utapy import ImExportAnimUi as ieau 
    reload(ieau)

    ieau.ImportExportanimUI()

def ImExportAnimUi2(*args):
    from utaTools.utapy import ImExportAnimUi2 as ieau2 
    reload(ieau2)

    ieau2.ImportExportanimUI2()


def localControlMakerUi(*args):
    from lpRig import localControlMaker
    reload(localControlMaker)

    localControlMaker.run()

def deleteGrpFile(*args):
    from utaTools.utapy import DeleteGrp  
    reload(DeleteGrp)

    DeleteGrp.deleteGrp(obj = '')

def proxyObjFile(*args):
    from utaTools.utapy import ProxyObj
    reload(ProxyObj)

    ProxyObj.proxyObj(elem = 'Proxy')

def rivetDetailControlFile(*args):
    from lpRig import detailControl
    reload(detailControl)

    detailControl.attachSelectedControlsToSelectedMesh()

def parentScaleConstraintFile(*args):
    from utaTools.utapy import utaCore
    reload(utaCore)

    utaCore.parentScaleConstraint()

def mainGroupFile (*args):
    from ncmel import mainRig
    reload(mainRig)


    # utaCore.mainGroupName()
    mainRig.Run(assetName = '' ,size = 1)

def mainProxyRigFile (*args):
    from utaTools.utapy import utaCore
    reload(utaCore)

    utaCore.mainProxyRig(GeoVis = True, size = 0.75)
    utaCore.createCvAxis(axisCheck = '')

def rivetUiFile (*args):
    from utaTools.utapy import rivetUi
    reload(rivetUi)

    rivetUi.rivetUi()

#ConnectAttr Translate Rotate Scale
def connectTRSFile(*args):
    from utaTools.utapy import utaCore
    reload(utaCore)

    utaCore.connectTRS(trs = True, translate = False, rotate = False, scale = False)
#ConnectAttr Translate 
def connectTFile(*args):
    from utaTools.utapy import utaCore
    reload(utaCore)

    utaCore.connectTRS(trs = False, translate = True, rotate = False, scale = False)
#ConnectAttr Rotate
def connectRFile(*args):
    from utaTools.utapy import utaCore
    reload(utaCore)

    utaCore.connectTRS(trs = False, translate = False, rotate = True, scale = False)
#ConnectAttr Scale
def connectSFile(*args):
    from utaTools.utapy import utaCore
    reload(utaCore)

    utaCore.connectTRS(trs = False, translate = False, rotate = False, scale = True)
#ConnectDtlNrb
def connectDtlNrbFile(*args):
    from utaTools.utapy import utaCore
    reload(utaCore)

    utaCore.connectObjMirror(translate = True, 
                            rotate = True, 
                            valueA = -1, 
                            valueB = 1)

def attrWriteFile(*args):
    from utaTools.utapy import utaAttrWriteRead
    reload(utaAttrWriteRead)

    utaAttrWriteRead.attrWrite()

def attrReadAllFile(*args):
    from utaTools.utapy import utaAttrWriteRead
    reload(utaAttrWriteRead)

    utaAttrWriteRead.attrRead()

def attrReadSELFile(*args):
    from utaTools.utapy import utaAttrWriteRead
    reload(utaAttrWriteRead)

    utaAttrWriteRead.attrReadSelected()

def tagNameConnectionWriteFile(*args):
    from utaTools.utapy import utaTagNameConnectWriteRead as tagNameConWR 
    reload(tagNameConWR)

    tagNameConWR.tagNameConnectionWrite()

def fixParentConToMatrixCon(*args):
    from nuTools.util import matrixConstraint as mtcon
    reload(mtcon)
    import pymel.core as pm

    # convert constraints into matrix constraint
    old_constraints = mtcon.convertToMatrixConstraint(objs=pm.ls(type='transform'))

    # delete old constraints
    pm.delete(old_constraints) 

def tagNameConnectionReadFile(*args):
    from utaTools.utapy import utaTagNameConnectWriteRead as tagNameConWR 
    reload(tagNameConWR)

    tagNameConWR.tagNameConnectionRead()

def createLocatorAndJntFile(*args):
    from utaTools.utapy import utaCore
    reload(utaCore)
    from lpRig import rigTools as lrr
    reload(lrr)
    
    select = lrr.locatorOnMidPos()
    mc.select(select)
    utaCore.jointFromLocator()
def jointFromLocatorFile(*args):
    from utaTools.utapy import utaCore
    reload(utaCore)

    utaCore.jointFromLocator()

def ribbonUiFile(*args):
    mm.eval('source "O:/Pipeline/core/rf_maya/rftool/rig/utaTools/utamel/ribbonUi.mel"')
    mm.eval('pkRbbnIk()')
def parentJntFile(*args):
    from utaTools.utapy import utaCore
    reload(utaCore)

    utaCore.parentJnt()

def nameCheckerFile(*args):
    from rftool.model import name_checker
    reload(name_checker)

    name_checker.show()

def detailCtrlRigFile(*args):
    from ncmel import detailCtrlRig 
    reload(detailCtrlRig)

    detailCtrlRig.detailCtrlUI()

def duplicateMthJawJntObj(*args):
    from utaTools.utapy import utaRefElem 
    reload(utaRefElem)
    from utaTools.utapy import utaCore
    reload(utaCore)

    utaRefElem.refElem(elem = 'Ctrl')
    ctrlRef = mc.ls('*:Rig_Grp')
    if ctrlRef:
        utaCore.duplicateMthJawJnt()
        mc.select(ctrlRef[0])
        removeSelectRefFile()
        mc.setAttr ("%s.rotateOrder" % "mthJawLwr1_Jnt" , 5)
        mc.setAttr ("%s.rotateOrder" % "mthJawLwr2_Jnt" , 5)
        mc.setAttr ("%s.rotateOrder" % "mthHead_Jnt" , 3)
        
def reBuildCurveFile(*args):
    from utaTools.utapy import utaCore
    reload(utaCore)

    fieldObjNumMthJnt = pm.textField ( "txtFieldNumMthJnt" , q  = True , text = True  )
    utaCore.reBuildCurve(   lists = [], 
                            spans = fieldObjNumMthJnt)

def mthRigLoopTmpLocFile(*args):
    from utaTools.utapy import mouthLoop
    reload(mouthLoop)

    mouthLoop.SnapMotuhLoop()

def copyNonlinearFile(*args):
    from utaTools.utapy import copyNonLinear2
    reload(copyNonLinear2)

    copyNonLinear2.copyNonlinearUI()

def proceduralRibbonFile(*args):
    from utaTools.utapy import UtaProceduralRibbon
    reload(UtaProceduralRibbon)

    UtaProceduralRibbon.UtaProceduralRibbon()

def ribbonDeformerUiFile(*args):
    from utaTools.utapy import ribbonDeformerUi as rdf
    reload(rdf)
    rdf.buildRibbonDeformerUI()

def muscleConnectUiFile(*args):
    from utaTools.utapy import muscleConnect as muscleConnect
    reload(muscleConnect)
    muscleConnect.MuscleConnectUI()

def miarmyUiFile(*args):
    import sys
    from rf_maya.rftool.rig.miarmyTool.utils import OaUI as OaUi
    reload(OaUi)

    x = OaUi.oaCustomUI()
    x.showUI()

def nonRollRigUiFile(*args):
    from utaTools.utapy import NonRollRigUI 
    reload(NonRollRigUI)
    NonRollRigUI.openUI()

def gridControllerUiFile(*args):
    from utaTools.utapy import GridControllerUI 
    reload(GridControllerUI)
    GridControllerUI.openUI()

def fxRigUiFile(*args):
    from utaTools.utapy import fxRigTools2
    reload(fxRigTools2)
    fxRigTools2.Run()

def copySkinWeightFile(*args):
    from utaTools.utapy import TransferWeights1by1
    reload(TransferWeights1by1)
    a = TransferWeights1by1.TransferWeights()
    a.openUI()

def transferAttr_LtoR_RunFile(*args):
    from utaTools.utapy import transferAttr_LtoR as ltor
    reload(ltor)

    ltor.transferAttr_LtoR_Run()

def GenerateJntAxisFile(*args):
    from utaTools.utapy import GenerateJntAxisUi
    reload(GenerateJntAxisUi)

    GenerateJntAxisUi.GenerateJntAxisUi()

def ExFaceProxyUiFile(*args):
    from utaTools.utapy import ExFaceProxyUi
    reload(ExFaceProxyUi)

    ExFaceProxyUi.ExFaceProxyUi()

def StringsRigUiFile(*args): 
    from utaTools.utapy.strings import app as stringsApp
    reload(stringsApp)

    stringsApp.show()

def copyShapeAllUiFile(*args):
    from utaTools.utapy import copyShapeAllUi
    reload(copyShapeAllUi)

    copyShapeAllUi.copyShapeAllUi()

def copyShapeUiFile(*args):
    from utaTools.utapy import buildCopyShapeNewHair 
    reload(buildCopyShapeNewHair)

    buildCopyShapeNewHair.buildCopyShapeNewHairUI()

def projectNameField(item,*args):
    item = pm.optionMenu('projectOption', v = True, q=True)
    if item == 'Hanuman':
        pm.textField( "txt7FieldfacialCtrlAttr", text = "FacialCtrlMover_Hnm", e = True )
    else:
        pm.textField("txt7FieldfacialCtrlAttr", text = "FacialCtrlMover", e = True )   

def addAllSelectionObject(field, butt, *args):
    sel = mc.ls(sl=True)
    letter = ", ".join(sel)
    pm.textField(field, e=1, tx=letter)
    pm.button(butt, e=1, bgc=[0.498039, 1, 0])

def disableFieldByCheckBox(cb, fldLs, *args):
    check = mc.checkBox(cb, q=True, v=True)
    for fld in fldLs:
        if bool(check) == True:
            mc.textField(fld, e=True, en=True)

        elif bool(check) == False:
            mc.textField(fld, e=True, en=False)

def createfacialDirectRig(*args):
    from utaTools.pkmel import ctrlShapeTools
    reload(ctrlShapeTools)
    from utaTools.utapy import MouthDir , EyeDir, lipRig
    reload(MouthDir)
    reload(EyeDir)
    reload(lipRig)

    ## Eye L
    eyeLidLUprDir = pm.textField("eyeUprLTmpJntField", q=True, tx=True).split(", ")
    eyeLidLLwrDir = pm.textField("eyeLwrLTmpJntField", q=True, tx=True).split(", ")
    sideL = pm.textField("eyeLSideField", q=True, tx=True)
    colorL = pm.textField("eyeLColorField", q=True, tx=True)

    ## Eye R
    eyeLidRUprDir = pm.textField("eyeUprRTmpJntField", q=True, tx=True).split(", ")
    eyeLidRLwrDir = pm.textField("eyeLwrRTmpJntField", q=True, tx=True).split(", ")
    sideR = pm.textField("eyeRSideField", q=True, tx=True)
    colorR = pm.textField("eyeRColorField", q=True, tx=True)

    mainLipCen = pm.textField("mainLipCenTmpJntField", q=True, tx=True).split(", ")
    lipCnr = pm.textField("lipCnrTmpJntField", q=True, tx=True).split(", ")
    lipUpCen = pm.textField("lipUpCenTmpJntField", q=True, tx=True).split(", ")
    lipUpL = pm.textField("lipUpLTmpJntField", q=True, tx=True).split(", ")
    lipUpR = pm.textField("lipUpRTmpJntField", q=True, tx=True).split(", ")
    lipDnCen = pm.textField("lipDnCenTmpJntField", q=True, tx=True).split(", ")
    lipDnL = pm.textField("lipDnLTmpJntField", q=True, tx=True).split(", ")
    lipDnR = pm.textField("lipDnRTmpJntField", q=True, tx=True).split(", ")
    tongue = pm.textField("tongueTmpJntField", q=True, tx=True).split(", ")
    teethMainUp = pm.textField("teethMainUpTmpJntField", q=True, tx=True).split(", ")
    teethUp = pm.textField("teethUpTmpJntField", q=True, tx=True).split(", ")
    teethMainDn = pm.textField("teethMainDnTmpJntField", q=True, tx=True).split(", ")
    teethDn = pm.textField("teethDnTmpJntField", q=True, tx=True).split(", ")
    dtlOnFace = pm.textField("dtlOnFaceField", q=True, tx=True).split(", ")
    nose = pm.textField("noseDirField", q=True, tx=True).split(", ")
    jaw = pm.textField("jawTmpJntField", q=True, tx=True).split(", ")
    size = pm.floatField("sizeField", q=True, v=True)

    teethBox = pm.checkBox("teethRigDirectCheckBox", q=True, v=True)
    tongueBox = pm.checkBox("tongueRigDirectCheckBox", q=True, v=True)
    detailBox = pm.checkBox("detailRigDirectCheckBox", q=True, v=True)
    noseBox = pm.checkBox("detailRigDirectCheckBox", q=True, v=True)
    eyeLBox = pm.checkBox("eyeRigLJointDirectCheckBox", q=True, v=True)
    eyeRBox = pm.checkBox("eyeRigRJointDirectCheckBox", q=True, v=True)

    ## Mouth Rig
    MouthDir.Run(   lipUpCenTmpJnt      = lipUpCen,
                    lipDnCenTmpJnt      = lipDnCen,
                    lipCnrTmpJnt        = lipCnr,
                    lipUpLTmpJnt        = lipUpL, 
                    lipUpRTmpJnt        = lipUpR, 
                    lipDnLTmpJnt        = lipDnL, 
                    lipDnRTmpJnt        = lipDnR, 
                    jawTmpJnt           = jaw,
                    tongueTmpJnt        = tongue,
                    teethMainUpTmpJnt   = teethMainUp,
                    teethUpTmpJnt       = teethUp,
                    teethMainDnTmpJnt   = teethMainDn,
                    teethDnTmpJnt       = teethDn,
                    noseTmpJnt          = nose,
                    dtlOnFace           = dtlOnFace,
                    teethRig            = teethBox, 
                    tongueRig           = tongueBox, 
                    detailRig           = detailBox, 
                    noseRig             = noseBox,
                    size = size)
    ## Eye Rig Left
    EyeDir.Run(     eyeMainTrgtTmpJnt   = 'EyeTrgt_TmpJnt' ,
                    eyeTrgtTmpJnt       = '' ,
                    eyeTmpJnt           = '' ,
                    eyeInner            = '' ,
                    eyeOuter            = '' ,
                    eyeLidMainUpr       = '' ,
                    eyeLidMainLwr       = '' ,
                    eyeLidUpr           = eyeLidLUprDir ,
                    eyeLidLwr           = eyeLidLLwrDir ,
                    eyeNrb              = '' ,
                    parent              = 'HeadUpEyeLid_Jnt' ,
                    elem                = '' ,
                    side                = sideL ,
                    clrEyeTrgt          = str(colorL) ,
                    size                = size )

    ## Eye Rig Right
    EyeDir.Run(     eyeMainTrgtTmpJnt   = 'EyeTrgt_TmpJnt' ,
                    eyeTrgtTmpJnt       = '' ,
                    eyeTmpJnt           = '' ,
                    eyeInner            = '' ,
                    eyeOuter            = '' ,
                    eyeLidMainUpr       = '' ,
                    eyeLidMainLwr       = '' ,
                    eyeLidUpr           = eyeLidRUprDir ,
                    eyeLidLwr           = eyeLidRLwrDir ,
                    eyeNrb              = '' ,
                    parent              = 'HeadUpEyeLid_Jnt' ,
                    elem                = '' ,
                    side                = sideR ,
                    clrEyeTrgt          = str(colorR) ,
                    size                = size )


    # ## lipSealRig
    # lipRig.run( geo = 'Body_Geo1', 
    #             curve = [   'upperLipLinear_Crv', 
    #                         'lowerLipLinear_Crv'], 
    #             control = [ 'lipCnrSub_L_Ctrl', 
    #                         'lipCnrSub_R_Ctrl'])
    # print '99'

    ## Button Run
    pm.button("createJointDirectButt", e=1, bgc=[0.498039, 1, 0])
    # Clean Template Joint ---------------------------------------------------------------------------------------------
    corePath = '%s/core' % os.environ.get('RFSCRIPT')
    path = '{}/rf_maya/rftool/rig/template/facialDirTemplate/data'.format(corePath)
    ctrlShapeTools.readAllCtrl( search = '', replace = '', dataFld = path )

    ## Wrap Grp
    mc.parent('EyeAllCtrl_Grp', 'FacialDirRigCtrl_Grp')
    mc.parent('EyeAllStill_Grp', 'FacialDirRigStill_Grp')

# MainUI --------------------------------------------------------------------------------------------------------
# MainUI --------------------------------------------------------------------------------------------------------
# MainUI --------------------------------------------------------------------------------------------------------
# MainUI --------------------------------------------------------------------------------------------------------

def caWoWcaUI ( ) :
    from utaTools.utapy import utaCore
    reload(utaCore)

    # check if window exists
    if pm.window ( 'caWoWcaUI' , exists = True ) :
        pm.deleteUI ( 'caWoWcaUI' ) ;
    title = "ca facial rig and tools v0.1 (08-02-2023)" ;
    # create window 
    window = pm.window ( 'caWoWcaUI', title = title , width = 520,
    mnb = True , mxb = True , sizeable = True , rtf = True ) ;
    window = pm.window ( 'caWoWcaUI', e = True , width = 520)

    ## setWindowIcon
    utaCore.wintes(window, pngImags() ,'icon_222190632.png')

    windowRowColumnLayout = pm.rowColumnLayout( w = 520, p = window , nc = 1 )
    # title logo
    titleLayout = pm.rowColumnLayout ( w = 520 , p = windowRowColumnLayout , nc = 2 , cw = [(1, 424), (2, 96)]) ;
    pm.symbolCheckBox ("titleButtonP1",image = ("%s%s" % (pngImags(),"Title_v03_A.png")) , h = 50, p = titleLayout , onc = openExporerFile , bgc = (0, 0, 0)) ; 
    pm.symbolCheckBox ("titleButtonP2",image = ("%s%s" % (pngImags(),"Title_v03_B.png")) , h = 50, p = titleLayout , onc = openWebbrowserFile, bgc = (0, 0, 0)) ; 
    mainLayout = pm.rowColumnLayout ( w = 520 , p = titleLayout , nc = 2 , columnWidth = [ ( 1 , 260 ) , ( 2 , 260 ) ] ) ;

# mainTab A-------------------------------------------------------------------------------------------------------------------------------    
    mainTab_A = pm.tabLayout ( innerMarginWidth = 5 , innerMarginHeight = 5 , p = mainLayout ) ;

# TmpRig Tab -------------------------
# TmpRig Tab -------------------------
    HdRigTab = pm.rowColumnLayout ( "Run", nc = 1 , p = mainTab_A ) ;
    mainHdRigTab = pm.rowColumnLayout ( nc = 1 , p = HdRigTab , w = 300) ;

    # Free Space
    mc.tabLayout(p=mainHdRigTab)

#     pm.rowColumnLayout (p = mainHdRigTab ,h=8) ;
#     ken = pm.text( label='# Run Scrip Rig For Previs' , p = mainHdRigTab, al = "left")
#     # free space
#     pm.rowColumnLayout (p = mainHdRigTab ,h=8) ;

#     templateLayout = pm.rowColumnLayout ( nc = 5 , p = mainHdRigTab , w = 248, cw = [(1, 10), (2,110),(3,8),(4,110), (5,10)]) ;
#     pm.rowColumnLayout (p = templateLayout ) ;
#     pm.button ("previsTmpJntButton",label = "Previs Tmp Jnt" , h=50, p = templateLayout , c = templatePrevisJntFile ,bgc = (0.411765, 0.411765, 0.411765)) ; 
#     pm.rowColumnLayout (p = templateLayout ) ;
#     pm.button ("runPrevisRigButton",label = "Run PrevisRig" , h=50, p = templateLayout , c = runPrevisRigFile ,bgc = (0.411765, 0.411765, 0.411765)) ; 
#     pm.rowColumnLayout (p = templateLayout ) ;
#     # Free Space
#     mc.tabLayout(p=mainHdRigTab)

#     pm.rowColumnLayout (p = mainHdRigTab ,h=8) ;
#     ken = pm.text( label='# Run Scrip Rig For Animation' , p = mainHdRigTab, al = "left")
#     # free space
#     pm.rowColumnLayout (p = mainHdRigTab ,h=8) ;

#     templateLayout = pm.rowColumnLayout ( nc = 5 , p = mainHdRigTab , w = 248, cw = [(1, 10), (2,110),(3,8),(4,110), (5,10)]) ;
#     pm.rowColumnLayout (p = templateLayout ) ;
#     pm.button ("templateCharJntButton",label = "Copy Texture Fld" , h=20, p = templateLayout , c = copyTxrFldFile ,bgc = (0.411765, 0.411765, 0.411765)) ; 
#     pm.rowColumnLayout (p = templateLayout ) ;
#     pm.button ("dataScripCharButton",label = ">>  Copy Scrip" , h=20, p = templateLayout , c = copyScripCharFile ,bgc = (0.411765, 0.411765, 0.411765)) ; 
#     pm.rowColumnLayout (p = templateLayout ) ;
#     templateLayout = pm.rowColumnLayout ( nc = 5 , p = mainHdRigTab , w = 248, cw = [(1, 10), (2,110),(3,8),(4,110), (5,10)]) ;
#     pm.rowColumnLayout (p = templateLayout ) ;
#     pm.button ("templateQuadJntButton",label = "Quad Tmp Jnt" , h=20, p = templateLayout , c = templateQuadJntFile ,bgc = (0.411765, 0.411765, 0.411765)) ; 
#     pm.rowColumnLayout (p = templateLayout ) ;
#     pm.button ("dataScripQuadButton",label = ">>  Copy Scrip" , h=20, p = templateLayout , c = copyScripQuadFile ,bgc = (0.411765, 0.411765, 0.411765)) ; 
#     pm.rowColumnLayout (p = templateLayout ) ;



#     pm.rowColumnLayout (p = mainHdRigTab ,h=8) ;
#     mc.separator(st="in", h=10, p = mainHdRigTab)
#     ken = pm.text( label='# Copy Module ( mainRig.py )' , p = mainHdRigTab, al = "left")
#     # free space
#     pm.rowColumnLayout (p = mainHdRigTab ,h=8) ;

#     templateLayout = pm.rowColumnLayout ( nc = 5 , p = mainHdRigTab , w = 248, cw = [(1, 10), (2,110),(3,8),(4,110), (5,10)]) ;
#     pm.rowColumnLayout (p = templateLayout ) ;
#     pm.text(l="Sorce: Project / Char", p = templateLayout)
#     pm.rowColumnLayout (p = templateLayout ) ;
#     pm.text(l="Desti: Project / Char", p = templateLayout)
#     pm.rowColumnLayout (p = templateLayout ) ;

#     templateALayout = pm.rowColumnLayout ( nc = 5 , p = mainHdRigTab , w = 248, cw = [(1, 10), (2,110),(3,8),(4,110), (5,10)]) ;
#     pm.rowColumnLayout (p = templateALayout ) ;
#     projectFromSorce = mc.optionMenu("projectFromSorceMenu", h=20, p=templateALayout)
#     for obj in get_project_from_sg():
#         pm.menuItem(l=obj, p=projectFromSorce)
#     pm.rowColumnLayout (p = templateALayout ) ;
#     projectFromDesti = mc.optionMenu("projectToDestiMenu", h=20, p=templateALayout)
#     for obj in get_project_from_sg():
#         pm.menuItem(l=obj, p=projectFromDesti)
#     pm.rowColumnLayout (p = templateALayout ) ;

#     templateBLayout = pm.rowColumnLayout ( nc = 5 , p = mainHdRigTab , w = 248, cw = [(1, 10), (2,110),(3,8),(4,110), (5,10)]) ;
#     pm.rowColumnLayout (p = templateBLayout ) ;
#     charFromLayout = pm.columnLayout (p = templateBLayout ) ;
#     charFromSorce = mc.optionMenu("charFromSorceMenu", h=20, w=110, p=charFromLayout)
#     mc.optionMenu(projectFromSorce, e=True, cc=partial(get_char_from_proj, projectFromSorce, "charFromSorceMenu", charFromLayout))
#     pm.rowColumnLayout (p = templateBLayout ) ;
#     charToLayout = pm.columnLayout (p = templateBLayout ) ;
#     charToDesti = mc.optionMenu("charToDestiMenu", h=20, w=110, p=charToLayout)
#     mc.optionMenu(projectFromDesti, e=True, cc=partial(get_char_from_proj, projectFromDesti, "charToDestiMenu", charToLayout))
#     pm.rowColumnLayout (p = templateBLayout ) ;

    mc.separator(st="none", h=10, p=mainHdRigTab)
    copyMainRigLayout = pm.rowColumnLayout ( nc = 3 , p = mainHdRigTab , w = 220, cw=[(1,10),(2,208),(3,10)]) ;
    pm.rowColumnLayout (p = copyMainRigLayout ) ;
    pm.button ("runFacialUiCtrlButton",label = "Run Facial UiCtrl" , h = 50, w = 228, p = copyMainRigLayout , c = runFacialUiCtrl ,bgc = (0.411765, 0.411765, 0.411765)) ; 
    # pm.rowColumnLayout (p = copyMainRigLayout ) ;
    mc.separator(st="in", h=10, p=mainHdRigTab)

   
   

    runMainRigLayout = pm.rowColumnLayout ( nc = 3 , p = mainHdRigTab , w = 220, cw=[(1,10),(2,208),(3,10)]) ;
    pm.rowColumnLayout (p = runMainRigLayout ) ;
    pm.button ("runFacialUiRigButton",label = "Run Facial UiRig" , h=50, w=228, p = runMainRigLayout , c = runFacialUiRig ,bgc = (0.411765, 0.411765, 0.411765)) ; 
    pm.rowColumnLayout (p = runMainRigLayout ) ;


    # free space

    rowLine_Facial_Rig = pm.rowColumnLayout ( nc = 1 , p = mainHdRigTab ) ;
    # Free Space
    # mc.tabLayout(p=rowLine_Facial_Rig, w= 248)
    # free space
    # pm.rowColumnLayout (p = rowLine_Facial_Rig ,h=8) ;
    # pm.text( label='# Facial Rig' , p = rowLine_Facial_Rig, al = "left")
    # free space
    # pm.rowColumnLayout (p = rowLine_Facial_Rig ,h=8) ;
    # R--------------------------------------------
    mainHdRig = pm.rowColumnLayout ( nc = 1 , p = mainHdRigTab) ;
    # Colb_R
    Colb_R = pm.rowColumnLayout ( nc = 4 , p = mainHdRig , cw = [ ( 1 , 61.95 ) , ( 2 , 61.95 ), (3, 61.95) , (4, 61.95)] ) ;
    
    # # rig
    # pm.symbolCheckBox ("HdRigButton",image = ("%s%s" % (pngImags(),"hd.PNG")) , h = 50, p = Colb_R , onc = hdRigCtrl ,bgc = (0.411765, 0.411765, 0.411765)) ; 
    # pm.symbolCheckBox ("EyeRigButton",image = ("%s%s" % (pngImags(),"eye.PNG")) , h = 50, p = Colb_R , onc = eyeRigCtrl ,bgc = (0.411765, 0.411765, 0.411765)) ; 
    # pm.symbolCheckBox ("EarRigButton",image = ("%s%s" % (pngImags(),"ear.PNG")) , h = 50, p = Colb_R , onc = earRigCtrl ,bgc = (0.411765, 0.411765, 0.411765)) ; 
    # pm.symbolCheckBox ("NsRigButton",image = ("%s%s" % (pngImags(),"ns.PNG")) , h = 50, p = Colb_R , onc = nsRigCtrl ,bgc = (0.411765, 0.411765, 0.411765)) ; 

    # # textEyeRig = pm.textField( "txtFieldEyeRig", text = "hdRig", parent = Colb_R , editable = True )
    # pm.button ("HdRigTmpLocButton",label = "Hd Tmp" , h=20, p = Colb_R , c = hdRigTmpLocFile ,bgc = (0.941176, 0.972549, 1)) ; 
    # pm.button ("EyeRigTmpLocButton",label = "Eye Tmp" , h=20, p = Colb_R , c = eyeRigTmpLocFile ,bgc = (0.941176, 0.972549, 1)) ; 
    # pm.button ("EarRigTmpLocButton",label = "Ear Tmp" , h=20, p = Colb_R , c = earRigTmpLocFile ,bgc = (0.941176, 0.972549, 1)) ; 
    # pm.button ("NsRigTmpLocButton",label = "Ns Tmp" , h=20, p = Colb_R , c = nsRigTmpLocFile ,bgc = (0.941176, 0.972549, 1)) ; 

    # # remove 
    # pm.button ("RemoveHdRigTmpLocButton",label = "Remove" , h=20, p = Colb_R , c = removeHdRig ,bgc = (0.411765, 0.411765, 0.411765)) ; 
    # pm.button ("RemoveEyeRigTmpLocButton",label = "Remove" , h=20, p = Colb_R , c = removeEyeRig ,bgc = (0.411765, 0.411765, 0.411765)) ; 
    # pm.button ("RemoveEarRigTmpLocButton",label = "Remove" , h=20, p = Colb_R , c = removeEarRig ,bgc = (0.411765, 0.411765, 0.411765)) ; 
    # pm.button ("RemoveNsRigTmpLocButton",label = "Remove" , h=20, p = Colb_R , c = removeNsRig ,bgc = (0.411765, 0.411765, 0.411765)) ; 

    # #rig
    # pm.symbolCheckBox ("TtRigButton",image = ("%s%s" % (pngImags(),"tt.PNG")) , h = 50, p = Colb_R , onc = ttRigCtrl ,bgc = (0.411765, 0.411765, 0.411765)) ; 
    # pm.symbolCheckBox ("EbRigButton",image = ("%s%s" % (pngImags(),"eb.PNG")) , h = 50, p = Colb_R , onc = ebRigCtrl ,bgc = (0.411765, 0.411765, 0.411765)) ; 
    # pm.symbolCheckBox ("MthRigButton",image = ("%s%s" % (pngImags(),"mth.PNG")) , h = 50, p = Colb_R , onc = mthRigCtrl ,bgc = (0.411765, 0.411765, 0.411765)) ; 
    # pm.symbolCheckBox ("DtlRigButton",image = ("%s%s" % (pngImags(),"dtl.PNG")) , h = 50, p = Colb_R , onc = dtlRigCtrl ,bgc = (0.411765, 0.411765, 0.411765)) ; 

    # pm.button ("TtRigTmpLocButton",label = "Tt Tmp" , h=20, p = Colb_R , c = ttRigTmpLocFile ,bgc = (0.941176, 0.972549, 1)) ; 
    # pm.button ("EbRigTmpLocButton",label = "Eb Tmp" , h=20, p = Colb_R , c = ebRigTmpLocFile ,bgc = (0.941176, 0.972549, 1)) ; 
    # pm.button ("MthRigTmpLocButton",label = "Mth Tmp" , h=20, p = Colb_R , c = mthRigTmpLocFile ,bgc = (0.941176, 0.972549, 1)) ; 
    # pm.button ("DtlRigTmpLocButton",label = "Dtl Tmp" , h=20, p = Colb_R , c = dtlRigTmpLocFile ,bgc = (0.941176, 0.972549, 1)) ; 

    # # remove 
    # pm.button ("RemoveTtRigTmpLocButton",label = "Remove" , h=20, p = Colb_R , c = removeTtRig ,bgc = (0.411765, 0.411765, 0.411765)) ; 
    # pm.button ("RemoveEbRigTmpLocButton",label = "Remove" , h=20, p = Colb_R , c = removeEbRig ,bgc = (0.411765, 0.411765, 0.411765)) ; 
    # pm.button ("RemoveMthRigTmpLocButton",label = "Remove" , h=20, p = Colb_R ,c = removeMthRig ,bgc = (0.411765, 0.411765, 0.411765)) ; 
    # pm.button ("RemoveDtlRigTmpLocButton",label = "Remove" , h=20, p = Colb_R ,c = removeDtlRig, bgc = (0.411765, 0.411765, 0.411765)) ; 

    # # import hd geo
    # pm.rowColumnLayout (p = mainHdRig ,h=8) ;
    # pm.button ("ImportHdGeo",label = "Import HdGeo" , h = 25, w = 244, p = mainHdRig , c = importHdGeoFile ,bgc = (0.411765, 0.411765, 0.411765)) ; 

    # free space
    pm.rowColumnLayout (p = mainHdRig ,h=8) ;
    mc.tabLayout(p=mainHdRig, w= 248)

# Bsh Tab -------------------------
# Bsh Tab -------------------------
    # HdGeoTab = pm.rowColumnLayout ( "Bsh", nc = 1 , p = mainTab_A ) ;
    # mainHdGeoTab = pm.rowColumnLayout ( nc = 1 , p = HdGeoTab , w = 300) ;

    # # line TabA
    # rowLine_TabA = pm.rowColumnLayout ( nc = 1 , p = mainHdGeoTab ) ;
    # mc.tabLayout(p=rowLine_TabA, w= 248)
    # # free space
    # pm.rowColumnLayout (p = rowLine_TabA ,h=8) ;
    # # pm.button (label = "-"*200 ,h = 10, w = 248, p = rowLine_TabA , bgc = (0.333333, 0.333333, 0.333333)) ; 
    # pm.text( label='# Duplicate And Connect Bsh' , p = rowLine_TabA, al = "left")
    # pm.rowColumnLayout (p = rowLine_TabA ,h=8) ;


    # connectBshRowColum = pm.rowColumnLayout ( nc = 3 , p = mainHdGeoTab , w = 248, cw=[(1,10),(2,228),(3,10)]) ;
    # pm.rowColumnLayout (p = connectBshRowColum ) ;
    # pm.button ("DuplicateBshButton2", label = "Duplicate Bsh", h = 40, w = 229, p = connectBshRowColum , c = dupBlendShapeFile, bgc = (0.411765, 0.411765, 0.411765)) ;
    # pm.rowColumnLayout (p = connectBshRowColum ) ;

    # # duplicate "BodyFclRigPos_Geo"
    # rowLine_BodyFclRigPos = pm.rowColumnLayout ( nc = 6 , p = mainHdGeoTab , h = 80 , cw = [(1, 10), (2, 118), (3,55),(4, 2 ), (5, 54 ), (6, 8 )] ) ;
    # pm.rowColumnLayout (p = rowLine_BodyFclRigPos ) ;
    # pm.textField( "txtFieldfacialCtrlAttr", text = "facialCtrlAttr2", parent = rowLine_BodyFclRigPos , editable = True )
    # pm.button ('addFacialCtrlAttr2_0', label = "<< Add" ,h=20, p = rowLine_BodyFclRigPos , c = addRefElemPathFile, bgc = (0.411765, 0.411765, 0.411765)) ;
    # pm.rowColumnLayout (p = rowLine_BodyFclRigPos ) ;
    # pm.button ('addFacialCtrlAttr2_1', label = "REF Ctrl" ,h=20, p = rowLine_BodyFclRigPos , c = refElemPathFile, bgc = (0.411765, 0.411765, 0.411765)) ;
    # pm.rowColumnLayout (p = rowLine_BodyFclRigPos ) ;

    # # pm.rowColumnLayout (p = rowLine_BodyFclRigPos ) ;
    # connectBshRowColum = pm.rowColumnLayout ( nc = 3 , p = rowLine_BodyFclRigPos , w = 248, cw=[(1,10),(2,228),(3,10)]) ;
    # pm.rowColumnLayout (p = connectBshRowColum ) ;
    # pm.button ('connectBshButton',label = "Connect BshCtrl", h =40, p = connectBshRowColum , c = connectBshCtrlFile, bgc = (0.411765, 0.411765, 0.411765)) ;
    # pm.rowColumnLayout (p = connectBshRowColum ) ;


# line TabB
    # # rowLine_TabB = pm.rowColumnLayout ( nc = 1 , p = mainHdGeoTab) ;
    # # pm.rowColumnLayout (p = rowLine_TabB,h=8 ) ;
    # mc.tabLayout(p=rowLine_TabB, w= 248)

    # FacialCtrlAttrRowLine = pm.rowColumnLayout ( nc = 1 , p = mainHdGeoTab ) ;
    # pm.rowColumnLayout (p = FacialCtrlAttrRowLine ,h=8) ;
    # pm.text( label='# Duplicate And Connect Bsh ( 7ChickMovie )' , p = FacialCtrlAttrRowLine, al = "left")
    # pm.rowColumnLayout (p = FacialCtrlAttrRowLine,h=8 ) ;

    # rowLine_OptionBshPrjct = pm.rowColumnLayout ( nc = 3 , p = FacialCtrlAttrRowLine  , cw = [(1, 11), (2, 229), (3, 8 )] ) ;
    # pm.text('')
    # projectOption = pm.optionMenu('projectOption',label = 'Project', changeCommand = projectNameField)
    # pm.menuItem( label='select...' , p = projectOption )
    # pm.menuItem( label='7Chicks' , p = projectOption )
    # pm.menuItem( label='ThreeEyes' , p = projectOption )
    # pm.menuItem( label='Hanuman' , p = projectOption )

    # rowLine_BodyFclRigPos = pm.rowColumnLayout ( nc = 7 , p = FacialCtrlAttrRowLine  , cw = [(1, 11), (2, 115), (3,2), (4, 55 ), (5,2), (6, 54 ), (7, 8 )] ) ;
    # pm.rowColumnLayout (p = rowLine_BodyFclRigPos ) ;
    # # pm.textField( "txtFieldfacialCtrlAttr", text = "facialCtrlAttr2", parent = rowLine_BodyFclRigPos , editable = True )
    # pm.textField( "txt7FieldfacialCtrlAttr", text = "FacialCtrlMover", parent = rowLine_BodyFclRigPos , editable = True )

    # pm.rowColumnLayout (p = rowLine_BodyFclRigPos ) ;
    # pm.button ('add7FacialCtrlAttr2_0', label = "<< Add" ,h=20, p = rowLine_BodyFclRigPos , c = add7RefElemPathFile, bgc = (0.411765, 0.411765, 0.411765)) ;
    # pm.rowColumnLayout (p = rowLine_BodyFclRigPos ) ;
    # pm.button ('add7FacialCtrlAttr3_0', label = "REF Ctrl" ,h=20, p = rowLine_BodyFclRigPos , c = refElemPathFile, bgc = (0.411765, 0.411765, 0.411765)) ;
    # pm.rowColumnLayout (p = rowLine_BodyFclRigPos ) ;


    # checkBoxDupBlendShapeFile = pm.rowColumnLayout ( nc = 6 , p = FacialCtrlAttrRowLine , w = 248, cw=[(1,10),(2,70),(3,10),(4,70),(5,10),(6,70)]) ;
    # pm.rowColumnLayout (p = checkBoxDupBlendShapeFile ) ;
    # pm.checkBox('eyebrowCheckBox', label='eyebrow' , v = True, p = checkBoxDupBlendShapeFile)
    # pm.rowColumnLayout (p = checkBoxDupBlendShapeFile ) ;
    # pm.checkBox('eyeCheckBox', label='eye', v = True, p = checkBoxDupBlendShapeFile)
    # pm.rowColumnLayout (p = checkBoxDupBlendShapeFile ) ;
    # pm.checkBox('mouthCheckBox', label='mouth', v = True, p = checkBoxDupBlendShapeFile)


    # checkBoxDupBlendShape2File = pm.rowColumnLayout ( nc = 6 , p = FacialCtrlAttrRowLine , w = 248, cw=[(1,10),(2,70),(3,10),(4,70),(5,10),(6,70)]) ;
    # pm.rowColumnLayout (p = checkBoxDupBlendShape2File ) ;
    # pm.checkBox('noseCheckBox', label='nose', v = True, p = checkBoxDupBlendShape2File)
    # pm.rowColumnLayout (p = checkBoxDupBlendShape2File ) ;
    # pm.checkBox('headCheckBox', label='head', v = True, p = checkBoxDupBlendShape2File)
    # pm.rowColumnLayout (p = checkBoxDupBlendShape2File ) ;
    # pm.checkBox('threeEyeCheckBox', label='threeEye', v = False, p = checkBoxDupBlendShape2File)


    # connectBshRowColum = pm.rowColumnLayout ( nc = 3 , p = FacialCtrlAttrRowLine , w = 248, cw=[(1,10),(2,228),(3,10)]) ;
    # pm.rowColumnLayout (p = connectBshRowColum ) ;
    # pm.button ("DuplicateBsh7Button2", label = "Duplicate Bsh ( 7 Chick)", h = 40, w = 229, p = connectBshRowColum , c = dupBlendShapeFile, bgc = (0.411765, 0.411765, 0.411765)) ;
    # pm.rowColumnLayout (p = connectBshRowColum ) ;



    # conAndDisconBshRowColum = pm.rowColumnLayout ( nc = 5 , p = FacialCtrlAttrRowLine , w = 248, cw=[(1,10),(2,112),(3,4),(4,112),(5,10)]) ;
    # pm.rowColumnLayout (p = conAndDisconBshRowColum ) ;
    # pm.button ('connect7BshButton',label = "Connect Bsh" ,h=20, p = conAndDisconBshRowColum , c = connect7BshCtrlFile, bgc = (0.411765, 0.411765, 0.411765)) ;
    # pm.rowColumnLayout (p = conAndDisconBshRowColum ) ;
    # pm.button ('disconnect7BshButton',label = "Disconnect Bsh" ,h=30, p = conAndDisconBshRowColum , c = disconnect7BshCtrlFile, bgc = (0.411765, 0.411765, 0.411765)) ;
    # pm.rowColumnLayout (p = conAndDisconBshRowColum ) ;

#     # reColor Re-Status --------------------------------------
#     reColorColumn= pm.rowColumnLayout ( nc = 1 , p = HdGeoTab ) ;
#     # pm.rowColumnLayout (p = reColorColumn,h=8 ) ;
#     pm.text( label='# Duplicate Bsh Separate Path UI' , p = reColorColumn, al = "left")
#     pm.rowColumnLayout (p = reColorColumn,h=8 ) ;
#     ResetLayout = pm.rowColumnLayout ( nc = 3 , p = reColorColumn , w = 220, cw=[(1,10),(2,228),(3,10)]) ;
#     pm.rowColumnLayout (p = ResetLayout ) ;
#     pm.button ('buttonDuplicateBshUI',label = "Duplicate Bsh 7 UI" ,h=40, p = ResetLayout , c = duplicateBsh7UiFile, bgc = (0.411765, 0.411765, 0.411765)) ;
#     pm.rowColumnLayout (p = ResetLayout ) ;
#     # line TabB
#     rowLine_TabB = pm.rowColumnLayout ( nc = 1 , p = reColorColumn) ;
#     pm.rowColumnLayout (p = rowLine_TabB,h=4 ) ;
#     mc.tabLayout(p=rowLine_TabB, w= 248)


# # Build CopyShape NewHair ( 7ChickMovie )
#     FacialCtrlAttrRowLine = pm.rowColumnLayout ( nc = 1 , p = rowLine_TabB ) ;
#     pm.rowColumnLayout (p = FacialCtrlAttrRowLine ,h=8) ;
#     pm.text( label='# Build CopyShape NewHair ( 7ChickMovie )' , p = FacialCtrlAttrRowLine, al = "left")
#     pm.rowColumnLayout (p = FacialCtrlAttrRowLine,h=8 ) ;

#     rowLine_BodyFclRigPos = pm.rowColumnLayout ( nc = 5 , p = FacialCtrlAttrRowLine  , cw = [(1, 11), (2, 170), (3,2), (4, 55 ), (5,2)] ) ;
#     pm.rowColumnLayout (p = rowLine_BodyFclRigPos ) ;
#     pm.textField( "txt7FieldGroupNewHairBsh", text = "Bsh_Grp", parent = rowLine_BodyFclRigPos , editable = True )
#     pm.rowColumnLayout (p = rowLine_BodyFclRigPos ) ;
#     pm.button ('add7GroupNewHairButtonBsh', label = "<< Add" ,h=20, p = rowLine_BodyFclRigPos , c = add7GroupNewHairBshFile, bgc = (0.411765, 0.411765, 0.411765)) ;
#     pm.rowColumnLayout (p = rowLine_BodyFclRigPos ) ;


#     rowLine_BodyFclRigPos = pm.rowColumnLayout ( nc = 5 , p = FacialCtrlAttrRowLine  , cw = [(1, 11), (2, 170), (3,2), (4, 55 ), (5,2)] ) ;
#     pm.rowColumnLayout (p = rowLine_BodyFclRigPos ) ;
#     pm.textField( "txt7FieldGroupNewHair", text = "New Group", parent = rowLine_BodyFclRigPos , editable = True )
#     pm.rowColumnLayout (p = rowLine_BodyFclRigPos ) ;
#     pm.button ('add7GroupNewHairButton', label = "<< Add" ,h=20, p = rowLine_BodyFclRigPos , c = add7GroupNewHairFile, bgc = (0.411765, 0.411765, 0.411765)) ;
#     pm.rowColumnLayout (p = rowLine_BodyFclRigPos ) ;


#     # pm.rowColumnLayout (p = rowLine_BodyFclRigPos ) ;
#     connectBshRowColum = pm.rowColumnLayout ( nc = 3 , p = FacialCtrlAttrRowLine , w = 248, cw=[(1,10),(2,228),(3,10)]) ;
#     pm.rowColumnLayout (p = connectBshRowColum ) ;
#     pm.button ('copyShapeBshButton',label = "CopyShape BlendShape" ,h=40, p = connectBshRowColum , c = copyShapeBshButtonFile, bgc = (0.411765, 0.411765, 0.411765)) ;
#     pm.rowColumnLayout (p = connectBshRowColum ) ;


#     # line TabB
#     rowLine_TabB = pm.rowColumnLayout ( nc = 1 , p = mainHdGeoTab) ;
#     # pm.rowColumnLayout (p = rowLine_TabB,h=8 ) ;
#     mc.tabLayout(p=rowLine_TabB, w= 248)

#     # reColor Re-Status --------------------------------------
#     reColorColumn= pm.rowColumnLayout ( nc = 1 , p = HdGeoTab ) ;
#     # pm.rowColumnLayout (p = reColorColumn,h=8 ) ;
#     pm.text( label='# Re-Check Status' , p = reColorColumn, al = "left")
#     pm.rowColumnLayout (p = reColorColumn,h=8 ) ;
#     ResetLayout = pm.rowColumnLayout ( nc = 3 , p = reColorColumn , w = 220, cw=[(1,10),(2,228),(3,10)]) ;
#     pm.rowColumnLayout (p = ResetLayout ) ;
#     pm.symbolCheckBox (image = ("%s%s" % (pngImags(),"Reset.png")), p = ResetLayout , h=25, onc = reStatusHdGeo , bgc = (0.411765, 0.411765, 0.411765)) ;
#     pm.rowColumnLayout (p = ResetLayout ) ;
#     # line TabB
#     rowLine_TabB = pm.rowColumnLayout ( nc = 1 , p = reColorColumn) ;
#     pm.rowColumnLayout (p = rowLine_TabB,h=4 ) ;
#     mc.tabLayout(p=rowLine_TabB, w= 248)




# #--------------------------------
# # Bpm Tab -------------------------
# # Bpm Tab -------------------------
#     bpmRigTab = pm.rowColumnLayout ( "Bpm Rig", nc = 1 , p = mainTab_A ) ;
#     mainBpmRigTab = pm.rowColumnLayout ( nc = 1 , p = bpmRigTab , w = 300) ;

#     # line TabA
#     mc.tabLayout(p=rowLine_TabA, w= 248)

#     ## Bpm Rig --------------------------------------------------------------------------------------------------------------------------
#     ## Bpm Rig --------------------------------------------------------------------------------------------------------------------------
#     # line TabA
#     # free space
#     helpLaptop = pm.rowColumnLayout ( nc = 1 , p = mainHdRig) ;
#     pm.rowColumnLayout (p = helpLaptop ,h=8) ;
#     rowLine_TabA = pm.rowColumnLayout ( nc = 1 , p = mainBpmRigTab , w = 244 ) ;


#     mc.tabLayout(p=mainBpmRigTab)
#     pm.rowColumnLayout (p = mainBpmRigTab ,h=8) ;
#     pm.text( label='# Bpm Rig' , p = mainBpmRigTab, al = "left")
#     pm.rowColumnLayout (p = mainBpmRigTab ,h=8) ;

#     TemJntSkinLayout = pm.rowColumnLayout ( nc = 3 , p =mainBpmRigTab , w = 220, cw=[(1,10),(2,208),(3,10)]) ;
#     pm.rowColumnLayout (p = TemJntSkinLayout ) ;
#     pm.button ("refBodyRigButton",label = "1. Ref BodyRig" , h = 25, w = 228, p = TemJntSkinLayout , c = refBodyRigFile ,bgc = (0.411765, 0.411765, 0.411765)) ; 
#     pm.rowColumnLayout (p = TemJntSkinLayout ) ;
#     TemJntSkinLayout = pm.rowColumnLayout ( nc = 3 , p =mainBpmRigTab , w = 220, cw=[(1,10),(2,208),(3,10)]) ;
#     pm.rowColumnLayout (p = TemJntSkinLayout ) ;
#     pm.button ("duplicateGeoSkinButton",label = "2. Duplicate GeoSkinWeight ( sel )" , h = 25, w = 228, p = TemJntSkinLayout , c = duplicateGeoSkinFile ,bgc = (0.411765, 0.411765, 0.411765)) ; 
#     pm.rowColumnLayout (p = TemJntSkinLayout ) ;
#     TemJntSkinLayout = pm.rowColumnLayout ( nc = 3 , p =mainBpmRigTab , w = 220, cw=[(1,10),(2,208),(3,10)]) ;
#     pm.rowColumnLayout (p = TemJntSkinLayout ) ;
#     pm.button ("copyScripBpmFileButton",label = "3. Copy 'bpmRig.py' Scrip" , h = 25, w = 228, p = TemJntSkinLayout , c = copyScripBpmFile ,bgc = (0.411765, 0.411765, 0.411765)) ; 
#     pm.rowColumnLayout (p = TemJntSkinLayout ) ;


#     pm.rowColumnLayout (p = mainBpmRigTab ,h=8) ;
#     runBpmRigLayout = pm.rowColumnLayout ( nc = 3 , p =mainBpmRigTab , w = 220, cw=[(1,10),(2,208),(3,10)]) ;
#     pm.rowColumnLayout (p = runBpmRigLayout ) ;
#     pm.button ("runBpmRigButton",label = "Run BpmRig" , h = 50, w = 228, p = runBpmRigLayout , c = runBpmRigFile ,bgc = (0.411765, 0.411765, 0.411765)) ; 
#     pm.rowColumnLayout (p = runBpmRigLayout ) ;

#     pm.rowColumnLayout (p = mainBpmRigTab ,h=8) ;
#     TemJntSkinLayout = pm.rowColumnLayout ( nc = 3 , p =mainBpmRigTab , w = 220, cw=[(1,10),(2,208),(3,10)]) ;
#     pm.rowColumnLayout (p = TemJntSkinLayout ) ;
#     pm.button ("BshBpmButton",label = "Bsh BaseGeo > BpmGeo" , w = 228, p = TemJntSkinLayout , c = doAddBSH ,bgc = (0.411765, 0.411765, 0.411765)) ; 
#     pm.rowColumnLayout (p = TemJntSkinLayout ) ;

#     pm.rowColumnLayout (p = mainBpmRigTab ) ;
#     TemJntSkinLayout = pm.rowColumnLayout ( nc = 3 , p =mainBpmRigTab , w = 220, cw=[(1,10),(2,208),(3,10)]) ;
#     pm.rowColumnLayout (p = TemJntSkinLayout ) ;
#     pm.button ("connectBpmButton",label = "Connect Bpm Jnt : main ( sel )" , w = 228, p = TemJntSkinLayout , c = connectBpmFile ,bgc = (0.411765, 0.411765, 0.411765)) ; 
#     pm.rowColumnLayout (p = TemJntSkinLayout ) ;

#     # free space
#     pm.rowColumnLayout (p = mainBpmRigTab ,h=8) ;
#     mc.tabLayout(p=mainBpmRigTab, w= 248)




#     # reColor Re-Status --------------------------------------
#     reColorColumn= pm.rowColumnLayout ( nc = 1 , p = HdGeoTab ) ;
#     # pm.rowColumnLayout (p = reColorColumn,h=8 ) ;
#     pm.text( label='# Re-Check Status' , p = reColorColumn, al = "left")
#     pm.rowColumnLayout (p = reColorColumn,h=8 ) ;
#     ResetLayout = pm.rowColumnLayout ( nc = 3 , p = reColorColumn , w = 220, cw=[(1,10),(2,228),(3,10)]) ;
#     pm.rowColumnLayout (p = ResetLayout ) ;
#     pm.symbolCheckBox (image = ("%s%s" % (pngImags(),"Reset.png")), p = ResetLayout , h=25, onc = reStatusHdGeo , bgc = (0.411765, 0.411765, 0.411765)) ;
#     pm.rowColumnLayout (p = ResetLayout ) ;
#     # line TabB
#     rowLine_TabB = pm.rowColumnLayout ( nc = 1 , p = reColorColumn) ;
#     pm.rowColumnLayout (p = rowLine_TabB,h=4 ) ;
#     mc.tabLayout(p=rowLine_TabB, w= 248)

# #-----------------------------
# # eyeRigTab -------------------------
# # eyeRigTab -------------------------
#     eyeRigTab = pm.rowColumnLayout ( "Eye", nc = 1 , p = mainTab_A ) ;
#     mainEyeRigTab = pm.rowColumnLayout ( nc = 1 , p = eyeRigTab , w = 250) ;
#     # line TabA
#     rowLine_TabB = pm.rowColumnLayout ( nc = 1 , p = mainEyeRigTab ) ;
#     mc.tabLayout(p=rowLine_TabB, w= 248)

#     pm.rowColumnLayout (p = rowLine_TabB ,h=8) ;
#     pm.text( label='# Create Bsh' , p = rowLine_TabB, al = "left")
#     pm.rowColumnLayout (p = rowLine_TabB ,h=8) ;

#     rowEye = pm.rowColumnLayout ( nc = 3 , p = rowLine_TabB , cw=[(1,10),(2,228),(3,10)]) ;
#     pm.rowColumnLayout (p = rowEye ) ;
#     # pm.rowColumnLayout (p = rowEye ,h=8) ;
#     pm.button ( 'CreateEyeBshButton' , label = 'Create Eye Bsh', h=20 , p = rowEye , c = createEyeBshFile, bgc = (0.411765, 0.411765, 0.411765)) ; 
#     pm.rowColumnLayout (p = rowEye ) ;
#     pm.rowColumnLayout (p = rowEye ) ;
#     pm.rowColumnLayout (p = rowEye ) ;
#     pm.rowColumnLayout (p = rowEye ) ;
#     pm.rowColumnLayout (p = rowEye ) ;
#     pm.button ( 'CopyShapeMButton' , label = 'Copy Shape', h=20 , p = rowEye , c = copyShapeFile, bgc = (0.411765, 0.411765, 0.411765)) ; 
#     pm.rowColumnLayout (p = rowEye) ;
#     pm.rowColumnLayout (p = rowLine_TabB ,h=4) ;
#     # line TabA
#     rowLine_TabB = pm.rowColumnLayout ( nc = 1 , p = mainEyeRigTab ) ;
#     mc.tabLayout(p=rowLine_TabB, w= 248)

#     ## create mark on face ##
#     markMain_Tab = pm.columnLayout(cal="center", cat=["both", 10], adj=1, p=eyeRigTab)

#     pm.separator(st="none", h=10, p=eyeRigTab)
#     pm.text(l="# Create Wrinkled Rig", al="left", p=markMain_Tab)
#     pm.separator(st="none", h=10, p=markMain_Tab)

#     elem_Tab = pm.rowLayout(nc=5, p = markMain_Tab)
#     pm.text(l="Elem : ", w=50, p=elem_Tab)
#     elem_txt = pm.textField("elem_txt", w=160, p=elem_Tab)
#     pm.separator(st="none", w=10, p=elem_Tab)

#     axis_Tab = pm.rowLayout(nc=5, p = markMain_Tab)
#     pm.text(l="Axis : ", w=50, p=axis_Tab)
#     axis_radioCol = pm.radioCollection("axis_radioCol", p=axis_Tab)
#     axis_x = pm.radioButton("axis_x", l="X", w=60, p=axis_Tab)
#     axis_y = pm.radioButton("axis_y", l="Y", w=60, p=axis_Tab)
#     axis_z = pm.radioButton("axis_z", l="Z", w=60, p=axis_Tab)
#     pm.radioCollection(axis_radioCol, e=True, sl=axis_y)

#     pm.separator(st="none", h=10, p=markMain_Tab)
#     pm.text(l="Please select geo edges before press 'Run'", p=markMain_Tab)
#     pm.separator(st="none", h=10, p=markMain_Tab)
#     runCreate_Butt = pm.button(l="Run", h=30, p=markMain_Tab)
#     pm.button(runCreate_Butt, e=True, c=partial(createWrinkleRig, runCreate_Butt))

#     pm.separator(st="none", h=20, p=markMain_Tab)
#     pm.text(l="# Parent Wrinkled Rig", al="left", p=markMain_Tab)
#     pm.separator(st="none", h=10, p=markMain_Tab)

#     motherGeo_Tab = pm.rowLayout(nc=5, p = markMain_Tab)
#     pm.text(l="posGeo : ", w=50, p=motherGeo_Tab)
#     motherGeo_Txt = pm.textField("motherGeo_Txt", w=110, p=motherGeo_Tab)
#     pm.separator(st="none", w=10, p=motherGeo_Tab)
#     motherGeo_Butt = pm.button(l="<< Add", w=50, p=motherGeo_Tab)
#     pm.button(motherGeo_Butt, e=True, c=partial(addSelectionOnField, motherGeo_Txt, motherGeo_Butt))

#     geoGrp_Tab = pm.rowLayout(nc=5, p = markMain_Tab)
#     pm.text(l="geoGrp : ", w=50, p=geoGrp_Tab)
#     geoGrp_Txt = pm.textField("geoGrp_Txt", w=110, p=geoGrp_Tab)
#     pm.separator(st="none", w=10, p=geoGrp_Tab)
#     geoGrp_Butt = pm.button(l="<< Add", w=50, p=geoGrp_Tab)
#     pm.button(geoGrp_Butt, e=True, c=partial(addSelectionOnField, geoGrp_Txt, geoGrp_Butt))

#     ctrlGrp_Tab = pm.rowLayout(nc=5, p = markMain_Tab)
#     pm.text(l="ctrlGrp : ", w=50, p=ctrlGrp_Tab)
#     ctrlGrp_Txt = pm.textField("ctrlGrp_Txt", w=110, p=ctrlGrp_Tab)
#     pm.separator(st="none", w=10, p=ctrlGrp_Tab)
#     ctrlGrp_Butt = pm.button(l="<< Add", w=50, p=ctrlGrp_Tab)
#     pm.button(ctrlGrp_Butt, e=True, c=partial(addSelectionOnField, ctrlGrp_Txt, ctrlGrp_Butt))

#     stillGrp_Tab = pm.rowLayout(nc=5, p = markMain_Tab)
#     pm.text(l="stillGrp : ", w=50, p=stillGrp_Tab)
#     stillGrp_Txt = pm.textField("stillGrp_Txt", w=110, p=stillGrp_Tab)
#     pm.separator(st="none", w=10, p=stillGrp_Tab)
#     stillGrp_Butt = pm.button(l="<< Add", w=50, p=stillGrp_Tab)
#     pm.button(stillGrp_Butt, e=True, c=partial(addSelectionOnField, stillGrp_Txt, stillGrp_Butt))

#     pm.separator(st="none", h=10, p=markMain_Tab)
#     runConnect_Butt = pm.button(l="Run", h=30, p=markMain_Tab)
#     pm.button(runConnect_Butt, e=True, c=partial(connectWrinkledRig, runConnect_Butt))

#     pm.separator(st="in", h=20, p=markMain_Tab)

# # ebRigTab -------------------------
# # ebRigTab -------------------------
#     ebRigTab = pm.rowColumnLayout ( "Eb", nc = 1 , p = mainTab_A ) ;
#     mainEbRigTab = pm.rowColumnLayout ( nc = 1 , p = ebRigTab , w = 250) ;
#     mc.tabLayout(p=mainEbRigTab, w= 248)
#     pm.rowColumnLayout (p = mainEbRigTab ,h=8) ;
#     pm.text( label='# Eb Rig' , p = mainEbRigTab, al = "left")
#     pm.rowColumnLayout (p = mainEbRigTab ,h=8) ;

#     bshColumn2= pm.rowColumnLayout ( nc = 4 , p = mainEbRigTab ,cw= [(1,10), (2, 114) , (3, 114),(4,10)]) ;
#     # ebJntTmp Button
#     pm.rowColumnLayout (p = bshColumn2 ) ;
#     pm.button ('ebJntTmpButton', label = "Eb Jnt Tmp" , p = bshColumn2, h=20,c = ebJntTmp,  bgc = (0.411765, 0.411765, 0.411765)) ;
#     # create BlendShape Hd to Fcl --------------------------------------
#     pm.button ('EbCreateBshButton',label = 'Eb CreateBsh', p = bshColumn2 ,  h=20, c = ebCreateBsh , bgc = (0.411765, 0.411765, 0.411765)) ;
#     pm.rowColumnLayout (p = bshColumn2 ) ;

#     pm.rowColumnLayout (p = bshColumn2 ) ;
#     pm.rowColumnLayout (p = bshColumn2 ) ;
#     pm.rowColumnLayout (p = bshColumn2 ) ;
#     pm.rowColumnLayout (p = bshColumn2 ) ;
    
#     # MirrorOrientation button
#     pm.rowColumnLayout (p = bshColumn2 ) ;
#     pm.button ('MirrorOrientationButton',label = 'Mirror Orientation', p = bshColumn2 ,  h=20, c = ebmirrorOrientSelected , bgc = (0.411765, 0.411765, 0.411765)) ;
#    # create BlendShape 2 Head
#     pm.button ('EbWeightDividerButton',label = 'Weight Divider', p = bshColumn2 ,  h=20, c = ebWeightDivider , bgc = (0.411765, 0.411765, 0.411765)) ;
#     # pm.rowColumnLayout (p = mainEbRigTab ,h=8) ;
#     rowLine1 = pm.rowColumnLayout ( nc = 3 , p = mainEbRigTab , h = 10, w = 248 ) ;
#     mc.tabLayout(p=rowLine1, w= 248)
#     # pm.button (label = "-"*200 ,h = 10, w = 248, p = rowLine1 , bgc = (0.333333, 0.333333, 0.333333)) ; 
    
#     rowCopyShape_A = pm.rowColumnLayout ( nc = 1 , p = mainEbRigTab , w = 248 ) ;
#     pm.rowColumnLayout (p = rowCopyShape_A ,h=8) ;
#     pm.text( label='# Duplicate ProxyBsh From Eb' , p = rowCopyShape_A, al = "left")
#     pm.rowColumnLayout (p = rowCopyShape_A ,h=8) ;

#     rowColumnDupEbFromBsh = pm.rowColumnLayout ( nc = 5 , p = mainEbRigTab ,cw= [(1,10), (2,70),(3, 109) , (4, 49),(5,10)]) ;

#     pm.rowColumnLayout (p = rowColumnDupEbFromBsh) ;
#     pm.text( label='Ref BshRig ' , p = rowColumnDupEbFromBsh, al = "left")
#     textBodyBshRig_Geo = pm.textField( "txtFieldBodyBshRig_Geo", text = "Bsh", p = rowColumnDupEbFromBsh , editable = True , h=20) 
#     # pm.rowColumnLayout (p = rowColumnDupEbFromBsh) ;
#     pm.button ( 'refBshRigButton' , label = 'REF', h=20 , p = rowColumnDupEbFromBsh , c = refBshRig , bgc = (0.411765, 0.411765, 0.411765)) ; 
#     pm.rowColumnLayout (p = rowColumnDupEbFromBsh) ;

#     pm.rowColumnLayout (p = rowColumnDupEbFromBsh ) ;
#     pm.text(label= "BodyBsh :", p = rowColumnDupEbFromBsh );
#     pm.textField( "txtFieldBodyGeoBsn", text = "..:BodyBshRig_Geo" , p = rowColumnDupEbFromBsh , editable = True , h=20) 
#     pm.button ( 'AddBodyGeoBshEb' , label = '<< Add', h=20 , p = rowColumnDupEbFromBsh ,c = AddBodyBshEbFile ,  bgc = (0.411765, 0.411765, 0.411765)) ; 
#     pm.rowColumnLayout (p = rowColumnDupEbFromBsh ) ;

#     rowColumnDupEyeBrowFromBsh = pm.rowColumnLayout ( nc = 5 , p = mainEbRigTab ,cw= [(1,10), (2,70),(3, 109) , (4, 49),(5,10)]) ;
#     pm.rowColumnLayout (p = rowColumnDupEyeBrowFromBsh ) ;
#     pm.text(label= "EyebrowBsh :", p = rowColumnDupEyeBrowFromBsh );
#     pm.textField( "txtFieldEyebrowGeoBsn", text = "..:EyebrowBshRig_Geo" , p = rowColumnDupEyeBrowFromBsh , editable = True , h=20) 
#     pm.button ( 'AddEyeBrowGeoBshEb' , label = '<< Add', h=20 , p = rowColumnDupEyeBrowFromBsh ,c = AddEyebrowBshEbFile ,  bgc = (0.411765, 0.411765, 0.411765)) ; 
#     pm.rowColumnLayout (p = rowColumnDupEyeBrowFromBsh ) ;

#     # rowColumnDupEbFromBsh = pm.rowColumnLayout ( nc = 4 , p = rowCopyShape_A , w = 250 , cw = [ ( 1 , 104 ) , ( 2 , 50 ) , (3, 64) , (4, 30)] ) ;
#     rowColumnDupEbFromBsh2 = pm.rowColumnLayout ( nc = 5 , p = mainEbRigTab ,cw= [(1,10), (2,100),(3, 79) , (4, 49),(5,10)]) ;
#     pm.rowColumnLayout (p = rowColumnDupEbFromBsh2 ) ;
#     pm.text(label= "TranslateX Offset :", p = rowColumnDupEbFromBsh2 );
#     pm.textField( "txtFieldTyOffset", text = "40" , p = rowColumnDupEbFromBsh2 , editable = True , h=20) 
#     pm.rowColumnLayout (p = rowColumnDupEbFromBsh2 ) ;
#     pm.rowColumnLayout (p = rowColumnDupEbFromBsh2 ) ;

#     DEFBRow = pm.rowColumnLayout ( nc = 3 , p = mainEbRigTab ,cw =[(1, 10),(2, 231),(3,10)]) ;
#     pm.rowColumnLayout (p = DEFBRow ) ;
#     pm.button ( 'DuplicateEbFromBshButton' , label = 'Duplicate Eb From Bsh', h=20, p = DEFBRow , c = dupProxyEbBshFile , bgc = (0.411765, 0.411765, 0.411765)) ; 
#     pm.rowColumnLayout (p = DEFBRow ) ;


#     SpliteRowColumn = pm.rowColumnLayout ( nc = 1 , p = mainEbRigTab , w = 248 ) ;
#     mc.tabLayout(p=SpliteRowColumn, w= 248)
#     # pm.button (label = "-"*200 ,h = 10, w = 248, p = rowLine1 , bgc = (0.333333, 0.333333, 0.333333)) ; 
#     rowCopyShape_A = pm.rowColumnLayout ( nc = 1 , p = SpliteRowColumn , w = 248 ) ;
#     pm.rowColumnLayout (p = rowCopyShape_A ,h=8) ;
#     pm.text( label='# Split Eb Shape' , p = rowCopyShape_A, al = "left")
#     pm.rowColumnLayout (p = rowCopyShape_A ,h=8) ;

#     rowColumnEbRig1 = pm.rowColumnLayout ( nc = 6 , p = mainEbRigTab , w = 250 , cw = [ (1, 10), ( 2 , 90 ) , ( 3 , 20 ), ( 4 , 100 ) , ( 5 , 20 ), (6, 10)] ) ;

#     pm.rowColumnLayout (p = rowColumnEbRig1 )
#     txtFieldBodySides = pm.textField( "txtFieldBodySide", text = "BodySide", parent = rowColumnEbRig1 , editable = True  ) 
#     pm.button ( 'bodySideButton' , label = '<<', p = rowColumnEbRig1 , c = addTxtFieldBodySide , bgc = (0, 0, 0)) ; 
#     txtFieldEbSides = pm.textField( "txtFieldEbSide", text = "EbSide", parent = rowColumnEbRig1 , editable = True ) 
#     pm.button ( 'ebSideButton' , label = '<<', p = rowColumnEbRig1 , c = addTxtFieldEbSide , bgc = (0, 0, 0)) ; 
#     pm.rowColumnLayout (p = rowColumnEbRig1 )
#     pm.rowColumnLayout (p = rowColumnEbRig1 )    
#     txtFieldBodyEachs = pm.textField( "txtFieldBodyEach", text = "BodyEach", parent = rowColumnEbRig1 , editable = True )
#     pm.button ( 'bodyEachButton' , label = '<<', p = rowColumnEbRig1 , c = addTxtFieldBodyEach , bgc = (0, 0, 0)) ; 
#     txtFieldEbEachs = pm.textField( "txtFieldEbEach", text = "EbEach", parent = rowColumnEbRig1 , editable = True ) 
#     pm.button ( 'ebEachButton' , label = '<<', p = rowColumnEbRig1 , c = addTxtFieldEbEach , bgc = (0, 0, 0)) ; 
#     pm.rowColumnLayout (p = rowColumnEbRig1 )

#     splitEbShapeRow = pm.rowColumnLayout ( nc = 3 , p = mainEbRigTab ,cw =[(1, 10),(2, 231),(3,10)]) ;
#     pm.rowColumnLayout (p = splitEbShapeRow )
#     pm.button ( 'copyShapeToBsh' , label = ' Split', h=20 , p = splitEbShapeRow , c = splitEbShapeFile , bgc = (0.411765, 0.411765, 0.411765)) ; 
#     splitEbShapeRow2 = pm.rowColumnLayout ( nc = 1 , p = mainEbRigTab ) ;

#     # line TabB
#     reColorColumn = pm.rowColumnLayout ( nc = 1 , p = mainEbRigTab , w = 248 ) ;
#     mc.tabLayout(p=reColorColumn, w= 248)
#     # pm.button (label = "-"*200 ,h = 10, w = 248, p = reColorColumn , bgc = (0.333333, 0.333333, 0.333333)) ; 
#     pm.rowColumnLayout (p = reColorColumn ,h=8) ;
#     pm.text( label='# Re-Check Status' , p = reColorColumn, al = "left")
#     pm.rowColumnLayout (p = reColorColumn ,h=8) ;
#     # reColor Re-Status --------------------------------------
#     ResetLayout = pm.rowColumnLayout ( nc = 3 , p = reColorColumn , w = 220, cw=[(1,10),(2,228),(3,10)]) ;
#     pm.rowColumnLayout (p = ResetLayout ) ;
#     pm.symbolCheckBox (image = ("%s%s" % (pngImags(),"Reset.png")), p = ResetLayout , h=25, onc = reStatusEb , bgc = (0.411765, 0.411765, 0.411765)) ;
#     pm.rowColumnLayout (p = ResetLayout) ;
#     # line TabB
#     rowLine_TabB = pm.rowColumnLayout ( nc = 1 , p = mainEbRigTab , h = 10 ) ;
#     # pm.button (label = "-"*200 ,h = 10, w = 248, p = rowLine_TabB , bgc = (0.333333, 0.333333, 0.333333)) ; 
#     mc.tabLayout(p=rowLine_TabB, w= 248)












# # mouthTab -------------------------
# # mouthTab -------------------------
#     bshTab = pm.rowColumnLayout ( "M", nc = 1 , p = mainTab_A ) ;
#     mainBshTab = pm.rowColumnLayout ( nc = 1 , p = bshTab , w = 250) ;

#     mc.tabLayout(p=mainBshTab, w= 248)
#     pm.rowColumnLayout (p = mainBshTab ,h=8) ;
#     pm.text( label='# Create Loft' , p = mainBshTab, al = "left")
#     pm.rowColumnLayout (p = mainBshTab ,h=8) ;

#     rowCreateAdd12CV = pm.rowColumnLayout ( nc = 6 , p = mainBshTab , w = 248 , cw = [(1, 10),(2,39),(3,35),(4,30),(5,124),(6,10)]) ;
#     pm.rowColumnLayout (p = rowCreateAdd12CV) ;
#     pm.text( label=' Curve ' , p = rowCreateAdd12CV, al = "center")
#     textGeoBase = pm.textField( "txtFieldAdd12CV", text = "12", p = rowCreateAdd12CV , editable = True , h=20) 
#     pm.text( label='CV' , p = rowCreateAdd12CV, al = "center")
#     pm.button ( 'Create12CVButton' , label = 'Create 12 CV for Lips', h=20 , p = rowCreateAdd12CV , c = create12CVFile , bgc = (0.411765, 0.411765, 0.411765)) ; 
#     pm.rowColumnLayout (p = rowCreateAdd12CV) ;

#     rowCreate12CV = pm.rowColumnLayout ( nc = 3 , p = mainBshTab , w = 248 , cw = [(1, 10),(2, 228), (3,10)]) ;
#     pm.rowColumnLayout (p = rowCreate12CV) ;
#     pm.button ( 'CreateLoftButton' , label = 'Create Loft for Lips', h=20 , p = rowCreate12CV , c = createLoftFile , bgc = (0.411765, 0.411765, 0.411765)) ; 
#     pm.rowColumnLayout (p = rowCreate12CV) ;

#     rowCopyShape_A = pm.rowColumnLayout ( nc = 1 , p = mainBshTab , w = 248 ) ;
#     rowLine2 = pm.rowColumnLayout ( nc = 1 , p = rowCopyShape_A , w = 248 ) ;
#     mc.tabLayout(p=rowLine2, w= 248)
#     pm.rowColumnLayout (p = rowLine2 ,h=8) ;

#     pm.text( label='# Duplicate Proxy Mth and Set Rotate Jaw' , p = rowLine2, al = "left")
#     pm.rowColumnLayout (p = rowLine2 ,h=8) ;
#     rowDuplicateProxyMthBsh = pm.rowColumnLayout ( nc = 3 , p = rowCopyShape_A , w = 248 , cw = [(1, 10),(2, 228), (3,10)]) ;

#     rowCreateMthBsh = pm.rowColumnLayout ( nc = 3 , p = rowLine2 , w = 248 , cw = [(1, 10),(2, 228), (3,10)]) ;
#     pm.rowColumnLayout (p = rowCreateMthBsh) ;
#     pm.button ( 'CreateMthBshButton' , label = '1. Create Mth Bsh', h=20 , p = rowCreateMthBsh , c = createMthBsh , bgc = (0.411765, 0.411765, 0.411765)) ; 
#     pm.rowColumnLayout (p = rowCreateMthBsh) ;

#     rowColumnCorrec = pm.rowColumnLayout ( nc = 6 , p = rowDuplicateProxyMthBsh , w = 250 , cw = [(1,10), ( 2 , 74 ) , (3 , 30 ) , (4, 94) , (5, 30), (6,10)] ) ;
#     pm.rowColumnLayout (p = rowColumnCorrec) ; 
#     tettyOffset = pm.text(label= "2. tyOffset >>", p = rowColumnCorrec );
#     txtFieldTyOffsets = pm.textField( "txtFieldTyOffsetMB", text = "40" , parent = rowColumnCorrec , editable = True , h=20, w = 30) 
#     tetJawMthRotateX = pm.text(label= "3. jawMth RX >>", p = rowColumnCorrec );
#     txtFieldJawMthRotateXs = pm.textField( "txtFieldJawMthRX", text = "20" , parent = rowColumnCorrec , editable = True , h=20, w = 30) 
#     pm.rowColumnLayout (p = rowColumnCorrec) ; 

#     rowColumnGeoBase1 = pm.rowColumnLayout ( nc = 6 , p = rowCopyShape_A , w = 250 , cw = [(1,10),(2 , 85), ( 3 , 113 ) ,(4,5), ( 5 , 25 ),(6,10)] ) ;

#     pm.rowColumnLayout (p = rowColumnGeoBase1) ;
#     pm.text( label=' 4. Ref BshRig ' , p = rowColumnGeoBase1, al = "left")
#     textBodyBshRig_Geo = pm.textField( "txtFieldBodyBshRig_Geo", text = "Bsh", p = rowColumnGeoBase1 , editable = True , h=20) 
#     pm.rowColumnLayout (p = rowColumnGeoBase1) ;
#     pm.button ( 'refBshRigButton' , label = 'REF', h=20 , p = rowColumnGeoBase1 , c = refBshRig , bgc = (0.411765, 0.411765, 0.411765)) ; 
#     pm.rowColumnLayout (p = rowColumnGeoBase1) ;

#     pm.rowColumnLayout (p = rowColumnGeoBase1) ;
#     pm.text( label=' 5. BshRig Geo' , p = rowColumnGeoBase1, al = "left")
#     textEyeRig = pm.textField( "txtFieldBodyBshRig_Geo2", text = ".....:BodyBshRig_Geo", p = rowColumnGeoBase1 , editable = True )
#     pm.rowColumnLayout (p = rowColumnGeoBase1) ;
#     pm.button ("addBodyBshRig_Geo",label = "<<" , p = rowColumnGeoBase1 , c = addTextBodyBshRig_Geo ,bgc = (0.411765, 0.411765, 0.411765)) ; 
#     pm.rowColumnLayout (p = rowColumnGeoBase1) ;

#     pm.rowColumnLayout (p = rowColumnGeoBase1) ;
#     pm.text( label=' 6. Obj01 BshRig ' , p = rowColumnGeoBase1, al = "left")
#     textBshRig1 = pm.textField( "txtFieldObj01BshRig_Geo", text = "..:Obj01BshRig_Geo", p = rowColumnGeoBase1 , editable = True )
#     pm.rowColumnLayout (p = rowColumnGeoBase1) ;
#     pm.button ("addObj01BshRig_Geo",label = "<<" , p = rowColumnGeoBase1 , c = addTextObj01BshRig_Geo ,bgc = (0.411765, 0.411765, 0.411765)) ; 
#     pm.rowColumnLayout (p = rowColumnGeoBase1) ;

#     pm.rowColumnLayout (p = rowColumnGeoBase1) ;
#     pm.text( label=' 7. Obj02 BshRig ' , p = rowColumnGeoBase1, al = "left")
#     textBshRig2 = pm.textField( "txtFieldObj02BshRig_Geo", text = "..:Obj02BshRig_Geo", p = rowColumnGeoBase1 , editable = True )
#     pm.rowColumnLayout (p = rowColumnGeoBase1) ;
#     pm.button ("addObj02BshRig_Geo",label = "<<" , p = rowColumnGeoBase1 , c = addTextObj02BshRig_Geo ,bgc = (0.411765, 0.411765, 0.411765)) ; 



#     rowLine2 = pm.rowColumnLayout ( nc = 1 , p = rowCopyShape_A , w = 248 ) ;

#     rowCreateMthBsh = pm.rowColumnLayout ( nc = 3 , p = rowLine2 , w = 248 , cw = [(1, 10),(2, 228), (3,10)]) ;
#     pm.rowColumnLayout (p = rowCreateMthBsh) ;
#     pm.button ( 'DuplicateProxyMthBshButton' , label = '8. Duplicate Proxy MthBsh ', h=20, p = rowCreateMthBsh , c = dupProxyMthBshFile , bgc = (0.411765, 0.411765, 0.411765)) ; 
#     pm.rowColumnLayout (p = rowLine2) ; 


#     mc.tabLayout(p=rowLine2, w= 248)
#     pm.rowColumnLayout (p = rowLine2 ,h=8) ;

#     rowColumnGeoBase2 = pm.rowColumnLayout ( nc = 6 , p = rowCopyShape_A , w = 250 , cw = [(1,10),(2 , 85), ( 3 , 113 ) ,(4,5), ( 5 , 25 ),(6,10)] ) ;

#     pm.rowColumnLayout (p = rowColumnGeoBase2) ;
#     pm.text( label=' 9. Paths Rig' , p = rowColumnGeoBase2, al = "left")
#     textPathsRig_Geo = pm.textField( "txtFieldPathsRig_Geo", text = "", p = rowColumnGeoBase2 , editable = True , h=20) 
#     pm.rowColumnLayout (p = rowColumnGeoBase2) ;
#     pm.button ( 'addPathRig_Geo' , label = '<<', h=20 , p = rowColumnGeoBase2 , c = addTextPathsMthRig_Geo , bgc = (0.411765, 0.411765, 0.411765)) ; 
#     pm.rowColumnLayout (p = rowColumnGeoBase2) ;

#     pm.rowColumnLayout (p = rowColumnGeoBase2) ;
#     pm.text( label=' 10. MthRig Geo ' , p = rowColumnGeoBase2, al = "left")
#     textBodyMthRig_Geo = pm.textField( "txtFieldBodyMthRig_Geo", text = "BodyMthRig_Geo", p = rowColumnGeoBase2 , editable = True , h=20) 
#     pm.rowColumnLayout (p = rowColumnGeoBase2) ;
#     pm.button ( 'addBodyMthRig_Geo' , label = '<<', h=20 , p = rowColumnGeoBase2 , c = addTextBodyMthRig_Geo , bgc = (0.411765, 0.411765, 0.411765)) ; 
#     pm.rowColumnLayout (p = rowColumnGeoBase2) ;
#     pm.rowColumnLayout (p = rowColumnGeoBase2) ;
#     pm.text( label=' 11. MthJaw Geo ' , p = rowColumnGeoBase2, al = "left")
#     textBodyMthJawRig_Geo = pm.textField( "txtFieldBodyMthJawRig_Geo", text = "BodyMthJawRig_Geo", p = rowColumnGeoBase2 , editable = True , h=20) 
#     pm.rowColumnLayout (p = rowColumnGeoBase2) ;
#     pm.button ( 'addBodyMthJawRig_Geo' , label = '<<', h=20 , p = rowColumnGeoBase2 , c = addTextBodyMthJawRig_Geo , bgc = (0.411765, 0.411765, 0.411765)) ; 
#     pm.rowColumnLayout (p = rowColumnGeoBase2) ;
#     pm.rowColumnLayout (p = rowColumnGeoBase2) ;
#     pm.text( label=' 12. MthLip Geo' , p = rowColumnGeoBase2, al = "left")
#     textBodyMthLipRig_Geo = pm.textField( "txtFieldBodyMthLipRig_Geo", text = "BodyMthLipRig_Geo", p = rowColumnGeoBase2 , editable = True , h=20) 
#     pm.rowColumnLayout (p = rowColumnGeoBase2) ;
#     pm.button ( 'addBodyMthLipRig_Geo' , label = '<<', h=20 , p = rowColumnGeoBase2 , c = addTextBodyMthLipRig_Geo , bgc = (0.411765, 0.411765, 0.411765)) ; 
#     pm.rowColumnLayout (p = rowColumnGeoBase2) ;

#     # rowLine3 = pm.rowColumnLayout ( nc = 1 , p = rowCopyShape_A , w = 248 ) ;
#     rowLine3 = pm.rowColumnLayout ( nc = 3 , p = rowCopyShape_A , w = 248 , cw = [(1, 10),(2, 228), (3,10)]) ;

#     pm.rowColumnLayout (p = rowLine3) ; 
#     pm.button ( 'DuplicateCorrecMthShapeButton' , label = ' 13. Duplicate Corrective MthShape', h=20 , p = rowLine3 , c = proxyCrtMthBsnFile , bgc = (0.411765, 0.411765, 0.411765)) ; 
#     pm.rowColumnLayout (p = rowLine3) ; 


#     rowLine3 = pm.rowColumnLayout ( nc = 3 , p = rowCopyShape_A , w = 248 , cw = [(1, 10),(2, 228), (3,10)]) ;

#     pm.rowColumnLayout (p = rowLine3) ; 
#     pm.button ('addBSHJLMthButton',label = '14.  Add Bsh Body 2 Mth', p = rowLine3 ,  h=20, c = doAddBSHMthFile , bgc = (0.411765, 0.411765, 0.411765)) ;
#     pm.rowColumnLayout (p = rowLine3) ; 

#     rowLineSplitShape = pm.rowColumnLayout ( nc = 1 , p = rowCopyShape_A , w = 248 ) ;
#     mc.tabLayout(p=rowLineSplitShape, w= 248)
#     pm.rowColumnLayout (p = rowLineSplitShape ,h=8) ;

#     pm.text( label='# Split Corrective Mouth' , p = rowLineSplitShape, al = "left")
#     pm.rowColumnLayout (p = rowLineSplitShape ,h=8) ;

#     rowMouthcopyShapeToBsh =  pm.rowColumnLayout ( nc = 3 , p = rowCopyShape_A , w = 248 , cw = [(1, 10),(2, 228), (3,10)]) ;
#     pm.rowColumnLayout (p = rowMouthcopyShapeToBsh) ; 
#     pm.button ( 'MouthcopyShapeToBsh' , label = '15. Mouth ', h=20, p = rowMouthcopyShapeToBsh , c = copyShapeMouths , bgc = (0.411765, 0.411765, 0.411765)) ; 
#     pm.rowColumnLayout (p = rowMouthcopyShapeToBsh) ; 

#     rowColumnShapeLR = pm.rowColumnLayout ( nc = 6 , p = rowCopyShape_A , w = 250 , cw = [(1,10), (2,84), (3,30), (4,84), (5,30), (6,10)] ) ;
#     pm.rowColumnLayout (p = rowColumnShapeLR) ; 
#     ValueRGT = pm.text(label= "Value RGT >>", p = rowColumnShapeLR );
#     txtFieldValueRGTs = pm.textField( "txtFieldValueRGT", text = "0.1" , parent = rowColumnShapeLR , editable = True , h=20) 
#     ValueLFT = pm.text(label= "Value LFT >>", p = rowColumnShapeLR );
#     txtFieldValueLFTs = pm.textField( "txtFieldValueLFT", text = "0.1" , parent = rowColumnShapeLR , editable = True , h=20) 
#     pm.rowColumnLayout (p = rowColumnShapeLR) ; 

#     rowColumn2 = pm.rowColumnLayout ( nc = 5 , p = mainBshTab , w = 250 , cw = [(1,10), ( 2 , 114 ), ( 3, 114 ),(4,10)] ) ;
#     pm.rowColumnLayout (p = rowColumn2) ; 
#     pm.button ( 'MouthRGTcopyShapeToBsh' , label = '16. RGT ', h=20 , p = rowColumn2 , c = copyShapeMouth_RGT, bgc = (0.411765, 0.411765, 0.411765)) ; 
#     pm.button ( 'MouthLFTcopyShapeToBsh' , label = '17. LFT ', h=20 , p = rowColumn2 , c = copyShapeMouth_LFT, bgc = (0.411765, 0.411765, 0.411765)) ;
#     pm.rowColumnLayout (p = rowColumn2) ; 

# # 0. copyShape A
#     rowLine1 = pm.rowColumnLayout ( nc = 1 , p = mainBshTab ) ;
#     # pm.rowColumnLayout (p = rowLine1 ,h=8) ;
#     mc.tabLayout(p=rowLine1, w= 248)
#     pm.rowColumnLayout (p = rowLine1 ,h=8) ;

#     pm.text( label='# Create And Edit BaseShape' , p = rowLine1, al = "left")
#     pm.rowColumnLayout (p = rowLine1 ,h=8) ;

#     rowColumnGeoBase = pm.rowColumnLayout ( nc = 5 , p = mainBshTab , w = 250 , cw = [(1,10), ( 2 , 148 ) ,(3,10), ( 4 , 70 ),(5,10)] ) ;
#     pm.rowColumnLayout (p = rowColumnGeoBase) ;
#     textGeoBase = pm.textField( "txtFieldGeoBase", text = "BodyMthRig_Geo", parent = rowColumnGeoBase , editable = True , h=20) 
#     pm.rowColumnLayout (p = rowColumnGeoBase) ;
#     pm.button ( 'addGeoBaseToBsh' , label = '<< Add ', h=20 , p = rowColumnGeoBase , c = addTextGeoBase , bgc = (0.411765, 0.411765, 0.411765)) ; 
#     pm.rowColumnLayout (p = rowColumnGeoBase) ;

#     rowRevertToGeoBase = pm.rowColumnLayout ( nc = 3 , p = mainBshTab , w = 248 , cw = [(1, 10),(2, 228), (3,10)]) ;
#     pm.rowColumnLayout (p = rowRevertToGeoBase) ;
#     pm.button ( 'copyShapeToBsh' , label = ' Revert to GeoBase ', h=20 , p = rowRevertToGeoBase , c = getGeoBase , bgc = (0.411765, 0.411765, 0.411765)) ; 
#     pm.rowColumnLayout (p = rowRevertToGeoBase) ;
    
#     rowRevertDefaultShapeDelHis = pm.rowColumnLayout ( nc = 5 , p = mainBshTab , w = 250 , cw = [(1,10), ( 2 , 114 ), ( 3, 114 ),(4,10)] ) ;
#     pm.rowColumnLayout (p = rowRevertDefaultShapeDelHis) ;
#     # pm.button ( 'RevertToDefaultShape' , label = ' Re Def Bsh ', h=20 , p = rowRevertDefaultShapeDelHis , c = ReMthShapeFile , bgc = (0.411765, 0.411765, 0.411765)) ; 
#     pm.button ( 'CopyGeometryShape' , label = ' Copy Shape (Del His)', h=20 , p = rowRevertDefaultShapeDelHis , c = copyShapeDHFile , bgc = (0.411765, 0.411765, 0.411765)) ; 
#     pm.button ( 'copyShapeToBsh' , label = ' Copy Shape ', h=20 , p = rowRevertDefaultShapeDelHis , c = copyShapeFile , bgc = (0.411765, 0.411765, 0.411765)) ; 

#     rowRevertDefaultShapeDelHis2 = pm.rowColumnLayout ( nc = 5 , p = mainBshTab , w = 250 , cw = [(1,10), ( 2 , 114 ), ( 3, 114 ),(4,10)] ) ;
#     pm.rowColumnLayout (p = rowRevertDefaultShapeDelHis2) ;
#     pm.button ( 'RevertToDefaultShape' , label = ' Re Default Bsh (Sel)', h=20 , p = rowRevertDefaultShapeDelHis2 , c = ReMthShapeFile , bgc = (0.411765, 0.411765, 0.411765)) ; 
#     pm.button ( 'CopyGeometryShape' , label = ' Mirror Shape', h=20 , p = rowRevertDefaultShapeDelHis2 , c = mirrorShapeFile , bgc = (0.411765, 0.411765, 0.411765)) ; 
#     pm.rowColumnLayout (p = rowRevertDefaultShapeDelHis) ; 
#     # pm.rowColumnLayout (p = mainBshTab ,h=8) ;

#     # tab re-checkStatus -----------------------------------------------
#     mainBshColumn = pm.rowColumnLayout ( nc = 1 , p = mainBshTab , w = 248 ) ;
#     mc.tabLayout(p=mainBshColumn, w= 248)
#     # pm.button (label = "-"*200 ,h = 10, w = 248, p = reColorColumn , bgc = (0.333333, 0.333333, 0.333333)) ; 
#     pm.rowColumnLayout (p = mainBshColumn ,h=8) ;
#     pm.text( label='# Re-Check Status' , p = mainBshColumn, al = "left")
#     pm.rowColumnLayout (p = mainBshColumn ,h=8) ;
#     # reColor Re-Status --------------------------------------
#     ResetLayout = pm.rowColumnLayout ( nc = 3 , p = mainBshColumn , w = 220, cw=[(1,10),(2,228),(3,10)]) ;
#     pm.rowColumnLayout (p = ResetLayout ) ;
#     pm.symbolCheckBox (image = ("%s%s" % (pngImags(),"Reset.png")), p = ResetLayout , h=25, onc = reStatusMth , bgc = (0.411765, 0.411765, 0.411765)) ;
#     pm.rowColumnLayout (p = ResetLayout) ;
#     # line TabB ----------------------------------------------
#     rowLine_TabB = pm.rowColumnLayout ( nc = 1 , p = mainBshTab , h = 10 ) ;
#     mc.tabLayout(p=rowLine_TabB, w= 248)

   






# # mouthTab -------------------------
# # mouthTab -------------------------
#     bshTab = pm.rowColumnLayout ( "M7", nc = 1 , p = mainTab_A ) ;
#     mainBshTab = pm.rowColumnLayout ( nc = 1 , p = bshTab , w = 250) ;

#     mc.tabLayout(p=mainBshTab, w= 248)
#     pm.rowColumnLayout (p = mainBshTab ,h=8) ;
#     pm.text( label='# Create Curve Mth and MthRigCtrl' , p = mainBshTab, al = "left")
#     # pm.rowColumnLayout (p = mainBshTab ,h=8) ;

#     rowCopyShape_A = pm.rowColumnLayout ( nc = 1 , p = mainBshTab , w = 248 ) ;
#     rowLine2 = pm.rowColumnLayout ( nc = 1 , p = rowCopyShape_A , w = 248 ) ;

#     # pm.rowColumnLayout (p = rowLine2 ,h=8) ;
#     rowDuplicateProxyMthBsh = pm.rowColumnLayout ( nc = 3 , p = rowCopyShape_A , w = 248 , cw = [(1, 10),(2, 228), (3,10)]) ;

#     rowColumnGeoBase1 = pm.rowColumnLayout ( nc = 6 , p = rowCopyShape_A , w = 250 , cw = [(1,10),(2 , 85), ( 3 , 113 ) ,(4,5), ( 5 , 25 ),(6,10)] ) ;

#     rowLine2 = pm.rowColumnLayout ( nc = 1 , p = rowCopyShape_A , w = 248 ) ;

#     rowCreateMthBsh = pm.rowColumnLayout ( nc = 3 , p = rowLine2 , w = 248 , cw = [(1, 10),(2, 228), (3,10)]) ;
#     pm.rowColumnLayout (p = rowCreateMthBsh) ;
#     pm.button ( 'CreateMthTmpButton' , label = '1. Create Mth Tmp ', h=20, p = rowCreateMthBsh , c = mthRigTmpLocFile , bgc = (0.411765, 0.411765, 0.411765)) ; 
#     pm.rowColumnLayout (p = rowLine2) ; 

#     rowCreateMthBsh2 = pm.rowColumnLayout ( nc = 3 , p = rowLine2 , w = 248 , cw = [(1, 10),(2, 228), (3,10)]) ;
#     pm.rowColumnLayout (p = rowCreateMthBsh2) ;
#     pm.button ('CuplicateJaw12Button' , label = '2. Duplicate JawJnt From Ctrl', h=20, p = rowCreateMthBsh2 , c = duplicateMthJawJntObj , bgc = (0.411765, 0.411765, 0.411765)) ; 
#     pm.rowColumnLayout (p = rowLine2) ; 

#     rowCreateMthBsh3 = pm.rowColumnLayout ( nc = 3 , p = rowLine2 , w = 248 , cw = [(1, 10),(2, 228), (3,10)]) ;
#     pm.rowColumnLayout (p = rowCreateMthBsh3) ;
#     pm.button ('CuplicateJaw12Button' , label = '3. ReBuild Curve', h=20, p = rowCreateMthBsh3 , c = reBuildCurveFile , bgc = (0.411765, 0.411765, 0.411765)) ; 
#     pm.rowColumnLayout (p = rowLine2) ; 

#     rowColumnShapeLR = pm.rowColumnLayout ( nc = 6 , p = rowCopyShape_A , w = 250 , cw = [(1,10), (2,84), (3,30), (4,84), (5,30), (6,10)] ) ;
#     pm.rowColumnLayout (p = rowColumnShapeLR) ; 
#     ValueRGT = pm.text(label= "numMthJnt >>", p = rowColumnShapeLR );
#     txtFieldValueRGTs = pm.textField( "txtFieldNumMthJnt", text = "28" , parent = rowColumnShapeLR , editable = True , h=20) 
#     ValueLFT = pm.text(label= "skipJntCen >>", p = rowColumnShapeLR );
#     txtFieldValueLFTs = pm.textField( "txtFieldSkipMthJntCen", text = "2" , parent = rowColumnShapeLR , editable = True , h=20) 
#     pm.rowColumnLayout (p = rowColumnShapeLR) ; 


#     rowLine2 = pm.rowColumnLayout ( nc = 1 , p = rowCopyShape_A , w = 248 ) ;

#     rowCreateMthBsh = pm.rowColumnLayout ( nc = 3 , p = rowLine2 , w = 248 , cw = [(1, 10),(2, 228), (3,10)]) ;
#     pm.rowColumnLayout (p = rowCreateMthBsh) ;
#     pm.button ('CreateMthRigCtrlButton' , label = '4. Create MthRig Ctrl ', h=20, p = rowCreateMthBsh , c = mthRigCtrl , bgc = (0.411765, 0.411765, 0.411765)) ; 
#     pm.rowColumnLayout (p = rowLine2) ; 


#     # tab re-checkStatus -----------------------------------------------
#     mainBshColumn = pm.rowColumnLayout ( nc = 1 , p = mainBshTab , w = 248 ) ;
#     mc.tabLayout(p=mainBshColumn, w= 248)
#     # pm.button (label = "-"*200 ,h = 10, w = 248, p = reColorColumn , bgc = (0.333333, 0.333333, 0.333333)) ; 
#     pm.rowColumnLayout (p = mainBshColumn ,h=8) ;
#     pm.text( label='# Re-Check Status' , p = mainBshColumn, al = "left")
#     pm.rowColumnLayout (p = mainBshColumn ,h=8) ;
#     # reColor Re-Status --------------------------------------
#     ResetLayout = pm.rowColumnLayout ( nc = 3 , p = mainBshColumn , w = 220, cw=[(1,10),(2,228),(3,10)]) ;
#     pm.rowColumnLayout (p = ResetLayout ) ;
#     pm.symbolCheckBox (image = ("%s%s" % (pngImags(),"Reset.png")), p = ResetLayout , h=25, onc = reStatusMth , bgc = (0.411765, 0.411765, 0.411765)) ;
#     pm.rowColumnLayout (p = ResetLayout) ;
#     # line TabB ----------------------------------------------
#     rowLine_TabB = pm.rowColumnLayout ( nc = 1 , p = mainBshTab , h = 10 ) ;
#     mc.tabLayout(p=rowLine_TabB, w= 248)






# # facial CombRig Tab -------------------------
# # facial CombRig Tab -------------------------
#     facialCombRigTab = pm.rowColumnLayout ( "Fcl", nc = 1 , p = mainTab_A ) ;
#     mainfacialCombRigTab1 = pm.rowColumnLayout (p = facialCombRigTab , w = 300) ;
#     mc.tabLayout(p=mainfacialCombRigTab1, w= 248)

#     # mc.tabLayout(p=mainfacialCombRigTab2, w= 248)
#     pm.rowColumnLayout (p = mainfacialCombRigTab1 ,h=8) ;
#     pm.text( label='# Create MainGroup' , p = mainfacialCombRigTab1, al = "left")
#     pm.rowColumnLayout (p = mainfacialCombRigTab1 ,h=8) ;

#     mainfacialCombRigTab2 = pm.rowColumnLayout ( nc = 3 , p = mainfacialCombRigTab1 , w = 248 , cw = [(1, 10),(2, 228), (3,10)]) ;
#     # create MainGroup
#     pm.rowColumnLayout (p = mainfacialCombRigTab2 , h=20 ) ;
#     pm.button ("cteateMainGroupButton",label = "Create MainGroup", h=20, p = mainfacialCombRigTab2 , c = mainGroup, bgc = (0.411765, 0.411765, 0.411765)) ;
#     pm.rowColumnLayout (p = mainfacialCombRigTab2 , h=20 ) ;

#     # duplicate "BodyFclRigPos_Geo"
#     mainfacialCombRigTab = pm.rowColumnLayout ( nc = 4 , p = mainfacialCombRigTab1 , w = 248 , cw = [(1, 10),(2, 170),(3,58), (4,10)]) ;
#     pm.rowColumnLayout (p = mainfacialCombRigTab ) ;
#     pm.textField( "txtFieldBodyFclRigPos", text = "BodyFclRig_Geo", p = mainfacialCombRigTab , editable = True )
#     pm.button ('addBodyFclRigPos', label = "<< Add" ,h=20, p = mainfacialCombRigTab , c = addDupHeadFclRigFile, bgc = (0.411765, 0.411765, 0.411765)) ;
#     pm.rowColumnLayout (p = mainfacialCombRigTab ) ;

#     mainfacialCombRigTab3 = pm.rowColumnLayout ( nc = 3 , p = mainfacialCombRigTab1 , w = 248 , cw = [(1, 10),(2, 228), (3,10)]) ;
#     pm.rowColumnLayout (p = mainfacialCombRigTab3 ) ;
#     pm.button ("CreateBodyFclRigPos_GeoButton",label = "Create ( BodyFclRigPos_Geo )" ,h=20, p = mainfacialCombRigTab3 , c = dupHeadFclRigFile, bgc = (0.411765, 0.411765, 0.411765)) ;
#     pm.rowColumnLayout (p = mainfacialCombRigTab3) ;

#     tabCRF = pm.rowColumnLayout (p = facialCombRigTab , w = 300) ;
#     mc.tabLayout(p=tabCRF, w= 248)
#     pm.rowColumnLayout (p = tabCRF ,h=8) ;
#     pm.text( label='# Create Reference FacialRig' , p = tabCRF, al = "left")
#     pm.rowColumnLayout (p = tabCRF ,h=8) ;

#     # R--------------------------------------------
#     mainfacialComb = pm.rowColumnLayout ( nc = 2 , p = tabCRF , cw = [ ( 1 , 170 ) , ( 2 , 77 )]) ;
#     # Colb_R
#     Colb_R = pm.rowColumnLayout ( nc = 5 , p = mainfacialComb , cw = [ ( 1 , 10 ) , ( 2 , 60 ) , ( 3 , 50 ) , ( 4 , 50 ) , ( 5 , 10 )] ) ;
    
#     pm.rowColumnLayout (p = Colb_R) ;
#     pm.text( label='HdRig File :' , p = Colb_R, al = "left")
#     textEyeRig = pm.textField( "txtFieldHdRig", text = "hdRig", parent = Colb_R , editable = True )
#     pm.button ("refHdRigButton",label = "<< REF" , p = Colb_R , c = refHdRig ,bgc = (0.411765, 0.411765, 0.411765)) ; 
#     pm.rowColumnLayout (p = Colb_R) ;

#     pm.rowColumnLayout (p = Colb_R) ;
#     pm.text( label='EyeRig File :' , p = Colb_R, al = "left")
#     textEbRig = pm.textField( "txtFieldEyeRig", text = "eyeRig", parent = Colb_R , editable = True )
#     pm.button ("refEyeRigButton",label = "<< REF" , p = Colb_R , c = refEyeRig ,bgc = (0.411765, 0.411765, 0.411765)) ; 
#     pm.rowColumnLayout (p = Colb_R) ;

#     pm.rowColumnLayout (p = Colb_R) ;
#     pm.text( label='EbRig File :' , p = Colb_R, al = "left")
#     textMthRig = pm.textField( "txtFieldEbRig", text = "ebRig", parent = Colb_R , editable = True )
#     pm.button ("refEbRigButton",label = "<< REF" , p = Colb_R , c = refEbRig ,bgc = (0.411765, 0.411765, 0.411765)) ; 
#     pm.rowColumnLayout (p = Colb_R) ;

#     pm.rowColumnLayout (p = Colb_R) ;
#     pm.text( label='MthRig File :' , p = Colb_R, al = "left")
#     textMthRig = pm.textField( "txtFieldMthRig", text = "mthRig", parent = Colb_R , editable = True )
#     pm.button ("refMthRigButton",label = "<< REF" , p = Colb_R , c = refMthRig ,bgc = (0.411765, 0.411765, 0.411765)) ; 
#     pm.rowColumnLayout (p = Colb_R) ;

#     pm.rowColumnLayout (p = Colb_R) ;
#     pm.text( label='TtRig File :' , p = Colb_R, al = "left")
#     textTtRig = pm.textField( "txtFieldTtRig", text = "ttRig", parent = Colb_R , editable = True )
#     pm.button ("refTtRigButton",label = "<< REF" , p = Colb_R , c = refTtRig ,bgc = (0.411765, 0.411765, 0.411765)) ; 
#     pm.rowColumnLayout (p = Colb_R) ;

#     pm.rowColumnLayout (p = Colb_R) ;
#     pm.text( label='NsRig File :' , p = Colb_R, al = "left")
#     textNsRig = pm.textField( "txtFieldNsRig", text = "nsRig", parent = Colb_R , editable = True )
#     pm.button ("refNsRigButton",label = "<< REF" , p = Colb_R , c = refNsRig ,bgc = (0.411765, 0.411765, 0.411765)) ; 
#     pm.rowColumnLayout (p = Colb_R) ;

#     pm.rowColumnLayout (p = Colb_R) ;
#     pm.text( label='EarRig File :' , p = Colb_R, al = "left")
#     textEarRig = pm.textField( "txtFieldEarRig", text = "earRig", parent = Colb_R , editable = True )
#     pm.button ("refEarRigButton",label = "<< REF" , p = Colb_R , c = refEarRig ,bgc = (0.411765, 0.411765, 0.411765)) ; 
#     pm.rowColumnLayout (p = Colb_R) ;

#     pm.rowColumnLayout (p = Colb_R) ;
#     pm.text( label='DtlRig File :' , p = Colb_R, al = "left")
#     textDtlRig = pm.textField( "txtFieldDtlRig", text = "dtlRig", parent = Colb_R , editable = True )
#     pm.button ("refDtlRigButton",label = "<< REF" , p = Colb_R , c = refDtlRig ,bgc = (0.411765, 0.411765, 0.411765)) ; 
#     pm.rowColumnLayout (p = Colb_R) ;    

#     pm.rowColumnLayout (p = Colb_R) ;
#     pm.text( label='BshRig File :' , p = Colb_R, al = "left")
#     textBshRig = pm.textField( "txtFieldBshRig", text = "bsh", parent = Colb_R , editable = True )
#     pm.button ("refBshRigButton",label = "<< REF" , p = Colb_R , c = refBshRig ,bgc = (0.411765, 0.411765, 0.411765)) ; 
#     pm.rowColumnLayout (p = Colb_R) ;    
#     # L--------------------------------------------
#     Colb_L = pm.rowColumnLayout ( nc = 3 , p = mainfacialComb , cw = [(1,10),(2,57),(3,10)]) ;
#     pm.rowColumnLayout (p = Colb_L) ;
#     pm.button ( 'conHdRigButton' , label = 'Parent', p = Colb_L , c = connectHdRig , bgc = (0.411765, 0.411765, 0.411765)) ;
#     pm.rowColumnLayout (p = Colb_L) ;
#     pm.rowColumnLayout (p = Colb_L) ;
#     pm.button ( 'conEyeRigButton' , label = 'Parent', p = Colb_L ,  c = connectEyeRig , bgc = (0.411765, 0.411765, 0.411765)) ;
#     pm.rowColumnLayout (p = Colb_L) ;
#     pm.rowColumnLayout (p = Colb_L) ;
#     pm.button ( 'conEbRigButton' , label = 'Parent', p = Colb_L , c = connectEbRig , bgc = (0.411765, 0.411765, 0.411765)) ;
#     pm.rowColumnLayout (p = Colb_L) ;
#     pm.rowColumnLayout (p = Colb_L) ;
#     pm.button ( 'conMthRigButton' , label = 'Parent', p = Colb_L ,  c = connectMthRig , bgc = (0.411765, 0.411765, 0.411765)) ;
#     pm.rowColumnLayout (p = Colb_L) ;
#     pm.rowColumnLayout (p = Colb_L) ;
#     pm.button ( 'conTtRigButton' , label = 'Parent', p = Colb_L , c = connectTtRig , bgc = (0.411765, 0.411765, 0.411765)) ;
#     pm.rowColumnLayout (p = Colb_L) ;
#     pm.rowColumnLayout (p = Colb_L) ;
#     pm.button ( 'conNsRigButton' , label = 'Parent', p = Colb_L , c = connectNsRig , bgc = (0.411765, 0.411765, 0.411765)) ;
#     pm.rowColumnLayout (p = Colb_L) ;
#     pm.rowColumnLayout (p = Colb_L) ;
#     pm.button ( 'conEarRigButton' , label = 'Parent', p = Colb_L , c = connectEarRig , bgc = (0.411765, 0.411765, 0.411765)) ;
#     pm.rowColumnLayout (p = Colb_L) ;
#     pm.rowColumnLayout (p = Colb_L) ;
#     pm.button ( 'conDtlRigButton' , label = 'Parent', p = Colb_L , c = connectDtlRig , bgc = (0.411765, 0.411765, 0.411765)) ;
#     pm.rowColumnLayout (p = Colb_L) ;
#     pm.rowColumnLayout (p = Colb_L) ;
#     pm.button ( 'conBshRigButton' , label = 'Parent', p = Colb_L , c = connectBshRig , bgc = (0.411765, 0.411765, 0.411765)) ;
#     pm.rowColumnLayout (p = Colb_L) ;

#     reColorColumn= pm.rowColumnLayout ( nc = 1 , p = facialCombRigTab ) ;
#     mc.tabLayout(p=reColorColumn)
#     pm.rowColumnLayout (p = reColorColumn,h =8) ;
#     pm.text( label='# Connect Hd to Fcl' , p = reColorColumn, al = "left")
#     pm.rowColumnLayout (p = reColorColumn ,h=8) ;

#     bshColumn2= pm.rowColumnLayout ( nc = 4 , p = reColorColumn , w = 248 , cw = [(1, 10),(2, 170),(3,58), (4,10)]) ;
#     pm.rowColumnLayout (p = bshColumn2) ;
#     textHdRigGeo = pm.textField( "txtFieldFclRigGeo", text = "...........FclRigGeo_Grp", p = bshColumn2 , editable = True )
#     pm.button ('addFclRigGeo', label = "<< Add" ,h=20, p = bshColumn2 , c = addCreateFclFile, bgc = (0.411765, 0.411765, 0.411765)) ;
#     pm.rowColumnLayout (p = bshColumn2) ;
#     pm.rowColumnLayout (p = bshColumn2) ;
#     textFclRigGeo = pm.textField( "txtFieldHdRigGeo", text = "ref : .....HdRigGeo_Grp", p = bshColumn2 , editable = True )
#     pm.button ('addHdRigGeo', label = "<< Add" ,h=20, p = bshColumn2 , c = addCreateHdFile, bgc = (0.411765, 0.411765, 0.411765)) ;
#     pm.rowColumnLayout (p = bshColumn2) ;

#     bshColumn3= pm.rowColumnLayout ( nc = 3 , p = reColorColumn ,cw =[(1,10), (2, 228),(3,10)]) ;
#     pm.rowColumnLayout (p = bshColumn3) ;
#     pm.button ('createBS2HdAutoButton',label = 'Bsh : Hd 2 Fcl', h = 30 ,  p = bshColumn3 , c = createHd2Fcl , bgc = (0.411765, 0.411765, 0.411765)) ;
#     pm.rowColumnLayout (p = bshColumn3) ;

#     BSHd2FclColumn= pm.rowColumnLayout ( nc = 4 , p = reColorColumn ,cw= [(1,10),(2, 114) , (3, 114),(4,10)]) ;


#     # create BlendShape Hd to Fcl --------------------------------------
#     pm.rowColumnLayout (p = BSHd2FclColumn) ;
#     pm.button ('createBSHd2FclButton',label = 'Bsh : Auto 2 Head', p = BSHd2FclColumn ,  h=20, c = con2HeadFile , bgc = (0.411765, 0.411765, 0.411765)) ;
#     # create BlendShape 2 Head
#     pm.button ('createBS2HdButton',label = 'Bsh : 2 Head', p = BSHd2FclColumn ,  h=20, c = createBs2Head , bgc = (0.411765, 0.411765, 0.411765)) ;
#     pm.rowColumnLayout (p = BSHd2FclColumn ,h=8) ;

#     mc.tabLayout(p=reColorColumn)
#     pm.rowColumnLayout (p = reColorColumn ,h=8) ;
#     pm.text( label='# Create CombRig' , p = reColorColumn, al = "left")
#     pm.rowColumnLayout (p = reColorColumn ,h=8) ;

#     bshColumn4= pm.rowColumnLayout ( nc = 4 , p = reColorColumn ,cw= [(1,10),(2, 114) , (3, 114),(4,10)]) ;
#     pm.rowColumnLayout (p = bshColumn4) ;
#     pm.button ('addBSHButton',label = 'Add Bsh', p = bshColumn4 ,  h=20, c = doAddBSH , bgc = (0.411765, 0.411765, 0.411765)) ;
#     pm.button ('assemBlerButton', label = 'Fcl AssemBler', p = bshColumn4, h=20, c = assemBler, bgc = (0.411765, 0.411765, 0.411765)) ;
#     pm.rowColumnLayout (p = bshColumn4) ;

#     pm.rowColumnLayout (p = bshColumn4) ;
#     pm.button ('sqHairButton', label = 'SQ hair' ,  h=20, p = bshColumn4 , c = sqHairPaScaConstraintFile, bgc = (0.411765, 0.411765, 0.411765)) ;
#     pm.button ('addFurGuideButton', label = 'Add FurGuide' ,  h=20, p = bshColumn4 , c = addFurGuideFile, bgc = (0.411765, 0.411765, 0.411765)) ;
#     pm.rowColumnLayout (p = bshColumn4) ;



#     # tab re-checkStatus -----------------------------------------------
#     mc.tabLayout(p=reColorColumn)
#     pm.rowColumnLayout (p = reColorColumn ,h=8) ;
#     pm.text( label='# Re-Check Status' , p = facialCombRigTab, al = "left")
#     pm.rowColumnLayout (p = facialCombRigTab ,h=8) ;
#     # reColor Re-Status --------------------------------------
#     ResetLayout = pm.rowColumnLayout ( nc = 3 , p = facialCombRigTab , w = 220, cw=[(1,10),(2,228),(3,10)]) ;
#     pm.rowColumnLayout (p = ResetLayout ) ;
#     pm.symbolCheckBox (image = ("%s%s" % (pngImags(),"Reset.png")), p = ResetLayout , h=25, onc = reStatusFc , bgc = (0.411765, 0.411765, 0.411765)) ;
#     pm.rowColumnLayout (p = ResetLayout) ;

#     # line TabB ----------------------------------------------
#     rowLine_TabB = pm.rowColumnLayout ( nc = 1 , p = facialCombRigTab , h = 10 ) ;
#     mc.tabLayout(p=rowLine_TabB, w= 248)








# facial CombRig Tab -------------------------
# facial CombRig Tab -------------------------
    facialCombRigTab = pm.rowColumnLayout ( "facial", nc = 1 , p = mainTab_A ) ;
    mainfacialCombRigTab1 = pm.rowColumnLayout (p = facialCombRigTab , w = 300) ;
    mc.tabLayout(p=mainfacialCombRigTab1, w= 248)

    # mc.tabLayout(p=mainfacialCombRigTab2, w= 248)
    pm.rowColumnLayout (p = mainfacialCombRigTab1 ,h=8) ;
    pm.text( label='# Create MainGroup' , p = mainfacialCombRigTab1, al = "left")
    pm.rowColumnLayout (p = mainfacialCombRigTab1 ,h=8) ;

    mainfacialCombRigTab2 = pm.rowColumnLayout ( nc = 3 , p = mainfacialCombRigTab1 , w = 248 , cw = [(1, 10),(2, 228), (3,10)]) ;
    # create MainGroup
    pm.rowColumnLayout (p = mainfacialCombRigTab2 , h=20 ) ;
    pm.button ("cteateMainGroupButton",label = "Create MainGroup", h=20, p = mainfacialCombRigTab2 , c = mainGroup, bgc = (0.411765, 0.411765, 0.411765)) ;
    pm.rowColumnLayout (p = mainfacialCombRigTab2 , h=20 ) ;

    # duplicate "BodyFclRigPos_Geo"
    mainfacialCombRigTab = pm.rowColumnLayout ( nc = 4 , p = mainfacialCombRigTab1 , w = 248 , cw = [(1, 10),(2, 170),(3,58), (4,10)]) ;
    pm.rowColumnLayout (p = mainfacialCombRigTab ) ;
    pm.textField( "txtFieldBodyFclRigPos", text = "BodyFclRig_Geo", p = mainfacialCombRigTab , editable = True )
    pm.button ('addBodyFclRigPos', label = "<< Add" ,h=20, p = mainfacialCombRigTab , c = addDupHeadFclRigFile, bgc = (0.411765, 0.411765, 0.411765)) ;
    pm.rowColumnLayout (p = mainfacialCombRigTab ) ;

    mainfacialCombRigTab3 = pm.rowColumnLayout ( nc = 3 , p = mainfacialCombRigTab1 , w = 248 , cw = [(1, 10),(2, 228), (3,10)]) ;
    pm.rowColumnLayout (p = mainfacialCombRigTab3 ) ;
    pm.button ("CreateBodyFclRigPos_GeoButton",label = "Create ( BodyFclRigPos_Geo )" ,h=20, p = mainfacialCombRigTab3 , c = dupHeadFclRigFile, bgc = (0.411765, 0.411765, 0.411765)) ;
    pm.rowColumnLayout (p = mainfacialCombRigTab3) ;

    tabCRF = pm.rowColumnLayout (p = facialCombRigTab , w = 300) ;
    mc.tabLayout(p=tabCRF, w= 248)
    pm.rowColumnLayout (p = tabCRF ,h=8) ;
    pm.text( label='# Create Reference FacialRig' , p = tabCRF, al = "left")
    pm.rowColumnLayout (p = tabCRF ,h=8) ;

    # R--------------------------------------------
    mainfacialComb = pm.rowColumnLayout ( nc = 2 , p = tabCRF , cw = [ ( 1 , 170 ) , ( 2 , 77 )]) ;
    # Colb_R
    Colb_R = pm.rowColumnLayout ( nc = 5 , p = mainfacialComb , cw = [ ( 1 , 10 ) , ( 2 , 60 ) , ( 3 , 50 ) , ( 4 , 50 ) , ( 5 , 10 )] ) ;
    
    # pm.rowColumnLayout (p = Colb_R) ;
    # pm.text( label='HdRig File :' , p = Colb_R, al = "left")
    # textEyeRig = pm.textField( "txtFieldHdRig", text = "hdRig", parent = Colb_R , editable = True )
    # pm.button ("refHdRigButton",label = "<< REF" , p = Colb_R , c = refHdRig ,bgc = (0.411765, 0.411765, 0.411765)) ; 
    # pm.rowColumnLayout (p = Colb_R) ;

    # pm.rowColumnLayout (p = Colb_R) ;
    # pm.text( label='EyeRig File :' , p = Colb_R, al = "left")
    # textEbRig = pm.textField( "txtFieldEyeRig", text = "eyeRig", parent = Colb_R , editable = True )
    # pm.button ("refEyeRigButton",label = "<< REF" , p = Colb_R , c = refEyeRig ,bgc = (0.411765, 0.411765, 0.411765)) ; 
    # pm.rowColumnLayout (p = Colb_R) ;

    # pm.rowColumnLayout (p = Colb_R) ;
    # pm.text( label='EbRig File :' , p = Colb_R, al = "left")
    # textMthRig = pm.textField( "txtFieldEbRig", text = "ebRig", parent = Colb_R , editable = True )
    # pm.button ("refEbRigButton",label = "<< REF" , p = Colb_R , c = refEbRig ,bgc = (0.411765, 0.411765, 0.411765)) ; 
    # pm.rowColumnLayout (p = Colb_R) ;

    # pm.rowColumnLayout (p = Colb_R) ;
    # pm.text( label='MthRig File :' , p = Colb_R, al = "left")
    # textMthRig = pm.textField( "txtFieldMthRig", text = "mthRig", parent = Colb_R , editable = True )
    # pm.button ("refMthRigButton",label = "<< REF" , p = Colb_R , c = refMthRig ,bgc = (0.411765, 0.411765, 0.411765)) ; 
    # pm.rowColumnLayout (p = Colb_R) ;

    pm.rowColumnLayout (p = Colb_R) ;
    pm.text( label='TtRig File :' , p = Colb_R, al = "left")
    textTtRig = pm.textField( "txtField7TtRig", text = "ttRig", parent = Colb_R , editable = True )
    pm.button ("refTt7RigButton",label = "<< REF" , p = Colb_R , c = refTtRig ,bgc = (0.411765, 0.411765, 0.411765)) ; 
    pm.rowColumnLayout (p = Colb_R) ;

    pm.rowColumnLayout (p = Colb_R) ;
    pm.text( label='DtlRig File :' , p = Colb_R, al = "left")
    textDtlRig = pm.textField( "txtField7DtlRig", text = "dtlRig", parent = Colb_R , editable = True )
    pm.button ("refDtl7RigButton",label = "<< REF" , p = Colb_R , c = refDtlRig ,bgc = (0.411765, 0.411765, 0.411765)) ; 
    pm.rowColumnLayout (p = Colb_R) ;    

    pm.rowColumnLayout (p = Colb_R) ;
    pm.text( label='MthRig File :' , p = Colb_R, al = "left")
    textDtlRig = pm.textField( "txtField7MthRig", text = "mthRig", parent = Colb_R , editable = True )
    pm.button ("refMth7RigButton",label = "<< REF" , p = Colb_R , c = refMthRig ,bgc = (0.411765, 0.411765, 0.411765)) ; 
    pm.rowColumnLayout (p = Colb_R) ;   

    pm.rowColumnLayout (p = Colb_R) ;
    pm.text( label='BshRig File :' , p = Colb_R, al = "left")
    textBshRig = pm.textField( "txtField7BshRig", text = "bsh", parent = Colb_R , editable = True )
    pm.button ("refBsh7RigButton",label = "<< REF" , p = Colb_R , c = refBshRig ,bgc = (0.411765, 0.411765, 0.411765)) ; 
    pm.rowColumnLayout (p = Colb_R) ;  

    pm.rowColumnLayout (p = Colb_R) ;
    pm.text( label='HdDfmRig File :' , p = Colb_R, al = "left")
    textHdDfmRig = pm.textField( "txtField7HdDfmRig", text = "hdRig", parent = Colb_R , editable = True )
    pm.button ("refHd7RigButton",label = "<< REF" , p = Colb_R , c = refHdRig ,bgc = (0.411765, 0.411765, 0.411765)) ; 
    pm.rowColumnLayout (p = Colb_R) ;    

    pm.rowColumnLayout (p = Colb_R) ;
    pm.text( label='WrinkledRig File :' , p = Colb_R, al = "left")
    textHdDfmRig = pm.textField( "txtField7WrinkledRig", text = "wrinkledRig", parent = Colb_R , editable = True )
    pm.button ("refWrinkled7RigButton",label = "<< REF" , p = Colb_R , c = refWrinkledRig ,bgc = (0.411765, 0.411765, 0.411765)) ; 
    pm.rowColumnLayout (p = Colb_R) ;  
    # L--------------------------------------------
    Colb_L = pm.rowColumnLayout ( nc = 3 , p = mainfacialComb , cw = [(1,10),(2,57),(3,10)]) ;
    pm.rowColumnLayout (p = Colb_L) ;
    pm.button ( 'con7TtRigButton' , label = 'Parent', p = Colb_L , c = connectTtRig , bgc = (0.411765, 0.411765, 0.411765)) ;
    pm.rowColumnLayout (p = Colb_L) ;
    pm.rowColumnLayout (p = Colb_L) ;
    pm.button ( 'con7DtlRigButton' , label = 'Parent', p = Colb_L , c = connectDtlRig , bgc = (0.411765, 0.411765, 0.411765)) ;
    pm.rowColumnLayout (p = Colb_L) ;
    pm.rowColumnLayout (p = Colb_L) ;
    pm.button ( 'con7MthRigButton' , label = 'Parent', p = Colb_L , c = connectMthRig , bgc = (0.411765, 0.411765, 0.411765)) ;
    pm.rowColumnLayout (p = Colb_L) ;
    pm.rowColumnLayout (p = Colb_L) ;
    pm.button ( 'con7BshRigButton' , label = 'Parent', p = Colb_L , c = connectBshRig , bgc = (0.411765, 0.411765, 0.411765)) ;
    pm.rowColumnLayout (p = Colb_L) ;
    pm.rowColumnLayout (p = Colb_L) ;
    pm.button ( 'con7HdRigButton' , label = 'Parent', p = Colb_L , c = connectHdRig , bgc = (0.411765, 0.411765, 0.411765)) ;
    pm.rowColumnLayout (p = Colb_L) ;
    pm.rowColumnLayout (p = Colb_L) ;
    pm.button ( 'con7WrinkledRigButton' , label = 'Parent', p = Colb_L , c = connectWrinkledRigAuto , bgc = (0.411765, 0.411765, 0.411765)) ;
    pm.rowColumnLayout (p = Colb_L) ;

    # reColorColumn= pm.rowColumnLayout ( nc = 1 , p = facialCombRigTab ) ;
    # mc.tabLayout(p=reColorColumn)
    # pm.rowColumnLayout (p = reColorColumn,h =8) ;
    # pm.text( label='# Connect Hd to Fcl' , p = reColorColumn, al = "left")
    # pm.rowColumnLayout (p = reColorColumn ,h=8) ;

    # bshColumn2= pm.rowColumnLayout ( nc = 4 , p = reColorColumn , w = 248 , cw = [(1, 10),(2, 170),(3,58), (4,10)]) ;

    # bshColumn3= pm.rowColumnLayout ( nc = 3 , p = reColorColumn ,cw =[(1,10), (2, 228),(3,10)]) ;
    # pm.rowColumnLayout (p = bshColumn3) ;
    # pm.button ('createBS2HdAutoButton',label = 'Bsh : Hd 2 Fcl', h = 30 ,  p = bshColumn3 , c = createHd2Fcl , bgc = (0.411765, 0.411765, 0.411765)) ;
    # pm.rowColumnLayout (p = bshColumn3) ;

    # BSHd2FclColumn= pm.rowColumnLayout ( nc = 5 , p = reColorColumn ,cw= [(1,10),(2, 113), (3, 2) , (4, 113),(5,10)]) ;
    # # create BlendShape Hd to Fcl --------------------------------------
    # pm.rowColumnLayout (p = BSHd2FclColumn) ;
    # pm.button ('createBSHd2FclButton',label = 'Bsh : Auto 2 Head', p = BSHd2FclColumn ,  h=20, c = con2HeadFile , bgc = (0.411765, 0.411765, 0.411765)) ;
    # pm.rowColumnLayout (p = BSHd2FclColumn) ;
    # pm.button ('createBS2HdButton',label = 'Bsh : 2 Head', p = BSHd2FclColumn ,  h=20, c = createBs2Head , bgc = (0.411765, 0.411765, 0.411765)) ;
    # pm.rowColumnLayout (p = BSHd2FclColumn ,h=8) ;

    # mc.tabLayout(p=reColorColumn)
    # pm.rowColumnLayout (p = reColorColumn ,h=8) ;
    # pm.text( label='# Create CombRig' , p = reColorColumn, al = "left")
    # pm.rowColumnLayout (p = reColorColumn ,h=8) ;

    # bshColumn4= pm.rowColumnLayout ( nc = 5 , p = reColorColumn ,cw= [(1,10),(2, 113) ,(3,2),  (4, 113),(5,10)]) ;
    # pm.rowColumnLayout (p = bshColumn4) ;
    # pm.button ('addBSHButton',label = 'Add Bsh', p = bshColumn4 ,  h=20, c = doAddBSH , bgc = (0.411765, 0.411765, 0.411765)) ;
    # pm.rowColumnLayout (p = bshColumn4) ;
    # pm.button ('assemBlerButton', label = 'Fcl AssemBler', p = bshColumn4, h=20, c = assemBler, bgc = (0.411765, 0.411765, 0.411765)) ;
    # pm.rowColumnLayout (p = bshColumn4) ;

    # bshColumn5= pm.rowColumnLayout ( nc = 5 , p = reColorColumn ,cw= [(1,10),(2, 113) ,(3,2),  (4, 113),(5,10)]) ;
    # pm.rowColumnLayout (p = bshColumn5) ;
    # pm.button ('sqHairButton', label = 'SQ hair' ,  h=20, p = bshColumn5 , c = sqHairPaScaConstraintFile, bgc = (0.411765, 0.411765, 0.411765)) ;
    # # pm.rowColumnLayout (p = bshColumn5) ;

    # pm.rowColumnLayout (p = bshColumn5) ;
    # pm.button ('connectLipsSealButton', label = 'Attr lipsSeal (Bsh)' ,  h=20, p = bshColumn5 , c = connectMthLipsSealFile, bgc = (0.411765, 0.411765, 0.411765)) ;
    # pm.rowColumnLayout (p = bshColumn5) ;



    # mc.tabLayout(p=reColorColumn)
    # pm.rowColumnLayout (p = reColorColumn ,h=8) ;
    # pm.text( label='# Connect Bsh After AssemBler ( MainRig )' , p = reColorColumn, al = "left")
    # pm.rowColumnLayout (p = reColorColumn ,h=8) ;

    # bshColumn4= pm.rowColumnLayout ( nc = 5 , p = reColorColumn ,cw= [(1,10),(2, 113) ,(3,2),  (4, 113),(5,10)]) ;

    # pm.rowColumnLayout (p = bshColumn4) ;
    # pm.button ('addEyelidUpDnButton', label = 'Eyelid Up - Dn', p = bshColumn4, h=20, c = doEyelidUpDn, bgc = (0.411765, 0.411765, 0.411765)) ;
    # pm.rowColumnLayout (p = bshColumn4) ;
    # pm.button ('addJawMouthSqStButton',label = 'Jaw Sq - St', p = bshColumn4 ,  h=20, c = doJawSqSt , bgc = (0.411765, 0.411765, 0.411765)) ;
    # pm.rowColumnLayout (p = bshColumn4) ;


    # bshColumn5= pm.rowColumnLayout ( nc = 5 , p = reColorColumn ,cw= [(1,10),(2, 113) ,(3,2),  (4, 113),(5,10)]) ;
    # pm.rowColumnLayout (p = bshColumn5) ;
    # pm.button ('addEyelidFollowButton', label = 'Eyelid Follow' ,  h=20, p = bshColumn5 , c = doEyeLidFollow, bgc = (0.411765, 0.411765, 0.411765)) ;
    # pm.rowColumnLayout (p = bshColumn5) ;
    # pm.button ('addLatticeForeyeButton', label = 'Add Lattice For Eye' ,  h=20, p = bshColumn5 , c = doLatticeForEye, bgc = (0.411765, 0.411765, 0.411765)) ;
    # pm.rowColumnLayout (p = bshColumn5) ;

    # bshColumn6= pm.rowColumnLayout ( nc = 5 , p = reColorColumn ,cw= [(1,10),(2, 113) ,(3,2),  (4, 113),(5,10)]) ;
    # pm.rowColumnLayout (p = bshColumn6) ;
    # pm.button ('addAttrForLipsealLRButton', label = 'Attr LipsSeal L R' ,  h=20, p = bshColumn6 , c = doAttrForLipsealLR, bgc = (0.411765, 0.411765, 0.411765)) ;
    # pm.rowColumnLayout (p = bshColumn6) ;
    # pm.button ('addTeethUpLowVisButton', label = 'TeethUpLow Vis' ,  h=20, p = bshColumn6 , c = doTeethUpLowVis, bgc = (0.411765, 0.411765, 0.411765)) ;
    # pm.rowColumnLayout (p = bshColumn6) ;

    # bshColumn7= pm.rowColumnLayout ( nc = 5 , p = reColorColumn ,cw= [(1,10),(2, 113) ,(3,2),  (4, 113),(5,10)]) ;
    # pm.rowColumnLayout (p = bshColumn7) ;
    # pm.button ('addTtRigConnectScaleButton', label = 'TtRig ConnectScale' ,  h=20, p = bshColumn7 , c = doTtRigConnectScale, bgc = (0.411765, 0.411765, 0.411765)) ;
    # pm.rowColumnLayout (p = bshColumn7) ;
    # pm.button ('addFixWrinkledRigButton', label = 'Fix WrinkledRig' , ann = 'Select Geo then press a button' , h=20, p = bshColumn7 , c = fixWrinkleRig, bgc = (0.411765, 0.411765, 0.411765)) ;
    # pm.rowColumnLayout (p = bshColumn7) ;

    # mc.tabLayout(p=reColorColumn)
    # pm.rowColumnLayout (p = reColorColumn,h =8) ;
    # pm.text( label='# Connect Add FurGuide' , p = reColorColumn, al = "left")
    # pm.rowColumnLayout (p = reColorColumn ,h=8) ;


    # rowColumnGeoBase1 = pm.rowColumnLayout ( nc = 6 , p = reColorColumn , w = 250 , cw = [(1,10),(2 , 85), ( 3 , 113 ) ,(4,5), ( 5 , 25 ),(6,10)] ) ;

    # pm.rowColumnLayout (p = rowColumnGeoBase1) ;
    # pm.text( label='Body_Geo' , p = rowColumnGeoBase1, al = "left")
    # textEyeRig = pm.textField( "txtFieldInputBody_Geo", text = "....:Body_Geo", p = rowColumnGeoBase1 , editable = True )
    # pm.rowColumnLayout (p = rowColumnGeoBase1) ;
    # pm.button ("addInputBody_Geo",label = "<<" , p = rowColumnGeoBase1 , c = addTextInputBody_Geo ,bgc = (0.411765, 0.411765, 0.411765)) ; 
    # pm.rowColumnLayout (p = rowColumnGeoBase1) ;

    # pm.rowColumnLayout (p = rowColumnGeoBase1) ;
    # pm.text( label='BodyFur_Geo ' , p = rowColumnGeoBase1, al = "left")
    # textBshRig1 = pm.textField( "txtFieldInputBodyFur_Geo", text = "....:BodyFur_Geo", p = rowColumnGeoBase1 , editable = True )
    # pm.rowColumnLayout (p = rowColumnGeoBase1) ;
    # pm.button ("addInputBodyFur_Geo",label = "<<" , p = rowColumnGeoBase1 , c = addTextInputBodyFur_Geo ,bgc = (0.411765, 0.411765, 0.411765)) ; 
    # pm.rowColumnLayout (p = rowColumnGeoBase1) ;

    # pm.rowColumnLayout (p = rowColumnGeoBase1) ;
    # pm.text( label='BodyFcl..Wrap' , p = rowColumnGeoBase1, al = "left")
    # textBshRig1 = pm.textField( "txtFieldBodyFclRigWrapped_Geo", text = "BodyFcl..Wrap..Geo", p = rowColumnGeoBase1 , editable = True )
    # pm.rowColumnLayout (p = rowColumnGeoBase1) ;
    # pm.button ("addBodyFclRigWrapped_Geo",label = "<<" , p = rowColumnGeoBase1 , c = addTextBodyFclRigWrapped_Geo ,bgc = (0.411765, 0.411765, 0.411765)) ; 
    # pm.rowColumnLayout (p = rowColumnGeoBase1) ;

    # bshColumn6= pm.rowColumnLayout ( nc = 3 , p = reColorColumn ,cw =[(1,10), (2, 228),(3,10)]) ;
    # pm.rowColumnLayout (p = bshColumn6) ;
    # pm.button ('addFurGuideButton', label = 'Add FurGuide' ,  h=40, p = bshColumn6 , c = addFurGuideFile, bgc = (0.411765, 0.411765, 0.411765)) ;
    # pm.rowColumnLayout (p = bshColumn6) ;











    # # tab re-checkStatus -----------------------------------------------
    # mc.tabLayout(p=reColorColumn)
    # pm.rowColumnLayout (p = reColorColumn ,h=8) ;
    # pm.text( label='# Re-Check Status' , p = facialCombRigTab, al = "left")
    # pm.rowColumnLayout (p = facialCombRigTab ,h=8) ;
    # # reColor Re-Status --------------------------------------
    # ResetLayout = pm.rowColumnLayout ( nc = 3 , p = facialCombRigTab , w = 220, cw=[(1,10),(2,228),(3,10)]) ;
    # pm.rowColumnLayout (p = ResetLayout ) ;
    # pm.symbolCheckBox (image = ("%s%s" % (pngImags(),"Reset.png")), p = ResetLayout , h=25, onc = reStatusFc , bgc = (0.411765, 0.411765, 0.411765)) ;
    # pm.rowColumnLayout (p = ResetLayout) ;

    # # line TabB ----------------------------------------------
    # rowLine_TabB = pm.rowColumnLayout ( nc = 1 , p = facialCombRigTab , h = 10 ) ;
    # mc.tabLayout(p=rowLine_TabB, w= 248)






# # facial joint direct tap
#     jointDirectTab = pm.rowColumnLayout ("FclDir", nc = 1, p = mainTab_A ) ;
#     jointDirectColumn = pm.columnLayout("jointDirectColumn", w=300, cal="center", p=jointDirectTab)

#     pm.separator(style="in", w=250, h=10, p=jointDirectColumn)
#     pm.text(l="# Create Mouth Joints Direct", w=300, al="left", p=jointDirectColumn)
#     pm.separator(style="none", w=250, h=10, p=jointDirectColumn)

#     #########################################################################################################################################

#     # free space
#     pm.rowColumnLayout (p = jointDirectColumn ,h=8) ;

#     templateLayout = pm.rowColumnLayout ( nc = 3 , p = jointDirectColumn , w = 248, cw = [(1, 8), (2,232),(3,8)]) ;
#     pm.rowColumnLayout (p = templateLayout ) ;
#     pm.button ("FacialDirButton",label = "Facial Direc Tmp" , h=30, p = templateLayout , c = templateFacialDirTmpJntFile ,bgc = (0.411765, 0.411765, 0.411765)) ; 
#     pm.rowColumnLayout (p = templateLayout ) ;




#     ### define variable
#     # eye upper check box left
#     pm.separator(style="in", w=250, h=10, p=jointDirectColumn)
#     jointDirectEyeLCheckBoxColumn = pm.rowLayout("jointDirectEyeLCheckBoxColumn", nc=2, p=jointDirectColumn)
#     pm.text(l="  eyeRigL", w=110, al="left", p=jointDirectEyeLCheckBoxColumn)
#     eyeRigLJointDirectCheckBox = pm.checkBox("eyeRigLJointDirectCheckBox", l="", v=1, p=jointDirectEyeLCheckBoxColumn)

#     jointDirectEyeLVariableColumn = pm.rowColumnLayout("jointDirectEyeLVariableColumn", nc=4, p=jointDirectColumn)

#     # eye upper left
#     pm.text(l="  eyeLidUpr", w=110, al="left", p=jointDirectEyeLVariableColumn)
#     eyeUprLTmpJntField = pm.textField("eyeUprLTmpJntField", tx="EyeLidUpr1_L_TmpJnt, EyeLidUpr2_L_TmpJnt, EyeLidUpr3_L_TmpJnt, EyeLidUpr4_L_TmpJnt, EyeLidUpr5_L_TmpJnt, EyeLidUpr6_L_TmpJnt, EyeLidUpr7_L_TmpJnt, EyeLidUpr8_L_TmpJnt, EyeLidUpr9_L_TmpJnt, EyeLidUpr10_L_TmpJnt", p=jointDirectEyeLVariableColumn)
#     pm.separator(style="none", w=5, p=jointDirectEyeLVariableColumn)
#     eyeUprLTmpJntButt = pm.button("eyeUprLTmpJntButt", l="<<", w=30, p=jointDirectEyeLVariableColumn)
#     pm.button(eyeUprLTmpJntButt, e=True, c=partial(addAllSelectionObject, eyeUprLTmpJntField, eyeUprLTmpJntButt))

#     # eye lower left
#     pm.text(l="  eyeLidLwr", w=110, al="left", p=jointDirectEyeLVariableColumn)
#     eyeLwrLTmpJntField = pm.textField("eyeLwrLTmpJntField", tx="EyeLidLwr1_L_TmpJnt, EyeLidLwr2_L_TmpJnt, EyeLidLwr3_L_TmpJnt, EyeLidLwr4_L_TmpJnt, EyeLidLwr5_L_TmpJnt, EyeLidLwr6_L_TmpJnt, EyeLidLwr7_L_TmpJnt, EyeLidLwr8_L_TmpJnt, EyeLidLwr9_L_TmpJnt, EyeLidLwr10_L_TmpJnt", p=jointDirectEyeLVariableColumn)
#     pm.separator(style="none", w=5, p=jointDirectEyeLVariableColumn)
#     eyeLwrLTmpJntButt = pm.button("eyeLwrLTmpJntButt", l="<<", w=30, p=jointDirectEyeLVariableColumn)
#     pm.button(eyeLwrLTmpJntButt, e=True, c=partial(addAllSelectionObject, eyeLwrLTmpJntField, eyeLwrLTmpJntButt))

#     # eye side color left
#     jointDirectEyeLSideColorColumn = pm.rowLayout("jointDirectEyeLSideColorColumn", nc=6, p=jointDirectColumn)
#     pm.text("  side", w=40, al="left", p=jointDirectEyeLSideColorColumn)
#     eyeLSideField = pm.textField("eyeLSideField", w=30, tx="L", p=jointDirectEyeLSideColorColumn)
#     pm.separator(style="none", w=30, p=jointDirectEyeLSideColorColumn)
#     pm.text("  color", w=40, al="left", p=jointDirectEyeLSideColorColumn)
#     eyeLColorField = pm.textField("eyeLColorField", w=55, tx="red", p=jointDirectEyeLSideColorColumn)

#     pm.checkBox(eyeRigLJointDirectCheckBox, e=True, cc=partial(disableFieldByCheckBox, eyeRigLJointDirectCheckBox, [eyeUprLTmpJntField, eyeLwrLTmpJntField, eyeLSideField, eyeLColorField]))

#     # eye upper check box right
#     pm.separator(style="in", w=250, h=10, p=jointDirectColumn)
#     jointDirectEyeRCheckBoxColumn = pm.rowLayout("jointDirectEyeRCheckBoxColumn", nc=2, p=jointDirectColumn)
#     pm.text(l="  eyeRigR", w=110, al="left", p=jointDirectEyeRCheckBoxColumn)
#     eyeRigRJointDirectCheckBox = pm.checkBox("eyeRigRJointDirectCheckBox", l="", v=1, p=jointDirectEyeRCheckBoxColumn)

#     jointDirectEyeRVariableColumn = pm.rowColumnLayout("jointDirectEyeRVariableColumn", nc=4, p=jointDirectColumn)

#     # eye upper right
#     pm.text(l="  eyeLidUpr", w=110, al="left", p=jointDirectEyeRVariableColumn)
#     eyeUprRTmpJntField = pm.textField("eyeUprRTmpJntField", tx="EyeLidUpr1_R_TmpJnt, EyeLidUpr2_R_TmpJnt, EyeLidUpr3_R_TmpJnt, EyeLidUpr4_R_TmpJnt, EyeLidUpr5_R_TmpJnt, EyeLidUpr6_R_TmpJnt, EyeLidUpr7_R_TmpJnt, EyeLidUpr8_R_TmpJnt, EyeLidUpr9_R_TmpJnt, EyeLidUpr10_R_TmpJnt", p=jointDirectEyeRVariableColumn)
#     pm.separator(style="none", w=5, p=jointDirectEyeRVariableColumn)
#     eyeUprRTmpJntButt = pm.button("eyeUprRTmpJntButt", l="<<", w=30, p=jointDirectEyeRVariableColumn)
#     pm.button(eyeUprRTmpJntButt, e=True, c=partial(addAllSelectionObject, eyeUprRTmpJntField, eyeUprRTmpJntButt))

#     # eye lower right
#     pm.text(l="  eyeLidLwr", w=110, al="left", p=jointDirectEyeRVariableColumn)
#     eyeLwrRTmpJntField = pm.textField("eyeLwrRTmpJntField", tx="EyeLidLwr1_R_TmpJnt, EyeLidLwr2_R_TmpJnt, EyeLidLwr3_R_TmpJnt, EyeLidLwr4_R_TmpJnt, EyeLidLwr5_R_TmpJnt, EyeLidLwr6_R_TmpJnt, EyeLidLwr7_R_TmpJnt, EyeLidLwr8_R_TmpJnt, EyeLidLwr9_R_TmpJnt, EyeLidLwr10_R_TmpJnt", p=jointDirectEyeRVariableColumn)
#     pm.separator(style="none", w=5, p=jointDirectEyeRVariableColumn)
#     eyeLwrRTmpJntButt = pm.button("eyeLwrRTmpJntButt", l="<<", w=30, p=jointDirectEyeRVariableColumn)
#     pm.button(eyeLwrRTmpJntButt, e=True, c=partial(addAllSelectionObject, eyeLwrRTmpJntField, eyeLwrRTmpJntButt))

#     # eye side color right
#     jointDirectEyeRSideColorColumn = pm.rowLayout("jointDirectEyeRSideColorColumn", nc=6, p=jointDirectColumn)
#     pm.text("  side", w=40, al="left", p=jointDirectEyeRSideColorColumn)
#     eyeRSideField = pm.textField("eyeRSideField", w=30, tx="R", p=jointDirectEyeRSideColorColumn)
#     pm.separator(style="none", w=30, p=jointDirectEyeRSideColorColumn)
#     pm.text("  color", w=40, al="left", p=jointDirectEyeRSideColorColumn)
#     eyeRColorField = pm.textField("eyeRColorField", w=55, tx="blue", p=jointDirectEyeRSideColorColumn)

#     pm.checkBox(eyeRigRJointDirectCheckBox, e=True, cc=partial(disableFieldByCheckBox, eyeRigRJointDirectCheckBox, [eyeUprRTmpJntField, eyeLwrRTmpJntField, eyeRSideField, eyeRColorField]))

#     #########################################################################################################################################

#     ### define variable
#     pm.separator(style="none", w=250, h=5, p=jointDirectColumn)
#     jointDirectVariableFrame = pm.frameLayout("Lip Advance Setting", w=250, cl=True, cll=True, p=jointDirectColumn)

#     ## lip variable column
#     jointDirectLipVariableColumn = pm.rowColumnLayout("jointDirectLipVariableColumn", nc=4, p=jointDirectVariableFrame)

#     # main lip center
#     pm.text(l="  mainLipCenJnt", w=110, al="left", p=jointDirectLipVariableColumn)
#     mainLipCenTmpJntField = pm.textField("mainLipCenTmpJntField", tx="lipMainUp_TmpJnt, lipMainDn_TmpJnt", en=False, p=jointDirectLipVariableColumn)
#     pm.separator(style="none", w=5, p=jointDirectLipVariableColumn)
#     mainLipCenTmpJntButt = pm.button("mainLipCenTmpJntButt", l="<<", w=30, p=jointDirectLipVariableColumn)
#     pm.button(mainLipCenTmpJntButt, e=True, c=partial(addAllSelectionObject, mainLipCenTmpJntField, mainLipCenTmpJntButt))

#     # lip corner
#     pm.text(l="  lipCnrJnt", w=110, al="left", p=jointDirectLipVariableColumn)
#     lipCnrTmpJntField = pm.textField("lipCnrTmpJntField", tx="lipCnr_L_TmpJnt, lipCnr_R_TmpJnt", en=False, p=jointDirectLipVariableColumn)
#     pm.separator(style="none", w=5, p=jointDirectLipVariableColumn)
#     lipCnrTmpJntButt = pm.button("lipCnrTmpJntButt", l="<<", w=30, p=jointDirectLipVariableColumn)
#     pm.button(lipCnrTmpJntButt, e=True, c=partial(addAllSelectionObject, lipCnrTmpJntField, lipCnrTmpJntButt))

#     # lip up center
#     pm.text(l="  lipUpCenJnt", w=110, al="left", p=jointDirectLipVariableColumn)
#     lipUpCenTmpJntField = pm.textField("lipUpCenTmpJntField", tx="lipUp_TmpJnt", en=False, p=jointDirectLipVariableColumn)
#     pm.separator(style="none", w=5, p=jointDirectLipVariableColumn)
#     lipUpCenTmpJntButt = pm.button("lipUpCenTmpJntButt", l="<<", w=30, p=jointDirectLipVariableColumn)
#     pm.button(lipUpCenTmpJntButt, e=True, c=partial(addAllSelectionObject, lipUpCenTmpJntField, lipUpCenTmpJntButt))

#     # lip up left
#     pm.text(l="  lipUpLJnt", w=110, al="left", p=jointDirectLipVariableColumn)
#     lipUpLTmpJntField = pm.textField("lipUpLTmpJntField", tx="lipUp1_L_TmpJnt, lipUp2_L_TmpJnt", en=False, p=jointDirectLipVariableColumn)
#     pm.separator(style="none", w=5, p=jointDirectLipVariableColumn)
#     lipUpLTmpJntButt = pm.button("lipUpLTmpJntButt", l="<<", w=30, p=jointDirectLipVariableColumn)
#     pm.button(lipUpLTmpJntButt, e=True, c=partial(addAllSelectionObject, lipUpLTmpJntField, lipUpLTmpJntButt))

#     # lip up right
#     pm.text(l="  lipUpRJnt", w=110, al="left", p=jointDirectLipVariableColumn)
#     lipUpRTmpJntField = pm.textField("lipUpRTmpJntField", tx="lipUp1_R_TmpJnt, lipUp2_R_TmpJnt", en=False, p=jointDirectLipVariableColumn)
#     pm.separator(style="none", w=5, p=jointDirectLipVariableColumn)
#     lipUpRTmpJntButt = pm.button("lipUpRTmpJntButt", l="<<", w=30, p=jointDirectLipVariableColumn)
#     pm.button(lipUpRTmpJntButt, e=True, c=partial(addAllSelectionObject, lipUpRTmpJntField, lipUpRTmpJntButt))

#     # lip down center
#     pm.text(l="  lipDnCenJnt", w=110, al="left", p=jointDirectLipVariableColumn)
#     lipDnCenTmpJntField = pm.textField("lipDnCenTmpJntField", tx="lipDn_TmpJnt", en=False, p=jointDirectLipVariableColumn)
#     pm.separator(style="none", w=5, p=jointDirectLipVariableColumn)
#     lipDnCenTmpJntButt = pm.button("lipDnCenTmpJntButt", l="<<", w=30, p=jointDirectLipVariableColumn)
#     pm.button(lipDnCenTmpJntButt, e=True, c=partial(addAllSelectionObject, lipDnCenTmpJntField, lipDnCenTmpJntButt))

#     # lip down left
#     pm.text(l="  lipDnLJnt", w=110, al="left", p=jointDirectLipVariableColumn)
#     lipDnLTmpJntField = pm.textField("lipDnLTmpJntField", tx="lipDn1_L_TmpJnt, lipDn2_L_TmpJnt", en=False, p=jointDirectLipVariableColumn)
#     pm.separator(style="none", w=5, p=jointDirectLipVariableColumn)
#     lipDnLTmpJntButt = pm.button("lipDnLTmpJntButt", l="<<", w=30, p=jointDirectLipVariableColumn)
#     pm.button(lipDnLTmpJntButt, e=True, c=partial(addAllSelectionObject, lipDnLTmpJntField, lipDnLTmpJntButt))

#     # lip down right
#     pm.text(l="  lipDnRJnt", w=110, al="left", p=jointDirectLipVariableColumn)
#     lipDnRTmpJntField = pm.textField("lipDnRTmpJntField", tx="lipDn1_R_TmpJnt, lipDn2_R_TmpJnt", en=False, p=jointDirectLipVariableColumn)
#     pm.separator(style="none", w=5, p=jointDirectLipVariableColumn)
#     lipDnRTmpJntButt = pm.button("lipDnRTmpJntButt", l="<<", w=30, p=jointDirectLipVariableColumn)
#     pm.button(lipDnRTmpJntButt, e=True, c=partial(addAllSelectionObject, lipDnRTmpJntField, lipDnRTmpJntButt))


#     #########################################################################################################################################

#     ## Jaw variable column
#     pm.separator(style="in", w=250, h=10, p=jointDirectColumn)
#     jointDirectJawColumn = pm.rowLayout("jointDirectJawColumn", w=300, nc=3, p=jointDirectColumn)
#     pm.text(l="  jawRig", w=110, al="left", p=jointDirectJawColumn)
#     jawRigDirectCheckBox = pm.checkBox("jawRigDirectCheckBox", l="", v=1, p=jointDirectJawColumn)
    
#     jointDirectJawVariableColumn = pm.rowColumnLayout("jointDirectJawVariableColumn", nc=4, p=jointDirectColumn)

#     # tongue
#     pm.text(l="  jawJnt", w=110, al="left", p=jointDirectJawVariableColumn)
#     jawTmpJntField = pm.textField("jawTmpJntField", tx="jawDn1_TmpJnt, jawDn2_TmpJnt, jawDn3_TmpJnt", p=jointDirectJawVariableColumn)
#     pm.separator(style="none", w=5, p=jointDirectJawVariableColumn)
#     jawTmpJntButt = pm.button("jawTmpJntButt", l="<<", w=30, p=jointDirectJawVariableColumn)
#     pm.button(jawTmpJntButt, e=True, c=partial(addAllSelectionObject, jawTmpJntField, jawTmpJntButt))

#     pm.checkBox(jawRigDirectCheckBox, e=True, cc=partial(disableFieldByCheckBox, jawRigDirectCheckBox, [jawTmpJntField]))


#     #########################################################################################################################################

#     ## tongue variable column
#     pm.separator(style="in", w=250, h=10, p=jointDirectColumn)
#     jointDirectTongueColumn = pm.rowLayout("jointDirectTongueColumn", w=300, nc=3, p=jointDirectColumn)
#     pm.text(l="  tongueRig", w=110, al="left", p=jointDirectTongueColumn)
#     tongueRigDirectCheckBox = pm.checkBox("tongueRigDirectCheckBox", l="", v=1, p=jointDirectTongueColumn)
    
#     jointDirectTongueVariableColumn = pm.rowColumnLayout("jointDirectTongueVariableColumn", nc=4, p=jointDirectColumn)

#     # tongue
#     pm.text(l="  tongueJnt", w=110, al="left", p=jointDirectTongueVariableColumn)
#     tongueTmpJntField = pm.textField("tongueTmpJntField", tx="tongue01_TmpJnt, tongue02_TmpJnt, tongue03_TmpJnt, tongue04_TmpJnt, tongue05_TmpJnt, tongue06_TmpJnt", p=jointDirectTongueVariableColumn)
#     pm.separator(style="none", w=5, p=jointDirectTongueVariableColumn)
#     tongueTmpJntButt = pm.button("tongueTmpJntButt", l="<<", w=30, p=jointDirectTongueVariableColumn)
#     pm.button(tongueTmpJntButt, e=True, c=partial(addAllSelectionObject, tongueTmpJntField, tongueTmpJntButt))

#     pm.checkBox(tongueRigDirectCheckBox, e=True, cc=partial(disableFieldByCheckBox, tongueRigDirectCheckBox, [tongueTmpJntField]))

#     #########################################################################################################################################

#     ## teeth variable column
#     pm.separator(style="in", w=250, h=10, p=jointDirectColumn)
#     jointDirectTeethColumn = pm.rowLayout("jointDirectTeethColumn", w=300, nc=3, p=jointDirectColumn)
#     pm.text(l="  teethRig", w=110, al="left", p=jointDirectTeethColumn)
#     teethRigDirectCheckBox = pm.checkBox("teethRigDirectCheckBox", l="", v=1, p=jointDirectTeethColumn)

#     jointDirectTeethVariableColumn = pm.rowColumnLayout("jointDirectTeethVariableColumn", nc=4, p=jointDirectColumn)

#     # teeth main up
#     pm.text(l="  teethMainUpJnt", w=110, al="left", p=jointDirectTeethVariableColumn)
#     teethMainUpTmpJntField = pm.textField("teethMainUpTmpJntField", tx="teethMainUp_TmpJnt", p=jointDirectTeethVariableColumn)
#     pm.separator(style="none", w=5, p=jointDirectTeethVariableColumn)
#     teethMainUpTmpJntButt = pm.button("teethMainUpJntButt", l="<<", w=30, p=jointDirectTeethVariableColumn)
#     pm.button(teethMainUpTmpJntButt, e=True, c=partial(addAllSelectionObject, teethMainUpTmpJntField, teethMainUpTmpJntButt))

#     # teeth up
#     pm.text(l="  teethUpJnt", w=110, al="left", p=jointDirectTeethVariableColumn)
#     teethUpTmpJntField = pm.textField("teethUpTmpJntField", tx="teethUp_TmpJnt, teethUp_L_TmpJnt, teethUp_R_TmpJnt, teethUpPos_L_TmpJnt, teethUpPos_R_TmpJnt", p=jointDirectTeethVariableColumn)
#     pm.separator(style="none", w=5, p=jointDirectTeethVariableColumn)
#     teethUpTmpJntButt = pm.button("teethUpTmpJntButt", l="<<", w=30, p=jointDirectTeethVariableColumn)
#     pm.button(teethUpTmpJntButt, e=True, c=partial(addAllSelectionObject, teethUpTmpJntField, teethUpTmpJntButt))

#     # teeth main down
#     pm.text(l="  teethMainDnJnt", w=110, al="left", p=jointDirectTeethVariableColumn)
#     teethMainDnTmpJntField = pm.textField("teethMainDnTmpJntField", tx="teethMainDn_TmpJnt", p=jointDirectTeethVariableColumn)
#     pm.separator(style="none", w=5, p=jointDirectTeethVariableColumn)
#     teethMainDnTmpJntButt = pm.button("teethMainDnTmpJntButt", l="<<", w=30, p=jointDirectTeethVariableColumn)
#     pm.button(teethMainDnTmpJntButt, e=True, c=partial(addAllSelectionObject, teethMainDnTmpJntField, teethMainDnTmpJntButt))

#     # teeth down
#     pm.text(l="  teethDnJnt", w=110, al="left", p=jointDirectTeethVariableColumn)
#     teethDnTmpJntField = pm.textField("teethDnTmpJntField", tx="teethDn_TmpJnt, teethDn_L_TmpJnt, teethDn_R_TmpJnt, teethDnPos_L_TmpJnt, teethDnPos_R_TmpJnt", p=jointDirectTeethVariableColumn)
#     pm.separator(style="none", w=5, p=jointDirectTeethVariableColumn)
#     teethDnTmpJntButt = pm.button("teethDnTmpJntButt", l="<<", w=30, p=jointDirectTeethVariableColumn)
#     pm.button(teethDnTmpJntButt, e=True, c=partial(addAllSelectionObject, teethDnTmpJntField, teethDnTmpJntButt))

#     pm.checkBox(teethRigDirectCheckBox, e=True, cc=partial(disableFieldByCheckBox, teethRigDirectCheckBox, [teethMainUpTmpJntField, teethUpTmpJntField, teethMainDnTmpJntField, teethDnTmpJntField]))

#     #########################################################################################################################################

#     ## nose variable column
#     pm.separator(style="in", w=250, h=10, p=jointDirectColumn)
#     jointDirectNoseColumn = pm.rowLayout("jointDirectNoseColumn", w=300, nc=3, p=jointDirectColumn)
#     pm.text(l="  noseRig", w=110, al="left", p=jointDirectNoseColumn)
#     noseRigDirectCheckBox = pm.checkBox("noseRigDirectCheckBox", l="", v=1, p=jointDirectNoseColumn)

#     jointDirectNoseVariableColumn = pm.rowColumnLayout("jointDirectNoseVariableColumn", nc=4, p=jointDirectColumn)

#     # nose joint
#     pm.text(l="  noseJnt", w=110, al="left", p=jointDirectNoseVariableColumn)
#     noseDirField = pm.textField("noseDirField", tx="nose_TmpJnt, nose_L_TmpJnt, nose_R_TmpJnt", p=jointDirectNoseVariableColumn)
#     pm.separator(style="none", w=5, p=jointDirectNoseVariableColumn)
#     dtlOnFaceButt = pm.button("dtlOnFaceButt", l="<<", w=30, p=jointDirectNoseVariableColumn)
#     pm.button(dtlOnFaceButt, e=True, c=partial(addAllSelectionObject, noseDirField, dtlOnFaceButt))

#     pm.checkBox(noseRigDirectCheckBox, e=True, cc=partial(disableFieldByCheckBox, noseRigDirectCheckBox, [noseDirField]))

#     #########################################################################################################################################

#     ## detail variable column
#     pm.separator(style="in", w=250, h=10, p=jointDirectColumn)
#     jointDirectDetailColumn = pm.rowLayout("jointDirectDetailColumn", w=300, nc=3, p=jointDirectColumn)
#     pm.text(l="  detailRig", w=110, al="left", p=jointDirectDetailColumn)
#     detailRigDirectCheckBox = pm.checkBox("detailRigDirectCheckBox", l="", v=1, p=jointDirectDetailColumn)

#     jointDirectDtlVariableColumn = pm.rowColumnLayout("jointDirectDtlVariableColumn", nc=4, p=jointDirectColumn)

#     # detail on face
#     pm.text(l="  dtlOnFace", w=110, al="left", p=jointDirectDtlVariableColumn)
#     dtlOnFaceField = pm.textField("dtlOnFaceField", tx="cheekU_L_TmpJnt, cheekD_L_TmpJnt, cheekU_R_TmpJnt, cheekD_R_TmpJnt", p=jointDirectDtlVariableColumn)
#     pm.separator(style="none", w=5, p=jointDirectDtlVariableColumn)
#     dtlOnFaceButt = pm.button("dtlOnFaceButt", l="<<", w=30, p=jointDirectDtlVariableColumn)
#     pm.button(dtlOnFaceButt, e=True, c=partial(addAllSelectionObject, dtlOnFaceField, dtlOnFaceButt))

#     pm.checkBox(detailRigDirectCheckBox, e=True, cc=partial(disableFieldByCheckBox, detailRigDirectCheckBox, [dtlOnFaceField]))



#     # size
#     pm.separator(style="in", h=10, p=jointDirectColumn)
#     jointDirectSizeColumn = pm.rowLayout("jointDirectSizeColumn", w=300, nc=3, p=jointDirectColumn)
#     pm.text(l="  size", w=110, al="left", p=jointDirectSizeColumn)
#     sizeField = pm.floatField("sizeField", w=93, v=0.75, pre=2, p=jointDirectSizeColumn)

#     ### create button
#     mc.separator(style="none", h=10, p=jointDirectColumn)
#     createJointDirectMouthButt = pm.button("createJointDirectButt", l="Create Controller", w=250, h=30, c=createfacialDirectRig, p=jointDirectColumn)

#     pm.separator(style="in", w=250, h=20, p=jointDirectColumn)










# mainTab B-------------------------------------------------------------------------------------------------------------------------------       
# mainTab B-------------------------------------------------------------------------------------------------------------------------------    
    mainTab_B = pm.tabLayout ( innerMarginWidth = 5 , innerMarginHeight = 5 , p = mainLayout ) ;

# Tools Tab -------------------------
# Tools Tab -------------------------
    ToolsTab = pm.rowColumnLayout ( "Tools", nc = 1 , p = mainTab_B ) ;
    mainToolsTab = pm.rowColumnLayout ( nc = 1 , p = ToolsTab , w = 250) ;
    # FrameLayout A
    Tools_A_FrameLayout = pm.frameLayout ("Tools_A_Frame", label = "Building Ui", collapse = False, collapsable = True,  p = mainToolsTab)
    Tools_A_Layout = pm.rowColumnLayout ( nc = 6 , p = Tools_A_FrameLayout, cw = [(1, 40), (2, 40), (3, 40), (4, 40), (5, 40), (6, 40)]) ;

    # pm.symbolButton ( 'toolsA1' , image = ("%s%s" % (pngImags(),"Wrist_skinWeight2.1.png")), h = 40, p = Tools_A_Layout , c = writeSelectedWeightFile , bgc = (0.312, 0.312, 0.312)) ; 
    # pm.symbolButton ( 'toolsA2' , image = ("%s%s" % (pngImags(),"Read_skinWeight.1.png")), h = 40, p = Tools_A_Layout , c = readSelectedWeightFile , bgc = (0.312, 0.312, 0.312)) ; 
    # pm.symbolButton ( 'toolsA3' , image = ("%s%s" % (pngImags(),"paintSkinWeights.png")), h = 40, p = Tools_A_Layout , c = paintSkinWeightAFile , bgc = (0.312, 0.312, 0.312)) ; 
    # pm.symbolButton ( 'toolsA4' , image = ("%s%s" % (pngImags(),"paintSkinWeights.png")), h = 40, p = Tools_A_Layout , c = paintSkinWeightBFile , bgc = (0.312, 0.312, 0.312)) ; 
    # pm.symbolButton ( 'toolsA5' , image = ("%s%s" % (pngImags(),"separateNeck3.png")), h = 40, p = Tools_A_Layout , c = deleteSelectEdgeFile , bgc = (0.312, 0.312, 0.312)) ; 
    # pm.symbolButton ( 'toolsA6' , image = ("%s%s" % (pngImags(),"arrow_mix.png")), h = 40, p = Tools_A_Layout , c = snapFile, bgc = (0.312, 0.312, 0.312)) ; 

    # # pm.symbolButton ( 'toolsA6' , image = ("%s%s" % (pngImags(),"snap2.png")), h = 40, p = Tools_A_Layout , c = snapFile , bgc = (0.411765, 0.411765, 0.411765)) ; 
    # pm.symbolButton ( 'toolsA7' , image = ("%s%s" % (pngImags(),"Write_ControlShape.png")), h = 40, p = Tools_A_Layout , c = writeAllCtrlFile , bgc = (0.352, 0.352, 0.352)) ; 
    # pm.symbolButton ( 'toolsA8' , image = ("%s%s" % (pngImags(),"Read_ControlShape.png")), h = 40, p = Tools_A_Layout , c = readAllCtrlFile , bgc = (0.352, 0.352, 0.352)) ; 
    # pm.symbolButton ( 'toolsA9' , image = ("%s%s" % (pngImags(),"bone-icon.png")), h = 40, p = Tools_A_Layout , c = createJntFile, bgc = (0.352, 0.352, 0.352)) ; 
    # pm.symbolButton ( 'toolsA10' , image = ("%s%s" % (pngImags(),"selectJoint.png")), h = 40, p = Tools_A_Layout , c = selectJntFile , bgc = (0.352, 0.352, 0.352)) ; 
    # pm.symbolButton ( 'toolsA12' , image = ("%s%s" % (pngImags(),"parentJnt2.png")), h = 40, p = Tools_A_Layout , c = parentJntFile , bgc = (0.352, 0.352, 0.352)) ; 
    # pm.symbolButton ( 'toolsA11' , image = ("%s%s" % (pngImags(),"zero_icon.png")), h = 40, p = Tools_A_Layout , c = createGroupFile , bgc = (0.352, 0.352, 0.352)) ; 

    # pm.symbolButton ( 'toolsA13' , image = ("%s%s" % (pngImags(),"copySkinWeight_2.png")), h = 40, p = Tools_A_Layout , c = copySkinWeightAFile , bgc = (0.412, 0.412, 0.412)) ; 
    # pm.symbolButton ( 'toolsA14' , image = ("%s%s" % (pngImags(),"copySkinWeight_1.png")), h = 40, p = Tools_A_Layout , c = copySkinWeightBFile , bgc = (0.412, 0.412, 0.412)) ; 
    # pm.symbolButton ( 'toolsA15' , image = ("%s%s" % (pngImags(),"copySkinWeight_Position.png")), h = 40, p = Tools_A_Layout , c = copyShapePositionFile , bgc = (0.412, 0.412, 0.412)) ; 
    # pm.symbolButton ( 'toolsA16' , image = ("%s%s" % (pngImags(),"Unlock2.ico")), h = 40, p = Tools_A_Layout , c = unlockNodeFile , bgc = (0.412, 0.412, 0.412)) ; 
    # pm.symbolButton ( 'toolsA17' , image = ("%s%s" % (pngImags(),"De-Unlock2.ico")), h = 40, p = Tools_A_Layout , c = deleteUnlockNodeFile , bgc = (0.412, 0.412, 0.412)) ; 
    # pm.symbolButton ( 'toolsA18' , image = ("%s%s" % (pngImags(),"clean.png")), h = 40, p = Tools_A_Layout , c = cleanFile , bgc = (0.412, 0.412, 0.412)) ; 

    # pm.symbolButton ( 'toolsA19' , image = ("%s%s" % (pngImags(),"import_Ref.png")), h = 40, p = Tools_A_Layout , c = importSelectRefFile , bgc = (0.452, 0.452, 0.452)) ; 
    # pm.symbolButton ( 'toolsA20' , image = ("%s%s" % (pngImags(),"remove_Ref.png")), h = 40, p = Tools_A_Layout , c = removeSelectRefFile , bgc = (0.452, 0.452, 0.452)) ; 
    # pm.symbolButton ( 'toolsA21' , image = ("%s%s" % (pngImags(),"axis.png")), h = 40, p = Tools_A_Layout , c = LRAFile , bgc = (0.452, 0.452, 0.452)) ; 
    # pm.symbolButton ( 'toolsA22' , image = ("%s%s" % (pngImags(),"DeleteAxis.png")), h = 40, p = Tools_A_Layout , c = DLRAFile , bgc = (0.452, 0.452, 0.452)) ; 
    # pm.symbolButton ( 'toolsA23' , image = ("%s%s" % (pngImags(),"mainGroupProxyRig.png")), h = 40, p = Tools_A_Layout , c = mainProxyRigFile, bgc = (0.452, 0.452, 0.452)) ; 
    # pm.symbolButton ( 'toolsA24' , image = ("%s%s" % (pngImags(),"mainGroup3.png")), h = 40, p = Tools_A_Layout , c = mainGroupFile, bgc = (0.452, 0.452, 0.452)) ; 

    # pm.symbolButton ( 'toolsA25' , image = ("%s%s" % (pngImags(),"delGrp2.png")), h = 40, p = Tools_A_Layout , c = deleteGrpFile , bgc = (0.512, 0.512, 0.512)) ; 
    # pm.symbolButton ( 'toolsA26' , image = ("%s%s" % (pngImags(),"proxy.png")), h = 40, p = Tools_A_Layout , c = proxyObjFile , bgc = (0.512, 0.512, 0.512)) ; 
    # pm.symbolButton ( 'toolsA27' , image = ("%s%s" % (pngImags(),"ps-c-icon.png")), h = 40, p = Tools_A_Layout , c = parentScaleConstraintFile , bgc = (0.512, 0.512, 0.512)) ; 
    # pm.symbolButton ( 'toolsA28' , image = ("%s%s" % (pngImags(),"locator.png")), h = 40, p = Tools_A_Layout , c = locatorOnMidPosFile, bgc = (0.512, 0.512, 0.512)) ; 
    # pm.symbolButton ( 'toolsA29' , image = ("%s%s" % (pngImags(),"LocFrmEdge.png")), h = 40, p = Tools_A_Layout , c = jointFromLocatorFile, bgc = (0.512, 0.512, 0.512)) ; 
    # pm.symbolButton ( 'toolsA30' , image = ("%s%s" % (pngImags(),"jntFrmEdge.png")), h = 40, p = Tools_A_Layout , c = createLocatorAndJntFile, bgc = (0.512, 0.512, 0.512)) ; 

    # pm.symbolButton ( 'toolsA31' , image = ("%s%s" % (pngImags(),"connectTRS.png")), h = 40, p = Tools_A_Layout , c = connectTRSFile, bgc = (0.552, 0.552, 0.552)) ; 
    # pm.symbolButton ( 'toolsA32' , image = ("%s%s" % (pngImags(),"connectAttrTranslate.png")), h = 40, p = Tools_A_Layout , c = connectTFile, bgc = (0.552, 0.552, 0.552)) ; 
    # pm.symbolButton ( 'toolsA33' , image = ("%s%s" % (pngImags(),"connectAttrRotate.png")), h = 40, p = Tools_A_Layout , c = connectRFile, bgc = (0.552, 0.552, 0.552)) ; 
    # pm.symbolButton ( 'toolsA34' , image = ("%s%s" % (pngImags(),"connectAttrScale.png")), h = 40, p = Tools_A_Layout , c = connectSFile, bgc = (0.552, 0.552, 0.552)) ; 
    # pm.symbolButton ( 'toolsA35' , image = ("%s%s" % (pngImags(),"DtlConLR2.png")), h = 40, p = Tools_A_Layout , c = connectDtlNrbFile, bgc = (0.552, 0.552, 0.552)) ; 
    # pm.symbolButton ( 'toolsA36' , image = ("%s%s" % (pngImags(),"copyAttr.png")), h = 40, p = Tools_A_Layout , c = transferAttr_LtoR_RunFile, bgc = (0.552, 0.552, 0.552)) ; 


    # pm.symbolButton ( 'toolsA37' , image = ("%s%s" % (pngImags(),"WriteLayerVis.png")), h = 40, p = Tools_A_Layout , c = tagNameConnectionWriteFile, bgc = (0.612, 0.612, 0.612)) ; 
    # pm.symbolButton ( 'toolsA38' , image = ("%s%s" % (pngImags(),"ReadLayerVis.png")), h = 40, p = Tools_A_Layout , c = tagNameConnectionReadFile, bgc = (0.612, 0.612, 0.612)) ; 
    # pm.symbolButton ( 'toolsA39' , image = ("%s%s" % (pngImags(),"WriteDefaultAttrAll.png")), h = 40, p = Tools_A_Layout , c = attrWriteFile, bgc = (0.612, 0.612, 0.612)) ; 
    # pm.symbolButton ( 'toolsA40' , image = ("%s%s" % (pngImags(),"DefaultAttrAll2.png")), h = 40, p = Tools_A_Layout , c = attrReadAllFile, bgc = (0.612, 0.612, 0.612)) ; 
    # pm.symbolButton ( 'toolsA41' , image = ("%s%s" % (pngImags(),"DefaultAttrSEL2.png")), h = 40, p = Tools_A_Layout , c = attrReadSELFile, bgc = (0.612, 0.612, 0.612)) ; 
    # pm.symbolButton ( 'toolsA42' , image = ("%s%s" % (pngImags(),"tranferShade.png")), h = 40, p = Tools_A_Layout , c = transferShadeAssignFile, bgc = (0.612, 0.612, 0.612)) ; 


    # pm.symbolButton ( 'toolsA43' , image = ("%s%s" % (pngImags(),"FixParConToMatrixCon.png")), h = 40, p = Tools_A_Layout , c = fixParentConToMatrixCon, bgc = (0.612, 0.612, 0.612)) ; 
    





    # pm.symbolButton ( 'toolsA44' , image = ("%s%s" % (pngImags(),"ReadLayerVis.png")), h = 40, p = Tools_A_Layout , c = tagNameConnectionReadFile, bgc = (0.612, 0.612, 0.612)) ; 
    # pm.symbolButton ( 'toolsA45' , image = ("%s%s" % (pngImags(),"WriteDefaultAttrAll.png")), h = 40, p = Tools_A_Layout , c = attrWriteFile, bgc = (0.612, 0.612, 0.612)) ; 
    # pm.symbolButton ( 'toolsA46' , image = ("%s%s" % (pngImags(),"DefaultAttrAll2.png")), h = 40, p = Tools_A_Layout , c = attrReadAllFile, bgc = (0.612, 0.612, 0.612)) ; 
    # pm.symbolButton ( 'toolsA47' , image = ("%s%s" % (pngImags(),"DefaultAttrSEL2.png")), h = 40, p = Tools_A_Layout , c = attrReadSELFile, bgc = (0.612, 0.612, 0.612)) ; 
    # pm.symbolButton ( 'toolsA18' , image = ("%s%s" % (pngImags(),"tranferShade.png")), h = 40, p = Tools_A_Layout , c = transferShadeAssignFile, bgc = (0.612, 0.612, 0.612)) ; 





  #   # FrameLayout B Option
  #   Tools_B_FrameLayout = pm.frameLayout ("Tools_B_Frame", label = "Tools B Option", collapse = False, collapsable = True, p = mainToolsTab)
  #   Tools_B_Layout = pm.rowColumnLayout ( nc = 6 , p = Tools_B_FrameLayout, cw = [(1, 40), (2, 40), (3, 40), (4, 40), (5, 40), (6, 40)]) ;

  #   pm.symbolButton ( 'toolsB1' , image = ("%s%s" % (pngImags(),"localWorld.png")), h = 40, p = Tools_B_Layout , c = localWorldFile, bgc = (0.312, 0.312, 0.312)) ; 
  #   pm.symbolButton ( 'toolsB2' , image = ("%s%s" % (pngImags(),"KeyFrame2.png")), h = 40, p = Tools_B_Layout , c = keyFrameFile, bgc = (0.312, 0.312, 0.312)) ; 
  #   pm.symbolButton ( 'toolsB3' , image = ("%s%s" % (pngImags(),"rename2.png")), h = 40, p = Tools_B_Layout , c = cometRenameFile , bgc = (0.312, 0.312, 0.312)) ; 
  #   pm.symbolButton ( 'toolsB4' , image = ("%s%s" % (pngImags(),"rename2.png")), h = 40, p = Tools_B_Layout , c = nuRenameFile , bgc = (0.312, 0.312, 0.312)) ; 
  #   pm.symbolButton ( 'toolsB5' , image = ("%s%s" % (pngImags(),"LR-icon.png")), h = 40, p = Tools_B_Layout , c = pkCopyCrvShapeUiFile , bgc = (0.312, 0.312, 0.312)) ; 
  #   pm.symbolButton ( 'toolsB6' , image = ("%s%s" % (pngImags(),"scaleGimbal.png")), h = 40, p = Tools_B_Layout , c = scaleGmblFile , bgc = (0.312, 0.312, 0.312)) ;

  #   pm.symbolButton ( 'toolsB7' , image = ("%s%s" % (pngImags(),"abSymMeshA.png")), h = 40, p = Tools_B_Layout , c = symMesfile, bgc = (0.352, 0.352, 0.352)) ; 
  #   pm.symbolButton ( 'toolsB8' , image = ("%s%s" % (pngImags(),"abSymMeshB.png")), h = 40, p = Tools_B_Layout , c = abSymMesfile, bgc = (0.352, 0.352, 0.352)) ; 
  #   pm.symbolButton ( 'toolsB9' , image = ("%s%s" % (pngImags(),"peckTools.png")), h = 40, p = Tools_B_Layout , c = peckToolsFile , bgc = (0.352, 0.352, 0.352)) ; 
  #   pm.symbolButton ( 'toolsB10' , image = ("%s%s" % (pngImags(),"taTools.png")), h = 40, p = Tools_B_Layout , c = taToolsFile , bgc = (0.352, 0.352, 0.352)) ; 
  #   pm.symbolButton ( 'toolsB11' , image = ("%s%s" % (pngImags(),"skinWeightTools2.png")), h = 40, p = Tools_B_Layout , c = skinToolsFile , bgc = (0.352, 0.352, 0.352)) ; 
  #   pm.symbolButton ( 'toolsB12' , image = ("%s%s" % (pngImags(),"skinWeightPuller2.png")), h = 40, p = Tools_B_Layout , c = weightPullerFile , bgc = (0.352, 0.352, 0.352)) ; 

  #   pm.symbolButton ( 'toolsB13' , image = ("%s%s" % (pngImags(),"copyUV.png")), h = 40, p = Tools_B_Layout , c = copyUVFile, bgc = (0.412, 0.412, 0.412)) ; 
  #   pm.symbolButton ( 'toolsB14' , image =("%s%s" % (pngImags(),"xboxControl.png")), h = 40, p = Tools_B_Layout , c = controlFile,  bgc = (0.412, 0.412, 0.412)) ; 
  #   pm.symbolButton ( 'toolsB15' , image =("%s%s" % (pngImags(),"og-mrv4.png")), h = 40, p = Tools_B_Layout , c = addDtlFile, bgc = (0.412, 0.412, 0.412)) ; 
  #   pm.symbolButton ( 'toolsB16' , image = ("%s%s" % (pngImags(),"fileTextureManager5.png")), h = 40, p = Tools_B_Layout , c = fileTextureManagerFile , bgc = (0.412, 0.412, 0.412)) ; 
  #   pm.symbolButton ( 'toolsB17' , image = ("%s%s" % (pngImags(), "motions.png")), h = 40, p = Tools_B_Layout , c = utaMotionsToolsUi, bgc = (0.412, 0.412, 0.412)) ; 
  #   pm.symbolButton ( 'toolsB18' , image = ("%s%s" % (pngImags(), "export2.png")), h = 40, p = Tools_B_Layout , c = ImExportAnimUi, bgc = (0.412, 0.412, 0.412)) ; 

  #   pm.symbolButton ( 'toolsB19' , image = ("%s%s" % (pngImags(),"ControlLocal.png")), h = 40, p = Tools_B_Layout , c = localControlMakerUi, bgc = (0.452, 0.452, 0.452)) ; 
  #   pm.symbolButton ( 'toolsB20' , image = ("%s%s" % (pngImags(),"dtlOnFace2.png")), h = 40, p = Tools_B_Layout , c =detailCtrlRigFile, bgc = (0.452, 0.452, 0.452)) ; 
  #   pm.symbolButton ( 'toolsB21' , image = ("%s%s" % (pngImags(), "export2.png")), h = 40, p = Tools_B_Layout , c = ImExportAnimUi2,  bgc = (0.452, 0.452, 0.452)) ; 
  #   pm.symbolButton ( 'toolsB22' , image = ("%s%s" % (pngImags(), "Rivet.png")), h = 40, p = Tools_B_Layout , c = rivetUiFile,  bgc = (0.452, 0.452, 0.452)) ; 
  #   pm.symbolButton ( 'toolsB23' , image = ("%s%s" % (pngImags(),"rbbui.png")), h = 40, p = Tools_B_Layout , c = ribbonUiFile, bgc = (0.452, 0.452, 0.452)) ; 
  #   pm.symbolButton ( 'toolsB24' , image = ("%s%s" % (pngImags(),"spotlight-icon.png")), h = 40, p = Tools_B_Layout , c =nameCheckerFile, bgc = (0.452, 0.452, 0.452)) ; 

  #   pm.symbolButton ( 'toolsB25' , image = ("%s%s" % (pngImags(),"lipSeal.png")), h = 40, p = Tools_B_Layout , c = mthRigLoopTmpLocFile, bgc = (0.492, 0.492, 0.492)) ; 
  #   pm.symbolButton ( 'toolsB26' , image = ("%s%s" % (pngImags(),"bone-icon-Option.png")), h = 40, p = Tools_B_Layout , c = GenerateJntAxisFile, bgc = (0.492, 0.492, 0.492)) ; 
  #   pm.symbolButton ( 'toolsB27' , image = ("%s%s" % (pngImags(),"proxy-Option2.png")), h = 40, p = Tools_B_Layout , c = ExFaceProxyUiFile, bgc = (0.492, 0.492, 0.492)) ; 
  #   pm.symbolButton ( 'toolsB28' , image = ("%s%s" % (pngImags(),"Strings3.png")), h = 40, p = Tools_B_Layout , c = StringsRigUiFile, bgc = (0.492, 0.492, 0.492)) ; 
  #   pm.symbolButton ( 'toolsB29' , image = ("%s%s" % (pngImags(),"copyShapeBshAll.png")), h = 40, p = Tools_B_Layout , c = copyShapeAllUiFile, bgc = (0.492, 0.492, 0.492)) ; 
  #   pm.symbolButton ( 'toolsB30' , image = ("%s%s" % (pngImags(),"512.png")), h = 40, p = Tools_B_Layout , c = copyShapeUiFile, bgc = (0.492, 0.492, 0.492)) ; 

  #   pm.symbolButton ( 'toolsB31' , image = ("%s%s" % (pngImags(),"Light-Room-icon.png")), h = 40, p = Tools_B_Layout , c = copyNonlinearFile, bgc = (0.532, 0.532, 0.532)) ; 
  #   pm.symbolButton ( 'toolsB32' , image = ("%s%s" % (pngImags(),"Light-Room-icon.png")), h = 40, p = Tools_B_Layout , c = proceduralRibbonFile, bgc = (0.532, 0.532, 0.532)) ; 
  #   pm.symbolButton ( 'toolsB33' , image = ("%s%s" % (pngImags(),"ghost.png")), h = 40, p = Tools_B_Layout , c = ribbonDeformerUiFile, bgc = (0.532, 0.532, 0.532)) ; 
  #   pm.symbolButton ( 'toolsB34' , image = ("%s%s" % (pngImags(),"MuscleConnectIcon.png")), h = 40, p = Tools_B_Layout , c = muscleConnectUiFile, bgc = (0.532, 0.532, 0.532)) ; 
  #   pm.symbolButton ( 'toolsB35' , image = ("%s%s" % (pngImags(),"miarmyUi.jpg")), h = 40, p = Tools_B_Layout , c = miarmyUiFile, bgc = (0.532, 0.532, 0.532)) ; 
  #   pm.symbolButton ( 'toolsB36' , image = ("%s%s" % (pngImags(),"miarmyUi.jpg")), h = 40, p = Tools_B_Layout , c = nonRollRigUiFile, bgc = (0.532, 0.532, 0.532)) ; 

  #   pm.symbolButton ( 'toolsB37' , image = ("%s%s" % (pngImags(),"gridController.png")), h = 40, p = Tools_B_Layout , c = gridControllerUiFile, bgc = (0.532, 0.532, 0.532)) ;
  #   pm.symbolButton ( 'toolsB38' , image = ("%s%s" % (pngImags(),"fxRigIcon.png")), h = 40, p = Tools_B_Layout , c = fxRigUiFile, bgc = (0.532, 0.532, 0.532)) ;  
  #   pm.symbolButton ( 'toolsB39' , image = ("%s%s" % (pngImags(),"fxRigIcon.png")), h = 40, p = Tools_B_Layout , c = copySkinWeightFile, bgc = (0.532, 0.532, 0.532)) ;  


  #  # FrameLayout C
  #   Tools_C_FrameLayout = pm.frameLayout ("Tools_C_Frame", label = "Tools C", collapse = True, collapsable = True,  p = mainToolsTab)
  #   Tools_C_Layout = pm.rowColumnLayout ( nc = 6 , p = Tools_C_FrameLayout, cw = [(1, 40), (2, 40), (3, 40), (4, 40), (5, 40), (6, 40)]) ;

  #   pm.button ( 'toolsC1' , label = 'SJB', h = 40, p = Tools_C_Layout , c = selJntBindSkin , bgc = (0.411765, 0.411765, 0.411765)) ; 
  #   pm.button ( 'toolsC2' , label = '----', h = 40, p = Tools_C_Layout , c = connectBsnFile , bgc = (0.411765, 0.411765, 0.411765)) ; 
  #   pm.button ( 'toolsC3' , label = '----', h = 40, p = Tools_C_Layout , c = connectBsnFile , bgc = (0.411765, 0.411765, 0.411765)) ; 
  #   pm.button ( 'toolsC4' , label = '----', h = 40, p = Tools_C_Layout , c = connectBsnFile , bgc = (0.411765, 0.411765, 0.411765)) ; 
  #   pm.button ( 'toolsC5' , label = '----', h = 40, p = Tools_C_Layout , c = connectBsnFile , bgc = (0.411765, 0.411765, 0.411765)) ; 
  #   pm.button ( 'toolsC6' , label = '----', h = 40, p = Tools_C_Layout , c = connectBsnFile , bgc = (0.411765, 0.411765, 0.411765)) ; 
  #   pm.button ( 'toolsC7' , label = '----', h = 40, p = Tools_C_Layout , c = connectBsnFile , bgc = (0.411765, 0.411765, 0.411765)) ; 
  #   pm.button ( 'toolsC8' , label = '----', h = 40, p = Tools_C_Layout , c = connectBsnFile , bgc = (0.411765, 0.411765, 0.411765)) ; 
  #   pm.button ( 'toolsC9' , label = '----', h = 40, p = Tools_C_Layout , c = connectBsnFile , bgc = (0.411765, 0.411765, 0.411765)) ; 
  #   pm.button ( 'toolsC10' , label = '----', h = 40, p = Tools_C_Layout , c = connectBsnFile , bgc = (0.411765, 0.411765, 0.411765)) ; 
  #   pm.button ( 'toolsC11' , label = '----', h = 40, p = Tools_C_Layout , c = connectBsnFile , bgc = (0.411765, 0.411765, 0.411765)) ; 
  #   pm.button ( 'toolsC12' , label = '----', h = 40, p = Tools_C_Layout , c = connectBsnFile , bgc = (0.411765, 0.411765, 0.411765)) ; 
  # # FrameLayout D
  #   Tools_D_FrameLayout = pm.frameLayout ("Tools_D_Frame", label = "Tools D", collapse = True, collapsable = True,  p = mainToolsTab)
  #   Tools_D_Layout = pm.rowColumnLayout ( nc = 6 , p = Tools_D_FrameLayout, cw = [(1, 40), (2, 40), (3, 40), (4, 40), (5, 40), (6, 40)]) ;

  #   pm.button ( 'toolsD1' , label = '----', h = 40, p = Tools_D_Layout , c = connectBsnFile , bgc = (0.411765, 0.411765, 0.411765)) ; 
  #   pm.button ( 'toolsD2' , label = '----', h = 40, p = Tools_D_Layout , c = connectBsnFile , bgc = (0.411765, 0.411765, 0.411765)) ; 
  #   pm.button ( 'toolsD3' , label = '----', h = 40, p = Tools_D_Layout , c = connectBsnFile , bgc = (0.411765, 0.411765, 0.411765)) ; 
  #   pm.button ( 'toolsD4' , label = '----', h = 40, p = Tools_D_Layout , c = connectBsnFile , bgc = (0.411765, 0.411765, 0.411765)) ; 
  #   pm.button ( 'toolsD5' , label = '----', h = 40, p = Tools_D_Layout , c = connectBsnFile , bgc = (0.411765, 0.411765, 0.411765)) ; 
  #   pm.button ( 'toolsD6' , label = '----', h = 40, p = Tools_D_Layout , c = connectBsnFile , bgc = (0.411765, 0.411765, 0.411765)) ; 
  #   pm.button ( 'toolsD7' , label = '----', h = 40, p = Tools_D_Layout , c = connectBsnFile , bgc = (0.411765, 0.411765, 0.411765)) ; 
  #   pm.button ( 'toolsD8' , label = '----', h = 40, p = Tools_D_Layout , c = connectBsnFile , bgc = (0.411765, 0.411765, 0.411765)) ; 
  #   pm.button ( 'toolsD9' , label = '----', h = 40, p = Tools_D_Layout , c = connectBsnFile , bgc = (0.411765, 0.411765, 0.411765)) ; 
  #   pm.button ( 'toolsD10' , label = '----', h = 40, p = Tools_D_Layout , c = connectBsnFile , bgc = (0.411765, 0.411765, 0.411765)) ; 
  #   pm.button ( 'toolsD11' , label = '----', h = 40, p = Tools_D_Layout , c = connectBsnFile , bgc = (0.411765, 0.411765, 0.411765)) ; 
  #   pm.button ( 'toolsD12' , label = '----', h = 40, p = Tools_D_Layout , c = connectBsnFile , bgc = (0.411765, 0.411765, 0.411765)) ; 
    # FrameLayout Dn
    # mainToolsDnTab = pm.rowColumnLayout ( nc = 1 , p = ToolsTab , w = 244) ;

    # # Free Space
    # mc.tabLayout(p=mainToolsDnTab, w= 248)
    # # free space
    # pm.rowColumnLayout (p = mainToolsDnTab ,h=8) ;
    # pm.text( label='# Tuterial FacialRig' , p = mainToolsDnTab, al = "left")
    # # free space
    # pm.rowColumnLayout (p = mainToolsDnTab ,h=8) ;

    # laptopLayout = pm.rowColumnLayout ( nc = 3 , p = mainToolsDnTab , w = 220, cw=[(1,10),(2,228),(3,10)]) ;

    # pm.rowColumnLayout (p = laptopLayout ) ;
    # pm.symbolCheckBox (image = ("%s%s" % (pngImags(),"laptop.png")), p = laptopLayout , h=20, onc = openTuterialFile , bgc = (0.941176, 0.972549, 1)) ;
    # freeSpace = pm.rowColumnLayout (p = mainToolsDnTab ,h=8) ;
    # # Free Space
    # mc.tabLayout(p=mainToolsDnTab, w= 248)


# cleanTab -------------------------
# cleanTab -------------------------
    cleanTab = pm.rowColumnLayout ( "Clean", nc = 1 , p = mainTab_B ) ;
    mainCleanTab = pm.rowColumnLayout ( nc = 1 , p = cleanTab , h = 320, w = 250) ;

    # 0. clean
    pm.button (label = "-"*100 ,h = 10, w = 250, p = mainCleanTab , bgc = (0.333333, 0.333333, 0.333333)) ; 
        # pm.text ( label = '1. Clean Skin File' , p = rowSkinLabel ) ;
    pm.button ( 'clean' , label = ' CLEAN ', h=20 , w = 250, p = mainCleanTab , c = cleanFile , bgc = (0.411765, 0.411765, 0.411765)) ; 


    # 1. clean Skin File
    # 0. clean
    pm.button (label = "-"*100 ,h = 10, w = 250, p = mainCleanTab , bgc = (0.333333, 0.333333, 0.333333)) ; 
        # pm.text ( label = '1. Clean Skin File' , p = rowSkinLabel ) ;
    pm.button ( 'butSkin' , label = '1. Clean Skin File' , h=20 , w = 250, p = mainCleanTab , c = cleanFile , bgc = (0.411765, 0.411765, 0.411765)) ; 

    # 2. clean Dtl File
    # 0. clean
    pm.button (label = "-"*100 ,h = 10, w = 250, p = mainCleanTab , bgc = (0.333333, 0.333333, 0.333333)) ; 
    pm.button ( 'butDtl' , label = '2. Clean Dtl File' , h=20 , w = 250, p = mainCleanTab , c = cleanDtlFile , bgc = (0.411765, 0.411765, 0.411765)) ; 

    # 3. clean Bsn File
    # 0. clean
    pm.button (label = "-"*100 ,h = 10, w = 250, p = mainCleanTab , bgc = (0.333333, 0.333333, 0.333333)) ; 
        # pm.text ( label = '3. Clean Bsh File' , p = rowBshLabel ) ;
    pm.button ( 'butBsh' , label = '3. Clean Bsh File' , h=20 , w = 250, p = mainCleanTab , c = cleanBshFile , bgc = (0.411765, 0.411765, 0.411765)) ; 

    # 4. clean Bsn File
    # 0. clean
    pm.button (label = "-"*100 ,h = 10, w = 250, p = mainCleanTab , bgc = (0.333333, 0.333333, 0.333333)) ; 
        # pm.text ( label = '3. Clean Bsh File' , p = rowBshLabel ) ;
    pm.button ( 'butSq' , label = '4. Clean FacialSq File' , h=20 , w = 250, p = mainCleanTab , c = cleanFacialSqFile , bgc = (0.411765, 0.411765, 0.411765)) ; 

    # roll 2 Column
    # 0. clean
    pm.button (label = "-"*100 ,h = 10, w = 250, p = mainCleanTab , bgc = (0.333333, 0.333333, 0.333333)) ; 
    rowColumn2 = pm.rowColumnLayout ( nc = 2 , p = mainCleanTab , w = 240 , cw = [ ( 1 , 120 ) , ( 2 , 120 )] ) ;

    # 4.4 Connect Dtl
    # 0. clean
    pm.button ( 'connectDtl' , label = '5.1 Connect Dtl' , h=20 , w = 128, p = rowColumn2 , c = connectDtlFile , bgc = (0.700 , 0.500, 0.900)) ; 

    # 4.2. Connect Bsn
    # 0. clean
    pm.button ( 'connectBsn' , label = '5.2 Connect Bsn' , h=20 , w = 128, p = rowColumn2 , c = connectBsnFile , bgc = (0.498039, 1, 0)) ; 
 
    # roll 4 Column  
    rowColumn3 = pm.rowColumnLayout ( nc = 2 , p = mainCleanTab , w = 240 , cw = [ ( 1 , 120 ) , ( 2 , 120 )] ) ;

    # 4.4 Connect Dtl
    # 0. clean
    pm.button (label = "-"*100 ,h = 10, w = 250, p = mainCleanTab , bgc = (0.333333, 0.333333, 0.333333)) ; 
    pm.button ( 'connectFaceialSq' , label = '5.3 Connect FacialSq' , h=20 , w = 50, p = rowColumn3 , c = connectFacialSqFile , bgc = (0.922 , 0.863, 0.044)) ; 

    # # 4.2. Connect Bsn
    # 0. clean
    pm.button ( 'connectBsn' , label = '5.4 Connect EyeLid' , h=20 , w = 128, p = rowColumn3 , c = disconEyeLid , bgc = (0.356 , 0.730, 0.882)) ;  
   
    rowCombRigLabelDn = pm.rowColumnLayout ( nc = 1 , p = mainCleanTab ) ;
    # 5 Clean CombRig File'
    # 0. clean
    pm.button (label = "-"*100 ,h = 10, w = 250, p = mainCleanTab , bgc = (0.333333, 0.333333, 0.333333)) ; 
    pm.button ( 'cleanCombRig' , label = '6. Clean CombRig File' , h=20 , w = 248, p = rowCombRigLabelDn , c = cleanFileAndLockEyeLid , bgc = (0.411765, 0.411765, 0.411765)) ; 
 
# # shadeTab -------------------------
# # shadeTab -------------------------
#     shadeTab = pm.rowColumnLayout ( "Shade", nc = 1 , p = mainTab_B ) ;
#     mainShadeTab = pm.rowColumnLayout ( nc = 1 , p = shadeTab , h = 220, w = 250) ;
            
#     # line TabB
#     rowLine_TabB = pm.rowColumnLayout ( nc = 1 , p = mainShadeTab , h = 10 ) ;
#     pm.button (label = "-"*200 ,h = 10, w = 248, p = rowLine_TabB , bgc = (0.333333, 0.333333, 0.333333)) ; 

#     rowColumnGeoBase = pm.rowColumnLayout ( nc = 2 , p = mainShadeTab , cw = [ ( 1 , 124.5 ) , ( 2 , 124.5 )] ) ;
#     #button copyShade
#     pm.button ( 'copyShadeButton' , label = 'Copy Shade ', h=20 , w = 125.5, p = rowColumnGeoBase , c = copyShadeFile , bgc = (0.411765, 0.411765, 0.411765)) ;

#     #button paseShade
#     pm.button ( 'paseShadeButton' , label = 'Pase Shade ', h=20, w = 125.5, p = rowColumnGeoBase , c = paseShadeFile , bgc = (0.941176, 0.972549, 1)) ;

#     # line TabB
#     rowLine_TabB = pm.rowColumnLayout ( nc = 1 , p = mainShadeTab , h = 10 ) ;
#     pm.button (label = "-"*200 ,h = 10, w = 248, p = rowLine_TabB , bgc = (0.333333, 0.333333, 0.333333)) ; 

# # Deatline Tab -------------------------
# # Deatline Tab -------------------------
#     DeatlineTab = pm.rowColumnLayout ( "Deadline", nc = 1 , p = mainTab_B ) ;
#     mainDeatlineTab = pm.rowColumnLayout ( nc = 1 , p = DeatlineTab , h = 220, w = 248) ;

#     # Free Space
#     mc.tabLayout(p=mainDeatlineTab)
#     # free space
#     pm.rowColumnLayout (p = mainDeatlineTab ,h=8) ;
#     ken = pm.text( label='# 2 Heros Deatline' , p = mainDeatlineTab, al = "left")
#     # free space
#     pm.rowColumnLayout (p = mainDeatlineTab ,h=8) ;

#     pm.button ( '2HerosDeatlineButton' , label = '2 Heros Deadline', h=20, p = mainDeatlineTab , c = twoHerosDeatlineFile , bgc = (0.411765, 0.411765, 0.411765)) ;

#     pm.rowColumnLayout (p = mainDeatlineTab ,h=8) ;
#     mc.tabLayout(p=mainDeatlineTab, w= 248)

#     # free space
#     pm.rowColumnLayout (p = mainDeatlineTab ,h=8) ;
#     ken = pm.text( label='# Nakee 2 Deatline' , p = mainDeatlineTab, al = "left")
#     # free space
#     pm.rowColumnLayout (p = mainDeatlineTab ,h=8) ;

#     pm.button ( 'Nakee2DeatlineButton' , label = 'Nakee 2 Deadline', h=20, p = mainDeatlineTab , c = nakee2DeatlineFile , bgc = (0.411765, 0.411765, 0.411765)) ;

#     pm.rowColumnLayout (p = mainDeatlineTab ,h=8) ;
#     mc.tabLayout(p=mainDeatlineTab, w= 248)
# xxx Tab -------------------------
# xxx Tab -------------------------
    # xxxTab = pm.rowColumnLayout ( "XXX", nc = 1 , p = mainTab_B ) ;
    # mainXXXTab = pm.rowColumnLayout ( nc = 1 , p = xxxTab , h = 220, w = 250) ; 

    titleLayoutDn = pm.rowColumnLayout ( w = 520 , p = windowRowColumnLayout , nc = 1 ) ;
    pm.symbolCheckBox ("titleButtonP1",image = ("%s%s" % (pngImags(),"titleLogoDn.png")) , p = titleLayoutDn , onc = openExporerFile , bgc = (0, 0, 0)) ; 

    pm.showWindow ( window ) ;
