# --------------------------------------------------------------------------------------------
#
# Add Detail Control
#
# description :  - select two edge
#                - run script
#                - duplicate base model to skin detail
#                - copy weight base model to nurb from script
#                - bind skin joint detail to model skin detail
#
# --------------------------------------------------------------------------------------------

import maya.cmds as mc
import maya.mel as mm
import ppmel.core as core
import re
# reload( core )

def runAddDtlCtrlNurb( arg = None ) :
    name = mc.textField( 'nameTF' , q = True , tx = True )
    side = mc.textField( 'sideTF' , q = True , tx = True )
    shape = mc.textField( 'shapeTF' , q = True , tx = True )
    mirror = mc.checkBox( 'mirrorCB' , q = True , v = True )
    
    addDtlCtrlNurb( name = name , side = side , shape = shape , mirror = mirror , charSize = 1 )
    
def addDtlCtrlNurb( name = '' , side = '' , shape = '' , mirror = False , charSize = 1 ) :
    sel = mc.ls( sl = True )
    
    if side == '' :
        side = '_'
    else :
        if not side == 'cen' :
            if side == 'lft' :
                side = '_lft_' 
                sideMirr = '_rgt_'
            else :
                side = '_rgt_' 
                sideMirr = '_lft_'
            
            if mirror == True :
                sideList = [ side , sideMirr ]
            else :
                sideList = [side]
        else :
            side = '_%s_' %side
            sideList = [side]
        
        
    if sel :
        obj = sel[0].split('.')[0]
        
        for sides in sideList :
            #-- Nurb
            dtlNrb = mc.loft( sel , ch = False , d = 1 , n = '%sDtl%snrb' % ( name , sides ))[0]
            
            if mirror == True :
                if sides == sideMirr :
                    core.setVal( '%s.sx' %dtlNrb , -1 )
                    core.freeze( dtlNrb )
            
            #-- Controls
            dtlCtrl = core.addCtrl( '%sDtl%sctrl' %( name , sides ) , shape , 'green' , jnt = False )
            dtlZro = core.addGrp( dtlCtrl )
            dtlOfst = core.addGrp( dtlCtrl , 'Ofst' )
            
            for attr in ( 'parameterU' , 'parameterV' ) :
                core.addAttr( '%sShape' %dtlCtrl , attr )
                
            core.setVal( '%sShape.parameterU' %dtlCtrl , 0.5 )
            core.setVal( '%sShape.parameterV' %dtlCtrl , 0.5 )
            
            if not side == 'cen' :
                if mirror == True :
                    if sides == sideMirr :
                        core.setVal( '%s.ry' %dtlZro , 180 )
                
            #-- Shape
            core.scaleShape( dtlCtrl , charSize * 0.1 )
            
            #-- PointOnSurfaceInfo
            dtlPosi = core.pointOnSurfaceInfo( '%sDtl%sposi' %( name , sides ))
            core.setVal( '%s.turnOnPercentage' %dtlPosi , 1 )
            
            mc.connectAttr( '%s.position' %dtlPosi , '%s.t' %dtlZro )
            mc.connectAttr( '%s.worldSpace[0]' %dtlNrb , '%s.inputSurface' %dtlPosi )
            mc.connectAttr( '%sShape.parameterU' %dtlCtrl , '%s.parameterU' %dtlPosi )
            mc.connectAttr( '%sShape.parameterV' %dtlCtrl , '%s.parameterV' %dtlPosi )
            
            #-- Joint
            dtlJnt = core.addJnt( '%sDtl%sjnt' %( name , sides ))
            dtlJntZro = core.addGrp( dtlJnt )
            core.setVal( '%s.radius' %dtlJnt , charSize * 0.1  )
            core.snap( dtlZro , dtlJntZro )
            
            if not side == 'cen' :
                if mirror == True :
                    if sides == sideMirr :
                        core.setVal( '%s.ry' %dtlJntZro , 180 )
            
            mc.connectAttr( '%s.t' %dtlCtrl , '%s.t' %dtlJnt )
            mc.connectAttr( '%s.r' %dtlCtrl , '%s.r' %dtlJnt )
            mc.connectAttr( '%s.s' %dtlCtrl , '%s.s' %dtlJnt )
            
            #-- Counter
            mdv = core.multiplyDivide( '%sDtl%smdv' %( name , sides ))
            mc.setAttr( '%s.i2x' %mdv , -1 )
            mc.setAttr( '%s.i2y' %mdv , -1 )
            mc.setAttr( '%s.i2z' %mdv , -1 )
            
            mc.connectAttr( '%s.t' %dtlCtrl , '%s.i1' %mdv )
            mc.connectAttr( '%s.o' %mdv , '%s.t' %dtlOfst )
            
            
    mc.select( cl = True )
    print 'success!'


# --------------------------------------------------------------------------------------------
#
# Add Detail Control
#
# description :  - select one edge
#                - run script
#
# --------------------------------------------------------------------------------------------

def addDtlCtrl( ctrl = '' , side = '' , shape = 'cube' , charSize = 1 ) :
    sel = mc.ls( sl = True )
    
    if side == '' :
        side = '_'
    else :
        side = '_%s_' %side
    
    if sel :
        obj = sel[0].split('.')[0]
        edge = mc.filterExpand( sm = 32 )
            
        #-- Joint
        jnt = core.addJnt( '%sDtl%sjnt' %( ctrl , side ))
        jntZro = core.addGrp( jnt )
        
        #-- Controls
        dtlCtrl = core.addCtrl( '%sDtl%sctrl' %( ctrl , side ) , shape , 'red' , jnt = False )
        dtlZro = core.addGrp( dtlCtrl )
        dtlOfst = core.addGrp( dtlCtrl , 'Ofst' )
        
        for attr in ( 'parameterU' , 'parameterV' ) :
            core.addAttr( '%sShape' %dtlCtrl , attr )
            
        core.setVal( '%sShape.parameterU' %dtlCtrl , 0.5 )
        core.setVal( '%sShape.parameterV' %dtlCtrl , 0.5 )
        
        #-- Shape
        core.scaleShape( dtlCtrl , charSize * 0.1 )
        
        #-- Info
        edgeNum = []
        for each in edge :
            e = re.findall(r'\d+', '%s\n' %each)[-1]
            edgeNum.append(e)
        
        #-- Create node
        cfme1 = core.curveFromMeshEdge( '%s1Dtl%scfme' %( ctrl , side ))
        core.setVal( '%s.ei[0]' %cfme1 , int(edgeNum[0]))
        
        cfme2 = core.curveFromMeshEdge( '%s2Dtl%scfme' %( ctrl , side ))
        core.setVal( '%s.ei[0]' %cfme2 , int(edgeNum[1]))
        
        loft = core.loft( '%sDtl%sloft' %( ctrl , side ))
        core.setVal( '%s.u' %loft , True )
        core.setVal( '%s.rsn' %loft , True )
        
        posi = core.pointOnSurfaceInfo( '%sDtl%sposi' %( ctrl , side ))
        core.setVal( '%s.turnOnPercentage' %posi , 1 )
        
        mdv = core.multiplyDivide( '%sDtl%smdv' %( ctrl , side ))
        mc.setAttr( '%s.i2x' %mdv , -1 )
        mc.setAttr( '%s.i2y' %mdv , -1 )
        mc.setAttr( '%s.i2z' %mdv , -1 )
        
        #-- Connect attribute
        mc.connectAttr( '%s.w' %obj , '%s.im' %cfme1 )
        mc.connectAttr( '%s.w' %obj , '%s.im' %cfme2 )
        mc.connectAttr( '%s.oc' %cfme1 , '%s.ic[0]' %loft )
        mc.connectAttr( '%s.oc' %cfme2 , '%s.ic[1]' %loft )
        mc.connectAttr( '%s.os' %loft , '%s.is' %posi )
        
        mc.connectAttr( '%s.position' %posi , '%s.t' %dtlZro )
        
        mc.connectAttr( '%s.t' %dtlCtrl , '%s.i1' %mdv )
        mc.connectAttr( '%s.o' %mdv , '%s.t' %dtlOfst )
        
        mc.connectAttr( '%sShape.parameterU' %dtlCtrl , '%s.parameterU' %posi )
        mc.connectAttr( '%sShape.parameterV' %dtlCtrl , '%s.parameterV' %posi )
        
        core.snap( dtlZro , jntZro )
        
        mc.connectAttr( '%s.t' %dtlCtrl , '%s.t' %jnt )
        mc.connectAttr( '%s.r' %dtlCtrl , '%s.r' %jnt )
        mc.connectAttr( '%s.s' %dtlCtrl , '%s.s' %jnt )
        
        #-- Cleanup
        core.setLock( dtlOfst , True , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
        
    mc.select( cl = True )