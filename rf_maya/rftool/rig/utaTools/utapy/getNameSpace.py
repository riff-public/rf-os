import maya.cmds as mc
import pymel.core as pm
def getNameSpace(*args):
# gather all the ns
	sel = mc.ls(sl=True)
	pathSel = sel[0]
	objSel = sel[-1]
	allns = []
	for obj in pm.ls(objSel):
		namespace = obj.namespace()
		if namespace:
			allns.append(namespace)
	print allns
def getNs(nodeName=''):
	'''
	Get namespace from given name
	Ex. aaa:bbb:ccc:ddd_grp > aaa:bbb:ccc:
	'''
	ns = ''

	if '|' in nodeName:
		nodeName = nodeName.split('|')[-1]

	if ':' in nodeName:

		nmElms = nodeName.split(':')

		if nmElms:
			ns = '%s:' % ':'.join(nmElms[0:-1])

	return ns