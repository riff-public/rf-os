import maya.cmds as mc

from ncmel import core
reload(core)
from ncmel import rigTools
reload(rigTools)
from ncmel import mainRig
reload(mainRig)
from ncmel import rootRig
reload(rootRig)
from ncmel import pelvisRig
reload(pelvisRig)
from ncmel import spineRig
reload(spineRig)
from ncmel import torsoRig
reload(torsoRig)
from ncmel import neckRig
reload(neckRig)
from ncmel import headRig
reload(headRig)
from ncmel import eyeRig
reload(eyeRig)
from ncmel import jawRig
reload(jawRig)
from ncmel import clavicleRig
reload(clavicleRig)
from ncmel import armRig
reload(armRig)
from utaTools.utapy import legRig02 as legRig
reload(legRig)
from ncmel import legAnimalRig
reload(legAnimalRig)
from utaTools.utapy import fingerRig
reload(fingerRig)
from ncmel import subRig
reload(subRig)
from ncmel import fkRig
reload(fkRig)
from ncmel import tailRig
reload(tailRig)
from ncmel import mouthRig
reload(mouthRig)
from utaTools.utapy import utaCore
reload(utaCore)
from utaTools.pkmel import ctrlShapeTools
reload(ctrlShapeTools)
from lpRig import rubberBandRig
reload(rubberBandRig)
from utaTools.utapy import charTextName
reload(charTextName)
from nuTools.rigTools import proc
reload(proc)
from utaTools.utapy import clavMuscleRig
reload(clavMuscleRig)
from utils.crvScript import ctrlData
reload(ctrlData)

def main( size = 0.75 ):
	
	print "# Generate >> Main Group"
	main = mainRig.Run( 
									assetName = '' ,
									size      = size
					   )
	print "##------------------------------------------------------------------------------------"
	print "##-----------------------------MAIN GROUP---------------------------------------------"
	print "##------------------------------------------------------------------------------------"


	print "# Generate >> Root"
	root = rootRig.Run( 
									rootTmpJnt = 'Root_TmpJnt' ,
									ctrlGrp    =  main.mainCtrlGrp ,
									skinGrp    =  main.skinGrp ,
									size       =  size
					   )

	print "# Generate >> Torso"
	torso = torsoRig.Run(   
									pelvisTmpJnt = 'Pelvis_TmpJnt' ,
									spine1TmpJnt = 'Spine1_TmpJnt' ,
									spine2TmpJnt = 'Spine2_TmpJnt' ,
									spine3TmpJnt = 'Spine3_TmpJnt' ,
									spine4TmpJnt = 'Spine4_TmpJnt' ,
									spine5TmpJnt = 'Spine5_TmpJnt' ,
									parent       =  root.rootJnt ,
									ctrlGrp      =  main.mainCtrlGrp ,
									skinGrp      =  main.skinGrp ,
									jntGrp       =  main.jntGrp ,
									stillGrp     =  main.stillGrp ,
									elem         = '' ,
									axis         = 'y' ,
									size         =  size
						 )
	## SetAttr SpineIk 
	mc.setAttr('%s.fkIk' % 'Spine_Ctrl', 1)

	print "# Generate >> Neck"
	neck = neckRig.Run(    
									neckTmpJnt = 'Neck_TmpJnt' ,
									headTmpJnt = 'Head_TmpJnt' ,
									parent     =  torso.jntDict[-1] ,
									ctrlGrp    =  main.mainCtrlGrp ,
									skinGrp    =  main.skinGrp ,
									elem       = '' ,
									side       = '' ,
									ribbon     = True ,
									head       = True ,
									axis       = 'y' ,
									size       = size
					   )
					 
	print "# Generate >> Head"
	head = headRig.Run(    
									headTmpJnt      = 'Head_TmpJnt' ,
									headEndTmpJnt   = 'HeadEnd_TmpJnt' ,
									eyeTrgTmpJnt    = 'EyeTrgt_TmpJnt' ,
									eyeLftTrgTmpJnt = 'EyeTrgt_L_TmpJnt' ,
									eyeRgtTrgTmpJnt = 'EyeTrgt_R_TmpJnt' ,
									eyeLftTmpJnt    = 'Eye_L_TmpJnt' ,
									eyeRgtTmpJnt    = 'Eye_R_TmpJnt' ,
									jawUpr1TmpJnt   = 'JawUpr1_TmpJnt' ,
									jawUprEndTmpJnt = 'JawUprEnd_TmpJnt' ,
									jawLwr1TmpJnt   = 'JawLwr1_TmpJnt' ,
									jawLwr2TmpJnt   = 'JawLwr2_TmpJnt' ,
									jawLwrEndTmpJnt = 'JawLwrEnd_TmpJnt' ,
									parent          =  neck.neckEndJnt ,
									ctrlGrp         =  main.mainCtrlGrp ,
									skinGrp         =  main.skinGrp ,
									elem            = '' ,
									side            = '' ,
									eyeRig          = False ,
									jawRig          = False ,
									size            = size 
					   )
					 
	print "# Generate >> Eye"
	eye = eyeRig.Run(    
									eyeTrgTmpJnt    = 'EyeTrgt_TmpJnt' ,
									eyeLftTrgTmpJnt = 'EyeTrgt_L_TmpJnt' ,
									eyeRgtTrgTmpJnt = 'EyeTrgt_R_TmpJnt' ,
									eyeLftTmpJnt    = 'Eye_L_TmpJnt' ,
									eyeRgtTmpJnt    = 'Eye_R_TmpJnt' ,
									parent          =  head.headJnt ,
									ctrlGrp         =  main.mainCtrlGrp ,
									skinGrp         =  main.skinGrp ,
									elem            = '' ,
									side            = '' ,
									size            = size 
					   )

	print "# Generate >> Jaw"
	jawObj = jawRig.Run(    
									jawUpr1TmpJnt   = 'JawUpr1_TmpJnt' ,
									jawUprEndTmpJnt = 'JawUprEnd_TmpJnt' ,
									jawLwr1TmpJnt   = 'JawLwr1_TmpJnt' ,
									jawLwr2TmpJnt   = 'JawLwr2_TmpJnt' ,
									jawLwrEndTmpJnt = 'JawLwrEnd_TmpJnt' ,
									parent          =  head.headJnt ,
									ctrlGrp         =  main.mainCtrlGrp ,
									skinGrp         =  main.skinGrp ,
									elem            = '' ,
									side            = '' ,
									size            = size 
					   )

	print "# Generate >> Clavicle Left"
	clavL = clavicleRig.Run(   
									clavTmpJnt  = 'Clav_L_TmpJnt' ,
									upArmTmpJnt = 'UpArm_L_TmpJnt' ,
									parent      =  torso.jntDict[-1] ,
									ctrlGrp     =  main.mainCtrlGrp ,
									jntGrp      =  main.jntGrp ,
									skinGrp     =  main.skinGrp ,
									elem        = '' ,
									side        = 'L' ,
									axis        = 'y' ,
									size        = size
							)
	
	print "# Generate >> Clavicle Right"
	clavR = clavicleRig.Run(   
									clavTmpJnt  = 'Clav_R_TmpJnt' ,
									upArmTmpJnt = 'UpArm_R_TmpJnt' ,
									parent      =  torso.jntDict[-1] ,
									ctrlGrp     =  main.mainCtrlGrp ,
									jntGrp      =  main.jntGrp ,
									skinGrp     =  main.skinGrp ,
									elem        = '' ,
									side        = 'R' ,
									axis        = 'y' ,
									size        = size
							)
	 
	print "# Generate >> Arm Left"
	armL = armRig.Run(              upArmTmpJnt   = 'UpArm_L_TmpJnt' ,
									forearmTmpJnt = 'Forearm_L_TmpJnt' ,
									wristTmpJnt   = 'Wrist_L_TmpJnt' ,
									handTmpJnt    = 'Hand_L_TmpJnt' ,
									elbowTmpJnt   = 'Elbow_L_TmpJnt' ,
									parent        = 'ClavEnd_L_Jnt' , 
									ctrlGrp       =  main.mainCtrlGrp ,
									jntGrp        =  main.jntGrp ,
									skinGrp       =  main.skinGrp ,
									stillGrp      =  main.stillGrp ,
									ikhGrp        =  main.ikhGrp ,
									elem          = '' ,
									side          = 'L' ,
									ribbon        = True ,
									size          = size
					)

	rigTools.addMoreSpace( obj = armL.wristIkLocWor ,
						   spaces = [ ('Shoulder' , armL.wristIkLocWor['localSpace'] ) ,
									  ('Head', head.headJnt ) ,
									  ('Chest', torso.jntDict[-1] ) ,
									  ('Pelvis', torso.pelvisJnt ) ] )
	
	print "# Generate >> Arm Right"
	armR = armRig.Run(              
									upArmTmpJnt   = 'UpArm_R_TmpJnt' ,
									forearmTmpJnt = 'Forearm_R_TmpJnt' ,
									wristTmpJnt   = 'Wrist_R_TmpJnt' ,
									handTmpJnt    = 'Hand_R_TmpJnt' ,
									elbowTmpJnt   = 'Elbow_R_TmpJnt' ,
									parent        = 'ClavEnd_R_Jnt' , 
									ctrlGrp       =  main.mainCtrlGrp ,
									jntGrp        =  main.jntGrp ,
									skinGrp       =  main.skinGrp ,
									stillGrp      =  main.stillGrp ,
									ikhGrp        =  main.ikhGrp ,
									elem          = '' ,
									side          = 'R' ,
									ribbon        = True ,
									size          = size
					)

	rigTools.addMoreSpace( obj = armR.wristIkLocWor ,
						   spaces = [ ('Shoulder' , armR.wristIkLocWor['localSpace'] ) ,
									  ('Head', head.headJnt ) ,
									  ('Chest', torso.jntDict[-1] ) ,
									  ('Pelvis', torso.pelvisJnt ) ] )
	
	print "# Generate >> Leg Left"
	legL = legRig.Run(              
									upLegTmpJnt   = 'UpLeg_L_TmpJnt' ,
									lowLegTmpJnt  = 'LowLeg_L_TmpJnt' ,
									ankleTmpJnt   = 'Ankle_L_TmpJnt' ,
									ballTmpJnt    = 'Ball_L_TmpJnt' ,
									toeTmpJnt     = 'Toe_L_TmpJnt' ,
									heelTmpJnt    = 'Heel_L_TmpJnt' ,
									footInTmpJnt  = 'FootIn_L_TmpJnt' ,
									footOutTmpJnt = 'FootOut_L_TmpJnt' ,
									kneeTmpJnt    = 'Knee_L_TmpJnt' ,
									heelIkTwistPivTmpJnt = 'HeelIkTwistPiv_L_TmpJnt',
									parent        =  torso.pelvisJnt , 
									ctrlGrp       =  main.mainCtrlGrp ,
									jntGrp        =  main.jntGrp ,
									skinGrp       =  main.skinGrp ,
									stillGrp      =  main.stillGrp ,
									ikhGrp        =  main.ikhGrp ,
									elem          = '' ,
									side          = 'L' ,
									ribbon        = True ,
									foot          = True ,
									kneePos       = 'KneePos_L_TmpJnt' ,
									size          = size
					)
	
	rigTools.addMoreSpace( obj = legL.kneeIkLocWor ,
						   spaces = [ ('Ankle' , legL.kneeIkLocWor['localSpace'] ) ,
									  ('Root', root.rootJnt )] )


	print "# Generate >> Leg Right"
	legR = legRig.Run(              
									upLegTmpJnt   = 'UpLeg_R_TmpJnt' ,
									lowLegTmpJnt  = 'LowLeg_R_TmpJnt' ,
									ankleTmpJnt   = 'Ankle_R_TmpJnt' ,
									ballTmpJnt    = 'Ball_R_TmpJnt' ,
									toeTmpJnt     = 'Toe_R_TmpJnt' ,
									heelTmpJnt    = 'Heel_R_TmpJnt' ,
									footInTmpJnt  = 'FootIn_R_TmpJnt' ,
									footOutTmpJnt = 'FootOut_R_TmpJnt' ,
									kneeTmpJnt    = 'Knee_R_TmpJnt' ,
									heelIkTwistPivTmpJnt = 'HeelIkTwistPiv_R_TmpJnt',
									parent        =  torso.pelvisJnt , 
									ctrlGrp       =  main.mainCtrlGrp ,
									jntGrp        =  main.jntGrp ,
									skinGrp       =  main.skinGrp ,
									stillGrp      =  main.stillGrp ,
									ikhGrp        =  main.ikhGrp ,
									elem          = '' ,
									side          = 'R' ,
									ribbon        = True ,
									foot          = True ,
									kneePos       = 'KneePos_R_TmpJnt' ,
									size          = size
					)
	rigTools.addMoreSpace( obj = legR.kneeIkLocWor ,
						   spaces = [ ('Ankle' , legR.kneeIkLocWor['localSpace'] ) ,
									  ('Root', root.rootJnt )] )


	# print "# Generate >> Eyedot Left Rig"
	# eyeDotRigObj_L = subRig.Run(
	# 								name       = 'EyeDot' ,
	# 								tmpJnt     = 'EyeDot_L_TmpJnt' ,
	# 								parent     =  eye.eyeLftJnt ,
	# 								ctrlGrp    =  main.mainCtrlGrp ,
	# 								shape      = 'circle' ,
	# 								side       = 'L' ,
	# 								size       = size  
	# 							)

	# print "# Generate >> Eyedot Right Rig"
	# eyeDotRigObj_R = subRig.Run(
	# 								name       = 'EyeDot' ,
	# 								tmpJnt     = 'EyeDot_R_TmpJnt' ,
	# 								parent     =  eye.eyeRgtJnt ,
	# 								ctrlGrp    =  main.mainCtrlGrp ,
	# 								shape      = 'circle' ,
	# 								side       = 'R' ,
	# 								size       = size  
	# 							)  

	# print "# Generate >> Iris Left Rig"
	# IrisRigObj_L = subRig.Run(
	# 								name       = 'Iris' ,
	# 								tmpJnt     = 'Iris_L_TmpJnt' ,
	# 								parent     = eye.eyeLftJnt,
	# 								ctrlGrp    = main.mainCtrlGrp ,
	# 								shape      = 'circle' ,
	# 								side       = 'L' ,
	# 								size       = size  
	# 							)

	# print "# Generate >> Iris Left Rig"
	# IrisRigObj_R = subRig.Run(
	# 								name       = 'Iris' ,
	# 								tmpJnt     = 'Iris_R_TmpJnt' ,
	# 								parent     = eye.eyeRgtJnt ,
	# 								ctrlGrp    = main.mainCtrlGrp ,
	# 								shape      = 'circle' ,
	# 								side       = 'R' ,
	# 								size       = size  
	# 							)

	# print "# Generate >> Pupil Left Rig"
	# PupilRigObj_L = subRig.Run(
	# 								name       = 'Pupil' ,
	# 								tmpJnt     = 'Pupil_L_TmpJnt' ,
	# 								parent     = 'IrisSub_L_Jnt'  ,
	# 								ctrlGrp    =  main.mainCtrlGrp ,
	# 								shape      = 'circle' ,
	# 								side       = 'L' ,
	# 								size       = size  
	# 							)

	# print "# Generate >> Pupil Right Rig"
	# PupilRigObj_R = subRig.Run(
	# 								name       = 'Pupil' ,
	# 								tmpJnt     = 'Pupil_R_TmpJnt' ,
	# 								parent     = 'IrisSub_R_Jnt' ,
	# 								ctrlGrp    =  main.mainCtrlGrp ,
	# 								shape      = 'circle' ,
	# 								side       = 'R' ,
	# 								size       = size  
	# 							)

	# print "# Generate >> Hand Cup Inner Left"
	# handCupInnerL = fingerRig.HandCup( name       = 'HandCupInner' ,
	# 								handCupTmpJnt = 'HandCupInner_L_TmpJnt' ,
	# 								armCtrl       = armL.armCtrl ,
	# 								parent        = armL.handJnt ,
	# 								elem          = '' ,
	# 								side          = 'L' ,
	# 							)

	# print "# Generate >> Hand Cup Outer Left"
	# handCupOuterL = fingerRig.HandCup( name       = 'HandCupOuter' ,
	# 								handCupTmpJnt = 'HandCupOuter_L_TmpJnt' ,
	# 								armCtrl       = armL.armCtrl ,
	# 								parent        = armL.handJnt ,
	# 								elem          = '' ,
	# 								side          = 'L' ,
	# 							)

	# print "# Generate >> Thumb Left"
	# fngrThumbL = fingerRig.Run(         
	# 								fngr          = 'Thumb' ,
	# 								fngrTmpJnt    =['Thumb1_L_TmpJnt' , 
	# 												'Thumb2_L_TmpJnt' , 
	# 												'Thumb3_L_TmpJnt' , 
	# 												'Thumb4_L_TmpJnt' ] ,
	# 								parent        =  armL.handJnt ,
	# 								armCtrl       =  armL.armCtrl ,
	# 								innerCup      = armL.handJnt , 
	# 								outerCup      = '' ,
	# 								innerValue    = 1 , 
	# 								outerValue    = 0 ,
	# 								ctrlGrp       =  main.mainCtrlGrp ,
	# 								elem          = '' ,
	# 								side          = 'L' ,
	# 								size          = size 
	# 						 )

	# finger = 'Thumb'
	# ctrlShape = core.Dag(armL.armCtrl.shape)
	# ctrlShape.attr('%s2FistRx' %finger).value = -4.5
	# ctrlShape.attr('%s3FistRx' %finger).value = -9
	# ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
	# ctrlShape.attr('%s3SlideRx' %finger).value = -8
	# ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
	# ctrlShape.attr('%s3ScruchRx' %finger).value = -8

	print "# Generate >> Index Left"
	fngrIndexL = fingerRig.Run(         
									fngr          = 'Index' ,
									fngrTmpJnt    =['IndexFoot1_L_TmpJnt' , 
													'IndexFoot2_L_TmpJnt' , 
													'IndexFoot3_L_TmpJnt' , 
													'IndexFoot4_L_TmpJnt' ] ,
									parent        =  legL.ballJnt ,
									armCtrl       =  legL.legCtrl ,
									innerCup      = "" , 
									outerCup      = "" ,
									innerValue    = 1 , 
									outerValue    = 0 ,
									ctrlGrp       =  main.mainCtrlGrp ,
									elem          = '' ,
									side          = 'L' ,
									size          = size 
							 )

	finger = 'Index'
	ctrlShape = core.Dag(legL.legCtrl.shape)
	ctrlShape.attr('%s2FistRx' %finger).value = -9
	ctrlShape.attr('%s3FistRx' %finger).value = -9
	# ctrlShape.attr('%s4FistRx' %finger).value = -9
	ctrlShape.attr('%s2RelaxRx' %finger).value = -1
	ctrlShape.attr('%s3RelaxRx' %finger).value = -1
	# ctrlShape.attr('%s4RelaxRx' %finger).value = -1
	ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
	ctrlShape.attr('%s3SlideRx' %finger).value = -8
	# ctrlShape.attr('%s4SlideRx' %finger).value = 4.5
	ctrlShape.attr('%s1ScruchRx' %finger).value = 1.5
	ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
	ctrlShape.attr('%s3ScruchRx' %finger).value = -8
	# ctrlShape.attr('%s4ScruchRx' %finger).value = -8
	ctrlShape.attr('%sBaseSpread' %finger).value = -4
	ctrlShape.attr('%sSpread' %finger).value = -4
	ctrlShape.attr('%sBaseBreak' %finger).value = -2
	ctrlShape.attr('%sBreak' %finger).value = -2
	ctrlShape.attr('%sBaseFlex' %finger).value = -9
	ctrlShape.attr('%sFlex' %finger).value = -9

	print "# Generate >> Middle Left"
	fngrMiddleL = fingerRig.Run(         
									fngr          = 'Middle' ,
									fngrTmpJnt    =['MiddleFoot1_L_TmpJnt' , 
													'MiddleFoot2_L_TmpJnt' , 
													'MiddleFoot3_L_TmpJnt' , 
													'MiddleFoot4_L_TmpJnt' , 
													'MiddleFoot5_L_TmpJnt' ] ,
									parent        =  legL.ballJnt ,
									armCtrl       =  legL.legCtrl ,
									innerCup      = "" , 
									outerCup      = "" ,
									innerValue    = 1 , 
									outerValue    = 0.5 ,
									ctrlGrp       =  main.mainCtrlGrp ,
									elem          = '' ,
									side          = 'L' ,
									size          = size 
							   )

	finger = 'Middle'
	ctrlShape = core.Dag(legL.legCtrl.shape)
	ctrlShape.attr('%s2FistRx' %finger).value = -9
	ctrlShape.attr('%s3FistRx' %finger).value = -9
	ctrlShape.attr('%s4FistRx' %finger).value = -9
	ctrlShape.attr('%s1RelaxRx' %finger).value = -0.5
	ctrlShape.attr('%s2RelaxRx' %finger).value = -2.5
	ctrlShape.attr('%s3RelaxRx' %finger).value = -2.5
	ctrlShape.attr('%s4RelaxRx' %finger).value = -2.5
	ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
	ctrlShape.attr('%s3SlideRx' %finger).value = -8
	ctrlShape.attr('%s4SlideRx' %finger).value = 4.5
	ctrlShape.attr('%s1ScruchRx' %finger).value = 1.5
	ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
	ctrlShape.attr('%s3ScruchRx' %finger).value = -8
	ctrlShape.attr('%s4ScruchRx' %finger).value = -8
	ctrlShape.attr('%sBaseBreak' %finger).value = 0
	ctrlShape.attr('%sBreak' %finger).value = 0
	ctrlShape.attr('%sBaseFlex' %finger).value = -9
	ctrlShape.attr('%sFlex' %finger).value = -9

	print "# Generate >> Ring Left"
	fngrRingL = fingerRig.Run(         
									fngr          = 'Ring' ,
									fngrTmpJnt    =['RingFoot1_L_TmpJnt' , 
													'RingFoot2_L_TmpJnt' , 
													'RingFoot3_L_TmpJnt' , 
													'RingFoot4_L_TmpJnt' ] ,
									parent        =  legL.ballJnt ,
									armCtrl       =  legL.legCtrl ,
									innerCup      = "" , 
									outerCup      = "" ,
									innerValue    = 0.5 , 
									outerValue    = 1 ,
									ctrlGrp       =  main.mainCtrlGrp ,
									elem          = '' ,
									side          = 'L' ,
									size          = size 
							   )

	finger = 'Ring'
	ctrlShape = core.Dag(legL.legCtrl.shape)
	ctrlShape.attr('%s2FistRx' %finger).value = -9
	ctrlShape.attr('%s3FistRx' %finger).value = -9
	# ctrlShape.attr('%s4FistRx' %finger).value = -9
	ctrlShape.attr('%s1RelaxRx' %finger).value = -1
	ctrlShape.attr('%s2RelaxRx' %finger).value = -5
	ctrlShape.attr('%s3RelaxRx' %finger).value = -5
	# ctrlShape.attr('%s4RelaxRx' %finger).value = -5
	ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
	ctrlShape.attr('%s3SlideRx' %finger).value = -8
	# ctrlShape.attr('%s4SlideRx' %finger).value = 4.5
	ctrlShape.attr('%s1ScruchRx' %finger).value = 1.5
	ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
	ctrlShape.attr('%s3ScruchRx' %finger).value = -8
	# ctrlShape.attr('%s4ScruchRx' %finger).value = -8
	ctrlShape.attr('%sBaseSpread' %finger).value = 3
	ctrlShape.attr('%sSpread' %finger).value = 3
	ctrlShape.attr('%sBaseBreak' %finger).value = 2
	ctrlShape.attr('%sBreak' %finger).value = 2
	ctrlShape.attr('%sBaseFlex' %finger).value = -9
	ctrlShape.attr('%sFlex' %finger).value = -9

	print "# Generate >> Pinky Left"
	fngrPinkyL = fingerRig.Run(         
									fngr          = 'Pinky' ,
									fngrTmpJnt    =['PinkyFoot1_L_TmpJnt' , 
													'PinkyFoot2_L_TmpJnt' , 
													'PinkyFoot3_L_TmpJnt' , 
													'PinkyFoot4_L_TmpJnt' ] ,
									parent        =  legL.ankleJnt ,
									armCtrl       =  legL.legCtrl ,
									innerCup      = "" , 
									outerCup      = "" ,
									innerValue    = 0 , 
									outerValue    = 1 ,
									ctrlGrp       =  main.mainCtrlGrp ,
									elem          = '' ,
									side          = 'L' ,
									size          = size 
							   )

	finger = 'Pinky'
	ctrlShape = core.Dag(legL.legCtrl.shape)
	ctrlShape.attr('%s2FistRx' %finger).value = -9
	ctrlShape.attr('%s3FistRx' %finger).value = -9
	# ctrlShape.attr('%s4FistRx' %finger).value = -9
	ctrlShape.attr('%s1RelaxRx' %finger).value = -2
	ctrlShape.attr('%s2RelaxRx' %finger).value = -3
	ctrlShape.attr('%s3RelaxRx' %finger).value = -3
	# ctrlShape.attr('%s4RelaxRx' %finger).value = -3
	ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
	ctrlShape.attr('%s3SlideRx' %finger).value = -8
	# ctrlShape.attr('%s4SlideRx' %finger).value = 4.5
	ctrlShape.attr('%s1ScruchRx' %finger).value = 1.5
	ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
	ctrlShape.attr('%s3ScruchRx' %finger).value = -8
	# ctrlShape.attr('%s4ScruchRx' %finger).value = -8
	ctrlShape.attr('%sBaseSpread' %finger).value = 5
	ctrlShape.attr('%sSpread' %finger).value = 5
	ctrlShape.attr('%sBaseBreak' %finger).value = 4
	ctrlShape.attr('%sBreak' %finger).value = 4
	ctrlShape.attr('%sBaseFlex' %finger).value = -9
	ctrlShape.attr('%sFlex' %finger).value = -9

	# print "# Generate >> Hand Cup Inner Right"
	# handCupInnerR = fingerRig.HandCup( name       = 'HandCupInner' ,
	# 								handCupTmpJnt = 'HandCupInner_R_TmpJnt' ,
	# 								armCtrl       = armR.armCtrl ,
	# 								parent        = armR.handJnt ,
	# 								elem          = '' ,
	# 								side          = 'R' ,
	# 							)
								
	# print "# Generate >> Hand Cup Outer Right"
	# handCupOuterR = fingerRig.HandCup( name       = 'HandCupOuter' ,
	# 								handCupTmpJnt = 'HandCupOuter_R_TmpJnt' ,
	# 								armCtrl       = armR.armCtrl ,
	# 								parent        = armR.handJnt ,
	# 								elem          = '' ,
	# 								side          = 'R' ,
	# 							)

	# print "# Generate >> Thumb Right"
	# fngrThumbR = fingerRig.Run(         
	# 								fngr          = 'Thumb' ,
	# 								fngrTmpJnt    =['Thumb1_R_TmpJnt' , 
	# 												'Thumb2_R_TmpJnt' , 
	# 												'Thumb3_R_TmpJnt' , 
	# 												'Thumb4_R_TmpJnt' ] ,
	# 								parent        =  armR.handJnt ,
	# 								armCtrl       =  armR.armCtrl ,
	# 								innerCup      = armR.handJnt , 
	# 								outerCup      = '' ,
	# 								innerValue    = 1 , 
	# 								outerValue    = 0 ,
	# 								ctrlGrp       =  main.mainCtrlGrp ,
	# 								elem          = '' ,
	# 								side          = 'R' ,
	# 								size          = size 
	# 						 )

	# finger = 'Thumb'
	# ctrlShape = core.Dag(armR.armCtrl.shape)
	# ctrlShape.attr('%s2FistRx' %finger).value = -4.5
	# ctrlShape.attr('%s3FistRx' %finger).value = -9
	# ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
	# ctrlShape.attr('%s3SlideRx' %finger).value = -8
	# ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
	# ctrlShape.attr('%s3ScruchRx' %finger).value = -8
	
	print "# Generate >> Index Right"
	fngrIndexR = fingerRig.Run(         
									fngr          = 'Index' ,
									fngrTmpJnt    =['IndexFoot1_R_TmpJnt' , 
													'IndexFoot2_R_TmpJnt' , 
													'IndexFoot3_R_TmpJnt' , 
													'IndexFoot4_R_TmpJnt' ] ,
									parent        =  legR.ballJnt ,
									armCtrl       =  legR.legCtrl ,
									innerCup      = "" , 
									outerCup      = "" ,
									innerValue    = 1 , 
									outerValue    = 0 ,
									ctrlGrp       =  main.mainCtrlGrp ,
									elem          = '' ,
									side          = 'R' ,
									size          = size 
							 )

	finger = 'Index'
	ctrlShape = core.Dag(legR.legCtrl.shape)
	ctrlShape.attr('%s2FistRx' %finger).value = -9
	ctrlShape.attr('%s3FistRx' %finger).value = -9
	# ctrlShape.attr('%s4FistRx' %finger).value = -9
	ctrlShape.attr('%s2RelaxRx' %finger).value = -1
	ctrlShape.attr('%s3RelaxRx' %finger).value = -1
	# ctrlShape.attr('%s4RelaxRx' %finger).value = -1
	ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
	ctrlShape.attr('%s3SlideRx' %finger).value = -8
	# ctrlShape.attr('%s4SlideRx' %finger).value = 4.5
	ctrlShape.attr('%s1ScruchRx' %finger).value = 1.5
	ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
	ctrlShape.attr('%s3ScruchRx' %finger).value = -8
	# ctrlShape.attr('%s4ScruchRx' %finger).value = -8
	ctrlShape.attr('%sBaseSpread' %finger).value = -4
	ctrlShape.attr('%sSpread' %finger).value = -4
	ctrlShape.attr('%sBaseBreak' %finger).value = -2
	ctrlShape.attr('%sBreak' %finger).value = -2
	ctrlShape.attr('%sBaseFlex' %finger).value = -9
	ctrlShape.attr('%sFlex' %finger).value = -9
	
	print "# Generate >> Middle Right"
	fngrMiddleR = fingerRig.Run(         
									fngr          = 'Middle' ,
									fngrTmpJnt    =['MiddleFoot1_R_TmpJnt' , 
													'MiddleFoot2_R_TmpJnt' , 
													'MiddleFoot3_R_TmpJnt' , 
													'MiddleFoot4_R_TmpJnt' , 
													'MiddleFoot5_R_TmpJnt' ] ,
									parent        =  legR.ballJnt ,
									armCtrl       =  legR.legCtrl ,
									innerCup      = "" , 
									outerCup      = "" ,
									innerValue    = 1 , 
									outerValue    = 0.5 ,
									ctrlGrp       =  main.mainCtrlGrp ,
									elem          = '' ,
									side          = 'R' ,
									size          = size 
							   )

	finger = 'Middle'
	ctrlShape = core.Dag(legR.legCtrl.shape)
	ctrlShape.attr('%s2FistRx' %finger).value = -9
	ctrlShape.attr('%s3FistRx' %finger).value = -9
	ctrlShape.attr('%s4FistRx' %finger).value = -9
	ctrlShape.attr('%s1RelaxRx' %finger).value = -0.5
	ctrlShape.attr('%s2RelaxRx' %finger).value = -2.5
	ctrlShape.attr('%s3RelaxRx' %finger).value = -2.5
	ctrlShape.attr('%s4RelaxRx' %finger).value = -2.5
	ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
	ctrlShape.attr('%s3SlideRx' %finger).value = -8
	ctrlShape.attr('%s4SlideRx' %finger).value = 4.5
	ctrlShape.attr('%s1ScruchRx' %finger).value = 1.5
	ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
	ctrlShape.attr('%s3ScruchRx' %finger).value = -8
	ctrlShape.attr('%s4ScruchRx' %finger).value = -8
	ctrlShape.attr('%sBaseBreak' %finger).value = -0
	ctrlShape.attr('%sBreak' %finger).value = -0
	ctrlShape.attr('%sBaseFlex' %finger).value = -9
	ctrlShape.attr('%sFlex' %finger).value = -9

	print "# Generate >> Ring Right"
	fngrRingR = fingerRig.Run(         
									fngr          = 'Ring' ,
									fngrTmpJnt    =['RingFoot1_R_TmpJnt' , 
													'RingFoot2_R_TmpJnt' , 
													'RingFoot3_R_TmpJnt' , 
													'RingFoot4_R_TmpJnt' ] ,
									parent        =  legR.ballJnt ,
									armCtrl       =  legR.legCtrl ,
									innerCup      = "" , 
									outerCup      = "" ,
									innerValue    = 0.5 , 
									outerValue    = 1 ,
									ctrlGrp       =  main.mainCtrlGrp ,
									elem          = '' ,
									side          = 'R' ,
									size          = size 
							   )

	finger = 'Ring'
	ctrlShape = core.Dag(legR.legCtrl.shape)
	ctrlShape.attr('%s2FistRx' %finger).value = -9
	ctrlShape.attr('%s3FistRx' %finger).value = -9
	# ctrlShape.attr('%s4FistRx' %finger).value = -9
	ctrlShape.attr('%s1RelaxRx' %finger).value = -1
	ctrlShape.attr('%s2RelaxRx' %finger).value = -5
	ctrlShape.attr('%s3RelaxRx' %finger).value = -5
	# ctrlShape.attr('%s4RelaxRx' %finger).value = -5
	ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
	ctrlShape.attr('%s3SlideRx' %finger).value = -8
	# ctrlShape.attr('%s4SlideRx' %finger).value = 4.5
	ctrlShape.attr('%s1ScruchRx' %finger).value = 1.5
	ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
	ctrlShape.attr('%s3ScruchRx' %finger).value = -8
	# ctrlShape.attr('%s4ScruchRx' %finger).value = -8
	ctrlShape.attr('%sBaseSpread' %finger).value = 3
	ctrlShape.attr('%sSpread' %finger).value = 3
	ctrlShape.attr('%sBaseBreak' %finger).value = -2
	ctrlShape.attr('%sBreak' %finger).value = -2
	ctrlShape.attr('%sBaseFlex' %finger).value = -9
	ctrlShape.attr('%sFlex' %finger).value = -9
	
	print "# Generate >> Pinky Right"
	fngrPinkyR = fingerRig.Run(         
									fngr          = 'Pinky' ,
									fngrTmpJnt    =['PinkyFoot1_R_TmpJnt' , 
													'PinkyFoot2_R_TmpJnt' , 
													'PinkyFoot3_R_TmpJnt' , 
													'PinkyFoot4_R_TmpJnt' ] ,
									parent        =  legR.ankleJnt ,
									armCtrl       =  legR.legCtrl ,
									innerCup      = "" , 
									outerCup      = "" ,
									innerValue    = 0 , 
									outerValue    = 1 ,
									ctrlGrp       =  main.mainCtrlGrp ,
									elem          = '' ,
									side          = 'R' ,
									size          = size 
							   )
	
	finger = 'Pinky'
	ctrlShape = core.Dag(legR.legCtrl.shape)
	ctrlShape.attr('%s2FistRx' %finger).value = -9
	ctrlShape.attr('%s3FistRx' %finger).value = -9
	# ctrlShape.attr('%s4FistRx' %finger).value = -9
	ctrlShape.attr('%s1RelaxRx' %finger).value = -2
	ctrlShape.attr('%s2RelaxRx' %finger).value = -3
	ctrlShape.attr('%s3RelaxRx' %finger).value = -3
	# ctrlShape.attr('%s4RelaxRx' %finger).value = -3
	ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
	ctrlShape.attr('%s3SlideRx' %finger).value = -8
	# ctrlShape.attr('%s4SlideRx' %finger).value = 4.5
	ctrlShape.attr('%s1ScruchRx' %finger).value = 1.5
	ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
	ctrlShape.attr('%s3ScruchRx' %finger).value = -8
	# ctrlShape.attr('%s4ScruchRx' %finger).value = -8
	ctrlShape.attr('%sBaseSpread' %finger).value = 5
	ctrlShape.attr('%sSpread' %finger).value = 5
	ctrlShape.attr('%sBaseBreak' %finger).value = -4
	ctrlShape.attr('%sBreak' %finger).value = -4
	ctrlShape.attr('%sBaseFlex' %finger).value = -9
	ctrlShape.attr('%sFlex' %finger).value = -9
	
	print '##------------------------------------------------------------------------------------'
	print '##-----------------------------ADD RIG GROUP------------------------------------------'
	print '##------------------------------------------------------------------------------------'

	print "# Generate >> Tail"
	tail = subRig.Run(	name       = 'Tail' ,
                        tmpJnt     = 'Tail_TmpJnt' ,
                        parent     = torso.pelvisJnt ,
                        ctrlGrp    = main.ctrlGrp ,
                        shape      = 'cylinder' ,
                        side       = '' ,
                        size       = size )

	print "# Generate >> Tail Tip"
	tailTip = subRig.Run(	name       = 'TailTip' ,
	                        tmpJnt     = 'TailTip_Jnt' ,
	                        parent     = tail.jnt ,
	                        ctrlGrp    = main.ctrlGrp ,
	                        shape      = 'cube' ,
	                        side       = '' ,
	                        size       = size )

	print "# Generate >> Wing A Left"
	wingAL = subRig.Run(	name       = 'WingA' ,
	                        tmpJnt     = 'WingA1_L_TmpJnt' ,
	                        parent     = armL.forearmJnt ,
	                        ctrlGrp    = main.ctrlGrp ,
	                        shape      = 'stick' ,
	                        side       = 'L' ,
	                        size       = size )

	print "# Generate >> Wing B Left"
	wingBL = subRig.Run(	name       = 'WingB' ,
	                        tmpJnt     = 'WingB1_L_TmpJnt' ,
	                        parent     = armL.wristJnt ,
	                        ctrlGrp    = main.ctrlGrp ,
	                        shape      = 'stick' ,
	                        side       = 'L' ,
	                        size       = size )

	print "# Generate >> Wing C Left"
	wingCL = subRig.Run(	name       = 'WingC' ,
	                        tmpJnt     = 'WingC1_L_TmpJnt' ,
	                        parent     = armL.handJnt ,
	                        ctrlGrp    = main.ctrlGrp ,
	                        shape      = 'stick' ,
	                        side       = 'L' ,
	                        size       = size )

	print "# Generate >> Wing A Right"
	wingAR = subRig.Run(	name       = 'WingA' ,
	                        tmpJnt     = 'WingA1_R_TmpJnt' ,
	                        parent     = armR.forearmJnt ,
	                        ctrlGrp    = main.ctrlGrp ,
	                        shape      = 'stick' ,
	                        side       = 'R' ,
	                        size       = size )

	print "# Generate >> Wing B Right"
	wingBR = subRig.Run(	name       = 'WingB' ,
	                        tmpJnt     = 'WingB1_R_TmpJnt' ,
	                        parent     = armR.wristJnt ,
	                        ctrlGrp    = main.ctrlGrp ,
	                        shape      = 'stick' ,
	                        side       = 'R' ,
	                        size       = size )

	print "# Generate >> Wing C Right"
	wingCR = subRig.Run(	name       = 'WingC' ,
	                        tmpJnt     = 'WingC1_R_TmpJnt' ,
	                        parent     = armR.handJnt ,
	                        ctrlGrp    = main.ctrlGrp ,
	                        shape      = 'stick' ,
	                        side       = 'R' ,
	                        size       = size )

	print "# Generate >> Wing Detail A Left"
	wingDtlAL = subRig.Run(	name       = 'WingDtlA' ,
	                        tmpJnt     = 'WingA2_L_TmpJnt' ,
	                        parent     = wingAL.jnt ,
	                        ctrlGrp    = main.ctrlGrp ,
	                        shape      = 'cube' ,
	                        side       = 'L' ,
	                        size       = size )

	print "# Generate >> Wing Detail B Left"
	wingDtlBL = subRig.Run(	name       = 'WingDtlB' ,
	                        tmpJnt     = 'WingB2_L_TmpJnt' ,
	                        parent     = wingBL.jnt ,
	                        ctrlGrp    = main.ctrlGrp ,
	                        shape      = 'cube' ,
	                        side       = 'L' ,
	                        size       = size )

	print "# Generate >> Wing Detail C Left"
	wingDtlCL = subRig.Run(	name       = 'WingDtlC' ,
	                        tmpJnt     = 'WingC2_L_TmpJnt' ,
	                        parent     = wingCL.jnt ,
	                        ctrlGrp    = main.ctrlGrp ,
	                        shape      = 'cube' ,
	                        side       = 'L' ,
	                        size       = size )

	print "# Generate >> Wing Detail A Right"
	wingDtlAR = subRig.Run(	name       = 'WingDtlA' ,
	                        tmpJnt     = 'WingA2_R_TmpJnt' ,
	                        parent     = wingAR.jnt ,
	                        ctrlGrp    = main.ctrlGrp ,
	                        shape      = 'cube' ,
	                        side       = 'R' ,
	                        size       = size )

	print "# Generate >> Wing Detail B Right"
	wingDtlBR = subRig.Run(	name       = 'WingDtlB' ,
	                        tmpJnt     = 'WingB2_R_TmpJnt' ,
	                        parent     = wingBR.jnt ,
	                        ctrlGrp    = main.ctrlGrp ,
	                        shape      = 'cube' ,
	                        side       = 'R' ,
	                        size       = size )

	print "# Generate >> Wing Detail C Right"
	wingDtlCR = subRig.Run(	name       = 'WingDtlC' ,
	                        tmpJnt     = 'WingC2_R_TmpJnt' ,
	                        parent     = wingCR.jnt ,
	                        ctrlGrp    = main.ctrlGrp ,
	                        shape      = 'cube' ,
	                        side       = 'R' ,
	                        size       = size )


	print "# Generate >> BtwJnt"
	proc.btwJnt(jnts=['Neck5RbnDtl_Jnt', 'Head_Jnt', 'HeadEnd_Jnt'], axis='y', pointConstraint=True, side="")
	proc.btwJnt(jnts=['Spine5_Jnt', 'Neck_Jnt', 'Neck1RbnDtl_Jnt'], axis='y', pointConstraint=True, side="")
	
	proc.btwJnt(jnts=['Spine5_Jnt', 'Clav_L_Jnt', 'UpArm_L_Jnt'],axis='y', pointConstraint=True, side="L")
	proc.btwJnt(jnts=['Spine5_Jnt', 'Clav_R_Jnt', 'UpArm_R_Jnt'],axis='y', pointConstraint=True, side="R")
	proc.btwJnt(jnts=['ClavEnd_L_Jnt', 'UpArm_L_Jnt', 'UpArm1RbnDtl_L_Jnt'], axis='y', pointConstraint=True, side="L")
	proc.btwJnt(jnts=['ClavEnd_R_Jnt', 'UpArm_R_Jnt', 'UpArm1RbnDtl_R_Jnt'], axis='y', pointConstraint=True, side="R")
	proc.btwJnt(jnts=['ClavEnd_L_Jnt', 'UpArm_L_Jnt', 'UpArm1RbnDtl_L_Jnt'], axis='y', pointConstraint=True, elem="ArmPit", side="L")
	proc.btwJnt(jnts=['ClavEnd_R_Jnt', 'UpArm_R_Jnt', 'UpArm1RbnDtl_R_Jnt'], axis='y', pointConstraint=True, elem="ArmPit", side="R")

	proc.btwJnt(jnts=['UpArm5RbnDtl_L_Jnt', 'Forearm_L_Jnt', 'Forearm1RbnDtl_L_Jnt'], axis='y', pointConstraint=False, side="L")
	proc.btwJnt(jnts=['UpArm5RbnDtl_R_Jnt', 'Forearm_R_Jnt', 'Forearm1RbnDtl_R_Jnt'], axis='y', pointConstraint=False, side="R")
	proc.btwJnt(jnts=['Forearm5RbnDtl_L_Jnt', 'Wrist_L_Jnt', 'Hand_L_Jnt'], axis='y', pointConstraint=True, side="L")
	proc.btwJnt(jnts=['Forearm5RbnDtl_R_Jnt', 'Wrist_R_Jnt', 'Hand_R_Jnt'], axis='y', pointConstraint=True, side="R")

	proc.btwJnt(jnts=['Pelvis_Jnt', 'UpLeg_L_Jnt', 'UpLeg1RbnDtl_L_Jnt'], axis='y', pointConstraint=True, side="L")
	proc.btwJnt(jnts=['Pelvis_Jnt', 'UpLeg_R_Jnt', 'UpLeg1RbnDtl_R_Jnt'], axis='y', pointConstraint=True, side="R")
	proc.btwJnt(jnts=['UpLeg5RbnDtl_L_Jnt', 'LowLeg_L_Jnt', 'LowLeg1RbnDtl_L_Jnt'], axis='y', pointConstraint=False, side="L")
	proc.btwJnt(jnts=['UpLeg5RbnDtl_R_Jnt', 'LowLeg_R_Jnt', 'LowLeg1RbnDtl_R_Jnt'], axis='y', pointConstraint=False, side="R")
	proc.btwJnt(jnts=['LowLeg5RbnDtl_L_Jnt', 'Ankle_L_Jnt', 'Ball_L_Jnt'], axis='y', pointConstraint=True, side="L")
	proc.btwJnt(jnts=['LowLeg5RbnDtl_R_Jnt', 'Ankle_R_Jnt', 'Ball_R_Jnt'], axis='y', pointConstraint=True, side="R")

	proc.btwJnt(jnts=['Index1_L_Jnt', 'Index2_L_Jnt', 'Index3_L_Jnt'], axis='y', pointConstraint=True, side="L")
	proc.btwJnt(jnts=['Index1_R_Jnt', 'Index2_R_Jnt', 'Index3_R_Jnt'], axis='y', pointConstraint=True, side="R")
	proc.btwJnt(jnts=['Middle1_L_Jnt', 'Middle2_L_Jnt', 'Middle3_L_Jnt'], axis='y', pointConstraint=True, side="L")
	proc.btwJnt(jnts=['Middle1_R_Jnt', 'Middle2_R_Jnt', 'Middle3_R_Jnt'], axis='y', pointConstraint=True, side="R")
	proc.btwJnt(jnts=['Middle2_L_Jnt', 'Middle3_L_Jnt', 'Middle4_L_Jnt'], axis='y', pointConstraint=True, side="L")
	proc.btwJnt(jnts=['Middle2_R_Jnt', 'Middle3_R_Jnt', 'Middle4_R_Jnt'], axis='y', pointConstraint=True, side="R")
	proc.btwJnt(jnts=['Ring1_L_Jnt', 'Ring2_L_Jnt', 'Ring3_L_Jnt'], axis='y', pointConstraint=True, side="L")
	proc.btwJnt(jnts=['Ring1_R_Jnt', 'Ring2_R_Jnt', 'Ring3_R_Jnt'], axis='y', pointConstraint=True, side="R")
	proc.btwJnt(jnts=['Pinky1_L_Jnt', 'Pinky2_L_Jnt', 'Pinky3_L_Jnt'], axis='y', pointConstraint=True, side="L")
	proc.btwJnt(jnts=['Pinky1_R_Jnt', 'Pinky2_R_Jnt', 'Pinky3_R_Jnt'], axis='y', pointConstraint=True, side="R")

##----------------------------------------------------Clavicle Muscle Rig------------------------------------------
##----------------------------------------------------Clavicle Muscle Rig------------------------------------------
	'''
	print "# Generate >> Clavicle Muscle Rig Left"
	clavMusLObj = clavMuscleRig.clavMuscleRig(	clavMusSpineRoot  = 'SpineMusRoot_Jnt',
												elem            = 'clavMus',
												side            = 'L',
												men 			= False,
												ctrlGrp         = main.addRigCtrlGrp ,
												skinGrp         = main.skinGrp ,
												jntGrp          = main.jntGrp ,
												ikhGrp          = main.ikhGrp ,
												stillGrp        = main.stillGrp )


	print "# Generate >> Clavicle Muscle Rig Right"
	clavMusRObj = clavMuscleRig.clavMuscleRig(	clavMusSpineRoot  = 'SpineMusRoot_Jnt',
												elem            = 'clavMus',
												side            = 'R',
												men 			= False,
												ctrlGrp         = main.addRigCtrlGrp ,
												skinGrp         = main.skinGrp ,
												jntGrp          = main.jntGrp ,
												ikhGrp          = main.ikhGrp ,
												stillGrp        = main.stillGrp )
	'''
	# print "# Generate >> Fix some node"
	# mc.setAttr("pec1BindCtrlOfst_L_Grp_pointConstraint1.Spine5_JntW1", 0)
	# mc.setAttr("pec1BindCtrlOfst_L_Grp_pointConstraint1.clavMus2Trash_L_JntW0", 0)
	# mc.setAttr("pec1BindCtrlOfst_R_Grp_pointConstraint1.Spine5_JntW1", 0)
	# mc.setAttr("pec1BindCtrlOfst_R_Grp_pointConstraint1.clavMus2Trash_R_JntW0", 0)

	# print "# Generate >> Chest Driver X Rig L"
	# clavMuscleRig.generateSetRange(	ctrl = 'pec1Bind_L_Ctrl',
 #                                side = 'L',
 #                                elem = 'MilkInOut',
 #                                driver = 'clavMus1Trash_L_Jnt', 
 #                                driverAxis = 'rotateY', 
 #                                driven = 'pec1BindCtrlZro_L_Grp',
 #                                drivenAxis = 'translateX')

	# print "# Generate >> Chest Driver X Rig R"
	# clavMuscleRig.generateSetRange(	ctrl = 'pec1Bind_R_Ctrl',
 #                                side = 'R',
 #                                elem = 'MilkInOut',
 #                                driver = 'clavMus1Trash_R_Jnt', 
 #                                driverAxis = 'rotateY', 
 #                                driven = 'pec1BindCtrlZro_R_Grp',
 #                                drivenAxis = 'translateX')
	# print "# Generate >> Chest Driver Z Rig L"
	# clavMuscleRig.generateSetRange(	ctrl = 'Breast1_L_Ctrl',
 #                                side = 'L',
 #                                elem = 'MilkUpDn',
 #                                driver = 'clavMus1Trash_L_Jnt', 
 #                                driverAxis = 'rotateZ', 
 #                                driven = 'Breast1CtrlOfst_L_Grp',
 #                                drivenAxis = 'translateZ')
	# print "# Generate >> Chest Driver Z Rig R"
	# clavMuscleRig.generateSetRange(	ctrl = 'Breast1_R_Ctrl',
 #                                side = 'R',
 #                                elem = 'MilkUpDn',
 #                                driver = 'clavMus1Trash_R_Jnt', 
 #                                driverAxis = 'rotateZ', 
 #                                driven = 'Breast1CtrlOfst_R_Grp',
 #                                drivenAxis = 'translateZ')

##---------------------------------------set preferred ankle Leg Ik --------------------------------------
##---------------------------------------set preferred ankle Leg Ik --------------------------------------
	print "# Generate >> Set preferred ankle Leg Ik"
	utaCore.preferredAnkleLegIk()


	print '##------------------------------------------------------------------------------------'
	print '##-----------------------------CLEAR UTILITIE-----------------------------------------'
	print '##------------------------------------------------------------------------------------'

	print "# Generate >> Read Control Shape"
	ctrlData.read_ctrl_dataFld()

	print "# Generate >> Character Text Name"
	charTextName.charTextName(value = '5', size = '1')

	print "# Generate >> Set Defalut Attribute"
	jnt = mc.ls(typ="joint")
	for i in jnt:
		if not mc.getAttr(i + ".radi", lock=True):
			mc.setAttr(i + ".radi", 0.01)

	# mc.setAttr("NeckRbnCtrlZro_Grp.v", 0)
	mc.move(0, 0.901272, 0.0813683, "FootSmartIkCtrlZro_L_Grp", os=True, r=True)
	mc.move(0, 0.901272, -0.0813683, "FootSmartIkCtrlZro_R_Grp", os=True, r=True)

	mc.delete("barnOwlCrv_Grp_parentConstraint1", "barnOwlCrv_Grp_scaleConstraint1")
	mc.move(0, 0, 0.965516, "barnOwlCrv_Grp", ws=True, r=True)
	mc.parentConstraint("HeadEnd_Jnt", "barnOwlCrv_Grp", mo=True)
	mc.scaleConstraint("HeadEnd_Jnt", "barnOwlCrv_Grp", mo=True)

	wing = [wingAL, wingBL, wingAR, wingBR, "PinkyCtrl_L_Grp", "PinkyCtrl_R_Grp", "Pinky1CtrlZro_L_Grp", "Pinky1CtrlZro_R_Grp"]
	attr, axis = "trs", "xyz"
	for obj in wing:
		for at in attr:
			for ax in axis:
				try:
					mc.setAttr("{}.{}{}".format(obj.grp, at, ax), l=False)
				except:
					mc.setAttr("{}.{}{}".format(obj, at, ax), l=False)

	mc.parentConstraint(armL.upArmJnt, wingAL.grp, mo=True)
	mc.parentConstraint(armL.wristJnt, wingAL.grp, mo=True)
	mc.parentConstraint(armL.handJnt, wingBL.grp, mo=True)

	mc.parentConstraint(armL.upArmJnt, wingAL.grp, mo=True)
	mc.parentConstraint(armR.wristJnt, wingAR.grp, mo=True)
	mc.parentConstraint(armR.handJnt, wingBR.grp, mo=True)

	rigTools.localWorld( obj = 'Pinky1_L_Ctrl' , worldObj = main.ctrlGrp , localObj = 'PinkyCtrl_L_Grp' , consGrp = 'Pinky1CtrlZro_L_Grp' , type = 'orient' )
	rigTools.localWorld( obj = 'Pinky1_R_Ctrl' , worldObj = main.ctrlGrp , localObj = 'PinkyCtrl_R_Grp' , consGrp = 'Pinky1CtrlZro_R_Grp' , type = 'orient' )

	mc.setAttr("UpArmFk_L_Ctrl.localWorld", 0)
	mc.setAttr("UpArmFk_R_Ctrl.localWorld", 0)
	mc.setAttr("WristIk_L_Ctrl.spaceFollow", 1)
	mc.setAttr("WristIk_R_Ctrl.spaceFollow", 1)

	for obj in wing:
		for at in attr:
			for ax in axis:
				try:
					mc.setAttr("{}.{}{}".format(obj.grp, at, ax), l=True)
				except:
					mc.setAttr("{}.{}{}".format(obj, at, ax), l=True)

	wingTipLMdv = mc.createNode("multiplyDivide", n="WingTip_L_Mdv")
	mc.setAttr(wingTipLMdv + ".i2", 1.5, 1, 1)
	mc.connectAttr(armL.wristJnt.getName() + ".r", wingTipLMdv + ".i1")
	mc.connectAttr(wingTipLMdv + ".o", armL.handJnt.getName() + ".r")

	wingTipRMdv = mc.createNode("multiplyDivide", n="WingTip_R_Mdv")
	mc.setAttr(wingTipRMdv + ".i2", 1.5, 1, 1)
	mc.connectAttr(armR.wristJnt.getName() + ".r", wingTipRMdv + ".i1")
	mc.connectAttr(wingTipRMdv + ".o", armR.handJnt.getName() + ".r")

	print "# Generate >> Fix Smart Amp !! "
	side = ["L", "R"]
	for si in side:
		obj = ["InIkPiv_{}_Grp".format(si), "OutIkPiv_{}_Grp".format(si)]
		for grp in obj:
			mc.setAttr("{}.ry".format(grp), l=False)	

		mc.disconnectAttr("FootSmartIk_{}_Ctrl.tx".format(si), "FootRockSmartIk_{}_Pma.i1[0]".format(si))
		mc.connectAttr("FootSmartIk_{}_Ctrl.tx".format(si), "FootSmartIk_{}_Mdv.i1y".format(si), f=True)
		mc.connectAttr("FootSmartIk_{}_Mdv.oy".format(si), "FootRockSmartIk_{}_Pma.i1[0]".format(si))
		mc.connectAttr("FootRockSmartIk_{}_Pma.o1".format(si), "InIkPiv_{}_Grp.ry".format(si), f=True)
		mc.connectAttr("FootRockSmartIk_{}_Pma.o1".format(si), "OutIkPiv_{}_Grp.ry".format(si), f=True)

		for grp in obj:
			mc.setAttr("{}.ry".format(grp), l=True)

	print "# Generate >> DONE !! "