def selectControlAmp(process = '',*args):
    if process == 'eyeRig':
        print '# Generate >> ::', process , '::'
        return eyeRigControlAmp()  
    # elif process == 'ebRig':
    #     print '# Generate >> ::', process , '::'
    #     return masterToadControlVis()  


def eyeRigControlAmp (*args):
	## Tital Ctrl Attr -----------------------------------------------------
	titalListAttr = 'EyeRigLowAmp'

	listAttrL = [
				'EyeRigLowAmp_L_Mul1', 
				'EyeRigUpAmp_L_Mul1', 
				]
	listAttrR = [
				'EyeRigLowAmp_R_Mul1', 
				'EyeRigUpAmp_R_Mul1', 
				]
	eyeRigAmp = [-4, 4]
	## EyeRigLowAmp = -4
	## EyeRigUpAmp = 4
	return titalListAttr, listAttrL, listAttrR, eyeRigAmp
