import maya.cmds as mc
reload(mc)
def tailScale (*args):
	ctrlList = mc.ls('TailIk*_Ctrl')
	jntLists = mc.ls('TailIk*_Jnt')
	jntRZLists = mc.ls('TailIkRZ*_Jnt')
	# print jntRZLists
	lists = [a.replace('_Crv', '') for a in ctrlList]
	nameLists = lists[0]
	# print ctrlList

	ikCtrlList = []
	for ctrlLists in ctrlList:
	    if'Gmbl' in ctrlLists:
	        pass
	    else:
	        ikCtrlList.append(ctrlLists)


	jntCount = [5, 9, 13, 17, 21, 25, 29, 33, 37, 41, 45, 49, 53, 57, 61, 65, 69, 73, 77, 81, 85, 89, 93, 97]

	attrs = ['X', 'Y', 'Z']
	setNode = [0.75, 0.5, 0.25]
	ii= 0
	for ikCtrlLists in ikCtrlList:
		for each in attrs:
			mc.connectAttr ('%s.scale%s' % (ikCtrlLists, each), '%s.scale%s' % (jntRZLists[ii+4], each))
			# print ii
		ii+=4
	iii = 1
	x = 4
	for ctrlLists in ctrlList:
		i=0
		for each in range(0, 3):
			mdvNode = mc.createNode ('multiplyDivide', n =('TailIk%s%s_Mdv' % (iii, attrs[i])))
			mc.setAttr('%s.operation' % mdvNode, 3)
			pmaNode = mc.createNode ('plusMinusAverage', n =('TailIk%s%s_Pma' % (iii,attrs[i])))

			mdv2Node = mc.createNode ( 'multiplyDivide', n = ('TailIk2%s%s_Mdv' % (iii, attrs[i])))
			mc.setAttr('%s.operation' % mdv2Node, 2)
			mdv3Node = mc.createNode ( 'multiplyDivide', n = ('TailIk3%s%s_Mdv' % (iii, attrs[i])))
			for each in attrs:
				# print i
				mc.setAttr('%s.input2%s' % (mdvNode, each),setNode[i])
				mc.connectAttr('%s.scale%s' % (ctrlLists, each), '%s.input1%s' % (mdvNode, each))
				mc.connectAttr('%s.output%s' % (mdvNode, each), '%s.input3D[%s].input3D%s' % (pmaNode, i, each.lower()))
				# Set Attr mdv2Node
				mc.setAttr('%s.input2%s' % (mdv2Node, each),2)
				# Connect Pma To Mdv2
				mc.connectAttr('%s.output3D%s' % (pmaNode, each.lower()), '%s.input1%s' % (mdv2Node, each))
				mc.connectAttr('%s.output%s' % (mdv2Node, each), '%s.input1%s' % (mdv3Node, each))
		

				#Connect to JntScale
				ix=0
				for xx in jntCount:
					if x+1 == xx:
						pass
					else:
						mc.connectAttr('%s.output%s' % (mdv3Node, each), '%s.scale%s' % (jntRZLists[x+1], each))
				ix+=1
				# print mdv3Node
				# print jntRZLists[x+1]
			x+=1
			i+=1
		iii+=4	
	# tailLoop = [1, 2, 3,-4, -5, -6]
	# ii= 0
	# for ikCtrlLists in ikCtrlList:
	# 	for each in attrs:
	# 		mc.connectAttr ('%s.output%s' % (mdv3Node, each),, '%s.scale%s' % (jntRZLists[ii+4], each))
	# 		print ii
	# 	ii+=4

	# jntCount = [5, 9, 13, 17, 21, 25, 29, 33, 37, 41, 45, 49, 53, 57, 61, 65, 69, 73, 77, 81, 85, 89, 93, 97]
	# tailLoop = [-3,-2, -1, 0, 1, 2, 3]
	# attrs = ['X', 'Y', 'Z']
	# x = 1
	# for jntCounts in jntCount:
	# 	for each in attrs:
	# 		i=0
	# 		for piect in range(0, 3):
	# 			# for eachh in attrs:
	# 			if jntCounts == 5:
	# 				pass
	# 			else:
	# 				# mc.connectAttr('TailIk%s%s_Mdv.output%s'% (jntCounts, each, each), ('TailIk%s%s_Pma.input3D[1].input3D%s' % (jntCount[x-1],each, attrs[i].lower())))
	# 				print jntCounts, each, each,
	# 				print jntCount[x-1], each, attrs[i].lower()
			
	# 				x+=1
	# 			i+=1
	



	# mc.connectAttr ('TailIk13X_Mdv.outputX', 'TailIk5X_Pma.input3D[1].input3Dx')
	# mc.connectAttr ('TailIk13X_Mdv.outputY', 'TailIk5X_Pma.input3D[1].input3Dy')
	# mc.connectAttr ('TailIk13X_Mdv.outputZ', 'TailIk5X_Pma.input3D[1].input3Dz')

	# mc.connectAttr ('TailIk13Y_Mdv.outputX', 'TailIk5Y_Pma.input3D[1].input3Dx')
	# mc.connectAttr ('TailIk13Y_Mdv.outputY', 'TailIk5Y_Pma.input3D[1].input3Dy')
	# mc.connectAttr ('TailIk13Y_Mdv.outputZ', 'TailIk5Y_Pma.input3D[1].input3Dz')

	# mc.connectAttr ('TailIk13Z_Mdv.outputX', 'TailIk5Z_Pma.input3D[1].input3Dx')
	# mc.connectAttr ('TailIk13Z_Mdv.outputY', 'TailIk5Z_Pma.input3D[1].input3Dy')
	# mc.connectAttr ('TailIk13Z_Mdv.outputZ', 'TailIk5Z_Pma.input3D[1].input3Dz')

# # TailFk_2_Ctrl
# 	jntCount = [5, 9, 13, 17, 21, 25, 29, 33, 37, 41, 45, 49, 53, 57, 61, 65, 69, 73, 77, 81, 85, 89, 93, 97]
# 	x = 1
# 	for jntCounts in jntCount:
# 		mc.connectAttr ('TailIk%sX_Mdv.outputX'% jntCount[x+1], 'TailIk%sX_Pma.input3D[1].input3Dx'% jntCounts)
# 		mc.connectAttr ('TailIk%sX_Mdv.outputY'% jntCount[x+1], 'TailIk%sX_Pma.input3D[1].input3Dy'% jntCounts)
# 		mc.connectAttr ('TailIk%sX_Mdv.outputZ'% jntCount[x+1], 'TailIk%sX_Pma.input3D[1].input3Dz'% jntCounts)

# 		mc.connectAttr ('TailIk%sY_Mdv.outputX'% jntCount[x+1], 'TailIk%sY_Pma.input3D[2].input3Dx'% jntCounts)
# 		mc.connectAttr ('TailIk%sY_Mdv.outputY'% jntCount[x+1], 'TailIk%sY_Pma.input3D[2].input3Dy'% jntCounts)
# 		mc.connectAttr ('TailIk%sY_Mdv.outputZ'% jntCount[x+1], 'TailIk%sY_Pma.input3D[2].input3Dz'% jntCounts)

# 		mc.connectAttr ('TailIk%sZ_Mdv.outputX'% jntCount[x+1], 'TailIk%sZ_Pma.input3D[3].input3Dx'% jntCounts)
# 		mc.connectAttr ('TailIk%sZ_Mdv.outputY'% jntCount[x+1], 'TailIk%sZ_Pma.input3D[3].input3Dy'% jntCounts)
# 		mc.connectAttr ('TailIk%sZ_Mdv.outputZ'% jntCount[x+1], 'TailIk%sZ_Pma.input3D[3].input3Dz'% jntCounts)
# 		x+=1

		# connectAttr -f TailIk35Z_Mdv.outputX TailIkRZ6_Jnt.scaleX;
		# connectAttr -f TailIk35Z_Mdv.outputY TailIkRZ6_Jnt.scaleY;
		# connectAttr -f TailIk35Z_Mdv.outputZ TailIkRZ6_Jnt.scaleZ;

		# connectAttr -f TailIk35Y_Mdv.outputX TailIkRZ7_Jnt.scaleX;
		# connectAttr -f TailIk35Y_Mdv.outputY TailIkRZ7_Jnt.scaleY;
		# connectAttr -f TailIk35Y_Mdv.outputZ TailIkRZ7_Jnt.scaleZ;

		# connectAttr -f TailIk35X_Mdv.outputX TailIkRZ8_Jnt.scaleX;
		# connectAttr -f TailIk35X_Mdv.outputY TailIkRZ8_Jnt.scaleY;
		# connectAttr -f TailIk35X_Mdv.outputZ TailIkRZ8_Jnt.scaleZ;