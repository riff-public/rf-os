import maya.cmds as mc
from  utaTools.utapy import utaCore
reload(utaCore)

def proxyBindSkin(*args):

	# HairGeo = ['Hair_Geo']
	# HairJnt = ['ctrl_md:HairRootSub_Jnt']

	# EyeGeo = ['EyeGeo_L_Grp', 'EyeGeo_R_Grp']
	# EyeJnt= ['ctrl_md:EyeLFT_Jnt', 'ctrl_md:EyeRGT_Jnt']

	# HeadGeo = ['Eyelash_Geo', 'Eyebrows_Geo', 'Tongue_Geo']
	# HeadJnt = ['ctrl_md:Head_Jnt']

	# BodyGeo = ['Body_Geo']
	BodyJnt = ['ctrl_md:HairRootSub_Jnt',
				'ctrl_md:EyeLFT_Jnt', 
				'ctrl_md:EyeRGT_Jnt',
				'ctrl_md:Root_Jnt',
				'ctrl_md:Pelvis_Jnt',
				'ctrl_md:UpLeg1RbnDtl_L_Jnt',
				'ctrl_md:UpLeg2RbnDtl_L_Jnt',
				'ctrl_md:UpLeg3RbnDtl_L_Jnt',
				'ctrl_md:UpLeg4RbnDtl_L_Jnt',
				'ctrl_md:UpLeg5RbnDtl_L_Jnt',
				'ctrl_md:LowLeg1RbnDtl_L_Jnt',
				'ctrl_md:LowLeg2RbnDtl_L_Jnt',
				'ctrl_md:LowLeg3RbnDtl_L_Jnt',
				'ctrl_md:LowLeg4RbnDtl_L_Jnt',
				'ctrl_md:LowLeg5RbnDtl_L_Jnt',
				'ctrl_md:Ankle_L_Jnt',
				'ctrl_md:MiddleToe1_L_Jnt',
				'ctrl_md:MiddleToe2_L_Jnt',
				'ctrl_md:MiddleToe3_L_Jnt',
				'ctrl_md:IndexToe1_L_Jnt',
				'ctrl_md:IndexToe2_L_Jnt',
				'ctrl_md:IndexToe3_L_Jnt',

				'ctrl_md:UpLeg1RbnDtl_R_Jnt',
				'ctrl_md:UpLeg2RbnDtl_R_Jnt',
				'ctrl_md:UpLeg3RbnDtl_R_Jnt',
				'ctrl_md:UpLeg4RbnDtl_R_Jnt',
				'ctrl_md:UpLeg5RbnDtl_R_Jnt',
				'ctrl_md:LowLeg1RbnDtl_R_Jnt',
				'ctrl_md:LowLeg2RbnDtl_R_Jnt',
				'ctrl_md:LowLeg3RbnDtl_R_Jnt',
				'ctrl_md:LowLeg4RbnDtl_R_Jnt',
				'ctrl_md:LowLeg5RbnDtl_R_Jnt',
				'ctrl_md:Ankle_R_Jnt',
				'ctrl_md:MiddleToe1_R_Jnt',
				'ctrl_md:MiddleToe2_R_Jnt',
				'ctrl_md:MiddleToe3_R_Jnt',
				'ctrl_md:IndexToe1_R_Jnt',
				'ctrl_md:IndexToe2_R_Jnt',
				'ctrl_md:IndexToe3_R_Jnt',

				'ctrl_md:Spine1Brth_Jnt',
				'ctrl_md:Spine1Sca_Jnt',
				'ctrl_md:Spine5Brth_Jnt',
				'ctrl_md:Spine5Sca_Jnt',
				'ctrl_md:Spine4Brth_Jnt',
				'ctrl_md:Spine4Sca_Jnt',
				'ctrl_md:Spine3Brth_Jnt',
				'ctrl_md:Spine3Sca_Jnt',
				'ctrl_md:Spine2Brth_Jnt',
				'ctrl_md:Spine2Sca_Jnt',

				'ctrl_md:Clav_L_Jnt',
				'ctrl_md:UpArm1RbnDtl_L_Jnt',
				'ctrl_md:UpArm2RbnDtl_L_Jnt',
				'ctrl_md:UpArm3RbnDtl_L_Jnt',
				'ctrl_md:UpArm4RbnDtl_L_Jnt',
				'ctrl_md:UpArm5RbnDtl_L_Jnt',
				'ctrl_md:Forearm1RbnDtl_L_Jnt',
				'ctrl_md:Forearm2RbnDtl_L_Jnt',
				'ctrl_md:Forearm3RbnDtl_L_Jnt',
				'ctrl_md:Forearm4RbnDtl_L_Jnt',
				'ctrl_md:Forearm5RbnDtl_L_Jnt',

				'ctrl_md:Hand_L_Jnt',
				'ctrl_md:Thumb1_L_Jnt',
				'ctrl_md:Thumb2_L_Jnt',
				'ctrl_md:Thumb3_L_Jnt',
				'ctrl_md:Thumb4_L_Jnt',
				'ctrl_md:Index1_L_Jnt',
				'ctrl_md:Index2_L_Jnt',
				'ctrl_md:Index3_L_Jnt',
				'ctrl_md:Index4_L_Jnt',
				'ctrl_md:Index5_L_Jnt',

				'ctrl_md:Middle1_L_Jnt',
				'ctrl_md:Middle2_L_Jnt',
				'ctrl_md:Middle3_L_Jnt',
				'ctrl_md:Middle4_L_Jnt',
				'ctrl_md:Middle5_L_Jnt',
				'ctrl_md:Ring1_L_Jnt',
				'ctrl_md:Ring2_L_Jnt',
				'ctrl_md:Ring3_L_Jnt',
				'ctrl_md:Ring4_L_Jnt',
				'ctrl_md:Ring5_L_Jnt',
				'ctrl_md:Pinky1_L_Jnt',
				'ctrl_md:Pinky2_L_Jnt',
				'ctrl_md:Pinky3_L_Jnt',
				'ctrl_md:Pinky4_L_Jnt',
				'ctrl_md:Pinky5_L_Jnt',

				'ctrl_md:Clav_R_Jnt',
				'ctrl_md:UpArm1RbnDtl_R_Jnt',
				'ctrl_md:UpArm2RbnDtl_R_Jnt',
				'ctrl_md:UpArm3RbnDtl_R_Jnt',
				'ctrl_md:UpArm4RbnDtl_R_Jnt',
				'ctrl_md:UpArm5RbnDtl_R_Jnt',
				'ctrl_md:Forearm1RbnDtl_R_Jnt',
				'ctrl_md:Forearm2RbnDtl_R_Jnt',
				'ctrl_md:Forearm3RbnDtl_R_Jnt',
				'ctrl_md:Forearm4RbnDtl_R_Jnt',
				'ctrl_md:Forearm5RbnDtl_R_Jnt',

				'ctrl_md:Hand_R_Jnt',
				'ctrl_md:Thumb1_R_Jnt',
				'ctrl_md:Thumb2_R_Jnt',
				'ctrl_md:Thumb3_R_Jnt',
				'ctrl_md:Thumb4_R_Jnt',
				'ctrl_md:Index1_R_Jnt',
				'ctrl_md:Index2_R_Jnt',
				'ctrl_md:Index3_R_Jnt',
				'ctrl_md:Index4_R_Jnt',
				'ctrl_md:Index5_R_Jnt',

				'ctrl_md:Middle1_R_Jnt',
				'ctrl_md:Middle2_R_Jnt',
				'ctrl_md:Middle3_R_Jnt',
				'ctrl_md:Middle4_R_Jnt',
				'ctrl_md:Middle5_R_Jnt',
				'ctrl_md:Ring1_R_Jnt',
				'ctrl_md:Ring2_R_Jnt',
				'ctrl_md:Ring3_R_Jnt',
				'ctrl_md:Ring4_R_Jnt',
				'ctrl_md:Ring5_R_Jnt',
				'ctrl_md:Pinky1_R_Jnt',
				'ctrl_md:Pinky2_R_Jnt',
				'ctrl_md:Pinky3_R_Jnt',
				'ctrl_md:Pinky4_R_Jnt',
				'ctrl_md:Pinky5_R_Jnt', 


				'ctrl_md:Neck1RbnDtl_Jnt',
				'ctrl_md:Neck2RbnDtl_Jnt',
				'ctrl_md:Neck3RbnDtl_Jnt',
				'ctrl_md:Neck4RbnDtl_Jnt',
				'ctrl_md:Neck5RbnDtl_Jnt',
				'ctrl_md:Head_Jnt',
				'ctrl_md:JawLwr1_Jnt',
				'ctrl_md:JawLwr2_Jnt',
				'ctrl_md:JawLwrEnd_Jnt',
				'ctrl_md:JawUpr1_Jnt',
				'ctrl_md:JawUprEnd_Jnt'
				]


	# HairObj = [HairGeo, HairJnt]
	# EyeObj = [EyeGeo , EyeJnt]
	# HeadObj = [HeadGeo, HeadJnt]
	# BodyObj = [BodyGeo, BodyJnt]
	MdGeo = 'MdGeo_Grp'


	# # BindSkin Hair
	SkinName = 'Md'
	# mc.select( MdGeo , r = True)
	# mc.select( BodyJnt, add = True)
	# mc.skinCluster(MdGeo, BodyJnt, n = 'test')
	objLists = utaCore.listGeo(sels = MdGeo)
	# print objLists, '22'
	for each in objLists:
		# print each, 'each'
		mc.skinCluster(BodyJnt, each, tsb=True, mi = 4)
		# print ken, 'ken'
		utaCore.unInfluences(listObj = each)
	# mc.select(clear = True)
	# print '# Generate >> BindSkin " %s " ' % HairName

	# # BindSkin Head
	# HeadName = 'Head'
	# mc.select(HeadGeo, r = True)
	# mc.select(HeadJnt, add = True)
	# mc.skinCluster(n = '%sSkinCluster' % HeadName)
	# mc.select(clear = True)
	# print '# Generate >> BindSkin " %s " ' % HeadName

