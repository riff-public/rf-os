# v.0.0.1 polytag switcher
_title = 'Strings Tools'
_version = 'v.0.0.1'
_des = 'wip'
uiName = 'StringsToolsUI'

#Import python modules
import sys
import os 
import getpass

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]


# framework modules 
import rf_config as config
from rf_utils import log_utils
from rf_utils.ui import load
user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'

logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

from utaTools.utapy import Strings
reload(Strings)
import maya.cmds as mc
reload(mc)

class Tool(QtWidgets.QMainWindow):

    def __init__(self, parent=None):
        #Setup Window
        super(Tool, self).__init__(parent)

        # ui read
        uiFile = '%s/ui.ui' % moduleDir
        self.ui = load.setup_ui_maya(uiFile, parent)
        self.setCentralWidget(self.ui)
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.setObjectName(uiName)

        self.init_signals()
        self.default_slider()

    def init_signals(self):
        self.ui.GenerateStrings_pushButton.clicked.connect(self.generate_strings)
        self.ui.Joint_pushButton.clicked.connect(self.list_joint)
        self.ui.RefreshSlider_pushButton.clicked.connect(self.refresh)
        self.ui.cv1_verticalSlider.valueChanged.connect(self.cv1_slider)
        self.ui.cv1_lineEdit.returnPressed.connect(self.cv1_enter)
        self.ui.cv2_verticalSlider.valueChanged.connect(self.cv2_slider)
        self.ui.cv2_lineEdit.returnPressed.connect(self.cv2_enter)
        self.ui.cv3_verticalSlider.valueChanged.connect(self.cv3_slider)
        self.ui.cv3_lineEdit.returnPressed.connect(self.cv3_enter)
        self.ui.cv4_verticalSlider.valueChanged.connect(self.cv4_slider)
        self.ui.cv4_lineEdit.returnPressed.connect(self.cv4_enter)
        self.ui.cv5_verticalSlider.valueChanged.connect(self.cv5_slider)
        self.ui.cv5_lineEdit.returnPressed.connect(self.cv5_enter)
        self.ui.cv6_verticalSlider.valueChanged.connect(self.cv6_slider)
        self.ui.cv6_lineEdit.returnPressed.connect(self.cv6_enter)
        self.ui.cv7_verticalSlider.valueChanged.connect(self.cv7_slider)
        self.ui.cv7_lineEdit.returnPressed.connect(self.cv7_enter)
        self.ui.cv8_verticalSlider.valueChanged.connect(self.cv8_slider)
        self.ui.cv8_lineEdit.returnPressed.connect(self.cv8_enter)
        self.ui.cv9_verticalSlider.valueChanged.connect(self.cv9_slider)
        self.ui.cv9_lineEdit.returnPressed.connect(self.cv9_enter)

    def generate_strings(self):
        statusBox =  self.ui.NoiceCtrl_checkBox.isChecked()
        statusNewStringBox =  self.ui.NoiceStringsCtrl_checkBox.isChecked()
        JntLineEdit = self.listJnt
        nameLineEdit = self.ui.Name_lineEdit.text()
        sideComboBox = self.ui.Side_comboBox.currentText()
        spansLineEdit = self.ui.Spans_lineEdit.text()
        shapeComboBox = self.ui.Shape_comboBox.currentText()
        axisComboBox = self.ui.Axis_comboBox.currentText()
        newNoiseAxisComboBox = self.ui.NoiceStringsCtrlAxis_comboBox.currentText()
        valueCvs = self.get_list_cv()
        Strings.Strings(   
                            name = nameLineEdit, 
                            jnt = JntLineEdit,
                            side = str(sideComboBox), 
                            spans = str(spansLineEdit), 
                            shape = str(shapeComboBox), 
                            valueCv = valueCvs, 
                            sringsNoiseCtrl = statusNewStringBox,
                            stringsNoiseAxis= newNoiseAxisComboBox,  
                            newNoiseCtrl =statusBox , 
                            newNoiseAxis= str(axisComboBox)

                                )
    # name = nameLineEdit, 
    # jnt = JntLineEdit, 
    # side = str(sideComboBox), 
    # spans = str(spansLineEdit), 
    # shape = str(shapeComboBox),
    # attrAxis= str(axisComboBox),
    # valueCv = valueCvs,
    # newNoise = statusBox,
    # stringsNoise = statusNewStringBox
    ## valueCv = [1, 0.5, 1, 0.78, 0.48, 0.25, 0.05, 1, 1],
    def list_joint(self):
        self.listJnt = mc.ls(sl = True)
        text = ''
        for ix in self.listJnt:
            text+='%s,'%ix
        self.ui.Joint_lineEdit.setText(text)

    def default_slider(self):
        self.ui.cv1_lineEdit.setText(str(0))
        self.ui.cv2_lineEdit.setText(str(0.5))
        self.ui.cv3_lineEdit.setText(str(1.0))
        self.ui.cv4_lineEdit.setText(str(0.78))
        self.ui.cv5_lineEdit.setText(str(0.48))
        self.ui.cv6_lineEdit.setText(str(0.25))
        self.ui.cv7_lineEdit.setText(str(0.05))
        self.ui.cv8_lineEdit.setText(str(0))
        self.ui.cv9_lineEdit.setText(str(0))
    def refresh(self):
        self.cv1_enter()
        self.cv2_enter()
        self.cv3_enter()
        self.cv4_enter()
        self.cv5_enter()
        self.cv6_enter()
        self.cv7_enter()
        self.cv8_enter()
        self.cv9_enter()


    def cv1_slider(self):
        ## Slider 1

        new_value1 = int(self.ui.cv1_verticalSlider.value()) * 0.01
        self.ui.cv1_lineEdit.setText(str(new_value1))

    def cv2_slider(self):
        ## Slider 2
        new_value2 = int(self.ui.cv2_verticalSlider.value()) * 0.01
        self.ui.cv2_lineEdit.setText(str(new_value2))

    def cv3_slider(self):
        ## Slider 3
        new_value3 = int(self.ui.cv3_verticalSlider.value()) * 0.01
        self.ui.cv3_lineEdit.setText(str(new_value3))

    def cv4_slider(self):
        ## Slider 4
        new_value4 = float(self.ui.cv4_verticalSlider.value()) * 0.01
        self.ui.cv4_lineEdit.setText(str(new_value4))

    def cv5_slider(self):
        ## Slider 5
        new_value5 = float(self.ui.cv5_verticalSlider.value()) * 0.01

        self.ui.cv5_lineEdit.setText(str(new_value5))

    def cv6_slider(self):
        ## Slider 6
        new_value6 = int(self.ui.cv6_verticalSlider.value()) * 0.01
        self.ui.cv6_lineEdit.setText(str(new_value6))

    def cv7_slider(self):
        ## Slider 7
        new_value7 = int(self.ui.cv7_verticalSlider.value()) * 0.01
        self.ui.cv7_lineEdit.setText(str(new_value7))

    def cv8_slider(self):
        ## Slider 8
        new_value8 = int(self.ui.cv8_verticalSlider.value()) * 0.01
        self.ui.cv8_lineEdit.setText(str(new_value8))

    def cv9_slider(self):
        ## Slider 9
        new_value9 = int(self.ui.cv9_verticalSlider.value()) * 0.01
        self.ui.cv9_lineEdit.setText(str(new_value9))

    def cv1_enter(self):
        value1 = float(self.ui.cv1_lineEdit.text()) *100
        if value1 <= 100:
            self.ui.cv1_verticalSlider.setValue(int(value1) )

    def cv2_enter(self):
        value2 = float(self.ui.cv2_lineEdit.text()) *100
        if value2 <= 100:
            self.ui.cv2_verticalSlider.setValue(int(value2) )

    def cv3_enter(self):
        value3 = float(self.ui.cv3_lineEdit.text()) *100
        if value3 <= 100:
            self.ui.cv3_verticalSlider.setValue(int(value3) )

    def cv4_enter(self):
        value4 = float(self.ui.cv4_lineEdit.text()) *100
        if value4 <= 100:
            self.ui.cv4_verticalSlider.setValue(int(value4) )

    def cv5_enter(self):
        value5 = float(self.ui.cv5_lineEdit.text()) *100
        if value5 <= 100:
            self.ui.cv5_verticalSlider.setValue(int(value5) )

    def cv6_enter(self):
        value6 = float(self.ui.cv6_lineEdit.text()) *100
        if value6 <= 100:
            self.ui.cv6_verticalSlider.setValue(int(value6) )

    def cv7_enter(self):
        value7 = float(self.ui.cv7_lineEdit.text()) *100
        if value7 <= 100:
            self.ui.cv7_verticalSlider.setValue(int(value7) )

    def cv8_enter(self):
        value8 = float(self.ui.cv8_lineEdit.text()) *100
        if value8 <= 100:
            self.ui.cv8_verticalSlider.setValue(int(value8) )

    def cv9_enter(self):
        value9 = float(self.ui.cv9_lineEdit.text()) *100
        if value9 <= 100:
            self.ui.cv9_verticalSlider.setValue(int(value9) )

    def get_list_cv(self):
        list_ = [   int(self.ui.cv1_verticalSlider.value()), 
                    int(self.ui.cv2_verticalSlider.value()),
                    int(self.ui.cv3_verticalSlider.value()), 
                    int(self.ui.cv4_verticalSlider.value()), 
                    int(self.ui.cv5_verticalSlider.value()), 
                    int(self.ui.cv6_verticalSlider.value()), 
                    int(self.ui.cv7_verticalSlider.value()), 
                    int(self.ui.cv8_verticalSlider.value()),
                    int(self.ui.cv9_verticalSlider.value()),  ]
        return list_


def show():
    from rftool.utils.ui import maya_win
    logger.info('Run in Maya\n')
    maya_win.deleteUI(uiName)
    myApp = Tool(maya_win.getMayaWindow())
    myApp.show()
    return myApp


