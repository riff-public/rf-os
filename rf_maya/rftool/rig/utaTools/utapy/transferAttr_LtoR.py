import maya.cmds as mc

def transferAttr(sel):
    listL = []
    listShapeL = []
    list_attr = mc.listAttr(sel)

    shapeL = mc.listRelatives(sel)
    shapeL = shapeL[0]
    list_attrShape = mc.listAttr(shapeL, keyable=True)
    
    for attr in list_attr:
        if '_L' in attr:
            listL.append(attr)
    for attrShape in list_attrShape:
        if '_L' in attrShape:
            listShapeL.append(attrShape)
            
    for attrL in listL:
        attrR = attrL.replace('_L', '_R')
        attrL = '%s.%s'%(sel,attrL)
        attrR = '%s.%s'%(sel,attrR)
        value = mc.getAttr(attrL)
        mc.setAttr(attrR, value)
        print 'setAttr %s to %s' %(attrL,attrR)

    for attrShapeL in listShapeL:
        attrR = attrShapeL.replace('_L', '_R')
        attrShapeL = '%s.%s'%(shapeL,attrShapeL)
        attrR = '%s.%s'%(sel,attrR)
        value = mc.getAttr(attrShapeL)
        mc.setAttr(attrR, value)
        print 'setAttr %s to %s' %(attrShapeL,attrR)


        
def transferMultiAttr(sel):
    listL = []
    listShapeL = []
    listTrans = []
    listTransShape = []
    list_attr = mc.listAttr(sel[0], keyable=True, unlocked=True)

    shapeL = mc.listRelatives(sel[0])
    shapeL = shapeL[0]
    list_attrShape = mc.listAttr(shapeL, keyable=True, unlocked=True)


    for attr in list_attr:
        if '_L' in attr:
            listL.append(attr)
        elif not '.' in attr:
            listTrans.append(attr)
    for attrShape in list_attrShape:
        if '_L' in attrShape:
            listShapeL.append(attrShape)
        elif not '.' in attrShape:
            listTransShape.append(attrShape)

    # print list_attrShape


    for attrL in listL:
        attrR = attrL.replace('_L', '_R')
        attrL = '%s.%s'%(sel[0],attrL)
        value = mc.getAttr(attrL)
        attrR = '%s.%s'%(sel[1],attrR)
        mc.setAttr(attrR, value)
        print 'setAttr %s to %s' %(attrL,attrR)

    for attrShapeL in listShapeL:
        attrR = attrShapeL.replace('_L', '_R')
        attrShapeL = '%s.%s'%(shapeL,attrShapeL)
        value = mc.getAttr(attrShapeL)
        attrR = '%s.%s'%(sel[1],attrR)
        mc.setAttr(attrR, value)
        print 'setAttr %s to %s' %(attrShapeL,attrR)

    for attrTrans in listTrans:
        attrL = '%s.%s'%(sel[0],attrTrans)
        value = mc.getAttr(attrL)
        types = mc.getAttr(attrL,type=True)
        attrR = '%s.%s'%(sel[1],attrTrans)
        if not types == 'string' and not types == 'float3':
            mc.setAttr(attrR, value)
            print 'setAttr %s to %s' %(attrL,attrR)
        
    for attrTransShape in listTransShape:
        attrL = '%s.%s'%(sel[0],attrTransShape)
        value = mc.getAttr(attrL)
        types = mc.getAttr(attrL,type=True)
        attrR = '%s.%s'%(sel[1],attrTransShape)
        if not types == 'string' and not types == 'float3':
            mc.setAttr(attrR, value)
            print 'setAttr %s to %s' %(attrL,attrR)

def transferAttr_LtoR_Run():
    sel =mc.ls(sl=True)
    if len(sel) == 1 :
        transferAttr(sel[0])
    elif len(sel) == 2 :
        transferMultiAttr(sel)
    else:
        print 'Wrong Selection'

### Copy Control  to ControlShape
def transferRefAttr():
    sel =mc.ls(sl=True)
    list_attr = mc.listAttr(sel[0])
    shapeAttr = mc.listRelatives(sel[1])
    list_shapeAttr = mc.listAttr(shapeAttr, keyable=True)

    for shapeAttr in list_shapeAttr:
        if shapeAttr in list_attr:
            attr = '%s.%s'%(sel[0], shapeAttr)
            newAttr ='%s.%s'%(sel[1], shapeAttr)
            value = mc.getAttr(attr)
            mc.setAttr(newAttr, value)