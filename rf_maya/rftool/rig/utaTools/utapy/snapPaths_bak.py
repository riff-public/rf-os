import maya.cmds as mc ;
import pymel.core as pm ;
# import rigTools
# reload(rigTools)
# import getNameSpace as gns 
# reload(gns)

def snapPaths(objSel, elem, ns, obj1, obj2, *args):
	# sel = mc.ls(sl=True)
	objSelA = obj1
	objSelB = ('%sOnOffSwitch1_Ctrl' % obj2)
	# print objSelB
	# onOffSeitch = objSelB[0]

	lstPthTmp = mc.ls('%sPathsM*_Jnt' % ns)
	# print lstPthTmp
	NoLstPthTmp = len(lstPthTmp)
	# print NoLstPthTmp

	lstPthConMGrp = mc.listRelatives('%sTailPathsJntZro_grp' % ns ,ad = True)
	# print lstPthConMGrp

	lstPthUpJnt = mc.ls('%sPathsUp*_Jnt' % ns)
	lstPthConGrp = []
	# print lstPthUpJnt

	for lstGrps in lstPthConMGrp:
	    lstGrp = lstGrps
	    # print lstGrp
	    if'_Jnt' in lstGrp:
	        pass
	    else:
	        lstPthConGrp.append(lstGrp)
	        # print lstPthConGrp

	for i in range(NoLstPthTmp):
	    mc.pointConstraint(lstPthTmp[-i],lstPthConGrp[i-1],mo = True)
	# mc.ls(cl=True)
	startJntPthMn = lstPthTmp[0]
	endJntPthMn = lstPthTmp[-1]
	startJntPthUp = lstPthUpJnt[0]
	endJntPthUp = lstPthUpJnt[-1]

	# Snap PathJnt to CrvPath
	snapPthMn = mc.ikHandle(sj =startJntPthMn,ee =endJntPthMn,c=('%s' % objSelA), n= ('%s%sIkPaths_ikh' % (ns,objSelA)), sol='ikSplineSolver', ccv=False, roc=True, pcv=False)
	snapPthUp = mc.ikHandle(sj =startJntPthUp,ee =endJntPthUp,c=('%sMotionsUpTemp' % objSelA), n= ('%s%sIkPathsUp_ikh' % (ns,objSelA)), sol='ikSplineSolver', ccv=False, roc=True, pcv =False)

	#create Ctrl and Grp Ctrl
	# ZGrp = mc.createNode('transform',n='%sIkPathCtrlZro_Grp' % ns)
	# CbCrv = mc.curve(d = 1,n = '%sIkPath_Ctrl' % ns,p=[(-0.5,1,0.5),(-0.5,0,0.5),(-0.5,0,-0.5),(-0.5,1,-0.5),(-0.5,1,0.5),(0.5,1,0.5),(0.5,0,0.5),(-0.5,0,0.5),(0.5,0,0.5),(0.5,0,-0.5),(0.5,1,-0.5),(0.5,1,0.5),(0.5,1,-0.5),(-0.5,1,-0.5),(-0.5,0,-0.5),(0.5,0,-0.5)])
	# CbCrv = mc.curve(d = 1,n = 'IkPath_Ctrl',p=[(-99.056548,0,0),(-88.41312,0,0),(-87.324554,0,-13.830848),(-84.08589,0,-27.321117),(-78.776635,0,-40.138682),(-71.527721,0,-51.967883),(-62.517537,0,-62.517537),(-51.967883,0,-71.527721),(-40.138682,0,-78.776635),(-27.321117,0,-84.08589),(-13.830848,0,-87.324554),(0,0,-88.41312),(0,0,-99.056548),(0,0,-88.41312),(13.830848,0,-87.324554),(27.321117,0,-84.08589),(40.138682,0,-78.776635),(51.967883,0,-71.527721),(62.517537,0,-62.517537),(71.527721,0,-51.967883),(78.776635,0,-40.138682),(84.08589,0,-27.321117),(87.324554,0,-13.830848),(88.41312,0,0),(99.056548,0,0),(88.41312,0,0),(87.324554,0,13.830848),(84.08589,0,27.321117),(78.776635,0,40.138682),(71.527721,0,51.967883),(62.517537,0,62.517537),(51.967883,0,71.527721),(40.138682,0,78.776635),(27.321117,0,84.08589),(13.830848,0,87.324554),(0,0,88.41312),(0,0,99.056548),(0,0,88.41312),(-13.830848,0,87.324554),(-27.321117,0,84.08589),(-40.138682,0,78.776635),(-51.967883,0,71.527721),(-62.517537,0,62.517537),(-71.527721,0,51.967883),(-78.776635,0,40.138682),(-84.08589,0,27.321117),(-87.324554,0,13.830848),(-88.41312,0,0)])
	# CbShape = mc.listRelatives( CbCrv , shapes = True )[0]
	# mc.setAttr('%s.overrideEnabled'%CbShape,1)
	# mc.setAttr('%s.overrideColor'%CbShape,22)
	# mc.parent(CbCrv,ZGrp)


	#Add Attr to IkPathCtrl
	# mc.addAttr(CbCrv,ln = 'IkPathVaule',at='double',dv = 0,k=True)
	# mc.setAttr(CbCrv+'.IkPathVaule',l=1)
	# mc.addAttr(CbCrv,ln = 'Offset',at='float', dv = 0 ,k=True)
	# mc.addAttr(CbCrv,ln = 'Roll',at='float',dv = 0,min = 0,k=True)
	# mc.addAttr(CbCrv,ln = 'Twist',at='float',dv = 0,k=True)
	# mc.addAttr(CbCrv,ln = 'IkBlend',at='float',dv = 1,min = 0,max =1,k=True)


	# mc.connectAttr(CbCrv+'.Roll',snapPthMn[0]+'.roll')
	# mc.connectAttr(CbCrv+'.Twist',snapPthMn[0]+'.twist')
	# mc.connectAttr(CbCrv+'.IkBlend',snapPthMn[0]+'.ikBlend')

	mc.connectAttr(snapPthMn[0]+'.offset',snapPthUp[0]+'.offset')
#----------------------------------------------------------------------------------------------
	# addAttribute to Functions Control
	mc.addAttr(objSelB, ln= 'Motions_______', at='double',dv = 0, k = True)
	# mc.setAttr('%s.Motions_______' % objSelB, lock = True)
	mc.addAttr(objSelB, ln = 'PathsOffset',at='double',dv = 0,k=True)
	pathsOffsetMdv = mc.createNode ('multiplyDivide', n = 'PathsOffset_Mdv')
	pathsOffsetPma = mc.createNode ('plusMinusAverage', n = 'PathsOffset_Pma')
	mc.connectAttr ('%s.PathsOffset' % objSelB, '%s.input1X' % pathsOffsetMdv)
	mc.setAttr('%s.input2X' % pathsOffsetMdv, -0.1)
	mc.connectAttr('%s.outputX' % pathsOffsetMdv, '%s.input2D[0].input2Dx' % pathsOffsetPma)
	mc.setAttr('%s.input2D[1].input2Dx' % pathsOffsetPma, 1)
	mc.connectAttr('%s.output2Dx' % pathsOffsetPma, '%s.offset' % snapPthMn[0])
	# mc.parent(ZGrp, ('%sCtrl_Grp' % ns))
# #----------------------------------------------------------------------------------------------
	# Create Group Jnt Motions Paths-----------------------------------------
	motionsMZro = mc.createNode ('transform', n = ('%s%sMZro_Grp' % (ns, elem)))
	motionsUpZro = mc.createNode ('transform', n = ('%s%sUpZro_Grp' % (ns, elem )))
	mc.parent('%sPathsM1_Jnt'% ns, motionsMZro)
	mc.parent('%sPathsUp1_Jnt'% ns, motionsUpZro)

	mc.hide(motionsMZro, motionsUpZro)

	#Create Group Ikh-------------------------------------------------------
	motionsIkhZro = mc.createNode ('transform', n = ('%s%sIkhZro_Grp' % (ns, elem )))
	mc.parent(('%s%sIkPaths_ikh'% (ns, objSelA )), ('%s%sIkPathsUp_ikh' % (ns,objSelA)), motionsIkhZro)
	mc.hide(motionsIkhZro)


def snapPathsObj(elem, ns, obj1, obj2, *args):
	# sel = mc.ls(sl=True)
	objSelA = obj1
	objSelB = obj2

	objSelASplit = objSelA.split(':')
    
	newBbjSelB = objSelB.split('_Crv')
	elem = newBbjSelB[0]
	## Create PathsM1
	lstPthTmp = []
	PathsM1 = mc.createNode ('joint', n = '%sM1_Jnt' % elem)
	lstPthTmp.append(PathsM1)
	PathsM2 = mc.createNode ('joint', n = '%sM2_Jnt' % elem)
	lstPthTmp.append(PathsM2)
	mc.parent(PathsM2, PathsM1)
	mc.setAttr('%s.tz' % PathsM2, -5)

	# print lstPthTmp, 'lstPthTmp'
	# lstPthTmp = mc.ls(mc.createNode ('joint', n = '%sPathsM1_Jnt' % ns))
	# print lstPthTmp

	# NoLstPthTmp = len(lstPthTmp)


	# print NoLstPthTmp

	# lstPthConMGrp = mc.listRelatives('%s%s' % (ns,objSel[-1]) ,ad = True)


	## Create PathsUp1
	lstPthUpJnt = []
	PathsUp1 = mc.createNode ('joint', n = '%sUp1_Jnt' % elem)
	lstPthUpJnt.append(PathsUp1)
	PathsUp2 = mc.createNode ('joint', n = '%sUp2_Jnt' % elem)
	lstPthUpJnt.append(PathsUp2)
	mc.parent(PathsUp2, PathsUp1)
	mc.setAttr('%s.tz' % PathsUp2, -5)
	# lstPthUpJnt = mc.ls(mc.createNode ('joint', n = '%sPathsUp_Jnt' % ns))

	# lstPthConGrp = []

	# for lstGrps in lstPthConMGrp:
	#     lstGrp = lstGrps
	#     if'_Jnt' in lstGrp:
	#         pass
	#     else:
	#         lstPthConGrp.append(lstGrp)
	# print lstPthConGrp, '0000...'
	# for i in range(NoLstPthTmp):


	startJntPthMn = lstPthTmp[0]
	# print startJntPthMn, 'startJntPthMn'
	endJntPthMn = lstPthTmp[-1]
	# print endJntPthMn, 'endJntPthMn'
	startJntPthUp = lstPthUpJnt[0]
	endJntPthUp = lstPthUpJnt[-1]

	# Snap PathJnt to CrvPath
	snapPthMn = mc.ikHandle(sj =startJntPthMn,ee =endJntPthMn,c=('%s' % objSelB), n= ('%sIkPaths_ikh' % objSelB), sol='ikSplineSolver', ccv=False, roc=True, pcv=False)
	snapPthUp = mc.ikHandle(sj =startJntPthUp,ee =endJntPthUp,c=('%sUpTemp_Crv' % elem), n= ('%s%sIkPathsUp_ikh' % (ns,objSelA)), sol='ikSplineSolver', ccv=False, roc=True, pcv =False)

	## pointConstraint Obj on Paths
	mc.parentConstraint(lstPthTmp[0],objSelA,)

	mc.connectAttr(snapPthMn[0]+'.offset',snapPthUp[0]+'.offset')
#----------------------------------------------------------------------------------------------
	# addAttribute to Functions Control
	print 
	mc.addAttr(objSelB, ln= '%s_______' % elem, at='double',dv = 0, k = True)
	# mc.setAttr('%s.Motions_______' % objSelB, lock = True)
	mc.addAttr(objSelB, ln = '%s_Offset'% elem,at='double',dv = 0,k=True)
	pathsOffsetMdv = mc.createNode ('multiplyDivide', n = 'Offset_Mdv')
	pathsOffsetPma = mc.createNode ('plusMinusAverage', n = 'Offset_Pma')
	mc.connectAttr ('%s.%s_Offset' % (objSelB, elem ), '%s.input1X' % pathsOffsetMdv)
	mc.setAttr('%s.input2X' % pathsOffsetMdv, -0.025)
	mc.connectAttr('%s.outputX' % pathsOffsetMdv, '%s.input2D[0].input2Dx' % pathsOffsetPma)
	mc.setAttr('%s.input2D[1].input2Dx' % pathsOffsetPma, 1)
	mc.connectAttr('%s.output2Dx' % pathsOffsetPma, '%s.offset' % snapPthMn[0])
	# mc.parent(ZGrp, ('%sCtrl_Grp' % ns))
#----------------------------------------------------------------------------------------------
	mc.addAttr(objSelB, ln = '%s_Roll'% elem,at='double',dv = 0,k=True)
	pathsRollMdv = mc.createNode ('multiplyDivide', n = 'Roll_Mdv')
	mc.connectAttr ('%s.%s_Roll' % (objSelB, elem ), '%s.input1X' % pathsRollMdv)
	mc.setAttr('%s.input2X' % pathsRollMdv, -0.5)
	mc.connectAttr('%s.outputX' % pathsRollMdv, '%s_CrvIkPaths_ikh.roll' % elem)
# #----------------------------------------------------------------------------------------------
# 	mc.addAttr(objSelB, ln = '%s_Twist'% elem,at='double',dv = 0,k=True)
# 	pathsTwidtMdv = mc.createNode ('multiplyDivide', n = 'Twist_Mdv')
# 	mc.connectAttr ('%s.%s_Roll' % (objSelB, elem ), '%s.input1X' % pathsTwidtMdv)
# 	mc.setAttr('%s.input2X' % pathsTwidtMdv, -0.5)
# 	mc.connectAttr('%s.outputX' % pathsTwidtMdv, '%s_CrvIkPaths_ikh.twist' % elem)
# #----------------------------------------------------------------------------------------------
	# Create Group Jnt Motions Paths-----------------------------------------
	motionsMZro = mc.createNode ('transform', n = ('%sMZro_Grp' %  elem))
	motionsUpZro = mc.createNode ('transform', n = ('%sUpZro_Grp' % elem ))
	mc.parent('%s'% lstPthTmp[0], motionsMZro)
	mc.parent('%s'% lstPthUpJnt[0], motionsUpZro)

	mc.hide(motionsMZro, motionsUpZro)

	#Create Group Ikh-------------------------------------------------------
	motionsIkhZro = mc.createNode ('transform', n = ('%sIkhZro_Grp' % elem ))
	mc.parent('%sIkPaths_ikh'%  objSelB , '%sIkPathsUp_ikh' % objSelASplit[-1], motionsIkhZro)
	mc.hide(motionsIkhZro)
