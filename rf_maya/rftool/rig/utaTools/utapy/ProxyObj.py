import maya.cmds as mc
reload(mc)
from utaTools.utapy import DeleteGrp 
reload(DeleteGrp)
from utaTools.utapy import lockHideAttrs
reload(lockHideAttrs)
    
def proxyObj (elem='Proxy') :	
    sels = mc.ls( sl=True )
    objDup = mc.duplicate(sels)
    objLists = []
    for ix in range( 0 , len( objDup ) ) :	
        lockHideAttrs.lockHideAttrs(obj = objDup[ix], attrArray = ['tx', 'ty', 'tz','rx', 'ry', 'rz', 'sx', 'sy', 'sz', 'v'], lock = False)
        mc.parent(objDup[ix], w= True)
        currElem = mc.rename(objDup[ix], '%s_%s' % ( sels[ix], elem))
        objLists.append(currElem)
        DeleteGrp.deleteGrp()
        ix+=1

    return objLists
