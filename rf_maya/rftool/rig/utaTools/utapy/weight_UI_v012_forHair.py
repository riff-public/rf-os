import maya.cmds as mc
import maya.mel as mm
from functools import partial

class TransferWeight(object):
	def __init__(self, parent=None):
		self.w = 470
		self.h = 470

		self.count = 0
		
	def show(self):
		uiName = 'AutoWeightTools'
		if mc.window(uiName, q=True, ex=True):
			mc.deleteUI(uiName, window=True)
		mc.window(uiName, title = uiName)
		
		mc.columnLayout ( w=470, cal='center')

		mc.frameLayout( label='Add', collapsable=True, bgc=[0,0,0], w=470)
		mc.columnLayout ( w=470, cal='center')
		self.objText2 = mc.textFieldButtonGrp(l='Object', tx='', bl='<<', ed=False)
		mc.textFieldButtonGrp(self.objText2, e=True, bc=partial(self.getSelectedObj, self.objText2))
		mc.textFieldButtonGrp(self.objText2, e=True, cw=[1, 70], h=35)
		self.jntText = mc.textFieldButtonGrp(l='Jnt List', tx='', bl='<<', ed=False)
		mc.textFieldButtonGrp(self.jntText, e=True, bc=partial(self.getMultiSelectedObj, self.jntText))
		mc.textFieldButtonGrp(self.jntText, e=True, cw=[1, 70], h=35)
		mc.setParent('..')

		mc.frameLayout( label='Create Guide', collapsable=True, bgc=[0,0,0], w=470)
		#marginHeight = 10
		self.scaleUi = mc.floatSliderGrp(label='ScaleYZ', field=True, fieldMinValue=0.0, fieldMaxValue=10.0, value=3.0, precision=3, cw=[1, 70])
		self.createGuideButton = mc.button(l='Create Guide', c=partial(self.createGuide), h=35, w=470)
		mc.setParent('..')

		mc.frameLayout( label='Auto Weight', collapsable=True, bgc=[0,0,0], w=470)
		self.minUi = mc.floatSliderGrp(label='Drop Off', field=True, minValue=0.0, maxValue=1.0, fieldMinValue=0.0, fieldMaxValue=10.0, value=0.0, precision=4, cw=[1, 70])
		self.autoWeightButton = mc.button(l='Auto Weight', c=partial(self.paintWeight), h=35, w=470)
		mc.setParent('..')

		mc.frameLayout( label='Adjust Weight', collapsable=True, bgc=[0,0,0], w=470)
		mc.columnLayout ( w=470, cal='center')
		self.objText = mc.textFieldButtonGrp(l='Object', tx='', bl='<<', ed=False)
		mc.textFieldButtonGrp(self.objText, e=True, bc=partial(self.getSelectedObj, self.objText))
		mc.textFieldButtonGrp(self.objText, e=True, cw=[1, 70], h=35)
		mc.separator()

		#mc.columnLayout ( w=470, adj=True, cal='center')
		mc.rowLayout(numberOfColumns=3 ,w=470)
		self.sel1Text = mc.textFieldButtonGrp(l='Jnt Release', tx='', bl='<<', ed=False)
		mc.textFieldButtonGrp(self.sel1Text, e=True, bc=partial(self.getSelectedObj, self.sel1Text))
		mc.textFieldButtonGrp(self.sel1Text, e=True, cw=[1, 70], h=35)
		self.set1Button = mc.button(l='SetReciv', w=50, h=23, c=self.set1)
		self.getButton = mc.button(l='Get Vtx', c=partial(self.get_vertex, 2), h=23, w=70)
		mc.setParent('..')
		mc.separator()

		#mc.columnLayout ( w=470, adj=True, cal='center')
		mc.rowLayout(numberOfColumns=3 ,w=470)
		self.sel2Text = mc.textFieldButtonGrp(l='Jnt Receive', tx='', bl='<<', ed=False)
		mc.textFieldButtonGrp(self.sel2Text, e=True, bc=partial(self.getSelectedObj, self.sel2Text))
		mc.textFieldButtonGrp(self.sel2Text, e=True, cw=[1, 70], h=35)
		self.set2Button = mc.button(l='SetReciv', w=50, h=23, c=self.set2)
		self.getButton2 = mc.button(l='Get Vtx', c=partial(self.get_vertex, 1), h=23, w=70)
		mc.setParent('..')
		mc.separator()

		#mc.columnLayout ( w=470, adj=True, cal='center')
		self.valueUi = mc.floatSliderGrp(label='Value', field=True, minValue=0.0, maxValue=1.0, fieldMinValue=0.0, fieldMaxValue=10.0, value=0.0, precision=4, cw=[1, 70])
		mc.separator()

		self.floodButton = mc.button(l='Flood', c=partial(self.flood), h=35, w=470)
		self.smoothButton = mc.button(l='Smooth', c=partial(self.smooth), h=35, w=470)
		mc.setParent('..')

		mc.showWindow(uiName)

	def getSelectedObj(self, *args):
		sel = mc.ls(sl=True)
		if sel:
			mc.textFieldButtonGrp(args[0], e=True, tx=sel[0])
		else:
			mc.textFieldButtonGrp(args[0], e=True, tx='')

	def getMultiSelectedObj(self, *args):
		sels = mc.ls(sl=True)
		jnt = ''
		for sel in sels:
			if jnt == '':
				jnt = jnt + sel
			else:
				jnt = jnt + ',' + sel

		if sels:
			print jnt
			mc.textFieldButtonGrp(args[0], e=True, tx=jnt)
		else:
			mc.textFieldButtonGrp(args[0], e=True, tx='')


	def get_nodes_in_history_by_type(self, typ , selection, *args):
		nodes = None
		for node in mc.listHistory(selection):
			if mc.nodeType(node) == typ:
				nodes = node
		return nodes

	def get_vertex(self, count,*args):

		self.count = count

		ctx = mc.currentCtx()
		if ctx == 'artAttrSkinContext':
			mc.setToolTo('selectSuperContext')

		obj = mc.textFieldButtonGrp(self.objText, q=True, tx=True)
		if not self.count == 2:
			sel1 = mc.textFieldButtonGrp(self.sel1Text, q=True, tx=True)
			self.sel2 = mc.textFieldButtonGrp(self.sel2Text, q=True, tx=True)
			self.count = 2
		elif not self.count == 1:
			sel1 = mc.textFieldButtonGrp(self.sel2Text, q=True, tx=True)
			self.sel2 = mc.textFieldButtonGrp(self.sel1Text, q=True, tx=True)
			self.count = 1

		vtx_list = []
		jntLock_list = []

		jntPath = mc.listRelatives(self.sel2, fullPath=True, allDescendents=True)
		jnt = jntPath[0]
		jntGrp = jnt.split('|')
		jntGrp.remove(jntGrp[0])

		node = self.get_nodes_in_history_by_type('skinCluster', obj)

		vtxnum = mc.polyEvaluate(obj,v=True)

		for jnt in jntGrp:
			if not sel1 == jnt:
				if not self.sel2 == jnt:
					mc.setAttr('%s.liw' % jnt, 1)
					jntLock_list.append(jnt)

		mc.select(cl=True)
		for i in range(vtxnum):
			vtx = '%s.vtx[%s]'%(obj,i)

			if mc.skinPercent(node , vtx, transform=self.sel2, query=True ):

				vtx_list.append(vtx)
				mc.select(vtx, tgl=True)
				# valueOld = mc.skinPercent(node , vtx, transform=sel2, query=True )
				# if valueOld == 1:
				# 	continue

				# value = valueOld + valueNew
				# if value > 1:
				# 	value =1
		#self.flood( sel2, value)



		skinPaintTool = 'artAttrSkinContext'
		if not mc.artAttrSkinPaintCtx(skinPaintTool,ex=True):
			mc.artAttrSkinPaintCtx(skinPaintTool,i1='paintSkinWeights.xpm',whichTool='skinWeights')

		mc.setToolTo(skinPaintTool)

		mm.eval('artSkinInflListChanging "%s" 1;'% self.sel2)
		mm.eval('artSkinInflListChanged artAttrSkinPaintCtx;')



		for jnt in jntLock_list:
			mc.setAttr('%s.liw' % jnt, 0)\



	def set1(self, *args):
		if not self.count == 1:

			ctx = mc.currentCtx()
			if ctx == 'artAttrSkinContext':
				mc.setToolTo('selectSuperContext')

			obj = mc.textFieldButtonGrp(self.objText, q=True, tx=True)
			sel1 = mc.textFieldButtonGrp(self.sel2Text, q=True, tx=True)
			self.sel2 = mc.textFieldButtonGrp(self.sel1Text, q=True, tx=True)

			# vtx_list = []
			# jntLock_list = []

			# jntPath = mc.listRelatives(self.sel2, fullPath=True, allDescendents=True)
			# jnt = jntPath[0]
			# jntGrp = jnt.split('|')
			# jntGrp.remove(jntGrp[0])

			# node = self.get_nodes_in_history_by_type('skinCluster', obj)

			# vtxnum = mc.polyEvaluate(obj,v=True)

			# for jnt in jntGrp:
			# 	if not sel1 == jnt:
			# 		if not self.sel2 == jnt:
			# 			mc.setAttr('%s.liw' % jnt, 1)
			# 			jntLock_list.append(jnt)

			# mc.select(cl=True)
			# for i in range(vtxnum):
			# 	vtx = '%s.vtx[%s]'%(obj,i)

			# 	if mc.skinPercent(node , vtx, transform=self.sel2, query=True ):

			# 		vtx_list.append(vtx)
			# 		mc.select(vtx, tgl=True)



			skinPaintTool = 'artAttrSkinContext'
			if not mc.artAttrSkinPaintCtx(skinPaintTool,ex=True):
				mc.artAttrSkinPaintCtx(skinPaintTool,i1='paintSkinWeights.xpm',whichTool='skinWeights')
				
			mc.setToolTo(skinPaintTool)
			mm.eval('artSkinInflListChanging "%s" 1;'% self.sel2)
			mm.eval('artSkinInflListChanged artAttrSkinPaintCtx;')

			self.count = 1
			print 'Receive Joint Change to %s ' % self.sel2

		else:
			print 'Joint %s is already Receive Joint' % self.sel2


	def set2(self, *args):
		if not self.count == 2:

			ctx = mc.currentCtx()
			if ctx == 'artAttrSkinContext':
				mc.setToolTo('selectSuperContext')

			obj = mc.textFieldButtonGrp(self.objText, q=True, tx=True)
			sel1 = mc.textFieldButtonGrp(self.sel1Text, q=True, tx=True)
			self.sel2 = mc.textFieldButtonGrp(self.sel2Text, q=True, tx=True)

			# vtx_list = []
			# jntLock_list = []

			# jntPath = mc.listRelatives(self.sel2, fullPath=True, allDescendents=True)
			# jnt = jntPath[0]
			# jntGrp = jnt.split('|')
			# jntGrp.remove(jntGrp[0])

			# node = self.get_nodes_in_history_by_type('skinCluster', obj)

			# vtxnum = mc.polyEvaluate(obj,v=True)

			# for jnt in jntGrp:
			# 	if not sel1 == jnt:
			# 		if not self.sel2 == jnt:
			# 			mc.setAttr('%s.liw' % jnt, 1)
			# 			jntLock_list.append(jnt)

			# mc.select(cl=True)
			# for i in range(vtxnum):
			# 	vtx = '%s.vtx[%s]'%(obj,i)

			# 	if mc.skinPercent(node , vtx, transform=self.sel2, query=True ):

			# 		vtx_list.append(vtx)
			# 		mc.select(vtx, tgl=True)



			skinPaintTool = 'artAttrSkinContext'
			if not mc.artAttrSkinPaintCtx(skinPaintTool,ex=True):
				mc.artAttrSkinPaintCtx(skinPaintTool,i1='paintSkinWeights.xpm',whichTool='skinWeights')
				
			mc.setToolTo(skinPaintTool)
			mm.eval('artSkinInflListChanging "%s" 1;'% self.sel2)
			mm.eval('artSkinInflListChanged artAttrSkinPaintCtx;')

			self.count = 2
			print 'Receive Joint Change to %s ' % self.sel2

		else:
			print 'Joint %s is already Receive Joint' % self.sel2


	def flood(self, *args):

		mc.toolPropertyWindow()

		value = mc.floatSliderGrp( self.valueUi, q=True, value=True)

		skinPaintTool = 'artAttrSkinContext'
		if not mc.artAttrSkinPaintCtx(skinPaintTool,ex=True):
			mc.artAttrSkinPaintCtx(skinPaintTool,i1='paintSkinWeights.xpm',whichTool='skinWeights')
			mc.setToolTo(skinPaintTool)
	
		ctx = mc.currentCtx()
		if not ctx == 'artAttrSkinContext':
			mc.setToolTo('artAttrSkinContext')
		
		mm.eval('artSkinInflListChanging "%s" 1;'% self.sel2)
		mm.eval('artSkinInflListChanged artAttrSkinPaintCtx;')

		mm.eval('artAttrPaintOperation artAttrSkinPaintCtx Add;')
		mm.eval('artSkinSetSelectionValue %s false artAttrSkinPaintCtx artAttrSkin;'% value)
		mm.eval('artAttrSkinValues artAttrSkinContext;')

		mm.eval('artAttrSkinPaintCtx -e -clear `currentCtx`;')

	def smooth(self, *args):

		mc.toolPropertyWindow()

		value = mc.floatSliderGrp( self.valueUi, q=True, value=True)

		skinPaintTool = 'artAttrSkinContext'
		if not mc.artAttrSkinPaintCtx(skinPaintTool,ex=True):
			mc.artAttrSkinPaintCtx(skinPaintTool,i1='paintSkinWeights.xpm',whichTool='skinWeights')
			mc.setToolTo(skinPaintTool)
	
		ctx = mc.currentCtx()
		if not ctx == 'artAttrSkinContext':
			mc.setToolTo('artAttrSkinContext')
		
		mm.eval('artSkinInflListChanging "%s" 1;'% self.sel2)
		mm.eval('artSkinInflListChanged artAttrSkinPaintCtx;')

		mm.eval('artAttrPaintOperation artAttrSkinPaintCtx Smooth;')
		mm.eval('artAttrSkinValues artAttrSkinContext;')

		mm.eval('artAttrSkinPaintCtx -e -clear `currentCtx`;')



	def dragScale(self, objs,*args):
		value = mc.floatSliderGrp(self.scaleUi, q=True, value=True)
		for obj in objs:
			mc.setAttr('%s.sy' % obj, value)
			mc.setAttr('%s.sz' % obj, value)

	def valueScale(self, objs,*args):
		value = mc.floatSliderGrp(self.scaleUi, q=True, value=True)
		for obj in objs:
			mc.setAttr('%s.sy' % obj, value)
			mc.setAttr('%s.sz' % obj, value)


	def createGuide(self, *args):

		#parents = ['Root_Jnt']

		nameCount = 0
		parents = mc.ls(sl=True)
		guide_create_list = []

		# get Joint
		jntUi = mc.textFieldButtonGrp(self.jntText, q=True, tx=True)
		if jntUi:
			self.jnt_list = jntUi.split(',') # ['jnt1_Jnt','jnt2_Jnt','jnt3_Jnt']


			if mc.objExists('Guide_Grp'):
				mc.warning( "Error : 'Guide_Grp' Exists" )
			else:
				mc.group( n = 'Guide_Grp', empty=True )

				scaleYZ = mc.floatSliderGrp(self.scaleUi, q=True, value=True)
				for i in range(len(self.jnt_list)):
				
					if self.jnt_list[i] == self.jnt_list[-1]:
						break

					p =self.jnt_list[i]
					c =self.jnt_list[i+1]
					guideName = p.replace('_Jnt','_Guide')

					if not mc.objExists(p):
						continue
					if not mc.objExists(c):
						continue


					jnt1Pos = mc.xform(p , q=True, ws=True, t=True )
					jnt2Pos = mc.xform(c  , q=True, ws=True, t=True )

					print '%s >> %s' % (p,c)

					disx = abs(jnt2Pos[0] - jnt1Pos[0])
					disy = abs(jnt2Pos[1] - jnt1Pos[1])
					disz = abs(jnt2Pos[2] - jnt1Pos[2])


					if jnt2Pos[0] >= jnt1Pos[0]:
						centerx = jnt1Pos[0] + (disx/2)
					else :
						centerx = jnt2Pos[0] + (disx/2)
						
					if jnt2Pos[1] >= jnt1Pos[1]:
						centery = jnt1Pos[1] + (disy/2)
					else :
						centery = jnt2Pos[1] + (disy/2)
						
					if jnt2Pos[2] >= jnt1Pos[2]:
						centerz = jnt1Pos[2] + (disz/2)
					else :
						centerz = jnt2Pos[2] + (disz/2)

					mc.polyCube (n = guideName)
					mc.parent(guideName , 'Guide_Grp')


					mc.setAttr('%s.translateX' % guideName, centerx)
					mc.setAttr('%s.translateY' % guideName, centery)
					mc.setAttr('%s.translateZ' % guideName, centerz)

					mc.delete(mc.aimConstraint(c ,guideName))

					scaleV = (disx + disy + disz)

					scaleV = scaleV + (scaleV/5)

					scaleYZ_input = scaleYZ

					mc.setAttr('%s.scaleX' % guideName, scaleV)
					mc.setAttr('%s.scaleY' % guideName, scaleYZ_input)
					mc.setAttr('%s.scaleZ' % guideName, scaleYZ_input)

					print 'Create %s \n' % guideName
					guide_create_list.append(guideName)


				mc.floatSliderGrp(self.scaleUi, e=True, dragCommand=partial(self.dragScale, guide_create_list))
				mc.floatSliderGrp(self.scaleUi, e=True, cc=partial(self.valueScale, guide_create_list))
		else:
			mc.warning( "Error : Don't have Jnt in Jnt List" )
		

	def paintWeight(self, *args):

		mainJnt = self.jnt_list[0]
		mainJntName = mainJnt.split('_')[0]
		scndName = self.jnt_list[1].split('_')[0]

		minWeight = mc.floatSliderGrp(self.minUi, q=True, value=True)
		maxWeight = 1.0 - minWeight

		if minWeight > maxWeight:
			re = maxWeight
			maxWeight = minWeight
			minWeight = re


		sels = mc.ls(sl=True)
		if sels:

			for sel in sels:
				node = self.get_nodes_in_history_by_type('skinCluster', sel)
				jntGrp = mc.skinCluster(sel ,q=True ,influence=True )
				if not node:
					mc.warning("%s Object doesn't have SKINCLUSTER !!" % sel)
					continue

				print sel

				vtxnum = mc.polyEvaluate(sel,v=True)

				mc.skinPercent( node, sel, transformValue = [mainJnt, 1])


				guide_list = mc.listRelatives('Guide_Grp', children=True )


				for guide in guide_list:

					dupCount = 0

					vtx_list1 = []
					vtx_list2 = []
					vtx_list3 = []

					vtx_intersect_dict = {}
					vtx_intersect_dict2 = {}

					startvtx = None
					startvtxX = None
					startvtxY = None
					startvtxZ = None
					startvtxX3 = None
					startvtxY3 = None
					startvtxZ3 = None
					endvtx = None
					endvtxX = None
					endvtxY = None
					endvtxZ = None
					endvtxX3 = None
					endvtxY3 = None
					endvtxZ3 = None

					guide0 = None
					guide1 = None
					guide2 = None

					bbx = mc.exactWorldBoundingBox(guide)

					mc.duplicate( sel , n='dup1')
					mc.duplicate( guide , n='dup2')
					intersectName = 'intersect_geo'
					mc.polyBoolOp( 'dup1', 'dup2', op=3, n=intersectName )

					vtxnumInt = mc.polyEvaluate(intersectName,v=True)
					vtxpos_list =[]

					for i in range(vtxnumInt):
						vtx = '%s.vtx[%s]'%(intersectName,i)
						vtxpos = mc.xform(vtx, q=True, ws=True, t=True )
						vtxpos_list.append(vtxpos)

					mc.delete('dup1')
					mc.delete('dup2')
					mc.delete(intersectName)

					vtxnum = mc.polyEvaluate(sel,v=True)
					for i in range(vtxnum):
						vtx = '%s.vtx[%s]'%(sel,i)
						vtxpos = mc.xform(vtx, q=True, ws=True, t=True )
						for vtxposInt in vtxpos_list:
							if vtxpos[0] - vtxposInt[0] <= 0.000009 and vtxpos[0] - vtxposInt[0] >= -0.000009:
								if vtxpos[1] - vtxposInt[1] <= 0.000009 and vtxpos[1] - vtxposInt[1] >= -0.000009:
									if vtxpos[2] - vtxposInt[2] <= 0.000009 and vtxpos[2] - vtxposInt[2] >= -0.000009:
										vtx_list1.append(vtx)

					for guide2 in guide_list:

						startvtxX = None
						startvtxY = None
						startvtxZ = None

						endvtxX = None
						endvtxY = None
						endvtxZ = None

						vtx_list2 = []

						if guide2 == guide:
							continue

						bbx2 = mc.exactWorldBoundingBox(guide2)

						for vtx in vtx_list1:

							vtxpos = mc.xform(vtx, q=True, ws=True, t=True )

							if vtxpos[0] >= bbx2[0] and vtxpos[0] <= bbx2[3]:
								#print 'pass1'
								if vtxpos[1] >= bbx2[1] and vtxpos[1] <= bbx2[4]:
									#print 'pass2'
									if vtxpos[2] >= bbx2[2] and vtxpos[2] <= bbx2[5]:
										dupCount = 1
										break



						if dupCount == 1:
							dupCount = 0
							mc.duplicate( guide , n='dup1')
							mc.duplicate( guide2 , n='dup2')
							intersectName = 'intersect_geo'
							mc.polyBoolOp( 'dup1', 'dup2', op=3, n=intersectName )

							mc.delete('dup1')
							mc.delete('dup2')

							mc.duplicate( sel , n='dup1')
							mc.duplicate( intersectName , n='dup2')
							mc.delete(intersectName)
							mc.polyBoolOp( 'dup1', 'dup2', op=3, n=intersectName )

							vtxnumInt = mc.polyEvaluate(intersectName,v=True)
							vtxpos_list =[]

							for i in range(vtxnumInt):
								vtx = '%s.vtx[%s]'%(intersectName,i)
								vtxpos = mc.xform(vtx, q=True, ws=True, t=True )
								vtxpos_list.append(vtxpos)

							mc.delete('dup1')
							mc.delete('dup2')
							mc.delete(intersectName)


							for vtx in vtx_list1:

								vtxpos = mc.xform(vtx, q=True, ws=True, t=True )
								for vtxposInt in vtxpos_list:
									if vtxpos[0] - vtxposInt[0] <= 0.000009 and vtxpos[0] - vtxposInt[0] >= -0.000009:
										if vtxpos[1] - vtxposInt[1] <= 0.000009 and vtxpos[1] - vtxposInt[1] >= -0.000009:
											if vtxpos[2] - vtxposInt[2] <= 0.000009 and vtxpos[2] - vtxposInt[2] >= -0.000009:

												vtx_list2.append(vtx)

												vtxposCalX = vtxpos[0]
												vtxposCalY = vtxpos[1]
												vtxposCalZ = vtxpos[2]

												if not startvtxX:
													startvtxX = vtxposCalX
												if vtxposCalX < startvtxX:
													startvtxX = vtxposCalX
													
												if not endvtxX:
													endvtxX = vtxposCalX
												if vtxposCalX > endvtxX:
													endvtxX = vtxposCalX

												if not startvtxY:
													startvtxY = vtxposCalY
												if vtxposCalY < startvtxY:
													startvtxY = vtxposCalY
													
												if not endvtxY:
													endvtxY = vtxposCalY
												if vtxposCalY > endvtxY:
													endvtxY = vtxposCalY

												if not startvtxZ:
													startvtxZ = vtxposCalZ
												if vtxposCalZ < startvtxZ:
													startvtxZ = vtxposCalZ
													
												if not endvtxZ:
													endvtxZ = vtxposCalZ
												if vtxposCalZ > endvtxZ:
													endvtxZ = vtxposCalZ



						jntName1 = guide.replace('_Guide', '_Jnt')

						for jnt in jntGrp:
							if not mainJntName in guide:
								if not mainJntName in jnt:
									guideName = jnt.replace('_Jnt', '_Guide')
									if not guideName == guide:
										mc.setAttr('%s.liw' % jnt, 1)

							else:
								if not scndName in jnt:
									if not mainJntName in jnt:
										guideName = jnt.replace('_Jnt', '_Guide')
										if not guideName == guide:
											mc.setAttr('%s.liw' % jnt, 1)



						if vtx_list2:

							for vtx2 in vtx_list2:
								if vtx2 in vtx_list1:
									vtx_list1.remove(vtx2)

							for guide3 in guide_list:

								startvtxX3 = None
								startvtxY3 = None
								startvtxZ3 = None
								endvtxX3 = None
								endvtxY3 = None
								endvtxZ3 = None

								vtx_list3 = []

								if guide3 == guide:
									continue
								if guide3 == guide2:
									continue

								bbx3 = mc.exactWorldBoundingBox(guide3)

								for vtx in vtx_list2:

									vtxpos = mc.xform(vtx, q=True, ws=True, t=True )

									if vtxpos[0] >= bbx3[0] and vtxpos[0] <= bbx3[3]:
										#print 'pass1'
										if vtxpos[1] >= bbx3[1] and vtxpos[1] <= bbx3[4]:
											#print 'pass2'
											if vtxpos[2] >= bbx3[2] and vtxpos[2] <= bbx3[5]:
												dupCount = 1
												break



								if dupCount == 1:

									dupCount = 0

									mc.duplicate( guide , n='dup1')
									mc.duplicate( guide2 , n='dup2')
									intersectName = 'intersect_geo'
									mc.polyBoolOp( 'dup1', 'dup2', op=3, n=intersectName )

									mc.delete('dup1')
									mc.delete('dup2')

									mc.duplicate( intersectName , n='dup1')
									mc.duplicate( guide3 , n='dup2')
									mc.delete(intersectName)
									mc.polyBoolOp( 'dup1', 'dup2', op=3, n=intersectName )

									mc.delete('dup1')
									mc.delete('dup2')

									mc.duplicate( sel , n='dup1')
									mc.duplicate( intersectName , n='dup2')
									mc.delete(intersectName)
									mc.polyBoolOp( 'dup1', 'dup2', op=3, n=intersectName )


									vtxnumInt = mc.polyEvaluate(intersectName,v=True)
									vtxpos_list =[]

									for i in range(vtxnumInt):
										vtx = '%s.vtx[%s]'%(intersectName,i)
										vtxpos = mc.xform(vtx, q=True, ws=True, t=True )
										vtxpos_list.append(vtxpos)

									mc.delete('dup1')
									mc.delete('dup2')
									mc.delete(intersectName)

									for vtx in vtx_list2:

										vtxpos = mc.xform(vtx, q=True, ws=True, t=True )

										for vtxposInt in vtxpos_list:
											if vtxpos[0] - vtxposInt[0] <= 0.000009 and vtxpos[0] - vtxposInt[0] >= -0.000009:
												if vtxpos[1] - vtxposInt[1] <= 0.000009 and vtxpos[1] - vtxposInt[1] >= -0.000009:
													if vtxpos[2] - vtxposInt[2] <= 0.000009 and vtxpos[2] - vtxposInt[2] >= -0.000009:
														
														vtx_list3.append(vtx)

														vtxposCalX = vtxpos[0]
														vtxposCalY = vtxpos[1]
														vtxposCalZ = vtxpos[2]

														if not startvtxX3:
															startvtxX3 = vtxposCalX
														if vtxposCalX < startvtxX3:
															startvtxX3 = vtxposCalX
															
														if not endvtxX3:
															endvtxX3 = vtxposCalX
														if vtxposCalX > endvtxX3:
															endvtxX3 = vtxposCalX

														if not startvtxY3:
															startvtxY3 = vtxposCalY
														if vtxposCalY < startvtxY3:
															startvtxY3 = vtxposCalY
															
														if not endvtxY3:
															endvtxY3 = vtxposCalY
														if vtxposCalY > endvtxY3:
															endvtxY3 = vtxposCalY

														if not startvtxZ3:
															startvtxZ3 = vtxposCalZ
														if vtxposCalZ < startvtxZ3:
															startvtxZ3 = vtxposCalZ
															
														if not endvtxZ3:
															endvtxZ3 = vtxposCalZ
														if vtxposCalZ > endvtxZ3:
															endvtxZ3 = vtxposCalZ


								if vtx_list3:

									for vtx3 in vtx_list3:
										if vtx3 in vtx_list2:
											vtx_list2.remove(vtx3)



									for vtx in vtx_list3:


										alldisX = endvtxX3 - startvtxX3
										alldisY = endvtxY3 - startvtxY3
										alldisZ = endvtxZ3 - startvtxZ3

										vtxpos = mc.xform(vtx, q=True, ws=True, t=True )

										vtxposCalX = vtxpos[0]
										vtxposCalY = vtxpos[1]
										vtxposCalZ = vtxpos[1]

										disvtxX = vtxposCalX-startvtxX3
										disvtxY = vtxposCalY-startvtxY3
										disvtxZ = vtxposCalZ-startvtxZ3
										perdisvtxX = (disvtxX/alldisX)*100.0
										perdisvtxY = (disvtxY/alldisY)*100.0
										perdisvtxZ = (disvtxZ/alldisZ)*100.0



										centerper = (perdisvtxX+perdisvtxY+perdisvtxZ)/3

										weight = ((centerper)/100.0)*1.0

	

										weight2 = 1.0 - weight
										weight3 = 1.0 - weight

										if weight < 0.5:
											weight3 = weight


										bbx1 = mc.exactWorldBoundingBox(guide)
										bbx2 = mc.exactWorldBoundingBox(guide2)
										bbx3 = mc.exactWorldBoundingBox(guide3)

										if bbx2[3] > bbx1[3]:
											if bbx3[3] > bbx2[3]:
												weight2 = weight
												weight = 1.0 - weight
										elif bbx2[4] > bbx1[4]:
											if bbx3[4] > bbx2[4]:
												weight2 = weight
												weight = 1.0 - weight
										elif bbx2[5] > bbx1[5]:
											if bbx3[5] > bbx2[5]:
												weight2 = weight
												weight = 1.0 - weight


										minusWeight = ((weight+weight2+weight3) - 1.0) / 3
										weight = weight - minusWeight
										weight2 = weight2 - minusWeight
										weight3 = weight3 - minusWeight



										if weight < 0.0:
											plus = abs(weight)/2
											weight = weight + minusWeight + plus


										if weight < minWeight:
											weight = minWeight
										elif weight > maxWeight:
											weight = maxWeight

											
										mc.skinPercent( node, vtx, transformValue = [jntName1, weight])


							for vtx in vtx_list2:

								alldisX = endvtxX - startvtxX
								alldisY = endvtxY - startvtxY
								alldisZ = endvtxZ - startvtxZ

								vtxpos = mc.xform(vtx, q=True, ws=True, t=True )

								vtxposCalX = vtxpos[0]
								vtxposCalY = vtxpos[1]
								vtxposCalZ = vtxpos[2]

								disvtxX = vtxposCalX-startvtxX
								disvtxY = vtxposCalY-startvtxY
								disvtxZ = vtxposCalZ-startvtxZ
								perdisvtxX = (disvtxX/alldisX)*100.0
								perdisvtxY = (disvtxY/alldisY)*100.0
								perdisvtxZ = (disvtxZ/alldisZ)*100.0


								centerper = (perdisvtxX+perdisvtxY+perdisvtxZ)/3

								weight = ((centerper)/100.0)*1.0
								bbx1 = mc.exactWorldBoundingBox(guide)
								bbx2 = mc.exactWorldBoundingBox(guide2)
								bbx3 = mc.exactWorldBoundingBox(guide3)

								if bbx2[3] > bbx1[3]:
									if bbx3[3] > bbx2[3]:
										weight2 = weight
										weight = 1.0 - weight
								elif bbx2[4] > bbx1[4]:
									if bbx3[4] > bbx2[4]:
										weight2 = weight
										weight = 1.0 - weight
								elif bbx2[5] > bbx1[5]:
									if bbx3[5] > bbx2[5]:
										weight2 = weight
										weight = 1.0 - weight

								if weight < 0.0:
									plus = abs(weight)/2
									weight = weight + minusWeight + plus


								if weight < minWeight:
									weight = minWeight
								elif weight > maxWeight:
									weight = maxWeight



									
								mc.skinPercent( node, vtx, transformValue = [jntName1, weight])
								

					for vtx in vtx_list1:
						mc.skinPercent( node, vtx, transformValue = [jntName1, 1.0])


					for jnt in jntGrp:
						if not mainJntName in guide:
							if not mainJntName in jnt:
								guideName = jnt.replace('_Jnt', '_Guide')
								if not guideName == guide:
									mc.setAttr('%s.liw' % jnt, 0)

						else:
							if not scndName in jnt:
								if not mainJntName in jnt:
									guideName = jnt.replace('_Jnt', '_Guide')
									if not guideName == guide:
										mc.setAttr('%s.liw' % jnt, 0)

											
					print ">>>>>>>>>>>>>>>>>> FINISH Paint Weight %s <<<<<<<<<<<<<<<<<" % jntName1

					if guide == guide_list[-1]:

						print "\n>>>>>>>>>>>>>>>>>> ... PAINT WEIGHT : %s : FINISH ... <<<<<<<<<<<<<<<<<\n" % sel


				if sel == sels[-1]:
					print "\n\n>>>>>>>>>>>>>>>>>> ... PAINT WEIGHT ALL DONE ... <<<<<<<<<<<<<<<<<\n\n"


		else:
			mc.warning( "NONE Selected Object !!!")



transferWeight = TransferWeight()
transferWeight.show()
