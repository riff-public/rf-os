import maya.cmds as mc
from lpRig import rigTools as lrr
reload(lrr)

import maya.mel as mel

import sys,os
from rf_utils.context import context_info
reload(context_info)

import pickle

class TransferWeights(object):

    def __init__(self):
        self.winName='TransferWeights1by1_UI'

    def openUI(self, *args):

        #windowID = 'BlendshapeFclToMrm_UI'
        
        
        if mc.window(self.winName, exists=True):
            mc.deleteUI(self.winName)
        winWidth = 500
        winHight = 250
        
        SourceJntsList = []
        TargetJntsList = []
        mc.window(self.winName , title = 'Transfer Weights 1 by 1 UI' , width = winWidth , height = winHight , sizeable = 0 )
        mainCL = mc.columnLayout('columnLayoutName01' , adj=1)
        #utaCore.wintes(window = 'MuscleConnectUI' , path = 'D:/' , pngIcon = 'MuscleConnectIcon.png')

        mc.rowLayout(numberOfColumns=2 , h=20)
        mc.text(label = 'Source Jnts :' , w=winWidth*0.5)
        mc.text(label = 'Target Jnts :', w=winWidth*0.5)
        mc.setParent('..')

        mc.rowLayout(numberOfColumns=3 , h=140)
        mc.textScrollList( 'SourceJntsScroll' ,w=winWidth*.45  , numberOfRows=8, allowMultiSelection=True,
                    append=SourceJntsList,
                    showIndexedItem=4, 
                    h=130)
        mc.button('SCAN', label='SCAN'  , w=.1*winWidth , h = 20,bgc=[1,.85,0] , c = self.scanSelGeo) 
        mc.textScrollList( 'TargetJntsScroll' ,w=winWidth*.45  , numberOfRows=8, allowMultiSelection=True,
                    append=TargetJntsList,
                    showIndexedItem=4, 
                    h=130)
        mc.setParent('..')


        mc.separator(style='none' , h=10)
               
        mc.rowLayout(numberOfColumns=9)
        mc.button('AddSourceJnts'  ,label = 'Add', w=.11*winWidth , h = 30 , c = self.addSourceJnts)
        mc.button('RemoveSourceJnts' ,label = 'Del' , w=.11*winWidth , h = 30 ,c=self.removeSourceJnts)
        mc.button('UpSourceJnts'  ,l = 'Up', w=.11*winWidth , h = 30 , c = self.upSourceJnts)
        mc.button('DownSourceJnts'  ,l = 'Down', w=.11*winWidth , h = 30 , c = self.downSourceJnts)  
        mc.text(l='' , w=winWidth*.1)
        mc.button('AddTargetJnts'  ,l = 'Add', w=.11*winWidth , h = 30 , c = self.addTargetJnts)
        mc.button('RemoveTargetJnts' ,l = 'Del' , w=.11*winWidth , h = 30 ,c=self.removeTargetJnts)
        mc.button('UpTargetJnts' ,l = 'Up' , w=.11*winWidth , h = 30 , c = self.upTargetJnts)
        mc.button('DownTargetJnts' ,l = 'Down' , w=.11*winWidth , h = 30 , c = self.downTargetJnts)  
     
        mc.setParent('..')

        mc.rowLayout(numberOfColumns=4)
        mc.button('deleteRButton'  ,l = 'Remove _R_', w=.45*winWidth , h = 15 , c = self.deleteR)
        mc.text(l='' , w=winWidth*.1)
        mc.button('writeButton'  ,l = 'Write', w=.225*winWidth , h = 15 , c = self.writeDict)
        mc.button('readButton'  ,l = 'Read', w=.225*winWidth , h = 15 , c = self.readDict)
        mc.setParent('..')

        mc.separator(style='none' , h=10)
        mc.button('transferButton' ,l = 'Transfer Weights' , w=winWidth , h = 50 , bgc=[0,1,0] , c = self.doTransferWeights)   
        mc.showWindow()

    def scanSelGeo(self,*args):
        
        selJnts = []


        sel = mc.ls(sl=True)
        for each in sel:

            Conlist = []

            inputList = mc.listHistory(each , pdo =True) 
            for i in inputList:
                subj = mc.objectType(i)
                if subj == 'skinCluster':
                    Conlist.append(i)
            else : pass
                    
            jntList = mc.listConnections( '{}.influenceColor'.format(Conlist[0]), d=False, s=True )

            for a in jntList:

                selJnts.append(a)

        print selJnts


        aaa = list(set(selJnts))
        aaa.sort()
        mc.textScrollList( 'SourceJntsScroll' , e=True , ra=True)
        mc.textScrollList( 'SourceJntsScroll' , e=True , append=aaa)
        

    def removeGeo(self,scrollName , *args):
        List = mc.textScrollList( scrollName ,q=True, si=True)

        for each in List:
            mc.textScrollList(scrollName ,edit=True , ri='{}'.format(each))


    def removeSourceJnts(self,*args):
        self.removeGeo('SourceJntsScroll')
    def removeTargetJnts(self,*args):
        self.removeGeo('TargetJntsScroll')


    def addToDisplayGeo(self,scrollName,*args):
        
        sel = mc.ls(sl=True)
        
        for each in sel:
            mc.textScrollList(scrollName ,edit=True,append=['{}'.format(each)])
            print mc.textScrollList(scrollName ,q=True,ai=1)

    def addSourceJnts(self,*args):
        self.addToDisplayGeo('SourceJntsScroll')
    def addTargetJnts(self,*args):
        self.addToDisplayGeo('TargetJntsScroll')


    def upScroll(self,scrollName , *args):
        intList = mc.textScrollList(scrollName,q=1,sii=1)
        strList = mc.textScrollList(scrollName,q=1,si=1)
        selList = []
        for num in range(len(strList)):
            idx = intList[num]
            if 1 not  in intList:
                if idx  >= 2:
                    mc.textScrollList(scrollName,e=1,rii=idx)
                    mc.textScrollList(scrollName,e=1,ap=[idx-1,strList[num]])
                    selList.append(idx-1)

                else:
                    selList.append(idx)
                    pass

                mc.textScrollList(scrollName,e=1,sii=selList)
            else:
                mc.textScrollList(scrollName,e=1,sii = intList)

    def downScroll(self,scrollName,*args):
            intList = mc.textScrollList(scrollName,q=1,sii=1)
            strList = mc.textScrollList(scrollName,q=1,si=1)
            maxNum = mc.textScrollList(scrollName,q=1,ni=1)
            selList = []

            for num in range(len(strList))[::-1]:
                idx = intList[num]
                if maxNum not in intList:
                    if idx  <= maxNum-1:
                        #print 'idx : {} < strList : {}'.format(idx+1,strList[num])
                        mc.textScrollList(scrollName,e=1,rii=idx)
                        mc.textScrollList(scrollName,e=1,ap=[idx+1,strList[num]])
                        selList.append(idx+1)
                    else:
                        #print 'there something in this case : idx: {} and strList :{}'.format(idx+1,strList[num])
                        selList.append(idx)
                    #print selList

                    mc.textScrollList(scrollName,e=1,sii=selList)
                else:
                    mc.textScrollList(scrollName,e=1,sii = intList)

    def upSourceJnts(self,*args):
            self.upScroll('SourceJntsScroll')
    def downSourceJnts(self,*args):
            self.downScroll('SourceJntsScroll')
    def upTargetJnts(self,*args):
            self.upScroll('TargetJntsScroll')
    def downTargetJnts(self,*args):
            self.downScroll('TargetJntsScroll')


    def getSide(self,obj,*args):

        fullname = obj.split(':')[-1]
        name = fullname.split('_')
        if len(name) == 2:
            side = ''
        elif len(name) == 3:
            side = '{}'.format(name[1])

        return side

    def deleteR(self , *args):
        allItems = mc.textScrollList('SourceJntsScroll',q=1,ai=1)
        newList =  []
        sides = ['RGT' , 'Rgt' ,  'rgt' ,  'R' ]

        for each in allItems:

            s = self.getSide(each)

            if s in sides:
                pass
            else:
                newList.append(each)



        newDisplayList = list(set(newList))
        newDisplayList.sort()
        mc.textScrollList( 'SourceJntsScroll' , e=True , ra=True)
        mc.textScrollList( 'SourceJntsScroll' , e=True , append=newDisplayList)



    def doTransferWeights(self , *args):
        allSource = mc.textScrollList('SourceJntsScroll',q=1,ai=1)
        allTarget = mc.textScrollList('TargetJntsScroll',q=1,ai=1)

        if len(allSource)==len(allTarget):
            # print 'yes ysyes'

            jntDict = {}

            for i in range(len(allSource)):
                jntDict[allSource[i]] = allTarget[i]
                

            geo=mc.ls(sl=True)

            for each in geo:
                sp = mc.listRelatives(each , c=True , type='mesh')[0]
                
                Conlist = []
                inputList = mc.listHistory(each , pdo =True) 
                for i in inputList:
                    subj = mc.objectType(i)
                    if subj == 'skinCluster':
                        Conlist.append(i)
                    else : pass

                infs = mc.skinCluster(sp,inf=True ,q=True)

                for a in infs:

                    if not a in allSource:
                        return

                    # print jntDict[a] , infs[a]
                    if jntDict[a] == a:
                        pass
                    else:
                        mc.skinCluster(Conlist[0] , ai = jntDict[a] , lw=True , edit=True , wt=False)


                jntList = mc.listConnections( '{}.influenceColor'.format(Conlist[0]), d=False, s=True )
            
                for e in jntList:
                    mc.setAttr('{}.liw'.format(e) , 1)


                for i in range(len(infs)):
                    mc.setAttr('{}.liw'.format(infs[i]) , 0)
                    mc.setAttr('{}.liw'.format(jntDict[infs[i]]) , 0)

                    mc.skinPercent( '{}'.format(Conlist[0]), '{}.vtx[:]'.format(each), transformValue=[('{}'.format(jntDict[infs[i]]), 1)])

                    mc.skinCluster(Conlist[0] , inf=infs[i] , e=True , lw=True)
                    mc.skinCluster(Conlist[0] , inf=jntDict[infs[i]] , e=True , lw=True)

        else:
            print 'nononononono'


    def writeDict(self , *args):

        allSource = mc.textScrollList('SourceJntsScroll',q=1,ai=1)
        allTarget = mc.textScrollList('TargetJntsScroll',q=1,ai=1)

        asset = context_info.ContextPathInfo()
        projectPath = os.environ['RFPROJECT']
        fileWorkPath = (asset.path.name()).replace('$RFPROJECT',projectPath)
        filePath = fileWorkPath.replace('work','publ')
        path = filePath + '/rig/data/TransferWeightsDict.txt'

        fid = open(path,'w')

        jntDict = allSource
        targetDict = allTarget
        pickle.dump([jntDict,targetDict],fid)
        fid.close()


    def readDict(self , *args):
        asset = context_info.ContextPathInfo()
        projectPath = os.environ['RFPROJECT']
        fileWorkPath = (asset.path.name()).replace('$RFPROJECT',projectPath)
        filePath = fileWorkPath.replace('work','publ')
        path = filePath + '/rig/data/TransferWeightsDict.txt'

        fid = open(path , 'r')
        jntDict , targetDict = pickle.load(fid)
        fid.close()
        clear = []
        mc.textScrollList( 'SourceJntsScroll' , e=True , ra=True)
        mc.textScrollList( 'SourceJntsScroll' , e=True , append=clear)
        

        mc.textScrollList( 'SourceJntsScroll' , e=True , ra=True)
        mc.textScrollList( 'SourceJntsScroll' , e=True , append=jntDict)

        mc.textScrollList( 'TargetJntsScroll' , e=True , ra=True)
        mc.textScrollList( 'TargetJntsScroll' , e=True , append=clear)
        

        mc.textScrollList( 'TargetJntsScroll' , e=True , ra=True)
        mc.textScrollList( 'TargetJntsScroll' , e=True , append=targetDict)
        