import pymel.core as pm
import maya.cmds as mc

def createJnt( *args ):
    nCluster = mc.cluster( rel=True )
    nJoint = mc.createNode('joint')
    def snap( *args ):
        mc.delete( mc.parentConstraint ( *args , mo = False ))
        mc.select( cl = True )
    snap(nCluster,nJoint)
    mc.delete(nCluster)
  
def parentJnt(*args):
    jnt = mc.ls(sl=True)
    for i in range(len(jnt)):
        if not i == len(jnt)-1:
            mc.parent(jnt[i+1],jnt[i]) 
            
def hairUI ( *args ) : 

	if pm.window ( 'hairA' , exists = True ) :
		pm.deleteUI ( 'hairA' ) ; 
	else : pass ; 

	window = pm.window ( 'hairA', title = "rig_Hair" , 
	mnb = False , mxb = False , sizeable = False , rtf = True ) ; 
	pm.window ( 'hairA' , e = True , w = 300, h = 50 ) ; 
	
	row = pm.rowColumnLayout('createJnt', nc = 1, p = window)
	
	pm.button(label = 'createJnt', p = row, c = createJnt, w = 300)
	pm.button(label = 'parentJnt', p = row, c = parentJnt, w = 300)

	window.show () ; 

hairUI ( ) ;