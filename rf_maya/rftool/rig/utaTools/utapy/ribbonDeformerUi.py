# import sys
# sys.path.append("N:\Staff\FahR")
from utaTools.utapy import ribbonDeformer as rbnD
reload(rbnD)
import maya.cmds as mc
from utaTools.utapy import utaCore
reload(utaCore)

def buildRibbonDeformerUI(*args):
    # check if window exists
    if mc.window ( 'RibbonDeformer' , exists = True ) :
        mc.deleteUI ( 'RibbonDeformer' ) 
    else : pass

    winWidth = 250
    tmpWidth2 = [winWidth*0.25,winWidth*0.75]
    tmpWidth3 = [winWidth*0.15,winWidth*0.65, winWidth*0.2]
    tmpWidth4 = [winWidth*0.15, winWidth*0.4,winWidth*0.15, winWidth*0.3]
    tmpWidth42 = [winWidth*0.25, winWidth*0.25,winWidth*0.25, winWidth*0.25]
    window = mc.window('RibbonDeformer',title = 'Ribbon Deformer' , width = winWidth)
    ## Title Window
    utaCore.wintes(window = window , path = "D:/Ken_Pipeline/core/rf_maya/rftool/rig/utaTools/utaicon/uiIcon/" , pngIcon = 'ghost.png')
    mainCL = mc.columnLayout()
    mc.rowLayout(numberOfColumns=4 , columnWidth4=tmpWidth4)
    mc.text(label = 'Prefix', align='center' , width=tmpWidth4[0])
    mc.textField('prefix' ,ec='enter', ed = True , width=tmpWidth4[1])
    mc.text(label = ' Side ', align='center' , width=tmpWidth4[2])
    mc.textField('side' , tx = 'L' ,ec='enter', ed = True , width=tmpWidth4[3])
    mc.setParent('..')
    mc.radioButtonGrp('axis',l=' Axis',la3=['X','Y','Z'],numberOfRadioButtons=3,cw4=tmpWidth42,cl4=['left','left','left','left'],sl=2)
    mc.radioButtonGrp('direction',l='  Axis Direction',la2=['+','-'],numberOfRadioButtons=2,cw3=[winWidth*0.5, winWidth*0.25,winWidth*0.25],cl3=['left','left','left'],sl=1)
    mc.setParent('..')
    mc.rowLayout(numberOfColumns=3,columnWidth3=tmpWidth3)
    mc.text(label = 'PosA', align='center' , width=tmpWidth3[0])
    mc.textField('posA' ,ec='enter', ed = True , width=tmpWidth3[1])
    mc.button('posAButton' , l = '<<', width=tmpWidth3[2] , c = posA_cmd)
    mc.setParent('..')
    mc.rowLayout(numberOfColumns=3,columnWidth3=tmpWidth3)
    mc.text(label = 'PosB', align='center' , width=tmpWidth3[0])
    mc.textField('posB' ,ec='enter', ed = True , width=tmpWidth3[1])
    mc.button('posBButton' , l = '<<', width=tmpWidth3[2] , c = posB_cmd)
    mc.setParent('..')
    # mc.rowLayout(numberOfColumns=3,columnWidth3=tmpWidth3)
    # mc.text(label = 'Parent', align='center' , width=tmpWidth3[0])
    # mc.textField('parent' ,ec='enter', ed = True , width=tmpWidth3[1])
    # mc.button('parentButton' , l = '<<', width=tmpWidth3[2] , c = parent_cmd)
    mc.radioButtonGrp('hi',l='  hi',la2=['True','False'],numberOfRadioButtons=2,cw3=[winWidth*0.3, winWidth*0.35,winWidth*0.35],cl3=['left','left','left'],sl=1)
    mc.setParent('..')
    # mc.setParent('..')
    mc.rowLayout(numberOfColumns=2 , columnWidth2=tmpWidth2)
    mc.text(label = 'NumJnts', align='center' , width=tmpWidth2[0])
    mc.intField('numJnts' , v = 5  ,ec='enter', ed = True , width=tmpWidth2[1])
    mc.setParent('..')
    mc.rowLayout(numberOfColumns=2 , columnWidth2=tmpWidth2)
    mc.text(label = 'NurbValue', align='center' , width=tmpWidth2[0])
    mc.intField('nurbValue' , v = 5  ,ec='enter', ed = True , width=tmpWidth2[1])
    mc.setParent('..')
    # mc.rowLayout(numberOfColumns=2,columnWidth2=tmpWidth2)
    # mc.text(label = 'Ctrl Grp', align='center' , width=tmpWidth2[0])
    # mc.textField('ctrlGrp' , tx ='MainCtrl_Grp' ,ec='enter', ed = True , width=tmpWidth2[1])
    # mc.setParent('..')
    # mc.rowLayout(numberOfColumns=2,columnWidth2=tmpWidth2)
    # mc.text(label = 'Skin Grp', align='center' , width=tmpWidth2[0])
    # mc.textField('skinGrp' , tx ='Skin_Grp' ,ec='enter', ed = True , width=tmpWidth2[1])
    # mc.setParent('..')
    # mc.rowLayout(numberOfColumns=2,columnWidth2=tmpWidth2)
    # mc.text(label = 'Jnt Grp', align='center' , width=tmpWidth2[0])
    # mc.textField('jntGrp' , tx ='Jnt_Grp' ,ec='enter', ed = True , width=tmpWidth2[1])
    # mc.setParent('..')
    # mc.rowLayout(numberOfColumns=2,columnWidth2=tmpWidth2)
    # mc.text(label = 'Still Grp', align='center' , width=tmpWidth2[0])
    # mc.textField('stillGrp' , tx ='Still_Grp' ,ec='enter', ed = True , width=tmpWidth2[1])
    # mc.setParent('..')
    mc.rowLayout(numberOfColumns=1,columnWidth1=winWidth)
    mc.button('createRibbon',l='Create Ribbon', width=winWidth*1.01 , height = 40 , al='center' , c = run_cmd)
    mc.showWindow()
    

def posA_cmd(*args):
    strObj = ''
    obj = mc.ls(sl = True)
    if obj != []: strObj = obj[0]
    else: strObj=strObj
    mc.textField('posA', e=True , text=strObj) 
    mc.button('posAButton', e=True, bgc= (0.498039, 1, 0))

def posB_cmd(*args):
    strObj = ''
    obj = mc.ls(sl = True)
    if obj != []: strObj = obj[0]
    else: strObj=strObj
    mc.textField('posB', e=True , text=strObj) 
    mc.button('posBButton', e=True, bgc= (0.498039, 1, 0))

def parent_cmd(*args):
    strObj = ''
    obj = mc.ls(sl = True)
    if obj != []: strObj = obj[0]
    else: strObj=strObj
    mc.textField('parent', e=True , text=strObj) 
    mc.button('parentButton', e=True, bgc= (0.498039, 1, 0))

def run_cmd(*args):
    nameRbn = mc.textField('prefix' , q=True , tx=True)
    sideRbn = mc.textField('side' , q=True , tx=True)
    posARbn = mc.textField('posA' , q=True , tx=True)
    posBRbn = mc.textField('posB' , q=True , tx=True)
    hiRbnBut = mc.radioButtonGrp('hi',query=True, sl=True)
    # parentRbn = mc.textField('parent' , q=True , tx=True)
    numJntsRbn = mc.intField('numJnts' , q=True , v=True)
    nurbValueRbn = mc.intField('nurbValue' , q=True , v=True)
    # ctrlGrpRbn = mc.textField('ctrlGrp' , q=True , tx=True)
    # skinGrpRbn = mc.textField('skinGrp' , q=True , tx=True)
    # jntGrpRbn = mc.textField('jntGrp' , q=True , tx=True)
    # stillGrpRbn = mc.textField('stillGrp' , q=True , tx=True)
    Axis = mc.radioButtonGrp('axis',query=True, sl=True)
    Drt = mc.radioButtonGrp('direction',query=True, sl=True)
    if Axis == 1: ax='x'
    elif Axis == 2: ax='y'
    elif Axis == 3: ax='z'
    if Drt == 1:direction = '+'
    else: direction = '-'
    if hiRbnBut == 1: hiRbn= True
    else: hiRbn = False
    rbn = rbnD.run(name = nameRbn,
                        axis = '{}{}'.format(ax,direction),
                        posiA=posARbn,
                        posiB=posBRbn,
                        side=sideRbn,
                        hi = hiRbn ,
                        numJnts=numJntsRbn,
                        nurbValue = nurbValueRbn) 
    mc.button('createRibbon', e=True, bgc= (0.498039, 1, 0))


buildRibbonDeformerUI()