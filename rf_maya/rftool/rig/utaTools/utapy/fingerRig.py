import maya.cmds as mc
import maya.mel as mm
from ncmel import core
from ncmel import rigTools as rt
reload( core )
reload( rt )

# -------------------------------------------------------------------------------------------------------------
#
#  FINGER RIGGING MODULE
#  
# -------------------------------------------------------------------------------------------------------------

class Run( object ):

    def __init__(self , fngr          = 'Index' ,
                        fngrTmpJnt    =['Index1_L_TmpJnt' , 
                                        'Index2_L_TmpJnt' , 
                                        'Index3_L_TmpJnt' , 
                                        'Index4_L_TmpJnt' , 
                                        'Index5_L_TmpJnt' ] ,
                        parent        = 'Hand_L_Jnt' , 
                        armCtrl       = 'Arm_L_Ctrl' ,
                        innerCup      = 'HandCupInner_L_Jnt' , 
                        outerCup      = 'HandCupOuter_L_Jnt' ,
                        innerValue    = 1 , 
                        outerValue    = 1 ,
                        ctrlGrp       = 'Ctrl_Grp' ,
                        elem          = '' ,
                        side          = 'L' ,
                        size          = 1 
                 ):

        #-- Info
        name = '{}{}'.format(fngr , elem)

        if side == '':
            side = '_'
        else:
            side = '_{}_'.format(side)

        if 'L' in side:
            valueSide = 1
        elif 'R' in side:
            valueSide = -1
        else:
            valueSide = 1

        self.fngrJntDict = []
        self.fngrCtrlDict = []
        self.fngrGmblDict = []
        self.fngrZroDict = []
        self.fngrDrvDict = []
        self.fngrDrvAMdvDict = []
        self.fngrDrvBMdvDict = []
        self.fngrRxPmaDict = []
        self.fngrRyPmaDict = []
        self.fngrRzPmaDict = []
        self.fngrTyPmaDict = []

        self.fngrMember = len(fngrTmpJnt)
        
        slideValue = { '1' : 0 , '2' : 3.5 , '3' : -7 , '4' : 3.5 , '5' : 0 , '6' : 0 }
        scruchValue = { '1' : 1 , '2' : 3.5 , '3' : -8 , '4' : -8 , '5' : 0 , '6' : 0 }

        #-- Create Main Group
        self.fngrCtrlGrp = rt.createNode('transform' , '{}Ctrl{}Grp'.format(name , side))

        parCons = []
        sclCons = []
        if innerCup:
            parCons = mc.parentConstraint(innerCup , self.fngrCtrlGrp , mo=False)
            sclCons = mc.scaleConstraint(innerCup , self.fngrCtrlGrp , mo=False)

        if outerCup:
            parCons = mc.parentConstraint(outerCup , self.fngrCtrlGrp , mo=False)
            sclCons = mc.scaleConstraint(outerCup , self.fngrCtrlGrp , mo=False)

        if parCons:
            self.parCons = core.Dag(parCons[0])
            self.sclCons = core.Dag(sclCons[0])

            # Set Shortest
            self.parCons.attr('interpType').value = 2

            # Set Value Constraint
            if innerCup:
                self.parCons.attr('{}W0'.format(innerCup)).value = innerValue
                self.sclCons.attr('{}W0'.format(innerCup)).value = innerValue
                if outerCup:
                    self.parCons.attr('{}W1'.format(outerCup)).value = outerValue
                    self.sclCons.attr('{}W1'.format(outerCup)).value = outerValue
            else:
                self.parCons.attr('{}W0'.format(outerCup)).value = outerValue
                self.sclCons.attr('{}W0'.format(outerCup)).value = outerValue

        else:
            mc.parentConstraint(parent , self.fngrCtrlGrp , mo=False)
            mc.scaleConstraint(parent , self.fngrCtrlGrp , mo=False)

        #-- Create Controls
        self.fngrMainCtrl = rt.createCtrl('{}Attr{}Ctrl'.format(name,side) , 'stick' , 'blue')
        self.fngrMainZro = rt.addGrp(self.fngrMainCtrl)
        self.fngrMainZro.snap(fngrTmpJnt[0])

        #-- Adjust Shape Controls
        if 'L' in side:
            self.fngrMainCtrl.rotateShape(90 , 0 , 0)
        else:
            self.fngrMainCtrl.rotateShape(-90 , 0 , 0)

        #-- Add Attribute
        self.fngrMainCtrl.addAttr( 'fold' )
        self.fngrMainCtrl.addAttr( 'twist' )
        self.fngrMainCtrl.addAttr( 'slide' )
        self.fngrMainCtrl.addAttr( 'scruch' )
        self.fngrMainCtrl.addAttr( 'stretch' )

        for i in range(self.fngrMember -1):
            self.fngrMainCtrl.addTitleAttr('Finger{}'.format(i+1))
            self.fngrMainCtrl.addAttr( 'fold{}'.format(i+1) )
            self.fngrMainCtrl.addAttr( 'twist{}'.format(i+1) )
            self.fngrMainCtrl.addAttr( 'spread{}'.format(i+1) )

        self.fngrMainCtrlShape = core.Dag(self.fngrMainCtrl.shape)
        self.fngrMainCtrlShape.addAttr('detailCtrl' , 0 , 1)

        #-- Detail Controls
        for i in range(self.fngrMember):
            #-- Create Joint
            self.fngrJnt = rt.createJnt('{}{}{}Jnt'.format(name , i+1 , side) , fngrTmpJnt[i])
            self.fngrJntDict.append(self.fngrJnt)

            if not i == (self.fngrMember - 1):
                self.fngrCtrl = rt.createCtrl( '{}{}{}Ctrl'.format(name , i+1 , side) ,'cylinder' , 'red' )
                self.fngrGmbl = rt.addGimbal(self.fngrCtrl)
                self.fngrZro = rt.addGrp(self.fngrCtrl)
                self.fngrDrv = rt.addGrp(self.fngrCtrl , 'Drv')
                self.fngrZro.snap(self.fngrJnt)

                #-- Adjust Shape Controls
                for ctrl in (self.fngrCtrl , self.fngrGmbl):
                    ctrl.scaleShape( size * 0.35 )

                #-- Adjust Rotate Order
                for obj in (self.fngrCtrl , self.fngrJnt):
                    obj.setRotateOrder( 'xyz' )

                #-- Rig process
                mc.parentConstraint( self.fngrGmbl , self.fngrJnt , mo = False)
                rt.addSquash( self.fngrCtrl , self.fngrJnt , ax = ( 'sx' , 'sz' ))\

                #-- Create Node
                #-- DrvA == fold(x) , twist(y) , spread(z)
                #-- DrvB == slide(x) , scruch(y) , stretch(z)
                self.fngrDrvAMdv = rt.createNode( 'multiplyDivide' , '{}{}DrvA{}Mdv'.format( name , i+1 , side ))
                self.fngrDrvBMdv = rt.createNode( 'multiplyDivide' , '{}{}DrvB{}Mdv'.format( name , i+1 , side ))
                self.fngrRxPma = rt.createNode( 'plusMinusAverage' , '{}{}Rx{}Pma'.format( name , i+1 , side ))
                self.fngrRyPma = rt.createNode( 'plusMinusAverage' , '{}{}Ry{}Pma'.format( name , i+1 , side ))
                self.fngrRzPma = rt.createNode( 'plusMinusAverage' , '{}{}Rz{}Pma'.format( name , i+1 , side ))
                self.fngrTyPma = rt.createNode( 'plusMinusAverage' , '{}{}Ty{}Pma'.format( name , i+1 , side ))

                self.fngrMainCtrl.attr('fold{}'.format(i+1)) >> self.fngrDrvAMdv.attr('i1x')
                self.fngrMainCtrl.attr('twist{}'.format(i+1)) >> self.fngrDrvAMdv.attr('i1y')
                self.fngrMainCtrl.attr('spread{}'.format(i+1)) >> self.fngrDrvAMdv.attr('i1z')
                
                self.fngrMainCtrl.attr('slide') >> self.fngrDrvBMdv.attr('i1x')
                self.fngrMainCtrl.attr('scruch') >> self.fngrDrvBMdv.attr('i1y')
                self.fngrMainCtrl.attr('stretch') >> self.fngrDrvBMdv.attr('i1z')

                self.fngrDrvAMdv.attr('ox') >> self.fngrRxPma.attr('i1[0]')
                self.fngrDrvAMdv.attr('oy') >> self.fngrRyPma.attr('i1[0]')
                self.fngrDrvAMdv.attr('oz') >> self.fngrRzPma.attr('i1[0]')
                self.fngrDrvBMdv.attr('ox') >> self.fngrRxPma.attr('i1[1]')
                self.fngrDrvBMdv.attr('oy') >> self.fngrRxPma.attr('i1[2]')
                self.fngrDrvBMdv.attr('oz') >> self.fngrTyPma.attr('i1[0]')

                self.fngrRxPma.attr('o1') >> self.fngrDrv.attr('rx')
                self.fngrRyPma.attr('o1') >> self.fngrDrv.attr('ry')
                self.fngrRzPma.attr('o1') >> self.fngrDrv.attr('rz')
                self.fngrTyPma.attr('o1') >> self.fngrDrv.attr('ty')
                
                #-- Adjust Attribute Value
                self.fngrDrvAMdv.attr('i2x').value = -9
                self.fngrDrvAMdv.attr('i2y').value = 1
                self.fngrDrvAMdv.attr('i2z').value = 1
                self.fngrDrvBMdv.attr('i2x').value = slideValue['{}'.format(i+1)]
                self.fngrDrvBMdv.attr('i2y').value = scruchValue['{}'.format(i+1)]
                self.fngrDrvBMdv.attr('i2z').value = valueSide

                #-- Info
                self.fngrCtrlDict.append( self.fngrCtrl )
                self.fngrGmblDict.append( self.fngrGmbl )
                self.fngrZroDict.append( self.fngrZro )
                self.fngrDrvDict.append( self.fngrDrv )
                self.fngrDrvAMdvDict.append( self.fngrDrvAMdv )
                self.fngrDrvBMdvDict.append( self.fngrDrvBMdv )
                self.fngrRxPmaDict.append( self.fngrRxPma )
                self.fngrRyPmaDict.append( self.fngrRyPma )
                self.fngrRzPmaDict.append( self.fngrRzPma )
                self.fngrTyPmaDict.append( self.fngrTyPma )

        self.fngrFoldMdv = rt.createNode( 'multiplyDivide' , '{}Fold{}Mdv'.format( name , side ))
        self.fngrTwistMdv = rt.createNode( 'multiplyDivide' , '{}Twist{}Mdv'.format( name , side ))

        self.fngrMainCtrl.attr('fold') >> self.fngrFoldMdv.attr('i1x')
        self.fngrMainCtrl.attr('twist') >> self.fngrTwistMdv.attr('i1x')
        self.fngrFoldMdv.attr('i2x').value = -9

        self.fngrMainCtrlShape.attr('detailCtrl') >> self.fngrZroDict[0].attr('v')

        for i in range(self.fngrMember-1) :
            if not i == 0 :
                self.fngrFoldMdv.attr('ox') >> self.fngrRxPmaDict[i].attr('i1[3]')
                self.fngrTwistMdv.attr('ox') >> self.fngrRyPmaDict[i].attr('i1[1]')

        #-- Create Attribute Main Controls
        if armCtrl :
            self.armCtrl = core.Dag(armCtrl)
            self.armCtrlShape = core.Dag(self.armCtrl.shape)
            
            if not mc.objExists('{}.{}'.format(self.armCtrl , 'Finger')):
                self.armCtrl.addTitleAttr('Finger')

            for attr in ('fist','relax','slide','scruch','baseSpread','spread','baseBreak','break','baseFlex','flex') :
                if not mc.objExists( '{}.{}'.format( self.armCtrl , attr )) :
                    self.armCtrl.addAttr(attr)
            
            self.armCtrlShape.addTitleAttr( '{}'.format(fngr) )
            
            #-- Fist
            for i in range(self.fngrMember-1):
                fngrNum = '{}{}'.format(fngr,i+1)

                self.fistMainMdv = rt.createNode( 'multiplyDivide' , '{}{}Fist{}Mdv'.format( name , i+1 , side ))
                
                self.armCtrl.attr('fist') >> self.fistMainMdv.attr('i1x')
                self.armCtrl.attr('fist') >> self.fistMainMdv.attr('i1y')
                self.armCtrl.attr('fist') >> self.fistMainMdv.attr('i1z')

                self.fistMainMdv.attr('ox') >> self.fngrRxPmaDict[i].attr('i1[4]')
                self.fistMainMdv.attr('oy') >> self.fngrRyPmaDict[i].attr('i1[2]')
                self.fistMainMdv.attr('oz') >> self.fngrRzPmaDict[i].attr('i1[1]')

                for fistAttr in ('x','y','z'):
                    self.armCtrlShape.addAttr( '{}FistR{}'.format(fngrNum,fistAttr))
                    self.armCtrlShape.attr('{}FistR{}'.format(fngrNum,fistAttr)) >> self.fistMainMdv.attr('i2{}'.format(fistAttr))

            #-- Relax
            for i in range(self.fngrMember-1):
                fngrNum = '{}{}'.format(fngr,i+1)

                self.relaxMainMdv = rt.createNode( 'multiplyDivide' , '{}{}Relax{}Mdv'.format( name , i+1 , side ))
                
                self.armCtrl.attr('relax') >> self.relaxMainMdv.attr('i1x')
                self.armCtrl.attr('relax') >> self.relaxMainMdv.attr('i1y')
                self.armCtrl.attr('relax') >> self.relaxMainMdv.attr('i1z')

                self.relaxMainMdv.attr('ox') >> self.fngrRxPmaDict[i].attr('i1[5]')
                self.relaxMainMdv.attr('oy') >> self.fngrRyPmaDict[i].attr('i1[3]')
                self.relaxMainMdv.attr('oz') >> self.fngrRzPmaDict[i].attr('i1[2]')

                for relaxAttr in ('x','y','z'):
                    self.armCtrlShape.addAttr( '{}RelaxR{}'.format(fngrNum,relaxAttr))
                    self.armCtrlShape.attr('{}RelaxR{}'.format(fngrNum,relaxAttr)) >> self.relaxMainMdv.attr('i2{}'.format(relaxAttr))
            
            #-- Slide
            for i in range(self.fngrMember-1):
                fngrNum = '{}{}'.format(fngr,i+1)

                self.slideMainMdv = rt.createNode( 'multiplyDivide' , '{}{}Slide{}Mdv'.format( name , i+1 , side ))
                
                self.armCtrl.attr('slide') >> self.slideMainMdv.attr('i1x')
                self.armCtrl.attr('slide') >> self.slideMainMdv.attr('i1y')
                self.armCtrl.attr('slide') >> self.slideMainMdv.attr('i1z')

                self.slideMainMdv.attr('ox') >> self.fngrRxPmaDict[i].attr('i1[6]')
                self.slideMainMdv.attr('oy') >> self.fngrRyPmaDict[i].attr('i1[4]')
                self.slideMainMdv.attr('oz') >> self.fngrRzPmaDict[i].attr('i1[3]')

                for slideAttr in ('x','y','z'):
                    self.armCtrlShape.addAttr( '{}SlideR{}'.format(fngrNum,slideAttr))
                    self.armCtrlShape.attr('{}SlideR{}'.format(fngrNum,slideAttr)) >> self.slideMainMdv.attr('i2{}'.format(slideAttr))
            
            #-- Scruch
            for i in range(self.fngrMember-1):
                fngrNum = '{}{}'.format(fngr,i+1)

                self.scruchMainMdv = rt.createNode( 'multiplyDivide' , '{}{}Scruch{}Mdv'.format( name , i+1 , side ))
                
                self.armCtrl.attr('scruch') >> self.scruchMainMdv.attr('i1x')
                self.armCtrl.attr('scruch') >> self.scruchMainMdv.attr('i1y')
                self.armCtrl.attr('scruch') >> self.scruchMainMdv.attr('i1z')

                self.scruchMainMdv.attr('ox') >> self.fngrRxPmaDict[i].attr('i1[7]')
                self.scruchMainMdv.attr('oy') >> self.fngrRyPmaDict[i].attr('i1[5]')
                self.scruchMainMdv.attr('oz') >> self.fngrRzPmaDict[i].attr('i1[4]')

                for scruchAttr in ('x','y','z'):
                    self.armCtrlShape.addAttr( '{}ScruchR{}'.format(fngrNum,scruchAttr))
                    self.armCtrlShape.attr('{}ScruchR{}'.format(fngrNum,scruchAttr)) >> self.scruchMainMdv.attr('i2{}'.format(scruchAttr))


            #-- Spread
            self.armCtrlShape.addAttr( '{}BaseSpread'.format(fngr))
            self.armCtrlShape.addAttr( '{}Spread'.format(fngr))
            self.spreadMainMdv = rt.createNode( 'multiplyDivide' , '{}Spread{}Mdv'.format( name , side ))
            self.armCtrl.attr('baseSpread') >> self.spreadMainMdv.attr('i1x')
            self.armCtrl.attr('spread') >> self.spreadMainMdv.attr('i1y')
            self.spreadMainMdv.attr('ox') >> self.fngrRzPmaDict[0].attr('i1[5]')
            self.spreadMainMdv.attr('oy') >> self.fngrRzPmaDict[1].attr('i1[5]')
            self.armCtrlShape.attr('{}BaseSpread'.format(fngr)) >> self.spreadMainMdv.attr('i2x')
            self.armCtrlShape.attr('{}Spread'.format(fngr)) >> self.spreadMainMdv.attr('i2y')

            #-- Break
            self.armCtrlShape.addAttr( '{}BaseBreak'.format(fngr))
            self.armCtrlShape.addAttr( '{}Break'.format(fngr))
            self.breakMainMdv = rt.createNode( 'multiplyDivide' , '{}Break{}Mdv'.format( name , side ))
            self.armCtrl.attr('baseBreak') >> self.breakMainMdv.attr('i1x')
            self.armCtrl.attr('break') >> self.breakMainMdv.attr('i1y')
            self.breakMainMdv.attr('ox') >> self.fngrRzPmaDict[0].attr('i1[6]')
            self.breakMainMdv.attr('oy') >> self.fngrRzPmaDict[1].attr('i1[6]')
            self.armCtrlShape.attr('{}BaseBreak'.format(fngr)) >> self.breakMainMdv.attr('i2x')
            self.armCtrlShape.attr('{}Break'.format(fngr)) >> self.breakMainMdv.attr('i2y')

            #-- Flex
            self.armCtrlShape.addAttr( '{}BaseFlex'.format(fngr))
            self.armCtrlShape.addAttr( '{}Flex'.format(fngr))
            self.flexMainMdv = rt.createNode( 'multiplyDivide' , '{}Flex{}Mdv'.format( name , side ))
            self.armCtrl.attr('baseFlex') >> self.flexMainMdv.attr('i1x')
            self.armCtrl.attr('flex') >> self.flexMainMdv.attr('i1y')
            self.flexMainMdv.attr('ox') >> self.fngrRxPmaDict[0].attr('i1[8]')
            self.flexMainMdv.attr('oy') >> self.fngrRxPmaDict[1].attr('i1[8]')
            self.armCtrlShape.attr('{}BaseFlex'.format(fngr)) >> self.flexMainMdv.attr('i2x')
            self.armCtrlShape.attr('{}Flex'.format(fngr)) >> self.flexMainMdv.attr('i2y')
            
        #-- Adjust Hierarchy
        if not mc.objExists( 'Finger{}Ctrl{}Grp'.format( elem , side )) :
            self.allFngrGrp = rt.createNode( 'transform' , 'Finger{}Ctrl{}Grp'.format( elem , side ))
            self.allFngrGrp.snap( parent )
            self.allFngrGrp.parent( ctrlGrp )
        else :
            self.allFngrGrp = 'Finger{}Ctrl{}Grp'.format( elem , side )

        mc.parent( self.fngrMainZro , self.fngrZroDict[0] , self.fngrCtrlGrp )
        mc.parent( self.fngrCtrlGrp , self.allFngrGrp )
        mc.parent( self.fngrJntDict[0] , parent )

        for i in range(self.fngrMember) :
            if not i == 0 :
                mc.parent( self.fngrJntDict[i] , self.fngrJntDict[i-1] )

                if not i == self.fngrMember-1 :
                    mc.parent( self.fngrZroDict[i] , self.fngrGmblDict[i-1] )

        #-- Cleanup
        for obj in self.fngrCtrlDict :
            obj.lockHideAttrs( 'sx' , 'sy' , 'sz' , 'v' )

        for jnt in self.fngrJntDict :
            jnt.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        for grp in self.fngrZroDict :
            grp.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        for grp in self.fngrDrvDict :
            grp.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        self.fngrMainCtrl.lockHideAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        mc.select( cl = True )



class HandCup( object ):

    def __init__(self , name          = 'HandCupInner' ,
                        handCupTmpJnt = 'HandCupInner_L_TmpJnt' ,
                        armCtrl       = 'Arm_L_Ctrl' ,
                        parent        = 'Hand_L_Jnt' ,
                        ctrlGrp       = 'MainCtrl_Grp' ,
                        elem          = '' ,
                        side          = 'L' ,
                        size          = 1
                 ):

        #-- Info
        if side == '':
            side = '_'
        else:
            side = '_{}_'.format(side)

        #-- Create Main Group
        self.handCupCtrlGrp = rt.createNode('transform' , '{}{}Ctrl{}Grp'.format(name , elem , side))
        mc.parentConstraint( parent , self.handCupCtrlGrp , mo = False)
        mc.scaleConstraint( parent , self.handCupCtrlGrp , mo = False)

        #-- Create Controls
        self.handCupCtrl = rt.createCtrl( '{}{}{}Ctrl'.format(name , elem , side) , 'cube' , 'yellow' )
        self.handCupGmbl = rt.addGimbal( self.handCupCtrl )
        self.handCupZro = rt.addGrp( self.handCupCtrl )
        self.handCupDrv = rt.addGrp( self.handCupCtrl , 'Drv' )
        self.handCupZro.snap( handCupTmpJnt )

        #-- Adjust Shape Control
        for ctrl in (self.handCupCtrl , self.handCupGmbl):
            ctrl.scaleShape( size * 0.35 )

        #-- Create Joint
        self.handCupJnt = rt.createJnt( '{}{}{}Jnt'.format(name , elem , side) , handCupTmpJnt)

        #-- Adjust Rotate Order
        for obj in (self.handCupCtrl , self.handCupJnt):
            obj.setRotateOrder( 'xyz' )

        #-- Rig process
        mc.parentConstraint(self.handCupGmbl , self.handCupJnt , mo = False)
        mc.scaleConstraint(self.handCupGmbl , self.handCupJnt , mo = False)
        self.handCupJnt.attr('ssc').value = 0

        if armCtrl:
            self.armCtrl = core.Dag(armCtrl)
            self.armCtrlShape = core.Dag(self.armCtrl.shape)

            if not mc.objExists('{}.{}'.format(self.armCtrl , 'Finger')):
                self.armCtrl.addTitleAttr('Finger')

            for attr in ('fist','relax','slide','scruch','baseSpread','spread','baseBreak','break','baseFlex','flex') :
                if not mc.objExists( '{}.{}'.format( self.armCtrl , attr )) :
                    self.armCtrl.addAttr(attr)

            if not mc.objExists('{}.{}'.format(self.armCtrlShape , '{}{}FistRx'.format(name , elem))):
                self.armCtrlShape.addAttr('{}{}FistRx'.format(name , elem))
                self.armCtrlShape.addAttr('{}{}FistRy'.format(name , elem))
                self.armCtrlShape.addAttr('{}{}FistRz'.format(name , elem))
                if 'Inner' in name:
                    self.armCtrlShape.attr('{}{}FistRx'.format(name , elem)).value = 0
                    self.armCtrlShape.attr('{}{}FistRy'.format(name , elem)).value = 0
                    self.armCtrlShape.attr('{}{}FistRz'.format(name , elem)).value = 0
                else:
                    self.armCtrlShape.attr('{}{}FistRx'.format(name , elem)).value = 0
                    self.armCtrlShape.attr('{}{}FistRy'.format(name , elem)).value = -1
                    self.armCtrlShape.attr('{}{}FistRz'.format(name , elem)).value = 0

        self.handCupMdv = rt.createNode('multiplyDivide' , '{}{}{}Mdv'.format(name , elem , side))
        #self.handCupPma = rt.createNode('plusMinusAverage' , '{}{}{}Pma'.format(name , elem , side))

        self.armCtrl.attr('fist') >> self.handCupMdv.attr('i1x')
        self.armCtrl.attr('fist') >> self.handCupMdv.attr('i1y')
        self.armCtrl.attr('fist') >> self.handCupMdv.attr('i1z')

        self.armCtrlShape.attr('{}{}FistRx'.format(name , elem)) >> self.handCupMdv.attr('i2x')
        self.armCtrlShape.attr('{}{}FistRy'.format(name , elem)) >> self.handCupMdv.attr('i2y')
        self.armCtrlShape.attr('{}{}FistRz'.format(name , elem)) >> self.handCupMdv.attr('i2z')

        # self.handCupMdv.attr('ox') >> self.handCupPma.attr('i1[0]')
        # self.handCupMdv.attr('oy') >> self.handCupPma.attr('i1[1]')
        #self.handCupPma.attr('o1') >> self.handCupDrv.attr('ry')
        self.handCupMdv.attr('ox') >> self.handCupDrv.attr('rx')
        self.handCupMdv.attr('oy') >> self.handCupDrv.attr('ry')
        self.handCupMdv.attr('oz') >> self.handCupDrv.attr('rz')

        #-- Adjust Hierarchy
        if not mc.objExists( 'Finger{}Ctrl{}Grp'.format(elem , side)):
            self.allFngrGrp = rt.createNode('transform' , 'Finger{}Ctrl{}Grp'.format(elem , side))
            self.allFngrGrp.snap( parent )
            self.allFngrGrp.parent( ctrlGrp )
        else:
            self.allFngrGrp = 'Finger{}Ctrl{}Grp'.format(elem , side)

        mc.parent( self.handCupZro , self.handCupCtrlGrp )
        mc.parent( self.handCupCtrlGrp , self.allFngrGrp )
        mc.parent( self.handCupJnt , parent )

        #-- Cleanup
        self.handCupCtrl.lockHideAttrs('sx' , 'sy' , 'sz' , 'v')

        for grp in (self.handCupJnt , self.handCupZro , self.handCupDrv):
            grp.lockAttrs('tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v')

        mc.select( cl = True)