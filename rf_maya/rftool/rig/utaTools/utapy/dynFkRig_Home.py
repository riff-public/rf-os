import maya.cmds as mc
from utaTools.utapy import utaCore
reload(utaCore)
##--------------------------------------------------------------------------------
# import maya.cmds as mc
# from utaTools.utapy import dynFkRig
# reload(dynFkRig)
# dynFkRig.dynFkRig(tmpJnt = ['dynIkHair1_Jnt', 'dynIkHair2_Jnt', 'dynIkHair3_Jnt', 'dynIkHair4_Jnt', 'dynIkHair5_Jnt', 'dynIkHair6_Jnt'],
                        # curve = '', 
                        # divideJnt = True,
                        # name = 'Dyn', 
                        # parent = '',
                        # elem = 'Hair',
                        # side = '',
                        # ctrlGrp = '',
                        # skinGrp = '',
                        # ikhGrp = '',
                        # jntGrp = '',
                        # stillGrp = '')
##--------------------------------------------------------------------------------

def dynFkRig(tmpJnt = [],curve = '', divideJnt = True, parent = '', name = '', elem = '', side = '', ctrlGrp = '', skinGrp = '', ikhGrp = '', jntGrp = '', stillGrp = '',*args):
    fkCtrlGrp = []
    fkGmblCtrlGrp = []
    positionJnt = []
    lenPositionJnt = len(positionJnt)
    objDrvJnt = []
    objCtrl = []

    if side == 'L':
        side = '_L_'
    elif side == 'R':
        side = '_R_'
    elif side == '':
        side = '_'

    ## Group
    # objSkinGrp= mc.group(n = '{}{}Skin{}Grp'.format(name, elem, side), em = True)
    objCtrlGrp = mc.group(n = '{}{}Ctrl{}Grp'.format(name, elem, side), em = True)
    objIkhGrp= mc.group(n = '{}{}Ikh{}Grp'.format(name, elem, side), em = True)
    objStillGrp = mc.group(n = '{}{}Still{}Grp'.format(name, elem, side), em = True)

    ## Generate Curve spine Ik
    if not curve:
        value = []
        for jnt in tmpJnt:
            positionJnt = mc.xform( jnt , q = True, t = True , ws = True)
            value.append(tuple(positionJnt))
        curve = mc.curve( p=value,d = 3, n = '{}{}{}Crv'.format(name, elem, side))

    ## duplicate curve
    crvGoal= mc.duplicate(curve, n= '{}{}Goal{}Crv'.format(name, elem,  side))[0]
    crvParticle= mc.duplicate(curve, n= '{}{}Particle{}Crv'.format(name, elem,  side))[0]
    ## blendShape
    mc.select(crvGoal, r = True)   
    mc.select(crvParticle, add= True)
    mc.select(curve, add= True)
    bshLists = mc.blendShape(name = '{}{}{}Bsh'.format(name, elem, side))

    ## Create spineIk
    spineIkName = '{}{}Ik{}Ikh'.format(name, elem.capitalize(), side)
    utaCore.createIkhSpine (strJnt = tmpJnt[0], endJnt = tmpJnt[-1] ,curve = curve , name = spineIkName, sol = 'ikSplineSolver')
    ''' ikSplineSolver
        ikRPsolver
        ikSCsolver'''

    ## generate ik Control
    crvFkCtrlPos= mc.duplicate(curve, n= '{}fkCtrlPos{}Crv'.format(name, side))[0]

    if divideJnt:
        fkCtrlCount = len(tmpJnt)//2
        jointFnt, jointBck, jointAll = utaCore.createJointOnCurve(cur_name = crvFkCtrlPos, name = '{}{}{}Jnt'.format(name, elem, side), elem = '', side = side,  span_curve = fkCtrlCount, rebuildCurve = False, numberPass = 2)
        ## aim axis joint
        aimJointAxis(jnt = jointAll)
        ## generate control

        for i in range(len(jointAll)):
            nameNew, sideNew, lastNameNew = utaCore.splitName(sels = [jointAll[i]])
            if sideNew == '_L_':
                sideNew == 'L'
            elif sideNew == '_R_':
                sideNew == 'R'
            else:
                sideNew = '_'
            objNew = mc.rename(jointAll[i], '{}{}Drv{}{}Jnt'.format(name, elem, i+1, sideNew))
            mc.select(objNew)
            grpCtrlName, grpCtrlOfsName, grpCtrlParsName, ctrlName, GmblCtrlName = utaCore.createControlCurve(curveCtrl = 'circle', parentCon = False, connections = False, geoVis = False)
             
            fkCtrlGrp.append(grpCtrlName)
            fkGmblCtrlGrp.append(GmblCtrlName)
            objDrvJnt.append(objNew)
            objCtrl.append(ctrlName)

    else:
        fkCtrlCount = len(tmpJnt)
        jointFnt, jointBck, jointAll = utaCore.createJointOnCurve(cur_name = crvFkCtrlPos, name = '{}{}{}Jnt'.format(name, elem, side), elem = '', side = side,  span_curve = fkCtrlCount, rebuildCurve = False, numberPass = 2)
        ## aim axis joint
        aimJointAxis(jnt = jointAll)
        ## generate control
        for i in range(0, len(jointAll), 1):
            nameNew, sideNew, lastNameNew = utaCore.splitName(sels = [jointAll[i]])
            if sideNew == '_L_':
                sideNew == 'L'
            elif sideNew == '_R_':
                sideNew == 'R'
            else:
                sideNew = '_'
            objNew = mc.rename(jointAll[i], '{}{}Drv{}{}Jnt'.format(name, elem, i+1, sideNew))
            mc.select(objNew)
            grpCtrlName, grpCtrlOfsName, grpCtrlParsName, ctrlName, GmblCtrlName = utaCore.createControlCurve(curveCtrl = 'circle', parentCon = False, connections = False, geoVis = False)
             
            fkCtrlGrp.append(grpCtrlName)
            fkGmblCtrlGrp.append(GmblCtrlName)
            objDrvJnt.append(objNew)
            objCtrl.append(ctrlName)
    mc.delete(crvFkCtrlPos)
    ## parent joint Drv
    for i in range(len(fkCtrlGrp)):
        if not i == 0:
            mc.parent(fkCtrlGrp[i], fkGmblCtrlGrp[i-1])
        mc.parent(objDrvJnt[i], fkGmblCtrlGrp[i])  
    ## bindSkin obj Drv jnt
    utaCore.bindSkinCluster(jntObj = objDrvJnt, obj = [crvGoal])

    ## Create Attribute
    ctrlFunction = objCtrl[-1]
    onOffAttr = utaCore.addAttrCtrl (ctrl = ctrlFunction, elem = ['dynamicBlendOnOff'], min = 0, max = 1, at = 'long', dv = 0)[0]
    utaCore.assignColor(obj = [ctrlFunction], col = 'yellow')
    mc.connectAttr('{}.{}'.format(ctrlFunction, onOffAttr),  '{}.{}'.format(bshLists[0], crvParticle))
    particleRev = mc.createNode('reverse', n = '{}{}Rev'.format(name, side))
    mc.connectAttr('{}.{}'.format(ctrlFunction, onOffAttr),  '{}.inputX'.format(particleRev))
    mc.connectAttr('{}.outputX'.format(particleRev), '{}.{}'.format(bshLists[0], crvGoal))

    ## Create soft body 
    CrvParticleNode = mc.soft(crvParticle, c = True )[0]
    mc.select(CrvParticleNode)

    ## Create turbulenceNode
    # turbulenceNode = mc.turbulence(pos= (0, 0 , 0), m= 5 ,att= 1 ,f= 1,phaseX= 0 ,phaseY =0 ,phaseZ= 0 ,noiseLevel= 0 ,noiseRatio= 0.707,mxd= -1,  vsh= 'none' ,vex= 0 ,vof = (0, 0, 0),vsw =360 ,tsr= 0.5 )[0]
    # mc.connectDynamic (CrvParticle, f = turbulenceNode)
    # mc.setAttr('{}.magnitude'.format(turbulenceNode), 2000)

    ## Generate attribute conserve
    goalSmoothnessAttr = utaCore.addAttrCtrl (ctrl = ctrlFunction, elem = ['goalSmoothness'], min = 0, max = 100, at = 'float', dv = 10)[0]
    goalSmoothnessMdv = mc.createNode('multiplyDivide', n = '{}{}GoalSmoothness{}Mdv'.format(name, elem, side))
    mc.connectAttr('{}.{}'.format(ctrlFunction, goalSmoothnessAttr), '{}.input1X'.format(goalSmoothnessMdv))
    mc.setAttr('{}.input2X'.format(goalSmoothnessMdv), 0.1)
    mc.connectAttr('{}.outputX'.format(goalSmoothnessMdv), '{}Shape.goalSmoothness'.format(CrvParticleNode))


    ## Create Goal
    mc.goal(CrvParticleNode, g = crvGoal, w= 0.5 ,utr= 0)
		
    ## Create remap Node
    arrayMapperNode = mc.arrayMapper(target = '{}Shape'.format(CrvParticleNode), destAttr = 'goalPP' , inputV = 'ageNormalized', type = 'ramp')[0]
    mc.disconnectAttr('{}Shape.ageNormalized'.format(CrvParticleNode), '{}.vCoordPP'.format(arrayMapperNode))
    mc.setAttr('{}Shape.lifespanMode'.format(CrvParticleNode), 0)
    ## reverse map ramp1 left = white, right = black
    mc.setAttr('ramp1.colorEntryList[1].position', 0)
    mc.setAttr('ramp1.colorEntryList[0].position', 1)
    mc.setAttr("ramp1.colorEntryList[1].color", 1 ,1 ,1,type = 'double3')
    mc.setAttr("ramp1.colorEntryList[0].color", 0 ,0 ,0,type = 'double3')
    mc.connectAttr('{}Shape.mass'.format(CrvParticleNode), '{}.vCoordPP'.format(arrayMapperNode))

    ## SetAttribute pt particle
    cvPoint = len(mc.getAttr('{}.cv[*]'.format(crvParticle)))
    for i in range(0,cvPoint):
        if i == 0:
            pointFirst = 0.001
            mc.particle('{}Shape'.format(CrvParticleNode), e = True, order = i , at = "mass" , fv= pointFirst)
        else:
            value = cvPoint - 1
            answer = float(i)/value
            mc.particle('{}Shape'.format(CrvParticleNode),e = True,order = i ,at = "mass", fv= answer)

    ## Generate attribute goal min weight
    goalMinweightAttr = utaCore.addAttrCtrl (ctrl = ctrlFunction, elem = ['goalMinWeight'], min = 0, max = 100, at = 'float', dv = 20)[0]
    goalMinWeightMdv = mc.createNode('multiplyDivide', n = '{}{}GoalMinWeight{}Mdv'.format(name, elem, side))
    mc.connectAttr('{}.{}'.format(ctrlFunction, goalMinweightAttr), '{}.input1X'.format(goalMinWeightMdv))
    mc.setAttr('{}.input2X'.format(goalMinWeightMdv), 0.01)
    mc.connectAttr('{}.outputX'.format(goalMinWeightMdv), '{}.minValue'.format(arrayMapperNode))
    mc.connectAttr('{}.{}'.format(ctrlFunction, onOffAttr), '{}Shape.isDynamic'.format(CrvParticleNode))

    ## Generate attribute conserve
    conserveAttr = utaCore.addAttrCtrl (ctrl = ctrlFunction, elem = ['conserve'], min = 0, max = 100, at = 'float', dv = 75)[0]
    conserveMdv = mc.createNode('multiplyDivide', n = '{}{}Conserve{}Mdv'.format(name, elem, side))
    mc.connectAttr('{}.{}'.format(ctrlFunction, conserveAttr), '{}.input1X'.format(conserveMdv))
    mc.setAttr('{}.input2X'.format(conserveMdv), 0.01)
    mc.connectAttr('{}.outputX'.format(conserveMdv), '{}Shape.conserve'.format(CrvParticleNode))


# setAttr "DynHairParticle_CrvParticleShape.goalSmoothness" 2;

    ## wrap group
    if parent:
        utaCore.parentScaleConstraintLoop(obj = parent, lists = [objCtrlGrp],par = True, sca = True, ori = False, poi = False)
        try:
            mc.parent(tmpJnt[0], parent)
        except:pass
    mc.parent(fkCtrlGrp[0], objCtrlGrp)
    mc.parent(crvGoal, crvParticle, spineIkName, curve, objStillGrp)
    mc.parent(spineIkName, objIkhGrp)

    ## wrap to base grp 
    if ctrlGrp or ikhGrp or stillGrp:
        mc.parent(objCtrlGrp, ctrlGrp)
        mc.parent(objIkhGrp, ikhGrp)
        mc.parent(objStillGrp, stillGrp)

    ## set inheritsTransform
    listsInher = [objStillGrp]
    for each in listsInher:
        mc.setAttr('{}.inheritsTransform'.format(each), 0)
    mc.select(cl = True)


def aimJointAxis(jnt = []):
    mc.select(jnt)  
    utaCore.parentObj(jntName = '', Side = '', lastName = '')
    mc.select(cl = True)
    for each in jnt:
        obj = mc.listRelatives(each, c = True)
        if not obj == None:
            mc.parent(obj[0], w = True)
