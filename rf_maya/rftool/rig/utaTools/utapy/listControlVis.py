from collections import OrderedDict

def layerControlAttr (*args):
    ## Tital Ctrl Attr -----------------------------------------------------
    mainAttrLayerVis = OrderedDict()
    mainAttrLayerVis['Control'] = [ 'MasterVis', 
                                    'BlockingVis', 
                                    'SecondaryVis', 
                                    'AddRigVis']
    mainAttrLayerVis['Facial'] =  [ 'FacialVis', 
                                    'DetailVis']
    mainAttrLayerVis['Geometry'] = ['GeoVis']
                        

    return  mainAttrLayerVis

def listControlVis (*args):
    objLayerVis = OrderedDict()
    objLayerVis['MasterVis'] = [  'Offset_Ctrl'   ] 

    objLayerVis['BlockingVis'] = [
                                    'RootCtrl_Grp', 
                                    'SpineCtrl_Grp',
                                    'SpineBodyCtrl_Grp',
                                    'PelvisCtrl_Grp', 
                                    'NeckCtrl_Grp', 
                                    'HeadCtrl_Grp', 
                                    'ClavCtrl_L_Grp', 
                                    'ClavCtrl_R_Grp', 
                                    'UpLegIkCtrlZro_L_Grp', 
                                    'UpLegIkCtrlZro_R_Grp',
                                    'SpineMainCtrl_Grp', 
                                    'LegCtrl_L_Grp',
                                    'LegCtrl_R_Grp',
                                    'ArmCtrlZro_L_Grp',
                                    'ArmCtrlZro_R_Grp',
                                    'ArmCtrl_L_Grp',
                                    'ArmCtrl_R_Grp',
                                    'ArmIkCtrl_L_Grp',
                                    'ArmIkCtrl_R_Grp',
                                    'UpArmFkCtrlZro_L_Grp',
                                    'UpArmFkCtrlZro_R_Grp',
                                    'LegCtrlZro_L_Grp',
                                    'LegCtrlZro_R_Grp',
                                    'LegIkCtrl_L_Grp',
                                    'LegIkCtrl_R_Grp',
                                    'LegFntCtrl_L_Grp',
                                    'LegFntCtrl_R_Grp',
                                    'LegBckCtrl_L_Grp',
                                    'LegBckCtrl_R_Grp',
                                    ] 

    objLayerVis['SecondaryVis'] = [
                                    'EyeCtrl_Grp', 
                                    'ArmRbnCtrl_L_Grp', 
                                    'ArmRbnCtrl_R_Grp', 
                                    'LegRbnCtrlZro_L_Grp', 
                                    'UpLegRbnCtrl_L_Grp', 
                                    'LowLegRbnCtrl_L_Grp', 
                                    'LegRbnCtrlZro_R_Grp', 
                                    'UpLegRbnCtrl_R_Grp', 
                                    'LowLegRbnCtrl_R_Grp', 
                                    'FingerCtrl_L_Grp', 
                                    'FingerCtrl_R_Grp', 
                                    'NeckRbnCtrl_Grp',
                                    'EarCtrl_L_Grp',
                                    'EarCtrl_R_Grp',
                                    'ArmRbnCtrlZro_L_Grp',
                                    'ArmRbnCtrlZro_R_Grp', 
                                    'UpArmRbnCtrl_L_Grp', 
                                    'UpArmRbnCtrl_R_Grp', 
                                    'ForearmRbnCtrl_L_Grp',
                                    'ForearmRbnCtrl_R_Grp',
                                    'EyeDotSubCtrl_L_Grp',
                                    'EyeDotSubCtrl_R_Grp',
                                    'LegFntRbnCtrlZro_L_Grp',
                                    'UpLegFntRbnCtrl_L_Grp',
                                    'LowLegFntRbnCtrl_L_Grp',
                                    'LegFntRbnCtrlZro_L_Grp',
                                    'UpLegFntRbnCtrl_L_Grp',
                                    'LowLegFntRbnCtrl_L_Grp',
                                    'LegFntRbnCtrlZro_R_Grp',
                                    'UpLegFntRbnCtrl_R_Grp',
                                    'LowLegFntRbnCtrl_R_Grp',
                                    'UpKneeBckRbnCtrlZro_L_Grp',
                                    'LowKneeBckRbnCtrlZro_L_Grp',
                                    'UpLegBckRbnCtrl_L_Grp',
                                    'MidLegBckRbnCtrl_L_Grp',
                                    'LowLegBckRbnCtrl_L_Grp',
                                    'UpKneeBckRbnCtrlZro_R_Grp',
                                    'LowKneeBckRbnCtrlZro_R_Grp',
                                    'UpLegBckRbnCtrl_R_Grp',
                                    'MidLegBckRbnCtrl_R_Grp',
                                    'LowLegBckRbnCtrl_R_Grp',
                                    ]
    objLayerVis['AddRigVis'] = [  'AddCtrl_Grp']

    objLayerVis['FacialVis'] = [ 'FacialCtrl_Grp',
                                    'JawCtrl_Grp',
                                    'IrisSubCtrl_L_Grp', 
                                    'IrisSubCtrl_R_Grp',
                                    'PupilSubCtrl_L_Grp',
                                    'PupilSubCtrl_R_Grp'
                                    ]
                    
    objLayerVis['DetailVis'] = ['DtlRigCtrlHi_Grp', 'DtlRigCtrlMid_Grp', 'DtlRigCtrlLow_Grp']

    objLayerVis['GeoVis'] = ['Geo_Grp']

    return  objLayerVis

def checkListsDefaultAttr (*args):
    lists = ['arthurBabyWrap']
    return lists

