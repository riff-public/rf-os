#  DogDtlRig
import pkmel.fkRig as pfk
reload(pfk)
import pkmel.fkGroupRig as pfkg
reload(pfkg)

def dogDtlRig():
#tail 
    tailRig = pfk.FkRig(
                        parent='',
                        animGrp='tailAnim_grp', 
                        charSize=1,
                        tmpJnt=[
                                'tail1CEN_Jnt',
                                'tail2CEN_Jnt',
                                'tail3CEN_Jnt',
                                'tail4CEN_Jnt',
                                'tail5CEN_Jnt'
                                ],
                        name='tail',
                        side='',
                        ax='y',
                        shape='circle'
                    )

#earRig LFT
    earRig = pfk.FkRig(
                        parent='',
                        animGrp='earAnim_grp', 
                        charSize=1,
                        tmpJnt=[
                                'ear1LFT_Jnt',
                                'ear2LFT_Jnt',
                                'ear3LFT_Jnt',
                                'ear4LFT_Jnt',                          
                                ],
                        name='ear',
                        side='LFT',
                        ax='y',
                        shape='circle'
                    ) 
#earRig RGT
    earRig = pfk.FkRig(
                        parent='',
                        animGrp='earAnim_grp', 
                        charSize=1,
                        tmpJnt=[
                                'ear1RGT_Jnt',
                                'ear2RGT_Jnt',
                                'ear3RGT_Jnt',
                                'ear4RGT_Jnt',                          
                                ],
                        name='ear',
                        side='RGT',
                        ax='y',
                        shape='circle'
                    )
#moustache
    moustacheRig = pfk.FkRig(
                        parent='',
                        animGrp='moustacheAnim_grp', 
                        charSize=1,
                        tmpJnt=[
                                'moustache1Dn_Jnt',
                                'moustache2Dn_Jnt',
                                'moustache3Dn_Jnt',
                                'moustache4Dn_Jnt',                    
                                ],
                        name='moustache',
                        side='',
                        ax='y',
                        shape='circle'
                    )                           
#moustache LFT
    moustacheRig = pfk.FkRig(
                        parent='',
                        animGrp='moustacheAnim_grp', 
                        charSize=1,
                        tmpJnt=[
                                'moustache1LFT_Jnt',
                                'moustache2LFT_Jnt',
                                'moustache3LFT_Jnt',
                                'moustache4LFT_Jnt',                    
                                ],
                        name='moustache',
                        side='LFT',
                        ax='y',
                        shape='circle'
                    )  
#moustache RGT
    moustacheRig = pfk.FkRig(
                        parent='',
                        animGrp='moustacheAnim_grp', 
                        charSize=1,
                        tmpJnt=[
                                'moustache1RGT_Jnt',
                                'moustache2RGT_Jnt',
                                'moustache3RGT_Jnt',
                                'moustache4RGT_Jnt',                    
                                ],
                        name='moustache',
                        side='RGT',
                        ax='y',
                        shape='circle'
                    )  
#tongue
    tongueRig = pfk.FkRig(
                        parent='',
                        animGrp='tongueAnim_grp', 
                        charSize=1,
                        tmpJnt=[
                                'tongue1_jnt',
                                'tongue2_jnt',
                                'tongue3_jnt',
                                'tongue4_jnt', 
                                'tongue5_jnt',   
                                'tongue6_jnt',                      
                                ],
                        name='tongue',
                        side='',
                        ax='y',
                        shape='square'
                    ) 
#Teeth Upper
    upperTeethRig = pfkg.FkGroupRig(
                        parent='',
                        animGrp='upperTeethAnim_grp', 
                        charSize=1,
                        tmpJnt=[
                                'upperTeeth_jnt',                                                  
                                ],
                        name='upperTeeth',
                        side='',
                        shape='square'
                    ) 
                    
#Teeth Lower
    lowerTeethRig = pfkg.FkGroupRig(
                        parent='',
                        animGrp='lowerTeethAnim_grp', 
                        charSize=1,
                        tmpJnt=[
                                'lowerTeeth_jnt',                                                  
                                ],
                        name='lowerTeeth',
                        side='',
                        shape='square'
                    ) 
                    