import maya.cmds as mc
import maya.mel as mm
from utaTools.utapy import DeleteGrp 
reload(DeleteGrp)

def DtlGrpHiMidLow():
	GrpDtlHi = mc.createNode('transform', n = 'DtlRigCtrlHi_Grp')
	GrpDtlMid = mc.createNode('transform', n = 'DtlRigCtrlMid_Grp')
	GrpDtlLow = mc.createNode ('transform', n = 'DtlRigCtrlLow_Grp')
	GrpDtlMidLow = mc.createNode('transform', n = 'DtlRigCtrlMidLow_Grp')

	listHi = mc.ls('EyeBrow*DtlRigCtrlZr_*_Grp',
				'EyeSkt*DtlRigCtrlZr_*_Grp',
				'Cheek*DtlRigCtrlZr_*_Grp',
				'EyeBrowDtlRigCtrlZr_Grp',
				'NsDtlRigCtrlZr_Grp',)
	listMid = mc.ls('NsDtlRigCtrlZr_*_Grp')
	
	listLo = mc.ls('PuffDtlRigCtrlZr_*_Grp',
				'ChinDtlRigCtrlZr_Grp',
				'Mouth*DtlRigCtrlZr*Grp')

	for each in listHi:
		try:
			mc.parent(each,	GrpDtlHi)
		except: pass
	for each in listMid:
		try:
			mc.parent(each, GrpDtlMid)
		except: pass
	for each in listLo:
		try:
			mc.parent(each,	GrpDtlLow)
		except: pass

	mc.select('DtlRigCtrl_Grp')
	DeleteGrp.deleteGrp()


