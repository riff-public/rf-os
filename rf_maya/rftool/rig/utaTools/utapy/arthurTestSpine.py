import maya.cmds as mc
import pymel.core as pm



from utaTools.nuTools.rigTools import rbnSpineRig as rsr
reload(rsr)

def test(*args):
    print 'Creating Torso Rig'
    spineJnts = [   pm.PyNode('pelvis_tmpJnt'), 
                    pm.PyNode('spine1_tmpJnt'), 
                    pm.PyNode('spine2_tmpJnt'), 
                    pm.PyNode('spine3_tmpJnt'), 
                    pm.PyNode('spine4_tmpJnt'),]
    # print 'ken'
    spineRig = rsr.RbnSpineRig(jnts=spineJnts, 
                    elem='spine',
                    aimAxis='+y', 
                    upAxis='+z', 
                    worldUpAxis='+z', 
                    # parent = pm.PyNode(rootRig.root_jnt.name),
                    parent = 'null1',
                    animGrp='null2',
                    skinGrp='null2',
                    utilGrp='null2',
                    stillGrp='null2')
    spineRig.rig()