import maya.cmds as mc
import pymel.core as pm
import maya.mel as mm 
from utaTools.utapy import utaCore
reload(utaCore)
from lpRig import rigTools as lrr
reload(lrr)
from utaTools.pkmel import weightTools
reload(weightTools)
from utaTools.utapy import DeleteGrp  
reload(DeleteGrp)
from utaTools.pkmel import weightTools
reload(weightTools)
from utaTools.pkmel import rigTools as rigTools
reload( rigTools )
from utaTools.utapy import utaCore
reload(utaCore)





# from utaTools.utapy import duplicateSkinPos as dsp
# reload(dsp)
# dsp.run(name = '', 
#       side = '', 
#       elem = '', 
#       mirrorBehavior = True, 
#       createGeo = True,
#       baseGeoGrp = 'samanakhaLeader_md:AllGeo_Grp', 
#       listsJnt = 'SkinPosJnt_Grp', 
#       baseJnt = 'Base_Jnt')

def run(name, side, elem, mirrorBehavior = True, createGeo = True, baseGeoGrp = 'samanakhaLeader_md:AllGeo_Grp', listsJnt = 'SkinPosJnt_Grp', baseJnt = 'Base_Jnt', *args):
    if not mc.objExists('SkinPosGeo_Grp'):
        SkinPosGrp = mc.group(n = 'SkinPosGeo_Grp', em = True)
    else:
        SkinPosGrp = 'SkinPosGeo_Grp'
    if not mc.objExists('SkinPosJnt_Grp'):
        SkinJntGrp = mc.group(n = 'SkinPosJnt_Grp', em = True)
    else:
        SkinJntGrp = 'SkinPosJnt_Grp'
    # listsJntAll = mc.listRelatives(listsJnt, c = True)
    ## >> Create Joint on vertex
    jointAll= createJointSkinPos(name = name, side = side, elem = elem, mirrorBehavior = mirrorBehavior)
    for each in jointAll:
        if each:
            mc.parent(each, SkinJntGrp)
    ## >> Duplicate Geo of joint
    if createGeo:
        if not listsJnt:
            listsJnt = jointAll
        duplicateGeo(baseGeoGrp = baseGeoGrp , listsJnt = listsJnt, baseJnt = baseJnt)
    # createControlSkinPos(jntLists = listsJnt, baseJnt = baseJnt)

def duplicateGeo(baseGeoGrp = '',listsJnt = '', baseJnt = '',*args):
    # lists = ['EyeInSkinPos_L_Jnt', 
    #       'EyeInSkinPos_R_Jnt',
    #       'EyeOutSkinPos_L_Jnt', 
    #       'EyeOutSkinPos_R_Jnt',
    #       'EyeBrow_L',
    #       'EyeBrow_R',
    #       'NoseUp' ,
    #       'NoseDn', 
    #       'Cheek_L' ,
    #       'Cheek_R' ,
    #       'Ear_L', 
    #       'Ear_R', 
    #       'Chin', 
    #       'MouthOut_', 
    #       'MouthLip_', 
    #       'NoseSide_L',
    #       'NoseSide_R']
    # skinPosGeo = mc.group(n = 'SkinPosGeo_Grp', em = True)

    if not mc.objExists('SkinPosGeo_Grp'):
        SkinPosGrp = mc.group(n = 'SkinPosGeo_Grp', em = True)
    else:
        SkinPosGrp = 'SkinPosGeo_Grp'
    if not listsJnt:
        listsJntAll = mc.listRelatives(listsJnt, c = True)
    else:
        listsJntAll = listsJnt
    jnt = mc.listRelatives(listsJntAll, c = True)
    for each in jnt:
        if not '_R' in each:
            if not 'Base' in each:
                name, side, lastName = utaCore.splitName(sels = [each])
                newGrp = utaCore.duplicate(baseGeoGrp, name, side, elem = name)
                mc.parent(newGrp[-1], SkinPosGrp)

                print name, '....name'
                print side, '.....side'
                print lastName, '...lastName'
                ## Check Jnt
                checkJnt = []
                jntGrp = [each, '{}_R_Jnt'.format(name), baseJnt]
                for jnt in jntGrp:
                    if mc.objExists(jnt):
                        checkJnt.append(jnt)
                ## BindSkin Jnt, Geo
                geoLists = utaCore.listGeo(sels = newGrp[-1], nameing = '_Geo', namePass = '_Grp')
                for each in geoLists:
                    Geo = each.split('|')[-1]
                    utaCore.bindSkinCluster(jntObj = checkJnt, obj = [Geo])

def createControlSkinPos(jntLists = 'SkinPosJnt_Grp', baseJnt = 'BaseSkinPos_Jnt', curveCtrl = 'circle',*args):
    if not mc.objExists('SkinPosCtrl_Grp'):
        SkinPosGrp = mc.group(n = 'SkinPosCtrl_Grp', em = True)
    else:
        SkinPosGrp = 'SkinPosCtrl_Grp'

    jntLists = mc.listRelatives('SkinPosJnt_Grp', c = True)
    grpCtrlNameGrp = []
    grpCtrlOfsNameGrp = []
    grpCtrlParsNameGrp = []
    ctrlNameGrp = []
    GmblCtrlNameGrp = []
    for each in jntLists:
        if not '_R' in each:
            if not 'Base' in each:
                if not 'JawAddB' in each:
                    if not 'JawAddA' in each:
                        mc.select(each)
                        grpCtrlName, grpCtrlOfsName, grpCtrlParsName, ctrlName, GmblCtrlName = utaCore.createControlCurve(curveCtrl = curveCtrl, parentCon = True, connections = False, geoVis = False)
                        grpCtrlNameGrp.append(grpCtrlName)
                        grpCtrlOfsNameGrp.append(grpCtrlOfsName)
                        grpCtrlParsNameGrp.append(grpCtrlParsName)
                        ctrlNameGrp.append(ctrlName)
                        GmblCtrlNameGrp.append(GmblCtrlName)
                        # GeoLists = mc.listRelatives(grpCtrlName, c = True)
                        # utaCore.bindSkinCluster(jntObj = [each], obj = GeoLists)





    mc.parent(grpCtrlNameGrp, SkinPosGrp)

def createJointSkinPos(name = '', side = '', elem = '', mirrorBehavior = True, *args):
    sels = mc.ls(sl = True)
    if mirrorBehavior:
        if side:
            side = '_'+side+'_'
        else:
            side = '_L_'    
    else:
        if side:
            side = '_'+side+'_'
        else:
            side = '_'

    select = lrr.locatorOnMidPos()
    mc.select(select)
    NewJnt = mc.rename(utaCore.jointFromLocator(), '{}{}{}Jnt'.format(name, elem, side))
    if mirrorBehavior:
        if '_L' in side:
            NewSide = mc.mirrorJoint(NewJnt, mirrorYZ = True, mirrorBehavior = mirrorBehavior, searchReplace=("_L_", "_R_"))[0]
        else:
            NewSide = mc.mirrorJoint(NewJnt, mirrorYZ = True, mirrorBehavior = mirrorBehavior, searchReplace=("_R_", "_L_"))[0]

        ##>> connect Attr for mirror
        jntMdv = mc.createNode('multiplyDivide', n = '{}{}{}Mdv'.format(name, elem, side))
        mc.connectAttr('{}.tx'.format(NewJnt), '{}.input1X'.format(jntMdv))
        mc.setAttr('{}.input2X'.format(jntMdv), -1)
        mc.connectAttr('{}.output.outputX'.format(jntMdv), '{}.tx'.format(NewSide))
        mc.connectAttr('{}.ty'.format(NewJnt), '{}.ty'.format(NewSide))
        mc.connectAttr('{}.tz'.format(NewJnt), '{}.tz'.format(NewSide))
        mc.select(NewJnt, NewSide)
        utaCore.connectTRS(trs = False, translate = False, rotate = True, scale = True)
        mc.select(NewJnt, NewSide)
        utaCore.connectTRS(trs = False, translate = False, rotate = False, scale = True)
    else:
        NewSide = ''    
    jointAll = [NewJnt, NewSide]
    return jointAll

def connectJointMirror(jointA = '', jointB = '', bodyCon = False, axisNode = [],*args):
    if not jointA:
        jointA = mc.ls(sl = True)[0]
        jointB = mc.ls(sl = True)[-1]
    axiss = ('x', 'y', 'z')
    ##>> translate

    for axis in axiss:
        name, side, lastName = utaCore.splitName(sels = [jointA])
        if axis in axisNode:
            axis = axis.upper()
            mdvNode = mc.createNode('multiplyDivide', n = '{}{}{}Mdv'.format(name, axis, side))
            mc.setAttr('{}.input2X'.format(mdvNode),-1)
            mc.connectAttr('{}.translate{}'.format(jointA, axis), '{}.input1X'.format(mdvNode))
            try:
                mc.connectAttr('{}.outputX'.format(mdvNode), '{}.translate{}'.format(jointB, axis))
            except:pass
        else:
            axis = axis.upper()
            mc.connectAttr('{}.translate{}'.format(jointA, axis), '{}.translate{}'.format(jointB, axis))
    ##>> rotate
    mc.select(jointA)
    mc.select(jointB, add = True)
    utaCore.connectTRS(trs = False, translate = False, rotate = True, scale = False)

    if bodyCon:
        ##>> scale for body connect
        mc.connectAttr('{}.sx'.format(jointA), '{}.sx'.format(jointB))
        mc.connectAttr('{}.sy'.format(jointA), '{}.sy'.format(jointB))
        mc.connectAttr('{}.sz'.format(jointA), '{}.sz'.format(jointB))
    else:
        ##>> scale for non body connect
        mc.select(jointA)
        mc.select(jointB, add = True)
        utaCore.connectTRS(trs = False, translate = False, rotate = False, scale = True)


def connectScale(stretchy = True,*args):
    sels = mc.ls(sl = True)
    if stretchy:
        attrNew = ['stretch', 'scales']
    else:
        attrNew = ['scales']
    for each in sels:
        elems = utaCore.addAttrCtrl (ctrl = each, elem = attrNew, min = -1000, max = 1000, at = 'float', dv = 0)
        mc.setAttr('{}.{}'.format(each, elems[-1]), 1)
        mc.connectAttr('{}.sx'.format(each,), '{}.sz'.format(each))
        mc.connectAttr('{}.{}'.format(each, elems[-1]), '{}.sx'.format(each))
        # mc.connectAttr('{}.{}'.format(each, elems[0]), '{}.ty'.format(each))
        utaCore.lockAttr(listObj = [each], attrs = ['tx', 'ty', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz'] ,lock = True, keyable = False)

def fixScales(*args):
    sel = mc.ls(sl = True)[0]
    utaCore.lockAttr(listObj = [sel], attrs = ['sx', 'sy', 'sz'],lock = False, keyable = True)
    obj = utaCore.checkConnect(object = sel, attr = 'sx')
    mc.disconnectAttr('{}.scales'.format(sel), '{}.sx'.format(sel))
    #mc.disconnectAttr('{}.sx'.format(sel), '{}.sz'.format(sel))
    utaCore.lockAttr(listObj = [sel], attrs = ['sy', 'sz'],lock = True, keyable = False)
    mm.eval('catch (`deleteAttr -attribute "scales" "{}"`)'.format(sel))
    name, side, lastName = utaCore.splitName(sels = [sel])
    new = '{}{}Jnt'.format(name, side)
    mm.eval('CBdeleteConnection "{}.sy"'.format(new))

def fixNodeConStraint(*args):
    sels = mc.ls(sl = True)
    for each in sels:
        nameSp = each.split(':')[0]
        obj = each.split(':')[-1]
        name , side, lastName = utaCore.splitName(sels = [obj])
        jnt = '{}{}Jnt'.format(name, side)
        mc.delete('{}:{}_parentConstraint1'.format(nameSp, jnt), '{}:{}_scaleConstraint1'.format(nameSp, jnt))
        tyNode = mc.createNode('plusMinusAverage', n = '{}{}Pma'.format(name, side))
        tyAttr = mc.getAttr('{}:{}.ty'.format(nameSp, jnt))
        mc.connectAttr('{}.stretch'.format(each), '{}.input2D[1].input2Dx'.format(tyNode))
        mc.setAttr('{}.input2D[0].input2Dx'.format(tyNode), tyAttr)
        mc.connectAttr('{}.output2Dx'.format(tyNode), '{}:{}.ty'.format(nameSp, jnt))

        mc.connectAttr('{}.scaleX'.format(each), '{}:{}.scaleX'.format(nameSp, jnt))
        mc.connectAttr('{}.scaleZ'.format(each), '{}:{}.scaleZ'.format(nameSp, jnt))

def copyNewHeadBodyRig(copyOrRead = True,*args):
    ## select Body_Geo First and Run

    newGeo = 'Body_Geo'
    sels = mc.ls(sl = True)
    for each in sels:
        sell = mc.rename(each, '{}xxxxxx'.format(each))
        name, side, lastName = utaCore.splitName(sels = [sell])
        dup = mc.duplicate(newGeo)[0]
        newNameDup = mc.rename(dup, '{}{}Geo'.format(name, side))
        pos = mc.listRelatives(sell, p = True)[0]
        mc.parent(newNameDup,pos)
        mc.select(sell)
        mc.select(newNameDup, add = True)
        if copyOrRead:
            weightTools.copySelectedWeight()
        else:
            weightTools.readSelectedWeight()
        DeleteGrp.deleteGrp(obj = sell)
        mm.eval('reorder -relative -4 {}'.format(newNameDup))

def fixParenConstraintToConnection(bodyCon = True, axisNode = [], *args):
    # newGeo = 'Body_Geo'
    sels = mc.ls(sl = True)
    for each in sels:
        nameSp = each.split(':')[0]
        nameObj = each.split(':')[-1]
        name, side, lastName = utaCore.splitName(sels = [nameObj])
        newGrp = '{}Geo{}Grp'.format(name, name, side)
        ## Write SkinWeight
        if mc.objExists(newGrp):
            geoLists = utaCore.listGeo(sels = newGrp, nameing = '_Geo', namePass = '_Grp')

        ## check joint
        jntL = '{}:{}{}Jnt'.format(nameSp, name, side)
        if '_L' in side:
            jntR = '{}:{}_R_Jnt'.format(nameSp, name)
        else:
            jntR = ''
        ## clear Parent and scale Constraint
        try:
            mc.delete(  '{}:{}{}Jnt_parentConstraint1'.format(nameSp, name, side),
                        '{}:{}{}Jnt_scaleConstraint1'.format(nameSp, name, side))
        except:pass
        if mc.objExists(jntR):
            txMdvNode = '{}:{}{}Mdv'.format(nameSp, name, side)
            if mc.objExists(txMdvNode):
                try:
                    mc.disconnectAttr('{}.output.outputX'.format(txMdvNode), '{}.translate.translateX'.format(jntR))
                    mc.delete(txMdvNode)
                except:pass
            try:
                mc.disconnectAttr('{}.ty'.format(jntL), '{}.ty'.format(jntR))
                mc.disconnectAttr('{}.tz'.format(jntL), '{}.tz'.format(jntR))
                mc.disconnectAttr('{}.r'.format(jntL), '{}.r'.format(jntR))
                mc.disconnectAttr('{}.s'.format(jntL), '{}.s'.format(jntR))
            except:pass

        ## ZroGrp
        LR = [jntL, jntR]
        for eachLR in LR:
            if eachLR:
                mc.select(eachLR)
                grp = rigTools.UtaDoZeroGroup()
        ## Connect Control to Jnt
        mc.select(each)
        mc.select(jntL, add = True)
        utaCore.connectTRS(trs = True, translate = False, rotate = False, scale = False)
        ## Connect JntL to Jnt R
        if mc.objExists(jntR):
            connectJointMirror(jointA = jntL, jointB = jntR, bodyCon = bodyCon, axisNode = axisNode)

def wrapSkinPosToBodyRig(*args):
    # bodyStrtWorldPosiZro_Grp  parent Still_Grp
    # Head_Jnt parentScaleConstraint bodyRig:HeadMoveCtrlZro_Grp
    # Neck_Jnt parentScaleConstraint bodyRig:NeckCtrlZro_Grp
    # UpArm_L_Jnt parentScaleConstraint bodyRig:ClavicleCtrlZro_L_Grp, bodyRig:UpArmCtrlZro_L_Grp, bodyRig:ElbowCtrlZro_L_Grp, bodyRig:ForeArmCtrlZro_L_Grp
    # Spine5_Jnt parentScaleConstraint bodyRig:ChestCtrlZro_Grp, bodyRig:ChestFntCtrlZro_L_Grp
    # Spine3_Jnt parentScaleConstraint bodyRig:SpineMidCtrlZro_Grp
    # Pelvis_Jnt parentScaleConstraint bodyRig:SpineRootCtrlZro_Grp
    # Root_Jnt parentScaleConstraint bodyRig:RootCtrlZro_Grp
    # UpLeg_L_Jnt parentScaleConstraint bodyRig:UpLegTipCtrlZro_L_Grp, bodyRig:UpLegCtrlZro_L_Grp
    # LowLeg_L_Jnt parentScaleConstraint bodyRig:KneeCtrlZro_L_Grp, bodyRig:LowLegCtrlZro_L_Grp

    mc.parent('bodyStrtWorldPosiZro_Grp', 'Still_Grp')
    utacore.parentScaleConstraintLoop(obj = 'Head_Jnt', lists = ['bodyRig:HeadMoveCtrlZro_Grp'],par = True, sca = True, ori = False, poi = False)
    utacore.parentScaleConstraintLoop(obj = 'Neck_Jnt', lists = ['bodyRig:NeckCtrlZro_Grp'],par = True, sca = True, ori = False, poi = False)
    utacore.parentScaleConstraintLoop(obj = 'UpArm_L_Jnt', lists = ['bodyRig:ClavicleCtrlZro_L_Grp', 'bodyRig:UpArmCtrlZro_L_Grp', 'bodyRig:ElbowCtrlZro_L_Grp', 'bodyRig:ForeArmCtrlZro_L_Grp'],par = True, sca = True, ori = False, poi = False)
    utacore.parentScaleConstraintLoop(obj = 'Spine5_Jnt', lists = ['bodyRig:ChestCtrlZro_Grp', 'bodyRig:ChestFntCtrlZro_L_Grp'],par = True, sca = True, ori = False, poi = False)
    utacore.parentScaleConstraintLoop(obj = 'Spine3_Jnt', lists = ['bodyRig:SpineMidCtrlZro_Grp'],par = True, sca = True, ori = False, poi = False)
    utacore.parentScaleConstraintLoop(obj = 'Pelvis_Jnt', lists = ['bodyRig:SpineRootCtrlZro_Grp'],par = True, sca = True, ori = False, poi = False)
    utacore.parentScaleConstraintLoop(obj = 'Root_Jnt', lists = ['bodyRig:RootCtrlZro_Grp'],par = True, sca = True, ori = False, poi = False)
    utacore.parentScaleConstraintLoop(obj = 'UpLeg_L_Jnt', lists = ['bodyRig:UpLegTipCtrlZro_L_Grp', 'bodyRig:UpLegCtrlZro_L_Grp'],par = True, sca = True, ori = False, poi = False)
    utacore.parentScaleConstraintLoop(obj = 'LowLeg_L_Jnt', lists = ['bodyRig:KneeCtrlZro_L_Grp', 'bodyRig:LowLegCtrlZro_L_Grp'],par = True, sca = True, ori = False, poi = False)
