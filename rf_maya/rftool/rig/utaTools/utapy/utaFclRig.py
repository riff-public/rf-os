import os
from functools import partial

import maya.cmds as mc
import maya.mel as mm
import pymel.core as pm
import re

from lpRig import core as lrc
reload(lrc)
from lpRig import rigTools as lrr
reload(lrr)
from lpRig import detailControl
reload(detailControl)
# from lpRig import blendShapeAdder
# reload(blendShapeAdder)
from utaTools.utapy import utaBlendShapeAdder
reload(utaBlendShapeAdder)
from lpRig import core as lrc
reload(lrc)
from utaTools.utapy import utaCore
reload(utaCore)
from rf_utils.context import context_info
from ncmel import core
reload(core)
from utaTools.utapy import utaTagNameConnectWriteRead as tagNameConWR 
reload(tagNameConWR)
from ncmel import bshTools      
reload(bshTools)     
from rf_utils import file_utils

from rf_app.sg_manager import dcc_hook
reload(dcc_hook)
from rf_utils.context import context_info
reload(context_info)

from utaTools.utapy import utaAttrWriteRead
reload(utaAttrWriteRead)

#path project config
rootScript = os.environ['RFSCRIPT']
pathProjectJson = r"{}\core\rf_maya\rftool\rig\export_config\project_config.json".format(rootScript)
projectData = file_utils.json_loader(pathProjectJson)


CTRL_GRP = 'FclRigCtrl_Grp'
JNT_GRP = 'FclRigJnt_Grp'
SKIN_GRP = 'FclRigSkin_Grp'
STILL_GRP = 'FclRigStill_Grp'
RVT_GRP = 'FclRigRvt_Grp'

HD_LOW_GRP = 'HdLowFclRigSpc_Grp'
HD_MID_GRP = 'HdMidFclRigSpc_Grp'
HD_HI_GRP = 'HdHiFclRigSpc_Grp'

def refElem(elem=''):

	scnLoc = os.path.normpath(mc.file(q=True, l=True)[0])

	fldPath, flNmExt = os.path.split(scnLoc)
	assetPath, taskFld = os.path.split(fldPath)
	catPath, assetNm = os.path.split(assetPath)

	elemPath = os.path.join(
								fldPath,
								'%s_%s.ma' % (assetNm, elem)
							)

	mc.file(elemPath, r=True, ns=elem)

def fclRigAssembler():
	# FclRigAssembler call back
	ui = FclRigAssembler()
	ui.show()

	return ui

class FclRigAssembler(object):
	'''
	UI class for assemFclRig function.
	'''
	def __init__(self):

		self.ui = 'pkFclRigAssembler'
		self.win = '%sWin' % self.ui

	def show(self):

		if mc.window(self.win, exists=True):
			mc.deleteUI(self.win)

		mc.window(self.win, t='pkFclRigAssembler', rtf=True)
		self.mainCl = mc.columnLayout(adj=True)
		self.mainRl = mc.rowLayout(adj=True, nc=4)

		# Part Src Objs
		self.partSrcLister = utaBlendShapeAdder.BlendShapeLister('Body FclPart')
		mc.setParent(self.mainRl)

		# Part Objs
		self.partObjLister = utaBlendShapeAdder.BlendShapeLister('Body RigPart')
		mc.setParent(self.mainRl)

		# Bsh Src Objs
		self.bshSrcLister = utaBlendShapeAdder.BlendShapeLister('All FclPath')
		mc.setParent(self.mainRl)

		# Bsh Objs
		self.bshObjLister = utaBlendShapeAdder.BlendShapeLister('All RigPath')
		mc.setParent(self.mainRl)

		mc.setParent(self.mainCl)
		self.assemButton = mc.button(l='Assemble', c=partial(self.assem), h=35)

		mc.showWindow(self.win)
		mc.window(self.win, e=True, w=250)
		mc.window(self.win, e=True, h=250)

	def getObjs(self, *args):
		mc.textScrollList(args[0], e=True, ra=True)
		for sel in mc.ls(sl=True):
			if not mc.listRelatives(sel, s=True): continue
			mc.textScrollList(args[0], e=True, a=sel)

	def assem(self, *args):

		partSrcObjs = mc.textScrollList(self.partSrcLister.bshTsl, q=True, ai=True)
		partObjs = mc.textScrollList(self.partObjLister.bshTsl, q=True, ai=True)
		bshSrcObjs = mc.textScrollList(self.bshSrcLister.bshTsl, q=True, ai=True)
		bshObjs = mc.textScrollList(self.bshObjLister.bshTsl, q=True, ai=True)

		assemFclRig(partObjs, partSrcObjs, bshObjs, bshSrcObjs)

def assemFclRig(
					wrappedGeos=[
									'Head_Geo'
								],
					wrapGeos=	[
									'ModHero_FclRig:HeadFclRig_Geo'
								],
					rigBshGeos=	[
									'Tongue_Geo',
									'UpperGum_Geo',
									'LowerGum_Geo',
									'UpperTeeth_Geo',
									'LowerTeeth_Geo',
									'Hair_Geo',
									'Eyebrow_Geo',
									'EyeGeo_L_Grp',
									'EyeGeo_R_Grp'
								],
					fclBshGeos=	[
									'ModHero_FclRig:TongueFclRig_Geo',
									'ModHero_FclRig:UpperGumFclRig_Geo',
									'ModHero_FclRig:LowerGumFclRig_Geo',
									'ModHero_FclRig:UpperTeethFclRig_Geo',
									'ModHero_FclRig:LowerTeethFclRig_Geo',
									'ModHero_FclRig:HairFclRig_Geo',
									'ModHero_FclRig:EyebrowFclRig_Geo',
									'ModHero_FclRig:EyeGeoFclRig_L_Grp',
									'ModHero_FclRig:EyeGeoFclRig_R_Grp'
								]
				):

	# BodyRig elms
	hdRigJnt = lrc.Dag(mc.ls('*:Head_Jnt')[0])
	fclRigGrp = lrc.Dag(mc.ls('*:FacialCtrl_Grp')[0])  
	stillRigGrp = lrc.Dag(mc.ls('*:Still_Grp')[0])

	# FclRig elms
	ctrlFclRigGrp = lrc.Dag(mc.ls('*:FclRigCtrl_Grp')[0])
	jntFclRigGrp = lrc.Dag(mc.ls('*:FclRigJnt_Grp')[0])
	skinFclRigGrp = lrc.Dag(mc.ls('*:FclRigSkin_Grp')[0])
	stillFclRigGrp = lrc.Dag(mc.ls('*:FclRigStill_Grp')[0])

	# Par groups
	fclRigStillPar = lrc.Null()
	fclRigStillPar.name = 'FclRigStillPar_Grp'
	fclRigStillPar.parent(stillRigGrp)

	fclRigCtrlPar = lrc.Null()
	fclRigCtrlPar.name = 'FclRigCtrlPar_Grp'

	mc.parentConstraint(hdRigJnt, fclRigCtrlPar, mo=True)
	mc.scaleConstraint(hdRigJnt, fclRigCtrlPar, mo=True)

	# Parenting fclRig to main
	ctrlFclRigGrp.parent(fclRigCtrlPar)
	jntFclRigGrp.parent(fclRigStillPar)
	skinFclRigGrp.parent(fclRigStillPar)
	stillFclRigGrp.parent(fclRigStillPar)

	fclRigCtrlPar.parent(fclRigGrp)

	asset = context_info.ContextPathInfo()
	projectName = asset.project 
	charName = asset.name 

	if projectName in projectData['project']:
		connect7FacialBodyToMain(project = projectName)

		## Connect FangeMthBsh
		if charName == 'canis': 
			connectFang()

	else:
		fclRigCtrlPar.parent(fclRigGrp)

	if projectName == 'Hanuman':
		for each in ['bodyRig_md:JawUpr1_Ctrl' , 'bodyRig_md:JawUpr1Gmbl_Ctrl']:
			pm.PyNode(each).tx.lock()
			pm.PyNode(each).ty.lock()
			pm.PyNode(each).tz.lock()
			pm.PyNode(each).rx.lock()
			pm.PyNode(each).ry.lock()
			pm.PyNode(each).rz.lock()
			pm.PyNode(each).sx.lock()
			pm.PyNode(each).sy.lock()
			pm.PyNode(each).sz.lock()
			pm.PyNode(each).v.unlock()
			mc.setAttr(each+'.v',0)
			pm.PyNode(each).v.lock()

	# Wrap geos
	if wrappedGeos:
		for ix, wrappedGeo in enumerate(wrappedGeos):

			wrapGeo = wrapGeos[ix]

			# Get wrapped geo
			duppedGeo = lrc.Dag(mc.duplicate(wrappedGeo, rr=True)[0])
			fclRigWrappedGeo = mc.rename(duppedGeo, 'BodyFclRigWrapped_Geo')

			# Create wrap
			if wrapGeo:
				check = utaCore.checkSameObject(objA = wrapGeo, objB = fclRigWrappedGeo)
				if check:
					mc.select(wrapGeo, r = True)
					mc.select(fclRigWrappedGeo, add = True)
					lrr.doAddBlendShape()					
				else:
					mc.select(fclRigWrappedGeo, r=True)
					mc.select(wrapGeo, add=True)
					wrap = mm.eval('doWrapArgList "7"{"1","0","0.1","2","1","0","1"};')[0]
					
					# Parent base geo
					base = mc.listConnections('%s.basePoints[0]' % wrap, s=True, d=False)[0]
					mc.parent(base, stillRigGrp)

			# Adding blend shape
			mc.select(fclRigWrappedGeo, r=True)
			mc.select(wrappedGeo, add=True)
			lrr.doAddBlendShape()

			mc.setAttr('%s.v' % fclRigWrappedGeo, 0)
			mc.parent(fclRigWrappedGeo, stillRigGrp)

	# Bsh geos
	if rigBshGeos:
		for ix, rigBshGeo in enumerate(rigBshGeos):

			fclBshGeo = fclBshGeos[ix]

			mc.select(fclBshGeo, r=True)
			mc.select(rigBshGeo, add=True)
			lrr.doAddBlendShape()

	# Wrinkled scaleConstraint ctrlGrp
	wrinkledRig = mc.ls('*:WrinkledRigCtrl_Grp')
	if wrinkledRig:
		get_midCtrl = []
		wrinkledGrp = mc.listRelatives(wrinkledRig, c=True)

		for grp in wrinkledGrp:
			ctrlGrp = mc.listRelatives(grp, c=True)
			for sca in ctrlGrp:
				mc.scaleConstraint(hdRigJnt.getName(), sca, mo=True)
				if "Mid" in sca:
					all_children = mc.listRelatives(sca, c=True, ad=True, typ="nurbsCurve")
					if not "Gmbl" in all_children:
						midCtrl = mc.listRelatives(all_children, ap=True)[0]
						get_midCtrl.append(midCtrl)

		wrinkledRigGeo = mc.listRelatives('*:WrinkledRigGeo_Grp', c=True)
		if wrinkledRigGeo:
			all_wrinkledGeoGrp = []; [all_wrinkledGeoGrp.append(grp) for grp in wrinkledRigGeo if "WrinkledRig" in grp]
			wrinkledGeoGrp = mc.listRelatives(all_wrinkledGeoGrp, c=True)
			for i in range(len(wrinkledRigGeo)):
				if "WrinkledRig" in wrinkledRigGeo[i]:
					wrinkledGeo = utaCore.listGeo(sels = wrinkledRigGeo[i], nameing = '_Geo', namePass = '_Grp')

					for each in wrinkledGeo:
						check = each.split('|')[-1]
						if not 'Base' in check:
							geo = check.replace("WrinkledRig_", "_").replace("fclRig_md:", "bodyRig_md:")
							mc.select(check, geo)
							lrr.doAddBlendShape()
						mc.connectAttr(get_midCtrl[i] + ".wrinkledVis", geo + ".v")


	utaAttrWriteRead.attrRead()



	# check_wrinkledRig = mc.listRelatives("*:FclRigCtrl_Grp")
	# if check_wrinkledRig:
	# 	all_wrinkledCtrlGrp = ""
	# 	for check in check_wrinkledRig:
	# 		if "WrinkledRig" in check:
	# 			all_wrinkledCtrlGrp = check

	# 	if all_wrinkledCtrlGrp == "":
	# 		return
	# 	get_midCtrl = []
	# 	wrinkledGrp = mc.listRelatives(all_wrinkledCtrlGrp, c=True)
	# 	for grp in wrinkledGrp:
	# 		ctrlGrp = mc.listRelatives(grp, c=True)
	# 		for sca in ctrlGrp:
	# 			mc.scaleConstraint(hdRigJnt.getName(), sca, mo=True)
	# 			if "Mid" in sca:
	# 				all_children = mc.listRelatives(sca, c=True, ad=True, typ="nurbsCurve")
	# 				if not "Gmbl" in all_children:
	# 					midCtrl = mc.listRelatives(all_children, ap=True)[0]
	# 					get_midCtrl.append(midCtrl)

	# 	check_wrinkledStill = mc.listRelatives("*:FclRigStill_Grp")
	# 	if check_wrinkledStill:
	# 		for check in check_wrinkledStill:
	# 			if "WrinkledRigGeo" in check:
	# 				all_wrinkledGeoGrp = check

	# 		wrinkledGeoGrp = mc.listRelatives(all_wrinkledGeoGrp, c=True)
	# 		for grp, ctrl in zip(wrinkledGeoGrp, get_midCtrl):
	# 			if "WrinkledRig" in grp:
	# 				wrinkledGeo = mc.listRelatives(grp, c=True)[0]
	# 				geo = wrinkledGeo.replace("WrinkledRig_", "_").replace("fclRig_md:", "bodyRig_md:")
	# 				mc.select(wrinkledGeo, geo)
	#     			lrr.doAddBlendShape()
	#     			mc.connectAttr(ctrl + ".wrinkledVis", geo + ".v")

	# connectEyeRig()
def connect7FacialBodyToMain(project = '',*args):
	# BodyRig elms
	hdRigJnt = lrc.Dag(mc.ls('*:Head_Jnt')[0])
	JawUprRigJnt = lrc.Dag(mc.ls('*:JawUpr1_Jnt')[0])
	fclRigGrp = lrc.Dag(mc.ls('*:FacialCtrl_Grp')[0])  
	stillRigGrp = lrc.Dag(mc.ls('*:Still_Grp')[0])

	# FclRig elms
	ctrlFclRigGrp = lrc.Dag(mc.ls('*:FclRigCtrl_Grp')[0])
	jntFclRigGrp = lrc.Dag(mc.ls('*:FclRigJnt_Grp')[0])
	skinFclRigGrp = lrc.Dag(mc.ls('*:FclRigSkin_Grp')[0])
	stillFclRigGrp = lrc.Dag(mc.ls('*:FclRigStill_Grp')[0])

	# mainRigGrp = lrc.Dag(mc.ls('*:FacialCtrl_Grp')[0])  
	jawLwr2Jnt = lrc.Dag(mc.ls('*:JawLwr2_Jnt')[0])  
	JawPostZrGrp = lrc.Dag(mc.ls('*:TtRigCtrlJawPostZr_Grp')[0])
	CtrlUpTthRigGrp = lrc.Dag(mc.ls('*:CtrlUpTthRig_Grp')[0])
	
	dtlRigCtrlHi = lrc.Dag(mc.ls('*:DtlRigCtrlHi_Grp')[0])
	dtlRigCtrlMid = lrc.Dag(mc.ls('*:DtlRigCtrlMid_Grp')[0])
	dtlRigCtrlLow = lrc.Dag(mc.ls('*:DtlRigCtrlLow_Grp')[0])  
	dtlRigCtrlMidLow = lrc.Dag(mc.ls('*:DtlRigCtrlMidLow_Grp')[0]) 

	# mc.parent(fclRigCtrlPar, mainRigGrp)
	# fclRigCtrlPar.parent(mainRigGrp)

## Dtl Group
	utaCore.parentScaleConstraintLoop(  obj = jawLwr2Jnt, 
										lists =  [ JawPostZrGrp],
										par = True,sca = True, ori = False, poi = False)
	utaCore.parentScaleConstraintLoop(  obj = JawUprRigJnt, 
										lists =  [ CtrlUpTthRigGrp],
										par = True,sca = True, ori = False, poi = False)

## Bsh Group
	BodyGeo = mc.ls("*:Body_Geo")
	if not BodyGeo:
		BodyGeo = mc.ls("*:Head_Geo")
	BodyFclRigPos = mc.ls("*:BodyFclRigPost_Geo")

	# Bsh Get wrapped geo
	BodyDtlWrapBsGeo = lrc.Dag(mc.duplicate(BodyGeo, rr=True, n = "BodyDtlWrapBs_Geo",)[0]) 
	mc.delete(BodyDtlWrapBsGeo, ch=True)
	BodyDtlWrapBsGeo.attr('v').v = 0

	# Create wrap
	mc.select(BodyFclRigPos, r=True)
	mc.select(BodyDtlWrapBsGeo, add=True)

	wrap = mm.eval('doWrapArgList "7"{"1","0","0.1","2","1","0","1"};')[0]

	# Parent base geo
	base = mc.listConnections('%s.basePoints[0]' % wrap, s=True, d=False)[0]
	mc.parent(BodyDtlWrapBsGeo,'%sBase' % BodyDtlWrapBsGeo , stillRigGrp)

	if BodyFclRigPos:
		mc.select(BodyGeo[0], r=True)
		mc.select(BodyDtlWrapBsGeo, add=True)
		lrr.doAddBlendShape()

	# Connect Jaw to headUprSquashBsh, headLwrStretchBsh
	if mc.ls('*:MouthBsh_Ctrl'):
		connectJawSquashStretchBsh()    


## Dtrl Group
	# Orient Scale Constraint DtlCtrlRigHi
	dtlRigCtrlHiList = mc.listRelatives(dtlRigCtrlHi, c = True, s = False)
	for each in dtlRigCtrlHiList:
		utaCore.parentScaleConstraintLoop(  obj = hdRigJnt, 
											lists =  [each],
											par = False,sca = True, ori = True, poi = False)

	# Orient Scale Constraint DtlCtrlRigMid
	dtlRigCtrlMidList = mc.listRelatives(dtlRigCtrlMid, c = True, s = False)
	for each in dtlRigCtrlMidList:
		utaCore.parentScaleConstraintLoop(  obj = JawUprRigJnt, 
											lists =  [each],
											par = False,sca = True, ori = True, poi = False)
	# Orient Scale Constraint DtlCtrlRigLow
	dtlRigCtrlLowList = mc.listRelatives(dtlRigCtrlLow, c = True, s = False)
	for each in dtlRigCtrlLowList:
		utaCore.parentScaleConstraintLoop(  obj = jawLwr2Jnt, 
											lists =  [each],
											par = False,sca = True, ori = True, poi = False)

	# transformGrp Fix Filp MouthCnr
	jawPosZro = mc.createNode('transform', name = 'JawUprPosZro_Grp')
	jawPosGrp = mc.createNode('transform', name = 'JawUprPos_Grp')
	mc.parent(jawPosGrp,jawPosZro)
	mc.delete(mc.parentConstraint(JawUprRigJnt,jawPosZro,mo=False))
	mc.parent(jawPosZro,JawUprRigJnt)
	mc.setAttr(jawPosZro+'.rz',180)

	# Orient Scale Constraint DtlCtrlRigMidLow		
	dtlRigCtrlMidLowList = mc.listRelatives(dtlRigCtrlMidLow, c = True, s = False)
	if dtlRigCtrlMidLowList != None:
		for each in dtlRigCtrlMidLowList:
			if 'Cnr' in each: 
				utaCore.parentScaleConstraintLoop(  obj = [jawPosGrp,jawLwr2Jnt], 
													lists =  [each],
													par = False,sca = True, ori = True, poi = False)
			elif 'Lwr' in each:
				utaCore.parentScaleConstraintLoop(  obj = [jawLwr2Jnt], 
													lists =  [each],
													par = False,sca = True, ori = True, poi = False)
			else:
				utaCore.parentScaleConstraintLoop(  obj = [jawPosGrp], 
									lists =  [each],
									par = False,sca = True, ori = True, poi = False)


			cons = mc.listRelatives(each,c=True,type='orientConstraint')[0]
    		mc.setAttr(cons+'.interpType' , 2)

	if mc.objExists('fclRig_md:MouthUprBsh_Ctrl'):
		# connect bsh ctrl with ctrl
		udBsh_follow_list = ['fclRig_md:MouthUprBsh_Ctrl','fclRig_md:MouthLwrBsh_Ctrl' , 'fclRig_md:NoseBsh_Ctrl']
		lrBsh_follow_list = ['fclRig_md:MouthCnrBsh_L_Ctrl','fclRig_md:MouthCnrBsh_R_Ctrl']

		for ctrl in udBsh_follow_list:
			rivet = ctrl.replace('Bsh_','BshDtl_C_').replace('_Ctrl','_ctrl')
			zrGrp = ctrl.replace('Bsh_','BshCtrlZro_').replace('_Ctrl','_Grp')
			invGrp = ctrl.replace('Bsh_','BshCtrlOfst_').replace('_Ctrl','_Grp')

			if mc.objExists(invGrp):
				mul = ctrl.replace('Bsh_','BshCtrlOfst_').replace('_Ctrl','_Mdv')
				mdv = mc.createNode('multiplyDivide',n=  mul)
				for ax in 'XYZ':
					mc.setAttr(mdv+'.input2%s'%ax,-1)

				mc.connectAttr(ctrl+'.ty',mdv+'.input1Y')
				mc.connectAttr(mdv+'.outputY',invGrp+'.ty')
				mc.parentConstraint(rivet,zrGrp,mo=1)

				mc.setAttr(rivet +'.v',0)
				for tf in 'trs':
					for ax in 'xyz':
						mc.setAttr(rivet+'.%s%s'%(tf,ax))
			

		for ctrl in lrBsh_follow_list:
			
			rivet = ctrl.replace('Bsh_','BshDtl_').replace('_Ctrl','_ctrl')
			zrGrp = ctrl.replace('Bsh_','BshCtrlZro_').replace('_Ctrl','_Grp')
			invGrp = ctrl.replace('Bsh_','BshCtrlOfst_').replace('_Ctrl','_Grp')
			
			if mc.objExists(invGrp):
				mul = ctrl.replace('Bsh_','BshCtrlOfst_').replace('_Ctrl','_Mdv')
				mdv = mc.createNode('multiplyDivide',n=mul)
				
				for ax in 'XYZ':
					mc.setAttr(mdv+'.input2%s'%ax,-1)
				
				mc.connectAttr(ctrl+'.tx',mdv+'.input1X')
				mc.connectAttr(ctrl+'.ty',mdv+'.input1Y')
				mc.connectAttr(mdv+'.outputX',invGrp+'.tx')
				mc.connectAttr(mdv+'.outputY',invGrp+'.ty')
				mc.parentConstraint(rivet,zrGrp,mo=1)
				
				mc.setAttr(rivet +'.v',0)
				for tf in 'trs':
					for ax in 'xyz':
						mc.setAttr(rivet+'.%s%s'%(tf,ax))

		mouthBsh = 'fclRig_md:MouthBshCtrlZro_Grp'
		if mc.objExists(mouthBsh):
			cons = mc.parentConstraint([jawPosGrp,jawLwr2Jnt],mouthBsh,mo=1)
			mc.setAttr(cons[0]+'.interpType',2)

# Connect MthRig at main

	connectMth7Fcl()

## Connect EyeLidFollow
	connectEyeLidFollow()
	bshTools.connectLoLidUpDnLR()       

## Connect LipSeal
	if not project == 'Hanuman':
		connectLipSeal()


# Connect Layer Visibility 
	try:
		tagNameConWR.tagNameConnectionRead()
	except: pass

def connectLipSeal(*args):
	## Con LipsSeal L (at main)     
	utaCore.con(    objA = 'fclRig_md:MouthCnrBsh_L_Ctrl',      
	objB = 'bodyRig_md:JawLwr1_Ctrl',       
	attrA = ['LipsSeal'],       
	attrB = ['LipsSealL'])      
	## Con LipsSeal R (at main)     
	utaCore.con(    objA = 'fclRig_md:MouthCnrBsh_R_Ctrl',      
	objB = 'bodyRig_md:JawLwr1_Ctrl',       
	attrA = ['LipsSeal'],       
	attrB = ['LipsSealR'])  

def connectJawSquashStretchBsh(*args):
	## Bsh Jaw connect to Bsh FaceLwrBsh_Ctrl

	if mc.objExists('*:FacialAll_LocShape'):
		facialCtrl = lrc.Dag(mc.ls('*:FacialAll_LocShape')[0])
	else:
		facialCtrl = lrc.Dag(mc.ls('*:FacialAll_Bsn')[0])


	jawLwr1Jnt = lrc.Dag(mc.ls('*:JawLwr1_Jnt')[0]) 
	JawUpr1Jnt = lrc.Dag(mc.ls('*:JawUpr1_Jnt')[0])
	
	facialNode = lrc.Dag(mc.ls('*:FaceLwrTy_Mdv')[0])  
	JawUprRoot = lrc.Dag(mc.ls('*:JawUprRootSub_Ctrl')[0])  
	JawLwr1Ctrl = lrc.Dag(mc.ls('*:JawLwr1_Ctrl')[0])
 
	# Add Attribute Ctrl on Shape 
	utaCore.attrName(obj = '%sShape' % JawLwr1Ctrl, elem = 'HeadLwrSquashAmp')
	utaCore.attrName(obj = '%sShape' % JawLwr1Ctrl, elem = 'HeadUprSquashAmp')
	utaCore.attrName(obj = '%sShape' % JawLwr1Ctrl, elem = 'HeadLwrStretchAmp')
	# CleateNode 'HeadLwrSquash'    
	jawUpSqMdv = mc.createNode('multiplyDivide', n = 'jawUpSquashBsh_Mdv')
	jawUpSqClam = mc.createNode('clamp', n = 'jawUpSquashBsh_Clam')    
	jawUpSqPma = mc.createNode('plusMinusAverage', n = 'jawUpSquashBsh_Pma')
	jawLwrSqPma = mc.createNode('plusMinusAverage', n = 'jawLwrStretchBsh_Pma')
	# SetAttr
	mc.setAttr('%sShape.HeadLwrSquashAmp' % JawLwr1Ctrl, -0.1)
	mc.setAttr('%sShape.HeadUprSquashAmp' % JawLwr1Ctrl, 0.02)
	mc.setAttr('%sShape.HeadLwrStretchAmp' % JawLwr1Ctrl, 0.01)
	mc.setAttr('%s.maxR' % jawUpSqClam, 90) 
	mc.setAttr('%s.maxG' % jawUpSqClam, 90)
	mc.setAttr('%s.maxB' % jawUpSqClam, 90)      

	# Connect Attribute 'JawLwr1_Ctrl'
	mc.connectAttr('%sShape.HeadLwrSquashAmp' % JawLwr1Ctrl, '%s.input2X' % jawUpSqMdv)
	mc.connectAttr('%sShape.HeadUprSquashAmp' % JawLwr1Ctrl, '%s.input2Y' % jawUpSqMdv)
	mc.connectAttr('%sShape.HeadLwrStretchAmp' % JawLwr1Ctrl, '%s.input2Z' % jawUpSqMdv)
	mc.connectAttr('%s.rotateX' % jawLwr1Jnt, '%s.input1X' % jawUpSqMdv)
	mc.connectAttr('%s.rotateX' % jawLwr1Jnt, '%s.input1Y' % jawUpSqMdv)
	mc.connectAttr('%s.rotateX' % jawLwr1Jnt, '%s.input1Z' % jawUpSqMdv)
	mc.connectAttr('%s.outputX' % jawUpSqMdv, '%s.inputR' % jawUpSqClam)
	mc.connectAttr('%s.outputY' % jawUpSqMdv, '%s.inputG' % jawUpSqClam)
	mc.connectAttr('%s.outputZ' % jawUpSqMdv, '%s.inputB' % jawUpSqClam)
	mc.connectAttr('%s.outputR' % jawUpSqClam, '%s.input1D[0]' % jawUpSqPma)
	mc.connectAttr('%s.outputB' % jawUpSqClam, '%s.input1D[0]' % jawLwrSqPma)
	mc.connectAttr('%s.outputG' % jawUpSqClam, '%s.rotateX' % JawUprRoot)

	# DisConnectAttr
	mc.disconnectAttr('%s.outputY' % facialNode , '%s.HeadLwrSquash' % facialCtrl)
	mc.connectAttr('%s.output1D' % jawUpSqPma, '%s.HeadLwrSquash' % facialCtrl)
	mc.connectAttr('%s.outputY' % facialNode, '%s.input1D[1]' % jawUpSqPma)

	mc.disconnectAttr('%s.outputX' % facialNode , '%s.HeadLwrStretch' % facialCtrl)
	mc.connectAttr('%s.output1D' % jawLwrSqPma, '%s.HeadLwrStretch' % facialCtrl)
	mc.connectAttr('%s.outputX' % facialNode, '%s.input1D[1]' % jawLwrSqPma)

def connectEyeLidFollow(*args):
	## List Control
	headJnt = mc.ls('*:Head_Jnt')
	obj = mc.ls('*:EyeLFT_Jnt','*:EyeRGT_Jnt')
	objs = mc.ls('*:EyeLidsBsh_L_Jnt','*:EyeLidsBsh_R_Jnt')
	ctrl = mc.ls('*:EyeLFT_Ctrl', '*:EyeRGT_Ctrl')
	## CreateNode
	mulL = mc.createNode ('multiplyDivide', n = 'EyelidFollow_L_Mdv')
	mulR = mc.createNode ('multiplyDivide', n = 'EyelidFollow_R_Mdv')
	revL = mc.createNode ('reverse', n = 'EyelidFollow_L_Rev')
	revR = mc.createNode ('reverse', n = 'EyelidFollow_R_Rev')
	## SetAttr Node
	mc.setAttr('%s.input2X' % mulL, 1)
	mc.setAttr('%s.input2X' % mulR, 1)
	## Connect Attr 
	mc.connectAttr('%s.eyelidsFollow' % ctrl[0], '%s.input1X' % mulL )
	mc.connectAttr('%s.eyelidsFollow' % ctrl[1], '%s.input1X' % mulR )
	mc.connectAttr('%s.eyelidsFollow' % ctrl[0], '%s.input.inputX' % revL )
	mc.connectAttr('%s.eyelidsFollow' % ctrl[1], '%s.input.inputX' % revR )
	## Parent Constraint
	i = 0
	if objs:
		for each in obj: 
			utaCore.parentScaleConstraintLoop(obj = each, lists = [objs[i]],par = True, sca = False, ori = False, poi = False)
			utaCore.parentScaleConstraintLoop(obj = headJnt[0], lists = [objs[i]],par = True, sca = False, ori = False, poi = False)
			i += 1
		## Connect ParentConstraint Mode
		spL = objs[0].split(':')
		spR = objs[1].split(':')

		mc.connectAttr('%s.outputX' % mulL, '%s_parentConstraint1.EyeLFT_JntW0' % spL[-1] )
		mc.connectAttr('%s.outputX' % mulR, '%s_parentConstraint1.EyeRGT_JntW0' % spR[-1] )
		mc.connectAttr('%s.outputX' % revL, '%s_parentConstraint1.Head_JntW1' % spL[-1] )
		mc.connectAttr('%s.outputX' % revR, '%s_parentConstraint1.Head_JntW1' % spR[-1] )
		## SetAttr eyeLidFollow 0.5
		mc.setAttr ('%s.eyelidsFollow' % ctrl[0], 0.5)
		mc.setAttr ('%s.eyelidsFollow' % ctrl[1], 0.5)
	
def connectMth7Fcl(*args):
## Mth Group
	from rf_utils.context import context_info
	asset = context_info.ContextPathInfo() 
	projectName = asset.project

	if not projectName == 'Hanuman':
		# for each in objPass:
		#-- Add Attribute Lips Seal
		locMth = mc.ls('*:mthJawLwr1_Loc')
		addLipSealAttr  = mc.ls('*:JawLwr1_Ctrl')  
		JawLwr1Ctrl = mc.ls('*:JawLwr1_Jnt')  
		JawLwr2Ctrl = mc.ls('*:JawLwr2_Jnt')  
		mthJawLwr1Ctrl = mc.ls('*:mthJawLwr1_Jnt')
		mthJawLwr2Ctrl = mc.ls('*:mthJawLwr2_Jnt')       
		jawCtrl = core.Dag( addLipSealAttr[0] )
		jawCtrl.addAttr( 'LipsSealL' , 0 , 20 )
		jawCtrl.addAttr( 'LipsSealR' , 0 , 20 )

		mc.connectAttr('%s.LipsSealL' % jawCtrl, '%s.LipsSealL' % locMth[0])
		mc.connectAttr('%s.LipsSealR' % jawCtrl, '%s.LipsSealR' % locMth[0])

		# Connect Attr Translate Rotate Scale  Jaw1
		mc.select(JawLwr1Ctrl[0],r = True)
		mc.select(mthJawLwr1Ctrl[0], add = True)
		utaCore.connectTRS(trs = True, translate = False, rotate = False, scale = False)

		# Connect Attr Translate Rotate Scale  Jaw2
		mc.select(JawLwr2Ctrl[0],r = True)
		mc.select(mthJawLwr2Ctrl[0], add = True)
		utaCore.connectTRS(trs = True, translate = False, rotate = False, scale = False)

	else:
		if mc.objExists('*:MouthCnrBsh_L_Ctrl'):       
			locMth = mc.ls('*:mthJawLwr1_Loc')
			# addLipSealAttr  = mc.ls('*:JawLwr1_Ctrl')  
			mouthBshCnrL = mc.ls('*:MouthCnrBsh_L_Ctrl')
			mouthBshCnrR = mc.ls('*:MouthCnrBsh_R_Ctrl')

			mthBshCnrL = core.Dag( mouthBshCnrL[0] )
			mthBshCnrR = core.Dag( mouthBshCnrR[0] )
			loc = core.Dag(locMth[0])
			mthBshCnrL.attr('LipsSeal') >>  loc.attr('LipsSealL')
			mthBshCnrR.attr('LipsSeal') >>  loc.attr('LipsSealR')

		# Connect Attr Translate Rotate Scale  Jaw1
		JawLwr1Jnt = mc.ls('*:JawLwr1_Jnt')  
		JawLwr2Jnt = mc.ls('*:JawLwr2_Jnt')  
		mthJawLwr1Jnt = mc.ls('*:mthJawLwr1_Jnt')
		mthJawLwr2Jnt = mc.ls('*:mthJawLwr2_Jnt') 
		mc.select(JawLwr1Jnt[0],r = True)
		mc.select(mthJawLwr1Jnt[0], add = True)
		utaCore.connectTRS(trs = True, translate = False, rotate = False, scale = False)

		# Connect Attr Translate Rotate Scale  Jaw2
		mc.select(JawLwr2Jnt[0],r = True)
		mc.select(mthJawLwr2Jnt[0], add = True)
		utaCore.connectTRS(trs = True, translate = False, rotate = False, scale = False)

def connectBshRig():
	bshRigGeo = mc.ls('*:FacialBshGeo_Grp')
	fclBshGeo = 'FclRigGeo_Grp'
	mc.select(bshRigGeo, r=True)
	mc.select(fclBshGeo, add=True)
	lrr.doAddBlendShape()

	# Connect Bsh to FclRig
	connectGeoToBsh()

	#Wrap 
	bshStill = mc.ls('*:BshRigStill_Grp')
	bshCtrl = mc.ls('*:BshRigCtrl_Grp')
	mc.parent(bshCtrl , CTRL_GRP)
	mc.parent(bshStill , STILL_GRP)

def coonect7BshRig():
	## BlendShape
	asset = context_info.ContextPathInfo()
	projectName = asset.project

	if projectName == 'Hanuman':
		mc.setAttr('*:MouthCnrBsh_L_CtrlShape.autoCheek',0)
		mc.setAttr('*:MouthCnrBsh_L_CtrlShape.autoEyeLid',0)
		mc.setAttr('*:MouthCnrBsh_R_CtrlShape.autoCheek',0)
		mc.setAttr('*:MouthCnrBsh_R_CtrlShape.autoEyeLid',0)


	addBshToFcl(obj = 'BshRigStill_Grp', target = 'Fcl')

	ctrl, jnt, skin, still, geo = findMainGroup('BshRig')
	try:
		if ctrl:
			mc.parent(ctrl, CTRL_GRP)
		if jnt:
			mc.parent(jnt, JNT_GRP)
		if skin:
			mc.parent(skin, SKIN_GRP)
		if still:
			mc.parent(still, STILL_GRP)
		if geo:
			mc.parent(geo, STILL_GRP)
	except: pass

def connectGeoToBsh(*args):
	FacialBshZro = mc.ls("*:FacialBshGeo_Grp")
	FacialBshGeoList =[]
	try:
		FacialBshGeo = mc.listRelatives(FacialBshZro[0], c = True, s = False)
		for i in FacialBshGeo:
			if not '_Grp' in i :
				FacialBshGeoList.append(i)
			else:
				FacialBshGeoSub1 = mc.listRelatives(mc.ls(i), c = True, s = False)
				for x in FacialBshGeoSub1:
					if not '_Grp' in x :
						FacialBshGeoList.append(x)
	except: pass

	for each in FacialBshGeoList:
		mc.select(each)
		# connect7SelectedToHeadDeform()

def addBshToFcl(obj = '', target = '', *args):
	if not obj:
		print 'Please Re Check "BshRigGeo_Grp" Of Bsh'
	# Add BlendShape Mth 
	MthRigGeo = mc.ls('*:%s' % obj)[0]
	listObj = utaCore.listGeo(sels = MthRigGeo)
	# listObj = mc.listRelatives(MthRigGeo, c = True, s = False)
	for each in listObj:
		splits = each.split(':')
		pathsName = obj.split('RigStill_Grp')
		source = pathsName[0]
		fclLists = splits[-1].replace(source, target)
		mc.select(each, r =True)
		mc.select(fclLists, add=True)
		lrr.doAddBlendShape()

def connectEyeRig():
	'''
	Connect eyeRig to bodyRig
	'''
	mainRigNs = ''
	eyeRigNs = ''
	
	# Get namespace
	for each in mc.ls():

		if ('Eye_' in each) and each.endswith('_Jnt'):
			mainRigNs = ':'.join(each.split(':')[:-1])

		if ('CntEyeRig_' in each) and each.endswith('_Ctrl'):
			eyeRigNs = ':'.join(each.split(':')[:-1])
	
	for side in ('_L_', '_R_'):
		
		eyeJnt = lrc.Dag('%s:Eye%sJnt' % (mainRigNs, side))
		# eyeRigCtrlZr = lrc.Dag('%s:CntEyeRigCtrlZr%sGrp' % (eyeRigNs, side))
		eyeRigCtrlOri = lrc.Dag('%s:CntEyeRigCtrlOri%sGrp' % (eyeRigNs, side))
		
		# eyeJnt.attr('r') >> eyeRigCtrlZr.attr('r')
		eyeJnt.attr('r') >> eyeRigCtrlOri.attr('r')
		
		eyeRigCtrl = lrc.Dag('%s:CntEyeRig%sCtrl' % (eyeRigNs, side))
		cntLidZr = lrc.Dag('%s:PartCntLidRigJntZr%sGrp' % (eyeRigNs, side))
		
		followAmper = lrc.MultiplyDivide()
		followAmper.name = 'LidFlw%sMdv' % side

		eyeJnt.attr('r') >> followAmper.attr('i1')
		eyeRigCtrl.attr('lidFollow') >> followAmper.attr('i2x')
		eyeRigCtrl.attr('lidFollow') >> followAmper.attr('i2y')
		eyeRigCtrl.attr('lidFollow') >> followAmper.attr('i2z')
		followAmper.attr('o') >> cntLidZr.attr('r')
		try:
			eyeCtrlZr = lrc.Dag('%s:EyeCtrlZr%sGrp' % (mainRigNs, side))
			eyeCtrlZr.attr('v').v = 0
		except: pass

def connectSelectedToHeadDeform():
	'''
	Connect each selected geometries to the same geometries in HeadRig module.
	'''
	for sel in mc.ls(sl=True):
		
		currNmElms = re.search(r'(.+:)([a-zA-Z0-9]+)(.+)', sel)
		objMidNmElms = re.findall('[A-Z][^A-Z]*', currNmElms.group(2))
		objMidNmElms[-2] = 'Hd'
		hdRigMidNm = ''.join(objMidNmElms)

		hdRigNm = '{midNm}{suffix}'.format(midNm=hdRigMidNm, suffix=currNmElms.group(3))
		hdRigGeo = mc.ls('*:%s' % hdRigNm)[0]

		if not mc.objExists(hdRigGeo): print '%s has not been found.' % hdRigGeo; return False

		mc.select(sel, r=True)
		mc.select(hdRigGeo, add=True)

		print 'Adding blend shape from {src} to {tar}'.format(src=sel, tar=hdRigGeo)

		lrr.doAddBlendShape()

	return True

def connect7SelectedToHeadDeform():
	'''
	Connect each selected geometries to the same geometries in HeadRig module.
	'''
	for sel in mc.ls(sl=True):
		
		currNmElms = re.search(r'(.+:)([a-zA-Z0-9]+)(.+)', sel)
		objMidNmElms = re.findall('[A-Z][^A-Z]*', currNmElms.group(2))
		objMidNmElms[-2] = 'Fcl'
		fclRigMidNm = ''.join(objMidNmElms)

		fclRigNm = '{midNm}{suffix}'.format(midNm=fclRigMidNm, suffix=currNmElms.group(3))

		mc.select(sel, r=True)
		mc.select(fclRigNm, add=True)

		print 'Adding blend shape from {src} to {tar}'.format(src=sel, tar=fclRigNm)

		lrr.doAddBlendShape()

	return True

def createMainGroup():
	'''
	Create the main groups for FclRig module.
	'''
	ctrlGrp = lrc.Null()
	ctrlGrp.name = CTRL_GRP

	jntGrp = lrc.Null()
	jntGrp.name = JNT_GRP

	skinGrp = lrc.Null()
	skinGrp.name = SKIN_GRP

	stillGrp = lrc.Null()
	stillGrp.name = STILL_GRP

	rvtGrp = lrc.Null()
	rvtGrp.name = RVT_GRP
	rvtGrp.parent(ctrlGrp)

# HdRig
def createHdRigSpaceGroup():
	'''
	Create head deform space groups in FclRig module.
	'''
	for part in ('Low', 'Mid', 'Hi'):

		currSpcGrp = lrc.Null()
		currSpcGrp.name = 'Hd{}FclRigSpc_Grp'.format(part)

		currSpcZrGrp = lrc.group(currSpcGrp)
		currSpcZrGrp.name = 'Hd{}FclRigSpcZr_Grp'.format(part)

		curSpcSrc = lrc.Dag(mc.ls('*:Hd{}HdDfmRigSpc_Grp'.format(part))[0])
		curSpcZrSrc = lrc.Dag(mc.ls('*:Hd{}HdDfmRigSpcZr_Grp'.format(part))[0])

		currSpcZrGrp.snap(curSpcZrSrc)

		for attr in ('t', 'r', 's'):
			curSpcSrc.attr(attr) >> currSpcGrp.attr(attr)

		currSpcZrGrp.parent(CTRL_GRP)

def parentHdRig():

	ctrl, jnt, skin, still = findMainGroup('HdDfmRig')
	mc.parent(ctrl, CTRL_GRP)
	mc.parent(jnt, JNT_GRP)
	mc.parent(skin, SKIN_GRP)
	mc.parent(still, STILL_GRP)
	## Hide GeoVis

def coonect7HdRig():
	## BlendShape
	addBshToFcl(obj = 'HdDfmRigStill_Grp', target = 'Fcl')

	ctrl, jnt, skin, still, geo = findMainGroup('HdDfmRig')
	nameSp = ctrl.split(':')[0]
	
	try:
		if ctrl:
			mc.parent(ctrl, CTRL_GRP)
		if jnt:
			mc.parent(jnt, JNT_GRP)
		if skin:
			mc.parent(skin, SKIN_GRP)
		if still:
			mc.parent(still, STILL_GRP)
		if geo:
			mc.parent(geo, STILL_GRP)
	except: pass
	
	## Hide Control
	lists = ['HdUpHdDfmRigCtrlZr_Grp', 'HdLowHdDfmRigCtrlZr_Grp']
	for each in lists:
		mc.setAttr('{}:{}.v'.format(nameSp, each), 0)

def conect7WrinkledRig():
	from utaTools.utapy import utaCore
	reload(utaCore)

	mc.select(ado=True)
	sel = mc.ls(sl=True)
	for i in sel:
		if "WrinkledRigGeo" in i:
			geo = i
		if "WrinkledRigJnt" in i:
			jnt = i
		if "WrinkledRigCtrl" in i:
			ctrl = i
		if "WrinkledRigStill" in i:
			still = i

	posGeo = "BodyFclRigPost_Geo"
	utaCore.groupWrinkledRig(motherGeo=posGeo, geoGrp=geo, ctrlGrp=ctrl, stillGrp=still)

	mc.parent(ctrl, CTRL_GRP)
	mc.parent(jnt, JNT_GRP)
	mc.parent(geo, still, STILL_GRP)
	mc.select(cl=True)

# EyeRig
def parentEyeRig():

	ctrl, jnt, skin, still = findMainGroup('EyeRig')
	mc.parent(ctrl, HD_HI_GRP)
	mc.parent(jnt, JNT_GRP)
	mc.parent(skin, SKIN_GRP)
	mc.parent(still, STILL_GRP)

def stickEyeCtrl(postGeo='BodyFclRigPost_Geo'):

	eyeCtrls = ['LidUpEyeRig_L_Ctrl', 'LidLowEyeRig_L_Ctrl', 'CnrInLidRig_L_Ctrl', 'CnrInUpLidRig_L_Ctrl',
				'CnrInDnLidRig_L_Ctrl', 'CnrOutLidRig_L_Ctrl', 'CnrOutUpLidRig_L_Ctrl', 'CnrOutDnLidRig_L_Ctrl',
				'UpInLidRig_L_Ctrl', 'UpMidLidRig_L_Ctrl', 'UpOutLidRig_L_Ctrl', 'LowOutLidRig_L_Ctrl',
				'LowMidLidRig_L_Ctrl', 'LowInLidRig_L_Ctrl', 'CnrInSktLidRig_L_Ctrl', 'CnrOutSktLidRig_L_Ctrl',
				'UpInSktLidRig_L_Ctrl', 'UpMidSktLidRig_L_Ctrl', 'UpOutSktLidRig_L_Ctrl', 'LowOutSktLidRig_L_Ctrl',
				'LowMidSktLidRig_L_Ctrl', 'LowInSktLidRig_L_Ctrl', 'LidUpEyeRig_R_Ctrl', 'LidLowEyeRig_R_Ctrl',
				'CnrInLidRig_R_Ctrl', 'CnrInUpLidRig_R_Ctrl', 'CnrInDnLidRig_R_Ctrl', 'CnrOutLidRig_R_Ctrl',
				'CnrOutUpLidRig_R_Ctrl', 'CnrOutDnLidRig_R_Ctrl', 'UpInLidRig_R_Ctrl', 'UpMidLidRig_R_Ctrl',
				'UpOutLidRig_R_Ctrl', 'LowOutLidRig_R_Ctrl', 'LowMidLidRig_R_Ctrl', 'LowInLidRig_R_Ctrl',
				'CnrInSktLidRig_R_Ctrl', 'CnrOutSktLidRig_R_Ctrl', 'UpInSktLidRig_R_Ctrl', 'UpMidSktLidRig_R_Ctrl',
				'UpOutSktLidRig_R_Ctrl', 'LowOutSktLidRig_R_Ctrl', 'LowMidSktLidRig_R_Ctrl', 'LowInSktLidRig_R_Ctrl']
	
	mc.select(cl=True)
	for eyeCtrl in eyeCtrls:
		currCtrls = mc.ls('*:{ctrl}'.format(ctrl=eyeCtrl))
		if not currCtrls:
			continue
		mc.select(currCtrls[0], add=True)
	
	mc.select(postGeo, add=True)
	rvts = detailControl.attachSelectedControlsToSelectedMesh()
	mc.parent(rvts, RVT_GRP)

# MthRig
def parentMthRig():
	
	ctrl, jnt, skin, still = findMainGroup('MthRig')
	mc.parent(ctrl, HD_LOW_GRP)
	mc.parent(jnt, JNT_GRP)
	mc.parent(skin, SKIN_GRP)
	mc.parent(still, STILL_GRP)

def parent7MthRig():

	ctrl, jnt, skin, still, geo = findMainGroup('MthRig')
	if ctrl:
		mc.parent(ctrl, CTRL_GRP)
	if jnt:
		mc.parent(jnt, JNT_GRP)
	if skin:
		mc.parent(skin, SKIN_GRP)
	if still:
		mc.parent(still, STILL_GRP)
	if geo:
		mc.parent(geo, STILL_GRP)

	# Add BlendShape Mth 
	MthRigGeo = mc.ls('*:MthRigGeo_Grp')
	listObj = utaCore.listGeo(sels = MthRigGeo, nameing = '_Geo', namePass = '_Grp')
	for each in listObj:
		splits = each.split('mthRig:')
		fclLists = splits[-1].replace('Mth', 'Fcl')
		mc.select(each, r =True)
		mc.select(fclLists, add=True)
		lrr.doAddBlendShape()


def stickMthCtrl(postGeo='BodyFclRigPost_Geo'):

	mthCtrls = ['LipUpMthMthRig_Ctrl', 'LipLowMthMthRig_Ctrl', 'MthMthRig_L_Ctrl', 'MthMthRig_R_Ctrl',
				'LowMidLipRig_Ctrl', 'LowALipRig_L_Ctrl', 'LowBLipRig_L_Ctrl', 'LowALipRig_R_Ctrl',
				'LowBLipRig_R_Ctrl', 'LipRigDtl_11_Ctrl', 'LipRigDtl_12_Ctrl', 'LipRigDtl_13_Ctrl',
				'LipRigDtl_14_Ctrl', 'LipRigDtl_15_Ctrl', 'LipRigDtl_16_Ctrl', 'LipRigDtl_17_Ctrl',
				'LipRigDtl_18_Ctrl', 'LipRigDtl_19_Ctrl', 'LipRigDtl_20_Ctrl', 'LipRigDtl_21_Ctrl',
				'LipRigDtl_22_Ctrl', 'LipRigDtl_23_Ctrl', 'UpMidLipRig_Ctrl', 'CnrLipRig_L_Ctrl',
				'CnrLipRig_R_Ctrl', 'UpALipRig_L_Ctrl', 'UpBLipRig_L_Ctrl', 'UpALipRig_R_Ctrl',
				'UpBLipRig_R_Ctrl', 'LipRigDtl_1_Ctrl', 'LipRigDtl_2_Ctrl', 'LipRigDtl_3_Ctrl',
				'LipRigDtl_4_Ctrl', 'LipRigDtl_5_Ctrl', 'LipRigDtl_6_Ctrl', 'LipRigDtl_7_Ctrl',
				'LipRigDtl_8_Ctrl', 'LipRigDtl_9_Ctrl', 'LipRigDtl_10_Ctrl', 'LipRigDtl_24_Ctrl']
	mc.select(cl=True)
	for mthCtrl in mthCtrls:
		currCtrls = mc.ls('*:{ctrl}'.format(ctrl=mthCtrl))
		if not currCtrls:
			continue
		mc.select(currCtrls[0], add=True)

	mc.select(postGeo, add=True)
	rvts = detailControl.attachSelectedControlsToSelectedMesh()
	mc.parent(rvts, RVT_GRP)

# TtRig
def parentTtRig():

	ctrl, jnt, skin, still = findMainGroup('TtRig')
	mc.parent(ctrl, HD_LOW_GRP)
	mc.parent(jnt, JNT_GRP)
	mc.parent(skin, SKIN_GRP)
	mc.parent(still, STILL_GRP)

	jawPiv = None
	jawJnt = None
	for each in mc.ls():
		if each.endswith(':TtRigCtrlJawPost_Grp'):
			jawPiv = each
		elif each.endswith(':JawMthRig_Jnt'):
			jawJnt = each

	for attr in ('t', 'r', 's'):
		mc.connectAttr('{jawJnt}.{attr}'.format(jawJnt=jawJnt, attr=attr), 
						'{jawPiv}.{attr}'.format(jawPiv=jawPiv, attr=attr))
def parent7TtRig():

	ctrl, jnt, skin, still, geo = findMainGroup('TtRig')
	if ctrl:
		mc.parent(ctrl, CTRL_GRP)
	if jnt:
		mc.parent(jnt, JNT_GRP)
	if skin:
		mc.parent(skin, SKIN_GRP)
	if still:
		mc.parent(still, STILL_GRP)
	if geo:
		mc.parent(geo, STILL_GRP)

	# list Geo 
	TtRigGeo = utaCore.listGeo(sels = geo, nameing = '_Geo', namePass = '_Grp')
	i = 0 
	for each in TtRigGeo:
		if mc.objExists(each):
			name = each.split('|')[-1].split(':')[-1]
			fclGeo = name.replace('TtRig', 'FclRig')
			mc.select(each, r=True)
			mc.select(fclGeo, add=True)
			lrr.doAddBlendShape()
			i += 1

def parentEbRig():

	ctrl, jnt, skin, still = findMainGroup('EbRig')
	mc.parent(ctrl, HD_HI_GRP)
	mc.parent(still, STILL_GRP)

def stickEbCtrl(postGeo='BodyFclRigPost_Geo'):

	ctrls = ['AllEbRig_L_Ctrl', 'InEbRig_L_Ctrl', 'MidEbRig_L_Ctrl', 'OutEbRig_L_Ctrl',
				'AllEbRig_R_Ctrl', 'InEbRig_R_Ctrl', 'MidEbRig_R_Ctrl', 'OutEbRig_R_Ctrl',
				'EbRbn_1_L_Ctrl', 'EbRbn_2_L_Ctrl', 'EbRbn_3_L_Ctrl', 'EbRbn_4_L_Ctrl', 
				'EbRbn_1_R_Ctrl', 'EbRbn_2_R_Ctrl', 'EbRbn_3_R_Ctrl', 'EbRbn_4_R_Ctrl']
	mc.select(cl=True)
	for ctrl in ctrls:
		currCtrls = mc.ls('*:{ctrl}'.format(ctrl=ctrl))
		if not currCtrls:
			continue
		mc.select(currCtrls[0], add=True)

	mc.select(postGeo, add=True)
	rvts = detailControl.attachSelectedControlsToSelectedMesh()
	mc.parent(rvts, RVT_GRP)

# NsRig
def parentNsRig():
	
	ctrl, jnt, skin, still = findMainGroup('NsRig')
	mc.parent(ctrl, HD_MID_GRP)
	mc.parent(skin, SKIN_GRP)
	mc.parent(still, STILL_GRP)

# EarRig
def parentEarRig():
	
	ctrl, jnt, skin, still = findMainGroup('EarRig')
	mc.parent(ctrl, HD_HI_GRP)
	mc.parent(skin, SKIN_GRP)
	mc.parent(still, STILL_GRP)

# DtlRig
def parentDtlRig():
	
	ctrl, jnt, skin, still = findMainGroup('DtlRig')
	mc.parent(skin, SKIN_GRP)
	mc.parent(still, STILL_GRP)

	mc.parent(mc.ls('*:DtlRigCtrlLow_Grp')[0], HD_LOW_GRP)
	mc.parent(mc.ls('*:DtlRigCtrlMid_Grp')[0], HD_MID_GRP)
	mc.parent(mc.ls('*:DtlRigCtrlHi_Grp')[0], HD_HI_GRP)

def parent7DtlRig():
	ctrl, jnt, skin, still, geo = findMainGroup('DtlRig')
	if ctrl: 
		mc.parent(ctrl, CTRL_GRP)
	if skin: 
		mc.parent(skin, SKIN_GRP)
	if still: 
		mc.parent(still, STILL_GRP)
	if geo: 
		mc.parent(geo, STILL_GRP)
	if jnt: 
		mc.parent(jnt, JNT_GRP)

	# Adding blend shape
	DtlRigGeo = mc.ls('*:DtlRigGeo_Grp')
	listObj = utaCore.listGeo(sels = DtlRigGeo, nameing = '_Geo', namePass = '_Grp')
	for each in listObj:
		splits = each.split('dtlRig:')
		fclLists = splits[-1].replace('Dtl', 'Fcl')
		mc.select(each, r =True)
		mc.select(fclLists, add=True)
		lrr.doAddBlendShape()
	# Parent
	dtlCtrlGrp = mc.ls('*:DtlRigCtrlHi_Grp','*:DtlRigCtrlMid_Grp','*:DtlRigCtrlLow_Grp', '*:DtlRigCtrlMidLow_Grp')
	mc.parent(dtlCtrlGrp, CTRL_GRP)
	for eachDtlGrp in dtlCtrlGrp:
		mc.setAttr(eachDtlGrp+'.inheritsTransform',0)
	

def stickDtlCtrl(postGeo='BodyFclRigPost_Geo'):

	ctrls =[
			'NsDtlRig_L_Ctrl', 'CheekADtlRig_L_Ctrl','CheekBDtlRig_L_Ctrl','CheekCDtlRig_L_Ctrl', 
			'NsDtlRig_R_Ctrl', 'CheekADtlRig_R_Ctrl','CheekBDtlRig_R_Ctrl', 'CheekCDtlRig_R_Ctrl', 
			'EyeSktADtlRig_L_Ctrl','EyeSktBDtlRig_L_Ctrl', 'EyeSktADtlRig_R_Ctrl', 'EyeSktBDtlRig_R_Ctrl',
			'PuffDtlRig_L_Ctrl', 'PuffDtlRig_R_Ctrl','NsDtlRig_Ctrl','EyeBrowDtlRig_Ctrl',
			'EyeBrowADtlRig_L_Ctrl','EyeBrowBDtlRig_L_Ctrl','EyeBrowCDtlRig_L_Ctrl','EyeBrowADtlRig_R_Ctrl',
			'EyeBrowBDtlRig_R_Ctrl','EyeBrowCDtlRig_R_Ctrl',]
	mc.select(cl=True)
	for ctrl in ctrls:
		currCtrls = mc.ls('*:{ctrl}'.format(ctrl=ctrl))
		if not currCtrls:
			continue
		mc.select(currCtrls[0], add=True)

	mc.select(postGeo, add=True)
	rvts = detailControl.attachSelectedControlsToSelectedMesh()
	mc.parent(rvts, RVT_GRP)  

def stick7DtlCtrl(postGeo='BodyFclRigPost_Geo'):
	NrbObj = mc.ls('*:*Dtl*_Nrb')
	mc.select(NrbObj, r=True)
	mc.select(postGeo, add=True)
	wrap = mm.eval('doWrapArgList "7"{"1","0","0.1","2","1","0","1"};')[0]

def stickDtlCtrlAdd(postGeo="", ctrls= []):

	# ctrls = ['NsDtlRig_L_Ctrl', 'CheekADtlRig_L_Ctrl', 'CheekBDtlRig_L_Ctrl',
	#       'CheekCDtlRig_L_Ctrl', 'NsDtlRig_R_Ctrl', 'CheekADtlRig_R_Ctrl',
	#       'CheekBDtlRig_R_Ctrl', 'CheekCDtlRig_R_Ctrl', 'EyeSktADtlRig_L_Ctrl',
	#       'EyeSktBDtlRig_L_Ctrl', 'EyeSktADtlRig_R_Ctrl', 'EyeSktBDtlRig_R_Ctrl',
	#       'PuffDtlRig_L_Ctrl', 'PuffDtlRig_R_Ctrl']
	# mc.select(cl=True)
	for ctrl in ctrls:
		currCtrls = mc.ls('*:{ctrl}'.format(ctrl=ctrl))
		if not currCtrls:
			continue
		mc.select(currCtrls[0], add=True)

	mc.select(postGeo, add=True)
	rvts = detailControl.attachSelectedControlsToSelectedMesh()
	mc.parent(rvts, RVT_GRP)

def findMainGroup(part=''):
	'''
	Find the main groups of the given facial rig part.
	'''
	ctrl = None
	jnt = None
	skin = None
	still = None
	geo = None

	for each in mc.ls():
		if each.endswith(':{part}Ctrl_Grp'.format(part=part)):
			ctrl = each
		elif each.endswith(':{part}Jnt_Grp'.format(part=part)):
			jnt = each
		elif each.endswith(':{part}Skin_Grp'.format(part=part)):
			skin = each
		elif each.endswith(':{part}Still_Grp'.format(part=part)):
			still = each
		elif each.endswith(':{part}CtrlHi_Grp'.format(part=part)):
			ctrl = each
		elif each.endswith(':{part}CtrlMid_Grp'.format(part=part)):
			ctrl = each
		elif each.endswith(':{part}CtrlLow_Grp'.format(part=part)):
			ctrl = each
		elif each.endswith(':{part}Geo_Grp'.format(part=part)):
			geo = each

	return ctrl, jnt, skin, still, geo


def addBshFacial(hdRigList = [], fclRigList = []):
	for i in range(0, len(hdRigList)):
		currHdRig = []
		currFclRig = []        
		currHdRig = mc.ls("*:%s" % hdRigList[i])
		currFclRig = (fclRigList[i])
		listAmount = len(currHdRig)
		if listAmount != 0:
			mc.select(currHdRig)
			mc.select(currFclRig, add=True)
			lrr.doAddBlendShape()
		else :
			print hdRigList[i], "This Object is not Ready"  

	print "Create BlendShape Hd 2 Fcl is Done!!"

def addBshHd2Fcl(hdRigGeo ='', fclRigGeo = '',*args):
	# lists = mc.ls(hdRigGeo,fclRigGeo)
	mc.select(hdRigGeo)
	mc.select(fclRigGeo, add=True)
	lrr.doAddBlendShape()
	listsPos = mc.ls("BodyFclRig_Geo", "BodyFclRigPost_Geo")
	mc.select(listsPos[0])
	mc.select(listsPos[1], add=True)
	lrr.doAddBlendShape()
def dupHeadFclRig(headSel=''):
	#2. duplicate "BodyFclRigPos_Geo" and parent to FclRigStill_Grp
	if mc.objExists (headSel)==True:
		pass
	else :
		headSel = mc.ls("BodyFclRig_Geo")
	headGeoPos = mc.duplicate(headSel)
	headName = mc.rename(headGeoPos, "BodyFclRigPost_Geo")
	mc.parent(headName,w=True)
	mc.parent(headName, "FclRigStill_Grp")

def sqHairPaScaConstraint():
	lists = mc.ls("*:HdHiFclRigSpc_Grp","*:HairPntCtrlZr_Grp")
	mc.parentConstraint(lists[0], lists[1], mo=True)
	mc.scaleConstraint(lists[0], lists[1], mo=True)

def fixEyeDot():
	headTip = ("*:EyeDotPntCtrlZr_L_Grp", "*:EyeDotPntCtrlZr_R_Grp")
	cntEyeRig = ("*:CntEyeRig_L_Ctrl", "*:CntEyeRig_R_Ctrl")


	# parent scale Constriant
	mc.parentConstraint (cntEyeRig[0], headTip[0], mo=True)
	mc.scaleConstraint (cntEyeRig[0], headTip[0], mo=True)

	mc.parentConstraint (cntEyeRig[1], headTip[1], mo=True)
	mc.scaleConstraint (cntEyeRig[1], headTip[1], mo=True)

def connectFang():
	## List 
	jaw1LwrJnt = mc.ls('*:mthJawLwr1_Jnt')
	jaw2LwrJnt = mc.ls('*:mthJawLwr2_Jnt')

	## CreateNode
	FangMthPma = mc.createNode ('plusMinusAverage', n = 'FangMthRig_pma')
	FangMthMdv = mc.createNode ('multiplyDivide', n = 'FangMthRig_mdv')
	FangMthRev = mc.createNode ('reverse', n = 'FangMthRig_rev')
	FangMthClm = mc.createNode ('clamp', n = 'FangMthRig_clm')

	## Setting Node
	mc.setAttr('%s.input2X' % FangMthMdv, 0.5)
	mc.setAttr('%s.minR' % FangMthClm, 0)
	mc.setAttr('%s.maxR' % FangMthClm, 1)

	## ConnectAttr 
	mc.connectAttr ('%s.rotateX' % jaw1LwrJnt[0], '%s.input1D[0]' % FangMthPma)
	mc.connectAttr ('%s.rotateX' % jaw2LwrJnt[0], '%s.input1D[1]' % FangMthPma)
	mc.connectAttr ('%s.output1D' % FangMthPma,  '%s.input1X' % FangMthMdv)
	mc.connectAttr ('%s.outputX' % FangMthMdv, '%s.inputX' % FangMthRev)
	mc.connectAttr ('%s.outputX' % FangMthRev, '%s.inputR' % FangMthClm)
	mc.connectAttr ('%s.outputR' % FangMthClm, 'TeethFclRig_Bsn.TeethMthRig_Geo')


def parentConClothDtlOfs():
	source = mc.ls('bodyRig*:Head_Jnt')
	obj = mc.ls('clothRig*:ClothB*DtlCtrlOfst_*_Grp')
	for each in obj:
		utaCore.parentScaleConstraintLoop(obj = source[0] , lists = [each],par = False, sca = True, ori = True, poi = False)

	source = mc.ls('bodyRig*:Neck_Jnt')
	obj = mc.ls('clothRig*:ClothA*DtlCtrlOfst_*_Grp')
	for each in obj:
		utaCore.parentScaleConstraintLoop(obj = source[0] , lists = [each],par = False, sca = True, ori = True, poi = False)


## Scrips Connect EyeLidUp for MasterToad
def eyeLidsMidOutUpMasterToad():
	## Lists 
	facialMover = mc.ls('FacialCtrlMover_EyeLidsLwrAuto_L_Mdv', 'FacialCtrlMover_EyeLidsLwrAuto_R_Mdv')
	bshNode = 'FacialAll_Bsn'
	eyeLidLobj = mc.ls('EyeLidsLwrMidUD_L_Mdv', 'EyeLidsLwrMidUD_R_Mdv')

	# eyeLidMidobj = mc.ls('EyeLidsLwrMidUD_L_Mdv', 'EyeLidsLwrMidUD_R_Mdv')
	eyeLidOutobj = mc.ls('EyeLidsLwrOutUD_L_Mdv', 'EyeLidsLwrOutUD_R_Mdv')
	i = 0
	for each in eyeLidLobj:
		spObj = each.split('_')
		elem = spObj[0]
		side = spObj[1]

		## CreateNode 
		eyeLidMidMdv = mc.createNode('multiplyDivide', n = '%sMid_%s_Mdv' % (elem , side))
		eyeLidMidPma = mc.createNode('plusMinusAverage', n = '%sMid_%s_Pma' % (elem , side))
		eyeLidOutMdv = mc.createNode('multiplyDivide', n = '%sOut_%s_Mdv' % (elem , side))
		eyeLidOutPma = mc.createNode('plusMinusAverage', n = '%sOut_%s_Pma' % (elem , side))

		## Set Attribute
		mc.setAttr ("%s.input2Y" % eyeLidMidMdv, 0.5)
		mc.setAttr ("%s.input2Y" % eyeLidOutMdv, 0.7)

		## Connect Attribute Mid
		mc.connectAttr('%s.outputX' % facialMover[i], '%s.input1Y' % eyeLidMidMdv)
		mc.connectAttr('%s.outputX' % each, '%s.input1X' % eyeLidMidMdv)
		mc.connectAttr('%s.outputX' % eyeLidMidMdv, '%s.input1D[0]' % eyeLidMidPma)
		mc.connectAttr('%s.outputY' % eyeLidMidMdv, '%s.input1D[1]' % eyeLidMidPma)
		mc.disconnectAttr('FacialCtrlMover_EyeLidsLwrAuto_%s_Mdv.outputX' % side, 'FacialCtrlMover_EyeLidsLwrUp_%s_Pma.input1D[1]' % side)
		mc.connectAttr('%s.output1D' % eyeLidMidPma, '%s.LoLidMidUp_%s' % (bshNode , side),f = True)

		## Connect Attribute Out
		mc.connectAttr('%s.outputX' % facialMover[i], '%s.input1Y' % eyeLidOutMdv)
		mc.connectAttr('%s.outputX' % eyeLidOutobj[i], '%s.input1X' % eyeLidOutMdv)
		mc.connectAttr('%s.outputX' % eyeLidOutMdv, '%s.input1D[0]' % eyeLidOutPma)
		mc.connectAttr('%s.outputY' % eyeLidOutMdv, '%s.input1D[1]' % eyeLidOutPma)
		mc.connectAttr('%s.output1D' % eyeLidOutPma, '%s.LoLidOutUp_%s' % (bshNode , side),f = True)

		i += 1

def importHdGeo(*args):
	#import hdGeo group for fclRig process
	entity = context_info.ContextPathInfo()
	processName = entity.process.split('Rig')[0]
	asset = entity.copy()
	asset.context.update(process='hdGeo',res='md')
	heroPath = asset.path.output_hero().abs_path()
	hdGeoFile = asset.publish_name(hero=True)
	rigHdGeoFile = '{}/{}'.format(heroPath,hdGeoFile)

	if os.path.exists(rigHdGeoFile):
		try:
			dcc_hook.import_file(rigHdGeoFile)
			print '============= import hd geo group done ============='
		except:
			pass

	#rename hdGeo
	geoGrp = ['FclRigGeo_Grp']
	child = mc.listRelatives(geoGrp,ad=True)
	for each in child:
		name = each.split('Fcl')
		c = mc.rename(each,'{}{}{}'.format(name[0],processName.capitalize(),name[1]))
	childNew = mc.listRelatives(geoGrp,c=True)
	mc.parent(childNew,'*:{}RigGeo_Grp'.format(processName.capitalize()))
	mc.delete(geoGrp)



# import maya.cmds as mc
# import random ;
# white = 16 ;
# lightYellow = 21 ;
# lightBlue = 18 ;
# lightPink = 19 ;
# skin = 20 ;
# lightGreen = 22 ;
# purple = 30 ;

# color = [ white , lightYellow , lightBlue , lightPink , skin , lightGreen , purple ] ;

# colorRandom = random.SystemRandom ( ) ;
# import pymel.core as pm ;
# def randomColor(sel=''):
#   sel = pm.ls ( sl = True ) ;
#   for each in sel : 
#     # curveShape = each + 'Shape' ;   
#     # curveShape = pm.general.PyNode ( each ) ;
#     # curveShape.overrideEnabled.set ( 1 ) ;
#     curveShape.overrideColor.set ( colorRandom.choice ( color ) ) ;
#     randomSel = sel(colorRandom.choice ( color ))
'''
# Scripts for each part

# HdRig
from lpRig import headDeformRig
reload(headDeformRig)
headDeformRig.main()

# EyeRig
from lpRig import eyeRig
reload(eyeRig)

eyeRig.main(do25=True, do50=True, do75=True)
rigData.readCtrlShape(fn='eyeRigCtrlShape.dat')
rigData.writeCtrlShape(fn='eyeRigCtrlShape.dat')
eyeRig.doCreateBsh()

eyeRig.mainLite()
eyeRig.createLidBsh('LidRigGeo_Grp', 'Lid')

# EbRig
from lpRig import eyebrowRig
reload(eyebrowRig)
eyebrowRig.main()
rigData.readCtrlShape('ebRigCtrlShape.dat')
eyebrowRig.createBsh('EbRigBshBuf_Bsh', 'EbRig')

eyebrowRig.main(detail=False, lite=True)
eyebrowRig.createBsh('EbRigBshBuf_Bsh', 'EbRig', True)

eyebrowRig.doApplyVertexMove()

# MthRig
from lpRig import mouthRig
reload(mouthRig)

mouthRig.main(lipRig=True)
rigData.readCtrlShape(fn='mouthRigCtrlShape.dat')
mouthRig.createBsh('MthRigBshBuf_Bsh', 'MthRig')

mouthRig.mainLite()

mouthRig.LipPartRibbon(
							tmpLocs=[
										'LowMidLipRigTmp_Loc',
										'CnrLipRigTmp_L_Loc',
										'CnrLipRigTmp_R_Loc',
										'LowALipRigTmp_L_Loc',
										'LowBLipRigTmp_L_Loc',
										'LowALipRigTmp_R_Loc',
										'LowBLipRigTmp_R_Loc'
									],
							part='TmpLip',
							offsetCurve=0.1
						)

from lpRig import applyLeftShape
applyLeftShape.main(0.1, 1)

# HdRig
from lpRig import headDeformRig
reload(headDeformRig)
headDeformRig.main()

# TtRig
from lpRig import ttRig
reload(ttRig)
ttRig.main()
rigData.readCtrlShape('ttRigCtrlShape.dat')

# NsRig
from lpRig import noseRig as nsRig
reload(nsRig)
nsRig.main()
rigData.readCtrlShape('nsRigCtrlShape.dat')

# EarRig
from lpRig import earRig
reload(earRig)
earRig.main()
rigData.readCtrlShape('earRigCtrlShape.dat')

# DtlRig
from lpRig import detailRig
detailRig.createDetailRigToSelected()

## Assembly Part ##

# FclRig
from lpRig import fclRig
reload(fclRig)

from utaTools.utapy import utaBlendShapeAdder
reload(utaBlendShapeAdder)
utaBlendShapeAdder.run()

fclRig.refElem('FclRig')
fclRig.fclRigAssembler()

fclRig.createMainGroup()
fclRig.connectSelectedToHeadDeform()
fclRig.connectEyeRig()

# FclRig HdRig
fclRig.createHdRigSpaceGroup()
fclRig.parentHdRig()

# FclRig EyeRig
fclRig.refElem('EyeRig')
fclRig.parentEyeRig()
fclRig.stickEyeCtrl()

# FclRig EbRig
fclRig.refElem('EbRig')
fclRig.parentEbRig()
fclRig.stickEbCtrl()

# FclRig MthRig
fclRig.refElem('MthRig')
fclRig.parentMthRig()
fclRig.stickMthCtrl()

# FclRig TtRig
fclRig.refElem('TtRig')
fclRig.parentTtRig()

# FclRig NsRig
fclRig.parentNsRig()

# FclRig EarRig
fclRig.parentEarRig()

# FclRig DtlRig
fclRig.parentDtlRig()
fclRig.stickDtlCtrl()
'''