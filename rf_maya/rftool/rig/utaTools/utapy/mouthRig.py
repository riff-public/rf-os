import maya.cmds as mc
import pymel.core as pm
from utaTools.utapy import utaCore
reload(utaCore)
from utaTools.pkmel import rigTools as rigTools
reload( rigTools )

def lipsRig(spans = 16, *args):
	jntFoll = []
	jntDtl = []
	followJntGrp = 'jntFollow_Grp'
	lipsJntDtlGrp = 'jntLipsDtl_Grp'
	lipsRigCtrlGrp = 'lipsRigCtrl_Grp'
	lipsMainRigCtrlGrp = 'lipsMainRigCtrl_Grp'
	mouthJntGrp = 'mouthJnt_Grp'
	sels = mc.ls(sl = True)

	if not mc.objExists(followJntGrp):
		followJntGrp = mc.group( n ='jntFollow_Grp', em = True)
	if not mc.objExists(lipsJntDtlGrp):
		lipsJntDtlGrp = mc.group( n ='jntLipsDtl_Grp', em = True)
	if not mc.objExists(lipsRigCtrlGrp):
		lipsRigCtrlGrp = mc.group( n ='lipsRigCtrl_Grp', em = True)
	if not mc.objExists(mouthJntGrp):
		mouthJntGrp = mc.group( n ='mouthJnt_Grp', em = True)
	if not mc.objExists(lipsMainRigCtrlGrp):
		lipsMainRigCtrlGrp = mc.group( n ='lipsMainRigCtrl_Grp', em = True)

	for sel in sels:

		utaCore.lockAttrObj(listObj = [sel], lock = False, keyable = True)

		# splitName
		nameAll = utaCore.splitName(sels = [sel])
		name = nameAll[0]
		side = nameAll[1]
		lastName = nameAll[-1]
		utaCore.reBuildCurve(lists = [sel], spans = spans, degree = 3)
		jointFnt, jointBck, jointAll = utaCore.createJointOnCurve(cur_name = sel, name = name, elem = 'Follow', side = side,  span_curve = spans)

		## Joint for Control Detail Mouth
		jntDtlFnt, jntDtlBck, jntDtlAll = utaCore.createJointOnCurve(cur_name = sel, name = name, elem = 'Dtl', side = side,  span_curve = spans)

		## Joint for Control Detail Mouth
		jntMouthFnt, jntMouthBck, jntMouthAll = utaCore.createJointOnCurve(cur_name = sel, name = name, elem = 'Mouth', side = side,  span_curve = spans)


		## Delete follow Jnt
		for eachFollJnt in range(0, len(jointAll), 2):
			mc.delete(jointAll[eachFollJnt])
			mc.parent(jointAll[eachFollJnt+1], followJntGrp)
			mc.setAttr('{}.radius'.format(jointAll[eachFollJnt+1]), 0.1)
			jntFoll.append(jointAll[eachFollJnt+1])

		## Dtl Jnt Group
		for eachDtlJnt in range(0, len(jntDtlAll), 2):
			mc.delete(jntDtlAll[eachDtlJnt])
			mc.parent(jntDtlAll[eachDtlJnt+1], lipsJntDtlGrp)
			mc.setAttr('{}.radius'.format(jntDtlAll[eachDtlJnt+1]), 0.25)
			jntDtl.append(jntDtlAll[eachDtlJnt+1])
		
		jntLists = []
		removeList = []
		for each in jntMouthAll:
			for x in range(1, len(jntMouthAll), 4):
				if each == jntMouthAll[x]:
					jntLists.append(jntMouthAll[x])
					mc.parent(jntMouthAll[x], mouthJntGrp)
					mc.setAttr('{}.radius'.format(jntMouthAll[x]), 0.5)

		removeList = [a for a in jntMouthAll if not a in jntLists]
		mc.delete(removeList)

		## jntMouthAll bindSkin Curve
		utaCore.bindSkinCluster(jntObj = jntLists, obj = [sel])

		## Create Control jntMain
		for each in jntLists:
			mc.select(each)
			mainJntGrp = rigTools.doZeroGroup()
			mc.select(each)
			mainCtrlName, mainCtrlOfsName, mainCtrlParsName, mainCtrlGrpName, mainCtrlGmblGrpName = utaCore.createControlCurve(curveCtrl = 'cube', parentCon = False, connections = True, geoVis = False)
			mc.parent(mainCtrlName, lipsMainRigCtrlGrp)

		## Attach to Motion Path
		pathUValue = [0, 0.13, 0.25, 0.38, 0.5, 0.63, 0.75, 0.87]
		for sel in sels:
			for i in range(len(jntFoll)):
				mc.select(jntFoll[i])
				mc.select(sel, add = True)
				mc.pathAnimation (fractionMode = False, follow = True, followAxis= 'x', upAxis = 'y', worldUpType =  "vector", worldUpVector= [0, 1, 0],inverseUp = False, inverseFront= False, bank = False)
				motionPathObj = setMotionPath(objAll = [jntFoll[i]])
				mc.setAttr ("{}.uValue".format(motionPathObj), pathUValue[i])
				mc.select(cl = True)

		## Create Dtl Control lips
		for i in range(len(jntDtl)):
			mc.select(jntDtl[i])
			jntDtlGrp = rigTools.doZeroGroup()
			mc.select(jntDtl[i])
			grpCtrlName, grpCtrlOfsName, grpCtrlParsName, ctrlName, GmblCtrlName = utaCore.createControlCurve(curveCtrl = 'circle', parentCon = False, connections = True, geoVis = False)
			utaCore.assignColor(obj = [ctrlName], col = 'yellow')
			mc.parent(grpCtrlName, lipsRigCtrlGrp)

			## parentConstraint Follow jnt to Dtl Control
			utaCore.parentScaleConstraintLoop(obj = jntFoll[i], lists = [grpCtrlName] ,par = False, sca = False, ori = True, poi = True)


def setMotionPath(objAll = [],*args):
	for each in objAll:
		conObj = mc.listConnections(each)
		for each in conObj:
		    if 'motionPath' in each:
				motionPathObj = each.split('_')[0]
				conObj = mc.listConnections("{}.uValue".format(each))
				utaCore.disConToObj(objA = '{}.output'.format(conObj[0]), objB = '{}.uValue'.format(motionPathObj), connectionsObj = False)
				break
        	return motionPathObj