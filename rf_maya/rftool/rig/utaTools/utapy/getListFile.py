import maya.cmds as cmds



# List the contents of the user's projects directory
#
	cmds.getFileList( folder=cmds.internalVar(userWorkspaceDir=True) )

# List all MEL files in the user's script directory
#
	cmds.getFileList( folder=cmds.internalVar(userScriptDir=True), filespec='*.mel' )




