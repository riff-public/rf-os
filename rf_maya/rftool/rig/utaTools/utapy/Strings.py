import pymel.core as pm
import maya.cmds as mc
import maya.mel as mm
reload(mc)
from utaTools.utapy import utaCore
reload(utaCore)
from ncmel import subRig
reload(subRig)

def Strings(    name = '', 
                jnt = [],
                side = '', 
                spans = '', 
                shape = '', 
                valueCv = [], 
                sringsNoiseCtrl = True,
                stringsNoiseAxis= '',  
                newNoiseCtrl = True , 
                newNoiseAxis= '',  

                ): 

    ##=====================Generate Press Button Line==================================
    ##=====================Generate Press Button Line==================================
    ## Check Side for subRig
    if side:
        side = '%s' % side
    else:
        side = ''

    ## Create Control at cluster
    LineCtrl = subRig.Run(    name   = name, 
                                tmpJnt     = jnt[2] ,
                                parent     = '' ,
                                ctrlGrp    = '' ,
                                shape      = shape ,
                                side       = side ,
                                size       = 1 
                            ) 

    ## Check Side
    if side:
        side = '_%s_' % side
    else:
        side = '_'
    # print side, 'side'
    ## Create Group
    ctrlGrp = mc.group(n = '%sCtrl%sGrp' % (name, side), em = True)    
    jntGrp = mc.group(n = '%sJnt%sGrp' % (name, side), em = True)        
    stillGrp = mc.group(n = '%sStill%sGrp' % (name, side), em = True)   
    curveGrp = mc.group(n = '%sCrv%sGrp' % (name, side), em = True)   
    clsGrp = mc.group(n = '%sCls%sGrp' % (name, side), em = True)
    
    ## Create Curve From xform
    crvXform = []
    for each in jnt:                                
        jntXform = mc.xform(each, q=True, t=True)    
        crvXform.append(jntXform)
        
    ## Create Cluster on curve anc rebuild 
    curveXform = mc.curve (each,d = 1,n = ('%s%sCrv' % (name, side)) , p=[(crvXform[0][0],crvXform[0][1],crvXform[0][2]),(crvXform[2][0],crvXform[2][1],crvXform[2][2]),(crvXform[3][0],crvXform[3][1],crvXform[3][2])]) 
    print curveXform, '....curveXform..'
    mc.select('%s.cv[1]' % curveXform)
    clsObj = mc.cluster(n = ('%s%sCls' % (name, side)))   
    utaCore.reBuildCurve( lists = [curveXform], spans = int(spans), degree = 3)

    ## Lock and Hide Attribute 
    utaCore.lockAttr(listObj= ['%sSub%sCtrl' % (name, side)], attrs = ['rx', 'ry', 'rz', 'sx', 'sy', 'sz', 'radius'],lock = True,keyable = False )      

    # jnt[2]
    clsJntGrp = mc.group(n = '%sClsJnt%sGrp' % (name, side), em = True)  
    mc.delete(mc.parentConstraint (jnt[2], clsJntGrp))
    # mc.makeIdentity (clsJntGrp , apply = True)
    mc.parent(jnt[2], clsJntGrp)

    # mc.cluster (clsObj,weightedNode = ['test_ClsHandle', jnt[2]]
    # print clsObj, jnt[2], 'ken'
    mc.cluster( clsObj[-1],e = True, bs = 1, wn=(jnt[2], jnt[2]) )
    ## Create Connection 
    mc.select('%sSub%sCtrl' % (name, side), jnt[2])
    utaCore.connectTRS(trs = True, translate = False, rotate = False, scale = False)


    ## CleanUp 
    mc.delete('%sSub%sJnt' % (name, side))
    # print joint[1], clsGrp, '1'
    # mc.delete(mc.parentConstraint(each, newBendIK, mo = False))
    # mc.delete(mc.parentConstraint(jnt[1], clsGrp, mo = False))
    # mc.makeIdentity (clsGrp , apply = True , jointOrient = False, normal = 1, translate =True, rotate = True, scale = True) 
    # mc.makeIdentity (clsGrp , apply = True)

    mc.parent(clsObj, clsGrp)
    mc.parent(curveXform,curveGrp)
    mc.parent('%sSubCtrl%sGrp' % (name, side) , ctrlGrp)    
    mc.parent(curveGrp, clsGrp, stillGrp)
    # mc.delete(jnt[2])
    # print jnt[2], 'jnt[2]'

    if not newNoiseCtrl:
        crvNoiseGrp = mc.group(n = '%sNoise%sGrp' % (name, side), em = True)  
        mc.parent(jnt[0], jnt[1], jnt[-1], crvNoiseGrp)
        mc.parent(crvNoiseGrp, clsJntGrp, jntGrp)

    ## Lock and Hide Attribute 
    mc.setAttr('%s.v' % clsGrp , False)

    ##================================== Generate Noise Strings Line ==================================
    ##================================== Generate Noise Strings Line ==================================
    ## Create Noise For Strings Ctrl
    if sringsNoiseCtrl:
        ## Add Attribute Name
        # utaCore.addAttrCtrl (ctrl = '%sSub%sCtrl' % (name, side), elem = ['GeoVis'], min = 0, max = 1, at = 'long')
        utaCore.attrName(obj = '%sSub%sCtrl' % (name, side), elem = 'Frequency')
        utaCore.attrName(obj = '%sSub%sCtrl' % (name, side), elem = 'Value')

        ## Generate Noise Node
        creNoise = mc.shadingNode('noise', asTexture = True, n = ('%sStrings_Noi' % name ))
        # print creNoise, 'creNoise'
        mc.setAttr('%s.frequency' % creNoise, 0)
        time = 'time1'
        mc.connectAttr('%s.outTime' % time, '%s.time' % creNoise, f=True)
        creReMaValu = mc.shadingNode('remapValue', asUtility=True, n=('%sStrings%sRem' % (name, side)))
        mc.connectAttr( '%s.outColor' % creNoise, '%s.color[0].color_Color' % creReMaValu, f=True)

        multiNoise = mc.shadingNode('multiplyDivide', asUtility=True, n=('%sNoiseStrings%sMdv' % (name, side)))
        multiNoiseValue = mc.shadingNode('multiplyDivide', asUtility=True, n=('%sNoiseStringsValue%sMdv' % (name, side)))    
        mc.connectAttr( '%s.outColor' % creReMaValu , '%s.input1' % multiNoise , f=True)
        mc.connectAttr( '%s.output' % multiNoise , '%s.input1' % multiNoiseValue , f=True)
        mc.connectAttr( '%sSub%sCtrl.Frequency'  % (name, side) , '%s.input2X' % multiNoiseValue , f=True)
        utaCore.lockAttr(listObj= ['%sSubCtrlOfst%sGrp' % (name, side)], attrs = [stringsNoiseAxis],lock = False,keyable = True )      
        mc.connectAttr( '%s.outputX' % multiNoiseValue , '%sSubCtrlOfst%sGrp.%s' % (name , side, stringsNoiseAxis), f=True)

        ## Disconnect and Connect Attribute Axis
        plusMiNoise = mc.createNode( 'plusMinusAverage', n=('%sNoiseStrings%sPma' % (name, side)))
        mc.connectAttr('%s.outputX' % multiNoiseValue , '%s.input1D[0]' % plusMiNoise)
        mc.connectAttr('%sSub%sCtrl.%s' % (name , side,  stringsNoiseAxis) , '%s.input1D[1]' % plusMiNoise)
        mc.connectAttr('%s.output1D' % plusMiNoise , '%s.%s' % (jnt[2], stringsNoiseAxis), f = True)

        ## Hide Attribute 
        # mc.setAttr('%s.v' % clsGrp , False)
        # mc.setAttr('%s.v' % curveNoiseGrp, False)
        ## Clean Up
        mc.delete(clsGrp)

    ##================================== Generate Strum Button Line ==================================
    ##================================== Generate Strum Button Line ==================================
    if newNoiseCtrl:
        ## Check Side for subRig
        if side == '_':
            side = ''
        elif side == '_L_'or '_R_' :
            sides = side.split('_')
            side = side[1]
        else:
            side = side
        ## Create Control For Noise
        LineNoiseCtrl = subRig.Run(    name   = '%sNoise' % name, 
                                    tmpJnt     = jnt[1] ,
                                    parent     = '' ,
                                    ctrlGrp    = '' ,
                                    shape      = shape ,
                                    side       = side ,
                                    size       = 1 
                                )  

        ## Check Side
        if side:
            side = '_%s_' % side
        else:
            side = '_'
        # print side, 'side'

        ## Create curve anc rebuild for Noise
        curveNoiseGrp = mc.group(n = '%sNoiseCrv%sGrp' % (name, side), em = True)   
        curveNoiseXform = mc.curve (each,d = 1,n = ('%sNoise%sCrv' % (name, side)) , p=[(crvXform[0][0],crvXform[0][1],crvXform[0][2]),(crvXform[3][0],crvXform[3][1],crvXform[3][2])])     
        utaCore.reBuildCurve( lists = [curveNoiseXform], spans = 6, degree = 3)

      

        ## Lock and Hide Attribute 
        utaCore.lockAttr(listObj= ['%sNoiseSub%sCtrl' % (name, side)], attrs = ['sx', 'sy', 'sz', 'radius'],lock = True,keyable = False )      

        ## Add Attribute Name
        utaCore.addAttrCtrl (ctrl = '%sNoiseSub%sCtrl' % (name, side), elem = ['GeoVis'], min = 0, max = 1, at = 'long')
        utaCore.attrName(obj = '%sNoiseSub%sCtrl' % (name, side), elem = 'Frequency')
        utaCore.attrName(obj = '%sNoiseSub%sCtrl' % (name, side), elem = 'Value')

        crvNoiseGrp = mc.group(n = '%sNoise%sGrp' % (name, side), em = True)  
        mc.delete('%sNoiseSub%sJnt_parentConstraint1' % (name, side), '%sNoiseSub%sJnt_scaleConstraint1' % (name, side))
        mc.delete(mc.parentConstraint('%sNoiseSub%sJnt' % (name, side), crvNoiseGrp))
        mc.parent(jnt[0], jnt[1], jnt[-1], crvNoiseGrp)
        mc.parent(crvNoiseGrp, clsJntGrp, jntGrp)

        ## Create Connection 
        mc.select('%sNoiseSub%sCtrl' % (name, side), jnt[1])
        utaCore.connectTRS(trs = True, translate = False, rotate = False, scale = False)
           
        ## Bind Skin
        jntNoise = (jnt[0], jnt[1], jnt[3])
        mc.skinCluster( '%sNoise%sCrv' % (name, side) , jntNoise , tsb=True , n = '%sSkinCluster%sBind' % (name, side))

        ## Set CV SkinCluster
        mc.select('%sNoise%sCrv.cv[0]' % (name, side))

        ## CleanUp 
        mc.delete('%sNoiseSub%sJnt' % (name, side))
        mc.parent(curveNoiseXform,curveNoiseGrp)
        mc.parent('%sNoiseCrv%sGrp' % (name, side), stillGrp)
        mc.parent('%sNoiseSubCtrl%sGrp' % (name, side), ctrlGrp)

        #Wire
        mc.wire( curveXform ,w = curveNoiseXform, n = '%sWire%sWire' % (name, side))
        mc.setAttr ("%sWire%sWire.dropoffDistance[0]" % (name, side),  1000)

        # valueCv = [1, 0.5, 1, 0.78, 0.48, 0.25, 0.05, 1, 1]

        for i in range(len(valueCv)):
            jntNum = 1
            # if i == 0:
            #     jntNum = 0
            # elif i == 7 or i == 8:
            #     jntNum = 3

            mc.skinPercent( '%sSkinCluster%sBind' % (name, side), '%sNoise%sCrv.cv[%s]' % (name, side,i), transformValue=[(jnt[jntNum], valueCv[i]*0.01)])
            # print valueCv[i], 'i'

        ## MEL TO Python
        ## Generate Noise Node
        creNoise = mc.shadingNode('noise', asTexture = True, n = ('%s_Noi' % name ))
        # print creNoise, 'creNoise'
        mc.setAttr('%s.frequency' % creNoise, 0)
        time = 'time1'
        mc.connectAttr('%s.outTime' % time, '%s.time' % creNoise, f=True)
        creReMaValu = mc.shadingNode('remapValue', asUtility=True, n=('%s%sRem' % (name, side)))
        mc.connectAttr( '%s.outColor' % creNoise, '%s.color[0].color_Color' % creReMaValu, f=True)

        multiNoise = mc.shadingNode('multiplyDivide', asUtility=True, n=('%sNoise%sMdv' % (name, side)))
        multiNoiseValue = mc.shadingNode('multiplyDivide', asUtility=True, n=('%sNoiseValue%sMdv' % (name, side)))    
        mc.connectAttr( '%s.outColor' % creReMaValu , '%s.input1' % multiNoise , f=True)
        mc.connectAttr( '%s.output' % multiNoise , '%s.input1' % multiNoiseValue , f=True)
        mc.connectAttr( '%sNoiseSub%sCtrl.Frequency'  % (name, side) , '%s.input2X' % multiNoiseValue , f=True)
        utaCore.lockAttr(listObj= ['%sNoiseSubCtrlOfst%sGrp' % (name, side)], attrs = [newNoiseAxis],lock = False,keyable = True )      
        mc.connectAttr( '%s.outputX' % multiNoiseValue , '%sNoiseSubCtrlOfst%sGrp.%s' % (name , side, newNoiseAxis), f=True)

        ## Disconnect and Connect Attribute Axis
        plusMiNoise = mc.createNode( 'plusMinusAverage', n=('%sNoise%sPma' % (name, side)))
        mc.connectAttr('%s.outputX' % multiNoiseValue , '%s.input1D[0]' % plusMiNoise)
        mc.connectAttr('%sNoiseSub%sCtrl.%s' % (name , side,  newNoiseAxis) , '%s.input1D[1]' % plusMiNoise)
        mc.connectAttr('%s.output1D' % plusMiNoise , '%s.%s' % (jnt[1], newNoiseAxis), f = True)

        ## Hide Attribute 
        mc.setAttr('%s.v' % clsGrp , False)
        mc.setAttr('%s.v' % curveNoiseGrp, False)



    print '================================== Generate is DONE =================================='