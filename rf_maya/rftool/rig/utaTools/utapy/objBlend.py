import maya.cmds as mc

def objBlend(objBlend=[], elem = '', side = '',valueDefault= 0.1,*args):
    objBlend1 = objBlend[0]
    objBlend2 = objBlend[1]
    #Parent Constraint 2 Obj
    mc.parentConstraint(objBlend1,objBlend2, '%sPntCtrl_%s_Grp' % (elem, side), mo=True)
    mc.setAttr ('%sPntCtrl_%s_Grp_parentConstraint1.interpType' % (elem, side), 2)

    #Add Atrribute FkPosition
    mc.addAttr('%sPnt_%s_Ctrl' % (elem , side),ln='%s_______' % elem, k=True)
    mc.setAttr('%sPnt_%s_Ctrl.%s_______' % (elem , side, elem),lock = 1)
    mc.addAttr('%sPnt_%s_Ctrl' % (elem , side),ln='Blend',min = 0,max = 1,dv = valueDefault,k=True)
    # Connect Attr Wing
    mc.connectAttr('%sPnt_%s_Ctrl.Blend' % (elem ,side), '%sPntCtrl_%s_Grp_parentConstraint1.%sW0' % (elem ,side, objBlend1))
    wingPma = mc.createNode ('plusMinusAverage', n = '%s_%s_Pma' % (elem, side))
    # print wingPma
    mc.connectAttr('%sPnt_%s_Ctrl.Blend' % (elem, side), '%s.input2D[0].input2Dx' % wingPma)
    mc.setAttr('%s.input2D[1].input2Dx' % wingPma, -1.0)
    wingMdv = mc.createNode('multiplyDivide', n = '%s_%s_Mdv' % (elem, side))
    mc.connectAttr('%s.output2Dx' % wingPma, '%s.input1X' % wingMdv)
    mc.setAttr('%s.input2X' % wingMdv, -1)
    mc.connectAttr('%s.outputX' % wingMdv, '%sPntCtrl_%s_Grp_parentConstraint1.%sW1' % (elem, side, objBlend2))
