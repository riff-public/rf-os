#------------------------copyShape----------------------------------------------
import sys
import maya.cmds as mc
import maya.mel as mel
from lpRig import applyLeftShape as sides
reload(sides)
import pymel.core as pm 
import maya.mel as mm 

lft = 1
rgt = -1

#list All Obj
bsh1 = ("upLipsCurlOut", 	"BodyMthRig_Geo_LipUpRollOutMthRig_Bsh" )
bsh2 = ("upLipsCurlIn", 	"BodyMthRig_Geo_LipUpRollInMthRig_Bsh" )
bsh3 = ("loLipsCurlOut", 	"BodyMthRig_Geo_LipLowRollOutMthRig_Bsh" )
bsh4 = ("loLipsCurlIn", 	"BodyMthRig_Geo_LipLowRollInMthRig_Bsh" )
bsh5 = ("mouthU", 			"BodyMthRig_Geo_UMthRig_Bsh" )
bsh6 = ("JawOpenMth", 		"BodyMthRig_Geo_JawOpenMthRig_Bsh" )

bsh26 = ("LipUpPuffMthRigShapes", 		"BodyMthRig_Geo_LipUpPuffMthRig_Bsh" )
bsh27 = ("LipLowPuffMthRigShapes", 		"BodyMthRig_Geo_LipLowPuffMthRig_Bsh" )
bsh28 = ("LipUpThickMthRigShapes", 		"BodyMthRig_Geo_LipUpThickMthRig_Bsh" )
bsh29 = ("LipLowThickMthRigShapes", 		"BodyMthRig_Geo_LipLowThickMthRig_Bsh" )

bsh7 = ("cornerOut", 		"BodyMthRig_Geo_CnrOutMthRig_L_Bsh" )
bsh8 = ("cornerIn", 		"BodyMthRig_Geo_CnrInMthRig_L_Bsh" )
bsh9 = ("cornerInnerUp", 	"BodyMthRig_Geo_CnrInUpMthRig_L_Bsh" )
bsh10 = ("cornerUp", 		"BodyMthRig_Geo_CnrUpMthRig_L_Bsh" )
bsh11 = ("cornerOuterUp", 	"BodyMthRig_Geo_CnrOutUpMthRig_L_Bsh" )
bsh12 = ("cornerInnerDn", 	"BodyMthRig_Geo_CnrInDnMthRig_L_Bsh" )
bsh13 = ("cornerDn", 		"BodyMthRig_Geo_CnrDnMthRig_L_Bsh" )
bsh14 = ("cornerOuterDn", 	"BodyMthRig_Geo_CnrOutDnMthRig_L_Bsh" )
bsh15 = ("CnrDnOpen", 		"BodyMthRig_Geo_CnrDnOpenMthRig_L_Bsh" )
bsh16 = ("JawOpen", 		"BodyMthRig_Geo_CnrOpenMthRig_L_Bsh" )
bsh17 = ("CnrUpOpen", 		"BodyMthRig_Geo_CnrUpOpenMthRig_L_Bsh" )
bsh18 = ("CnrInDnOpen", 	"BodyMthRig_Geo_CnrInDnOpenMthRig_L_Bsh" )
bsh19 = ("CnrInOpen", 		"BodyMthRig_Geo_CnrInOpenMthRig_L_Bsh" )
bsh20 = ("CnrInUpOpen", 	"BodyMthRig_Geo_CnrInUpOpenMthRig_L_Bsh" )
bsh21 = ("CnrOutDnOpen", 	"BodyMthRig_Geo_CnrOutDnOpenMthRig_L_Bsh" )
bsh22 = ("CnrOutOpen", 		"BodyMthRig_Geo_CnrOutOpenMthRig_L_Bsh" )
bsh23 = ("CnrOutUpOpen", 	"BodyMthRig_Geo_CnrOutUpOpenMthRig_L_Bsh" )
bsh24 = ("puffUpperUp", 	"BodyMthRig_Geo_CheekMthRig_L_Bsh" )
bsh25 = ("puffLowerOut", 	"BodyMthRig_Geo_PuffMthRig_L_Bsh" )
# mirrot
bsh7_mir = ("cornerOut",        "BodyMthRig_Geo_CnrOutMthRig_R_Bsh")
bsh8_mir = ("cornerIn", 	    "BodyMthRig_Geo_CnrInMthRig_R_Bsh" )
bsh9_mir = ("cornerInnerUp",    "BodyMthRig_Geo_CnrInUpMthRig_R_Bsh" )
bsh10_mir = ("cornerUp", 		"BodyMthRig_Geo_CnrUpMthRig_R_Bsh" )
bsh11_mir = ("cornerOuterUp", 	"BodyMthRig_Geo_CnrOutUpMthRig_R_Bsh" )
bsh12_mir = ("cornerInnerDn", 	"BodyMthRig_Geo_CnrInDnMthRig_R_Bsh" )
bsh13_mir = ("cornerDn", 		"BodyMthRig_Geo_CnrDnMthRig_R_Bsh" )
bsh14_mir = ("cornerOuterDn", 	"BodyMthRig_Geo_CnrOutDnMthRig_R_Bsh" )
bsh15_mir = ("CnrDnOpen", 		"BodyMthRig_Geo_CnrDnOpenMthRig_R_Bsh" )
bsh16_mir = ("JawOpen", 		"BodyMthRig_Geo_CnrOpenMthRig_R_Bsh" )
bsh17_mir = ("CnrUpOpen", 		"BodyMthRig_Geo_CnrUpOpenMthRig_R_Bsh" )
bsh18_mir = ("CnrInDnOpen", 	"BodyMthRig_Geo_CnrInDnOpenMthRig_R_Bsh" )
bsh19_mir = ("CnrInOpen", 		"BodyMthRig_Geo_CnrInOpenMthRig_R_Bsh" )
bsh20_mir = ("CnrInUpOpen", 	"BodyMthRig_Geo_CnrInUpOpenMthRig_R_Bsh" )
bsh21_mir = ("CnrOutDnOpen", 	"BodyMthRig_Geo_CnrOutDnOpenMthRig_R_Bsh" )
bsh22_mir = ("CnrOutOpen", 		"BodyMthRig_Geo_CnrOutOpenMthRig_R_Bsh" )
bsh23_mir = ("CnrOutUpOpen", 	"BodyMthRig_Geo_CnrOutUpOpenMthRig_R_Bsh" )
bsh24_mir = ("puffUpperUp", 	"BodyMthRig_Geo_CheekMthRig_R_Bsh" )
bsh25_mir = ("puffLowerOut", 	"BodyMthRig_Geo_PuffMthRig_R_Bsh" )

listsBsh1to6 = (bsh1, bsh2, bsh3, bsh4, bsh5, bsh6, bsh26, bsh27, bsh28, bsh29)
listsBsh7to25LFT = (bsh7, bsh8, bsh9, bsh10, bsh11, bsh12, bsh13, bsh14, bsh15, bsh16, bsh17, bsh18, bsh19, bsh20, bsh21, bsh22, bsh23, bsh24, bsh25)
listsBsh7to25RGT = (bsh7_mir, bsh8_mir, bsh9_mir, bsh10_mir, bsh11_mir, bsh12_mir, bsh13_mir, bsh14_mir, bsh15_mir, bsh16_mir, bsh17_mir, bsh18_mir, bsh19_mir, bsh20_mir, bsh21_mir, bsh22_mir, bsh23_mir, bsh24_mir, bsh25_mir)
def copyMthShapeToBsh ():
	# Bsh 
	for each in listsBsh1to6: 
		print each, 'ooo'
		obj = mc.select(each)
		mm.eval('source "O:/Pipeline/core/rf_maya/rftool/rig/utaTools/utamel/utaControl.mel"')
		mel.eval('copyShape;')
		print 'CopyShape %s to %s is Done!!' % (str(each[0]),str(each[1]))
	print "_"*100
def copyMthShapeToBshLFT (value, *args):
	# Bsh lft
	for each in listsBsh7to25LFT:
		obj = mc.select(each)
		sides.main(float(value),lft) 
		print 'CopyShape %s to %s is Done!!' % (str(each[0]),str(each[1]))
	print "_"*100
def copyMthShapeToBshRGT (value, *args):
	# Bsh rgt
	for each in listsBsh7to25RGT:
		obj = mc.select(each)
		sides.main(float(value),rgt) 
		print 'CopyShape %s to %s is Done!!' % (str(each[0]),str(each[1]))
	print "_"*100
def copyShapeToBase (geoBase, lists=[]):
	listsAll = len(lists)
	if listsAll != 0:
		for each in lists:
			pm.delete ( each , ch = True ) ;
			obj = mc.select(geoBase,each)
			mel.eval('copyShape;')
	else:
		print "Please Select Object >= 1"






