

from maya import cmds as mc

def setDefaultAttribute():
	grp = 'Char_Grp'
	attrs = ['sx', 'sy', 'sz']
	ctrlAll = []
	value = 1
	charGrp = mc.listRelatives(grp, children = True)

	for obj in charGrp:
		shortName = obj.split("|")[-1]
		children = mc.listRelatives(shortName, fullPath=True, ad = True)
		for each in children:
			childName = each.split("|")[-1]
			if not 'Shape' in childName:
				if '_Ctrl' in childName:
					for i in attrs:
						attrNode = mc.getAttr('%s.%s' % (childName, i), k = True, cb =True, l = False)
						if attrNode:
							attrNode = mc.listAttr('%s.%s' % (each, i ), k = True, v = True, l = False, cb = True)
							ctrlObj = each.split("|")[-1]
							if not 'UprRoot' in ctrlObj:
								try:
									mc.setAttr('%s.%s' % (ctrlObj, i), value)
								except:
									pass

	print '>> D O N E <<'