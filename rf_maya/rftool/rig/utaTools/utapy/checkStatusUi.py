import maya.cmds as mc 
import pymel.core as pm 
# reload(mc)
# reload(pm)
def checkStatusTmpRig():
	# Check Status Button
    Hdlists= mc.ls("*:NeckHdDfmRigTmp_Loc")
    EyeLists = mc.ls("*:CntLidRigTmp_L_Loc")
    EarLists = mc.ls("*:EarTmpJnt_Grp")
    NsLists = mc.ls("*:NsBdgTmp_Loc")
    TtLists = mc.ls("*:JawPostTmp_Loc")
    EbLists = mc.ls("*:AllEbRigTmpPos_L_Loc")
    MthLists = mc.ls("*:LipUpMidMthRigTmp_Loc")
    DtlLists = mc.ls("*:NsDtlRig_L_Loc")

    HdCtrlList = mc.ls("HdMidHdDfmRig_Ctrl", "*:HdMidHdDfmRig_Ctrl")
    EyeCtrlList = mc.ls("CntEyeRig_L_Ctrl", "*:CntEyeRig_L_Ctrl")
    EarCtrlList = mc.ls("EarRig_1_L_Ctrl", "*:EarRig_1_L_Ctrl")
    NsCtrlList = mc.ls("NsNsRig_Ctrl", "*:NsNsRig_Ctrl")
    TtCtrlList = mc.ls("MainUpTthRig_Ctrl", "*:MainUpTthRig_Ctrl")
    EbCtrlList = mc.ls("AllEbRig_L_Ctrl", "*:AllEbRig_L_Ctrl")
    MthCtrlList = mc.ls("JawMthRig_Ctrl", "*:JawMthRig_Ctrl")
    DtlCtrlList = mc.ls("EyeSktADtlRig_L_Ctrl", "*:EyeSktADtlRig_L_Ctrl")

    for each in Hdlists:
        if mc.objExists(each):
            # print "YES"
            pm.button("HdRigTmpLocButton", e=True, bgc= (0.498039, 1, 0))
            pm.button("RemoveHdRigTmpLocButton", e=True, bgc= (1, 0.0784314, 0.576471))
    for each in EyeLists:
        if mc.objExists(each):
            # print "YES"
            pm.button("EyeRigTmpLocButton", e=True, bgc= (0.498039, 1, 0))
            pm.button("RemoveEyeRigTmpLocButton", e=True, bgc= (1, 0.0784314, 0.576471))
    for each in EarLists:
        if mc.objExists(each):
            # print "YES"
            pm.button("EarRigTmpLocButton", e=True, bgc= (0.498039, 1, 0))
            pm.button("RemoveEarRigTmpLocButton", e=True, bgc= (1, 0.0784314, 0.576471))
    for each in NsLists:
        if mc.objExists(each):
            # print "YES"
            pm.button("NsRigTmpLocButton", e=True, bgc= (0.498039, 1, 0))
            pm.button("RemoveNsRigTmpLocButton", e=True, bgc= (1, 0.0784314, 0.576471))
    for each in TtLists:
        if mc.objExists(each):
            # print "YES"
            pm.button("TtRigTmpLocButton", e=True, bgc= (0.498039, 1, 0))
            pm.button("RemoveTtRigTmpLocButton", e=True, bgc= (1, 0.0784314, 0.576471))
    for each in EbLists:
        if mc.objExists(each):
            # print "YES"
            pm.button("EbRigTmpLocButton", e=True, bgc= (0.498039, 1, 0))
            pm.button("RemoveEbRigTmpLocButton", e=True, bgc= (1, 0.0784314, 0.576471))
    for each in MthLists:
        if mc.objExists(each):
            # print "YES"
            pm.button("MthRigTmpLocButton", e=True, bgc= (0.498039, 1, 0))
            pm.button("RemoveMthRigTmpLocButton", e=True, bgc= (1, 0.0784314, 0.576471))
    for each in DtlLists:
        if mc.objExists(each):
            # print "YES"
            pm.button("DtlRigTmpLocButton", e=True, bgc= (0.498039, 1, 0))
            pm.button("RemoveDtlRigTmpLocButton", e=True, bgc= (1, 0.0784314, 0.576471))

    for each in HdCtrlList:
        if mc.objExists(each):
            # print "YES"
            pm.button("HdRigButton", e=True, bgc= (0.498039, 1, 0))
    for each in EyeCtrlList:
        if mc.objExists(each):
            # print "YES"
            pm.button("EyeRigButton", e=True, bgc= (0.498039, 1, 0))
    for each in EarCtrlList:
        if mc.objExists(each):
            # print "YES"
            pm.button("EarRigButton", e=True, bgc= (0.498039, 1, 0))
    for each in NsCtrlList:
        if mc.objExists(each):
            # print "YES"
            pm.button("NsRigButton", e=True, bgc= (0.498039, 1, 0))
    for each in TtCtrlList:
        if mc.objExists(each):
            # print "YES"
            pm.button("TtRigButton", e=True, bgc= (0.498039, 1, 0))    
    for each in EbCtrlList:
        if mc.objExists(each):
            # print "YES"
            pm.button("EbRigButton", e=True, bgc= (0.498039, 1, 0))
    for each in MthCtrlList:
        if mc.objExists(each):
            # print "YES"
            pm.button("MthRigButton", e=True, bgc= (0.498039, 1, 0))
    for each in DtlCtrlList:
        if mc.objExists(each):
            # print "YES"
            pm.button("DtlRigButton", e=True, bgc= (0.498039, 1, 0))    

    tmpJnt = mc.ls("*:Root_Jnt", "*:Root_Jnt")
    for each in tmpJnt:
        if mc.objExists(each):
            # print "YES"
            pm.button("templateJntButton", e=True, bgc= (0.498039, 1, 0)) 

	# pm.button("templateJntButton", e=True, bgc= (0.411765, 0.411765, 0.411765)) 
	pm.button("dataScripButton", e=True, bgc= (0.411765, 0.411765, 0.411765))
	pm.button("runMainRigButton", e=True, bgc= (0.411765, 0.411765, 0.411765))
def checkStatusHdGeoRig():
	# Check Status Button
	# Check Status Button
    HdGeolists= mc.ls("eyebrowBsh_lft_grp")
    MthCtrlLists = mc.ls("mouthBsh_ctrl")
    ConnectCtrlList = mc.ls("mouthUD_mdv")
    for each in HdGeolists:
        if mc.objExists(each):
            # print "YES"
            pm.button("DupBshButton", e=True, bgc= (0.498039, 1, 0))

    for each in MthCtrlLists:
        if mc.objExists(each):
            # print "YES"
            pm.button("addFacialCtrlAttr2_1", e=True, bgc= (0.498039, 1, 0))
    for each in ConnectCtrlList:
    	if mc.objExists(each):
    		pm.button("connectBshButton", e=True, bgc= (0.498039, 1, 0))

def checkStatusEbRig():
	EbJntTmpList = mc.ls("hd_jnt", "*:hd_jnt")
	EbCreateBshList = mc.ls("EbRigBsh_Grp")

	for each in EbJntTmpList:
		if mc.objExists(each):
		# print "YES"
			pm.button("ebJntTmpButton", e=True, bgc= (0.498039, 1, 0))
	for each in EbCreateBshList:
		if mc.objExists(each):
		# print "YES"
			pm.button("EbCreateBshButton", e=True, bgc= (0.498039, 1, 0))

def checkStatusMthRig():
	MthBshList = mc.ls("BodyMthRig_Geo_LipUpPuffMthRig_Bsh")
	proxyBshMth = mc.ls("puffUpperUp")
	ProxyCrtMthBshList = mc.ls("JawOpenMth")
	BodyMthJawOrig = mc.ls("BodyMthJawRig_GeoShapeOrig")
	RefBodyBshRig = mc.ls("*:BodyBshRig_Geo")

	for each in MthBshList:
		if mc.objExists(each):
		# print "YES"
			pm.button("CreateMthBshButton", e=True, bgc= (0.498039, 1, 0))
	for each in proxyBshMth:
		if mc.objExists(each):
		# print "YES"
			pm.button("DuplicateProxyMthBshButton", e=True, bgc= (0.498039, 1, 0))
	for each in ProxyCrtMthBshList:
		if mc.objExists(each):
		# print "YES"
			pm.button("DuplicateCorrecMthShapeButton", e=True, bgc= (0.498039, 1, 0))
	for each in BodyMthJawOrig:
		if mc.objExists(each):
			pm.button("addBSHJLMthButton", e=True, bgc= (0.498039, 1, 0))
	for each in RefBodyBshRig:
		if mc.objExists(each):
			pm.button("refBshRigButton", e=True, bgc= (0.498039, 1, 0))
def checkStatusFCRig():
	# list Create MainGroup
	MainGroupList = mc.ls("FclRigCtrl_Grp")
	BodyFclRigPostList = mc.ls("BodyFclRigPost_Geo")
	# list REF
	HdRigList = mc.ls("*:HdDfmRigCtrl_Grp")
	EyeRigList = mc.ls("*:EyeRigCtrl_Grp")
	EbRigList = mc.ls("*:EbRigCtrl_Grp")
	MthRigList = mc.ls("*:MthRigCtrl_Grp")
	TtRigList = mc.ls("*:TtRigCtrl_Grp")
	NsRigList = mc.ls("*:NsRigCtrl_Grp")
	EarRigList = mc.ls("*:EarRigCtrl_Grp")
	DtlRigLIst = mc.ls("*:DtlRigSkin_Grp")
	BshRigLIst = mc.ls("*:BshRigStill_Grp")
	# Check List Create MainGroup
	for each in MainGroupList:
		if mc.objExists(each):
		# print "YES"
			pm.button("cteateMainGroupButton", e=True, bgc= (0.498039, 1, 0))
	for each in BodyFclRigPostList:
		if mc.objExists(each):
		# print "YES"
			pm.button("CreateBodyFclRigPos_GeoButton", e=True, bgc= (0.498039, 1, 0))
	# Check List REF
	for each in HdRigList:
		if mc.objExists(each):
		# print "YES"
			pm.button("refHdRigButton", e=True, bgc= (0.498039, 1, 0))
	for each in EyeRigList:
		if mc.objExists(each):
		# print "YES"
			pm.button("refEyeRigButton", e=True, bgc= (0.498039, 1, 0))
	for each in EbRigList:
		if mc.objExists(each):
		# print "YES"
			pm.button("refEbRigButton", e=True, bgc= (0.498039, 1, 0))
	for each in MthRigList:
		if mc.objExists(each):
		# print "YES"
			pm.button("refMthRigButton", e=True, bgc= (0.498039, 1, 0))
	for each in TtRigList:
		if mc.objExists(each):
		# print "YES"
			pm.button("refTtRigButton", e=True, bgc= (0.498039, 1, 0))
	for each in NsRigList:
		if mc.objExists(each):
		# print "YES"
			pm.button("refNsRigButton", e=True, bgc= (0.498039, 1, 0))
	for each in EarRigList:
		if mc.objExists(each):
		# print "YES"
			pm.button("refEarRigButton", e=True, bgc= (0.498039, 1, 0))
	for each in DtlRigLIst:
		if mc.objExists(each):
		# print "YES"
			pm.button("refDtlRigButton", e=True, bgc= (0.498039, 1, 0))
	for each in BshRigLIst:
		if mc.objExists(each):
		# print "YES"
			pm.button("refBshRigButton", e=True, bgc= (0.498039, 1, 0))


	# list  7REF
	Hd7RigList = mc.ls("*:HdDfmRigCtrl_Grp")
	Eye7RigList = mc.ls("*:EyeRigCtrl_Grp")
	Eb7RigList = mc.ls("*:EbRigCtrl_Grp")
	Mth7RigList = mc.ls("*:MthRigStill_Grp")
	Tt7RigList = mc.ls("*:TtRigCtrl_Grp")
	Ns7RigList = mc.ls("*:NsRigCtrl_Grp")
	Ear7RigList = mc.ls("*:EarRigCtrl_Grp")
	Dtl7RigLIst = mc.ls("*:DtlRigCtrlHi_Grp")
	Bsh7RigLIst = mc.ls("*:BshRigCtrl_Grp")

	# Check List REF
	for each in Hd7RigList:
		if mc.objExists(each):
		# print "YES"
			pm.button("refHd7RigButton", e=True, bgc= (0.498039, 1, 0))
	for each in Eye7RigList:
		if mc.objExists(each):
		# print "YES"
			pm.button("refEye7RigButton", e=True, bgc= (0.498039, 1, 0))
	for each in Eb7RigList:
		if mc.objExists(each):
		# print "YES"
			pm.button("refEb7RigButton", e=True, bgc= (0.498039, 1, 0))
	for each in Mth7RigList:
		if mc.objExists(each):
		# print "YES"
			pm.button("refMth7RigButton", e=True, bgc= (0.498039, 1, 0))
	for each in Tt7RigList:
		if mc.objExists(each):
		# print "YES"
			pm.button("refTt7RigButton", e=True, bgc= (0.498039, 1, 0))
	for each in Ns7RigList:
		if mc.objExists(each):
		# print "YES"
			pm.button("refNs7RigButton", e=True, bgc= (0.498039, 1, 0))
	for each in Ear7RigList:
		if mc.objExists(each):
		# print "YES"
			pm.button("refEar7RigButton", e=True, bgc= (0.498039, 1, 0))
	for each in Dtl7RigLIst:
		if mc.objExists(each):
		# print "YES"
			pm.button("refDtl7RigButton", e=True, bgc= (0.498039, 1, 0))
	for each in Bsh7RigLIst:
		if mc.objExists(each):
		# print "YES"
			pm.button("refBsh7RigButton", e=True, bgc= (0.498039, 1, 0))

	# List Parent REF
	HdRigParList = mc.ls("FclRigCtrl_Grp|*:HdDfmRigCtrl_Grp")
	EyeRigParList = mc.ls("HdHiFclRigSpc_Grp|*:EyeRigCtrl_Grp")
	EbRigParList = mc.ls("HdHiFclRigSpc_Grp|*:EbRigCtrl_Grp")
	MthRigParList = mc.ls("HdLowFclRigSpc_Grp|*:MthRigCtrl_Grp")
	TtRigParList = mc.ls("FclRigStill_Grp|*:TtRigStill_Grp")
	NsRigParList = mc.ls("HdMidFclRigSpc_Grp|*:NsRigCtrl_Grp")
	EarRigParList = mc.ls("HdHiFclRigSpc_Grp|*:EarRigCtrl_Grp")
	DtlRigParList = mc.ls("FclRigStill_Grp|*:DtlRigStill_Grp")
	BshRigParList = mc.ls("FclRigStill_Grp|*:BshRigStill_Grp")

	
	# Check List Parent REF
	for each in HdRigParList:
		if mc.objExists(each):
		# print "YES"
			pm.button("conHdRigButton", e=True, bgc= (0.498039, 1, 0))
	for each in EyeRigParList:
		if mc.objExists(each):
		# print "YES"
			pm.button("conEyeRigButton", e=True, bgc= (0.498039, 1, 0))
	for each in EbRigParList:
		if mc.objExists(each):
		# print "YES"
			pm.button("conEbRigButton", e=True, bgc= (0.498039, 1, 0))
	for each in MthRigParList:
		if mc.objExists(each):
		# print "YES"
			pm.button("conMthRigButton", e=True, bgc= (0.498039, 1, 0))
	for each in TtRigParList:
		if mc.objExists(each):
		# print "YES"
			pm.button("conTtRigButton", e=True, bgc= (0.498039, 1, 0))
	for each in NsRigParList:
		if mc.objExists(each):
		# print "YES"
			pm.button("conNsRigButton", e=True, bgc= (0.498039, 1, 0))
	for each in EarRigParList:
		if mc.objExists(each):
		# print "YES"
			pm.button("conEarRigButton", e=True, bgc= (0.498039, 1, 0))
	for each in DtlRigParList:
		if mc.objExists(each):
		# print "YES"
			pm.button("conDtlRigButton", e=True, bgc= (0.498039, 1, 0))
	for each in BshRigParList:
		if mc.objExists(each):
		# print "YES"
			pm.button("conBshRigButton", e=True, bgc= (0.498039, 1, 0))



# List 7 Parent REF
	Hd7RigParList = mc.ls("FclRigCtrl_Grp|*:HdDfmRigCtrl_Grp")
	Eye7RigParList = mc.ls("HdHiFclRigSpc_Grp|*:EyeRigCtrl_Grp")
	Eb7RigParList = mc.ls("HdHiFclRigSpc_Grp|*:EbRigCtrl_Grp")
	Mth7RigParList = mc.ls("|FclRigStill_Grp|mthRig:MthRigStill_Grp|mthRig:mthJawLwr1_Loc|mthRig:mthJawLwr1_LocShape")
	Tt7RigParList = mc.ls("|FclRigStill_Grp|ttRig:TtRigStill_Grp|ttRig:StillUpTthRig_Grp|ttRig:RbnUpTthRig_Nrb")
	Ns7RigParList = mc.ls("HdMidFclRigSpc_Grp|*:NsRigCtrl_Grp")
	Ear7RigParList = mc.ls("HdHiFclRigSpc_Grp|*:EarRigCtrl_Grp")
	Dtl7RigParList = mc.ls("|FclRigStill_Grp|dtlRig:DtlRigStill_Grp")
	Bsh7RigParList = mc.ls("|FclRigStill_Grp|bsh:BshRigStill_Grp|bsh:FacialBshGeo_Grp")


	# Check 7 List Parent REF
	for each in Hd7RigParList:
		if mc.objExists(each):
		# print "YES"
			pm.button("con7HdRigButton", e=True, bgc= (0.498039, 1, 0))
	for each in Eye7RigParList:
		if mc.objExists(each):
		# print "YES"
			pm.button("con7EyeRigButton", e=True, bgc= (0.498039, 1, 0))
	for each in Eb7RigParList:
		if mc.objExists(each):
		# print "YES"
			pm.button("con7EbRigButton", e=True, bgc= (0.498039, 1, 0))
	for each in Mth7RigParList:
		if mc.objExists(each):
		# print "YES"
			pm.button("con7MthRigButton", e=True, bgc= (0.498039, 1, 0))
	for each in Tt7RigParList:
		if mc.objExists(each):
		# print "YES"
			pm.button("con7TtRigButton", e=True, bgc= (0.498039, 1, 0))
	for each in Ns7RigParList:
		if mc.objExists(each):
		# print "YES"
			pm.button("con7NsRigButton", e=True, bgc= (0.498039, 1, 0))
	for each in Ear7RigParList:
		if mc.objExists(each):
		# print "YES"
			pm.button("con7EarRigButton", e=True, bgc= (0.498039, 1, 0))
	for each in Dtl7RigParList:
		if mc.objExists(each):
		# print "YES"
			pm.button("con7DtlRigButton", e=True, bgc= (0.498039, 1, 0))
	for each in Bsh7RigParList:
		if mc.objExists(each):
		# print "YES"
			pm.button("con7BshRigButton", e=True, bgc= (0.498039, 1, 0))