import maya.cmds as mc
reload(mc)
def discon():

    try:
    # connect EyeLid LFT
        mc.disconnectAttr('*:eye_lft_ctrl.lidFollow','*:eyeLid_lft_mdv.input2Y')
        mc.disconnectAttr('*:eye_lft_ctrl.lidFollow','*:eyeLid_lft_mdv.input2X')
        mc.disconnectAttr('*:eye_lft_ctrl.lidFollow','*:eyeLid_lft_mdv.input2Z')
        mc.setAttr('*:eyeLid_lft_mdv.input2X',0)
        mc.setAttr('*:eyeLid_lft_mdv.input2Y',0)
        mc.setAttr('*:eyeLid_lft_mdv.input2Z',0)

        # mc.disconnectAttr('*:eye_lft_jnt.r','*:eyeLid_lft_mdv.input1')
        mc.connectAttr('*:eye_lft_ctrl.lidFollow','*:EyeLidsBsh_L_Jnt.eyelidsFollow')
        mc.setAttr('*:eye_lft_ctrl.lidFollow',1)
        
    # cpmmect EyeLid RGT
        mc.disconnectAttr('*:eye_rgt_ctrl.lidFollow','*:eyeLid_rgt_mdv.input2Y')
        mc.disconnectAttr('*:eye_rgt_ctrl.lidFollow','*:eyeLid_rgt_mdv.input2X')
        mc.disconnectAttr('*:eye_rgt_ctrl.lidFollow','*:eyeLid_rgt_mdv.input2Z')
        mc.setAttr('*:eyeLid_rgt_mdv.input2X',0)
        mc.setAttr('*:eyeLid_rgt_mdv.input2Y',0)
        mc.setAttr('*:eyeLid_rgt_mdv.input2Z',0)

        # mc.disconnectAttr('*:eye_rgt_jnt.r','*:eyeLid_rgt_mdv.input1')
        mc.connectAttr('*:eye_rgt_ctrl.lidFollow','*:EyeLidsBsh_R_Jnt.eyelidsFollow')
        mc.setAttr('*:eye_rgt_ctrl.lidFollow',1)
    except: pass

def lockAttrs(listObj, lock):
    # listEyes = ['EyeLidsBsh_L_Jnt','EyeLidsBsh_R_Jnt']
    attrs = ['t', 'r', 's' , 'eyelidsFollow']
#attrEyeLids = ["eyelidsFollow"]
    for each in listObj:
        if mc.objExists(each):
            for eyeJnt in listObj:
                for attr in attrs:
                    mc.setAttr( "%s.%s" %(eyeJnt, attr), lock = True)
                    #mc.setAttr('%s.v' %eyeJnt ,lock = 0)

def lockHideAttr(listObj, lock, keyable):
    # listEyes = ['EyeLidsBsh_L_Jnt','EyeLidsBsh_R_Jnt']
    attrs = ['t', 'r', 's', 'v', 'squash', 'stretch', 'eyelidsFollow']
#attrEyeLids = ["eyelidsFollow"]
    for each in listObj:
        if mc.objExists(each):
            for eyeJnt in listObj:
                for each in attrs:
                    if mc.objExists(each):
                        for attr in attrs:
                            mc.setAttr( "%s.%s" %(eyeJnt, attr), lock = lock, keyable = keyable)
                            #mc.setAttr('%s.v' %eyeJnt ,lock = 0)

# unlock mdv Node
def lockMdv(listObj, lock):
    attrsMdv = ['i1x', 'i1y', 'i1z', 'i2x' , 'i2y', 'i2z']
    for each in listObj:
        if mc.objExists(each):
            for listObjs in listObj:
                for attrsMdvs in attrsMdv:
                    mc.setAttr( "%s.%s" %(listObjs, attrsMdvs), lock = lock)
                    #mc.setAttr('%s.v' %listMdvs ,lock = 0)

# unlock bcl Node
def lockBcl(listObj, lock):
    try:
        attrsBcl = ['b', 'c1r', 'c1g', 'c1b' , 'c2r', 'c2g', 'c2b', 'arp']
        for each in listObj:
            if mc.objExists(each):
                for listObjs in listObj:
                    if mc.objExists(each)==True:
                        for attrsBcls in attrsBcl:
                            mc.setAttr( "%s.%s" %(listObjs, attrsBcls), lock = lock)
                            #mc.setAttr('%s.v' %listMdvs ,lock = 0)
    except: pass
# unlock rev Node
def lockRev(listObj, lock):
    try:
        attrsRev = ['ix', 'iy', 'iz']
        for each in listObj:
            if mc.objExists(each):
                for listObjs in listObj:
                    for attrsRevs in attrsRev:
                        mc.setAttr( "%s.%s" %(listObjs, attrsRevs), lock = lock)
                        #mc.setAttr('%s.v' %listMdvs ,lock = 0)
    except: pass
# unlock pma Node
def lockPma(listObj, lock):
    try:
        attrsPma = ['i1[0]', 'i1[1]', 'default']
        for each in listObj:
            if mc.objExists(each):
                for listObjs in listObj:
                    for attrsPmas in attrsPma:
                        mc.setAttr( "%s.%s" %(listObjs, attrsPmas), lock = lock)
                        #mc.setAttr('%s.v' %listMdvs ,lock = 0)
    except: pass
def lockAttrObj(listObj, lock):
    try:
        # listBow = ("bowARig_lft_grp","bowBRig_rgt_grp")
        for each in listObj:
            for attr in ( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v') :
                for i in range(len(listBow)):
                    mc.setAttr("%s.%s" % (each, attr), lock = lock)
    except: pass
# unlock HdLowHdDfmRig_Ctrl Node
def lockMidFacialSq_Ctrl(listObj, lock):
    try:
        attrsMidFacialSq = ['tx', 'ty', 'tz']
        for each in listObj:
            if mc.objExists(each):
                for listObjs in listObj:
                    for attrsMidFacialSqs in attrsMidFacialSq:
                        mc.setAttr( "%s.%s" %(listObjs, attrsMidFacialSqs), lock = lock)
                        #mc.setAttr('%s.v' %listMdvs ,lock = 0)
    except: pass
# unlock HdLowHdDfmRig_Ctrl Node
def lockUpLowFacialSq_Ctrl(listObj, lock):
    try:
        attrsUpLowFacialSq = ['tx', 'ty', 'tz', 'autoSquash', 'squash', 'slide']
        for each in listObj:
            if mc.objExists(each):
                for listObjs in listObj:
                    for attrsUpLowFacialSqs in attrsUpLowFacialSq:
                        mc.setAttr( "%s.%s" %(listObjs, attrsUpLowFacialSqs), lock = lock)
                        #mc.setAttr('%s.v' %listMdvs ,lock = 0)
    except: pass