import maya.cmds as mc
reload(mc)
import maya.mel as mm
reload(mm)

def eyeLidFollow():
	eyeList = mc.ls('*:eye_lft_ctrl', '*:eye_rgt_ctrl')
	eyeLidsList = mc.ls('*:EyeLidsBsh_L_Jnt', '*:EyeLidsBsh_R_Jnt')
	eyeCmp = mc.ls('*:EyeBallFollowIO_L_Cmp', '*:EyeBallFollowIO_R_Cmp',)

	mc.parentConstraint(eyeList[0],eyeLidsList[0])
	mc.parentConstraint(eyeList[1],eyeLidsList[1])

	# set eyeLidsBsh
	mc.setAttr('%s.eyelidsFollow' % eyeLidsList[0], 1)
	mc.setAttr('%s.eyelidsFollow' % eyeLidsList[1], 1)

	# L
	mc.setAttr('%s.minG' % eyeCmp[0], -300)
	mc.setAttr('%s.maxR' % eyeCmp[0], 300)
	# R
	mc.setAttr('%s.minR' % eyeCmp[1], -300)
	mc.setAttr('%s.maxG' % eyeCmp[1], 300)

