import sys
sys.path.append(r'P:\lib\local\utaTools\utapy')
import copyShapeMouth as csb 
reload(csb)
import pymel.core as pm 
reload(pm)
import eyelidFollowConnect as efc 
reload(efc)
import maya.cmds as mc
reload(mc)

# copyShape
def copyShapeMouths ( *args ) :
    csb.copyShapeToBsh_A();
def copyShapeMouth_LFT( *args ) :
    csb.copyShapeToBsh_B();
def copyShapeMouth_RGT ( *args ) :
    csb.copyShapeToBsh_C();
def getGeoBase(*args):
    sels = mc.ls(sl=True)       
    suffix = pm.textField ( "txtFieldGeoBase" , q  = True , text = True  )
    csb.copyShapeToBase(geoBase= suffix, lists= sels)
def addTextGeoBase(*args):
    sel = pm.ls(sl=True) ;
    sel = sel [0] ;
    pm.textField("txtFieldGeoBase", e = True  , text = sel)
    # print sel
def copyShapeUi ( *args ) :
    
    # check if window exists
    if pm.window ( 'copyShapeWinUi' , exists = True ) :
        pm.deleteUI ( 'copyShapeWinUi' ) ;
    else : pass ;
   
    window = pm.window ( 'copyShapeWinUi', title = "copyShapeToBsh v. 1.0" ,
        mnb = False , mxb = False , sizeable = True , rtf = True ) ;
        
    pm.window ( 'copyShapeWinUi' , e = True , h = 120, w = 300 ) ;
    mainLayout = pm.rowColumnLayout ( nc = 1 , p = window ) ;

    # 0. copyShape A
    rowLine1 = pm.rowColumnLayout ( nc = 3 , p = mainLayout , h = 10, w = 300 ) ;
    pm.button (label = "-"*100 ,h = 10, w = 300, p = rowLine1 , bgc = (0.333333, 0.333333, 0.333333)) ; 

    rowColumnGeoBase = pm.rowColumnLayout ( nc = 2 , p = mainLayout , w = 300 , cw = [ ( 1 , 220 ) , ( 2 , 80 )] ) ;
    textGeoBase = pm.textField( "txtFieldGeoBase", text = "BodyMthRig_Geo", parent = rowColumnGeoBase , editable = True , h = 28, w = 100) 
    pm.button ( 'addGeoBaseToBsh' , label = '<< add ', h = 30 , w = 20, p = rowColumnGeoBase , c = addTextGeoBase , bgc = (0.745098, 0.745098, 0.745098)) ; 
    
    rowRevert = pm.rowColumnLayout ( nc = 3 , p = mainLayout , h = 30, w = 300 ) ;
    pm.button ( 'copyShapeToBsh' , label = ' Revert to GeoBase ', h = 30 , w = 300, p = rowRevert , c = getGeoBase , bgc = (0, 1, 0)) ; 
    
    rowLine2 = pm.rowColumnLayout ( nc = 3 , p = mainLayout , h = 10, w = 300 ) ;
    pm.button (label = "-"*100 ,h = 10, w = 300, p = rowLine2 , bgc = (0.333333, 0.333333, 0.333333)) ; 
    
    rowCopyShape_A = pm.rowColumnLayout ( nc = 3 , p = mainLayout , h = 30, w = 300 ) ;
    pm.button ( 'copyShapeToBsh' , label = ' Mouth ', h = 30 , w = 300, p = rowCopyShape_A , c = copyShapeMouths , bgc = (255 , 120, 120)) ; 
    
    rowColumn2 = pm.rowColumnLayout ( nc = 2 , p = mainLayout , w = 300 , cw = [ ( 1 , 150 ) , ( 2 , 150 )] ) ;
    pm.button ( 'copyShapeToBsh_A' , label = ' Mouth LFT ', h = 30 , w = 150, p = rowColumn2 , c = copyShapeMouth_LFT, bgc = (0, 0.74902, 1)) ; 
    pm.button ( 'copyShapeToBsh_B' , label = ' Mouth RGT ', h = 30 , w = 150, p = rowColumn2 , c = copyShapeMouth_RGT, bgc = (1, 0.0784314, 0.576471)) ;
 
    rowLine3 = pm.rowColumnLayout ( nc = 3 , p = mainLayout , h = 10, w = 300 ) ;
    pm.button (label = "-"*100 ,h = 10, w = 300, p = rowLine3 , bgc = (0.333333, 0.333333, 0.333333)) ; 

    window.show () ;
# copyShapeWinUi () ;