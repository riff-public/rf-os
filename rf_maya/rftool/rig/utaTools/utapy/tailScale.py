import maya.cmds as mc
reload(mc)
def tailScale (*args):
	ctrlList = mc.ls('TailIk*_Ctrl')
	jntLists = mc.ls('TailIk*_Jnt')
	jntRZLists = mc.ls('TailIkRZ*_Jnt')
	lists = [a.replace('_Crv', '') for a in ctrlList]
	nameLists = lists[0]

	ikCtrlList = []
	for ctrlLists in ctrlList:
	    if'Gmbl' in ctrlLists:
	        pass
	    else:
	        ikCtrlList.append(ctrlLists)
	# print ikCtrlList
	attrs = ['X', 'Y', 'Z']
	setNode = [1.3, 0.8, 0.4]
	ii= 0
	for ikCtrlLists in ikCtrlList:
		for each in attrs:
			mc.connectAttr ('%s.scale%s' % (ikCtrlLists, each), '%s.scale%s' % (jntRZLists[ii+4], each))
		ii+=4
	iii = 1
	for ctrlLists in ikCtrlList:
		i=0
		for each in range(0, 3):
			mdvPNode = mc.createNode ('multiplyDivide', n =('TailIkP%s%s_Mdv' % (iii, attrs[i])))
			mc.setAttr('%s.operation' % mdvPNode, 3)
			pmaNode = mc.createNode ('plusMinusAverage', n =('TailIk%s%s_Pma' % (iii,attrs[i])))

			mdvDNode = mc.createNode ( 'multiplyDivide', n = ('TailIkD%s%s_Mdv' % (iii, attrs[i])))
			mc.setAttr('%s.operation' % mdvDNode, 2)
			mdvMNode = mc.createNode ( 'multiplyDivide', n = ('TailIkM%s%s_Mdv' % (iii, attrs[i])))
			for each in attrs:
				mc.setAttr('%s.input2%s' % (mdvPNode, each),setNode[i])
				mc.connectAttr('%s.scale%s' % (ctrlLists, each), '%s.input1%s' % (mdvPNode, each))
				mc.connectAttr('%s.output%s' % (mdvPNode, each), '%s.input3D[0].input3D%s' % (pmaNode, each.lower()))
				# Set Attr mdvDNode
				mc.setAttr('%s.input2%s' % (mdvDNode, each),2)
				# Connect Pma To Mdv2
				mc.connectAttr('%s.output3D%s' % (pmaNode, each.lower()), '%s.input1%s' % (mdvDNode, each))
				mc.connectAttr('%s.output%s' % (mdvDNode, each), '%s.input1%s' % (mdvMNode, each))
		
			i+=1
		iii+=4	

# Tail Loop Mdv To Pma Node
	jntCount = [1, 5, 9, 13, 17, 21, 25, 29, 33, 37, 41, 45, 49, 53, 57, 61, 65, 69, 73, 77, 81, 85]

#----------------------------------------------------------------------------------------------------------------
	# Connect TailIkP To HeadScale Jnt
	iii = 1
	tailAJnt = [4, 3, 2]
	setNodeTailA = [1.3, 1.3, 1.1]
	# print ikCtrlList[0]
	# for ctrlLists in ikCtrlList[0]:
	i=0
	# print ikCtrlList[0]
	for each in range(0, 3):
		mdvANode = mc.createNode ('multiplyDivide', n =('TailIkA%s%s_Mdv' % (i+1, attrs[i])))
		mc.setAttr('%s.operation' % mdvANode, 3)

		for each in attrs:
			#loop follow attrs(3 loop)
			mc.setAttr('%s.input2%s' % (mdvANode, each),setNodeTailA[i])
			mc.connectAttr('%s.scale%s' % (ikCtrlList[0], each), '%s.input1%s' % (mdvANode, each))
			mc.connectAttr ('%s.output%s' % (mdvANode, each),'TailIkRZ%s_Jnt.scale%s' % (tailAJnt[i], each))		# 	i+=1
		i+=1	
	

	# mc.setAttr('%s.input2X', mdvAXNode, 1.3)
	# mdvAYNode = mc.createNode ('multiplyDivide', n =('TailIkA1Y_Mdv'))
	# mc.setAttr('%s.input2X', mdvAYNode, 0.8)
	# mdvAZNode = mc.createNode ('multiplyDivide', n =('TailIkA1Z_Mdv'))
	# mc.setAttr('%s.input2X', mdvAZNode, 0.4)
	# mc.connectAttr('%s.scaleX' % ikCtrlList[0], '%s.input1X' % mdvAXNode)
	# mc.connectAttr('%s.scaleY' % ikCtrlList[0], '%s.input1X' % mdvAXNode)
	# mc.connectAttr('%s.scaleZ' % ikCtrlList[0], '%s.input1X' % mdvAXNode)



#----------------------------------------------------------------------------------------------------------------
	# mc.connectAttr ('TailIkM%sX_Mdv.output' % jntCount[0],'TailIkRZ%s_Jnt.scale' % 4)
	# mc.connectAttr ('TailIkM%sY_Mdv.output' % jntCount[0],'TailIkRZ%s_Jnt.scale' % 3)
	# mc.connectAttr ('TailIkM%sZ_Mdv.output' % jntCount[0],'TailIkRZ%s_Jnt.scale' % 2)
	print jntCount

	x = 0
	for jntCounts in jntCount:
		# print jntCount[-1], '...jntCount[-1]'
		if not jntCount[-1]:
			# print jntCounts
			# print jntCounts + 1
			# print 'TailIkP%sX_Mdv.outputX'% jntCount[x+1]
			# print 'TailIk%sZ_Pma.input3D[1].input3Dx'% jntCounts
			# Connect TailIkP to Pma Node
			mc.connectAttr ('TailIkP%sX_Mdv.outputX'% jntCount[x+1], 'TailIk%sZ_Pma.input3D[1].input3Dx'% jntCounts)
			mc.connectAttr ('TailIkP%sX_Mdv.outputY'% jntCount[x+1], 'TailIk%sZ_Pma.input3D[1].input3Dy'% jntCounts)
			mc.connectAttr ('TailIkP%sX_Mdv.outputZ'% jntCount[x+1], 'TailIk%sZ_Pma.input3D[1].input3Dz'% jntCounts)

			mc.connectAttr ('TailIkP%sY_Mdv.outputX'% jntCount[x+1], 'TailIk%sY_Pma.input3D[1].input3Dx'% jntCounts)
			mc.connectAttr ('TailIkP%sY_Mdv.outputY'% jntCount[x+1], 'TailIk%sY_Pma.input3D[1].input3Dy'% jntCounts)
			mc.connectAttr ('TailIkP%sY_Mdv.outputZ'% jntCount[x+1], 'TailIk%sY_Pma.input3D[1].input3Dz'% jntCounts)

			mc.connectAttr ('TailIkP%sZ_Mdv.outputX'% jntCount[x+1], 'TailIk%sX_Pma.input3D[1].input3Dx'% jntCounts)
			mc.connectAttr ('TailIkP%sZ_Mdv.outputY'% jntCount[x+1], 'TailIk%sX_Pma.input3D[1].input3Dy'% jntCounts)
			mc.connectAttr ('TailIkP%sZ_Mdv.outputZ'% jntCount[x+1], 'TailIk%sX_Pma.input3D[1].input3Dz'% jntCounts)

			# Connect TailIkM to ScaleJnt
			mc.connectAttr ('TailIkM%sX_Mdv.output' % jntCounts,'TailIkRZ%s_Jnt.scale' % (jntCount[x+1]+1))
			mc.connectAttr ('TailIkM%sY_Mdv.output' % jntCounts,'TailIkRZ%s_Jnt.scale' % (jntCount[x+1]+2))
			mc.connectAttr ('TailIkM%sZ_Mdv.output' % jntCounts,'TailIkRZ%s_Jnt.scale' % (jntCount[x+1]+3))

		x+=1


	# mc.connectAttr ('TailIkM1X_Mdv.output','TailIkRZ4_Jnt.scale')
	# mc.connectAttr ('TailIkM1Y_Mdv.output','TailIkRZ3_Jnt.scale')
	# mc.connectAttr ('TailIkM1Z_Mdv.output','TailIkRZ2_Jnt.scale')

