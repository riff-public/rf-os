# go to rest pose of the skinned obj
# select skinned obj, blend shape obj, jnt, target obj
import maya.cmds as mc
from lpRig import rigTools as lrr

bodySide = 'BodySide' 
ebSide = 'EbSide' 
bodyEach = 'BodyEach' 
ebEach = 'EbEach' 
ebJntL= 'eb_L_jnt' 
ebJntR= 'eb_R_jnt' 
ebInJntL= 'ebIn_L_jnt' 
ebInJntR= 'ebIn_R_jnt' 
ebMidJntL= 'ebMid_L_jnt' 
ebMidJntR= 'ebMid_R_jnt' 
ebOutJntL= 'ebOut_L_jnt' 
ebOutJntR= 'ebOut_R_jnt' 
bodyAllEyebrowUp= 'BodyAllEyebrowUp_Geo' 
ebAllEyebrowUp= 'EyebrowsAllEyebrowUp_Geo' 
bodyAllEyebrowDn= 'BodyAllEyebrowDn_Geo' 
ebAllEyebrowDn= 'EyebrowsAllEyebrowDn_Geo' 
bodyAllEyebrowIn= 'BodyAllEyebrowIn_Geo' 
ebAllEyebrowIn= 'EyebrowsAllEyebrowIn_Geo' 
bodyAllEyebrowOut= 'BodyAllEyebrowOut_Geo' 
ebAllEyebrowOut= 'EyebrowsAllEyebrowOut_Geo' 

def HeadEbRig_Geo_UpEbRig_L_Bsh(baseObj= bodySide ,
                                        srcObj=  bodyAllEyebrowUp ,
                                        tarObj=  'HeadEbRig_Geo_UpEbRig_L_Bsh' ,
                                        skinGeo= bodySide ,
                                        jnt=     ebJntL):
    # UpEbRig_L_Bsh
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= bodySide ,
                                        srcObj=  bodyAllEyebrowUp ,
                                        tarObj=  'HeadEbRig_Geo_UpEbRig_L_Bsh' ,
                                        skinGeo= bodySide ,
                                        jnt=     ebJntL
                                        )
def EyebrowEbRig_Geo_UpEbRig_L_Bsh():
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= ebSide ,
                                        srcObj=  ebAllEyebrowUp ,
                                        tarObj=  'EyebrowEbRig_Geo_UpEbRig_L_Bsh' ,
                                        skinGeo= ebSide ,
                                        jnt=     ebJntL
                                        )
def HeadEbRig_Geo_UpEbRig_R_Bsh():
    # UpEbRig_R_Bsh
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= bodySide ,
                                        srcObj=  bodyAllEyebrowUp ,
                                        tarObj=  'HeadEbRig_Geo_UpEbRig_R_Bsh' ,
                                        skinGeo= bodySide ,
                                        jnt=     ebJntR
                                        )
def EyebrowEbRig_Geo_UpEbRig_R_Bsh():
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= ebSide ,
                                        srcObj=  ebAllEyebrowUp ,
                                        tarObj=  'EyebrowEbRig_Geo_UpEbRig_R_Bsh' ,
                                        skinGeo= ebSide ,
                                        jnt=     ebJntR
                                        )
    ##################################################################################################################
def HeadEbRig_Geo_DnEbRig_L_Bsh():                                        
    # DnEbRig_L_Bsh
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= bodySide ,
                                        srcObj=  bodyAllEyebrowDn ,
                                        tarObj=  'HeadEbRig_Geo_DnEbRig_L_Bsh' ,
                                        skinGeo= bodySide ,
                                        jnt=     ebJntL
                                        )
def EyebrowEbRig_Geo_DnEbRig_L_Bsh():
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= ebSide ,
                                        srcObj=  ebAllEyebrowDn ,
                                        tarObj=  'EyebrowEbRig_Geo_DnEbRig_L_Bsh' ,
                                        skinGeo= ebSide ,
                                        jnt=     ebJntL
                                        )   
def HeadEbRig_Geo_DnEbRig_R_Bsh():                                                                        
    # DnEbRig_R_Bsh
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= bodySide ,
                                        srcObj=  bodyAllEyebrowDn ,
                                        tarObj=  'HeadEbRig_Geo_DnEbRig_R_Bsh' ,
                                        skinGeo= bodySide ,
                                        jnt=     ebJntR
                                        )
def EyebrowEbRig_Geo_DnEbRig_R_Bsh():
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= ebSide ,
                                        srcObj=  ebAllEyebrowDn ,
                                        tarObj=  'EyebrowEbRig_Geo_DnEbRig_R_Bsh' ,
                                        skinGeo= ebSide ,
                                        jnt=     ebJntR
                                        )

    ##################################################################################################################
def HeadEbRig_Geo_InEbRig_L_Bsh():                                       
    # InEbRig_L_Bsh
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= bodySide ,
                                        srcObj=  bodyAllEyebrowIn ,
                                        tarObj=  'HeadEbRig_Geo_InEbRig_L_Bsh' ,
                                        skinGeo= bodySide ,
                                        jnt=     ebJntL
                                        )
def EyebrowEbRig_Geo_InEbRig_L_Bsh():
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= ebSide ,
                                        srcObj=  ebAllEyebrowIn ,
                                        tarObj=  'EyebrowEbRig_Geo_InEbRig_L_Bsh' ,
                                        skinGeo= ebSide ,
                                        jnt=     ebJntL
                                        )   
def HeadEbRig_Geo_InEbRig_R_Bsh():                                                                        
    # InEbRig_R_Bsh
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= bodySide ,
                                        srcObj=  bodyAllEyebrowIn ,
                                        tarObj=  'HeadEbRig_Geo_InEbRig_R_Bsh' ,
                                        skinGeo= bodySide ,
                                        jnt=     ebJntR
                                        )
def EyebrowEbRig_Geo_InEbRig_R_Bsh():
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= ebSide ,
                                        srcObj=  ebAllEyebrowIn ,
                                        tarObj=  'EyebrowEbRig_Geo_InEbRig_R_Bsh' ,
                                        skinGeo= ebSide ,
                                        jnt=     ebJntR
                                        )

    ##################################################################################################################
def HeadEbRig_Geo_OutEbRig_L_Bsh():                                        
    # OutEbRig_L_Bsh
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= bodySide ,
                                        srcObj=  bodyAllEyebrowOut ,
                                        tarObj=  'HeadEbRig_Geo_OutEbRig_L_Bsh' ,
                                        skinGeo= bodySide ,
                                        jnt=     ebJntL
                                        )
def EyebrowEbRig_Geo_OutEbRig_L_Bsh():
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= ebSide ,
                                        srcObj=  ebAllEyebrowOut ,
                                        tarObj=  'EyebrowEbRig_Geo_OutEbRig_L_Bsh' ,
                                        skinGeo= ebSide ,
                                        jnt=     ebJntL
                                        )   
def HeadEbRig_Geo_OutEbRig_R_Bsh():                                                                        
    # OutEbRig_R_Bsh
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= bodySide ,
                                        srcObj=  bodyAllEyebrowOut ,
                                        tarObj=  'HeadEbRig_Geo_OutEbRig_R_Bsh' ,
                                        skinGeo= bodySide ,
                                        jnt=     ebJntR
                                        )
def EyebrowEbRig_Geo_OutEbRig_R_Bsh():
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= ebSide ,
                                        srcObj=  ebAllEyebrowOut ,
                                        tarObj=  'EyebrowEbRig_Geo_OutEbRig_R_Bsh' ,
                                        skinGeo= ebSide ,
                                        jnt=     ebJntR
                                        )

    ##################################################################################################################
def HeadEbRig_Geo_UpInEbRig_L_Bsh():                                       
    # UpInEbRig_L_Bsh
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= bodyEach ,
                                        srcObj=  bodyAllEyebrowUp ,
                                        tarObj=  'HeadEbRig_Geo_UpInEbRig_L_Bsh' ,
                                        skinGeo= bodyEach ,
                                        jnt=     ebInJntL
                                        )
def EyebrowEbRig_Geo_UpInEbRig_L_Bsh():
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= ebEach ,
                                        srcObj=  ebAllEyebrowUp ,
                                        tarObj=  'EyebrowEbRig_Geo_UpInEbRig_L_Bsh' ,
                                        skinGeo= ebEach ,
                                        jnt=     ebInJntL
                                        )   
def HeadEbRig_Geo_UpInEbRig_R_Bsh():                                                                       
    # UpInEbRig_R_Bsh
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= bodyEach ,
                                        srcObj=  bodyAllEyebrowUp ,
                                        tarObj=  'HeadEbRig_Geo_UpInEbRig_R_Bsh' ,
                                        skinGeo= bodyEach ,
                                        jnt=     ebInJntR
                                        )
def EyebrowEbRig_Geo_UpInEbRig_R_Bsh():
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= ebEach ,
                                        srcObj=  ebAllEyebrowUp ,
                                        tarObj=  'EyebrowEbRig_Geo_UpInEbRig_R_Bsh' ,
                                        skinGeo= ebEach ,
                                        jnt=     ebInJntR
                                        )

    ##################################################################################################################
def HeadEbRig_Geo_UpMidEbRig_L_Bsh():                                        
    # UpMidEbRig_L_Bsh
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= bodyEach ,
                                        srcObj=  bodyAllEyebrowUp ,
                                        tarObj=  'HeadEbRig_Geo_UpMidEbRig_L_Bsh' ,
                                        skinGeo= bodyEach ,
                                        jnt=     ebMidJntL
                                        )
def EyebrowEbRig_Geo_UpMidEbRig_L_Bsh():
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= ebEach ,
                                        srcObj=  ebAllEyebrowUp ,
                                        tarObj=  'EyebrowEbRig_Geo_UpMidEbRig_L_Bsh' ,
                                        skinGeo= ebEach ,
                                        jnt=     ebMidJntL
                                        )   
def HeadEbRig_Geo_UpMidEbRig_R_Bsh():                                                                        
    # UpMidEbRig_R_Bsh
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= bodyEach ,
                                        srcObj=  bodyAllEyebrowUp ,
                                        tarObj=  'HeadEbRig_Geo_UpMidEbRig_R_Bsh' ,
                                        skinGeo= bodyEach ,
                                        jnt=     ebMidJntR
                                        )
def EyebrowEbRig_Geo_UpMidEbRig_R_Bsh():
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= ebEach ,
                                        srcObj=  ebAllEyebrowUp ,
                                        tarObj=  'EyebrowEbRig_Geo_UpMidEbRig_R_Bsh' ,
                                        skinGeo= ebEach ,
                                        jnt=     ebMidJntR
                                        )

    ##################################################################################################################
def HeadEbRig_Geo_UpOutEbRig_L_Bsh():                                        
    # UpOutEbRig_L_Bsh
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= bodyEach ,
                                        srcObj=  bodyAllEyebrowUp ,
                                        tarObj=  'HeadEbRig_Geo_UpOutEbRig_L_Bsh' ,
                                        skinGeo= bodyEach ,
                                        jnt=     ebOutJntL
                                        )
def EyebrowEbRig_Geo_UpOutEbRig_L_Bsh():
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= ebEach ,
                                        srcObj=  ebAllEyebrowUp ,
                                        tarObj=  'EyebrowEbRig_Geo_UpOutEbRig_L_Bsh' ,
                                        skinGeo= ebEach ,
                                        jnt=     ebOutJntL
                                        )   
def HeadEbRig_Geo_UpOutEbRig_R_Bsh():                                                                        
    # UpOutEbRig_R_Bsh
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= bodyEach ,
                                        srcObj=  bodyAllEyebrowUp ,
                                        tarObj=  'HeadEbRig_Geo_UpOutEbRig_R_Bsh' ,
                                        skinGeo= bodyEach ,
                                        jnt=     ebOutJntR
                                        )
def EyebrowEbRig_Geo_UpOutEbRig_R_Bsh():
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= ebEach ,
                                        srcObj=  ebAllEyebrowUp ,
                                        tarObj=  'EyebrowEbRig_Geo_UpOutEbRig_R_Bsh' ,
                                        skinGeo= ebEach ,
                                        jnt=     ebOutJntR
                                        )
                                        
    ##################################################################################################################
def HeadEbRig_Geo_DnInEbRig_L_Bsh():                                        
    # DnInEbRig_L_Bsh
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= bodyEach ,
                                        srcObj=  bodyAllEyebrowDn ,
                                        tarObj=  'HeadEbRig_Geo_DnInEbRig_L_Bsh' ,
                                        skinGeo= bodyEach ,
                                        jnt=     ebInJntL
                                        )
def EyebrowEbRig_Geo_DnInEbRig_L_Bsh():
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= ebEach ,
                                        srcObj=  ebAllEyebrowDn ,
                                        tarObj=  'EyebrowEbRig_Geo_DnInEbRig_L_Bsh' ,
                                        skinGeo= ebEach ,
                                        jnt=     ebInJntL
                                        )   
def HeadEbRig_Geo_DnInEbRig_R_Bsh():                                                                   
    # DnInEbRig_R_Bsh
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= bodyEach ,
                                        srcObj=  bodyAllEyebrowDn ,
                                        tarObj=  'HeadEbRig_Geo_DnInEbRig_R_Bsh' ,
                                        skinGeo= bodyEach ,
                                        jnt=     ebInJntR
                                        )
def EyebrowEbRig_Geo_DnInEbRig_R_Bsh():
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= ebEach ,
                                        srcObj=  ebAllEyebrowDn ,
                                        tarObj=  'EyebrowEbRig_Geo_DnInEbRig_R_Bsh' ,
                                        skinGeo= ebEach ,
                                        jnt=     ebInJntR
                                        )

    ##################################################################################################################
def HeadEbRig_Geo_DnMidEbRig_L_Bsh():                                   
    # DnMidEbRig_L_Bsh
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= bodyEach ,
                                        srcObj=  bodyAllEyebrowDn ,
                                        tarObj=  'HeadEbRig_Geo_DnMidEbRig_L_Bsh' ,
                                        skinGeo= bodyEach ,
                                        jnt=     ebMidJntL
                                        )
def EyebrowEbRig_Geo_DnMidEbRig_L_Bsh():
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= ebEach ,
                                        srcObj=  ebAllEyebrowDn ,
                                        tarObj=  'EyebrowEbRig_Geo_DnMidEbRig_L_Bsh' ,
                                        skinGeo= ebEach ,
                                        jnt=     ebMidJntL
                                        )   
def HeadEbRig_Geo_DnMidEbRig_R_Bsh():                                                                 
    # DnMidEbRig_R_Bsh
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= bodyEach ,
                                        srcObj=  bodyAllEyebrowDn ,
                                        tarObj=  'HeadEbRig_Geo_DnMidEbRig_R_Bsh' ,
                                        skinGeo= bodyEach ,
                                        jnt=     ebMidJntR
                                        )
def EyebrowEbRig_Geo_DnMidEbRig_R_Bsh():
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= ebEach ,
                                        srcObj=  ebAllEyebrowDn ,
                                        tarObj=  'EyebrowEbRig_Geo_DnMidEbRig_R_Bsh' ,
                                        skinGeo= ebEach ,
                                        jnt=     ebMidJntR
                                        )

    ##################################################################################################################
def HeadEbRig_Geo_DnOutEbRig_L_Bsh():                                   
    # DnOutEbRig_L_Bsh
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= bodyEach ,
                                        srcObj=  bodyAllEyebrowDn ,
                                        tarObj=  'HeadEbRig_Geo_DnOutEbRig_L_Bsh' ,
                                        skinGeo= bodyEach ,
                                        jnt=     ebOutJntL
                                        )
def EyebrowEbRig_Geo_DnOutEbRig_L_Bsh():
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= ebEach ,
                                        srcObj=  ebAllEyebrowDn ,
                                        tarObj=  'EyebrowEbRig_Geo_DnOutEbRig_L_Bsh' ,
                                        skinGeo= ebEach ,
                                        jnt=     ebOutJntL
                                        )   
def HeadEbRig_Geo_DnOutEbRig_R_Bsh():                                                                   
    # DnOutEbRig_R_Bsh
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= bodyEach ,
                                        srcObj=  bodyAllEyebrowDn ,
                                        tarObj=  'HeadEbRig_Geo_DnOutEbRig_R_Bsh' ,
                                        skinGeo= bodyEach ,
                                        jnt=     ebOutJntR
                                        )
def EyebrowEbRig_Geo_DnOutEbRig_R_Bsh():
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= ebEach ,
                                        srcObj=  ebAllEyebrowDn ,
                                        tarObj=  'EyebrowEbRig_Geo_DnOutEbRig_R_Bsh' ,
                                        skinGeo= ebEach ,
                                        jnt=     ebOutJntR
                                        )
                                        
    ##################################################################################################################
def HeadEbRig_Geo_InInEbRig_L_Bsh():                                   
    # InInEbRig_L_Bsh
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= bodyEach ,
                                        srcObj=  bodyAllEyebrowIn ,
                                        tarObj=  'HeadEbRig_Geo_InInEbRig_L_Bsh' ,
                                        skinGeo= bodyEach ,
                                        jnt=     ebInJntL
                                        )
def EyebrowEbRig_Geo_InInEbRig_L_Bsh():
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= ebEach ,
                                        srcObj=  ebAllEyebrowIn ,
                                        tarObj=  'EyebrowEbRig_Geo_InInEbRig_L_Bsh' ,
                                        skinGeo= ebEach ,
                                        jnt=     ebInJntL
                                        )   
def HeadEbRig_Geo_InInEbRig_R_Bsh():                                                                    
    # InInEbRig_R_Bsh
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= bodyEach ,
                                        srcObj=  bodyAllEyebrowIn ,
                                        tarObj=  'HeadEbRig_Geo_InInEbRig_R_Bsh' ,
                                        skinGeo= bodyEach ,
                                        jnt=     ebInJntR
                                        )
def EyebrowEbRig_Geo_InInEbRig_R_Bsh():
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= ebEach ,
                                        srcObj=  ebAllEyebrowIn ,
                                        tarObj=  'EyebrowEbRig_Geo_InInEbRig_R_Bsh' ,
                                        skinGeo= ebEach ,
                                        jnt=     ebInJntR
                                        )

    ##################################################################################################################
def HeadEbRig_Geo_InMidEbRig_L_Bsh():                                        
    # InMidEbRig_L_Bsh
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= bodyEach ,
                                        srcObj=  bodyAllEyebrowIn ,
                                        tarObj=  'HeadEbRig_Geo_InMidEbRig_L_Bsh' ,
                                        skinGeo= bodyEach ,
                                        jnt=     ebMidJntL
                                        )
def EyebrowEbRig_Geo_InMidEbRig_L_Bsh():
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= ebEach ,
                                        srcObj=  ebAllEyebrowIn ,
                                        tarObj=  'EyebrowEbRig_Geo_InMidEbRig_L_Bsh' ,
                                        skinGeo= ebEach ,
                                        jnt=     ebMidJntL
                                        )   
def HeadEbRig_Geo_InMidEbRig_R_Bsh():                                                                        
    # InMidEbRig_R_Bsh
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= bodyEach ,
                                        srcObj=  bodyAllEyebrowIn ,
                                        tarObj=  'HeadEbRig_Geo_InMidEbRig_R_Bsh' ,
                                        skinGeo= bodyEach ,
                                        jnt=     ebMidJntR
                                        )
def EyebrowEbRig_Geo_InMidEbRig_R_Bsh():
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= ebEach ,
                                        srcObj=  ebAllEyebrowIn ,
                                        tarObj=  'EyebrowEbRig_Geo_InMidEbRig_R_Bsh' ,
                                        skinGeo= ebEach ,
                                        jnt=     ebMidJntR
                                        )

    ##################################################################################################################
def HeadEbRig_Geo_InOutEbRig_L_Bsh():                                        
    # InOutEbRig_L_Bsh
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= bodyEach ,
                                        srcObj=  bodyAllEyebrowIn ,
                                        tarObj=  'HeadEbRig_Geo_InOutEbRig_L_Bsh' ,
                                        skinGeo= bodyEach ,
                                        jnt=     ebOutJntL
                                        )
def EyebrowEbRig_Geo_InOutEbRig_L_Bsh():
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= ebEach ,
                                        srcObj=  ebAllEyebrowIn ,
                                        tarObj=  'EyebrowEbRig_Geo_InOutEbRig_L_Bsh' ,
                                        skinGeo= ebEach ,
                                        jnt=     ebOutJntL
                                        )   
def HeadEbRig_Geo_InOutEbRig_R_Bsh():                                                                        
    # InOutEbRig_R_Bsh
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= bodyEach ,
                                        srcObj=  bodyAllEyebrowIn ,
                                        tarObj=  'HeadEbRig_Geo_InOutEbRig_R_Bsh' ,
                                        skinGeo= bodyEach ,
                                        jnt=     ebOutJntR
                                        )
def EyebrowEbRig_Geo_InOutEbRig_R_Bsh():
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= ebEach ,
                                        srcObj=  ebAllEyebrowIn ,
                                        tarObj=  'EyebrowEbRig_Geo_InOutEbRig_R_Bsh' ,
                                        skinGeo= ebEach ,
                                        jnt=     ebOutJntR
                                        )
                                        
    ##################################################################################################################
def HeadEbRig_Geo_OutInEbRig_L_Bsh():                                        
    # OutInEbRig_L_Bsh
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= bodyEach ,
                                        srcObj=  bodyAllEyebrowOut ,
                                        tarObj=  'HeadEbRig_Geo_OutInEbRig_L_Bsh' ,
                                        skinGeo= bodyEach ,
                                        jnt=     ebInJntL
                                        )
def EyebrowEbRig_Geo_OutInEbRig_L_Bsh():
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= ebEach ,
                                        srcObj=  ebAllEyebrowOut ,
                                        tarObj=  'EyebrowEbRig_Geo_OutInEbRig_L_Bsh' ,
                                        skinGeo= ebEach ,
                                        jnt=     ebInJntL
                                        )   
def HeadEbRig_Geo_OutInEbRig_R_Bsh():                                                                       
    # OutInEbRig_R_Bsh
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= bodyEach ,
                                        srcObj=  bodyAllEyebrowOut ,
                                        tarObj=  'HeadEbRig_Geo_OutInEbRig_R_Bsh' ,
                                        skinGeo= bodyEach ,
                                        jnt=     ebInJntR
                                        )
def EyebrowEbRig_Geo_OutInEbRig_R_Bsh():
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= ebEach ,
                                        srcObj=  ebAllEyebrowOut ,
                                        tarObj=  'EyebrowEbRig_Geo_OutInEbRig_R_Bsh' ,
                                        skinGeo= ebEach ,
                                        jnt=     ebInJntR
                                        )

    ##################################################################################################################
def HeadEbRig_Geo_OutMidEbRig_L_Bsh():                                        
    # OutMidEbRig_L_Bsh
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= bodyEach ,
                                        srcObj=  bodyAllEyebrowOut ,
                                        tarObj=  'HeadEbRig_Geo_OutMidEbRig_L_Bsh' ,
                                        skinGeo= bodyEach ,
                                        jnt=     ebMidJntL
                                        )
def EyebrowEbRig_Geo_OutMidEbRig_L_Bsh():
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= ebEach ,
                                        srcObj=  ebAllEyebrowOut ,
                                        tarObj=  'EyebrowEbRig_Geo_OutMidEbRig_L_Bsh' ,
                                        skinGeo= ebEach ,
                                        jnt=     ebMidJntL
                                        )   
def HeadEbRig_Geo_OutMidEbRig_R_Bsh():                                                                       
    # OutMidEbRig_R_Bsh
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= bodyEach ,
                                        srcObj=  bodyAllEyebrowOut ,
                                        tarObj=  'HeadEbRig_Geo_OutMidEbRig_R_Bsh' ,
                                        skinGeo= bodyEach ,
                                        jnt=     ebMidJntR
                                        )
def EyebrowEbRig_Geo_OutMidEbRig_R_Bsh():
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= ebEach ,
                                        srcObj=  ebAllEyebrowOut ,
                                        tarObj=  'EyebrowEbRig_Geo_OutMidEbRig_R_Bsh' ,
                                        skinGeo= ebEach ,
                                        jnt=     ebMidJntR
                                        )

    ##################################################################################################################
def HeadEbRig_Geo_OutOutEbRig_L_Bsh():                                       
    # OutOutEbRig_L_Bsh
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= bodyEach ,
                                        srcObj=  bodyAllEyebrowOut ,
                                        tarObj=  'HeadEbRig_Geo_OutOutEbRig_L_Bsh' ,
                                        skinGeo= bodyEach ,
                                        jnt=     ebOutJntL
                                        )
def EyebrowEbRig_Geo_OutOutEbRig_L_Bsh():
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= ebEach ,
                                        srcObj=  ebAllEyebrowOut ,
                                        tarObj=  'EyebrowEbRig_Geo_OutOutEbRig_L_Bsh' ,
                                        skinGeo= ebEach ,
                                        jnt=     ebOutJntL
                                        )   
def HeadEbRig_Geo_OutOutEbRig_R_Bsh():                                                                        
    # OutOutEbRig_R_Bsh
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= bodyEach ,
                                        srcObj=  bodyAllEyebrowOut ,
                                        tarObj=  'HeadEbRig_Geo_OutOutEbRig_R_Bsh' ,
                                        skinGeo= bodyEach ,
                                        jnt=     ebOutJntR
                                        )
def EyebrowEbRig_Geo_OutOutEbRig_R_Bsh():
    lrr.applyVertexMoveWithSkinWeight(
                                        baseObj= ebEach ,
                                        srcObj=  ebAllEyebrowOut ,
                                        tarObj=  'EyebrowEbRig_Geo_OutOutEbRig_R_Bsh' ,
                                        skinGeo= ebEach ,
                                        jnt=     ebOutJntR
                                        )