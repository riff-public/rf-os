import maya.cmds as mc ;
import pymel.core as pm ;
import rigTools
reload(rigTools)
import getNameSpace as gns 
reload(gns)

def snapPaths(objSel, elem, ns, obj1, obj2, *args):
	# sel = mc.ls(sl=True)
	objSelA = obj1
	objSelB = ('%sOnOffSwitch1_Ctrl' % obj2)
	# print objSelB
	# onOffSeitch = objSelB[0]

	lstPthTmp = mc.ls('%sPathsM*_Jnt' % ns)
	NoLstPthTmp = len(lstPthTmp)

	lstPthConMGrp = mc.listRelatives('%sTailPathsJntZro_grp' % ns ,ad = True)

	lstPthUpJnt = mc.ls('%sPathsUp*_Jnt' % ns)
	lstPthConGrp = []

	for lstGrps in lstPthConMGrp:
	    lstGrp = lstGrps
	    # print lstGrp
	    if'_Jnt' in lstGrp:
	        pass
	    else:
	        lstPthConGrp.append(lstGrp)
	        # print lstPthConGrp

	for i in range(NoLstPthTmp):

	    mc.pointConstraint(lstPthTmp[-i],lstPthConGrp[i-1],mo = True)
	# mc.ls(cl=True)
	startJntPthMn = lstPthTmp[0]
	endJntPthMn = lstPthTmp[-1]
	startJntPthUp = lstPthUpJnt[0]
	endJntPthUp = lstPthUpJnt[-1]

	# Snap PathJnt to CrvPath
	snapPthMn = mc.ikHandle(sj =startJntPthMn,ee =endJntPthMn,c=('%s' % objSelA), n= ('%s%sIkPaths_ikh' % (ns,objSelA)), sol='ikSplineSolver', ccv=False, roc=True, pcv=False)
	snapPthUp = mc.ikHandle(sj =startJntPthUp,ee =endJntPthUp,c=('%sMotionsUpTemp' % objSelA), n= ('%s%sIkPathsUp_ikh' % (ns,objSelA)), sol='ikSplineSolver', ccv=False, roc=True, pcv =False)

	#create Ctrl and Grp Ctrl
	# ZGrp = mc.createNode('transform',n='%sIkPathCtrlZro_Grp' % ns)
	# CbCrv = mc.curve(d = 1,n = '%sIkPath_Ctrl' % ns,p=[(-0.5,1,0.5),(-0.5,0,0.5),(-0.5,0,-0.5),(-0.5,1,-0.5),(-0.5,1,0.5),(0.5,1,0.5),(0.5,0,0.5),(-0.5,0,0.5),(0.5,0,0.5),(0.5,0,-0.5),(0.5,1,-0.5),(0.5,1,0.5),(0.5,1,-0.5),(-0.5,1,-0.5),(-0.5,0,-0.5),(0.5,0,-0.5)])
	# CbCrv = mc.curve(d = 1,n = 'IkPath_Ctrl',p=[(-99.056548,0,0),(-88.41312,0,0),(-87.324554,0,-13.830848),(-84.08589,0,-27.321117),(-78.776635,0,-40.138682),(-71.527721,0,-51.967883),(-62.517537,0,-62.517537),(-51.967883,0,-71.527721),(-40.138682,0,-78.776635),(-27.321117,0,-84.08589),(-13.830848,0,-87.324554),(0,0,-88.41312),(0,0,-99.056548),(0,0,-88.41312),(13.830848,0,-87.324554),(27.321117,0,-84.08589),(40.138682,0,-78.776635),(51.967883,0,-71.527721),(62.517537,0,-62.517537),(71.527721,0,-51.967883),(78.776635,0,-40.138682),(84.08589,0,-27.321117),(87.324554,0,-13.830848),(88.41312,0,0),(99.056548,0,0),(88.41312,0,0),(87.324554,0,13.830848),(84.08589,0,27.321117),(78.776635,0,40.138682),(71.527721,0,51.967883),(62.517537,0,62.517537),(51.967883,0,71.527721),(40.138682,0,78.776635),(27.321117,0,84.08589),(13.830848,0,87.324554),(0,0,88.41312),(0,0,99.056548),(0,0,88.41312),(-13.830848,0,87.324554),(-27.321117,0,84.08589),(-40.138682,0,78.776635),(-51.967883,0,71.527721),(-62.517537,0,62.517537),(-71.527721,0,51.967883),(-78.776635,0,40.138682),(-84.08589,0,27.321117),(-87.324554,0,13.830848),(-88.41312,0,0)])
	# CbShape = mc.listRelatives( CbCrv , shapes = True )[0]
	# mc.setAttr('%s.overrideEnabled'%CbShape,1)
	# mc.setAttr('%s.overrideColor'%CbShape,22)
	# mc.parent(CbCrv,ZGrp)


	#Add Attr to IkPathCtrl
	# mc.addAttr(CbCrv,ln = 'IkPathVaule',at='double',dv = 0,k=True)
	# mc.setAttr(CbCrv+'.IkPathVaule',l=1)
	# mc.addAttr(CbCrv,ln = 'Offset',at='float', dv = 0 ,k=True)
	# mc.addAttr(CbCrv,ln = 'Roll',at='float',dv = 0,min = 0,k=True)
	# mc.addAttr(CbCrv,ln = 'Twist',at='float',dv = 0,k=True)
	# mc.addAttr(CbCrv,ln = 'IkBlend',at='float',dv = 1,min = 0,max =1,k=True)


	# mc.connectAttr(CbCrv+'.Roll',snapPthMn[0]+'.roll')
	# mc.connectAttr(CbCrv+'.Twist',snapPthMn[0]+'.twist')
	# mc.connectAttr(CbCrv+'.IkBlend',snapPthMn[0]+'.ikBlend')

	mc.connectAttr(snapPthMn[0]+'.offset',snapPthUp[0]+'.offset')
#----------------------------------------------------------------------------------------------
	# addAttribute to Functions Control
	mc.addAttr(objSelB, ln= 'Motions_______', at='double',dv = 0, k = True)
	# mc.setAttr('%s.Motions_______' % objSelB, lock = True)
	mc.addAttr(objSelB, ln = 'PathsOffset',at='double',dv = 0,k=True)
	pathsOffsetMdv = mc.createNode ('multiplyDivide', n = 'PathsOffset_Mdv')
	pathsOffsetPma = mc.createNode ('plusMinusAverage', n = 'PathsOffset_Pma')
	mc.connectAttr ('%s.PathsOffset' % objSelB, '%s.input1X' % pathsOffsetMdv)
	mc.setAttr('%s.input2X' % pathsOffsetMdv, -0.1)
	mc.connectAttr('%s.outputX' % pathsOffsetMdv, '%s.input2D[0].input2Dx' % pathsOffsetPma)
	mc.setAttr('%s.input2D[1].input2Dx' % pathsOffsetPma, 1)
	mc.connectAttr('%s.output2Dx' % pathsOffsetPma, '%s.offset' % snapPthMn[0])
	# mc.parent(ZGrp, ('%sCtrl_Grp' % ns))
# #----------------------------------------------------------------------------------------------
	# Create Group Jnt Motions Paths-----------------------------------------
	motionsMZro = mc.createNode ('transform', n = ('%s%sMotionsMZro_Grp' % (ns, elem)))
	motionsUpZro = mc.createNode ('transform', n = ('%s%sMotionsUpZro_Grp' % (ns, elem )))
	mc.parent('%sPathsM1_Jnt'% ns, motionsMZro)
	mc.parent('%sPathsUp1_Jnt'% ns, motionsUpZro)

	mc.hide(motionsMZro, motionsUpZro)

	#Create Group Ikh-------------------------------------------------------
	motionsIkhZro = mc.createNode ('transform', n = ('%s%sMotionsIkhZro_Grp' % (ns, elem )))
	mc.parent(('%s%sIkPaths_ikh'% (ns, objSelA )), ('%s%sIkPathsUp_ikh' % (ns,objSelA)), motionsIkhZro)
	mc.hide(motionsIkhZro)
