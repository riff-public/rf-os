from ncmel import core
reload(core)
from ncmel import rigTools
reload(rigTools)

def fixRbnSingleChain(rbnGrp=[] , parent=[] , spine =True , *args):

    for i in range(len(rbnGrp)):
        subGrp = mc.listRelatives(rbnGrp[i] , c=1 , typ = 'transform')
        
        for each in subGrp :
            jnt = mc.listRelatives(each , c=1 , typ = 'joint')
            
            if jnt :
                jnt = core.Dag(jnt[0])
                jnt.unlockAttrs('tx' , 'ty' , 'tz' , 'rx' , 'ry' ,'rz' , 'sx' , 'sy' , 'sz')
                mc.parent(jnt , parent[i])
                con =  mc.listRelatives(each , c=1 ,typ='parentConstraint')[0]
                allAttrs =  mc.listAttr(con)
                
                for b in allAttrs:
                    if 'W0' in b:
                        dtlCtrl = b.split('W0')[0]
                        mc.parentConstraint( dtlCtrl , jnt , mo=1)
                jnt.lockAttrs('tx' , 'ty' , 'tz' , 'rx' , 'ry' ,'rz' , 'sx' , 'sy' , 'sz')

    mc.delete(rbnGrp)

    if spine == True:
        spGrp = 'SpineSkin_Grp'
        rootJnt = 'Root_Jnt'
        sps = mc.listRelatives(spGrp ,  c = 1)
        mc.parent(sps , rootJnt)
        mc.delete(spGrp)
    
    print "# FixRbnSingleChain >> Done"

#### EXAMPLE FOR RUN #####
# print "# Generate >> FixRbnSingleChain"                    
# fixRbnSingleChain(rbnGrp = [ 'NeckRbnSkin_Grp' ,
#                               'UpArmRbnSkin_L_Grp' ,
#                               'ForearmRbnSkin_L_Grp' ,
#                               'UpArmRbnSkin_R_Grp' ,
#                               'ForearmRbnSkin_R_Grp' ,
#                               'UpLegRbnSkin_L_Grp' ,
#                               'LowLegRbnSkin_L_Grp' ,
#                               'UpLegRbnSkin_R_Grp' ,
#                               'LowLegRbnSkin_R_Grp' ] ,
#                     parent = [ 'Neck_Jnt' ,
#                                'UpArm_L_Jnt' ,
#                                'Forearm_L_Jnt' ,
#                                'UpArm_R_Jnt' ,
#                                'Forearm_R_Jnt' ,
#                                'UpLeg_L_Jnt' ,
#                                'LowLeg_L_Jnt' ,
#                                'UpLeg_R_Jnt' ,
#                                'LowLeg_R_Jnt' ] ,
#                     spine = True
#                             )
