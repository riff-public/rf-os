#-----------------------------#(py)scrip write controlShape  --------------------------------------------

import pkmel.ctrlShapeTools as pct
pct.writeAllCtrl()
#--------------------------#(py)scrip read controlShape ---------------------------------------------------

import pkmel.ctrlShapeTools as pct
pct.readAllCtrl()
#-------------------------------read rearchFor -------------------------------------------------------------
import pkmel.weightTools as wt
reload(wt)
wt.readSelectedWeight2(    weightFolderPath='',
                           searchFor='', 
                           replaceWith='', 
                           prefix='', 
                           suffix='')
#---------- #(py)scrip write and read (chang) skinWeight--------------------------------------------------
import pkmel.weightTools as weightTools
weightTools.readSelectedWeight()

help( weightTools.readWeight )

weightTools.readWeight( 'body_ply' , r'Y:\SHARE\nun\baseRigFriends\data\body_plyWeight.txt' )

#---------------------------------------------------------------------------------------------------------
#(py)gen  ribbon

import pkmel.ribbon as pkr
reload( pkr )

import pkmel.rigTools as rigTools

help( rigTools )
# create Ribbon ( distanceDimension)
rbnObj = pkr.RibbonIkHi( 2.5 , 'y+' )  ## pkr.RibbonIkHi  or pkr.RibbonIkLow

dir( rbnObj )
# rename Ribbon 
rigTools.dummyNaming( rbnObj, 
                      attr='rbn', 
                      dummy='animaRbn', 
                      charName='rope', 
                      elem='', 
                      side='RGT')

#---------------------------------------------------------------------------------------------------------
reverse Curve  >>|<<  Edit Curves> Reverse Curve Direction

#scrip run Rig dog
sys.path.append(r'Y:\SHARE\ken\BASE-RIG-MiniFig\data')
import miniFig  as mf
reload(mf)

mf.main()

#----------------------publish combRig  of Rig------------------------------------------------------
import pkmel.rigTools as rigTools
reload( rigTools )

rigTools.importRigElement()
rigTools.importAllReference()  #import Reference
rigTools.removeAllNamespace()  # remove NameSpace
rigTools.connectProxySkinJoint()  #connect ProxySkinJoint
#---------------------------------------------------------------------------------------------------


#----------------------Cut and Connect BS-----------------------------------------------------------
import pkmel.rigTools as rigTools
reload( rigTools )
import pkmel.blendShapeTools as blendShapeTools
reload( blendShapeTools )

rigTools.detachSelectedEdge()# cut HeadBody  @ bsh
blendShapeTools.dupObjectForBlendShape()# Duplicate Head to blendShape @ bsh
#importControl BSH "O:\globalMaya\python\ptTools\pkmel\bshCtrlFriends.ma"  , "C:\Users\ken\Dropbox\scrip\Template Joint & Scrip\mouth Control.ma"
blendShapeTools.connectBlendShape() # connect 1 blendshape @ bsh
blendShapeTools.frdConnectControl() # connect 2 Control @ bsh
blendShapeTools.connectAutoLid() # connect 3 eyelid @ bsh
#-----------------connect at Rig-------------------
blendShapeTools.connectEyeRigToBshCtrl()  # connect eyeLid to Control @ Anim

#---------------------------------------------------------------------------------------------------

#----------------------Publish Build baseRig--------------------------------------------------------
import rigBrowser.run as rigBrowser
reload( rigBrowser )
app = rigBrowser.mayaRun()

#---------------------------------------------------------------------------------------------------
#?????? Set ?????????
import pkmel.ptTools as ptTools
reload( ptTools )

ptTools.city2DSetPlayblastCam()
#?????????? Snap thumb nail

import pkmel.ptTools as ptTools
reload( ptTools )

ptTools.city2DCreateThumbnail()

#--------------------------process publish-------------------------------------------------------------------------

1. AutoFill( Red ) - Save
2. Rig Publish( Pink )
	2.1 Publish
	2.2 Export Proxy Set
	2.3 Open( Anim Shading )
	2.4 Connect Shade - Save
	2.5 Export Anim_Set
3. Open Rig Hero - Save to UV step
4. Global Publish( Green )
	4.1 Current Drpt - Rig
		Task - Proxy_Rig - Status - Client Approved
	4.2 Current Drpt - Rig
		Task - Anim_Rig - Status - Client Approved
	4.2 Current Drpt - Rig
		Task - Rig - Status - Wait to Start
	4.3 Next Dept - Texture
		Task - uv - Status - Ready to Start
#------------------------		
		import pkmel.rigTools as rigTools
reload( rigTools )

rigTools.saveToHero() 

#------------------------------
from tools import mayaTools
reload(mayaTools)
mayaTools.findDuplicatedName()
#---------------set thumnail Camera---------------
import pkmel.ptTools as ptTools
reload( ptTools )

ptTools.city2DSetPlayblastCam()
#---------------set photo to thumnail---------------
import pkmel.ptTools as ptTools
reload( ptTools )

ptTools.city2DCreateThumbnail()

#---------------hight and lock Attribute-----------
import pkmel.rigTools as rigTools
reload( rigTools )
    rigTools.hideControl()
    
#--------------- DUPLICATE AND CLEAN GEO----------   
import pkmel.rigTools as rigTools
reload( rigTools )
rigTools.dupAndCleanSelected()

#---------------export skin Weight type seartch joint and old skinWeight-----------    
import pkmel.weightTools as wt
reload(wt)
wt.readSelectedWeight2(     weightFolderPath='', 
                            searchFor='', 
                            replaceWith='', 
                            prefix='', 
                            suffix='')
                            
#------------------------------  tools rig nonRoll -------------------------------------------
# group "nnRlRig_grp"
# pelvisNnRlProxySkin_jnt,
#   upLegNnRlProxySkinLFT_jnt
#     lowLegNnRlProxySkinLFT_jnt
#   upLegNnRlProxySkinRGT_jnt
#     lowLegNnRlProxySkinRGT_jnt

#   upLegLFT_jnt,
#     lowLegLFT_jnt,
#   upLegRGT_jnt,
#     lowLegRGT_jnt

import pkmel.aimRig as aimRig
reload( aimRig )

a = aimRig.NonRollRig(
                       parent='pelvisNnRlProxySkin_jnt' ,
                       animGrp='legNnrlRig_grp' ,
                       rootJnt='upLegLFT_jnt' ,
                       endJnt='lowLegLFT_jnt' ,
                       parentRoot='upLegNnRlProxySkinLFT_jnt' ,
                       parentEnd='lowLegNnRlProxySkinLFT_jnt' ,
                       name='skirt' ,
                       side='LFT' ,
                       aimAx='y+' ,
                       upAx='x-'
                   )
b = aimRig.NonRollRig(
                       parent='pelvisNnRlProxySkin_jnt' ,
                       animGrp='legNnrlRig_grp' ,
                       rootJnt='upLegRGT_jnt' ,
                       endJnt='lowLegRGT_jnt' ,
                       parentRoot='upLegNnRlProxySkinRGT_jnt' ,
                       parentEnd='lowLegNnRlProxySkinRGT_jnt' ,
                       name='skirt' ,
                       side='RGT' ,
                       aimAx='y-' ,
                       upAx='x+'
                   )  
                          
#------------------------getListFile  -------------------------
import UTAModul as uta
reload(uta)
import maya.cmds as cmds



# List the contents of the user's projects directory
#
	cmds.getFileList( folder=cmds.internalVar(userWorkspaceDir=True) )

# List all MEL files in the user's script directory
#
	cmds.getFileList( folder=cmds.internalVar(userScriptDir=True), filespec='*.mel' )

getFileList([filespec=string], [folder=string])

#------------------------create Detail Control -------------------------
import pkmel.detailControl as dtlCtrl
reload( dtlCtrl )

ctrl = dtlCtrl.controlOnSelectedEdge()
mc.select( ctrl[0] )
dtlCtrl.autoName(
                   charName='' ,
                   elem='lip' ,
                   side='RGT'
                )
                            
dtlCtrl.autoNameList(
                    charname='' ,
                    elem='beltDtl' ,
                    side='RGT'
                  )

                  
                  
#-------------------Tools Brush Nu---------------------
import averageVertexSkinWeightBrush
reload(averageVertexSkinWeightBrush)
averageVertexSkinWeightBrush.paint()

#-------------------run Scrip horsePonyA---------------------
sys.path.append(r'P:\Lego_Friends2015\asset\3D\character\animal\frd_horsePonyA\rig\maya\data')
import horsePonyA  as hp
reload(hp)
hp.main()

#-------------------run Scrip horsePonyA---------------------
import sys
sys.path.append(r'Y:\SHARE\peck')

import genGalHairAdjust
reload( genGalHairAdjust )
genGalHairAdjust.main()

#------------------------delete camera Perse1 -------------------------
import maya.cmds as mc
# cams = mc.ls(sl=True)
for cam in cams:
    mc.camera(cam,e=True,startupCamera=False)
    mc.delete(cam)
    print 'delete %s' %(cam)
    
    
scOpt_performOneCleanup( { "unknownNodesOption" } );
Optimize Scene Size Summary:
    
#------------------------------Copy  nonlinear Node -----------------------------
from nuTools import misc
reload(misc)
misc.addDeformerMember(deformerName='wire2')
# '' put nodeName


sys.path.append(r'O:\systemTool\python\UTAModul')
import UTAModul.UTACore as uc
reload(uc)
uc.UTADoZroGroup()
uc.zeroGroup()

#------------------------------Cleanup File -----------------------------
from nuTools.pipeline import pipeTools
reload(pipeTools)
pipeTools.deleteAllTurtleNodes()
pipeTools.deleteExtraDefaultRenderLayer()
pipeTools.parentPreConsObj()
pipeTools.turnOffScaleCompensate()

#py--------------------------------Publish for New BaseRig Friends 2017-----------
#Modul
from nuTools.pipeline import pipeTools
reload(pipeTools)
from pkmel import ctrlShapeTools as pcst
reload(pcst)

#--- Pipeline publish
pipeTools.importAllRefs()
pipeTools.removeAllNameSpace()
pipeTools.parentPreConsObj()

pipeTools.connectDualSkeleton(False, False)

pipeTools.connectAllMeshInGrp()

#--- Controller
pcst.readAllCtrl()

pcst.writeAllCtrl()

#--------------------------------------

sys.path.append(r'P:\Lego_FRD360\asset\3D\character\main\frd_miaHelmet\rig\rig_md\data')
import mia360 as cr
reload(cr)
cr.main()



import pkmel.mainGroup as pmain
reload( pmain )

mainGroup = pmain.MainGroup(rotateOrder = 'zxy')
rigTools.nodeNaming(
							mainGroup ,
							charName = '' ,
							elem = '' ,
							side = ''	
              )
#--------------------------------------UTAGeoVis connect------------------------------------				
import sys
sys.path.append(r'O:\studioTools\maya\python\tool\UTATools')
import UTACore as uc
reload (uc)	
uc.UTAGeoVis	

#-------------------Fix dublicateName-------------------------------------------------- 
##import nu basic modules
import maya.cmds as mc
import pymel.core as pm

#basic modules
from nuTools import misc
from nuTools import controller as ctrl
from nuTools import config
from nuTools import ui
misc.duplicateNameDetect( createSet=False, 
                          autoRename=True, 
                          typ=None)


#------------------- Converter EXR to PNG --------------------------------------------------  

#O:\studioTools\tools\exrToPngConverter

#--------------------run fix cloth (nonRoll Rbb)
from nuTools.rigTools import proc
reload(proc)
proc.ribbonNonRollRig(  rbnCtrl=None, 
                        elem='', 
                        side='', 
                        skinGrp='skin_grp', 
                        animGrp='anim_grp',
                        rbnAttr='detail', 
                        visAttrName='nonRollDetail', 
                        ctrlColor='pink')
#select คอนโทรลริบบ้อน อันกลาง (เหลี่ยมๆ สีเหลืองๆไว้ ก่อนรัน)

#-----------------fix rotateOrder = 2 (zxy)--------------------------------------------------------
files =mc.ls('spine*_ctrl','neck*_ctrl','head*_ctrl','pelvis*_ctrl')
print files
for each in files:
    chang =mc.setAttr('%s.rotateOrder' %each,2)
    print chang
    

#--------------------- snap Pivot -------------------------------------------------------------------
misc.snapPivot()   
#--------------------- Connect DuleSkeleton same ProxySkin ----------------------------------------------------------------
from nuTools.pipeline import pipeTools

pipeTools.importAllRefs()
pipeTools.removeAllNameSpace()
pipeTools.deleteAllTurtleNodes()
pipeTools.deleteAllUnknownNodes()
pipeTools.deleteExtraDefaultRenderLayer()
pipeTools.deleteUnusedNodes()
pipeTools.parentPreConsObj()

# run scrip after import reference
pipeTools.connectDualSkeleton(False, False) 

#--------------------- Connect eyeLid to control (before Importj)----------------------------------------------------------------
from nuTools.pipeline import pipeTools
pipeTools.connectProxyCtrl(True)

#------------------------create WeaponRig----------------------------
sys.path.append(r'P:\Lego_FRDCG\asset\3D\character\support')
import olaBaby as utt
reload(utt)
utt.weaponRig2()

sys.path.append(r'P:\Lego_FRDCG\asset\3D\character\extra\frd_carnivorousMan\rig\rig_md\data')
import carnivorousMan2 as uc
reload(uc)
uc.main()


#--------------------create joint for wrist-------------
#1.create joint at forearmRbnDtl4LFT_jnt,RGT
#2.craate group for joint
#3.run scrip
#4.parent forearmRbnDtl4LFT_jnt to zroJnt New
import maya.cmds as mc
import pkmel.rigTools as rt
sys.path.append(r'O:/studioTools/maya/python/tool/lagacy/UTAModul')
import UTAModul.UTACore as uc
obj = mc.ls ( 'neckRotJntSkinZro_grp',
              'neckRotJntZro_grp',          
              'neckRot_jnt',
              'ctrlRig:spine3_jnt',
              'ctrlRig:head1_jnt',
              'ctrlRig:skin_grp'
      )
print obj   


#------------Mel     set T Post A Post for Character Friends-------------------
//////set
setAttr "rig:rootGmbl_ctrl.translateY" -0.1;

setAttr "rig:legIkConLFT_ctrl.translateX" 1.2;
setAttr "rig:legIkConLFT_ctrl.rotateY" 17;

setAttr "rig:legIkConRGT_ctrl.translateX" -1.2;
setAttr "rig:legIkConRGT_ctrl.rotateY" -17;

/////defalt
setAttr "rig:rootGmbl_ctrl.translateY" 0;

setAttr "rig:legIkConLFT_ctrl.translateX" 0;
setAttr "rig:legIkConLFT_ctrl.rotateY" 0;

setAttr "rig:legIkConRGT_ctrl.translateX" 0;
setAttr "rig:legIkConRGT_ctrl.rotateY" 0;     

#------------------ rig hiHeelModRig ---------------------------
from nuTools.rigTools import hiHeelModRig
reload(hiHeelModRig)

# reference in the locators, place them
locs = hiHeelModRig.referenceGuide()

# ns = namespace of the referenced base rig
hiHeelModRig.moveRigPivot(locs, ns='frd_teenFMd_rigMaster:')

# select bodyBase_ply, bodyCorrective_ply(from model team) and bodyMod_ply
hiHeelModRig.doInvertMesh(  deformed=None, 
                            corrective=None, 
                            target=None)

#-------------- fix model can't assign shade (export OBJ and import OBJ)----------------
from nuTools import legoCleanup as lc
reload(lc)
lc.swapImportObj()

#------------- Change Type Contrl -----------------
from nuTools import misc
reload(misc)
misc.redraw('crossCircle')

#------------ create polvector -----------------
from nuTools import misc
  # open polvector
misc.getPoleVectorPosition(createLoc=True)
  # create point
misc.getPoleVectorPosition( createLoc=True, 
                            offset=3)
#------------MEL chaing aim and up --------------
cometJointOrient

#-----------------------------snapSkeleton  -----------------------------
pipeTools.turnOffScaleCompensate()
misc.snapSkeleton(namespace='frd_teenFMdAndrea_rig_ctrlRig:')
#>Open > def
#>>reference ctrlRig
#>>> chang Namespace
#>>>>(selected) rootProxySkin_jnt > select hierarchy
#>>>>>(run scrip) 
    #misc.snapSkeleton(namespace='frd_teenFMd_rig_skel_2018:') 
    #pipeTools.turnOffScaleCompensate()

#--------------------- Create btwJnt ----------------------------------------------------------------
from nuTools.rigTools import proc
reload(proc)

proc.btwJnt(pointConstraint=True)

#--------------------- Create facialDtl  NUNU----------------------------------------------------------------
from nuTools.rigTools import dtlRig as dr
reload(dr)

faceDtlRig = dr.DtlRig(   rivetMesh='headPosDtl_ply', 
                          skinMesh='headSkinDtl_ply', 
                          tempLocs=['eb1LFT_ply', ...], 
                          ctrlColor='red', 
                          ctrlShape='cube')
## put button C and move locator on edge

#--------------------- copy nameList past to tempLocs:(faceDtlRig)----------------------------------------------------------------
misc.selectionToClipboard()


#--------------------- Create facialDtl  loft on Surface----------------------------------------------------------------
# create surface from loft on real mesh
import pymel.core as pm
from nuTools.rigTools import dtlRig2 as dt
reload(dt)
dtlRig = dt.DtlRig2(pm.PyNode('frd_teenFMdAndrea_rig_master:upArmLFT_jnt'), # parent
            surf=pm.PyNode('clothes_lf'), # NURBS 
            wrapMesh=pm.PyNode('clothes02_ply'), # with real skin weight
            skinMesh=pm.PyNode('clothes02Wrap_ply'), # render mesh
         num=5, # number of control
         paramVOffset=3,  # create control every n edge
         paramU=1, # placement of control along NURBS width
         elem='arm',
         side ='LFT',
         ctrlColor='lightBlue', 
         ctrlShape='sphere')
dtlRig.rig()  


#--------------------- Create Dtl Control on bodyProxy(follicle) ----------------------------------------------------------------
from nuTools import misc
reload(misc)
misc.createFollicleFromPosition_Mesh(name='shirtDtlLFT',doConstraint=True)


#-------------------------Create Ribbon Jnt -+  ------------------------------------------------------
from nuTools.rigTools import ribbonRig as rbr
reload(rbr)

strapAnimGrp = 'strapAnim_grp'
strapUtilGrp = 'strapUtil_grp'
strapStillGrp = 'strapStill_grp'

baseJnt = 'strapA_jnt1'
tipJnt = 'strapB_jnt1'

# Set Option of Ribbon
strapRig = rbr.RibbonIkRig(     numJnt= 7 ,
                                aimAxis='+y',
                                upAxis='+z',
                                elem='strap',
                                side='',
                                parent=baseJnt,
                                animGrp=strapAnimGrp, 
                                utilGrp=strapUtilGrp, 
                                stillGrp=strapStillGrp)
                            
strapRig.rig()
strapRig.attach(    base=baseJnt,
                    tip=tipJnt, 
                    parent=baseJnt, 
                    tipParent=tipJnt,
                    autoTwist=True,
                    nonRoll=False, 
                    upAxis='+z', 
                    parentUpAxis='-z')


#ES--------------------------------code  loop trainStaion -------------------------------------------------
railwayMoveIk_ikh.offset = trainStationOffset_ctrl.trainOffset*0.001;
int $round = railwayMoveIk_ikh.offset;
if (railwayMoveIk_ikh.offset > 1)
  railwayMoveIk_ikh.offset = ((trainStationOffset_ctrl.trainOffset*0.001)-$round);

railwayMoveIk_ikh.offset = trainStationbogie1_loc.offset*0.001;
int $round = railwayMoveIk_ikh.offset;
if (railwayMoveIk_ikh.offset > 1)
  railwayMoveIk_ikh.offset = ((trainStationbogie1_loc.offset*0.001)-$round);

#PY-----------------------------PT Resolution Lod ---------------------------------------------------------
from tool.trackSystem import app
reload(app)
myApp = app.TrackSystemUI()
myApp.show()

Y:/USERS/Roheem/Friends2018/mapHLCAll02.ma
P:/Lego_FRD/asset/3D/setDress/map/frd_mapOldTown/model/model_lo/scenes/frd_mapOldTown_model_lo_v010_gpu.ma

#PY------------------------------RUN Between Jnt (Muscle)-----------------------------------------------------------
from nuTools.rigTools import proc
reload(proc)
def main() :
# Neck 
  proc.btwJnt(jnts=['spine5Sca_jnt', 'neck1_jnt', 'neckRbn_jnt'], pointConstraint=True)

# Arm LFT
  proc.btwJnt(jnts=['spine5Sca_jnt', 'clav1LFT_jnt', 'upArmLFT_jnt'], pointConstraint=True)
  proc.btwJnt(jnts=['clav1LFT_jnt', 'upArmLFT_jnt', 'upArmRbnDtl1LFT_jnt'], pointConstraint=True)
  proc.btwJnt(jnts=['upArmRbnDtl5LFT_jnt', 'forearmLFT_jnt', 'forearmRbnDtl1LFT_jnt'], pointConstraint=False)
  proc.btwJnt(jnts=['forearmRbnDtl5LFT_jnt', 'wristLFT_jnt', 'handLFT_jnt'], pointConstraint=True)

# Arm RGT
  proc.btwJnt(jnts=['spine5Sca_jnt', 'clav1RGT_jnt', 'upArmRGT_jnt'], pointConstraint=True)
  proc.btwJnt(jnts=['clav1RGT_jnt', 'upArmRGT_jnt', 'upArmRbnDtl1RGT_jnt'], pointConstraint=True)
  proc.btwJnt(jnts=['upArmRbnDtl5RGT_jnt', 'forearmRGT_jnt', 'forearmRbnDtl1RGT_jnt'], pointConstraint=False)
  proc.btwJnt(jnts=['forearmRbnDtl5RGT_jnt', 'wristRGT_jnt', 'handRGT_jnt'], pointConstraint=True)

# Leg LFT
  proc.btwJnt(jnts=['spine1Sca_jnt', 'upLegLFT_jnt', 'upLegRbnDtl1LFT_jnt'], pointConstraint=True)
  proc.btwJnt(jnts=['upLegRbnDtl5LFT_jnt', 'lowLegLFT_jnt', 'lowLegRbnDtl1LFT_jnt'], pointConstraint=False)
  proc.btwJnt(jnts=['lowLegRbnDtl5LFT_jnt', 'ankleLFT_jnt', 'ballLFT_jnt'], pointConstraint=True)

# Leg RGT
  proc.btwJnt(jnts=['spine1Sca_jnt', 'upLegRGT_jnt', 'upLegRbnDtl1RGT_jnt'], pointConstraint=True)
  proc.btwJnt(jnts=['upLegRbnDtl5RGT_jnt', 'lowLegRGT_jnt', 'lowLegRbnDtl1RGT_jnt'], pointConstraint=False)
  proc.btwJnt(jnts=['lowLegRbnDtl5RGT_jnt', 'ankleRGT_jnt', 'ballRGT_jnt'], pointConstraint=True)

#----------------------------------connection A to B-------------------------------------------------
string $sels[] =`ls -sl`;
connectAttr ($sels[0] + ".translateX")($sels[1] + ".translateX");
connectAttr ($sels[0] + ".translateY")($sels[1] + ".translateY");
connectAttr ($sels[0] + ".translateZ")($sels[1] + ".translateZ");

connectAttr ($sels[0] + ".rotateX")($sels[1] + ".rotateX");
connectAttr ($sels[0] + ".rotateY")($sels[1] + ".rotateY");
connectAttr ($sels[0] + ".rotateZ")($sels[1] + ".rotateZ");

connectAttr ($sels[0] + ".scaleX")($sels[1] + ".scaleX");
connectAttr ($sels[0] + ".scaleY")($sels[1] + ".scaleY");
connectAttr ($sels[0] + ".scaleZ")($sels[1] + ".scaleZ");

#-----------------------Create beTween on ctrlRig Joint ---------------------------------
from pkmel import betweenDtl  as bd
reload(bd)
bd.main()
#-----------------------Create beTween on proxySkin Joint ---------------------------------
from pkmel import betweenDtlProxySkin  as bd
reload(bd)
bd.main()


# -------------------------- nu FK ----------------------------------
from nuTools.rigTools import fkRig as nfk
reload(nfk)
fkRig = nfk.FkRig(elem='hairBack',
                side='',
                jnts=pm.selected(), ctrlShp='circle')
fkRig.rig()

# -------------------------- skirtRig FK ----------------------------------
from nuTools.rigTools import skirtRigFk as sra
reload(sra)
sra.main()

#----------------------------addDetailCtrl_Riff-----------------------------
import sys
sys.path.append(r'P:\_ken\python\utamel')
import addDetailCtrlUi as pp

reload(pp)
pp.runUI()

#----------------rotate Axis for dtlControl  @Riff----------------------
import sys
sys.path.append(r'P/lib/local/utaTools')
import utapy.connectDtlAxis as cde    
reload(cde)      
    #------ create node test for dtlControl --------                      
cde.conDtl(elem='test', valueConY=180) 
    #------ clean node test ------------------
cde.deleteConDtl()  

#-----------TurnOff  Segment Scale Compensete ------------------------------------
import sys
sys.path.append(r'P:\lib\local\utaTools')
import utapy.turnOffSSC  as ssc
reload(ssc)
ssc.turnOff()

#--------------cleanResorutions (CompRig) ------------------------------------------
import sys 
sys.path.append(r'P:\lib\local\utaTools')
sys.path.append(r'P:\lib\local\utaTools\pkrig')
import utapy.cleanResolution  as cr
reload(cr)
import rigTools  
reload(rigTools)
# cleanSkin
cr.clean()

# cleanBsh
cr.cleanBsh()

# cleanDtl
cr.cleanDtl()

# cleanCombRig
cr.cleanCombRig()

cr.clean()

#------------------------copy skinWeight at vertixt ---------------------
from lpRig import rigTools as lrr
reload(lrr)

# CopyWeight
lrr.copyWeightBasedOnWorldPosition()

# -------------------lis  file Path on open ------------------------------
# mc.file( s=True , f=True , type='mayaAscii' )

# ---------------------connectEyeLidsFollow ------------------------------
import sys
sys.path.append(r'P:\lib\local\utaTools\utapy')
import connectEyeLidFollow as cef
reload(cef)
cef.eyeLidFollow()

#------------------------check symMesh----------------------------------------------
import sys
sys.path.append(r"P:\lib\local\utaTools\nuTools\util")
import symMeshReflector2 as smr
reload(smr)
smrObj = smr.SymMeshReflectors()
smrObj.UI()

#-----------------------------------------
from lpRig import rigTools as lrr
lrr.rivetToSeletedEdge('ShldRvtPostD')


# HSW------------------------------------
from lpRig import headDeformRig
reload(headDeformRig)
headDeformRig.main()

#----------selBkmkr----------------------
from lpRig import selBkmkr
selBkmkr.run()

#---------QuickOrient-----------------
# run SYS
lrr.quickOrient()

# write skinWeight
from ppmel import weightTools
reload( weightTools )

weightTools.writeSelectedWeight()

# read skinWeight
from ppmel import weightTools
reload( weightTools )

weightTools.readSelectedWeight()


# blendShapeAdder
from lpRig import blendShapeAdder
blendShapeAdder.run()

# localControlMaker
from lpRig import localControlMaker
localControlMaker.run()


import maya.cmds as mc
reload(mc)
def fixJawMthRigTZ():
    lists = mc.ls("JawMthRig_Ctrl", "JawMthRigJntTzColl_Pma")
    try:
        mc.disconnectAttr("%s.translate.translateY" % lists[0], "%s.input1D[0]" % lists[1])
        mc.connectAttr("%s.translate.translateZ" % lists[0], "%s.input1D[0]" % lists[1])
    except: pass

# Split Loop  ':' , [-1]
for i in grpRefs:
    names = i.split(':')[-1]
    grpRef.append(names)
    
print grpRef

# Replace Loop '_Crv' , ''  
lists = [a.replace('_Crv', '') for a in sel]

# Add localRig #shel
from lpRig import localControlMaker
localControlMaker.run()

## collections Exsample ----------------------------------------------------
## collections Exsample ----------------------------------------------------
from collections import OrderedDict
od = OrderedDict()
a['a'] = 'A'
a['b'] = 'B'
a['c'] = 'C'
b = {'a': 'A', 'b': 'B', 'c': 'C'}
print a 
print b
(14:36) Chanon Vilaiyuk: from collections import OrderedDict
a = OrderedDict()
a['a'] = ['A', 'B']
a['b'] = 'B'
a['c'] = 'C'
b = {'a': 'A', 'b': 'B', 'c': 'C'}
print a['a'][0]
print b

## Convert ABC to Bsh
## mel
#cgTkShotSculpt

## Set default only monitor
import maya.cmds as mc
for each in mc.lsUI(controls = True , windows = True , editors = True):
    try:mc.window(each, e = True, tlc = (0,0))
    except:pass