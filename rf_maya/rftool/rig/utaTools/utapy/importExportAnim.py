import os

try: 
    import maya.cmds as mc
    import maya.mel as mm
    isMaya = True

except ImportError: 
    isMaya = False

import logging
def export_anim(node, path): 

    # if not os.path.exists(os.path.dirname(path)): 
    #     print 'x'
    #     os.makedirs(os.path.dirname(path))
    # else:
    #     print path, 'path'
    print node, '..node..'
    print path, '..path..'

    mc.select(node)
    result = mc.file(path, f=True, options='precision=8;intValue=17;nodeNames=1;verboseUnits=0;whichRange=1;range=0:10;options=keys;hierarchy=below;controlPoints=0;shapes=1;helpPictures=0;useChannelBox=0;copyKeyCmd=-animation objects -option keys -hierarchy below -controlPoints 0 -shape 1', typ='animExport', pr=True, es=True)
    print node, 'Node'
    print result, 'resule'
    return result

def import_anim(node, path): 
    mc.select(node)
    result = mc.file(path, i=True, type='animImport', ignoreVersion=True, options='targetTime=4;copies=1;option=replace;pictures=0;connect=0', pr=True)
    return result



import maya.cmds as cmds
import maya.cmds as cmds
import json

#store
def saveJson(path, data):
    with open(path, 'w') as outfile:
      json.dump(data, outfile)

#load
def loadJson(path):
    with open(path, 'rb') as fp:
        data = json.load(fp)
        return data
# list of objects
objectNames = ['pSphere1']
# list of attributes
attributes = ['rotateY']
# dictionary to store data
data = {}
# this maya path for data defined by your setProject
path = cmds.workspace(expandName = 'data')
# i like to put json as extension but you can put any extension you want or even none
filename = 'attributes.json'
fullpath = path + '/' + filename

# beginning the storage
for o in objectNames:
    for attr in attributes:
        # all the time value
        frames = cmds.keyframe(o, q=1, at=attr)
        # all the attribute value associated
        values = cmds.keyframe(o, q=1, at=attr, valueChange=True)
        # store it in the dictionary with key 'objectName.attribute'
        data['{}.{}'.format(o, attr)]=zip(frames, values)
        # clear the keys on the object
        cmds.cutKey( o, attribute=attr, option="keys") 

# save the file data
saveJson(fullpath, data)

# ============================================================
# LOAD DATA BACK
# =============================================================

# load the dictionary back
new_data = loadJson(fullpath)
# set values again
for k, v in new_data.iteritems():
    for t, x in v:
        o, a = k.split('.')
        cmds.setKeyframe( o, time=t, attribute=a, value=x)