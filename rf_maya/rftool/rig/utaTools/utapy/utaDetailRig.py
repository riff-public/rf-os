import maya.cmds as mc
reload(mc)
from ncmel import detailCtrlRig
reload(detailCtrlRig)
from utaTools.utapy import DeleteGrp
reload(DeleteGrp)

def runDetailRig(shape = 'cube',mirror = 'OFF', charSize = 1, *args):
    listsObj = mc.ls(sl = True)
    TmpGrp = 'DtlNrbTmp_Grp'
    detailNrb = mc.createNode ('transform', n = 'NrbDtlRig_Grp')
    dtlRigStill = mc.createNode ('transform', n = 'DtlRigStill_Grp')
    dtlRigSkin = mc.createNode ('transform', n = 'DtlRigSkin_Grp')

    for each in listsObj:
        #dtlRigStill_Grp = mc.duplicate(base, rr=True, n = '%s%s' % (each, 'Tmp'))
        #mc.delete(mc.parentConstraint(each, dup, mo = False))
        #mc.parent(dup, nrbTmpGrp)
        # NameSplit
        tmpSpl =each.split('Tmp')
        sideSpl = tmpSpl[0].split('_')
        name = sideSpl[0]
        side = sideSpl[1]
        last = sideSpl[2]
        
        # Select Edge
        mc.select('%s.v[1]' % each)
        mc.select('%s.v[2]' % each,add = True)
        
        # Create Detail
        detailCtrlRig.addDtlCtrlNurb( name = name , 
                        side = side ,
                        shape = shape , 
                        mirror = mirror,
                        charSize = charSize )    
        print 'Create >>', '%sDtl_%s_ctrl' % (name, side)
        mc.parent(mc.ls('%sDtl_%s_%s' % (name, side, last)), detailNrb)     
        mc.parent(mc.ls('%sDtlJntZro_%s_Grp' % (name, side)), dtlRigSkin)

    dtlGrpHiMidLow()
    mc.parent(detailNrb, dtlRigStill)  
    DeleteGrp.deleteGrp(obj = TmpGrp)
    # mc.delete('dtlNrbTmp_Grp')

def dtlGrpHiMidLow(*args):
    dtlRigCtrlHi = mc.createNode ('transform', n = 'DtlRigCtrlHi_Grp')
    dtlRigCtrlMid = mc.createNode ('transform', n = 'DtlRigCtrlMid_Grp')
    dtlRigCtrlLow = mc.createNode ('transform', n = 'DtlRigCtrlLow_Grp')
    dtlRigCtrlMidLow = mc.createNode('transform',n = 'DtlRigCtrlMidLow_Grp')

    listDtlRigHi = [    'EyeBrowADtlCtrlZro_L_Grp',
                        'EyeBrowADtlCtrlZro_R_Grp',
                        'EyeBrowBDtlCtrlZro_L_Grp',
                        'EyeBrowBDtlCtrlZro_R_Grp',
                        'EyeBrowCDtlCtrlZro_L_Grp',
                        'EyeBrowCDtlCtrlZro_R_Grp',
                        'EyeBrowDtlCtrlZro_C_Grp',
                        'EyeLid1DtlCtrlZro_L_Grp',
                        'EyeLid1DtlCtrlZro_R_Grp',
                        'EyeLid2DtlCtrlZro_L_Grp',
                        'EyeLid2DtlCtrlZro_R_Grp',
                        'EyeLid3DtlCtrlZro_L_Grp',
                        'EyeLid3DtlCtrlZro_R_Grp',
                        'EyeLid4DtlCtrlZro_L_Grp',
                        'EyeLid4DtlCtrlZro_R_Grp',
                        'EyeLid5DtlCtrlZro_L_Grp',
                        'EyeLid5DtlCtrlZro_R_Grp',
                        'EyeLid6DtlCtrlZro_L_Grp',
                        'EyeLid6DtlCtrlZro_R_Grp',
                        'EyeLid7DtlCtrlZro_L_Grp',
                        'EyeLid7DtlCtrlZro_R_Grp',
                        'EyeLid8DtlCtrlZro_L_Grp',
                        'EyeLid8DtlCtrlZro_R_Grp',
                        'EyeLid9DtlCtrlZro_L_Grp',
                        'EyeLid9DtlCtrlZro_R_Grp',
                        'EyeLid10DtlCtrlZro_L_Grp',
                        'EyeLid10DtlCtrlZro_R_Grp',
                        'EyeLid11DtlCtrlZro_L_Grp',
                        'EyeLid11DtlCtrlZro_R_Grp',
                        'EyeLid12DtlCtrlZro_L_Grp',
                        'EyeLid12DtlCtrlZro_R_Grp',
                        'CheekADtlCtrlZro_L_Grp',
                        'CheekADtlCtrlZro_R_Grp',
                        'CheekBDtlCtrlZro_L_Grp',
                        'CheekBDtlCtrlZro_R_Grp',
                        'CheekCDtlCtrlZro_L_Grp',
                        'CheekCDtlCtrlZro_R_Grp',
                        'NsADtlCtrlZro_C_Grp',
                        ]

    listDtlRigMid =     [
                            'Mouth1DtlCtrlZro_C_Grp',
                            'Mouth2DtlCtrlZro_L_Grp',
                            'Mouth2DtlCtrlZro_R_Grp',
                            'Mouth3DtlCtrlZro_L_Grp',
                            'Mouth3DtlCtrlZro_R_Grp',
                            'Mouth4DtlCtrlZro_L_Grp',
                            'Mouth4DtlCtrlZro_R_Grp',
                            'Mouth5DtlCtrlZro_L_Grp',
                            'Mouth5DtlCtrlZro_R_Grp',
                            'NsBDtlCtrlZro_C_Grp',
                            'NsBDtlCtrlZro_L_Grp',
                            'NsBDtlCtrlZro_R_Grp',
                            'NoseBshDtlCtrlZro_C_Grp'
                        ] 

    listDtlRigLow =     [
                            'Mouth7DtlCtrlZro_L_Grp',
                            'Mouth7DtlCtrlZro_R_Grp',
                            'Mouth9DtlCtrlZro_L_Grp',
                            'Mouth9DtlCtrlZro_R_Grp',
                            'Mouth10DtlCtrlZro_L_Grp',
                            'Mouth10DtlCtrlZro_R_Grp',
                            'Mouth11DtlCtrlZro_L_Grp',
                            'Mouth11DtlCtrlZro_R_Grp',
                            'Mouth12DtlCtrlZro_L_Grp',
                            'Mouth12DtlCtrlZro_R_Grp',
                            'Mouth13DtlCtrlZro_C_Grp',
                            'ChinDtlCtrlZro_C_Grp',
                            'PuffDtlCtrlZro_R_Grp',
                            'PuffDtlCtrlZro_L_Grp'
                        ]  
    
    listDtlRigMidLow = [
                            'MouthCnrBshDtlCtrlZro_L_Grp',
                            'MouthCnrBshDtlCtrlZro_R_Grp',
                            'MouthUprBshDtlCtrlZro_C_Grp',
                            'MouthLwrBshDtlCtrlZro_C_Grp'
                        ]
    # mc.parent(mc.ls('%sDtlCtrlZro_%s_Grp' % (name, side)), dtlRigCtrl)
    # mc.parent(mc.ls('Cheek*DtlCtrlZro*Grp', dtlRigCtrlMid)
    mc.parent(mc.ls(listDtlRigHi, dtlRigCtrlHi))
    mc.parent(mc.ls(listDtlRigMid, dtlRigCtrlMid))
    mc.parent(mc.ls(listDtlRigLow, dtlRigCtrlLow))
    mc.parent(mc.ls(listDtlRigMidLow, dtlRigCtrlMidLow))