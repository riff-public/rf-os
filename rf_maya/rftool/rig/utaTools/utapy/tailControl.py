# sys.path.append(r'O:\Pipeline\legacy\lib\local\lpRig')
# sys.path.append(r'O:\Pipeline\legacy\lib\local\utaTools\utapy')
import maya.cmds as mc
reload(mc)
import rigTools 
reload(rigTools)
import lpRig.core as lrc
reload(lrc)

def tailControl(elem = '', zro2='',crvType='',jntsCount='', gimbal =True, color = 'red'):
    jntsCounts = int(jntsCount)
    ix= 0
    controlAll = []
    for x in range(0, jntsCounts):

        # Create Control
        ctrls = rigTools.jointControl(crvType=crvType)
        ctrlsName = mc.rename(ctrls,('%s%s_Ctrl'% (elem,ix+1)))
        ctrlsZro1 = mc.group(em=True,n=('%s%sCtrl_Grp' % (elem,ix+1)))
        ctrlsZro2 = mc.group(em=True,n=('%s%sCtrl%sZro_Grp' % (elem,ix+1,zro2)))

        # Set Color Controler
        if color == 'red':
            mc.setAttr('%s.overrideEnabled' % ctrlsName, 1)
            mc.setAttr('%s.overrideColor' % ctrlsName, 13)
        if color == 'yellow':
            mc.setAttr('%s.overrideEnabled' % ctrlsName, 1)
            mc.setAttr('%s.overrideColor' % ctrlsName, 17)
        if color == 'green':
            mc.setAttr('%s.overrideEnabled' % ctrlsName, 1)
            mc.setAttr('%s.overrideColor' % ctrlsName, 14)
        if color == 'blue':
            mc.setAttr('%s.overrideEnabled' % ctrlsName, 1)
            mc.setAttr('%s.overrideColor' % ctrlsName, 18)

        mc.parent(ctrlsName, ctrlsZro1)
        mc.parent(ctrlsZro1, ctrlsZro2)

        if gimbal ==True:
            # Add Gimbal
            gmbl = lrc.addGimbal(ctrlsName)
            gmbl.name = '%s%sGmbl_Ctrl' % (elem,ix+1)
            gmbl.scaleShape(0.75)
            ix+=1
            controlAll.append(ctrlsZro2)
    return controlAll


