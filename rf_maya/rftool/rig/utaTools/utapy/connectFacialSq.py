import maya.cmds as mc
reload(mc)
import getAttributeClass  as gac
reload(gac)
abdClass = gac.AttributeData()

#----------------------------------------#
#connectFacialSqRun(name = "sq")---------#
#----------------------------------------#

def connectFacialSqRun (name):
    lists = mc.ls("HdMidHdDfmRig_Ctrl", "HdUpHdDfmRig_Ctrl", "HdLowHdDfmRig_Ctrl")
    #lists = mc.ls("*:HdMidHdDfmRig_Ctrl", "*:HdUpHdDfmRig_Ctrl", "*:HdLowHdDfmRig_Ctrl")
    listAttrs = abdClass.listAttributeKeyableAll(obj = lists)
    #print listAttr
    add = ("Mid", "Up", "Low")
    newObj = []
    attrMid = ('translateX', 'translateY', 'translateZ')
    attrUpLow = ( 'translateX', 'translateY', 'translateZ', 'autoSquash' , 'squash', 'slide')
    grpRig = mc.group(em=True)
    grpRigName = mc.rename(grpRig, "facial%sRig_Grp" % name.title()) 
    i=0
    for each in lists:     
        hdD = mc.duplicate(each)
        ctrlName = mc.rename (hdD , "%s%sHdDfm_Ctrl" % (name, add[i]))
        grp = mc.group(em=True)
        grpName = mc.rename(grp, "%s%sHdDfmZro_Grp" % (name, add[i]))
        mc.delete(mc.pointConstraint(ctrlName, grpName))
        mc.parent(ctrlName, grpName)  
        
        for attr in attrMid:
            if "Mid" in ctrlName:
                mc.connectAttr("%s.%s" % (ctrlName, attr), "%s.%s" % (each, attr))
        else: pass
        if "Up" in ctrlName:
            for attr in attrUpLow:
                mc.connectAttr("%s.%s" % (ctrlName, attr), "%s.%s" % (each, attr))
        else: pass
        if "Low" in ctrlName:
            for attr in attrUpLow:
                mc.connectAttr("%s.%s" % (ctrlName, attr), "%s.%s" % (each, attr))
        else: pass
        mc.parent(grpName, grpRigName)
        i = i+1
    return grpName