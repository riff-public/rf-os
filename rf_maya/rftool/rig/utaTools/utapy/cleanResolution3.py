import sys
import maya.cmds as mc
import maya.mel as mm
from utaTools.pkrig import rigTools
reload(rigTools)
import maya.mel as mel
from utaTools.utapy import turnOffSSC 
reload(turnOffSSC)
from utaTools.utapy import utaCore as uc
reload(uc)
from utaTools.utapy import eyelidFollowConnect as efc 
reload(efc)
from utaTools.utapy import getAttributeClass  as gac
reload(gac)
abdClass = gac.AttributeData()
from utaTools.utapy import cleanUp 
reload(cleanUp)

def clean():
    # import reference and delete nameSpace
    print "## Import Reference and Delete 'Delete_Grp'"
    rigTools.importRigElementDeleteGrp()
    # delete unkownode
    print "## Delete 'TurtleDefaultBakeLayer'"
    try:
        sel = mc.ls('TurtleDefaultBakeLayer')
        mc.lockNode(sel,lock=False)
        mc.delete(sel)
    except: pass

    # TurnOff SSC
    print "## TurnOff 'Segment Scale Compensate'"
    turnOffSSC.turnOff()

    # delete vraySettings
    print "## Delete 'vraySettings'"
    try:
        sel = mc.ls('vraySettings')
        mc.lockNode(sel,lock=False)
        mc.delete(sel)
    except: pass

    # clean Camera
    print "## Delete 'Camera Not Yes Default'"
    try:
        cleanUp.clearCameraDefault()
    except: pass

    # try:
    #     for cam in cams:
    #         mc.camera(cam,e=True,startupCamera=False)
    #         mc.delete(cam)
    #         print 'delete %s' %(cam)
    # except: pass

    # delete  inheritsTransform
    print "## TurnOff 'inheritsTransform'"
    listInherCtrlRef = mc.ls('*:cheek*DtlCtrlZro_*_grp','*:eye*DtlCtrlZro_*_grp','*:mouth*DtlCtrlZro_*_grp','*:nose*DtlCtrlZro_*_grp','*:cheekDtlCtrlZro_*_grp')
    listInherJntRef = mc.ls('*:cheek*DtlJntZro_*_grp','*:eye*DtlJntZro_*_grp','*:mouth*DtlJntZro_*_grp','*:nose*DtlJntZro_*_grp','*:cheekDtlJntZro_*_grp')
    listInherCtrl = mc.ls('cheek*DtlCtrlZro_*_grp','eye*DtlCtrlZro_*_grp','mouth*DtlCtrlZro_*_grp','nose*DtlCtrlZro_*_grp','cheekDtlCtrlZro_*_grp')
    listInherJnt = mc.ls('cheek*DtlJntZro_*_grp','eye*DtlJntZro_*_grp','mouth*DtlJntZro_*_grp','nose*DtlJntZro_*_grp','cheekDtlJntZro_*_grp')
    for each in listInherCtrlRef:
        if mc.objExists(each):
            mc.setAttr('%s.inheritsTransform' % each,0)
        else: pass
    for each in listInherCtrl:
        if mc.objExists(each):
            mc.setAttr('%s.inheritsTransform' % each,0)
        else: pass
    for each in listInherJntRef:
        if mc.objExists(each):
            mc.setAttr('%s.inheritsTransform' % each,0)
        else: pass
    for each in listInherJnt:
        if mc.objExists(each):
            mc.setAttr('%s.inheritsTransform' % each,0)
        else: pass

    # assign shader 'lambert1'
    print "## Assign 'lambert1'"
    lisObj = mc.ls('FacialBshStill_Grp','FacialBshStill_grp', 'FclRigStill_Grp', 'LocRigStill_Grp')
    if not lisObj == []:
        for each in lisObj:
            mc.select(each)
            mc.hyperShade(each, assign ='lambert1')
    # # clean unused Node 
    # mel.eval('MLdeleteUnused;')

    # cleanUp
    print "## Delete 'Layer'"
    cleanUp.clearLayer();
    print "## Delete 'UnuseAnim'"
    cleanUp.clearUnusedAnim();
    print "## Delete 'Unknownode'"
    cleanUp.clearUnknowNode();
    print "## Delete 'UnusedNode'"
    cleanUp.clearUnusedNode();
    print '## Scale Gimbal'
    rigTools.scaleGimbalControl()

    print '------ Clean Success!! --------' 
    print '--------    *(^O^)*    --------' 
    print '-------------------------------'

def cleanFacialSq():
    # delete unkownode
    print "## Delete 'TurtleDefaultBakeLayer'"
    try:
        sel = mc.ls('TurtleDefaultBakeLayer')
        mc.lockNode(sel,lock=False)
        mc.delete(sel)
    except: pass

    # import reference and delete nameSpace
    print "## Import Reference and Delete 'Delete_Grp'"
    rigTools.importRigElementDeleteGrp()

    # clean unused Node 
    # mel.eval('MLdeleteUnused;')
    print "## Delete 'UnusedNode'"
    cleanUp.clearUnusedNode();

    # TurnOff SSC
    print "## TurnOff 'Segment Scale Compensate'"
    turnOffSSC.turnOff()

    # delete vraySettings
    print "## Delete 'vraySettings'"
    try:
        sel = mc.ls('vraySettings')
        mc.lockNode(sel,lock=False)
        mc.delete(sel)
    except: pass


    # #delete layer
    print "## Delete 'Layer'"
    cleanUp.clearLayer();
    # try:
    #     dispLayerStates = [] # Create array to store layer states
    #     dispLayers = mc.ls(type = 'displayLayer')
    #     # print dispLayers
    #     for lyr in dispLayers:
    #         dispLayerStates.append([lyr,mc.getAttr('%s.visibility' % lyr)])
    #             # Restore layer visibility
    #             #return dispLayerStates
    #             #loop = 0
    #     for lyr in dispLayers:
    #         if lyr != 'displayLayer':
    #             #loop += 1
    #             mc.delete(lyr)
    # except:
    #     print "'displayLayer' can't Delete"
       

    # check facialSqStill_Grp  and create group 
    listFacialSq = mc.ls('HdDfmRigCtrl_Grp', 'HdDfmRigJnt_Grp', 'HdDfmRigSkin_Grp' , 'HdDfmRigStill_Grp')
    facialSqStill = 'facialSqStill_Grp'
    try:
        for each in listFacialSq:      
            if mc.objExists (each)==True:
                if mc.objExists (facialSqStill)==True:
                    print  "done"
                    pass
                else:
                    sqStill = mc.group(em=True)
                    sqStillName = mc.rename(sqStill, "facialSqStill_Grp")
                    #mc.parent (each, sqStillName)
                ken = mc.parent (each, sqStillName)
                #print ken, "done"
            else: 
                print "------------//--------------"           
    except: pass

    #     listFacialSq = mc.ls('HdDfmRigCtrl_Grp', 'HdDfmRigJnt_Grp', 'HdDfmRigSkin_Grp' , 'HdDfmRigStill_Grp')
    #     facialSqStill = 'facialSqStill_Grp'
    # try:
    #     for each in listFacialSq:      
    #         if mc.objExists (each)==True:
    #             if mc.objExists (facialSqStill)==True:
    #                 pass
    #             else:
    #                 sqStill = mc.group(em=True)
    #                 sqStillName = mc.rename(sqStill, "facialSqStill_Grp")
    #             return sqStillName
    #             mc.parent (each, sqStillName)
    #         else: 
    #             print "------------This file don't have FacialSq--------------"           
    # except: pass

    #check headSqBsGeo_Grp
    facialSqGeo = mc.ls("facialSqGeoZro_Grp")
    for each in facialSqGeo:
        if mc.objExists(each)==True:
            pass
        else: 
            facialSqGrp = mc.group(em=True)
            facialSqGrpNew = mc.rename(facialSqGrp, "facialSqGeoZro_Grp")
    print '------ Clean Success!! --------' 
    print '--------    *(^O^)*    --------' 
    print '-------------------------------'

def cleanBsh():
    # delete unkownode
    print "## Delete 'TurtleDefaultBakeLayer'"
    try: 
        sel = mc.ls('TurtleDefaultBakeLayer')
        mc.lockNode(sel,lock=False)
        mc.delete(sel)
    except: pass

    # delete vraySettings
    print "## Delete 'vraySettings'"
    try:
        sel = mc.ls('vraySettings')
        mc.lockNode(sel,lock=False)
        mc.delete(sel)
    except: pass

    # # delete layer
    print "## Delete 'Layer'"
    cleanUp.clearLayer();
    # try:
    #     dispLayerStates = [] # Create array to store layer states
    #     dispLayers = mc.ls(type = 'displayLayer')
    #     # print dispLayers
    #     for lyr in dispLayers:
    #         if mc.objExists(lyr):
    #             dispLayerStates.append([lyr,mc.getAttr('%s.visibility' % lyr)])
    #             # Restore layer visibility
    #             #return dispLayerStates
    #             #loop = 0
    #     for lyr in dispLayers:
    #         if lyr != 'displayLayer':
    #             #loop += 1
    #             mc.delete(lyr)
    # except:
    #     print "'displayLayer' can't Delete"

    # import reference and delete nameSpace
    print "## Import Reference and Delete 'Delete_Grp'"
    rigTools.importRigElementDeleteGrp()

    # assign shader 'lambert1'
    print "## Assign 'lambert1'"
    lisObj = mc.ls('FacialBshStill_Grp','FacialBshStill_grp')
    for i in range(len(lisObj)):
        mc.select( lisObj[i] )
        mc.hyperShade(lisObj[i], assign ='lambert1')

    # clean unused Node 
    # mel.eval('MLdeleteUnused;')
    print "## Delete 'UnusedNode'"
    cleanUp.clearUnusedNode();


    # TurnOff SSC
    print "## TurnOff 'Segment Scale Compensate'"
    turnOffSSC.turnOff()

    # clean Delete_grp or 
    try:
        delGrp = mc.ls('Delete_Grp', 'Delete_grp')
        mc.delete(delGrp)
    except: pass

    # clean Camera
    print "## Delete Not Yes Default Camera"
    try:
        uc.cleanCamera()
    except: pass

    # # clean Camera
    # try:
    #     for cam in cams:
    #         mc.camera(cam,e=True,startupCamera=False)
    #         mc.delete(cam)
    #         print 'delete %s' %(cam)
    # except: pass

    print '----- Clean Bsh Success!! -----' 
    print '--------    *(^O^)*    --------' 
    print '-------------------------------'

# cleanDtl()
def cleanDtl():

    # delete unkownode
    print "## Delete 'TurtleDefaultBakeLayer'"
    try:
        sel = mc.ls('TurtleDefaultBakeLayer')
        mc.lockNode(sel,lock=False)
        mc.delete(sel)
    except: pass

    # delete vraySettings
    print "## Delete 'vraySettings'"
    try: 
        sel = mc.ls('vraySettings')
        mc.lockNode(sel,lock=False)
        mc.delete(sel)
    except: pass

    # delete layer
    print "## Delete 'Layer'"
    cleanUp.clearLayer();
    # try:
    #     dispLayerStates = [] # Create array to store layer states
    #     dispLayers = mc.ls(type = 'displayLayer')
    #     # print dispLayers
    #     # for lyr in dispLayers:
    #     #     dispLayerStates.append([lyr,mc.getAttr('%s.visibility' % lyr)])
    #     #         # Restore layer visibility
    #     #         #return dispLayerStates
    #     #         #loop = 0
    #     for lyr in dispLayers[1:]:
    #         if lyr != 'displayLayer':
    #             #loop += 1
    #             mc.delete(lyr)
    # except:
    #     print "'displayLayer' can't Delete"

    # import reference and delete nameSpace
    # rigTools.importRigElementDeleteGrp()

    # delete  inheritsTransform
    print "## TurnOff 'inheritsTransform'"
    listInherCtrlRef = mc.ls('*:cheek*DtlCtrlZro_*_grp','*:eye*DtlCtrlZro_*_grp','*:mouth*DtlCtrlZro_*_grp','*:nose*DtlCtrlZro_*_grp','*:cheekDtlCtrlZro_*_grp')
    listInherJntRef = mc.ls('*:cheek*DtlJntZro_*_grp','*:eye*DtlJntZro_*_grp','*:mouth*DtlJntZro_*_grp','*:nose*DtlJntZro_*_grp','*:cheekDtlJntZro_*_grp')
    listInherCtrl = mc.ls('cheek*DtlCtrlZro_*_grp','eye*DtlCtrlZro_*_grp','mouth*DtlCtrlZro_*_grp','nose*DtlCtrlZro_*_grp','cheekDtlCtrlZro_*_grp')
    listInherJnt = mc.ls('cheek*DtlJntZro_*_grp','eye*DtlJntZro_*_grp','mouth*DtlJntZro_*_grp','nose*DtlJntZro_*_grp','cheekDtlJntZro_*_grp')
    for each in listInherCtrlRef:
        if mc.objExists(each):
            mc.setAttr('%s.inheritsTransform' % each,0)
        else: pass
    for each in listInherCtrl:
        if mc.objExists(each):
            mc.setAttr('%s.inheritsTransform' % each,0)
        else: pass
    for each in listInherJntRef:
        if mc.objExists(each):
            mc.setAttr('%s.inheritsTransform' % each,0)
        else: pass
    for each in listInherJnt:
        if mc.objExists(each):
            mc.setAttr('%s.inheritsTransform' % each,0)
        else: pass

    # delete anather polygon
    listPly = mc.ls('eye_rgt_grp','eye_lft_grp','hairBand_ply','tongue_ply')
    mc.delete(listPly)

    mc.hyperShade(rss=True)

    # #assign shader 'lambert1'
    # lisObj = mc.ls('headDtl_Grp', 'dtlGeol_Grp')
    # mc.select( lisObj )
    # mc.hyperShade( assign ='lambert1')

    # clean unused Node 
    print "## Delete 'UnusedNode'"
    cleanUp.clearUnusedNode();
    # mel.eval('MLdeleteUnused;')

    # clean Delete_grp or 
    delGrp = mc.ls('Delete_Grp', 'Delete_grp')
    mc.delete(delGrp)

    # assign shader 'lambert1'
    lisObj = mc.ls('allDtlGeo_grp','dtlGeo_grp','dtlGeo_Grp','headDtl_Grp','dtlGeol_Grp')
    for i in lisObj:
        mc.select(lisObj)
        mc.hyperShade(lisObj, assign ='lambert1')

    # TurnOff SSC
    print "## TurnOff 'Segment Scale Compensate'"
    turnOffSSC.turnOff()

    # clean Camera
    try:
        uc.cleanCamera()
    except: pass

    # clean Delete_grp or 
    try:
        delGrp = mc.ls('Delete_Grp', 'Delete_grp')
        mc.delete(delGrp)
    except: pass

    print '----- Clean Dtl Success!! -----' 
    print '--------    *(^O^)*    --------' 
    print '-------------------------------'

def cleanCombRig():
    # delete unkownode
    print "## Delete 'TurtleDefaultBakeLayer'"
    try:
        sel = mc.ls('TurtleDefaultBakeLayer')
        mc.lockNode(sel,lock=False)
        mc.delete(sel)
    except: pass

    # delete vraySettings
    print "## Delete 'vraySettings'"
    try:
        sel = mc.ls('vraySettings')
        mc.lockNode(sel,lock=False)
        mc.delete(sel)
    except: pass

    # delete layer
    try:
        dispLayerStates = [] # Create array to store layer states
        dispLayers = mc.ls(type = 'displayLayer')
        # print dispLayers
        for lyr in dispLayers:
            dispLayerStates.append([lyr,mc.getAttr('%s.visibility' % lyr)])
                # Restore layer visibility
                #return dispLayerStates
                #loop = 0
        for lyr in dispLayers:
            if lyr != 'displayLayer':
                #loop += 1
                mc.delete(lyr)
    except:
        print "'displayLayer' can't Delete"

    # clean Camera
    try:
        uc.cleanCamera()
    except: pass

    # lock attr eyeLidBsn
    efc.lockEyeLid()


def connectDtl():

    # clean unused Node 
    # mel.eval('MLdeleteUnused;')
    print "## Delete 'UnusedNode'"
    cleanUp.clearUnusedNode();


    # TurnOff SSC
    print "## TurnOff 'Segment Scale Compensate'"
    turnOffSSC.turnOff()

    # List Head joint
    headJnt = mc.ls('*:head1_jnt')
    teethUp = mc.ls('*:teeth1_upr_jnt')
    teethDn = mc.ls('*:teeth1_lwr_jnt')

    # list Dtl Control
    eyeDtl =  mc.ls('*:eye*DtlCtrlZro_*_grp','*:eyeBrow*DtlCtrlZro_*_grp','*:dtlEyelashZro_grp','*:cheekLwrDtlCtrlZro_*_grp','*:cheekUpr*DtlCtrlZro_*_grp','*:noseDtlCtrlZro_*_grp','*:cheekDtlCtrlZro_*_grp')
    mouthUpDtl = mc.ls('*:mouth1DtlCtrlZro_*_grp','*:mouth2DtlCtrlZro_*_grp','*:mouth3DtlCtrlZro_*_grp','*:mouth4DtlCtrlZro_*_grp')
    mouthDnDtl = mc.ls('*:mouth5DtlCtrlZro_*_grp','*:mouth6DtlCtrlZro_*_grp','*:mouth7DtlCtrlZro_*_grp','*:mouth8DtlCtrlZro_*_grp','*:mouth9DtlCtrlZro_*_grp')
    
    # orientConstraint and scaleConstraint Dtl  
    if eyeDtl :
        for i in range(len(eyeDtl)):
            mc.orientConstraint(headJnt,'%s' %eyeDtl[i],mo=True)
            mc.scaleConstraint(headJnt,'%s' %eyeDtl[i],mo=True)
        for i in range(len(mouthUpDtl)):
            mc.orientConstraint(teethUp,'%s' %mouthUpDtl[i],mo=True)
            mc.scaleConstraint(teethUp,'%s' %mouthUpDtl[i],mo=True)
        for i in range(len(mouthDnDtl)):
            mc.orientConstraint(teethDn,'%s' %mouthDnDtl[i],mo=True)
            mc.scaleConstraint(teethDn,'%s' %mouthDnDtl[i],mo=True)

    # lis Skin Group
    animGrp = mc.ls('*:anim_grp')
    stillGrp = mc.ls('*:still_grp')
    skinGrp = mc.ls('*:skin_grp')
    jntGrp = mc.ls('*:jnt_grp')
    ikhGrp = mc.ls('*:ikh_grp')
    defGrp = mc.ls('*:def_grp')

    # lis Dtl Group
    allDtlGeo = mc.ls('*:allDtlGeo_grp')
    allDtlCtrl = mc.ls('*:allDtlCtrl_grp')
    allDtlJnt = mc.ls('*:allDtlJnt_grp')
    allDtlNrb = mc.ls('*:allDtlNrb_grp')

    # Dtl parent to Delete_grp and parent scale Constrant
    for each in allDtlGeo:
        if mc.objExists (each):
            mc.parentConstraint (defGrp,each,mo=True)
            mc.scaleConstraint (defGrp,each,mo=True)
        else: pass
    for each in allDtlCtrl:
        if mc.objExists  (each):
            mc.parentConstraint (animGrp,each,mo=True)
            mc.scaleConstraint (animGrp,each,mo=True)  
        else: pass
    for each in allDtlJnt:
        if mc.objExists (each):
            mc.parentConstraint (defGrp,each,mo=True)
            mc.scaleConstraint (defGrp,each,mo=True)
        else: pass
    for each in allDtlNrb:
        if mc.objExists (each):
            mc.parentConstraint (defGrp,each,mo=True)
            mc.scaleConstraint (defGrp,each,mo=True)
        else: pass

    # list Delete_Grp
    delGrpNew = mc.ls('Delete_Grp')
    delGrpAmount = len(delGrpNew)
    try:
        if delGrpAmount != 0:
            print "Delete_Grp is ready"
            mc.parent (allDtlGeo, allDtlCtrl, allDtlJnt, allDtlNrb, delGrpNew)
            # return delGrpNew
        else:
            delGrp = mc.group(em=True)
            delGrpNew = mc.rename(delGrp,'Delete_Grp') 
            mc.parent (allDtlGeo, allDtlCtrl, allDtlJnt, allDtlNrb, delGrpNew)
        # return delGrpNew
    except:pass
    # delGrpNew = mc.ls('Delete_Grp')
    # Parent to Rig
    # mc.parent (allDtlGeo, allDtlCtrl, allDtlJnt, allDtlNrb, delGrpNew)

def connectFacialSq():
    # lis Skin Group
    animGrp = mc.ls('*:anim_grp')
    stillGrp = mc.ls('*:still_grp')
    skinGrp = mc.ls('*:skin_grp')
    jntGrp = mc.ls('*:jnt_grp')
    ikhGrp = mc.ls('*:ikh_grp')
    defGrp = mc.ls('*:def_grp')

    # list Facial SQ group
    facialSqStill = '*:facialSqStill_Grp'
    headFacialSq = '*:facialSqGeoZro_grp'

    #list alldefGrp_geo
    allDefGeo = 'allDefGeo_grp'

    try:
        for each in facialSqStill:
            if mc.objExists (each):
                mc.parentConstraint (defGrp, each, mo=True)
                mc.scaleConstraint (defGrp, each, mo=True) 
        for each in headFacialSq:
            if mc.objExists (each):
                mc.parentConstraint (defGrp, each, mo=True)
                mc.scaleConstraint (defGrp, each, mo=True)     
    except: pass

    # list Delete_Grp
    delGrpNew = mc.ls('Delete_Grp')
    delGrpAmount = len(delGrpNew)
    try:
        if delGrpAmount != 0:
            print "Delete_Grp is ready"
            mc.parent (facialSqStill, delGrpNew)
            mc.parent (allDefGeo, delGrpNew)
            # return delGrpNew
        else:
            delGrp = mc.group(em=True)
            delGrpNew = mc.rename(delGrp,'Delete_Grp') 
            mc.parent (facialSqStill, delGrpNew)
            mc.parent (allDefGeo, delGrpNew)
        # return delGrpNew
    except:pass

def connectBsn():
    # clean unused Node 
    print "## Delete 'UnusedNode'"
    cleanUp.clearUnusedNode();
    # mel.eval('MLdeleteUnused;')

    # TurnOff SSC
    print "## TurnOff 'Segment Scale Compensate'"
    turnOffSSC.turnOff()

    # List Head joint
    headJnt = mc.ls('*:head1_jnt')
    teethUp = mc.ls('*:teeth1_upr_jnt')
    teethDn = mc.ls('*:teeth1_lwr_jnt')

    # lis Skin Group
    animGrp = mc.ls('*:anim_grp')
    stillGrp = mc.ls('*:still_grp')
    skinGrp = mc.ls('*:skin_grp')
    jntGrp = mc.ls('*:jnt_grp')
    ikhGrp = mc.ls('*:ikh_grp')
    defGrp = mc.ls('*:def_grp')

    # list Bsh
    FacialBshStill = mc.ls('*:FacialBshStill_Grp')
    allDefGeo = mc.ls('*:allDefGeo_grp')
    FacialRig = mc.ls('*:FacialRig_Grp')
    FacialBsh = mc.ls('*:FacialBshCtrlZro_Grp')
    EyeLidsBsh =mc.ls('*:EyeLidsBshJntZro_Grp')

    # list Delete_Grp
    delGrp = mc.ls('Delete_Grp')
    delGrpAmount = len(delGrp)
    try:
        if delGrpAmount != 0:
            print "Delete_Grp is ready"
            delGrpNew = delGrp
            mc.parent (FacialBshStill, FacialRig, delGrpNew)
        else:
            delGrp = mc.group(em=True)
            delGrpNew = mc.rename(delGrp,'Delete_Grp') 
            mc.parent (FacialBshStill, FacialRig, delGrpNew)
    except:pass

    # Parent to Rig
    # mc.parent (FacialBshStill, FacialRig, delGrpNew)

    # check Object
    # FacialBshStill parent to Delete_grp and parent scale Constrant
    for each in FacialBshStill:
        if mc.objExists (each):
            mc.parentConstraint (defGrp,each,mo=True)
            mc.scaleConstraint (defGrp,each,mo=True)
        else: pass
    # allDefGeo_grp parent to Delete_grp and parent scale Constrant
    for each in allDefGeo:
        if mc.objExists (each):
            mc.parentConstraint (defGrp,each,mo=True)
            mc.scaleConstraint (defGrp,each,mo=True)
        else: pass
    # Bsh parent to Delete_grp and parent scale Constrant
    for each in FacialRig :
        if mc.objExists (each):
            mc.parentConstraint (animGrp,each,mo=True)
            mc.scaleConstraint (animGrp,each,mo=True)
        else: pass
    for each in FacialBsh :
        if mc.objExists (each):
            mc.parentConstraint (headJnt,each,mo=True)
            mc.scaleConstraint (headJnt,each,mo=True)
        else: pass
    for each in EyeLidsBsh :
        if mc.objExists (each):     
            mc.parentConstraint (headJnt,each,mo=True)
            mc.scaleConstraint (headJnt,each,mo=True)
        else: pass

    # connectEyelidFollow
    uc.eyeLidFollow()

    print '--- Clean CombRig Success!! ---' 
    print '--------    *(^O^)*    --------' 
    print '-------------------------------'


#----------------------------------------#
#connectFacialSqRun(name = "sq")---------#
#----------------------------------------#

def connectFacialSqRun ():
    name ="sq"
    #lists = mc.ls("HdMidHdDfmRig_Ctrl", "HdUpHdDfmRig_Ctrl", "HdLowHdDfmRig_Ctrl")
    lists = mc.ls("*:HdMidHdDfmRig_Ctrl", "*:HdUpHdDfmRig_Ctrl", "*:HdLowHdDfmRig_Ctrl")
    listAttrs = abdClass.listAttributeKeyableAll(obj = lists)
    #print listAttr
    add = ("Mid", "Up", "Low")
    newObj = []
    attrMid = ('translateX', 'translateY', 'translateZ')
    attrUpLow = ( 'translateX', 'translateY', 'translateZ', 'autoSquash' , 'squash', 'slide')
    i=0

    # lis Skin Group
    animGrp = mc.ls('*:anim_grp')

    #list head1_jnt
    headJnt = mc.ls('*:head1_jnt')
    #rigGroup
    grpRig = mc.group(em=True)
    grpRigName = mc.rename(grpRig, "facial%sRig_Grp" % name.title()) 
     
    #list facialSqRig_Grp
    facialSqRig = mc.ls('facialSqRig_Grp')

    # list Delete_Grp
    delGrpNew = mc.ls('Delete_Grp')
    delGrpAmount = len(delGrpNew)
    try:
        if delGrpAmount != 0:
            print "Delete_Grp is ready"
            mc.parent (facialSqRig, animGrp)
        else:
            delGrp = mc.group(em=True)
            delGrpNew = mc.rename(delGrp,'Delete_Grp') 
            mc.parent (facialSqRig, animGrp)
    except:pass

    # lis Skin Group
    animGrp = mc.ls('*:anim_grp')
    stillGrp = mc.ls('*:still_grp')
    skinGrp = mc.ls('*:skin_grp')
    jntGrp = mc.ls('*:jnt_grp')
    ikhGrp = mc.ls('*:ikh_grp')
    defGrp = mc.ls('*:def_grp')

    # list Facial SQ group
    facialSqStill = mc.ls('*:facialSqStill_Grp', '*:facialSqStill_grp')
    headFacialSq = mc.ls('*:facialSqGeoZro_Grp', '*:facialSqGeoZro_grp')

    #list alldefGrp_geo
    allDefGeo = mc.ls('allDefGeo_grp')

    mc.parentConstraint (defGrp, facialSqStill, mo=True)
    mc.scaleConstraint (defGrp, facialSqStill, mo=True) 
    mc.parentConstraint (defGrp, headFacialSq, mo=True)
    mc.scaleConstraint (defGrp, headFacialSq, mo=True)  

    # list Delete_Grp
    delGrpNew = mc.ls('Delete_Grp')
    delGrpAmount = len(delGrpNew)
    try:
        if delGrpAmount != 0:
            print "Delete_Grp is ready"
            mc.parent (facialSqStill, delGrpNew)
            mc.parent (headFacialSq, delGrpNew)
            mc.parent (allDefGeo, defGrp)
            # return delGrpNew
        else:
            delGrp = mc.group(em=True)
            delGrpNew = mc.rename(delGrp,'Delete_Grp') 
            mc.parent (facialSqStill, delGrpNew)
            mc.parent (headFacialSq, delGrpNew)
            mc.parent (allDefGeo, defGrp)
        # return delGrpNew
    except:pass

    # parent RigGroup to rig
    for each in facialSqRig:
        if mc.objExists(each):
            mc.parentConstraint (headJnt, each, mo=True)
            mc.scaleConstraint (headJnt, each, mo=True) 

    for each in lists:     
        hdD = mc.duplicate(each)
        ctrlName = mc.rename (hdD , "%s%sHdDfm_Ctrl" % (name, add[i]))
        grp = mc.group(em=True)
        grpName = mc.rename(grp, "%s%sHdDfmZro_Grp" % (name, add[i]))
        mc.delete(mc.pointConstraint(ctrlName, grpName))
        mc.parent(ctrlName, grpName)  
        
        for attr in attrMid:
            if "Mid" in ctrlName:
                mc.connectAttr("%s.%s" % (ctrlName, attr), "%s.%s" % (each, attr))
                mc.setAttr("%s.translateY" % grpName, 20)
                mc.setAttr("%s.translateZ" % grpName, 17)
        else: pass
        if "Up" in ctrlName:
            for attr in attrUpLow:
                mc.connectAttr("%s.%s" % (ctrlName, attr), "%s.%s" % (each, attr))
                mc.setAttr("%s.translateY" % grpName, 28)
                mc.setAttr("%s.translateZ" % grpName, 17)
        else: pass
        if "Low" in ctrlName:
            for attr in attrUpLow:
                mc.connectAttr("%s.%s" % (ctrlName, attr), "%s.%s" % (each, attr))
                mc.setAttr("%s.translateY" % grpName, 12)
                mc.setAttr("%s.translateZ" % grpName, 17)
        else: pass
        mc.parent(grpName, grpRigName)
        i = i+1
    return grpName

    # #-----------------------------------------------
    # # lis Skin Group
    # animGrp = mc.ls('*:anim_grp')
    # stillGrp = mc.ls('*:still_grp')
    # skinGrp = mc.ls('*:skin_grp')
    # jntGrp = mc.ls('*:jnt_grp')
    # ikhGrp = mc.ls('*:ikh_grp')
    # defGrp = mc.ls('*:def_grp')

    # # list Facial SQ group
    # facialSqStill = mc.ls('*:facialSqStill_Grp')
    # headFacialSq = mc.ls('*:headSqBsZroGeo_grp')

    # #list alldefGrp_geo
    # allDefGeo = mc.ls('allDefGeo_grp')

    # mc.parentConstraint (defGrp, facialSqStill, mo=True)
    # mc.scaleConstraint (defGrp, facialSqStill, mo=True) 
    # mc.parentConstraint (defGrp, headFacialSq, mo=True)
    # mc.scaleConstraint (defGrp, headFacialSq, mo=True)   


    # # list Delete_Grp
    # delGrpNew = mc.ls('Delete_Grp')
    # delGrpAmount = len(delGrpNew)
    # try:
    #     if delGrpAmount != 0:
    #         print "Delete_Grp is ready"
    #         mc.parent (facialSqStill, delGrpNew)
    #         mc.parent (allDefGeo, delGrpNew)
    #         # return delGrpNew
    #     else:
    #         delGrp = mc.group(em=True)
    #         delGrpNew = mc.rename(delGrp,'Delete_Grp') 
    #         mc.parent (facialSqStill, delGrpNew)
    #         mc.parent (allDefGeo, delGrpNew)
    #     # return delGrpNew
    # except:pass

    print '----- Connect Facial Sq!! -----' 
    print '--------    *(^O^)*    --------' 
    print '-------------------------------'