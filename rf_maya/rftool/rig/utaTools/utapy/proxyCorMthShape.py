import maya.cmds as mc
reload(mc)
# import pymel.core as pm 
# reload(pm)
#-----------------#
# Shapes = Shape  #
#-----------------#
def proxyCorCrtMthBsn(tyOffset, jawMthRotateX, spaceTX,*args):
	#BodyMthRig_Geo
	#BodyMthLipRig_Geo
	#BodyMthJawRig_Geo
	
	headOrig = ["Orig", ("JawOpenRef%s" % jawMthRotateX)]
	crtMthShapes = ["JawOpenMthShapes", "JawOpenShapes"]
	crtBshShapes = ["CnrUpOpenRefShapes", "CnrDnOpenRefShapes", "CnrInOpenShapes", "CnrOutOpenShapes", "CnrInUpOpenShapes", "CnrInDnOpenShapes", "CnrOutUpOpenShapes", "CnrOutDnOpenShapes"]          
	crtBsh = ["JawOpenMth", "JawOpen", "CnrUpOpen", "CnrDnOpen", "CnrInOpen", "CnrOutOpen", "CnrInUpOpen", "CnrInDnOpen", "CnrOutUpOpen", "CnrOutDnOpen"]      
	crtPuff =  ["LipUpPuffMthRigShapes", "LipLowPuffMthRigShapes", "LipUpThickMthRigShapes", "LipLowThickMthRigShapes"]
	mthctrl = mc.ls("*:JawMthRig_Ctrl")
	# headJaw = mc.ls("BodyMthJawRig_Geo")
	# headMth = mc.ls("BodyMthRig_Geo")
	headGeo = mc.ls("BodyMthJawRig_Geo", "BodyMthRig_Geo")
	mc.select(headGeo[0])
	orig_Grp = mc.createNode("transform", n = "orig_Grp")
	crtBsh_Grp = mc.createNode('transform',n = 'crtBsh_Grp')
	crtBshShapes_Grps = mc.createNode('transform',n = 'crtBshShapes_Grp')
	crtBshCon_Grps = mc.createNode('transform',n = 'crtBshCon_Grp')
	crtPuff_Grps = mc.createNode('transform',n = 'crtPuff_Grp')
	attr = ["tx", "ty", "tz", "rx", "ry", "rz", "sx", "sy", "sz"]

	# HeadOriShapes and JawShapes(valueRotate)
	headBshOrig = mc.duplicate(headGeo[0], n = "%s" % headOrig[0])
	mc.setAttr("%s.rotateX" % (mthctrl[0]),jawMthRotateX)
	headJawOprnRef = mc.duplicate(headGeo[0], n = "%s" % headOrig[1])
	mc.setAttr("%s.rotateX" % (mthctrl[0]), 0)
	
	defaultTyEach = 0
	for each in headOrig:
		for attrs in attr:
			mc.setAttr("%s.%s" % (each, attrs), l = False);
			mc.setAttr("%s.translateX" % (each),defaultTyEach)
			defaultTyEach += 3
		mc.parent (each, orig_Grp)
	# unlock headJaw
	for each in headGeo:
		for attrs in attr:
			mc.setAttr("%s.%s" % (each, attrs), l = False);

	# Duplicate crtBshShapes
	crtMthShapesAmount = len(crtMthShapes)
	crtBshShapesAmount = len(crtBshShapes)
	crtBshAmount = len(crtBsh)
	crtPuffAmount = len(crtPuff)


	# for crtBshShapess in crtBshShapes:
	for i in range(crtMthShapesAmount):
		bshShapes = mc.duplicate(headGeo[0], n = "%s" % crtMthShapes[i])
		mc.parent(crtMthShapes[i], crtBshShapes_Grps)
	for i in range(crtBshShapesAmount):
		bshShapes = mc.duplicate(headGeo[1], n = "%s" % crtBshShapes[i])
		mc.parent(crtBshShapes[i], crtBshShapes_Grps)
		print headGeo[0]
	for i in range(crtBshAmount):
		bshShapes = mc.duplicate(headGeo[1], n = "%s" % crtBsh[i])
		mc.parent(crtBsh[i], crtBshCon_Grps)
		print headGeo[1]
	for i in range(crtPuffAmount):
		bshShapes = mc.duplicate(headGeo[1], n = "%s" % crtPuff[i])
		mc.parent(crtPuff[i], crtPuff_Grps)

	# setAttribute JawOpen Default
	mc.setAttr("%s.rotateX" % (mthctrl[0]),0)
	mc.setAttr ("%sShape.jawOpen" % mthctrl[0], jawMthRotateX);
	# parent to crtBsh_Grp
	mc.parent(crtBshShapes_Grps, crtBshCon_Grps, crtPuff_Grps, orig_Grp, crtBsh_Grp)
	mc.setAttr("%s.translateX" % (crtBshCon_Grps),-3.5*int(spaceTX))
	mc.setAttr("%s.translateX" % (crtBshShapes_Grps),-2.5*int(spaceTX))
	mc.setAttr("%s.translateX" % (crtPuff_Grps),-1.5*int(spaceTX))

	crtBshShapesTotal = (crtMthShapes+crtBshShapes)
	# print crtBshShapesTotal
	# translateY 
	crtBshShapes_GrpsAmount = len(crtBshShapesTotal)
	crtBshCon_GrpsAmount = len(crtBsh)
	crtPuff_GrpsAmount = len(crtPuff)
	defaultTy = 0
	for i in range(crtBshShapes_GrpsAmount):
		mc.setAttr("%s.translateY" % (crtBshShapesTotal[i]), defaultTy)
		defaultTy += tyOffset
	defaultTy = 0
	for i in range(crtBshCon_GrpsAmount):
		mc.setAttr("%s.translateY" % (crtBsh[i]), defaultTy)
		defaultTy += tyOffset
	defaultTy = 0
	for i in range(crtPuff_GrpsAmount):
		mc.setAttr("%s.translateY" % (crtPuff[i]), defaultTy)
		defaultTy += tyOffset
 	mc.select(cl=True)
	# create BlendShape Shapestive Mouth

	
	bshList = ["CnrOutDnOpen", "CnrOutUpOpen", "CnrInDnOpen", "CnrInUpOpen", "CnrOutOpen", "CnrInOpen", "CnrDnOpen", "CnrUpOpen", "JawOpen", "JawOpenMth"]
	# proxhList = ["cornerOuterDn" , "cornerOuterUp", "cornerInnerDn", "cornerInnerUp", "cornerOut", "cornerIn", "cornerDn", "cornerUp", ""]
	# ShapesList = ["CnrOutDnOpenShapes", "CnrOutUpOpenShapes", "CnrInDnOpenShapes", "CnrInUpOpenShapes", "CnrOutOpenShapes", "CnrInOpenShapes", "CnrDnOpenRefShapes", "CnrUpOpenRefShapes"]
	# jawOpenRefNum = ("JawOpenRef20")
	# jawOpenShapesList = ("JawOpenShapes")
	
	'''1
	CnrOutDnOpen
		JawOpenShapes -1
		cornerOuterDn -1
		CnrOutDnOpenShapes 1
	'''
	CnrOutDnOpenBsn = mc.blendShape("JawOpenShapes", "cornerOuterDn", "CnrOutDnOpenShapes", "CnrOutDnOpen",n="%sBsn" % bshList[0])[0]
	mc.setAttr("%sBsn" % bshList[0]+"."+"JawOpenShapes",-1)
	mc.setAttr("%sBsn" % bshList[0]+"."+"cornerOuterDn",-1)
	mc.setAttr("%sBsn" % bshList[0]+"."+"CnrOutDnOpenShapes",1)

	CnrOutDnOpenShapesBsn = mc.blendShape("JawOpenShapes", "cornerOuterDn", "CnrOutDnOpenShapes", n="%sShapesBsn" % bshList[0])[0]
	mc.setAttr("%sShapesBsn" % bshList[0]+"."+"JawOpenShapes",1)
	mc.setAttr("%sShapesBsn" % bshList[0]+"."+"cornerOuterDn",1)
	# mc.setAttr("%sBsn" % bshList[0]+"."+"CnrOutDnOpenShapes",1)
	'''2
	CnrOutUpOpen
		JawOpenShapes -1
		cornerOuterUp -1
		CnrOutUpOpenShapes 1
	'''
	CnrOutUpOpenBsn = mc.blendShape("JawOpenShapes", "cornerOuterUp", "CnrOutUpOpenShapes", "CnrOutUpOpen",n="%sBsn" % bshList[1])[0]
	mc.setAttr("%sBsn" % bshList[1]+"."+"JawOpenShapes",-1)
	mc.setAttr("%sBsn" % bshList[1]+"."+"cornerOuterUp",-1)
	mc.setAttr("%sBsn" % bshList[1]+"."+"CnrOutUpOpenShapes",1)

	CnrOutUpOpenShapesBsn = mc.blendShape("JawOpenShapes", "cornerOuterUp", "CnrOutUpOpenShapes", n="%sShapesBsn" % bshList[1])[0]
	mc.setAttr("%sShapesBsn" % bshList[1]+"."+"JawOpenShapes",1)
	mc.setAttr("%sShapesBsn" % bshList[1]+"."+"cornerOuterUp",1)
	# mc.setAttr("%sShapesBsn" % bshList[1]+"."+"CnrOutUpOpenShapes",1)
	'''3
	CnrInDnOpen
		JawOpenShapes -1
		cornerInnerDn -1
		cnrInDnOpenShapes 1
	'''
	CnrInDnOpenBsn = mc.blendShape("JawOpenShapes", "cornerInnerDn", "CnrInDnOpenShapes", "CnrInDnOpen",n="%sBsn" % bshList[2])[0]
	mc.setAttr("%sBsn" % bshList[2]+"."+"JawOpenShapes",-1)
	mc.setAttr("%sBsn" % bshList[2]+"."+"cornerInnerDn",-1)
	mc.setAttr("%sBsn" % bshList[2]+"."+"CnrInDnOpenShapes",1)

	CnrInDnOpenShapesBsn = mc.blendShape("JawOpenShapes", "cornerInnerDn", "CnrInDnOpenShapes", n="%sShapesBsn" % bshList[2])[0]
	mc.setAttr("%sShapesBsn" % bshList[2]+"."+"JawOpenShapes",1)
	mc.setAttr("%sShapesBsn" % bshList[2]+"."+"cornerInnerDn",1)
	# mc.setAttr("%sShapesBsn" % bshList[2]+"."+"CnrInDnOpenShapes",1)
	'''4
	CnrInUpOpen
		JawOpenShapes -1
		cornerInnerUp -1
		CnrInUpOpenShapes 1
	'''
	CnrInUpOpenBsn = mc.blendShape("JawOpenShapes", "cornerInnerUp", "CnrInUpOpenShapes", "CnrInUpOpen",n="%sBsn" % bshList[3])[0]
	mc.setAttr("%sBsn" % bshList[3]+"."+"JawOpenShapes",-1)
	mc.setAttr("%sBsn" % bshList[3]+"."+"cornerInnerUp",-1)
	mc.setAttr("%sBsn" % bshList[3]+"."+"CnrInUpOpenShapes",1)

	CnrInUpOpenBsn = mc.blendShape("JawOpenShapes", "cornerInnerUp", "CnrInUpOpenShapes", n="%sShapesBsn" % bshList[3])[0]
	mc.setAttr("%sShapesBsn" % bshList[3]+"."+"JawOpenShapes",1)
	mc.setAttr("%sShapesBsn" % bshList[3]+"."+"cornerInnerUp",1)
	# mc.setAttr("%sShapesBsn" % bshList[3]+"."+"CnrInUpOpenShapes",1)
	'''5
	CnrOutOpen
		JawOpenShapes -1
		cornerOut -1
		CnrOutOpenShapes 1
	'''
	CnrOutOpenBsn = mc.blendShape("JawOpenShapes", "cornerOut", "CnrOutOpenShapes", "CnrOutOpen",n="%sBsn" % bshList[4])[0]
	mc.setAttr("%sBsn" % bshList[4]+"."+"JawOpenShapes",-1)
	mc.setAttr("%sBsn" % bshList[4]+"."+"cornerOut",-1)
	mc.setAttr("%sBsn" % bshList[4]+"."+"CnrOutOpenShapes",1)

	CnrOutOpenShapesBsn = mc.blendShape("JawOpenShapes", "cornerOut", "CnrOutOpenShapes", n="%sShapesBsn" % bshList[4])[0]
	mc.setAttr("%sShapesBsn" % bshList[4]+"."+"JawOpenShapes",1)
	mc.setAttr("%sShapesBsn" % bshList[4]+"."+"cornerOut",1)
	# mc.setAttr("%sShapessn" % bshList[4]+"."+"CnrOutOpenShapes",1)
	'''6
	CnrInOpen
		JawOpenShapes -1
		cornerIn -1
		CnrInOpenShapes 1
	'''
	CnrInOpenBsn = mc.blendShape("JawOpenShapes", "cornerIn", "CnrInOpenShapes", "CnrInOpen",n="%sBsn" % bshList[5])[0]
	mc.setAttr("%sBsn" % bshList[5]+"."+"JawOpenShapes",-1)
	mc.setAttr("%sBsn" % bshList[5]+"."+"cornerIn",-1)
	mc.setAttr("%sBsn" % bshList[5]+"."+"CnrInOpenShapes",1)

	CnrInOpenShapesBsn = mc.blendShape("JawOpenShapes", "cornerIn", "CnrInOpenShapes", n="%sShapesBsn" % bshList[5])[0]
	mc.setAttr("%sShapesBsn" % bshList[5]+"."+"JawOpenShapes",1)
	mc.setAttr("%sShapesBsn" % bshList[5]+"."+"cornerIn",1)
	# mc.setAttr("%sShapesBsn" % bshList[5]+"."+"CnrInOpenShapes",1)
	'''7
	CnrDnOpen
		JawOpenRef20 -1
		cornerDn -1
		CnrDnOpenRefShapes 1
	'''
	CnrDnOpenBsn = mc.blendShape(("JawOpenRef%s" % jawMthRotateX), "cornerDn", "CnrDnOpenRefShapes", "CnrDnOpen",n="%sBsn" % bshList[6])[0]
	mc.setAttr("%sBsn" % bshList[6]+"."+("JawOpenRef%s" % jawMthRotateX),-1)
	mc.setAttr("%sBsn" % bshList[6]+"."+"cornerDn",-1)
	mc.setAttr("%sBsn" % bshList[6]+"."+"CnrDnOpenRefShapes",1)

	CnrDnOpenRefShapesBsn = mc.blendShape(("JawOpenRef%s" % jawMthRotateX), "cornerDn", "CnrDnOpenRefShapes",n="%sBsn" % crtBshShapesTotal[3])[0]
	mc.setAttr("%sBsn" % crtBshShapesTotal[3]+"."+("JawOpenRef%s" % jawMthRotateX),1)
	mc.setAttr("%sBsn" % crtBshShapesTotal[3]+"."+"cornerDn",1)
	# mc.setAttr("%sBsn" % bshList[6]+"."+"CnrDnOpenRefShapes",1)
	'''8
	CnrUpOpen
		JawOpenRef20 -1
		cornerUp -1
		CnrUpOpenRefShapes 1
	'''
	CnrUpOpenBsn = mc.blendShape(("JawOpenRef%s" % jawMthRotateX), "cornerUp", "CnrUpOpenRefShapes", "CnrUpOpen",n="%sBsn" % bshList[7])[0]
	mc.setAttr("%sBsn" % bshList[7]+"."+("JawOpenRef%s" % jawMthRotateX),-1)
	mc.setAttr("%sBsn" % bshList[7]+"."+"cornerUp",-1)
	mc.setAttr("%sBsn" % bshList[7]+"."+"CnrUpOpenRefShapes",1)

	CnrUpOpenRefShapesBsn = mc.blendShape(("JawOpenRef%s" % jawMthRotateX), "cornerUp", "CnrUpOpenRefShapes",n="%sBsn" % crtBshShapesTotal[2])[0]
	mc.setAttr("%sBsn" % crtBshShapesTotal[2]+"."+("JawOpenRef%s" % jawMthRotateX),1)
	mc.setAttr("%sBsn" % crtBshShapesTotal[2]+"."+"cornerUp",1)
	# mc.setAttr("%sBsn" % bshList[7]+"."+"CnrUpOpenRefShapes",1)

	'''9
	JawOpen
		JawOpenRef20 -1
		JawOpenShapes 1
	'''
	JawOpenBsn = mc.blendShape(("JawOpenRef%s" % jawMthRotateX), "JawOpenShapes", "JawOpen",n="%sBsn" % bshList[8])[0]
	mc.setAttr("%sBsn" % bshList[8]+"."+("JawOpenRef%s" % jawMthRotateX),-1)
	mc.setAttr("%sBsn" % bshList[8]+"."+"JawOpenShapes",1)

	# JawOpenShapesBsn = mc.blendShape(("JawOpenRef%s" % jawMthRotateX), "JawOpenShapes", "JawOpen",n="%sBsn" % bshList[8])[0]
	# mc.setAttr("%sBsn" % bshList[8]+"."+("JawOpenRef%s" % jawMthRotateX),-1)
	# mc.setAttr("%sBsn" % bshList[8]+"."+"JawOpenShapes",1)
	# JawOpenShape
	'''10
	JawOpenMth
		JawOpenRef20 -1
		JawOpenMthShapes 1
	'''
	JawOpenMthBsn = mc.blendShape(("JawOpenRef%s" % jawMthRotateX), "JawOpenMthShapes", "JawOpenMth",n="%sBsn" % bshList[9])[0]
	mc.setAttr("%sBsn" % bshList[9]+"."+("JawOpenRef%s" % jawMthRotateX),-1)
	mc.setAttr("%sBsn" % bshList[9]+"."+"JawOpenMthShapes",1)
