import maya.cmds as mc
import maya.mel as mm
def DtlGrpHiMidLow():
	GrpDtlHi = mc.createNode('transform', n = 'DtlRigCtrlHi_Grp')
	GrpDtlMid = mc.createNode('transform', n = 'DtlRigCtrlMid_Grp')
	GrpDtlLow = mc.createNode ('transform', n = 'DtlRigCtrlLow_Grp')
	# GrpDtlTail = mc.createNode('transform', n = 'DtlRigCtrlTail_Grp')

	listHi = ['EyeBrowDtlRigCtrlZr_Grp',
				'NsDtlRigCtrlZr_Grp',
				'EyeBrowADtlRigCtrlZr_L_Grp',
				'EyeBrowBDtlRigCtrlZr_L_Grp',
				'EyeBrowCDtlRigCtrlZr_L_Grp',
				'EyeBrowDDtlRigCtrlZr_L_Grp',
				'EyeBrowEDtlRigCtrlZr_L_Grp',
				'CheekADtlRigCtrlZr_L_Grp',
				'NsDtlDtlRigCtrlZr_L_Grp',
				'CheekADtlDtlRigCtrlZr_L_Grp', 
				'CheekBDtlDtlRigCtrlZr_L_Grp',
				'CheekCDtlDtlRigCtrlZr_L_Grp',
				'PuffDtlDtlRigCtrlZr_L_Grp',
				'EyeBrowDtlDtlRigCtrlZr_L_Grp',
				'EyeBrowADtlDtlRigCtrlZr_L_Grp',
				'EyeBrowBDtlDtlRigCtrlZr_L_Grp',
				'EyeBrowCDtlDtlRigCtrlZr_L_Grp',
				'EyeBrowDDtlDtlRigCtrlZr_L_Grp',
				'EyeBrowEDtlDtlRigCtrlZr_L_Grp',
				'NsDtlDtlRigCtrlZr_L_Grp',
				'NsDtlRigCtrlZr_L_Grp',
				'EyeSkt1DtlRigCtrlZr_L_Grp',
				'EyeSkt2DtlRigCtrlZr_L_Grp',
				'EyeSkt3DtlRigCtrlZr_L_Grp',
				'EyeSkt4DtlRigCtrlZr_L_Grp',
				'EyeSkt5DtlRigCtrlZr_L_Grp',
				'EyeSkt6DtlRigCtrlZr_L_Grp',
				'EyeSkt7DtlRigCtrlZr_L_Grp',
				'EyeSkt8DtlRigCtrlZr_L_Grp',
				'NsDtlRigCtrlZr_R_Grp',
				'CheekBDtlRigCtrlZr_L_Grp',
				'CheekCDtlRigCtrlZr_L_Grp',
				'OralCavityIn1DtlRigCtrlZr_Grp',
				'EyeBrowADtlRigCtrlZr_R_Grp',
				'EyeBrowBDtlRigCtrlZr_R_Grp',
				'EyeBrowCDtlRigCtrlZr_R_Grp',
				'EyeBrowDDtlRigCtrlZr_R_Grp',
				'EyeBrowEDtlRigCtrlZr_R_Grp',
				'CheekADtlRigCtrlZr_R_Grp',
				'NsDtlDtlRigCtrlZr_R_Grp',
				'CheekADtlDtlRigCtrlZr_R_Grp', 
				'CheekBDtlDtlRigCtrlZr_R_Grp',
				'CheekCDtlDtlRigCtrlZr_R_Grp',
				'PuffDtlDtlRigCtrlZr_R_Grp',
				'EyeBrowDtlDtlRigCtrlZr_R_Grp',
				'EyeBrowADtlDtlRigCtrlZr_R_Grp',
				'EyeBrowBDtlDtlRigCtrlZr_R_Grp',
				'EyeBrowCDtlDtlRigCtrlZr_R_Grp',
				'EyeBrowDDtlDtlRigCtrlZr_R_Grp',
				'EyeBrowEDtlDtlRigCtrlZr_R_Grp',
				'NsDtlDtlRigCtrlZr_R_Grp',
				'EyeSkt1DtlRigCtrlZr_R_Grp',
				'EyeSkt2DtlRigCtrlZr_R_Grp',
				'EyeSkt3DtlRigCtrlZr_R_Grp',
				'EyeSkt4DtlRigCtrlZr_R_Grp',
				'EyeSkt5DtlRigCtrlZr_R_Grp',
				'EyeSkt6DtlRigCtrlZr_R_Grp',
				'EyeSkt7DtlRigCtrlZr_R_Grp',
				'EyeSkt8DtlRigCtrlZr_R_Grp',
				'CheekBDtlRigCtrlZr_R_Grp',
				'CheekCDtlRigCtrlZr_R_Grp',
				'LipsUpDtlRigCtrlZr_Grp',
				'LipsUp1DtlRigCtrlZr_L_Grp',
				'LipsUp2DtlRigCtrlZr_L_Grp',
				'LipsUp3DtlRigCtrlZr_L_Grp',
				'LipsUp4DtlRigCtrlZr_L_Grp',
				'PuffDtlRigCtrlZr_L_Grp',
				'PuffDtlRigCtrlZr_R_Grp',
				'LipsUp1DtlRigCtrlZr_R_Grp',
				'LipsUp2DtlRigCtrlZr_R_Grp',
				'LipsUp3DtlRigCtrlZr_R_Grp',
				'LipsUp4DtlRigCtrlZr_R_Grp',
				'OralCavity2DtlRigCtrlZr_R_Grp',
				'OralCavity5DtlRigCtrlZr_R_Grp',
				'OralCavity2DtlRigCtrlZr_L_Grp',
				'OralCavity5DtlRigCtrlZr_L_Grp',
				'OralCavityIn2DtlRigCtrlZr_Grp',
				'OralCavity1DtlRigCtrlZr_R_Grp',
				'OralCavity1DtlRigCtrlZr_L_Grp',
				]


	listMid = [	'LipsUpDtlRigCtrlZr_L_Grp',
				'LipsUpDtlRigCtrlZr_R_Grp',

				'OralCavityIn1DtlRigCtrlZr_R_Grp',
				'OralCavityIn1DtlRigCtrlZr_L_Grp',
				'CheekDn1DtlRigCtrlZr_R_Grp',
				'CheekDn1DtlRigCtrlZr_L_Grp',
				'CheekDn2DtlRigCtrlZr_L_Grp',
				'CheekDn2DtlRigCtrlZr_R_Grp',
				]

	listLo = ['ChinDtlRigCtrlZr_Grp',
				'ChinDtlDtlRigCtrlZr_L_Grp',
				'LipsDn1DtlRigCtrlZr_L_Grp',
				'LipsDn2DtlRigCtrlZr_L_Grp',
				'LipsDn3DtlRigCtrlZr_L_Grp',
				'LipsDnDtlRigCtrlZr_L_Grp',
				'CheekDn3DtlRigCtrlZr_L_Grp',
				'OralCavityIn2DtlRigCtrlZr_L_Grp',
				'LipsDnDtlRigCtrlZr_Grp',
				'ChinDtlDtlRigCtrlZr_R_Grp',
				'LipsDn1DtlRigCtrlZr_R_Grp',
				'LipsDn2DtlRigCtrlZr_R_Grp',
				'LipsDn3DtlRigCtrlZr_R_Grp',
				'LipsDnDtlRigCtrlZr_R_Grp',
				'CheekDn3DtlRigCtrlZr_R_Grp',
				'OralCavityIn2DtlRigCtrlZr_R_Grp',
				'OralCavityIn1DtlRigCtrlZr_Grp',
				'OralCavity4DtlRigCtrlZr_R_Grp',
				'OralCavity4DtlRigCtrlZr_L_Grp',
				'OralCavity3DtlRigCtrlZr_R_Grp',
				'OralCavity3DtlRigCtrlZr_L_Grp',
				'LipsDn4DtlRigCtrlZr_R_Grp',
				'LipsDn4DtlRigCtrlZr_L_Grp',
				'LipsCenDtlRigCtrlZr_L_Grp',
				'LipsCenDtlRigCtrlZr_R_Grp',
				'LipsCornerDtlRigCtrlZr_L_Grp',
				'LipsCornerDtlRigCtrlZr_R_Grp',
				]

	# listTail = ['muscle1DtlRigCtrlZr_L_Grp',
	# 			'muscle2DtlRigCtrlZr_L_Grp',
	# 			'muscle3DtlRigCtrlZr_L_Grp',
	# 			'muscle1DtlRigCtrlZr_R_Grp',
	# 			'muscle2DtlRigCtrlZr_R_Grp',
	# 			'muscle3DtlRigCtrlZr_R_Grp',
	# 			]

	for each in listHi:
		try:
			mc.parent(each,	GrpDtlHi)
		except: pass
	for each in listMid:
		try:
			mc.parent(each, GrpDtlMid)
		except: pass
	for each in listLo:
		try:
			mc.parent(each,	GrpDtlLow)
		except: pass
	# for each in listTail:
	# 	try:
	# 		mc.parent(each,	GrpDtlTail)
	# 	except: pass
	mc.delete('DtlRigCtrl_Grp')

