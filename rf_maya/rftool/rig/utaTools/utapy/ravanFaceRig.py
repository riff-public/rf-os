
import maya.cmds as mc
from ncmel import mainRig
reload(mainRig)
from ncmel import subRig
reload(subRig)
from ncmel import fkRig
reload(fkRig)
from utaTools.utapy import utaCore
reload(utaCore)
from rf_maya.ncscript.rig import rigTools
reload(rigTools)

def main( size = 0.75 ):
	
	print "# Generate >> Main Group"
	main = mainRig.Run( 
									assetName = '' ,
									size      = size
					   )
	print "##------------------------------------------------------------------------------------"
	print "##-----------------------------MAIN GROUP---------------------------------------------"
	print "##------------------------------------------------------------------------------------"


	print "# Generate >> Head"
	Head = subRig.Run(
									name       = 'Head' ,
									tmpJnt     = 'Head_TmpJnt' ,
									parent     =  '',
									ctrlGrp    =  main.mainCtrlGrp ,
									shape      = 'cube' ,
									side       = '' ,
									size       = size  
								)

	print '# Generate >> HeadTip Rig'
	HeadTipObj = fkRig.Run(              
									name       = 'HeadTip' ,
									tmpJnt     = [	'HeadTip1_TmpJnt', 
													'HeadTip2_TmpJnt',
												],
									parent     = 'HeadSub_Jnt',
									ctrlGrp    = main.mainCtrlGrp ,
									axis       = 'y' ,
									shape      = 'cube' ,
									side       = '' ,
									size       = size 
				)

	print '# Generate >> jawLwr FK Rig'
	jawLwrFKObj = fkRig.Run(              
									name       = 'jawLwr' ,
									tmpJnt     = [	'jawLwr1_TmpJnt', 
													'jawLwr2_TmpJnt',
													'jawLwr3_TmpJnt',
												],
									parent     = 'HeadSub_Jnt',
									ctrlGrp    = main.mainCtrlGrp ,
									axis       = 'y' ,
									shape      = 'cube' ,
									side       = '' ,
									size       = size 
				)

	print '# Generate >> jawLwr L FK Rig'
	jawLwrLFKObj = fkRig.Run(              
									name       = 'jawLwr' ,
									tmpJnt     = [	'jawLwr1_L_TmpJnt', 
													'jawLwr2_L_TmpJnt',
												],
									parent     = 'jawLwr1_Jnt',
									ctrlGrp    = main.mainCtrlGrp ,
									axis       = 'y' ,
									shape      = 'cube' ,
									side       = 'L' ,
									size       = size 
				)
	print '# Generate >> jawLwr R FK Rig'
	jawLwrRFKObj = fkRig.Run(              
									name       = 'jawLwr' ,
									tmpJnt     = [	'jawLwr1_R_TmpJnt', 
													'jawLwr2_R_TmpJnt',
												],
									parent     = 'jawLwr1_Jnt',
									ctrlGrp    = main.mainCtrlGrp ,
									axis       = 'y' ,
									shape      = 'cube' ,
									side       = 'R' ,
									size       = size 
				)



	print '# Generate >> jawUpr FK Rig'
	jawUprFKObj = fkRig.Run(              
									name       = 'jawUpr' ,
									tmpJnt     = [	'jawUpr1_TmpJnt', 
													'jawUpr2_TmpJnt',
												],
									parent     = 'HeadSub_Jnt',
									ctrlGrp    = main.mainCtrlGrp ,
									axis       = 'y' ,
									shape      = 'cube' ,
									side       = '' ,
									size       = size 
				)


	print '# Generate >> jawUpr L FK Rig'
	jawUprLFKObj = fkRig.Run(              
									name       = 'jawUpr' ,
									tmpJnt     = [	'jawUpr1_L_TmpJnt', 
													'jawUpr2_L_TmpJnt',
												],
									parent     = 'HeadSub_Jnt',
									ctrlGrp    = main.mainCtrlGrp ,
									axis       = 'y' ,
									shape      = 'cube' ,
									side       = 'L' ,
									size       = size 
				)

	print '# Generate >> jawUpr R FK Rig'
	jawUprRFKObj = fkRig.Run(              
									name       = 'jawUpr' ,
									tmpJnt     = [	'jawUpr1_R_TmpJnt', 
													'jawUpr2_R_TmpJnt',
												],
									parent     = 'HeadSub_Jnt',
									ctrlGrp    = main.mainCtrlGrp ,
									axis       = 'y' ,
									shape      = 'cube' ,
									side       = 'R' ,
									size       = size 
				)

	print '# Generate >> jawUprBck FK Rig'
	jawUprBckFKObj = fkRig.Run(              
									name       = 'jawUprBck' ,
									tmpJnt     = [	'jawUpr1Bck_TmpJnt', 
													'jawUpr2Bck_TmpJnt',
												],
									parent     = 'HeadSub_Jnt',
									ctrlGrp    = main.mainCtrlGrp ,
									axis       = 'y' ,
									shape      = 'cube' ,
									side       = '' ,
									size       = size 
				)

	print '# Generate >> jawUprBck L FK Rig'
	jawUprBckLFKObj = fkRig.Run(              
									name       = 'jawUprBck' ,
									tmpJnt     = [	'jawUpr1Bck_L_TmpJnt', 
													'jawUpr2Bck_L_TmpJnt',
												],
									parent     = 'HeadSub_Jnt',
									ctrlGrp    = main.mainCtrlGrp ,
									axis       = 'y' ,
									shape      = 'cube' ,
									side       = 'L' ,
									size       = size 
				)
	print '# Generate >> jawUprBck R FK Rig'
	jawUprBckRFKObj = fkRig.Run(              
									name       = 'jawUprBck' ,
									tmpJnt     = [	'jawUpr1Bck_R_TmpJnt', 
													'jawUpr2Bck_R_TmpJnt',
												],
									parent     = 'HeadSub_Jnt',
									ctrlGrp    = main.mainCtrlGrp ,
									axis       = 'y' ,
									shape      = 'cube' ,
									side       = 'R' ,
									size       = size 
				)
	print "# Generate >> Power"
	PowerObj = subRig.Run(
									name       = 'Power' ,
									tmpJnt     = 'Power_TmpJnt' ,
									parent     = 'HeadSub_Jnt' ,
									ctrlGrp    =  main.mainCtrlGrp ,
									shape      = 'cube' ,
									side       = '' ,
									size       = size  
								)

	print 'Main Rig D O N E'

def addRig(*args):	
	print 'Generate PowerSub_Jnt'
	basePower = 'PowerSub_Jnt'
	listsPower = [	'ravanFaceEye_002_001:AllMover_Ctrl',
					'ravanFaceMouth_002_001:ravaFaceMouthMidCtrlOfst_L_Grp',
					'ravanFaceMouth_002_001:ravaFaceMouthMidCtrlOfst_R_Grp',
					'ravanFaceMouth_002_001:ravaFaceMouthDownCtrlOfst_Grp',
					'ravanFaceMouth_002_001:ravaFaceMouthBackCtrlOfst_Grp',
					]
	for each in listsPower:
		utaCore.parentScaleConstraintLoop(obj= basePower, lists = [each],par = True, sca = True, ori = False, poi = False)





	print 'Generate jawLwr2_Jnt'
	baseJawLwr = 'jawLwr2_Jnt'
	listsJawLwr = [	'ravanFaceChin_002_001:AllMover_Ctrl',
					'ravanFaceChin_002_001:AllMover_Ctrl',
					'ravanFaceBasreliefC_001_001:AllMover_Ctrl',
					'ravanFaceCheek_002_001:AllMover_Ctrl',	

					'ravanFaceSteelPassB_0019_001:AllMover_Ctrl',


					]
	for each in listsJawLwr:
		utaCore.parentScaleConstraintLoop(obj= baseJawLwr, lists = [each],par = True, sca = True, ori = False, poi = False)

	print 'Generate jawLwr1_L_Jnt'
	basejawLwr1_L = 'jawLwr1_L_Jnt'
	listsjawLwr1_L = [	'ravanFaceCheek_002_001:ravanFaceCheekFCtrlZro_Grp',
						'ravanFaceCheek_002_001:ravanFaceCheekCCtrlZro_Grp',
						'ravanFaceSteelPassB_0038_001:AllMover_Ctrl',
						'ravanFaceBasReliefB_002_001:ravanFaceBasreliefB3CtrlOfst_Grp',
						'ravanFaceBasReliefB_002_001:ravanFaceBasreliefB4CtrlOfst_Grp',
						'ravanFaceBasReliefB_001_001:ravanFaceBasreliefB4CtrlOfst_Grp',
						'ravanFaceBasReliefB_001_001:ravanFaceBasreliefB3CtrlOfst_Grp',
						'ravanFaceCheek_002_001:ravanFaceCheekCCtrlZro_Grp',
						'ravanFaceSteelPassB_0025_001:AllMover_Ctrl',
						'ravanFaceSteelPassC_0014_001:AllMover_Ctrl',

						]
	for each in listsjawLwr1_L:
		utaCore.parentScaleConstraintLoop(obj= basejawLwr1_L, lists = [each],par = True, sca = True, ori = False, poi = False)

	print 'Generate jawLwr1_R_Jnt'
	basejawLwr1_R = 'jawLwr1_R_Jnt'
	listsjawLwr1_R = [	'ravanFaceCanine_003_001:AllMover_Ctrl',
						'ravanFaceSteelPassC_0027_001:AllMover_Ctrl',
						'ravanFaceBasReliefB_001_001:ravanFaceBasreliefB1CtrlOfst_Grp',
						'ravanFaceBasReliefB_001_001:ravanFaceBasreliefB10CtrlOfst_Grp',
						'ravanFaceBasReliefB_002_001:ravanFaceBasreliefB1CtrlOfst_Grp',
						'ravanFaceBasReliefB_002_001:ravanFaceBasreliefB10CtrlOfst_Grp',
						'ravanFaceCheek_001_001:ravanFaceCheekCCtrlZro_Grp',
						'ravanFaceCheek_001_001:ravanFaceCheekFCtrlZro_Grp',
						'ravanFaceSteelPassC_0015_001:AllMover_Ctrl',
						'ravanFaceCheek_001_001:ravanFaceCheekGCtrlZro_Grp',
						'ravanFaceCheek_001_001:ravanFaceCheekECtrlZro_Grp',
						'ravanFaceSteelPassB_0032_001:AllMover_Ctrl', 
						'ravanFaceEye_0011_001:AllMover_Ctrl',
						'ravanFaceEye_0012_001:AllMover_Ctrl',
						]
	for each in listsjawLwr1_R:
		utaCore.parentScaleConstraintLoop(obj= basejawLwr1_R, lists = [each],par = True, sca = True, ori = False, poi = False)



	print 'Generate jawUpr1_Jnt'
	basejawUpr = 'jawUpr1_Jnt'
	listsjawUpr = [	'ravanFaceForehead_001_001:AllMover_Ctrl',
					'ravanFaceCheek_002_001:ravanFaceCheekACtrlZro_Grp',
					'ravanFaceEye_0010_001:AllMover_Ctrl',
					'ravanFaceEye_006_001:AllMover_Ctrl',
					'ravanFaceBasreliefA_003_001:ravanFaceBasreliefA3CtrlOfst_Grp',
					'ravanFaceBasreliefA_003_001:ravanFaceBasreliefA5CtrlOfst_Grp',
					'ravanFaceBasreliefA_003_001:ravanFaceBasreliefA4CtrlOfst_Grp',
					'ravanFaceEye_0027_001:AllMover_Ctrl',
					'ravanFaceSteelPassA_009_001:AllMover_Ctrl',
					'ravanFaceBasReliefB_001_001:ravanFaceBasreliefB13CtrlOfst_Grp',
					'ravanFaceSteelPassB_007_001:AllMover_Ctrl',
					'ravanFaceSteelPassB_008_001:AllMover_Ctrl',
					'ravanFaceSteelPassA_007_001:AllMover_Ctrl',
					'ravanFaceSteelPassB_009_001:AllMover_Ctrl',
					'ravanFaceSteelPassA_008_001:AllMover_Ctrl',
					'ravanFaceSteelPassC_005_001:AllMover_Ctrl',
					'ravanFaceSteelPassB_0011_001:AllMover_Ctrl',
					'ravanFaceSteelPassA_0013_001:AllMover_Ctrl',
					'ravanFaceSteelPassA_0017_001:AllMover_Ctrl',
					'ravanFaceBasReliefB_002_001:ravanFaceBasreliefB13CtrlOfst_Grp',
					'ravanFaceCheek_001_001:ravanFaceCheekACtrlZro_Grp',
					'ravanFaceCheek_002_001:ravanFaceCheekACtrlZro_Grp',
					'ravanFaceFaceCenter_001_001:ravanFaceFaceCenterLCtrlZro_Grp',
					'ravanFaceFaceCenter_001_001:ravanFaceFaceCenterKCtrlZro_Grp',
					'ravanFaceFaceCenter_001_001:ravanFaceFaceCenterFCtrlZro_Grp',
					'ravanFaceFaceCenter_001_001:ravanFaceFaceCenterCCtrlZro_Grp',
					]
	for each in listsjawUpr:
		utaCore.parentScaleConstraintLoop(obj= basejawUpr, lists = [each],par = True, sca = True, ori = False, poi = False)

	print 'Generate jawUpr1_L_Jnt'
	basejawUpr_L = 'jawUpr1_L_Jnt'
	listsjawUpr_L = [	'ravanCheekStage_001_001:AllMover_Ctrl',
						'ravanFaceLittleFaceStage_001_001:AllMover_Ctrl',
						'ravanFaceEye_008_001:AllMover_Ctrl',
						'ravanFaceCheek_002_001:ravanFaceCheekECtrlZro_Grp',
						'ravanFaceEye_009_001:AllMover_Ctrl',
						'ravanFaceMouth_002_001:ravaFaceMouthUpCtrlOfst_L_Grp',

						'ravanFaceBasreliefA_003_001:ravanFaceBasreliefA8CtrlOfst_Grp',
						'ravanFaceBasreliefA_003_001:ravanFaceBasreliefA11CtrlOfst_Grp',
						'ravanFaceSteelPassC_0013_001:AllMover_Ctrl',
						'ravanFaceSteelPassC_004_001:AllMover_Ctrl',
						'ravanFaceCheek_002_001:ravanFaceCheekGCtrlZro_Grp',
						'ravanFaceBasReliefB_001_001:ravanFaceBasreliefB14CtrlOfst_Grp',
						'ravanFaceBasReliefB_001_001:ravanFaceBasreliefB15CtrlOfst_Grp',
						'ravanFaceBasReliefB_001_001:ravanFaceBasreliefB16CtrlOfst_Grp',
						'ravanFaceSteelPassB_0036_001:AllMover_Ctrl',
						'ravanFaceSteelPassB_0037_001:AllMover_Ctrl',
						'ravanFaceSteelPassC_0028_001:AllMover_Ctrl',
						'ravanFaceBasReliefB_002_001:ravanFaceBasreliefB14CtrlOfst_Grp',
						'ravanFaceBasReliefB_002_001:ravanFaceBasreliefB15CtrlOfst_Grp',
						'ravanFaceBasReliefB_002_001:ravanFaceBasreliefB16CtrlOfst_Grp',
						'ravanFaceFaceCenter_001_001:ravanFaceFaceCenterJCtrlZro_Grp',
						'ravanFaceFaceCenter_001_001:ravanFaceFaceCenterHCtrlZro_Grp',
						'ravanFaceBasreliefA_003_001:ravanFaceBasreliefA1CtrlOfst_Grp',
						'ravanFaceEye_007_001:AllMover_Ctrl',
					]
	for each in listsjawUpr_L:
		utaCore.parentScaleConstraintLoop(obj= basejawUpr_L, lists = [each],par = True, sca = True, ori = False, poi = False)

	print 'Generate jawUpr1_R_Jnt'
	basejawUpr_R = 'jawUpr1_R_Jnt'
	listsjawUpr_R = [	
						'ravanFaceCheek_001_001:ravanFaceCheekBCtrlZro_Grp',
						'ravanFaceEye_0013_001:AllMover_Ctrl',
						'ravanFaceLittleFaceStage_002_001:AllMover_Ctrl',
						'ravanFaceSteelPassC_006_001:AllMover_Ctrl',
						'ravanFaceEye_004_001:AllMover_Ctrl',
						'ravanFaceMouth_002_001:ravaFaceMouthUpCtrlOfst_R_Grp',

						'ravanFaceBasreliefA_003_001:ravanFaceBasreliefA10CtrlOfst_Grp',
						'ravanFaceBasreliefA_003_001:ravanFaceBasreliefA2CtrlOfst_Grp',
						'ravanFaceBasreliefA_003_001:ravanFaceBasreliefA9CtrlOfst_Grp',
						'ravanFaceBasreliefA_003_001:ravanFaceBasreliefA7CtrlOfst_Grp',
						'ravanFaceSteelPassA_006_001:AllMover_Ctrl',
						'ravanFaceSteelPassB_0010_001:AllMover_Ctrl',
						'ravanFaceBasReliefB_001_001:ravanFaceBasreliefB2CtrlOfst_Grp',
						'ravanFaceBasReliefB_001_001:ravanFaceBasreliefB5CtrlOfst_Grp',
						'ravanFaceBasReliefB_001_001:ravanFaceBasreliefB6CtrlOfst_Grp',
						'ravanFaceBasReliefB_001_001:ravanFaceBasreliefB8CtrlOfst_Grp',
						'ravanFaceBasReliefB_001_001:ravanFaceBasreliefB9CtrlOfst_Grp',
						'ravanFaceBasReliefB_001_001:ravanFaceBasreliefB11CtrlOfst_Grp',
						'ravanFaceBasReliefB_001_001:ravanFaceBasreliefB12CtrlOfst_Grp',
						'ravanFaceSteelPassC_007_001:AllMover_Ctrl',
						'ravanFaceSteelPassB_0015_001:AllMover_Ctrl',
						'ravanFaceSteelPassA_0014_001:AllMover_Ctrl',
						'ravanFaceSteelPassB_0033_001:AllMover_Ctrl', 
						'ravanFaceSteelPassC_0017_001:AllMover_Ctrl', 
						'ravanFaceSteelPassA_0018_001:AllMover_Ctrl',
						'ravanFaceBasReliefB_002_001:ravanFaceBasreliefB2CtrlOfst_Grp',
						'ravanFaceBasReliefB_002_001:ravanFaceBasreliefB5CtrlOfst_Grp',
						'ravanFaceBasReliefB_002_001:ravanFaceBasreliefB6CtrlOfst_Grp',
						'ravanFaceBasReliefB_002_001:ravanFaceBasreliefB7CtrlOfst_Grp',
						'ravanFaceBasReliefB_002_001:ravanFaceBasreliefB8CtrlOfst_Grp',
						'ravanFaceBasReliefB_002_001:ravanFaceBasreliefB9CtrlOfst_Grp',
						'ravanFaceBasReliefB_002_001:ravanFaceBasreliefB11CtrlOfst_Grp',
						'ravanFaceBasReliefB_002_001:ravanFaceBasreliefB12CtrlOfst_Grp',
						'ravanFaceFaceCenter_001_001:ravanFaceFaceCenterICtrlZro_Grp',
						'ravanFaceFaceCenter_001_001:ravanFaceFaceCenterGCtrlZro_Grp',
						'ravanFaceBasreliefA_003_001:ravanFaceBasreliefA6CtrlOfst_Grp',
						'ravanFaceEye_003_001:AllMover_Ctrl',
						'ravanFaceSteelPassC_0026_001:AllMover_Ctrl',
					]
	for each in listsjawUpr_R:
		utaCore.parentScaleConstraintLoop(obj= basejawUpr_R, lists = [each],par = True, sca = True, ori = False, poi = False)


	print 'Generate HeadTip'
	baseHeadTip = 'HeadTip2Pos_Jnt'
	listsHeadTip = [	'ravanFaceJunkAround_001_001:AllMover_Ctrl',
						'ravanFaceCrownStage_001_001:AllMover_Ctrl',
						'ravanFaceEye_0037_001:AllMover_Ctrl',
						'ravanFaceEye_0033_001:AllMover_Ctrl',
						'ravanFaceEye_0028_001:AllMover_Ctrl',
						'ravanFaceEye_0036_001:AllMover_Ctrl',
						'ravanFaceSteelPassA_001_001:AllMover_Ctrl',
					]
	for each in listsHeadTip:
		utaCore.parentScaleConstraintLoop(obj= baseHeadTip, lists = [each],par = True, sca = True, ori = False, poi = False)

	print 'Generate jawUprBck2'
	basejawUprBck2 = 'jawUprBck2Pos_Jnt'
	listsjawUprBck2 = [	'ravanFaceFaceCenter_002_001:AllMover_Ctrl',
						'ravanFaceForehead_002_001:AllMover_Ctrl',
						'ravanFaceEye_0016_001:AllMover_Ctrl',
						'ravanFaceEye_0018_001:AllMover_Ctrl',
						'ravanFaceEye_0019_001:AllMover_Ctrl',
						'ravanFaceEye_0021_001:AllMover_Ctrl',
						'ravanFaceEye_0022_001:AllMover_Ctrl',
						'ravanFaceEye_0030_001:AllMover_Ctrl',
						'ravanFaceEye_0029_001:AllMover_Ctrl',
						'ravanFaceEye_0023_001:AllMover_Ctrl',
						'ravanFaceEye_0024_001:AllMover_Ctrl',
						'ravanFaceEye_0015_001:AllMover_Ctrl',
						'ravanFaceEye_0031_001:AllMover_Ctrl',
						'ravanFaceEye_0032_001:AllMover_Ctrl',
						'ravanFaceCanine_0010_001:AllMover_Ctrl',
						'ravanFaceCanine_0011_001:AllMover_Ctrl',
						'ravanFaceCanine_0012_001:AllMover_Ctrl',
						'ravanFaceChin_003_001:AllMover_Ctrl',
						'ravanFaceMouth_003_001:AllMover_Ctrl',
						'ravanFaceCanine_009_001:AllMover_Ctrl',
						'ravanFaceCanine_008_001:AllMover_Ctrl',
						'ravanFaceCanine_007_001:AllMover_Ctrl',
						'ravanFaceLittleFaceStage_004_001:AllMover_Ctrl',
						'ravanFaceLittleFaceStage_003_001:AllMover_Ctrl',
						'ravanFaceSteelPassC_003_001:AllMover_Ctrl',
						'ravanFaceSteelPassB_0012_001:AllMover_Ctrl',
						'ravanFaceSteelPassB_0013_001:AllMover_Ctrl',
						'ravanFaceSteelPassC_008_001:AllMover_Ctrl',
						'ravanFaceSteelPassA_0010_001:AllMover_Ctrl',
						'ravanFaceSteelPassB_0022_001:AllMover_Ctrl',
						'ravanFaceSteelPassC_009_001:AllMover_Ctrl',
						'ravanFaceSteelPassB_0024_001:AllMover_Ctrl',
						'ravanFaceSteelPassB_0026_001:AllMover_Ctrl',
						'ravanFaceSteelPassB_0027_001:AllMover_Ctrl',
						'ravanFaceSteelPassB_0028_001:AllMover_Ctrl',
						'ravanFaceSteelPassB_0029_001:AllMover_Ctrl',
						'ravanFaceSteelPassC_0012_001:AllMover_Ctrl',
						'ravanFaceSteelPassA_0016_001:AllMover_Ctrl',
						'ravanFaceSteelPassC_0016_001:AllMover_Ctrl',
						'ravanFaceSteelPassB_0031_001:AllMover_Ctrl',
						'ravanFaceSteelPassC_0018_001:AllMover_Ctrl', 
						'ravanFaceSteelPassB_0035_001:AllMover_Ctrl',
						'ravanFaceSteelPassC_0020_001:AllMover_Ctrl',
						'ravanFaceSteelPassC_0021_001:AllMover_Ctrl',
						'ravanFaceSteelPassA_0019_001:AllMover_Ctrl',
						'ravanFaceSteelPassC_0022_001:AllMover_Ctrl',
						'ravanFaceSteelPassC_0023_001:AllMover_Ctrl',
						'ravanFaceSteelPassC_0024_001:AllMover_Ctrl',
						'ravanFaceSteelPassC_0025_001:AllMover_Ctrl',
						'ravanFaceSteelPassA_0021_001:AllMover_Ctrl',
						'ravanFaceSteelPassA_0022_001:AllMover_Ctrl',
						'ravanFaceCheek_001_001:ravanFaceCheekDCtrlZro_Grp',
						'ravanFaceCheek_002_001:ravanFaceCheekDCtrlZro_Grp',
						'ravanFaceSteelPassC_0019_001:AllMover_Ctrl',
						'ravanFaceSteelPassB_0019_001:AllMover_Ctrl',
						'ravanFaceSteelPassB_0020_001:AllMover_Ctrl',
						'ravanFaceNoseStage_002_001:AllMover_Ctrl',
						'ravanFaceSteelPassB_0030_001:AllMover_Ctrl',
						'ravanFaceSteelPassB_0017_001:AllMover_Ctrl',
						'ravanFaceEye_0017_001:AllMover_Ctrl',
					]
	for each in listsjawUprBck2:
		utaCore.parentScaleConstraintLoop(obj= basejawUprBck2, lists = [each],par = True, sca = True, ori = False, poi = False)



	print 'Generate jawUprBck1_L'
	baseJawUprBck1_L = 'jawUprBck2Pos_L_Jnt'
	listsJawUprBck1_L = ['ravanFaceEar_001_001:AllMover_Ctrl',
						'ravanFaceEye_0035_001:AllMover_Ctrl',
						'ravanFaceSteelPassA_005_001:AllMover_Ctrl',
						'ravanFaceSteelPassC_0011_001:AllMover_Ctrl',
						'ravanFaceSteelPassC_002_001:AllMover_Ctrl',
						'ravanFaceSteelPassA_0011_001:AllMover_Ctrl',
						'ravanFaceSteelPassA_0015_001:AllMover_Ctrl',
						'ravanFaceSteelPassB_0034_001:AllMover_Ctrl', 
						'ravanFaceSteelPassB_006_001:AllMover_Ctrl',
						'ravanFaceSteelPassB_005_001:AllMover_Ctrl',
						'ravanFaceCheek_002_001:ravanFaceCheekBCtrlZro_Grp',
						'ravanFaceSteelPassB_0021_001:RootSubCtrlOfst_Grp',
						'ravanFaceEye_0034_001:RootSubCtrlOfst_Grp',
					]
	for each in listsJawUprBck1_L:
		utaCore.parentScaleConstraintLoop(obj= baseJawUprBck1_L, lists = [each],par = True, sca = True, ori = False, poi = False)

	print 'Generate jawUprBck1_R'
	baseJawUprBck1_R = 'jawUprBck2Pos_R_Jnt'
	listsJawUprBck1_R = ['ravanFaceEar_002_001:AllMover_Ctrl',
						'ravanFaceEye_0014_001:AllMover_Ctrl',
						'ravanFaceSteelPassA_002_001:AllMover_Ctrl',
						'ravanFaceSteelPassA_003_001:AllMover_Ctrl',	
						'ravanFaceSteelPassC_001_001:AllMover_Ctrl',
						'ravanFaceSteelPassA_004_001:AllMover_Ctrl',	
						'ravanFaceSteelPassA_0012_001:AllMover_Ctrl',
						'ravanFaceBasReliefB_001_001:ravanFaceBasreliefB7CtrlOfst_Grp',	
						'ravanFaceSteelPassC_0010_001:AllMover_Ctrl',	
						'ravanFaceSteelPassB_002_001:AllMover_Ctrl',
						'ravanFaceSteelPassB_001_001:AllMover_Ctrl',	
						'ravanFaceSteelPassB_004_001:AllMover_Ctrl',
						'ravanFaceSteelPassB_003_001:AllMover_Ctrl',	
						'ravanFaceSteelPassA_0020_001:AllMover_Ctrl',
						'ravanFaceEye_0020_001:AllMover_Ctrl',
					]
	for each in listsJawUprBck1_R:
		utaCore.parentScaleConstraintLoop(obj= baseJawUprBck1_R, lists = [each],par = True, sca = True, ori = False, poi = False)


	print 'Generate HeadSub_Jnt'
	baseHeadSub = 'HeadSub_Jnt'
	listsHeadSub = ['ravanFaceBasreliefA_003_001:AllMover_Ctrl',
					'ravanFaceNoseStage_001_001:AllMover_Ctrl',
					'ravanFaceFaceCenter_001_001:ravanFaceFaceCenterBCtrlZro_Grp',
					'ravanFaceFaceCenter_001_001:ravanFaceFaceCenterECtrlZro_Grp',
					'ravanFaceFaceCenter_001_001:ravanFaceFaceCenterDCtrlZro_Grp',
					'ravanFaceFaceCenter_001_001:ravanFaceFaceCenterACtrlZro_Grp',
					'ravanFaceSteelPassB_0014_001:AllMover_Ctrl',
					'ravanFaceSteelPassB_0018_001:AllMover_Ctrl',
					'ravanFaceSteelPassB_0023_001:AllMover_Ctrl',
					'ravanFaceEye_0034_001:AllMover_Ctrl',
					'ravanFaceSteelPassB_0021_001:AllMover_Ctrl',
					'ravanFaceCheek_001_001:AllMover_Ctrl',
					'ravanFaceBasReliefB_002_001:AllMover_Ctrl',
					'ravanFaceFaceCenter_001_001:AllMover_Ctrl',
					'ravanFaceBasReliefB_001_001:AllMover_Ctrl',
					'ravanFaceMouth_002_001:AllMover_Ctrl',
					]
	for each in listsHeadSub:
		utaCore.parentScaleConstraintLoop(obj= baseHeadSub, lists = [each],par = True, sca = True, ori = False, poi = False)


	print 'addRig Rig D O N E'


def fkRigLocalWorld(*args):

	##-------------------------------------------Beard Lwr L--------------------------------------------------------
	##-------------------------------------------Beard Lwr L--------------------------------------------------------
	print "# Generate >> BeardAL"
	BeardAL = subRig.Run(
									name       = 'BeardA' ,
									tmpJnt     = 'beardA1_L_TmpJnt' ,
									parent     = 'jawLwr1_L_Jnt' ,
									ctrlGrp    =  'AddCtrl_Grp' ,
									shape      = 'stick' ,
									side       = 'L' ,
									size       = 1  
								)
	utaCore.assignColor(obj = [BeardAL.ctrl], col = 'red')
	mc.setAttr('{}.localWorld'.format(BeardAL.ctrl), 1)
	utaCore.lockAttrObj(listObj = [BeardAL.offsetGrp], lock = False,keyable = True)
	mc.parentConstraint(BeardAL.gmbl, 'ravanFaceCanine_001_001:AllMover_Ctrl', mo = True)
	mc.scaleConstraint(BeardAL.gmbl, 'ravanFaceCanine_001_001:AllMover_Ctrl', mo = True)

	print "# Generate >> BeardBL"
	BeardBL = subRig.Run(
									name       = 'BeardB' ,
									tmpJnt     = 'beardB1_L_TmpJnt' ,
									parent     = 'jawLwr1_L_Jnt' ,
									ctrlGrp    =  'AddCtrl_Grp' ,
									shape      = 'stick' ,
									side       = 'L' ,
									size       = 1  
								)
	utaCore.assignColor(obj = [BeardBL.ctrl], col = 'red')
	mc.setAttr('{}.localWorld'.format(BeardBL.ctrl), 1)
	utaCore.lockAttrObj(listObj = [BeardBL.offsetGrp], lock = False,keyable = True)
	mc.parentConstraint(BeardBL.gmbl, 'ravanFaceCanine_004_001:AllMover_Ctrl' , mo = True)
	mc.scaleConstraint(BeardBL.gmbl,'ravanFaceCanine_004_001:AllMover_Ctrl' , mo = True)


	print "# Generate >> BeardCL"
	BeardCL = subRig.Run(
									name       = 'BeardC' ,
									tmpJnt     = 'beardC1_L_TmpJnt' ,
									parent     = 'jawLwr1_L_Jnt' ,
									ctrlGrp    =  'AddCtrl_Grp' ,
									shape      = 'stick' ,
									side       = 'L' ,
									size       = 1  
								)
	utaCore.assignColor(obj = [BeardCL.ctrl], col = 'red')
	mc.setAttr('{}.localWorld'.format(BeardCL.ctrl), 1)
	utaCore.lockAttrObj(listObj = [BeardCL.offsetGrp], lock = False,keyable = True)
	mc.parentConstraint(BeardCL.gmbl, 'ravanFaceCanine_002_001:AllMover_Ctrl' , mo = True)
	mc.scaleConstraint(BeardCL.gmbl, 'ravanFaceCanine_002_001:AllMover_Ctrl' , mo = True)

	print "# Generate >> BeardDL"
	BeardDL = subRig.Run(
									name       = 'BeardDD1' ,
									tmpJnt     = 'beardD1_L_TmpJnt' ,
									parent     = 'jawLwr2_Jnt' ,
									ctrlGrp    =  'AddCtrl_Grp' ,
									shape      = 'stick' ,
									side       = '' ,
									size       = 1  
								)
	utaCore.assignColor(obj = [BeardDL.ctrl], col = 'red')
	mc.setAttr('{}.localWorld'.format(BeardDL.ctrl), 1)
	utaCore.lockAttrObj(listObj = [BeardDL.offsetGrp], lock = False,keyable = True)
	mc.parentConstraint(BeardDL.gmbl, 'ravanFaceCanineStage_003_001:AllMover_Ctrl' , mo = True)
	mc.scaleConstraint(BeardDL.gmbl, 'ravanFaceCanineStage_003_001:AllMover_Ctrl' , mo = True)


	##-------------------------------------------Beard Lwr R--------------------------------------------------------
	##-------------------------------------------Beard Lwr R--------------------------------------------------------
	print "# Generate >> BeardAR"
	BeardAR = subRig.Run(
									name       = 'BeardA' ,
									tmpJnt     = 'beardA1_R_TmpJnt' ,
									parent     = 'jawLwr1_R_Jnt' ,
									ctrlGrp    =  'AddCtrl_Grp' ,
									shape      = 'stick' ,
									side       = 'R' ,
									size       = 1  
								)
	utaCore.assignColor(obj = [BeardAR.ctrl], col = 'red')
	mc.setAttr('{}.localWorld'.format(BeardAR.ctrl), 1)
	utaCore.lockAttrObj(listObj = [BeardAR.offsetGrp], lock = False,keyable = True)
	mc.parentConstraint(BeardAR.gmbl, 'ravanFaceCanine_005_001:AllMover_Ctrl',  mo = True)
	mc.scaleConstraint(BeardAR.gmbl, 'ravanFaceCanine_005_001:AllMover_Ctrl', mo = True)

	print "# Generate >> BeardBR"
	BeardBR = subRig.Run(
									name       = 'BeardB' ,
									tmpJnt     = 'beardB1_R_TmpJnt' ,
									parent     = 'jawLwr1_R_Jnt' ,
									ctrlGrp    =  'AddCtrl_Grp' ,
									shape      = 'stick' ,
									side       = 'R' ,
									size       = 1  
								)
	utaCore.assignColor(obj = [BeardBR.ctrl], col = 'red')
	mc.setAttr('{}.localWorld'.format(BeardBR.ctrl), 1)
	utaCore.lockAttrObj(listObj = [BeardBR.offsetGrp], lock = False,keyable = True)
	mc.parentConstraint(BeardBR.gmbl, 'ravanFaceCanine_006_001:AllMover_Ctrl' , mo = True)
	mc.scaleConstraint(BeardBR.gmbl,'ravanFaceCanine_006_001:AllMover_Ctrl' , mo = True)


	print "# Generate >> BeardCR"
	BeardCR = subRig.Run(
									name       = 'BeardC' ,
									tmpJnt     = 'beardC1_R_TmpJnt' ,
									parent     = 'jawLwr1_R_Jnt' ,
									ctrlGrp    =  'AddCtrl_Grp' ,
									shape      = 'stick' ,
									side       = 'R' ,
									size       = 1  
								)
	utaCore.assignColor(obj = [BeardCR.ctrl], col = 'red')
	mc.setAttr('{}.localWorld'.format(BeardCR.ctrl), 1)
	utaCore.lockAttrObj(listObj = [BeardCR.offsetGrp], lock = False,keyable = True)
	mc.parentConstraint(BeardCR.gmbl, 'ravanFaceCanine_003_001:AllMover_Ctrl' , mo = True)
	mc.scaleConstraint(BeardCR.gmbl, 'ravanFaceCanine_003_001:AllMover_Ctrl' , mo = True)

	print "# Generate >> BeardDR"
	BeardDR = subRig.Run(
									name       = 'BeardDD2' ,
									tmpJnt     = 'beardD1_R_TmpJnt' ,
									parent     = 'jawLwr2_Jnt' ,
									ctrlGrp    =  'AddCtrl_Grp' ,
									shape      = 'stick' ,
									side       = '' ,
									size       = 1  
								)
	utaCore.assignColor(obj = [BeardDR.ctrl], col = 'red')
	mc.setAttr('{}.localWorld'.format(BeardDR.ctrl), 1)
	utaCore.lockAttrObj(listObj = [BeardDR.offsetGrp], lock = False,keyable = True)
	mc.parentConstraint(BeardDR.gmbl, 'ravanFaceCanineStage_004_001:AllMover_Ctrl' , mo = True)
	mc.scaleConstraint(BeardDR.gmbl, 'ravanFaceCanineStage_004_001:AllMover_Ctrl' , mo = True)


	##-------------------------------------------Beard Upr L--------------------------------------------------------
	##-------------------------------------------Beard Upr L--------------------------------------------------------
	print "# Generate >> BeardEL"
	BeardEL = subRig.Run(
									name       = 'BeardE' ,
									tmpJnt     = 'beardE1_L_TmpJnt' ,
									parent     = 'jawUpr2Pos_L_Jnt' ,
									ctrlGrp    =  'AddCtrl_Grp' ,
									shape      = 'stick' ,
									side       = 'L' ,
									size       = 1  
								)
	utaCore.assignColor(obj = [BeardEL.ctrl], col = 'red')
	mc.setAttr('{}.localWorld'.format(BeardEL.ctrl), 1)
	utaCore.lockAttrObj(listObj = [BeardEL.offsetGrp], lock = False,keyable = True)
	mc.parentConstraint(BeardEL.gmbl, 'ravanFaceCanineStage_001_001:AllMover_Ctrl' , mo = True)
	mc.scaleConstraint(BeardEL.gmbl, 'ravanFaceCanineStage_001_001:AllMover_Ctrl' , mo = True)

	##-------------------------------------------Beard Upr R--------------------------------------------------------
	##-------------------------------------------Beard Upr R--------------------------------------------------------
	print "# Generate >> BeardER"
	BeardER = subRig.Run(
									name       = 'BeardE' ,
									tmpJnt     = 'beardE1_R_TmpJnt' ,
									parent     = 'jawUpr2Pos_R_Jnt' ,
									ctrlGrp    =  'AddCtrl_Grp' ,
									shape      = 'stick' ,
									side       = 'R' ,
									size       = 1  
								)
	utaCore.assignColor(obj = [BeardER.ctrl], col = 'red')
	mc.setAttr('{}.localWorld'.format(BeardER.ctrl), 1)
	utaCore.lockAttrObj(listObj = [BeardER.offsetGrp], lock = False,keyable = True)
	mc.parentConstraint(BeardER.gmbl, 'ravanFaceCanineStage_002_001:AllMover_Ctrl' , mo = True)
	mc.scaleConstraint(BeardER.gmbl, 'ravanFaceCanineStage_002_001:AllMover_Ctrl' , mo = True)

	print 'fkRigLocalWorld D O N E'

# def wrapGroup(*args):
# 	listsJnt = ['HeadTip1Pos_Jnt', 'jawLwr1Pos_Jnt', 'jawUpr1Pos_Jnt', 'jawUpr1Pos_L_Jnt', 'jawUpr1Pos_R_Jnt', 'jawUprBck1Pos_Jnt', 'jawUprBck1Pos_L_Jnt','jawUprBck1Pos_R_Jnt']
# 	for each in listsJnt:
# 		mc.parent(each, 'HeadSub_Jnt')
# 	print 'wrapGroup D O N E'