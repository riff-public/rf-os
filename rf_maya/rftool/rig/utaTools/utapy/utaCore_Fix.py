import maya.cmds as mc
import maya.mel as mm 
from utaTools.pkmel import core as pc
reload(pc)
import shutil 
import os
import pymel.core as pm
from rf_utils.context import context_info
# reload(context_info)
from lpRig import mainGroup
reload(mainGroup)
from utaTools.utapy import DeleteGrp
from utaTools.pkmel import rigTools
from utaTools.utapy import listControlVis as lcv
reload(lcv)
from utaTools.utapy import listControlAmpFcl as lcaf
reload(lcaf)
from lpRig import core as lrc
reload(lrc)
from utaTools.pkmel import weightTools
reload(weightTools)
from lpRig import rigTools as lrr
reload(lrr)

#snap obj to obj
def snap(*args):
    sels =mc.ls(sl=True)
    mean =mc.parentConstraint(sels[0],sels[1],mo=False)
    means=mc.delete (mean)

def jointFromLocator (*args):
    #poseLocator = locator
    #sels = obj
    sels =mc.ls(sl=True)                                # select object selected
    for each in sels:                                   # search in selected
        poseLocator = mc.xform(each, q=True, t=True)    # find xform (x,y,z) 
        jntFrLoc = mc.joint (each, p=(poseLocator[0],poseLocator[1],poseLocator[2]))  # create joint from poseLocator(x,y,z)
        mean = mc.parent (jntFrLoc, w=True) 
        changName = each.split('_')                     # split '_'
        mc.rename(jntFrLoc, '%s_Jnt' % changName[0])    # chang new name obj               
        deleats = mc.delete (each)                      # delete object from selected each

        # print changName 

        # print mean  

def function(*args):
    # passdynamicHair ():
    #poseLocator = locator
    #sels = obj
    sels =mc.ls(sl=True)                                # select object selected
    for each in sels:                                   # search in selected
        poseLocator = mc.xform(each, q=True, t=True)    # find xform (x,y,z) 
        jntFrLoc = mc.joint (each, p=(poseLocator[0],poseLocator[1],poseLocator[2]))  # create joint from poseLocator(x,y,z)
        mean = mc.parent (jntFrLoc, w=True)             # unparent from w=world  mean unparent
        deleats = mc.delete (each)                      # delete object from selected each
        print mean 

def zeroGroup( obj = '' ,*args ) :
    # Create zero group
    chld = pc.Dag( obj )
    grp = pc.Null()
    grp.snap( chld )
    chld.parent( grp )
    return grp

def doZroGroup (*args):
    # Create zero group with naming
    sels = mc.ls( sl=True )
    sides = ( 'LFT' , 'RGT' , 'UPR' , 'LWR' )
    zroGrps = []
    
    for sel in sels :
        curr = pc.Dag( sel )
        prnt = curr.getParent()
        zGrp = zeroGroup( sel )
        
        nameLst = curr.name.split( '_' )
        currType = nameLst[-1]
        charName = ''
        midName = ''
        currSide = ''
        
        if len( nameLst ) == 3 :
            charName = '%s_' % nameLst[0]
            midName = nameLst[1]
        else :
            midName = nameLst[0]
        
        for side in sides :
            if side in midName :
                currSide = side
                midName = midName.replace( side , '' )
        
        zGrp.name = '%s%s%s%sZro%s_Grp' % ( charName ,
                                            midName ,
                                            currType[0].upper() ,
                                            currType[1:] ,
                                            currSide
                                        )
        
        if prnt :
                zGrp.parent( prnt )
            # print 'aaaa'
        
        zroGrps.append( zGrp )
    
        return zroGrps

def shadeCoppy (*args):
    sels =mc.ls(sl=True,l=True,nt=True,ap=True)
    print sels
  #for each in sels:
      #posi = mc.group (each,em=True)

def selected (*args):
    sels =mc.ls(sl=True,l=True,visible=True)
    print sels
    
def gmbl(obj,*args):
    sels = mc.ls(sl=True)
    for each in sels:   
        each.add( ln='gimbalControl' , min=0 , max=1 , k=True )
      # print (nameFun)
      # tag = mc.attributeInfo( 'geoVis', ui=True, h=True, i=True )
# def geoVis(obj):
#     sels = mc.ls(sl=True)
#     if sels = '' :
#         sels = obj
#     else:
#     for each in sels:   
#         each.add( ln='geoVis' , min=0 , max=1 , k=True )
#         print (nameFun)
#         tag = mc.attributeInfo( 'geoVis', ui=True, h=True, i=True )
# def dupu():
#     sels =mc.ls(sl=True)
#     for sel in sels:
#         let = mc.listConnections(sel)
#         for make in let:
#             nameList = mc.setAttr (let[0]) 10
#             print let
def attrEnum(obj = '', ln = '', en ='',*args):
    mc.addAttr( obj , ln = ln , at = 'enum' , en = en , k = True )

def attrName(obj = '', elem = '', *args):

    if not obj:
        sels = mc.ls(sl=True)
        obj = sels
        if mc.objExists(elem == ''):
            mc.addAttr(obj,ln = 'gimbalControl' , k = True)
        else:
            mc.addAttr(obj,ln = elem , k = True)

    else:

        if mc.objExists(elem == ''):
            mc.addAttr(obj, ln= 'gimbalControl', k=True)
        else:
            mc.addAttr(obj,ln = elem , k = True)

def attrNameMinMax (obj = '', elem = '', min = 0, max = 1, *args):

    if not mc.objExists(obj):
        sels = mc.ls(sl=True)
        obj = sels
        if not mc.objExists(elem):
            mc.addAttr(obj,ln = 'gimbalControl' , at = 'long', min = min, max = max , k = True)
        else:
            mc.addAttr(obj,ln = elem  , at = 'long', min = min, max = max, k = True)

    else:

        if mc.objExists(elem):
            mc.addAttr(obj, ln= 'gimbalControl', at = 'long', min = min, max = max, k=True)
        else:
            mc.addAttr(obj,ln = elem ,at = 'long', min = min , max = max, k = True)
    # if mc.objExists(ln):
    #     for each in sels:   
    #         each.add( ln='gimbalControl' , min=0 , max=1 , k=True )
    # else:
    #     for each in sels:
            
    #         each.add( ln= elem , min=0, max=1, k=True )
    #         print each

#find and delete keyFrame and vraySetting      
def clearKey(*args):
# select target1
    cur = mc.select(all=True)
# currentTime
    mc.currentTime (0)
# delete keyFrame from 0 to 2000
    mc.cutKey(time=(-200,2000)) 
    mc.delete ('vraySettings')
    mc.select(cl=True)


def cleanCamera(*args):  
    # clean Camera fot default
    cams = mc.ls(sl=True)
    for cam in cams:
        mc.camera(cam,e=True,startupCamera=False)
        mc.delete(cam)
        print 'delete %s' %(cam)
    
def chang4Kto1K(*args):
    #P-----------------------------------Find Texture 4K chang to 1K----------------------------
    files = mc.ls('*_aFile')
    for each in files : 
        path = mc.getAttr('%s.fileTextureName' % each)
        print path
    if '/4K/' in path : 
        newPath = path.replace('/4K/', '/1K/')
        mc.setAttr('%s.fileTextureName' % each, newPath, type = 'string')
        print 'set %s' % each

#Expression
#     #ES--------------------------------code  loop trainStaion -------------------------------------------------
#     railwayMoveIk_ikh.offset = trainStationOffset_ctrl.trainOffset*0.001;
#     int $round = railwayMoveIk_ikh.offset;
#     if (railwayMoveIk_ikh.offset > 1)
#         railwayMoveIk_ikh.offset = ((trainStationOffset_ctrl.trainOffset*0.001)-$round);

#     railwayMoveIk_ikh.offset = trainStationbogie1_loc.offset*0.001;
#     int $round = railwayMoveIk_ikh.offset;
#     if (railwayMoveIk_ikh.offset > 1)
#         railwayMoveIk_ikh.offset = ((trainStationbogie1_loc.offset*0.001)-$round);

def eyeLidFollow(*args):
    eyeList = mc.ls('*:eye_lft_jnt', '*:eye_rgt_jnt')
    eyeLidsList = mc.ls('*:EyeLidsBsh_L_Jnt', '*:EyeLidsBsh_R_Jnt')
    eyeCmp = mc.ls('*:EyeBallFollowIO_L_Cmp', '*:EyeBallFollowIO_R_Cmp',)
    eyelidFol =mc.ls('*:eye_rgt_ctrl', '*:eye_lft_ctrl')

    mc.parentConstraint(eyeList[0],eyeLidsList[0], mo=True)
    mc.parentConstraint(eyeList[1],eyeLidsList[1], mo=True)

    # set eyeLidsBsh
    mc.setAttr('%s.eyelidsFollow' % eyeLidsList[0], 1)
    mc.setAttr('%s.eyelidsFollow' % eyeLidsList[1], 1)

    # set lidFollow
    mc.setAttr('%s.lidFollow' % eyelidFol[0], 0)
    mc.setAttr('%s.lidFollow' % eyelidFol[1], 0)

    # L
    #mc.setAttr('%s.minG' % eyeCmp[0], -300)
    #mc.setAttr('%s.maxR' % eyeCmp[0], 300)
    # R
    #mc.setAttr('%s.minR' % eyeCmp[1], -300)
    #mc.setAttr('%s.maxG' % eyeCmp[1], 300)

def getUserFromIp(*args):
    """
    Get user name from data base (config) using the current machine ip.
        return: User name(str)
    """

    ipAddress = socket.gethostbyname(socket.gethostname())
    try :
        user = config.USER_IP[str(ipAddress)]
    except:
        user = 'ken'
    return "%s : %s" % (user, ipAddress)

def lockAttrs(listObj, lock,*args):
    # listEyes = ['EyeLidsBsh_L_Jnt','EyeLidsBsh_R_Jnt']
    attrs = ['t', 'r', 's' , 'eyelidsFollow']
#attrEyeLids = ["eyelidsFollow"]
    for each in listObj:
        if mc.objExists(each):
            for eyeJnt in listObj:
                for attr in attrs:
                    mc.setAttr( "%s.%s" %(eyeJnt, attr), lock = True)
                    #mc.setAttr('%s.v' %eyeJnt ,lock = 0)

def lockHideAttr(listObj, lock, keyable,*args):
    # listEyes = ['EyeLidsBsh_L_Jnt','EyeLidsBsh_R_Jnt']
    attrs = ['t', 'r', 's', 'v', 'squash', 'stretch', 'eyelidsFollow']
#attrEyeLids = ["eyelidsFollow"]
    for each in listObj:
        if mc.objExists(each):
            for eyeJnt in listObj:
                for each in attrs:
                    if mc.objExists(each):
                        for attr in attrs:
                            mc.setAttr( "%s.%s" %(eyeJnt, attr), lock = lock, keyable = keyable)
                            #mc.setAttr('%s.v' %eyeJnt ,lock = 0)

# unlock mdv Node
def lockMdv(listObj, lock,*args):
    attrsMdv = ['i1x', 'i1y', 'i1z', 'i2x' , 'i2y', 'i2z']
    for each in listObj:
        if mc.objExists(each):
            for listObjs in listObj:
                for attrsMdvs in attrsMdv:
                    mc.setAttr( "%s.%s" %(listObjs, attrsMdvs), lock = lock)
                    #mc.setAttr('%s.v' %listMdvs ,lock = 0)

def lockAttrObj(listObj, lock = True,*args):
    # listBow = ("bowARig_lft_grp","bowBRig_rgt_grp")
    i = 0 
    for each in listObj:
        for attr in ( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v') :
            for i in range(len(listObj)):
                    mc.setAttr("%s.%s" % (each, attr) , lock = lock, k = False)
        i += 1  

def lockAttr(listObj, attrs = [],lock = True,*args):
    # listBow = ("bowARig_lft_grp","bowBRig_rgt_grp")
    # attrs = ( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v')
    i = 0 
    for each in listObj:
        for attr in  attrs:
            for i in range(len(listObj)):
                mc.setAttr("%s.%s" % (each, attr) , lock = lock)
        i += 1  

def unlockAttrs(listObj = [], lock = True, attrs = '', *args):
    # listBow = ("bowARig_lft_grp","bowBRig_rgt_grp")
    # #attr = ( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v')
    for each in listObj:
        for attr in attrs :
            for i in range(len(listObj)):
                mc.setAttr("%s.%s" % (each, attr) , lock = lock)

def unInfluences (listObj,*args):
    # listObj = mc.ls("*:*_ply", "*:*_geo")
    # listObj = mc.ls("*_ply", "*_geo")
    # print listObj
    for each in listObj:
        if mc.objExists(each):
            showInfluence = mc.skinCluster(each, removeInfluence=True)
            print showInfluence
            # skinList = 
            # skinJntList = 
            for each in skinList:
                for skinJntLists in skinJntList:
                    mc.skinCluster (each , ri= skinJntLists , e=True )
    print ">>> clear <<<"

def getDataFld(*args) :      
    wfn = os.path.normpath( pc.sceneName() )
    tmpAry = wfn.split( '\\' )
    tmpAry[-2] = 'data' 
    dataFld = '\\'.join( tmpAry[0:-1] ) 
    if not os.path.isdir( dataFld ) :
        os.mkdir( dataFld ) 
        #print (dataFld)
    return dataFld     
def copyObjFile(src,*args):
    shutil.copy(src, getDataFld())

def clean ( target = '' , *args ) :
    sels = mc.ls(sl=True)
    selsList = len(sels)
    if mc.objExists (target):
        # freeze transform , delete history , center pivot
        pm.makeIdentity ( target , apply = True ) ;    
        pm.delete ( target , ch = True ) ;
        pm.xform ( target , cp = True ) ;
    else:
        if selsList >= 1:
            target = sels
            # freeze transform , delete history , center pivot
            pm.makeIdentity ( target , apply = True ) ;    
            pm.delete ( target , ch = True ) ;
            pm.xform ( target , cp = True ) ;
        else:
            print "Don't Freeze Tranform. Please Select Object.!!"  

def fixJawMthRigTZ(*args):
    lists = mc.ls("JawMthRig_Ctrl", "JawMthRigJntTzColl_Pma")
    try:
        mc.disconnectAttr("%s.translate.translateY" % lists[0], "%s.input1D[0]" % lists[1])
        mc.connectAttr("%s.translate.translateZ" % lists[0], "%s.input1D[0]" % lists[1])
    except: pass

def parentScaleConstraint(*args):
    sels = mc.ls(sl=True)
    mc.parentConstraint(sels[0], sels[1], mo=True)
    mc.scaleConstraint(sels[0], sels[1], mo=True)

def parentScaleConstraintLoop(obj = '', lists = [],par = True, sca = True, ori = True, poi = True, *args):
    if obj:
        if par == True:
            for each in lists :
                mc.parentConstraint(obj, each, mo=True)
        if sca == True:
            for each in lists :
                mc.scaleConstraint(obj, each, mo=True)
        if ori == True:
            for each in lists :
                mc.orientConstraint(obj, each, mo=True)
        if poi == True:
            for each in lists :
                mc.pointConstraint(obj, each, mo=True)

def upperFnt(*args):
    sels = mc.ls(sl=True)
    x=0
    for i in range(len(sels)):
        ken = mc.rename(sels[i].upper())
        x+=1
        # for x in sels[i]:
        #     # if x == range(0):
        #     ken = mc.rename(sels[i].upper())
        #     print ken
def scaleObj(*args):
    sels = mc.ls(sl=True)
    addList =['scaleX', 'scaleY', 'scaleZ']

    for each in sels: 
        obj = ('%sShape' % each)  
        # for adds in addList:
        #     obj.add(ln = obj , min = 0, max = 1, k = True)

# SearchNaming for select
def searchName(elem = '', *args):
    sels = mc.ls(sl=True)
    obj = []
    for sel in sels:
        if elem in sel:
            obj.append(sel)
            mc.select(obj)
        else:
            pass
# Run            
# searchName(elem = 'R')

def rivetToSeletedEdge(part=''):

    sels = mc.ls(sl=True, fl=True)
    rivet(part=part, fstEdge=sels[0], secEdge=sels[1])

#ConnectAttr Translate Rotate Scale
def connectTRS(trs = True, translate = True, rotate = True, scale = True, *args):
    sels = mc.ls(sl=True)
    lists = len(sels)

    if lists == 0:
        print 'Please Select Object 1 and Object 2'
    else:
        obj1 = sels[0]
        obj2 = sels[1]
        if (trs == True):
            mc.connectAttr('%s.translate' % obj1, '%s.translate' % obj2)
            mc.connectAttr('%s.rotate' % obj1, '%s.rotate' % obj2)
            mc.connectAttr('%s.scale' % obj1, '%s.scale' % obj2) 
            print obj1,'ConnectAttr T, R, S :' , obj2
        else:
            if (translate ==True):
                mc.connectAttr('%s.translate' % obj1, '%s.translate' % obj2) 
                print obj1,'ConnectAttr Translate:' , obj2
            elif (rotate ==True):
                mc.connectAttr('%s.rotate' % obj1, '%s.rotate' % obj2)  
                print obj1,'ConnectAttr Rotate:' , obj2
            elif (scale == True):
                mc.connectAttr('%s.scale' % obj1, '%s.scale' % obj2)  
                print obj1,'ConnectAttr Scale:' , obj2           


def assetName (*args):
    asset = context_info.ContextPathInfo()
    projectName = asset.path.name() if mc.file(q=True, sn=True) else ''
    nameObjs =  projectName.split('/')
    subType = nameObjs[-1]
    return subType
    # print 'SubType : ',subType

def mainGroupName (*args):
    # asset = context_info.ContextPathInfo()
    # projectName = asset.path.name() if mc.file(q=True, sn=True) else ''
    # # if maya is not projectName to pass
    #   #Exsample a = 1 if (1<0) else 2
    # print 'Creating Main Groups'
    # nameObjs =  projectName.split('/')
    # subType = nameObjs[-1]
    subType = 'Rig'
    mgObj = mainGroup.MainGroup(subType)
    print 'SubType : ',subType


def weaponRig2(jnts=['Rig:wristLFT_jnt', 'Rig:wristRGT_jnt']) :
    # Rigging weapons
    sels = mc.ls( sl=True )
    
    lHandLoc, rHandLoc = createWeaponLocator2(jnts)
    
    for sel in sels :
        
        currName = sel.split( '_' )[0]
        currName2 = currName.split ('Geo') [0]
        # print currName2
        
        ctrl = rigTools.jointControl( 'sphere' )
        ctrl.name = '%s_ctrl' % currName2
        ctrl.color = 'yellow'

        ctrl.add( ln='geoVis' , min=0 , max=1 , dv=1 , k=True )
        ctrl.attr( 'v' ).lock = True
        ctrl.attr( 'v' ).hide = True

        mc.connectAttr( '%s.geoVis' % ctrl , '%s.v' % sel )
        
        ctrlGrp = pc.group( ctrl )
        ctrlGrp.name = '%sCtrlZero_grp' % currName2
        
        # mc.skinCluster( ctrl , sel , tsb=True )
        mc.parentConstraint( ctrl , sel , mo=True )
        mc.scaleConstraint ( ctrl , sel , mo=True ) 
        
        lftGrp , rgtGrp = parentLeftRight( ctrl , lHandLoc.name , rHandLoc.name , ctrlGrp )
        lftGrp.name = '%sCtrlLeft_grp' % currName2
        rgtGrp.name = '%sCtrlRgt_grp' % currName2

        print currName2
        
def createWeaponLocator2(jnts) :
    # Creating location for mounting weapons at left hand and right hand
    lHandJnt = jnts[0]
    rHandJnt = jnts[1]

    lHand = pc.Locator()
    rHand = pc.Locator()

    poses = []
    for i in [lHandJnt,rHandJnt]:
        position = ''
        nsSplit = i.split(':')[-1]
        revElement = nsSplit.split('_')[0][::-1]

        for a in range(len(revElement)):
            if revElement[a].isupper():
                position += revElement[a]
            else:
                break
        poses.append(position[::-1])

    lHand.name = 'weapon%s_loc' %poses[0]
    rHand.name = 'weapon%s_loc' %poses[1]

    # lHand.attr( 't' ).v = ( 1.92 , 0 , 0.015 )
    # lHand.attr( 'rz' ).v = -15
    # rHand.attr( 't' ).v = ( -1.92 , 0 , 0.015 )
    # rHand.attr( 'rz' ).v = 15

    mc.delete( mc.pointConstraint( lHandJnt , lHand  ) )
    mc.delete( mc.pointConstraint( rHandJnt , rHand  ) )

    mc.parentConstraint( lHandJnt , lHand , mo=True )
    mc.scaleConstraint(lHandJnt , lHand , mo=True)
    mc.parentConstraint( rHandJnt , rHand , mo=True )
    mc.scaleConstraint(rHandJnt , rHand , mo=True)

    return lHand, rHand

    
def parentLeftRight( ctrl = '' , localObj = '' , worldObj = '' , parGrp = '' ) :
    # Blending parent between left hand and right hand.
    # Returns : locGrp , worGrp , worGrpParCons , parGrpParCons and parGrpParConsRev
    locGrp = pc.Null()
    worGrp = pc.Null()

    locGrp.snap( localObj )
    worGrp.snap( worldObj )

    worGrpParCons = pc.parentConstraint( worldObj , worGrp )
    worGrpParCons = pc.scaleConstraint( worldObj , worGrp )

    locGrpParCons = pc.parentConstraint( localObj , locGrp )
    locGrpParCons = pc.scaleConstraint( localObj , locGrp )

    parGrpParCons = pc.parentConstraint( locGrp , worGrp , parGrp )
    parGrpParCons2 = pc.scaleConstraint( locGrp , worGrp , parGrp , mo=True)
    parGrpParConsRev = pc.Reverse()

    con = pc.Dag( ctrl )

    attr = 'leftRight'
    con.add( ln = attr , k = True, min = 0 , max = 1 )
    con.attr( attr ) >> parGrpParCons.attr( 'w1' )
    con.attr( attr ) >> parGrpParConsRev.attr( 'ix' )
    parGrpParConsRev.attr( 'ox' ) >> parGrpParCons.attr( 'w0' )

    pc.clsl()

    return locGrp , worGrp  


def parentJnt (*args):
    lists = mc.ls(sl=True)
    check = lists[-1]
    objLists = len(lists)
    for idx, each in enumerate(lists):   #idx , enumerate(lists) : check index (i=0, i+=1)
        if idx+1 >= objLists:
            break
        mc.parent(lists[idx+1], each)

# Exsample  enumerate
    # my_list = ['apple', 'banana', 'grapes', 'pear']
    # for c, value in enumerate(my_list, 1):
    #     print(c, value)

# get NameSpace in scenes
def nameSpaceLists(*args):
    nsList = mc.namespaceInfo(lon = True)
    nameSp = []
    for lists in nsList:
        if lists in ['UI' , 'shared']:
            pass

        else:
            nameSp.append(lists)
    return nameSp

def getNs(nodeName=''):
    """
    Get namespace from given name
    Ex. aaa:bbb:ccc:ddd_grp > aaa:bbb:ccc:
    """
    ns = ''

    if '|' in nodeName:
        nodeName = nodeName.split('|')[-1]

    if ':' in nodeName:

        nmElms = nodeName.split(':')

        if nmElms:
            ns = '%s:' % ':'.join(nmElms[0:-1])

    return ns

def parentExportGrp(*args):
    exportGrp = 'Export_Grp'
    DeleteGrp.deleteGrp(obj = exportGrp)

def reNameCapitalize(*args):
    lists = mc.ls(sl=True)
    objLen = len(lists)
    reNameObj = []
    sameObj = []
    i = 0 
    if i <= objLen:
        for each in lists:
            spLists = each.split('_')
            if len(spLists) == 1:
                nameObj = spLists[0]  
                nameCapObj = nameObj.capitalize()
                obj = mc.rename(each,'%s_Geo' % nameCapObj)
                reNameObj.append(each)
            elif len(spLists) == 2:
                nameObj = spLists[0]
                lastNameObj = spLists[1]
                nameCapObj = nameObj.capitalize()
                lastNameCapObj = lastNameObj.capitalize()
                obj = mc.rename(each,'%s_%s' % (nameCapObj , lastNameCapObj))
                reNameObj.append(each)
            elif len(spLists) == 3:
                nameObj = spLists[0]
                sideObj = spLists[1]
                lastNameObj = spLists[2]
                nameCapObj = nameObj.capitalize()
                sideCapObj = sideObj.capitalize()
                lastNameCapObj = lastNameObj.capitalize()
                obj = mc.rename(each,'%s_%s_%s' % (nameCapObj , sideCapObj, lastNameCapObj)) 
                reNameObj.append(each)    
            else:
                sameObj.append(each)
    print "All Obj :      ", objLen
    print "Don't Re-Name :", len(sameObj)   
    print 'Re-Name :      ', len(reNameObj)

def openBook(name = 'BookUp1', valueBook1 = 1, valueBook2 = 0.7,valueBook3 = 0.5,valueBook4 = 0.3, rotateObj = 'rotateX',*args):
    lists = mc.ls(sl=True)
    obj1 = lists[0]
    obj2 = lists[1]
    splObj2 = obj2.split('_')
    numbers = (int(splObj2[1]))
    attrName(obj = obj1, elem = name)



    # elemMdv = mc.createNode('multiplyDivide', n = ('%s_Mdv' % name))
    attrs = 'X'
    valueBook = [valueBook1, valueBook2, valueBook3, valueBook4]
    i = 0
    u = 0
    for each in valueBook:
        # attrName(obj = (obj1+'Shape'), elem = (name+'Amp'+(u)))
        elemMdv = mc.createNode('multiplyDivide', n = ('%s%s_Mdv' % (name, u+1)))
        attrName(obj = ('%sShape' % obj1), elem = ('%s%s%s' % (name,'Amp',u+1)))
        ampListt = mc.setAttr('%sShape.%s%s%s' % (obj1 , name, 'Amp', u+1), valueBook[u] )
        print '%sShape.%s%s%s' % (obj1 , name, 'Amp', u+1)
        print '%s.input2X' % elemMdv
        mc.connectAttr('%sShape.%s%s%s' % (obj1 , name, 'Amp', u+1), ('%s.input2X' % elemMdv))
        mc.connectAttr('%s.%s' % (obj1, name), ('%s.input1%s' % (elemMdv, attrs)))
        mc.connectAttr('%s.output%s' % (elemMdv, attrs), ('%s%sOfst_%s_Grp.%s' % (splObj2[0], splObj2[2], (numbers + u), rotateObj)))

        u += 1

def connectBsh(baseBsh = 'Body_Geo', bufferGeo = 'BodyBffr_Geo', eyeBshGrp = 'EyeBshGeo_Grp', mthBshGrp = 'MthBshGeo_Grp', ebBshGrp = 'EbBshGeo_Grp', eyeCtrl = 'Eye_Ctrl',*args):
    objGeo = [eyeBshGrp, mthBshGrp, ebBshGrp]
    eyeBshGrpLists = mc.listRelatives(mc.ls(eyeBshGrp))
    mthBshGrpLists = mc.listRelatives(mc.ls(mthBshGrp))
    ebBshGrpLists = mc.listRelatives(mc.ls(ebBshGrp))
    numbersEyeBsh = len(eyeBshGrpLists)

    eyeObj = eyeBshGrp.split('BshGeo_Grp')
  

    if mc.objExists(bufferGeo):
        bffrSplit = bufferGeo.split('_')
        newNameBffr = ('%s%s%s' % (bffrSplit[0], 'Bsh', '_Geo'))
        mc.blendShape(bufferGeo, baseBsh, frontOfChain = True, name = newNameBffr)

    # for each in objGeo:
        # mc.select(each)

    if mc.objExists(eyeBshGrp):
        bshListsObj = []
        if mc.objExists(eyeBshGrp):
            mc.select(eyeBshGrpLists)   
            mc.select(bufferGeo, add= True)
            bshLists = mc.blendShape(name = '%sBsh_Geo' % eyeObj[0])
        if mc.objExists(eyeCtrl):
            attrName(obj = ('%sShape' % eyeCtrl), elem = ('%s%s' % (eyeObj[0],'Amp')))
            mc.setAttr('%sShape.%sAmp' % (eyeCtrl,eyeObj[0]), l = True)
            for each in eyeBshGrpLists:
                eachSplit = each.split('_')
                newNameBsh = ('%s%s' % (eachSplit[0], 'Bsh'))
                bshListsObj.append(newNameBsh)
                attrName(obj = eyeCtrl, elem = newNameBsh)

                attrName(obj = ('%sShape' % eyeCtrl), elem = ('%s%s' % (newNameBsh,'Amp')))
                elemMdv = mc.createNode('multiplyDivide', n = ('%s_Mdv' % each))
                mc.setAttr('%sShape.%sAmp' % (eyeCtrl,newNameBsh), 0.1)
                mc.connectAttr('%sShape.%sAmp' % (eyeCtrl,newNameBsh), '%s.input2X' % elemMdv)
                mc.connectAttr('%s.outputX' % elemMdv, '%s.%s' % ('%sBsh_Geo' % eyeObj[0],each))
                mc.connectAttr('%s.%s' % (eyeCtrl, newNameBsh), '%s.input1X' % elemMdv)
            mc.select(clear=True)

    # if mc.objExists(mthBshGrp):
    #     bshListsObj = []
    #     for each in mthBshGrpLists:
    #         eachSplit = each.split('_')
    #         newNameBsh = ('%s%s%s' % (eachSplit[0], 'Bsh', '_Geo'))
    #         bshListsObj.append(newNameBsh)
    #     if mc.objExists(mthBshGrp):
    #         obj = mthBshGrp.split('BshGeo_Grp')
    #         mc.select(mthBshGrpLists)   
    #         mc.select(bufferGeo, add= True)
    #         # print obj[0]
    #         mc.blendShape(name = '%sBsh_Geo' % obj[0])
    #         mc.select(clear=True)


            # print bshListsObj
        # mc.blendShape((mc.select(eyeBshGrp), (mc.select(baseBsh, add= True))))
    # if 
        # elemMdv = mc.createNode('multiplyDivide', n = ('%s%s_Mdv' % (each, 'Bsh')))
        # attrName(obj = ('%sShape' % obj1), elem = ('%s%s%s' % (name,'Amp',u+1)))
        # ampListt = mc.setAttr('%sShape.%s%s%s' % (obj1 , name, 'Amp', u+1), valueBook[u] )
        # print '%sShape.%s%s%s' % (obj1 , name, 'Amp', u+1)
        # print '%s.input2X' % elemMdv
        # mc.connectAttr('%sShape.%s%s%s' % (obj1 , name, 'Amp', u+1), ('%s.input2X' % elemMdv))
        # mc.connectAttr('%s.%s' % (obj1, name), ('%s.input1%s' % (elemMdv, attrs)))
        # mc.connectAttr('%s.output%s' % (elemMdv, attrs), ('%s%sOfst_%s_Grp.%s' % (splObj2[0], splObj2[2], (numbers + u), rotateObj)))

        # u += 1


def ExFaceProxy(paths = '', side = '', count = ''):
    
    mc.ExtractFace(mc.ls(sl=True))
    listObj = mc.ls(sl=True)
    if side != '':
        findObj = mc.ls('%s*_ProxyGeo_%s_Grp' % (paths, side))
    else:
        findObj = mc.ls('%s*_ProxyGeo_Grp' % (paths))
    # Thumb1_ProxyGeo_L_Grp
    if not findObj:
        checkList = mc.listRelatives(listObj[-2], p = True)
        mc.parent(listObj[-2], w = True)

        objLisRe = mc.listRelatives(listObj, p = True)
        if not len(objLisRe) == 1:
            if side != '':
                objGeo = mc.polyUnite(objLisRe, n='%s%s_Proxy_%s_Geo' % (paths, count , side))
                grpObj = mc.group (n = '%s%s_ProxyGeo_%s_Grp' % (paths, count, side), em = True)
            else:
                objGeo = mc.polyUnite(objLisRe, n='%s%s_Proxy_Geo' % (paths, count))    
                grpObj = mc.group (n = '%s%s_ProxyGeo_Grp' % (paths, count), em = True)            
            objGeo = mc.ls(objGeo[0])
            pm.delete ( objGeo , ch = True ) ; 
            pm.delete ( listObj[-2] , ch = True ) ; 
            mc.parent(objGeo, grpObj)
        else:
            objGeo = mc.ls(listObj[0])
            pm.delete ( objGeo , ch = True ) ; 
            pm.delete ( listObj[-2] , ch = True ) ; 

            if side != '':
                grpObj = mc.group (n = '%s%s_ProxyGeo_%s_Grp' % (paths, count, side), em = True)
                nameObj = mc.rename(objGeo,'%s%s_Proxy_%s_Geo' % (paths, count , side)) 
            else:
                grpObj = mc.group (n = '%s%s_ProxyGeo_Grp' % (paths, count), em = True)
                nameObj = mc.rename(objGeo,'%s%s_Proxy_Geo' % (paths, count))    
            mc.parent(nameObj, grpObj)     
            mc.delete(checkList)

        if mc.objExists('%sProxyGeo_%s_Grp' % (paths, side)):
            mc.parent(grpObj, '%sProxyGeo_%s_Grp' % (paths, side))
        else:
            if side != '':
                grpMain = mc.group(n = '%sProxyGeo_%s_Grp' % (paths, side), em = True)
            
            else:
                grpMain = mc.group(n = '%sProxyGeo_Grp' % paths, em = True)
            mc.parent(grpObj, grpMain)

    else:
        objGrp = findObj[-1]
        spObj = objGrp.split('_')
        countObj =  int(spObj[0].split(paths)[1])  # split Paths/Number
        objGeo = mc.ls(sl=True)
        # checkList = mc.listRelatives()
        mc.parent (objGeo[-2], w = True)
        pm.delete (objGeo[-2], ch = True ) ; 
        objLisRe = mc.listRelatives(objGeo, p = True)

        if len(objLisRe) > 1:
            if side != '':
                grpObj = mc.group (n = '%s%s_ProxyGeo_%s_Grp' % (paths, countObj+1 , side), em = True)
                comObj = mc.polyUnite(objLisRe, n='%s%s_Proxy_%s_Geo' % (paths, countObj+1 , side))
            else:
                grpObj = mc.group (n = '%s%s_ProxyGeo_Grp' % (paths, countObj+1 ), em = True)
                comObj = mc.polyUnite(objLisRe, n='%s%s_Proxy_Geo' % (paths, countObj+1))  
                              
            geoObj = mc.ls(comObj[-2])   
            pm.delete (geoObj , ch = True ) ; 
            mc.parent(geoObj, grpObj)
        else:
            checkList = mc.listRelatives(listObj[0], p =True)
            objGeo = mc.ls(listObj[0])
            pm.delete ( objGeo , ch = True ) ; 
            # mc.delete(checkList)
            # # # pm.delete ( listObj[-2] , ch = True ) ; 
            if side != '':
                grpObj = mc.group (n = '%s%s_ProxyGeo_%s_Grp' % (paths, countObj+1 , side), em = True)
                nameObj = mc.rename(objGeo,'%s%s_Proxy_%s_Geo' % (paths, countObj+1 , side))

            else:
                grpObj = mc.group (n = '%s%s_ProxyGeo_Grp' % (paths, countObj+1), em = True)
                nameObj = mc.rename(objGeo,'%s%s_Proxy_Geo' % (paths, countObj+1))

            mc.parent(nameObj, grpObj)
            mc.delete(checkList)
        if side != '':    
            if mc.objExists('%sProxyGeo_%s_Grp' % (paths, side)):
                mc.parent(grpObj, '%sProxyGeo_%s_Grp' % (paths, side))
            else:
                grpMain = mc.group(n = '%sProxyGeo_%s_Grp' % (paths, side), em = True)
                mc.parent(grpObj, grpMain)
        else:
            if mc.objExists('%sProxyGeo_Grp' % paths):
                mc.parent(grpObj, '%sProxyGeo_Grp' % paths)
            else:
                grpMain = mc.group(n = '%sProxyGeo_Grp' % paths, em = True)
                mc.parent(grpObj, grpMain)

        if len(objLisRe) < 1:
            objGeo = mc.ls(sl=True)
            pm.delete ( objGeo , ch = True ) ; 
        #     pm.delete ( listObj[-1] , ch = True ) ;
        #     # pm.delete ( listObj[-2] , ch = True ) ; 
            grpObj = mc.group (n = '%s%s_ProxyGeo_%s_Grp' % (paths, countObj+1 , side), em = True)
            mc.parent(objGeo, grpObj)
            if mc.objExists('%sProxyGeo_%s_Grp' % (paths, side)):
                mc.parent(grpObj, '%sProxyGeo_%s_Grp' % (paths, side))
            else:
                grpMain = mc.group(n = '%sProxyGeo_%s_Grp' % (paths, side), em = True)
                mc.parent(grpObj, grpMain)

def extractFace (*args):
    mc.ExtractFace(mc.ls(sl=True))
    obj = mc.listRelatives(mc.ls(sl=True), p = True)
    objxx = mc.listRelatives(obj,c = True)
    mc.parent(objxx[-2], w = True)

def layerControlRun(ns = '', obj = '', shape = True, *args):
    titalListAttr, listAttr, titalListFcl, listFcl = lcv.layerControlVis()
    mins = 0 
    maxs = 1
    shapes = 'Shape'
    ctrlShape = '%s%s%s' % (ns, obj, shapes)
    ctrl = '%s%s' % (ns, obj)
    i = 0 
    if shape  == True:
        if obj:
            ## Create Attrs Control Visibility
            attrNameMinMax(obj = ctrlShape, elem = titalListAttr, min = mins, max = maxs )
            mc.setAttr('%s%s%s.%s' % (ns, obj, shapes, titalListAttr), l = True)
            for each in listAttr:
                attrNameMinMax(obj = ctrlShape, elem = each, min = mins, max = maxs )
                # print obj, 'kkk', each
                mc.setAttr('%s%s.%s' % (obj, shapes, each), 1)
            ## Create FacialVis Attr
            attrNameMinMax(obj = ctrlShape, elem = titalListFcl, min = mins, max = maxs )
            mc.setAttr('%s%s%s.%s' % (ns, obj, shapes, titalListFcl), l = True)
            for each in listFcl:
                attrNameMinMax(obj = ctrlShape, elem = each, min = mins, max = maxs )
                mc.setAttr('%s%s.%s' % (obj, shapes, each), 1)
            i += 1
    else:
        if obj:
            ## Create Attrs Control Visibility
            attrNameMinMax(obj = ctrl, elem = titalListAttr, min = mins, max = maxs )
            mc.setAttr('%s%s.%s' % (ns, obj, titalListAttr), l = True)
            for each in listAttr:
                attrNameMinMax(obj = ctrl, elem = each, min = mins, max = maxs )
                mc.setAttr('%s.%s' % (obj, each), 1)
            ## Create FacialVis Attr
            attrNameMinMax(obj = ctrl, elem = titalListFcl, min = mins, max = maxs )
            mc.setAttr('%s%s.%s' % (ns, obj, titalListFcl), l = True)
            for each in listFcl:
                attrNameMinMax(obj = ctrl, elem = each, min = mins, max = maxs )
                mc.setAttr('%s.%s' % (obj, each), 1)
            i += 1

    ## Unlock Group for SetAttr layerControl
    # Generate >> Unlock Group For SetAttr layerControl
    # layerControlUnlock()

def setControlVis(*args):
    lists = 'AllMover_Ctrl'
    titalListAttr, listAttr, titalListFcl, listFcl = lcv.layerControlVis()
    if mc.objExists(titalListAttr):
        if lists:
            for each in listAttr:
                if each == listAttr[0]:
                    pass
                else:
                    mc.setAttr('%sShape.%s' % (lists, each), 0)
            for each in listFcl:
                mc.setAttr('%sShape.%s' % (lists, each), 0)

    ## SetAttr AnkleIk
    listObj = [ 'AnkleIk_L_Ctrl', 
                'AnkleIk_R_Ctrl']
    listAttrs = 'toeBreak' 
    i = 0 
    if listAttrs:
        for sel in listObj:
            mc.setAttr ("%sShape.%s"% (sel, listAttrs), -100)
            print "Generate >> SetAttrs For Ankle%s" % sel
            i += 1 

def createAmpEye(ns = '', process = '', obj = '', shape = True, min = 0, max = 50, *args):
    titalListAttr, listAttrL, listAttrR, eyeRigAmp= lcaf.selectControlAmp(process = process)
    mins = min 
    maxs = max
    shapes = 'Shape'
    ctrlShape = '%s%s%s' % (ns, obj, shapes)
    ctrl = '%s%s' % (ns, obj)
    i = 0 
    if shape  == True:
        if obj:
            ## Create Attrs Control Visibility
            attrNameMinMax(obj = ctrlShape, elem = titalListAttr, min = mins, max = maxs )
            mc.setAttr('%s%s%s.%s' % (ns, obj, shapes, titalListAttr), l = True)
            for each in listAttrL:
                attrNameMinMax(obj = ctrlShape, elem = each, min = mins, max = maxs )
                mc.setAttr('%s%s.%s' % (obj, shapes, each), eyeRigAmp[each])
            i += 1
    else:
        if obj:
            ## Create Attrs Control Visibility
            attrNameMinMax(obj = ctrl, elem = titalListAttr, min = mins, max = maxs )
            mc.setAttr('%s%s.%s' % (ns, obj, titalListAttr), l = True)
            for each in listAttrL:
                attrNameMinMax(obj = ctrl, elem = each, min = mins, max = maxs )
                mc.setAttr('%s.%s' % (obj, each), eyeRigAmp[each])
            i += 1

## Get Path File and Open File
def openFileWorldSpace(*args):
    path = mc.file(q=True, sn=True)
    mc.file(path, o=True, f=True)


def connectLayerControlVis(obj = '', shape = True, *args):
    # listAttr, listFcl, listMasterAttr, listBlockAttr, listSeconAttr, listFclAttr = lcv.listControlVis()
    listAttr , listFcl , listGeo, listMasterAttr ,listBlockAttr, listSeconAttr, listsAddRigAttr ,listFclAttr , listsDtlAttr , listGeoAttr = lcv.listControlVis()
    mins = 0 
    maxs = 1
    shapes = 'Shape'
    objCtrlShapes = mc.ls('*:%s%s' % (obj, shapes))
    if objCtrlShapes:
        objCtrlShape = objCtrlShapes
    else:
        objCtrlShape = '%s%s' % (obj, shapes)
    objCtrl = mc.ls('*:%s' % obj)
    # print objCtrl, '---'
    # nsList = getNs(nodeName = objCtrl[0])
    i = 0 
    if (shape == True):
        if obj :
            for sel in  listMasterAttr:
                try:
                    selList = mc.ls('*:%s' % sel)
                    if selList:
                        selNsList = getNs(nodeName = selList[0])
                        mc.connectAttr('%s.%s' % (objCtrlShape, listAttr[0]), '%s%s.v' %  (selNsList,sel) )
                    else:
                        mc.connectAttr('%s.%s' % (objCtrlShape, listAttr[0]), '%s.v' % sel )
                except:
                    pass
            i += 1

            for sel in  listBlockAttr:
                try:
                    selList = mc.ls('*:%s' % sel)
                    if selList:
                        selNsList = getNs(nodeName = selList[0])                    
                        mc.connectAttr('%s.%s' % (objCtrlShape, listAttr[1]), '%s%s.v' %  (selNsList,sel) )
                    else:
                        mc.connectAttr('%s.%s' % (objCtrlShape, listAttr[1]), '%s.v' %  sel)

                except:
                    pass
            i += 1

            for sel in  listSeconAttr:
                try:
                    selList = mc.ls('*:%s' % sel)
                    if selList:
                        selNsList = getNs(nodeName = selList[0])  
                        mc.connectAttr('%s.%s' % (objCtrlShape, listAttr[2]), '%s%s.v' %  (selNsList,sel) )
                    else:
                        mc.connectAttr('%s.%s' % (objCtrlShape, listAttr[2]), '%s.v' %  sel)                        
                except:
                    pass
            i += 1

            for sel in  listsAddRigAttr:
                try:
                    selList = mc.ls('*:%s' % sel)
                    if selList:
                        selNsList = getNs(nodeName = selList[0])  
                        mc.connectAttr('%s.%s' % (objCtrlShape, listAttr[3]), '%s%s.v' %  (selNsList,sel) )
                    else:
                        mc.connectAttr('%s.%s' % (objCtrlShape, listAttr[3]), '%s.v' %  sel)                        
                except:
                    pass
            i += 1

            for sel in  listFclAttr:
                try:
                    selList = mc.ls('*:%s' % sel)
                    if selList:
                        selNsList = getNs(nodeName = selList[0])  
                        mc.connectAttr('%s.%s' % (objCtrlShape, listFcl[0]), '%s%s.v' %  (selNsList,sel) )
                    else:
                        mc.connectAttr('%s.%s' % (objCtrlShape, listFcl[0]), '%s.v' %  sel)                        
                except:
                    pass
            i += 1

            for sel in  listsDtlAttr:
                try:
                    selList = mc.ls('*:%s' % sel)
                    if selList:
                        selNsList = getNs(nodeName = selList[0])  
                        mc.connectAttr('%s.%s' % (objCtrlShape, listFcl[1]), '%s%s.v' %  (selNsList,sel) )
                    else:
                        mc.connectAttr('%s.%s' % (objCtrlShape, listFcl[1]), '%s.v' %  sel)                        
                except:
                    pass
            i += 1

    else:
        if obj :
            for sel in  listMasterAttr:
                try:
                    selList = mc.ls('*:%s' % sel)
                    if selList:
                        selNsList = getNs(nodeName = selList[0])
                        mc.connectAttr('%s.%s' % (obj, listAttr[0]), '%s%s.v' %  (selNsList,sel) )
                    else:
                        mc.connectAttr('%s.%s' % (obj, listAttr[0]), '%s.v' % sel )
                except:
                    pass
            i += 1

            for sel in  listBlockAttr:
                try:
                    selList = mc.ls('*:%s' % sel)
                    if selList:
                        selNsList = getNs(nodeName = selList[0])                    
                        mc.connectAttr('%s.%s' % (obj, listAttr[1]), '%s%s.v' %  (selNsList,sel) )
                    else:
                        mc.connectAttr('%s.%s' % (obj, listAttr[1]), '%s.v' %  sel)

                except:
                    pass
            i += 1

            for sel in  listSeconAttr:
                try:
                    selList = mc.ls('*:%s' % sel)
                    if selList:
                        selNsList = getNs(nodeName = selList[0])  
                        mc.connectAttr('%s.%s' % (obj, listAttr[2]), '%s%s.v' %  (selNsList,sel) )
                    else:
                        mc.connectAttr('%s.%s' % (obj, listAttr[2]), '%s.v' %  sel)                        
                except:
                    pass
            i += 1

            for sel in  listsAddRigAttr:
                try:
                    selList = mc.ls('*:%s' % sel)
                    if selList:
                        selNsList = getNs(nodeName = selList[0])  
                        mc.connectAttr('%s.%s' % (obj, listAttr[3]), '%s%s.v' %  (selNsList,sel) )
                    else:
                        mc.connectAttr('%s.%s' % (obj, listAttr[3]), '%s.v' %  sel)                        
                except:
                    pass
            i += 1

            for sel in  listFclAttr:
                try:
                    selList = mc.ls('*:%s' % sel)
                    if selList:
                        selNsList = getNs(nodeName = selList[0])  
                        mc.connectAttr('%s.%s' % (obj, listFcl[0]), '%s%s.v' %  (selNsList,sel) )
                    else:
                        mc.connectAttr('%s.%s' % (obj, listFcl[0]), '%s.v' %  sel)                        
                except:
                    pass
            i += 1

            for sel in  listsDtlAttr:
                try:
                    selList = mc.ls('*:%s' % sel)
                    if selList:
                        selNsList = getNs(nodeName = selList[0])  
                        mc.connectAttr('%s.%s' % (obj, listFcl[1]), '%s%s.v' %  (selNsList,sel) )
                    else:
                        mc.connectAttr('%s.%s' % (obj, listFcl[1]), '%s.v' %  sel)                        
                except:
                    pass
            i += 1

def layerControlUnlock(*args):
    listAttr , listFcl , listGeo, listMasterAttr ,listBlockAttr, listSeconAttr, listsAddRigAttr ,listFclAttr , listsDtlAttr , listGeoAttr   = lcv.listControlVis()
    # print listFclAttr, 'listFclAttr'[
    # listAll = []
    # for each in listMasterAttr, listBlockAttr, listSeconAttr, listFclAttr, listsAddRigAttr, listGeoAttr:
    #     listAttr.append(each)
    # print listAll, 'listAll'
    i = 0 

    for sel in  listMasterAttr:
        try:
            mc.setAttr('%s.v' % (sel), l = False)
        except:
            pass
    i += 1

    for sel in listBlockAttr:
        try:
            mc.setAttr('%s.v' % (sel), l = False)
        except:
            pass
    i += 1
    for sel in listSeconAttr:
        try:
            mc.setAttr('%s.v' % (sel), l = False)
        except:
            pass
    i += 1

    for sel in listFclAttr:
        try:
            #print sel, "sel"
            mc.setAttr('%s.v' % (sel), l = False)
        except:
            pass
    i += 1

def nonRollFk(      elem = '',
                    side = '',
                    objAttrs = '', 
                    source = '', 
                    target = '',
                    valueAmp = 0.8,
                    mins = 0,
                    maxs = 90,
                    axisSource = '',
                    axisTarget = '',
                    rotate = True,
                    translate = False,
                     *args):
    try:
        lockAttrObj(listObj = [target],lock = False)
    except:
        pass
    if rotate == True:
        attsObj  = 'rotate'
        parentList = mc.listConnections('%s.%s%s' % (target, attsObj, axisTarget), s = True, type = 'unitConversion')

    if translate == True:
        attsObj = 'translate'
        parentList = mc.listConnections('%s.%s%s' % (target, attsObj, axisTarget), s = True)

    objAttrsList = '%s_%s' % (elem, side)
    onOffAtr = 'AutoFk'

    if objAttrs:
        listAttrObj = mc.listAttr(objAttrs, s = True, r = True, k = True)

        ## Create MultiplyDivice 1
        elemMdv = mc.createNode('multiplyDivide', n = '%sNonRoll_Mdv' % elem)
        mc.connectAttr('%s.rotate%s' % (source, axisSource), '%s.input1X' % elemMdv)

        if onOffAtr in listAttrObj:
            ## Connect Attr OnOff
            mc.connectAttr('%s.%s' % (objAttrs, onOffAtr), '%s.input2X' % elemMdv)

        else:
            ## Create OnOff
            attrNameMinMax (obj = objAttrs, elem = onOffAtr, min = 0, max = 1)
            mc.setAttr("%s.%s" % (objAttrs, onOffAtr), 1)
            ## Connect Attr OnOff
            mc.connectAttr('%s.%s' % (objAttrs, onOffAtr), '%s.input2X' % elemMdv)

        ## Create Attr
        attrName(obj = objAttrs, elem = objAttrsList)   
        mc.setAttr("%s.%s" % (objAttrs, objAttrsList), valueAmp)

        ## Create Clamp
        elemClm = mc.createNode('clamp', n = '%sNonRoll_Clm' % elem)
        mc.setAttr ('%s.minR' % elemClm, mins)
        mc.setAttr ('%s.maxR' % elemClm, maxs)
        mc.connectAttr('%s.outputX' % elemMdv, '%s.inputR' % elemClm)
        ## Create MultiplyDivice 2
        elemDvMdv = mc.createNode('multiplyDivide', n = '%sDvNonRoll_Mdv' % elem)
        mc.connectAttr('%s.%s' % (objAttrs,objAttrsList), '%s.input2X' % elemDvMdv)
        mc.connectAttr('%s.outputR' % elemClm,  '%s.input1X' % elemDvMdv)

        # ## Check Connection at Attr
        # print attsObj, 'attsObj'
        # print target, 'target'
        # print axisTarget, 'axisTarget'
        # lists = '%s.%s%s' % (target, attsObj, axisTarget)
        # print lists, 'lists'
        # if rotate == True:
        #     parentList = mc.listConnections('%s.%s%s' % (target, attsObj, axisTarget), s = True, type = 'unitConversion')
        # if translate == True :
        #     parentList = mc.listConnections('%s.%s%s' % (target, attsObj, axisTarget), s = True)
        # # print parentList, 'parentList'
        if rotate :
            # print '1'
            if parentList :
                # print '2'
                ll = mc.listConnections('%s.%s%s' % (target, attsObj, axisTarget), s = True, type = 'unitConversion')
                ken = mc.listConnections( ll, s = True, type = 'plusMinusAverage')
                zz = mc.listConnections(ken[0],d = False, c = True )

                ## fine input2D
                ken2 = zz[-2].split('.')
                objInput = ken2[-2]
                ken3 = int(objInput[-2])+1
                renameKen4 = 'input2D[%s]' % ken3

                mc.connectAttr('%s.outputX' % elemDvMdv, '%s.%s.input2Dx' % (ken2[0], renameKen4))

            else:
                ## Create PlusMinusAverage 
                # print '3'
                elemPma = mc.createNode( 'plusMinusAverage', n = '%sNonRoll_Pma' % elem)
                mc.connectAttr('%s.outputX' % elemDvMdv, '%s.input2D[0].input2Dx' % elemPma)
                mc.connectAttr('%s.output2Dx' % elemPma, '%s.%s%s' % (target, attsObj, axisTarget))

        if translate:
            # print'1.1'
            if parentList:
                # print '1.2'
                # ll = mc.listConnections('%s.%s%s' % (target, attsObj, axisTarget), s = True, type = 'unitConversion')
                # ken = mc.listConnections( ll, s = True, type = 'plusMinusAverage')
                # print parentList, 'parentList Translate'
                zz = mc.listConnections(parentList[0],d = False, c = True )

                ## fine input2D
                ken2 = zz[-2].split('.')
                objInput = ken2[-2]
                ken3 = int(objInput[-2])+1
                renameKen4 = 'input2D[%s]' % ken3

                mc.connectAttr('%s.outputX' % elemDvMdv, '%s.%s.input2Dx' % (ken2[0], renameKen4))

            else:
                # print '3.1'
                ## Create PlusMinusAverage 
                elemPma = mc.createNode( 'plusMinusAverage', n = '%sNonRoll_Pma' % elem)
                mc.connectAttr('%s.outputX' % elemDvMdv, '%s.input2D[0].input2Dx' % elemPma)
                mc.connectAttr('%s.output2Dx' % elemPma, '%s.%s%s' % (target, attsObj, axisTarget))

def setDefaultControl(char = '', *args):
    ## SetAttr
    ArmList = ['Arm_L_Ctrl', 'Arm_R_Ctrl']
    SpineList = ['Spine_Ctrl']
    LegList = ['Leg_L_Ctrl', 'Leg_R_Ctrl']
    GrpList = ['Ctrl_Grp', 'Skin_Grp', 'Jnt_Grp', 'Ikh_Grp', 'Still_Grp', ]
    ## Arm
    for eachArm in ArmList:
        mc.setAttr('%s.fkIk' % eachArm, 0)

    ## Spine
    for eachSpine in SpineList:
        mc.setAttr('%s.fkIk' % eachSpine, 1)

    ## Leg
    for eachLeg in LegList:
        mc.setAttr('%s.fkIk' % eachLeg, 1)

    ## SetAttr AllMover Vis
    # listAttr, listFcl, listMaster, listBlock, listSecon = lcv.listControlVis()
    listAttr, listFcl, listMasterAttr, listBlockAttr, listSeconAttr, listsAddRigAttr, listFclAttr, listsDtlAttr = lcv.listControlVis()
    AllMoverObj = '%s_Crv' % char
    for eachListAttr in listAttr[0],listAttr[1]:
        mc.setAttr('%s.%s' % (AllMoverObj, eachListAttr), 1)
    for eachListAttr in listAttr[2],listAttr[-1]:
        mc.setAttr('%s.%s' % (AllMoverObj, eachListAttr), 0)
    for eachListFcl in listFcl:
        mc.setAttr('%s.%s' % (AllMoverObj, eachListFcl), 0)

    ## SetAttr Visibility
    for each in GrpList[0]:
        lockAttr(listObj = each, attrs = ['v'],lock = False)
    for each in GrpList[1], GrpList[-1]:
        lockAttr(listObj = each, attrs = ['v'],lock = True)

def combineCurves(char = '', curves = []):
    crvGrp = mc.group(n = '%s_Crv' % char, em=True)
    for crv in curves:
        crvShape = mc.listRelatives(crv, shapes = True)
        mc.parent(crvShape,crvGrp,s=True,r=True, relative=True)
        if curves[-1]:
            mc.parent(crv, crvGrp)
        mc.delete(crv) 
    ## Rename CurveShape1 >> CharNameShape
    mc.rename ("curveShape1","%sShape" % crvGrp)   
    ## LockAttr 
    lockAttrObj(listObj = [crvGrp], lock = True) 
    mc.setAttr(crvGrp + '.overrideEnabled', 1)
    mc.setAttr(crvGrp + '.overrideColor', 19)
    pm.delete ( crvGrp , ch = True )
    return  crvGrp

def textName(char = '', size = '',*args):
    if char == '':
        ## find charName
        asset = context_info.ContextPathInfo()         
        char = asset.name
        if char == '':
            print 'Please Check File Path'
            # char = 'TEST'

    fontText = 'MS Shell Dlg 2'
    txtCrv = mc.textCurves( n = '%s_' %char , f = fontText , t = char )
    listChi = mc.listRelatives(txtCrv[0], type = 'transform', c = True, ad = True, s = False)
    crvGrp = mc.createNode ('transform', n = '%sCrv_Grp' % char, )
    mc.select(cl = True)
    listGrp =[]
    listCrvObj = []
    for each in listChi:
        if '_1' in each :
            listConObj = mc.listConnections(each, s = True)
            listParObj = mc.listConnections(each,d = False, c = True ,p  = True)
            listSplitObj = listParObj[-1].split('.')
            objConnect = listSplitObj[-1]
            listNum = int(objConnect[-2])
            positionCon = 'position[%s]' % listNum
            # mc.disconnectAttr('%s.%s' % (listConObj[0], positionCon ), '%s.t'% each)

            listGrp.append(each)
        if 'curve' in each:
            listCrvObj.append(each)        

    mc.parent(listCrvObj, w = True)
    mc.select(crvGrp, add = True)
    mc.parent(listCrvObj, crvGrp)   
    for each in listCrvObj:       
        if 'curve' in each:
            ## Freeztransform
            mc.makeIdentity (each , apply = True , jointOrient = False, normal = 1, translate =True, rotate = True, scale = True) 
            ## Reset Transform
            mc.move("%s.scalePivot" % each,"%s.rotatePivot" % each, absolute=True) 
    ## Combine Curve
    crvUse = combineCurves(char = char, curves = listCrvObj)
    mc.parent(crvUse, crvGrp)
    mc.setAttr('%s.s' % crvGrp, float(size),float(size),float(size))
    mc.xform(crvGrp, cp=1)
    mc.delete('%s_Shape' % char)


def tyHeadEnd(char = '', value = '', *args):
    headEnd = 'HeadEnd_Jnt'
    textName = '%sCrv_Grp' % char
    mc.delete(mc.parentConstraint(headEnd, textName))
    lists = mc.xform(textName, q=True, t=True) 
    values =  lists[1]+float(value)
    mc.setAttr('%s.ty' % textName, values)
    # mc.setAttr('%s.tx' % textName, -1)
    # return values

def copyOneToTwoSkinWeight(*args):
    listaA = mc.ls(sl=True)
    listsB = mc.ls(sl=True)
    i = 0
    for i in range(len(listsB)):
        #print i
        mc.select(listaA[i])
        mc.select(listsB[i], add = True)
        weightTools.copySelectedWeight()
        print listaA[i], '>>' , listsB[i]
    i += 1

def copySkinWeightToPosition(*args):
    lists = mc.ls(sl=True)
    for each in lists:
        mc.select(each)
        mc.select('Fur1_Geo_Proxy3' , add = True)
        lrr.copyWeightBasedOnWorldPosition()

def addFurGuide(BodyGeo = '', BodyFurGeo = '', BodyFclRigWrap = '', *args):
    # Bsh Body_Geo to BodyDtlCtrl  
    stillRigGrp = mc.ls("*:Still_Grp")

    if BodyGeo == '':
        BodyGeo = mc.ls("*:Body_Geo")
        BodyFurGeo = mc.ls("*:BodyFur_Geo")
        BodyFclRigWrap = mc.ls("BodyFclRigWrapped_Geo")

    # Duplicate BodyFurFcl_Geo, BodyFurGuide_Geo
    dupBodyGeo = mc.duplicate(BodyGeo, n = 'BodyFurFcl_Geo')
    dupBodyFurGuideGeo = mc.duplicate(BodyFurGeo, n = 'BodyFurGuide_Geo')
    mc.parent( dupBodyGeo[0], dupBodyFurGuideGeo[0], stillRigGrp)
    mc.setAttr('%s.v' % dupBodyGeo[0], 0)
    mc.setAttr('%s.v' % dupBodyFurGuideGeo[0], 0)
    # Bsh 
    mc.select(dupBodyFurGuideGeo[0], r = True)
    mc.select(dupBodyGeo[0], add = True)
    lrr.doAddBlendShape()

    # mc.select(BodyGeo, r = True)
    # mc.select(dupBodyGeo[0], add = True)
    # lrr.doAddBlendShape()

    mc.select(BodyFclRigWrap, r = True)
    mc.select(dupBodyGeo[0], add = True)
    lrr.doAddBlendShape()

    mc.select(dupBodyGeo[0], r = True)
    mc.select(BodyFurGeo, add = True)
    lrr.doAddBlendShape()

def connectObjMirror(translate = True, rotate = True, valueA = -1, valueB = 1,*args):
    lists = mc.ls(sl = True)
    if translate == True:
        for each in lists:
            objSp =each.split('_')
            mdvNode = mc.createNode ('multiplyDivide', n = ('%sTr_%s_Mdv' % (objSp[0], objSp[1])))
            mc.setAttr('%s.input2.input2X' % mdvNode, valueA)
            #mc.setAttr('%s.input2.input2X' % mdvNode, valueB)
            #mc.setAttr('%s.input2.input2X' % mdvNode, valueB)
            mc.connectAttr('%s.translate.translateX' % each, '%s.input1.input1X' % mdvNode)
            mc.connectAttr('%s.translate.translateY' % each, '%s.input1.input1Y' % mdvNode)
            mc.connectAttr('%s.translate.translateZ' % each, '%s.input1.input1Z' % mdvNode)
            mc.connectAttr('%s.output.outputX' % mdvNode, '%s_%s_%s.translate.translateX' % (objSp[0], 'R', objSp[2]))
            mc.connectAttr('%s.output.outputY' % mdvNode, '%s_%s_%s.translate.translateY' % (objSp[0], 'R', objSp[2]))
            mc.connectAttr('%s.output.outputZ' % mdvNode, '%s_%s_%s.translate.translateZ' % (objSp[0], 'R', objSp[2]))
    if rotate == True:  
        for each in lists:
            objSp =each.split('_')
            mdvNode = mc.createNode ('multiplyDivide', n = ('%sRo_%s_Mdv' % (objSp[0], objSp[1])))
            #mc.setAttr('%s.input2.input2X' % mdvNode, valueA)
            mc.setAttr('%s.input2.input2Y' % mdvNode, valueA)
            mc.setAttr('%s.input2.input2Z' % mdvNode, valueA)
            mc.connectAttr('%s.rotateX' % each, '%s.input1X' % mdvNode)
            mc.connectAttr('%s.rotateY' % each, '%s.input1Y' % mdvNode)
            mc.connectAttr('%s.rotateZ' % each, '%s.input1Z' % mdvNode)
            mc.connectAttr('%s.outputX' % mdvNode, '%s_%s_%s.rotateX' % (objSp[0], 'R', objSp[2]))
            mc.connectAttr('%s.outputY' % mdvNode, '%s_%s_%s.rotateY' % (objSp[0], 'R', objSp[2]))
            mc.connectAttr('%s.outputZ' % mdvNode, '%s_%s_%s.rotateZ' % (objSp[0], 'R', objSp[2]))  

def duplicateMthJawJnt(*args):
    jaw1Obj = mc.ls('*:JawLwr1_Jnt')
    jaw2Obj = mc.ls('*:JawLwr2_Jnt')
    HeadJntObj = mc.ls('*:Head_Jnt')
    paList = mc.listRelatives(jaw1Obj[0], p = True)
    jawGrp = mc.createNode ('transform' , n = 'mthJawJnt_Grp')
    mc.delete(mc.parentConstraint(paList[0], jawGrp, mo = False))
    spJnt1Name = jaw1Obj[0].split(':')
    spJnt2Name = jaw2Obj[0].split(':')
    spHeadJntName = HeadJntObj[0].split(':')
    mthJaw1Obj = mc.createNode('joint', n = 'mth%s' % spJnt1Name[-1])
    mthJaw2Obj = mc.createNode('joint', n = 'mth%s' % spJnt2Name[-1])
    HeadJnt = mc.createNode('joint', n = 'mth%s' % spHeadJntName[-1])
    mc.setAttr('%s.radius' % mthJaw1Obj, 0.025)
    mc.setAttr('%s.radius' % mthJaw2Obj, 0.025)
    mc.setAttr('%s.radius' % HeadJnt, 0.025)
    mc.delete(mc.parentConstraint(jaw1Obj[0], mthJaw1Obj, mo =False))
    mc.delete(mc.parentConstraint(jaw2Obj[0], mthJaw2Obj, mo =False))
    mc.delete(mc.parentConstraint(HeadJntObj[0], HeadJnt, mo =False))
    mc.makeIdentity (mthJaw1Obj , apply = True)
    mc.makeIdentity (mthJaw2Obj , apply = True)
    mc.makeIdentity (HeadJnt , apply = True)
    mc.parent(mthJaw2Obj, mthJaw1Obj)
    mc.parent(mthJaw1Obj, jawGrp)

def reDuildCurve(lists = [], spans = '', *args):
    if lists:
        for each in lists:
            lists = each
            mc.rebuildCurve(lists, rt = 0, s = int(spans))
    else:
        lists = mc.ls(sl = True)
        for each in lists:
            # obj = each.split('|')
            lists = each
            print lists, 'lists'
            mc.rebuildCurve(lists, rt = 0, s = int(spans))

def OnOffVisibility(lists = [], v = 1):
    # Ctrl_Grp = mc.ls('*:Ctrl_Grp')
    # Skin_Grp = mc.ls('*:Skin_Grp')
    # Jnt_Grp = mc.ls('*:Jnt_Grp')
    # Ikh_Grp = mc.ls('*:Ikh_Grp')
    # lists = [Skin_Grp[0], Jnt_Grp[0], Ikh_Grp[0]]
    # mc.setAttr('%s.v' % Ctrl_Grp, v)
    for each in lists:
        mc.setAttr('%s.v' % each, v)