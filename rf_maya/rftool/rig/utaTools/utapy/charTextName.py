import maya.cmds as mc
reload(mc)
from utaTools.utapy import utaCore
reload(utaCore)
from rf_utils.context import context_info

# def charTextName(value = '10', size = '', *args): 
    # sels = mc.ls(sl = True)
    # bb = mc.exactWorldBoundingBox()

    # yOffset = float( abs( bb[1] - bb[4] ) * 1.2 )
    # xOffset = float( abs( bb[0] - bb[3] ) * 1.3 )
    # mc.select(cl = True)
    
    # # Generate >> Character Name Curve
    # asset = context_info.ContextPathInfo()         
    # char = asset.name   
    # nameCurveObj = utaCore.textName(char = char, size = size)
    # utaCore.tyHeadEnd(char = char, value = value)
    # mc.parent('%sCrv_Grp' % char, 'Rig_Grp' )
    # utaCore.parentScaleConstraintLoop(  obj = 'Head_Jnt', lists =  [ '%sCrv_Grp' % char],par = True,sca = True, ori = False, poi = False)

    # # Generate >> Control Layer Visibility
    # utaCore.layerControlRun(ns = '', obj = '%s_Crv' % char,shape = False)
    # utaCore.connectLayerControlVis( obj = '%s_Crv' % char, shape = False)

# def charTextName(char = '', value = '10', size = '', objPar = 'HeadEnd_Jnt',*args):
#     rigGrp = 'Rig_Grp'
#     # Generate >> Character Name Curve
#     asset = context_info.ContextPathInfo()         
#     char = asset.name  
#     nameCurveObj = utaCore.textName(char = char, size = size)
#     utaCore.tyHeadEnd(char = char, value = value, objPar = objPar)
#     if mc.objExists(rigGrp):
#         mc.parent('%sCrv_Grp' % char, rigGrp )
#     if objPar:
#         mc.parentConstraint( objPar, nameCurveObj, mo = True)
#         mc.scaleConstraint( objPar, nameCurveObj, mo = True)
#     # Generate >> Control Layer Visibility
#     utaCore.layerControlRun(ns = '', obj = char,shape = False)
#     utaCore.connectLayerControlVis( obj = char, shape = False)

    # ## SetAttr AnkleIk  -----------------------------------------
    # listObj = [ 'AnkleIk_L_Ctrl', 
    #             'AnkleIk_R_Ctrl']
    # listAttrs = 'toeBreak' 
    # i = 0 
    # if listAttrs:
    #     for sel in listObj:
    #         mc.setAttr ("%sShape.%s"% (sel, listAttrs), -100)
    #         print "Generate >> SetAttrs For Ankle%s" % sel
    #         i += 1 

def charTextName(char = '', value = '10', size = '', objPar = 'HeadEnd_Jnt',*args):
    if char == '':
        asset = context_info.ContextPathInfo()         
        char = asset.name

    fontText = 'MS Shell Dlg 2, 27.8pt'
    rigGrp = 'Rig_Grp'
    ctrlGrp = 'Ctrl_Grp'

    bb = mc.exactWorldBoundingBox(ctrlGrp, ii=True)
    txtCrv = mc.textCurves(n = '{}_'.format(char) , f = fontText , t = char)[0]
    listChi = mc.listRelatives(txtCrv, type = 'transform', c = True, ad = True, s = False)
    crvGrp = mc.group(n='{}Crv_Grp'.format(char),em=True)
    mc.select(cl = True)
    listGrp =[]
    listCrvObj = []
    for each in listChi:
        if '_1' in each :
            listConObj = mc.listConnections(each, s = True)
            listParObj = mc.listConnections(each,d = False, c = True ,p  = True)
            listSplitObj = listParObj[-1].split('.')
            objConnect = listSplitObj[-1]
            listNum = int(objConnect[-2])
            positionCon = 'position[%s]' % listNum
            listGrp.append(each)
        if 'curve' in each:
            listCrvObj.append(each)        

    mc.parent(listCrvObj, w = True)
    mc.select(crvGrp, add = True)
    mc.parent(listCrvObj, crvGrp)   
    for each in listCrvObj:       
        if 'curve' in each:
            ## Freeztransform
            mc.makeIdentity (each , apply = True , jointOrient = False, normal = 1, translate =True, rotate = True, scale = True) 
            ## Reset Transform
            mc.move("%s.scalePivot" % each,"%s.rotatePivot" % each, absolute=True) 
    crvUse = utaCore.combineCurves(char = char, curves = listCrvObj)
    mc.parent(crvUse, crvGrp)
    mc.xform(crvGrp, cp=1)
    mc.delete('{}_Shape'.format(char))
        
    txtBb = mc.exactWorldBoundingBox(crvUse, ii=True)
    mc.xform( crvGrp , cp = True )

    objWidth = abs(bb[0] - bb[3])
    txtWidth = abs(txtBb[0] - txtBb[3])

    tmpLoc = mc.spaceLocator()
    sclVal = (objWidth/txtWidth)/6
    mc.scale( sclVal , sclVal , sclVal , tmpLoc , r = True )

    posUp = mc.xform('HeadEnd_Jnt' , t=True , q=True ,ws=True)[1]

    upAx = mc.upAxis(q=True, ax=True)
    if upAx == "y":
        mc.move( 0 , posUp*1.1 , 0 , tmpLoc , r = True )

    elif upAx == "z":
        mc.move( 0 , 0 , posUp*1.1 , tmpLoc , r = True )
    
    mc.delete( mc.parentConstraint( tmpLoc , crvGrp ))  
    mc.delete( mc.scaleConstraint( tmpLoc , crvGrp ))
    mc.delete(tmpLoc)
    if mc.objExists(rigGrp):
        mc.parent('%sCrv_Grp' % char, rigGrp )
    if objPar:
        mc.parentConstraint( objPar, crvGrp, mo = True)
        mc.scaleConstraint( objPar, crvGrp, mo = True)
            
    utaCore.layerControlRun(ns = '', obj = char,shape = False)
    utaCore.connectLayerControlVis( obj = char, shape = False)
