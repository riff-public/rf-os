import pymel.core as pm
import maya.cmds as mc
from utaTools.utapy import utaCore
reload(utaCore)
from lpRig import rigTools as lrr
reload(lrr)

def motobikePathUi(*args):

	# check if window exists
	if pm.window ( 'motobikePathUi' , exists = True ) :
	    pm.deleteUI ( 'motobikePathUi' ) ;
	title = "GenerateJntAxis v1.0 (02-02-2022)" ;
	# create window 
	window = pm.window ( 'motobikePathUi', 
	                        title = title , 
	                        width = 100,
	                        mnb = True , 
	                        mxb = True , 
	                        sizeable = True , 
	                        rtf = True ) ;
	windowRowColumnLayout = pm.window ( 'motobikePathUi', 
	                        e = True , 
	                        h = 100,
	                        width = 200);

	# motionTab A-------------------------------------------------------------------------------------------------------------------------------    
	mainWindow = pm.tabLayout ( innerMarginWidth = 1 , innerMarginHeight = 1 , p = windowRowColumnLayout ) ;
	MotionTab_A = pm.rowColumnLayout ( "Generate motionPath for motobike", nc = 1 , p = mainWindow ) ;

	## Tab ----------------------------------------------------------------
	rowLine_TabA = pm.rowColumnLayout ( nc = 1 , p = MotionTab_A ) ;
	mc.tabLayout(p=rowLine_TabA, w= 200)

	# Control Tab ------------------------------------------------------
	pm.rowColumnLayout (p = rowLine_TabA ,h=8) ;

	## Create
	rowLine0 = pm.rowColumnLayout ( nc = 7 , p = rowLine_TabA  , cw = [(1, 10), (2, 85),(3, 10 ), (4, 25), (5,10) ]);

	pm.rowColumnLayout (p = rowLine0 ,h=8) ;
	pm.textField( "txtFieldName", text = "", p = rowLine0 , editable = True )
	pm.rowColumnLayout (p = rowLine0 ,h=8) ;
	pm.textField( "txtFieldSide", text = "L", p = rowLine0 , editable = True )
	pm.rowColumnLayout (p = rowLine0 ,h=8) ;
	# pm.textField( "txtFieldLastName", text = "_TmpJnt", p = rowLine0 , editable = True )
	# pm.rowColumnLayout (p = rowLine0 ,h=8) ;

	# rowLine0 = pm.rowColumnLayout ( nc = 3 , p = rowLine_TabA , h = 30 , cw = [(1, 10), (2, 180), (3, 10) ]) ;

	# pm.rowColumnLayout (p = rowLine0 ,h=8) ;
	# mc.floatFieldGrp( 'JointSize' , numberOfFields=1, label='Control Count   ', value1=0.3  ,p = rowLine0 ,columnWidth2=[50,50] )
	# pm.rowColumnLayout (p = rowLine0 ,h=8) ;


	## Create
	rowLine2 = pm.rowColumnLayout ( nc = 3 , p = rowLine_TabA , h = 30 , cw = [(1, 10), (2, 180), (3, 10) ]) ;

	pm.rowColumnLayout (p = rowLine2 ,h=8) ;
	pm.button ('cutFaceButton2', label = "Create Control" ,h=25, p = rowLine2 , c = createLocatorAndJntFileRun, bgc = (0.411765, 0.411765, 0.411765)) ;
	pm.rowColumnLayout (p = rowLine2 ,h=8) ;



	rowLine1 = pm.rowColumnLayout ( nc = 7 , p = rowLine_TabA  , cw = [(1, 10), (2, 85),(3, 10 ), (4, 25), (5,10) ,(6, 50) ,(7, 10)]);

	pm.rowColumnLayout (p = rowLine1 ,h=8) ;
	pm.textField( "txtFieldName", text = "", p = rowLine1 , editable = True )
	pm.rowColumnLayout (p = rowLine1 ,h=8) ;
	pm.textField( "txtFieldSide", text = "L", p = rowLine1 , editable = True )
	pm.rowColumnLayout (p = rowLine1 ,h=8) ;
	pm.textField( "txtFieldLastName", text = "_TmpJnt", p = rowLine1 , editable = True )
	pm.rowColumnLayout (p = rowLine1 ,h=8) ;


	## Part
	pm.rowColumnLayout (p = rowLine1 ,h=8) ;
	pm.text( label='Name' , p = rowLine1, al = "center")  
	pm.rowColumnLayout (p = rowLine1 ,h=8) ;
	pm.text( label='Side' , p = rowLine1, al = "center")  
	pm.rowColumnLayout (p = rowLine1 ,h=8) ;
	pm.text( label='LastName' , p = rowLine1, al = "center")  
	pm.rowColumnLayout (p = rowLine1 ,h=8) ;

	## Create
	rowLine2 = pm.rowColumnLayout ( nc = 3 , p = rowLine_TabA , h = 50 , cw = [(1, 10), (2, 180), (3, 10) ]) ;
	pm.rowColumnLayout (p = rowLine2 ,h=8) ;
	# pm.rowColumnLayout (p = rowLine1 ,h=8) ;
	pm.button ('buttonParentAndAimAxis', label = "Parent And Aim Axis" ,h=45, p = rowLine2 , c = run, bgc = (0.411765, 0.411765, 0.411765)) ;
	pm.rowColumnLayout (p = rowLine2 ,h=8) ;

	## Tab ----------------------------------------------------------------
	rowLine_TabA = pm.rowColumnLayout ( nc = 1 , p = MotionTab_A ) ;
	mc.tabLayout(p=rowLine_TabA, w= 200)


	pm.setParent("..")
	pm.showWindow(window)
	
def getJnt(*args):
    
	jnt = mc.ls(sl=True)[0]
	pm.textField( "txtFieldJnt", e=True , text = '%s'%jnt )
	
def createLocatorAndJntFileRun(*args):
    size = mc.floatFieldGrp('JointSize' , q=True , v=True)
    select = lrr.locatorOnMidPos()
    mc.select(select)
    jntFrLoc = utaCore.jointFromLocator()

    mc.setAttr(jntFrLoc+'.radius' , size[0])
	
def run(*args):

	txtFieldNameObj = pm.textField("txtFieldName", q=True, text=True) 
	txtFieldSideObj = pm.textField("txtFieldSide", q=True, text=True)
	txtFieldLastNameObj = pm.textField( "txtFieldLastName", q=True, text=True)
	utaCore.parentObj(jntName = txtFieldNameObj, Side = txtFieldSideObj, lastName = txtFieldLastNameObj)
