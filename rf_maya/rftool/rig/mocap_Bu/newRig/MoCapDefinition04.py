import maya.cmds as mc
import maya.mel as mel
import maya.app.hik.retargeter as r
import sys

sys.path.append(r'N:/Staff/Hong/Scripts/Mocap/DarkHorse/')

#import HIKMocapList as mth #mocapData joint List

#import mxmHIKMocapList as mth #mixamo mocapData joint List

def setTPose():
    currentF=mc.currentTime(0)
    back1F = currentF-1
    partList = mth.partJntList
    
    def saveFirstFrame(jnt):
        mc.currentTime(0)
        mc.setKeyframe(jnt,at='rotateX')
        mc.setKeyframe(jnt,at='rotateY')
        mc.setKeyframe(jnt,at='rotateZ')
    
    def setRotZero(jnt):
        print "%s.rotate"%(jnt)
        mc.currentTime(back1F)
        mc.setAttr("%s.rotate"%(jnt),0,0,0,type= 'double3')
        mc.setKeyframe(jnt, at='rotateX')
        mc.setKeyframe(jnt, at='rotateY')
        mc.setKeyframe(jnt, at='rotateZ')
    
    for i in partList[:]:
        if i!= '':
            NS = getNS()
            if NS != '':
                jnt = '%s:%s'%(NS,i)
            else:
                jnt = i
            if 'Hips' not in jnt:
                saveFirstFrame(jnt)        
                setRotZero(jnt)
            else:
                pass
    mc.currentTime(back1F)

def getNS():
    sel = mc.ls(sl=True)
    if ':' in sel[0]:
        NS = sel[0].split(':')[0]
        print NS
        return NS
    else:
        return ''

######### create HIK Definition for Character #########################################
def crateChar(NS='',Char='Character1'): 
    partNumList = mth.partNumList
    partNameList = mth.partNameList
    partJntList = mth.partJntList
    charName = Char
    mel.eval(" hikCreateDefinition();string $Char =hikGetCurrentCharacter();string $newName =`rename $Char "+"\""+charName+"\"`;")
    for i in range(len(partNumList)):
        #print i
        if partJntList[i]!='':
            mc.select(charName)
            if NS != '':
                partJnt = NS +':'+ partJntList[i]
            else:
                partJnt = partJntList[i]
            mel.eval(str("setCharacterObject("+"\""+partJnt+"\",$newName,"+"\""+str(partNumList[i])+"\",0);"))


    mel.eval("HIKCharacterControlsTool;")
    


################### run script ######################################################################
def createDefinition():
    NS=getNS()
    setTPose()
    crateChar(NS)
#createDefinition()
#createCustomRigMap()
#delUnknownHIKNode(Character)
#moveAnimLayer(ctlList,NS)
