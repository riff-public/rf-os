import maya.cmds as mc
import maya.mel as mel
import maya.app.hik.retargeter as r

def getNS():
    ''' get NameSpace for another usage'''
    sel = mc.ls(sl=True)
    if ':' in sel[0]:
        NS = sel[0].split(':')[0]
        return NS
    else:
        return None
######### create HIK Definition for Character #########################################
def crateChar(Char='Character1',NS=''): 
    '''Create Character Definition by createNode and Connect Jnt to HIK Character Node'''
    #NS = getNS()
    HIKChar = mc.createNode('HIKCharacterNode',n=Char)
    HIKProp = mc.createNode('HIKProperty2State',n='HIKproperties1')
    mc.connectAttr('%s.message'%(HIKProp),'%s.propertyState'%(HIKChar))
    
    ##### connect Joint to Character Definition ######
    partList = ['Hips','Spine','Spine1','Spine2','Spine3','Neck','Head','RightShoulder','RightArm','RightForeArm','RightHand','LeftShoulder','LeftArm','LeftForeArm','LeftHand','RightUpLeg','RightLeg','RightFoot','LeftUpLeg','LeftLeg','LeftFoot']
    JntList = ['Root_Jnt','Spine1Pos_Jnt','Spine2Pos_Jnt','Spine3Pos_Jnt','Spine4Pos_Jnt','Neck_Jnt','Head_Jnt','Clav_R_Jnt','UpArm_R_Jnt','Forearm_R_Jnt','Wrist_R_Jnt','Clav_L_Jnt','UpArm_L_Jnt','Forearm_L_Jnt','Wrist_L_Jnt','UpLeg_R_Jnt','LowLeg_R_Jnt','Ankle_R_Jnt','UpLeg_L_Jnt','LowLeg_L_Jnt','Ankle_L_Jnt']
    
    def connectDefinition(Jnt='',part=''):
        if (Jnt!='') and (part != ''):
            if not mc.objExists('%s.Character'%(Jnt)):
                mc.addAttr(Jnt,ln='Character',k=0)
                mc.connectAttr('%s.Character'%(Jnt),'%s.%s'%(HIKChar,part))
            else:
                mc.connectAttr('%s.Character'%(Jnt),'%s.%s'%(HIKChar,part))
        if part =='':
            return
    
    if len(partList) == len(JntList):
        for i in range(len(partList)):
            if NS != '':
                jnt = NS+':'+JntList[i]
            else:
                jnt = JntList[i]
            print jnt
            print partList[i]
            connectDefinition(jnt,partList[i])
    
    mel.eval("hikSetCurrentCharacter"+"\""+HIKChar+"\""+" ;")
    mel.eval("hikSetCurrentSourceFromCharacter("+"\""+HIKChar+"\""+");")
    mel.eval("hikUpdateSourceList();")

    mel.eval("hikDefinitionUpdateCharacterLists;")
    mel.eval("hikDefinitionUpdateBones")
    mel.eval("HIKCharacterControlsTool;")


###### connect Controller to CustomRig Map ###################################################################################
def createCustomRigMap(NS=''):
    ''' This Part is creating customRig map node by using MAYA HIK MEL MODULE'''
    mel.eval("hikCreateCustomRig(hikGetCurrentCharacter());")
    ''' Define controller on each part with NameSpace or Not add NameSpace '''
    if NS == '':
        root = 'Root_Ctrl'
        
        upLegL = 'UpLegFk_L_Ctrl'
        lowLegL='LowLegFk_L_Ctrl'
        AnkleL='AnkleFk_L_Ctrl'
        
        upLegR='UpLegFk_R_Ctrl'
        lowLegR='LowLegFk_R_Ctrl'
        AnkleR='AnkleFk_R_Ctrl'
        
        spine1='Spine1_Ctrl'
        spine2='Spine2_Ctrl'
        spine3='Spine3_Ctrl'
        spine4='Spine4_Ctrl'
        
        ClavL='Clav_L_Ctrl'
        UpArmL='UpArmFk_L_Ctrl'
        ForearmL='ForearmFk_L_Ctrl'
        WristL='WristFk_L_Ctrl'
        
        ClavR='Clav_R_Ctrl'
        UpArmR='UpArmFk_R_Ctrl'
        ForearmR='ForearmFk_R_Ctrl'
        WristR='WristFk_R_Ctrl'
        
        head= 'Head_Ctrl'
        neck = 'Neck_Ctrl'
    else:
        root = NS+':Root_Ctrl'
        upLegL = NS+':UpLegFk_L_Ctrl'
        lowLegL=NS+':LowLegFk_L_Ctrl'
        AnkleL=NS+':AnkleFk_L_Ctrl'
        
        upLegR=NS+':UpLegFk_R_Ctrl'
        lowLegR=NS+':LowLegFk_R_Ctrl'
        AnkleR=NS+':AnkleFk_R_Ctrl'
        
        spine1=NS+':Spine1_Ctrl'
        spine2=NS+':Spine2_Ctrl'
        spine3=NS+':Spine3_Ctrl'
        spine4=NS+':Spine4_Ctrl'
        
        ClavL=NS+':Clav_L_Ctrl'
        UpArmL=NS+':UpArmFk_L_Ctrl'
        ForearmL=NS+':ForearmFk_L_Ctrl'
        WristL=NS+':WristFk_L_Ctrl'
        
        ClavR= NS+':Clav_R_Ctrl'
        UpArmR= NS+':UpArmFk_R_Ctrl'
        ForearmR=NS+':ForearmFk_R_Ctrl'
        WristR=NS+':WristFk_R_Ctrl'
        
        head= NS+':Head_Ctrl'
        neck = NS+':Neck_Ctrl'    
    

    ''' Assign Each Controller to Custom Rig Map by using MAYA HIK MEL MODULE '''
    ######## root ############
    print root
    mc.select(root)
    mel.eval("hikCustomRigAssignEffector 1;")
    mc.select(d=1)
    ####### left Leg ###############
    mc.select(upLegL)
    mel.eval("hikCustomRigAssignEffector 2;")
    mc.select(d=1)
    
    mc.select(lowLegL)
    mel.eval("hikCustomRigAssignEffector 3;")
    mc.select(d=1)
    
    mc.select(AnkleL)
    mel.eval("hikCustomRigAssignEffector 4;")
    mc.select(d=1)
    ####### right Leg ###############
    mc.select(upLegR)
    mel.eval("hikCustomRigAssignEffector 5;")
    mc.select(d=1)
    
    mc.select(lowLegR)
    mel.eval("hikCustomRigAssignEffector 6;")
    mc.select(d=1)
    
    mc.select(AnkleR)
    mel.eval("hikCustomRigAssignEffector 7;")
    mc.select(d=1)
    
    ####### Spine###########
    mc.select(spine1)
    mel.eval("hikCustomRigAssignEffector 8;")
    mc.select(d=1)
    
    mc.select(spine2)
    mel.eval("hikCustomRigAssignEffector 23;")
    mc.select(d=1)
    
    mc.select(spine3)
    mel.eval("hikCustomRigAssignEffector 24;")
    mc.select(d=1)
    
    mc.select(spine4)
    mel.eval("hikCustomRigAssignEffector 25;")
    mc.select(d=1)
    
    
    ##### left upper ######
    mc.select(ClavL)
    mel.eval("hikCustomRigAssignEffector 18;")
    mc.select(d=1)
    
    mc.select(UpArmL)
    mel.eval("hikCustomRigAssignEffector 9;")
    mc.select(d=1)
    
    mc.select(ForearmL)
    mel.eval("hikCustomRigAssignEffector 10;")
    mc.select(d=1)
    
    mc.select(WristL)
    mel.eval("hikCustomRigAssignEffector 11;")
    mc.select(d=1)
    
    
    ##### right upper ######
    mc.select(ClavR)
    mel.eval("hikCustomRigAssignEffector 19;")
    mc.select(d=1)
    
    mc.select(UpArmR)
    mel.eval("hikCustomRigAssignEffector 12;")
    mc.select(d=1)
    
    mc.select(ForearmR)
    mel.eval("hikCustomRigAssignEffector 13;")
    mc.select(d=1)
    
    mc.select(WristR)
    mel.eval("hikCustomRigAssignEffector 14;")
    mc.select(d=1)
    
    #### head & neck #####
    mc.select(head)
    mel.eval("hikCustomRigAssignEffector 15;")
    mc.select(d=1)
    
    mc.select(neck)
    mel.eval("hikCustomRigAssignEffector 20;")
    mc.select(d=1)
    

def delUnknownHIKNode(Character='Character1'):
    '''' This script will delete Character Node and delete etc Node that made during retargeting '''
    delCom = "deleteCharacter("+'"'+Character+'"'+");"
    mel.eval(delCom)
    mc.delete(mc.ls(type='composeMatrix'))
    mc.delete(mc.ls(type='eulerToQuat'))
    mc.delete(mc.ls(type='quatInvert'))
    

#delUnknownHIKNode('Character1')

###############         Anim Layer          ##############################################################################################################
ctlList = ['WristFk_R_Ctrl','ForearmFk_R_Ctrl','UpArmFk_R_Ctrl','UpArmFk_L_Ctrl',
            'ForearmFk_L_Ctrl','WristFk_L_Ctrl','Neck_Ctrl','Head_Ctrl','Spine2_Ctrl',
            'Spine3_Ctrl','Spine4_Ctrl','Root_Ctrl','Spine1_Ctrl','UpLegFk_L_Ctrl',
            'UpLegFk_R_Ctrl','LowLegFk_L_Ctrl','LowLegFk_R_Ctrl','LegRbn_R_Ctrl','AnkleFk_R_Ctrl',
            'AnkleFk_L_Ctrl','BallFk_R_Ctrl']


def createControlList(ctlList=[],NS=''):
    '''createController List for another usage  '''
    if NS!='':          
        newCtlList = [NS+':'+i for i in ctlList]
    else:
        newCtlList = ctlList
    return newCtlList

def moveAnimLayer(ctlList=[],NS=''):
    ''' create Mocap and Animation AnimLayer by using controllerList and namespace '''
    newCtlList = createControlList(ctlList,NS)
    anim = mc.animLayer('%s:Anim'%NS)
    mc.animLayer(anim,newCtlList,aso=1,e=1)

    mc.select(newCtlList)
    mel.eval("string $lSelection[]=`ls -selection`;string $layers[]={" +"\"" +"BaseAnimation" +"\"" +"}; layerEditorExtractObjectsAnimLayer($lSelection, $layers );")
    mc.rename('BaseAnimation_extract','%s:Mocap'%NS)




################### run script ######################################################################
#def runAll()
#NS=getNS()
#crateChar()
#createCustomRigMap()
#delUnknownHIKNode(Character)
#moveAnimLayer(ctlList,NS)