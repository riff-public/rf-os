import maya.cmds as mc
import maya.mel as mel
import maya.app.hik.retargeter as r
#HIKChar = 'CustomRig_Definition'
#HIKProp = 'HIKproperties1'


HIKChar = mc.createNode('HIKCharacterNode',n='Character1')
HIKProp = mc.createNode('HIKProperty2State',n='HIKproperties1')
mc.connectAttr('%s.message'%(HIKProp),'%s.propertyState'%(HIKChar))

##### connect Joint to Character Definition ######
partList = ['Hips','Spine','Spine1','Spine2','Spine3','Neck','Head','RightShoulder','RightArm','RightForeArm','RightHand','LeftShoulder','LeftArm','LeftForeArm','LeftHand','RightUpLeg','RightLeg','RightFoot','LeftUpLeg','LeftLeg','LeftFoot']
JntList = ['Root_Jnt','Spine1Pos_Jnt','Spine2Pos_Jnt','Spine3Pos_Jnt','Spine4Pos_Jnt','Neck_Jnt','Head_Jnt','Clav_R_Jnt','UpArm_R_Jnt','Forearm_R_Jnt','Wrist_R_Jnt','Clav_L_Jnt','UpArm_L_Jnt','Forearm_L_Jnt','Wrist_L_Jnt','UpLeg_R_Jnt','LowLeg_R_Jnt','Ankle_R_Jnt','UpLeg_L_Jnt','LowLeg_L_Jnt','Ankle_L_Jnt']

def connectDefinition(Jnt='',part=''):
    if (Jnt!='') and (part != ''):
        if not mc.objExists('%s.Character'%(Jnt)):
            mc.addAttr(Jnt,ln='Character',k=0)
            mc.connectAttr('%s.Character'%(Jnt),'%s.%s'%(HIKChar,part))
        else:
            mc.connectAttr('%s.Character'%(Jnt),'%s.%s'%(HIKChar,part))
    if part =='':
        return
if len(partList) == len(JntList):
    for i in range(len(partList)):
        connectDefinition(JntList[i],partList[i])




###### connect Controller to CustomRig Map ######

mel.eval("hikCreateCustomRig(hikGetCurrentCharacter());")

mc.select('Root_Ctrl')
mel.eval("hikCustomRigAssignEffector 1;")
mc.select(d=1)

mc.select('UpLegFk_L_Ctrl')
mel.eval("hikCustomRigAssignEffector 2;")
mc.select(d=1)

mc.select('LowLegFk_L_Ctrl')
mel.eval("hikCustomRigAssignEffector 3;")
mc.select(d=1)

mc.select('AnkleFk_L_Ctrl')
mel.eval("hikCustomRigAssignEffector 4;")
mc.select(d=1)

mc.select('UpLegFk_R_Ctrl')
mel.eval("hikCustomRigAssignEffector 5;")
mc.select(d=1)

mc.select('LowLegFk_R_Ctrl')
mel.eval("hikCustomRigAssignEffector 6;")
mc.select(d=1)

mc.select('AnkleFk_R_Ctrl')
mel.eval("hikCustomRigAssignEffector 7;")
mc.select(d=1)

mc.select('Spine1_Ctrl')
mel.eval("hikCustomRigAssignEffector 8;")
mc.select(d=1)

mc.select('Spine2_Ctrl')
mel.eval("hikCustomRigAssignEffector 23;")
mc.select(d=1)

mc.select('Spine3_Ctrl')
mel.eval("hikCustomRigAssignEffector 24;")
mc.select(d=1)

mc.select('Spine4_Ctrl')
mel.eval("hikCustomRigAssignEffector 25;")
mc.select(d=1)

mc.select('Clav_L_Ctrl')
mel.eval("hikCustomRigAssignEffector 18;")
mc.select(d=1)

mc.select('Clav_R_Ctrl')
mel.eval("hikCustomRigAssignEffector 19;")
mc.select(d=1)

mc.select('UpArmFk_L_Ctrl')
mel.eval("hikCustomRigAssignEffector 9;")
mc.select(d=1)

mc.select('ForearmFk_L_Ctrl')
mel.eval("hikCustomRigAssignEffector 10;")
mc.select(d=1)

mc.select('WristFk_L_Ctrl')
mel.eval("hikCustomRigAssignEffector 11;")
mc.select(d=1)

mc.select('UpArmFk_R_Ctrl')
mel.eval("hikCustomRigAssignEffector 12;")
mc.select(d=1)

mc.select('ForearmFk_R_Ctrl')
mel.eval("hikCustomRigAssignEffector 13;")
mc.select(d=1)

mc.select('WristFk_R_Ctrl')
mel.eval("hikCustomRigAssignEffector 14;")
mc.select(d=1)

mc.select('Head_Ctrl')
mel.eval("hikCustomRigAssignEffector 15;")
mc.select(d=1)

mc.select('Neck_Ctrl')
mel.eval("hikCustomRigAssignEffector 20;")
mc.select(d=1)



