import maya.cmds as mc
import maya.mel as mel
import maya.app.hik.retargeter as r



def getNS():
    sel = mc.ls(sl=True)
    if ':' in sel[0]:
        NS = sel[0].split(':')[0]
        print NS
        return NS
    else:
        return None
######### create HIK Definition for Character ###############################################################################
def crateChar(Char='Character1'): 
    HIKChar = mc.createNode('HIKCharacterNode',n=Char)
    HIKProp = mc.createNode('HIKProperty2State',n='HIKproperties1')
    mc.connectAttr('%s.message'%(HIKProp),'%s.propertyState'%(HIKChar))
    
    ##### connect Joint to Character Definition ######
    partList = ['Hips','Spine','Spine1','Spine2','Spine3','Neck','Head','RightShoulder','RightArm','RightForeArm','RightHand','LeftShoulder','LeftArm','LeftForeArm','LeftHand','RightUpLeg','RightLeg','RightFoot','LeftUpLeg','LeftLeg','LeftFoot']
    
    if NS != None:
        JntList = [NS+':'+x for x in partList]
    else:
        JntList = list(partList)  
        
    def connectDefinition(Jnt='',part=''):
        if (Jnt!='') and (part != ''):
            if not mc.objExists('%s.Character'%(Jnt)):
                mc.addAttr(Jnt,ln='Character',k=0)
                mc.connectAttr('%s.Character'%(Jnt),'%s.%s'%(HIKChar,part))
            else:
                mc.connectAttr('%s.Character'%(Jnt),'%s.%s'%(HIKChar,part))
        if part =='':
            return
    
    if len(partList) == len(JntList):
        for i in range(len(partList)):
            print partList[i]
            connectDefinition(JntList[i],partList[i])
    
    mel.eval("HIKCharacterControlsTool;")

################### run script ######################################################################
#NS=getNS()
#crateChar()
#createCustomRigMap()
#delUnknownHIKNode(Character)
#moveAnimLayer(ctlList,NS)