import maya.cmds as mc
import maya.mel as mel
import maya.app.hik.retargeter as rte
import sys

########## HIK DEFINITION NEWRIG V 17
sys.path.append(r'N:/Staff/Hong/Scripts/Mocap/DarkHorse')
import HIKCharList as HIKCharList
reload(HIKCharList)


def getNS():
    ''' get NameSpace for another usage'''
    sel = mc.ls(sl=True)
    if ':' in sel[0]:
        NS = sel[0].split(':')[0]
        return NS
    else:
        return None
######### create HIK Definition for Character #########################################
def crateChar(NS='',Char='Character1',): 
    partNumList = HIKCharList.partNumList
    partNameList = HIKCharList.partNameList
    charJntList = HIKCharList.charJntList
    charName = Char
    mel.eval("hikCreateDefinition();string $Char =hikGetCurrentCharacter();string $newName =`rename $Char "+"\""+charName+"\"`;")
    for i in range(len(partNumList)):
        if charJntList[i]!= '':
            mc.select(charName)
            if NS != '':
                charJnt = NS+':'+charJntList[i]
            else:
                charJnt = charJntList[i]
            mel.eval(str("setCharacterObject("+"\""+charJnt+"\",$newName,"+"\""+str(partNumList[i])+"\",0);"))

    #mel.eval("hikCreateCustomRig(hikGetCurrentCharacter());")
    mel.eval("hikSetCurrentCharacter $newName ;")
    mel.eval("hikSetCurrentSourceFromCharacter($newName);")
    mel.eval("hikUpdateSourceList();")

    mel.eval("hikDefinitionUpdateCharacterLists;")
    mel.eval("hikDefinitionUpdateBones")
    mel.eval("HIKCharacterControlsTool;")


###### connect Controller to CustomRig Map ###################################################################################
def createCustomRigMap(NS=''):
    ''' This Part is creating customRig map node by using MAYA HIK MEL MODULE'''
    mel.eval("hikCreateCustomRig(hikGetCurrentCharacter());")
    partNumList = HIKCharList.partNumList
    partCtlList = HIKCharList.partCtlList
    partList = HIKCharList.partNameList

    ''' Assign Each Controller to Custom Rig Map by using MAYA HIK MEL MODULE '''
    ######## root ############
    for i in range(len(partNumList)):
        if partCtlList[i] != '':
            if NS!='':
                #print NS
                print NS+':'+partCtlList[i]
                partCtl = NS+':'+partCtlList[i]
                mc.select(partCtl)
                
            else:
                partCtl = partCtlList[i]
                mc.select(partCtl)
            #mel.eval("hikCustomRigAssignEffector " +str(i)+";")
            #mel.eval("hikCustomRigAssignEffector " +str(i)+";")

            charName = mel.eval(" hikGetCurrentCharacter()")
            #retargeter = mel.eval("RetargeterCreate("+'"'+ charName+'"'+");")
            #retargeter = mc.createNode('CustomRigRetargeterNode')
            retargeter = mel.eval("RetargeterGetName("+'"'+ charName+'"'+");")

            if 'Hips' in partList[i]:
                retargetCom = "RetargeterAddMapping("+'"'+retargeter+'"'+","+'"'+partList[i]+'",'+'"R"'+',"'+partCtl +'",'+str(i)+' );'
                mel.eval(retargetCom)
                retargetCom = "RetargeterAddMapping("+'"'+retargeter+'"'+","+'"'+partList[i]+'",'+'"T"'+',"'+partCtl +'",'+str(i)+' );'
                mel.eval(retargetCom)
            else :
                retargetCom = "RetargeterAddMapping("+'"'+retargeter+'"'+","+'"'+partList[i]+'",'+'"R"'+',"'+partCtl +'",'+str(i)+' );'
                mel.eval(retargetCom)
            print retargetCom
            
            mel.eval("hikUpdateCustomRigAssignedMappings " +'"'+charName+'"'+";")
            mel.eval("hikCustomRigToolWidget -e -sl "+str(i)+";")
            mel.eval("hikUpdateCustomRigUI;")
def delUnknownHIKNode(Character='Character1'):
    '''' This script will delete Character Node and delete etc Node that made during retargeting '''
    delCom = "deleteCharacter("+'"'+Character+'"'+");"
    mel.eval(delCom)
    mc.delete(mc.ls(type='composeMatrix'))
    mc.delete(mc.ls(type='eulerToQuat'))
    mc.delete(mc.ls(type='quatInvert'))
    

#delUnknownHIKNode('Character1')

###############         Anim Layer          ##############################################################################################################
ctlList = ['WristFk_R_Ctrl','ForearmFk_R_Ctrl','UpArmFk_R_Ctrl','UpArmFk_L_Ctrl',
            'ForearmFk_L_Ctrl','WristFk_L_Ctrl','Neck_Ctrl','Head_Ctrl','Spine2_Ctrl',
            'Spine3_Ctrl','Spine4_Ctrl','Root_Ctrl','Spine1_Ctrl','UpLegFk_L_Ctrl',
            'UpLegFk_R_Ctrl','LowLegFk_L_Ctrl','LowLegFk_R_Ctrl','LegRbn_R_Ctrl','AnkleFk_R_Ctrl',
            'AnkleFk_L_Ctrl','BallFk_R_Ctrl']


def createControlList(ctlList=[],NS=''):
    '''createController List for another usage  '''
    if NS!='':          
        newCtlList = [NS+':'+i for i in ctlList if i != '']
    else:
        newCtlList = ctlList
    return newCtlList

def moveAnimLayer(ctlList=[],NS=''):
    ''' create Mocap and Animation AnimLayer by using controllerList and namespace '''
    newCtlList = createControlList(ctlList,NS)
    anim = mc.animLayer('%s:Anim'%NS)
    mc.animLayer(anim,newCtlList,aso=1,e=1)

    mc.select(newCtlList)
    mel.eval("string $lSelection[]=`ls -selection`;string $layers[]={" +"\"" +"BaseAnimation" +"\"" +"}; layerEditorExtractObjectsAnimLayer($lSelection, $layers );")
    mc.rename('BaseAnimation_extract','%s:Mocap'%NS)




################### run script ######################################################################
#def runAll()
#NS=getNS()

#crateChar(NS)
#createCustomRigMap(NS)
#delUnknownHIKNode(Character)
#moveAnimLayer(HIKCharList.partCtlList,NS)