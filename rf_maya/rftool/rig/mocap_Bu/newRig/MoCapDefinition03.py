import maya.cmds as mc
import maya.mel as mel
import maya.app.hik.retargeter as r
import sys

sys.path.append(r'N:/Staff/Hong/Scripts/Mocap/DarkHorse/newRig')

import mixamoHIKPartList as mxmHIK
#from newRig import rsuHIKPartList as rsuHIK

def setTPose():
    currentF=mc.currentTime(0)
    back1F = currentF-1
    partList = mxmHIK.partList
    
    def saveFirstFrame(jnt):
        mc.currentTime(0)
        mc.setKeyframe(jnt,at='rotateX')
        mc.setKeyframe(jnt,at='rotateY')
        mc.setKeyframe(jnt,at='rotateZ')
    
    def setRotZero(jnt):
        print "%s.rotate"%(jnt)
        mc.currentTime(back1F)
        mc.setAttr("%s.rotate"%(jnt),0,0,0,type= 'double3')
        mc.setKeyframe(jnt, at='rotateX')
        mc.setKeyframe(jnt, at='rotateY')
        mc.setKeyframe(jnt, at='rotateZ')
    
    for i in partList[:]:
        NS = getNS()
        if NS != '':
            jnt = '%s:%s'%(NS,i)
        else:
            jnt = i
        saveFirstFrame(jnt)        
        setRotZero(jnt)    
            
    mc.currentTime(back1F)
def getNS():
    sel = mc.ls(sl=True)
    if ':' in sel[0]:
        NS = sel[0].split(':')[0]
        print NS
        return NS
    else:
        return ''
######### create HIK Definition for Character ###############################################################################
def getNS():
    ''' get NameSpace for another usage'''
    sel = mc.ls(sl=True)
    if ':' in sel[0]:
        NS = sel[0].split(':')[0]
        return NS
    else:
        return None
######### create HIK Definition for Character #########################################
def crateChar(Char='Character1',NS=''): 
    partNumList = mth.partNumList
    partNameList = mth.partNameList
    partJntList = mth.partJntList
    charName = Char
    mel.eval(" hikCreateDefinition();string $Char =hikGetCurrentCharacter();string $newName =`rename $Char "+"\""+charName+"\"`;")
    for i in partNumList:
        if partJntList!='':
            mc.select(charName)
            mel.eval(str("setCharacterObject("+"\""+partJntList[i]+"\",$newName,"+"\""+str(partNumList[i])+"\",0);"))


    mel.eval("hikCreateCustomRig($newName);")
    mel.eval("hikSetCurrentCharacter"+"\",$newName,\""+" ;")
    mel.eval("hikSetCurrentSourceFromCharacter("+"\",$newName,\""+");")
    mel.eval("hikUpdateSourceList();")

    mel.eval("hikDefinitionUpdateCharacterLists;")
    mel.eval("hikDefinitionUpdateBones")
    mel.eval("HIKCharacterControlsTool;")
    


################### run script ######################################################################
def createDefinition():
    setTPose()
    NS=getNS()
    crateChar(NS)
#createDefinition()
#createCustomRigMap()
#delUnknownHIKNode(Character)
#moveAnimLayer(ctlList,NS)