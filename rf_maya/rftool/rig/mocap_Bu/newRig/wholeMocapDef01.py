import maya.cmds as mc
import maya.mel as mel
from rf_utils import file_utils as fu
import sys

#sys.path.append(r'N:\Staff\Hong\Scripts\Mocap\DarkHorse')

import MoCapDefinition as mcd #other MocapData
reload(mcd)

#import mxmMoCapDefinition04 as mcd # mixamoMocapData
#reload(mcd)

def folderMocapDef(root='',destinationFolder=''):
	####### list mocap fbx file #########
	if root != '':
		fbxList = fu.list_file(root)
		print fbxList
		pass
		####### createNew file and assign HIK Definition and Save as MAYAAscii file ########
		for f in fbxList:
		    print f
		    if '.fbx' in f:
		        mc.file(new=1,f=1)
		        mc.file('%s/%s'%(root,f),i=1)
		        mc.select('*Hips*')
		        mcd.setTPose()
		        mc.select('*Hips*')
		        mcd.createDefinition()
		        mc.file(rename='%s/%s'%(destinationFolder,f.split('.fbx')[0]))
		        #print '%s/%s'%(root,f.split('.fbx')[0])
		        mc.file(save=1,type='mayaBinary')
	else:
		mc.confirmDialog(title = 'folderMocapDef Error',m='Please type directory of mocap data folder.',b='OK')


def multipleFolderMocapDef(fileList=[],destinationFolder=''):
	if fileList != []:
		
		for f in fileList:
		    print f
		    if '.fbx' in f:
		    	fileName = f.split('/')[-1]
		    	print fileName
		        mc.file(new=1,f=1)
		        mc.file('%s'%(f),i=1)
		        mc.select('*Hips*')
		        mcd.createDefinition()
		        mc.file(rename='%s/%s'%(destinationFolder,fileName.split('.fbx')[0]))
		        #print '%s/%s'%(DestinationFolder,fileName.split('.fbx')[0])
		        #print '%s/%s'%(root,f.split('.fbx')[0])
		        mc.file(save=1,type='mayaBinary')
	else:
		mc.confirmDialog(title = 'folderMocapDef Error',m='Please type directory of mocap data folder.',b='OK')		
