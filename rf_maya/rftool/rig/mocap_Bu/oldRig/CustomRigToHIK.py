import maya.cmds as mc


HIKChar = 'CustomRig_Definition'
HIKProp = 'HIKproperties1'
#HIKChar = mc.createNode('HIKCharacterNode',n='Character1')
#HIKProp = mc.createNode('HIKProperty2State',n='HIKproperties1')
#mc.connectAttr('%s.message'%(HIKProp),'%s.propertyState'%(HIKChar))

##### connect Joint to Character Definition ######
#HIKList = ['Hips','Spine','Spine1','Spine2','Spine3','Neck','Head','RightShoulder','RightArm','RightForeArm','RightHand','LeftShoulder','LeftArm','LeftForeArm','LeftHand','RightUpLeg','RightLeg','RightFoot','RightToeBase','LeftUpLeg','LeftLeg','LeftFoot','LeftToeBase']
'''
for i in HIKList:
    print i
    if not mc.objExists('%s.Character'%(i)):
        mc.addAttr(i,ln='Character',k=0)
        mc.connectAttr('%s.Character'%(i),'%s.%s'%(HIKChar,i))
    else:
        mc.connectAttr('%s.Character'%(i),'%s.%s'%(HIKChar,i))

'''
############### create and connect CustomRig Map Node to HIK characterNode ###########

HIKSolver= mc.createNode('HIKSolverNode')
HIKStateGlobal= mc.createNode('HIKState2GlobalSK')
CustomRigRetargeter = mc.createNode('CustomRigRetargeterNode')

mc.connectAttr('%s.OutputPropertySetState'%(HIKProp),'%s.InputPropertySetState'%(HIKSolver))
mc.connectAttr('%s.OutputCharacterDefinition'%(HIKChar),'%s.InputCharacterDefinition'%(HIKSolver))
mc.connectAttr('%s.OutputCharacterDefinition'%(HIKChar),'%s.InputCharacterDefinition'%(HIKStateGlobal))
mc.connectAttr('%s.message'%(HIKChar),'%s.destination'%(CustomRigRetargeter))
mc.connectAttr('%s.OutputCharacterState'%(HIKSolver),'%s.InputCharacterState'%(HIKStateGlobal))
mc.connectAttr('%s.message'%(HIKStateGlobal),'%s.source'%(CustomRigRetargeter))



