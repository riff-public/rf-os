import maya.cmds as mc
#HIKChar = 'CustomRig_Definition'
#HIKProp = 'HIKproperties1'


HIKChar = mc.createNode('HIKCharacterNode',n='Character1')
HIKProp = mc.createNode('HIKProperty2State',n='HIKproperties1')
#mc.connectAttr('%s.message'%(HIKProp),'%s.propertyState'%(HIKChar))

##### connect Joint to Character Definition ######
partList = ['Hips','Spine','Spine1','Spine2','Spine3','Neck','Head','RightShoulder','RightArm','RightForeArm','RightHand','LeftShoulder','LeftArm','LeftForeArm','LeftHand','RightUpLeg','RightLeg','RightFoot','LeftUpLeg','LeftLeg','LeftFoot']
jntList = ['root_skinJnt','spine1Pos_skinJnt','spine2Pos_skinJnt','spine3Pos_skinJnt','spine4Pos_skinJnt','neck1_jnt','head1_jnt','clav1_rgt_jnt','upArm_rgt_jnt','foreArm_rgt_jnt','wrist_rgt_jnt','clav1_lft_jnt','upArm_lft_jnt','foreArm_lft_jnt','wrist_lft_jnt','upLeg_rgt_jnt','lowLeg_rgt_jnt','ankle_rgt_jnt','upLeg_lft_jnt','lowLeg_lft_jnt','ankle_lft_jnt']

def connectDefinition(jnt='',part=''):
    if (jnt!='') and (part != ''):
        if not mc.objExists('%s.Character'%(jnt)):
            mc.addAttr(jnt,ln='Character',k=0)
            mc.connectAttr('%s.Character'%(jnt),'%s.%s'%(HIKChar,part))
        else:
            mc.connectAttr('%s.Character'%(jnt),'%s.%s'%(HIKChar,part))
    if part =='':
        return
if len(partList) == len(jntList):
    for i in range(len(partList)):
        connectDefinition(jntList[i],partList[i])

############### create and connect CustomRig Map Node to HIK characterNode ###########

HIKSolver= mc.createNode('HIKSolverNode')
HIKStateGlobal= mc.createNode('HIKState2GlobalSK')
CustomRigRetargeter = mc.createNode('CustomRigRetargeterNode')

mc.connectAttr('%s.OutputPropertySetState'%(HIKProp),'%s.InputPropertySetState'%(HIKSolver))
mc.connectAttr('%s.OutputCharacterDefinition'%(HIKChar),'%s.InputCharacterDefinition'%(HIKSolver))
mc.connectAttr('%s.OutputCharacterDefinition'%(HIKChar),'%s.InputCharacterDefinition'%(HIKStateGlobal))
mc.connectAttr('%s.message'%(HIKChar),'%s.destination'%(CustomRigRetargeter))
mc.connectAttr('%s.OutputCharacterState'%(HIKSolver),'%s.InputCharacterState'%(HIKStateGlobal))
mc.connectAttr('%s.message'%(HIKStateGlobal),'%s.source'%(CustomRigRetargeter))



###### connect Controller to CustomRig Map ######

def connectCustomRigMap(jnt='',ctl='',part='',type='r'):
    if (jnt =='') or (ctl =='') or (part==''):
        mc.warning('please type jnt,ctl or part in arguement.')
    else:
        customRigDfMap = mc.createNode('CustomRigDefaultMappingNode')
        if type =='r':
            mc.setAttr('%s.type'%customRigDfMap,1)
        elif type =='t':
            mc.setAttr('%s.type'%customRigDfMap,0)
        print mc.listAttr('CustomRigRetargeterNode1.mappings')
        mapID = mc.listConnections('CustomRigRetargeterNode1.mappings')
        if mapID == None:
            id = 0
        else:
            id = len(mapID)
        mc.connectAttr('%s.message'%customRigDfMap,'%s.mappings[%s]'%(CustomRigRetargeter,id))
        mc.connectAttr('%s.%sGX'%(HIKStateGlobal,part),'%s.matrixSource'%(customRigDfMap))
        mc.connectAttr('%s.message'%(jnt),'%s.destinationSkeleton'%(customRigDfMap))
        mc.connectAttr('%s.message'%(ctl),'%s.destinationRig'%customRigDfMap)
        mc.setAttr('%s.bodyPart'%(customRigDfMap),part,type='string')
        mc.setAttr('%s.identifier'%(customRigDfMap),id)
            
##### Hips ###
connectCustomRigMap('root_skinJnt','root_ctrl','Hips','r')
connectCustomRigMap('root_skinJnt','root_ctrl','Hips','t')
##### spine ####
connectCustomRigMap('spine1Pos_skinJnt','spine1Fk_ctrl','Spine','r')
connectCustomRigMap('spine2Pos_skinJnt','spine2Fk_ctrl','Spine1','r')
connectCustomRigMap('spine3Pos_skinJnt','spine3Fk_ctrl','Spine2','r')
connectCustomRigMap('spine4Pos_skinJnt','spine4Fk_ctrl','Spine3','r')

##### neck #####
connectCustomRigMap('neck1_skinJnt','neck_ctrl','Neck','r')

##### head ######
connectCustomRigMap('head1_skinJnt','head_ctrl','Head','r')

##### right arm #####
connectCustomRigMap('clav1_rgt_skinJnt','clavicle_rgt_ctrl','RightShoulder','r')
connectCustomRigMap('upArm_rgt_skinJnt','upArmFk_rgt_ctrl','RightArm','r')
connectCustomRigMap('foreArm_rgt_skinJnt','foreArmFk_rgt_ctrl','RightForeArm','r')
connectCustomRigMap('wrist_rgt_skinJnt','wristFk_rgt_ctrl','RightHand','r')

##### left arm #####
connectCustomRigMap('clav1_lft_skinJnt','clavicle_lft_ctrl','LeftShoulder','r')
connectCustomRigMap('upArm_lft_skinJnt','upArmFk_lft_ctrl','LeftArm','r')
connectCustomRigMap('foreArm_lft_skinJnt','foreArmFk_lft_ctrl','LeftForeArm','r')
connectCustomRigMap('wrist_lft_skinJnt','wristFk_lft_ctrl','LeftHand','r')

##### right Leg ####
connectCustomRigMap('upLeg_rgt_skinJnt','upLegFk_rgt_ctrl','RightUpLeg','r')
connectCustomRigMap('lowLeg_rgt_skinJnt','lowLegFk_rgt_ctrl','RightLeg','r')
connectCustomRigMap('ankle_rgt_skinJnt','ankleFk_rgt_ctrl','RightFoot','r')

##### left Leg ####
connectCustomRigMap('upLeg_lft_skinJnt','upLegFk_lft_ctrl','LeftUpLeg','r')
connectCustomRigMap('lowLeg_lft_skinJnt','lowLegFk_lft_ctrl','LeftLeg','r')
connectCustomRigMap('ankle_lft_skinJnt','ankleFk_lft_ctrl','LeftFoot','r')
