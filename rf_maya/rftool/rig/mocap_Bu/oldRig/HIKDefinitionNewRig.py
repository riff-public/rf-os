import maya.cmds as mc
#HIKChar = 'CustomRig_Definition'
#HIKProp = 'HIKproperties1'


HIKChar = mc.createNode('HIKCharacterNode',n='Character1')
HIKProp = mc.createNode('HIKProperty2State',n='HIKproperties1')
#mc.connectAttr('%s.message'%(HIKProp),'%s.propertyState'%(HIKChar))

##### connect Joint to Character Definition ######
partList = ['Hips','Spine','Spine1','Spine2','Spine3','Neck','Head','RightShoulder','RightArm','RightForeArm','RightHand','LeftShoulder','LeftArm','LeftForeArm','LeftHand','RightUpLeg','RightLeg','RightFoot','LeftUpLeg','LeftLeg','LeftFoot']
JntList = ['Root_Jnt','Spine1Pos_Jnt','Spine2Pos_Jnt','Spine3Pos_Jnt','Spine4Pos_Jnt','Neck_Jnt','Head_Jnt','Clav_R_Jnt','UpArm_R_Jnt','Forearm_R_Jnt','Wrist_R_Jnt','Clav_L_Jnt','UpArm_L_Jnt','Forearm_L_Jnt','Wrist_L_Jnt','UpLeg_R_Jnt','LowLeg_R_Jnt','Ankle_R_Jnt','UpLeg_L_Jnt','LowLeg_L_Jnt','Ankle_L_Jnt']

def connectDefinition(Jnt='',part=''):
    if (Jnt!='') and (part != ''):
        if not mc.objExists('%s.Character'%(Jnt)):
            mc.addAttr(Jnt,ln='Character',k=0)
            mc.connectAttr('%s.Character'%(Jnt),'%s.%s'%(HIKChar,part))
        else:
            mc.connectAttr('%s.Character'%(Jnt),'%s.%s'%(HIKChar,part))
    if part =='':
        return
if len(partList) == len(JntList):
    for i in range(len(partList)):
        connectDefinition(JntList[i],partList[i])

############### create and connect CustomRig Map Node to HIK characterNode ###########

HIKSolver= mc.createNode('HIKSolverNode')
HIKStateGlobal= mc.createNode('HIKState2GlobalSK')
CustomRigRetargeter = mc.createNode('CustomRigRetargeterNode')

mc.connectAttr('%s.OutputCharacterDefinition'%(HIKChar),'%s.InputCharacterDefinition'%(HIKState2SK))
mc.connectAttr('%s.OutputCharacterState'%(HIKChar),'%s.InputCharacterDefinition'%(HIKState2SK))

mc.connectAttr('%s.OutputPropertySetState'%(HIKProp),'%s.InputPropertySetState'%(HIKSolver))
mc.connectAttr('%s.OutputCharacterDefinition'%(HIKChar),'%s.InputCharacterDefinition'%(HIKSolver))
mc.connectAttr('%s.OutputCharacterDefinition'%(HIKChar),'%s.InputCharacterDefinition'%(HIKStateGlobal))
mc.connectAttr('%s.message'%(HIKChar),'%s.destination'%(CustomRigRetargeter))
mc.connectAttr('%s.OutputCharacterState'%(HIKSolver),'%s.InputCharacterState'%(HIKStateGlobal))
mc.connectAttr('%s.message'%(HIKStateGlobal),'%s.source'%(CustomRigRetargeter))



###### connect Controller to CustomRig Map ######

def connectCustomRigMap(Jnt='',ctl='',part='',type='r'):
    if (Jnt =='') or (ctl =='') or (part==''):
        mc.warning('please type Jnt,ctl or part in arguement.')
    else:
        customRigDfMap = mc.createNode('CustomRigDefaultMappingNode')
        if type =='r':
            mc.setAttr('%s.type'%customRigDfMap,1)
        elif type =='t':
            mc.setAttr('%s.type'%customRigDfMap,0)
        print mc.listAttr('CustomRigRetargeterNode1.mappings')
        mapID = mc.listConnections('CustomRigRetargeterNode1.mappings')
        if mapID == None:
            id = 0
        else:
            id = len(mapID)
        mc.connectAttr('%s.message'%customRigDfMap,'%s.mappings[%s]'%(CustomRigRetargeter,id))
        mc.connectAttr('%s.%sGX'%(HIKStateGlobal,part),'%s.matrixSource'%(customRigDfMap))
        mc.connectAttr('%s.message'%(Jnt),'%s.destinationSkeleton'%(customRigDfMap))
        mc.connectAttr('%s.message'%(ctl),'%s.destinationRig'%customRigDfMap)
        mc.setAttr('%s.bodyPart'%(customRigDfMap),part,type='string')
        mc.setAttr('%s.identifier'%(customRigDfMap),id)
            
##### Hips ###
connectCustomRigMap('Root_Jnt','Root_Ctrl','Hips','r')
connectCustomRigMap('Root_Jnt','Root_Ctrl','Hips','t')
##### Spine ####
connectCustomRigMap('Spine1Pos_Jnt','Spine1_Ctrl','Spine','r')
connectCustomRigMap('Spine2Pos_Jnt','Spine2_Ctrl','Spine1','r')
connectCustomRigMap('Spine3Pos_Jnt','Spine3_Ctrl','Spine2','r')
connectCustomRigMap('Spine4Pos_Jnt','Spine4_Ctrl','Spine3','r')

##### Neck #####
connectCustomRigMap('Neck_Jnt','Neck_Ctrl','Neck','r')

##### Head ######
connectCustomRigMap('Head_Jnt','Head_Ctrl','Head','r')

##### right arm #####
connectCustomRigMap('Clav_R_Jnt','Clav_R_Ctrl','RightShoulder','r')
connectCustomRigMap('UpArm_R_Jnt','UpArmFk_R_Ctrl','RightArm','r')
connectCustomRigMap('Forearm_R_Jnt','ForearmFk_R_Ctrl','RightForeArm','r')
connectCustomRigMap('Wrist_R_Jnt','WristFk_R_Ctrl','RightHand','r')

##### left arm #####
connectCustomRigMap('Clav_L_Jnt','Clav_L_Ctrl','LeftShoulder','r')
connectCustomRigMap('UpArm_L_Jnt','UpArmFk_L_Ctrl','LeftArm','r')
connectCustomRigMap('Forearm_L_Jnt','ForearmFk_L_Ctrl','LeftForeArm','r')
connectCustomRigMap('Wrist_L_Jnt','WristFk_L_Ctrl','LeftHand','r')

##### right Leg ####
connectCustomRigMap('UpLeg_R_Jnt','UpLegFk_R_Ctrl','RightUpLeg','r')
connectCustomRigMap('LowLeg_R_Jnt','LowLegFk_R_Ctrl','RightLeg','r')
connectCustomRigMap('Ankle_R_Jnt','AnkleFk_R_Ctrl','RightFoot','r')

##### left Leg ####
connectCustomRigMap('UpLeg_L_Jnt','UpLegFk_L_Ctrl','LeftUpLeg','r')
connectCustomRigMap('LowLeg_L_Jnt','LowLegFk_L_Ctrl','LeftLeg','r')
connectCustomRigMap('Ankle_L_Jnt','AnkleFk_L_Ctrl','LeftFoot','r')
