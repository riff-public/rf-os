import maya.cmds as mc

HIKChar = mc.createNode('HIKCharacterNode',n='Character1')
HIKProp = mc.createNode('HIKProperty2State',n='HIKproperties1')

mc.connectAttr('%s.message'%(HIKProp),'%s.propertyState'%(HIKChar))

##### connect Joint to Character Definition ######
HIKList = ['Hips','Spine','Spine1','Spine2','Spine3','Neck','Head','RightShoulder','RightArm','RightForeArm','RightHand','LeftShoulder','LeftArm','LeftForeArm','LeftHand','RightUpLeg','RightLeg','RightFoot','RightToeBase','LeftUpLeg','LeftLeg','LeftFoot','LeftToeBase']
for i in HIKList:
    print i
    if not mc.objExists('%s.Character'%(i)):
        mc.addAttr(i,ln='Character',k=0)
        mc.connectAttr('%s.Character'%(i),'%s.%s'%(HIKChar,i))
    else:
        mc.connectAttr('%s.Character'%(i),'%s.%s'%(HIKChar,i))

