import maya.cmds as mc
import maya.mel as mel
import maya.app.hik.retargeter as r
import sys
import pymel.core as pmc
#sys.path.append(r'N:/Staff/Hong/Scripts/Mocap/DarkHorse/')

#sys.path.append(r'D:\Script\core\rf_maya\rftool\rig\mocap')
import HIKMocapList as mth #mocapData joint List

#import HIKCharOldList as mth #mocapData joint List

#import mxmHIKMocapList as mth #mixamo mocapData joint List

def setTPose():
	currentF=mc.currentTime(q=1)
	back1F = currentF-1
	partList = mth.partJntList
	
	def saveFirstFrame(jnt):
		mc.currentTime(currentF)
		mc.setKeyframe(jnt,at='rotateX')
		mc.setKeyframe(jnt,at='rotateY')
		mc.setKeyframe(jnt,at='rotateZ')
	
	def setRotZero(jnt):
		print "%s.rotate"%(jnt)
		mc.currentTime(back1F)
		mc.setAttr("%s.rotate"%(jnt),0,0,0,type= 'double3')
		mc.setKeyframe(jnt, at='rotateX')
		mc.setKeyframe(jnt, at='rotateY')
		mc.setKeyframe(jnt, at='rotateZ')
	
	def setHips(obj):
		node = pmc.PyNode(obj)
		ns = node.namespace()
		print ns
		null = mc.createNode('transform')
		mc.delete(mc.pointConstraint('{}Hips'.format(ns),null,mo=0))

		mc.setAttr(null+'.rx',-90)

		nullRot = mc.xform(ws=1,rotation=1,q=1)
		mc.xform('{}Hips'.format(ns),ws=1,rotation = nullRot)
		mc.delete(null)	

	for i in partList[:]:
		if i!= '':
			NS = getNS(i)
			if NS != '':
				jnt = '%s:%s'%(NS,i)
			else:
				jnt = i

			if 'Hips' not in i:
				saveFirstFrame(jnt)        
				setRotZero(jnt)
			else:
				saveFirstFrame(jnt)
				setHips(jnt)
				#mc.setAttr('%s.rx'%jnt,-90)
				#mc.setAttr('%s.ry'%jnt,0)
				#mc.setAttr('%s.rz'%jnt,0)
				#mc.setKeyframe(jnt, at='rotateX')
				#mc.setKeyframe(jnt, at='rotateY')
			#mc.setKeyframe(jnt, at='rotateZ')   
	#mc.setAttr('Solving.rx',90)            
	mc.currentTime(back1F)

def getNS(obj):
	#sel = mc.ls(sl=True)
	NS = pmc.PyNode(obj).namespace()
	if NS != '':
		NS = sel[0].split(':')[0]
		print NS
		return NS
	else:
		return ''

######### create HIK Definition for Character #########################################
def crateChar(NS='',Char='Character1'): 
	partNumList = mth.partNumList
	partNameList = mth.partNameList
	partJntList = mth.partJntList
	charName = Char
	mel.eval(" hikCreateDefinition();string $Char =hikGetCurrentCharacter();string $newName =`rename $Char "+"\""+charName+"\"`;")
	for i in range(len(partNumList)):
		#print i
		if partJntList[i]!='':
			mc.select(charName)
			if NS != '':
				partJnt = NS +':'+ partJntList[i]
			else:
				partJnt = partJntList[i]
			mel.eval(str("setCharacterObject("+"\""+partJnt+"\",$newName,"+"\""+str(partNumList[i])+"\",0);"))


	mel.eval("HIKCharacterControlsTool;")
	


################### run script ######################################################################
def createDefinition():
	mel.eval("HIKCharacterControlsTool")
	NS=getNS(mc.ls(sl=True)[0])
	#setTPose()
	crateChar(NS)
#createDefinition()
#createCustomRigMap()
#delUnknownHIKNode(Character)
#moveAnimLayer(ctlList,NS)





