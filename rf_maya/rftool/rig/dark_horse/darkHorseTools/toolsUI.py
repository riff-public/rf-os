from ncmel import textureTools as txr
from ncmel import cleanUpTools as cut
import pymel.core as pm
import maya.cmds as mc
reload( txr )
reload( cut )

class ToolsUI( object ) :
    
    def __init__( self , version = '1.0' ) :
        
        if pm.window( 'ToolsUI' , exists = True ) :
            pm.deleteUI( 'ToolsUI' )
        
        width = 240
        window = pm.window( 'ToolsUI' , title = "NC-Tools  %s" %version , width = width , mnb = True , mxb = False , sizeable = True , rtf = True )
        
        allLayout = pm.rowColumnLayout ( w = width , nc = 1 , columnWidth = [ ( 1 , width ) ] )
        with allLayout :
            refFrameLayout = pm.frameLayout ( label = 'Reference Tools' , collapsable = True , collapse = False , bgc = ( 0 , 0 , 0 ))
            
            with refFrameLayout :
                refLayout = pm.rowColumnLayout ( w = width , nc = 1 , columnWidth = [ ( 1 , width-4 ) ] )
                
                with refLayout :
                    pm.button( 'Import Reference' , label = 'Import Reference' , h = 35 , c = self.importRef )
                    pm.separator( vis = False )
                    pm.button( 'Remove Reference' , label = 'Remove Reference' , h = 35 , c = self.removeRef )
                    pm.separator( vis = False )
                    pm.button( 'Reload Reference' , label = 'Reload Reference' , h = 35 , c = self.reloadRef )

            namespaceFrameLayout = pm.frameLayout ( label = 'Namespace Tools' , collapsable = True , collapse = False , bgc = ( 0 , 0 , 0 ))
            
            with namespaceFrameLayout :
                namespaceLayout = pm.rowColumnLayout ( w = width , nc = 1 , columnWidth = [ ( 1 , width-4 ) ] )
                
                with namespaceLayout :
                    pm.button( 'Remove All Namespace' , label = 'Remove All Namespace' , h = 35 , c = self.removeAllNamespace )
                    pm.separator( vis = False )
                    pm.button( 'Remove Seleted Namespace' , label = 'Remove Seleted Namespace' , h = 35 , c = self.removeNamespace )

            shadFrameLayout = pm.frameLayout ( label = 'Shading Tools' , collapsable = True , collapse = False , bgc = ( 0 , 0 , 0 ))
            
            with shadFrameLayout :
                shadLayout = pm.rowColumnLayout ( w = width , nc = 1 , columnWidth = [ ( 1 , width-4 ) ] )
                
                with shadLayout :
                    pm.button( 'Fix Texture Path' , label = 'Fix Texture Path' , h = 35 , c = self.fixTextureSelectPath )
                    pm.separator( vis = False )
                    pm.button( 'TransferShade' , label = 'Transfer Shading' , h = 35 , c = self.transferShade )
                    pm.separator( vis = False )
                    pm.button( 'TransferShadeWithOutRef' , label = 'Transfer Shading With Out Ref' , h = 35 , c = self.transferShadeWithOutRef )
                    
        pm.showWindow( window )

    def importRef( self , arg = None ) :
        cut.importRef()

    def removeRef( self , arg = None ) :
        cut.removeRef()

    def reloadRef( self , arg = None ) :
        cut.reloadRef()

    def removeAllNamespace( self , arg = None ) :
        cut.removeAllNamespace()

    def removeNamespace( self , arg = None ) :
        cut.removeNamespace()

    def fixTextureSelectPath( self , arg = None ) :
        txr.fixTextureSelectPath()

    def transferShade( self , arg = None ) :
        txr.transferShade()

    def transferShadeWithOutRef( self , arg = None ) :
        txr.transferShadeWithOutRef()