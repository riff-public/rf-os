from rftool.shade import shade_utils as shad
reload(shad)
from rftool.shade import shade_utils
from rftool.utils import  file_utils
reload(shade_utils)
import maya.cmds as mc
import json
import os

def exportShade():

    path = mc.file(q=True, sn=True)
    rig_path = getPath(path)
    recursively_create_folder(rig_path)
    sel = mc.ls(sl=True)
    
    for ix in sel:
        print ix
    	name = ix.split('_')[0]
    	truePath = os.path.join(rig_path, name).replace('\\', '/')
    	shadeEx = export_shade('%s.ma'% truePath, targetGrp = ix)
    	shadeData = shade_utils.export_shade_data('%s.yml'% truePath , targetGrp = ix)
    	print name
    	writeFile(rig_path, name, shadeEx, shadeData)


def writeFile(rigPath, name, shadeEx, shadeData):
    data_path = "%s/data.txt"%rigPath
    #print data_path
    if not os.path.exists(data_path):
        with open(data_path, 'w') as f:
            #print f
            data= {name:[shadeEx, shadeData]}
            f.write(json.dumps(data, ensure_ascii=False))
    else:
        with open(data_path, 'r+') as fa:
            data = json.load(fa)
            print '-------', data
            if name in data.keys():
                data[name] = [shadeEx, shadeData]
            else:
                data[name] = [shadeEx, shadeData]
            fa.seek(0)
            json.dump(data, fa)
            fa.truncate()

#exportShade()




def getPath(path):
    dir_path = os.path.dirname(path).replace('\\', '/')
    char_path = os.path.dirname(dir_path)
    rig_path = os.path.join(char_path, 'rig').replace('\\', '/')
    return rig_path

def importshade():
    path = mc.file(sn=True,q=True)
    rig_path = getPath(path)
    data_path = '%s/data.txt'%rig_path
    with open(data_path, 'r') as fa:
        dict_data = fa.read()
    dict = json.loads(dict_data)
    nameSpace = mc.ls(sl=True,an=True, showNamespace=True)[1]
    name = mc.ls(sl=True)[0]
    mesh_name = name.split(':')[1].split('_')
    
    data_asset = dict[mesh_name[0]]
    shd_name_space = check_name_space('shade01')
    shad.import_shade( data_asset[0] , shd_name_space )
    print data_asset[1], shd_name_space , nameSpace
    apply_shade( data_asset[1] , shd_name_space , nameSpace )

def check_name_space(name_space):
    if (mc.namespace(exists= name_space ) ):
        count_name_space = int(name_space.split('shade')[1])+1
        new_name_space = 'shade%02d'%(count_name_space)
        return check_name_space(new_name_space)
    else:
        return name_space
#importshade()

def export_shade(exportPath, targetGrp=None, objs=None) :
    """ export shadingEngines """
    exportResult = False
    mtime = 0 if not os.path.exists(exportPath) else os.path.getmtime(exportPath)

    shaderInfo = shade_utils.list_shader(targetGrp=targetGrp, objs=objs)

    if shaderInfo :
        shadingEngines = [shaderInfo.get(shadeNode).get('shadingEngine') for shadeNode in shaderInfo if not mc.referenceQuery(shadeNode, isNodeReferenced = True)]
        mc.select(shadingEngines, r=True, ne=True)
        result = mc.file(exportPath, force = True, options = 'v=0', typ = 'mayaAscii', pr = True, es = True, shader=True)

        if os.path.exists(exportPath):
            exportResult = True if not mtime == os.path.getmtime(exportPath) else False

    if exportResult:
        return result

def recursively_create_folder(path):
    if not os.path.exists(path):
        os.makedirs(path)
 
def apply_shade(shadeDataFile, shadeNamespace, targetNamespace):
    shadeInfo = file_utils.ymlLoader( shadeDataFile )
    keys = shadeInfo.keys()

    for key in keys :
        shade = key
        objs = shadeInfo.get(key).get('objs')
        shad = shadeInfo.get(key).get('shadingEngine')[0]

        for obj in objs :
            if '|' in obj :
                insert = '|%s:'%targetNamespace
                obj = insert.join(obj.split('|'))

            mc.select( '%s:%s' %(targetNamespace , obj))
            mc.hyperShade( assign = '%s:%s' %(shadeNamespace,shade))