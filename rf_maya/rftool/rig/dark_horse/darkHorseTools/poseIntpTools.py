import maya.cmds as mc
import maya.mel as mm
import pymel.core as pm
import operator
import re
import pickle
import os
import sys
import subprocess
from decimal import Decimal, ROUND_HALF_UP
from nuTools.corrective import invertDeformation

textEditor = "C:/Program Files/Sublime Text 3/sublime_text.exe"
imagePath = "O:/Pipeline/core/rf_maya/rftool/rig/ncmel/picture/PoseInterpolatorToolsTitle.jpg"
rootPath = "O:/Pipeline/core/rf_maya/rftool/rig/ncmel"
mm.eval( 'source "%s/BSpiritCorrectiveShape.mel" ;' %rootPath )
mm.eval( 'source "%s/copyShape.mel" ;' %rootPath )
mm.eval( 'source "%s/mirrorShape.mel" ;' %rootPath )

class PoseInterpolatorUI( object ) :
    
    def __init__ ( self ) :
        
        width = 520
        
        if pm.window( 'PoseInterpolatorUI' , exists = True ) :
            pm.deleteUI( 'PoseInterpolatorUI' )
            
        window = pm.window( 'PoseInterpolatorUI', title = "Corrective Blendshape Editor"  , width = width , height = 500 ,
                             mnb = True , mxb = False , sizeable = True , rtf = True , menuBar = True )
    
        window = pm.window( 'PoseInterpolatorUI', e = True , width = width )
        pm.menu( label = 'Skelton' , tearOff = True )
        pm.menuItem( label = 'Write Skelton' , c = write_selected_hierarchy )
        pm.menuItem( label = 'Read Skelton' , c = build_skel_group )
        pm.menu( label = 'Weight' , tearOff = True )
        pm.menuItem( label = 'Write Weight' , c = write_selected_weight )
        pm.menuItem( label = 'Read Weight' , c = read_selected_weight )
        pm.menu( label = 'Keys' , tearOff = True )
        pm.menuItem( label = 'Edit Keys Frame' , c = edit_key_anim )
        pm.menuItem( label = 'Add Keys Frame' , c = import_key_anim )
        pm.menu( label = 'Interpolator' , tearOff = True )
        pm.menuItem( label = 'Mirror Interpolator' , c = self.mirror_intpNode_button )
        pm.menuItem( label = 'Reconnect Interpolator' , c = reconnect_poseIntp_node )
        pm.menuItem( label = 'Limit On Off' , c = self.limit_on_off )
        pm.menu( label = 'Tools' , tearOff = True )
        pm.menuItem( label = 'Copy Shape' , c = self.copy_shape_button )
        pm.menuItem( label = 'Mirror Shape' , c = self.mirror_shape_button )
        pm.menuItem( label = 'Flip Shape' , c = self.flip_shape )
        pm.menu( label = 'Cleanup' , tearOff = True )
        pm.menuItem( label = 'Group Template' , c = create_group_template )
        pm.menuItem( label = 'Rename Geo' , c = rename_geo )
        pm.menu( label = 'Help' , tearOff = True )
        pm.menuItem( label = 'Reload Script' , c = self.reload_script )
        
        frameInfoLayout = pm.rowColumnLayout ( w = width  , nc = 1 , columnWidth = ( 1 , 518 ))
        with frameInfoLayout :
            pm.picture( h = 83 , image = imagePath )
            pm.separator( h = 2 , st = 'in' )
        
        frameLayout = pm.rowColumnLayout ( w = width , nc = 3 , columnWidth = [( 1 , 220 ) , ( 2 , 22 ) , ( 3 , 369 )])
        with frameLayout :
            poseIntpLayout = pm.rowColumnLayout ( w = width , nc = 1 , columnWidth = ( 1 , 218 ))
            with poseIntpLayout :
                pm.text( label = 'Pose Interpolator' )
                pm.separator( h = 5 , vis = False)
                pm.textScrollList( 'poseIntpListsTS' , h = 369 , numberOfRows = 15 , ams = True , fn = 'boldLabelFont' , sc = self.click_intp_cmd , dcc = self.double_click_intp_cmd )
                pm.separator( h = 5 , vis = False)

                poseIntpBTLayout = pm.rowColumnLayout ( w = width , nc = 2 , columnWidth = [( 1 , 109) , ( 2 , 108)])
                with poseIntpBTLayout :
                    pm.button( label = 'Create' , h = 30 , c = self.create_intpNode_button )
                    pm.button( label = 'Delete' , h = 30 , c = self.delete_intpNode_button )
              
            pm.popupMenu( 'poseIntpListMenu' , parent = 'poseIntpListsTS' , ctl = False , button = 3 )
            pm.menuItem( l = 'Reload' , c = self.reload_pose_intp )
                
            poseShapeLayout = pm.rowColumnLayout ( w = width , nc = 1 , columnWidth = ( 1 , 399))
            with poseShapeLayout :
                poseShapeSubLayout = pm.rowColumnLayout ( w = width , nc = 2 , columnWidth = [( 1 , 250 ) , ( 2 , 45 )])
                pm.text( label = 'Pose Shape' )
                pm.text( label = 'Frame' )
                pm.separator( h = 4 , vis = False)
                pm.separator( h = 4 , vis = False)
                with poseShapeSubLayout :
                    pm.textScrollList( 'poseShapeListsTS' , h = 321 , numberOfRows = 15 , fn = 'boldLabelFont' , dcc = self.double_click_shape_cmd )
                    pm.textScrollList( 'frameTS' , h = 321 , numberOfRows = 15 , fn = 'boldLabelFont' , en = False )

                separatorALayout = pm.rowColumnLayout ( w = width , nc = 1 , columnWidth = ( 1 , 368))
                with separatorALayout :
                    pm.separator( h = 4 , vis = False)
                    
                infoTextLayout = pm.rowColumnLayout ( w = width , nc = 2 , columnWidth = [( 1 , 140),( 2 , 140)])
                with infoTextLayout :
                    pm.text( label = 'Geometry Group' )
                    pm.text( label = 'Blendshape Node' )
                    pm.separator( h = 4 , vis = False)
                    pm.separator( h = 4 , vis = False)
                    
                infoLayout = pm.rowColumnLayout ( w = width , nc = 4 , columnWidth = [(1 , 127),(2 , 20),(3 , 127),(4 , 20)])
                with infoLayout :
                    pm.textField( 'geometryTF' , h = 20 , en = True )
                    pm.button( label = '+' , c = self.add_item_geometry_button )
                    pm.textField( 'blendshapeTF' , h = 20 , en = True )
                    pm.button( label = '+' , c = self.add_item_blendShape_button )
                    
                poseShapeBtLayout = pm.rowColumnLayout ( w = width , nc = 2 , columnWidth = [(1 , 147),(2 , 147)])
                with poseShapeBtLayout :
                    pm.separator( h = 2 , vis = False)
                    pm.separator( h = 2 , vis = False)
                    pm.button( 'addShapeBT' , label = 'Add Shape' , h = 30 , c = self.add_shape_button )
                    pm.button( 'editShapeBT' , label = 'Edit Shape' , h = 30 , c = self.edit_shape_button )
                    pm.popupMenu( 'editShapeBTMenu' , parent = 'editShapeBT' , ctl = False , button = 3 )
                    pm.menuItem( l = 'Duplicate Reset Pose' , c = self.duplicate_reset_pose_button )
                    pm.menuItem( l = 'Duplicate 50% Pose' , c = self.duplicate_50_pose_button )
                    pm.menuItem( d = True )
                    pm.menuItem( l = 'Sculpt Tools' , c = "mm.eval('SculptGeometryToolOptions')" )
            
            pm.popupMenu( 'poseShapeListMenu' , parent = 'poseShapeListsTS' , ctl = False , button = 3 )
            pm.menuItem( l = 'Clear Shape' , c = self.clear_shape )
                    
        progressBarLayout = pm.rowColumnLayout ( w = width  , nc = 1 , columnWidth = ( 1 , 518 ))
        with progressBarLayout :
            pm.separator( h = 3 , st = 'in' )
            pm.progressBar( 'progressControl' , maxValue = 100 , width = 518 , en = False )
            
        pm.showWindow( window )
        pm.autoKeyframe( state = False )

    def get_info( self ):
        poseIntpSel = pm.textScrollList( 'poseIntpListsTS' , q = True , si = True )
        poseIntpAll = pm.textScrollList( 'poseIntpListsTS' , q = True , ai = True )
        poseShpSel = pm.textScrollList( 'poseShapeListsTS' , q = True , si = True )
        poseShpAll = pm.textScrollList( 'poseShapeListsTS' , q = True , ai = True )
        poseShpIn = pm.textScrollList( 'poseShapeListsTS' , q = True , sii = True )
        geo = pm.textField( 'geometryTF' , q = True , tx = True )
        bshNd = pm.textField( 'blendshapeTF' , q = True , tx = True )
        frameAll = pm.textScrollList( 'frameTS' , q = True , ai = True )

        return { 'poseIntpSel' : poseIntpSel ,
                 'poseIntpAll' : poseIntpAll ,
                 'poseShpSel' : poseShpSel ,
                 'poseShpAll' : poseShpAll ,
                 'poseShpIn' : poseShpIn ,
                 'geo' : geo , 
                 'bshNd' : bshNd ,
                 'frameAll' : frameAll }

    def get_interpolator_node( self , *args ):
        return mc.ls( type = 'poseInterpolator' )
        
    def get_keyframe( self , obj ):
        return pm.keyframe( obj , query = True )
        
    def get_keyframe_from_attr( self , obj , attr ):
        return pm.keyframe( obj , at = True , query = True )
        
    def get_minMax_keyframe_from_poseShape( self , attr ):
        info = self.get_info()
        at = { 'rx' : -3 , 'ry' : -2  , 'rz' : -1 }
        value = [ ]
        minMax = { }
        
        for poseShp in info['poseShpAll'] :
            separate = poseShp.split('_')[at[attr]]
            
            if 'n' in separate :
                v = separate.split('n')[-1]
                separate = '-%s' %v
            
            value.append(int(separate))
        
        if value :
            minVal = min(value)
            maxVal = max(value)
            
            if minVal < 0 :
                minMax['min'] = info['poseShpAll'][value.index(minVal)]
                minMax['minVal'] = minVal
            else :
                minMax['min'] = ''
                minMax['minVal'] = ''
                
            if maxVal > 0 :
                minMax['max'] = info['poseShpAll'][value.index(maxVal)]
                minMax['maxVal'] = maxVal
            else :
                minMax['max'] = ''
                minMax['maxVal'] = ''
            
            greaterThan = [ ]
            lessThan = [ ]
            
            for v in value :
                if v > 0 :
                    greaterThan.append( info['poseShpAll'][value.index(v)] )
                if v < 0 :
                    lessThan.append( info['poseShpAll'][value.index(v)] )
                
            minMax['val>'] = greaterThan
            minMax['val<'] = lessThan
                
            return minMax
        
    def list_anim_curve( self ):
        info = self.get_info()
        animCuvs = mc.listConnections( info['poseIntpSel'] , type = 'animCurve' )
        return animCuvs

    def set_time_range( self , obj ):
        frame = self.get_keyframe( obj )
        if frame :
            timeslide = pm.playbackOptions( e = True , minTime = frame[0] , maxTime = frame[-1])
        
    def list_interpolator_node( self , *args ):
        intpNds = self.get_interpolator_node()
        pm.textScrollList( 'poseIntpListsTS' , e = True , ra = True )
        
        if intpNds :
            jntDict = [ ]
            jntKeys = { }
            jntNotKeys = [ ]
            jntNotFound = [ ]
            
            for intpNd in intpNds :
                jntNm = intpNd.split('_poseInterpolator')[0]
                jntDict.append( jntNm )
                
            if jntDict :
                for jnt in jntDict :
                    if mc.objExists( jnt ):
                        key = self.get_keyframe(jnt)
                        if key :
                            key = int(key[0])
                            jntKeys[key] = jnt
                        else :
                            jntNotKeys.append( jnt )
                    else :
                        jntNotFound.append( jnt )
                        
            if jntKeys :
                jntKeysSorted = sorted(jntKeys.items())
                for jnt in jntKeysSorted :
                    pm.textScrollList( 'poseIntpListsTS' , e = True , a = jnt[1] )
                        
            if jntNotKeys :
                for jnt in jntNotKeys :
                    pm.textScrollList( 'poseIntpListsTS' , e = True , a = jnt )
                        
            if jntNotFound :
                for jnt in jntNotFound :
                    pm.textScrollList( 'poseIntpListsTS' , e = True , a = jnt )
        
            if pm.textScrollList( 'poseIntpListsTS' , q = True , ai = True ) :
                pm.textScrollList( 'poseIntpListsTS' , e = True , sii = 1 )
                
    def list_key_pose_shape( self , obj ):
        poseShapeNm = [ ]
        attrVal = self.get_attr_and_key_value( obj )
        
        if attrVal :
            for attrKey in attrVal.keys() :
                for each in attrVal[attrKey] :
                    rot = { 'rotateX' : 0 , 'rotateY' : 0 , 'rotateZ' : 0 }
                    rot[attrKey] = int(Decimal(each[1]).quantize(0, ROUND_HALF_UP))
                    rot[attrKey] = '%s' %rot[attrKey]
                    
                    if '-' in rot[attrKey] :
                        val = rot[attrKey].split('-')[-1]
                        rot[attrKey] = 'n%s' %val
                    
                    if not rot[attrKey] == '0' :
                        poseNm = '%s_%s_%s_%s' %( obj , rot['rotateX'] , rot['rotateY'] , rot['rotateZ'])
                        poses = ( poseNm , int(each[0]) )
                        poseShapeNm.append(poses)
        
        return poseShapeNm
                
    def list_bsh_pose_shape( self , name , bsh ):
        if bsh :
            shapes = [ ]
            listBsh = self.get_shape_pose( bsh )
            if listBsh :
                for shp in listBsh :
                    if name in shp :
                        shapes.append(shp)
            
            return shapes
        
    def click_intp_cmd( self , *args ):
        obj = [ ]

        info = self.get_info()
        bshNd = info['bshNd']

        pm.textScrollList( 'poseShapeListsTS' , e = True , ra = True )
        pm.textScrollList( 'frameTS' , e = True , ra = True )

        if info['poseIntpAll'] :
            poseIntpSel = info['poseIntpSel'][0]
        
            if poseIntpSel :
                poseShape = self.list_key_pose_shape( poseIntpSel )
                
                if poseShape :
                    for poses in poseShape :
                        pm.textScrollList( 'poseShapeListsTS' , e = True , a = poses[0])
                        pm.textScrollList( 'frameTS' , e = True , a = poses[1] )
                else :
                    bshShape = self.list_bsh_pose_shape( poseIntpSel , bshNd )
                    if bshShape :
                        for bsh in bshShape :
                            pm.textScrollList( 'poseShapeListsTS' , e = True , a = bsh )
    
    def double_click_intp_cmd( self , *args ):
        info = self.get_info()
        poseIntpSel = info['poseIntpSel'][0]

        if mc.objExists( poseIntpSel ):
            self.set_time_range( poseIntpSel )
            pm.select( poseIntpSel )
    
    def double_click_shape_cmd( self , *args ):
        info = self.get_info()
        poseShpSel = info['poseShpSel']

        self.go_to_pose_intp( poseShpSel )
        self.double_click_intp_cmd()
        
    def get_attr_and_key_value( self , obj ):
        attrLists = { }
        
        if mc.objExists( obj ):
            animCuvs = mc.listConnections( obj , type = 'animCurve' )
            exception = [ 'rotateX' , 'rotateY' , 'rotateZ' ]
            
            if animCuvs :
                for anim in animCuvs :
                    attr = anim.split('_')[-1]
                    
                    if attr in exception :
                        range = pm.keyframe( obj , at = attr , query = True )
                        value = pm.keyframe( obj , at = attr , t =(range[0],range[-1]) , q = True , vc = True , tc = True )
                        attrLists[ attr ] = value
            
        return attrLists
        
    def get_shape_pose( self , bshNd ):
        if bshNd : 
            return mc.listAttr( '%s.w' %bshNd , m = True )
        
    def get_index_shape_pose( self , bshNd , poseShp ):
        shapes = self.get_shape_pose( bshNd )
        return shapes.index( poseShp )
        
    def reload_pose_intp( self , *args ):
        self.list_interpolator_node()
        self.click_intp_cmd()
    
    def reload_pose_shape( self , *args ):
        self.click_intp_cmd()
    
    def go_to_pose_intp( self , poseShp ):
        pm.textScrollList( 'poseShapeListsTS' , e = True , si = poseShp )

        info = self.get_info()
        poseShpIn = info['poseShpIn'][0]
        frameAll = info['frameAll']

        if frameAll :
            pm.currentTime( frameAll[poseShpIn-1] )
        
    def add_pose( self , poseIntp , poseShp , geo , bsnNd ):
        cmd = 'poseInterpolatorAddShapePose( "%s" , "%s" , "swing" , {"%s"} , 0 )' %( poseIntp , poseShp , bsnNd )
        pm.select( geo )
        mm.eval(cmd)
        
    def add_pose_all( self , intpNd , geo , bshNd ):
        info = self.get_info()
        poseShpAll = info['poseShpAll']
        
        for poseShp in poseShpAll :
            self.go_to_pose_intp( poseShp )
            self.add_pose( intpNd , poseShp , geo , bshNd )
        
    def add_pose_last_value( self , intpNd , geo , bshNd ):
        attrs = [ 'rx' , 'rz' ]
        i = 0
        
        for attr in attrs :
            val = self.get_minMax_keyframe_from_poseShape( attr )
            
            if val :
                if val['max'] :
                    aniCuvs = len(self.list_anim_curve())
                    angle = val['maxVal']

                    if aniCuvs > 1 :
                        if val['maxVal'] > 90 :
                            angle = 90

                    self.go_to_pose_intp( val['max'] )
                    self.add_pose( intpNd , val['max'] , geo , bshNd )
                    mc.setAttr( '%s.pose[%s].isIndependent' %(intpNd,i) , 1 )
                    mc.setAttr( '%s.pose[%s].poseRotationFalloff' %(intpNd,i) , angle )
                    i+=1
                    
                if val['min'] :
                    aniCuvs = len(self.list_anim_curve())
                    angle = abs(val['minVal'])

                    if aniCuvs > 1 :
                        if val['minVal'] < -90 :
                            angle = 90

                    self.go_to_pose_intp( val['min'] )
                    self.add_pose( intpNd , val['min'] , geo , bshNd )
                    mc.setAttr( '%s.pose[%s].isIndependent' %(intpNd,i) , 1)
                    mc.setAttr( '%s.pose[%s].poseRotationFalloff' %(intpNd,i) , angle )
                    i+=1
        
    def create_intpNode( self , name = '' , bsn = '' , twist = '1' ) :
        # Twist 0 = X , 1 = Y , 2 = Z
        mc.select( name )
        cmd1 = 'createPoseInterpolatorNode '
        cmd1+= '%s_poseInterpolator 0 ' %name
        cmd1+= '%s ' %twist
        intpNd = mm.eval( cmd1 )
        mc.setAttr( '%s.allowNegativeWeights' %intpNd , 0 )
        
        return intpNd
        
        '''
        poseName = intpNd.split('_poseInterpolator')[0]
        cmd2 = 'poseInterpolatorAddShapePose( "%s" , "%s_0_0_0" , "swing" , {"%s"} , 0 )' %( intpNd , poseName , bsn )
        poseNd = mm.eval( cmd2 )
        '''

    def mirror_intpNode( self , intpNd , bshNd , geo , search , replace ):
        tmpAry = intpNd.split( search )
        mirrorName = replace.join( tmpAry[0:2] )
        intpNd = self.create_intpNode( mirrorName , bshNd , twist = '1' )
        jntName = intpNd.split('_poseInterpolator')[0]
        pm.textScrollList( 'poseIntpListsTS' , e = True , a = jntName )
        pm.textScrollList( 'poseIntpListsTS' , e = True , da = True )
        pm.textScrollList( 'poseIntpListsTS' , e = True , si = jntName )
        self.reload_pose_shape()
        self.add_pose_last_value( intpNd , geo , bshNd )
        
        info = self.get_info()
        poseShpAll = info['poseShpAll']
        oriGrp = info['geo']
        
        step = (100/len(poseShpAll))
        percen = step
        
        if poseShpAll :
            for poseShape in poseShpAll :
                pm.textScrollList( 'poseShapeListsTS' , e = True , si = poseShape )
                absMirrGrp = self.mirror_abs_shape( poseShape , search , replace )
                
                inbtw = self.get_value_inbetween()
            
                blends = pm.PyNode( bshNd )
                bshList = blends.listAliases()
                
                for bsh in bshList :
                    if inbtw['base'] in bsh[0]:
                        indexBsnNd = int(bsh[1].split('[')[-1].split(']')[0])

                self.go_to_pose_intp( poseShape )

                if not inbtw['value'] == 1 :
                    val = mc.getAttr( '%s.%s' %(bshNd,inbtw['base']))
                    inbtw['value'] = float( "%.3f" %val ) 

                self.add_inbetween_shape( bshNd , oriGrp , indexBsnNd , absMirrGrp[0] , inbtw['value'] )
                mc.delete( absMirrGrp )
                
                if poseShape == poseShpAll[-1]:
                    pm.progressBar( 'progressControl' , edit = True , pr = 100 )
                else :
                    pm.progressBar( 'progressControl' , edit = True , pr = percen )
                    
                percen+=step
        
        return intpNd

    def mirror_abs_shape( self , poseShape , search , replace ):
        info = self.get_info()
        bshNd = info['bshNd']
        geo = info['geo']
        
        tmpAry = poseShape.split( '_' )
        joint = '_'.join( tmpAry[0:-3] ) 
        mirrorJnt = search.join( joint.split( replace )[0:2] )
        
        rot = [ ]
        
        for ro in ( -3 , -2 , -1 ) :
            if not 'n' in tmpAry[ro] :
                rot.append( int(tmpAry[ro])) 
            else :
                val = tmpAry[ro].split('n')[-1]
                rot.append( int('-' + val))
            
        if bshNd :
            if geo : 
                sknList = []
                listObj = mc.listRelatives( geo , f = True , ad = True )
                for chd in listObj :
                    mc.select(chd)
                    skn = mm.eval('findRelatedSkinCluster("%s")' %chd )
                    if skn :
                        sknList.append(skn)
                
                for skn in sknList :
                    mc.setAttr( '%s.envelope' %skn , 0 )
                
                pm.currentTime(0)
                
                mc.setAttr( '%s.rx' %mirrorJnt , rot[0] )
                mc.setAttr( '%s.ry' %mirrorJnt , rot[1] )
                mc.setAttr( '%s.rz' %mirrorJnt , rot[2] )
                
                absGrp = mc.duplicate( geo , n = 'AbsPose' , rc = True )
                
                for skn in sknList :
                    mc.setAttr( '%s.envelope' %skn , 1 )
                
                absMirrGrp = self.duplicate_default_pose( '%s' %poseShape )
                
                # self.mirror_shape( absGrp , absMirrGrp )
                
                bsnNd = mc.blendShape( absGrp[0] , absMirrGrp[0] , n = 'FlipBsh' )[0]
                mc.setAttr( '%s.%s' %(bsnNd,absGrp[0]) , 1 )
                
                for i in range(2) :
                    #doBlendShapeFlipTarget( int $axis, int $storeSymmetryEdge, string $inputBSTargets[] )
                    mm.eval( 'doBlendShapeFlipTarget 1 0 {"FlipBsh.0"}' )
                    
                mc.delete( absMirrGrp , ch = 1 )
                mc.delete( absGrp )
                
                return absMirrGrp

    def flip_shape( self , *args ):
        sel = mc.ls( sl = True )

        bsnNd = mc.blendShape( sel[0] , sel[1] , n = 'FlipBsh' )[0]
        mc.setAttr( '%s.%s' %(bsnNd,sel[0]) , 1 )
        
        for i in range(2) :
            #doBlendShapeFlipTarget( int $axis, int $storeSymmetryEdge, string $inputBSTargets[] )
            mm.eval( 'doBlendShapeFlipTarget 1 0 {"FlipBsh.0"}' )

        mc.delete( sel[1] , ch = 1 )
        mc.delete( sel[0] )
        
    def mirror_intpNode_button( self , *args ):
        info = self.get_info()
        poseIntpSel = info['poseIntpSel']
        bshNd = info['bshNd']
        geo = info['geo']

        for poseIntp in poseIntpSel :
            intpNdMirr = self.mirror_intpNode( poseIntp , bshNd , geo , '_L_' , '_R_' )
            if intpNdMirr :
                if mc.objExists( 'PoseIntp_Grp' ):
                    mc.parent( intpNdMirr , 'PoseIntp_Grp' )
        
        self.reload_pose_intp()
        pm.currentTime(0)
        mc.confirmDialog( title = 'Progress' , message = 'Mirror Interpolator Node is done.' )

    def create_intpNode_button( self , *args ):
        pm.currentTime(0)
        sels = pm.ls( sl = True )

        info = self.get_info()
        geo = info['geo']
        bshNd = info['bshNd']

        if not mc.objExists( 'PoseIntp_Grp' ):
            intpGrp = mc.createNode( 'transform' , n = 'PoseIntp_Grp' )
        else :
            intpGrp = 'PoseIntp_Grp'
        
        if geo :
            step = (100/len(sels))
            percen = step
            
            for sel in sels :
                if sel == sels[-1]:
                    pm.progressBar( 'progressControl' , edit = True , pr = 100 )
                else :
                    pm.progressBar( 'progressControl' , edit = True , pr = percen )
                
                percen+=step

                if pm.nodeType( sel ) == 'joint' :
                    intpNd = self.create_intpNode( '%s' %sel , bshNd , '1' )
                    jntName = intpNd.split('_poseInterpolator')[0]
                    pm.textScrollList( 'poseIntpListsTS' , e = True , a = jntName )
                    pm.textScrollList( 'poseIntpListsTS' , e = True , da = True )
                    pm.textScrollList( 'poseIntpListsTS' , e = True , si = jntName )
                    self.reload_pose_shape()
                    self.add_pose_last_value( intpNd , geo , bshNd )
                    mc.parent( intpNd , intpGrp )
            
            mc.confirmDialog( title = 'Progress' , message = 'Create Interpolator Node is done.' )
                
        else :
            mc.confirmDialog( title = 'Warning' , message = 'Geometry Group not found. Plese add Geometry Group.' )
        
        pm.currentTime(0)
        self.list_interpolator_node()
        self.reload_pose_shape()
        
    def delete_intpNode_button( self , *args ):
        info = self.get_info()
        poseIntpSel = info['poseIntpSel']

        if poseIntpSel :
            for poseIntp in poseIntpSel :
                mm.eval('deletePoseInterpolatorNode("%s_poseInterpolator");' %poseIntp)

            self.reload_script()
    
    def get_blendShape_node( self , *args ):
        return mc.ls( type = 'blendShape' )

    def make_menuItem_blendShape( self , bsh ):
        def change_text_blendShape( *args ):
            pm.textField( 'blendshapeTF' , e = True , tx = bsh )
            pm.select( bsh )
            self.reload_pose_shape()
        
        pm.menuItem( l = bsh , p = 'blendshapeMenu' , c = change_text_blendShape )
    
    def add_item_blendShape( self , *args ):
        bshList = self.get_blendShape_node()
        
        try : 
            pm.deleteUI( 'blendshapeMenu' ) 
        except : 
            pass
           
        if bshList :
            pm.popupMenu( 'blendshapeMenu' , parent = 'blendshapeTF' , ctl = False , button = 3 )
            for bsh in bshList :
                self.make_menuItem_blendShape( bsh )
            
            if 'Corrective_Bsh' in bshList :
                pm.textField( 'blendshapeTF' , e = True , tx = 'Corrective_Bsh' )
    
    def add_item_blendShape_button( self , *args ):
        geo = pm.textField( 'geometryTF' , q = True , tx = True )
        if geo :
            bshNd = mm.eval('blendShape  -name "Corrective_Bsh" -frontOfChain %s;' %geo)[0]
            pm.textField( 'blendshapeTF' , e = True , tx = bshNd )
            self.add_item_blendShape()
        else :
            mc.confirmDialog( title = 'Warning' , message = 'Geometry Group not found. Plese add Geometry Group.' )
    
    def add_item_geometry_button( self , *args ):
        sel = pm.ls( sl = True )[0]
        pm.textField( 'geometryTF' , e = True , tx = sel )
            
    def duplicate_reset_pose_button( self , *args ):
        info = self.get_info()
        poseShpSel = info['poseShpSel'][0]
        self.go_to_pose_intp( poseShpSel )
        editGrp = self.duplicate_edit_pose( 0 )
        mc.select( editGrp )
            
    def duplicate_50_pose_button( self , *args ):
        info = self.get_info()
        poseShpSel = info['poseShpSel'][0]
        self.go_to_pose_intp( poseShpSel )
        editGrp = self.duplicate_edit_pose( 0.5 )
        mc.select( editGrp )
        
    def duplicate_edit_pose( self , value ):
        info = self.get_info()
        poseShpSel = info['poseShpSel'][0]
        bshNd = info['bshNd']
        geo = info['geo']
        
        if bshNd :
            if geo :
                mc.setAttr( '%s.envelope' %bshNd , value )
                editPose = mc.duplicate( geo , n = '%s_EditPose' %poseShpSel , rc = True )[0]
                par = pm.listRelatives( editPose , p = True )
                
                if par :
                    mc.parent( editPose , w = True )
                    
                mc.setAttr( '%s.envelope' %bshNd , 1 )
                
                return  editPose
    
    def duplicate_default_pose( self , poseShp = '' ):
        info = self.get_info()
        bshNd = info['bshNd']
        geo = info['geo']

        if poseShp :
            poseShpSel = poseShp
        else :
            poseShpSel = info['poseShpSel'][0]
        
        if bshNd :
            if geo : 
                sknList = []
                listObj = mc.listRelatives( geo , f = True , ad = True )
                for chd in listObj :
                    mc.select(chd)
                    skn = mm.eval('findRelatedSkinCluster("%s")' %chd )
                    if skn :
                        sknList.append(skn)
                
                for skn in sknList :
                    mc.setAttr( '%s.envelope' %skn , 0 )
                
                mc.setAttr( '%s.envelope' %bshNd , 0 )
                defGrp = mc.duplicate( geo , n = '%s_AbsPose' %poseShpSel , rc = True )
                    
                par = pm.listRelatives( defGrp[0] , p = True )
                if par :
                    mc.parent( defGrp[0] , w = True )
                
                for skn in sknList :
                    mc.setAttr( '%s.envelope' %skn , 1 )
                
                mc.setAttr( '%s.envelope' %bshNd , 1 )
                mc.setAttr( '%s.v' %defGrp[0] , 0 )
                mc.select( cl = True )

                return defGrp
    
    def add_inbetween_shape( self , bshNd , geoGrp , indexBsnNd , absGrp , value ):
        # indexBsnNd start = 0
        # mc.blendShape( 'Corrective_Bsh' , edit = True , ib = True , t = ( 'AllGeo_Grp' , 0 , 'AllGeo_Grp1' , 1.00 ))
        mc.blendShape( bshNd , edit = True , ib = True , t = ( geoGrp , indexBsnNd , absGrp , value ))
    
    def remove_inbetween_shape( self , bshNd , indexBsnNd , value ):
        # indexBsnNd start = 0
        # value = 5500( 0.50 ) , 5750( 0.75 ) , 6000 ( 1.00 )
        mm.eval( "blendShapeDeleteInBetweenTarget %s %s %s" %( bshNd , indexBsnNd , value ))
    

    def getBshInbtweens( self , blendShapeNode , index = 0 , geoIndex = 0 ):
        if isinstance(blendShapeNode, (str, unicode)):
            blendShapeNode = pm.PyNode(blendShapeNode)
        inputTargetGrp = blendShapeNode.inputTarget[geoIndex].inputTargetGroup[index]
        arrayNums = inputTargetGrp.inputTargetItem.getArrayIndices()
        inb_weights = []
        for n in arrayNums:
            inb_weight = (n-5000.0)/1000
            inb_weights.append(inb_weight)

        return inb_weights 
        
    def clear_shape( self , *args ):
        info = self.get_info()
        bshNd = info['bshNd']
        poseIntpSel = info['poseIntpSel']
        poseShpSel = info['poseShpSel'][0]
        
        blends = pm.PyNode( bshNd )
        bshList = blends.listAliases()
        inbtw = self.get_value_inbetween()
        
        for bsh in bshList :
            if inbtw['base'] in bsh[0]:
                indexBsnNd = bsh[1].split('[')[-1].split(']')[0]
                
                if indexBsnNd :
                    self.go_to_pose_intp( poseShpSel )
                    if not inbtw['value'] == 1 :
                        val = mc.getAttr( '%s.%s' %(bshNd,inbtw['base']))
                        inbtw['value'] = float( "%.3f" %val ) 

                    self.remove_inbetween_shape( bshNd , int(indexBsnNd) , ((inbtw['value']*1000)+5000) )
                    
    def absolute_shape( self , oriGrp , editGrp , absGrp ):
        info = self.get_info()
        bshNd = info['bshNd']

        # oriGrp = mc.duplicate( oriGrp , n = 'OriClean_Grp'  , rc = True )[0]
        listOri = mc.listRelatives( oriGrp , f = True , ad = True , type = 'transform' )
        listEdit = mc.listRelatives( editGrp , f = True , ad = True , type = 'transform' )
        listDef = mc.listRelatives( absGrp , f = True , ad = True , type = 'transform' )
        num = len(listOri)
        
        if listEdit :
            if bshNd :
                mc.setAttr( '%s.envelope' %bshNd , 0 )
                
                for i in range(num) :
                    if mc.listRelatives( listOri[i] , s = True ) :
                        mc.select( listOri[i] , listEdit[i] ) # Maya Cmd
                        absMesh = mm.eval('invertShape;') 

                        # absMesh = invertDeformation.invert( listOri[i] , listEdit[i] ) # Nu Tools

                        # mc.select( listEdit[i] , listOri[i] ) #  BSpirit
                        # mm.eval( "BSpiritCorrectiveShape;" )
                        # absMesh = mc.ls( sl = True )
                        
                        if absMesh :
                            if not mc.nodeType( absMesh ) == 'mesh' :
                                mc.select( absMesh , listDef[i] )
                                mm.eval( 'copyShape ;' )
                                mc.delete( absMesh )
                            
                mc.setAttr( '%s.envelope' %bshNd , 1 )
                
        # mc.delete( oriGrp )
        mc.select( cl = True )
            
    def add_shape_button( self , *args ):
        info = self.get_info()
        poseShpSel = info['poseShpSel'][0]

        oriGrp = pm.textField( 'geometryTF' , q = True , tx = True )
        editGrp = mc.ls( sl = True )[0]
        absGrp = self.duplicate_default_pose()
        bshNd = pm.textField( 'blendshapeTF' , q = True , tx = True )
        inbtw = self.get_value_inbetween()

        blends = pm.PyNode('Corrective_Bsh')
        bshList = blends.listAliases()
        
        for bsh in bshList :
            if inbtw['base'] in bsh[0]:
                indexBsnNd = int(bsh[1].split('[')[-1].split(']')[0])

        self.go_to_pose_intp( poseShpSel )

        if not inbtw['value'] == 1 :
            val = mc.getAttr( '%s.%s' %(bshNd,inbtw['base']))
            inbtw['value'] = float( "%.3f" %val ) 

        self.clear_shape()
        self.absolute_shape( oriGrp , editGrp , absGrp )
        self.add_inbetween_shape( bshNd , oriGrp , indexBsnNd , absGrp[0] , inbtw['value'] )
        
        mc.delete( editGrp , absGrp[0] )
        
    def edit_shape_button( self , *args ) :
        info = self.get_info()
        poseShpSel = info['poseShpSel'][0]

        self.go_to_pose_intp( poseShpSel )
        editGrp = self.duplicate_edit_pose( 1 )
        mc.select( editGrp )
        
    def get_value_inbetween( self , *args ):
        poseSel = pm.textScrollList( 'poseShapeListsTS' , q = True , si = True )[0]
        at = { 'rx' : -3 , 'ry' : -2  , 'rz' : -1 }
        separate = poseSel.split('_')
        
        for attr in ( 'rx' , 'ry' , 'rz' ) :
            if not separate[at[attr]] == '0' :
                value = self.get_minMax_keyframe_from_poseShape( attr )
                if not 'n' in separate[at[attr]] :
                    listPose = value['val>']
                else :
                    listPose = value['val<']
                    
                index = listPose.index(poseSel)
                div = len(listPose)
                val = (1.0/div)*(index+1)
                
                value['value'] = float( "%.3f" %val ) 
                value['base'] = listPose[-1]
                
                return value
                
    def copy_shape( self , src , tgt ):
        srcGrp = mc.listRelatives( src , f = True , ad = True , type = 'transform' )
        tgtGrp = mc.listRelatives( tgt , f = True , ad = True , type = 'transform' )
        
        if srcGrp :
            for i in range(len(srcGrp)) :
                if not mc.listRelatives( srcGrp[i] , s = True ) : 
                    continue
                
                vis = mc.getAttr( '%s.v' %srcGrp[i] )
                
                if not vis == 0 :
                    mc.select( srcGrp[i] , tgtGrp[i] )
                    mm.eval( 'copyShape ;' )  
        else :
            mm.eval( 'copyShape ;' )
                
    def mirror_shape( self , src , tgt ):
        srcGrp = mc.listRelatives( src , f = True , ad = True , type = 'transform' )
        tgtGrp = mc.listRelatives( tgt , f = True , ad = True , type = 'transform' )
        
        if srcGrp :
            for i in range(len(srcGrp)) :
                if not mc.listRelatives( srcGrp[i] , s = True ) : 
                    continue
                
                vis = mc.getAttr( '%s.v' %srcGrp[i] )
                
                if not vis == 0 :
                    mc.select( srcGrp[i] , tgtGrp[i] )
                    mm.eval( 'mirrorShape ( "YZ" , "0" , "0" , "1" , "0.001" ) ;' )
        else :
            mm.eval( 'mirrorShape ( "YZ" , "0" , "0" , "1" , "0.001" ) ;' )

    def limit_on_off( self , ctrl ) :
        info = self.get_info()
        bshNd = info['bshNd']
        poseShpAll = info['poseShpAll']
        shapes = self.get_shape_pose( bshNd )
        ctrl = mc.ls( sl = True )[0]
        nameList = []

        for elem in shapes :
            name = elem.split('_Jnt')[0]

            if not name in nameList :
                nameList.append(name)

        for name in nameList :
            mc.addAttr( ctrl , ln = name , at = 'float' , k = True , min = 0 , max = 1 , dv = 1 )

            for each in shapes :
                if name in each :
                    rootConect = mc.listConnections( '%s.%s' %(bshNd,each) , p = True )[0]
                    mc.disconnectAttr( rootConect , '%s.%s' %(bshNd,each))
                    
                    mdl = mc.createNode( 'multDoubleLinear' , n = '%s_Mdl' %name )
                    mc.connectAttr( '%s.%s' %(ctrl,name) , '%s.i1' %mdl )
                    mc.connectAttr( rootConect , '%s.i2' %mdl )
                    mc.connectAttr( '%s.o' %mdl , '%s.%s' %(bshNd,each) )

    def copy_shape_button( self , *args ):
        sel = mc.ls( sl = True )
        self.copy_shape( sel[0] , sel[1] )

    def mirror_shape_button( self , *args ):
        sel = mc.ls( sl = True )
        self.mirror_shape( sel[0] , sel[1] )
        
    def reload_script( self , *args ):
        self.reload_pose_intp()
        self.reload_pose_shape()


def get_data_fld( *args ):
    scnPath = os.path.normpath( mc.file( q = True , l = True )[0])

    tmpAry = scnPath.split( '\\' )
    tmpAry[3] = 'publ'
    tmpAry[-3] = 'data\\volComp'

    dataFld = '\\'.join( tmpAry[0:-2] )

    if not os.path.isdir( dataFld ) :
        os.mkdir( dataFld )
    
    return dataFld

def write_selected_hierarchy( *args ):
    suffix = 'Skel'
    sel = mc.ls(sl=True)[0]
    flNm = sel

    if ':' in sel:
        flNm = sel.split(':')[-1]

    dataFld = get_data_fld()
    flNmExt = '%s%s.dat' % ( flNm , suffix )
    
    flPath = '%s\\%s' % ( dataFld , flNmExt )
    write_hierarchy( root = sel , flPath = flPath )
    #pm.progressBar( 'progressControl' , edit = True , pr = 100 )
    #mc.confirmDialog( title = 'Progress' , message = 'Exporting hierarchy is done.' )
    print flPath

def write_hierarchy( root = 'Skin_Grp' , flPath = '' ):
    skelDict = {}
    infTypes = ('joint', 'transform')
        
    if ':' in root :
        sp = ':'
    else :
        sp = '|'

    rootPos = mc.xform(root, q=True, t=True)
    rootOri = mc.xform(root, q=True, ro=True)

    rootInfo = {}
    rootInfo['name'] = root.split(sp)[-1]
    rootInfo['type'] = mc.nodeType(root)
    rootInfo['rotateOrder'] = mc.getAttr('%s.rotateOrder' %root)
    rootInfo['pos'] = rootPos
    rootInfo['ori'] = rootOri

    skelDict[0] = rootInfo
    
    listObj = mc.listRelatives(root, ad=True, f=True)
    
    for ix, chd in enumerate(sorted(listObj)):
        
        type_ = mc.nodeType(chd)
        
        if not type_ in infTypes: continue
        
        if ':' in chd :
            sp = ':'
        else :
            sp = '|'

        infoDict = {}
        infoDict['name'] = chd.split(sp)[-1]
        infoDict['type'] = type_
        infoDict['rotateOrder'] = mc.getAttr('%s.rotateOrder' %chd)
        infoDict['pos'] = mc.xform(chd, q=True, t=True, ws=True)
        infoDict['ori'] = mc.xform(chd, q=True, ro=True, ws=True)
        infoDict['parent'] = mc.listRelatives(chd, p=True, f=True)[0].split(sp)[-1]
        
        if type_ == 'joint' :
            infoDict['radius'] = mc.getAttr('%s.radius' %chd)
        
        skelDict[ix+1] = infoDict
        
        # Percent calculation
        vtxNo = len(listObj)
        # if ix == ( vtxNo - 1 ):
        #     pm.progressBar( 'progressControl' , edit = True , pr = 100 )
        # else:
        #     prePrcnt = 0
        #     if ix > 0:
        #         prePrcnt = int((float(ix - 1) / vtxNo) * 100.00)
            
        #     prcnt = int((float(ix) / vtxNo) * 100.00)

            # if not prcnt == prePrcnt:
            #     pm.progressBar( 'progressControl' , edit = True , pr = prcnt )

    flObj = open(flPath, 'w')
    pickle.dump(skelDict, flObj)
    flObj.close()

def read_hierarchy( flPath = '' ):
    print 'Loading %s' % flPath
    flObj = open(flPath, 'r')
    skelDict = pickle.load(flObj)
    flObj.close()

    for key in sorted(skelDict.keys()):
        
        obj = mc.createNode(skelDict[key]['type'], n=skelDict[key]['name'])
        mc.setAttr('%s.rotateOrder'%obj, skelDict[key]['rotateOrder'])
        mc.xform(obj, t=skelDict[key]['pos'], ws=True)
        mc.xform(obj, ro=skelDict[key]['ori'], ws=True)
        
        if key == 0:
            continue
        
        if skelDict[key]['type'] == 'joint':
            mc.makeIdentity(obj, a=True, r=True, s=True)
            mc.setAttr('%s.radius'%obj, skelDict[key]['radius'])
        
        mc.parent(obj, skelDict[key]['parent'])
        
    # Find parents for the ribbon joints.
    for obj in mc.listRelatives(skelDict[0]['name']):
        
        if 'Spine' in obj :
            mc.parent( obj , 'Root_Jnt' )
            spine = mc.listRelatives( obj ) 
            for i in range( len ( spine )):
                if not  i == 0 :
                    mc.parent( spine[i] , spine[i-1] )
        else :
            exp = r'(.+)RbnSkin(.+)Grp'
            reObj = re.match( exp , obj )
    
            if not reObj:
                continue
    
            currPar = '%s%sJnt' % (reObj.group(1), reObj.group(2))
            if mc.objExists(currPar):
                mc.parent(obj, currPar)
    
    mc.select( skelDict[0]['name'] )
            
def build_skel_group( *args ):
    dataFld = get_data_fld()  
    flPath = os.path.join( dataFld , 'Skin_GrpSkel.dat' )
    read_hierarchy( flPath = flPath )
    
def write_weight( geo = '' , flPath = '' ):
    shapes = mc.listRelatives( '%s' %geo , shapes = True )

    if shapes :
        skn = mm.eval('findRelatedSkinCluster("%s")' % geo)

        if skn:
            sknMeth = mc.getAttr('%s.skinningMethod' % skn)
            infs = mc.skinCluster(skn, q = True, inf = True)
            sknSet = mc.listConnections('%s.message' % skn, d=True, s=False)[0]
            
            flObj = open(flPath, 'w')
            wDct = {}

            wDct['influences'] = infs
            wDct['name'] = skn
            wDct['set'] = sknSet
            wDct['skinningMethod'] = sknMeth

            vtxNo = mc.polyEvaluate(geo, v = True)
            
            # for ix in xrange(vtxNo):
            #     currVtx = '%s.vtx[%d]' % (geo, ix)
            #     skinVal = mc.skinPercent(skn, currVtx, q = True, v = True)
            #     wDct[ix] = skinVal
                
            #     # Percent calculation
            #     if ix == (vtxNo - 1):
            #         pm.progressBar( 'progressControl' , edit = True , pr = 100 )
            #     else:
            #         prePrcnt = 0
            #         if ix > 0:
            #             prePrcnt = int((float(ix - 1) / vtxNo) * 100.00)
                    
            #         prcnt = int((float(ix) / vtxNo) * 100.00)
        
            #         if not prcnt == prePrcnt:
            #             pm.progressBar( 'progressControl' , edit = True , pr = prcnt )

            pickle.dump(wDct, flObj)
            flObj.close()
        else:
            print '%s has no related skinCluster node.' % geo
            
def write_selected_weight( *args ):
    suffix = 'Weight'
    for sel in mc.ls(sl=True):
        fld = get_data_fld()
        dataFld = os.path.join( fld , 'skinWeight' )
    
        if not os.path.isdir( dataFld ) :
            os.mkdir( dataFld )
        
        flNmExt = '%s%s.dat' % (sel.split(':')[-1], suffix)
        flPath = os.path.join(dataFld, flNmExt)
        write_weight(sel, flPath)

    pm.progressBar( 'progressControl' , edit = True , pr = 100 )
    mc.confirmDialog( title = 'Progress' , message = 'Exporting weight is done.' )
    print '\n%s' %dataFld

def read_weight( geo = '' , flPath = '' , searchFor = '' , replaceWith = '' , prefix = '' , suffix = '' ):
    print 'Loading %s' % flPath
    flObj = open(flPath, 'r')
    wDct = pickle.load(flObj)
    flObj.close()
    
    # infs = wDct['influences']
    infs = []
    
    for oInf in wDct['influences']:
        if ':' in oInf :
            nm = oInf.split(':')[0]
            searchFor = '%s:' %nm
        if searchFor:
            oInf = oInf.replace(searchFor, replaceWith)

        currInf = '%s%s%s' % (prefix, oInf, suffix)

        infs.append(currInf)
    
    for inf in infs:
        if not mc.objExists(inf):
            print 'Cannot find %s ' % inf

    oSkn = mm.eval('findRelatedSkinCluster "%s"' % geo)
    if oSkn:
        mc.skinCluster(oSkn, e = True, ub = True)

    tmpSkn = mc.skinCluster(infs, geo, tsb=True)[0]
    skn = mc.rename(tmpSkn, wDct['name'])
    mc.setAttr('%s.skinningMethod' % skn, wDct['skinningMethod'])

    sknSet = mc.listConnections('%s.message' % skn, d=True, s=False)[0]
    mc.rename(sknSet, wDct['set'])
    
    for inf in infs:
        mc.setAttr('%s.liw' % inf, False)
    
    mc.setAttr('%s.normalizeWeights' % skn, False)
    mc.skinPercent(skn, geo, nrm=False, prw=100)
    mc.setAttr('%s.normalizeWeights' % skn, True)
    
    vtxNo = mc.polyEvaluate(geo, v = True)
    
    # for ix in xrange(vtxNo):        
    #     for iy in xrange(len(infs)):
    #         wVal = wDct[ix][iy]
    #         if wVal:
    #             wlAttr = '%s.weightList[%s].weights[%s]' % (skn, ix, iy)
    #             mc.setAttr(wlAttr, wVal)
        
    #     # Percent calculation
    #     if ix == (vtxNo - 1):
    #         pm.progressBar( 'progressControl' , edit = True , pr = 100 )
    #     else:
    #         prePrcnt = 0
    #         if ix > 0:
    #             prePrcnt = int((float(ix - 1) / vtxNo) * 100.00)
            
    #         prcnt = int((float(ix) / vtxNo) * 100.00)

    #         if not prcnt == prePrcnt:
    #             pm.progressBar( 'progressControl' , edit = True , pr = prcnt )

def read_selected_weight( searchFor ='' , replaceWith ='' , prefix ='' , suffix ='' ):
    sels =  mc.ls(sl=True)
    for sel in sels:
        dataFld = get_data_fld()
        flNm = '%sWeight.dat' % sel
        
        try:
            print 'Importing %s.' % sel

            read_weight(sel, os.path.join(dataFld , 'skinWeight' , flNm), searchFor, replaceWith, prefix, suffix)
            print 'Importing %s done.' % flNm
        except:
            print 'Cannot find weight file for %s' % sel
    
    mc.select(sels)
    pm.progressBar( 'progressControl' , edit = True , pr = 100 )
    mc.confirmDialog( title = 'Progress' , message = 'Importing weight is done.')

def add_key( obj = '' , attr = '' , value = 0 , frame = 0 ):
    mc.setKeyframe( obj , v = value , at = attr , t = frame )

def setup_key_anim( jnt = '' ,
                    angleList = { } ,
                    startFrame = 0 ,
                    step = 5 ) :
    
    animNd = mc.listConnections( jnt , t = 'animCurve' )
    if animNd :
        mc.delete(animNd)
        print jnt
        
    for attr in angleList.keys() :
        for angle in angleList[attr] :
            add_key( jnt , attr , angle , startFrame )
            startFrame+=step
        startFrame-=step
    
    return startFrame

def import_key_anim( *args ):
    fld = get_data_fld()
    sys.path.insert(0, fld)
    
    import corrective_keys_anim
    reload(corrective_keys_anim)
    corrective_keys_anim.setUpAllKeyAnim()
    pm.progressBar( 'progressControl' , edit = True , pr = 100 )
    mc.confirmDialog( title = 'Progress' , message = 'Importing Keys is done.')
    
def edit_key_anim( *args ):
    subprocess.Popen( [ textEditor , get_data_fld() + "\\corrective_keys_anim.py"] )
    
def reconnect_poseIntp_node( *args ) :
    suffix = 'poseInterpolator'
    poseIntp = mc.ls( type = 'poseInterpolator' )
    for each in poseIntp :
        namespace , name = each.split( ':' )
        jntName = name.split( '_%s' %suffix )[0]
        jntName = mc.ls( '*:%s' %jntName )[0]
        
        if not mc.listConnections( '%s.driver[0].driverMatrix' %each ) :
            mc.connectAttr( '%s.matrix' %jntName , '%s.driver[0].driverMatrix' %each )
            
        if not mc.listConnections( '%s.driver[0].driverOrient' %each ) :
            mc.connectAttr( '%s.jointOrient' %jntName , '%s.driver[0].driverOrient' %each )
            
        if not mc.listConnections( '%s.driver[0].driverRotateAxis' %each ) :
            mc.connectAttr( '%s.rotateAxis' %jntName , '%s.driver[0].driverRotateAxis' %each )
            
        if not mc.listConnections( '%s.driver[0].driverRotateOrder' %each ) :
            mc.connectAttr( '%s.rotateOrder' %jntName , '%s.driver[0].driverRotateOrder' %each )
    
    mc.confirmDialog( title = 'Progress' , message = 'Reconnect PoseInterpolator Node is done.')

def create_group_template( *args ) :
    if not mc.objExists( 'Delete_Grp' ) :
        deleteGrp = mc.createNode( 'transform' , n = 'Delete_Grp' )
    if not mc.objExists( 'VolCompRigStill_Grp' ) :
        volStillGrp = mc.createNode( 'transform' , n = 'VolCompRigStill_Grp' )

def rename_geo( *args ) :
    sel = mc.ls( sl = True )[0]
    listChd = mc.listRelatives( sel , ad = True , type = 'transform' )
    
    for chd in listChd :
        tmp = chd.split('_')
        tmp[0] = tmp[0]+'VolComp'
        newName = '_'.join( tmp )
        obj = mc.rename( chd , newName )
    
    if not sel == 'VolCompRigGeo_Grp' :
        volGrp = mc.rename( sel , 'VolCompRigGeo_Grp' )
        
def run():
    pintp = PoseInterpolatorUI()
    pintp.add_item_blendShape()
    pintp.reload_pose_intp()
    pintp.reload_pose_shape()