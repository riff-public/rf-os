import maya.cmds as mc
import maya.mel as mel

def getRefNodes(objs):
    """
    Returns the names of the referenceNodes related to the given objs.
    Returns an empty list if no related referenceNodes.
    """
    res = []
    
    if not isinstance(objs, (list, tuple)):
        objs = [objs]
    
    for obj in objs:
        try:
            res.append(mc.referenceQuery(obj, rfn=True))
        except:
            continue
    return res

def selectInReferenceEditor():
    """
    Selects the first reference in the referenceEditor related to the current selection.
    
    This has not been production tested.  
    
    Note: Since the sceneEditor command doesn't seem to support programmatically selecting
    multiple items this will only select a single reference. UuuuuurgH!
    """ 
    # Get the global name of the reference editor panel from mel
    gReferenceEditorPanel = mel.eval("$gReferenceEditorPanel=$gReferenceEditorPanel;")
    
    # Get all referenced file names
    allReferencedFiles = mc.file(q=1, r=True)
    
    # Get referenceNodes in order of the referenced files
    allRefNodes = [mc.referenceQuery(path, rfn=True) for path in allReferencedFiles]
    
    # Get the referenceNodes belonging to selection and update the referenceEditor
    sel = mc.ls(sl=1)
    refNodes = getRefNodes(sel)
    if refNodes:
        for ref in refNodes:
            index = allRefNodes.index(ref)
            mc.sceneEditor(gReferenceEditorPanel, e=1, selectItem=index)

#selectInReferenceEditor() 