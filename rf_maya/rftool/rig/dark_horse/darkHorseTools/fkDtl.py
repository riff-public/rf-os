import os
import sys
sys.path.append( r'O:\Pipeline\legacy\lib\local\cherTools' )
from puccaTools import core
reload(core)
from puccaTools import rigTools as rt
reload(rt)
import maya.cmds as mc
import pymel.core as pm

import maya.cmds as mc 
def fkDtl(shape ='cube',col='green'):
    jnt = mc.ls(sl=True)
    for i in range (len(jnt)) :
    
        sideEach = core.Dag(jnt[i])
        side = sideEach.getSide()
        name  = jnt[i].split('_')[0]
        
        
        zGrp = mc.createNode ('transform')
        zGrp = mc.rename(zGrp,'%sDtlCtrl%sGrp'%(name,side))
        crv = rt.createCtrl( '%s%sCtrl' %( name ,side ) , shape, col, jnt = False )
        crvGmb = rt.addGimbal( crv )
        ctrlGrp = rt.addGrp(crv)
        
        
        snpCtrl = ctrlGrp.snap(jnt[i])
        
        
        mc.delete(mc.parentConstraint(jnt[i], zGrp ,mo = False)) 
        mc.parent(ctrlGrp,zGrp)
      
              
        #parentConstraint
        mc.parentConstraint(crvGmb,jnt[i],mo=True)
        mc.scaleConstraint(crv,jnt[i],mo=True)
        
        
        mc.setAttr('%s.v'%crvGmb,k=False)
        mc.setAttr('%s.sx'%crvGmb,l = True,k=False)
        mc.setAttr('%s.sy'%crvGmb,l = True,k=False)
        mc.setAttr('%s.sz'%crvGmb,l = True,k=False)