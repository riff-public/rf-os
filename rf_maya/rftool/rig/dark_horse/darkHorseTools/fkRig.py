import os
import sys
sys.path.append( r'O:\Pipeline\legacy\lib\local\cherTools' )
from puccaTools import core
reload(core)
from puccaTools import rigTools as rt
reload(rt)
import maya.cmds as mc
import pymel.core as pm

######################################3
def fkRig(shape='circle',col='blue'):
	jnt = mc.ls(sl=True)
	grp=[]
	ctrl=[]
	gmb = []

	for i in range(len(jnt)) :
	    sideEach = core.Dag(jnt[i])
	    side = sideEach.getSide()
	    name  = jnt[i].split('_')[0]
	    
	    zGrp = mc.createNode ('transform')
	    zGrp = mc.rename(zGrp,'%sCtrl%sGrp'%(name,side))
	    #frzGrp = mc.duplicate(zGrp)
	    #frzGrp = mc.rename(frzGrp,'%sFreez%sGrp'%(name,side))
	    crv = rt.createCtrl( '%s%sCtrl' %( name ,side ) , shape, col, jnt = False )
	    crvGmb = rt.addGimbal( crv )
	    ctrlGrp = rt.addGrp(crv)
	    
	    mc.delete(mc.parentConstraint(jnt[i], zGrp ,mo = False)) 
	    
	   
	    snpCtrl = ctrlGrp.snap(jnt[i])
	    mc.parent(ctrlGrp,zGrp)

	    mc.parentConstraint(crvGmb,jnt[i],mo=True)
	    #mc.scaleConstraint(crv,jnt[i],mo=True)
	    
	    grp.append(zGrp)
	    gmb.append(crvGmb)
	    ctrl.append(crv)
	    size = len(grp)
	    
	    mc.setAttr('%s.segmentScaleCompensate'%jnt[i])
	    mc.setAttr('%s.scaleX'%crv,l=True ,k = False)
	    mc.setAttr('%s.scaleY'%crv,l=True ,k = False)
	    mc.setAttr('%s.scaleZ'%crv,l=True ,k = False)
	    mc.setAttr('%s.v'%crv,l=True ,k = False)
	    mc.setAttr('%s.scaleX'%crvGmb,l=True ,k = False)
	    mc.setAttr('%s.scaleY'%crvGmb,l=True ,k = False)
	    mc.setAttr('%s.scaleZ'%crvGmb,l=True ,k = False)
	    mc.setAttr('%s.v'%crvGmb,l=True ,k = False)
	    
	    mc.setAttr('%s.scaleX'%zGrp,l=True ,k = False)
	    mc.setAttr('%s.scaleY'%zGrp,l=True ,k = False)
	    mc.setAttr('%s.scaleZ'%zGrp,l=True ,k = False)
	    rt.addSquash( crv , jnt[i] , ax = ( 'sx' , 'sz'  ))
	    
	    #rt.localWorld( ctrl[0] , ctrlGrp , grp , 'orient' )
	    #localWorld( obj = '' , worldObj = '' , localObj = '' , consGrp = '' , type = '' )

	for a in range(size):
	    if not a == size-1:
	        mc.parent(grp[a+1],gmb[a])