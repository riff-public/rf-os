import os
import sys

def runRig():
    sys.path.insert( 0,r'O:\Pipeline\core\rf_maya\rftool\rig\dark_horse' )

    import maya.cmds as mc


    scn_path = os.path.normpath( mc.file( q = True , l = True)[0])

    scn_fld_path , scn_nm_ext = scn_path.split('version')

    print scn_path

    rd_path = os.path.join(scn_fld_path, 'data')
    mr_path = os.path.join(rd_path, 'moduleRig.py')
    print rd_path

    if os.path.exists(mr_path):
        sys.path.insert(0,rd_path)
        
        import moduleRig
        reload(moduleRig)
        
        try :
            moduleRig.run()
        except :
            pass
        
        print moduleRig
        del(moduleRig)