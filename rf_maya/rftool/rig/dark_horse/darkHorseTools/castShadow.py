import maya.cmds as mc

def castShadowOff():
    obj = mc.ls(sl= True)
    for i in range(len(obj)):
        objList = mc.listRelatives(obj[i],ad = True,typ='shape')
        for objShape in objList:
            if  mc.objectType(objShape) == 'mesh' or mc.objectType(objShape) == 'transform':
                getCast = mc.getAttr('%s.castsShadows'%objShape)
            
                if getCast == True:
                    mc.setAttr('%s.castsShadows'%objShape,0)
                    print mc.getAttr('%s.castsShadows'%objShape)
                else: 
                    getCast = mc.getAttr('%s.castsShadows'%objShape)
                    print 'The cast shadow have already'+' ' + str(getCast)
            else:
                pass
                
def castShadowOn():
    obj = mc.ls(sl= True)
    for i in range(len(obj)):
        objList = mc.listRelatives(obj[i],ad = True,typ='shape')
        for objShape in objList:
            if  mc.objectType(objShape) == 'mesh' or mc.objectType(objShape) == 'transform':
                getCast = mc.getAttr('%s.castsShadows'%objShape)
            
                if getCast == False:
                    mc.setAttr('%s.castsShadows'%objShape,1)
                    print mc.getAttr('%s.castsShadows'%objShape)
                else: 
                    getCast = mc.getAttr('%s.castsShadows'%objShape)
                    print 'The cast shadow have already'+' ' + str(getCast)
            else:
                pass

