import maya.cmds as mc
import maya.mel as mm

def clearAllLayer() :
    layers = mc.ls( type = 'displayLayer' )
    for layer in layers :
        if not layer == 'defaultLayer' :
            attrs = ( 'identification' , 'drawInfo' )
            for attr in attrs :
                objs = mc.listConnections( '%s.%s' %(layer , attr) , p = True )
                if objs :
                    for obj in objs :
                        if 'layerManager' in obj :
                            mc.disconnectAttr( obj , '%s.%s' %(layer , attr) )
                        else :
                            mc.disconnectAttr( '%s.%s' %(layer , attr) , obj )
                            
            mc.delete( layer )

def clearAllAnim() :
    animCrvs = mc.ls( type = 'animCurve' )
    for animCrv in animCrvs :
        mc.delete( animCrv )

def clearUnusedAnim() :
    animCrvs = mc.ls( type = 'animCurve' )
    for animCrv in animCrvs :
        if not mc.listConnections( animCrv ) :
            mc.delete( animCrv )

def clearUnknowNode() :
    try : mc.delete( mc.ls ( type = 'unknown' ))
    except TypeError : pass

def clearUnusedNode() :
    mm.eval( 'MLdeleteUnused' )

def clearShapeName() :
    transformNode = mc.ls( type = 'transform' )
    
    if transformNode :
        for tfn in transformNode :
            shapes = mc.listRelatives( tfn , s = True )
            
            if shapes :
                if not mc.nodeType( shapes[0] ) == 'camera' :
                    mc.rename( shapes[0] , '%sShape' %tfn )

def clearUnusedShape() :
    types = [ 'mesh' , 'nurbsSurface' ]
    
    try :
        for type in types :
            shape = mc.ls( type = type )
            
            if len(shape) > 1 :
                shapes = shape 
            else :
                shapes = []
                shapes.append( shape )
                
            for shp in shapes :
                if not mc.listConnections( shp , s = False , d = True , type = 'shadingEngine') :
                    mc.delete( shp )
    
    except TypeError : pass

def removeAllShader():
    exceptions = ( 'lambert1' , 'particleCloud1' )
    mat = mc.ls( mat = True )
    
    for each in mat :
        if not each in exceptions :
            mc.delete( each )
        
    for each in mc.ls( type = 'mesh' ):
        mc.select( each , r = True )
        cmd = 'hyperShade -assign lambert1;'
        mm.eval(cmd)

def removeNamespace() :
    sels = mc.ls ( sl = True )
    
    if sels :
        for sel in sels :
            if ':' in sel :
                refprefix = sel.split(':')[0]
                mc.namespace( rm = refprefix , mnr = True )
    else :
        mc.warning( 'No Select Object. Please Select One Object.' )

def removeAllNamespace() :
    nss = sorted(mc.namespaceInfo( lon = True , r = True ) , key = len , reverse = True )

    for ns in nss:
        if not ns in ( 'UI', 'shared' ) :
            mc.namespace( rm = ns , mnr = True )

def importRef() :
    sel = mc.ls( sl = True )
    refNd = mc.ls( sel , rn = True )
    
    if sel == [] :
        mc.warning ( 'Please Select Object.' )
    else :
        if not refNd == [] :
            for each in sel :
                nameSpace = each.split(':')[0]
                reffile = mc.file ( q = True , r = True )
                
                for ref in reffile :
                    refNameSpace = mc.referenceQuery ( ref , referenceNode = True ).replace ( "RN" , "" )
                    
                    if nameSpace == refNameSpace :
                        mc.file ( ref , ir = True )
                        print '\n## %sRN has been imported.\n' %refNameSpace
        else :
            mc.warning ( 'Not Reference.' )

def removeRef() :
    sel = mc.ls ( sl = True )
    refNd = mc.ls( sel , rn = True )
    
    if sel == [] :
        mc.warning ( 'Please Select Object.' )
    else :
        if not refNd == [] :
            for each in sel :
                nameSpace = each.split(':')[0]
                reffile = mc.file ( q = True , r = True )
                
                for ref in reffile :
                    refNameSpace = mc.referenceQuery ( ref , referenceNode = True ).replace ( "RN" , "" )
                    
                    if nameSpace == refNameSpace :
                        mc.file ( ref , rr = True )
                        print '\n## %sRN has been removed.\n' %refNameSpace
        else :
            mc.warning ( 'Not Reference.' )

def reloadRef() :
    sel = mc.ls ( sl = True )
    refNd = mc.ls( sel , rn = True )
    
    if sel == [] :
        mc.warning ( 'Please Select Object.' )
    else :
        if not refNd == [] :
            for each in sel :
                nameSpace = each.split(':')[0]
                reffile = mc.file ( q = True , r = True )
                
                for ref in reffile :
                    refNameSpace = mc.referenceQuery ( ref , referenceNode = True ).replace ( "RN" , "" )
                    
                    if nameSpace == refNameSpace :
                        mc.file ( lr = '%sRN' %nameSpace )
                        print '\n## %sRN has been reload.\n' %refNameSpace
        else :
            mc.warning ( 'Not Reference.' )

def replaceRef() :
    sel = mc.ls ( sl = True )
    
    if sel == [] :
        mc.warning ( 'Please Select Object.' )
    else :
        refNd = mc.ls( sel[0] , rn = True )
        if not refNd == [] :
            nameSpace = sel[0].split(':')[0]
            reffile = mc.file ( q = True , r = True )
            
            for ref in reffile :
                refNameSpace = mc.referenceQuery ( ref , referenceNode = True ).replace ( "RN" , "" )
                
                if nameSpace == refNameSpace :
                    fileName = mc.fileDialog( t = 'Browser Reference ' )
                    array = len(fileName)
                    
                    if array == 0 :
                        pass
                    else :
                        mc.file( fileName , lr = '%sRN' %nameSpace , type = 'mayaAscii' , options = 'v=0' )
                        print '\n## %sRN has been replace.\n' %refNameSpace
        else :
            mc.warning ( 'Not Reference.' )