import maya.cmds as mc
import os
import pickle
def getpathDir() :
    sceneName = mc.file( q = True , sn = True )
    scenePath = sceneName.split( '/' )
    scenePath[-2] = 'data'
    
    pathDir = '/'.join( scenePath[0:-1] )

    if not os.path.isdir( pathDir ) :
    	os.mkdir( pathDir )
    
    return pathDir
def writeCtrlAll():
    getPD = getpathDir()
    ctrls = mc.ls('*trl')
    fn = '%s/ctrlShape.txt' %getPD
    writeCtrlShape(ctrls,fn)
    
def readCtrlAll(search='',replace=''):
    getPD = getpathDir()
    ctrls = mc.ls('*trl')
    fn = '%s/ctrlShape.txt' %getPD
 
    readCtrlShape(ctrls,fn,search = search,replace = replace)
        
def writeCtrlShape(ctrls=[],fn=''):
    fid = open(fn,'w')
    ctrlDict = {}
   
    for ctrl in ctrls:
        shapes = mc.listRelatives(ctrl,s=True)
        if type(shapes) == type([]) and mc.nodeType(shapes[0]) == 'nurbsCurve' :
            cv = mc.getAttr('%s.spans'%shapes[0]) + mc.getAttr('%s.degree'%shapes[0])
            
            for x in range(0,cv):
                cvName = '%s.cv[%s]' %(shapes[0],str(x))
                ctrlDict[cvName] = mc.xform(cvName,q=True,os=True,t=True)
               
                
            if mc.getAttr('%s.overrideEnabled' %shapes[0]):
                colVal = mc.getAttr('%s.overrideColor' %shapes[0])
                ctrlDict[shapes[0]] = colVal
                
    pickle.dump(ctrlDict,fid)               
    fid.close() 
     
                      
def readCtrlShape(ctrls=[],fn='',search='',replace=''):   
    fid = open(fn,'r')
    ctrlDict = pickle.load(fid)     
    fid.close()
    
    for key in ctrlDict.keys():
        print key
    
        if search:
            cvVertex = key.replace(search,replace)
                
        else:
            cvVertex = key
    
        if '.' in cvVertex:
            if mc.objExists( cvVertex ):
                mc.xform( cvVertex , os = True , t = ctrlDict[cvVertex] )
        else:
            if mc.objExists( cvVertex ):
                mc.setAttr( '%s.overrideEnabled' % cvVertex , 1 )
                mc.setAttr( '%s.overrideColor' % cvVertex , ctrlDict[cvVertex])