import maya.cmds as mc
import maya.mel as mm

def getSkinCluster():
    sels = mc.ls(sl = True)
    
    for mesh in sels:
        if not mesh == sels[0] :
        #print mesh 
            nodes = None
            for node in mc.listHistory(mesh):
                if mc.nodeType(node) == 'skinCluster':
                    nodes = node
    
            if nodes:
                copySkinweightSeletedToAll(sels[0],mesh)
            if not nodes:
                jnt = mc.skinCluster(sels[0],query=True,inf=True)
    
                mc.skinCluster( jnt,mesh)
                copySkinweightSeletedToAll(sels[0],mesh)
                    
def copySkinweightSeletedToAll(src, tar):
    mc.select( src , r = True )
    mc.select( tar , add = True )
    mc.copySkinWeights( noMirror = True , surfaceAssociation = 'closestPoint' )
    mc.select(clear = True)