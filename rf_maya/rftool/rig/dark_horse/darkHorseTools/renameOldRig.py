import maya.cmds as mc 
import re 
def reNameClav ():
        for side in 'L','R':
            clavLs = mc.listRelatives('ClavRig_%s_Grp'%side,ad=True)
            #print clavLs
            for clav in clavLs:
                mc.rename(clav,re.sub('Clavicle','Clav',clav))

def reNameSpine():

    spineLs = mc.listRelatives('SpineRig_Grp',ad=True)    

    for spine in spineLs:
        
        find = re.findall('Spine.Fk',spine)
        
        if not find ==[] :
                    find = find[0]
                    newSpine = re.split('Fk',find)[0]
                    print newSpine
                    mc.rename(spine,re.sub('%s'%find,'%s'%newSpine,spine))

def renameEye():
    eyeLs = mc.listRelatives('EyeRig_Grp',ad=True) 
    eyeLs.append('EyeRig_Grp')  

    for eye in eyeLs:
        eyeTrg = re.findall('EyeTarget',eye)
        eyeSide = re.findall(r'_([L|R])',eye)
        
        print eyeSide
        print eyeTrg
        if not eyeTrg ==[]:
            eyeTrg = eyeTrg[0]
            newEye = mc.rename(eye,re.sub('EyeTarget','EyeTrgt',eye))
            
            if not eyeSide ==[]:
                eyeSide = eyeSide[0]
                if eyeSide == 'L':
                    mc.rename(newEye,re.sub('_L','LFT',newEye))
                    
                if eyeSide == 'R':
                    mc.rename(newEye,re.sub('_R','RGT',newEye))
        
        if eyeTrg ==[]:
            if not eyeSide ==[]:
                eyeSide = eyeSide[0]
                if eyeSide == 'L':
                    mc.rename(eye,re.sub('_L','LFT',eye))
                    
                if eyeSide == 'R':
                    mc.rename(eye,re.sub('_R','RGT',eye))

def renameForeArm():
    for side in 'L','R':
        allForeArmLs =['ForeArmRbnSkin_%s_Grp'%side,'ForeArmFkCtrlZro_%s_Grp'%side,'ForeArmRbn_%s_Grp'%side,'ForeArmFk_%s_Jnt'%side]
        for each in allForeArmLs:
            #print each
            foreArmLs = mc.listRelatives(each,ad=True) 
            foreArmLs.append(each)
            #print foreArmLs  
        
            for foreArm in foreArmLs:
                mc.rename(foreArm,re.sub('ForeArm','Forearm',foreArm))

def renameOldRig():
    sel = mc.ls(sl=True)
    selLs = mc.listRelatives(sel,ad=True,pa=True)

    selLs.append(sel[0])
     
    for each in selLs:
       
        paname = each.split('|')[-1]

        if paname =='clusterHandleShape':
            continue
        if not re.search(r'lft|rgt|lwr|upr', each):
           
        
            selPart = re.findall(r'(^[a-zA-Z0-9]+)_',paname)
          
            
            selType = re.findall(r'_([a-zA-Z0-9]+)$',paname)
           
            if not selPart ==[] :
                selPart = selPart[0]
           
                newPart = "%s%s" % (selPart[0].upper(), selPart[1:])
                
                if newPart =='Master':
                     newPart = re.sub('Master','AllMover',newPart)
                
                elif newPart =='Placement':
                     newPart = re.sub('Placement','Offset',newPart)
            
            if not selType ==[]:
                selType = selType[0]
                newType = "%s%s" % (selType[0].upper(), selType[1:])
                
            print paname
            newname = mc.rename(each,'%s_%s' %(newPart,newType))
            
            
        elif re.search(r'lft|rgt|lwr|upr', paname): 
            selPart = re.findall(r'(^[a-zA-Z0-9]+)_',paname)
            selSide = re.findall(r'_(lft|rgt|lwr|upr)_',paname)
            selType = re.findall(r'_([a-zA-Z0-9]+)$',paname)
        
            if not selPart ==[]:
                selPart = selPart[0]
                
                newPart = "%s%s" % (selPart[0].upper(), selPart[1:])

            
                if selSide:
                    
                    if selSide[0] =='lft':
                        newSide = '_L'
                        
                    elif selSide[0] =='rgt':
                        newSide = '_R'
                        
                    elif selSide[0] =='upr':
                        newSide = 'Upr'
                    elif selSide[0] =='lwr':
                        newSide = 'Lwr'
                
                if not selType ==[]:
                    selType = selType[0]
                    newType = "%s%s" % (selType[0].upper(), selType[1:])
            print paname
            newname = mc.rename(each,'%s%s_%s' %(newPart,newSide,newType))
            
    amg = mc.createNode('transform', n='AllMover_Grp')
    mc.parent(amg,'Rig_Grp')
    mc.parent('AllMover_Ctrl',amg)

    reNameClav()
    reNameSpine()
    renameEye()
    renameForeArm()

    





            
        
      

    

