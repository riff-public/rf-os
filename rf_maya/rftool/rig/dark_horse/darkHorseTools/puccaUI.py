import sys
sys.path.append( r'O:\Pipeline\legacy\lib' )
import pymel.core as pm
import maya.cmds as mc 
from functools import partial
import rigPropPucca as rPrp
reload(rPrp)
import core as core
reload(core) 
#import runModuleRig as rm
import readWriteCtrl as rwCtrl
reload(rwCtrl)
import weightTools as rwWeight
reload(rwWeight)
import mainCtrl 
reload(mainCtrl)
import rigTools as rt
reload(rt)
import fixTextureTool as fixTxr
reload(fixTxr)
import maya.mel as mel
import cleanUpTools as cut
reload(cut)
import fkRig as fk
reload(fk)
import fkDtl as fkD
reload(fkD)
import parJnt as pj 
reload (pj)
import ieShadPucca as ieShad
reload (ieShad)
import castShadow as cs
reload (cs)
import selRefFromeOutliner as selRefOut
reload (selRefOut)
import copySkinweightSeletedToAll as scw
reload(scw)

class PuccaToolsUI( object ):
    def __init__( self ):
        # check if window exists
        if pm.window ( 'puccaUI' , exists = True ) :
            pm.deleteUI ( 'puccaUI' ) 
            
        width = 285
        window = pm.window ( 'puccaUI', title = "PuccaTools",width = width  , mnb = True , mxb = False , sizeable = True , rtf = True ) 
            
        #pm.window ( 'puccaUI' , e = True , w = 300, h = 250 ) 
        with window :
            mainLayout = pm.rowColumnLayout (w = width, nc = 1 ,columnWidth = [ ( 1 , width ) ] ) ;
            with mainLayout :
                rigFrameLayout = pm.frameLayout ( label = 'PuccaRig' , collapsable = True , collapse = False , bgc = ( 0 , 0 , 0 ))
                with rigFrameLayout:
                    # pm.button('Ctrl',w = width,h= 80)
                    pm.text(label = 'characterRig',align='left')
                    pm.button('runModuleRig',label = 'runModuleRig',h= 35,w = 50,c = self.runModuleRig)
                    layoutMainRig = pm.rowColumnLayout(w =width, nc =4)
                    with layoutMainRig:
                        pm.button('writeCtrl',label = 'writeCtrl',h= 35,w = width/4,c=self.writeCtrl)
                        pm.button('readCtrl',label = 'readCtrl',h= 35,w = width/4,c=self.readCtrl)
                        pm.button('writeWeight',label = 'writeWeight',h= 35,w = width/4,c=self.writeWeight)
                        pm.button('readWeight',label = 'readWeight',h= 35,w = width/4,c=self.readWeight)
                

            rigPropLayout = pm.frameLayout ( label = 'PuccaProp' , collapsable = True , collapse = False , bgc = ( 0 , 0 , 0 ))
            with rigPropLayout:
                
                #pm.text(label = 'MainCtrl',align='left' )
                pm.button('RigMainCtrl',label="RigMainCtrl",h= 35,c= self.rigMainCtrl)
                #pm.text(label = 'RigCtrl Prop',align='left' )

                rigToolLayoutAA = pm.rowColumnLayout(w =width, nc =4)
                with rigToolLayoutAA:

                    pm.button('selCtrl',label='rigPropCtrl',h= 35,w =width/3,c= self.importRigPropPucca)
                    
                    pm.popupMenu()
                    labels = ( 'sphere' , 'cylinder' , 'stick' , 'circle','plus', 'square' , 'cube', 'locator')
                
                    for label in labels :
                        mc.menuItem( l = label , c = partial( self.changeButtonCtrl , label ))


                    pm.button('fkRig',label = 'fkRigCtrl',h=35,w= width/3,c=self.importFkRig)
                    pm.popupMenu()
                    labels = ( 'sphere' , 'cylinder' , 'stick' , 'circle','plus', 'square' , 'cube', 'locator')
                
                    for label in labels :
                        mc.menuItem( l = label , c = partial( self.changeBFkCtrl , label ))

                    pm.button('fkDtl',label = 'fkDtlCtrl',h=35,w= width/3,c=self.importFkDtlRig)
                    pm.popupMenu()
                    labels = ( 'sphere' , 'cylinder' , 'stick' , 'circle','plus', 'square' , 'cube', 'locator')
                
                    for label in labels :
                        mc.menuItem( l = label , c = partial( self.changeDtlFkCtrl , label ))
                # pm.menuItem(label='circle')
                # pm.menuItem(label='square')
                # pm.menuItem(label='cube')
                # pm.menuItem(label='sphere')
                #pm.text(label = 'setColor',align='left' )
                layoutColor = pm.rowColumnLayout(w =width, nc =8)
                pm.button(label = 'R',h= 25,w = width/8,bgc = (1,0,0),c= partial(self.changeButtonColor ,'red'))
                pm.button(label = 'B',h= 25,w = width/8,bgc = (0,0,1),c= partial(self.changeButtonColor ,'blue'))
                pm.button(label = 'G',h= 25,w = width/8,bgc = (0,1,0),c= partial (self.changeButtonColor ,'green'))
                pm.button(label = 'Y',h= 25,w = width/8,bgc = (1,1,0),c= partial(self.changeButtonColor ,'yellow'))
                pm.button(label = 'W',h= 25,w = width/8,bgc = (1,1,1),c= partial(self.changeButtonColor ,'white'))
                pm.button(label = 'C',h= 25,w = width/8,bgc = (0.392,0.863,1),c= partial(self.changeButtonColor ,'cyan'))
                pm.button(label = 'P',h= 25,w = width/8,bgc = (1.010,0.707,0.707),c= partial(self.changeButtonColor ,'pink'))
                pm.button(label = 'DR',h= 25,w = width/8,bgc = (0.596,0.149,0),c= partial(self.changeButtonColor ,'darkRed'))
            
            rigToolLayout = pm.frameLayout ( label = 'toolsPucca' , collapsable = True , collapse = False , bgc = ( 0 , 0 , 0 ))
            with rigToolLayout:

                pm.text(label = 'tools',align='left' )
                
                pm.button('createCtrl',label='createCtrl',h = 35,w =  width/2,c = self.createCtrl)
                pm.popupMenu()
                labels = ( 'sphere' , 'cylinder' , 'stick' , 'circle','plus', 'square' , 'cube', 'locator')
            

                for label in labels :
                    mc.menuItem( l = label , c = partial( self.changeCreateBttCtrl, label ))  
                
                rigToolLayoutA = pm.rowColumnLayout(w =width, nc =5)
                with rigToolLayoutA:
                    pm.button(label= 'linkTxr',h=30,w = width/5,c= self.fixTxr)
                    pm.button(label= 'txrMng',h=30,w = width/5,c=self.txrMng)
                    pm.button('createGrp',label = 'createGrp',h= 30,w =width/5,c=self.addGrp)
                    pm.button('copyUv',label = 'copyUv',h= 30,w =width/5,c=self.copyUv)
                    pm.button('copyWeight',label = 'cw',h= 30,w =width/5,c=self.copyWeight)

                rigToolLayoutB = pm.rowColumnLayout(w =width, nc = 5)
                with rigToolLayoutB:
                    pm.button('selCopyWeight',label = 'selCW',h= 30,w =width/5,c=self.selCopyWeight)  
                
                pm.text(label = 'cleanupTools',align='left' )
                rigToolLayoutC = pm.rowColumnLayout(w =width, nc =4)
                with rigToolLayoutC:
                    pm.button(label= 'revAllNamespace',h=30,w = width/3,c=self.removeAllNamespace)
                    pm.button(label= 'revSelNamespace',h=30,w = width/3,c=self.removeNamespace)
                    pm.button(label= 'clearLayer',h=30,w = width/3,c =self.clearAllLayer) 

                rigToolLayoutD = pm.rowColumnLayout(w =width, nc =4)
                with rigToolLayoutD:
                    pm.button(label= 'importRef',h=30,w = width/3,c =self.importRef)
                    pm.button(label= 'reloadRef',h=30,w = width/3,c = self.reloadRef)
                    pm.button(label= 'delRef',h=30,w = width/3,c=self.removeRef)

                rigToolLayoutF = pm.rowColumnLayout(w =width, nc =4)
                with rigToolLayoutF:
                    
                    pm.button(label= 'unusedNode',h=30,w = width/4,c =self.clearUnusedNode)
                    pm.button(label= 'exShade',h=30,w = width/4,c= self.exportShadPucca)
                    pm.button(label= 'imShade',h=30,w = width/4,c= self.importShadPucca)
                    pm.button(label= 'addBsh',h=30,w = width/4,c = self.addBlendShape )
                    # pm.button(label= 'reloadRef',h=30,w = width/4,c = self.reloadRef)
                    # pm.button(label= 'delRef',h=30,w = width/3,c=self.removeRef)


                pm.text(label = 'castShadowOption',align='left' )
                rigToolLayoutG = pm.rowColumnLayout(w =width, nc =4)
                with rigToolLayoutG:

                    pm.button(label= 'castShaOff',h=30,w = width/4,c =self.castShaOff)
                    pm.button(label= 'castShaOn',h=30,w = width/4,c =self.castShaOn)

                
                rigToolLayoutH = pm.rowColumnLayout(w =width, nc =4)
                with rigToolLayoutH:

                    pm.button(label= 'selRefOut',h=30,w = width/4,c =self.selRefOut)
                    #pm.button(label= 'castShaOn',h=30,w = width/4,c =self.castShaOn)
                    
                    
                
                pm.text(label = 'snap',align='left')
                rigToolLayoutE = pm.rowColumnLayout(w =width, nc =5)
                with rigToolLayoutE:
                    pm.button(label = 'parent',h= 30,w= width/5,c= self.snapParent)
                    pm.button(label = 'point',h= 30,w= width/5,c= self.snapPoint)
                    pm.button(label = 'orient',h= 30,w= width/5,c = self.snapOrient)
                    pm.button(label = 'sclParent',h= 30,w= width/5,c=self.snapParScl)
                    pm.button(label = 'parJnt',h= 30,w= width/5,c=self.importparJnt)

                

                pm.separator( vis = False )
           
        


        pm.showWindow( window )
    
    def changeButtonCtrl( self , label = '' , arg = None ) :
        mc.button( 'selCtrl' , e = True , l = label )

    def changeButtonColor( self , col , arg=None) :
        sels = mc.ls(sl=True)
        for sel in sels:
            obj =core.Dag(sel)
            obj.setColor(col)
       

    def importRigPropPucca(self ,arg):
        shape = pm.button('selCtrl',q=True,l=True)
        rPrp.rigPropPucca(shape)

    def runModuleRig(self ,arg):
        import runModuleRig as rm
        reload(rm)
        rm.runPuccaRig()
        

    def readCtrl(self ,arg):
        rwCtrl.readCtrlAll()

    def writeCtrl(self ,arg):
        rwCtrl.writeCtrlAll()

    def writeWeight(self ,arg):
        rwWeight.writeSelectedWeight()

    def readWeight(self ,arg):
        rwWeight.readSelectedWeight()

    def rigMainCtrl (self,arg=None):
        mainCtrl.rigMain()

    def snapParent(self,arg):
        sel =mc.ls(sl=True)
        obj = core.Dag(sel[1])
        obj.snap(sel[0])

    def snapPoint(self,arg):
        sel =mc.ls(sl=True)
        obj = core.Dag(sel[1])
        obj.snapPoint(sel[0])

    def snapOrient(self,arg):
        sel =mc.ls(sl=True)
        obj = core.Dag(sel[1])
        obj.snapOrient(sel[0])

    def snapParScl(self,arg):
        sel =mc.ls(sl=True)
        obj = core.Dag(sel[1])
        obj.snap(sel[0])
        obj.snapScale(sel[0])

    def changeCreateBttCtrl( self , label = '' , arg = None) :
        mc.button( 'createCtrl' , e = True , l = label )


    def createCtrl(self,arg= None):
        shape = pm.button('createCtrl',q=True,l=True)
        rt.createCtrl('curve1',shape)

    def fixTxr(self,arg=None):
        fixTxr.fixTextureTool()
        

    def txrMng(self,arg):

        source=( 'O:/Pipeline/legacy/lib/local/cherTools/FileTextureManager.mel')
        mel.eval('FileTextureManager;')

    def addGrp(self,arg=None):
        sel =mc.ls(sl=True)
        for each in sel:
            name = each.split('_')[0]
            rt.addGrp(each)

    def importRef( self , arg = None ) :
        cut.importRef()

    def removeRef( self , arg = None ) :
        cut.removeRef()

    def reloadRef( self , arg = None ) :
        cut.reloadRef()

    def removeAllNamespace( self , arg = None ) :
        cut.removeAllNamespace()

    def removeNamespace( self , arg = None ) :
        cut.removeNamespace()

    def clearAllLayer( self , arg = None ) :
        cut.clearAllLayer()

    def clearUnusedNode( self , arg = None ) :
        cut.clearUnusedNode()

    def changeBFkCtrl( self , label = '' , arg = None ) :
        mc.button( 'fkRig' , e = True , l = label )
    
    def importFkRig(self ,arg):
        shape = pm.button('fkRig',q=True,l=True)
        fk.fkRig(shape)

    # def fixTextureSelectPath( self , arg = None ) :
    #     txr.fixTextureSelectPath()

    # def transferShade( self , arg = None ) :
    #     txr.transferShade()

    # def transferShadeWithOutRef( self , arg = None ) :
    #     txr.transferShadeWithOutRef()
     

    def copyUv(self,arg):
        mel.eval('source "O:/Pipeline/legacy/lib/local/cherTools/puccaTools/copyUV.mel"')
        mel.eval('copyUv();')

    def copyWeight(self,arg):
        mel.eval('source "O:/Pipeline/legacy/lib/local/cherTools/puccaTools/copyWeight.mel"')
        mel.eval('copyWeight();')

    def selCopyWeight(self,arg):
        print 'here'
        scw.getSkinCluster()

    def changeDtlFkCtrl( self , label = '' , arg = None) :
        mc.button( 'fkDtl' , e = True , l = label )

    def importFkDtlRig(self ,arg):
        shape = pm.button('fkDtl',q=True,l=True)
        fkD.fkDtl(shape)

    def importparJnt(self ,arg):
        pj.parJnt()

    def importShadPucca (self,arg):
        ieShad.importshade()

    def exportShadPucca (self,arg):
        ieShad.exportShade()

    def addBlendShape( self , arg = None ) :
        import sys
        sys.path.append( r'O:\Pipeline\legacy\lib\local' )
        from lpRig import core as lrc
        reload(lrc)
        from lpRig import rigTools as lrr
        reload(lrr)

        lrr.doAddBlendShape()

    def castShaOff (self,arg):
        cs.castShadowOff()

    def castShaOn (self,arg):
        cs.castShadowOn()

    def selRefOut(self,arg):
        selRefOut.selectInReferenceEditor() 
 


 
