import maya.cmds as mc
import os
import shutil

class SceneInfo(object) :
    def __init__(self, filePath = '') :
        
        if not filePath :
            filePath = mc.file(q = True, sn = True)
            self.filePath = filePath

        self.scnDir = '/'.join(self.filePath.split('/')[:-1])

    def __str__(self) :
        return self.filePath

    def __repr__(self) :
        return self.filePath

    def doCreateTextureDirectory(self) :
        if not os.path.exists('%s/txr' % self.scnDir) :
            os.makedirs('%s/txr' % self.scnDir)

        return '%s/txr' %self.scnDir


def fixTextureTool() :
    
    txtFls = mc.ls(type = 'file')
    scnInfo = SceneInfo()
    txtHeroPath = scnInfo.doCreateTextureDirectory()

    result = mc.confirmDialog(
                                title='Confirm',
                                message='All texture files will be moved to %s. Are you sure?' % txtHeroPath,
                                button=['Yes','No'],
                                defaultButton='Yes',
                                cancelButton='No',
                                dismissString='No'
                            )

    if result == 'Yes' :

        for fl in txtFls : 
            txtPth =mc.getAttr('%s.fileTextureName' % fl)

            if os.path.exists(txtPth) :
                try :
                    shutil.copyfile(txtPth, '%s/%s' % (txtHeroPath, txtPth.split('/')[-1]))

                    mc.setAttr('%s.fileTextureName' % fl, '%s/%s' % (txtHeroPath, txtPth.split('/')[-1]), type = 'string')

                    print '%s/%s Done.' % (txtHeroPath, txtPth.split('/')[-1])
                    
                except :
                    pass

            else :
                print '%s/%s does not exists.' % (txtHeroPath, txtPth.split('/')[-1])

#fixTextureTool()