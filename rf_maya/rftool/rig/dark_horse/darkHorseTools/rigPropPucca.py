import os
import sys
sys.path.append( r'O:\Pipeline\legacy\lib\local\cherTools' )
from puccaTools import core
reload(core)
from puccaTools import rigTools as rt
reload(rt)
import maya.cmds as mc
import pymel.core as pm




###############################################
def rigPropPucca(shape='square',col='red'):
    sel = mc.ls(sl=True)
    for each in sel:
        sideEach = core.Dag(each)
        side = sideEach.getSide()
        name = each.split('_')[0]

        grpZ = rt.addGrp(each)
        crv = rt.createCtrl( '%s%sCtrl' %( name ,side ) , shape,col , jnt = False )
        crvGmb = rt.addGimbal( crv )
        ctrlGrp = rt.addGrp(crv)
        snpCtrl = ctrlGrp.snap(grpZ)
        
        mc.parentConstraint(crvGmb,grpZ,mo=1)
        mc.scaleConstraint(crvGmb,grpZ,mo=1)
