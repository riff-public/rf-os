import maya.cmds as mc
import maya.mel as mm
import ppmel.core as core
reload( core )


def ribbonInfo( ax = '' ) :

    if ax == 'x+' :
        postAx = 'tx'
        twstAx =  'rx'
        sqshAx = [ 'sy' , 'sz' ]
        aimVec = [ 1 , 0 , 0 ]
        invAimVec = [ -1 , 0 , 0 ]
        upVec = [ 0 , 0 , 1 ]
        rotate = [ 0 , 0 , -90 ]
        
    elif ax == 'x-' :
        postAx = 'tx'
        twstAx =  'rx'
        sqshAx = [ 'sy' , 'sz' ]
        aimVec = [ -1 , 0 , 0 ]
        invAimVec = [ 1 , 0 , 0 ]
        upVec = [ 0 , 0 , 1 ]
        rotate = [ 0 , 0 , 90 ]
    
    elif ax == 'y+' :
        postAx = 'ty'
        twstAx =  'ry'
        sqshAx = [ 'sx' , 'sz' ]
        aimVec =  [ 0 , 1 , 0 ]
        invAimVec = [ 0 , -1 , 0 ]
        upVec = [ 0 , 0 , 1 ]
        rotate = [ 0 , 0 , 0 ]
        
    elif ax == 'y-' :
        postAx = 'ty'
        twstAx =  'ry'
        sqshAx = [ 'sx' , 'sz' ]
        aimVec = [ 0 , -1 , 0 ]
        invAimVec = [ 0 , 1 , 0 ]
        upVec = [ 0 , 0 , 1 ]
        rotate = [ 0 , 0 , 180 ]
    
    elif ax == 'z+' :
        postAx = 'tz'
        twstAx =  'rz'
        sqshAx = [ 'sx' , 'sy' ]
        aimVec = [ 0 , 0 , 1 ]
        invAimVec = [ 0 , 0 , -1 ]
        upVec = [ 0 , 1 , 0 ]
        rotate = [ 90 , 0 , 180 ]
    
    elif ax == 'z-' :
        postAx = 'tz'
        twstAx =  'rz'
        sqshAx = [ 'sx' , 'sy' ]
        aimVec = [ 0 , 0 , -1 ]
        invAimVec = [ 0 , 0 , 1 ]
        upVec = [ 0 , 1 , 0 ]
        rotate = [ -90 , 0 , 0 ]

    return { 'postAx' : postAx , 'twstAx' : twstAx , 'sqshAx' : sqshAx , 'aimVec' : aimVec , 'invAimVec' : invAimVec , 'upVec' : upVec , 'rotate' : rotate }


def rigRibbon( name = 'chain' ,
               side = '_lft' ,
               num = 6 ,
               numDtl = 5 , # max 5
               ax = 'y+' ,
               jntRoot = 'joint1' ,
               jntEnd = 'joint2' ,
               charSize = 60 ) :
    
    
    pars = core.getParent( jntRoot )
    
    rbnInfo   = ribbonInfo( ax = ax )
    
    postAx    = rbnInfo[ 'postAx' ]
    twstAx    = rbnInfo[ 'twstAx' ]
    sqshAx    = rbnInfo[ 'sqshAx' ]
    aimVec    = rbnInfo[ 'aimVec' ]
    invAimVec = rbnInfo[ 'invAimVec' ]
    upVec     = rbnInfo[ 'upVec' ]
    rotate    = rbnInfo[ 'rotate' ]
    
    #-- Group
    rbnAnimGrp = core.transform( '%sRbnAnim%s_grp' %( name , side ))
    rbnSkinGrp = core.transform( '%sRbnSkin%s_grp' %( name , side ))
    rbnStillGrp = core.transform( '%sRbnStill%s_grp' %( name , side ))
    
    #-- Nurbs
    lenght = core.getDist( jntRoot , jntEnd )
    halfLenght = lenght / 2
    
    rbnNrb = mc.nurbsPlane( p = (0,halfLenght,0) , ax = (0,0,1) , w = True , lr = lenght , d = 3 , u = 1 , v = (num-1)*5 , ch = 0 , n = '%sRbn%s_nrb' % ( name , side ))[0]
    mc.setAttr( '%s.rx' %rbnNrb , rotate[0] )
    mc.setAttr( '%s.ry' %rbnNrb , rotate[1] )
    mc.setAttr( '%s.rz' %rbnNrb , rotate[2] )
    mc.makeIdentity( rbnNrb , apply = True , r = True )
    mc.select( cl = True )
    
    #-- Joint Main
    rbnJntDict = []
    rbnAimDict = []
        
    for i in range(1,num+1) :
        postion = (lenght/(num-1)*(i-1))
        
        rbnJnt = core.addJnt( '%s%sRbn%s_jnt' %( name , i , side ))
        rbnAim = mc.group( rbnJnt , n = '%s%sRbnJntAim%s_grp' %( name , i , side ))
        
        if ax in ( 'x-' , 'y-' , 'z-' ) :
            mc.setAttr( '%s.%s' %( rbnAim , postAx ) , -postion )
        else :
            mc.setAttr( '%s.%s' %( rbnAim , postAx ) , postion )
        
        rbnJntDict.append( rbnJnt )
        rbnAimDict.append( rbnAim )
    
    #-- Main Controls
    #-- Dict
    rbnCtrlDict = []
    rbnCtrlGmblDict = []
    rbnCtrlZroDict = []
    rbnCtrlOfstDict = []
    rbnFkCtrlDict = []
    rbnFkCtrlGmblDict = []
    rbnFkCtrlZroDict = []
    rbnFkCtrlOfstDict = []
    rootGrpDict = []
    endGrpDict = []
    
    # Group
    rbnMainGrp = core.transform( '%sRbnMain%s_grp' %( name , side ))
    
    # Controls
    rbnRootCtrl = core.addCtrl( '%sRbnRoot%s_ctrl' %( name , side ) , 'locator' , 'yellow' , jnt = False )
    rbnRootCtrlZro = core.addGrp( rbnRootCtrl )
    
    rbnEndCtrl = core.addCtrl( '%sRbnEnd%s_ctrl' %( name , side ) , 'locator' , 'yellow' , jnt = False )
    rbnEndCtrlZro = core.addGrp( rbnEndCtrl )
    core.snap( rbnAimDict[-1] , rbnEndCtrlZro )
    
    attrCtrl = core.addCtrl( '%sRbnAttr%s_ctrl' %( name , side ) , 'stick' , 'green' , jnt = False )
    attrCtrlZro = core.addGrp( attrCtrl )
    core.parCons( 0 , rbnEndCtrl , attrCtrlZro )
    
    attrCtrlShape = core.getShape( attrCtrl )
    core.addAttr( attrCtrl , 'slide' , -10 , 10 , 0 )
    core.addAttr( attrCtrlShape , 'detailVis' , 0 , 1 , 0 )
    
    for i in range(1,num+1) :
        rbnCtrl = core.addCtrl( '%s%sRbn%s_ctrl' %( name , i , side ) , 'square' , 'yellow' , jnt = False )
        rbnCtrlZro = core.addGrp( rbnCtrl )
        rbnCtrlOfst = core.addGrp( rbnCtrl , 'Ofst' )
        rbnCtrlGmbl = core.addGimbal( rbnCtrl )
        
        core.snap( rbnAimDict[i-1] , rbnCtrlZro )
        mc.parent( rbnAimDict[i-1] , rbnCtrlGmbl )
        
        rbnCtrlDict.append( rbnCtrl )
        rbnCtrlGmblDict.append( rbnCtrlGmbl )
        rbnCtrlZroDict.append( rbnCtrlZro )
        rbnCtrlOfstDict.append( rbnCtrlOfst )
    
    #-- Shape
    for dict in ( rbnCtrlDict , rbnCtrlGmblDict ) :
        for rbnCtrl in dict :
            core.scaleShape( rbnCtrl , charSize )
            core.rotateShape( rbnCtrl , rotate[0] , rotate[1] , rotate[2] )
    
    core.scaleShape( attrCtrl , charSize*1.75 )
    core.rotateShape( attrCtrl , rotate[0] , rotate[1] , rotate[2] )
    
    #-- Rig process
    #-- Bind skin
    rbnSkc = mc.skinCluster( rbnJntDict , rbnNrb , dr = 7 , mi = 2 , n = '%sRbn%s_skc' % ( name , side ))[0]
    
    mc.skinPercent( rbnSkc , '%s.cv[0:3][0]' %rbnNrb , tv = [ rbnJntDict[0] , 1 ] )
    mc.skinPercent( rbnSkc , '%s.cv[0:3][%s]' %(rbnNrb , ((num-1)*5)+2 ) , tv = [ rbnJntDict[num-1] , 1 ] )
    
    j = 1
    for i in range(0,num-1) :
        mc.skinPercent( rbnSkc , '%s.cv[0:3][%s]' %(rbnNrb , j ) , tv = [ (rbnJntDict[0+i] , 0.94) , (rbnJntDict[1+i] , 0.06) ] )
        j += 1
        mc.skinPercent( rbnSkc , '%s.cv[0:3][%s]' %(rbnNrb , j ) , tv = [ (rbnJntDict[0+i] , 0.80) , (rbnJntDict[1+i] , 0.20) ] )
        j += 1
        mc.skinPercent( rbnSkc , '%s.cv[0:3][%s]' %(rbnNrb , j ) , tv = [ (rbnJntDict[0+i] , 0.60) , (rbnJntDict[1+i] , 0.40) ] )
        j += 1
        mc.skinPercent( rbnSkc , '%s.cv[0:3][%s]' %(rbnNrb , j ) , tv = [ (rbnJntDict[0+i] , 0.40) , (rbnJntDict[1+i] , 0.60) ] )
        j += 1
        mc.skinPercent( rbnSkc , '%s.cv[0:3][%s]' %(rbnNrb , j ) , tv = [ (rbnJntDict[0+i] , 0.20) , (rbnJntDict[1+i] , 0.80) ] )
        j += 1
    
    #-- Stretch
    core.pntCons( 0 , rbnRootCtrl , rbnCtrlOfstDict[0] )
    core.pntCons( 0 , rbnEndCtrl , rbnCtrlOfstDict[-1] )
    
    for i in range(0,num) :
        if not i == (num-1) :
            mc.rename( mc.aimConstraint( rbnEndCtrl , rbnCtrlOfstDict[i] , aim = aimVec , u = upVec , wut = "objectrotation" , wu = upVec , wuo = rbnRootCtrlZro ) , '%s_aimCons' %rbnCtrlOfstDict[i] )
        else :
            mc.rename( mc.aimConstraint( rbnRootCtrl , rbnCtrlOfstDict[i] , aim = invAimVec , u = upVec , wut = "objectrotation" , wu = upVec , wuo = rbnEndCtrlZro ) , '%s_aimCons' %rbnCtrlOfstDict[i] )
    
    for i in range(1,num-1) : 
        lenght1 = core.getDist( rbnCtrlGmblDict[0] , rbnCtrlGmblDict[i] )/lenght
        lenght2 = core.getDist( rbnCtrlGmblDict[i] , rbnCtrlGmblDict[-1] )/lenght
        
        txMdv = core.multiplyDivide( '%s%sRbnTx%s_mdv' %( name , i , side ))
        tyMdv = core.multiplyDivide( '%s%sRbnTy%s_mdv' %( name , i , side ))
        tzMdv = core.multiplyDivide( '%s%sRbnTz%s_mdv' %( name , i , side ))
        
        txPma = core.plusMinusAverage( '%s%sRbnTx%s_pma' %( name , i , side ))
        tyPma = core.plusMinusAverage( '%s%sRbnTy%s_pma' %( name , i , side ))
        tzPma = core.plusMinusAverage( '%s%sRbnTz%s_pma' %( name , i , side ))
        
        for mdv in ( txMdv , tyMdv , tzMdv ) :
            mc.setAttr( '%s.i2x' %mdv , lenght2 )
            mc.setAttr( '%s.i2y' %mdv , lenght1 )
            
            if 'Tx' in mdv :
                posi = 'tx'
                posiPma = txPma
            elif 'Ty' in mdv :
                posi = 'ty'
                posiPma = tyPma
            elif 'Tz' in mdv :
                posi = 'tz'
                posiPma = tzPma
            
            mc.connectAttr( '%s.%s' %(rbnRootCtrl , posi) , '%s.i1x' %mdv )
            mc.connectAttr( '%s.%s' %(rbnEndCtrl , posi) , '%s.i1y' %mdv )
            
            mc.connectAttr( '%s.ox' %mdv , '%s.i1[0]' %posiPma )
            mc.connectAttr( '%s.oy' %mdv , '%s.i1[1]' %posiPma )
            
            mc.connectAttr( '%s.o1' %posiPma , '%s.%s' %(rbnCtrlOfstDict[i] , posi ))
        
    #-- Detail controls
    #-- Dict
    rbnDtlPosGrpDict = []
    posiDtlDict = []
    slidePmaDict = []
    rbnDtlAimDict = []
    rbnDtlJntDict = []
    rbnDtlJntZroDict = []
    rbnDtlCtrlDict = []
    rbnDtlCtrlGmblDict = []
    rbnDtlCtrlZroDict = []
    
    #-- Group
    rbnDetailGrp = core.transform( '%sRbnDetail%s_grp' % ( name , side ))
    core.connectAttr( '%s.detailVis' %attrCtrlShape , '%s.v' %rbnDetailGrp )
    
    for i in range(1,((num-1)*numDtl+1)+1) :
        #-- Posi group
        rbnPosGrp = core.transform( '%s%sRbnDtlPos%s_grp' %( name , i , side ))
        posi = core.pointOnSurfaceInfo( '%s%sRbnDtlPos%s_posi' %( name , i , side ))
        
        mc.setAttr( '%s.turnOnPercentage' %posi , 1 )
        mc.setAttr ( '%s.parameterU' %posi , 0.5 )
        
        mc.connectAttr( '%s.worldSpace[0]' %rbnNrb , '%s.inputSurface' %posi )
        mc.connectAttr ( '%s.position' %posi , '%s.t' %rbnPosGrp )
        
        rbnDtlPosGrpDict.append( rbnPosGrp )
        posiDtlDict.append( posi )
        
        #-- Aim constraint
        rbnAim = mc.createNode( 'aimConstraint' , n = '%s%sRbnDtl%s_aimCons' %( name , i , side ))
        
        mc.setAttr( '%s.a' %rbnAim , aimVec[0] , aimVec[1] , aimVec[2] )
        mc.setAttr( '%s.u' %rbnAim , upVec[0] , upVec[1] , upVec[2] )
        
        mc.connectAttr( '%s.n' %posi , '%s.wu' %rbnAim )
        mc.connectAttr( '%s.tv' %posi , '%s.tg[0].tt' %rbnAim )
        mc.connectAttr( '%s.cr' %rbnAim , '%s.r' %rbnPosGrp )
        
        rbnDtlAimDict.append( rbnAim )
        mc.parent( rbnAim , rbnPosGrp )
        
        mc.select( cl = True )
        
        #-- Joint
        rbnJnt = core.addJnt( '%s%sRbnDtl%s_jnt' %( name , i , side ))
        rbnJntZro = core.addGrp( rbnJnt )
        
        rbnDtlJntDict.append( rbnJnt )
        rbnDtlJntZroDict.append( rbnJntZro )
        
        #-- Controls
        rbnDtlCtrl = core.addCtrl( '%s%sRbnDtl%s_ctrl' %( name , i , side ) , 'circle' , 'cyan' , jnt = False )
        rbnDtlCtrlZro = core.addGrp( rbnDtlCtrl )
        rbnDtlCtrlGmbl = core.addGimbal( rbnDtlCtrl )
        core.snap( rbnJntZro , rbnDtlCtrlZro )
        
        rbnDtlCtrlDict.append( rbnDtlCtrl )
        rbnDtlCtrlGmblDict.append( rbnDtlCtrlGmbl )
        rbnDtlCtrlZroDict.append( rbnDtlCtrlZro )
        
        #-- Shape
        for ctrl in rbnDtlCtrl , rbnDtlCtrlGmbl :
            core.scaleShape( ctrl , charSize )
            core.rotateShape( ctrl , rotate[0] , rotate[1] , rotate[2] )
        
        #-- Slide
        if core.getExists( '%sRbnDtlSlideAmp%s_mdv' %( name , side )) :
            slideMdv = '%sRbnDtlSlideAmp%s_mdv' %( name , side )
        else :
            slideMdv = core.multiplyDivide( '%sRbnDtlSlideAmp%s_mdv' %( name , side ))
            mc.connectAttr( '%s.slide' %attrCtrl , '%s.i1x' %slideMdv )
            mc.setAttr( '%s.i2x' %slideMdv , 0.1 )
        
        slidePma = core.plusMinusAverage( '%s%sRbnDtlSlide%s_pma' %( name , i , side ))
        slideCmp = core.clamp( '%s%sRbnDtlSlide%s_cmp' %( name , i , side ))
        
        core.addAttr( slidePma , 'constant' )
        mc.connectAttr( '%s.constant' %slidePma , '%s.i1[0]' %slidePma )
        mc.connectAttr( '%s.ox' %slideMdv , '%s.i1[1]' %slidePma )
        mc.connectAttr( '%s.o1' %slidePma , '%s.ipr' %slideCmp )
        mc.connectAttr( '%s.opr' %slideCmp , '%s.parameterV' %posi )
        mc.setAttr( '%s.maxR' %slideCmp , 1 )
        
        slidePmaDict.append( slidePma )
        
        #-- Rig process
        core.parCons( 0 , rbnPosGrp , rbnDtlCtrlZro )
        core.parCons( 0 , rbnDtlCtrlGmbl , rbnJntZro )
        
        if 'x' in ax :
            sk = 'x'
        elif 'y' in ax :
            sk = 'y'
        elif 'z' in ax :
            sk = 'z'
            
        mc.scaleConstraint( rbnDtlCtrlGmbl , rbnJntZro , skip = sk )
    
    #-- Math position
    position = float(1)/(((num-1)*numDtl))
    step = float(1)/(((num-1)*numDtl))
    
    for slidePma in slidePmaDict :
        if slidePma == slidePmaDict[0] :
            position = 0
            
        mc.setAttr ( '%s.constant' %slidePma , position )
        position += step
    
    #-- Hirerachy
    mc.parent( rbnDtlJntZroDict , rbnSkinGrp )
    mc.parent( rbnNrb , rbnDtlPosGrpDict , rbnStillGrp )
    mc.parent( attrCtrlZro , rbnRootCtrlZro , rbnEndCtrlZro , rbnCtrlZroDict , rbnMainGrp )
    mc.parent( rbnDtlCtrlZroDict , rbnDetailGrp )
    mc.parent( rbnMainGrp , rbnDetailGrp , rbnAnimGrp )
    
    #-- Cleanup
    for grp in ( rbnSkinGrp , rbnStillGrp , rbnDetailGrp ) :
        core.setLock( grp , True , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
        
    for grp in rbnAimDict :
        core.setVal( '%s.v' %grp , 0 )
        
    for dicts in ( rbnJntDict , rbnAimDict , rbnDtlCtrlZroDict , rbnDtlJntZroDict ) :
        for grp in dicts :
            core.setLock( grp , True , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
            
    core.setVal( '%s.v' %rbnNrb , 0 )
    
    for ctrl in rbnCtrlDict :
        core.setLockHide( ctrl , 'sx' , 'sy' , 'sz' , 'v' )
    
    core.setLockHide( attrCtrl , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
    
    mc.select( cl = True )