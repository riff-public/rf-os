import maya.cmds as mc
import maya.mel as mm
import ppmel.core as core
import re
reload( core )


def addFkRig( name = '' ,
              tmpJnt = [] ,
              parent = '' ,
              animGrp = 'anim_Grp' ,
              axis = 'y' ,
              shape = 'cube' ,
              color = 'red' ,
              side = '' ) :
    
    #-- Info
    size = len(tmpJnt)
    
    #-- Group
    fkRigGrp = core.transform( '%sRig_%s_Grp' %( name , side ))
    core.parCons( 0 , parent , fkRigGrp )
    
    #-- Main fk rig--#
    if size == 1 :
        #-- Joint
        prefix = tmpJnt[0].split('tmpJnt')[0]
        fkJnt = core.addJnt( '%sJnt' %prefix , tmpJnt[0] )
        core.parent( fkJnt , parent )
        
        #-- Controls
        fkCtrl = core.addCtrl( '%sCtrl' %prefix , shape , color , jnt = False )
        fkGmbl = core.addGimbal( fkCtrl )
        fkZro = core.addGrp( fkCtrl )
        fkOfst = core.addGrp( fkCtrl , 'Ofst' )
        core.snap( fkJnt , fkZro )
        
        #-- Rig process
        core.parCons( 0 , fkGmbl , fkJnt )
        core.sclCons( 0 , fkGmbl , fkJnt )
        core.addLocalWorld( fkCtrl , 'anim_Grp' , fkRigGrp , fkZro , 'orient' )
        
        #-- Hierarchy
        core.parent( fkZro , fkRigGrp , animGrp )
        
        #-- Cleanup
        for grp in ( fkZro , fkOfst , fkRigGrp ) :
            core.setLock( grp , True , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
        
        for ctrl in ( fkCtrl , fkGmbl ) :
            core.setLockHide( ctrl , 'v' )
            
    else :
        fkJntArray = []
        fkCtrlArray = []
        fkGmblArray = []
        fkZroArray = []
        fkOfstArray = []
        
        for i in range(0,size) :
            #-- Joint
            prefix = tmpJnt[i].split('tmpJnt')[0]
            fkJnt = core.addJnt( '%sJnt' %prefix , tmpJnt[i] )
            fkJntArray.append(fkJnt)
            
            if not i == size-1 :
                #-- Controls
                fkCtrl = core.addCtrl( '%sCtrl' %prefix , shape , color , jnt = False )
                fkGmbl = core.addGimbal( fkCtrl )
                fkZro = core.addGrp( fkCtrl )
                fkOfst = core.addGrp( fkCtrl , 'Ofst' )
                core.snap( fkJnt , fkZro )
                
                fkCtrlArray.append(fkCtrl)
                fkGmblArray.append(fkGmbl)
                fkZroArray.append(fkZro)
                fkOfstArray.append(fkOfst)
            
            #-- Hierarchy
            if i == 0 :
                if not i == size-1 :
                    core.parent( fkZro , fkRigGrp , animGrp )
                    
                core.parent( fkJnt , parent )
            else :
                if not i == size-1 :
                    core.parent( fkZro , fkGmblArray[i-1] )
                    
                core.parent( fkJnt , fkJntArray[i-1] )
            
            #-- Rig process
            if not i == size-1 :
                core.parCons( 0 , fkGmbl , fkJnt )
            
            if not i == 0 :
                if not i == size-1 :
                    core.addFkStretch( fkCtrlArray[i-1] , fkOfst , axis )
                else :
                    core.addFkStretch( fkCtrlArray[i-1] , fkJnt , axis )
            
            axSq = []
            for attr in ( 'x' , 'y' , 'z' ) :
                if not attr == axis :
                    axSq.append(attr)
                    
            core.addSquash( fkCtrl , fkJnt , ax = ( 's%s' %axSq[0] , 's%s' %axSq[1] ))
            
        core.addLocalWorld( fkCtrlArray[0] , 'anim_Grp' , fkRigGrp , fkZroArray[0] , 'orient' )
            
        #-- Cleanup
        for zro in fkZroArray :
            core.setLock( zro , True , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
        
        for ofst in fkOfstArray :
            core.setLock( ofst , True , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
        
        for jnt in fkJntArray :
            core.setLock( jnt , True , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
        
        for ctrl in fkCtrlArray :
            core.setLockHide( ctrl , 'sx' , 'sy' , 'sz' , 'v' )
        
        for gmbl in fkGmblArray :
            core.setLockHide( gmbl , 'sx' , 'sy' , 'sz' , 'v' )
        
    mc.select( cl = True )


def addRivet( name = '' , side = '' ) :
    sel = mc.ls( sl = True )
    
    if side == '' :
        side = '_'
    else :
        side = '_%s_' %side
    
    if sel :
        obj = sel[0].split('.')[0]
        type = core.getNodeType( sel[0] )
        
        if type == 'mesh' :
            edge = mc.filterExpand( sm = 32 )
            
            u = 0.5
            v = 0.5
            edgeNum = []
            for each in edge :
                e = re.findall(r'\d+', '%s\n' %each)[-1]
                edgeNum.append(e)
                
            cfme1 = core.curveFromMeshEdge( '%s1Rivet%sCfme' %( name , side ))
            core.setVal( '%s.ihi' %cfme1 , 1 )
            core.setVal( '%s.ei[0]' %cfme1 , int(edgeNum[0]))
            
            cfme2 = core.curveFromMeshEdge( '%s2Rivet%sCfme' %( name , side ))
            core.setVal( '%s.ihi' %cfme1 , 1 )
            core.setVal( '%s.ei[0]' %cfme2 , int(edgeNum[1]))
            
            loft = core.loft( '%sRivet%sLoft' %( name , side ))
            core.setVal( '%s.u' %loft , True )
            core.setVal( '%s.rsn' %loft , True )
            
            posi = core.pointOnSurfaceInfo( '%sRivet%sPosi' %( name , side ))
            core.setVal( '%s.turnOnPercentage' %posi , 1 )
            
            mc.connectAttr( '%s.w' %obj , '%s.im' %cfme1 )
            mc.connectAttr( '%s.w' %obj , '%s.im' %cfme2 )
            mc.connectAttr( '%s.oc' %cfme1 , '%s.ic[0]' %loft )
            mc.connectAttr( '%s.oc' %cfme2 , '%s.ic[1]' %loft )
            mc.connectAttr( '%s.os' %loft , '%s.is' %posi )
                
        elif type == 'nurbsSurface' :
            point = mc.filterExpand( sm = 41 )[0]
            
            void = point.split('.uv')[-1]
            u , v = void.split('][')
            u = u.split('[')[-1]
            v = v.split(']')[0]
            
            posi = core.pointOnSurfaceInfo( '%sRivet%sPosi' %( name , side ))
            core.setVal( '%s.turnOnPercentage' %posi , 0 )
            core.setVal( '%s.parameterU' %posi , float(u) )
            core.setVal( '%s.parameterV' %posi , float(v) )
            
            mc.connectAttr( '%s.ws' %obj , '%s.is' %posi )
                    
        else :
            mc.error( "No edges or point selected" )
        
    grp = core.transform( '%sRivetZro%sGrp' %( name , side ))
    loc = mc.spaceLocator( n = '%sRivet%sLoc' %( name , side ))[0]
    
    for attr in ( 'parameterU' , 'parameterV' , 'offsetX' , 'offsetY' , 'offsetZ' , 'scaleLocator' ) :
        core.addAttr( loc , attr )
        
    core.setVal( '%s.parameterU' %loc , float(u))
    core.setVal( '%s.parameterV' %loc , float(v))
    core.setVal( '%s.scaleLocator' %loc , 1 )
    
    aimCons = mc.createNode( 'aimConstraint' , n = '%sRivet%saimCons' %( name , side ))
    core.setVal( '%s.ax' %aimCons , 0 )
    core.setVal( '%s.ay' %aimCons , 1 )
    core.setVal( '%s.az' %aimCons , 0 )
    core.setVal( '%s.ux' %aimCons , 0 )
    core.setVal( '%s.uy' %aimCons , 0 )
    core.setVal( '%s.uz' %aimCons , 1 )
    
    mc.connectAttr( '%s.position' %posi , '%s.translate' %loc )
    mc.connectAttr( '%s.n' %posi , '%s.tg[0].tt' %aimCons )
    mc.connectAttr( '%s.tv' %posi , '%s.wu' %aimCons )
    mc.connectAttr( '%s.crx' %aimCons , '%s.rx' %loc )
    mc.connectAttr( '%s.cry' %aimCons , '%s.ry' %loc )
    mc.connectAttr( '%s.crz' %aimCons , '%s.rz' %loc )
    
    mc.connectAttr( '%s.parameterU' %loc , '%s.parameterU' %posi )
    mc.connectAttr( '%s.parameterV' %loc , '%s.parameterV' %posi )
    mc.connectAttr( '%s.offsetX' %loc , '%s.offsetX' %aimCons )
    mc.connectAttr( '%s.offsetY' %loc , '%s.offsetY' %aimCons )
    mc.connectAttr( '%s.offsetZ' %loc , '%s.offsetZ' %aimCons )
    
    mc.connectAttr( '%s.scaleLocator' %loc , '%sShape.localScaleX' %loc )
    mc.connectAttr( '%s.scaleLocator' %loc , '%sShape.localScaleY' %loc )
    mc.connectAttr( '%s.scaleLocator' %loc , '%sShape.localScaleZ' %loc )
    
    core.parent( aimCons , loc , grp )
    
    for each in ( grp , loc , aimCons ) :
        core.setLockHide( each , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
        
    for attr in ( 'offsetX' , 'offsetY' , 'offsetZ' ) :
        core.setLockHide( aimCons , attr )
    
    for attr in ( 'localPositionX' , 'localPositionY' , 'localPositionZ' , 'localScaleX' , 'localScaleY' , 'localScaleZ' ) :
        core.setLockHide( '%sShape' %loc , attr )
        
    mc.select( cl = True )


def addDtlCtrl( ctrl = '' , side = '' , shape = 'cube' , orient = '' , charSize = 1 ) :
    sel = mc.ls( sl = True )
    
    if side == '' :
        side = '_'
    else :
        side = '_%s_' %side
    
    if sel :
        obj = sel[0].split('.')[0]
        edge = mc.filterExpand( sm = 32 )
            
        #-- Joint
        jnt = core.addJnt( '%sDtl%sJnt' %( ctrl , side ))
        jntZro = core.addGrp( jnt )
        
        #-- Controls
        dtlCtrl = core.addCtrl( '%sDtl%sCtrl' %( ctrl , side ) , shape , 'red' , jnt = False )
        dtlZro = core.addGrp( dtlCtrl )
        dtlOfst = core.addGrp( dtlCtrl , 'Ofst' )
        
        for attr in ( 'parameterU' , 'parameterV' , 'offsetX' , 'offsetY' , 'offsetZ' ) :
            core.addAttr( '%sShape' %dtlCtrl , attr )
            
        core.setVal( '%sShape.parameterU' %dtlCtrl , 0.5 )
        core.setVal( '%sShape.parameterV' %dtlCtrl , 0.5 )
        
        #-- Shape
        core.scaleShape( dtlCtrl , charSize * 0.1 )
        
        #-- Info
        edgeNum = []
        for each in edge :
            e = re.findall(r'\d+', '%s\n' %each)[-1]
            edgeNum.append(e)
        
        #-- Create node
        cfme1 = core.curveFromMeshEdge( '%s1Dtl%sCfme' %( ctrl , side ))
        core.setVal( '%s.ei[0]' %cfme1 , int(edgeNum[0]))
        
        cfme2 = core.curveFromMeshEdge( '%s2Dtl%sCfme' %( ctrl , side ))
        core.setVal( '%s.ei[0]' %cfme2 , int(edgeNum[1]))
        
        loft = core.loft( '%sDtl%sLoft' %( ctrl , side ))
        core.setVal( '%s.u' %loft , True )
        core.setVal( '%s.rsn' %loft , True )
        
        posi = core.pointOnSurfaceInfo( '%sDtl%sPosi' %( ctrl , side ))
        core.setVal( '%s.turnOnPercentage' %posi , 1 )
        
        mdv = core.multiplyDivide( '%sDtl%sMdv' %( ctrl , side ))
        mc.setAttr( '%s.i2x' %mdv , -1 )
        mc.setAttr( '%s.i2y' %mdv , -1 )
        mc.setAttr( '%s.i2z' %mdv , -1 )
        
        #-- Connect attribute
        mc.connectAttr( '%s.w' %obj , '%s.im' %cfme1 )
        mc.connectAttr( '%s.w' %obj , '%s.im' %cfme2 )
        mc.connectAttr( '%s.oc' %cfme1 , '%s.ic[0]' %loft )
        mc.connectAttr( '%s.oc' %cfme2 , '%s.ic[1]' %loft )
        mc.connectAttr( '%s.os' %loft , '%s.is' %posi )
        
        mc.connectAttr( '%s.position' %posi , '%s.t' %dtlZro )
        
        mc.connectAttr( '%s.t' %dtlCtrl , '%s.i1' %mdv )
        mc.connectAttr( '%s.o' %mdv , '%s.t' %dtlOfst )
        
        mc.connectAttr( '%sShape.parameterU' %dtlCtrl , '%s.parameterU' %posi )
        mc.connectAttr( '%sShape.parameterV' %dtlCtrl , '%s.parameterV' %posi )
        mc.connectAttr( '%sShape.offsetX' %dtlCtrl , '%s.rx' %dtlOfst )
        mc.connectAttr( '%sShape.offsetY' %dtlCtrl , '%s.ry' %dtlOfst )
        mc.connectAttr( '%sShape.offsetZ' %dtlCtrl , '%s.rz' %dtlOfst )
        
        core.snap( dtlZro , jntZro )
        
        mc.connectAttr( '%s.t' %dtlCtrl , '%s.t' %jnt )
        mc.connectAttr( '%s.r' %dtlCtrl , '%s.r' %jnt )
        mc.connectAttr( '%s.s' %dtlCtrl , '%s.s' %jnt )
        
        core.oriCons( 1 , orient , dtlZro )
        
        #-- Cleanup
        for grp in ( dtlZro , dtlOfst , jntZro , jnt ) :
            core.setLock( grp , True , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )


def addCorrectiveBsh( upVec = 'x' , aim = 'y' , defGrp = 'def_Grp' ) :
    pos , aimObj , parent = mc.ls( sl = True )
    prefix = pos.split( '_' )[0]
    side = core.getSide( pos )
    scale = []
    
    for i in ( 'x' , 'y' , 'z' ) :
        if not i == twist :
            scale.append(i)
            
    if '-' in upVec :
        upVecVal = -1.0
    else :
        upVecVal = 1.0
        
    if '-' in aim :
        aimVecVal = -1.0
    else :
        aimVecVal = 1.0
    
    #-- Group
    posGrp = core.transform( '%sFixPos%s_Grp' %( prefix , side ))
    parentGrp = core.transform( '%sFixParent%s_Grp' %( prefix , side ))
    
    #-- Joint
    fixJnt = core.addJnt( '%sFix%s_Jnt' %( prefix , side ))
    core.parent( fixJnt , posGrp )
    
    #-- Create Node
    rev = core.reverse( '%sFix%s_Rev' %( prefix , side ))
    
    sumMdv = core.multiplyDivide( '%sFixSclSum%s_Mdv' %( prefix , side ))
    positiveMdv = core.multiplyDivide( '%sFixSclPositive%s_Mdv' %( prefix , side ))
    nagativeMdv = core.multiplyDivide( '%sFixSclNagative%s_Mdv' %( prefix , side ))
    
    positiveCmp = core.clamp( '%sFixSclPositive%s_Cmp' %( prefix , side ))
    nagativeCmp = core.clamp( '%sFixSclNagative%s_Cmp' %( prefix , side ))
    
    sumPma = core.plusMinusAverage( '%sFixSclSum%s_Pma' %( prefix , side ))
    
    scl1Dbl = core.addDoubleLinear( '%sFixScl%s%s_Dbl' %( prefix , scale[0] , side ))
    scl2Dbl = core.addDoubleLinear( '%sFixScl%s%s_Dbl' %( prefix , scale[1] , side ))
    
    blendOriCons = mc.createNode( 'orientConstraint' , n = '%sFixBlend%s_oriCons' %( prefix , side ))
    drive1AimCons = mc.createNode( 'aimConstraint' , n = '%sFixDrive1%s_aimCons' %( prefix , side ))
    drive2AimCons = mc.createNode( 'aimConstraint' , n = '%sFixDrive2%s_aimCons' %( prefix , side ))
    
    #-- Hierarchy
    mc.parent( posGrp , defGrp )
    mc.parent( parentGrp , posGrp )
    mc.parent( blendOriCons , drive1AimCons , drive2AimCons , fixJnt )
    
    core.snap( pos , posGrp )
    
    #-- Rig Process
    core.parCons( 1 , parent , posGrp )
    core.sclCons( 1 , parent , posGrp )
    
    #-- Add Attribute
    core.addAttr( fixJnt , 'aim' )
    core.addAttr( fixJnt , 'scaleShape' )
    
    core.addAttr( blendOriCons , '%sFixDrive1%s_aimConsW0' %( prefix , side ))
    core.addAttr( blendOriCons , '%sFixDrive2%s_aimConsW1' %( prefix , side ))
    
    #-- Set Attribute
    for paremeter in ( 'x' , 'y' ,'z' ) :
        core.setVal( '%s.i2%s' %( nagativeMdv , paremeter ) , 0.01 )
        core.setVal( '%s.i2%s' %( positiveMdv , paremeter ) , -0.01 )
        
    for paremeter in ( 'r' , 'g' ,'b' ) :
        core.setVal( '%s.mx%s' %( nagativeCmp , paremeter ) , 180 )
        core.setVal( '%s.mx%s' %( positiveCmp , paremeter ) , 180 )
        
    for each in ( scl1Dbl , scl2Dbl ) :
        core.setVal( '%s.i1' %each , 1 )
        
    for aimCons in ( drive1AimCons , drive2AimCons ) :
        for paremeter in ( 'x' , 'y' ,'z' ) :
            if paremeter == aim :
                core.setVal( '%s.a%s' %( aimCons , paremeter ) , aimVecVal )
            else :
                core.setVal( '%s.a%s' %( aimCons , paremeter ) , 0 )
                
            for attr in ( 'u' , 'wu' ) :
                if paremeter == upVec :
                    core.setVal( '%s.%s%s' %( aimCons , attr , paremeter ) , upVecVal )
                else :
                    core.setVal( '%s.%s%s' %( aimCons , attr , paremeter ) , 0 )
                    
        core.setVal( '%s.wut' %aimCons , 2 )
        mc.connectAttr( '%s.worldMatrix' %posGrp , '%s.wum' %aimCons )
        
    #-- Connect Attribute
    mc.connectAttr( '%s.rx' %fixJnt , '%s.i1x' %positiveMdv )
    mc.connectAttr( '%s.ry' %fixJnt , '%s.i1y' %positiveMdv )
    mc.connectAttr( '%s.rz' %fixJnt , '%s.i1z' %positiveMdv )
    
    mc.connectAttr( '%s.rx' %fixJnt , '%s.i1x' %nagativeMdv )
    mc.connectAttr( '%s.ry' %fixJnt , '%s.i1y' %nagativeMdv )
    mc.connectAttr( '%s.rz' %fixJnt , '%s.i1z' %nagativeMdv )
    
    mc.connectAttr( '%s.o' %positiveMdv ,  '%s.ip' %positiveCmp )
    mc.connectAttr( '%s.o' %nagativeMdv ,  '%s.ip' %nagativeCmp )
    
    mc.connectAttr( '%s.op' %positiveCmp ,  '%s.i3[0]' %sumPma )
    mc.connectAttr( '%s.op' %nagativeCmp ,  '%s.i3[1]' %sumPma )
    
    mc.connectAttr( '%s.o3' %sumPma ,  '%s.i1' %sumMdv)
    mc.connectAttr( '%s.scaleShape' %fixJnt , '%s.i2x' %sumMdv )
    mc.connectAttr( '%s.scaleShape' %fixJnt , '%s.i2y' %sumMdv )
    mc.connectAttr( '%s.scaleShape' %fixJnt , '%s.i2z' %sumMdv )
    
    mc.connectAttr( '%s.o%s' %( sumMdv , scale[0] ) , '%s.i2' %scl1Dbl )
    mc.connectAttr( '%s.o%s' %( sumMdv , scale[1] ) , '%s.i2' %scl2Dbl )
    
    mc.connectAttr( '%s.o' %scl1Dbl , '%s.s%s' %( fixJnt , scale[0] ) )
    mc.connectAttr( '%s.o' %scl2Dbl , '%s.s%s' %( fixJnt , scale[1] ) )
    
    mc.connectAttr( '%s.t' %aimObj , '%s.target[0].targetTranslate' %drive1AimCons )
    mc.connectAttr( '%s.parentMatrix' %aimObj , '%s.target[0].targetParentMatrix' %drive1AimCons )
    
    mc.connectAttr( '%s.t' %fixJnt , '%s.constraintTranslate' %drive1AimCons )
    mc.connectAttr( '%s.parentInverseMatrix' %fixJnt , '%s.constraintParentInverseMatrix' %drive1AimCons )
    
    mc.connectAttr( '%s.t' %fixJnt , '%s.target[0].targetTranslate' %drive2AimCons )
    mc.connectAttr( '%s.parentMatrix' %fixJnt , '%s.target[0].targetParentMatrix' %drive2AimCons )
    
    mc.connectAttr( '%s.t' %parentGrp , '%s.constraintTranslate' %drive2AimCons )
    mc.connectAttr( '%s.parentInverseMatrix' %parentGrp , '%s.constraintParentInverseMatrix' %drive2AimCons )
    
    mc.connectAttr( '%s.constraintRotateX' %drive1AimCons , '%s.target[0].targetRotateX' %blendOriCons )
    mc.connectAttr( '%s.constraintRotateY' %drive1AimCons , '%s.target[0].targetRotateY' %blendOriCons )
    mc.connectAttr( '%s.constraintRotateZ' %drive1AimCons , '%s.target[0].targetRotateZ' %blendOriCons )
    
    mc.connectAttr( '%s.constraintRotateX' %drive2AimCons , '%s.target[1].targetRotateX' %blendOriCons )
    mc.connectAttr( '%s.constraintRotateY' %drive2AimCons , '%s.target[1].targetRotateY' %blendOriCons )
    mc.connectAttr( '%s.constraintRotateZ' %drive2AimCons , '%s.target[1].targetRotateZ' %blendOriCons )
    
    mc.connectAttr( '%s.constraintRotateX' %blendOriCons , '%s.rotateX' %fixJnt )
    mc.connectAttr( '%s.constraintRotateY' %blendOriCons , '%s.rotateY' %fixJnt )
    mc.connectAttr( '%s.constraintRotateZ' %blendOriCons , '%s.rotateZ' %fixJnt )
    
    mc.connectAttr( '%s.aim' %fixJnt , '%s.ix' %rev )
    
    mc.connectAttr( '%s.aim' %fixJnt , '%s.%sFixDrive1%s_aimConsW0' %( blendOriCons ,prefix , side ))
    mc.connectAttr( '%s.ox' %rev , '%s.%sFixDrive2%s_aimConsW1' %( blendOriCons ,prefix , side ))
    mc.connectAttr( '%s.jointOrient' %fixJnt , '%s.constraintJointOrient' %blendOriCons )
    mc.connectAttr( '%s.rotateOrder' %fixJnt , '%s.constraintRotateOrder' %blendOriCons )
    
    mc.connectAttr( '%s.%sFixDrive1%s_aimConsW0' %( blendOriCons ,prefix , side ) , '%s.target[0].targetWeight' %blendOriCons )
    mc.connectAttr( '%s.%sFixDrive2%s_aimConsW1' %( blendOriCons ,prefix , side ) , '%s.target[1].targetWeight' %blendOriCons )
    
    #-- Cleanup
    for grp in ( posGrp , parentGrp ) :
        core.setLock( grp , True , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
        
    for cons in ( blendOriCons , drive1AimCons , drive1AimCons ) :
        core.setLockHide( grp , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'ox' , 'oy' , 'oz' , 'v' )
        
    core.setLockHide( fixJnt , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
    
    core.setVal( '%s.aim' %fixJnt , 0.5 )
    core.setVal( '%s.scaleShape' %fixJnt , 2.5 )
    
    mc.select( cl = True )