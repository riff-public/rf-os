import maya.cmds as mc
import maya.mel as mm
import ppmel.core as core
reload( core )


def rigSpine( spine1TmpJnt = 'spine1_tmpJnt' ,
              spine2TmpJnt = 'spine2_tmpJnt' ,
              spine3TmpJnt = 'spine3_tmpJnt' ,
              spine4TmpJnt = 'spine4_tmpJnt' ,
              spine5TmpJnt = 'spine5_tmpJnt' ,
              spine6TmpJnt = 'spine6_tmpJnt' ,
              parent = 'root_jnt' ,
              animGrp = 'anim_grp' ,
              skinGrp = 'skin_grp' ,
              jntGrp = 'jnt_grp' ,
              ikhGrp = 'ikh_grp' ,
              stillGrp = 'still_grp' ,
              charSize = 3 ) :
    
    #-- Group
    spineRigGrp = core.transform( 'spineRig_grp' )
    spineStillGrp = core.transform( 'spineStill_grp' )
    spineJntGrp = core.transform( 'spineJnt_grp' )
    spineFkRigGrp = core.transform( 'spineFkRig_grp' )
    core.parCons( 0 , parent , spineRigGrp )
    
    #-- Joint
    spine1PosJnt = core.addJnt( 'spine1Pos_jnt' , spine1TmpJnt )
    spine2PosJnt = core.addJnt( 'spine2Pos_jnt' , spine2TmpJnt )
    spine3PosJnt = core.addJnt( 'spine3Pos_jnt' , spine3TmpJnt )
    spine4PosJnt = core.addJnt( 'spine4Pos_jnt' , spine4TmpJnt )
    spine5PosJnt = core.addJnt( 'spine5Pos_jnt' , spine5TmpJnt )
    spine6PosJnt = core.addJnt( 'spine6Pos_jnt' , spine6TmpJnt )
    
    for each in ( spine1PosJnt , spine2PosJnt , spine3PosJnt , spine4PosJnt , spine5PosJnt , spine6PosJnt ) :
        core.setVal( '%s.rz' %each , 90 )
        core.freeze( each )
    
    spine1Jnt = core.addJnt( 'spine1_jnt' , spine1PosJnt )
    spine2Jnt = core.addJnt( 'spine2_jnt' , spine2PosJnt )
    spine3Jnt = core.addJnt( 'spine3_jnt' , spine3PosJnt )
    spine4Jnt = core.addJnt( 'spine4_jnt' , spine4PosJnt )
    spine5Jnt = core.addJnt( 'spine5_jnt' , spine5PosJnt )
    spine6Jnt = core.addJnt( 'spine6_jnt' , spine6PosJnt )
    
    core.parent( spine6PosJnt , spine5PosJnt , spine4PosJnt , spine3PosJnt , spine2PosJnt , spine1PosJnt )
    core.parent( spine6Jnt , parent )
    core.parent( spine5Jnt , spine5PosJnt )
    core.parent( spine4Jnt , spine4PosJnt )
    core.parent( spine3Jnt , spine3PosJnt )
    core.parent( spine2Jnt , spine2PosJnt )
    core.parent( spine1Jnt , spine1PosJnt )
    
    #-- Controls
    spine1FkCtrl = core.addCtrl( 'spine1Fk_ctrl' , 'circle' , 'red' , jnt = True )
    spine1FkGmbl = core.addGimbal( spine1FkCtrl )
    spine1FkZro = core.addGrp( spine1FkCtrl )
    core.snapPoint( spine1TmpJnt , spine1FkZro )
    core.snapJntOrient( spine1TmpJnt , spine1FkCtrl )
    
    spine2FkCtrl = core.addCtrl( 'spine2Fk_ctrl' , 'circle' , 'red' , jnt = True )
    spine2FkGmbl = core.addGimbal( spine2FkCtrl )
    spine2FkZro = core.addGrp( spine2FkCtrl )
    core.snapPoint( spine3TmpJnt , spine4TmpJnt , spine2FkZro )
    core.snapJntOrient( spine3TmpJnt , spine2FkCtrl )
    
    spine3FkCtrl = core.addCtrl( 'spine3Fk_ctrl' , 'circle' , 'red' , jnt = True )
    spine3FkGmbl = core.addGimbal( spine3FkCtrl )
    spine3FkZro = core.addGrp( spine3FkCtrl )
    core.snapPoint( spine6TmpJnt , spine3FkZro )
    core.snapJntOrient( spine6TmpJnt , spine3FkCtrl )
    
    spineRbnCtrl = core.addCtrl( 'spineRbn_ctrl' , 'circle' , 'yellow' , jnt = True )
    spineRbnGmbl = core.addGimbal( spineRbnCtrl )
    spineRbnZro = core.addGrp( spineRbnCtrl )
    core.snapPoint( spine3TmpJnt , spine4TmpJnt , spineRbnZro )
    core.snapJntOrient( spine3TmpJnt , spineRbnCtrl )
        
    #-- Shape
    for ctrl in ( spine1FkCtrl , spine1FkGmbl , spine2FkCtrl , spine2FkGmbl , spine3FkCtrl , spine3FkGmbl ) :
        core.scaleShape( ctrl , charSize * 1.8 )
    
    for ctrl in ( spineRbnCtrl , spineRbnGmbl ) :
        core.scaleShape( ctrl , charSize * 1.4 )
    
    #-- Rotate order
    for jnt in ( spine1PosJnt , spine2PosJnt , spine3PosJnt , spine4PosJnt , spine5PosJnt , spine6PosJnt , spine1Jnt , spine2Jnt , spine3Jnt , spine4Jnt , spine5Jnt , spine6Jnt ) :
        core.setRotateOrder( jnt , 'xzy' )
        
    for ctrl in ( spine1FkCtrl , spine2FkCtrl , spine3FkCtrl , spineRbnCtrl ) :
        core.setRotateOrder( ctrl , 'yzx' )
        
    #-- Rig process
    ikh = core.addIkh( 'spine' , 'ikSplineSolver' , spine1PosJnt , spine6PosJnt )
    
    ikhSpine = ikh['ikh']
    ikhCuv = ikh['ikhCuv']
    ikhZro = ikh['ikhZro']
    
    #-- Twist
    mc.setAttr( '%s.dTwistControlEnable' %ikhSpine , 1 )
    mc.setAttr( '%s.dWorldUpType' %ikhSpine , 4 )
    mc.setAttr( '%s.dWorldUpAxis' %ikhSpine , 3 )
    mc.setAttr( '%s.dwuv' %ikhSpine , 0 , 0 , 1 )
    mc.setAttr( '%s.dwve' %ikhSpine , 0 , 0 , 1 )
    
    mc.connectAttr( '%s.worldMatrix' %spine1FkGmbl , '%s.dWorldUpMatrix' %ikhSpine )
    mc.connectAttr( '%s.worldMatrix' %spine3FkGmbl , '%s.dWorldUpMatrixEnd' %ikhSpine )
    
    #-- Create cluster
    spineLwrJnt = core.addJnt( 'spineLwr_jnt' , spine1TmpJnt )
    spineCenJnt = core.addJnt( 'spineCen_jnt' , spine3TmpJnt )
    spineUprJnt = core.addJnt( 'spineUpr_jnt' , spine6TmpJnt )
    
    spineLwrJntZro = core.addGrp( spineLwrJnt )
    spineCenJntZro = core.addGrp( spineCenJnt )
    spineUprJntZro = core.addGrp( spineUprJnt )
    
    core.snapPoint( spine3TmpJnt , spine4TmpJnt , spineCenJntZro )
    mc.parent( spineLwrJntZro , spineCenJntZro , spineUprJntZro , spineJntGrp )
    
    spineCuvSkc = mc.skinCluster( spineLwrJnt , spineCenJnt , spineUprJnt , ikhCuv , dr = 7 , mi = 2 , n = 'spineCuv_skc' )[0]
    
    mc.skinPercent( spineCuvSkc , '%s.cv[0]' %ikhCuv , tv = [ spineLwrJnt , 1 ] )
    mc.skinPercent( spineCuvSkc , '%s.cv[1]' %ikhCuv , tv = [ (spineCenJnt , 0.6) , (spineLwrJnt , 0.3) , (spineUprJnt , 0.1) ] )
    mc.skinPercent( spineCuvSkc , '%s.cv[2]' %ikhCuv , tv = [ (spineCenJnt , 0.6) , (spineLwrJnt , 0.1) , (spineUprJnt , 0.3) ] )
    mc.skinPercent( spineCuvSkc , '%s.cv[3]' %ikhCuv , tv = [ spineUprJnt , 1 ] )
    
    core.parCons( 0 , spineRbnGmbl , spineCenJntZro )
    core.parCons( 0 , spine3FkGmbl , spineUprJntZro )
    core.parCons( 0 , spineUprJnt , spine6Jnt )
    
    '''
    spine1Clstr = mc.rename( mc.cluster( '%s.cv[0]' %ikhCuv )[1] , 'spine1_clstr' )
    spine2Clstr = mc.rename( mc.cluster( '%s.cv[1]' %ikhCuv )[1] , 'spine2_clstr' )
    spine3Clstr = mc.rename( mc.cluster( '%s.cv[2:3]' %ikhCuv )[1] , 'spine3_clstr' )
    
    core.parent( spine1Clstr , spine1FkGmbl )
    core.parent( spine2Clstr , spineRbnGmbl )
    core.parent( spine3Clstr , spine3FkGmbl )
    '''
    
    #-- Stretch
    mc.addAttr( spine1FkCtrl , ln = 'spineLength' , k = True  , dv = 1 )
    mc.setAttr( '%s.spineLength' %spine1FkCtrl , cb = True )
    core.addAttr( spine1FkCtrl , 'autoStretch' , 0 , 1 , 0 )
    core.setLock( spine1FkCtrl , True , 'spineLength' )
    core.setVal( '%s.autoStretch' %spine1FkCtrl , 1 )
    
    cif = core.curveInfo( 'spine_cif' )
    mdvGblScl = core.multiplyDivide( 'spineGblScl_mdv' )
    
    mdvAutoStrt = core.multiplyDivide( 'spineAutoStrt_mdv' )
    bclAutoStrt = core.blendColors( 'spineAutoStrt_bcl' )
    mc.setAttr( '%s.op' %mdvAutoStrt , 2 )
    
    mc.connectAttr( '%sShape.worldSpace' %ikhCuv , '%s.inputCurve' %cif )
    mc.connectAttr( '%s.arcLength' %cif , '%s.c1r' %bclAutoStrt )
    mc.connectAttr( '%s.opr' %bclAutoStrt , '%s.i1x' %mdvAutoStrt )
    mc.connectAttr( '%s.autoStretch' %spine1FkCtrl , '%s.b' %bclAutoStrt)
    
    len = mc.getAttr( '%s.arcLength' %cif )
    mc.setAttr( '%s.i2x' %mdvGblScl , len )
    
    mc.connectAttr( 'master_ctrl.globalScale'  , '%s.i1x' %mdvGblScl )
    mc.setAttr( '%s.i2x' %mdvGblScl , len )
    
    mc.connectAttr( '%s.ox' %mdvGblScl , '%s.i2x' %mdvAutoStrt )
    mc.connectAttr( '%s.ox' %mdvGblScl , '%s.c2r' %bclAutoStrt )
    
    for posJnt in ( spine1PosJnt , spine2PosJnt , spine3PosJnt , spine4PosJnt , spine5PosJnt , spine6PosJnt ) :
        mc.connectAttr( '%s.ox' %mdvAutoStrt , '%s.sx' %posJnt )
    
    core.connectAttr( '%s.ox' %mdvAutoStrt , '%s.spineLength' %spine1FkCtrl )
    
    #-- Squash
    core.addAttr( spine1FkCtrl , 'autoSquash' , 0 , 1 , 0 )
    core.addAttr( spine1FkCtrl , 'ampSquash' )
    mc.setAttr( '%s.ampSquash' %spine1FkCtrl , 1 )
    core.setLock( spine1FkCtrl , True , 'ampSquash' )
    
    for ctrl in ( spine1FkCtrl , spine2FkCtrl , spine3FkCtrl ) :
        core.addAttr( ctrl , 'squash' )
    
    # Auto squash
    pmaLenAutoSqsh = core.plusMinusAverage( 'spineLenAutoSqsh_pma' )
    core.addAttr( pmaLenAutoSqsh , 'length' )
    core.addAttr( pmaLenAutoSqsh , 'default' )
    
    mc.connectAttr( '%s.length' %pmaLenAutoSqsh , '%s.i1[0]' %pmaLenAutoSqsh )
    mc.connectAttr( '%s.default' %pmaLenAutoSqsh , '%s.i1[1]' %pmaLenAutoSqsh )
    
    mdvAutoSqshRev = core.multiplyDivide( 'spineRevLenAutoSqsh_pma' )
    mc.connectAttr( 'master_ctrl.globalScale' , '%s.i1x' %mdvAutoSqshRev )
    mc.connectAttr( '%s.ox' %mdvAutoSqshRev , '%s.default' %pmaLenAutoSqsh )
    mc.setAttr( '%s.i2x' %mdvAutoSqshRev , -1 )
    
    
    mdv1MulAutoSqsh = core.multiplyDivide( 'spine1MulAutoSqsh_mdv' )
    mdv2MulAutoSqsh = core.multiplyDivide( 'spine2MulAutoSqsh_mdv' )
    mdv3MulAutoSqsh = core.multiplyDivide( 'spine3MulAutoSqsh_mdv' )
    mdv4MulAutoSqsh = core.multiplyDivide( 'spine4MulAutoSqsh_mdv' )
    mdv5MulAutoSqsh = core.multiplyDivide( 'spine5MulAutoSqsh_mdv' )
    mdv6MulAutoSqsh = core.multiplyDivide( 'spine6MulAutoSqsh_mdv' )
    
    bcl1AutoSqsh = core.blendColors( 'spine1AutoSqsh_bcl' )
    bcl2AutoSqsh = core.blendColors( 'spine2AutoSqsh_bcl' )
    bcl3AutoSqsh = core.blendColors( 'spine3AutoSqsh_bcl' )
    bcl4AutoSqsh = core.blendColors( 'spine4AutoSqsh_bcl' )
    bcl5AutoSqsh = core.blendColors( 'spine5AutoSqsh_bcl' )
    bcl6AutoSqsh = core.blendColors( 'spine6AutoSqsh_bcl' )
    
    mdv1AmpAutoSqsh = core.multiplyDivide( 'spine1AmpAutoSqsh_mdv' )
    mdv2AmpAutoSqsh = core.multiplyDivide( 'spine2AmpAutoSqsh_mdv' )
    mdv3AmpAutoSqsh = core.multiplyDivide( 'spine3AmpAutoSqsh_mdv' )
    mdv4AmpAutoSqsh = core.multiplyDivide( 'spine4AmpAutoSqsh_mdv' )
    mdv5AmpAutoSqsh = core.multiplyDivide( 'spine5AmpAutoSqsh_mdv' )
    mdv6AmpAutoSqsh = core.multiplyDivide( 'spine6AmpAutoSqsh_mdv' )
    
    mdv1MulSqsh = core.multiplyDivide( 'spine1MulSqsh_mdv' )
    mdv2MulSqsh = core.multiplyDivide( 'spine2MulSqsh_mdv' )
    mdv3MulSqsh = core.multiplyDivide( 'spine3MulSqsh_mdv' )
    mdv4MulSqsh = core.multiplyDivide( 'spine4MulSqsh_mdv' )
    mdv5MulSqsh = core.multiplyDivide( 'spine5MulSqsh_mdv' )
    mdv6MulSqsh = core.multiplyDivide( 'spine6MulSqsh_mdv' )
    
    mdv1AmpSqsh = core.multiplyDivide( 'spine1AmpSqsh_mdv' )
    mdv2AmpSqsh = core.multiplyDivide( 'spine2AmpSqsh_mdv' )
    mdv3AmpSqsh = core.multiplyDivide( 'spine3AmpSqsh_mdv' )
    mdv4AmpSqsh = core.multiplyDivide( 'spine4AmpSqsh_mdv' )
    mdv5AmpSqsh = core.multiplyDivide( 'spine5AmpSqsh_mdv' )
    mdv6AmpSqsh = core.multiplyDivide( 'spine6AmpSqsh_mdv' )
    
    pma1Sqsh = core.plusMinusAverage( 'spine1Sqsh_pma' )
    pma2Sqsh = core.plusMinusAverage( 'spine2Sqsh_pma' )
    pma3Sqsh = core.plusMinusAverage( 'spine3Sqsh_pma' )
    pma4Sqsh = core.plusMinusAverage( 'spine4Sqsh_pma' )
    pma5Sqsh = core.plusMinusAverage( 'spine5Sqsh_pma' )
    pma6Sqsh = core.plusMinusAverage( 'spine6Sqsh_pma' )
    
    for pmaSqsh in ( pma1Sqsh , pma2Sqsh , pma3Sqsh , pma4Sqsh , pma5Sqsh , pma6Sqsh ) :
        core.addAttr( pmaSqsh , 'default' )
        mc.setAttr( '%s.default' %pmaSqsh , 1 )
        mc.connectAttr( '%s.default' %pmaSqsh , '%s.i1[0]' %pmaSqsh )
    
    mc.connectAttr( '%s.arcLength' %cif , '%s.length' %pmaLenAutoSqsh )
    
    for mdvMul in ( mdv1MulAutoSqsh , mdv2MulAutoSqsh , mdv3MulAutoSqsh , mdv4MulAutoSqsh , mdv5MulAutoSqsh , mdv6MulAutoSqsh ) :
        mc.connectAttr( '%s.o1' %pmaLenAutoSqsh , '%s.i1x' %mdvMul )
        
    mc.setAttr( '%s.i2x' %mdv1MulAutoSqsh , -0.15 )
    mc.setAttr( '%s.i2x' %mdv2MulAutoSqsh , -0.13 )
    mc.setAttr( '%s.i2x' %mdv3MulAutoSqsh , -0.1 )
    mc.setAttr( '%s.i2x' %mdv4MulAutoSqsh , -0.08 )
    mc.setAttr( '%s.i2x' %mdv5MulAutoSqsh , -0.05 )
    mc.setAttr( '%s.i2x' %mdv6MulAutoSqsh , -0.015 )
    
    for bclAutoSqsh in ( bcl1AutoSqsh , bcl2AutoSqsh , bcl3AutoSqsh , bcl4AutoSqsh  , bcl5AutoSqsh , bcl6AutoSqsh ) :
        mc.setAttr( '%s.c2r' %bclAutoSqsh , 0 )
    
    mc.connectAttr( '%s.ox' %mdv1MulAutoSqsh , '%s.i1x' %mdv1AmpAutoSqsh )
    mc.connectAttr( '%s.ox' %mdv2MulAutoSqsh , '%s.i1x' %mdv2AmpAutoSqsh )
    mc.connectAttr( '%s.ox' %mdv3MulAutoSqsh , '%s.i1x' %mdv3AmpAutoSqsh )
    mc.connectAttr( '%s.ox' %mdv4MulAutoSqsh , '%s.i1x' %mdv4AmpAutoSqsh )
    mc.connectAttr( '%s.ox' %mdv5MulAutoSqsh , '%s.i1x' %mdv5AmpAutoSqsh )
    mc.connectAttr( '%s.ox' %mdv6MulAutoSqsh , '%s.i1x' %mdv6AmpAutoSqsh )
    
    mc.connectAttr( '%s.ampSquash' %spine1FkCtrl , '%s.i2x' %mdv1AmpAutoSqsh )
    mc.connectAttr( '%s.ampSquash' %spine1FkCtrl , '%s.i2x' %mdv2AmpAutoSqsh )
    mc.connectAttr( '%s.ampSquash' %spine1FkCtrl , '%s.i2x' %mdv3AmpAutoSqsh )
    mc.connectAttr( '%s.ampSquash' %spine1FkCtrl , '%s.i2x' %mdv4AmpAutoSqsh )
    mc.connectAttr( '%s.ampSquash' %spine1FkCtrl , '%s.i2x' %mdv5AmpAutoSqsh )
    mc.connectAttr( '%s.ampSquash' %spine1FkCtrl , '%s.i2x' %mdv6AmpAutoSqsh )
    
    mc.connectAttr( '%s.ox' %mdv1AmpAutoSqsh , '%s.c1r' %bcl1AutoSqsh )
    mc.connectAttr( '%s.ox' %mdv2AmpAutoSqsh , '%s.c1r' %bcl2AutoSqsh )
    mc.connectAttr( '%s.ox' %mdv3AmpAutoSqsh , '%s.c1r' %bcl3AutoSqsh )
    mc.connectAttr( '%s.ox' %mdv4AmpAutoSqsh , '%s.c1r' %bcl4AutoSqsh )
    mc.connectAttr( '%s.ox' %mdv5AmpAutoSqsh , '%s.c1r' %bcl5AutoSqsh )
    mc.connectAttr( '%s.ox' %mdv6AmpAutoSqsh , '%s.c1r' %bcl6AutoSqsh )
    
    for bclAutoSqsh in ( bcl1AutoSqsh , bcl2AutoSqsh , bcl3AutoSqsh , bcl4AutoSqsh , bcl5AutoSqsh , bcl6AutoSqsh ) :
        mc.connectAttr( '%s.autoSquash' %spine1FkCtrl , '%s.b' %bclAutoSqsh )
    
    mc.connectAttr( '%s.opr' %bcl1AutoSqsh , '%s.i1[1]' %pma1Sqsh )
    mc.connectAttr( '%s.opr' %bcl2AutoSqsh , '%s.i1[1]' %pma2Sqsh )
    mc.connectAttr( '%s.opr' %bcl3AutoSqsh , '%s.i1[1]' %pma3Sqsh )
    mc.connectAttr( '%s.opr' %bcl4AutoSqsh , '%s.i1[1]' %pma4Sqsh )
    mc.connectAttr( '%s.opr' %bcl5AutoSqsh , '%s.i1[1]' %pma5Sqsh )
    mc.connectAttr( '%s.opr' %bcl6AutoSqsh , '%s.i1[1]' %pma6Sqsh )
    
    mc.connectAttr( '%s.o1' %pma1Sqsh , '%s.sy' %spine1Jnt )
    mc.connectAttr( '%s.o1' %pma1Sqsh , '%s.sz' %spine1Jnt )
    mc.connectAttr( '%s.o1' %pma2Sqsh , '%s.sy' %spine2Jnt )
    mc.connectAttr( '%s.o1' %pma2Sqsh , '%s.sz' %spine2Jnt )
    mc.connectAttr( '%s.o1' %pma3Sqsh , '%s.sy' %spine3Jnt )
    mc.connectAttr( '%s.o1' %pma3Sqsh , '%s.sz' %spine3Jnt )
    mc.connectAttr( '%s.o1' %pma4Sqsh , '%s.sy' %spine4Jnt )
    mc.connectAttr( '%s.o1' %pma4Sqsh , '%s.sz' %spine4Jnt )
    mc.connectAttr( '%s.o1' %pma5Sqsh , '%s.sy' %spine5Jnt )
    mc.connectAttr( '%s.o1' %pma5Sqsh , '%s.sz' %spine5Jnt )
    mc.connectAttr( '%s.o1' %pma6Sqsh , '%s.sy' %spine6Jnt )
    mc.connectAttr( '%s.o1' %pma6Sqsh , '%s.sz' %spine6Jnt )
    
    # Squash
    mc.connectAttr( '%s.squash' %spine1FkCtrl , '%s.i1x' %mdv1MulSqsh )
    mc.connectAttr( '%s.squash' %spine1FkCtrl , '%s.i1x' %mdv2MulSqsh )
    mc.connectAttr( '%s.squash' %spine1FkCtrl , '%s.i1x' %mdv3MulSqsh )
    mc.connectAttr( '%s.squash' %spine2FkCtrl , '%s.i1y' %mdv1MulSqsh )
    mc.connectAttr( '%s.squash' %spine2FkCtrl , '%s.i1y' %mdv2MulSqsh )
    mc.connectAttr( '%s.squash' %spine2FkCtrl , '%s.i1y' %mdv3MulSqsh )
    mc.connectAttr( '%s.squash' %spine2FkCtrl , '%s.i1y' %mdv4MulSqsh )
    mc.connectAttr( '%s.squash' %spine2FkCtrl , '%s.i1y' %mdv5MulSqsh )
    mc.connectAttr( '%s.squash' %spine2FkCtrl , '%s.i1y' %mdv6MulSqsh )
    mc.connectAttr( '%s.squash' %spine3FkCtrl , '%s.i1z' %mdv4MulSqsh )
    mc.connectAttr( '%s.squash' %spine3FkCtrl , '%s.i1z' %mdv5MulSqsh )
    mc.connectAttr( '%s.squash' %spine3FkCtrl , '%s.i1z' %mdv6MulSqsh )
    
    mc.connectAttr( '%s.ampSquash' %spine1FkCtrl , '%s.i1x' %mdv1AmpSqsh )
    mc.connectAttr( '%s.ampSquash' %spine1FkCtrl , '%s.i1x' %mdv2AmpSqsh )
    mc.connectAttr( '%s.ampSquash' %spine1FkCtrl , '%s.i1x' %mdv3AmpSqsh )
    mc.connectAttr( '%s.ampSquash' %spine1FkCtrl , '%s.i1y' %mdv1AmpSqsh )
    mc.connectAttr( '%s.ampSquash' %spine1FkCtrl , '%s.i1y' %mdv2AmpSqsh )
    mc.connectAttr( '%s.ampSquash' %spine1FkCtrl , '%s.i1y' %mdv3AmpSqsh )
    mc.connectAttr( '%s.ampSquash' %spine1FkCtrl , '%s.i1y' %mdv4AmpSqsh )
    mc.connectAttr( '%s.ampSquash' %spine1FkCtrl , '%s.i1y' %mdv5AmpSqsh )
    mc.connectAttr( '%s.ampSquash' %spine1FkCtrl , '%s.i1y' %mdv6AmpSqsh )
    mc.connectAttr( '%s.ampSquash' %spine1FkCtrl , '%s.i1z' %mdv4AmpSqsh )
    mc.connectAttr( '%s.ampSquash' %spine1FkCtrl , '%s.i1z' %mdv5AmpSqsh )
    mc.connectAttr( '%s.ampSquash' %spine1FkCtrl , '%s.i1z' %mdv6AmpSqsh )
    
    mc.connectAttr( '%s.ox' %mdv1MulSqsh , '%s.i2x' %mdv1AmpSqsh )
    mc.connectAttr( '%s.ox' %mdv2MulSqsh , '%s.i2x' %mdv2AmpSqsh )
    mc.connectAttr( '%s.ox' %mdv3MulSqsh , '%s.i2x' %mdv3AmpSqsh )
    mc.connectAttr( '%s.oy' %mdv1MulSqsh , '%s.i2y' %mdv1AmpSqsh )
    mc.connectAttr( '%s.oy' %mdv2MulSqsh , '%s.i2y' %mdv2AmpSqsh )
    mc.connectAttr( '%s.oy' %mdv3MulSqsh , '%s.i2y' %mdv3AmpSqsh )
    mc.connectAttr( '%s.oy' %mdv4MulSqsh , '%s.i2y' %mdv4AmpSqsh )
    mc.connectAttr( '%s.oy' %mdv5MulSqsh , '%s.i2y' %mdv5AmpSqsh )
    mc.connectAttr( '%s.oy' %mdv6MulSqsh , '%s.i2y' %mdv6AmpSqsh )
    mc.connectAttr( '%s.oz' %mdv4MulSqsh , '%s.i2z' %mdv4AmpSqsh )
    mc.connectAttr( '%s.oz' %mdv5MulSqsh , '%s.i2z' %mdv5AmpSqsh )
    mc.connectAttr( '%s.oz' %mdv6MulSqsh , '%s.i2z' %mdv6AmpSqsh )
    
    mc.connectAttr( '%s.ox' %mdv1AmpSqsh , '%s.i1[2]' %pma1Sqsh )
    mc.connectAttr( '%s.oy' %mdv1AmpSqsh , '%s.i1[3]' %pma1Sqsh )
    mc.connectAttr( '%s.ox' %mdv2AmpSqsh , '%s.i1[2]' %pma2Sqsh )
    mc.connectAttr( '%s.oy' %mdv2AmpSqsh , '%s.i1[3]' %pma2Sqsh )
    mc.connectAttr( '%s.ox' %mdv3AmpSqsh , '%s.i1[2]' %pma3Sqsh )
    mc.connectAttr( '%s.oy' %mdv3AmpSqsh , '%s.i1[3]' %pma3Sqsh )
    mc.connectAttr( '%s.oy' %mdv4AmpSqsh , '%s.i1[2]' %pma4Sqsh )
    mc.connectAttr( '%s.oz' %mdv4AmpSqsh , '%s.i1[3]' %pma4Sqsh )
    mc.connectAttr( '%s.oy' %mdv5AmpSqsh , '%s.i1[2]' %pma5Sqsh )
    mc.connectAttr( '%s.oz' %mdv5AmpSqsh , '%s.i1[3]' %pma5Sqsh )
    mc.connectAttr( '%s.oy' %mdv6AmpSqsh , '%s.i1[2]' %pma6Sqsh )
    mc.connectAttr( '%s.oz' %mdv6AmpSqsh , '%s.i1[3]' %pma6Sqsh )
    
    mc.setAttr( '%s.i2x' %mdv1MulSqsh , 0.1 )
    mc.setAttr( '%s.i2y' %mdv1MulSqsh , 0 )
    mc.setAttr( '%s.i2x' %mdv2MulSqsh , 0.07 )
    mc.setAttr( '%s.i2y' %mdv2MulSqsh , 0.03 )
    mc.setAttr( '%s.i2x' %mdv3MulSqsh , 0.02 )
    mc.setAttr( '%s.i2y' %mdv3MulSqsh , 0.08 )
    mc.setAttr( '%s.i2y' %mdv4MulSqsh , 0.08 )
    mc.setAttr( '%s.i2z' %mdv4MulSqsh , 0.02 )
    mc.setAttr( '%s.i2y' %mdv5MulSqsh , 0.03 )
    mc.setAttr( '%s.i2z' %mdv5MulSqsh , 0.07 )
    mc.setAttr( '%s.i2y' %mdv6MulSqsh , 0 )
    mc.setAttr( '%s.i2z' %mdv6MulSqsh , 0.1 )
    
    #-- Hierarchy
    core.parent( spineRigGrp , animGrp )
    core.parent( spineStillGrp , stillGrp )
    core.parent( spineJntGrp , jntGrp )
    core.parent( spine1PosJnt , parent )
    core.parent( spineFkRigGrp , spineRigGrp )
    core.parent( spine1FkZro , spineFkRigGrp )
    core.parent( ikhCuv , spineStillGrp )
    core.parent( ikhZro , ikhGrp )
    
    core.parent( spine3FkZro , spine2FkGmbl )
    core.parent( spine2FkZro , spine1FkGmbl )
    core.parent( spineRbnZro , spine2FkGmbl )
    
    #-- Cleanup
    for each in ( spineRigGrp , spineFkRigGrp , spine1FkZro , spine2FkZro , spine3FkZro , spineStillGrp , ikhCuv , ikhZro , spineLwrJnt , spineCenJnt , spineUprJnt ) :
        core.setLock( each , True , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
    
    for spineClstr in ( spineLwrJnt , spineCenJnt , spineUprJnt ) :
        core.setVal( '%s.v' %spineClstr , 0 )
    
    for ctrl in ( spine1FkCtrl , spine2FkCtrl , spine3FkCtrl , spine1FkGmbl , spine2FkGmbl , spine3FkGmbl , spineRbnCtrl ) :
        core.setLockHide( ctrl , 'sx' , 'sy' , 'sz' , 'v' )
    
    mc.select( cl = True )