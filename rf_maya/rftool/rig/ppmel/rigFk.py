import maya.cmds as mc
import maya.mel as mm
import ppmel.core as core
reload( core )


def rigFk( name = '' ,
           tmpJnt = [ ] ,
           parent = '' ,
           animGrp = 'anim_grp' ,
           axis = 'y' ,
           shape = 'square' ,
           color = 'red' ,
           side = '' ,
           charSize = 1 ) :
    
    #-- Info
    size = len(tmpJnt)
    
    if side :
        side = '_%s_' %side
    else :
        side = '_'
    
    #-- Group
    fkRigGrp = core.transform( '%sRig%sgrp' %( name , side ))
    core.parCons( 0 , parent , fkRigGrp )
    core.sclCons( 0 , parent , fkRigGrp )
    
    #-- Main fk rig--#
    if size == 1 :
        #-- Joint
        prefix = tmpJnt[0].split('tmpJnt')[0]
        fkJnt = core.addJnt( '%sjnt' %prefix , tmpJnt[0] )
        core.parent( fkJnt , parent )
        
        #-- Controls
        fkCtrl = core.addCtrl( '%sctrl' %prefix , shape , color , jnt = False )
        fkGmbl = core.addGimbal( fkCtrl )
        fkZro = core.addGrp( fkCtrl )
        fkOfst = core.addGrp( fkCtrl , 'Ofst' )
        core.snap( fkJnt , fkZro )
        
        #-- Hierarchy
        core.parent( fkZro , fkRigGrp , animGrp )
        
        #-- Shape
        for ctrl in ( fkCtrl , fkGmbl ) :
            core.scaleShape( ctrl , charSize )
        
        #-- Rig process
        core.parCons( 0 , fkGmbl , fkJnt )
        core.sclCons( 0 , fkGmbl , fkJnt )
        core.addLocalWorld( fkCtrl , 'anim_grp' , fkRigGrp , fkZro , 'orient' )
        
        #-- Cleanup
        for grp in ( fkZro , fkOfst , fkRigGrp ) :
            core.setLock( grp , True , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
        
        for ctrl in ( fkCtrl , fkGmbl ) :
            core.setLockHide( ctrl , 'sx' , 'sy' , 'sz' , 'v' )
            
    else :
        fkPosiArray = []
        fkJntArray = []
        fkCtrlArray = []
        fkGmblArray = []
        fkZroArray = []
        fkOfstArray = []
        
        for i in range(0,size) :
            #-- Joint
            name = tmpJnt[i].split('_')[0]
            fkPosJnt = core.addJnt( '%sPos%sjnt' %(name , side) , tmpJnt[i] )
            fkPosiArray.append(fkPosJnt)
            
            if not i == size-1 :
                #-- Joint
                fkJnt = core.addJnt( '%s%sjnt' %(name , side) , tmpJnt[i] )
                fkJntArray.append(fkJnt)
                
                #-- Controls
                fkCtrl = core.addCtrl( '%s%sctrl' %(name , side) , shape , color , jnt = False )
                fkGmbl = core.addGimbal( fkCtrl )
                fkZro = core.addGrp( fkCtrl )
                fkOfst = core.addGrp( fkCtrl , 'Ofst' )
                core.snap( fkPosJnt , fkZro )
                
                fkCtrlArray.append(fkCtrl)
                fkGmblArray.append(fkGmbl)
                fkZroArray.append(fkZro)
                fkOfstArray.append(fkOfst)
            
            #-- Shape
            for ctrl in ( fkCtrl , fkGmbl ) :
                core.scaleShape( ctrl , charSize )
                    
            #-- Hierarchy
            if i == 0 :
                if not i == size-1 :
                    core.parent( fkZro , fkRigGrp , animGrp )
                    
                core.parent( fkJnt , fkPosJnt , parent )
            else :
                if not i == size-1 :
                    core.parent( fkZro , fkGmblArray[i-1] )
                    core.parent( fkJnt , fkPosJnt )
                    
                core.parent( fkPosJnt , fkPosiArray[i-1] )
                
            #-- Rig process
            if not i == size-1 :
                core.parCons( 0 , fkGmbl , fkPosJnt )
                core.addSquash( fkCtrl , fkJnt , ax = ( 'sx' , 'sz'  ))
                
            if not i == 0 :
                if not i == size-1 :
                    core.addFkStretch( fkCtrlArray[i-1] , fkOfst , axis )
                else :
                    core.addFkStretch( fkCtrlArray[i-1] , fkPosJnt , axis )
                   
        core.addLocalWorld( fkCtrlArray[0] , 'anim_grp' , fkRigGrp , fkZroArray[0] , 'orient' )
        
        #-- Cleanup
        for zro in fkZroArray :
            core.setLock( zro , True , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
        
        for ofst in fkOfstArray :
            core.setLock( ofst , True , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
        
        for jnt in fkPosiArray :
            core.setLock( jnt , True , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
        
        for jnt in fkJntArray :
            core.setLock( jnt , True , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
        
        for ctrl in fkCtrlArray :
            core.setLockHide( ctrl , 'sx' , 'sy' , 'sz' , 'v' )
        
        for gmbl in fkGmblArray :
            core.setLockHide( gmbl , 'sx' , 'sy' , 'sz' , 'v' )
        
    mc.select( cl = True )