import maya.cmds as mc
import maya.mel as mm


### Snap Tools ###

def snap( *args ) :
    mc.delete( mc.parentConstraint ( *args , mo = False ))
    mc.select( cl = True )


def snapPoint( *args ) :
    mc.delete( mc.pointConstraint ( *args , mo = False ))
    mc.select( cl = True )


def snapOrient( *args ) :
    mc.delete( mc.orientConstraint ( *args , mo = False  ))
    mc.select( cl = True )


def snapAim( src = '' , trgt = '' , aim = (0,0,0) , upvec = (0,0,0)) :
    mc.delete( mc.aimConstraint ( src , trgt , mo = False  , aimVector = aim , upVector = upvec))
    mc.select( cl = True )


def snapJntOrient( target = '' , jnt = '' ) :
    mc.delete( mc.orientConstraint (( target , jnt ) , mo = False ))
    rot = mc.getAttr( '%s.rotate' %jnt )[0]
    mc.setAttr( '%s.jointOrient' %jnt , rot[0] , rot[1] , rot[2] )
    mc.setAttr( '%s.rotate' %jnt , 0 , 0 , 0 )


def freeze( *args ) :
    mc.makeIdentity( *args , a = True )
    mc.select( cl = True )


def parent( *args ) :
    check = len( args )

    for i in range(1,check) :
        mc.parent( args[i-1] , args[i] )
        mc.select( cl = True )


def connectAttr( src = '' , trgt = '' ) :
    attrLock = mc.getAttr( trgt , l =True )

    if attrLock == True :
        mc.setAttr( trgt , l = False )
        mc.connectAttr( src , trgt , f = True )
        mc.setAttr( trgt , l = True )
    else :
        mc.connectAttr( src , trgt , f = True )


def multiOrientJnt( obj = '' , aimVec = (0,1,0) , upVec = (1,0,0)) :
    
    chldrn = mc.listRelatives( obj , ad = True , type = 'joint')
    
    lenght = len(chldrn)
    
    if chldrn :
        for chld in chldrn :
            mc.parent( chld , w = True )
            
    for i in range(0,lenght) :
        if i == lenght-1 :
            orientJnt( chldrn[i] , obj , '' , aimVec = aimVec , upVec = upVec )
        else :
            orientJnt( chldrn[i] , chldrn[1+i] , '' , aimVec = aimVec , upVec = upVec )
            
    for i in range(0,lenght) :
        if not i+1 == lenght :
            mc.parent( chldrn[i] , chldrn[i+1] )
        else :
            mc.parent( chldrn[i] , obj )
    
    for attr in ( 'jointOrientX' , 'jointOrientY' , 'jointOrientZ' ) :
        mc.setAttr( '%s.%s' %(chldrn[0],attr) , 0 )
            
    mc.select( cl = True )
    
def orientJnt( trgt = '' , src = '' , objUpvec = '' , aimVec = (0,1,0) , upVec = (1,0,0)) :
    # Need duplicate src(joint) to upVector
    if not objUpvec : 
        objUpvec = mc.duplicate( src , rr = True )[0]
        tmpUpChldrn = mc.listRelatives( objUpvec , f = True )
    
        if tmpUpChldrn :
            mc.delete( tmpUpChldrn )
    
    aim = aimVec
    u = upVec
    wuo = objUpvec
    wut = 'objectrotation'
    
    mc.delete( mc.aimConstraint( trgt , src , aim = aim , wuo = objUpvec , wut = wut , u = u ))
    mc.makeIdentity( trgt , a = True )
    
    if not objUpvec : 
        mc.delete( objUpvec )

### Constraint Tools ###

def pntCons( offset = '' , *args ) :
    lockAttr = mc.listAttr( '%s' %args[-1] , l = True )
    lists = [ 'translateX' , 'translateY' , 'translateZ' ]
    
    listLock = []
    
    if lockAttr :
        for i in range(0,3) :
            if lists[i] in lockAttr :
                mc.setAttr( '%s.%s' %( args[-1] , lists[i] ) , lock = False , k = True )
                listLock.append( lists[i] )
            else :
                pass
                
    cons = mc.pointConstraint( *args , mo = offset )[0]
    pntCons = mc.rename( cons , '%s_pntCons' %args[-1] )
    
    for lock in listLock :
        mc.setAttr( '%s.%s' %( args[-1] , lock ) , lock = True , k = True )
    
    mc.select( cl = True )   
    return pntCons


def oriCons( offset = '' , *args ) :
    lockAttr = mc.listAttr( '%s' %args[-1] , l = True )
    lists = [ 'rotateX' , 'rotateY' , 'rotateZ' ]
    
    listLock = []
    
    if lockAttr :
        for i in range(0,3) :
            if lists[i] in lockAttr :
                mc.setAttr( '%s.%s' %( args[-1] , lists[i] ) , lock = False , k = True )
                listLock.append( lists[i] )
            else :
                pass
                    
    cons = mc.orientConstraint( *args , mo = offset )[0]
    oriCons = mc.rename( cons , '%s_oriCons' %args[-1] )
    
    for lock in listLock :
        mc.setAttr( '%s.%s' %( args[-1] , lock ) , lock = True , k = True )
    
    mc.select( cl = True )
    return oriCons


def parCons( offset = '' , *args ) :
    lockAttr = mc.listAttr( '%s' %args[-1] , l = True )
    lists = [ 'translateX' , 'translateY' , 'translateZ' , 'rotateX' , 'rotateY' , 'rotateZ' ]
    
    listLock = []
    
    if lockAttr :
        for i in range(0,6) :
            if lists[i] in lockAttr :
                mc.setAttr( '%s.%s' %( args[-1] , lists[i] ) , lock = False , k = True )
                listLock.append( lists[i] )
            else :
                pass
                    
    cons = mc.parentConstraint( *args , mo = offset )[0]
    parCons = mc.rename( cons , '%s_parCons' %args[-1] )
    
    for lock in listLock :
        mc.setAttr( '%s.%s' % ( args[-1] , lock ) , lock = True , k = True )
    
    mc.select( cl = True )
    return parCons


def sclCons( offset = '' , *args ) :
    lockAttr = mc.listAttr( '%s' %args[-1] , l = True )
    lists = [ 'scaleX' , 'scaleY' , 'scaleZ' ]
    
    listLock = []
    
    if lockAttr :
        for i in range(0,3) :
            if lists[i] in lockAttr :
                mc.setAttr( '%s.%s' %( args[-1] , lists[i] ) , lock = False , k = True )
                listLock.append( lists[i] )
            else :
                pass
                
    cons = mc.scaleConstraint( *args , mo = offset )[0]
    sclCons = mc.rename( cons , '%s_sclCons' %args[-1] )
    
    for lock in listLock :
        mc.setAttr( '%s.%s' %( args[-1] , lock ) , lock = True , k = True )
    
    mc.select( cl = True )
    return sclCons


def aimCons( *args ) :
    cons = mc.aimConstraint( *args , mo = 0 )[0]
    aimCons = mc.rename( cons , '%s_aimCons' %args[-1] )
    mc.select( cl = True )
    return aimCons


def pvtCons( *args ) :
    cons = mc.poleVectorConstraint( *args )[0]
    pvtCons = mc.rename( cons , '%s_pvtCons' %args[-1] )
    mc.select( cl = True )
    return pvtCons


def getCons( obj ) :
    cons = mc.listRelatives( obj , ad = True , type = 'constraint' )
    return cons


def clearCons( obj ) :
    listAttr = [ 'translateX' , 'translateY' , 'translateZ' , 'rotateX' , 'rotateY' , 'rotateZ' , 'scaleX' , 'scaleY' , 'scaleZ' ]
    
    for attr in listAttr :
        mc.setAttr( '%s.%s' %( obj, attr ) , l = False )
        
    cons = mc.listRelatives( obj , ad = True , type = 'constraint' )
    mc.delete(cons)


### Information Tools ###

def getExists( obj = '' ) :
    return mc.objExists( obj )


def getNodeType( obj = '' ) :
    return mc.nodeType( obj )


def getParent( obj = '' ) :
    return mc.listRelatives( obj , p = True )


def getChildren( obj = '' ) :
    return mc.listRelatives( obj , ad = True , type = 'transform' )


def getCons( obj = '' ) :
    return mc.listRelatives( obj , ad = True , type = 'constraint' )


def getPivot( obj = '' ) :
    return mc.xform( obj , q = True , t = True , ws = True  )


def getSide( obj = '' ) :
    sides = [ 'LFT' , 'RGT' , 'CEN' , 'Lft' , 'Rgt' , 'Cen' , 
              'lft' , 'rgt' , 'cen' ]
    
    currentSide = ''
    for side in sides :
        if side in obj :
            currentSide = '_%s' %side
    
    return currentSide


def getDef( obj = '' , deformers = '' ) :
    if deformers :
        listDef = [ deformers ]
    else :
        listDef = [ 'skinCluster' , 'tweak' , 'cluster' ]
    
    deformers = []
    
    shape = getShape( obj )
    
    for df in listDef :
        try :
            defNode = mc.listConnections( shape , type = df )[0]
            defSet = mc.listConnections( defNode , type = 'objectSet' )[0]
            
            deformers.append( defNode )
            deformers.append( defSet )
        except TypeError :
            continue
    
    return deformers


def getLock( obj = '' ) :
    return mc.getAttr( obj , l = True )


def setLock( obj = '' , lock = True , *args ) :
    for attr in args :
        mc.setAttr( '%s.%s' %( obj , attr ) , l = lock )


def getHide( obj = '' ) :
    return mc.getAttr( obj , k = False )


def setHide( obj = '' , hide = True , *args ) :
    for attr in args :
        mc.setAttr( '%s.%s' %( obj , attr ) , k = not hide , cb = not hide )


def setLockHide( obj = '' , *args ) :
    for attr in args :
        mc.setAttr( '%s.%s' %( obj , attr ) , l = True , k = False , cb = False )


def getVal( obj = '' , attr = '' ) :
    if attr :
        attrVal = mc.getAttr( '%s.%s' %( obj , attr ))
    else :
        listAttr = mc.listAttr( obj , k = True )
        attrVal = dict()
        
        for attr in listAttr :
            val = mc.getAttr( '%s.%s' %( obj , attr ))
            attrVal.update({ attr : val })
    
    return attrVal


def setVal( obj = '' , val = '' ) :
    name , attr = obj.split('.')
    lockAttr = mc.getAttr( obj , l = True )
    
    if lockAttr == True :
        mc.setAttr( obj , lock = False )
        mc.setAttr( obj , val )
        mc.setAttr( obj , lock = True )
    else :
        mc.setAttr( obj , val )


def getDist( st = '' , en = '' ) :
    # st , en = mc.ls( sl = True )
    
    dist1Grp = transform( 'null1' )
    dist2Grp = transform( 'null2' )
    distNode =  distanceBetween( 'dist' )
    
    snap( st , dist1Grp )
    snap( en , dist2Grp )

    mc.connectAttr( '%s.t' %dist1Grp , '%s.point1' %distNode )
    mc.connectAttr( '%s.t' %dist2Grp , '%s.point2' %distNode )
    
    distance = mc.getAttr( '%s.distance' %distNode )
    mc.delete( dist1Grp , dist2Grp , distNode )
    return distance


def getRotateOrder( obj = '' ) :
    dict = { 'xyz' : 0 ,
             'yzx' : 1 ,
             'zxy' : 2 ,
             'xzy' : 3 ,
             'yxz' : 4 ,
             'zyx' : 5 }
              
    id = mc.getAttr( '%s.rotateOrder' %obj )
    
    for key in dict.keys() :
        if id == dict[ key ] :
            return id


def setRotateOrder( obj = '' , rotateOrder = '' ) :
    dict = { 'xyz' : 0 ,
             'yzx' : 1 ,
             'zxy' : 2 ,
             'xzy' : 3 ,
             'yxz' : 4 ,
             'zyx' : 5 }
    
    if rotateOrder in dict.keys() :
        val = dict[ rotateOrder ]
    else :
        val = rotateOrder
        
    mc.setAttr( '%s.rotateOrder' %obj , val )


def getColor( obj = '' ) :
    shape = getShape( obj )
    return mc.getAttr( '%s.overrideColor' %shape )


def setColor( obj = '' , col = '' ) :
    shape = getShape( obj )
    
    colDict = { 'black'         : 1 ,
                'gray'          : 2 ,
                'softGray'      : 3 ,
                'darkRed'       : 4 ,
                'darkBlue'      : 5 ,
                'blue'          : 6 ,
                'darkGreen'     : 7 ,
                'brown'         : 11 ,
                'red'           : 13 ,
                'green'         : 14 ,
                'white'         : 16 ,
                'yellow'        : 17 ,
                'cyan'          : 18 }
    
    if type( col ) == type( str() ) :
        if col in colDict.keys() :
            colId = colDict[col]
        else :
            colId = 0
    else :
        colId = col
    
    mc.setAttr( '%s.overrideEnabled' %shape , 1 )
    mc.setAttr( '%s.overrideColor' %shape , colId )


def getPostPolevector( obj = '' , stJnt = '' , enJnt = '' ) :
    #stJnt , enJnt , obj = mc.ls( sl = True )
    posiGrp = transform()
    ikhDict = addIkh( type = 'ikRPsolver' , stJnt = stJnt , enJnt = enJnt )
    
    ikh = ikhDict[ 'ikh' ][0]
    
    mc.connectAttr( '%s.poleVector' %ikh , '%s.translate' %posiGrp )
    mc.disconnectAttr( '%s.poleVector' %ikh , '%s.translate' %posiGrp )
    
    pntGrp =  mc.group( posiGrp )
    mc.xform( pntGrp , os = True , piv = (0,0,0))
    
    snapPoint( stJnt , pntGrp )
    aimCons( stJnt , posiGrp )
    snap( posiGrp , obj )
    
    mc.delete( pntGrp ,ikhDict )


def getInfsPerVtcs( obj = '' , influences = 4 ) :
    obj = mc.ls( sl = True )[0]
    skc = getDef( obj , deformers = 'skinCluster' )[0]
    infs = mc.skinCluster( skc , q = True , inf = True )
    vtcs = []
    
    for i in range( mc.polyEvaluate ( obj , v = True )) :
        vtx = '%s.vtx[%s]' % ( obj , i )
        infVals = mc.skinPercent( skc , vtx , q = True , v = True )
        array = []
        
        for j in range( len ( infVals )) :
            if infVals[j-1] > 0 :
                array.append( infVals[j-1] )
        
        if len( array ) > influences :
            vtcs.append( vtx )
    
    if vtcs :
        mc.select( vtcs , r = True )


### Shader Tools ###

def clearShader() :
    sels = mc.ls( sl = True )
    
    for sel in sels :
        shape = getShape( sel )
        shadingGrps = mc.listConnections( shape , type = 'shadingEngine' )
        
        if shadingGrps :
            for shadingGrp in shadingGrps :
                mesh = mc.ls( mc.listConnections( shadingGrp ) , transforms = True )
                
                if len(mesh) == 1 :
                    shadingNode = mc.ls( mc.listConnections( shadingGrp ) , materials = True )[0]
                    
                    if not 'initialShadingGroup' in shadingGrp :
                        mc.delete( shadingGrp )
                    
                    if not 'lambert1' in shadingNode :
                        mc.delete( shadingNode )
        
    mc.hyperShade( assign = 'lambert1' )


### Shape Tools ###

def getShape( obj = '' ) :
    return mc.listRelatives( obj , shapes = True )[0]


def correctShapeName( obj = '' ) :
    shapes = getShape( obj )
    return mc.rename( shapes , '%sShape' %obj )


def parentShape( obj = '' , target = '' ) :
    shape = getShape( obj )
    
    mc.parent( shape , target , r = True , s = True )
    correctShapeName( target )
    
    mc.delete( obj )
    mc.select( cl = True )


def translateShape( obj  , target ) :
    piv = getPivot( obj )
    shape = getShape( obj )

    degree = mc.getAttr( '%s.degree' %shape )
    spans = mc.getAttr( '%s.spans' %shape )

    cv = ( degree + spans ) - 1

    tx = mc.getAttr( '%s.tx' %target )
    ty = mc.getAttr( '%s.ty' %target )
    tz = mc.getAttr( '%s.tz' %target )

    mc.select( obj + '.cv[0:%s]' %cv )
    mc.move( tx ,ty ,tz , r = True , os = True , wd = True )
    mc.select( cl = True )


def rotateShape( obj , rx , ry , rz ) :
    piv = getPivot( obj )
    shape = getShape( obj )

    degree = mc.getAttr( '%s.degree' %shape )
    spans = mc.getAttr( '%s.spans' %shape )

    cv = ( degree + spans ) - 1

    mc.select( obj + '.cv[0:%s]' %cv )
    mc.rotate( rx ,ry ,rz , r = True , os = True ,  p = ( piv[0] , piv[1] , piv[2] ))
    mc.select( cl = True )


def scaleShape( obj , value ) :
    shape = getShape( obj )

    degree = mc.getAttr( '%s.degree' %shape )
    spans = mc.getAttr( '%s.spans' %shape )

    cv = ( degree + spans ) - 1

    mc.select( obj + '.cv[0:%s]' %cv )
    mc.scale( value , value , value )
    mc.select( cl = True )


### Node Tools ###

def joint( name = '' ) :
    return mc.createNode( 'joint' , n = name )


def transform( name = '' ) :
    return mc.createNode( 'transform' , n = name )


def multiplyDivide( name = '' ) :
    return mc.createNode( 'multiplyDivide' , n = name )


def plusMinusAverage( name = '' ) :
    return mc.createNode( 'plusMinusAverage' , n = name )


def condition( name = '' ) :
    return mc.createNode( 'condition' , n = name )


def blendColors( name = '' ) :
    return mc.createNode( 'blendColors' , n = name )


def distanceBetween( name = '' ) :
    return mc.createNode( 'distanceBetween' , n = name )


def addDoubleLinear( name = '' ) :
    return mc.createNode( 'addDoubleLinear' , n = name )


def multDoubleLinear( name = '' ) :
    return mc.createNode( 'multDoubleLinear' , n = name )


def blendTwoAttr( name = '' ) :
    return mc.createNode( 'blendTwoAttr' , n = name )


def reverse( name = '' ) :
    return mc.createNode( 'reverse' , n = name )


def clamp( name = '' ) :
    return mc.createNode( 'clamp' , n = name )


def curveFromMeshEdge( name = '' ) :
    return mc.createNode( 'curveFromMeshEdge' , n = name )


def pointOnCurveInfo( name = '' ) :
    return mc.createNode( 'pointOnCurveInfo' , n = name )


def pointOnSurfaceInfo( name = '' ) :
    return mc.createNode( 'pointOnSurfaceInfo' , n = name )


def curveInfo( name = '' ) :
    return mc.createNode( 'curveInfo' , n = name )


def loft( name = '' ) :
    return mc.createNode( 'loft' , n = name )


### Curve Tools ###

def createCurve( name = '' , shape = '' ) :

    crvDict = { 'sphere'        : [(0,1.250064,0,),(0,1,0,),(1.93883e-007,0.92388,-0.382683,),(3.58248e-007,0.707107,-0.707107,),(4.68074e-007,0.382683,-0.923879,),(5.06639e-007,0,-1,),(0,0,-1.250064,),(5.06639e-007,0,-1,),(4.68074e-007,-0.382683,-0.923879,),(3.58248e-007,-0.707107,-0.707107,),(1.93883e-007,-0.92388,-0.382683,),(0,-1,0,),(0,-1.250064,0,),(0,-1,0,),(-5.70243e-008,-0.92388,0.382683,),(-1.05367e-007,-0.707107,0.707107,),(-1.37669e-007,-0.382683,0.92388,),(-1.49012e-007,0,1,),(0,0,1.250064,),(-1.49012e-007,0,1,),(-1.37669e-007,0.382683,0.92388,),(-1.05367e-007,0.707107,0.707107,),(-5.70243e-008,0.92388,0.382683,),(0,1,0,),(0.382683,0.92388,0,),(0.707107,0.707107,0,),(0.92388,0.382683,0,),(1,0,0,),(1.250064,0,0,),(1,0,0,),(0.92388,-0.382683,0,),(0.707107,-0.707107,0,),(0.382683,-0.92388,0,),(0,-1,0,),(0,-1.250064,0,),(0,-1,0,),(-0.382683,-0.92388,-1.36858e-007,),(-0.707107,-0.707107,-2.52881e-007,),(-0.92388,-0.382683,-3.30405e-007,),(-1,0,-3.57628e-007,),(-1.250064,0,0,),(-1,0,-3.57628e-007,),(-0.92388,0,0.382683,),(-0.707107,0,0.707107,),(-0.382684,0,0.923879,),(-1.49012e-007,0,1,),(0.382683,0,0.92388,),(0.707107,0,0.707107,),(0.92388,0,0.382683,),(1,0,0,),(0.92388,0,-0.382683,),(0.707107,0,-0.707106,),(0.382684,0,-0.923879,),(5.06639e-007,0,-1,),(-0.382683,0,-0.92388,),(-0.707106,0,-0.707107,),(-0.923879,0,-0.382684,),(-1,0,-3.57628e-007,),(-0.92388,0.382683,-3.30405e-007,),(-0.707107,0.707107,-2.52881e-007,),(-0.382683,0.92388,-1.36858e-007,),(0,1,0)] ,
                'cylinder'      : [(-2.98023e-008,0.5,1,),(0.309017,0.5,0.951057,),(0.587785,0.5,0.809017,),(0.809017,0.5,0.587785,),(0.951057,0.5,0.309017,),(1,0.5,0,),(0.951057,0.5,-0.309017,),(0.809018,0.5,-0.587786,),(0.587786,0.5,-0.809017,),(0.309017,0.5,-0.951057,),(0,0.5,-1,),(-0.309017,0.5,-0.951057,),(-0.587785,0.5,-0.809017,),(-0.809017,0.5,-0.587785,),(-0.951057,0.5,-0.309017,),(-1,0.5,0,),(-0.951057,0.5,0.309017,),(-0.809017,0.5,0.587785,),(-0.587785,0.5,0.809017,),(-0.309017,0.5,0.951057,),(-2.98023e-008,0.5,1,),(-2.98023e-008,-0.5,1,),(0.309017,-0.5,0.951057,),(0.587785,-0.5,0.809017,),(0.698401,-0.5,0.698401,),(0.698401,0.5,0.698401,),(0.698401,-0.5,0.698401,),(0.809017,-0.5,0.587785,),(0.951057,-0.5,0.309017,),(1,-0.5,0,),(1,0.5,0,),(1,-0.5,0,),(0.951057,-0.5,-0.309017,),(0.809018,-0.5,-0.587786,),(0.698402,-0.5,-0.698402,),(0.698402,0.5,-0.698402,),(0.698402,-0.5,-0.698402,),(0.587786,-0.5,-0.809017,),(0.309017,-0.5,-0.951057,),(0,-0.5,-1,),(0,0.5,-1,),(0,-0.5,-1,),(-0.309017,-0.5,-0.951057,),(-0.587785,-0.5,-0.809017,),(-0.698401,-0.5,-0.698401,),(-0.698401,0.5,-0.698401,),(-0.698401,-0.5,-0.698401,),(-0.809017,-0.5,-0.587785,),(-0.951057,-0.5,-0.309017,),(-1,-0.5,0,),(-1,0.5,0,),(-1,-0.5,0,),(-0.951057,-0.5,0.309017,),(-0.809017,-0.5,0.587785,),(-0.698401,-0.5,0.698401,),(-0.698401,0.5,0.698401,),(-0.698401,-0.5,0.698401,),(-0.587785,-0.5,0.809017,),(-0.309017,-0.5,0.951057,),(-2.98023e-008,-0.5,1,),(-2.98023e-008,0.5,1)] ,
                'stick'         : [(0,0,0),(0,1.499085,0),(0.0523598,1.501829,0),(0.104146,1.510032,0),(0.154791,1.523602,0),(0.20374,1.542392,0),(0.250457,1.566195,0),(0.29443,1.594752,0),(0.335177,1.627748,0),(0.372252,1.664823,0),(0.405248,1.70557,0),(0.433805,1.749543,0),(0.457608,1.79626,0),(0.476398,1.845209,0),(0.489968,1.895854,0),(0.498171,1.94764,0),(0.500915,2,0),(0.498171,2.05236,0),(0.489968,2.104146,0),(0.476398,2.154791,0),(0.457608,2.20374,0),(0.433805,2.250457,0),(0.405248,2.29443,0),(0.372252,2.335177,0),(0.335177,2.372252,0),(0.29443,2.405248,0),(0.250457,2.433805,0),(0.20374,2.457608,0),(0.154791,2.476398,0),(0.104146,2.489968,0),(0.0523598,2.498171,0),(0,2.500915,0),(-0.0523598,2.498171,0),(-0.104146,2.489968,0),(-0.154791,2.476398,0),(-0.20374,2.457608,0),(-0.250457,2.433805,0),(-0.29443,2.405248,0),(-0.335177,2.372252,0),(-0.372252,2.335177,0),(-0.405248,2.29443,0),(-0.433805,2.250457,0),(-0.457608,2.20374,0),(-0.476398,2.154791,0),(-0.489968,2.104146,0),(-0.498171,2.05236,0),(-0.500915,2,0),(-0.498171,1.94764,0),(-0.489968,1.895854,0),(-0.476398,1.845209,0),(-0.457608,1.79626,0),(-0.433805,1.749543,0),(-0.405248,1.70557,0),(-0.372252,1.664823,0),(-0.335177,1.627748,0),(-0.29443,1.594752,0),(-0.250457,1.566195,0),(-0.20374,1.542392,0),(-0.154791,1.523602,0),(-0.104146,1.510032,0),(-0.0523598,1.501829,0),(0,1.499085,0),(0,2.576874,0),(0,2,0),(0.576874,2,0),(-0.576874,2,0)] ,
                'circle'        : [(1.125,0,0),(1.004121,0,0),(0.991758,0,-0.157079),(0.954976,0,-0.31029),(0.894678,0,-0.455861) ,(0.812351,0,-0.590207),(0.710021,0,-0.710021),(0.590207,0,-0.812351),(0.455861,0,-0.894678),(0.31029,0,-0.954976),(0.157079,0,-0.991758),(0,0,-1.004121),(0,0,-1.125),(0,0,-1.004121),(-0.157079,0,-0.991758),(-0.31029,0,-0.954976),(-0.455861,0,-0.894678),(-0.590207,0,-0.812351),(-0.710021,0,-0.710021),(-0.812351,0,-0.590207),(-0.894678,0,-0.455861) ,(-0.954976,0,-0.31029),(-0.991758,0,-0.157079),(-1.004121,0,0),(-1.125,0,0),(-1.004121,0,0),(-0.991758,0,0.157079),(-0.954976,0,0.31029),(-0.894678,0,0.455861),(-0.812351,0,0.590207),(-0.710021,0,0.710021),(-0.590207,0,0.812351),(-0.455861,0,0.894678),(-0.31029,0,0.954976),(-0.157079,0,0.991758),(0,0,1.004121),(0,0,1.125) ,(0,0,1.004121),(0.157079,0,0.991758),(0.31029,0,0.954976),(0.455861,0,0.894678),(0.590207,0,0.812351),(0.710021,0,0.710021),(0.812351,0,0.590207),(0.894678,0,0.455861),(0.954976,0,0.31029),(0.991758,0,0.157079),(1.004121,0,0)] ,
                'arrowBall'     : [(-0.101079,1.958922,0.406811),(-0.101079,1.849883,0.83017),(-0.101079,1.66815,1.093854),(-0.354509,1.66815,1.093854),(0,1.322858,1.357538),(0.354509,1.66815,1.093854),(0.101079,1.66815,1.093854),(0.101079,1.849883,0.83017),(0.101079,1.958922,0.406811),(0.527367,1.849883,0.406811),(0.791052,1.66815,0.406811),(0.791052,1.66815,0.657311),(1.054736,1.322858,0.302802),(0.791052,1.66815,-0.0517064),(0.791052,1.66815,0.198794),(0.527367,1.849883,0.198794),(0.101079,1.958922,0.198794),(0.101079,1.849883,-0.224565),(0.101079,1.66815,-0.488249),(0.354509,1.66815,-0.488249),(0,1.322858,-0.751934),(-0.354509,1.66815,-0.488249),(-0.101079,1.66815,-0.488249),(-0.101079,1.849883,-0.224565),(-0.101079,1.958922,0.198794),(-0.527367,1.849883,0.198794),(-0.791052,1.66815,0.198794),(-0.791052,1.66815,-0.0517064),(-1.054736,1.322858,0.302802),(-0.791052,1.66815,0.657311),(-0.791052,1.66815,0.406811),(-0.527367,1.849883,0.406811),(-0.101079,1.958922,0.406811)] ,
                'arrowCircle'   : [(-0.407582,0,2.049652),(-0.52278,0,2.613902),(-1.045561,0,2.613902),(1.37739e-008,0,3.659463),(1.045561,0,2.613902),(0.52278,0,2.613902),(0.407582,0,2.049652),(1.161124,0,1.737523),(1.737531,0,1.161114),(2.049654,0,0.407569),(2.613902,0,0.52278),(2.613902,0,1.045561),(3.659463,0,0),(2.613902,0,-1.045561),(2.613902,0,-0.52278),(2.049648,0,-0.407595),(1.737521,0,-1.16114),(1.161091,0,-1.73752),(0.407573,0,-2.049738) ,(0.52278,0,-2.613902),(1.045561,0,-2.613902),(1.37739e-008,0,-3.659463),(-1.045561,0,-2.613902),(-0.52278,0,-2.613902),(-0.407573,0,-2.049738),(-1.161091,0,-1.73752),(-1.737521,0,-1.16114),(-2.049648,0,-0.407595),(-2.613902,0,-0.52278),(-2.613902,0,-1.045561),(-3.659463,0,0),(-2.613902,0,1.045561),(-2.613902,0,0.52278),(-2.049654,0,0.407569),(-1.737531,0,1.161114),(-1.161124,0,1.737523),(-0.407582,0,2.04965)] ,
                'pyramid'       : [(-0.999999,0.0754167,-0.999999),(-0.999999,0.0754167,0.999999),(0.999999,0.0754167,0.999999),(0.999999,0.0754167,-0.999999),(-0.999999,0.0754167,-0.999999),(-0.999999,-0.0695844,-0.999999),(-0.112596,-1,-0.112596),(-0.112596,-1,0.112596),(0.112596,-1,0.112596),(0.999999,-0.0695844,0.999999),(0.999999,0.0754167,0.999999),(0.999999,-0.0695844,0.999999),(-0.999999,-0.0695844,0.999999),(-0.999999,0.0754167,0.999999),(-0.999999,-0.0695844,0.999999),(-0.112596,-1,0.112596),(-0.999999,-0.0695844,0.999999),(-0.999999,-0.0695844,-0.999999),(0.999999,-0.0695844,-0.999999),(0.999999,0.0754167,-0.999999),(0.999999,-0.0695844,-0.999999),(0.112596,-1,-0.112596),(-0.112596,-1,-0.112596),(0.112596,-1,-0.112596),(0.112596,-1,0.112596),(0.112596,-1,-0.112596),(0.999999,-0.0695844,-0.999999),(0.999999,-0.0695844,0.999999)] ,
                'capsule'       : [(-2.011489,0,0),(-1.977023,0.261792,0),(-1.875975,0.505744,0),(-1.71523,0.71523,0),(-1.505744,0.875975,0),(-1.261792,0.977023,0),(-1,1.011489,0),(1,1.011489,0),(1.261792,0.977023,0),(1.505744,0.875975,0),(1.71523,0.71523,0),(1.875975,0.505744,0),(1.977023,0.261792,0),(2.011489,0,0),(1.977023,-0.261792,0),(1.875975,-0.505744,0),(1.71523,-0.71523,0),(1.505744,-0.875975,0),(1.261792,-0.977023,0),(1,-1.011489,0),(-1,-1.011489,0),(-1.261792,-0.977023,0),(-1.505744,-0.875975,0), (-1.71523,-0.71523,0),(-1.875975,-0.505744,0),(-1.977023,-0.261792,0),(-2.011489,0,0)] ,
                'arrowCross'    : [(-3.629392,0,-2.087399),(-3.629392,0,-1.723768),(-1.723768,0,-1.723768),(-1.723768,0,-3.629392),(-2.087399,0,-3.629392),(0,0,-5.704041),(2.087399,0,-3.629392),(1.723768,0,-3.629392),(1.723768,0,-1.723768),(3.629392,0,-1.723768),(3.629392,0,-2.087399),(5.704041,0,0),(3.629392,0,2.087399),(3.629392,0,1.723768),(1.723768,0,1.723768),(1.723768,0,3.629392),(2.087399,0,3.629392),(0,0,5.704041),(-2.087399,0,3.629392),(-1.723768,0,3.629392),(-1.723768,0,1.723768),(-3.629392,0,1.723768),(-3.629392,0,2.087399),(-5.704041,0,0),(-3.629392,0,-2.087399)] ,
                'triangle'      : [(-1,0,1,),(-1,0,1,),(-0.9,0,0.8,),(-0.8,0,0.6,),(-0.7,0,0.4,),(-0.6,0,0.2,),(-0.5,0,0,),(-0.4,0,-0.2,),(-0.3,0,-0.4,),(-0.2,0,-0.6,),(-0.1,0,-0.8,),(0,0,-1,),(0.1,0,-0.8,),(0.2,0,-0.6,),(0.3,0,-0.4,),(0.4,0,-0.2,),(0.5,0,0,),(0.6,0,0.2,),(0.7,0,0.4,),(0.8,0,0.6,),(0.9,0,0.8,),(1,0,1,),(0.8,0,1,),(0.6,0,1,),(0.4,0,1,),(0.2,0,1,),(0,0,1,),(-0.2,0,1,),(-0.4,0,1,),(-0.6,0,1,),(-0.8,0,1,),(-1,0,1)] ,
                'drop'          : [(0,0,-2.28198),(0.585544,0,-1.569395),(1.08325,0,-1.071748),(1.413628,0,-0.588119) ,(1.531946,0,0.00057226),(1.413628,0,0.585419),(1.08325,0,1.083278),(0.585544,0,1.413621) ,(0,0,1.531949),(-0.585544,0,1.413621),(-1.08325,0,1.083277),(-1.413628,0,0.585419) ,(-1.531946,0,0.000554983),(-1.413628,0,-0.588078),(-1.08325,0,-1.071626),(-0.585544,0,-1.570833),(0,0,-2.28198)] ,
                'plus'          : [(-0.545455,0,2),(0.545455,0,2),(0.545455,0,0.545455),(2,0,0.545455),(2,0,-0.545455),(0.545455,0,-0.545455),(0.545455,0,-2),(-0.545455,0,-2),(-0.545455,0,-0.545455),(-2,0,-0.545455),(-2,0,0.545455),(-0.545455,0,0.545455),(-0.545455,0,2)] ,
                'daimond'       : [(0,1,0,),(0,0,0.625,),(0,-1,0,),(0.625,0,0,),(0,1,0,),(0,0,-0.625,),(0,-1,0,),(-0.625,0,0,),(0,1,0,),(0.625,0,0,),(0,0,-0.625,),(-0.625,0,0,),(0,0,0.625,),(0.625,0,0)] ,
                'square'        : [(0,0,-1.12558),(0,0,-1),(-1,0,-1),(-1,0,0),(-1.12558,0,0),(-1,0,0),(-1,0,1),(0,0,1),(0,0,1.12558),(0,0,1),(1,0,1),(1,0,0),(1.12558,0,0),(1,0,0),(1,0,-1),(0,0,-1)] ,
                'cube'          : [(1,-1,1),(1,1,1),(1,1,-1),(1,-1,-1),(-1,-1,-1),(-1,1,-1),(-1,1,1),(-1,-1,1),(1,-1,1),(1,-1,-1),(-1,-1,-1),(-1,-1,1),(-1,1,1),(1,1,1),(1,1,-1),(-1,1,-1)] ,
                'arrow'         : [(-1,0,0,),(0,0,-1,),(1,0,0,),(0.4,0,0,),(0.4,0,1,),(-0.4,0,1,),(-0.4,0,0,),(-1,0,0)],
                'locator'       : [(0,1,0),(0,-1,0),(0,0,0),(-1,0,0),(1,0,0),(0,0,0),(0,0,-1),(0,0,1)] ,
                'null'          : [(0,0,0),(0,0,0),(0,0,0)] ,
                'line'          : [(0.3,0,0),(-0.3,0,0)] }
    
    if shape in crvDict.keys() :
        return mc.curve( n = name , d = 1 , p = crvDict[shape] )


### Add Rig Tools ###

def addCtrl( name = '' , shape = '' , col = '' , traget = '' , jnt = False ) :
    ctrl = createCurve( name , shape )
    correctShapeName( ctrl )
    setColor( ctrl , col )
    
    if traget :
        snap( traget , ctrl )

    if jnt == True :
        addJntCtrl( ctrl )
    
    return ctrl


def addGrp( obj = '' , type = 'Zro' ) :
    # obj = mc.ls( sl = True )[0]
    prefix = obj.split( '_' )[0]
    sufffix = obj.split( '_' )[-1]
    sufffix = sufffix.capitalize()
    side = getSide( obj )
    
    grp = transform( '%s%s%s%s_grp' %( prefix , sufffix , type , side ))
    pars = getParent( obj )
    
    snap( obj , grp )
    mc.parent( obj , grp )
    
    if pars :
        mc.parent( grp , pars )
        
    mc.select( cl = True )
    return grp


def addGimbal( obj = '' ) :
    # obj = mc.ls( sl = True )[0]
    prefix = obj.split( '_' )[0]
    suffix = obj.replace( prefix , '' )
        
    gmb = mc.duplicate( obj )
    gmb = mc.rename( gmb , '%sGmbl%s' %( prefix , suffix ))
    
    objShape = getShape( obj )
    gmbShape = getShape( gmb )
    
    addAttr( objShape , 'gimbalVis' , 0 , 1 , 0 )
    mc.connectAttr( '%s.gimbalVis' %objShape , '%s.v' %gmbShape )
    
    mc.connectAttr( '%s.rotateOrder' %obj , '%s.rotateOrder' %gmb )
    setColor( gmb , 'white' )
    scaleShape( gmb , 0.8 )
    
    snap( obj , gmb )
    mc.parent( gmb , obj )
    
    mc.select( cl = True )
    return gmb


def addCons( obj = '' ) :
    # obj = mc.ls( sl = True )[0]
    prefix = obj.split( '_' )[0]
    side = getSide( obj )
    
    ctrlCons = createCurve( '%sCons%s_ctrl' %( prefix , side ) , 'null' )
    correctShapeName( ctrlCons )
    setVal( '%sShape.v' %ctrlCons , 0 )

    pars = getParent( obj )
    
    if pars :
        mc.parent( ctrlCons , pars )
        mc.parent( obj , ctrlCons )
    else :
        mc.parent( obj , ctrlCons )
    
    mc.select( cl = True )
    return ctrlCons

def addCuvToJnt( name = '' , array = '' ) :
    size = len(array)
    position = []
    
    for i in range(0,size) :
        loc = mc.spaceLocator()[0]
        snap( array[i] , loc )
        pos = mc.getAttr( '%s.t' %loc )[0]
        position.append(pos)
        mc.delete(loc)
        
    cuv = mc.curve( n = name , p = position )
    return cuv


def addLine( ctrl = '' , target = '' ) :
    prefix = ctrl.split( '_' )[0]
    suffix = ctrl.replace( prefix , '' )
    side = getSide( ctrl )
    
    crv = mc.rename( mc.curve( d = 1 , p = [( 0,0,0 ),( 0,0,0 )] ) , '%sLine%s_Crv' %( prefix , side ))
    crvZro = addGrp( crv )

    clstr1 = mc.rename( mc.cluster( '%s.cv[0]' %crv , wn = ( ctrl , ctrl ))[0] , '%s1%s_clstr'  %( prefix , side ))
    clstr2 = mc.rename( mc.cluster( '%s.cv[1]' %crv , wn = ( target , target ))[0] , '%s2%s_clstr'  %( prefix , side ))

    setVal( '%s.overrideEnabled' %crv , 1 )
    setVal( '%s.overrideDisplayType' %crv , 2 )
    setVal( '%s.inheritsTransform' %crv , 0 )
    
    setLockHide( crv , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

    mc.select( cl = True )
    return crvZro


def addJnt( name = '' , target = '' ) :
    jnt = joint( name )
    
    if target :
        snap( target , jnt )
        freeze( jnt )
    
    mc.select( cl = True )
    return jnt


def addJntToCuv( name = '' , cuv = '' , skip = 0 ) :
    
    eps = mc.getAttr( '%s.spans' %cuv )
    
    jntDict = []
    
    if skip == 0 :
        for ep in range(eps+1):
            epName = ep+1
            posi = mc.pointPosition( '%s.ep[%s]' %(cuv , ep))
            jnt = addJnt( '%s%s_jnt' %(name , epName))
            mc.setAttr( '%s.t' %jnt , posi[0] , posi[1] , posi[2] )
            jntDict.append(jnt)
    else :
        ep = 0
        numLoop = 1
        while ep <= eps :
            posi = mc.pointPosition( '%s.ep[%s]' %(cuv , ep))
            jnt = addJnt( '%s%s_jnt' %(name , numLoop))
            mc.setAttr( '%s.t' %jnt , posi[0] , posi[1] , posi[2] )
            jntDict.append(jnt)
            
            ep += (skip+1)
            numLoop += 1 
            
    return jntDict


def addJntCtrl( ctrl = '' ) :
    suffix = ctrl.split( '_' )[-1]
    prefix = ctrl.replace( suffix , '' )
    
    transform = mc.rename( ctrl , 'transform' )
    rotateOrd = getRotateOrder( transform )
    shapesCol = getColor( transform )
    
    jnt = joint( ctrl )
    snap( transform , jnt )
    parentShape( transform , jnt )
    
    setVal( '%s.radius' %jnt , 0 )
    setVal( '%s.drawStyle' %jnt , 2 )
    setColor( jnt , shapesCol )
    setLockHide( jnt , 'radius' )
    setRotateOrder( jnt , rotateOrd )
    
    mc.select( cl = True )
    return jnt


def addIkh( name = '' , type = '' , stJnt = '' , enJnt = '' ) :
    side = getSide( stJnt )
    
    if not  type == 'ikSplineSolver' :
        ikh = mc.ikHandle( n = '%s%s_ikh' %( name , side ) , sol = type , sj = stJnt , ee = enJnt )
        ikhZro = addGrp( ikh[0] )
        mc.rename( ikh[-1] , '%s%s_eff' %( name , side ))
        return { 'ikh' : ikh[0] , 'ikhZro' : ikhZro }
    else :
        ikh = mc.ikHandle( n = '%s%s_ikh' %( name , side ) , sol = type , sj = stJnt , ee = enJnt , ns = 1 )
        ikhZro = addGrp( ikh[0] )
        mc.rename( ikh[1] , '%s%s_eff' %( name , side ))
        ikhCuv = mc.rename( ikh[2] , '%s%s_cuv' %( name , side ))
        return { 'ikh' : ikh[0] , 'ikhZro' : ikhZro , 'ikhCuv' : ikhCuv }
    
    mc.select( cl = True )


def addAttrType( obj = '' , attrName = '' ) :
    if not mc.objExists( '%s.%s' %( obj , attrName )) :
        mc.addAttr( obj , ln = attrName , nn = '__%s' %attrName , at = 'float' , k = True )
        setLock( obj , True , attrName )


def addAttr( obj = '' , attrName = '' , minVal = None , maxVal = None , defaultVal = None ) :
    if not mc.objExists( '%s.%s' %( obj , attrName )) :
        mc.addAttr( obj , ln = attrName , at = 'float' , k = True )
        
        if not minVal == None :
            mc.addAttr( '%s.%s' %( obj , attrName ) , e = True , min = int(minVal) )
        
        if not maxVal == None :
            mc.addAttr( '%s.%s' %( obj , attrName ) , e = True , max = int(maxVal) )
        
        if not defaultVal == None :
            mc.addAttr( '%s.%s' %( obj , attrName ) , e = True , dv = int(defaultVal) )


def addLocalWorld( ctrl = '' , worldObj = '' , localObj = '' , consGrp = '' , type = '' ) :
    prefix = ctrl.split( '_' )[0]
    suffix = ctrl.replace( prefix , '' )
    side = getSide( ctrl )
    
    addAttr( ctrl , 'localWorld' , '0' , '1' , None )
    
    rev = reverse( '%sLocWor%s_rev' %( prefix , side ))
    world = transform( '%sWor%s_grp' %( prefix , side ))
    local = transform( '%sLoc%s_grp' %( prefix , side ))
    
    snap( consGrp , world )
    snap( consGrp , local )
    
    if type == 'orient' :
        conNd = oriCons( 1 , local , world , consGrp )
        conWor = oriCons( 1 , worldObj , world )
    elif type == 'parent' :
        conNd = parCons( 1 , local , world , consGrp )
        conWor = parCons( 1 , worldObj , world )
    
    mc.connectAttr( '%s.localWorld' %ctrl , '%s.ix' %rev )
    mc.connectAttr( '%s.localWorld' %ctrl , '%s.%sW1' %( conNd , world ))
    mc.connectAttr( '%s.ox' %rev , '%s.%sW0' %( conNd , local ))
    
    mc.parent( world , local , localObj )
    
    for grp in ( world , local ) :
        setLock( grp , True , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )


def addBlendFkIk( ctrl = '' , jntFk = '' , jntIk = '' , jntMain = '' ) :
    prefixCtrl = ctrl.split( '_' )[0]    
    prefixJntMain = jntMain.split( '_' )[0]
    side = getSide( ctrl )
    
    addAttr( ctrl , 'fkIk' , 0 , 1 )
    
    pars = parCons( 0 , jntFk , jntIk , jntMain )
    
    if not mc.objExists( '%sFkIk%s_rev' %( prefixCtrl , side )) :
        rev = reverse( '%sFkIk%s_rev' %( prefixCtrl , side ))
        mc.connectAttr( '%s.fkIk' %ctrl , '%s.ix' %rev )
    else : 
        rev = '%sFkIk%s_rev' %( prefixCtrl , side )
        
    mc.connectAttr( '%s.fkIk' %ctrl , '%s.%sW1' %( pars , jntIk ))
    mc.connectAttr( '%s.ox' %rev , '%s.%sW0' %( pars , jntFk ))


def addSquash( ctrl = '' , jnt = '' , ax = ( 'sx' , 'sz'  )) :
    prefix = jnt.split( '_' )[0]
    suffix = jnt.replace( prefix , '' )
    side = getSide( ctrl )
    
    mdv = multiplyDivide( '%sSqsh%s_mdv' %( prefix , side ))
    pma = plusMinusAverage( '%sSqsh%s_pma' %( prefix , side ))
    
    addAttr( ctrl , 'squash' )
    addAttr( pma , 'default' )
    
    mc.setAttr( '%s.i2x' %mdv , 0.1 )
    mc.setAttr( '%s.default' %pma , 1 )
    mc.connectAttr( '%s.default' %pma , '%s.i1[0]' %pma )
    mc.connectAttr( '%s.squash' %ctrl , '%s.i1x' %mdv )
    mc.connectAttr( '%s.ox' %mdv , '%s.i1[1]' %pma )
    mc.connectAttr( '%s.o1' %pma , '%s.%s' %( jnt , ax[0] ))
    mc.connectAttr( '%s.o1' %pma , '%s.%s' %( jnt , ax[1] ))


def addFkStretch( ctrl = '' , target = '' , ax = 'y' , val = 0.1 ) :
    prefix = ctrl.split( '_' )[0]
    suffix = ctrl.replace( prefix , '' )
    side = getSide( ctrl )
    
    mdv = multiplyDivide( '%sStrt%s_mdv' %( prefix , side ))
    pma = plusMinusAverage( '%sStrt%s_pma' %( prefix , side ))
    
    addAttr( ctrl , 'stretch' )
    addAttr( pma , 'default' )
    
    valAx = mc.getAttr( '%s.t%s' %( target , ax ))
    mc.setAttr( '%s.default' %pma , valAx )
    mc.setAttr( '%s.i2x' %mdv , val )
    
    mc.connectAttr( '%s.default' %pma , '%s.i1[0]' %pma )
    mc.connectAttr( '%s.stretch' %ctrl , '%s.i1x' %mdv )
    mc.connectAttr( '%s.ox' %mdv , '%s.i1[1]' %pma )
    mc.connectAttr( '%s.o1' %pma , '%s.t%s' %( target , ax ))


def addIkStretch( ctrl = '' , lock = '' , axis = '' , type = '' , val = 0.1 , *args ) :
    prefixDict = []
    distGrpDict = []
    
    mdvDict = [ 'x' , 'y' , 'z' ]
    pmaDict = [ 'x' , 'y' , 'z' ]
    bclDict = [ 'r' , 'g' , 'b' ]
    
    side = getSide( ctrl )
    gmbl = mc.ls( mc.listRelatives( ctrl , c = True ) , tr = True )[0]
    
    addAttr( ctrl , 'stretch' )
    addAttr( ctrl , 'autoStretch' , 0 , 1 , None )
    setLock( ctrl , True , 'stretch' )
    
    for arg in args :
        prefixDict.append(arg.split( 'Ik' )[0])
        
        for prefix in prefixDict :
            if not arg == args[-1] :
                addAttr( ctrl , '%sStretch' %prefix )
    
    distSt = transform( '%sIkDistSt%s_grp' %( type , side ))
    distEn = transform( '%sIkDistEn%s_grp' %( type , side ))
    
    distGrpDict.append( distSt )
    distGrpDict.append( distEn )
    
    dist = distanceBetween( '%sIkAutoStrt%s_dtw' %( type , side ))
    pma = plusMinusAverage( '%sIkStrt%s_pma' %( type , side ))
    cond = condition( '%sIkAutoStrt%s_cnd' %( type , side ))
    bcl = blendColors( '%sIkStrt%s_bcl' %( type , side ))
    mdvAuto = multiplyDivide( '%sIkAutoStrt%s_mdv' %( type , side ))
    mdvAmp = multiplyDivide( '%sIkStrtAmp%s_mdv' %( type , side ))
    mdvStrt = multiplyDivide( '%sIkStrt%s_mdv' %( type , side ))
    
    pntCons( 0 , args[0] , distSt )
    pntCons( 0 , gmbl , distEn )
    
    mc.connectAttr( '%s.t' %distSt , '%s.point1' %dist )
    mc.connectAttr( '%s.t' %distEn , '%s.point2' %dist )
    
    mc.connectAttr( '%s.d' %dist , '%s.i1x' %mdvAuto )
    mc.connectAttr( '%s.d' %dist , '%s.i1y' %mdvAuto )
    mc.connectAttr( '%s.d' %dist , '%s.i1z' %mdvAuto )
    mc.connectAttr( '%s.d' %dist , '%s.ft' %cond )
    mc.connectAttr( '%s.o' %mdvAuto , '%s.ct' %cond )
    mc.connectAttr( '%s.oc' %cond , '%s.i1' %mdvStrt )
    mc.connectAttr( '%s.o' %mdvStrt , '%s.c1' %bcl )
    mc.connectAttr( '%s.op' %bcl , '%s.i3[0]' %pma )
    mc.connectAttr( '%s.o' %mdvAmp , '%s.i3[1]' %pma )
    mc.connectAttr( '%s.autoStretch' %ctrl , '%s.b' %bcl )
    
    setVal( '%s.op' %cond , 2 )
    setVal( '%s.op' %mdvAuto , 2 )
    setVal( '%s.st' %cond , getVal( dist , 'distance' ))
    setVal( '%s.i2x' %mdvAuto , getVal( dist , 'distance' ))
    setVal( '%s.i2y' %mdvAuto , getVal( dist , 'distance' ))
    setVal( '%s.i2z' %mdvAuto , getVal( dist , 'distance' ))
    
    for arg in args :
        if not arg == args[-1] :
            i = args.index( arg )
            
            mc.connectAttr( '%s.%sStretch' %( ctrl , prefixDict[i] ) , '%s.i1%s' %( mdvAmp , mdvDict[i] ))
            
            axVal = mc.getAttr( '%s.%s' %( args[i+1] , axis ) )
            
            setVal( '%s.i2%s' %( mdvAmp , mdvDict[i] ) , val )
            setVal( '%s.i2%s' %( mdvStrt , mdvDict[i] ) , axVal )
            setVal( '%s.c2%s' %( bcl , bclDict[i] ) , axVal )
    
    if not lock == '' :
        addAttr( lock , 'lock' , 0 , 1 , None )
        
        dist1Lock = distanceBetween( '%sIkLock1%s_dtw' %( type , side ))
        dist2Lock = distanceBetween( '%sIkLock2%s_dtw' %( type , side ))
        distLock = transform( '%sIkDistLock%s_grp' %( type , side ))
        mdvLock = multiplyDivide( '%sIkLock%s_mdv' %( type , side ))
        bclLock = blendColors( '%sIkLock%s_bcl' %( type , side ))        
        
        distGrpDict.append( distLock )
        
        pntCons( 0 , lock , distLock )
        
        mc.connectAttr( '%s.t' %distSt , '%s.point1' %dist1Lock )
        mc.connectAttr( '%s.t' %distEn , '%s.point1' %dist2Lock )
        mc.connectAttr( '%s.t' %distLock , '%s.point2' %dist1Lock )
        mc.connectAttr( '%s.t' %distLock , '%s.point2' %dist2Lock )
        mc.connectAttr( '%s.d' %dist1Lock , '%s.i1x' %mdvLock )
        mc.connectAttr( '%s.d' %dist2Lock , '%s.i1y' %mdvLock )
        mc.connectAttr( '%s.lock' %lock , '%s.b' %bclLock )
        mc.connectAttr( '%s.ox' %mdvLock , '%s.c1r' %bclLock )
        mc.connectAttr( '%s.oy' %mdvLock , '%s.c1g' %bclLock )
        mc.connectAttr( '%s.o3x' %pma , '%s.c2r' %bclLock )
        mc.connectAttr( '%s.o3y' %pma , '%s.c2g' %bclLock )
        mc.connectAttr( '%s.opr' %bclLock , '%s.%s' %( args[1] , axis ))
        mc.connectAttr( '%s.opg' %bclLock , '%s.%s' %( args[2] , axis ))
        
        if side == '_lft' :
            val = 1
        else :
            val = -1
            
        mc.setAttr( '%s.i2x' %mdvLock , val )
        mc.setAttr( '%s.i2y' %mdvLock , val )
        
    else :
        for i in range(0,len(args)-1) :
            mc.connectAttr( '%s.o3%s' %( pma , pmaDict[i] ) , '%s.%s' %( args[i+1] , axis ))
            
    for each in distGrpDict :
        setLockHide( each , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
        
    ikDistGrp = transform( '%sIkDist%s_grp' %( type , side ))
    mc.parent( distGrpDict , ikDistGrp )
    return ikDistGrp


### CleanUp Attr Tools ###

def jntRadius( r = 0.1 ) :
    selJnt = mc.ls( '*_jnt' )
    
    for each in selJnt :
        if not 'Pos' in each :
            if 'Rbn' in each :
                setVal( '%s.radius' %each , r*1.7 )
            else :
                setVal( '%s.radius' %each , r*1.1 )
        else :
            setVal( '%s.radius' %each , r*0.3 )
        
        
def cleanUpAttr() :
    selJnt = mc.ls( '*_jnt' )
    selGrp = mc.ls( '*_grp' )
    
    for jnt in selJnt :
        if not 'Ik' in jnt :
            setLock( jnt , True , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
    
    for grp in selGrp :
        setLock( grp , True , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' )