import maya.cmds as mc
import maya.mel as mm
import ppmel.core as core
import ppmel.rigRibbon as rbn
reload( core )
reload( rbn )


def rigSpineAnimal( spine1TmpJnt = 'spine1_tmpJnt' ,
                    spine2TmpJnt = 'spine2_tmpJnt' ,
                    neckTmpJnt = 'neck_tmpJnt' ,
                    parent = 'root_jnt' ,
                    animGrp = 'anim_grp' ,
                    skinGrp = 'skin_grp' ,
                    jntGrp = 'jnt_grp' ,
                    stillGrp = 'still_grp' ,
                    ikhGrp = 'ikh_grp' ,
                    spineFk = True ,
                    spineIk = True ,
                    ribbon = True ,
                    rotateOrder = 'yzx' ,
                    axis = 'z' ,
                    charSize = 1 ) :
    
    ## Main Rig ##
    #-- Group
    spineRigGrp = core.transform( 'spineRig_grp' )
    spineJntGrp = core.transform( 'spineJnt_grp' )
    core.parCons( 0 , parent , spineRigGrp )
    core.snap( parent , spineJntGrp )
    
    #-- Joint
    spine1PosJnt = core.addJnt( 'spine1Pos_jnt' , spine1TmpJnt )
    spine2PosJnt = core.addJnt( 'spine2Pos_jnt' , spine2TmpJnt )
    spine3PosJnt = core.addJnt( 'spine3Pos_jnt' , spine2TmpJnt )
    
    spine1Jnt = core.addJnt( 'spine1_jnt' , spine1TmpJnt )
    spine2Jnt = core.addJnt( 'spine2_jnt' , spine2TmpJnt )
    
    core.parent( spine3PosJnt , spine2PosJnt , spine1PosJnt )
    core.parent( spine2Jnt , spine2PosJnt )
    core.parent( spine1Jnt , spine1PosJnt )
    
    #-- Rotate order
    for each in ( spine1PosJnt , spine2PosJnt , spine1Jnt , spine2Jnt ) :
        core.setRotateOrder( each , rotateOrder )
    
    #-- Hierarchy
    core.parent( spineRigGrp , animGrp )
    core.parent( spineJntGrp , jntGrp )
    core.parent( spine1PosJnt , parent )
    
    #-- Cleanup
    for grp in ( spineRigGrp , spineJntGrp ) :
        core.setLock( grp , True , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
    
    
    ## Fk Rig ##
    if spineFk == True :
        #-- Group
        spineFkGrp = core.transform( 'spineFk_grp' )
        spineFkJntGrp = core.transform( 'spineFkJnt_grp' )
        core.snap( parent , spineFkGrp )
        core.snap( parent , spineFkJntGrp )
        
        #-- Joint
        spine1FkPosJnt = core.addJnt( 'spine1FkPos_jnt' , spine1TmpJnt )
        spine2FkPosJnt = core.addJnt( 'spine2FkPos_jnt' , spine2TmpJnt )
        spine3FkPosJnt = core.addJnt( 'spine3FkPos_jnt' , neckTmpJnt )
        
        spine1FkJnt = core.addJnt( 'spine1Fk_jnt' , spine1TmpJnt )
        spine2FkJnt = core.addJnt( 'spine2Fk_jnt' , spine2TmpJnt )
        
        core.parent( spine3FkPosJnt , spine2FkPosJnt , spine1FkPosJnt )
        core.parent( spine2FkJnt , spine2FkPosJnt )
        core.parent( spine1FkJnt , spine1FkPosJnt )
        
        #-- Controls
        spine1FkCtrl = core.addCtrl( 'spine1Fk_ctrl' , 'circle' , 'red' , jnt = True )
        spine1FkGmbl = core.addGimbal( spine1FkCtrl )
        spine1FkZro = core.addGrp( spine1FkCtrl )
        spine1FkOfst = core.addGrp( spine1FkCtrl , 'Ofst' )
        core.snapPoint( spine1FkJnt , spine1FkZro )
        core.snapJntOrient( spine1FkJnt , spine1FkCtrl )
        
        spine2FkCtrl = core.addCtrl( 'spine2Fk_ctrl' , 'circle' , 'red' , jnt = True )
        spine2FkGmbl = core.addGimbal( spine2FkCtrl )
        spine2FkZro = core.addGrp( spine2FkCtrl )
        spine2FkOfst = core.addGrp( spine2FkCtrl , 'Ofst' )
        core.snapPoint( spine2FkJnt , spine2FkZro )
        core.snapJntOrient( spine2FkJnt , spine2FkCtrl )
        
        #-- Shape
        for ctrl in ( spine1FkCtrl , spine1FkGmbl , spine2FkCtrl , spine2FkGmbl ) :
            core.scaleShape( ctrl , charSize * 2 )
            
        for ctrl in ( spine1FkCtrl , spine1FkGmbl , spine2FkCtrl , spine2FkGmbl ) :
            core.rotateShape( ctrl , 90 , 0 , 0 )
        
        #-- Rotate order
        for each in ( spine1FkCtrl , spine2FkCtrl , spine1FkPosJnt , spine2FkPosJnt , spine3FkPosJnt , spine1FkJnt , spine2FkJnt ) :
            core.setRotateOrder( each , rotateOrder )
        
        #-- Rig process
        core.parCons( 0 , spine1FkGmbl , spine1FkPosJnt )
        core.parCons( 0 , spine2FkGmbl , spine2FkPosJnt )
        
        core.sclCons( 0 , spine1FkGmbl , spine1FkJnt )
        core.sclCons( 0 , spine2FkGmbl , spine2FkJnt )
        
        core.parCons( 0 , spine1FkPosJnt , spine1PosJnt )
        core.parCons( 0 , spine2FkPosJnt , spine2PosJnt )
        
        core.sclCons( 0 , spine1FkJnt , spine1Jnt )
        core.sclCons( 0 , spine2FkJnt , spine2Jnt )
        
        core.addFkStretch( spine1FkCtrl , spine2FkOfst , axis )
        core.addFkStretch( spine2FkCtrl , spine3FkPosJnt , axis )
        
        core.addLocalWorld( spine1FkCtrl , 'anim_grp' , spineFkGrp , spine1FkZro , 'orient' )
            
        #-- Hierarchy
        core.parent( spine2FkZro , spine1FkGmbl )
        
        core.parent( spine1FkZro , spineFkGrp , spineRigGrp )
        core.parent( spine1FkPosJnt , spineFkJntGrp , spineJntGrp )
        
        #-- Cleanup
        for grp in ( spineFkGrp , spineFkJntGrp , spine1FkZro , spine2FkZro , spine1FkOfst , spine2FkOfst ) :
            core.setLock( grp , True , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
        
        for gmbl in ( spine1FkGmbl , spine2FkGmbl ) :
            core.setLockHide( gmbl , 'sx' , 'sy' , 'sz' , 'v' )
        
        for ctrl in ( spine1FkCtrl , spine2FkCtrl ) :
            core.setLockHide( ctrl , 'v' )
    
    
    ## Ik Rig ##
    if spineIk == True :
        #-- Group
        spineIkGrp = core.transform( 'spineIk_grp' )
        spineIkJntGrp = core.transform( 'spineIkJnt_grp' )
        spineIkIkhGrp = core.transform( 'spineIkHandle_grp' )
        core.snap( parent , spineIkGrp )
        core.snap( parent , spineIkJntGrp )
        core.snap( parent , spineIkIkhGrp )
        
        #-- Joint
        spine1IkPosJnt = core.addJnt( 'spine1IkPos_jnt' , spine1TmpJnt )
        spine2IkPosJnt = core.addJnt( 'spine2IkPos_jnt' , spine2TmpJnt )
        spine3IkPosJnt = core.addJnt( 'spine3IkPos_jnt' , neckTmpJnt )
        
        spine1IkJnt = core.addJnt( 'spine1Ik_jnt' , spine1TmpJnt )
        spine2IkJnt = core.addJnt( 'spine2Ik_jnt' , spine2TmpJnt )
        
        core.parent( spine2IkPosJnt , spine1IkPosJnt )
        core.parent( spine3IkPosJnt , spine2IkPosJnt )
        core.parent( spine2IkJnt , spine2IkPosJnt )
        core.parent( spine1IkJnt , spine1IkPosJnt )
        
        #-- Controls
        spine1IkCtrl = core.addCtrl( 'spine1Ik_ctrl' , 'circle' , 'blue' , jnt = True )
        spine1IkGmbl = core.addGimbal( spine1IkCtrl )
        spine1IkZro = core.addGrp( spine1IkCtrl )
        spine1IkPvt = core.transform( 'spine1IkPvt_grp' )
        core.parent( spine1IkPvt , spine1IkGmbl )
        core.snapPoint( spine1IkJnt , spine1IkZro )
        core.snapJntOrient( spine1IkJnt , spine1IkCtrl )
        
        core.setVal( '%sShape.v' %spine1IkCtrl , 0 )
        
        spine2IkCtrl = core.addCtrl( 'spine2Ik_ctrl' , 'circle' , 'blue' , jnt = True )
        spine2IkGmbl = core.addGimbal( spine2IkCtrl )
        spine2IkZro = core.addGrp( spine2IkCtrl )
        core.snapPoint( spine2IkJnt , spine2IkZro )
        core.snapJntOrient( spine2IkJnt , spine2IkCtrl )
        
        #-- Shape
        for ctrl in ( spine1IkCtrl , spine1IkGmbl , spine2IkCtrl , spine2IkGmbl ) :
            core.scaleShape( ctrl , charSize * 2 )
            
        for ctrl in ( spine1IkCtrl , spine1IkGmbl , spine2IkCtrl , spine2IkGmbl ) :
            core.rotateShape( ctrl , 90 , 0 , 0 )
        
        #-- Rotate order
        for each in ( spine1IkCtrl , spine2IkCtrl , spine1IkPosJnt , spine2IkPosJnt , spine3IkPosJnt , spine1IkJnt , spine2IkJnt ) :
            core.setRotateOrder( each , rotateOrder )
        
        #-- Rig process
        core.sclCons( 0 , spine1FkGmbl , spine1FkJnt )
        core.sclCons( 0 , spine2FkGmbl , spine2FkJnt )
        
        spine1IkhDict = core.addIkh( 'spine1Ik' , 'ikRPsolver' , spine1IkPosJnt , spine2IkPosJnt )
        spine2IkhDict = core.addIkh( 'spine2Ik' , 'ikSCsolver' , spine2IkPosJnt , spine3IkPosJnt )
        
        spine1Ikh = spine1IkhDict['ikh']
        spine1IkhZro = spine1IkhDict['ikhZro']
        spine2Ikh = spine2IkhDict['ikh']
        spine2IkhZro = spine2IkhDict['ikhZro']
        
        polevector = core.pvtCons( spine1IkPvt , spine1Ikh )
        core.addAttr( spine2IkCtrl , 'twist' )
        
        core.connectAttr( '%s.twist' %spine2IkCtrl , '%s.twist' %spine1Ikh )
        
        core.pntCons( 0 , spine1IkGmbl , spine1IkPosJnt  )
        core.parCons( 1 , spine2IkGmbl , spine1IkhZro  )
        core.parCons( 1 , spine2IkGmbl , spine2IkhZro  )
        
        core.addLocalWorld( spine2IkCtrl , 'anim_grp' , spineIkGrp , spine2IkZro , 'parent' )
        
        ikStrt = core.addIkStretch( spine2IkCtrl , '' ,'t%s' %axis , 'spine' , 0.1 , spine1IkPosJnt , spine2IkPosJnt , spine3IkPosJnt )
        
        #-- Fix IkStrt
        spine3IkPosPma = core.plusMinusAverage( 'spine3IkPosStrt_pma' )
        defVal = mc.getAttr( '%s.t%s' %( spine3IkPosJnt , axis ))
        
        core.addAttr( spine3IkPosPma , 'default' )
        core.setVal( '%s.default' %spine3IkPosPma , defVal )
        
        core.connectAttr( '%s.default' %spine3IkPosPma , '%s.i1[0]' %spine3IkPosPma )
        core.connectAttr( 'spineIkStrtAmp_mdv.oy' , '%s.i1[1]' %spine3IkPosPma )
        core.connectAttr( '%s.o1' %spine3IkPosPma , '%s.t%s' %( spine3IkPosJnt , axis ))
            
        #-- Hierarchy
        core.parent( spine2IkZro , spine1IkGmbl )
        
        core.parent( spine1IkZro , spineIkGrp , spineRigGrp )
        core.parent( spine1IkPosJnt , spineIkJntGrp , spineJntGrp )
        core.parent( spineIkIkhGrp , ikhGrp )
        core.parent( ikStrt , spineIkGrp )
        mc.parent( spine1IkhZro , spine2IkhZro , spineIkIkhGrp )
        
        #-- Cleanup
        for grp in ( spineIkGrp , spineIkJntGrp , spine1IkZro , spine2IkZro , spine1IkPvt ) :
            core.setLock( grp , True , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
        
        for gmbl in ( spine1IkGmbl , spine2IkGmbl ) :
            core.setLockHide( gmbl , 'sx' , 'sy' , 'sz' , 'v' )
        
        for ctrl in ( spine1IkCtrl , spine2IkCtrl ) :
            core.setLockHide( ctrl , 'v' )
        
        
    ## Fk Ik Blending ##
    if spineFk == True and spineIk == True :
        #-- Controls
        spineCtrl = core.addCtrl( 'spine_ctrl' , 'stick' , 'green' , jnt = False )
        spineZro = core.addGrp( spineCtrl )
        
        #-- Shape
        core.scaleShape( spineCtrl , charSize )
        
        #-- Rig process
        core.parCons( 0 , spine1PosJnt , spineZro )
        
        core.addBlendFkIk( spineCtrl , spine1FkPosJnt , spine1IkPosJnt , spine1PosJnt )
        core.addBlendFkIk( spineCtrl , spine2FkPosJnt , spine2IkPosJnt , spine2PosJnt )
        core.addBlendFkIk( spineCtrl , spine3FkPosJnt , spine3IkPosJnt , spine3PosJnt )
        core.addBlendFkIk( spineCtrl , spine1FkJnt , spine1IkJnt , spine1Jnt )
        core.addBlendFkIk( spineCtrl , spine2FkJnt , spine2IkJnt , spine2Jnt )
        
        if mc.objExists( 'spineFkIk_rev') :
            revFkIk = 'spineFkIk_rev'
            core.connectAttr( '%s.fkIk' %spineCtrl , '%s.v' %spineIkGrp )
            core.connectAttr( '%s.ox' %revFkIk , '%s.v' %spineFkGrp )

        core.setVal( '%s.fkIk' %spineCtrl , 1 )
            
        #-- Hierarchy
        core.parent( spineZro , spineRigGrp )
            
        #-- Cleanup
        core.setLock( spineZro , True , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
        core.setLockHide( spineCtrl , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
        
    elif spineFk == True and spineIk == False :
        core.parCons( 0 , spine1FkPosJnt , spine1PosJnt )
        core.parCons( 0 , spine2FkPosJnt , spine2PosJnt )
        core.parCons( 0 , spine3FkPosJnt , spine3PosJnt )
        core.parCons( 0 , spine1FkJnt , spine1Jnt )
        core.parCons( 0 , spine2FkJnt , spine2Jnt )
        
    elif spineFk == False and spineIk == True :
        core.parCons( 0 , spine1IkPosJnt , spine1PosJnt )
        core.parCons( 0 , spine2IkPosJnt , spine2PosJnt )
        core.parCons( 0 , spine3IkPosJnt , spine3PosJnt )
        core.parCons( 0 , spine1IkJnt , spine1Jnt )
        core.parCons( 0 , spine2IkJnt , spine2Jnt )
    
    
    ## Ribbon ##
    rbnAxis = 'z+'
    rbnAim = ( 0,0,1 )
    rbnUp = ( 1,0,0 )
    rootTwsAmp = -1
    endTwsAmp = 1
        
    #-- Group
    spineRbnGrp = core.transform( 'spineRbn_grp' )
    
    #-- Controls
    spineRbnCtrl = core.addCtrl( 'spineRbn_ctrl' , 'locator' , 'yellow' , jnt = True )
    spineRbnZro = core.addGrp( spineRbnCtrl )
    core.snapPoint( spine2PosJnt , spineRbnZro )
    core.snapJntOrient( spine2PosJnt , spineRbnCtrl )
    mc.parent( spineRbnZro , spineRbnGrp )
    
    #-- Shape
    core.scaleShape( spineRbnCtrl , charSize * 2 )
    
    #-- Rig process
    core.parCons( 1 , spine2PosJnt , spineRbnZro )

    if ribbon == True :
        rbnSpine1Par = core.getParent( spine1PosJnt )
        rbnSpine2Par = core.getParent( spine2PosJnt )
        rbnSpine3Par = core.getParent( spine3PosJnt )
        
        rbnSpine1 = rbn.rigRibbon( 'spine1' , ax = rbnAxis , jntRoot = spine1PosJnt , jntEnd = spine2PosJnt , animGrp = spineRbnGrp , skinGrp = skinGrp , stillGrp = stillGrp , charSize = charSize * 2 )
        rbnSpine2 = rbn.rigRibbon( 'spine2' , ax = rbnAxis , jntRoot = spine2PosJnt , jntEnd = spine3PosJnt , animGrp = spineRbnGrp , skinGrp = skinGrp , stillGrp = stillGrp , charSize = charSize * 2 )
        
        rbnSpine1Grp = rbnSpine1['rbnGrp']
        rbnSpine1Ctrl = rbnSpine1['rbnCtrl']
        rbnSpine1RootCtrl = rbnSpine1['rbnRootCtrl']
        rbnSpine1EndCtrl = rbnSpine1['rbnEndCtrl']
        rbnSpine1RootTwZro = rbnSpine1['rbnRootTwZro']
        rbnSpine1EndTwZro = rbnSpine1['rbnEndTwZro']
        rbnSpine1RootTwGrp = rbnSpine1['rbnRootTwGrp']
        rbnSpine1EndTwGrp = rbnSpine1['rbnEndTwGrp']
        rbnSpine1CtrlShape = rbnSpine1['rbnCtrlShape']
        
        rbnSpine2Grp = rbnSpine2['rbnGrp']
        rbnSpine2Ctrl = rbnSpine2['rbnCtrl']
        rbnSpine2RootCtrl = rbnSpine2['rbnRootCtrl']
        rbnSpine2EndCtrl = rbnSpine2['rbnEndCtrl']
        rbnSpine2RootTwZro = rbnSpine2['rbnRootTwZro']
        rbnSpine2EndTwZro = rbnSpine2['rbnEndTwZro']
        rbnSpine2RootTwGrp = rbnSpine2['rbnRootTwGrp']
        rbnSpine2EndTwGrp = rbnSpine2['rbnEndTwGrp']
        rbnSpine2CtrlShape = rbnSpine2['rbnCtrlShape']
        
        mc.setAttr( '%s.rootTwistAmp' %rbnSpine1CtrlShape , rootTwsAmp )
        mc.setAttr( '%s.endTwistAmp' %rbnSpine1CtrlShape , endTwsAmp )
        
        mc.setAttr( '%s.rootTwistAmp' %rbnSpine2CtrlShape , rootTwsAmp )
        mc.setAttr( '%s.endTwistAmp' %rbnSpine2CtrlShape , endTwsAmp )
        
        for each in ( rbnSpine1RootTwZro , rbnSpine1RootTwGrp , rbnSpine1EndTwZro , rbnSpine1EndTwGrp , rbnSpine2RootTwZro , rbnSpine2RootTwGrp , rbnSpine2EndTwZro , rbnSpine2EndTwGrp ) :
            core.setRotateOrder( each , rotateOrder )
        
        core.snapPoint( spine1PosJnt , rbnSpine1Grp )
        mc.delete( mc.aimConstraint( spine2PosJnt , rbnSpine1Grp , aim = rbnAim , u = rbnUp , wut = 'vector' , wu = ( 1,0,0 ) ))
        core.snap( spine1PosJnt , rbnSpine1RootTwZro )
        core.snap( spine2PosJnt , rbnSpine1EndTwZro )
        core.parCons( 1 , spine1PosJnt , rbnSpine1Grp )
        core.pntCons( 0 , spine1PosJnt , rbnSpine1RootCtrl )
        core.pntCons( 0 , spineRbnCtrl , rbnSpine1EndCtrl )
        core.parCons( 1 , rbnSpine1Par , rbnSpine1RootTwZro )
        core.parCons( 1 , rbnSpine2Par , rbnSpine1EndTwZro )
        core.parCons( 0 , spine1PosJnt , rbnSpine1RootTwGrp )
        core.parCons( 0 , spine2PosJnt , rbnSpine1EndTwGrp )
        
        core.snapPoint( spine2PosJnt , rbnSpine2Grp )
        mc.delete( mc.aimConstraint( spine3PosJnt , rbnSpine2Grp , aim = rbnAim , u = rbnUp , wut = 'vector' , wu = ( 1,0,0 ) ))
        core.snap( spine2PosJnt , rbnSpine2RootTwZro )
        core.snap( spine3PosJnt , rbnSpine2EndTwZro )
        core.parCons( 1 , spine2PosJnt , rbnSpine2Grp )
        core.pntCons( 0 , spineRbnCtrl , rbnSpine2RootCtrl )
        core.pntCons( 0 , spine3PosJnt , rbnSpine2EndCtrl )
        core.parCons( 1 , rbnSpine3Par , rbnSpine2EndTwZro )
        core.parCons( 0 , spine3PosJnt , rbnSpine2EndTwGrp )
        
        #-- Hierarchy
        core.parent( spineRbnGrp , spineRigGrp )
        
        #-- Cleanup
        for each in ( spineRbnGrp , spineRbnZro , rbnSpine1Grp , rbnSpine2Grp , rbnSpine1RootCtrl , rbnSpine1EndCtrl , rbnSpine2RootCtrl , rbnSpine2EndCtrl , rbnSpine1RootTwZro , rbnSpine1EndTwZro , rbnSpine1RootTwGrp , rbnSpine1EndTwGrp , rbnSpine2RootTwZro , rbnSpine2EndTwZro , rbnSpine2RootTwGrp , rbnSpine2EndTwGrp ) :
            core.setLock( each , True , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
        
        for each in ( rbnSpine1RootCtrl , rbnSpine1EndCtrl , rbnSpine2RootCtrl , rbnSpine2EndCtrl ) :
            core.setVal( '%s.v' %each , 0 )
        
        for each in ( rbnSpine1Ctrl , rbnSpine2Ctrl ) :
            core.setVal( '%s.autoTwist' %each , 1 )
        
        core.setLockHide( spineRbnCtrl , 'sx' , 'sy' , 'sz' , 'v' )
    
    else :
        #-- Group
        spineRbnGrp = core.transform( 'spineRbn_grp' )
        
        #-- Joint
        bellyJnt = core.addJnt( 'belly_jnt' )
        chestJnt = core.addJnt( 'chest_jnt' )
        
        core.snap( spine1TmpJnt , spine2TmpJnt , bellyJnt )
        core.snap( spine2TmpJnt , neckTmpJnt , chestJnt )
        
        mc.parent( bellyJnt , spine1PosJnt )
        mc.parent( chestJnt , spine2PosJnt )
        
        mc.delete( mc.aimConstraint( spine2TmpJnt , bellyJnt , aim = rbnAim , u = rbnUp , wut = 'vector' , wu = ( 1,0,0 ) ))
        mc.delete( mc.aimConstraint( neckTmpJnt , chestJnt , aim = rbnAim , u = rbnUp , wut = 'vector' , wu = ( 1,0,0 ) ))
        core.freeze( bellyJnt , chestJnt )
        
        #-- Controls
        bellyCtrl = core.addCtrl( 'belly_ctrl' , 'square' , 'yellow' , jnt = True )
        bellyGmbl = core.addGimbal( bellyCtrl )
        bellyZro = core.addGrp( bellyCtrl )
        bellyAim = core.addGrp( bellyCtrl , 'Aim' )
        core.snap( bellyJnt , bellyZro )
        
        chestCtrl = core.addCtrl( 'chest_ctrl' , 'square' , 'yellow' , jnt = True )
        chestGmbl = core.addGimbal( chestCtrl )
        chestZro = core.addGrp( chestCtrl )
        chestAim = core.addGrp( chestCtrl , 'Aim' )
        core.snap( chestJnt , chestZro )
        
        #-- Shape
        for ctrl in ( bellyCtrl , bellyGmbl , chestCtrl , chestGmbl ) :
            core.scaleShape( ctrl , charSize * 1.8 )
            
        for ctrl in ( bellyCtrl , bellyGmbl , chestCtrl , chestGmbl ) :
            core.rotateShape( ctrl , 90 , 0 , 0 )
        
        #-- Rig process
        core.parCons( 0 , bellyGmbl , bellyJnt )
        core.parCons( 0 , chestGmbl , chestJnt )
        
        mc.aimConstraint( spineRbnCtrl , bellyAim , aim = (0,0,1) , u = (0,1,0) , wut = 'objectrotation' , wuo = bellyZro , wu = ( 0,1,0 ))
        mc.aimConstraint( spineRbnCtrl , chestAim , aim = (0,0,-1) , u = (0,1,0) , wut = 'objectrotation' , wuo = chestZro , wu = ( 0,1,0 ))
        
        core.pntCons( 0 , spineRbnCtrl , spine1PosJnt , bellyZro )
        core.pntCons( 0 , spineRbnCtrl , spine3PosJnt , chestZro )
        
        #-- Hierarchy
        mc.parent( spineRbnGrp , spineRigGrp )
        mc.parent( spineRbnZro , bellyZro , chestZro , spineRbnGrp )
        
        #-- Cleanup
        for each in ( spineRbnGrp , spineRbnZro , bellyZro , bellyAim , chestZro , chestAim ) :
            core.setLock( each , True , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
        
    mc.select( cl = True )
    return spine1IkZro , spine1FkZro