//Maya ASCII 2015 scene
//Name: tmpJntHuman.ma
//Last modified: Thu, Feb 16, 2017 12:32:08 PM
//Codeset: 1252
requires maya "2015";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2015";
fileInfo "version" "2015";
fileInfo "cutIdentifier" "201503261530-955654";
fileInfo "osv" "Microsoft Windows 8 Business Edition, 64-bit  (Build 9200)\n";
createNode transform -n "tmpJnt_grp";
createNode joint -n "root_tmpJnt" -p "tmpJnt_grp";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".t" -type "double3" 0 30.631368707651099 0.031438849604281671 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 15.73707328056101 0 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "spine1_tmpJnt" -p "root_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" 4.9303806576313238e-032 1.3686312923489012 -0.36801838305054296 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 16.462721689908392 7.9790056857053538e-017 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "spine2_tmpJnt" -p "spine1_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 8.8495320473911514e-031 4 -7.2164496600635175e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 18.962119031422834 1.1092698799812162e-016 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "spine3_tmpJnt" -p "spine2_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" 1.2935286343432839e-015 4 -0.00039599416322522174 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 16.462721689908392 7.9790056857053538e-017 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "spine4_tmpJnt" -p "spine3_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" -1.1580287744150471e-011 4 2.0227153285645727e-012 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 16.462721689908392 7.9790056857053538e-017 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "neck1_tmpJnt" -p "spine4_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 0 3.0033243299767349 -0.10672510313519606 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 22.764810214551506 1.2852494451027699e-016 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "head1_tmpJnt" -p "neck1_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 0 5.5534401579638839 1.2508894082127742 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 25.118764441136726 1.5379363139276727e-016 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "head2_tmpJnt" -p "head1_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" 0 4.3631392240861473 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 27.864768711571326 -1.9234515956468954e-015 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "eye_lft_tmpJnt" -p "head1_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" 1.0281571257306426 0.77422021214944436 3.1439508446925091 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0.53833165295886931 25.828273426804412 1.4860476249829009 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "eye_rgt_tmpJnt" -p "head1_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" -1.0280906176201243 0.77422021214944436 3.1439508446925091 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -0.53833200000000003 25.828300000000006 1.4860499999999996 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "jaw1_upr_tmpJnt" -p "head1_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" 0 -0.7592158367292825 3.7570380903960201 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" 0 0 180 ;
	setAttr ".bps" -type "matrix" -1 1.2246467991473532e-016 0 0 -1.2246467991473532e-016 -1 0 0
		 0 0 1 0 0 24.387496966804516 0.55021567872571864 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "jaw2_upr_tmpJnt" -p "jaw1_upr_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" 0 7.1054273576010019e-015 0.571741216560679 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" -1 1.2246467991473532e-016 0 0 -1.2246467991473532e-016 -1 0 0
		 0 0 1 0 -3.2163030071266839e-032 24.387496966804516 2.0776527139297962 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "teeth_upr_tmpJnt" -p "jaw1_upr_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".t" -type "double3" -2.7801952318961374e-017 0.22702016890355026 -0.14284411305356137 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 -180 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 24.169010636166544 2.0776527139297967 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "jaw1_lwr_tmpJnt" -p "head1_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" 0 -0.25088529577887186 1.1788504685318186 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 25.105065775409837 0.43032997634803533 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "jaw2_lwr_tmpJnt" -p "jaw1_lwr_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" 0 -1.5265626649298767 2.2589275580308033 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 24.169010636166544 0.55021567872571864 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "jaw3_lwr_tmpJnt" -p "jaw2_lwr_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".t" -type "double3" 0 0 0.63658680564558612 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 24.169010636166544 2.0776527139297967 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "teeth_lwr_tmpJnt" -p "jaw2_lwr_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".t" -type "double3" 0 0.33033941860330174 0.17641595077983663 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 24.169010636166544 2.0776527139297967 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "tongue1_tmpJnt" -p "jaw1_lwr_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" 0 -1.4850183967011148 -0.77832724388948415 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" 32.38843485659649 0 0 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 25.105065775409837 0.43032997634803533 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "tongue2_tmpJnt" -p "tongue1_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" 0 0.78845114897644208 3.5527136788005009e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" 52.785353509946098 0 0 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 24.169010636166544 0.55021567872571864 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "tongue3_tmpJnt" -p "tongue2_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".t" -type "double3" 0 0.60087284901111193 4.9737991503207013e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 9.5040328481669594 0 0 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 24.169010636166544 2.0776527139297967 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "tongue4_tmpJnt" -p "tongue3_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".t" -type "double3" 0 0.70265318447990444 -3.5527136788005009e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 1.0001348544728343 0 0 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 24.169010636166544 2.0776527139297967 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "tongue5_tmpJnt" -p "tongue4_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".t" -type "double3" 0 0.73556191680060934 2.8421709430404007e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 1.1085356221394229 0 0 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 24.169010636166544 2.0776527139297967 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "tongue6_tmpJnt" -p "tongue5_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".t" -type "double3" 0 0.74023730501491869 -3.5527136788005009e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 24.169010636166544 2.0776527139297967 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "eyeTrgt_tmpJnt" -p "head1_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" 3.3254055259124652e-005 0.77422021214943015 10.632494170993738 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -0.53833200000000003 25.828300000000006 1.4860499999999996 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "eyeTrgt_lft_tmpJnt" -p "eyeTrgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" 1.0281238716753833 0 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0.53833165295886931 25.828273426804412 1.4860476249829009 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "eyeTrgt_rgt_tmpJnt" -p "eyeTrgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" -1.0281238716753833 0 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -0.53833200000000003 25.828300000000006 1.4860499999999996 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "clav_lft_tmpJnt" -p "spine4_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".ove" yes;
	setAttr ".t" -type "double3" 0.69906421108609684 1.5444933889646606 2.1839441134202175 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" 0 0 -89.999999999999986 ;
	setAttr ".bps" -type "matrix" 2.2204460492503131e-016 -1 0 0 1 2.2204460492503131e-016 0 0
		 0 0 1 0 0.40396301701286447 22.628380391977608 0.93191360083425789 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "upArm_lft_tmpJnt" -p "clav_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 0.23302140369656854 4.6604280739073127 -3.2440013806601038 ;
	setAttr ".ro" 4;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" 2.2204460492503131e-016 -1 0 0 1 2.2204460492503131e-016 0 0
		 0 0 1 0 2.6840862727483024 21.950819439051291 -0.34347723857962076 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "foreArm_lft_tmpJnt" -p "upArm_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" -1.8559376258053817e-011 11.23696165375851 -0.18455847921239354 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" -5.9496752756331675e-005 0 0 ;
	setAttr ".bps" -type "matrix" 2.2204460492503131e-016 -1 0 0 0.99999999999946088 2.2204460492491157e-016 -1.0384142298428127e-006 0
		 1.0384142298428127e-006 2.3057427741397801e-022 0.99999999999946088 0 6.7891781465089913 21.950819439060187 -0.42637972484178432 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "wrist_lft_tmpJnt" -p "foreArm_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" -5.9493743265193189e-011 10.051462517543214 -0.038818690697907554 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" 2.2204460492503131e-016 -1 0 0 1 2.2204460492503131e-016 8.3960588285190204e-014 0
		 -8.3960588285190204e-014 -1.8642995655058271e-029 1 0 11.145136555499452 21.950819439088733 -0.34347297829513029 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "hand_lft_tmpJnt" -p "wrist_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" -1.0956568985420745e-011 3.6512104801778413 -1.6223504019641233e-006 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 2.2204460492503131e-016 -1 0 0 1 2.2204460492503131e-016 8.3960588285190204e-014 0
		 -8.3960588285190204e-014 -1.8642995655058271e-029 1 0 11.94334900537585 21.950819439093973 -0.34347375639834526 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "index1_lft_tmpJnt" -p "hand_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".t" -type "double3" -0.39707867703201316 -2.0367360965159627 1.8668575201408932 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" 89.99999999993301 -85.861080851320125 -89.99999999993311 ;
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 11.915961054098767 22.155901831531715 0.36968006095532863 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "index2_lft_tmpJnt" -p "index1_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" 1.5476508963274682e-013 3.1250395818733807 0.10214452065420687 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 13.114219270621863 22.155901831531715 0.36968006095542938 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "index3_lft_tmpJnt" -p "index2_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 4.4408920985006262e-016 1.8951722186324815 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 13.805785441300909 22.155901831531718 0.3696800609554875 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "index4_lft_tmpJnt" -p "index3_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" -3.3306690738754696e-016 1.2192721270569393 -0.14256587046898517 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 14.271394348292739 22.155901831531718 0.36968006095552658 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "index5_lft_tmpJnt" -p "index4_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 5.5511151231257827e-016 1.3258959960118659 -2.1316282072803006e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 14.812322343180313 22.155901831531722 0.36968006095557193 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "middle1_lft_tmpJnt" -p "hand_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".t" -type "double3" -0.53438062404953968 -2.3155928181253458 0.67642447434893027 ;
	setAttr ".r" -type "double3" -1.2722218725854067e-014 1.2722218725854067e-014 -1.4124500153760508e-030 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" 0 -89.999999999995168 0 ;
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 11.876633548480955 22.169748485139639 -0.10080869585328409 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "middle2_lft_tmpJnt" -p "middle1_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" 1.5626389071599078e-013 3.3655647451023931 -2.1316282072803006e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 13.108601055533605 22.169748485139639 -0.10080869585318036 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "middle3_lft_tmpJnt" -p "middle2_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" -2.7755575615628914e-016 2.147599857301973 -0.091365869506702779 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 13.906913312889571 22.169748485139639 -0.10080869585311319 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "middle4_lft_tmpJnt" -p "middle3_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 1.27675647831893e-015 1.3916693564898921 -0.10330270284305243 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 14.411849725499213 22.169748485139639 -0.10080869585307067 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "middle5_lft_tmpJnt" -p "middle4_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" -4.9960036108132044e-016 1.5000505795473558 7.1054273576010019e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 15.081996667416741 22.169748485139635 -0.10080869585301426 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "ring1_lft_tmpJnt" -p "hand_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".t" -type "double3" -0.53475513680430709 -2.3758132294456686 -0.41469561210490918 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" -89.999999999947804 -84.700386817738419 89.999999999948002 ;
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 11.876633548480955 22.13656260093245 -0.5683912766476541 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "ring2_lft_tmpJnt" -p "ring1_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" 1.574296248918472e-013 3.2777981830989731 7.1054273576010019e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 13.097480078235414 22.13656260093245 -0.56839127664755129 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "ring3_lft_tmpJnt" -p "ring2_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 2.2204460492503131e-016 2.0767910682928381 -0.095308086555341731 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 13.845747937749518 22.136562600932457 -0.56839127664748834 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "ring4_lft_tmpJnt" -p "ring3_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 1.9984014443252818e-015 1.3253251718784327 -0.12179550255214622 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 14.345123861710062 22.136562600932454 -0.56839127664744615 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "ring5_lft_tmpJnt" -p "ring4_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 1.9984014443252818e-015 1.4041335739348 7.1054273576010019e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 14.876258587400191 22.136562600932457 -0.56839127664740152 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "pinky1_lft_tmpJnt" -p "hand_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".t" -type "double3" -0.37978079868556591 -2.2266570343551138 -1.6321542239467746 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" -89.999999999969191 -80.961428166628821 89.999999999969518 ;
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 11.876633548480955 22.020988418911262 -0.95907086085240945 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "pinky2_lft_tmpJnt" -p "pinky1_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" 1.5631940186722204e-013 2.6505959023329027 2.1316282072803006e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 13.083100380346423 22.020988418911262 -0.95907086085230775 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "pinky3_lft_tmpJnt" -p "pinky2_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" -2.2204460492503131e-015 1.8361607679212604 7.1054273576010019e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 13.601293073636691 22.020988418911262 -0.95907086085226423 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "pinky4_lft_tmpJnt" -p "pinky3_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 2.6645352591003757e-015 1.1255101442056272 -0.16160203069438239 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 13.978441565540829 22.020988418911262 -0.95907086085223248 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "pinky5_lft_tmpJnt" -p "pinky4_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 8.8817841970012523e-016 1.2065484415781782 2.1316282072803006e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 14.437677801786013 22.020988418911262 -0.95907086085219373 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "thumb1_lft_tmpJnt" -p "hand_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".t" -type "double3" 1.0033924477005982 -1.8520617913846742 1.7331624257721847 ;
	setAttr ".r" -type "double3" 4.880075117341856 -27.621602456918993 -367.35715999936485 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" 23.149018263985358 7.9513867036587939e-016 -36.547276247142648 ;
	setAttr ".bps" -type "matrix" -0.59548586752167076 -0.80336578317847407 -4.9997343752636337e-014 0
		 0.73868319246536318 -0.54754062333671982 0.39312390790336815 0 -0.31582229615899904 0.23409973134130962 0.91948550452673483 0
		 11.518645340649305 21.905429719636764 0.32567524419271171 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "thumb2_lft_tmpJnt" -p "thumb1_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" -0.0077247178717456677 0.80434934109592326 1.0715957764575741 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -0.59548586752167076 -0.80336578317847407 -4.9997343752636337e-014 0
		 0.73868319246536318 -0.54754062333671982 0.39312390790336815 0 -0.31582229615899904 0.23409973134130962 0.91948550452673483 0
		 12.026286103962779 21.529146703032435 0.59583939943602238 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "thumb3_lft_tmpJnt" -p "thumb2_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 0 1.9402316867612068 1.4210854715202004e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -0.59548586752167076 -0.80336578317847407 -4.9997343752636337e-014 0
		 0.73868319246536318 -0.54754062333671982 0.39312390790336815 0 -0.31582229615899904 0.23409973134130962 0.91948550452673483 0
		 12.724953059079672 21.011267666374362 0.96766684389406166 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "thumb4_lft_tmpJnt" -p "thumb3_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" -1.4210854715202004e-014 1.8711655974639161 -1.4210854715202004e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" -0.59548586752167076 -0.80336578317847407 -4.9997343752636337e-014 0
		 0.73868319246536318 -0.54754062333671982 0.39312390790336815 0 -0.31582229615899904 0.23409973134130962 0.91948550452673483 0
		 13.320225968832865 20.570028305053697 1.2844684375111461 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "elbow_lft_tmpJnt" -p "foreArm_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 1.8566481685411418e-011 -3.5527136788005009e-015 -10.425031511471245 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" 0 5.9496752756331688e-005 -90.000000000000014 ;
	setAttr ".bps" -type "matrix" -1 -1.2246467991473532e-016 0 0 1.2246467991473532e-016 -1 0 0
		 0 0 1 0 1.3486489690923307 14.656585133659888 1.5144370407379352e-017 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "clav_rgt_tmpJnt" -p "spine4_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".ove" yes;
	setAttr ".t" -type "double3" -0.69906421108609684 1.5444933889646606 2.183943372509122 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" -180 0 89.999999999999972 ;
	setAttr ".bps" -type "matrix" 2.2204460492503131e-016 -1 0 0 1 2.2204460492503131e-016 0 0
		 0 0 1 0 0.40396301701286447 22.628380391977608 0.93191360083425789 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "upArm_rgt_tmpJnt" -p "clav_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" -0.23302140369536772 -4.6604280739073118 3.2440074715450331 ;
	setAttr ".ro" 4;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" 2.2204460492503131e-016 -1 0 0 1 2.2204460492503131e-016 0 0
		 0 0 1 0 2.6840862727483024 21.950819439051291 -0.34347723857962076 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "foreArm_rgt_tmpJnt" -p "upArm_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 7.6902915878918066e-005 -11.23685485102472 0.18454349635537981 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" 5.9489095021769627e-005 0 0 ;
	setAttr ".bps" -type "matrix" 2.2204460492503131e-016 -1 0 0 0.99999999999946088 2.2204460492491157e-016 -1.0384142298428127e-006 0
		 1.0384142298428127e-006 2.3057427741397801e-022 0.99999999999946088 0 6.7891781465089913 21.950819439060187 -0.42637972484178432 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "wrist_rgt_tmpJnt" -p "foreArm_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" -6.465684899126245e-005 -10.051580369749896 0.038848929452573344 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" 2.2204460492503131e-016 -1 0 0 1 2.2204460492503131e-016 8.3960588285190204e-014 0
		 -8.3960588285190204e-014 -1.8642995655058271e-029 1 0 11.145136555499452 21.950819439088733 -0.34347297829513029 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "hand_rgt_tmpJnt" -p "wrist_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" 4.4352572189154671e-005 -3.6511246755653666 -1.9125025795263539e-006 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 2.2204460492503131e-016 -1 0 0 1 2.2204460492503131e-016 8.3960588285190204e-014 0
		 -8.3960588285190204e-014 -1.8642995655058271e-029 1 0 11.94334900537585 21.950819439093973 -0.34347375639834526 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "index1_rgt_tmpJnt" -p "hand_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".t" -type "double3" 0.39705948574020056 2.03671116019812 -1.8668553313450618 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" 89.99999999993311 -85.861080851320295 -89.99999999993355 ;
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 11.915961054098767 22.155901831531715 0.36968006095532863 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "index2_rgt_tmpJnt" -p "index1_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" -1.1137834806329572e-005 -3.1249299235655776 -0.10220642241173294 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 13.114219270621863 22.155901831531715 0.36968006095542938 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "index3_rgt_tmpJnt" -p "index2_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 5.9528683591381082e-006 -1.8952300365704566 4.3276217404297768e-005 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 13.805785441300909 22.155901831531718 0.3696800609554875 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "index4_rgt_tmpJnt" -p "index3_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 1.1252159827224162e-006 -1.2192830080145356 0.1425509712283386 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 14.271394348292739 22.155901831531718 0.36968006095552658 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "index5_rgt_tmpJnt" -p "index4_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 2.5075340470692709e-007 -1.3258984405409393 1.5276753934756471e-005 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 14.812322343180313 22.155901831531722 0.36968006095557193 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "middle1_rgt_tmpJnt" -p "hand_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".t" -type "double3" 0.53436944665311614 2.3155996660438838 -0.67642374693676166 ;
	setAttr ".r" -type "double3" -1.2722218725854067e-014 1.2722218725854067e-014 -1.4124500153760508e-030 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" 0 -89.999999999995168 0 ;
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 11.876633548480955 22.169748485139639 -0.10080869585328409 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "middle2_rgt_tmpJnt" -p "middle1_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" -2.4730089869251515e-006 -3.3654582274749671 -7.9989421685411344e-005 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 13.108601055533605 22.169748485139639 -0.10080869585318036 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "middle3_rgt_tmpJnt" -p "middle2_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 2.5422965179711099e-006 -2.1476874741936278 0.091420675222153136 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 13.906913312889571 22.169748485139639 -0.10080869585311319 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "middle4_rgt_tmpJnt" -p "middle3_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" -6.4523154597040389e-008 -1.391683103444862 0.10331825963447727 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 14.411849725499213 22.169748485139639 -0.10080869585307067 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "middle5_rgt_tmpJnt" -p "middle4_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 1.3399933465052527e-006 -1.5000814300834193 -6.5811203739940538e-005 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 15.081996667416741 22.169748485139635 -0.10080869585301426 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "ring1_rgt_tmpJnt" -p "hand_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".t" -type "double3" 0.53473027171662579 2.3757649728993542 0.41469813893326146 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" -89.999999999947732 -84.700386817738263 89.999999999948002 ;
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 11.876633548480955 22.13656260093245 -0.5683912766476541 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "ring2_rgt_tmpJnt" -p "ring1_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" 7.9104412549213521e-006 -3.2776770237547517 -6.6862939149814338e-005 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 13.097480078235414 22.13656260093245 -0.56839127664755129 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "ring3_rgt_tmpJnt" -p "ring2_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" -4.3586981797183455e-006 -2.0768398329283819 0.095332466180806819 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 13.845747937749518 22.136562600932457 -0.56839127664748834 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "ring4_rgt_tmpJnt" -p "ring3_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 1.2015040451451853e-006 -1.3253228383723297 0.12180850202035742 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 14.345123861710062 22.136562600932454 -0.56839127664744615 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "ring5_rgt_tmpJnt" -p "ring4_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" -2.1577332471878208e-006 -1.4041176899337557 5.434676592130927e-005 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 14.876258587400191 22.136562600932457 -0.56839127664740152 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "pinky1_rgt_tmpJnt" -p "hand_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".t" -type "double3" 0.37981757328609689 2.2267172407703235 1.6321586936442878 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" -89.999999999969106 -80.96142816662865 89.999999999969518 ;
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 11.876633548480955 22.020988418911262 -0.95907086085240945 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "pinky2_rgt_tmpJnt" -p "pinky1_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" -6.6344258904393882e-006 -2.6505768622901158 -1.0901003406615928e-006 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 13.083100380346423 22.020988418911262 -0.95907086085230775 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "pinky3_rgt_tmpJnt" -p "pinky2_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" -9.6105686724357042e-006 -1.8362236171720347 3.857723739741914e-005 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 13.601293073636691 22.020988418911262 -0.95907086085226423 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "pinky4_rgt_tmpJnt" -p "pinky3_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" -4.1730761495273327e-006 -1.1255026368305465 0.1616024041911075 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 13.978441565540829 22.020988418911262 -0.95907086085223248 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "pinky5_rgt_tmpJnt" -p "pinky4_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 9.5177767587273365e-006 -1.206558695231692 2.8909452467473784e-006 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 14.437677801786013 22.020988418911262 -0.95907086085219373 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "thumb1_rgt_tmpJnt" -p "hand_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".t" -type "double3" -1.0033390523463765 1.8521506184788805 -1.7331653702727774 ;
	setAttr ".r" -type "double3" 4.880075117341856 -27.621602456918993 -367.35715999936485 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" 23.149018263985226 0 -36.547276247142655 ;
	setAttr ".bps" -type "matrix" -0.59548586752167076 -0.80336578317847407 -4.9997343752636337e-014 0
		 0.73868319246536318 -0.54754062333671982 0.39312390790336815 0 -0.31582229615899904 0.23409973134130962 0.91948550452673483 0
		 11.518645340649305 21.905429719636764 0.32567524419271171 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "thumb2_rgt_tmpJnt" -p "thumb1_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" 0.0077828517356621774 -0.80448993365885357 -1.0715358581987218 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -0.59548586752167076 -0.80336578317847407 -4.9997343752636337e-014 0
		 0.73868319246536318 -0.54754062333671982 0.39312390790336815 0 -0.31582229615899904 0.23409973134130962 0.91948550452673483 0
		 12.026286103962779 21.529146703032435 0.59583939943602238 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "thumb3_rgt_tmpJnt" -p "thumb2_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" -7.1860445849836196e-005 -1.9400895096591748 -5.5587117024913368e-005 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -0.59548586752167076 -0.80336578317847407 -4.9997343752636337e-014 0
		 0.73868319246536318 -0.54754062333671982 0.39312390790336815 0 -0.31582229615899904 0.23409973134130962 0.91948550452673483 0
		 12.724953059079672 21.011267666374362 0.96766684389406166 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "thumb4_rgt_tmpJnt" -p "thumb3_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 4.0132864057795814e-005 -1.8712390622527915 2.4758073490005472e-005 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" -0.59548586752167076 -0.80336578317847407 -4.9997343752636337e-014 0
		 0.73868319246536318 -0.54754062333671982 0.39312390790336815 0 -0.31582229615899904 0.23409973134130962 0.91948550452673483 0
		 13.320225968832865 20.570028305053697 1.2844684375111461 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "elbow_rgt_tmpJnt" -p "foreArm_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 1.3751705516540369e-005 8.144894019324056e-005 10.425027639502469 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" 0.00011898431608851722 -5.9489094801861205e-005 -90.000000000000028 ;
	setAttr ".bps" -type "matrix" -1 -1.2246467991473532e-016 0 0 1.2246467991473532e-016 -1 0 0
		 0 0 1 0 1.3486489690923307 14.656585133659888 1.5144370407379352e-017 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "pelvis_tmpJnt" -p "root_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" 0 -0.98442014341870987 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 15.4830611404753 0 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "upLeg_lft_tmpJnt" -p "pelvis_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 4.1363087971254435 -0.15075311660130808 -0.12335000285253746 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" 0 0 -180 ;
	setAttr ".bps" -type "matrix" -1 -1.2246467991473532e-016 0 0 1.2246467991473532e-016 -1 0 0
		 0 0 1 0 1.3486489690923307 14.656585133659888 1.5144370407379352e-017 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "lowLeg_lft_tmpJnt" -p "upLeg_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 2.6645352591003757e-015 13.07338963976574 0.41850626395453244 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -1 -1.2246467991473532e-016 0 0 1.2246467991473532e-016 -1 0 0
		 0 0 1 0 1.3486489690923305 7.7535119809750004 0.15259281481888803 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "knee_lft_tmpJnt" -p "lowLeg_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" -1.7763568394002505e-015 0 10.425031511471248 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -1 -1.2246467991473532e-016 0 0 1.2246467991473532e-016 -1 0 0
		 0 0 1 0 1.3486489690923307 14.656585133659888 1.5144370407379352e-017 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "ankle_lft_tmpJnt" -p "lowLeg_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 0 14.209012486474023 -1.686006016533546 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -1 -1.2246467991473532e-016 0 0 1.2246467991473532e-016 -1 0 0
		 0 0 1 0 1.348648969092332 1.0615915425310156 0 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "ball_lft_tmpJnt" -p "ankle_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 4.4408920985006262e-015 2.2335635584389988 4.255195706512521 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" 89.999999999999986 0 0 ;
	setAttr ".bps" -type "matrix" -1 -1.2246467991473532e-016 0 0 2.7192621468937821e-032 -2.2204460492503131e-016 1 0
		 -1.2246467991473532e-016 1 2.2204460492503131e-016 0 1.3486489690923311 5.5511151231257827e-015 1.476681409489522 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "toe_lft_tmpJnt" -p "ball_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" 0 4.4501948831511342 9.8532293435482643e-016 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" -1 -1.2246467991473532e-016 0 0 2.7192621468937821e-032 -2.2204460492503131e-016 1 0
		 -1.2246467991473532e-016 1 2.2204460492503131e-016 0 1.3486489690923309 5.0024078434460801e-015 3.3551152167720435 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "thumbFoot1_lft_tmpJnt" -p "toe_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 0.76132879246347951 -2.5231679737403567 0.78782494777233847 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 0 2.0913097891518781e-006 ;
	setAttr ".bps" -type "matrix" -0.99999999999999911 -1.887412057596498e-008 3.6500241499888554e-008 0
		 3.6500241499888581e-008 -1.4172455962248266e-015 0.99999999999999933 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 14.483562648086886 3.3809123766256222 25.20550640416533 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "thumbFoot2_lft_tmpJnt" -p "thumbFoot1_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 2.6645352591003757e-015 0.92576787951842121 -3.1086244689504383e-015 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999999999999911 -1.887412057596498e-008 3.6500241499888554e-008 0
		 3.6500241499888581e-008 -1.4172455962248266e-015 0.99999999999999933 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 14.483562389251162 3.3809123766256186 18.1141638774779 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "thumbFoot3_lft_tmpJnt" -p "thumbFoot2_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" -1.7763568394002505e-015 0.47930406946157866 3.1086244689504383e-015 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999999999999911 -1.887412057596498e-008 3.6500241499888554e-008 0
		 3.6500241499888581e-008 -1.4172455962248266e-015 0.99999999999999933 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 14.483562464328882 3.3809123766256284 20.17107393750047 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "thumbFoot4_lft_tmpJnt" -p "thumbFoot3_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" -4.4408920985006262e-016 1.1731305201917674 3.3306690738754696e-016 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999999999999911 -1.887412057596498e-008 3.6500241499888554e-008 0
		 3.6500241499888581e-008 -1.4172455962248266e-015 0.99999999999999933 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 14.483562648086886 3.3809123766256222 25.20550640416533 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "indexFoot1_lft_tmpJnt" -p "toe_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" -0.44366540550111022 -2.341515342403814 0.79850334598006101 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 0 2.0913097891518781e-006 ;
	setAttr ".bps" -type "matrix" -0.99999999999999911 -1.887412057596498e-008 3.6500241499888554e-008 0
		 3.6500241499888581e-008 -1.4172455962248266e-015 0.99999999999999933 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 19.654736497602165 3.0356842493631335 24.950677281414283 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "indexFoot2_lft_tmpJnt" -p "indexFoot1_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 2.7149900105882807e-009 0.91972559424033395 -0.016453541419196638 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999999999999911 -1.887412057596498e-008 3.6500241499888554e-008 0
		 3.6500241499888581e-008 -1.4172455962248266e-015 0.99999999999999933 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 19.654736265256656 3.5823867347681335 18.867787120980601 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "indexFoot3_lft_tmpJnt" -p "indexFoot2_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" -9.9510444329098391e-010 0.51124777198153026 -0.074670399598353532 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999999999999911 -1.887412057596498e-008 3.6500241499888554e-008 0
		 3.6500241499888581e-008 -1.4172455962248266e-015 0.99999999999999933 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 19.654736355656542 3.0356842493631389 21.06178202391191 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "indexFoot4_lft_tmpJnt" -p "indexFoot3_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" -2.6645352591003757e-015 0.90619583172745344 0 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999999999999911 -1.887412057596498e-008 3.6500241499888554e-008 0
		 3.6500241499888581e-008 -1.4172455962248266e-015 0.99999999999999933 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 19.654736497602165 3.0356842493631335 24.950677281414283 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "middleFoot1_lft_tmpJnt" -p "toe_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" -1.4555465493447466 -2.5212158691836111 0.78581149610921242 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 0 -2.2499929316109859 ;
	setAttr ".bps" -type "matrix" -0.99922904108406752 -1.8859569321739476e-008 -0.03925969248751842 0
		 -0.039259692487518413 -7.4099427431652264e-010 0.99922904108406774 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 23.632825854991474 2.5509665919724842 23.422981436211693 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "middleFoot2_lft_tmpJnt" -p "middleFoot1_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 5.5441438107095564e-009 0.97308055092308265 -0.062194519388097835 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99922904108406752 -1.8859569321739476e-008 -0.03925969248751842 0
		 -0.039259692487518413 -7.4099427431652264e-010 0.99922904108406774 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 23.833229097571561 3.545618914658315 18.322361793410607 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "middleFoot3_lft_tmpJnt" -p "middleFoot2_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" -1.9347714541595451e-009 0.55856531325370451 -0.12918715907726763 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99922904108406752 -1.8859569321739476e-008 -0.03925969248751842 0
		 -0.039259692487518413 -7.4099427431652264e-010 0.99922904108406774 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 23.739121441737378 2.5509665939787247 20.717569587931241 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "middleFoot4_lft_tmpJnt" -p "middleFoot3_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 0 0.63090526850224382 1.1102230246251565e-016 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99922904108406752 -1.8859569321739476e-008 -0.03925969248751842 0
		 -0.039259692487518413 -7.4099427431652264e-010 0.99922904108406774 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 23.632825854991474 2.5509665919724842 23.422981436211693 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "ringFoot1_lft_tmpJnt" -p "toe_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" -2.2817717567190821 -2.8045332539710097 0.74008395476184219 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 0 -2.2499929316109859 ;
	setAttr ".bps" -type "matrix" -0.99922904108406752 -1.8859569321739476e-008 -0.03925969248751842 0
		 -0.039259692487518413 -7.4099427431652264e-010 0.99922904108406774 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 27.225828301514859 2.8802883702174338 21.003340089239849 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "ringFoot2_lft_tmpJnt" -p "ringFoot1_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 1.2997105613976601e-009 0.92072713237502946 0.013077546184747324 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99922904108406752 -1.8859569321739476e-008 -0.03925969248751842 0
		 -0.039259692487518413 -7.4099427431652264e-010 0.99922904108406774 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 27.387754888646541 3.2321558806201516 16.882020110151402 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "ringFoot3_lft_tmpJnt" -p "ringFoot2_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" -8.8817841970012523e-016 0.33019146419784917 -0.081992660476667978 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99922904108406752 -1.8859569321739476e-008 -0.03925969248751842 0
		 -0.039259692487518413 -7.4099427431652264e-010 0.99922904108406774 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 27.332123888260764 2.8802883722236743 18.297928240959397 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "ringFoot4_lft_tmpJnt" -p "ringFoot3_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 0 0.63090526850224471 1.1102230246251565e-016 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99922904108406752 -1.8859569321739476e-008 -0.03925969248751842 0
		 -0.039259692487518413 -7.4099427431652264e-010 0.99922904108406774 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 27.225828301514859 2.8802883702174338 21.003340089239849 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "pinkyFoot1_lft_tmpJnt" -p "toe_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" -3.1827159919081431 -3.4222721419268178 0.47581906007169 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 0 -8.3039791409633299 ;
	setAttr ".bps" -type "matrix" -0.98951576127278995 -1.8676239485899002e-008 -0.14442492233936113 0
		 -0.14442492233936108 -2.7258954824815882e-009 0.98951576127279017 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 30.320681697400357 2.0419542914872575 17.740972323318903 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "pinkyFoot2_lft_tmpJnt" -p "pinkyFoot1_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 6.2172489379008766e-015 0.58181578466233619 -3.1086244689504383e-015 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.98951576127278995 -1.8676239485899002e-008 -0.14442492233936116 0
		 -0.14442492233936111 -2.7258954824815886e-009 0.98951576127279017 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 31.048633193268319 2.0419543052267 12.753471087826441 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "pinkyFoot3_lft_tmpJnt" -p "pinkyFoot2_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" -4.4408920985006262e-015 0.54360309640941562 2.9420910152566648e-015 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.98951576127278995 -1.8676239485899002e-008 -0.14442492233936113 0
		 -0.14442492233936108 -2.7258954824815882e-009 0.98951576127279017 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 30.711712061830106 2.0419542988676174 15.06185917254086 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "pinkyFoot4_lft_tmpJnt" -p "pinkyFoot3_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" -8.8817841970012523e-016 0.63090526850224382 -1.1102230246251565e-016 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.98951576127278995 -1.8676239485899002e-008 -0.14442492233936113 0
		 -0.14442492233936108 -2.7258954824815882e-009 0.98951576127279017 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 30.320681697400357 2.0419542914872575 17.740972323318903 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "footIn_lft_tmpJnt" -p "ankle_lft_tmpJnt";
	setAttr ".t" -type "double3" 2.155776902031032 2.2134250566394305 2.6909978842447333 ;
	setAttr ".jo" -type "double3" 0 0 180 ;
	setAttr ".radi" 0.4;
createNode joint -n "footOut_lft_tmpJnt" -p "ankle_lft_tmpJnt";
	setAttr ".t" -type "double3" -3.7979827154182235 2.2134250566394393 2.6942848725059934 ;
	setAttr ".jo" -type "double3" 0 0 180 ;
	setAttr ".radi" 0.4;
createNode joint -n "heel_lft_tmpJnt" -p "ankle_lft_tmpJnt";
	setAttr ".t" -type "double3" 3.5527136788005009e-015 2.21342505663943 -2.1164566422265709 ;
	setAttr ".jo" -type "double3" 0 0 180 ;
	setAttr ".radi" 0.4;
createNode joint -n "upLeg_rgt_tmpJnt" -p "pelvis_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" -4.13631 -0.15074856423238714 -0.12335004960428167 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" 0 180 0 ;
	setAttr ".bps" -type "matrix" -1 -1.2246467991473532e-016 0 0 1.2246467991473532e-016 -1 0 0
		 0 0 1 0 1.3486489690923307 14.656585133659888 1.5144370407379352e-017 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "lowLeg_rgt_tmpJnt" -p "upLeg_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 0 -13.073400000000003 -0.41850619999999999 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -1 -1.2246467991473532e-016 0 0 1.2246467991473532e-016 -1 0 0
		 0 0 1 0 1.3486489690923305 7.7535119809750004 0.15259281481888803 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "knee_rgt_tmpJnt" -p "lowLeg_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" -8.8817841970012523e-016 0 -10.425004999999999 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -1 -1.2246467991473532e-016 0 0 1.2246467991473532e-016 -1 0 0
		 0 0 1 0 1.3486489690923307 14.656585133659888 1.5144370407379352e-017 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "ankle_rgt_tmpJnt" -p "lowLeg_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 0 -14.20901 1.686005 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -1 -1.2246467991473532e-016 0 0 1.2246467991473532e-016 -1 0 0
		 0 0 1 0 1.348648969092332 1.0615915425310156 0 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "ball_rgt_tmpJnt" -p "ankle_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 0 -2.2335601999999994 -4.25519 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" 89.999999999999986 1.403341859706975e-014 -1.4124500153760508e-030 ;
	setAttr ".bps" -type "matrix" -1 -1.2246467991473532e-016 0 0 2.7192621468937821e-032 -2.2204460492503131e-016 1 0
		 -1.2246467991473532e-016 1 2.2204460492503131e-016 0 1.3486489690923311 5.5511151231257827e-015 1.476681409489522 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "toe_rgt_tmpJnt" -p "ball_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" -1.7763568394002505e-015 -4.4502 -9.4368957093138306e-016 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" -1 -1.2246467991473532e-016 0 0 2.7192621468937821e-032 -2.2204460492503131e-016 1 0
		 -1.2246467991473532e-016 1 2.2204460492503131e-016 0 1.3486489690923309 5.0024078434460801e-015 3.3551152167720435 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "thumbFoot1_rgt_tmpJnt" -p "toe_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" -0.76132999999999962 2.5231700000000012 -0.78782519999999934 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 2.2588727439225493e-006 8.2449400394057175e-014 1.3486420626726041e-021 ;
	setAttr ".bps" -type "matrix" -0.99999999999999911 -1.887412057596498e-008 3.6500241499888554e-008 0
		 3.6500241499888581e-008 -1.4172455962248266e-015 0.99999999999999933 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 14.483562648086886 3.3809123766256222 25.20550640416533 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "thumbFoot2_rgt_tmpJnt" -p "thumbFoot1_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" -4.4408920985006262e-016 -0.92577000000000087 3.6498266187123818e-008 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 3.0783246591999735e-006 1.1235959309887553e-013 -1.7883704425214774e-021 ;
	setAttr ".bps" -type "matrix" -0.99999999999999911 -1.887412057596498e-008 3.6500241499888554e-008 0
		 3.6500241499888581e-008 -1.4172455962248266e-015 0.99999999999999933 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 14.483562389251162 3.3809123766256186 18.1141638774779 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "thumbFoot3_rgt_tmpJnt" -p "thumbFoot2_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 0 -0.47929999999999762 4.4647594066304919e-008 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 5.7272877312492664e-006 2.0904738462855707e-013 -9.7263117033715992e-021 ;
	setAttr ".bps" -type "matrix" -0.99999999999999911 -1.887412057596498e-008 3.6500241499888554e-008 0
		 3.6500241499888581e-008 -1.4172455962248266e-015 0.99999999999999933 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 14.483562464328882 3.3809123766256284 20.17107393750047 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "thumbFoot4_rgt_tmpJnt" -p "thumbFoot3_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 0 -1.1731299999999782 2.2654512343844857e-007 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 1.1262047877356559e-005 4.1106746592761763e-013 -4.0361465724772158e-020 ;
	setAttr ".bps" -type "matrix" -0.99999999999999911 -1.887412057596498e-008 3.6500241499888554e-008 0
		 3.6500241499888581e-008 -1.4172455962248266e-015 0.99999999999999933 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 14.483562648086886 3.3809123766256222 25.20550640416533 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "indexFoot1_rgt_tmpJnt" -p "toe_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 0.44366000000000039 2.3415200000000009 -0.7985031999999993 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 2.2588727439225493e-006 8.2449400394057175e-014 1.3486420626726041e-021 ;
	setAttr ".bps" -type "matrix" -0.99999999999999911 -1.887412057596498e-008 3.6500241499888554e-008 0
		 3.6500241499888581e-008 -1.4172455962248266e-015 0.99999999999999933 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 19.654736497602165 3.0356842493631335 24.950677281414283 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "indexFoot2_rgt_tmpJnt" -p "indexFoot1_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 0 -0.91972999935134325 0.016453036260140719 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 3.0783246591999735e-006 1.1235959309887553e-013 -1.7883704425214774e-021 ;
	setAttr ".bps" -type "matrix" -0.99999999999999911 -1.887412057596498e-008 3.6500241499888554e-008 0
		 3.6500241499888581e-008 -1.4172455962248266e-015 0.99999999999999933 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 19.654736265256656 3.5823867347681335 18.867787120980601 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "indexFoot3_rgt_tmpJnt" -p "indexFoot2_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" -8.8817841970012523e-016 -0.51124999304426932 0.07467104762378951 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 5.7272877312492664e-006 2.0904738462855707e-013 -9.7263117033715992e-021 ;
	setAttr ".bps" -type "matrix" -0.99999999999999911 -1.887412057596498e-008 3.6500241499888554e-008 0
		 3.6500241499888581e-008 -1.4172455962248266e-015 0.99999999999999933 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 19.654736355656542 3.0356842493631389 21.06178202391191 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "indexFoot4_rgt_tmpJnt" -p "indexFoot3_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 8.8817841970012523e-016 -0.90618999999998362 1.7499588722547088e-007 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 1.1262047877356559e-005 4.1106746592761763e-013 -4.0361465724772158e-020 ;
	setAttr ".bps" -type "matrix" -0.99999999999999911 -1.887412057596498e-008 3.6500241499888554e-008 0
		 3.6500241499888581e-008 -1.4172455962248266e-015 0.99999999999999933 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 19.654736497602165 3.0356842493631335 24.950677281414283 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "middleFoot1_rgt_tmpJnt" -p "toe_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 1.4555499999999997 2.5212199999999996 -0.78581119999999927 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" -1.3768380943749832e-016 7.0112997039405498e-015 -2.2499929316109561 ;
	setAttr ".bps" -type "matrix" -0.99922904108406752 -1.8859569321739476e-008 -0.03925969248751842 0
		 -0.039259692487518413 -7.4099427431652264e-010 0.99922904108406774 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 23.632825854991474 2.5509665919724842 23.422981436211693 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "middleFoot2_rgt_tmpJnt" -p "middleFoot1_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" -7.1648634332177608e-006 -0.973080486367218 0.062193999999999749 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99922904108406752 -1.8859569321739476e-008 -0.03925969248751842 0
		 -0.039259692487518413 -7.4099427431652264e-010 0.99922904108406774 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 23.833229097571561 3.545618914658315 18.322361793410607 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "middleFoot3_rgt_tmpJnt" -p "middleFoot2_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" -6.881059908536713e-007 -0.55857066204691286 0.12918699999999983 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99922904108406752 -1.8859569321739476e-008 -0.03925969248751842 0
		 -0.039259692487518413 -7.4099427431652264e-010 0.99922904108406774 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 23.739121441737378 2.5509665939787247 20.717569587931241 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "middleFoot4_rgt_tmpJnt" -p "middleFoot3_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" -8.0800967072747198e-007 -0.63090643466313345 1.1102230246251565e-016 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99922904108406752 -1.8859569321739476e-008 -0.03925969248751842 0
		 -0.039259692487518413 -7.4099427431652264e-010 0.99922904108406774 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 23.632825854991474 2.5509665919724842 23.422981436211693 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "ringFoot1_rgt_tmpJnt" -p "toe_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 2.28177 2.8045299999999997 -0.7400841999999993 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" -1.3768380943749832e-016 7.0112997039405498e-015 -2.2499929316109561 ;
	setAttr ".bps" -type "matrix" -0.99922904108406752 -1.8859569321739476e-008 -0.03925969248751842 0
		 -0.039259692487518413 -7.4099427431652264e-010 0.99922904108406774 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 27.225828301514859 2.8802883702174338 21.003340089239849 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "ringFoot2_rgt_tmpJnt" -p "ringFoot1_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" -2.8201497483948401e-006 -0.9207199479711754 -0.013077000000000116 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99922904108406752 -1.8859569321739476e-008 -0.03925969248751842 0
		 -0.039259692487518413 -7.4099427431652264e-010 0.99922904108406774 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 27.387754888646541 3.2321558806201516 16.882020110151402 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "ringFoot3_rgt_tmpJnt" -p "ringFoot2_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 3.3345668821738172e-006 -0.33019443542991667 0.081991999999999954 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99922904108406752 -1.8859569321739476e-008 -0.03925969248751842 0
		 -0.039259692487518413 -7.4099427431652264e-010 0.99922904108406774 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 27.332123888260764 2.8802883722236743 18.297928240959397 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "ringFoot4_rgt_tmpJnt" -p "ringFoot3_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" -8.080096716156504e-007 -0.63090643466313434 -2.2204460492503131e-016 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99922904108406752 -1.8859569321739476e-008 -0.03925969248751842 0
		 -0.039259692487518413 -7.4099427431652264e-010 0.99922904108406774 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 27.225828301514859 2.8802883702174338 21.003340089239849 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "pinkyFoot1_rgt_tmpJnt" -p "toe_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 3.18271 3.42227 -0.47581919999999911 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 8.537736457475702e-007 6.9431442410211058e-015 -8.3039791409633246 ;
	setAttr ".bps" -type "matrix" -0.98951576127278995 -1.8676239485899002e-008 -0.14442492233936113 0
		 -0.14442492233936108 -2.7258954824815882e-009 0.98951576127279017 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 30.320681697400357 2.0419542914872575 17.740972323318903 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "pinkyFoot2_rgt_tmpJnt" -p "pinkyFoot1_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 7.7577778538895359e-006 -0.5818087008973114 -1.1102230246251565e-016 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.98951576127278995 -1.8676239485899002e-008 -0.14442492233936116 0
		 -0.14442492233936111 -2.7258954824815886e-009 0.98951576127279017 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 31.048633193268319 2.0419543052267 12.753471087826441 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "pinkyFoot3_rgt_tmpJnt" -p "pinkyFoot2_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 7.2755803870450109e-007 -0.54360922379910992 -1.1102230246251565e-016 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.98951576127278995 -1.8676239485899002e-008 -0.14442492233936113 0
		 -0.14442492233936108 -2.7258954824815882e-009 0.98951576127279017 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 30.711712061830106 2.0419542988676174 15.06185917254086 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "pinkyFoot4_rgt_tmpJnt" -p "pinkyFoot3_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" -1.6413999368580789e-006 -0.63090479352855278 -1.6653345369377348e-016 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.98951576127278995 -1.8676239485899002e-008 -0.14442492233936113 0
		 -0.14442492233936108 -2.7258954824815882e-009 0.98951576127279017 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 30.320681697400357 2.0419542914872575 17.740972323318903 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "footIn_rgt_tmpJnt" -p "ankle_rgt_tmpJnt";
	setAttr ".t" -type "double3" -2.15578 -2.2134217349999994 -2.691 ;
	setAttr ".jo" -type "double3" 7.016709298534876e-015 -2.1050127895604626e-014 180 ;
	setAttr ".radi" 0.4;
createNode joint -n "footOut_rgt_tmpJnt" -p "ankle_rgt_tmpJnt";
	setAttr ".t" -type "double3" 3.79798 -2.2134217349999994 -2.6942800000000005 ;
	setAttr ".jo" -type "double3" 7.016709298534876e-015 -2.1050127895604626e-014 180 ;
	setAttr ".radi" 0.4;
createNode joint -n "heel_rgt_tmpJnt" -p "ankle_rgt_tmpJnt";
	setAttr ".t" -type "double3" 0 -2.2134217349999994 2.11646 ;
	setAttr ".jo" -type "double3" 7.016709298534876e-015 -2.1050127895604626e-014 180 ;
	setAttr ".radi" 0.4;
select -ne :time1;
	setAttr -av -k on ".cch";
	setAttr -av -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 1;
	setAttr -av ".unw" 1;
	setAttr -k on ".etw";
	setAttr -k on ".tps";
	setAttr -k on ".tms";
select -ne :renderPartition;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 8 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
select -ne :renderGlobalsList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
select -ne :defaultShaderList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".s";
select -ne :postProcessList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
	setAttr -k on ".ihi";
select -ne :initialShadingGroup;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -s 3 ".gn";
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -k on ".mico";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -k on ".micr";
	setAttr -k on ".micg";
	setAttr -k on ".micb";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
select -ne :initialParticleSE;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -k on ".mico";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -k on ".micr";
	setAttr -k on ".micg";
	setAttr -k on ".micb";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
select -ne :defaultResolution;
	setAttr -av -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -av -k on ".w";
	setAttr -av -k on ".h";
	setAttr -av -k on ".pa" 1;
	setAttr -av -k on ".al";
	setAttr -av -k on ".dar";
	setAttr -av -k on ".ldar";
	setAttr -k on ".dpi";
	setAttr -av -k on ".off";
	setAttr -av -k on ".fld";
	setAttr -av -k on ".zsl";
	setAttr -k on ".isu";
	setAttr -k on ".pdu";
select -ne :hardwareRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k off -cb on ".ctrs" 256;
	setAttr -av -k off -cb on ".btrs" 512;
	setAttr -k off -cb on ".fbfm";
	setAttr -k off -cb on ".ehql";
	setAttr -k off -cb on ".eams";
	setAttr -k off -cb on ".eeaa";
	setAttr -k off -cb on ".engm";
	setAttr -k off -cb on ".mes";
	setAttr -k off -cb on ".emb";
	setAttr -av -k off -cb on ".mbbf";
	setAttr -k off -cb on ".mbs";
	setAttr -k off -cb on ".trm";
	setAttr -k off -cb on ".tshc";
	setAttr -k off -cb on ".enpt";
	setAttr -k off -cb on ".clmt";
	setAttr -k off -cb on ".tcov";
	setAttr -k off -cb on ".lith";
	setAttr -k off -cb on ".sobc";
	setAttr -k off -cb on ".cuth";
	setAttr -k off -cb on ".hgcd";
	setAttr -k off -cb on ".hgci";
	setAttr -k off -cb on ".mgcs";
	setAttr -k off -cb on ".twa";
	setAttr -k off -cb on ".twz";
	setAttr -cb on ".hwcc";
	setAttr -cb on ".hwdp";
	setAttr -cb on ".hwql";
	setAttr -k on ".hwfr";
	setAttr -k on ".soll";
	setAttr -k on ".sosl";
	setAttr -k on ".bswa";
	setAttr -k on ".shml";
	setAttr -k on ".hwel";
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
select -ne :defaultHardwareRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -av -k on ".rp";
	setAttr -k on ".cai";
	setAttr -k on ".coi";
	setAttr -cb on ".bc";
	setAttr -av -k on ".bcb";
	setAttr -av -k on ".bcg";
	setAttr -av -k on ".bcr";
	setAttr -k on ".ei";
	setAttr -av -k on ".ex";
	setAttr -av -k on ".es";
	setAttr -av -k on ".ef";
	setAttr -av -k on ".bf";
	setAttr -k on ".fii";
	setAttr -av -k on ".sf";
	setAttr -k on ".gr";
	setAttr -k on ".li";
	setAttr -k on ".ls";
	setAttr -av -k on ".mb";
	setAttr -k on ".ti";
	setAttr -k on ".txt";
	setAttr -k on ".mpr";
	setAttr -k on ".wzd";
	setAttr -k on ".fn";
	setAttr -k on ".if";
	setAttr -k on ".res" -type "string" "ntsc_4d 646 485 1.333";
	setAttr -k on ".as";
	setAttr -k on ".ds";
	setAttr -k on ".lm";
	setAttr -av -k on ".fir";
	setAttr -k on ".aap";
	setAttr -av -k on ".gh";
	setAttr -cb on ".sd";
connectAttr "root_tmpJnt.s" "spine1_tmpJnt.is";
connectAttr "spine1_tmpJnt.s" "spine2_tmpJnt.is";
connectAttr "spine2_tmpJnt.s" "spine3_tmpJnt.is";
connectAttr "spine3_tmpJnt.s" "spine4_tmpJnt.is";
connectAttr "spine4_tmpJnt.s" "neck1_tmpJnt.is";
connectAttr "neck1_tmpJnt.s" "head1_tmpJnt.is";
connectAttr "head1_tmpJnt.s" "head2_tmpJnt.is";
connectAttr "head1_tmpJnt.s" "eye_lft_tmpJnt.is";
connectAttr "head1_tmpJnt.s" "eye_rgt_tmpJnt.is";
connectAttr "head1_tmpJnt.s" "jaw1_upr_tmpJnt.is";
connectAttr "jaw1_upr_tmpJnt.s" "jaw2_upr_tmpJnt.is";
connectAttr "jaw1_upr_tmpJnt.s" "teeth_upr_tmpJnt.is";
connectAttr "head1_tmpJnt.s" "jaw1_lwr_tmpJnt.is";
connectAttr "jaw1_lwr_tmpJnt.s" "jaw2_lwr_tmpJnt.is";
connectAttr "jaw2_lwr_tmpJnt.s" "jaw3_lwr_tmpJnt.is";
connectAttr "jaw2_lwr_tmpJnt.s" "teeth_lwr_tmpJnt.is";
connectAttr "jaw1_lwr_tmpJnt.s" "tongue1_tmpJnt.is";
connectAttr "tongue1_tmpJnt.s" "tongue2_tmpJnt.is";
connectAttr "tongue2_tmpJnt.s" "tongue3_tmpJnt.is";
connectAttr "tongue4_tmpJnt.s" "tongue5_tmpJnt.is";
connectAttr "head1_tmpJnt.s" "eyeTrgt_tmpJnt.is";
connectAttr "eyeTrgt_tmpJnt.s" "eyeTrgt_lft_tmpJnt.is";
connectAttr "eyeTrgt_tmpJnt.s" "eyeTrgt_rgt_tmpJnt.is";
connectAttr "spine4_tmpJnt.s" "clav_lft_tmpJnt.is";
connectAttr "clav_lft_tmpJnt.s" "upArm_lft_tmpJnt.is";
connectAttr "upArm_lft_tmpJnt.s" "foreArm_lft_tmpJnt.is";
connectAttr "foreArm_lft_tmpJnt.s" "wrist_lft_tmpJnt.is";
connectAttr "wrist_lft_tmpJnt.s" "hand_lft_tmpJnt.is";
connectAttr "hand_lft_tmpJnt.s" "index1_lft_tmpJnt.is";
connectAttr "index1_lft_tmpJnt.s" "index2_lft_tmpJnt.is";
connectAttr "index2_lft_tmpJnt.s" "index3_lft_tmpJnt.is";
connectAttr "index3_lft_tmpJnt.s" "index4_lft_tmpJnt.is";
connectAttr "index4_lft_tmpJnt.s" "index5_lft_tmpJnt.is";
connectAttr "hand_lft_tmpJnt.s" "middle1_lft_tmpJnt.is";
connectAttr "middle1_lft_tmpJnt.s" "middle2_lft_tmpJnt.is";
connectAttr "middle2_lft_tmpJnt.s" "middle3_lft_tmpJnt.is";
connectAttr "middle3_lft_tmpJnt.s" "middle4_lft_tmpJnt.is";
connectAttr "middle4_lft_tmpJnt.s" "middle5_lft_tmpJnt.is";
connectAttr "hand_lft_tmpJnt.s" "ring1_lft_tmpJnt.is";
connectAttr "ring1_lft_tmpJnt.s" "ring2_lft_tmpJnt.is";
connectAttr "ring2_lft_tmpJnt.s" "ring3_lft_tmpJnt.is";
connectAttr "ring3_lft_tmpJnt.s" "ring4_lft_tmpJnt.is";
connectAttr "ring4_lft_tmpJnt.s" "ring5_lft_tmpJnt.is";
connectAttr "hand_lft_tmpJnt.s" "pinky1_lft_tmpJnt.is";
connectAttr "pinky1_lft_tmpJnt.s" "pinky2_lft_tmpJnt.is";
connectAttr "pinky2_lft_tmpJnt.s" "pinky3_lft_tmpJnt.is";
connectAttr "pinky3_lft_tmpJnt.s" "pinky4_lft_tmpJnt.is";
connectAttr "pinky4_lft_tmpJnt.s" "pinky5_lft_tmpJnt.is";
connectAttr "hand_lft_tmpJnt.s" "thumb1_lft_tmpJnt.is";
connectAttr "thumb1_lft_tmpJnt.s" "thumb2_lft_tmpJnt.is";
connectAttr "thumb2_lft_tmpJnt.s" "thumb3_lft_tmpJnt.is";
connectAttr "thumb3_lft_tmpJnt.s" "thumb4_lft_tmpJnt.is";
connectAttr "spine4_tmpJnt.s" "clav_rgt_tmpJnt.is";
connectAttr "clav_rgt_tmpJnt.s" "upArm_rgt_tmpJnt.is";
connectAttr "upArm_rgt_tmpJnt.s" "foreArm_rgt_tmpJnt.is";
connectAttr "foreArm_rgt_tmpJnt.s" "wrist_rgt_tmpJnt.is";
connectAttr "wrist_rgt_tmpJnt.s" "hand_rgt_tmpJnt.is";
connectAttr "hand_rgt_tmpJnt.s" "index1_rgt_tmpJnt.is";
connectAttr "index1_rgt_tmpJnt.s" "index2_rgt_tmpJnt.is";
connectAttr "index2_rgt_tmpJnt.s" "index3_rgt_tmpJnt.is";
connectAttr "index3_rgt_tmpJnt.s" "index4_rgt_tmpJnt.is";
connectAttr "index4_rgt_tmpJnt.s" "index5_rgt_tmpJnt.is";
connectAttr "hand_rgt_tmpJnt.s" "middle1_rgt_tmpJnt.is";
connectAttr "middle1_rgt_tmpJnt.s" "middle2_rgt_tmpJnt.is";
connectAttr "middle2_rgt_tmpJnt.s" "middle3_rgt_tmpJnt.is";
connectAttr "middle3_rgt_tmpJnt.s" "middle4_rgt_tmpJnt.is";
connectAttr "middle4_rgt_tmpJnt.s" "middle5_rgt_tmpJnt.is";
connectAttr "hand_rgt_tmpJnt.s" "ring1_rgt_tmpJnt.is";
connectAttr "ring1_rgt_tmpJnt.s" "ring2_rgt_tmpJnt.is";
connectAttr "ring2_rgt_tmpJnt.s" "ring3_rgt_tmpJnt.is";
connectAttr "ring3_rgt_tmpJnt.s" "ring4_rgt_tmpJnt.is";
connectAttr "ring4_rgt_tmpJnt.s" "ring5_rgt_tmpJnt.is";
connectAttr "hand_rgt_tmpJnt.s" "pinky1_rgt_tmpJnt.is";
connectAttr "pinky1_rgt_tmpJnt.s" "pinky2_rgt_tmpJnt.is";
connectAttr "pinky2_rgt_tmpJnt.s" "pinky3_rgt_tmpJnt.is";
connectAttr "pinky3_rgt_tmpJnt.s" "pinky4_rgt_tmpJnt.is";
connectAttr "pinky4_rgt_tmpJnt.s" "pinky5_rgt_tmpJnt.is";
connectAttr "hand_rgt_tmpJnt.s" "thumb1_rgt_tmpJnt.is";
connectAttr "thumb1_rgt_tmpJnt.s" "thumb2_rgt_tmpJnt.is";
connectAttr "thumb2_rgt_tmpJnt.s" "thumb3_rgt_tmpJnt.is";
connectAttr "thumb3_rgt_tmpJnt.s" "thumb4_rgt_tmpJnt.is";
connectAttr "root_tmpJnt.s" "pelvis_tmpJnt.is";
connectAttr "pelvis_tmpJnt.s" "upLeg_lft_tmpJnt.is";
connectAttr "upLeg_lft_tmpJnt.s" "lowLeg_lft_tmpJnt.is";
connectAttr "lowLeg_lft_tmpJnt.s" "knee_lft_tmpJnt.is";
connectAttr "lowLeg_lft_tmpJnt.s" "ankle_lft_tmpJnt.is";
connectAttr "ball_lft_tmpJnt.s" "toe_lft_tmpJnt.is";
connectAttr "toe_lft_tmpJnt.s" "thumbFoot1_lft_tmpJnt.is";
connectAttr "thumbFoot1_lft_tmpJnt.s" "thumbFoot2_lft_tmpJnt.is";
connectAttr "thumbFoot2_lft_tmpJnt.s" "thumbFoot3_lft_tmpJnt.is";
connectAttr "toe_lft_tmpJnt.s" "indexFoot1_lft_tmpJnt.is";
connectAttr "indexFoot1_lft_tmpJnt.s" "indexFoot2_lft_tmpJnt.is";
connectAttr "indexFoot2_lft_tmpJnt.s" "indexFoot3_lft_tmpJnt.is";
connectAttr "toe_lft_tmpJnt.s" "middleFoot1_lft_tmpJnt.is";
connectAttr "middleFoot1_lft_tmpJnt.s" "middleFoot2_lft_tmpJnt.is";
connectAttr "middleFoot2_lft_tmpJnt.s" "middleFoot3_lft_tmpJnt.is";
connectAttr "toe_lft_tmpJnt.s" "ringFoot1_lft_tmpJnt.is";
connectAttr "ringFoot1_lft_tmpJnt.s" "ringFoot2_lft_tmpJnt.is";
connectAttr "ringFoot2_lft_tmpJnt.s" "ringFoot3_lft_tmpJnt.is";
connectAttr "toe_lft_tmpJnt.s" "pinkyFoot1_lft_tmpJnt.is";
connectAttr "pinkyFoot1_lft_tmpJnt.s" "pinkyFoot2_lft_tmpJnt.is";
connectAttr "pinkyFoot2_lft_tmpJnt.s" "pinkyFoot3_lft_tmpJnt.is";
connectAttr "ankle_lft_tmpJnt.s" "footIn_lft_tmpJnt.is";
connectAttr "ankle_lft_tmpJnt.s" "footOut_lft_tmpJnt.is";
connectAttr "ankle_lft_tmpJnt.s" "heel_lft_tmpJnt.is";
connectAttr "pelvis_tmpJnt.s" "upLeg_rgt_tmpJnt.is";
connectAttr "upLeg_rgt_tmpJnt.s" "lowLeg_rgt_tmpJnt.is";
connectAttr "lowLeg_rgt_tmpJnt.s" "knee_rgt_tmpJnt.is";
connectAttr "lowLeg_rgt_tmpJnt.s" "ankle_rgt_tmpJnt.is";
connectAttr "ball_rgt_tmpJnt.s" "toe_rgt_tmpJnt.is";
connectAttr "toe_rgt_tmpJnt.s" "thumbFoot1_rgt_tmpJnt.is";
connectAttr "thumbFoot1_rgt_tmpJnt.s" "thumbFoot2_rgt_tmpJnt.is";
connectAttr "thumbFoot2_rgt_tmpJnt.s" "thumbFoot3_rgt_tmpJnt.is";
connectAttr "toe_rgt_tmpJnt.s" "indexFoot1_rgt_tmpJnt.is";
connectAttr "indexFoot1_rgt_tmpJnt.s" "indexFoot2_rgt_tmpJnt.is";
connectAttr "indexFoot2_rgt_tmpJnt.s" "indexFoot3_rgt_tmpJnt.is";
connectAttr "toe_rgt_tmpJnt.s" "middleFoot1_rgt_tmpJnt.is";
connectAttr "middleFoot1_rgt_tmpJnt.s" "middleFoot2_rgt_tmpJnt.is";
connectAttr "middleFoot2_rgt_tmpJnt.s" "middleFoot3_rgt_tmpJnt.is";
connectAttr "toe_rgt_tmpJnt.s" "ringFoot1_rgt_tmpJnt.is";
connectAttr "ringFoot1_rgt_tmpJnt.s" "ringFoot2_rgt_tmpJnt.is";
connectAttr "ringFoot2_rgt_tmpJnt.s" "ringFoot3_rgt_tmpJnt.is";
connectAttr "toe_rgt_tmpJnt.s" "pinkyFoot1_rgt_tmpJnt.is";
connectAttr "pinkyFoot1_rgt_tmpJnt.s" "pinkyFoot2_rgt_tmpJnt.is";
connectAttr "pinkyFoot2_rgt_tmpJnt.s" "pinkyFoot3_rgt_tmpJnt.is";
connectAttr "ankle_rgt_tmpJnt.s" "footIn_rgt_tmpJnt.is";
connectAttr "ankle_rgt_tmpJnt.s" "footOut_rgt_tmpJnt.is";
connectAttr "ankle_rgt_tmpJnt.s" "heel_rgt_tmpJnt.is";
// End of tmpJntHuman.ma
