//Maya ASCII 2015 scene
//Name: facialCtrlAttr.ma
//Last modified: Thu, Sep 29, 2016 07:01:44 PM
//Codeset: 1252
requires maya "2015";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2015";
fileInfo "version" "2015";
fileInfo "cutIdentifier" "201503261530-955654";
fileInfo "osv" "Microsoft Windows 8 Business Edition, 64-bit  (Build 9200)\n";
createNode transform -n "facailRig_grp";
	setAttr ".t" -type "double3" -4.2114081090789787e-018 0 0 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 0.99999999999999989 ;
createNode transform -n "eyeBrowBshCtrl_lft_grp" -p "facailRig_grp";
	setAttr ".t" -type "double3" 0.35 0.65 0 ;
createNode transform -n "eyeBrowBsh_lft_ctrl" -p "eyeBrowBshCtrl_lft_grp";
	addAttr -ci true -k true -sn "eyebrow" -ln "eyebrow" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "allUD" -ln "allUD" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "allIO" -ln "allIO" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "innerUD" -ln "innerUD" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "innerIO" -ln "innerIO" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "middleUD" -ln "middleUD" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "middleIO" -ln "middleIO" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "outerUD" -ln "outerUD" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "outerIO" -ln "outerIO" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "turnUD" -ln "turnUD" -min -10 -max 10 -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -l on -k on ".eyebrow";
createNode nurbsCurve -n "eyeBrowBsh_lft_ctrlShape" -p "eyeBrowBsh_lft_ctrl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		3 14 0 no 3
		19 0 0 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 14 14
		17
		-7.7297251561664839e-018 -0.00035705672614272377 -5.5837517380084909e-011
		-0.094615581781073085 -0.011654808191212618 -5.5837517380084909e-011
		-0.17482657351495715 -0.061247360135161411 -5.5837517380084909e-011
		-0.21159322289087509 -0.083979540991834356 -5.5837517380084909e-011
		-0.22902683698637949 -0.052590134326391702 -5.5837517380084909e-011
		-0.21159322289087509 0.00035676958764163658 -5.5837517380084909e-011
		-0.17482657351495715 0.023088950444314582 -5.5837517380084909e-011
		-0.094615581781073085 0.072681502388263319 -5.5837517380084909e-011
		-7.7297251561664839e-018 0.083979540991834356 -5.5837517380084909e-011
		0.094615581781073058 0.072681502388263319 -5.5837517380084909e-011
		0.1748265735149572 0.023088950444314582 -5.5837517380084909e-011
		0.21159322289087512 0.00035676958764163658 -5.5837517380084909e-011
		0.22902683698637949 -0.052590134326391702 -5.5837517380084909e-011
		0.21159322289087512 -0.083979540991834356 -5.5837517380084909e-011
		0.1748265735149572 -0.061247360135161411 -5.5837517380084909e-011
		0.094615581781073058 -0.011654808191212618 -5.5837517380084909e-011
		-7.7297251561664839e-018 -0.00035705672614272377 -5.5837517380084909e-011
		;
createNode transform -n "eyeBrowBshCtrl_rgt_grp" -p "facailRig_grp";
	setAttr ".t" -type "double3" -0.35 0.65 0 ;
createNode transform -n "eyeBrowBsh_rgt_ctrl" -p "eyeBrowBshCtrl_rgt_grp";
	addAttr -ci true -k true -sn "eyebrow" -ln "eyebrow" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "allUD" -ln "allUD" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "allIO" -ln "allIO" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "innerUD" -ln "innerUD" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "innerIO" -ln "innerIO" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "middleUD" -ln "middleUD" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "middleIO" -ln "middleIO" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "outerUD" -ln "outerUD" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "outerIO" -ln "outerIO" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "turnUD" -ln "turnUD" -min -10 -max 10 -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -l on -k on ".eyebrow";
createNode nurbsCurve -n "eyeBrowBsh_rgt_ctrlShape" -p "eyeBrowBsh_rgt_ctrl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		3 14 0 no 3
		19 0 0 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 14 14
		17
		-7.3217730460128513e-018 -0.00035705672614272377 5.5837517380084909e-011
		-0.094615581781073016 -0.011654808191212618 5.5837517380084909e-011
		-0.1748265735149572 -0.061247360135161411 5.5837517380084909e-011
		-0.21159322289087509 -0.083979540991834356 5.5837517380084909e-011
		-0.22902683698637949 -0.052590134326391702 5.5837517380084909e-011
		-0.21159322289087509 0.00035676958764163658 5.5837517380084909e-011
		-0.1748265735149572 0.023088950444314582 5.5837517380084909e-011
		-0.094615581781073016 0.072681502388263319 5.5837517380084909e-011
		-7.3217730460128513e-018 0.083979540991834356 5.5837517380084909e-011
		0.094615581781073085 0.072681502388263319 5.5837517380084909e-011
		0.17482657351495717 0.023088950444314582 5.5837517380084909e-011
		0.21159322289087512 0.00035676958764163658 5.5837517380084909e-011
		0.22902683698637949 -0.052590134326391702 5.5837517380084909e-011
		0.21159322289087512 -0.083979540991834356 5.5837517380084909e-011
		0.17482657351495717 -0.061247360135161411 5.5837517380084909e-011
		0.094615581781073085 -0.011654808191212618 5.5837517380084909e-011
		-7.3217730460128513e-018 -0.00035705672614272377 5.5837517380084909e-011
		;
createNode transform -n "eyeBshCtrl_lft_grp" -p "facailRig_grp";
	setAttr ".t" -type "double3" 0.35 0.4 0 ;
createNode transform -n "eyeBsh_lft_ctrl" -p "eyeBshCtrl_lft_grp";
	addAttr -ci true -k true -sn "lid" -ln "lid" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "upLidUD" -ln "upLidUD" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "loLidUD" -ln "loLidUD" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "upLidTW" -ln "upLidTW" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "loLidTW" -ln "loLidTW" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "upInnerUD" -ln "upInnerUD" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "upMidUD" -ln "upMidUD" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "upOuterUD" -ln "upOuterUD" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "loInnerUD" -ln "loInnerUD" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "loMidUD" -ln "loMidUD" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "loOuterUD" -ln "loOuterUD" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "ball" -ln "ball" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "eyeBallUD" -ln "eyeBallUD" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "eyeBallIO" -ln "eyeBallIO" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "eyeBallTW" -ln "eyeBallTW" -min -10 -max 10 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 4.2114081090789787e-018 0 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -l on -k on ".lid";
	setAttr -l on -k on ".ball";
createNode nurbsCurve -n "eyeBsh_lft_ctrlShape" -p "eyeBsh_lft_ctrl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 47 0 no 3
		48 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27
		 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47
		48
		0.16681500330994362 0 0
		0.14889106483429673 0 0
		0.14705787915792268 0.023291674582153452 5.1444960424339207e-018
		0.14160384408970392 0.046009802112926608 -2.0577984169735675e-018
		0.13266285647229667 0.067595070421221509 8.2311936678942701e-018
		0.12045540867007647 0.08751589569649057 8.2311936678942701e-018
		0.10528191596900395 0.10528191596900395 2.4693581003682792e-017
		0.087515895696490598 0.12045540867007647 2.0577984169735683e-017
		0.067595070421221509 0.13266285647229667 2.0577984169735683e-017
		0.046009802112926601 0.14160384408970392 1.646238733578854e-017
		0.023291674582153442 0.14705787915792268 2.0577984169735683e-017
		0 0.14889106483429673 3.292477467157708e-017
		0 0.1668150033099435 4.9387162007365584e-017
		0 0.14889106483429673 3.292477467157708e-017
		-0.023291674582153442 0.14705787915792268 2.0577984169735683e-017
		-0.046009802112926601 0.14160384408970392 1.646238733578854e-017
		-0.067595070421221509 0.13266285647229667 2.0577984169735683e-017
		-0.087515895696490598 0.12045540867007647 2.0577984169735683e-017
		-0.10528191596900395 0.10528191596900395 2.4693581003682792e-017
		-0.12045540867007647 0.08751589569649057 8.2311936678942701e-018
		-0.13266285647229667 0.067595070421221509 8.2311936678942701e-018
		-0.14160384408970392 0.046009802112926608 -2.0577984169735675e-018
		-0.14705787915792268 0.023291674582153452 5.1444960424339207e-018
		-0.14889106483429673 0 0
		-0.16681500330994362 0 0
		-0.14889106483429673 0 0
		-0.14705787915792268 -0.023291674582153452 -5.1444960424339207e-018
		-0.14160384408970392 -0.046009802112926608 2.0577984169735675e-018
		-0.13266285647229667 -0.067595070421221509 -8.2311936678942701e-018
		-0.12045540867007647 -0.08751589569649057 -8.2311936678942701e-018
		-0.10528191596900395 -0.10528191596900395 -2.4693581003682792e-017
		-0.087515895696490598 -0.12045540867007647 -2.0577984169735683e-017
		-0.067595070421221509 -0.13266285647229667 -2.0577984169735683e-017
		-0.046009802112926601 -0.14160384408970392 -1.646238733578854e-017
		-0.023291674582153442 -0.14705787915792268 -2.0577984169735683e-017
		0 -0.14889106483429673 -3.292477467157708e-017
		0 -0.1668150033099435 -4.9387162007365584e-017
		0 -0.14889106483429673 -3.292477467157708e-017
		0.023291674582153442 -0.14705787915792268 -2.0577984169735683e-017
		0.046009802112926601 -0.14160384408970392 -1.646238733578854e-017
		0.067595070421221509 -0.13266285647229667 -2.0577984169735683e-017
		0.087515895696490598 -0.12045540867007647 -2.0577984169735683e-017
		0.10528191596900395 -0.10528191596900395 -2.4693581003682792e-017
		0.12045540867007647 -0.08751589569649057 -8.2311936678942701e-018
		0.13266285647229667 -0.067595070421221509 -8.2311936678942701e-018
		0.14160384408970392 -0.046009802112926608 2.0577984169735675e-018
		0.14705787915792268 -0.023291674582153452 -5.1444960424339207e-018
		0.14889106483429673 0 0
		;
createNode transform -n "eyeBshCtrl_rgt_grp" -p "facailRig_grp";
	setAttr ".t" -type "double3" -0.35 0.4 0 ;
createNode transform -n "eyeBsh_rgt_ctrl" -p "eyeBshCtrl_rgt_grp";
	addAttr -ci true -k true -sn "lid" -ln "lid" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "upLidUD" -ln "upLidUD" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "loLidUD" -ln "loLidUD" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "upLidTW" -ln "upLidTW" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "loLidTW" -ln "loLidTW" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "upInnerUD" -ln "upInnerUD" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "upMidUD" -ln "upMidUD" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "upOuterUD" -ln "upOuterUD" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "loInnerUD" -ln "loInnerUD" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "loMidUD" -ln "loMidUD" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "loOuterUD" -ln "loOuterUD" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "ball" -ln "ball" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "eyeBallUD" -ln "eyeBallUD" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "eyeBallIO" -ln "eyeBallIO" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "eyeBallTW" -ln "eyeBallTW" -min -10 -max 10 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 4.2114081090789787e-018 0 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -l on -k on ".lid";
	setAttr -l on -k on ".ball";
createNode nurbsCurve -n "eyeBsh_rgt_ctrlShape" -p "eyeBsh_rgt_ctrl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 47 0 no 3
		48 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27
		 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47
		48
		0.16681500330994362 0 0
		0.14889106483429673 0 0
		0.14705787915792268 0.023291674582153452 5.1444960424339207e-018
		0.14160384408970392 0.046009802112926608 -2.0577984169735675e-018
		0.13266285647229667 0.067595070421221509 8.2311936678942701e-018
		0.12045540867007647 0.08751589569649057 8.2311936678942701e-018
		0.10528191596900395 0.10528191596900395 2.4693581003682792e-017
		0.087515895696490598 0.12045540867007647 2.0577984169735683e-017
		0.067595070421221509 0.13266285647229667 2.0577984169735683e-017
		0.046009802112926601 0.14160384408970392 1.646238733578854e-017
		0.023291674582153442 0.14705787915792268 2.0577984169735683e-017
		0 0.14889106483429673 3.292477467157708e-017
		0 0.1668150033099435 4.9387162007365584e-017
		0 0.14889106483429673 3.292477467157708e-017
		-0.023291674582153442 0.14705787915792268 2.0577984169735683e-017
		-0.046009802112926601 0.14160384408970392 1.646238733578854e-017
		-0.067595070421221509 0.13266285647229667 2.0577984169735683e-017
		-0.087515895696490598 0.12045540867007647 2.0577984169735683e-017
		-0.10528191596900395 0.10528191596900395 2.4693581003682792e-017
		-0.12045540867007647 0.08751589569649057 8.2311936678942701e-018
		-0.13266285647229667 0.067595070421221509 8.2311936678942701e-018
		-0.14160384408970392 0.046009802112926608 -2.0577984169735675e-018
		-0.14705787915792268 0.023291674582153452 5.1444960424339207e-018
		-0.14889106483429673 0 0
		-0.16681500330994362 0 0
		-0.14889106483429673 0 0
		-0.14705787915792268 -0.023291674582153452 -5.1444960424339207e-018
		-0.14160384408970392 -0.046009802112926608 2.0577984169735675e-018
		-0.13266285647229667 -0.067595070421221509 -8.2311936678942701e-018
		-0.12045540867007647 -0.08751589569649057 -8.2311936678942701e-018
		-0.10528191596900395 -0.10528191596900395 -2.4693581003682792e-017
		-0.087515895696490598 -0.12045540867007647 -2.0577984169735683e-017
		-0.067595070421221509 -0.13266285647229667 -2.0577984169735683e-017
		-0.046009802112926601 -0.14160384408970392 -1.646238733578854e-017
		-0.023291674582153442 -0.14705787915792268 -2.0577984169735683e-017
		0 -0.14889106483429673 -3.292477467157708e-017
		0 -0.1668150033099435 -4.9387162007365584e-017
		0 -0.14889106483429673 -3.292477467157708e-017
		0.023291674582153442 -0.14705787915792268 -2.0577984169735683e-017
		0.046009802112926601 -0.14160384408970392 -1.646238733578854e-017
		0.067595070421221509 -0.13266285647229667 -2.0577984169735683e-017
		0.087515895696490598 -0.12045540867007647 -2.0577984169735683e-017
		0.10528191596900395 -0.10528191596900395 -2.4693581003682792e-017
		0.12045540867007647 -0.08751589569649057 -8.2311936678942701e-018
		0.13266285647229667 -0.067595070421221509 -8.2311936678942701e-018
		0.14160384408970392 -0.046009802112926608 2.0577984169735675e-018
		0.14705787915792268 -0.023291674582153452 -5.1444960424339207e-018
		0.14889106483429673 0 0
		;
createNode transform -n "noseBshCtrl_grp" -p "facailRig_grp";
createNode transform -n "noseBsh_ctrl" -p "noseBshCtrl_grp";
	addAttr -ci true -k true -sn "nose" -ln "nose" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "noseUD" -ln "noseUD" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "noseLR" -ln "noseLR" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "noseSTQ" -ln "noseSTQ" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "noseLft" -ln "noseLft" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "noseLftUD" -ln "noseLftUD" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "noseLftTurn" -ln "noseLftTurn" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "noseRgt" -ln "noseRgt" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "noseRgtUD" -ln "noseRgtUD" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "noseRgtTurn" -ln "noseRgtTurn" -min -10 -max 10 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 4.2114081090789787e-018 0 0 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -l on -k on ".nose";
	setAttr -l on -k on ".noseLft";
	setAttr -l on -k on ".noseRgt";
createNode nurbsCurve -n "noseBsh_ctrlShape" -p "noseBsh_ctrl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 47 0 no 3
		48 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27
		 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47
		48
		0.16681500330994362 0 0
		0.14889106483429673 0 0
		0.14705787915792268 0.023291674582153452 5.1444960424339207e-018
		0.14160384408970392 0.046009802112926608 -2.0577984169735675e-018
		0.13266285647229667 0.067595070421221509 8.2311936678942701e-018
		0.12045540867007647 0.08751589569649057 8.2311936678942701e-018
		0.10528191596900395 0.10528191596900395 2.4693581003682792e-017
		0.087515895696490598 0.12045540867007647 2.0577984169735683e-017
		0.067595070421221509 0.13266285647229667 2.0577984169735683e-017
		0.046009802112926601 0.14160384408970392 1.646238733578854e-017
		0.023291674582153442 0.14705787915792268 2.0577984169735683e-017
		0 0.14889106483429673 3.292477467157708e-017
		0 0.1668150033099435 4.9387162007365584e-017
		0 0.14889106483429673 3.292477467157708e-017
		-0.023291674582153442 0.14705787915792268 2.0577984169735683e-017
		-0.046009802112926601 0.14160384408970392 1.646238733578854e-017
		-0.067595070421221509 0.13266285647229667 2.0577984169735683e-017
		-0.087515895696490598 0.12045540867007647 2.0577984169735683e-017
		-0.10528191596900395 0.10528191596900395 2.4693581003682792e-017
		-0.12045540867007647 0.08751589569649057 8.2311936678942701e-018
		-0.13266285647229667 0.067595070421221509 8.2311936678942701e-018
		-0.14160384408970392 0.046009802112926608 -2.0577984169735675e-018
		-0.14705787915792268 0.023291674582153452 5.1444960424339207e-018
		-0.14889106483429673 0 0
		-0.16681500330994362 0 0
		-0.14889106483429673 0 0
		-0.14705787915792268 -0.023291674582153452 -5.1444960424339207e-018
		-0.14160384408970392 -0.046009802112926608 2.0577984169735675e-018
		-0.13266285647229667 -0.067595070421221509 -8.2311936678942701e-018
		-0.12045540867007647 -0.08751589569649057 -8.2311936678942701e-018
		-0.10528191596900395 -0.10528191596900395 -2.4693581003682792e-017
		-0.087515895696490598 -0.12045540867007647 -2.0577984169735683e-017
		-0.067595070421221509 -0.13266285647229667 -2.0577984169735683e-017
		-0.046009802112926601 -0.14160384408970392 -1.646238733578854e-017
		-0.023291674582153442 -0.14705787915792268 -2.0577984169735683e-017
		0 -0.14889106483429673 -3.292477467157708e-017
		0 -0.1668150033099435 -4.9387162007365584e-017
		0 -0.14889106483429673 -3.292477467157708e-017
		0.023291674582153442 -0.14705787915792268 -2.0577984169735683e-017
		0.046009802112926601 -0.14160384408970392 -1.646238733578854e-017
		0.067595070421221509 -0.13266285647229667 -2.0577984169735683e-017
		0.087515895696490598 -0.12045540867007647 -2.0577984169735683e-017
		0.10528191596900395 -0.10528191596900395 -2.4693581003682792e-017
		0.12045540867007647 -0.08751589569649057 -8.2311936678942701e-018
		0.13266285647229667 -0.067595070421221509 -8.2311936678942701e-018
		0.14160384408970392 -0.046009802112926608 2.0577984169735675e-018
		0.14705787915792268 -0.023291674582153452 -5.1444960424339207e-018
		0.14889106483429673 0 0
		;
createNode transform -n "mouthBshCtrl_grp" -p "facailRig_grp";
	setAttr ".t" -type "double3" 0 -0.35 0 ;
createNode transform -n "mouthBsh_ctrl" -p "mouthBshCtrl_grp";
	addAttr -ci true -k true -sn "mouth" -ln "mouth" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "mouthUD" -ln "mouthUD" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "mouthLR" -ln "mouthLR" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "mouthTurn" -ln "mouthTurn" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "upLipsUD" -ln "upLipsUD" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "loLipsUD" -ln "loLipsUD" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "upLipsCurlIO" -ln "upLipsCurlIO" -min -10 -max 10 
		-at "double";
	addAttr -ci true -k true -sn "loLipsCurlIO" -ln "loLipsCurlIO" -min -10 -max 10 
		-at "double";
	addAttr -ci true -k true -sn "mouthClench" -ln "mouthClench" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "mouthPull" -ln "mouthPull" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "mouthU" -ln "mouthU" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "eyebrowPullIO" -ln "eyebrowPullIO" -min -10 -max 10 
		-at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -l on -k on ".mouth";
createNode nurbsCurve -n "mouthBsh_ctrlShape" -p "mouthBsh_ctrl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 12 0 no 3
		17 0 0 0 1 2 3 4 5 6 7 8 9 10 11 12 12 12
		15
		-0.1690017162424455 0.075263978505135554 6.0922461141700976e-012
		0.1690017162424455 0.075263978505135554 6.0922461141700976e-012
		0.31227454100382418 0.075263978505135554 6.0922461141700976e-012
		0.37794729385860359 0.075263978505135554 6.0922461141700976e-012
		0.40908719564522328 -1.9428902930940239e-016 -3.1019272970738538e-025
		0.37794729385860359 -0.075263978505135526 -6.0922461141700976e-012
		0.31227454100382418 -0.075263978505135526 -6.0922461141700976e-012
		0.1690017162424455 -0.075263978505135526 -6.0922461141700976e-012
		-0.1690017162424455 -0.075263978505135526 -6.0922461141700976e-012
		-0.31227454100382418 -0.075263978505135526 -6.0922461141700976e-012
		-0.37794729385860359 -0.075263978505135526 -6.0922461141700976e-012
		-0.40908719564522328 -1.9428902930940239e-016 -3.1019272970738538e-025
		-0.37794729385860359 0.075263978505135554 6.0922461141700976e-012
		-0.31227454100382418 0.075263978505135554 6.0922461141700976e-012
		-0.1690017162424455 0.075263978505135554 6.0922461141700976e-012
		;
createNode transform -n "mouthBshCtrl_lft_grp" -p "facailRig_grp";
	setAttr ".t" -type "double3" 0.5 -0.35 0 ;
createNode transform -n "mouthBsh_lft_ctrl" -p "mouthBshCtrl_lft_grp";
	addAttr -ci true -k true -sn "lip" -ln "lip" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "upLipUD" -ln "upLipUD" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "loLipUD" -ln "loLipUD" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "cornerUD" -ln "cornerUD" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "cornerIO" -ln "cornerIO" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "lipPartIO" -ln "lipPartIO" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "cheekUprIO" -ln "cheekUprIO" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "cheekLwrIO" -ln "cheekLwrIO" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "puffIO" -ln "puffIO" -min -10 -max 10 -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -l on -k on ".lip";
createNode nurbsCurve -n "mouthBsh_lft_ctrlShape" -p "mouthBsh_lft_ctrl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		1 4 0 no 3
		5 0 1 2 3 4
		5
		0.075068581929480072 0.075012009945932157 6.0718505078496372e-012
		-0.075068581929480072 0.075012009945932157 6.0718505078496372e-012
		-0.075068581929480072 -0.075012009945932184 -6.0718505078495338e-012
		0.075068581929480072 -0.075012009945932184 -6.0718505078495338e-012
		0.075068581929480072 0.075012009945932157 6.0718505078496372e-012
		;
createNode transform -n "mouthBshCtrl_rgt_grp" -p "facailRig_grp";
	setAttr ".t" -type "double3" -0.5 -0.35 0 ;
createNode transform -n "mouthBsh_rgt_ctrl" -p "mouthBshCtrl_rgt_grp";
	addAttr -ci true -k true -sn "lip" -ln "lip" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "upLipUD" -ln "upLipUD" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "loLipUD" -ln "loLipUD" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "cornerUD" -ln "cornerUD" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "cornerIO" -ln "cornerIO" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "lipPartIO" -ln "lipPartIO" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "cheekUprIO" -ln "cheekUprIO" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "cheekLwrIO" -ln "cheekLwrIO" -min -10 -max 10 -at "double";
	addAttr -ci true -k true -sn "puffIO" -ln "puffIO" -min -10 -max 10 -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -l on -k on ".lip";
createNode nurbsCurve -n "mouthBsh_rgt_ctrlShape" -p "mouthBsh_rgt_ctrl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		1 4 0 no 3
		5 0 1 2 3 4
		5
		0.075068581929480072 0.075012009945932157 6.0718505078496372e-012
		-0.075068581929480072 0.075012009945932157 6.0718505078496372e-012
		-0.075068581929480072 -0.075012009945932184 -6.0718505078495338e-012
		0.075068581929480072 -0.075012009945932184 -6.0718505078495338e-012
		0.075068581929480072 0.075012009945932157 6.0718505078496372e-012
		;
createNode transform -n "faceUprBshCtrl_grp" -p "facailRig_grp";
	setAttr ".t" -type "double3" 4.2114081090789787e-018 1 -0.5 ;
createNode transform -n "faceUprBsh_ctrl" -p "faceUprBshCtrl_grp";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "faceUprBsh_ctrlShape" -p "faceUprBsh_ctrl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		0.061277412381898579 -0.061277412381898579 0.061277412381898579
		0.061277412381898579 0.061277412381898579 0.061277412381898579
		0.061277412381898579 0.061277412381898579 -0.061277412381898579
		0.061277412381898579 -0.061277412381898579 -0.061277412381898579
		-0.061277412381898579 -0.061277412381898579 -0.061277412381898579
		-0.061277412381898579 0.061277412381898579 -0.061277412381898579
		-0.061277412381898579 0.061277412381898579 0.061277412381898579
		-0.061277412381898579 -0.061277412381898579 0.061277412381898579
		0.061277412381898579 -0.061277412381898579 0.061277412381898579
		0.061277412381898579 -0.061277412381898579 -0.061277412381898579
		-0.061277412381898579 -0.061277412381898579 -0.061277412381898579
		-0.061277412381898579 -0.061277412381898579 0.061277412381898579
		-0.061277412381898579 0.061277412381898579 0.061277412381898579
		0.061277412381898579 0.061277412381898579 0.061277412381898579
		0.061277412381898579 0.061277412381898579 -0.061277412381898579
		-0.061277412381898579 0.061277412381898579 -0.061277412381898579
		;
createNode transform -n "faceLwrBshCtrl_grp" -p "facailRig_grp";
	setAttr ".t" -type "double3" 4.2114081090789787e-018 -0.5 -0.5 ;
createNode transform -n "faceLwrBsh_ctrl" -p "faceLwrBshCtrl_grp";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "faceLwrBsh_ctrlShape" -p "faceLwrBsh_ctrl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 15 0 no 3
		16 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		16
		0.061277412381898579 -0.061277412381898579 0.061277412381898579
		0.061277412381898579 0.061277412381898579 0.061277412381898579
		0.061277412381898579 0.061277412381898579 -0.061277412381898579
		0.061277412381898579 -0.061277412381898579 -0.061277412381898579
		-0.061277412381898579 -0.061277412381898579 -0.061277412381898579
		-0.061277412381898579 0.061277412381898579 -0.061277412381898579
		-0.061277412381898579 0.061277412381898579 0.061277412381898579
		-0.061277412381898579 -0.061277412381898579 0.061277412381898579
		0.061277412381898579 -0.061277412381898579 0.061277412381898579
		0.061277412381898579 -0.061277412381898579 -0.061277412381898579
		-0.061277412381898579 -0.061277412381898579 -0.061277412381898579
		-0.061277412381898579 -0.061277412381898579 0.061277412381898579
		-0.061277412381898579 0.061277412381898579 0.061277412381898579
		0.061277412381898579 0.061277412381898579 0.061277412381898579
		0.061277412381898579 0.061277412381898579 -0.061277412381898579
		-0.061277412381898579 0.061277412381898579 -0.061277412381898579
		;
select -ne :time1;
	setAttr -av -k on ".cch";
	setAttr -av -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 1;
	setAttr -av ".unw" 1;
	setAttr -k on ".etw";
	setAttr -k on ".tps";
	setAttr -k on ".tms";
select -ne :renderPartition;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
select -ne :renderGlobalsList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
select -ne :defaultShaderList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".s";
select -ne :postProcessList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
	setAttr -k on ".ihi";
select -ne :initialShadingGroup;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -k on ".mico";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -k on ".micr";
	setAttr -k on ".micg";
	setAttr -k on ".micb";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
select -ne :initialParticleSE;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -k on ".mico";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -k on ".micr";
	setAttr -k on ".micg";
	setAttr -k on ".micb";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
select -ne :defaultResolution;
	setAttr -av -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -av -k on ".w";
	setAttr -av -k on ".h";
	setAttr -av -k on ".pa" 1;
	setAttr -av -k on ".al";
	setAttr -av -k on ".dar";
	setAttr -av -k on ".ldar";
	setAttr -cb on ".dpi";
	setAttr -av -k on ".off";
	setAttr -av -k on ".fld";
	setAttr -av -k on ".zsl";
	setAttr -cb on ".isu";
	setAttr -cb on ".pdu";
select -ne :hardwareRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k off ".ctrs" 256;
	setAttr -av -k off ".btrs" 512;
	setAttr -k off ".fbfm";
	setAttr -k off -cb on ".ehql";
	setAttr -k off -cb on ".eams";
	setAttr -k off -cb on ".eeaa";
	setAttr -k off -cb on ".engm";
	setAttr -k off -cb on ".mes";
	setAttr -k off -cb on ".emb";
	setAttr -av -k off -cb on ".mbbf";
	setAttr -k off -cb on ".mbs";
	setAttr -k off -cb on ".trm";
	setAttr -k off -cb on ".tshc";
	setAttr -k off ".enpt";
	setAttr -k off -cb on ".clmt";
	setAttr -k off -cb on ".tcov";
	setAttr -k off -cb on ".lith";
	setAttr -k off -cb on ".sobc";
	setAttr -k off -cb on ".cuth";
	setAttr -k off -cb on ".hgcd";
	setAttr -k off -cb on ".hgci";
	setAttr -k off -cb on ".mgcs";
	setAttr -k off -cb on ".twa";
	setAttr -k off -cb on ".twz";
	setAttr -k on ".hwcc";
	setAttr -k on ".hwdp";
	setAttr -k on ".hwql";
	setAttr -k on ".hwfr";
	setAttr -k on ".soll";
	setAttr -k on ".sosl";
	setAttr -k on ".bswa";
	setAttr -k on ".shml";
	setAttr -k on ".hwel";
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
select -ne :defaultHardwareRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -av -k on ".rp";
	setAttr -k on ".cai";
	setAttr -k on ".coi";
	setAttr -cb on ".bc";
	setAttr -av -k on ".bcb";
	setAttr -av -k on ".bcg";
	setAttr -av -k on ".bcr";
	setAttr -k on ".ei";
	setAttr -av -k on ".ex";
	setAttr -av -k on ".es";
	setAttr -av -k on ".ef";
	setAttr -av -k on ".bf";
	setAttr -k on ".fii";
	setAttr -av -k on ".sf";
	setAttr -k on ".gr";
	setAttr -k on ".li";
	setAttr -k on ".ls";
	setAttr -av -k on ".mb";
	setAttr -k on ".ti";
	setAttr -k on ".txt";
	setAttr -k on ".mpr";
	setAttr -k on ".wzd";
	setAttr -k on ".fn";
	setAttr -k on ".if";
	setAttr ".res" -type "string" "ntsc_4d 646 485 1.333";
	setAttr -k on ".as";
	setAttr -k on ".ds";
	setAttr -k on ".lm";
	setAttr -av -k on ".fir";
	setAttr -k on ".aap";
	setAttr -av -k on ".gh";
	setAttr -cb on ".sd";
// End of facialCtrlAttr.ma
