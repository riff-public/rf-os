import maya.cmds as mc
import maya.mel as mm
import ppmel.core as core
import ppmel.rigRibbon as rbn
reload( core )
reload( rbn )


def rigLegAnimal( upLegTmpJnt = 'upLegBack_lft_tmpJnt' ,
                  midLegTmpJnt = 'midLegBack_lft_tmpJnt' ,
                  lowLegTmpJnt = 'lowLegBack_lft_tmpJnt' ,
                  ankleTmpJnt = 'ankleBack_lft_tmpJnt' ,
                  ballTmpJnt = 'ballBack_lft_tmpJnt' ,
                  toeTmpJnt = 'toeBack_lft_tmpJnt' ,
                  kneeTmpJnt = 'kneeBack_lft_tmpJnt' ,
                  heelTmpJnt = 'heelBack_lft_tmpJnt' ,
                  footInTmpJnt = 'footInBack_lft_tmpJnt' ,
                  footOutTmpJnt = 'footOutBack_lft_tmpJnt' ,
                  parent = 'pelvis_jnt' ,
                  animGrp = 'anim_grp' ,
                  skinGrp = 'skin_grp' ,
                  jntGrp = 'jnt_grp' ,
                  stillGrp = 'still_grp' ,
                  ikhGrp = 'ikh_grp' ,
                  side = 'lft' ,
                  val = 0.1 , # Left = 0.1 , Right = -0.1
                  elem = 'back' , # Front , Back , Mid
                  foot = True ,
                  legFk = True ,
                  legIk = True ,
                  ribbon = True ,
                  charSize = 1 ) :
    
    #-- Info
    elem = elem.capitalize()
    
    ## Main Rig ##
    #-- Group
    legRigGrp = core.transform( 'leg%sRig_%s_grp' %( elem , side ))
    legJntGrp = core.transform( 'leg%sJnt_%s_grp' %( elem , side ))
    core.parCons( 0 , parent , legRigGrp )
    core.snap( parent , legJntGrp )
    
    #-- Joint
    upLegJnt = core.addJnt( 'upLeg%s_%s_jnt' %( elem , side ) , upLegTmpJnt )
    midLegJnt = core.addJnt( 'midLeg%s_%s_jnt' %( elem , side ) , midLegTmpJnt )
    lowLegJnt = core.addJnt( 'lowLeg%s_%s_jnt' %( elem , side ) , lowLegTmpJnt )
    ankleJnt = core.addJnt( 'ankle%s_%s_jnt' %( elem , side ) , ankleTmpJnt )
    core.parent( ankleJnt , lowLegJnt , midLegJnt , upLegJnt , parent )
    
    if foot == True :
        ballJnt = core.addJnt( 'ball%s_%s_jnt' %( elem , side ) , ballTmpJnt )
        toeJnt = core.addJnt( 'toe%s_%s_jnt' %( elem , side ) , toeTmpJnt )
        core.parent( toeJnt , ballJnt , ankleJnt )
        
    #-- Rotate order
    for each in ( upLegJnt , midLegJnt , lowLegJnt , ankleJnt ) :
        core.setRotateOrder( each , 'yzx' )
        
    if foot == True :
        for each in ( ballJnt , toeJnt ) :
            core.setRotateOrder( each , 'yzx' )
        
    #-- Hierarchy
    core.parent( legRigGrp , animGrp )
    core.parent( legJntGrp , jntGrp )
    
    #-- Controls
    legCtrl = core.addCtrl( 'leg%s_%s_ctrl' %( elem , side ) , 'stick' , 'green' , jnt = False )
    legZro = core.addGrp( legCtrl )
    core.parent( legZro , legRigGrp )
    
    #-- Shape
    if side == 'lft' :
        core.rotateShape( legCtrl , -90 , 0 , 0 )
    elif side == 'rgt' :
        core.rotateShape( legCtrl , 90 , 0 , 0 )
        
    core.scaleShape( legCtrl , charSize )
    
    #-- Rig process
    core.parCons( 0 , ankleJnt , legZro )
            
    #-- Cleanup
    core.setLock( legZro , True , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
    core.setLockHide( legCtrl , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
            
            
    ## Fk Rig ##
    if legFk == True :
        #-- Group
        legFkGrp = core.transform( 'leg%sFk_%s_grp' %( elem , side ))
        legFkJntGrp = core.transform( 'leg%sFkJnt_%s_grp' %( elem , side ))
        core.snap( legRigGrp , legFkGrp )
        core.snap( legRigGrp , legFkJntGrp )
        
        #-- Joint
        upLegFkJnt = core.addJnt( 'upLeg%sFk_%s_jnt' %( elem , side ) , upLegTmpJnt )
        midLegFkJnt = core.addJnt( 'midLeg%sFk_%s_jnt' %( elem , side ) , midLegTmpJnt )
        lowLegFkJnt = core.addJnt( 'lowLeg%sFk_%s_jnt' %( elem , side ) , lowLegTmpJnt )
        ankleFkJnt = core.addJnt( 'ankle%sFk_%s_jnt' %( elem , side ) , ankleTmpJnt )
        core.parent( ankleFkJnt , lowLegFkJnt , midLegFkJnt , upLegFkJnt , legFkJntGrp )
        
        if foot == True :
            ballFkJnt = core.addJnt( 'ball%sFk_%s_jnt' %( elem , side ) , ballTmpJnt )
            toeFkJnt = core.addJnt( 'toe%sFk_%s_jnt' %( elem , side ) , toeTmpJnt )
            core.parent( toeFkJnt , ballFkJnt , ankleFkJnt )
            
        #-- Controls
        upLegFkCtrl = core.addCtrl( 'upLeg%sFk_%s_ctrl' %( elem , side ) , 'circle' , 'red' , jnt = True )
        upLegFkGmbl = core.addGimbal( upLegFkCtrl )
        upLegFkZro = core.addGrp( upLegFkCtrl )
        upLegFkOfst = core.addGrp( upLegFkCtrl , 'Ofst' )
        core.snapPoint( upLegFkJnt , upLegFkZro )
        core.snapJntOrient( upLegFkJnt , upLegFkCtrl )
        
        midLegFkCtrl = core.addCtrl( 'midLeg%sFk_%s_ctrl' %( elem , side ) , 'circle' , 'red' , jnt = True )
        midLegFkGmbl = core.addGimbal( midLegFkCtrl )
        midLegFkZro = core.addGrp( midLegFkCtrl )
        midLegFkOfst = core.addGrp( midLegFkCtrl , 'Ofst' )
        core.snapPoint( midLegFkJnt , midLegFkZro )
        core.snapJntOrient( midLegFkJnt , midLegFkCtrl )
        
        lowLegFkCtrl = core.addCtrl( 'lowLeg%sFk_%s_ctrl' %( elem , side ) , 'circle' , 'red' , jnt = True )
        lowLegFkGmbl = core.addGimbal( lowLegFkCtrl )
        lowLegFkZro = core.addGrp( lowLegFkCtrl )
        lowLegFkOfst = core.addGrp( lowLegFkCtrl , 'Ofst' )
        core.snapPoint( lowLegFkJnt , lowLegFkZro )
        core.snapJntOrient( lowLegFkJnt , lowLegFkCtrl )
        
        ankleFkCtrl = core.addCtrl( 'ankle%sFk_%s_ctrl' %( elem , side ) , 'circle' , 'red' , jnt = True )
        ankleFkGmbl = core.addGimbal( ankleFkCtrl )
        ankleFkZro = core.addGrp( ankleFkCtrl )
        ankleFkOfst = core.addGrp( ankleFkCtrl , 'Ofst' )
        core.snapPoint( ankleFkJnt , ankleFkZro )
        core.snapJntOrient( ankleFkJnt , ankleFkCtrl )
        
        if foot == True :
            ballFkCtrl = core.addCtrl( 'ball%sFk_%s_ctrl' %( elem , side ) , 'circle' , 'red' , jnt = True )
            ballFkGmbl = core.addGimbal( ballFkCtrl )
            ballFkZro = core.addGrp( ballFkCtrl )
            ballFkOfst = core.addGrp( ballFkCtrl , 'Ofst' )
            core.snapPoint( ballFkJnt , ballFkZro )
            core.snapJntOrient( ballFkJnt , ballFkCtrl )
              
        #-- Shape
        for ctrl in ( upLegFkCtrl , upLegFkGmbl , midLegFkCtrl , midLegFkGmbl , lowLegFkCtrl , lowLegFkGmbl , ankleFkCtrl , ankleFkGmbl ) :
            core.scaleShape( ctrl , charSize )
            
        if foot == True :
            for ctrl in ( ballFkCtrl , ballFkGmbl ) :
                core.scaleShape( ctrl , charSize )
        
        #-- Rotate order
        for each in ( upLegFkJnt , midLegFkJnt , lowLegFkJnt , ankleFkJnt , upLegFkCtrl , midLegFkCtrl , lowLegFkCtrl , ankleFkCtrl ) :
            core.setRotateOrder( each , 'yzx' )
            
        if foot == True :
            for each in ( ballFkJnt , toeFkJnt , ballFkCtrl ) :
                core.setRotateOrder( each , 'yzx' )
                
        #-- Rig process
        core.parCons( 0 , upLegFkGmbl , upLegFkJnt )
        core.parCons( 0 , midLegFkGmbl , midLegFkJnt )
        core.parCons( 0 , lowLegFkGmbl , lowLegFkJnt )
        core.parCons( 0 , ankleFkGmbl , ankleFkJnt )
        
        core.addFkStretch( upLegFkCtrl , midLegFkOfst , 'y' , -0.1 )
        core.addFkStretch( midLegFkCtrl , lowLegFkOfst , 'y' , -0.1 )
        core.addFkStretch( lowLegFkOfst , ankleFkOfst , 'y' , -0.1 )
        
        core.addLocalWorld( upLegFkCtrl , 'anim_grp' , legFkGrp , upLegFkZro , 'orient' )
        
        if foot == True :
            core.parCons( 0 , ballFkGmbl , ballFkJnt )
            core.addFkStretch( ballFkCtrl , toeFkJnt , 'y' , val )
                
        #-- Hierarchy
        mc.parent( legFkGrp , legRigGrp )
        mc.parent( legFkJntGrp , legJntGrp )
        mc.parent( upLegFkZro , legFkGrp )
        mc.parent( midLegFkZro , upLegFkGmbl )
        mc.parent( lowLegFkZro , midLegFkGmbl )
        mc.parent( ankleFkZro , lowLegFkGmbl )
        
        if foot == True :
            mc.parent( ballFkZro , ankleFkGmbl )
            
        #-- Cleanup
        for grp in ( legFkGrp , legFkJntGrp , upLegFkZro , upLegFkOfst , midLegFkZro , midLegFkOfst , lowLegFkZro , lowLegFkOfst , ankleFkZro , ankleFkOfst ) :
            core.setLock( grp , True , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
            
        for ctrl in ( upLegFkCtrl , upLegFkGmbl , midLegFkCtrl , midLegFkGmbl , lowLegFkCtrl , lowLegFkGmbl , ankleFkCtrl , ankleFkGmbl ) :
            core.setLockHide( ctrl , 'sx' , 'sy' , 'sz' , 'v' )
            
        if foot == True :
            for grp in ( ballFkZro , ballFkOfst ) :
                core.setLock( grp , True , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
                
        for ctrl in ( ballFkCtrl , ballFkGmbl ) :
            core.setLockHide( ctrl , 'sx' , 'sy' , 'sz' , 'v' )
            
            
    ## Ik Rig ##
    if legIk == True :
        #-- Group
        legIkGrp = core.transform( 'leg%sIk_%s_grp' %( elem , side ))
        legIkJntGrp = core.transform( 'leg%sIkJnt_%s_grp' %( elem , side ))
        legIkIkhGrp = core.transform( 'leg%sIkHandle_%s_grp' %( elem , side ))
        core.snap( legRigGrp , legIkGrp )
        core.snap( legRigGrp , legIkJntGrp )
        core.snap( legRigGrp , legIkIkhGrp )
        
        if foot == True :
            footPivGrp = core.transform( 'foot%sIkPiv_%s_grp' %( elem , side ))
            footBallPivGrp = core.transform( 'footBall%sIkPiv_%s_grp' %( elem , side ))
            footBendPivGrp = core.transform( 'footBend%sIkPiv_%s_grp' %( elem , side ))
            footHeelPivGrp = core.transform( 'footHeel%sIkPiv_%s_grp' %( elem , side ))
            footToePivGrp = core.transform( 'footToe%sIkPiv_%s_grp' %( elem , side ))
            footInPivGrp = core.transform( 'footIn%sIkPiv_%s_grp' %( elem , side ))
            footOutPivGrp = core.transform( 'footOut%sIkPiv_%s_grp' %( elem , side ))
            
            core.snapPoint( ankleTmpJnt , footPivGrp )
            core.snapPoint( ballTmpJnt , footBallPivGrp )
            core.snapPoint( ballTmpJnt , footBendPivGrp )
            core.snapPoint( toeTmpJnt , footToePivGrp )
            core.snap( heelTmpJnt , footHeelPivGrp )
            core.snap( footInTmpJnt , footInPivGrp )
            core.snap( footOutTmpJnt , footOutPivGrp )
            
            for each in ( footPivGrp , footBallPivGrp , footBendPivGrp , footToePivGrp ) :
                core.snapOrient( footHeelPivGrp , each )
            
            mc.parent( footBallPivGrp , footBendPivGrp , footOutPivGrp )
            core.parent( footOutPivGrp , footInPivGrp , footHeelPivGrp , footToePivGrp , footPivGrp )
            
        #-- Joint
        upLegIkJnt = core.addJnt( 'upLeg%sIk_%s_jnt' %( elem , side ) , upLegTmpJnt )
        midLegIkJnt = core.addJnt( 'midLeg%sIk_%s_jnt' %( elem , side ) , midLegTmpJnt )
        lowLegIkJnt = core.addJnt( 'lowLeg%sIk_%s_jnt' %( elem , side ) , lowLegTmpJnt )
        ankleIkJnt = core.addJnt( 'ankle%sIk_%s_jnt' %( elem , side ) , ankleTmpJnt )
        core.parent( ankleIkJnt , lowLegIkJnt , midLegIkJnt , upLegIkJnt , legIkJntGrp )
        
        upLegIkLJnt = core.addJnt( 'upLeg%sIkL_%s_jnt' %( elem , side ) , upLegTmpJnt )
        midLegIkLJnt = core.addJnt( 'midLeg%sIkL_%s_jnt' %( elem , side ) , midLegTmpJnt )
        lowLegIkLJnt = core.addJnt( 'lowLeg%sIkL_%s_jnt' %( elem , side ) , lowLegTmpJnt )
        ankleIkLJnt = core.addJnt( 'ankle%sIkL_%s_jnt' %( elem , side ) , ankleTmpJnt )
        core.parent( ankleIkLJnt , lowLegIkLJnt , midLegIkLJnt , upLegIkLJnt , legIkJntGrp )
        
        if foot == True :
            ballIkJnt = core.addJnt( 'ball%sIk_%s_jnt' %( elem , side ) , ballTmpJnt )
            toeIkJnt = core.addJnt( 'toe%sIk_%s_jnt' %( elem , side ) , toeTmpJnt )
            core.parent( toeIkJnt , ballIkJnt , ankleIkJnt )
            
        for each in ( midLegIkLJnt , lowLegIkLJnt , ankleIkLJnt ) :
            if not core.getVal( '%s' %each , 'translateX' ) == 0 :
                core.setVal( '%s.tx' %each , 0 )
        
        
        upVecLoc = core.transform( 'upVecLoc_grp')
        upVecLocZro = core.addGrp( upVecLoc )
        core.snap( upLegIkLJnt , upVecLocZro )
        core.setVal( '%s.tx' %upVecLoc , 2 )
        
        mc.delete( mc.aimConstraint ( ankleTmpJnt , upLegIkLJnt , mo = False  , aim = (0,1,0)  , u = (1,0,0) , wut = 'object' , wuo = upVecLoc ))
        
        core.setVal( '%s.rx' %upLegIkLJnt , 0 )
        core.setVal( '%s.ry' %upLegIkLJnt , 0 )
        core.snapPoint( ankleTmpJnt , ankleIkLJnt )
        mc.delete( upVecLocZro )
        
        #-- Controls
        upLegIkCtrl = core.addCtrl( 'upLeg%sIk_%s_ctrl' %( elem , side ) , 'cube' , 'blue' , jnt = True )
        upLegIkGmbl = core.addGimbal( upLegIkCtrl )
        upLegIkZro = core.addGrp( upLegIkCtrl )
        core.snapPoint( upLegIkJnt , upLegIkZro )
        core.snapJntOrient( upLegIkJnt , upLegIkCtrl )
        
        ankleIkCtrl = core.addCtrl( 'ankle%sIk_%s_ctrl' %( elem , side ) , 'cube' , 'blue' , jnt = True )
        ankleIkGmbl = core.addGimbal( ankleIkCtrl )
        ankleIkZro = core.addGrp( ankleIkCtrl )
        core.snapPoint( ankleIkJnt , ankleIkZro )
        core.snapJntOrient( ankleIkJnt , ankleIkCtrl )
        
        kneeIkCtrl = core.addCtrl( 'knee%sIk_%s_ctrl' %( elem , side ) , 'sphere' , 'blue' , jnt = False )
        kneeIkZro = core.addGrp( kneeIkCtrl )
        core.snapPoint( kneeTmpJnt , kneeIkZro )
        
        midLegTmpJntTx = mc.getAttr( '%s.tx' %midLegTmpJnt )
        
        if not int(midLegTmpJntTx) == 0 :
            mc.delete( mc.pointConstraint ( midLegIkLJnt , kneeIkZro , mo = False , sk = 'z' ))
        
        if foot == True :
            ankleRollCtrl = core.addCtrl( 'ankleRoll%sIk_%s_ctrl' %( elem , side ) , 'circle' , 'blue' , jnt = False )
            core.snap( footBallPivGrp , ankleRollCtrl )
            
        #-- Shape
        for ctrl in ( upLegIkCtrl , upLegIkGmbl , ankleIkCtrl , ankleIkGmbl , kneeIkCtrl ) :
            core.scaleShape( ctrl , charSize )
            
        if foot == True :
            core.scaleShape( ankleRollCtrl , charSize )
        
        #-- Rotate order
        for each in ( upLegIkJnt , midLegIkJnt , lowLegIkJnt , ankleIkJnt , upLegIkCtrl , ankleIkCtrl ) :
            core.setRotateOrder( each , 'xzy' )
        
        if foot == True :
            for each in ( ballIkJnt , toeIkJnt , ankleRollCtrl ) :
                core.setRotateOrder( each , 'xzy' )
            
        #-- Rig process
        crv = core.addLine( kneeIkCtrl , midLegIkJnt )
        
        core.addLocalWorld( ankleIkCtrl , 'anim_grp' , upLegIkGmbl , ankleIkZro , 'parent' )
        core.addLocalWorld( kneeIkCtrl , 'anim_grp' , ankleIkCtrl , kneeIkZro , 'parent' )
        
        ankleIkhLDict = core.addIkh( 'ankle%sIkL' %elem , 'ikRPsolver' , upLegIkLJnt , ankleIkLJnt )
        lowLegIkhDict = core.addIkh( 'lowLeg%sIk' %elem , 'ikRPsolver' , upLegIkJnt , lowLegIkJnt )
        ankleIkhDict = core.addIkh( 'ankle%sIk' %elem , 'ikRPsolver' , lowLegIkJnt , ankleIkJnt )
        
        ankleIkhL = ankleIkhLDict['ikh']
        ankleIkhLZro = ankleIkhLDict['ikhZro']
        
        lowLegIkh = lowLegIkhDict['ikh']
        lowLegIkhZro = lowLegIkhDict['ikhZro']
        
        ankleIkh = ankleIkhDict['ikh']
        ankleIkhZro = ankleIkhDict['ikhZro']
        
        polevector = core.pvtCons( kneeIkCtrl , ankleIkhL )
        core.addAttrType( ankleIkCtrl , 'Twist' )
        core.addAttr( ankleIkCtrl , 'upperTwist' )
        core.addAttr( ankleIkCtrl , 'lowerTwist' )
        
        core.connectAttr( '%s.upperTwist' %ankleIkCtrl , '%s.twist' %lowLegIkh )
        core.connectAttr( '%s.lowerTwist' %ankleIkCtrl , '%s.twist' %ankleIkhL )
        
        anklePivot = core.transform( 'ankle%sIkPiv_%s_grp' %( elem , side ))
        core.snapPoint( ankleIkJnt , anklePivot )
        core.snapOrient( kneeTmpJnt , anklePivot )
        
        if foot == False :
            core.parCons( 1 , ankleIkGmbl , ankleIkhLZro  )
            
        core.pntCons( 0 , upLegIkGmbl , upLegIkJnt  )
        core.pntCons( 0 , upLegIkGmbl , upLegIkLJnt  )
        core.parCons( 1 , ankleRollCtrl , ankleIkhLZro  )
        core.parCons( 1 , lowLegIkLJnt , lowLegIkhZro  )
        
        ikStrt = core.addIkStretch( ankleIkCtrl , '' ,'ty' , 'leg%s' %elem , val , upLegIkJnt , midLegIkJnt , lowLegIkJnt , ankleIkJnt )
        mc.connectAttr( 'leg%sIkStrt_%s_pma.o3x' %( elem , side ) , '%s.ty' %midLegIkLJnt )
        mc.connectAttr( 'leg%sIkStrt_%s_pma.o3y' %( elem , side ) , '%s.ty' %lowLegIkLJnt )
        mc.connectAttr( 'leg%sIkStrt_%s_pma.o3z' %( elem , side ) , '%s.ty' %ankleIkLJnt )
        
        if foot == True :
            core.addAttr( ankleIkCtrl , 'toeStretch' )
            toeStrtMdv = core.multiplyDivide( 'toe%sIkStrt_%s_mdv' %( elem , side ))
            mc.setAttr( '%s.i2x' %toeStrtMdv , val )
            
            toeStrtPma = core.plusMinusAverage( 'toe%sIkStrt_%s_pma' %( elem , side ))
            core.addAttr( toeStrtPma , 'default' )
            mc.setAttr( '%s.default' %toeStrtPma , mc.getAttr( '%s.ty' %toeIkJnt ))
            core.connectAttr( '%s.toeStretch' %ankleIkCtrl , '%s.i1x' %toeStrtMdv )
            core.connectAttr( '%s.default' %toeStrtPma , '%s.i1[0]' %toeStrtPma )
            core.connectAttr( '%s.ox' %toeStrtMdv , '%s.i1[1]' %toeStrtPma )
            core.connectAttr( '%s.o1' %toeStrtPma , '%s.ty' %toeIkJnt )
            
            ballIkhDict = core.addIkh( 'ball%sIk' %elem , 'ikSCsolver' , ankleIkJnt , ballIkJnt )
            toeIkhDict = core.addIkh( 'toe%sIk' %elem , 'ikSCsolver' , ballIkJnt , toeIkJnt )
            
            ballIkh = ballIkhDict['ikh']
            ballIkhZro = ballIkhDict['ikhZro']
            
            toeIkh = toeIkhDict['ikh']
            toeIkhZro = toeIkhDict['ikhZro']
            
            core.parCons( 1 , ankleRollCtrl , ankleIkhZro )
            core.parCons( 1 , ankleRollCtrl , ballIkhZro )
            core.parCons( 1 , footBendPivGrp , toeIkhZro )
            
            core.addAttrType( ankleIkCtrl , 'Leg' )
            core.addAttr( ankleIkCtrl , 'legFlex' )
            core.addAttr( ankleIkCtrl , 'legBend' )
            
            core.addAttrType( ankleIkCtrl , 'Foot' )
            core.addAttr( ankleIkCtrl , 'ballRoll' )
            core.addAttr( ankleIkCtrl , 'heelRoll' )
            core.addAttr( ankleIkCtrl , 'toeRoll' )
            core.addAttr( ankleIkCtrl , 'heelTwist' )
            core.addAttr( ankleIkCtrl , 'toeTwist' )
            core.addAttr( ankleIkCtrl , 'toeBend' )
            core.addAttr( ankleIkCtrl , 'toeSide' )
            core.addAttr( ankleIkCtrl , 'toeSwirl' )
            core.addAttr( ankleIkCtrl , 'footRock' )
            
            mc.connectAttr( '%s.legFlex' %ankleIkCtrl , '%s.rx' %anklePivot )
            mc.connectAttr( '%s.legBend' %ankleIkCtrl , '%s.rz' %anklePivot )
            
            mc.connectAttr( '%s.ballRoll' %ankleIkCtrl , '%s.rx' %footBallPivGrp )
            mc.connectAttr( '%s.heelRoll' %ankleIkCtrl , '%s.rx' %footHeelPivGrp )
            mc.connectAttr( '%s.toeRoll' %ankleIkCtrl , '%s.rx' %footToePivGrp )
            mc.connectAttr( '%s.heelTwist' %ankleIkCtrl , '%s.ry' %footHeelPivGrp )
            mc.connectAttr( '%s.toeTwist' %ankleIkCtrl , '%s.ry' %footToePivGrp )
            mc.connectAttr( '%s.toeBend' %ankleIkCtrl , '%s.rx' %footBendPivGrp )
            mc.connectAttr( '%s.toeSide' %ankleIkCtrl , '%s.ry' %footBendPivGrp )
            mc.connectAttr( '%s.toeSwirl' %ankleIkCtrl , '%s.rz' %footBendPivGrp )
            mc.connectAttr( '%s.footRock' %ankleIkCtrl , '%s.rz' %footInPivGrp )
            mc.connectAttr( '%s.footRock' %ankleIkCtrl , '%s.rz' %footOutPivGrp )
            
            mc.transformLimits( footInPivGrp , erz = ( True , False ) , rz = ( 0 , 45 ) )
            mc.transformLimits( footOutPivGrp , erz = ( False , True ) , rz = ( -45 , 0 ) )
        
        #-- Hierarchy
        mc.parent( legIkGrp , legRigGrp )
        mc.parent( legIkJntGrp , legJntGrp )
        mc.parent( legIkIkhGrp , ikhGrp )
        mc.parent( upLegIkZro , ikStrt , crv , legIkGrp )
        mc.parent( ankleIkZro , kneeIkZro , upLegIkGmbl )
        mc.parent( lowLegIkhZro , ankleIkhZro , ankleIkhLZro , legIkIkhGrp )
        core.parent( lowLegIkh , anklePivot , lowLegIkhZro )
        
        if foot == True :
            mc.parent( ankleRollCtrl , footBallPivGrp )
            mc.parent( footPivGrp , ankleIkGmbl )
            mc.parent( ballIkhZro , toeIkhZro , legIkIkhGrp )
        
        #-- Cleanup
        for grp in ( legIkGrp , legIkJntGrp , upLegIkZro , lowLegIkhZro , ankleIkZro , footPivGrp , footBallPivGrp , footBendPivGrp , footHeelPivGrp , footToePivGrp , footInPivGrp , footOutPivGrp , anklePivot ) :
            core.setLock( grp , True , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
        
        for ctrl in ( upLegIkCtrl , upLegIkGmbl , kneeIkCtrl , ankleIkCtrl , ankleIkGmbl ) :
            core.setLockHide( ctrl , 'sx' , 'sy' , 'sz' , 'v' )
            
        if foot == True :
            core.setLockHide( ankleRollCtrl , 'sx' , 'sy' , 'sz' , 'v' )
                
        core.setVal( '%s.localWorld' %ankleIkCtrl , 1 )
        
        
    ## Fk Ik Blending ##
    if legFk == True and legIk == True :
        #-- Controls
        
        core.addBlendFkIk( legCtrl , upLegFkJnt , upLegIkJnt , upLegJnt )
        core.addBlendFkIk( legCtrl , midLegFkJnt , midLegIkJnt , midLegJnt )
        core.addBlendFkIk( legCtrl , lowLegFkJnt , lowLegIkJnt , lowLegJnt )
        core.addBlendFkIk( legCtrl , ankleFkJnt , ankleIkJnt , ankleJnt )
        
        if foot == True :
            core.addBlendFkIk( legCtrl , ballFkJnt , ballIkJnt , ballJnt )
            core.addBlendFkIk( legCtrl , toeFkJnt , toeIkJnt , toeJnt )
        
        if mc.objExists( 'leg%sFkIk_%s_rev' %( elem , side )) :
            revFkIk = 'leg%sFkIk_%s_rev' %( elem , side )
            core.connectAttr( '%s.fkIk' %legCtrl , '%s.v' %legIkGrp )
            core.connectAttr( '%s.ox' %revFkIk , '%s.v' %legFkGrp )
        
        core.setVal( '%s.fkIk' %legCtrl , 1 )
        
    elif legFk == True and legIk == False :
        core.parCons( 0 , upLegFkJnt , upLegJnt )
        core.parCons( 0 , midLegFkJnt , midLegJnt )
        core.parCons( 0 , lowLegFkJnt , lowLegJnt )
        core.parCons( 0 , ankleFkJnt , ankleJnt )
        
        if foot == True :
            core.parCons( 0 , ballFkJnt , ballJnt )
            core.parCons( 0 , toeFkJnt , toeJnt )
            
    elif legFk == False and legIk == True :
        core.scaleShape( legCtrl , charSize )
        core.parCons( 0 , upLegIkJnt , upLegJnt )
        core.parCons( 0 , midLegFkJnt , midLegJnt )
        core.parCons( 0 , lowLegIkJnt , lowLegJnt )
        core.parCons( 0 , ankleIkJnt , ankleJnt )
        
        if foot == True :
            core.parCons( 0 , ballIkJnt , ballJnt )
            core.parCons( 0 , toeIkJnt , toeJnt )
            
            
    ## Ribbon ##
    if ribbon == True :
        rbnAxis = 'y-'
        rbnAim = ( 0,-1,0 )
        
        if side == 'rgt' :
            rbnUp = ( 0,0,-1 )
            rootTwsAmp = -1
            endTwsAmp = 1
        else :
            rbnUp = ( 0,0,1 )
            rootTwsAmp = 1
            endTwsAmp = -1
            
        rbnUpLegPar  = core.getParent( upLegJnt )
        rbnMidLegPar = core.getParent( midLegJnt )
        rbnLowLegPar = core.getParent( lowLegJnt )
        rbnAnklePar  = core.getParent( ankleJnt )
        
        #-- Group
        legRbnGrp = core.transform( 'leg%sRbn_%s_grp' %( elem , side ))
        
        #-- Controls
        upLegRbnCtrl = core.addCtrl( 'upKnee%sRbn_%s_ctrl' %( elem , side ) , 'locator' , 'yellow' , jnt = True )
        upLegRbnZro = core.addGrp( upLegRbnCtrl )
        core.snapPoint( midLegJnt , upLegRbnZro )
        core.snapJntOrient( midLegJnt , upLegRbnCtrl )
        mc.parent( upLegRbnZro , legRbnGrp )
        
        lowLegRbnCtrl = core.addCtrl( 'lowKnee%sRbn_%s_ctrl' %( elem , side ) , 'locator' , 'yellow' , jnt = True )
        lowLegRbnZro = core.addGrp( lowLegRbnCtrl )
        core.snapPoint( lowLegJnt , lowLegRbnZro )
        core.snapJntOrient( lowLegJnt , lowLegRbnCtrl )
        mc.parent( lowLegRbnZro , legRbnGrp )
        
        #-- Shape
        for ctrl in ( upLegRbnCtrl , lowLegRbnCtrl ) :
            core.scaleShape( ctrl , charSize )
        
        #-- Rig process
        core.parCons( 1 , midLegJnt , upLegRbnZro )
        core.parCons( 1 , lowLegJnt , lowLegRbnZro )
        
        rbnUpLeg = rbn.rigRibbon( 'upLeg%s' %elem , ax = rbnAxis , jntRoot = upLegJnt , jntEnd = midLegJnt , animGrp = legRbnGrp , skinGrp = skinGrp , stillGrp = stillGrp , charSize = charSize )
        rbnMidLeg = rbn.rigRibbon( 'midLeg%s' %elem , ax = rbnAxis , jntRoot = midLegJnt , jntEnd = lowLegJnt , animGrp = legRbnGrp , skinGrp = skinGrp , stillGrp = stillGrp , charSize = charSize )
        rbnLowLeg = rbn.rigRibbon( 'lowLeg%s' %elem , ax = rbnAxis , jntRoot = lowLegJnt , jntEnd = ankleJnt , animGrp = legRbnGrp , skinGrp = skinGrp , stillGrp = stillGrp , charSize = charSize )
        
        rbnUpLegGrp = rbnUpLeg['rbnGrp']
        rbnUpLegCtrl = rbnUpLeg['rbnCtrl']
        rbnUpLegRootCtrl = rbnUpLeg['rbnRootCtrl']
        rbnUpLegEndCtrl = rbnUpLeg['rbnEndCtrl']
        rbnUpLegRootTwZro = rbnUpLeg['rbnRootTwZro']
        rbnUpLegEndTwZro = rbnUpLeg['rbnEndTwZro']
        rbnUpLegRootTwGrp = rbnUpLeg['rbnRootTwGrp']
        rbnUpLegEndTwGrp = rbnUpLeg['rbnEndTwGrp']
        rbnUpLegCtrlShape = rbnUpLeg['rbnCtrlShape']
        
        rbnMidLegGrp = rbnMidLeg['rbnGrp']
        rbnMidLegCtrl = rbnMidLeg['rbnCtrl']
        rbnMidLegRootCtrl = rbnMidLeg['rbnRootCtrl']
        rbnMidLegEndCtrl = rbnMidLeg['rbnEndCtrl']
        rbnMidLegRootTwZro = rbnMidLeg['rbnRootTwZro']
        rbnMidLegEndTwZro = rbnMidLeg['rbnEndTwZro']
        rbnMidLegRootTwGrp = rbnMidLeg['rbnRootTwGrp']
        rbnMidLegEndTwGrp = rbnMidLeg['rbnEndTwGrp']
        rbnMidLegCtrlShape = rbnMidLeg['rbnCtrlShape']
        
        rbnLowLegGrp = rbnLowLeg['rbnGrp']
        rbnLowLegCtrl = rbnLowLeg['rbnCtrl']
        rbnLowLegRootCtrl = rbnLowLeg['rbnRootCtrl']
        rbnLowLegEndCtrl = rbnLowLeg['rbnEndCtrl']
        rbnLowLegRootTwZro = rbnLowLeg['rbnRootTwZro']
        rbnLowLegEndTwZro = rbnLowLeg['rbnEndTwZro']
        rbnLowLegRootTwGrp = rbnLowLeg['rbnRootTwGrp']
        rbnLowLegEndTwGrp = rbnLowLeg['rbnEndTwGrp']
        rbnLowLegCtrlShape = rbnLowLeg['rbnCtrlShape']
        
        mc.setAttr( '%s.rootTwistAmp' %rbnUpLegCtrlShape , rootTwsAmp )
        mc.setAttr( '%s.endTwistAmp' %rbnUpLegCtrlShape , endTwsAmp )
        
        mc.setAttr( '%s.rootTwistAmp' %rbnMidLegCtrlShape , rootTwsAmp )
        mc.setAttr( '%s.endTwistAmp' %rbnMidLegCtrlShape , endTwsAmp )
        
        mc.setAttr( '%s.rootTwistAmp' %rbnLowLegCtrlShape , rootTwsAmp )
        mc.setAttr( '%s.endTwistAmp' %rbnLowLegCtrlShape , endTwsAmp )
        
        for each in ( rbnUpLegRootTwZro , rbnUpLegRootTwGrp , rbnUpLegEndTwZro , rbnUpLegEndTwGrp , rbnMidLegRootTwZro , rbnMidLegRootTwGrp , rbnMidLegEndTwZro , rbnMidLegEndTwGrp , rbnLowLegRootTwZro , rbnLowLegRootTwGrp , rbnLowLegEndTwZro , rbnLowLegEndTwGrp ) :
            core.setRotateOrder( each , 'yzx' )
            
        core.snapPoint( upLegJnt , rbnUpLegGrp )
        mc.delete( mc.aimConstraint( midLegJnt , rbnUpLegGrp , aim = rbnAim , u = rbnUp , wut = 'objectrotation' , wuo = midLegTmpJnt , wu = ( 0,0,1 ) ))
        core.snap( upLegJnt , rbnUpLegRootTwZro )
        core.snap( midLegJnt , rbnUpLegEndTwZro )
        core.parCons( 1 , upLegJnt , rbnUpLegGrp )
        core.pntCons( 0 , upLegJnt , rbnUpLegRootCtrl )
        core.pntCons( 0 , upLegRbnCtrl , rbnUpLegEndCtrl )
        core.parCons( 1 , rbnUpLegPar , rbnUpLegRootTwZro )
        core.parCons( 1 , rbnMidLegPar , rbnUpLegEndTwZro )
        core.parCons( 0 , upLegJnt , rbnUpLegRootTwGrp )
        core.parCons( 0 , midLegJnt , rbnUpLegEndTwGrp )
        
        core.snapPoint( midLegJnt , rbnMidLegGrp )
        mc.delete( mc.aimConstraint( lowLegJnt , rbnMidLegGrp , aim = rbnAim , u = rbnUp , wut = 'objectrotation' , wuo = lowLegTmpJnt , wu = ( 0,0,1 ) ))
        core.snap( midLegJnt , rbnMidLegRootTwZro )
        core.snap( lowLegJnt , rbnMidLegEndTwZro )
        core.parCons( 1 , midLegJnt , rbnMidLegGrp )
        core.pntCons( 0 , upLegRbnCtrl , rbnMidLegRootCtrl )
        core.pntCons( 0 , lowLegRbnCtrl , rbnMidLegEndCtrl )
        core.parCons( 1 , rbnLowLegPar , rbnMidLegEndTwZro )
        core.parCons( 0 , lowLegJnt , rbnMidLegEndTwGrp )
        
        core.snapPoint( lowLegJnt , rbnLowLegGrp )
        mc.delete( mc.aimConstraint( ankleJnt , rbnLowLegGrp , aim = rbnAim , u = rbnUp , wut = 'objectrotation' , wuo = ankleTmpJnt , wu = ( 0,0,1 ) ))
        core.snap( lowLegJnt , rbnLowLegRootTwZro )
        core.snap( ankleJnt , rbnLowLegEndTwZro )
        core.parCons( 1 , lowLegJnt , rbnLowLegGrp )
        core.pntCons( 0 , lowLegRbnCtrl , rbnLowLegRootCtrl )
        core.pntCons( 0 , ankleJnt , rbnLowLegEndCtrl )
        core.parCons( 1 , rbnAnklePar , rbnLowLegEndTwZro )
        core.parCons( 0 , ankleJnt , rbnLowLegEndTwGrp )
        
        #-- Hierarchy
        mc.parent( legRbnGrp , legRigGrp )
        
        #-- Cleanup
        for each in ( legRbnGrp , upLegRbnZro , lowLegRbnZro , rbnUpLegGrp , rbnMidLegGrp , rbnLowLegGrp , rbnUpLegRootCtrl , rbnUpLegEndCtrl , rbnMidLegRootCtrl , rbnMidLegEndCtrl , rbnLowLegRootCtrl , rbnLowLegEndCtrl , rbnUpLegRootTwZro , rbnUpLegEndTwZro , rbnUpLegRootTwGrp , rbnUpLegEndTwGrp , rbnMidLegRootTwZro , rbnMidLegEndTwZro , rbnMidLegRootTwGrp , rbnMidLegEndTwGrp , rbnLowLegRootTwZro , rbnLowLegEndTwZro , rbnLowLegRootTwGrp , rbnLowLegEndTwGrp ) :
            core.setLock( each , True , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
        
        for each in ( rbnUpLegRootCtrl , rbnUpLegEndCtrl , rbnMidLegRootCtrl , rbnMidLegEndCtrl , rbnLowLegRootCtrl , rbnLowLegEndCtrl ) :
            core.setVal( '%s.v' %each , 0 )
        
        for each in ( rbnUpLegCtrl , rbnMidLegCtrl , rbnLowLegCtrl ) :
            core.setVal( '%s.autoTwist' %each , 1 )
        
        for each in ( upLegRbnCtrl , lowLegRbnCtrl ) :
            core.setLockHide( each , 'sx' , 'sy' , 'sz' , 'v' )
    
    mc.select( cl = True )