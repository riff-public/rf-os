import maya.cmds as mc
import maya.mel as mm
import ppmel.core as core
reload( core )


def rigHead( head1TmpJnt = 'head1_tmpJnt' ,
             head2TmpJnt = 'head2_tmpJnt' ,
             eyeTrgTmpJnt = 'eyeTrgt_tmpJnt' ,
             eyeLftTrgTmpJnt = 'eyeTrgt_lft_tmpJnt' ,
             eyeRgtTrgTmpJnt = 'eyeTrgt_rgt_tmpJnt' ,
             eyeLftTmpJnt = 'eye_lft_tmpJnt' ,
             eyeRgtTmpJnt = 'eye_rgt_tmpJnt' ,
             jaw1UprTmpJnt = 'jaw1_upr_tmpJnt' ,
             jaw2UprTmpJnt = 'jaw2_upr_tmpJnt' ,
             jaw1LwrTmpJnt = 'jaw1_lwr_tmpJnt' ,
             jaw2LwrTmpJnt = 'jaw2_lwr_tmpJnt' ,
             jaw3LwrTmpJnt = 'jaw3_lwr_tmpJnt' ,
             parent = 'neck2_jnt' ,
             animGrp = 'anim_grp' ,
             skinGrp = 'skin_grp' ,
             jntGrp = 'jnt_grp' ,
             eyeRig = True ,
             jawRig = True ,
             type = 'human' , # human , animal
             charSize = 1 ) :
    
    ## Head Rig ##
    #-- Group
    headRigGrp = core.transform( 'headRig_grp' )
    core.parCons( 0 , parent , headRigGrp )
    
    #-- Joint
    head1Jnt = core.addJnt( 'head1_jnt' , head1TmpJnt )
    head2Jnt = core.addJnt( 'head2_jnt' , head2TmpJnt )
    core.parent( head2Jnt , head1Jnt , parent )
    
    #-- Controls
    headCtrl = core.addCtrl( 'head_ctrl' , 'cube' , 'blue' , jnt = True )
    headGmbl = core.addGimbal( headCtrl )
    headZro = core.addGrp( headCtrl )
    core.snapPoint( head1Jnt , headZro )
    core.snapJntOrient( head1Jnt , headCtrl )
    
    #-- Shape
    for ctrl in ( headCtrl , headGmbl ) :
        core.scaleShape( ctrl , charSize )
    
    #-- Rotate order
    for each in ( headCtrl , head1Jnt , head1Jnt ) :
        core.setRotateOrder( each , 'xzy' )
    
    #-- Rig process
    core.setVal( '%s.ssc' %headGmbl , 0 )
    core.setVal( '%s.ssc' %head2Jnt , 0 )
    core.parCons( 0 , headGmbl , head1Jnt )
    core.sclCons( 0 , headGmbl , head1Jnt )
    core.addLocalWorld( headCtrl , 'anim_grp' , headRigGrp , headZro , 'orient' )
    
    #-- Hierarchy
    core.parent( headZro , headRigGrp , animGrp )
    
    #-- Cleanup
    for grp in ( headRigGrp , headZro ) :
        core.setLock( grp , True , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
    
    for ctrl in ( headCtrl , headGmbl ) :
        core.setLockHide( ctrl , 'v' )
    
    
    ## Eye Rig ##
    if eyeRig == True :
        #-- Group
        eyeRigGrp = core.transform( 'eyeRig_grp' )
        core.snap( head1Jnt , eyeRigGrp )
        
        #-- Joint
        eyeLftJnt = core.addJnt( 'eye_lft_jnt' , eyeLftTmpJnt )
        eyeRgtJnt = core.addJnt( 'eye_rgt_jnt' , eyeRgtTmpJnt )
        eyeLidLftJnt = core.addJnt( 'eyeLid_lft_jnt' , eyeLftTmpJnt )
        eyeLidRgtJnt = core.addJnt( 'eyeLid_rgt_jnt' , eyeRgtTmpJnt )
        
        core.parent( eyeLidLftJnt , eyeLftJnt , head1Jnt )
        core.parent( eyeLidRgtJnt , eyeRgtJnt , head1Jnt )
        
        #-- Controls
        eyeLftCtrl = core.addCtrl( 'eye_lft_ctrl' , 'sphere' , 'red' , jnt = False )
        eyeLftGmbl = core.addGimbal( eyeLftCtrl )
        eyeLftZro = core.addGrp( eyeLftCtrl )
        eyeLftAim = core.addGrp( eyeLftCtrl , 'Aim' )
        core.snap( eyeLftJnt , eyeLftZro )
        
        eyeRgtCtrl = core.addCtrl( 'eye_rgt_ctrl' , 'sphere' , 'red' , jnt = False )
        eyeRgtGmbl = core.addGimbal( eyeRgtCtrl )
        eyeRgtZro = core.addGrp( eyeRgtCtrl )
        eyeRgtAim = core.addGrp( eyeRgtCtrl , 'Aim' )
        core.snap( eyeRgtJnt , eyeRgtZro )
        
        eyeTrgCtrl = core.addCtrl( 'eyeTarget_ctrl' , 'capsule' , 'yellow' , jnt = False )
        eyeTrgZro = core.addGrp( eyeTrgCtrl )
        core.snap( eyeTrgTmpJnt , eyeTrgZro )
        
        eyeLftTrgCtrl = core.addCtrl( 'eyeTarget_lft_ctrl' , 'circle' , 'red' , jnt = False )
        eyeLftTrgZro = core.addGrp( eyeLftTrgCtrl )
        core.snap( eyeLftTrgTmpJnt , eyeLftTrgZro )
        
        eyeRgtTrgCtrl = core.addCtrl( 'eyeTarget_rgt_ctrl' , 'circle' , 'blue' , jnt = False )
        eyeRgtTrgZro = core.addGrp( eyeRgtTrgCtrl )
        core.snap( eyeRgtTrgTmpJnt , eyeRgtTrgZro )
        
        #-- Shape
        for ctrl in ( eyeLftCtrl , eyeLftGmbl , eyeRgtCtrl , eyeRgtGmbl ) :
            core.scaleShape( ctrl , charSize * 0.5 )
        
        for ctrl in ( eyeLftTrgCtrl , eyeRgtTrgCtrl ) :
            core.scaleShape( ctrl , charSize * 0.4 )
            core.rotateShape( ctrl , 90 , 0 , 0 )
        
        core.scaleShape( eyeTrgCtrl , charSize * 0.55 )
        
        #-- Rig process
        core.setVal( '%s.ssc' %eyeLftJnt , 0 )
        core.setVal( '%s.ssc' %eyeRgtJnt , 0 )
        core.setVal( '%s.ssc' %eyeLidLftJnt , 0 )
        core.setVal( '%s.ssc' %eyeLidRgtJnt , 0 )
        
        core.parCons( 0 , eyeLftGmbl , eyeLftJnt )
        core.sclCons( 0 , eyeLftGmbl , eyeLftJnt )
        core.parCons( 0 , eyeRgtGmbl , eyeRgtJnt )
        core.sclCons( 0 , eyeRgtGmbl , eyeRgtJnt )
        
        core.addLocalWorld( eyeTrgCtrl , 'anim_grp' , headGmbl , eyeTrgZro , 'orient' )
        
        mc.rename( mc.aimConstraint( eyeLftTrgCtrl , eyeLftAim , aim = (0,0,1) , u = (0,1,0) , wut = "objectrotation" , wu = (0,1,0) , wuo = eyeLftZro , mo = 1 )[0] , '%s_aimCons' %eyeLftAim )
        mc.rename( mc.aimConstraint( eyeRgtTrgCtrl , eyeRgtAim , aim = (0,0,1) , u = (0,1,0) , wut = "objectrotation" , wu = (0,1,0) , wuo = eyeRgtZro , mo = 1 )[0] , '%s_aimCons' %eyeRgtAim )
        
        lidLftCons = core.oriCons( 1 , head1Jnt , eyeLftJnt , eyeLidLftJnt )
        lidRgtCons = core.oriCons( 1 , head1Jnt , eyeRgtJnt , eyeLidRgtJnt )
        
        lidLftRev = core.reverse( 'eyelid_lft_rev' )
        lidRgtRev = core.reverse( 'eyelid_rgt_rev' )
        
        for ctrl in ( eyeLftCtrl , eyeRgtCtrl ) :
            core.addAttr( ctrl , 'lidFollow' , 0 , 1 , 0 )
        
        mc.connectAttr( '%s.lidFollow' %eyeLftCtrl , '%s.eye_lft_jntW1' %lidLftCons )
        mc.connectAttr( '%s.lidFollow' %eyeLftCtrl , '%s.ix' %lidLftRev )
        mc.connectAttr( '%s.ox' %lidLftRev , '%s.head1_jntW0' %lidLftCons )
        
        mc.connectAttr( '%s.lidFollow' %eyeRgtCtrl , '%s.eye_rgt_jntW1' %lidRgtCons )
        mc.connectAttr( '%s.lidFollow' %eyeRgtCtrl , '%s.ix' %lidRgtRev )
        mc.connectAttr( '%s.ox' %lidRgtRev , '%s.head1_jntW0' %lidRgtCons )
        
        #-- Hierarchy
        mc.parent( eyeLftZro , eyeRgtZro , eyeTrgZro , eyeRigGrp )
        mc.parent( eyeLftTrgZro , eyeRgtTrgZro , eyeTrgCtrl )
        mc.parent( eyeRigGrp , headGmbl )
        
        #-- Cleanup
        for grp in ( eyeRigGrp , eyeLftZro , eyeRgtZro , eyeLftTrgZro , eyeRgtTrgZro , eyeTrgZro ) :
            core.setLock( grp , True , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
        
        for ctrl in ( eyeLftGmbl , eyeRgtGmbl , eyeLftTrgCtrl , eyeRgtTrgCtrl , eyeTrgCtrl ) :
            core.setLockHide( ctrl , 'sx' , 'sy' , 'sz' , 'v' )
        
        for ctrl in ( eyeLftCtrl , eyeRgtCtrl ) :
            core.setLockHide( ctrl , 'v' )
    
    
    ## Jaw Rig ##
    if jawRig == True :
        #-- Group
        jawRigGrp = core.transform( 'jawRig_grp' )
        core.snap( head1Jnt , jawRigGrp )
        
        #-- Joint
        jawLwrJnt = core.addJnt( 'jaw_lwr_jnt' , jaw1LwrTmpJnt )
        teeth1LwrJnt = core.addJnt( 'teeth1_lwr_jnt' , jaw2LwrTmpJnt )
        teeth2LwrJnt = core.addJnt( 'teeth2_lwr_jnt' , jaw3LwrTmpJnt )
        teeth1UprJnt = core.addJnt( 'teeth1_upr_jnt' , jaw1UprTmpJnt )
        teeth2UprJnt = core.addJnt( 'teeth2_upr_jnt' , jaw2UprTmpJnt )
        
        core.parent( teeth2LwrJnt , teeth1LwrJnt , jawLwrJnt , head1Jnt )
        core.parent( teeth2UprJnt , teeth1UprJnt , head1Jnt )
        
        #-- Controls
        jawLwrCtrl = core.addCtrl( 'jaw_lwr_ctrl' , 'square' , 'red' , jnt = True )
        jawLwrGmbl = core.addGimbal( jawLwrCtrl )
        jawLwrZro = core.addGrp( jawLwrCtrl )
        core.snapPoint( jawLwrJnt , jawLwrZro )
        core.snapJntOrient( jawLwrJnt , jawLwrCtrl )
        
        teethLwrCtrl = core.addCtrl( 'teeth_lwr_ctrl' , 'square' , 'yellow' , jnt = True )
        teethLwrGmbl = core.addGimbal( teethLwrCtrl )
        teethLwrZro = core.addGrp( teethLwrCtrl )
        core.snapPoint( teeth1LwrJnt , teethLwrZro )
        core.snapJntOrient( teeth1LwrJnt , teethLwrCtrl )

        teethUprCtrl = core.addCtrl( 'teeth_upr_ctrl' , 'square' , 'yellow' , jnt = True )
        teethUprGmbl = core.addGimbal( teethUprCtrl )
        teethUprZro = core.addGrp( teethUprCtrl )
        core.snapPoint( teeth1UprJnt , teethUprZro )
        core.snapJntOrient( teeth1UprJnt , teethUprCtrl )
        
        #-- Shape
        for ctrl in ( teethUprCtrl , teethUprGmbl , teethLwrCtrl , teethLwrGmbl ) :
            core.scaleShape( ctrl , charSize * 0.3 )
            core.rotateShape( ctrl , 90 , 0 , 0 )
        
        for ctrl in ( jawLwrCtrl , jawLwrGmbl ) :
            core.scaleShape( ctrl , charSize * 0.9 )
        
        #-- Rotate order
        for each in ( teethUprCtrl , jawLwrCtrl , teethLwrCtrl , teeth1UprJnt , teeth2UprJnt , jawLwrJnt , teeth1LwrJnt , teeth2LwrJnt ) :
            core.setRotateOrder( each , 'zyx' )
        
        #-- Rig process
        core.setVal( '%s.ssc' %teeth1UprJnt , 0 )
        core.setVal( '%s.ssc' %jawLwrJnt , 0 )
        
        core.parCons( 0 , teethUprGmbl , teeth1UprJnt )
        core.sclCons( 0 , teethUprGmbl , teeth1UprJnt )
        core.parCons( 0 , jawLwrGmbl , jawLwrJnt )
        core.sclCons( 0 , jawLwrGmbl , jawLwrJnt )
        core.parCons( 0 , teethLwrGmbl , teeth1LwrJnt )
        core.sclCons( 0 , teethLwrGmbl , teeth1LwrJnt )
        
        #-- Hierarchy
        mc.parent( teethUprZro , jawLwrZro , jawRigGrp )
        mc.parent( teethLwrZro , jawLwrGmbl )
        mc.parent( jawRigGrp , headGmbl )
        
        #-- Cleanup
        for grp in ( jawRigGrp , teethUprZro , jawLwrZro , teethLwrZro ) :
            core.setLock( grp , True , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
        
        for ctrl in ( teethUprCtrl , jawLwrCtrl , teethLwrCtrl ) :
            core.setLockHide( ctrl , 'v' )
    
    mc.select( cl = True )