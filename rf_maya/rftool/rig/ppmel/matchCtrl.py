import maya.cmds as mc
import maya.mel as mm


def match() :
    sel = mc.ls( sl = True )[0]
    
    np = sel.split( ':' )[0]
    
    if 'arm' in sel :
        armMatch( '%s:' %np )
    elif 'leg' in sel :
        legMatch( '%s:' %np )


def armMatch( nameSpace = '' ) :
    sel = mc.ls( sl = True )[0]
    
    if 'lft' in sel :
        side = 'lft'
    else :
        side = 'rgt'
    
    #-- Arm control
    arm = '%sarm_%s_ctrl' %( nameSpace , side )
    
    #-- Fk control
    upArmFkCtrl = '%supArmFk_%s_ctrl' %( nameSpace , side )
    foreArmFkCtrl = '%sforeArmFk_%s_ctrl' %( nameSpace , side )
    wristFkCtrl = '%swristFk_%s_ctrl' %( nameSpace , side )
    
    #-- Ik control
    upArmIkCtrl = '%supArmIk_%s_ctrl' %( nameSpace , side )
    elbowIkCtrl = '%selbowIk_%s_ctrl' %( nameSpace , side )
    wristIkCtrl = '%swristIk_%s_ctrl' %( nameSpace , side )
    
    #-- Fk joint
    upArmFkJnt = '%supArmFk_%s_jnt' %( nameSpace , side )
    foreArmFkJnt = '%sforeArmFk_%s_jnt' %( nameSpace , side )
    wristFkJnt = '%swristFk_%s_jnt' %( nameSpace , side )
    handFkJnt = '%shandFk_%s_jnt' %( nameSpace , side )
    
    #-- Ik joint
    upArmIkJnt = '%supArmIk_%s_jnt' %( nameSpace , side )
    foreArmIkJnt = '%sforeArmIk_%s_jnt' %( nameSpace , side )
    wristIkJnt = '%swristIk_%s_jnt' %( nameSpace , side )
    handIkJnt = '%shandIk_%s_jnt' %( nameSpace , side )
    
    #-- Template position
    upArm = mc.createNode( 'joint' )
    upArmGrp = mc.group( upArm )
    foreArm = mc.createNode( 'joint' )
    foreArmGrp = mc.group( foreArm )
    wrist = mc.createNode( 'joint' )
    wristGrp = mc.group( wrist )
    
    #-- Fk to Ik
    if mc.getAttr('%s.fkIk' %sel) == 0 :
        #-- Adjust attribute
        mc.setAttr( '%s.rotateOrder' %upArm , mc.getAttr( '%s.rotateOrder' %upArmIkCtrl ))
        mc.setAttr( '%s.rotateOrder' %foreArm , mc.getAttr( '%s.rotateOrder' %elbowIkCtrl ))
        mc.setAttr( '%s.rotateOrder' %wrist , mc.getAttr( '%s.rotateOrder' %wristIkCtrl ))
        
        '''
        upArmJntOri = mc.getAttr( '%s.jointOrient' %upArmIkCtrl )[0]
        mc.setAttr( '%s.jointOrient' %upArm , upArmJntOri[0] , upArmJntOri[1] , upArmJntOri[2] )
        '''
        
        wristJntOri = mc.getAttr( '%s.jointOrient' %wristIkCtrl )[0]
        mc.setAttr( '%s.jointOrient' %wrist , wristJntOri[0] , wristJntOri[1] , wristJntOri[2] )
        
        #-- Adjust position
        # upArm
        mc.delete( mc.parentConstraint ( mc.listRelatives( upArmIkCtrl , p = True ) , upArmGrp , mo = False ))
        mc.delete( mc.parentConstraint ( upArmFkJnt , upArm , mo = False ))
        upArmPosi = mc.getAttr( '%s.t' %upArm )[0]
        mc.setAttr( '%s.t' %upArmIkCtrl , upArmPosi[0] , upArmPosi[1] , upArmPosi[2] )
        
        # wrist
        mc.delete( mc.parentConstraint ( mc.listRelatives( wristIkCtrl , p = True ) , wristGrp , mo = False ))
        mc.delete( mc.parentConstraint ( wristFkJnt , wrist , mo = False ))
        wristPosi = mc.getAttr( '%s.t' %wrist )[0]
        wristOri = mc.getAttr( '%s.r' %wrist )[0]
        mc.setAttr( '%s.t' %wristIkCtrl , wristPosi[0] , wristPosi[1] , wristPosi[2] )
        mc.setAttr( '%s.r' %wristIkCtrl , wristOri[0] , wristOri[1] , wristOri[2] )
        
        # elbow
        mc.delete( mc.parentConstraint ( mc.listRelatives( elbowIkCtrl , p = True ) , foreArmGrp , mo = False ))
        mc.delete( mc.parentConstraint ( foreArmFkJnt , foreArm , mo = False ))
        elbowPosi = mc.getAttr( '%s.t' %foreArm )[0]
        mc.setAttr( '%s.t' %elbowIkCtrl , elbowPosi[0] , elbowPosi[1] , elbowPosi[2] )
        
        #-- Adjust twist and stretch
        mc.setAttr( '%s.twist' %wristIkCtrl , 0 )
        mc.setAttr( '%s.autoStretch' %wristIkCtrl , 0 )
        upArmStretch = mc.getAttr( '%s.stretch' %upArmFkCtrl )
        mc.setAttr( '%s.upArmStretch' %wristIkCtrl , upArmStretch )
        foreArmStretch = mc.getAttr( '%s.stretch' %foreArmFkCtrl )
        mc.setAttr( '%s.foreArmStretch' %wristIkCtrl , foreArmStretch )
        
        #-- Switch fk to ik
        mc.setAttr( '%s.fkIk' %arm , 1 )
    
    #-- Ik to Fk
    elif mc.getAttr('%s.fkIk' %sel) == 1 :
        #-- Adjust attribute
        mc.setAttr( '%s.rotateOrder' %upArm , mc.getAttr( '%s.rotateOrder' %upArmFkCtrl ))
        mc.setAttr( '%s.rotateOrder' %foreArm , mc.getAttr( '%s.rotateOrder' %foreArmFkCtrl ))
        mc.setAttr( '%s.rotateOrder' %wrist , mc.getAttr( '%s.rotateOrder' %wristFkCtrl ))
        
        upArmJntOri = mc.getAttr( '%s.jointOrient' %upArmFkCtrl )[0]
        mc.setAttr( '%s.jointOrient' %upArm , upArmJntOri[0] , upArmJntOri[1] , upArmJntOri[2] )
        
        foreArmJntOri = mc.getAttr( '%s.jointOrient' %foreArmFkCtrl )[0]
        mc.setAttr( '%s.jointOrient' %foreArm , foreArmJntOri[0] , foreArmJntOri[1] , foreArmJntOri[2] )
        
        wristJntOri = mc.getAttr( '%s.jointOrient' %wristFkCtrl )[0]
        mc.setAttr( '%s.jointOrient' %wrist , wristJntOri[0] , wristJntOri[1] , wristJntOri[2] )
        
        #-- Adjust position
        # upArm
        mc.delete( mc.parentConstraint ( mc.listRelatives( upArmFkCtrl , p = True ) , upArmGrp , mo = False ))
        mc.delete( mc.parentConstraint ( upArmIkJnt , upArm , mo = False ))
        upArmPosi = mc.getAttr( '%s.t' %upArm )[0]
        upArmOri = mc.getAttr( '%s.r' %upArm )[0]
        mc.setAttr( '%s.t' %upArmFkCtrl , upArmPosi[0] , upArmPosi[1] , upArmPosi[2] )
        mc.setAttr( '%s.r' %upArmFkCtrl , upArmOri[0] , upArmOri[1] , upArmOri[2] )
        
        mc.setAttr( '%s.stretch' %upArmFkCtrl , 0 )
        mc.setAttr( '%s.stretch' %upArmFkCtrl , (( mc.getAttr( '%s.ty' %foreArmIkJnt ) / ( mc.getAttr( '%s.ty' %foreArmFkJnt )) - 1 ) * 10 ))
        
        # foreArm
        mc.delete( mc.parentConstraint ( mc.listRelatives( foreArmFkCtrl , p = True ) , foreArmGrp , mo = False ))
        mc.delete( mc.parentConstraint ( foreArmIkJnt , foreArm , mo = False ))
        foreArmPosi = mc.getAttr( '%s.t' %foreArm )[0]
        foreArmOri = mc.getAttr( '%s.r' %foreArm )[0]
        mc.setAttr( '%s.t' %foreArmFkCtrl , foreArmPosi[0] , foreArmPosi[1] , foreArmPosi[2] )
        mc.setAttr( '%s.r' %foreArmFkCtrl , foreArmOri[0] , foreArmOri[1] , foreArmOri[2] )
        
        mc.setAttr( '%s.stretch' %foreArmFkCtrl , 0 )
        mc.setAttr( '%s.stretch' %foreArmFkCtrl , (( mc.getAttr( '%s.ty' %wristIkJnt ) / ( mc.getAttr( '%s.ty' %wristFkJnt )) - 1 ) * 10 ))
        
        # wrist
        mc.delete( mc.parentConstraint ( mc.listRelatives( wristFkCtrl , p = True ) , wristGrp , mo = False ))
        mc.delete( mc.parentConstraint ( wristIkJnt , wrist , mo = False ))
        wristPosi = mc.getAttr( '%s.t' %wrist )[0]
        wristOri = mc.getAttr( '%s.r' %wrist )[0]
        mc.setAttr( '%s.t' %wristFkCtrl , wristPosi[0] , wristPosi[1] , wristPosi[2] )
        mc.setAttr( '%s.r' %wristFkCtrl , wristOri[0] , wristOri[1] , wristOri[2] )
        
        #-- Switch fk to ik
        mc.setAttr( '%s.fkIk' %arm , 0 )
        
    #-- Cleanup
    mc.delete( upArmGrp , foreArmGrp , wristGrp )


def legMatch( nameSpace = '' ) :
    sel = mc.ls( sl = True )[0]
    
    if 'lft' in sel :
        side = 'lft'
    else :
        side = 'rgt'
    
    name = sel.split( '_' )[0]
    elem = name.split( 'leg' )[-1]
    
    #-- Leg control
    leg = '%sleg%s_%s_ctrl' %( nameSpace , elem , side )
    
    #-- Fk control
    upLegFkCtrl = '%supLeg%sFk_%s_ctrl' %( nameSpace , elem , side )
    lowLegFkCtrl = '%slowLeg%sFk_%s_ctrl' %( nameSpace , elem , side )
    ankleFkCtrl = '%sankle%sFk_%s_ctrl' %( nameSpace , elem , side )
    ballFkCtrl = '%sball%sFk_%s_ctrl' %( nameSpace , elem , side )
    
    #-- Ik control
    upLegIkCtrl = '%supLeg%sIk_%s_ctrl' %( nameSpace , elem , side )
    kneeIkCtrl = '%sknee%sIk_%s_ctrl' %( nameSpace , elem , side )
    ankleIkCtrl = '%sankle%sIk_%s_ctrl' %( nameSpace , elem , side )
    
    #-- Fk joint
    upLegFkJnt = '%supLeg%sFk_%s_jnt' %( nameSpace , elem , side )
    lowLegFkJnt = '%slowLeg%sFk_%s_jnt' %( nameSpace , elem , side )
    ankleFkJnt = '%sankle%sFk_%s_jnt' %( nameSpace , elem , side )
    ballFkJnt = '%sball%sFk_%s_jnt' %( nameSpace , elem , side )
    toeFkJnt = '%stoe%sFk_%s_jnt' %( nameSpace , elem , side )
    
    #-- Ik joint
    upLegIkJnt = '%supLeg%sIk_%s_jnt' %( nameSpace , elem , side )
    lowLegIkJnt = '%slowLeg%sIk_%s_jnt' %( nameSpace , elem , side )
    ankleIkJnt = '%sankle%sIk_%s_jnt' %( nameSpace , elem , side )
    ballIkJnt = '%sball%sIk_%s_jnt' %( nameSpace , elem , side )
    toeIkJnt = '%stoe%sIk_%s_jnt' %( nameSpace , elem , side )
    
    #-- Template position
    upLeg = mc.createNode( 'joint' )
    upLegGrp = mc.group( upLeg )
    lowLeg = mc.createNode( 'joint' )
    lowLegGrp = mc.group( lowLeg )
    ankle = mc.createNode( 'joint' )
    ankleGrp = mc.group( ankle )
    
    #-- Fk to Ik
    if mc.getAttr('%s.fkIk' %sel) == 0 :
        #-- Adjust attribute
        mc.setAttr( '%s.rotateOrder' %upLeg , mc.getAttr( '%s.rotateOrder' %upLegIkCtrl ))
        mc.setAttr( '%s.rotateOrder' %lowLeg , mc.getAttr( '%s.rotateOrder' %kneeIkCtrl ))
        mc.setAttr( '%s.rotateOrder' %ankle , mc.getAttr( '%s.rotateOrder' %ankleIkCtrl ))
        
        upLegJntOri = mc.getAttr( '%s.jointOrient' %upLegIkCtrl )[0]
        mc.setAttr( '%s.jointOrient' %upLeg , upLegJntOri[0] , upLegJntOri[1] , upLegJntOri[2] )
        
        ankleJntOri = mc.getAttr( '%s.jointOrient' %ankleIkCtrl )[0]
        mc.setAttr( '%s.jointOrient' %ankle , ankleJntOri[0] , ankleJntOri[1] , ankleJntOri[2] )
        
        #-- Adjust position
        # upLeg
        mc.delete( mc.parentConstraint ( mc.listRelatives( upLegIkCtrl , p = True ) , upLegGrp , mo = False ))
        mc.delete( mc.parentConstraint ( upLegFkJnt , upLeg , mo = False ))
        upLegPosi = mc.getAttr( '%s.t' %upLeg )[0]
        mc.setAttr( '%s.t' %upLegIkCtrl , upLegPosi[0] , upLegPosi[1] , upLegPosi[2] )
        
        # ankle
        mc.delete( mc.parentConstraint ( mc.listRelatives( ankleIkCtrl , p = True ) , ankleGrp , mo = False ))
        mc.delete( mc.parentConstraint ( ankleFkJnt , ankle , mo = False ))
        anklePosi = mc.getAttr( '%s.t' %ankle )[0]
        ankleOri = mc.getAttr( '%s.r' %ankle )[0]
        mc.setAttr( '%s.t' %ankleIkCtrl , anklePosi[0] , anklePosi[1] , anklePosi[2] )
        mc.setAttr( '%s.r' %ankleIkCtrl , ankleOri[0] , ankleOri[1] , ankleOri[2] )
        
        # knee
        mc.delete( mc.parentConstraint ( mc.listRelatives( kneeIkCtrl , p = True ) , lowLegGrp , mo = False ))
        mc.delete( mc.parentConstraint ( lowLegFkJnt , lowLeg , mo = False ))
        kneePosi = mc.getAttr( '%s.t' %lowLeg )[0]
        mc.setAttr( '%s.t' %kneeIkCtrl , kneePosi[0] , kneePosi[1] , kneePosi[2] )
        
        # ball
        mc.setAttr( '%s.toeBend' %ankleIkCtrl , 0 )
        mc.setAttr( '%s.toeBend' %ankleIkCtrl , -( mc.getAttr( '%s.rx' %ballFkCtrl )) )
        
        '''
        mc.setAttr( '%s.toeSide' %ankleIkCtrl , 0 )
        mc.setAttr( '%s.toeSide' %ankleIkCtrl , ( mc.getAttr( '%s.rz' %ballFkCtrl )) )
        mc.setAttr( '%s.toeSwirl' %ankleIkCtrl , 0 )
        mc.setAttr( '%s.toeSwirl' %ankleIkCtrl , ( mc.getAttr( '%s.ry' %ballFkCtrl )) )
        '''

        #-- Adjust twist and stretch
        mc.setAttr( '%s.twist' %ankleIkCtrl , 0 )
        mc.setAttr( '%s.autoStretch' %ankleIkCtrl , 0 )
        upLegStretch = mc.getAttr( '%s.stretch' %upLegFkCtrl )
        mc.setAttr( '%s.upLegStretch' %ankleIkCtrl , upLegStretch )
        lowLegStretch = mc.getAttr( '%s.stretch' %lowLegFkCtrl )
        mc.setAttr( '%s.lowLegStretch' %ankleIkCtrl , lowLegStretch )

        '''
        toeStretch = mc.getAttr( '%s.stretch' %ballFkCtrl )
        mc.setAttr( '%s.toeStretch' %ankleIkCtrl , toeStretch )
        '''

        #-- Switch fk to ik
        mc.setAttr( '%s.fkIk' %leg , 1 )
    
    #-- Ik to Fk
    elif mc.getAttr('%s.fkIk' %sel) == 1 :
        #-- Adjust attribute
        mc.setAttr( '%s.rotateOrder' %upLeg , mc.getAttr( '%s.rotateOrder' %upLegFkCtrl ))
        mc.setAttr( '%s.rotateOrder' %lowLeg , mc.getAttr( '%s.rotateOrder' %lowLegFkCtrl ))
        mc.setAttr( '%s.rotateOrder' %ankle , mc.getAttr( '%s.rotateOrder' %ankleFkCtrl ))
        
        upLegJntOri = mc.getAttr( '%s.jointOrient' %upLegFkCtrl )[0]
        mc.setAttr( '%s.jointOrient' %upLeg , upLegJntOri[0] , upLegJntOri[1] , upLegJntOri[2] )
        
        lowLegJntOri = mc.getAttr( '%s.jointOrient' %lowLegFkCtrl )[0]
        mc.setAttr( '%s.jointOrient' %lowLeg , lowLegJntOri[0] , lowLegJntOri[1] , lowLegJntOri[2] )
        
        ankleJntOri = mc.getAttr( '%s.jointOrient' %ankleFkCtrl )[0]
        mc.setAttr( '%s.jointOrient' %ankle , ankleJntOri[0] , ankleJntOri[1] , ankleJntOri[2] )
        
        #-- Adjust position
        # upLeg
        mc.delete( mc.parentConstraint ( mc.listRelatives( upLegFkCtrl , p = True ) , upLegGrp , mo = False ))
        mc.delete( mc.parentConstraint ( upLegIkJnt , upLeg , mo = False ))
        upLegPosi = mc.getAttr( '%s.t' %upLeg )[0]
        upLegOri = mc.getAttr( '%s.r' %upLeg )[0]
        mc.setAttr( '%s.t' %upLegFkCtrl , upLegPosi[0] , upLegPosi[1] , upLegPosi[2] )
        mc.setAttr( '%s.r' %upLegFkCtrl , upLegOri[0] , upLegOri[1] , upLegOri[2] )
        
        mc.setAttr( '%s.stretch' %upLegFkCtrl , 0 )
        mc.setAttr( '%s.stretch' %upLegFkCtrl , (( mc.getAttr( '%s.ty' %lowLegIkJnt ) / ( mc.getAttr( '%s.ty' %lowLegFkJnt )) - 1 ) * 10 ))
        
        # lowLeg
        mc.delete( mc.parentConstraint ( mc.listRelatives( lowLegFkCtrl , p = True ) , lowLegGrp , mo = False ))
        mc.delete( mc.parentConstraint ( lowLegIkJnt , lowLeg , mo = False ))
        lowLegPosi = mc.getAttr( '%s.t' %lowLeg )[0]
        lowLegOri = mc.getAttr( '%s.r' %lowLeg )[0]
        mc.setAttr( '%s.t' %lowLegFkCtrl , lowLegPosi[0] , lowLegPosi[1] , lowLegPosi[2] )
        mc.setAttr( '%s.r' %lowLegFkCtrl , lowLegOri[0] , lowLegOri[1] , lowLegOri[2] )
        
        mc.setAttr( '%s.stretch' %lowLegFkCtrl , 0 )
        mc.setAttr( '%s.stretch' %lowLegFkCtrl , (( mc.getAttr( '%s.ty' %ankleIkJnt ) / ( mc.getAttr( '%s.ty' %ankleFkJnt )) - 1 ) * 10 ))
        
        # ankle
        mc.delete( mc.parentConstraint ( mc.listRelatives( ankleFkCtrl , p = True ) , ankleGrp , mo = False ))
        mc.delete( mc.parentConstraint ( ankleIkJnt , ankle , mo = False ))
        anklePosi = mc.getAttr( '%s.t' %ankle )[0]
        ankleOri = mc.getAttr( '%s.r' %ankle )[0]
        mc.setAttr( '%s.t' %ankleFkCtrl , anklePosi[0] , anklePosi[1] , anklePosi[2] )
        mc.setAttr( '%s.r' %ankleFkCtrl , ankleOri[0] , ankleOri[1] , ankleOri[2] )
        
        # ball
        mc.setAttr( '%s.r' %ballFkCtrl , 0 , 0 , 0 )
        mc.setAttr( '%s.rx' %ballFkCtrl , -( mc.getAttr( '%s.toeBend' %ankleIkCtrl )) )

        '''
        mc.setAttr( '%s.rz' %ballFkCtrl , ( mc.getAttr( '%s.toeSide' %ankleIkCtrl )) )
        mc.setAttr( '%s.ry' %ballFkCtrl , ( mc.getAttr( '%s.toeSwirl' %ankleIkCtrl )) )
        
        toeStretch = mc.getAttr( '%s.toeStretch' %ankleIkCtrl )
        mc.setAttr( '%s.stretch' %ballFkCtrl , toeStretch )
        '''
        
        #-- Switch fk to ik
        mc.setAttr( '%s.fkIk' %leg , 0 )
        
    #-- Cleanup
    mc.delete( upLegGrp , lowLegGrp , ankleGrp )

match()