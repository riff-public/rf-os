import maya.cmds as mc
import maya.mel as mm



def importRef() :
    sel = mc.ls( sl = True )
    refNd = mc.ls( sel , rn = True )
    
    if sel == [] :
        mc.warning ( 'Please Select Object.' )
    else :
        if not refNd == [] :
            for each in sel :
                nameSpace = each.split(':')[0]
                reffile = mc.file ( q = True , r = True )
                
                for ref in reffile :
                    refNameSpace = mc.referenceQuery ( ref , referenceNode = True ).replace ( "RN" , "" )
                    
                    if nameSpace == refNameSpace :
                        choice = mc.confirmDialog( t = 'Import Reference' , m = 'Do you want to Import Reference. Are you sure ?' , b = [ 'Yes' , 'No' ] , db = 'Yes' , cb = 'No' , ds = 'No' )
                        
                        if choice == 'Yes' :
                            mc.file ( ref , ir = True )
                            print '\n## %sRN has been imported.\n' %refNameSpace
                        else :
                            pass
        else :
            mc.warning ( 'Not Reference.' )


def removeRef() :
    sel = mc.ls ( sl = True )
    refNd = mc.ls( sel , rn = True )
    
    if sel == [] :
        mc.warning ( 'Please Select Object.' )
    else :
        if not refNd == [] :
            for each in sel :
                nameSpace = each.split(':')[0]
                reffile = mc.file ( q = True , r = True )
                
                for ref in reffile :
                    refNameSpace = mc.referenceQuery ( ref , referenceNode = True ).replace ( "RN" , "" )
                    
                    if nameSpace == refNameSpace :
                        choice = mc.confirmDialog( t = 'Remove Reference' , m = 'Do you want to Remove Reference. Are you sure ?' , b = [ 'Yes' , 'No' ] , db = 'Yes' , cb = 'No' , ds = 'No' )
                        
                        if choice == 'Yes' :
                            mc.file ( ref , rr = True )
                            print '\n## %sRN has been removed.\n' %refNameSpace
                        else :
                            pass
        else :
            mc.warning ( 'Not Reference.' )


def reloadRef() :
    sel = mc.ls ( sl = True )
    refNd = mc.ls( sel , rn = True )
    
    if sel == [] :
        mc.warning ( 'Please Select Object.' )
    else :
        if not refNd == [] :
            for each in sel :
                nameSpace = each.split(':')[0]
                reffile = mc.file ( q = True , r = True )
                
                for ref in reffile :
                    refNameSpace = mc.referenceQuery ( ref , referenceNode = True ).replace ( "RN" , "" )
                    
                    if nameSpace == refNameSpace :
                        choice = mc.confirmDialog( t = 'Reload Reference' , m = 'Do you want to Reload Reference. Are you sure ?' , b = [ 'Yes' , 'No' ] , db = 'Yes' , cb = 'No' , ds = 'No' )
                        
                        if choice == 'Yes' :
                            mc.file ( lr = '%sRN' %nameSpace )
                            print '\n## %sRN has been reload.\n' %refNameSpace
                        else :
                            pass
        else :
            mc.warning ( 'Not Reference.' )


def replaceRef() :
    sel = mc.ls ( sl = True )
    
    if sel == [] :
        mc.warning ( 'Please Select Object.' )
    else :
        refNd = mc.ls( sel[0] , rn = True )
        if not refNd == [] :
            nameSpace = sel[0].split(':')[0]
            reffile = mc.file ( q = True , r = True )
            
            for ref in reffile :
                refNameSpace = mc.referenceQuery ( ref , referenceNode = True ).replace ( "RN" , "" )
                
                if nameSpace == refNameSpace :
                    fileName = mc.fileDialog( t = 'Browser Reference ' )
                    array = len(fileName)
                    
                    if array == 0 :
                        pass
                    else :
                        choice = mc.confirmDialog( t = 'Replace Reference' , m = 'Do you want to Replace Reference. Are you sure ?' , b = [ 'Yes' , 'No' ] , db = 'Yes' , cb = 'No' , ds = 'No' )
                        
                        if choice == 'Yes' :
                            mc.file( fileName , lr = '%sRN' %nameSpace , type = 'mayaAscii' , options = 'v=0' )
                            print '\n## %sRN has been replace.\n' %refNameSpace
                        else :
                            pass
        else :
            mc.warning ( 'Not Reference.' )