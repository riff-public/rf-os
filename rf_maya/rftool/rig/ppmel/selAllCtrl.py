import maya.mel as mm
import maya.cmds as mc

def selAllCtrl() :
    sel = mc.ls( sl = True )
    
    if ':' in sel[0] :
        nameSpaceDict = []
        
        for each in sel :
            ctrl = each.split(':')[-1]
            nameSpace = each.split(ctrl)[0]
            nameSpaceDict.append(nameSpace)
        
        for name in nameSpaceDict :
            try :
                mc.select( '%s*_Ctrl' %name , add = True )
            except : pass
            
            try :
                mc.select( '%s*_ctrl' %name , add = True )
            except : pass
    
    else :
        mc.select( '*_Ctrl' , add = True )
        mc.select( '*_ctrl' , add = True )
            
def run() :
    selAllCtrl()