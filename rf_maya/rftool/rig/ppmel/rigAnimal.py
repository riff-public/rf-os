import maya.cmds as mc
import maya.mel as mm
import ppmel.core as core
import ppmel.rigMain as main
import ppmel.rigRoot as root
import ppmel.rigSpineAnimal as spineAnimal
import ppmel.rigPelvis as pelvis
import ppmel.rigNeck as neck
import ppmel.rigHead as head
import ppmel.rigClavicle as clavicle
import ppmel.rigArm as arm
import ppmel.rigLeg as leg
import ppmel.rigLegAnimal as legAnimal
import ppmel.rigFinger as finger
import ppmel.rigFingerAttr as fingerAttr
import ppmel.rigFk as fk
reload( core )
reload( root )
reload( spineAnimal )
reload( pelvis )
reload( neck )
reload( head )
reload( clavicle )
reload( arm )
reload( leg )
reload( legAnimal )
reload( finger )
reload( fingerAttr )
reload( fk )


def rigAnimal( animGrp = 'anim_grp' ,
               skinGrp = 'skin_grp' ,
               jntGrp = 'jnt_grp' ,
               ikhGrp = 'ikh_grp' ,
               stillGrp = 'still_grp' ,
               radius = 0.5 ,
               size = 1 ) :
    
    #-- Main
    main.rigMain( charSize = size )
    
    
    #-- Root
    root.rigRoot(
                    rootTmpJnt = 'root_tmpJnt' ,
                    animGrp = animGrp ,
                    skinGrp = skinGrp ,
                    charSize = size
                 )
    
    
    #-- Spine
    spineAnimal.rigSpineAnimal(
                                  spine1TmpJnt = 'spine1_tmpJnt' ,
                                  spine2TmpJnt = 'spine2_tmpJnt' ,
                                  neckTmpJnt = 'neck_tmpJnt' ,
                                  parent = 'root_jnt' ,
                                  animGrp = 'anim_grp' ,
                                  skinGrp = 'skin_grp' ,
                                  jntGrp = 'jnt_grp' ,
                                  stillGrp = 'still_grp' ,
                                  ikhGrp = 'ikh_grp' ,
                                  spineFk = True ,
                                  spineIk = True ,
                                  ribbon = True ,
                                  rotateOrder = 'yzx' ,
                                  axis = 'z' ,
                                  charSize = size
                               )
    
    
    #-- Pelvis
    pelvis.rigPelvis(
                        pelvisTmpJnt = 'pelvis_tmpJnt' ,
                        parent = 'root_jnt' ,
                        animGrp = animGrp ,
                        skinGrp = skinGrp ,
                        charSize = size
                     )
    
    
    #-- Neck
    neck.rigNeck(
                    neck1TmpJnt = 'neck_tmpJnt' ,
                    neck2TmpJnt = 'head1_tmpJnt' ,
                    parent = 'spine3Pos_jnt' ,
                    animGrp = animGrp ,
                    skinGrp = skinGrp ,
                    jntGrp = jntGrp ,
                    ribbon = True ,
                    rotateOrder = 'xzy' ,
                    axis = 'y' ,
                    charSize = size
                )
    
    
    #-- Head
    head.rigHead(
                    head1TmpJnt = 'head1_tmpJnt' ,
                    head2TmpJnt = 'head2_tmpJnt' ,
                    eyeTrgTmpJnt = 'eyeTrgt_tmpJnt' ,
                    eyeLftTrgTmpJnt = 'eyeTrgt_lft_tmpJnt' ,
                    eyeRgtTrgTmpJnt = 'eyeTrgt_rgt_tmpJnt' ,
                    eyeLftTmpJnt = 'eye_lft_tmpJnt' ,
                    eyeRgtTmpJnt = 'eye_rgt_tmpJnt' ,
                    jaw1UprTmpJnt = 'jaw1_upr_tmpJnt' ,
                    jaw2UprTmpJnt = 'jaw2_upr_tmpJnt' ,
                    jaw1LwrTmpJnt = 'jaw1_lwr_tmpJnt' ,
                    jaw2LwrTmpJnt = 'jaw2_lwr_tmpJnt' ,
                    jaw3LwrTmpJnt = 'jaw3_lwr_tmpJnt' ,
                    parent = 'neck2_jnt' ,
                    animGrp = animGrp ,
                    skinGrp = skinGrp ,
                    jntGrp = jntGrp ,
                    eyeRig = True ,
                    jawRig = True ,
                    charSize = size
                )
    
    
    #-- Ear
    # Left
    fk.rigFk(
                name = 'ear' ,
                tmpJnt = [ 'ear1_lft_tmpJnt' , 'ear2_lft_tmpJnt' , 'ear3_lft_tmpJnt' , 'ear4_lft_tmpJnt' ] ,
                parent = 'head2_jnt' ,
                animGrp = 'anim_grp' ,
                axis = 'y' ,
                shape = 'square' ,
                color = 'red' ,
                side = 'lft'
            )
    
    # Right
    fk.rigFk(
                name = 'ear' ,
                tmpJnt = [ 'ear1_rgt_tmpJnt' , 'ear2_rgt_tmpJnt' , 'ear3_rgt_tmpJnt' , 'ear4_rgt_tmpJnt' ] ,
                parent = 'head2_jnt' ,
                animGrp = 'anim_grp' ,
                axis = 'y' ,
                shape = 'square' ,
                color = 'red' ,
                side = 'rgt'
            )
    
    
    #-- Tongue
    fk.rigFk(
                name = 'toungue' ,
                tmpJnt = [ 'tongue1_tmpJnt' , 'tongue2_tmpJnt' , 'tongue3_tmpJnt' , 
                           'tongue4_tmpJnt' , 'tongue5_tmpJnt' , 'tongue6_tmpJnt' ] ,
                parent = 'jaw_lwr_jnt' ,
                animGrp = 'anim_grp' ,
                axis = 'y' ,
                shape = 'square' ,
                color = 'blue' ,
                side = ''
            )
    
    
    #-- Clavicle front
    # Left
    clavicle.rigClavicle(
                            clav1TmpJnt = 'clavFront_lft_tmpJnt' ,
                            clav2TmpJnt = 'upLegFront_lft_tmpJnt' ,
                            parent = 'spine2_jnt' ,
                            animGrp = animGrp ,
                            skinGrp = skinGrp ,
                            jntGrp = jntGrp ,
                            stillGrp = stillGrp ,
                            side = 'lft' ,
                            val = 0.1 ,
                            elem = 'front' ,
                            charSize = size 
                        )
    
    # Right
    clavicle.rigClavicle(
                            clav1TmpJnt = 'clavFront_rgt_tmpJnt' ,
                            clav2TmpJnt = 'upLegFront_rgt_tmpJnt' ,
                            parent = 'spine2_jnt' ,
                            animGrp = animGrp ,
                            skinGrp = skinGrp ,
                            jntGrp = jntGrp ,
                            stillGrp = stillGrp ,
                            side = 'rgt' ,
                            val = -0.1 ,
                            elem = 'front' ,
                            charSize = size 
                        )
    
    
    #-- Clavicle back
    # Left
    clavicle.rigClavicle(
                            clav1TmpJnt = 'clavBack_lft_tmpJnt' ,
                            clav2TmpJnt = 'upLegBack_lft_tmpJnt' ,
                            parent = 'pelvis_jnt' ,
                            animGrp = animGrp ,
                            skinGrp = skinGrp ,
                            jntGrp = jntGrp ,
                            stillGrp = stillGrp ,
                            side = 'lft' ,
                            val = 0.1 ,
                            elem = 'back' ,
                            charSize = size 
                        )
    
    # Right
    clavicle.rigClavicle(
                            clav1TmpJnt = 'clavBack_rgt_tmpJnt' ,
                            clav2TmpJnt = 'upLegBack_rgt_tmpJnt' ,
                            parent = 'pelvis_jnt' ,
                            animGrp = animGrp ,
                            skinGrp = skinGrp ,
                            jntGrp = jntGrp ,
                            stillGrp = stillGrp ,
                            side = 'rgt' ,
                            val = -0.1 ,
                            elem = 'back' ,
                            charSize = size 
                        )
    
    
    #-- Leg front
    # Left
    leg.rigLeg(
                    upLegTmpJnt = 'upLegFront_lft_tmpJnt' ,
                    lowLegTmpJnt = 'lowLegFront_lft_tmpJnt' ,
                    ankleTmpJnt = 'ankleFront_lft_tmpJnt' ,
                    ballTmpJnt = 'ballFront_lft_tmpJnt' ,
                    toeTmpJnt = 'toeFront_lft_tmpJnt' ,
                    heelTmpJnt = 'heelFront_lft_tmpJnt' ,
                    footInTmpJnt = 'footInFront_lft_tmpJnt' ,
                    footOutTmpJnt = 'footOutFront_lft_tmpJnt' ,
                    kneeTmpJnt = 'kneeFront_lft_tmpJnt' ,
                    parent = 'clav2Front_lft_jnt' ,
                    animGrp = animGrp ,
                    skinGrp = skinGrp ,
                    jntGrp = jntGrp ,
                    stillGrp = stillGrp ,
                    ikhGrp = ikhGrp ,
                    side = 'lft' ,
                    val = 0.1 ,
                    elem = 'front' ,
                    foot = True ,
                    legFk = True ,
                    legIk = True ,
                    ribbon = True ,
                    charSize = size
                )
    
    # Right
    leg.rigLeg(
                    upLegTmpJnt = 'upLegFront_rgt_tmpJnt' ,
                    lowLegTmpJnt = 'lowLegFront_rgt_tmpJnt' ,
                    ankleTmpJnt = 'ankleFront_rgt_tmpJnt' ,
                    ballTmpJnt = 'ballFront_rgt_tmpJnt' ,
                    toeTmpJnt = 'toeFront_rgt_tmpJnt' ,
                    heelTmpJnt = 'heelFront_rgt_tmpJnt' ,
                    footInTmpJnt = 'footInFront_rgt_tmpJnt' ,
                    footOutTmpJnt = 'footOutFront_rgt_tmpJnt' ,
                    kneeTmpJnt = 'kneeFront_rgt_tmpJnt' ,
                    parent = 'clav2Front_rgt_jnt' ,
                    animGrp = animGrp ,
                    skinGrp = skinGrp ,
                    jntGrp = jntGrp ,
                    stillGrp = stillGrp ,
                    ikhGrp = ikhGrp ,
                    side = 'rgt' ,
                    val = -0.1 ,
                    elem = 'front' ,
                    foot = True ,
                    legFk = True ,
                    legIk = True ,
                    ribbon = True ,
                    charSize = size
                )
    
    
    #-- Leg back
    # Left
    legAnimal.rigLegAnimal( 
                                upLegTmpJnt = 'upLegBack_lft_tmpJnt' ,
                                midLegTmpJnt = 'midLegBack_lft_tmpJnt' ,
                                lowLegTmpJnt = 'lowLegBack_lft_tmpJnt' ,
                                ankleTmpJnt = 'ankleBack_lft_tmpJnt' ,
                                ballTmpJnt = 'ballBack_lft_tmpJnt' ,
                                toeTmpJnt = 'toeBack_lft_tmpJnt' ,
                                kneeTmpJnt = 'kneeBack_lft_tmpJnt' ,
                                heelTmpJnt = 'heelBack_lft_tmpJnt' ,
                                footInTmpJnt = 'footInBack_lft_tmpJnt' ,
                                footOutTmpJnt = 'footOutBack_lft_tmpJnt' ,
                                parent = 'clav2Back_lft_jnt' ,
                                animGrp = 'anim_grp' ,
                                skinGrp = 'skin_grp' ,
                                jntGrp = 'jnt_grp' ,
                                stillGrp = 'still_grp' ,
                                ikhGrp = 'ikh_grp' ,
                                side = 'lft' ,
                                val = 0.1 ,
                                elem = 'back' ,
                                foot = True ,
                                legFk = True ,
                                legIk = True ,
                                ribbon = True ,
                                charSize = size
                            )
                            
    # Right
    legAnimal.rigLegAnimal( 
                                upLegTmpJnt = 'upLegBack_rgt_tmpJnt' ,
                                midLegTmpJnt = 'midLegBack_rgt_tmpJnt' ,
                                lowLegTmpJnt = 'lowLegBack_rgt_tmpJnt' ,
                                ankleTmpJnt = 'ankleBack_rgt_tmpJnt' ,
                                ballTmpJnt = 'ballBack_rgt_tmpJnt' ,
                                toeTmpJnt = 'toeBack_rgt_tmpJnt' ,
                                kneeTmpJnt = 'kneeBack_rgt_tmpJnt' ,
                                heelTmpJnt = 'heelBack_rgt_tmpJnt' ,
                                footInTmpJnt = 'footInBack_rgt_tmpJnt' ,
                                footOutTmpJnt = 'footOutBack_rgt_tmpJnt' ,
                                parent = 'clav2Back_rgt_jnt' ,
                                animGrp = 'anim_grp' ,
                                skinGrp = 'skin_grp' ,
                                jntGrp = 'jnt_grp' ,
                                stillGrp = 'still_grp' ,
                                ikhGrp = 'ikh_grp' ,
                                side = 'rgt' ,
                                val = 0.1 ,
                                elem = 'back' ,
                                foot = True ,
                                legFk = True ,
                                legIk = True ,
                                ribbon = True ,
                                charSize = size
                            )
                            
    
    #-- Tail
    fk.rigFk(
                name = 'tail' ,
                tmpJnt = [ 'tail1_tmpJnt' , 'tail2_tmpJnt' , 'tail3_tmpJnt' , 
                           'tail4_tmpJnt' , 'tail5_tmpJnt' , 'tail6_tmpJnt' ] ,
                parent = 'pelvis_jnt' ,
                animGrp = 'anim_grp' ,
                axis = 'y' ,
                shape = 'circle' ,
                color = 'red' ,
                side = ''
            )
            
                            
    #-- Finger Front Foot
    # Left
    # thumb
    finger.rigFinger(
                       fngr1TmpJnt = 'thumbFrontFoot1_lft_tmpJnt' ,
                       fngr2TmpJnt = 'thumbFrontFoot2_lft_tmpJnt' ,
                       fngr3TmpJnt = 'thumbFrontFoot3_lft_tmpJnt' ,
                       fngr4TmpJnt = 'thumbFrontFoot4_lft_tmpJnt' ,
                       fngr5TmpJnt = '' ,
                       parent = 'toeFront_lft_jnt' ,
                       animGrp = 'anim_grp' ,
                       skinGrp = 'skin_grp' ,
                       jntGrp = 'jnt_grp' ,
                       stillGrp = 'still_grp' ,
                       side = 'lft' ,
                       finger = 'thumb' ,
                       type = 'foot' ,
                       elem = 'Front' ,
                       val = 0.1 ,
                       charSize = size 
                     )
    
    # index
    finger.rigFinger(
                       fngr1TmpJnt = 'indexFrontFoot1_lft_tmpJnt' ,
                       fngr2TmpJnt = 'indexFrontFoot2_lft_tmpJnt' ,
                       fngr3TmpJnt = 'indexFrontFoot3_lft_tmpJnt' ,
                       fngr4TmpJnt = 'indexFrontFoot4_lft_tmpJnt' ,
                       fngr5TmpJnt = '' ,
                       parent = 'toeFront_lft_jnt' ,
                       animGrp = 'anim_grp' ,
                       skinGrp = 'skin_grp' ,
                       jntGrp = 'jnt_grp' ,
                       stillGrp = 'still_grp' ,
                       side = 'lft' ,
                       finger = 'index' ,
                       type = 'foot' ,
                       elem = 'Front' ,
                       val = 0.1 ,
                       charSize = size 
                     )
    
    # middle
    finger.rigFinger(
                       fngr1TmpJnt = 'middleFrontFoot1_lft_tmpJnt' ,
                       fngr2TmpJnt = 'middleFrontFoot2_lft_tmpJnt' ,
                       fngr3TmpJnt = 'middleFrontFoot3_lft_tmpJnt' ,
                       fngr4TmpJnt = 'middleFrontFoot4_lft_tmpJnt' ,
                       fngr5TmpJnt = '' ,
                       parent = 'toeFront_lft_jnt' ,
                       animGrp = 'anim_grp' ,
                       skinGrp = 'skin_grp' ,
                       jntGrp = 'jnt_grp' ,
                       stillGrp = 'still_grp' ,
                       side = 'lft' ,
                       finger = 'middle' ,
                       type = 'foot' ,
                       elem = 'Front' ,
                       val = 0.1 ,
                       charSize = size 
                     )
    
    # ring
    finger.rigFinger(
                       fngr1TmpJnt = 'ringFrontFoot1_lft_tmpJnt' ,
                       fngr2TmpJnt = 'ringFrontFoot2_lft_tmpJnt' ,
                       fngr3TmpJnt = 'ringFrontFoot3_lft_tmpJnt' ,
                       fngr4TmpJnt = 'ringFrontFoot4_lft_tmpJnt' ,
                       fngr5TmpJnt = '' ,
                       parent = 'toeFront_lft_jnt' ,
                       animGrp = 'anim_grp' ,
                       skinGrp = 'skin_grp' ,
                       jntGrp = 'jnt_grp' ,
                       stillGrp = 'still_grp' ,
                       side = 'lft' ,
                       finger = 'ring' ,
                       type = 'foot' ,
                       elem = 'Front' ,
                       val = 0.1 ,
                       charSize = size 
                     )
    
    # pinky
    finger.rigFinger(
                       fngr1TmpJnt = 'pinkyFrontFoot1_lft_tmpJnt' ,
                       fngr2TmpJnt = 'pinkyFrontFoot2_lft_tmpJnt' ,
                       fngr3TmpJnt = 'pinkyFrontFoot3_lft_tmpJnt' ,
                       fngr4TmpJnt = 'pinkyFrontFoot4_lft_tmpJnt' ,
                       fngr5TmpJnt = '' ,
                       parent = 'toeFront_lft_jnt' ,
                       animGrp = 'anim_grp' ,
                       skinGrp = 'skin_grp' ,
                       jntGrp = 'jnt_grp' ,
                       stillGrp = 'still_grp' ,
                       side = 'lft' ,
                       finger = 'pinky' ,
                       type = 'foot' ,
                       elem = 'Front' ,
                       val = 0.1 ,
                       charSize = size 
                     )
                     
    # Right
    # thumb
    finger.rigFinger(
                       fngr1TmpJnt = 'thumbFrontFoot1_rgt_tmpJnt' ,
                       fngr2TmpJnt = 'thumbFrontFoot2_rgt_tmpJnt' ,
                       fngr3TmpJnt = 'thumbFrontFoot3_rgt_tmpJnt' ,
                       fngr4TmpJnt = 'thumbFrontFoot4_rgt_tmpJnt' ,
                       fngr5TmpJnt = '' ,
                       parent = 'toeFront_rgt_jnt' ,
                       animGrp = 'anim_grp' ,
                       skinGrp = 'skin_grp' ,
                       jntGrp = 'jnt_grp' ,
                       stillGrp = 'still_grp' ,
                       side = 'rgt' ,
                       finger = 'thumb' ,
                       type = 'foot' ,
                       elem = 'Front' ,
                       val = 0.1 ,
                       charSize = size 
                     )
    
    # index
    finger.rigFinger(
                       fngr1TmpJnt = 'indexFrontFoot1_rgt_tmpJnt' ,
                       fngr2TmpJnt = 'indexFrontFoot2_rgt_tmpJnt' ,
                       fngr3TmpJnt = 'indexFrontFoot3_rgt_tmpJnt' ,
                       fngr4TmpJnt = 'indexFrontFoot4_rgt_tmpJnt' ,
                       fngr5TmpJnt = '' ,
                       parent = 'toeFront_rgt_jnt' ,
                       animGrp = 'anim_grp' ,
                       skinGrp = 'skin_grp' ,
                       jntGrp = 'jnt_grp' ,
                       stillGrp = 'still_grp' ,
                       side = 'rgt' ,
                       finger = 'index' ,
                       type = 'foot' ,
                       elem = 'Front' ,
                       val = 0.1 ,
                       charSize = size 
                     )
    
    # middle
    finger.rigFinger(
                       fngr1TmpJnt = 'middleFrontFoot1_rgt_tmpJnt' ,
                       fngr2TmpJnt = 'middleFrontFoot2_rgt_tmpJnt' ,
                       fngr3TmpJnt = 'middleFrontFoot3_rgt_tmpJnt' ,
                       fngr4TmpJnt = 'middleFrontFoot4_rgt_tmpJnt' ,
                       fngr5TmpJnt = '' ,
                       parent = 'toeFront_rgt_jnt' ,
                       animGrp = 'anim_grp' ,
                       skinGrp = 'skin_grp' ,
                       jntGrp = 'jnt_grp' ,
                       stillGrp = 'still_grp' ,
                       side = 'rgt' ,
                       finger = 'middle' ,
                       type = 'foot' ,
                       elem = 'Front' ,
                       val = 0.1 ,
                       charSize = size 
                     )
    
    # ring
    finger.rigFinger(
                       fngr1TmpJnt = 'ringFrontFoot1_rgt_tmpJnt' ,
                       fngr2TmpJnt = 'ringFrontFoot2_rgt_tmpJnt' ,
                       fngr3TmpJnt = 'ringFrontFoot3_rgt_tmpJnt' ,
                       fngr4TmpJnt = 'ringFrontFoot4_rgt_tmpJnt' ,
                       fngr5TmpJnt = '' ,
                       parent = 'toeFront_rgt_jnt' ,
                       animGrp = 'anim_grp' ,
                       skinGrp = 'skin_grp' ,
                       jntGrp = 'jnt_grp' ,
                       stillGrp = 'still_grp' ,
                       side = 'rgt' ,
                       finger = 'ring' ,
                       type = 'foot' ,
                       elem = 'Front' ,
                       val = 0.1 ,
                       charSize = size 
                     )
    
    # pinky
    finger.rigFinger(
                       fngr1TmpJnt = 'pinkyFrontFoot1_rgt_tmpJnt' ,
                       fngr2TmpJnt = 'pinkyFrontFoot2_rgt_tmpJnt' ,
                       fngr3TmpJnt = 'pinkyFrontFoot3_rgt_tmpJnt' ,
                       fngr4TmpJnt = 'pinkyFrontFoot4_rgt_tmpJnt' ,
                       fngr5TmpJnt = '' ,
                       parent = 'toeFront_rgt_jnt' ,
                       animGrp = 'anim_grp' ,
                       skinGrp = 'skin_grp' ,
                       jntGrp = 'jnt_grp' ,
                       stillGrp = 'still_grp' ,
                       side = 'rgt' ,
                       finger = 'pinky' ,
                       type = 'foot' ,
                       elem = 'Front' ,
                       val = 0.1 ,
                       charSize = size 
                     )
    
    
    #-- Finger Foot Attribute
    # Left
    fingerAttr.rigFingerAttr(  
                               ctrl = 'legFront_lft_ctrl' ,
                               side = 'lft' ,
                               type = 'foot' ,
                               elem = 'Front' ,
                               num = 4 ,
                               finger = ( 'thumb' , 'index' , 'middle' , 'ring' , 'pinky' )
                            )
    
    # Right
    fingerAttr.rigFingerAttr(  
                               ctrl = 'legFront_rgt_ctrl' ,
                               side = 'rgt' ,
                               type = 'foot' ,
                               elem = 'Front' ,
                               num = 4 ,
                               finger = ( 'thumb' , 'index' , 'middle' , 'ring' , 'pinky' )
                            )
                            
    #-- Finger Back Foot
    # Left
    # thumb
    finger.rigFinger(
                       fngr1TmpJnt = 'thumbBackFoot1_lft_tmpJnt' ,
                       fngr2TmpJnt = 'thumbBackFoot2_lft_tmpJnt' ,
                       fngr3TmpJnt = 'thumbBackFoot3_lft_tmpJnt' ,
                       fngr4TmpJnt = 'thumbBackFoot4_lft_tmpJnt' ,
                       fngr5TmpJnt = '' ,
                       parent = 'toeBack_lft_jnt' ,
                       animGrp = 'anim_grp' ,
                       skinGrp = 'skin_grp' ,
                       jntGrp = 'jnt_grp' ,
                       stillGrp = 'still_grp' ,
                       side = 'lft' ,
                       finger = 'thumb' ,
                       type = 'foot' ,
                       elem = 'Back' ,
                       val = 0.1 ,
                       charSize = size 
                     )
    
    # index
    finger.rigFinger(
                       fngr1TmpJnt = 'indexBackFoot1_lft_tmpJnt' ,
                       fngr2TmpJnt = 'indexBackFoot2_lft_tmpJnt' ,
                       fngr3TmpJnt = 'indexBackFoot3_lft_tmpJnt' ,
                       fngr4TmpJnt = 'indexBackFoot4_lft_tmpJnt' ,
                       fngr5TmpJnt = '' ,
                       parent = 'toeBack_lft_jnt' ,
                       animGrp = 'anim_grp' ,
                       skinGrp = 'skin_grp' ,
                       jntGrp = 'jnt_grp' ,
                       stillGrp = 'still_grp' ,
                       side = 'lft' ,
                       finger = 'index' ,
                       type = 'foot' ,
                       elem = 'Back' ,
                       val = 0.1 ,
                       charSize = size 
                     )
    
    # middle
    finger.rigFinger(
                       fngr1TmpJnt = 'middleBackFoot1_lft_tmpJnt' ,
                       fngr2TmpJnt = 'middleBackFoot2_lft_tmpJnt' ,
                       fngr3TmpJnt = 'middleBackFoot3_lft_tmpJnt' ,
                       fngr4TmpJnt = 'middleBackFoot4_lft_tmpJnt' ,
                       fngr5TmpJnt = '' ,
                       parent = 'toeBack_lft_jnt' ,
                       animGrp = 'anim_grp' ,
                       skinGrp = 'skin_grp' ,
                       jntGrp = 'jnt_grp' ,
                       stillGrp = 'still_grp' ,
                       side = 'lft' ,
                       finger = 'middle' ,
                       type = 'foot' ,
                       elem = 'Back' ,
                       val = 0.1 ,
                       charSize = size 
                     )
    
    # ring
    finger.rigFinger(
                       fngr1TmpJnt = 'ringBackFoot1_lft_tmpJnt' ,
                       fngr2TmpJnt = 'ringBackFoot2_lft_tmpJnt' ,
                       fngr3TmpJnt = 'ringBackFoot3_lft_tmpJnt' ,
                       fngr4TmpJnt = 'ringBackFoot4_lft_tmpJnt' ,
                       fngr5TmpJnt = '' ,
                       parent = 'toeBack_lft_jnt' ,
                       animGrp = 'anim_grp' ,
                       skinGrp = 'skin_grp' ,
                       jntGrp = 'jnt_grp' ,
                       stillGrp = 'still_grp' ,
                       side = 'lft' ,
                       finger = 'ring' ,
                       type = 'foot' ,
                       elem = 'Back' ,
                       val = 0.1 ,
                       charSize = size 
                     )
    
    # pinky
    finger.rigFinger(
                       fngr1TmpJnt = 'pinkyBackFoot1_lft_tmpJnt' ,
                       fngr2TmpJnt = 'pinkyBackFoot2_lft_tmpJnt' ,
                       fngr3TmpJnt = 'pinkyBackFoot3_lft_tmpJnt' ,
                       fngr4TmpJnt = 'pinkyBackFoot4_lft_tmpJnt' ,
                       fngr5TmpJnt = '' ,
                       parent = 'toeBack_lft_jnt' ,
                       animGrp = 'anim_grp' ,
                       skinGrp = 'skin_grp' ,
                       jntGrp = 'jnt_grp' ,
                       stillGrp = 'still_grp' ,
                       side = 'lft' ,
                       finger = 'pinky' ,
                       type = 'foot' ,
                       elem = 'Back' ,
                       val = 0.1 ,
                       charSize = size 
                     )
                     
    # Right
    # thumb
    finger.rigFinger(
                       fngr1TmpJnt = 'thumbBackFoot1_rgt_tmpJnt' ,
                       fngr2TmpJnt = 'thumbBackFoot2_rgt_tmpJnt' ,
                       fngr3TmpJnt = 'thumbBackFoot3_rgt_tmpJnt' ,
                       fngr4TmpJnt = 'thumbBackFoot4_rgt_tmpJnt' ,
                       fngr5TmpJnt = '' ,
                       parent = 'toeBack_rgt_jnt' ,
                       animGrp = 'anim_grp' ,
                       skinGrp = 'skin_grp' ,
                       jntGrp = 'jnt_grp' ,
                       stillGrp = 'still_grp' ,
                       side = 'rgt' ,
                       finger = 'thumb' ,
                       type = 'foot' ,
                       elem = 'Back' ,
                       val = 0.1 ,
                       charSize = size 
                     )
    
    # index
    finger.rigFinger(
                       fngr1TmpJnt = 'indexBackFoot1_rgt_tmpJnt' ,
                       fngr2TmpJnt = 'indexBackFoot2_rgt_tmpJnt' ,
                       fngr3TmpJnt = 'indexBackFoot3_rgt_tmpJnt' ,
                       fngr4TmpJnt = 'indexBackFoot4_rgt_tmpJnt' ,
                       fngr5TmpJnt = '' ,
                       parent = 'toeBack_rgt_jnt' ,
                       animGrp = 'anim_grp' ,
                       skinGrp = 'skin_grp' ,
                       jntGrp = 'jnt_grp' ,
                       stillGrp = 'still_grp' ,
                       side = 'rgt' ,
                       finger = 'index' ,
                       type = 'foot' ,
                       elem = 'Back' ,
                       val = 0.1 ,
                       charSize = size 
                     )
    
    # middle
    finger.rigFinger(
                       fngr1TmpJnt = 'middleBackFoot1_rgt_tmpJnt' ,
                       fngr2TmpJnt = 'middleBackFoot2_rgt_tmpJnt' ,
                       fngr3TmpJnt = 'middleBackFoot3_rgt_tmpJnt' ,
                       fngr4TmpJnt = 'middleBackFoot4_rgt_tmpJnt' ,
                       fngr5TmpJnt = '' ,
                       parent = 'toeBack_rgt_jnt' ,
                       animGrp = 'anim_grp' ,
                       skinGrp = 'skin_grp' ,
                       jntGrp = 'jnt_grp' ,
                       stillGrp = 'still_grp' ,
                       side = 'rgt' ,
                       finger = 'middle' ,
                       type = 'foot' ,
                       elem = 'Back' ,
                       val = 0.1 ,
                       charSize = size 
                     )
    
    # ring
    finger.rigFinger(
                       fngr1TmpJnt = 'ringBackFoot1_rgt_tmpJnt' ,
                       fngr2TmpJnt = 'ringBackFoot2_rgt_tmpJnt' ,
                       fngr3TmpJnt = 'ringBackFoot3_rgt_tmpJnt' ,
                       fngr4TmpJnt = 'ringBackFoot4_rgt_tmpJnt' ,
                       fngr5TmpJnt = '' ,
                       parent = 'toeBack_rgt_jnt' ,
                       animGrp = 'anim_grp' ,
                       skinGrp = 'skin_grp' ,
                       jntGrp = 'jnt_grp' ,
                       stillGrp = 'still_grp' ,
                       side = 'rgt' ,
                       finger = 'ring' ,
                       type = 'foot' ,
                       elem = 'Back' ,
                       val = 0.1 ,
                       charSize = size 
                     )
    
    # pinky
    finger.rigFinger(
                       fngr1TmpJnt = 'pinkyBackFoot1_rgt_tmpJnt' ,
                       fngr2TmpJnt = 'pinkyBackFoot2_rgt_tmpJnt' ,
                       fngr3TmpJnt = 'pinkyBackFoot3_rgt_tmpJnt' ,
                       fngr4TmpJnt = 'pinkyBackFoot4_rgt_tmpJnt' ,
                       fngr5TmpJnt = '' ,
                       parent = 'toeBack_rgt_jnt' ,
                       animGrp = 'anim_grp' ,
                       skinGrp = 'skin_grp' ,
                       jntGrp = 'jnt_grp' ,
                       stillGrp = 'still_grp' ,
                       side = 'rgt' ,
                       finger = 'pinky' ,
                       type = 'foot' ,
                       elem = 'Back' ,
                       val = 0.1 ,
                       charSize = size 
                     )
    
    
    #-- Finger Foot Attribute
    # Left
    fingerAttr.rigFingerAttr(  
                               ctrl = 'legBack_lft_ctrl' ,
                               side = 'lft' ,
                               type = 'foot' ,
                               elem = 'Back' ,
                               num = 4 ,
                               finger = ( 'thumb' , 'index' , 'middle' , 'ring' , 'pinky' )
                            )
    
    # Right
    fingerAttr.rigFingerAttr(  
                               ctrl = 'legBack_rgt_ctrl' ,
                               side = 'rgt' ,
                               type = 'foot' ,
                               elem = 'Back' ,
                               num = 4 ,
                               finger = ( 'thumb' , 'index' , 'middle' , 'ring' , 'pinky' )
                            )
    
    #-- CleanUp
    core.jntRadius( r = radius )
    core.cleanUpAttr()