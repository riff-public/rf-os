import maya.cmds as mc
import maya.mel as mm
import ppmel.core as core
import ppmel.rigMain as main
import ppmel.rigRoot as root
import ppmel.rigSpine as spine
import ppmel.rigPelvis as pelvis
import ppmel.rigNeck as neck
import ppmel.rigHead as head
import ppmel.rigClavicle as clavicle
import ppmel.rigArm as arm
import ppmel.rigLeg as leg
import ppmel.rigFinger as finger
import ppmel.rigFingerAttr as fingerAttr
import ppmel.rigFk as fk
reload( core )
reload( root )
reload( spine )
reload( pelvis )
reload( neck )
reload( head )
reload( clavicle )
reload( arm )
reload( leg )
reload( finger )
reload( fingerAttr )
reload( fk )


def rigHuman( animGrp = 'anim_grp' ,
              skinGrp = 'skin_grp' ,
              jntGrp = 'jnt_grp' ,
              ikhGrp = 'ikh_grp' ,
              stillGrp = 'still_grp' ,
              radius = 0.5 ,
              size = 3 ) :

    #-- Main
    main.rigMain( charSize = size )
    
    
    #-- Root
    root.rigRoot(
                    rootTmpJnt = 'root_tmpJnt' ,
                    animGrp = animGrp ,
                    skinGrp = skinGrp ,
                    charSize = size
                )
    
    
    #-- Spine
    spine.rigSpine(
                        spine1TmpJnt = 'spine1_tmpJnt' ,
                        spine2TmpJnt = 'spine2_tmpJnt' ,
                        spine3TmpJnt = 'spine3_tmpJnt' ,
                        spine4TmpJnt = 'spine4_tmpJnt' ,
                        parent = 'root_jnt' ,
                        animGrp = animGrp ,
                        skinGrp = skinGrp ,
                        jntGrp = jntGrp ,
                        spineFk = True ,
                        spineIk = False ,
                        rotateOrder = 'yzx' ,
                        axis = 'y' ,
                        charSize = size
                    )
    
    
    #-- Pelvis
    pelvis.rigPelvis(
                        pelvisTmpJnt = 'pelvis_tmpJnt' ,
                        parent = 'root_jnt' ,
                        animGrp = animGrp ,
                        skinGrp = skinGrp ,
                        charSize = size
                    )
    
    
    #-- Neck
    neck.rigNeck(
                    neck1TmpJnt = 'neck1_tmpJnt' ,
                    neck2TmpJnt = 'head1_tmpJnt' ,
                    parent = 'spine4Pos_jnt' ,
                    animGrp = animGrp ,
                    skinGrp = skinGrp ,
                    jntGrp = jntGrp ,
                    ribbon = True ,
                    rotateOrder = 'xzy' ,
                    axis = 'y' ,
                    charSize = size
                )
    
    
    #-- Head
    head.rigHead(
                    head1TmpJnt = 'head1_tmpJnt' ,
                    head2TmpJnt = 'head2_tmpJnt' ,
                    eyeTrgTmpJnt = 'eyeTrgt_tmpJnt' ,
                    eyeLftTrgTmpJnt = 'eyeTrgt_lft_tmpJnt' ,
                    eyeRgtTrgTmpJnt = 'eyeTrgt_rgt_tmpJnt' ,
                    eyeLftTmpJnt = 'eye_lft_tmpJnt' ,
                    eyeRgtTmpJnt = 'eye_rgt_tmpJnt' ,
                    jaw1UprTmpJnt = 'jaw1_upr_tmpJnt' ,
                    jaw2UprTmpJnt = 'jaw2_upr_tmpJnt' ,
                    jaw1LwrTmpJnt = 'jaw1_lwr_tmpJnt' ,
                    jaw2LwrTmpJnt = 'jaw2_lwr_tmpJnt' ,
                    jaw3LwrTmpJnt = 'jaw3_lwr_tmpJnt' ,
                    parent = 'neck2_jnt' ,
                    animGrp = animGrp ,
                    skinGrp = skinGrp ,
                    jntGrp = jntGrp ,
                    eyeRig = True ,
                    jawRig = True ,
                    charSize = size
                )
    
    
    #-- Tongue
    fk.rigFk(
                name = 'toungue' ,
                tmpJnt = [ 'tongue1_tmpJnt' , 'tongue2_tmpJnt' , 'tongue3_tmpJnt' , 
                           'tongue4_tmpJnt' , 'tongue5_tmpJnt' , 'tongue6_tmpJnt' ] ,
                parent = 'jaw_lwr_jnt' ,
                animGrp = 'anim_grp' ,
                axis = 'y' ,
                shape = 'square' ,
                color = 'blue' ,
                side = ''
            )
    
    
    #-- Clavicle
    # Left
    clavicle.rigClavicle(
                            clav1TmpJnt = 'clav_lft_tmpJnt' ,
                            clav2TmpJnt = 'upArm_lft_tmpJnt' ,
                            parent = 'spine4Pos_jnt' ,
                            animGrp = animGrp ,
                            skinGrp = skinGrp ,
                            jntGrp = jntGrp ,
                            stillGrp = stillGrp ,
                            side = 'lft' ,
                            val = 0.1 ,
                            elem = '' ,
                            charSize = size 
                        )
    
    # Right
    clavicle.rigClavicle(
                            clav1TmpJnt = 'clav_rgt_tmpJnt' ,
                            clav2TmpJnt = 'upArm_rgt_tmpJnt' ,
                            parent = 'spine4Pos_jnt' ,
                            animGrp = animGrp ,
                            skinGrp = skinGrp ,
                            jntGrp = jntGrp ,
                            stillGrp = stillGrp ,
                            side = 'rgt' ,
                            val = -0.1 ,
                            elem = '' ,
                            charSize = size 
                        )
    
    
    #-- Arm
    # Left
    arm.rigArm(
                    upArmTmpJnt = 'upArm_lft_tmpJnt' ,
                    foreArmTmpJnt = 'foreArm_lft_tmpJnt' ,
                    wristTmpJnt = 'wrist_lft_tmpJnt' ,
                    handTmpJnt = 'hand_lft_tmpJnt' ,
                    elbowTmpJnt = 'elbow_lft_tmpJnt' ,
                    parent = 'clav2_lft_jnt' ,
                    animGrp = animGrp ,
                    skinGrp = skinGrp ,
                    jntGrp = jntGrp ,
                    stillGrp = stillGrp ,
                    ikhGrp = ikhGrp ,
                    side = 'lft' ,
                    val = 0.1 ,
                    elem = '' ,
                    armFk = True ,
                    armIk = True ,
                    ribbon = True ,
                    charSize = size 
                )
    
    # Right
    arm.rigArm(
                    upArmTmpJnt = 'upArm_rgt_tmpJnt' ,
                    foreArmTmpJnt = 'foreArm_rgt_tmpJnt' ,
                    wristTmpJnt = 'wrist_rgt_tmpJnt' ,
                    handTmpJnt = 'hand_rgt_tmpJnt' ,
                    elbowTmpJnt = 'elbow_rgt_tmpJnt' ,
                    parent = 'clav2_rgt_jnt' ,
                    animGrp = animGrp ,
                    skinGrp = skinGrp ,
                    jntGrp = jntGrp ,
                    stillGrp = stillGrp ,
                    ikhGrp = ikhGrp ,
                    side = 'rgt' ,
                    val = -0.1 ,
                    elem = '' ,
                    armFk = True ,
                    armIk = True ,
                    ribbon = True ,
                    charSize = size 
                )
    
    
    #-- Leg
    # Left
    leg.rigLeg(
                    upLegTmpJnt = 'upLeg_lft_tmpJnt' ,
                    lowLegTmpJnt = 'lowLeg_lft_tmpJnt' ,
                    ankleTmpJnt = 'ankle_lft_tmpJnt' ,
                    ballTmpJnt = 'ball_lft_tmpJnt' ,
                    toeTmpJnt = 'toe_lft_tmpJnt' ,
                    heelTmpJnt = 'heel_lft_tmpJnt' ,
                    footInTmpJnt = 'footIn_lft_tmpJnt' ,
                    footOutTmpJnt = 'footOut_lft_tmpJnt' ,
                    kneeTmpJnt = 'knee_lft_tmpJnt' ,
                    parent = 'pelvis_jnt' ,
                    animGrp = animGrp ,
                    skinGrp = skinGrp ,
                    jntGrp = jntGrp ,
                    stillGrp = stillGrp ,
                    ikhGrp = ikhGrp ,
                    side = 'lft' ,
                    val = 0.1 ,
                    elem = '' ,
                    foot = True ,
                    legFk = True ,
                    legIk = True ,
                    ribbon = True ,
                    charSize = size
                )
    
    # Right
    leg.rigLeg(
                    upLegTmpJnt = 'upLeg_rgt_tmpJnt' ,
                    lowLegTmpJnt = 'lowLeg_rgt_tmpJnt' ,
                    ankleTmpJnt = 'ankle_rgt_tmpJnt' ,
                    ballTmpJnt = 'ball_rgt_tmpJnt' ,
                    toeTmpJnt = 'toe_rgt_tmpJnt' ,
                    heelTmpJnt = 'heel_rgt_tmpJnt' ,
                    footInTmpJnt = 'footIn_rgt_tmpJnt' ,
                    footOutTmpJnt = 'footOut_rgt_tmpJnt' ,
                    kneeTmpJnt = 'knee_rgt_tmpJnt' ,
                    parent = 'pelvis_jnt' ,
                    animGrp = animGrp ,
                    skinGrp = skinGrp ,
                    jntGrp = jntGrp ,
                    stillGrp = stillGrp ,
                    ikhGrp = ikhGrp ,
                    side = 'rgt' ,
                    val = -0.1 ,
                    elem = '' ,
                    foot = True ,
                    legFk = True ,
                    legIk = True ,
                    ribbon = True ,
                    charSize = size
                )
    
    
    #-- Finger Hand
    # Left
    # thumb
    finger.rigFinger(
                        fngr1TmpJnt = 'thumb1_lft_tmpJnt' ,
                        fngr2TmpJnt = 'thumb2_lft_tmpJnt' ,
                        fngr3TmpJnt = 'thumb3_lft_tmpJnt' ,
                        fngr4TmpJnt = 'thumb4_lft_tmpJnt' ,
                        fngr5TmpJnt = '' ,
                        parent = 'hand_lft_jnt' ,
                        animGrp = animGrp ,
                        skinGrp = skinGrp ,
                        jntGrp = jntGrp ,
                        stillGrp = stillGrp ,
                        side = 'lft' ,
                        finger = 'thumb' ,
                        type = 'hand' , # hand , foot
                        elem = '' ,
                        val = 0.1 ,
                        charSize = size 
                    )
    
    # index
    finger.rigFinger(
                        fngr1TmpJnt = 'index1_lft_tmpJnt' ,
                        fngr2TmpJnt = 'index2_lft_tmpJnt' ,
                        fngr3TmpJnt = 'index3_lft_tmpJnt' ,
                        fngr4TmpJnt = 'index4_lft_tmpJnt' ,
                        fngr5TmpJnt = 'index5_lft_tmpJnt' ,
                        parent = 'hand_lft_jnt' ,
                        animGrp = animGrp ,
                        skinGrp = skinGrp ,
                        jntGrp = jntGrp ,
                        stillGrp = stillGrp ,
                        side = 'lft' ,
                        finger = 'index' ,
                        type = 'hand' , # hand , foot
                        elem = '' ,
                        val = 0.1 ,
                        charSize = size 
                    )
    
    # middle
    finger.rigFinger(
                        fngr1TmpJnt = 'middle1_lft_tmpJnt' ,
                        fngr2TmpJnt = 'middle2_lft_tmpJnt' ,
                        fngr3TmpJnt = 'middle3_lft_tmpJnt' ,
                        fngr4TmpJnt = 'middle4_lft_tmpJnt' ,
                        fngr5TmpJnt = 'middle5_lft_tmpJnt' ,
                        parent = 'hand_lft_jnt' ,
                        animGrp = animGrp ,
                        skinGrp = skinGrp ,
                        jntGrp = jntGrp ,
                        stillGrp = stillGrp ,
                        side = 'lft' ,
                        finger = 'middle' ,
                        type = 'hand' , # hand , foot
                        elem = '' ,
                        val = 0.1 ,
                        charSize = size 
                    )
    
    # ring
    finger.rigFinger(
                        fngr1TmpJnt = 'ring1_lft_tmpJnt' ,
                        fngr2TmpJnt = 'ring2_lft_tmpJnt' ,
                        fngr3TmpJnt = 'ring3_lft_tmpJnt' ,
                        fngr4TmpJnt = 'ring4_lft_tmpJnt' ,
                        fngr5TmpJnt = 'ring5_lft_tmpJnt' ,
                        parent = 'hand_lft_jnt' ,
                        animGrp = animGrp ,
                        skinGrp = skinGrp ,
                        jntGrp = jntGrp ,
                        stillGrp = stillGrp ,
                        side = 'lft' ,
                        finger = 'ring' ,
                        type = 'hand' , # hand , foot
                        elem = '' ,
                        val = 0.1 ,
                        charSize = size 
                    )
    
    # pinky
    finger.rigFinger(
                        fngr1TmpJnt = 'pinky1_lft_tmpJnt' ,
                        fngr2TmpJnt = 'pinky2_lft_tmpJnt' ,
                        fngr3TmpJnt = 'pinky3_lft_tmpJnt' ,
                        fngr4TmpJnt = 'pinky4_lft_tmpJnt' ,
                        fngr5TmpJnt = 'pinky5_lft_tmpJnt' ,
                        parent = 'hand_lft_jnt' ,
                        animGrp = animGrp ,
                        skinGrp = skinGrp ,
                        jntGrp = jntGrp ,
                        stillGrp = stillGrp ,
                        side = 'lft' ,
                        finger = 'pinky' ,
                        type = 'hand' , # hand , foot
                        elem = '' ,
                        val = 0.1 ,
                        charSize = size 
                    )
    
    # Right
    # thumb
    finger.rigFinger(
                        fngr1TmpJnt = 'thumb1_rgt_tmpJnt' ,
                        fngr2TmpJnt = 'thumb2_rgt_tmpJnt' ,
                        fngr3TmpJnt = 'thumb3_rgt_tmpJnt' ,
                        fngr4TmpJnt = 'thumb4_rgt_tmpJnt' ,
                        fngr5TmpJnt = '' ,
                        parent = 'hand_rgt_jnt' ,
                        animGrp = animGrp ,
                        skinGrp = skinGrp ,
                        jntGrp = jntGrp ,
                        stillGrp = stillGrp ,
                        side = 'rgt' ,
                        finger = 'thumb' ,
                        type = 'hand' , # hand , foot
                        elem = '' ,
                        val = -0.1 ,
                        charSize = size 
                    )
    
    # index
    finger.rigFinger(
                        fngr1TmpJnt = 'index1_rgt_tmpJnt' ,
                        fngr2TmpJnt = 'index2_rgt_tmpJnt' ,
                        fngr3TmpJnt = 'index3_rgt_tmpJnt' ,
                        fngr4TmpJnt = 'index4_rgt_tmpJnt' ,
                        fngr5TmpJnt = 'index5_rgt_tmpJnt' ,
                        parent = 'hand_rgt_jnt' ,
                        animGrp = animGrp ,
                        skinGrp = skinGrp ,
                        jntGrp = jntGrp ,
                        stillGrp = stillGrp ,
                        side = 'rgt' ,
                        finger = 'index' ,
                        type = 'hand' , # hand , foot
                        elem = '' ,
                        val = -0.1 ,
                        charSize = size 
                    )
    
    # middle
    finger.rigFinger(
                        fngr1TmpJnt = 'middle1_rgt_tmpJnt' ,
                        fngr2TmpJnt = 'middle2_rgt_tmpJnt' ,
                        fngr3TmpJnt = 'middle3_rgt_tmpJnt' ,
                        fngr4TmpJnt = 'middle4_rgt_tmpJnt' ,
                        fngr5TmpJnt = 'middle5_rgt_tmpJnt' ,
                        parent = 'hand_rgt_jnt' ,
                        animGrp = animGrp ,
                        skinGrp = skinGrp ,
                        jntGrp = jntGrp ,
                        stillGrp = stillGrp ,
                        side = 'rgt' ,
                        finger = 'middle' ,
                        type = 'hand' , # hand , foot
                        elem = '' ,
                        val = -0.1 ,
                        charSize = size 
                    )
    
    # ring
    finger.rigFinger(
                        fngr1TmpJnt = 'ring1_rgt_tmpJnt' ,
                        fngr2TmpJnt = 'ring2_rgt_tmpJnt' ,
                        fngr3TmpJnt = 'ring3_rgt_tmpJnt' ,
                        fngr4TmpJnt = 'ring4_rgt_tmpJnt' ,
                        fngr5TmpJnt = 'ring5_rgt_tmpJnt' ,
                        parent = 'hand_rgt_jnt' ,
                        animGrp = animGrp ,
                        skinGrp = skinGrp ,
                        jntGrp = jntGrp ,
                        stillGrp = stillGrp ,
                        side = 'rgt' ,
                        finger = 'ring' ,
                        type = 'hand' , # hand , foot
                        elem = '' ,
                        val = -0.1 ,
                        charSize = size 
                    )
    
    # pinky
    finger.rigFinger(
                        fngr1TmpJnt = 'pinky1_rgt_tmpJnt' ,
                        fngr2TmpJnt = 'pinky2_rgt_tmpJnt' ,
                        fngr3TmpJnt = 'pinky3_rgt_tmpJnt' ,
                        fngr4TmpJnt = 'pinky4_rgt_tmpJnt' ,
                        fngr5TmpJnt = 'pinky5_rgt_tmpJnt' ,
                        parent = 'hand_rgt_jnt' ,
                        animGrp = animGrp ,
                        skinGrp = skinGrp ,
                        jntGrp = jntGrp ,
                        stillGrp = stillGrp ,
                        side = 'rgt' ,
                        finger = 'pinky' ,
                        type = 'hand' , # hand , foot
                        elem = '' ,
                        val = -0.1 ,
                        charSize = size 
                    )
    
    
    #-- Finger Hand Attribute
    # Left
    fingerAttr.rigFingerAttr(   ctrl = 'arm_lft_ctrl' ,
                                side = 'lft' ,
                                type = 'hand' ,  # hand , foot
                                elem = '' ,
                                num = 5 ,
                                finger = ( 'thumb' , 'index' , 'middle' , 'ring' , 'pinky' )
                            )
                            
    # Right
    fingerAttr.rigFingerAttr(   ctrl = 'arm_rgt_ctrl' ,
                                side = 'rgt' ,
                                type = 'hand' ,  # hand , foot
                                elem = '' ,
                                num = 5 ,
                                finger = ( 'thumb' , 'index' , 'middle' , 'ring' , 'pinky' )
                            )
    
    #-- Finger Foot
    # Left
    # thumb
    finger.rigFinger(
                        fngr1TmpJnt = 'thumbFoot1_lft_tmpJnt' ,
                        fngr2TmpJnt = 'thumbFoot2_lft_tmpJnt' ,
                        fngr3TmpJnt = 'thumbFoot3_lft_tmpJnt' ,
                        fngr4TmpJnt = 'thumbFoot4_lft_tmpJnt' ,
                        fngr5TmpJnt = '' ,
                        parent = 'toe_lft_jnt' ,
                        animGrp = animGrp ,
                        skinGrp = skinGrp ,
                        jntGrp = jntGrp ,
                        stillGrp = stillGrp ,
                        side = 'lft' ,
                        finger = 'thumb' ,
                        type = 'foot' , # hand , foot
                        elem = '' ,
                        val = 0.1 ,
                        charSize = size 
                    )
    
    # index
    finger.rigFinger(
                        fngr1TmpJnt = 'indexFoot1_lft_tmpJnt' ,
                        fngr2TmpJnt = 'indexFoot2_lft_tmpJnt' ,
                        fngr3TmpJnt = 'indexFoot3_lft_tmpJnt' ,
                        fngr4TmpJnt = 'indexFoot4_lft_tmpJnt' ,
                        fngr5TmpJnt = '' ,
                        parent = 'toe_lft_jnt' ,
                        animGrp = animGrp ,
                        skinGrp = skinGrp ,
                        jntGrp = jntGrp ,
                        stillGrp = stillGrp ,
                        side = 'lft' ,
                        finger = 'index' ,
                        type = 'foot' , # hand , foot
                        elem = '' ,
                        val = 0.1 ,
                        charSize = size 
                )
    
    # middle
    finger.rigFinger(
                        fngr1TmpJnt = 'middleFoot1_lft_tmpJnt' ,
                        fngr2TmpJnt = 'middleFoot2_lft_tmpJnt' ,
                        fngr3TmpJnt = 'middleFoot3_lft_tmpJnt' ,
                        fngr4TmpJnt = 'middleFoot4_lft_tmpJnt' ,
                        fngr5TmpJnt = '' ,
                        parent = 'toe_lft_jnt' ,
                        animGrp = animGrp ,
                        skinGrp = skinGrp ,
                        jntGrp = jntGrp ,
                        stillGrp = stillGrp ,
                        side = 'lft' ,
                        finger = 'middle' ,
                        type = 'foot' , # hand , foot
                        elem = '' ,
                        val = 0.1 ,
                        charSize = size 
                    )
    
    # ring
    finger.rigFinger(
                        fngr1TmpJnt = 'ringFoot1_lft_tmpJnt' ,
                        fngr2TmpJnt = 'ringFoot2_lft_tmpJnt' ,
                        fngr3TmpJnt = 'ringFoot3_lft_tmpJnt' ,
                        fngr4TmpJnt = 'ringFoot4_lft_tmpJnt' ,
                        fngr5TmpJnt = '' ,
                        parent = 'toe_lft_jnt' ,
                        animGrp = animGrp ,
                        skinGrp = skinGrp ,
                        jntGrp = jntGrp ,
                        stillGrp = stillGrp ,
                        side = 'lft' ,
                        finger = 'ring' ,
                        type = 'foot' , # hand , foot
                        elem = '' ,
                        val = 0.1 ,
                        charSize = size 
                    )
    
    # pinky
    finger.rigFinger(
                        fngr1TmpJnt = 'pinkyFoot1_lft_tmpJnt' ,
                        fngr2TmpJnt = 'pinkyFoot2_lft_tmpJnt' ,
                        fngr3TmpJnt = 'pinkyFoot3_lft_tmpJnt' ,
                        fngr4TmpJnt = 'pinkyFoot4_lft_tmpJnt' ,
                        fngr5TmpJnt = '' ,
                        parent = 'toe_lft_jnt' ,
                        animGrp = animGrp ,
                        skinGrp = skinGrp ,
                        jntGrp = jntGrp ,
                        stillGrp = stillGrp ,
                        side = 'lft' ,
                        finger = 'pinky' ,
                        type = 'foot' , # hand , foot
                        elem = '' ,
                        val = 0.1 ,
                        charSize = size 
                    )
    
    # Right
    # thumb
    finger.rigFinger(
                        fngr1TmpJnt = 'thumbFoot1_rgt_tmpJnt' ,
                        fngr2TmpJnt = 'thumbFoot2_rgt_tmpJnt' ,
                        fngr3TmpJnt = 'thumbFoot3_rgt_tmpJnt' ,
                        fngr4TmpJnt = 'thumbFoot4_rgt_tmpJnt' ,
                        fngr5TmpJnt = '' ,
                        parent = 'toe_rgt_jnt' ,
                        animGrp = animGrp ,
                        skinGrp = skinGrp ,
                        jntGrp = jntGrp ,
                        stillGrp = stillGrp ,
                        side = 'rgt' ,
                        finger = 'thumb' ,
                        type = 'foot' , # hand , foot
                        elem = '' ,
                        val = -0.1 ,
                        charSize = size 
                    )
    
    # index
    finger.rigFinger(
                        fngr1TmpJnt = 'indexFoot1_rgt_tmpJnt' ,
                        fngr2TmpJnt = 'indexFoot2_rgt_tmpJnt' ,
                        fngr3TmpJnt = 'indexFoot3_rgt_tmpJnt' ,
                        fngr4TmpJnt = 'indexFoot4_rgt_tmpJnt' ,
                        fngr5TmpJnt = '' ,
                        parent = 'toe_rgt_jnt' ,
                        animGrp = animGrp ,
                        skinGrp = skinGrp ,
                        jntGrp = jntGrp ,
                        stillGrp = stillGrp ,
                        side = 'rgt' ,
                        finger = 'index' ,
                        type = 'foot' , # hand , foot
                        elem = '' ,
                        val = -0.1 ,
                        charSize = size 
                )
    
    # middle
    finger.rigFinger(
                        fngr1TmpJnt = 'middleFoot1_rgt_tmpJnt' ,
                        fngr2TmpJnt = 'middleFoot2_rgt_tmpJnt' ,
                        fngr3TmpJnt = 'middleFoot3_rgt_tmpJnt' ,
                        fngr4TmpJnt = 'middleFoot4_rgt_tmpJnt' ,
                        fngr5TmpJnt = '' ,
                        parent = 'toe_rgt_jnt' ,
                        animGrp = animGrp ,
                        skinGrp = skinGrp ,
                        jntGrp = jntGrp ,
                        stillGrp = stillGrp ,
                        side = 'rgt' ,
                        finger = 'middle' ,
                        type = 'foot' , # hand , foot
                        elem = '' ,
                        val = -0.1 ,
                        charSize = size 
                    )
    
    # ring
    finger.rigFinger(
                        fngr1TmpJnt = 'ringFoot1_rgt_tmpJnt' ,
                        fngr2TmpJnt = 'ringFoot2_rgt_tmpJnt' ,
                        fngr3TmpJnt = 'ringFoot3_rgt_tmpJnt' ,
                        fngr4TmpJnt = 'ringFoot4_rgt_tmpJnt' ,
                        fngr5TmpJnt = '' ,
                        parent = 'toe_rgt_jnt' ,
                        animGrp = animGrp ,
                        skinGrp = skinGrp ,
                        jntGrp = jntGrp ,
                        stillGrp = stillGrp ,
                        side = 'rgt' ,
                        finger = 'ring' ,
                        type = 'foot' , # hand , foot
                        elem = '' ,
                        val = -0.1 ,
                        charSize = size 
                    )
    
    # pinky
    finger.rigFinger(
                        fngr1TmpJnt = 'pinkyFoot1_rgt_tmpJnt' ,
                        fngr2TmpJnt = 'pinkyFoot2_rgt_tmpJnt' ,
                        fngr3TmpJnt = 'pinkyFoot3_rgt_tmpJnt' ,
                        fngr4TmpJnt = 'pinkyFoot4_rgt_tmpJnt' ,
                        fngr5TmpJnt = '' ,
                        parent = 'toe_rgt_jnt' ,
                        animGrp = animGrp ,
                        skinGrp = skinGrp ,
                        jntGrp = jntGrp ,
                        stillGrp = stillGrp ,
                        side = 'rgt' ,
                        finger = 'pinky' ,
                        type = 'foot' , # hand , foot
                        elem = '' ,
                        val = -0.1 ,
                        charSize = size 
                    )
    
    
    #-- Finger Foot Attribute
    # Left
    fingerAttr.rigFingerAttr(   ctrl = 'leg_lft_ctrl' ,
                                side = 'lft' ,
                                type = 'foot' ,  # hand , foot
                                elem = '' ,
                                num = 4 ,
                                finger = ( 'thumb' , 'index' , 'middle' , 'ring' , 'pinky' )
                            )
                            
    # Right
    fingerAttr.rigFingerAttr(   ctrl = 'leg_rgt_ctrl' ,
                                side = 'rgt' ,
                                type = 'foot' ,  # hand , foot
                                elem = '' ,
                                num = 4 ,
                                finger = ( 'thumb' , 'index' , 'middle' , 'ring' , 'pinky' )
                            )
    
    #-- CleanUp
    core.jntRadius( r = radius )
    core.cleanUpAttr()