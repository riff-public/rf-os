import maya.cmds as mc
import maya.mel as mm
import ppmel.core as core
import ppmel.rigRibbon as rbn
reload( core )
reload( rbn )


def rigLeg( upLegTmpJnt = 'upLeg_lft_tmpJnt' ,
            lowLegTmpJnt = 'lowLeg_lft_tmpJnt' ,
            ankleTmpJnt = 'ankle_lft_tmpJnt' ,
            ballTmpJnt = 'ball_lft_tmpJnt' ,
            toeTmpJnt = 'toe_lft_tmpJnt' ,
            heelTmpJnt = 'heel_lft_tmpJnt' ,
            footInTmpJnt = 'footIn_lft_tmpJnt' ,
            footOutTmpJnt = 'footOut_lft_tmpJnt' ,
            kneeTmpJnt = 'knee_lft_tmpJnt' ,
            parent = 'pelvis_jnt' ,
            animGrp = 'anim_grp' ,
            skinGrp = 'skin_grp' ,
            jntGrp = 'jnt_grp' ,
            stillGrp = 'still_grp' ,
            ikhGrp = 'ikh_grp' ,
            side = 'lft' ,
            val = 0.1 , # Left = 0.1 , Right = -0.1
            elem = '' , # front , back , mid
            foot = True ,
            legFk = True ,
            legIk = True ,
            ribbon = True ,
            charSize = 1 ) :
    
    #-- Info
    elem = elem.capitalize()
            
    ## Main Rig ##
    #-- Group
    legRigGrp = core.transform( 'leg%sRig_%s_grp' %( elem , side ))
    legJntGrp = core.transform( 'leg%sJnt_%s_grp' %( elem , side ))
    core.parCons( 0 , parent , legRigGrp )
    core.snap( parent , legJntGrp )
    
    #-- Joint
    upLegJnt = core.addJnt( 'upLeg%s_%s_jnt' %( elem , side ) , upLegTmpJnt )
    lowLegJnt = core.addJnt( 'lowLeg%s_%s_jnt' %( elem , side ) , lowLegTmpJnt )
    ankleJnt = core.addJnt( 'ankle%s_%s_jnt' %( elem , side ) , ankleTmpJnt )
    core.parent( ankleJnt , lowLegJnt , upLegJnt , parent )
    
    if foot == True :
        ballJnt = core.addJnt( 'ball%s_%s_jnt' %( elem , side ) , ballTmpJnt )
        toeJnt = core.addJnt( 'toe%s_%s_jnt' %( elem , side ) , toeTmpJnt )
        core.parent( toeJnt , ballJnt , ankleJnt )
        
    #-- Controls
    legCtrl = core.addCtrl( 'leg%s_%s_ctrl' %( elem , side ) , 'stick' , 'green' , jnt = False )
    legZro = core.addGrp( legCtrl )
    core.parent( legZro , legRigGrp )
    
    #-- Shape
    if side == 'lft' :
        core.rotateShape( legCtrl , -90 , 0 , 0 )
    elif side == 'rgt' :
        core.rotateShape( legCtrl , 90 , 0 , 0 )
        
    core.scaleShape( legCtrl , charSize )
    
    #-- Rig process
    core.parCons( 0 , ankleJnt , legZro )
    
    #-- Rotate order
    for each in ( upLegJnt , lowLegJnt , ankleJnt ) :
        core.setRotateOrder( each , 'yzx' )
        
    if foot == True :
        for each in ( ballJnt , toeJnt ) :
            core.setRotateOrder( each , 'yzx' )
        
    #-- Hierarchy
    core.parent( legRigGrp , animGrp )
    core.parent( legJntGrp , jntGrp )
            
    #-- Cleanup
    core.setLock( legZro , True , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
    core.setLockHide( legCtrl , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
            
            
    ## Fk Rig ##
    if legFk == True :
        #-- Group
        legFkGrp = core.transform( 'leg%sFk_%s_grp' %( elem , side ))
        legFkJntGrp = core.transform( 'leg%sFkJnt_%s_grp' %( elem , side ))
        core.snap( legRigGrp , legFkGrp )
        core.snap( legRigGrp , legFkJntGrp )
        
        #-- Joint
        upLegFkJnt = core.addJnt( 'upLeg%sFk_%s_jnt' %( elem , side ) , upLegTmpJnt )
        lowLegFkJnt = core.addJnt( 'lowLeg%sFk_%s_jnt' %( elem , side ) , lowLegTmpJnt )
        ankleFkJnt = core.addJnt( 'ankle%sFk_%s_jnt' %( elem , side ) , ankleTmpJnt )
        core.parent( ankleFkJnt , lowLegFkJnt , upLegFkJnt , legFkJntGrp )
        
        if foot == True :
            ballFkJnt = core.addJnt( 'ball%sFk_%s_jnt' %( elem , side ) , ballTmpJnt )
            toeFkJnt = core.addJnt( 'toe%sFk_%s_jnt' %( elem , side ) , toeTmpJnt )
            core.parent( toeFkJnt , ballFkJnt , ankleFkJnt )
            
        #-- Controls
        upLegFkCtrl = core.addCtrl( 'upLeg%sFk_%s_ctrl' %( elem , side ) , 'circle' , 'red' , jnt = True )
        upLegFkGmbl = core.addGimbal( upLegFkCtrl )
        upLegFkZro = core.addGrp( upLegFkCtrl )
        upLegFkOfst = core.addGrp( upLegFkCtrl , 'Ofst' )
        core.snapPoint( upLegFkJnt , upLegFkZro )
        core.snapJntOrient( upLegFkJnt , upLegFkCtrl )
        
        lowLegFkCtrl = core.addCtrl( 'lowLeg%sFk_%s_ctrl' %( elem , side ) , 'circle' , 'red' , jnt = True )
        lowLegFkGmbl = core.addGimbal( lowLegFkCtrl )
        lowLegFkZro = core.addGrp( lowLegFkCtrl )
        lowLegFkOfst = core.addGrp( lowLegFkCtrl , 'Ofst' )
        core.snapPoint( lowLegFkJnt , lowLegFkZro )
        core.snapJntOrient( lowLegFkJnt , lowLegFkCtrl )
        
        ankleFkCtrl = core.addCtrl( 'ankle%sFk_%s_ctrl' %( elem , side ) , 'circle' , 'red' , jnt = True )
        ankleFkGmbl = core.addGimbal( ankleFkCtrl )
        ankleFkZro = core.addGrp( ankleFkCtrl )
        ankleFkOfst = core.addGrp( ankleFkCtrl , 'Ofst' )
        core.snapPoint( ankleFkJnt , ankleFkZro )
        core.snapJntOrient( ankleFkJnt , ankleFkCtrl )
        
        if foot == True :
            ballFkCtrl = core.addCtrl( 'ball%sFk_%s_ctrl' %( elem , side ) , 'circle' , 'red' , jnt = True )
            ballFkGmbl = core.addGimbal( ballFkCtrl )
            ballFkZro = core.addGrp( ballFkCtrl )
            ballFkOfst = core.addGrp( ballFkCtrl , 'Ofst' )
            ballFkSclOfst = core.transform( 'ball%sFkCtrlSclOfst_%s_grp' %( elem , side ))
            core.snap( ankleFkJnt , ballFkSclOfst )
            core.snapPoint( ballFkJnt , ballFkZro )
            core.snapJntOrient( ballFkJnt , ballFkCtrl )
            core.parent( ballFkZro , ballFkSclOfst )
            
        #-- Shape   
        for ctrl in ( upLegFkCtrl , upLegFkGmbl , lowLegFkCtrl , lowLegFkGmbl , ankleFkCtrl , ankleFkGmbl ) :
            core.scaleShape( ctrl , charSize )
            
        if foot == True :
            for ctrl in ( ballFkCtrl , ballFkGmbl ) :
                core.scaleShape( ctrl , charSize )
            
        #-- Rotate order
        for each in ( upLegFkJnt , lowLegFkJnt , ankleFkJnt , upLegFkCtrl , lowLegFkCtrl , ankleFkCtrl ) :
            core.setRotateOrder( each , 'yzx' )
            
        if foot == True :
            for each in ( ballFkJnt , toeFkJnt , ballFkCtrl ) :
                core.setRotateOrder( each , 'yzx' )
                
        #-- Rig process
        core.parCons( 0 , upLegFkGmbl , upLegFkJnt )
        core.parCons( 0 , lowLegFkGmbl , lowLegFkJnt )
        core.parCons( 0 , ankleFkGmbl , ankleFkJnt )
        
        core.addFkStretch( upLegFkCtrl , lowLegFkOfst , 'y' , -0.1 )
        core.addFkStretch( lowLegFkCtrl , ankleFkOfst , 'y' , -0.1 )
        
        core.addLocalWorld( upLegFkCtrl , 'anim_grp' , legFkGrp , upLegFkZro , 'orient' )
        
        if foot == True :
            core.parCons( 0 , ballFkGmbl , ballFkJnt )
            core.addFkStretch( ballFkCtrl , toeFkJnt , 'y' , val )
                
        #-- Hierarchy
        mc.parent( legFkGrp , legRigGrp )
        mc.parent( legFkJntGrp , legJntGrp )
        mc.parent( upLegFkZro , legFkGrp )
        mc.parent( lowLegFkZro , upLegFkGmbl )
        mc.parent( ankleFkZro , lowLegFkGmbl )
        
        if foot == True :
            mc.parent( ballFkSclOfst , ankleFkGmbl )
        
        #-- Cleanup
        for grp in ( legFkGrp , legFkJntGrp , upLegFkZro , upLegFkOfst , lowLegFkZro , lowLegFkOfst , ankleFkZro , ankleFkOfst ) :
            core.setLock( grp , True , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
        
        for ctrl in ( upLegFkCtrl , upLegFkGmbl , lowLegFkCtrl , lowLegFkGmbl , ankleFkCtrl , ankleFkGmbl ) :
            core.setLockHide( ctrl , 'sx' , 'sy' , 'sz' , 'v' )
            
        if foot == True :
            for grp in ( ballFkZro , ballFkOfst , ballFkSclOfst ) :
                core.setLock( grp , True , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
                
        for ctrl in ( ballFkCtrl , ballFkGmbl ) :
            core.setLockHide( ctrl , 'sx' , 'sy' , 'sz' , 'v' )
            
            
    ## Ik Rig ##
    if legIk == True :
        #-- Group
        legIkGrp = core.transform( 'leg%sIk_%s_grp' %( elem , side ))
        legIkJntGrp = core.transform( 'leg%sIkJnt_%s_grp' %( elem , side ))
        legIkIkhGrp = core.transform( 'leg%sIkHandle_%s_grp' %( elem , side ))
        core.snap( legRigGrp , legIkGrp )
        core.snap( legRigGrp , legIkJntGrp )
        core.snap( legRigGrp , legIkIkhGrp )
        
        if foot == True :
            footPivGrp = core.transform( 'foot%sIkPiv_%s_grp' %( elem , side ))
            footBallPivGrp = core.transform( 'footBall%sIkPiv_%s_grp' %( elem , side ))
            footBendPivGrp = core.transform( 'footBend%sIkPiv_%s_grp' %( elem , side ))
            footHeelPivGrp = core.transform( 'footHeel%sIkPiv_%s_grp' %( elem , side ))
            footToePivGrp = core.transform( 'footToe%sIkPiv_%s_grp' %( elem , side ))
            footInPivGrp = core.transform( 'footIn%sIkPiv_%s_grp' %( elem , side ))
            footOutPivGrp = core.transform( 'footOut%sIkPiv_%s_grp' %( elem , side ))
            
            core.snapPoint( ankleTmpJnt , footPivGrp )
            core.snapPoint( ballTmpJnt , footBallPivGrp )
            core.snapPoint( ballTmpJnt , footBendPivGrp )
            core.snapPoint( toeTmpJnt , footToePivGrp )
            core.snap( heelTmpJnt , footHeelPivGrp )
            core.snap( footInTmpJnt , footInPivGrp )
            core.snap( footOutTmpJnt , footOutPivGrp )
            
            for each in ( footPivGrp , footBallPivGrp , footBendPivGrp , footToePivGrp ) :
                core.snapOrient( footHeelPivGrp , each )
            
            mc.parent( footBallPivGrp , footBendPivGrp , footOutPivGrp )
            core.parent( footOutPivGrp , footInPivGrp , footHeelPivGrp , footToePivGrp , footPivGrp )
            
            #-- Joint
            upLegIkJnt = core.addJnt( 'upLeg%sIk_%s_jnt' %( elem , side ) , upLegTmpJnt )
            lowLegIkJnt = core.addJnt( 'lowLeg%sIk_%s_jnt' %( elem , side ) , lowLegTmpJnt )
            ankleIkJnt = core.addJnt( 'ankle%sIk_%s_jnt' %( elem , side ) , ankleTmpJnt )
            core.parent( ankleIkJnt , lowLegIkJnt , upLegIkJnt , legIkJntGrp )
        
        if foot == True :
            ballIkJnt = core.addJnt( 'ball%sIk_%s_jnt' %( elem , side ) , ballTmpJnt )
            toeIkJnt = core.addJnt( 'toe%sIk_%s_jnt' %( elem , side ) , toeTmpJnt )
            core.parent( toeIkJnt , ballIkJnt , ankleIkJnt )
        
        #-- Controls
        upLegIkCtrl = core.addCtrl( 'upLeg%sIk_%s_ctrl' %( elem , side ) , 'cube' , 'blue' , jnt = True )
        upLegIkGmbl = core.addGimbal( upLegIkCtrl )
        upLegIkZro = core.addGrp( upLegIkCtrl )
        core.snapPoint( upLegIkJnt , upLegIkZro )
        core.snapJntOrient( upLegIkJnt , upLegIkCtrl )
        
        ankleIkCtrl = core.addCtrl( 'ankle%sIk_%s_ctrl' %( elem , side ) , 'cube' , 'blue' , jnt = True )
        ankleIkGmbl = core.addGimbal( ankleIkCtrl )
        ankleIkZro = core.addGrp( ankleIkCtrl )
        core.snapPoint( ankleIkJnt , ankleIkZro )
        core.snapJntOrient( ankleIkJnt , ankleIkCtrl )
        
        kneeIkCtrl = core.addCtrl( 'knee%sIk_%s_ctrl' %( elem , side ) , 'sphere' , 'blue' , jnt = False )
        kneeIkZro = core.addGrp( kneeIkCtrl )
        core.snapPoint( kneeTmpJnt , kneeIkZro )
            
        #-- Shape
        for ctrl in ( upLegIkCtrl , upLegIkGmbl , ankleIkCtrl , ankleIkGmbl , kneeIkCtrl) :
            core.scaleShape( ctrl , charSize )
            
        #-- Rotate order
        for each in ( upLegIkJnt , lowLegIkJnt , ankleIkJnt , upLegIkCtrl , ankleIkCtrl ) :
            core.setRotateOrder( each , 'xzy' )
        
        if foot == True :
            for each in ( ballIkJnt , toeIkJnt ) :
                core.setRotateOrder( each , 'xzy' )
            
        #-- Rig process
        crv = core.addLine( kneeIkCtrl , lowLegIkJnt )
        
        ankleIkhDict = core.addIkh( 'ankle%sIk' %elem , 'ikRPsolver' , upLegIkJnt , ankleIkJnt )
        
        ankleIkh = ankleIkhDict['ikh']
        ankleIkhZro = ankleIkhDict['ikhZro']
        
        core.pvtCons( kneeIkCtrl , ankleIkh )
        core.addAttr( ankleIkCtrl , 'twist' )
        
        core.connectAttr( '%s.twist' %ankleIkCtrl , '%s.twist' %ankleIkh )
        
        core.pntCons( 0 , upLegIkCtrl , upLegIkJnt  )
        core.parCons( 1 , ankleIkGmbl , ankleIkhZro  )
        
        core.addLocalWorld( ankleIkCtrl , 'anim_grp' , upLegIkGmbl , ankleIkZro , 'parent' )
        core.addLocalWorld( kneeIkCtrl , 'anim_grp' , ankleIkCtrl , kneeIkZro , 'parent' )
        
        ikStrt = core.addIkStretch( ankleIkCtrl , kneeIkCtrl ,'ty' , 'leg%s' %elem , val , upLegIkJnt , lowLegIkJnt , ankleIkJnt )
        
        if foot == True :
            core.addAttr( ankleIkCtrl , 'toeStretch' )
            toeStrtMdv = core.multiplyDivide( 'toe%sIkStrt_%s_mdv' %( elem , side ))
            mc.setAttr( '%s.i2x' %toeStrtMdv , val )
            
            toeStrtPma = core.plusMinusAverage( 'toe%sIkStrt_%s_pma' %( elem , side ))
            core.addAttr( toeStrtPma , 'default' )
            mc.setAttr( '%s.default' %toeStrtPma , mc.getAttr( '%s.ty' %toeIkJnt ))
            core.connectAttr( '%s.toeStretch' %ankleIkCtrl , '%s.i1x' %toeStrtMdv )
            core.connectAttr( '%s.default' %toeStrtPma , '%s.i1[0]' %toeStrtPma )
            core.connectAttr( '%s.ox' %toeStrtMdv , '%s.i1[1]' %toeStrtPma )
            core.connectAttr( '%s.o1' %toeStrtPma , '%s.ty' %toeIkJnt )
            
            ballIkhDict = core.addIkh( 'ball%sIk' %elem , 'ikSCsolver' , ankleIkJnt , ballIkJnt )
            toeIkhDict = core.addIkh( 'toe%sIk' %elem , 'ikSCsolver' , ballIkJnt , toeIkJnt )
            
            ballIkh = ballIkhDict['ikh']
            ballIkhZro = ballIkhDict['ikhZro']
            
            toeIkh = toeIkhDict['ikh']
            toeIkhZro = toeIkhDict['ikhZro']
            
            core.clearCons( ankleIkhZro )
            
            core.parCons( 1 , footBallPivGrp , ankleIkhZro  )
            core.parCons( 1 , footBallPivGrp , ballIkhZro  )
            core.parCons( 1 , footBendPivGrp , toeIkhZro  )
            core.pvtCons( kneeIkCtrl , ankleIkh )
            
            core.addAttrType( ankleIkCtrl , 'Foot' )
            core.addAttr( ankleIkCtrl , 'ballRoll' )
            core.addAttr( ankleIkCtrl , 'heelRoll' )
            core.addAttr( ankleIkCtrl , 'toeRoll' )
            core.addAttr( ankleIkCtrl , 'heelTwist' )
            core.addAttr( ankleIkCtrl , 'toeTwist' )
            core.addAttr( ankleIkCtrl , 'toeBend' )
            core.addAttr( ankleIkCtrl , 'toeSide' )
            core.addAttr( ankleIkCtrl , 'toeSwirl' )
            core.addAttr( ankleIkCtrl , 'footRock' )
            
            mc.connectAttr( '%s.ballRoll' %ankleIkCtrl , '%s.rx' %footBallPivGrp )
            mc.connectAttr( '%s.heelRoll' %ankleIkCtrl , '%s.rx' %footHeelPivGrp )
            mc.connectAttr( '%s.toeRoll' %ankleIkCtrl , '%s.rx' %footToePivGrp )
            mc.connectAttr( '%s.heelTwist' %ankleIkCtrl , '%s.ry' %footHeelPivGrp )
            mc.connectAttr( '%s.toeTwist' %ankleIkCtrl , '%s.ry' %footToePivGrp )
            mc.connectAttr( '%s.toeBend' %ankleIkCtrl , '%s.rx' %footBendPivGrp )
            mc.connectAttr( '%s.toeSide' %ankleIkCtrl , '%s.ry' %footBendPivGrp )
            mc.connectAttr( '%s.toeSwirl' %ankleIkCtrl , '%s.rz' %footBendPivGrp )
            mc.connectAttr( '%s.footRock' %ankleIkCtrl , '%s.rz' %footInPivGrp )
            mc.connectAttr( '%s.footRock' %ankleIkCtrl , '%s.rz' %footOutPivGrp )
            
            mc.transformLimits( footInPivGrp , erz = ( True , False ) , rz = ( 0 , 45 ) )
            mc.transformLimits( footOutPivGrp , erz = ( False , True ) , rz = ( -45 , 0 ) )
        
        #-- Hierarchy
        mc.parent( legIkGrp , legRigGrp )
        mc.parent( legIkJntGrp , legJntGrp )
        mc.parent( legIkIkhGrp , ikhGrp )
        mc.parent( upLegIkZro , ikStrt , crv , legIkGrp )
        mc.parent( ankleIkZro , kneeIkZro , upLegIkGmbl )
        mc.parent( ankleIkhZro , legIkIkhGrp )
        
        if foot == True :
            mc.parent( footPivGrp , ankleIkGmbl )
            mc.parent( ballIkhZro , toeIkhZro , legIkIkhGrp )
        
        #-- Cleanup
        for grp in ( legIkGrp , legIkJntGrp , legIkIkhGrp , upLegIkZro , ankleIkZro , footPivGrp , footBallPivGrp , footBendPivGrp , footHeelPivGrp , footToePivGrp , footInPivGrp , footOutPivGrp ) :
            core.setLock( grp , True , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
        
        for ctrl in ( upLegIkCtrl , upLegIkGmbl , kneeIkCtrl , ankleIkCtrl , ankleIkGmbl ) :
            core.setLockHide( ctrl , 'sx' , 'sy' , 'sz' , 'v' )
                
        core.setVal( '%s.localWorld' %ankleIkCtrl , 1 )
        
        
    ## Fk Ik Blending ##
    if legFk == True and legIk == True :
        core.addBlendFkIk( legCtrl , upLegFkJnt , upLegIkJnt , upLegJnt )
        core.addBlendFkIk( legCtrl , lowLegFkJnt , lowLegIkJnt , lowLegJnt )
        core.addBlendFkIk( legCtrl , ankleFkJnt , ankleIkJnt , ankleJnt )
        
        if foot == True :
            core.addBlendFkIk( legCtrl , ballFkJnt , ballIkJnt , ballJnt )
            core.addBlendFkIk( legCtrl , toeFkJnt , toeIkJnt , toeJnt )
        
        if mc.objExists( 'leg%sFkIk_%s_rev' %( elem , side )) :
            revFkIk = 'leg%sFkIk_%s_rev' %( elem , side )
            core.connectAttr( '%s.fkIk' %legCtrl , '%s.v' %legIkGrp )
            core.connectAttr( '%s.ox' %revFkIk , '%s.v' %legFkGrp )
            
        core.setVal( '%s.fkIk' %legCtrl , 1 )
            
    elif legFk == True and legIk == False :
        core.parCons( 0 , upLegFkJnt , upLegJnt )
        core.parCons( 0 , lowLegFkJnt , lowLegJnt )
        core.parCons( 0 , ankleFkJnt , ankleJnt )
        
        if foot == True :
            core.parCons( 0 , ballFkJnt , ballJnt )
            core.parCons( 0 , toeFkJnt , toeJnt )
            
    elif legFk == False and legIk == True :
        core.scaleShape( legCtrl , charSize )
        core.parCons( 0 , upLegIkJnt , upLegJnt )
        core.parCons( 0 , lowLegIkJnt , lowLegJnt )
        core.parCons( 0 , ankleIkJnt , ankleJnt )
        
        if foot == True :
            core.parCons( 0 , ballIkJnt , ballJnt )
            core.parCons( 0 , toeIkJnt , toeJnt )
            
            
    ## Add Scale ##
    if foot == True :
        core.addAttr( legCtrl , 'scaleFoot' )
        core.setVal( '%s.scaleFoot' %legCtrl , 1 )
        core.setVal( '%s.segmentScaleCompensate' %ballJnt , 0 )
        
        #-- Rig process
        core.connectAttr( '%s.scaleFoot' %legCtrl , '%s.sx' %ankleJnt )
        core.connectAttr( '%s.scaleFoot' %legCtrl , '%s.sy' %ankleJnt )
        core.connectAttr( '%s.scaleFoot' %legCtrl , '%s.sz' %ankleJnt )
        
        if legFk == True :
            #-- Group
            core.setVal( '%s.segmentScaleCompensate' %ballFkJnt , 0 )
            
            #-- Rig process
            core.connectAttr( '%s.scaleFoot' %legCtrl , '%s.sx' %ballFkSclOfst )
            core.connectAttr( '%s.scaleFoot' %legCtrl , '%s.sy' %ballFkSclOfst )
            core.connectAttr( '%s.scaleFoot' %legCtrl , '%s.sz' %ballFkSclOfst )
            
            core.connectAttr( '%s.scaleFoot' %legCtrl , '%s.sx' %ankleFkJnt )
            core.connectAttr( '%s.scaleFoot' %legCtrl , '%s.sy' %ankleFkJnt )
            core.connectAttr( '%s.scaleFoot' %legCtrl , '%s.sz' %ankleFkJnt )
            
        if legIk == True :
            core.setVal( '%s.segmentScaleCompensate' %ballIkJnt , 0 )
            
            #-- Rig process
            core.connectAttr( '%s.scaleFoot' %legCtrl , '%s.sx' %footPivGrp )
            core.connectAttr( '%s.scaleFoot' %legCtrl , '%s.sy' %footPivGrp )
            core.connectAttr( '%s.scaleFoot' %legCtrl , '%s.sz' %footPivGrp )
            
            core.connectAttr( '%s.scaleFoot' %legCtrl , '%s.sx' %ankleIkJnt )
            core.connectAttr( '%s.scaleFoot' %legCtrl , '%s.sy' %ankleIkJnt )
            core.connectAttr( '%s.scaleFoot' %legCtrl , '%s.sz' %ankleIkJnt )
        
        
    ## Ribbon ##
    if ribbon == True :
        rbnAxis = 'y-'
        rbnAim = ( 0,-1,0 )
        
        if side == 'rgt' :
            rbnUp = ( 0,0,-1 )
            rootTwsAmp = -1
            endTwsAmp = 1
        else :
            rbnUp = ( 0,0,1 )
            rootTwsAmp = 1
            endTwsAmp = -1
            
        rbnUpLegPar  = core.getParent( upLegJnt )
        rbnLowLegPar = core.getParent( lowLegJnt )
        rbnAnklePar  = core.getParent( ankleJnt )
        
        #-- Group
        legRbnGrp = core.transform( 'leg%sRbn_%s_grp' %( elem , side ))
        
        #-- Controls
        legRbnCtrl = core.addCtrl( 'leg%sRbn_%s_ctrl' %( elem , side ) , 'locator' , 'yellow' , jnt = True )
        legRbnZro = core.addGrp( legRbnCtrl )
        core.snapPoint( lowLegJnt , legRbnZro )
        core.snapJntOrient( lowLegJnt , legRbnCtrl )
        mc.parent( legRbnZro , legRbnGrp )
        
        #-- Shape
        core.scaleShape( legRbnCtrl , charSize )
        
        #-- Rig process
        core.parCons( 1 , lowLegJnt , legRbnZro )
        
        rbnUpLeg = rbn.rigRibbon( 'upLeg%s' %elem , ax = rbnAxis , jntRoot = upLegJnt , jntEnd = lowLegJnt , animGrp = legRbnGrp , skinGrp = skinGrp , stillGrp = stillGrp , charSize = charSize )
        rbnLowLeg = rbn.rigRibbon( 'lowLeg%s' %elem ,ax = rbnAxis , jntRoot = lowLegJnt , jntEnd = ankleJnt , animGrp = legRbnGrp , skinGrp = skinGrp , stillGrp = stillGrp , charSize = charSize )
        
        rbnUpLegGrp = rbnUpLeg['rbnGrp']
        rbnUpLegCtrl = rbnUpLeg['rbnCtrl']
        rbnUpLegRootCtrl = rbnUpLeg['rbnRootCtrl']
        rbnUpLegEndCtrl = rbnUpLeg['rbnEndCtrl']
        rbnUpLegRootTwZro = rbnUpLeg['rbnRootTwZro']
        rbnUpLegEndTwZro = rbnUpLeg['rbnEndTwZro']
        rbnUpLegRootTwGrp = rbnUpLeg['rbnRootTwGrp']
        rbnUpLegEndTwGrp = rbnUpLeg['rbnEndTwGrp']
        rbnUpLegCtrlShape = rbnUpLeg['rbnCtrlShape']
        
        rbnLowLegGrp = rbnLowLeg['rbnGrp']
        rbnLowLegCtrl = rbnLowLeg['rbnCtrl']
        rbnLowLegRootCtrl = rbnLowLeg['rbnRootCtrl']
        rbnLowLegEndCtrl = rbnLowLeg['rbnEndCtrl']
        rbnLowLegRootTwZro = rbnLowLeg['rbnRootTwZro']
        rbnLowLegEndTwZro = rbnLowLeg['rbnEndTwZro']
        rbnLowLegRootTwGrp = rbnLowLeg['rbnRootTwGrp']
        rbnLowLegEndTwGrp = rbnLowLeg['rbnEndTwGrp']
        rbnLowLegCtrlShape = rbnLowLeg['rbnCtrlShape']
        
        mc.setAttr( '%s.rootTwistAmp' %rbnUpLegCtrlShape , rootTwsAmp )
        mc.setAttr( '%s.endTwistAmp' %rbnUpLegCtrlShape , endTwsAmp )
        
        mc.setAttr( '%s.rootTwistAmp' %rbnLowLegCtrlShape , rootTwsAmp )
        mc.setAttr( '%s.endTwistAmp' %rbnLowLegCtrlShape , endTwsAmp )
        
        for each in ( rbnUpLegRootTwZro , rbnUpLegRootTwGrp , rbnUpLegEndTwZro , rbnUpLegEndTwGrp , rbnLowLegRootTwZro , rbnLowLegRootTwGrp , rbnLowLegEndTwZro , rbnLowLegEndTwGrp ) :
            core.setRotateOrder( each , 'yzx' )
        
        core.snapPoint( upLegJnt , rbnUpLegGrp )
        mc.delete( mc.aimConstraint( lowLegJnt , rbnUpLegGrp , aim = rbnAim , u = rbnUp , wut = 'objectrotation' , wuo = lowLegTmpJnt , wu = ( 0,0,1 ) ))
        core.snap( upLegJnt , rbnUpLegRootTwZro )
        core.snap( lowLegJnt , rbnUpLegEndTwZro )
        core.parCons( 1 , upLegJnt , rbnUpLegGrp )
        core.pntCons( 0 , upLegJnt , rbnUpLegRootCtrl )
        core.pntCons( 0 , legRbnCtrl , rbnUpLegEndCtrl )
        core.parCons( 1 , rbnUpLegPar , rbnUpLegRootTwZro )
        core.parCons( 1 , rbnLowLegPar , rbnUpLegEndTwZro )
        core.parCons( 0 , upLegJnt , rbnUpLegRootTwGrp )
        core.parCons( 0 , lowLegJnt , rbnUpLegEndTwGrp )
        
        core.snapPoint( lowLegJnt , rbnLowLegGrp )
        mc.delete( mc.aimConstraint( ankleJnt , rbnLowLegGrp , aim = rbnAim , u = rbnUp , wut = 'objectrotation' , wuo = ankleTmpJnt , wu = ( 0,0,1 ) ))
        core.snap( lowLegJnt , rbnLowLegRootTwZro )
        core.snap( ankleJnt , rbnLowLegEndTwZro )
        core.parCons( 1 , lowLegJnt , rbnLowLegGrp )
        core.pntCons( 0 , legRbnCtrl , rbnLowLegRootCtrl )
        core.pntCons( 0 , ankleJnt , rbnLowLegEndCtrl )
        core.parCons( 1 , rbnAnklePar , rbnLowLegEndTwZro )
        core.parCons( 0 , ankleJnt , rbnLowLegEndTwGrp )
        
        #-- Hierarchy
        core.parent( legRbnGrp , legRigGrp )
        
        #-- Cleanup
        for each in ( legRbnGrp , legRbnZro , rbnUpLegGrp , rbnLowLegGrp , rbnUpLegRootCtrl , rbnUpLegEndCtrl , rbnLowLegRootCtrl , rbnLowLegEndCtrl , rbnUpLegRootTwZro , rbnUpLegEndTwZro , rbnUpLegRootTwGrp , rbnUpLegEndTwGrp , rbnLowLegRootTwZro , rbnLowLegEndTwZro , rbnLowLegRootTwGrp , rbnLowLegEndTwGrp ) :
            core.setLock( each , True , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
        
        for each in ( rbnUpLegRootCtrl , rbnUpLegEndCtrl , rbnLowLegRootCtrl , rbnLowLegEndCtrl ) :
            core.setVal( '%s.v' %each , 0 )
        
        for each in ( rbnUpLegCtrl , rbnLowLegCtrl ) :
            core.setVal( '%s.autoTwist' %each , 1 )
        
        core.setLockHide( legRbnCtrl , 'sx' , 'sy' , 'sz' , 'v' )
    
    mc.select( cl = True )