import maya.cmds as mc
import maya.mel as mm
import ppmel.core as core
reload( core )


def rigCtrl( obj = '' , shape = '' , col = '' ) :
    # obj = mc.ls( sl = True )[0]
    if '_' in obj :
        prefix = obj.split( '_' )[0]
        name = '%s' %prefix
    else :
        name = '%s' %obj
    
    side = core.getSide( obj )
    
    ctrl = core.addCtrl( '%s%s_ctrl' %( name , side ) , shape , col , obj )
    ctrlZro = core.addGrp( ctrl )
    ctrlCons = core.addCons( ctrl )
    ctrlGmbl = core.addGimbal( ctrl )
    
    core.parCons( 0 , ctrlGmbl , obj )
    #core.sclCons( 0 ,ctrlGmbl , obj )
    
    mc.select( cl = True )