import maya.cmds as mc
import maya.mel as mm
import ppmel.core as core
reload( core )

def rigHairDyn( name = 'hairA' ,
                tmpJnt = [ 'hair1_tmpJnt' , 'hair2_tmpJnt' , 'hair3_tmpJnt' ,
                           'hair4_tmpJnt' , 'hair5_tmpJnt' , 'hair6_tmpJnt' ] ,
                parent = 'joint1' ,
                animGrp = 'anim_grp' ,
                skinGrp = 'skin_grp' ,
                jntGrp = 'jnt_grp' ,
                stillGrp = 'still_grp' ,
                ikhGrp = 'ikh_grp' ,
                side = 'lft' ,
                elem = 'front' ,
                charSize = 1 ) :
                   
    #-- Info
    size = len(tmpJnt)
    elem = elem.capitalize()
    
    if side :
        side = '_%s_' %side
    else :
        side = '_'
    
    ## Main Rig ##
    #-- Group
    hairRigGrp = core.transform( '%s%sRig%sgrp' %( name , elem , side ))
    hairJntGrp = core.transform( '%s%sJnt%sgrp' %( name , elem , side ))
    core.parCons( 0 , parent , hairRigGrp )
    core.sclCons( 0 , parent , hairRigGrp )
    core.snap( parent , hairJntGrp )
    
    #-- Joint
    mainJntArray = []
    
    for i in range(0,size) :
        mainJnt = core.addJnt( '%s%s%s%sjnt' %( name , i+1 , elem , side ) , tmpJnt[i] )
        mainJntArray.append(mainJnt)
        
        #-- Hierarchy
        if i == 0 :
            core.parent( mainJnt , parent )
        if not i == 0 :
            core.parent( mainJnt , mainJntArray[i-1] )
    
    
    ## Fk Rig ##
    #-- Group
    hairFkRigGrp = core.transform( '%s%sFkCtrl%sgrp' %( name , elem , side ))
    hairFkJntGrp = core.transform( '%s%sFkJnt%sgrp' %( name , elem , side ))
    core.snap( hairRigGrp , hairFkRigGrp )
    core.snap( hairRigGrp , hairFkJntGrp )
    core.parent( hairFkRigGrp , hairRigGrp )
    core.parent( hairFkJntGrp , hairJntGrp )
    
    #-- Joint
    fkJntArray = []
    fkCtrlArray = []
    fkGmblArray = []
    fkZroArray = []
    fkOfstArray = []
    
    for i in range(0,size) :
        fkJnt = core.addJnt( '%s%s%sFk%sjnt' %( name , i+1 , elem , side ) , tmpJnt[i] )
        fkJntArray.append(fkJnt)
        
        #-- Hierarchy
        if i == 0 :
            core.parent( fkJnt , hairFkJntGrp )
        if not i == 0 :
            core.parent( fkJnt , fkJntArray[i-1] )
        
        #-- Controls
        if not i == size-1 :
            fkCtrl = core.addCtrl( '%s%sFk%sctrl' %(name , i+1 , side) , 'circle' , 'red' , jnt = False )
            fkGmbl = core.addGimbal( fkCtrl )
            fkZro = core.addGrp( fkCtrl )
            fkOfst = core.addGrp( fkCtrl , 'Ofst' )
            core.snap( fkJnt , fkZro )
            
            fkCtrlArray.append(fkCtrl)
            fkGmblArray.append(fkGmbl)
            fkZroArray.append(fkZro)
            fkOfstArray.append(fkOfst)
        
        #-- Shape
        for ctrl in ( fkCtrl , fkGmbl ) :
            core.scaleShape( ctrl , charSize )
                    
        #-- Hierarchy
        if i == 0 :
            if not i == size-1 :
                core.parent( fkZro , hairFkRigGrp )
        else :
            if not i == size-1 :
                core.parent( fkZro , fkGmblArray[i-1] )
        
        #-- Rig process
        if not i == size-1 :
            core.parCons( 0 , fkGmbl , fkJnt )
            core.sclCons( 0 , fkGmbl , fkJnt )
        
    core.addLocalWorld( fkCtrlArray[0] , 'anim_grp' , hairFkRigGrp , fkZroArray[0] , 'orient' )
        
    #-- Cleanup
    for ctrl in fkCtrlArray :
        core.setLockHide( ctrl , 'v' )
    
    for gmbl in fkGmblArray :
        core.setLockHide( gmbl , 'v' )
    
    
    ## Ik Rig ##
    #-- Group
    hairIkRigGrp = core.transform( '%s%sIkCtrl%sgrp' %( name , elem , side ))
    hairIkJntGrp = core.transform( '%s%sIkJnt%sgrp' %( name , elem , side ))
    core.snap( hairRigGrp , hairIkRigGrp )
    core.snap( hairRigGrp , hairIkJntGrp )
    core.parent( hairIkRigGrp , hairRigGrp )
    core.parent( hairIkJntGrp , hairJntGrp )
    
    #-- Joint
    ikJntArray = []
    ikCtrlArray = []
    ikGmblArray = []
    ikZroArray = []
    ikOfstArray = []
    
    for i in range(0,size) :
        ikJnt = core.addJnt( '%s%s%sIk%sjnt' %( name , i+1 , elem , side ) , tmpJnt[i] )
        ikJntArray.append(ikJnt)
        
        #-- Hierarchy
        if i == 0 :
            core.parent( ikJnt , hairIkJntGrp )
        if not i == 0 :
            core.parent( ikJnt , ikJntArray[i-1] )
    
    #-- Controls
    ikCtrl = core.addCtrl( '%sIk%sctrl' %(name , side) , 'cube' , 'blue' , jnt = False )
    ikZro = core.addGrp( ikCtrl )
    ikOfst = core.addGrp( ikCtrl , 'Ofst' )
    core.snap( ikJntArray[0] , ikZro )
    
    core.addAttrType( ikCtrl , 'HairDynamic' )
    mc.addAttr( ikCtrl , ln = 'fkIk' , nn = 'On Off' , at = 'float' , k = True , min = 0 , max = 1 , dv = 0 )
    mc.addAttr( ikCtrl , ln = 'startFrame' , at = 'float' , k = True , dv = 1 )
    mc.addAttr( ikCtrl , ln = 'siffness' , at = 'float' , k = True , min = 0 , max = 1 , dv = 0.15 )
    mc.addAttr( ikCtrl , ln = 'startCurveAttract' , at = 'float' , k = True , min = 0 , max = 1 , dv = 0 )
    mc.addAttr( ikCtrl , ln = 'attractionDamp' , at = 'float' , k = True , min = 0 , max = 1 , dv = 0 )
    
    core.addAttrType( ikCtrl , 'Forces' )
    mc.addAttr( ikCtrl , ln = 'mass' , at = 'float' , k = True , min = 0 , dv = 1)
    mc.addAttr( ikCtrl , ln = 'drag' , at = 'float' , k = True , min = 0 , dv = 0.05 )
    mc.addAttr( ikCtrl , ln = 'damp' , at = 'float' , k = True , min = 0 , dv = 0 )
    mc.addAttr( ikCtrl , ln = 'gravity' , at = 'float' , k = True , min = 0 , dv = 0.980 )
    
    core.addAttrType( ikCtrl , 'Turbulence' )
    mc.addAttr( ikCtrl , ln = 'intensity' , at = 'float' , k = True , min = 0 )
    mc.addAttr( ikCtrl , ln = 'frequency' , at = 'float' , k = True , min = 0 )
    mc.addAttr( ikCtrl , ln = 'speed' , at = 'float' , k = True , min = 0 )
    
    
    #-- Rig process
    # Make curve for hair dynamic #
    ikhCuv = mc.ikHandle( n = '%s%sikh' %( name , side ) , sol = 'ikSplineSolver' , sj = ikJntArray[0] , ee = ikJntArray[-1] , scv = False )
    dynCuv = mc.duplicate( ikhCuv[-1] , n = '%s%sFollicles%scuv' %( name , elem , side ))[0]
    mc.delete(ikhCuv)
    
    # Create nodes needed for simulation #
    dynOutputCuvShape = mc.createNode( 'nurbsCurve' , n = '%s%sOutput%scuvShape' %( name , elem , side ))
    dynOutputCuv = core.getParent( dynOutputCuvShape )[0]
    hairSysShape = mc.createNode( 'hairSystem' , n = '%s%s%shairSystemShape' %( name , elem , side ))
    hairSys = core.getParent( hairSysShape )[0]
    nucleus = mc.createNode( 'nucleus' , n = '%s%s%snucleus' %( name , elem , side ))
    follicleShape = mc.createNode( 'follicle' , n = '%s%sFollicles%sfollicleShape' %( name , elem , side ))
    follicle = core.getParent( follicleShape )[0]
    mc.setAttr('%s.restPose' %follicleShape , 1 )
    mc.setAttr('%s.degree' %follicleShape , 3 )
    
    # Connect nodes to set up simulation #
    # Rebuild curve
    rebuildDynCuv = mc.createNode( 'rebuildCurve', n = '%s%s%srebuildCuv' %( name , elem , side ))
    rebuiltDynOutputCuvShape = mc.createNode( 'nurbsCurve', n = '%s%s%srebuiltCurveShape' %( name , elem , side ))
    rebuiltDynOutputCuv = core.getParent( rebuiltDynOutputCuvShape )[0]
    
    # Generate curve output
    mc.connectAttr( '%sShape.worldSpace[0]' %dynCuv , '%s.inputCurve' %rebuildDynCuv )
    mc.connectAttr( '%s.outputCurve' %rebuildDynCuv , '%s.create '%rebuiltDynOutputCuvShape )
    
    # Connect curves to follicle
    mc.connectAttr( '%s.worldMatrix[0]' %dynCuv , '%s.startPositionMatrix' %follicleShape )
    mc.connectAttr( '%s.local' %rebuiltDynOutputCuvShape , '%s.startPosition' %follicleShape )
    
    # Connect follicle to output curve
    mc.connectAttr( '%s.outCurve' %follicleShape , '%s.create' %dynOutputCuvShape )
    
    # Connect time to hair system and nucleus
    mc.connectAttr( 'time1.outTime' , '%s.currentTime' %nucleus )
    mc.connectAttr( 'time1.outTime' , '%s.currentTime' %hairSysShape )
    
    # Connect hair system and nucleus together
    mc.connectAttr( '%s.currentState' %hairSysShape , '%s.inputActive[0]' %nucleus )
    mc.connectAttr( '%s.startState' %hairSysShape , '%s.inputActiveStart[0]' %nucleus )
    mc.connectAttr( '%s.outputObjects[0]' %nucleus , '%s.nextState' %hairSysShape )
    mc.connectAttr( '%s.startFrame' %nucleus , '%s.startFrame' %hairSysShape )
            
    # Connect hair system to follicle
    mc.connectAttr( '%s.outputHair[0]' %hairSysShape , '%s.currentPosition' %follicleShape )
    mc.connectAttr( '%s.outHair' %follicleShape , '%s.inputHair[0]' %hairSysShape )    
    
    hairFolGrp = mc.createNode( 'transform' , n = '%s%sFollicles%sgrp' %( name , elem , side ))
    hairSysOutputGrp = mc.createNode( 'transform' , n = '%s%sOutput%sgrp' %( name , elem , side ))
    
    # Connect Attribute
    mc.connectAttr( '%s.startFrame' %ikCtrl ,  '%s.startFrame' %nucleus )
    mc.connectAttr( '%s.siffness' %ikCtrl ,  '%s.stiffness' %hairSysShape )
    mc.connectAttr( '%s.startCurveAttract' %ikCtrl ,  '%s.startCurveAttract' %hairSysShape )
    mc.connectAttr( '%s.attractionDamp' %ikCtrl ,  '%s.attractionDamp' %hairSysShape )
    
    mc.connectAttr( '%s.mass' %ikCtrl ,  '%s.mass' %hairSysShape )
    mc.connectAttr( '%s.drag' %ikCtrl ,  '%s.drag' %hairSysShape )
    mc.connectAttr( '%s.damp' %ikCtrl ,  '%s.damp' %hairSysShape )
    mc.connectAttr( '%s.gravity' %ikCtrl ,  '%s.gravity' %hairSysShape )
    
    mc.connectAttr( '%s.intensity' %ikCtrl ,  '%s.turbulenceStrength' %hairSysShape )
    mc.connectAttr( '%s.frequency' %ikCtrl ,  '%s.turbulenceFrequency' %hairSysShape )
    mc.connectAttr( '%s.speed' %ikCtrl ,  '%s.turbulenceSpeed' %hairSysShape )    
    
    # Constraint
    core.parCons( 1 , ikCtrl , hairFolGrp )
    core.sclCons( 1 , ikCtrl , hairFolGrp )
    
    core.parCons( 1 , parent , hairJntGrp )
    core.sclCons( 1 , parent , hairJntGrp )
    
    # Hierarchy
    core.parent( dynCuv , follicle , hairFolGrp )
    core.parent( rebuiltDynOutputCuv , follicle)
    core.parent( dynOutputCuv , hairSysOutputGrp )
    core.parent( ikZro , hairIkRigGrp )
    
    hairSysGrp = core.transform( '%s%sHairSystem%sgrp' %( name , elem , side ))
    nucleusGrp = core.transform( '%s%sNucleus%sgrp' %( name , elem , side ))
    mc.parent( hairSys , hairSysGrp )
    mc.parent( nucleus , nucleusGrp )
    
    # CleanUp
    mc.setAttr( '%s.v' %rebuiltDynOutputCuv , 0 )
    
    core.setLockHide( ikCtrl , 'sx' )
    core.setLockHide( ikCtrl , 'sy' )
    core.setLockHide( ikCtrl , 'sz' )
    core.setLockHide( ikCtrl , 'v' )
    
    # Create Ikh Spine #
    hairIkhSpineGrp = core.transform( '%s%sIkh%sgrp' %( name , elem , side ))
    ikhSpline = mc.ikHandle(sj = ikJntArray[0] , ee = ikJntArray[-1] , c = dynOutputCuv, n = '%s%s%sikh' %( name , elem , side ) , sol = 'ikSplineSolver' , ccv = False , rootOnCurve = True , parentCurve = False , ns = mc.getAttr( '%s.spans' %dynCuv ))
    mc.parent( ikhSpline[0] , hairIkhSpineGrp )
    
    # Blend Fk Ik #
    for i in range(0,size) :
        core.addBlendFkIk( ikCtrl , fkJntArray[i] , ikJntArray[i] , mainJntArray[i] )
        
    # CleanUp Hierarchy #
    mc.parent( hairRigGrp , animGrp )
    mc.parent( hairJntGrp , jntGrp )
    mc.parent( hairIkhSpineGrp , ikhGrp )
    mc.parent( hairSysGrp , hairFolGrp , hairSysOutputGrp , nucleusGrp , stillGrp )