import maya.cmds as mc
import os
import pickle
import pkmel.core as pc
reload( pc )

def getDataFld():

    scnPath = os.path.normpath(mc.file(q=True, l=True)[0])
    fldPath = os.path.split(scnPath)[0]

    dataFld = os.path.join(fldPath, 'rigData')

    if not os.path.exists(dataFld):
        os.mkdir(dataFld)

    return dataFld
    
def writeCtrlShape(fn='ctrlShape.dat'):

    flPath = os.path.join(getDataFld(), fn)
    flObj = open(flPath, 'w')
    
    ctrlDict = {}
    
    for ctrl in mc.ls('*Ctrl'):
        
        shapes = mc.listRelatives(ctrl, s=True)

        if not shapes: continue # Continue if current controller has no shape.
        
        if mc.nodeType(shapes[0]) == 'nurbsCurve':
            
            cv = mc.getAttr('%s.spans' % shapes[0]) + mc.getAttr('%s.degree' % shapes[0])
        
            for ix in range(0, cv):
                cvName = '%s.cv[%s]' % (shapes[0], str(ix))
                ctrlDict[cvName] = mc.xform(cvName, q = True, os = True, t = True)

            # Write color property
            if mc.getAttr('%s.overrideEnabled' % shapes[0]):
                colVal = mc.getAttr('%s.overrideColor' % shapes[0])
                ctrlDict[shapes[0]] = colVal
        
    pickle.dump(ctrlDict, flObj)
    flObj.close()

def readCtrlShape(fn='ctrlShape.dat', searchFor='', replaceWith='', prefix='', suffix=''):

    flPath = os.path.join(getDataFld(), fn)
    flObj = open(flPath, 'r')
    ctrlDict = pickle.load(flObj)
    flObj.close()

    for key in ctrlDict.keys():

        curr = '%s%s%s' % (prefix, key.replace(searchFor, replaceWith), suffix)

        if '.' in curr:
            # If current is cv, read the position.
            if mc.objExists(curr):
                mc.xform(curr, os=True, t=ctrlDict[curr])
        else:
            # If current is object, read the color.
            if mc.objExists(curr):
                mc.setAttr('%s.overrideEnabled' % curr, 1)
                mc.setAttr('%s.overrideColor' % curr, ctrlDict[curr])