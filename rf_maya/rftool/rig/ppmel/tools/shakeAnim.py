import maya.cmds as mc
import maya.mel as mm
import random


def shakeAnim( ctrl = 'pSphere1' , attr = 'ty' , stFrame = '' , enFrame = '' feq = '' , val = 10 ) :
    
    animCrv = mc.ls( mc.listConnections( '%s.%s' %(ctrl,attr)) , type = 'animCurve' )
    
    if animCrv :
        
        i = stFrame+val
        while i <= enFrame :
            mc.setKeyframe( animCrv[0] , insert = True , time = i )
            i+=val
            
        i = stFrame+val
        while i <= enFrame :
            currVal = mc.getAttr( '%s.%s' %(ctrl,attr) , time = 11 )
            randVal = random.uniform(currVal,currVal+val)