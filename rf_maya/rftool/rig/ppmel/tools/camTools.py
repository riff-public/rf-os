import maya.mel as mm
import maya.cmds as mc

def camToolsUI() :
    
    if mc.window( 'camTool' , exists = True ) :
        mc.deleteUI( 'camTool' , window = True )
    
    mc.window( 'camTool' , t = 'Camera Tool' )
    form = mc.formLayout( w = 250 , h = 180 )
    
    camNameTX = mc.text( l = 'Camera Name  :' , al = 'left' )
    camNameTF = mc.textField( 'camNameTF' , en = 0 , w = 160 )
    getCameraBT = mc.button( 'getCameraBT' , l = 'Get Camera' , h = 25 , w = 245 , c = 'getCam()' )
    horizontalTx = mc.text( l = 'Horizontal  :' , al = 'left' )
    horizontalSD = mc.floatSlider( 'horizontalSD' ,  min = -10 , max = 10 , value = 0 , w = 175  , dc = 'horizontalValue()' )
    verticalTx = mc.text( l = 'Vertical  :' , al = 'left' )
    verticalSD = mc.floatSlider( 'verticalSD' ,  min = -10 , max = 10 , value = 0 , w = 175  , dc = 'verticalValue()' )
    zoomTx = mc.text( l = 'Zoom  :' , al = 'left' )
    zoomSD = mc.floatSlider( 'zoomSD' ,  min = 0.001 , max = 15 , value = 1 , w = 175  , dc = 'zoomValue()' )
    resetBT = mc.button( 'resetBT' , l = 'Reset' , h = 25 , w = 245 , c = 'resetValue()' )
    ampTx = mc.text( l = 'Amplitude  :' , al = 'left' )
    ampSD = mc.floatSlider( 'ampSD' ,  min = 1 , max = 20 , value = 1 , w = 175 )
    
    mc.formLayout( form , e = True , attachForm = [ ( camNameTX , 'top' , 10 ) ,
                                                    ( camNameTX , 'left' , 5 ),
                                                    ( camNameTF , 'top' , 7 ) ,
                                                    ( getCameraBT , 'left' , 3 ) ,
                                                    ( horizontalTx , 'left' , 5 ) ,
                                                    ( verticalTx , 'left' , 19 ) ,
                                                    ( zoomTx , 'left' , 27 ) ,
                                                    ( resetBT , 'left' , 3 ) ,
                                                    ( ampTx , 'left' , 5 ) ,
                                                  ]
                                   , attachControl = [ ( camNameTF , 'left' , 5 , camNameTX ) ,
                                                       ( getCameraBT , 'top' , 5 , camNameTF ) ,
                                                       ( horizontalTx , 'top' , 5 , getCameraBT ) ,
                                                       ( horizontalSD , 'top' , 6 , getCameraBT ) ,
                                                       ( horizontalSD , 'left' , 7 , horizontalTx ) ,
                                                       ( verticalTx , 'top' , 9 , horizontalTx ) ,
                                                       ( verticalSD , 'top' , 7 , horizontalSD ) ,
                                                       ( verticalSD , 'left' , 7 , verticalTx ) ,
                                                       ( zoomTx , 'top' , 9 , verticalTx ) ,
                                                       ( zoomSD , 'top' , 7 , verticalSD ) ,
                                                       ( zoomSD , 'left' , 7 , zoomTx ) ,
                                                       ( resetBT , 'top' , 5 , zoomSD ) ,
                                                       ( ampTx , 'top' , 5 , resetBT ) ,
                                                       ( ampSD , 'top' , 6 , resetBT ) ,
                                                       ( ampSD , 'left' , 7 , ampTx ) ,
                                                      ] )
    
    mc.showWindow( 'camTool' )

def getCam() :
    sel = mc.ls ( sl = True )[0]
    mc.textField( 'camNameTF' , e = True , tx = sel )

def horizontalValue() :
    camName = mc.textField( 'camNameTF' , q = True , tx = True )
    amp = mc.floatSlider( 'ampSD' ,  q = True , v = True )
    value = mc.floatSlider( 'horizontalSD' ,  q = True , v = True )
    camNameShape = mc.listRelatives( camName , shapes = True )[0]

    try : mc.setAttr( '%s.filmTranslateH' %camNameShape , value*amp )
    except RuntimeError : mc.warning( 'Please Get Camera First.' )
    

def verticalValue() :
    camName = mc.textField( 'camNameTF' , q = True , tx = True )
    amp = mc.floatSlider( 'ampSD' ,  q = True , v = True )
    value = mc.floatSlider( 'verticalSD' ,  q = True , v = True )
    camNameShape = mc.listRelatives( camName , shapes = True )[0]
    
    try : mc.setAttr( '%s.filmTranslateV' %camNameShape , value*amp )
    except RuntimeError : mc.warning( 'Please Get Camera First.' )

def zoomValue() :
    camName = mc.textField( 'camNameTF' , q = True , tx = True )
    amp = mc.floatSlider( 'ampSD' ,  q = True , v = True )
    value = mc.floatSlider( 'zoomSD' ,  q = True , v = True )
    camNameShape = mc.listRelatives( camName , shapes = True )[0]
    
    try : mc.setAttr( '%s.preScale' %camNameShape , value*amp )
    except RuntimeError : mc.warning( 'Please Get Camera First.' )
    
def resetValue() :
    camName = mc.textField( 'camNameTF' , q = True , tx = True )
    camNameShape = mc.listRelatives( camName , shapes = True )[0]
    
    mc.setAttr( '%s.filmTranslateH' %camNameShape , 0 )
    mc.setAttr( '%s.filmTranslateV' %camNameShape , 0 )
    mc.setAttr( '%s.preScale' %camNameShape , 1 )
    
    mc.floatSlider( 'horizontalSD' ,  e = True , v = 0 )
    mc.floatSlider( 'verticalSD' ,  e = True , v = 0 )
    mc.floatSlider( 'zoomSD' ,  e = True , v = 1 )
    mc.floatSlider( 'ampSD' ,  e = True , v = 1 )