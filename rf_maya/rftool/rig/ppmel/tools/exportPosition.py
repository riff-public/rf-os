import maya.cmds as mc 
import maya.mel as mm 
import os

def run() :
    if mc.window( 'exportPositionUI' , exists = True ) :
        mc.deleteUI( 'exportPositionUI' , window = True )
        
    window = mc.window( 'exportPositionUI' , title = 'Export Position v.1.00' )
    mc.columnLayout ( w = 250 , h = 96 , rs = 2 , adjustableColumn = True )
    mc.button( h = 30 , w = 238 , l = 'Export Locator' , c = exportLoc )
    mc.button( h = 30 , w = 238 , l = 'Export Origin'  , c = exportOrigin )
    mc.button( h = 30 , w = 238 , l = 'Import Position'  , c = importPosition )
    mc.showWindow ( window )

def exportLoc( args = None ):
    sels = mc.ls( sl = True )
    
    if sels :
        locGrp = mc.createNode( 'transform' , n = 'locPosition_grp' )
        
        for sel in sels :
            name = sel.split('_')[0]
            loc = mc.spaceLocator( n = '%s_loc' %name )
            mc.delete( mc.parentConstraint ( sel , loc , mo = False ))
            mc.parent( loc , locGrp )
        
        fileName = mc.file( q = True , sn = True ).split('/')[-1]
        name = fileName.split('_')[0]
        path = getDataFld()
        exportName = '%sLocPosi.ma' %name
        
        mc.select( locGrp )
        mc.file( r'%s/%s' %( path , exportName ) , type = 'mayaAscii' , force = True , es = True , pr = True )
        mc.delete( locGrp )
        
        writePathTxt( '%s/%s' %( path , exportName ))

        mc.select(sels)

        print 'Export  ' + '%s/%s' %( path , exportName) + '  is Done!' 
        
    else :
        mc.warning( 'Plese Select Object' )
    
def getDataFld() :
    
    wfn = mc.file( q = True , sn = True )
    
    tmpAry = wfn.split( '/' )
    tmpAry[-3] = 'data'
    
    dataFld = '/'.join( tmpAry[0:-2] )
    
    if not os.path.isdir( dataFld ) :
        os.mkdir( dataFld )
    
    return dataFld

def writePathTxt( dataPath = '' ) :
    
    wfn = mc.file( q = True , sn = True )
    tmpAry = wfn.split( '/' )
    name = tmpAry[-1].split('_')[0]
    tmpAry[-6] = 'setDress'
    
    pathTxt = '/'.join( tmpAry[0:-5] )+'/%s' %name
    
    if not os.path.exists( pathTxt ) :
        os.makedirs( pathTxt )
        
    dataTxt = open( "%s/dataPath.txt" %pathTxt , "w" )
    dataTxt.write( "%s" %dataPath )
    dataTxt.close()

def exportOrigin( args = None ) : 
    
    sels = mc.ls( sl = True )
    
    if sels :
        locOrigin = mc.spaceLocator( n = 'origin_loc' )
        
        for sel in sels :
            locLocal = mc.spaceLocator( n = 'local_loc' )
            mc.delete(mc.parentConstraint( sel , locLocal , mo = False ))
            
            mc.delete(mc.parentConstraint( locOrigin , sel , mo = False ))
            
            path = getAbcFld( sel )
            objName = sel.split('_')[0]
            
            mc.select( sel )
            
            cacheName = '%sOrigin.abc' %objName
            cachePath = r'%s/%s' %( path , cacheName )
            
            command = 'AbcExport -j "-frameRange 0 0 '
            command += '-stripNamespaces -uvWrite -writeVisibility -dataFormat ogawa '
            command += '-root %s ' %sel
            command += '-file %s";' %cachePath
            
            mm.eval( command )
            
            mc.delete(mc.parentConstraint( locLocal , sel , mo = False ))
            mc.delete( locLocal )
        
        mc.delete( locOrigin )
        
    else :
        mc.warning('Please select object')
     
def getAbcFld( sel = '' ) :
    
    wfn = mc.file( q = True , sn = True )
    fileName = wfn.split('/')[-1]
    name = fileName.split('_')[0]
    objName = sel.split('_')[0]
    
    tmpAry = wfn.split( '/' )
    tmpAry[-6] = 'setDress'
    
    dataFld = '/'.join( tmpAry[0:-5] )+'/%s/%s/alembic' %( name , objName )
    
    if not os.path.exists( dataFld ) :
        os.makedirs( dataFld )
    
    return dataFld

def importPosition( args = None ) :
    
    pathFile = mc.file( q = True , sn = True )
    
    if pathFile :
        tmpAry = pathFile.split( '/' )
        name = tmpAry[-1].split('_')[0]
        tmpAry[-6] = 'setDress'
        
        pathTxt = '/'.join( tmpAry[0:-5] )+ '/%s/dataPath.txt' %name
        
        text = open( '%s' %pathTxt , 'r' )
        pathData = text.read()
        
        sels = mc.ls( sl = True )
        
        if sels :
            mc.file( '%s' %pathData , r = True , type = 'mayaAscii' , gl = True )
            
            for sel in sels:
                name = sel.split( '_ply' )[0]
                mc.delete(mc.parentConstraint( '*_%s_loc' %name , sel , mo = False ))
                
            mc.file( '%s' %pathData , removeReference = True )
        
        else:
            mc.warning('Please select object')
    else :
        mc.warning('Please Save File')


def exportPositionRun() :
    run()