import maya.cmds as mc
import maya.mel as mm

def convertNaming() :
    # rig_grp rename to root
    srpn = 'searchReplaceNames "rig_grp" "root" "all";'
    mm.eval( srpn )
    print 'rename "rig_grp" to "root" is done!'
    
    # mesh rename suffix "_ply" to "_geo"
    srpn = 'searchReplaceNames "_ply" "_geo" "all";'
    mm.eval( srpn )
    print 'rename "_ply" to "_geo" is done!'
    
    # group rename suffix "_grp" to "_GP"
    mc.select( sel )
    srpn = 'searchReplaceNames "_grp" "_GP" "all";'
    mm.eval( srpn )
    print 'rename "_grp" to "_GP" is done!'
    
    # lef , rgt rename to L , R side = 'lft'
    srpn = 'searchReplaceNames "_lft_" "L_" "all";'
    mm.eval( srpn )
    print 'rename "lft" to "L" is done!'
    
    srpn = 'searchReplaceNames "_rgt_" "R_" "all";'
    mm.eval( srpn )
    print 'rename "rgt" to "R" is done!'