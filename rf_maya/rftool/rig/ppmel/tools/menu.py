import os, shutil, subprocess, sys
import maya.cmds as mc
import maya.utils as utils
        

###  Create Menu UI  ###
def run():
    
    if mc.menu( 'piipezMenu', exists=1 ):
        mc.deleteUI( 'piipezMenu' )
    
    ###  Change Script Folder  ###
    scriptsFolder = 'C:/Users/Piipez/Documents/maya/scripts'

    if scriptsFolder in sys.path:
        print ''
    else:
        sys.path.append( scriptsFolder )
    
    scriptsMenu = mc.menu( 'piipezMenu' , p = 'MayaWindow' , to = 1 , aob = 1 , l = 'Piipez Tools' )
    mc.menuItem( p = scriptsMenu , l = "Update Menu" , c = 'run()' )
    mc.menuItem( p = scriptsMenu , d = 1 )
        
    absoluteFiles = []
    relativeFiles = []
    folders = []
    allFiles = []
    currentFile = ''
        
    for root, dirs, files in os.walk( scriptsFolder ):
        for x in files:
            correct = root.replace( '\\', '/' )
            currentFile = ( correct + '/' + x )
            allFiles.append( currentFile )
            if currentFile.endswith( '.mel' ) :
                relativeFiles.append( currentFile.replace (( scriptsFolder + '/' ) , "" ))
            if currentFile.endswith( '.py' ) :
                relativeFiles.append( currentFile.replace (( scriptsFolder + '/' ) , "" ))
                        
    relativeFiles.sort()
    
    for relativeFile in relativeFiles:
        split = relativeFile.split( '/' )
        fileName = split[-1].split( '.' )
        i = 0
        while i < ( len ( split)) :
            ### Create Folders ###
            if i==0 and len( split ) != 1 :
                if mc.menu( split[i] , ex = 1 ) == 0 :
                    mc.menuItem( split[i] , p = scriptsMenu , bld = 1 , sm = 1 , to = 1 , l = split[i] )
            if i > 0 and i < ( len ( split ) -1 ) :
                if mc.menu( split[i] , ex=1 ) == 0 :
                    mc.menuItem( split[i] , p = split[i-1] , bld = 1 , sm = 1 , to = 1 , l = split[i] )
            
            ### Create .mel Files  ###
            if fileName[-1] == 'mel' :
                if i == len( split ) -1 and len( split ) > 1 :
                    scriptName = split[-1].split( '.' )
                    temp1 = 'source ' + '"' + scriptsFolder + '/' + relativeFile + '"; ' + scriptName[0]
                    command = '''mmeval( ''' + "'" + temp1 + '''' )'''
                    mc.menuItem( split[i] , p = split[i-1] , c = command , l = split[i] )
                if i == len( split ) -1 and len( split ) == 1 :
                    scriptName = split[-1].split( '.' )
                    temp1 = 'source ' + '"' + scriptsFolder + '/' + relativeFile + '"; ' + scriptName[0]
                    command = '''mmeval( ''' + "'" + temp1 + '''' )'''
                    mc.menuItem( split[i] , p = scriptsMenu , c = command , l = split[i] )
                
            ### Create .py Files  ###
            if fileName[-1] == 'py' :
                if i == len( split ) -1 and len( split ) > 1 :
                    command = 'import ' + fileName[0] + '\n' + fileName[0] + '.' + fileName[0]+ '()'
                    mc.menuItem( split[i] , p = split[i-1] , c = command , l = split[i] )
                if i == len( split ) -1 and len( split ) == 1 :
                    command = 'import ' + fileName[0] + '\n' + fileName[0] + '.' + fileName[0]+ '()'
                    mc.menuItem( split[i] , p = scriptsMenu , c = command , l = split[i] )
            i+=1