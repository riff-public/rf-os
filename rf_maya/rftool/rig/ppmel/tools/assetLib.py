import maya.cmds as mc
import maya.mel as mm

class AssetNamingGUI():
    
    def __init__(self):
        if mc.window( 'assetNamingGUI' , exists = True ) :
            mc.deleteUI( 'assetNamingGUI' , window = True )
        
        mc.window( 'assetNamingGUI' , t = 'Asset Naming v.1' )
        self.form = mc.formLayout ( w = 650 , h = 750 )
        self.projOM = mc.optionMenu ( 'projOM' , label = 'Project' , w = 273 , h = 23 )
        self.browseBT = mc.button( l = 'Browse' , w = 60 , h = 23 )
        self.pathCurrTX = mc.text( l = '' , al = 'left' )
        self.categoryOM = mc.optionMenu ( 'categoryOM' , label = 'Category' , w = 273 , h = 23 )
        self.categoryAddBT = mc.button( 'categoryAddBT' , l = '+' , w = 23 , h = 23 )
        self.typeOM = mc.optionMenu ( 'typeOM' , label = 'Type' , w = 273 , h = 23 )
        self.typeAddBT = mc.button( 'typeAddBT' , l = '+' , w = 23 , h = 23 )
        self.separatorA = mc.separator( w = 630 , h = 5 , style = 'in' )
        self.thumbPT = mc.picture( image = '' , w = 250 , h = 250 , bgc = (0.2,0.2,0.2) )
        self.captureBT = mc.button( 'captureBT' , l = 'Create Thumbnail 250 x 250 pixel' , w = 250 , h = 30 )
        self.listAssetTS = mc.textScrollList ( 'listAssetTS' , ams = False , w = 250 , h = 300 )
        self.assetNameTF = mc.textField( 'assetNameTF' , en = True , w = 250 , h = 25 )
        self.createAssetBT = mc.button( 'createAssetBT' , l = 'Create Asset' , w = 250 , h = 30 )
        self.separatorB = mc.separator( w = 5 , h = 657 , style = 'in' , hr = False )
        self.jobOM = mc.optionMenu ( 'jobOM' , label = 'Department' , w = 130 , h = 23 )
        mc.menuItem ( l = 'model' )
        mc.menuItem ( l = 'rig' )
        mc.menuItem ( l = 'shad' )
        self.processOM = mc.optionMenu ( 'processOM' , label = 'Process' , w = 150 , h = 23 )
        self.editProcessBT = mc.button( 'editProcessBT' , l = 'Edit' , w = 50 , h = 23 )
        self.separatorC = mc.separator( w = 357 , h = 5 , style = 'in' )
        self.libaryHeroTX = mc.text( l = '- - - - - - - - - - - - - - - - - - - - -  Libary Hero  - - - - - - - - - - - - - - - - - - - - -' , al = 'center' )
        self.listHeroTS = mc.textScrollList ( 'listHeroTS' , ams = False , w = 357 , h = 70 )
        self.libaryHeroNameTF = mc.textField( 'libaryHeroNameTF' , en = True , w = 250 , h = 26 )
        self.publishLibaryBT = mc.button( 'publishLibaryBT' , l = 'Publish Libary' , w = 100 , h = 25 )
        self.departmentHeroTX = mc.text( l = '- - - - - - - - - - - - - - - - - - -  Department Hero  - - - - - - - - - - - - - - - - - - -' , al = 'center' )
        self.listDepartmentHeroTS = mc.textScrollList ( 'listDepartmentHeroTS' , ams = False , w = 357 , h = 110 )
        self.departmentHeroNameTF = mc.textField( 'departmentHeroNameTF' , en = True , w = 250 , h = 26 )
        self.publishHeroBT = mc.button( 'publishHeroBT' , l = 'Publish' , w = 100 , h = 25 )
        self.departmentVersionTX = mc.text( l = '- - - - - - - - - - - - - - - - - -  Department Version  - - - - - - - - - - - - - - - - - - -' , al = 'center' )
        self.listDepartmentVersionTS = mc.textScrollList ( 'listDepartmentVersionTS' , ams = False , w = 357 , h = 161 )
        self.departmentVersionNameTF = mc.textField( 'departmentVersionNameTF' , en = True , w = 250 , h = 26 )
        self.saveVersionBT = mc.button( 'saveVersionBT' , l = 'Save' , w = 100 , h = 25 )
        self.separatorD = mc.separator( w = 357 , h = 5 , style = 'in' )
        self.openBT = mc.button( 'openBT' , l = 'Open' , w = 175 , h = 30 )
        self.deleteBT = mc.button( 'deleteBT' , l = 'Delete' , w = 175 , h = 30 )
        self.importBT = mc.button( 'importBT' , l = 'Import' , w = 175 , h = 30 )
        self.referenceBT = mc.button( 'referenceBT' , l = 'Reference' , w = 175 , h = 30 )
        
        mc.formLayout( self.form , e = True , attachForm = [       ( self.projOM , 'top' , 10 ) ,
                                                                   ( self.projOM , 'left' , 10 ) ,
                                                                   ( self.browseBT , 'top' , 10 ) ,
                                                                   ( self.pathCurrTX , 'top' , 16 ) ,
                                                                   ( self.categoryOM , 'left' , 10 ) ,
                                                                   ( self.separatorA , 'left' , 10 ) ,
                                                                   ( self.thumbPT , 'left' , 10 ) ,
                                                                   ( self.captureBT , 'left' , 10 ) ,
                                                                   ( self.listAssetTS , 'left' , 10 ) ,
                                                                   ( self.assetNameTF , 'left' , 10 ) ,
                                                                   ( self.createAssetBT , 'left' , 10 ) ,
                                                       ]
                                            , attachControl = [    ( self.browseBT , 'left' , 10 , self.projOM ) ,
                                                                   ( self.pathCurrTX , 'left' , 14 , self.browseBT ) ,
                                                                   ( self.categoryOM , 'top' , 15 , self.projOM ) ,
                                                                   ( self.categoryAddBT , 'top' , 15 , self.projOM ) ,
                                                                   ( self.categoryAddBT , 'left' , 10 , self.categoryOM ) ,
                                                                   ( self.typeOM , 'top' , 15 , self.projOM ) ,
                                                                   ( self.typeOM , 'left' , 20 , self.categoryAddBT ) ,
                                                                   ( self.typeAddBT , 'top' , 15 , self.projOM ) ,
                                                                   ( self.typeAddBT , 'left' , 10 , self.typeOM ) ,
                                                                   ( self.separatorA , 'top' , 7 , self.categoryOM ) ,
                                                                   ( self.thumbPT , 'top' , 5 , self.separatorA ) ,
                                                                   ( self.captureBT , 'top' , 5 , self.thumbPT ) ,
                                                                   ( self.listAssetTS , 'top' , 7 , self.captureBT ) ,
                                                                   ( self.assetNameTF , 'top' , 5 , self.listAssetTS ) ,
                                                                   ( self.createAssetBT , 'top' , 5 , self.assetNameTF ) ,
                                                                   ( self.separatorB , 'top' , 5 , self.separatorA ) ,
                                                                   ( self.separatorB , 'left' , 7 , self.thumbPT ) ,
                                                                   ( self.jobOM , 'top' , 5 , self.separatorA ) ,
                                                                   ( self.jobOM , 'left' , 15 , self.separatorB ) ,
                                                                   ( self.processOM , 'top' , 5 , self.separatorA ) ,
                                                                   ( self.processOM , 'left' , 15 , self.jobOM ) ,
                                                                   ( self.editProcessBT , 'top' , 5 , self.separatorA ) ,
                                                                   ( self.editProcessBT , 'left' , 10 , self.processOM ) ,
                                                                   ( self.separatorC , 'top' , 10 , self.jobOM ) ,
                                                                   ( self.separatorC , 'left' , 10 , self.separatorB ) ,
                                                                   ( self.libaryHeroTX , 'top' , 10 , self.separatorC ) ,
                                                                   ( self.libaryHeroTX , 'left' , 10 , self.separatorB ) ,
                                                                   ( self.listHeroTS , 'top' , 10 , self.libaryHeroTX ) ,
                                                                   ( self.listHeroTS , 'left' , 10 , self.separatorB ) ,
                                                                   ( self.libaryHeroNameTF , 'top' , 4 , self.listHeroTS ) ,
                                                                   ( self.libaryHeroNameTF , 'left' , 10 , self.separatorB ) ,
                                                                   ( self.publishLibaryBT , 'top' , 5 , self.listHeroTS ) ,
                                                                   ( self.publishLibaryBT , 'left' , 5 , self.libaryHeroNameTF ) ,
                                                                   ( self.departmentHeroTX , 'top' , 10 , self.libaryHeroNameTF ) ,
                                                                   ( self.departmentHeroTX , 'left' , 10 , self.separatorB ) ,
                                                                   ( self.listDepartmentHeroTS , 'top' , 10 , self.departmentHeroTX ) ,
                                                                   ( self.listDepartmentHeroTS , 'left' , 10 , self.separatorB ) ,
                                                                   ( self.departmentHeroNameTF , 'top' , 4 , self.listDepartmentHeroTS ) ,
                                                                   ( self.departmentHeroNameTF , 'left' , 10 , self.separatorB ) ,
                                                                   ( self.publishHeroBT , 'top' , 5 , self.listDepartmentHeroTS ) ,
                                                                   ( self.publishHeroBT , 'left' , 5 , self.departmentHeroNameTF ) ,
                                                                   ( self.departmentVersionTX , 'top' , 10 , self.departmentHeroNameTF ) ,
                                                                   ( self.departmentVersionTX , 'left' , 10 , self.separatorB ) ,
                                                                   ( self.listDepartmentVersionTS , 'top' , 10 , self.departmentVersionTX ) ,
                                                                   ( self.listDepartmentVersionTS , 'left' , 10 , self.separatorB ) ,
                                                                   ( self.departmentVersionNameTF , 'top' , 4 , self.listDepartmentVersionTS ) ,
                                                                   ( self.departmentVersionNameTF , 'left' , 10 , self.separatorB ) ,
                                                                   ( self.saveVersionBT , 'top' , 5 , self.listDepartmentVersionTS ) ,
                                                                   ( self.saveVersionBT , 'left' , 5 , self.departmentVersionNameTF ) ,
                                                                   ( self.separatorD , 'top' , 10 , self.departmentVersionNameTF ) ,
                                                                   ( self.separatorD , 'left' , 10 , self.separatorB ) ,
                                                                   ( self.openBT , 'top' , 7 , self.separatorD ) ,
                                                                   ( self.openBT , 'left' , 10 , self.separatorB ) ,
                                                                   ( self.deleteBT , 'top' , 7 , self.separatorD ) ,
                                                                   ( self.deleteBT , 'left' , 5 , self.openBT ) ,
                                                                   ( self.importBT , 'top' , 7 , self.openBT ) ,
                                                                   ( self.importBT , 'left' , 10 , self.separatorB ) ,
                                                                   ( self.referenceBT , 'top' , 7 , self.deleteBT ) ,
                                                                   ( self.referenceBT , 'left' , 5 , self.importBT ) ,
                                                       ] ) 
        
        mc.showWindow( 'assetNamingGUI' )
    
    def readProjectData(self) :
        pathProjTxt = 'C:/Users/Riff/Desktop/P/database/projectData.txt'
        
        projTxt = open( '%s' %pathProjTxt , 'r' )
        projData = projTxt.read()
        
        projSplits = projData.split('\r\n')
        
        for projSplit in projSplits :
            projStatus , projName , projPath = projSplit.split('>')
            
            if projStatus == '1' :
                mc.menuItem ( p = 'projOM' , l = '%s' %projName )
    
    def readCategoryData(self) :
        projectName = mc.optionMenu ( 'projOM' , q = True , v = True )
        pathCatTxt = 'C:/Users/Riff/Desktop/P/database/%sData/%sCat.txt' %(projectName,projectName)
        
        catTxt = open( '%s' %pathCatTxt , 'r' )
        catData = catTxt.read()
        
        catSplits = catData.split('\r\n')
        
        for catName in catSplits :
            mc.menuItem ( p = self.categoryOM , l = '%s' %catName )
    
    def writeCategoryData(self,*args) :
        categoryPD = mc.promptDialog( t = 'Category :' , m = 'char , prop , set , setDress' , b = ['OK', 'Cancel'] , db = 'OK' , cb = 'Cancel' , ds = 'Cancel' )
        if not categoryPD == 'Cancel' :
            categoryName = mc.promptDialog( q = True , t = True )
            
            if categoryName :
                projectName = mc.optionMenu ( self.projOM , q = True , v = True )
                
                pathCatTxt = 'C:/Users/Riff/Desktop/P/database/%sData' %projectName
                
                if not os.path.exists( pathCatTxt ) :
                    os.makedirs( pathCatTxt )
                
                catTxt = open( "%s/%sCat.txt" %(pathCatTxt,projectName) , "a" )
                catTxt.write( "%s\r\n" %categoryName )
                catTxt.close()
                
                run()
    
    def cmdCategoryAddBT(self) :
        mc.button( self.categoryAddBT , e = True , c = self.writeCategoryData )

def run():
    assetGUI = AssetNamingGUI()
    
    assetGUI.readProjectData()
    assetGUI.readCategoryData()
    
    assetGUI.cmdCategoryAddBT()