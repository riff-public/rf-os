# --------------------------------------------------------------------------------------------
#
# Shift Anim Node
#
# description :  - Pls. check start frame & end frame 
#                - Set frame * defualt in frame 1 *
#                - Chose shift or import sound 
#                - Click shift frame button
#
# --------------------------------------------------------------------------------------------

import maya.cmds as mc
import maya.mel as mm

def shiftFrameUI():
    
    if mc.window( 'shiftFrame' , exists = True ) :
        mc.deleteUI( 'shiftFrame' , window = True )
    
    mc.window( 'shiftFrame' , t = 'Shift Frame Tool' )
    form = mc.formLayout( w = 200 , h = 90 )
    
    frameTX = mc.text( l = 'frame  :' , al = 'left' )
    frameTF = mc.textField( 'frameTF' , en = 0 , w = 40 , tx = '1' )
    frameCB = mc.checkBox( l = 'overideFrame' )
    soundTX = mc.text( l = 'sound  :' , al = 'left' )
    soundRC = mc.radioCollection()
    shifBT = mc.radioButton( l = 'Shift' , sl = True )
    importBT = mc.radioButton( l = 'Import' )
    shiftFrameBT = mc.button( 'shiftFrameBT' , l = 'Shift Frame' , h = 30 , w = 200 )
    
    mc.formLayout( form , e = True , attachForm = [ ( frameTX , 'top' , 10 ) ,
                                                    ( frameTX , 'left' , 10 ),
                                                    ( frameTF , 'top' , 7 ) ,
                                                    ( frameCB , 'top' , 10 ) ,
                                                    ( soundTX , 'left' , 9 )
                                                  ]
                                   , attachControl = [ ( frameTF , 'left' , 7 , frameTX ) ,
                                                       ( frameCB , 'left' , 10 , frameTF ) ,
                                                       ( soundTX , 'top' , 10 , frameTF ) ,
                                                       ( shifBT , 'left' , 5 , soundTX ) ,
                                                       ( shifBT , 'top' , 9 , frameTF ) ,
                                                       ( importBT , 'left' , 7 , shifBT ) ,
                                                       ( importBT , 'top' , 9 , frameTF ) ,
                                                       ( shiftFrameBT , 'top' , 10 , soundTX )
                                                      ] )
    
    mc.showWindow( 'shiftFrame' )
    

def shiftFrame( frame = 1 ):
    
    #-- Get Information
    allAnimCrvs = mc.ls( type = 'animCurve' )
    stFrame = int( mc.playbackOptions( q = True , minTime = True ))
    endFrame = int( mc.playbackOptions( q = True , maxTime = True ))
    
    #-- Add Keys and Delete
    for animCrv in allAnimCrvs :
        #-- Check Infinity Anim
        preInfValue = mc.getAttr( '%s.preInfinity' %animCrv )
        postInfValue = mc.getAttr( '%s.postInfinity' %animCrv )
        
        if not ( preInfValue + postInfValue ) == 0 :
            mc.bakeResults( animCrv , sb = 1 , t = (stFrame-20,endFrame+20) , shape = True , sac = True )
        
        mc.setKeyframe( animCrv , insert = True , t = stFrame )
        mc.setKeyframe( animCrv , insert = True , t = endFrame )
    
    keysSt = mc.selectKey( allAnimCrvs , add = True , k = True , t = (-999999,stFrame-1))
    try : mc.cutKey( animation = 'keys' , clear = True )
    except TypeError : pass
    
    keysEnd = mc.selectKey( allAnimCrvs , add = True , k = True , t = (endFrame+1,999999))
    try : mc.cutKey( animation = 'keys' , clear = True )
    except TypeError : pass
    
    #-- Move KeysFrame
    if not frame == stFrame :
        allKeys = mc.selectKey( allAnimCrvs , add = True , k = True )
        mc.keyframe( animation = 'keys' , option = 'over' , relative = True , timeChange = ((frame-1) - (stFrame-1)))
        mc.select( cl = True )
    
    #-- Set Sound
    allSound = mc.ls( type = 'audio' )
    for sounds in allSound :
        offset = mc.getAttr( '%s.o' %sounds )
        mc.setAttr( '%s.o' %sounds , ((offset+(frame-1)) - (stFrame-1)) )
        
    #-- Set Timeslider
    mc.playbackOptions( min = frame , max = ( endFrame - (stFrame-1))+(frame-1) , ast = frame , aet = ( endFrame - (stFrame-1))+(frame-1))
    mc.currentTime( frame )
        
    #-- Print
    print "\n#-- DONE --#"