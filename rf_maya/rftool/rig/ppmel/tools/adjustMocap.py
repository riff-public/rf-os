import maya.cmds as mc
import maya.mel as mm

def adjustPositionFbx( grp = 'Fbx_Root' , frame = -50 ) :
    
    jnts = mc.ls( mc.listRelatives( 'Fbx_Root' , ad = True , type = 'joint' ))
    
    for jnt in jnts :
        for attr in ( 'rx' , 'ry' , 'rz' ) :
            mc.setKeyframe( jnt , v = 0 , at = attr , t = frame )
            
        for each in ( 'LeftLeg' , 'RightLeg' , 'LeftFoot' , 'RightFoot' ) :
            if each == jnt :
                for attr in ( 'tx' , 'tz' ) :
                    mc.setKeyframe( jnt , v = 0 , at = attr , t = frame )
            
        for each in ( 'LeftShoulder' , 'RightShoulder' , 'LeftArm' , 'RightArm' , 'LeftForeArm' , 'RightForeArm' , 'LeftHand' , 'RightHand' ) :
            if each == jnt :
                mc.setKeyframe( jnt , v = 0 , at = 'tz' , t = frame )
        
        for attr in ( 'tx' , 'tz' ) :
            mc.setKeyframe( 'Hips' , v = 0 , at = attr , t = frame )

def mapFbxMocap() :
    
    transform = mc.createNode( 'transform' )
    mc.delete( mc.parentConstraint ( 'Head_End' , transform , mo = False ))
    hight = mc.getAttr( '%s.ty' %transform )/54
    mc.delete( transform )
    
    mc.setAttr( 'master_ctrlMocap.globalScale' , hight )
    
    fbxJnts = mc.ls( mc.listRelatives( 'Fbx_Root' , ad = True , type = 'joint' ))
    cons = []
    
    for fbxJnt in fbxJnts :
        if not 'End' in fbxJnt :
            mc.delete(mc.pointConstraint ( fbxJnt , '%s_locMocap' %fbxJnt , mo = False ))
            cons.append(mc.parentConstraint ( fbxJnt , '%s_locMocap' %fbxJnt , mo = True )[0])
    
    minTime = mc.playbackOptions( q = True , min = True )
    maxTime = mc.playbackOptions( q = True , max = True )
    
    locMocap = mc.ls( '*_locMocap' )
    mc.bakeResults( locMocap , simulation = True , t = ( minTime , maxTime ))
    mc.delete(cons)
    mc.setAttr( 'master_ctrlMocap.globalScale' , 1 )
         
def transferRig( ctrl = 'ctrl' ) :
    
    namespace = mc.ls( sl = True )[0].split( ':' )[0]
    
    ctrlMocaps = mc.ls( '*_ctrlMocap' )
    
    consMaps = []
    
    for ctrlMocap in ctrlMocaps :
        name = ctrlMocap.split( '_ctrlMocap' )[0]
        consMaps.append(mc.parentConstraint ( '%s:%s_%s' %( namespace , name , ctrl ) , ctrlMocap , mo = False )[0])
    
    mc.delete( consMaps )
    
    cons = []
    ctrlBreak = []
    
    cons.append(mc.parentConstraint ( 'pelvis_ctrlMocap' , '%s:root_%s' %( namespace , ctrl ) , mo = True )[0])
    
    for ctrlMocap in ctrlMocaps :
        name = ctrlMocap.split( '_ctrlMocap' )[0]
        cons.append(mc.parentConstraint ( ctrlMocap , '%s:%s_%s' %( namespace , name , ctrl ) , mo = False )[0])
        
        ctrlBreak.append('%s:%s_%s' %( namespace , name , ctrl ))
    
    for side in ( 'lft' , 'rgt' ) :
        mc.delete( mc.pointConstraint ( 'lowLegFk_%s_ctrlMocap' %side , '%s:kneeIk_%s_%s' %( namespace , side , ctrl ) , mo = False ))
        mc.delete( mc.pointConstraint ( 'foreArmFk_%s_ctrlMocap' %side , '%s:elbowIk_%s_%s' %( namespace , side , ctrl ) , mo = False ))
        cons.append(mc.parentConstraint ( 'ankleFk_%s_ctrlMocap' %side , '%s:ankleIk_%s_%s' %( namespace , side , ctrl ) , mo = True )[0])
        cons.append(mc.parentConstraint ( 'lowLegFk_%s_ctrlMocap' %side , '%s:kneeIk_%s_%s' %( namespace , side , ctrl ) , mo = True )[0])
        cons.append(mc.parentConstraint ( 'wristFk_%s_ctrlMocap' %side , '%s:wristIk_%s_%s' %( namespace , side , ctrl ) , mo = True )[0])
        cons.append(mc.parentConstraint ( 'foreArmFk_%s_ctrlMocap' %side , '%s:elbowIk_%s_%s' %( namespace , side , ctrl ) , mo = True )[0])
    
    for addCtrl in ( '%s:root_%s' %( namespace , ctrl ) ,
                     '%s:ankleIk_lft_%s' %( namespace , ctrl ) , 
                     '%s:ankleIk_rgt_%s' %( namespace , ctrl ) , 
                     '%s:kneeIk_lft_%s'  %( namespace , ctrl ) , 
                     '%s:kneeIk_rgt_%s'  %( namespace , ctrl ) ,
                     '%s:wristIk_lft_%s' %( namespace , ctrl ) ,
                     '%s:wristIk_rgt_%s' %( namespace , ctrl ) ,
                     '%s:elbowIk_lft_%s' %( namespace , ctrl ) ,
                     '%s:elbowIk_rgt_%s' %( namespace , ctrl ) ):
    
        ctrlBreak.append( addCtrl )
    
    minTime = mc.playbackOptions( q = True , min = True )
    maxTime = mc.playbackOptions( q = True , max = True )
    
    mc.bakeResults( ctrlBreak , simulation = True , s = False , t = ( minTime , maxTime ))
    mc.delete( cons )