import maya.cmds as mc

sels = mc.ls(sl = True)

for sel in sels :
    name = sel.split('_bake')[0]
    mc.parentConstraint( 'aiya_med:%s_skinJnt' %name , sel , mo = 0 )

minTime = mc.playbackOptions( q = True , min = True )
maxTime = mc.playbackOptions( q = True , max = True )
mc.bakeResults( sels , simulation = True , t = ( minTime , maxTime ))

for sel in sels :
    name = sel.split('_bake')[0]
    mc.parentConstraint( sel , 'aiya_med:%sFk_ctrl' %name , mo = 0 )