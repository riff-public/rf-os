import maya.cmds as mc
import maya.mel as mm

# --- How to Use --- #
#
# 1 : 
# 2 : 
# 3 : 
# 4 : 
#
#-------------------- #

def characterUI():
    
    if mc.window( 'characterUI' , exists = True ) :
        mc.deleteUI( 'characterUI' , window = True )
    
    mc.window( 'characterUI' , t = 'characterUI Tool' , rtf = True )
    
    formOut = mc.formLayout()
    
    nameSpaceTX = mc.text( l = 'nameSpace  :' , al = 'left' )
    nameSpaceTF = mc.textField( 'nameSpaceTF' , en = 0 , w = 240 )
    addNameSpaceBT = mc.button( 'addNameSpaceBT' , l = '+' , h = 20 , w = 20 )
    showTX = mc.text( l = 'show  :' , al = 'left' )
    polygonCB = mc.checkBox( l = 'polygon' , v = 1 )
    ctrlCB = mc.checkBox( l = 'controls' , v = 1 )
    jointCB = mc.checkBox( l = 'joint' , v = 1 )
    tabs = mc.tabLayout( w = 355 , h = 550 )
    
    mc.formLayout( formOut , e = True , attachForm = [ ( nameSpaceTX , 'top' , 10 ) ,
                                                       ( nameSpaceTX , 'left' , 10 ) ,
                                                       ( nameSpaceTF , 'top' , 7 ) ,
                                                       ( addNameSpaceBT , 'top' , 7 ) ,
                                                       ( showTX , 'left' , 40 ) ,
                                                     ]
                                   , attachControl = [ ( nameSpaceTF , 'left' , 5 , nameSpaceTX ) ,
                                                       ( addNameSpaceBT , 'left' , 5 , nameSpaceTF ) ,
                                                       ( showTX , 'top' , 10 , nameSpaceTF ) ,
                                                       ( polygonCB , 'top' , 10 , nameSpaceTF ) ,
                                                       ( polygonCB , 'left' , 5 , showTX ) ,
                                                       ( ctrlCB , 'top' , 10 , nameSpaceTF ) ,
                                                       ( ctrlCB , 'left' , 20 , polygonCB ) ,
                                                       ( jointCB , 'top' , 10 , nameSpaceTF ) ,
                                                       ( jointCB , 'left' , 20 , ctrlCB ) ,
                                                       ( tabs , 'top' , 5 , showTX ) ,
                                                     ] )
    
    #--- MAIN TAB ---#
    mainForm = mc.formLayout()
    
    #--- HEAD FORUM ---#
    headColumn = mc.columnLayout()
    headForm = mc.formLayout( w = 145 , h = 60 )
    headCtrlBT = mc.button( 'headCtrlBT' , l = '' , h = 60 , w = 55 )
    eyeTrgtBT = mc.button( 'eyeTrgtBT' , l = 'Eye' , h = 19 , w = 40 )
    eyeTrgtLftBT = mc.button( 'eyeTrgtLftBT' , l = '' , h = 8 , w = 19 )
    eyeTrgtRgtBT = mc.button( 'eyeTrgtRgtBT' , l = '' , h = 8 , w = 19 )
    eyeLftBT = mc.button( 'eyeLftBT' , l = 'L' , h = 16 , w = 17 )
    eyeRgtBT = mc.button( 'eyeRgtBT' , l = 'R' , h = 16 , w = 17 )
    jawBT = mc.button( 'jawBT' , l = 'Jaw' , h = 19 , w = 40 )
    jawUpBT = mc.button( 'jawUpBT' , l = '' , h = 8 , w = 40 )
    jawDnBT = mc.button( 'jawDnBT' , l = '' , h = 8 , w = 40 )
    mc.setParent( '..' )
    mc.setParent( '..' )
    
    mc.formLayout( headForm , e = True , attachForm = [ ( headCtrlBT , 'top' , 0 ) ,
                                                        ( headCtrlBT , 'left' , 45 ) ,
                                                        ( eyeTrgtBT , 'top' , 25 ) ,
                                                        ( jawBT , 'top' , 25 ) ,
                                                      ]
                                    , attachControl = [ ( eyeTrgtBT , 'right' , 5 , headCtrlBT ) ,
                                                        ( eyeTrgtLftBT , 'top' , 2 , eyeTrgtBT ) ,
                                                        ( eyeTrgtLftBT , 'right' , 26 , headCtrlBT ) ,
                                                        ( eyeTrgtRgtBT , 'top' , 2 , eyeTrgtBT ) ,
                                                        ( eyeTrgtRgtBT , 'right' , 5 , headCtrlBT ) ,
                                                        ( eyeLftBT , 'bottom' , 2 , eyeTrgtBT ) ,
                                                        ( eyeLftBT , 'right' , 27 , headCtrlBT ) ,
                                                        ( eyeRgtBT , 'bottom' , 2 , eyeTrgtBT ) ,
                                                        ( eyeRgtBT , 'right' , 5 , headCtrlBT ) ,
                                                        ( jawBT , 'left' , 5 , headCtrlBT ) ,
                                                        ( jawUpBT , 'bottom' , 2 , jawBT ) ,
                                                        ( jawUpBT , 'left' , 5 , headCtrlBT ) ,
                                                        ( jawDnBT , 'top' , 2 , jawBT ) ,
                                                        ( jawDnBT , 'left' , 5 , headCtrlBT ) ,
                                                      ] )
    
    #--- SPINE FORUM ---#
    spineColumn = mc.columnLayout()
    spineForm = mc.formLayout( w = 90 , h = 135 )
    neckRbnCtrlBT = mc.button( 'neckRbnCtrlBT' , l = '' , h = 8 , w = 30 )
    neckCtrlBT = mc.button( 'neckCtrlBT' , l = '' , h = 8 , w = 40 )
    spine4CtrlBT = mc.button( 'spine4CtrlBT' , l = '' , h = 17 , w = 90 )
    spine3CtrlBT = mc.button( 'spine3CtrlBT' , l = '' , h = 17 , w = 67 )
    spine2CtrlBT = mc.button( 'spine2CtrlBT' , l = '' , h = 17 , w = 67 )
    spine1CtrlBT = mc.button( 'spine1CtrlBT' , l = '' , h = 17 , w = 67 )
    spineAllCtrlBT = mc.button( 'spineAllCtrlBT' , l = '' , h = 57 , w = 10 )
    rootCtrlBT = mc.button( 'rootCtrlBT' , l = 'Root' , h = 17 , w = 90 )
    pelvisCtrlBT = mc.button( 'pelvisCtrlBT' , l = '' , h = 12 , w = 80 )
    clavLeftCtrlBT = mc.button( 'clavLeftCtrlBT' , l = '' , h = 15 , w = 15 )
    clavRightCtrlBT = mc.button( 'clavRightCtrlBT' , l = '' , h = 15 , w = 15 )
    mc.setParent( '..' )
    mc.setParent( '..' )
    
    mc.formLayout( spineForm , e = True , attachForm = [ ( neckRbnCtrlBT , 'top' , 1 ) ,
                                                         ( neckRbnCtrlBT , 'left' , 30 ) ,
                                                         ( neckCtrlBT , 'left' , 25 ) ,
                                                         ( spine4CtrlBT , 'left' , 0 ) ,
                                                         ( spine3CtrlBT , 'left' , 5 ) ,
                                                         ( spine2CtrlBT , 'left' , 5 ) ,
                                                         ( spine1CtrlBT , 'left' , 5 ) ,
                                                         ( rootCtrlBT , 'left' , 0 ) ,
                                                         ( pelvisCtrlBT , 'left' , 5 ) ,
                                                         ( clavLeftCtrlBT , 'right' , 0 ) ,
                                                         ( clavRightCtrlBT , 'left' , 0 ) ,
                                                       ]
                                     , attachControl = [ ( neckCtrlBT , 'top' , 2 , neckRbnCtrlBT ) ,
                                                         ( spine4CtrlBT , 'top' , 3 , neckCtrlBT ) ,
                                                         ( spine3CtrlBT , 'top' , 3 , spine4CtrlBT ) ,
                                                         ( spine2CtrlBT , 'top' , 3 , spine3CtrlBT ) ,
                                                         ( spine1CtrlBT , 'top' , 3 , spine2CtrlBT ) ,
                                                         ( spineAllCtrlBT , 'top' , 3 , spine4CtrlBT ) ,
                                                         ( spineAllCtrlBT , 'left' , 3 , spine1CtrlBT ) ,
                                                         ( rootCtrlBT , 'top' , 3 , spine1CtrlBT ) ,
                                                         ( pelvisCtrlBT , 'top' , 3 , rootCtrlBT ) ,
                                                         ( clavLeftCtrlBT , 'bottom' , 3 , spine4CtrlBT ) ,
                                                         ( clavRightCtrlBT , 'bottom' , 3 , spine4CtrlBT ) ,
                                                       ] )
    
    
    #--- LEG LEFT FORUM ---#
    legLeftColumn = mc.columnLayout()
    legLeftForm = mc.formLayout( w = 81 , h = 185 )
    upLegIkLeftCtrlBT = mc.button( 'upLegIkLeftCtrlBT' , l = 'ik' , h = 17 , w = 18 )
    upLegFkLeftCtrlBT = mc.button( 'upLegFkLeftCtrlBT' , l = '' , h = 54 , w = 18 )
    kneeIkLeftCtrlBT = mc.button( 'kneeIkLeftCtrlBT' , l = 'ik' , h = 17 , w = 18 )
    lowLegFkLeftCtrlBT = mc.button( 'lowLegFkLeftCtrlBT' , l = '' , h = 54 , w = 18 )
    ankleIkLeftCtrlBT = mc.button( 'ankleIkLeftCtrlBT' , l = 'ik' , h = 15 , w = 35 )
    ankleFkLeftCtrlBT = mc.button( 'ankleFkLeftCtrlBT' , l = '' , h = 15 , w = 35 )
    ballIkLeftCtrlBT = mc.button( 'ballIkLeftCtrlBT' , l = 'ik' , h = 20 , w = 15 )
    ballFkLeftCtrlBT = mc.button( 'ballFkLeftCtrlBT' , l = '' , h = 20 , w = 15 )
    upLegRbnLeftCtrlBT = mc.button( 'upLegRbnLeftCtrlBT' , l = '' , h = 10 , w = 10 )
    upLegRbn1LeftCtrlBT = mc.button( 'upLegRbn1LeftCtrlBT' , l = '' , h = 10 , w = 10 )
    upLegRbn2LeftCtrlBT = mc.button( 'upLegRbn2LeftCtrlBT' , l = '' , h = 10 , w = 10 )
    upLegRbn3LeftCtrlBT = mc.button( 'upLegRbn3LeftCtrlBT' , l = '' , h = 10 , w = 10 )
    upLegRbn4LeftCtrlBT = mc.button( 'upLegRbn4LeftCtrlBT' , l = '' , h = 10 , w = 10 )
    upLegRbn5LeftCtrlBT = mc.button( 'upLegRbn5LeftCtrlBT' , l = '' , h = 10 , w = 10 )
    kneeRbnLeftCtrlBT = mc.button( 'kneeRbnLeftCtrlBT' , l = '' , h = 10 , w = 10 )
    lowLegRbnLeftCtrlBT = mc.button( 'lowLegRbnLeftCtrlBT' , l = '' , h = 10 , w = 10 )
    lowLegRbn1LeftCtrlBT = mc.button( 'lowLegRbn1LeftCtrlBT' , l = '' , h = 10 , w = 10 )
    lowLegRbn2LeftCtrlBT = mc.button( 'lowLegRbn2LeftCtrlBT' , l = '' , h = 10 , w = 10 )
    lowLegRbn3LeftCtrlBT = mc.button( 'lowLegRbn3LeftCtrlBT' , l = '' , h = 10 , w = 10 )
    lowLegRbn4LeftCtrlBT = mc.button( 'lowLegRbn4LeftCtrlBT' , l = '' , h = 10 , w = 10 )
    lowLegRbn5LeftCtrlBT = mc.button( 'lowLegRbn5LeftCtrlBT' , l = '' , h = 10 , w = 10 )
    mc.setParent( '..' )
    mc.setParent( '..' )
    
    mc.formLayout( legLeftForm , e = True , attachForm = [ ( upLegIkLeftCtrlBT , 'top' , 0 ) ,
                                                            ( upLegIkLeftCtrlBT , 'right' , 50 ) ,
                                                            ( upLegFkLeftCtrlBT , 'right' , 50 ) ,
                                                            ( kneeIkLeftCtrlBT , 'right' , 50 ) ,
                                                            ( lowLegFkLeftCtrlBT , 'right' , 50 ) ,
                                                            ( ankleIkLeftCtrlBT , 'right' , 35 ) ,
                                                            ( ankleFkLeftCtrlBT , 'right' , 35 ) ,
                                                          ]
                                        , attachControl = [ ( upLegFkLeftCtrlBT , 'top' , 3 , upLegIkLeftCtrlBT ) ,
                                                            ( kneeIkLeftCtrlBT , 'top' , 3 , upLegFkLeftCtrlBT ) ,
                                                            ( lowLegFkLeftCtrlBT , 'top' , 3 , kneeIkLeftCtrlBT ) ,
                                                            ( ankleIkLeftCtrlBT , 'top' , 3 , lowLegFkLeftCtrlBT ) ,
                                                            ( ankleFkLeftCtrlBT , 'top' , 1 , ankleIkLeftCtrlBT ) ,
                                                            ( ballIkLeftCtrlBT , 'top' , 14 , lowLegFkLeftCtrlBT ) ,
                                                            ( ballIkLeftCtrlBT , 'left' , 2 , ankleFkLeftCtrlBT ) ,
                                                            ( ballFkLeftCtrlBT , 'top' , 14 , lowLegFkLeftCtrlBT ) ,
                                                            ( ballFkLeftCtrlBT , 'left' , 2 , ballIkLeftCtrlBT ) ,
                                                            ( upLegRbnLeftCtrlBT , 'top' , 25 , upLegIkLeftCtrlBT ) ,
                                                            ( upLegRbnLeftCtrlBT , 'left' , 2 , upLegFkLeftCtrlBT ) ,
                                                            ( upLegRbn1LeftCtrlBT , 'top' , 3 , upLegIkLeftCtrlBT ) ,
                                                            ( upLegRbn1LeftCtrlBT , 'right' , 2 , upLegFkLeftCtrlBT ) ,
                                                            ( upLegRbn2LeftCtrlBT , 'top' , 1 , upLegRbn1LeftCtrlBT ) ,
                                                            ( upLegRbn2LeftCtrlBT , 'right' , 2 , upLegFkLeftCtrlBT ) ,
                                                            ( upLegRbn3LeftCtrlBT , 'top' , 1 , upLegRbn2LeftCtrlBT ) ,
                                                            ( upLegRbn3LeftCtrlBT , 'right' , 2 , upLegFkLeftCtrlBT ) ,
                                                            ( upLegRbn4LeftCtrlBT , 'top' , 1 , upLegRbn3LeftCtrlBT ) ,
                                                            ( upLegRbn4LeftCtrlBT , 'right' , 2 , upLegFkLeftCtrlBT ) ,
                                                            ( upLegRbn5LeftCtrlBT , 'top' , 1 , upLegRbn4LeftCtrlBT ) ,
                                                            ( upLegRbn5LeftCtrlBT , 'right' , 2 , upLegFkLeftCtrlBT ) ,
                                                            ( kneeRbnLeftCtrlBT , 'top' , 6 , upLegFkLeftCtrlBT ) ,
                                                            ( kneeRbnLeftCtrlBT , 'left' , 2 , kneeIkLeftCtrlBT ) ,
                                                            ( lowLegRbnLeftCtrlBT , 'top' , 25 , kneeIkLeftCtrlBT ) ,
                                                            ( lowLegRbnLeftCtrlBT , 'left' , 2 , lowLegFkLeftCtrlBT ) ,
                                                            ( lowLegRbn1LeftCtrlBT , 'top' , 3 , kneeIkLeftCtrlBT ) ,
                                                            ( lowLegRbn1LeftCtrlBT , 'right' , 2 , lowLegFkLeftCtrlBT ) ,
                                                            ( lowLegRbn2LeftCtrlBT , 'top' , 1 , lowLegRbn1LeftCtrlBT ) ,
                                                            ( lowLegRbn2LeftCtrlBT , 'right' , 2 , lowLegFkLeftCtrlBT ) ,
                                                            ( lowLegRbn3LeftCtrlBT , 'top' , 1 , lowLegRbn2LeftCtrlBT ) ,
                                                            ( lowLegRbn3LeftCtrlBT , 'right' , 2 , lowLegFkLeftCtrlBT ) ,
                                                            ( lowLegRbn4LeftCtrlBT , 'top' , 1 , lowLegRbn3LeftCtrlBT ) ,
                                                            ( lowLegRbn4LeftCtrlBT , 'right' , 2 , lowLegFkLeftCtrlBT ) ,
                                                            ( lowLegRbn5LeftCtrlBT , 'top' , 1 , lowLegRbn4LeftCtrlBT ) ,
                                                            ( lowLegRbn5LeftCtrlBT , 'right' , 2 , lowLegFkLeftCtrlBT ) ,
                                                          ] )
                                                          
    
    #--- LEG RIGHT FORUM ---#
    legRightColumn = mc.columnLayout()
    legRightForm = mc.formLayout( w = 81 , h = 185 )
    upLegIkRightCtrlBT = mc.button( 'upLegIkRightCtrlBT' , l = 'ik' , h = 17 , w = 18 )
    upLegFkRightCtrlBT = mc.button( 'upLegFkRightCtrlBT' , l = '' , h = 54 , w = 18 )
    kneeIkRightCtrlBT = mc.button( 'kneeIkRightCtrlBT' , l = 'ik' , h = 17 , w = 18 )
    lowLegFkRightCtrlBT = mc.button( 'lowLegFkRightCtrlBT' , l = '' , h = 54 , w = 18 )
    ankleIkRightCtrlBT = mc.button( 'ankleIkRightCtrlBT' , l = 'ik' , h = 15 , w = 35 )
    ankleFkRightCtrlBT = mc.button( 'ankleFkRightCtrlBT' , l = '' , h = 15 , w = 35 )
    ballIkRightCtrlBT = mc.button( 'ballIkRightCtrlBT' , l = 'ik' , h = 20 , w = 15 )
    ballFkRightCtrlBT = mc.button( 'ballFkRightCtrlBT' , l = '' , h = 20 , w = 15 )
    upLegRbnRightCtrlBT = mc.button( 'upLegRbnRightCtrlBT' , l = '' , h = 10 , w = 10 )
    upLegRbn1RightCtrlBT = mc.button( 'upLegRbn1RightCtrlBT' , l = '' , h = 10 , w = 10 )
    upLegRbn2RightCtrlBT = mc.button( 'upLegRbn2RightCtrlBT' , l = '' , h = 10 , w = 10 )
    upLegRbn3RightCtrlBT = mc.button( 'upLegRbn3RightCtrlBT' , l = '' , h = 10 , w = 10 )
    upLegRbn4RightCtrlBT = mc.button( 'upLegRbn4RightCtrlBT' , l = '' , h = 10 , w = 10 )
    upLegRbn5RightCtrlBT = mc.button( 'upLegRbn5RightCtrlBT' , l = '' , h = 10 , w = 10 )
    kneeRbnRightCtrlBT = mc.button( 'kneeRbnRightCtrlBT' , l = '' , h = 10 , w = 10 )
    lowLegRbnRightCtrlBT = mc.button( 'lowLegRbnRightCtrlBT' , l = '' , h = 10 , w = 10 )
    lowLegRbn1RightCtrlBT = mc.button( 'lowLegRbn1RightCtrlBT' , l = '' , h = 10 , w = 10 )
    lowLegRbn2RightCtrlBT = mc.button( 'lowLegRbn2RightCtrlBT' , l = '' , h = 10 , w = 10 )
    lowLegRbn3RightCtrlBT = mc.button( 'lowLegRbn3RightCtrlBT' , l = '' , h = 10 , w = 10 )
    lowLegRbn4RightCtrlBT = mc.button( 'lowLegRbn4RightCtrlBT' , l = '' , h = 10 , w = 10 )
    lowLegRbn5RightCtrlBT = mc.button( 'lowLegRbn5RightCtrlBT' , l = '' , h = 10 , w = 10 )
    mc.setParent( '..' )
    mc.setParent( '..' )
    
    mc.formLayout( legRightForm , e = True , attachForm = [ ( upLegIkRightCtrlBT , 'top' , 0 ) ,
                                                           ( upLegIkRightCtrlBT , 'left' , 50 ) ,
                                                           ( upLegFkRightCtrlBT , 'left' , 50 ) ,
                                                           ( kneeIkRightCtrlBT , 'left' , 50 ) ,
                                                           ( lowLegFkRightCtrlBT , 'left' , 50 ) ,
                                                           ( ankleIkRightCtrlBT , 'left' , 35 ) ,
                                                           ( ankleFkRightCtrlBT , 'left' , 35 ) ,
                                                         ]
                                       , attachControl = [ ( upLegFkRightCtrlBT , 'top' , 3 , upLegIkRightCtrlBT ) ,
                                                           ( kneeIkRightCtrlBT , 'top' , 3 , upLegFkRightCtrlBT ) ,
                                                           ( lowLegFkRightCtrlBT , 'top' , 3 , kneeIkRightCtrlBT ) ,
                                                           ( ankleIkRightCtrlBT , 'top' , 3 , lowLegFkRightCtrlBT ) ,
                                                           ( ankleFkRightCtrlBT , 'top' , 1 , ankleIkRightCtrlBT ) ,
                                                           ( ballIkRightCtrlBT , 'top' , 14 , lowLegFkRightCtrlBT ) ,
                                                           ( ballIkRightCtrlBT , 'right' , 2 , ankleFkRightCtrlBT ) ,
                                                           ( ballFkRightCtrlBT , 'top' , 14 , lowLegFkRightCtrlBT ) ,
                                                           ( ballFkRightCtrlBT , 'right' , 2 , ballIkRightCtrlBT ) ,
                                                           ( upLegRbnRightCtrlBT , 'top' , 25 , upLegIkRightCtrlBT ) ,
                                                           ( upLegRbnRightCtrlBT , 'right' , 2 , upLegFkRightCtrlBT ) ,
                                                           ( upLegRbn1RightCtrlBT , 'top' , 3 , upLegIkRightCtrlBT ) ,
                                                           ( upLegRbn1RightCtrlBT , 'left' , 2 , upLegFkRightCtrlBT ) ,
                                                           ( upLegRbn2RightCtrlBT , 'top' , 1 , upLegRbn1RightCtrlBT ) ,
                                                           ( upLegRbn2RightCtrlBT , 'left' , 2 , upLegFkRightCtrlBT ) ,
                                                           ( upLegRbn3RightCtrlBT , 'top' , 1 , upLegRbn2RightCtrlBT ) ,
                                                           ( upLegRbn3RightCtrlBT , 'left' , 2 , upLegFkRightCtrlBT ) ,
                                                           ( upLegRbn4RightCtrlBT , 'top' , 1 , upLegRbn3RightCtrlBT ) ,
                                                           ( upLegRbn4RightCtrlBT , 'left' , 2 , upLegFkRightCtrlBT ) ,
                                                           ( upLegRbn5RightCtrlBT , 'top' , 1 , upLegRbn4RightCtrlBT ) ,
                                                           ( upLegRbn5RightCtrlBT , 'left' , 2 , upLegFkRightCtrlBT ) ,
                                                           ( kneeRbnRightCtrlBT , 'top' , 6 , upLegFkRightCtrlBT ) ,
                                                           ( kneeRbnRightCtrlBT , 'right' , 2 , kneeIkRightCtrlBT ) ,
                                                           ( lowLegRbnRightCtrlBT , 'top' , 25 , kneeIkRightCtrlBT ) ,
                                                           ( lowLegRbnRightCtrlBT , 'right' , 2 , lowLegFkRightCtrlBT ) ,
                                                           ( lowLegRbn1RightCtrlBT , 'top' , 3 , kneeIkRightCtrlBT ) ,
                                                           ( lowLegRbn1RightCtrlBT , 'left' , 2 , lowLegFkRightCtrlBT ) ,
                                                           ( lowLegRbn2RightCtrlBT , 'top' , 1 , lowLegRbn1RightCtrlBT ) ,
                                                           ( lowLegRbn2RightCtrlBT , 'left' , 2 , lowLegFkRightCtrlBT ) ,
                                                           ( lowLegRbn3RightCtrlBT , 'top' , 1 , lowLegRbn2RightCtrlBT ) ,
                                                           ( lowLegRbn3RightCtrlBT , 'left' , 2 , lowLegFkRightCtrlBT ) ,
                                                           ( lowLegRbn4RightCtrlBT , 'top' , 1 , lowLegRbn3RightCtrlBT ) ,
                                                           ( lowLegRbn4RightCtrlBT , 'left' , 2 , lowLegFkRightCtrlBT ) ,
                                                           ( lowLegRbn5RightCtrlBT , 'top' , 1 , lowLegRbn4RightCtrlBT ) ,
                                                           ( lowLegRbn5RightCtrlBT , 'left' , 2 , lowLegFkRightCtrlBT ) ,
                                                         ] )
    
    
    #--- ARM LEFT FORUM ---#
    armLeftColumn = mc.columnLayout()
    armLeftForm = mc.formLayout( w = 115 , h = 107 )
    upArmIkLeftCtrlBT = mc.button( 'upArmIkLeftCtrlBT' , l = 'ik' , h = 18 , w = 18 )
    upArmFkLeftCtrlBT = mc.button( 'upArmFkLeftCtrlBT' , l = '' , h = 18 , w = 54 )
    elbowIkLeftCtrlBT = mc.button( 'elbowIkLeftCtrlBT' , l = 'ik' , h = 30 , w = 18 )
    foreArmFkLeftCtrlBT = mc.button( 'foreArmFkLeftCtrlBT' , l = '' , h = 54 , w = 18 )
    upArmRbnLeftCtrlBT = mc.button( 'upArmRbnLeftCtrlBT' , l = '' , h = 10 , w = 10 )
    upArmRbn1LeftCtrlBT = mc.button( 'upArmRbn1LeftCtrlBT' , l = '' , h = 10 , w = 10 )
    upArmRbn2LeftCtrlBT = mc.button( 'upArmRbn2LeftCtrlBT' , l = '' , h = 10 , w = 10 )
    upArmRbn3LeftCtrlBT = mc.button( 'upArmRbn3LeftCtrlBT' , l = '' , h = 10 , w = 10 )
    upArmRbn4LeftCtrlBT = mc.button( 'upArmRbn4LeftCtrlBT' , l = '' , h = 10 , w = 10 )
    upArmRbn5LeftCtrlBT = mc.button( 'upArmRbn5LeftCtrlBT' , l = '' , h = 10 , w = 10 )
    elbowRbnLeftCtrlBT = mc.button( 'elbowRbnLeftCtrlBT' , l = '' , h = 10 , w = 10 )
    foreArmRbnLeftCtrlBT = mc.button( 'foreArmRbnLeftCtrlBT' , l = '' , h = 10 , w = 10 )
    foreArmRbn1LeftCtrlBT = mc.button( 'foreArmRbn1LeftCtrlBT' , l = '' , h = 10 , w = 10 )
    foreArmRbn2LeftCtrlBT = mc.button( 'foreArmRbn2LeftCtrlBT' , l = '' , h = 10 , w = 10 )
    foreArmRbn3LeftCtrlBT = mc.button( 'foreArmRbn3LeftCtrlBT' , l = '' , h = 10 , w = 10 )
    foreArmRbn4LeftCtrlBT = mc.button( 'foreArmRbn4LeftCtrlBT' , l = '' , h = 10 , w = 10 )
    foreArmRbn5LeftCtrlBT = mc.button( 'foreArmRbn5LeftCtrlBT' , l = '' , h = 10 , w = 10 )
    mc.setParent( '..' )
    mc.setParent( '..' )
    
    mc.formLayout( armLeftForm , e = True , attachForm = [ ( upArmIkLeftCtrlBT , 'top' , 21 ) ,
                                                           ( upArmFkLeftCtrlBT , 'top' , 21 ) ,
                                                           ( elbowIkLeftCtrlBT , 'top' , 21 ) ,
                                                         ]
                                       , attachControl = [ ( upArmFkLeftCtrlBT , 'left' , 2 , upArmIkLeftCtrlBT ) ,
                                                           ( elbowIkLeftCtrlBT , 'left' , 2 , upArmFkLeftCtrlBT ) ,
                                                           ( foreArmFkLeftCtrlBT , 'top' , 2 , elbowIkLeftCtrlBT ) ,
                                                           ( foreArmFkLeftCtrlBT , 'left' , 2 , upArmFkLeftCtrlBT ) ,
                                                           ( upArmRbnLeftCtrlBT , 'bottom' , 2 , upArmFkLeftCtrlBT ) ,
                                                           ( upArmRbnLeftCtrlBT , 'left' , 25 , upArmIkLeftCtrlBT ) ,
                                                           ( upArmRbn1LeftCtrlBT , 'top' , 2 , upArmFkLeftCtrlBT ) ,
                                                           ( upArmRbn1LeftCtrlBT , 'left' , 2 , upArmIkLeftCtrlBT ) ,
                                                           ( upArmRbn2LeftCtrlBT , 'top' , 2 , upArmFkLeftCtrlBT ) ,
                                                           ( upArmRbn2LeftCtrlBT , 'left' , 1 , upArmRbn1LeftCtrlBT ) ,
                                                           ( upArmRbn3LeftCtrlBT , 'top' , 2 , upArmFkLeftCtrlBT ) ,
                                                           ( upArmRbn3LeftCtrlBT , 'left' , 1 , upArmRbn2LeftCtrlBT ) ,
                                                           ( upArmRbn4LeftCtrlBT , 'top' , 2 , upArmFkLeftCtrlBT ) ,
                                                           ( upArmRbn4LeftCtrlBT , 'left' , 1 , upArmRbn3LeftCtrlBT ) ,
                                                           ( upArmRbn5LeftCtrlBT , 'top' , 2 , upArmFkLeftCtrlBT ) ,
                                                           ( upArmRbn5LeftCtrlBT , 'left' , 1 , upArmRbn4LeftCtrlBT ) ,
                                                           ( elbowRbnLeftCtrlBT , 'bottom' , 2 , elbowIkLeftCtrlBT ) ,
                                                           ( elbowRbnLeftCtrlBT , 'left' , 2 , elbowIkLeftCtrlBT ) ,
                                                           ( foreArmRbnLeftCtrlBT , 'top' , 25 , elbowIkLeftCtrlBT ) ,
                                                           ( foreArmRbnLeftCtrlBT , 'left' , 2 , foreArmFkLeftCtrlBT ) ,
                                                           ( foreArmRbn1LeftCtrlBT , 'top' , 2 , elbowIkLeftCtrlBT ) ,
                                                           ( foreArmRbn1LeftCtrlBT , 'right' , 2 , foreArmFkLeftCtrlBT ) ,
                                                           ( foreArmRbn2LeftCtrlBT , 'top' , 1 , foreArmRbn1LeftCtrlBT ) ,
                                                           ( foreArmRbn2LeftCtrlBT , 'right' , 2 , foreArmFkLeftCtrlBT ) ,
                                                           ( foreArmRbn3LeftCtrlBT , 'top' , 1 , foreArmRbn2LeftCtrlBT ) ,
                                                           ( foreArmRbn3LeftCtrlBT , 'right' , 2 , foreArmFkLeftCtrlBT ) ,
                                                           ( foreArmRbn4LeftCtrlBT , 'top' , 1 , foreArmRbn3LeftCtrlBT ) ,
                                                           ( foreArmRbn4LeftCtrlBT , 'right' , 2 , foreArmFkLeftCtrlBT ) ,
                                                           ( foreArmRbn5LeftCtrlBT , 'top' , 1 , foreArmRbn4LeftCtrlBT ) ,
                                                           ( foreArmRbn5LeftCtrlBT , 'right' , 2 , foreArmFkLeftCtrlBT ) ,
                                                         ] )
    
    
    #--- HAND LEFT FORUM ---#
    handLeftColumn = mc.columnLayout()
    handLeftForm = mc.formLayout( w = 71 , h = 88 )
    wristIkLeftCtrlBT = mc.button( 'wristIkLeftCtrlBT' , l = 'ik' , h = 14 , w = 51 )
    wristFkLeftCtrlBT = mc.button( 'wristFkLeftCtrlBT' , l = '' , h = 14 , w = 51 )
    thumbLeftCtrlBT = mc.button( 'thumbLeftCtrlBT' , l = '' , h = 12 , w = 12 )
    thumb1LeftCtrlBT = mc.button( 'thumb1LeftCtrlBT' , l = '' , h = 10 , w = 10 )
    thumb2LeftCtrlBT = mc.button( 'thumb2LeftCtrlBT' , l = '' , h = 10 , w = 10 )
    thumb3LeftCtrlBT = mc.button( 'thumb3LeftCtrlBT' , l = '' , h = 10 , w = 10 )
    indexLeftCtrlBT = mc.button( 'indexLeftCtrlBT' , l = '' , h = 12 , w = 12 )
    index1LeftCtrlBT = mc.button( 'index1LeftCtrlBT' , l = '' , h = 10 , w = 10 )
    index2LeftCtrlBT = mc.button( 'index2LeftCtrlBT' , l = '' , h = 10 , w = 10 )
    index3LeftCtrlBT = mc.button( 'index3LeftCtrlBT' , l = '' , h = 10 , w = 10 )
    index4LeftCtrlBT = mc.button( 'index4LeftCtrlBT' , l = '' , h = 10 , w = 10 )
    middleLeftCtrlBT = mc.button( 'middleLeftCtrlBT' , l = '' , h = 12 , w = 12 )
    middle1LeftCtrlBT = mc.button( 'middle1LeftCtrlBT' , l = '' , h = 10 , w = 10 )
    middle2LeftCtrlBT = mc.button( 'middle2LeftCtrlBT' , l = '' , h = 10 , w = 10 )
    middle3LeftCtrlBT = mc.button( 'middle3LeftCtrlBT' , l = '' , h = 10 , w = 10 )
    middle4LeftCtrlBT = mc.button( 'middle4LeftCtrlBT' , l = '' , h = 10 , w = 10 )
    ringLeftCtrlBT = mc.button( 'ringLeftCtrlBT' , l = '' , h = 12 , w = 12 )
    ring1LeftCtrlBT = mc.button( 'ring1LeftCtrlBT' , l = '' , h = 10 , w = 10 )
    ring2LeftCtrlBT = mc.button( 'ring2LeftCtrlBT' , l = '' , h = 10 , w = 10 )
    ring3LeftCtrlBT = mc.button( 'ring3LeftCtrlBT' , l = '' , h = 10 , w = 10 )
    ring4LeftCtrlBT = mc.button( 'ring4LeftCtrlBT' , l = '' , h = 10 , w = 10 )
    pinkyLeftCtrlBT = mc.button( 'pinkyLeftCtrlBT' , l = '' , h = 12 , w = 12 )
    pinky1LeftCtrlBT = mc.button( 'pinky1LeftCtrlBT' , l = '' , h = 10 , w = 10 )
    pinky2LeftCtrlBT = mc.button( 'pinky2LeftCtrlBT' , l = '' , h = 10 , w = 10 )
    pinky3LeftCtrlBT = mc.button( 'pinky3LeftCtrlBT' , l = '' , h = 10 , w = 10 )
    pinky4LeftCtrlBT = mc.button( 'pinky4LeftCtrlBT' , l = '' , h = 10 , w = 10 )
    
    mc.setParent( '..' )
    mc.setParent( '..' )
    
    mc.formLayout( handLeftForm , e = True , attachForm = [ ( wristIkLeftCtrlBT , 'right' , 0 ) ,
                                                            ( wristFkLeftCtrlBT , 'right' , 0 ) ,
                                                            ( thumbLeftCtrlBT , 'top' , 10 ) ,
                                                         ]
                                       , attachControl = [ ( wristFkLeftCtrlBT , 'top' , 2 , wristIkLeftCtrlBT ) ,
                                                           ( thumbLeftCtrlBT , 'right' , 2 , wristFkLeftCtrlBT ) ,
                                                           ( thumb1LeftCtrlBT , 'top' , 1 , thumbLeftCtrlBT ) ,
                                                           ( thumb1LeftCtrlBT , 'right' , 3 , wristFkLeftCtrlBT ) ,
                                                           ( thumb2LeftCtrlBT , 'top' , 1 , thumb1LeftCtrlBT ) ,
                                                           ( thumb2LeftCtrlBT , 'right' , 3 , wristFkLeftCtrlBT ) ,
                                                           ( thumb3LeftCtrlBT , 'top' , 1 , thumb2LeftCtrlBT ) ,
                                                           ( thumb3LeftCtrlBT , 'right' , 3 , wristFkLeftCtrlBT ) ,
                                                           ( indexLeftCtrlBT , 'top' , 2 , wristFkLeftCtrlBT ) ,
                                                           ( indexLeftCtrlBT , 'right' , -12 , wristFkLeftCtrlBT ) ,
                                                           ( index1LeftCtrlBT , 'top' , 1 , indexLeftCtrlBT ) ,
                                                           ( index1LeftCtrlBT , 'right' , -11 , wristFkLeftCtrlBT ) ,
                                                           ( index2LeftCtrlBT , 'top' , 1 , index1LeftCtrlBT ) ,
                                                           ( index2LeftCtrlBT , 'right' , -11 , wristFkLeftCtrlBT ) ,
                                                           ( index3LeftCtrlBT , 'top' , 1 , index2LeftCtrlBT ) ,
                                                           ( index3LeftCtrlBT , 'right' , -11 , wristFkLeftCtrlBT ) ,
                                                           ( index4LeftCtrlBT , 'top' , 1 , index3LeftCtrlBT ) ,
                                                           ( index4LeftCtrlBT , 'right' , -11 , wristFkLeftCtrlBT ) ,
                                                           ( middleLeftCtrlBT , 'top' , 2 , wristFkLeftCtrlBT ) ,
                                                           ( middleLeftCtrlBT , 'left' , 1 , indexLeftCtrlBT ) ,
                                                           ( middle1LeftCtrlBT , 'top' , 1 , middleLeftCtrlBT ) ,
                                                           ( middle1LeftCtrlBT , 'left' , 3 , index1LeftCtrlBT ) ,
                                                           ( middle2LeftCtrlBT , 'top' , 1 , middle1LeftCtrlBT ) ,
                                                           ( middle2LeftCtrlBT , 'left' , 3 , index2LeftCtrlBT ) ,
                                                           ( middle3LeftCtrlBT , 'top' , 1 , middle2LeftCtrlBT ) ,
                                                           ( middle3LeftCtrlBT , 'left' , 3 , index3LeftCtrlBT ) ,
                                                           ( middle4LeftCtrlBT , 'top' , 1 , middle3LeftCtrlBT ) ,
                                                           ( middle4LeftCtrlBT , 'left' , 3 , index4LeftCtrlBT ) ,
                                                           ( ringLeftCtrlBT , 'top' , 2 , wristFkLeftCtrlBT ) ,
                                                           ( ringLeftCtrlBT , 'left' , 1 , middleLeftCtrlBT ) ,
                                                           ( ring1LeftCtrlBT , 'top' , 1 , ringLeftCtrlBT ) ,
                                                           ( ring1LeftCtrlBT , 'left' , 3 , middle1LeftCtrlBT ) ,
                                                           ( ring2LeftCtrlBT , 'top' , 1 , ring1LeftCtrlBT ) ,
                                                           ( ring2LeftCtrlBT , 'left' , 3 , middle2LeftCtrlBT ) ,
                                                           ( ring3LeftCtrlBT , 'top' , 1 , ring2LeftCtrlBT ) ,
                                                           ( ring3LeftCtrlBT , 'left' , 3 , middle3LeftCtrlBT ) ,
                                                           ( ring4LeftCtrlBT , 'top' , 1 , ring3LeftCtrlBT ) ,
                                                           ( ring4LeftCtrlBT , 'left' , 3 , middle4LeftCtrlBT ) ,
                                                           ( pinkyLeftCtrlBT , 'top' , 2 , wristFkLeftCtrlBT ) ,
                                                           ( pinkyLeftCtrlBT , 'left' , 1 , ringLeftCtrlBT ) ,
                                                           ( pinky1LeftCtrlBT , 'top' , 1 , pinkyLeftCtrlBT ) ,
                                                           ( pinky1LeftCtrlBT , 'left' , 3 , ring1LeftCtrlBT ) ,
                                                           ( pinky2LeftCtrlBT , 'top' , 1 , pinky1LeftCtrlBT ) ,
                                                           ( pinky2LeftCtrlBT , 'left' , 3 , ring2LeftCtrlBT ) ,
                                                           ( pinky3LeftCtrlBT , 'top' , 1 , pinky2LeftCtrlBT ) ,
                                                           ( pinky3LeftCtrlBT , 'left' , 3 , ring3LeftCtrlBT ) ,
                                                           ( pinky4LeftCtrlBT , 'top' , 1 , pinky3LeftCtrlBT ) ,
                                                           ( pinky4LeftCtrlBT , 'left' , 3 , ring4LeftCtrlBT ) ,
                                                         ] )
    
    
    #--- ARM RIGHT FORUM ---#
    armRightColumn = mc.columnLayout()
    armRightForm = mc.formLayout( w = 115 , h = 107 )
    upArmIkRightCtrlBT = mc.button( 'upArmIkRightCtrlBT' , l = 'ik' , h = 18 , w = 18 )
    upArmFkRightCtrlBT = mc.button( 'upArmFkRightCtrlBT' , l = '' , h = 18 , w = 54 )
    elbowIkRightCtrlBT = mc.button( 'elbowIkRightCtrlBT' , l = 'ik' , h = 30 , w = 18 )
    foreArmFkRightCtrlBT = mc.button( 'foreArmFkRightCtrlBT' , l = '' , h = 54 , w = 18 )
    upArmRbnRightCtrlBT = mc.button( 'upArmRbnRightCtrlBT' , l = '' , h = 10 , w = 10 )
    upArmRbn1RightCtrlBT = mc.button( 'upArmRbn1RightCtrlBT' , l = '' , h = 10 , w = 10 )
    upArmRbn2RightCtrlBT = mc.button( 'upArmRbn2RightCtrlBT' , l = '' , h = 10 , w = 10 )
    upArmRbn3RightCtrlBT = mc.button( 'upArmRbn3RightCtrlBT' , l = '' , h = 10 , w = 10 )
    upArmRbn4RightCtrlBT = mc.button( 'upArmRbn4RightCtrlBT' , l = '' , h = 10 , w = 10 )
    upArmRbn5RightCtrlBT = mc.button( 'upArmRbn5RightCtrlBT' , l = '' , h = 10 , w = 10 )
    elbowRbnRightCtrlBT = mc.button( 'elbowRbnRightCtrlBT' , l = '' , h = 10 , w = 10 )
    foreArmRbnRightCtrlBT = mc.button( 'foreArmRbnRightCtrlBT' , l = '' , h = 10 , w = 10 )
    foreArmRbn1RightCtrlBT = mc.button( 'foreArmRbn1RightCtrlBT' , l = '' , h = 10 , w = 10 )
    foreArmRbn2RightCtrlBT = mc.button( 'foreArmRbn2RightCtrlBT' , l = '' , h = 10 , w = 10 )
    foreArmRbn3RightCtrlBT = mc.button( 'foreArmRbn3RightCtrlBT' , l = '' , h = 10 , w = 10 )
    foreArmRbn4RightCtrlBT = mc.button( 'foreArmRbn4RightCtrlBT' , l = '' , h = 10 , w = 10 )
    foreArmRbn5RightCtrlBT = mc.button( 'foreArmRbn5RightCtrlBT' , l = '' , h = 10 , w = 10 )
    mc.setParent( '..' )
    mc.setParent( '..' )
    
    mc.formLayout( armRightForm , e = True , attachForm = [ ( upArmIkRightCtrlBT , 'top' , 21 ) ,
                                                            ( upArmIkRightCtrlBT , 'right' , 0 ) ,
                                                            ( upArmFkRightCtrlBT , 'top' , 21 ) ,
                                                            ( elbowIkRightCtrlBT , 'top' , 21 ) ,
                                                          ]
                                       , attachControl = [ ( upArmFkRightCtrlBT , 'right' , 2 , upArmIkRightCtrlBT ) ,
                                                           ( elbowIkRightCtrlBT , 'right' , 2 , upArmFkRightCtrlBT ) ,
                                                           ( foreArmFkRightCtrlBT , 'top' , 2 , elbowIkRightCtrlBT ) ,
                                                           ( foreArmFkRightCtrlBT , 'right' , 2 , upArmFkRightCtrlBT ) ,
                                                           ( upArmRbnRightCtrlBT , 'bottom' , 2 , upArmFkRightCtrlBT ) ,
                                                           ( upArmRbnRightCtrlBT , 'right' , 25 , upArmIkRightCtrlBT ) ,
                                                           ( upArmRbn1RightCtrlBT , 'top' , 2 , upArmFkRightCtrlBT ) ,
                                                           ( upArmRbn1RightCtrlBT , 'right' , 2 , upArmIkRightCtrlBT ) ,
                                                           ( upArmRbn2RightCtrlBT , 'top' , 2 , upArmFkRightCtrlBT ) ,
                                                           ( upArmRbn2RightCtrlBT , 'right' , 1 , upArmRbn1RightCtrlBT ) ,
                                                           ( upArmRbn3RightCtrlBT , 'top' , 2 , upArmFkRightCtrlBT ) ,
                                                           ( upArmRbn3RightCtrlBT , 'right' , 1 , upArmRbn2RightCtrlBT ) ,
                                                           ( upArmRbn4RightCtrlBT , 'top' , 2 , upArmFkRightCtrlBT ) ,
                                                           ( upArmRbn4RightCtrlBT , 'right' , 1 , upArmRbn3RightCtrlBT ) ,
                                                           ( upArmRbn5RightCtrlBT , 'top' , 2 , upArmFkRightCtrlBT ) ,
                                                           ( upArmRbn5RightCtrlBT , 'right' , 1 , upArmRbn4RightCtrlBT ) ,
                                                           ( elbowRbnRightCtrlBT , 'bottom' , 2 , elbowIkRightCtrlBT ) ,
                                                           ( elbowRbnRightCtrlBT , 'right' , 2 , elbowIkRightCtrlBT ) ,
                                                           ( foreArmRbnRightCtrlBT , 'top' , 25 , elbowIkRightCtrlBT ) ,
                                                           ( foreArmRbnRightCtrlBT , 'right' , 2 , foreArmFkRightCtrlBT ) ,
                                                           ( foreArmRbn1RightCtrlBT , 'top' , 2 , elbowIkRightCtrlBT ) ,
                                                           ( foreArmRbn1RightCtrlBT , 'left' , 2 , foreArmFkRightCtrlBT ) ,
                                                           ( foreArmRbn2RightCtrlBT , 'top' , 1 , foreArmRbn1RightCtrlBT ) ,
                                                           ( foreArmRbn2RightCtrlBT , 'left' , 2 , foreArmFkRightCtrlBT ) ,
                                                           ( foreArmRbn3RightCtrlBT , 'top' , 1 , foreArmRbn2RightCtrlBT ) ,
                                                           ( foreArmRbn3RightCtrlBT , 'left' , 2 , foreArmFkRightCtrlBT ) ,
                                                           ( foreArmRbn4RightCtrlBT , 'top' , 1 , foreArmRbn3RightCtrlBT ) ,
                                                           ( foreArmRbn4RightCtrlBT , 'left' , 2 , foreArmFkRightCtrlBT ) ,
                                                           ( foreArmRbn5RightCtrlBT , 'top' , 1 , foreArmRbn4RightCtrlBT ) ,
                                                           ( foreArmRbn5RightCtrlBT , 'left' , 2 , foreArmFkRightCtrlBT ) ,
                                                         ] )
    
    
    #--- HAND LEFT FORUM ---#
    handRightColumn = mc.columnLayout()
    handRightForm = mc.formLayout( w = 71 , h = 88 )
    wristIkRightCtrlBT = mc.button( 'wristIkRightCtrlBT' , l = 'ik' , h = 14 , w = 51 )
    wristFkRightCtrlBT = mc.button( 'wristFkRightCtrlBT' , l = '' , h = 14 , w = 51 )
    thumbRightCtrlBT = mc.button( 'thumbRightCtrlBT' , l = '' , h = 12 , w = 12 )
    thumb1RightCtrlBT = mc.button( 'thumb1RightCtrlBT' , l = '' , h = 10 , w = 10 )
    thumb2RightCtrlBT = mc.button( 'thumb2RightCtrlBT' , l = '' , h = 10 , w = 10 )
    thumb3RightCtrlBT = mc.button( 'thumb3RightCtrlBT' , l = '' , h = 10 , w = 10 )
    indexRightCtrlBT = mc.button( 'indexRightCtrlBT' , l = '' , h = 12 , w = 12 )
    index1RightCtrlBT = mc.button( 'index1RightCtrlBT' , l = '' , h = 10 , w = 10 )
    index2RightCtrlBT = mc.button( 'index2RightCtrlBT' , l = '' , h = 10 , w = 10 )
    index3RightCtrlBT = mc.button( 'index3RightCtrlBT' , l = '' , h = 10 , w = 10 )
    index4RightCtrlBT = mc.button( 'index4RightCtrlBT' , l = '' , h = 10 , w = 10 )
    middleRightCtrlBT = mc.button( 'middleRightCtrlBT' , l = '' , h = 12 , w = 12 )
    middle1RightCtrlBT = mc.button( 'middle1RightCtrlBT' , l = '' , h = 10 , w = 10 )
    middle2RightCtrlBT = mc.button( 'middle2RightCtrlBT' , l = '' , h = 10 , w = 10 )
    middle3RightCtrlBT = mc.button( 'middle3RightCtrlBT' , l = '' , h = 10 , w = 10 )
    middle4RightCtrlBT = mc.button( 'middle4RightCtrlBT' , l = '' , h = 10 , w = 10 )
    ringRightCtrlBT = mc.button( 'ringRightCtrlBT' , l = '' , h = 12 , w = 12 )
    ring1RightCtrlBT = mc.button( 'ring1RightCtrlBT' , l = '' , h = 10 , w = 10 )
    ring2RightCtrlBT = mc.button( 'ring2RightCtrlBT' , l = '' , h = 10 , w = 10 )
    ring3RightCtrlBT = mc.button( 'ring3RightCtrlBT' , l = '' , h = 10 , w = 10 )
    ring4RightCtrlBT = mc.button( 'ring4RightCtrlBT' , l = '' , h = 10 , w = 10 )
    pinkyRightCtrlBT = mc.button( 'pinkyRightCtrlBT' , l = '' , h = 12 , w = 12 )
    pinky1RightCtrlBT = mc.button( 'pinky1RightCtrlBT' , l = '' , h = 10 , w = 10 )
    pinky2RightCtrlBT = mc.button( 'pinky2RightCtrlBT' , l = '' , h = 10 , w = 10 )
    pinky3RightCtrlBT = mc.button( 'pinky3RightCtrlBT' , l = '' , h = 10 , w = 10 )
    pinky4RightCtrlBT = mc.button( 'pinky4RightCtrlBT' , l = '' , h = 10 , w = 10 )
    
    mc.setParent( '..' )
    mc.setParent( '..' )
    
    mc.formLayout( handRightForm , e = True , attachForm = [ ( wristIkRightCtrlBT , 'left' , 0 ) ,
                                                            ( wristFkRightCtrlBT , 'left' , 0 ) ,
                                                            ( thumbRightCtrlBT , 'top' , 10 ) ,
                                                         ]
                                       , attachControl = [ ( wristFkRightCtrlBT , 'top' , 2 , wristIkRightCtrlBT ) ,
                                                           ( thumbRightCtrlBT , 'left' , 2 , wristFkRightCtrlBT ) ,
                                                           ( thumb1RightCtrlBT , 'top' , 1 , thumbRightCtrlBT ) ,
                                                           ( thumb1RightCtrlBT , 'left' , 3 , wristFkRightCtrlBT ) ,
                                                           ( thumb2RightCtrlBT , 'top' , 1 , thumb1RightCtrlBT ) ,
                                                           ( thumb2RightCtrlBT , 'left' , 3 , wristFkRightCtrlBT ) ,
                                                           ( thumb3RightCtrlBT , 'top' , 1 , thumb2RightCtrlBT ) ,
                                                           ( thumb3RightCtrlBT , 'left' , 3 , wristFkRightCtrlBT ) ,
                                                           ( indexRightCtrlBT , 'top' , 2 , wristFkRightCtrlBT ) ,
                                                           ( indexRightCtrlBT , 'left' , -12 , wristFkRightCtrlBT ) ,
                                                           ( index1RightCtrlBT , 'top' , 1 , indexRightCtrlBT ) ,
                                                           ( index1RightCtrlBT , 'left' , -11 , wristFkRightCtrlBT ) ,
                                                           ( index2RightCtrlBT , 'top' , 1 , index1RightCtrlBT ) ,
                                                           ( index2RightCtrlBT , 'left' , -11 , wristFkRightCtrlBT ) ,
                                                           ( index3RightCtrlBT , 'top' , 1 , index2RightCtrlBT ) ,
                                                           ( index3RightCtrlBT , 'left' , -11 , wristFkRightCtrlBT ) ,
                                                           ( index4RightCtrlBT , 'top' , 1 , index3RightCtrlBT ) ,
                                                           ( index4RightCtrlBT , 'left' , -11 , wristFkRightCtrlBT ) ,
                                                           ( middleRightCtrlBT , 'top' , 2 , wristFkRightCtrlBT ) ,
                                                           ( middleRightCtrlBT , 'right' , 1 , indexRightCtrlBT ) ,
                                                           ( middle1RightCtrlBT , 'top' , 1 , middleRightCtrlBT ) ,
                                                           ( middle1RightCtrlBT , 'right' , 3 , index1RightCtrlBT ) ,
                                                           ( middle2RightCtrlBT , 'top' , 1 , middle1RightCtrlBT ) ,
                                                           ( middle2RightCtrlBT , 'right' , 3 , index2RightCtrlBT ) ,
                                                           ( middle3RightCtrlBT , 'top' , 1 , middle2RightCtrlBT ) ,
                                                           ( middle3RightCtrlBT , 'right' , 3 , index3RightCtrlBT ) ,
                                                           ( middle4RightCtrlBT , 'top' , 1 , middle3RightCtrlBT ) ,
                                                           ( middle4RightCtrlBT , 'right' , 3 , index4RightCtrlBT ) ,
                                                           ( ringRightCtrlBT , 'top' , 2 , wristFkRightCtrlBT ) ,
                                                           ( ringRightCtrlBT , 'right' , 1 , middleRightCtrlBT ) ,
                                                           ( ring1RightCtrlBT , 'top' , 1 , ringRightCtrlBT ) ,
                                                           ( ring1RightCtrlBT , 'right' , 3 , middle1RightCtrlBT ) ,
                                                           ( ring2RightCtrlBT , 'top' , 1 , ring1RightCtrlBT ) ,
                                                           ( ring2RightCtrlBT , 'right' , 3 , middle2RightCtrlBT ) ,
                                                           ( ring3RightCtrlBT , 'top' , 1 , ring2RightCtrlBT ) ,
                                                           ( ring3RightCtrlBT , 'right' , 3 , middle3RightCtrlBT ) ,
                                                           ( ring4RightCtrlBT , 'top' , 1 , ring3RightCtrlBT ) ,
                                                           ( ring4RightCtrlBT , 'right' , 3 , middle4RightCtrlBT ) ,
                                                           ( pinkyRightCtrlBT , 'top' , 2 , wristFkRightCtrlBT ) ,
                                                           ( pinkyRightCtrlBT , 'right' , 1 , ringRightCtrlBT ) ,
                                                           ( pinky1RightCtrlBT , 'top' , 1 , pinkyRightCtrlBT ) ,
                                                           ( pinky1RightCtrlBT , 'right' , 3 , ring1RightCtrlBT ) ,
                                                           ( pinky2RightCtrlBT , 'top' , 1 , pinky1RightCtrlBT ) ,
                                                           ( pinky2RightCtrlBT , 'right' , 3 , ring2RightCtrlBT ) ,
                                                           ( pinky3RightCtrlBT , 'top' , 1 , pinky2RightCtrlBT ) ,
                                                           ( pinky3RightCtrlBT , 'right' , 3 , ring3RightCtrlBT ) ,
                                                           ( pinky4RightCtrlBT , 'top' , 1 , pinky3RightCtrlBT ) ,
                                                           ( pinky4RightCtrlBT , 'right' , 3 , ring4RightCtrlBT ) ,
                                                         ] )
                                                         
    
    
    #--- EXTRA BUTTON ---#
    leftConerColumn = mc.columnLayout()
    mc.button( 'resetPoseBT' , l = 'Reset Pose' , h = 19 , w = 65 )
    mc.button( 'selAllBT' , l = 'Select All' , h = 19 , w = 65 )
    mc.button( 'keyBT' , l = 'Key All' , h = 19 , w = 65 )
    mc.setParent( '..' )
    
    rightConerColumn = mc.columnLayout()
    mc.button( 'mirrorPoseBT' , l = 'Mirror Pose' , h = 19 , w = 65 )
    mc.button( 'mirrorArmBT' , l = 'Mirror Arm' , h = 19 , w = 65 )
    mc.button( 'mirrorLegBT' , l = 'Mirror Leg' , h = 19 , w = 65 )
    mc.setParent( '..' )
    
    armLeftExColumn = mc.columnLayout()
    mc.button( 'fkIkArmLeftBT' , l = 'Fk Ik Arm' , h = 19 , w = 65 )
    mc.button( 'elbowLockLeftBT' , l = 'Elbow Lock' , h = 19 , w = 65 )
    mc.button( 'armPoseLeftBT' , l = '<-- Arm' , h = 19 , w = 65 )
    mc.button( 'handPoseLeftBT' , l = '<-- Hand' , h = 19 , w = 65 )
    mc.setParent( '..' )
    
    armRightExColumn = mc.columnLayout()
    mc.button( 'fkIkArmRightBT' , l = 'Fk Ik Arm' , h = 19 , w = 65 )
    mc.button( 'elbowLockRightBT' , l = 'Elbow Lock' , h = 19 , w = 65 )
    mc.button( 'armPoseRightBT' , l = 'Arm -->' , h = 19 , w = 65 )
    mc.button( 'handPoseRightBT' , l = 'Hand -->' , h = 19 , w = 65 )
    mc.setParent( '..' )
    
    legLeftExColumn = mc.columnLayout()
    mc.button( 'fkIkLegLeftBT' , l = 'Fk Ik Leg' , h = 19 , w = 65 )
    mc.button( 'kneeLockLeftBT' , l = 'Knee Lock' , h = 19 , w = 65 )
    mc.button( 'legPoseLeftBT' , l = '<-- Leg' , h = 19 , w = 65 )
    mc.setParent( '..' )
    
    legRightExColumn = mc.columnLayout()
    mc.button( 'fkIkLegRightBT' , l = 'Fk Ik Leg' , h = 19 , w = 65 )
    mc.button( 'kneeLockRightBT' , l = 'Knee Lock' , h = 19 , w = 65 )
    mc.button( 'legPoseRightBT' , l = 'Leg -->' , h = 19 , w = 65 )
    mc.setParent( '..' )
    
    
    mc.formLayout( mainForm , e = True , attachForm = [ ( leftConerColumn , 'left' , 1 ) ,
                                                        ( leftConerColumn , 'top' , 1 ) ,
                                                        ( rightConerColumn , 'right' , 1 ) ,
                                                        ( rightConerColumn , 'top' , 1 ) ,
                                                        ( armLeftExColumn , 'left' , 1 ) ,
                                                        ( armLeftExColumn , 'bottom' , 1 ) ,
                                                        ( armRightExColumn , 'right' , 1 ) ,
                                                        ( armRightExColumn , 'bottom' , 1 ) ,
                                                        ( legLeftExColumn , 'bottom' , 1 ) ,
                                                        ( legRightExColumn , 'bottom' , 1 ) ,
                                                        ( headColumn , 'top' , 20 ) ,
                                                        ( headColumn , 'left' , 102 ) ,
                                                        ( spineColumn , 'left' , 130 ) ,
                                                        ( legLeftColumn , 'right' , 75) ,
                                                        ( legRightColumn , 'left' , 79 ) ,
                                                      ]
                                    , attachControl = [ ( legLeftExColumn , 'left' , 2 , armLeftExColumn ) ,
                                                        ( legRightExColumn , 'right' , 2 , armRightExColumn ) ,
                                                        ( spineColumn , 'top' , 0 , headColumn ) ,
                                                        ( legLeftColumn , 'top' , 0 , spineColumn ) ,
                                                        ( legRightColumn , 'top' , 0 , spineColumn ) ,
                                                        ( armLeftColumn , 'top' , 0 , headColumn ) ,
                                                        ( armLeftColumn , 'left' , 7 , spineColumn ) ,
                                                        ( armRightColumn , 'top' , 0 , headColumn ) ,
                                                        ( armRightColumn , 'right' , 7 , spineColumn ) ,
                                                        ( handLeftColumn , 'top' , 0 , armLeftColumn ) ,
                                                        ( handLeftColumn , 'left' , 46 , spineColumn ) ,
                                                        ( handRightColumn , 'top' , 0 , armRightColumn ) ,
                                                        ( handRightColumn , 'right' , 46 , spineColumn ) ,
                                                      ] )
                                                     
    
    mc.setParent( '..' )
    mc.setParent( '..' )
    
    mc.tabLayout( tabs , edit = True , tabLabel = (( mainForm , 'Main' )))
    
    mc.showWindow( 'characterUI' )