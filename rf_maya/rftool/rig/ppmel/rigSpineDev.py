import maya.cmds as mc
import maya.mel as mm
import ppmel.core as core
reload( core )


def rigSpine( spine1TmpJnt = 'spine1_tmpJnt' ,
              spine2TmpJnt = 'spine2_tmpJnt' ,
              spine3TmpJnt = 'spine3_tmpJnt' ,
              spine4TmpJnt = 'spine4_tmpJnt' ,
              spine5TmpJnt = 'spine5_tmpJnt' ,
              parent = 'root_jnt' ,
              animGrp = 'anim_grp' ,
              skinGrp = 'skin_grp' ,
              jntGrp = 'jnt_grp' ,
              spineFk = True ,
              spineIk = False ,
              rotateOrder = 'yzx' ,
              elem = '' ,
              axis = 'y' ,
              charSize = 2 ) :
    
    ## Main Rig ##
    #-- Group
    spineRigGrp = core.transform( 'spine%sRig_grp' %elem )
    spineJntGrp = core.transform( 'spine%sJnt_grp' %elem )
    core.parCons( 0 , parent , spineRigGrp )
    core.snap( parent , spineJntGrp )
    core.parent( spineRigGrp , animGrp )
    core.parent( spineJntGrp , jntGrp )
    
    #-- Ribbon
    loft1 = core.createCurve( 'loft1_cuv' , 'line' )
    loft2 = core.createCurve( 'loft2_cuv' , 'line' )
    loft3 = core.createCurve( 'loft3_cuv' , 'line' )
    loft4 = core.createCurve( 'loft4_cuv' , 'line' )
    loft5 = core.createCurve( 'loft5_cuv' , 'line' )
    
    core.snap( spine1TmpJnt , loft1 )
    core.snap( spine2TmpJnt , loft2 )
    core.snap( spine3TmpJnt , loft3 )
    core.snap( spine4TmpJnt , loft4 )
    core.snap( spine5TmpJnt , loft5 )
    
    for ctrl in ( loft1 , loft2 , loft3 , loft4 , loft5 ) :
            core.scaleShape( ctrl , charSize * 1.3 )
    
    spineRbnNrb = mc.loft( loft1 , loft2 , loft3 , loft4 , loft5 , ch = True , rn = True , ar = True , n = 'spine%sRbn_nrb' %elem )[0]
    mc.delete( spineRbnNrb , ch = True )
    mc.delete( loft1 , loft2 , loft3 , loft4 , loft5 )
    
    #-- Joint
    rbnRootJnt = core.addJnt( 'spine%sRbnRoot_jnt' %elem )
    rbnMidJnt = core.addJnt( 'spine%sRbnMid_jnt' %elem )
    rbnEndJnt = core.addJnt( 'spine%sRbnEnd_jnt' %elem )
    
    rbnRootJntAim = mc.group( rbnRootJnt , n = 'spine%sRbnRootJntAim_grp' %elem )
    rbnMidJntAim = mc.group( rbnMidJnt , n = 'spine%sRbnMidJntAim_grp' %elem )
    rbnEndJntAim = mc.group( rbnEndJnt , n = 'spine%sRbnEndJntAim_grp' %elem )
    
    #-- Controls
    rbnRootCtrl = core.addCtrl( 'spine%sRbnRoot_ctrl' %elem , 'locator' , 'yellow' , jnt = False )
    rbnRootCtrlZro = core.addGrp( rbnRootCtrl )
    
    rbnEndCtrl = core.addCtrl( 'spine%sRbnEnd_ctrl' %elem , 'locator' , 'yellow' , jnt = False )
    rbnEndCtrlZro = core.addGrp( rbnEndCtrl )
    
    rbnCtrl = core.addCtrl( 'spine%sRbn_ctrl' %elem , 'square' , 'yellow' , jnt = False )
    rbnCtrlZro = core.addGrp( rbnCtrl )
    rbnCtrlGmbl = core.addGimbal( rbnCtrl )
    rbnCtrlAim = core.addGrp( rbnCtrl , 'Aim' )
    
    rbnCtrlShape = core.getShape( rbnCtrl )
    core.addAttr( rbnCtrlShape , 'detailVis' , 0 , 1 , 0 )
    
    mc.parent( rbnMidJntAim , rbnCtrlGmbl )
    mc.parent( rbnEndJntAim , rbnEndCtrl )
    mc.parent( rbnRootJntAim , rbnRootCtrl )
    
    #-- Shape
    for ctrl in ( rbnCtrl , rbnCtrlGmbl ) :
        core.scaleShape( ctrl , charSize )
    
    #-- Rig process
    # Ribbon
    if   axis == 'y' :
         postAx = 'ty'
         twstAx =  'ry'
         sqshAx = [ 'sx' , 'sz' ]
         aimVec =  [ 0 , 1 , 0 ]
         invAimVec = [ 0 , -1 , 0 ]
         upVec = [ 0 , 1 , 0 ]
    
    elif axis == 'z' :
         postAx = 'tz'
         twstAx =  'rz'
         sqshAx = [ 'sx' , 'sy' ]
         aimVec = [ 0 , 0 , 1 ]
         invAimVec = [ 0 , 0 , -1 ]
         upVec = [ 0 , 1 , 0 ]
    
    core.snap( spine1TmpJnt , rbnRootCtrlZro )
    core.snap( spine3TmpJnt , rbnCtrlZro )
    core.snap( spine5TmpJnt , rbnEndCtrlZro )
    
    core.pntCons( 1 , rbnRootCtrl , rbnEndCtrl , rbnCtrlZro )
    
    aimMidCons = mc.rename( mc.aimConstraint( rbnEndCtrl , rbnCtrlAim , mo = True , aim = aimVec , u = upVec , wut = "objectrotation" , wu = upVec , wuo = rbnCtrlZro ) , '%s_aimCons' %rbnCtrlAim )
    
    #-- Bind skin
    rbnSkc = mc.skinCluster( rbnRootJnt , rbnMidJnt , rbnEndJnt , spineRbnNrb , dr = 7 , mi = 2 , n = 'spine%sRbn_skc' %elem )[0]
    
    mc.skinPercent( rbnSkc , '%s.cv[6][0:3]' %spineRbnNrb , tv = [ rbnEndJnt , 1 ] )
    mc.skinPercent( rbnSkc , '%s.cv[5][0:3]' %spineRbnNrb , tv = [ rbnMidJnt , 0.2 ] )
    mc.skinPercent( rbnSkc , '%s.cv[4][0:3]' %spineRbnNrb , tv = [ rbnEndJnt , 0.3 ] )
    mc.skinPercent( rbnSkc , '%s.cv[3][0:3]' %spineRbnNrb , tv = [ rbnMidJnt , 1 ] )
    mc.skinPercent( rbnSkc , '%s.cv[2][0:3]' %spineRbnNrb , tv = [ rbnRootJnt , 0.3 ] )
    mc.skinPercent( rbnSkc , '%s.cv[1][0:3]' %spineRbnNrb , tv = [ rbnRootJnt , 0.8 ] )
    mc.skinPercent( rbnSkc , '%s.cv[0][0:3]' %spineRbnNrb , tv = [ rbnRootJnt , 1 ] )
    
    #-- Detail controls
    #-- Dict
    rbnPosGrpDict = []
    posiDtlDict = []
    rbnDtlAimDict = []
    rbnDtlJntDict = []
    rbnDtlJntZroDict = []
    rbnDtlCtrlDict = []
    rbnDtlCtrlZroDict = []
    
    #-- Group
    rbnDetailGrp = core.transform( 'spine%sRbnDtl_grp' %elem )
    
    for i in range( 1,6 ) :
        exception = ( 1 , 5 )
        if i in exception  :
            rbnJnt = core.addJnt( 'spine%s%sRbnDtl_jnt' %( elem , i ))
            rbnJntZro = core.addGrp( rbnJnt )
        
            rbnDtlJntDict.append( rbnJnt )
            rbnDtlJntZroDict.append( rbnJntZro )
            
            if i == 1 :
                core.snap( spine1TmpJnt , rbnJntZro )
            else :
                core.snap( spine5TmpJnt , rbnJntZro )
                
            #-- Controls
            rbnDtlCtrl = core.addCtrl( 'spine%s%sRbnDtl_ctrl' %( elem , i ) , 'circle' , 'cyan' , jnt = False )
            rbnDtlCtrlZro = core.addGrp( rbnDtlCtrl )
            core.snap( rbnJntZro , rbnDtlCtrlZro )
            
            rbnDtlCtrlDict.append( rbnDtlCtrl )
            rbnDtlCtrlZroDict.append( rbnDtlCtrlZro )
            
            #-- Shape
            core.scaleShape( rbnDtlCtrl , charSize )
            
            #-- Rig process
            core.parCons( 0 , rbnDtlCtrl , rbnJntZro )
            
            mc.connectAttr( '%sShape.detailVis' %rbnCtrl , '%s.v' %rbnDtlCtrlZro )
            
        else :
            #-- Posi group
            rbnPosGrp = core.transform( 'spine%s%sRbnDtlPos_grp' %( elem , i ))
            posi = core.pointOnSurfaceInfo( 'spine%s%sRbnDtlPos_posi' %( elem , i ))
            
            mc.setAttr( '%s.turnOnPercentage' %posi , 1 )
            mc.setAttr ( '%s.parameterV' %posi , 0.5 )
            
            mc.connectAttr( '%s.worldSpace[0]' %spineRbnNrb , '%s.inputSurface' %posi )
            mc.connectAttr ( '%s.position' %posi , '%s.t' %rbnPosGrp )
            
            rbnPosGrpDict.append( rbnPosGrp )
            posiDtlDict.append( posi )
            
            #-- Aim constraint
            rbnAim = mc.createNode( 'aimConstraint' , n = 'spine%s%sRbnDtl_aimCons' %( elem , i ))
            
            mc.setAttr( '%s.a' %rbnAim , aimVec[0] , aimVec[1] , aimVec[2] )
            mc.setAttr( '%s.u' %rbnAim , upVec[0] , upVec[1] , upVec[2] )
            
            mc.connectAttr( '%s.n' %posi , '%s.wu' %rbnAim )
            mc.connectAttr( '%s.tu' %posi , '%s.tg[0].tt' %rbnAim )
            mc.connectAttr( '%s.cr' %rbnAim , '%s.r' %rbnPosGrp )
            
            rbnDtlAimDict.append( rbnAim )
            mc.parent( rbnAim , rbnPosGrp )
            
            mc.select( cl = True )
            
            #-- Joint
            rbnJnt = core.addJnt( 'spine%s%sRbnDtl_jnt' %( elem , i ))
            rbnJntZro = core.addGrp( rbnJnt )
            
            rbnDtlJntDict.append( rbnJnt )
            rbnDtlJntZroDict.append( rbnJntZro )
            
            #-- Controls
            rbnDtlCtrl = core.addCtrl( 'spine%s%sRbnDtl_ctrl' %( elem , i ) , 'circle' , 'cyan' , jnt = False )
            rbnDtlCtrlZro = core.addGrp( rbnDtlCtrl )
            core.snap( rbnJntZro , rbnDtlCtrlZro )
            
            rbnDtlCtrlDict.append( rbnDtlCtrl )
            rbnDtlCtrlZroDict.append( rbnDtlCtrlZro )
            
            #-- Shape
            core.scaleShape( rbnDtlCtrl , charSize )
            
            #-- Rig process
            core.parCons( 0 , rbnPosGrp , rbnDtlCtrlZro )
            core.parCons( 0 , rbnDtlCtrl , rbnJntZro )
            
            mc.connectAttr( '%sShape.detailVis' %rbnCtrl , '%s.v' %rbnDtlCtrlZro )
    
    mc.setAttr ( '%s.parameterU' %posiDtlDict[0] , 0.25 )
    mc.setAttr ( '%s.parameterU' %posiDtlDict[1] , 0.5 )
    mc.setAttr ( '%s.parameterU' %posiDtlDict[2] , 0.75 )
    
    
    #-- Cleanup
    for grp in ( spineRigGrp , spineJntGrp ) :
        core.setLock( grp , True , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
    
    
    ## Fk Rig ##
    if spineFk == True :
        #-- Group
        spineFkRigGrp = core.transform( 'spineFkRig_grp' )
        spineFkJntGrp = core.transform( 'spineFkJnt_grp' )
        core.snap( parent , spineFkRigGrp )
        core.snap( parent , spineFkJntGrp )
        
        #-- Joint
        spineFkJnt = core.addJnt( 'spineFk_jnt' , spine1TmpJnt )
        spineMidFkJnt = core.addJnt( 'spineMidFk_jnt' , spine3TmpJnt )
        chestFkJnt = core.addJnt( 'chestFk_jnt' , spine5TmpJnt )
        
        core.parent( chestFkJnt , spineMidFkJnt , spineFkJnt )
        
        #-- Controls
        spineFkCtrl = core.addCtrl( 'spineFk_ctrl' , 'circle' , 'red' )
        spineFkGmbl = core.addGimbal( spineFkCtrl )
        spineFkZro = core.addGrp( spineFkCtrl )
        spineFkOfst = core.addGrp( spineFkCtrl , 'Ofst' )
        core.snap( spineFkJnt , spineFkZro )
        
        spineMidFkCtrl = core.addCtrl( 'spineMidFk_ctrl' , 'circle' , 'red' )
        spineMidFkGmbl = core.addGimbal( spineMidFkCtrl )
        spineMidFkZro = core.addGrp( spineMidFkCtrl )
        spineMidFkOfst = core.addGrp( spineMidFkCtrl , 'Ofst' )
        core.snap( spineMidFkJnt , spineMidFkZro )
        
        chestFkCtrl = core.addCtrl( 'chestFk_ctrl' , 'cube' , 'red' )
        chestFkGmbl = core.addGimbal( chestFkCtrl )
        chestFkZro = core.addGrp( chestFkCtrl )
        chestFkOfst = core.addGrp( chestFkCtrl , 'Ofst' )
        core.snap( chestFkJnt , chestFkZro )
        
        #-- Shape
        for ctrl in ( spineFkCtrl , spineFkGmbl , spineMidFkCtrl , spineMidFkGmbl , chestFkCtrl , chestFkGmbl ) :
            core.scaleShape( ctrl , charSize * 1.8 )
        
        #-- Rotate order
        for each in ( spineFkCtrl , spineMidFkCtrl , chestFkCtrl , spineFkJnt , spineMidFkJnt , chestFkJnt ) :
            core.setRotateOrder( each , rotateOrder )
        
        #-- Rig process
        core.parCons( 0 , spineFkGmbl , spineFkJnt )
        core.parCons( 0 , spineMidFkGmbl , spineMidFkJnt )
        core.parCons( 0 , chestFkGmbl , chestFkJnt )
            
        #-- Hierarchy
        core.parent( spineMidFkZro , spineFkGmbl )
        core.parent( chestFkZro , spineMidFkGmbl )
        
        core.parent( spine1FkZro , spineFkRigGrp , spineRigGrp )
        core.parent( spine1FkPosJnt , spineFkJntGrp , spineJntGrp )
        
        #-- Cleanup
        for grp in ( spineFkRigGrp , spineFkJntGrp , spine1FkZro , spine2FkZro , spine3FkZro , spine4FkZro , spine1FkOfst , spine2FkOfst , spine3FkOfst , spine4FkOfst ) :
            core.setLock( grp , True , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
        
        for gmbl in ( spine1FkGmbl , spine2FkGmbl , spine3FkGmbl , spine4FkGmbl ) :
            core.setLockHide( gmbl , 'sx' , 'sy' , 'sz' , 'v' )
        
        for ctrl in ( spine1FkCtrl , spine2FkCtrl , spine3FkCtrl , spine4FkCtrl ) :
            core.setLockHide( ctrl , 'v' )
        
    mc.select( cl = True )