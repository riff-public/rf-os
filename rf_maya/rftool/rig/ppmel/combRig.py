import maya.cmds as mc
import maya.mel as mm
import ppmel.core as core
reload(core)

def combRig() :
    consSkinJnt()
    importDelNamespace()
    
    sels = mc.ls( '*_jnt' , '*_skinJnt' )
    for sel in sels :
        mc.setAttr( '%s.segmentScaleCompensate' %sel , 0 )
        
    mc.setAttr( 'skin_grp.v', 0 )
    
    for grp in ( 'allDtlCtrl_grp' , 'allDtlJnt_grp' , 'stillAddRig_grp' ) :
        mc.setAttr( '%s.inheritsTransform' %grp , 0 )
    
    mc.parent( 'allGeo_grp' , 'geo_grp')

def importDelNamespace () :
    # run script delete namespace #
    reffile = mc.file ( q = True , r = True )
    size = len ( reffile )

    if size == 0 :
        removeNamespace ()
    else :
        refNamespace ()

def refNamespace () :
    # not import reference #
    reffile = mc.file ( q = True , r = True )

    for ref in reffile :
        refprefix = mc.referenceQuery ( ref , referenceNode = True ).replace ( "RN" , "" )
        mc.file ( ref , ir = True )
        mc.namespace ( mv = [ refprefix ,":" ] , force = True )
        mc.namespace ( rm = refprefix )

def removeNamespace () :
    # import reference #
    sel = mc.ls ( sl = True )
    size = len ( sel )
    
    if size == 0 :
        mc.warning ( 'No Select Object. Please Select One Object.' )
    else :
        refprefix = sel[0].split(':')[0]
        mc.namespace ( mv = [ refprefix ,":" ] , force = True )
        mc.namespace ( rm = refprefix )

def consSkinJnt() :
    
    # Constraint skinJnt to jnt
    skinJnts = mc.ls( '*:*_skinJnt' )
    
    for skinJnt in skinJnts :
        split = skinJnt.split('_skinJnt')[0]
        nameSpace , name = split.split( ':' )
        
        try :
            core.parCons( 1 , '*:%s_jnt' %name , skinJnt )
            core.sclCons( 1 , '*:%s_jnt' %name , skinJnt )
        except :
            pass
    
    '''
    # Parent skinJnt to jnt
    skinJnts = mc.ls( '*_skinJnt' )
    
    for skinJnt in skinJnts :
        name = skinJnt.split('_skinJnt')[0]
        
        try :
            mc.parent( skinJnt , '*%s_jnt' %name )
        except :
            pass
    
    '''