import maya.cmds as mc
import re
import ppmel.core as core
reload(core)

# Draw curve template

def rigTail( name = 'tail' ,
             numCtrl  = 15 ,
             curveTmp = 'tail_cuvTemp'  ,
             curveUpVecTmp = 'tailUpvec_cuvTemp'  ,
             parent = 'pelvis_jnt' ,
             animGrp = 'anim_grp' ,
             skinGrp = 'skin_grp' ,
             jntGrp = 'jnt_grp' ,
             stillGrp = 'still_grp' ,
             ikhGrp = 'ikh_grp' ,
             side = '' ,
             elem = '' ,
             axis = 'y' ,
             charSize = 50 ) :
    
    
    #-- Info
    if 'x' in axis :
        aimVec = [ 1 , 0 , 0 ]
        invAimVec = [ -1 , 0 , 0 ]
        upVec = [ 0 , 0 , 1 ]
    
    elif 'y' in axis :
        aimVec =  [ 0 , 1 , 0 ]
        invAimVec = [ 0 , -1 , 0 ]
        upVec = [ 0 , 0 , 1 ]
    
    elif 'z' in axis :
        aimVec = [ 0 , 0 , 1 ]
        invAimVec = [ 0 , 0 , -1 ]
        upVec = [ 0 , 1 , 0 ]
        
    fkNum = numCtrl
    ikNum = ((numCtrl-1)*3)+numCtrl
    
    if side :
        side = '_%s' %side
    else :
        side = ''
    
    
    ## Main Rig ##
    #-- Group
    rigGrp = core.transform( '%s%sRig%s_grp' %( name , elem , side ))
    rigJntGrp = core.transform( '%s%sJnt%s_grp' %( name , elem , side ))
    rigIkhGrp = core.transform( '%s%sIkh%s_grp' %( name , elem , side ))
    rigStillGrp = core.transform( '%s%sStill%s_grp' %( name , elem , side ))
    core.parCons( 0 , parent , rigGrp )
    core.parent( rigGrp , animGrp )
    core.parent( rigJntGrp , jntGrp )
    core.parent( rigIkhGrp , ikhGrp )
    core.parent( rigStillGrp , stillGrp )
    
    
    #-- Curve for Draw Joint
    cuvPosi = mc.rename( mc.duplicate( curveTmp )[0] , 'tempPosi_cuv' )
    cuvUpvec = mc.rename( mc.duplicate( curveUpVecTmp )[0] , 'tempUpvec_cuv' )
    mc.rebuildCurve( cuvPosi , rt = 0 , s = ((numCtrl-1)*3)+(numCtrl-1))
    mc.rebuildCurve( cuvUpvec , rt = 0 , s = ((numCtrl-1)*3)+(numCtrl-1))
    
    #-- Joint
    jntFkDict = []
    jntIkDict = []
    jntIkPosDict = []
    jntIkUpvecDict = []
    
    eps = mc.getAttr( '%s.spans' %cuvPosi )
        
    # Fk
    ep = 0
    numLoop = 1
    while ep <= eps :
        posi = mc.pointPosition( '%s.ep[%s]' %(cuvPosi , ep))
        jntFk = core.addJnt( '%sFk%s%s%s_jnt' %(name , elem , numLoop , side))
        mc.setAttr( '%s.t' %jntFk , posi[0] , posi[1] , posi[2] )
        jntFkDict.append(jntFk)
        
        ep += 4
        numLoop += 1
        
    # Ik
    num = 1
    ep = 0
    while num <= numCtrl :
        for each in [ '' , 'A' , 'B' , 'C' ] :
            if not ep > ((numCtrl-1)*3)+numCtrl-1 :
                posi = mc.pointPosition( '%s.ep[%s]' %(cuvPosi , ep))
                posiUpvec = mc.pointPosition( '%s.ep[%s]' %(cuvUpvec , ep))
                jntIkPos = core.addJnt( '%sIk%s%s%sPos%s_jnt' %(name , elem , num , each , side))
                jntIk = core.addJnt( '%sIk%s%s%s%s_jnt' %(name , elem , num , each , side))
                jntIkUpvec = core.addJnt( '%sIk%s%s%sUpvec%s_jnt' %(name , elem , num , each , side))
                mc.setAttr( '%s.t' %jntIkPos , posi[0] , posi[1] , posi[2] )
                mc.setAttr( '%s.t' %jntIk , posi[0] , posi[1] , posi[2] )
                mc.setAttr( '%s.t' %jntIkUpvec , posiUpvec[0] , posiUpvec[1] , posiUpvec[2] )
                jntIkPosDict.append(jntIkPos)
                jntIkDict.append(jntIk)
                jntIkUpvecDict.append(jntIkUpvec)
            
            ep += 1
        num += 1
    
    # Orient Joint
    # Ik
    lenght = ((numCtrl-1)*3)+(numCtrl-1)
    for i in range(0,lenght) :
        if not i == lenght :
            core.orientJnt( jntIkPosDict[i+1] , jntIkPosDict[i] , jntIkUpvecDict[i] , aimVec = aimVec , upVec = upVec )
        
        core.snapOrient( jntIkPosDict[-2] , jntIkPosDict[-1] )
    
    for i in range(0,((numCtrl-1)*3)+(numCtrl-1)+1) :
        core.snapJntOrient( jntIkPosDict[i] , jntIkDict[i] )
        mc.parent( jntIkDict[i] , jntIkPosDict[i] )
    
    mc.delete(jntIkUpvecDict)
    
    # Fk
    for each in jntFkDict :
        nameSplit = each.split('Fk')
        core.snap( '%sIk%s' %(nameSplit[0] , nameSplit[-1]) , each )
    
    #-- Joint Hierarchy
    # Fk
    lenArrayFkDict = numCtrl-1
    
    while lenArrayFkDict > 0 :
        mc.parent( jntFkDict[lenArrayFkDict] , jntFkDict[lenArrayFkDict-1] )
        
        lenArrayFkDict -= 1
    
    core.freeze(jntFkDict[0])
    
    # Ik
    lenArrayIkDict = ((numCtrl-1)*3)+(numCtrl-1)
    
    while lenArrayIkDict > 0 :
        mc.parent( jntIkPosDict[lenArrayIkDict] , jntIkPosDict[lenArrayIkDict-1] )
        
        lenArrayIkDict -= 1
    
    core.freeze(jntIkPosDict[0])
    
    #-- Controls
    # Main Controls
    mainCtrl = core.addCtrl( '%s%s%s_ctrl' %(name , elem , side) , 'stick' , 'green' , jnt = False )
    mainZro = core.addGrp( mainCtrl )
    core.scaleShape( mainCtrl , charSize )
    
    core.setLockHide( mainCtrl , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
    
    core.addAttr( mainCtrl , 'autoStretch' )
    core.addAttr( mainCtrl , 'autoSquash' )
    core.addAttr( mainCtrl , 'ampSquash' )
    core.setVal( '%s.autoStretch' %mainCtrl , 1 )
    core.setVal( '%s.ampSquash' %mainCtrl , 1 )
    
    core.parCons( 0 , jntFkDict[0] , mainZro )
    mc.parent( mainZro , rigGrp )
    
    # Fk Ik Controls
    fkCtrlArray = []
    fkGmblArray = []
    fkZroArray = []
    fkOfstArray = []
    
    ikCtrlArray = []
    ikGmblArray = []
    ikZroArray = []
    ikOfstArray = []
    
    ampSqshArray = []
    
    for i in range(1,numCtrl+1) :
        fkCtrl = core.addCtrl( '%sFk%s%s%s_ctrl' %(name , elem , str(i) , side) , 'circle' , 'red' , jnt = False )
        fkGmbl = core.addGimbal( fkCtrl )
        fkZro = core.addGrp( fkCtrl )
        fkOfst = core.addGrp( fkCtrl , 'Ofst' )
        
        ikCtrl = core.addCtrl( '%sIk%s%s%s_ctrl' %(name , elem , str(i) , side) , 'square' , 'blue' , jnt = False )
        ikGmbl = core.addGimbal( ikCtrl )
        ikZro = core.addGrp( ikCtrl )
        ikOfst = core.addGrp( ikCtrl , 'Ofst' )
        
        for ctrl in ( fkCtrl , fkGmbl , ikCtrl , ikGmbl ) :
            core.setLockHide( ctrl , 'sx' , 'sy' , 'sz' , 'v' )
        
        core.addAttr( ikCtrl , 'squash' )
        ampSqshMdv = core.multiplyDivide( '%sIk%s%sAmpSqsh%s_mdv' %(name , elem , str(i) , side))
        
        mc.connectAttr( '%s.squash' %ikCtrl , '%s.i1x' %ampSqshMdv )
        mc.connectAttr( '%s.ampSquash' %mainCtrl , '%s.i2x' %ampSqshMdv )
        
        core.snap( jntFkDict[i-1] , fkZro )
        core.snap( jntFkDict[i-1] , ikZro )
        
        fkCtrlArray.append(fkCtrl)
        fkGmblArray.append(fkGmbl)
        fkZroArray.append(fkZro)
        fkOfstArray.append(fkOfst)
        
        ikCtrlArray.append(ikCtrl)
        ikGmblArray.append(ikGmbl)
        ikZroArray.append(ikZro)
        ikOfstArray.append(ikOfst)
        
        ampSqshArray.append(ampSqshMdv)
        
    for fkCtrl in fkCtrlArray :
        core.scaleShape( fkCtrl , charSize )
        
    for fkGmbl in fkGmblArray :
        core.scaleShape( fkGmbl , charSize )
        
    for ikCtrl in ikCtrlArray :
        core.scaleShape( ikCtrl , charSize )
        
    for ikGmbl in ikGmblArray :
        core.scaleShape( ikGmbl , charSize )
        
    #-- Ctrl Hierarchy
    lenght = numCtrl-1
    
    while lenght >= 0 :
        mc.parent( ikZroArray[lenght] , fkGmblArray[lenght] )
        
        if not lenght == 0 :
            mc.parent( fkZroArray[lenght] , fkGmblArray[lenght-1] )
            
        lenght -= 1
    
    mc.parent( fkZroArray[0] , rigGrp )
    
    #-- Rig Process
    # Fk
    core.addLocalWorld( fkCtrlArray[0] , animGrp , rigGrp , fkZroArray[0] , 'orient' )

    # Ik
    for i in range(0,numCtrl) :
        core.parCons( 0 , ikGmblArray[i] , jntFkDict[i] )
        
    cuvIkSpine = mc.rename( mc.duplicate( curveTmp )[0] , '%sIk%s%s_cuv' %( name , elem , side ) )
    mc.rebuildCurve( cuvIkSpine , rt = 0 , s = (numCtrl-1)*2)
    
    ikhSpineHandle = mc.ikHandle( n = '%sIk%s%s_ikh' %( name , elem , side ) , sol = 'ikSplineSolver' , sj = jntIkPosDict[0] , ee = jntIkPosDict[-1] , scv = False , ccv = False , c = cuvIkSpine )
    spineSkc = mc.skinCluster( jntFkDict , cuvIkSpine , dr = 7 , mi = 2 , n = '%s%s%s_skc' %( name , elem , side ))[0]
    
    # SkinCluster
    loop = 0
    for i in range(1,(numCtrl+(numCtrl-1))+1) :
        if i % 2 == 0 :
            j = i/2
            mc.skinPercent( spineSkc , '%s.cv[%s]' %(cuvIkSpine,i) , tv = [( jntFkDict[j-1] , 0.5 ),( jntFkDict[j] , 0.5 )] )
        else :
            mc.skinPercent( spineSkc , '%s.cv[%s]' %(cuvIkSpine,i) , tv = [ jntFkDict[loop] , 1 ])
            loop += 1
    
    mc.skinPercent( spineSkc , '%s.cv[1]' %(cuvIkSpine) , tv = [ jntFkDict[0] , 1 ])
    mc.skinPercent( spineSkc , '%s.cv[%s]' %(cuvIkSpine,numCtrl*2) , tv = [ jntFkDict[-1] , 1 ])
    
    deformerSet = core.getDef( cuvIkSpine , 'tweak' )
    mc.rename( deformerSet[0] , '%sIk%s%s_tweak' %( name , elem , side ))
    mc.rename( deformerSet[1] , '%sIk%s%s_tweakSet' %( name , elem , side ))
    
    # Auto Stretch
    cuvInfo = core.curveInfo( '%s%s%s_cif' %( name , elem , side ))
    autoStrtBcl = core.blendColors( '%s%sAutoStrt%s_bcl' %( name , elem , side ))
    autoStrtMdv = core.multiplyDivide( '%s%sAutoStrt%s_mdv' %( name , elem , side ))
    gblSclMdv = core.multiplyDivide( '%s%sGblScl%s_mdv' %( name , elem , side ))
    
    mc.setAttr( '%s.operation' %autoStrtMdv , 2 )
    
    mc.connectAttr( '%s.autoStretch' %mainCtrl , '%s.b' %autoStrtBcl )
    mc.connectAttr( '%s.worldSpace[0]' %cuvIkSpine , '%s.inputCurve' %cuvInfo )
    mc.connectAttr( '%s.arcLength' %cuvInfo , '%s.color1R' %autoStrtBcl )
    mc.connectAttr( '%s.outputR' %autoStrtBcl , '%s.i1x' %autoStrtMdv )
    
    mc.connectAttr( 'master_ctrl.globalScale' , '%s.i1x' %gblSclMdv )
    mc.connectAttr( '%s.ox' %gblSclMdv , '%s.color2R' %autoStrtBcl )
    mc.connectAttr( '%s.ox' %gblSclMdv , '%s.i2x' %autoStrtMdv )
    mc.setAttr( '%s.i2x' %gblSclMdv , mc.getAttr( '%s.arcLength' %cuvInfo ))
    
    for jnt in jntIkPosDict :
        mc.connectAttr( '%s.ox' %autoStrtMdv , '%s.s%s' %(jnt,axis) )
    
    # Squash
    sqshMdvArray = []
    
    for ikCtrl in ikCtrlArray :
        n = ikCtrl.split('_')[0]
        sqshMdv = core.multiplyDivide( '%sSqsh%s_mdv' %( n , side ))
        
        ampSqshMdv = '%sAmpSqsh%s_mdv' %( n , side )
        
        for attr in ( 'i1x' , 'i1y' , 'i1z' ) :
            mc.connectAttr( '%s.ox' %ampSqshMdv , '%s.%s' %( sqshMdv , attr ))
        
        mc.setAttr( '%s.i2x' %sqshMdv , 0.75 )
        mc.setAttr( '%s.i2y' %sqshMdv , 0.5 )
        mc.setAttr( '%s.i2z' %sqshMdv , 0.25 )
        
        sqshMdvArray.append(sqshMdv)
    
    if 'x' in axis :
        s1 = 'y'
        s2 = 'z'
    elif 'y' in axis :
        s1 = 'x'
        s2 = 'z'
    elif 'z' in axis :
        s1 = 'x'
        s2 = 'y'
        
    for jnt in jntIkDict :
        n = jnt.split('_')[0]
        
        sqshPma = core.plusMinusAverage( '%sSqsh%s_pma' %( n , side ))
        core.addAttr( sqshPma , 'constant' )
        core.setVal( '%s.constant' %sqshPma , 1 )
        
        mc.connectAttr( '%s.constant' %sqshPma , '%s.i1[0]' %sqshPma )
        mc.connectAttr( '%s.o1' %sqshPma , '%s.s%s' %(jnt,s1))
        mc.connectAttr( '%s.o1' %sqshPma , '%s.s%s' %(jnt,s2))
        
    for i in range(1,len(sqshMdvArray)+1) :
        mc.connectAttr( '%s.ox' %ampSqshArray[i-1] , '%sIk%s%sSqsh%s_pma.i1[1]' %(name , elem , i , side))
        
        if not i == len(sqshMdvArray) :
            mc.connectAttr( '%s.ox' %sqshMdvArray[i-1] , '%sIk%s%sASqsh%s_pma.i1[1]' %(name , elem , i , side))
            mc.connectAttr( '%s.oy' %sqshMdvArray[i-1] , '%sIk%s%sBSqsh%s_pma.i1[1]' %(name , elem , i , side))
            mc.connectAttr( '%s.oz' %sqshMdvArray[i-1] , '%sIk%s%sCSqsh%s_pma.i1[1]' %(name , elem , i , side))
            
            mc.connectAttr( '%s.ox' %sqshMdvArray[i] , '%sIk%s%sCSqsh%s_pma.i1[2]' %(name , elem , i , side))
            mc.connectAttr( '%s.oy' %sqshMdvArray[i] , '%sIk%s%sBSqsh%s_pma.i1[2]' %(name , elem , i , side))
            mc.connectAttr( '%s.oz' %sqshMdvArray[i] , '%sIk%s%sASqsh%s_pma.i1[2]' %(name , elem , i , side))
    
    # Twist
    # createNode Pma
    for jnt in jntIkDict :
        n = jnt.split('_')[0]
        
        twistPma = core.plusMinusAverage( '%sTwist%s_pma' %( n , side ))
        
        mc.connectAttr( '%s.o1' %twistPma , '%s.r%s' %(jnt , axis))
    
    # createNode mdv
    twistMdvArray = []
    
    for ikCtrl in ikCtrlArray :
        n = ikCtrl.split('_')[0]
        twistMdv = core.multiplyDivide( '%sTwist%s_mdv' %( n , side ))
        
        for attr in ( 'i1x' , 'i1y' , 'i1z' ) :
            mc.connectAttr( '%s%s_jnt.r%s' %( n , side , axis ) , '%s.%s' %( twistMdv , attr ))
        
        mc.setAttr( '%s.i2x' %twistMdv , 0.75 )
        mc.setAttr( '%s.i2y' %twistMdv , 0.5 )
        mc.setAttr( '%s.i2z' %twistMdv , 0.25 )
        
        twistMdvArray.append(twistMdv)
    
    # connecting Node
    i = 1
    while i <= numCtrl :
        k = 0
        for j in range(0,i) :
            
            for ctrlFk in ( fkCtrlArray[j] , fkGmblArray[j] ) :
                mc.connectAttr( '%s.r%s' %(ctrlFk , axis) , '%sIk%s%sTwist%s_pma.i1[%s]' %(name , elem , i , side , k))
                k += 1
            
            if j == i-1 :
                for ctrlIk in ( ikCtrlArray[j] , ikGmblArray[j] ) :
                    mc.connectAttr( '%s.r%s' %(ctrlIk , axis) , '%sIk%s%sTwist%s_pma.i1[%s]' %(name , elem , i , side , k))
                    k += 1
        i += 1
    
    for i in range(1,len(twistMdvArray)+1) :
        
        if not i == len(twistMdvArray) :
            mc.connectAttr( '%s.ox' %twistMdvArray[i-1] , '%sIk%s%sATwist%s_pma.i1[1]' %(name , elem , i , side))
            mc.connectAttr( '%s.oy' %twistMdvArray[i-1] , '%sIk%s%sBTwist%s_pma.i1[1]' %(name , elem , i , side))
            mc.connectAttr( '%s.oz' %twistMdvArray[i-1] , '%sIk%s%sCTwist%s_pma.i1[1]' %(name , elem , i , side))
            
            mc.connectAttr( '%s.ox' %twistMdvArray[i] , '%sIk%s%sCTwist%s_pma.i1[2]' %(name , elem , i , side))
            mc.connectAttr( '%s.oy' %twistMdvArray[i] , '%sIk%s%sBTwist%s_pma.i1[2]' %(name , elem , i , side))
            mc.connectAttr( '%s.oz' %twistMdvArray[i] , '%sIk%s%sATwist%s_pma.i1[2]' %(name , elem , i , side))
    
    #-- CleanUp
    mc.delete( cuvPosi , cuvUpvec )
    
    mc.parent( jntFkDict[0] , rigJntGrp )
    mc.parent( jntIkPosDict[0] , parent )
    mc.parent( ikhSpineHandle[0] , rigIkhGrp )
    mc.parent( cuvIkSpine , rigStillGrp )