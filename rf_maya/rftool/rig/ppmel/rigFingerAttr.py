import maya.cmds as mc
import maya.mel as mm
import ppmel.core as core
reload( core )
           

def rigFingerAttr( ctrl = 'arm_lft_ctrl' ,
                   side = 'lft' ,
                   type = 'hand' ,  # hand , foot
                   elem = '' ,
                   num = 5 , # hand default 5 , foot default 4
                   finger = ( 'thumb' , 'index' , 'middle' , 'ring' , 'pinky' )) :
                
    ## Main Rig ##        
    #-- Value
    attrDict = [ ('thumb' , None ) ,
                 ('thumb1FistRx' , 0) ,
                 ('thumb1FistRy' , 0) ,
                 ('thumb1FistRz' , 0) ,
                 
                 ('thumb2FistRx' , -4.5) ,
                 ('thumb2FistRy' , 0) ,
                 ('thumb2FistRz' , 0) ,
                 
                 ('thumb3FistRx' , -9) ,
                 ('thumb3FistRy' , 0) ,
                 ('thumb3FistRz' , 0) ,
                 
                 ('thumb1CupRx' , 0) ,
                 ('thumb1CupRy' , 0) ,
                 ('thumb1CupRz' , 0) ,
                 
                 ('thumb2CupRx' , 0) ,
                 ('thumb2CupRy' , 0) ,
                 ('thumb2CupRz' , 0) ,
                 
                 ('thumb3CupRx' , 0) ,
                 ('thumb3CupRy' , 0) ,
                 ('thumb3CupRz' , 0) ,
                 
                 ('thumb1Slide' , 0) ,
                 ('thumb2Slide' , 3.5) ,
                 ('thumb3Slide' , -8) ,
                 
                 ('thumb1Scruch' , 0) ,
                 ('thumb2Scruch' , 3.5) ,
                 ('thumb3Scruch' , -8) ,
                 
                 ('thumbSpread' , -3) ,
                 ('thumbBreak' , 0) ,
                 ('thumbFlex' , 0) ,
                 
                 ('thumbBaseSpread' , 0) ,
                 ('thumbBaseBreak' , 0) ,
                 ('thumbBaseFlex' , 0) ,
                 
                 ('index' , None ) ,
                 ('index1FistRx' , 0) ,
                 ('index1FistRy' , 0) ,
                 ('index1FistRz' , 0) ,
                 
                 ('index2FistRx' , -9) ,
                 ('index2FistRy' , 0) ,
                 ('index2FistRz' , 0) ,
                 
                 ('index3FistRx' , -9) ,
                 ('index3FistRy' , 0) ,
                 ('index3FistRz' , 0) ,
                 
                 ('index4FistRx' , -9) ,
                 ('index4FistRy' , 0) ,
                 ('index4FistRz' , 0) ,
                 
                 ('index1CupRx' , 0) ,
                 ('index1CupRy' , 0) ,
                 ('index1CupRz' , 0) ,
                 
                 ('index2CupRx' , -1) ,
                 ('index2CupRy' , 0) ,
                 ('index2CupRz' , 0) ,
                 
                 ('index3CupRx' , -1) ,
                 ('index3CupRy' , 0) ,
                 ('index3CupRz' , 0) ,
                 
                 ('index4CupRx' , -1) ,
                 ('index4CupRy' , 0) ,
                 ('index4CupRz' , 0) ,
                 
                 ('index1Slide' , 0) ,
                 ('index2Slide' , 3.5) ,
                 ('index3Slide' , -8) ,
                 ('index4Slide' , 4.5) ,
                 
                 ('index1Scruch' , 1.5) ,
                 ('index2Scruch' , 2.5) ,
                 ('index3Scruch' , -8) ,
                 ('index4Scruch' , -8) ,
                 
                 ('indexSpread' , -4) ,
                 ('indexBreak' , -4.5) ,
                 ('indexFlex' , -9) ,
                 
                 ('indexBaseSpread' , -4) ,
                 ('indexBaseBreak' , -4.5) ,
                 ('indexBaseFlex' , -9) ,
                 
                 ('middle' , None ) ,
                 ('middle1FistRx' , 0) ,
                 ('middle1FistRy' , 0) ,
                 ('middle1FistRz' , 0) ,
                 
                 ('middle2FistRx' , -9) ,
                 ('middle2FistRy' , 0) ,
                 ('middle2FistRz' , 0) ,
                 
                 ('middle3FistRx' , -9) ,
                 ('middle3FistRy' , 0) ,
                 ('middle3FistRz' , 0) ,
                 
                 ('middle4FistRx' , -9) ,
                 ('middle4FistRy' , 0) ,
                 ('middle4FistRz' , 0) ,
                 
                 ('middle1CupRx' , -0.5) ,
                 ('middle1CupRy' , 0) ,
                 ('middle1CupRz' , 0) ,
                 
                 ('middle2CupRx' , -2.5) ,
                 ('middle2CupRy' , 0) ,
                 ('middle2CupRz' , 0) ,
                 
                 ('middle3CupRx' , -2.5) ,
                 ('middle3CupRy' , 0) ,
                 ('middle3CupRz' , 0) ,
                 
                 ('middle4CupRx' , -2.5) ,
                 ('middle4CupRy' , 0) ,
                 ('middle4CupRz' , 0) ,
                 
                 ('middle1Slide' , 0) ,
                 ('middle2Slide' , 3.5) ,
                 ('middle3Slide' , -8) ,
                 ('middle4Slide' , 4.5) ,
                 
                 ('middle1Scruch' , 1.5) ,
                 ('middle2Scruch' , 2.5) ,
                 ('middle3Scruch' , -8) ,
                 ('middle4Scruch' , -8) ,
                 
                 ('middleSpread' , 0) ,
                 ('middleBreak' , -4.5) ,
                 ('middleFlex' , -9) ,
                 
                 ('middleBaseSpread' , 0) ,
                 ('middleBaseBreak' , -4.5) ,
                 ('middleBaseFlex' , -9) ,
                 
                 ('ring' , None ) ,
                 ('ring1FistRx' , 0) ,
                 ('ring1FistRy' , 0) ,
                 ('ring1FistRz' , 0) ,
                 
                 ('ring2FistRx' , -9) ,
                 ('ring2FistRy' , 0) ,
                 ('ring2FistRz' , 0) ,
                 
                 ('ring3FistRx' , -9) ,
                 ('ring3FistRy' , 0) ,
                 ('ring3FistRz' , 0) ,
                 
                 ('ring4FistRx' , -9) ,
                 ('ring4FistRy' , 0) ,
                 ('ring4FistRz' , 0) ,
                 
                 ('ring1CupRx' , -1) ,
                 ('ring1CupRy' , 0) ,
                 ('ring1CupRz' , 0) ,
                 
                 ('ring2CupRx' , -5) ,
                 ('ring2CupRy' , 0) ,
                 ('ring2CupRz' , 0) ,
                 
                 ('ring3CupRx' , -5) ,
                 ('ring3CupRy' , 0) ,
                 ('ring3CupRz' , 0) ,
                 
                 ('ring4CupRx' , -5) ,
                 ('ring4CupRy' , 0) ,
                 ('ring4CupRz' , 0) ,
                 
                 ('ring1Slide' , 0) ,
                 ('ring2Slide' , 3.5) ,
                 ('ring3Slide' , -8) ,
                 ('ring4Slide' , 4.5) ,
                  
                 ('ring1Scruch' , 1.5) ,
                 ('ring2Scruch' , 2.5) ,
                 ('ring3Scruch' , -8) ,
                 ('ring4Scruch' , -8) ,
                 
                 ('ringSpread' , 3) ,
                 ('ringBreak' , -4.5) ,
                 ('ringFlex' , -9) ,
                 
                 ('ringBaseSpread' , 3 ) ,
                 ('ringBaseBreak' , -4.5) ,
                 ('ringBaseFlex' , -9) ,
                 
                 ('pinky' , None ) ,
                 ('pinky1FistRx' , 0) ,
                 ('pinky1FistRy' , 0) ,
                 ('pinky1FistRz' , 0) ,
                 
                 ('pinky2FistRx' , -9) ,
                 ('pinky2FistRy' , 0) ,
                 ('pinky2FistRz' , 0) ,
                 
                 ('pinky3FistRx' , -9) ,
                 ('pinky3FistRy' , 0) ,
                 ('pinky3FistRz' , 0) ,
                 
                 ('pinky4FistRx' , -9) ,
                 ('pinky4FistRy' , 0) ,
                 ('pinky4FistRz' , 0) ,
                 
                 ('pinky1CupRx' , -2) ,
                 ('pinky1CupRy' , 0) ,
                 ('pinky1CupRz' , 0) ,
                 
                 ('pinky2CupRx' , -7.5) ,
                 ('pinky2CupRy' , 0) ,
                 ('pinky2CupRz' , 0) ,
                 
                 ('pinky3CupRx' , -7.5) ,
                 ('pinky3CupRy' , 0) ,
                 ('pinky3CupRz' , 0) ,
                 
                 ('pinky4CupRx' , -7.5) ,
                 ('pinky4CupRy' , 0) ,
                 ('pinky4CupRz' , 0) ,
                 
                 ('pinky1Slide' , 0) ,
                 ('pinky2Slide' , 3.5) ,
                 ('pinky3Slide' , -8) ,
                 ('pinky4Slide' , 4.5) ,
                 
                 ('pinky1Scruch' , 1.5) ,
                 ('pinky2Scruch' , 2.5) ,
                 ('pinky3Scruch' , -8) ,
                 ('pinky4Scruch' , -8) ,
                 
                 ('pinkySpread' , 5) ,
                 ('pinkyBreak' , -4.5) ,
                 ('pinkyFlex' , -9) ,
                 
                 ('pinkyBaseSpread' , 5) ,
                 ('pinkyBaseBreak' , -4.5) ,
                 ('pinkyBaseFlex' , -9) ]
                 
    ctrlShape = core.getShape( ctrl )
    
    #-- Add attribute       
    if type == 'hand' :
        core.addAttr( ctrl , 'scaleHand' )
        core.setVal( '%s.scaleHand' %ctrl , 1 )
        
        #-- Rig process
        sclMdv = core.multiplyDivide( 'hand%sScale_%s_mdv' %( elem , side ))
        mc.connectAttr( '%s.scaleHand' %ctrl , '%s.i1x' %sclMdv )
        mc.connectAttr( '%s.ox' %sclMdv , 'wrist%s_%s_jnt.sx' %( elem , side ))
        mc.connectAttr( '%s.ox' %sclMdv , 'wrist%s_%s_jnt.sy' %( elem , side ))
        mc.connectAttr( '%s.ox' %sclMdv , 'wrist%s_%s_jnt.sz' %( elem , side ))
        mc.setAttr( '%s.i2x' %sclMdv , 1 )
        
    core.addAttrType( ctrl , '%s' %type.capitalize() )
    
    for attr in ( 'fist' , 'cup' , 'slide' , 'scruch' , 'spread' , 'break' , 'flex' , 'baseSpread' , 'baseBreak' , 'baseFlex' ) :
        core.addAttr( ctrl , attr )
    
    for attr in attrDict :
        for each in finger :
            if each in attr[0] :
                if attr[1] == None :
                    core.addAttrType( ctrlShape , attr[0] )
                else :
                    if not num == 5 :
                        if not '4' in attr[0] :
                            core.addAttr( ctrlShape , attr[0] )
                            core.setVal( '%s.%s' %( ctrlShape , attr[0] ) , attr[1] )
                    else :
                        core.addAttr( ctrlShape , attr[0] )
                        core.setVal( '%s.%s' %( ctrlShape , attr[0] ) , attr[1] )
        
    #-- Create node
    if 'foot' in type :
        type = ('%s' %type).capitalize()
    else :
        type = ''
        
    for each in finger :
        fist1Mdv = core.multiplyDivide( '%s%s1%sFist_%s_mdv' %( each , elem , type , side ))
        fist2Mdv = core.multiplyDivide( '%s%s2%sFist_%s_mdv' %( each , elem , type , side ))
        fist3Mdv = core.multiplyDivide( '%s%s3%sFist_%s_mdv' %( each , elem , type , side ))
        cup1Mdv = core.multiplyDivide( '%s%s1%sCup_%s_mdv' %( each , elem , type , side ))
        cup2Mdv = core.multiplyDivide( '%s%s2%sCup_%s_mdv' %( each , elem , type , side ))
        cup3Mdv = core.multiplyDivide( '%s%s3%sCup_%s_mdv' %( each , elem , type , side ))
        slide1Mdv = core.multiplyDivide( '%s%s1%sSlide_%s_mdv' %( each , elem , type , side ))
        slide2Mdv = core.multiplyDivide( '%s%s2%sSlide_%s_mdv' %( each , elem , type , side ))
        slide3Mdv = core.multiplyDivide( '%s%s3%sSlide_%s_mdv' %( each , elem , type , side ))
        scruch1Mdv = core.multiplyDivide( '%s%s1%sScruch_%s_mdv' %( each , elem , type , side ))
        scruch2Mdv = core.multiplyDivide( '%s%s2%sScruch_%s_mdv' %( each , elem , type , side ))
        scruch3Mdv = core.multiplyDivide( '%s%s3%sScruch_%s_mdv' %( each , elem , type , side ))
        spreadMdv = core.multiplyDivide( '%s%s%sSpread_%s_mdv' %( each , elem , type , side ))
        breakMdv = core.multiplyDivide( '%s%s%sBreak_%s_mdv' %( each , elem , type , side ))
        flexMdv = core.multiplyDivide( '%s%s%sFlex_%s_mdv' %( each , elem , type , side ))
        baseSpreadMdv = core.multiplyDivide( '%s%s%sBaseSpread_%s_mdv' %( each , elem , type , side ))
        baseBreakMdv = core.multiplyDivide( '%s%s%sBaseBreak_%s_mdv' %( each , elem , type , side ))
        baseFlexMdv = core.multiplyDivide( '%s%s%sBaseFlex_%s_mdv' %( each , elem , type , side ))
        
        if not 'thumb' in each :
            if num == 5 :
                fist4Mdv = core.multiplyDivide( '%s%s4%sFist_%s_mdv' %( each , elem , type , side ))
                cup4Mdv = core.multiplyDivide( '%s%s4%sCup_%s_mdv' %( each , elem , type , side ))
                slide4Mdv = core.multiplyDivide( '%s%s4%sSlide_%s_mdv' %( each , elem , type , side ))
                scruch4Mdv = core.multiplyDivide( '%s%s4%sScruch_%s_mdv' %( each , elem , type , side ))
                
        #-- Connect node
        mc.connectAttr( '%s.fist' %ctrl , '%s.i1x' %fist1Mdv )
        mc.connectAttr( '%s.fist' %ctrl , '%s.i1y' %fist1Mdv )
        mc.connectAttr( '%s.fist' %ctrl , '%s.i1z' %fist1Mdv )
        mc.connectAttr( '%s.fist' %ctrl , '%s.i1x' %fist2Mdv )
        mc.connectAttr( '%s.fist' %ctrl , '%s.i1y' %fist2Mdv )
        mc.connectAttr( '%s.fist' %ctrl , '%s.i1z' %fist2Mdv )
        mc.connectAttr( '%s.fist' %ctrl , '%s.i1x' %fist3Mdv )
        mc.connectAttr( '%s.fist' %ctrl , '%s.i1y' %fist3Mdv )
        mc.connectAttr( '%s.fist' %ctrl , '%s.i1z' %fist3Mdv )
        mc.connectAttr( '%s.cup' %ctrl , '%s.i1x' %cup1Mdv )
        mc.connectAttr( '%s.cup' %ctrl , '%s.i1x' %cup2Mdv )
        mc.connectAttr( '%s.cup' %ctrl , '%s.i1x' %cup3Mdv )
        mc.connectAttr( '%s.slide' %ctrl , '%s.i1x' %slide1Mdv )
        mc.connectAttr( '%s.slide' %ctrl , '%s.i1x' %slide2Mdv )
        mc.connectAttr( '%s.slide' %ctrl , '%s.i1x' %slide3Mdv )
        mc.connectAttr( '%s.scruch' %ctrl , '%s.i1x' %scruch1Mdv )
        mc.connectAttr( '%s.scruch' %ctrl , '%s.i1x' %scruch2Mdv )
        mc.connectAttr( '%s.scruch' %ctrl , '%s.i1x' %scruch3Mdv )
        mc.connectAttr( '%s.spread' %ctrl , '%s.i1z' %spreadMdv )
        mc.connectAttr( '%s.break' %ctrl , '%s.i1z' %breakMdv )
        mc.connectAttr( '%s.flex' %ctrl , '%s.i1x' %flexMdv )
        mc.connectAttr( '%s.baseSpread' %ctrl , '%s.i1z' %baseSpreadMdv )
        mc.connectAttr( '%s.baseBreak' %ctrl , '%s.i1z' %baseBreakMdv )
        mc.connectAttr( '%s.baseFlex' %ctrl , '%s.i1x' %baseFlexMdv )
         
        mc.connectAttr( '%s.%s1FistRx' %( ctrlShape , each ) , '%s.i2x' %fist1Mdv )
        mc.connectAttr( '%s.%s1FistRy' %( ctrlShape , each ) , '%s.i2y' %fist1Mdv )
        mc.connectAttr( '%s.%s1FistRz' %( ctrlShape , each ) , '%s.i2z' %fist1Mdv )
        mc.connectAttr( '%s.%s2FistRx' %( ctrlShape , each ) , '%s.i2x' %fist2Mdv )
        mc.connectAttr( '%s.%s2FistRy' %( ctrlShape , each ) , '%s.i2y' %fist2Mdv )
        mc.connectAttr( '%s.%s2FistRz' %( ctrlShape , each ) , '%s.i2z' %fist2Mdv )
        mc.connectAttr( '%s.%s3FistRx' %( ctrlShape , each ) , '%s.i2x' %fist3Mdv )
        mc.connectAttr( '%s.%s3FistRy' %( ctrlShape , each ) , '%s.i2y' %fist3Mdv )
        mc.connectAttr( '%s.%s3FistRz' %( ctrlShape , each ) , '%s.i2z' %fist3Mdv )
        mc.connectAttr( '%s.%s1CupRx' %( ctrlShape , each ) , '%s.i2x' %cup1Mdv )
        mc.connectAttr( '%s.%s1CupRy' %( ctrlShape , each ) , '%s.i2y' %cup1Mdv )
        mc.connectAttr( '%s.%s1CupRz' %( ctrlShape , each ) , '%s.i2z' %cup1Mdv )
        mc.connectAttr( '%s.%s2CupRx' %( ctrlShape , each ) , '%s.i2x' %cup2Mdv )
        mc.connectAttr( '%s.%s2CupRy' %( ctrlShape , each ) , '%s.i2y' %cup2Mdv )
        mc.connectAttr( '%s.%s2CupRz' %( ctrlShape , each ) , '%s.i2z' %cup2Mdv )
        mc.connectAttr( '%s.%s3CupRx' %( ctrlShape , each ) , '%s.i2x' %cup3Mdv )
        mc.connectAttr( '%s.%s3CupRy' %( ctrlShape , each ) , '%s.i2y' %cup3Mdv )
        mc.connectAttr( '%s.%s3CupRz' %( ctrlShape , each ) , '%s.i2z' %cup3Mdv )
        mc.connectAttr( '%s.%s1Slide' %( ctrlShape , each ) , '%s.i2x' %slide1Mdv )
        mc.connectAttr( '%s.%s2Slide' %( ctrlShape , each ) , '%s.i2x' %slide2Mdv )
        mc.connectAttr( '%s.%s3Slide' %( ctrlShape , each ) , '%s.i2x' %slide3Mdv )
        mc.connectAttr( '%s.%s1Scruch' %( ctrlShape , each ) , '%s.i2x' %scruch1Mdv )
        mc.connectAttr( '%s.%s2Scruch' %( ctrlShape , each ) , '%s.i2x' %scruch2Mdv )
        mc.connectAttr( '%s.%s3Scruch' %( ctrlShape , each ) , '%s.i2x' %scruch3Mdv )
        mc.connectAttr( '%s.%sSpread' %( ctrlShape , each ) , '%s.i2z' %spreadMdv )
        mc.connectAttr( '%s.%sBreak' %( ctrlShape , each ) , '%s.i2z' %breakMdv )
        mc.connectAttr( '%s.%sFlex' %( ctrlShape , each ) , '%s.i2x' %flexMdv )
        mc.connectAttr( '%s.%sBaseSpread' %( ctrlShape , each ) , '%s.i2z' %baseSpreadMdv )
        mc.connectAttr( '%s.%sBaseBreak' %( ctrlShape , each ) , '%s.i2z' %baseBreakMdv )
        mc.connectAttr( '%s.%sBaseFlex' %( ctrlShape , each ) , '%s.i2x' %baseFlexMdv )
        
        mc.connectAttr( '%s.o' %fist1Mdv , '%s%s1%sDrv_%s_pma.i3[1]' %( each , elem , type , side ))
        mc.connectAttr( '%s.o' %fist2Mdv , '%s%s2%sDrv_%s_pma.i3[5]' %( each , elem , type , side ))
        mc.connectAttr( '%s.o' %fist3Mdv , '%s%s3%sDrv_%s_pma.i3[5]' %( each , elem , type , side ))
        mc.connectAttr( '%s.o' %cup1Mdv , '%s%s1%sDrv_%s_pma.i3[2]' %( each , elem , type , side ))
        mc.connectAttr( '%s.o' %cup2Mdv , '%s%s2%sDrv_%s_pma.i3[6]' %( each , elem , type , side ))
        mc.connectAttr( '%s.o' %cup3Mdv , '%s%s3%sDrv_%s_pma.i3[6]' %( each , elem , type , side ))
        mc.connectAttr( '%s.o' %slide1Mdv , '%s%s1%sDrv_%s_pma.i3[3]' %( each , elem , type , side ))
        mc.connectAttr( '%s.o' %slide2Mdv , '%s%s2%sDrv_%s_pma.i3[7]' %( each , elem , type , side ))
        mc.connectAttr( '%s.o' %slide3Mdv , '%s%s3%sDrv_%s_pma.i3[7]' %( each , elem , type , side ))
        mc.connectAttr( '%s.o' %scruch1Mdv , '%s%s1%sDrv_%s_pma.i3[4]' %( each , elem , type , side ))
        mc.connectAttr( '%s.o' %scruch2Mdv , '%s%s2%sDrv_%s_pma.i3[8]' %( each , elem , type , side ))
        mc.connectAttr( '%s.o' %scruch3Mdv , '%s%s3%sDrv_%s_pma.i3[8]' %( each , elem , type , side ))
        mc.connectAttr( '%s.o' %spreadMdv , '%s%s2%sDrv_%s_pma.i3[9]' %( each , elem , type , side ))
        mc.connectAttr( '%s.o' %breakMdv , '%s%s2%sDrv_%s_pma.i3[10]' %( each , elem , type , side ))
        mc.connectAttr( '%s.o' %flexMdv , '%s%s2%sDrv_%s_pma.i3[11]' %( each , elem , type , side ))
        mc.connectAttr( '%s.o' %baseSpreadMdv , '%s%s1%sDrv_%s_pma.i3[5]' %( each , elem , type , side ))
        mc.connectAttr( '%s.o' %baseBreakMdv , '%s%s1%sDrv_%s_pma.i3[6]' %( each , elem , type , side ))
        mc.connectAttr( '%s.o' %baseFlexMdv , '%s%s1%sDrv_%s_pma.i3[7]' %( each , elem , type , side ))
        
        if not each == 'thumb' :
            if num == 5 :
                #-- Connect node
                mc.connectAttr( '%s.fist' %ctrl , '%s.i1x' %fist4Mdv )
                mc.connectAttr( '%s.fist' %ctrl , '%s.i1y' %fist4Mdv )
                mc.connectAttr( '%s.fist' %ctrl , '%s.i1z' %fist4Mdv )
                mc.connectAttr( '%s.cup' %ctrl , '%s.i1x' %cup4Mdv )
                mc.connectAttr( '%s.slide' %ctrl , '%s.i1x' %slide4Mdv )
                mc.connectAttr( '%s.scruch' %ctrl , '%s.i1x' %scruch4Mdv )
                
                mc.connectAttr( '%s.%s4FistRx' %( ctrlShape , each ) , '%s.i2x' %fist4Mdv )
                mc.connectAttr( '%s.%s4FistRy' %( ctrlShape , each ) , '%s.i2y' %fist4Mdv )
                mc.connectAttr( '%s.%s4FistRz' %( ctrlShape , each ) , '%s.i2z' %fist4Mdv )
                mc.connectAttr( '%s.%s4CupRx' %( ctrlShape , each ) , '%s.i2x' %cup4Mdv )
                mc.connectAttr( '%s.%s4CupRy' %( ctrlShape , each ) , '%s.i2y' %cup4Mdv )
                mc.connectAttr( '%s.%s4CupRz' %( ctrlShape , each ) , '%s.i2z' %cup4Mdv )
                mc.connectAttr( '%s.%s4Slide' %( ctrlShape , each ) , '%s.i2x' %slide4Mdv )
                mc.connectAttr( '%s.%s4Scruch' %( ctrlShape , each ) , '%s.i2x' %scruch4Mdv )
                
                mc.connectAttr( '%s.o' %fist4Mdv , '%s%s4%sDrv_%s_pma.i3[5]' %( each , elem , type , side ))
                mc.connectAttr( '%s.o' %cup4Mdv , '%s%s4%sDrv_%s_pma.i3[6]' %( each , elem , type , side ))
                mc.connectAttr( '%s.o' %slide4Mdv , '%s%s4%sDrv_%s_pma.i3[7]' %( each , elem , type , side ))
                mc.connectAttr( '%s.o' %scruch4Mdv , '%s%s4%sDrv_%s_pma.i3[8]' %( each , elem , type , side ))
    
    mc.select( cl = True )