import maya.cmds as mc
import maya.mel as mm
import ppmel.core as core
reload(core)


# --------------------------------------------------------------------------------------------
#
# Duplicate Blendshape
#
# description :  - Cut head geomety [ eye , eye brow , eyelash , in mouth ] 
#                - Rename Group "headBshGeo_grp"
#                - Select Group and run script "dupBlendshape()"
#
# --------------------------------------------------------------------------------------------

def dupBlendshape() :
    
    bshBase =         [ ]
    
    bshNode =         [ ]
    
    bshInBtw =        [ ]
    
    side =            [ 'lft' ,
                        'rgt' ]
    
    eyebrow_side =    [ 'eyebrowUp' ,
                        'eyebrowDn' ,
                        'eyebrowIn' ,
                        'eyebrowOut' ,
                        'eyebrowInnerUp' ,
                        'eyebrowInnerDn' ,
                        'eyebrowInnerIn' ,
                        'eyebrowInnerOut' ,
                        'eyebrowMiddleUp' ,
                        'eyebrowMiddleDn' ,
                        'eyebrowMiddleIn' ,
                        'eyebrowMiddleOut' ,
                        'eyebrowOuterUp' ,
                        'eyebrowOuterDn' ,
                        'eyebrowOuterIn' ,
                        'eyebrowOuterOut' ,
                        'eyebrowTurnF' ,
                        'eyebrowTurnC' ]
    
    lid_side =        [ 'upLidUp' ,
                        'upLidDn' ,
                        'loLidUp' ,
                        'loLidDn' ,
                        'upLidTurnF' ,
                        'upLidTurnC' ,
                        'loLidTurnF' ,
                        'loLidTurnC' ,
                        'upLidInUD' ,
                        'upLidMidUD' ,
                        'upLidOutUD' ,
                        'loLidInUD' ,
                        'loLidMidUD' ,
                        'loLidOutUD' ,
                        'eyeBallUp' ,
                        'eyeBallDn' ,
                        'eyeBallIn' ,
                        'eyeBallOut' ,
                        'eyeBallTurnF' ,
                        'eyeBallTurnC' ]
    
    lip =             [ 'mouthUp' ,
                        'mouthDn' ,
                        'mouthL' ,
                        'mouthR' ,
                        'mouthTurnF' ,
                        'mouthTurnC' ,
                        'upLipsUp' ,
                        'upLipsDn' ,
                        'loLipsUp' ,
                        'loLipsDn' ,
                        'upLipsCurlIn' ,
                        'upLipsCurlOut' ,
                        'loLipsCurlIn' ,
                        'loLipsCurlOut' ,
                        'mouthClench' ,
                        'mouthPull' ,
                        'mouthU' ]
    
    lip_side =         [ 'upLipsUp' ,
                         'upLipsDn' ,
                         'loLipsUp' ,
                         'loLipsDn' ,
                         'cornerUp' ,
                         'cornerDn' ,
                         'cornerIn' ,
                         'cornerOut' ,
                         'lipsPartIn' ,
                         'lipsPartOut' ,
                         'cheekUpper' ,
                         'cheekLower' ,
                         'cornerPuffIn' ,
                         'cornerPuffOut' ]
    
    nose =             [ 'noseUp' ,
                         'noseDn' ,
                         'noseL' ,
                         'noseR' ,
                         'noseStretch' ,
                         'noseSquash' ]
    
    nose_side =        [ 'noseUp' ,
                         'noseDn' ,
                         'noseTurnF' ,
                         'noseTurnC' ]
    
    face =             [ 'faceUprStretch' ,
                         'faceUprSquash' ,
                         'faceUprLft' ,
                         'faceUprRgt' ,
                         'faceUprFront' ,
                         'faceUprBack' ,
                         'faceLwrStretch' ,
                         'faceLwrSquash' ,
                         'faceLwrLft' ,
                         'faceLwrRgt' ,
                         'faceLwrFront' ,
                         'faceLwrBack' ,
                         'faceEyebrowPull' ]
    
    inBetween_side =   [ 'upLidDnInBtw' ,
                         'loLidUpInBtw' ]

    sel = mc.ls( sl = True )[0]
    bshBase.append( sel )
    
    shapeOrig = mc.ls( '*ShapeOrig*' )
    
    if shapeOrig :
        mc.delete(shapeOrig)
    
    bb = mc.exactWorldBoundingBox()
    yOffset = float( abs( bb[1] - bb[4] ) * 1.1 )
    xOffset = float( abs( bb[0] - bb[3] ) * 1.2 )
    xVal = 0
    
    for sides in side :
        xVal += xOffset
        yVal = 0
        
        for eyebrows in eyebrow_side :
            if not mc.objExists( 'eyebrowBsh_%s_grp' %sides ) :
                bshGrp = mc.createNode( 'transform' , n = 'eyebrowBsh_%s_grp' %sides )
            
            mc.select( sel , r = True )
            duppedNode = mc.duplicate( sel , rr = True )[0]
            bshNodes = mc.rename( duppedNode , '%s_%s' %(eyebrows , sides) )
            mc.parent( bshNodes , bshGrp )
            mc.move( 0 , yVal , 0 , bshNodes , r = True )
            
            bshNode.append( bshNodes )
            
            yVal += yOffset
        
        mc.move( xVal , 0 , 0 , bshGrp , r=True )
    
    for sides in side :
        xVal += xOffset
        yVal = 0
        
        for lids in lid_side :
            if not mc.objExists( 'lidBsh_%s_grp' %sides ) :
                bshGrp = mc.createNode( 'transform' , n = 'lidBsh_%s_grp' %sides )
            
            mc.select( sel , r = True )
            duppedNode = mc.duplicate( sel , rr = True )[0]
            bshNodes = mc.rename( duppedNode , '%s_%s' %(lids , sides) )
            mc.parent( bshNodes , bshGrp )
            mc.move( 0 , yVal , 0 , bshNodes , r = True )
            
            bshNode.append( bshNodes )
            
            yVal += yOffset
        
        mc.move( xVal , 0 , 0 , bshGrp , r=True )
    
    yVal = 0
    
    for lips in lip :
        if not mc.objExists( 'lipBsh_grp' ) :
            bshGrp = mc.createNode( 'transform' , n = 'lipBsh_grp' )
        
        mc.select( sel , r = True )
        duppedNode = mc.duplicate( sel , rr = True )[0]
        bshNodes = mc.rename( duppedNode , '%s' %lips )
        mc.parent( bshNodes , bshGrp )
        mc.move( 0 , yVal , 0 , bshNodes , r = True )
        
        bshNode.append( bshNodes )
            
        yVal += yOffset
    
    xVal += xOffset
    mc.move( xVal , 0 , 0 , bshGrp , r=True )
    
    for sides in side :
        xVal += xOffset
        yVal = 0
        
        for lips in lip_side :
            if not mc.objExists( 'lipBsh_%s_grp' %sides ) :
                bshGrp = mc.createNode( 'transform' , n = 'lipBsh_%s_grp' %sides )
            
            mc.select( sel , r = True )
            duppedNode = mc.duplicate( sel , rr = True )[0]
            bshNodes = mc.rename( duppedNode , '%s_%s' %(lips , sides) )
            mc.parent( bshNodes , bshGrp )
            mc.move( 0 , yVal , 0 , bshNodes , r = True )
            
            bshNode.append( bshNodes )
            
            yVal += yOffset
        
        mc.move( xVal , 0 , 0 , bshGrp , r=True )
    
    yVal = 0
    
    for noses in nose :
        if not mc.objExists( 'noseBsh_grp' ) :
            bshGrp = mc.createNode( 'transform' , n = 'noseBsh_grp' )
        
        mc.select( sel , r = True )
        duppedNode = mc.duplicate( sel , rr = True )[0]
        bshNodes = mc.rename( duppedNode , '%s' %noses )
        mc.parent( bshNodes , bshGrp )
        mc.move( 0 , yVal , 0 , bshNodes , r = True )
        
        bshNode.append( bshNodes )
        
        yVal += yOffset
    
    xVal += xOffset
    mc.move( xVal , 0 , 0 , bshGrp , r=True )
    
    for sides in side :
        xVal += xOffset
        yVal = 0
        
        for noses in nose_side :
            if not mc.objExists( 'noseBsh_%s_grp' %sides ) :
                bshGrp = mc.createNode( 'transform' , n = 'noseBsh_%s_grp' %sides )
            
            mc.select( sel , r = True )
            duppedNode = mc.duplicate( sel , rr = True )[0]
            bshNodes = mc.rename( duppedNode , '%s_%s' %(noses , sides) )
            mc.parent( bshNodes , bshGrp )
            mc.move( 0 , yVal , 0 , bshNodes , r = True )
            
            bshNode.append( bshNodes )
            
            yVal += yOffset
        
        mc.move( xVal , 0 , 0 , bshGrp , r=True )
    
    yVal = 0
    
    for faces in face :
        if not mc.objExists( 'faceBsh_grp' ) :
            bshGrp = mc.createNode( 'transform' , n = 'faceBsh_grp' )
        
        mc.select( sel , r = True )
        duppedNode = mc.duplicate( sel , rr = True )[0]
        bshNodes = mc.rename( duppedNode , '%s' %faces )
        mc.parent( bshNodes , bshGrp )
        mc.move( 0 , yVal , 0 , bshNodes , r = True )
        
        bshNode.append( bshNodes )
        
        yVal += yOffset
    
    xVal += xOffset
    mc.move( xVal , 0 , 0 , bshGrp , r=True )
    
    yVal = 0
    
    for sides in side :
        xVal += xOffset
        yVal = 0
        
        for inBetweens in inBetween_side :
            if not mc.objExists( 'inBetweenBsh_%s_grp' %sides ) :
                bshGrp = mc.createNode( 'transform' , n = 'inBetweenBsh_%s_grp' %sides )
            
            mc.select( sel , r = True )
            duppedNode = mc.duplicate( sel , rr = True )[0]
            bshNodes = mc.rename( duppedNode , '%s_%s' %(inBetweens , sides) )
            mc.parent( bshNodes , bshGrp )
            mc.move( 0 , yVal , 0 , bshNodes , r = True )
            
            bshInBtw.append( bshNodes )
            
            yVal += yOffset
        
        mc.move( xVal , 0 , 0 , bshGrp , r=True )
    
    yVal = 0
    
    
    #--- Create blendshape
    bsnName = mc.blendShape( bshNode , bshBase , n = 'facialAll_bsh' )
    
    indexUpLidDnLFT = bshNode.index( 'upLidDn_lft' )
    indexLoLidUpLFT = bshNode.index( 'loLidUp_lft' )
    indexUpLidDnRGT = bshNode.index( 'upLidDn_rgt' )
    indexLoLidUpRGT = bshNode.index( 'loLidUp_rgt' )
    
    mc.blendShape( bsnName , edit = True , ib = True , t = ( bshBase[0] , indexUpLidDnLFT , 'upLidDnInBtw_lft' , 0.5 ))
    mc.blendShape( bsnName , edit = True , ib = True , t = ( bshBase[0] , indexLoLidUpLFT , 'loLidUpInBtw_lft' , 0.5 ))
    mc.blendShape( bsnName , edit = True , ib = True , t = ( bshBase[0] , indexUpLidDnRGT , 'upLidDnInBtw_rgt' , 0.5 ))
    mc.blendShape( bsnName , edit = True , ib = True , t = ( bshBase[0] , indexLoLidUpRGT , 'loLidUpInBtw_rgt' , 0.5 ))
    
    mc.select( cl = True )


# --------------------------------------------------------------------------------------------
#
# Add Attribute
#
# --------------------------------------------------------------------------------------------

def addAttrBshCtrl() :
    attrBrow( 'eyeBrowBsh_lft_ctrl' )
    attrBrow( 'eyeBrowBsh_rgt_ctrl' )
    attrEye( 'eyeBsh_lft_ctrl' )
    attrEye( 'eyeBsh_rgt_ctrl' )
    attrNose( 'noseBsh_ctrl' )
    attrMouth( 'mouthBsh_ctrl' )
    attrMouthSide( 'mouthBsh_lft_ctrl' )
    attrMouthSide( 'mouthBsh_rgt_ctrl' )
    
def attrBrow( obj ):
    #obj = mc.ls( sl = True )[0]
    
    listAttr = [ 'eyebrow' , 
                 'allUD' , 
                 'allIO' , 
                 'innerUD' , 
                 'innerIO' , 
                 'middleUD' , 
                 'middleIO' , 
                 'outerUD' ,
                 'outerIO' ,
                 'turnUD' ]
    
    for attr in listAttr :
        mc.addAttr( obj , ln = '%s' %attr , at = 'double' , k = True , min = -10 , max = 10 )
        
    core.setLock( obj , True , 'eyebrow' )
    
def attrEye( obj ):
    #obj = mc.ls( sl = True )[0]
    
    listAttr = [ 'lid' ,
                 'upLidUD' , 
                 'loLidUD' , 
                 'upLidTW' , 
                 'loLidTW' , 
                 'upInnerUD' , 
                 'upMidUD' , 
                 'upOuterUD' ,
                 'loInnerUD' ,
                 'loMidUD' ,
                 'loOuterUD' ,
                 'ball' ,
                 'eyeBallUD' ,
                 'eyeBallIO' ,
                 'eyeBallTW' ,
                 'iris' ,
                 'scaleIris' ]
    
    for attr in listAttr :
        mc.addAttr( obj , ln = '%s' %attr , at = 'double' , k = True , min = -10 , max = 10 )
    
    core.setLock( obj , True , 'lid' , 'ball' , 'iris' )
    
def attrNose( obj ):
    #obj = mc.ls( sl = True )[0]
    
    listAttr = [ 'nose' ,
                 'noseUD' ,
                 'noseLR' ,
                 'noseSTQ' , 
                 'noseLft' , 
                 'noseLftUD' , 
                 'noseLftTurn' , 
                 'noseRgt' , 
                 'noseRgtUD' , 
                 'noseRgtTurn' ]
    
    for attr in listAttr :
        mc.addAttr( obj , ln = '%s' %attr , at = 'double' , k = True , min = -10 , max = 10 )
    
    core.setLock( obj , True , 'nose' , 'noseLft' , 'noseRgt' )
    
def attrMouth( obj ):
    #obj = mc.ls( sl = True )[0]
    
    listAttr = [ 'mouth' ,
                 'mouthUD' ,
                 'mouthLR' ,
                 'mouthTurn' , 
                 'upLipsUD' , 
                 'loLipsUD' , 
                 'upLipsCurlIO' , 
                 'loLipsCurlIO' , 
                 'mouthClench' , 
                 'mouthPull' ,
                 'mouthU' ,
                 'face' ,
                 'faceSquash' ,
                 'eyebrowPullIO' ]
    
    for attr in listAttr :
        mc.addAttr( obj , ln = '%s' %attr , at = 'double' , k = True , min = -10 , max = 10 )
    
    core.setLock( obj , True , 'mouth' , 'face' )
    
def attrMouthSide( obj ):
    #obj = mc.ls( sl = True )[0]
    
    listAttr = [ 'lip' ,
                 'upLipUD' ,
                 'loLipUD' ,
                 'cornerUD' , 
                 'cornerIO' , 
                 'lipPartIO' , 
                 'cheekUprIO' , 
                 'cheekLwrIO' , 
                 'puffIO' ]
    
    for attr in listAttr :
        mc.addAttr( obj , ln = '%s' %attr , at = 'double' , k = True , min = -10 , max = 10 )
    
    core.setLock( obj , True , 'lip' )



# --------------------------------------------------------------------------------------------
#
# Connect Blendshape to Control
#
# --------------------------------------------------------------------------------------------

def connectBshCtrl( eyeBrowLFT = 'eyeBrowBsh_lft_ctrl' ,
                    eyeBrowRGT = 'eyeBrowBsh_rgt_ctrl' ,
                    eyeLFT = 'eyeBsh_lft_ctrl' ,
                    eyeRGT = 'eyeBsh_rgt_ctrl' ,
                    nose = 'noseBsh_ctrl' ,
                    mouth = 'mouthBsh_ctrl' ,
                    mouthLFT = 'mouthBsh_lft_ctrl' ,
                    mouthRGT = 'mouthBsh_rgt_ctrl' ,
                    faceUpr = 'faceUprBsh_ctrl' ,
                    faceLwr = 'faceLwrBsh_ctrl' ,
                    bshName = 'facialAll_bsh' ) :
    
    
    #-- eyeBrow
    for side in ( 'lft' , 'rgt' ) :
        
        if side == 'lft' :
            ctrl = eyeBrowLFT
        else :
            ctrl = eyeBrowRGT
            
        attrDict = { 'allUD' : ( 'eyebrowUp' , 'eyebrowDn' ) , 
                     'allIO' : ( 'eyebrowIn' , 'eyebrowOut' ) , 
                     'innerUD' : ( 'eyebrowInnerUp' , 'eyebrowInnerDn' ) , 
                     'innerIO' : ( 'eyebrowInnerIn' , 'eyebrowInnerOut' ) , 
                     'middleUD' : ( 'eyebrowMiddleUp' , 'eyebrowMiddleDn' ) , 
                     'middleIO' : ( 'eyebrowMiddleIn' , 'eyebrowMiddleOut' ) , 
                     'outerUD' : ( 'eyebrowOuterUp' , 'eyebrowOuterDn' ) , 
                     'outerIO' : ( 'eyebrowOuterIn' , 'eyebrowOuterOut' ) , 
                     'turnUD' : ( 'eyebrowTurnF' , 'eyebrowTurnC' ) }
        
        
        for attr in attrDict :
            mdv = core.multiplyDivide( '%s_%s_mdv' %(attr , side))
            core.setVal( '%s.i2x' %mdv , 0.1 )
            core.setVal( '%s.i2y' %mdv , -0.1 )
            
            cmp = core.clamp( '%s_%s_cmp' %(attr , side))
            core.setVal( '%s.mng' %cmp , -10 )
            core.setVal( '%s.mxr' %cmp , 10 )
            
            mc.connectAttr( '%s.%s' %(ctrl , attr) , '%s.ipr' %cmp )
            mc.connectAttr( '%s.%s' %(ctrl , attr) , '%s.ipg' %cmp )
            
            mc.connectAttr( '%s.opr' %cmp , '%s.i1x' %mdv )
            mc.connectAttr( '%s.opg' %cmp , '%s.i1y' %mdv )
            
            mc.connectAttr( '%s.ox' %mdv , '%s.%s_%s' %(bshName , attrDict[attr][0] , side))
            mc.connectAttr( '%s.oy' %mdv , '%s.%s_%s' %(bshName , attrDict[attr][1] , side))
            
            
    #-- eye
    for side in ( 'lft' , 'rgt' ) :
        
        if side == 'lft' :
            ctrl = eyeLFT
        else :
            ctrl = eyeRGT
        
        attrDict = { 'upLidUD' : ( 'upLidUp' , 'upLidDn' ) , 
                     'loLidUD' : ( 'loLidUp' , 'loLidDn' ) , 
                     'upLidTW' : ( 'upLidTurnF' , 'upLidTurnC' ) , 
                     'loLidTW' : ( 'loLidTurnF' , 'loLidTurnC' ) , 
                     'upInnerUD' : 'upLidInUD'  , 
                     'upMidUD' : 'upLidMidUD' , 
                     'upOuterUD' : 'upLidOutUD' , 
                     'loInnerUD' : 'loLidInUD' , 
                     'loMidUD' : 'loLidMidUD' , 
                     'loOuterUD' : 'loLidOutUD' , 
                     'eyeBallUD' : ( 'eyeBallUp' , 'eyeBallDn' ) , 
                     'eyeBallIO' : ( 'eyeBallIn' , 'eyeBallOut' ) , 
                     'eyeBallTW' : ( 'eyeBallTurnF' , 'eyeBallTurnC' ) }
        
        for attr in attrDict :
                
            mdv = core.multiplyDivide( '%s_%s_mdv' %(attr , side))
            if not attr == 'loLidUD' :
                core.setVal( '%s.i2x' %mdv , 0.1 )
                core.setVal( '%s.i2y' %mdv , -0.1 )
            else :
                core.setVal( '%s.i2x' %mdv , -0.1 )
                core.setVal( '%s.i2y' %mdv , 0.1 )
            
            if not attr in ( 'upInnerUD' , 'upMidUD' , 'upOuterUD' , 'loInnerUD' , 'loMidUD' , 'loOuterUD' ) :
                cmp = core.clamp( '%s_%s_cmp' %(attr , side))
                
                if not attr == 'loLidUD' :
                    core.setVal( '%s.mng' %cmp , -10 )
                    core.setVal( '%s.mxr' %cmp , 10 )
                else :
                    core.setVal( '%s.mnr' %cmp , -10 )
                    core.setVal( '%s.mxg' %cmp , 10 )
                
                mc.connectAttr( '%s.%s' %(ctrl , attr) , '%s.ipr' %cmp )
                mc.connectAttr( '%s.%s' %(ctrl , attr) , '%s.ipg' %cmp )
                
                mc.connectAttr( '%s.opr' %cmp , '%s.i1x' %mdv )
                mc.connectAttr( '%s.opg' %cmp , '%s.i1y' %mdv )
                
                mc.connectAttr( '%s.ox' %mdv , '%s.%s_%s' %(bshName , attrDict[attr][0] , side))
                mc.connectAttr( '%s.oy' %mdv , '%s.%s_%s' %(bshName , attrDict[attr][1] , side))
            
            else :
                mc.connectAttr( '%s.%s' %(ctrl , attr) , '%s.i1x' %mdv )
                mc.connectAttr( '%s.ox' %mdv , '%s.%s_%s' %(bshName , attrDict[attr] , side))
    
    
    #-- nose
    attrDict = { 'noseUD' : ( 'noseUp' , 'noseDn' ) , 
                 'noseLR' : ( 'noseL' , 'noseR' ) , 
                 'noseSTQ' : ( 'noseStretch' , 'noseSquash' ) , 
                 'noseLftUD' : ( 'noseUp_lft' , 'noseDn_lft' ) , 
                 'noseLftTurn' : ( 'noseTurnF_lft' , 'noseTurnC_lft' ) , 
                 'noseRgtUD' : ( 'noseUp_rgt' , 'noseDn_rgt' ) , 
                 'noseRgtTurn' : ( 'noseTurnF_rgt' , 'noseTurnC_rgt' ) }
        
    for attr in attrDict :
            mdv = core.multiplyDivide( '%s_mdv' %attr )
            core.setVal( '%s.i2x' %mdv , 0.1 )
            core.setVal( '%s.i2y' %mdv , -0.1 )
            
            cmp = core.clamp( '%s_cmp' %attr )
            core.setVal( '%s.mng' %cmp , -10 )
            core.setVal( '%s.mxr' %cmp , 10 )
            
            mc.connectAttr( '%s.%s' %(nose , attr) , '%s.ipr' %cmp )
            mc.connectAttr( '%s.%s' %(nose , attr) , '%s.ipg' %cmp )
            
            mc.connectAttr( '%s.opr' %cmp , '%s.i1x' %mdv )
            mc.connectAttr( '%s.opg' %cmp , '%s.i1y' %mdv )
            
            mc.connectAttr( '%s.ox' %mdv , '%s.%s' %(bshName , attrDict[attr][0] ))
            mc.connectAttr( '%s.oy' %mdv , '%s.%s' %(bshName , attrDict[attr][1] ))
    
    
    #-- mouth
    attrDict = { 'mouthUD' : ( 'mouthUp' , 'mouthDn' ) ,
                 'mouthLR' : ( 'mouthL' , 'mouthR' ) ,
                 'mouthTurn' : ( 'mouthTurnF' , 'mouthTurnC' ) ,
                 'upLipsUD' : ( 'upLipsUp' , 'upLipsDn' ) ,
                 'loLipsUD' : ( 'loLipsUp' , 'loLipsDn' ) ,
                 'upLipsCurlIO' : ( 'upLipsCurlIn' , 'upLipsCurlOut' ) ,
                 'loLipsCurlIO' : ( 'loLipsCurlIn' , 'loLipsCurlOut' ) ,
                 'mouthClench' : 'mouthClench' ,
                 'mouthPull' : 'mouthPull' ,
                 'mouthU' : 'mouthU' ,
                 'eyebrowPullIO' : 'faceEyebrowPull' }
        
    for attr in attrDict :
                
            mdv = core.multiplyDivide( '%s_mdv' %attr )
            core.setVal( '%s.i2x' %mdv , 0.1 )
            core.setVal( '%s.i2y' %mdv , -0.1 )
            
            if not attr in ( 'mouthClench' , 'mouthPull' , 'mouthU' , 'eyebrowPullIO' ) :
                cmp = core.clamp( '%s_cmp' %attr )
                core.setVal( '%s.mng' %cmp , -10 )
                core.setVal( '%s.mxr' %cmp , 10 )
                
                mc.connectAttr( '%s.%s' %(mouth , attr) , '%s.ipr' %cmp )
                mc.connectAttr( '%s.%s' %(mouth , attr) , '%s.ipg' %cmp )
                
                mc.connectAttr( '%s.opr' %cmp , '%s.i1x' %mdv )
                mc.connectAttr( '%s.opg' %cmp , '%s.i1y' %mdv )
                
                mc.connectAttr( '%s.ox' %mdv , '%s.%s' %(bshName , attrDict[attr][0] ))
                mc.connectAttr( '%s.oy' %mdv , '%s.%s' %(bshName , attrDict[attr][1] ))
            
            else :
                mc.connectAttr( '%s.%s' %(mouth , attr) , '%s.i1x' %mdv )
                mc.connectAttr( '%s.ox' %mdv , '%s.%s' %(bshName , attrDict[attr] ))
    
    
    #-- mouthSide
    for side in ( 'lft' , 'rgt' ) :
        
        if side == 'lft' :
            ctrl = mouthLFT
        else :
            ctrl = mouthRGT
            
        attrDict = { 'upLipUD' : ( 'upLipsUp' , 'upLipsDn' ) ,
                     'loLipUD' : ( 'loLipsUp' , 'loLipsDn' ) ,
                     'cornerUD' : ( 'cornerUp' , 'cornerDn' ) ,
                     'cornerIO' : ( 'cornerIn' , 'cornerOut' ) ,
                     'lipPartIO' : ( 'lipsPartIn' , 'lipsPartOut' ) ,
                     'cheekUprIO' : 'cheekUpper' ,
                     'cheekLwrIO' : 'cheekLower' ,
                     'puffIO' : ( 'cornerPuffIn' , 'cornerPuffOut' ) }
        
        for attr in attrDict :
                
            mdv = core.multiplyDivide( '%s_%s_mdv' %(attr , side))
            core.setVal( '%s.i2x' %mdv , 0.1 )
            core.setVal( '%s.i2y' %mdv , -0.1 )
            
            if not attr in ( 'cheekUprIO' , 'cheekLwrIO' ) :
                cmp = core.clamp( '%s_%s_cmp' %(attr , side))
                core.setVal( '%s.mng' %cmp , -10 )
                core.setVal( '%s.mxr' %cmp , 10 )
                
                mc.connectAttr( '%s.%s' %(ctrl , attr) , '%s.ipr' %cmp )
                mc.connectAttr( '%s.%s' %(ctrl , attr) , '%s.ipg' %cmp )
                
                mc.connectAttr( '%s.opr' %cmp , '%s.i1x' %mdv )
                mc.connectAttr( '%s.opg' %cmp , '%s.i1y' %mdv )
                
                mc.connectAttr( '%s.ox' %mdv , '%s.%s_%s' %(bshName , attrDict[attr][0] , side))
                mc.connectAttr( '%s.oy' %mdv , '%s.%s_%s' %(bshName , attrDict[attr][1] , side))
            
            else :
                mc.connectAttr( '%s.%s' %(ctrl , attr) , '%s.i1x' %mdv )
                mc.connectAttr( '%s.ox' %mdv , '%s.%s_%s' %(bshName , attrDict[attr] , side))
    
    
    #-- faceUpr
    attrDict = { 'tx' : ( 'faceUprLft' , 'faceUprRgt' ) , 
                 'ty' : ( 'faceUprStretch' , 'faceUprSquash' ) , 
                 'tz' : ( 'faceUprFront' , 'faceUprBack' ) }
        
    for attr in attrDict :
            mdv = core.multiplyDivide( 'faceUpr%s_mdv' %attr )
            core.setVal( '%s.i2x' %mdv , 1 )
            core.setVal( '%s.i2y' %mdv , -1 )
            
            cmp = core.clamp( 'faceUpr%s_cmp' %attr )
            core.setVal( '%s.mng' %cmp , -100 )
            core.setVal( '%s.mxr' %cmp , 100 )
            
            mc.connectAttr( '%s.%s' %(faceUpr , attr) , '%s.ipr' %cmp )
            mc.connectAttr( '%s.%s' %(faceUpr , attr) , '%s.ipg' %cmp )
            
            mc.connectAttr( '%s.opr' %cmp , '%s.i1x' %mdv )
            mc.connectAttr( '%s.opg' %cmp , '%s.i1y' %mdv )
            
            mc.connectAttr( '%s.ox' %mdv , '%s.%s' %(bshName , attrDict[attr][0] ))
            mc.connectAttr( '%s.oy' %mdv , '%s.%s' %(bshName , attrDict[attr][1] ))
    
    
    #-- faceLwr
    attrDict = { 'tx' : ( 'faceLwrLft' , 'faceLwrRgt' ) , 
                 'ty' : ( 'faceLwrStretch' , 'faceLwrSquash' ) , 
                 'tz' : ( 'faceLwrFront' , 'faceLwrBack' ) }
        
    for attr in attrDict :
            mdv = core.multiplyDivide( 'faceLwr%s_mdv' %attr )
            
            if not attr == 'ty' :
                core.setVal( '%s.i2x' %mdv , 1 )
                core.setVal( '%s.i2y' %mdv , -1 )
            else :
                core.setVal( '%s.i2x' %mdv , -1 )
                core.setVal( '%s.i2y' %mdv , 1 )
            
            cmp = core.clamp( 'faceLwr%s_cmp' %attr )
            
            if not attr == 'ty' :
                core.setVal( '%s.mng' %cmp , -100 )
                core.setVal( '%s.mxr' %cmp , 100 )
            else :
                core.setVal( '%s.mnr' %cmp , -100 )
                core.setVal( '%s.mxg' %cmp , 100 )
            
            mc.connectAttr( '%s.%s' %(faceLwr , attr) , '%s.ipr' %cmp )
            mc.connectAttr( '%s.%s' %(faceLwr , attr) , '%s.ipg' %cmp )
            
            mc.connectAttr( '%s.opr' %cmp , '%s.i1x' %mdv )
            mc.connectAttr( '%s.opg' %cmp , '%s.i1y' %mdv )
            
            mc.connectAttr( '%s.ox' %mdv , '%s.%s' %(bshName , attrDict[attr][0] ))
            mc.connectAttr( '%s.oy' %mdv , '%s.%s' %(bshName , attrDict[attr][1] ))
    
    mc.select( cl = True )


def connectBshCtrl( eyeBrowLFT = 'Eyebrow_L_Ctrl' ,
                    eyeBrowInLFT = 'EyebrowIn_L_Ctrl' ,
                    eyeBrowMidLFT = 'EyebrowMid_L_Ctrl' ,
                    eyeBrowOutLFT = 'EyebrowOut_L_Ctrl' ,
                    eyeBrowRGT = 'Eyebrow_R_Ctrl' ,
                    eyeBrowInRGT = 'EyebrowIn_R_Ctrl' ,
                    eyeBrowMidRGT = 'EyebrowMid_R_Ctrl' ,
                    eyeBrowOutRGT = 'EyebrowOut_R_Ctrl' ,
                    eyeLidsLFT = 'EyeLids_L_Ctrl' ,
                    eyeLidsUprLFT = 'EyeLidsUpr_L_Ctrl' ,
                    eyeLidsLwrLFT = 'EyeLidsLwr_L_Ctrl' ,
                    eyeLidsRGT = 'EyeLids_R_Ctrl' ,
                    eyeLidsUprRGT = 'EyeLidsUpr_R_Ctrl' ,
                    eyeLidsLwrRGT = 'EyeLidsLwr_R_Ctrl' ,
                    nose = 'Nose_Ctrl' ,
                    mouth = 'Mouth_Ctrl' ,
                    mouthCnrLFT = 'MouthCnr_L_Ctrl' ,
                    mouthCnrRGT = 'MouthCnr_R_Ctrl' ,
                    mouthUprLFT = 'MouthUpr_Ctrl' ,
                    mouthLwrLFT = 'MouthLwr_Ctrl' ,
                    faceUpr = 'FaceUprBsh_Ctrl' ,
                    faceLwr = 'FaceLwrBsh_Ctrl' ,
                    bshName = 'facialAll_bsh' ) :

    #-- eyeBrow
    for side in ( '_L' , '_R' ) :

        mc.connectAttr('EyeBrowUD%s_Mdv.ox' %side , '%s.eyebrowUp_%s' %(bshName , side))
        mc.connectAttr('EyeBrowUD%s_Mdv.oy' %side , '%s.eyebrowDn_%s' %(bshName , side))
        mc.connectAttr('EyeBrowLR%s_Mdv.ox' %side , '%s.eyebrowIn_%s' %(bshName , side))
        mc.connectAttr('EyeBrowLR%s_Mdv.oy' %side , '%s.eyebrowOut_%s' %(bshName , side))
        mc.connectAttr('EyeBrowFC%s_Mdv.ox' %side , '%s.eyebrowTurnF_%s' %(bshName , side))
        mc.connectAttr('EyeBrowFC%s_Mdv.oy' %side , '%s.eyebrowTurnC_%s' %(bshName , side))

        mc.connectAttr('EyeBrowInUD%s_Mdv.ox' %side , '%s.eyebrowInnerUp_%s' %(bshName , side))
        mc.connectAttr('EyeBrowInUD%s_Mdv.oy' %side , '%s.eyebrowInnerDn_%s' %(bshName , side))
        mc.connectAttr('EyeBrowInIO%s_Mdv.ox' %side , '%s.eyebrowInnerIn_%s' %(bshName , side))
        mc.connectAttr('EyeBrowInIO%s_Mdv.oy' %side , '%s.eyebrowInnerOut_%s' %(bshName , side))

        mc.connectAttr('EyeBrowMidUD%s_Mdv.ox' %side , '%s.eyebrowMidUp_%s' %(bshName , side))
        mc.connectAttr('EyeBrowMidUD%s_Mdv.oy' %side , '%s.eyebrowMidDn_%s' %(bshName , side))
        mc.connectAttr('EyeBrowMidIO%s_Mdv.ox' %side , '%s.eyebrowMidIn_%s' %(bshName , side))
        mc.connectAttr('EyeBrowMidIO%s_Mdv.oy' %side , '%s.eyebrowMidOut_%s' %(bshName , side))

        mc.connectAttr('EyeBrowOutUD%s_Mdv.ox' %side , '%s.eyebrowOuterUp_%s' %(bshName , side))
        mc.connectAttr('EyeBrowOutUD%s_Mdv.oy' %side , '%s.eyebrowOuterDn_%s' %(bshName , side))
        mc.connectAttr('EyeBrowOutIO%s_Mdv.ox' %side , '%s.eyebrowOuterIn_%s' %(bshName , side))
        mc.connectAttr('EyeBrowOutIO%s_Mdv.oy' %side , '%s.eyebrowOuterOut_%s' %(bshName , side))

    #-- eye
    for side in ( '_L' , '_R' ) :

        mc.connectAttr('EyeBallUD%s_Mdv.ox' %side , '%s.eyeBallUp_%s' %(bshName , side))
        mc.connectAttr('EyeBallUD%s_Mdv.oy' %side , '%s.eyeBallDn_%s' %(bshName , side))
        mc.connectAttr('EyeBallAmpIO%s_Cmp.opr' %side , '%s.eyebrowIn_%s' %(bshName , side))
        mc.connectAttr('EyeBallAmpIO%s_Cmp.opg' %side , '%s.eyebrowOut_%s' %(bshName , side))
        mc.connectAttr('EyeBallFC%s_Mdv.ox' %side , '%s.eyeBallTurnF_%s' %(bshName , side))
        mc.connectAttr('EyeBallFC%s_Mdv.oy' %side , '%s.eyeBallTurnC_%s' %(bshName , side))

        mc.connectAttr('EyeLidsUprAmpUD%s_Cmp.opr' %side , '%s.upLidUp_%s' %(bshName , side))
        mc.connectAttr('EyeLidsUprAmpUD%s_Cmp.opg' %side , '%s.upLidDn_%s' %(bshName , side))
        mc.connectAttr('EyeLidsUprInUD%s_Mdv.ox' %side , '%s.upLidInUD_%s' %(bshName , side))
        mc.connectAttr('EyeLidsUprMidUD%s_Mdv.ox' %side , '%s.upLidMidUD_%s' %(bshName , side))
        mc.connectAttr('EyeLidsUprOutUD%s_Mdv.ox' %side , '%s.upLidOutUD_%s' %(bshName , side))
        mc.connectAttr('UpInnerUD%s_Mdv.oy' %side , '%s.upLidInU_%s' %(bshName , side))
        mc.connectAttr('UpMidUD%s_Mdv.oy' %side , '%s.upLidMidU_%s' %(bshName , side))
        mc.connectAttr('UpOuterUD%s_Mdv.oy' %side , '%s.upLidOutU_%s' %(bshName , side))
        mc.connectAttr('EyeLidsUprTurnFC%s_Mdv.ox' %side , '%s.upLidTurnF_%s' %(bshName , side))
        mc.connectAttr('EyeLidsUprTurnFC%s_Mdv.oy' %side , '%s.upLidTurnC_%s' %(bshName , side))

        mc.connectAttr('EyeLidsLwrUD%s_Mdv.ox' %side , '%s.loLidUp_%s' %(bshName , side))
        mc.connectAttr('EyeLidsLwrUD%s_Mdv.oy' %side , '%s.loLidDn_%s' %(bshName , side))
        mc.connectAttr('EyeLidsLwrInUD%s_Mdv.ox' %side , '%s.loLidInUD_%s' %(bshName , side))
        mc.connectAttr('EyeLidsLwrMidUD%s_Mdv.ox' %side , '%s.loLidMidUD_%s' %(bshName , side))
        mc.connectAttr('EyeLidsLwrOutUD%s_Mdv.ox' %side , '%s.loLidOutUD_%s' %(bshName , side))

    # nose
    mc.connectAttr('NoseUD_Mdv.ox' , '%s.noseUp' %bshName )
    mc.connectAttr('NoseUD_Mdv.oy' , '%s.noseDn' %bshName )
    mc.connectAttr('NoseLR_Mdv.ox' , '%s.noseL' %bshName )
    mc.connectAttr('NoseLR_Mdv.oy' , '%s.noseR' %bshName )
    mc.connectAttr('NoseStSq_Mdv.ox' , '%s.noseStretch' %bshName )
    mc.connectAttr('NoseStSq_Mdv.oy' , '%s.noseSquash' %bshName )

    for side in ( '_L' , '_R' ) :
        mc.connectAttr('NoseUD%s_Mdv.ox' %side , '%s.noseUp_%s' %(bshName , side))
        mc.connectAttr('NoseUD%s_Mdv.oy' %side , '%s.noseDn_%s' %(bshName , side))

        mc.connectAttr('NoseFC%s_Mdv.ox' %side , '%s.noseTurnC_%s' %(bshName , side))
        mc.connectAttr('NoseFC%s_Mdv.oy' %side , '%s.noseTurnF_%s' %(bshName , side))

    # mouth
    mc.connectAttr('MouthUD_Mdv.ox' , '%s.mouthUp' %bshName )
    mc.connectAttr('MouthUD_Mdv.oy' , '%s.mouthDn' %bshName )
    mc.connectAttr('MouthLR_Mdv.ox' , '%s.mouthR' %bshName )
    mc.connectAttr('MouthLR_Mdv.oy' , '%s.mouthL' %bshName )
    mc.connectAttr('MouthFC_Mdv.ox' , '%s.mouthTurnC' %bshName )
    mc.connectAttr('MouthFC_Mdv.oy' , '%s.mouthTurnF' %bshName )
    mc.connectAttr('MouthClench_Mdv.ox' , '%s.mouthClench' %bshName )
    mc.connectAttr('MouthPull_Mdv.ox' , '%s.mouthPull' %bshName )
    mc.connectAttr('MouthU_Mdv.ox' , '%s.mouthU' %bshName )

    mc.connectAttr('LipsUprCurlIO_Mdv.ox' , '%s.upLipsCurlIn' %bshName )
    mc.connectAttr('LipsUprCurlIO_Mdv.ox' , '%s.upLipsCurlOut' %bshName )
    mc.connectAttr('LipsLftUprUp_Pma.o1' , '%s.upLipsUp_lft' %bshName )
    mc.connectAttr('LipsLftUprDn_Pma.o1' , '%s.upLipsDn_lft' %bshName )
    mc.connectAttr('LipsRgtUprUp_Pma.o1' , '%s.upLipsUp_rgt' %bshName )
    mc.connectAttr('LipsRgtUprDn_Pma.o1' , '%s.upLipsDn_rgt' %bshName )
    mc.connectAttr('LipsMidUprUp_Pma.o1' , '%s.upLipsUp' %bshName )
    mc.connectAttr('LipsMidUprDn_Pma.o1' , '%s.upLipsDn' %bshName )

    mc.connectAttr('LipsLwrCurlIO_Mdv.ox' , '%s.loLipsCurlIn' %bshName )
    mc.connectAttr('LipsLwrCurlIO_Mdv.ox' , '%s.loLipsCurlOut' %bshName )
    mc.connectAttr('LipsLftLwrUp_Pma.o1' , '%s.loLipsUp_lft' %bshName )
    mc.connectAttr('LipsLftLwrDn_Pma.o1' , '%s.loLipsDn_lft' %bshName )
    mc.connectAttr('LipsRgtLwrUp_Pma.o1' , '%s.loLipsUp_rgt' %bshName )
    mc.connectAttr('LipsRgtLwrDn_Pma.o1' , '%s.loLipsDn_rgt' %bshName )
    mc.connectAttr('LipsMidLwrUp_Pma.o1' , '%s.loLipsUp' %bshName )
    mc.connectAttr('LipsMidLwrDn_Pma.o1' , '%s.loLipsDn' %bshName )


    for side in ( '_L' , '_R' ) :
        mc.connectAttr('CheekUpr%s_Pma.o1' %side , '%s.cheekUpper_%s' %(bshName , side))
        mc.connectAttr('CheekLwr%s_Pma.o1' %side , '%s.cheeklower_%s' %(bshName , side))
        mc.connectAttr('LipsCnrUD%s_Mdv.ox' %side , '%s.connerUp%s' %(bshName , side))
        mc.connectAttr('LipsCnrUD%s_Mdv.oy' %side , '%s.connerDn%s' %(bshName , side))
        mc.connectAttr('LipsCnrIO%s_Mdv.ox' %side , '%s.connerIn%s' %(bshName , side))
        mc.connectAttr('LipsCnrIO%s_Mdv.oy' %side , '%s.connerOut%s' %(bshName , side))
        mc.connectAttr('LipsPartIO%s_Mdv.ox' %side , '%s.lipsPartIn%s' %(bshName , side))
        mc.connectAttr('LipsPartIO%s_Mdv.oy' %side , '%s.lipsPartOut%s' %(bshName , side))
        mc.connectAttr('PuffAttrIO%s_Mdv.ox' %side , '%s.connerPuffIn%s' %(bshName , side))
        mc.connectAttr('PuffAttrIO%s_Mdv.oy' %side , '%s.connerPuffOut%s' %(bshName , side))
       
    # face upr
    mc.connectAttr('FaceUprTx_Mdv.ox' , '%s.faceUprLft' %bshName )
    mc.connectAttr('FaceUprTx_Mdv.oy' , '%s.faceUprRgt' %bshName )
    mc.connectAttr('FaceUprTy_Mdv.ox' , '%s.faceUprStretch' %bshName )
    mc.connectAttr('FaceUprTy_Mdv.oy' , '%s.faceUprSquash' %bshName )
    mc.connectAttr('FaceUprTz_Mdv.ox' , '%s.faceUprFront' %bshName )
    mc.connectAttr('FaceUprTz_Mdv.oy' , '%s.faceUprBack' %bshName )
       
    # face lwr
    mc.connectAttr('FaceLwrTx_Mdv.ox' , '%s.faceLwrLft' %bshName )
    mc.connectAttr('FaceLwrTx_Mdv.oy' , '%s.faceLwrRgt' %bshName )
    mc.connectAttr('FaceLwrTy_Mdv.ox' , '%s.faceLwrStretch' %bshName )
    mc.connectAttr('FaceLwrTy_Mdv.oy' , '%s.faceLwrSquash' %bshName )
    mc.connectAttr('FaceLwrTz_Mdv.ox' , '%s.faceLwrFront' %bshName )
    mc.connectAttr('FaceLwrTz_Mdv.oy' , '%s.faceLwrBack' %bshName )
