import maya.cmds as mc
import maya.mel as mm
import ppmel.core as core
reload( core )


def rigMain( charSize = 1 ) :
    
    #-- Group
    rigGrp = core.transform( 'rig_grp' )
    geoGrp = core.transform( 'geo_grp' )
    jntGrp = core.transform( 'jnt_grp' )
    ikhGrp = core.transform( 'ikh_grp' )
    animGrp = core.transform( 'anim_grp' )
    skinGrp = core.transform( 'skin_grp' )
    defGrp = core.transform( 'def_grp' )
    stillGrp = core.transform( 'still_grp' )
    
    #-- Controls
    masterCtrl = core.addCtrl( 'master_ctrl' , 'square' , 'yellow' )
    placementCtrl = core.addCtrl( 'placement_ctrl' , 'arrowCross' , 'yellow' )
    
    #-- Shape
    core.rotateShape( masterCtrl , 0 , 45 , 0 )
    core.scaleShape( masterCtrl , charSize * 7 )
    core.scaleShape( placementCtrl , charSize * 1.5 )
    
    #-- Rotate order
    for ctrl in ( masterCtrl , placementCtrl ) :
        core.setRotateOrder( ctrl , 'xzy' )
    
    #-- Rig process
    core.addAttr( masterCtrl , 'globalScale' )
    mc.setAttr( '%s.globalScale' %masterCtrl , 1 )
    
    mdv = core.multiplyDivide( 'globalScale_mdv' )
    
    mc.connectAttr( '%s.globalScale' %masterCtrl , '%s.sx' %masterCtrl )
    mc.connectAttr( '%s.globalScale' %masterCtrl , '%s.sy' %masterCtrl )
    mc.connectAttr( '%s.globalScale' %masterCtrl , '%s.sz' %masterCtrl )
    
    #-- Hierarchy
    core.parent( placementCtrl , masterCtrl )
    mc.parent( animGrp , skinGrp , jntGrp , ikhGrp , placementCtrl )
    mc.parent( geoGrp , masterCtrl , stillGrp , defGrp , rigGrp )
    
    #-- Cleanup
    for grp in ( rigGrp , geoGrp , jntGrp , ikhGrp , animGrp , skinGrp , stillGrp , defGrp ) :
        core.setLockHide( grp , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' )
    
    for ctrl in ( masterCtrl , placementCtrl  ) :
        core.setLockHide( ctrl , 'sx' , 'sy' , 'sz' , 'v' )
    
    for grp in ( jntGrp , ikhGrp , stillGrp ) :
        core.setVal( '%s.v' %grp , 0 )
    
    mc.select( cl = True )