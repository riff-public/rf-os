import maya.cmds as mc
import maya.mel as mm
import ppmel.core as core
reload( core )


def rigNeck( neck1TmpJnt = 'neck_tmpJnt' ,
             neck2TmpJnt = 'head1_tmpJnt' ,
             parent = 'spine4Pos_jnt' ,
             animGrp = 'anim_grp' ,
             skinGrp = 'skin_grp' ,
             jntGrp = 'jnt_grp' ,
             ribbon = True ,
             rotateOrder = 'xzy' ,
             axis = 'y' ,
             charSize = 1 ) :
    
    ## Main Rig ##
    #-- Group
    neckRigGrp = core.transform( 'neckRig_grp' )
    core.parCons( 0 , parent , neckRigGrp )
    
    #-- Joint
    neck1Jnt = core.addJnt( 'neck1_jnt' , neck1TmpJnt )
    neck2Jnt = core.addJnt( 'neck2_jnt' , neck2TmpJnt )
    core.parent( neck2Jnt , neck1Jnt , parent )
    
    #-- Controls
    neckCtrl = core.addCtrl( 'neck_ctrl' , 'circle' , 'red' , jnt = True )
    neckGmbl = core.addGimbal( neckCtrl )
    neckZro = core.addGrp( neckCtrl )
    core.snapPoint( neck1Jnt , neckZro )
    core.snapJntOrient( neck1Jnt , neckCtrl )
    
    #-- Shape
    for ctrl in ( neckCtrl , neckGmbl ) :
        core.scaleShape( ctrl , charSize )
    
    #-- Rotate order
    for each in ( neckCtrl , neck1Jnt , neck2Jnt ) :
        core.setRotateOrder( each , rotateOrder )
    
    #-- Rig process
    core.parCons( 0 , neckGmbl , neck1Jnt )
    core.addFkStretch( neckCtrl , neck2Jnt , axis )
    core.addLocalWorld( neckCtrl , 'anim_grp' , neckRigGrp , neckZro , 'orient' )
    
    #-- Hierarchy
    core.parent( neckZro , neckRigGrp , animGrp )
    
    #-- Cleanup
    for grp in ( neckRigGrp , neckZro ) :
        core.setLock( grp , True , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
    
    for ctrl in ( neckCtrl , neckGmbl ) :
        core.setLockHide( ctrl , 'sx' , 'sy' , 'sz' , 'v' )
        
        
    ## Ribbon Rig ##
    if ribbon == True :
        #-- Joint
        neckRbnJnt = core.addJnt( 'neckRbn_jnt' )
        core.snapPoint( neck1TmpJnt , neck2TmpJnt , neckRbnJnt )
        core.parent( neckRbnJnt , neck1Jnt )
        
        #-- Controls
        neckRbnCtrl = core.addCtrl( 'neckRbn_ctrl' , 'square' , 'yellow' , jnt = True )
        neckRbnGmbl = core.addGimbal( neckRbnCtrl )
        neckRbnZro = core.addGrp( neckRbnCtrl )
        neckRbnAim = core.addGrp( neckRbnCtrl , 'Aim' )
        core.snapPoint( neckRbnJnt , neckRbnZro )
        core.snapJntOrient( neckRbnJnt , neckRbnCtrl )
        
        #-- Shape
        for ctrl in ( neckRbnCtrl , neckRbnGmbl ) :
            core.scaleShape( ctrl , charSize )
        
        #-- Rig process
        core.parCons( 0 , neckRbnGmbl , neckRbnJnt )
        core.sclCons( 0 , neckRbnGmbl , neckRbnJnt )
        core.pntCons( 0 , neck1Jnt , neck2Jnt , neckRbnZro )
        mc.rename( mc.aimConstraint( neck2Jnt , neckRbnAim , aim = ( 0,1,0 ) , u = ( 0,1,0 ) , wut = "object" , wu = ( 0,1,0 ) , wuo = neckRbnZro )[0] , '%s_aimCons' %neckRbnAim )
        
        #-- Rotate order
        for each in ( neckRbnCtrl , neckRbnJnt ) :
            core.setRotateOrder( each , rotateOrder )
        
        #-- Hierarchy
        core.parent( neckRbnZro , neckGmbl )
        
        #-- Cleanup
        for ctrl in ( neckRbnCtrl , neckRbnGmbl ) :
            core.setLockHide( ctrl , 'v' )
    
    mc.select( cl = True )