import maya.cmds as mc
import maya.mel as mm
import ppmel.core as core
reload( core )


def rigClavicle( clav1TmpJnt = 'clav_lft_tmpJnt' ,
                 clav2TmpJnt = 'upArm_lft_tmpJnt' ,
                 parent = 'spine4_jnt' ,
                 animGrp = 'anim_grp' ,
                 skinGrp = 'skin_grp' ,
                 jntGrp = 'jnt_grp' ,
                 stillGrp = 'still_grp' ,
                 side = 'lft' ,
                 val = 0.1 , # Left = 0.1 , Right = -0.1
                 elem = '' , # Front , Back , Up , Down
                 charSize = 1 ) :
    
    #-- Info
    elem = elem.capitalize()

    #-- Group
    clavRigGrp = core.transform( 'clav%sRig_%s_grp' %( elem , side ) )
    core.parCons( 0 , parent , clavRigGrp )
    
    #-- Joint
    clav1Jnt = core.addJnt( 'clav1%s_%s_jnt' %( elem , side ) , clav1TmpJnt )
    clav2Jnt = core.addJnt( 'clav2%s_%s_jnt' %( elem , side ) , clav2TmpJnt )
    core.parent( clav2Jnt , clav1Jnt , parent )
    
    #-- Controls
    clavCtrl = core.addCtrl( 'clavicle%s_%s_ctrl' %( elem , side ) , 'stick' , 'red' , jnt = True )
    clavGmbl = core.addGimbal( clavCtrl )
    clavZro = core.addGrp( clavCtrl )
    core.snapPoint( clav1Jnt , clavZro )
    core.snapJntOrient( clav1Jnt , clavCtrl )
    
    #-- Shape
    for ctrl in ( clavCtrl , clavGmbl ) :
        core.scaleShape( ctrl , charSize )
        if side == 'lft' :
            core.rotateShape( ctrl , 90 , 0 ,0 )
        else :
            core.rotateShape( ctrl , -90 , 0 ,0 )
    
    #-- Rotate order
    for each in ( clavCtrl , clav1Jnt , clav2Jnt ) :
        core.setRotateOrder( each , 'xyz' )
    
    #-- Rig process
    core.parCons( 0 , clavGmbl , clav1Jnt )
    core.addFkStretch( clavCtrl , clav2Jnt , 'y' , val )
    
    #-- Hierarchy
    core.parent( clavZro , clavRigGrp , animGrp )
    
    #-- Cleanup
    for grp in ( clavRigGrp , clavZro ) :
        core.setLock( grp , True , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
    
    for ctrl in ( clavCtrl , clavGmbl ) :
        core.setLockHide( ctrl , 'sx' , 'sy' , 'sz' , 'v' )
    
    mc.select( cl = True )