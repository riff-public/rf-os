import maya.cmds as mc
import maya.mel as mm
import ppmel.core as core
reload( core )


def rigPelvis( pelvisTmpJnt = 'pelvis_tmpJnt' ,
               parent = 'root_jnt' ,
               animGrp = 'anim_grp' ,
               skinGrp = 'skin_grp' ,
               charSize = 1 ) :
    
    #-- Group
    pelvisRigGrp = core.transform( 'pelvisRig_grp' )
    core.parCons( 0 , parent , pelvisRigGrp )
    
    #-- Joint
    pelvisJnt = core.addJnt( 'pelvis_jnt' , pelvisTmpJnt )
    
    #-- Controls
    pelvisCtrl = core.addCtrl( 'pelvis_ctrl' , 'square' , 'red' , jnt = True )
    pelvisGmbl = core.addGimbal( pelvisCtrl )
    pelvisZro = core.addGrp( pelvisCtrl )
    core.snapPoint( pelvisJnt , pelvisZro )
    core.snapJntOrient( pelvisJnt , pelvisCtrl )
    
    #-- Shape
    for ctrl in ( pelvisCtrl , pelvisGmbl ) :
        core.scaleShape( ctrl , charSize * 1.8 )
    
    #-- Rotate order
    for each in ( pelvisCtrl , pelvisJnt ) :
        core.setRotateOrder( each , 'xzy' )
    
    #-- Rig process
    core.parCons( 0 , pelvisGmbl , pelvisJnt )
    core.addLocalWorld( pelvisCtrl , 'anim_grp' , pelvisRigGrp , pelvisZro , 'orient' )
    
    #-- Hierarchy
    core.parent( pelvisZro , pelvisRigGrp , animGrp )
    core.parent( pelvisJnt , parent )
    
    #-- Cleanup
    for grp in ( pelvisRigGrp , pelvisZro ) :
        core.setLock( grp , True , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
    
    for ctrl in ( pelvisCtrl , pelvisGmbl ) :
        core.setLockHide( ctrl , 'v' )
    
    mc.select( cl = True )
    return pelvisJnt