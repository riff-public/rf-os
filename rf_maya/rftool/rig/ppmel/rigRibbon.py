import maya.cmds as mc
import maya.mel as mm
import ppmel.core as core
reload( core )


def ribbonInfo( ax = '' ) :

    if ax == 'x+' :
        postAx = 'tx'
        twstAx =  'rx'
        sqshAx = [ 'sy' , 'sz' ]
        aimVec = [ 1 , 0 , 0 ]
        invAimVec = [ -1 , 0 , 0 ]
        upVec = [ 0 , 0 , 1 ]
        rotate = [ 0 , 0 , -90 ]
        
    elif ax == 'x-' :
        postAx = 'tx'
        twstAx =  'rx'
        sqshAx = [ 'sy' , 'sz' ]
        aimVec = [ -1 , 0 , 0 ]
        invAimVec = [ 1 , 0 , 0 ]
        upVec = [ 0 , 0 , 1 ]
        rotate = [ 0 , 0 , 90 ]
    
    elif ax == 'y+' :
        postAx = 'ty'
        twstAx =  'ry'
        sqshAx = [ 'sx' , 'sz' ]
        aimVec =  [ 0 , 1 , 0 ]
        invAimVec = [ 0 , -1 , 0 ]
        upVec = [ 0 , 0 , 1 ]
        rotate = [ 0 , 0 , 0 ]
        
    elif ax == 'y-' :
        postAx = 'ty'
        twstAx =  'ry'
        sqshAx = [ 'sx' , 'sz' ]
        aimVec = [ 0 , -1 , 0 ]
        invAimVec = [ 0 , 1 , 0 ]
        upVec = [ 0 , 0 , 1 ]
        rotate = [ 0 , 0 , 180 ]
    
    elif ax == 'z+' :
        postAx = 'tz'
        twstAx =  'rz'
        sqshAx = [ 'sx' , 'sy' ]
        aimVec = [ 0 , 0 , 1 ]
        invAimVec = [ 0 , 0 , -1 ]
        upVec = [ 0 , 1 , 0 ]
        rotate = [ 90 , 0 , 180 ]
    
    elif ax == 'z-' :
        postAx = 'tz'
        twstAx =  'rz'
        sqshAx = [ 'sx' , 'sy' ]
        aimVec = [ 0 , 0 , -1 ]
        invAimVec = [ 0 , 0 , 1 ]
        upVec = [ 0 , 1 , 0 ]
        rotate = [ -90 , 0 , 0 ]

    return { 'postAx' : postAx , 'twstAx' : twstAx , 'sqshAx' : sqshAx , 'aimVec' : aimVec , 'invAimVec' : invAimVec , 'upVec' : upVec , 'rotate' : rotate }


def rigRibbon( name = '' ,
               ax = 'x+' ,
               jntRoot = 'joint1' ,
               jntEnd = 'joint2' ,
               animGrp = 'anim_grp' ,
               skinGrp = 'skin_grp' ,
               stillGrp = 'still_grp' ,
               charSize = 1 ) :
    
    pars = core.getParent( jntRoot )
    side = core.getSide( jntRoot )
    
    rbnInfo   = ribbonInfo( ax = ax )
    
    postAx    = rbnInfo[ 'postAx' ]
    twstAx    = rbnInfo[ 'twstAx' ]
    sqshAx    = rbnInfo[ 'sqshAx' ]
    aimVec    = rbnInfo[ 'aimVec' ]
    invAimVec = rbnInfo[ 'invAimVec' ]
    upVec     = rbnInfo[ 'upVec' ]
    rotate    = rbnInfo[ 'rotate' ]
    
    #-- Group
    rbnGrp = core.transform( '%sRbn%s_grp' %( name , side ))
    rbnSkinGrp = core.transform( '%sRbnSkin%s_grp' %( name , side ))
    rbnStillGrp = core.transform( '%sRbnStill%s_grp' %( name , side ))
    
    #-- Nurbs
    lenght = core.getDist( jntRoot , jntEnd )
    halfLenght = lenght / 2
    
    rbnNrb = mc.nurbsPlane( p = (0,halfLenght,0) , ax = (0,0,1) , w = True , lr = lenght , d = 3 , u = 1 , v = 5 , ch = 0 , n = '%sRbn%s_nrb' % ( name , side ))[0]
    mc.setAttr( '%s.rx' %rbnNrb , rotate[0] )
    mc.setAttr( '%s.ry' %rbnNrb , rotate[1] )
    mc.setAttr( '%s.rz' %rbnNrb , rotate[2] )
    mc.makeIdentity( rbnNrb , apply = True , r = True )
    mc.select( cl = True )
    
    #-- Joint
    rbnRootJnt = core.addJnt( '%sRbnRoot%s_jnt' %( name , side ))
    rbnMidJnt = core.addJnt( '%sRbnMid%s_jnt' %( name , side ))
    rbnEndJnt = core.addJnt( '%sRbnEnd%s_jnt' %( name , side ))
    
    rbnRootJntAim = mc.group( rbnRootJnt , n = '%sRbnRootJntAim%s_grp' %( name , side ))
    rbnMidJntAim = mc.group( rbnMidJnt , n = '%sRbnMidJntAim%s_grp' %( name , side ))
    rbnEndJntAim = mc.group( rbnEndJnt , n = '%sRbnEndJntAim%s_grp' %( name , side ))
    
    #-- Controls
    rbnRootCtrl = core.addCtrl( '%sRbnRoot%s_ctrl' %( name , side ) , 'locator' , 'yellow' , jnt = False )
    rbnRootCtrlZro = core.addGrp( rbnRootCtrl )
    
    rbnEndCtrl = core.addCtrl( '%sRbnEnd%s_ctrl' %( name , side ) , 'locator' , 'yellow' , jnt = False )
    rbnEndCtrlZro = core.addGrp( rbnEndCtrl )
    
    rbnCtrl = core.addCtrl( '%sRbn%s_ctrl' %( name , side ) , 'square' , 'yellow' , jnt = False )
    rbnCtrlZro = core.addGrp( rbnCtrl )
    rbnCtrlGmbl = core.addGimbal( rbnCtrl )
    rbnCtrlAim = core.addGrp( rbnCtrl , 'Aim' )
    
    mc.parent( rbnMidJntAim , rbnCtrlGmbl )
    mc.parent( rbnEndJntAim , rbnEndCtrl )
    mc.parent( rbnRootJntAim , rbnRootCtrl )
    
    #-- Shape
    for ctrl in ( rbnCtrl , rbnCtrlGmbl ) :
        core.scaleShape( ctrl , charSize )
        core.rotateShape( ctrl , rotate[0] , rotate[1] , rotate[2] )
    
    #-- Rig process
    if ax in ( 'x-' , 'y-' , 'z-' ) :
        mc.setAttr( '%s.%s' %( rbnCtrlZro , postAx ) , -halfLenght )
        mc.setAttr( '%s.%s' %( rbnEndCtrlZro , postAx ) , -(halfLenght*2) )
    else :
        mc.setAttr( '%s.%s' %( rbnCtrlZro , postAx ) , halfLenght )
        mc.setAttr( '%s.%s' %( rbnEndCtrlZro , postAx ) , halfLenght*2 )
    
    core.pntCons( 0 , rbnRootCtrl , rbnEndCtrl , rbnCtrlZro )
    
    aimRootCons = mc.rename( mc.aimConstraint( rbnEndCtrl , rbnRootJntAim , aim = aimVec , u = upVec , wut = "objectrotation" , wu = upVec , wuo = rbnRootCtrlZro ) , '%s_aimCons' %rbnRootJntAim )
    aimEndCons = mc.rename( mc.aimConstraint( rbnRootCtrl , rbnEndJntAim , aim = invAimVec , u = upVec , wut = "objectrotation" , wu = upVec , wuo = rbnEndCtrlZro ) , '%s_aimCons' %rbnEndJntAim )
    aimMidCons = mc.rename( mc.aimConstraint( rbnEndCtrl , rbnCtrlAim , aim = aimVec , u = upVec , wut = "objectrotation" , wu = upVec , wuo = rbnCtrlZro ) , '%s_aimCons' %rbnCtrlAim )
    
    #-- Bind skin
    rbnSkc = mc.skinCluster( rbnRootJnt , rbnMidJnt , rbnEndJnt , rbnNrb , dr = 7 , mi = 2 , n = '%sRbn%s_skc' % ( name , side ))[0]
    
    mc.skinPercent( rbnSkc , '%s.cv[0:3][7]' %rbnNrb , tv = [ rbnEndJnt , 1 ] )
    mc.skinPercent( rbnSkc , '%s.cv[0:3][6]' %rbnNrb , tv = [ rbnMidJnt , 0.2 ] )
    mc.skinPercent( rbnSkc , '%s.cv[0:3][5]' %rbnNrb , tv = [ rbnMidJnt , 0.6 ] )
    mc.skinPercent( rbnSkc , '%s.cv[0:3][4]' %rbnNrb , tv = [ rbnEndJnt , 0.1 ] )
    mc.skinPercent( rbnSkc , '%s.cv[0:3][3]' %rbnNrb , tv = [ rbnRootJnt , 0.1 ] )
    mc.skinPercent( rbnSkc , '%s.cv[0:3][2]' %rbnNrb , tv = [ rbnRootJnt , 0.4 ] )
    mc.skinPercent( rbnSkc , '%s.cv[0:3][1]' %rbnNrb , tv = [ rbnRootJnt , 0.8 ] )
    mc.skinPercent( rbnSkc , '%s.cv[0:3][0]' %rbnNrb , tv = [ rbnRootJnt , 1 ] )
    
    #-- Twist distribute
    core.addAttrType( rbnCtrl , 'Twist' )
    core.addAttr( rbnCtrl , 'autoTwist' , 0 , 1 , 1 )
    core.addAttr( rbnCtrl , 'rootTwist' )
    core.addAttr( rbnCtrl , 'endTwist' )
        
    rbnCtrlShape = core.getShape( rbnCtrl )
    
    core.addAttr( rbnCtrlShape , 'detailVis' , 0 , 1 , 0 )
    core.addAttr( rbnCtrlShape , 'rootTwistAmp' )
    core.addAttr( rbnCtrlShape , 'endTwistAmp' )
    core.setHide( rbnCtrlShape , True , 'rootTwistAmp' )
    core.setHide( rbnCtrlShape , True , 'endTwistAmp' )
    
    rbnTwistGrp = core.transform( '%sRbnTwist%s_grp' %( name , side ))
    rbnRootTwGrp = core.transform( '%sRbnRootTwist%s_grp' %( name , side ))
    rbnEndTwGrp = core.transform( '%sRbnEndTwist%s_grp' %( name , side ))
    rbnRootTwZro = core.addGrp( rbnRootTwGrp )
    rbnEndTwZro = core.addGrp( rbnEndTwGrp )
    
    mc.parent( rbnRootTwZro , rbnEndTwZro , rbnTwistGrp )
    
    core.snap( rbnRootJnt , rbnTwistGrp )
    core.snap( rbnEndJnt , rbnEndTwZro )
    
    rbnAmpTwMdv = core.multiplyDivide( '%sRbnAmpTwist%s_mdv' %( name , side ))
    rbnAutoTwMdv = core.multiplyDivide( '%sRbnAutoTwist%s_mdv' %( name , side ))
    rbnRootAutoPma = core.plusMinusAverage( '%sRbnRootAutoTwist%s_pma' %( name , side ))
    rbnEndAutoPma = core.plusMinusAverage( '%sRbnEndAutoTwist%s_pma' %( name , side ))
    
    mc.connectAttr( '%s.%s' %( rbnRootTwGrp , twstAx ) , '%s.i1x' %rbnAmpTwMdv )
    mc.connectAttr( '%s.rootTwistAmp' %rbnCtrlShape , '%s.i2x' %rbnAmpTwMdv )
    mc.connectAttr( '%s.%s' %( rbnEndTwGrp , twstAx ) , '%s.i1y' %rbnAmpTwMdv )
    mc.connectAttr( '%s.endTwistAmp' %rbnCtrlShape , '%s.i2y' %rbnAmpTwMdv )
    
    mc.connectAttr( '%s.autoTwist' %rbnCtrl , '%s.i1x' %rbnAutoTwMdv )
    mc.connectAttr( '%s.autoTwist' %rbnCtrl , '%s.i1y' %rbnAutoTwMdv )
    
    mc.connectAttr( '%s.ox' %rbnAmpTwMdv , '%s.i2x' %rbnAutoTwMdv )
    mc.connectAttr( '%s.oy' %rbnAmpTwMdv , '%s.i2y' %rbnAutoTwMdv )
    
    mc.connectAttr( '%s.rootTwist' %rbnCtrl , '%s.i1[0]' %rbnRootAutoPma )
    mc.connectAttr( '%s.endTwist' %rbnCtrl , '%s.i1[0]' %rbnEndAutoPma )
    
    mc.connectAttr( '%s.ox' %rbnAutoTwMdv , '%s.i1[1]' %rbnRootAutoPma )
    mc.connectAttr( '%s.oy' %rbnAutoTwMdv , '%s.i1[1]' %rbnEndAutoPma )
    
    #-- Detail controls
    #-- Dict
    rbnPosGrpDict = []
    posiDtlDict = []
    rbnDtlAimDict = []
    rbnDtlJntDict = []
    rbnDtlJntZroDict = []
    rbnDtlCtrlDict = []
    rbnDtlCtrlZroDict = []
    rbnDtlCtrlTwsDict = []
    rbnDtlMdvDict = []
    rbnDtlPmaDict = []
    
    #-- Group
    rbnDetailGrp = core.transform( '%sRbnDtl%s_grp' % ( name , side ))
    
    for i in range( 1,6 ) :
        #-- Posi group
        rbnPosGrp = core.transform( '%s%sRbnDtlPos%s_grp' %( name , i , side ))
        posi = core.pointOnSurfaceInfo( '%s%sRbnDtlPos%s_posi' %( name , i , side ))
        
        mc.setAttr( '%s.turnOnPercentage' %posi , 1 )
        mc.setAttr ( '%s.parameterU' %posi , 0.5 )
        
        mc.connectAttr( '%s.worldSpace[0]' %rbnNrb , '%s.inputSurface' %posi )
        mc.connectAttr ( '%s.position' %posi , '%s.t' %rbnPosGrp )
        
        rbnPosGrpDict.append( rbnPosGrp )
        posiDtlDict.append( posi )
        
        #-- Aim constraint
        rbnAim = mc.createNode( 'aimConstraint' , n = '%s%sRbnDtl%s_aimCons' %( name , i , side ))
        
        mc.setAttr( '%s.a' %rbnAim , aimVec[0] , aimVec[1] , aimVec[2] )
        mc.setAttr( '%s.u' %rbnAim , upVec[0] , upVec[1] , upVec[2] )
        
        mc.connectAttr( '%s.n' %posi , '%s.wu' %rbnAim )
        mc.connectAttr( '%s.tv' %posi , '%s.tg[0].tt' %rbnAim )
        mc.connectAttr( '%s.cr' %rbnAim , '%s.r' %rbnPosGrp )
        
        rbnDtlAimDict.append( rbnAim )
        mc.parent( rbnAim , rbnPosGrp )
        
        mc.select( cl = True )
        
        #-- Joint
        rbnJnt = core.addJnt( '%s%sRbnDtl%s_jnt' %( name , i , side ))
        rbnJntZro = core.addGrp( rbnJnt )
        
        rbnDtlJntDict.append( rbnJnt )
        rbnDtlJntZroDict.append( rbnJntZro )
        
        #-- Controls
        rbnDtlCtrl = core.addCtrl( '%s%sRbnDtl%s_ctrl' %( name , i , side ) , 'circle' , 'cyan' , jnt = False )
        rbnDtlCtrlZro = core.addGrp( rbnDtlCtrl )
        rbnDtlCtrlTws = core.addGrp( rbnDtlCtrl , 'Twist' )
        core.snap( rbnJntZro , rbnDtlCtrlZro )
        
        rbnDtlCtrlDict.append( rbnDtlCtrl )
        rbnDtlCtrlZroDict.append( rbnDtlCtrlZro )
        rbnDtlCtrlTwsDict.append( rbnDtlCtrlTws )
        
        #-- Shape
        core.scaleShape( rbnDtlCtrl , charSize )
        core.rotateShape( rbnDtlCtrl , rotate[0] , rotate[1] , rotate[2] )
        
        #-- Rig process
        core.parCons( 0 , rbnPosGrp , rbnDtlCtrlZro )
        core.parCons( 0 , rbnDtlCtrl , rbnJntZro )
        
        rbnDtlMdv = core.multiplyDivide( '%s%sRbnDtl%s_mdv' %( name , i , side ))
        rbnDtlPma = core.plusMinusAverage( '%s%sRbnDtl%s_pma' %( name , i , side ))
        
        rbnDtlMdvDict.append( rbnDtlMdv )
        rbnDtlPmaDict.append( rbnDtlPma )
        
        mc.connectAttr( '%s.o1' %rbnRootAutoPma , '%s.i2x' %rbnDtlMdv )
        mc.connectAttr( '%s.o1' %rbnEndAutoPma , '%s.i2y' %rbnDtlMdv )
        
        mc.connectAttr( '%s.ox' %rbnDtlMdv , '%s.i1[0]' %rbnDtlPma )
        mc.connectAttr( '%s.oy' %rbnDtlMdv , '%s.i1[1]' %rbnDtlPma )
        
        mc.connectAttr( '%s.o1' %rbnDtlPma , '%s.%s' %( rbnDtlCtrlTws , twstAx ))
        
        mc.connectAttr( '%s.detailVis' %rbnCtrlShape , '%s.v' %rbnDtlCtrlZro )
    
    mc.setAttr ( '%s.parameterV' %posiDtlDict[0] , 0.1 )
    mc.setAttr ( '%s.parameterV' %posiDtlDict[1] , 0.3 )
    mc.setAttr ( '%s.parameterV' %posiDtlDict[2] , 0.5 )
    mc.setAttr ( '%s.parameterV' %posiDtlDict[3] , 0.7 )
    mc.setAttr ( '%s.parameterV' %posiDtlDict[4] , 0.9 )
    
    mc.setAttr ( '%s.i1x' %rbnDtlMdvDict[0] , 1 )
    mc.setAttr ( '%s.i1y' %rbnDtlMdvDict[0] , 0 )
    mc.setAttr ( '%s.i1x' %rbnDtlMdvDict[1] , 0.75 )
    mc.setAttr ( '%s.i1y' %rbnDtlMdvDict[1] , 0.25 )
    mc.setAttr ( '%s.i1x' %rbnDtlMdvDict[2] , 0.5 )
    mc.setAttr ( '%s.i1y' %rbnDtlMdvDict[2] , 0.5 )
    mc.setAttr ( '%s.i1x' %rbnDtlMdvDict[3] , 0.25 )
    mc.setAttr ( '%s.i1y' %rbnDtlMdvDict[3] , 0.75 )
    mc.setAttr ( '%s.i1x' %rbnDtlMdvDict[4] , 0 )
    mc.setAttr ( '%s.i1y' %rbnDtlMdvDict[4] , 1 )
    
    #-- Squash
    core.addAttrType( rbnCtrl , 'Squash' )
    core.addAttr( rbnCtrl , 'squash' )
    
    rbnSqshMdv = core.multiplyDivide( '%sRbnSqsh%s_mdv' % ( name , side ))
    mc.setAttr( '%s.i2x' %rbnSqshMdv , 0.1 )
    
    core.addSquash( rbnDtlCtrlDict[0] , rbnDtlJntDict[0] , ax = ( sqshAx[0] , sqshAx[1] ))
    core.addSquash( rbnDtlCtrlDict[1] , rbnDtlJntDict[1] , ax = ( sqshAx[0] , sqshAx[1] ))
    core.addSquash( rbnDtlCtrlDict[2] , rbnDtlJntDict[2] , ax = ( sqshAx[0] , sqshAx[1] ))
    core.addSquash( rbnDtlCtrlDict[3] , rbnDtlJntDict[3] , ax = ( sqshAx[0] , sqshAx[1] ))
    core.addSquash( rbnDtlCtrlDict[4] , rbnDtlJntDict[4] , ax = ( sqshAx[0] , sqshAx[1] ))
    
    mc.connectAttr( '%s.squash' %rbnCtrl , '%s.i1x' %rbnSqshMdv )
    mc.connectAttr( '%s.ox' %rbnSqshMdv , '%s1RbnDtlSqsh%s_pma.i1[2]' %( name , side ))
    mc.connectAttr( '%s.ox' %rbnSqshMdv , '%s2RbnDtlSqsh%s_pma.i1[2]' %( name , side ))
    mc.connectAttr( '%s.ox' %rbnSqshMdv , '%s3RbnDtlSqsh%s_pma.i1[2]' %( name , side ))
    mc.connectAttr( '%s.ox' %rbnSqshMdv , '%s4RbnDtlSqsh%s_pma.i1[2]' %( name , side ))
    mc.connectAttr( '%s.ox' %rbnSqshMdv , '%s5RbnDtlSqsh%s_pma.i1[2]' %( name , side ))
    
    #-- Hirerachy
    mc.parent( rbnGrp , animGrp )
    mc.parent( rbnSkinGrp , skinGrp )
    mc.parent( rbnStillGrp , stillGrp )
    
    mc.parent( rbnDtlJntZroDict , rbnSkinGrp )
    mc.parent( rbnNrb , rbnPosGrpDict , rbnStillGrp )
    mc.parent( rbnDtlCtrlZroDict , rbnDetailGrp )
    mc.parent( rbnRootCtrlZro , rbnEndCtrlZro , rbnCtrlZro , rbnDetailGrp , rbnTwistGrp , rbnGrp )
    
    #-- Cleanup
    for grp in ( rbnSkinGrp , rbnStillGrp , rbnDetailGrp ) :
        core.setLock( grp , True , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
    
    for dicts in ( rbnDtlCtrlZroDict , rbnDtlJntZroDict , rbnDtlCtrlTwsDict , rbnDtlJntDict ) :
        for each in dicts :
            core.setLock( each , True , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
    
    for each in ( rbnRootJnt , rbnMidJnt , rbnEndJnt , rbnNrb ) :
        core.setVal( '%s.v' %each , 0 )
    
    for dtlCtrl in rbnDtlCtrlDict :
        core.setLockHide( dtlCtrl , 'sx' , 'sy' , 'sz' , 'v' )
        
    core.setLockHide( rbnCtrl , 'sx' , 'sy' , 'sz' , 'v' )
    
    return { 'rbnGrp' : rbnGrp , 'rbnCtrl' : rbnCtrl , 'rbnRootCtrl' : rbnRootCtrl , 'rbnEndCtrl' : rbnEndCtrl , 'rbnRootTwZro' : rbnRootTwZro , 
             'rbnEndTwZro' : rbnEndTwZro , 'rbnRootTwGrp' : rbnRootTwGrp , 'rbnEndTwGrp' : rbnEndTwGrp , 'rbnCtrlShape' : rbnCtrlShape }
    
    mc.select( cl = True )