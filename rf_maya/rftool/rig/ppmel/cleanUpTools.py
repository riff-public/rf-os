import maya.cmds as mc
import maya.mel as mm


def clearLayer() :
    layers = mc.ls( type = 'displayLayer' )
    for layer in layers :
        if not layer == 'defaultLayer' :
            attrs = ( 'identification' , 'drawInfo' )
            for attr in attrs :
                objs = mc.listConnections( '%s.%s' %(layer , attr) , p = True )
                if objs :
                    for obj in objs :
                        if 'layerManager' in obj :
                            mc.disconnectAttr( obj , '%s.%s' %(layer , attr) )
                        else :
                            mc.disconnectAttr( '%s.%s' %(layer , attr) , obj )
                            
            mc.delete( layer )
        
        
    print '\n## Layer has been clear.\n'


def clearUnusedAnim() :
    animCrvs = mc.ls( type = 'animCurve' )
    for animCrv in animCrvs :
        if not mc.listConnections( animCrv ) :
            mc.delete( animCrv )
    
    print '\n## Anim has been clear.\n'


def clearUnknowNode() :
    try : mc.delete( mc.ls ( type = 'unknown' ))
    except TypeError : pass
    
    print '\n## UnknowNode has been clear.\n'


def clearUnusedNode() :
    mm.eval( 'MLdeleteUnused' )
    
    print '\n## UnusedNode has been clear.\n'


def clearUnusedShape() :
    types = [ 'mesh' , 'nurbsSurface' ]
    
    try :
        for type in types :
            shape = mc.ls( type = type )
            
            if len(shape) > 1 :
                shapes = shape 
            else :
                shapes = []
                shapes.append( shape )
                
            for shp in shapes :
                if not mc.listConnections( shp , s = False , d = True , type = 'shadingEngine') :
                    mc.delete( shp )
    
    except TypeError : pass
    
    print '\n## UnusedShape has been clear.\n'


def clearShapeName() :
    transformNode = mc.ls( type = 'transform' )
    
    if transformNode :
        for tfn in transformNode :
            shapes = mc.listRelatives( tfn , s = True )
            
            if shapes :
                if not mc.nodeType( shapes[0] ) == 'camera' :
                    mc.rename( shapes[0] , '%sShape' %tfn )
    
    print '\n## ShapeName has been clear.\n'


def clearNamespace() :
    allNamespace = mc.namespaceInfo( lon = True )
    
    if allNamespace :
        allNamespace.remove( 'UI' )
        allNamespace.remove( 'shared' )
    
    if allNamespace :
        for nss in allNamespace :
            mc.namespace( mv = [ nss ,":" ] , force = True )
            mc.namespace( rm = nss )
    else :
        mc.warning( 'Please Select Object.' )


def reduceNamespace() :
    allNamespace = mc.namespaceInfo( lon = True )
    
    if allNamespace :
        allNamespace.remove( 'UI' )
        allNamespace.remove( 'shared' )
    
    reffile = mc.file ( q = True , r = True )
    refs = []
    
    for ref in reffile :
        refNameSpace = mc.referenceQuery ( ref , referenceNode = True ).replace ( "RN" , "" )
        refs.append( refNameSpace )
        
    for namespaces in allNamespace :
        if not namespaces in refs :
            allSels = mc.ls( '%s:*' %namespaces )
            
            dicts = []
            
            for each in allSels :
                suffix = each.split(':')[1]
                
                if not 'VRayLightMtl1SG' in suffix and not 'Shape' in suffix :
                    dicts.append( suffix )
                    print '%s' %suffix
                    
            mc.namespace( mv = [ namespaces ,":"  ] , force = True )
            mc.namespace( rm = namespaces )
            
            for dict in dicts :
                try : mc.rename( dict , '%s_%s' %( namespaces , dict ))
                except RuntimeError : pass
                
        else :
            print '\n## The source namespace "%s" contains referenced nodes.' %namespaces