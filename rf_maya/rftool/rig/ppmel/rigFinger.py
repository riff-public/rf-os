import maya.cmds as mc
import maya.mel as mm
import ppmel.core as core
reload( core )
    
    
def rigFinger( fngr1TmpJnt = 'index1_lft_tmpJnt' ,
               fngr2TmpJnt = 'index2_lft_tmpJnt' ,
               fngr3TmpJnt = 'index3_lft_tmpJnt' ,
               fngr4TmpJnt = 'index4_lft_tmpJnt' ,
               fngr5TmpJnt = 'index5_lft_tmpJnt' ,
               parent = 'hand_lft_jnt' ,
               animGrp = 'anim_grp' ,
               skinGrp = 'skin_grp' ,
               jntGrp = 'jnt_grp' ,
               stillGrp = 'still_grp' ,
               side = 'lft' ,
               finger = 'index' ,
               type = 'hand' , # hand , foot
               elem = '' ,
               val = 0.1 , # Left = 0.1 , Right = -0.1
               charSize = 1 ) :
    
    
    ## Main Rig ##
    if 'foot' in type :
        type = ('%s' %type).capitalize()
    else :
        type = ''
        
    #-- Group
    fngrRigGrp = core.transform( '%s%s%sRig_%s_grp' %( finger , elem , type , side ))
    core.parCons( 0 , parent , fngrRigGrp )
    core.sclCons( 0 , parent , fngrRigGrp )
    
    #-- Joint
    fngr1Jnt = core.addJnt( '%s%s1%s_%s_jnt' %( finger , elem , type , side ) , fngr1TmpJnt )
    fngr2Jnt = core.addJnt( '%s%s2%s_%s_jnt' %( finger , elem , type , side ) , fngr2TmpJnt )
    fngr3Jnt = core.addJnt( '%s%s3%s_%s_jnt' %( finger , elem , type , side ) , fngr3TmpJnt )
    fngr4Jnt = core.addJnt( '%s%s4%s_%s_jnt' %( finger , elem , type , side ) , fngr4TmpJnt )
    core.parent( fngr4Jnt , fngr3Jnt , fngr2Jnt , fngr1Jnt , parent )
    core.setVal( '%s.ssc' %fngr1Jnt , 0 )
    
    if not fngr5TmpJnt == '' :
        fngr5Jnt = core.addJnt( '%s%s5%s_%s_jnt' %( finger , elem , type , side ) , fngr5TmpJnt )
        mc.parent( fngr5Jnt , fngr4Jnt )
    
    #-- Controls
    fngrCtrl = core.addCtrl( '%s%s%sAttr_%s_ctrl' %( finger , elem , type , side ) , 'stick' , 'blue' , jnt = False )
    fngrZro = core.addGrp( fngrCtrl )
    fngrCtrlShape = core.getShape( fngrCtrl )
    
    fngr1Ctrl = core.addCtrl( '%s%s1%s_%s_ctrl' %( finger , elem , type , side ) , 'circle' , 'red' , jnt = True )
    fngr1Gmbl = core.addGimbal( fngr1Ctrl )
    fngr1Zro = core.addGrp( fngr1Ctrl )
    fngr1Ofst = core.addGrp( fngr1Ctrl , 'Ofst' )
    fngr1Drv = core.addGrp( fngr1Ctrl , 'Drv' )
    core.snap( fngr1Jnt , fngr1Zro )
    mc.parent( fngr1Zro , fngrRigGrp )
    
    fngr2Ctrl = core.addCtrl( '%s%s2%s_%s_ctrl' %( finger , elem , type , side ) , 'circle' , 'red' , jnt = True )
    fngr2Gmbl = core.addGimbal( fngr2Ctrl )
    fngr2Zro = core.addGrp( fngr2Ctrl )
    fngr2Ofst = core.addGrp( fngr2Ctrl , 'Ofst' )
    fngr2Drv = core.addGrp( fngr2Ctrl , 'Drv' )
    core.snap( fngr2Jnt , fngr2Zro )
    mc.parent( fngr2Zro , fngr1Gmbl )
    
    fngr3Ctrl = core.addCtrl( '%s%s3%s_%s_ctrl' %( finger , elem , type , side ) , 'circle' , 'red' , jnt = True )
    fngr3Gmbl = core.addGimbal( fngr3Ctrl )
    fngr3Zro = core.addGrp( fngr3Ctrl )
    fngr3Ofst = core.addGrp( fngr3Ctrl , 'Ofst' )
    fngr3Drv = core.addGrp( fngr3Ctrl , 'Drv' )
    core.snap( fngr3Jnt , fngr3Zro )
    mc.parent( fngr3Zro , fngr2Gmbl )
    
    if not fngr5TmpJnt == '' :
        fngr4Ctrl = core.addCtrl( '%s%s4%s_%s_ctrl' %( finger , elem , type , side ) , 'circle' , 'red' , jnt = True )
        fngr4Gmbl = core.addGimbal( fngr4Ctrl )
        fngr4Zro = core.addGrp( fngr4Ctrl )
        fngr4Ofst = core.addGrp( fngr4Ctrl , 'Ofst' )
        fngr4Drv = core.addGrp( fngr4Ctrl , 'Drv' )
        core.snap( fngr4Jnt , fngr4Zro )
        mc.parent( fngr4Zro , fngr3Gmbl )
        
    #-- Shape
    if side == 'lft' :
        core.rotateShape( fngrCtrl , 90 , 0 , 0 )
    elif side == 'rgt' :
        core.rotateShape( fngrCtrl , -90 , 0 , 0 )
        
    core.scaleShape( fngrCtrl , charSize )
        
    #-- Rotate order
    for each in ( fngr1Ctrl , fngr2Ctrl , fngr3Ctrl ) :
        core.setRotateOrder( each , 'xyz' )
    
    if not fngr5TmpJnt == '' :
        core.setRotateOrder( fngr4Ctrl , 'xyz' )
    
    #-- Rig process
    core.parCons( 0 , fngr2Jnt , fngrZro )
     
    core.parCons( 0 , fngr1Gmbl , fngr1Jnt )
    core.parCons( 0 , fngr2Gmbl , fngr2Jnt )
    core.parCons( 0 , fngr3Gmbl , fngr3Jnt )
    
    core.addSquash( fngr1Ctrl , fngr1Jnt )
    core.addSquash( fngr2Ctrl , fngr2Jnt )
    core.addSquash( fngr3Ctrl , fngr3Jnt )
    core.addFkStretch( fngr1Ctrl , fngr2Ofst )
    core.addFkStretch( fngr2Ctrl , fngr3Ofst )
    
    mc.addAttr( fngrCtrl , ln = 'fold' , at = 'float' , k = True )
    mc.addAttr( fngrCtrl , ln = 'twist' , at = 'float' , k = True )
    mc.addAttr( fngrCtrl , ln = 'slide' , at = 'float' , k = True )
    mc.addAttr( fngrCtrl , ln = 'scruch' , at = 'float' , k = True )
    mc.addAttr( fngrCtrl , ln = 'stretch' , at = 'float' , k = True )
    mc.addAttr( fngrCtrl , ln = 'finger1' , nn = '__01' , at = 'float' , k = True )
    mc.addAttr( fngrCtrl , ln = 'finger1Fold' , nn = 'Fold 01' , at = 'float' , k = True )
    mc.addAttr( fngrCtrl , ln = 'finger1Twist' , nn = 'Twist 01' , at = 'float' , k = True )
    mc.addAttr( fngrCtrl , ln = 'finger1Spread' , nn = 'Spread 01' , at = 'float' , k = True )
    mc.addAttr( fngrCtrl , ln = 'finger2' , nn = '__02' , at = 'float' , k = True )
    mc.addAttr( fngrCtrl , ln = 'finger2Fold' , nn = 'Fold 02' , at = 'float' , k = True )
    mc.addAttr( fngrCtrl , ln = 'finger2Twist' , nn = 'Twist 02' , at = 'float' , k = True )
    mc.addAttr( fngrCtrl , ln = 'finger2Spread' , nn = 'Spread 02' , at = 'float' , k = True )
    mc.addAttr( fngrCtrl , ln = 'finger3' , nn = '__03' , at = 'float' , k = True )
    mc.addAttr( fngrCtrl , ln = 'finger3Fold' , nn = 'Fold 03' , at = 'float' , k = True )
    mc.addAttr( fngrCtrl , ln = 'finger3Twist' , nn = 'Twist 03' , at = 'float' , k = True )
    mc.addAttr( fngrCtrl , ln = 'finger3Spread' , nn = 'Spread 03' , at = 'float' , k = True )
    mc.addAttr( fngrCtrlShape , ln = 'detailVis' , at = 'float' , min = 0 , max = 1 , k = True )
    
    core.setLock( fngrCtrl , True , 'finger1' , 'finger2' , 'finger3' )
        
    if not fngr5TmpJnt == '' :
        core.parCons( 0 , fngr4Gmbl , fngr4Jnt )
        
        core.addSquash( fngr4Ctrl , fngr4Jnt )
        core.addFkStretch( fngr3Ctrl , fngr4Ofst )
        core.addFkStretch( fngr4Ctrl , fngr5Jnt )
        
        mc.addAttr( fngrCtrl , ln = 'finger4' , nn = '__04' , at = 'float' , k = True )
        mc.addAttr( fngrCtrl , ln = 'finger4Fold' , nn = 'Fold 04' , at = 'float' , k = True )
        mc.addAttr( fngrCtrl , ln = 'finger4Twist' , nn = 'Twist 04' , at = 'float' , k = True )
        mc.addAttr( fngrCtrl , ln = 'finger4Spread' , nn = 'Spread 04' , at = 'float' , k = True )
        core.setLock( fngrCtrl , True , 'finger4' )
    else :
        core.addFkStretch( fngr3Ctrl , fngr4Jnt )        
    
    #-- Create node
    mdvFngrFold = core.multiplyDivide( '%s%s%sFoldAttr_%s_mdv' %( finger , elem , type , side ))
    mdvFngrTwist = core.multiplyDivide( '%s%s%sTwistAttr_%s_mdv' %( finger , elem , type , side ))
    mdvFngr2Slide = core.multiplyDivide( '%s%s2%sSlideAttr_%s_mdv' %( finger , elem , type , side ))
    mdvFngr3Slide = core.multiplyDivide( '%s%s3%sSlideAttr_%s_mdv' %( finger , elem , type , side ))
    mdvFngr2Scruch = core.multiplyDivide( '%s%s2%sScruchAttr_%s_mdv' %( finger , elem , type , side ))
    mdvFngr3Scruch = core.multiplyDivide( '%s%s3%sScruchAttr_%s_mdv' %( finger , elem , type , side ))
    mdvFngrStrt = core.multiplyDivide( '%s%s%sStrtAttr_%s_mdv' %( finger , elem , type , side ))
    mdvFngr1Drv = core.multiplyDivide( '%s%s1%sDrv_%s_mdv' %( finger , elem , type , side ))
    mdvFngr2Drv = core.multiplyDivide( '%s%s2%sDrv_%s_mdv' %( finger , elem , type , side ))
    mdvFngr3Drv = core.multiplyDivide( '%s%s3%sDrv_%s_mdv' %( finger , elem , type , side ))
    
    pmaFngr1Drv = core.plusMinusAverage( '%s%s1%sDrv_%s_pma' %( finger , elem , type , side ))
    pmaFngr2Drv = core.plusMinusAverage( '%s%s2%sDrv_%s_pma' %( finger , elem , type , side ))
    pmaFngr3Drv = core.plusMinusAverage( '%s%s3%sDrv_%s_pma' %( finger , elem , type , side ))
        
    if not fngr5TmpJnt == '' :
        mdvFngr4Slide = core.multiplyDivide( '%s%s4%sSlideAttr_%s_mdv' %( finger , elem , type , side ))
        mdvFngr4Scruch = core.multiplyDivide( '%s%s4%sScruchAttr_%s_mdv' %( finger , elem , type , side ))
        mdvFngr4Drv = core.multiplyDivide( '%s%s4%sDrv_%s_mdv' %( finger , elem , type , side ))
        pmaFngr4Drv = core.plusMinusAverage( '%s%s4%sDrv_%s_pma' %( finger , elem , type , side ))
    
    #-- Connect attribute
    mc.connectAttr( '%s.detailVis' %fngrCtrlShape , '%s.v' %fngr1Zro )
    
    mc.connectAttr( '%s.fold' %fngrCtrl , '%s.i1x' %mdvFngrFold )
    mc.connectAttr( '%s.twist' %fngrCtrl , '%s.i1y' %mdvFngrTwist )
    mc.connectAttr( '%s.slide' %fngrCtrl , '%s.i1x' %mdvFngr2Slide )
    mc.connectAttr( '%s.slide' %fngrCtrl , '%s.i1x' %mdvFngr3Slide )
    mc.connectAttr( '%s.scruch' %fngrCtrl , '%s.i1x' %mdvFngr2Scruch )
    mc.connectAttr( '%s.scruch' %fngrCtrl , '%s.i1x' %mdvFngr3Scruch )
    mc.connectAttr( '%s.stretch' %fngrCtrl , '%s.i1x' %mdvFngrStrt )
    mc.connectAttr( '%s.finger1Fold' %fngrCtrl , '%s.i1x' %mdvFngr1Drv )
    mc.connectAttr( '%s.finger1Twist' %fngrCtrl , '%s.i1y' %mdvFngr1Drv )
    mc.connectAttr( '%s.finger1Spread' %fngrCtrl , '%s.i1z' %mdvFngr1Drv )
    mc.connectAttr( '%s.finger2Fold' %fngrCtrl , '%s.i1x' %mdvFngr2Drv )
    mc.connectAttr( '%s.finger2Twist' %fngrCtrl , '%s.i1y' %mdvFngr2Drv )
    mc.connectAttr( '%s.finger2Spread' %fngrCtrl , '%s.i1z' %mdvFngr2Drv )
    mc.connectAttr( '%s.finger3Fold' %fngrCtrl , '%s.i1x' %mdvFngr3Drv )
    mc.connectAttr( '%s.finger3Twist' %fngrCtrl , '%s.i1y' %mdvFngr3Drv )
    mc.connectAttr( '%s.finger3Spread' %fngrCtrl , '%s.i1z' %mdvFngr3Drv )
    
    mc.connectAttr( '%s.o' %mdvFngr1Drv , '%s.i3[0]' %pmaFngr1Drv )
    mc.connectAttr( '%s.o' %mdvFngr2Drv , '%s.i3[0]' %pmaFngr2Drv )
    mc.connectAttr( '%s.o' %mdvFngr3Drv , '%s.i3[0]' %pmaFngr3Drv )
    mc.connectAttr( '%s.o' %mdvFngrFold , '%s.i3[1]' %pmaFngr2Drv )
    mc.connectAttr( '%s.o' %mdvFngrFold , '%s.i3[1]' %pmaFngr3Drv )
    mc.connectAttr( '%s.o' %mdvFngrTwist , '%s.i3[2]' %pmaFngr2Drv )
    mc.connectAttr( '%s.o' %mdvFngrTwist , '%s.i3[2]' %pmaFngr3Drv )
    mc.connectAttr( '%s.o' %mdvFngr2Slide , '%s.i3[3]' %pmaFngr2Drv )
    mc.connectAttr( '%s.o' %mdvFngr3Slide , '%s.i3[3]' %pmaFngr3Drv )
    mc.connectAttr( '%s.o' %mdvFngr2Scruch , '%s.i3[4]' %pmaFngr2Drv )
    mc.connectAttr( '%s.o' %mdvFngr3Scruch , '%s.i3[4]' %pmaFngr3Drv )
    
    mc.connectAttr( '%s.o3x' %pmaFngr1Drv , '%s.rx' %fngr1Drv )
    mc.connectAttr( '%s.o3y' %pmaFngr1Drv , '%s.ry' %fngr1Drv )
    mc.connectAttr( '%s.o3z' %pmaFngr1Drv , '%s.rz' %fngr1Drv )
    mc.connectAttr( '%s.o3x' %pmaFngr2Drv , '%s.rx' %fngr2Drv )
    mc.connectAttr( '%s.o3y' %pmaFngr2Drv , '%s.ry' %fngr2Drv )
    mc.connectAttr( '%s.o3z' %pmaFngr2Drv , '%s.rz' %fngr2Drv )
    mc.connectAttr( '%s.o3x' %pmaFngr3Drv , '%s.rx' %fngr3Drv )
    mc.connectAttr( '%s.o3y' %pmaFngr3Drv , '%s.ry' %fngr3Drv )
    mc.connectAttr( '%s.o3z' %pmaFngr3Drv , '%s.rz' %fngr3Drv )
    
    mc.connectAttr( '%s.ox' %mdvFngrStrt , '%s%s2%sStrt_%s_pma.i1[2]' %( finger , elem , type , side ))
    mc.connectAttr( '%s.ox' %mdvFngrStrt , '%s%s3%sStrt_%s_pma.i1[2]' %( finger , elem , type , side ))
    
    if not fngr5TmpJnt == '' :
        mc.connectAttr( '%s.slide' %fngrCtrl , '%s.i1x' %mdvFngr4Slide )
        mc.connectAttr( '%s.scruch' %fngrCtrl , '%s.i1x' %mdvFngr4Scruch )
        mc.connectAttr( '%s.finger4Fold' %fngrCtrl , '%s.i1x' %mdvFngr4Drv )
        mc.connectAttr( '%s.finger4Twist' %fngrCtrl , '%s.i1y' %mdvFngr4Drv )
        mc.connectAttr( '%s.finger4Spread' %fngrCtrl , '%s.i1z' %mdvFngr4Drv )
        mc.connectAttr( '%s.o' %mdvFngr4Drv , '%s.i3[0]' %pmaFngr4Drv )
        mc.connectAttr( '%s.o' %mdvFngrFold , '%s.i3[1]' %pmaFngr4Drv )
        mc.connectAttr( '%s.o' %mdvFngrTwist , '%s.i3[2]' %pmaFngr4Drv )
        mc.connectAttr( '%s.o' %mdvFngr4Slide , '%s.i3[3]' %pmaFngr4Drv )
        mc.connectAttr( '%s.o' %mdvFngr4Scruch , '%s.i3[4]' %pmaFngr4Drv )
        mc.connectAttr( '%s.o3x' %pmaFngr4Drv , '%s.rx' %fngr4Drv )
        mc.connectAttr( '%s.o3y' %pmaFngr4Drv , '%s.ry' %fngr4Drv )
        mc.connectAttr( '%s.o3z' %pmaFngr4Drv , '%s.rz' %fngr4Drv )
        
        mc.connectAttr( '%s.ox' %mdvFngrStrt , '%s%s4%sStrt_%s_pma.i1[2]' %( finger , elem , type , side ))
    
    #-- Set attribute
    mc.setAttr( '%s.i2x' %mdvFngrFold , -2 )
    mc.setAttr( '%s.i2x' %mdvFngr1Drv , -2 )
    mc.setAttr( '%s.i2x' %mdvFngr2Drv , -2 )
    mc.setAttr( '%s.i2x' %mdvFngr3Drv , -2 )
    mc.setAttr( '%s.i2x' %mdvFngr2Slide , 1 )
    mc.setAttr( '%s.i2x' %mdvFngr3Slide , -2 )
    mc.setAttr( '%s.i2x' %mdvFngr2Scruch , 3.5 )
    mc.setAttr( '%s.i2x' %mdvFngr3Scruch , -8 )
    mc.setAttr( '%s.i2x' %mdvFngrStrt , val )
    
    if not fngr5TmpJnt == '' :   
        mc.setAttr( '%s.i2x' %mdvFngr4Drv , -2 )
        mc.setAttr( '%s.i2x' %mdvFngr4Scruch , -8 )
        
    #-- Hierarchy
    if type == 'foot' :
        nameGrp = 'fngr%sFoot' %elem
    else :
        nameGrp = 'fngr%s' %elem
        
    if not mc.objExists( '%sRig_%s_grp' %( nameGrp , side )) :
        fngrAllGrp = core.transform( '%sRig_%s_grp' %( nameGrp , side ))
        core.snap( parent , fngrAllGrp )
        core.parent( fngrAllGrp , animGrp )
    else :
        fngrAllGrp = '%sRig_%s_grp' %( nameGrp , side )
    
    core.parent( fngrZro , fngrRigGrp , fngrAllGrp )
        
    #-- Cleanup
    for grp in ( fngrRigGrp , fngrZro , fngr1Zro , fngr2Zro , fngr3Zro , fngr1Ofst , fngr2Ofst , fngr3Ofst , fngr1Drv , fngr2Drv , fngr3Drv ) :
        core.setLock( grp , True , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
        
    for ctrl in ( fngr1Ctrl , fngr2Ctrl , fngr3Ctrl ) :
        core.setLockHide( ctrl , 'sx' , 'sy' , 'sz' , 'v' )

    core.setLockHide( fngrCtrl , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' ,'sx' , 'sy' , 'sz' , 'v' )
    
    if not fngr5TmpJnt == '' :
        for grp in ( fngr4Zro , fngr4Ofst , fngr4Drv ) :
            core.setLock( grp , True , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
        
        core.setLockHide( fngr4Ctrl , 'sx' , 'sy' , 'sz' , 'v' )
        
    core.setLockHide( fngrCtrl , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

    mc.select( cl = True )