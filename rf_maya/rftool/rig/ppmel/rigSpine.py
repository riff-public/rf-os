import maya.cmds as mc
import maya.mel as mm
import ppmel.core as core
reload( core )


def rigSpine( spine1TmpJnt = 'spine1_tmpJnt' ,
              spine2TmpJnt = 'spine2_tmpJnt' ,
              spine3TmpJnt = 'spine3_tmpJnt' ,
              spine4TmpJnt = 'spine4_tmpJnt' ,
              parent = 'root_jnt' ,
              animGrp = 'anim_grp' ,
              skinGrp = 'skin_grp' ,
              jntGrp = 'jnt_grp' ,
              spineFk = True ,
              spineIk = False ,
              rotateOrder = 'yzx' ,
              axis = 'y' ,
              charSize = 1 ) :
    
    ## Main Rig ##
    #-- Group
    spineRigGrp = core.transform( 'spineRig_grp' )
    spineJntGrp = core.transform( 'spineJnt_grp' )
    core.parCons( 0 , parent , spineRigGrp )
    core.snap( parent , spineJntGrp )
    
    #-- Joint
    spine1PosJnt = core.addJnt( 'spine1Pos_jnt' , spine1TmpJnt )
    spine2PosJnt = core.addJnt( 'spine2Pos_jnt' , spine2TmpJnt )
    spine3PosJnt = core.addJnt( 'spine3Pos_jnt' , spine3TmpJnt )
    spine4PosJnt = core.addJnt( 'spine4Pos_jnt' , spine4TmpJnt )
    
    spine1Jnt = core.addJnt( 'spine1_jnt' , spine1TmpJnt )
    spine2Jnt = core.addJnt( 'spine2_jnt' , spine2TmpJnt )
    spine3Jnt = core.addJnt( 'spine3_jnt' , spine3TmpJnt )
    spine4Jnt = core.addJnt( 'spine4_jnt' , spine4TmpJnt )
    
    core.parent( spine4PosJnt , spine3PosJnt , spine2PosJnt , spine1PosJnt )
    core.parent( spine4Jnt , spine4PosJnt )
    core.parent( spine3Jnt , spine3PosJnt )
    core.parent( spine2Jnt , spine2PosJnt )
    core.parent( spine1Jnt , spine1PosJnt )
    
    #-- Rotate order
    for each in ( spine1PosJnt , spine2PosJnt , spine3PosJnt , spine4PosJnt , spine1Jnt , spine2Jnt , spine3Jnt , spine4Jnt ) :
        core.setRotateOrder( each , rotateOrder )
    
    #-- Hierarchy
    core.parent( spineRigGrp , animGrp )
    core.parent( spineJntGrp , jntGrp )
    core.parent( spine1PosJnt , parent )
    
    #-- Cleanup
    for grp in ( spineRigGrp , spineJntGrp ) :
        core.setLock( grp , True , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
    
    
    ## Fk Rig ##
    if spineFk == True :
        #-- Group
        spineFkRigGrp = core.transform( 'spineFkRig_grp' )
        spineFkJntGrp = core.transform( 'spineFkJnt_grp' )
        core.snap( parent , spineFkRigGrp )
        core.snap( parent , spineFkJntGrp )
        
        #-- Joint
        spine1FkPosJnt = core.addJnt( 'spine1FkPos_jnt' , spine1TmpJnt )
        spine2FkPosJnt = core.addJnt( 'spine2FkPos_jnt' , spine2TmpJnt )
        spine3FkPosJnt = core.addJnt( 'spine3FkPos_jnt' , spine3TmpJnt )
        spine4FkPosJnt = core.addJnt( 'spine4FkPos_jnt' , spine4TmpJnt )
        
        spine1FkJnt = core.addJnt( 'spine1Fk_jnt' , spine1TmpJnt )
        spine2FkJnt = core.addJnt( 'spine2Fk_jnt' , spine2TmpJnt )
        spine3FkJnt = core.addJnt( 'spine3Fk_jnt' , spine3TmpJnt )
        spine4FkJnt = core.addJnt( 'spine4Fk_jnt' , spine4TmpJnt )
        
        core.parent( spine4FkPosJnt , spine3FkPosJnt , spine2FkPosJnt , spine1FkPosJnt )
        core.parent( spine4FkJnt , spine4FkPosJnt )
        core.parent( spine3FkJnt , spine3FkPosJnt )
        core.parent( spine2FkJnt , spine2FkPosJnt )
        core.parent( spine1FkJnt , spine1FkPosJnt )
        
        #-- Controls
        spine1FkCtrl = core.addCtrl( 'spine1Fk_ctrl' , 'circle' , 'red' , jnt = True )
        spine1FkGmbl = core.addGimbal( spine1FkCtrl )
        spine1FkZro = core.addGrp( spine1FkCtrl )
        spine1FkOfst = core.addGrp( spine1FkCtrl , 'Ofst' )
        core.snapPoint( spine1FkJnt , spine1FkZro )
        core.snapJntOrient( spine1FkJnt , spine1FkCtrl )
        
        spine2FkCtrl = core.addCtrl( 'spine2Fk_ctrl' , 'circle' , 'red' , jnt = True )
        spine2FkGmbl = core.addGimbal( spine2FkCtrl )
        spine2FkZro = core.addGrp( spine2FkCtrl )
        spine2FkOfst = core.addGrp( spine2FkCtrl , 'Ofst' )
        core.snapPoint( spine2FkJnt , spine2FkZro )
        core.snapJntOrient( spine2FkJnt , spine2FkCtrl )
        
        spine3FkCtrl = core.addCtrl( 'spine3Fk_ctrl' , 'circle' , 'red' , jnt = True )
        spine3FkGmbl = core.addGimbal( spine3FkCtrl )
        spine3FkZro = core.addGrp( spine3FkCtrl )
        spine3FkOfst = core.addGrp( spine3FkCtrl , 'Ofst' )
        core.snapPoint( spine3FkJnt , spine3FkZro )
        core.snapJntOrient( spine3FkJnt , spine3FkCtrl )
        
        spine4FkCtrl = core.addCtrl( 'spine4Fk_ctrl' , 'circle' , 'red' , jnt = True )
        spine4FkGmbl = core.addGimbal( spine4FkCtrl )
        spine4FkZro = core.addGrp( spine4FkCtrl )
        spine4FkOfst = core.addGrp( spine4FkCtrl , 'Ofst' )
        core.snapPoint( spine4FkJnt , spine4FkZro )
        core.snapJntOrient( spine3FkJnt , spine4FkCtrl )
        
        #-- Shape
        for ctrl in ( spine1FkCtrl , spine1FkGmbl , spine2FkCtrl , spine2FkGmbl , spine3FkCtrl , spine3FkGmbl , spine4FkCtrl , spine4FkGmbl ) :
            core.scaleShape( ctrl , charSize * 1.8 )
        
        #-- Rotate order
        for each in ( spine1FkCtrl , spine2FkCtrl , spine3FkCtrl , spine4FkCtrl , spine1FkPosJnt , spine2FkPosJnt , spine3FkPosJnt , spine4FkPosJnt , spine1FkJnt , spine2FkJnt , spine3FkJnt , spine4FkJnt ) :
            core.setRotateOrder( each , rotateOrder )
        
        #-- Rig process
        core.parCons( 0 , spine1FkGmbl , spine1FkPosJnt )
        core.parCons( 0 , spine2FkGmbl , spine2FkPosJnt )
        core.parCons( 0 , spine3FkGmbl , spine3FkPosJnt )
        core.parCons( 0 , spine4FkGmbl , spine4FkPosJnt )
        
        core.sclCons( 0 , spine1FkGmbl , spine1FkJnt )
        core.sclCons( 0 , spine2FkGmbl , spine2FkJnt )
        core.sclCons( 0 , spine3FkGmbl , spine3FkJnt )
        core.sclCons( 0 , spine4FkGmbl , spine4FkJnt )
        
        core.parCons( 0 , spine1FkPosJnt , spine1PosJnt )
        core.parCons( 0 , spine2FkPosJnt , spine2PosJnt )
        core.parCons( 0 , spine3FkPosJnt , spine3PosJnt )
        core.parCons( 0 , spine4FkPosJnt , spine4PosJnt )
        
        core.sclCons( 0 , spine1FkJnt , spine1Jnt )
        core.sclCons( 0 , spine2FkJnt , spine2Jnt )
        core.sclCons( 0 , spine3FkJnt , spine3Jnt )
        core.sclCons( 0 , spine4FkJnt , spine4Jnt )
        
        core.addFkStretch( spine1FkCtrl , spine2FkOfst , axis )
        core.addFkStretch( spine2FkCtrl , spine3FkOfst , axis )
        core.addFkStretch( spine3FkCtrl , spine4FkOfst , axis )
        
        core.addLocalWorld( spine1FkCtrl , 'anim_grp' , spineFkRigGrp , spine1FkZro , 'orient' )
            
        #-- Hierarchy
        core.parent( spine2FkZro , spine1FkGmbl )
        core.parent( spine3FkZro , spine2FkGmbl )
        core.parent( spine4FkZro , spine3FkGmbl )
        
        core.parent( spine1FkZro , spineFkRigGrp , spineRigGrp )
        core.parent( spine1FkPosJnt , spineFkJntGrp , spineJntGrp )
        
        #-- Cleanup
        for grp in ( spineFkRigGrp , spineFkJntGrp , spine1FkZro , spine2FkZro , spine3FkZro , spine4FkZro , spine1FkOfst , spine2FkOfst , spine3FkOfst , spine4FkOfst ) :
            core.setLock( grp , True , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
        
        for gmbl in ( spine1FkGmbl , spine2FkGmbl , spine3FkGmbl , spine4FkGmbl ) :
            core.setLockHide( gmbl , 'sx' , 'sy' , 'sz' , 'v' )
        
        for ctrl in ( spine1FkCtrl , spine2FkCtrl , spine3FkCtrl , spine4FkCtrl ) :
            core.setLockHide( ctrl , 'v' )
        
    mc.select( cl = True )