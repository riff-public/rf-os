import maya.cmds as mc
import maya.mel as mm
import ppmel.core as core
reload( core )


def rigRoot( rootTmpJnt = 'root_tmpJnt' ,
             animGrp = 'anim_grp' ,
             skinGrp = 'skin_grp' ,
             charSize = 1 ) :
    
    #-- Group
    rootRigGrp = core.transform( 'rootRig_grp' )
    core.snap( rootTmpJnt , rootRigGrp )
    
    #-- Joint
    rootJnt = core.addJnt( 'root_jnt' , rootTmpJnt )
    
    #-- Controls
    rootCtrl = core.addCtrl( 'root_ctrl' , 'circle' , 'cyan' , jnt = True )
    rootGmbl = core.addGimbal( rootCtrl )
    rootZro = core.addGrp( rootCtrl )
    core.snapPoint( rootJnt , rootZro )
    core.snapJntOrient( rootJnt , rootCtrl )
    
    #-- Shape
    for ctrl in ( rootCtrl , rootGmbl ) :
        core.scaleShape( ctrl , charSize * 2.3 )
    
    #-- Rotate order
    for each in ( rootJnt , rootCtrl ) :
        core.setRotateOrder( each , 'zxy' )
    
    #-- Rig process
    core.parCons( 0 , rootGmbl , rootJnt )
    
    #-- Hierarchy
    core.parent( rootZro , rootRigGrp , animGrp )
    core.parent( rootJnt , skinGrp )
    
    #-- Cleanup
    for grp in ( rootRigGrp , rootZro ) :
        core.setLock( grp , True , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
    
    for ctrl in ( rootCtrl , rootGmbl ) :
        core.setLockHide( ctrl , 'sx' , 'sy' , 'sz' , 'v' )
    
    mc.select( cl = True )