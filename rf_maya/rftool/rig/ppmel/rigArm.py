import maya.cmds as mc
import maya.mel as mm
import ppmel.core as core
import ppmel.rigRibbon as rbn
reload( core )
reload( rbn )


def rigArm( upArmTmpJnt = 'upArm_lft_tmpJnt' ,
            foreArmTmpJnt = 'foreArm_lft_tmpJnt' ,
            wristTmpJnt = 'wrist_lft_tmpJnt' ,
            handTmpJnt = 'hand_lft_tmpJnt' ,
            elbowTmpJnt = 'elbow_lft_tmpJnt' ,
            parent = 'clav2_lft_jnt' ,
            animGrp = 'anim_grp' ,
            skinGrp = 'skin_grp' ,
            jntGrp = 'jnt_grp' ,
            stillGrp = 'still_grp' ,
            ikhGrp = 'ikh_grp' ,
            side = 'lft' ,
            val = 0.1 , # Left = 0.1 , Right = -0.1
            elem = '' , # Front , Back , Up , Down
            armFk = True ,
            armIk = True ,
            ribbon = True ,
            charSize = 3 ) :
    
    #-- Info
    elem = elem.capitalize()
    
    ## Main Rig ##
    #-- Group
    armRigGrp = core.transform( 'arm%sRig_%s_grp' %( elem , side ))
    armJntGrp = core.transform( 'arm%sJnt_%s_grp' %( elem , side ))
    core.parCons( 0 , parent , armRigGrp )
    core.snap( parent , armJntGrp )
    
    #-- Joint
    upArmJnt = core.addJnt( 'upArm%s_%s_jnt' %( elem , side ) , upArmTmpJnt )
    foreArmJnt = core.addJnt( 'foreArm%s_%s_jnt' %( elem , side ) , foreArmTmpJnt )
    wristJnt = core.addJnt( 'wrist%s_%s_jnt' %( elem , side ) , wristTmpJnt )
    handJnt = core.addJnt( 'hand%s_%s_jnt' %( elem , side ) , handTmpJnt )
    core.parent( handJnt , wristJnt , foreArmJnt , upArmJnt , parent )
    
    #-- Controls
    armCtrl = core.addCtrl( 'arm%s_%s_ctrl' %( elem , side ) , 'stick' , 'green' , jnt = False )
    armZro = core.addGrp( armCtrl )
    
    #-- Shape
    if side == 'lft' :
        core.rotateShape( armCtrl , 0 , 90 , 90 )
    elif side == 'rgt' :
        core.rotateShape( armCtrl , 0 , 90 , -90 )
        
    core.scaleShape( armCtrl , charSize )
    
    #-- Rotate order
    for each in ( upArmJnt , foreArmJnt , wristJnt , handJnt ) :
        core.setRotateOrder( each , 'yxz' )
    
    #-- Rig process
    core.parCons( 0 , wristJnt , armZro )
    
    #-- Hierarchy
    core.parent( armZro , armRigGrp )
    core.parent( armJntGrp , jntGrp )
    
    #-- Cleanup
    for grp in ( armZro , armRigGrp ) :
        core.setLock( grp , True , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
    
    core.setLockHide( armCtrl , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
    
    
    ## Fk Rig ##
    if armFk == True :
        #-- Group
        armFkGrp = core.transform( 'arm%sFk_%s_grp' %( elem , side ))
        armFkJntGrp = core.transform( 'arm%sFkJnt_%s_grp' %( elem , side ))
        core.snap( armRigGrp , armFkGrp )
        core.snap( armRigGrp , armFkJntGrp )
        
        #-- Joint
        upArmFkJnt = core.addJnt( 'upArm%sFk_%s_jnt' %( elem , side ) , upArmTmpJnt )
        foreArmFkJnt = core.addJnt( 'foreArm%sFk_%s_jnt' %( elem , side ) , foreArmTmpJnt )
        wristFkJnt = core.addJnt( 'wrist%sFk_%s_jnt' %( elem , side ) , wristTmpJnt )
        handFkJnt = core.addJnt( 'hand%sFk_%s_jnt' %( elem , side ) , handTmpJnt )
        core.parent( handFkJnt , wristFkJnt , foreArmFkJnt , upArmFkJnt , armFkJntGrp )
        
        #-- Controls
        upArmFkCtrl = core.addCtrl( 'upArm%sFk_%s_ctrl' %( elem , side ) , 'circle' , 'red' , jnt = True )
        upArmFkGmbl = core.addGimbal( upArmFkCtrl )
        upArmFkZro = core.addGrp( upArmFkCtrl )
        upArmFkOfst = core.addGrp( upArmFkCtrl , 'Ofst' )
        core.snapPoint( upArmFkJnt , upArmFkZro )
        core.snapJntOrient( upArmFkJnt , upArmFkCtrl )
        
        foreArmFkCtrl = core.addCtrl( 'foreArm%sFk_%s_ctrl' %( elem , side ) , 'circle' , 'red' , jnt = True )
        foreArmFkGmbl = core.addGimbal( foreArmFkCtrl )
        foreArmFkZro = core.addGrp( foreArmFkCtrl )
        foreArmFkOfst = core.addGrp( foreArmFkCtrl , 'Ofst' )
        core.snapPoint( foreArmFkJnt , foreArmFkZro )
        core.snapJntOrient( foreArmFkJnt , foreArmFkCtrl )
        
        wristFkCtrl = core.addCtrl( 'wrist%sFk_%s_ctrl' %( elem , side ) , 'circle' , 'red' , jnt = True )
        wristFkGmbl = core.addGimbal( wristFkCtrl )
        wristFkZro = core.addGrp( wristFkCtrl )
        wristFkOfst = core.addGrp( wristFkCtrl , 'Ofst' )
        core.snapPoint( wristFkJnt , wristFkZro )
        core.snapJntOrient( wristFkJnt , wristFkCtrl )
        
        #-- Shape
        for ctrl in ( upArmFkCtrl , upArmFkGmbl , foreArmFkCtrl , foreArmFkGmbl , wristFkCtrl , wristFkGmbl ) :
            core.scaleShape( ctrl , charSize )
        
        #-- Rotate order
        for each in ( upArmFkJnt , foreArmFkJnt , wristFkJnt , handFkJnt , upArmFkCtrl , foreArmFkCtrl , wristFkCtrl ) :
            core.setRotateOrder( each , 'yxz' )
        
        #-- Rig process
        core.parCons( 0 , upArmFkGmbl , upArmFkJnt )
        core.parCons( 0 , foreArmFkGmbl , foreArmFkJnt )
        core.parCons( 0 , wristFkGmbl , wristFkJnt )
        
        core.addFkStretch( upArmFkCtrl , foreArmFkOfst , 'x' , val )
        core.addFkStretch( foreArmFkCtrl , wristFkOfst , 'x' , val )
        
        core.addLocalWorld( upArmFkCtrl , 'anim_grp' , armFkGrp , upArmFkZro , 'orient' )
        
        #-- Hierarchy
        mc.parent( armFkGrp , armRigGrp )
        mc.parent( armFkJntGrp , armJntGrp )
        mc.parent( upArmFkZro , armFkGrp )
        mc.parent( foreArmFkZro , upArmFkGmbl )
        mc.parent( wristFkZro , foreArmFkGmbl )
        
        #-- Cleanup
        for grp in ( armFkGrp , armFkJntGrp , upArmFkZro , upArmFkOfst , foreArmFkZro , foreArmFkOfst , wristFkZro , wristFkOfst ) :
            core.setLock( grp , True , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
        
        for ctrl in ( upArmFkCtrl , upArmFkGmbl , foreArmFkCtrl , foreArmFkGmbl , wristFkCtrl , wristFkGmbl ) :
            core.setLockHide( ctrl , 'sx' , 'sy' , 'sz' , 'v' )
            
        core.setVal( '%s.localWorld' %upArmFkCtrl , 1 )
        
        
    ## Ik Rig ##
    if armIk == True :
        #-- Group
        armIkGrp = core.transform( 'arm%sIk_%s_grp' %( elem , side ))
        armIkJntGrp = core.transform( 'arm%sIkJnt_%s_grp' %( elem , side ))
        armIkIkhGrp = core.transform( 'arm%sIkHandle_%s_grp' %( elem , side ))
        core.snap( armRigGrp , armIkGrp )
        core.snap( armRigGrp , armIkJntGrp )
        core.snap( armRigGrp , armIkIkhGrp )
        
        #-- Joint
        upArmIkJnt = core.addJnt( 'upArm%sIk_%s_jnt' %( elem , side ) , upArmTmpJnt )
        foreArmIkJnt = core.addJnt( 'foreArm%sIk_%s_jnt' %( elem , side ) , foreArmTmpJnt )
        wristIkJnt = core.addJnt( 'wrist%sIk_%s_jnt' %( elem , side ) , wristTmpJnt )
        handIkJnt = core.addJnt( 'hand%sIk_%s_jnt' %( elem , side ) , handTmpJnt )
        core.parent( handIkJnt , wristIkJnt , foreArmIkJnt , upArmIkJnt , armIkJntGrp )
        
        #-- Controls
        upArmIkCtrl = core.addCtrl( 'upArm%sIk_%s_ctrl' %( elem , side ) , 'cube' , 'blue' , jnt = True )
        upArmIkGmbl = core.addGimbal( upArmIkCtrl )
        upArmIkZro = core.addGrp( upArmIkCtrl )
        core.snapPoint( upArmIkJnt , upArmIkZro )
        core.snapJntOrient( upArmIkJnt , upArmIkCtrl )
        
        wristIkCtrl = core.addCtrl( 'wrist%sIk_%s_ctrl' %( elem , side ) , 'cube' , 'blue' , jnt = True )
        wristIkZro = core.addGrp( wristIkCtrl )
        wristIkCons = core.addCons( wristIkCtrl )
        wristIkGmbl = core.addGimbal( wristIkCtrl )
        core.snapPoint( wristIkJnt , wristIkZro )
        core.snapJntOrient( wristIkJnt , wristIkCtrl )
        
        elbowIkCtrl = core.addCtrl( 'elbow%sIk_%s_ctrl' %( elem , side ) , 'sphere' , 'blue' , jnt = False )
        elbowIkZro = core.addGrp( elbowIkCtrl )
        core.snapPoint( elbowTmpJnt , elbowIkZro )
        
        #-- Shape
        for ctrl in ( upArmIkCtrl , upArmIkGmbl , wristIkCtrl , wristIkGmbl , elbowIkCtrl) :
            core.scaleShape( ctrl , charSize )
        
        #-- Rotate order
        for each in ( upArmIkJnt , wristIkJnt , handIkJnt , upArmIkCtrl , wristIkCtrl ) :
            core.setRotateOrder( each , 'yxz' )
        
        #-- Rig process
        crv = core.addLine( elbowIkCtrl , foreArmIkJnt )
        
        wristIkhDict = core.addIkh( 'wrist%sIk' %elem , 'ikRPsolver' , upArmIkJnt , wristIkJnt )
        handIkhDict = core.addIkh( 'hand%sIk' %elem , 'ikSCsolver' , wristIkJnt , handIkJnt )
        
        wristIkh = wristIkhDict['ikh']
        wristIkhZro = wristIkhDict['ikhZro']
        handIkh = handIkhDict['ikh']
        handIkhZro = handIkhDict['ikhZro']
        
        polevector = core.pvtCons( elbowIkCtrl , wristIkh )
        core.addAttr( wristIkCtrl , 'twist' )
        
        core.connectAttr( '%s.twist' %wristIkCtrl , '%s.twist' %wristIkh )
        
        core.pntCons( 0 , upArmIkGmbl , upArmIkJnt  )
        core.parCons( 1 , wristIkGmbl , wristIkhZro  )
        core.parCons( 1 , wristIkGmbl , handIkhZro  )
        
        core.addLocalWorld( wristIkCtrl , 'anim_grp' , upArmIkGmbl , wristIkZro , 'parent' )
        core.addLocalWorld( elbowIkCtrl , 'anim_grp' , wristIkCtrl , elbowIkZro , 'parent' )
        
        ikStrt = core.addIkStretch( wristIkCtrl , elbowIkCtrl ,'ty' , 'arm%s' %elem , val , upArmIkJnt , foreArmIkJnt , wristIkJnt )
        
        #-- Hierarchy
        mc.parent( armIkGrp , armRigGrp )
        mc.parent( armIkJntGrp , armJntGrp )
        mc.parent( armIkIkhGrp , ikhGrp )
        mc.parent( upArmIkZro , ikStrt , crv , armIkGrp )
        mc.parent( wristIkZro , elbowIkZro , upArmIkGmbl )
        mc.parent( wristIkhZro , handIkhZro , armIkIkhGrp )
        
        #-- Cleanup
        for grp in ( armIkGrp , armIkJntGrp , armIkIkhGrp , upArmIkZro , elbowIkZro , wristIkZro ) :
            core.setLock( grp , True , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
        
        for ctrl in ( upArmIkCtrl , upArmIkGmbl , elbowIkCtrl , wristIkCtrl , wristIkGmbl ) :
            core.setLockHide( ctrl , 'sx' , 'sy' , 'sz' , 'v' )
        
        core.setVal( '%s.localWorld' %wristIkCtrl , 1 )
        
        
    ## Fk Ik Blending ##
    if armFk == True and armIk == True :
        core.addBlendFkIk( armCtrl , upArmFkJnt , upArmIkJnt , upArmJnt )
        core.addBlendFkIk( armCtrl , foreArmFkJnt , foreArmIkJnt , foreArmJnt )
        core.addBlendFkIk( armCtrl , wristFkJnt , wristIkJnt , wristJnt )
        
        if mc.objExists( 'arm%sFkIk_%s_rev' %( elem , side )) :
            revFkIk = 'arm%sFkIk_%s_rev' %( elem , side )
            core.connectAttr( '%s.fkIk' %armCtrl , '%s.v' %armIkGrp )
            core.connectAttr( '%s.ox' %revFkIk , '%s.v' %armFkGrp )
            
    elif armFk == True and armIk == False :
        core.parCons( 0 , upArmFkJnt , upArmJnt )
        core.parCons( 0 , foreArmFkJnt , foreArmJnt )
        core.parCons( 0 , wristFkJnt , wristJnt )
        
    elif armFk == False and armIk == True :
        core.parCons( 0 , upArmIkJnt , upArmJnt )
        core.parCons( 0 , foreArmIkJnt , foreArmJnt )
        core.parCons( 0 , wristIkJnt , wristJnt )
        
        
    ## Ribbon ##
    if ribbon == True :
        
        if side == 'rgt' :
            rbnAxis = 'x-'
            rbnAim = ( 1,0,0 )
            rbnUp = ( 0,0,-1 )
            rootTwsAmp = -1
            endTwsAmp = 1
        
        else :
            rbnAxis = 'x+'
            rbnAim = ( -1,0,0 )
            rbnUp = ( 0,0,1 )
            rootTwsAmp = -1
            endTwsAmp = 1
        
        rbnUpArmPar   = core.getParent( upArmJnt )
        rbnForeArmPar = core.getParent( foreArmJnt )
        rbnWristPar   = core.getParent( wristJnt )
        
        #-- Group
        armRbnGrp = core.transform( 'arm%sRbn_%s_grp' %( elem , side ))
        
        #-- Controls
        armRbnCtrl = core.addCtrl( 'arm%sRbn_%s_ctrl' %( elem , side ) , 'locator' , 'yellow' , jnt = True )
        armRbnZro = core.addGrp( armRbnCtrl )
        core.snapPoint( foreArmJnt , armRbnZro )
        core.snapJntOrient( foreArmJnt , armRbnCtrl )
        mc.parent( armRbnZro , armRbnGrp )
        
        #-- Shape
        core.scaleShape( armRbnCtrl , charSize )
        
        #-- Rig process
        core.parCons( 1 , foreArmJnt , armRbnZro )
        
        rbnUpArm = rbn.rigRibbon( 'upArm%s' %elem ,ax = rbnAxis , jntRoot = upArmJnt , jntEnd = foreArmJnt , animGrp = armRbnGrp , skinGrp = skinGrp , stillGrp = stillGrp , charSize = charSize )
        rbnForeArm = rbn.rigRibbon( 'foreArm%s' %elem , ax = rbnAxis , jntRoot = foreArmJnt , jntEnd = wristJnt , animGrp = armRbnGrp , skinGrp = skinGrp , stillGrp = stillGrp , charSize = charSize )

        rbnUpArmGrp = rbnUpArm['rbnGrp']
        rbnUpArmCtrl = rbnUpArm['rbnCtrl']
        rbnUpArmRootCtrl = rbnUpArm['rbnRootCtrl']
        rbnUpArmEndCtrl = rbnUpArm['rbnEndCtrl']
        rbnUpArmRootTwZro = rbnUpArm['rbnRootTwZro']
        rbnUpArmEndTwZro = rbnUpArm['rbnEndTwZro']
        rbnUpArmRootTwGrp = rbnUpArm['rbnRootTwGrp']
        rbnUpArmEndTwGrp = rbnUpArm['rbnEndTwGrp']
        rbnUpArmCtrlShape = rbnUpArm['rbnCtrlShape']
        
        rbnForeArmGrp = rbnForeArm['rbnGrp']
        rbnForeArmCtrl = rbnForeArm['rbnCtrl']
        rbnForeArmRootCtrl = rbnForeArm['rbnRootCtrl']
        rbnForeArmEndCtrl = rbnForeArm['rbnEndCtrl']
        rbnForeArmRootTwZro = rbnForeArm['rbnRootTwZro']
        rbnForeArmEndTwZro = rbnForeArm['rbnEndTwZro']
        rbnForeArmRootTwGrp = rbnForeArm['rbnRootTwGrp']
        rbnForeArmEndTwGrp = rbnForeArm['rbnEndTwGrp']
        rbnForeArmCtrlShape = rbnForeArm['rbnCtrlShape']
        
        mc.setAttr( '%s.rootTwistAmp' %rbnUpArmCtrlShape , rootTwsAmp )
        mc.setAttr( '%s.endTwistAmp' %rbnUpArmCtrlShape , endTwsAmp )
        
        mc.setAttr( '%s.rootTwistAmp' %rbnForeArmCtrlShape , rootTwsAmp )
        mc.setAttr( '%s.endTwistAmp' %rbnForeArmCtrlShape , endTwsAmp )
        
        for each in ( rbnUpArmRootTwZro , rbnUpArmRootTwGrp , rbnUpArmEndTwZro , rbnUpArmEndTwGrp , rbnForeArmRootTwZro , rbnForeArmRootTwGrp , rbnForeArmEndTwZro , rbnForeArmEndTwGrp ) :
            core.setRotateOrder( each , 'yxz' )
        
        core.snapPoint( upArmJnt , rbnUpArmGrp )
        mc.delete( mc.aimConstraint( foreArmJnt , rbnUpArmGrp , aim = rbnAim , u = rbnUp , wut = 'objectrotation' , wuo = foreArmTmpJnt , wu = ( 0,0,1 ) ))
        core.snap( upArmJnt , rbnUpArmRootTwZro )
        core.snap( foreArmJnt , rbnUpArmEndTwZro )
        core.parCons( 1 , upArmJnt , rbnUpArmGrp )
        core.pntCons( 0 , upArmJnt , rbnUpArmRootCtrl )
        core.pntCons( 0 , armRbnCtrl , rbnUpArmEndCtrl )
        core.parCons( 1 , rbnUpArmPar , rbnUpArmRootTwZro )
        core.parCons( 1 , rbnForeArmPar , rbnUpArmEndTwZro )
        core.parCons( 0 , upArmJnt , rbnUpArmRootTwGrp )
        core.parCons( 0 , foreArmJnt , rbnUpArmEndTwGrp )
        
        core.snapPoint( foreArmJnt , rbnForeArmGrp )
        mc.delete( mc.aimConstraint( wristJnt , rbnForeArmGrp , aim = rbnAim , u = rbnUp , wut = 'objectrotation' , wuo = wristTmpJnt , wu = ( 0,0,1 ) ))
        core.snap( foreArmJnt , rbnForeArmRootTwZro )
        core.snap( wristJnt , rbnForeArmEndTwZro )
        core.parCons( 1 , foreArmJnt , rbnForeArmGrp )
        core.pntCons( 0 , armRbnCtrl , rbnForeArmRootCtrl )
        core.pntCons( 0 , wristJnt , rbnForeArmEndCtrl )
        core.parCons( 1 , rbnWristPar , rbnForeArmEndTwZro )
        core.parCons( 0 , wristJnt , rbnForeArmEndTwGrp )

        #-- Fix ribbon
        core.connectAttr( '%s.ry' %rbnUpArmRootTwGrp , 'upArm%sRbnAmpTwist_%s_mdv.i1x' %( elem , side ))
        core.connectAttr( '%s.ry' %rbnUpArmEndTwGrp , 'upArm%sRbnAmpTwist_%s_mdv.i1y' %( elem , side ))
        
        core.connectAttr( '%s.ry' %rbnForeArmRootTwGrp , 'foreArm%sRbnAmpTwist_%s_mdv.i1x' %( elem , side ))
        core.connectAttr( '%s.ry' %rbnForeArmEndTwGrp , 'foreArm%sRbnAmpTwist_%s_mdv.i1y' %( elem , side ))
        
        #-- Hierarchy
        core.parent( armRbnGrp , armRigGrp , animGrp)
        
        #-- Cleanup
        for each in ( armRbnGrp , armRbnZro , rbnUpArmGrp , rbnForeArmGrp , rbnUpArmRootCtrl , rbnUpArmEndCtrl , rbnForeArmRootCtrl , rbnForeArmEndCtrl , rbnUpArmRootTwZro , rbnUpArmEndTwZro , rbnUpArmRootTwGrp , rbnUpArmEndTwGrp , rbnForeArmRootTwZro , rbnForeArmEndTwZro , rbnForeArmRootTwGrp , rbnForeArmEndTwGrp ) :
            core.setLock( each , True , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
        
        for each in ( rbnUpArmRootCtrl , rbnUpArmEndCtrl , rbnForeArmRootCtrl , rbnForeArmEndCtrl ) :
            core.setVal( '%s.v' %each , 0 )
        
        for each in ( rbnUpArmCtrl , rbnForeArmCtrl ) :
            core.setVal( '%s.autoTwist' %each , 1 )
        
        core.setLockHide( armRbnCtrl , 'sx' , 'sy' , 'sz' , 'v' )
        
        mc.select( cl = True )