import maya.cmds as mc
import maya.mel as mm
import ppmel.core as core
reload( core )


def rigSpine( spine1TmpJnt = 'spine1_tmpJnt' ,
              spine2TmpJnt = 'spine2_tmpJnt' ,
              spine3TmpJnt = 'spine3_tmpJnt' ,
              parent = 'root_jnt' ,
              animGrp = 'anim_grp' ,
              skinGrp = 'skin_grp' ,
              jntGrp = 'jnt_grp' ,
              stillGrp = 'still_grp' ,
              spineFk = True ,
              spineIk = False ,
              rotateOrder = 'yzx' ,
              axis = 'y' ,
              charSize = 1 ) :
    
    ## Main Rig ##
    #-- Group
    spineRigGrp = core.transform( 'spineRig_grp' )
    spineJntGrp = core.transform( 'spineJnt_grp' )
    core.parCons( 0 , parent , spineRigGrp )
    core.snap( parent , spineJntGrp )
    
    #-- Joint
    spine1PosJnt = core.addJnt( 'spine1Pos_jnt' , spine1TmpJnt )
    spine2PosJnt = core.addJnt( 'spine2Pos_jnt' , spine2TmpJnt )
    spine3PosJnt = core.addJnt( 'spine3Pos_jnt' , spine3TmpJnt )
    
    spine1Jnt = core.addJnt( 'spine1_jnt' , spine1TmpJnt )
    spine2Jnt = core.addJnt( 'spine2_jnt' , spine2TmpJnt )
    spine3Jnt = core.addJnt( 'spine3_jnt' , spine3TmpJnt )
    
    core.parent( spine3PosJnt , spine2PosJnt , spine1PosJnt )
    core.parent( spine3Jnt , spine3PosJnt )
    core.parent( spine2Jnt , spine2PosJnt )
    core.parent( spine1Jnt , spine1PosJnt )
    
    #-- Rotate order
    for each in ( spine1PosJnt , spine2PosJnt , spine3PosJnt , spine1Jnt , spine2Jnt , spine3Jnt ) :
        core.setRotateOrder( each , rotateOrder )
    
    #-- Hierarchy
    core.parent( spineRigGrp , animGrp )
    core.parent( spineJntGrp , jntGrp )
    core.parent( spine1PosJnt , parent )
    
    #-- Cleanup
    for grp in ( spineRigGrp , spineJntGrp ) :
        core.setLock( grp , True , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
    
    
    ## Fk Rig ##
    if spineFk == True :
        #-- Group
        spineFkRigGrp = core.transform( 'spineFkRig_grp' )
        spineFkJntGrp = core.transform( 'spineFkJnt_grp' )
        spineFkStillGrp = core.transform( 'spineFkStill_grp' )
        core.snap( parent , spineFkRigGrp )
        core.snap( parent , spineFkJntGrp )
        core.snap( parent , spineFkStillGrp )
        
        #-- Joint
        spine1FkPosJnt = core.addJnt( 'spine1FkPos_jnt' , spine1TmpJnt )
        spine2FkPosJnt = core.addJnt( 'spine2FkPos_jnt' , spine2TmpJnt )
        spine3FkPosJnt = core.addJnt( 'spine3FkPos_jnt' , spine3TmpJnt )
        
        spine1FkJnt = core.addJnt( 'spine1Fk_jnt' , spine1TmpJnt )
        spine2FkJnt = core.addJnt( 'spine2Fk_jnt' , spine2TmpJnt )
        spine3FkJnt = core.addJnt( 'spine3Fk_jnt' , spine3TmpJnt )
        
        core.parent( spine3FkPosJnt , spine2FkPosJnt , spine1FkPosJnt )
        core.parent( spine3FkJnt , spine3FkPosJnt )
        core.parent( spine2FkJnt , spine2FkPosJnt )
        core.parent( spine1FkJnt , spine1FkPosJnt )
        
        #-- Controls
        spine1FkCtrl = core.addCtrl( 'spine1Fk_ctrl' , 'circle' , 'red' , jnt = True )
        spine1FkGmbl = core.addGimbal( spine1FkCtrl )
        spine1FkZro = core.addGrp( spine1FkCtrl )
        spine1FkOfst = core.addGrp( spine1FkCtrl , 'Ofst' )
        core.snapPoint( spine1FkJnt , spine1FkZro )
        core.snapJntOrient( spine1FkJnt , spine1FkCtrl )
        
        spine2FkCtrl = core.addCtrl( 'spine2Fk_ctrl' , 'circle' , 'red' , jnt = True )
        spine2FkGmbl = core.addGimbal( spine2FkCtrl )
        spine2FkZro = core.addGrp( spine2FkCtrl )
        spine2FkOfst = core.addGrp( spine2FkCtrl , 'Ofst' )
        core.snapPoint( spine2FkJnt , spine2FkZro )
        core.snapJntOrient( spine2FkJnt , spine2FkCtrl )
        
        spine3FkCtrl = core.addCtrl( 'spine3Fk_ctrl' , 'circle' , 'red' , jnt = True )
        spine3FkGmbl = core.addGimbal( spine3FkCtrl )
        spine3FkZro = core.addGrp( spine3FkCtrl )
        spine3FkOfst = core.addGrp( spine3FkCtrl , 'Ofst' )
        core.snapPoint( spine3FkJnt , spine3FkZro )
        core.snapJntOrient( spine3FkJnt , spine3FkCtrl )
        
        #-- Shape
        for ctrl in ( spine1FkCtrl , spine1FkGmbl , spine2FkCtrl , spine2FkGmbl , spine3FkCtrl , spine3FkGmbl ) :
            core.scaleShape( ctrl , charSize * 1.8 )
        
        #-- Rotate order
        for each in ( spine1FkCtrl , spine2FkCtrl , spine3FkCtrl , spine1FkPosJnt , spine2FkPosJnt , spine3FkPosJnt , spine1FkJnt , spine2FkJnt , spine3FkJnt  ) :
            core.setRotateOrder( each , rotateOrder )
        
        #-- Rig process
        core.parCons( 0 , spine3FkGmbl , spine3FkPosJnt )
        
        core.sclCons( 0 , spine1FkGmbl , spine1FkJnt )
        core.sclCons( 0 , spine2FkGmbl , spine2FkJnt )
        core.sclCons( 0 , spine3FkGmbl , spine3FkJnt )
        
        core.parCons( 0 , spine1FkPosJnt , spine1PosJnt )
        core.parCons( 0 , spine2FkPosJnt , spine2PosJnt )
        core.parCons( 0 , spine3FkPosJnt , spine3PosJnt )
        
        core.sclCons( 0 , spine1FkJnt , spine1Jnt )
        core.sclCons( 0 , spine2FkJnt , spine2Jnt )
        core.sclCons( 0 , spine3FkJnt , spine3Jnt )
        
        core.addFkStretch( spine1FkCtrl , spine2FkOfst , axis )
        core.addFkStretch( spine2FkCtrl , spine3FkOfst , axis )
        
        core.addLocalWorld( spine1FkCtrl , 'anim_grp' , spineFkRigGrp , spine1FkZro , 'orient' )
            
        #-- Hierarchy
        core.parent( spine2FkZro , spine1FkGmbl )
        core.parent( spine3FkZro , spine2FkGmbl )
        
        core.parent( spine1FkZro , spineFkRigGrp , spineRigGrp )
        core.parent( spine1FkPosJnt , spineFkJntGrp , spineJntGrp )
        core.parent( spineFkStillGrp , stillGrp )
        
        # Ribbon #
        #-- Group
        spineRbnFkRigGrp = core.transform( 'spineRbnFkRig_grp' )
        spineRbnFkJntGrp = core.transform( 'spineRbnFkJnt_grp' )
        spineRbnFkStillGrp = core.transform( 'spineRbnFkStill_grp' )
        core.snap( parent , spineRbnFkRigGrp )
        core.snap( parent , spineRbnFkJntGrp )
        
        #-- Create nurbs surface
        loftFk1 = core.createCurve( 'loftFk1_cuv' , 'line' )
        loftFk2 = core.createCurve( 'loftFk2_cuv' , 'line' )
        loftFk3 = core.createCurve( 'loftFk3_cuv' , 'line' )
        
        core.snap( spine1TmpJnt , loftFk1 )
        core.snap( spine2TmpJnt , loftFk2 )
        core.snap( spine3TmpJnt , loftFk3 )
        
        #-- Shape
        for ctrl in ( loftFk1 , loftFk2 , loftFk3 ) :
            core.scaleShape( ctrl , charSize * 1.8 )
            
        spineFkNrb = mc.loft( loftFk1 , loftFk2 , loftFk3 , ch = True , rn = True , ar = True , n = 'spineFk_nrb' )[0]
        mc.delete( spineFkNrb , ch = True )
        mc.delete( loftFk1 , loftFk2 , loftFk3 )
        
        #-- Joint
        spineRootFkJnt = core.addJnt( 'spineRootFk_jnt' , spine1TmpJnt )
        spineMidFkJnt = core.addJnt( 'spineMidFk_jnt' , spine2TmpJnt )
        spineEndFkJnt = core.addJnt( 'spineEndFk_jnt' , spine3TmpJnt )
        
        core.parCons( 0 , spine3FkGmbl , spineEndFkJnt )
        core.parCons( 0 , spine1FkGmbl , spineRootFkJnt )
        
        #-- Bind skin
        spineFkSkc = mc.skinCluster( spineRootFkJnt , spineMidFkJnt , spineEndFkJnt , spineFkNrb , dr = 7 , mi = 2 , n = 'spineFk_skc' )[0]
        
        mc.skinPercent( spineFkSkc , '%s.cv[4][0:1]' %spineFkNrb , tv = [ spineEndFkJnt , 1 ] )
        mc.skinPercent( spineFkSkc , '%s.cv[3][0:1]' %spineFkNrb , tv = [ spineMidFkJnt , 0.5 ] )
        mc.skinPercent( spineFkSkc , '%s.cv[2][0:1]' %spineFkNrb , tv = [ spineMidFkJnt , 1 ] )
        mc.skinPercent( spineFkSkc , '%s.cv[1][0:1]' %spineFkNrb , tv = [ spineMidFkJnt , 0.5 ] )
        mc.skinPercent( spineFkSkc , '%s.cv[0][0:1]' %spineFkNrb , tv = [ spineRootFkJnt , 1 ] )
        
        deformerSet = core.getDef( spineFkNrb , 'tweak' )
        mc.rename( deformerSet[0] , 'spineFk_tweak' )
        mc.rename( deformerSet[1] , 'spineFk_tweakSet' )
        mc.delete ( 'bindPose*' )
        
        #-- Controls
        spineRbnFkCtrl = core.addCtrl( 'spineRbnFk_ctrl' , 'square' , 'yellow' , jnt = True )
        spineRbnFkGmbl = core.addGimbal( spineRbnFkCtrl )
        spineRbnFkZro = core.addGrp( spineRbnFkCtrl )
        spineRbnFkOfst = core.addGrp( spineRbnFkCtrl , 'Ofst' )
        core.snapPoint( spineMidFkJnt , spineRbnFkZro )
        core.snapJntOrient( spineMidFkJnt , spineRbnFkCtrl )
        
        #-- Shape
        for ctrl in ( spineRbnFkCtrl , spineRbnFkGmbl ) :
            core.scaleShape( ctrl , charSize * 1.8 )
        
        #-- Rig process
        core.parCons( 1 , spine2FkGmbl , spineRbnFkOfst )
        core.parCons( 1 , spineRbnFkGmbl , spineMidFkJnt )
        core.parCons( 1 , spineRootFkJnt , spineEndFkJnt , spineRbnFkZro )
        
        #-- Create pos group
        spineFkPosGrps = []
        spineFkPosis = []
        spineFkAims = []
        spineFkDtlGrps = []
        
        for i in range(1,3) :
            # PosGrp
            spineFkPosGrp = core.transform( 'spine%sFkDtlPos_grp' %i )
            posi = core.pointOnSurfaceInfo( 'spine%sFkDtlPos_posi' %i )
            
            mc.connectAttr( '%s.worldSpace[0]' %spineFkNrb , '%s.inputSurface' %posi )
            mc.connectAttr ( '%s.position' %posi , '%s.t' %spineFkPosGrp )
            
            mc.setAttr( '%s.turnOnPercentage' %posi , 1 )
            mc.setAttr ( '%s.parameterU' %posi , 0.5 )
            
            # AimCons
            spineFkAim = mc.createNode( 'aimConstraint' , n = 'spine%sFkDtl_aimCons' %i )
            
            mc.connectAttr( '%s.n' %posi , '%s.wu' %spineFkAim )
            mc.connectAttr( '%s.tv' %posi , '%s.tg[0].tt' %spineFkAim )
            mc.connectAttr( '%s.cr' %spineFkAim , '%s.r' %spineFkPosGrp )
            
            mc.setAttr( '%s.a' %spineFkAim , -1 , 0 , 0 )
            mc.setAttr( '%s.u' %spineFkAim , 0 , 0 , 1 )
            
            mc.parent( spineFkAim , spineFkPosGrp )
            
            # Detail group
            spineFkDtlGrp = core.transform( 'spine%sFkDtl_grp' %i )
            core.snapPoint( 'spine%sFk_jnt' %i , spineFkDtlGrp )
            
            core.parCons( 1 , spineFkDtlGrp , 'spine%sFkPos_jnt' %i )
            
            spineFkPosGrps.append( spineFkPosGrp )
            spineFkPosis.append( posi )
            spineFkAims.append( spineFkAim )
            spineFkDtlGrps.append( spineFkDtlGrp )
        
        mc.setAttr ( '%s.parameterV' %spineFkPosis[0] , 0.5 )
        mc.setAttr ( '%s.parameterV' %spineFkPosis[1] , 0.5 )
        mc.setAttr ( '%s.parameterU' %spineFkPosis[0] , 0 )
        mc.setAttr ( '%s.parameterU' %spineFkPosis[1] , 0.5 )
        
        core.parCons( 1 , spineFkPosGrps[0] , spineFkDtlGrps[0] )
        core.parCons( 1 , spineFkPosGrps[1] , spineFkDtlGrps[1] )
        
        #-- Hierarchy
        core.parent( spineRbnFkZro , spineRbnFkRigGrp , spineFkRigGrp )
        core.parent( spineFkDtlGrps , spineRbnFkZro )
        core.parent( spineRbnFkJntGrp , spineFkJntGrp )
        core.parent( spineRbnFkStillGrp , spineFkStillGrp )
        mc.parent( spineFkNrb , spineFkPosGrps , spineRbnFkStillGrp )
        mc.parent( spineRootFkJnt , spineMidFkJnt , spineEndFkJnt , spineFkJntGrp )
        
        #-- Cleanup
        for grp in ( spineFkRigGrp , spineFkJntGrp , spine1FkZro , spine2FkZro , spine3FkZro , spine1FkOfst , spine2FkOfst , spine3FkOfst ) :
            core.setLock( grp , True , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
        
        for gmbl in ( spine1FkGmbl , spine2FkGmbl , spine3FkGmbl ) :
            core.setLockHide( gmbl , 'sx' , 'sy' , 'sz' , 'v' )
        
        for ctrl in ( spine1FkCtrl , spine2FkCtrl , spine3FkCtrl ) :
            core.setLockHide( ctrl , 'v' )
        
    mc.select( cl = True )