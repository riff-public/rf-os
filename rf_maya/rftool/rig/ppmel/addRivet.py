# --------------------------------------------------------------------------------------------
#
# Add Rivet
#
# description :  - select two edge
#                - run script
#
# --------------------------------------------------------------------------------------------

import maya.cmds as mc
import maya.mel as mm
import ppmel.core as core
import re
reload( core )


def addRivet( name = '' , side = '' ) :
    sel = mc.ls( sl = True )
    
    if side == '' :
        side = '_'
    else :
        side = '_%s_' %side
    
    if sel :
        obj = sel[0].split('.')[0]
        type = core.getNodeType( sel[0] )
        
        if type == 'mesh' :
            edge = mc.filterExpand( sm = 32 )
            
            u = 0.5
            v = 0.5
            edgeNum = []
            for each in edge :
                e = re.findall(r'\d+', '%s\n' %each)[-1]
                edgeNum.append(e)
                
            cfme1 = core.curveFromMeshEdge( '%s1Rivet%scfme' %( name , side ))
            core.setVal( '%s.ihi' %cfme1 , 1 )
            core.setVal( '%s.ei[0]' %cfme1 , int(edgeNum[0]))
            
            cfme2 = core.curveFromMeshEdge( '%s2Rivet%scfme' %( name , side ))
            core.setVal( '%s.ihi' %cfme1 , 1 )
            core.setVal( '%s.ei[0]' %cfme2 , int(edgeNum[1]))
            
            loft = core.loft( '%sRivet%sloft' %( name , side ))
            core.setVal( '%s.u' %loft , True )
            core.setVal( '%s.rsn' %loft , True )
            
            posi = core.pointOnSurfaceInfo( '%sRivet%sposi' %( name , side ))
            core.setVal( '%s.turnOnPercentage' %posi , 1 )
            
            mc.connectAttr( '%s.w' %obj , '%s.im' %cfme1 )
            mc.connectAttr( '%s.w' %obj , '%s.im' %cfme2 )
            mc.connectAttr( '%s.oc' %cfme1 , '%s.ic[0]' %loft )
            mc.connectAttr( '%s.oc' %cfme2 , '%s.ic[1]' %loft )
            mc.connectAttr( '%s.os' %loft , '%s.is' %posi )
                
        elif type == 'nurbsSurface' :
            point = mc.filterExpand( sm = 41 )[0]
            
            void = point.split('.uv')[-1]
            u , v = void.split('][')
            u = u.split('[')[-1]
            v = v.split(']')[0]
            
            posi = core.pointOnSurfaceInfo( '%sRivet%sposi' %( name , side ))
            core.setVal( '%s.turnOnPercentage' %posi , 0 )
            core.setVal( '%s.parameterU' %posi , float(u) )
            core.setVal( '%s.parameterV' %posi , float(v) )
            
            mc.connectAttr( '%s.ws' %obj , '%s.is' %posi )
                    
        else :
            mc.error( "No edges or point selected" )
        
    grp = core.transform( '%sRivetZro%sgrp' %( name , side ))
    loc = mc.spaceLocator( n = '%sRivet%sloc' %( name , side ))[0]
    
    for attr in ( 'parameterU' , 'parameterV' , 'offsetX' , 'offsetY' , 'offsetZ' , 'scaleLocator' ) :
        core.addAttr( loc , attr )
        
    core.setVal( '%s.parameterU' %loc , float(u))
    core.setVal( '%s.parameterV' %loc , float(v))
    core.setVal( '%s.scaleLocator' %loc , 1 )
    
    aimCons = mc.createNode( 'aimConstraint' , n = '%sRivet%saimCons' %( name , side ))
    core.setVal( '%s.ax' %aimCons , 0 )
    core.setVal( '%s.ay' %aimCons , 1 )
    core.setVal( '%s.az' %aimCons , 0 )
    core.setVal( '%s.ux' %aimCons , 0 )
    core.setVal( '%s.uy' %aimCons , 0 )
    core.setVal( '%s.uz' %aimCons , 1 )
    
    mc.connectAttr( '%s.position' %posi , '%s.translate' %loc )
    mc.connectAttr( '%s.n' %posi , '%s.tg[0].tt' %aimCons )
    mc.connectAttr( '%s.tv' %posi , '%s.wu' %aimCons )
    mc.connectAttr( '%s.crx' %aimCons , '%s.rx' %loc )
    mc.connectAttr( '%s.cry' %aimCons , '%s.ry' %loc )
    mc.connectAttr( '%s.crz' %aimCons , '%s.rz' %loc )
    
    mc.connectAttr( '%s.parameterU' %loc , '%s.parameterU' %posi )
    mc.connectAttr( '%s.parameterV' %loc , '%s.parameterV' %posi )
    mc.connectAttr( '%s.offsetX' %loc , '%s.offsetX' %aimCons )
    mc.connectAttr( '%s.offsetY' %loc , '%s.offsetY' %aimCons )
    mc.connectAttr( '%s.offsetZ' %loc , '%s.offsetZ' %aimCons )
    
    mc.connectAttr( '%s.scaleLocator' %loc , '%sShape.localScaleX' %loc )
    mc.connectAttr( '%s.scaleLocator' %loc , '%sShape.localScaleY' %loc )
    mc.connectAttr( '%s.scaleLocator' %loc , '%sShape.localScaleZ' %loc )
    
    core.parent( aimCons , loc , grp )
    
    for each in ( grp , loc , aimCons ) :
        core.setLockHide( each , 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
        
    for attr in ( 'offsetX' , 'offsetY' , 'offsetZ' ) :
        core.setLockHide( aimCons , attr )
    
    for attr in ( 'localPositionX' , 'localPositionY' , 'localPositionZ' , 'localScaleX' , 'localScaleY' , 'localScaleZ' ) :
        core.setLockHide( '%sShape' %loc , attr )
        
    mc.select( cl = True )