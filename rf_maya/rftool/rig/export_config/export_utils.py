import maya.cmds as mc
import os
import yaml
import sys
from rf_utils.context import context_info

modulePath = sys.modules[__name__].__file__
moduleDir  = os.path.dirname(modulePath).replace('\\', '/')
configLoc = moduleDir+'/config'
allModuleLoc = moduleDir+'/export_module'


def importAndRun(module =[]):
    for each in module:
        exec('import {}'.format(each))
        exec('reload({})'.format(each))
        exec('{}.run()'.format(each))        

def export_rig(project,resolution,process):
    
    #all module we have 
    sys.path.append(r'{}'.format(allModuleLoc))
    module = os.listdir(allModuleLoc)
    
    #clean pyc out
    pyModule = []
    for each in module:
        if not '.pyc' in each:
            pyModule.append(each)
        
    #open config yaml
    print pyModule
    if project + '.yml' not in os.listdir(configLoc):
        project = 'default'
        
    with open(r'{}/{}.yml'.format(configLoc , project) ,'r') as config:
        config_data = yaml.load(config)    

    #check process is in keys or not 
    allKeys = config_data['{}'.format(resolution)].keys()
    if not process in allKeys:
        process = 'default'


    #read out from config , what modules we have to use
    config_module = config_data['{}'.format(resolution)]['{}'.format(process)]

    #compare wth all pyModule
    useModule = []
    alien = []
    if config_module:
        for each in config_module:
            fullname = each+'.py'
            if fullname in pyModule:
                useModule.append(each)
            else:
                alien.append(each)


        importAndRun(module=useModule)

    if not alien == []:
        for each in alien:
            print 'No {} In Export_Module'.format(alien)

    return useModule




