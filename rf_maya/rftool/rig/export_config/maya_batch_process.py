import os
import sys

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

core = '%s/core' % (os.environ.get('RFSCRIPT'))
if not core in sys.path: 
	sys.path.append(core)
import rf_config as config
import maya.standalone
maya.standalone.initialize( name='python' )
import maya.cmds as mc
from rf_maya import lib
from rf_maya.rftool.rig.export_config import export_utils
reload(export_utils)

def main() :
	# init maya standalone
	# set arguments
	for i in os.environ['MAYA_PLUG_IN_PATH'].split(';'):
		print i	
	for i in mc.pluginInfo( query=True, listPlugins=True ):
		print i
	mc.loadPlugin('redshift4maya')



	src = sys.argv[1]
	dst = sys.argv[2]
	processes = sys.argv[3]
	output = sys.argv[4]
	export = sys.argv[5]
	project = sys.argv[6]
	res = sys.argv[7]
	task = sys.argv[8]

	print src
	print 'processes\n\n', processes
	
	print 'create Folder'
	if not os.path.exists(os.path.dirname(dst)):
		os.makedirs(os.path.dirname(dst))
	print 'create finish'
	print 'mtoa load:',mc.pluginInfo('mtoa',q=1,l=1)
	print 'redshift load:',mc.pluginInfo('redshift4maya',q=1,l=1)

	#mc.loadPlugin('redshift4maya') 
	print 'mtoa load:',mc.pluginInfo('mtoa',q=1,l=1)
	print 'redshift load:',mc.pluginInfo('redshift4maya',q=1,l=1)
	
	# open scene
	print 'open scene'
	x = mc.file(src, o=True, f=True, prompt=False)
	print 'open success'
	# run process
	# for process in eval(processes):
	#     lib.run(process)
	print x
	name, ext = os.path.splitext(dst)
	print name
	print ext
	type = 'mayaAscii' if ext == '.ma' else 'mayaBinary' if ext == '.mb' else 'mayaAscii'
	print type
	print 'start cleanup'
	export_utils.export_rig(project,res,task)
	print  'finish cleanup'

	
	if output == 'save':
		print 'strat save'
		mc.file(rename=dst)
		mc.file(save=True, f=True, type=type)

	elif output == 'export':
		print 'strat export'
		export_content = mc.listRelatives(export,c=1)
		mc.parent(export_content, w=1)
		mc.select(export_content)
		#mc.select(mc.listRelatives('Export_Grp'))
		mc.file(dst, force=True, typ= 'mayaAscii', pr=True, es=True)
		mc.file(dst, force=True, typ= 'mayaBinary', pr=True, es=True)
		#return  mc.file(r'C:\Users\pornpavis.t\Desktop\testExport.ma' ,force=True, options='v=0', typ=type, pr=True, es=True) 
	print 'finish'

	return dst

if __name__ == '__main__':
	main()