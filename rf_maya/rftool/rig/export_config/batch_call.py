import sys, os
import subprocess
import maya.cmds as mc
from rf_utils import publish_info
from rf_utils.context import context_info
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
version = mc.about(v = True)

if version in ['2015', '2016', '2017','2018']:
	MAYA_PYTHON = 'C:/Program Files/Autodesk/Maya%s/bin/mayabatch.exe' % version
	MAYA_PYTHON = 'C:\\Program Files\\Autodesk\\Maya%s\\bin\\mayapy.exe' % version

MAYACMD = '%s/maya_batch_process.py' % moduleDir

def run(src, dst, processList, output='save', export='',project=''  , res='' , task =''):
	print 'Running subprocess'
	# subprocess.call([MAYA_PYTHON, MAYACMD, src, dst, processList, output])
	# return
	print '##############'
	print src
	print dst
	print processList
	print output
	print export
	print project
	print res
	print task
	print '##############'
	publish_context = context_info.ContextPathInfo(path = src)
	env = publish_context.projectInfo.get_project_env(dcc='maya', dccVersion=mc.about(v=True), department='Py')
	#env = asset.projectInfo.get_project_env(dcc='maya', dccVersion=mc.about(v=True), department='Py')
	print env
	# print type(env)
	# print dir(env)
	p = subprocess.Popen([MAYA_PYTHON, MAYACMD, src, dst, processList, output, export, project, res , task],
	 stdout=subprocess.PIPE, stderr=subprocess.STDOUT,env=env)
	stdout, stderr = p.communicate()
	print 'Finish subprocess\n\n'

	logger.info(stdout)
	# logger.error(stderr)
	print 'dist : {}'.format(dst)
	print os.path.exists(dst)
	if os.path.exists(dst):
		return dst