import os
import sys

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

core = '%s/core' % (os.environ.get('RFSCRIPT'))
if not core in sys.path: 
    sys.path.append(core)
import rf_config as config
import maya.standalone
maya.standalone.initialize( name='python' )
import maya.cmds as mc
from rf_maya import lib
from rf_maya.rftool.rig.export_config import export_utils
reload(export_utils)

def main() :
    # init maya standalone
    # set arguments

    src = sys.argv[1]
    dst = sys.argv[2]
    processes = sys.argv[3]
    output = sys.argv[4]
    export = sys.argv[5]
    project = sys.argv[6]
    res = sys.argv[7]
    task = sys.argv[8]

    print 'processes\n\n', processes

    if not os.path.exists(os.path.dirname(dst)):
        os.makedirs(os.path.dirname(dst))

    # open scene
    mc.file(src, o=True, f=True, prompt=False)

    # run process   # for process in eval(processes):
    #     lib.run(process)
 

    name, ext = os.path.splitext(dst)
    type = 'mayaAscii' if ext == '.ma' else 'mayaBinary' if ext == '.mb' else 'mayaAscii'

    export_utils.export_rig(project,res,'ue')

    if output == 'save':
        mc.file(rename=dst)
        mc.file(save=True, f=True, type=type)
    elif output == 'export':
        print export
        export_content = mc.listRelatives(export,c=1)
        mc.parent(export_content,w=1)
        mc.delete(export)
        mc.select(export_content)
        #mc.file( dst,force=True , options="v=0;" ,typ="FBX export" ,pr=True , es=True)
        #if not mc.pluginInfo('fbxmaya.mll',q=1,loaded =1):
        mc.loadPlugin('fbxmaya.mll')
        #mc.select(mc.listRelatives('Export_Grp'))    
        mc.file(dst ,force=True , options="v=0;" ,typ="FBX export" ,pr=True , es=True) 
    return dst

if __name__ == '__main__':
    main()