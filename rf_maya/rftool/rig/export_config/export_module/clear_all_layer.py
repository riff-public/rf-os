import maya.cmds as mc

def run() :
    layers = mc.ls( type = 'displayLayer' )
    if not mc.objExists (layers == []):
        for layer in layers :
            if not layer == 'defaultLayer' :
                attrs = ( 'identification' , 'drawInfo' )
                for attr in attrs :
                    objs = mc.listConnections( '{}.{}'.format(layer , attr) , p = True )
                    if objs :
                        for obj in objs :
                            if 'layerManager' in obj :
                                mc.disconnectAttr( obj , '{}.{}'.format(layer , attr) )
                            else :
                                mc.disconnectAttr( '{}.{}'.format(layer , attr) , obj )                               
                mc.delete( layer )  