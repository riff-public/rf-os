import maya.cmds as mc
import maya.mel as mm 

# def run():
#     exceptions = ( 'lambert1' , 'particleCloud1' )
#     mat = mc.ls( mat = True )
    
#     # for each in mat :
#     #     if not each in exceptions :
#     #         mc.delete( each )
#     to_del = [shd for shd in mat if shd not in exceptions]
#     try:
#         mc.delete(to_del)
#     except Exception, e:
#         pass
        
#     for each in mc.ls( type = 'mesh' ):
#         mc.select( each , r = True )
#         cmd = 'hyperShade -assign lambert1;'
#         mm.eval(cmd)

def run():
    exceptions = ( 'lambert1' , 'particleCloud1' )
    mat = mc.ls( mat = True )

    to_del = [shd for shd in mat if shd not in exceptions]
    if to_del:
        sg_del = []
        for obj in to_del:
            sg = mc.listConnections("{}.oc".format(obj), d=True)
            if sg:
                sg_del.append(sg[0])

        mc.delete(to_del, sg_del)
        mesh = mc.ls(typ="mesh")
        # for num, obj in enumerate(mesh):
        #     mc.connectAttr("{}.iog[0]".format(obj), "initialShadingGroup.dsm[{}]".format(num), f=1)
        mc.sets(mesh, e=True, forceElement='initialShadingGroup')

if __name__ == "__main__":
    run()