import maya.cmds as mc

def run():
    try:
        cams = mc.ls(sl=True)
        camList = []
        for cam in cams:
            mc.camera(cam,e=True,startupCamera=False)
            mc.delete(cam)
            camList.append(cam)
    except: pass