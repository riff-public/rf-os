import maya.cmds as mc
import maya.mel as mm

def run(entity = None):
    unknownNodes = mc.ls(type='unknown')
    success = True
    for node in unknownNodes:
        # plugin = mc.unknownNode(node, q=True, plugin=True)
        # if plugin in PLUGINS_TO_REMOVE:
        if mc.objExists(node):

            mc.lockNode(node, l=False)
            try:
                mc.delete(node)
            except:
                success = False
    mm.eval('flushUndo;')

    unknownPlugins = mc.unknownPlugin(q=True, list=True)
    if unknownPlugins:
        for plugin in unknownPlugins:
            mc.unknownPlugin(plugin, remove=True)

    return entity	