import maya.cmds as mc
import maya.mel as mm
import pymel.core as pmc

def run(entity = None) :
    bindPoses = pmc.ls( type = 'dagPose' )

    for bindPose in bindPoses :
        try : mc.delete( bindPose.nodeName() )
        except : pass
    return entity