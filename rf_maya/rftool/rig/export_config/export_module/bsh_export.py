import os
from rf_utils import publish_info
from rf_utils.context import context_info

def run(entity = None):
    entity = publishInfo.entity.copy()
    entity.context.update(step='model', process='modelBsh', app='maya')
    processPath = entity.path.app().abs_path()
    if not os.path.exists(processPath): 
        os.makedirs(processPath) 
