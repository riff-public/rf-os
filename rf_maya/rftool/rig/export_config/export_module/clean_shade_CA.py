import maya.cmds as mc
import maya.mel as mm 
import json
from rftool.rig.rigScript import core 
reload(core)
cn = core.Node()

# def run():
#     exceptions = ( 'lambert1' , 'particleCloud1' )
#     mat = mc.ls( mat = True )
    
#     # for each in mat :
#     #     if not each in exceptions :
#     #         mc.delete( each )
#     to_del = [shd for shd in mat if shd not in exceptions]
#     try:
#         mc.delete(to_del)
#     except Exception, e:
#         pass
        
#     for each in mc.ls( type = 'mesh' ):
#         mc.select( each , r = True )
#         cmd = 'hyperShade -assign lambert1;'
#         mm.eval(cmd)

def run():
    exceptions = [ 'lambert1' , 'particleCloud1' ]
    mat = mc.ls( mat = True )
    fclCheck = 0
    try:
        uiShd = json.loads(mc.getAttr('TmpFclData.fclMat'))
        if uiShd:
            fclCheck = 1
            for shd in uiShd:
                exceptions.append(shd)
    except:
        print 'TmpFclData has error or not found'
    to_del = [shd for shd in mat if shd not in exceptions]
    if to_del:
        sg_del = []
        for obj in to_del:
            print obj
            sg = mc.listConnections("{}.oc".format(obj), d=True)
            if sg:
                sg_del.append(sg[0])
            else:
                continue
        mc.delete(to_del, sg_del)
        mesh = mc.listRelatives('Geo_Grp',type='mesh',ad=1)
        # for num, obj in enumerate(mesh):
        #     mc.connectAttr("{}.iog[0]".format(obj), "initialShadingGroup.dsm[{}]".format(num), f=1)
        ## check plane and head geo not assign lambert1

        projectName = cn.checkProject()
        
        if projectName in 'CA':
            meshh = []
            keep = []
            attrCheck = 'keepShade'
            for each in mesh:
                if mc.objExists('{}.{}'.format(each, attrCheck)):
                    if not mc.getAttr('{}.{}'.format(each, attrCheck)) or mc.getAttr('{}.intermediateObject'.format(each)):
                        meshh.append(each)
                    else:
                        keep.append(each)
                else:
                    meshh.append(each)
            mc.sets(meshh, e=True, rm='initialShadingGroup')
            #mc.sets(meshh, e=True, forceElement='initialShadingGroup')
        else:
            mc.sets(mesh, e=True, forceElement='initialShadingGroup')

    if fclCheck:
        mc.delete('TmpFclData')

if __name__ == "__main__":
    run()