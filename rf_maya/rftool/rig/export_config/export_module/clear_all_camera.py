import maya.cmds as mc

def run() :
    exception = [ 'frontShape' , 'sideShape' , 'topShape' , 'perspShape' ]
    cams = mc.ls( type = 'camera' )
    for cam in cams :
        if not cam in exception :
            mc.delete(mc.listRelatives( cam , p = True ))