import maya.cmds as mc

def run():
    try:
        sels = mc.ls('vraySettings')
        mc.lockNode(sels,lock=False)
        mc.delete(sels)
    except: 
        pass   