import maya.cmds as mc

def run():
	sel = mc.ls(typ="objectSet")
	exception = ["defaultLightSet", "defaultObjectSet", "initialParticleSE", "initialShadingGroup"]
	objLs = [i for i in sel if i not in exception]

	if objLs:
		mc.delete(objLs)