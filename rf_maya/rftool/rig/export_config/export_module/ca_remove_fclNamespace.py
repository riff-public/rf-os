import maya.cmds as mc
import maya.mel as mm 
import json
from rf_maya.lib import maya_lib
def run(entity = None):
    namespaces = maya_lib.non_ref_namespace()
    maya_lib.remove_namespaces2(namespaces)
    result = maya_lib.non_ref_namespace()
    return True
if __name__ == "__main__":
    run()