import maya.cmds as mc
import os

from rf_utils.context import context_info
from rftool.utils import file_utils
reload(context_info)
reload(file_utils)

def run(entity=None):
	if not entity:
		entity = context_info.ContextPathInfo(path=mc.file(q=True, sn=True))

	heroPath = entity.path.hero().abs_path()
	dataFile = os.path.join(heroPath, "{}_skin.hero.yml".format(entity.name))

	data = []
	if os.path.exists(dataFile):
		data_from_file = file_utils.ymlLoader(dataFile)
		if 'nonSkin' in data_from_file:
			data = data_from_file['nonSkin']
	print data
	if data:
		for geo in data:
			if mc.objExists(geo):
				mc.skinCluster(geo, e=True, ub=True)

if __name__ == "__main__":
	run()