import maya.cmds as mc
from rf_utils.context import context_info

# def run():
# 	asset = context_info.ContextPathInfo()
# 	allmover = "AllMover_Ctrl"

# 	if asset.type == "prop":
# 		if mc.objExists("{}.lockScale".format(allmover)):
# 			mc.setAttr("{}.lockScale".format(allmover), 0)

# 		mc.setAttr("{}.sx".format(allmover), lock=True)
# 		mc.setAttr("{}.sy".format(allmover), lock=False)
# 		mc.setAttr("{}.sz".format(allmover), lock=True)

# 	else:
# 		if mc.objExists("{}.lockScale".format(allmover)):
# 			mc.setAttr("{}.lockScale".format(allmover), 1)

# 		mc.setAttr("{}.sx".format(allmover), lock=True)
# 		mc.setAttr("{}.sy".format(allmover), lock=True)
# 		mc.setAttr("{}.sz".format(allmover), lock=True)
def run() :
    objs = mc.ls( '*.lockScale' )

    for obj in objs :
        obj , lockScale = obj.split('.')

        if mc.getAttr( '%s.%s' %( obj , lockScale )) == True :
            for attr in ( 'sx' , 'sy' , 'sz' ):
                mc.setAttr( '%s.%s' % ( obj , attr ) , l = True )


if __name__ == "__main__":
	run()