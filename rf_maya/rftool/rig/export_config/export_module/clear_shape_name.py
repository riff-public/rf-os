import maya.cmds as mc

def run() :
    transformNode = mc.ls( type = 'transform' )
    
    if transformNode :
        for tfn in transformNode :
            shapes = mc.listRelatives( tfn , s = True )
            
            if shapes :
                if not mc.nodeType( shapes[0] ) == 'camera' :
                    mc.rename( shapes[0] , '{}Shape'.format(tfn) )