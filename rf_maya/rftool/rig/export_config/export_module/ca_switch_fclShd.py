from rf_utils.context import context_info
import maya.cmds as mc
import os
def run():
    asset = context_info.ContextPathInfo()
    asset.context.update(look=asset.process)
    textPreviewPath = asset.path.scheme(key='previewTexturePath').abs_path()
    
    uiShd = mc.ls('fclRig_md:*Plane*',type='file')
    fclShd = mc.ls('fclRig_md:*',type = 'file')
    geoShd = list(set(fclShd)-set(uiShd))
    
    
    from rf_utils import admin
    for shd in uiShd:
        imgPath = mc.getAttr(shd+'.fileTextureName')
        #print imgPath
        img_split = imgPath.split('/')
        img_file = img_split[-1]
        destination =os.path.join(textPreviewPath,img_file)
        admin.copyfile(imgPath,destination)    
        mc.setAttr(shd+'.fileTextureName',destination,type='string')
    for shd in geoShd:
        imgPath = mc.getAttr(shd+'.fileTextureName')
        #print imgPath
        img_split = imgPath.split('/')
        img_file = img_split[-1]
        destination =os.path.join(textPreviewPath,img_file)  
        mc.setAttr(shd+'.fileTextureName',destination,type='string')
        
        print imgPath

if __name__ == "__main__":
    run()