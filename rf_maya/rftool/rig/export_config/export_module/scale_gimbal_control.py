import maya.cmds as mc
import maya.OpenMaya as om

def run( size=0.85 ) :
	
	gmblCtrls = mc.ls( '*Gmbl*_ctrl' , '*Gmbl*_Ctrl', '*Gmbl*Ctrl', '*:*Gmbl*Ctrl', '*Gmbl*ctrl', '*:*Gmbl*ctrl')
	for each in gmblCtrls :
		ctrl = pc.Dag( each.replace( 'Gmbl' , ''  ) )
		gmbl = pc.Dag( each )
		copyCurveShape( ctrl , gmbl )
		mc.scale(size,size,size,'{}Shape.cv[0:]'.format(gmbl),cs=True)
		#gmbl.scaleShape( size )
	print "     >>", len(gmblCtrls)	

def copyCurveShape( src = '' , target = '' ) :

	copy_crv_shape(original = src, copy_list = [target])

def copy_crv_shape(original='' , copy_list = [] ):
	if original and copy_list:
		crv_dict = get_shape(original)
		#print crv_dict
		for crv in copy_list:
			set_shape_base(crv,crv_dict)

def get_shape(crv=None):
	# get crv data from curve input
	# vlidate_curve to get shapeNode
	crvShapes =  validate_curve(crv)
	for crvShape in crvShapes:
		# create controlShape dict
		# get point, dorm, degree, colour by get attribute command
		crvShapeDict = {
		"points": mc.getAttr(crvShape+'.controlPoints[*]'),
		"knots": [],
		"form": mc.getAttr(crvShape + ".form"),
		"degree": mc.getAttr(crvShape + ".degree"),
		"colour": mc.getAttr(crvShape + ".overrideColor")}
		# get knot by get_knots function
		crvShapeDict["knots"] = get_knots(crvShape)
		return crvShapeDict

def get_knots(crvShape=None):
	# get knots data by using maya api
	mObj = om.MObject()
	sel = om.MSelectionList()
	sel.add(crvShape)
	sel.getDependNode(0, mObj)
	fnCurve = om.MFnNurbsCurve(mObj)
	tmpKnots = om.MDoubleArray()
	fnCurve.getKnots(tmpKnots)
	return [tmpKnots[i] for i in range(tmpKnots.length())]

def validate_curve(crv=None):
	# check input that is nurbsCurves shape or not
	if (mc.nodeType(crv) == "transform" or mc.nodeType(crv) == 'joint') and mc.nodeType(mc.listRelatives(crv, c=1, s=1)[0]) == "nurbsCurve":
		crvShapes = mc.listRelatives(crv, c=1, s=1)
	elif mc.nodeType(crv) == "nurbsCurve":
		crvShapes = mc.listRelatives(mc.listRelatives(crv, p=1)[0], c=1, s=1)
	else:
		mc.error("The object " + crv + " passed to validateCurve() is not a curve")
	return crvShapes



def set_shape_base(crv,crvShapeDict):
	# base function for change shape from crvShapeDict data
	mc.curve(crv, r=1, p = crvShapeDict["points"], k=crvShapeDict["knots"], d=crvShapeDict["degree"], per=bool(crvShapeDict["form"]))

