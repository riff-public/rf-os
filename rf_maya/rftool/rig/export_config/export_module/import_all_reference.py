import maya.cmds as mc
import maya.mel as mm
import pymel.core as pm
from rf_maya.lib import maya_lib

# def importAllReference() :
# 	'''
# 	Import all reference node in current scene.
# 	Return imported reference nodes.
# 	'''
# 	rfns = mc.ls( type='reference' )
# 	impRfns = []
	
# 	for rfn in rfns :
		
# 		if not rfn == 'sharedReferenceNode' :
			
# 			try :
				
# 				fn = mc.referenceQuery( rfn , filename=True )
# 				mc.file( fn , importReference=True )
# 				print '%s has been imported.' % rfn
# 				impRfns.append( rfn )
				
# 			except RuntimeError :
				
# 				print '%s is not connected to reference file.' % rfn
# 				mc.lockNode( rfn , l=0 )
# 				mc.delete( rfn )
			
# 	return impRfns

def run():
	""" Checking for Rig_Grp and Geo_Grp """
	maya_lib.import_ref()
	return None