import maya.cmds as mc
import pymel.core as pmc

def run():
    shapes = mc.ls( type = 'mesh' , ni = True )

    for shape in shapes :
        skcNode = mc.listConnections( shape , type = 'skinCluster' )
        if skcNode :
            mc.select( shape )
            print shape
            pmc.mel.removeUnusedInfluences()