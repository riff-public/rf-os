import maya.cmds as mc
import maya.mel as mm 
import json
# def run():
#     exceptions = ( 'lambert1' , 'particleCloud1' )
#     mat = mc.ls( mat = True )
    
#     # for each in mat :
#     #     if not each in exceptions :
#     #         mc.delete( each )
#     to_del = [shd for shd in mat if shd not in exceptions]
#     try:
#         mc.delete(to_del)
#     except Exception, e:
#         pass
        
#     for each in mc.ls( type = 'mesh' ):
#         mc.select( each , r = True )
#         cmd = 'hyperShade -assign lambert1;'
#         mm.eval(cmd)

def run():
    fcl_mat = mc.ls('fclRig_md:*')
    fcl_list = []
    if fcl_mat:
        for i in fcl_mat:
            fcl_list.append(i)
            fclStr = json.dumps(fcl_list)
        mc.createNode('transform',n='TmpFclData')
        mc.addAttr('TmpFclData',ln='fclMat',dt='string')
        mc.setAttr('TmpFclData.fclMat',fclStr,type='string')
if __name__ == "__main__":
    run()