from rf_maya.rftool.shade import shade_utils
from rf_utils.context import context_info
import os
import clean_shade
import maya.cmds as mc
def run():
	# get ue texture file
	# entity = context_info.ContextPathInfo()
	# entity.context.update(look='main', res='md')
	# path = entity.library('techMtr')
	entity = context_info.ContextPathInfo()
	entity.context.update(look='main', res='md')
	hero_dir = entity.path.hero().abs_path()
	file_path = os.listdir(hero_dir)
	for f in file_path:
		if '_mtr_tech_' in f:
			tech_path = os.path.join(hero_dir,f)
		else:
			tech_path = ''

	# if ue texture not found assign lambert 1 to every mesh
	if os.path.isfile(tech_path):
		# find namespace of rigGrp
		rigGrp = mc.ls('*:*Rig_Grp')[0]
		char_namespace = ':'.join(rigGrp.split(':')[:-1])
		shade_utils.apply_ref_shade(tech_path,'techMtr',char_namespace)
		print 'finish assign material'
	else:
		print 'assign lambert 1'
		clean_shade.run()