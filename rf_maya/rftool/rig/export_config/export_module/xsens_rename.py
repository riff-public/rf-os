import maya.cmds as mc

def run():
	try:
		# rename joint
		mc.rename("Root_UeJnt", "Hips")

		mc.rename("UpLeg_R_UeJnt", "RightUpLeg")
		mc.rename("LowLeg_R_UeJnt", "RightLeg")
		mc.rename("Ankle_R_UeJnt", "RightFoot")
		mc.rename("Ball_R_UeJnt", "RightToeBase")

		mc.rename("UpLeg_L_UeJnt", "LeftUpLeg")
		mc.rename("LowLeg_L_UeJnt", "LeftLeg")
		mc.rename("Ankle_L_UeJnt", "LeftFoot")
		mc.rename("Ball_L_UeJnt", "LeftToeBase")

		mc.rename("Spine2_UeJnt", "Spine")
		mc.rename("Spine3_UeJnt", "Spine1")
		mc.rename("Spine4_UeJnt", "Spine2")
		mc.rename("Spine5_UeJnt", "Spine3")

		mc.rename("Clav_R_UeJnt", "RightShoulder")
		mc.rename("UpArm_R_UeJnt", "RightArm")
		mc.rename("Forearm_R_UeJnt", "RightForeArm")
		mc.rename("Wrist_R_UeJnt", "RightHand")

		mc.rename("Clav_L_UeJnt", "LeftShoulder")
		mc.rename("UpArm_L_UeJnt", "LeftArm")
		mc.rename("Forearm_L_UeJnt", "LeftForeArm")
		mc.rename("Wrist_L_UeJnt", "LeftHand")

		mc.rename("Neck_UeJnt", "Neck")
		mc.rename("Head_UeJnt", "Head")

		# rename finger
		for part in ['Index','Middle','Ring','Pinky']:
			mc.rename('{}2_L_UeJnt'.format(part),'LeftHand{}1'.format(part))
			mc.rename('{}3_L_UeJnt'.format(part),'LeftHand{}2'.format(part))
			mc.rename('{}4_L_UeJnt'.format(part),'LeftHand{}3'.format(part))

			mc.rename('{}2_R_UeJnt'.format(part),'RightHand{}1'.format(part))
			mc.rename('{}3_R_UeJnt'.format(part),'RightHand{}2'.format(part))
			mc.rename('{}4_R_UeJnt'.format(part),'RightHand{}3'.format(part))				


		mc.rename('Thumb1_L_UeJnt','LeftHandThumb1')
		mc.rename('Thumb2_L_UeJnt','LeftHandThumb2')
		mc.rename('Thumb3_L_UeJnt','LeftHandThumb3')

		mc.rename('Thumb1_R_UeJnt','RightHandThumb1')
		mc.rename('Thumb2_R_UeJnt','RightHandThumb2')
		mc.rename('Thumb3_R_UeJnt','RightHandThumb3')

		# reorder hierachy
		mc.parent("RightUpLeg", "LeftUpLeg", "Spine1_UeJnt", w=True)
		mc.parent("RightUpLeg", "LeftUpLeg", "Spine1_UeJnt", "Pelvis_UeJnt")

		mc.parent("LeftShoulder", "RightShoulder", "Neck", "Spine5Sca_UeJnt", w=True)
		mc.parent("LeftShoulder", "RightShoulder", "Neck", "Spine5Sca_UeJnt", "Spine3")



	except Exception as e:
		print e

if __name__ == "__main__":
	run()