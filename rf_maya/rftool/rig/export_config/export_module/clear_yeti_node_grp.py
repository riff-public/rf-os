import maya.cmds as mc
import maya.mel as mm
import re

def run():
    if mc.objExists ('YetiNode_Grp'):
        importRigElementDeleteGrp()
    else :
        print 'No YetiNode_Grp'
        # rigTools.importAllReference()
        # rigTools.removeAllNamespace()
        # maya_lib.import_ref()
        # maya_lib.remove_namespaces()    

def importRigElementDeleteGrp() :

    '''
    Import all reference.
    Then iterate through each child parented to 'Delete_Grp'.
    Parent each child to related constraint target.
    Remove 'Delete_Grp'.
    '''
    
    # Import all referenced files
    # importAllReferences()
    
    # delGrp2 = mc.ls('delete_grp', 'Delete_Grp')
    delGrp = 'YetiNode_Grp'
    
    if mc.objExists( delGrp ) :
        
        for each in mc.listRelatives( delGrp , type='transform' ) :
            
            target = findConstraintSource( each )
            
            if target :
                mc.parent( each , target )
                removeConstraintTarget( target , each )

        mc.delete( delGrp )
    # removeAllNodeInNamespace()
    # removeAllNamespace()


def removeConstraintTarget( target='' , dag='' ) :
    '''
    Remove constraint target from given dag.
    '''
    attrDict = {
                    'tx' : None ,
                    'ty' : None ,
                    'tz' : None ,
                    'rx' : None ,
                    'ry' : None ,
                    'rz' : None ,
                    'sx' : None ,
                    'sy' : None ,
                    'sz' : None
                }
    # Collecting transform values before remove the constraint target.
    for attr in attrDict.keys() :
        currNodeAttr = '{}.{}'.format( dag , attr )
        if not mc.getAttr( currNodeAttr , l=True ) :
            attrDict[attr] = mc.getAttr( currNodeAttr )

    mc.select( target , r=True )
    mc.select( dag , add=True )
    mm.eval( 'performRemoveConstraintTarget 0;' )

    # Setting transform values back after constraint target has been removed.
    for attr in attrDict.keys() :
        currNodeAttr = '{}.{}'.format( dag , attr )
        if attrDict[attr] :
            mc.setAttr( currNodeAttr , attrDict[attr] )

def findConstraintSource( dag='' ) :
    '''
    Find the constraint target of given DAG.
    Return a constraint target.
    '''
    cnstrs = findConnectedContraint( dag )
    
    # Make sure that given DAG has connected constraint node.
    if cnstrs :

        targets = []
        for cnstr in cnstrs :
            # Iterate to each constraint node
            # and get current constraint target.
            currTargets = findTargetFromConstraintNode( cnstr )
            for currTarget in currTargets :
                if not currTarget in targets :
                    targets.append( currTarget )
        
        # Make sure that given DAG has only one target.
        if len( targets ) > 1 :
            print '{} has more than one target.'.format(dag)
            return None
        else :
            return targets[0]
    else :
        print '{} has no related constraint node.'.format(dag)
        return None

def findConnectedContraint( dag='' ) :
    '''
    Find constraint nodes that connected to given dag.
    Return list of constraint nodes.
    '''
    cnstrs = []

    cons = mc.listConnections( dag , s=True , d=False , scn=True , type='constraint' )
    if cons :
        for con in cons :
            if not con in cnstrs :
                cnstrs.append( con )

    return cnstrs

def findTargetFromConstraintNode( constraintNode='' ) :
    '''
    Find constraint targets that connected to given constraintNode.
    Return list of constraint targets.
    '''
    targets = []
    # Get current source connections.
    cnstrSrcs = mc.listConnections( constraintNode , s=True , d=False , p=True )
    
    for cnstrSrc in cnstrSrcs :
        # Iterate to each source connection.
        # If source has attribute parentMatrix connected in,
        # append to 'targets'
        exp = r'(.*)(\.parentMatrix)'
        m = re.search( exp , cnstrSrc )
        if m :
            if not m.group(1) in targets :
                targets.append( m.group(1) )

    return targets

