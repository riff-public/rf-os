from rf_maya.rftool.subasset import subasset_utils
reload(subasset_utils)



def run(entity = None):
	# ----- RIG WORKSPACE
	# write data
	print 'write subAssetExport'
	res = subasset_utils.write_subasset_data()
	print res
	# disconnect constraint before rig publish
	if res:
		subasset_utils.disconnect_subasset_setups()

