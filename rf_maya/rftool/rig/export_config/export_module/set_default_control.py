import maya.cmds as mc
from collections import OrderedDict

from rf_utils.context import context_info

# setDefaultControl
def run(char = '', *args):
	if char == '':
		char = context_info.ContextPathInfo().name

	## SetAttr
	ArmList = ['Arm_L_Ctrl', 'Arm_R_Ctrl']
	SpineList = ['Spine_Ctrl']
	LegList = ['Leg_L_Ctrl', 'Leg_R_Ctrl']
	GrpHideList = ['Skin_Grp', 'Jnt_Grp', 'Ikh_Grp', 'Still_Grp', 'TechGeo_Grp']
	GrpShowList = ['Ctrl_Grp']
	localWorldAttr = ['Head_Ctrl', 'UpArmFk_L_Ctrl', 'UpArmFk_R_Ctrl']
	MdPrGeo = ['MdGeo_Grp', 'PrGeo_Grp']

	## Arm
	for eachArm in ArmList:
		if mc.objExists('%s.fkIk' % eachArm):
			mc.setAttr('%s.fkIk' % eachArm, 0)

	## Spine
	for eachSpine in SpineList:
		if mc.objExists('%s.fkIk' % eachSpine):
			mc.setAttr('%s.fkIk' % eachSpine, 1)

	## Leg
	for eachLeg in LegList:
		if mc.objExists('%s.fkIk' % eachLeg):
			mc.setAttr('%s.fkIk' % eachLeg, 1)

	## SetAttr AllMover Vis
	attrCtrlVis = layerControlAttr()
	AllMoverObj = '%s_Crv' % char
	
	if mc.objExists(AllMoverObj):
		for each in attrCtrlVis:
			for sels in attrCtrlVis[each]:
				if sels:
					if sels == 'MasterVis' or sels == 'GeoVis':
						mc.setAttr('%s.%s' % (AllMoverObj,'MasterVis'), 1)
		  
					else:
						mc.setAttr('%s.%s' % (AllMoverObj,sels), 0)
	
	## SetAttr Visibility
	for each in GrpHideList:
		if mc.objExists(each):
			mc.setAttr('%s.v' % each, 0)
	for each in GrpShowList:
		if mc.objExists(each):
			mc.setAttr('%s.v' % each, 1)
	for each in MdPrGeo:
		if mc.objExists(each):
			mc.setAttr('%s.v' % each, 1)     
			   
	## SetAttr LocalWorld
	for each in localWorldAttr:
		if mc.objExists('%s.localWorld' % each):
			mc.setAttr('%s.localWorld' % each, 1)

	## Lock Attribute Visibility 'Rig_Grp'
	lockAttr(listObj = ['Rig_Grp'], attrs = ['visibility'], lock = True, keyable = False)

	## turn off visibility wrinkledRig
	allMidWrinkle = mc.ls("*MidWrinkledRig*", typ="nurbsCurve")
	for shape in allMidWrinkle:
		if "Gmbl" not in shape:
			wrinkledCrv = mc.listRelatives(shape, ap=True)[0]
			mc.setAttr(wrinkledCrv + ".wrinkledVis", 0)

	## lock Atribute Amp Facial
	lockAttrAmpCharacter()
	
def layerControlAttr (*args):
	## Tital Ctrl Attr 
	mainAttrLayerVis = OrderedDict()
	mainAttrLayerVis['Control'] = [ 'MasterVis', 
									'BlockingVis', 
									'SecondaryVis', 
									'AddRigVis']
	mainAttrLayerVis['Facial'] =  [ 'FacialVis', 
									'DetailVis']
	mainAttrLayerVis['Geometry'] = ['GeoVis']                

	return  mainAttrLayerVis

def lockAttrAmpCharacter( *artgs):

	ctrlAll = 'FacialCtrl_Grp'
	if mc.objExists(ctrlAll):
		allLists = listGeo(sels = ctrlAll, nameing = '_Ctrl', namePass = '_Grp')
	else:
		return

	allCtrl = []
	for each in allLists:
		lists = each.split('|')[-1]
		if '_Ctrl' in lists:
			if not 'Gmbl_Ctrl' in lists:
				allCtrl.append(lists)
				eachShape = '{}Shape'.format(lists)
				attrs = listAttribute(obj = [eachShape])
				for obj in attrs:
					if '_Min' in obj or '_Max' in obj:
						lockAttr(listObj = [eachShape], attrs = [obj],lock = True, keyable = True)

def listGeo(sels = '', nameing = '_Geo', namePass = '_Grp', *args):
	if not sels:
		sels = mc.ls(sl = True)[0]
	objLists = []
	objAlls = mc.listRelatives(sels , c = True, ad = True , type = 'transform' , f = True)
	objAll = []
	if objAlls:
		for each in objAlls:
			if nameing in each:
				objAll.append(each)
		if objAll:
			for each in objAll:
				obj = each.split('|')[-1]
				if namePass in obj:
					pass
				else:
					objLists.append(each)
	return objLists

def lockAttr(listObj = [], attrs = [],lock = True, keyable = True,*args):
    for i in range(len(listObj)):
        for x in range(len(attrs)):
            mc.setAttr("%s.%s" % (listObj[i], attrs[x]) , lock = lock, keyable = keyable)