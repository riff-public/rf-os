import maya.cmds as mc

def run():
	#-- CleanUp Resolution
	resGeoGrp = 'PvGeo_Grp'
	chlds = mc.listRelatives( 'Geo_Grp' , c = True , fullPath = True )
	delList = []

	for chld in chlds :
	    if not chld.split('|')[-1] == resGeoGrp :
	        delList.append(chld)

	if delList :
	    mc.delete(delList)

