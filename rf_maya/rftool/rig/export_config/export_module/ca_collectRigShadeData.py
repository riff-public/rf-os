from rf_maya.rftool.rig.rigScript import caRigModule
reload (caRigModule)
import maya.cmds as mc

def run():
	caRigModule.createShadeData( targetGrp='Rig_Grp', objs=None)

if __name__ == "__main__":
	run()