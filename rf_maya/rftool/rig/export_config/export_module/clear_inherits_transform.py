import maya.cmds as mc

def run():
    listInherCtrlRef = mc.ls('*:cheek*DtlCtrlZro_*_grp','*:eye*DtlCtrlZro_*_grp','*:mouth*DtlCtrlZro_*_grp','*:nose*DtlCtrlZro_*_grp','*:cheekDtlCtrlZro_*_grp')
    listInherJntRef = mc.ls('*:cheek*DtlJntZro_*_grp','*:eye*DtlJntZro_*_grp','*:mouth*DtlJntZro_*_grp','*:nose*DtlJntZro_*_grp','*:cheekDtlJntZro_*_grp')
    listInherCtrl = mc.ls('cheek*DtlCtrlZro_*_grp','eye*DtlCtrlZro_*_grp','mouth*DtlCtrlZro_*_grp','nose*DtlCtrlZro_*_grp','cheekDtlCtrlZro_*_grp')
    listInherJnt = mc.ls('cheek*DtlJntZro_*_grp','eye*DtlJntZro_*_grp','mouth*DtlJntZro_*_grp','nose*DtlJntZro_*_grp','cheekDtlJntZro_*_grp')
    listsInherAll = []
    for each in listInherCtrlRef:
        if mc.objExists(each):
            mc.setAttr('{}.inheritsTransform'.format(each),0)
            listsInherAll.append(each)
        else: pass
    for each in listInherCtrl:
        if mc.objExists(each):
            mc.setAttr('{}.inheritsTransform'.format(each),0)
            listsInherAll.append(each)
        else: pass
    for each in listInherJntRef:
        if mc.objExists(each):
            mc.setAttr('{}.inheritsTransform'.format(each),0)
            listsInherAll.append(each)
        else: pass
    for each in listInherJnt:
        if mc.objExists(each):
            mc.setAttr('{}.inheritsTransform'.format(each),0)
            listsInherAll.append(each)
        else: pass