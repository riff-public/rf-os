import maya.cmds as mc

def run():
	try:
		mc.parent("EyeLFT_UeJnt", "EyeLidLFT_UeJnt", "EyeRGT_UeJnt", "EyeLidRGT_UeJnt", w=True)
		mc.parent("EyeLFT_UeJnt", "EyeLidLFT_UeJnt")
		mc.parent("EyeRGT_UeJnt", "EyeLidRGT_UeJnt")

		mc.rename("EyeLidLFT_UeJnt", "EyeLFT_UeJnt")
		mc.rename("|EyeLFT_UeJnt|EyeLFT_UeJnt", "EyeLidLFT_UeJnt")
		mc.rename("EyeLidRGT_UeJnt", "EyeRGT_UeJnt")
		mc.rename("|EyeRGT_UeJnt|EyeRGT_UeJnt", "EyeLidRGT_UeJnt")

		mc.parent("EyeLFT_UeJnt", "EyeAdjLFT_UeJnt")
		mc.parent("EyeRGT_UeJnt", "EyeAdjRGT_UeJnt")
	except Exception as e:
		print e

if __name__ == "__main__":
	run()