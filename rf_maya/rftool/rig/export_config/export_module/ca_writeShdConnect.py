from rf_maya.rftool.rig.rigScript import caRigModule
reload (caRigModule)
import maya.cmds as mc
from rf_maya.lib import material_layer
from rftool.utils import file_utils
from rf_utils.context import context_info
import os

def run():
    entity = context_info.ContextPathInfo()
    geoGrp = entity.projectInfo.asset.geo_grp()
    geoGrpLs = mc.ls('*:%s' %geoGrp, type='transform')

    if geoGrpLs:
        geoGrp = geoGrpLs[0]
    
    
    layers = material_layer.list_layer_info(entity=entity)
    if not layers or not geoGrp:
        return
        
    print layers
    # # add shdConnect attr if it does not exists
    # if not mc.objExists('%s.shdConnect' %geoGrp):
    #     mc.addAttr(geoGrp, ln='shdConnect', dt='string')

    # build data
    result = {}
    disconnects = []
    for lyr, data in layers.iteritems():
        print lyr
        # setup data key dict
        shadeFile = data['path']
        shadeFileExt = os.path.basename(shadeFile)
        shadeFileName, ext = os.path.splitext(shadeFileExt)
        shadeFileName = shadeFileName.split('.')[0]  # get rid of .hero
        shadeKey = str(shadeFileName.split('_mtr_')[-1])

        if shadeKey not in result:
            result[shadeKey] = ([], [])

        # set current renderlayer 
        mc.editRenderLayerGlobals(crl=lyr)

        # loop each attribute that starts with sc__
        scAttrs = mc.listAttr(geoGrp, st='sc__*', ud=True)
        for attr in scAttrs:
            in_cons = mc.listConnections('%s.%s' %(geoGrp, attr), s=True, d=False, c=True, p=True,scn=1)
            if in_cons:
                for i in xrange(0, len(in_cons), 2):
                    src = in_cons[i+1]
                    des = in_cons[i]
                    
                    srcNode = src.split('.')[0]
                    if mc.nodeType(srcNode) == 'renderLayer':
                        continue

                    if mc.referenceQuery(srcNode, inr=True):
                        sfn = mc.referenceQuery(srcNode, f=True, wcn=True)
                        if sfn == shadeFile:
                            srcStr = str(src.split(':')[-1])
                            desStr = str(des.split(':')[-1])
                            result[shadeKey][0].append((srcStr, desStr))
                            # mc.disconnectAttr(src, des)
                            disconnects.append((src, des))


            out_cons = mc.listConnections('%s.%s' %(geoGrp, attr), s=False, d=True, c=True, p=True,scn=1)
            if out_cons:
                for i in xrange(0, len(out_cons), 2):
                    src = out_cons[i]
                    des = out_cons[i+1]
                    desNode = des.split('.')[0]
                    if mc.nodeType(desNode) == 'renderLayer':
                        continue

                    if mc.referenceQuery(desNode, inr=True):
                        sfn = mc.referenceQuery(desNode, f=True, wcn=True)
                        if sfn == shadeFile:
                            srcStr = str(src.split(':')[-1])
                            desStr = str(des.split(':')[-1])
                            result[shadeKey][1].append((srcStr, desStr))
                            # mc.disconnectAttr(src, des)
                            disconnects.append((src, des))
    # # disconnects            
    # for src, des in disconnects:
    #     mc.disconnectAttr(src, des)

    # # print str(result)
    # mc.setAttr('%s.shdConnect' %geoGrp, str(result), type='string')
    if not entity:
        entity = context_info.ContextPathInfo(path=mc.file(q=True, sn=True))
    dataFile = entity.output_name(outputKey='shdConnectData', hero=True)
    
    heroPath = entity.path.hero().abs_path()
    dataPath = '%s/%s' % (heroPath, dataFile)
    file_utils.ymlDumper(dataPath, result)
    # get back to masterLayer
    mc.editRenderLayerGlobals(crl='defaultRenderLayer')
    print 'write shdConnect data Done'

    print result
if __name__ == "__main__":
    run()
