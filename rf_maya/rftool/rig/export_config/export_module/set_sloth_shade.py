import maya.cmds as mc

def run(*args):
	if args:
		cl = color_list()
		for idx, obj in enumerate(args):
			shade = lambert(obj)
			mc.setAttr("{}.c".format(shade[0]), cl[idx][0], cl[idx][1], cl[idx][2])

def color_list():
	return ((1, 1, 1), (0, 0, 0), (1, 0, 0), (0, 0, 1), (0, 1, 0), (1, 1, 0), (1, 0, 1), (0, 1, 1))

def lambert(obj):
	shade = mc.shadingNode("lambert", asShader=True)
	shadeSG = mc.sets(n='{}SG'.format(shade), em=True, nss=True, r=True)
	mc.connectAttr("{}.oc".format(shade), "{}.ss".format(shadeSG), f=True)
	mc.sets(obj, fe=shadeSG)
	return shade, shadeSG

if __name__ == "__main__":
	try:
		run(("EyeGlass_L_Geo", "EyeGlass_R_Geo"))
	except:
		run(("Eyeglass_L_Geo", "Eyeglass_R_Geo"))