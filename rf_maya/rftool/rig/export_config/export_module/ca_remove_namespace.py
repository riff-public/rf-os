import maya.cmds as mc
import maya.mel as mm
import pymel.core as pm
from rf_maya.lib import maya_lib

# def removeNamespace() :
#     sels = mc.ls ( sl = True )
    
#     if sels :
#         for sel in sels :
#             if ':' in sel :
#                 refprefix = sel.split(':')[0]
#                 mc.namespace( rm = refprefix , mnr = True )
#     else :
#         mc.warning( 'No Select Object. Please Select One Object.' )

def run(entity = None):
	""" Checking for Rig_Grp and Geo_Grp """
	namespaces = maya_lib.non_ref_namespace()
	if 'fclRig_md' in namespaces:
		idx = namespaces.index('fclRig_md')
		namespaces.pop(idx)
	maya_lib.remove_namespaces2(namespaces)
	result = maya_lib.non_ref_namespace()
	return True
