import maya.cmds as mc

def run():
	rigGrp = "Rig_Grp"
	geoGrp = "Geo_Grp"
	skinGrp = "Reference"
	
	try:
		mc.parent(geoGrp, skinGrp, "Export_Grp")
	except:
		pass
	cons = mc.listRelatives(skinGrp, c=1, ad=1, typ="constraint")
	if cons:
		mc.delete(cons)
	#mc.rotate(0, 0, 0, mc.listRelatives(skinGrp, c=1, ad=1, typ="joint"))

	if mc.objExists(rigGrp):
		mc.delete(rigGrp)

if __name__ == "__main__":
	run()