import os
import maya.cmds as mc

def run():
	file_list = mc.ls('*_File')
	for i in file_list:
		fnPath = mc.getAttr(i+'.computedFileTextureNamePattern')
		#print fnPath
		if "_Diffuse_Head" not in i:
			fnPath = fnPath.replace('work','publ').replace('images','hero')
			pathList = fnPath.split('/')
			pathList.insert(9,'texPreview')

			nwPath = '/'.join(pathList)
		else:
			fnPath = fnPath.replace('work','publ').replace('images','hero')
			pathList = fnPath.split('/')
			pathList.insert(9,'texRen')      

		nwPath = '/'.join(pathList)
		mc.setAttr(i+'.fileTextureName',nwPath,type='string')    
if __name__ == "__main__":
	run()