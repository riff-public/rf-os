import maya.cmds as mc

def run() :
    animCrvs = mc.ls( type='animCurve')
    animKeyTypes = ('animCurveTL', 'animCurveTA', 'animCurveTT', 'animCurveTU')
    if animCrvs:
        # filter only anim curve with time input type and with no connection to "input" channel
        to_delete = [crv for crv in animCrvs if mc.nodeType(crv) in animKeyTypes and not mc.listConnections('{}.input'.format(crv), s=True, d=False)]
        if to_delete:
            mc.delete( to_delete )   
