import sys, os
import subprocess
import maya.cmds as mc

import logging
from rf_utils.context import context_info
reload(context_info)

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
version = mc.about(v = True)

if version in ['2015', '2016', '2017','2018','2020']:
	MAYA_PYTHON = 'C:/Program Files/Autodesk/Maya%s/bin/mayabatch.exe' % version
	MAYA_PYTHON = 'C:\\Program Files\\Autodesk\\Maya%s\\bin\\mayapy.exe' % version
department = 'Py'
asset_context = context_info.ContextPathInfo()
env = asset_context.projectInfo.get_project_env(dcc='maya', dccVersion=version, department=department)




MAYACMD = '%s/mayaUE_batch_process.py' % moduleDir

def run(src, dst, processList, output='save', export='',project=''  , res='' , task =''):
	print 'Running subprocess'
	# subprocess.call([MAYA_PYTHON, MAYACMD, src, dst, processList, output])
	# return
	p = subprocess.Popen([MAYA_PYTHON, MAYACMD, src, dst, processList, output, export, project, res , task], stdout=subprocess.PIPE, stderr=subprocess.STDOUT,env=env)
	stdout, stderr = p.communicate()
	print 'Finish subprocess\n\n'

	logger.info(stdout)
	# logger.error(stderr)
	if os.path.exists(dst):
		return dst