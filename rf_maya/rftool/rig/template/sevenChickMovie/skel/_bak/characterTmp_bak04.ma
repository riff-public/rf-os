//Maya ASCII 2017ff05 scene
//Name: characterTmp.ma
//Last modified: Thu, Dec 06, 2018 06:32:48 PM
//Codeset: 1252
requires maya "2017ff05";
requires "stereoCamera" "10.0";
requires -nodeType "VRaySettingsNode" -dataType "vrayFloatVectorData" -dataType "vrayFloatVectorData"
		 -dataType "vrayIntData" "vrayformaya" "3.52.03";
requires -nodeType "ilrOptionsNode" -nodeType "ilrUIOptionsNode" -nodeType "ilrBakeLayerManager"
		 "Turtle" "2017.0.0";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2017";
fileInfo "version" "2017";
fileInfo "cutIdentifier" "201710312130-1018716";
fileInfo "osv" "Microsoft Windows 8 Business Edition, 64-bit  (Build 9200)\n";
fileInfo "vrayBuild" "3.52.03 b05f456";
createNode transform -s -n "persp";
	rename -uid "55D91CF0-4174-028C-AB3E-B783365A84BC";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -4.8908054103463403 18.745974512268063 33.41748389973651 ;
	setAttr ".r" -type "double3" -16.53835272958478 -10.999999999996787 -2.0250525957655078e-016 ;
	setAttr ".rp" -type "double3" 1.2490009027033011e-016 -1.7763568394002505e-015 -8.8817841970012523e-016 ;
	setAttr ".rpt" -type "double3" -6.9126617953226463e-016 -7.9903663801131593e-017 
		4.4837055728371754e-016 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "87CA264F-402C-8A8C-6542-878FC16E721B";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999979;
	setAttr ".coi" 34.488558761053156;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 0.55542624698535548 11.577205916169927 0.2198038385166754 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "83CA69B1-4331-EE27-E940-DBBF2C677A44";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 1000.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "51E1E102-4960-9788-0D04-868C6976E1F2";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "F569C215-49C8-5BAA-D552-19B82B1B344F";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1.100639105902778 17.728276282244639 1000.195989455337 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "2ED6BF06-487B-DE10-7B9D-F0A13D59606A";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.195989455337;
	setAttr ".ow" 22.757226562499998;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".tp" -type "double3" 1.100639105902778 17.728276282244639 0 ;
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "3F72F437-4DAE-5212-4626-32B8BEF88BEF";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1004140897326 12.212560477299839 1.0952854175233151 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "FD0102CD-4EFB-A46C-3BEA-72A1ECB2F32B";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 999.51532991962927;
	setAttr ".ow" 1.0526315789473684;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".tp" -type "double3" 0.58508417010307312 12.203959465026855 1.0939621848657111 ;
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "Export_Grp";
	rename -uid "3D9A1DE1-4FEB-9036-F213-838DB79670CA";
createNode transform -n "TmpJnt_Grp" -p "Export_Grp";
	rename -uid "72ED4B28-47D2-1B0B-9B2D-96B07E045087";
createNode joint -n "Root_TmpJnt" -p "TmpJnt_Grp";
	rename -uid "06EACF38-4558-CAB6-EA03-B3A8D04AC021";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".t" -type "double3" 0 7.3509799766627761 0.36272652352661428 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 15.73707328056101 0 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Spine1_TmpJnt" -p "Root_TmpJnt";
	rename -uid "8CBF09F9-4390-7748-17FE-C480003D92B3";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" 1.2191639353682723e-032 0.11392668982184073 0.0053023698430085808 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 16.462721689908392 7.9790056857053538e-017 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Spine2_TmpJnt" -p "Spine1_TmpJnt";
	rename -uid "39638C02-4532-83C1-3652-AEBD29A17D68";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 2.18827532116929e-031 0.26851330389030093 0.0056082788197299172 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 18.962119031422834 1.1092698799812162e-016 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Spine3_TmpJnt" -p "Spine2_TmpJnt";
	rename -uid "309E4275-44BC-1E6A-94D0-2EB55A768C35";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" 3.1985835777539049e-016 0.32509547499180869 -0.0076429184310304876 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 16.462721689908392 7.9790056857053538e-017 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Spine4_TmpJnt" -p "Spine3_TmpJnt";
	rename -uid "D35A4DCB-4F6F-F1EA-B9A6-B5B41ABAAA72";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" -2.8635251838015738e-012 0.32762898273426622 -0.019369922501899506 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 16.462721689908392 7.9790056857053538e-017 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Spine5_TmpJnt" -p "Spine4_TmpJnt";
	rename -uid "23E622F6-42DA-888D-C0CB-999C62090852";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" 0 0.35611440348509404 -0.049869664149744808 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 16.462721689908392 7.9790056857053538e-017 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Clav_L_TmpJnt" -p "Spine5_TmpJnt";
	rename -uid "D05600F6-457E-2124-44B4-53B8EF5AAC62";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".ove" yes;
	setAttr ".t" -type "double3" 0.24727582311425278 1.315619877454056 -0.190877519717914 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" 0 0 -89.999999999999986 ;
	setAttr ".bps" -type "matrix" 2.2204460492503131e-016 -1 0 0 1 2.2204460492503131e-016 0 0
		 0 0 1 0 0.40396301701286447 22.628380391977608 0.93191360083425789 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "UpArm_L_TmpJnt" -p "Clav_L_TmpJnt";
	rename -uid "C4713DC7-405C-B172-2DC0-D8B32BA23C5A";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 0.23654892391681237 0.62735473917581264 -0.20160620419344621 ;
	setAttr ".ro" 4;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" -4.3311767654220716 0 0 ;
	setAttr ".bps" -type "matrix" 2.2204460492503131e-016 -1 0 0 1 2.2204460492503131e-016 0 0
		 0 0 1 0 2.6840862727483024 21.950819439051291 -0.34347723857962076 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Forearm_L_TmpJnt" -p "UpArm_L_TmpJnt";
	rename -uid "E841F686-45D9-1923-413C-29835682632F";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" -4.5474735088646412e-012 1.849660844833477 -0.018140870306724764 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" 4.3311172686693062 0 0 ;
	setAttr ".bps" -type "matrix" 2.2204460492503131e-016 -1 0 0 0.99999999999946088 2.2204460492491157e-016 -1.0384142298428127e-006 0
		 1.0384142298428127e-006 2.3057427741397801e-022 0.99999999999946088 0 6.7891781465089913 21.950819439060187 -0.42637972484178432 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Wrist_L_TmpJnt" -p "Forearm_L_TmpJnt";
	rename -uid "9F61AA47-486F-EDAF-5DC3-469A028B485D";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" -1.4713563700752275e-011 2.1739863867946694 0.018358648332171101 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" 2.2204460492503131e-016 -1 0 0 1 2.2204460492503131e-016 8.3960588285190204e-014 0
		 -8.3960588285190204e-014 -1.8642995655058271e-029 1 0 11.145136555499452 21.950819439088733 -0.34347297829513029 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Hand_L_TmpJnt" -p "Wrist_L_TmpJnt";
	rename -uid "D49091D4-40DC-C009-04AE-3096DF5C2216";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" -2.7213786779611837e-012 0.59573146846045777 -4.0116803129430778e-007 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 2.2204460492503131e-016 -1 0 0 1 2.2204460492503131e-016 8.3960588285190204e-014 0
		 -8.3960588285190204e-014 -1.8642995655058271e-029 1 0 11.94334900537585 21.950819439093973 -0.34347375639834526 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Index1_L_TmpJnt" -p "Hand_L_TmpJnt";
	rename -uid "27545D27-422D-4D1B-C1B5-0BBC76570201";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".t" -type "double3" -0.038352345096976492 -0.12421866028796913 0.26201475274996783 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" 89.999999999999872 -87.241493305866285 -89.999999999999872 ;
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 11.915961054098767 22.155901831531715 0.36968006095532863 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Index2_L_TmpJnt" -p "Index1_L_TmpJnt";
	rename -uid "08E96B63-46AF-B095-C6F9-93BAB8BC5FA2";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" -1.0775588558978054e-016 0.36443626844157695 0.0010085040853926586 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 13.114219270621863 22.155901831531715 0.36968006095542938 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Index3_L_TmpJnt" -p "Index2_L_TmpJnt";
	rename -uid "8B01DB43-4401-796D-1C01-40A5CAA06C6A";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" -4.842245386026166e-017 0.43245486206038919 -0.010775498683168294 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 13.805785441300909 22.155901831531718 0.3696800609554875 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Index4_L_TmpJnt" -p "Index3_L_TmpJnt";
	rename -uid "CE8894A8-4CE5-8614-ABCE-A0A4DE275673";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 5.141290607381148e-017 0.2474254322569811 -0.030214513893994877 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 14.271394348292739 22.155901831531718 0.36968006095552658 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Index5_L_TmpJnt" -p "Index4_L_TmpJnt";
	rename -uid "D4DA77ED-4488-0C03-18BF-42BEBA9C554E";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" -5.5511151231258e-017 0.26974814872242281 -0.019628258556032563 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 14.812322343180313 22.155901831531722 0.36968006095557193 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Thumb1_L_TmpJnt" -p "Hand_L_TmpJnt";
	rename -uid "99FD6254-43C4-F746-ED3F-5C8E26357ACB";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".t" -type "double3" 0.07746190630449945 -0.29941979947261288 0.18744907331972965 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" 44.72036750614312 10.637194302648707 -77.29041894046172 ;
	setAttr ".bps" -type "matrix" -0.59548586752167076 -0.80336578317847407 -4.9997343752636337e-014 0
		 0.73868319246536318 -0.54754062333671982 0.39312390790336815 0 -0.31582229615899904 0.23409973134130962 0.91948550452673483 0
		 11.518645340649305 21.905429719636764 0.32567524419271171 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Thumb2_L_TmpJnt" -p "Thumb1_L_TmpJnt";
	rename -uid "A0BC8DD8-4A45-21D5-F3D7-579B3F1ED332";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" -0.062882821164431058 0.27919502856170875 0.041970177158242347 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" -19.700796181147314 40.857685490685967 24.264632059267729 ;
	setAttr ".bps" -type "matrix" -0.59548586752167076 -0.80336578317847407 -4.9997343752636337e-014 0
		 0.73868319246536318 -0.54754062333671982 0.39312390790336815 0 -0.31582229615899904 0.23409973134130962 0.91948550452673483 0
		 12.026286103962779 21.529146703032435 0.59583939943602238 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Thumb3_L_TmpJnt" -p "Thumb2_L_TmpJnt";
	rename -uid "D9AE9F30-4184-6826-EF1D-198AB945AACB";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" -1.2434497875801873e-014 0.44156608118478524 -0.011012088131048604 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -0.59548586752167076 -0.80336578317847407 -4.9997343752636337e-014 0
		 0.73868319246536318 -0.54754062333671982 0.39312390790336815 0 -0.31582229615899904 0.23409973134130962 0.91948550452673483 0
		 12.724953059079672 21.011267666374362 0.96766684389406166 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Thumb4_L_TmpJnt" -p "Thumb3_L_TmpJnt";
	rename -uid "5EC22DAC-4EE9-025B-4016-9A8E669235F5";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 1.4210854715201994e-014 0.30386497438559157 -0.0076981831557087835 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" -0.59548586752167076 -0.80336578317847407 -4.9997343752636337e-014 0
		 0.73868319246536318 -0.54754062333671982 0.39312390790336815 0 -0.31582229615899904 0.23409973134130962 0.91948550452673483 0
		 13.320225968832865 20.570028305053697 1.2844684375111461 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "HandCup_L_TmpJnt" -p "Hand_L_TmpJnt";
	rename -uid "EEA8427F-41F2-6332-D3C6-61B49239312F";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" 0.057333983830499577 -0.3259699612694712 0.13510183305099147 ;
	setAttr ".r" -type "double3" -89.999999999858076 -88.060670781710982 89.999999999858076 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" 2.2204460492503131e-016 -1 0 0 1 2.2204460492503131e-016 8.3960588285190204e-014 0
		 -8.3960588285190204e-014 -1.8642995655058271e-029 1 0 11.145136555499452 21.950819439088733 -0.34347297829513029 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Pinky1_L_TmpJnt" -p "HandCup_L_TmpJnt";
	rename -uid "34C90712-454E-6E40-8020-54AB1F172A83";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".t" -type "double3" -0.44045160345318601 0.19692164003543286 0.13657982496343343 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" 0 0 1.8724610209345338 ;
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 11.876633548480955 22.020988418911262 -0.95907086085240945 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Pinky2_L_TmpJnt" -p "Pinky1_L_TmpJnt";
	rename -uid "E90C6F56-46E6-5676-F529-21AC4A58ED4B";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" 2.5716657744989863e-016 0.31099083325423305 -0.0011813382686174361 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 13.083100380346423 22.020988418911262 -0.95907086085230775 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Pinky3_L_TmpJnt" -p "Pinky2_L_TmpJnt";
	rename -uid "3A0EB2CC-47FD-FC50-F9D7-6391765F7EEF";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" -5.2368313847524398e-016 0.38384594935658178 -0.042519961286512142 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 13.601293073636691 22.020988418911262 -0.95907086085226423 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Pinky4_L_TmpJnt" -p "Pinky3_L_TmpJnt";
	rename -uid "A786D6FF-498B-7FDF-01FE-7D96748D1E9C";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" -7.531256105771883e-016 0.22457297228422224 -0.018101266002036098 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 13.978441565540829 22.020988418911262 -0.95907086085223248 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Pinky5_L_TmpJnt" -p "Pinky4_L_TmpJnt";
	rename -uid "14A05881-449B-A3DC-0157-5BB74C567398";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 5.2735593669694916e-016 0.24720738706205425 -0.014196892786332072 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 14.437677801786013 22.020988418911262 -0.95907086085219373 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Middle1_L_TmpJnt" -p "HandCup_L_TmpJnt";
	rename -uid "7ABB3971-4911-3053-261F-54B3341309E2";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".t" -type "double3" -0.1253012654906516 0.21995595040992874 0.157134183679252 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 11.876633548480955 22.169748485139639 -0.10080869585328409 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Middle2_L_TmpJnt" -p "Middle1_L_TmpJnt";
	rename -uid "31A0240B-49D0-A2ED-00CC-4B96D0CA82B5";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" 2.1351774241914102e-016 0.36531174709005132 0.0023031845772010233 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 13.108601055533605 22.169748485139639 -0.10080869585318036 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Middle3_L_TmpJnt" -p "Middle2_L_TmpJnt";
	rename -uid "0E891FE6-4D76-606C-485D-34AD4F3F8859";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 3.2914565822440847e-015 0.45542758279325568 -0.041169530829726853 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 13.906913312889571 22.169748485139639 -0.10080869585311319 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Middle4_L_TmpJnt" -p "Middle3_L_TmpJnt";
	rename -uid "ABB37D52-4F0C-5915-A949-A59F28A77448";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 5.5358745543440652e-016 0.29127838065553835 -0.018550485395048 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 14.411849725499213 22.169748485139639 -0.10080869585307067 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Middle5_L_TmpJnt" -p "Middle4_L_TmpJnt";
	rename -uid "0A286703-4B3E-EE02-6433-0E9B281C9760";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 1.1796119636642214e-016 0.34346430910569375 -0.0043640884736220187 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 15.081996667416741 22.169748485139635 -0.10080869585301426 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Elbow_L_TmpJnt" -p "Forearm_L_TmpJnt";
	rename -uid "E4DBCB47-431E-B951-A27A-BCB3050073C3";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 4.5705661477768444e-012 8.1557710363000524e-008 -2.5429481496776059 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" 0 5.9496752756331688e-005 -90.000000000000014 ;
	setAttr ".bps" -type "matrix" -1 -1.2246467991473532e-016 0 0 1.2246467991473532e-016 -1 0 0
		 0 0 1 0 1.3486489690923307 14.656585133659888 1.5144370407379352e-017 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "ShoulderArmor_L_TmpJnt" -p "Clav_L_TmpJnt";
	rename -uid "98E7FDE5-4AD4-A5A4-75C5-7E8A812AFC2E";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" -0.20528613543957874 0.53042158327933975 -0.19587195618162517 ;
	setAttr ".ro" 4;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" -4.3311767654220716 0 0 ;
	setAttr ".bps" -type "matrix" 2.2204460492503131e-016 -1 0 0 1 2.2204460492503131e-016 0 0
		 0 0 1 0 2.6840862727483024 21.950819439051291 -0.34347723857962076 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Neck_TmpJnt" -p "Spine5_TmpJnt";
	rename -uid "DE6EADB7-4533-06E0-7560-5DBB5C39C6AE";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 0 1.5573644012619567 -0.3875671967307377 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 22.764810214551506 1.2852494451027699e-016 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Head_TmpJnt" -p "Neck_TmpJnt";
	rename -uid "7B5DB929-4D01-3911-9CA1-0A813F8005E5";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 0 0.96255186718179964 -0.073656514289546338 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 25.118764441136726 1.5379363139276727e-016 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "HeadEnd_TmpJnt" -p "Head_TmpJnt";
	rename -uid "C19FF5A0-40C9-63A0-B1CA-129724C7D26A";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" -4.0389678347315804e-028 2.6465350431800623 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 27.864768711571326 -1.9234515956468954e-015 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Eye_L_TmpJnt" -p "Head_TmpJnt";
	rename -uid "F8A1FC64-44CC-1334-A82A-2F9275597B22";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" 0.52633771300602172 0.94948719245062563 0.74093274325442637 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0.53833165295886931 25.828273426804412 1.4860476249829009 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "EyeDot_L_TmpJnt" -p "Eye_L_TmpJnt";
	rename -uid "9BED35CA-4B33-E09F-53D7-D494F5A34F4D";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" 0.15352907776832581 0.01448822021484375 0.36464214324951172 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 21.934863766566533 0 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0.53833165295886931 25.828273426804412 1.4860476249829009 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Pupil_L_TmpJnt" -p "Eye_L_TmpJnt";
	rename -uid "32D6D525-4E84-1790-8549-CC8364937F8E";
	setAttr ".t" -type "double3" 0.058746457099914551 -0.0077028274536132813 0.32149371504783641 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.1;
createNode joint -n "Iris_L_TmpJnt" -p "Pupil_L_TmpJnt";
	rename -uid "A0AAA24B-477D-F3BF-B1D9-D6BB7283452A";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.05;
createNode joint -n "Eye_R_TmpJnt" -p "Head_TmpJnt";
	rename -uid "9D957B26-49B3-B9C3-969E-8E908EA64917";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" -0.526 0.94948719245062563 0.74093274325442637 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -0.53833200000000003 25.828300000000006 1.4860499999999996 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "EyeDot_R_TmpJnt" -p "Eye_R_TmpJnt";
	rename -uid "8582FCB4-46AD-59F9-4EDF-0CB0F539E749";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" 0.02745149231243349 0.01448822021484375 0.39170169830322266 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 1.7356856264772427 0 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0.53833165295886931 25.828273426804412 1.4860476249829009 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Pupil_R_TmpJnt" -p "Eye_R_TmpJnt";
	rename -uid "9AE23422-4122-7B24-9D2B-CC8DC6FF64AA";
	setAttr ".t" -type "double3" -0.059083999997136871 -0.0077028274536132813 0.32149371504783641 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.1;
createNode joint -n "Iris_R_TmpJnt" -p "Pupil_R_TmpJnt";
	rename -uid "A8A4E857-4081-4E1F-99ED-BA8B873D613A";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.05;
createNode joint -n "JawUprRoot_TmpJnt" -p "Head_TmpJnt";
	rename -uid "AB633BFD-4507-E518-0041-C7B9085DFE05";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" -1.0760815779207081e-018 0.57635246149165464 1.2604807720449307 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" -13.293318598545932 1.7655625192200634e-031 180 ;
	setAttr ".bps" -type "matrix" -1 1.2246467991473532e-016 0 0 -1.2246467991473532e-016 -1 0 0
		 0 0 1 0 0 24.387496966804516 0.55021567872571864 1;
	setAttr -k on -cb off ".radi" 0.1;
createNode joint -n "JawUpr1_TmpJnt" -p "JawUprRoot_TmpJnt";
	rename -uid "80BC138C-4584-4FCD-B482-D7AE81640367";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -1 1.2246467991473532e-016 0 0 -1.2246467991473532e-016 -1 0 0
		 0 0 1 0 0 24.387496966804516 0.55021567872571864 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "TeethUpr_TmpJnt" -p "JawUpr1_TmpJnt";
	rename -uid "303BBDA5-45A4-FECB-1361-DB8C7E4C64EB";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".t" -type "double3" -2.7355308705863204e-017 0.2521312665366704 0.13064812193036457 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 8.9999999999999982 0 0 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 24.169010636166544 2.0776527139297967 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "JawUprEnd_TmpJnt" -p "JawUpr1_TmpJnt";
	rename -uid "EEB5C79E-443C-23FC-9EE7-F7A51ED0A764";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" -1.3328593854614215e-026 -3.5527136788005009e-015 1.3952723249138534 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" -1 1.2246467991473532e-016 0 0 -1.2246467991473532e-016 -1 0 0
		 0 0 1 0 -3.2163030071266839e-032 24.387496966804516 2.0776527139297962 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "JawLwr1_TmpJnt" -p "Head_TmpJnt";
	rename -uid "E98A02D1-452C-9C75-F4A7-548A572F0E65";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" -6.0584517520973707e-027 0.9199783271561266 0.076194824430387609 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" 30.811524785433054 0 0 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 25.105065775409837 0.43032997634803533 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "JawLwr2_TmpJnt" -p "JawLwr1_TmpJnt";
	rename -uid "23BE29EF-4304-8F4C-1A5C-618C90222F19";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" 3.6350710512584224e-027 -0.49146653045920169 0.55799105865172061 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" -23.761930192199674 0 0 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 24.169010636166544 0.55021567872571864 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "JawLwrEnd_TmpJnt" -p "JawLwr2_TmpJnt";
	rename -uid "1E284DC0-4584-8E48-A0D3-F293A6C32EAB";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".t" -type "double3" 0 -0.047276548850481888 1.6057358672749762 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 24.169010636166544 2.0776527139297967 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "TeethLwr_TmpJnt" -p "JawLwr2_TmpJnt";
	rename -uid "92322076-4D58-6C4E-AF4B-34AD4AAFAB18";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".t" -type "double3" -4.0389678347316163e-028 -0.0001109944409127539 0.9885288637082138 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 24.169010636166544 2.0776527139297967 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "EyeTrgt_TmpJnt" -p "Head_TmpJnt";
	rename -uid "8ACF2328-4F9D-AA12-242F-A2BD3210872C";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" 8.2229238859917653e-006 0.94948719245062563 5.5420865335902505 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -0.53833200000000003 25.828300000000006 1.4860499999999996 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "EyeTrgt_L_TmpJnt" -p "EyeTrgt_TmpJnt";
	rename -uid "C2DA37C0-4AAF-788A-5E2A-D986B5D015FB";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" 0.52632949008213581 0 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0.53833165295886931 25.828273426804412 1.4860476249829009 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "EyeTrgt_R_TmpJnt" -p "EyeTrgt_TmpJnt";
	rename -uid "451B9081-4D27-3DC2-454A-4B9422E44931";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" -0.52600822292388594 0 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -0.53833200000000003 25.828300000000006 1.4860499999999996 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Fur1_L_TmpJnt" -p "Head_TmpJnt";
	rename -uid "52F249A3-4A94-DA42-87BF-3CA6F67FB4C1";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" 1.4790830558606927 0.6608397556153438 -0.15392187663412502 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -97.127028926986952 -16.072740501214355 3.2448065005504425 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 27.864768711571326 -1.9234515956468954e-015 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Fur2_L_TmpJnt" -p "Fur1_L_TmpJnt";
	rename -uid "97A5E77C-4EA6-EB83-9640-FFB3C1C96120";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" 1.1102230246251565e-015 0.54595481486183439 -7.1054273576010019e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 27.864768711571326 -1.9234515956468954e-015 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Fur3_L_TmpJnt" -p "Fur2_L_TmpJnt";
	rename -uid "2CE6B9A8-4AA7-868F-C7E5-D2A2B55CC1B9";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" 0 0.54595481486183528 5.3290705182007514e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 27.864768711571326 -1.9234515956468954e-015 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Fur1_R_TmpJnt" -p "Head_TmpJnt";
	rename -uid "88B44C0B-4C3B-066C-FC10-798E2909F0FA";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" -1.4790799999971367 0.66082489997015692 -0.15392195608639389 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 82.872971073013019 16.072740501214358 -3.2448065005504607 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 27.864768711571326 -1.9234515956468954e-015 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Fur2_R_TmpJnt" -p "Fur1_R_TmpJnt";
	rename -uid "8FA23217-4D17-74E2-EE1B-3B939423785C";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" -4.7321767846497664e-006 -0.54595085725416315 -3.7961179815582113e-005 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 27.864768711571326 -1.9234515956468954e-015 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Fur3_R_TmpJnt" -p "Fur2_R_TmpJnt";
	rename -uid "4FA83FC9-44F9-8F22-1DDF-1D95A2F49C1C";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" 4.2994185944777996e-007 -0.54596073586688254 6.1420737758055566e-005 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 27.864768711571326 -1.9234515956468954e-015 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Tongue1_TmpJnt" -p "Head_TmpJnt";
	rename -uid "2CF373A1-4773-F23A-9158-10A2156BDAF1";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" -1.6155871338926322e-027 -0.093391651932252628 0.022134295321902309 ;
	setAttr ".r" -type "double3" -8.6576709189818857 0 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" 56.710435076321076 0 0 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 25.105065775409837 0.43032997634803533 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Tongue2_TmpJnt" -p "Tongue1_TmpJnt";
	rename -uid "7B390607-48B1-0962-DA83-3187FCC9179B";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" -8.0779356694631609e-028 0.3113101307378775 -1.7763568394002416e-015 ;
	setAttr ".r" -type "double3" 17.833148627704674 0 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 24.169010636166544 0.55021567872571864 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Tongue3_TmpJnt" -p "Tongue2_TmpJnt";
	rename -uid "9D2CF8B6-4048-39E3-4DCB-F5B5F11594B7";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".t" -type "double3" -1.1658803223182478e-042 0.33066658742938221 0.012871713850290116 ;
	setAttr ".r" -type "double3" 23.253698580381954 0 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 24.169010636166544 2.0776527139297967 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Tongue4_TmpJnt" -p "Tongue3_TmpJnt";
	rename -uid "0388554E-47C9-9CA0-4F5B-F6B48DB5E2A7";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".t" -type "double3" 4.0389678347315804e-028 0.38535795949086826 0.0075285315478293077 ;
	setAttr ".r" -type "double3" 8.5340568518517514 0 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 24.169010636166544 2.0776527139297967 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Tongue5_TmpJnt" -p "Tongue4_TmpJnt";
	rename -uid "737A3D20-47D7-4BF7-1E08-9AAECECFC8D1";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".t" -type "double3" -4.0389678347315804e-028 0.38116932054281732 2.0022503620376061e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 24.169010636166544 2.0776527139297967 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Tongue6_TmpJnt" -p "Tongue5_TmpJnt";
	rename -uid "9BE6A535-490B-5920-ACC3-BFB1DC2D5044";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".t" -type "double3" 4.0389678347315804e-028 0.37687721380316075 -4.8635547629169456e-015 ;
	setAttr ".r" -type "double3" -3.1805546814635168e-015 0 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 24.169010636166544 2.0776527139297967 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "HoodA1_TmpJnt" -p "Spine5_TmpJnt";
	rename -uid "C18350DB-4A5D-F4D4-7CAB-BB80609F36C1";
	setAttr ".t" -type "double3" 2.8632053254437986e-012 1.5996777814552274 -0.96703844572572395 ;
	setAttr ".ssc" no;
	setAttr ".radi" 0.05;
createNode joint -n "HoodA2_TmpJnt" -p "HoodA1_TmpJnt";
	rename -uid "360AC4C0-4BE1-2CB0-E99B-1BA208236E65";
	setAttr ".t" -type "double3" 0 1.2917270769731228 0 ;
	setAttr ".ssc" no;
	setAttr ".radi" 0.05;
createNode joint -n "HoodA3_TmpJnt" -p "HoodA2_TmpJnt";
	rename -uid "757FE555-49A3-26C2-E5D7-F5B8770B26E0";
	setAttr ".t" -type "double3" 0 1.2122119200990511 0 ;
	setAttr ".ssc" no;
	setAttr ".radi" 0.05;
createNode joint -n "HoodA4_TmpJnt" -p "HoodA3_TmpJnt";
	rename -uid "B6A7805B-4BEB-C5E5-DB42-4BA805E466D5";
	setAttr ".t" -type "double3" 0 1.3036996121819975 0 ;
	setAttr ".ssc" no;
	setAttr ".radi" 0.05;
createNode joint -n "Ear1_L_TmpJnt" -p "HoodA4_TmpJnt";
	rename -uid "FA85107E-4D1F-877A-40B7-AF9C02E93472";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" 0.74218401605782469 -1.0734378215752454 -0.069134550088930879 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -32.368376506802655 -22.445478615076592 -32.232402985850193 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 27.864768711571326 -1.9234515956468954e-015 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Ear2_L_TmpJnt" -p "Ear1_L_TmpJnt";
	rename -uid "E242297D-4893-3890-BFE5-608C6C58D56F";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" -4.4408920985006262e-015 0.39198977077529484 -8.8817841970012523e-016 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 27.864768711571326 -1.9234515956468954e-015 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Ear3_L_TmpJnt" -p "Ear2_L_TmpJnt";
	rename -uid "0568AF25-458F-D6EC-8FEC-3E9E2D02A6E5";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" 3.5527136788005009e-015 0.35961471179645166 8.8817841970012523e-016 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 27.864768711571326 -1.9234515956468954e-015 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Ear4_L_TmpJnt" -p "Ear3_L_TmpJnt";
	rename -uid "F55BA0C1-43D3-1A6D-0B5A-97B6E3243D4A";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" 3.5527136788005009e-015 0.35521361612564917 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 27.864768711571326 -1.9234515956468954e-015 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Ear1_R_TmpJnt" -p "HoodA4_TmpJnt";
	rename -uid "6A67C110-4098-FE76-4F0B-F8ABD2E58654";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" -0.742184 -1.0734752222954853 -0.069134221380954042 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 147.63162349319734 22.44547861507662 32.232402985850186 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 27.864768711571326 -1.9234515956468954e-015 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Ear2_R_TmpJnt" -p "Ear1_R_TmpJnt";
	rename -uid "474648B5-4D14-9402-4519-BABE22ADE3B6";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" 3.6386773124341687e-005 -0.39203413201189541 -4.5811380920746103e-005 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 27.864768711571326 -1.9234515956468954e-015 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Ear3_R_TmpJnt" -p "Ear2_R_TmpJnt";
	rename -uid "3CDD93DF-407C-1B2B-A671-AF907C919DA8";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" -1.1137991372756062e-005 -0.3595945337534765 1.2311155112243455e-005 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 27.864768711571326 -1.9234515956468954e-015 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Ear4_R_TmpJnt" -p "Ear3_R_TmpJnt";
	rename -uid "679E96E0-4892-EC97-090E-8988237E1BAB";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" 1.9218589230263206e-005 -0.35524467036791485 -1.7488256572661953e-005 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 27.864768711571326 -1.9234515956468954e-015 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "HoodHairBck_TmpJnt" -p "HoodA2_TmpJnt";
	rename -uid "A61EA6AC-473F-E406-A974-3BA746561AFC";
	setAttr ".t" -type "double3" 0 0.12741711467282713 -1.7166794492090678 ;
	setAttr ".r" -type "double3" -90.000000000000028 0 0 ;
	setAttr ".radi" 0.05;
createNode joint -n "HoodBA_TmpJnt" -p "Spine5_TmpJnt";
	rename -uid "404E9107-4131-1B0A-9C2D-DA98DD44CAEC";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 0 1.5955625840746563 -0.77296894537563743 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" -114.74762169600731 0 0 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 22.764810214551506 1.2852494451027699e-016 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "HoodBA1_TmpJnt" -p "HoodBA_TmpJnt";
	rename -uid "FFB52021-404E-69BD-704B-30BC8505F9BC";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 1.2116903504194741e-027 -1.1990408665951691e-014 0.63473506735277496 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 22.764810214551506 1.2852494451027699e-016 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "HoodBB_TmpJnt" -p "HoodBA_TmpJnt";
	rename -uid "ED0BC7E9-4020-D5A8-B8D2-3FB5D7B56264";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 4.0389678347315804e-028 0.65300824661809598 -0.023151935554327707 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" -15.237306582021269 0 0 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 22.764810214551506 1.2852494451027699e-016 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "HoodBB1_TmpJnt" -p "HoodBB_TmpJnt";
	rename -uid "549FACE7-4EDE-55E0-84A0-8CB83D617D43";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 2.4233807008389483e-027 8.8817841970012523e-016 0.84560281147746075 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 22.764810214551506 1.2852494451027699e-016 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "HoodBC_TmpJnt" -p "HoodBB_TmpJnt";
	rename -uid "51BFCB0B-4EC6-4855-9306-0285B3C64ACC";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 3.2311742677852644e-027 0.62397159737856267 -0.12420362276030694 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" -18.810135659668063 0 0 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 22.764810214551506 1.2852494451027699e-016 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "HoodBC1_TmpJnt" -p "HoodBC_TmpJnt";
	rename -uid "67806AAC-492A-4FA8-23CD-A19688FD937B";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" -1.6155871338926322e-027 -9.7699626167013776e-015 0.92112536493940311 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 22.764810214551506 1.2852494451027699e-016 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "HoodBD_TmpJnt" -p "HoodBC_TmpJnt";
	rename -uid "E2418BEE-49D3-EA91-1428-49A1D459A6D7";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" -8.0779356694631609e-028 0.69200502739072078 -0.10018760491725676 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" -12.651004474280084 0 0 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 22.764810214551506 1.2852494451027699e-016 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Clav_R_TmpJnt" -p "Spine5_TmpJnt";
	rename -uid "D1A36EBA-4BEB-6885-80D4-5CBC28FD228A";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".ove" yes;
	setAttr ".t" -type "double3" -0.24727599999713679 1.3156411684139133 -0.19087766710667795 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" -180 7.0622500768802503e-031 89.999999999999972 ;
	setAttr ".bps" -type "matrix" 2.2204460492503131e-016 -1 0 0 1 2.2204460492503131e-016 0 0
		 0 0 1 0 0.40396301701286447 22.628380391977608 0.93191360083425789 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "UpArm_R_TmpJnt" -p "Clav_R_TmpJnt";
	rename -uid "DA7D5AC0-4EE2-B32E-F16E-F394F79411DB";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" -0.23657000000000039 -0.62735499999999988 0.20160610000000007 ;
	setAttr ".ro" 4;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" -4.3311767654219304 9.6079876718733594e-016 -3.6332275256963662e-017 ;
	setAttr ".bps" -type "matrix" 2.2204460492503131e-016 -1 0 0 1 2.2204460492503131e-016 0 0
		 0 0 1 0 2.6840862727483024 21.950819439051291 -0.34347723857962076 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Forearm_R_TmpJnt" -p "UpArm_R_TmpJnt";
	rename -uid "96122D7E-4865-3D03-CEB6-40B097D1E955";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" -1.7763568394002505e-015 -1.8496613067512082 0.018140841127773418 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" 4.3311172686690211 -9.5529798408803886e-016 -1.0878731970750718e-016 ;
	setAttr ".bps" -type "matrix" 2.2204460492503131e-016 -1 0 0 0.99999999999946088 2.2204460492491157e-016 -1.0384142298428127e-006 0
		 1.0384142298428127e-006 2.3057427741397801e-022 0.99999999999946088 0 6.7891781465089913 21.950819439060187 -0.42637972484178432 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Wrist_R_TmpJnt" -p "Forearm_R_TmpJnt";
	rename -uid "B1B408A5-40B3-499D-9165-7DAFA82FA430";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" 1.7763568394002505e-015 -2.1739899809376961 -0.018358257502147118 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" 2.2204460492503131e-016 -1 0 0 1 2.2204460492503131e-016 8.3960588285190204e-014 0
		 -8.3960588285190204e-014 -1.8642995655058271e-029 1 0 11.145136555499452 21.950819439088733 -0.34347297829513029 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Hand_R_TmpJnt" -p "Wrist_R_TmpJnt";
	rename -uid "359B1736-46E4-90EF-90A1-BAB5C05ECE6F";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" -1.7763568394002505e-015 -0.59573000000071641 3.8138548930088945e-007 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 2.2204460492503131e-016 -1 0 0 1 2.2204460492503131e-016 8.3960588285190204e-014 0
		 -8.3960588285190204e-014 -1.8642995655058271e-029 1 0 11.94334900537585 21.950819439093973 -0.34347375639834526 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Index1_R_TmpJnt" -p "Hand_R_TmpJnt";
	rename -uid "2A79D148-4BE2-0B1F-16AE-AE95E34505E8";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".t" -type "double3" 0.038350000000001216 0.1242202720803478 -0.26201517100804272 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" 89.99999999999973 -87.241493305866157 -90.00000000000027 ;
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 11.915961054098767 22.155901831531715 0.36968006095532863 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Index2_R_TmpJnt" -p "Index1_R_TmpJnt";
	rename -uid "3F0C2B6B-4C3C-11D4-B323-65B9E0EA2F5A";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" -1.5498543279313104e-007 -0.36443227435277326 -0.0010100000000008436 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 13.114219270621863 22.155901831531715 0.36968006095542938 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Index3_R_TmpJnt" -p "Index2_R_TmpJnt";
	rename -uid "B5D741C4-48C9-C677-CBED-849C23B34F8C";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 2.5823682237025025e-007 -0.43246107929655775 0.010769999999991953 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 13.805785441300909 22.155901831531718 0.3696800609554875 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Index4_R_TmpJnt" -p "Index3_R_TmpJnt";
	rename -uid "6568482D-4464-110B-D063-42B3F2868220";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 1.0587006274431765e-007 -0.24742668767687537 0.03022000000000169 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 14.271394348292739 22.155901831531718 0.36968006095552658 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Index5_R_TmpJnt" -p "Index4_R_TmpJnt";
	rename -uid "6120B7D1-49C9-7E59-AEDB-88A3FB70847A";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" -3.3429028370446368e-007 -0.26974256622032744 0.019629999999997594 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 14.812322343180313 22.155901831531722 0.36968006095557193 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Thumb1_R_TmpJnt" -p "Hand_R_TmpJnt";
	rename -uid "B1C4DE73-4D2A-4449-9E5C-E9B20600A724";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".t" -type "double3" -0.077460000000000306 0.29942019465037717 -0.18744948907790943 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" 44.720367506143162 10.63719430264883 -77.290418940461706 ;
	setAttr ".bps" -type "matrix" -0.59548586752167076 -0.80336578317847407 -4.9997343752636337e-014 0
		 0.73868319246536318 -0.54754062333671982 0.39312390790336815 0 -0.31582229615899904 0.23409973134130962 0.91948550452673483 0
		 11.518645340649305 21.905429719636764 0.32567524419271171 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Thumb2_R_TmpJnt" -p "Thumb1_R_TmpJnt";
	rename -uid "E215B109-4DE9-80CE-9502-8D8E335F0604";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" 0.062880045102319926 -0.27919902610806613 -0.041967577517371346 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" -19.700796181147311 40.857685490686002 24.264632059267711 ;
	setAttr ".bps" -type "matrix" -0.59548586752167076 -0.80336578317847407 -4.9997343752636337e-014 0
		 0.73868319246536318 -0.54754062333671982 0.39312390790336815 0 -0.31582229615899904 0.23409973134130962 0.91948550452673483 0
		 12.026286103962779 21.529146703032435 0.59583939943602238 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Thumb3_R_TmpJnt" -p "Thumb2_R_TmpJnt";
	rename -uid "198D98F8-406F-3036-9775-A3A6CA6CB85E";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 2.4415272701361346e-006 -0.44156512313392282 0.011012729986912273 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -0.59548586752167076 -0.80336578317847407 -4.9997343752636337e-014 0
		 0.73868319246536318 -0.54754062333671982 0.39312390790336815 0 -0.31582229615899904 0.23409973134130962 0.91948550452673483 0
		 12.724953059079672 21.011267666374362 0.96766684389406166 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Thumb4_R_TmpJnt" -p "Thumb3_R_TmpJnt";
	rename -uid "0717E514-4C57-4C78-109F-5C8BF5A30F90";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 1.3990113600925724e-007 -0.30386041030147526 0.0076956839462125615 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" -0.59548586752167076 -0.80336578317847407 -4.9997343752636337e-014 0
		 0.73868319246536318 -0.54754062333671982 0.39312390790336815 0 -0.31582229615899904 0.23409973134130962 0.91948550452673483 0
		 13.320225968832865 20.570028305053697 1.2844684375111461 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "HandCup_R_TmpJnt" -p "Hand_R_TmpJnt";
	rename -uid "ADA8AE4E-4529-2CF2-B9D9-75B9D57712CA";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" -0.057330000000000325 0.32597014029270088 -0.13510266150803979 ;
	setAttr ".r" -type "double3" -89.999999999858076 -88.060670781710982 89.999999999858076 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" 2.2204460492503131e-016 -1 0 0 1 2.2204460492503131e-016 8.3960588285190204e-014 0
		 -8.3960588285190204e-014 -1.8642995655058271e-029 1 0 11.145136555499452 21.950819439088733 -0.34347297829513029 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Pinky1_R_TmpJnt" -p "HandCup_R_TmpJnt";
	rename -uid "111C9CBE-4F09-CCB9-2242-5D81160FD099";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".t" -type "double3" 0.44045212417052626 -0.19691869085915048 -0.13657999999996306 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" 1.3821474857550912e-017 8.4577642388635635e-016 1.8724610209342865 ;
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 11.876633548480955 22.020988418911262 -0.95907086085240945 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Pinky2_R_TmpJnt" -p "Pinky1_R_TmpJnt";
	rename -uid "833FE03E-4C93-8237-1FA4-DC8E83D274DA";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" 4.3648346198521537e-007 -0.31098801524304598 0.001190000000002911 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 13.083100380346423 22.020988418911262 -0.95907086085230775 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Pinky3_R_TmpJnt" -p "Pinky2_R_TmpJnt";
	rename -uid "40F4D344-405C-4681-62EB-F29D49B887B7";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" -3.9247877747783377e-007 -0.38384914787432667 0.042520000000001446 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 13.601293073636691 22.020988418911262 -0.95907086085226423 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Pinky4_R_TmpJnt" -p "Pinky3_R_TmpJnt";
	rename -uid "7B0CBE57-4092-A94D-C746-1B840D064D25";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 7.4035234443048381e-008 -0.22457682716610528 0.018100000000000449 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 13.978441565540829 22.020988418911262 -0.95907086085223248 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Pinky5_R_TmpJnt" -p "Pinky4_R_TmpJnt";
	rename -uid "C9B0A525-4CDD-CAE1-9819-718392C0FDE3";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" -3.7750102321876255e-007 -0.24720686065693442 0.014190000000002811 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 14.437677801786013 22.020988418911262 -0.95907086085219373 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Middle1_R_TmpJnt" -p "HandCup_R_TmpJnt";
	rename -uid "F890022A-499B-990A-42DF-C9BF3EDCBD2A";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".t" -type "double3" 0.12530195076208125 -0.21995649458749877 -0.15712999999998978 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 11.876633548480955 22.169748485139639 -0.10080869585328409 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Middle2_R_TmpJnt" -p "Middle1_R_TmpJnt";
	rename -uid "7A6D68B2-4610-84CB-03BC-2F9714104D8E";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" 1.1089723568613596e-007 -0.3653092577104875 -0.0022999999999999687 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 13.108601055533605 22.169748485139639 -0.10080869585318036 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Middle3_R_TmpJnt" -p "Middle2_R_TmpJnt";
	rename -uid "84E3FD20-4CA5-D2CC-5A50-7DA1EBD616E4";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" -8.0754586100073578e-007 -0.45543084946383328 0.041170000000001039 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 13.906913312889571 22.169748485139639 -0.10080869585311319 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Middle4_R_TmpJnt" -p "Middle3_R_TmpJnt";
	rename -uid "0A555C92-4ADB-621A-DCBD-44B46B176EA7";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 5.3426458392069609e-007 -0.29127686530810237 0.018550000000002953 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 14.411849725499213 22.169748485139639 -0.10080869585307067 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Middle5_R_TmpJnt" -p "Middle4_R_TmpJnt";
	rename -uid "F2D220CB-4B16-4538-7C9D-20B70B830748";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" -3.5036505686980579e-007 -0.34345672468140354 0.0043599999999983652 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" -8.3960588285190191e-014 -8.437694987151191e-014 1 0
		 1 2.2204460492503131e-016 8.3960588285190204e-014 0 -2.2204460493211562e-016 1 8.4376949871511897e-014 0
		 15.081996667416741 22.169748485139635 -0.10080869585301426 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Elbow_R_TmpJnt" -p "Forearm_R_TmpJnt";
	rename -uid "90ACD1D8-4932-7CF7-DAE2-939E840F0938";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 1.7763568394002505e-015 -2.6406385873478655e-006 2.5429529999986289 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" -1.3784766372981863e-008 5.9496752892300412e-005 -90.000000000000057 ;
	setAttr ".bps" -type "matrix" -1 -1.2246467991473532e-016 0 0 1.2246467991473532e-016 -1 0 0
		 0 0 1 0 1.3486489690923307 14.656585133659888 1.5144370407379352e-017 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "ShoulderArmor_R_TmpJnt" -p "Clav_R_TmpJnt";
	rename -uid "670B0A4D-452E-8751-E019-4CBEC0411974";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 0.20529999999999937 -0.530421 0.19587180000000007 ;
	setAttr ".ro" 4;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" -4.3311767654219304 9.6079876718733594e-016 -3.6332275256963662e-017 ;
	setAttr ".bps" -type "matrix" 2.2204460492503131e-016 -1 0 0 1 2.2204460492503131e-016 0 0
		 0 0 1 0 2.6840862727483024 21.950819439051291 -0.34347723857962076 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Pelvis_TmpJnt" -p "Root_TmpJnt";
	rename -uid "6CDDD88C-4FC8-177D-5AD0-D3BE0FB60E5E";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" 0 -0.11275621285083925 -0.019370769017112766 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 15.4830611404753 0 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "UpLeg_L_TmpJnt" -p "Pelvis_TmpJnt";
	rename -uid "0697CCED-47D6-2F01-C7F1-07AFD188EAB6";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 0.64318041241209223 -0.61701708694347612 -0.039871990417926995 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" 0 0 -180 ;
	setAttr ".bps" -type "matrix" -1 -1.2246467991473532e-016 0 0 1.2246467991473532e-016 -1 0 0
		 0 0 1 0 1.3486489690923307 14.656585133659888 1.5144370407379352e-017 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "LowLeg_L_TmpJnt" -p "UpLeg_L_TmpJnt";
	rename -uid "D82D1A97-40C1-5AFF-21B4-7F92D799CE11";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 1.1704896756004554e-015 3.3342532066134507 -0.21262924734644481 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -1 -1.2246467991473532e-016 0 0 1.2246467991473532e-016 -1 0 0
		 0 0 1 0 1.3486489690923305 7.7535119809750004 0.15259281481888803 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Knee_L_TmpJnt" -p "LowLeg_L_TmpJnt";
	rename -uid "11C4B0E8-44BF-8D34-2651-3BA02BB44A0D";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" -1.2212453270876722e-015 1.4361446840638337e-006 3.4511827551436451 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -1 -1.2246467991473532e-016 0 0 1.2246467991473532e-016 -1 0 0
		 0 0 1 0 1.3486489690923307 14.656585133659888 1.5144370407379352e-017 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Ankle_L_TmpJnt" -p "LowLeg_L_TmpJnt";
	rename -uid "129C736E-4CDE-2515-C7BE-6A95F0FB37DF";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" -2.187311079585457e-015 2.8248780686585806 -0.44307383114293764 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -1 -1.2246467991473532e-016 0 0 1.2246467991473532e-016 -1 0 0
		 0 0 1 0 1.348648969092332 1.0615915425310156 0 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Ball_L_TmpJnt" -p "Ankle_L_TmpJnt";
	rename -uid "0EF539A1-4077-27A2-3479-F0A865D45580";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 2.7755575615628914e-015 0.46696410323550352 1.0639842033220237 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" 89.999999999999986 0 0 ;
	setAttr ".bps" -type "matrix" -1 -1.2246467991473532e-016 0 0 2.7192621468937821e-032 -2.2204460492503131e-016 1 0
		 -1.2246467991473532e-016 1 2.2204460492503131e-016 0 1.3486489690923311 5.5511151231257827e-015 1.476681409489522 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Toe_L_TmpJnt" -p "Ball_L_TmpJnt";
	rename -uid "4C13365C-4F8F-65A5-BB8F-959DD71EA1FC";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" -1.1102230246251565e-015 0.83079981387991364 1.6306400674181987e-016 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" -1 -1.2246467991473532e-016 0 0 2.7192621468937821e-032 -2.2204460492503131e-016 1 0
		 -1.2246467991473532e-016 1 2.2204460492503131e-016 0 1.3486489690923309 5.0024078434460801e-015 3.3551152167720435 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "FootIn_L_TmpJnt" -p "Ankle_L_TmpJnt";
	rename -uid "C8BFC9B2-4AE1-1622-15F6-BE8ABF3FF65A";
	setAttr ".t" -type "double3" 0.34069780150494711 0.46198433862678723 0.86106195904330851 ;
	setAttr ".jo" -type "double3" 89.999999999999986 0 0 ;
	setAttr ".radi" 0.05;
createNode joint -n "FootOut_L_TmpJnt" -p "Ankle_L_TmpJnt";
	rename -uid "488A307C-4F97-ADC6-3ABA-F29B6BE8CD62";
	setAttr ".t" -type "double3" -0.40572428727338239 0.46198433862678717 0.86187475177116901 ;
	setAttr ".jo" -type "double3" 89.999999999999986 0 0 ;
	setAttr ".radi" 0.05;
createNode joint -n "Heel_L_TmpJnt" -p "Ankle_L_TmpJnt";
	rename -uid "0CDA8760-48BC-0DD9-EEE4-02BD0E6F5476";
	setAttr ".t" -type "double3" 2.55351295663786e-015 0.4619843386267849 -0.28631188297925947 ;
	setAttr ".jo" -type "double3" 89.999999999999986 0 0 ;
	setAttr ".radi" 0.05;
createNode joint -n "UpLeg_R_TmpJnt" -p "Pelvis_TmpJnt";
	rename -uid "69430AFF-4FEE-57E6-6834-3F9516FE5476";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" -0.64318 -0.61701755001567093 -0.039871754509501534 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" 0 180 0 ;
	setAttr ".bps" -type "matrix" -1 -1.2246467991473532e-016 0 0 1.2246467991473532e-016 -1 0 0
		 0 0 1 0 1.3486489690923307 14.656585133659888 1.5144370407379352e-017 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "LowLeg_R_TmpJnt" -p "UpLeg_R_TmpJnt";
	rename -uid "8503AA58-4044-72A3-CBA4-8BAFB4278AFC";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" -3.4666738998970245e-033 -3.3342486535174216 0.2126294999999998 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -1 -1.2246467991473532e-016 0 0 1.2246467991473532e-016 -1 0 0
		 0 0 1 0 1.3486489690923305 7.7535119809750004 0.15259281481888803 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Knee_R_TmpJnt" -p "LowLeg_R_TmpJnt";
	rename -uid "75892A1B-4FE2-76B6-2019-77923EC2A546";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" -4.4408920985006262e-016 -1.0000000000065512e-005 -3.4511854999999989 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -1 -1.2246467991473532e-016 0 0 1.2246467991473532e-016 -1 0 0
		 0 0 1 0 1.3486489690923307 14.656585133659888 1.5144370407379352e-017 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Ankle_R_TmpJnt" -p "LowLeg_R_TmpJnt";
	rename -uid "2B0F2D30-45BE-60A8-9737-1EBAA2DFA941";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 3.4666738998970245e-033 -2.8248825602788443 0.44307350000000034 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -1 -1.2246467991473532e-016 0 0 1.2246467991473532e-016 -1 0 0
		 0 0 1 0 1.348648969092332 1.0615915425310156 0 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Ball_R_TmpJnt" -p "Ankle_R_TmpJnt";
	rename -uid "853FCC69-4781-AD87-D172-D3ACB2926009";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" -1.1102230246251565e-016 -0.46696370000000004 -1.0639840000000003 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" 89.999999999999986 0 0 ;
	setAttr ".bps" -type "matrix" -1 -1.2246467991473532e-016 0 0 2.7192621468937821e-032 -2.2204460492503131e-016 1 0
		 -1.2246467991473532e-016 1 2.2204460492503131e-016 0 1.3486489690923311 5.5511151231257827e-015 1.476681409489522 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "Toe_R_TmpJnt" -p "Ball_R_TmpJnt";
	rename -uid "150E5531-4228-2F6C-C517-40BACB91C038";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" 4.4408920985006262e-016 -0.83079499999999951 -2.0383000842727483e-016 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" -1 -1.2246467991473532e-016 0 0 2.7192621468937821e-032 -2.2204460492503131e-016 1 0
		 -1.2246467991473532e-016 1 2.2204460492503131e-016 0 1.3486489690923309 5.0024078434460801e-015 3.3551152167720435 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "FootIn_R_TmpJnt" -p "Ankle_R_TmpJnt";
	rename -uid "C3DA7CC4-406D-3DCE-FF3C-138C018FA717";
	setAttr ".t" -type "double3" -0.34069699999999981 -0.46198393700000001 -0.861062 ;
	setAttr ".jo" -type "double3" 89.999999999999986 0 0 ;
	setAttr ".radi" 0.05;
createNode joint -n "FootOut_R_TmpJnt" -p "Ankle_R_TmpJnt";
	rename -uid "CD86B615-4A2D-0B52-E450-D9A0DC7ABEE4";
	setAttr ".t" -type "double3" 0.40572000000000019 -0.46198393700000001 -0.86187400000000025 ;
	setAttr ".jo" -type "double3" 89.999999999999986 0 0 ;
	setAttr ".radi" 0.05;
createNode joint -n "Heel_R_TmpJnt" -p "Ankle_R_TmpJnt";
	rename -uid "1D1217EF-422D-148A-790C-8FADE8ABC34D";
	setAttr ".t" -type "double3" 5.5511151231257827e-016 -0.46198393700000001 0.28631199999999973 ;
	setAttr ".jo" -type "double3" 89.999999999999986 0 0 ;
	setAttr ".radi" 0.05;
createNode transform -n "Tail_CuvTemp" -p "TmpJnt_Grp";
	rename -uid "FE88F80F-45AB-A69C-0626-E088810C19F6";
createNode nurbsCurve -n "Tail_CuvTempShape" -p "Tail_CuvTemp";
	rename -uid "BEAB60E8-434E-1D13-AEA7-63A9DE41B6AF";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 6.5828146934509277 -0.5050809383392334
		0 6.5828146934509277 -8.4936764759785781
		;
createNode transform -n "TailUpvec_CuvTemp" -p "TmpJnt_Grp";
	rename -uid "213281DD-4F4E-A7CF-C82F-029977ED6807";
createNode nurbsCurve -n "TailUpvec_CuvTempShape" -p "TailUpvec_CuvTemp";
	rename -uid "ED240DF5-4DC4-0280-BC0C-36B4CD245920";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 7.3151828183438177 -0.5050809383392334
		0 7.3151828183438177 -8.4936764759785781
		;
createNode transform -n "PonyTail_CuvTemp" -p "TmpJnt_Grp";
	rename -uid "108FFF30-4CF2-E204-42FB-F0BEFDA22996";
createNode nurbsCurve -n "PonyTail_CuvTempShape" -p "PonyTail_CuvTemp";
	rename -uid "CC2CD98E-4666-E577-1563-14B73D633C0C";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 0 no 3
		13 0 0 0 0.125 0.25 0.37499999999999994 0.5 0.625 0.74999999999999989 0.875
		 1 1 1
		11
		8.1973056693888157e-017 12.573490641685435 -1.4581650733773193
		8.1973056693888169e-017 12.372352173235372 -1.650868161324089
		3.7575249063741572e-017 11.97007524532423 -2.0362743286056175
		1.3352195012382753e-017 11.366659844029865 -2.6143835885601807
		-2.8065682754314291e-017 10.763244447032882 -3.1924928443976119
		-6.2727850468429159e-017 10.159829048289454 -3.7706021019082057
		-9.7390018182544642e-017 9.5564136495460801 -4.3487113594187718
		-1.3880789594924093e-016 8.9529982525490599 -4.9268206152562266
		-1.4309837557888974e-016 8.5123446731578802 -5.3489939872512426
		-2.2867659624139409e-016 8.3183519729918967 -5.7631046848076295
		-1.5755626603930131e-016 8.3166601867211618 -6.2608587524215533
		;
createNode transform -n "PonyTailUpvec_CuvTemp" -p "TmpJnt_Grp";
	rename -uid "5E45B693-429A-3DC9-7657-AB944FD5212A";
createNode nurbsCurve -n "PonyTailUpvec_CuvTempShape" -p "PonyTailUpvec_CuvTemp";
	rename -uid "266D4732-4099-A0B5-2038-A5B6E4DE6A6C";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 0 no 3
		13 0 0 0 0.125 0.25 0.37499999999999994 0.5 0.625 0.74999999999999989 0.875
		 1 1 1
		11
		0.64938860637281082 12.573490641685435 -1.4581650733773193
		0.64938860637281082 12.372352173235372 -1.650868161324089
		0.64938860637281082 11.97007524532423 -2.0362743286056175
		0.64938860637281082 11.366659844029865 -2.6143835885601807
		0.6493886063728106 10.763244447032882 -3.1924928443976119
		0.6493886063728106 10.159829048289454 -3.7706021019082057
		0.6493886063728106 9.5564136495460801 -4.3487113594187718
		0.6493886063728106 8.9529982525490599 -4.9268206152562266
		0.6493886063728106 8.5123446731578802 -5.3489939872512426
		0.64938860637281048 8.3183519729918967 -5.7631046848076295
		0.6493886063728106 8.3166601867211618 -6.2608587524215533
		;
createNode joint -n "Belt_TmpJnt" -p "TmpJnt_Grp";
	rename -uid "666E9ACA-4A0C-3A34-94BF-13B683ECB2DD";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 7.2382237638119369 0.34335575450950151 ;
	setAttr ".radi" 0.05;
createNode joint -n "BeltFnt_TmpJnt" -p "Belt_TmpJnt";
	rename -uid "ED4A8CF5-4FC0-692C-F767-259B696ADEB0";
	setAttr ".t" -type "double3" 0 -0.035566382841806465 0.8047352827232005 ;
	setAttr ".r" -type "double3" 90.000000000000028 0 0 ;
	setAttr ".radi" 0.05;
createNode joint -n "BeltBck_TmpJnt" -p "Belt_TmpJnt";
	rename -uid "D679A336-44C5-1195-6053-67975DE2C002";
	setAttr ".t" -type "double3" 1.9354263733757475e-015 0.23887633920601292 -0.67507717347538043 ;
	setAttr ".r" -type "double3" 89.999999999999957 179.99999999999986 0 ;
	setAttr ".radi" 0.05;
createNode joint -n "Belt1_L_TmpJnt" -p "Belt_TmpJnt";
	rename -uid "34407DDB-4A08-D268-EBCF-28BECF9C5D88";
	setAttr ".t" -type "double3" 0.6049172912866142 0.01521622010814383 0.5602136293469907 ;
	setAttr ".r" -type "double3" 90.000000000000085 45.000000000000007 4.4979835663949442e-015 ;
	setAttr ".radi" 0.05;
createNode joint -n "Belt2_L_TmpJnt" -p "Belt_TmpJnt";
	rename -uid "F46395A4-4D0C-8D35-0A03-26B53D494879";
	setAttr ".t" -type "double3" 0.7981973993064837 0.13362068232845559 -0.047293134154829219 ;
	setAttr ".r" -type "double3" 89.999999999999957 89.999999999999929 0 ;
	setAttr ".radi" 0.05;
createNode joint -n "Belt3_L_TmpJnt" -p "Belt_TmpJnt";
	rename -uid "8DE14DCE-4428-CE6E-BE30-3AA5532FC5B6";
	setAttr ".t" -type "double3" 0.47460424591535011 0.21298423786109577 -0.47963545370413091 ;
	setAttr ".r" -type "double3" 89.999999999999972 134.99999999999989 0 ;
	setAttr ".radi" 0.05;
createNode joint -n "Belt1_R_TmpJnt" -p "Belt_TmpJnt";
	rename -uid "547FB9FA-4659-7AA2-AF7E-3D85570E50A5";
	setAttr ".t" -type "double3" -0.604917 0.015216236188063448 0.56021324549049845 ;
	setAttr ".r" -type "double3" 90.000000000000085 45.000000000000007 4.4979835663949442e-015 ;
	setAttr ".jo" -type "double3" -180 0 0 ;
	setAttr ".radi" 0.05;
createNode joint -n "Belt2_R_TmpJnt" -p "Belt_TmpJnt";
	rename -uid "B8514BF3-4032-BC4A-39C4-0881AA2E1D87";
	setAttr ".t" -type "double3" -0.798197 0.13361623618806284 -0.047292754509501489 ;
	setAttr ".r" -type "double3" 89.999999999999957 89.999999999999929 0 ;
	setAttr ".jo" -type "double3" -180 0 0 ;
	setAttr ".radi" 0.05;
createNode joint -n "Belt3_R_TmpJnt" -p "Belt_TmpJnt";
	rename -uid "C6E491A1-4A94-34F3-1BE0-59B71F435B85";
	setAttr ".t" -type "double3" -0.474604 0.21298623618806278 -0.47963575450950152 ;
	setAttr ".r" -type "double3" 89.999999999999972 134.99999999999989 0 ;
	setAttr ".jo" -type "double3" -180 0 0 ;
	setAttr ".radi" 0.05;
createNode joint -n "ClothLwrA1_L_TmpJnt" -p "Belt_TmpJnt";
	rename -uid "A78AA58D-4707-6415-E5B1-FA907B6D7B79";
	setAttr ".t" -type "double3" 0.60299506946728187 -0.1720161575189243 0.49343349606883502 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 12.448999914258398 -45.122997277184531 -178.31846014157341 ;
	setAttr ".radi" 0.05;
createNode joint -n "ClothLwrA2_L_TmpJnt" -p "ClothLwrA1_L_TmpJnt";
	rename -uid "00F6F2A5-4643-6EB7-15FA-0AA1C71FB620";
	setAttr ".t" -type "double3" 3.3653635433950058e-016 0.5989313616699663 1.3322676295501878e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -2.1040114660384623e-014 3.5781240166464552e-015 2.9817700138720455e-016 ;
	setAttr ".radi" 0.05;
createNode joint -n "ClothLwrA3_L_TmpJnt" -p "ClothLwrA2_L_TmpJnt";
	rename -uid "968C1BD1-4A12-BE92-A290-B498C232655B";
	setAttr ".t" -type "double3" 1.1449174941446927e-016 0.022728582787434171 4.4408920985006262e-016 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -7.9513867036587899e-016 -6.361109362927032e-015 -7.9513867036587899e-016 ;
	setAttr ".radi" 0.05;
createNode joint -n "ClothLwrB1_L_TmpJnt" -p "Belt_TmpJnt";
	rename -uid "A950965F-4562-9145-7DFE-E18E59E322A4";
	setAttr ".t" -type "double3" 0.89007431090208733 -0.052534692048999077 0.07867195660531856 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -24.081256863001926 -85.607855286770899 -138.27623686088845 ;
	setAttr ".radi" 0.05;
createNode joint -n "ClothLwrB2_L_TmpJnt" -p "ClothLwrB1_L_TmpJnt";
	rename -uid "8E2EC785-44DE-3617-8185-EFA394A3D3BA";
	setAttr ".t" -type "double3" 4.1720099597242211e-016 0.76996396689251778 -3.1086244689504383e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -7.5538173684758519e-015 0 0 ;
	setAttr ".radi" 0.05;
createNode joint -n "ClothLwrB3_L_TmpJnt" -p "ClothLwrB2_L_TmpJnt";
	rename -uid "C8FDD08A-41D1-DC3E-68E5-E79534D6BAAF";
	setAttr ".t" -type "double3" 8.4134088584875144e-017 0.022728582787435947 1.7763568394002505e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -8.6968292071268034e-016 0 1.9878466759146985e-016 ;
	setAttr ".radi" 0.05;
createNode joint -n "ClothLwrC1_L_TmpJnt" -p "Belt_TmpJnt";
	rename -uid "A87F40C7-4783-1C81-5CFC-439CFFC12080";
	setAttr ".t" -type "double3" 0.79012126047905029 0.012422001377025238 -0.35122171193184987 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -157.46052796494604 -50.607745877499006 -3.3497735304007024 ;
	setAttr ".radi" 0.05;
createNode joint -n "ClothLwrC2_L_TmpJnt" -p "ClothLwrC1_L_TmpJnt";
	rename -uid "B20CD816-4C73-EA5B-0CBF-5987FAC1A3B3";
	setAttr ".t" -type "double3" -1.9428902930940239e-016 0.81548559466098869 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.05;
createNode joint -n "ClothLwrC3_L_TmpJnt" -p "ClothLwrC2_L_TmpJnt";
	rename -uid "1C8DF67A-42E5-F431-B08D-AE88F4B58B27";
	setAttr ".t" -type "double3" -2.2204460492503131e-016 0.022728582787435947 8.8817841970012523e-016 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 3.1805546814635168e-015 0 -1.5902773407317584e-015 ;
	setAttr ".radi" 0.05;
createNode joint -n "ClothLwrD1_L_TmpJnt" -p "Belt_TmpJnt";
	rename -uid "F1604EBD-4E9E-05A2-F60F-DE9C6230BB09";
	setAttr ".t" -type "double3" 0.43692300280367169 0.059149912828475593 -0.6661406685387109 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -154.48546693287594 -24.94173370098035 -4.3926685937890557 ;
	setAttr ".radi" 0.05;
createNode joint -n "ClothLwrD2_L_TmpJnt" -p "ClothLwrD1_L_TmpJnt";
	rename -uid "D6C31024-4AC2-42A4-A93B-E5B752208EE0";
	setAttr ".t" -type "double3" -5.5511151231261907e-017 0.66657734365158305 -1.3322676295502531e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.05;
createNode joint -n "ClothLwrD3_L_TmpJnt" -p "ClothLwrD2_L_TmpJnt";
	rename -uid "36C61D83-496F-A2B7-08E0-16A9B7608212";
	setAttr ".t" -type "double3" 3.0531133177191805e-016 0.022728582787435059 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 -3.1805546814635168e-015 -1.5902773407317584e-015 ;
	setAttr ".radi" 0.05;
createNode joint -n "ClothLwrA1_R_TmpJnt" -p "Belt_TmpJnt";
	rename -uid "10944FA5-46D7-FD6F-9D5D-54A3F2584C46";
	setAttr ".t" -type "double3" -0.602995 -0.172013763811937 0.4934332454904985 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -167.55100008574161 45.122997277184524 178.31846014157341 ;
	setAttr ".radi" 0.05;
createNode joint -n "ClothLwrA2_R_TmpJnt" -p "ClothLwrA1_R_TmpJnt";
	rename -uid "820B150F-4234-5A02-024D-FE99682CDBC1";
	setAttr ".t" -type "double3" -5.069098407695094e-007 -0.59893830850344898 1.6646252234409076e-006 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.05;
createNode joint -n "ClothLwrA3_R_TmpJnt" -p "ClothLwrA2_R_TmpJnt";
	rename -uid "0B4D44FE-4A02-3984-A2B2-43BF49C3115F";
	setAttr ".t" -type "double3" 3.0399058481567209e-007 -0.022725935235980543 -7.4673049388351842e-007 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.05;
createNode joint -n "ClothLwrB1_R_TmpJnt" -p "Belt_TmpJnt";
	rename -uid "0F9D3A76-444B-4887-6A0C-9FA61508D197";
	setAttr ".t" -type "double3" -0.890074 -0.052533763811936751 0.078672245490498505 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 155.91874313699807 85.607855286770871 138.2762368608883 ;
	setAttr ".radi" 0.05;
createNode joint -n "ClothLwrB2_R_TmpJnt" -p "ClothLwrB1_R_TmpJnt";
	rename -uid "EDF7B9AE-4977-8F46-FCB4-DD8BDF468953";
	setAttr ".t" -type "double3" -1.4018116006875303e-007 -0.76996796901611919 2.2744911998273665e-006 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.05;
createNode joint -n "ClothLwrB3_R_TmpJnt" -p "ClothLwrB2_R_TmpJnt";
	rename -uid "4B45B8A7-45C5-B2E9-9904-02B0578FDA88";
	setAttr ".t" -type "double3" -8.2748908475371863e-008 -0.022727555908446995 -1.5561281512255221e-006 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.05;
createNode joint -n "ClothLwrC1_R_TmpJnt" -p "Belt_TmpJnt";
	rename -uid "9416EAB3-4B03-F145-1C2A-99B7436B3FAE";
	setAttr ".t" -type "double3" -0.790121 0.012426236188063378 -0.35122171450950151 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 22.539472035053944 50.607745877498971 3.3497735304006881 ;
	setAttr ".radi" 0.05;
createNode joint -n "ClothLwrC2_R_TmpJnt" -p "ClothLwrC1_R_TmpJnt";
	rename -uid "BFD14BBC-48DD-3EE8-F1EA-95A198740E83";
	setAttr ".t" -type "double3" -5.4130842447319694e-008 -0.81549120419739562 2.0004092684544617e-006 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 8.5377364625159366e-007 1.7655625192200631e-030 -2.3696978997167325e-022 ;
	setAttr ".radi" 0.05;
createNode joint -n "ClothLwrC3_R_TmpJnt" -p "ClothLwrC2_R_TmpJnt";
	rename -uid "A58BE14F-410F-257D-1F27-67BDE5CF6E0E";
	setAttr ".t" -type "double3" -9.4436725939228694e-008 -0.022728558379058406 -2.7597911067545056e-008 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 8.5377364625159366e-007 1.7655625192200631e-030 -2.3696978997167325e-022 ;
	setAttr ".radi" 0.05;
createNode joint -n "ClothLwrD1_R_TmpJnt" -p "Belt_TmpJnt";
	rename -uid "C821118D-4D72-F9E1-C60F-FB8B49593BFE";
	setAttr ".t" -type "double3" -0.436923 0.059146236188063028 -0.6661407545095015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 25.514533067124049 24.941733700980347 4.3926685937890557 ;
	setAttr ".radi" 0.05;
createNode joint -n "ClothLwrD2_R_TmpJnt" -p "ClothLwrD1_R_TmpJnt";
	rename -uid "9842EF37-4A0C-A075-FB42-8C8476C05A62";
	setAttr ".t" -type "double3" 8.604967927372531e-007 -0.66657140490498179 -2.6943327884332291e-006 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.05;
createNode joint -n "ClothLwrD3_R_TmpJnt" -p "ClothLwrD2_R_TmpJnt";
	rename -uid "817BE152-439A-2F52-9585-6B938CC5F383";
	setAttr ".t" -type "double3" -8.1393949119545894e-007 -0.022730511301176648 5.6964227379552312e-007 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.05;
createNode joint -n "ClothLwrB1_TmpJnt" -p "Belt_TmpJnt";
	rename -uid "DB5D2EB9-4AF1-8D46-6453-099A8542EB64";
	setAttr ".t" -type "double3" 0 0.036647379943740788 -0.77591858097447908 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -142.4436953582121 -0.069747308033240654 0.081888105708920855 ;
	setAttr ".radi" 0.05;
createNode joint -n "ClothLwrB2_TmpJnt" -p "ClothLwrB1_TmpJnt";
	rename -uid "01F8C087-4D5F-255C-D610-36BD19299D47";
	setAttr ".t" -type "double3" -2.3071822230491534e-016 0.66657734365158294 -2.6645352591003757e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.05;
createNode joint -n "ClothLwrB3_TmpJnt" -p "ClothLwrB2_TmpJnt";
	rename -uid "871605E4-48FC-BBF2-82F5-66A0BC95CE93";
	setAttr ".t" -type "double3" 6.6613381477509392e-016 0.022728582787434171 8.8817841970012523e-016 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.05;
createNode transform -n "BeltStrapTmp_L_Crv" -p "TmpJnt_Grp";
	rename -uid "EE4300CD-45B8-3F32-FE0B-6DBA4F9FE650";
createNode nurbsCurve -n "BeltStrapTmp_L_CrvShape" -p "BeltStrapTmp_L_Crv";
	rename -uid "F6C83186-40C8-A992-A904-0A8041A40809";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 10 2 no 3
		15 -0.20000000000000001 -0.10000000000000001 0 0.10000000000000001 0.20000000000000001
		 0.29999999999999999 0.40000000000000002 0.5 0.59999999999999998 0.69999999999999996
		 0.80000000000000004 0.90000000000000002 1 1.1000000000000001 1.2
		13
		-0.24240693267581551 9.3692674401493292 1.095564984108879
		-0.5059729591531763 9.66949628336884 0.60827883872620381
		-0.80840253991558109 10.123169026717118 0.2390572880157249
		-0.88145423272501888 10.454991709943588 -0.35298645398615602
		-0.37182981392665965 9.8243189196815131 -0.69965365152468528
		-0.0075063175300960879 9.395906594298415 -0.54674163081600302
		0.44158309425116982 8.891471618159418 -0.44335767365396411
		0.74756646821237882 8.494449160348486 0.014707607714056242
		0.62733648923752361 8.4658590794662647 0.75928777554925442
		0.28453591946780993 8.7693450623084015 1.1508528605437276
		-0.24240693267581551 9.3692674401493292 1.095564984108879
		-0.5059729591531763 9.66949628336884 0.60827883872620381
		-0.80840253991558109 10.123169026717118 0.2390572880157249
		;
createNode transform -n "LipsIn_CuvTemp" -p "TmpJnt_Grp";
	rename -uid "7E69E1EA-43D3-75C1-C86D-F5A7074E9CBB";
createNode nurbsCurve -n "LipsIn_CuvTempShape" -p "LipsIn_CuvTemp";
	rename -uid "64E96D55-4925-905C-07B3-709D5C158F63";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 68 2 no 3
		73 -0.029411764705882353 -0.014705882352941176 0 0.014705882352941176 0.029411764705882353
		 0.044117647058823532 0.058823529411764705 0.073529411764705885 0.088235294117647065
		 0.10294117647058824 0.11764705882352941 0.13235294117647059 0.14705882352941177 0.16176470588235295
		 0.17647058823529413 0.19117647058823528 0.20588235294117649 0.22058823529411761 0.23529411764705882
		 0.25 0.26470588235294118 0.27941176470588236 0.29411764705882354 0.30882352941176472
		 0.3235294117647059 0.33823529411764708 0.35294117647058826 0.36764705882352944 0.38235294117647056
		 0.39705882352941174 0.41176470588235298 0.42647058823529416 0.44117647058823523 0.45588235294117646
		 0.47058823529411764 0.48529411764705882 0.5 0.51470588235294112 0.52941176470588236
		 0.54411764705882348 0.55882352941176472 0.57352941176470584 0.58823529411764708 0.6029411764705882
		 0.61764705882352944 0.63235294117647056 0.6470588235294118 0.66176470588235292 0.67647058823529416
		 0.69117647058823528 0.70588235294117652 0.72058823529411764 0.73529411764705888 0.75
		 0.76470588235294112 0.77941176470588236 0.79411764705882348 0.80882352941176483 0.82352941176470595
		 0.83823529411764697 0.85294117647058831 0.86764705882352944 0.88235294117647045 0.8970588235294118
		 0.91176470588235292 0.92647058823529416 0.94117647058823528 0.95588235294117652 0.97058823529411764
		 0.98529411764705888 1 1.0147058823529411 1.0294117647058822
		71
		0.70269298553466797 11.416803359985352 0.65583103895187378
		0.69340276718139648 11.423260688781738 0.67060482501983643
		0.67497646808624268 11.42872142791748 0.69801449775695801
		0.65895682573318481 11.434903144836426 0.74824965000152588
		0.64050793647766113 11.441821098327637 0.81140613555908203
		0.61033898591995239 11.447929382324219 0.8910210132598877
		0.58349597454071045 11.453658103942871 0.97930198907852173
		0.54650598764419556 11.447914123535156 1.0939780473709106
		0.50605601072311401 11.435195922851563 1.2080320119857788
		0.46080499887466431 11.419367790222168 1.3261940479278564
		0.41998898983001709 11.395881652832031 1.4375720024108887
		0.37952700257301331 11.368411064147949 1.536998987197876
		0.33016601204872131 11.334582328796387 1.6268320083618164
		0.26813507080078125 11.307827949523926 1.6933679580688477
		0.19731523096561432 11.291312217712402 1.7362697124481201
		0.12387940287590027 11.279718399047852 1.7678130865097046
		0.057312317192554474 11.274123191833496 1.788557767868042
		0 11.272055625915527 1.7961262464523315
		-0.057311665266752243 11.274123191833496 1.788557767868042
		-0.12387940287590027 11.279718399047852 1.7678130865097046
		-0.19731520116329193 11.291312217712402 1.7362697124481201
		-0.26813504099845886 11.307828903198242 1.6933679580688477
		-0.33016601204872131 11.33458137512207 1.6268320083618164
		-0.37952700257301331 11.368411064147949 1.5369999408721924
		-0.41998898983001709 11.395881652832031 1.4375720024108887
		-0.46080499887466431 11.419367790222168 1.3261940479278564
		-0.50605601072311401 11.435195922851563 1.2080320119857788
		-0.54650598764419556 11.447914123535156 1.0939780473709106
		-0.58349597454071045 11.453658103942871 0.97930198907852173
		-0.61033898591995239 11.447929382324219 0.8910210132598877
		-0.64050763845443726 11.441821098327637 0.8114057183265686
		-0.65895670652389526 11.434902191162109 0.74824923276901245
		-0.67497658729553223 11.42872142791748 0.69801408052444458
		-0.69340282678604126 11.423261642456055 0.6706046462059021
		-0.70269310474395752 11.416802406311035 0.65583091974258423
		-0.69290518760681152 11.409844398498535 0.66184735298156738
		-0.67674767971038818 11.407010078430176 0.67856299877166748
		-0.65938711166381836 11.405804634094238 0.71964699029922485
		-0.64162427186965942 11.407413482666016 0.77731072902679443
		-0.61055463552474976 11.40812873840332 0.85739719867706299
		-0.56919300556182861 11.413741111755371 0.95471447706222534
		-0.52593803405761719 11.41302490234375 1.0566720962524414
		-0.47777855396270752 11.407795906066895 1.1671144962310791
		-0.43169492483139038 11.39582347869873 1.2716379165649414
		-0.38557952642440796 11.378588676452637 1.3732806444168091
		-0.33243116736412048 11.35664176940918 1.4711768627166748
		-0.2818620502948761 11.334331512451172 1.5533316135406494
		-0.23206843435764313 11.320755004882813 1.617011547088623
		-0.18388879299163818 11.302083969116211 1.6568779945373535
		-0.13128587603569031 11.291987419128418 1.6836951971054077
		-0.066027373075485229 11.283895492553711 1.7055666446685791
		0 11.281249046325684 1.7143428325653076
		0.066029973328113556 11.283895492553711 1.7055666446685791
		0.13128592073917389 11.291987419128418 1.6836951971054077
		0.18388888239860535 11.302083969116211 1.6568779945373535
		0.23206858336925507 11.320755004882813 1.6170110702514648
		0.28186175227165222 11.334332466125488 1.5533316135406494
		0.33243122696876526 11.35664176940918 1.4711769819259644
		0.3855794370174408 11.378588676452637 1.3732802867889404
		0.43169489502906799 11.39582347869873 1.2716381549835205
		0.47777801752090454 11.407795906066895 1.1671144962310791
		0.52593815326690674 11.41302490234375 1.0566720962524414
		0.56919306516647339 11.413741111755371 0.95471447706222534
		0.61055469512939453 11.40812873840332 0.85739725828170776
		0.64162445068359375 11.407413482666016 0.77731078863143921
		0.65938735008239746 11.405803680419922 0.71964722871780396
		0.67674779891967773 11.407010078430176 0.67856305837631226
		0.69290518760681152 11.409846305847168 0.66184735298156738
		0.70269298553466797 11.416803359985352 0.65583103895187378
		0.69340276718139648 11.423260688781738 0.67060482501983643
		0.67497646808624268 11.42872142791748 0.69801449775695801
		;
createNode transform -n "LipsOut_CuvTemp" -p "TmpJnt_Grp";
	rename -uid "05365A60-4480-9FB2-CA0A-A3A2ABEB9B65";
createNode nurbsCurve -n "LipsOut_CuvTempShape" -p "LipsOut_CuvTemp";
	rename -uid "14249364-4F32-3619-602F-19847AEE8980";
	setAttr -k off ".v";
	setAttr ".tw" yes;
	setAttr -s 71 ".cp[0:70]" -type "double3" 0.67903613626771997 11.418294906616211 
		1.6551014257723851 0.76063466399055402 11.440176963806152 1.680131044325492 0.83245586458463972 
		11.456089973449707 1.7051519824320838 0.89882604859762827 11.47240161895752 1.7400983741016482 
		0.96212321263893164 11.488487243652342 1.7777972000846887 1.0123236109729064 11.497702598571777 
		1.8320530124284133 1.0530616780827382 11.495469093322752 1.8951818140110175 1.0904579220039055 
		11.485635757446289 1.9678473851384619 1.1265329398760202 11.470723152160643 2.0352584212714864 
		1.1586625925248755 11.453756332397459 2.1001357201228896 1.1908287192383438 11.429439544677734 
		2.1545805407415237 1.2145814376960737 11.395685195922852 2.2012229830961059 1.2165084289027139 
		11.355637550354004 2.2315089165261823 1.1833528333693815 11.309870719909668 2.2244556684979844 
		1.1389682974241297 11.279775619506836 2.1884077121911352 1.0890518665496793 11.264646530151369 
		2.1344493604873107 1.0422315283384229 11.258966445922852 2.0658525676517461 0.99704475237339674 
		11.257402420043945 1.983055762268227 0.94298588294586616 11.258966445922852 1.8820844888820127 
		0.87026038098311864 11.264646530151367 1.768276043861438 0.77754743493561962 11.279775619506834 
		1.6432950049331869 0.67416549533452641 11.30987071990967 1.5048502705188849 0.56709200902920698 
		11.355636596679688 1.3433223441858611 0.47948882721155528 11.395685195922852 1.1521033495068209 
		0.39999143209720811 11.429439544677734 0.95299905023763976 0.31489542994820285 11.453756332397461 
		0.75685504330238795 0.21674773730981101 11.470723152160645 0.56170039717779574 0.11112458235591915 
		11.485635757446289 0.37702723634289426 -0.0039430588256483823 11.495469093322752 
		0.20111154658952735 -0.12413709728971312 11.497703552246092 0.048452576997365271 
		-0.24251787528415053 11.488487243652344 -0.079721607635626968 -0.35371357865354536 
		11.472400665283203 -0.17641771569634279 -0.46628314484224204 11.456089973449707 -0.25416038300488342 
		-0.57686711847543515 11.44017505645752 -0.30508060724216113 -0.67903650612020094 
		11.418292999267578 -0.33898801534546918 -0.76058512927389965 11.393548011779785 -0.34784861814606494 
		-0.83078681612440985 11.374625205993651 -0.33312214435044996 -0.88678072399474184 
		11.363489151000977 -0.28367401514979107 -0.94427215823381028 11.362876892089844 -0.20394871552657179 
		-0.98836123277142296 11.368411064147949 -0.092739231303799685 -1.0261027350234662 
		11.375078201293945 0.04407020549604157 -1.0584486784926439 11.374191284179688 0.18890651217189114 
		-1.0830586823948551 11.369706153869629 0.34747528123302718 -1.1045632127998519 11.358016014099119 
		0.50932487284250116 -1.1227827442177278 11.340321540832518 0.67167132299121335 -1.1286628924128905 
		11.32033634185791 0.83414390998998023 -1.1291578217805767 11.298874855041504 0.99071400746958505 
		-1.1262345519051515 11.284804344177248 1.1365261538509963 -1.1160585816690263 11.266639709472654 
		1.2628956504697149 -1.0937399791853313 11.25535202026367 1.379648568635198 -1.0509895027186502 
		11.247365951538086 1.4911601093113558 -0.99704474982868285 11.244543075561523 1.590809532376422 
		-0.93422065193067472 11.247366905212402 1.6749284266153237 -0.86557219188310996 11.255352020263672 
		1.7458216469883912 -0.80045716638817321 11.266640663146973 1.8080083575856682 -0.73128373198952823 
		11.284804344177246 1.8561310747339494 -0.65444276525800049 11.29887580871582 1.8789016533168312 
		-0.56540728302556187 11.32033634185791 1.8832635425358251 -0.46803746580799632 11.340323448181154 
		1.8732524570709561 -0.36899472140473755 11.358016014099121 1.8526061448800377 -0.26022238186584051 
		11.369706153869629 1.8210331863861984 -0.14313269368024029 11.374191284179688 1.7797267804124406 
		-0.023016539784120549 11.375079154968262 1.7381402342201466 0.10017376767951547 11.368409156799316 
		1.6908615009386987 0.22466675704211547 11.362875938415527 1.6535698572397082 0.34166789997230129 
		11.36348819732666 1.6328415337891227 0.46461337368543049 11.374625205993652 1.6261899251369587 
		0.5768168273615677 11.393548965454102 1.6373621990396539 0 0 0 0 0 0 0 0 0;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "33A0B733-4B88-E889-D2BE-C494A46075DC";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "A3B91284-49EC-0A83-8B70-7581E4DEA35F";
	setAttr ".bsdt[0].bscd" -type "Int32Array" 0 ;
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "9B269729-47AA-6158-7AE7-55826DABD36B";
createNode displayLayerManager -n "layerManager";
	rename -uid "DEA6167D-4418-5100-9B4D-15A686C702E4";
	setAttr -s 3 ".dli[1:2]"  1 2;
	setAttr -s 3 ".dli";
createNode displayLayer -n "defaultLayer";
	rename -uid "8B4AD38C-41C7-CCCF-1BCB-05A2D1B03FC3";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "32590567-480C-DB93-079C-A8B1404CE12C";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "9E074BE8-4928-12C7-EDAD-358435705579";
	setAttr ".g" yes;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "E9F2BDFC-41BE-A00F-6659-F5BA9E35B353";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode ilrOptionsNode -s -n "TurtleRenderOptions";
	rename -uid "738E948C-477D-769B-33EB-D8B4B4DCC4CC";
lockNode -l 1 ;
createNode ilrUIOptionsNode -s -n "TurtleUIOptions";
	rename -uid "60EB280D-4880-16E6-9DEF-5D97989645FB";
lockNode -l 1 ;
createNode ilrBakeLayerManager -s -n "TurtleBakeLayerManager";
	rename -uid "0BEC0EC6-4120-49EE-AD5F-AA9F31B8EEB5";
lockNode -l 1 ;
createNode VRaySettingsNode -s -n "vraySettings";
	rename -uid "8B78BD4C-44BC-21B3-DF5A-CE9554D94A90";
	setAttr ".pe" 2;
	setAttr ".se" 3;
	setAttr ".cmph" 60;
	setAttr ".cfile" -type "string" "";
	setAttr ".cfile2" -type "string" "";
	setAttr ".casf" -type "string" "";
	setAttr ".casf2" -type "string" "";
	setAttr ".st" 3;
	setAttr ".msr" 2;
	setAttr ".sd" 1000;
	setAttr ".ss" 0.01;
	setAttr ".pfts" 20;
	setAttr ".ufg" yes;
	setAttr ".fnm" -type "string" "";
	setAttr ".lcfnm" -type "string" "";
	setAttr ".asf" -type "string" "";
	setAttr ".lcasf" -type "string" "";
	setAttr ".urtrshd" yes;
	setAttr ".rtrshd" 2;
	setAttr ".ifile" -type "string" "";
	setAttr ".ifile2" -type "string" "";
	setAttr ".iasf" -type "string" "";
	setAttr ".iasf2" -type "string" "";
	setAttr ".pmfile" -type "string" "";
	setAttr ".pmfile2" -type "string" "";
	setAttr ".pmasf" -type "string" "";
	setAttr ".pmasf2" -type "string" "";
	setAttr ".dmcstd" yes;
	setAttr ".cmao" 2;
	setAttr ".cg" 2.2000000476837158;
	setAttr ".mtah" yes;
	setAttr ".srflc" 1;
	setAttr ".seu" yes;
	setAttr ".gormio" yes;
	setAttr ".wi" 960;
	setAttr ".he" 540;
	setAttr ".aspr" 1.7777780294418335;
	setAttr ".jpegq" 100;
	setAttr ".vfbOn" yes;
	setAttr ".vfbSA" -type "Int32Array" 1 0 ;
	setAttr ".mSceneName" -type "string" "E:/Ken_Pipeline/core/rf_maya/rftool/rig/template/sevenChickMovie/skel/characterTmp.ma";
createNode renderGlobals -s -n "swiffworksRenderGlobals";
	rename -uid "70408C54-478C-7055-C303-19BB7BA03223";
	addAttr -ci true -sn "rm" -ln "renderMode" -at "short";
	addAttr -ci true -sn "twos" -ln "twoSided" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "ctc" -ln "convertToCurves" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "cqlty" -ln "curveQuality" -at "short";
	addAttr -ci true -sn "inttype" -ln "intersType" -at "short";
	addAttr -ci true -sn "dlt" -ln "defaultLineThickness" -dv 0.2 -at "float";
	addAttr -ci true -sn "dlo" -ln "defaultLineOpacity" -dv 1 -at "float";
	addAttr -ci true -sn "dlcr" -ln "defaultLineColorR" -at "float";
	addAttr -ci true -sn "dlcg" -ln "defaultLineColorG" -at "float";
	addAttr -ci true -sn "dlcb" -ln "defaultLineColorB" -at "float";
	addAttr -ci true -sn "bw" -ln "bandwidth" -dv 14400 -at "float";
	addAttr -ci true -sn "oswg" -ln "onlySWG" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "fa" -ln "defaultFoldAngle" -dv 45 -at "float";
	addAttr -ci true -sn "df" -ln "defaultDisplayFolds" -at "short";
createNode renderGlobals -s -n "inkPaintRenderGlobals";
	rename -uid "8414D715-4400-8278-DF1C-6A9C2C1D2BCB";
	addAttr -ci true -sn "regMode" -ln "regionMode" -at "short";
	addAttr -ci true -sn "dpi" -ln "dpiValue" -dv 100 -at "short";
createNode renderGlobals -s -n "inkworksRenderGlobals";
	rename -uid "889D6D20-4038-35B7-CEEC-F7903C3F6052";
	addAttr -ci true -sn "bb" -ln "blendBackgr" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "rm" -ln "renderMode" -at "short";
	addAttr -ci true -sn "as" -ln "adaptiveSubdv" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "twos" -ln "twoSided" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "oiw" -ln "onlyIW" -min 0 -max 1 -at "bool";
createNode animLayer -s -n "BaseAnimation";
	rename -uid "BC0DD98C-4144-343B-78C8-1EA952AF8FE4";
	setAttr ".ovrd" yes;
createNode displayLayer -n "layer1";
	rename -uid "CF7A2ACE-4977-B1C5-197D-5192DD54F795";
	setAttr ".v" no;
	setAttr ".do" 1;
createNode displayLayer -n "layer2";
	rename -uid "8D97E761-43A8-296B-A986-D1B8C1469E0E";
	setAttr ".v" no;
	setAttr ".do" 2;
createNode rebuildCurve -n "rebuildCurve1";
	rename -uid "AD52468D-48A4-5953-56A5-2797541959E0";
	setAttr ".s" 68;
	setAttr ".tol" 0.0001;
	setAttr ".end" 1;
	setAttr ".kr" 0;
	setAttr ".kt" no;
createNode makeNurbCircle -n "makeNurbCircle1";
	rename -uid "1F3D4D59-440A-983A-7886-F9990F6A5571";
	setAttr ".nr" -type "double3" 0 1 0 ;
	setAttr ".tol" 0.0001;
select -ne :time1;
	setAttr -av -k on ".cch";
	setAttr -av -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 1;
	setAttr -av -k on ".unw" 1;
	setAttr -av -k on ".etw";
	setAttr -av -k on ".tps";
	setAttr -av -k on ".tms";
select -ne :hardwareRenderingGlobals;
	setAttr -k on ".ihi";
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr -av ".aoam";
	setAttr -av ".aora";
	setAttr -av ".mbe";
	setAttr -av -k on ".mbsof";
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
select -ne :renderGlobalsList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
select -ne :defaultShaderList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
	setAttr -k on ".ihi";
select -ne :initialShadingGroup;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
select -ne :initialParticleSE;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
select -ne :defaultRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".macc";
	setAttr -k on ".macd";
	setAttr -k on ".macq";
	setAttr -k on ".mcfr";
	setAttr -cb on ".ifg";
	setAttr -k on ".clip";
	setAttr -k on ".edm";
	setAttr -av -k on ".edl";
	setAttr -cb on ".ren";
	setAttr -av -k on ".esr";
	setAttr -k on ".ors";
	setAttr -cb on ".sdf";
	setAttr -av -k on ".outf";
	setAttr -av -cb on ".imfkey";
	setAttr -av -k on ".gama";
	setAttr -av -k on ".an";
	setAttr -cb on ".ar";
	setAttr -k on ".fs";
	setAttr -k on ".ef";
	setAttr -av -k on ".bfs";
	setAttr -cb on ".me";
	setAttr -cb on ".se";
	setAttr -av -k on ".be";
	setAttr -av -cb on ".ep";
	setAttr -k on ".fec";
	setAttr -av -k on ".ofc";
	setAttr -cb on ".ofe";
	setAttr -cb on ".efe";
	setAttr -cb on ".oft";
	setAttr -cb on ".umfn";
	setAttr -cb on ".ufe";
	setAttr -av -k on ".pff";
	setAttr -av -cb on ".peie";
	setAttr -av -cb on ".ifp";
	setAttr -k on ".rv";
	setAttr -k on ".comp";
	setAttr -k on ".cth";
	setAttr -k on ".soll";
	setAttr -cb on ".sosl";
	setAttr -k on ".rd";
	setAttr -k on ".lp";
	setAttr -av -k on ".sp";
	setAttr -k on ".shs";
	setAttr -av -k on ".lpr";
	setAttr -cb on ".gv";
	setAttr -cb on ".sv";
	setAttr -k on ".mm";
	setAttr -k on ".npu";
	setAttr -k on ".itf";
	setAttr -k on ".shp";
	setAttr -cb on ".isp";
	setAttr -k on ".uf";
	setAttr -k on ".oi";
	setAttr -k on ".rut";
	setAttr -k on ".mot";
	setAttr -av -cb on ".mb";
	setAttr -av -k on ".mbf";
	setAttr -k on ".mbso";
	setAttr -k on ".mbsc";
	setAttr -av -k on ".afp";
	setAttr -k on ".pfb";
	setAttr -k on ".pram" -type "string" "pgYetiVRayPreRender";
	setAttr -k on ".poam" -type "string" "pgYetiVRayPostRender";
	setAttr -k on ".prlm";
	setAttr -k on ".polm";
	setAttr -cb on ".prm";
	setAttr -cb on ".pom";
	setAttr -cb on ".pfrm";
	setAttr -cb on ".pfom";
	setAttr -av -k on ".bll";
	setAttr -av -k on ".bls";
	setAttr -av -k on ".smv";
	setAttr -k on ".ubc";
	setAttr -k on ".mbc";
	setAttr -cb on ".mbt";
	setAttr -k on ".udbx";
	setAttr -k on ".smc";
	setAttr -k on ".kmv";
	setAttr -cb on ".isl";
	setAttr -cb on ".ism";
	setAttr -cb on ".imb";
	setAttr -av -k on ".rlen";
	setAttr -av -k on ".frts";
	setAttr -k on ".tlwd";
	setAttr -k on ".tlht";
	setAttr -k on ".jfc";
	setAttr -cb on ".rsb";
	setAttr -k on ".ope";
	setAttr -k on ".oppf";
	setAttr -av -k on ".rcp";
	setAttr -av -k on ".icp";
	setAttr -av -k on ".ocp";
	setAttr -cb on ".hbl";
select -ne :defaultResolution;
	setAttr -av -k on ".cch";
	setAttr -av -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -av -k on ".w";
	setAttr -av -k on ".h";
	setAttr -av -k on ".pa" 1;
	setAttr -av -k on ".al";
	setAttr -av -k on ".dar";
	setAttr -av -k on ".ldar";
	setAttr -av -k on ".dpi";
	setAttr -av -k on ".off";
	setAttr -av -k on ".fld";
	setAttr -av -k on ".zsl";
	setAttr -av -k on ".isu";
	setAttr -av -k on ".pdu";
select -ne :hardwareRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k off -cb on ".ctrs" 256;
	setAttr -av -k off -cb on ".btrs" 512;
	setAttr -k off -cb on ".fbfm";
	setAttr -k off -cb on ".ehql";
	setAttr -k off -cb on ".eams";
	setAttr -k off -cb on ".eeaa";
	setAttr -k off -cb on ".engm";
	setAttr -k off -cb on ".mes";
	setAttr -k off -cb on ".emb";
	setAttr -av -k off -cb on ".mbbf";
	setAttr -k off -cb on ".mbs";
	setAttr -k off -cb on ".trm";
	setAttr -k off -cb on ".tshc";
	setAttr -k off -cb on ".enpt";
	setAttr -k off -cb on ".clmt";
	setAttr -k off -cb on ".tcov";
	setAttr -k off -cb on ".lith";
	setAttr -k off -cb on ".sobc";
	setAttr -k off -cb on ".cuth";
	setAttr -k off -cb on ".hgcd";
	setAttr -k off -cb on ".hgci";
	setAttr -k off -cb on ".mgcs";
	setAttr -k off -cb on ".twa";
	setAttr -k off -cb on ".twz";
	setAttr -cb on ".hwcc";
	setAttr -cb on ".hwdp";
	setAttr -cb on ".hwql";
	setAttr -k on ".hwfr";
	setAttr -k on ".soll";
	setAttr -k on ".sosl";
	setAttr -k on ".bswa";
	setAttr -k on ".shml";
	setAttr -k on ".hwel";
connectAttr "Root_TmpJnt.s" "Spine1_TmpJnt.is";
connectAttr "Spine1_TmpJnt.s" "Spine2_TmpJnt.is";
connectAttr "Spine2_TmpJnt.s" "Spine3_TmpJnt.is";
connectAttr "Spine3_TmpJnt.s" "Spine4_TmpJnt.is";
connectAttr "Spine4_TmpJnt.s" "Spine5_TmpJnt.is";
connectAttr "Spine5_TmpJnt.s" "Clav_L_TmpJnt.is";
connectAttr "Clav_L_TmpJnt.s" "UpArm_L_TmpJnt.is";
connectAttr "UpArm_L_TmpJnt.s" "Forearm_L_TmpJnt.is";
connectAttr "Forearm_L_TmpJnt.s" "Wrist_L_TmpJnt.is";
connectAttr "Wrist_L_TmpJnt.s" "Hand_L_TmpJnt.is";
connectAttr "Hand_L_TmpJnt.s" "Index1_L_TmpJnt.is";
connectAttr "Index1_L_TmpJnt.s" "Index2_L_TmpJnt.is";
connectAttr "Index2_L_TmpJnt.s" "Index3_L_TmpJnt.is";
connectAttr "Index3_L_TmpJnt.s" "Index4_L_TmpJnt.is";
connectAttr "Index4_L_TmpJnt.s" "Index5_L_TmpJnt.is";
connectAttr "Hand_L_TmpJnt.s" "Thumb1_L_TmpJnt.is";
connectAttr "Thumb1_L_TmpJnt.s" "Thumb2_L_TmpJnt.is";
connectAttr "Thumb2_L_TmpJnt.s" "Thumb3_L_TmpJnt.is";
connectAttr "Thumb3_L_TmpJnt.s" "Thumb4_L_TmpJnt.is";
connectAttr "Hand_L_TmpJnt.s" "HandCup_L_TmpJnt.is";
connectAttr "HandCup_L_TmpJnt.s" "Pinky1_L_TmpJnt.is";
connectAttr "Pinky1_L_TmpJnt.s" "Pinky2_L_TmpJnt.is";
connectAttr "Pinky2_L_TmpJnt.s" "Pinky3_L_TmpJnt.is";
connectAttr "Pinky3_L_TmpJnt.s" "Pinky4_L_TmpJnt.is";
connectAttr "Pinky4_L_TmpJnt.s" "Pinky5_L_TmpJnt.is";
connectAttr "HandCup_L_TmpJnt.s" "Middle1_L_TmpJnt.is";
connectAttr "Middle1_L_TmpJnt.s" "Middle2_L_TmpJnt.is";
connectAttr "Middle2_L_TmpJnt.s" "Middle3_L_TmpJnt.is";
connectAttr "Middle3_L_TmpJnt.s" "Middle4_L_TmpJnt.is";
connectAttr "Middle4_L_TmpJnt.s" "Middle5_L_TmpJnt.is";
connectAttr "Clav_L_TmpJnt.s" "ShoulderArmor_L_TmpJnt.is";
connectAttr "Spine5_TmpJnt.s" "Neck_TmpJnt.is";
connectAttr "Neck_TmpJnt.s" "Head_TmpJnt.is";
connectAttr "Head_TmpJnt.s" "HeadEnd_TmpJnt.is";
connectAttr "Head_TmpJnt.s" "Eye_L_TmpJnt.is";
connectAttr "Eye_L_TmpJnt.s" "EyeDot_L_TmpJnt.is";
connectAttr "Eye_L_TmpJnt.s" "Pupil_L_TmpJnt.is";
connectAttr "Pupil_L_TmpJnt.s" "Iris_L_TmpJnt.is";
connectAttr "Head_TmpJnt.s" "Eye_R_TmpJnt.is";
connectAttr "Eye_R_TmpJnt.s" "EyeDot_R_TmpJnt.is";
connectAttr "Eye_R_TmpJnt.s" "Pupil_R_TmpJnt.is";
connectAttr "Pupil_R_TmpJnt.s" "Iris_R_TmpJnt.is";
connectAttr "Head_TmpJnt.s" "JawUprRoot_TmpJnt.is";
connectAttr "JawUprRoot_TmpJnt.s" "JawUpr1_TmpJnt.is";
connectAttr "JawUpr1_TmpJnt.s" "TeethUpr_TmpJnt.is";
connectAttr "JawUpr1_TmpJnt.s" "JawUprEnd_TmpJnt.is";
connectAttr "Head_TmpJnt.s" "JawLwr1_TmpJnt.is";
connectAttr "JawLwr1_TmpJnt.s" "JawLwr2_TmpJnt.is";
connectAttr "JawLwr2_TmpJnt.s" "JawLwrEnd_TmpJnt.is";
connectAttr "JawLwr2_TmpJnt.s" "TeethLwr_TmpJnt.is";
connectAttr "Head_TmpJnt.s" "EyeTrgt_TmpJnt.is";
connectAttr "EyeTrgt_TmpJnt.s" "EyeTrgt_L_TmpJnt.is";
connectAttr "EyeTrgt_TmpJnt.s" "EyeTrgt_R_TmpJnt.is";
connectAttr "Head_TmpJnt.s" "Fur1_L_TmpJnt.is";
connectAttr "Fur1_L_TmpJnt.s" "Fur2_L_TmpJnt.is";
connectAttr "Fur2_L_TmpJnt.s" "Fur3_L_TmpJnt.is";
connectAttr "Head_TmpJnt.s" "Fur1_R_TmpJnt.is";
connectAttr "Fur1_R_TmpJnt.s" "Fur2_R_TmpJnt.is";
connectAttr "Fur2_R_TmpJnt.s" "Fur3_R_TmpJnt.is";
connectAttr "Head_TmpJnt.s" "Tongue1_TmpJnt.is";
connectAttr "Tongue1_TmpJnt.s" "Tongue2_TmpJnt.is";
connectAttr "Tongue2_TmpJnt.s" "Tongue3_TmpJnt.is";
connectAttr "Tongue3_TmpJnt.s" "Tongue4_TmpJnt.is";
connectAttr "Tongue4_TmpJnt.s" "Tongue5_TmpJnt.is";
connectAttr "Spine5_TmpJnt.s" "HoodA1_TmpJnt.is";
connectAttr "HoodA1_TmpJnt.s" "HoodA2_TmpJnt.is";
connectAttr "HoodA2_TmpJnt.s" "HoodA3_TmpJnt.is";
connectAttr "HoodA3_TmpJnt.s" "HoodA4_TmpJnt.is";
connectAttr "HoodA4_TmpJnt.s" "Ear1_L_TmpJnt.is";
connectAttr "Ear1_L_TmpJnt.s" "Ear2_L_TmpJnt.is";
connectAttr "Ear2_L_TmpJnt.s" "Ear3_L_TmpJnt.is";
connectAttr "Ear3_L_TmpJnt.s" "Ear4_L_TmpJnt.is";
connectAttr "HoodA4_TmpJnt.s" "Ear1_R_TmpJnt.is";
connectAttr "Ear1_R_TmpJnt.s" "Ear2_R_TmpJnt.is";
connectAttr "Ear2_R_TmpJnt.s" "Ear3_R_TmpJnt.is";
connectAttr "Ear3_R_TmpJnt.s" "Ear4_R_TmpJnt.is";
connectAttr "HoodA2_TmpJnt.s" "HoodHairBck_TmpJnt.is";
connectAttr "Spine5_TmpJnt.s" "HoodBA_TmpJnt.is";
connectAttr "HoodBA_TmpJnt.s" "HoodBA1_TmpJnt.is";
connectAttr "HoodBA_TmpJnt.s" "HoodBB_TmpJnt.is";
connectAttr "HoodBB_TmpJnt.s" "HoodBB1_TmpJnt.is";
connectAttr "HoodBB_TmpJnt.s" "HoodBC_TmpJnt.is";
connectAttr "HoodBC_TmpJnt.s" "HoodBC1_TmpJnt.is";
connectAttr "HoodBC_TmpJnt.s" "HoodBD_TmpJnt.is";
connectAttr "Spine5_TmpJnt.s" "Clav_R_TmpJnt.is";
connectAttr "Clav_R_TmpJnt.s" "UpArm_R_TmpJnt.is";
connectAttr "UpArm_R_TmpJnt.s" "Forearm_R_TmpJnt.is";
connectAttr "Forearm_R_TmpJnt.s" "Wrist_R_TmpJnt.is";
connectAttr "Wrist_R_TmpJnt.s" "Hand_R_TmpJnt.is";
connectAttr "Hand_R_TmpJnt.s" "Index1_R_TmpJnt.is";
connectAttr "Index1_R_TmpJnt.s" "Index2_R_TmpJnt.is";
connectAttr "Index2_R_TmpJnt.s" "Index3_R_TmpJnt.is";
connectAttr "Index3_R_TmpJnt.s" "Index4_R_TmpJnt.is";
connectAttr "Index4_R_TmpJnt.s" "Index5_R_TmpJnt.is";
connectAttr "Hand_R_TmpJnt.s" "Thumb1_R_TmpJnt.is";
connectAttr "Thumb1_R_TmpJnt.s" "Thumb2_R_TmpJnt.is";
connectAttr "Thumb2_R_TmpJnt.s" "Thumb3_R_TmpJnt.is";
connectAttr "Thumb3_R_TmpJnt.s" "Thumb4_R_TmpJnt.is";
connectAttr "Hand_R_TmpJnt.s" "HandCup_R_TmpJnt.is";
connectAttr "HandCup_R_TmpJnt.s" "Pinky1_R_TmpJnt.is";
connectAttr "Pinky1_R_TmpJnt.s" "Pinky2_R_TmpJnt.is";
connectAttr "Pinky2_R_TmpJnt.s" "Pinky3_R_TmpJnt.is";
connectAttr "Pinky3_R_TmpJnt.s" "Pinky4_R_TmpJnt.is";
connectAttr "Pinky4_R_TmpJnt.s" "Pinky5_R_TmpJnt.is";
connectAttr "HandCup_R_TmpJnt.s" "Middle1_R_TmpJnt.is";
connectAttr "Middle1_R_TmpJnt.s" "Middle2_R_TmpJnt.is";
connectAttr "Middle2_R_TmpJnt.s" "Middle3_R_TmpJnt.is";
connectAttr "Middle3_R_TmpJnt.s" "Middle4_R_TmpJnt.is";
connectAttr "Middle4_R_TmpJnt.s" "Middle5_R_TmpJnt.is";
connectAttr "Clav_R_TmpJnt.s" "ShoulderArmor_R_TmpJnt.is";
connectAttr "Root_TmpJnt.s" "Pelvis_TmpJnt.is";
connectAttr "Pelvis_TmpJnt.s" "UpLeg_L_TmpJnt.is";
connectAttr "UpLeg_L_TmpJnt.s" "LowLeg_L_TmpJnt.is";
connectAttr "LowLeg_L_TmpJnt.s" "Knee_L_TmpJnt.is";
connectAttr "LowLeg_L_TmpJnt.s" "Ankle_L_TmpJnt.is";
connectAttr "Ball_L_TmpJnt.s" "Toe_L_TmpJnt.is";
connectAttr "Ankle_L_TmpJnt.s" "FootIn_L_TmpJnt.is";
connectAttr "Ankle_L_TmpJnt.s" "FootOut_L_TmpJnt.is";
connectAttr "Ankle_L_TmpJnt.s" "Heel_L_TmpJnt.is";
connectAttr "Pelvis_TmpJnt.s" "UpLeg_R_TmpJnt.is";
connectAttr "UpLeg_R_TmpJnt.s" "LowLeg_R_TmpJnt.is";
connectAttr "LowLeg_R_TmpJnt.s" "Knee_R_TmpJnt.is";
connectAttr "LowLeg_R_TmpJnt.s" "Ankle_R_TmpJnt.is";
connectAttr "Ball_R_TmpJnt.s" "Toe_R_TmpJnt.is";
connectAttr "Ankle_R_TmpJnt.s" "FootIn_R_TmpJnt.is";
connectAttr "Ankle_R_TmpJnt.s" "FootOut_R_TmpJnt.is";
connectAttr "Ankle_R_TmpJnt.s" "Heel_R_TmpJnt.is";
connectAttr "Belt_TmpJnt.s" "BeltFnt_TmpJnt.is";
connectAttr "Belt_TmpJnt.s" "BeltBck_TmpJnt.is";
connectAttr "Belt_TmpJnt.s" "Belt1_L_TmpJnt.is";
connectAttr "Belt_TmpJnt.s" "Belt2_L_TmpJnt.is";
connectAttr "Belt_TmpJnt.s" "Belt3_L_TmpJnt.is";
connectAttr "Belt_TmpJnt.s" "Belt1_R_TmpJnt.is";
connectAttr "Belt_TmpJnt.s" "Belt2_R_TmpJnt.is";
connectAttr "Belt_TmpJnt.s" "Belt3_R_TmpJnt.is";
connectAttr "Belt_TmpJnt.s" "ClothLwrA1_L_TmpJnt.is";
connectAttr "ClothLwrA1_L_TmpJnt.s" "ClothLwrA2_L_TmpJnt.is";
connectAttr "ClothLwrA2_L_TmpJnt.s" "ClothLwrA3_L_TmpJnt.is";
connectAttr "Belt_TmpJnt.s" "ClothLwrB1_L_TmpJnt.is";
connectAttr "ClothLwrB1_L_TmpJnt.s" "ClothLwrB2_L_TmpJnt.is";
connectAttr "ClothLwrB2_L_TmpJnt.s" "ClothLwrB3_L_TmpJnt.is";
connectAttr "Belt_TmpJnt.s" "ClothLwrC1_L_TmpJnt.is";
connectAttr "ClothLwrC1_L_TmpJnt.s" "ClothLwrC2_L_TmpJnt.is";
connectAttr "ClothLwrC2_L_TmpJnt.s" "ClothLwrC3_L_TmpJnt.is";
connectAttr "Belt_TmpJnt.s" "ClothLwrD1_L_TmpJnt.is";
connectAttr "ClothLwrD1_L_TmpJnt.s" "ClothLwrD2_L_TmpJnt.is";
connectAttr "ClothLwrD2_L_TmpJnt.s" "ClothLwrD3_L_TmpJnt.is";
connectAttr "Belt_TmpJnt.s" "ClothLwrA1_R_TmpJnt.is";
connectAttr "ClothLwrA1_R_TmpJnt.s" "ClothLwrA2_R_TmpJnt.is";
connectAttr "ClothLwrA2_R_TmpJnt.s" "ClothLwrA3_R_TmpJnt.is";
connectAttr "Belt_TmpJnt.s" "ClothLwrB1_R_TmpJnt.is";
connectAttr "ClothLwrB1_R_TmpJnt.s" "ClothLwrB2_R_TmpJnt.is";
connectAttr "ClothLwrB2_R_TmpJnt.s" "ClothLwrB3_R_TmpJnt.is";
connectAttr "Belt_TmpJnt.s" "ClothLwrC1_R_TmpJnt.is";
connectAttr "ClothLwrC1_R_TmpJnt.s" "ClothLwrC2_R_TmpJnt.is";
connectAttr "ClothLwrC2_R_TmpJnt.s" "ClothLwrC3_R_TmpJnt.is";
connectAttr "Belt_TmpJnt.s" "ClothLwrD1_R_TmpJnt.is";
connectAttr "ClothLwrD1_R_TmpJnt.s" "ClothLwrD2_R_TmpJnt.is";
connectAttr "ClothLwrD2_R_TmpJnt.s" "ClothLwrD3_R_TmpJnt.is";
connectAttr "Belt_TmpJnt.s" "ClothLwrB1_TmpJnt.is";
connectAttr "ClothLwrB1_TmpJnt.s" "ClothLwrB2_TmpJnt.is";
connectAttr "ClothLwrB2_TmpJnt.s" "ClothLwrB3_TmpJnt.is";
connectAttr "rebuildCurve1.oc" "LipsOut_CuvTempShape.cr";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "layerManager.dli[1]" "layer1.id";
connectAttr "layerManager.dli[2]" "layer2.id";
connectAttr "makeNurbCircle1.oc" "rebuildCurve1.ic";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of characterTmp.ma
