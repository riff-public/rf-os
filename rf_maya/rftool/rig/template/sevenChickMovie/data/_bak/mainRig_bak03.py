import maya.cmds as mc

import sys
# sys.path.append(r'D:\tools')

from lpRig import rigTools as lrr
from lpRig import core as lrc

from lpRig import rigData
reload(rigData)

from lpRig import mainGroup
reload(mainGroup)
from lpRig import rootRig
reload(rootRig)
from lpRig import torsoRig
reload(torsoRig)
from lpRig import neckRig
reload(neckRig)
from lpRig import headRig
reload(headRig)
from lpRig import clavRig
reload(clavRig)
from lpRig import armRig
reload(armRig)
from lpRig import thumbRig
reload(thumbRig)
from lpRig import fingerRig
reload(fingerRig)
from lpRig import legRig
reload(legRig)
from lpRig import aimRig
reload(aimRig)
from lpRig import fkRig
reload(fkRig)
from lpRig import pointRig
reload(pointRig)
from lpRig import nonRollRig
reload(nonRollRig)
from lpRig import ribbon
reload (ribbon)
from utaTools.pkmel import ctrlShapeTools as ctrlShapeTools
reload(ctrlShapeTools)
from utaTools.utapy import setAttrCharacter as sac 
reload(sac)


def main():

    print 'Creating Main Groups'
    mgObj = mainGroup.MainGroup('Rig')

    print 'Creating Root Rig'
    rootObj = rootRig.RootRig(
                                ctrlGrp=mgObj.Ctrl_Grp,
                                skinGrp=mgObj.Skin_Grp,
                                tmpJnt='Root_Jnt',
                                part=''
                            )

    print 'Creating Torso Rig'
    torsoObj = torsoRig.TorsoRig(
                                    parent=rootObj.Root_Jnt,
                                    ctrlGrp=mgObj.Ctrl_Grp,
                                    jntGrp=mgObj.Jnt_Grp,
                                    skinGrp=mgObj.Skin_Grp,
                                    stillGrp=mgObj.Still_Grp,
                                    tmpJnt=[
                                                'Pelvis_Jnt',
                                                'Spine_Jnt',
                                                'Chest_Jnt',
                                                'ChestTip_Jnt',
                                                'SpineMid_Jnt'
                                            ],
                                    part=''
                                )

    print 'Creating Left Clav Rig'
    lftClavObj = clavRig.ClavRig(
                                    parent=torsoObj.Chest_Jnt,
                                    ctrlGrp=mgObj.Ctrl_Grp,
                                    ax='y',
                                    tmpJnt=[
                                                'Clav_L_Jnt',
                                                'ClavTip_L_Jnt'
                                            ],
                                    part='',
                                    side='L'
                                )

    print 'Creating Right Clav Rig'
    rgtClavObj = clavRig.ClavRig(
                                    parent=torsoObj.Chest_Jnt,
                                    ctrlGrp=mgObj.Ctrl_Grp,
                                    ax='y',
                                    tmpJnt=[
                                                'Clav_R_Jnt',
                                                'ClavTip_R_Jnt'
                                            ],
                                    part='',
                                    side='R'
                                )

    print 'Creating Neck Rig'
    neckObj = neckRig.NeckRig(
                                parent=torsoObj.ChestTip_Jnt,
                                ctrlGrp=mgObj.Ctrl_Grp,
                                skinGrp=mgObj.Skin_Grp,
                                jntGrp=mgObj.Jnt_Grp,
                                stillGrp=mgObj.Still_Grp,
                                ax='y',
                                tmpJnt=[
                                        'Neck_Jnt',
                                        'NeckTip_Jnt'
                                        ],
                                part='',
                                headJnt='Head_Jnt'
                            )

    print 'Creating Left Arm Rig'
    lftArmObj = armRig.ArmRig(
                                parent=lftClavObj.ClavTip_Jnt,
                                ctrlGrp=mgObj.Ctrl_Grp,
                                jntGrp=mgObj.Jnt_Grp,
                                ikhGrp=mgObj.Ikh_Grp,
                                skinGrp=mgObj.Skin_Grp,
                                stillGrp=mgObj.Still_Grp,
                                ribbon=True,
                                tmpJnt=[
                                            'UpArm_L_Jnt',
                                            'Forearm_L_Jnt',
                                            'Wrist_L_Jnt',
                                            'Hand_L_Jnt',
                                            'ElbowIk_L_Jnt'
                                        ],
                                part='',
                                side='L'
                            )
    
    print 'Creating Head Rig'
    headObj = headRig.HeadRig(
                                parent=neckObj.NeckTip_Jnt,
                                ctrlGrp=mgObj.Ctrl_Grp,
                                tmpJnt=[
                                            'Head_Jnt',
                                            'HeadTip_Jnt',
                                            'Eye_L_Jnt',
                                            'Eye_R_Jnt',
                                            '',
                                            '',
                                            '',
                                            'EyeTar_Jnt',
                                            'EyeTar_L_Jnt',
                                            'EyeTar_R_Jnt'
                                        ],
                                part=''
                            )

    print 'Creating Right Arm Rig'
    rgtArmObj = armRig.ArmRig(
                                parent=rgtClavObj.ClavTip_Jnt,
                                ctrlGrp=mgObj.Ctrl_Grp,
                                jntGrp=mgObj.Jnt_Grp,
                                ikhGrp=mgObj.Ikh_Grp,
                                skinGrp=mgObj.Skin_Grp,
                                stillGrp=mgObj.Still_Grp,
                                ribbon=True,
                                tmpJnt=[
                                            'UpArm_R_Jnt',
                                            'Forearm_R_Jnt',
                                            'Wrist_R_Jnt',
                                            'Hand_R_Jnt',
                                            'ElbowIk_R_Jnt'
                                        ],
                                part='',
                                side='R'
                            )

    print 'Creating Left Thumb Rig'
    lftThumbObj = thumbRig.ThumbRig(
                                        thumb='Thumb',
                                        parent=lftArmObj.Hand_Jnt,
                                        armCtrl=lftArmObj.Arm_Ctrl,
                                        ctrlGrp=mgObj.Ctrl_Grp,
                                        tmpJnt=(
                                                    'Thumb_1_L_Jnt',
                                                    'Thumb_2_L_Jnt',
                                                    'Thumb_3_L_Jnt',
                                                    'Thumb_4_L_Jnt'
                                                ),
                                        side='L'
                                    )
    ctrlShp = lrc.Dag(lftArmObj.Arm_Ctrl.shape)
    fngr = 'Thumb'
    ctrlShp.attr('%sFlatRx_1' % fngr).value = -2
    ctrlShp.attr('%sFlatRz_1' % fngr).value = -0.5
    ctrlShp.attr('%sFlatRx_2' % fngr).value = -0.3
    ctrlShp.attr('%sFlatRx_3' % fngr).value = 3.3

    print 'Creating Left Index Rig'
    lftIndexObj = fingerRig.FingerRig(
                                            fngr='Index',
                                            parent=lftArmObj.Hand_Jnt,
                                            armCtrl=lftArmObj.Arm_Ctrl,
                                            ctrlGrp=mgObj.Ctrl_Grp,
                                            tmpJnt=(
                                                        'Index_1_L_Jnt',
                                                        'Index_2_L_Jnt',
                                                        'Index_3_L_Jnt',
                                                        'Index_4_L_Jnt',
                                                        'Index_5_L_Jnt'
                                                    ),
                                            side='L'
                                        )
    ctrlShp = lrc.Dag(lftArmObj.Arm_Ctrl.shape)
    fngr = 'Index'
    ctrlShp.attr('%sFlatRx_1' % fngr).value = -2
    ctrlShp.attr('%sFlatRz_1' % fngr).value = -0.5
    ctrlShp.attr('%sFlatRx_2' % fngr).value = -0.3
    ctrlShp.attr('%sFlatRx_3' % fngr).value = 3.3
    ctrlShp.attr('%sFlatRx_4' % fngr).value = 1
    ctrlShp.attr('%sCupRx_1' % fngr).value = 0
    ctrlShp.attr('%sCupRx_2' % fngr).value = -1
    ctrlShp.attr('%sCupRx_3' % fngr).value = -1
    ctrlShp.attr('%sCupRx_4' % fngr).value = -1
    ctrlShp.attr('%sSprd_2' % fngr).value = -9
    ctrlShp.attr('%sBsSprd_1' % fngr).value = -9

    print 'Creating Left Middle Rig'
    lftMiddleObj = fingerRig.FingerRig(
                                            fngr='Middle',
                                            parent=lftArmObj.Hand_Jnt,
                                            armCtrl=lftArmObj.Arm_Ctrl,
                                            ctrlGrp=mgObj.Ctrl_Grp,
                                            tmpJnt=(
                                                        'Middle_1_L_Jnt',
                                                        'Middle_2_L_Jnt',
                                                        'Middle_3_L_Jnt',
                                                        'Middle_4_L_Jnt',
                                                        'Middle_5_L_Jnt'
                                                    ),
                                            side='L'
                                        )
    ctrlShp = lrc.Dag(lftArmObj.Arm_Ctrl.shape)
    fngr = 'Middle'
    ctrlShp.attr('%sFlatRx_1' % fngr).value = -2
    ctrlShp.attr('%sFlatRz_1' % fngr).value = -0.2
    ctrlShp.attr('%sFlatRx_2' % fngr).value = -0.4
    ctrlShp.attr('%sFlatRx_3' % fngr).value = 3.5
    ctrlShp.attr('%sFlatRx_4' % fngr).value = 1.25
    ctrlShp.attr('%sCupRx_1' % fngr).value = 0
    ctrlShp.attr('%sCupRx_2' % fngr).value = -2.5
    ctrlShp.attr('%sCupRx_3' % fngr).value = -2.5
    ctrlShp.attr('%sCupRx_4' % fngr).value = -2.5
    ctrlShp.attr('%sSprd_2' % fngr).value = -4.5
    ctrlShp.attr('%sBsSprd_1' % fngr).value = -4.5

    print 'Creating Left Ring Rig'
    lftRingObj = fingerRig.FingerRig(
                                            fngr='Ring',
                                            parent=lftArmObj.Hand_Jnt,
                                            armCtrl=lftArmObj.Arm_Ctrl,
                                            ctrlGrp=mgObj.Ctrl_Grp,
                                            tmpJnt=(
                                                        'Ring_1_L_Jnt',
                                                        'Ring_2_L_Jnt',
                                                        'Ring_3_L_Jnt',
                                                        'Ring_4_L_Jnt',
                                                        'Ring_5_L_Jnt'
                                                    ),
                                            side='L'
                                        )
    ctrlShp = lrc.Dag(lftArmObj.Arm_Ctrl.shape)
    fngr = 'Ring'
    ctrlShp.attr('%sFlatRx_1' % fngr).value = -2
    ctrlShp.attr('%sFlatRx_2' % fngr).value = -0.2
    ctrlShp.attr('%sFlatRx_3' % fngr).value = 2.5
    ctrlShp.attr('%sFlatRx_4' % fngr).value = 2.5
    ctrlShp.attr('%sCupRx_1' % fngr).value = 0
    ctrlShp.attr('%sCupRx_2' % fngr).value = -3.5
    ctrlShp.attr('%sCupRx_3' % fngr).value = -3.5
    ctrlShp.attr('%sCupRx_4' % fngr).value = -3.5
    ctrlShp.attr('%sSprd_2' % fngr).value = 4.5
    ctrlShp.attr('%sBsSprd_1' % fngr).value = 4.5

    print 'Creating Left Pinky Rig'
    lftPinkyObj = fingerRig.FingerRig(
                                            fngr='Pinky',
                                            parent=lftArmObj.Hand_Jnt,
                                            armCtrl=lftArmObj.Arm_Ctrl,
                                            ctrlGrp=mgObj.Ctrl_Grp,
                                            tmpJnt=(
                                                        'Pinky_1_L_Jnt',
                                                        'Pinky_2_L_Jnt',
                                                        'Pinky_3_L_Jnt',
                                                        'Pinky_4_L_Jnt',
                                                        'Pinky_5_L_Jnt'
                                                    ),
                                            side='L'
                                        )
    ctrlShp = lrc.Dag(lftArmObj.Arm_Ctrl.shape)
    fngr = 'Pinky'
    ctrlShp.attr('%sFlatRx_1' % fngr).value = -2
    ctrlShp.attr('%sFlatRz_1' % fngr).value = 0.2
    ctrlShp.attr('%sFlatRx_2' % fngr).value = -0.5
    ctrlShp.attr('%sFlatRx_3' % fngr).value = 4.3
    ctrlShp.attr('%sFlatRx_4' % fngr).value = 0.65
    ctrlShp.attr('%sCupRx_1' % fngr).value = 0
    ctrlShp.attr('%sCupRx_2' % fngr).value = -4.5
    ctrlShp.attr('%sCupRx_3' % fngr).value = -4.5
    ctrlShp.attr('%sCupRx_4' % fngr).value = -4.5
    ctrlShp.attr('%sSprd_2' % fngr).value = 9
    ctrlShp.attr('%sBsSprd_1' % fngr).value = 9

    print 'Creating Right Thumb Rig'
    rgtThumbObj = thumbRig.ThumbRig(
                                        thumb='Thumb',
                                        parent=rgtArmObj.Hand_Jnt,
                                        armCtrl=rgtArmObj.Arm_Ctrl,
                                        ctrlGrp=mgObj.Ctrl_Grp,
                                        tmpJnt=(
                                                    'Thumb_1_R_Jnt',
                                                    'Thumb_2_R_Jnt',
                                                    'Thumb_3_R_Jnt',
                                                    'Thumb_4_R_Jnt'
                                                ),
                                        side='R'
                                    )
    ctrlShp = lrc.Dag(rgtArmObj.Arm_Ctrl.shape)
    fngr = 'Thumb'
    ctrlShp.attr('%sFlatRx_1' % fngr).value = -2
    ctrlShp.attr('%sFlatRz_1' % fngr).value = -0.5
    ctrlShp.attr('%sFlatRx_2' % fngr).value = -0.3
    ctrlShp.attr('%sFlatRx_3' % fngr).value = 3.3

    print 'Creating Right Index Rig'
    rgtIndexObj = fingerRig.FingerRig(
                                            fngr='Index',
                                            parent=rgtArmObj.Hand_Jnt,
                                            armCtrl=rgtArmObj.Arm_Ctrl,
                                            ctrlGrp=mgObj.Ctrl_Grp,
                                            tmpJnt=(
                                                        'Index_1_R_Jnt',
                                                        'Index_2_R_Jnt',
                                                        'Index_3_R_Jnt',
                                                        'Index_4_R_Jnt',
                                                        'Index_5_R_Jnt'
                                                    ),
                                            side='R'
                                        )
    ctrlShp = lrc.Dag(rgtArmObj.Arm_Ctrl.shape)
    fngr = 'Index'
    ctrlShp.attr('%sFlatRx_1' % fngr).value = -2
    ctrlShp.attr('%sFlatRz_1' % fngr).value = -0.5
    ctrlShp.attr('%sFlatRx_2' % fngr).value = -0.3
    ctrlShp.attr('%sFlatRx_3' % fngr).value = 3.3
    ctrlShp.attr('%sFlatRx_4' % fngr).value = 1
    ctrlShp.attr('%sCupRx_1' % fngr).value = 0
    ctrlShp.attr('%sCupRx_2' % fngr).value = -1
    ctrlShp.attr('%sCupRx_3' % fngr).value = -1
    ctrlShp.attr('%sCupRx_4' % fngr).value = -1
    ctrlShp.attr('%sSprd_2' % fngr).value = -9
    ctrlShp.attr('%sBsSprd_1' % fngr).value = -9

    print 'Creating Right Middle Rig'
    rgtMiddleObj = fingerRig.FingerRig(
                                            fngr='Middle',
                                            parent=rgtArmObj.Hand_Jnt,
                                            armCtrl=rgtArmObj.Arm_Ctrl,
                                            ctrlGrp=mgObj.Ctrl_Grp,
                                            tmpJnt=(
                                                        'Middle_1_R_Jnt',
                                                        'Middle_2_R_Jnt',
                                                        'Middle_3_R_Jnt',
                                                        'Middle_4_R_Jnt',
                                                        'Middle_5_R_Jnt'
                                                    ),
                                            side='R'
                                        )
    ctrlShp = lrc.Dag(rgtArmObj.Arm_Ctrl.shape)
    fngr = 'Middle'
    ctrlShp.attr('%sFlatRx_1' % fngr).value = -2
    ctrlShp.attr('%sFlatRz_1' % fngr).value = -0.2
    ctrlShp.attr('%sFlatRx_2' % fngr).value = -0.4
    ctrlShp.attr('%sFlatRx_3' % fngr).value = 3.5
    ctrlShp.attr('%sFlatRx_4' % fngr).value = 1.25
    ctrlShp.attr('%sCupRx_1' % fngr).value = 0
    ctrlShp.attr('%sCupRx_2' % fngr).value = -2.5
    ctrlShp.attr('%sCupRx_3' % fngr).value = -2.5
    ctrlShp.attr('%sCupRx_4' % fngr).value = -2.5
    ctrlShp.attr('%sSprd_2' % fngr).value = -4.5
    ctrlShp.attr('%sBsSprd_1' % fngr).value = -4.5

    print 'Creating Right Ring Rig'
    rgtRingObj = fingerRig.FingerRig(
                                            fngr='Ring',
                                            parent=rgtArmObj.Hand_Jnt,
                                            armCtrl=rgtArmObj.Arm_Ctrl,
                                            ctrlGrp=mgObj.Ctrl_Grp,
                                            tmpJnt=(
                                                        'Ring_1_R_Jnt',
                                                        'Ring_2_R_Jnt',
                                                        'Ring_3_R_Jnt',
                                                        'Ring_4_R_Jnt',
                                                        'Ring_5_R_Jnt'
                                                    ),
                                            side='R'
                                        )
    ctrlShp = lrc.Dag(rgtArmObj.Arm_Ctrl.shape)
    fngr = 'Ring'
    ctrlShp.attr('%sFlatRx_1' % fngr).value = -2
    ctrlShp.attr('%sFlatRx_2' % fngr).value = -0.2
    ctrlShp.attr('%sFlatRx_3' % fngr).value = 2.5
    ctrlShp.attr('%sFlatRx_4' % fngr).value = 2.5
    ctrlShp.attr('%sCupRx_1' % fngr).value = 0
    ctrlShp.attr('%sCupRx_2' % fngr).value = -3.5
    ctrlShp.attr('%sCupRx_3' % fngr).value = -3.5
    ctrlShp.attr('%sCupRx_4' % fngr).value = -3.5
    ctrlShp.attr('%sSprd_2' % fngr).value = 4.5
    ctrlShp.attr('%sBsSprd_1' % fngr).value = 4.5

    print 'Creating Right Pinky Rig'
    rgtPinkyObj = fingerRig.FingerRig(
                                            fngr='Pinky',
                                            parent=rgtArmObj.Hand_Jnt,
                                            armCtrl=rgtArmObj.Arm_Ctrl,
                                            ctrlGrp=mgObj.Ctrl_Grp,
                                            tmpJnt=(
                                                        'Pinky_1_R_Jnt',
                                                        'Pinky_2_R_Jnt',
                                                        'Pinky_3_R_Jnt',
                                                        'Pinky_4_R_Jnt',
                                                        'Pinky_5_R_Jnt'
                                                    ),
                                            side='R'
                                        )
    ctrlShp = lrc.Dag(rgtArmObj.Arm_Ctrl.shape)
    fngr = 'Pinky'
    ctrlShp.attr('%sFlatRx_1' % fngr).value = -2
    ctrlShp.attr('%sFlatRz_1' % fngr).value = 0.2
    ctrlShp.attr('%sFlatRx_2' % fngr).value = -0.5
    ctrlShp.attr('%sFlatRx_3' % fngr).value = 4.3
    ctrlShp.attr('%sFlatRx_4' % fngr).value = 0.65
    ctrlShp.attr('%sCupRx_1' % fngr).value = 0
    ctrlShp.attr('%sCupRx_2' % fngr).value = -4.5
    ctrlShp.attr('%sCupRx_3' % fngr).value = -4.5
    ctrlShp.attr('%sCupRx_4' % fngr).value = -4.5
    ctrlShp.attr('%sSprd_2' % fngr).value = 9
    ctrlShp.attr('%sBsSprd_1' % fngr).value = 9

    print 'Creating Left Leg Rig'
    lftLegObj = legRig.LegRig(
                                parent=torsoObj.Pelvis_Jnt,
                                ctrlGrp=mgObj.Ctrl_Grp,
                                jntGrp=mgObj.Jnt_Grp,
                                ikhGrp=mgObj.Ikh_Grp,
                                skinGrp=mgObj.Skin_Grp,
                                stillGrp=mgObj.Still_Grp,
                                ribbon=True,
                                tmpJnt=[
                                            'UpLeg_L_Jnt',
                                            'LowLeg_L_Jnt',
                                            'Ankle_L_Jnt',
                                            'Ball_L_Jnt',
                                            'Toe_L_Jnt',
                                            'HeelPiv_L_Jnt',
                                            'FootInPiv_L_Jnt',
                                            'FootOutPiv_L_Jnt',
                                            'KneeIk_L_Jnt'
                                        ],
                                part='',
                                side='L'
                            )

    print 'Creating Right Leg Rig'
    rgtLegObj = legRig.LegRig(
                                parent=torsoObj.Pelvis_Jnt,
                                ctrlGrp=mgObj.Ctrl_Grp,
                                jntGrp=mgObj.Jnt_Grp,
                                ikhGrp=mgObj.Ikh_Grp,
                                skinGrp=mgObj.Skin_Grp,
                                stillGrp=mgObj.Still_Grp,
                                ribbon=True,
                                tmpJnt=[
                                            'UpLeg_R_Jnt',
                                            'LowLeg_R_Jnt',
                                            'Ankle_R_Jnt',
                                            'Ball_R_Jnt',
                                            'Toe_R_Jnt',
                                            'HeelPiv_R_Jnt',
                                            'FootInPiv_R_Jnt',
                                            'FootOutPiv_R_Jnt',
                                            'KneeIk_R_Jnt'
                                        ],
                                part='',
                                side='R'
                            )

    print 'Creating Hair Rig'
    hairRigObj = pointRig.PointRig(
                                    parent=headObj.Head_Jnt, 
                                    ctrlGrp=mgObj.Ctrl_Grp, 
                                    tmpJnt='Hair_Jnt', 
                                    part='Hair', side='', shape='cube')

    print 'Creating Left ShoulderPad Rig'
    lShldPadRigObj = fkRig.FkRig(
                                    parent=lftArmObj.UpArm_Jnt, 
                                    ctrlGrp=mgObj.Ctrl_Grp, 
                                    tmpJnt=['ShoulderPad_L_Jnt', 'ShoulderPadTip_L_Jnt'], 
                                    part='ShldPad', side='L', ax='y', shape='cube')

    print 'Creating Right ShoulderPad Rig'
    rShldPadRigObj = fkRig.FkRig(
                                    parent=rgtArmObj.UpArm_Jnt, 
                                    ctrlGrp=mgObj.Ctrl_Grp, 
                                    tmpJnt=['ShoulderPad_R_Jnt', 'ShoulderPadTip_R_Jnt'], 
                                    part='ShldPad', side='R', ax='y', shape='cube')

    print 'Creating Belt Rig'
    beltRigObj = pointRig.PointRig(
                                    parent= 'SpineSkin_1_Jnt' , 
                                    ctrlGrp=mgObj.Ctrl_Grp, 
                                    tmpJnt='Belt_Jnt', 
                                    part='Belt', side='', shape='circle')

    
    print 'Creating HeadBeltA Rig'
    headBeltARigObj = pointRig.PointRig(
                                    parent='Belt_Jnt', 
                                    ctrlGrp=mgObj.Ctrl_Grp, 
                                    tmpJnt='HeadBeltA_Jnt', 
                                    part='headBeltA', side='', shape='cube')

    print 'Creating HeadBeltB Rig'
    headBeltBRigObj = pointRig.PointRig(
                                    parent='Belt_Jnt', 
                                    ctrlGrp=mgObj.Ctrl_Grp, 
                                    tmpJnt='HeadBeltB_Jnt', 
                                    part='headBeltB', side='', shape='cube')

    print 'Creating BagA Rig'
    BagARigObj = pointRig.PointRig(
                                    parent='Belt_Jnt', 
                                    ctrlGrp=mgObj.Ctrl_Grp, 
                                    tmpJnt='BagA_Jnt', 
                                    part='BagA', side='', shape='cube')

    print 'Creating BagB Rig'
    BagBRigObj = pointRig.PointRig(
                                    parent='Belt_Jnt', 
                                    ctrlGrp=mgObj.Ctrl_Grp, 
                                    tmpJnt='BagB_Jnt', 
                                    part='BagB', side='', shape='cube')

    print 'Creating BagC Rig'
    BagCRigObj = pointRig.PointRig(
                                    parent='Belt_Jnt', 
                                    ctrlGrp=mgObj.Ctrl_Grp, 
                                    tmpJnt='BagC_Jnt', 
                                    part='BagC', side='', shape='cube')

    print 'Creating Flashlight Rig'
    FlashlightRigObj = pointRig.PointRig(
                                    parent='Belt_Jnt', 
                                    ctrlGrp=mgObj.Ctrl_Grp, 
                                    tmpJnt='Flashlight_Jnt', 
                                    part='Flashlight', side='', shape='cube')

    print 'Creating Axe Rig'
    AxeRigObj = pointRig.PointRig(
                                    parent='Belt_Jnt', 
                                    ctrlGrp=mgObj.Ctrl_Grp, 
                                    tmpJnt='Axe_Jnt', 
                                    part='Axe', side='', shape='cube')

    print 'Creating Rope Rig'
    RopeRigObj = pointRig.PointRig(
                                    parent='Belt_Jnt', 
                                    ctrlGrp=mgObj.Ctrl_Grp, 
                                    tmpJnt='Rope_Jnt', 
                                    part='Rope', side='', shape='cube')

    print 'Creating Knife Rig'
    RopeRigObj = pointRig.PointRig(
                                    parent='RbnUpLegDtl_3_L_Jnt', 
                                    ctrlGrp=mgObj.Ctrl_Grp, 
                                    tmpJnt='Knife_Jnt', 
                                    part='Knife', side='', shape='cube')

    print 'Creating Rope Rig'
    RopeRigObj = pointRig.PointRig(
                                    parent='Belt_Jnt', 
                                    ctrlGrp=mgObj.Ctrl_Grp, 
                                    tmpJnt='RopeA_Jnt', 
                                    part='RopeA', side='', shape='circle')
    

    print 'Creating Left Shoulderpad Rbn Post Rig A'
    shldPadRbnAPost_L = pointRig.PointRig(
                                            parent=lShldPadRigObj.Jnts[0], 
                                            ctrlGrp=mgObj.Ctrl_Grp, 
                                            tmpJnt='ShoulderPadRbnAPost_L_Jnt', 
                                            part='ShoulderPadRbnAPost', side='L', shape='cube')
    
    print 'Creating Left Shoulderpad Rbn Post Rig B'
    shldPadRbnBPost_L = pointRig.PointRig(
                                            parent=torsoObj.Chest_Jnt, 
                                            ctrlGrp=mgObj.Ctrl_Grp, 
                                            tmpJnt='ShoulderPadRbnBPost_L_Jnt', 
                                            part='ShoulderPadRbnBPost', side='L', shape='cube')

    print 'Creating Left Shoulderpad1 Rbn Rig'
    shldPadRbnAB_L = ribbon.createRibbon(locA= shldPadRbnAPost_L.Jnt, locB=shldPadRbnBPost_L.Jnt,
                                        ax= 'y-', part = 'ShldpadAB' ,side= '_L_' ,hi=True)

    shldPadRbnAB_L.Ctrl_Grp.parent(mgObj.Ctrl_Grp)
    shldPadRbnAB_L.Jnt_Grp.parent(mgObj.Jnt_Grp)
    shldPadRbnAB_L.Skin_Grp.parent(mgObj.Skin_Grp)
    shldPadRbnAB_L.Still_Grp.parent(mgObj.Still_Grp)

    print 'Creating Right Shoulderpad Rbn Post Rig A'
    shldPadRbnAPost_R = pointRig.PointRig(
                                            parent=rShldPadRigObj.Jnts[0], 
                                            ctrlGrp=mgObj.Ctrl_Grp, 
                                            tmpJnt='ShoulderPadRbnAPost_R_Jnt', 
                                            part='ShoulderPadRbnAPost', side='R', shape='cube')
    
    print 'Creating Right Shoulderpad Rbn Post Rig B'
    shldPadRbnBPost_R = pointRig.PointRig(
                                            parent=torsoObj.Chest_Jnt, 
                                            ctrlGrp=mgObj.Ctrl_Grp, 
                                            tmpJnt='ShoulderPadRbnBPost_R_Jnt', 
                                            part='ShoulderPadRbnBPost', side='R', shape='cube')

    print 'Creating Right Shoulderpad1 Rbn Rig'
    shldPadRbnAB_R = ribbon.createRibbon(locA= shldPadRbnAPost_R.Jnt, locB=shldPadRbnBPost_R.Jnt,
                                        ax= 'y-', part = 'ShldpadAB' ,side= '_R_' ,hi=True)

    shldPadRbnAB_R.Ctrl_Grp.parent(mgObj.Ctrl_Grp)
    shldPadRbnAB_R.Jnt_Grp.parent(mgObj.Jnt_Grp)
    shldPadRbnAB_R.Skin_Grp.parent(mgObj.Skin_Grp)
    shldPadRbnAB_R.Still_Grp.parent(mgObj.Still_Grp)


    print 'Creating Left Shoulderpad Rbn Post Rig C'
    shldPadRbnCPost_L = pointRig.PointRig(
                                            parent=lShldPadRigObj.Jnts[0], 
                                            ctrlGrp=mgObj.Ctrl_Grp, 
                                            tmpJnt='ShoulderPadRbnCPost_L_Jnt', 
                                            part='ShoulderPadRbnCPost', side='L', shape='cube')
    
    print 'Creating Left Shoulderpad Rbn Post Rig D'
    shldPadRbnDPost_L = pointRig.PointRig(
                                            parent=torsoObj.Chest_Jnt, 
                                            ctrlGrp=mgObj.Ctrl_Grp, 
                                            tmpJnt='ShoulderPadRbnDPost_L_Jnt', 
                                            part='ShoulderPadRbnDPost', side='L', shape='cube')

    print 'Creating Left Shoulderpad2 Rbn Rig'
    shldPadRbnCD_L = ribbon.createRibbon(locA= shldPadRbnCPost_L.Jnt, locB=shldPadRbnDPost_L.Jnt,
                                        ax= 'y-', part = 'ShldpadCD' ,side= '_L_' ,hi=True)

    shldPadRbnCD_L.Ctrl_Grp.parent(mgObj.Ctrl_Grp)
    shldPadRbnCD_L.Jnt_Grp.parent(mgObj.Jnt_Grp)
    shldPadRbnCD_L.Skin_Grp.parent(mgObj.Skin_Grp)
    shldPadRbnCD_L.Still_Grp.parent(mgObj.Still_Grp)

    print 'Creating Right Shoulderpad Rbn Post Rig C'
    shldPadRbnCPost_R = pointRig.PointRig(
                                            parent=rShldPadRigObj.Jnts[0], 
                                            ctrlGrp=mgObj.Ctrl_Grp, 
                                            tmpJnt='ShoulderPadRbnCPost_R_Jnt', 
                                            part='ShoulderPadRbnCPost', side='R', shape='cube')
    
    print 'Creating Right Shoulderpad Rbn Post Rig D'
    shldPadRbnDPost_R = pointRig.PointRig(
                                            parent=torsoObj.Chest_Jnt, 
                                            ctrlGrp=mgObj.Ctrl_Grp, 
                                            tmpJnt='ShoulderPadRbnDPost_R_Jnt', 
                                            part='ShoulderPadRbnDPost', side='R', shape='cube')

    print 'Creating Right Shoulderpad2 Rbn Rig'
    shldPadRbnCD_R = ribbon.createRibbon(locA= shldPadRbnCPost_R.Jnt, locB=shldPadRbnDPost_R.Jnt,
                                        ax= 'y-', part = 'ShldpadCD' ,side= '_R_' ,hi=True)

    shldPadRbnCD_R.Ctrl_Grp.parent(mgObj.Ctrl_Grp)
    shldPadRbnCD_R.Jnt_Grp.parent(mgObj.Jnt_Grp)
    shldPadRbnCD_R.Skin_Grp.parent(mgObj.Skin_Grp)
    shldPadRbnCD_R.Still_Grp.parent(mgObj.Still_Grp)

    print 'Creating Eyedot Left Rig'
    eyeDotRigObj_L = pointRig.PointRig(
                                    parent='', 
                                    ctrlGrp=mgObj.Ctrl_Grp, 
                                    tmpJnt='EyeDot_L_Jnt', 
                                    part='EyeDot', side='L', shape='circle')
    
    print 'Creating Eyedot Right Rig'
    eyeDotRigObj_R = pointRig.PointRig(
                                    parent='', 
                                    ctrlGrp=mgObj.Ctrl_Grp, 
                                    tmpJnt='EyeDot_R_Jnt', 
                                    part='EyeDot', side='R', shape='circle')

    mc.delete('EyeDotPntCtrlOri_L_Grp_parentConstraint')
    mc.delete('EyeDotPntCtrlOri_R_Grp_parentConstraint')
    mc.parentConstraint('Eye_L_Ctrl', 'EyeDotPntCtrlZr_L_Grp' ,mo=True)
    mc.parentConstraint('Eye_R_Ctrl', 'EyeDotPntCtrlZr_R_Grp' ,mo=True)
    mc.parentConstraint('Eye_L_Ctrl', 'EyeDotPntCtrlZr_L_Grp' ,mo=True)
    mc.parentConstraint('Eye_R_Ctrl', 'EyeDotPntCtrlZr_R_Grp' ,mo=True)
    
    mc.addAttr( 'BeltPnt_CtrlShape' , ln = 'belt' , at = 'float' , k = True , min = 0 , max = 1 , dv = 1 )
    beltRev = mc.createNode( 'reverse' , n = 'BeltSwitch_Rev' )
    mc.connectAttr( 'BeltPnt_CtrlShape.belt' , '%s.ix' %beltRev )
    mc.connectAttr( '%s.ox' %beltRev , 'headBeltAPntCtrl_Grp.v' )
    mc.connectAttr( '%s.ox' %beltRev , 'headBeltBPntCtrl_Grp.v' )
    mc.connectAttr( '%s.ox' %beltRev , 'BagAPntCtrl_Grp.v' )
    mc.connectAttr( '%s.ox' %beltRev , 'BagBPntCtrl_Grp.v' )
    mc.connectAttr( '%s.ox' %beltRev , 'BagCPntCtrl_Grp.v' )
    mc.connectAttr( '%s.ox' %beltRev , 'AxePntCtrl_Grp.v' )
    mc.connectAttr( '%s.ox' %beltRev , 'RopePntCtrl_Grp.v' )
    mc.connectAttr( '%s.ox' %beltRev , 'KnifePntCtrl_Grp.v' )
    mc.connectAttr( '%s.ox' %beltRev , 'FlashlightPntCtrl_Grp.v' )

    print 'Creating Set Value Some Attribute'
    sac.setAttrChar()
    
    print 'Creating Cleanup'
    ctrlShapeTools.readAllCtrl()
    mc.delete('Export_Grp')
