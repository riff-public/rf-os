import maya.cmds as mc
from ncmel import core
reload(core)
from ncmel import rigTools
reload(rigTools)
from ncmel import mainRig
reload(mainRig)
from ncmel import rootRig
reload(rootRig)
from ncmel import pelvisRig
reload(pelvisRig)
from ncmel import spineRig
reload(spineRig)
from ncmel import torsoRig
reload(torsoRig)
from ncmel import neckRig
reload(neckRig)
from ncmel import headRig
reload(headRig)
from ncmel import eyeRig
reload(eyeRig)
from ncmel import jawRig
reload(jawRig)
from ncmel import clavicleRig
reload(clavicleRig)
from ncmel import armRig
reload(armRig)
from ncmel import legRig
reload(legRig)
from ncmel import legAnimalRig
reload(legAnimalRig)
from ncmel import fingerRig
reload(fingerRig)
from ncmel import subRig
reload(subRig)
from ncmel import fkRig
reload(fkRig)
from ncmel import tailRig
reload(tailRig)
from ncmel import mouthRig
reload(mouthRig)
from utaTools.utapy import utaCore
reload(utaCore)
from utaTools.pkmel import ctrlShapeTools
reload(ctrlShapeTools)
from lpRig import rubberBandRig
reload(rubberBandRig)
from utaTools.utapy import charTextName
reload(charTextName)
from nuTools.rigTools import proc
reload(proc)
from utils.crvScript import ctrlData
reload(ctrlData)

def main( size = 0.75 ):
    
    print "# Generate >> Main Group"
    main = mainRig.Run( 
                                    assetName = '' ,
                                    size      = size
                       )
    print "##------------------------------------------------------------------------------------"
    print "##-----------------------------MAIN GROUP---------------------------------------------"
    print "##------------------------------------------------------------------------------------"


    print "# Generate >> Root"
    root = rootRig.Run( 
                                    rootTmpJnt = 'Root_TmpJnt' ,
                                    ctrlGrp    =  main.mainCtrlGrp ,
                                    skinGrp    =  main.skinGrp ,
                                    size       =  size
                       )

    print "# Generate >> Torso"
    torso = torsoRig.Run(   
                                    pelvisTmpJnt = 'Pelvis_TmpJnt' ,
                                    spine1TmpJnt = 'Spine1_TmpJnt' ,
                                    spine2TmpJnt = 'Spine2_TmpJnt' ,
                                    spine3TmpJnt = 'Spine3_TmpJnt' ,
                                    spine4TmpJnt = 'Spine4_TmpJnt' ,
                                    spine5TmpJnt = 'Spine5_TmpJnt' ,
                                    parent       =  root.rootJnt ,
                                    ctrlGrp      =  main.mainCtrlGrp ,
                                    skinGrp      =  main.skinGrp ,
                                    jntGrp       =  main.jntGrp ,
                                    stillGrp     =  main.stillGrp ,
                                    elem         = '' ,
                                    axis         = 'y' ,
                                    size         =  size
                         )
    ## SetAttr SpineIk 
    mc.setAttr('%s.fkIk' % 'Spine_Ctrl', 1)

    print "# Generate >> Neck"
    neck = neckRig.Run(    
                                    neckTmpJnt = 'Neck_TmpJnt' ,
                                    headTmpJnt = 'Head_TmpJnt' ,
                                    parent     =  torso.jntDict[-1] ,
                                    ctrlGrp    =  main.mainCtrlGrp ,
                                    skinGrp    =  main.skinGrp ,
                                    elem       = '' ,
                                    side       = '' ,
                                    ribbon     = True ,
                                    head       = True ,
                                    axis       = 'y' ,
                                    size       = size
                       )
                     
    print "# Generate >> Head"
    head = headRig.Run(    
                                    headTmpJnt      = 'Head_TmpJnt' ,
                                    headEndTmpJnt   = 'HeadEnd_TmpJnt' ,
                                    eyeTrgTmpJnt    = 'EyeTrgt_TmpJnt' ,
                                    eyeLftTrgTmpJnt = 'EyeTrgt_L_TmpJnt' ,
                                    eyeRgtTrgTmpJnt = 'EyeTrgt_R_TmpJnt' ,
                                    eyeLftTmpJnt    = 'Eye_L_TmpJnt' ,
                                    eyeRgtTmpJnt    = 'Eye_R_TmpJnt' ,
                                    jawUpr1TmpJnt   = 'JawUpr1_TmpJnt' ,
                                    jawUprEndTmpJnt = 'JawUprEnd_TmpJnt' ,
                                    jawLwr1TmpJnt   = 'JawLwr1_TmpJnt' ,
                                    jawLwr2TmpJnt   = 'JawLwr2_TmpJnt' ,
                                    jawLwrEndTmpJnt = 'JawLwrEnd_TmpJnt' ,
                                    parent          =  neck.neckEndJnt ,
                                    ctrlGrp         =  main.mainCtrlGrp ,
                                    skinGrp         =  main.skinGrp ,
                                    elem            = '' ,
                                    side            = '' ,
                                    eyeRig          = False ,
                                    jawRig          = False ,
                                    size            = size 
                       )
                     
    print "# Generate >> Eye"
    eye = eyeRig.Run(    
                                    eyeTrgTmpJnt    = 'EyeTrgt_TmpJnt' ,
                                    eyeLftTrgTmpJnt = 'EyeTrgt_L_TmpJnt' ,
                                    eyeRgtTrgTmpJnt = 'EyeTrgt_R_TmpJnt' ,
                                    eyeLftTmpJnt    = 'Eye_L_TmpJnt' ,
                                    eyeRgtTmpJnt    = 'Eye_R_TmpJnt' ,
                                    parent          =  head.headJnt ,
                                    ctrlGrp         =  main.mainCtrlGrp ,
                                    skinGrp         =  main.skinGrp ,
                                    elem            = '' ,
                                    side            = '' ,
                                    size            = size 
                       )

    print "# Generate >> Jaw"
    jawObj = jawRig.Run(    
                                    jawUpr1TmpJnt   = 'JawUpr1_TmpJnt' ,
                                    jawUprEndTmpJnt = 'JawUprEnd_TmpJnt' ,
                                    jawLwr1TmpJnt   = 'JawLwr1_TmpJnt' ,
                                    jawLwr2TmpJnt   = 'JawLwr2_TmpJnt' ,
                                    jawLwrEndTmpJnt = 'JawLwrEnd_TmpJnt' ,
                                    parent          =  head.headJnt ,
                                    ctrlGrp         =  main.mainCtrlGrp ,
                                    skinGrp         =  main.skinGrp ,
                                    elem            = '' ,
                                    side            = '' ,
                                    size            = size 
                       )

    # print "# Generate >> Mouth"
    # mouth = mouthRig.Run(    
    #                                 lipsInTmpCuv     = 'LipsIn_CuvTemp' ,
    #                                 lipsOutTmpCuv    = 'LipsOut_CuvTemp' ,
    #                                 jawTmpJnt        =  jawObj.jawLwr2Jnt ,
    #                                 headTmpJnt       =  head.headJnt ,
    #                                 jawCtrl          =  jawObj.jawLwr1Ctrl ,
    #                                 numDtlJnt        =  68 ,
    #                                 skipJntCen       =  8 ,
    #                                 skinGrp          =  main.skinGrp ,
    #                                 elem             = '' ,
    #                                 side             = '' ,
    #                     ) 


    # print "# Generate >> Teeth Upper"
    # teethUpr = subRig.Run(              
    #                                 name       = 'TeethUpr' ,
    #                                 tmpJnt     = 'TeethUpr_TmpJnt' ,
    #                                 parent     = jaw.jawUpr1Jnt ,
    #                                 ctrlGrp    = main.ctrlGrp ,
    #                                 shape      = 'circle' ,
    #                                 side       = '' ,
    #                                 size       = size 
    #                       )

    # print "# Generate >> Teeth Lower"
    # teethLwr = subRig.Run(              
    #                                 name       = 'TeethLwr' ,
    #                                 tmpJnt     = 'TeethLwr_TmpJnt' ,
    #                                 parent     = jaw.jawLwr2Jnt ,
    #                                 ctrlGrp    = main.ctrlGrp ,
    #                                 shape      = 'circle' ,
    #                                 side       = '' ,
    #                                 size       = size 
    #                       )


    print "# Generate >> Clavicle Left"
    clavL = clavicleRig.Run(   
                                    clavTmpJnt  = 'Clav_L_TmpJnt' ,
                                    upArmTmpJnt = 'UpArm_L_TmpJnt' ,
                                    parent      =  torso.jntDict[-1] ,
                                    ctrlGrp     =  main.mainCtrlGrp ,
                                    jntGrp      =  main.jntGrp ,
                                    skinGrp     =  main.skinGrp ,
                                    elem        = '' ,
                                    side        = 'L' ,
                                    axis        = 'y' ,
                                    size        = size
                            )
    
    print "# Generate >> Clavicle Right"
    clavR = clavicleRig.Run(   
                                    clavTmpJnt  = 'Clav_R_TmpJnt' ,
                                    upArmTmpJnt = 'UpArm_R_TmpJnt' ,
                                    parent      =  torso.jntDict[-1] ,
                                    ctrlGrp     =  main.mainCtrlGrp ,
                                    jntGrp      =  main.jntGrp ,
                                    skinGrp     =  main.skinGrp ,
                                    elem        = '' ,
                                    side        = 'R' ,
                                    axis        = 'y' ,
                                    size        = size
                            )
     
    print "# Generate >> Arm Left"
    armL = armRig.Run(              upArmTmpJnt   = 'UpArm_L_TmpJnt' ,
                                    forearmTmpJnt = 'Forearm_L_TmpJnt' ,
                                    wristTmpJnt   = 'Wrist_L_TmpJnt' ,
                                    handTmpJnt    = 'Hand_L_TmpJnt' ,
                                    elbowTmpJnt   = 'Elbow_L_TmpJnt' ,
                                    parent        = 'ClavEnd_L_Jnt' , 
                                    ctrlGrp       =  main.mainCtrlGrp ,
                                    jntGrp        =  main.jntGrp ,
                                    skinGrp       =  main.skinGrp ,
                                    stillGrp      =  main.stillGrp ,
                                    ikhGrp        =  main.ikhGrp ,
                                    elem          = '' ,
                                    side          = 'L' ,
                                    ribbon        = True ,
                                    size          = size
                    )

    rigTools.addMoreSpace( obj = armL.wristIkLocWor ,
                           spaces = [ ('Shoulder' , armL.wristIkLocWor['localSpace'] ) ,
                                      ('Head', head.headJnt ) ,
                                      ('Chest', torso.jntDict[-1] ) ,
                                      ('Pelvis', torso.pelvisJnt ) ] )
    
    print "# Generate >> Arm Right"
    armR = armRig.Run(              
                                    upArmTmpJnt   = 'UpArm_R_TmpJnt' ,
                                    forearmTmpJnt = 'Forearm_R_TmpJnt' ,
                                    wristTmpJnt   = 'Wrist_R_TmpJnt' ,
                                    handTmpJnt    = 'Hand_R_TmpJnt' ,
                                    elbowTmpJnt   = 'Elbow_R_TmpJnt' ,
                                    parent        = 'ClavEnd_R_Jnt' , 
                                    ctrlGrp       =  main.mainCtrlGrp ,
                                    jntGrp        =  main.jntGrp ,
                                    skinGrp       =  main.skinGrp ,
                                    stillGrp      =  main.stillGrp ,
                                    ikhGrp        =  main.ikhGrp ,
                                    elem          = '' ,
                                    side          = 'R' ,
                                    ribbon        = True ,
                                    size          = size
                    )

    rigTools.addMoreSpace( obj = armR.wristIkLocWor ,
                           spaces = [ ('Shoulder' , armR.wristIkLocWor['localSpace'] ) ,
                                      ('Head', head.headJnt ) ,
                                      ('Chest', torso.jntDict[-1] ) ,
                                      ('Pelvis', torso.pelvisJnt ) ] )
    
    print "# Generate >> Leg Left"
    legL = legRig.Run(              
                                    upLegTmpJnt   = 'UpLeg_L_TmpJnt' ,
                                    lowLegTmpJnt  = 'LowLeg_L_TmpJnt' ,
                                    ankleTmpJnt   = 'Ankle_L_TmpJnt' ,
                                    ballTmpJnt    = 'Ball_L_TmpJnt' ,
                                    toeTmpJnt     = 'Toe_L_TmpJnt' ,
                                    heelTmpJnt    = 'Heel_L_TmpJnt' ,
                                    footInTmpJnt  = 'FootIn_L_TmpJnt' ,
                                    footOutTmpJnt = 'FootOut_L_TmpJnt' ,
                                    kneeTmpJnt    = 'Knee_L_TmpJnt' ,
                                    heelIkTwistPivTmpJnt = 'HeelIkTwistPiv_L_TmpJnt',
                                    parent        =  torso.pelvisJnt , 
                                    ctrlGrp       =  main.mainCtrlGrp ,
                                    jntGrp        =  main.jntGrp ,
                                    skinGrp       =  main.skinGrp ,
                                    stillGrp      =  main.stillGrp ,
                                    ikhGrp        =  main.ikhGrp ,
                                    elem          = '' ,
                                    side          = 'L' ,
                                    ribbon        = True ,
                                    foot          = True ,
                                    size          = size
                    )
    
    rigTools.addMoreSpace( obj = legL.kneeIkLocWor ,
                           spaces = [ ('Ankle' , legL.kneeIkLocWor['localSpace'] ) ,
                                      ('Root', root.rootJnt )] )


    print "# Generate >> Leg Right"
    legR = legRig.Run(              
                                    upLegTmpJnt   = 'UpLeg_R_TmpJnt' ,
                                    lowLegTmpJnt  = 'LowLeg_R_TmpJnt' ,
                                    ankleTmpJnt   = 'Ankle_R_TmpJnt' ,
                                    ballTmpJnt    = 'Ball_R_TmpJnt' ,
                                    toeTmpJnt     = 'Toe_R_TmpJnt' ,
                                    heelTmpJnt    = 'Heel_R_TmpJnt' ,
                                    footInTmpJnt  = 'FootIn_R_TmpJnt' ,
                                    footOutTmpJnt = 'FootOut_R_TmpJnt' ,
                                    kneeTmpJnt    = 'Knee_R_TmpJnt' ,
                                    heelIkTwistPivTmpJnt = 'HeelIkTwistPiv_R_TmpJnt',
                                    parent        =  torso.pelvisJnt , 
                                    ctrlGrp       =  main.mainCtrlGrp ,
                                    jntGrp        =  main.jntGrp ,
                                    skinGrp       =  main.skinGrp ,
                                    stillGrp      =  main.stillGrp ,
                                    ikhGrp        =  main.ikhGrp ,
                                    elem          = '' ,
                                    side          = 'R' ,
                                    ribbon        = True ,
                                    foot          = True ,
                                    size          = size
                    )
    rigTools.addMoreSpace( obj = legR.kneeIkLocWor ,
                           spaces = [ ('Ankle' , legR.kneeIkLocWor['localSpace'] ) ,
                                      ('Root', root.rootJnt )] )


    print "# Generate >> Eyedot Left Rig"
    eyeDotRigObj_L = subRig.Run(
                                    name       = 'EyeDot' ,
                                    tmpJnt     = 'EyeDot_L_TmpJnt' ,
                                    parent     =  eye.eyeLftJnt ,
                                    ctrlGrp    =  main.mainCtrlGrp ,
                                    shape      = 'circle' ,
                                    side       = 'L' ,
                                    size       = size  
                                )

    print "# Generate >> Eyedot Right Rig"
    eyeDotRigObj_R = subRig.Run(
                                    name       = 'EyeDot' ,
                                    tmpJnt     = 'EyeDot_R_TmpJnt' ,
                                    parent     =  eye.eyeRgtJnt ,
                                    ctrlGrp    =  main.mainCtrlGrp ,
                                    shape      = 'circle' ,
                                    side       = 'R' ,
                                    size       = size  
                                )  

    print "# Generate >> Iris Left Rig"
    IrisRigObj_L = subRig.Run(
                                    name       = 'Iris' ,
                                    tmpJnt     = 'Iris_L_TmpJnt' ,
                                    parent     = eye.eyeLftJnt,
                                    ctrlGrp    = main.mainCtrlGrp ,
                                    shape      = 'circle' ,
                                    side       = 'L' ,
                                    size       = size  
                                )

    print "# Generate >> Iris Left Rig"
    IrisRigObj_R = subRig.Run(
                                    name       = 'Iris' ,
                                    tmpJnt     = 'Iris_R_TmpJnt' ,
                                    parent     = eye.eyeRgtJnt ,
                                    ctrlGrp    = main.mainCtrlGrp ,
                                    shape      = 'circle' ,
                                    side       = 'R' ,
                                    size       = size  
                                )

    print "# Generate >> Pupil Left Rig"
    PupilRigObj_L = subRig.Run(
                                    name       = 'Pupil' ,
                                    tmpJnt     = 'Pupil_L_TmpJnt' ,
                                    parent     = 'IrisSub_L_Jnt'  ,
                                    ctrlGrp    =  main.mainCtrlGrp ,
                                    shape      = 'circle' ,
                                    side       = 'L' ,
                                    size       = size  
                                )

    print "# Generate >> Pupil Right Rig"
    PupilRigObj_R = subRig.Run(
                                    name       = 'Pupil' ,
                                    tmpJnt     = 'Pupil_R_TmpJnt' ,
                                    parent     = 'IrisSub_R_Jnt' ,
                                    ctrlGrp    =  main.mainCtrlGrp ,
                                    shape      = 'circle' ,
                                    side       = 'R' ,
                                    size       = size  
                                )

    print "# Generate >> Thumb Left"
    fngrThumbL = fingerRig.Run(         
                                    fngr          = 'Thumb' ,
                                    fngrTmpJnt    =['Thumb1_L_TmpJnt' , 
                                                    'Thumb2_L_TmpJnt' , 
                                                    'Thumb3_L_TmpJnt' , 
                                                    'Thumb4_L_TmpJnt' ] ,
                                    parent        =  armL.handJnt , 
                                    armCtrl       =  armL.armCtrl ,
                                    ctrlGrp       =  main.mainCtrlGrp ,
                                    elem          = '' ,
                                    side          = 'L' ,
                                    size          = size 
                             )

    finger = 'Thumb'
    ctrlShape = core.Dag(armL.armCtrl.shape)
    ctrlShape.attr('%s2FistRx' %finger).value = -4.5
    ctrlShape.attr('%s3FistRx' %finger).value = -9
    ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
    ctrlShape.attr('%s3SlideRx' %finger).value = -8
    ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
    ctrlShape.attr('%s3ScruchRx' %finger).value = -8

    
    print "# Generate >> Index Left"
    fngrIndexL = fingerRig.Run(         
                                    fngr          = 'Index' ,
                                    fngrTmpJnt    =['Index1_L_TmpJnt' , 
                                                    'Index2_L_TmpJnt' , 
                                                    'Index3_L_TmpJnt' , 
                                                    'Index4_L_TmpJnt' , 
                                                    'Index5_L_TmpJnt' ] ,
                                    parent        =  armL.handJnt , 
                                    armCtrl       =  armL.armCtrl ,
                                    ctrlGrp       =  main.mainCtrlGrp ,
                                    elem          = '' ,
                                    side          = 'L' ,
                                    size          = size 
                             )

    finger = 'Index'
    ctrlShape = core.Dag(armL.armCtrl.shape)
    ctrlShape.attr('%s2FistRx' %finger).value = -9
    ctrlShape.attr('%s3FistRx' %finger).value = -9
    ctrlShape.attr('%s4FistRx' %finger).value = -9
    ctrlShape.attr('%s2RelaxRx' %finger).value = -1
    ctrlShape.attr('%s3RelaxRx' %finger).value = -1
    ctrlShape.attr('%s4RelaxRx' %finger).value = -1
    ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
    ctrlShape.attr('%s3SlideRx' %finger).value = -8
    ctrlShape.attr('%s4SlideRx' %finger).value = 4.5
    ctrlShape.attr('%s1ScruchRx' %finger).value = 1.5
    ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
    ctrlShape.attr('%s3ScruchRx' %finger).value = -8
    ctrlShape.attr('%s4ScruchRx' %finger).value = -8
    ctrlShape.attr('%sBaseSpread' %finger).value = -4
    ctrlShape.attr('%sSpread' %finger).value = -4
    ctrlShape.attr('%sBaseBreak' %finger).value = -4.5
    ctrlShape.attr('%sBreak' %finger).value = -4.5
    ctrlShape.attr('%sBaseFlex' %finger).value = -9
    ctrlShape.attr('%sFlex' %finger).value = -9

    print "# Generate >> Hand Cup Left"
    handCupL = fingerRig.HandCup( 
                                     handCupTmpJnt = 'HandCup_L_TmpJnt' ,
                                     armCtrl       =  armL.armCtrl ,
                                     parent        =  armL.handJnt ,
                                     elem          = '' ,
                                     side          = 'L' ,
                                     size          = size 
                                )

    print "# Generate >> Middle Left"
    fngrMiddleL = fingerRig.Run(         
                                    fngr          = 'Middle' ,
                                    fngrTmpJnt    =['Middle1_L_TmpJnt' , 
                                                    'Middle2_L_TmpJnt' , 
                                                    'Middle3_L_TmpJnt' , 
                                                    'Middle4_L_TmpJnt' , 
                                                    'Middle5_L_TmpJnt' ] ,
                                    parent        =  handCupL.handCupJnt , 
                                    armCtrl       =  armL.armCtrl ,
                                    ctrlGrp       =  main.mainCtrlGrp ,
                                    elem          = '' ,
                                    side          = 'L' ,
                                    size          = size 
                               )

    finger = 'Middle'
    ctrlShape = core.Dag(armL.armCtrl.shape)
    ctrlShape.attr('%s2FistRx' %finger).value = -9
    ctrlShape.attr('%s3FistRx' %finger).value = -9
    ctrlShape.attr('%s4FistRx' %finger).value = -9
    ctrlShape.attr('%s1RelaxRx' %finger).value = -0.5
    ctrlShape.attr('%s2RelaxRx' %finger).value = -2.5
    ctrlShape.attr('%s3RelaxRx' %finger).value = -2.5
    ctrlShape.attr('%s4RelaxRx' %finger).value = -2.5
    ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
    ctrlShape.attr('%s3SlideRx' %finger).value = -8
    ctrlShape.attr('%s4SlideRx' %finger).value = 4.5
    ctrlShape.attr('%s1ScruchRx' %finger).value = 1.5
    ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
    ctrlShape.attr('%s3ScruchRx' %finger).value = -8
    ctrlShape.attr('%s4ScruchRx' %finger).value = -8
    ctrlShape.attr('%sBaseBreak' %finger).value = -4.5
    ctrlShape.attr('%sBreak' %finger).value = -4.5
    ctrlShape.attr('%sBaseFlex' %finger).value = -9
    ctrlShape.attr('%sFlex' %finger).value = -9
    



    print "# Generate >> Pinky Left"
    fngrPinkyL = fingerRig.Run(         
                                    fngr          = 'Pinky' ,
                                    fngrTmpJnt    =['Pinky1_L_TmpJnt' , 
                                                    'Pinky2_L_TmpJnt' , 
                                                    'Pinky3_L_TmpJnt' , 
                                                    'Pinky4_L_TmpJnt' , 
                                                    'Pinky5_L_TmpJnt' ] ,
                                    parent        =  handCupL.handCupJnt , 
                                    armCtrl       =  armL.armCtrl ,
                                    ctrlGrp       =  main.mainCtrlGrp ,
                                    elem          = '' ,
                                    side          = 'L' ,
                                    size          = size 
                               )

    finger = 'Pinky'
    ctrlShape = core.Dag(armL.armCtrl.shape)
    ctrlShape.attr('%s2FistRx' %finger).value = -9
    ctrlShape.attr('%s3FistRx' %finger).value = -9
    ctrlShape.attr('%s4FistRx' %finger).value = -9
    ctrlShape.attr('%s1RelaxRx' %finger).value = -2
    ctrlShape.attr('%s2RelaxRx' %finger).value = -3
    ctrlShape.attr('%s3RelaxRx' %finger).value = -3
    ctrlShape.attr('%s4RelaxRx' %finger).value = -3
    ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
    ctrlShape.attr('%s3SlideRx' %finger).value = -8
    ctrlShape.attr('%s4SlideRx' %finger).value = 4.5
    ctrlShape.attr('%s1ScruchRx' %finger).value = 1.5
    ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
    ctrlShape.attr('%s3ScruchRx' %finger).value = -8
    ctrlShape.attr('%s4ScruchRx' %finger).value = -8
    ctrlShape.attr('%sBaseSpread' %finger).value = 5
    ctrlShape.attr('%sSpread' %finger).value = 5
    ctrlShape.attr('%sBaseBreak' %finger).value = -4.5
    ctrlShape.attr('%sBreak' %finger).value = -4.5
    ctrlShape.attr('%sBaseFlex' %finger).value = -9
    ctrlShape.attr('%sFlex' %finger).value = -9


    print "# Generate >> Thumb Right"
    fngrThumbR = fingerRig.Run(         
                                    fngr          = 'Thumb' ,
                                    fngrTmpJnt    =['Thumb1_R_TmpJnt' , 
                                                    'Thumb2_R_TmpJnt' , 
                                                    'Thumb3_R_TmpJnt' , 
                                                    'Thumb4_R_TmpJnt' ] ,
                                    parent        =  armR.handJnt , 
                                    armCtrl       =  armR.armCtrl ,
                                    ctrlGrp       =  main.mainCtrlGrp ,
                                    elem          = '' ,
                                    side          = 'R' ,
                                    size          = size 
                             )

    finger = 'Thumb'
    ctrlShape = core.Dag(armR.armCtrl.shape)
    ctrlShape.attr('%s2FistRx' %finger).value = -4.5
    ctrlShape.attr('%s3FistRx' %finger).value = -9
    ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
    ctrlShape.attr('%s3SlideRx' %finger).value = -8
    ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
    ctrlShape.attr('%s3ScruchRx' %finger).value = -8
    

    print "# Generate >> Index Right"
    fngrIndexR = fingerRig.Run(         
                                    fngr          = 'Index' ,
                                    fngrTmpJnt    =['Index1_R_TmpJnt' , 
                                                    'Index2_R_TmpJnt' , 
                                                    'Index3_R_TmpJnt' , 
                                                    'Index4_R_TmpJnt' , 
                                                    'Index5_R_TmpJnt' ] ,
                                    parent        =  armR.handJnt , 
                                    armCtrl       =  armR.armCtrl ,
                                    ctrlGrp       =  main.mainCtrlGrp ,
                                    elem          = '' ,
                                    side          = 'R' ,
                                    size          = size 
                             )

    finger = 'Index'
    ctrlShape = core.Dag(armR.armCtrl.shape)
    ctrlShape.attr('%s2FistRx' %finger).value = -9
    ctrlShape.attr('%s3FistRx' %finger).value = -9
    ctrlShape.attr('%s4FistRx' %finger).value = -9
    ctrlShape.attr('%s2RelaxRx' %finger).value = -1
    ctrlShape.attr('%s3RelaxRx' %finger).value = -1
    ctrlShape.attr('%s4RelaxRx' %finger).value = -1
    ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
    ctrlShape.attr('%s3SlideRx' %finger).value = -8
    ctrlShape.attr('%s4SlideRx' %finger).value = 4.5
    ctrlShape.attr('%s1ScruchRx' %finger).value = 1.5
    ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
    ctrlShape.attr('%s3ScruchRx' %finger).value = -8
    ctrlShape.attr('%s4ScruchRx' %finger).value = -8
    ctrlShape.attr('%sBaseSpread' %finger).value = -4
    ctrlShape.attr('%sSpread' %finger).value = -4
    ctrlShape.attr('%sBaseBreak' %finger).value = -4.5
    ctrlShape.attr('%sBreak' %finger).value = -4.5
    ctrlShape.attr('%sBaseFlex' %finger).value = -9
    ctrlShape.attr('%sFlex' %finger).value = -9
    

    print "# Generate >> Hand Cup Right"
    handCupR = fingerRig.HandCup( 
                                     handCupTmpJnt = 'HandCup_R_TmpJnt' ,
                                     armCtrl       =  armR.armCtrl ,
                                     parent        =  armR.handJnt ,
                                     elem          = '' ,
                                     side          = 'R' ,
                                     size          = size 
                                )

    print "# Generate >> Middle Right"
    fngrMiddleR = fingerRig.Run(         
                                    fngr          = 'Middle' ,
                                    fngrTmpJnt    =['Middle1_R_TmpJnt' , 
                                                    'Middle2_R_TmpJnt' , 
                                                    'Middle3_R_TmpJnt' , 
                                                    'Middle4_R_TmpJnt' , 
                                                    'Middle5_R_TmpJnt' ] ,
                                    parent        =  handCupR.handCupJnt  , 
                                    armCtrl       =  armR.armCtrl ,
                                    ctrlGrp       =  main.mainCtrlGrp ,
                                    elem          = '' ,
                                    side          = 'R' ,
                                    size          = size 
                               )

    finger = 'Middle'
    ctrlShape = core.Dag(armR.armCtrl.shape)
    ctrlShape.attr('%s2FistRx' %finger).value = -9
    ctrlShape.attr('%s3FistRx' %finger).value = -9
    ctrlShape.attr('%s4FistRx' %finger).value = -9
    ctrlShape.attr('%s1RelaxRx' %finger).value = -0.5
    ctrlShape.attr('%s2RelaxRx' %finger).value = -2.5
    ctrlShape.attr('%s3RelaxRx' %finger).value = -2.5
    ctrlShape.attr('%s4RelaxRx' %finger).value = -2.5
    ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
    ctrlShape.attr('%s3SlideRx' %finger).value = -8
    ctrlShape.attr('%s4SlideRx' %finger).value = 4.5
    ctrlShape.attr('%s1ScruchRx' %finger).value = 1.5
    ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
    ctrlShape.attr('%s3ScruchRx' %finger).value = -8
    ctrlShape.attr('%s4ScruchRx' %finger).value = -8
    ctrlShape.attr('%sBaseBreak' %finger).value = -4.5
    ctrlShape.attr('%sBreak' %finger).value = -4.5
    ctrlShape.attr('%sBaseFlex' %finger).value = -9
    ctrlShape.attr('%sFlex' %finger).value = -9
    

    print "# Generate >> Pinky Right"
    fngrPinkyR = fingerRig.Run(         
                                    fngr          = 'Pinky' ,
                                    fngrTmpJnt    =['Pinky1_R_TmpJnt' , 
                                                    'Pinky2_R_TmpJnt' , 
                                                    'Pinky3_R_TmpJnt' , 
                                                    'Pinky4_R_TmpJnt' , 
                                                    'Pinky5_R_TmpJnt' ] ,
                                    parent        =  handCupR.handCupJnt , 
                                    armCtrl       =  armR.armCtrl ,
                                    ctrlGrp       =  main.mainCtrlGrp ,
                                    elem          = '' ,
                                    side          = 'R' ,
                                    size          = size 
                               )
    
    finger = 'Pinky'
    ctrlShape = core.Dag(armR.armCtrl.shape)
    ctrlShape.attr('%s2FistRx' %finger).value = -9
    ctrlShape.attr('%s3FistRx' %finger).value = -9
    ctrlShape.attr('%s4FistRx' %finger).value = -9
    ctrlShape.attr('%s1RelaxRx' %finger).value = -2
    ctrlShape.attr('%s2RelaxRx' %finger).value = -3
    ctrlShape.attr('%s3RelaxRx' %finger).value = -3
    ctrlShape.attr('%s4RelaxRx' %finger).value = -3
    ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
    ctrlShape.attr('%s3SlideRx' %finger).value = -8
    ctrlShape.attr('%s4SlideRx' %finger).value = 4.5
    ctrlShape.attr('%s1ScruchRx' %finger).value = 1.5
    ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
    ctrlShape.attr('%s3ScruchRx' %finger).value = -8
    ctrlShape.attr('%s4ScruchRx' %finger).value = -8
    ctrlShape.attr('%sBaseSpread' %finger).value = 5
    ctrlShape.attr('%sSpread' %finger).value = 5
    ctrlShape.attr('%sBaseBreak' %finger).value = -4.5
    ctrlShape.attr('%sBreak' %finger).value = -4.5
    ctrlShape.attr('%sBaseFlex' %finger).value = -9
    ctrlShape.attr('%sFlex' %finger).value = -9
    
    print '##------------------------------------------------------------------------------------'
    print '##-----------------------------ADD RIG GROUP------------------------------------------'
    print '##------------------------------------------------------------------------------------'


    print "# Generate >> Ear Left"
    earL = fkRig.Run(              
                                    name       = 'Ear' ,
                                    tmpJnt     = [ 'Ear1_L_TmpJnt' , 'Ear2_L_TmpJnt' , 'Ear3_L_TmpJnt' , 'Ear4_L_TmpJnt' ] ,
                                    parent     = head.headEndJnt ,
                                    ctrlGrp    = main.addRigCtrlGrp ,
                                    axis       = 'y' ,
                                    shape      = 'circle' ,
                                    side       = 'L' ,
                                    size       = size 
                    )

    print "# Generate >> Ear Right"
    earR = fkRig.Run(              
                                    name       = 'Ear' ,
                                    tmpJnt     = [ 'Ear1_R_TmpJnt' , 'Ear2_R_TmpJnt' , 'Ear3_R_TmpJnt' , 'Ear4_R_TmpJnt' ] ,
                                    parent     = head.headEndJnt ,
                                    ctrlGrp    = main.addRigCtrlGrp ,
                                    axis       = 'y' ,
                                    shape      = 'circle' ,
                                    side       = 'R' ,
                                    size       = size 
                    )

    print "# Generate >> ShoulderArmor Left"
    shoulderArmorL = subRig.Run(              
                                    name       = 'ShoulderArmor' ,
                                    tmpJnt     = 'ShoulderArmor_L_TmpJnt' ,
                                    parent     = armL.upArmNonRollRootNr ,
                                    ctrlGrp    = main.addRigCtrlGrp ,
                                    shape      = 'square' ,
                                    side       = 'L' ,
                                    size       = size 
                          )

    mc.parent( shoulderArmorL.jnt , armL.upArmJnt )

    print "# Generate >> ShoulderArmor Right"
    shoulderArmorR = subRig.Run(              
                                    name       = 'ShoulderArmor' ,
                                    tmpJnt     = 'ShoulderArmor_R_TmpJnt' ,
                                    parent     = armR.upArmNonRollRootNr ,
                                    ctrlGrp    = main.addRigCtrlGrp ,
                                    shape      = 'square' ,
                                    side       = 'R' ,
                                    size       = size 
                          )

    mc.parent( shoulderArmorR.jnt , armR.upArmJnt )

    print "# Generate >> Tail"
    tail = tailRig.Run(      name          = 'Tail' ,
                             numCtrl       = 5 ,
                             curveTmp      = 'Tail_CuvTemp' ,
                             curveUpVecTmp = 'TailUpvec_CuvTemp' ,
                             parent        = torso.pelvisJnt , 
                             ctrlGrp       = main.addRigCtrlGrp ,
                             jntGrp        = main.jntGrp ,
                             skinGrp       = main.skinGrp ,
                             stillGrp      = main.stillGrp ,
                             ikhGrp        = main.ikhGrp ,
                             elem          = '' ,
                             side          = '' ,
                             axis          = 'y' ,
                             size          = size
                       )

    print "# Generate >> PonyTail"
    ponyTail = tailRig.Run(  name          = 'PonyTail' ,
                             numCtrl       = 5 ,
                             curveTmp      = 'PonyTail_CuvTemp' ,
                             curveUpVecTmp = 'PonyTailUpvec_CuvTemp' ,
                             parent        = head.headJnt , 
                             ctrlGrp       = main.addRigCtrlGrp ,
                             jntGrp        = main.jntGrp ,
                             skinGrp       = main.skinGrp ,
                             stillGrp      = main.stillGrp ,
                             ikhGrp        = main.ikhGrp ,
                             elem          = '' ,
                             side          = '' ,
                             axis          = 'y' ,
                             size          = size
                       )

    print "# Generate >> Fur Left"
    furL = fkRig.Run(              
                                    name       = 'Fur' ,
                                    tmpJnt     = [ 'Fur1_L_TmpJnt' , 'Fur2_L_TmpJnt' , 'Fur3_L_TmpJnt' ] ,
                                    parent     = head.headJnt ,
                                    ctrlGrp    = main.addRigCtrlGrp ,
                                    axis       = 'y' ,
                                    shape      = 'square' ,
                                    side       = 'L' ,
                                    size       = size 
                    )

    print "# Generate >> Fur Right"
    furR = fkRig.Run(              
                                    name       = 'Fur' ,
                                    tmpJnt     = [ 'Fur1_R_TmpJnt' , 'Fur2_R_TmpJnt' , 'Fur3_R_TmpJnt' ] ,
                                    parent     = head.headJnt ,
                                    ctrlGrp    = main.addRigCtrlGrp ,
                                    axis       = 'y' ,
                                    shape      = 'square' ,
                                    side       = 'R' ,
                                    size       = size 
                    )

    print "# Generate >> HoodB"
    hoodB = fkRig.Run(              
                                    name       = 'HoodB' ,
                                    tmpJnt     = [ 'HoodBA_TmpJnt' , 'HoodBB_TmpJnt' , 'HoodBC_TmpJnt' , 'HoodBD_TmpJnt' ] ,
                                    parent     = 'SpineEnd_Jnt' ,
                                    ctrlGrp    = main.addRigCtrlGrp ,
                                    axis       = 'y' ,
                                    shape      = 'square' ,
                                    side       = '' ,
                                    size       = size 
                    )
    ## Unlock Group for HoodVis
    utaCore.unlockAttrs(listObj  =['HoodB1CtrlOfst_Grp'], 
                        lock = False, 
                        attrs = 'v'
                    )

    print "# Generate >> HoodBA"
    hoodBA = subRig.Run(              
                                    name       = 'HoodBA' ,
                                    tmpJnt     = 'HoodBA1_TmpJnt' ,
                                    parent     = hoodB.fkJntArray[0] ,
                                    ctrlGrp    = main.addRigCtrlGrp ,
                                    shape      = 'sphere' ,
                                    side       = '' ,
                                    size       = size 
                          )

    print "# Generate >> HoodBB"
    hoodBB = subRig.Run(              
                                    name       = 'HoodBB' ,
                                    tmpJnt     = 'HoodBB1_TmpJnt' ,
                                    parent     = hoodB.fkJntArray[1] ,
                                    ctrlGrp    = main.addRigCtrlGrp ,
                                    shape      = 'sphere' ,
                                    side       = '' ,
                                    size       = size 
                          )

    print "# Generate >> HoodBC"
    hoodBC = subRig.Run(              
                                    name       = 'HoodBC' ,
                                    tmpJnt     = 'HoodBC1_TmpJnt' ,
                                    parent     = hoodB.fkJntArray[2] ,
                                    ctrlGrp    = main.addRigCtrlGrp ,
                                    shape      = 'sphere' ,
                                    side       = '' ,
                                    size       = size 
                          )


    print "# Generate >> HoodA"
    HoodA = fkRig.Run(              
                                    name       = 'HoodA' ,
                                    tmpJnt     = [ 'HoodA1_TmpJnt' , 'HoodA2_TmpJnt' , 'HoodA3_TmpJnt' , 'HoodA4_TmpJnt' ] ,
                                    parent     = 'SpineEnd_Jnt' ,
                                    ctrlGrp    = main.addRigCtrlGrp ,
                                    axis       = 'y' ,
                                    shape      = 'square' ,
                                    side       = '' ,
                                    size       = size 
                    )

    ## Unlock Group for HoodVis
    utaCore.unlockAttrs(listObj  =['HoodA1CtrlOfst_Grp', 
                                    'HoodA2CtrlZro_Grp'], 
                        lock = False, 
                        attrs = ['tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v']
                    )
    mc.parentConstraint('Head_Jnt', 'HoodA2CtrlZro_Grp', mo = True)
    mc.scaleConstraint('Head_Jnt', 'HoodA2CtrlZro_Grp', mo = True)

    # print "# Generate >> HoodAUp"
    # HoodAUp = subRig.Run(              
    #                                 name       = 'HoodAUp' ,
    #                                 tmpJnt     = 'HoodAUp_TmpJnt' ,
    #                                 parent     = 'HoodA4Pos_Jnt' ,
    #                                 ctrlGrp    = main.ctrlGrp ,
    #                                 shape      = 'sphere' ,
    #                                 side       = '' ,
    #                                 size       = size 
    #                       )

    # print "# Generate >> HoodA1L"
    # HoodA1L = subRig.Run(              
    #                                 name       = 'HoodA1' ,
    #                                 tmpJnt     = 'HoodA1_L_TmpJnt' ,
    #                                 parent     = 'HoodA3_Jnt' ,
    #                                 ctrlGrp    = main.ctrlGrp ,
    #                                 shape      = 'sphere' ,
    #                                 side       = 'L' ,
    #                                 size       = size  
    #                       )

    # print "# Generate >> HoodA2L"
    # HoodA2L = subRig.Run(              
    #                                 name       = 'HoodA2' ,
    #                                 tmpJnt     = 'HoodA2_L_TmpJnt' ,
    #                                 parent     = 'HoodA3_Jnt' ,
    #                                 ctrlGrp    = main.ctrlGrp ,
    #                                 shape      = 'sphere' ,
    #                                 side       = 'L' ,
    #                                 size       = size  
    #                       )

    # print "# Generate >> HoodA3L"
    # HoodA3L = subRig.Run(              
    #                                 name       = 'HoodA3' ,
    #                                 tmpJnt     = 'HoodA3_L_TmpJnt' ,
    #                                 parent     = 'HoodA2_Jnt' ,
    #                                 ctrlGrp    = main.ctrlGrp ,
    #                                 shape      = 'sphere' ,
    #                                 side       = 'L' ,
    #                                 size       = size  
    #                       )

    # print "# Generate >> HoodA4L"
    # HoodA4L = subRig.Run(              
    #                                 name       = 'HoodA4' ,
    #                                 tmpJnt     = 'HoodA4_L_TmpJnt' ,
    #                                 parent     = 'HoodA1Pos_Jnt' ,
    #                                 ctrlGrp    = main.ctrlGrp ,
    #                                 shape      = 'sphere' ,
    #                                 side       = 'L' ,
    #                                 size       = size  
    #                       )

    # print "# Generate >> HoodA5L"
    # HoodA5L = subRig.Run(              
    #                                 name       = 'HoodA5' ,
    #                                 tmpJnt     = 'HoodA5_L_TmpJnt' ,
    #                                 parent     = 'HoodA1Pos_Jnt' ,
    #                                 ctrlGrp    = main.ctrlGrp ,
    #                                 shape      = 'sphere' ,
    #                                 side       = 'L' ,
    #                                 size       = size  
    #                       )


    # print "# Generate >> HoodA1L"
    # HoodA1R = subRig.Run(              
    #                                 name       = 'HoodA1' ,
    #                                 tmpJnt     = 'HoodA1_R_TmpJnt' ,
    #                                 parent     = 'HoodA3_Jnt' ,
    #                                 ctrlGrp    = main.ctrlGrp ,
    #                                 shape      = 'sphere' ,
    #                                 side       = 'R' ,
    #                                 size       = size  
    #                       )

    # print "# Generate >> HoodA2R"
    # HoodA2R = subRig.Run(              
    #                                 name       = 'HoodA2' ,
    #                                 tmpJnt     = 'HoodA2_R_TmpJnt' ,
    #                                 parent     = 'HoodA3_Jnt' ,
    #                                 ctrlGrp    = main.ctrlGrp ,
    #                                 shape      = 'sphere' ,
    #                                 side       = 'R' ,
    #                                 size       = size  
    #                       )

    # print "# Generate >> HoodA3R"
    # HoodA3R = subRig.Run(              
    #                                 name       = 'HoodA3' ,
    #                                 tmpJnt     = 'HoodA3_R_TmpJnt' ,
    #                                 parent     = 'HoodA2_Jnt' ,
    #                                 ctrlGrp    = main.ctrlGrp ,
    #                                 shape      = 'sphere' ,
    #                                 side       = 'R' ,
    #                                 size       = size  
    #                       )

    # print "# Generate >> HoodA4R"
    # HoodA4R = subRig.Run(              
    #                                 name       = 'HoodA4' ,
    #                                 tmpJnt     = 'HoodA4_R_TmpJnt' ,
    #                                 parent     = 'HoodA1Pos_Jnt' ,
    #                                 ctrlGrp    = main.ctrlGrp ,
    #                                 shape      = 'sphere' ,
    #                                 side       = 'R' ,
    #                                 size       = size  
    #                       )


    print "# Generate >> HoodRig Grp"
    hoodGrp = mc.createNode ('transform', n = 'HoodRig_Grp')
    mc.parent(hoodGrp, main.addRigCtrlGrp)
    mc.parent(  'HoodACtrl_Grp', 
                # 'HoodAUpSubCtrl_Grp', 
                # 'HoodA1SubCtrl_L_Grp', 
                # 'HoodA2SubCtrl_L_Grp',
                # 'HoodA3SubCtrl_L_Grp',
                # 'HoodA4SubCtrl_L_Grp',
                # 'HoodA5SubCtrl_L_Grp',
                # 'HoodA1SubCtrl_R_Grp', 
                # 'HoodA2SubCtrl_R_Grp',
                # 'HoodA3SubCtrl_R_Grp',
                # 'HoodA4SubCtrl_R_Grp',
                # 'HoodA5SubCtrl_R_Grp',
                hoodGrp
                )

    print "# Generate >> HoodHairBck"
    HoodHairBck = subRig.Run(              
                                    name       = 'HoodHairBck' ,
                                    tmpJnt     = 'HoodHairBck_TmpJnt' ,
                                    parent     = 'PonyTailIk2_Ctrl' ,
                                    ctrlGrp    = main.addRigCtrlGrp ,
                                    shape      = 'circle' ,
                                    side       = '' ,
                                    size       = size  
                          )
    mc.parent('HoodHairBckSub_Jnt','HoodA2Pos_Jnt')

    print "# Generate >> Belt"
    Belt = subRig.Run(              
                                    name       = 'Belt' ,
                                    tmpJnt     = 'Belt_TmpJnt' ,
                                    parent     = torso.pelvisJnt ,
                                    ctrlGrp    = main.addRigCtrlGrp ,
                                    shape      = 'circle' ,
                                    side       = '' ,
                                    size       = size  
                          )

    print "# Generate >> BeltFnt"
    BeltFnt = subRig.Run(              
                                    name       = 'BeltFnt' ,
                                    tmpJnt     = 'BeltFnt_TmpJnt' ,
                                    parent     = 'BeltSub_Jnt' ,
                                    ctrlGrp    = main.addRigCtrlGrp ,
                                    shape      = 'sphere' ,
                                    side       = '' ,
                                    size       = size  
                          )

    print "# Generate >> BeltBck"
    BeltBck = subRig.Run(              
                                    name       = 'BeltBck' ,
                                    tmpJnt     = 'BeltBck_TmpJnt' ,
                                    parent     = 'BeltSub_Jnt' ,
                                    ctrlGrp    = main.addRigCtrlGrp ,
                                    shape      = 'sphere' ,
                                    side       = '' ,
                                    size       = size  
                          )

    print "# Generate >> Belt1L"
    Belt1L = subRig.Run(              
                                    name       = 'Belt1' ,
                                    tmpJnt     = 'Belt1_L_TmpJnt' ,
                                    parent     = 'BeltSub_Jnt' ,
                                    ctrlGrp    = main.addRigCtrlGrp ,
                                    shape      = 'sphere' ,
                                    side       = 'L' ,
                                    size       = size  
                          )

    print "# Generate >> Belt2L"
    Belt2L = subRig.Run(              
                                    name       = 'Belt2' ,
                                    tmpJnt     = 'Belt2_L_TmpJnt' ,
                                    parent     = 'BeltSub_Jnt' ,
                                    ctrlGrp    = main.addRigCtrlGrp ,
                                    shape      = 'sphere' ,
                                    side       = 'L' ,
                                    size       = size  
                          )

    print "# Generate >> Belt3L"
    Belt3L = subRig.Run(              
                                    name       = 'Belt3' ,
                                    tmpJnt     = 'Belt3_L_TmpJnt' ,
                                    parent     = 'BeltSub_Jnt' ,
                                    ctrlGrp    = main.addRigCtrlGrp ,
                                    shape      = 'sphere' ,
                                    side       = 'L' ,
                                    size       = size  
                          )

    print "# Generate >> Belt1R"
    Belt1R = subRig.Run(              
                                    name       = 'Belt1' ,
                                    tmpJnt     = 'Belt1_R_TmpJnt' ,
                                    parent     = 'BeltSub_Jnt' ,
                                    ctrlGrp    = main.addRigCtrlGrp ,
                                    shape      = 'sphere' ,
                                    side       = 'R' ,
                                    size       = size  
                          )

    print "# Generate >> Belt2R"
    Belt2R = subRig.Run(              
                                    name       = 'Belt2' ,
                                    tmpJnt     = 'Belt2_R_TmpJnt' ,
                                    parent     = 'BeltSub_Jnt' ,
                                    ctrlGrp    = main.addRigCtrlGrp ,
                                    shape      = 'sphere' ,
                                    side       = 'R' ,
                                    size       = size  
                          )

    print "# Generate >> Belt3R"
    Belt3R = subRig.Run(              
                                    name       = 'Belt3' ,
                                    tmpJnt     = 'Belt3_R_TmpJnt' ,
                                    parent     = 'BeltSub_Jnt' ,
                                    ctrlGrp    = main.addRigCtrlGrp ,
                                    shape      = 'sphere' ,
                                    side       = 'R' ,
                                    size       = size  
                          )

    print "# Generate >> BeltRig Grp"
    beltGrp = mc.createNode ('transform', n = 'BeltRig_Grp')
    mc.parent(beltGrp, main.addRigCtrlGrp)
    mc.parent(  'BeltSubCtrl_Grp', 
                'BeltFntSubCtrl_Grp', 
                'BeltBckSubCtrl_Grp', 
                'Belt1SubCtrl_L_Grp',
                'Belt2SubCtrl_L_Grp',
                'Belt3SubCtrl_L_Grp',
                'Belt1SubCtrl_R_Grp',
                'Belt2SubCtrl_R_Grp', 
                'Belt3SubCtrl_R_Grp',
                beltGrp
                )

    print "# Generate >> BeltStrap"
    beltStrapObj = rubberBandRig.RubberLineRig(
                                                parent=torso.jntDict[-1],
                                                ctrlGrp=main.addRigCtrlGrp,
                                                jntGrp=main.jntGrp,
                                                skinGrp=main.skinGrp,
                                                stillGrp=main.stillGrp,
                                                crv='BeltStrapTmp_L_Crv',
                                                spans=6,
                                                jntNo=10,
                                                part='BeltStrap'
                                            )

    print "# Generate >> ClothALwr Left"
    ClothALwrLObj = fkRig.Run(              
                                    name       = 'ClothLwrA' ,
                                    tmpJnt     = [ 'ClothLwrA1_L_TmpJnt' , 'ClothLwrA2_L_TmpJnt' , 'ClothLwrA3_L_TmpJnt'  ] ,
                                    parent     = 'BeltSub_Jnt' ,
                                    ctrlGrp    = main.addRigCtrlGrp ,
                                    axis       = 'y' ,
                                    shape      = 'square' ,
                                    side       = 'L' ,
                                    size       = size 
                    )   

    print "# Generate >> ClothBLwr Left"
    ClothBLwrLObj = fkRig.Run(              
                                    name       = 'ClothLwrB' ,
                                    tmpJnt     = [ 'ClothLwrB1_L_TmpJnt' , 'ClothLwrB2_L_TmpJnt' , 'ClothLwrB3_L_TmpJnt'  ] ,
                                    parent     = 'BeltSub_Jnt' ,
                                    ctrlGrp    = main.addRigCtrlGrp ,
                                    axis       = 'y' ,
                                    shape      = 'square' ,
                                    side       = 'L' ,
                                    size       = size 
                    )

    print "# Generate >> ClothCLwr Left"
    ClothCLwrLObj = fkRig.Run(              
                                    name       = 'ClothLwrC' ,
                                    tmpJnt     = [ 'ClothLwrC1_L_TmpJnt' , 'ClothLwrC2_L_TmpJnt' , 'ClothLwrC3_L_TmpJnt'  ] ,
                                    parent     = 'BeltSub_Jnt'  ,
                                    ctrlGrp    = main.addRigCtrlGrp ,
                                    axis       = 'y' ,
                                    shape      = 'square' ,
                                    side       = 'L' ,
                                    size       = size 
                    )

    print "# Generate >> ClothDLwr Left"
    ClothDLwrLObj = fkRig.Run(              
                                    name       = 'ClothLwrD' ,
                                    tmpJnt     = [ 'ClothLwrD1_L_TmpJnt' , 'ClothLwrD2_L_TmpJnt' , 'ClothLwrD3_L_TmpJnt'  ] ,
                                    parent     = 'BeltSub_Jnt'  ,
                                    ctrlGrp    = main.addRigCtrlGrp ,
                                    axis       = 'y' ,
                                    shape      = 'square' ,
                                    side       = 'L' ,
                                    size       = size 
                    )

    print "# Generate >> ClothALwr Right"
    ClothALwrRObj = fkRig.Run(              
                                    name       = 'ClothLwrA' ,
                                    tmpJnt     = [ 'ClothLwrA1_R_TmpJnt' , 'ClothLwrA2_R_TmpJnt' , 'ClothLwrA3_R_TmpJnt'  ] ,
                                    parent     = 'BeltSub_Jnt'  ,
                                    ctrlGrp    = main.addRigCtrlGrp ,
                                    axis       = 'y' ,
                                    shape      = 'square' ,
                                    side       = 'R' ,
                                    size       = size 
                    )

    print "# Generate >> ClothBLwr Right"
    ClothBLwrRObj = fkRig.Run(              
                                    name       = 'ClothLwrB' ,
                                    tmpJnt     = [ 'ClothLwrB1_R_TmpJnt' , 'ClothLwrB2_R_TmpJnt' , 'ClothLwrB3_R_TmpJnt'  ] ,
                                    parent     = 'BeltSub_Jnt'  ,
                                    ctrlGrp    = main.addRigCtrlGrp ,
                                    axis       = 'y' ,
                                    shape      = 'square' ,
                                    side       = 'R' ,
                                    size       = size 
                    )

    print "# Generate >> ClothCLwr Right"
    ClothCLwrRObj = fkRig.Run(              
                                    name       = 'ClothLwrC' ,
                                    tmpJnt     = [ 'ClothLwrC1_R_TmpJnt' , 'ClothLwrC2_R_TmpJnt' , 'ClothLwrC3_R_TmpJnt'  ] ,
                                    parent     = 'BeltSub_Jnt'  ,
                                    ctrlGrp    = main.addRigCtrlGrp ,
                                    axis       = 'y' ,
                                    shape      = 'square' ,
                                    side       = 'R' ,
                                    size       = size 
                    )

    print "# Generate >> ClothDLwr Right"
    ClothDLwrRObj = fkRig.Run(              
                                    name       = 'ClothLwrD' ,
                                    tmpJnt     = [ 'ClothLwrD1_R_TmpJnt' , 'ClothLwrD2_R_TmpJnt' , 'ClothLwrD3_R_TmpJnt'  ] ,
                                    parent     = 'BeltSub_Jnt'  ,
                                    ctrlGrp    = main.addRigCtrlGrp ,
                                    axis       = 'y' ,
                                    shape      = 'square' ,
                                    side       = 'R' ,
                                    size       = size 
                    )
    print "# Generate >> ClothDLwr Back"
    ClothBLwrRObj = fkRig.Run(              
                                    name       = 'ClothLwrB' ,
                                    tmpJnt     = [ 'ClothLwrB1_TmpJnt' , 'ClothLwrB2_TmpJnt' , 'ClothLwrB3_TmpJnt'  ] ,
                                    parent     = 'BeltSub_Jnt'  ,
                                    ctrlGrp    = main.addRigCtrlGrp ,
                                    axis       = 'y' ,
                                    shape      = 'square' ,
                                    side       = '' ,
                                    size       = size 
                    )

    #------------------------------------------------------------------------
    print "# Generate >> ClothLwrA 1 Auto nonRoll Left"
    utaCore.nonRollFk(              elem = 'ClothLwrA1',
                                    side = 'L',
                                    objAttrs = 'ClothLwrA1_L_Ctrl', 
                                    source = 'UpLegNonRoll_L_Jnt', 
                                    target = 'ClothLwrA1CtrlOfst_L_Grp',
                                    valueAmp = 0.8,
                                    mins = 0,
                                    maxs = 90,
                                    axisSource = 'X',
                                    axisTarget = 'X',
                                        ) 
    print "# Generate >> ClothLwrA 2 Auto nonRoll Left"
    utaCore.nonRollFk(              elem = 'ClothLwrA2',
                                    side = 'L',
                                    objAttrs = 'ClothLwrA1_L_Ctrl', 
                                    source = 'UpLegNonRoll_L_Jnt', 
                                    target = 'ClothLwrB1CtrlOfst_L_Grp',
                                    valueAmp = 0.25,
                                    mins = 0,
                                    maxs = 90,
                                    axisSource = 'X',
                                    axisTarget = 'X',
                                        ) 

    print "# Generate >> ClothLwrA 3 Auto nonRoll Left"
    utaCore.nonRollFk(              elem = 'ClothLwrA3',
                                    side = 'L',
                                    objAttrs = 'ClothLwrA1_L_Ctrl', 
                                    source = 'UpLegNonRoll_L_Jnt', 
                                    target = 'ClothLwrB1CtrlOfst_L_Grp',
                                    valueAmp = -0.25,
                                    mins = 0,
                                    maxs = 90,
                                    axisSource = 'X',
                                    axisTarget = 'Z',
                                        ) 

    print "# Generate >> ClothLwrB 1 Auto nonRoll Left"
    utaCore.nonRollFk(              elem = 'ClothLwrB1',
                                    side = 'L',
                                    objAttrs = 'ClothLwrB1_L_Ctrl', 
                                    source = 'UpLegNonRoll_L_Jnt', 
                                    target = 'ClothLwrB1CtrlOfst_L_Grp',
                                    valueAmp = 0.9,
                                    mins = 0,
                                    maxs = 90,
                                    axisSource = 'Z',
                                    axisTarget = 'X',
                                        ) 
    print "# Generate >> ClothLwrB 2 Auto nonRoll Left"
    utaCore.nonRollFk(              elem = 'ClothLwrB2',
                                    side = 'L',
                                    objAttrs = 'ClothLwrB1_L_Ctrl', 
                                    source = 'UpLegNonRoll_L_Jnt', 
                                    target = 'ClothLwrA1CtrlOfst_L_Grp',
                                    valueAmp = 0.25,
                                    mins = 0,
                                    maxs = 90,
                                    axisSource = 'Z',
                                    axisTarget = 'X',
                                        ) 

    print "# Generate >> ClothLwrB 3 Auto nonRoll Left"
    utaCore.nonRollFk(              elem = 'ClothLwrB3',
                                    side = 'L',
                                    objAttrs = 'ClothLwrB1_L_Ctrl', 
                                    source = 'UpLegNonRoll_L_Jnt', 
                                    target = 'ClothLwrC1CtrlOfst_L_Grp',
                                    valueAmp = 0.5,
                                    mins = 0,
                                    maxs = 90,
                                    axisSource = 'Z',
                                    axisTarget = 'X',
                                        )

    print "# Generate >> ClothLwrD 1 Auto nonRoll Left"
    utaCore.nonRollFk(              elem = 'ClothLwrD1',
                                    side = 'L',
                                    objAttrs = 'ClothLwrD1_L_Ctrl', 
                                    source = 'UpLegNonRoll_L_Jnt', 
                                    target = 'ClothLwrD1CtrlOfst_L_Grp',
                                    valueAmp = -0.8,
                                    mins = -90,
                                    maxs = 0,
                                    axisSource = 'X',
                                    axisTarget = 'X',
                                        )    

    print "# Generate >> ClothLwrD 2 Auto nonRoll Left"
    utaCore.nonRollFk(              elem = 'ClothLwrD2',
                                    side = 'L',
                                    objAttrs = 'ClothLwrD1_L_Ctrl', 
                                    source = 'UpLegNonRoll_L_Jnt', 
                                    target = 'ClothLwrC1CtrlOfst_L_Grp',
                                    valueAmp = -0.25,
                                    mins = -90,
                                    maxs = 0,
                                    axisSource = 'X',
                                    axisTarget = 'X',
                                        )  

    print "# Generate >> ClothLwrD 3 Auto nonRoll Left"
    utaCore.nonRollFk(              elem = 'ClothLwrD3',
                                    side = 'L',
                                    objAttrs = 'ClothLwrD1_L_Ctrl', 
                                    source = 'UpLegNonRoll_L_Jnt', 
                                    target = 'ClothLwrC1CtrlOfst_L_Grp',
                                    valueAmp = -0.25,
                                    mins = -90,
                                    maxs = 0,
                                    axisSource = 'X',
                                    axisTarget = 'Z',
                                        )
    #----------------------------------------------------------------------------
    #------------------------------------------------------------------------
    print "# Generate >> ClothLwrA 1 Auto nonRoll Right"
    utaCore.nonRollFk(              elem = 'ClothLwrA1',
                                    side = 'R',
                                    objAttrs = 'ClothLwrA1_R_Ctrl', 
                                    source = 'UpLegNonRoll_R_Jnt', 
                                    target = 'ClothLwrA1CtrlOfst_R_Grp',
                                    valueAmp = 0.8,
                                    mins = 0,
                                    maxs = 90,
                                    axisSource = 'X',
                                    axisTarget = 'X',
                                        ) 
    print "# Generate >> ClothLwrA 2 Auto nonRoll Right"
    utaCore.nonRollFk(              elem = 'ClothLwrA2',
                                    side = 'R',
                                    objAttrs = 'ClothLwrA1_R_Ctrl', 
                                    source = 'UpLegNonRoll_R_Jnt', 
                                    target = 'ClothLwrB1CtrlOfst_R_Grp',
                                    valueAmp = 0.25,
                                    mins = 0,
                                    maxs = 90,
                                    axisSource = 'X',
                                    axisTarget = 'X',
                                        ) 

    print "# Generate >> ClothLwrA 3 Auto nonRoll Right"
    utaCore.nonRollFk(              elem = 'ClothLwrA3',
                                    side = 'R',
                                    objAttrs = 'ClothLwrA1_R_Ctrl', 
                                    source = 'UpLegNonRoll_R_Jnt', 
                                    target = 'ClothLwrB1CtrlOfst_R_Grp',
                                    valueAmp = -0.25,
                                    mins = 0,
                                    maxs = 90,
                                    axisSource = 'X',
                                    axisTarget = 'Z',
                                        ) 

    print "# Generate >> ClothLwrB 1 Auto nonRoll Right"
    utaCore.nonRollFk(              elem = 'ClothLwrB1',
                                    side = 'R',
                                    objAttrs = 'ClothLwrB1_R_Ctrl', 
                                    source = 'UpLegNonRoll_R_Jnt', 
                                    target = 'ClothLwrB1CtrlOfst_R_Grp',
                                    valueAmp = 0.9,
                                    mins = 0,
                                    maxs = 90,
                                    axisSource = 'Z',
                                    axisTarget = 'X',
                                        ) 
    print "# Generate >> ClothLwrB 2 Auto nonRoll Right"
    utaCore.nonRollFk(              elem = 'ClothLwrB2',
                                    side = 'R',
                                    objAttrs = 'ClothLwrB1_R_Ctrl', 
                                    source = 'UpLegNonRoll_R_Jnt', 
                                    target = 'ClothLwrA1CtrlOfst_R_Grp',
                                    valueAmp = 0.25,
                                    mins = 0,
                                    maxs = 90,
                                    axisSource = 'Z',
                                    axisTarget = 'X',
                                        ) 

    print "# Generate >> ClothLwrB 3 Auto nonRoll Right"
    utaCore.nonRollFk(              elem = 'ClothLwrB3',
                                    side = 'R',
                                    objAttrs = 'ClothLwrB1_R_Ctrl', 
                                    source = 'UpLegNonRoll_R_Jnt', 
                                    target = 'ClothLwrC1CtrlOfst_R_Grp',
                                    valueAmp = 0.5,
                                    mins = 0,
                                    maxs = 90,
                                    axisSource = 'Z',
                                    axisTarget = 'X',
                                        )

    print "# Generate >> ClothLwrD 1 Auto nonRoll Right"
    utaCore.nonRollFk(              elem = 'ClothLwrD1',
                                    side = 'R',
                                    objAttrs = 'ClothLwrD1_R_Ctrl', 
                                    source = 'UpLegNonRoll_R_Jnt', 
                                    target = 'ClothLwrD1CtrlOfst_R_Grp',
                                    valueAmp = -0.8,
                                    mins = -90,
                                    maxs = 0,
                                    axisSource = 'X',
                                    axisTarget = 'X',
                                        )    

    print "# Generate >> ClothLwrD 2 Auto nonRoll Right"
    utaCore.nonRollFk(              elem = 'ClothLwrD2',
                                    side = 'R',
                                    objAttrs = 'ClothLwrD1_R_Ctrl', 
                                    source = 'UpLegNonRoll_R_Jnt', 
                                    target = 'ClothLwrC1CtrlOfst_R_Grp',
                                    valueAmp = -0.25,
                                    mins = -90,
                                    maxs = 0,
                                    axisSource = 'X',
                                    axisTarget = 'X',
                                        )  

    print "# Generate >> ClothLwrD 3 Auto nonRoll Right"
    utaCore.nonRollFk(              elem = 'ClothLwrD3',
                                    side = 'R',
                                    objAttrs = 'ClothLwrD1_R_Ctrl', 
                                    source = 'UpLegNonRoll_R_Jnt', 
                                    target = 'ClothLwrC1CtrlOfst_R_Grp',
                                    valueAmp = -0.25,
                                    mins = -90,
                                    maxs = 0,
                                    axisSource = 'X',
                                    axisTarget = 'Z',
                                        )
    #----------------------------------------------------------------------------
    print "# Generate >> Unlocck Attrs "
    listObj = [ 'ClothLwrA1CtrlOfst_L_Grp', 
                'ClothLwrB1CtrlOfst_L_Grp',
                'ClothLwrC1CtrlOfst_L_Grp', 
                'ClothLwrD1CtrlOfst_L_Grp',
                'ClothLwrA1CtrlOfst_R_Grp', 
                'ClothLwrB1CtrlOfst_R_Grp',
                'ClothLwrC1CtrlOfst_R_Grp', 
                'ClothLwrD1CtrlOfst_R_Grp']
    utaCore.lockAttrObj(listObj = listObj, lock = False)
 
    print "# Generate >> ParentScaleConstraint  Upleg_L_Jnt at clothLwr"
    utaCore.parentScaleConstraintLoop(  obj = 'BeltStrapRbl_1_Ctrl', 
                                        lists =  [ 'BeltStrapRblCtrlZr_7_Grp'],
                                        par = True,
                                        sca = True, 
                                        ori = False, 
                                        poi = False
                        )
    mc.setAttr('BeltStrapRblCtrlZr_7_Grp.v', 0)
    mc.setAttr('BeltStrapRblDtlCtrlZr_10_Grp.v', 0)


    ## JawUprRoot_Jnt for hdSquash, Stretch -----------------------------------------
    ## JawUprRoot_Jnt for hdSquash, Stretch -----------------------------------------

    print "# Generate >> JawUprRoot"
    jawUprRoot = subRig.Run(              
                                    name       = 'JawUprRoot' ,
                                    tmpJnt     = 'JawUprRoot_TmpJnt' ,
                                    parent     = head.headJnt ,
                                    ctrlGrp    = main.addRigCtrlGrp ,
                                    shape      = 'square' ,
                                    side       = '' ,
                                    size       = size 
                            )
    mc.setAttr('JawUprRootSubCtrl_Grp.v', 0)
    utaCore.lockAttr(listObj = ['JawUpr1CtrlZro_Grp'], attrs = ['tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v'],lock = False,keyable = True)
    utaCore.parentScaleConstraintLoop(obj = 'JawUprRootSub_Jnt', lists = ['JawUpr1CtrlZro_Grp'],par = True, sca = True, ori = False, poi = False)
    utaCore.lockAttr(listObj = ['JawUpr1CtrlZro_Grp'], attrs = ['tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v'],lock = True,keyable = True)

#------------------------------------------------------------------------------------------------------------
#------------------------------------------btwJnt------------------------------------------------------------------
    print "# Generate >> BtwJnt"
    ## btwJnt >> Head
    proc.btwJnt(jnts=['Neck5RbnDtl_Jnt', 'Head_Jnt', 'HeadEnd_Jnt'],axis='y', pointConstraint=True)
    proc.btwJnt(jnts=['Spine5_Jnt', 'Neck_Jnt', 'Neck1RbnDtl_Jnt'],axis='y', pointConstraint=True)
    proc.btwJnt(jnts=['Spine5_Jnt', 'Clav_L_Jnt', 'UpArm_L_Jnt'],axis='y', pointConstraint=True)
    proc.btwJnt(jnts=['Spine5_Jnt', 'Clav_R_Jnt', 'UpArm_R_Jnt'],axis='y', pointConstraint=True)
    proc.btwJnt(jnts=['UpArm5RbnDtl_L_Jnt', 'Forearm_L_Jnt', 'Forearm1RbnDtl_L_Jnt'],axis='y', pointConstraint=False)
    proc.btwJnt(jnts=['UpArm5RbnDtl_R_Jnt', 'Forearm_R_Jnt', 'Forearm1RbnDtl_R_Jnt'],axis='y', pointConstraint=False)
    proc.btwJnt(jnts=['Forearm5RbnDtl_L_Jnt', 'Wrist_L_Jnt', 'Hand_L_Jnt'],axis='y', pointConstraint=True)
    proc.btwJnt(jnts=['Forearm5RbnDtl_R_Jnt', 'Wrist_R_Jnt', 'Hand_R_Jnt'],axis='y', pointConstraint=True)
    proc.btwJnt(jnts=['Pelvis_Jnt', 'UpLeg_L_Jnt', 'UpLeg1RbnDtl_L_Jnt'],axis='y', pointConstraint=True)
    proc.btwJnt(jnts=['Pelvis_Jnt', 'UpLeg_R_Jnt', 'UpLeg1RbnDtl_R_Jnt'],axis='y', pointConstraint=True)
    proc.btwJnt(jnts=['UpLeg5RbnDtl_L_Jnt', 'LowLeg_L_Jnt', 'LowLeg1RbnDtl_L_Jnt'],axis='y', pointConstraint=False)
    proc.btwJnt(jnts=['UpLeg5RbnDtl_R_Jnt', 'LowLeg_R_Jnt', 'LowLeg1RbnDtl_R_Jnt'],axis='y', pointConstraint=False)
    proc.btwJnt(jnts=['Spine4Sca_Jnt', 'Spine5_Jnt', 'Neck_Jnt'],axis='y', pointConstraint=True)

##---------------------------------------set preferred ankle Leg Ik --------------------------------------
##---------------------------------------set preferred ankle Leg Ik --------------------------------------
    print "# Generate >> Set preferred ankle Leg Ik"
    utaCore.preferredAnkleLegIk()

    print '##------------------------------------------------------------------------------------'
    print '##-----------------------------CLEAR UTILITIE-----------------------------------------'
    print '##------------------------------------------------------------------------------------'

    print "# Generate >> Read Control Shape"
    # ctrlShapeTools.readAllCtrl()
    ctrlData.read_ctrl_dataFld()

    print "# Generate >> Character Text Name"
    charTextName.charTextName(value = '3', size = '1')


 

    print "# Generate >> Set Defalut Attribute"
    mc.setAttr( 'TailFk1_Ctrl.localWorld' , 1 )
    mc.setAttr( 'PonyTailFk1_Ctrl.localWorld' , 1 )
    rigTools.jntRadius( r = 0.025 )
    mc.delete( 'TmpJnt_Grp' )

    print "# Generate >> DONE !! "