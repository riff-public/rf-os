from nuTools.rigTools import bpmRig as bpmRig
reload(bpmRig)
from utaTools.pkmel import ctrlShapeTools
reload(ctrlShapeTools)
import maya.cmds as mc 


def BpmRig(*args):
    ## Create Group
    GrpCtrl = mc.group(em = True, n = 'BpmRigCtrl_Grp')
    GrpStill = mc.group(em = True, n = 'BpmRigStill_Grp')

    ## Create Collar Bmp Control
    print "## Generate >> Crete CollarFnt "
    CollarFntObj = bpmRig.BpmFkRig(jnts=['CollarFnt1BpmSca_Jnt', 'CollarFnt2BpmSca_Jnt', 'CollarFnt3BpmSca_Jnt'],
                                    parent='Spine5BpmSca_Jnt',
                                    rootJnt='CollarSubBpmRoot_Jnt',
                                    inputGeometry='Cloth_Geo',
                                    skinCluster='skinCluster246',
                                    ctrlShp='crossCircle',
                                    ctrlColor='red',
                                    elem = 'CollarFnt',
                                    side = '')
    CollarFntObj.rig()
    
    print "## Generate >> Crete CollarA L "
    CollarALObj = bpmRig.BpmFkRig(jnts=['CollarA1BpmSca_L_Jnt', 'CollarA2BpmSca_L_Jnt', 'CollarA3BpmSca_L_Jnt'],
                                    parent='Spine5BpmSca_Jnt',
                                    rootJnt='CollarSubBpmRoot_Jnt',
                                    inputGeometry='Cloth_Geo',
                                    skinCluster='skinCluster246',
                                    ctrlShp='crossCircle',
                                    ctrlColor='red',
                                    elem = 'CollarA',
                                    side = '_L')
    CollarALObj.rig()

    print "## Generate >> Crete CollarB L "
    CollarBLObj = bpmRig.BpmFkRig(jnts=['CollarB1BpmSca_L_Jnt', 'CollarB2BpmSca_L_Jnt', 'CollarB3BpmSca_L_Jnt'],
                                    parent='Spine5BpmSca_Jnt',
                                    rootJnt='CollarSubBpmRoot_Jnt',
                                    inputGeometry='Cloth_Geo',
                                    skinCluster='skinCluster246',
                                    ctrlShp='crossCircle',
                                    ctrlColor='red',
                                    elem = 'CollarB',
                                    side = '_L')
    CollarBLObj.rig()

    print "## Generate >> Crete CollarC L "
    CollarCLObj = bpmRig.BpmFkRig(jnts=['CollarC1BpmSca_L_Jnt', 'CollarC2BpmSca_L_Jnt', 'CollarC3BpmSca_L_Jnt'],
                                    parent='Spine5BpmSca_Jnt',
                                    rootJnt='CollarSubBpmRoot_Jnt',
                                    inputGeometry='Cloth_Geo',
                                    skinCluster='skinCluster246',
                                    ctrlShp='crossCircle',
                                    ctrlColor='red',
                                    elem = 'CollarC',
                                    side = '_L')
    CollarCLObj.rig()



    print "## Generate >> Crete CollarA R "
    CollarARObj = bpmRig.BpmFkRig(jnts=['CollarA1BpmSca_R_Jnt', 'CollarA2BpmSca_R_Jnt', 'CollarA3BpmSca_R_Jnt'],
                                    parent='Spine5BpmSca_Jnt',
                                    rootJnt='CollarSubBpmRoot_Jnt',
                                    inputGeometry='Cloth_Geo',
                                    skinCluster='skinCluster246',
                                    ctrlShp='crossCircle',
                                    ctrlColor='red',
                                    elem = 'CollarA',
                                    side = '_R')
    CollarARObj.rig()

    print "## Generate >> Crete CollarB R "
    CollarBRObj = bpmRig.BpmFkRig(jnts=['CollarB1BpmSca_R_Jnt', 'CollarB2BpmSca_R_Jnt', 'CollarB3BpmSca_R_Jnt'],
                                    parent='Spine5BpmSca_Jnt',
                                    rootJnt='CollarSubBpmRoot_Jnt',
                                    inputGeometry='Cloth_Geo',
                                    skinCluster='skinCluster246',
                                    ctrlShp='crossCircle',
                                    ctrlColor='red',
                                    elem = 'CollarB',
                                    side = '_R')
    CollarBRObj.rig()

    print "## Generate >> Crete CollarC R "
    CollarCRObj = bpmRig.BpmFkRig(jnts=['CollarC1BpmSca_R_Jnt', 'CollarC2BpmSca_R_Jnt', 'CollarC3BpmSca_R_Jnt'],
                                    parent='Spine5BpmSca_Jnt',
                                    rootJnt='CollarSubBpmRoot_Jnt',
                                    inputGeometry='Cloth_Geo',
                                    skinCluster='skinCluster246',
                                    ctrlShp='crossCircle',
                                    ctrlColor='red',
                                    elem = 'CollarC',
                                    side = '_R')
    CollarCRObj.rig()

    print "## Generate >> Crete CollarBck "
    CollarBckObj = bpmRig.BpmFkRig(jnts=['CollarBck1BpmSca_Jnt', 'CollarBck2BpmSca_Jnt', 'CollarBck3BpmSca_Jnt'],
                                    parent='Spine5BpmSca_Jnt',
                                    rootJnt='CollarSubBpmRoot_Jnt',
                                    inputGeometry='Cloth_Geo',
                                    skinCluster='skinCluster246',
                                    ctrlShp='crossCircle',
                                    ctrlColor='red',
                                    elem = 'CollarBck',
                                    side = '')
    CollarBckObj.rig()

    ## Wrap Group
    objCtrlGrp = mc.ls('Collar*BpmRig*grp')
    objStillGrp = mc.ls('Collar*BpmStill*grp')
    for each in objCtrlGrp:
        mc.parent( each, GrpCtrl)
    for each in objStillGrp:
        mc.parent( each, GrpStill)

    ## Read Shape Control
    print "# Generate >> Read Control Shape"
    ctrlShapeTools.readAllCtrl()