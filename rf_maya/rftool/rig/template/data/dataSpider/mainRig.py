import maya.cmds as mc

import sys
sys.path.append(r'P:\lib\local')
sys.path.append (r'P:\lib\local\utaTools\utapy')

from lpRig import rigTools as lrr
from lpRig import core as lrc
from lpRig import rigData
from lpRig import mainGroup
from lpRig import rootRig
from lpRig import torsoRig
from lpRig import hindLegRig
from lpRig import neckRig
from lpRig import headRig
from lpRig import clavRig
from lpRig import legRig
from lpRig import fkRig
from lpRig import pointRig
from lpRig import ribbon
import pkmel.ctrlShapeTools as ctrlShapeTools
import utaCore as uc
from lpRig import tailRig
import addGrpRbn as agr

reload(rigData)
reload(mainGroup)
reload(rootRig)
reload(torsoRig)
reload(hindLegRig)
reload(neckRig)
reload(headRig)
reload(clavRig)
reload(legRig)
reload(fkRig)
reload(pointRig)
reload (ribbon)
reload(uc)
reload(tailRig)
reload(agr)
# from lpRig import armRig
# reload(armRig)
# from lpRig import thumbRig
# reload(thumbRig)
# from lpRig import fingerRig
# reload(fingerRig)
# from lpRig import aimRig
# reload(aimRig)
# from lpRig import nonRollRig
# reload(nonRollRig)

def main():

	print 'Creating Main Groups'
	mgObj = mainGroup.MainGroup('Spider')

	print 'Creating Root Rig'
	rootObj = rootRig.RootRig(
								ctrlGrp=mgObj.Ctrl_Grp,
								skinGrp=mgObj.Skin_Grp,
								tmpJnt='Root_Jnt',
								part=''
							)

	print 'Creating Torso Rig'
	torsoObj = torsoRig.TorsoRig2(
									parent=rootObj.Root_Jnt,
									ctrlGrp=mgObj.Ctrl_Grp,
									jntGrp=mgObj.Jnt_Grp,
									skinGrp=mgObj.Skin_Grp,
									stillGrp=mgObj.Still_Grp,
									tmpJnt=[
												'Pelvis_Jnt',
												'Spine_Jnt',
												'Chest_Jnt',
												'ChestTip_Jnt',
												'SpineMid_Jnt'
											],
									part=''
								)

	# print 'Creating Neck Rig'
	# neckObj = neckRig.NeckRig(
 #    						parent=torsoObj.ChestTip_Jnt,
	# 						ctrlGrp=mgObj.Ctrl_Grp,
	# 						skinGrp=mgObj.Skin_Grp,
	# 						jntGrp=mgObj.Jnt_Grp,
	# 						stillGrp=mgObj.Still_Grp,
	# 						ax='y',
	# 						tmpJnt=[
	# 								'Neck_Jnt',
	# 								'NeckTip_Jnt'
	# 								],
	# 						part='',
	# 						headJnt='Head_Jnt'
 #                            )

	print 'Creating Neck Rig'
	neckObj = neckRig.NeckRig(
                                parent=torsoObj.ChestTip_Jnt,
                                ctrlGrp=mgObj.Ctrl_Grp,
                                skinGrp=mgObj.Skin_Grp,
                                jntGrp=mgObj.Jnt_Grp,
                                stillGrp=mgObj.Still_Grp,
                                ax='y',
                                tmpJnt=[
                                        'Neck_Jnt',
                                        'NeckTip_Jnt'
                                        ],
                                part='',
                                # headJnt='Head_Jnt'
                            )

	print 'Creating Head Rig'
	headObj = headRig.HeadRig(
                                parent=neckObj.NeckTip_Jnt,
                                ctrlGrp=mgObj.Ctrl_Grp,
                                tmpJnt=[
                                            'Head_Jnt',
                                            'HeadTip_Jnt',
                                            'Eye_L_Jnt',
                                            'Eye_R_Jnt',
                                            '',
                                            '',
                                            '',
                                            'EyeTar_Jnt',
                                            'EyeTar_L_Jnt',
                                            'EyeTar_R_Jnt'
                                        ],
                                part=''
                            )

	# print 'Creating Hair Rig'
	# hairRigObj = pointRig.PointRig(
 #                                    parent=headObj.Head_Jnt, 
 #                                    ctrlGrp=mgObj.Ctrl_Grp, 
 #                                    tmpJnt='Hair_Jnt', 
 #                                    part='Hair', side='', shape='cube')

	# print 'Creating Eyedot Left Rig'
	# eyeDotRigObj_L = pointRig.PointRig(
 #                                    parent='', 
 #                                    ctrlGrp=mgObj.Ctrl_Grp, 
 #                                    tmpJnt='EyeDot_L_Jnt', 
 #                                    part='EyeDot', side='L', shape='circle')

	# print 'Creating Eyedot Right Rig'
	# eyeDotRigObj_R = pointRig.PointRig(
 #                                    parent='', 
 #                                    ctrlGrp=mgObj.Ctrl_Grp, 
 #                                    tmpJnt='EyeDot_R_Jnt', 
 #                                    part='EyeDot', side='R', shape='circle')

	# mc.delete('EyeDotPntCtrlOri_L_Grp_parentConstraint')
	# mc.delete('EyeDotPntCtrlOri_R_Grp_parentConstraint')

	# print 'Creating Left Clav Bck Rig'
	# lftClavBckObj = clavRig.ClavRig(
	# 								parent=torsoObj.Pelvis_Jnt,
	# 								ctrlGrp=mgObj.Ctrl_Grp,
	# 								ax='y',
	# 								tmpJnt=[
	# 											'ClavBck_L_Jnt',
	# 											'ClavTipBck_L_Jnt'
	# 										],
	# 								part='Bck',
	# 								side='L'
	# 							)

	# print 'Creating Right Clav Bck Rig'
	# rgtClavBckObj = clavRig.ClavRig(
	# 								parent=torsoObj.Pelvis_Jnt,
	# 								ctrlGrp=mgObj.Ctrl_Grp,
	# 								ax='y',
	# 								tmpJnt=[
	# 											'ClavBck_R_Jnt',
	# 											'ClavTipBck_R_Jnt'
	# 										],
	# 								part='Bck',
	# 								side='R'
	# 							)

	# print 'Creating Left Leg Rig'
	# lftLegObj = hindLegRig.HindLegRig(
	# 									parent='ClavTipBck_L_Jnt',
	# 									ctrlGrp=mgObj.Ctrl_Grp,
	# 									jntGrp=mgObj.Jnt_Grp,
	# 									ikhGrp=mgObj.Ikh_Grp,
	# 									skinGrp=mgObj.Skin_Grp,
	# 									stillGrp=mgObj.Still_Grp,
	# 									ribbon=True,
	# 									tmpJnt=[
	# 												'UpLegBck_L_Jnt',
	# 												'MidLegBck_L_Jnt',
	# 												'LowLegBck_L_Jnt',
	# 												'AnkleBck_L_Jnt',
	# 												'BallBck_L_Jnt',
	# 												'ToeBck_L_Jnt',
	# 												'HeelBck_L_Jnt',
	# 												'FootInBck_L_Jnt',
	# 												'FootOutBck_L_Jnt',
	# 												'KneeIkBck_L_Jnt',
	# 												'UpKneeIkBck_L_Jnt',
	# 												'LowKneeIkBck_L_Jnt'
	# 											],
	# 									part='Bck',
	# 									side='L'
	# 								)

	# print 'Creating Right Leg Rig'
	# rgtLegObj = hindLegRig.HindLegRig(
	# 									parent='ClavTipBck_R_Jnt',
	# 									ctrlGrp=mgObj.Ctrl_Grp,
	# 									jntGrp=mgObj.Jnt_Grp,
	# 									ikhGrp=mgObj.Ikh_Grp,
	# 									skinGrp=mgObj.Skin_Grp,
	# 									stillGrp=mgObj.Still_Grp,
	# 									ribbon=True,
	# 									tmpJnt=[
	# 												'UpLegBck_R_Jnt',
	# 												'MidLegBck_R_Jnt',
	# 												'LowLegBck_R_Jnt',
	# 												'AnkleBck_R_Jnt',
	# 												'BallBck_R_Jnt',
	# 												'ToeBck_R_Jnt',
	# 												'HeelBck_R_Jnt',
	# 												'FootInBck_R_Jnt',
	# 												'FootOutBck_R_Jnt',
	# 												'KneeIkBck_R_Jnt',
	# 												'UpKneeIkBck_R_Jnt',
	# 												'LowKneeIkBck_R_Jnt'
	# 											],
	# 									part='Bck',
	# 									side='R'
	# 								)

	# print 'Creating Left Clav Fnt Rig'
	# lftClavObj = clavRig.ClavRig(
	# 								parent=torsoObj.Chest_Jnt,
	# 								ctrlGrp=mgObj.Ctrl_Grp,
	# 								ax='y',
	# 								tmpJnt=[
	# 											'ClavFnt_L_Jnt',
	# 											'ClavTipFnt_L_Jnt'
	# 										],
	# 								part='Fnt',
	# 								side='L'
	# 							)

	# print 'Creating Right Clav Fnt Rig'
	# rgtClavObj = clavRig.ClavRig(
	# 								parent=torsoObj.Chest_Jnt,
	# 								ctrlGrp=mgObj.Ctrl_Grp,
	# 								ax='y',
	# 								tmpJnt=[
	# 											'ClavFnt_R_Jnt',
	# 											'ClavTipFnt_R_Jnt'
	# 										],
	# 								part='Fnt',
	# 								side='R'
	# 							)

	# print 'Creating Left Leg Fnt Rig'
	# lftFntLegObj = hindLegRig.HindLegRig(
	# 									parent='ClavTipFnt_L_Jnt',
	# 									ctrlGrp=mgObj.Ctrl_Grp,
	# 									jntGrp=mgObj.Jnt_Grp,
	# 									ikhGrp=mgObj.Ikh_Grp,
	# 									skinGrp=mgObj.Skin_Grp,
	# 									stillGrp=mgObj.Still_Grp,
	# 									ribbon=True,
	# 									tmpJnt=[
	# 												'UpLegFnt_L_Jnt',
	# 												'MidLegFnt_L_Jnt',
	# 												'LowLegFnt_L_Jnt',
	# 												'AnkleFnt_L_Jnt',
	# 												'BallFnt_L_Jnt',
	# 												'ToeFnt_L_Jnt',
	# 												'HeelFnt_L_Jnt',
	# 												'FootInFnt_L_Jnt',
	# 												'FootOutFnt_L_Jnt',
	# 												'KneeIkFnt_L_Jnt',
	# 												'UpKneeIkFnt_L_Jnt',
	# 												'LowKneeIkFnt_L_Jnt'
	# 											],
	# 									part='Fnt',
	# 									side='L'
	# 								)

	# print 'Creating Right Leg Fnt Rig'
	# rgtFntLegObj = hindLegRig.HindLegRig(
	# 									parent='ClavTipFnt_R_Jnt',
	# 									ctrlGrp=mgObj.Ctrl_Grp,
	# 									jntGrp=mgObj.Jnt_Grp,
	# 									ikhGrp=mgObj.Ikh_Grp,
	# 									skinGrp=mgObj.Skin_Grp,
	# 									stillGrp=mgObj.Still_Grp,
	# 									ribbon=True,
	# 									tmpJnt=[
	# 												'UpLegFnt_R_Jnt',
	# 												'MidLegFnt_R_Jnt',
	# 												'LowLegFnt_R_Jnt',
	# 												'AnkleFnt_R_Jnt',
	# 												'BallFnt_R_Jnt',
	# 												'ToeFnt_R_Jnt',
	# 												'HeelFnt_R_Jnt',
	# 												'FootInFnt_R_Jnt',
	# 												'FootOutFnt_R_Jnt',
	# 												'KneeIkFnt_R_Jnt',
	# 												'UpKneeIkFnt_R_Jnt',
	# 												'LowKneeIkFnt_R_Jnt'
	# 											],
	# 									part='Fnt',
	# 									side='R'
	# 								)
	print 'Creating Left ClavFnt Rig'
	lftClavFntObj = clavRig.ClavRig(
                                    parent=rootObj.Root_Jnt,
                                    ctrlGrp=mgObj.Ctrl_Grp,
                                    ax='y',
                                    tmpJnt=[
                                                'ClavFnt_L_Jnt',
                                                'ClavFntTip_L_Jnt'
                                            ],
                                    part='Fnt',
                                    side='L'
                                )

	print 'Creating right ClavFnt Rig'
	rgtClavFntObj = clavRig.ClavRig(
                                    parent=rootObj.Root_Jnt,
                                    ctrlGrp=mgObj.Ctrl_Grp,
                                    ax='y',
                                    tmpJnt=[
                                                'ClavFnt_R_Jnt',
                                                'ClavFntTip_R_Jnt'
                                            ],
                                    part='Fnt',
                                    side='R'
                                )
	print 'Creating Left ClavMidA Rig'
	lftClavMidAObj = clavRig.ClavRig(
                                    parent=rootObj.Root_Jnt,
                                    ctrlGrp=mgObj.Ctrl_Grp,
                                    ax='y',
                                    tmpJnt=[
                                                'ClavMidA_L_Jnt',
                                                'ClavMidATip_L_Jnt'
                                            ],
                                    part='MidA',
                                    side='L'
                                )

	print 'Creating right ClavMidA Rig'
	rgtClavMidBObj = clavRig.ClavRig(
                                    parent=rootObj.Root_Jnt,
                                    ctrlGrp=mgObj.Ctrl_Grp,
                                    ax='y',
                                    tmpJnt=[
                                                'ClavMidA_R_Jnt',
                                                'ClavMidATip_R_Jnt'
                                            ],
                                    part='MidA',
                                    side='R'
                                )

	print 'Creating Left ClavMidB Rig'
	lftClavMidBObj = clavRig.ClavRig(
                                    parent=rootObj.Root_Jnt,
                                    ctrlGrp=mgObj.Ctrl_Grp,
                                    ax='y',
                                    tmpJnt=[
                                                'ClavMidB_L_Jnt',
                                                'ClavMidBTip_L_Jnt'
                                            ],
                                    part='MidB',
                                    side='L'
                                )

	print 'Creating right ClavMidB Rig'
	rgtClavMidBObj = clavRig.ClavRig(
                                    parent=rootObj.Root_Jnt,
                                    ctrlGrp=mgObj.Ctrl_Grp,
                                    ax='y',
                                    tmpJnt=[
                                                'ClavMidB_R_Jnt',
                                                'ClavMidBTip_R_Jnt'
                                            ],
                                    part='MidB',
                                    side='R'
                                )

	print 'Creating Left ClavBck Rig'
	lftClavBckObj = clavRig.ClavRig(
                                    parent=rootObj.Root_Jnt,
                                    ctrlGrp=mgObj.Ctrl_Grp,
                                    ax='y',
                                    tmpJnt=[
                                                'ClavBck_L_Jnt',
                                                'ClavBckTip_L_Jnt'
                                            ],
                                    part='Bck',
                                    side='L'
                                )

	print 'Creating right ClavBck Rig'
	rgtClavBckObj = clavRig.ClavRig(
                                    parent=rootObj.Root_Jnt,
                                    ctrlGrp=mgObj.Ctrl_Grp,
                                    ax='y',
                                    tmpJnt=[
                                                'ClavBck_R_Jnt',
                                                'ClavBckTip_R_Jnt'
                                            ],
                                    part='Bck',
                                    side='R'
                                )
	# print 'Creating Left LegFnt Rig'
	# lftLegFntObj = legRig.LegRig(
 #                                parent='ClavFntTip_L_Jnt',
 #                                ctrlGrp=mgObj.Ctrl_Grp,
 #                                jntGrp=mgObj.Jnt_Grp,
 #                                ikhGrp=mgObj.Ikh_Grp,
 #                                skinGrp=mgObj.Skin_Grp,
 #                                stillGrp=mgObj.Still_Grp,
 #                                ribbon=True,
 #                                tmpJnt=[
 #                                            'UpLegFnt_L_Jnt',
 #                                            'LowLegFnt_L_Jnt',
 #                                            'AnkleFnt_L_Jnt',
 #                                            'BallFnt_L_Jnt',
 #                                            'ToeFnt_L_Jnt',
 #                                            'HeelPivFnt_L_Jnt',
 #                                            'FootInPivFnt_L_Jnt',
 #                                            'FootOutPivFnt_L_Jnt',
 #                                            'KneeIkFnt_L_Jnt'
 #                                        ],
 #                                part='Fnt',
 #                                side='L'
 #                            )

	print 'Create Lef LegFnt Rig by hindLegRig'
	lftLegFntObj = hindLegRig.HindLegRig(
							parent='ClavFntTip_L_Jnt',
							ctrlGrp=mgObj.Ctrl_Grp,
							jntGrp=mgObj.Jnt_Grp,
							ikhGrp=mgObj.Ikh_Grp,
							skinGrp=mgObj.Skin_Grp,
							stillGrp=mgObj.Still_Grp,
							ribbon=True,
							tmpJnt=[
								'UpLegFnt_L_Jnt',
								'MiddleLegFnt_L_Jnt',
								'LowLegFnt_L_Jnt',
								'AnkleFnt_L_Jnt',
								'BallFnt_L_Jnt',
								'ToeFnt_L_Jnt',
								'HeelPivFnt_L_Jnt',
								'FootInPivFnt_L_Jnt',
								'FootOutPivFnt_L_Jnt',
								'KneeIkFnt_L_Jnt',
								'UpKneeIkFnt_L_Jnt',
								'LowKneeIkFnt_L_Jnt'
								],
							part='Fnt',
							side='L'
							)

	print 'Create Right LegFnt Rig by hindLegRig'
	rgtLegFntObj = hindLegRig.HindLegRig(
							parent='ClavFntTip_R_Jnt',
							ctrlGrp=mgObj.Ctrl_Grp,
							jntGrp=mgObj.Jnt_Grp,
							ikhGrp=mgObj.Ikh_Grp,
							skinGrp=mgObj.Skin_Grp,
							stillGrp=mgObj.Still_Grp,
							ribbon=True,
							tmpJnt=[
								'UpLegFnt_R_Jnt',
								'MiddleLegFnt_R_Jnt',
								'LowLegFnt_R_Jnt',
								'AnkleFnt_R_Jnt',
								'BallFnt_R_Jnt',
								'ToeFnt_R_Jnt',
								'HeelPivFnt_R_Jnt',
								'FootInPivFnt_R_Jnt',
								'FootOutPivFnt_R_Jnt',
								'KneeIkFnt_R_Jnt',
								'UpKneeIkFnt_R_Jnt',
								'LowKneeIkFnt_R_Jnt'
								],
							part='Fnt',
							side='R'
							)

	print 'Create Lef LegMidA Rig by hindLegRig'
	lftLegMidAObj = hindLegRig.HindLegRig(
							parent='ClavMidATip_L_Jnt',
							ctrlGrp=mgObj.Ctrl_Grp,
							jntGrp=mgObj.Jnt_Grp,
							ikhGrp=mgObj.Ikh_Grp,
							skinGrp=mgObj.Skin_Grp,
							stillGrp=mgObj.Still_Grp,
							ribbon=True,
							tmpJnt=[
								'UpLegMidA_L_Jnt',
								'MiddleLegMidA_L_Jnt',
								'LowLegMidA_L_Jnt',
								'AnkleMidA_L_Jnt',
								'BallMidA_L_Jnt',
								'ToeMidA_L_Jnt',
								'HeelPivMidA_L_Jnt',
								'FootInPivMidA_L_Jnt',
								'FootOutPivMidA_L_Jnt',
								'KneeIkMidA_L_Jnt',
								'UpKneeIkMidA_L_Jnt',
								'LowKneeIkMidA_L_Jnt'
								],
							part='MidA',
							side='L'
							)

	print 'Create Right LegMidA Rig by hindLegRig'
	lftLegMidAObj = hindLegRig.HindLegRig(
							parent='ClavMidATip_R_Jnt',
							ctrlGrp=mgObj.Ctrl_Grp,
							jntGrp=mgObj.Jnt_Grp,
							ikhGrp=mgObj.Ikh_Grp,
							skinGrp=mgObj.Skin_Grp,
							stillGrp=mgObj.Still_Grp,
							ribbon=True,
							tmpJnt=[
								'UpLegMidA_R_Jnt',
								'MiddleLegMidA_R_Jnt',
								'LowLegMidA_R_Jnt',
								'AnkleMidA_R_Jnt',
								'BallMidA_R_Jnt',
								'ToeMidA_R_Jnt',
								'HeelPivMidA_R_Jnt',
								'FootInPivMidA_R_Jnt',
								'FootOutPivMidA_R_Jnt',
								'KneeIkMidA_R_Jnt',
								'UpKneeIkMidA_R_Jnt',
								'LowKneeIkMidA_R_Jnt'
								],
							part='MidA',
							side='R'
							)

	print 'Create Lef LegMidB Rig by hindLegRig'
	lftLegMidBObj = hindLegRig.HindLegRig(
							parent='ClavMidBTip_L_Jnt',
							ctrlGrp=mgObj.Ctrl_Grp,
							jntGrp=mgObj.Jnt_Grp,
							ikhGrp=mgObj.Ikh_Grp,
							skinGrp=mgObj.Skin_Grp,
							stillGrp=mgObj.Still_Grp,
							ribbon=True,
							tmpJnt=[
								'UpLegMidB_L_Jnt',
								'MiddleLegMidB_L_Jnt',
								'LowLegMidB_L_Jnt',
								'AnkleMidB_L_Jnt',
								'BallMidB_L_Jnt',
								'ToeMidB_L_Jnt',
								'HeelPivMidB_L_Jnt',
								'FootInPivMidB_L_Jnt',
								'FootOutPivMidB_L_Jnt',
								'KneeIkMidB_L_Jnt',
								'UpKneeIkMidB_L_Jnt',
								'LowKneeIkMidB_L_Jnt'
								],
							part='MidB',
							side='L'
							)

	print 'Create Right LegMidB Rig by hindLegRig'
	lftLegMidBObj = hindLegRig.HindLegRig(
							parent='ClavMidBTip_R_Jnt',
							ctrlGrp=mgObj.Ctrl_Grp,
							jntGrp=mgObj.Jnt_Grp,
							ikhGrp=mgObj.Ikh_Grp,
							skinGrp=mgObj.Skin_Grp,
							stillGrp=mgObj.Still_Grp,
							ribbon=True,
							tmpJnt=[
								'UpLegMidB_R_Jnt',
								'MiddleLegMidB_R_Jnt',
								'LowLegMidB_R_Jnt',
								'AnkleMidB_R_Jnt',
								'BallMidB_R_Jnt',
								'ToeMidB_R_Jnt',
								'HeelPivMidB_R_Jnt',
								'FootInPivMidB_R_Jnt',
								'FootOutPivMidB_R_Jnt',
								'KneeIkMidB_R_Jnt',
								'UpKneeIkMidB_R_Jnt',
								'LowKneeIkMidB_R_Jnt'
								],
							part='MidB',
							side='R'
							)

	print 'Create Lef LegBck Rig by hindLegRig'
	lftLegBckObj = hindLegRig.HindLegRig(
							parent='ClavBckTip_L_Jnt',
							ctrlGrp=mgObj.Ctrl_Grp,
							jntGrp=mgObj.Jnt_Grp,
							ikhGrp=mgObj.Ikh_Grp,
							skinGrp=mgObj.Skin_Grp,
							stillGrp=mgObj.Still_Grp,
							ribbon=True,
							tmpJnt=[
								'UpLegBck_L_Jnt',
								'MiddleLegBck_L_Jnt',
								'LowLegBck_L_Jnt',
								'AnkleBck_L_Jnt',
								'BallBck_L_Jnt',
								'ToeBck_L_Jnt',
								'HeelPivBck_L_Jnt',
								'FootInPivBck_L_Jnt',
								'FootOutPivBck_L_Jnt',
								'KneeIkBck_L_Jnt',
								'UpKneeIkBck_L_Jnt',
								'LowKneeIkBck_L_Jnt'
								],
							part='Bck',
							side='L'
							)

	print 'Create Right LegBck Rig by hindLegRig'
	lftLegBckObj = hindLegRig.HindLegRig(
							parent='ClavBckTip_R_Jnt',
							ctrlGrp=mgObj.Ctrl_Grp,
							jntGrp=mgObj.Jnt_Grp,
							ikhGrp=mgObj.Ikh_Grp,
							skinGrp=mgObj.Skin_Grp,
							stillGrp=mgObj.Still_Grp,
							ribbon=True,
							tmpJnt=[
								'UpLegBck_R_Jnt',
								'MiddleLegBck_R_Jnt',
								'LowLegBck_R_Jnt',
								'AnkleBck_R_Jnt',
								'BallBck_R_Jnt',
								'ToeBck_R_Jnt',
								'HeelPivBck_R_Jnt',
								'FootInPivBck_R_Jnt',
								'FootOutPivBck_R_Jnt',
								'KneeIkBck_R_Jnt',
								'UpKneeIkBck_R_Jnt',
								'LowKneeIkBck_R_Jnt'
								],
							part='Bck',
							side='R'
							)

	print 'Creating Left Horn Rig'
	lHornRigObj = fkRig.FkRig(
                                    parent=headObj.Head_Jnt, 
                                    ctrlGrp=mgObj.Ctrl_Grp, 
                                    tmpJnt=['Horn1_L_Jnt',
	                                	'Horn2_L_Jnt',
	                                	'Horn3_L_Jnt',
	                                	'Horn4_L_Jnt',
	                                	'Horn5_L_Jnt'], 
                                    part='Horn', side='L', ax='y', shape='circle')

	print 'Creating Right Horn Rig'
	rHornRigObj = fkRig.FkRig(
                                    parent=headObj.Head_Jnt, 
                                    ctrlGrp=mgObj.Ctrl_Grp, 
                                    tmpJnt=['Horn1_R_Jnt',
	                                	'Horn2_R_Jnt',
	                                	'Horn3_R_Jnt',
	                                	'Horn4_R_Jnt',
	                                	'Horn5_R_Jnt'], 
                                    part='Horn', side='R', ax='y', shape='circle')

	print 'Creating Left Teeth Rig'
	lTeethRigObj = fkRig.FkRig(
                                    parent=headObj.Head_Jnt, 
                                    ctrlGrp=mgObj.Ctrl_Grp, 
                                    tmpJnt=['Teeth1_L_Jnt',
	                                	'Teeth2_L_Jnt',
	                                	'Teeth3_L_Jnt',
	                                	'Teeth4_L_Jnt'], 
                                    part='Teeth', side='L', ax='y', shape='circle')

	print 'Creating Right Teeth Rig'
	rTeethRigObj = fkRig.FkRig(
                                    parent=headObj.Head_Jnt, 
                                    ctrlGrp=mgObj.Ctrl_Grp, 
                                    tmpJnt=['Teeth1_R_Jnt',
	                                	'Teeth2_R_Jnt',
	                                	'Teeth3_R_Jnt',
	                                	'Teeth4_R_Jnt'], 
                                    part='Teeth', side='R', ax='y', shape='circle')

	# print 'Creating Left Ear Rig'
	# lEarRigObj = fkRig.FkRig(
 #                                    parent=headObj.Head_Jnt, 
 #                                    ctrlGrp=mgObj.Ctrl_Grp, 
 #                                    tmpJnt=['Ear1_L_Jnt',
	#                                 	'Ear2_L_Jnt',
	#                                 	'Ear3_L_Jnt'], 
 #                                    part='Ear', side='L', ax='y', shape='circle')

	# print 'Creating Right Ear Rig'
	# rEarRigObj = fkRig.FkRig(
 #                                    parent=headObj.Head_Jnt, 
 #                                    ctrlGrp=mgObj.Ctrl_Grp, 
 #                                    tmpJnt=['Ear1_R_Jnt',
	#                                 	'Ear1_R_Jnt',
	#                                 	'Ear1_R_Jnt'], 
 #                                    part='Ear', side='R', ax='y', shape='circle')

	# print 'Creating Ring Rig'
	# ringRigObj = fkRig.FkRig(
 #                                    parent=headObj.Head_Jnt, 
 #                                    ctrlGrp=mgObj.Ctrl_Grp, 
 #                                    tmpJnt=['Ring1_Jnt',
	#                                 	'Ring2_Jnt',
	#                                 	'Ring3_Jnt'], 
 #                                    part='Ring', side='', ax='y', shape='circle')


	# print 'Create Ribbon Tail Rig'
	# tailRigObj = ribbon.RibbonSurface(part='Tail',
	# 									size = 78.419,
	# 									vPatch = 24,
	# 									jntNo = 24,
	# 									side = '')
	# # lists
	# tailList = ['Tail1_Jnt','Tail2_Jnt','Tail3_Jnt','Tail4_Jnt','Tail5_Jnt','Tail6_Jnt','Tail7_Jnt','Tail8_Jnt','Tail9_Jnt']
	# tailIkCtrlList = ['Tail1IkCtrl_Jnt','Tail2IkCtrl_Jnt','Tail3IkCtrl_Jnt','Tail4IkCtrl_Jnt','Tail5IkCtrl_Jnt']
	# tailIkCrvList = ['TailIk_Crv','TailIkUp_Crv']
	# tailIkCtrlCrv = (tailIkCtrlList + tailIkCrvList)
	# rbnTail_NrbList = ['RbnTail_Nrb']
	# # snape
	# mc.select(tailList[0])
	# mc.select(rbnTail_NrbList,add=True)
	# uc.snap()
	# # bindSkin
	# bindJntNrb = (tailList + rbnTail_NrbList)
	# mc.skinCluster(bindJntNrb, tsb=True)
	# mc.skinCluster(tailIkCtrlCrv,tsb=True)

	# print 'Tail Rig'
	# tailObj = tailRig.TailRig(parent=torsoObj.Pelvis_Jnt, 
	# 							ctrlGrp=mgObj.Ctrl_Grp, 
	# 							jntGrp=mgObj.Jnt_Grp, 
	# 							ikhGrp=mgObj.Ikh_Grp, 
	# 							stillGrp=mgObj.Still_Grp, 
	# 							tmpJnt=['Tail1_Jnt',
	# 									'Tail2_Jnt',
	# 									'Tail3_Jnt',
	# 									'Tail4_Jnt',
	# 									'Tail5_Jnt',
	# 									'Tail6_Jnt',
	# 									'Tail7_Jnt',
	# 									'Tail8_Jnt',
	# 									'Tail9_Jnt'], 
	# 							ax='y', 
	# 							aimVec=(0, 1, 0), 
	# 							upVec=(0, 0, 1), 
	# 							ikUpJnts=['Tail1IkUp_Jnt',
	# 									'Tail2IkUp_Jnt',
	# 									'Tail3IkUp_Jnt',
	# 									'Tail4IkUp_Jnt',
	# 									'Tail5IkUp_Jnt',
	# 									'Tail6IkUp_Jnt',
	# 									'Tail7IkUp_Jnt',
	# 									'Tail8IkUp_Jnt',
	# 									'Tail9IkUp_Jnt'], 
	# 							ikCtrlJnts=['Tail1IkCtrl_Jnt',
	# 									'Tail2IkCtrl_Jnt',
	# 									'Tail3IkCtrl_Jnt',
	# 									'Tail4IkCtrl_Jnt',
	# 									'Tail5IkCtrl_Jnt'], 
	# 							ikCrv='TailIk_Crv', 
	# 							ikUpCrv='TailIkUp_Crv', 
	# 							part='Tail', 
	# 							side='')

	print 'Creating Tail Tip Rig'
	tailTipRigObj = fkRig.FkRig(
                                    parent=torsoObj.Pelvis_Jnt, 
                                    ctrlGrp=mgObj.Ctrl_Grp, 
                                    tmpJnt=['Tail1Tip_Jnt',
	                                	'Tail2Tip_Jnt',
	                                	'Tail3Tip_Jnt',
	                                	'Tail4Tip_Jnt',
	                                	'Tail5Tip_Jnt'], 
                                    part='TailTip', side='', ax='y', shape='circle')

	# mc.parent('RbnSkinTail_Grp', mgObj.Skin_Grp)
	# mc.parent('RbnStillTail_Grp', mgObj.Still_Grp)
	
	# print 'Create Ribbon Group'
	# agr.addGrpToRbnSpider(elem = 'UpLegFnt', numberDtl = 5, side = 'L')
	# agr.addGrpToRbnSpider(elem = 'LowLegFnt', numberDtl = 5, side = 'L')
	# agr.addGrpToRbnSpider(elem = 'UpLegMidA', numberDtl = 5, side = 'L')
	# agr.addGrpToRbnSpider(elem = 'UpLegMidB', numberDtl = 5, side = 'L')
	# agr.addGrpToRbnSpider(elem = 'LowLegBck', numberDtl = 5, side = 'L')

	# agr.addGrpToRbnSpider(elem = 'UpLegFnt', numberDtl = 5, side = 'R')
	# agr.addGrpToRbnSpider(elem = 'LowLegFnt', numberDtl = 5, side = 'R')
	# agr.addGrpToRbnSpider(elem = 'UpLegMidA', numberDtl = 5, side = 'R')
	# agr.addGrpToRbnSpider(elem = 'UpLegMidB', numberDtl = 5, side = 'R')
	# agr.addGrpToRbnSpider(elem = 'LowLegBck', numberDtl = 5, side = 'R')


	# print 'cleanup'
	# rigData.readCtrlShape()
	ctrlShapeTools.readAllCtrl()
	# mc.delete('TmpJnt_Grp')
	# mc.delete('PivJnt_Grp')