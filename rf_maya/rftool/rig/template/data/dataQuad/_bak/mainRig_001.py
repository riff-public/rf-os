import maya.cmds as mc

import sys
sys.path.append(r'D:\tools')

from lpRig import rigTools as lrr
from lpRig import core as lrc

from lpRig import rigData
reload(rigData)

from lpRig import mainGroup
reload(mainGroup)
from lpRig import rootRig
reload(rootRig)
from lpRig import torsoRig
reload(torsoRig)
from lpRig import hindLegRig
reload(hindLegRig)

from lpRig import neckRig
reload(neckRig)
from lpRig import headRig
reload(headRig)
from lpRig import clavRig
reload(clavRig)
# from lpRig import armRig
# reload(armRig)
# from lpRig import thumbRig
# reload(thumbRig)
# from lpRig import fingerRig
# reload(fingerRig)
# from lpRig import legRig
# reload(legRig)
# from lpRig import aimRig
# reload(aimRig)
from lpRig import fkRig
reload(fkRig)
from lpRig import pointRig
reload(pointRig)
# from lpRig import nonRollRig
# reload(nonRollRig)
from lpRig import ribbon
reload (ribbon)

def main():

	print 'Creating Main Groups'
	mgObj = mainGroup.MainGroup('bullDemonBaby')

	print 'Creating Root Rig'
	rootObj = rootRig.RootRig(
								ctrlGrp=mgObj.Ctrl_Grp,
								skinGrp=mgObj.Skin_Grp,
								tmpJnt='Root_Jnt',
								part=''
							)

	print 'Creating Torso Rig'
	torsoObj = torsoRig.TorsoRig2(
									parent=rootObj.Root_Jnt,
									ctrlGrp=mgObj.Ctrl_Grp,
									jntGrp=mgObj.Jnt_Grp,
									skinGrp=mgObj.Skin_Grp,
									stillGrp=mgObj.Still_Grp,
									tmpJnt=[
												'Pelvis_Jnt',
												'Spine_Jnt',
												'Chest_Jnt',
												'ChestTip_Jnt',
												'SpineMid_Jnt'
											],
									part=''
								)

	print 'Creating Neck Rig'
	neckObj = neckRig.NeckRig(
    						parent=torsoObj.ChestTip_Jnt,
							ctrlGrp=mgObj.Ctrl_Grp,
							skinGrp=mgObj.Skin_Grp,
							jntGrp=mgObj.Jnt_Grp,
							stillGrp=mgObj.Still_Grp,
							ax='y',
							tmpJnt=[
									'Neck_Jnt',
									'NeckTip_Jnt'
									],
							part='',
							headJnt='Head_Jnt'
                            )

	print 'Creating Head Rig'
	headObj = headRig.HeadRig(
                                parent=neckObj.NeckTip_Jnt,
                                ctrlGrp=mgObj.Ctrl_Grp,
                                tmpJnt=[
                                            'Head_Jnt',
                                            'HeadTip_Jnt',
                                            'Eye_L_Jnt',
                                            'Eye_R_Jnt',
                                            '',
                                            '',
                                            '',
                                            'EyeTar_Jnt',
                                            'EyeTar_L_Jnt',
                                            'EyeTar_R_Jnt'
                                        ],
                                part=''
                            )

	print 'Creating Hair Rig'
	hairRigObj = pointRig.PointRig(
                                    parent=headObj.Head_Jnt, 
                                    ctrlGrp=mgObj.Ctrl_Grp, 
                                    tmpJnt='Hair_Jnt', 
                                    part='Hair', side='', shape='cube')

	print 'Creating Eyedot Left Rig'
	eyeDotRigObj_L = pointRig.PointRig(
                                    parent='', 
                                    ctrlGrp=mgObj.Ctrl_Grp, 
                                    tmpJnt='EyeDot_L_Jnt', 
                                    part='EyeDot', side='L', shape='circle')

	print 'Creating Eyedot Right Rig'
	eyeDotRigObj_R = pointRig.PointRig(
                                    parent='', 
                                    ctrlGrp=mgObj.Ctrl_Grp, 
                                    tmpJnt='EyeDot_R_Jnt', 
                                    part='EyeDot', side='R', shape='circle')

	mc.delete('EyeDotPntCtrlOri_L_Grp_parentConstraint')
	mc.delete('EyeDotPntCtrlOri_R_Grp_parentConstraint')

	print 'Creating Left Leg Rig'
	lftLegObj = hindLegRig.HindLegRig(
										parent=torsoObj.Pelvis_Jnt,
										ctrlGrp=mgObj.Ctrl_Grp,
										jntGrp=mgObj.Jnt_Grp,
										ikhGrp=mgObj.Ikh_Grp,
										skinGrp=mgObj.Skin_Grp,
										stillGrp=mgObj.Still_Grp,
										ribbon=True,
										tmpJnt=[
													'UpLeg_L_Jnt',
													'MidLeg_L_Jnt',
													'LowLeg_L_Jnt',
													'Ankle_L_Jnt',
													'Ball_L_Jnt',
													'Toe_L_Jnt',
													'Heel_L_Jnt',
													'FootIn_L_Jnt',
													'FootOut_L_Jnt',
													'KneeIk_L_Jnt',
													'UpKneeIk_L_Jnt',
													'LowKneeIk_L_Jnt'
												],
										part='',
										side='L'
									)

	print 'Creating Right Leg Rig'
	rgtLegObj = hindLegRig.HindLegRig(
										parent=torsoObj.Pelvis_Jnt,
										ctrlGrp=mgObj.Ctrl_Grp,
										jntGrp=mgObj.Jnt_Grp,
										ikhGrp=mgObj.Ikh_Grp,
										skinGrp=mgObj.Skin_Grp,
										stillGrp=mgObj.Still_Grp,
										ribbon=True,
										tmpJnt=[
													'UpLeg_R_Jnt',
													'MidLeg_R_Jnt',
													'LowLeg_R_Jnt',
													'Ankle_R_Jnt',
													'Ball_R_Jnt',
													'Toe_R_Jnt',
													'Heel_R_Jnt',
													'FootIn_R_Jnt',
													'FootOut_R_Jnt',
													'KneeIk_R_Jnt',
													'UpKneeIk_R_Jnt',
													'LowKneeIk_R_Jnt'
												],
										part='',
										side='R'
									)

	print 'Creating Left Clav Rig'
	lftClavObj = clavRig.ClavRig(
									parent=torsoObj.Chest_Jnt,
									ctrlGrp=mgObj.Ctrl_Grp,
									ax='y',
									tmpJnt=[
												'ClavFnt_L_Jnt',
												'ClavTipFnt_L_Jnt'
											],
									part='Fnt',
									side='L'
								)

	print 'Creating Right Clav Rig'
	rgtClavObj = clavRig.ClavRig(
									parent=torsoObj.Chest_Jnt,
									ctrlGrp=mgObj.Ctrl_Grp,
									ax='y',
									tmpJnt=[
												'ClavFnt_R_Jnt',
												'ClavTipFnt_R_Jnt'
											],
									part='Fnt',
									side='R'
								)

	print 'Creating Left LegFnt Rig'
	lftFntLegObj = hindLegRig.HindLegRig(
										parent='Pelvis_Jnt',
										ctrlGrp=mgObj.Ctrl_Grp,
										jntGrp=mgObj.Jnt_Grp,
										ikhGrp=mgObj.Ikh_Grp,
										skinGrp=mgObj.Skin_Grp,
										stillGrp=mgObj.Still_Grp,
										ribbon=True,
										tmpJnt=[
													'UpLegFnt_L_Jnt',
													'MidLegFnt_L_Jnt',
													'LowLegFnt_L_Jnt',
													'AnkleFnt_L_Jnt',
													'BallFnt_L_Jnt',
													'ToeFnt_L_Jnt',
													'HeelFnt_L_Jnt',
													'FootInFnt_L_Jnt',
													'FootOutFnt_L_Jnt',
													'KneeIkFnt_L_Jnt',
													'UpKneeIkFnt_L_Jnt',
													'LowKneeIkFnt_L_Jnt'
												],
										part='Fnt',
										side='L'
									)

	print 'Creating Right LegFnt Rig'
	rgtFntLegObj = hindLegRig.HindLegRig(
										parent='ClavTipFnt_R_Jnt',
										ctrlGrp=mgObj.Ctrl_Grp,
										jntGrp=mgObj.Jnt_Grp,
										ikhGrp=mgObj.Ikh_Grp,
										skinGrp=mgObj.Skin_Grp,
										stillGrp=mgObj.Still_Grp,
										ribbon=True,
										tmpJnt=[
													'UpLegFnt_R_Jnt',
													'MidLegFnt_R_Jnt',
													'LowLegFnt_R_Jnt',
													'AnkleFnt_R_Jnt',
													'BallFnt_R_Jnt',
													'ToeFnt_R_Jnt',
													'HeelFnt_R_Jnt',
													'FootInFnt_R_Jnt',
													'FootOutFnt_R_Jnt',
													'KneeIkFnt_R_Jnt',
													'UpKneeIkFnt_R_Jnt',
													'LowKneeIkFnt_R_Jnt'
												],
										part='Fnt',
										side='R'
									)

	print 'Creating Left Tail Rig'
	lShldPadRigObj = fkRig.FkRig(
                                    parent=torsoObj.Pelvis_Jnt, 
                                    ctrlGrp=mgObj.Ctrl_Grp, 
                                    tmpJnt=['Tail1_Jnt',
                                    	 'Tail2_Jnt',
                                    	 'Tail3_Jnt',
                                    	 'Tail4_Jnt',
                                    	 'Tail5_Jnt',
                                    	 'Tail6_Jnt',
                                    	 'Tail7_Jnt',
                                    	 'Tail8_Jnt',
                                    	 'Tail9_Jnt'], 
                                    part='Tail', side='L', ax='y', shape='circle')


	print 'cleanup'
	# rigData.readCtrlShape()
	# mc.delete('TmpJnt_Grp')
	# mc.delete('PivJnt_Grp')