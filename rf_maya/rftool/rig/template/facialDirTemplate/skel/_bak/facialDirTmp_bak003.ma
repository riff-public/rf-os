//Maya ASCII 2018ff09 scene
//Name: facialDirTmp.ma
//Last modified: Thu, Oct 15, 2020 11:36:53 AM
//Codeset: 1252
requires maya "2018ff09";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2018";
fileInfo "version" "2018";
fileInfo "cutIdentifier" "201903222215-65bada0e52";
fileInfo "osv" "Microsoft Windows 8 Business Edition, 64-bit  (Build 9200)\n";
createNode transform -s -n "persp";
	rename -uid "C2B371E3-48B4-DF96-99DE-78A665973388";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 37.019552000408545 8.1426116341508283 22.443322156228326 ;
	setAttr ".r" -type "double3" 3.8616472703739992 60.200000000005552 3.9999010648463652e-16 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "BF610E98-40DE-80E9-BDF9-3C9E9C1E93A4";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 42.520301602097241;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" -3.6894454880087579e-08 10.967821237714304 0.84251580662632519 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "CEA6C266-4807-6F97-9299-779803290C13";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -0.065952573377876855 1000.1545898470238 1.1214865257794961 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "509AA318-48F2-5C99-5314-97A48BBF5BA9";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 989.14435933593643;
	setAttr ".ow" 11.045977480846354;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".tp" -type "double3" -0.065952573377876855 11.010230511087389 1.1214865257792765 ;
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "B6A9730C-40C4-3636-BE21-B6BFC139119A";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0.12399099381956757 21.958076272606622 1000.373643822992 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "91DCD299-4783-2632-DACC-89A73EB5E925";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 986.41973150357217;
	setAttr ".ow" 11.543119777369522;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".tp" -type "double3" 0.0030707051065168578 23.569872591569982 13.953912319419782 ;
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "F68D37D0-46E8-79CD-007A-EEA057299B09";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.3819873949842 22.44995144127569 12.85086479018976 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "C2AEDA16-4D71-38CB-9706-F881015CC2F4";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.3819873949842;
	setAttr ".ow" 16.894877211201429;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".tp" -type "double3" 0 21.634649055748543 11.054179903564901 ;
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "TmpJnt_Grp";
	rename -uid "AB0F2A42-4FE2-7B66-0261-599B57BE4D2D";
	setAttr ".t" -type "double3" -2.8632053254437986e-12 11.262175100029843 -0.16446904391360606 ;
createNode transform -n "EyeLidSurface_R_Nrb" -p "TmpJnt_Grp";
	rename -uid "1F9DDFD8-43A7-F074-D4F1-028BDB01A2AA";
	setAttr ".t" -type "double3" -0.52599999999713687 0.94948719245062563 0.74093274325442637 ;
	setAttr ".s" -type "double3" 0.5 0.5 0.5 ;
createNode nurbsSurface -n "EyeLidSurface_R_NrbShape" -p "EyeLidSurface_R_Nrb";
	rename -uid "76C52EDA-41E7-96A7-16A4-7BB4F04F232E";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".tw" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".nufa" 4.5;
	setAttr ".nvfa" 4.5;
createNode nurbsSurface -n "EyeLidSurface_R_NrbShapeOrig" -p "EyeLidSurface_R_Nrb";
	rename -uid "2517D269-46E8-D97C-889C-9EA4B8A45671";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		3 3 0 2 no 
		9 0 0 0 1 2 3 4 4 4
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		
		77
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		0.19991679083637276 -1 -0.19991679083637284
		0.28272503694690354 -1 -1.1177774761168466e-16
		0.19991679083637276 -1 0.19991679083637268
		4.7890007918352597e-17 -1 0.2827250369469036
		-0.19991679083637273 -1 0.19991679083637273
		-0.28272503694690371 -1 3.2191243563517383e-19
		-0.19991679083637276 -1 -0.19991679083637279
		-7.1788605542032352e-17 -1 -0.28272503694690371
		0.19991679083637276 -1 -0.19991679083637284
		0.28272503694690354 -1 -1.1177774761168466e-16
		0.19991679083637276 -1 0.19991679083637268
		0.61642997969058977 -0.78361162489122427 -0.61642997969058977
		0.87176363753180319 -0.78361162489122427 -4.7776033311964345e-17
		0.61642997969058966 -0.78361162489122427 0.61642997969058977
		-8.3940866021681387e-18 -0.78361162489122427 0.87176363753180353
		-0.61642997969058977 -0.78361162489122427 0.61642997969058977
		-0.87176363753180375 -0.78361162489122427 -1.4243681125611212e-17
		-0.61642997969058966 -0.78361162489122427 -0.61642997969058977
		-6.529563193606176e-17 -0.78361162489122427 -0.87176363753180353
		0.61642997969058977 -0.78361162489122427 -0.61642997969058977
		0.87176363753180319 -0.78361162489122427 -4.7776033311964345e-17
		0.61642997969058966 -0.78361162489122427 0.61642997969058977
		0.86720244749154218 1.7637621209264518e-16 -0.86720244749154185
		1.2264094625656801 1.2327537701598182e-16 7.5095921138754302e-17
		0.86720244749154174 7.0174541939318428e-17 0.86720244749154207
		-8.6614559170421727e-17 4.8179455877227502e-17 1.2264094625656805
		-0.86720244749154207 7.0174541939318453e-17 0.86720244749154185
		-1.2264094625656807 1.2327537701598182e-16 -2.7341567632466632e-17
		-0.86720244749154174 1.7637621209264521e-16 -0.86720244749154207
		-1.7053183114584738e-17 1.9837129815473613e-16 -1.2264094625656805
		0.86720244749154218 1.7637621209264518e-16 -0.86720244749154185
		1.2264094625656801 1.2327537701598182e-16 7.5095921138754302e-17
		0.86720244749154174 7.0174541939318428e-17 0.86720244749154207
		0.61642997969058988 0.78361162489122471 -0.61642997969058966
		0.87176363753180319 0.78361162489122471 1.5453628814360203e-16
		0.61642997969058955 0.78361162489122471 0.61642997969058988
		-1.147416612379651e-16 0.78361162489122471 0.87176363753180353
		-0.61642997969058988 0.78361162489122471 0.61642997969058966
		-0.87176363753180375 0.78361162489122471 -2.4626508941638749e-17
		-0.61642997969058955 0.78361162489122471 -0.61642997969058988
		4.105194269973521e-17 0.78361162489122471 -0.87176363753180353
		0.61642997969058988 0.78361162489122471 -0.61642997969058966
		0.87176363753180319 0.78361162489122471 1.5453628814360203e-16
		0.61642997969058955 0.78361162489122471 0.61642997969058988
		0.19991679083637259 1 -0.19991679083637234
		0.28272503694690315 1 1.4640157876526888e-16
		0.19991679083637237 1 0.19991679083637262
		-8.7824638543865684e-17 1 0.28272503694690332
		-0.19991679083637257 1 0.19991679083637245
		-0.28272503694690332 1 -1.2928054111847823e-17
		-0.19991679083637237 1 -0.19991679083637251
		6.3926040920185966e-17 1 -0.28272503694690321
		0.19991679083637259 1 -0.19991679083637234
		0.28272503694690315 1 1.4640157876526888e-16
		0.19991679083637237 1 0.19991679083637262
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		
		;
createNode transform -n "EyeLidSurface_L_Nrb" -p "TmpJnt_Grp";
	rename -uid "5193088E-429D-2748-6128-BE9278BF1B23";
	setAttr ".t" -type "double3" 0.526 0.94948719245062563 0.74093274325442637 ;
	setAttr ".s" -type "double3" 0.5 0.5 0.5 ;
createNode nurbsSurface -n "EyeLidSurface_L_NrbShape" -p "EyeLidSurface_L_Nrb";
	rename -uid "102EF48E-431B-F340-2F7B-7F8CD3B31BCF";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".tw" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".nufa" 4.5;
	setAttr ".nvfa" 4.5;
createNode nurbsSurface -n "EyeLidSurface_L_NrbShapeOrig" -p "EyeLidSurface_L_Nrb";
	rename -uid "1C72E7EF-4D4D-B35D-A98E-E4955692258B";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		3 3 0 2 no 
		9 0 0 0 1 2 3 4 4 4
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		
		77
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		0.19991679083637276 -1 -0.19991679083637284
		0.28272503694690354 -1 -1.1177774761168466e-16
		0.19991679083637276 -1 0.19991679083637268
		4.7890007918352597e-17 -1 0.2827250369469036
		-0.19991679083637273 -1 0.19991679083637273
		-0.28272503694690371 -1 3.2191243563517383e-19
		-0.19991679083637276 -1 -0.19991679083637279
		-7.1788605542032352e-17 -1 -0.28272503694690371
		0.19991679083637276 -1 -0.19991679083637284
		0.28272503694690354 -1 -1.1177774761168466e-16
		0.19991679083637276 -1 0.19991679083637268
		0.61642997969058977 -0.78361162489122427 -0.61642997969058977
		0.87176363753180319 -0.78361162489122427 -4.7776033311964345e-17
		0.61642997969058966 -0.78361162489122427 0.61642997969058977
		-8.3940866021681387e-18 -0.78361162489122427 0.87176363753180353
		-0.61642997969058977 -0.78361162489122427 0.61642997969058977
		-0.87176363753180375 -0.78361162489122427 -1.4243681125611212e-17
		-0.61642997969058966 -0.78361162489122427 -0.61642997969058977
		-6.529563193606176e-17 -0.78361162489122427 -0.87176363753180353
		0.61642997969058977 -0.78361162489122427 -0.61642997969058977
		0.87176363753180319 -0.78361162489122427 -4.7776033311964345e-17
		0.61642997969058966 -0.78361162489122427 0.61642997969058977
		0.86720244749154218 1.7637621209264518e-16 -0.86720244749154185
		1.2264094625656801 1.2327537701598182e-16 7.5095921138754302e-17
		0.86720244749154174 7.0174541939318428e-17 0.86720244749154207
		-8.6614559170421727e-17 4.8179455877227502e-17 1.2264094625656805
		-0.86720244749154207 7.0174541939318453e-17 0.86720244749154185
		-1.2264094625656807 1.2327537701598182e-16 -2.7341567632466632e-17
		-0.86720244749154174 1.7637621209264521e-16 -0.86720244749154207
		-1.7053183114584738e-17 1.9837129815473613e-16 -1.2264094625656805
		0.86720244749154218 1.7637621209264518e-16 -0.86720244749154185
		1.2264094625656801 1.2327537701598182e-16 7.5095921138754302e-17
		0.86720244749154174 7.0174541939318428e-17 0.86720244749154207
		0.61642997969058988 0.78361162489122471 -0.61642997969058966
		0.87176363753180319 0.78361162489122471 1.5453628814360203e-16
		0.61642997969058955 0.78361162489122471 0.61642997969058988
		-1.147416612379651e-16 0.78361162489122471 0.87176363753180353
		-0.61642997969058988 0.78361162489122471 0.61642997969058966
		-0.87176363753180375 0.78361162489122471 -2.4626508941638749e-17
		-0.61642997969058955 0.78361162489122471 -0.61642997969058988
		4.105194269973521e-17 0.78361162489122471 -0.87176363753180353
		0.61642997969058988 0.78361162489122471 -0.61642997969058966
		0.87176363753180319 0.78361162489122471 1.5453628814360203e-16
		0.61642997969058955 0.78361162489122471 0.61642997969058988
		0.19991679083637259 1 -0.19991679083637234
		0.28272503694690315 1 1.4640157876526888e-16
		0.19991679083637237 1 0.19991679083637262
		-8.7824638543865684e-17 1 0.28272503694690332
		-0.19991679083637257 1 0.19991679083637245
		-0.28272503694690332 1 -1.2928054111847823e-17
		-0.19991679083637237 1 -0.19991679083637251
		6.3926040920185966e-17 1 -0.28272503694690321
		0.19991679083637259 1 -0.19991679083637234
		0.28272503694690315 1 1.4640157876526888e-16
		0.19991679083637237 1 0.19991679083637262
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		
		;
createNode joint -n "EyeTrgt_TmpJnt" -p "TmpJnt_Grp";
	rename -uid "77FADF72-4427-8F09-0E89-3FB6DC6DFDDE";
	setAttr ".t" -type "double3" 8.2229238859917653e-06 0.94948719245062563 5.5420865335902505 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeTrgt_L_TmpJnt" -p "EyeTrgt_TmpJnt";
	rename -uid "3D7DFAB7-4DED-00F1-CBB3-A3A63FFD00CF";
	setAttr ".t" -type "double3" 0.52632949008213581 0 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeTrgt_R_TmpJnt" -p "EyeTrgt_TmpJnt";
	rename -uid "DF720B02-448E-1CFF-A25F-B2A75C99E5A3";
	setAttr ".t" -type "double3" -0.52600822292388594 0 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.05;
createNode joint -n "Eye_R_TmpJnt" -p "TmpJnt_Grp";
	rename -uid "9A1313B0-4B6B-795B-FDFF-ABA96A7AD28F";
	setAttr ".t" -type "double3" -0.526 0.94948719245062563 0.74093274325442637 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.05;
createNode joint -n "Iris_R_TmpJnt" -p "Eye_R_TmpJnt";
	rename -uid "5CB5EC67-4421-479F-6877-1FBD69F929C9";
	setAttr ".t" -type "double3" -0.059083999997136871 -0.0077028274536132813 0.32149371504783641 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.05;
createNode joint -n "Pupil_R_TmpJnt" -p "Iris_R_TmpJnt";
	rename -uid "CF320495-4485-D86A-CB8D-7FB383E869ED";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeDot_R_TmpJnt" -p "Eye_R_TmpJnt";
	rename -uid "9DD4F637-438F-7E39-EFB3-7698EA6CDF5B";
	setAttr ".t" -type "double3" 0.02745149231243349 0.01448822021484375 0.39170169830322266 ;
	setAttr ".r" -type "double3" 0 1.7356856264772427 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeInner_R_TmpJnt" -p "Eye_R_TmpJnt";
	rename -uid "FABFC470-4D36-DEF9-E99D-6E9345D2EA75";
	setAttr ".t" -type "double3" 0.191544 -0.11960000000000015 0.35604 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeOuter_R_TmpJnt" -p "Eye_R_TmpJnt";
	rename -uid "031FF3FE-45DD-A7F1-44BA-26B6B459BD16";
	setAttr ".t" -type "double3" -0.398956 0.025399999999999423 0.05114300000000005 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeLidUpr_R_TmpJnt" -p "Eye_R_TmpJnt";
	rename -uid "484DCD27-4FB1-D44B-A895-62BD35B61466";
	setAttr ".t" -type "double3" -0.16833799999713683 0.084037707519531324 0.36300030065917965 ;
	setAttr ".r" -type "double3" -11.86 24.835 -2.190416739716853e-14 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -180 0 0 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeLidUpr1_R_TmpJnt" -p "EyeLidUpr_R_TmpJnt";
	rename -uid "B4963BCA-4540-7B4A-679C-A4972BCFAD62";
	setAttr ".t" -type "double3" 0.28111532844256809 0.064537714143723335 0.11015901414196749 ;
	setAttr ".r" -type "double3" 0.2427213565934192 -17.789392846891921 -1.0483680136060398e-11 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 13.029194807943668 -24.270376341976863 -5.4334791855791158 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeLidUpr2_R_TmpJnt" -p "EyeLidUpr_R_TmpJnt";
	rename -uid "E76DB3C5-4F71-4E02-C6D4-5D975201DE9B";
	setAttr ".t" -type "double3" 0.17803278808232759 0.002018204274289559 0.036613672912455542 ;
	setAttr ".r" -type "double3" -10.407952539187567 -1.1924569456725582 -1.6227449233677762e-12 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 13.029194807943668 -24.270376341976863 -5.4334791855791158 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeLidUpr3_R_TmpJnt" -p "EyeLidUpr_R_TmpJnt";
	rename -uid "541A1DB4-4C85-D258-0723-C285BA6A96DA";
	setAttr ".t" -type "double3" 6.9293403902492834e-05 0.00014086998213436175 -0.00018422831570807929 ;
	setAttr ".r" -type "double3" -11.843365516735384 24.825243253043283 1.0162719139351127e-13 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 13.029194807943668 -24.270376341976863 -5.4334791855791158 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeLidUpr4_R_TmpJnt" -p "EyeLidUpr_R_TmpJnt";
	rename -uid "C8B7C482-4C56-08DD-3B9C-6D9445E9A296";
	setAttr ".t" -type "double3" -0.19478918000481243 0.0088189512997747244 0.052886610555272551 ;
	setAttr ".r" -type "double3" -9.1568316366288176 53.929572082132374 -1.6881086356479598e-13 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 13.029194807943668 -24.270376341976863 -5.4334791855791158 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeLidUpr5_R_TmpJnt" -p "EyeLidUpr_R_TmpJnt";
	rename -uid "EA936F7A-419A-E01B-4D19-FA92E0433745";
	setAttr ".t" -type "double3" -0.30632218084652063 0.013714816221019888 0.14440352565798165 ;
	setAttr ".r" -type "double3" -5.8098735351237023 74.349670035987714 4.1265209117907955e-14 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 13.029194807943668 -24.270376341976863 -5.4334791855791158 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeLidUpr6_R_TmpJnt" -p "EyeLidUpr_R_TmpJnt";
	rename -uid "DD690800-4029-8699-642D-F9AD548FD779";
	setAttr ".t" -type "double3" 0.30977653108466086 0.024372645398829107 0.11506193458054215 ;
	setAttr ".r" -type "double3" -4.8977031837599627 -21.818140653666177 7.3165712605349962e-13 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 13.029194807943668 -24.270376341976863 -5.4334791855791158 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeLidUpr7_R_TmpJnt" -p "EyeLidUpr_R_TmpJnt";
	rename -uid "2238E292-4EA6-AA10-C12B-9A8628575389";
	setAttr ".t" -type "double3" 0.20080317811323645 -0.073386211824265857 0.038047763452109695 ;
	setAttr ".r" -type "double3" -20.24536343935911 -5.1738647966224169 4.7224863606175622e-13 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 13.029194807943668 -24.270376341976863 -5.4334791855791158 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeLidUpr8_R_TmpJnt" -p "EyeLidUpr_R_TmpJnt";
	rename -uid "1C69AABE-4FB4-2468-1CA0-5B8CD18D1FDB";
	setAttr ".t" -type "double3" -0.00090257979213459461 -0.085281338836361797 0.010027261490402228 ;
	setAttr ".r" -type "double3" -23.941 24.974 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 13.029194807943668 -24.270376341976863 -5.4334791855791158 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeLidUpr9_R_TmpJnt" -p "EyeLidUpr_R_TmpJnt";
	rename -uid "535C12D8-45AB-99DA-03C4-6E850C558B34";
	setAttr ".t" -type "double3" -0.20470713572793836 -0.062053014717193378 0.05852613723432798 ;
	setAttr ".r" -type "double3" -18.870912343958761 56.650551829999252 -4.3391401416338299e-14 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 13.029194807943668 -24.270376341976863 -5.4334791855791158 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeLidUpr10_R_TmpJnt" -p "EyeLidUpr_R_TmpJnt";
	rename -uid "3C30AAC0-44CF-1591-4FA4-7DA599D49D68";
	setAttr ".t" -type "double3" -0.33287961726691762 -0.025684984468815841 0.14906836020788328 ;
	setAttr ".r" -type "double3" -10.700485579886955 78.057126653864387 1.8827914533144553e-13 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 13.029194807943668 -24.270376341976863 -5.4334791855791158 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeLidLwr_R_TmpJnt" -p "Eye_R_TmpJnt";
	rename -uid "7EA8576B-4A64-B18D-6811-9191E6C501E4";
	setAttr ".t" -type "double3" -0.20833799999713687 -0.17596229248046846 0.32100030065917973 ;
	setAttr ".r" -type "double3" -24.709 -32.942 -180 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -180 0 0 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeLidLwr1_R_TmpJnt" -p "EyeLidLwr_R_TmpJnt";
	rename -uid "2F4C084A-43D7-E79D-BEED-82A5D40E49CA";
	setAttr ".t" -type "double3" -0.31504829253792366 -0.044613180039460687 0.1467154919826319 ;
	setAttr ".r" -type "double3" -21.670623621375867 20.859293051943936 -180 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -28.735368514325867 -29.604227302372884 -164.84467494758204 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeLidLwr2_R_TmpJnt" -p "EyeLidLwr_R_TmpJnt";
	rename -uid "C9336229-456A-CCBB-A848-A6A1D953ACA7";
	setAttr ".t" -type "double3" -0.18669329580489621 -0.041554085941697849 0.043897295942408654 ;
	setAttr ".r" -type "double3" -27.515359733096737 -3.0865568828968257 -179.99999999999977 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -28.735368514325867 -29.604227302372884 -164.84467494758204 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeLidLwr3_R_TmpJnt" -p "EyeLidLwr_R_TmpJnt";
	rename -uid "B02E0983-451B-C6D9-4756-E1ACBB291342";
	setAttr ".t" -type "double3" 2.7755575615628914e-17 0 0 ;
	setAttr ".r" -type "double3" -24.708704479623282 -32.942231800685292 -179.99999999999989 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -28.735368514325867 -29.604227302372884 -164.84467494758204 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeLidLwr4_R_TmpJnt" -p "EyeLidLwr_R_TmpJnt";
	rename -uid "B521B4A6-4362-0110-734E-50A5D962D6F6";
	setAttr ".t" -type "double3" 0.17320287750460753 0.053870258895637235 0.054213961224400364 ;
	setAttr ".r" -type "double3" -14.769072774779264 -58.901141802946405 179.99999999999997 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -28.735368514325867 -29.604227302372884 -164.84467494758204 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeLidLwr5_R_TmpJnt" -p "EyeLidLwr_R_TmpJnt";
	rename -uid "8F41FB5B-4005-BB66-D591-A3B6B4DC1CA7";
	setAttr ".t" -type "double3" 0.27903536984075561 0.10226067582348186 0.14378145318140856 ;
	setAttr ".r" -type "double3" -3.2405052034493789 -76.385340322898756 -179.99999999999983 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -28.735368514325867 -29.604227302372884 -164.84467494758204 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeLidLwr6_R_TmpJnt" -p "EyeLidLwr_R_TmpJnt";
	rename -uid "E4AE8807-451A-F3E2-A319-0EA2697C8EBE";
	setAttr ".t" -type "double3" -0.34300374096150921 -0.08783518287246217 0.13347347040206525 ;
	setAttr ".r" -type "double3" -26.006173700298021 23.849081480902921 179.99999999999991 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -28.735368514325867 -29.604227302372884 -164.84467494758204 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeLidLwr7_R_TmpJnt" -p "EyeLidLwr_R_TmpJnt";
	rename -uid "F78A0BA8-4DD5-4642-0C00-C7A74DC458F7";
	setAttr ".t" -type "double3" -0.19433562645487537 -0.12965175079622604 0.035360809872857502 ;
	setAttr ".r" -type "double3" -38.224970263022655 0.3282853521212224 179.99999999999673 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -28.735368514325867 -29.604227302372884 -164.84467494758204 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeLidLwr8_R_TmpJnt" -p "EyeLidLwr_R_TmpJnt";
	rename -uid "F9335591-46CB-A8A4-5E04-34B8C0875546";
	setAttr ".t" -type "double3" 0.030351631804000606 -0.11039959695226997 0.0035685503514288897 ;
	setAttr ".r" -type "double3" -39.40048860302916 -38.148527444594777 -179.99999999999997 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -28.735368514325867 -29.604227302372884 -164.84467494758204 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeLidLwr9_R_TmpJnt" -p "EyeLidLwr_R_TmpJnt";
	rename -uid "2C1FEFE9-4C8A-A475-F17C-AEACC5A21F40";
	setAttr ".t" -type "double3" 0.20462747576594073 -0.034390654760635186 0.066605938172164869 ;
	setAttr ".r" -type "double3" -25.900707955071056 -66.574248692543023 -179.99999999999997 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -28.735368514325867 -29.604227302372884 -164.84467494758204 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeLidLwr10_R_TmpJnt" -p "EyeLidLwr_R_TmpJnt";
	rename -uid "58855C7C-48CD-EE4F-754E-069360B6F704";
	setAttr ".t" -type "double3" 0.31196490444318159 0.05487447320327199 0.1446131124290595 ;
	setAttr ".r" -type "double3" -8.9901110146195986 -81.642667785055394 -179.99999999999989 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -28.735368514325867 -29.604227302372884 -164.84467494758204 ;
	setAttr ".radi" 0.05;
createNode joint -n "Eye_L_TmpJnt" -p "TmpJnt_Grp";
	rename -uid "19D465D2-41EE-9F01-BBD7-9EA7145EF4C5";
	setAttr ".t" -type "double3" 0.52633771300602172 0.94948719245062563 0.74093274325442637 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.05;
createNode joint -n "Iris_L_TmpJnt" -p "Eye_L_TmpJnt";
	rename -uid "887A74C4-427A-EA5B-4B40-36944F611C24";
	setAttr ".t" -type "double3" 0.058746457099914551 -0.0077028274536132813 0.32149371504783641 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.05;
createNode joint -n "Pupil_L_TmpJnt" -p "Iris_L_TmpJnt";
	rename -uid "4B46F3E2-4EE5-D99E-96A3-3487BB0CEB1A";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeDot_L_TmpJnt" -p "Eye_L_TmpJnt";
	rename -uid "F083C88A-435F-2F6F-AAF2-479176AED4EE";
	setAttr ".t" -type "double3" 0.15352907776832581 0.01448822021484375 0.36464214324951172 ;
	setAttr ".r" -type "double3" 0 21.934863766566533 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeInner_L_TmpJnt" -p "Eye_L_TmpJnt";
	rename -uid "28FC276C-49A8-EEFF-D009-E9AF61EAAD80";
	setAttr ".t" -type "double3" -0.19154343008995056 -0.11960411071777344 0.3560405969619751 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeOuter_L_TmpJnt" -p "Eye_L_TmpJnt";
	rename -uid "B7CBE30A-495C-79E9-995A-4CBB7367A539";
	setAttr ".t" -type "double3" 0.39895597100257874 0.025388717651367188 0.051143348217010498 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeLidUpr_L_TmpJnt" -p "Eye_L_TmpJnt";
	rename -uid "622756E1-4869-81E4-E3AB-E99C69C9E23A";
	setAttr ".t" -type "double3" 0.16800000000000004 0.083999999999999631 0.363 ;
	setAttr ".r" -type "double3" -11.86 24.835 -2.190416739716853e-14 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeLidUpr1_L_TmpJnt" -p "EyeLidUpr_L_TmpJnt";
	rename -uid "AD3330DE-4171-4404-54B0-8D8DB1835BBF";
	setAttr ".t" -type "double3" -0.28111531650420363 -0.064497119959264992 -0.11015050669492688 ;
	setAttr ".r" -type "double3" 0.2427213565934192 -17.789392846891921 -1.0483680136060398e-11 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 13.029194807943759 -24.27037634197686 -5.4334791855791185 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeLidUpr2_L_TmpJnt" -p "EyeLidUpr_L_TmpJnt";
	rename -uid "EBCE5DBA-4E54-E41F-846E-0284F9F80137";
	setAttr ".t" -type "double3" -0.17803234056344597 -0.001996676950783538 -0.036608953422476276 ;
	setAttr ".r" -type "double3" -10.407952539187567 -1.1924569456725582 -1.6227449233677762e-12 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 13.029194807943759 -24.27037634197686 -5.4334791855791185 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeLidUpr3_L_TmpJnt" -p "EyeLidUpr_L_TmpJnt";
	rename -uid "9F3B7AB0-4EB2-3934-6117-418CECC9DDB6";
	setAttr ".t" -type "double3" -6.9319408572798835e-05 -0.00011893749834968048 0.00018871336053871701 ;
	setAttr ".r" -type "double3" -11.843365516735384 24.825243253043283 1.0162719139351127e-13 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 13.029194807943759 -24.27037634197686 -5.4334791855791185 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeLidUpr4_L_TmpJnt" -p "EyeLidUpr_L_TmpJnt";
	rename -uid "7C092E37-4A92-B155-6344-99B82B597A4A";
	setAttr ".t" -type "double3" 0.19478874041505473 -0.0087482619769190251 -0.052871131835201091 ;
	setAttr ".r" -type "double3" -9.1568316366288176 53.929572082132374 -1.6881086356479598e-13 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 13.029194807943759 -24.27037634197686 -5.4334791855791185 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeLidUpr5_L_TmpJnt" -p "EyeLidUpr_L_TmpJnt";
	rename -uid "423B5606-47D5-C95C-C09E-38AFE76872E3";
	setAttr ".t" -type "double3" 0.30632242311829472 -0.013636312647136606 -0.14438607357181432 ;
	setAttr ".r" -type "double3" -5.8098735351237023 74.349670035987714 4.1265209117907955e-14 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 13.029194807943759 -24.27037634197686 -5.4334791855791185 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeLidUpr6_L_TmpJnt" -p "EyeLidUpr_L_TmpJnt";
	rename -uid "6C637F33-42DB-4CB0-AB84-BFA9F4E91123";
	setAttr ".t" -type "double3" -0.30977606495052429 -0.024370844856443696 -0.11506105402496836 ;
	setAttr ".r" -type "double3" -4.8977031837599627 -21.818140653666177 7.3165712605349962e-13 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 13.029194807943759 -24.27037634197686 -5.4334791855791185 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeLidUpr7_L_TmpJnt" -p "EyeLidUpr_L_TmpJnt";
	rename -uid "20ECBB29-4C83-82FF-E72B-FCBD1D6FA0A1";
	setAttr ".t" -type "double3" -0.20080274355317529 0.073467061472207718 -0.038030062439566859 ;
	setAttr ".r" -type "double3" -20.24536343935911 -5.1738647966224169 4.7224863606175622e-13 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 13.029194807943759 -24.27037634197686 -5.4334791855791185 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeLidUpr8_L_TmpJnt" -p "EyeLidUpr_L_TmpJnt";
	rename -uid "A019AB44-4499-D3AC-B142-2BAC863E10EB";
	setAttr ".t" -type "double3" 0.00090226008799737523 0.08533960071092217 -0.010014466559723711 ;
	setAttr ".r" -type "double3" -23.941 24.974 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 13.029194807943759 -24.27037634197686 -5.4334791855791185 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeLidUpr9_L_TmpJnt" -p "EyeLidUpr_L_TmpJnt";
	rename -uid "77AE1446-4E42-040E-E258-37854F751C08";
	setAttr ".t" -type "double3" 0.20470725178484095 0.062078243724377202 -0.058520963295093154 ;
	setAttr ".r" -type "double3" -18.870912343958761 56.650551829999252 -4.3391401416338299e-14 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 13.029194807943759 -24.27037634197686 -5.4334791855791185 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeLidUpr10_L_TmpJnt" -p "EyeLidUpr_L_TmpJnt";
	rename -uid "76E49F89-4B6F-E5E7-C6B6-018D82409C4A";
	setAttr ".t" -type "double3" 0.33287992254200049 0.025716435514961944 -0.14906095641314465 ;
	setAttr ".r" -type "double3" -10.700485579886955 78.057126653864387 1.8827914533144553e-13 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 13.029194807943759 -24.27037634197686 -5.4334791855791185 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeLidLwr_L_TmpJnt" -p "Eye_L_TmpJnt";
	rename -uid "8A384330-4D91-157E-0352-BFB57FAE6291";
	setAttr ".t" -type "double3" 0.20799999999999996 -0.17600000000000016 0.32099999999999995 ;
	setAttr ".r" -type "double3" -24.709 -32.942 -180 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeLidLwr1_L_TmpJnt" -p "EyeLidLwr_L_TmpJnt";
	rename -uid "2B5F0DC2-4D62-4797-DD76-11BB1C84F68D";
	setAttr ".t" -type "double3" 0.315047948136774 0.044548175598665196 -0.14674491271371881 ;
	setAttr ".r" -type "double3" -21.670623621375867 20.859293051943936 -180 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -28.735368514325859 -29.604227302372873 -164.84467494758204 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeLidLwr2_L_TmpJnt" -p "EyeLidLwr_L_TmpJnt";
	rename -uid "D4896929-47E5-29A2-124A-678C9CB81B73";
	setAttr ".t" -type "double3" 0.18669293728759023 0.041510424349581854 -0.043916618224038473 ;
	setAttr ".r" -type "double3" -27.515359733096737 -3.0865568828968257 -179.99999999999977 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -28.735368514325859 -29.604227302372873 -164.84467494758204 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeLidLwr3_L_TmpJnt" -p "EyeLidLwr_L_TmpJnt";
	rename -uid "50C5E597-4E83-368D-0F3C-EBB47BE7A189";
	setAttr ".t" -type "double3" -5.5511151231257827e-17 0 0 ;
	setAttr ".r" -type "double3" -24.708704479623282 -32.942231800685292 -179.99999999999989 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -28.735368514325859 -29.604227302372873 -164.84467494758204 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeLidLwr4_L_TmpJnt" -p "EyeLidLwr_L_TmpJnt";
	rename -uid "C7418850-420E-432A-C436-DA90B494B7DF";
	setAttr ".t" -type "double3" -0.17320293237217305 -0.053946022676214511 -0.054248887233782739 ;
	setAttr ".r" -type "double3" -14.769072774779264 -58.901141802946405 179.99999999999997 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -28.735368514325859 -29.604227302372873 -164.84467494758204 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeLidLwr5_L_TmpJnt" -p "EyeLidLwr_L_TmpJnt";
	rename -uid "029541F6-4D99-74E4-70EB-FA92B5A45BB7";
	setAttr ".t" -type "double3" -0.27903503856060313 -0.10228406728843709 -0.14379158563220518 ;
	setAttr ".r" -type "double3" -3.2405052034493789 -76.385340322898756 -179.99999999999983 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -28.735368514325859 -29.604227302372873 -164.84467494758204 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeLidLwr6_L_TmpJnt" -p "EyeLidLwr_L_TmpJnt";
	rename -uid "F2DFBECE-4215-03A7-8921-C8B731F52482";
	setAttr ".t" -type "double3" 0.34300421531721725 0.087838433665066162 -0.13347153334669937 ;
	setAttr ".r" -type "double3" -26.006173700298021 23.849081480902921 179.99999999999991 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -28.735368514325859 -29.604227302372873 -164.84467494758204 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeLidLwr7_L_TmpJnt" -p "EyeLidLwr_L_TmpJnt";
	rename -uid "F82D8705-443B-DEFF-AE02-E4BACB9A0777";
	setAttr ".t" -type "double3" 0.19433570624678204 0.12966289895766714 -0.0353557684250978 ;
	setAttr ".r" -type "double3" -38.224970263022655 0.3282853521212224 179.99999999999673 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -28.735368514325859 -29.604227302372873 -164.84467494758204 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeLidLwr8_L_TmpJnt" -p "EyeLidLwr_L_TmpJnt";
	rename -uid "290BA869-45F0-9371-66BE-CE9C80671D2C";
	setAttr ".t" -type "double3" -0.030352230410214731 0.11036048517862973 -0.0035861066101166017 ;
	setAttr ".r" -type "double3" -39.40048860302916 -38.148527444594777 -179.99999999999997 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -28.735368514325859 -29.604227302372873 -164.84467494758204 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeLidLwr9_L_TmpJnt" -p "EyeLidLwr_L_TmpJnt";
	rename -uid "9422757B-47D9-6E25-D2DC-D98ADFC8940F";
	setAttr ".t" -type "double3" -0.20462732441513065 0.034394735538620935 -0.066603261152170745 ;
	setAttr ".r" -type "double3" -25.900707955071056 -66.574248692543023 -179.99999999999997 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -28.735368514325859 -29.604227302372873 -164.84467494758204 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeLidLwr10_L_TmpJnt" -p "EyeLidLwr_L_TmpJnt";
	rename -uid "D6E9F62A-43F9-839F-0C50-BBA567CBE4E4";
	setAttr ".t" -type "double3" -0.31196494377481465 -0.054879052999821099 -0.14461463432418142 ;
	setAttr ".r" -type "double3" -8.9901110146195986 -81.642667785055394 -179.99999999999989 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -28.735368514325859 -29.604227302372873 -164.84467494758204 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeBrowMid_L_TmpJnt" -p "TmpJnt_Grp";
	rename -uid "3D2AC4C7-46B9-D587-24E5-1186BE0C2195";
	setAttr ".t" -type "double3" 0.68503600359249328 1.4452233145819733 1.0952606603534254 ;
	setAttr ".r" -type "double3" 0 30.736 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeBrowInner_L_TmpJnt" -p "EyeBrowMid_L_TmpJnt";
	rename -uid "83B9FD3E-472C-ECCA-344B-7F9F9BF21B24";
	setAttr ".t" -type "double3" -0.50442523043060306 -0.0615081787109375 -0.069056761041909054 ;
	setAttr ".r" -type "double3" 0 11.352000000000002 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 -30.736000000000008 0 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeBrowOuter_L_TmpJnt" -p "EyeBrowMid_L_TmpJnt";
	rename -uid "179E265F-4B96-5783-E6CC-4B8388A3544B";
	setAttr ".t" -type "double3" 0.58529226305139981 -0.18535232543945313 -0.12847564605165407 ;
	setAttr ".r" -type "double3" 0 61.712999999999994 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 -30.736000000000008 0 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeBrowMid_R_TmpJnt" -p "TmpJnt_Grp";
	rename -uid "E9E116DC-4763-8730-1E54-3DB58C625AB1";
	setAttr ".t" -type "double3" -0.68503599999713682 1.4452248999701567 1.0952610439136059 ;
	setAttr ".r" -type "double3" 0 -30.736 -180 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -180 0 0 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeBrowInner_R_TmpJnt" -p "EyeBrowMid_R_TmpJnt";
	rename -uid "A5D983E2-4395-E657-53F1-40AD5BFA12EA";
	setAttr ".t" -type "double3" -0.50442584720419092 -0.061499999999998778 0.069055674244293819 ;
	setAttr ".r" -type "double3" 0 -11.352000000000002 -180 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 4.1721823403348504e-15 -30.735999999999994 180 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeBrowOuter_R_TmpJnt" -p "EyeBrowMid_R_TmpJnt";
	rename -uid "A8BA128F-4888-8C50-4CCE-999B8D44E825";
	setAttr ".t" -type "double3" 0.58529123974878761 -0.18539999999999779 0.12847647221700575 ;
	setAttr ".r" -type "double3" 0 -61.712999999999994 -180 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 4.1721823403348504e-15 -30.735999999999994 180 ;
	setAttr ".radi" 0.05;
createNode joint -n "EyeBrow_C_TmpJnt" -p "TmpJnt_Grp";
	rename -uid "BEDBD0C2-4925-BFAB-77BC-DCA8F3616DE1";
	setAttr ".t" -type "double3" 2.8632053254437986e-12 1.3379387686835358 1.326253096762422 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.05;
createNode joint -n "HeadPos_TmpJnt" -p "TmpJnt_Grp";
	rename -uid "151F0123-4397-D8A3-CA48-2BA307C1625B";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 2.8632053254437986e-12 -0.29913571162346564 0.16446904391360606 ;
	setAttr ".s" -type "double3" 1.0000000000000013 1 0.99999999999999867 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".is" -type "double3" 1.0000000000000009 1 0.99999999999999933 ;
	setAttr ".bps" -type "matrix" 1.0000000000000013 5.7677653070062022e-16 -5.7428840619523754e-14 0
		 -3.9281737601001492e-16 0.99999302813598423 0.0037341236488293368 0 5.7508815496966421e-14 -0.0037341236488293876 0.99999302813598301 0
		 2.7582089509916692e-12 3.1579897975355844 43.287873653853673 1;
	setAttr ".radi" 0.05;
createNode joint -n "cheekU_L_TmpJnt" -p "HeadPos_TmpJnt";
	rename -uid "27BE1FCF-4424-7B1C-C45D-9D9700ED42BC";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.51149738517473453 0.63803386984302968 1.5118859677860661 ;
	setAttr ".s" -type "double3" 0.99999999999999889 1 1.0000000000000013 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 0.91228105091926848 0.40956474962285894 -9.9746599868666408e-18 0
		 -0.40956474962285994 0.91228105091926825 3.4694469519536142e-18 0 8.8901712446213503e-17 -4.5970172113385388e-17 1.0000000000000002 0
		 3.9427188542344718 -5.3237105726561431 45.022308497181172 1;
	setAttr ".radi" 0.05;
createNode joint -n "cheekD_L_TmpJnt" -p "HeadPos_TmpJnt";
	rename -uid "5676030D-4394-8BA0-972B-78B3D14466AC";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.8690825314085916 0.16092411559487907 1.3647674298021337 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 0.30059000016696891 0.78149195566352603 0.54673208707082366 0
		 -0.93333913514334965 0.35899590361154021 1.2381588809784461e-16 0 -0.19627457963141395 -0.51028645330180111 0.83730760474701238 0
		 6.6990529801024516 -0.0097290463850563391 43.888290904269802 1;
	setAttr ".radi" 0.05;
createNode joint -n "cheekU_R_TmpJnt" -p "HeadPos_TmpJnt";
	rename -uid "794E610D-40BB-AA70-62E4-9AB1C2EE1193";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.51149781728284349 0.63803381764609846 1.5118846708302123 ;
	setAttr ".s" -type "double3" 0.99999999999999889 1.0000000000000002 1.0000000000000013 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 0.91228105091926848 -0.40956474962285905 -9.7578195523695399e-18 0
		 -0.40956474962285994 -0.91228105091926825 -4.0635897424756706e-16 0 7.9366464723536785e-17 4.2067044292437572e-16 -1 0
		 -3.9427221850000014 -5.3237109749999938 45.022298499999934 1;
	setAttr ".radi" 0.05;
createNode joint -n "cheekD_R_TmpJnt" -p "HeadPos_TmpJnt";
	rename -uid "9EDE80FD-4ED6-F00D-4198-F08E0828DB24";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.86908261572192269 0.16092412135304812 1.364768460618673 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 0.30059000016697568 -0.78149195566351159 -0.54673208707084009 0
		 -0.93333913514334965 -0.35899590361153993 -2.1260120400268256e-14 0 -0.19627457963140341 0.51028645330182298 -0.83730760474700139 0
		 -6.6990536300000096 -0.0097290020000073696 43.888298849999956 1;
	setAttr ".radi" 0.05;
createNode joint -n "tongue01_TmpJnt" -p "HeadPos_TmpJnt";
	rename -uid "66F18529-40AA-4DE8-9B84-E0A3C94420B0";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -1.8416602284401707e-15 -0.30385102541904807 0.30380789519129986 ;
	setAttr ".s" -type "double3" 0.99999999999999889 1.0000000000000002 1.0000000000000013 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 53.232358192036152 0 0 ;
	setAttr ".bps" -type "matrix" 1.0000000000000002 5.7677653070061962e-16 -5.7428840619523691e-14 0
		 2.1293487851944074e-14 0.92496633972913433 0.380049036794053 0 5.3422893411477993e-14 -0.38004903679405405 0.92496633972913445 0
		 2.7578487462019971e-12 2.1020391679035786 43.517308688170466 1;
	setAttr ".radi" 0.05;
createNode joint -n "tongue02_TmpJnt" -p "tongue01_TmpJnt";
	rename -uid "FFC5AF8A-4938-D532-DF5E-BA81F138822C";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 0.18312269798740388 -3.5527136788005009e-15 ;
	setAttr ".s" -type "double3" 1 0.99999999999999978 1 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 12.968767978005925 0 0 ;
	setAttr ".bps" -type "matrix" 1.0000000000000002 5.7677653070061962e-16 -5.7428840619523691e-14 0
		 2.1293487851944067e-14 0.924966339729134 0.38004903679405289 0 5.3422893411477993e-14 -0.38004903679405405 0.92496633972913445 0
		 2.7023626978406629e-12 -0.31498038247475613 42.524206655611529 1;
	setAttr ".radi" 0.05;
createNode joint -n "tongue03_TmpJnt" -p "tongue02_TmpJnt";
	rename -uid "176FF831-4B54-2E25-3A60-B0BF1A2D4BF8";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 3.9443045261050599e-31 0.21199913657559577 5.3290705182007514e-15 ;
	setAttr ".s" -type "double3" 1 0.99999999999999944 1 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 6.4353347094920936 0 0 ;
	setAttr ".bps" -type "matrix" 1.0000000000000002 5.7677653070061962e-16 -5.7428840619523691e-14 0
		 2.6677824958728549e-14 0.88094787514788186 0.47321331476663903 0 5.094812872742697e-14 -0.47321331476664041 0.88094787514788297 0
		 2.7023626978406589e-12 -1.8613809396298249 41.68648469376356 1;
	setAttr ".radi" 0.05;
createNode joint -n "tongue04_TmpJnt" -p "tongue03_TmpJnt";
	rename -uid "2839D0CE-456E-F2BC-AD29-A4A6DA7E72BB";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -3.9443045261050599e-31 0.17893409237620173 0 ;
	setAttr ".s" -type "double3" 1 0.99999999999999944 1 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 18.616125179957578 0 0 ;
	setAttr ".bps" -type "matrix" 1.0000000000000002 5.7677653070061962e-16 -5.7428840619523691e-14 0
		 2.6677824958728549e-14 0.88094787514788186 0.47321331476663903 0 5.094812872742697e-14 -0.47321331476664041 0.88094787514788297 0
		 2.7023626978406589e-12 -1.8613809396298249 41.68648469376356 1;
	setAttr ".radi" 0.05;
createNode joint -n "tongue05_TmpJnt" -p "tongue04_TmpJnt";
	rename -uid "C22BA8CE-4D35-7734-3424-39BE6255CA15";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 1.1832913578315177e-30 0.20011372794880278 -5.3290705182007514e-15 ;
	setAttr ".s" -type "double3" 1 0.99999999999999944 1 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 9.2773326301724737 0 0 ;
	setAttr ".bps" -type "matrix" 1.0000000000000002 5.7677653070061962e-16 -5.7428840619523691e-14 0
		 2.6677824958728549e-14 0.88094787514788186 0.47321331476663903 0 5.094812872742697e-14 -0.47321331476664041 0.88094787514788297 0
		 2.7023626978406589e-12 -1.8613809396298249 41.68648469376356 1;
	setAttr ".radi" 0.05;
createNode joint -n "tongue06_TmpJnt" -p "tongue05_TmpJnt";
	rename -uid "AF27DA9A-4120-E89F-56AC-FEA173B40141";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -7.8886090522101181e-31 0.25180761619672587 5.3290705182007514e-15 ;
	setAttr ".r" -type "double3" 3.1805546814635168e-14 0 0 ;
	setAttr ".s" -type "double3" 1 0.99999999999999944 1 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1.0000000000000002 5.7677653070061962e-16 -5.7428840619523691e-14 0
		 2.6677824958728549e-14 0.88094787514788186 0.47321331476663903 0 5.094812872742697e-14 -0.47321331476664041 0.88094787514788297 0
		 2.7023626978406589e-12 -1.8613809396298249 41.68648469376356 1;
	setAttr ".radi" 0.05;
createNode joint -n "teethMainUp_TmpJnt" -p "HeadPos_TmpJnt";
	rename -uid "53C1760F-4968-11AA-B31A-79B4BC6856FC";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.06595257337787637 0.13676547403703587 1.1712966056945937 ;
	setAttr ".s" -type "double3" 0.99999999999999889 1 1.0000000000000013 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 89.999999999999986 0 0 ;
	setAttr ".bps" -type "matrix" 1.0000000000000002 5.7677653070061962e-16 -5.7428840619523691e-14 0
		 2.7086258208957452e-14 0.8771178665751479 0.48027517959463645 0 5.0732167128880986e-14 -0.48027517959463778 0.87711786657514823 0
		 0.01592607367873344 0.12501306961924419 46.465345148587645 1;
	setAttr ".radi" 0.05;
createNode joint -n "teethUp_R_TmpJnt" -p "teethMainUp_TmpJnt";
	rename -uid "4C5EAD9D-4222-1D2B-6986-FA8723C0252E";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.47547610541586782 7.9557275434183339e-08 -2.3200923671140572e-06 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 180 0 90.000000000000014 ;
	setAttr ".bps" -type "matrix" 1 5.7677653070061953e-16 -5.7428840619523678e-14 0
		 -2.7086258208956587e-14 -0.877117866575156 -0.48027517959462146 0 5.073216712888146e-14 -0.48027517959462296 0.87711786657515667 0
		 -4.4033729599999996 -0.012855628050096907 46.389887850000157 1;
	setAttr ".radi" 0.05;
createNode joint -n "teethUp_L_TmpJnt" -p "teethMainUp_TmpJnt";
	rename -uid "8F61840A-4894-3514-9ADB-20B644043C2D";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.47547582357995 -8.8817841970012523e-16 0 ;
	setAttr ".s" -type "double3" 1 0.99999999999999933 1.0000000000000004 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 -89.999999999999972 ;
	setAttr ".bps" -type "matrix" 1.0000000000000002 5.7677653070061962e-16 -5.7428840619523691e-14 0
		 2.7086258208957436e-14 0.87711786657514745 0.48027517959463617 0 5.0732167128881012e-14 -0.480275179594638 0.87711786657514867 0
		 4.4033707420688959 -0.012855669558010679 46.389853659938275 1;
	setAttr ".radi" 0.05;
createNode joint -n "teethUp_TmpJnt" -p "teethMainUp_TmpJnt";
	rename -uid "2166224B-4135-1329-D8EB-30B729628B21";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 0.42595039362508413 0 ;
	setAttr ".s" -type "double3" 1 0.99999999999999933 1.0000000000000004 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1.0000000000000002 5.7677653070061962e-16 -5.7428840619523691e-14 0
		 2.7086258208957436e-14 0.87711786657514745 0.48027517959463617 0 5.0732167128881012e-14 -0.480275179594638 0.87711786657514867 0
		 2.7804598901060018e-12 -5.1175310879145268 43.594734558218917 1;
	setAttr ".radi" 0.05;
createNode joint -n "teethMainDn_TmpJnt" -p "HeadPos_TmpJnt";
	rename -uid "A5E946F2-4CF9-9AE1-1847-05973B05B2A9";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.06595257337787637 0.047191122681011777 1.121486525779277 ;
	setAttr ".r" -type "double3" 90 0 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1.0000000000000002 5.7677653070061962e-16 -5.7428840619523691e-14 0
		 2.9511115583222014e-14 0.85283227421620023 0.52218494047149933 0 4.9361039518923608e-14 -0.52218494047150066 0.85283227421620034 0
		 -1.8521245151397982e-09 2.045740909568214 43.000912267151918 1;
	setAttr ".radi" 0.05;
createNode joint -n "teethDn_L_TmpJnt" -p "teethMainDn_TmpJnt";
	rename -uid "F4B9BD14-4F0E-A16E-7DB9-D2827A99B280";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.50041632220420273 -1.1102230246251565e-15 -8.3401110912884341e-17 ;
	setAttr ".s" -type "double3" 1 0.99999999999999933 1.0000000000000004 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 -89.999999999999986 ;
	setAttr ".bps" -type "matrix" 0.97478167878208011 -0.19031861039197456 -0.11653113424856819 0
		 0.22316065672691957 0.83132527598001194 0.50901631290751892 0 4.9361039518923627e-14 -0.52218494047150088 0.85283227421620067 0
		 4.534801201274111 2.0316239137271781 42.992268501131633 1;
	setAttr ".radi" 0.05;
createNode joint -n "teethDn_TmpJnt" -p "teethMainDn_TmpJnt";
	rename -uid "D7486291-48D5-3084-B374-EFB4C6CFD555";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 0.50609004511912969 -3.0179546387218632e-17 ;
	setAttr ".s" -type "double3" 1 0.99999999999999933 1.0000000000000002 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1.0000000000000002 5.7677653070061962e-16 -5.7428840619523691e-14 0
		 3.8632187485347138e-14 0.7336441155904575 0.6795338929439011 0 4.2602491188333267e-14 -0.67953389294390298 0.7336441155904585 0
		 -1.8521245151398003e-09 -2.7053269250402261 40.091857169921767 1;
	setAttr ".radi" 0.05;
createNode joint -n "teethDn_R_TmpJnt" -p "teethMainDn_TmpJnt";
	rename -uid "33C6B8EF-4F81-6A80-5610-968FC49A68A5";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.50041642662212416 3.4742207237759004e-06 3.0511087388163105e-05 ;
	setAttr ".s" -type "double3" 1 0.99999999999999933 1.0000000000000004 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -180 0 89.999999999999986 ;
	setAttr ".bps" -type "matrix" 0.97478167878208011 -0.19031861039197456 -0.11653113424856819 0
		 0.22316065672691957 0.83132527598001194 0.50901631290751892 0 4.9361039518923627e-14 -0.52218494047150088 0.85283227421620067 0
		 4.534801201274111 2.0316239137271781 42.992268501131633 1;
	setAttr ".radi" 0.05;
createNode joint -n "lipUp_TmpJnt" -p "HeadPos_TmpJnt";
	rename -uid "DB91C155-4DCF-9D19-CB50-9785C9FA27F2";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.00039836916876291649 0.19070107065319419 1.810271015565917 ;
	setAttr ".s" -type "double3" 0.99999999999999889 1.0000000000000002 1.0000000000000013 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1.0000000000000002 5.7677653070061962e-16 -5.7428840619523691e-14 0
		 2.3929716163367511e-14 0.90487439190602448 0.4256786756142501 0 5.2295189546417125e-14 -0.42567867561425116 0.90487439190602459 0
		 0.0030707051091594622 -6.0628083042306447 41.178591889943434 1;
	setAttr ".radi" 0.05;
createNode joint -n "lipDn_TmpJnt" -p "HeadPos_TmpJnt";
	rename -uid "DE2A9300-4013-FF4D-A4FF-5F9F20C217FC";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.00039836916876291649 -0.11712860272701242 1.810271015565917 ;
	setAttr ".s" -type "double3" 0.99999999999999889 1.0000000000000002 1.0000000000000013 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1.0000000000000002 5.7677653070061962e-16 -5.7428840619523691e-14 0
		 3.2042934045430673e-14 0.8244659544551155 0.56591155664504211 0 4.7756345579568861e-14 -0.56591155664504367 0.82446595445511561 0
		 0.0030707051091592406 -4.3823947943021277 39.530465108334667 1;
	setAttr ".radi" 0.05;
createNode joint -n "lipUp1_L_TmpJnt" -p "HeadPos_TmpJnt";
	rename -uid "6C87D38F-4A12-74D7-8594-899222BD5747";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.30365060887364953 0.19070107065319419 1.810271015565917 ;
	setAttr ".s" -type "double3" 0.99999999999999889 1.0000000000000002 1.0000000000000013 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 20.05232061580989 0 ;
	setAttr ".bps" -type "matrix" 0.82995227039268116 0.46635673384895515 0.30608924460648446 0
		 -0.55006845717557218 0.77543132774191237 0.3100499127187446 0 -0.092757324727058291 -0.42569666753470092 0.90009912007474502 0
		 2.3731795743456856 -5.0930367687478064 41.718719177983019 1;
	setAttr ".radi" 0.05;
createNode joint -n "lipUp2_L_TmpJnt" -p "HeadPos_TmpJnt";
	rename -uid "00F7B86D-4210-1F08-6EEE-FF87EDC9223B";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.57387264188297471 0.19070107065319064 1.6654646789867067 ;
	setAttr ".s" -type "double3" 0.99999999999999889 1 1.0000000000000013 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 32.763906087709969 0 ;
	setAttr ".bps" -type "matrix" 0.8185523695232757 0.45266987781597179 0.35364106105754067 0
		 -0.57195586300494139 0.58516454486653979 0.57484688935872963 0 0.053277660629052009 -0.67280936166882299 0.73789501538409097 0
		 4.5812682868491317 -3.6850020464801649 42.372932534218236 1;
	setAttr ".radi" 0.05;
createNode joint -n "lipDn1_L_TmpJnt" -p "HeadPos_TmpJnt";
	rename -uid "C29E144B-487D-B3D4-1707-C3847A0A0D52";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.30365060887364953 -0.11706595237253481 1.810271015565917 ;
	setAttr ".s" -type "double3" 0.99999999999999867 1 1.0000000000000013 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 22.052320615809887 0 ;
	setAttr ".bps" -type "matrix" 0.79760097970714605 0.49443431716630121 0.34549585117406678 0
		 -0.60292369248800015 0.63664164337077689 0.48080187079793268 0 0.017767898211013058 -0.59179567752261442 0.80589216267358887 0
		 2.392434881244601 -3.7081886358561831 40.044019820819173 1;
	setAttr ".radi" 0.05;
createNode joint -n "lipDn2_L_TmpJnt" -p "HeadPos_TmpJnt";
	rename -uid "8BAD3425-466A-0621-5658-40B4382A73A4";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.57390633966227744 -0.11706595237253836 1.6654646789867067 ;
	setAttr ".s" -type "double3" 0.99999999999999889 1 1.0000000000000013 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 32.763906087709969 0 ;
	setAttr ".bps" -type "matrix" 0.8185523695232757 0.45266987781597179 0.35364106105754067 0
		 -0.57195586300494139 0.58516454486653979 0.57484688935872963 0 0.053277660629052009 -0.67280936166882299 0.73789501538409097 0
		 4.5258731406293524 -2.4182823207010311 41.191899026875838 1;
	setAttr ".radi" 0.05;
createNode joint -n "lipCnr_L_TmpJnt" -p "HeadPos_TmpJnt";
	rename -uid "9CEA3804-48D7-1FFD-DAF8-C5A6662942B4";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.65096879989102763 0.046555700289898638 1.6061276195474512 ;
	setAttr ".s" -type "double3" 0.99999999999999889 1 1.0000000000000013 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 43.874351634461739 0 ;
	setAttr ".bps" -type "matrix" 0.8185523695232757 0.45266987781597179 0.35364106105754067 0
		 -0.57195586300494139 0.58516454486653979 0.57484688935872963 0 0.053277660629052009 -0.67280936166882299 0.73789501538409097 0
		 4.5812682868491317 -3.6850020464801649 42.372932534218236 1;
	setAttr ".radi" 0.05;
createNode joint -n "lipMainUp_TmpJnt" -p "HeadPos_TmpJnt";
	rename -uid "272D65DA-4094-3276-9CFA-8C9DDB9D3A20";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.00039836916876291649 0.19070107065319419 1.810271015565917 ;
	setAttr ".s" -type "double3" 0.99999999999999889 1.0000000000000002 1.0000000000000013 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1.0000000000000002 5.7677653070061962e-16 -5.7428840619523691e-14 0
		 2.3929716163367511e-14 0.90487439190602448 0.4256786756142501 0 5.2295189546417125e-14 -0.42567867561425116 0.90487439190602459 0
		 0.0030707051091594622 -6.0628083042306447 41.178591889943434 1;
	setAttr ".radi" 0.05;
createNode joint -n "lipMainDn_TmpJnt" -p "HeadPos_TmpJnt";
	rename -uid "58B8A314-4254-0EA2-2468-CAA5FC7CB96B";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.00039836916876291649 -0.11712860272701242 1.810271015565917 ;
	setAttr ".r" -type "double3" 0 -1.8391287186326793e-14 5.6294003190949772e-15 ;
	setAttr ".s" -type "double3" 0.99999999999999889 1.0000000000000002 1.0000000000000013 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1.0000000000000002 5.7677653070061962e-16 -5.7428840619523691e-14 0
		 3.2042934045430673e-14 0.8244659544551155 0.56591155664504211 0 4.7756345579568861e-14 -0.56591155664504367 0.82446595445511561 0
		 0.0030707051091592406 -4.3823947943021277 39.530465108334667 1;
	setAttr ".radi" 0.05;
createNode joint -n "jawDn1_TmpJnt" -p "HeadPos_TmpJnt";
	rename -uid "CD4BAEE1-48F8-F3E9-69AB-EFB813725E71";
	setAttr ".t" -type "double3" 0 0.24601966540786258 0.39184782580485694 ;
	setAttr ".jo" -type "double3" 112.98658027531923 0 0 ;
	setAttr ".radi" 0.05;
createNode joint -n "jawDn2_TmpJnt" -p "jawDn1_TmpJnt";
	rename -uid "272D57B0-49AC-824D-513C-56A503BAF5D4";
	setAttr ".t" -type "double3" 0 0.47433519460899598 0.21327934514105884 ;
	setAttr ".jo" -type "double3" -12.019517947010007 0 0 ;
	setAttr ".radi" 0.05;
createNode joint -n "jawDn3_TmpJnt" -p "jawDn2_TmpJnt";
	rename -uid "742769EC-40E3-B154-F877-228614D8B990";
	setAttr ".t" -type "double3" 0 1.1130910317385727 3.5527136788005009e-15 ;
	setAttr ".jo" -type "double3" 4.7708320221952767e-15 0 0 ;
	setAttr ".radi" 0.05;
createNode joint -n "nose_TmpJnt" -p "HeadPos_TmpJnt";
	rename -uid "84F483A7-4B2A-88ED-34F4-0CA7767B47E5";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.06595257337787637 0.35495628131805135 1.570963348591556 ;
	setAttr ".s" -type "double3" 0.99999999999999889 1 1.0000000000000013 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 89.999999999999986 0 0 ;
	setAttr ".bps" -type "matrix" 1.0000000000000002 5.7677653070061962e-16 -5.7428840619523691e-14 0
		 2.7086258208957452e-14 0.8771178665751479 0.48027517959463645 0 5.0732167128880986e-14 -0.48027517959463778 0.87711786657514823 0
		 0.01592607367873344 0.12501306961924419 46.465345148587645 1;
	setAttr ".radi" 0.05;
createNode joint -n "nose_L_TmpJnt" -p "nose_TmpJnt";
	rename -uid "F40D5CD9-4180-B02C-E734-DBAB60723C4F";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.11216769049882053 -1.291932324112161e-15 -2.0167652719012557e-17 ;
	setAttr ".s" -type "double3" 1 0.99999999999999933 1.0000000000000004 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 -89.999999999999972 ;
	setAttr ".bps" -type "matrix" 1.0000000000000002 5.7677653070061962e-16 -5.7428840619523691e-14 0
		 2.7086258208957436e-14 0.87711786657514745 0.48027517959463617 0 5.0732167128881012e-14 -0.480275179594638 0.87711786657514867 0
		 4.4033707420688959 -0.012855669558010679 46.389853659938275 1;
	setAttr ".radi" 0.05;
createNode joint -n "nose_R_TmpJnt" -p "nose_TmpJnt";
	rename -uid "EFA62FA3-4A36-BE17-8685-919129F5B6C4";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.11216742662212384 -3.3485915549302803e-06 -4.3302755727836484e-06 ;
	setAttr ".s" -type "double3" 1 0.99999999999999933 1.0000000000000004 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -180 0 89.999999999999972 ;
	setAttr ".bps" -type "matrix" 1.0000000000000002 5.7677653070061962e-16 -5.7428840619523691e-14 0
		 2.7086258208957436e-14 0.87711786657514745 0.48027517959463617 0 5.0732167128881012e-14 -0.480275179594638 0.87711786657514867 0
		 4.4033707420688959 -0.012855669558010679 46.389853659938275 1;
	setAttr ".radi" 0.05;
createNode joint -n "lipUp1_R_TmpJnt" -p "HeadPos_TmpJnt";
	rename -uid "1AC420FE-470A-7227-CE22-D39F5E0C159A";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.30365099999999989 0.19066061159362313 1.8102700000000012 ;
	setAttr ".s" -type "double3" 0.99999999999999889 1.0000000000000002 1.0000000000000013 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -180 -20.05232061580994 3.7589957057380797e-31 ;
	setAttr ".bps" -type "matrix" 0.82995227039268116 0.46635673384895515 0.30608924460648446 0
		 -0.55006845717557218 0.77543132774191237 0.3100499127187446 0 -0.092757324727058291 -0.42569666753470092 0.90009912007474502 0
		 2.3731795743456856 -5.0930367687478064 41.718719177983019 1;
	setAttr ".radi" 0.05;
createNode joint -n "lipDn1_R_TmpJnt" -p "HeadPos_TmpJnt";
	rename -uid "F977D2D1-441D-82A2-4C4A-C48E6B0009C3";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.30365099999999989 -0.1170393884063774 1.8102700000000012 ;
	setAttr ".s" -type "double3" 0.99999999999999867 1 1.0000000000000013 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -180 -22.052320615809933 1.1429544724241213e-30 ;
	setAttr ".bps" -type "matrix" 0.79760097970714605 0.49443431716630121 0.34549585117406678 0
		 -0.60292369248800015 0.63664164337077689 0.48080187079793268 0 0.017767898211013058 -0.59179567752261442 0.80589216267358887 0
		 2.392434881244601 -3.7081886358561831 40.044019820819173 1;
	setAttr ".radi" 0.05;
createNode joint -n "lipUp2_R_TmpJnt" -p "HeadPos_TmpJnt";
	rename -uid "A8667AC1-4371-5A0E-3B48-04A58137A2EC";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.57387299999999974 0.19066061159362313 1.6654600000000011 ;
	setAttr ".s" -type "double3" 0.99999999999999889 1 1.0000000000000013 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -180 -32.763906087710033 8.3983654350554423e-31 ;
	setAttr ".bps" -type "matrix" 0.8185523695232757 0.45266987781597179 0.35364106105754067 0
		 -0.57195586300494139 0.58516454486653979 0.57484688935872963 0 0.053277660629052009 -0.67280936166882299 0.73789501538409097 0
		 4.5812682868491317 -3.6850020464801649 42.372932534218236 1;
	setAttr ".radi" 0.05;
createNode joint -n "lipDn2_R_TmpJnt" -p "HeadPos_TmpJnt";
	rename -uid "8F70DE12-4263-FE75-52A7-C8B60D8B7DE4";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.57390599999999981 -0.1170393884063774 1.6654600000000011 ;
	setAttr ".s" -type "double3" 0.99999999999999889 1 1.0000000000000013 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -180 -32.763906087710033 8.3983654350554423e-31 ;
	setAttr ".bps" -type "matrix" 0.8185523695232757 0.45266987781597179 0.35364106105754067 0
		 -0.57195586300494139 0.58516454486653979 0.57484688935872963 0 0.053277660629052009 -0.67280936166882299 0.73789501538409097 0
		 4.5258731406293524 -2.4182823207010311 41.191899026875838 1;
	setAttr ".radi" 0.05;
createNode joint -n "lipCnr_R_TmpJnt" -p "HeadPos_TmpJnt";
	rename -uid "A822EBF2-4F60-657D-80DB-CA9C7279CC78";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.65096899999999969 0.046560611593623236 1.6061300000000012 ;
	setAttr ".s" -type "double3" 0.99999999999999889 1 1.0000000000000013 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -180 -43.874351634461839 -1.9593918297813898e-30 ;
	setAttr ".bps" -type "matrix" 0.8185523695232757 0.45266987781597179 0.35364106105754067 0
		 -0.57195586300494139 0.58516454486653979 0.57484688935872963 0 0.053277660629052009 -0.67280936166882299 0.73789501538409097 0
		 4.5812682868491317 -3.6850020464801649 42.372932534218236 1;
	setAttr ".radi" 0.05;
createNode transform -n "Mouth_Nrb" -p "TmpJnt_Grp";
	rename -uid "BE10FB75-49FF-A2E8-5CF6-809BBD45FAD6";
	setAttr ".ove" yes;
	setAttr ".rp" -type "double3" 2.8631541582058162e-12 -0.2063469371721407 0.82641704226911727 ;
	setAttr ".sp" -type "double3" 2.8631541582058162e-12 -0.20634693717213892 0.82641704226911727 ;
createNode nurbsSurface -n "Mouth_NrbShape" -p "Mouth_Nrb";
	rename -uid "F32339CB-427F-F4FF-4603-78AA7B9A60E1";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		3 3 0 0 no 
		9 0 0 0 0.875 1.75 2.625 3.5 3.5 3.5
		15 0 0 0 1 2 3 4 5 6 7 8 9 10 10 10
		
		91
		0.52909287175422426 -0.62133587198495022 0.14210926115594286
		0.53062066273865116 -0.62133587198495022 0.23653701526828047
		0.54313839151414034 -0.62133587198495022 0.47857997536900598
		0.54387217955423062 -0.62133587198495022 0.91932992198494223
		0.46220427805516073 -0.62133587198495022 1.3360639290590344
		0.26886775365561172 -0.62133587198495022 1.6296394085115784
		0.010681736501828899 -0.62133587198495022 1.7294328683128395
		-0.25087767642443909 -0.62133587198495022 1.6304704571772848
		-0.45188701146715299 -0.62133587198495022 1.3365403155759243
		-0.54372103356560153 -0.62133587198495022 0.91933652095125562
		-0.56124182288951707 -0.62133587198495022 0.47774318595199777
		-0.54240241248003529 -0.62133587198495022 0.23599243848845849
		-0.54307115060940347 -0.62133587198495022 0.14146331455104402
		0.55527336303296015 -0.56928306451500887 0.10824844984676361
		0.55687675193351682 -0.56928306451500887 0.20734866341754285
		0.57001388101948747 -0.56928306451500887 0.46136835566553935
		0.57078397824537419 -0.56928306451500887 0.92392742266385042
		0.48507499833246803 -0.56928306451500887 1.3612821971310876
		0.28217182606985325 -0.56928306451500887 1.6693843328472413
		0.011210288527727945 -0.56928306451500887 1.7741157571222126
		-0.26329156663239695 -0.56928306451500887 1.6702565033339103
		-0.47424721436235362 -0.56928306451500887 1.3617821561315937
		-0.57062535327470154 -0.56928306451500887 0.92393434815918973
		-0.58901310357386705 -0.56928306451500887 0.46049016036460461
		-0.56924148438544375 -0.56928306451500887 0.20677713997410863
		-0.5699433129110324 -0.56928306451500887 0.10757054061231716
		0.61855649606225616 -0.44892637371939692 0.039310790124530148
		0.62034261923361944 -0.44892637371939692 0.14970521014600016
		0.63497695445790336 -0.44892637371939692 0.43267489900263911
		0.63583481776161066 -0.44892637371939692 0.94795067982101255
		0.54035779720645993 -0.44892637371939692 1.4351496971717461
		0.31433025180213314 -0.44892637371939692 1.7783654719336082
		0.012487897408774509 -0.44892637371939692 1.8950328778277479
		-0.2932982558529057 -0.44892637371939692 1.779337041535392
		-0.52829599745483635 -0.44892637371939692 1.4357066352640739
		-0.63565811469566635 -0.44892637371939692 0.94795839459794173
		-0.65614147145782087 -0.44892637371939692 0.4316966179533957
		-0.63411652968882637 -0.44892637371939692 0.14906855158802909
		-0.63489834387716371 -0.44892637371939692 0.038555621249838856
		0.6714187662339629 -0.25526995218533699 -0.041969418120135049
		0.67335753273903309 -0.25526995218533699 0.077859386139154801
		0.68924252847264433 -0.25526995218533699 0.38501186224002815
		0.69017370537344391 -0.25526995218533699 0.94432347117125115
		0.58653715195762579 -0.25526995218533699 1.4731588568192331
		0.34119313465860585 -0.25526995218533699 1.8715265715341762
		0.013555121843007034 -0.25526995218533699 1.9981644554200559
		-0.31836372964666781 -0.25526995218533699 1.8725811721527021
		-0.57344454237539388 -0.25526995218533699 1.4737633912299137
		-0.6899819011405588 -0.25526995218533699 0.94433184525840508
		-0.71221577987779972 -0.25526995218533699 0.38394997660905555
		-0.6883085712023449 -0.25526995218533699 0.077168318296158028
		-0.68915719977725998 -0.25526995218533699 -0.042789124254290128
		0.70319329874435332 0.041711437960666231 -0.081034561865627103
		0.70522381633301501 0.034337330846986092 0.044223459543593011
		0.72186056095830275 0.015435571182431218 0.36529243511384268
		0.72283580530582192 -0.018983730083892425 0.94994539830571345
		0.61429470766008831 -0.051527564626665523 1.5027413180089071
		0.35733991651026081 -0.074453631774865414 1.8921679652501568
		0.014196610703989667 -0.08224676069905712 2.0245435728434051
		-0.33343012216748147 -0.074518530510784942 1.893270347833961
		-0.60058249736134717 -0.051564766879710788 1.5033732427337152
		-0.72263492404089957 -0.018984245414207734 0.94995415180696474
		-0.74592100914814441 0.015500918228442018 0.36418243744741607
		-0.72088240466206155 0.034379858253718254 0.043501080719833621
		-0.72177119412850799 0.041761881598757711 -0.081891407338933353
		0.70123117415358061 0.37166320387363783 -0.072914922520022435
		0.70325602597651349 0.35742477131804762 0.051328796374690538
		0.7198463489908471 0.32092795536233609 0.36979785007496979
		0.72081887210805917 0.25446880415447559 0.9497164660050228
		0.61258063735533286 0.19163093292558031 1.498036007388841
		0.35634282874122392 0.14736370764362761 1.8843091924959388
		0.014156997813747229 0.13231619899295133 2.0156128614609803
		-0.33249975004478677 0.1472383967063049 1.88540264831071
		-0.59890668832139315 0.19155910025991041 1.4986628149736045
		-0.72061855136335096 0.25446780911929956 0.94972514862300339
		-0.74383966116395783 0.32105413192757659 0.36869684084227017
		-0.71887092205012537 0.35750688615625315 0.050612267161592492
		-0.71975723152185178 0.371760603915126 -0.073764829511398372
		0.64017798541528514 0.70791270683995045 0.02294367663117497
		0.64202654208040166 0.68361523610968078 0.13428106902207615
		0.65717241687343353 0.62133448196277108 0.41966784198429263
		0.65806026657677552 0.50792387479606482 0.9393450302637889
		0.55924587037923268 0.40069287651197172 1.4307056276263239
		0.32531758802107136 0.32515214501647272 1.7768530994095606
		0.012924408774387485 0.29947400947902381 1.8945170605657315
		-0.30355042385477338 0.32493830554924941 1.7778329680113241
		-0.54676245340032958 0.40057029615877759 1.4312673229992505
		-0.65787738689905928 0.50792217679750173 0.939352810939171
		-0.67907673433137705 0.62154979859884207 0.41868120460690517
		-0.65628191616953357 0.6837553626910271 0.13363897222339685
		-0.65709105847948424 0.70807891717581306 0.022182057218648799
		
		;
	setAttr ".nufa" 4.5;
	setAttr ".nvfa" 4.5;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "13FCE6ED-4DAE-76A3-EFEC-93BD35C77B30";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode displayLayerManager -n "layerManager";
	rename -uid "5CF65BFA-4B18-C825-C75E-A499F2F4EB25";
	setAttr ".cdl" 3;
	setAttr -s 4 ".dli[1:3]"  1 2 3;
createNode displayLayer -n "defaultLayer";
	rename -uid "D1B9A8CE-4D42-E2DE-5195-17B1555B73FA";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "BAD9E485-4126-7DD6-A8B0-A0ACED34F962";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "9162BE4D-4287-2020-A2CE-97A034D2B460";
	setAttr ".g" yes;
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "3C769D10-43F9-2AAB-B23E-9780A0AC90C8";
	setAttr ".bsdt[0].bscd" -type "Int32Array" 0 ;
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "3EFC88E6-41BC-6EA4-526E-04B84EB29695";
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "C1E93915-4181-890A-8C7A-7B84749F21D4";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "C377EA00-4CC7-8DF9-4C12-088124E5F464";
	setAttr ".def" no;
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -330.95236780151544 -323.80951094248991 ;
	setAttr ".tgi[0].vh" -type "double2" 317.85713022663526 338.09522466054096 ;
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo1";
	rename -uid "ABA3B769-4E53-0DFC-AD46-81BD88006494";
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -229.76189563198733 -38.095236581469415 ;
	setAttr ".tgi[0].vh" -type "double2" 221.42856262979089 222.6190387729618 ;
createNode nodeGraphEditorInfo -n "MayaNodeEditorSavedTabsInfo";
	rename -uid "9559DA95-4465-67A0-86EC-50B34C1782E5";
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -888.09520280550544 -476.19045726836754 ;
	setAttr ".tgi[0].vh" -type "double2" 889.28567894867638 476.19045726836754 ;
	setAttr -s 2 ".tgi[0].ni";
	setAttr ".tgi[0].ni[0].x" -91.428573608398438;
	setAttr ".tgi[0].ni[0].y" 98.571426391601563;
	setAttr ".tgi[0].ni[0].nvs" 18304;
	setAttr ".tgi[0].ni[1].x" -91.428573608398438;
	setAttr ".tgi[0].ni[1].y" -31.428571701049805;
	setAttr ".tgi[0].ni[1].nvs" 18304;
select -ne :time1;
	setAttr -av -k on ".cch";
	setAttr -av -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 1;
	setAttr -av -k on ".unw" 1;
	setAttr -av -k on ".etw";
	setAttr -av -k on ".tps";
	setAttr -av -k on ".tms";
select -ne :hardwareRenderingGlobals;
	setAttr -av -k on ".ihi";
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".cons" no;
	setAttr -av ".ta" 0;
	setAttr -av ".tq";
	setAttr -av ".tmr" 1024;
	setAttr -av ".aoam";
	setAttr -av ".aora";
	setAttr -av ".hfd";
	setAttr -av ".hfs";
	setAttr -av ".hfe";
	setAttr -av ".hfcr";
	setAttr -av ".hfcg";
	setAttr -av ".hfcb";
	setAttr -av ".hfa";
	setAttr -av ".mbe";
	setAttr -av -k on ".mbsof";
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
select -ne :renderGlobalsList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
select -ne :defaultShaderList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
	setAttr -k on ".ihi";
select -ne :initialShadingGroup;
	setAttr -av -k on ".cch";
	setAttr -k on ".fzn";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".bbx";
	setAttr -k on ".vwm";
	setAttr -k on ".tpv";
	setAttr -k on ".uit";
	setAttr -s 3 ".dsm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
select -ne :initialParticleSE;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
select -ne :defaultResolution;
	setAttr -av -k on ".cch";
	setAttr -av -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -av -k on ".w";
	setAttr -av -k on ".h";
	setAttr -av -k on ".pa" 1;
	setAttr -av -k on ".al";
	setAttr -av -k on ".dar";
	setAttr -av -k on ".ldar";
	setAttr -av -k on ".dpi";
	setAttr -av -k on ".off";
	setAttr -av -k on ".fld";
	setAttr -av -k on ".zsl";
	setAttr -av -k on ".isu";
	setAttr -av -k on ".pdu";
select -ne :hardwareRenderGlobals;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -av -k off -cb on ".ctrs" 256;
	setAttr -av -k off -cb on ".btrs" 512;
	setAttr -av -k off -cb on ".fbfm";
	setAttr -av -k off -cb on ".ehql";
	setAttr -av -k off -cb on ".eams";
	setAttr -av -k off -cb on ".eeaa";
	setAttr -av -k off -cb on ".engm";
	setAttr -av -k off -cb on ".mes";
	setAttr -av -k off -cb on ".emb";
	setAttr -av -k off -cb on ".mbbf";
	setAttr -av -k off -cb on ".mbs";
	setAttr -av -k off -cb on ".trm";
	setAttr -av -k off -cb on ".tshc";
	setAttr -av -k off -cb on ".enpt";
	setAttr -av -k off -cb on ".clmt";
	setAttr -av -k off -cb on ".tcov";
	setAttr -av -k off -cb on ".lith";
	setAttr -av -k off -cb on ".sobc";
	setAttr -av -k off -cb on ".cuth";
	setAttr -av -k off -cb on ".hgcd";
	setAttr -av -k off -cb on ".hgci";
	setAttr -av -k off -cb on ".mgcs";
	setAttr -av -k off -cb on ".twa";
	setAttr -av -k off -cb on ".twz";
	setAttr -k on ".hwcc";
	setAttr -k on ".hwdp";
	setAttr -k on ".hwql";
	setAttr -k on ".hwfr";
	setAttr -k on ".soll";
	setAttr -k on ".sosl";
	setAttr -k on ".bswa";
	setAttr -k on ".shml";
	setAttr -k on ".hwel";
connectAttr "EyeLidSurface_R_NrbShapeOrig.ws" "EyeLidSurface_R_NrbShape.cr";
connectAttr "EyeLidSurface_L_NrbShapeOrig.ws" "EyeLidSurface_L_NrbShape.cr";
connectAttr "EyeTrgt_TmpJnt.s" "EyeTrgt_L_TmpJnt.is";
connectAttr "EyeTrgt_TmpJnt.s" "EyeTrgt_R_TmpJnt.is";
connectAttr "Eye_R_TmpJnt.s" "Iris_R_TmpJnt.is";
connectAttr "Iris_R_TmpJnt.s" "Pupil_R_TmpJnt.is";
connectAttr "Eye_R_TmpJnt.s" "EyeDot_R_TmpJnt.is";
connectAttr "Eye_R_TmpJnt.s" "EyeInner_R_TmpJnt.is";
connectAttr "Eye_R_TmpJnt.s" "EyeOuter_R_TmpJnt.is";
connectAttr "Eye_R_TmpJnt.s" "EyeLidUpr_R_TmpJnt.is";
connectAttr "EyeLidUpr_R_TmpJnt.s" "EyeLidUpr1_R_TmpJnt.is";
connectAttr "EyeLidUpr_R_TmpJnt.s" "EyeLidUpr2_R_TmpJnt.is";
connectAttr "EyeLidUpr_R_TmpJnt.s" "EyeLidUpr3_R_TmpJnt.is";
connectAttr "EyeLidUpr_R_TmpJnt.s" "EyeLidUpr4_R_TmpJnt.is";
connectAttr "EyeLidUpr_R_TmpJnt.s" "EyeLidUpr5_R_TmpJnt.is";
connectAttr "EyeLidUpr_R_TmpJnt.s" "EyeLidUpr6_R_TmpJnt.is";
connectAttr "EyeLidUpr_R_TmpJnt.s" "EyeLidUpr7_R_TmpJnt.is";
connectAttr "EyeLidUpr_R_TmpJnt.s" "EyeLidUpr8_R_TmpJnt.is";
connectAttr "EyeLidUpr_R_TmpJnt.s" "EyeLidUpr9_R_TmpJnt.is";
connectAttr "EyeLidUpr_R_TmpJnt.s" "EyeLidUpr10_R_TmpJnt.is";
connectAttr "Eye_R_TmpJnt.s" "EyeLidLwr_R_TmpJnt.is";
connectAttr "EyeLidLwr_R_TmpJnt.s" "EyeLidLwr1_R_TmpJnt.is";
connectAttr "EyeLidLwr_R_TmpJnt.s" "EyeLidLwr2_R_TmpJnt.is";
connectAttr "EyeLidLwr_R_TmpJnt.s" "EyeLidLwr3_R_TmpJnt.is";
connectAttr "EyeLidLwr_R_TmpJnt.s" "EyeLidLwr4_R_TmpJnt.is";
connectAttr "EyeLidLwr_R_TmpJnt.s" "EyeLidLwr5_R_TmpJnt.is";
connectAttr "EyeLidLwr_R_TmpJnt.s" "EyeLidLwr6_R_TmpJnt.is";
connectAttr "EyeLidLwr_R_TmpJnt.s" "EyeLidLwr7_R_TmpJnt.is";
connectAttr "EyeLidLwr_R_TmpJnt.s" "EyeLidLwr8_R_TmpJnt.is";
connectAttr "EyeLidLwr_R_TmpJnt.s" "EyeLidLwr9_R_TmpJnt.is";
connectAttr "EyeLidLwr_R_TmpJnt.s" "EyeLidLwr10_R_TmpJnt.is";
connectAttr "Eye_L_TmpJnt.s" "Iris_L_TmpJnt.is";
connectAttr "Iris_L_TmpJnt.s" "Pupil_L_TmpJnt.is";
connectAttr "Eye_L_TmpJnt.s" "EyeDot_L_TmpJnt.is";
connectAttr "Eye_L_TmpJnt.s" "EyeInner_L_TmpJnt.is";
connectAttr "Eye_L_TmpJnt.s" "EyeOuter_L_TmpJnt.is";
connectAttr "Eye_L_TmpJnt.s" "EyeLidUpr_L_TmpJnt.is";
connectAttr "EyeLidUpr_L_TmpJnt.s" "EyeLidUpr1_L_TmpJnt.is";
connectAttr "EyeLidUpr_L_TmpJnt.s" "EyeLidUpr2_L_TmpJnt.is";
connectAttr "EyeLidUpr_L_TmpJnt.s" "EyeLidUpr3_L_TmpJnt.is";
connectAttr "EyeLidUpr_L_TmpJnt.s" "EyeLidUpr4_L_TmpJnt.is";
connectAttr "EyeLidUpr_L_TmpJnt.s" "EyeLidUpr5_L_TmpJnt.is";
connectAttr "EyeLidUpr_L_TmpJnt.s" "EyeLidUpr6_L_TmpJnt.is";
connectAttr "EyeLidUpr_L_TmpJnt.s" "EyeLidUpr7_L_TmpJnt.is";
connectAttr "EyeLidUpr_L_TmpJnt.s" "EyeLidUpr8_L_TmpJnt.is";
connectAttr "EyeLidUpr_L_TmpJnt.s" "EyeLidUpr9_L_TmpJnt.is";
connectAttr "EyeLidUpr_L_TmpJnt.s" "EyeLidUpr10_L_TmpJnt.is";
connectAttr "Eye_L_TmpJnt.s" "EyeLidLwr_L_TmpJnt.is";
connectAttr "EyeLidLwr_L_TmpJnt.s" "EyeLidLwr1_L_TmpJnt.is";
connectAttr "EyeLidLwr_L_TmpJnt.s" "EyeLidLwr2_L_TmpJnt.is";
connectAttr "EyeLidLwr_L_TmpJnt.s" "EyeLidLwr3_L_TmpJnt.is";
connectAttr "EyeLidLwr_L_TmpJnt.s" "EyeLidLwr4_L_TmpJnt.is";
connectAttr "EyeLidLwr_L_TmpJnt.s" "EyeLidLwr5_L_TmpJnt.is";
connectAttr "EyeLidLwr_L_TmpJnt.s" "EyeLidLwr6_L_TmpJnt.is";
connectAttr "EyeLidLwr_L_TmpJnt.s" "EyeLidLwr7_L_TmpJnt.is";
connectAttr "EyeLidLwr_L_TmpJnt.s" "EyeLidLwr8_L_TmpJnt.is";
connectAttr "EyeLidLwr_L_TmpJnt.s" "EyeLidLwr9_L_TmpJnt.is";
connectAttr "EyeLidLwr_L_TmpJnt.s" "EyeLidLwr10_L_TmpJnt.is";
connectAttr "EyeBrowMid_L_TmpJnt.s" "EyeBrowInner_L_TmpJnt.is";
connectAttr "EyeBrowMid_L_TmpJnt.s" "EyeBrowOuter_L_TmpJnt.is";
connectAttr "EyeBrowMid_R_TmpJnt.s" "EyeBrowInner_R_TmpJnt.is";
connectAttr "EyeBrowMid_R_TmpJnt.s" "EyeBrowOuter_R_TmpJnt.is";
connectAttr "HeadPos_TmpJnt.s" "cheekU_L_TmpJnt.is";
connectAttr "HeadPos_TmpJnt.s" "cheekD_L_TmpJnt.is";
connectAttr "HeadPos_TmpJnt.s" "cheekU_R_TmpJnt.is";
connectAttr "HeadPos_TmpJnt.s" "cheekD_R_TmpJnt.is";
connectAttr "HeadPos_TmpJnt.s" "tongue01_TmpJnt.is";
connectAttr "tongue01_TmpJnt.s" "tongue02_TmpJnt.is";
connectAttr "tongue02_TmpJnt.s" "tongue03_TmpJnt.is";
connectAttr "tongue03_TmpJnt.s" "tongue04_TmpJnt.is";
connectAttr "tongue04_TmpJnt.s" "tongue05_TmpJnt.is";
connectAttr "tongue05_TmpJnt.s" "tongue06_TmpJnt.is";
connectAttr "HeadPos_TmpJnt.s" "teethMainUp_TmpJnt.is";
connectAttr "teethMainUp_TmpJnt.s" "teethUp_R_TmpJnt.is";
connectAttr "teethMainUp_TmpJnt.s" "teethUp_L_TmpJnt.is";
connectAttr "teethMainUp_TmpJnt.s" "teethUp_TmpJnt.is";
connectAttr "HeadPos_TmpJnt.s" "teethMainDn_TmpJnt.is";
connectAttr "teethMainDn_TmpJnt.s" "teethDn_L_TmpJnt.is";
connectAttr "teethMainDn_TmpJnt.s" "teethDn_TmpJnt.is";
connectAttr "teethMainDn_TmpJnt.s" "teethDn_R_TmpJnt.is";
connectAttr "HeadPos_TmpJnt.s" "lipUp_TmpJnt.is";
connectAttr "HeadPos_TmpJnt.s" "lipDn_TmpJnt.is";
connectAttr "HeadPos_TmpJnt.s" "lipUp1_L_TmpJnt.is";
connectAttr "HeadPos_TmpJnt.s" "lipUp2_L_TmpJnt.is";
connectAttr "HeadPos_TmpJnt.s" "lipDn1_L_TmpJnt.is";
connectAttr "HeadPos_TmpJnt.s" "lipDn2_L_TmpJnt.is";
connectAttr "HeadPos_TmpJnt.s" "lipCnr_L_TmpJnt.is";
connectAttr "HeadPos_TmpJnt.s" "lipMainUp_TmpJnt.is";
connectAttr "HeadPos_TmpJnt.s" "lipMainDn_TmpJnt.is";
connectAttr "HeadPos_TmpJnt.s" "jawDn1_TmpJnt.is";
connectAttr "jawDn1_TmpJnt.s" "jawDn2_TmpJnt.is";
connectAttr "jawDn2_TmpJnt.s" "jawDn3_TmpJnt.is";
connectAttr "HeadPos_TmpJnt.s" "nose_TmpJnt.is";
connectAttr "nose_TmpJnt.s" "nose_L_TmpJnt.is";
connectAttr "nose_TmpJnt.s" "nose_R_TmpJnt.is";
connectAttr "HeadPos_TmpJnt.s" "lipUp1_R_TmpJnt.is";
connectAttr "HeadPos_TmpJnt.s" "lipDn1_R_TmpJnt.is";
connectAttr "HeadPos_TmpJnt.s" "lipUp2_R_TmpJnt.is";
connectAttr "HeadPos_TmpJnt.s" "lipDn2_R_TmpJnt.is";
connectAttr "HeadPos_TmpJnt.s" "lipCnr_R_TmpJnt.is";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "Mouth_Nrb.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[0].dn";
connectAttr "Mouth_NrbShape.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[1].dn";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "EyeLidSurface_R_NrbShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "EyeLidSurface_L_NrbShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "Mouth_NrbShape.iog" ":initialShadingGroup.dsm" -na;
// End of facialDirTmp.ma
