//Maya ASCII 2020 scene
//Name: EyeUiCtrlTmp.ma
//Last modified: Wed, Feb 15, 2023 12:18:30 PM
//Codeset: 1252
requires maya "2020";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2020";
fileInfo "version" "2020";
fileInfo "cutIdentifier" "202011110415-b1e20b88e2";
fileInfo "osv" "Microsoft Windows 10 Technical Preview  (Build 19041)\n";
fileInfo "UUID" "D40D9CC3-4351-85B5-1213-FFBF5502CE40";
createNode transform -s -n "persp";
	rename -uid "BD5859C6-453F-9F1C-C5DA-EF92A6C2464C";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 4.3055665338183395 8.64020380671273 33.202840662609105 ;
	setAttr ".r" -type "double3" -14.738352729629177 -356.19999999986942 1.9922266493730683e-16 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "7C339057-427E-4623-02DC-4E8FCF97D7BC";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 28.471303629905371;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 11.402600335726369 12.43179741768963 0.91861792332058023 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "88A7F482-4B1C-85F3-038F-34A15B9A2130";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0.16097377055327464 1000.2647399433143 0.14300549164587889 ;
	setAttr ".r" -type "double3" -90 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "7DA05755-4739-4EE2-76E9-E0B64CA9D5A2";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 986.55059905128553;
	setAttr ".ow" 4.7810353296605834;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".tp" -type "double3" -1.8233057539910078 13.714140892028809 -0.087710380554199219 ;
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "64B23769-4C19-E696-DC27-F689BF05D164";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 12.138605364464695 12.142756424874745 1000.8925566837374 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "21E110A1-4141-711D-32B3-B58A2AFF94AB";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 999.9739387604169;
	setAttr ".ow" 28.869281511253266;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".tp" -type "double3" 11.436481862086957 13.690233677007193 0.91861792332059622 ;
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "A227667D-415C-2850-8E21-EC809101E7D0";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1717251885491 12.702306163155843 0.66166559741010966 ;
	setAttr ".r" -type "double3" 0 90 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "E712D94B-4A83-8FEB-AB2F-7CA2ED8E4B26";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 988.80366412287526;
	setAttr ".ow" 33.483859083627273;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".tp" -type "double3" 11.368061065673828 12.382330894470215 0.21605258352814971 ;
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "EyeUiRigCtrl_Grp";
	rename -uid "C1553AF8-4E9E-C568-7B91-F39675AEE6B0";
createNode transform -n "EyeMaskUpUiRigCtrl_L_Grp" -p "EyeUiRigCtrl_Grp";
	rename -uid "0181A0A0-467D-C913-DC86-DBA399A1B952";
	setAttr -av ".v";
	setAttr ".rp" -type "double3" 1.4490316752470065 1.2584362593175626 -0.0085445230678260486 ;
	setAttr ".sp" -type "double3" 1.4490316752470065 1.2584362593175626 -0.0085445230678260486 ;
createNode transform -n "EyeMaskUpUiRig_L_Ctrl" -p "EyeMaskUpUiRigCtrl_L_Grp";
	rename -uid "55D2741D-41B4-C997-7010-E598EF07AC07";
	setAttr -l on ".ro";
	setAttr ".rp" -type "double3" 1.4490316752470065 1.2584362593175626 -0.0085445230678153905 ;
	setAttr ".sp" -type "double3" 1.4490316752470065 1.2584362593175626 -0.0085445230678153905 ;
	setAttr ".mntl" -type "double3" 0 -2.25 -1 ;
	setAttr ".mxtl" -type "double3" 0 0 1 ;
	setAttr ".mtxe" yes;
	setAttr ".mtye" yes;
	setAttr ".xtxe" yes;
	setAttr ".xtye" yes;
	setAttr ".mnrl" -type "double3" 0 0 -45 ;
	setAttr ".mxrl" -type "double3" 0 45 45 ;
	setAttr ".mnsl" -type "double3" 0 0 -1 ;
	setAttr ".mxsl" -type "double3" 2 2 1 ;
	setAttr ".msxe" yes;
	setAttr ".msye" yes;
	setAttr ".xsxe" yes;
	setAttr ".xsye" yes;
createNode nurbsCurve -n "EyeMaskUpUiRig_L_CtrlShape" -p "EyeMaskUpUiRig_L_Ctrl";
	rename -uid "523D152F-46CC-56A5-F582-BD8883A189AE";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		2.5141783250939369 1.3862011090965893 -0.0085445230678153905
		1.4490316752470136 1.3931376789692678 -0.0085445230678153905
		0.38388502540016844 1.3862011090965893 -0.0085445230678153905
		0.32845062545847981 1.2419635374898519 -0.0085445230678153905
		0.38388502540016844 1.1237348396658717 -0.0085445230678153905
		1.4490316752470136 1.1472396245637952 -0.0085445230678153905
		2.5141783250939369 1.1237348396658717 -0.0085445230678153905
		2.5696127250355332 1.2419635374898519 -0.0085445230678153905
		2.5141783250939369 1.3862011090965893 -0.0085445230678153905
		1.4490316752470136 1.3931376789692678 -0.0085445230678153905
		0.38388502540016844 1.3862011090965893 -0.0085445230678153905
		;
createNode transform -n "EyeMaskUpUiRigCtrl_R_Grp" -p "EyeUiRigCtrl_Grp";
	rename -uid "7AE4A4D6-44A3-90A1-69FA-04BDF2E9CA91";
	setAttr -av ".v";
	setAttr ".r" -type "double3" 0 179.99999999999994 0 ;
	setAttr ".s" -type "double3" 1 1 -1 ;
	setAttr ".rp" -type "double3" 1.4622256395641671 1.2584362593175626 -0.0085445230678488429 ;
	setAttr ".rpt" -type "double3" -2.9244512791283341 0 0.017089046135696208 ;
	setAttr ".sp" -type "double3" 1.4622256395641671 1.2584362593175626 0.0085445230678488429 ;
	setAttr ".spt" -type "double3" 0 0 -0.017089046135697686 ;
createNode transform -n "EyeMaskUpUiRig_R_Ctrl" -p "EyeMaskUpUiRigCtrl_R_Grp";
	rename -uid "9373CCF4-4CE8-3F73-5A7C-3996CD1B167A";
	setAttr -l on ".ro";
	setAttr ".rp" -type "double3" 1.4622256395641671 1.2584362593175626 0.0085445230678595011 ;
	setAttr ".sp" -type "double3" 1.4622256395641671 1.2584362593175626 0.0085445230678595011 ;
	setAttr ".mntl" -type "double3" 0 -2.25 -1 ;
	setAttr ".mxtl" -type "double3" 0 0 1 ;
	setAttr ".mtxe" yes;
	setAttr ".mtye" yes;
	setAttr ".xtxe" yes;
	setAttr ".xtye" yes;
	setAttr ".mnrl" -type "double3" 0 0 -45 ;
	setAttr ".mxrl" -type "double3" 0 45 45 ;
	setAttr ".mnsl" -type "double3" 0 0 -1 ;
	setAttr ".mxsl" -type "double3" 2 2 1 ;
	setAttr ".msxe" yes;
	setAttr ".msye" yes;
	setAttr ".xsxe" yes;
	setAttr ".xsye" yes;
createNode nurbsCurve -n "EyeMaskUpUiRig_R_CtrlShape" -p "EyeMaskUpUiRig_R_Ctrl";
	rename -uid "55CCB14D-4A6B-59D6-590F-14AF2E9FB8D6";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		2.5273722894110975 1.3862011090965893 0.0085445230678615758
		1.4622256395641742 1.3931376789692678 0.0085445230678615758
		0.39707898971732902 1.3862011090965893 0.0085445230678615758
		0.34164458977564038 1.2419635374898519 0.0085445230678615758
		0.39707898971732902 1.1237348396658717 0.0085445230678615758
		1.4622256395641742 1.1472396245637952 0.0085445230678615758
		2.5273722894110975 1.1237348396658717 0.0085445230678615758
		2.5828066893526938 1.2419635374898519 0.0085445230678615758
		2.5273722894110975 1.3862011090965893 0.0085445230678615758
		1.4622256395641742 1.3931376789692678 0.0085445230678615758
		0.39707898971732902 1.3862011090965893 0.0085445230678615758
		;
createNode transform -n "EyeMaskDnUiRigCtrl_L_Grp" -p "EyeUiRigCtrl_Grp";
	rename -uid "3BDD7461-46E0-B433-7B4A-44A7E403A198";
	setAttr -av ".v";
	setAttr ".r" -type "double3" 0 0 180 ;
	setAttr ".rp" -type "double3" -1.4490316752470138 1.2584362593174914 -0.0085445230678580231 ;
	setAttr ".rpt" -type "double3" 2.8980633504940272 -2.5168725186349827 0 ;
	setAttr ".sp" -type "double3" -1.4490316752470138 1.2584362593174914 -0.0085445230678580231 ;
createNode transform -n "EyeMaskDnUiRig_L_Ctrl" -p "EyeMaskDnUiRigCtrl_L_Grp";
	rename -uid "87F6DB25-49B6-BACF-8671-DBB2DA415BB7";
	setAttr -l on ".ro";
	setAttr ".rp" -type "double3" -1.449031675247014 1.2584362593174916 -0.0085445230678615758 ;
	setAttr ".sp" -type "double3" -1.449031675247014 1.2584362593174916 -0.0085445230678615758 ;
	setAttr ".mntl" -type "double3" 0 -2.25 -1 ;
	setAttr ".mxtl" -type "double3" 0 0 1 ;
	setAttr ".mtxe" yes;
	setAttr ".mtye" yes;
	setAttr ".xtxe" yes;
	setAttr ".xtye" yes;
	setAttr ".mnrl" -type "double3" 0 0 -45 ;
	setAttr ".mxrl" -type "double3" 0 45 45 ;
	setAttr ".mnsl" -type "double3" 0 0 -1 ;
	setAttr ".mxsl" -type "double3" 2 2 1 ;
	setAttr ".msxe" yes;
	setAttr ".msye" yes;
	setAttr ".xsxe" yes;
	setAttr ".xsye" yes;
createNode nurbsCurve -n "EyeMaskDnUiRig_L_CtrlShape" -p "EyeMaskDnUiRig_L_Ctrl";
	rename -uid "69951367-42DA-7ADA-1435-DAABC8221516";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.38388502540008318 1.3862011090965183 -0.0085445230678615758
		-1.4490316752470065 1.3931376789691967 -0.0085445230678615758
		-2.5141783250938516 1.3862011090965183 -0.0085445230678615758
		-2.5696127250355403 1.2419635374897808 -0.0085445230678615758
		-2.5141783250938516 1.1237348396658007 -0.0085445230678615758
		-1.4490316752470065 1.1472396245637242 -0.0085445230678615758
		-0.38388502540008318 1.1237348396658007 -0.0085445230678615758
		-0.32845062545848691 1.2419635374897808 -0.0085445230678615758
		-0.38388502540008318 1.3862011090965183 -0.0085445230678615758
		-1.4490316752470065 1.3931376789691967 -0.0085445230678615758
		-2.5141783250938516 1.3862011090965183 -0.0085445230678615758
		;
createNode transform -n "EyeMaskDnUiRigCtrl_R_Grp" -p "EyeUiRigCtrl_Grp";
	rename -uid "74C0F782-4A30-700F-221D-80A2BFB8D7D5";
	setAttr -av ".v";
	setAttr ".r" -type "double3" 180 0 0 ;
	setAttr ".s" -type "double3" 1 1 -1 ;
	setAttr ".rp" -type "double3" -1.4622256395641813 1.2584362593175626 -0.0085445230678401051 ;
	setAttr ".rpt" -type "double3" 0 -2.5168725186351253 0.017089046135680363 ;
	setAttr ".sp" -type "double3" -1.4622256395641813 1.2584362593175626 0.0085445230678401051 ;
	setAttr ".spt" -type "double3" 0 0 -0.01708904613568021 ;
createNode transform -n "EyeMaskDnUiRig_R_Ctrl" -p "EyeMaskDnUiRigCtrl_R_Grp";
	rename -uid "955AE26C-49A6-4CC4-DB82-CCBCA518438B";
	setAttr -l on ".ro";
	setAttr ".rp" -type "double3" -1.4622256395641813 1.2584362593175626 0.0085445230678365541 ;
	setAttr ".sp" -type "double3" -1.4622256395641813 1.2584362593175626 0.0085445230678365541 ;
	setAttr ".mntl" -type "double3" 0 -2.25 -1 ;
	setAttr ".mxtl" -type "double3" 0 0 1 ;
	setAttr ".mtxe" yes;
	setAttr ".mtye" yes;
	setAttr ".xtxe" yes;
	setAttr ".xtye" yes;
	setAttr ".mnrl" -type "double3" 0 0 -45 ;
	setAttr ".mxrl" -type "double3" 0 45 45 ;
	setAttr ".mnsl" -type "double3" 0 0 -1 ;
	setAttr ".mxsl" -type "double3" 2 2 1 ;
	setAttr ".msxe" yes;
	setAttr ".msye" yes;
	setAttr ".xsxe" yes;
	setAttr ".xsye" yes;
createNode nurbsCurve -n "EyeMaskDnUiRig_R_CtrlShape" -p "EyeMaskDnUiRig_R_Ctrl";
	rename -uid "EAF3C978-4678-19FF-713F-F1957D1933C6";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.39707898971725086 1.3862011090965893 0.0085445230678367068
		-1.4622256395641742 1.3931376789692678 0.0085445230678367068
		-2.5273722894110193 1.3862011090965893 0.0085445230678367068
		-2.582806689352708 1.2419635374898519 0.0085445230678367068
		-2.5273722894110193 1.1237348396658717 0.0085445230678367068
		-1.4622256395641742 1.1472396245637952 0.0085445230678367068
		-0.39707898971725086 1.1237348396658717 0.0085445230678367068
		-0.34164458977565459 1.2419635374898519 0.0085445230678367068
		-0.39707898971725086 1.3862011090965893 0.0085445230678367068
		-1.4622256395641742 1.3931376789692678 0.0085445230678367068
		-2.5273722894110193 1.3862011090965893 0.0085445230678367068
		;
createNode transform -n "EyeBallUiRigCtrl_L_Grp" -p "EyeUiRigCtrl_Grp";
	rename -uid "7A732DE5-4BA5-38CE-BE58-8990C0790ADD";
	setAttr -av ".v";
	setAttr ".rp" -type "double3" 1.42525618255506 -0.0012722910087461514 -0.0085445230658933724 ;
	setAttr ".sp" -type "double3" 1.42525618255506 -0.0012722910087461514 -0.0085445230658933724 ;
createNode transform -n "EyeUiRig_L_Ctrl" -p "EyeBallUiRigCtrl_L_Grp";
	rename -uid "9D2CBE8B-4E16-6E3E-241F-8AAA1C1F0FA4";
	setAttr ".ove" yes;
	setAttr -l on ".ro";
	setAttr ".rp" -type "double3" 1.42525618255506 -0.0012722910087532568 -0.0085445230658933724 ;
	setAttr ".sp" -type "double3" 1.42525618255506 -0.0012722910087532568 -0.0085445230658933724 ;
createNode nurbsCurve -n "EyeUiRig_L_CtrlShape" -p "EyeUiRig_L_Ctrl";
	rename -uid "D732DF3E-47F5-4989-0AD6-ADA09D3E6C35";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		2.2454650145127744 -0.82148112296645337 -0.0085445230658933724
		1.4252561825550885 -1.1612227451414938 -0.0085445230658933724
		0.60504735059739545 -0.82148112296645337 -0.0085445230658933724
		0.26530572842231948 -0.0012722910087958894 -0.0085445230658933724
		0.60504735059739545 0.81893654094889001 -0.0085445230658933724
		1.4252561825550885 1.1586781631240015 -0.0085445230658933724
		2.2454650145127744 0.81893654094889001 -0.0085445230658933724
		2.5852066366878006 -0.0012722910087958894 -0.0085445230658933724
		2.2454650145127744 -0.82148112296645337 -0.0085445230658933724
		1.4252561825550885 -1.1612227451414938 -0.0085445230658933724
		0.60504735059739545 -0.82148112296645337 -0.0085445230658933724
		;
createNode transform -n "EyeUiRigCtrl_L_Grp" -p "EyeUiRig_L_Ctrl";
	rename -uid "ABD3F7F7-45DE-E15A-74EB-248CF3F77FBA";
	setAttr ".rp" -type "double3" 0.99655891095305549 -0.0023468392523113835 -0.38835019729969744 ;
	setAttr ".sp" -type "double3" 0.99655891095305549 -0.0023468392523113835 -0.38835019729969744 ;
createNode transform -n "IrisUiRig_L_Ctrl" -p "EyeUiRigCtrl_L_Grp";
	rename -uid "FDEC0CEA-4A70-33CA-DF2C-AAB8A0210115";
	setAttr -l on ".ro";
	setAttr ".rp" -type "double3" 1.4266386008512342 -0.0023468392523398052 -0.0085445230658933724 ;
	setAttr ".sp" -type "double3" 1.4266386008512342 -0.0023468392523398052 -0.0085445230658933724 ;
	setAttr ".mntl" -type "double3" -2 -5 -1 ;
	setAttr ".mxtl" -type "double3" 10 5 1 ;
	setAttr ".mtxe" yes;
	setAttr ".mtye" yes;
	setAttr ".xtxe" yes;
	setAttr ".xtye" yes;
	setAttr ".mnsl" -type "double3" 0 0 -1 ;
	setAttr ".mxsl" -type "double3" 2 2 1 ;
	setAttr ".msxe" yes;
	setAttr ".msye" yes;
	setAttr ".xsxe" yes;
	setAttr ".xsye" yes;
createNode nurbsCurve -n "IrisUiRig_L_CtrlShape" -p "IrisUiRig_L_Ctrl";
	rename -uid "0637FFFD-41B0-9607-7761-C2B6581578C6";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.8881809523664472 0.45919551226286615 -0.0085445230658933724
		1.4266386008512342 0.65037261387004719 -0.0085445230658933724
		0.96509624933602112 0.45919551226286615 -0.0085445230658933724
		0.77391914772885428 -0.0023468392523398052 -0.0085445230658933724
		0.96509624933602112 -0.46388919076755997 -0.0085445230658933724
		1.4266386008512342 -0.6550662923747268 -0.0085445230658933724
		1.8881809523664472 -0.46388919076755997 -0.0085445230658933724
		2.0793580539736212 -0.0023468392523398052 -0.0085445230658933724
		1.8881809523664472 0.45919551226286615 -0.0085445230658933724
		1.4266386008512342 0.65037261387004719 -0.0085445230658933724
		0.96509624933602112 0.45919551226286615 -0.0085445230658933724
		;
createNode transform -n "SpecularUiRig_L_Ctrl" -p "IrisUiRig_L_Ctrl";
	rename -uid "AF31CAD6-4E0A-6A1E-CE2E-3AA8123B0D05";
	setAttr -l on ".ro";
	setAttr ".rp" -type "double3" 1.4266386008512342 -0.0023468392523398052 -0.0085445230658933724 ;
	setAttr ".sp" -type "double3" 1.4266386008512342 -0.0023468392523398052 -0.0085445230658933724 ;
	setAttr ".mntl" -type "double3" -10 -10 -1 ;
	setAttr ".mxtl" -type "double3" 10 5 1 ;
	setAttr ".mtxe" yes;
	setAttr ".mtye" yes;
	setAttr ".xtxe" yes;
	setAttr ".xtye" yes;
	setAttr ".mnsl" -type "double3" 0.5 0.5 -1 ;
	setAttr ".mxsl" -type "double3" 2 2 1 ;
	setAttr ".msxe" yes;
	setAttr ".msye" yes;
	setAttr ".xsxe" yes;
	setAttr ".xsye" yes;
createNode nurbsCurve -n "SpecularUiRig_L_CtrlShape" -p "SpecularUiRig_L_Ctrl";
	rename -uid "E8F62CC3-4E41-88EC-0035-4799C58F882E";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 16;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.6574097766088443 0.22842433650525606 -0.0085445230658933724
		1.4266386008512342 0.32401288730883948 -0.0085445230658933724
		1.1958674250936383 0.22842433650525606 -0.0085445230658933724
		1.1002788742900407 -0.0023468392523398052 -0.0085445230658933724
		1.1958674250936383 -0.23311801500994989 -0.0085445230658933724
		1.4266386008512342 -0.3287065658135333 -0.0085445230658933724
		1.6574097766088443 -0.23311801500994989 -0.0085445230658933724
		1.7529983274124206 -0.0023468392523398052 -0.0085445230658933724
		1.6574097766088443 0.22842433650525606 -0.0085445230658933724
		1.4266386008512342 0.32401288730883948 -0.0085445230658933724
		1.1958674250936383 0.22842433650525606 -0.0085445230658933724
		;
createNode transform -n "BlinkUiRig_L_Ctrl" -p "EyeUiRigCtrl_L_Grp";
	rename -uid "6126FEE2-4608-44C4-D16D-228FA9D75F10";
	setAttr -l on ".ro";
	setAttr ".rp" -type "double3" 1.4266386008512342 0.70163813684460763 -0.0085445230658933724 ;
	setAttr ".sp" -type "double3" 1.4266386008512342 0.70163813684460763 -0.0085445230658933724 ;
	setAttr ".mntl" -type "double3" -2 -5 -1 ;
	setAttr ".mxtl" -type "double3" 10 5 1 ;
	setAttr ".mtxe" yes;
	setAttr ".mtye" yes;
	setAttr ".xtxe" yes;
	setAttr ".xtye" yes;
	setAttr ".mnsl" -type "double3" 0 0 -1 ;
	setAttr ".mxsl" -type "double3" 2 2 1 ;
	setAttr ".msxe" yes;
	setAttr ".msye" yes;
	setAttr ".xsxe" yes;
	setAttr ".xsye" yes;
createNode nurbsCurve -n "BlinkUiRig_L_CtrlShape" -p "BlinkUiRig_L_Ctrl";
	rename -uid "52927D68-43A2-3BCF-5F87-9A911D8643AF";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.8881809523664472 0.83682076179862008 -0.0085445230658933724
		1.4266386008512342 1.0279978634058011 -0.0085445230658933724
		0.96509624933602112 0.83682076179862008 -0.0085445230658933724
		0.77391914772885428 0.37527841028341413 -0.0085445230658933724
		0.96509624933602112 0.52398240451810807 -0.0085445230658933724
		1.4266386008512342 0.73183183505160798 -0.0085445230658933724
		1.8881809523664472 0.52398240451810807 -0.0085445230658933724
		2.0793580539736212 0.37527841028341413 -0.0085445230658933724
		1.8881809523664472 0.83682076179862008 -0.0085445230658933724
		1.4266386008512342 1.0279978634058011 -0.0085445230658933724
		0.96509624933602112 0.83682076179862008 -0.0085445230658933724
		;
createNode transform -n "EyeBallUiRigCtrl_R_Grp" -p "EyeUiRigCtrl_Grp";
	rename -uid "02E20067-43AC-5797-4DCC-B09C04D9A085";
	setAttr -av ".v";
	setAttr ".r" -type "double3" 0 179.99999999999994 0 ;
	setAttr ".s" -type "double3" 1 1 -1 ;
	setAttr ".rp" -type "double3" 1.4247438174449425 -0.0012722910087532568 0.0085445230659416706 ;
	setAttr ".rpt" -type "double3" -2.849487634889885 0 -0.017089046131884781 ;
	setAttr ".sp" -type "double3" 1.4247438174449425 -0.0012722910087532568 -0.0085445230659416706 ;
	setAttr ".spt" -type "double3" 0 0 0.017089046131883341 ;
createNode transform -n "EyeUiRig_R_Ctrl" -p "EyeBallUiRigCtrl_R_Grp";
	rename -uid "A9013535-414E-BBD9-A8EF-4AB76644B862";
	setAttr ".ove" yes;
	setAttr -l on ".ro";
	setAttr ".rp" -type "double3" 1.4247438174449425 -0.0012722910087532568 -0.0085445230659416706 ;
	setAttr ".sp" -type "double3" 1.4247438174449425 -0.0012722910087532568 -0.0085445230659416706 ;
createNode nurbsCurve -n "EyeUiRig_R_CtrlShape" -p "EyeUiRig_R_Ctrl";
	rename -uid "328DAD8D-437B-7F26-9A26-A3BACAE96CB0";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		2.2453149462464141 0.8192988377927648 -0.0085445230659431104
		1.4247438174449247 1.1591905282341131 -0.0085445230659431104
		0.60417268864347062 0.8192988377927648 -0.0085445230659431104
		0.26428099820208684 -0.0012722910086822026 -0.0085445230659431104
		0.60417268864347062 -0.82184341981017184 -0.0085445230659431104
		1.4247438174449247 -1.1617351102516196 -0.0085445230659431104
		2.2453149462464141 -0.82184341981017184 -0.0085445230659431104
		2.5852066366877979 -0.0012722910086822026 -0.0085445230659431104
		2.2453149462464141 0.8192988377927648 -0.0085445230659431104
		1.4247438174449247 1.1591905282341131 -0.0085445230659431104
		0.60417268864347062 0.8192988377927648 -0.0085445230659431104
		;
createNode transform -n "EyeUiRigCtrl_R_Grp" -p "EyeUiRig_R_Ctrl";
	rename -uid "001340B3-4C16-61A8-E5C9-C0AF6D3672F1";
	setAttr ".rp" -type "double3" 1.7262668959278931 -0.00091641141263210102 -0.37710864015338613 ;
	setAttr ".sp" -type "double3" 1.7262668959278931 -0.00091641141263210102 -0.37710864015338613 ;
createNode transform -n "IrisUiRig_R_Ctrl" -p "EyeUiRigCtrl_R_Grp";
	rename -uid "938D8822-48B4-6138-DFDE-7FA5F4213833";
	setAttr -l on ".ro";
	setAttr ".rp" -type "double3" 1.4220492395678477 -0.00089962614191563262 -0.008544523065941674 ;
	setAttr ".sp" -type "double3" 1.4220492395678477 -0.00089962614191563262 -0.008544523065941674 ;
	setAttr ".mntl" -type "double3" -10 -5 -1 ;
	setAttr ".mxtl" -type "double3" 2 5 1 ;
	setAttr ".mtxe" yes;
	setAttr ".mtye" yes;
	setAttr ".xtxe" yes;
	setAttr ".xtye" yes;
	setAttr ".mnsl" -type "double3" 0.1 0.1 -1 ;
	setAttr ".mxsl" -type "double3" 2 2 1 ;
	setAttr ".msxe" yes;
	setAttr ".msye" yes;
	setAttr ".xsxe" yes;
	setAttr ".xsye" yes;
createNode nurbsCurve -n "IrisUiRig_R_CtrlShape" -p "IrisUiRig_R_Ctrl";
	rename -uid "300D313E-411A-7831-907B-228B508BA9B6";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.9053605079849234 0.48241164227513877 -0.0085445230659431104
		1.4220492395678193 0.68260572450121515 -0.0085445230659431104
		0.93873797115079327 0.48241164227513877 -0.0085445230659431104
		0.73854388892470269 -0.00089962614191563262 -0.0085445230659431104
		0.93873797115079327 -0.48421089455892741 -0.0085445230659431104
		1.4220492395678193 -0.6844049767850322 -0.0085445230659431104
		1.9053605079849234 -0.48421089455892741 -0.0085445230659431104
		2.1055545902109856 -0.00089962614191563262 -0.0085445230659431104
		1.9053605079849234 0.48241164227513877 -0.0085445230659431104
		1.4220492395678193 0.68260572450121515 -0.0085445230659431104
		0.93873797115079327 0.48241164227513877 -0.0085445230659431104
		;
createNode transform -n "SpecularUiRig_R_Ctrl" -p "IrisUiRig_R_Ctrl";
	rename -uid "D4C79CDE-4C65-BBFF-FA16-19B4387F3760";
	setAttr -l on ".ro";
	setAttr ".rp" -type "double3" 1.4220492395678477 -0.00089962614188721091 -0.008544523065941674 ;
	setAttr ".sp" -type "double3" 1.4220492395678477 -0.00089962614188721091 -0.008544523065941674 ;
	setAttr ".mntl" -type "double3" -10 -10 -1 ;
	setAttr ".mxtl" -type "double3" 10 10 1 ;
	setAttr ".mtxe" yes;
	setAttr ".mtye" yes;
	setAttr ".xtxe" yes;
	setAttr ".xtye" yes;
	setAttr ".mnsl" -type "double3" 0.5 0.5 -1 ;
	setAttr ".mxsl" -type "double3" 2 2 1 ;
	setAttr ".msxe" yes;
	setAttr ".msye" yes;
	setAttr ".xsxe" yes;
	setAttr ".xsye" yes;
createNode nurbsCurve -n "SpecularUiRig_R_CtrlShape" -p "SpecularUiRig_R_Ctrl";
	rename -uid "D79AC29B-4819-8A11-59BA-509CB5688710";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 16;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.663704873776382 0.24075600806659736 -0.0085445230659431104
		1.4220492395678193 0.34085304917967107 -0.0085445230659431104
		1.1803936053593134 0.24075600806659736 -0.0085445230659431104
		1.0802965642462681 -0.00089962614191563262 -0.0085445230659431104
		1.1803936053593134 -0.24255526035041441 -0.0085445230659431104
		1.4220492395678193 -0.34265230146345971 -0.0085445230659431104
		1.663704873776382 -0.24255526035041441 -0.0085445230659431104
		1.7638019148894344 -0.00089962614191563262 -0.0085445230659431104
		1.663704873776382 0.24075600806659736 -0.0085445230659431104
		1.4220492395678193 0.34085304917967107 -0.0085445230659431104
		1.1803936053593134 0.24075600806659736 -0.0085445230659431104
		;
createNode transform -n "BlinkUiRig_R_Ctrl" -p "EyeUiRigCtrl_R_Grp";
	rename -uid "7F7336B8-47C0-D59B-7842-199AE2245F7C";
	setAttr -l on ".ro";
	setAttr ".rp" -type "double3" 1.4195582565076776 0.73479132125589786 -0.0085445230659416758 ;
	setAttr ".sp" -type "double3" 1.4195582565076776 0.73479132125589786 -0.0085445230659416758 ;
	setAttr ".mntl" -type "double3" -2 -5 -1 ;
	setAttr ".mxtl" -type "double3" 10 5 1 ;
	setAttr ".mnsl" -type "double3" 0 0 -1 ;
	setAttr ".mxsl" -type "double3" 2 2 1 ;
	setAttr ".msxe" yes;
	setAttr ".msye" yes;
	setAttr ".xsxe" yes;
	setAttr ".xsye" yes;
createNode nurbsCurve -n "BlinkUiRig_R_CtrlShape" -p "BlinkUiRig_R_Ctrl";
	rename -uid "01EC3D5B-4014-43F2-E07E-DA9A317FB4E7";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.9028695249247107 0.87634991435136556 -0.0085445230659431104
		1.4195582565076492 1.0765439965774561 -0.0085445230659431104
		0.93624698809063034 0.87634991435136556 -0.0085445230659431104
		0.73605290586456107 0.39303864593432536 -0.0085445230659431104
		0.93624698809063034 0.54875635165799963 -0.0085445230659431104
		1.4195582565076492 0.76640912302582365 -0.0085445230659431104
		1.9028695249247107 0.54875635165799963 -0.0085445230659431104
		2.1030636071508013 0.39303864593432536 -0.0085445230659431104
		1.9028695249247107 0.87634991435136556 -0.0085445230659431104
		1.4195582565076492 1.0765439965774561 -0.0085445230659431104
		0.93624698809063034 0.87634991435136556 -0.0085445230659431104
		;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "183805CA-426F-A471-5860-AE9D8F463A2F";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "406B7BB3-49F4-580D-EEBC-23B1E79A9B85";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "F5F58AA3-459C-06BE-A963-518B7CA407FB";
createNode displayLayerManager -n "layerManager";
	rename -uid "23AA9093-400B-734B-1147-EEB19DF60D49";
createNode displayLayer -n "defaultLayer";
	rename -uid "2BF2E9DA-46A3-2178-FFCC-938CEBB0EA75";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "BFF123B8-4982-35FF-5DE7-D8B2FA21F8F4";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "8CA7FFA2-4BC2-9184-0912-C8B45AA34562";
	setAttr ".g" yes;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "F7DCE56F-434D-3D43-3A90-CBA9F97CD3AC";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "7240E78E-4915-2B8F-B875-8DB1BB0CDE38";
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -536.38093106708914 -434.2023636987293 ;
	setAttr ".tgi[0].vh" -type "double2" 541.29759753838505 433.0118875555583 ;
createNode nodeGraphEditorInfo -n "MayaNodeEditorSavedTabsInfo";
	rename -uid "317D53DB-4E39-74D0-2A27-49832B0B444E";
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -6060.7140448831478 -5719.5479198532876 ;
	setAttr ".tgi[0].vh" -type "double2" 6316.6664156648949 1083.8338183457336 ;
	setAttr -s 15 ".tgi[0].ni";
	setAttr ".tgi[0].ni[0].x" -91.428573608398438;
	setAttr ".tgi[0].ni[0].y" -358.57144165039063;
	setAttr ".tgi[0].ni[0].nvs" 18304;
	setAttr ".tgi[0].ni[1].x" -91.428573608398438;
	setAttr ".tgi[0].ni[1].y" -98.571426391601563;
	setAttr ".tgi[0].ni[1].nvs" 18304;
	setAttr ".tgi[0].ni[2].x" -91.428573608398438;
	setAttr ".tgi[0].ni[2].y" 421.42855834960938;
	setAttr ".tgi[0].ni[2].nvs" 18304;
	setAttr ".tgi[0].ni[3].x" -91.428573608398438;
	setAttr ".tgi[0].ni[3].y" -488.57144165039063;
	setAttr ".tgi[0].ni[3].nvs" 18304;
	setAttr ".tgi[0].ni[4].x" -295.71429443359375;
	setAttr ".tgi[0].ni[4].y" 227.14285278320313;
	setAttr ".tgi[0].ni[4].nvs" 18304;
	setAttr ".tgi[0].ni[5].x" -91.428573608398438;
	setAttr ".tgi[0].ni[5].y" 551.4285888671875;
	setAttr ".tgi[0].ni[5].nvs" 18304;
	setAttr ".tgi[0].ni[6].x" -91.428573608398438;
	setAttr ".tgi[0].ni[6].y" 161.42857360839844;
	setAttr ".tgi[0].ni[6].nvs" 18304;
	setAttr ".tgi[0].ni[7].x" -602.85711669921875;
	setAttr ".tgi[0].ni[7].y" 227.14285278320313;
	setAttr ".tgi[0].ni[7].nvs" 18304;
	setAttr ".tgi[0].ni[8].x" -295.71429443359375;
	setAttr ".tgi[0].ni[8].y" 97.142860412597656;
	setAttr ".tgi[0].ni[8].nvs" 18304;
	setAttr ".tgi[0].ni[9].x" -91.428573608398438;
	setAttr ".tgi[0].ni[9].y" 291.42855834960938;
	setAttr ".tgi[0].ni[9].nvs" 18304;
	setAttr ".tgi[0].ni[10].x" -295.71429443359375;
	setAttr ".tgi[0].ni[10].y" -32.857143402099609;
	setAttr ".tgi[0].ni[10].nvs" 18304;
	setAttr ".tgi[0].ni[11].x" -295.71429443359375;
	setAttr ".tgi[0].ni[11].y" -162.85714721679688;
	setAttr ".tgi[0].ni[11].nvs" 18304;
	setAttr ".tgi[0].ni[12].x" -91.428573608398438;
	setAttr ".tgi[0].ni[12].y" -228.57142639160156;
	setAttr ".tgi[0].ni[12].nvs" 18304;
	setAttr ".tgi[0].ni[13].x" -91.428573608398438;
	setAttr ".tgi[0].ni[13].y" 31.428571701049805;
	setAttr ".tgi[0].ni[13].nvs" 18304;
	setAttr ".tgi[0].ni[14].x" 55.714286804199219;
	setAttr ".tgi[0].ni[14].y" -3128.571533203125;
	setAttr ".tgi[0].ni[14].nvs" 18304;
select -ne :time1;
	setAttr -av -k on ".cch";
	setAttr -k on ".fzn";
	setAttr -av -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 70;
	setAttr -av -k on ".unw" 70;
	setAttr -av -k on ".etw";
	setAttr -av -k on ".tps";
	setAttr -av -k on ".tms";
select -ne :hardwareRenderingGlobals;
	setAttr -av -k on ".cch";
	setAttr -k on ".fzn";
	setAttr -av -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".rm";
	setAttr -k on ".lm";
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr -k on ".hom";
	setAttr -k on ".hodm";
	setAttr -k on ".xry";
	setAttr -k on ".jxr";
	setAttr -k on ".sslt";
	setAttr -k on ".cbr";
	setAttr -k on ".bbr";
	setAttr -k on ".mhl";
	setAttr -k on ".cons";
	setAttr -k on ".vac";
	setAttr -av -k on ".hwi";
	setAttr -k on ".csvd";
	setAttr -av -k on ".ta";
	setAttr -av -k on ".tq";
	setAttr -k on ".ts";
	setAttr -av -k on ".etmr";
	setAttr -av -k on ".tmr";
	setAttr -av -k on ".aoon";
	setAttr -av -k on ".aoam";
	setAttr -av -k on ".aora";
	setAttr -k on ".aofr";
	setAttr -av -k on ".aosm";
	setAttr -av -k on ".hff";
	setAttr -av -k on ".hfd";
	setAttr -av -k on ".hfs";
	setAttr -av -k on ".hfe";
	setAttr -av ".hfc";
	setAttr -av -k on ".hfcr";
	setAttr -av -k on ".hfcg";
	setAttr -av -k on ".hfcb";
	setAttr -av -k on ".hfa";
	setAttr -av -k on ".mbe";
	setAttr -k on ".mbt";
	setAttr -av -k on ".mbsof";
	setAttr -k on ".mbsc";
	setAttr -k on ".mbc";
	setAttr -k on ".mbfa";
	setAttr -k on ".mbftb";
	setAttr -k on ".mbftg";
	setAttr -k on ".mbftr";
	setAttr -k on ".mbfta";
	setAttr -k on ".mbfe";
	setAttr -k on ".mbme";
	setAttr -k on ".mbcsx";
	setAttr -k on ".mbcsy";
	setAttr -k on ".mbasx";
	setAttr -k on ".mbasy";
	setAttr -av -k on ".blen";
	setAttr -k on ".blth";
	setAttr -k on ".blfr";
	setAttr -k on ".blfa";
	setAttr -av -k on ".blat";
	setAttr -av -k on ".msaa";
	setAttr -av -k on ".aasc";
	setAttr -k on ".aasq";
	setAttr -k on ".laa";
	setAttr -k on ".fprt" yes;
	setAttr -k on ".rtfm";
select -ne :renderPartition;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
select -ne :renderGlobalsList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
select -ne :defaultShaderList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 5 ".s";
select -ne :postProcessList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
	setAttr -k on ".ihi";
select -ne :initialShadingGroup;
	setAttr -av -k on ".cch";
	setAttr -k on ".fzn";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".bbx";
	setAttr -k on ".vwm";
	setAttr -k on ".tpv";
	setAttr -k on ".uit";
	setAttr -k on ".mwc";
	setAttr -av -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr -k on ".ro" yes;
select -ne :initialParticleSE;
	setAttr -av -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr -k on ".ro" yes;
lockNode -l 0 -lu 1;
select -ne :defaultRenderGlobals;
	addAttr -ci true -h true -sn "dss" -ln "defaultSurfaceShader" -dt "string";
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -av -k on ".macc";
	setAttr -av -k on ".macd";
	setAttr -av -k on ".macq";
	setAttr -av -k on ".mcfr";
	setAttr -cb on ".ifg";
	setAttr -av -k on ".clip";
	setAttr -av -k on ".edm";
	setAttr -av -k on ".edl";
	setAttr -av -cb on ".ren";
	setAttr -av -k on ".esr";
	setAttr -av -k on ".ors";
	setAttr -cb on ".sdf";
	setAttr -av -k on ".outf";
	setAttr -av -cb on ".imfkey";
	setAttr -av -k on ".gama";
	setAttr -av -k on ".exrc";
	setAttr -av -k on ".expt";
	setAttr -av -k on ".an";
	setAttr -cb on ".ar";
	setAttr -av -k on ".fs";
	setAttr -av -k on ".ef";
	setAttr -av -k on ".bfs";
	setAttr -av -cb on ".me";
	setAttr -cb on ".se";
	setAttr -av -k on ".be";
	setAttr -av -cb on ".ep";
	setAttr -av -k on ".fec";
	setAttr -av -k on ".ofc";
	setAttr -cb on ".ofe";
	setAttr -cb on ".efe";
	setAttr -cb on ".oft";
	setAttr -cb on ".umfn";
	setAttr -cb on ".ufe";
	setAttr -av -k on ".pff";
	setAttr -av -cb on ".peie";
	setAttr -av -cb on ".ifp";
	setAttr -k on ".rv";
	setAttr -av -k on ".comp";
	setAttr -av -k on ".cth";
	setAttr -av -k on ".soll";
	setAttr -av -cb on ".sosl";
	setAttr -av -k on ".rd";
	setAttr -av -k on ".lp";
	setAttr -av -k on ".sp";
	setAttr -av -k on ".shs";
	setAttr -av -k on ".lpr";
	setAttr -cb on ".gv";
	setAttr -cb on ".sv";
	setAttr -av -k on ".mm";
	setAttr -av -k on ".npu";
	setAttr -av -k on ".itf";
	setAttr -av -k on ".shp";
	setAttr -cb on ".isp";
	setAttr -av -k on ".uf";
	setAttr -av -k on ".oi";
	setAttr -av -k on ".rut";
	setAttr -av -k on ".mot";
	setAttr -av -k on ".mb";
	setAttr -av -k on ".mbf";
	setAttr -av -k on ".mbso";
	setAttr -av -k on ".mbsc";
	setAttr -av -k on ".afp";
	setAttr -av -k on ".pfb";
	setAttr -av -k on ".pram";
	setAttr -av -k on ".poam";
	setAttr -av -k on ".prlm";
	setAttr -av -k on ".polm";
	setAttr -av -cb on ".prm";
	setAttr -av -cb on ".pom";
	setAttr -cb on ".pfrm";
	setAttr -cb on ".pfom";
	setAttr -av -k on ".bll";
	setAttr -av -k on ".bls";
	setAttr -av -k on ".smv";
	setAttr -av -k on ".ubc";
	setAttr -av -k on ".mbc";
	setAttr -cb on ".mbt";
	setAttr -av -k on ".udbx";
	setAttr -av -k on ".smc";
	setAttr -av -k on ".kmv";
	setAttr -cb on ".isl";
	setAttr -cb on ".ism";
	setAttr -cb on ".imb";
	setAttr -av -k on ".rlen";
	setAttr -av -k on ".frts";
	setAttr -av -k on ".tlwd";
	setAttr -av -k on ".tlht";
	setAttr -av -k on ".jfc";
	setAttr -cb on ".rsb";
	setAttr -av -k on ".ope";
	setAttr -av -k on ".oppf";
	setAttr -av -k on ".rcp";
	setAttr -av -k on ".icp";
	setAttr -av -k on ".ocp";
	setAttr -cb on ".hbl";
	setAttr ".dss" -type "string" "lambert1";
select -ne :defaultResolution;
	setAttr -av -k on ".cch";
	setAttr -av -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -av -k on ".w";
	setAttr -av -k on ".h";
	setAttr -av -k on ".pa" 1;
	setAttr -av -k on ".al";
	setAttr -av -k on ".dar";
	setAttr -av -k on ".ldar";
	setAttr -av -k on ".dpi";
	setAttr -av -k on ".off";
	setAttr -av -k on ".fld";
	setAttr -av -k on ".zsl";
	setAttr -av -k on ".isu";
	setAttr -av -k on ".pdu";
select -ne :hardwareRenderGlobals;
	setAttr -av -k on ".cch";
	setAttr -av -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -av -k off -cb on ".ctrs" 256;
	setAttr -av -k off -cb on ".btrs" 512;
	setAttr -av -k off -cb on ".fbfm";
	setAttr -av -k off -cb on ".ehql";
	setAttr -av -k off -cb on ".eams";
	setAttr -av -k off -cb on ".eeaa";
	setAttr -av -k off -cb on ".engm";
	setAttr -av -k off -cb on ".mes";
	setAttr -av -k off -cb on ".emb";
	setAttr -av -k off -cb on ".mbbf";
	setAttr -av -k off -cb on ".mbs";
	setAttr -av -k off -cb on ".trm";
	setAttr -av -k off -cb on ".tshc";
	setAttr -av -k off -cb on ".enpt";
	setAttr -av -k off -cb on ".clmt";
	setAttr -av -k off -cb on ".tcov";
	setAttr -av -k off -cb on ".lith";
	setAttr -av -k off -cb on ".sobc";
	setAttr -av -k off -cb on ".cuth";
	setAttr -av -k off -cb on ".hgcd";
	setAttr -av -k off -cb on ".hgci";
	setAttr -av -k off -cb on ".mgcs";
	setAttr -av -k off -cb on ".twa";
	setAttr -av -k off -cb on ".twz";
	setAttr -av -k on ".hwcc";
	setAttr -av -k on ".hwdp";
	setAttr -av -k on ".hwql";
	setAttr -av -k on ".hwfr";
	setAttr -av -k on ".soll";
	setAttr -av -k on ".sosl";
	setAttr -av -k on ".bswa";
	setAttr -av -k on ".shml";
	setAttr -av -k on ".hwel";
select -ne :ikSystem;
	setAttr -s 4 ".sol";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr ":perspShape.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[0].dn";
connectAttr ":topShape.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[1].dn";
connectAttr ":front.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[2].dn";
connectAttr ":lightLinker1.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[3].dn";
connectAttr "defaultLayer.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[4].dn";
connectAttr ":side.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[5].dn";
connectAttr ":frontShape.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[6].dn";
connectAttr "layerManager.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[7].dn";
connectAttr "shapeEditorManager.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[8].dn"
		;
connectAttr ":sideShape.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[9].dn";
connectAttr "defaultRenderLayer.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[10].dn"
		;
connectAttr "poseInterpolatorManager.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[11].dn"
		;
connectAttr ":persp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[12].dn";
connectAttr ":top.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[13].dn";
connectAttr "EyeUiRigCtrl_Grp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[14].dn"
		;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
dataStructure -fmt "raw" -as "name=faceConnectOutputStructure:bool=faceConnectOutput:string[200]=faceConnectOutputAttributes:string[200]=faceConnectOutputGroups";
dataStructure -fmt "raw" -as "name=faceConnectMarkerStructure:bool=faceConnectMarker:string[200]=faceConnectOutputGroups";
dataStructure -fmt "raw" -as "name=externalContentTablZ:string=nodZ:string=key:string=upath:uint32=upathcrc:string=rpath:string=roles";
dataStructure -fmt "raw" -as "name=FBXFastExportSetting_FBX:string=54";
dataStructure -fmt "raw" -as "name=FBXFastExportSetting_MB:string=19424";
dataStructure -fmt "raw" -as "name=idStructure:int32=ID";
// End of EyeUiCtrlTmp.ma
