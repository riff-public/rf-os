from rf_maya.rftool.rig.rigScript import caFacialRig
reload(caFacialRig)
from rf_maya.rftool.rig.rigScript import caFacialModule
reload(caFacialModule)
from utaTools.utapy import utaCore
reload(utaCore)

def main():
       charName = utaCore.checkCharacterName()
       caFacialRig.facialUiRig(           eyeSeq = True, 
                                          eyeShaDowCtrl = True,
                                          eyeSide = ['R', 'L'],
                                          eyeUiRigCtrl = 'EyeUiRig_L_Ctrl',
                                          eyeTextureUiRigCtrl = 'EyeTextureUiRig_L_Ctrl',
                                          eyeElem = 'EyeSeq',
                                          eyePlaneGeo = 'EyePlaneUiRig_L_Geo', 
                                          eyeTextureUiGeo = 'EyeUiRig_Geo',
                                          eyeMaskCtrl = ['EyeMaskUpUiRig_L_Ctrl', 'EyeMaskDnUiRig_L_Ctrl'],
                                          eyeMaskElem = 'Mask',
                                          eyeMaskSeq = True, 
                                          eyeWhiteLineSeq = True,
                                          eyeMaskShadow = True, 
                                          eyeWhiteLineShadow = False,

                                          eyeBall = True, 
                                          eyeBallSide = ['R', 'L'],
                                          eyeBallEyeUiRigCtrl = 'EyeUiRig_L_Ctrl',
                                          eyeBallElem = 'EyeBall',
                                          eyeBallIrisCtrl = 'IrisUiRig_L_Ctrl',
                                          eyeBallBlinkCtrl = 'BlinkUiRig_L_Ctrl',
                                          eyeBallSpecularCtrl = 'SpecularUiRig_L_Ctrl',
                                          eyeBallSpecularSpACtrl = 'SpecularSpAUiRig_L_Ctrl',
                                          eyeBallSpecularSpBCtrl = 'SpecularSpBUiRig_L_Ctrl',
                                          eyeBallSpecularPosition = [[94, 0, 0],[104, 0, 0]],
                                          
                                          mouth = True, 
                                          mouthShadowCtrl = True,
                                          mouthSide = [],
                                          mouthUiRigCtrl = 'MouthUiRig_Ctrl',
                                          mouthTextureUiRigCtrl = 'MouthTextureUiRig_Ctrl',
                                          mouthElem = 'MouthSeq',
                                          mouthPlaneGeo = 'MouthPlaneUiRig_Geo',
                                          mouthShadowPlaneGeo = '',
                                          mouthTextureUiGeo = 'MouthUiRig_Geo', 

                                          mouthBall = True, 
                                          mouthBallSide = [],
                                          mouthBallMouthUiRigCtrl = 'MouthUiRig_Ctrl',
                                          mouthBallElem = 'MouthBall',
                                          mouthBallTeethUpCtrl = 'TeethUpUiRig_Ctrl',
                                          mouthBallTongueCtrl = 'TongueUiRig_Ctrl',
                                          mouthBallTeethDnCtrl = 'TeethDnUiRig_Ctrl',

                                          eyebrow = True, 
                                          eyebrwoShadowCtrl = True,
                                          eyebrowSide = ['R', 'L'],
                                          eyebrowUiRigCtrl = 'EyebrowUiRig_L_Ctrl',
                                          eyebrowTextureUiRigCtrl = 'EyebrowTextureUiRig_L_Ctrl',
                                          eyebrowElem = 'EyebrowSeq',
                                          eyebrowPlaneGeo = 'EyebrowPlaneUiRig_L_Geo',
                                          eyebrowShadowPlaneGeo = 'EyebrowShadowPlaneUiRig_L_Geo',
                                          eyebrowTextureUiGeo = 'EyebrowUiRig_Geo',

                                          cheek = True, 
                                          cheekShadowCtrl = True,
                                          checkSide = ['R', 'L'],
                                          cheekUiRigCtrl = 'CheekUiRig_L_Ctrl',
                                          cheekTextureUiRigCtrl = 'CheekTextureUiRig_L_Ctrl',
                                          cheekElem = 'CheekSeq',
                                          cheekPlaneGeo = 'CheekPlaneUiRig_L_Geo',
                                          cheekShadowPlaneGeo = 'CheekShadowPlaneUiRig_L_Geo',
                                          cheekTextureUiGeo = 'CheekUiRig_Geo',

                                          wrinkle = True, 
                                          wrinkleShadowCtrl = True,
                                          wrinkleSide = ['R', 'L'],
                                          wrinkleUiRigCtrl = 'WrinkleUiRig_Ctrl',
                                          wrinkleTextureUiRigCtrl = 'WrinkleTextureUiRig_Ctrl',
                                          wrinkleElem = 'WrinkleSeq',
                                          wrinklePlaneGeo = 'WrinklePlaneUiRig_Geo',
                                          wrinkleShadowPlaneGeo = '',
                                          wrinkleTextureUiGeo = 'WrinkleUiRig_Geo',
                                          wrinkleFollow = True,
                                          wrinkleFollowCtrl = 'EyebrowUiRig_L_Ctrl',

                                          jaw = True, 
                                          jawShadowCtrl = True,
                                          jawSide = [],
                                          jawUiRigCtrl = 'JawUiRig_Ctrl',
                                          jawTextureUiRigCtrl = 'JawTextureUiRig_Ctrl',
                                          jawElem = 'JawSeq',
                                          jawPlaneGeo = 'JawPlaneUiRig_Geo', 
                                          jawShadowPlaneGeo = '', 
                                          jawTextureUiGeo = 'JawUiRig_Geo',

                                          mouthEffUp = True,
                                          mouthEffUpShadowCtrl = True,
                                          mouthEffUpSide = ['L', 'R'],
                                          mouthEffUpElem = 'MouthEffUpSeq',
                                          mouthEffUpUiRigCtrl = 'MouthEffUpUiRig_L_Ctrl',
                                          mouthEffUpTextureUiRigCtrl = 'MouthEffUpTextureUiRig_L_Ctrl',
                                          mouthEffUpPlaneGeo = 'MouthEffUpPlaneUiRig_L_Geo',
                                          mouthEffUpShadowPlaneGeo = '',
                                          mouthEffUpTextureUiGeo = 'MouthEffUpUiRig_Geo',
                                          mouthEffUpFollow = True,
                                          mouthEffUpFollowCtrl = 'MouthUiRig_Ctrl', 

                                          mouthEffDn = True,
                                          mouthEffDnShadowCtrl = True,
                                          mouthEffDnSide = ['R', 'L'],
                                          mouthEffDnElem = 'MouthEffDnSeq',
                                          mouthEffDnUiRigCtrl = 'MouthEffDnUiRig_L_Ctrl',
                                          mouthEffDnTextureUiRigCtrl = 'MouthEffDnTextureUiRig_L_Ctrl',
                                          mouthEffDnPlaneGeo = 'MouthEffDnPlaneUiRig_L_Geo',
                                          mouthEffDnShadowPlaneGeo = '',
                                          mouthEffDnTextureUiGeo = 'MouthEffDnUiRig_Geo',
                                          mouthEffDnFollow = True,
                                          mouthEffDnFollowCtrl = 'MouthUiRig_Ctrl', 

                                          head = True,
                                          headRamp = True,
                                          headRampCtrl = 'HeadRamp_Ctrl',
                                          headRampFolderName = 'txr',
                                          headRampElem = 'Head',
                                          headRampGeo = 'HeadUiRig_Geo',


                                          texturePath = 'P:/CA/asset/work/char/{}/texture/main/images/preview'.format(charName), 
                                          )

       ## addRig ----------------------------------------------------------------   
       # ## Sweat 1
       # caFacialModule.imageSeqFacial(
       #                                                uiRigCtrl = 'Sweat1UiRig_Ctrl', 
       #                                                textureUiRigCtrl = '', 
       #                                                shaDowCtrl = True, 
       #                                                specular = False, 
       #                                                elem = 'SweatSeq', 
       #                                                planeGeo = 'Sweat1PlaneUiRig_Geo', 
       #                                                textureUiGeo = '', 
       #                                                texturePath = 'P:/CA/asset/work/char/{}/texture/main/images/preview'.format(charName), 
       #                                                )
       # print 'Sweat 1 is DONE'
       # ## Sweat 2
       # caFacialModule.imageSeqFacial(
       #                                                uiRigCtrl = 'Sweat2UiRig_Ctrl', 
       #                                                textureUiRigCtrl = '', 
       #                                                shaDowCtrl = True, 
       #                                                specular = False, 
       #                                                elem = 'SweatSeq', 
       #                                                planeGeo = 'Sweat2PlaneUiRig_Geo', 
       #                                                textureUiGeo = '', 
       #                                                texturePath = 'P:/CA/asset/work/char/{}/texture/main/images/preview'.format(charName), 
       #                                                )
       # print 'Sweat 2 is DONE'

       # ## Sweat 3
       # caFacialModule.imageSeqFacial(
       #                                                uiRigCtrl = 'Sweat3UiRig_Ctrl', 
       #                                                textureUiRigCtrl = '', 
       #                                                shaDowCtrl = True, 
       #                                                specular = False, 
       #                                                elem = 'SweatSeq', 
       #                                                planeGeo = 'Sweat3PlaneUiRig_Geo', 
       #                                                textureUiGeo = '', 
       #                                                texturePath = 'P:/CA/asset/work/char/{}/texture/main/images/preview'.format(charName), 
       #                                                )
       # print 'Sweat 3 is DONE'
