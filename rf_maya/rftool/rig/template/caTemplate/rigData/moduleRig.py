import maya.cmds as mc
import pymel.core as pm
from rigScript import characterRig
reload(characterRig)
from rigScript import fkChain
reload(fkChain)
from ncmel import subRig
reload(subRig)
from ncmel import fkRig
reload(fkRig)
from lpRig import rubberBandRig
reload(rubberBandRig)
from utaTools.utapy import utaCore
reload(utaCore)
from ncscript.rig import angleTools
reload(angleTools)
from ncmel import core 
reload(core)
from utaTools.utapy import clavMuscleRig
reload(clavMuscleRig)

from rigScript import ribbonRig

from rigScript import caRigModule
reload(caRigModule)

from ncscript2.pymel import shapeTools
reload(shapeTools)


from rigScript import ribbonRig
from ncmel import rigTools
reload(rigTools)


# check Tes
def Run():
     charRig = characterRig.HumanRig(   head        = True ,
                                        hairRoot    = True ,
                                        hair        = [] ,
                                        neck        = True ,
                                        eyeL        = False ,
                                        eyeR        = False ,
                                        addEye      = [] ,
                                        iris        = False ,
                                        pupil       = False ,
                                        eyeDot      = False ,
                                        jaw         = False ,
                                        torso       = True ,
                                        clavL       = True ,
                                        clavR       = True ,
                                        armL        = True ,
                                        armR        = True ,
                                        fingersL    = [] ,
                                        fingersR    = [] ,
                                        breastL     = False ,
                                        breastR     = False ,
                                        legL        = True ,
                                        legR        = True ,
                                        footL       = True,
                                        footR       = True ,
                                        toeFingersL = [] ,
                                        toeFingersR = [] ,
                                        sex         = 'Male' ,
                                        ribbon      = True ,
                                        hi          = True ,
                                        inbtw       = True ,
                                        size        = 1
                              )  

     print '#Generate >> ------------------------AddRig---------------------------'
     
     size = 1
     ###################### HAND

     print "# Generate >> Hand L"
     HandL = fkChain.Run( name = 'Hand',
                                   side = 'L',
                                   parent = 'Wrist_L_Jnt',
                                   shape = 'circle',
                                   jnt_ctrl =0,
                                   scalable =1,
                                   ctrlGrp =charRig.main.addRigCtrlGrp )

     print "# Generate >> Hand R"
     HandR = fkChain.Run( name = 'Hand',
                              side = 'R',
                              parent = 'Wrist_R_Jnt',
                              shape = 'circle',
                              inv_stretch = 1,
                              jnt_ctrl =0,
                              scalable=1,
                              ctrlGrp =charRig.main.addRigCtrlGrp)

     ####################### FINGERS

     print "# Generate >> Thumb L"
     ThumbL = fkChain.Run( name = 'Thumb',
                                   side = 'L',
                                   parent = 'Wrist_L_Jnt',
                                   shape = 'circle',
                                   jnt_ctrl =0,
                                   color = 'pink',
                                   scalable =1,
                                   ctrlGrp =charRig.main.addRigCtrlGrp)
     pm.addAttr( ThumbL.ctrlArray[0] , ln = 'geoVis' , at = 'long' , k = True , min = 0 , max = 1 , dv= 1)
     ThumbL.ctrlArray[0].attr('geoVis') >> ThumbL.parsArray[1].attr('v')

     print "# Generate >> Thumb R"
     ThumbR = fkChain.Run( name = 'Thumb',
                              side = 'R',
                              parent = 'Wrist_R_Jnt',
                              shape = 'circle',
                              inv_stretch = 1,
                              color = 'pink',
                              jnt_ctrl =0,
                              scalable=1,
                              ctrlGrp =charRig.main.addRigCtrlGrp)
     pm.addAttr( ThumbR.ctrlArray[0] , ln = 'geoVis' , at = 'long' , k = True , min = 0 , max = 1 , dv= 1)
     ThumbR.ctrlArray[0].attr('geoVis') >> ThumbR.parsArray[1].attr('v')


     print "# Generate >> Index L"
     IndexL = fkChain.Run( name = 'Index',
                                   side = 'L',
                                   parent = 'Wrist_L_Jnt',
                                   shape = 'circle',
                                   jnt_ctrl =0,
                                   color = 'pink',
                                   scalable =1,
                                   ctrlGrp =charRig.main.addRigCtrlGrp)
     pm.addAttr( IndexL.ctrlArray[0] , ln = 'geoVis' , at = 'long' , k = True , min = 0 , max = 1 , dv= 1)
     IndexL.ctrlArray[0].attr('geoVis') >> IndexL.parsArray[1].attr('v')
     
     print "# Generate >> Index R"
     IndexR = fkChain.Run( name = 'Index',
                              side = 'R',
                              parent = 'Wrist_R_Jnt',
                              shape = 'circle',
                              inv_stretch = 1,
                              color = 'pink',
                              jnt_ctrl =0,
                              scalable=1,
                              ctrlGrp =charRig.main.addRigCtrlGrp)
     pm.addAttr( IndexR.ctrlArray[0] , ln = 'geoVis' , at = 'long' , k = True , min = 0 , max = 1 , dv= 1)
     IndexR.ctrlArray[0].attr('geoVis') >> IndexR.parsArray[1].attr('v')


    ########################## FOOT


     print "# Generate >> Foot_L_Jnt"
     HandL = subRig.Run(              
                                    name       = 'Foot' ,
                                    tmpJnt     = 'Foot_L_TmpJnt' ,
                                    parent     = 'Ankle_L_Jnt' ,
                                    ctrlGrp    =  charRig.main.addRigCtrlGrp ,
                                    shape      = 'circle' ,
                                    side       = 'L' ,
                                    size       = size 
                            )
     print "# Generate >> Foot_R_Jnt"
     HandR = subRig.Run(              
                                    name       = 'Foot' ,
                                    tmpJnt     = 'Foot_R_TmpJnt' ,
                                    parent     = 'Ankle_R_Jnt' ,
                                    ctrlGrp    =  charRig.main.addRigCtrlGrp ,
                                    shape      = 'circle' ,
                                    side       = 'R' ,
                                    size       = size 
                            )

    ## Arm
     armBtw = 10
     mc.setAttr("ArmRbn_L_Ctrl.ElbowPosAmp", 0)
     mc.setAttr("ArmRbn_R_Ctrl.ElbowPosAmp", 0)
     mc.setAttr("ArmLimbBtw_L_Jnt.scaleLimit", armBtw)
     mc.setAttr("ArmLimbBtw_L_Jnt.scaleBtw", armBtw)
     mc.setAttr("ArmLimbBtw_R_Jnt.scaleLimit", armBtw)
     mc.setAttr("ArmLimbBtw_R_Jnt.scaleBtw", armBtw)

    ## Leg
     legBtw = 10
     mc.setAttr ("LegRbn_L_Ctrl.KneePosAmp", 0)
     mc.setAttr ("LegRbn_R_Ctrl.KneePosAmp", 0)
     mc.setAttr ("LegLimbBtw_L_Jnt.scaleLimit", legBtw)
     mc.setAttr ("LegLimbBtw_L_Jnt.scaleBtw", legBtw)
     mc.setAttr ("LegLimbBtw_R_Jnt.scaleLimit", legBtw)
     mc.setAttr ("LegLimbBtw_R_Jnt.scaleBtw", legBtw)


     print '----------DONE CHARACTER RIG----------'






