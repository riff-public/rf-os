import maya.cmds as mc
from rf_maya.rftool.rig.rigScript import caFacialModule
reload(caFacialModule)


'''

import sys
sys.path.append(r'P:/CA/asset/work/char/GB/rig/rigData')
import caFacialCtrlRun
reload(caFacialCtrlRun)
## run fclUiCtrl
caFacialCtrlRun.FacialCtrlRun(fclRigGeoGrp = 'FclRigGeo_Grp',fclPlaneGeoGrp = 'FacePlaneFclRigGeo_Grp')

'''
def FacialCtrlRun(fclRigGeoGrp = 'FclRigGeo_Grp', fclPlaneGeoGrp = 'FacePlaneFclRigGeo_Grp', *args):

	################################### Generate Facial Ctrl Rig  Process / Module ###################################

	## generate main Ui
	faceUiRigCtrlNameGrp, FaceUiRigGeoGrp, CtrlUiRigGrp, PlaneUiRigGeoGrp, visVisCtrlGrp, visCtrlName = caFacialModule.generateMainUiCtrl(	
																																			fclRigGeoGrp = fclRigGeoGrp, 
																																			fclPlaneGeoGrp = fclPlaneGeoGrp
																																			)
	
	## generate facialUiCtrl
	ctrlGrpAll, ctrlAll = caFacialModule.generateFacialUiCtrl(
													fclRigGeoGrp = fclRigGeoGrp, 
													fclPlaneGeoGrp = fclPlaneGeoGrp, 
													ctrlUiRigGrp = CtrlUiRigGrp,  
													faceUiRigGeoGrp = FaceUiRigGeoGrp,
													process = 'UiRig',
													visCtrl = visCtrlName,
													organizePlaneUiGeo = True,
													writeOrRead = False,
													distanceUiCtrlAmp = 1, 
													mouthOpOne = '',
													mouthOpTwo = '',
													mouthOpThree = '',
													)
	##------------------------------------------------------------------------------------------------
	##------------------------------------------------------------------------------------------------

	##01 generate sub textureUiCtrl "Eye"
	caFacialModule.generateViewUiCtrl(	
													numberNo = 1,
													name = 'Eye', 
													elem = 'UiRig',  
													menuName = ['Eye'], 
													sides = [['R', 'L']], 
													position =[0],
													parent = PlaneUiRigGeoGrp,)
	

	##02 generate sub textureUiCtrl "Mouth"
	caFacialModule.generateViewUiCtrl(	
													numberNo = 2,
													name = 'Mouth', 
													elem = 'UiRig',  
													menuName = ['Mouth'], 
													sides = [['M']], 
													position =[0],
													parent = PlaneUiRigGeoGrp)


	##03 generate sub textureUiCtrl "Eyebrow"
	caFacialModule.generateViewUiCtrl(	
													numberNo = 3,
													name = 'Eyebrow', 
													elem = 'UiRig',  
													menuName = ['Eyebrow'], 
													sides = [['R', 'L']], 
													position =[0],
													parent = PlaneUiRigGeoGrp)

	##04 generate sub textureUiCtrl "Wrinkle"
	caFacialModule.generateViewUiCtrl(	
													numberNo = 4,
													name = 'Wrinkle', 
													elem = 'UiRig',  
													menuName = ['Wrinkle'], 
													sides = [['R', 'L']], 
													position =[0],
													parent = PlaneUiRigGeoGrp)

	##05 generate sub textureUiCtrl "Cheek"
	caFacialModule.generateViewUiCtrl(
													numberNo = 5,
													name = 'Cheek', 
													elem = 'UiRig',  
													menuName = ['Cheek'], 
													sides = [['R', 'L']],
													position =[0],
													parent = PlaneUiRigGeoGrp)


	##06 generate sub textureUiCtrl "Jaw"
	caFacialModule.generateViewUiCtrl(
													numberNo = 6,
													name = 'Jaw', 
													elem = 'UiRig',  
													menuName = ['Jaw'], 
													sides = [['J']],
													position =[0],
													parent = PlaneUiRigGeoGrp)

	##07 generate sub textureUiCtrl "MouthEffUp"
	caFacialModule.generateViewUiCtrl(
													numberNo = 7,
													name = 'MouthEffUp', 
													elem = 'UiRig',  
													menuName = ['MouthEffUp'], 
													sides = [['R', 'L']], 
													position =[0],
													parent = PlaneUiRigGeoGrp)

	##08 generate sub textureUiCtrl "MouthEffDn"
	caFacialModule.generateViewUiCtrl(
													numberNo = 8,
													name = 'MouthEffDn', 
													elem = 'UiRig',  
													menuName = ['MouthEffDn'], 
													sides = [['R', 'L']], 
													position =[0],
													parent = PlaneUiRigGeoGrp)


	# # ## generate sub textureUiCtrl "Mouth"
	# caFacialModule.generateViewUiCtrl(
	# 												numberNo = 6,
	# 												name = 'MouthEffUpDn', 
	# 												elem = 'UiRig',  
	# 												menuName = ['MouthEffUp', 'MouthEffDn'], 
	# 												sides = ['R', 'L'], 
	# 												parent = PlaneUiRigGeoGrp)




