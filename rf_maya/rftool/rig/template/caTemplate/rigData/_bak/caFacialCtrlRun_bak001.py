import maya.cmds as mc
from rf_maya.rftool.rig.rigScript import caFacialModule
reload(caFacialModule)


'''

import sys
sys.path.append(r'P:/CA/asset/work/char/GB/rig/rigData')
import caFacialCtrlRun
reload(caFacialCtrlRun)
## run fclUiCtrl
caFacialCtrlRun.FacialCtrlRun(fclRigGeoGrp = 'FclRigGeo_Grp',fclPlaneGeoGrp = 'FacePlaneFclRigGeo_Grp')

'''
def FacialCtrlRun(fclRigGeoGrp = 'FclRigGeo_Grp', fclPlaneGeoGrp = 'FacePlaneFclRigGeo_Grp', *args):

	################################### Generate Facial Ctrl Rig  Process / Module ###################################

	## generate main Ui
	faceUiRigCtrlNameGrp, FaceUiRigGeoGrp, CtrlUiRigGrp, PlaneUiRigGeoGrp, visVisCtrlGrp, visCtrlName = caFacialModule.generateMainUiCtrl(	
																																			fclRigGeoGrp = fclRigGeoGrp, 
																																			fclPlaneGeoGrp = fclPlaneGeoGrp
																																			)
	
	## generate facialUiCtrl
	ctrlGrpAll, ctrlAll = caFacialModule.generateFacialUiCtrl(
													fclRigGeoGrp = fclRigGeoGrp, 
													fclPlaneGeoGrp = fclPlaneGeoGrp, 
													ctrlUiRigGrp = CtrlUiRigGrp,  
													faceUiRigGeoGrp = FaceUiRigGeoGrp,
													process = 'UiRig',
													visCtrl = visCtrlName,
													organizePlaneUiGeo = True,
													writeOrRead = False,
													distanceUiCtrlAmp = 1, 
													)
	##------------------------------------------------------------------------------------------------
	##------------------------------------------------------------------------------------------------

	##01 generate sub textureUiCtrl "Eye"
	caFacialModule.generateViewUiCtrl(	
													numberNo = 1,
													name = 'Eye', 
													elem = 'UiRig',  
													menuName = ['Eye'], 
													side = [['R', 'L']], 
													parent = PlaneUiRigGeoGrp,)
	

	##02 generate sub textureUiCtrl "Mouth"
	caFacialModule.generateViewUiCtrl(	
													numberNo = 2,
													name = 'Mouth', 
													elem = 'UiRig',  
													menuName = ['Mouth'], 
													side = [['M']], 
													parent = PlaneUiRigGeoGrp)


	##03 generate sub textureUiCtrl "Eyebrow"
	caFacialModule.generateViewUiCtrl(	
													numberNo = 3,
													name = 'Eyebrow', 
													elem = 'UiRig',  
													menuName = ['Eyebrow', 'Wrinkle'], 
													side = [['R', 'L'], ['R', 'L']], 
													parent = PlaneUiRigGeoGrp)

	##04 generate sub textureUiCtrl "Cheek"
	caFacialModule.generateViewUiCtrl(
													numberNo = 4,
													name = 'Cheek', 
													elem = 'UiRig',  
													menuName = ['Cheek', 'Jaw'], 
													side = [['R', 'L'], ['J']],
													parent = PlaneUiRigGeoGrp)


	##05 generate sub textureUiCtrl "MouthEff"
	caFacialModule.generateViewUiCtrl(
													numberNo = 5,
													name = 'MouthEff', 
													elem = 'UiRig',  
													menuName = ['MouthEffUp', 'MouthEffDn'], 
													side = [['R', 'L'], ['R', 'L']], 
													parent = PlaneUiRigGeoGrp)


	# # ## generate sub textureUiCtrl "Mouth"
	# caFacialModule.generateViewUiCtrl(
	# 												numberNo = 6,
	# 												name = 'MouthEffUpDn', 
	# 												elem = 'UiRig',  
	# 												menuName = ['MouthEffUp', 'MouthEffDn'], 
	# 												side = ['R', 'L'], 
	# 												parent = PlaneUiRigGeoGrp)




