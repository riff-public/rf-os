import maya.cmds as mc
import pymel.core as pm

from ncmel import core
reload(core)
from ncmel import rigTools
reload(rigTools)
from ncmel import mainRig
reload(mainRig)
from ncmel import rootRig
reload(rootRig)
from ncmel import pelvisRig
reload(pelvisRig)
from ncmel import spineRig
reload(spineRig)
from ncmel import torsoRig
reload(torsoRig)
from ncmel import neckRig
reload(neckRig)
from ncmel import headRig
reload(headRig)
from ncmel import eyeRig
reload(eyeRig)
from ncmel import jawRig
reload(jawRig)
from ncmel import clavicleRig
reload(clavicleRig)
from ncmel import armRig
reload(armRig)
from utaTools.utapy import legRig
reload(legRig)
from ncmel import legAnimalRig
reload(legAnimalRig)
from utaTools.utapy import fingerRig
reload(fingerRig)
from ncmel import fingerRig as toeRig
reload(toeRig)
from ncmel import subRig
reload(subRig)
from ncmel import fkRig
reload(fkRig)
from ncmel import tailRig
reload(tailRig)
from ncmel import mouthRig
reload(mouthRig)
from ncmel import ribbonRig
reload(ribbonRig)
from ncscript.rig import angleTools as agt
reload( agt )
from utaTools.utapy import utaCore
reload(utaCore)
from utaTools.pkmel import ctrlShapeTools
reload(ctrlShapeTools)
from lpRig import rubberBandRig
reload(rubberBandRig)
from utaTools.utapy import charTextName
reload(charTextName)
from nuTools.rigTools import proc
reload(proc)

def main( size = 0.75 ):
	
	print "# Generate >> Main Group"
	main = mainRig.Run( 
									assetName = '' ,
									size      = size
					   )
	print "##------------------------------------------------------------------------------------"
	print "##-----------------------------MAIN GROUP---------------------------------------------"
	print "##------------------------------------------------------------------------------------"


	print "# Generate >> Root"
	root = rootRig.Run( rootTmpJnt = 'Root_TmpJnt' ,
						ctrlGrp    =  main.mainCtrlGrp ,
						skinGrp    =  main.skinGrp ,
						size       =  size )

	print "# Generate >> Spine"
	spine = fkRig.Run(	name       = 'Spine' ,
						tmpJnt     = ["Spine1_TmpJnt", 
									  "Spine2_TmpJnt", 
									  "Spine3_TmpJnt", 
									  "Spine4_TmpJnt"] ,
						parent     = root.rootJnt ,
						ctrlGrp    = main.mainCtrlGrp , 
						axis       = 'y' ,
						shape      = 'circle' ,
						side       = '' ,
						size       = size )

	print "# Generate >> Pelvis"
	pelvis = pelvisRig.Run(	pelvisTmpJnt = 'Pelvis_TmpJnt' , 
	                        parent       = root.rootJnt ,
	                        ctrlGrp      = main.mainCtrlGrp , 
	                        skinGrp      = main.skinGrp , 
	                        size         = size )

	print "# Generate >> Neck"
	neck = neckRig.Run( neckTmpJnt = 'Neck_TmpJnt' ,
						headTmpJnt = 'Head_TmpJnt' ,
						parent     =  spine.fkPosiArray[-1] ,
						ctrlGrp    =  main.mainCtrlGrp ,
						skinGrp    =  main.skinGrp ,
						elem       = '' ,
						side       = '' ,
						ribbon     = False ,
						head       = True ,
						axis       = 'y' ,
						size       = size )

	print "# Generate >> Head"
	head = headRig.Run( headTmpJnt      = 'Head_TmpJnt' ,
						headEndTmpJnt   = 'HeadEnd_TmpJnt' ,
						eyeTrgTmpJnt    = 'EyeTrgt_TmpJnt' ,
						eyeLftTrgTmpJnt = 'EyeTrgt_L_TmpJnt' ,
						eyeRgtTrgTmpJnt = 'EyeTrgt_R_TmpJnt' ,
						eyeLftTmpJnt    = 'Eye_L_TmpJnt' ,
						eyeRgtTmpJnt    = 'Eye_R_TmpJnt' ,
						jawUpr1TmpJnt   = 'JawUpr1_TmpJnt' ,
						jawUprEndTmpJnt = 'JawUprEnd_TmpJnt' ,
						jawLwr1TmpJnt   = 'JawLwr1_TmpJnt' ,
						jawLwr2TmpJnt   = 'JawLwr2_TmpJnt' ,
						jawLwrEndTmpJnt = 'JawLwrEnd_TmpJnt' ,
						parent          =  neck.neckEndJnt ,
						ctrlGrp         =  main.mainCtrlGrp ,
						skinGrp         =  main.skinGrp ,
						elem            = '' ,
						side            = '' ,
						eyeRig          = False ,
						jawRig          = False ,
						size            = size )

	print "# Generate >> Clavicle Left"
	clavL = clavicleRig.Run(    clavTmpJnt  = 'Clav_L_TmpJnt' ,
								upArmTmpJnt = 'UpArm_L_TmpJnt' ,
								parent      =  spine.fkPosiArray[-1] ,
								ctrlGrp     =  main.mainCtrlGrp ,
								jntGrp      =  main.jntGrp ,
								skinGrp     =  main.skinGrp ,
								elem        = '' ,
								side        = 'L' ,
								axis        = 'y' ,
								size        = size )
	
	print "# Generate >> Clavicle Right"
	clavR = clavicleRig.Run(    clavTmpJnt  = 'Clav_R_TmpJnt' ,
								upArmTmpJnt = 'UpArm_R_TmpJnt' ,
								parent      =  spine.fkPosiArray[-1] ,
								ctrlGrp     =  main.mainCtrlGrp ,
								jntGrp      =  main.jntGrp ,
								skinGrp     =  main.skinGrp ,
								elem        = '' ,
								side        = 'R' ,
								axis        = 'y' ,
								size        = size )

	print "# Generate >> Arm Left"
	armL = armRig.Run(  upArmTmpJnt   = 'UpArm_L_TmpJnt' ,
						forearmTmpJnt = 'Forearm_L_TmpJnt' ,
						wristTmpJnt   = 'Wrist_L_TmpJnt' ,
						handTmpJnt    = 'Hand_L_TmpJnt' ,
						elbowTmpJnt   = 'Elbow_L_TmpJnt' ,
						parent        = 'ClavEnd_L_Jnt' , 
						ctrlGrp       =  main.mainCtrlGrp ,
						jntGrp        =  main.jntGrp ,
						skinGrp       =  main.skinGrp ,
						stillGrp      =  main.stillGrp ,
						ikhGrp        =  main.ikhGrp ,
						elem          = '' ,
						side          = 'L' ,
						ribbon        = True ,
						hi			  = False ,
						size          = size )

	rigTools.addMoreSpace( obj = armL.wristIkLocWor ,
						   spaces = [ ('Shoulder' , armL.wristIkLocWor['localSpace'] ) ,
									  ('Head', head.headJnt ) ,
									  ('Chest', spine.fkPosiArray[-1] ) ,
									  ('Pelvis', pelvis.pelvisJnt ) ] )
	
	print "# Generate >> Arm Right"
	armR = armRig.Run(  upArmTmpJnt   = 'UpArm_R_TmpJnt' ,
						forearmTmpJnt = 'Forearm_R_TmpJnt' ,
						wristTmpJnt   = 'Wrist_R_TmpJnt' ,
						handTmpJnt    = 'Hand_R_TmpJnt' ,
						elbowTmpJnt   = 'Elbow_R_TmpJnt' ,
						parent        = 'ClavEnd_R_Jnt' , 
						ctrlGrp       =  main.mainCtrlGrp ,
						jntGrp        =  main.jntGrp ,
						skinGrp       =  main.skinGrp ,
						stillGrp      =  main.stillGrp ,
						ikhGrp        =  main.ikhGrp ,
						elem          = '' ,
						side          = 'R' ,
						ribbon        = True ,
						hi			  = False ,
						size          = size )

	rigTools.addMoreSpace( obj = armR.wristIkLocWor ,
						   spaces = [ ('Shoulder' , armR.wristIkLocWor['localSpace'] ) ,
									  ('Head', head.headJnt ) ,
									  ('Chest', spine.fkPosiArray[-1] ) ,
									  ('Pelvis', pelvis.pelvisJnt ) ] )

	print "# Generate >> Leg Left"
	legL = legRig.Run(  upLegTmpJnt   = 'UpLeg_L_TmpJnt' ,
						lowLegTmpJnt  = 'LowLeg_L_TmpJnt' ,
						ankleTmpJnt   = 'Ankle_L_TmpJnt' ,
						ballTmpJnt    = 'Ball_L_TmpJnt' ,
						toeTmpJnt     = 'Toe_L_TmpJnt' ,
						heelTmpJnt    = 'Heel_L_TmpJnt' ,
						footInTmpJnt  = 'FootIn_L_TmpJnt' ,
						footOutTmpJnt = 'FootOut_L_TmpJnt' ,
						kneeTmpJnt    = 'Knee_L_TmpJnt' ,
						heelIkTwistPivTmpJnt = 'HeelIkTwistPiv_L_TmpJnt',
						parent        =  pelvis.pelvisJnt , 
						ctrlGrp       =  main.mainCtrlGrp ,
						jntGrp        =  main.jntGrp ,
						skinGrp       =  main.skinGrp ,
						stillGrp      =  main.stillGrp ,
						ikhGrp        =  main.ikhGrp ,
						elem          = '' ,
						side          = 'L' ,
						ribbon        = True ,
						hi			  = False ,
						foot          = True ,
						kneePos       = 'Knee_L_TmpJnt' ,
						size          = size )
	
	rigTools.addMoreSpace( obj = legL.kneeIkLocWor ,
						   spaces = [ ('Ankle' , legL.kneeIkLocWor['localSpace'] ) ,
						   			  ('Pelvis', pelvis.pelvisJnt ) ] )

	print "# Generate >> Leg Right"
	legR = legRig.Run(  upLegTmpJnt   = 'UpLeg_R_TmpJnt' ,
						lowLegTmpJnt  = 'LowLeg_R_TmpJnt' ,
						ankleTmpJnt   = 'Ankle_R_TmpJnt' ,
						ballTmpJnt    = 'Ball_R_TmpJnt' ,
						toeTmpJnt     = 'Toe_R_TmpJnt' ,
						heelTmpJnt    = 'Heel_R_TmpJnt' ,
						footInTmpJnt  = 'FootIn_R_TmpJnt' ,
						footOutTmpJnt = 'FootOut_R_TmpJnt' ,
						kneeTmpJnt    = 'Knee_R_TmpJnt' ,
						heelIkTwistPivTmpJnt = 'HeelIkTwistPiv_R_TmpJnt',
						parent        =  pelvis.pelvisJnt , 
						ctrlGrp       =  main.mainCtrlGrp ,
						jntGrp        =  main.jntGrp ,
						skinGrp       =  main.skinGrp ,
						stillGrp      =  main.stillGrp ,
						ikhGrp        =  main.ikhGrp ,
						elem          = '' ,
						side          = 'R' ,
						ribbon        = True ,
						hi			  = False ,
						foot          = True ,
						kneePos       = 'Knee_R_TmpJnt' ,
						size          = size )

	rigTools.addMoreSpace( obj = legR.kneeIkLocWor ,
						   spaces = [ ('Ankle' , legR.kneeIkLocWor['localSpace'] ) ,
						   			  ('Pelvis', pelvis.pelvisJnt ) ,
									  ('Root', root.rootJnt ) ] )

	print "# Generate >> Hand Cup Inner Left"
	handCupInnerL = fingerRig.HandCup( name       = 'HandCupInner' ,
									handCupTmpJnt = 'HandCupInner_L_TmpJnt' ,
									armCtrl       = armL.armCtrl ,
									parent        = armL.handJnt ,
									elem          = '' ,
									side          = 'L' )

	print "# Generate >> Hand Cup Outer Left"
	handCupOuterL = fingerRig.HandCup( 	name       = 'HandCupOuter' ,
										handCupTmpJnt = 'HandCupOuter_L_TmpJnt' ,
										armCtrl       = armL.armCtrl ,
										parent        = armL.handJnt ,
										elem          = '' ,
										side          = 'L' )

	print "# Generate >> Thumb Left"
	fngrThumbL = fingerRig.Run( fngr          = 'Thumb' ,
								fngrTmpJnt    =['Thumb1_L_TmpJnt' , 
												'Thumb2_L_TmpJnt' , 
												'Thumb3_L_TmpJnt' , 
												'Thumb4_L_TmpJnt' ] ,
								parent        =  armL.handJnt ,
								armCtrl       =  armL.armCtrl ,
								innerCup      = armL.handJnt , 
								outerCup      = '' ,
								innerValue    = 1 , 
								outerValue    = 0 ,
								ctrlGrp       =  main.mainCtrlGrp ,
								elem          = '' ,
								side          = 'L' ,
								size          = size )

	finger = 'Thumb'
	ctrlShape = core.Dag(armL.armCtrl.shape)
	ctrlShape.attr('%s2FistRx' %finger).value = -4.5
	ctrlShape.attr('%s3FistRx' %finger).value = -9
	ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
	ctrlShape.attr('%s3SlideRx' %finger).value = -8
	ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
	ctrlShape.attr('%s3ScruchRx' %finger).value = -8

	print "# Generate >> Index Left"
	fngrIndexL = fingerRig.Run( fngr          = 'Index' ,
								fngrTmpJnt    =['Index1_L_TmpJnt' , 
												'Index2_L_TmpJnt' , 
												'Index3_L_TmpJnt' , 
												'Index4_L_TmpJnt' , 
												'Index5_L_TmpJnt' ] ,
								parent        =  handCupInnerL.handCupJnt ,
								armCtrl       =  armL.armCtrl ,
								innerCup      = handCupInnerL.handCupJnt , 
								outerCup      = handCupOuterL.handCupJnt ,
								innerValue    = 1 , 
								outerValue    = 0 ,
								ctrlGrp       =  main.mainCtrlGrp ,
								elem          = '' ,
								side          = 'L' ,
								size          = size )

	finger = 'Index'
	ctrlShape = core.Dag(armL.armCtrl.shape)
	ctrlShape.attr('%s2FistRx' %finger).value = -9
	ctrlShape.attr('%s3FistRx' %finger).value = -9
	ctrlShape.attr('%s4FistRx' %finger).value = -9
	ctrlShape.attr('%s2RelaxRx' %finger).value = -1
	ctrlShape.attr('%s3RelaxRx' %finger).value = -1
	ctrlShape.attr('%s4RelaxRx' %finger).value = -1
	ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
	ctrlShape.attr('%s3SlideRx' %finger).value = -8
	ctrlShape.attr('%s4SlideRx' %finger).value = 4.5
	ctrlShape.attr('%s1ScruchRx' %finger).value = 1.5
	ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
	ctrlShape.attr('%s3ScruchRx' %finger).value = -8
	ctrlShape.attr('%s4ScruchRx' %finger).value = -8
	ctrlShape.attr('%sBaseSpread' %finger).value = -2
	ctrlShape.attr('%sSpread' %finger).value = -2
	ctrlShape.attr('%sBaseBreak' %finger).value = -4.5
	ctrlShape.attr('%sBreak' %finger).value = -4.5
	ctrlShape.attr('%sBaseFlex' %finger).value = -9
	ctrlShape.attr('%sFlex' %finger).value = -9

	print "# Generate >> Middle Left"
	fngrMiddleL = fingerRig.Run(	fngr          = 'Middle' ,
									fngrTmpJnt    =['Middle1_L_TmpJnt' , 
													'Middle2_L_TmpJnt' , 
													'Middle3_L_TmpJnt' , 
													'Middle4_L_TmpJnt' , 
													'Middle5_L_TmpJnt' ] ,
									parent        =  handCupInnerL.handCupJnt ,
									armCtrl       =  armL.armCtrl ,
									innerCup      = handCupInnerL.handCupJnt , 
									outerCup      = handCupOuterL.handCupJnt ,
									innerValue    = 1 , 
									outerValue    = 0.5 ,
									ctrlGrp       =  main.mainCtrlGrp ,
									elem          = '' ,
									side          = 'L' ,
									size          = size )

	finger = 'Middle'
	ctrlShape = core.Dag(armL.armCtrl.shape)
	ctrlShape.attr('%s2FistRx' %finger).value = -9
	ctrlShape.attr('%s3FistRx' %finger).value = -9
	ctrlShape.attr('%s4FistRx' %finger).value = -9
	ctrlShape.attr('%s1RelaxRx' %finger).value = -0.5
	ctrlShape.attr('%s2RelaxRx' %finger).value = -2
	ctrlShape.attr('%s3RelaxRx' %finger).value = -2
	ctrlShape.attr('%s4RelaxRx' %finger).value = -2
	ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
	ctrlShape.attr('%s3SlideRx' %finger).value = -8
	ctrlShape.attr('%s4SlideRx' %finger).value = 4.5
	ctrlShape.attr('%s1ScruchRx' %finger).value = 1.5
	ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
	ctrlShape.attr('%s3ScruchRx' %finger).value = -8
	ctrlShape.attr('%s4ScruchRx' %finger).value = -8
	ctrlShape.attr('%sBaseSpread' %finger).value = 0
	ctrlShape.attr('%sSpread' %finger).value = 0
	ctrlShape.attr('%sBaseBreak' %finger).value = -4.5
	ctrlShape.attr('%sBreak' %finger).value = -4.5
	ctrlShape.attr('%sBaseFlex' %finger).value = -9
	ctrlShape.attr('%sFlex' %finger).value = -9

	print "# Generate >> Ring Left"
	fngrRingL = fingerRig.Run(  fngr          = 'Ring' ,
								fngrTmpJnt    =['Ring1_L_TmpJnt' , 
												'Ring2_L_TmpJnt' , 
												'Ring3_L_TmpJnt' , 
												'Ring4_L_TmpJnt' , 
												'Ring5_L_TmpJnt' ] ,
								parent        =  handCupOuterL.handCupJnt ,
								armCtrl       =  armL.armCtrl ,
								innerCup      = handCupInnerL.handCupJnt , 
								outerCup      = handCupOuterL.handCupJnt ,
								innerValue    = 0.5 , 
								outerValue    = 1 ,
								ctrlGrp       =  main.mainCtrlGrp ,
								elem          = '' ,
								side          = 'L' ,
								size          = size )

	finger = 'Ring'
	ctrlShape = core.Dag(armL.armCtrl.shape)
	ctrlShape.attr('%s2FistRx' %finger).value = -9
	ctrlShape.attr('%s3FistRx' %finger).value = -9
	ctrlShape.attr('%s4FistRx' %finger).value = -9
	ctrlShape.attr('%s1RelaxRx' %finger).value = -1
	ctrlShape.attr('%s2RelaxRx' %finger).value = -3
	ctrlShape.attr('%s3RelaxRx' %finger).value = -3
	ctrlShape.attr('%s4RelaxRx' %finger).value = -3
	ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
	ctrlShape.attr('%s3SlideRx' %finger).value = -8
	ctrlShape.attr('%s4SlideRx' %finger).value = 4.5
	ctrlShape.attr('%s1ScruchRx' %finger).value = 1.5
	ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
	ctrlShape.attr('%s3ScruchRx' %finger).value = -8
	ctrlShape.attr('%s4ScruchRx' %finger).value = -8
	ctrlShape.attr('%sBaseSpread' %finger).value = 2
	ctrlShape.attr('%sSpread' %finger).value = 2
	ctrlShape.attr('%sBaseBreak' %finger).value = -4.5
	ctrlShape.attr('%sBreak' %finger).value = -4.5
	ctrlShape.attr('%sBaseFlex' %finger).value = -9
	ctrlShape.attr('%sFlex' %finger).value = -9

	print "# Generate >> Pinky Left"
	fngrPinkyL = fingerRig.Run( fngr          = 'Pinky' ,
								fngrTmpJnt    =['Pinky1_L_TmpJnt' , 
												'Pinky2_L_TmpJnt' , 
												'Pinky3_L_TmpJnt' , 
												'Pinky4_L_TmpJnt' , 
												'Pinky5_L_TmpJnt' ] ,
								parent        =  handCupOuterL.handCupJnt ,
								armCtrl       =  armL.armCtrl ,
								innerCup      = handCupInnerL.handCupJnt , 
								outerCup      = handCupOuterL.handCupJnt ,
								innerValue    = 0 , 
								outerValue    = 1 ,
								ctrlGrp       =  main.mainCtrlGrp ,
								elem          = '' ,
								side          = 'L' ,
								size          = size )

	finger = 'Pinky'
	ctrlShape = core.Dag(armL.armCtrl.shape)
	ctrlShape.attr('%s2FistRx' %finger).value = -9
	ctrlShape.attr('%s3FistRx' %finger).value = -9
	ctrlShape.attr('%s4FistRx' %finger).value = -9
	ctrlShape.attr('%s1RelaxRx' %finger).value = -1.5
	ctrlShape.attr('%s2RelaxRx' %finger).value = -4
	ctrlShape.attr('%s3RelaxRx' %finger).value = -4
	ctrlShape.attr('%s4RelaxRx' %finger).value = -4
	ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
	ctrlShape.attr('%s3SlideRx' %finger).value = -8
	ctrlShape.attr('%s4SlideRx' %finger).value = 4.5
	ctrlShape.attr('%s1ScruchRx' %finger).value = 1.5
	ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
	ctrlShape.attr('%s3ScruchRx' %finger).value = -8
	ctrlShape.attr('%s4ScruchRx' %finger).value = -8
	ctrlShape.attr('%sBaseSpread' %finger).value = 4
	ctrlShape.attr('%sSpread' %finger).value = 4
	ctrlShape.attr('%sBaseBreak' %finger).value = -4.5
	ctrlShape.attr('%sBreak' %finger).value = -4.5
	ctrlShape.attr('%sBaseFlex' %finger).value = -9
	ctrlShape.attr('%sFlex' %finger).value = -9

	print "# Generate >> Hand Cup Inner Right"
	handCupInnerR = fingerRig.HandCup(	 name       = 'HandCupInner' ,
										handCupTmpJnt = 'HandCupInner_R_TmpJnt' ,
										armCtrl       = armR.armCtrl ,
										parent        = armR.handJnt ,
										elem          = '' ,
										side          = 'R' )
								
	print "# Generate >> Hand Cup Outer Right"
	handCupOuterR = fingerRig.HandCup( 	name       = 'HandCupOuter' ,
										handCupTmpJnt = 'HandCupOuter_R_TmpJnt' ,
										armCtrl       = armR.armCtrl ,
										parent        = armR.handJnt ,
										elem          = '' ,
										side          = 'R' )

	print "# Generate >> Thumb Right"
	fngrThumbR = fingerRig.Run( fngr          = 'Thumb' ,
								fngrTmpJnt    =['Thumb1_R_TmpJnt' , 
												'Thumb2_R_TmpJnt' , 
												'Thumb3_R_TmpJnt' , 
												'Thumb4_R_TmpJnt' ] ,
								parent        =  armR.handJnt ,
								armCtrl       =  armR.armCtrl ,
								innerCup      = armR.handJnt , 
								outerCup      = '' ,
								innerValue    = 1 , 
								outerValue    = 0 ,
								ctrlGrp       =  main.mainCtrlGrp ,
								elem          = '' ,
								side          = 'R' ,
								size          = size )


	finger = 'Thumb'
	ctrlShape = core.Dag(armR.armCtrl.shape)
	ctrlShape.attr('%s2FistRx' %finger).value = -4.5
	ctrlShape.attr('%s3FistRx' %finger).value = -9
	ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
	ctrlShape.attr('%s3SlideRx' %finger).value = -8
	ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
	ctrlShape.attr('%s3ScruchRx' %finger).value = -8

	print "# Generate >> Index Right"
	fngrIndexL = fingerRig.Run( fngr          = 'Index' ,
								fngrTmpJnt    =['Index1_R_TmpJnt' , 
												'Index2_R_TmpJnt' , 
												'Index3_R_TmpJnt' , 
												'Index4_R_TmpJnt' , 
												'Index5_R_TmpJnt' ] ,
								parent        =  handCupInnerR.handCupJnt ,
								armCtrl       =  armR.armCtrl ,
								innerCup      = handCupInnerR.handCupJnt , 
								outerCup      = handCupOuterR.handCupJnt ,
								innerValue    = 1 , 
								outerValue    = 0 ,
								ctrlGrp       =  main.mainCtrlGrp ,
								elem          = '' ,
								side          = 'R' ,
								size          = size )

	finger = 'Index'
	ctrlShape = core.Dag(armR.armCtrl.shape)
	ctrlShape.attr('%s2FistRx' %finger).value = -9
	ctrlShape.attr('%s3FistRx' %finger).value = -9
	ctrlShape.attr('%s4FistRx' %finger).value = -9
	ctrlShape.attr('%s2RelaxRx' %finger).value = -1
	ctrlShape.attr('%s3RelaxRx' %finger).value = -1
	ctrlShape.attr('%s4RelaxRx' %finger).value = -1
	ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
	ctrlShape.attr('%s3SlideRx' %finger).value = -8
	ctrlShape.attr('%s4SlideRx' %finger).value = 4.5
	ctrlShape.attr('%s1ScruchRx' %finger).value = 1.5
	ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
	ctrlShape.attr('%s3ScruchRx' %finger).value = -8
	ctrlShape.attr('%s4ScruchRx' %finger).value = -8
	ctrlShape.attr('%sBaseSpread' %finger).value = -2
	ctrlShape.attr('%sSpread' %finger).value = -2
	ctrlShape.attr('%sBaseBreak' %finger).value = -4.5
	ctrlShape.attr('%sBreak' %finger).value = -4.5
	ctrlShape.attr('%sBaseFlex' %finger).value = -9
	ctrlShape.attr('%sFlex' %finger).value = -9

	print "# Generate >> Middle Right"
	fngrMiddleL = fingerRig.Run(    fngr          = 'Middle' ,
									fngrTmpJnt    =['Middle1_R_TmpJnt' , 
													'Middle2_R_TmpJnt' , 
													'Middle3_R_TmpJnt' , 
													'Middle4_R_TmpJnt' , 
													'Middle5_R_TmpJnt' ] ,
									parent        =  handCupInnerR.handCupJnt ,
									armCtrl       =  armR.armCtrl ,
									innerCup      = handCupInnerR.handCupJnt , 
									outerCup      = handCupOuterR.handCupJnt ,
									innerValue    = 1 , 
									outerValue    = 0.5 ,
									ctrlGrp       =  main.mainCtrlGrp ,
									elem          = '' ,
									side          = 'R' ,
									size          = size )

	finger = 'Middle'
	ctrlShape = core.Dag(armR.armCtrl.shape)
	ctrlShape.attr('%s2FistRx' %finger).value = -9
	ctrlShape.attr('%s3FistRx' %finger).value = -9
	ctrlShape.attr('%s4FistRx' %finger).value = -9
	ctrlShape.attr('%s1RelaxRx' %finger).value = -0.5
	ctrlShape.attr('%s2RelaxRx' %finger).value = -2
	ctrlShape.attr('%s3RelaxRx' %finger).value = -2
	ctrlShape.attr('%s4RelaxRx' %finger).value = -2
	ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
	ctrlShape.attr('%s3SlideRx' %finger).value = -8
	ctrlShape.attr('%s4SlideRx' %finger).value = 4.5
	ctrlShape.attr('%s1ScruchRx' %finger).value = 1.5
	ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
	ctrlShape.attr('%s3ScruchRx' %finger).value = -8
	ctrlShape.attr('%s4ScruchRx' %finger).value = -8
	ctrlShape.attr('%sBaseSpread' %finger).value = 0
	ctrlShape.attr('%sSpread' %finger).value = 0
	ctrlShape.attr('%sBaseBreak' %finger).value = -4.5
	ctrlShape.attr('%sBreak' %finger).value = -4.5
	ctrlShape.attr('%sBaseFlex' %finger).value = -9
	ctrlShape.attr('%sFlex' %finger).value = -9

	print "# Generate >> Ring Right"
	fngrRingL = fingerRig.Run(  fngr          = 'Ring' ,
								fngrTmpJnt    =['Ring1_R_TmpJnt' , 
												'Ring2_R_TmpJnt' , 
												'Ring3_R_TmpJnt' , 
												'Ring4_R_TmpJnt' , 
												'Ring5_R_TmpJnt' ] ,
								parent        =  handCupOuterR.handCupJnt ,
								armCtrl       =  armR.armCtrl ,
								innerCup      = handCupInnerR.handCupJnt , 
								outerCup      = handCupOuterR.handCupJnt ,
								innerValue    = 0.5 , 
								outerValue    = 1 ,
								ctrlGrp       =  main.mainCtrlGrp ,
								elem          = '' ,
								side          = 'R' ,
								size          = size )

	finger = 'Ring'
	ctrlShape = core.Dag(armR.armCtrl.shape)
	ctrlShape.attr('%s2FistRx' %finger).value = -9
	ctrlShape.attr('%s3FistRx' %finger).value = -9
	ctrlShape.attr('%s4FistRx' %finger).value = -9
	ctrlShape.attr('%s1RelaxRx' %finger).value = -1
	ctrlShape.attr('%s2RelaxRx' %finger).value = -3
	ctrlShape.attr('%s3RelaxRx' %finger).value = -3
	ctrlShape.attr('%s4RelaxRx' %finger).value = -3
	ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
	ctrlShape.attr('%s3SlideRx' %finger).value = -8
	ctrlShape.attr('%s4SlideRx' %finger).value = 4.5
	ctrlShape.attr('%s1ScruchRx' %finger).value = 1.5
	ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
	ctrlShape.attr('%s3ScruchRx' %finger).value = -8
	ctrlShape.attr('%s4ScruchRx' %finger).value = -8
	ctrlShape.attr('%sBaseSpread' %finger).value = 2
	ctrlShape.attr('%sSpread' %finger).value = 2
	ctrlShape.attr('%sBaseBreak' %finger).value = -4.5
	ctrlShape.attr('%sBreak' %finger).value = -4.5
	ctrlShape.attr('%sBaseFlex' %finger).value = -9
	ctrlShape.attr('%sFlex' %finger).value = -9
	
	print "# Generate >> Pinky Right"
	fngrPinkyL = fingerRig.Run( fngr          = 'Pinky' ,
								fngrTmpJnt    =['Pinky1_R_TmpJnt' , 
												'Pinky2_R_TmpJnt' , 
												'Pinky3_R_TmpJnt' , 
												'Pinky4_R_TmpJnt' , 
												'Pinky5_R_TmpJnt' ] ,
								parent        =  handCupOuterR.handCupJnt ,
								armCtrl       =  armR.armCtrl ,
								innerCup      = handCupInnerR.handCupJnt , 
								outerCup      = handCupOuterR.handCupJnt ,
								innerValue    = 0 , 
								outerValue    = 1 ,
								ctrlGrp       =  main.mainCtrlGrp ,
								elem          = '' ,
								side          = 'R' ,
								size          = size )

	finger = 'Pinky'
	ctrlShape = core.Dag(armR.armCtrl.shape)
	ctrlShape.attr('%s2FistRx' %finger).value = -9
	ctrlShape.attr('%s3FistRx' %finger).value = -9
	ctrlShape.attr('%s4FistRx' %finger).value = -9
	ctrlShape.attr('%s1RelaxRx' %finger).value = -1.5
	ctrlShape.attr('%s2RelaxRx' %finger).value = -4
	ctrlShape.attr('%s3RelaxRx' %finger).value = -4
	ctrlShape.attr('%s4RelaxRx' %finger).value = -4
	ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
	ctrlShape.attr('%s3SlideRx' %finger).value = -8
	ctrlShape.attr('%s4SlideRx' %finger).value = 4.5
	ctrlShape.attr('%s1ScruchRx' %finger).value = 1.5
	ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
	ctrlShape.attr('%s3ScruchRx' %finger).value = -8
	ctrlShape.attr('%s4ScruchRx' %finger).value = -8
	ctrlShape.attr('%sBaseSpread' %finger).value = 4
	ctrlShape.attr('%sSpread' %finger).value = 4
	ctrlShape.attr('%sBaseBreak' %finger).value = -4.5
	ctrlShape.attr('%sBreak' %finger).value = -4.5
	ctrlShape.attr('%sBaseFlex' %finger).value = -9
	ctrlShape.attr('%sFlex' %finger).value = -9

	print '##------------------------------------------------------------------------------------'
	print '##-----------------------------ADD RIG GROUP------------------------------------------'
	print '##------------------------------------------------------------------------------------'






##---------------------------------------set preferred ankle Leg Ik --------------------------------------
##---------------------------------------set preferred ankle Leg Ik --------------------------------------
	print "# Generate >> Set preferred ankle Leg Ik"
	utaCore.preferredAnkleLegIk()

	print '##------------------------------------------------------------------------------------'
	print '##-----------------------------CLEAR UTILITIE-----------------------------------------'
	print '##------------------------------------------------------------------------------------'


	print "# Generate >> Character Text Name"
	charTextName.charTextName(value = '3', size = '1')

	print "# Generate >> Read Control Shape"
	ctrlShapeTools.readAllCtrl()
 

	print "# Generate >> Set Defalut Attribute"
	mc.rotate(90, 0, 0, main.allMoverCtrl.getName() + ".cv[:]")
	mc.rotate(90, 0, 0, main.offsetCtrl.getName() + ".cv[:]")

	print "# Generate >> DONE !! "