//Maya ASCII 2018ff09 scene
//Name: dac_characterTmp.ma
//Last modified: Thu, Oct 08, 2020 02:46:43 PM
//Codeset: 1252
requires maya "2018ff09";
requires "stereoCamera" "10.0";
requires "FurryBall_2012" "4.5.3028";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2018";
fileInfo "version" "2018";
fileInfo "cutIdentifier" "201903222215-65bada0e52";
fileInfo "osv" "Microsoft Windows 8 Business Edition, 64-bit  (Build 9200)\n";
createNode transform -s -n "persp";
	rename -uid "6DDE37FB-4964-D486-A9C5-F19F9AA240AE";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 7.9205674158631778 -150.05860789483719 58.989083406371584 ;
	setAttr ".r" -type "double3" 82.882924915356085 -5.963540027744092e-16 1.9949139947458332 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "D6DBDE3B-40D4-C169-DF23-27985484D4E4";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 164.54193179486083;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "66D16DED-4F4F-E0A3-B28A-35AB284A23E9";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 1000.1 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "029BE57E-42E9-55AC-35CB-8FAEB1487573";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "D1C4BD60-4C24-962B-2D43-93AEE453391A";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 -1000.1 0 ;
	setAttr ".r" -type "double3" 89.999999999999986 0 0 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "9A34BAB1-4FAC-2FB5-259C-AEB4C4C4FEAB";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "62BCB748-4EDE-3088-1E01-1E87CB5DE631";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1 0 0 ;
	setAttr ".r" -type "double3" 90 1.2722218725854067e-14 89.999999999999986 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "9FCF1C15-49C4-BB35-8A49-84BF539F8F10";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "dac_characterTmp:TmpJnt_Grp";
	rename -uid "A97932A6-47FB-6005-2FED-8DB3C006B88D";
	setAttr ".r" -type "double3" 90 0 0 ;
createNode joint -n "dac_characterTmp:Root_TmpJnt" -p "dac_characterTmp:TmpJnt_Grp";
	rename -uid "D7A8FD33-4D4E-212E-6747-7E9FC7C957FE";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".t" -type "double3" 0 49.288391226855424 0.86230907825140646 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 15.73707328056101 0 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Spine1_TmpJnt" -p "dac_characterTmp:Root_TmpJnt";
	rename -uid "736E54D2-4F0C-2B3F-62A7-A8BDD040F4F1";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" 6.1629758220391547e-32 0.53449076522295513 -0.021334489470551721 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 16.462721689908392 7.9790056857053538e-17 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Spine2_TmpJnt" -p "dac_characterTmp:Spine1_TmpJnt";
	rename -uid "7DBEAFE1-46F4-62DD-91DB-FA80AC713FA6";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 1.3989955116028881e-30 4.4085328061042119 0.13418214684623289 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 18.962119031422834 1.1092698799812162e-16 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Spine3_TmpJnt" -p "dac_characterTmp:Spine2_TmpJnt";
	rename -uid "C661F15C-4EF1-D809-224F-2DA7E15D6C4E";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" 1.9105417929250297e-15 4.4085328061042119 0.022627974061622851 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 16.462721689908392 7.9790056857053538e-17 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Spine4_TmpJnt" -p "dac_characterTmp:Spine3_TmpJnt";
	rename -uid "D9138A45-4D6A-2167-CA4F-31B8BD8C3DFB";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" 3.9443045261050599e-31 0 -0.18102379249297001 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 16.462721689908392 7.9790056857053538e-17 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Neck_TmpJnt" -p "dac_characterTmp:Spine4_TmpJnt";
	rename -uid "EF50092A-4655-031F-E3E2-2193C13DE85B";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 2.3874875296513922e-27 6.8506893159121915 -1.3676400950668122 ;
	setAttr ".r" -type "double3" 10.692259411475778 0 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 22.764810214551506 1.2852494451027699e-16 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Head_TmpJnt" -p "dac_characterTmp:Neck_TmpJnt";
	rename -uid "354B58EC-490C-8C51-9334-DD8139B8A630";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" -7.8886090522101181e-31 4.6257579676055371 -0.0083948940736053856 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" -10.692259411475778 0 0 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 25.118764441136726 1.5379363139276727e-16 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:HeadEnd_TmpJnt" -p "dac_characterTmp:Head_TmpJnt";
	rename -uid "5E657F60-4F0D-A0A8-52E0-78B25B619190";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" 0 10.921229326281306 -6.106226635438361e-16 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 27.864768711571326 -1.9234515956468954e-15 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Clav_L_TmpJnt" -p "dac_characterTmp:Spine4_TmpJnt";
	rename -uid "C0D683AC-48B8-E22B-F423-55AE0997A852";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".ove" yes;
	setAttr ".t" -type "double3" 2.1855908371885113 5.0433517847660312 -0.53543728274838509 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" -1.2722218725854064e-14 1.2722218725854067e-14 -89.999999999999986 ;
	setAttr ".bps" -type "matrix" 2.2204460492503131e-16 -1 0 0 1 2.2204460492503131e-16 0 0
		 0 0 1 0 0.40396301701286447 22.628380391977608 0.93191360083425789 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:UpArm_L_TmpJnt" -p "dac_characterTmp:Clav_L_TmpJnt";
	rename -uid "26371ED3-4056-04C4-88F6-CA8A1566A9DF";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 1.7707503465965502 3.3171308040618892 -1.2294923912496056 ;
	setAttr ".r" -type "double3" -4 0 0 ;
	setAttr ".ro" 4;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" 2.2204460492503131e-16 -1 0 0 1 2.2204460492503131e-16 0 0
		 0 0 1 0 2.6840862727483024 21.950819439051291 -0.34347723857962076 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Forearm_L_TmpJnt" -p "dac_characterTmp:UpArm_L_TmpJnt";
	rename -uid "46E0C0CE-4B33-1994-CFFA-04BE1F91290F";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 4.259590014044079e-09 10.921649980943565 0 ;
	setAttr ".r" -type "double3" 8 0 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" -5.9496752756331675e-05 0 0 ;
	setAttr ".bps" -type "matrix" 2.2204460492503131e-16 -1 0 0 0.99999999999946088 2.2204460492491157e-16 -1.0384142298428127e-06 0
		 1.0384142298428127e-06 2.3057427741397801e-22 0.99999999999946088 0 6.7891781465089913 21.950819439060187 -0.42637972484178432 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Wrist_L_TmpJnt" -p "dac_characterTmp:Forearm_L_TmpJnt";
	rename -uid "C2DBB66E-4742-02E9-0202-FA847F416DA6";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" -8.7773344148445176e-11 11.092769625607156 0 ;
	setAttr ".r" -type "double3" -4 1.9801038241349232e-06 4.4995857799076475e-08 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" 2.2204460492503131e-16 -1 0 0 1 2.2204460492503131e-16 8.3960588285190204e-14 0
		 -8.3960588285190204e-14 -1.8642995655058271e-29 1 0 11.145136555499452 21.950819439088733 -0.34347297829513029 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Hand_L_TmpJnt" -p "dac_characterTmp:Wrist_L_TmpJnt";
	rename -uid "79662F48-4EC2-F0F7-A836-589C2DE0AC4C";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" 0 1.3950424457226234 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 2.2204460492503131e-16 -1 0 0 1 2.2204460492503131e-16 8.3960588285190204e-14 0
		 -8.3960588285190204e-14 -1.8642995655058271e-29 1 0 11.94334900537585 21.950819439093973 -0.34347375639834526 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Thumb1_L_TmpJnt" -p "dac_characterTmp:Hand_L_TmpJnt";
	rename -uid "07B5B1E8-48E9-F47B-CADD-C7B671E5BC09";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".t" -type "double3" 1.196014420765394 -0.20968889040514327 1.4027368508151135 ;
	setAttr ".r" -type "double3" 4.4355566940309572 14.036160539675029 14.081816860710981 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" 30.639469948822885 -21.375623757889937 -57.046296358281801 ;
	setAttr ".bps" -type "matrix" -0.59548586752167076 -0.80336578317847407 -4.9997343752636337e-14 0
		 0.73868319246536318 -0.54754062333671982 0.39312390790336815 0 -0.31582229615899904 0.23409973134130962 0.91948550452673483 0
		 11.518645340649305 21.905429719636764 0.32567524419271171 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Thumb2_L_TmpJnt" -p "dac_characterTmp:Thumb1_L_TmpJnt";
	rename -uid "059BEB71-4077-A253-4AA0-A095CBFE745D";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" -9.5258201326942071e-10 1.7077288062045586 0.58019136880025357 ;
	setAttr ".r" -type "double3" 0.41396271739684226 0 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -0.59548586752167076 -0.80336578317847407 -4.9997343752636337e-14 0
		 0.73868319246536318 -0.54754062333671982 0.39312390790336815 0 -0.31582229615899904 0.23409973134130962 0.91948550452673483 0
		 12.026286103962779 21.529146703032435 0.59583939943602238 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Thumb3_L_TmpJnt" -p "dac_characterTmp:Thumb2_L_TmpJnt";
	rename -uid "DFEBD327-440C-D2EB-1FBB-23B20807B856";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 1.1075078631961333e-06 0.81869593337022439 1.5664183159458389e-06 ;
	setAttr ".r" -type "double3" 1.9811204732243901e-05 8.4022342688732486e-06 -1.0449136034575707e-05 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -0.59548586752167076 -0.80336578317847407 -4.9997343752636337e-14 0
		 0.73868319246536318 -0.54754062333671982 0.39312390790336815 0 -0.31582229615899904 0.23409973134130962 0.91948550452673483 0
		 12.724953059079672 21.011267666374362 0.96766684389406166 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Thumb4_L_TmpJnt" -p "dac_characterTmp:Thumb3_L_TmpJnt";
	rename -uid "01C93E79-422D-571B-82BF-A6A8528647A5";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" -9.241176712748711e-10 1.3738945722580007 2.8123281481384765e-10 ;
	setAttr ".r" -type "double3" 1.1729200673035528e-08 2.0051580232284338e-08 3.8535522122013761e-08 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" -0.59548586752167076 -0.80336578317847407 -4.9997343752636337e-14 0
		 0.73868319246536318 -0.54754062333671982 0.39312390790336815 0 -0.31582229615899904 0.23409973134130962 0.91948550452673483 0
		 13.320225968832865 20.570028305053697 1.2844684375111461 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:HandCupInner_L_TmpJnt" -p "dac_characterTmp:Hand_L_TmpJnt";
	rename -uid "8918BBFE-41F6-FBC1-0E36-9EB52BD6D64C";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" 0.18182274050516439 0.34489963278195196 0.63967281332927162 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" 1.3017646500251427 3.1049391554247603e-06 -2.1338492114880605 ;
	setAttr ".bps" -type "matrix" 2.2204460492503131e-16 -1 0 0 1 2.2204460492503131e-16 8.3960588285190204e-14 0
		 -8.3960588285190204e-14 -1.8642995655058271e-29 1 0 11.145136555499452 21.950819439088733 -0.34347297829513029 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Index1_L_TmpJnt" -p "dac_characterTmp:HandCupInner_L_TmpJnt";
	rename -uid "3B325DAE-4BBB-DFF9-51B0-7098D4FA339E";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".t" -type "double3" -0.019190370757286246 1.2899919584822435 0.84103973967157708 ;
	setAttr ".r" -type "double3" 6.0776872350974251 -0.057451793381633344 0.62360691504919974 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" -2.7392527194104522e-13 -89.999999999999986 0 ;
	setAttr ".bps" -type "matrix" -8.3960588285190191e-14 -8.437694987151191e-14 1 0
		 1 2.2204460492503131e-16 8.3960588285190204e-14 0 -2.2204460493211562e-16 1 8.4376949871511897e-14 0
		 11.915961054098767 22.155901831531715 0.36968006095532863 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Index2_L_TmpJnt" -p "dac_characterTmp:Index1_L_TmpJnt";
	rename -uid "DB9047FC-43B5-6952-5785-3890DFBAEFB4";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" 1.1849410341824296e-12 1.6642106650773272 -0.21654579175988431 ;
	setAttr ".r" -type "double3" -4.9966809737460167 0 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-14 -8.437694987151191e-14 1 0
		 1 2.2204460492503131e-16 8.3960588285190204e-14 0 -2.2204460493211562e-16 1 8.4376949871511897e-14 0
		 13.114219270621863 22.155901831531715 0.36968006095542938 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Index3_L_TmpJnt" -p "dac_characterTmp:Index2_L_TmpJnt";
	rename -uid "A1B6144A-4E74-212C-154C-A39A044F613F";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 1.6973089600469393e-12 1.7621448003771469 1.3837535561833647e-09 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-14 -8.437694987151191e-14 1 0
		 1 2.2204460492503131e-16 8.3960588285190204e-14 0 -2.2204460493211562e-16 1 8.4376949871511897e-14 0
		 13.805785441300909 22.155901831531718 0.3696800609554875 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Index4_L_TmpJnt" -p "dac_characterTmp:Index3_L_TmpJnt";
	rename -uid "27FD53D0-4401-77F5-B621-44ABA6D3FAD6";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 3.12441622307702e-06 1.0197286642256387 6.5134706517255836e-08 ;
	setAttr ".r" -type "double3" 0 -2.2255657207179173e-06 -2.3818287790657693e-05 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-14 -8.437694987151191e-14 1 0
		 1 2.2204460492503131e-16 8.3960588285190204e-14 0 -2.2204460493211562e-16 1 8.4376949871511897e-14 0
		 14.271394348292739 22.155901831531718 0.36968006095552658 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Index5_L_TmpJnt" -p "dac_characterTmp:Index4_L_TmpJnt";
	rename -uid "140A9516-4D2F-86F6-D6B4-AC843F03E944";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 1.1557976797860192e-12 1.201886534690864 9.4387786475635949e-10 ;
	setAttr ".r" -type "double3" -9.2540546237594936e-13 5.3162927997018629e-10 -5.5147836391628063e-11 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" -8.3960588285190191e-14 -8.437694987151191e-14 1 0
		 1 2.2204460492503131e-16 8.3960588285190204e-14 0 -2.2204460493211562e-16 1 8.4376949871511897e-14 0
		 14.812322343180313 22.155901831531722 0.36968006095557193 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Middle1_L_TmpJnt" -p "dac_characterTmp:HandCupInner_L_TmpJnt";
	rename -uid "A69B9B7B-4462-3BB0-4A37-708752F53DCB";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".t" -type "double3" -0.18162448661457375 1.2650723095483833 -0.21107205420049124 ;
	setAttr ".r" -type "double3" 4.6980854431389023 0.4935833473055235 1.8599270689119174 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" -1.9003814221744514e-13 -89.999999999995183 0 ;
	setAttr ".bps" -type "matrix" -8.3960588285190191e-14 -8.437694987151191e-14 1 0
		 1 2.2204460492503131e-16 8.3960588285190204e-14 0 -2.2204460493211562e-16 1 8.4376949871511897e-14 0
		 11.876633548480955 22.169748485139639 -0.10080869585328409 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Middle2_L_TmpJnt" -p "dac_characterTmp:Middle1_L_TmpJnt";
	rename -uid "6D9B4BC6-40B8-39E7-DC03-1BBFFA610D20";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" -3.4427863104036227e-05 1.6780271159623439 -0.2027097639212343 ;
	setAttr ".r" -type "double3" -4.7286055832585197 -3.7229239363060597e-06 -2.3630146386096708e-05 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-14 -8.437694987151191e-14 1 0
		 1 2.2204460492503131e-16 8.3960588285190204e-14 0 -2.2204460493211562e-16 1 8.4376949871511897e-14 0
		 13.108601055533605 22.169748485139639 -0.10080869585318036 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Middle3_L_TmpJnt" -p "dac_characterTmp:Middle2_L_TmpJnt";
	rename -uid "73852C95-4D1C-F321-F10B-A69EF5E9056B";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 3.6381112241801183e-05 1.9377645788637849 -9.9475983006414026e-14 ;
	setAttr ".r" -type "double3" -7.3358776270297159e-13 -4.385389547182396e-10 3.8737297385318759e-10 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-14 -8.437694987151191e-14 1 0
		 1 2.2204460492503131e-16 8.3960588285190204e-14 0 -2.2204460493211562e-16 1 8.4376949871511897e-14 0
		 13.906913312889571 22.169748485139639 -0.10080869585311319 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Middle4_L_TmpJnt" -p "dac_characterTmp:Middle3_L_TmpJnt";
	rename -uid "52FAEE1B-4C0C-D00E-C937-6AB2605C7DEB";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" -7.7514661356303804e-12 1.1467923671346298 7.1054273576010019e-14 ;
	setAttr ".r" -type "double3" -7.3358776270297179e-13 -4.3853915350290709e-10 3.8737297385318759e-10 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-14 -8.437694987151191e-14 1 0
		 1 2.2204460492503131e-16 8.3960588285190204e-14 0 -2.2204460493211562e-16 1 8.4376949871511897e-14 0
		 14.411849725499213 22.169748485139639 -0.10080869585307067 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Middle5_L_TmpJnt" -p "dac_characterTmp:Middle4_L_TmpJnt";
	rename -uid "ADB8349F-4052-29E5-4C0B-99BA35097F72";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" -9.077849583150055e-12 1.3423327207565237 -3.5527136788005009e-14 ;
	setAttr ".r" -type "double3" -7.3358776270297189e-13 -4.3853935228757467e-10 3.8737297385318759e-10 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" -8.3960588285190191e-14 -8.437694987151191e-14 1 0
		 1 2.2204460492503131e-16 8.3960588285190204e-14 0 -2.2204460493211562e-16 1 8.4376949871511897e-14 0
		 15.081996667416741 22.169748485139635 -0.10080869585301426 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:HandCupOuter_L_TmpJnt" -p "dac_characterTmp:Hand_L_TmpJnt";
	rename -uid "C3C869E4-4236-E6EA-97C5-B89509F827C7";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" 0.18182278245100747 0.34490089621799314 -0.5770247320968922 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" 1.3017646500251427 3.1049391554255696e-06 -2.1338492114880583 ;
	setAttr ".bps" -type "matrix" 2.2204460492503131e-16 -1 0 0 1 2.2204460492503131e-16 8.3960588285190204e-14 0
		 -8.3960588285190204e-14 -1.8642995655058271e-29 1 0 11.145136555499452 21.950819439088733 -0.34347297829513029 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Ring1_L_TmpJnt" -p "dac_characterTmp:HandCupOuter_L_TmpJnt";
	rename -uid "38E4C809-4970-0647-E0BA-208AF25B405E";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".t" -type "double3" -0.2222435606747428 1.1481143539233849 -0.054928953329502428 ;
	setAttr ".r" -type "double3" 1.7607815396797539 0.054544603401168332 2.6276658961730472 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" -6.3611093628984145e-15 -90 0 ;
	setAttr ".bps" -type "matrix" -8.3960588285190191e-14 -8.437694987151191e-14 1 0
		 1 2.2204460492503131e-16 8.3960588285190204e-14 0 -2.2204460493211562e-16 1 8.4376949871511897e-14 0
		 11.876633548480955 22.13656260093245 -0.5683912766476541 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Ring2_L_TmpJnt" -p "dac_characterTmp:Ring1_L_TmpJnt";
	rename -uid "36DA4D32-4B90-4868-828C-A881CA329848";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" 6.106226635438361e-15 1.5727920057135201 -0.24916185151803205 ;
	setAttr ".r" -type "double3" -0.27026140853818587 2.6654153335147113e-14 -4.6737559059409608e-15 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-14 -8.437694987151191e-14 1 0
		 1 2.2204460492503131e-16 8.3960588285190204e-14 0 -2.2204460493211562e-16 1 8.4376949871511897e-14 0
		 13.097480078235414 22.13656260093245 -0.56839127664755129 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Ring3_L_TmpJnt" -p "dac_characterTmp:Ring2_L_TmpJnt";
	rename -uid "551AE4BC-4B29-6CC7-2775-A9ADC5A60A92";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 0.00011607904546817593 1.9646705621418228 -0.054117892908131182 ;
	setAttr ".r" -type "double3" -9.9451421645007478e-13 2.6711481998701152e-14 -4.3732358437187842e-15 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-14 -8.437694987151191e-14 1 0
		 1 2.2204460492503131e-16 8.3960588285190204e-14 0 -2.2204460493211562e-16 1 8.4376949871511897e-14 0
		 13.845747937749518 22.136562600932457 -0.56839127664748834 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Ring4_L_TmpJnt" -p "dac_characterTmp:Ring3_L_TmpJnt";
	rename -uid "8DA5491F-4DE3-CF2F-5DA0-8EA422264861";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 4.1078251911130792e-15 1.0127946545815476 5.6843418860808015e-14 ;
	setAttr ".r" -type "double3" -9.9451421644176904e-13 2.673633008215009e-14 -4.1744511761273166e-15 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-14 -8.437694987151191e-14 1 0
		 1 2.2204460492503131e-16 8.3960588285190204e-14 0 -2.2204460493211562e-16 1 8.4376949871511897e-14 0
		 14.345123861710062 22.136562600932454 -0.56839127664744615 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Ring5_L_TmpJnt" -p "dac_characterTmp:Ring4_L_TmpJnt";
	rename -uid "E2A9B066-404F-309E-BDDB-EB9B987439DE";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 1.8873791418627661e-15 1.1668307781219482 -5.6843418860808015e-14 ;
	setAttr ".r" -type "double3" -9.9451421643346311e-13 2.6748754123874563e-14 -3.9756665085358466e-15 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" -8.3960588285190191e-14 -8.437694987151191e-14 1 0
		 1 2.2204460492503131e-16 8.3960588285190204e-14 0 -2.2204460493211562e-16 1 8.4376949871511897e-14 0
		 14.876258587400191 22.136562600932457 -0.56839127664740152 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Pinky1_L_TmpJnt" -p "dac_characterTmp:HandCupOuter_L_TmpJnt";
	rename -uid "B402012B-46A9-C88F-CEE9-6EB5592930C8";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".t" -type "double3" -0.12454709503517061 0.79773167123749578 -0.88581870330070622 ;
	setAttr ".r" -type "double3" 0.71347204496072636 0.022145375705238547 4.487030944693176 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" -6.3611093628984145e-15 -90 0 ;
	setAttr ".bps" -type "matrix" -8.3960588285190191e-14 -8.437694987151191e-14 1 0
		 1 2.2204460492503131e-16 8.3960588285190204e-14 0 -2.2204460493211562e-16 1 8.4376949871511897e-14 0
		 11.876633548480955 22.020988418911262 -0.95907086085240945 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Pinky2_L_TmpJnt" -p "dac_characterTmp:Pinky1_L_TmpJnt";
	rename -uid "0F4C8692-466B-8004-5B8A-F79B58247804";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" 1.7207951171771541e-06 1.5112030625085353 -0.21184571123419005 ;
	setAttr ".r" -type "double3" -1.2149715034897679 -2.0673978309848345e-06 -2.3832223712996636e-05 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-14 -8.437694987151191e-14 1 0
		 1 2.2204460492503131e-16 8.3960588285190204e-14 0 -2.2204460493211562e-16 1 8.4376949871511897e-14 0
		 13.083100380346423 22.020988418911262 -0.95907086085230775 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Pinky3_L_TmpJnt" -p "dac_characterTmp:Pinky2_L_TmpJnt";
	rename -uid "07209220-41D0-F37B-9685-C68E1C01E7E1";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 1.3311574065255627e-13 1.6513207971815014 -6.3948846218409017e-14 ;
	setAttr ".r" -type "double3" -6.4955687390626904e-13 -2.5002336034993868e-09 -4.5243390315822371e-12 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-14 -8.437694987151191e-14 1 0
		 1 2.2204460492503131e-16 8.3960588285190204e-14 0 -2.2204460493211562e-16 1 8.4376949871511897e-14 0
		 13.601293073636691 22.020988418911262 -0.95907086085226423 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Pinky4_L_TmpJnt" -p "dac_characterTmp:Pinky3_L_TmpJnt";
	rename -uid "4C929301-4EF5-B058-4305-0694863ABB1F";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 6.8056671409522096e-14 0.83508248766883852 -1.4210854715202004e-13 ;
	setAttr ".r" -type "double3" -6.4955687389545067e-13 -2.5002335786513031e-09 -4.5239414622470542e-12 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-14 -8.437694987151191e-14 1 0
		 1 2.2204460492503131e-16 8.3960588285190204e-14 0 -2.2204460493211562e-16 1 8.4376949871511897e-14 0
		 13.978441565540829 22.020988418911262 -0.95907086085223248 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Pinky5_L_TmpJnt" -p "dac_characterTmp:Pinky4_L_TmpJnt";
	rename -uid "BA5058AA-46BB-7CEF-7A6E-25A47608E0F3";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 8.22675261247241e-14 1.0171070098877131 -1.4210854715202004e-14 ;
	setAttr ".r" -type "double3" -6.4955687388463239e-13 -2.5002335786513031e-09 -4.5235438929118713e-12 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" -8.3960588285190191e-14 -8.437694987151191e-14 1 0
		 1 2.2204460492503131e-16 8.3960588285190204e-14 0 -2.2204460493211562e-16 1 8.4376949871511897e-14 0
		 14.437677801786013 22.020988418911262 -0.95907086085219373 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Elbow_L_TmpJnt" -p "dac_characterTmp:Forearm_L_TmpJnt";
	rename -uid "6CF0F407-4206-9FD8-4D78-97BAAF6239ED";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 1.4210854715202004e-14 -0.93655545911426685 -13.393734519204605 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" 1.3802597105193982e-15 -3.9999405032472461 -90.000000000000028 ;
	setAttr ".bps" -type "matrix" -1 -1.2246467991473532e-16 0 0 1.2246467991473532e-16 -1 0 0
		 0 0 1 0 1.3486489690923307 14.656585133659888 1.5144370407379352e-17 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Clav_R_TmpJnt" -p "dac_characterTmp:Spine4_TmpJnt";
	rename -uid "FCC7ED7F-4389-C8FB-308F-0EBB2A6009F4";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".ove" yes;
	setAttr ".t" -type "double3" -2.1855900000000017 5.0433523957131996 -0.53543691719572628 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" -180 -1.2722218725854067e-14 89.999999999999986 ;
	setAttr ".bps" -type "matrix" 2.2204460492503131e-16 -1 0 0 1 2.2204460492503131e-16 0 0
		 0 0 1 0 0.40396301701286447 22.628380391977608 0.93191360083425789 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:UpArm_R_TmpJnt" -p "dac_characterTmp:Clav_R_TmpJnt";
	rename -uid "6810C57D-47FC-C23C-7085-258A574DCA0A";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" -1.7708000000000013 -3.3171299999999992 1.2294930000000008 ;
	setAttr ".r" -type "double3" -4 0 0 ;
	setAttr ".ro" 4;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" 2.2204460492503131e-16 -1 0 0 1 2.2204460492503131e-16 0 0
		 0 0 1 0 2.6840862727483024 21.950819439051291 -0.34347723857962076 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Forearm_R_TmpJnt" -p "dac_characterTmp:UpArm_R_TmpJnt";
	rename -uid "9E2A65BD-49E4-20D5-DE00-7689127C1614";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" -7.1054273576010019e-15 -10.921684171983246 -7.1927056458642724e-06 ;
	setAttr ".r" -type "double3" 8 0 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" 5.948909502176962e-05 0 0 ;
	setAttr ".bps" -type "matrix" 2.2204460492503131e-16 -1 0 0 0.99999999999946088 2.2204460492491157e-16 -1.0384142298428127e-06 0
		 1.0384142298428127e-06 2.3057427741397801e-22 0.99999999999946088 0 6.7891781465089913 21.950819439060187 -0.42637972484178432 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Wrist_R_TmpJnt" -p "dac_characterTmp:Forearm_R_TmpJnt";
	rename -uid "E90624C4-4E8D-0B8A-98C6-86B4AB8A3462";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" 0 -11.092720396168399 2.4608312657825593e-05 ;
	setAttr ".r" -type "double3" -4 1.9801038241349232e-06 4.4995857799076475e-08 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" -0.00011898431646615623 3.045630880675183e-42 2.9331898623289967e-36 ;
	setAttr ".bps" -type "matrix" 2.2204460492503131e-16 -1 0 0 1 2.2204460492503131e-16 8.3960588285190204e-14 0
		 -8.3960588285190204e-14 -1.8642995655058271e-29 1 0 11.145136555499452 21.950819439088733 -0.34347297829513029 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Hand_R_TmpJnt" -p "dac_characterTmp:Wrist_R_TmpJnt";
	rename -uid "5D98815F-453C-9946-F1D9-26BE8FDD7F7F";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" -3.3632048257459246e-09 -1.3951000000002871 -4.4865440584906935e-07 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 2.2204460492503131e-16 -1 0 0 1 2.2204460492503131e-16 8.3960588285190204e-14 0
		 -8.3960588285190204e-14 -1.8642995655058271e-29 1 0 11.94334900537585 21.950819439093973 -0.34347375639834526 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Thumb1_R_TmpJnt" -p "dac_characterTmp:Hand_R_TmpJnt";
	rename -uid "060F60D9-4F3D-479C-66B5-D4B20D6234BD";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".t" -type "double3" -1.1959999511349011 0.20970145946770558 -1.4027368234816586 ;
	setAttr ".r" -type "double3" 4.4355566940309572 14.036160539675029 14.081816860710981 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" 30.63946994792834 -21.375623759174879 -57.046296357955789 ;
	setAttr ".bps" -type "matrix" -0.59548586752167076 -0.80336578317847407 -4.9997343752636337e-14 0
		 0.73868319246536318 -0.54754062333671982 0.39312390790336815 0 -0.31582229615899904 0.23409973134130962 0.91948550452673483 0
		 11.518645340649305 21.905429719636764 0.32567524419271171 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Thumb2_R_TmpJnt" -p "dac_characterTmp:Thumb1_R_TmpJnt";
	rename -uid "BB662A9A-4867-B584-76E0-ECB8C22E6C38";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" -2.4510256210419357e-05 -1.7076595684001852 -0.5802232704179886 ;
	setAttr ".r" -type "double3" 0.41396271739684226 0 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" 8.5377364625159377e-07 -4.7393960819234678e-23 3.7915166360156471e-22 ;
	setAttr ".bps" -type "matrix" -0.59548586752167076 -0.80336578317847407 -4.9997343752636337e-14 0
		 0.73868319246536318 -0.54754062333671982 0.39312390790336815 0 -0.31582229615899904 0.23409973134130962 0.91948550452673483 0
		 12.026286103962779 21.529146703032435 0.59583939943602238 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Thumb3_R_TmpJnt" -p "dac_characterTmp:Thumb2_R_TmpJnt";
	rename -uid "6606FE57-4252-E9BF-AC99-39AADAE218B1";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 3.2332052427364033e-05 -0.81876258881528274 3.179836663491642e-05 ;
	setAttr ".r" -type "double3" 1.9811204732243901e-05 8.4022342688732486e-06 -1.0449136034575707e-05 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -0.59548586752167076 -0.80336578317847407 -4.9997343752636337e-14 0
		 0.73868319246536318 -0.54754062333671982 0.39312390790336815 0 -0.31582229615899904 0.23409973134130962 0.91948550452673483 0
		 12.724953059079672 21.011267666374362 0.96766684389406166 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Thumb4_R_TmpJnt" -p "dac_characterTmp:Thumb3_R_TmpJnt";
	rename -uid "361DDC5D-4F24-8969-33A7-B18188B68372";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 2.4499934141886115e-05 -1.3738526198385625 -2.9368133489526826e-05 ;
	setAttr ".r" -type "double3" 1.1729200673035528e-08 2.0051580232284338e-08 3.8535522122013761e-08 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" -0.59548586752167076 -0.80336578317847407 -4.9997343752636337e-14 0
		 0.73868319246536318 -0.54754062333671982 0.39312390790336815 0 -0.31582229615899904 0.23409973134130962 0.91948550452673483 0
		 13.320225968832865 20.570028305053697 1.2844684375111461 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:HandCupInner_R_TmpJnt" -p "dac_characterTmp:Hand_R_TmpJnt";
	rename -uid "EF65C630-45F5-7D2B-C9F7-1EB4D67C64CA";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" -0.18179997877866327 -0.34489933533412653 -0.6396723644070863 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" 1.3017646484936847 3.1048821386086977e-06 -2.1338492114880787 ;
	setAttr ".bps" -type "matrix" 2.2204460492503131e-16 -1 0 0 1 2.2204460492503131e-16 8.3960588285190204e-14 0
		 -8.3960588285190204e-14 -1.8642995655058271e-29 1 0 11.145136555499452 21.950819439088733 -0.34347297829513029 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Index1_R_TmpJnt" -p "dac_characterTmp:HandCupInner_R_TmpJnt";
	rename -uid "483724C1-4938-3A1B-1C7A-609B321024AA";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".t" -type "double3" 0.019217984838469704 -1.2899043231846434 -0.84104217639326107 ;
	setAttr ".r" -type "double3" 6.0776872350974251 -0.057451793381633344 0.62360691504919974 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" -1.2021013575951234e-12 -89.999999999998806 0 ;
	setAttr ".bps" -type "matrix" -8.3960588285190191e-14 -8.437694987151191e-14 1 0
		 1 2.2204460492503131e-16 8.3960588285190204e-14 0 -2.2204460493211562e-16 1 8.4376949871511897e-14 0
		 11.915961054098767 22.155901831531715 0.36968006095532863 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Index2_R_TmpJnt" -p "dac_characterTmp:Index1_R_TmpJnt";
	rename -uid "664E762E-4847-5B45-8892-4D9D43C5D2F9";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" 1.1039644104426927e-06 -1.6642626580782363 0.21658297705536711 ;
	setAttr ".r" -type "double3" -4.9966809737460167 0 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-14 -8.437694987151191e-14 1 0
		 1 2.2204460492503131e-16 8.3960588285190204e-14 0 -2.2204460493211562e-16 1 8.4376949871511897e-14 0
		 13.114219270621863 22.155901831531715 0.36968006095542938 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Index3_R_TmpJnt" -p "dac_characterTmp:Index2_R_TmpJnt";
	rename -uid "C6B09FFF-44B0-3101-26BB-24A411400FC5";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" -8.5868229804386687e-07 -1.7621190549193528 -7.3575682421278543e-05 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-14 -8.437694987151191e-14 1 0
		 1 2.2204460492503131e-16 8.3960588285190204e-14 0 -2.2204460493211562e-16 1 8.4376949871511897e-14 0
		 13.805785441300909 22.155901831531718 0.3696800609554875 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Index4_R_TmpJnt" -p "dac_characterTmp:Index3_R_TmpJnt";
	rename -uid "448F7481-4926-626E-CF9F-F88A3248B1C5";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" -2.9147117100447595e-06 -1.0197445210565732 6.5319217092962845e-05 ;
	setAttr ".r" -type "double3" 0 -2.2255657207179173e-06 -2.3818287790657693e-05 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-14 -8.437694987151191e-14 1 0
		 1 2.2204460492503131e-16 8.3960588285190204e-14 0 -2.2204460493211562e-16 1 8.4376949871511897e-14 0
		 14.271394348292739 22.155901831531718 0.36968006095552658 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Index5_R_TmpJnt" -p "dac_characterTmp:Index4_R_TmpJnt";
	rename -uid "EFE0FEEB-4043-9443-F1F4-BB922FBFC500";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 3.1487670770768972e-07 -1.2018870917187456 1.8993080800555617e-05 ;
	setAttr ".r" -type "double3" -9.2540546237594936e-13 5.3162927997018629e-10 -5.5147836391628063e-11 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" -8.3960588285190191e-14 -8.437694987151191e-14 1 0
		 1 2.2204460492503131e-16 8.3960588285190204e-14 0 -2.2204460493211562e-16 1 8.4376949871511897e-14 0
		 14.812322343180313 22.155901831531722 0.36968006095557193 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Middle1_R_TmpJnt" -p "dac_characterTmp:HandCupInner_R_TmpJnt";
	rename -uid "A055B05B-4C54-1592-358E-B3950653C78F";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".t" -type "double3" 0.18169521321231485 -1.2650466191602057 0.21107065198903185 ;
	setAttr ".r" -type "double3" 4.6980854431389023 0.4935833473055235 1.8599270689119174 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" -1.202242292818334e-12 -89.999999999995026 0 ;
	setAttr ".bps" -type "matrix" -8.3960588285190191e-14 -8.437694987151191e-14 1 0
		 1 2.2204460492503131e-16 8.3960588285190204e-14 0 -2.2204460493211562e-16 1 8.4376949871511897e-14 0
		 11.876633548480955 22.169748485139639 -0.10080869585328409 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Middle2_R_TmpJnt" -p "dac_characterTmp:Middle1_R_TmpJnt";
	rename -uid "258376C1-43FF-D3FE-9CDD-17A7E7EE35BD";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" 3.3974965132022028e-05 -1.6779936243470388 0.202781413379995 ;
	setAttr ".r" -type "double3" -4.7286055832585197 -3.7229239363060597e-06 -2.3630146386096708e-05 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-14 -8.437694987151191e-14 1 0
		 1 2.2204460492503131e-16 8.3960588285190204e-14 0 -2.2204460493211562e-16 1 8.4376949871511897e-14 0
		 13.108601055533605 22.169748485139639 -0.10080869585318036 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Middle3_R_TmpJnt" -p "dac_characterTmp:Middle2_R_TmpJnt";
	rename -uid "A8CA75B2-4020-04A8-6F48-B68B1F45A115";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" -3.6335998323311358e-05 -1.9377748291441499 2.0397667448435186e-05 ;
	setAttr ".r" -type "double3" -7.3358776270297159e-13 -4.385389547182396e-10 3.8737297385318759e-10 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-14 -8.437694987151191e-14 1 0
		 1 2.2204460492503131e-16 8.3960588285190204e-14 0 -2.2204460493211562e-16 1 8.4376949871511897e-14 0
		 13.906913312889571 22.169748485139639 -0.10080869585311319 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Middle4_R_TmpJnt" -p "dac_characterTmp:Middle3_R_TmpJnt";
	rename -uid "619B6B41-4D61-DB1D-06DE-20BA6D2B00CF";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 6.7200066267147918e-07 -1.1467722736571844 -7.3959931654599131e-06 ;
	setAttr ".r" -type "double3" -7.3358776270297179e-13 -4.3853915350290709e-10 3.8737297385318759e-10 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-14 -8.437694987151191e-14 1 0
		 1 2.2204460492503131e-16 8.3960588285190204e-14 0 -2.2204460493211562e-16 1 8.4376949871511897e-14 0
		 14.411849725499213 22.169748485139639 -0.10080869585307067 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Middle5_R_TmpJnt" -p "dac_characterTmp:Middle4_R_TmpJnt";
	rename -uid "2D317269-4FC0-E763-F5DB-B9B23C3867F1";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" -4.4441944557771507e-07 -1.342321624957485 7.762341056150035e-06 ;
	setAttr ".r" -type "double3" -7.3358776270297189e-13 -4.3853935228757467e-10 3.8737297385318759e-10 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" -8.3960588285190191e-14 -8.437694987151191e-14 1 0
		 1 2.2204460492503131e-16 8.3960588285190204e-14 0 -2.2204460493211562e-16 1 8.4376949871511897e-14 0
		 15.081996667416741 22.169748485139635 -0.10080869585301426 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:HandCupOuter_R_TmpJnt" -p "dac_characterTmp:Hand_R_TmpJnt";
	rename -uid "F8793A54-475A-FC29-0D32-FA95D5D7E131";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" -0.18180002072447365 -0.3449005987370839 0.57702463559225703 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" 1.3017646484936847 3.1048821386521818e-06 -2.1338492114880792 ;
	setAttr ".bps" -type "matrix" 2.2204460492503131e-16 -1 0 0 1 2.2204460492503131e-16 8.3960588285190204e-14 0
		 -8.3960588285190204e-14 -1.8642995655058271e-29 1 0 11.145136555499452 21.950819439088733 -0.34347297829513029 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Ring1_R_TmpJnt" -p "dac_characterTmp:HandCupOuter_R_TmpJnt";
	rename -uid "73B16A2A-4C30-9169-379A-A1A3B5385446";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".t" -type "double3" 0.22223688407807174 -1.148070520660383 0.054925002010642654 ;
	setAttr ".r" -type "double3" 1.7607815396797539 0.054544603401168332 2.6276658961730472 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" -1.2021001928412116e-12 -89.999999999998806 0 ;
	setAttr ".bps" -type "matrix" -8.3960588285190191e-14 -8.437694987151191e-14 1 0
		 1 2.2204460492503131e-16 8.3960588285190204e-14 0 -2.2204460493211562e-16 1 8.4376949871511897e-14 0
		 11.876633548480955 22.13656260093245 -0.5683912766476541 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Ring2_R_TmpJnt" -p "dac_characterTmp:Ring1_R_TmpJnt";
	rename -uid "DE614FDB-462C-4715-C0B7-26A0E7B5E1A5";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" 4.2602409208170045e-06 -1.5727830280380175 0.24918057548185857 ;
	setAttr ".r" -type "double3" -0.27026140853818587 2.6654153335147113e-14 -4.6737559059409608e-15 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-14 -8.437694987151191e-14 1 0
		 1 2.2204460492503131e-16 8.3960588285190204e-14 0 -2.2204460493211562e-16 1 8.4376949871511897e-14 0
		 13.097480078235414 22.13656260093245 -0.56839127664755129 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Ring3_R_TmpJnt" -p "dac_characterTmp:Ring2_R_TmpJnt";
	rename -uid "8FC32594-470D-06B1-2881-A3ADA5ABD746";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" -0.00011270855980560768 -1.9646542464742289 0.054063721732418912 ;
	setAttr ".r" -type "double3" -9.9451421645007478e-13 2.6711481998701152e-14 -4.3732358437187842e-15 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-14 -8.437694987151191e-14 1 0
		 1 2.2204460492503131e-16 8.3960588285190204e-14 0 -2.2204460493211562e-16 1 8.4376949871511897e-14 0
		 13.845747937749518 22.136562600932457 -0.56839127664748834 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Ring4_R_TmpJnt" -p "dac_characterTmp:Ring3_R_TmpJnt";
	rename -uid "5755A3AB-46A3-9511-157D-2098C3FCFB3D";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" -4.3655036666701008e-06 -1.0128345217944101 3.7979051384695595e-05 ;
	setAttr ".r" -type "double3" -9.9451421644176904e-13 2.673633008215009e-14 -4.1744511761273166e-15 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-14 -8.437694987151191e-14 1 0
		 1 2.2204460492503131e-16 8.3960588285190204e-14 0 -2.2204460493211562e-16 1 8.4376949871511897e-14 0
		 14.345123861710062 22.136562600932454 -0.56839127664744615 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Ring5_R_TmpJnt" -p "dac_characterTmp:Ring4_R_TmpJnt";
	rename -uid "79E3BA26-4D29-EF45-886B-39A43C693B02";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" -1.7704527832673733e-06 -1.166785075100286 1.0958955805051573e-05 ;
	setAttr ".r" -type "double3" -9.9451421643346311e-13 2.6748754123874563e-14 -3.9756665085358466e-15 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" -8.3960588285190191e-14 -8.437694987151191e-14 1 0
		 1 2.2204460492503131e-16 8.3960588285190204e-14 0 -2.2204460493211562e-16 1 8.4376949871511897e-14 0
		 14.876258587400191 22.136562600932457 -0.56839127664740152 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Pinky1_R_TmpJnt" -p "dac_characterTmp:HandCupOuter_R_TmpJnt";
	rename -uid "FE48B772-4B1B-A433-75B5-D49B45BDC7D5";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".t" -type "double3" 0.12453002056523133 -0.79771281221773549 0.88582031705177267 ;
	setAttr ".r" -type "double3" 0.71347204496072636 0.022145375705238547 4.487030944693176 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" -1.2021001928412116e-12 -89.999999999998806 0 ;
	setAttr ".bps" -type "matrix" -8.3960588285190191e-14 -8.437694987151191e-14 1 0
		 1 2.2204460492503131e-16 8.3960588285190204e-14 0 -2.2204460493211562e-16 1 8.4376949871511897e-14 0
		 11.876633548480955 22.020988418911262 -0.95907086085240945 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Pinky2_R_TmpJnt" -p "dac_characterTmp:Pinky1_R_TmpJnt";
	rename -uid "44B9EFA1-4FD1-AFC2-25F1-B1AAC26FE617";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" -3.0426678895301151e-06 -1.5111551528589366 0.21179423122136143 ;
	setAttr ".r" -type "double3" -1.2149715034897679 -2.0673978309848345e-06 -2.3832223712996636e-05 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-14 -8.437694987151191e-14 1 0
		 1 2.2204460492503131e-16 8.3960588285190204e-14 0 -2.2204460493211562e-16 1 8.4376949871511897e-14 0
		 13.083100380346423 22.020988418911262 -0.95907086085230775 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Pinky3_R_TmpJnt" -p "dac_characterTmp:Pinky2_R_TmpJnt";
	rename -uid "EF855B77-4334-FDEF-1D10-82996D7B7DEC";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 3.1248861673827122e-06 -1.6512948061042252 -3.038792726073325e-05 ;
	setAttr ".r" -type "double3" -6.4955687390626904e-13 -2.5002336034993868e-09 -4.5243390315822371e-12 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-14 -8.437694987151191e-14 1 0
		 1 2.2204460492503131e-16 8.3960588285190204e-14 0 -2.2204460493211562e-16 1 8.4376949871511897e-14 0
		 13.601293073636691 22.020988418911262 -0.95907086085226423 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Pinky4_R_TmpJnt" -p "dac_characterTmp:Pinky3_R_TmpJnt";
	rename -uid "9FD8319A-4F67-5741-6BBD-4984DE3A92B0";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" -7.836811249717357e-06 -0.835174543252446 4.7346081530008632e-05 ;
	setAttr ".r" -type "double3" -6.4955687389545067e-13 -2.5002335786513031e-09 -4.5239414622470542e-12 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -8.3960588285190191e-14 -8.437694987151191e-14 1 0
		 1 2.2204460492503131e-16 8.3960588285190204e-14 0 -2.2204460493211562e-16 1 8.4376949871511897e-14 0
		 13.978441565540829 22.020988418911262 -0.95907086085223248 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Pinky5_R_TmpJnt" -p "dac_characterTmp:Pinky4_R_TmpJnt";
	rename -uid "F4CEF0BD-4D51-F887-8960-1C837A56E430";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 6.1915718200244996e-06 -1.0170460954849077 -4.5361029563650845e-06 ;
	setAttr ".r" -type "double3" -6.4955687388463239e-13 -2.5002335786513031e-09 -4.5235438929118713e-12 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" -8.3960588285190191e-14 -8.437694987151191e-14 1 0
		 1 2.2204460492503131e-16 8.3960588285190204e-14 0 -2.2204460493211562e-16 1 8.4376949871511897e-14 0
		 14.437677801786013 22.020988418911262 -0.95907086085219373 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Elbow_R_TmpJnt" -p "dac_characterTmp:Forearm_R_TmpJnt";
	rename -uid "0413A752-4712-7A62-61D6-0497D67170A3";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 7.1054273576010019e-15 0.93659780609068299 13.393772797088435 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" 1.1956205637460297e-14 -4.0000594890950305 -90.000000000000057 ;
	setAttr ".bps" -type "matrix" -1 -1.2246467991473532e-16 0 0 1.2246467991473532e-16 -1 0 0
		 0 0 1 0 1.3486489690923307 14.656585133659888 1.5144370407379352e-17 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Pelvis_TmpJnt" -p "dac_characterTmp:Root_TmpJnt";
	rename -uid "CEDF7B01-4A97-D7F5-FC47-E1BBBB44AE30";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" -6.1629758220391547e-33 -0.3653318047671803 -0.26483091210960019 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 15.4830611404753 0 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:UpLeg_L_TmpJnt" -p "dac_characterTmp:Pelvis_TmpJnt";
	rename -uid "E3EC4F8C-40E1-6BBF-B42C-44A267D549AD";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 3.7941088276720967 -3.4348458209502937 -0.11140282602242156 ;
	setAttr ".r" -type "double3" 2.0414694228308763 0 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" 0 0 -180 ;
	setAttr ".bps" -type "matrix" -1 -1.2246467991473532e-16 0 0 1.2246467991473532e-16 -1 0 0
		 0 0 1 0 1.3486489690923307 14.656585133659888 1.5144370407379352e-17 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:LowLeg_L_TmpJnt" -p "dac_characterTmp:UpLeg_L_TmpJnt";
	rename -uid "71A1F2E1-4290-71B2-3843-7F98818AD703";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 1.5987211554602254e-14 20.014236913889356 0 ;
	setAttr ".r" -type "double3" -10 0 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -1 -1.2246467991473532e-16 0 0 1.2246467991473532e-16 -1 0 0
		 0 0 1 0 1.3486489690923305 7.7535119809750004 0.15259281481888803 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Ankle_L_TmpJnt" -p "dac_characterTmp:LowLeg_L_TmpJnt";
	rename -uid "6AF2D5CE-436D-100B-D8A0-BDB2E4089726";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 3.1086244689504383e-15 20 0 ;
	setAttr ".r" -type "double3" 10 0 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -1 -1.2246467991473532e-16 0 0 1.2246467991473532e-16 -1 0 0
		 0 0 1 0 1.348648969092332 1.0615915425310156 0 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Ball_L_TmpJnt" -p "dac_characterTmp:Ankle_L_TmpJnt";
	rename -uid "B5807522-4007-BE13-A78A-08881E1CBACA";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" -0.018907945002795135 4.7339087230825108 6.6643220495006679 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" 87.958530577169114 -7.2622108379341389e-15 -6.7623008292801882e-15 ;
	setAttr ".bps" -type "matrix" -1 -1.2246467991473532e-16 0 0 2.7192621468937821e-32 -2.2204460492503131e-16 1 0
		 -1.2246467991473532e-16 1 2.2204460492503131e-16 0 1.3486489690923311 5.5511151231257827e-15 1.476681409489522 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Toe_L_TmpJnt" -p "dac_characterTmp:Ball_L_TmpJnt";
	rename -uid "DEDF98C1-4E46-BF8D-2663-1BB8D8FA5DE9";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" 7.4387794102293014e-07 4.6096758824030042 5.5274343244982038e-06 ;
	setAttr ".r" -type "double3" 3.1326085551823781e-05 -4.5198400492033635e-29 -1.2646100017450386e-06 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" -1 -1.2246467991473532e-16 0 0 2.7192621468937821e-32 -2.2204460492503131e-16 1 0
		 -1.2246467991473532e-16 1 2.2204460492503131e-16 0 1.3486489690923309 5.0024078434460801e-15 3.3551152167720435 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:HeelIkTwistPiv_L_TmpJnt" -p "dac_characterTmp:Ball_L_TmpJnt";
	rename -uid "43026FAF-4449-E779-8ED8-549437D0D0C8";
	setAttr ".t" -type "double3" 0.030733023871861054 -6.8287274426509912 -0.092027285713577323 ;
	setAttr ".jo" -type "double3" 2.041469422830883 -7.016709298534876e-15 7.016709298534876e-15 ;
	setAttr ".radi" 0.4;
createNode joint -n "dac_characterTmp:Heel_L_TmpJnt" -p "dac_characterTmp:HeelIkTwistPiv_L_TmpJnt";
	rename -uid "179FF03E-41D6-953F-424B-08A9A3BB155A";
	setAttr ".t" -type "double3" 0 -2.9389998613778543 0.10476196825947581 ;
	setAttr ".jo" -type "double3" -3.9756933518293969e-16 6.7623008292801913e-15 -7.2622108379341373e-15 ;
	setAttr ".radi" 0.4;
createNode joint -n "dac_characterTmp:FootIn_L_TmpJnt" -p "dac_characterTmp:Ankle_L_TmpJnt";
	rename -uid "BAFD81E9-46E1-B301-FF16-75AB3332E2BA";
	setAttr ".t" -type "double3" 1.5937914967671685 4.7063886609567946 3.3088924199721022 ;
	setAttr ".jo" -type "double3" 89.999999999999986 0 0 ;
	setAttr ".radi" 0.4;
createNode joint -n "dac_characterTmp:FootOut_L_TmpJnt" -p "dac_characterTmp:Ankle_L_TmpJnt";
	rename -uid "B9B8AB5B-4A3F-BD25-8C66-14A5E0510C9F";
	setAttr ".t" -type "double3" -2.164234929888138 4.7063886609568071 3.3974074527004112 ;
	setAttr ".jo" -type "double3" 89.999999999999986 0 0 ;
	setAttr ".radi" 0.4;
createNode joint -n "dac_characterTmp:Knee_L_TmpJnt" -p "dac_characterTmp:LowLeg_L_TmpJnt";
	rename -uid "CA3C4E99-4EF6-D506-DB98-21B0090232A3";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" -8.8817841970012523e-16 -3.0286208273992608 21.663514976786406 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" 4 0 0 ;
	setAttr ".bps" -type "matrix" -1 -1.2246467991473532e-16 0 0 1.2246467991473532e-16 -1 0 0
		 0 0 1 0 1.3486489690923307 14.656585133659888 1.5144370407379352e-17 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:UpLeg_R_TmpJnt" -p "dac_characterTmp:Pelvis_TmpJnt";
	rename -uid "86016858-4AED-2575-9FC2-0CB9C78DB31E";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" -3.79411 -3.4348594220882447 -0.11140316614180629 ;
	setAttr ".r" -type "double3" 2.0414694228308763 0 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" 0 180 0 ;
	setAttr ".bps" -type "matrix" -1 -1.2246467991473532e-16 0 0 1.2246467991473532e-16 -1 0 0
		 0 0 1 0 1.3486489690923307 14.656585133659888 1.5144370407379352e-17 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:LowLeg_R_TmpJnt" -p "dac_characterTmp:UpLeg_R_TmpJnt";
	rename -uid "7A318FCA-4628-8048-0B35-6C8B98FF0486";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 4.4408920985006262e-16 -20.014202990407053 -2.5597984758540804e-06 ;
	setAttr ".r" -type "double3" -10 0 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -1 -1.2246467991473532e-16 0 0 1.2246467991473532e-16 -1 0 0
		 0 0 1 0 1.3486489690923305 7.7535119809750004 0.15259281481888803 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Ankle_R_TmpJnt" -p "dac_characterTmp:LowLeg_R_TmpJnt";
	rename -uid "B7DB8601-4AD4-B686-2908-50B3CB47CA28";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" -8.8817841970012523e-16 -20.000019489215493 7.0115716299667952e-07 ;
	setAttr ".r" -type "double3" 10 0 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -1 -1.2246467991473532e-16 0 0 1.2246467991473532e-16 -1 0 0
		 0 0 1 0 1.348648969092332 1.0615915425310156 0 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Ball_R_TmpJnt" -p "dac_characterTmp:Ankle_R_TmpJnt";
	rename -uid "9634FB7D-447D-F20D-912B-4A851A87ECEE";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 0.018910000000000871 -4.733906652217625 -6.6643246822907516 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" 87.958530577169114 6.7623008292801882e-15 -7.2622108379341389e-15 ;
	setAttr ".bps" -type "matrix" -1 -1.2246467991473532e-16 0 0 2.7192621468937821e-32 -2.2204460492503131e-16 1 0
		 -1.2246467991473532e-16 1 2.2204460492503131e-16 0 1.3486489690923311 5.5511151231257827e-15 1.476681409489522 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:Toe_R_TmpJnt" -p "dac_characterTmp:Ball_R_TmpJnt";
	rename -uid "CFD6CBBA-4932-9D96-5C81-1B813D0F5200";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" -1.7763568394002505e-15 -4.609675746362198 -3.2982032771577963e-07 ;
	setAttr ".r" -type "double3" 3.1326085551823781e-05 -4.5198400492033635e-29 -1.2646100017450386e-06 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 1.2074182697257331e-06 -1.5303964851875898e-22 -1.4250483593091742e-22 ;
	setAttr ".bps" -type "matrix" -1 -1.2246467991473532e-16 0 0 2.7192621468937821e-32 -2.2204460492503131e-16 1 0
		 -1.2246467991473532e-16 1 2.2204460492503131e-16 0 1.3486489690923309 5.0024078434460801e-15 3.3551152167720435 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode joint -n "dac_characterTmp:HeelIkTwistPiv_R_TmpJnt" -p "dac_characterTmp:Ball_R_TmpJnt";
	rename -uid "BF5E8BEA-419A-4D61-21DB-42A9A83EDB8A";
	setAttr ".t" -type "double3" -0.030740000000000212 6.8287276167948905 0.092030636625853868 ;
	setAttr ".jo" -type "double3" 2.0414694228308621 -7.016709298534876e-15 -7.016709298534876e-15 ;
	setAttr ".radi" 0.4;
createNode joint -n "dac_characterTmp:Heel_R_TmpJnt" -p "dac_characterTmp:HeelIkTwistPiv_R_TmpJnt";
	rename -uid "9EB98194-4154-AA59-0249-8E8E660F8702";
	setAttr ".t" -type "double3" 0 2.9389958348098877 -0.10476182473065343 ;
	setAttr ".jo" -type "double3" 1.7095481412866407e-14 7.2622108379341342e-15 6.7623008292801945e-15 ;
	setAttr ".radi" 0.4;
createNode joint -n "dac_characterTmp:FootIn_R_TmpJnt" -p "dac_characterTmp:Ankle_R_TmpJnt";
	rename -uid "A1D72A67-4F19-6469-3762-EAA48DB35D82";
	setAttr ".t" -type "double3" -1.5937899999999994 -4.7063893133437844 -3.3088952559190226 ;
	setAttr ".jo" -type "double3" 89.999999999999986 1.4024511667214329e-14 -4.999100086539459e-16 ;
	setAttr ".radi" 0.4;
createNode joint -n "dac_characterTmp:FootOut_R_TmpJnt" -p "dac_characterTmp:Ankle_R_TmpJnt";
	rename -uid "B17A0B50-43BE-8EB2-F64A-AC815A1E922B";
	setAttr ".t" -type "double3" 2.1642300000000008 -4.706392507691155 -3.3974113227645182 ;
	setAttr ".jo" -type "double3" 89.999999999999986 1.4024511667214329e-14 -4.999100086539459e-16 ;
	setAttr ".radi" 0.4;
createNode joint -n "dac_characterTmp:Knee_R_TmpJnt" -p "dac_characterTmp:LowLeg_R_TmpJnt";
	rename -uid "9AEDE46E-48B1-E84F-82D1-CE804510C758";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" -3.5527136788005009e-15 3.0286159232973766 -21.663479898085381 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" 3.9999999999997673 5.6707681723866391e-16 -3.9768344388434169e-16 ;
	setAttr ".bps" -type "matrix" -1 -1.2246467991473532e-16 0 0 1.2246467991473532e-16 -1 0 0
		 0 0 1 0 1.3486489690923307 14.656585133659888 1.5144370407379352e-17 1;
	setAttr -k on -cb off ".radi" 0.4;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "E3E20555-45B7-4EF6-0236-64A0C0FD06D7";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "B61AC087-4879-76B8-4D34-2EAB08E012B6";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "6F7D5D51-4E35-52D1-87AF-26B9D377EAA3";
createNode displayLayerManager -n "layerManager";
	rename -uid "38F3C99C-40F8-3D69-03EC-778B76DC1E54";
createNode displayLayer -n "defaultLayer";
	rename -uid "E61D6A4D-4AB3-3910-1FFE-84810EB3E90E";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "45567680-452F-7F16-2D3B-B1BB2057A97F";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "D45B09CE-461B-384E-CB4B-668E832F6D05";
	setAttr ".g" yes;
createNode script -n "dac_characterTmp:sceneConfigurationScriptNode";
	rename -uid "814BDA3B-480C-A726-65FF-98BDE8434341";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 300 -ast 1 -aet 500 ";
	setAttr ".st" 6;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".cons" no;
	setAttr ".ta" 0;
	setAttr ".tmr" 1024;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
connectAttr "dac_characterTmp:Root_TmpJnt.s" "dac_characterTmp:Spine1_TmpJnt.is";
connectAttr "dac_characterTmp:Spine1_TmpJnt.s" "dac_characterTmp:Spine2_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Spine2_TmpJnt.s" "dac_characterTmp:Spine3_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Spine3_TmpJnt.s" "dac_characterTmp:Spine4_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Spine4_TmpJnt.s" "dac_characterTmp:Neck_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Neck_TmpJnt.s" "dac_characterTmp:Head_TmpJnt.is";
connectAttr "dac_characterTmp:Head_TmpJnt.s" "dac_characterTmp:HeadEnd_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Spine4_TmpJnt.s" "dac_characterTmp:Clav_L_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Clav_L_TmpJnt.s" "dac_characterTmp:UpArm_L_TmpJnt.is"
		;
connectAttr "dac_characterTmp:UpArm_L_TmpJnt.s" "dac_characterTmp:Forearm_L_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Forearm_L_TmpJnt.s" "dac_characterTmp:Wrist_L_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Wrist_L_TmpJnt.s" "dac_characterTmp:Hand_L_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Hand_L_TmpJnt.s" "dac_characterTmp:Thumb1_L_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Thumb1_L_TmpJnt.s" "dac_characterTmp:Thumb2_L_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Thumb2_L_TmpJnt.s" "dac_characterTmp:Thumb3_L_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Thumb3_L_TmpJnt.s" "dac_characterTmp:Thumb4_L_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Hand_L_TmpJnt.s" "dac_characterTmp:HandCupInner_L_TmpJnt.is"
		;
connectAttr "dac_characterTmp:HandCupInner_L_TmpJnt.s" "dac_characterTmp:Index1_L_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Index1_L_TmpJnt.s" "dac_characterTmp:Index2_L_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Index2_L_TmpJnt.s" "dac_characterTmp:Index3_L_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Index3_L_TmpJnt.s" "dac_characterTmp:Index4_L_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Index4_L_TmpJnt.s" "dac_characterTmp:Index5_L_TmpJnt.is"
		;
connectAttr "dac_characterTmp:HandCupInner_L_TmpJnt.s" "dac_characterTmp:Middle1_L_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Middle1_L_TmpJnt.s" "dac_characterTmp:Middle2_L_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Middle2_L_TmpJnt.s" "dac_characterTmp:Middle3_L_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Middle3_L_TmpJnt.s" "dac_characterTmp:Middle4_L_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Middle4_L_TmpJnt.s" "dac_characterTmp:Middle5_L_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Hand_L_TmpJnt.s" "dac_characterTmp:HandCupOuter_L_TmpJnt.is"
		;
connectAttr "dac_characterTmp:HandCupOuter_L_TmpJnt.s" "dac_characterTmp:Ring1_L_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Ring1_L_TmpJnt.s" "dac_characterTmp:Ring2_L_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Ring2_L_TmpJnt.s" "dac_characterTmp:Ring3_L_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Ring3_L_TmpJnt.s" "dac_characterTmp:Ring4_L_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Ring4_L_TmpJnt.s" "dac_characterTmp:Ring5_L_TmpJnt.is"
		;
connectAttr "dac_characterTmp:HandCupOuter_L_TmpJnt.s" "dac_characterTmp:Pinky1_L_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Pinky1_L_TmpJnt.s" "dac_characterTmp:Pinky2_L_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Pinky2_L_TmpJnt.s" "dac_characterTmp:Pinky3_L_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Pinky3_L_TmpJnt.s" "dac_characterTmp:Pinky4_L_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Pinky4_L_TmpJnt.s" "dac_characterTmp:Pinky5_L_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Forearm_L_TmpJnt.s" "dac_characterTmp:Elbow_L_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Spine4_TmpJnt.s" "dac_characterTmp:Clav_R_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Clav_R_TmpJnt.s" "dac_characterTmp:UpArm_R_TmpJnt.is"
		;
connectAttr "dac_characterTmp:UpArm_R_TmpJnt.s" "dac_characterTmp:Forearm_R_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Forearm_R_TmpJnt.s" "dac_characterTmp:Wrist_R_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Wrist_R_TmpJnt.s" "dac_characterTmp:Hand_R_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Hand_R_TmpJnt.s" "dac_characterTmp:Thumb1_R_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Thumb1_R_TmpJnt.s" "dac_characterTmp:Thumb2_R_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Thumb2_R_TmpJnt.s" "dac_characterTmp:Thumb3_R_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Thumb3_R_TmpJnt.s" "dac_characterTmp:Thumb4_R_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Hand_R_TmpJnt.s" "dac_characterTmp:HandCupInner_R_TmpJnt.is"
		;
connectAttr "dac_characterTmp:HandCupInner_R_TmpJnt.s" "dac_characterTmp:Index1_R_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Index1_R_TmpJnt.s" "dac_characterTmp:Index2_R_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Index2_R_TmpJnt.s" "dac_characterTmp:Index3_R_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Index3_R_TmpJnt.s" "dac_characterTmp:Index4_R_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Index4_R_TmpJnt.s" "dac_characterTmp:Index5_R_TmpJnt.is"
		;
connectAttr "dac_characterTmp:HandCupInner_R_TmpJnt.s" "dac_characterTmp:Middle1_R_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Middle1_R_TmpJnt.s" "dac_characterTmp:Middle2_R_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Middle2_R_TmpJnt.s" "dac_characterTmp:Middle3_R_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Middle3_R_TmpJnt.s" "dac_characterTmp:Middle4_R_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Middle4_R_TmpJnt.s" "dac_characterTmp:Middle5_R_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Hand_R_TmpJnt.s" "dac_characterTmp:HandCupOuter_R_TmpJnt.is"
		;
connectAttr "dac_characterTmp:HandCupOuter_R_TmpJnt.s" "dac_characterTmp:Ring1_R_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Ring1_R_TmpJnt.s" "dac_characterTmp:Ring2_R_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Ring2_R_TmpJnt.s" "dac_characterTmp:Ring3_R_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Ring3_R_TmpJnt.s" "dac_characterTmp:Ring4_R_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Ring4_R_TmpJnt.s" "dac_characterTmp:Ring5_R_TmpJnt.is"
		;
connectAttr "dac_characterTmp:HandCupOuter_R_TmpJnt.s" "dac_characterTmp:Pinky1_R_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Pinky1_R_TmpJnt.s" "dac_characterTmp:Pinky2_R_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Pinky2_R_TmpJnt.s" "dac_characterTmp:Pinky3_R_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Pinky3_R_TmpJnt.s" "dac_characterTmp:Pinky4_R_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Pinky4_R_TmpJnt.s" "dac_characterTmp:Pinky5_R_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Forearm_R_TmpJnt.s" "dac_characterTmp:Elbow_R_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Root_TmpJnt.s" "dac_characterTmp:Pelvis_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Pelvis_TmpJnt.s" "dac_characterTmp:UpLeg_L_TmpJnt.is"
		;
connectAttr "dac_characterTmp:UpLeg_L_TmpJnt.s" "dac_characterTmp:LowLeg_L_TmpJnt.is"
		;
connectAttr "dac_characterTmp:LowLeg_L_TmpJnt.s" "dac_characterTmp:Ankle_L_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Ankle_L_TmpJnt.s" "dac_characterTmp:Ball_L_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Ball_L_TmpJnt.s" "dac_characterTmp:Toe_L_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Ball_L_TmpJnt.s" "dac_characterTmp:HeelIkTwistPiv_L_TmpJnt.is"
		;
connectAttr "dac_characterTmp:HeelIkTwistPiv_L_TmpJnt.s" "dac_characterTmp:Heel_L_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Ankle_L_TmpJnt.s" "dac_characterTmp:FootIn_L_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Ankle_L_TmpJnt.s" "dac_characterTmp:FootOut_L_TmpJnt.is"
		;
connectAttr "dac_characterTmp:LowLeg_L_TmpJnt.s" "dac_characterTmp:Knee_L_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Pelvis_TmpJnt.s" "dac_characterTmp:UpLeg_R_TmpJnt.is"
		;
connectAttr "dac_characterTmp:UpLeg_R_TmpJnt.s" "dac_characterTmp:LowLeg_R_TmpJnt.is"
		;
connectAttr "dac_characterTmp:LowLeg_R_TmpJnt.s" "dac_characterTmp:Ankle_R_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Ankle_R_TmpJnt.s" "dac_characterTmp:Ball_R_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Ball_R_TmpJnt.s" "dac_characterTmp:Toe_R_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Ball_R_TmpJnt.s" "dac_characterTmp:HeelIkTwistPiv_R_TmpJnt.is"
		;
connectAttr "dac_characterTmp:HeelIkTwistPiv_R_TmpJnt.s" "dac_characterTmp:Heel_R_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Ankle_R_TmpJnt.s" "dac_characterTmp:FootIn_R_TmpJnt.is"
		;
connectAttr "dac_characterTmp:Ankle_R_TmpJnt.s" "dac_characterTmp:FootOut_R_TmpJnt.is"
		;
connectAttr "dac_characterTmp:LowLeg_R_TmpJnt.s" "dac_characterTmp:Knee_R_TmpJnt.is"
		;
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of dac_characterTmp.ma
