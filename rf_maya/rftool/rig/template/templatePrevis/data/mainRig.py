import sys, os
import maya.cmds as mc
import pymel.core as pm
from ncmel import core
reload(core)
from ncmel import rigTools
reload(rigTools)
from ncmel import mainRig
reload(mainRig)
from ncmel import rootRig
reload(rootRig)
from ncmel import pelvisRig
reload(pelvisRig)
from ncmel import spineRig
reload(spineRig)
from ncmel import torsoRig
reload(torsoRig)
from ncmel import neckRig
reload(neckRig)
from ncmel import headRig
reload(headRig)
from ncmel import eyeRig
reload(eyeRig)
from ncmel import jawRig
reload(jawRig)
from ncmel import clavicleRig
reload(clavicleRig)
from ncmel import armRig
reload(armRig)
from utaTools.utapy import legRig
reload(legRig)
from ncmel import legAnimalRig
reload(legAnimalRig)
from utaTools.utapy import fingerRig
reload(fingerRig)
from ncmel import subRig
reload(subRig)
from ncmel import fkRig
reload(fkRig)
from ncmel import tailRig
reload(tailRig)
from utaTools.pkmel import ctrlShapeTools
reload(ctrlShapeTools)
from lpRig import pointRig
reload(pointRig)
from utaTools.utapy import utaCore
reload(utaCore)
from lpRig import rubberBandRig
reload(rubberBandRig)
from utaTools.utapy import charTextName
reload(charTextName)
from ncmel import rigHairDyn
reload(rigHairDyn)
from ncmel import ribbonRig
reload(ribbonRig)
from utaTools.utapy import utaCore
reload(utaCore)
from ncscript.rig import angleTools as agt
reload( agt )
from nuTools.rigTools import proc
reload(proc)
from utils.crvScript import ctrlData
reload(ctrlData)

def main( size = 0.75 ):
    
    print "##------------------------------------------------------------------------------------"
    print "##----------------------- M A I N R I G   F O R   A N I M ----------------------------"
    print "##------------------------------------------------------------------------------------"

    print "#01 Generate >> Main Group"
    main = mainRig.Run( 
                                    assetName = '' ,
                                    size      = size
                       )

    print "##------------------------------------------------------------------------------------"
    print "##---------------------------------- MAIN GROUP --------------------------------------"
    print "##------------------------------------------------------------------------------------"

    print "#02 Generate >> Root"
    root = rootRig.Run( 
                                    rootTmpJnt = 'Root_TmpJnt' ,
                                    ctrlGrp    =  main.mainCtrlGrp ,
                                    skinGrp    =  main.skinGrp ,
                                    size       =  size
                       )

    print "#03 Generate >> Torso"
    torso = torsoRig.Run(   
                                    pelvisTmpJnt = 'Pelvis_TmpJnt' ,
                                    spine1TmpJnt = 'Spine1_TmpJnt' ,
                                    spine2TmpJnt = 'Spine2_TmpJnt' ,
                                    spine3TmpJnt = 'Spine3_TmpJnt' ,
                                    spine4TmpJnt = 'Spine4_TmpJnt' ,
                                    spine5TmpJnt = 'Spine5_TmpJnt' ,
                                    parent       =  root.rootJnt ,
                                    ctrlGrp      =  main.mainCtrlGrp ,
                                    skinGrp      =  main.skinGrp ,
                                    jntGrp       =  main.jntGrp ,
                                    stillGrp     =  main.stillGrp ,
                                    elem         = '' ,
                                    axis         = 'y' ,
                                    size         =  size
                         )

    # print "# Generate >> ChestEnd Rig"
    # chestEnd = subRig.Run (         name       = 'ChestEnd' ,
    #                                 tmpJnt     = 'ChestEnd_TmpJnt',
    #                                 parent     = torso.jntDict[-1],
    #                                 ctrlGrp    = main.addRigCtrlGrp  ,
    #                                 shape      = 'circle' ,
    #                                 side       = '' ,
    #                                 size       = size )
    
    print "#04 Generate >> Neck"
    neck = neckRig.Run(    
                                    neckTmpJnt = 'Neck_TmpJnt' ,
                                    headTmpJnt = 'Head_TmpJnt' ,
                                    parent     = torso.jntDict[-1] ,
                                    ctrlGrp    =  main.mainCtrlGrp ,
                                    skinGrp    =  main.skinGrp ,
                                    elem       = '' ,
                                    side       = '' ,
                                    ribbon     = True ,
                                    head       = True ,
                                    axis       = 'y' ,
                                    size       = size
                       )
                     
    print "#05 Generate >> Head"
    head = headRig.Run(    
                                    headTmpJnt      = 'Head_TmpJnt' ,
                                    headEndTmpJnt   = 'HeadEnd_TmpJnt' ,
                                    eyeTrgTmpJnt    = 'EyeTrgt_TmpJnt' ,
                                    eyeLftTrgTmpJnt = 'EyeTrgt_L_TmpJnt' ,
                                    eyeRgtTrgTmpJnt = 'EyeTrgt_R_TmpJnt' ,
                                    eyeLftTmpJnt    = 'Eye_L_TmpJnt' ,
                                    eyeRgtTmpJnt    = 'Eye_R_TmpJnt' ,
                                    jawUpr1TmpJnt   = 'JawUpr1_TmpJnt' ,
                                    jawUprEndTmpJnt = 'JawUprEnd_TmpJnt' ,
                                    jawLwr1TmpJnt   = 'JawLwr1_TmpJnt' ,
                                    jawLwr2TmpJnt   = 'JawLwr2_TmpJnt' ,
                                    jawLwrEndTmpJnt = 'JawLwrEnd_TmpJnt' ,
                                    parent          =  neck.neckEndJnt ,
                                    ctrlGrp         =  main.mainCtrlGrp ,
                                    skinGrp         =  main.skinGrp ,
                                    elem            = '' ,
                                    side            = '' ,
                                    eyeRig          = False ,
                                    jawRig          = False ,
                                    size            = size 
                       )
                     
    print "#06 Generate >> Eye"
    eye = eyeRig.Run(    
                                    eyeTrgTmpJnt    = 'EyeTrgt_TmpJnt' ,
                                    eyeLftTrgTmpJnt = 'EyeTrgt_L_TmpJnt' ,
                                    eyeRgtTrgTmpJnt = 'EyeTrgt_R_TmpJnt' ,
                                    eyeLftTmpJnt    = 'Eye_L_TmpJnt' ,
                                    eyeRgtTmpJnt    = 'Eye_R_TmpJnt' ,
                                    parent          =  head.headJnt ,
                                    ctrlGrp         =  main.mainCtrlGrp ,
                                    skinGrp         =  main.skinGrp ,
                                    elem            = '' ,
                                    side            = '' ,
                                    size            = size 
                       )

    print "#07 Generate >> Jaw"
    jaw = jawRig.Run(    
                                    jawUpr1TmpJnt   = 'JawUpr1_TmpJnt' ,
                                    jawUprEndTmpJnt = 'JawUprEnd_TmpJnt' ,
                                    jawLwr1TmpJnt   = 'JawLwr1_TmpJnt' ,
                                    jawLwr2TmpJnt   = 'JawLwr2_TmpJnt' ,
                                    jawLwrEndTmpJnt = 'JawLwrEnd_TmpJnt' ,
                                    parent          =  head.headJnt ,
                                    ctrlGrp         =  main.mainCtrlGrp ,
                                    skinGrp         =  main.skinGrp ,
                                    elem            = '' ,
                                    side            = '' ,
                                    size            = size 
                       )


    print "#08 Generate >> Clavicle Left"
    clavL = clavicleRig.Run(   
                                    clavTmpJnt  = 'Clav_L_TmpJnt' ,
                                    upArmTmpJnt = 'UpArm_L_TmpJnt' ,
                                    parent      = torso.jntDict[-1] ,
                                    ctrlGrp     =  main.mainCtrlGrp ,
                                    jntGrp      =  main.jntGrp ,
                                    skinGrp     =  main.skinGrp ,
                                    elem        = '' ,
                                    side        = 'L' ,
                                    axis        = 'y' ,
                                    size        = size
                            )
    
    print "#09 Generate >> Clavicle Right"
    clavR = clavicleRig.Run(
                                    clavTmpJnt  = 'Clav_R_TmpJnt' ,
                                    upArmTmpJnt = 'UpArm_R_TmpJnt' ,
                                    parent      = torso.jntDict[-1] ,
                                    ctrlGrp     =  main.mainCtrlGrp ,
                                    jntGrp      =  main.jntGrp ,
                                    skinGrp     =  main.skinGrp ,
                                    elem        = '' ,
                                    side        = 'R' ,
                                    axis        = 'y' ,
                                    size        = size
                            )
     
    print "#10 Generate >> Arm Left"
    armL = armRig.Run(              upArmTmpJnt   = 'UpArm_L_TmpJnt' ,
                                    forearmTmpJnt = 'Forearm_L_TmpJnt' ,
                                    wristTmpJnt   = 'Wrist_L_TmpJnt' ,
                                    handTmpJnt    = 'Hand_L_TmpJnt' ,
                                    elbowTmpJnt   = 'Elbow_L_TmpJnt' ,
                                    parent        = 'ClavEnd_L_Jnt' , 
                                    ctrlGrp       =  main.mainCtrlGrp ,
                                    jntGrp        =  main.jntGrp ,
                                    skinGrp       =  main.skinGrp ,
                                    stillGrp      =  main.stillGrp ,
                                    ikhGrp        =  main.ikhGrp ,
                                    elem          = '' ,
                                    side          = 'L' ,
                                    ribbon        = True ,
                                    size          = size
                    )

    rigTools.addMoreSpace( obj = armL.wristIkLocWor ,
                           spaces = [ ('Shoulder' , armL.wristIkLocWor['localSpace'] ) ,
                                      ('Head', head.headJnt ) ,
                                      ('Chest', torso.jntDict[-1] ) ,
                                      ('Pelvis', torso.pelvisJnt ) ] )
    
    print "#11 Generate >> Arm Right"
    armR = armRig.Run(              
                                    upArmTmpJnt   = 'UpArm_R_TmpJnt' ,
                                    forearmTmpJnt = 'Forearm_R_TmpJnt' ,
                                    wristTmpJnt   = 'Wrist_R_TmpJnt' ,
                                    handTmpJnt    = 'Hand_R_TmpJnt' ,
                                    elbowTmpJnt   = 'Elbow_R_TmpJnt' ,
                                    parent        = 'ClavEnd_R_Jnt' , 
                                    ctrlGrp       =  main.mainCtrlGrp ,
                                    jntGrp        =  main.jntGrp ,
                                    skinGrp       =  main.skinGrp ,
                                    stillGrp      =  main.stillGrp ,
                                    ikhGrp        =  main.ikhGrp ,
                                    elem          = '' ,
                                    side          = 'R' ,
                                    ribbon        = True ,
                                    size          = size
                    )

    rigTools.addMoreSpace( obj = armR.wristIkLocWor ,
                           spaces = [ ('Shoulder' , armR.wristIkLocWor['localSpace'] ) ,
                                      ('Head', head.headJnt ) ,
                                      ('Chest', torso.jntDict[-1] ) ,
                                      ('Pelvis', torso.pelvisJnt ) ] )
    
    print "#12 Generate >> Leg Left"
    legL = legRig.Run(              
                                    upLegTmpJnt   = 'UpLeg_L_TmpJnt' ,
                                    lowLegTmpJnt  = 'LowLeg_L_TmpJnt' ,
                                    ankleTmpJnt   = 'Ankle_L_TmpJnt' ,
                                    ballTmpJnt    = 'Ball_L_TmpJnt' ,
                                    toeTmpJnt     = 'Toe_L_TmpJnt' ,
                                    heelTmpJnt    = 'Heel_L_TmpJnt' ,
                                    footInTmpJnt  = 'FootIn_L_TmpJnt' ,
                                    footOutTmpJnt = 'FootOut_L_TmpJnt' ,
                                    kneeTmpJnt    = 'Knee_L_TmpJnt' ,
                                    heelIkTwistPivTmpJnt = 'HeelIkTwistPiv_L_TmpJnt',
                                    parent        =  torso.pelvisJnt , 
                                    ctrlGrp       =  main.mainCtrlGrp ,
                                    jntGrp        =  main.jntGrp ,
                                    skinGrp       =  main.skinGrp ,
                                    stillGrp      =  main.stillGrp ,
                                    ikhGrp        =  main.ikhGrp ,
                                    elem          = '' ,
                                    side          = 'L' ,
                                    ribbon        = True ,
                                    foot          = True ,
                                    kneePos       = 'KneePos_L_TmpJnt' ,
                                    size          = size
                    )
    
    rigTools.addMoreSpace( obj = legL.kneeIkLocWor ,
                           spaces = [ ('Ankle' , legL.kneeIkLocWor['localSpace'] ) ,
                                      ('Root', root.rootJnt )] )

    print "#13 Generate >> Leg Right"
    legR = legRig.Run(              
                                    upLegTmpJnt   = 'UpLeg_R_TmpJnt' ,
                                    lowLegTmpJnt  = 'LowLeg_R_TmpJnt' ,
                                    ankleTmpJnt   = 'Ankle_R_TmpJnt' ,
                                    ballTmpJnt    = 'Ball_R_TmpJnt' ,
                                    toeTmpJnt     = 'Toe_R_TmpJnt' ,
                                    heelTmpJnt    = 'Heel_R_TmpJnt' ,
                                    footInTmpJnt  = 'FootIn_R_TmpJnt' ,
                                    footOutTmpJnt = 'FootOut_R_TmpJnt' ,
                                    kneeTmpJnt    = 'Knee_R_TmpJnt' ,
                                    heelIkTwistPivTmpJnt = 'HeelIkTwistPiv_R_TmpJnt',
                                    parent        =  torso.pelvisJnt , 
                                    ctrlGrp       =  main.mainCtrlGrp ,
                                    jntGrp        =  main.jntGrp ,
                                    skinGrp       =  main.skinGrp ,
                                    stillGrp      =  main.stillGrp ,
                                    ikhGrp        =  main.ikhGrp ,
                                    elem          = '' ,
                                    side          = 'R' ,
                                    ribbon        = True ,
                                    foot          = True ,
                                    kneePos     = 'KneePos_R_TmpJnt' ,
                                    size          = size
                    )

    rigTools.addMoreSpace( obj = legR.kneeIkLocWor ,
                           spaces = [ ('Ankle' , legR.kneeIkLocWor['localSpace'] ) ,
                                      ('Root', root.rootJnt )] )




    print "#14 Generate >> EyeDot Left"
    EyeDotL = subRig.Run(
                                    name       = 'EyeDot' ,
                                    tmpJnt     = 'EyeDot_L_TmpJnt' ,
                                    parent     = eye.eyeLftJnt ,
                                    ctrlGrp    = main.mainCtrlGrp ,
                                    shape      = 'circle' ,
                                    side       = 'L' ,
                                    size       = size  
                                )

    print "#15 Generate >> EyeDot Right"
    EyeDotR = subRig.Run(
                                    name       = 'EyeDot' ,
                                    tmpJnt     = 'EyeDot_R_TmpJnt' ,
                                    parent     = eye.eyeRgtJnt ,
                                    ctrlGrp    = main.mainCtrlGrp ,
                                    shape      = 'circle' ,
                                    side       = 'R' ,
                                    size       = size  
                                )

    print "#16 Generate >> Iris Left Rig"
    IrisRigObj_L = subRig.Run(
                                    name       = 'Iris' ,
                                    tmpJnt     = 'Iris_L_TmpJnt' ,
                                    parent     = eye.eyeLftJnt ,
                                    ctrlGrp    = main.mainCtrlGrp ,
                                    shape      = 'circle' ,
                                    side       = 'L' ,
                                    size       = size  
                                )

    print "#17 Generate >> Iris Right Rig"
    IrisRigObj_R = subRig.Run(
                                    name       = 'Iris' ,
                                    tmpJnt     = 'Iris_R_TmpJnt' ,
                                    parent     = eye.eyeRgtJnt ,
                                    ctrlGrp    = main.mainCtrlGrp ,
                                    shape      = 'circle' ,
                                    side       = 'R' ,
                                    size       = size  
                                )

    print "#18 Generate >> Pupil Left Rig"
    PupilRigObj_L = subRig.Run(
                                    name       = 'Pupil' ,
                                    tmpJnt     = 'Pupil_L_TmpJnt' ,
                                    parent     = 'IrisSub_L_Jnt'  ,
                                    ctrlGrp    =  main.mainCtrlGrp ,
                                    shape      = 'circle' ,
                                    side       = 'L' ,
                                    size       = size  
                                )
    print "#19 Generate >> Pupil Right Rig"
    PupilRigObj_R = subRig.Run(
                                    name       = 'Pupil' ,
                                    tmpJnt     = 'Pupil_R_TmpJnt' ,
                                    parent     = 'IrisSub_R_Jnt'  ,
                                    ctrlGrp    =  main.mainCtrlGrp ,
                                    shape      = 'circle' ,
                                    side       = 'R' ,
                                    size       = size  
                                )
    print "#20 Generate >> Hand Cup Inner Left"
    handCupInnerL = fingerRig.HandCup( name       = 'HandCupInner' ,
                                    handCupTmpJnt = 'HandCupInner_L_TmpJnt' ,
                                    armCtrl       = armL.armCtrl ,
                                    parent        = armL.handJnt ,
                                    ctrlGrp       = 'MainCtrl_Grp' ,
                                    elem          = '' ,
                                    side          = 'L' ,
                                )
    print "#21 Generate >> Hand Cup Outer Left"
    handCupOuterL = fingerRig.HandCup( name       = 'HandCupOuter' ,
                                    handCupTmpJnt = 'HandCupOuter_L_TmpJnt' ,
                                    armCtrl       = armL.armCtrl ,
                                    parent        = armL.handJnt ,
                                    elem          = '' ,
                                    side          = 'L' ,
                                )
    print "#22 Generate >> Thumb Left"
    fngrThumbL = fingerRig.Run(         
                                    fngr          = 'Thumb' ,
                                    fngrTmpJnt    =['Thumb1_L_TmpJnt' , 
                                                    'Thumb2_L_TmpJnt' , 
                                                    'Thumb3_L_TmpJnt' , 
                                                    'Thumb4_L_TmpJnt' ] ,
                                    parent        =  armL.handJnt ,
                                    armCtrl       =  armL.armCtrl ,
                                    innerCup      = armL.handJnt , 
                                    outerCup      = '' ,
                                    innerValue    = 1 , 
                                    outerValue    = 0 ,
                                    ctrlGrp       =  main.mainCtrlGrp ,
                                    elem          = '' ,
                                    side          = 'L' ,
                                    size          = size 
                                )


    finger = 'Thumb'
    ctrlShape = core.Dag(armL.armCtrl.shape)
    ctrlShape.attr('%s2FistRx' %finger).value = -4.5
    ctrlShape.attr('%s3FistRx' %finger).value = -9
    ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
    ctrlShape.attr('%s3SlideRx' %finger).value = -8
    ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
    ctrlShape.attr('%s3ScruchRx' %finger).value = -8

    print "#23 Generate >> Index Left"
    fngrIndexL = fingerRig.Run(         
                                    fngr          = 'Index' ,
                                    fngrTmpJnt    =['Index1_L_TmpJnt' , 
                                                    'Index2_L_TmpJnt' , 
                                                    'Index3_L_TmpJnt' , 
                                                    'Index4_L_TmpJnt' , 
                                                    'Index5_L_TmpJnt' ] ,
                                    parent        =  handCupInnerL.handCupJnt ,
                                    armCtrl       =  armL.armCtrl ,
                                    innerCup      = handCupInnerL.handCupJnt , 
                                    outerCup      = handCupOuterL.handCupJnt ,
                                    innerValue    = 1 , 
                                    outerValue    = 0 ,
                                    ctrlGrp       =  main.mainCtrlGrp ,
                                    elem          = '' ,
                                    side          = 'L' ,
                                    size          = size 
                                )

    finger = 'Index'
    ctrlShape = core.Dag(armL.armCtrl.shape)
    ctrlShape.attr('%s2FistRx' %finger).value = -9
    ctrlShape.attr('%s3FistRx' %finger).value = -9
    ctrlShape.attr('%s4FistRx' %finger).value = -9
    ctrlShape.attr('%s2RelaxRx' %finger).value = -1
    ctrlShape.attr('%s3RelaxRx' %finger).value = -1
    ctrlShape.attr('%s4RelaxRx' %finger).value = -1
    ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
    ctrlShape.attr('%s3SlideRx' %finger).value = -8
    ctrlShape.attr('%s4SlideRx' %finger).value = 4.5
    ctrlShape.attr('%s1ScruchRx' %finger).value = 1.5
    ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
    ctrlShape.attr('%s3ScruchRx' %finger).value = -8
    ctrlShape.attr('%s4ScruchRx' %finger).value = -8
    ctrlShape.attr('%sBaseSpread' %finger).value = -4
    ctrlShape.attr('%sSpread' %finger).value = -4
    ctrlShape.attr('%sBaseBreak' %finger).value = -4.5
    ctrlShape.attr('%sBreak' %finger).value = -4.5
    ctrlShape.attr('%sBaseFlex' %finger).value = -9
    ctrlShape.attr('%sFlex' %finger).value = -9

    print "#24 Generate >> Middle Left"
    fngrMiddleL = fingerRig.Run(         
                                    fngr          = 'Middle' ,
                                    fngrTmpJnt    =['Middle1_L_TmpJnt' , 
                                                    'Middle2_L_TmpJnt' , 
                                                    'Middle3_L_TmpJnt' , 
                                                    'Middle4_L_TmpJnt' , 
                                                    'Middle5_L_TmpJnt' ] ,
                                    parent        =  handCupInnerL.handCupJnt ,
                                    armCtrl       =  armL.armCtrl ,
                                    innerCup      = handCupInnerL.handCupJnt , 
                                    outerCup      = handCupOuterL.handCupJnt ,
                                    innerValue    = 1 , 
                                    outerValue    = 0.5 ,
                                    ctrlGrp       =  main.mainCtrlGrp ,
                                    elem          = '' ,
                                    side          = 'L' ,
                                    size          = size 
                                )

    finger = 'Middle'
    ctrlShape = core.Dag(armL.armCtrl.shape)
    ctrlShape.attr('%s2FistRx' %finger).value = -9
    ctrlShape.attr('%s3FistRx' %finger).value = -9
    ctrlShape.attr('%s4FistRx' %finger).value = -9
    ctrlShape.attr('%s1RelaxRx' %finger).value = -0.5
    ctrlShape.attr('%s2RelaxRx' %finger).value = -2.5
    ctrlShape.attr('%s3RelaxRx' %finger).value = -2.5
    ctrlShape.attr('%s4RelaxRx' %finger).value = -2.5
    ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
    ctrlShape.attr('%s3SlideRx' %finger).value = -8
    ctrlShape.attr('%s4SlideRx' %finger).value = 4.5
    ctrlShape.attr('%s1ScruchRx' %finger).value = 1.5
    ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
    ctrlShape.attr('%s3ScruchRx' %finger).value = -8
    ctrlShape.attr('%s4ScruchRx' %finger).value = -8
    ctrlShape.attr('%sBaseBreak' %finger).value = -4.5
    ctrlShape.attr('%sBreak' %finger).value = -4.5
    ctrlShape.attr('%sBaseFlex' %finger).value = -9
    ctrlShape.attr('%sFlex' %finger).value = -9

    print "#25 Generate >> Ring Left"
    fngrRingL = fingerRig.Run(         
                                    fngr          = 'Ring' ,
                                    fngrTmpJnt    =['Ring1_L_TmpJnt' , 
                                                    'Ring2_L_TmpJnt' , 
                                                    'Ring3_L_TmpJnt' , 
                                                    'Ring4_L_TmpJnt' , 
                                                    'Ring5_L_TmpJnt' ] ,
                                    parent        =  handCupOuterL.handCupJnt ,
                                    armCtrl       =  armL.armCtrl ,
                                    innerCup      = handCupInnerL.handCupJnt , 
                                    outerCup      = handCupOuterL.handCupJnt ,
                                    innerValue    = 0.5 , 
                                    outerValue    = 1 ,
                                    ctrlGrp       =  main.mainCtrlGrp ,
                                    elem          = '' ,
                                    side          = 'L' ,
                                    size          = size 
                                )

    finger = 'Ring'
    ctrlShape = core.Dag(armL.armCtrl.shape)
    ctrlShape.attr('%s2FistRx' %finger).value = -9
    ctrlShape.attr('%s3FistRx' %finger).value = -9
    ctrlShape.attr('%s4FistRx' %finger).value = -9
    ctrlShape.attr('%s1RelaxRx' %finger).value = -0.5
    ctrlShape.attr('%s2RelaxRx' %finger).value = -2.5
    ctrlShape.attr('%s3RelaxRx' %finger).value = -2.5
    ctrlShape.attr('%s4RelaxRx' %finger).value = -2.5
    ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
    ctrlShape.attr('%s3SlideRx' %finger).value = -8
    ctrlShape.attr('%s4SlideRx' %finger).value = 4.5
    ctrlShape.attr('%s1ScruchRx' %finger).value = 1.5
    ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
    ctrlShape.attr('%s3ScruchRx' %finger).value = -8
    ctrlShape.attr('%s4ScruchRx' %finger).value = -8
    ctrlShape.attr('%sBaseBreak' %finger).value = -4.5
    ctrlShape.attr('%sBreak' %finger).value = -4.5
    ctrlShape.attr('%sBaseFlex' %finger).value = -9
    ctrlShape.attr('%sFlex' %finger).value = -9

    print "#26 Generate >> Pinky Left"
    fngrPinkyL = fingerRig.Run(         
                                    fngr          = 'Pinky' ,
                                    fngrTmpJnt    =['Pinky1_L_TmpJnt' , 
                                                    'Pinky2_L_TmpJnt' , 
                                                    'Pinky3_L_TmpJnt' , 
                                                    'Pinky4_L_TmpJnt' , 
                                                    'Pinky5_L_TmpJnt' ] ,
                                    parent        =  handCupOuterL.handCupJnt ,
                                    armCtrl       =  armL.armCtrl ,
                                    innerCup      = handCupInnerL.handCupJnt , 
                                    outerCup      = handCupOuterL.handCupJnt ,
                                    innerValue    = 0 , 
                                    outerValue    = 1 ,
                                    ctrlGrp       =  main.mainCtrlGrp ,
                                    elem          = '' ,
                                    side          = 'L' ,
                                    size          = size 
                                )

    finger = 'Pinky'
    ctrlShape = core.Dag(armL.armCtrl.shape)
    ctrlShape.attr('%s2FistRx' %finger).value = -9
    ctrlShape.attr('%s3FistRx' %finger).value = -9
    ctrlShape.attr('%s4FistRx' %finger).value = -9
    ctrlShape.attr('%s1RelaxRx' %finger).value = -2
    ctrlShape.attr('%s2RelaxRx' %finger).value = -3
    ctrlShape.attr('%s3RelaxRx' %finger).value = -3
    ctrlShape.attr('%s4RelaxRx' %finger).value = -3
    ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
    ctrlShape.attr('%s3SlideRx' %finger).value = -8
    ctrlShape.attr('%s4SlideRx' %finger).value = 4.5
    ctrlShape.attr('%s1ScruchRx' %finger).value = 1.5
    ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
    ctrlShape.attr('%s3ScruchRx' %finger).value = -8
    ctrlShape.attr('%s4ScruchRx' %finger).value = -8
    ctrlShape.attr('%sBaseSpread' %finger).value = 5
    ctrlShape.attr('%sSpread' %finger).value = 5
    ctrlShape.attr('%sBaseBreak' %finger).value = -4.5
    ctrlShape.attr('%sBreak' %finger).value = -4.5
    ctrlShape.attr('%sBaseFlex' %finger).value = -9
    ctrlShape.attr('%sFlex' %finger).value = -9

    print "#27 Generate >> Hand Cup Inner Right"
    handCupInnerR = fingerRig.HandCup( name       = 'HandCupInner' ,
                                    handCupTmpJnt = 'HandCupInner_R_TmpJnt' ,
                                    armCtrl       = armR.armCtrl ,
                                    parent        = armR.handJnt ,
                                    elem          = '' ,
                                    side          = 'R' ,
                                )
                                
    print "#28 Generate >> Hand Cup Outer Right"
    handCupOuterR = fingerRig.HandCup( name       = 'HandCupOuter' ,
                                    handCupTmpJnt = 'HandCupOuter_R_TmpJnt' ,
                                    armCtrl       = armR.armCtrl ,
                                    parent        = armR.handJnt ,
                                    elem          = '' ,
                                    side          = 'R' ,
                                )

    print "#29 Generate >> Thumb Right"
    fngrThumbR = fingerRig.Run(         
                                    fngr          = 'Thumb' ,
                                    fngrTmpJnt    =['Thumb1_R_TmpJnt' , 
                                                    'Thumb2_R_TmpJnt' , 
                                                    'Thumb3_R_TmpJnt' , 
                                                    'Thumb4_R_TmpJnt' ] ,
                                    parent        =  armR.handJnt ,
                                    armCtrl       =  armR.armCtrl ,
                                    innerCup      = armR.handJnt , 
                                    outerCup      = '' ,
                                    innerValue    = 1 , 
                                    outerValue    = 0 ,
                                    ctrlGrp       =  main.mainCtrlGrp ,
                                    elem          = '' ,
                                    side          = 'R' ,
                                    size          = size 
                                )


    finger = 'Thumb'
    ctrlShape = core.Dag(armR.armCtrl.shape)
    ctrlShape.attr('%s2FistRx' %finger).value = -4.5
    ctrlShape.attr('%s3FistRx' %finger).value = -9
    ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
    ctrlShape.attr('%s3SlideRx' %finger).value = -8
    ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
    ctrlShape.attr('%s3ScruchRx' %finger).value = -8

    print "#30 Generate >> Index Right"
    fngrIndexL = fingerRig.Run(         
                                    fngr          = 'Index' ,
                                    fngrTmpJnt    =['Index1_R_TmpJnt' , 
                                                    'Index2_R_TmpJnt' , 
                                                    'Index3_R_TmpJnt' , 
                                                    'Index4_R_TmpJnt' , 
                                                    'Index5_R_TmpJnt' ] ,
                                    parent        =  handCupInnerR.handCupJnt ,
                                    armCtrl       =  armR.armCtrl ,
                                    innerCup      = handCupInnerR.handCupJnt , 
                                    outerCup      = handCupOuterR.handCupJnt ,
                                    innerValue    = 1 , 
                                    outerValue    = 0 ,
                                    ctrlGrp       =  main.mainCtrlGrp ,
                                    elem          = '' ,
                                    side          = 'R' ,
                                    size          = size 
                                )

    finger = 'Index'
    ctrlShape = core.Dag(armR.armCtrl.shape)
    ctrlShape.attr('%s2FistRx' %finger).value = -9
    ctrlShape.attr('%s3FistRx' %finger).value = -9
    ctrlShape.attr('%s4FistRx' %finger).value = -9
    ctrlShape.attr('%s2RelaxRx' %finger).value = -1
    ctrlShape.attr('%s3RelaxRx' %finger).value = -1
    ctrlShape.attr('%s4RelaxRx' %finger).value = -1
    ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
    ctrlShape.attr('%s3SlideRx' %finger).value = -8
    ctrlShape.attr('%s4SlideRx' %finger).value = 4.5
    ctrlShape.attr('%s1ScruchRx' %finger).value = 1.5
    ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
    ctrlShape.attr('%s3ScruchRx' %finger).value = -8
    ctrlShape.attr('%s4ScruchRx' %finger).value = -8
    ctrlShape.attr('%sBaseSpread' %finger).value = -4
    ctrlShape.attr('%sSpread' %finger).value = -4
    ctrlShape.attr('%sBaseBreak' %finger).value = -4.5
    ctrlShape.attr('%sBreak' %finger).value = -4.5
    ctrlShape.attr('%sBaseFlex' %finger).value = -9
    ctrlShape.attr('%sFlex' %finger).value = -9

    print "#31 Generate >> Middle Right"
    fngrMiddleL = fingerRig.Run(         
                                    fngr          = 'Middle' ,
                                    fngrTmpJnt    =['Middle1_R_TmpJnt' , 
                                                    'Middle2_R_TmpJnt' , 
                                                    'Middle3_R_TmpJnt' , 
                                                    'Middle4_R_TmpJnt' , 
                                                    'Middle5_R_TmpJnt' ] ,
                                    parent        =  handCupInnerR.handCupJnt ,
                                    armCtrl       =  armR.armCtrl ,
                                    innerCup      = handCupInnerR.handCupJnt , 
                                    outerCup      = handCupOuterR.handCupJnt ,
                                    innerValue    = 1 , 
                                    outerValue    = 0.5 ,
                                    ctrlGrp       =  main.mainCtrlGrp ,
                                    elem          = '' ,
                                    side          = 'R' ,
                                    size          = size 
                                )

    finger = 'Middle'
    ctrlShape = core.Dag(armR.armCtrl.shape)
    ctrlShape.attr('%s2FistRx' %finger).value = -9
    ctrlShape.attr('%s3FistRx' %finger).value = -9
    ctrlShape.attr('%s4FistRx' %finger).value = -9
    ctrlShape.attr('%s1RelaxRx' %finger).value = -0.5
    ctrlShape.attr('%s2RelaxRx' %finger).value = -2.5
    ctrlShape.attr('%s3RelaxRx' %finger).value = -2.5
    ctrlShape.attr('%s4RelaxRx' %finger).value = -2.5
    ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
    ctrlShape.attr('%s3SlideRx' %finger).value = -8
    ctrlShape.attr('%s4SlideRx' %finger).value = 4.5
    ctrlShape.attr('%s1ScruchRx' %finger).value = 1.5
    ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
    ctrlShape.attr('%s3ScruchRx' %finger).value = -8
    ctrlShape.attr('%s4ScruchRx' %finger).value = -8
    ctrlShape.attr('%sBaseBreak' %finger).value = -4.5
    ctrlShape.attr('%sBreak' %finger).value = -4.5
    ctrlShape.attr('%sBaseFlex' %finger).value = -9
    ctrlShape.attr('%sFlex' %finger).value = -9

    print "#32 Generate >> Ring Right"
    fngrRingL = fingerRig.Run(         
                                        fngr          = 'Ring' ,
                                        fngrTmpJnt    =['Ring1_R_TmpJnt' , 
                                                        'Ring2_R_TmpJnt' , 
                                                        'Ring3_R_TmpJnt' , 
                                                        'Ring4_R_TmpJnt' , 
                                                        'Ring5_R_TmpJnt' ] ,
                                        parent        =  handCupOuterR.handCupJnt ,
                                        armCtrl       =  armR.armCtrl ,
                                        innerCup      = handCupInnerR.handCupJnt , 
                                        outerCup      = handCupOuterR.handCupJnt ,
                                        innerValue    = 0.5 , 
                                        outerValue    = 1 ,
                                        ctrlGrp       =  main.mainCtrlGrp ,
                                        elem          = '' ,
                                        side          = 'R' ,
                                        size          = size 
                                    )

    finger = 'Ring'
    ctrlShape = core.Dag(armR.armCtrl.shape)
    ctrlShape.attr('%s2FistRx' %finger).value = -9
    ctrlShape.attr('%s3FistRx' %finger).value = -9
    ctrlShape.attr('%s4FistRx' %finger).value = -9
    ctrlShape.attr('%s1RelaxRx' %finger).value = -0.5
    ctrlShape.attr('%s2RelaxRx' %finger).value = -2.5
    ctrlShape.attr('%s3RelaxRx' %finger).value = -2.5
    ctrlShape.attr('%s4RelaxRx' %finger).value = -2.5
    ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
    ctrlShape.attr('%s3SlideRx' %finger).value = -8
    ctrlShape.attr('%s4SlideRx' %finger).value = 4.5
    ctrlShape.attr('%s1ScruchRx' %finger).value = 1.5
    ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
    ctrlShape.attr('%s3ScruchRx' %finger).value = -8
    ctrlShape.attr('%s4ScruchRx' %finger).value = -8
    ctrlShape.attr('%sBaseBreak' %finger).value = -4.5
    ctrlShape.attr('%sBreak' %finger).value = -4.5
    ctrlShape.attr('%sBaseFlex' %finger).value = -9
    ctrlShape.attr('%sFlex' %finger).value = -9
    
    print "#33 Generate >> Pinky Right"
    fngrPinkyL = fingerRig.Run(         
                                        fngr          = 'Pinky' ,
                                        fngrTmpJnt    =['Pinky1_R_TmpJnt' , 
                                                        'Pinky2_R_TmpJnt' , 
                                                        'Pinky3_R_TmpJnt' , 
                                                        'Pinky4_R_TmpJnt' , 
                                                        'Pinky5_R_TmpJnt' ] ,
                                        parent        =  handCupOuterR.handCupJnt ,
                                        armCtrl       =  armR.armCtrl ,
                                        innerCup      = handCupInnerR.handCupJnt , 
                                        outerCup      = handCupOuterR.handCupJnt ,
                                        innerValue    = 0 , 
                                        outerValue    = 1 ,
                                        ctrlGrp       =  main.mainCtrlGrp ,
                                        elem          = '' ,
                                        side          = 'R' ,
                                        size          = size 
                                    )

    finger = 'Pinky'
    ctrlShape = core.Dag(armR.armCtrl.shape)
    ctrlShape.attr('%s2FistRx' %finger).value = -9
    ctrlShape.attr('%s3FistRx' %finger).value = -9
    ctrlShape.attr('%s4FistRx' %finger).value = -9
    ctrlShape.attr('%s1RelaxRx' %finger).value = -2
    ctrlShape.attr('%s2RelaxRx' %finger).value = -3
    ctrlShape.attr('%s3RelaxRx' %finger).value = -3
    ctrlShape.attr('%s4RelaxRx' %finger).value = -3
    ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
    ctrlShape.attr('%s3SlideRx' %finger).value = -8
    ctrlShape.attr('%s4SlideRx' %finger).value = 4.5
    ctrlShape.attr('%s1ScruchRx' %finger).value = 1.5
    ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
    ctrlShape.attr('%s3ScruchRx' %finger).value = -8
    ctrlShape.attr('%s4ScruchRx' %finger).value = -8
    ctrlShape.attr('%sBaseSpread' %finger).value = 5
    ctrlShape.attr('%sSpread' %finger).value = 5
    ctrlShape.attr('%sBaseBreak' %finger).value = -4.5
    ctrlShape.attr('%sBreak' %finger).value = -4.5
    ctrlShape.attr('%sBaseFlex' %finger).value = -9
    ctrlShape.attr('%sFlex' %finger).value = -9


    print '##------------------------------------------------------------------------------------'
    print '##-----------------------------ADD RIG GROUP------------------------------------------'
    print '##------------------------------------------------------------------------------------'

    ## JawUprRoot_Jnt for hdSquash, Stretch -----------------------------------------
    ## JawUprRoot_Jnt for hdSquash, Stretch -----------------------------------------

    print "# Generate >> JawUprRoot"
    jawUprRoot = subRig.Run(              
                                    name       = 'JawUprRoot' ,
                                    tmpJnt     = 'JawUprRoot_TmpJnt' ,
                                    parent     = head.headJnt ,
                                    ctrlGrp    = main.addRigCtrlGrp ,
                                    shape      = 'square' ,
                                    side       = '' ,
                                    size       = size 
                            )
    mc.setAttr('JawUprRootSubCtrl_Grp.v', 0)
    utaCore.lockAttr(listObj = ['JawUpr1CtrlZro_Grp'], attrs = ['tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v'],lock = False,keyable = True)
    utaCore.parentScaleConstraintLoop(obj = 'JawUprRootSub_Jnt', lists = ['JawUpr1CtrlZro_Grp'],par = True, sca = True, ori = False, poi = False)
    utaCore.lockAttr(listObj = ['JawUpr1CtrlZro_Grp', 'JawUprRootSubCtrlPars_Grp', 'JawUprRootSub_Ctrl', 'JawUprRootSubGmbl_Ctrl'], attrs = ['tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v'],lock = True,keyable = True)
    utaCore.lockAttr(listObj = ['JawUprRootSub_Ctrl'], attrs = ['rx'],lock = False,keyable = True)


    # print "# Generate >> HairA Rig"
    # HairA = fkRig.Run (             name       = 'HairA' ,
    #                                 tmpJnt     = [  'HairA1_TmpJnt', 
    #                                                 'HairA2_TmpJnt', 
    #                                                 'HairA3_TmpJnt',
    #                                                 'HairA4_TmpJnt',
    #                                                 'HairA5_TmpJnt', ] ,
    #                                 parent     = 'Head_Jnt' ,
    #                                 ctrlGrp    = main.addRigCtrlGrp  ,
    #                                 axis       = 'y' ,
    #                                 shape      = 'circle' ,
    #                                 side       = '' ,
    #                                 size       = size )

    # print "# Generate >> HairB Rig"
    # HairB = fkRig.Run (             name       = 'HairB' ,
    #                                 tmpJnt     = [  'HairB1_TmpJnt', 
    #                                                 'HairB2_TmpJnt', 
    #                                                 'HairB3_TmpJnt',
    #                                                 'HairB4_TmpJnt',
    #                                                 'HairB5_TmpJnt', ] ,
    #                                 parent     = 'Head_Jnt' ,
    #                                 ctrlGrp    = main.addRigCtrlGrp  ,
    #                                 axis       = 'y' ,
    #                                 shape      = 'circle' ,
    #                                 side       = '' ,
    #                                 size       = size )

    # print "# Generate >> HairC Rig"
    # HairC = fkRig.Run (             name       = 'HairC' ,
    #                                 tmpJnt     = [  'HairC1_TmpJnt', 
    #                                                 'HairC2_TmpJnt', 
    #                                                 'HairC3_TmpJnt',
    #                                                 'HairC4_TmpJnt',
    #                                                 'HairC5_TmpJnt',  ] ,
    #                                 parent     = 'Head_Jnt' ,
    #                                 ctrlGrp    = main.addRigCtrlGrp  ,
    #                                 axis       = 'y' ,
    #                                 shape      = 'circle' ,
    #                                 side       = '' ,
    #                                 size       = size )

    # print "# Generate >> HairD Rig"
    # HairD = fkRig.Run (             name       = 'HairD' ,
    #                                 tmpJnt     = [  'HairD1_TmpJnt', 
    #                                                 'HairD2_TmpJnt', 
    #                                                 'HairD3_TmpJnt',
    #                                                 'HairD4_TmpJnt',  
    #                                                 'HairD5_TmpJnt', ] ,
    #                                 parent     = 'Head_Jnt' ,
    #                                 ctrlGrp    = main.addRigCtrlGrp  ,
    #                                 axis       = 'y' ,
    #                                 shape      = 'circle' ,
    #                                 side       = '' ,
    #                                 size       = size )


    # print "# Generate >> HairE Rig"
    # HairE = fkRig.Run (             name       = 'HairE' ,
    #                                 tmpJnt     = [  'HairE1_TmpJnt', 
    #                                                 'HairE2_TmpJnt', 
    #                                                 'HairE3_TmpJnt',
    #                                                 'HairE4_TmpJnt',  
    #                                             ] ,
    #                                 parent     = 'Head_Jnt' ,
    #                                 ctrlGrp    = main.addRigCtrlGrp  ,
    #                                 axis       = 'y' ,
    #                                 shape      = 'circle' ,
    #                                 side       = '' ,
    #                                 size       = size )

    # print "# Generate >> HairF Rig"
    # HairF = fkRig.Run (             name       = 'HairF' ,
    #                                 tmpJnt     = [  'HairF1_TmpJnt', 
    #                                                 'HairF2_TmpJnt', 
    #                                                 'HairF3_TmpJnt',
    #                                                 'HairF4_TmpJnt',  
    #                                                 'HairF5_TmpJnt', 
    #                                                  ] ,
    #                                 parent     = 'Head_Jnt' ,
    #                                 ctrlGrp    = main.addRigCtrlGrp  ,
    #                                 axis       = 'y' ,
    #                                 shape      = 'circle' ,
    #                                 side       = '' ,
    #                                 size       = size )
    # print "# Generate >> HairG Rig"
    # HairG = fkRig.Run (             name       = 'HairG' ,
    #                                 tmpJnt     = [  'HairG1_TmpJnt', 
    #                                                 'HairG2_TmpJnt', 
    #                                                 'HairG3_TmpJnt',
    #                                                 'HairG4_TmpJnt',  
    #                                                  ] ,
    #                                 parent     = 'Head_Jnt' ,
    #                                 ctrlGrp    = main.addRigCtrlGrp  ,
    #                                 axis       = 'y' ,
    #                                 shape      = 'circle' ,
    #                                 side       = '' ,
    #                                 size       = size )

    # print "# Generate >> HairH Rig"
    # HairH = fkRig.Run (             name       = 'HairH' ,
    #                                 tmpJnt     = [  'HairH1_TmpJnt', 
    #                                                 'HairH2_TmpJnt', 
    #                                                 'HairH3_TmpJnt',
    #                                                 'HairH4_TmpJnt', 
    #                                                 'HairH5_TmpJnt', 
    #                                                  ] ,
    #                                 parent     = 'Head_Jnt' ,
    #                                 ctrlGrp    = main.addRigCtrlGrp  ,
    #                                 axis       = 'y' ,
    #                                 shape      = 'circle' ,
    #                                 side       = '' ,
    #                                 size       = size )

    # print "# Generate >> HairI Rig"
    # HairI = fkRig.Run (             name       = 'HairI' ,
    #                                 tmpJnt     = [  'HairI1_TmpJnt', 
    #                                                 'HairI2_TmpJnt', 
    #                                                 'HairI3_TmpJnt',
    #                                                 'HairI4_TmpJnt',  
    #                                                  ] ,
    #                                 parent     = 'Head_Jnt' ,
    #                                 ctrlGrp    = main.addRigCtrlGrp  ,
    #                                 axis       = 'y' ,
    #                                 shape      = 'circle' ,
    #                                 side       = '' ,
    #                                 size       = size )

    # print "# Generate >> HairJ Rig"
    # HairJ = fkRig.Run (             name       = 'HairJ' ,
    #                                 tmpJnt     = [  'HairJ1_TmpJnt', 
    #                                                 'HairJ2_TmpJnt', 
    #                                                 'HairJ3_TmpJnt', 
    #                                                 'HairJ4_TmpJnt', 
    #                                                 'HairJ5_TmpJnt', 
    #                                                  ] ,
    #                                 parent     = 'Head_Jnt' ,
    #                                 ctrlGrp    = main.addRigCtrlGrp  ,
    #                                 axis       = 'y' ,
    #                                 shape      = 'circle' ,
    #                                 side       = '' ,
    #                                 size       = size )

    # print "# Generate >> HairK Rig"
    # HairK = fkRig.Run (             name       = 'HairK' ,
    #                                 tmpJnt     = [  'HairK1_TmpJnt', 
    #                                                 'HairK2_TmpJnt', 
    #                                                 'HairK3_TmpJnt', 
    #                                                 'HairK4_TmpJnt', 
    #                                                 'HairK5_TmpJnt', 
    #                                                  ] ,
    #                                 parent     = 'Head_Jnt' ,
    #                                 ctrlGrp    = main.addRigCtrlGrp  ,
    #                                 axis       = 'y' ,
    #                                 shape      = 'circle' ,
    #                                 side       = '' ,
    #                                 size       = size )

    # print "# Generate >> HairL Rig"
    # HairL = fkRig.Run (             name       = 'HairL' ,
    #                                 tmpJnt     = [  'HairL1_TmpJnt', 
    #                                                 'HairL2_TmpJnt', 
    #                                                 'HairL3_TmpJnt', 
    #                                                 'HairL4_TmpJnt', 
    #                                                 'HairL5_TmpJnt', 
    #                                                  ] ,
    #                                 parent     = 'Head_Jnt' ,
    #                                 ctrlGrp    = main.addRigCtrlGrp  ,
    #                                 axis       = 'y' ,
    #                                 shape      = 'circle' ,
    #                                 side       = '' ,
    #                                 size       = size )

    # print "# Generate >> HairM Rig"
    # HairM = fkRig.Run (             name       = 'HairM' ,
    #                                 tmpJnt     = [  'HairM1_TmpJnt', 
    #                                                 'HairM2_TmpJnt', 
    #                                                 'HairM3_TmpJnt', 
    #                                                  ] ,
    #                                 parent     = 'Head_Jnt' ,
    #                                 ctrlGrp    = main.addRigCtrlGrp  ,
    #                                 axis       = 'y' ,
    #                                 shape      = 'circle' ,
    #                                 side       = '' ,
    #                                 size       = size )

    # print "# Generate >> HairN Rig"
    # HairN = fkRig.Run (        name       = 'HairN' ,
    #                                 tmpJnt     = [  'HairN1_TmpJnt', 
    #                                                 'HairN2_TmpJnt', 
    #                                                 'HairN3_TmpJnt', 
    #                                                 'HairN4_TmpJnt', 
    #                                                 'HairN5_TmpJnt',                                                 
    #                                                  ] ,
    #                                 parent     = 'Head_Jnt' ,
    #                                 ctrlGrp    = main.addRigCtrlGrp  ,
    #                                 axis       = 'y' ,
    #                                 shape      = 'circle' ,
    #                                 side       = '' ,
    #                                 size       = size )

    # print "# Generate >> HairO Rig"
    # HairO = fkRig.Run (             name       = 'HairO' ,
    #                                 tmpJnt     = [  'HairO1_TmpJnt', 
    #                                                 'HairO2_TmpJnt', 
    #                                                 'HairO3_TmpJnt', 
    #                                                 'HairO4_TmpJnt', 
    #                                                 'HairO5_TmpJnt', 
    #                                                  ] ,
    #                                 parent     = 'Head_Jnt' ,
    #                                 ctrlGrp    = main.addRigCtrlGrp  ,
    #                                 axis       = 'y' ,
    #                                 shape      = 'circle' ,
    #                                 side       = '' ,
    #                                 size       = size )
    # print "# Generate >> HairP Rig"
    # HairP = fkRig.Run (             name       = 'HairP' ,
    #                                 tmpJnt     = [  'HairP1_TmpJnt', 
    #                                                 'HairP2_TmpJnt', 
    #                                                 'HairP3_TmpJnt', 
    #                                                 'HairP4_TmpJnt', 
    #                                                  ] ,
    #                                 parent     = 'Head_Jnt' ,
    #                                 ctrlGrp    = main.addRigCtrlGrp  ,
    #                                 axis       = 'y' ,
    #                                 shape      = 'circle' ,
    #                                 side       = '' ,
    #                                 size       = size )

    # print "# Generate >> HairQ Rig"
    # HairQ = fkRig.Run (             name       = 'HairQ' ,
    #                                 tmpJnt     = [  'HairQ1_TmpJnt', 
    #                                                 'HairQ2_TmpJnt', 
    #                                                 'HairQ3_TmpJnt', 
    #                                                 'HairQ4_TmpJnt', 
    #                                                 'HairQ5_TmpJnt', 
    #                                                  ] ,
    #                                 parent     = 'Head_Jnt' ,
    #                                 ctrlGrp    = main.addRigCtrlGrp  ,
    #                                 axis       = 'y' ,
    #                                 shape      = 'circle' ,
    #                                 side       = '' ,
    #                                 size       = size )

    # print "# Generate >> HairR Rig"
    # HairR = fkRig.Run (             name       = 'HairR' ,
    #                                 tmpJnt     = [  'HairR1_TmpJnt', 
    #                                                 'HairR2_TmpJnt', 
    #                                                 'HairR3_TmpJnt', 
    #                                                 'HairR4_TmpJnt', 
    #                                                  ] ,
    #                                 parent     = 'Head_Jnt' ,
    #                                 ctrlGrp    = main.addRigCtrlGrp  ,
    #                                 axis       = 'y' ,
    #                                 shape      = 'circle' ,
    #                                 side       = '' ,
    #                                 size       = size )

    # print "# Generate >> HairS Rig"
    # HairS = fkRig.Run (             name       = 'HairS' ,
    #                                 tmpJnt     = [  'HairS1_TmpJnt', 
    #                                                 'HairS2_TmpJnt', 
    #                                                 'HairS3_TmpJnt', 
    #                                                 'HairS4_TmpJnt', 
    #                                                 'HairS5_TmpJnt', 
    #                                                  ] ,
    #                                 parent     = 'Head_Jnt' ,
    #                                 ctrlGrp    = main.addRigCtrlGrp  ,
    #                                 axis       = 'y' ,
    #                                 shape      = 'circle' ,
    #                                 side       = '' ,
    #                                 size       = size )

    # print "# Generate >> HairT Rig"
    # HairT = fkRig.Run (             name       = 'HairT' ,
    #                                 tmpJnt     = [  'HairT1_TmpJnt', 
    #                                                 'HairT2_TmpJnt', 
    #                                                 'HairT3_TmpJnt', 
    #                                                 'HairT4_TmpJnt', 
    #                                                 'HairT5_TmpJnt', 
    #                                                  ] ,
    #                                 parent     = 'Head_Jnt' ,
    #                                 ctrlGrp    = main.addRigCtrlGrp  ,
    #                                 axis       = 'y' ,
    #                                 shape      = 'circle' ,
    #                                 side       = '' ,
    #                                 size       = size )
    # print "# Generate >> HairU Rig"
    # HairU = fkRig.Run (             name       = 'HairU' ,
    #                                 tmpJnt     = [  'HairU1_TmpJnt', 
    #                                                 'HairU2_TmpJnt', 
    #                                                 'HairU3_TmpJnt', 
    #                                                 'HairU4_TmpJnt', 
    #                                                 'HairU5_TmpJnt',                                                     
    #                                                  ] ,
    #                                 parent     = 'Head_Jnt' ,
    #                                 ctrlGrp    = main.addRigCtrlGrp  ,
    #                                 axis       = 'y' ,
    #                                 shape      = 'circle' ,
    #                                 side       = '' ,
    #                                 size       = size )

    # print "# Generate >> HairV Rig"
    # HairV = fkRig.Run (             name       = 'HairV' ,
    #                                 tmpJnt     = [  'HairV1_TmpJnt', 
    #                                                 'HairV2_TmpJnt', 
    #                                                 'HairV3_TmpJnt', 
    #                                                  ] ,
    #                                 parent     = 'Head_Jnt' ,
    #                                 ctrlGrp    = main.addRigCtrlGrp  ,
    #                                 axis       = 'y' ,
    #                                 shape      = 'circle' ,
    #                                 side       = '' ,
    #                                 size       = size )


    # print "# Generate >> PonyTail Rig"
    # HairV = fkRig.Run (             name       = 'PonyTail' ,
    #                                 tmpJnt     = ['PonyTail1_TmpJnt',
    #                                             'PonyTail2_TmpJnt',
    #                                             'PonyTail3_TmpJnt',
    #                                             'PonyTail4_TmpJnt',
    #                                             'PonyTail5_TmpJnt',
    #                                             'PonyTail6_TmpJnt'],
    #                                 parent     = 'Head_Jnt' ,
    #                                 ctrlGrp    = main.addRigCtrlGrp  ,
    #                                 axis       = 'y' ,
    #                                 shape      = 'circle' ,
    #                                 side       = '' ,
    #                                 size       = size )

    # print "# Generate >> Boob L Rig"
    # BoobL = subRig.Run (            name       = 'Boob' ,
    #                                 tmpJnt     = 'Boob_L_TmpJnt',
    #                                 parent     = torso.jntDict[-1],
    #                                 ctrlGrp    = main.addRigCtrlGrp  ,
    #                                 shape      = 'circle' ,
    #                                 side       = 'L' ,
    #                                 size       = size )


    # print "# Generate >> Boob R Rig"
    # BoobR = subRig.Run (            name      = 'Boob' ,
    #                                 tmpJnt     = 'Boob_R_TmpJnt',
    #                                 parent     = torso.jntDict[-1],
    #                                 ctrlGrp    = main.addRigCtrlGrp  ,
    #                                 shape      = 'circle' ,
    #                                 side       = 'R' ,
    #                                 size       = size )

    
    # print "# Generate >> skirtA L Rig"
    # skirtAL = fkRig.Run (           name       = 'SkirtA' ,
    #                                 tmpJnt     = ['SkirtA1_L_TmpJnt',
    #                                              'SkirtA2_L_TmpJnt',
    #                                              'SkirtA3_L_TmpJnt'] ,
    #                                 parent     = torso.pelvisJnt,
    #                                 ctrlGrp    = main.addRigCtrlGrp  ,
    #                                 axis       = 'y' ,
    #                                 shape      = 'square' ,
    #                                 side       = 'L' ,
    #                                 size       = size )


    # print "# Generate >> skirtB L Rig"
    # skirtBL = fkRig.Run (           name       = 'SkirtB' ,
    #                                 tmpJnt     = ['SkirtB1_L_TmpJnt',
    #                                              'SkirtB2_L_TmpJnt',
    #                                              'SkirtB3_L_TmpJnt'] ,
    #                                 parent     = torso.pelvisJnt,
    #                                 ctrlGrp    = main.addRigCtrlGrp  ,
    #                                 axis       = 'y' ,
    #                                 shape      = 'square' ,
    #                                 side       = 'L' ,
    #                                 size       = size )


    # print "# Generate >> skirtC L Rig"
    # skirtCL = fkRig.Run (           name       = 'SkirtC' ,
    #                                 tmpJnt     = ['SkirtC1_L_TmpJnt',
    #                                              'SkirtC2_L_TmpJnt',
    #                                              'SkirtC3_L_TmpJnt'] ,
    #                                 parent     = torso.pelvisJnt,
    #                                 ctrlGrp    = main.addRigCtrlGrp  ,
    #                                 axis       = 'y' ,
    #                                 shape      = 'square' ,
    #                                 side       = 'L' ,
    #                                 size       = size )

    # print "# Generate >> skirtD L Rig"
    # skirtDL = fkRig.Run (           name       = 'SkirtD' ,
    #                                 tmpJnt     = ['SkirtD1_L_TmpJnt',
    #                                              'SkirtD2_L_TmpJnt',
    #                                              'SkirtD3_L_TmpJnt'] ,
    #                                 parent     = torso.pelvisJnt,
    #                                 ctrlGrp    = main.addRigCtrlGrp  ,
    #                                 axis       = 'y' ,
    #                                 shape      = 'square' ,
    #                                 side       = 'L' ,
    #                                 size       = size )


    # print "# Generate >> skirtE L Rig"
    # skirtEL = fkRig.Run (           name       = 'SkirtE' ,
    #                                 tmpJnt     = ['SkirtE1_L_TmpJnt',
    #                                              'SkirtE2_L_TmpJnt',
    #                                              'SkirtE3_L_TmpJnt'] ,
    #                                 parent     = torso.pelvisJnt,
    #                                 ctrlGrp    = main.addRigCtrlGrp  ,
    #                                 axis       = 'y' ,
    #                                 shape      = 'square' ,
    #                                 side       = 'L' ,
    #                                 size       = size )


    # print "# Generate >> skirtA R Rig"
    # skirtAR = fkRig.Run (           name       = 'SkirtA' ,
    #                                 tmpJnt     = ['SkirtA1_R_TmpJnt',
    #                                              'SkirtA2_R_TmpJnt',
    #                                              'SkirtA3_R_TmpJnt'] ,
    #                                 parent     = torso.pelvisJnt,
    #                                 ctrlGrp    = main.addRigCtrlGrp  ,
    #                                 axis       = 'y' ,
    #                                 shape      = 'square' ,
    #                                 side       = 'R' ,
    #                                 size       = size )


    # print "# Generate >> skirtB R Rig"
    # skirtBR = fkRig.Run (           name       = 'SkirtB' ,
    #                                 tmpJnt     = ['SkirtB1_R_TmpJnt',
    #                                              'SkirtB2_R_TmpJnt',
    #                                              'SkirtB3_R_TmpJnt'] ,
    #                                 parent     = torso.pelvisJnt,
    #                                 ctrlGrp    = main.addRigCtrlGrp  ,
    #                                 axis       = 'y' ,
    #                                 shape      = 'square' ,
    #                                 side       = 'R' ,
    #                                 size       = size )


    # print "# Generate >> skirtC R Rig"
    # skirtCR = fkRig.Run (           name       = 'SkirtC' ,
    #                                 tmpJnt     = ['SkirtC1_R_TmpJnt',
    #                                              'SkirtC2_R_TmpJnt',
    #                                              'SkirtC3_R_TmpJnt'] ,
    #                                 parent     = torso.pelvisJnt,
    #                                 ctrlGrp    = main.addRigCtrlGrp  ,
    #                                 axis       = 'y' ,
    #                                 shape      = 'square' ,
    #                                 side       = 'R' ,
    #                                 size       = size )

    # print "# Generate >> skirtD R Rig"
    # skirtDR = fkRig.Run (           name       = 'SkirtD' ,
    #                                 tmpJnt     = ['SkirtD1_R_TmpJnt',
    #                                              'SkirtD2_R_TmpJnt',
    #                                              'SkirtD3_R_TmpJnt'] ,
    #                                 parent     = torso.pelvisJnt,
    #                                 ctrlGrp    = main.addRigCtrlGrp  ,
    #                                 axis       = 'y' ,
    #                                 shape      = 'square' ,
    #                                 side       = 'R' ,
    #                                 size       = size )


    # print "# Generate >> skirtE R Rig"
    # skirtER = fkRig.Run (           name       = 'SkirtE' ,
    #                                 tmpJnt     = ['SkirtE1_R_TmpJnt',
    #                                              'SkirtE2_R_TmpJnt',
    #                                              'SkirtE3_R_TmpJnt'] ,
    #                                 parent     = torso.pelvisJnt,
    #                                 ctrlGrp    = main.addRigCtrlGrp  ,
    #                                 axis       = 'y' ,
    #                                 shape      = 'square' ,
    #                                 side       = 'R' ,
    #                                 size       = size )

    # skritJntPosGrp = pm.createNode('transform' , n = 'SkirtSkinJnt_Grp')
    # pm.parent(skritJntPosGrp , 'Pelvis_Jnt')
    # pm.parent('SkirtA1Pos_L_Jnt','SkirtB1Pos_L_Jnt','SkirtC1Pos_L_Jnt','SkirtD1Pos_L_Jnt','SkirtE1Pos_L_Jnt','SkirtA1Pos_R_Jnt','SkirtB1Pos_R_Jnt','SkirtC1Pos_R_Jnt','SkirtD1Pos_R_Jnt','SkirtE1Pos_R_Jnt', skritJntPosGrp)


    # print "# Generate >> NeckArmor A L Rig"
    # NeckArmorA = fkRig.Run (        name       = 'NeckArmorA' ,
    #                                 tmpJnt     = ['NeckArmorA1_L_TmpJnt', 'NeckArmorA2_L_TmpJnt', 'NeckArmorA3_L_TmpJnt'] ,
    #                                 parent     = torso.jntDict[-1],
    #                                 ctrlGrp    = main.addRigCtrlGrp  ,
    #                                 axis       = 'y' ,
    #                                 shape      = 'square' ,
    #                                 side       = 'L' ,
    #                                 size       = size )

    # print "# Generate >> NeckArmor B L Rig"
    # NeckArmorB = fkRig.Run (        name       = 'NeckArmorB' ,
    #                                 tmpJnt     = ['NeckArmorB1_L_TmpJnt', 'NeckArmorB2_L_TmpJnt', 'NeckArmorB3_L_TmpJnt'] ,
    #                                 parent     = torso.jntDict[-1],
    #                                 ctrlGrp    = main.addRigCtrlGrp  ,
    #                                 axis       = 'y' ,
    #                                 shape      = 'square' ,
    #                                 side       = 'L' ,
    #                                 size       = size )

    # print "# Generate >> NeckArmor C Rig"
    # NeckArmorC = fkRig.Run (        name       = 'NeckArmorC' ,
    #                                 tmpJnt     = ['NeckArmorC1_TmpJnt', 'NeckArmorC2_TmpJnt', 'NeckArmorC3_TmpJnt'] ,
    #                                 parent     = torso.jntDict[-1],
    #                                 ctrlGrp    = main.addRigCtrlGrp  ,
    #                                 axis       = 'y' ,
    #                                 shape      = 'square' ,
    #                                 side       = '' ,
    #                                 size       = size )

    # print "# Generate >> NeckArmor A R Rig"
    # NeckArmorA = fkRig.Run (        name       = 'NeckArmorA' ,
    #                                 tmpJnt     = ['NeckArmorA1_R_TmpJnt', 'NeckArmorA2_R_TmpJnt', 'NeckArmorA3_R_TmpJnt'] ,
    #                                 parent     = torso.jntDict[-1],
    #                                 ctrlGrp    = main.addRigCtrlGrp  ,
    #                                 axis       = 'y' ,
    #                                 shape      = 'square' ,
    #                                 side       = 'R' ,
    #                                 size       = size )

    # print "# Generate >> NeckArmor B R Rig"
    # NeckArmorB = fkRig.Run (        name       = 'NeckArmorB' ,
    #                                 tmpJnt     = ['NeckArmorB1_R_TmpJnt', 'NeckArmorB2_R_TmpJnt', 'NeckArmorB3_R_TmpJnt'] ,
    #                                 parent     = torso.jntDict[-1],
    #                                 ctrlGrp    = main.addRigCtrlGrp  ,
    #                                 axis       = 'y' ,
    #                                 shape      = 'square' ,
    #                                 side       = 'R' ,
    #                                 size       = size )

    # neckArmorJntPosGrp = pm.createNode('transform' , n = 'NeckArmorSkinJnt_Grp')
    # pm.parent(neckArmorJntPosGrp , 'Spine5_Jnt')
    # pm.parent('NeckArmorA1Pos_L_Jnt', 'NeckArmorB1Pos_L_Jnt', 'NeckArmorC1Pos_Jnt', 'NeckArmorA1Pos_R_Jnt', 'NeckArmorB1Pos_R_Jnt', 'Spine5_Jnt')

    
#---------------------------------------------------- sub ctrl ------------------------------------------------------#


    # print "# Generate >> Shoulder Armour Left Rig"
    # ShoulderArmorL = subRig.Run(              
    #                                 name       = 'ShoulderArmor' ,
    #                                 tmpJnt     = 'ShoulderArmor_L_TmpJnt' ,
    #                                 parent     = 'UpArmNonRoll_L_Jnt' ,
    #                                 ctrlGrp    = main.addRigCtrlGrp ,
    #                                 shape      = 'cube' ,
    #                                 side       = 'L' ,
    #                                 size       = size 
    #                       )

    # print "# Generate >> Shoulder Armour Right Rig"
    # ShoulderArmorR = subRig.Run(              
    #                                 name       = 'ShoulderArmor' ,
    #                                 tmpJnt     = 'ShoulderArmor_R_TmpJnt' ,
    #                                 parent     = 'UpArmNonRoll_R_Jnt' ,
    #                                 ctrlGrp    = main.addRigCtrlGrp ,
    #                                 shape      = 'cube' ,
    #                                 side       = 'R' ,
    #                                 size       = size 
    #                       )



# ## Skirt Front L
#     print "# Generate >> SkirtA_Ctrl AutoSkirt Front L"
#     utaCore.nonRollFkAll(   elem = ['SkirtAFnt', 'SkirtBFnt', 'SkirtCFnt', 'SkirtDFnt', 'SkirtEFnt'],
#                             target = ['SkirtA1CtrlOfst_L_Grp', 'SkirtB1CtrlOfst_L_Grp', 'SkirtC1CtrlOfst_L_Grp', 'SkirtD1CtrlOfst_L_Grp', 'SkirtE1CtrlOfst_L_Grp'],
#                             source = 'UpLegNonRoll_L_Jnt', 
#                             objAttrs = 'SkirtA1_L_Ctrl', 
#                             shape = True,
#                             side = 'L',
#                             valueAmp = 0,
#                             mins = 0,
#                             maxs = 90,
#                             axisSource = 'X',
#                             rotate = True,
#                             translate = True,
#                             scale = False)



# ## Skirt Side L
#     print "# Generate >> SkirtC_Ctrl AutoSkirt Side L"
#     utaCore.nonRollFkAll(   elem = ['SkirtASide', 'SkirtBSide', 'SkirtCSide', 'SkirtDSide', 'SkirtESide'],
#                             target = ['SkirtA1CtrlOfst_L_Grp', 'SkirtB1CtrlOfst_L_Grp', 'SkirtC1CtrlOfst_L_Grp', 'SkirtD1CtrlOfst_L_Grp', 'SkirtE1CtrlOfst_L_Grp'],
#                             source = 'UpLegNonRoll_L_Jnt', 
#                             objAttrs = 'SkirtC1_L_Ctrl', 
#                             shape = True,
#                             side = 'L',
#                             valueAmp = 0,
#                             mins = 0,
#                             maxs = 90,
#                             axisSource = 'Z',
#                             rotate = True,
#                             translate = True,
#                             scale = False)

# ## Skirt Back L
#     print "# Generate >> SkirtE_Ctrl AutoSkirt Back L"
#     utaCore.nonRollFkAll(   elem = ['SkirtABck', 'SkirtBBck', 'SkirtCBck', 'SkirtDBck', 'SkirtEBck'],
#                             target = ['SkirtA1CtrlOfst_L_Grp', 'SkirtB1CtrlOfst_L_Grp', 'SkirtC1CtrlOfst_L_Grp', 'SkirtD1CtrlOfst_L_Grp', 'SkirtE1CtrlOfst_L_Grp'],
#                             source = 'UpLegNonRoll_L_Jnt', 
#                             objAttrs = 'SkirtE1_L_Ctrl', 
#                             shape = True,
#                             side = 'L',
#                             valueAmp = 0,
#                             mins = -90,
#                             maxs = 0,
#                             axisSource = 'X',
#                             rotate = True,
#                             translate = True,
#                             scale = False)


# ## Skirt Front L
#     print "# Generate >> SkirtA_Ctrl AutoSkirt Front R"
#     utaCore.nonRollFkAll(   elem = ['SkirtAFnt', 'SkirtBFnt', 'SkirtCFnt', 'SkirtDFnt', 'SkirtEFnt'],
#                             target = ['SkirtA1CtrlOfst_R_Grp', 'SkirtB1CtrlOfst_R_Grp', 'SkirtC1CtrlOfst_R_Grp', 'SkirtD1CtrlOfst_R_Grp', 'SkirtE1CtrlOfst_R_Grp'],
#                             source = 'UpLegNonRoll_R_Jnt', 
#                             objAttrs = 'SkirtA1_R_Ctrl', 
#                             shape = True,
#                             side = 'R',
#                             valueAmp = 0,
#                             mins = 0,
#                             maxs = 90,
#                             axisSource = 'X',
#                             rotate = True,
#                             translate = True,
#                             scale = False)



# ## Skirt Side L
#     print "# Generate >> SkirtC_Ctrl AutoSkirt Side R"
#     utaCore.nonRollFkAll(   elem = ['SkirtASide', 'SkirtBSide', 'SkirtCSide', 'SkirtDSide', 'SkirtESide'],
#                             target = ['SkirtA1CtrlOfst_R_Grp', 'SkirtB1CtrlOfst_R_Grp', 'SkirtC1CtrlOfst_R_Grp', 'SkirtD1CtrlOfst_R_Grp', 'SkirtE1CtrlOfst_R_Grp'],
#                             source = 'UpLegNonRoll_R_Jnt', 
#                             objAttrs = 'SkirtC1_R_Ctrl', 
#                             shape = True,
#                             side = 'R',
#                             valueAmp = 0,
#                             mins = 0,
#                             maxs = 90,
#                             axisSource = 'Z',
#                             rotate = True,
#                             translate = True,
#                             scale = False)

# ## Skirt Back R
#     print "# Generate >> SkirtE_Ctrl AutoSkirt Back R"
#     utaCore.nonRollFkAll(   elem = ['SkirtABck', 'SkirtBBck', 'SkirtCBck', 'SkirtDBck', 'SkirtEBck'],
#                             target = ['SkirtA1CtrlOfst_R_Grp', 'SkirtB1CtrlOfst_R_Grp', 'SkirtC1CtrlOfst_R_Grp', 'SkirtD1CtrlOfst_R_Grp', 'SkirtE1CtrlOfst_R_Grp'],
#                             source = 'UpLegNonRoll_R_Jnt', 
#                             objAttrs = 'SkirtE1_R_Ctrl', 
#                             shape = True,
#                             side = 'R',
#                             valueAmp = 0,
#                             mins = -90,
#                             maxs = 0,
#                             axisSource = 'X',
#                             rotate = True,
#                             translate = True,
#                             scale = False)



    # ampAgtGrp = pm.createNode('transform' , n = 'AmpAgtStill_Grp')
    # pm.parent(ampAgtGrp , 'Still_Grp')


    # ## -------------------------------------------------------------------------------------
    # ## ---------------------------------Auto Neck-------------------------------------------
    # ## ------------------------------------------------------------------------------------- 

    # print "# Generate >> Create Neck Angle Axis "
    # neckArmrAg = agt.AngleAxis(     name     = 'NeckArmor' ,
    #                                 root     = 'Neck_Jnt' ,
    #                                 aim      = (0,1,0) ,
    #                                 side     = '' 
    #                            )

    # print "# Generate >> Connect NeckArmorA Angle Axis "        
    # NeckArmrAAg = agt.ConnectAngleAxis(     name  = 'NeckArmorA' ,
    #                                         side  = 'L' ,
    #                                         obj   = 'NeckArmorA1Space_L_Grp' ,
    #                                         loc   = neckArmrAg.loc ,
    #                                         parent = torso.jntDict[-1] ,
    #                                         val   = { 'rx' : [0,0] , 'rz' : [0,-0.7] } 
    #                             )

    # print "# Generate >> Connect NeckArmorB Angle Axis "        
    # NeckArmrBAg = agt.ConnectAngleAxis(     name  = 'NeckArmorB' ,
    #                                         side  = 'L' ,
    #                                         obj   = 'NeckArmorB1Space_L_Grp' ,
    #                                         loc   = neckArmrAg.loc ,
    #                                         parent = torso.jntDict[-1] ,
    #                                         val   = { 'rx' : [0,-0.56] , 'rz' : [0,-0.6] } 
    #                             )

    # print "# Generate >> Connect NeckArmorC Angle Axis "        
    # NeckArmrCAg = agt.ConnectAngleAxis(     name  = 'NeckArmorC' ,
    #                                         side  = '' ,
    #                                         obj   = 'NeckArmorC1Space_Grp' ,
    #                                         loc   = neckArmrAg.loc ,
    #                                         parent = torso.jntDict[-1] ,
    #                                         val   = { 'rx' : [0,-1.8] , 'rz' : [0,0] } 
    #                             )

    # print "# Generate >> Connect NeckArmorA Angle Axis "        
    # NeckArmrAAg = agt.ConnectAngleAxis(     name  = 'NeckArmorA' ,
    #                                         side  = 'R' ,
    #                                         obj   = 'NeckArmorA1Space_R_Grp' ,
    #                                         loc   = neckArmrAg.loc ,
    #                                         parent = torso.jntDict[-1] ,
    #                                         val   = { 'rx' : [0,0] , 'rz' : [0.7,0] } 
    #                             )

    # print "# Generate >> Connect NeckArmorB Angle Axis "        
    # NeckArmrBAg = agt.ConnectAngleAxis(     name  = 'NeckArmorB' ,
    #                                         side  = 'R' ,
    #                                         obj   = 'NeckArmorB1Space_R_Grp' ,
    #                                         loc   = neckArmrAg.loc ,
    #                                         parent = torso.jntDict[-1] ,
    #                                         val   = { 'rx' : [0,-0.56] , 'rz' : [0.6,0] } 
    #                             )

    # pm.setAttr('NeckArmorAngle_Loc.outputXn' , 90)
    # pm.setAttr('NeckArmorAngle_Loc.outputXp' , 90)
    # pm.setAttr('NeckArmorAngle_Loc.outputZn' , 90)
    # pm.setAttr('NeckArmorAngle_Loc.outputZp' , 90)
    # pm.parent('NeckArmorAngle_Grp' ,'NeckArmorAAmpAg_L_Grp' ,'NeckArmorBAmpAg_L_Grp' ,'NeckArmorCAmpAg_Grp' ,'NeckArmorAAmpAg_R_Grp', 'NeckArmorBAmpAg_R_Grp' , ampAgtGrp)


    print "# Generate >> BtwJnt"
    proc.btwJnt(jnts=['Neck5RbnDtl_Jnt', 'Head_Jnt', 'HeadEnd_Jnt'],axis='y', pointConstraint=True)
    proc.btwJnt(jnts=['Spine5_Jnt', 'Neck_Jnt', 'Neck1RbnDtl_Jnt'],axis='y', pointConstraint=True)
    proc.btwJnt(jnts=['Spine5_Jnt', 'Clav_L_Jnt', 'UpArm_L_Jnt'],axis='y', pointConstraint=True)
    proc.btwJnt(jnts=['Spine5_Jnt', 'Clav_R_Jnt', 'UpArm_R_Jnt'],axis='y', pointConstraint=True)
    proc.btwJnt(jnts=['UpArm5RbnDtl_L_Jnt', 'Forearm_L_Jnt', 'Forearm1RbnDtl_L_Jnt'],axis='y', pointConstraint=False)
    proc.btwJnt(jnts=['UpArm5RbnDtl_R_Jnt', 'Forearm_R_Jnt', 'Forearm1RbnDtl_R_Jnt'],axis='y', pointConstraint=False)
    proc.btwJnt(jnts=['Forearm5RbnDtl_L_Jnt', 'Wrist_L_Jnt', 'Hand_L_Jnt'],axis='y', pointConstraint=True)
    proc.btwJnt(jnts=['Forearm5RbnDtl_R_Jnt', 'Wrist_R_Jnt', 'Hand_R_Jnt'],axis='y', pointConstraint=True)
    proc.btwJnt(jnts=['Pelvis_Jnt', 'UpLeg_L_Jnt', 'UpLeg1RbnDtl_L_Jnt'],axis='y', pointConstraint=True)
    proc.btwJnt(jnts=['Pelvis_Jnt', 'UpLeg_R_Jnt', 'UpLeg1RbnDtl_R_Jnt'],axis='y', pointConstraint=True)
    proc.btwJnt(jnts=['UpLeg5RbnDtl_L_Jnt', 'LowLeg_L_Jnt', 'LowLeg1RbnDtl_L_Jnt'],axis='y', pointConstraint=False)
    # proc.btwJnt(jnts=['LowLegBtw_L_Jnt', 'LowLeg_L_Jnt', 'LowLeg2RbnDtl_L_Jnt'],axis='y', pointConstraint=False, elem='LowLegBtwDn')

    proc.btwJnt(jnts=['UpLeg5RbnDtl_R_Jnt', 'LowLeg_R_Jnt', 'LowLeg1RbnDtl_R_Jnt'],axis='y', pointConstraint=False)
    # proc.btwJnt(jnts=['LowLegBtw_R_Jnt', 'LowLeg_R_Jnt', 'LowLeg2RbnDtl_R_Jnt'],axis='y', pointConstraint=False, elem='LowLegBtwDn')
    proc.btwJnt(jnts=['Spine4Sca_Jnt', 'Spine5_Jnt', 'Neck_Jnt'],axis='y', pointConstraint=True)

    fngrList = ['Thumb','Index','Middle','Ring','Pinky']
    sideList = ['_L_','_R_']
    for eachS in sideList:
        for eachFngr in fngrList:
            if eachFngr == 'Thumb':
                proc.btwJnt(jnts=['Hand{}Jnt'.format(eachS), '{}1{}Jnt'.format(eachFngr,eachS), '{}2{}Jnt'.format(eachFngr,eachS)],axis='y', pointConstraint=True)
                proc.btwJnt(jnts=['{}1{}Jnt'.format(eachFngr,eachS), '{}2{}Jnt'.format(eachFngr,eachS), '{}3{}Jnt'.format(eachFngr,eachS)],axis='y', pointConstraint=True)
                proc.btwJnt(jnts=['{}2{}Jnt'.format(eachFngr,eachS), '{}3{}Jnt'.format(eachFngr,eachS), '{}4{}Jnt'.format(eachFngr,eachS)],axis='y', pointConstraint=True)
            else:
                if eachFngr == 'Index' or eachFngr == 'Middle':
                    proc.btwJnt(jnts=['HandCupInner{}Jnt'.format(eachS), '{}1{}Jnt'.format(eachFngr,eachS), '{}2{}Jnt'.format(eachFngr,eachS)],axis='y', pointConstraint=True)
                    proc.btwJnt(jnts=['{}1{}Jnt'.format(eachFngr,eachS), '{}2{}Jnt'.format(eachFngr,eachS), '{}3{}Jnt'.format(eachFngr,eachS)],axis='y', pointConstraint=True)
                    proc.btwJnt(jnts=['{}2{}Jnt'.format(eachFngr,eachS), '{}3{}Jnt'.format(eachFngr,eachS), '{}4{}Jnt'.format(eachFngr,eachS)],axis='y', pointConstraint=True)
                    proc.btwJnt(jnts=['{}3{}Jnt'.format(eachFngr,eachS), '{}4{}Jnt'.format(eachFngr,eachS), '{}5{}Jnt'.format(eachFngr,eachS)],axis='y', pointConstraint=True)
                else:
                    proc.btwJnt(jnts=['HandCupOuter{}Jnt'.format(eachS), '{}1{}Jnt'.format(eachFngr,eachS), '{}2{}Jnt'.format(eachFngr,eachS)],axis='y', pointConstraint=True)
                    proc.btwJnt(jnts=['{}1{}Jnt'.format(eachFngr,eachS), '{}2{}Jnt'.format(eachFngr,eachS), '{}3{}Jnt'.format(eachFngr,eachS)],axis='y', pointConstraint=True)
                    proc.btwJnt(jnts=['{}2{}Jnt'.format(eachFngr,eachS), '{}3{}Jnt'.format(eachFngr,eachS), '{}4{}Jnt'.format(eachFngr,eachS)],axis='y', pointConstraint=True)
                    proc.btwJnt(jnts=['{}3{}Jnt'.format(eachFngr,eachS), '{}4{}Jnt'.format(eachFngr,eachS), '{}5{}Jnt'.format(eachFngr,eachS)],axis='y', pointConstraint=True)

    # print "# Generate >> NeckArmor L"
    # utaCore.nonRollFkAll(   elem = ['NeckArmorA1', 'NeckArmorB1', 'NeckArmorC1'],
    #                 target = ['NeckArmorA1CtrlOfst_L_Grp', 'NeckArmorB1CtrlOfst_L_Grp', 'NeckArmorC1CtrlOfst_Grp'],
    #                 source = 'Clav_L_Jnt', 
    #                 objAttrs = 'NeckArmorA1_L_Ctrl', 
    #                 shape = True,
    #                 side = 'L',
    #                 valueAmp = 0,
    #                 mins = 0,
    #                 maxs = 90,
    #                 axisSource = 'X',
    #                 rotate = True,
    #                 translate = True,
    #                 scale = False)

    # print "# Generate >> NeckArmor L"
    # utaCore.nonRollFkAll(   elem = ['NeckArmorA1', 'NeckArmorB1', 'NeckArmorC1'],
    #                 target = ['NeckArmorA1CtrlOfst_L_Grp', 'NeckArmorB1CtrlOfst_L_Grp', 'NeckArmorC1CtrlOfst_Grp'],
    #                 source = 'Clav_L_Jnt', 
    #                 objAttrs = 'NeckArmorB1_L_Ctrl', 
    #                 shape = True,
    #                 side = 'L',
    #                 valueAmp = 0,
    #                 mins = 0,
    #                 maxs = 90,
    #                 axisSource = 'Z',
    #                 rotate = True,
    #                 translate = True,
    #                 scale = False)

    # print "# Generate >> NeckArmor R"
    # utaCore.nonRollFkAll(   elem = ['NeckArmorA1', 'NeckArmorB1', 'NeckArmorC1'],
    #                 target = ['NeckArmorA1CtrlOfst_R_Grp', 'NeckArmorB1CtrlOfst_R_Grp', 'NeckArmorC1CtrlOfst_Grp'],
    #                 source = 'Clav_R_Jnt', 
    #                 objAttrs = 'NeckArmorA1_R_Ctrl', 
    #                 shape = True,
    #                 side = 'R',
    #                 valueAmp = 0,
    #                 mins = 0,
    #                 maxs = 90,
    #                 axisSource = 'X',
    #                 rotate = True,
    #                 translate = True,
    #                 scale = False)

    # print "# Generate >> NeckArmor R"
    # utaCore.nonRollFkAll(   elem = ['NeckArmorA1', 'NeckArmorB1', 'NeckArmorC1'],
    #                 target = ['NeckArmorA1CtrlOfst_R_Grp', 'NeckArmorB1CtrlOfst_R_Grp', 'NeckArmorC1CtrlOfst_Grp'],
    #                 source = 'Clav_R_Jnt', 
    #                 objAttrs = 'NeckArmorB1_R_Ctrl', 
    #                 shape = True,
    #                 side = 'R',
    #                 valueAmp = 0,
    #                 mins = 0,
    #                 maxs = 90,
    #                 axisSource = 'Z',
    #                 rotate = True,
    #                 translate = True,
    #                 scale = False)

    # print "# Generate >> NeckArmor C"
    # utaCore.nonRollFkAll(   elem = ['NeckArmorA1L', 'NeckArmorB1L', 'NeckArmorC1','NeckArmorA1R', 'NeckArmorB1R'],
    #                 target = ['NeckArmorA1CtrlOfst_R_Grp', 'NeckArmorB1CtrlOfst_R_Grp', 'NeckArmorC1CtrlOfst_Grp','NeckArmorA1CtrlOfst_L_Grp', 'NeckArmorB1CtrlOfst_L_Grp'],
    #                 source = 'Neck_Jnt', 
    #                 objAttrs = 'NeckArmorC1_Ctrl', 
    #                 shape = True,
    #                 side = '',
    #                 valueAmp = 0,
    #                 mins = 0,
    #                 maxs = 90,
    #                 axisSource = 'X',
    #                 rotate = True,
    #                 translate = True,
    #                 scale = False)


    print "# Generate >> Set preferred ankle Leg Ik"
    utaCore.preferredAnkleLegIk()

    print '##------------------------------------------------------------------------------------'
    print '##-------------------------------- CLEAR UTILITIE ------------------------------------'
    print '##------------------------------------------------------------------------------------'


    print "# Generate >> Character Text Name"
    charTextName.charTextName(value = '3', size = '1')

    print "# Generate >> Read Control Shape"
    # ctrlShapeTools.readAllCtrl()
    # Path ---------------------------------------
    corePath = '%s/core' % os.environ.get('RFSCRIPT')
    ctrlData.read_ctrl_dataFld( pathFld = '{}/rf_maya/rftool/rig/template/templatePrevis/data'.format(corePath))

    print "# Generate >> Set Defalut Attribute"
    rigTools.jntRadius( r = 0.025 )
    mc.delete( 'TmpJnt_Grp' )
    mc.parent('Rig_Grp','Export_Grp')

    print "##------------------------------------------------------------------------------------"
    print "##--------------------------------- D O N E  ! ! -------------------------------------"
    print "##------------------------------------------------------------------------------------"