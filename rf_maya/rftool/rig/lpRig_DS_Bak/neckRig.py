import maya.cmds as mc

import lpRig.core as lrc
reload(lrc)
import lpRig.rigTools as lrr
reload(lrr)
import lpRig.ribbon as lrb
reload(lrb)

class NeckRig(object):

	def __init__(
					self,
					parent='',
					ctrlGrp='',
					skinGrp='',
					jntGrp='',
					stillGrp='',
					ax='y',
					tmpJnt=[
								'Neck_Jnt',
								'NeckTip_Jnt'
							],
					part='',
					headJnt='',
					ribbon=True
				):

		# Skin joints
		self.Neck_Jnt = lrc.Dag(tmpJnt[0])
		self.NeckTip_Jnt = lrc.Dag(tmpJnt[1])

		# Main group
		self.Ctrl_Grp = lrc.Null()
		self.Ctrl_Grp.name = "NeckCtrl%s_Grp" % part

		mc.parentConstraint(parent, self.Ctrl_Grp)
		self.Ctrl_Grp.parent(ctrlGrp)

		self.Skin_Grp = lrc.Null()
		self.Skin_Grp.name = "NeckSkin%s_Grp" % part

		self.Skin_Grp.parent(skinGrp)

		self.Jnt_Grp = lrc.Null()
		self.Jnt_Grp.name = "NeckJnt%s_Grp" % part

		self.Jnt_Grp.parent(jntGrp)

		self.Still_Grp = lrc.Null()
		self.Still_Grp.name = "NeckStill%s_Grp" % part
		
		self.Still_Grp.parent(stillGrp)
		
		# Controller
		self.Neck_Ctrl = lrr.jointControl('circle')
		self.Neck_Ctrl.name = "Neck%s_Ctrl" % part
		self.Neck_Ctrl.lockHideAttrs('v')
		self.Neck_Ctrl.color = 'yellow'
		self.Neck_Ctrl.rotateOrder = 'xyz'

		self.NeckCtrlZr_Grp = lrc.group(self.Neck_Ctrl)
		self.NeckCtrlZr_Grp.name = "NeckCtrlZr%s_Grp" % part
		
		# Controller - Positioning
		self.NeckCtrlZr_Grp.snapPoint(self.Neck_Jnt)
		self.Neck_Ctrl.snapOrient(self.Neck_Jnt)
		self.Neck_Ctrl.freeze(r=True, t=False, s=False)
		self.NeckCtrlZr_Grp.parent(self.Ctrl_Grp)
		
		self.NeckGmbl_Ctrl = lrc.addGimbal(self.Neck_Ctrl)
		self.NeckGmbl_Ctrl.name = "NeckGmbl%s_Ctrl" % part
		self.NeckGmbl_Ctrl.rotateOrder = 'xyz'

		# Local/world setup
		(self.NeckCtrlLoc_Grp,
		self.NeckCtrlWor_Grp,
		self.NeckCtrlWor_Grp_orientConstraint,
		self.NeckCtrlZr_Grp_orientConstraint,
		self.NeckCtrlZrGrpOri_Rev) = lrr.oriLocWor(
														self.Neck_Ctrl,
														self.Ctrl_Grp,
														ctrlGrp,
														self.NeckCtrlZr_Grp
													)

		self.NeckCtrlLoc_Grp.name = "NeckCtrlLoc%s_Grp" % part
		self.NeckCtrlWor_Grp.name = "NeckCtrlWor%s_Grp" % part
		self.NeckCtrlZrGrpOri_Rev.name = "NeckCtrlZrGrpOri%s_Rev" % part

		# Stretch setup
		(self.NeckStrt_Add,
		self.NeckStrt_Mul) = lrr.fkStretch(
												ctrl=self.Neck_Ctrl,
												target=self.NeckTip_Jnt,
												ax=ax
											)

		self.NeckStrt_Add.name = "NeckStrt%s_Add" % part
		self.NeckStrt_Mul.name = "NeckStrt%s_Mul" % part
		
		# Connect to joint
		mc.parentConstraint(self.NeckGmbl_Ctrl, self.Neck_Jnt)

		# Ribbon
		rbnAx = 'y+'
		rbnAim = (0, 1, 0)
		rbnUp = (1, 0, 0)

		neckLen = lrc.distance(self.Neck_Jnt, self.NeckTip_Jnt)

		if ribbon :
			self.Rbn = lrb.RibbonIkHi(
										size=neckLen,
										ax='y+',
										part='Neck%s' % part,
										side=''
									)
		else:
			self.Rbn = lrb.RibbonIkLow(
										size=neckLen,
										ax='y+',
										part='Neck%s' % part,
										side=''
									)

		self.Rbn.Ctrl_Grp.snapPoint(self.Neck_Jnt)
		mc.delete(
					mc.aimConstraint(
										self.NeckTip_Jnt,
										self.Rbn.Ctrl_Grp,
										aim=rbnAim, u=rbnUp,
										wut='objectrotation',
										wuo=self.Neck_Jnt,
										wu=rbnUp
									)
				)
		self.Rbn.RbnRootTws_Grp.rotateOrder = 'yxz'
		self.Rbn.RbnEndTws_Grp.rotateOrder = 'yxz'
		rbnCtrlShp = lrc.Dag(self.Rbn.Rbn_Ctrl.shape)
		rbnCtrlShp.attr('upVector').v = rbnUp

		mc.parentConstraint(
								self.Neck_Jnt,
								self.Rbn.Ctrl_Grp
							)
		mc.parentConstraint(
								self.NeckTip_Jnt,
								self.Rbn.RbnEndTwsZr_Grp
							)
		self.Rbn.RbnRoot_Ctrl.hide()
		self.Rbn.RbnEnd_Ctrl.hide()

		if headJnt:
			mc.pointConstraint(headJnt, self.Rbn.RbnEnd_Ctrl)
			mc.parentConstraint(
									headJnt,
									self.Rbn.RbnEndTws_Grp
								)

		self.Rbn.Ctrl_Grp.parent(self.Ctrl_Grp)
		self.Rbn.Skin_Grp.parent(self.Skin_Grp)
		self.Rbn.Jnt_Grp.parent(self.Jnt_Grp)
		if ribbon:
			self.Rbn.Still_Grp.parent(self.Still_Grp)