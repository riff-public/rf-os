import maya.cmds as mc

from lpRig import rigTools as lrr
reload(lrr)
from lpRig import core as lrc
reload(lrc)

def findClosestVertext(obj='', mesh=''):
	'''
	Find the closest vetex from given mesh to given object
	'''
	objPos = mc.xform(obj, q=True, ws=True, rp=True)
	vtxNo = mc.polyEvaluate(mesh, v=True)

	closestVtx = None
	minLen = None

	for ix in range(vtxNo):

		vtx = '%s.vtx[%s]' % (mesh, ix)

		vtxPos = mc.xform(vtx, q=True, ws=True, t=True)

		vct = lrc.diff(objPos, vtxPos)
		dist = lrc.mag(vct)

		if minLen == None:
			closestVtx = vtx
			minLen = dist

		if dist < minLen:
			closestVtx = vtx
			minLen = dist

	return closestVtx

def pointOnEdge(obj='', edge=''):
	# Create transform node that will be sticked on given edge
	prxy = lrc.Dag(obj)
	edgeId = ''.join([x for x in edge.split('.')[1] if x.isdigit()])
	msh = lrc.Dag(edge.split('.')[0])

	rvt = lrc.Null()
	
	# Casting edge to two vertices
	vtxs = [x for x in mc.polyInfo(edge, ev=True)[0].split(':')[1].split(' ') if x.isdigit()]
	
	# Curve from mesh edge
	cfme = lrc.CurveFromMeshEdge()
	msh.attr('w') >> cfme.attr('im')
	cfme.attr('ei[0]').value = int(edgeId)
	
	# Point on curve info
	poci = lrc.PointOnCurveInfo()
	cfme.attr('outputCurve') >> poci.attr('ic')
	poci.attr('position') >> rvt.attr('t')
	poci.attr('turnOnPercentage').value = 1
	
	# Finding world position
	vtxAPos = mc.xform('%s.vtx[%s]' % (msh, vtxs[0]), q = True, t = True, ws = True)
	vtxBPos = mc.xform('%s.vtx[%s]' % (msh, vtxs[1]), q = True, t = True, ws = True)
	prxyPos = prxy.ws
	
	# Finding length and length ratio
	edgeLen = lrc.mag((vtxBPos[0] - vtxAPos[0], vtxBPos[1] - vtxAPos[1], vtxBPos[2] - vtxAPos[2]))
	prxyLen = lrc.mag((prxyPos[0] - vtxAPos[0], prxyPos[1] - vtxAPos[1], prxyPos[2] - vtxAPos[2]))
	lenRatio = prxyLen / edgeLen
	
	poci.attr('parameter').value = lenRatio
	
	return rvt, cfme, poci

def jointOnTransform(obj=''):
	# Create joint at given transform node
	obj = lrc.Dag(obj)
	jnt = lrc.Joint()
	grp = lrc.group(jnt)
	grp.snapPoint(obj)
	
	return jnt, grp

def attachSelectedControlsToSelectedMesh():

	'''
	Attach selected controllers to selected mesh.
	Select controllers first then select mesh the last.
	'''

	sels = mc.ls(sl=True)

	mesh = lrc.Dag(sels[-1])

	rvts = []

	for sel in sels[:-1]:

		ctrl = lrc.Dag(sel)

		ctrlNm = ctrl.name.split(':')[-1]

		ndNm, ndIdx, ndSide, ndType = lrr.extractName(ctrlNm)

		rvt, cfme, poci, mdv = attachControlToClosestVertex(ctrl.name, mesh.name)

		rvt.name = '%s%s%sRvt%sGrp' % (ndNm, ndIdx, ndType, ndSide)
		cfme.name = '%s%s%sRvt%sCfme' % (ndNm, ndIdx, ndType, ndSide)
		poci.name = '%s%s%sRvt%sPoci' % (ndNm, ndIdx, ndType, ndSide)
		mdv.name = '%s%s%sInv%sMdv' % (ndNm, ndIdx, ndType, ndSide)

		rvts.append(rvt)

	mc.select(sels, r=True)

	return rvts

def attachControlToClosestVertex(ctrl='', mesh=''):

	ctrlObj = lrc.Dag(ctrl)
	ctrlInvGrp = ctrlObj.getParent()
	ctrlZrGrp = ctrlInvGrp.getParent()

	vtx = findClosestVertext(ctrl, mesh)

	mc.select(vtx, r=True)
	tmpLoc = lrr.locatorOnMidPos()

	edges = mc.polyListComponentConversion(vtx, te=True)
	edge = mc.ls(edges, fl=True)[0]

	# Creating rivet
	rvt, cfme, poci = pointOnEdge(tmpLoc, edge)
	mc.delete(tmpLoc)
	lrc.pointConstraint(rvt, ctrlZrGrp, mo=True)

	# Countering control's transformation
	mdv = lrc.MultiplyDivide()

	mdv.attr('i2x').value = -1
	mdv.attr('i2y').value = -1
	mdv.attr('i2z').value = -1
	ctrlObj.attr('t') >> mdv.attr('i1')
	mdv.attr('o') >> ctrlInvGrp.attr('t')

	return rvt, cfme, poci, mdv

def attachControlToSelectedEdge():
	'''
	Attach selected control to selectd edge.
	'''

	sels = mc.ls(sl=True)

	ctrl = lrc.Dag(sels[0])
	ctrlInvGrp = lrc.Dag(ctrl.getParent())
	ctrlZrGrp = lrc.Dag(ctrlInvGrp.getParent())
	edge = sels[1]

	rvt, cfme, poci = pointOnEdge(ctrl, edge)

	lrc.pointConstraint(rvt, ctrlZrGrp)

	mdv = lrc.MultiplyDivide()

	mdv.attr('i2x').value = -1
	mdv.attr('i2y').value = -1
	mdv.attr('i2z').value = -1
	ctrl.attr('t') >> mdv.attr('i1')
	mdv.attr('o') >> ctrlInvGrp.attr('t')

	ndNm, ndIdx, ndSide, ndType = lrr.extractName(str(ctrl))

	rvt.name = '%s%s%sRvt%sGrp' % (ndNm, ndIdx, ndType, ndSide)
	cfme.name = '%s%s%sRvt%sCfme' % (ndNm, ndIdx, ndType, ndSide)
	poci.name = '%s%s%sRvt%sPoci' % (ndNm, ndIdx, ndType, ndSide)
	mdv.name = '%s%s%sInv%sMdv' % (ndNm, ndIdx, ndType, ndSide)

	return rvt, cfme, poci, mdv

def controlOnSelectedEdge(ctrlShp='', col=''):
	# Create control that will be sticked on selected mesh edge
	# Select transform node, edge then run script
	sels = lrc.lssl()
	
	ctrlPos = sels[0]
	edge = sels[1]
	
	jnt, jntGrp = jointOnTransform(ctrlPos)
	
	# Creating control
	ctrl, ctrlGrp = jointOnTransform(ctrlPos)
	ctrl.attr('radius').v = 0
	ctrl.lockHideAttrs('v', 'radius')
	mc.setAttr(ctrl.attr('radius'), k=False, channelBox=False)
	ctrlInvGrp = lrc.group(ctrl)
	ctrlInvMdv = lrc.MultiplyDivide()
	
	# Creating rivet
	rvt, cfme, poci = pointOnEdge(ctrlPos, edge)
	pntCon = lrc.pointConstraint(rvt, ctrlGrp, mo = True)
	
	# Countering control's transformation
	ctrlInvMdv.attr('i2x').value = -1
	ctrlInvMdv.attr('i2y').value = -1
	ctrlInvMdv.attr('i2z').value = -1
	ctrl.attr('t') >> ctrlInvMdv.attr('i1')
	ctrlInvMdv.attr('o') >> ctrlInvGrp.attr('t')
	
	# Connecting control to joint
	ctrl.rotateOrder = jnt.rotateOrder
	ctrl.attr('t') >> jnt.attr('t')
	ctrl.attr('r') >> jnt.attr('r')
	ctrl.attr('s') >> jnt.attr('s')
	
	# Control's shape adjustment
	ctrl.createCurve(ctrlShp)
	ctrl.color = col
	
	return [
				ctrl,
				ctrlInvGrp,
				ctrlGrp,
				jnt,
				jntGrp,
				ctrlInvMdv,
				rvt,
				cfme,
				poci
			]

# Naming tools
def autoName(part='', side=''):
	# Renaming facial control
	# Select control then run script

	if side:
		side = '_%s_' % side
	else:
		side = '_'

	ctrl = lrc.lssl()[0]
	ctrlInvGrp = mc.listRelatives(ctrl, p=True)[0]
	ctrlZrGrp = mc.listRelatives(ctrlInvGrp, p=True)[0]
	
	ctrlZrGrpPntCon = mc.listConnections(ctrlZrGrp, d=True, s=False)[0]
	
	sknJnt = mc.listConnections('%s.r' % ctrl, d=True, s=False)[0]
	sknJntZrGrp = mc.listRelatives(sknJnt, p=True)[0]
	ctrlInvMdv = mc.listConnections('%s.t' % ctrlInvGrp, d=False, s=True)[0]
	
	rvt = mc.listConnections('%s.target[0].targetTranslate' % ctrlZrGrpPntCon, d=False, s=True)[0]
	poci = mc.listConnections('%s.translate' % rvt, d=False, s=True)[0]
	cfme = mc.listConnections('%s.ic' % poci, d=False, s=True)[0]
	
	# Renaming
	mc.rename(ctrl, '%sDtl%sCtrl' % (part, side))
	mc.rename(ctrlInvGrp, '%sDtlCtrlInv%sGrp' % (part, side))
	mc.rename(ctrlZrGrp, '%sDtlCtrlZr%sGrp' % (part, side))
	mc.rename(ctrlZrGrpPntCon, '%sDtlCtrlZrGrp%sPointConstraint' % (part, side))
	mc.rename(sknJnt, '%sDtl%sJnt' % (part, side))
	mc.rename(sknJntZrGrp, '%sDtlJntZr%sGrp' % (part, side))
	mc.rename(ctrlInvMdv, '%sDtlCtrlInv%sMdv' % (part, side))
	mc.rename(rvt, '%sDtlRvt%sGrp' % (part, side))
	mc.rename(poci, '%sDtl%sPoci' % (part, side))
	mc.rename(cfme, '%sDtl%sCfme' % (part, side))

def autoNameList(part='', side=''):
	
	sels = mc.ls(sl=True)
	
	for ix, sel in enumerate(sels):
		
		currPart = '%s%s' % (part, chr(65+ix))
		mc.select(sel, r=True)
		autoName(part=currPart, side=side)