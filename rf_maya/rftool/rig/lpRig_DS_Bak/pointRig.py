import maya.cmds as mc

import lpRig.core as lrc
reload(lrc)
import lpRig.rigTools as lrr
reload(lrr)

class PointRig(object):

	def __init__(
					self,
					parent='',
					ctrlGrp='',
					tmpJnt='',
					part='',
					side='',
					shape='sphere',
					cons = ''
				):

		if side:
			side = '_%s_' % side
		else:
			side = '_'

		# Main group
		self.CtrlGrp = lrc.Null()
		self.CtrlGrp.name = '%sPntCtrl%sGrp' % (part, side)

		if mc.objExists(parent):
			mc.parentConstraint(parent, self.CtrlGrp)
			mc.scaleConstraint(parent, self.CtrlGrp)

		if mc.objExists(ctrlGrp):
			self.CtrlGrp.parent(ctrlGrp)

		self.Jnt = lrc.Dag(tmpJnt)
		self.Jnt.attr('ssc').v = 0

		self.Ctrl = lrr.jointControl(shape)
		self.Ctrl.name = '%sPnt%sCtrl' % (part, side)
		self.Ctrl.color = 'yellow'
		if side == '_R_':
			self.Ctrl.color = 'darkBlue'
		elif side == '_L_':
			self.Ctrl.color = 'darkRed'
		self.Ctrl.lockHideAttrs('v')

		self.Gmbl = lrc.addGimbal(self.Ctrl)
		self.Gmbl.name = '%sPntGmbl%sCtrl' % (part, side)

		self.OriGrp = lrc.group(self.Ctrl)
		self.OriGrp.name = '%sPntCtrlOri%sGrp' % (part, side)

		self.ZrGrp = lrc.group(self.OriGrp)
		self.ZrGrp.name = '%sPntCtrlZr%sGrp' % (part, side)
		self.ZrGrp.snap(self.Jnt)
		self.ZrGrp.parent(self.CtrlGrp)
		
		if mc.objExists(ctrlGrp):
			(self.LocGrp, self.WorGrp, 
			self.WorGrpParCons, self.ParGrpParCons, 
			self.ParGrpParConsRev) = lrr.doParLocWar(self.Ctrl, self.ZrGrp, 
														ctrlGrp, self.OriGrp)
		
		mc.parentConstraint(self.Gmbl, self.Jnt)
		mc.scaleConstraint(self.Gmbl, self.Jnt)