import re
from functools import partial
import math

import maya.cmds as mc
import maya.mel as mm

import lpRig.core as lrc
reload(lrc)
import lpRig.rigTools as lrr
reload(lrr)

def loc_at(obj=''):
	"""Create a locator at the position of given object.
	"""
	loc = mc.spaceLocator()[0]
	mc.delete(mc.parentConstraint(obj, loc))

	return loc

def loc_under_plane(obj='', plane='', up_ax='x'):
	"""Create a locator, snap to the position of the given obj.
	Parent to the plane object, set translate value on the given up_ax to 0.
	"""
	loc = loc_at(obj)
	mc.parent(loc, plane)
	mc.setAttr('%s.t%s' % (loc, up_ax), 0)

	return loc

def is_ik(ctrl=''):
	"""Return True if the given ctrol is an IK controller.
	"""
	re_exp = r"(.+)(Ik)([A-Z]|_)(.+)"
	re_obj = re.match(re_exp, ctrl)

	if re_obj:
		return True
	else:
		return False

def norm_side(side=''):
	side = side.replace('_', '')
	if side:
		return '_%s_' % side
	else:
		return '_'

def reset_xforms(obj=''):
	"""Reset transformations of the given object.
	"""
	mc.setAttr('%s.t' % obj, 0, 0, 0)
	mc.setAttr('%s.r' % obj, 0, 0, 0)
	mc.setAttr('%s.s' % obj, 1, 1, 1)

	return True

def find_opp(ctrl=''):
	"""Find the opposite controller of the given controller.
	"""
	if '_L_' in ctrl:
		return ctrl.replace('_L_', '_R_')
	elif '_R_' in ctrl:
		return ctrl.replace('_R_', '_L_')

def find_root(ctrl=''):
	"""Find root joint of the given node.
	"""
	ns = ''

	if ':' in ctrl:
		ns = ctrl.replace(ctrl.split(':')[-1], '')

	return '%sRoot_Jnt' % ns

def create_proxy_ctrls(src='', tar=''):
	"""Create proxy controller and its parent.
	Match rotate order and joint orient to the target.
	And match xforms to the source.
	"""
	tar_type = mc.nodeType(tar)
	tar_par = mc.listRelatives(tar, p=True)[0]

	# Getting rotate order of the target node.
	tar_ro = mc.getAttr('%s.rotateOrder' % tar)
	tar_jo = None
	if tar_type == 'joint':
		tar_jo = mc.getAttr('%s.jointOrient' % tar)[0]

	# Creating temp joints and matching the xforms.
	tmp_par = mc.createNode('transform')
	mc.delete(mc.parentConstraint(tar_par, tmp_par))

	tmp_tar = mc.createNode(tar_type)
	mc.parent(tmp_tar, tmp_par)
	mc.setAttr('%s.rotateOrder' % tmp_tar, tar_ro)
	if tar_type == 'joint':
		mc.setAttr('%s.jointOrient' % tmp_tar, tar_jo[0], tar_jo[1], tar_jo[2])
	mc.delete(mc.parentConstraint(src, tmp_tar))

	return tmp_par, tmp_tar

def copy_rot_to(src='', tar=''):
	"""Copy rotate values from s(ou)rc(e) to tar(get).
	"""
	tmp_par, tmp_tar = create_proxy_ctrls(src, tar)

	# Copying values.
	tmp_src_rot = mc.xform(tmp_tar, q=True, ws=True, ro=True)
	try:
		mc.xform(tar, ws=True, ro=tmp_src_rot)
	except:
		mc.warning('Cannot set rotate values to %s.' % tar)
		pass

	# Cleanup
	mc.delete(tmp_par)

	return True

def copy_post_to(src='', tar=''):
	"""Copy position from s(ou)rc(e) to tar(get).
	"""
	tmp_par, tmp_tar = create_proxy_ctrls(src, tar)

	# Copying values.
	tmp_src_post = mc.xform(tmp_tar, q=True, ws=True, t=True)
	try:
		mc.xform(tar, ws=True, t=tmp_src_post)
	except:
		mc.warning('Cannot set translate values to %s.' % tar)
		pass

	# Cleanup
	mc.delete(tmp_par)

	return True

def copy_pose_to(src='', tar=''):
	"""Copy xforms from s(ou)rc(e) to tar(get).
	"""
	tmp_par, tmp_tar = create_proxy_ctrls(src, tar)

	# Copying values.
	tmp_src_post = mc.xform(tmp_tar, q=True, ws=True, t=True)
	tmp_src_rot = mc.xform(tmp_tar, q=True, ws=True, ro=True)
	try:
		mc.xform(tar, ws=True, t=tmp_src_post)
	except:
		mc.warning('Cannot set translate values to %s.' % tar)
		pass
	try:
		mc.xform(tar, ws=True, ro=tmp_src_rot)
	except:
		mc.warning('Cannot set rotate values to %s.' % tar)
		pass

	# Cleanup
	mc.delete(tmp_par)

	return True

def create_mirror_jnt(ctrl=''):
	"""Create the mirrored joint of the given ctrl.
	"""
	root_jnt = find_root(ctrl)
	
	# Creating temp root.
	tmp_root_jnt = mc.createNode('joint')
	mc.delete(mc.parentConstraint(root_jnt, tmp_root_jnt))

	# Creating temp joint.
	tmp_jnt = mc.createNode('joint')
	mc.delete(mc.parentConstraint(ctrl, tmp_jnt))
	mc.parent(tmp_jnt, tmp_root_jnt)

	# Bringing temp root to the origin.
	reset_xforms(tmp_root_jnt)

	# Mirroring temp joint.
	tmp_opp_jnt = mc.mirrorJoint(tmp_jnt, mirrorYZ=True, mirrorBehavior=True)[0]

	# Snapping temp root back to its previous position.
	mc.delete(mc.parentConstraint(root_jnt, tmp_root_jnt))

	return tmp_root_jnt, tmp_jnt, tmp_opp_jnt

def mirror_selected_rot_to_opp():
	mirror_rot_to_opp(mc.ls(sl=True)[0])
	return True

def mirror_rot_to_opp(ctrl=''):
	"""Mirror rotatoin to the opposite controller.
	"""
	tmp_root_jnt, tmp_jnt, tmp_opp_jnt = create_mirror_jnt(ctrl)
	
	# Copy rotations to target
	opp = find_opp(ctrl)
	copy_rot_to(tmp_opp_jnt, opp)

	# Cleanup
	mc.delete(tmp_root_jnt)

	return True

def mirror_selected_pose_to_opp():
	mirror_pose_to_opp(mc.ls(sl=True)[0])
	return True

def mirror_pose_to_opp(ctrl=''):
	"""Mirror position to the opposite controller.
	"""
	tmp_root_jnt, tmp_jnt, tmp_opp_jnt = create_mirror_jnt(ctrl)
	
	# Copy rotations to target
	opp = find_opp(ctrl)
	copy_pose_to(tmp_opp_jnt, opp)

	# Cleanup
	mc.delete(tmp_root_jnt)

	return True

def mirror_selected():
	"""Mirror the first selected controller to the opposited controller.
	If FK controller is selected, mirror_rot_to_opp will be triggered
	else mirror_pose_to_opp will be triggered.
	"""
	sel = mc.ls(sl=True)[0]

	if is_ik(sel):
		mirror_pose_to_opp(sel)
	else:
		mirror_rot_to_opp(sel)

	mc.select(sel, r=True)

def swap_rot(ctrl=''):
	"""Swap the pose with the opposite controller.
	"""
	tmp_root_jnt, tmp_jnt, tmp_opp_jnt = create_mirror_jnt(ctrl)

	opp = find_opp(ctrl)
	opp_root_jnt, opp_jnt, curr_jnt = create_mirror_jnt(opp)

	copy_rot_to(tmp_opp_jnt, opp)
	copy_rot_to(curr_jnt, ctrl)

	# Cleanup
	mc.delete(tmp_root_jnt)
	mc.delete(opp_root_jnt)

def swap_pose(ctrl=''):
	"""Swap the pose with the opposite controller.
	"""
	tmp_root_jnt, tmp_jnt, tmp_opp_jnt = create_mirror_jnt(ctrl)

	opp = find_opp(ctrl)
	opp_root_jnt, opp_jnt, curr_jnt = create_mirror_jnt(opp)

	copy_rot_to(tmp_opp_jnt, opp)
	copy_rot_to(curr_jnt, ctrl)

	copy_post_to(tmp_opp_jnt, opp)
	copy_post_to(curr_jnt, ctrl)

	# Cleanup
	mc.delete(tmp_root_jnt)
	mc.delete(opp_root_jnt)

def swap_selected():
	"""Swap the pose of selected controller to its opposite.
	"""
	sel = mc.ls(sl=True)[0]

	if is_ik(sel):
		swap_pose(sel)
	else:
		swap_rot(sel)

	mc.select(sel, r=True)

def loc_wor_selected():
	"""Switch selected controller space from world to local or vice versa.
	"""
	sel = mc.ls(sl=True)[0]
	loc_wor(sel)
	mc.select(sel, r=True)

def loc_wor(ctrl=''):
	"""Change the orientation of the selected controller from local to world or vice versa.
	"""
	loc = loc_at(ctrl)

	curr_state = mc.getAttr('%s.localWorld' % ctrl)
	if curr_state:
		mc.setAttr('%s.localWorld' % ctrl, 0)
	else:
		mc.setAttr('%s.localWorld' % ctrl, 1)

	copy_post_to(loc, ctrl)
	copy_rot_to(loc, ctrl)

	# Cleanup
	mc.delete(loc)

def hleg_ik_to_fk(ns='', part='', side=''):
	
	side = norm_side(side)

	leg_ctrl = mc.ls('%sLeg%s%sCtrl' % (ns, part, side))[0]
	
	# FK controllers
	up_fk_ctrl = lrc.Dag(mc.ls('%sUpLegFk%s%sCtrl' % (ns, part, side))[0])
	mid_fk_ctrl = lrc.Dag(mc.ls('%sMidLegFk%s%sCtrl' % (ns, part, side))[0])
	low_fk_ctrl = lrc.Dag(mc.ls('%sLowLegFk%s%sCtrl' % (ns, part, side))[0])
	ank_fk_ctrl = lrc.Dag(mc.ls('%sAnkleFk%s%sCtrl' % (ns, part, side))[0])
	toe_fk_ctrl = lrc.Dag(mc.ls('%sToeFk%s%sCtrl' % (ns, part, side))[0])

	# IK joints
	up_ik_jnt = mc.ls('%sUpLegIk%s%sJnt' % (ns, part, side))[0]
	mid_ik_jnt = mc.ls('%sMidLegIk%s%sJnt' % (ns, part, side))[0]
	low_ik_jnt = mc.ls('%sLowLegIk%s%sJnt' % (ns, part, side))[0]
	ank_ik_jnt = mc.ls('%sAnkleIk%s%sJnt' % (ns, part, side))[0]
	ball_ik_jnt = mc.ls('%sBallIk%s%sJnt' % (ns, part, side))[0]
	toe_ik_jnt = mc.ls('%sToeIk%s%sJnt' % (ns, part, side))[0]

	copy_pose_to(up_ik_jnt, up_fk_ctrl)
	copy_pose_to(low_ik_jnt, low_fk_ctrl)
	copy_pose_to(mid_ik_jnt, mid_fk_ctrl)
	copy_pose_to(ank_ik_jnt, ank_fk_ctrl)
	copy_pose_to(ball_ik_jnt, toe_fk_ctrl)

	mc.setAttr('%s.fkIk' % leg_ctrl, 0)

# Math tools
def add_vec(a, b):
	return [a[0]+b[0], a[1]+b[1], a[2]+b[2]]

def diff_vec(a, b):
	return [a[0]-b[0], a[1]-b[1], a[2]-b[2]]

def dot_prod(a, b):
	return a[0]*b[0] + a[1]*b[1] + a[2]*b[2]

def mag(vec=(0, 0, 0)):
	return pow(pow(vec[0],2) + pow(vec[1],2) + pow(vec[2],2), 0.5)

def norm_vec(vec=(0, 0, 0)):
	return [ix/mag(vec) for ix in vec]

def angle(a_vec, b_vec):
	return math.acos(dot_prod(a_vec, b_vec) / (mag(a_vec) * mag(b_vec)))

def mid_angle(a_vec, b_vec, c_vec):
	"""Find the angle between ba and bc vectors.
	"""
	ba_vec = diff_vec(a_vec, b_vec)
	bc_vec = diff_vec(c_vec, b_vec)

	return angle(ba_vec, bc_vec)

def angle_from_three_objs(a='', b='', c=''):
	"""Find the angle between the vectors from b to a and b to c.
	"""
	a_post = mc.xform(a, q=True, ws=True, rp=True)
	b_post = mc.xform(b, q=True, ws=True, rp=True)
	c_post = mc.xform(c, q=True, ws=True, rp=True)

	return mid_angle(a_post, b_post, c_post)

def ang_to_deg(ang=0):
	return ang*((180*7)/22)

def degree_from_three_objs(a='', b='', c=''):
	"""Return the degree between the vectors from b to a and b to c.
	"""
	return ang_to_deg(angle_from_three_objs(a, b, c))

def find_polvec_post(a='', b='', c=''):
	"""Find a proper position of the polvector controller
	"""
	a_post = mc.xform(a, q=True, ws=True, rp=True)
	b_post = mc.xform(b, q=True, ws=True, rp=True)
	c_post = mc.xform(c, q=True, ws=True, rp=True)

	ab_vec = diff_vec(b_post, a_post)
	ac_vec = diff_vec(c_post, a_post)

	dot = dot_prod(ab_vec, ac_vec)
	ab_norm = norm_vec(ab_vec)

	ang = angle(ab_vec, ac_vec)
	ac_mag = mag(ac_vec)
	ac_proj_ab = ac_mag*math.cos(ang)

	proj_post = [x*ac_proj_ab for x in ab_norm]
	proj_to_ac = diff_vec(ac_vec, proj_post)

	polvec_post = add_vec(c_post, proj_to_ac)

	return polvec_post

def hleg_fk_to_ik(ns='', part='', side=''):

	side = norm_side(side)

	leg_ctrl = mc.ls('%sLeg%s%sCtrl' % (ns, part, side))[0]

	# IK controllers
	root_ik_ctrl = lrc.Dag(mc.ls('%sLegIkRoot%s%sCtrl' % (ns, part, side))[0])
	leg_ik_ctrl = lrc.Dag(mc.ls('%sLegIk%s%sCtrl' % (ns, part, side))[0])
	knee_ik_ctrl = lrc.Dag(mc.ls('%sKneeIk%s%sCtrl' % (ns, part, side))[0])

	# IK joints
	up_ik_jnt = mc.ls('%sUpLegIk%s%sJnt' % (ns, part, side))[0]
	mid_ik_jnt = mc.ls('%sMidLegIk%s%sJnt' % (ns, part, side))[0]
	low_ik_jnt = mc.ls('%sLowLegIk%s%sJnt' % (ns, part, side))[0]
	ank_ik_jnt = mc.ls('%sAnkleIk%s%sJnt' % (ns, part, side))[0]
	ball_ik_jnt = mc.ls('%sBallIk%s%sJnt' % (ns, part, side))[0]
	toe_ik_jnt = mc.ls('%sToeIk%s%sJnt' % (ns, part, side))[0]

	# FK joints
	up_fk_jnt = mc.ls('%sUpLegFk%s%sJnt' % (ns, part, side))[0]
	mid_fk_jnt = mc.ls('%sMidLegFk%s%sJnt' % (ns, part, side))[0]
	low_fk_jnt = mc.ls('%sLowLegFk%s%sJnt' % (ns, part, side))[0]
	ank_fk_jnt = mc.ls('%sAnkleFk%s%sJnt' % (ns, part, side))[0]
	ball_fk_jnt = mc.ls('%sBallFk%s%sJnt' % (ns, part, side))[0]
	toe_fk_jnt = mc.ls('%sToeFk%s%sJnt' % (ns, part, side))[0]

	copy_post_to(up_fk_jnt, root_ik_ctrl)
	copy_pose_to(ank_fk_jnt, leg_ik_ctrl)

	polvec_post = find_polvec_post(up_fk_jnt, ank_fk_jnt, low_fk_jnt)
	mc.xform(knee_ik_ctrl, ws=True, t=polvec_post)

	# Reset all values.
	attrs = ('legFlex', 'legBend', 'heelRoll', 'ballRoll', 
				'toeRoll', 'heelTwist', 'toeTwist', 
				'footRock', 'toeBend')
	[mc.setAttr('%s.%s' % (leg_ik_ctrl, x)) for x in attrs]

	# Tmp plane obj
	plane = loc_at(up_fk_jnt)

	pln_low_fk = loc_under_plane(low_fk_jnt, plane, 'x')
	pln_ank_fk = loc_under_plane(ank_fk_jnt, plane, 'x')
	pln_ball_fk = loc_under_plane(ball_fk_jnt, plane, 'x')

	ank_fk_deg = degree_from_three_objs(pln_low_fk, pln_ank_fk, pln_ball_fk)

	pln_low_ik = loc_under_plane(low_ik_jnt, plane, 'x')
	pln_ank_ik = loc_under_plane(ank_ik_jnt, plane, 'x')
	pln_ball_ik = loc_under_plane(ball_ik_jnt, plane, 'x')

	ank_ik_deg = degree_from_three_objs(pln_low_ik, pln_ank_ik, pln_ball_ik)

	mc.setAttr('%s.legFlex' % leg_ik_ctrl, ank_ik_deg - ank_fk_deg)

	mc.delete(plane)

	mc.setAttr('%s.fkIk' % leg_ctrl, 1)

class Poser(object):

	def __init__(self):

		self.ui = 'Poser'
		self.win = '%sWin' % self.ui

		win_w = 180
		win_h = 180

		if mc.window(self.win, exists=True):
			mc.deleteUI(self.win)

		mc.window(self.win, t=self.ui)
		self.main_col = mc.columnLayout(adj=True)

		but_no = 3
		but_h = int(180/3)
		self.mirror_but = mc.button(l='Mirror', c=partial(self._mirror_triggered), h=but_h)
		self.swap_but = mc.button(l='Swap', c=partial(self._swap_triggered), h=but_h)
		self.loc_wor_but = mc.button(l='Local/World', c=partial(self._loc_wor_triggered), h=but_h)

		mc.showWindow(self.win)
		mc.window(self.win, e=True, w=win_w)
		mc.window(self.win, e=True, h=win_h)

	def _mirror_triggered(self, *args):
		mirror_selected()

	def _swap_triggered(self, *args):
		swap_selected()

	def _loc_wor_triggered(self, *args):
		loc_wor_selected()

def poser_app():
	app = Poser()
	return app