import maya.cmds as mc

import lpRig.core as lrc
reload(lrc)
import lpRig.rigTools as lrr
reload(lrr)

class FingerRig(object):

	def __init__(
					self,
					fngr='Index',
					parent='Hand_L_Jnt',
					armCtrl='Arm_L_Ctrl',
					ctrlGrp='',
					tmpJnt=(
								'index_1_L_Jnt',
								'index_2_L_Jnt',
								'index_3_L_Jnt',
								'index_4_L_Jnt',
								'index_5_L_Jnt'
							),
					side=''
				):
		
		if side:
			side = '_%s_' % side
		else:
			side = '_'
		
		self.Fngr_1_Jnt = lrc.Dag(tmpJnt[0])
		self.Fngr_2_Jnt = lrc.Dag(tmpJnt[1])
		self.Fngr_3_Jnt = lrc.Dag(tmpJnt[2])
		self.Fngr_4_Jnt = lrc.Dag(tmpJnt[3])
		self.Fngr_5_Jnt = lrc.Dag(tmpJnt[4])

		self.Fngr_1_Jnt.attr('ssc').v = False

		# Main group
		self.Ctrl_Grp = lrc.Null()
		self.Ctrl_Grp.name = '%sCtrl%sGrp' % (fngr, side)

		mc.parentConstraint(parent, self.Ctrl_Grp)
		mc.scaleConstraint(parent, self.Ctrl_Grp)

		self.Fngr_Ctrl = lrc.Control('stick')
		self.Fngr_Ctrl.name = '%s%sCtrl' % (fngr, side)
		self.Fngr_Ctrl.lockHideAttrs('tx', 'ty', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz', 'v')

		self.FngrCtrlZr_Grp = lrc.group(self.Fngr_Ctrl)
		self.FngrCtrlZr_Grp.name = '%sCtrlZr%sGrp' % (fngr, side)
		self.FngrCtrlZr_Grp.snap(self.Fngr_1_Jnt)
		self.FngrCtrlZr_Grp.parent(self.Ctrl_Grp)

		# Controllers - Finger 1
		self.Fngr_1_Ctrl = lrr.jointControl('cube')
		self.Fngr_1_Ctrl.name = '%s_1%sCtrl' % (fngr, side)
		self.Fngr_1_Ctrl.lockHideAttrs('sx', 'sy', 'sz', 'v')
		self.Fngr_1_Ctrl.scaleShape(0.5)

		self.FngrCtrlTws_1_Grp = lrc.group(self.Fngr_1_Ctrl)
		self.FngrCtrlSdk_1_Grp = lrc.group(self.FngrCtrlTws_1_Grp)
		self.FngrCtrlZr_1_Grp = lrc.group(self.FngrCtrlSdk_1_Grp)

		self.FngrCtrlTws_1_Grp.name = '%sCtrlTws_1%sGrp' % (fngr, side)
		self.FngrCtrlSdk_1_Grp.name = '%sCtrlSdk_1%sGrp' % (fngr, side)
		self.FngrCtrlZr_1_Grp.name = '%sCtrlZr_1%sGrp' % (fngr, side)

		# Controllers - Finger 2
		self.Fngr_2_Ctrl = lrr.jointControl('cube')
		self.Fngr_2_Ctrl.name = '%s_2%sCtrl' % (fngr, side)
		self.Fngr_2_Ctrl.lockHideAttrs('sx', 'sy', 'sz', 'v')
		self.Fngr_2_Ctrl.scaleShape(0.5)

		self.FngrCtrlTws_2_Grp = lrc.group(self.Fngr_2_Ctrl)
		self.FngrCtrlSdk_2_Grp = lrc.group(self.FngrCtrlTws_2_Grp)
		self.FngrCtrlZr_2_Grp = lrc.group(self.FngrCtrlSdk_2_Grp)
		
		self.FngrCtrlTws_2_Grp.name = '%sCtrlTws_2%sGrp' % (fngr, side)
		self.FngrCtrlSdk_2_Grp.name = '%sCtrlSdk_2%sGrp' % (fngr, side)
		self.FngrCtrlZr_2_Grp.name = '%sCtrlZr_2%sGrp' % (fngr, side)
		
		# Controllers - Finger 3
		self.Fngr_3_Ctrl = lrr.jointControl('cube')
		self.Fngr_3_Ctrl.name = '%s_3%sCtrl' % (fngr, side)
		self.Fngr_3_Ctrl.lockHideAttrs('sx', 'sy', 'sz', 'v')
		self.Fngr_3_Ctrl.scaleShape(0.5)

		self.FngrCtrlTws_3_Grp = lrc.group(self.Fngr_3_Ctrl)
		self.FngrCtrlSdk_3_Grp = lrc.group(self.FngrCtrlTws_3_Grp)
		self.FngrCtrlZr_3_Grp = lrc.group(self.FngrCtrlSdk_3_Grp)
		
		self.FngrCtrlTws_3_Grp.name = '%sCtrlTws_3%sGrp' % (fngr, side)
		self.FngrCtrlSdk_3_Grp.name = '%sCtrlSdk_3%sGrp' % (fngr, side)
		self.FngrCtrlZr_3_Grp.name = '%sCtrlZr_3%sGrp' % (fngr, side)
		
		# Controllers - Finger 4
		self.Fngr_4_Ctrl = lrr.jointControl('cube')
		self.Fngr_4_Ctrl.name = '%s_4%sCtrl' % (fngr, side)
		self.Fngr_4_Ctrl.lockHideAttrs('sx', 'sy', 'sz', 'v')
		self.Fngr_4_Ctrl.scaleShape(0.5)
		
		self.FngrCtrlTws_4_Grp = lrc.group(self.Fngr_4_Ctrl)
		self.FngrCtrlSdk_4_Grp = lrc.group(self.FngrCtrlTws_4_Grp)
		self.FngrCtrlZr_4_Grp = lrc.group(self.FngrCtrlSdk_4_Grp)
		
		self.FngrCtrlTws_4_Grp.name = '%sCtrlTws_4%sGrp' % (fngr, side)
		self.FngrCtrlSdk_4_Grp.name = '%sCtrlSdk_4%sGrp' % (fngr, side)
		self.FngrCtrlZr_4_Grp.name = '%sCtrlZr_4%sGrp' % (fngr, side)

		# Controllers - Color
		if side == '_R_':
			self.Fngr_Ctrl.color = 'darkBlue'
			self.Fngr_1_Ctrl.color = 'darkBlue'
			self.Fngr_2_Ctrl.color = 'darkBlue'
			self.Fngr_3_Ctrl.color = 'darkBlue'
			self.Fngr_4_Ctrl.color = 'darkBlue'
		else:
			self.Fngr_Ctrl.color = 'darkRed'
			self.Fngr_1_Ctrl.color = 'darkRed'
			self.Fngr_2_Ctrl.color = 'darkRed'
			self.Fngr_3_Ctrl.color = 'darkRed'
			self.Fngr_4_Ctrl.color = 'darkRed'
		
		# Controllers - parenting and positioning
		self.FngrCtrlZr_1_Grp.rotateOrder = 'xzy'
		self.FngrCtrlZr_2_Grp.rotateOrder = 'xzy'
		self.FngrCtrlZr_3_Grp.rotateOrder = 'xzy'
		self.FngrCtrlZr_4_Grp.rotateOrder = 'xzy'
		
		self.FngrCtrlZr_1_Grp.snap(self.Fngr_1_Jnt)
		self.FngrCtrlZr_2_Grp.snap(self.Fngr_2_Jnt)
		self.FngrCtrlZr_3_Grp.snap(self.Fngr_3_Jnt)
		self.FngrCtrlZr_4_Grp.snap(self.Fngr_4_Jnt)

		self.FngrCtrlZr_4_Grp.parent(self.Fngr_3_Ctrl)
		self.FngrCtrlZr_3_Grp.parent(self.Fngr_2_Ctrl)
		self.FngrCtrlZr_2_Grp.parent(self.Fngr_1_Ctrl)
		self.FngrCtrlZr_1_Grp.parent(self.Fngr_Ctrl)

		# Stertch setup
		(self.FngrStrt_2_Add,
		self.FngrStrt_2_Mul) = lrr.fkStretch(
												ctrl=self.Fngr_2_Ctrl,
												target=self.FngrCtrlZr_3_Grp
											)
		self.FngrStrt_2_Add.name = "%sStrt_2%sAdd" % (fngr, side)
		self.FngrStrt_2_Mul.name = "%sStrt_2%sMul" % (fngr, side)

		(self.FngrStrt_3_Add,
		self.FngrStrt_3_Mul) = lrr.fkStretch(
												ctrl=self.Fngr_3_Ctrl,
												target=self.FngrCtrlZr_4_Grp
											)
		self.FngrStrt_3_Add.name = "%sStrt_3%sAdd" % (fngr, side)
		self.FngrStrt_3_Mul.name = "%sStrt_3%sMul" % (fngr, side)

		(self.FngrStrt_4_Add,
		self.FngrStrt_4_Mul) = lrr.fkStretch(
												ctrl=self.Fngr_4_Ctrl,
												target=self.Fngr_5_Jnt
											)
		self.FngrStrt_4_Add.name = "%sStrt_4%sAdd" % (fngr, side)
		self.FngrStrt_4_Mul.name = "%sStrt_4%sMul" % (fngr, side)

		# Stretch collector
		self.FngrCtrlStrt_2_Pma = lrc.PlusMinusAverage()
		self.FngrCtrlStrt_3_Pma = lrc.PlusMinusAverage()
		self.FngrCtrlStrt_4_Pma = lrc.PlusMinusAverage()

		self.FngrCtrlStrt_2_Pma.name = "%sCtrlStrt_2%sPma" % (fngr, side)
		self.FngrCtrlStrt_3_Pma.name = "%sCtrlStrt_3%sPma" % (fngr, side)
		self.FngrCtrlStrt_4_Pma.name = "%sCtrlStrt_4%sPma" % (fngr, side)

		self.FngrCtrlStrt_2_Pma.attr('o1') >> self.FngrCtrlZr_3_Grp.attr('ty')
		self.FngrCtrlStrt_3_Pma.attr('o1') >> self.FngrCtrlZr_4_Grp.attr('ty')
		self.FngrCtrlStrt_4_Pma.attr('o1') >> self.Fngr_5_Jnt.attr('ty')
		
		self.FngrStrt_2_Add.attr('o') >> self.FngrCtrlStrt_2_Pma.last1D()
		self.FngrStrt_3_Add.attr('o') >> self.FngrCtrlStrt_3_Pma.last1D()
		self.FngrStrt_4_Add.attr('o') >> self.FngrCtrlStrt_4_Pma.last1D()

		# Squash setup
		(self.FngrSqs_2_Add,
		self.FngrSqs_2_Mul) = lrr.fkSquash(
												ctrl=self.Fngr_2_Ctrl,
												target=self.Fngr_2_Jnt
											)
		self.FngrSqs_2_Add.name = "%sSqs_2%sAdd" % (fngr, side)
		self.FngrSqs_2_Mul.name = "%sSqs_2%sMul" % (fngr, side)

		(self.FngrSqs_3_Add,
		self.FngrSqs_3_Mul) = lrr.fkSquash(
												ctrl=self.Fngr_3_Ctrl,
												target=self.Fngr_3_Jnt
											)
		self.FngrSqs_3_Add.name = "%sSqs_3%sAdd" % (fngr, side)
		self.FngrSqs_3_Mul.name = "%sSqs_3%sMul" % (fngr, side)

		(self.FngrSqs_4_Add,
		self.FngrSqs_4_Mul) = lrr.fkSquash(
												ctrl=self.Fngr_4_Ctrl,
												target=self.Fngr_4_Jnt
											)
		self.FngrSqs_4_Add.name = "%sSqs_4%sAdd" % (fngr, side)
		self.FngrSqs_4_Mul.name = "%sSqs_4%sMul" % (fngr, side)

		(self.FngrSqsTip_4_Add,
		self.FngrSqsTip_4_Mul) = lrr.fkSquash(
												ctrl=self.Fngr_4_Ctrl,
												target=self.Fngr_5_Jnt
											)
		self.FngrSqsTip_4_Add.name = "%sSqsTip_4%sAdd" % (fngr, side)
		self.FngrSqsTip_4_Mul.name = "%sSqsTip_4%sMul" % (fngr, side)

		# Squash setup - Collectors
		self.FngrSqs_2_Pma = lrc.PlusMinusAverage()
		self.FngrSqs_3_Pma = lrc.PlusMinusAverage()
		self.FngrSqs_4_Pma = lrc.PlusMinusAverage()
		self.FngrSqsTip_4_Pma = lrc.PlusMinusAverage()

		self.FngrSqs_2_Pma.name = "%sSqs_2%sPma" % (fngr, side)
		self.FngrSqs_3_Pma.name = "%sSqs_3%sPma" % (fngr, side)
		self.FngrSqs_4_Pma.name = "%sSqs_4%sPma" % (fngr, side)
		self.FngrSqsTip_4_Pma.name = "%sSqsTip_4%sPma" % (fngr, side)
		
		self.FngrSqs_2_Pma.attr('o1') >> self.Fngr_2_Jnt.attr('sx')
		self.FngrSqs_2_Pma.attr('o1') >> self.Fngr_2_Jnt.attr('sz')
		self.FngrSqs_3_Pma.attr('o1') >> self.Fngr_3_Jnt.attr('sx')
		self.FngrSqs_3_Pma.attr('o1') >> self.Fngr_3_Jnt.attr('sz')
		self.FngrSqs_4_Pma.attr('o1') >> self.Fngr_4_Jnt.attr('sx')
		self.FngrSqs_4_Pma.attr('o1') >> self.Fngr_4_Jnt.attr('sz')
		self.FngrSqsTip_4_Pma.attr('o1') >> self.Fngr_5_Jnt.attr('sx')
		self.FngrSqsTip_4_Pma.attr('o1') >> self.Fngr_5_Jnt.attr('sz')
		
		self.FngrSqs_2_Add.attr('o') >> self.FngrSqs_2_Pma.last1D()
		self.FngrSqs_3_Add.attr('o') >> self.FngrSqs_3_Pma.last1D()
		self.FngrSqs_4_Add.attr('o') >> self.FngrSqs_4_Pma.last1D()
		self.FngrSqsTip_4_Add.attr('o') >> self.FngrSqsTip_4_Pma.last1D()
		
		# Connect to joints
		mc.parentConstraint(self.Fngr_1_Ctrl, self.Fngr_1_Jnt)
		mc.parentConstraint(self.Fngr_2_Ctrl, self.Fngr_2_Jnt)
		mc.parentConstraint(self.Fngr_3_Ctrl, self.Fngr_3_Jnt)
		mc.parentConstraint(self.Fngr_4_Ctrl, self.Fngr_4_Jnt)
		
		# Translation Collectors
		self.FngrSdkTx_1_Pma = lrc.PlusMinusAverage()
		self.FngrSdkTy_1_Pma = lrc.PlusMinusAverage()
		self.FngrSdkTz_1_Pma = lrc.PlusMinusAverage()

		self.FngrSdkTx_1_Pma.name = "%sSdkTx_1%sPma" % (fngr, side)
		self.FngrSdkTy_1_Pma.name = "%sSdkTy_1%sPma" % (fngr, side)
		self.FngrSdkTz_1_Pma.name = "%sSdkTz_1%sPma" % (fngr, side)

		# Rotation collectors
		self.FngrSdkRx_1_Pma = lrc.PlusMinusAverage()
		self.FngrSdkRx_2_Pma = lrc.PlusMinusAverage()
		self.FngrSdkRx_3_Pma = lrc.PlusMinusAverage()
		self.FngrSdkRx_4_Pma = lrc.PlusMinusAverage()

		self.FngrSdkRy_1_Pma = lrc.PlusMinusAverage()
		self.FngrSdkRy_2_Pma = lrc.PlusMinusAverage()
		self.FngrSdkRy_3_Pma = lrc.PlusMinusAverage()
		self.FngrSdkRy_4_Pma = lrc.PlusMinusAverage()

		self.FngrSdkRz_1_Pma = lrc.PlusMinusAverage()
		self.FngrSdkRz_2_Pma = lrc.PlusMinusAverage()
		self.FngrSdkRz_3_Pma = lrc.PlusMinusAverage()
		self.FngrSdkRz_4_Pma = lrc.PlusMinusAverage()

		self.FngrSdkRx_1_Pma.name = "%sSdkRx_1%sPma" % (fngr, side)
		self.FngrSdkRx_2_Pma.name = "%sSdkRx_2%sPma" % (fngr, side)
		self.FngrSdkRx_3_Pma.name = "%sSdkRx_3%sPma" % (fngr, side)
		self.FngrSdkRx_4_Pma.name = "%sSdkRx_4%sPma" % (fngr, side)

		self.FngrSdkRy_1_Pma.name = "%sSdkRy_1%sPma" % (fngr, side)
		self.FngrSdkRy_2_Pma.name = "%sSdkRy_2%sPma" % (fngr, side)
		self.FngrSdkRy_3_Pma.name = "%sSdkRy_3%sPma" % (fngr, side)
		self.FngrSdkRy_4_Pma.name = "%sSdkRy_4%sPma" % (fngr, side)

		self.FngrSdkRz_1_Pma.name = "%sSdkRz_1%sPma" % (fngr, side)
		self.FngrSdkRz_2_Pma.name = "%sSdkRz_2%sPma" % (fngr, side)
		self.FngrSdkRz_3_Pma.name = "%sSdkRz_3%sPma" % (fngr, side)
		self.FngrSdkRz_4_Pma.name = "%sSdkRz_4%sPma" % (fngr, side)

		self.FngrSdkTx_1_Pma.attr('o1') >> self.FngrCtrlSdk_1_Grp.attr('tx')
		self.FngrSdkTy_1_Pma.attr('o1') >> self.FngrCtrlSdk_1_Grp.attr('ty')
		self.FngrSdkTz_1_Pma.attr('o1') >> self.FngrCtrlSdk_1_Grp.attr('tz')
		
		self.FngrSdkRx_1_Pma.attr('o1') >> self.FngrCtrlSdk_1_Grp.attr('rx')
		self.FngrSdkRx_2_Pma.attr('o1') >> self.FngrCtrlSdk_2_Grp.attr('rx')
		self.FngrSdkRx_3_Pma.attr('o1') >> self.FngrCtrlSdk_3_Grp.attr('rx')
		self.FngrSdkRx_4_Pma.attr('o1') >> self.FngrCtrlSdk_4_Grp.attr('rx')

		self.FngrSdkRy_1_Pma.attr('o1') >> self.FngrCtrlTws_1_Grp.attr('ry')
		self.FngrSdkRy_2_Pma.attr('o1') >> self.FngrCtrlTws_2_Grp.attr('ry')
		self.FngrSdkRy_3_Pma.attr('o1') >> self.FngrCtrlTws_3_Grp.attr('ry')
		self.FngrSdkRy_4_Pma.attr('o1') >> self.FngrCtrlTws_4_Grp.attr('ry')

		self.FngrSdkRz_1_Pma.attr('o1') >> self.FngrCtrlSdk_1_Grp.attr('rz')
		self.FngrSdkRz_2_Pma.attr('o1') >> self.FngrCtrlSdk_2_Grp.attr('rz')
		self.FngrSdkRz_3_Pma.attr('o1') >> self.FngrCtrlSdk_3_Grp.attr('rz')
		self.FngrSdkRz_4_Pma.attr('o1') >> self.FngrCtrlSdk_4_Grp.attr('rz')
		
		# Hand attributes
		handCtrl = lrc.Dag(armCtrl)

		if not handCtrl.attr('__hand__').exists :
			handCtrl.add(ln = '__hand__', k = True)
			handCtrl.attr('__hand__').set(l = True)

		# Hand attributes - flat
		if not handCtrl.attr('flat').exists :
			handCtrl.add(ln='flat', k=True)

		self.FngrFlatTxAmp_1_Mul = lrr.attrAmper(
													handCtrl.attr('flat'),
													self.FngrSdkTx_1_Pma.last1D(),
													0,
													'%sFlatTx_1' % fngr
												)
		self.FngrFlatTyAmp_1_Mul = lrr.attrAmper(
													handCtrl.attr('flat'),
													self.FngrSdkTy_1_Pma.last1D(),
													0,
													'%sFlatTy_1' % fngr
												)
		self.FngrFlatTzAmp_1_Mul = lrr.attrAmper(
													handCtrl.attr('flat'),
													self.FngrSdkTz_1_Pma.last1D(),
													0,
													'%sFlatTz_1' % fngr
												)
		self.FngrFlatRxAmp_1_Mul = lrr.attrAmper(
													handCtrl.attr('flat'),
													self.FngrSdkRx_1_Pma.last1D(),
													-2,
													'%sFlatRx_1' % fngr
												)
		self.FngrFlatRzAmp_1_Mul = lrr.attrAmper(
													handCtrl.attr('flat'),
													self.FngrSdkRz_1_Pma.last1D(),
													0,
													'%sFlatRz_1' % fngr
												)
		self.FngrFlatRxAmp_2_Mul = lrr.attrAmper(
													handCtrl.attr('flat'),
													self.FngrSdkRx_2_Pma.last1D(),
													-1,
													'%sFlatRx_2' % fngr
												)
		self.FngrFlatRxAmp_3_Mul = lrr.attrAmper(
													handCtrl.attr('flat'),
													self.FngrSdkRx_3_Pma.last1D(),
													2,
													'%sFlatRx_3' % fngr
												)
		self.FngrFlatRxAmp_4_Mul = lrr.attrAmper(
													handCtrl.attr('flat'),
													self.FngrSdkRx_4_Pma.last1D(),
													1,
													'%sFlatRx_4' % fngr
												)
		
		self.FngrFlatTxAmp_1_Mul.name = "%sFlatTxAmp_1%sMul" % (fngr, side)
		self.FngrFlatTyAmp_1_Mul.name = "%sFlatTyAmp_1%sMul" % (fngr, side)
		self.FngrFlatTzAmp_1_Mul.name = "%sFlatTzAmp_1%sMul" % (fngr, side)
		self.FngrFlatRxAmp_1_Mul.name = "%sFlatRxAmp_1%sMul" % (fngr, side)
		self.FngrFlatRzAmp_1_Mul.name = "%sFlatRzAmp_1%sMul" % (fngr, side)
		self.FngrFlatRxAmp_2_Mul.name = "%sFlatRxAmp_2%sMul" % (fngr, side)
		self.FngrFlatRxAmp_3_Mul.name = "%sFlatRxAmp_3%sMul" % (fngr, side)
		self.FngrFlatRxAmp_4_Mul.name = "%sFlatRxAmp_4%sMul" % (fngr, side)
		
		# Hand attributes - fist
		if not handCtrl.attr('fist').exists :
			handCtrl.add(ln='fist', k=True)
		
		self.FngrFistRxAmp_1_Mul = lrr.attrAmper(handCtrl.attr('fist'), self.FngrSdkRx_1_Pma.last1D(), 0, '%sFistRx_1'%fngr)
		self.FngrFistRzAmp_1_Mul = lrr.attrAmper(handCtrl.attr('fist'), self.FngrSdkRz_1_Pma.last1D(), 0, '%sFistRz_1'%fngr)
		self.FngrFistRxAmp_2_Mul = lrr.attrAmper(handCtrl.attr('fist'), self.FngrSdkRx_2_Pma.last1D(), -9, '%sFistRx_2'%fngr)
		self.FngrFistRyAmp_2_Mul = lrr.attrAmper(handCtrl.attr('fist'), self.FngrSdkRy_2_Pma.last1D(), 0, '%sFistRy_2'%fngr)
		self.FngrFistRzAmp_2_Mul = lrr.attrAmper(handCtrl.attr('fist'), self.FngrSdkRz_2_Pma.last1D(), 0, '%sFistRz_2'%fngr)
		self.FngrFistRxAmp_3_Mul = lrr.attrAmper(handCtrl.attr('fist'), self.FngrSdkRx_3_Pma.last1D(), -9, '%sFistRx_3'%fngr)
		self.FngrFistRxAmp_4_Mul = lrr.attrAmper(handCtrl.attr('fist'), self.FngrSdkRx_4_Pma.last1D(), -9, '%sFistRx_4'%fngr)
		
		self.FngrFistRxAmp_1_Mul.name = "%sFistRxAmp_1%sMul" % (fngr, side)
		self.FngrFistRzAmp_1_Mul.name = "%sFistRzAmp_1%sMul" % (fngr, side)
		self.FngrFistRxAmp_2_Mul.name = "%sFistRxAmp_2%sMul" % (fngr, side)
		self.FngrFistRyAmp_2_Mul.name = "%sFistRyAmp_2%sMul" % (fngr, side)
		self.FngrFistRzAmp_2_Mul.name = "%sFistRzAmp_2%sMul" % (fngr, side)
		self.FngrFistRxAmp_3_Mul.name = "%sFistRxAmp_3%sMul" % (fngr, side)
		self.FngrFistRxAmp_4_Mul.name = "%sFistRxAmp_4%sMul" % (fngr, side)
		
		# Hand attributes - scrunch
		if not handCtrl.attr('scrunch').exists :
			handCtrl.add(ln='scrunch', k=True)
		
		self.FngrScrunchRxAmp_2_Mul = lrr.attrAmper(handCtrl.attr('scrunch'), self.FngrSdkRx_2_Pma.last1D(), 9, '%sScrunchRx_2'%fngr)
		self.FngrScrunchRxAmp_3_Mul = lrr.attrAmper(handCtrl.attr('scrunch'), self.FngrSdkRx_3_Pma.last1D(), -9, '%sScrunchRx_3'%fngr)
		self.FngrScrunchRxAmp_4_Mul = lrr.attrAmper(handCtrl.attr('scrunch'), self.FngrSdkRx_4_Pma.last1D(), -4.5, '%sScrunchRx_4'%fngr)
		
		self.FngrScrunchRxAmp_2_Mul.name = "%sScrunchRxAmp_2%sMul" % (fngr, side)
		self.FngrScrunchRxAmp_3_Mul.name = "%sScrunchRxAmp_3%sMul" % (fngr, side)
		self.FngrScrunchRxAmp_4_Mul.name = "%sScrunchRxAmp_4%sMul" % (fngr, side)
		
		# Hand attributes - cup
		if not handCtrl.attr('cup').exists :
			handCtrl.add(ln='cup', k=True)

		self.FngrCupRxAmp_1_Mul = lrr.attrAmper(handCtrl.attr('cup'), self.FngrSdkRx_1_Pma.last1D(), -4.5, '%sCupRx_1'%fngr)
		self.FngrCupRxAmp_2_Mul = lrr.attrAmper(handCtrl.attr('cup'), self.FngrSdkRx_2_Pma.last1D(), -9, '%sCupRx_2'%fngr)
		self.FngrCupRxAmp_3_Mul = lrr.attrAmper(handCtrl.attr('cup'), self.FngrSdkRx_3_Pma.last1D(), -9, '%sCupRx_3'%fngr)
		self.FngrCupRxAmp_4_Mul = lrr.attrAmper(handCtrl.attr('cup'), self.FngrSdkRx_4_Pma.last1D(), -9, '%sCupRx_4'%fngr)

		self.FngrCupRxAmp_1_Mul.name = "%sCupRxAmp_1%sMul" % (fngr, side)
		self.FngrCupRxAmp_2_Mul.name = "%sCupRxAmp_2%sMul" % (fngr, side)
		self.FngrCupRxAmp_3_Mul.name = "%sCupRxAmp_3%sMul" % (fngr, side)
		self.FngrCupRxAmp_4_Mul.name = "%sCupRxAmp_4%sMul" % (fngr, side)

		# Hand attributes - spread
		if not handCtrl.attr('spread').exists :
			handCtrl.add(ln='spread', k=True)

		self.FngrSprdAmp_Mul = lrr.attrAmper(handCtrl.attr('spread'), self.FngrSdkRz_2_Pma.last1D(), -9, '%sSprd_2'%fngr)
		self.FngrSprdAmp_Mul.name = "%sSprdAmp_1%sMul" % (fngr, side)

		# Hand attributes - break
		if not handCtrl.attr('break').exists :
			handCtrl.add(ln='break', k=True)

		self.FngrBrkAmp_2_Mul = lrr.attrAmper(handCtrl.attr('break'), self.FngrSdkRz_2_Pma.last1D(), -9, '%sBrk_2'%fngr)
		self.FngrBrkAmp_2_Mul.name = "%sBrkAmp_1%sMul" % (fngr, side)
		
		# Hand attributes - flex
		if not handCtrl.attr('flex').exists :
			handCtrl.add(ln='flex', k=True)

		self.FngrFlexAmp_2_Mul = lrr.attrAmper(handCtrl.attr('flex'), self.FngrSdkRx_2_Pma.last1D(), -9, '%sFlex_2'%fngr)
		self.FngrFlexAmp_2_Mul.name = "%sFlexAmp_1%sMul" % (fngr, side)

		# Hand attributes - baseSpread
		if not handCtrl.attr('baseSpread').exists :
			handCtrl.add(ln='baseSpread', k=True)

		self.FngrBsSprdAmp_1_Mul = lrr.attrAmper(handCtrl.attr('baseSpread'), self.FngrSdkRz_1_Pma.last1D(), 9, '%sBsSprd_1'%fngr)
		self.FngrBsSprdAmp_1_Mul.name = "%sBsSprdAmp_1%sMul" % (fngr, side)

		# Hand attributes - baseBreak
		if not handCtrl.attr('baseBreak').exists :
			handCtrl.add(ln='baseBreak', k=True)

		self.FngrBsBrkAmp_1_Mul = lrr.attrAmper(handCtrl.attr('baseBreak'), self.FngrSdkRz_1_Pma.last1D(), 9, '%sBsBrk_1'%fngr)
		self.FngrBsBrkAmp_1_Mul.name = "%sBsBrkAmp_1%sMul" % (fngr, side)

		# Hand attributes - baseFlex
		if not handCtrl.attr('baseFlex').exists :
			handCtrl.add(ln='baseFlex', k=True)

		self.FngrBsFlexAmp_1_Mul = lrr.attrAmper(handCtrl.attr('baseFlex'), self.FngrSdkRx_1_Pma.last1D(), -9, '%sBsFlex_1'%fngr)
		self.FngrBsFlexAmp_1_Mul.name = "%sBsFlexAmp_1%sMul" % (fngr, side)
		
		# Finger attributes
		self.Fngr_Ctrl.add(ln='__%s__' % fngr, k=True)
		self.Fngr_Ctrl.attr('__%s__' % fngr).set(l=True)

		# Finger attributes - fold
		self.Fngr_Ctrl.add(ln='fold', k=True)
		
		self.FngrFoldAmp_2_Mul = lrr.attrAmper(self.Fngr_Ctrl.attr('fold'), self.FngrSdkRx_2_Pma.last1D(),-9)
		self.FngrFoldAmp_3_Mul = lrr.attrAmper(self.Fngr_Ctrl.attr('fold'), self.FngrSdkRx_3_Pma.last1D(),-9)
		self.FngrFoldAmp_4_Mul = lrr.attrAmper(self.Fngr_Ctrl.attr('fold'), self.FngrSdkRx_4_Pma.last1D(),-9)

		self.FngrFoldAmp_2_Mul.name = "%sFldAmp_2%sMul" % (fngr, side)
		self.FngrFoldAmp_3_Mul.name = "%sFldAmp_3%sMul" % (fngr, side)
		self.FngrFoldAmp_4_Mul.name = "%sFldAmp_4%sMul" % (fngr, side)
		
		# Finger attributes - stretch
		self.Fngr_Ctrl.add(ln='stretch', k=True)
		
		self.FngrStrtAmp_2_Mul = lrr.attrAmper(self.Fngr_Ctrl.attr('stretch'), self.FngrCtrlStrt_2_Pma.last1D(), self.FngrStrt_2_Mul.attr('i1').v)
		self.FngrStrtAmp_3_Mul = lrr.attrAmper(self.Fngr_Ctrl.attr('stretch'), self.FngrCtrlStrt_3_Pma.last1D(), self.FngrStrt_3_Mul.attr('i1').v)
		self.FngrStrtAmp_4_Mul = lrr.attrAmper(self.Fngr_Ctrl.attr('stretch'), self.FngrCtrlStrt_4_Pma.last1D(), self.FngrStrt_4_Mul.attr('i1').v)

		self.FngrStrtAmp_2_Mul.name = "%sStrtAmp_2%sMul" % (fngr, side)
		self.FngrStrtAmp_3_Mul.name = "%sStrtAmp_3%sMul" % (fngr, side)
		self.FngrStrtAmp_4_Mul.name = "%sStrtAmp_4%sMul" % (fngr, side)

		# Finger attributes - squash
		self.Fngr_Ctrl.add(ln='squash', k=True)
		
		self.FngrSqsAmp_2_Mul = lrr.attrAmper(self.Fngr_Ctrl.attr('squash'), self.FngrSqs_2_Pma.last1D(), 0.1)
		self.FngrSqsAmp_3_Mul = lrr.attrAmper(self.Fngr_Ctrl.attr('squash'), self.FngrSqs_3_Pma.last1D(), 0.1)
		self.FngrSqsAmp_4_Mul = lrr.attrAmper(self.Fngr_Ctrl.attr('squash'), self.FngrSqs_4_Pma.last1D(), 0.1)
		self.FngrSqsTipAmp_4_Mul = lrr.attrAmper(self.Fngr_Ctrl.attr('squash'), self.FngrSqsTip_4_Pma.last1D(), 0.1)

		self.FngrSqsAmp_2_Mul.name = "%sSqsAmp_2%sMul" % (fngr, side)
		self.FngrSqsAmp_3_Mul.name = "%sSqsAmp_3%sMul" % (fngr, side)
		self.FngrSqsAmp_4_Mul.name = "%sSqsAmp_4%sMul" % (fngr, side)
		self.FngrSqsTipAmp_4_Mul.name = "%sSqsTipAmp_4%sMul" % (fngr, side)

		# fngr attribute - __fngr1__
		self.Fngr_Ctrl.add(ln = '__fngr1__', k = True)
		self.Fngr_Ctrl.attr('__fngr1__').set(l = True)
		
		# fngr attribute - __fngr1__ - fngr1Fold
		self.Fngr_Ctrl.add(ln = 'fngr1Fold', k = True)
		self.FngrFoldAmp_1_Mul = lrr.attrAmper(self.Fngr_Ctrl.attr('fngr1Fold'), self.FngrSdkRx_1_Pma.last1D(), -9)
		self.FngrFoldAmp_1_Mul.name = "%sFoldAmp_1%sMul" % (fngr, side)

		# fngr attribute - __fngr1__ - fngr1Spread
		self.Fngr_Ctrl.add(ln = 'fngr1Spread', k = True)
		self.FngrSprdAmp_1_Mul = lrr.attrAmper(self.Fngr_Ctrl.attr('fngr1Spread'), self.FngrSdkRz_1_Pma.last1D(), -9)
		self.FngrSprdAmp_1_Mul.name = "%sSprdAmp_1%sMul" % (fngr, side)

		# fngr attribute - __fngr1__ - fngr1Twist
		self.Fngr_Ctrl.add(ln = 'fngr1Twist', k = True)
		self.FngrTwsAmp_1_Mul = lrr.attrAmper(self.Fngr_Ctrl.attr('fngr1Twist'), self.FngrSdkRy_1_Pma.last1D(), -9)
		self.FngrTwsAmp_1_Mul.name = "%sTwsAmp_1%sMul" % (fngr, side)

		# fngr attribute - __fngr2__
		self.Fngr_Ctrl.add(ln = '__fngr2__', k = True)
		self.Fngr_Ctrl.attr('__fngr2__').set(l = True)
		
		# fngr attribute - __fngr2__ - fngr2Fold
		self.Fngr_Ctrl.add(ln = 'fngr2Fold', k = True)
		self.FngrFoldAmp_2_Mul = lrr.attrAmper(self.Fngr_Ctrl.attr('fngr2Fold'), self.FngrSdkRx_2_Pma.last1D(), -9)
		self.FngrFoldAmp_2_Mul.name = "%sFoldAmp_2%sMul" % (fngr, side)

		# fngr attribute - __fngr2__ - fngr2Spread
		self.Fngr_Ctrl.add(ln = 'fngr2Spread', k = True)
		self.FngrSprdAmp_2_Mul = lrr.attrAmper(self.Fngr_Ctrl.attr('fngr2Spread'), self.FngrSdkRz_2_Pma.last1D(), -9)
		self.FngrSprdAmp_2_Mul.name = "%sSprdAmp_2%sMul" % (fngr, side)

		# fngr attribute - __fngr2__ - fngr2Twist
		self.Fngr_Ctrl.add(ln = 'fngr2Twist', k = True)
		self.FngrTwsAmp_2_Mul = lrr.attrAmper(self.Fngr_Ctrl.attr('fngr2Twist'), self.FngrSdkRy_2_Pma.last1D(), -9)
		self.FngrTwsAmp_2_Mul.name = "%sTwsAmp_2%sMul" % (fngr, side)
		
		# fngr attribute - __fngr3__
		self.Fngr_Ctrl.add(ln = '__fngr3__', k = True)
		self.Fngr_Ctrl.attr('__fngr3__').set(l = True)
		
		# fngr attribute - __fngr3__ - fngr3Fold
		self.Fngr_Ctrl.add(ln = 'fngr3Fold', k = True)
		self.FngrFoldAmp_3_Mul = lrr.attrAmper(self.Fngr_Ctrl.attr('fngr3Fold'), self.FngrSdkRx_3_Pma.last1D(), -9)
		self.FngrFoldAmp_3_Mul.name = "%sFoldAmp_3%sMul" % (fngr, side)

		# fngr attribute - __fngr3__ - fngr3Spread
		self.Fngr_Ctrl.add(ln = 'fngr3Spread', k = True)
		self.FngrSprdAmp_3_Mul = lrr.attrAmper(self.Fngr_Ctrl.attr('fngr3Spread'), self.FngrSdkRz_3_Pma.last1D(), -9)
		self.FngrSprdAmp_3_Mul.name = "%sSprdAmp_3%sMul" % (fngr, side)

		# fngr attribute - __fngr3__ - fngr3Twist
		self.Fngr_Ctrl.add(ln = 'fngr3Twist', k = True)
		self.FngrTwsAmp_3_Mul = lrr.attrAmper(self.Fngr_Ctrl.attr('fngr3Twist'), self.FngrSdkRy_3_Pma.last1D(), -9)
		self.FngrTwsAmp_3_Mul.name = "%sTwsAmp_3%sMul" % (fngr, side)
		
		# fngr attribute - __fngr4__
		self.Fngr_Ctrl.add(ln = '__fngr4__', k = True)
		self.Fngr_Ctrl.attr('__fngr4__').set(l = True)
		
		# fngr attribute - __fngr4__ - fngr4Fold
		self.Fngr_Ctrl.add(ln = 'fngr4Fold', k = True)
		self.FngrFoldAmp_4_Mul = lrr.attrAmper(self.Fngr_Ctrl.attr('fngr4Fold'), self.FngrSdkRx_4_Pma.last1D(), -9)
		self.FngrFoldAmp_4_Mul.name = "%sFoldAmp_4%sMul" % (fngr, side)

		# fngr attribute - __fngr4__ - fngr4Spread
		self.Fngr_Ctrl.add(ln = 'fngr4Spread', k = True)
		self.FngrSprdAmp_4_Mul = lrr.attrAmper(self.Fngr_Ctrl.attr('fngr4Spread'), self.FngrSdkRz_4_Pma.last1D(), -9)
		self.FngrSprdAmp_4_Mul.name = "%sSprdAmp_4%sMul" % (fngr, side)

		# fngr attribute - __fngr4__ - fngr4Twist
		self.Fngr_Ctrl.add(ln = 'fngr4Twist', k = True)
		self.FngrTwsAmp_4_Mul = lrr.attrAmper(self.Fngr_Ctrl.attr('fngr4Twist'), self.FngrSdkRy_4_Pma.last1D(), -9)
		self.FngrTwsAmp_4_Mul.name = "%sTwsAmp_4%sMul" % (fngr, side)
		
		# Grouping
		self.Ctrl_Grp.parent(ctrlGrp)