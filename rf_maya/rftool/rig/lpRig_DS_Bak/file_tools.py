import os
import re

import maya.cmds as mc
import maya.mel as mm

import rigTools as rts
reload(rts)

def file_info():
	'''
	Split folder path, file name and file extension from current scene path.
	'''
	curr_path = os.path.normpath(mc.file(q=True, l=True)[0])

	folder_path, file_name_ext = os.path.split(curr_path)
	file_name, file_ext = os.path.splitext(file_name_ext)

	return folder_path, file_name, file_ext

def ls(task=''):
	'''
	List all task files.
	'''
	folder_path, file_name, file_ext = file_info()

	files = [x for x in os.listdir(folder_path) \
				if os.path.isfile(os.path.join(folder_path, x))]

	if not files:
		return None

	if task:
		files = [x for x in files if x.startswith(task)]

	for _file in sorted(files):
		print _file

def save_warning():
	'''
	Check if current scene has been modified.
	'''
	if not mc.file(q=True, mf=True):
		return True
	
	folder_path, file_name, file_ext = file_info()
	
	result = mc.confirmDialog(title='Warning: Scene Not Saved', 
								message='Save changes to {file_name}?'.format(file_name=file_name), 
								button=['Save', "Don't Save", 'Cancel'], 
								defaultButton='Cancel', cancelButton='Cancel', 
								dismissString='Cancel' )

	if result == 'Save':
		mc.file(s=True, f=True, type='mayaAscii')
		return True

	elif result == "Don't Save":
		return True

	elif result == 'Cancel':
		return False

def get_last_ver(folder_path='', task='rig'):
	'''
	Get the last version of given task file in given folder path.
	'''
	ver = 0

	folder_path = os.path.normpath(folder_path)

	files = [x for x in os.listdir(folder_path) \
				if os.path.isfile(os.path.join(folder_path, x))]

	re_exp = r'({task})(_v)(\d+)'.format(task=task)

	for _file in files:

		re_obj = re.findall(re_exp, _file)

		if not re_obj:
			continue

		curr_ver = int(re_obj[0][2])

		if curr_ver > ver:
			ver = curr_ver

	return ver

def save_to(task='rig'):
	'''
	Save to the latest version of given task file.
	'''
	if not save_warning():
		return False

	folder_path, file_name, file_ext = file_info()

	ver = get_last_ver(folder_path, task)

	new_file_name = '{task}_v{ver}'.format(task=task, ver=ver+1)
	new_file_path = os.path.join(folder_path, new_file_name)

	mc.file(rn=new_file_path)
	mc.file(s=True, f=True, type='mayaAscii')

	return True

st = save_to

def save_next():
	"""Save the current working file to the next version.
	"""
	folder_path, file_name, file_ext = file_info()
	task_name = file_name.split('_')[0]
	save_to(task_name)

sn = save_next

def open_last(task='rig'):
	'''
	Open the latest version of given task file.
	'''
	if not save_warning():
		return False

	folder_path, file_name, file_ext = file_info()

	ver = get_last_ver(folder_path, task)

	new_file = '{task}_v{ver}.ma'.format(task=task, ver=ver)
	new_file_path = os.path.join(folder_path, new_file)

	mc.file(new_file_path, o=True, f=True)

	return True

ol = open_last

def open_pub(task='rig'):

	if not save_warning():
		return False

	folder_path, file_name, file_ext = file_info()

	new_file = '{task}.ma'.format(task=task)
	new_file_path = os.path.join(folder_path, new_file)

	mc.file(new_file_path, o=True, f=True)
	
	return True

op = open_pub

def publish():
	'''
	Publish current scene.
	'''
	if not save_warning():
		return False

	folder_path, file_name, file_ext = file_info()

	re_exp = r'(.+)(_v)(\d+)'
	re_obj = re.findall(re_exp, file_name)

	if not re_obj:
		raise ValueError('{file_name} has invalid name.'.format(file_name=file_name))

	task = re_obj[0][0]

	# Run default actions.
	rts.importAllReference()
	rts.removeAllNamespace()
	rts.removeAllAnimCurve()
	rts.cleanupScene()
	rts.removeTmpGrp()
	rts.removeAllUnknowns()
	rts.hideAllAiCurveAttrs()

	# Additional publish actions according to task.
	publish_action_callback(task)

	# Save
	pub_file_path = os.path.join(folder_path, task)

	mc.file(rn=pub_file_path)
	mc.file(s=True, f=True, type='mayaAscii')

pub = publish

def publish_action_callback(task=''):

	if task == 'rig':
		rig_publish_action()

def rig_publish_action():

	rts.cleanupScene()

def ref(task=''):
	'''
	Reference published task file to the current scene.
	'''
	folder_path, file_name, file_ext = file_info()

	mc.workspace(dir=folder_path)

	file_name = '{task}.ma'.format(task=task)
	pub_file_path = os.path.join(folder_path, file_name)

	if os.path.exists(pub_file_path):
		mc.file(file_name, r=True, ns=task)

def overwrite_last(task=''):
	'''
	Overwrite the last version of given task file.
	'''
	if not save_warning():
		return False

	folder_path, file_name, file_ext = file_info()

	ver = get_last_ver(folder_path, task)

	ow_file_name = '{task}_v{ver}'.format(task=task, ver=ver)
	ow_file_path = os.path.join(folder_path, ow_file_name)

	result = mc.confirmDialog(title='Confirm Overwrite', 
								message='Do you want to replace {file_name}?'.format(file_name=ow_file_name), 
								button=['Yes', 'No'], 
								defaultButton='No', cancelButton='No', 
								dismissString='No' )

	if result == 'Yes':
		mc.file(rn=ow_file_path)
		mc.file(s=True, f=True, type='mayaAscii')
		return True

	elif result == 'No':
		return False

owl = overwrite_last
