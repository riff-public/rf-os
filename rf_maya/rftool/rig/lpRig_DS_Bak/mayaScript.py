# sets -rm sculpt1Set orig1.vtx[145] ;
# select -d orig1.vtx[145] ;
setNm = 'sculpt1Set'
sels = mc.ls(sl=True, fl=True)
for sel in sels:
	mc.sets(sel, rm=setNm)

# Mirror selected shapes
import maya.cmds as mc
import maya.mel as mm

orig = 'orig'

for sel in mc.ls(sl=True):
	
	rgt = sel.replace('_L_', '_R_')

	if mc.objExists(orig):
		mc.select(orig, r=True)
		mc.select(rgt, add=True)
		mm.eval('copyShape;')
	
	mc.select(sel, r=True)
	mc.select(rgt, add=True)
		
	mm.eval('mirrorShape2 YZ 0 0 1 0.001;')

# Reset shape
orig = 'ebOrig'

for sel in mc.ls(sl=True):
	sel = mc.ls(sl=True)
	
	mc.select(orig, r=True)
	mc.select(sel, add=True)
	
	cmd = 'copyShape;'
	mm.eval(cmd)

# Mirror name
sels = mc.ls(sl=True)
mc.rename(sels[1], sels[0].replace('_L_', '_R_'))

# Parent constrain and hide
sels = mc.ls(sl=True)

mc.parentConstraint(sels[1], sels[0], mo=True)
mc.scaleConstraint(sels[1], sels[0], mo=True)

mc.setAttr('{}.v'.format(sels[0]), 0)

# Run main rig
import os
import sys

sn = os.path.normpath(mc.file(q=True, l=True)[0])
rigFldPath, flNmExt = os.path.split(sn)
assetFldPath, rigFld = os.path.split(rigFldPath)
typeFldPath, assetNm = os.path.split(assetFldPath)
rigDataFldPath = os.path.join(rigFldPath, 'rigData')

sys.path.append(rigDataFldPath)

mdlNm = '{}MainRig'.format(assetNm)

exec('import {}'.format(mdlNm))
exec('reload({})'.format(mdlNm))
exec('{}.main()'.format(mdlNm))

# Delete children constrain nodes
mc.delete(mc.listRelatives(mc.ls(sl=True)[0], ad=True, type='constraint'))

# Store constraint targets to dict
sels = mc.ls(sl=True)
geoToConTarDict = {}

for sel in sels:
	cons = mc.listConnections(sel, d=True, s=False, type='parentConstraint')
	jnts = mc.listConnections(cons[0], d=False, s=True, type='joint')
	
	geoToConTarDict[sel] = jnts[0]

# Constrain back to target
for key in geoToConTarDict.keys():
	
	tar = geoToConTarDict[key]
	
	mc.parentConstraint(tar, key, mo=True)
	mc.scaleConstraint(tar, key, mo=True)

# Constraint zerogroup to the second selected object.
# Then connect xform from controller to the second selected object.
sels = mc.ls(sl=True)
mc.delete(mc.parentConstraint(sels[0], sels[1]))

for attr in ('t', 'r', 's'):
	
	ctrl = mc.listRelatives(sels[1], type='transform')[0]
	
	mc.connectAttr(
						'{0}.{1}'.format(ctrl, attr),
						'{0}.{1}'.format(sels[0], attr)
					)

# Mirror locs
for sel in mc.ls(sl=True):
	dup = mc.duplicate(sel, rr=True)[0]
	for attr in ('tx', 'ry'):
		mc.setAttr('%s.%s' % (dup, attr), -mc.getAttr('%s.%s' % (sel, attr)))
	
	mc.rename(dup, sel.replace('_L_', '_R_'))

# Remove pasted
for each in mc.ls():
	if 'pasted__' in each:
		try:
			mc.rename(each, each.replace('pasted__', ''))
		except:
			pass

# Transfer blend shapes
bsn = 'MthRigBuf_Bsn_old'

pfxs = ['HeadMthRig_Geo']

bshs = mc.aliasAttr(bsn , q=True)

for ix in range(0, len(bshs), 2):
	
	bshAttr = '%s.%s' % (bsn, bshs[ix])
	
	if mc.getAttr(bshAttr, l=True): continue
	
	mc.setAttr(bshAttr, 1)
	
	for pfx in pfxs:
		
		src = '%s_new' % pfx
		tar = '%s_%s' % (pfx, bshs[ix])

		if not mc.objExists(tar):
			continue
		
		mc.select(src, r=True)
		mc.select(tar, add=True)
		
		cmd = 'copyShape;'
		mm.eval(cmd)
	
	mc.setAttr(bshAttr, 0)

# Get adjacent vertices
cnt = 'locator1'
selVtx = 'HeadJawRig_Geo.vtx[767]'

cntPos = mc.xform(cnt, q=True, t=True, ws=True)
vtxPos = mc.pointPosition(selVtx)
mainVec = lrc.diff(vtxPos, cntPos)

# Get adjacent vtcs and their vector from center object
adjVtcs = []
adjVtxVecs = []

fcs = mc.polyListComponentConversion(selVtx, tf=True)
for fc in fcs:
	for vtx in mc.ls(mc.polyListComponentConversion(fc, tv=True), fl=True):
				
		if vtx == selVtx:
			continue
		
		if not vtx in adjVtcs:
			adjVtcs.append(vtx)
			currVec = lrc.diff(mc.pointPosition(vtx), cntPos)
			angle = lrc.angle(mainVec, currVec)
			print vtx, angle

# Copy component selections.
sels = mc.ls(sl=True, fl=True)
tarGeo = sels[-1]

tarCmps = []

for sel in sels[:-1]:
	
	tarCmps.append('%s.%s' % (tarGeo, sel.split('.')[-1]))

mc.select(tarCmps, r=True)

# go to rest pose of the skinned obj
# select skinned obj, blend shape obj, jnt, target obj
import maya.cmds as mc
from lpRig import rigTools as lrr

sels = mc.ls(sl=True)

lrr.applyVertexMoveWithSkinWeight(
									baseObj=sels[0],
									srcObj=sels[1],
									tarObj=sels[3],
									skinGeo=sels[0],
									jnt=sels[2]
									)

# Remove selected vtcs from set.
setNm = 'sculpt1Set'

for sel in mc.ls(sl=True, fl=True):
	
	mc.sets(sel, rm=setNm)

# Reset shapes
origs = ['bodyOrig', 'ebOrig']
vtxNoToOrig = {}

for orig in origs:
	
	vtxNoToOrig[mc.polyEvaluate(orig, v=True)] = orig

for sel in mc.ls(sl=True):
	
	vtxNo = mc.polyEvaluate(sel, v=True)
	currOrig = vtxNoToOrig[vtxNo]
	
	mc.select(currOrig, r=True)
	mc.select(sel, add=True)
	
	cmd = 'copyShape;'
	mm.eval(cmd)

# go to rest pose of the skinned obj
# select skinned obj, blend shape obj, jnt, target obj
import maya.cmds as mc
from lpRig import rigTools as lrr

sels = mc.ls(sl=True)

lrr.applyVertexMoveWithSkinWeight(
									baseObj=sels[0],
									srcObj=sels[1],
									tarObj=sels[3],
									skinGeo=sels[0],
									jnt=sels[2]
									)

# Copy Eb rot shapes
startFrame = 115

shps = ['_RotDnInEbRig_L_Bsh', '_RotDnMidEbRig_L_Bsh' ,'_RotDnOutEbRig_L_Bsh',
		'_RotUpInEbRig_L_Bsh', '_RotUpMidEbRig_L_Bsh' ,'_RotUpOutEbRig_L_Bsh']

srcs = ['bodyRotSkin', 'ebRotSkin']
geos = ['BodyEbRig_Geo', 'EyebrowEbRig_Geo']

for idx, shp in enumerate(shps):
	
	mc.currentTime(startFrame + (idx*10))
	
	for geoIdx in range(len(geos)):
		
		currSrc = srcs[geoIdx]
		currGeo = '%s%s' % (geos[geoIdx], shp)
		
		mc.select(currSrc, r=True)
		mc.select(currGeo, add=True)
		
		cmd = 'copyShape;'
		mm.eval(cmd)
		
		mc.select(currGeo, r=True)
		mc.select(currGeo.replace('_L_', '_R_', add=True))
		
		cmd = 'mirrorShape2 "YZ" 0 0 1 0.001;'
		mm.eval(cmd)

# Auto divider
from lpRig import weightDivider
reload(weightDivider)
import time

startTime = time.time()
weightDivider.autoDivideCharacter(['BodyProxy_Geo'], 'Susan_Ctrl:')
print time.time() - startTime

# Copy component selection to the last selected object.
sels = mc.ls(sl=True)
tar = sels[-1]

newSels = []

for sel in sels[:-1]:
	
	currObj, currComp = sel.split('.')
	newSel = '%s.%s' % (tar, currComp)
	
	newSels.append(newSel)

mc.select(newSels, r=True)

# Find keyword in python files.
fld_path = r'X:\Lani Pixels\Libraries\Maya\mayaLpPython\lpRig'

py_files = [x for x in os.listdir(fld_path) if x.endswith('.py')]

keyword = 'enableRenderStatToSelected'

for py_file in py_files:
	
	curr_py_path = os.path.join(fld_path, py_file)
	
	with open(curr_py_path, 'r') as file_obj:
		
		for line in file_obj:
			if keyword in line:
				print '{keyword} has been found in {py_file}'.format(keyword=keyword, 
																		py_file=py_file)

print 'Checking keyword is done'

# Get left side shape from symetrical corrected shape.
from lpRig import applyLeftShape

orig = 'orig'
skinnedShpObj = 'Body_Geo'
per = 0.15

sculptedObj = mc.ls(sl=True)[0]

mc.select(sculptedObj, r=True)
mc.select(skinnedShpObj, add=True)

mm.eval('BSpiritCorrectiveShape')

correctedObj = mc.ls(sl=True)[0]

outObj = mc.duplicate(orig, rr=True)[0]

mc.select(correctedObj, r=True)
mc.select(outObj, add=True)

applyLeftShape.main(per, 1)

mc.delete(correctedObj)

mc.select(outObj, r=True)
cmds.showHidden(outObj)

# Get 40
orig = 'orig'
nums = ['90', '45']
per = 0.3

srcObj = mc.ls(sl=True)[0]
tarNm = srcObj.replace(nums[0], nums[1])

dupObj = mc.duplicate(srcObj, rr=True)[0]

mc.rename(dupObj, tarNm)

mc.select(orig, r=True)
mc.select(tarNm, add=True)

lrr.doAddBlendShape(1)

mc.delete(tarNm, ch=True)

mc.select(srcObj, r=True)
mc.select(tarNm, add=True)

lrr.doAddBlendShape(per)

mc.delete(tarNm, ch=True)

# Create point rig to selected joints.
from lpRig import pointRig

_parent = 'Root_Jnt'

sels = mc.ls(sl=True)
ctrlShp = 'cube'

for sel in sels:
	
	ndNm, ndIdx, ndSide, ndType = lrr.extractName(sel)
	rigObj = pointRig.PointRig(_parent, 'Ctrl_Grp', sel, ndNm, ndSide.replace('_', ''), ctrlShp)

# Duplicate right shapes
orig = 'orig'

for sel in mc.ls(sl=True):
	
	rgt = sel.replace('_L_', '_R_')
	
	currObj = mc.duplicate(sel, rr=True)[0]
	mc.rename(currObj, rgt)
	
	mc.select(orig, r=True)
	mc.select(rgt, add=True)
	
	cmd = 'copyShape;'
	mm.eval(cmd)
	
	mc.select(sel, r=True)
	mc.select(rgt, add=True)
	
	cmd = 'mirrorShape2 "YZ" 0 0 1 0.001;'
	mm.eval(cmd)

# Check geo
from publishCmds import CharacterRig
print CharacterRig.checkVertexCount()

# transfer UVs
sel = mc.ls(sl=True)[0]
srch = '_old'
rep = ''

#tar = sel.replace(srch, rep)

tar = mc.ls(sl=True)[1]

mc.select(tar, add=True)

cmd = 'transferAttributes -transferPositions 0 -transferNormals 0 -transferUVs 2 -transferColors 0 -sampleSpace 0 -sourceUvSpace "map1" -targetUvSpace "map1" -searchMethod 3-flipUVs 0 -colorBorders 1 ;'

mm.eval(cmd)

lrr.dupShade()

# mc.select(tar, r=True)

# smooth skin selected vtcs
import glTools.tools.smoothWeights as smoothWeights

sels = mc.ls(sl=True)
mm.eval('PolySelectConvert 3;')
expandVtcs = mc.ls(sl=True)
smoothWeights.smoothWeights(vtxList=expandVtcs, faceConnectivity=False, 
											showProgress=False, debug=False)

mc.select(sels, r=True)

# copy color
sels = mc.ls(sl=True)

col = mc.getAttr('%s.color' % sels[0])[0]

mc.setAttr('%s.color' % sels[1], col[0], col[1], col[2], type='double3')


# copy partial shape
# sels = mc.ls(sl=True)
# vtcs = mc.ls(sl=True)

for sel in sels:
    
    mc.select(vtcs, r=True)
    mc.select(sel, add=True)
    
    mm.eval('copyShape')

# Transfer uv
ns = 'Patrick_Shading:'

for sel in mc.ls(sl=True):
    
    src = '%s%s' % (ns, sel)
    
    mc.select(src, r=True)
    mc.select(sel, add=True)
    
    lrr.transferUvToOrig()

# Pull weight
from lpRig import weightPuller

# idx = 1
start = 7
max = 24

currIdx = start + idx
if currIdx > max:
    currIdx -= max
weightPuller.pull(srch='_%s_' % str(start), rep='_%s_' % str(currIdx), 
                    mult=1, opr='mult')
idx += 1

# Using skin_tools to adjust skin weight values on ribbon joints.
from lpRig import skin_tools
reload(skin_tools)

# Select geo, first inf and second inf.
sels = mc.ls(sl=True)
skin_tools.unlock_selected_joints_from_selected_geos()
skin_tools.select_affected_vertices(sels[0], sels[1])

# Print out help text.
mdlNm = 'pointRig'

exec('from lpRig import %s' % mdlNm)
exec('print help(%s)' % mdlNm)