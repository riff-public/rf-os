import os
from functools import partial

import maya.cmds as mc
import maya.mel as mm
import re

import core as lrc
reload(lrc)
import rigTools as lrr
reload(lrr)
import detailControl
reload(detailControl)
import blendShapeAdder
reload(blendShapeAdder)

CTRL_GRP = 'FclRigCtrl_Grp'
JNT_GRP = 'FclRigJnt_Grp'
SKIN_GRP = 'FclRigSkin_Grp'
STILL_GRP = 'FclRigStill_Grp'
RVT_GRP = 'FclRigRvt_Grp'

HD_LOW_GRP = 'HdLowFclRigSpc_Grp'
HD_MID_GRP = 'HdMidFclRigSpc_Grp'
HD_HI_GRP = 'HdHiFclRigSpc_Grp'

def refElem(elem=''):

	scnLoc = os.path.normpath(mc.file(q=True, l=True)[0])

	fldPath, flNmExt = os.path.split(scnLoc)
	assetPath, taskFld = os.path.split(fldPath)
	catPath, assetNm = os.path.split(assetPath)

	elemPath = os.path.join(
								fldPath,
								'%s_%s.ma' % (assetNm, elem)
							)

	mc.file(elemPath, r=True, ns=elem)

def fclRigAssembler():
	# FclRigAssembler call back
	ui = FclRigAssembler()
	ui.show()

	return ui

class FclRigAssembler(object):
	'''
	UI class for assemFclRig function.
	'''
	def __init__(self):

		self.ui = 'pkFclRigAssembler'
		self.win = '%sWin' % self.ui

	def show(self):

		if mc.window(self.win, exists=True):
			mc.deleteUI(self.win)

		mc.window(self.win, t='pkFclRigAssembler', rtf=True)
		self.mainCl = mc.columnLayout(adj=True)
		self.mainRl = mc.rowLayout(adj=True, nc=4)

		# Part Src Objs
		self.partSrcLister = blendShapeAdder.BlendShapeLister('partSrc')
		mc.setParent(self.mainRl)

		# Part Objs
		self.partObjLister = blendShapeAdder.BlendShapeLister('partObj')
		mc.setParent(self.mainRl)

		# Bsh Src Objs
		self.bshSrcLister = blendShapeAdder.BlendShapeLister('bshSrc')
		mc.setParent(self.mainRl)

		# Bsh Objs
		self.bshObjLister = blendShapeAdder.BlendShapeLister('bshObj')
		mc.setParent(self.mainRl)

		mc.setParent(self.mainCl)
		self.assemButton = mc.button(l='Assemble', c=partial(self.assem), h=35)

		mc.showWindow(self.win)
		mc.window(self.win, e=True, w=250)
		mc.window(self.win, e=True, h=250)

	def getObjs(self, *args):
		mc.textScrollList(args[0], e=True, ra=True)
		for sel in mc.ls(sl=True):
			if not mc.listRelatives(sel, s=True): continue
			mc.textScrollList(args[0], e=True, a=sel)

	def assem(self, *args):

		partSrcObjs = mc.textScrollList(self.partSrcLister.bshTsl, q=True, ai=True)
		partObjs = mc.textScrollList(self.partObjLister.bshTsl, q=True, ai=True)
		bshSrcObjs = mc.textScrollList(self.bshSrcLister.bshTsl, q=True, ai=True)
		bshObjs = mc.textScrollList(self.bshObjLister.bshTsl, q=True, ai=True)

		assemFclRig(partObjs, partSrcObjs, bshObjs, bshSrcObjs)

def assemFclRig(
					wrappedGeos=[
									'Head_Geo'
								],
					wrapGeos=[
								'ModHero_FclRig:HeadFclRig_Geo'
							],
					rigBshGeos=[
									'Tongue_Geo',
									'UpperGum_Geo',
									'LowerGum_Geo',
									'UpperTeeth_Geo',
									'LowerTeeth_Geo',
									'Hair_Geo',
									'Eyebrow_Geo',
									'EyeGeo_L_Grp',
									'EyeGeo_R_Grp'
								],
					fclBshGeos=[
									'ModHero_FclRig:TongueFclRig_Geo',
									'ModHero_FclRig:UpperGumFclRig_Geo',
									'ModHero_FclRig:LowerGumFclRig_Geo',
									'ModHero_FclRig:UpperTeethFclRig_Geo',
									'ModHero_FclRig:LowerTeethFclRig_Geo',
									'ModHero_FclRig:HairFclRig_Geo',
									'ModHero_FclRig:EyebrowFclRig_Geo',
									'ModHero_FclRig:EyeGeoFclRig_L_Grp',
									'ModHero_FclRig:EyeGeoFclRig_R_Grp'
								]
				):
	
	# BodyRig elms
	hdRigJnt = lrc.Dag(mc.ls('*:Head_Jnt')[0])
	ctrlRigGrp = lrc.Dag(mc.ls('*:Ctrl_Grp')[0])
	stillRigGrp = lrc.Dag(mc.ls('*:Still_Grp')[0])

	# FclRig elms
	ctrlFclRigGrp = lrc.Dag(mc.ls('*:FclRigCtrl_Grp')[0])
	jntFclRigGrp = lrc.Dag(mc.ls('*:FclRigJnt_Grp')[0])
	skinFclRigGrp = lrc.Dag(mc.ls('*:FclRigSkin_Grp')[0])
	stillFclRigGrp = lrc.Dag(mc.ls('*:FclRigStill_Grp')[0])

	# Par groups
	fclRigStillPar = lrc.Null()
	fclRigStillPar.name = 'FclRigStillPar_Grp'
	fclRigStillPar.parent(stillRigGrp)

	fclRigCtrlPar = lrc.Null()
	fclRigCtrlPar.name = 'FclRigCtrlPar_Grp'
	fclRigCtrlPar.parent(ctrlRigGrp)
	mc.parentConstraint(hdRigJnt, fclRigCtrlPar, mo=True)
	mc.scaleConstraint(hdRigJnt, fclRigCtrlPar, mo=True)

	# Parenting fclRig elms
	ctrlFclRigGrp.parent(fclRigCtrlPar)
	jntFclRigGrp.parent(fclRigStillPar)
	skinFclRigGrp.parent(fclRigStillPar)
	stillFclRigGrp.parent(fclRigStillPar)

	# Wrap geos
	if wrappedGeos:
		for ix, wrappedGeo in enumerate(wrappedGeos):

			wrapGeo = wrapGeos[ix]

			# Get wrapped geo
			duppedGeo = lrc.Dag(mc.duplicate(wrappedGeo, rr=True)[0])
			duppedGeo.name = lrr.addPartName(wrappedGeo, 'FclRigWrapped')
			mc.delete(duppedGeo, ch=True)
			duppedGeo.attr('v').v = 0
			duppedGeo.parent(stillRigGrp)
			
			# Create wrap
			mc.select(duppedGeo, r=True)
			mc.select(wrapGeo, add=True)

			wrap = mm.eval('doWrapArgList "7"{"1","0","0.001","2","1","0","0"};')[0]

			# Parent base geo
			base = mc.listConnections('%s.basePoints[0]' % wrap, s=True, d=False)[0]
			mc.parent(base, stillRigGrp)

			# Adding blend shape
			mc.select(duppedGeo, r=True)
			mc.select(wrappedGeo, add=True)
			lrr.doAddBlendShape()

	# Bsh geos
	if rigBshGeos:
		for ix, rigBshGeo in enumerate(rigBshGeos):

			fclBshGeo = fclBshGeos[ix]

			mc.select(fclBshGeo, r=True)
			mc.select(rigBshGeo, add=True)
			lrr.doAddBlendShape()

	connectEyeRig()

def connectEyeRig():
	'''
	Connect eyeRig to bodyRig
	'''
	mainRigNs = ''
	eyeRigNs = ''
	
	# Get namespace
	for each in mc.ls():

		if ('Eye_' in each) and each.endswith('_Jnt'):
			mainRigNs = ':'.join(each.split(':')[:-1])

		if ('CntEyeRig_' in each) and each.endswith('_Ctrl'):
			eyeRigNs = ':'.join(each.split(':')[:-1])
	
	for side in ('_L_', '_R_'):
		
		eyeJnt = lrc.Dag('%s:Eye%sJnt' % (mainRigNs, side))
		# eyeRigCtrlZr = lrc.Dag('%s:CntEyeRigCtrlZr%sGrp' % (eyeRigNs, side))
		eyeRigCtrlOri = lrc.Dag('%s:CntEyeRigCtrlOri%sGrp' % (eyeRigNs, side))
		
		# eyeJnt.attr('r') >> eyeRigCtrlZr.attr('r')
		eyeJnt.attr('r') >> eyeRigCtrlOri.attr('r')
		
		eyeRigCtrl = lrc.Dag('%s:CntEyeRig%sCtrl' % (eyeRigNs, side))
		cntLidZr = lrc.Dag('%s:PartCntLidRigJntZr%sGrp' % (eyeRigNs, side))
		
		followAmper = lrc.MultiplyDivide()
		followAmper.name = 'LidFlw%sMdv' % side

		eyeJnt.attr('r') >> followAmper.attr('i1')
		eyeRigCtrl.attr('lidFollow') >> followAmper.attr('i2x')
		eyeRigCtrl.attr('lidFollow') >> followAmper.attr('i2y')
		eyeRigCtrl.attr('lidFollow') >> followAmper.attr('i2z')
		followAmper.attr('o') >> cntLidZr.attr('r')

		eyeCtrlZr = lrc.Dag('%s:EyeCtrlZr%sGrp' % (mainRigNs, side))
		eyeCtrlZr.attr('v').v = 0

def connectSelectedToHeadDeform():
	'''
	Connect each selected geometries to the same geometries in HeadRig module.
	'''
	for sel in mc.ls(sl=True):
		
		currNmElms = re.search(r'(.+:)([a-zA-Z0-9]+)(.+)', sel)
		objMidNmElms = re.findall('[A-Z][^A-Z]*', currNmElms.group(2))
		objMidNmElms[-2] = 'Hd'
		hdRigMidNm = ''.join(objMidNmElms)

		hdRigNm = '{midNm}{suffix}'.format(midNm=hdRigMidNm, suffix=currNmElms.group(3))
		hdRigGeo = mc.ls('*:%s' % hdRigNm)[0]

		if not mc.objExists(hdRigGeo): print '%s has not been found.' % hdRigGeo; return False

		mc.select(sel, r=True)
		mc.select(hdRigGeo, add=True)

		print 'Adding blend shape from {src} to {tar}'.format(src=sel, tar=hdRigGeo)

		lrr.doAddBlendShape()

	return True

def createMainGroup():
	'''
	Create the main groups for FclRig module.
	'''
	ctrlGrp = lrc.Null()
	ctrlGrp.name = CTRL_GRP

	jntGrp = lrc.Null()
	jntGrp.name = JNT_GRP

	skinGrp = lrc.Null()
	skinGrp.name = SKIN_GRP

	stillGrp = lrc.Null()
	stillGrp.name = STILL_GRP

	rvtGrp = lrc.Null()
	rvtGrp.name = RVT_GRP
	rvtGrp.parent(ctrlGrp)

# HdRig
def createHdRigSpaceGroup():
	'''
	Create head deform space groups in FclRig module.
	'''
	for part in ('Low', 'Mid', 'Hi'):

		currSpcGrp = lrc.Null()
		currSpcGrp.name = 'Hd{}FclRigSpc_Grp'.format(part)

		currSpcZrGrp = lrc.group(currSpcGrp)
		currSpcZrGrp.name = 'Hd{}FclRigSpcZr_Grp'.format(part)

		curSpcSrc = lrc.Dag(mc.ls('*:Hd{}HdDfmRigSpc_Grp'.format(part))[0])
		curSpcZrSrc = lrc.Dag(mc.ls('*:Hd{}HdDfmRigSpcZr_Grp'.format(part))[0])

		currSpcZrGrp.snap(curSpcZrSrc)

		for attr in ('t', 'r', 's'):
			curSpcSrc.attr(attr) >> currSpcGrp.attr(attr)

		currSpcZrGrp.parent(CTRL_GRP)

def parentHdRig():

	ctrl, jnt, skin, still = findMainGroup('HdDfmRig')
	mc.parent(ctrl, CTRL_GRP)
	mc.parent(jnt, JNT_GRP)
	mc.parent(skin, SKIN_GRP)
	mc.parent(still, STILL_GRP)

# EyeRig
def parentEyeRig():

	ctrl, jnt, skin, still = findMainGroup('EyeRig')
	mc.parent(ctrl, HD_HI_GRP)
	mc.parent(jnt, JNT_GRP)
	mc.parent(skin, SKIN_GRP)
	mc.parent(still, STILL_GRP)

def stickEyeCtrl(postGeo='BodyFclRigPost_Geo'):

	eyeCtrls = ['LidUpEyeRig_L_Ctrl', 'LidLowEyeRig_L_Ctrl', 'CnrInLidRig_L_Ctrl', 'CnrInUpLidRig_L_Ctrl',
				'CnrInDnLidRig_L_Ctrl', 'CnrOutLidRig_L_Ctrl', 'CnrOutUpLidRig_L_Ctrl', 'CnrOutDnLidRig_L_Ctrl',
				'UpInLidRig_L_Ctrl', 'UpMidLidRig_L_Ctrl', 'UpOutLidRig_L_Ctrl', 'LowOutLidRig_L_Ctrl',
				'LowMidLidRig_L_Ctrl', 'LowInLidRig_L_Ctrl', 'CnrInSktLidRig_L_Ctrl', 'CnrOutSktLidRig_L_Ctrl',
				'UpInSktLidRig_L_Ctrl', 'UpMidSktLidRig_L_Ctrl', 'UpOutSktLidRig_L_Ctrl', 'LowOutSktLidRig_L_Ctrl',
				'LowMidSktLidRig_L_Ctrl', 'LowInSktLidRig_L_Ctrl', 'LidUpEyeRig_R_Ctrl', 'LidLowEyeRig_R_Ctrl',
				'CnrInLidRig_R_Ctrl', 'CnrInUpLidRig_R_Ctrl', 'CnrInDnLidRig_R_Ctrl', 'CnrOutLidRig_R_Ctrl',
				'CnrOutUpLidRig_R_Ctrl', 'CnrOutDnLidRig_R_Ctrl', 'UpInLidRig_R_Ctrl', 'UpMidLidRig_R_Ctrl',
				'UpOutLidRig_R_Ctrl', 'LowOutLidRig_R_Ctrl', 'LowMidLidRig_R_Ctrl', 'LowInLidRig_R_Ctrl',
				'CnrInSktLidRig_R_Ctrl', 'CnrOutSktLidRig_R_Ctrl', 'UpInSktLidRig_R_Ctrl', 'UpMidSktLidRig_R_Ctrl',
				'UpOutSktLidRig_R_Ctrl', 'LowOutSktLidRig_R_Ctrl', 'LowMidSktLidRig_R_Ctrl', 'LowInSktLidRig_R_Ctrl']
	
	mc.select(cl=True)
	for eyeCtrl in eyeCtrls:
		currCtrls = mc.ls('*:{ctrl}'.format(ctrl=eyeCtrl))
		if not currCtrls:
			continue
		mc.select(currCtrls[0], add=True)
	
	mc.select(postGeo, add=True)
	rvts = detailControl.attachSelectedControlsToSelectedMesh()
	mc.parent(rvts, RVT_GRP)

# MthRig
def parentMthRig():
	
	ctrl, jnt, skin, still = findMainGroup('MthRig')
	mc.parent(ctrl, HD_LOW_GRP)
	mc.parent(jnt, JNT_GRP)
	mc.parent(skin, SKIN_GRP)
	mc.parent(still, STILL_GRP)

def stickMthCtrl(postGeo='BodyFclRigPost_Geo'):

	mthCtrls = ['LipUpMthMthRig_Ctrl', 'LipLowMthMthRig_Ctrl', 'MthMthRig_L_Ctrl', 'MthMthRig_R_Ctrl',
				'LowMidLipRig_Ctrl', 'LowALipRig_L_Ctrl', 'LowBLipRig_L_Ctrl', 'LowALipRig_R_Ctrl',
				'LowBLipRig_R_Ctrl', 'LipRigDtl_11_Ctrl', 'LipRigDtl_12_Ctrl', 'LipRigDtl_13_Ctrl',
				'LipRigDtl_14_Ctrl', 'LipRigDtl_15_Ctrl', 'LipRigDtl_16_Ctrl', 'LipRigDtl_17_Ctrl',
				'LipRigDtl_18_Ctrl', 'LipRigDtl_19_Ctrl', 'LipRigDtl_20_Ctrl', 'LipRigDtl_21_Ctrl',
				'LipRigDtl_22_Ctrl', 'LipRigDtl_23_Ctrl', 'UpMidLipRig_Ctrl', 'CnrLipRig_L_Ctrl',
				'CnrLipRig_R_Ctrl', 'UpALipRig_L_Ctrl', 'UpBLipRig_L_Ctrl', 'UpALipRig_R_Ctrl',
				'UpBLipRig_R_Ctrl', 'LipRigDtl_1_Ctrl', 'LipRigDtl_2_Ctrl', 'LipRigDtl_3_Ctrl',
				'LipRigDtl_4_Ctrl', 'LipRigDtl_5_Ctrl', 'LipRigDtl_6_Ctrl', 'LipRigDtl_7_Ctrl',
				'LipRigDtl_8_Ctrl', 'LipRigDtl_9_Ctrl', 'LipRigDtl_10_Ctrl', 'LipRigDtl_24_Ctrl']
	mc.select(cl=True)
	for mthCtrl in mthCtrls:
		currCtrls = mc.ls('*:{ctrl}'.format(ctrl=mthCtrl))
		if not currCtrls:
			continue
		mc.select(currCtrls[0], add=True)

	mc.select(postGeo, add=True)
	rvts = detailControl.attachSelectedControlsToSelectedMesh()
	mc.parent(rvts, RVT_GRP)

# TtRig
def parentTtRig():

	ctrl, jnt, skin, still = findMainGroup('TtRig')
	mc.parent(ctrl, HD_LOW_GRP)
	mc.parent(jnt, JNT_GRP)
	mc.parent(skin, SKIN_GRP)
	mc.parent(still, STILL_GRP)

	jawPiv = None
	jawJnt = None
	for each in mc.ls():
		if each.endswith(':TtRigCtrlJawPost_Grp'):
			jawPiv = each
		elif each.endswith(':JawMthRig_Jnt'):
			jawJnt = each

	for attr in ('t', 'r', 's'):
		mc.connectAttr('{jawJnt}.{attr}'.format(jawJnt=jawJnt, attr=attr), 
						'{jawPiv}.{attr}'.format(jawPiv=jawPiv, attr=attr))

# EbRig
def parentEbRig():

	ctrl, jnt, skin, still = findMainGroup('EbRig')
	mc.parent(ctrl, HD_HI_GRP)
	mc.parent(still, STILL_GRP)

def stickEbCtrl(postGeo='BodyFclRigPost_Geo'):

	ctrls = ['AllEbRig_L_Ctrl', 'InEbRig_L_Ctrl', 'MidEbRig_L_Ctrl', 'OutEbRig_L_Ctrl',
				'AllEbRig_R_Ctrl', 'InEbRig_R_Ctrl', 'MidEbRig_R_Ctrl', 'OutEbRig_R_Ctrl',
				'EbRbn_1_L_Ctrl', 'EbRbn_2_L_Ctrl', 'EbRbn_3_L_Ctrl', 'EbRbn_4_L_Ctrl', 
				'EbRbn_1_R_Ctrl', 'EbRbn_2_R_Ctrl', 'EbRbn_3_R_Ctrl', 'EbRbn_4_R_Ctrl']
	mc.select(cl=True)
	for ctrl in ctrls:
		currCtrls = mc.ls('*:{ctrl}'.format(ctrl=ctrl))
		if not currCtrls:
			continue
		mc.select(currCtrls[0], add=True)

	mc.select(postGeo, add=True)
	rvts = detailControl.attachSelectedControlsToSelectedMesh()
	mc.parent(rvts, RVT_GRP)

# NsRig
def parentNsRig():
	
	ctrl, jnt, skin, still = findMainGroup('NsRig')
	mc.parent(ctrl, HD_MID_GRP)
	mc.parent(skin, SKIN_GRP)
	mc.parent(still, STILL_GRP)

# EarRig
def parentEarRig():
	
	ctrl, jnt, skin, still = findMainGroup('EarRig')
	mc.parent(ctrl, HD_HI_GRP)
	mc.parent(skin, SKIN_GRP)
	mc.parent(still, STILL_GRP)

# DtlRig
def parentDtlRig():
	
	ctrl, jnt, skin, still = findMainGroup('DtlRig')
	mc.parent(skin, SKIN_GRP)
	mc.parent(still, STILL_GRP)

	mc.parent(mc.ls('*:DtlRigCtrlLow_Grp')[0], HD_LOW_GRP)
	mc.parent(mc.ls('*:DtlRigCtrlMid_Grp')[0], HD_MID_GRP)
	mc.parent(mc.ls('*:DtlRigCtrlHi_Grp')[0], HD_HI_GRP)

def stickDtlCtrl(postGeo='BodyFclRigPost_Geo'):

	ctrls =[
            'NsDtlRig_L_Ctrl', 'CheekADtlRig_L_Ctrl','CheekBDtlRig_L_Ctrl','CheekCDtlRig_L_Ctrl', 
            'NsDtlRig_R_Ctrl', 'CheekADtlRig_R_Ctrl','CheekBDtlRig_R_Ctrl', 'CheekCDtlRig_R_Ctrl', 
            'EyeSktADtlRig_L_Ctrl','EyeSktBDtlRig_L_Ctrl', 'EyeSktADtlRig_R_Ctrl', 'EyeSktBDtlRig_R_Ctrl',
            'PuffDtlRig_L_Ctrl', 'PuffDtlRig_R_Ctrl','NsDtlRig_Ctrl','EyeBrowDtlRig_Ctrl',
            'EyeBrowADtlRig_L_Ctrl','EyeBrowBDtlRig_L_Ctrl','EyeBrowCDtlRig_L_Ctrl','EyeBrowADtlRig_R_Ctrl',
            'EyeBrowBDtlRig_R_Ctrl','EyeBrowCDtlRig_R_Ctrl',]
	mc.select(cl=True)
	for ctrl in ctrls:
		currCtrls = mc.ls('*:{ctrl}'.format(ctrl=ctrl))
		if not currCtrls:
			continue
		mc.select(currCtrls[0], add=True)

	mc.select(postGeo, add=True)
	rvts = detailControl.attachSelectedControlsToSelectedMesh()
	mc.parent(rvts, RVT_GRP)  

def findMainGroup(part=''):
	'''
	Find the main groups of the given facial rig part.
	'''
	ctrl = None
	jnt = None
	skin = None
	still = None

	for each in mc.ls():
		if each.endswith(':{part}Ctrl_Grp'.format(part=part)):
			ctrl = each
		elif each.endswith(':{part}Jnt_Grp'.format(part=part)):
			jnt = each
		elif each.endswith(':{part}Skin_Grp'.format(part=part)):
			skin = each
		elif each.endswith(':{part}Still_Grp'.format(part=part)):
			still = each

	return ctrl, jnt, skin, still

'''
# Scripts for each part

# HdRig
from lpRig import headDeformRig
reload(headDeformRig)
headDeformRig.main()

# EyeRig
from lpRig import eyeRig
reload(eyeRig)

eyeRig.main(do25=True, do50=True, do75=True)
rigData.readCtrlShape(fn='eyeRigCtrlShape.dat')
rigData.writeCtrlShape(fn='eyeRigCtrlShape.dat')
eyeRig.doCreateBsh()

eyeRig.mainLite()
eyeRig.createLidBsh('LidRigGeo_Grp', 'Lid')

# EbRig
from lpRig import eyebrowRig
reload(eyebrowRig)
eyebrowRig.main()
rigData.readCtrlShape('ebRigCtrlShape.dat')
eyebrowRig.createBsh('EbRigBshBuf_Bsh', 'EbRig')

eyebrowRig.main(detail=False, lite=True)
eyebrowRig.createBsh('EbRigBshBuf_Bsh', 'EbRig', True)

eyebrowRig.doApplyVertexMove()

# MthRig
from lpRig import mouthRig
reload(mouthRig)

mouthRig.main(lipRig=True)
rigData.readCtrlShape(fn='mouthRigCtrlShape.dat')
mouthRig.createBsh('MthRigBshBuf_Bsh', 'MthRig')

mouthRig.mainLite()

mouthRig.LipPartRibbon(
                            tmpLocs=[
                                        'LowMidLipRigTmp_Loc',
                                        'CnrLipRigTmp_L_Loc',
                                        'CnrLipRigTmp_R_Loc',
                                        'LowALipRigTmp_L_Loc',
                                        'LowBLipRigTmp_L_Loc',
                                        'LowALipRigTmp_R_Loc',
                                        'LowBLipRigTmp_R_Loc'
                                    ],
                            part='TmpLip',
                            offsetCurve=0.1
                        )

from lpRig import applyLeftShape
applyLeftShape.main(0.1, 1)

# HdRig
from lpRig import headDeformRig
reload(headDeformRig)
headDeformRig.main()

# TtRig
from lpRig import ttRig
reload(ttRig)
ttRig.main()
rigData.readCtrlShape('ttRigCtrlShape.dat')

# NsRig
from lpRig import noseRig as nsRig
reload(nsRig)
nsRig.main()
rigData.readCtrlShape('nsRigCtrlShape.dat')

# EarRig
from lpRig import earRig
reload(earRig)
earRig.main()
rigData.readCtrlShape('earRigCtrlShape.dat')

# DtlRig
from lpRig import detailRig
detailRig.createDetailRigToSelected()

## Assembly Part ##

# FclRig
from lpRig import fclRig
reload(fclRig)

from lpRig import blendShapeAdder
reload(blendShapeAdder)
blendShapeAdder.run()

fclRig.refElem('FclRig')
fclRig.fclRigAssembler()

fclRig.createMainGroup()
fclRig.connectSelectedToHeadDeform()
fclRig.connectEyeRig()

# FclRig HdRig
fclRig.createHdRigSpaceGroup()
fclRig.parentHdRig()

# FclRig EyeRig
fclRig.refElem('EyeRig')
fclRig.parentEyeRig()
fclRig.stickEyeCtrl()

# FclRig EbRig
fclRig.refElem('EbRig')
fclRig.parentEbRig()
fclRig.stickEbCtrl()

# FclRig MthRig
fclRig.refElem('MthRig')
fclRig.parentMthRig()
fclRig.stickMthCtrl()

# FclRig TtRig
fclRig.refElem('TtRig')
fclRig.parentTtRig()

# FclRig NsRig
fclRig.parentNsRig()

# FclRig EarRig
fclRig.parentEarRig()

# FclRig DtlRig
fclRig.parentDtlRig()
fclRig.stickDtlCtrl()
'''