import maya.cmds as mc
import maya.mel as mm

from lpRig import rigTools as lrr
from lpRig import core as lrc

# Modifiers: shift = 1, ctrl = 4

def selectAll():
	
	'''
	Select all controllers from selected.
	'''
	
	mod = mc.getModifiers()
	
	ctrls = []
	
	sels = mc.ls(sl=True)
	
	ns = getNs(sels[0])
	ctrlGrp = '%sCtrl_Grp' % ns
	currCtrls = getAllControlInModule(ctrlGrp)

	plcCtrl = '%sPlace_Ctrl' % ns
	if mc.objExists(plcCtrl):
		currCtrls.append(plcCtrl)

	ofst = '%sOffset_Ctrl' % ns
	if mc.objExists(ofst):
		currCtrls.append(ofst)
	
	ctrls += currCtrls
	
	if mod == 1:
		mc.select(ctrls, add=True)
	elif mod == 4:
		mc.select(ctrls, deselect=True)
	else:
		mc.select(ctrls, r=True)

def getAllControlInModule(moduleName=''):

	'''
	Get all controllers in the given module
	'''
	
	ctrls = []

	for chd in mc.listRelatives(moduleName, ad=True, type='transform'):
		if chd.endswith('_Ctrl') and not chd.endswith('Zr_Ctrl'):
			cnncs = mc.listConnections(chd, s=True, d=False, type='constraint')
			if not cnncs:
				ctrls.append(chd)

	return ctrls

def getNs(nodeName=''):

	'''
	Get namespace from given name
	'''

	ns = ''

	if '|' in nodeName:
		nodeName = nodeName.split('|')[-1]

	if ':' in nodeName:

		nmElms = nodeName.split(':')

		if nmElms:
			ns = '%s:' % ':'.join(nmElms[0:-1])

	return ns

def selectAllControlInModule(modules=[]):

	'''
	Select all controller from given module(s)
	'''
	mod = mc.getModifiers()

	ns = getNs(mc.ls(sl=True)[0])
	ctrls = []

	for mdl in modules:
		mdlNm = '%s%s' % (ns, mdl)
		currCtrls = getAllControlInModule(mdlNm)
		ctrls += currCtrls

	if mod == 1:
		mc.select(ctrls, add=True)

	elif mod == 4:
		mc.select(ctrls, deselect=True)

	else:
		mc.select(ctrls, r=True)

def selectAllControlInModuleFromSelected():
	
	'''
	Select all controller in selected module
	'''

	mod = mc.getModifiers()

	ctrls = []

	for sel in mc.ls(sl=True, l=True):

		ns = getNs(sel)
		ctrlGrp = '|%sCtrl_Grp|' % ns
		mdlGrp = sel.split(ctrlGrp)[1].split('|')[1]
		mdlCtrls = getAllControlInModule(mdlGrp)
		
		ctrls += mdlCtrls
		
	if mod == 1:
		mc.select(ctrls, add=True)
	elif mod == 4:
		mc.select(ctrls, deselect=True)
	else:
		mc.select(ctrls, r=True)

def mergeDicts(dicts=[]):
	
	outputDict = {}
	
	for eachDict in dicts:
		for key in eachDict.keys():
			if not key in outputDict.keys():
				outputDict[key] = eachDict[key]
	
	return outputDict

def resetSelected():

	tfDict = {'tx': 0, 'ty': 0, 'tz': 0, 
				'rx': 0, 'ry': 0, 'rz': 0, 
				'sx': 1, 'sy': 1, 'sz': 1}

	bLegDict = {
					'twist': 0,
					'autoStretch': 0,
					'upLegStretch': 0,
					'midLegStretch': 0,
					'lowLegStretch': 0,
					'toeStretch': 0,
					'legFlex': 0,
					'legBend': 0,
					'heelRoll': 0,
					'ballRoll': 0,
					'toeRoll': 0,
					'ballRollMix': 0,
					'toeRollMix': 0,
					'heelTwist': 0,
					'toeTwist': 0,
					'footRock': 0,
					'toeBend': 0,
					'pinToe': 1
				}
	
	footDict = {
					'footScale': 1,
					'handScale': 1,
					'fist': 0,
					'scrunch': 0,
					'cup': 0,
					'spread': 0,
					'break': 0,
					'flex': 0,
					'baseSpread': 0,
					'baseBreak': 0,
					'baseFlex': 0
				}

	fingerDict = {
					'fold': 0,
					'stretch': 0,
					'squash': 0,
					'fngr1Fold': 0,
					'fngr1Spread': 0,
					'fngr1Twist': 0,
					'fngr2Fold': 0,
					'fngr2Spread': 0,
					'fngr2Twist': 0,
					'fngr3Fold': 0,
					'fngr3Spread': 0,
					'fngr3Twist': 0,
					'fngr4Fold': 0,
					'fngr4Spread': 0,
					'fngr4Twist': 0,
				}

	rbnDict = {'rootTwist': 0, 'endTwist': 0, 'squash': 0}
	
	fkDict = {'tipSquash': 0}
	
	# attrDict = mergeDicts([tfDict, bLegDict, footDict, fingerDict, rbnDict, fkDict])
	# sels = mc.ls(sl=True, l=True)
	# for sel in sels:
	# 	for attr in attrDict.keys():
	# 		if mc.objExists('%s.%s' % (sel, attr)):
	# 			nodeAttr = '%s.%s' % (sel, attr)
	# 			attrVal = attrDict[attr]
	# 			if not mc.getAttr(nodeAttr, l=True):
	# 				mc.setAttr(nodeAttr, attrVal)

	specialDict = {'footScale': 1, 'handScale': 1}
	sels = mc.ls(sl=True, l=True)
	for sel in sels:

		# tfAttrs
		for tfAttr in tfDict.keys():
			nodeAttr = '%s.%s' % (sel, tfAttr)
			if mc.objExists(nodeAttr):
				if not mc.getAttr(nodeAttr, l=True):
					mc.setAttr(nodeAttr, tfDict[tfAttr])
		
		# udAttrs
		udAttrs = mc.listAttr(sel, ud=True)

		if not udAttrs: continue

		for udAttr in udAttrs:
			nodeAttr = '%s.%s' % (sel, udAttr)
			
			if not mc.objExists(nodeAttr): continue

			if mc.getAttr(nodeAttr, l=True): continue
			if mc.getAttr(nodeAttr, cb=True): continue

			if not udAttr in specialDict.keys():
				mc.setAttr(nodeAttr, 0)
			else:
				mc.setAttr(nodeAttr, specialDict[udAttr])