import maya.cmds as mc

import lpRig.core as lrc
reload(lrc)
import lpRig.rigTools as lrr
reload(lrr)

class HipRig(object):

	def __init__(
					self,
					parent='',
					ctrlGrp='',
					ax='y',
					tmpJnt=[
								'Hip_L_Jnt',
								'HipTip_L_Jnt'
							],
					part='',
					side='L'
				):

		if side:
			side = '_%s_' % side
		else:
			side = '_'

		# Skin joints
		self.Hip_Jnt = lrc.Dag(tmpJnt[0])
		self.HipTip_Jnt = lrc.Dag(tmpJnt[1])

		self.HipSca_Jnt = lrr.jointAt(self.Hip_Jnt)
		self.HipSca_Jnt.name = "HipSca%s%sJnt" % (part, side)
		self.HipSca_Jnt.attr('ssc').v = 0
		self.HipSca_Jnt.parent(self.Hip_Jnt)

		# Main Group
		self.Ctrl_Grp = lrc.Null()
		self.Ctrl_Grp.name = "HipCtrl%s%sGrp" % (part, side)

		mc.parentConstraint(parent, self.Ctrl_Grp)

		# Controller
		self.Hip_Ctrl = lrr.jointControl('circle')
		self.Hip_Ctrl.name = "Hip%s%sCtrl" % (part, side)
		self.Hip_Ctrl.lockHideAttrs('v')
		self.Hip_Ctrl.rotateOrder = 'xyz'

		self.HipCtrlZr_Grp = lrc.group(self.Hip_Ctrl)
		self.HipCtrlZr_Grp.name = "HipCtrlZr%s%sGrp" % (part, side)

		# Controller - Color
		if side == '_R_':
			self.Hip_Ctrl.color = 'blue'
		else:
			self.Hip_Ctrl.color = 'red'

		# Controller - Positioning
		self.HipCtrlZr_Grp.snapPoint(self.Hip_Jnt)
		self.Hip_Ctrl.snapOrient(self.Hip_Jnt)
		self.Hip_Ctrl.freeze(r=True, t=False, s=False)
		self.HipCtrlZr_Grp.parent(self.Ctrl_Grp)
		
		self.HipGmbl_Ctrl = lrc.addGimbal(self.Hip_Ctrl)
		self.HipGmbl_Ctrl.name = "HipGmbl%s%sCtrl" % (part, side)
		self.HipGmbl_Ctrl.rotateOrder = 'xyz'
		
		# Stretch setup
		(self.HipStrt_Add,
		self.HipStrt_Mul) = lrr.fkStretch(
												ctrl=self.Hip_Ctrl,
												target=self.HipTip_Jnt,
												ax=ax
											)

		self.HipStrt_Add.name = "HipStrt%s%sAdd" % (part, side)
		self.HipStrt_Mul.name = "HipStrt%s%sMul" % (part, side)

		# Connect to joint
		mc.parentConstraint(self.HipGmbl_Ctrl, self.Hip_Jnt)
		mc.scaleConstraint(self.HipGmbl_Ctrl, self.HipSca_Jnt)

		# Grouping
		self.Ctrl_Grp.parent(ctrlGrp)