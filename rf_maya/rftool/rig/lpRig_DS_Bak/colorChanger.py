import maya.cmds as mc
import maya.mel as mm
from functools import partial

def run():

	'''
	Callback proc
	'''

	ui = ColorChanger()
	ui.show()

	return ui

class ColorChanger(object):

	def __init__(self):

		self.ui = 'ColorChanger'
		self.win = '%sWin' % self.ui

	def show(self):

		if mc.window(self.win, exists=True):
			mc.deleteUI(self.win)

		mc.window(self.win, t='Color Changer', rtf=True)

		self.mainCol = mc.columnLayout('%sMainCl' % self.ui, adj=True)

		# red, darkRed, brightRed=31
		# blue, darkBlue, brightBlue
		# green, darkGreen, brightGreen
		# white, brown, none
		nc = 3
		w = 180
		cw = (w / nc)
		self.redColButRl = mc.rowLayout(
											'%sRedColButRl' % self.ui,
											numberOfColumns=nc,
											columnWidth3=(cw, cw, cw)
										)
		mc.button(w=cw, l='R', bgc=[1,0,0], c=partial(self.assignColor,'red'))
		mc.button(w=cw, l='DR', bgc=[0.25,0,0], c=partial(self.assignColor,'darkRed'))
		mc.button(w=cw, l='SR', bgc=[0.75,0,0], c=partial(self.assignColor,'softRed'))

		mc.setParent('..')
		self.blueColButRl = mc.rowLayout(
											'%sBlueColButRl' % self.ui,
											numberOfColumns=nc,
											columnWidth3=(cw, cw, cw)
										)
		mc.button(w=cw, l='B', bgc=[0,0,1], c=partial(self.assignColor,'blue'))
		mc.button(w=cw, l='DB', bgc=[0,0,0.25], c=partial(self.assignColor,'darkBlue'))
		mc.button(w=cw, l='SB', bgc=[0,1,1], c=partial(self.assignColor,'softBlue'))

		mc.setParent('..')
		self.greenColButRl = mc.rowLayout(
												'%sGleenColButRl' % self.ui,
												numberOfColumns=nc,
												columnWidth3=(cw, cw, cw)
											)
		mc.button(w=cw, l='G', bgc=[0,1,0], c=partial(self.assignColor,'green'))
		mc.button(w=cw, l='DG', bgc=[0,0.25,0], c=partial(self.assignColor,'darkGreen'))
		mc.button(w=cw, l='Y', bgc=[1,1,0], c=partial(self.assignColor,'yellow'))

		mc.setParent('..')
		self.mainColButRl = mc.rowLayout(
												'%sMainColButRl' % self.ui,
												numberOfColumns=nc,
												columnWidth3=(cw, cw, cw)
											)
		mc.button(w=cw, l='W', bgc=[1,1,1], c=partial(self.assignColor,'white'))
		mc.button(w=cw, l='Br', bgc=[0.2,0,0], c=partial(self.assignColor,'brown'))
		mc.button(w=cw, l='Nn', c=partial(self.assignColor,None))
		
		mc.showWindow(self.win)

	def assignColor(self, col='', arg=None):
		
		colDic = {
					'black': 1,
					'gray': 2,
					'softGray': 3,
					'darkBlue': 15,
					'blue': 6,
					'darkGreen': 7,
					'brown': 11,
					'darkRed': 12,
					'red': 13,
					'green': 14,
					'white': 16,
					'yellow': 17,
					'softBlue': 18,
					'softRed': 31,
					None: 0
				}
		
		cid = colDic[col]
		sels = mc.ls(sl=True)
		
		for sel in sels :
			shps = mc.listRelatives(sel, s=True)
			
			if shps :
				for shp in shps :
					if col == None :
						mc.setAttr('%s.overrideColor' % shp, cid)
						mc.setAttr('%s.overrideEnabled' % shp, 0)

					else :
						mc.setAttr('%s.overrideEnabled' % shp, 1)
						mc.setAttr('%s.overrideColor' % shp, cid)
