import maya.cmds as mc
import maya.mel as mm

def main(collision_thickness=0.05, steps=50, push_out=0.1, iterations=1):

	sels = mc.ls(sl=True)

	# Duplicate to get temp objects.
	tmps = mc.duplicate(sels, rr=True)

	cloth_obj = tmps[0]
	collision_objs = tmps[1:]

	# Create nCloth
	mc.select(cloth_obj, r=True)
	cloth_shape = mm.eval('createNCloth 0')[0]

	# Collision objects
	mc.select(collision_objs, r=True)
	collision_shapes = mm.eval('makeCollideNCloth')

	# Set collision attributes.
	for each in collision_shapes + [cloth_shape]:
		mc.setAttr('%s.thickness' % each, collision_thickness)

	mc.setAttr('%s.selfCollisionFlag' % cloth_shape, 3)
	mc.setAttr('%s.selfCollideWidthScale' % cloth_shape, 1)

	# Resolve penetration.
	mc.currentTime(0)
	for ix in range(iterations):
		mc.select(tmps, r=True)
		mm.eval('resolveInterpenetration {steps} {push_out} {push_out} 0.99 1;'.format(steps=steps, push_out=push_out))

	# Copy cloth shape
	for ix in range(mc.polyEvaluate(cloth_obj, v=True)):

		src_vtx = '{0}.vtx[{1}]'.format(cloth_obj, ix)
		tar_vtx = '{0}.vtx[{1}]'.format(sels[0], ix)
		
		src_posts = mc.xform(src_vtx, q=True, os=True, t=True)
		mc.xform(tar_vtx, os=True, t=src_posts)

	# Cleanup
	mc.delete(tmps)
	nucleuses = mc.ls(type='nucleus')
	if nucleuses:
		mc.delete(nucleuses)