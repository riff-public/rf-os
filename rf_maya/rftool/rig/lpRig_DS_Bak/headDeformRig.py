import maya.cmds as mc
import maya.mel as mm

from lpRig import core as lrc
reload(lrc)
from lpRig import rigTools as lrr
reload(lrr)
import lpRig.ribbon as lrb
reload(lrb)

'''
How to run:

from lpRig import headDeformRig
reload(headDeformRig)

headDeformRig.main()
'''

def createSpaceGroup():
	'''
	Create head deform space groups in FclRig module.
	'''
	for part in ('Low', 'Mid', 'Hi'):

		currSpcGrp = lrc.Null()
		currSpcGrp.name = 'Hd{}FclRigSpc_Grp'.format(part)

		currSpcZrGrp = lrc.group(currSpcGrp)
		currSpcZrGrp.name = 'Hd{}FclRigSpcZr_Grp'.format(part)

		curSpcSrc = lrc.Dag(mc.ls('*:Hd{}HdDfmRigSpc_Grp'.format(part))[0])
		curSpcZrSrc = lrc.Dag(mc.ls('*:Hd{}HdDfmRigSpcZr_Grp'.format(part))[0])

		currSpcZrGrp.snap(curSpcZrSrc)

		for attr in ('t', 'r', 's'):
			curSpcSrc.attr(attr) >> currSpcGrp.attr(attr)

		currSpcZrGrp.parent('FclRigCtrl_Grp')

class HeadDeformRig(object):

	def __init__(
					self,
					tmpLocs=[],
					part='',
					side=''
				):

		# Define Side
		if side:
			side = '_%s_' % side
		else:
			side = '_'

		# Tmp Locs
		midLoc = lrc.Dag(tmpLocs[0])
		upLoc = lrc.Dag(tmpLocs[1])
		upTipLoc = lrc.Dag(tmpLocs[2])
		lowLoc = lrc.Dag(tmpLocs[3])
		lowTipLoc = lrc.Dag(tmpLocs[4])
		neckLoc = lrc.Dag(tmpLocs[5])

		# Main Group
		self.Ctrl_Grp = lrc.Null()
		self.Ctrl_Grp.name = '%sCtrl%sGrp' % (part, side)

		self.Jnt_Grp = lrc.Null()
		self.Jnt_Grp.name = '%sJnt%sGrp' % (part, side)

		self.Skin_Grp = lrc.Null()
		self.Skin_Grp.name = '%sSkin%sGrp' % (part, side)

		self.Still_Grp = lrc.Null()
		self.Still_Grp.name = '%sStill%sGrp' % (part, side)

		# Pivot Groups
		# Pivot Groups - Mid Piv
		self.HdMidPiv_Grp = lrc.Null()
		self.HdMidPiv_Grp.snap(midLoc)
		self.HdMidPiv_Grp.name = 'HdMid%sPiv%sGrp' % (part, side)

		self.HdMidPivZr_Grp = lrr.groupAt(self.HdMidPiv_Grp)
		self.HdMidPivZr_Grp.name = 'HdMid%sPivZr%sGrp' % (part, side)

		# Pivot Groups - Up Piv
		self.HdUpPiv_Grp = lrc.Null()
		self.HdUpPiv_Grp.snap(upLoc)
		self.HdUpPiv_Grp.name = 'HdUp%sPiv%sGrp' % (part, side)

		self.HdUpPivZr_Grp = lrr.groupAt(self.HdUpPiv_Grp)
		self.HdUpPivZr_Grp.name = 'HdUp%sPivZr%sGrp' % (part, side)

		# Pivot Groups - UpTip Piv
		self.HdUpTipPiv_Grp = lrc.Null()
		self.HdUpTipPiv_Grp.snap(upTipLoc)
		self.HdUpTipPiv_Grp.name = 'HdUpTip%sPiv%sGrp' % (part, side)

		self.HdUpTipPivZr_Grp = lrr.groupAt(self.HdUpTipPiv_Grp)
		self.HdUpTipPivZr_Grp.name = 'HdUpTip%sPivZr%sGrp' % (part, side)

		# Pivot Groups - Low Piv
		self.HdLowPiv_Grp = lrc.Null()
		self.HdLowPiv_Grp.snap(lowLoc)
		self.HdLowPiv_Grp.name = 'HdLow%sPiv%sGrp' % (part, side)

		self.HdLowPivZr_Grp = lrr.groupAt(self.HdLowPiv_Grp)
		self.HdLowPivZr_Grp.name = 'HdLow%sPivZr%sGrp' % (part, side)

		# Pivot Groups - LowTip Piv
		self.HdLowTipPiv_Grp = lrc.Null()
		self.HdLowTipPiv_Grp.snap(lowTipLoc)
		self.HdLowTipPiv_Grp.name = 'HdLowTip%sPiv%sGrp' % (part, side)

		self.HdLowTipPivZr_Grp = lrr.groupAt(self.HdLowTipPiv_Grp)
		self.HdLowTipPivZr_Grp.name = 'HdLowTip%sPivZr%sGrp' % (part, side)

		# Pivot Groups  - Parenting
		self.HdUpPivZr_Grp.parent(self.HdMidPiv_Grp)
		self.HdLowPivZr_Grp.parent(self.HdMidPiv_Grp)
		self.HdMidPivZr_Grp.parent(self.Jnt_Grp)
		self.HdUpTipPivZr_Grp.parent(self.Jnt_Grp)
		self.HdLowTipPivZr_Grp.parent(self.Jnt_Grp)

		# Skin Joints
		self.Neck_Jnt = lrr.jointAt(neckLoc)
		self.Neck_Jnt.name = 'Neck%s%sJnt' % (part, side)

		self.HdMid_Jnt = lrr.jointAt(midLoc)
		self.HdMid_Jnt.name = 'HdMid%s%sJnt' % (part, side)

		self.HdMidJntZr_Grp = lrr.groupAt(self.HdMid_Jnt)
		self.HdMidJntZr_Grp.name = 'HdMid%sJntZr%sGrp' % (part, side)

		self.HdMidJntZr_Grp.parent(self.Neck_Jnt)
		self.Neck_Jnt.parent(self.Skin_Grp)

		mc.parentConstraint(self.HdMidPiv_Grp, self.HdMid_Jnt)

		# Main Controllers - Middle
		self.HdMid_Ctrl = lrc.Control('nrbCircle')
		self.HdMid_Ctrl.name = 'HdMid%s%sCtrl' % (part, side)
		self.HdMid_Ctrl.color = 'yellow'
		self.HdMid_Ctrl.lockHideAttrs('rx', 'ry', 'rz', 'sx', 'sy', 'sz', 'v')

		self.HdMidCtrlZr_Grp = lrc.group(self.HdMid_Ctrl)
		self.HdMidCtrlZr_Grp.name = 'HdMid%sCtrlZr%sGrp' % (part, side)

		self.HdMidCtrlZr_Grp.snap(self.HdMidPiv_Grp)
		self.HdMidCtrlZr_Grp.parent(self.Ctrl_Grp)

		self.HdMid_Ctrl.attr('t') >> self.HdMidPiv_Grp.attr('t')

		# Main Controllers - Upper
		self.HdUp_Ctrl = lrc.Control('nrbCircle')
		self.HdUp_Ctrl.name = 'HdUp%s%sCtrl' % (part, side)
		self.HdUp_Ctrl.color = 'yellow'
		self.HdUp_Ctrl.lockHideAttrs('rx', 'ry', 'rz', 'sx', 'sy', 'sz', 'v')

		self.HdUpCtrlZr_Grp = lrc.group(self.HdUp_Ctrl)
		self.HdUpCtrlZr_Grp.name = 'HdUp%sCtrlZr%sGrp' % (part, side)

		self.HdUpCtrlZr_Grp.snap(self.HdUpTipPiv_Grp)
		self.HdUpCtrlZr_Grp.parent(self.Ctrl_Grp)

		self.HdUp_Ctrl.attr('t') >> self.HdUpTipPiv_Grp.attr('t')

		# Main Controllers - Lower
		self.HdLow_Ctrl = lrc.Control('nrbCircle')
		self.HdLow_Ctrl.name = 'HdLow%s%sCtrl' % (part, side)
		self.HdLow_Ctrl.color = 'yellow'
		self.HdLow_Ctrl.lockHideAttrs('rx', 'ry', 'rz', 'sx', 'sy', 'sz', 'v')

		self.HdLowCtrlZr_Grp = lrc.group(self.HdLow_Ctrl)
		self.HdLowCtrlZr_Grp.name = 'HdLow%sCtrlZr%sGrp' % (part, side)

		self.HdLowCtrlZr_Grp.snap(self.HdLowTipPiv_Grp)
		self.HdLowCtrlZr_Grp.parent(self.Ctrl_Grp)

		self.HdLow_Ctrl.attr('t') >> self.HdLowTipPiv_Grp.attr('t')

		# Ribbon - Up
		self.UpRbn = lrb.RibbonIkLow(
										size=lrc.distance(self.HdUpPiv_Grp, self.HdUpTipPiv_Grp),
										ax='y+',
										part='HdUp%s' % part,
										side=side.replace('_','')
									)

		self.UpRbn.Ctrl_Grp.snapPoint(self.HdUpPiv_Grp)
		mc.delete(
					mc.aimConstraint(
										self.HdUpTipPiv_Grp,
										self.UpRbn.Ctrl_Grp,
										aim=(0, 1, 0), u=(1, 0, 0), wut='objectrotation',
										wuo=self.HdUpPiv_Grp, wu=(1, 0, 0)
									)
					)

		mc.pointConstraint(self.HdUpPiv_Grp, self.UpRbn.RbnRoot_Ctrl)
		mc.pointConstraint(self.HdUpTipPiv_Grp, self.UpRbn.RbnEnd_Ctrl)

		# Ribbon - Low
		self.LowRbn = lrb.RibbonIkLow(
										size=lrc.distance(self.HdLowPiv_Grp, self.HdLowTipPiv_Grp),
										ax='y-',
										part='HdLow%s' % part,
										side=side.replace('_','')
									)

		self.LowRbn.Ctrl_Grp.snapPoint(self.HdLowPiv_Grp)
		mc.delete(
					mc.aimConstraint(
										self.HdLowTipPiv_Grp,
										self.LowRbn.Ctrl_Grp,
										aim=(0, -1, 0), u=(1, 0, 0), wut='objectrotation',
										wuo=self.HdLowPiv_Grp, wu=(1, 0, 0)
									)
					)

		mc.pointConstraint(self.HdLowPiv_Grp, self.LowRbn.RbnRoot_Ctrl)
		mc.pointConstraint(self.HdLowTipPiv_Grp, self.LowRbn.RbnEnd_Ctrl)

		# Ribbon - Connection
		# Ribbon - Squash Setup
		self.HdUp_Ctrl.add(ln='autoSquash', min=0, max=1, k=True)
		self.HdUp_Ctrl.add(ln='squash', k=True)

		self.HdUp_Ctrl.attr('autoSquash') >> self.UpRbn.Rbn_Ctrl.attr('autoSquash')
		self.HdUp_Ctrl.attr('squash') >> self.UpRbn.Rbn_Ctrl.attr('squash')

		self.HdLow_Ctrl.add(ln='autoSquash', min=0, max=1, k=True)
		self.HdLow_Ctrl.add(ln='squash', k=True)

		self.HdLow_Ctrl.attr('autoSquash') >> self.LowRbn.Rbn_Ctrl.attr('autoSquash')
		self.HdLow_Ctrl.attr('squash') >> self.LowRbn.Rbn_Ctrl.attr('squash')

		# Ribbon - Cleanup
		for rbnObj in (self.UpRbn, self.LowRbn):

			rbnObj.Rbn_Ctrl.lockKeyableAttrs()
			rbnObj.RbnRoot_Ctrl.lockKeyableAttrs()
			rbnObj.RbnEnd_Ctrl.lockKeyableAttrs()

			rbnObj.Rbn_Ctrl.scaleShape(0)
			rbnObj.RbnRoot_Ctrl.scaleShape(0)
			rbnObj.RbnEnd_Ctrl.scaleShape(0)

			rbnObj.Ctrl_Grp.parent(self.Still_Grp)
			rbnObj.Jnt_Grp.parent(self.Jnt_Grp)
			rbnObj.Skin_Grp.parent(self.Skin_Grp)

			rbnObj.RbnMid_Jnt.attr('v').v = 0

		# Mid Head Space
		self.HdMidSpc_Grp, self.HdMidSpcZr_Grp = self.createSpaceGroup(self.HdMidJntZr_Grp, self.HdMid_Jnt, 'HdMid', part, side)
		self.HdHiSpc_Grp, self.HdHiSpcZr_Grp = self.createSpaceGroup(self.UpRbn.RbnJntOfst_Grp, self.UpRbn.Rbn_Jnt, 'HdHi', part, side)
		self.HdLowSpc_Grp, self.HdLowSpcZr_Grp = self.createSpaceGroup(self.LowRbn.RbnJntOfst_Grp, self.LowRbn.Rbn_Jnt, 'HdLow', part, side)

		# Slide attribute
		self.HdUp_Ctrl.add(ln='slide', k=True)
		self.HdUp_Ctrl.attr('slide') >> self.UpRbn.RbnJntOfst_Grp.attr('tx')

		self.HdLow_Ctrl.add(ln='slide', k=True)
		self.HdLow_Ctrl.attr('slide') >> self.LowRbn.RbnJntOfst_Grp.attr('tx')

	def createSpaceGroup(self, zrGrp, target, grpPart, part, side):

		spcGrp = lrc.Null()
		spcGrp.name = '%s%sSpc%sGrp' % (grpPart, part, side)

		spcZrGrp = lrc.group(spcGrp)
		spcZrGrp.name = '%s%sSpcZr%sGrp' % (grpPart, part, side)

		# mc.parentConstraint(zrGrp, spcZrGrp)
		# mc.scaleConstraint(zrGrp, spcZrGrp)

		spcZrGrp.snap(zrGrp)

		mc.parentConstraint(target, spcGrp)
		mc.scaleConstraint(target, spcGrp)

		spcZrGrp.parent(self.Still_Grp)

		return spcGrp, spcZrGrp

def main():
	
	hdDfrmObj = HeadDeformRig(
									tmpLocs = [
												'HdMidHdDfmRigTmp_Loc',
												'HdUpHdDfmRigTmp_Loc',
												'HdUpTipHdDfmRigTmp_Loc',
												'HdLowHdDfmRigTmp_Loc',
												'HdLowTipHdDfmRigTmp_Loc',
												'NeckHdDfmRigTmp_Loc'
												],
									part='HdDfmRig',
									side=''
								)