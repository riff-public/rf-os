from functools import partial
import re

import maya.cmds as mc
import maya.mel as mm
import maya.cmds as mc
import maya.OpenMaya as om
import maya.OpenMayaAnim as oma

_comp_conv = mc.polyListComponentConversion
# maya.OpenMayaAnim.MFnSkinCluster
# 'absoluteName', 'addAttribute', 'addExternalContentForFileAttr', 
# 'allocateFlag', 'attribute', 'attributeClass', 'attributeCount', 
# 'canBeWritten', 'className', 'classification', 'create', 
# 'deallocateAllFlags', 'deallocateFlag', 'deformerSet', 'dgCallbackIds', 
# 'dgCallbacks', 'dgTimer', 'dgTimerOff', 'dgTimerOn', 'dgTimerQueryState', 
# 'dgTimerReset', 'enableDGTiming', 'envelope', 'findAlias', 'findPlug', 
# 'getAffectedAttributes', 'getAffectedByAttributes', 'getAliasAttr', 
# 'getAliasList', 'getBlendWeights', 'getConnections', 'getExternalContent', 
# 'getInputGeometry', 'getOutputGeometry', 'getPathAtIndex', 
# 'getPointsAffectedByInfluence', 'getWeights', 'groupIdAtIndex', 
# 'hasAttribute', 'hasObj', 'hasUniqueName', 'icon', 'indexForGroupId', 
# 'indexForInfluenceObject', 'indexForOutputConnection', 
# 'indexForOutputShape', 'influenceObjects', 'inputShapeAtIndex', 
# 'isDefaultNode', 'isFlagSet', 'isFromReferencedFile', 'isLocked', 
# 'isNewAttribute', 'isShared', 'isTrackingEdits', 'kExtensionAttr', 
# 'kInvalidAttr', 'kLocalDynamicAttr', 'kNormalAttr', 'kTimerInvalidState', 
# 'kTimerMetric_callback', 'kTimerMetric_callbackNotViaAPI', 
# 'kTimerMetric_callbackViaAPI', 'kTimerMetric_compute', 
# 'kTimerMetric_computeDuringCallback', 
# 'kTimerMetric_computeNotDuringCallback', 'kTimerMetric_dirty', 
# 'kTimerMetric_draw', 'kTimerMetric_fetch', 'kTimerMetrics', 'kTimerOff', 
# 'kTimerOn', 'kTimerType_count', 'kTimerType_inclusive', 'kTimerType_self', 
# 'kTimerTypes', 'kTimerUninitialized', 'name', 'numOutputConnections', 
# 'object', 'outputShapeAtIndex', 'parentNamespace', 'pluginName', 
# 'plugsAlias', 'removeAttribute', 'reorderedAttribute', 'setAlias', 
# 'setBlendWeights', 'setDoNotWrite', 'setEnvelope', 'setExternalContent', 
# 'setExternalContentForFileAttr', 'setFlag', 'setIcon', 'setLocked', 
# 'setName', 'setObject', 'setUuid', 'setWeights', 'this', 'type', 'typeId', 
# 'typeName', 'userNode', 'uuid'

# maya.OpenMaya.MSelectionList
# 'add', 'assign', 'className', 'clear', 'getDagPath', 'getDependNode', 'getPlug', 
# 'getSelectionStrings', 'hasItem', 'hasItemPartly', 'intersect', 'isEmpty', 
# 'kMergeNormal', 'kRemoveFromList', 'kXORWithList', 'length', 'merge', 'remove', 
# 'replace', 'this', 'toggle'

# maya.OpenMaya.MItMeshVertex
# 'className', 'connectedToEdge', 'connectedToFace', 'count', 'currentItem', 
# 'geomChanged', 'getColor', 'getColorIndices', 'getColors', 'getConnectedEdges', 
# 'getConnectedFaces', 'getConnectedVertices', 'getNormal', 'getNormalIndices', 
# 'getNormals', 'getOppositeVertex', 'getUV', 'getUVIndices', 'getUVs', 'hasColor', 
# 'index', 'isDone', 'next', 'numConnectedEdges', 'numConnectedFaces', 'numUVs', 
# 'onBoundary', 'position', 'reset', 'setIndex', 'setPosition', 'setUV', 'setUVs', 
# 'this', 'translateBy', 'updateSurface', 'vertex'

def print_obj(obj):
	"""Print each attribute/method of given object.
	"""
	print str(type(obj)).split('\'')[-2]
	print [ x for x in dir(obj) if not x.startswith('__')]

def id_from_str(comp_str=''):
	"""Get component id(int) from the component name.
	"""
	exp = r'(.+\[)([0-9]+)(\])'
	re_obj = re.search(exp, comp_str)

	if not re_obj:
		return None
	
	comp_id = int(re_obj.group(2))

	return comp_id

def dag_path_from_dag_str(dag_str=''):
	"""Get Dag Path object from dag_str.
	"""
	sel_list = om.MSelectionList()
	sel_list.add(dag_str)

	dag_path = om.MDagPath()

	sel_list.getDagPath(0, dag_path)

	return dag_path

def mirror_skin_selected():
	"""Mirror skin weight to selected geos.
	"""
	sels = mc.ls(sl=True)

	for sel in sels:
		mirror_skin(sel)

	mc.select(sels, r=True)

def mirror_skin(geo=''):
	"""Mirror skin weight to given geo.
	"""
	mirror_vtcs = []

	sel_list = om.MSelectionList()
	sel_list.add(geo)

	# Initiate vertex iterator for all vertices.
	dag_path = om.MDagPath()
	comp = om.MObject()
	sel_list.getDagPath(0, dag_path, comp)
	vtx_iter = om.MItMeshVertex(dag_path, comp)

	# Find related skin cluster node.
	skin_fn = get_skin_fn_from_dag_path(dag_path)

	# Turn skin envelope off for temporary.
	mc.setAttr('%s.envelope' % skin_fn.name(), 0)

	# Find the non-center vertices.
	tor = 0.0001
	while not vtx_iter.isDone():

		curr_pnt = vtx_iter.position(om.MSpace.kWorld)
		if not (-tor < curr_pnt[0] < tor):
			curr_vtx = '{dag}.vtx[{idx}]'.format(dag=dag_path.fullPathName(), 
													idx=vtx_iter.index())
			mirror_vtcs.append(curr_vtx)
		vtx_iter.next()

	# Mirror skin weight value to non-center vertices.
	mc.select(mirror_vtcs, r=True)
	mc.copySkinWeights(ss=skin_fn.name(), ds=skin_fn.name(), mirrorMode='YZ', 
						surfaceAssociation='closestPoint', 
						influenceAssociation='closestJoint')

	# Turn skin envelope back to on.
	mc.setAttr('%s.envelope' % skin_fn.name(), 1)

def move_weight(geo='', src_jnts=[], tar_jnt=''):
	"""Select geo, source joints then target joint.
	"""
	geo_dag_path = dag_path_from_dag_str(geo)

	skin_fn = get_skin_fn_from_dag_path(geo_dag_path)

	src_jnt_dag_paths = [dag_path_from_dag_str(ix) for ix in src_jnts]
	tar_jnt_dag_path = dag_path_from_dag_str(tar_jnt)

	inf_dag_ary = om.MDagPathArray()
	skin_fn.influenceObjects(inf_dag_ary)

	# Populate Index Array for all influence joints, 
	# source joints and an index of target joint.
	inf_idx_ary = om.MIntArray()
	src_idx_ary = om.MIntArray()
	tar_idx = None

	for ix in xrange(inf_dag_ary.length()):
		inf_idx_ary.append(ix)
		if inf_dag_ary[ix] in src_jnt_dag_paths:
			src_idx_ary.append(ix)
		elif inf_dag_ary[ix] == tar_jnt_dag_path:
			tar_idx = ix

	# Iterate through each of source index.
	for idx in src_idx_ary:

		afct_vtx_sel_list = om.MSelectionList()
		afct_vtx_dbl_ary = om.MDoubleArray()
		skin_fn.getPointsAffectedByInfluence(inf_dag_ary[idx], afct_vtx_sel_list, 
												afct_vtx_dbl_ary)

		if afct_vtx_sel_list.isEmpty():
			continue

		# Get vertex iterator.
		curr_dag_path = om.MDagPath()
		curr_mobj = om.MObject()
		afct_vtx_sel_list.getDagPath(0, curr_dag_path, curr_mobj)
		vtx_iter = om.MItMeshVertex(curr_dag_path, curr_mobj)

		# Iterate through each affected vertex.
		while not vtx_iter.isDone():

			curr_vtx_str = '{dag}.vtx[{idx}]'.format(dag=curr_dag_path.fullPathName(), 
													idx=vtx_iter.index())

			# Get weight values of current vertex.
			curr_mobj = vtx_iter.currentItem()
			curr_wgts = om.MDoubleArray()
			skin_fn.getWeights(geo_dag_path, curr_mobj, inf_idx_ary, curr_wgts)

			curr_src_wgt = curr_wgts[idx]

			# Skip if current source influence doesn't have weight value.
			if not curr_src_wgt:
				vtx_iter.next()
				continue

			# Calculation and assign weight values.
			curr_vtx_id = vtx_iter.index()
			new_tar_wgt = curr_wgts[tar_idx] + curr_src_wgt

			src_wl_attr = '%s.weightList[%s].weights[%s]' % (skin_fn.name(), 
																curr_vtx_id, idx)
			tar_wl_attr = '%s.weightList[%s].weights[%s]' % (skin_fn.name(), 
																curr_vtx_id, tar_idx)

			mc.setAttr(src_wl_attr, 0)
			mc.setAttr(tar_wl_attr, new_tar_wgt)

			vtx_iter.next()

def edit_weight_to_selected(val=0, opr='Add'):
	"""Select components then a joint to add weight value to.
	"""
	sels = mc.ls(sl=True, fl=True, l=True)

	comps = sels[0:-1]
	jnt = sels[-1]

	if not mc.nodeType(jnt) == 'joint':
		print "The last selection wasn't joint."
		return False

	vtcs = _comp_conv(comps, tv=True)

	edit_weight_to(vtcs, jnt, val, opr)

def smooth_selected():
	"""Smooth skin weight value to selected vertices.
	"""
	sels = mc.ls(sl=True, fl=True)
	vtcs = mc.ls(_comp_conv(sels, tv=True), fl=True)

	# List of output weight value tables.
	result_list = []

	# Append all vertices to selection list.
	vtx_list = om.MSelectionList()
	[vtx_list.add(vtx) for vtx in vtcs]

	# Initiate DAG path, MObject and vertex iterator for all vertices.
	dag_path = om.MDagPath()
	comp = om.MObject()
	vtx_list.getDagPath(0, dag_path, comp)
	vtx_iter = om.MItMeshVertex(dag_path, comp)

	# Find related skin cluster node.
	skin_fn = get_skin_fn_from_dag_path(dag_path)

	# Populate influence index array.
	inf_dag_ary = om.MDagPathArray()
	skin_fn.influenceObjects(inf_dag_ary)
	inf_num = inf_dag_ary.length()
	inf_idx_ary = om.MIntArray()
	[inf_idx_ary.append(ix) for ix in range(inf_num)]

	# List of current unhold influences.
	unhold_list = [not mc.getAttr('%s.liw' % inf_dag_ary[ix].fullPathName()) for ix in range(inf_dag_ary.length())]

	while not vtx_iter.isDone():

		curr_vtx_str = '{dag}.vtx[{idx}]'.format(dag=dag_path.fullPathName(), 
													idx=vtx_iter.index())
		curr_result_list = [0 for ix in xrange(inf_num)]

		# Get weight values of current vertex.
		curr_mobj = vtx_iter.currentItem()
		curr_wgts = om.MDoubleArray()
		skin_fn.getWeights(dag_path, curr_mobj, inf_idx_ary, curr_wgts)

		# Do nothing if summation of current unhold values is already 1.
		unhold_sum = 0
		for ix in xrange(inf_num):
			if not unhold_list[ix]:
				unhold_sum += curr_wgts[ix]

		if unhold_sum == 1:
			result_list.append(curr_wgts)
			vtx_iter.next()
			continue

		# Get connected vertices.
		con_fvs = _comp_conv(curr_vtx_str, fv=True, te=True, internal=False)
		con_vtcs = mc.ls(_comp_conv(con_fvs, ff=True, fe=True, 
							tv=True, internal=False), fl=True)
		con_num = len(con_vtcs)

		# Iterate through each influence.
		for ix in xrange(inf_num):

			# Skip if current influence is set to hold.
			if not unhold_list[ix]:
				curr_result_list[ix] = curr_wgts[ix]
				continue

			# Average weight value.
			curr_inf_wgt_val = 0
			for con_vtx in con_vtcs:
				curr_con_vtx_id = id_from_str(con_vtx)
				curr_wl_attr = '%s.weightList[%s].weights[%s]' % (skin_fn.name(), curr_con_vtx_id, ix)
				curr_con_wgt_val = mc.getAttr(curr_wl_attr)

				if curr_con_wgt_val:
					curr_inf_wgt_val += curr_con_wgt_val

			curr_avg = curr_inf_wgt_val/float(con_num)
			curr_result_list[ix] = curr_avg

		result_list.append(curr_result_list)

		vtx_iter.next()

	# Assign weight values
	for ix, vtx in enumerate(vtcs):

		curr_vtx_id = id_from_str(vtx)

		for inf_idx in xrange(inf_num):
			curr_wgt_vals = result_list[ix][inf_idx]
			wl_attr = '%s.weightList[%s].weights[%s]' % (skin_fn.name(), curr_vtx_id, 
															inf_idx)
			mc.setAttr(wl_attr, curr_wgt_vals)

	mc.skinPercent(skin_fn.name(), vtcs, normalize=True)

def auto_smooth(iteration=1):
	"""Expand the current selectoin and apply smooth weight.
	"""
	sels = mc.ls(sl=True)

	mm.eval('PolySelectConvert 3;')
	root_vtcs = mc.ls(sl=True, fl=True)

	exp_vtx_list = []
	all_vtcs = root_vtcs

	for ix in xrange(iteration):

		mm.eval('GrowPolygonSelectionRegion;')
		curr_sels = mc.ls(sl=True, fl=True)
		curr_exp_list = []

		for curr_sel in curr_sels:
			if not curr_sel in all_vtcs:
				curr_exp_list.append(curr_sel)
				all_vtcs.append(curr_sel)

		if curr_exp_list:
			exp_vtx_list.append(curr_exp_list)

	for ix in xrange(iteration):
		for exp_vtcs in exp_vtx_list[0:ix+1]:
			mc.select(exp_vtcs, r=True)
			smooth_selected()

	mc.select(sels, r=True)

def edit_weight_to(vtcs=[], jnt='', val=0.00, opr='Add'):
	"""Edit skin weight values of given joint on given vertices according to
	given value and operation.
	"""

	# Append all vertices to selection list.
	vtx_list = om.MSelectionList()
	[vtx_list.add(vtx) for vtx in vtcs]

	# Initiate vertex iterator for all vertices.
	dag_path = om.MDagPath()
	comp = om.MObject()
	vtx_list.getDagPath(0, dag_path, comp)
	vtx_iter = om.MItMeshVertex(dag_path, comp)

	# Find related skin cluster node.
	skin_fn = get_skin_fn_from_dag_path(dag_path)

	# Get influence index array for the selected joint.
	inf_dag_ary = om.MDagPathArray()
	skin_fn.influenceObjects(inf_dag_ary)
	inf_idx_ary = om.MIntArray()

	for ix in range(inf_dag_ary.length()):
		if jnt == inf_dag_ary[ix].fullPathName():
			inf_idx_ary.append(ix)

	# Iterate through each vertex and assign values.
	while not vtx_iter.isDone():

		curr_vtx_str = '{dag}.vtx[{idx}]'.format(dag=dag_path.fullPathName(), 
													idx=vtx_iter.index())

		curr_mobj = vtx_iter.currentItem()

		curr_wgts = om.MDoubleArray()
		skin_fn.getWeights(dag_path, curr_mobj, inf_idx_ary, curr_wgts)

		new_wgt = curr_wgts[0]
		if opr == 'Add':
			new_wgt = curr_wgts[0] + val
		elif opr == 'Replace':
			new_wgt = val
		elif opr == 'Scale':
			new_wgt = float(curr_wgts[0]) * float(val)

		mc.skinPercent(skin_fn.name(), curr_vtx_str, 
						transformValue=[(jnt, new_wgt)])
		
		vtx_iter.next()

def get_skin_fn_from_dag_path(dag_path):
	"""Get skin function that connected to given dag_path.
	dag_path is a MDagPath object
	"""
	try:
		dag_path.extendToShape()
	except:
		return False
	shape_node = dag_path.node()
	
	try:
		iter_dg = om.MItDependencyGraph(shape_node, om.MFn.kSkinClusterFilter, 
											om.MItDependencyGraph.kUpstream)

		while not iter_dg.isDone():
			curr_item = iter_dg.currentItem()
			fn_skin = oma.MFnSkinCluster(curr_item)

			return fn_skin
	except:
		return False

def unlock_selected_joints_from_selected_geos():
	"""Unlock selected influence joints of given geo, lock the rest.
	"""
	sels = mc.ls(sl=True)

	geos = []
	jnts = []

	for sel in sels:
		if mc.nodeType(sel) == 'joint':
			jnts.append(sel)
		elif mc.nodeType(sel) == 'transform':
			geos.append(sel)

	if geos and (not jnts):
		# Unhold all influences of selected geos.
		for geo in geos:
			lock_all_influences(geo, False)

	if jnts and (not geos):
		# Unhold selected influences.
		for jnt in jnts:
			mc.setAttr('%s.liw' % jnt, False)

	if jnts and geos:
		# Unhold only selected influences and 
		# hold the rest influences of selected geos.
		for geo in geos:
			lock_all_influences(geo, True)

		for jnt in jnts:
			mc.setAttr('%s.liw' % jnt, False)

def lock_all_influences(geo='', lock=True):
	"""Lock/Unlock all influence joints of given geo.
	"""
	infs = []
	skin = mm.eval('findRelatedSkinCluster %s' % geo)

	if not skin:
		return infs

	curr_infs = mc.skinCluster(skin, q=True, inf=True)

	for inf in curr_infs:
		mc.setAttr('%s.liw' % inf, lock)
		if not inf in infs:
			infs.append(inf)

	return infs

def select_affected_vertices(geo='', jnt=''):
	"""Selected affected vertices of the give joint.
	"""
	geo_dag_path = dag_path_from_dag_str(geo)
	skin_fn = get_skin_fn_from_dag_path(geo_dag_path)

	sel_jnt_dag_path = dag_path_from_dag_str(jnt)
	self_jnt_fp = sel_jnt_dag_path.fullPathName()

	inf_dag_ary = om.MDagPathArray()
	skin_fn.influenceObjects(inf_dag_ary)

	inf_idx = None

	for ix in xrange(inf_dag_ary.length()):
		curr_dag_fp = inf_dag_ary[ix].fullPathName()
		if curr_dag_fp == self_jnt_fp:
			inf_idx = ix

	afct_vtx_sel_list = om.MSelectionList()
	afct_vtx_dbl_ary = om.MDoubleArray()
	skin_fn.getPointsAffectedByInfluence(inf_dag_ary[inf_idx], afct_vtx_sel_list, 
											afct_vtx_dbl_ary)

	# Initiate vertex iterator for all vertices.
	dag_path = om.MDagPath()
	comp = om.MObject()
	afct_vtx_sel_list.getDagPath(0, dag_path, comp)
	vtx_iter = om.MItMeshVertex(dag_path, comp)

	vtx_list = []
	while not vtx_iter.isDone():
		curr_vtx_str = '{dag}.vtx[{idx}]'.format(dag=dag_path.fullPathName(), 
													idx=vtx_iter.index())
		vtx_list.append(curr_vtx_str)
		vtx_iter.next()

	mc.select(vtx_list, r=True)

def show(obj_type='', *args):
	"""Toggle visibility of the given object type.
	"""
	panel = mc.getPanel(wf=True)

	if obj_type == 'nurbsCurves':
		curr_state = mc.modelEditor(panel, q=True, nurbsCurves=True)
		mc.modelEditor(panel, e=True, nurbsCurves=not(curr_state))

	elif obj_type == 'nurbsSurfaces':
		curr_state = mc.modelEditor(panel, q=True, nurbsSurfaces=True)
		mc.modelEditor(panel, e=True, nurbsSurfaces=not(curr_state))

	elif obj_type == 'polymeshes':
		curr_state = mc.modelEditor(panel, q=True, polymeshes=True)
		mc.modelEditor(panel, e=True, polymeshes=not(curr_state))

	elif obj_type == 'joints':
		curr_state = mc.modelEditor(panel, q=True, joints=True)
		mc.modelEditor(panel, e=True, joints=not(curr_state))

	elif obj_type == 'grid':
		curr_state = mc.modelEditor(panel, q=True, grid=True)
		mc.modelEditor(panel, e=True, grid=not(curr_state))

	elif obj_type == 'wos':
		curr_state = mc.modelEditor(panel, q=True, wos=True)
		mc.modelEditor(panel, e=True, wos=not(curr_state))

def app():
	"""Run SkinTools app.
	"""
	app = SkinTools()
	return app

class SkinTools(object):

	def __init__(self):

		self.ui = 'SkinTools'
		self.win = '%sWin' % self.ui

		win_w = 220
		win_h = 350

		if mc.window(self.win, exists=True):
			mc.deleteUI(self.win)

		mc.window(self.win, t=self.ui)
		self.main_col = mc.columnLayout(adj=True)

		self.show_tx = mc.text(label='Show', align='left')

		show_but_h = 40
		show_but_w = win_w/3

		show_but_lambda = lambda l, c: mc.button(l=l, c=c, w=show_but_w, h=show_but_h)

		self.show_1_rl = mc.rowLayout(nc=3, adj=3, cw3=(show_but_w, show_but_w, show_but_w))
		self.crv_show_but = show_but_lambda(l='Curve', c=partial(show, 'nurbsCurves'))
		self.srf_show_but = show_but_lambda(l='Surface', c=partial(show, 'nurbsSurfaces'))
		self.ply_show_but = show_but_lambda(l='Polygon', c=partial(show, 'polymeshes'))

		mc.setParent('..')

		self.show_2_rl = mc.rowLayout(nc=3, adj=3, cw3=(show_but_w, show_but_w, show_but_w))
		self.jnt_show_but = show_but_lambda(l='Joint', c=partial(show, 'joints'))
		self.grid_show_but = show_but_lambda(l='Grid', c=partial(show, 'grid'))
		self.wire_show_but = show_but_lambda(l='WireShade', c=partial(show, 'wos'))

		mc.setParent('..')

		# Value float field.
		mc.separator()
		self.edit_tx = mc.text(label='Edit', align='left')

		self.val_ff = mc.floatField()
		mc.popupMenu()
		for ix in (0, 0.01, 0.05, 0.1, 0.5, 0.98, 1):
			mc.menuItem(l=str(ix), c=partial(self._set_val, ix))

		mc.floatField(self.val_ff, e=True, v=0.01)

		# Operation radio collection.
		self.opr_rad_coll = mc.radioCollection()
		self.add_rad_but = mc.radioButton(l='Add')
		self.rep_rad_but = mc.radioButton(l='Replace')
		self.sca_rad_but = mc.radioButton(l='Scale')

		mc.radioButton(self.add_rad_but, e=True, sl=True)

		# Assign weight values.
		self.asg_but = mc.button(l='Assign', c=partial(self._assign_wgt), h=show_but_h)

		# Smooth weight.
		mc.separator()
		self.smth_tx = mc.text(label='Smooth', align='left')
		self.smth_but = mc.button(l='Smooth Selected', c=partial(self._smth_wgt), h=show_but_h)

		# Auto-Smooth weight.
		mc.separator()
		smth_if_w = win_w/6
		smth_but_w = (win_w/6)*5
		self.at_smth_rl = mc.rowLayout(nc=2, adj=2, cw2=(smth_if_w, smth_but_w))
		self.smth_iter_if = mc.intField(minValue=1, value=1, h=show_but_h)
		self.at_smth_but = mc.button(l='Auto Smooth', c=partial(self._at_smth_wgt), h=show_but_h)

		mc.setParent('..')

		# Hold/Unhold
		mc.separator()
		self.hold_tx = mc.text(label='Hold/Unhold', align='left')

		hold_but_w = win_w/2
		self.hold_rl = mc.rowLayout(nc=2, adj=2, cw2=(hold_but_w, hold_but_w))
		self.hold_but = mc.button(l='Hold', h=show_but_h, w=hold_but_w, c=partial(self._hold))
		self.unhold_but = mc.button(l='Unhold', h=show_but_h, w=hold_but_w, c=partial(self._unhold))

		mc.setParent('..')

		# Other tools
		mc.separator()
		self.tls_tx = mc.text(label='Tools', align='left')

		tls_no = 3
		tls_but_w = win_w/tls_no
		self.tls_rl = mc.rowLayout(nc=tls_no, adj=1)

		self.mirror_wgt_but = mc.button(l='Mirror', h=show_but_h, w=tls_but_w, c=partial(self._mirror_wgt))
		self.move_wgt_but = mc.button(l='Transfer', h=show_but_h, w=tls_but_w, c=partial(self._move_wgt))
		self.sel_afctd_but = mc.button(l='Sel Afctd', h=show_but_h, w=tls_but_w, c=partial(self._sel_afctd))

		mc.setParent('..')

		mc.showWindow(self.win)
		mc.window(self.win, e=True, w=win_w)
		mc.window(self.win, e=True, h=win_h)

	def _set_val(self, val=0, *args):
		mc.floatField(self.val_ff, e=True, v=val)

	def _mirror_wgt(self, *args):
		mirror_skin_selected()

	def _assign_wgt(self, *args):

		curr_rad = mc.radioCollection(self.opr_rad_coll, q=True, sl=True)
		opr = mc.radioButton(curr_rad, q=True, l=True)

		val = mc.floatField(self.val_ff, q=True, v=True)
		
		edit_weight_to_selected(val, opr)

	def _move_wgt(self, *args):

		sels = mc.ls(sl=True)

		geo = sels[0]
		src_jnts = sels[1:-1]
		tar_jnt = sels[-1]

		move_weight(geo, src_jnts, tar_jnt)

	def _at_smth_wgt(self, *args):

		iteration = mc.intField(self.smth_iter_if, q=True, v=True)
		auto_smooth(iteration)

	def _smth_wgt(self, *args):
		smooth_selected()

	def _hold(self, *args):
		"""Lock/Unlock all influences of selected geos.
		"""
		sels = mc.ls(sl=True)

		geos = []
		jnts = []

		for sel in sels:
			if mc.nodeType(sel) == 'joint':
				jnts.append(sel)
			elif mc.nodeType(sel) == 'transform':
				geos.append(sel)

		if geos and (not jnts):
			# Hold all influences of selected geos.
			for geo in geos:
				lock_all_influences(geo, True)

		if jnts and (not geos):
			# Hold selected influences.
			for jnt in jnts:
				mc.setAttr('%s.liw' % jnt, True)

		if jnts and geos:
			# Hold only selected influences and 
			# uhold the rest influences of selected geos.
			for geo in geos:
				lock_all_influences(geo, False)

			for jnt in jnts:
				mc.setAttr('%s.liw' % jnt, True)

	def _unhold(self, *args):
		
		unlock_selected_joints_from_selected_geos()

	def _sel_afctd(self, *args):

		sels = mc.ls(sl=True)

		geos = []
		jnts = []

		for sel in sels:
			if mc.nodeType(sel) == 'joint':
				jnts.append(sel)
			elif mc.nodeType(sel) == 'transform':
				geos.append(sel)

		select_affected_vertices(geos[0], jnts[0])
