# Skin tools
import maya.cmds as mc
import maya.mel as mm
from functools import partial
import re

def run():
	# SkinTools call back
	ui = SkinTools()
	ui.show()

	mc.window(ui.win, e=True, w=200)
	return ui

def smoothSelected():
	
	sels = mc.ls(sl=True, fl=True)
	mm.eval('PolySelectConvert 3;')
	
	selVtcs = mc.ls(sl=True, fl=True)
	
	resultList = list()
	
	for selVtx in selVtcs:
		
		currShp = mc.ls(selVtx, o=True)[0]
		currTf = mc.listRelatives(currShp, p=True)[0]
		currSkc = mm.eval('findRelatedSkinCluster %s' % currTf)

		currInfs = mc.skinCluster(currSkc, q=True, inf=True)
		currWgtVals = mc.skinPercent(currSkc, selVtx, q=True, v=True)
		currUnHolds = [not mc.getAttr('%s.liw' % inf) for inf in currInfs]

		currResultList =  [0 for inf in currInfs]

		# Do nothing if sumation of current unhold values already 1.
		unHoldSum = 0
		for ix, inf in enumerate(currInfs):
			if not currUnHolds[ix]:
				unHoldSum += currWgtVals[ix]

		if unHoldSum == 1:
			resultList.append(currWgtVals)
			continue

		# Get connected vertices.
		conSel = mc.polyListComponentConversion(selVtx, fv=True, te=True, internal=False)
		vtcsSel = mc.polyListComponentConversion(conSel, ff=True, fe=True, tv=True, internal=False)
		conVtcs = mc.ls(vtcsSel, fl=True)
		conNo = len(conVtcs)

		for ix, inf in enumerate(currInfs):

			if not currUnHolds[ix]:
				currResultList[ix] = currWgtVals[ix]
				continue

			currInfWgtVal = 0

			for conVtx in conVtcs:

				currConWgtVals = mc.skinPercent(currSkc, conVtx, q=True, v=True)

				if currConWgtVals[ix]:
					currInfWgtVal += currConWgtVals[ix]

			currAverage = currInfWgtVal/float(conNo)
			currResultList[ix] = currAverage

		# # Normalize
		# currSum = sum(currResultList)
		
		# if not currSum == 1:
		# 	unholdNo = sum(currUnHolds)
		# 	compVal = (1 - currSum)/float(unholdNo)

		# 	for ix, result in enumerate(currResultList):
		# 		if currUnHolds[ix]:
		# 			currResultList[ix] = currResultList[ix] + compVal

		resultList.append(currResultList)
	
	# Assign weight values
	for ix, vtx in enumerate(selVtcs):
		
		exp = r'(.+vtx\[)([0-9]+)(\])'
		reObj = re.search(exp, vtx)
		
		if not reObj:
			continue

		currVtxId = int(reObj.group(2))

		for infIdx in xrange(len(currInfs)):
			currWgtVals = resultList[ix][infIdx]
			wlAttr = '%s.weightList[%s].weights[%s]' % (currSkc, currVtxId, infIdx)
			mc.setAttr(wlAttr, currWgtVals)

		mc.skinPercent(currSkc, vtx, normalize=True )

class SkinTools(object):
	
	def __init__(self):
		self.ui = 'pkSkinTools'
		self.win = '%sWin' % self.ui
		self.paintModeRadioButtons = {
										'%sPaintAddRB'%self.ui: 'Add',
										'%sPaintReplaceRB'%self.ui: 'Replace',
										'%sPaintSmoothRB'%self.ui: 'Smooth',
										'%sPaintScaleRB'%self.ui: 'Scale'
									}
		self.paintUi = 'artAttrSkinContext'
	
	def show(self):
		
		if mc.window(self.win, exists=True):
			mc.deleteUI(self.win)
		
		# mc.window(self.win, t='pkSkinTools', rtf=True)
		mc.window(self.win, t='pkSkinTools')
		mc.columnLayout('%sMainCL'%self.ui, adj=True)
		
		mc.button(l='Curves', c=partial(self.toggleVis, 'nurbsCurves'))
		mc.button(l='Surfaces', c=partial(self.toggleVis, 'nurbsSurfaces'))
		mc.button(l='Polygons', c=partial(self.toggleVis, 'polymeshes'))
		mc.button(l='Joints', c=partial(self.toggleVis, 'joints'))
		mc.button(l='Grid', c=partial(self.toggleVis, 'grid'))
		mc.button(l='WireShd', c=partial(self.toggleVis, 'wos'))
		
		mc.separator()
		
		mc.button(l='Paint', c=partial(self.paintTool), h=50)
		
		mc.separator()
		
		w = 100
		mc.rowLayout(nc=2, cw2=[w,w])
		mc.button(l='Mirrored Vtcs', w=w, c=partial(self.selectMirrorVertices))
		mc.button(l='Mirror', w=w, c=partial(self.mirrorSkin))
		
		mc.setParent('..')
		mc.separator()

		mc.button(l='Affected Vtcs', w=w, c=partial(self.selectAffectedVertices))
		mc.separator()
		
		
		mc.floatField('%sValueFF'%self.ui, changeCommand=partial(self.paintValueChange))
		mc.popupMenu()
		for ix in (0, 0.01, 0.05, 0.1, 0.5, 0.98, 1):
			mc.menuItem(l=str(ix), c=partial(self.setValueFloatField, ix))
		
		# mc.setParent('..')
		
		mc.radioCollection('%sPaintRCL'%self.ui)
		for key in self.paintModeRadioButtons.keys():
			mc.radioButton(
								key,
								label=self.paintModeRadioButtons[key],
								changeCommand=partial(self.setOperation)
							)
		
		mc.radioButton(self.paintModeRadioButtons.keys()[0], e=True, sl=True)
		
		mc.button(l='Flood', h=50, c=partial(self.flood))
		
		mc.separator()

		w = 100
		mc.rowLayout(nc=2, cw2=[w,w])
		mc.button(l='Hold', w=w, c=partial(self.hold, True))
		mc.button(l='Unhold', w=w, c=partial(self.hold, False))

		mc.setParent('..')

		mc.separator()
		mc.button(l='Transfer', h=50, c=partial(self.transferWeight))
		
		mc.showWindow(self.win)
		mc.window(self.win, e=True, w=200)
		mc.window(self.win, e=True, h=420)

	def selectAffectedVertices(self, arg=None):

		# Select geometry then joints.
		sels = mc.ls(sl=True)

		vtcs = []

		geoTypes = ('mesh', 'nurbsCurve', 'nurbsSurface')
		geos = []
		infJnts = []

		for sel in sels:

			if mc.nodeType(sel) == 'joint':
				infJnts.append(sel)
				continue

			shp = mc.listRelatives(sel, s=True)

			if shp:
				if mc.nodeType(shp[0]) in 'mesh':
					geos.append(sel)

		for geo in geos:

			skc = mm.eval('findRelatedSkinCluster %s' % geo)

			for jnt in infJnts:
				
				mc.select(cl=True)
				mc.skinCluster(skc, e=True, selectInfluenceVerts=jnt)
				currSels = mc.ls(sl=True, fl=True)

				for sel in currSels:
					currVal = mc.skinPercent(skc, sel, transform=jnt, query=True)
					if not sel in vtcs:
						vtcs.append(sel)

		mc.select(vtcs, r=True)
	
	def transferWeight(self, arg=None):

		# Select a geometry then select influences.
		# Skin weight values in the selected influences will be transfered to the last selected influence.

		sels = mc.ls(sl=True)

		geo = sels[0]
		infJnts = sels[1:-1]
		targetJnt = sels[-1]
		
		skn = mm.eval('findRelatedSkinCluster %s' % geo)
		infs = mc.skinCluster(skn, q = True, inf = True)

		if not targetJnt in infs:
			mc.skinCluster(skn, e=True, ai=targetJnt)
			print 'Added %s to %s' % (targetJnt, skn)
			infs = mc.skinCluster(skn, q = True, inf = True)

		mc.setAttr('%s.normalizeWeights' % skn, False)

		for infJnt in infJnts:
			
			mc.select(cl=True)

			if infJnt in infs:
				mc.skinCluster(skn, e=True, selectInfluenceVerts=infJnt)
				affectedVtcs = mc.ls(sl=True, fl=True)

				if affectedVtcs:

					for vtx in affectedVtcs:
						mc.select(vtx, r=True)
						skinVal = mc.skinPercent(skn, vtx, q = True, v = True)

						weightDict = dict(zip(infs, skinVal))

						currSkinVal = weightDict[infJnt]
						targetSkinVal = weightDict[targetJnt]

						mc.skinPercent(skn, vtx, transformValue=[(infJnt, 0)])
						mc.skinPercent(skn, vtx, transformValue=[(targetJnt, targetSkinVal + currSkinVal)])
				else:
					print '%s has no effected vertices.' % infJnt

		mc.setAttr('%s.normalizeWeights' % skn, True)

		mc.select(sels, r=True)

	def setValueFloatField(self, val=0.0, arg=None):
		# Set value float field's value
		mc.floatField('%sValueFF'%self.ui, e=True, v=val)
		self.paintValueChange()
	
	def paintValueChange(self, arg=None):
		# Float field's change command
		if mc.artAttrSkinPaintCtx(self.paintUi, exists=True):
			
			val = mc.floatField('%sValueFF'%self.ui, q=True, v=True)
			mc.artAttrSkinPaintCtx(self.paintUi, e=True, value=val)
	
	def getJointFromSelectedObjects(self):
		# Returns the first joint that was found in selected objects
		sels = mc.ls(sl=True)
		jnt = None
		
		for sel in sels:
			if mc.nodeType(sel) == 'joint':
				jnt = sel
		
		return jnt
	
	def hold(self, hold=True, arg=None):
		# Hold/Unhold weight influences
		# If object is selected, script will hold/unhold all of object's influences.
		# If joint is selected, script will hold/unhold selected joint.
		sels = mc.ls(sl=True)

		jnts = []
		meshes = []

		for sel in sels:
			if mc.nodeType(sel) == 'joint':
				jnts.append(sel)
			elif mc.nodeType(sel) == 'transform':
				meshes.append(sel)

		if not meshes:
			for jnt in jnts:
				mc.setAttr('%s.liw' % sel, hold)

		if not jnts:
			skn = mm.eval('findRelatedSkinCluster %s' % sels[0])
			infs = mc.skinCluster(skn, q=True, inf=True)

			for inf in infs:
				mc.setAttr('%s.liw' % inf, hold)

		if jnts and meshes:

			skn = mm.eval('findRelatedSkinCluster %s' % sels[0])
			infs = mc.skinCluster(skn, q=True, inf=True)

			for inf in infs:
				if inf in jnts:
					mc.setAttr('%s.liw' % inf, hold)
				else:
					mc.setAttr('%s.liw' % inf, not hold)
	
	def flood(self, arg=None):

		currState = self.getOperation()

		if currState == 'Smooth':
			smoothSelected()
			return True
		
		jnt = self.getJointFromSelectedObjects()
		
		if jnt:
			# A joint and components are selected
			
			sels = mc.ls(sl=True, fl=True)
			sels.remove(jnt)
			val = mc.floatField('%sValueFF'%self.ui, q=True, v=True)

			mc.select(sels, r=True)
			mm.eval('PolySelectConvert 3;')
			vtcs = mc.ls(sl=True, fl=True)
			
			for vtx in vtcs:
				# Selected infos
				geo = vtx.split('.')[0]
				skn = mm.eval('findRelatedSkinCluster %s' % geo)
				infs = mc.skinCluster(skn, q=True, inf=True)
				wghtVals = mc.skinPercent(skn, vtx, q=True, v=True)
				
				for ix in range(len(infs)):
					
					if infs[ix] == jnt:
						
						self.assignWeightToVtx(
													val=val,
													vtx = vtx,
													skn=skn,
													inf=infs[ix],
													wghtVal=wghtVals[ix]
												)

			mc.select(sels, r=True)
			mc.select(jnt, add=True)
			
		else:
			# No joint is selected
			mc.artAttrSkinPaintCtx(self.paintUi, e=True, clear=True)
	
	def assignWeightToVtx(self, vtx='', val=0, skn='', inf='', wghtVal=0.0):
		# Get the current operation.
		# Assign calculated weight value to the input vertex regarding the current operation.
		currState = self.getOperation()
						
		if currState == 'Add':
			newVal = wghtVal + val
			mc.skinPercent(skn, vtx, transformValue=[(inf, newVal)])
		
		elif currState == 'Replace':
			newVal = val
			mc.skinPercent(skn, vtx, transformValue=[(inf, newVal)])
		
		elif currState == 'Scale':
			newVal = wghtVal * val 
			mc.skinPercent(skn, vtx, transformValue=[(inf, newVal)])
	
	def getOperation(self, arg=None):
		# Get current state of paint operation
		key = mc.radioCollection('%sPaintRCL'%self.ui, q=True, sl=True)
		return self.paintModeRadioButtons[key]
	
	def setOperation(self, arg=None):
		# Will be triggered when the operation is changed.
		currState = self.getOperation()

		if currState == 'Scale':
			self.setValueFloatField(0.98)

		# Set state of paint operation
		if mc.currentCtx() == 'artAttrSkinContext':
			mm.eval('artAttrPaintOperation artAttrCtx %s;' % currState)
			mm.eval('artUpdateStampProfile solid artAttrCtx;')
			mm.eval('artAttrCtx -e -opacity 1 %s;' % self.paintUi)
	
	def paintTool(self, arg=None):
		# Call paint skin weight tool
		mm.eval('ArtPaintSkinWeightsToolOptions')
		
		self.setOperation()
		self.paintValueChange()
	
	def selectMirrorVertices(self, arg=None):
		# Select the vertices that has tx greater than offset or less than -offset
		sels = mc.ls(sl=True)
		offset = 0.0005
		vtcs = []
		
		for sel in sels:
			
			num = mc.polyEvaluate(sel, v=True)
			
			for ix in range(num):
				
				vtx = '%s.vtx[%s]' % (sel, ix)
				vtxPos = mc.xform(vtx, q=True, t=True, ws=True)
				
				if (vtxPos[0] > offset) or (vtxPos[0] < -offset):
					vtcs.append(vtx)
		
		mc.select(vtcs, r=True)
	
	def mirrorSkin(self, arg=None):
		
		geos = []
		
		for sel in mc.ls(sl=True):
			
			currGeo = sel.split('.')[0]
			if not currGeo in geos:
				geos.append(currGeo)
		
		for geo in geos:
			
			skin = mm.eval('findRelatedSkinCluster %s' % geo)
			
			mc.copySkinWeights(ss=skin, ds=skin, mirrorMode='YZ', surfaceAssociation='closestPoint', influenceAssociation='closestJoint')
	
	def toggleVis(self, elem='', arg=None):
		# Toggle visibility
		currPanel = mc.getPanel(wf=True)
		
		if elem == 'nurbsCurves':
			
			currState = mc.modelEditor(currPanel, q=True, nurbsCurves=True)
			mc.modelEditor(currPanel, e=True, nurbsCurves=not(currState))
			
		elif elem == 'nurbsSurfaces':
			
			currState = mc.modelEditor(currPanel, q=True, nurbsSurfaces=True)
			mc.modelEditor(currPanel, e=True, nurbsSurfaces=not(currState))
			
		elif elem == 'polymeshes':
			
			currState = mc.modelEditor(currPanel, q=True, polymeshes=True)
			mc.modelEditor(currPanel, e=True, polymeshes=not(currState))
			
		elif elem == 'joints':
			
			currState = mc.modelEditor(currPanel, q=True, joints=True)
			mc.modelEditor(currPanel, e=True, joints=not(currState))
			
		elif elem == 'grid':
			
			currState = mc.modelEditor(currPanel, q=True, grid=True)
			mc.modelEditor(currPanel, e=True, grid=not(currState))
			
		elif elem == 'wos':
			
			currState = mc.modelEditor(currPanel, q=True, wos=True)
			mc.modelEditor(currPanel, e=True, wos=not(currState))
		

