import maya.cmds as mc

import lpRig.core as lrc
reload(lrc)
import lpRig.rigTools as lrr
reload(lrr)

class AimRig(object):

	def __init__(
					self,
					parent='',
					ctrlGrp='',
					rootJnt='',
					endJnt='',
					parentRoot='',
					parentEnd='',
					part='',
					side='',
					aimAx='y+',
					upAx='x+'
				):

		'''
		Base class for aim rig.
		Controller will be created at the position of 'rootJnt'
		and will be parented to 'parentRoot'.
		Target will be created at the position of 'endJnt'
		and will be parented to 'parentEnd'.
		'Ctrl_Grp' will be parented to 'parent'.
		'''

		if side:
			side = '_%s_' % side
		else:
			side = '_'

		self.vecDict = {
							'x+': (1, 0, 0),
							'x-': (-1, 0, 0),
							'y+': (0, 1, 0),
							'y-': (0, -1, 0),
							'z+': (0, 0, 1),
							'z-': (0, 0, -1)
						}
		self.jntLen = lrc.distance(rootJnt, endJnt)

		# Main group
		self.Ctrl_Grp = lrc.Null()
		self.Ctrl_Grp.name = '%sAimCtrl%sGrp' % (part, side)

		mc.parentConstraint(parent, self.Ctrl_Grp)
		mc.scaleConstraint(parent, self.Ctrl_Grp)

		self.Ctrl_Grp.parent(ctrlGrp)

		self.Root_Ctrl = lrr.jointControl('sphere')
		self.RootGmbl_Ctrl = lrc.addGimbal(self.Root_Ctrl)
		self.RootCtrlAim_Grp = lrc.group(self.Root_Ctrl)
		self.RootCtrlZr_Grp = lrc.group(self.RootCtrlAim_Grp)

		self.Root_Ctrl.name = '%sAimRoot%sCtrl' % (part, side)
		self.Root_Ctrl .color = 'blue'
		self.RootGmbl_Ctrl.name = '%sAimRootGmbl%sCtrl' % (part, side)
		self.RootCtrlAim_Grp.name = '%sAimRootCtrlAim%sGrp' % (part, side)
		self.RootCtrlZr_Grp.name = '%sAimRootCtrlZr%sGrp' % (part, side)
		# self.RootCtrlAimOri_Grp.name = '%sAimRootCtrlAimOri%sGrp' % (part, side)

		if side == '_R_':
			self.Root_Ctrl.color = 'darkBlue'
		else:
			self.Root_Ctrl.color = 'darkRed'

		self.RootCtrlZr_Grp.snap(rootJnt)
		self.Root_Ctrl.lockHideAttrs('v')
		self.RootCtrlZr_Grp.parent(self.Ctrl_Grp)

		# Target
		self.Trg_Grp = lrc.Null()
		self.TrgZr_Grp = lrc.group(self.Trg_Grp)
		
		self.Trg_Grp.name = '%sAimTrg%sGrp' % (part, side)
		self.TrgZr_Grp.name = '%sAimTrgZr%sGrp' % (part, side)
		
		self.TrgZr_Grp.snap(parentEnd)
		self.Trg_Grp.snap(endJnt)
		self.TrgZr_Grp.parent(self.Ctrl_Grp)
		mc.parentConstraint(parentEnd, self.TrgZr_Grp)

		# Up Controller
		self.Up_Ctrl = lrc.Control('plus')
		self.UpCtrlZr_Grp = lrc.group(self.Up_Ctrl)

		self.Up_Ctrl.name = '%sAimUp%sCtrl' % (part, side)
		self.UpCtrlZr_Grp.name = '%sAimUpCtrlZr%sGrp' % (part, side)

		self.Up_Ctrl.lockHideAttrs('rx', 'ry', 'rz', 'sx', 'sy', 'sz', 'v')
		self.Up_Ctrl.color = 'blue'

		self.UpCtrlZr_Grp.parent(rootJnt)
		self.UpCtrlZr_Grp.attr('t').v = (0, 0, 0)

		if upAx[-1] == '+':
			self.UpCtrlZr_Grp.attr('t%s' % upAx[0]).v = self.jntLen
		else:
			self.UpCtrlZr_Grp.attr('t%s' % upAx[0]).v = -self.jntLen

		self.UpCtrlZr_Grp.parent(self.Ctrl_Grp)

		# Local/World
		# self.Root_Ctrl.add(ln='localWorld', min=0, max=1, dv=1, k=True)
		# parCon = lrc.parentConstraint(parent, parentRoot, self.UpCtrlZr_Grp, mo=True)

		# self.UpCtrlLocWor_Rev = lrc.Reverse()
		# self.UpCtrlLocWor_Rev.name = '%sUpCtrlLocWor%sRev' % (part, side)

		# self.Root_Ctrl.attr('localWorld') >> self.UpCtrlLocWor_Rev.attr('ix')
		# self.Root_Ctrl.attr('localWorld') >> parCon.attr('w1')
		# self.UpCtrlLocWor_Rev.attr('ox') >> parCon.attr('w0')

		# Up Curve
		(self.UpCtrl_Crv,
		self.UpCtrl1_Cls,
		self.UpCtrl2_Cls) = lrr.crvGuide(self.Up_Ctrl, rootJnt)

		self.UpCtrl_Crv.name = '%sAimUp%sCrv' % (part, side)
		self.UpCtrl1_Cls.name = '%sAimRootUp%sCls' % (part, side)
		self.UpCtrl2_Cls.name = '%sAimEndUp%sCls' % (part, side)

		lrr.renameClsElms(self.UpCtrl1_Cls)
		lrr.renameClsElms(self.UpCtrl2_Cls)

		self.UpCtrl_Crv.attr('inheritsTransform').value = 0
		self.UpCtrl_Crv.attr('overrideEnabled').value = 1
		self.UpCtrl_Crv.attr('overrideDisplayType').value = 2

		self.UpCtrl_Crv.parent(self.UpCtrlZr_Grp)
		self.UpCtrl_Crv.attr('t').value = (0, 0, 0)

		Root_CtrlShp = lrc.Dag(self.Root_Ctrl.shape)
		Root_CtrlShp.add(ln='upCtrlVis', min=0, max=1, k=True)
		Root_CtrlShp.attr('upCtrlVis') >> self.UpCtrlZr_Grp.attr('v')
		
		lrc.aimConstraint(
							self.Trg_Grp,
							self.RootCtrlAim_Grp,
							aim=self.vecDict[aimAx],
							u=self.vecDict[upAx],
							wut='object',
							wuo=self.Up_Ctrl,
						)

		# Connect to joint
		mc.parentConstraint(self.RootGmbl_Ctrl, rootJnt, mo=True)
		mc.scaleConstraint(self.RootGmbl_Ctrl, rootJnt, mo=True)

class SuspensionRig(object):

	def __init__(
					self,
					ctrlGrp='',
					rootJnt='',
					endJnt='',
					parentRoot='',
					parentEnd='',
					part='',
					side='',
					aimAx='y+',
					upAx='x+'
				):
		
		'''
		Instance two AimRigs to create the suspension rig.
		This module doesn't need 'parent' like the others.
		'parentRoot' will become 'parent' to 'rootRig'
		and 'parentEnd' will become 'parent' to 'endRig'.

		'''
		if side:
			side = '_%s_' % side
		else:
			side = '_'
		
		self.inverseVecDict = {
									'x+': 'x-',
									'x-': 'x+',
									'y+': 'y-',
									'y-': 'y+',
									'z+': 'z-',
									'z-': 'z+'
								}
		
		# Main groupr
		self.Ctrl_Grp = lrc.Null()
		self.Ctrl_Grp.name = '%sSuspRig%sGrp' % (part, side)
		self.Ctrl_Grp.parent(ctrlGrp)

		self.rootRig = AimRig(
								parentRoot,
								ctrlGrp,
								rootJnt,
								endJnt,
								parentRoot,
								parentEnd,
								'%sSuspRoot' % part,
								side.replace('_', ''),
								aimAx,
								upAx
							)

		self.endRig = AimRig(
								parentEnd,
								ctrlGrp,
								endJnt,
								rootJnt,
								parentEnd,
								parentRoot,
								'%sSuspEnd' % part,
								side.replace('_', ''),
								self.inverseVecDict[aimAx],
								upAx
							)
		
		self.rootRig.Ctrl_Grp.parent(self.Ctrl_Grp)
		self.endRig.Ctrl_Grp.parent(self.Ctrl_Grp)

class NonRollRig(AimRig):

	def __init__(
					self,
					parent='',
					ctrlGrp='',
					rootJnt='',
					endJnt='',
					parentRoot='',
					parentEnd='',
					part='',
					side='',
					aimAx='y+',
					upAx='x+'
				):

		super(NonRollRig, self).__init__(
											parent,
											ctrlGrp,
											rootJnt,
											endJnt,
											parentRoot,
											parentEnd,
											part,
											side,
											aimAx,
											upAx
										)

		if side:
			side = '_%s_' % side
		else:
			side = '_'

		# End controller
		self.End_Ctrl = lrr.jointControl('sphere')
		self.EndGmbl_Ctrl = lrc.addGimbal(self.End_Ctrl)
		self.EndCtrlOff_Grp = lrc.group(self.End_Ctrl)
		self.EndCtrlZr_Grp = lrc.group(self.EndCtrlOff_Grp)

		self.End_Ctrl.name = '%sAimEnd%sCtrl' % (part, side)
		self.End_Ctrl .color = 'blue'
		self.EndGmbl_Ctrl.name = '%sAimEndGmbl%sCtrl' % (part, side)
		self.EndCtrlOff_Grp.name = '%sAimEndCtrlOff%sGrp' % (part, side)
		self.EndCtrlZr_Grp.name = '%sAimEndCtrlZro%sGrp' % (part, side)

		self.EndCtrlZr_Grp.snapPoint(endJnt)
		self.EndCtrlZr_Grp.snapOrient(rootJnt)
		self.EndCtrlZr_Grp.parent(self.RootGmbl_Ctrl)
		self.EndCtrlOff_Grp.snapOrient(endJnt)
		self.End_Ctrl.lockHideAttrs('v')
		# mc.pointConstraint(self.Trg_Grp, self.EndCtrlZr_Grp)

		if side == '_R_':
			self.End_Ctrl.color = 'darkBlue'
		else:
			self.End_Ctrl.color = 'darkRed'

		# Connect to joint
		mc.parentConstraint(self.EndGmbl_Ctrl, endJnt, mo=True)
		mc.scaleConstraint(self.EndGmbl_Ctrl, endJnt, mo=True)

		ofstVal = mc.getAttr('%s.t%s' % (self.EndCtrlZr_Grp, aimAx[0]))
		self.Strt_Add = lrc.AddDoubleLinear()
		self.Strt_Add.name = '%sNnRlStrt%sAdd' % (part, side)
		self.Strt_Add.add(ln='default', k=True)
		self.Strt_Add.attr('default') >> self.Strt_Add.attr('i1')
		
		self.Strt_Mul = lrc.MultDoubleLinear()
		self.Strt_Mul.name = '%sNnRlStrt%sMul' % (part, side)
		self.Strt_Mul.add(ln='default', dv=ofstVal, k=True)
		self.Strt_Mul.attr('default') >> self.Strt_Mul.attr('i1')
		self.Strt_Mul.attr('o') >> self.Strt_Add.attr('i2')
		
		self.StrtAmp_Mul = lrc.MultDoubleLinear()
		self.StrtAmp_Mul.name = '%sNonRlStrtAmp%sMul' % (part, side)
		self.StrtAmp_Mul.add(ln='amp', dv=0.1, k=True)
		self.StrtAmp_Mul.attr('amp') >> self.StrtAmp_Mul.attr('i1')
		
		self.Root_Ctrl.add(ln='stretch', k=True)
		self.Root_Ctrl.attr('stretch') >> self.StrtAmp_Mul.attr('i2')
		self.StrtAmp_Mul.attr('o') >> self.Strt_Mul.attr('i2')

		self.Strt_Add.attr('o') >> self.EndCtrlOff_Grp.attr('t%s'%aimAx[0])

		# Disable segment scale compensation
		mc.setAttr('%s.segmentScaleCompensate' % rootJnt, 0)
		mc.setAttr('%s.segmentScaleCompensate' % endJnt, 0)



