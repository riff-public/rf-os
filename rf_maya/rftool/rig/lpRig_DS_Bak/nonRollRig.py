import maya.cmds as mc

import lpRig.core as lrc
reload(lrc)
import lpRig.rigTools as lrr
reload(lrr)

class NonRollRig(object):

	def __init__(
					self,
					ctrlGrp='',
					jntGrp='',
					ikhGrp='',
					parent='',
					rootPar='',
					tipPar='',
					rootJnt='',
					tipJnt='',
					part='',
					side=''
				):

		if side:
			side = '_%s_' % side
		else:
			side = '_'

		# Color
		col = 'yellow'
		if side == '_R_':
			col = 'blue'
		else:
			col = 'red'

		# Main groups
		self.Ctrl_Grp = lrc.Null()
		self.Ctrl_Grp.name = '{0}Ctrl{1}Grp'.format(part, side)
		mc.parentConstraint(parent, self.Ctrl_Grp)
		mc.scaleConstraint(parent, self.Ctrl_Grp)
		self.Ctrl_Grp.parent(ctrlGrp)

		self.Jnt_Grp = lrc.Null()
		self.Jnt_Grp.name = '{0}Jnt{1}Grp'.format(part, side)
		mc.parentConstraint(parent, self.Jnt_Grp)
		mc.scaleConstraint(parent, self.Jnt_Grp)
		self.Jnt_Grp.parent(jntGrp)

		# Non-Roll joints
		(self.Nr_Jnt,
		self.NrJntZr_Grp,
		self.NrTip_Jnt,
		self.Nr_Ikh,
		self.NrIkhZr_Grp) = lrr.createNonRollJointChain(
															parent,
															rootPar,
															tipPar,
															part,
															side
														)
		self.NrJntZr_Grp.parent(self.Jnt_Grp)
		self.NrIkhZr_Grp.parent(ikhGrp)

		# Ctrl
		self.Root_Ctrl = lrr.jointControl('square')
		self.Root_Ctrl.name = '{0}Root{1}Ctrl'.format(part, side)
		self.Root_Ctrl.lockHideAttrs('v')
		self.Root_Ctrl.rotateOrder = 'yxz'
		self.Root_Ctrl.color = col

		self.RootCtrlZr_Grp = lrc.group(self.Root_Ctrl)
		self.RootCtrlZr_Grp.name = '{0}RootCtrlZr{1}Grp'.format(part, side)

		self.RootGmbl_Ctrl = lrc.addGimbal(self.Root_Ctrl)
		self.RootGmbl_Ctrl.name = '{0}RootGmbl{1}Ctrl'.format(part, side)
		self.RootGmbl_Ctrl.rotateOrder = 'yxz'

		self.RootCtrlZr_Grp.snap(rootJnt)

		mc.parentConstraint(self.RootGmbl_Ctrl, rootJnt)
		mc.scaleConstraint(self.RootGmbl_Ctrl, rootJnt)
		mc.setAttr('{0}.ssc'.format(rootJnt), 0)
		mc.setAttr('{0}.ssc'.format(tipJnt), 0)

		self.RootCtrlZr_Grp.parent(self.Ctrl_Grp)

		# Non-Roll rig
		self.Root_Ctrl.add(ln='nonRoll', min=0, max=1, dv=1, k=True)

		parCon = lrc.parentConstraint(rootPar, self.Nr_Jnt, self.RootCtrlZr_Grp, mo=True)

		self.Nr_Rev = lrc.Reverse()
		self.Nr_Rev.name = '{0}NonRoll{1}Rev'.format(part, side)

		self.Root_Ctrl.attr('nonRoll') >> parCon.attr('w1')
		self.Root_Ctrl.attr('nonRoll') >> self.Nr_Rev.attr('ix')
		self.Nr_Rev.attr('ox') >> parCon.attr('w0')