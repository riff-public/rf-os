import maya.cmds as mc
import maya.mel as mm

import lpRig.core as lrc
import lpRig.rigTools as lrr

class ThreadRig(object):

	def __init__(
					self,
					parent='Root_Jnt',
					ctrlGrp='',
					skinGrp='',
					stillGrp='',
					nrb='',
					no=100,
					part='',
					side=''
				):

		# Side definition
		if side:
			side = '_%s_' % side
		else:
			side = '_'

		# Color difinition
		col = 'yellow'
		darkCol = 'softYellow'
		if side == '_R_':
			col = 'blue'
			darkCol = 'darkBlue'
		elif side == '_L_':
			col = 'red'
			darkCol = 'darkRed'

		# Main group
		self.Ctrl_Grp = lrc.Null()
		self.Ctrl_Grp.name = 'Thd%sCtrl%sGrp' % (part, side)
		if ctrlGrp:
			self.Ctrl_Grp.parent(ctrlGrp)

		self.Skin_Grp = lrc.Null()
		self.Skin_Grp.name = 'Thd%sSkin%sGrp' % (part, side)
		if skinGrp:
			self.Skin_Grp.parent(skinGrp)

		self.Still_Grp = lrc.Null()
		self.Still_Grp.name = 'Thd%sStill%sGrp' % (part, side)
		if stillGrp:
			self.Still_Grp.parent(stillGrp)

		if parent:
			mc.parentConstraint(parent, self.Ctrl_Grp)
			mc.scaleConstraint(parent, self.Ctrl_Grp)

		# Thread NURBs
		self.Thd_Nrb = lrc.Dag(nrb)
		self.Thd_Nrb.name = 'Thd%s%sNrb' % (part, side)
		self.Thd_Nrb.parent(self.Still_Grp)

		self.Thd_Nrb.add(ln='drive', k=True)

		self.DriveAmp_Mdv = lrc.MultiplyDivide()
		self.DriveAmp_Mdv.name = 'Thd%sDriveAmp%sNrb' % (part, side)
		self.DriveAmp_Mdv.attr('i2x').v = 0.1

		self.Thd_Nrb.attr('drive') >> self.DriveAmp_Mdv.attr('i1x')

		self.Off_Add = lrc.AddDoubleLinear()
		self.Off_Add.name = 'Thd%sOff%sAdd' % (part, side)
		self.Off_Add.add(ln='default', dv=1000, k=True)

		self.Off_Add.attr('default') >> self.Off_Add.attr('i1')
		self.DriveAmp_Mdv.attr('ox') >> self.Off_Add.attr('i2')

		for ix in range(no):
			
			par = 1.000/float(no) * float(ix)
			idx = ix+1
			
			# POS
			pos = lrc.Null()
			pos.name = 'Thd%sPos_%s%sGrp' % (part, idx, side)

			posi = lrc.PointOnSurfaceInfo()
			posi.name = 'Thd%s_%s%sPosi' % (part, idx, side)

			self.Thd_Nrb.attr('worldSpace[0]') >> posi.attr('is')
			posi.attr('position') >> pos.attr('t')
			posi.attr('parameterV').v = 0.5
			posi.attr('turnOnPercentage').v = 1

			aimCon = lrc.AimConstraint()
			aimCon.name = '%s_aimConstraint' % pos
			aimCon.attr('a').v = (0, 1, 0)
			aimCon.attr('u').v = (1, 0, 0)

			posi.attr('n') >> aimCon.attr('wu')
			posi.attr('tu') >> aimCon.attr('tg[0].tt')
			aimCon.attr('cr') >> pos.attr('r')

			aimCon.parent(pos)
			aimCon.attr('t').v = (0, 0, 0)
			aimCon.attr('r').v = (0, 0, 0)
			pos.parent(self.Still_Grp)
			
			# Expression
			expNm = 'Thd%sPos_%s%sExp' % (part, idx, side)
			
			exp = 'float $input = {amp}.o + {par};\n'.format(amp=self.Off_Add.name, par=str(par))
			exp += '{obj}.parameterU = $input % 1;'.format(obj=posi.name)
			
			mc.expression(s=exp, n=expNm)
			
			# Skin joint
			skinJnt = lrc.Joint()
			skinJntZrGrp = lrc.group(skinJnt)

			skinJnt.name = 'Thd%s_%s%sJnt' % (part, idx, side)
			skinJntZrGrp.name = 'ThdJntZr%s_%s%sGrp' % (part, idx, side)

			mc.parentConstraint(pos, skinJntZrGrp)
			skinJntZrGrp.parent(self.Skin_Grp)
