import maya.cmds as mc

import lpRig.core as lrc
reload(lrc)
import lpRig.rigTools as lrr
reload(lrr)

class ScapRig(object):

	def __init__(
					self,
					parent='',
					ctrlGrp='',
					ax='y',
					tmpJnt=[
								'Scap_L_Jnt',
								'ScapTip_L_Jnt'
							],
					part='',
					side='L'
				):

		if side:
			side = '_%s_' % side
		else:
			side = '_'

		# Skin joints
		self.Scap_Jnt = lrc.Dag(tmpJnt[0])
		self.ScapTip_Jnt = lrc.Dag(tmpJnt[1])
		
		self.ScapSca_Jnt = lrr.jointAt(self.Scap_Jnt)
		self.ScapSca_Jnt.name = "ScapSca%s%sJnt" % (part, side)
		self.ScapSca_Jnt.attr('ssc').v = 0
		self.ScapSca_Jnt.parent(self.Scap_Jnt)
		
		# Main Group
		self.Ctrl_Grp = lrc.Null()
		self.Ctrl_Grp.name = "ScapCtrl%s%sGrp" % (part, side)
		
		mc.parentConstraint(parent, self.Ctrl_Grp)
		
		# Controller
		self.Scap_Ctrl = lrr.jointControl('circle')
		self.Scap_Ctrl.name = "Scap%s%sCtrl" % (part, side)
		self.Scap_Ctrl.lockHideAttrs('v')
		self.Scap_Ctrl.rotateOrder = 'xyz'
		
		self.ScapCtrlZr_Grp = lrc.group(self.Scap_Ctrl)
		self.ScapCtrlZr_Grp.name = "ScapCtrlZr%s%sGrp" % (part, side)

		# Controller - Color
		if side == '_R_':
			self.Scap_Ctrl.color = 'blue'
		else:
			self.Scap_Ctrl.color = 'red'
		
		# Controller - Positioning
		self.ScapCtrlZr_Grp.snapPoint(self.Scap_Jnt)
		self.Scap_Ctrl.snapOrient(self.Scap_Jnt)
		self.Scap_Ctrl.freeze(r=True, t=False, s=False)
		self.ScapCtrlZr_Grp.parent(self.Ctrl_Grp)
		
		self.ScapGmbl_Ctrl = lrc.addGimbal(self.Scap_Ctrl)
		self.ScapGmbl_Ctrl.name = "ScapGmbl%s%sCtrl" % (part, side)
		self.ScapGmbl_Ctrl.rotateOrder = 'xyz'
		
		# Stretch setup
		(self.ScapStrt_Add,
		self.ScapStrt_Mul) = lrr.fkStretch(
												ctrl=self.Scap_Ctrl,
												target=self.ScapTip_Jnt,
												ax=ax
											)

		self.ScapStrt_Add.name = "ScapStrt%s%sAdd" % (part, side)
		self.ScapStrt_Mul.name = "ScapStrt%s%sMul" % (part, side)

		# Connect to joint
		mc.parentConstraint(self.ScapGmbl_Ctrl, self.Scap_Jnt)
		mc.scaleConstraint(self.ScapGmbl_Ctrl, self.ScapSca_Jnt)

		# Grouping
		self.Ctrl_Grp.parent(ctrlGrp)