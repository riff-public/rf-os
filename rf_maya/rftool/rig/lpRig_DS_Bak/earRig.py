import maya.cmds as mc

import lpRig.core as lrc
reload(lrc)
import lpRig.rigTools as lrr
reload(lrr)

class EarRig(object):

	def __init__(
					self,
					tmpLftLocs=[],
					tmpRgtLocs=[],
					part=''
				):

		# Main Groups
		self.Ctrl_Grp = lrc.Null()
		self.Ctrl_Grp.name = 'EarRig{part}Ctrl_Grp'.format(part=part)

		self.Skin_Grp = lrc.Null()
		self.Skin_Grp.name = 'EarRig{part}Skin_Grp'.format(part=part)

		# Left Ear
		self.LeftEar = EarSideRig(tmpLftLocs, 'L', 'EarRig%s' % part)
		self.LeftEar.Ctrl_Grp.parent(self.Ctrl_Grp)
		self.LeftEar.Skin_Grp.parent(self.Skin_Grp)

		# Right Ear
		self.RightEar = EarSideRig(tmpRgtLocs, 'R', 'EarRig%s' % part)
		self.RightEar.Ctrl_Grp.parent(self.Ctrl_Grp)
		self.RightEar.Skin_Grp.parent(self.Skin_Grp)

class EarSideRig(object):

	def __init__(
					self,
					tmpLocs=[],
					side='',
					part=''
				):

		if side:
			side = '_%s_' % side
		else:
			side = '_'

		col = 'yellow'
		if side == '_L_':
			col = 'red'
		elif side == '_R_':
			col = 'blue'

		# Main Groups
		self.Ctrl_Grp = lrc.Null()
		self.Ctrl_Grp.name = '{part}Ctrl{side}Grp'.format(part=part, side=side)

		self.Skin_Grp = lrc.Null()
		self.Skin_Grp.name = '{part}Skin{side}Grp'.format(part=part, side=side)
		
		self.Jnts = []
		self.JntZrGrps = []
		self.Ctrls = []
		self.CtrlZrGrps = []

		for ix, tmpLoc in enumerate(tmpLocs):

			idx = ix + 1

			jnt = lrc.Joint()
			jnt.name = '{part}_{idx}{side}Jnt'.format(part=part, idx=idx, side=side)

			jntZr = lrc.group(jnt)
			jntZr.name = '{part}JntZr_{idx}{side}Grp'.format(part=part, idx=idx, side=side)

			jntZr.snap(tmpLoc)

			ctrl = lrc.Control('nrbCircle')
			ctrl.color = col
			ctrl.lockHideAttrs('v')
			ctrl.name = '{part}_{idx}{side}Ctrl'.format(part=part, idx=idx, side=side)

			ctrlZr = lrc.group(ctrl)
			ctrlZr.name = '{part}CtrlZr_{idx}{side}Grp'.format(part=part, idx=idx, side=side)

			ctrlZr.snap(tmpLoc)

			for attr in ('t', 'r', 's'):
				ctrl.attr(attr) >> jnt.attr(attr)

			self.Jnts.append(jnt)
			self.JntZrGrps.append(jntZr)
			self.Ctrls.append(ctrl)
			self.CtrlZrGrps.append(ctrlZr)

			if not ix: continue

			jntZr.parent(self.Jnts[ix-1])
			ctrlZr.parent(self.Ctrls[ix-1])

		self.CtrlZrGrps[0].parent(self.Ctrl_Grp)
		self.JntZrGrps[0].parent(self.Skin_Grp)

def main():

	earRigObj = EarRig(
							tmpLftLocs=[
											'EarTmp_1_L_Jnt',
											'EarTmp_2_L_Jnt'
										],
							tmpRgtLocs=[
											'EarTmp_1_R_Jnt',
											'EarTmp_2_R_Jnt'
										],
							part=''
						)