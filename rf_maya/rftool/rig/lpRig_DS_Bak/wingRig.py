import maya.cmds as mc
import maya.mel as mm

import lpRig.core as lrc
import lpRig.rigTools as lrr
import lpRig.ribbon as lrb
reload(lrc)
reload(lrr)
reload(lrb)

import armRig

def wingCurve(objA='', objB=''):
	'''
	Create a NURBs curve from objA to objB.
	'''
	posA = mc.xform(objA, q=True, ws=True, t=True)
	posB = mc.xform(objB, q=True, ws=True, t=True)

	crv = lrc.Dag(mc.curve(d=1, p=[posA, posB]))
	
	return crv

def wingTargetCurve(objA='', objB=''):
	'''
	Create a NURBs curve from objA to objB and bind created curve the those objects.
	'''
	crv = wingCurve(objA, objB)
	
	skc = mc.skinCluster(objA, objB, crv, tsb=True)[0]
	mc.skinPercent(skc, '%s.cv[0]' % crv, tv=[objA, 1])
	mc.skinPercent(skc, '%s.cv[1]' % crv, tv=[objB, 1])
	
	return crv

def bindWingCurve(rbnObj=None, wingCurve=None):
	'''
	Bind wing curve to arm ribbon joints.
	'''
	skc = mc.skinCluster(
								rbnObj.RbnRoot_Jnt,
								rbnObj.RbnMid_Jnt,
								rbnObj.RbnEnd_Jnt,
								wingCurve,
								dr=7,
								mi=2, tsb=True
							)[0]
	mc.skinPercent(skc, '%s.cv[0]' % wingCurve, tv=[rbnObj.RbnRoot_Jnt, 1])
	mc.skinPercent(skc, '%s.cv[1]' % wingCurve, tv=[rbnObj.RbnMid_Jnt, 1])
	mc.skinPercent(skc, '%s.cv[2]' % wingCurve, tv=[rbnObj.RbnMid_Jnt, 1])
	mc.skinPercent(skc, '%s.cv[3]' % wingCurve, tv=[rbnObj.RbnEnd_Jnt, 1])
	
	return skc

def fourJointChain(rootJnt=None, endJnt=None, oriJnt=None):
	'''
	Split the given two joints chain to four joints chain.
	'''
	posRoot = lrc.pos(rootJnt)
	posEnd = lrc.pos(endJnt)

	if not oriJnt:
		oriJnt = rootJnt

	tmpJnt = lrc.Joint()
	tmpJnt.snap(rootJnt)
	mc.aimConstraint(endJnt, tmpJnt, aim=(0, 1, 0), u=(1, 0, 0), wut='objectrotation', 
							wuo=oriJnt, wu=(1, 0, 0))
	
	jntVec = lrc.diff(posEnd, posRoot)
	
	posB = lrc.add(posRoot, [jntVec[0]*0.33, jntVec[1]*0.33, jntVec[2]*0.33])
	posC = lrc.add(posRoot, [jntVec[0]*0.66, jntVec[1]*0.66, jntVec[2]*0.66])
	
	jntA = lrc.Joint()
	jntA.attr('t').v = posRoot
	
	jntB = lrc.Joint()
	jntB.attr('t').v = posB

	jntC = lrc.Joint()
	jntC.attr('t').v = posC

	jntD = lrc.Joint()
	jntD.attr('t').v = posEnd

	jntA.snapOrient(tmpJnt)
	jntA.freeze(r=True)
			
	jntB.snapOrient(tmpJnt)
	jntB.freeze(r=True)
	jntB.parent(jntA)

	jntC.snapOrient(tmpJnt)
	jntC.freeze(r=True)
	jntC.parent(jntB)
	
	jntD.snapOrient(tmpJnt)
	jntD.freeze(r=True)
	jntD.parent(jntC)

	mc.delete(tmpJnt)
	return jntA, jntB, jntC, jntD

def grpOnCrv(crv=None, crvLen=0, idx=0, no=0):

	crvShp = lrc.Dag(crv.shape)

	poci = lrc.PointOnCurveInfo()
	poci.attr('turnOnPercentage').v = 1
	crvShp.attr('worldSpace') >> poci.attr('inputCurve')
	poci.attr('parameter').v = ((crvLen/float(no))*float(idx))/crvLen

	pos = lrc.Null()
	poci.attr('result.position') >> pos.attr('t')

	return poci, pos

def fthrRotDstr(fstFthr=None, secFthr=None, tar=None, mult=0.0000):

	fstMult = 1.0000 - mult

	aFstMdv = lrc.MultiplyDivide()
	aFstMdv.attr('i2').v = [fstMult, fstMult, fstMult]
	fstFthr.attr('r') >> aFstMdv.attr('i1')

	aSecMdv = lrc.MultiplyDivide()
	aSecMdv.attr('i2').v = [mult, mult, mult]
	if secFthr:
		secFthr.attr('r') >> aSecMdv.attr('i1')

	aPma = lrc.PlusMinusAverage()
	aFstMdv.attr('o') >> aPma.last3D()
	if secFthr:
		aSecMdv.attr('o') >> aPma.last3D()
	aPma.attr('output3D') >> tar.attr('r')

	return aFstMdv, aSecMdv, aPma

def rigFthrs(mainJnt=None, rootCrv=None, tarCrv=None, fstFthrs=None, 
				secFthrs=None, fthrNo=0, fthrPart='', side='', 
				postGrp=None, tarGrp=None, ctrlGrp=None):
	
	rootCrvLen = mc.arclen(rootCrv)
	tarCrvLen = mc.arclen(tarCrv)
	
	for ix in range(fthrNo):

		# Root
		rootPoci, rootPost = grpOnCrv(rootCrv, rootCrvLen, ix, fthrNo)
		rootPoci.name = '%sRoot_%s%sPoci' % (fthrPart, ix+1, side)
		rootPost.name = '%sRootPost_%s%sGrp' % (fthrPart, ix+1, side)
		rootPost.parent(postGrp)

		# Target
		tarPoci, tarPost = grpOnCrv(tarCrv, tarCrvLen, ix, fthrNo)
		tarPoci.name = '%sTar_%s%sPoci' % (fthrPart, ix+1, side)
		tarPost.name = '%sTarPost_%s%sGrp' % (fthrPart, ix+1, side)
		tarPost.parent(tarGrp)
		aJnt, bJnt, cJnt, dJnt = fourJointChain(rootPost, tarPost, fstFthrs[0])

		aJnt.name = '%sA_%s%sJnt' % (fthrPart, ix+1, side)
		bJnt.name = '%sB_%s%sJnt' % (fthrPart, ix+1, side)
		cJnt.name = '%sC_%s%sJnt' % (fthrPart, ix+1, side)
		dJnt.name = '%sD_%s%sJnt' % (fthrPart, ix+1, side)
		aJnt.parent(mainJnt)
		fthrRig = FeatherRig(mainJnt, aJnt, bJnt, cJnt, dJnt, 
								fthrPart, ix+1, side.replace('_', ''), 'cube')
		fthrRig.ACtrlZr_Grp.parent(ctrlGrp)
		mc.pointConstraint(rootPost, fthrRig.ACtrlAim_Grp)
		mc.aimConstraint(tarPost, fthrRig.ACtrlAim_Grp, 
							aim=(0, 1, 0), u=(1, 0, 0), wut='objectrotation', 
							wuo=mainJnt, wu=(1, 0, 0), mo=True)

		# Rotation calculation.
		mult = (1.0000/float(fthrNo))*float(ix)

		aFstMdv, aSecMdv, aPma = fthrRotDstr(fstFthr=fstFthrs[0], secFthr=secFthrs[0], 
												tar=fthrRig.ACtrlRot_Grp, mult=mult)
		aFstMdv.name = '%sAFstRot_%s%sMdv' % (fthrPart, ix+1, side)
		aSecMdv.name = '%sASecRot_%s%sMdv' % (fthrPart, ix+1, side)
		aPma.name = '%sARot_%s%sPma' % (fthrPart, ix+1, side)

		bFstMdv, bSecMdv, bPma = fthrRotDstr(fstFthr=fstFthrs[1], secFthr=secFthrs[1], 
												tar=fthrRig.BCtrlRot_Grp, mult=mult)
		bFstMdv.name = '%sBFstRot_%s%sMdv' % (fthrPart, ix+1, side)
		bSecMdv.name = '%sBSecRot_%s%sMdv' % (fthrPart, ix+1, side)
		bPma.name = '%sBRot_%s%sPma' % (fthrPart, ix+1, side)

		cFstMdv, cSecMdv, cPma = fthrRotDstr(fstFthr=fstFthrs[2], secFthr=secFthrs[2], 
												tar=fthrRig.CCtrlRot_Grp, mult=mult)
		cFstMdv.name = '%sCFstRot_%s%sMdv' % (fthrPart, ix+1, side)
		cSecMdv.name = '%sCSecRot_%s%sMdv' % (fthrPart, ix+1, side)
		cPma.name = '%sCRot_%s%sPma' % (fthrPart, ix+1, side)

def addFoldGrp(obj=''):

	obj = lrc.Dag(obj)

	par = obj.getParent()

	fldGrp = lrc.Null()
	fldGrp.snap(par)
	fldGrp.parent(par)
	obj.parent(fldGrp)

	return(fldGrp)

def autoFold(srcAttr=None, tarAttr=None, side=''):

	currNm = tarAttr.name.split('_')[0]
	currAttr = tarAttr.name.split('.')[-1]

	mdv = lrc.MultiplyDivide()
	mdv.name = '{nm}{attr}{side}Mdv'.format(nm=currNm, 
												attr=currAttr.upper(), 
												side=side)

	src = lrc.Dag(srcAttr.name.split('.')[0])
	srcShp = lrc.Dag(src.shape)

	dvdr = '__{nm}__'.format(nm=currNm)
	srcShp.add(ln=dvdr, k=True)
	srcShp.attr(dvdr).lock = True

	for ax in ('x', 'y', 'z'):

		srcAttr >> mdv.attr('i1{ax}'.format(ax=ax))

		attrNm = '{nm}{attr}{ax}'.format(nm=currNm, attr=currAttr.upper(), 
											ax=ax)
		srcShp.add(ln=attrNm, k=True)
		srcShp.attr(attrNm) >> mdv.attr('i2{ax}'.format(ax=ax))

	mdv.attr('o') >> tarAttr

class SpreadRig(object):

	def __init__(
						self,
						parent='',
						rootJnt='',
						endJnt='',
						part='',
						side='',
						crvShp='circle'
					):

		# Side definition
		if side:
			side = '_%s_' % side
		else:
			side = '_'

		# Color difinition
		col = 'yellow'
		darkCol = 'softYellow'
		if side == '_R_':
			col = 'blue'
			darkCol = 'darkBlue'
		elif side == '_L_':
			col = 'red'
			darkCol = 'darkRed'

		# Controllers
		self.Root_Ctrl = lrc.Control(crvShp)
		self.Root_Ctrl.lockHideAttrs('v', 'tx', 'ty', 'tz', 'sx', 'sy', 'sz')
		self.Root_Ctrl.color = col
		self.RootCtrlFold_Grp = lrc.group(self.Root_Ctrl)
		self.RootCtrlAim_Grp = lrc.group(self.RootCtrlFold_Grp)
		self.RootCtrlOff_Grp = lrc.group(self.RootCtrlAim_Grp)
		self.RootCtrlZr_Grp = lrc.group(self.RootCtrlOff_Grp)
		self.RootCtrlZr_Grp.snap(rootJnt)

		self.Root_Ctrl.name = '%sRoot%sCtrl' % (part, side)
		self.RootCtrlFold_Grp.name = '%sRootCtrlFold%sGrp' % (part, side)
		self.RootCtrlAim_Grp.name = '%sRootCtrlAim%sGrp' % (part, side)
		self.RootCtrlOff_Grp.name = '%sRootCtrlOff%sGrp' % (part, side)
		self.RootCtrlZr_Grp.name = '%sRootCtrlZr%sGrp' % (part, side)

		mc.parentConstraint(parent, self.RootCtrlZr_Grp, mo=True)
		mc.scaleConstraint(parent, self.RootCtrlZr_Grp, mo=True)

		# Connect to joint
		mc.parentConstraint(self.Root_Ctrl, rootJnt)

class FeatherRig(object):

	def __init__(
						self,
						par='',
						jntA='',
						jntB='',
						jntC='',
						jntD='',
						part='',
						idx='',
						side='',
						crvShp='circle',
						lockTrans=False
					):

		# Index definition
		if idx:
			idx = '_%s' % idx

		# Side definition
		if side:
			side = '_%s_' % side
		else:
			side = '_'

		# Color difinition
		col = 'yellow'
		darkCol = 'softYellow'
		if side == '_R_':
			col = 'blue'
			darkCol = 'darkBlue'
		elif side == '_L_':
			col = 'red'
			darkCol = 'darkRed'

		# Controllers
		self.A_Ctrl = lrc.Control(crvShp)

		self.A_Ctrl.lockHideAttrs('v', 'sx', 'sy', 'sz')
		if lockTrans:
			self.A_Ctrl.lockHideAttrs('tx', 'ty', 'tz')

		self.A_Ctrl.color = darkCol
		self.ACtrlFold_Grp = lrc.group(self.A_Ctrl)
		self.ACtrlRot_Grp = lrc.group(self.ACtrlFold_Grp)
		self.ACtrlAim_Grp = lrc.group(self.ACtrlRot_Grp)
		self.ACtrlOff_Grp = lrc.group(self.ACtrlAim_Grp)
		self.ACtrlZr_Grp = lrc.group(self.ACtrlOff_Grp)
		self.ACtrlZr_Grp.snap(jntA)
		mc.parentConstraint(par, self.ACtrlZr_Grp, mo=True)
		mc.scaleConstraint(par, self.ACtrlZr_Grp, mo=True)

		self.A_Ctrl.name = '%sA%s%sCtrl' % (part, idx, side)
		self.ACtrlFold_Grp.name = '%sACtrlFold%s%sGrp' % (part, idx, side)
		self.ACtrlRot_Grp.name = '%sACtrlRot%s%sGrp' % (part, idx, side)
		self.ACtrlAim_Grp.name = '%sACtrlAim%s%sGrp' % (part, idx, side)
		self.ACtrlOff_Grp.name = '%sACtrlOff%s%sGrp' % (part, idx, side)
		self.ACtrlZr_Grp.name = '%sACtrlZr%s%sGrp' % (part, idx, side)

		self.AGmbl_Ctrl = lrc.addGimbal(self.A_Ctrl)
		self.AGmbl_Ctrl.name = '%sAGmbl%s%sCtrl' % (part, idx, side)

		self.B_Ctrl = lrc.Control(crvShp)
		self.B_Ctrl.lockHideAttrs('v', 'sx', 'sy', 'sz')
		if lockTrans:
			self.B_Ctrl.lockHideAttrs('tx', 'ty', 'tz')
		self.B_Ctrl.color = darkCol
		self.BCtrlFold_Grp = lrc.group(self.B_Ctrl)
		self.BCtrlRot_Grp = lrc.group(self.BCtrlFold_Grp)
		self.BCtrlOff_Grp = lrc.group(self.BCtrlRot_Grp)
		self.BCtrlZr_Grp = lrc.group(self.BCtrlOff_Grp)
		self.BCtrlZr_Grp.snap(jntB)
		self.BCtrlZr_Grp.parent(self.AGmbl_Ctrl)
		
		self.B_Ctrl.name = '%sB%s%sCtrl' % (part, idx, side)
		self.BCtrlFold_Grp.name = '%sBCtrlFold%s%sGrp' % (part, idx, side)
		self.BCtrlRot_Grp.name = '%sBCtrlRot%s%sGrp' % (part, idx, side)
		self.BCtrlOff_Grp.name = '%sBCtrlOff%s%sGrp' % (part, idx, side)
		self.BCtrlZr_Grp.name = '%sBCtrlZr%s%sGrp' % (part, idx, side)
		
		self.BGmbl_Ctrl = lrc.addGimbal(self.B_Ctrl)
		self.BGmbl_Ctrl.name = '%sBGmbl%s%sCtrl' % (part, idx, side)

		self.C_Ctrl = lrc.Control(crvShp)
		self.C_Ctrl.lockHideAttrs('v', 'sx', 'sy', 'sz')
		if lockTrans:
			self.C_Ctrl.lockHideAttrs('tx', 'ty', 'tz')
		self.C_Ctrl.color = darkCol
		self.CCtrlFold_Grp = lrc.group(self.C_Ctrl)
		self.CCtrlRot_Grp = lrc.group(self.CCtrlFold_Grp)
		self.CCtrlOff_Grp = lrc.group(self.CCtrlRot_Grp)
		self.CCtrlZr_Grp = lrc.group(self.CCtrlOff_Grp)
		self.CCtrlZr_Grp.snap(jntC)
		self.CCtrlZr_Grp.parent(self.BGmbl_Ctrl)

		self.C_Ctrl.name = '%sC%s%sCtrl' % (part, idx, side)
		self.CCtrlFold_Grp.name = '%sCCtrlFold%s%sGrp' % (part, idx, side)
		self.CCtrlRot_Grp.name = '%sCCtrlRot%s%sGrp' % (part, idx, side)
		self.CCtrlOff_Grp.name = '%sCCtrlOff%s%sGrp' % (part, idx, side)
		self.CCtrlZr_Grp.name = '%sCCtrlZr%s%sGrp' % (part, idx, side)

		self.CGmbl_Ctrl = lrc.addGimbal(self.C_Ctrl)
		self.CGmbl_Ctrl.name = '%sCGmbl%s%sCtrl' % (part, idx, side)
		
		# Connect to joint
		mc.parentConstraint(self.AGmbl_Ctrl, jntA)
		mc.parentConstraint(self.BGmbl_Ctrl, jntB)
		mc.parentConstraint(self.CGmbl_Ctrl, jntC)

class WingRig(object):

	def __init__(
					self,
					parent='ClavTip_L_Jnt',
					ctrlGrp='',
					jntGrp='',
					ikhGrp='',
					skinGrp='',
					stillGrp='',
					ribbon=True,
					tmpJnt=[
								'UpperArm_L_Jnt',
								'Forearm_L_Jnt',
								'Wrist_L_Jnt',
								'Hand_L_Jnt',
								'ElbowIk_L_Jnt',
								'UpArmWing_L_Jnt', 'UpArmWingTip_L_Jnt',
								'ForearmWing_L_Jnt', 'ForearmWingTip_L_Jnt',
								'WristWing_L_Jnt', 'WristWingTip_L_Jnt',
								'HandWing_L_Jnt', 'HandWingTip_L_Jnt'
							],
					part='',
					side='',
					primNo=5,
					secNo=7,
					scapNo=4
				):

		# Side definition
		if side:
			side = '_%s_' % side
		else:
			side = '_'

		# Color difinition
		col = 'yellow'
		darkCol = 'softYellow'
		if side == '_R_':
			col = 'blue'
			darkCol = 'darkBlue'
		elif side == '_L_':
			col = 'red'
			darkCol = 'darkRed'

		self.UpArmWing_Jnt = tmpJnt[5]
		self.UpArmWingTip_Jnt = tmpJnt[6]
		self.ForearmWing_Jnt = tmpJnt[7]
		self.ForearmWingTip_Jnt = tmpJnt[8]
		self.WristWing_Jnt = tmpJnt[9]
		self.WristWingTip_Jnt = tmpJnt[10]
		self.HandWing_Jnt = tmpJnt[11]
		self.HandWingTip_Jnt = tmpJnt[12]

		self.Arm = armRig.ArmRig(
									parent=parent,
									ctrlGrp=ctrlGrp,
									jntGrp=jntGrp,
									ikhGrp=ikhGrp,
									skinGrp=skinGrp,
									stillGrp=stillGrp,
									ribbon=ribbon,
									tmpJnt=tmpJnt[:5],
									part=part,
									side=side.replace('_', '')
								)
		
		self.HandLen = lrc.distance(self.Arm.Wrist_Jnt, self.Arm.Hand_Jnt)
		
		# Ribbon
		rbnAx = 'y-'
		rbnAim = (0, -1, 0)
		rbnUp = (0, 0, 1)
		rbnAmp = 1

		if side == '_R_' :
			rbnUp = (0, 0, -1)
			rbnAmp = -1

		# Ribbon Control
		self.HandRbn_Ctrl = lrc.Control('plus')
		self.HandRbn_Ctrl.name = 'HandRbn%s%sCtrl' % (part, side)
		self.HandRbn_Ctrl.lockHideAttrs('rx', 'ry', 'rz', 'sx', 'sy', 'sz', 'v')

		self.HandRbnCtrlZr_Grp = lrc.group(self.HandRbn_Ctrl)
		self.HandRbnCtrlZr_Grp.name = 'HandRbnCtrlZr%s%sGrp' % (part, side)

		mc.pointConstraint(self.Arm.Wrist_Jnt, self.HandRbnCtrlZr_Grp)
		mc.orientConstraint(self.Arm.Forearm_Jnt, self.HandRbnCtrlZr_Grp, mo=True)

		# Ribbon Control - Color
		self.HandRbn_Ctrl.color = col

		# Ribbon - Hand
		if ribbon :
			self.HandRbn = lrb.RibbonIkHi(
												size=self.HandLen,
												ax=rbnAx,
												part='Hand%s' % part,
												side=side.replace('_','')
											)
		else :
			self.HandRbn = lrb.RibbonIkLow(
												size=self.HandLen,
												ax=rbnAx,
												part='Hand%s' % part,
												side=side.replace('_','')
											)
		
		self.HandRbn.Ctrl_Grp.snapPoint(self.Arm.Wrist_Jnt)
		mc.delete(mc.aimConstraint(self.Arm.Hand_Jnt, self.HandRbn.Ctrl_Grp, aim=rbnAim, u=rbnUp, wut='objectrotation', wuo=self.Arm.Wrist_Jnt, wu=(0,0,1)))
		mc.parentConstraint(self.Arm.Wrist_Jnt, self.HandRbn.Ctrl_Grp, mo=True)
		mc.pointConstraint(self.HandRbn_Ctrl, self.HandRbn.RbnRoot_Ctrl)
		mc.pointConstraint(self.Arm.Hand_Jnt, self.HandRbn.RbnEnd_Ctrl)

		mc.delete(mc.listConnections('%s.tx' % self.Arm.ForearmRbn.RbnEnd_Ctrl, s=True, d=False)[0])
		for attr in ('tx', 'ty', 'tz'):
			self.Arm.ForearmRbn.RbnEnd_Ctrl.attr(attr).l = False
		mc.pointConstraint(self.HandRbn_Ctrl, self.Arm.ForearmRbn.RbnEnd_Ctrl)

		handRbnShp = lrc.Dag(self.HandRbn.Rbn_Ctrl.shape)
		handRbnShp.attr('rootTwistAmp').v = 1 * rbnAmp
		handRbnShp.attr('endTwistAmp').v = -1 * rbnAmp
		
		self.HandRbn.RbnRootTwsZr_Grp.snap(self.Arm.Wrist_Jnt)
		self.HandRbn.RbnEndTwsZr_Grp.snap(self.Arm.Hand_Jnt)

		mc.parentConstraint(
								self.Arm.Wrist_Jnt,
								self.HandRbn.RbnEndTwsZr_Grp,
								mo=True
							)
		mc.parentConstraint(
								self.Arm.Hand_Jnt,
								self.HandRbn.RbnEndTws_Grp,
								mo=True
							)
		
		self.HandRbn.RbnRootTws_Grp.rotateOrder = 'yzx'
		self.HandRbn.RbnEndTws_Grp.rotateOrder = 'yzx'

		self.HandRbn.Ctrl_Grp.parent(self.Arm.ArmRbnCtrl_Grp)
		self.HandRbn.Skin_Grp.parent(skinGrp)
		self.HandRbn.Jnt_Grp.parent(jntGrp)

		self.HandRbnCtrlZr_Grp.parent(self.Arm.ArmRbnCtrl_Grp)

		if ribbon :
			self.HandRbn.Still_Grp.parent(stillGrp)

		self.HandRbn.RbnRoot_Ctrl.hide()
		self.HandRbn.RbnEnd_Ctrl.hide()

		# Skin joints
		self.UpArmWing_Jnt = lrc.Dag(tmpJnt[5])
		self.UpArmWingTip_Jnt = lrc.Dag(tmpJnt[6])
		self.ForearmWing_Jnt = lrc.Dag(tmpJnt[7])
		self.ForearmWingTip_Jnt = lrc.Dag(tmpJnt[8])
		self.WristWing_Jnt = lrc.Dag(tmpJnt[9])
		self.WristWingTip_Jnt = lrc.Dag(tmpJnt[10])
		self.HandWing_Jnt = lrc.Dag(tmpJnt[11])
		self.HandWingTip_Jnt = lrc.Dag(tmpJnt[12])

		self.UpArmWing_Jnt.name = 'UpArmWing%s%sJnt' % (part, side)
		self.UpArmWingTip_Jnt.name = 'UpArmWingTip%s%sJnt' % (part, side)
		self.ForearmWing_Jnt.name = 'ForearmWing%s%sJnt' % (part, side)
		self.ForearmWingTip_Jnt.name = 'ForearmWingTip%s%sJnt' % (part, side)
		self.WristWing_Jnt.name = 'WristWing%s%sJnt' % (part, side)
		self.WristWingTip_Jnt.name = 'WristWingTip%s%sJnt' % (part, side)
		self.HandWing_Jnt.name = 'HandWing%s%sJnt' % (part, side)
		self.HandWingTip_Jnt.name = 'HandWingTip%s%sJnt' % (part, side)

		# Wing curves
		self.WingCrv_Grp = lrc.Null()
		self.WingCrv_Grp.name = 'WingCrv%s%sGrp' % (part, side)
		self.WingCrv_Grp.parent(stillGrp)

		self.UpArmWing_Crv = wingCurve(self.Arm.UpArm_Jnt, self.Arm.Forearm_Jnt)
		self.UpArmWing_Crv.name = 'UpArmWing%s%sCrv' % (part, side)
		mc.rebuildCurve(self.UpArmWing_Crv, ch=False, kep=1, s=0, d=3)
		self.UpArmWing_Crv.parent(self.WingCrv_Grp)
		bindWingCurve(self.Arm.UpArmRbn, self.UpArmWing_Crv)
		
		self.ForearmWing_Crv = wingCurve(self.Arm.Forearm_Jnt, self.Arm.Wrist_Jnt)
		self.ForearmWing_Crv.name = 'ForearmWing%s%sCrv' % (part, side)
		mc.rebuildCurve(self.ForearmWing_Crv, ch=False, kep=1, s=0, d=3)
		self.ForearmWing_Crv.parent(self.WingCrv_Grp)
		bindWingCurve(self.Arm.ForearmRbn, self.ForearmWing_Crv)
		
		self.WristWing_Crv = wingCurve(self.Arm.Wrist_Jnt, self.Arm.Hand_Jnt)
		self.WristWing_Crv.name = 'WristWing%s%sCrv' % (part, side)
		mc.rebuildCurve(self.WristWing_Crv, ch=False, kep=1, s=0, d=3)
		self.WristWing_Crv.parent(self.WingCrv_Grp)
		bindWingCurve(self.HandRbn, self.WristWing_Crv)

		# Target curves
		self.WingTar_Crv = wingTargetCurve(self.UpArmWingTip_Jnt, self.HandWingTip_Jnt)
		self.WingTar_Crv.name = 'WingTar%s%sCrv' % (part, side)
		self.WingTar_Crv.parent(self.WingCrv_Grp)

		self.UparmWingTar_Crv = wingTargetCurve(self.UpArmWingTip_Jnt, self.ForearmWingTip_Jnt)
		self.UparmWingTar_Crv.name = 'UparmWingTar%s%sCrv' % (part, side)
		self.UparmWingTar_Crv.parent(self.WingCrv_Grp)

		self.ForearmWingTar_Crv = wingTargetCurve(self.ForearmWingTip_Jnt, self.WristWingTip_Jnt)
		self.ForearmWingTar_Crv.name = 'ForearmWingTar%s%sCrv' % (part, side)
		self.ForearmWingTar_Crv.parent(self.WingCrv_Grp)

		self.HandWingTar_Crv = wingTargetCurve(self.WristWingTip_Jnt, self.HandWingTip_Jnt)
		self.HandWingTar_Crv.name = 'HandWingTar%s%sCrv' % (part, side)
		self.HandWingTar_Crv.parent(self.WingCrv_Grp)

		# Main wing posts
		self.WingPost_Grp = lrc.Null()
		self.WingPost_Grp.name = 'WingPost%s%sGrp' % (part, side)
		self.WingPost_Grp.parent(stillGrp)

		# Main wing targets
		self.WingTar_Grp = lrc.Null()
		self.WingTar_Grp.name = 'WingTar%s%sGrp' % (part, side)
		self.WingTar_Grp.parent(stillGrp)

		# Spread controllers - First layer
		self.UpArmSprdRig = SpreadRig(self.Arm.UpArm_Jnt, self.UpArmWing_Jnt,
										self.UpArmWingTip_Jnt, 'UpArmSprd', 
										side.replace('_', ''))
		self.UpArmSprdRig.RootCtrlZr_Grp.parent(self.Arm.Ctrl_Grp)
		self.UpArmWing_Jnt.attr('v').v = 0

		self.HandSprdRig = SpreadRig(self.Arm.Hand_Jnt,self.HandWing_Jnt, 
										self.HandWingTip_Jnt, 'HandSprd', 
										side.replace('_', ''))
		self.HandSprdRig.RootCtrlZr_Grp.parent(self.Arm.Ctrl_Grp)
		self.HandWing_Jnt.attr('v').v = 0

		# Spread controllers - Second layer
		self.ForearmSprdRig = SpreadRig(self.Arm.Forearm_Jnt, self.ForearmWing_Jnt, 
											self.ForearmWingTip_Jnt,'ForearmSprd', 
											side.replace('_', ''))
		self.ForearmSprdRig.Root_Ctrl.col = darkCol
		
		self.ForearmSprdRig.RootCtrlZr_Grp.parent(self.Arm.Ctrl_Grp)
		mc.pointConstraint(self.Arm.ArmRbn_Ctrl, self.ForearmSprdRig.RootCtrlOff_Grp)
		self.ForearmWing_Jnt.attr('v').v = 0


		self.WristSprdRig = SpreadRig(self.Arm.Wrist_Jnt, self.WristWing_Jnt, 
										self.WristWingTip_Jnt, 'WristSprd', 
										side.replace('_', ''))
		self.WristSprdRig.Root_Ctrl.col = darkCol
		self.WristSprdRig.RootCtrlZr_Grp.parent(self.Arm.Ctrl_Grp)
		mc.pointConstraint(self.HandRbn_Ctrl, self.WristSprdRig.RootCtrlOff_Grp)
		self.WristWing_Jnt.attr('v').v = 0

		# Spread controllers - Second layer - Targets
		wingTarCrvLen = lrc.distance(self.UpArmWingTip_Jnt, self.HandWingTip_Jnt)
		crvShp = lrc.Dag(self.WingTar_Crv.shape)

		self.ForearmSprdTar_Grp = lrc.Null()
		self.ForearmSprdTar_Grp.name = 'ForearmSprd%sTar%sJnt' % (part, side)
		self.ForearmSprdTar_Grp.parent(self.WingTar_Grp)

		intxn = lrc.findIntersection(self.UpArmWingTip_Jnt, self.HandWingTip_Jnt, 
										self.ForearmWing_Jnt)
		intxnLen = lrc.mag(lrc.diff(self.UpArmWingTip_Jnt.ws, intxn))
		par = intxnLen/wingTarCrvLen

		self.ForearmSprdTar_Poci = lrc.PointOnCurveInfo()
		self.ForearmSprdTar_Poci.name = 'ForearmSprd%sTar%sPoci' % (part, side)

		self.ForearmSprdTar_Poci.attr('turnOnPercentage').v = 1
		crvShp.attr('worldSpace') >> self.ForearmSprdTar_Poci.attr('inputCurve')
		self.ForearmSprdTar_Poci.attr('parameter').v = par
		self.ForearmSprdTar_Poci.attr('result.position') >> self.ForearmSprdTar_Grp.attr('t')

		mc.aimConstraint(self.ForearmSprdTar_Grp, self.ForearmSprdRig.RootCtrlAim_Grp, 
							aim=(0, 1, 0), u=(1, 0, 0), wut='objectrotation', 
							wuo=self.Arm.Forearm_Jnt, wu=(1, 0, 0), mo=True)

		self.WristSprdTar_Grp = lrc.Null()
		self.WristSprdTar_Grp.name = 'WristSprd%sTar%sJnt' % (part, side)
		self.WristSprdTar_Grp.parent(self.WingTar_Grp)
		
		intxn = lrc.findIntersection(self.UpArmWingTip_Jnt, self.HandWingTip_Jnt, self.WristWing_Jnt)
		intxnLen = lrc.mag(lrc.diff(self.UpArmWingTip_Jnt.ws, intxn))
		par = intxnLen/wingTarCrvLen

		self.WristSprdTar_Poci = lrc.PointOnCurveInfo()
		self.WristSprdTar_Poci.name = 'WristSprd%sTar%sPoci' % (part, side)
		
		self.WristSprdTar_Poci.attr('turnOnPercentage').v = 1
		crvShp.attr('worldSpace') >> self.WristSprdTar_Poci.attr('inputCurve')
		self.WristSprdTar_Poci.attr('parameter').v = par
		self.WristSprdTar_Poci.attr('result.position') >> self.WristSprdTar_Grp.attr('t')

		mc.aimConstraint(self.WristSprdTar_Grp, self.WristSprdRig.RootCtrlAim_Grp, 
							aim=(0, 1, 0), u=(1, 0, 0), wut='objectrotation', 
							wuo=self.Arm.Wrist_Jnt, wu=(1, 0, 0), mo=True)


		# Main feather joints
		self.UpArmMainFthrA_Jnt, self.UpArmMainFthrB_Jnt, self.UpArmMainFthrC_Jnt, \
		self.UpArmMainFthrD_Jnt = fourJointChain(self.UpArmWing_Jnt, self.UpArmWingTip_Jnt)

		self.UpArmMainFthrA_Jnt.name = 'UpArmMainFthr%sA%sJnt' % (part, side)
		self.UpArmMainFthrB_Jnt.name = 'UpArmMainFthr%sB%sJnt' % (part, side)
		self.UpArmMainFthrC_Jnt.name = 'UpArmMainFthr%sC%sJnt' % (part, side)
		self.UpArmMainFthrD_Jnt.name = 'UpArmMainFthr%sD%sJnt' % (part, side)
		self.UpArmMainFthrA_Jnt.parent(self.UpArmWing_Jnt)

		self.ForearmMainFthrA_Jnt, self.ForearmMainFthrB_Jnt, self.ForearmMainFthrC_Jnt, \
		self.ForearmMainFthrD_Jnt = fourJointChain(self.ForearmWing_Jnt, self.ForearmWingTip_Jnt)

		self.ForearmMainFthrA_Jnt.name = 'ForearmMainFthr%sA%sJnt' % (part, side)
		self.ForearmMainFthrB_Jnt.name = 'ForearmMainFthr%sB%sJnt' % (part, side)
		self.ForearmMainFthrC_Jnt.name = 'ForearmMainFthr%sC%sJnt' % (part, side)
		self.ForearmMainFthrD_Jnt.name = 'ForearmMainFthr%sD%sJnt' % (part, side)
		self.ForearmMainFthrA_Jnt.parent(self.ForearmWing_Jnt)

		self.WristMainFthrA_Jnt, self.WristMainFthrB_Jnt, self.WristMainFthrC_Jnt, \
		self.WristMainFthrD_Jnt = fourJointChain(self.WristWing_Jnt, self.WristWingTip_Jnt)

		self.WristMainFthrA_Jnt.name = 'WristMainFthr%sA%sJnt' % (part, side)
		self.WristMainFthrB_Jnt.name = 'WristMainFthr%sB%sJnt' % (part, side)
		self.WristMainFthrC_Jnt.name = 'WristMainFthr%sC%sJnt' % (part, side)
		self.WristMainFthrD_Jnt.name = 'WristMainFthr%sD%sJnt' % (part, side)
		self.WristMainFthrA_Jnt.parent(self.WristWing_Jnt)

		self.HandMainFthrA_Jnt, self.HandMainFthrB_Jnt, self.HandMainFthrC_Jnt, \
		self.HandMainFthrD_Jnt = fourJointChain(self.HandWing_Jnt, self.HandWingTip_Jnt)

		self.HandMainFthrA_Jnt.name = 'HandMainFthr%sA%sJnt' % (part, side)
		self.HandMainFthrB_Jnt.name = 'HandMainFthr%sB%sJnt' % (part, side)
		self.HandMainFthrC_Jnt.name = 'HandMainFthr%sC%sJnt' % (part, side)
		self.HandMainFthrD_Jnt.name = 'HandMainFthr%sD%sJnt' % (part, side)
		self.HandMainFthrA_Jnt.parent(self.HandWing_Jnt)

		# Main feather rigs
		self.UpArmMainFthrRig = FeatherRig(self.UpArmWing_Jnt, self.UpArmMainFthrA_Jnt,
											self.UpArmMainFthrB_Jnt, self.UpArmMainFthrC_Jnt,
											self.UpArmMainFthrD_Jnt, 'UpArmMainFthr%s' % part, 
											'', side.replace('_', ''), 'circle', True)
		self.UpArmMainFthrRig.ACtrlZr_Grp.parent(self.Arm.Ctrl_Grp)

		self.ForearmMainFthrRig = FeatherRig(self.ForearmWing_Jnt, self.ForearmMainFthrA_Jnt,
											self.ForearmMainFthrB_Jnt, self.ForearmMainFthrC_Jnt,
											self.ForearmMainFthrD_Jnt, 'ForearmMainFthr%s' % part, 
											'', side.replace('_', ''), 'circle', True)
		self.ForearmMainFthrRig.ACtrlZr_Grp.parent(self.Arm.Ctrl_Grp)
		mc.pointConstraint(self.Arm.ArmRbn_Ctrl, self.ForearmMainFthrRig.ACtrlOff_Grp)

		self.WristMainFthrRig = FeatherRig(self.WristWing_Jnt, self.WristMainFthrA_Jnt,
											self.WristMainFthrB_Jnt, self.WristMainFthrC_Jnt,
											self.WristMainFthrD_Jnt, 'WristMainFthr%s' % part, 
											'', side.replace('_', ''), 'circle', True)
		self.WristMainFthrRig.ACtrlZr_Grp.parent(self.Arm.Ctrl_Grp)
		mc.pointConstraint(self.HandRbn_Ctrl, self.WristMainFthrRig.ACtrlOff_Grp)

		self.HandMainFthrRig = FeatherRig(self.HandWing_Jnt, self.HandMainFthrA_Jnt,
											self.HandMainFthrB_Jnt, self.HandMainFthrC_Jnt,
											self.HandMainFthrD_Jnt, 'HandMainFthr%s' % part, 
											'', side.replace('_', ''), 'circle', True)
		self.HandMainFthrRig.ACtrlZr_Grp.parent(self.Arm.Ctrl_Grp)

		# Feathers
		armCtrlShp = lrc.Dag(self.Arm.Arm_Ctrl.shape)
		armCtrlShp.add(ln='fthrCtrlVis', min=0, max=1, dv=0, k=True)

		self.FeatherCtrl_Grp = lrc.Null()
		self.FeatherCtrl_Grp.name = 'FeatherCtrl%s%sGrp' % (part, side)
		self.FeatherCtrl_Grp.parent(self.Arm.Ctrl_Grp)

		armCtrlShp.attr('fthrCtrlVis') >> self.FeatherCtrl_Grp.attr('v')

		# Scap feathers
		mainJnt = self.Arm.UpArm_Jnt
		rootCrv = self.UpArmWing_Crv
		tarCrv = self.UparmWingTar_Crv

		fstFthrs = [self.UpArmMainFthrA_Jnt, self.UpArmMainFthrB_Jnt,
						self.UpArmMainFthrC_Jnt, self.UpArmMainFthrD_Jnt]
		secFthrs = [self.ForearmMainFthrA_Jnt, self.ForearmMainFthrB_Jnt,
						self.ForearmMainFthrC_Jnt, self.ForearmMainFthrD_Jnt]

		fthrNo = scapNo
		fthrPart = 'ScapFthr%s' % part
		
		rigFthrs(mainJnt=mainJnt, rootCrv=rootCrv, tarCrv=tarCrv, fstFthrs=fstFthrs,
					secFthrs=secFthrs, fthrNo=fthrNo, fthrPart=fthrPart, 
					side=side, postGrp=self.WingPost_Grp, 
					tarGrp=self.WingTar_Grp, ctrlGrp=self.FeatherCtrl_Grp)

		# Forearm feathers
		mainJnt = self.Arm.Forearm_Jnt
		rootCrv = self.ForearmWing_Crv
		tarCrv = self.ForearmWingTar_Crv

		fstFthrs = [self.ForearmMainFthrA_Jnt, self.ForearmMainFthrB_Jnt,
						self.ForearmMainFthrC_Jnt, self.ForearmMainFthrD_Jnt]
		secFthrs = [self.WristMainFthrA_Jnt, self.WristMainFthrB_Jnt,
						self.WristMainFthrC_Jnt, self.WristMainFthrD_Jnt]

		fthrNo = secNo
		fthrPart = 'SecFthr%s' % part
		
		rigFthrs(mainJnt=mainJnt, rootCrv=rootCrv, tarCrv=tarCrv, fstFthrs=fstFthrs,
					secFthrs=secFthrs, fthrNo=fthrNo, fthrPart=fthrPart, 
					side=side, postGrp=self.WingPost_Grp, 
					tarGrp=self.WingTar_Grp, ctrlGrp=self.FeatherCtrl_Grp)

		# Primary feathers
		mainJnt = self.Arm.Wrist_Jnt
		rootCrv = self.WristWing_Crv
		tarCrv = self.HandWingTar_Crv

		fstFthrs = [self.WristMainFthrA_Jnt, self.WristMainFthrB_Jnt,
						self.WristMainFthrC_Jnt, self.WristMainFthrD_Jnt]
		secFthrs = [self.HandMainFthrA_Jnt, self.HandMainFthrB_Jnt,
						self.HandMainFthrC_Jnt, self.HandMainFthrD_Jnt]

		fthrNo = primNo
		fthrPart = 'PrimFthr%s' % part
		
		rigFthrs(mainJnt=mainJnt, rootCrv=rootCrv, tarCrv=tarCrv, fstFthrs=fstFthrs,
					secFthrs=secFthrs, fthrNo=fthrNo, fthrPart=fthrPart, 
					side=side, postGrp=self.WingPost_Grp, 
					tarGrp=self.WingTar_Grp, ctrlGrp=self.FeatherCtrl_Grp)

		# Thumb feather
		mainJnt = self.Arm.Hand_Jnt
		fthrPart = 'ThumbFthr%s' % part

		aJnt, bJnt, cJnt, dJnt = fourJointChain(self.HandMainFthrA_Jnt, 
													self.HandMainFthrD_Jnt)

		aJnt.name = '%sA%sJnt' % (fthrPart, side)
		bJnt.name = '%sB%sJnt' % (fthrPart, side)
		cJnt.name = '%sC%sJnt' % (fthrPart, side)
		dJnt.name = '%sD%sJnt' % (fthrPart, side)
		aJnt.parent(mainJnt)
		thumbRig = FeatherRig(mainJnt, aJnt, bJnt, cJnt, dJnt, 
								fthrPart, '', side.replace('_', ''), 'cube')
		thumbRig.ACtrlZr_Grp.parent(self.FeatherCtrl_Grp)

		mc.orientConstraint(self.HandMainFthrA_Jnt, thumbRig.ACtrlRot_Grp)
		mc.orientConstraint(self.HandMainFthrB_Jnt, thumbRig.BCtrlRot_Grp)
		mc.orientConstraint(self.HandMainFthrC_Jnt, thumbRig.CCtrlRot_Grp)

		# Folding control
		self.Arm.UpArmFk_Ctrl.attr('localWorld').v = 0

		self.Arm.Arm_Ctrl.add(ln='fold', min=0, max=10, dv=0, k=True)

		self.UpArmFkCtrlFold_Grp = addFoldGrp(self.Arm.UpArmFk_Ctrl)
		self.UpArmFkCtrlFold_Grp.name = 'UpArmFkCtrlFold%s%sGrp' % (part, side)

		self.ForearmFkCtrlFold_Grp = addFoldGrp(self.Arm.ForearmFk_Ctrl)
		self.ForearmFkCtrlFold_Grp.name = 'ForearmFkCtrlFold%s%sGrp' % (part, side)

		self.WristFkCtrlFold_Grp = addFoldGrp(self.Arm.WristFk_Ctrl)
		self.WristFkCtrlFold_Grp.name = 'WristFkCtrlFold%s%sGrp' % (part, side)

		self.ArmRbnCtrlFold_Grp = addFoldGrp(self.Arm.ArmRbn_Ctrl)
		self.ArmRbnCtrlFold_Grp.name = 'ArmRbnCtrlFold%s%sGrp' % (part, side)

		autoFold(self.Arm.Arm_Ctrl.attr('fold'), self.UpArmFkCtrlFold_Grp.attr('r'), side)
		autoFold(self.Arm.Arm_Ctrl.attr('fold'), self.ForearmFkCtrlFold_Grp.attr('r'), side)
		autoFold(self.Arm.Arm_Ctrl.attr('fold'), self.WristFkCtrlFold_Grp.attr('r'), side)
		autoFold(self.Arm.Arm_Ctrl.attr('fold'), self.ArmRbnCtrlFold_Grp.attr('t'), side)
		
		autoFold(self.Arm.Arm_Ctrl.attr('fold'), self.UpArmSprdRig.RootCtrlFold_Grp.attr('r'), side)
		autoFold(self.Arm.Arm_Ctrl.attr('fold'), self.HandSprdRig.RootCtrlFold_Grp.attr('r'), side)
		
		autoFold(self.Arm.Arm_Ctrl.attr('fold'), self.ForearmSprdRig.RootCtrlFold_Grp.attr('r'), side)
		autoFold(self.Arm.Arm_Ctrl.attr('fold'), self.WristSprdRig.RootCtrlFold_Grp.attr('r'), side)
		
		autoFold(self.Arm.Arm_Ctrl.attr('fold'), self.UpArmMainFthrRig.ACtrlFold_Grp.attr('r'), side)
		autoFold(self.Arm.Arm_Ctrl.attr('fold'), self.UpArmMainFthrRig.BCtrlFold_Grp.attr('r'), side)
		autoFold(self.Arm.Arm_Ctrl.attr('fold'), self.UpArmMainFthrRig.CCtrlFold_Grp.attr('r'), side)
		
		autoFold(self.Arm.Arm_Ctrl.attr('fold'), self.ForearmMainFthrRig.ACtrlFold_Grp.attr('r'), side)
		autoFold(self.Arm.Arm_Ctrl.attr('fold'), self.ForearmMainFthrRig.BCtrlFold_Grp.attr('r'), side)
		autoFold(self.Arm.Arm_Ctrl.attr('fold'), self.ForearmMainFthrRig.CCtrlFold_Grp.attr('r'), side)
		
		autoFold(self.Arm.Arm_Ctrl.attr('fold'), self.WristMainFthrRig.ACtrlFold_Grp.attr('r'), side)
		autoFold(self.Arm.Arm_Ctrl.attr('fold'), self.WristMainFthrRig.BCtrlFold_Grp.attr('r'), side)
		autoFold(self.Arm.Arm_Ctrl.attr('fold'), self.WristMainFthrRig.CCtrlFold_Grp.attr('r'), side)
		
		autoFold(self.Arm.Arm_Ctrl.attr('fold'), self.HandMainFthrRig.ACtrlFold_Grp.attr('r'), side)
		autoFold(self.Arm.Arm_Ctrl.attr('fold'), self.HandMainFthrRig.BCtrlFold_Grp.attr('r'), side)
		autoFold(self.Arm.Arm_Ctrl.attr('fold'), self.HandMainFthrRig.CCtrlFold_Grp.attr('r'), side)
		
		# Set default attributes
		self.Arm.UpArmRbn.Rbn_Ctrl.attr('autoTwist').v = 0
		self.Arm.ForearmRbn.Rbn_Ctrl.attr('autoTwist').v = 0