import maya.cmds as mc
import maya.mel as mm
import maya.api.OpenMaya as om

import os
import shutil
import sys
import re
from functools import partial

import lpRig.core as lrc
reload(lrc)

def edgeLoopToCircle(spans=12):

	crv = mc.polyToCurve(degree=1, ch=False)[0]
	crvShp = mc.listRelatives(crv, s=True)[0]

	crvSpans = mc.getAttr('%s.spans' % crvShp)

	crc = mc.circle(d=3, ch=False, s=crvSpans)[0]

	for ix in xrange(crvSpans):

		currCrvCv = '%s.cv[%d]' % (crv, ix)
		currCrcCv = '%s.cv[%d]' % (crc, ix)

		currPost = mc.xform(currCrvCv, q=True, ws=True, t=True)
		mc.xform(currCrcCv, ws=True, t=currPost)

	mc.rebuildCurve(crc, ch=False, s=spans)
	mc.delete(crv)

def equalizeSelectedEdge(cubic=False):
	"""Select edge loop to average then run the script.
	"""
	sels = mc.ls(sl=True, fl=True)
	vtcs = mc.ls(mc.polyListComponentConversion(sels, tv=True), fl=True)

	geo = sels[0].split('.')[0]

	crv = mc.polyToCurve(form=2, degree=1, ch=False)[0]
	crvShp = mc.listRelatives(crv, s=True)[0]
	cvNo = mc.getAttr('%s.spans' % crvShp)
	
	# Pair each vertex to the cv that has the same world position.
	vtxToCv = {}
	tol = 0.0001
	for ix in xrange(cvNo):

		currCv = '%s.cv[%s]' % (crv, ix)
		cvPost = mc.xform(currCv, q=True, ws=True, t=True)

		for vtx in vtcs:

			vtxPost = mc.xform(vtx, q=True, ws=True, t=True)

			match = True
			for iy in xrange(3):
				if not (cvPost[iy] - tol) <= vtxPost[iy] <= (cvPost[iy] + tol):
					match = False

			if match:
				vtxToCv[vtx] = currCv

	# Smooth curve.
	rbCmd = 'rebuildCurve -ch 0 -rpo 1 -rt 0 -end 1 -kr 0 -kcp 0 -kep 1 -kt 1 -s 2 \
				-d 3 -tol 0.001 "%s";' % crv
	mm.eval(rbCmd)

	# Rebuild the curve.
	rbCmd = 'rebuildCurve -ch 0 -rpo 1 -rt 0 -end 1 -kr 0 -kcp 0 -kep 1 -kt 1 -s %s \
				-d 1 -tol 0.001 "%s";' % (len(vtcs) - 1, crv)
	if cubic:
		rbCmd = 'rebuildCurve -ch 0 -rpo 1 -rt 0 -end 1 -kr 0 -kcp 0 -kep 1 -kt 1 -s %s \
				-d 3 -tol 0.001 "%s";' % (len(vtcs) - 3, crv)

	mm.eval(rbCmd)

	# Apply CVs' position to vertices' position.
	if len(vtcs) == 3:
		# If only two edges have been selected.
		mc.delete('%s.cv[1]' % crv)
		mc.delete('%s.cv[3]' % crv)

	for vtx in vtxToCv.keys():
		cvPost = mc.xform(vtxToCv[vtx], q=True, ws=True, t=True)
		mc.xform(vtx, ws=True, t=cvPost)

	# Cleanup
	mc.delete(crv)
	cmd = 'doMenuComponentSelection("%s", "edge");' % geo
	mm.eval(cmd)

def deleteHalf(tol=-0.0001):

	sel = mc.ls(sl=True)[0]

	fcNo = mc.polyEvaluate(sel, f=True)

	delFcs = []
	for ix in range(fcNo):
		currFc = '%s.f[%s]' % (sel, ix)
		currPost = mc.xform(currFc, q=True, t=True, os=True)

		for iy in range(0, len(currPost), 3):
			
			if not currPost[iy] <= tol:
				continue
			
			if not currFc in delFcs:
				delFcs.append(currFc)

	mc.delete(delFcs)

def enableRenderStatToAllMeshes():

	attrs = ('castsShadows', 'receiveShadows', 'motionBlur', 
				'primaryVisibility', 'visibleInReflections', 'visibleInRefractions')

	for mesh in mc.ls(type='mesh'):
		for attr in attrs:
			currAttr = '%s.%s' % (mesh, attr)			
			if not mc.objExists(currAttr):
				continue
			mc.setAttr(currAttr, 1)

def addRiToSelected(part='', side=''):
	"""Adding RotateIntensity object to the selected transform node.
	"""
	sel = mc.ls(sl=True)[0]
	par = mc.listRelatives(sel, p=True)[0]

	riObj = lrc.RotateIntensity()
	riObj.rename(part, '', side)
	riObj.orig.snap(sel)
	mc.parentConstraint(par, riObj.orig, mo=True)
	mc.pointConstraint(sel, riObj.point)
	mc.orientConstraint(sel, riObj.rot, mo=True)

def pointRigToSelected(part='', side='', shape='', parent=''):
	"""Create point rig at the position of selected node.
	"""
	import pointRig
	reload(pointRig)

	sel = mc.ls(sl=True)[0]

	ctrlGrp = ''
	if mc.objExists('Ctrl_Grp'):
		ctrlGrp = 'Ctrl_Grp'

	rigObj = pointRig.PointRig(parent=parent, ctrlGrp=ctrlGrp, 
									tmpJnt=sel, part=part, side=side, 
									shape=shape)

def printMeshIssues(lm=True, fs=True, nmf=True, ezr=True, fzr=True):
	"""Print all mesh issues to text scroll list.
	"""
	def checkMeshTslCommand(tsl=''):
		"""Select command on text scroll list.
		"""
		selected = mc.textScrollList(tsl, q=True, si=True)

		if mc.objExists(selected[0]):
			mc.select(selected[0])

	result = checkBadGeos(lm, fs, nmf, ezr, fzr)

	if result:

		ui = 'checkMesh'
		win = '%sWin' % ui
		if mc.window(win, exists=True):
			mc.deleteUI(win)
		
		currScn = os.path.normpath(mc.file(q=True, l=True)[0])
		fldPath, scnNmExt = os.path.split(currScn)
		scnNm, scnExt = os.path.splitext(scnNmExt)

		lines = []

		for key in result.keys():
			currIssues = result[key]
			txt = '%s' % key
			lines.append(txt)

			for ix in currIssues:
				txt = '	%s' % ix
				lines.append(txt)

		for line in lines:
			print line

		mc.window(win, t='Mesh Issues')
		mc.paneLayout()
		tsl = '%sTsl' % ui
		mc.textScrollList(tsl, numberOfRows=32, allowMultiSelection=True,
							append=lines, sc=partial(checkMeshTslCommand, tsl))
		mc.showWindow(win)

	else:
		mc.confirmDialog(title='Confirm', message='All meshes are clean.')

def checkMesh(meshName='', cmd=''):

	mc.select(meshName, r=True)

	mm.eval(cmd)

	sels = mc.ls(sl=True, fl=True)
	results = []

	for sel in sels:

		if '.map[' in sel:
			continue
		else:
			results.append(sel)

	return results

def checkBadGeos(lm=True, fs=True, nmf=False, ezr=True, fzr=True):

	meshes = mc.ls(type='mesh', l=True)
	result = {}

	for mesh in meshes:

		currKey = mesh.split('|')[-1]
		currIssues = []

		if mc.getAttr('%s.intermediateObject' % mesh):
			continue

		if lm:
			# Lamina
			cmd = 'polyCleanupArgList 4 { "0","2","1","0","0","0","0","0","0","1e-005","0","1e-005","0","1e-005","0","-1","1","0" };'
			issues = checkMesh(mesh, cmd)
			if issues:
				currIssues.append('Lamina face')

		if fs:
			# Faces more than 4 sides
			cmd = 'polyCleanupArgList 4 { "0","2","1","0","1","0","0","0","0","1e-005","0","1e-005","0","1e-005","0","-1","0","0" };'
			issues = checkMesh(mesh, cmd)
			if issues:
				currIssues.append('Faces more than 4 sides')

		if nmf:
			# Non-manifold geometry
			cmd = 'polyCleanupArgList 4 { "0","2","1","0","0","0","0","0","0","1e-005","0","1e-005","0","1e-005","0","1","0","0" };'
			issues = checkMesh(mesh, cmd)
			if issues:
				currIssues.append('Non-manifold geometry')

		if ezr:
			# Edge with zero length
			cmd = 'polyCleanupArgList 4 { "0","2","1","0","0","0","0","0","0","1e-005","1","1e-006","0","1e-005","0","-1","0","0" };'
			issues = checkMesh(mesh, cmd)
			if issues:
				currIssues.append('Edge with zero length')

		if fzr:
			# Face with zero geometry area
			cmd = 'polyCleanupArgList 4 { "0","2","1","0","0","0","0","0","1","1e-005","0","1e-006","0","1e-005","0","-1","0","0" };'
			issues = checkMesh(mesh, cmd)
			if issues:
				currIssues.append('Face with zero geometry area')

		if currIssues:
			result[currKey] = currIssues

	return result

def setAllDeforms(val=True):

	bshs = mc.ls(type='blendShape')
	skins = mc.ls(type='skinCluster')

	deforms = bshs + skins

	for deform in deforms:
		mc.setAttr('%s.envelope' % deform, val)

def hideAllAiCurveAttrs():

	print 'Removing ai attributes.'

	aiAttrs = ('aiRenderCurve', 'aiCurveWidth', 'aiSampleRate',
				'aiCurveShaderR', 'aiCurveShaderG', 'aiCurveShaderB')

	for crvShp in mc.ls(type='nurbsCurve'):

		for attr in aiAttrs:

			currNdAttr = '%s.%s' % (crvShp, attr)

			if not mc.objExists(currNdAttr):
				continue

			mc.setAttr(currNdAttr, k=False)

def editAttr():
	'''
	Edit attributes according to the connection from "AttrEdit_Grp".
	'''
	editGrp = 'AttrEdit_Grp'

	if not mc.objExists(editGrp):
		print '%s has not been found.' % editGrp
		return False

	udAttrs = mc.listAttr(editGrp, ud=True)

	for attr in udAttrs:

		currPlug = '%s.%s' % (editGrp, attr)

		tar = mc.listConnections(currPlug, d=True, s=False, p=True)[0]
		
		mc.disconnectAttr(currPlug, tar)

		mc.setAttr(tar, l=True)

		if attr.endswith('lockHide'):
			mc.setAttr(tar, k=False)

def connectAttrEdit(opr=0):
	'''
	Connect the attr edit to the referenced attributes.
	0 to lock selected attrs.
	1 to lock and hide selected attrs.
	'''
	editGrp = 'AttrEdit_Grp'
	
	objs = mc.ls(sl=True, l=True)
	chnls = mc.channelBox('mainChannelBox', q=True, sma=True)
	
	addAttrEditGrp()
	
	for obj in objs:
		for chnl in chnls:
			objNm = obj.split(':')[-1]
			attrNm = '%s_%s' % (objNm, chnl)

			if opr == 0:
				attrNm = '%s_lock' % attrNm

			elif opr == 1:
				attrNm = '%s_lockHide' % attrNm

			mc.addAttr(editGrp, ln=attrNm, k=True)
			currVal = mc.getAttr('%s.%s' % (obj, chnl))
			mc.setAttr('%s.%s' % (editGrp, attrNm), currVal)

			mc.connectAttr('%s.%s' % (editGrp, attrNm), '%s.%s' % (obj, chnl))

def addAttrEditGrp():
	'''
	Add AttrEdit group.
	'''
	editGrp = 'AttrEdit_Grp'

	if mc.objExists(editGrp):
		print '"AttrEdit_Grp" is existed.'
		return False

	delGrpNm = 'Delete_Grp'

	if not mc.objExists(delGrpNm):
		mc.group(em=True, n=delGrpNm)

	mc.group(em=True, n=editGrp)
	mc.parent(editGrp, delGrpNm)

def addInbetweenToSelected(weight=0.5):
	'''
	Select target object, blend shape object and 
	blend shape attribute then run the script.
	'''
	sels = mc.ls(sl=True)
		
	# Derive blend shape node from the selection.
	bsn = ''
	for sel in sels:
		if mc.nodeType(sel) == 'blendShape':
			bsn = sel

	if not bsn:
		# raise InputError('There is no blend shape node selected.')
		print 'Blend shape node has not been found.'
		return False
	
	attr = mm.eval('selectedChannelBoxAttributes')[0]

	bshAttrs = mc.aliasAttr(bsn, q=True)

	bshIdx = None
	for ix in range(0, len(bshAttrs), 2):
		if bshAttrs[ix] == attr:
			bshIdx = ix
			break

	mc.blendShape(bsn, e=True, ib=True, t=(sels[-1], bshIdx, sels[-2], weight))

def createModNode(nodeName='ModExp_Grp'):
	"""Create modulus node using expression.
	"""
	grp = lrc.Null()
	grp.name = nodeName

	grp.add(ln='input', k=True)
	grp.add(ln='mod', dv=1, k=True)
	grp.add(ln='output', k=True)

	exp = 'float $input = {obj}.input;\n'.format(obj=grp.name)
	exp += 'float $mod = {obj}.mod;\n'.format(obj=grp.name)
	exp += '{obj}.output = $input%$mod;'.format(obj=grp.name)
	
	mc.expression(s=exp)

	return grp

def lraOff():
	'''
	Turn all displayLocalAxis off.
	'''
	for each in mc.ls():
		attr = '%s.displayLocalAxis' % each
		if mc.objExists(attr):
			try:
				mc.setAttr(attr, 0)
			except:
				pass

def setBorderVis(vis=0):
	"""Set display borders to all meshes.
	"""
	for each in mc.ls(type='mesh'):
		mc.setAttr('%s.displayBorders' % each, vis)

def borderOn():
	setBorderVis(1)

def borderOff():
	setBorderVis(0)

def removeAllUnknowns():
	'''
	Recursively remove unknown nodes from current scene.
	'''
	unkns = mc.ls(type='unknown')

	if not unkns:
		print 'All unknown nodes have been removed.'
		return True

	for unkn in unkns:
		if mc.objExists(unkn):
			try:
				mc.delete(unkn)
				print '%s has been removed.' % unkn
			except:
				pass
	removeAllUnknowns()

def removeIllegalKeyframe():
	'''
	Remove all animCurves that connected to _Jnt
	'''
	attrs = ['%s%s' % (tf, ax) for tf in ('t', 'r', 's') for ax in ('x', 'y', 'z')]

	for each in mc.ls():

		if not each.endswith('_Jnt'):
			continue

		if not mc.nodeType(each) == 'joint':
			continue

		for attr in attrs:
					
			ndAttr = '%s.%s' % (each, attr)
			
			cnncs = mc.listConnections(ndAttr, s=True, d=False)
			
			if not cnncs:
				continue
			
			for cnnc in cnncs:

				if 'animCurve' in mc.nodeType(cnnc):
					print '%s has been removed.' % cnnc
					mc.delete(cnnc)

				elif 'pairBlend' in mc.nodeType(cnnc):
					
					pbCnncs = mc.listConnections(cnnc, s=True, d=False)
					
					if not pbCnncs:
						continue
					
					for pbCnnc in pbCnncs:
						if 'animCurve' in mc.nodeType(pbCnnc):
							print '%s has been removed.' % pbCnnc
							mc.delete(pbCnnc)

def keyMovedVertices():
	"""Create keyframes to only selected moved vertices.
	"""
	sels = mc.ls(sl=True, fl=True)

	for sel in sels:
		
		reExp = r"(.+)\.(.+)\[([0-9]+)\]"
		reObj = re.findall(reExp, sel)

		if not reObj:
			continue
			print '%s is illegal object.' % sel
		
		tf = reObj[0][0]
		shp = mc.listRelatives(tf, s=True)[0]
		vtxNo = reObj[0][2]
		
		for ax in ('x', 'y', 'z'):
			pntNm = '{shp}.pnts[{vtxNo}].pnt{ax}'.format(shp=shp, vtxNo=vtxNo, ax=ax)
			val = mc.getAttr(pntNm)
			oldVal = mc.keyframe(pntNm, query=True, lastSelected=True, valueChange=True )[0]
			
			if not val == oldVal:
				vtxNm = '{tf}.vtx[{vtxNo}]'.format(tf=tf, vtxNo=vtxNo)
				cmd = 'setKeyframe -breakdown 0 -hierarchy none -controlPoints 0 -shape 0 {"%s"};' % vtxNm
				mm.eval(cmd)
				print '%s has been keyed.' % vtxNm

def renameVolCompBsh(geoName='Body'):
	"""Select shaped object, driver joint and driver attribute.
	"""
	sels = mc.ls(sl=True)
	chnls = mc.channelBox('mainChannelBox', q=True, sma=True)

	jntNm, jntIdx, jntSide, jntType = extractName(sels[1])

	if not len(sels) == 2:
		print 'Selection error.'
		return False

	if not len(chnls) == 1:
		print 'Selection error.'
		return False

	val = int(mc.getAttr('%s.%s' % (sels[1], chnls[0])))

	valStr = 'P%s' % str(val)
	if val < 0:
		valStr = 'M%s' % str(abs(val))

	bshNm = '%sGeo%s%s%s%s%s%s%sBsh' % (geoName, jntNm, jntType, 
										chnls[0][0].upper(), chnls[0][1:], 
										valStr, jntIdx, jntSide)

	mc.rename(sels[0], bshNm)

def reconnectVolCompRem():
	'''
	Reconnect the drivers back to volume compensate remap nodes. 
	'''
	rems = mc.ls(type='remapValue')

	for rem in rems:

		if not mc.objExists('%s.driver' % rem):
			continue

		driver = mc.getAttr('%s.driver' % rem, asString=True)
		driverNode, driverAttr = driver.split('.')

		results = mc.ls(driverNode)
		if not results:
			results = mc.ls('*:%s' % driverNode)
		
		driverNode = results[0]

		currDriver = '%s.%s' % (driverNode, driverAttr)

		if mc.objExists(currDriver):
			try:
				mc.connectAttr(currDriver, '%s.i' % rem)
				print 'Connected %s to %s.i' % (currDriver, rem)
			except:
				pass
		else:
			print '%s does not exist.' % currDriver

def remapSelectedBlendShapes():
	'''
	Automatically remap selected blend shape attributes.
	Examples of valid blend shape attribute name.
	'Body[Geo]Forearm[Jnt]Rx[P]40_R_Bsh'
	'Body[Geo]Index[Jnt]Rx[M]80_1_R_Bsh'
	
	[Geo] and [Jnt] are separators to derive the driver.
	[M] and [P] are the prefix for the input values to dedicate that the input is [M]inus or [P]lus value.
	'''
	sels = mc.ls(sl=True)
	
	# Derive blend shape node from the selection.
	bsh = ''
	for sel in sels:
		if mc.nodeType(sel) == 'blendShape':
			bsh = sel
	
	if not bsh:
		# raise InputError('There is no blend shape node selected.')
		return False
	
	attrs = mm.eval('selectedChannelBoxAttributes')

	# Needed information.
	driver = ''
	inVals = list()
	drivens = list()

	# Keywords that need in the blend shape attribute name.
	keywords = 'Jnt'

	for attr in attrs:

		nm, idx, side, _type = extractName(attr)

		for keyword in keywords:
			if not keyword in nm:
				raise ValueError('%s does not contain keyword %s' % (attr, keyword))

		drivens.append('%s.%s' % (bsh, attr))
		
		# Derive joint name from current attribute name.
		jntNm = nm.split('Geo')[1].split('Jnt')[0]

		# Derive input value from attribute name.
		reExp = r'(.+)([M|P]\d+)'
		reObj = re.findall(reExp, nm)

		if not reObj:
			raise ValueError('Cannot find input value from \
								current blend shape attribute.')

		currValStr = reObj[0][1]

		currVal = None
		if currValStr.startswith('P'):
			currVal = int(currValStr.replace('P', ''))

		elif currValStr.startswith('M'):
			currVal = -int(currValStr.replace('M', ''))

		inVals.append(currVal)

		# Derive driver from blend shape name.
		driverNode = composeName(jntNm, idx, side, 'Jnt')
		tmpAttrNm = nm.split('Jnt')[1].replace(currValStr, '')
		driverAttr = '%s%s' % (tmpAttrNm[0].lower(), tmpAttrNm[1:])
		
		currDriver = '%s.%s' % (driverNode, driverAttr)
		
		if not driver:
			driver = currDriver
		else:
			if not currDriver == driver:
				raise ValueError('Driver is not match to each attribute.')

	if not mc.objExists(driver):
		raise ValueError('Driver %s does not exist.' % driver)

	rems = remapBlendShapes(driver, drivens, inVals)
	mc.select(sels, r=True)
	
	return rems

def remapBlendShapes(driver='', drivens=None, inVals=None):
	'''
	remapBlendShape('Forearm_L_Jnt.rx',
						['BodyVolComp_Bsn.BodyGeoForearmJntRxP40_L_Bsh', 
							'BodyVolComp_Bsn.BodyGeoForearmJntRxP80_L_Bsh'], 
						inVals=[40, 80])
	'''
	if not len(drivens) == len(inVals):
		raise ValueError('Number of drivens and inVals are not equal.')

	valLen = len(inVals)

	for ix in range(valLen):

		nextInVal = 0
		if ix < valLen - 1:
			nextInVal = inVals[ix+1]

		preInVal = 0
		if ix > 0:
			preInVal = inVals[ix-1]

		currInVals = [preInVal, inVals[ix], nextInVal]
		currOutVals = [0, 1, 0]
		
		rem = remapDriven(driver=driver, driven=drivens[ix], inVals=currInVals, outVals=currOutVals)
		drivenAttr = drivens[ix].split('.')[1]
		rem.name = changeType(drivenAttr, 'Rem')

def remapSelectedDrivens(driver='', frames=None):
	'''
	Select drivens and channel box attributes.
	remapSelectedDrivens(driver='Forearm_L_Jnt.rx', frames=[20, 25, 30])
	'''
	sels = mc.ls(sl=True)
	attrs = mm.eval('selectedChannelBoxAttributes')

	rems = list()

	# Populate input values regarding a given frame list.
	inVals = []
	for frame in frames:
		inVals.append(mc.getAttr(driver, time=frame))

	# Iterate through selected nodes, selected attributes and 
	# given frame list to get the output values.
	for sel in sels:

		outVals = []

		for attr in attrs:

			driven = '%s.%s' % (sel, attr)
			currOutVals = []

			for frame in frames:

				currOutVals.append(mc.getAttr(driven, time=frame))

			outVals.append(currOutVals)

		rem = remapDrivens(driver, sel, attrs, inVals, outVals)
		rems.append(rem)

	return rems

def remapDrivens(driver='', drivenNode='', drivenAttrs=None, 
					inVals=None, outVals=None):
	'''
	Remap multiple driven attributes.
	remapDrivens(driver='Forearm_L_Jnt.rx', drivenNode='ForearmVolComp_L_Jnt', 
					drivenAttrs=['tz', 'sz'], inVals=[0, 80], 
					outVals=[[0, -1.42], [1, 2]])
	'''
	if not len(drivenAttrs) == len(outVals):
		raise ValueError('Number of outVals and drivenAttrs are not equal.')

	rems = []
	for ix, attr in enumerate(drivenAttrs):

		driven = '%s.%s' % (drivenNode, attr)
		rem = remapDriven(driver, driven, inVals, outVals[ix])

		rems.append(rem)

	return rems

def remapDriven(driver='', driven='', inVals=None, outVals=None):
	'''
	Remap given driver attribute to given driven attribute.
	remapDrivens(driver='Forearm_L_Jnt.rx', drivenNode='ForearmVolComp_L_Jnt.sz', 
					inVals=[0, 80], outVals=[1, 2])
	'''
	drvnNd, drvnAttr = driven.split('.')
	drvnNm, drvnIdx, drvnSide, drvnType = extractName(drvnNd)
	drvnNm = '%s%s%s' % (drvnNm, drvnType, drvnAttr.title())
	drvnType = 'Rem'
	remNm = composeName(drvnNm, drvnIdx, drvnSide, drvnType)

	if mc.objExists(remNm):
		mc.delete(remNm)

	rem = remapValue(inVals, outVals, driver, driven)
	rem.name = remNm

	rem.add(ln='driver', at='enum', en='%s:' % driver, k=True)

	return rem

def remapValue(inVals=[0, -35, -75], outVals=[1, 1.1, 1.3], 
				srcAttr='', tarAttr=''):
	'''
	Remap value using remapColor node from srcAttr to tarAttr
	with a sequence of input values to a sequence of output values.
	'''
	if not len(inVals) == len(outVals):
		raise ValueError('Number of inVals and outVals are not equal.')

	srcNdNm, srcAttrNm = srcAttr.split('.')
	srcNd = lrc.Dag(srcNdNm)
	
	tarNdNm, tarAttrNm = tarAttr.split('.')
	tarNd = lrc.Dag(tarNdNm)
	
	srcNm, srcIdx, srcSide, srcType = extractName(srcNdNm)
	tarNm, tarIdx, tarSide, tarType = extractName(tarNdNm)
	
	remVal = lrc.RemapValue()
	setRemap(remVal.name, inVals, outVals)

	srcNd.attr(srcAttrNm) >> remVal.attr('i')
	remVal.attr('ov') >> tarNd.attr(tarAttrNm)

	return remVal

def setRemap(remNdNm='', inVals=[0, -35, -75], outVals=[1, 1.1, 1.3]):
	'''
	Set positions and output values of given remapColor node.
	'''
	if not len(inVals) == len(outVals):
		raise ValueError('Number of inVals and outVals are not equal.')

	remVal = lrc.Node(remNdNm)

	for ix in range(len(inVals)):
		
		remItpAttr = 'value[%s].value_Interp' % ix
		remValAttr = 'value[%s].value_FloatValue' % ix
		remPosAttr = 'value[%s].value_Position' % ix

		remVal.attr(remValAttr).v = outVals[ix]
		remVal.attr(remPosAttr).v = inVals[ix]
		remVal.attr(remItpAttr).v = 1

def focusToSelectedFace():

	currPan = None

	visPans = mc.getPanel(vis=True)
	modPan = mc.getPanel(wf=True)

	if not 'model' in modPan:
		for visPan in visPans:
			if 'model' in visPan:
				currPan = visPan
	else:
		currPan = modPan

	if not currPan: return False

	cam = mc.modelPanel(currPan, q=True, cam=True)

	selPosts = mc.xform(mc.ls(sl=True)[0], q=True, t=True, ws=True)

	xPosts = [selPosts[ix] for ix in range(0, len(selPosts), 3)]
	yPosts = [selPosts[ix] for ix in range(1, len(selPosts), 3)]
	zPosts = [selPosts[ix] for ix in range(2, len(selPosts), 3)]

	xMid = sum(xPosts)/len(xPosts)
	yMid = sum(yPosts)/len(yPosts)
	zMid = sum(zPosts)/len(zPosts)

	mc.xform(cam, t=(xMid, yMid, zMid))

	mc.delete(mc.normalConstraint(mc.ls(sl=True)[0], cam, aimVector = (0, 0, 1), worldUpType = 0))
	mc.viewFit()

def rivetToSeletedEdge(part=''):

	sels = mc.ls(sl=True, fl=True)
	rivet(part=part, fstEdge=sels[0], secEdge=sels[1])

def rivet(part='', fstEdge='', secEdge=''):

	fstGeo = fstEdge.split('.')[0]
	secGeo = secEdge.split('.')[0]
	fstDig = int(re.findall(r'\d+', fstEdge)[-1])
	secDig = int(re.findall(r'\d+', secEdge)[-1])

	# First curveFromMeshEdge
	fstCfme = mc.createNode('curveFromMeshEdge', n='Fst%sRvt_Cfme' % part)
	mc.connectAttr('%s.w' % fstGeo, '%s.im' % fstCfme)
	mc.setAttr('%s.ihi' % fstCfme, 1)
	mc.setAttr('%s.ei[0]' % fstCfme, fstDig)

	# Second curveFromMeshEdge
	secCfme = mc.createNode('curveFromMeshEdge', n='SecR%svt_Cfme' % part)
	mc.connectAttr('%s.w' % secGeo, '%s.im' % secCfme)
	mc.setAttr('%s.ihi' % secCfme, 1)
	mc.setAttr('%s.ei[0]' % secCfme, secDig)

	# loft
	loft = mc.createNode('loft', n='%sRvt_Loft' % part)
	mc.setAttr('%s.u' % loft, True)

	# pointOnSurfaceInfo
	posi = mc.createNode('pointOnSurfaceInfo', n='%sRvt_Posi' % part)
	mc.setAttr('%s.turnOnPercentage' % posi, True)
	mc.setAttr('%s.parameterU' % posi, 0.5)
	mc.setAttr('%s.parameterV' % posi, 0.5)

	# Connections
	mc.connectAttr('%s.oc' % fstCfme, '%s.ic[0]' % loft)
	mc.connectAttr('%s.oc' % secCfme, '%s.ic[1]' % loft)
	mc.connectAttr('%s.os' % loft, '%s.is' % posi)

	# Transform
	tf = mc.group(em=True, n='%sRvt_Grp' % part)
	mc.addAttr(tf, ln='uVal', min=0, max=1, dv=0.5, k=True)
	mc.addAttr(tf, ln='vVal', min=0, max=1, dv=0.5, k=True)

	# aimConstraint
	aimCon = mc.createNode('aimConstraint', n='%s_aimConstraint1' % tf, p=tf)

	# Connections
	mc.connectAttr('%s.position' % posi, '%s.t' % tf)
	mc.connectAttr('%s.n' % posi, '%s.tg[0].tt' % aimCon)
	mc.connectAttr('%s.tv' % posi, '%s.wu' % aimCon)
	mc.connectAttr('%s.cr' % aimCon, '%s.r' % tf)
	mc.connectAttr('%s.uVal' % tf, '%s.parameterU' % posi)
	mc.connectAttr('%s.vVal' % tf, '%s.parameterV' % posi)

	# Lock/Hide attrs
	mc.setAttr('%s.v' % tf, l=True, k=False)
	for attr in ('t', 'r', 's'):
		for ax in ('x', 'y', 'z'):
			mc.setAttr('{tf}.{attr}{ax}'.format(tf=tf, attr=attr, ax=ax), l=True, k=False)

def copyShapeToTargetOrig():
	'''
	Copy shape from the first selection to orig shape of the second selection.
	'''
	srcTf, tarTf = mc.ls(sl=True)

	tarOrig = getOrigShape(tarTf)

	for ix in range(mc.polyEvaluate(srcTf, v=True)):

		currSrcVtx = '{0}.vtx[{1}]'.format(srcTf, ix)
		currTarVtx = '{0}.vtx[{1}]'.format(tarOrig, ix)
		
		mc.xform(currTarVtx, os=True, t=mc.xform(currSrcVtx, q=True, os=True, t=True))

def nextRange(amt=100):
	'''
	Put time range to next 'amt' frames.
	'''
	minTime = mc.playbackOptions(q=True, min=True)
	maxTime = mc.playbackOptions(q=True, max=True)

	if (maxTime + amt) > mc.playbackOptions(q=True, aet=True):
		mc.playbackOptions(min=mc.playbackOptions(q=True, ast=True))
		mc.playbackOptions(max=maxTime - minTime)

	else:
		mc.playbackOptions(min=minTime + amt)
		mc.playbackOptions(max=maxTime + amt)

def selectDefaultSkinJoints():

	jnts = [
				'Pelvis_Jnt',
				'Spine_Jnt',
				'SpineSkin_1_Jnt',
				'SpineSkin_2_Jnt',
				'SpineSkin_3_Jnt',
				'SpineSkin_4_Jnt',
				'SpineSkin_5_Jnt',
				'Chest_Jnt',
				'Neck_Jnt',
				'RbnNeckDtl_1_Jnt',
				'RbnNeckDtl_2_Jnt',
				'RbnNeckDtl_3_Jnt',
				'RbnNeckDtl_4_Jnt',
				'RbnNeckDtl_5_Jnt',
				'Head_Jnt'
			]

	sideJnts = [	
					'UpArm_%s_Jnt', 'Forearm_%s_Jnt', 'Hand_%s_Jnt',
					'RbnUpArmDtl_1_%s_Jnt','RbnUpArmDtl_2_%s_Jnt','RbnUpArmDtl_3_%s_Jnt',
					'RbnUpArmDtl_4_%s_Jnt','RbnUpArmDtl_5_%s_Jnt',
					'RbnForearmDtl_1_%s_Jnt','RbnForearmDtl_2_%s_Jnt','RbnForearmDtl_3_%s_Jnt',
					'RbnForearmDtl_4_%s_Jnt','RbnForearmDtl_5_%s_Jnt',
					'Thumb_1_%s_Jnt','Thumb_2_%s_Jnt',
					'Thumb_3_%s_Jnt','Thumb_4_%s_Jnt',
					'Index_1_%s_Jnt','Index_2_%s_Jnt','Index_3_%s_Jnt',
					'Index_4_%s_Jnt','Index_5_%s_Jnt',
					'Middle_1_%s_Jnt','Middle_2_%s_Jnt','Middle_3_%s_Jnt',
					'Middle_4_%s_Jnt','Middle_5_%s_Jnt',
					'Ring_1_%s_Jnt','Ring_2_%s_Jnt','Ring_3_%s_Jnt',
					'Ring_4_%s_Jnt','Ring_5_%s_Jnt',
					'Pinky_1_%s_Jnt','Pinky_2_%s_Jnt',
					'Pinky_3_%s_Jnt','Pinky_4_%s_Jnt','Pinky_5_%s_Jnt',
					'RbnUpLegDtl_1_%s_Jnt','RbnUpLegDtl_2_%s_Jnt','RbnUpLegDtl_3_%s_Jnt',
					'RbnUpLegDtl_4_%s_Jnt','RbnUpLegDtl_5_%s_Jnt',
					'RbnLowLegDtl_1_%s_Jnt','RbnLowLegDtl_2_%s_Jnt','RbnLowLegDtl_3_%s_Jnt',
					'RbnLowLegDtl_4_%s_Jnt','RbnLowLegDtl_5_%s_Jnt',
					'UpLeg_%s_Jnt', 'LowLeg_%s_Jnt','Ankle_%s_Jnt','Ball_%s_Jnt','Toe_%s_Jnt',
					'ClavSca_%s_Jnt'
				]

	mc.select(cl=True)

	for nd in mc.ls():
		
		for jnt in jnts:
			if nd.endswith(jnt):
				mc.select(nd, add=True)

		for sideJnt in sideJnts:
			for side in ('L', 'R'):
				currSideJnt = sideJnt % side
				if nd.endswith(currSideJnt):
					mc.select(nd, add=True)

def assignShade():
	# Assigning temporary shader to selected objects
	sels = mc.ls(sl=True)
	name = ''

	nameResult = mc.promptDialog(
									title='Shading Name',
									message='Enter Name:',
									button=['OK', 'Cancel'],
									defaultButton='OK',
									cancelButton='Cancel',
									dismissString='Cancel'
								)
	
	if nameResult == 'OK':
		name = mc.promptDialog(query=True, text=True)
	
	if name:
		shadingName = '%sTmp_Mat' % name
		
		if not mc.objExists(shadingName):
			mc.shadingNode('lambert', asShader=True, n=shadingName)

			if name == 'Blue':
				mc.setAttr('%s.color' % shadingName, 0.36, 0.36, 1, type='double3')

			elif name == 'Red':
				mc.setAttr('%s.color' % shadingName, 1, 0.327, 0.327, type='double3')
		
		mc.select(sels, r=True)
		cmd = 'hyperShade -assign %s;' % shadingName
		mm.eval(cmd)
		
		mc.select(shadingName, r=True)

def partVtxMove(tolerance=0.01):
	'''
	Select partial geo, parial deformed geo then target.
	'''
	partGeo, partDeformGeo, targetGeo = mc.ls(sl=True)

	partMshObj = om.MFnMesh(om.MGlobal.getSelectionListByName(partGeo).getDagPath(0))
	partDeformMshObj = om.MFnMesh(om.MGlobal.getSelectionListByName(partDeformGeo).getDagPath(0))
	targetMshObj = om.MFnMesh(om.MGlobal.getSelectionListByName(targetGeo).getDagPath(0))

	partVtcs = ['{obj}.vtx[{no}]'.format(obj=partGeo, no=ix) for ix in range(partMshObj.numVertices)]
	partDeformVtcs = ['{obj}.vtx[{no}]'.format(obj=partDeformGeo, no=ix) for ix in range(partDeformMshObj.numVertices)]
	targetVtcs = ['{obj}.vtx[{no}]'.format(obj=targetGeo, no=ix) for ix in range(targetMshObj.numVertices)]

	partVtcsPos = partMshObj.getPoints(space=om.MSpace.kObject)
	partDeformVtcsPos = partDeformMshObj.getPoints(space=om.MSpace.kObject)
	targetVtcsPos = targetMshObj.getPoints(space=om.MSpace.kObject)

	for ix in range(len(targetVtcs)):
		currTargetPos = targetVtcsPos[ix]
		
		for iy in range(len(partVtcs)):
			currPartVtxPos = partVtcsPos[iy]

			move = True
			for iz in range(3):
				if not currPartVtxPos[iz]-tolerance <= currTargetPos[iz] <= currPartVtxPos[iz]+tolerance:
					move = False

			if move:
				currPartDeformVtxPos = partDeformVtcsPos[iy]
				mc.xform(targetVtcs[ix], ws=True, t=(currPartDeformVtxPos[0], currPartDeformVtxPos[1], currPartDeformVtxPos[2]))
	
	print 'Done...'

def copyWeightBasedOnWorldPosition(tolerance=0.01):
	'''
	Select vertices then object.
	Script will select the vertices of selected object that share the same position as selected vertices.
	'''
	sels = mc.ls(sl=True, fl=True)

	vtxDict = {}
	for ix in range(mc.polyEvaluate(sels[0], v=True)):
		currVtx = '{obj}.vtx[{no}]'.format(obj=sels[0], no=ix)
		currPos = mc.xform(currVtx, q=True, t=True, ws=True)

		vtxDict[currVtx] = currPos

	selVtcs = []
	for ix in range(mc.polyEvaluate(sels[1], v=True)):

		currVtx = '{obj}.vtx[{no}]'.format(obj=sels[1], no=ix)
		currPos = mc.xform(currVtx, q=True, t=True, ws=True)

		for vtx in vtxDict.keys():

			if not vtxDict[vtx][0]-tolerance <= currPos[0] <= vtxDict[vtx][0]+tolerance:
				continue

			if not vtxDict[vtx][1]-tolerance <= currPos[1] <= vtxDict[vtx][1]+tolerance:
				continue

			if not vtxDict[vtx][2]-tolerance <= currPos[2] <= vtxDict[vtx][2]+tolerance:
				continue

			selVtcs.append(currVtx)

	if selVtcs:
		mc.select(vtxDict.keys(), r=True)
		mc.select(selVtcs, add=True)
		mc.copySkinWeights(noMirror=True, surfaceAssociation='closestPoint', influenceAssociation='oneToOne')

def ribbonSurface(vPatch=5):
	'''
	Generating NURBs surface for Ribbon IK
	'''
	nrb = lrc.nurbsPlane(
							p=(0,0,0),
							ax=(0,0,1),
							w=True,
							lr=5,
							d=3,
							u=1,
							v=vPatch,
							ch=0
						)

	nrb.attr('t').v = (0, 2.5, 0)
	nrb.freeze()
	nrb.attr('rp').v = (0, 0, 0)
	nrb.attr('sp').v = (0, 0, 0)
	nrb.scaleShape(0.2)
	
	return nrb

def createNonRollJointChain(parent='', rootJnt='', tipJnt='', part='', side=''):
	'''
	Create a chain of non-roll joints.
	The joint chain will be constrained to 'parent' and the ik handle will be constrained to 'tipJnt'.
	'''
	root = jointAt(rootJnt)
	root.name = '{0}Nr{1}Jnt'.format(part, side)

	rootZr = groupAt(root)
	rootZr.name = '{0}NrJntZr{1}Grp'.format(part, side)

	tip = jointAt(tipJnt)
	tip.name = '{0}NrTip{1}Jnt'.format(part, side)
	tip.parent(root)

	ikh = lrc.IkRp(root, tip)
	ikh.name = '{0}Nr{1}Ikh'.format(part, side)
	ikh.attr('poleVector').v = (0, 0, 0)

	ikhZr = groupAt(ikh)
	ikhZr.name = '{0}NrIkhZr{1}Grp'.format(part, side)

	mc.parentConstraint(parent, rootZr, mo=True)
	mc.pointConstraint(rootJnt, root)
	mc.parentConstraint(parent, ikhZr, mo=True)
	mc.parentConstraint(tipJnt, ikh, mo=True)

	return root, rootZr, tip, ikh, ikhZr

def layerControlToSelected():
	'''
	Create a set of controllers to each selected controller.
	'''
	sels = mc.ls(sl=True)

	for sel in sels:

		par = mc.listRelatives(sel, p=True, type='transform')

		ctrl = jointControl('sphere')
		ctrl.lockHideAttrs('v')
		ctrl.name = '%s_lyrCtrl' % sel

		gmbl = lrc.addGimbal(ctrl)
		gmbl.name = '%s_lyrGmblCtrl' % sel
		gmbl.scaleShape(0.75)

		offAGrp = lrc.group(ctrl)
		offAGrp.name = '%s_lyrCtrlOffAGrp' % sel

		offBGrp = lrc.group(offAGrp)
		offBGrp.name = '%s_lyrCtrlOffBGrp' % sel

		zrGrp = lrc.group(offBGrp)
		zrGrp.name = '%s_lyrCtrlZrGrp' % sel

		mc.parentConstraint(par, zrGrp)

		# If the current selected is joint.
		if mc.nodeType(sel) == 'joint':
			jor = mc.getAttr('%s.jointOrient' % sel)[0]
			mc.setAttr('%s.jointOrientX' % ctrl, jor[0])
			mc.setAttr('%s.jointOrientY' % ctrl, jor[1])
			mc.setAttr('%s.jointOrientZ' % ctrl, jor[2])

		# Match the rotation order.
		mc.setAttr('%s.rotateOrder' % ctrl, mc.getAttr('%s.rotateOrder' % sel))

		# Get the keyable transform attributes.
		trsLocks = [False, False, False]
		for ix, attr in enumerate(('t', 'r', 's')):
			for ax in ('x', 'y', 'z'):
				if not mc.getAttr('%s.%s%s' % (sel, attr, ax), l=True):
					trsLocks[ix] = True

		# Parent constraint
		if trsLocks[0] and trsLocks[1]:
			if mc.cutKey(sel, at='t'): mc.pasteKey(ctrl)
			if mc.cutKey(sel, at='r'): mc.pasteKey(ctrl)
			mc.parentConstraint(gmbl, sel, mo=True)

		# Scale constraint
		if trsLocks[2]:
			if mc.cutKey(sel, at='s'): mc.pasteKey(ctrl)
			mc.scaleConstraint(gmbl, sel, mo=True)

		# Point constraint
		if trsLocks[0] and not trsLocks[1]:
			if mc.cutKey(sel, at='t'): mc.pasteKey(ctrl)
			mc.pointConstraint(gmbl, sel)

		# Orient constraint
		if trsLocks[1] and not trsLocks[0]:
			if mc.cutKey(sel, at='r'): mc.pasteKey(ctrl)
			mc.orientConstraint(gmbl, sel)

def addPartNameToSelected(partName=''):
	
	sels = sorted(mc.ls(sl=True, l=True), reverse=True)
	
	for sel in sels:
		currNm = sel.split('|')[-1]
		newNm = addPartName(currNm, partName)
		mc.rename(sel, newNm)

def addPartName(name='', partName=''):

	ndNm, ndIdx, ndSide, ndType = extractName(name)
	return '{ndNm}{part}{ndIdx}{ndSide}{ndType}'.format(ndNm=ndNm, part=partName, ndIdx=ndIdx, ndSide=ndSide, ndType=ndType)

def changeType(name='', _type=''):
	ndNm, ndIdx, ndSide, ndType = extractName(name)
	ndType = _type

	return composeName(ndNm, ndIdx, ndSide, ndType)

def composeName(ndNm='', ndIdx='', ndSide='', ndType=''):
	'''
	composeName('Arm', '1', 'L', 'Jnt')
	composeName('Arm', '', 'L', 'Jnt')
	composeName('Spine', '', '', 'Jnt')
	'''
	# Capitalize first character.
	ndNm = '%s%s' % (ndNm[0].upper(), ndNm[1:])
	ndType = '%s%s' % (ndType[0].upper(), ndType[1:])

	# Clean side
	ndSide = ndSide.replace('_', '')
	if ndSide:
		ndSide = '_%s_' % ndSide
	else:
		ndSide = '_'

	# Clean index
	ndIdx = ndIdx.replace('_', '')
	if ndIdx:
		ndIdx = '_%s' % ndIdx

	return '%s%s%s%s' % (ndNm, ndIdx, ndSide, ndType)

def extractName(inputName=''):
	'''
	Extract name elements from given inputName.
	'''
	ndNm = ''
	ndIdx = ''
	ndSide = '_'
	ndType = ''

	tmpList = inputName.split('_')
	
	ndNm = tmpList[0]
	
	if len(tmpList) == 4:
		ndIdx = '_%s' % tmpList[1]
		ndSide = '_%s_' % tmpList[2]
		ndType = tmpList[3]

	elif len(tmpList) == 3:
		
		if tmpList[1].isdigit():
			ndIdx = '_%s_' % tmpList[1]
			ndType = tmpList[2]
		else:
			ndSide = '_%s_' % tmpList[1]
			ndType = tmpList[2]

	elif len(tmpList) == 2:
		ndType = tmpList[1]

	return [ndNm, ndIdx, ndSide, ndType]

def execPubScript():
	'''
	Find the 'PubScript' node in the scene.
	Execute commands in 'PubScript.notes' then delete 'PubScript' node.
	'''
	pubScript = 'PubScript'
	if mc.objExists(pubScript):
		cmd = mc.getAttr('%s.notes' % pubScript)
		mc.python(cmd)
		mc.delete(pubScript)
		return True
	else:
		print '%s does not exist.' % pubScript
		return False

def createPubScript():
	'''
	Add PubScript node to the scene.
	'''
	pubScript = 'PubScript'

	if not mc.objExists(pubScript):

		mc.createNode('transform', n=pubScript)
		mc.addAttr(pubScript, ci=True, sn='nts', ln='notes', dt='string')
		
		txt = 'import maya.cmds as mc\n'
		txt += 'import lpRig.rigTools as lrr\n'
		mc.setAttr('%s.nts' % pubScript, txt, type='string')

	return pubScript

def createAssetPubScript():
	'''
	Add PubScript node for asset to the scene.
	'''
	pubScript = createPubScript()
	txt = mc.getAttr('%s.nts' % pubScript)
	txt += 'lrr.importAllReference()\nlrr.removeAllNamespace()\n'
	txt += 'lrr.removeAllShader()\nlrr.removeNonStartupCamera()\n'
	txt += 'lrr.cleanupScene()\nlrr.removeAllAnimCurve()\n'
	txt += 'if mc.objExists("Delete_Grp"):\n	mc.delete("Delete_Grp")\n'
	mc.setAttr('%s.nts' % pubScript, txt, type='string')

	return pubScript

def removeTmpGrp():

	for each in ('Delete_Grp', 'Tmp_Grp'):
		if mc.objExists(each):
			mc.delete(each)

def connectEyeRig(mainRigNs='', eyeRigNs=''):
	'''
	Connect eyeRig to bodyRig
	'''
	for side in ('_L_', '_R_'):

		eyeJnt = lrc.Dag('%sEye%sJnt' % (mainRigNs, side))
		eyeRigCtrlZr = lrc.Dag('%sCntEyeRigCtrlZr%sGrp' % (eyeRigNs, side))

		eyeJnt.attr('r') >> eyeRigCtrlZr.attr('r')

		eyeRigCtrl = lrc.Dag('%sCntEyeRig%sCtrl' % (eyeRigNs, side))
		cntLidZr = lrc.Dag('%sPartCntLidRigJntZr%sGrp' % (eyeRigNs, side))

		followAmper = lrc.MultiplyDivide()
		followAmper.name = 'LidFlw%sMdv' % side

		eyeJnt.attr('r') >> followAmper.attr('i1')
		eyeRigCtrl.attr('lidFollow') >> followAmper.attr('i2x')
		eyeRigCtrl.attr('lidFollow') >> followAmper.attr('i2y')
		eyeRigCtrl.attr('lidFollow') >> followAmper.attr('i2z')
		followAmper.attr('o') >> cntLidZr.attr('r')

def applyVertexMoveWithSkinWeight(baseObj='', srcObj='', tarObj='', skinGeo='', jnt=''):
	'''
	Move vertices based on the different between baseObj and srcObj then multiply by skin weight value of the given jnt that assigned to each vertex.
	'''
	skn = mm.eval('findRelatedSkinCluster("%s")' % skinGeo)
	infs = mc.skinCluster(skn, q=True, inf=True)

	jntIdx = None
	for ix, inf in enumerate(infs):
		if jnt == inf:
			jntIdx = ix

	for ix in xrange(mc.polyEvaluate(skinGeo, v=True)):

		# Vtx name definition.
		baseVtx = '%s.vtx[%s]' % (baseObj, ix)
		srcVtx = '%s.vtx[%s]' % (srcObj, ix)
		skinVtx = '%s.vtx[%s]' % (skinGeo, ix)
		tarVtx = '%s.vtx[%s]' % (tarObj, ix)

		skinVal = mc.skinPercent(skn, skinVtx, q=True, v=True)[jntIdx]

		# Go to next vtx if current vertex has no skin weight value from given jnt
		if not skinVal: continue
		
		# Vector calculation.
		basePos = mc.xform(baseVtx, q=True, os=True, t=True)
		srcPos = mc.xform(srcVtx, q=True, os=True, t=True)

		srcVec = lrc.diff(srcPos, basePos)
		moveVec = [srcVec[0]*skinVal, srcVec[1]*skinVal, srcVec[2]*skinVal]
		tarVec = [basePos[0]+moveVec[0], basePos[1]+moveVec[1], basePos[2]+moveVec[2]]

		mc.xform(tarVtx, os=True, t=tarVec)

	print 'CopyShape Base: %s, Source: %s, to : %s, is Done!!' % (baseObj, srcObj, tarObj)

def removeAllAnimCurve():

	for each in mc.ls(type='animCurve'):

		if not mc.objExists(each):
			continue

		cnncs = mc.listConnections(each, s=True, d=False)

		if not cnncs:
			if mc.objExists(each):
				mc.delete(each)

def assignShader(mat='', obj=''):
	'''
	Assign mat to boj
	'''
	cnncs = mc.listConnections('%s.outColor' % mat, type='shadingEngine')

	sg = None
	if 'initialShadingGroup' in cnncs:
		sg = 'initialShadingGroup'
	else:
		sg = cnncs[0]

	mc.select(obj, r=True)
	mc.sets(e=True, forceElement=sg)

	return True

def quickMirrorPolygon():
	'''
	Mirror first selected polygon to -x of world origin then soft edges.
	'''
	sels = mc.ls(sl=True)

	mc.polyMirrorFace(sels[0], ws=True,  d=1, mm=1, mt=0.001, p=(0, 0, 0), ch=False)
	mc.polySoftEdge(sels[0], a=180, ch=False)
	mc.select(sels[0], r=True)

def doAddBlendShape(bshVal=1):
	'''
	Select two objects then call the procedure.
	Add the first selected object to the blend shape node that connected to the second selected object.
	'''
	sels = mc.ls(sl=True)

	bshs = findRelatedBlendshape(sels[-1])

	if not bshs:
		# Create front of chain blend shape if no blend shape is connected to the target.
		createFrontOfChainBlendShape(bshVal)
	else:
		for ix in range(len(sels)-1):
			addBlendShape(bshs[0], sels[ix], sels[-1], bshVal)

	return True

def addBlendShape(bsn='', bshObj='', targetObj='', bshVal=1):
	'''
	Add bshObj to bsn as the last blend shape attr.
	'''
	# Build the alias attribute dictionary.
	# Keys are weight numbers and values are attribute names.
	bshAttrs = mc.aliasAttr(bsn, q=True)
	
	bshIdx = 0
	if bshAttrs:
		bshAttrDict = {}
		for ix in range(1, len(bshAttrs), 2):
			exp = r'([0-9]+)'
			m = re.search(exp, bshAttrs[ix])
			bshAttrDict[int(m.group(1))] = bshAttrs[ix-1]

		bshIdx = sorted(bshAttrDict.keys())[-1] + 1

	mc.blendShape(bsn, e=True, t=(targetObj, bshIdx, bshObj, 1))

	mc.setAttr('%s.weight[%s]' % (bsn, bshIdx), bshVal)

def findRelatedBlendshape(obj=''):
	'''
	Find the blend shape nodes that connected to the shape node of given transform node.
	'''
	chdn = mc.listRelatives(obj, ad=True)

	# Quit if there is no related shape node.
	if not chdn: return False
	
	# Find related objectSet.
	objSets = mc.listConnections(chdn[0], type='objectSet', d=True, s=False)

	# Quit, if current shape has no related objectSet.
	if not objSets: return False

	bshs = []

	for objSet in objSets:

		relBshs = mc.listConnections(objSet, type='blendShape', s=True, d=False)

		# Continue, if current object set has no related blend shape node.
		if not relBshs: continue

		for relBsh in relBshs:
			if not relBsh in bshs:
				bshs.append(relBsh)

	return bshs

def createFrontOfChainBlendShape(bshVal=1):

	# Create blend shape node with 'frontOfChain' option.
	# Select source geo and target geo then run the procedure.

	sels = mc.ls(sl=True)

	localName = sels[-1].split(':')[-1]
	tmpNameList = localName.split('_')

	if len(tmpNameList) > 1:
		tmpNameList[-1] = 'Bsn'
	else:
		tmpNameList.append('Bsn')

	bsn = '_'.join(tmpNameList)
	mc.blendShape(frontOfChain=True, origin='local', n=bsn)

	for ix in range(len(sels)-1):
		mc.setAttr('%s.w[%s]' % (bsn, ix), bshVal)

def removeNonStartupCamera():
	
	for each in mc.ls(type='camera'):
		if not mc.camera(each, q=True, startupCamera=True):
			try:
				mc.delete(mc.listRelatives(each, type='transform', p=True)[0])
			except:
				print '%s cannot be deleted' % each

def getNs(nodeName=''):
	'''
	Get namespace from given name
	Ex. aaa:bbb:ccc:ddd_grp > aaa:bbb:ccc:
	'''
	ns = ''

	if '|' in nodeName:
		nodeName = nodeName.split('|')[-1]

	if ':' in nodeName:

		nmElms = nodeName.split(':')

		if nmElms:
			ns = '%s:' % ':'.join(nmElms[0:-1])

	return ns

def dupShade():
	'''
	Duplicating material from first selected to last selected object.
	'''
	sels = mc.ls(sl=True, l=True)
	
	mc.hyperShade(smn=True)
	
	refMat = mc.ls(sl=True)[0]
	
	if ':' in refMat:
		matName = refMat.split(':')[-1]
	else:
		matName = refMat
	
	if not mc.objExists(matName):
		mc.duplicate(upstreamNodes=True)
	
	for sel in sels:
		if not sel == sels[0]:
			mc.select(sel, r=True)
			mc.hyperShade(assign=matName)
	
	mc.select(sels, r=True)

def dupExactShade():
	"""Assign materials to another duplicated object.
	"""
	src, tar = mc.ls(sl=True)
	srcShp = mc.listRelatives(src, s=True)[0]
	tarShp = mc.listRelatives(tar, s=True)[0]
	
	ogSgs = list(set(mc.listConnections(srcShp, type='shadingEngine')))
	
	mats = []
	for ogSg in ogSgs:

		currMat = mc.listConnections('%s.surfaceShader' % ogSg, s=True, d=False)[0]

		# If current mat is not referenced.
		if not mc.referenceQuery(currMat, isNodeReferenced=True):
			mats.append(currMat)
			continue

		# If current mat is already duplicated.
		currMatNm = currMat.split(':')[-1]
		if mc.objExists(currMatNm):
			mats.append(currMatNm)
			continue
		
		duppedMat = mc.duplicate(currMat, upstreamNodes=True)[0]
		mats.append(duppedMat)

	for ix in xrange(len(mats)):

		# Object is assigned to current SG.
		if mc.sets(src, isMember=ogSgs[ix]) or mc.sets(srcShp, isMember=ogSgs[ix]):
			mc.select(tar, r=True)
			mc.hyperShade(assign=mats[ix])
			continue

		# Faces is assigned to current SG.
		for iy in xrange(mc.polyEvaluate(src, f=True)):
			currSrcFc = '%s.f[%s]' % (src, iy)
			currTarFc = '%s.f[%s]' % (tar, iy)
			if mc.sets(currSrcFc, isMember=ogSgs[ix]):
				# mc.sets(currTarFc, e=True, forceElement=sgs[ix])
				mc.select(currTarFc, r=True)
				mc.hyperShade(assign=mats[ix])

def locatorOnMidPos():
	'''
	Create locator at the middle of selected objects.
	'''
	sels = mc.ls(sl=True, fl=True, l=True)
	no = len(sels)
	posSum = [0,0,0]
	loc = lrc.Locator()
	
	for sel in sels:
		
		currPos = mc.xform(sel, q=True, t=True, ws=True)
		posSum[0] = float(posSum[0] + currPos[0])
		posSum[1] = float(posSum[1] + currPos[1])
		posSum[2] = float(posSum[2] + currPos[2])
	
	loc.attr('tx').v = posSum[0]/no
	loc.attr('ty').v = posSum[1]/no
	loc.attr('tz').v = posSum[2]/no
	
	return loc

def addGimbalToSelected():
	'''
	Add gimbal helper controller to selected nodes.
	'''
	sels = mc.ls(sl=True, l=True)
	gmbCtrls = []

	for sel in sels:

		ctrl = lrc.Dag(sel)
		ctrlShp = lrc.Dag(ctrl.shape)

		tmpCtrl = mc.duplicate(ctrl, rr=True)[0]

		ndNm = sel.split('|')[-1]
		gmbNm = ''

		if '_' in ndNm:
			nmElms = ndNm.split('_')
			nmElms[0] = '%s%sGmbl' % (nmElms[0], nmElms[-1])
			nmElms[-1] = 'Ctrl'

			gmbNm = '_'.join(nmElms)

		else:
			gmbNm = '%sGmbl_Ctrl' % ndNm

		gmbCtrl = lrc.addGimbal(ctrl)
		gmbCtrl.name = gmbNm

		gmbCtrls.append(gmbCtrl)

		mc.delete(tmpCtrl)

		chdn = mc.listRelatives(ctrl, type='transform', c=True)
		for chd in chdn:
			if not chd == gmbCtrl.name:
				mc.parent(chd, gmbCtrl)

	return gmbCtrls

def cleanAllDeathIntermediate():
	'''
	Delete all unused intermediated shape
	'''
	for mesh in mc.ls(type="mesh"):
		if mc.getAttr(mesh+'.io'):
			if not mc.listConnections(mesh):
				mc.delete(mesh)
				print '%s has been deleted' % mesh

def mirrorOrientSelected(aimVec=(1,0,0), upVec=(0,1,0)):
	sels = mc.ls(sl=True)

	for each in sels[1:]:
		mirrorOrient(each, sels[0], aimVec=aimVec, upVec=upVec)

	mc.select(sels, r=True)

def mirrorOrient(node, target, aimVec=(1,0,0), upVec=(0,1,0)):
	'''
	Match the orientation of given node to the target using aim constraint
	'''
	tmpTarget = mc.group(em=True)
	tmpUp = mc.group(em=True)

	mc.parent(tmpTarget, target)
	mc.parent(tmpUp, target)

	for each in (tmpTarget, tmpUp):
		for attr in ('tx', 'ty', 'tz', 'rx', 'ry', 'rz'):
			mc.setAttr('%s.%s' % (each, attr), 0)
		for attr in ('sx', 'sy', 'sz'):
			mc.setAttr('%s.%s' % (each, attr), 1)

	mc.setAttr('%s.t' % tmpTarget, aimVec[0], aimVec[1], aimVec[2])
	mc.setAttr('%s.t' % tmpUp, upVec[0], upVec[1], upVec[2])

	piv = mc.group(em=True)
	mc.parent(tmpTarget, piv)
	mc.parent(tmpUp, piv)

	mc.setAttr('%s.sx' % piv, -1)

	mc.delete(
				mc.aimConstraint(
									tmpTarget,
									node,
									aim=aimVec,
									upVector=upVec,
									wuo=tmpUp,
									wut='object',
								)
			)

	mc.delete(piv)

def selectPlaceCtrl():
	'''
	Select place controller from selected referenced node.
	'''
	sels = mc.ls(sl=True, l=True)
	plcCtrls = []

	for sel in sels:

		refNd = mc.referenceQuery(
									sel,
									referenceNode=True,
									topReference=True
								)
		nodes = mc.referenceQuery(refNd, n=True, dp=True)
		
		for node in nodes:
			if node.endswith('Place_Ctrl'):
				plcCtrls.append(node)

	mc.select(plcCtrls, r=True)

def disableSelected():
	'''
	Scale shape of selected node to zero and disable all keyable attributes.
	'''
	sels = mc.ls(sl=True)
	for sel in sels:
		
		ctrl = lrc.Dag(sel)
		ctrl.scaleShape(0)

		for attr in mc.listAttr(ctrl, k=True):
			
			ctrl.attr(attr).lockHide()
		
		shp = lrc.Dag(ctrl.shape)
		
		if mc.objExists(shp.attr('gimbalControl')):
			
			gmbl = lrc.Dag(mc.listConnections(shp.attr('gimbalControl'), d=True, s=False)[0])
			gmbl.scaleShape(0)

			for attr in mc.listAttr(gmbl, k=True):
				gmbl.attr(attr).lockHide()
	
	mc.select(sels, r=True)

def transferUvToOrig():
	'''
	Transfer UVs from first selected object to the orig shape of last selected object.
	'''
	sels = mc.ls(sl=True)

	origShape = getOrigShape(sels[-1])

	intermediate = mc.getAttr('%s.intermediateObject' % origShape)

	if intermediate:
		mc.setAttr('%s.intermediateObject' % origShape, 0)

	mc.select(sels[0], r=True)
	mc.select(origShape, add=True)

	cmd = 'transferAttributes -pos 0 -nml 0 -uvs 2 -col 2 -spa 4 -suv "map1" -tuv "map1" -sm 3 -fuv 0 -clb 1;'
	mm.eval(cmd)
	mc.delete(origShape, ch=True)

	if intermediate:
		mc.setAttr('%s.intermediateObject' % origShape, 1)

	mc.select(sels, r=True)

def groupOnSelected(part=''):
	'''
	Create upper group on selected
	'''
	sels = mc.ls(sl=True, l=True)
	grps = []

	for sel in sels:

		pars = mc.listRelatives(sel, p=True)
		ndNm = sel.split('|')[-1]
		grpNm = ''

		if '_' in ndNm:
			nmElms = ndNm.split('_')
			nmElms[0] = '%s%s%s' % (nmElms[0], nmElms[-1], part)
			nmElms[-1] = 'Grp'

			grpNm = '_'.join(nmElms)

		grp = groupAt(sel)
		if grpNm:
			grp.name = grpNm
		else:
			grp.name = '%s_zr_grp' % ndNm

		if pars:
			grp.parent(pars[0])
		grps.append(grp)

	return grps

def zrGrpOnSelected():
	'''
	Create Zr grp on selected
	'''
	return groupOnSelected('Zr')

def invGrpOnSelected():
	'''
	Create Inv grp on selected
	'''
	return groupOnSelected('Inv')

def detachSelectedEdge():

	objs = []

	for sel in mc.ls(sl=True, fl=True):

		obj = sel.split('.')[0]

		if not obj in objs:
			objs.append(obj)
	
	mm.eval('DetachComponent')
	mc.select(objs, r=True)
	mm.eval('SeparatePolygon')
	mc.delete(ch=True)

def selectChildJoint():
	'''
	Select all child joint of the selected nodes
	'''
	sels = mc.ls(sl=True, l=True)
	jnts = []

	for sel in sels:
		currJnts = []

		if mc.nodeType(sel) == 'joint':
			currJnts.append(sel)

		chdn = mc.listRelatives(sel, ad=True, type='joint')
		if chdn:
			for chd in chdn:
				if mc.nodeType(chd) == 'joint':
					currJnts.append(chd)

		if currJnts:
			for currJnt in currJnts:
				if not currJnt in jnts:
					jnts.append(currJnt)
	
	mc.select(jnts, r=True)

def copyWeight():
	'''
	Copy skin weights from first selected object to the rest selected object(s).
	Select source geometry then target geometries then run script.
	'''
	sels = mc.ls(sl=True)
	
	jnts = mc.skinCluster(sels[0], q=True, inf=True)

	for sel in sels[1:]:
	
		oSkn = mm.eval('findRelatedSkinCluster("%s")' % sel)
		if oSkn:
			mc.skinCluster(oSkn, e=True, ub=True)
		
		skn = mc.skinCluster(jnts, sel, tsb=True)[0]
		
		mc.select(sels[0], r=True)
		mc.select(sel, add=True)
		mm.eval('copySkinWeights  -noMirror -surfaceAssociation closestPoint -influenceAssociation closestJoint;')

def copyCurveShape(src='', target=''):
	'''
	Copy cv's positions from 'src' to 'target'
	'''
	crv = lrc.Dag(target)
	
	crvShape = lrc.Dag(crv.shape)
	cv = crvShape.attr('spans').value + crvShape.attr('degree').value
	
	origCrv = lrc.Dag(src)
	origCrvShape = lrc.Dag(origCrv.shape)
	
	for ix in range(0, cv):
		pos = mc.xform('%s.cv[%s]' % (origCrvShape, str(ix)), q=True, t = True, ws=True)
		mc.xform('%s.cv[%s]' % (crv.name, str(ix)), t=(pos[0], pos[1], pos[2]), ws=True)

def scaleHelperControl(size=0.2):
	'''
	Scale down gimbal controller for the given size and
	scale up con controller for the given size.
	'''
	for gmblCtrl in mc.ls('*Gmbl*Ctrl'):
		
		ctrl = lrc.Dag(mc.listRelatives(gmblCtrl, p=True, type='transform')[0])
		gmbl = lrc.Dag(gmblCtrl)
		copyCurveShape(ctrl, gmbl)
		gmbl.scaleShape(1-size)

	for conCtrl in mc.ls('*Con*Ctrl'):
		
		ctrl = lrc.Dag(mc.listRelatives(conCtrl, c=True, type='transform')[0])
		con = lrc.Dag(conCtrl)
		copyCurveShape(ctrl, con)
		con.scaleShape(1+size)

def quickOrient(aimVec=[0, 1, 0], upVec=[-1, 0, 0]):
	
	sels = mc.ls(sl=True)
	targ = sels[0]
	src = sels[1]
	
	chldrn = mc.listRelatives(src, s=False, c=True, p=False, type='transform')

	# Parent all children to world.
	if chldrn:
		for chld in chldrn:
			mc.parent(chld, w=True)
	
	# Do the local aim.
	localAim(targ, src, aimVec, upVec)
	
	if chldrn:
		for chld in chldrn:
			mc.parent(chld, src)

			grandChldrn = mc.listRelatives(chld, s=False, c=True, p=False)

			if not grandChldrn and mc.nodeType(chld) == 'joint':
				mc.setAttr('%s.jointOrient' % chld, 0, 0, 0)
	
	mc.select(src, r=True)

def localAim(targ='', src='', aimVec=[0,1,0], upVec=[-1,0,0]):
	'''
	Use current object's rotation up as an up vector and aim to the given targ
	'''
	tmpUp = mc.duplicate(src, rr=True)[0]
	tmpUpChldrn = mc.listRelatives(tmpUp, f=True)
	
	if tmpUpChldrn:
		mc.delete(tmpUpChldrn)

	aim = aimVec
	wu = upVec
	u = upVec
	wuo = tmpUp
	wut = 'objectrotation'

	aimCon = mc.aimConstraint(
								targ,
								src,
								aim=aim,
								wu=wu,
								wuo=wuo,
								wut=wut,
								u=u
							)
	mc.delete(aimCon)
	mc.delete(tmpUp)

def renameAllSgs():
	'''
	Rename all shading groups according to connected materials.
	'''
	sgs = mc.ls(type='shadingEngine')
	exceptions = ('initialShadingGroup', 'initialParticleSE')

	if not sgs:
		return False

	for sg in sgs:

		if sg in exceptions:
			continue

		sgAttr = '%s.surfaceShader' % sg

		if not mc.objExists(sgAttr):
			continue

		cnncs = mc.listConnections(sgAttr)

		if not cnncs:
			continue

		mc.rename(sg, '%sSg' % cnncs[0])

def renameClsElms(cls=''):
	
	renameList = ['groupPats', 'groupId', 'objectSet']

	for cnnc in mc.listConnections(cls, s=True, d=True):
		nodeType = mc.nodeType(cnnc)
		if nodeType in renameList:
			mc.rename(cnnc, '%s%s%s' % (cls, nodeType[0].upper(), nodeType[1:]))

		if (nodeType == 'joint') or (nodeType == 'transform'):
			shps = mc.listRelatives(cnnc, s=True, f=True)
			for shp in shps:
				shpType = mc.nodeType(shp)
				if shpType == 'clusterHandle':
					mc.rename(shp, '%sHandleShape' % cls)

def crvGuide(ctrl='', target=''):
	'''
	Create NURBs curve between control and target
	Returns: curve and two clusters
	'''
	crv = lrc.Dag(mc.curve(d=1, p=[(0, 0, 0), (0, 0, 0) ]))
	clstr1 = lrc.Dag(mc.cluster('%s.cv[0]' % crv, wn=(ctrl, ctrl))[0 ])
	clstr2 = lrc.Dag(mc.cluster('%s.cv[1]' % crv, wn=(target, target))[0 ])
	mc.select(cl=True)
	
	return crv, clstr1, clstr2

def doParLocWarToSelected():
	'''
	Select controller, local object, world object and orient group then run this proc
	'''
	sels = mc.ls(sl=True)

	doParLocWar(sels[0], sels[1], sels[2], sels[3])

def doParLocWar(ctrl='', localObj='', worldObj='', parGrp=''):
	'''
	Run parentLocalWorldCtrl() the rename the returned objects.
	'''
	(locGrp, worGrp, worGrpParCons,
	parGrpParCons, parGrpParConsRev) = parLocWor(ctrl, localObj, worldObj, parGrp)

	ndNm, ndIdx, ndSide, ndType = extractName(ctrl.name)

	locGrp.name = composeName('%s%sLocPar' % (ndNm, ndType), ndIdx, ndSide, 'Grp')
	worGrp.name = composeName('%s%sWorPar' % (ndNm, ndType), ndIdx, ndSide, 'Grp')
	worGrpParCons.name = '%s_parentConstraint' % worGrp
	parGrpParCons.name = '%s_parentConstraint' % parGrp
	parGrpParConsRev.name = composeName('%s%sLocWorPar' % (ndNm, ndType), ndIdx, ndSide, 'Rev')

	return locGrp, worGrp, worGrpParCons, parGrpParCons, parGrpParConsRev

def parentLocalWorldCtrl(ctrl='', localObj='', worldObj='', parGrp=''):
	'''
	Blending parent between local and world object.
	Returns: locGrp, worGrp, worGrpParCons, parGrpParCons and parGrpParConsRev
	'''
	locGrp = lrc.Null()
	worGrp = lrc.Null()
		
	locGrp.snap(parGrp)
	worGrp.snap(parGrp)
	
	worGrpParCons = lrc.parentConstraint(worldObj, worGrp, mo=True)
	parGrpParCons = lrc.parentConstraint(locGrp, worGrp, parGrp)
	parGrpParConsRev = lrc.Reverse()
	
	con = lrc.Dag(ctrl)
	
	attr = 'localWorld'
	con.add(ln=attr, k=True, min=0, max=1)
	con.attr(attr) >> parGrpParCons.attr('w1')
	con.attr(attr) >> parGrpParConsRev.attr('ix')
	parGrpParConsRev.attr('ox') >> parGrpParCons.attr('w0')
	
	locGrp.parent(localObj)
	worGrp.parent(localObj)
	
	return locGrp, worGrp, worGrpParCons, parGrpParCons, parGrpParConsRev

def parLocWor(ctrl='', localObj='', worldObj='', parGrp=''):
	'''
	Short call for parentLocalWorldCtrl
	'''
	return parentLocalWorldCtrl(ctrl, localObj, worldObj, parGrp)

def orientLocalWorldCtrl(ctrl='', localObj='', worldObj='', oriGrp=''):
	'''
	Blending orientation between local and world object.
	Returns: locGrp, worGrp, worGrpOriCons, oriGrpOriCons and oriGrpOriConsRev
	'''
	locGrp = lrc.Null()
	worGrp = lrc.Null()
	locGrp.snap(oriGrp)
	worGrp.snap(oriGrp)
	
	oLoc = str(locGrp.name)
	oWor = str(worGrp.name)
	
	locGrp.name = 'local'
	worGrp.name = 'world'
	
	worGrpOriCons = lrc.orientConstraint(worldObj, worGrp, mo = True)
	oriGrpOriCons = lrc.orientConstraint(locGrp, worGrp, oriGrp)
	oriGrpOriConsRev = lrc.Reverse()
	
	locGrp.name = oLoc
	worGrp.name = oWor
	
	con = lrc.Dag(ctrl)
	
	attr = 'localWorld'
	con.add(ln=attr, k=True, min=0, max=1)
	con.attr(attr) >> oriGrpOriCons.attr('w1')
	con.attr(attr) >> oriGrpOriConsRev.attr('ix')
	oriGrpOriConsRev.attr('ox') >> oriGrpOriCons.attr('w0')
	
	locGrp.parent(localObj)
	worGrp.parent(localObj)
	
	return locGrp, worGrp, worGrpOriCons, oriGrpOriCons, oriGrpOriConsRev

oriLocWor = orientLocalWorldCtrl

def doOriLocWar(ctrl='', localObj='', worldObj='', parGrp=''):
	'''
	Run orientLocalWorldCtrl() the rename the returned objects.
	'''
	(locGrp, worGrp, worGrpParCons,
	parGrpParCons, parGrpParConsRev) = oriLocWor(ctrl, localObj, worldObj, parGrp)

	ndNm, ndIdx, ndSide, ndType = extractName(ctrl)

	locGrp.name = composeName('%s%sLocOri' % (ndNm, ndType), ndIdx, ndSide, 'Grp')
	worGrp.name = composeName('%s%sWorOri' % (ndNm, ndType), ndIdx, ndSide, 'Grp')
	worGrpParCons.name = '%s_parentConstraint' % worGrp
	parGrpParCons.name = '%s_parentConstraint' % parGrp
	parGrpParConsRev.name = composeName('%s%sLocWorOri' % (ndNm, ndType), ndIdx, ndSide, 'Rev')

	return locGrp, worGrp, worGrpParCons, parGrpParCons, parGrpParConsRev

def attrAmper(ctrlAttr='', targetAttr='', dv=1, ampAttr=''):
	'''
	Create attribute amplifier
	Returns: multiDoubleLinear
	'''
	shape = lrc.Dag('')
	ctrl = str(ctrlAttr).split('.')[0]
	try:
		shape = lrc.Dag(mc.listRelatives(ctrl)[0])
	except:
		pass
	mul = lrc.MultDoubleLinear()
	mul.add(ln='amp', k=True, dv=dv)
	
	if shape.exists and ampAttr:
		if shape.attr(ampAttr).exists:
			shape.attr(ampAttr) >> mul.attr('amp')
		else:
			shape.add(ln=ampAttr, k=True, dv=dv)
			shape.attr(ampAttr) >> mul.attr('amp')
	
	mul.attr('amp') >> mul.attr('i1')
	mc.connectAttr(ctrlAttr, '%s.i2' % mul.name)
	mc.connectAttr('%s.o' % mul.name, targetAttr, f=True)
	mc.select(cl=True)
	
	return mul

def fkSquash(ctrl = '', attr = '', target = '', ax = 'y'):
	'''
	Create FK squash by connecting to the other two axes
	Returns: addDoubleLinear and multiDoulbleLinear
	'''
	if attr:
		if not mc.objExists('%s.%s' % (ctrl, attr)):
			mc.addAttr(ctrl, ln=attr, at='float', k=True)		
	else:
		if mc.objExists('%s.squash' % ctrl):
			attr = 'squash'
		else:
			mc.addAttr(ctrl, ln='squash', at='float', k=True)
			attr = 'squash'
	
	add = lrc.AddDoubleLinear()
	
	mul = attrAmper('%s.%s' % (ctrl, attr), '%s.i2' % add, dv=0.1)
	add.add(ln='default', k=True, dv=1)
	add.attr('default') >> add.attr('i1')
	
	for eachAx in ('x', 'y', 'z'):
		if not eachAx == ax:
			add.attr('o') >> '%s.s%s' % (target, eachAx)
	
	mc.select(cl=True)
	
	return add, mul

def fkStretch(ctrl='', attr='', target='', ax='y'):
	'''
	Create stretchable FK controller using translate of given 'ax' to given 'target'
	Returns: addDoubleLinear and multiDoulbleLinear
	'''
	if attr:
		if not mc.objExists('%s.%s' % (ctrl, attr)):
			mc.addAttr(ctrl, ln=attr, at='float', k=True)		
	else:
		if mc.objExists('%s.stretch' % ctrl):
			attr = 'stretch'
		else:
			mc.addAttr(ctrl, ln='stretch', at='float', k=True)
			attr = 'stretch'
	
	add = lrc.AddDoubleLinear()
	
	dv = mc.getAttr('%s.t%s' % (target, ax))
	mul = attrAmper('%s.%s' % (ctrl, attr), '%s.i2' % add, dv=dv)
	add.add(ln='default', k=True, dv=dv)
	add.attr('default') >> add.attr('i1')
	add.attr('o') >> '%s.t%s' % (target, ax)
	
	mc.select(cl=True)
	
	return add, mul

def cleanupScene():
	'''
	Remove every node in nodeTypes.
	'''
	nodeTypes = ['script', 'renderLayer', 'displayLayer']
	exceptions = ['defaultRenderLayer', 'defaultLayer']

	for nodeType in nodeTypes:
		currNodes = mc.ls(type=nodeType)

		if not currNodes: continue # Pass if no node with current node type.

		for currNode in currNodes:

			if currNode in exceptions: continue # Pass if current node is in exception list.

			try:
				mc.delete(currNode)
				print '%s has been deleted.' % currNode
			except:
				print '%s cannot be deleted.' % currNode

	for msh in mc.ls(type='mesh'):
		mc.setAttr('%s.displaySmoothMesh' % msh, 0)

	removeAllColorSets()

def getAllMeshTransforms():

	meshes = mc.ls(type='mesh', l=True)

	if not meshes:
		return False

	meshTfs = []
	for mesh in meshes:

		pars = mc.listRelatives(mesh, p=True, f=True)

		if not pars[0] in meshTfs:
			meshTfs.append(pars[0])

	return meshTfs

def softEdgeAllMeshes():

	meshTfs = getAllMeshTransforms()

	if not meshTfs:
		return False

	mc.select(meshTfs, r=True)

	softEdgeSelectedMesh()

def softEdgeSelectedMesh():
	
	sels = mc.ls(sl=True)
	
	for sel in sels:
		
		shp = getOrigShape(sel)
		
		intermediate = mc.getAttr('%s.intermediateObject' % shp)
		
		if intermediate:
			mc.setAttr('%s.intermediateObject' % shp, 0)
		
		mc.polySoftEdge(shp, a=180, ch=False)
		mc.delete(shp, ch=True)
		
		if intermediate:
			mc.setAttr('%s.intermediateObject' % shp, 1)

	mc.select(sels, r=True)

def unlockAllNormals():

	meshTfs = getAllMeshTransforms()

	if not meshTfs:
		return False

	mc.select(meshTfs, r=True)

	editSelectedOrig('polyNormalPerVertex -ufn true')

def editSelectedOrig(cmd=''):
	
	# polyNormalPerVertex -ufn true
	# polySoftEdge -a 180 -ch 1
	
	sels = mc.ls(sl=True)
	
	for sel in sels:
		
		shp = getOrigShape(sel)

		intermediate = mc.getAttr('%s.intermediateObject' % shp)

		if intermediate:
			mc.setAttr('%s.intermediateObject' % shp, 0)
		
		mm.eval('%s %s' % (cmd, shp))
		mc.delete(shp, ch=True)
		
		if intermediate:
			mc.setAttr('%s.intermediateObject' % shp, 1)

	mc.select(sels, r=True)

def removeAllColorSets():

	meshTfs = getAllMeshTransforms()

	if not meshTfs:
		return False

	for meshTf in meshTfs:

		shp = getOrigShape(meshTf)

		colorSets = mc.polyColorSet(shp, q=True, allColorSets=True)
	
		if colorSets:
			for colorSet in colorSets:
				mc.polyColorSet(shp, delete=True, colorSet=colorSet)
				print 'Color set "%s" has been deleted from %s' % (colorSet, shp)

def jointControl(crvType = ''):
	'''
	Create a controller using joint node.
	'''
	jnt = lrc.Joint()
	jnt.createCurve(crvType)
	jnt.attr('radius').v = 0
	jnt.attr('radius').lock = 1
	jnt.attr('radius').hide = 1
	
	return jnt

def jointAt(obj):
	'''
	Create a joint object at the postion of the given object.
	'''
	target = lrc.Dag(obj)
	
	jnt = lrc.Joint()
	
	jnt.snap(target)
	jnt.freeze(r=True, s=True)
	jnt.rotateOrder = target.rotateOrder
	if target.attr('radius').exists:
		jnt.attr('radius').v = target.attr('radius').v
	mc.select(cl=True)
	
	return jnt

def groupAt(obj):
	'''
	Create a group at the obj's position.
	'''
	target = lrc.Dag(obj)
	
	grp = lrc.Null()
	
	grp.snap(target)
	mc.select(cl=True)
	target.parent(grp)
	
	return grp

def getOrigShape(transform=''):
	'''
	Get origShape from given transform node
	'''
	origShp = None

	shps = mc.listRelatives(transform, s=True, f=True)

	if not shps:
		return None

	if len(shps) == 1:
		return shps[0]

	for shp in shps:
		if mc.getAttr('%s.intermediateObject' % shp):
			connections = mc.listConnections(shp, s=True, d=False)
			if not connections:
				return shp

def flipSelectedOrigNormal():

	sels = mc.ls(sl=True, l=True)
	origShps = []

	for sel in sels:
		currOrigShp = getOrigShape(sel)
		if not currOrigShp in origShps:
			origShps.append(currOrigShp)

	if origShps:

		for origShp in origShps:

			mc.setAttr('%s.intermediateObject' % origShp, 0)
			cmd = 'polyNormal -normalMode 0 -userNormalMode 0 -ch 1 %s;' % origShp
			mm.eval(cmd)
			mc.delete(origShp, ch=True)
			mc.setAttr('%s.intermediateObject' % origShp, 1)

	mc.select(sels, r=True)

def removeAllNamespace():
	'''
	Remove all name space in current scene.
	'''
	exceptions = ['UI', 'shared' ]

	nss = sorted(mc.namespaceInfo(lon=True, r=True), key=len, reverse=True)

	for ns in nss:

		if ns in exceptions:
			continue

		removeAllNodeInNamespace(ns)
		mc.namespace(rm=ns)

def removeAllNodeInNamespace(ns=''):
	# Remove every nodes that belong to given namespace.
	nodes = sorted(mc.ls('%s:*' % ns, l=True), key=len, reverse=True)
	mc.namespace(set=':')

	if not nodes:
		return False

	for node in nodes:

		if not mc.objExists(node): continue

		lockState = mc.lockNode(node, q=True)[0]

		if lockState:
			mc.lockNode(node, l=False)
		newName = node
		try:
			newName = mc.rename(node, node.split(':')[-1])
		except:
			pass
		mc.lockNode(newName, l=lockState)

def removeAllShader():

	exceptions = ('initialShadingGroup', 'initialParticleSE')

	for each in mc.ls(type='shadingEngine'):
		if not each in exceptions:

			shaders = mc.listConnections('%s.surfaceShader' % each)
			if shaders:
				try:
					mc.delete(shaders[0])
					print '%s has been deleted.' % shaders[0]
				except:
					print '%s cannot be deleted.' % shaders[0]
			try:
				mc.delete(each)
				print '%s has been deleted.' % each
			except:
				print '%s cannot be deleted.' % each

	for each in mc.ls(type='mesh'):
		mc.select(each, r=True)
		assignShader('lambert1', each)

def importAllReference():
	
	'''
	Import all reference node in current scene.
	Return imported reference nodes.
	'''
	rfns = sorted(mc.ls(type='reference'), reverse=True)
	impRfns = []
	
	for rfn in rfns:
		
		if not rfn == 'sharedReferenceNode':
			
			try:
				
				fn = mc.referenceQuery(rfn, filename=True)
				mc.file(fn, importReference=True)
				print '%s has been imported.' % rfn
				impRfns.append(rfn)
				
			except RuntimeError:
				
				print '%s is not connected to reference file.' % rfn
				mc.lockNode(rfn, l=0)
				mc.delete(rfn)
			
	return impRfns

def adjustRadius(default=1):
	'''
	Adjust radius to each type of joint
	'''
	jnts = mc.ls(type='joint')
	scaJnts = mc.ls('*Sca*_jnt')
	dtlJnts = mc.ls('*RbnDtl*_jnt')

	for jnt in jnts:
		rad = default
		if 'Sca' in jnt:
			rad = default*2
		elif 'RbnDtl' in jnt:
			rad = default*2
		elif 'RbnDtl3' in jnt:
			rad = rad*2
		else:
			rad = default

		try:
			mc.setAttr('%s.radius' % jnt, rad)
		except:
			pass

def removeSelectedReference():
	# Remove selected reference from the scene
	editSelectedReference(opr='remove')

def importSelectedReference():
	# Import selected reference from the scene
	editSelectedReference(opr='import')

def reloadSelectedReference():
	# Reload selected reference from the scene
	editSelectedReference(opr='reload')

def editSelectedReference(opr='reload'):

	# Edit selected reference from the scene, regarding given operation.
	# Valid operatoins are 'remove', 'import' and 'reload'.
	sels = mc.ls(sl=True)
	
	for sel in sels:
		
		if mc.objExists(sel) and mc.referenceQuery(sel, isNodeReferenced=True):
			
			refNode = mc.referenceQuery(sel, referenceNode=True, topReference=True)
			fileName = mc.referenceQuery(refNode, filename=True)

			if opr == 'reload ':
				mc.file(fileName, lr=True)
			elif opr == 'remove':
				mc.file(fileName, rr=True)
			elif opr == 'import':
				mc.file(fileName, i=True)

def addCheckRigKeyToSelected(vals=[], frameRange=10, oneTime=False):
	'''
	Adding check rig keyframes to selected objects and selected channel box attributes,
	regarding valDict.
	'''
	objs = mc.ls(sl=True, l=True)
	attrs = mc.channelBox('mainChannelBox', q=True, sma=True)

	currFrame = mc.currentTime(q=True)

	for obj in objs:
		for attr in attrs:
			if not oneTime:
				currFrame = mc.currentTime(q=True)
			else:
				mc.currentTime(currFrame)
			nodeAttr = '%s.%s' % (obj, attr)

			if not mc.objExists(nodeAttr):
				continue

			addCheckRigKey(nodeAttr, vals, currFrame, frameRange)

def addCheckRigKey(nodeAttr='', vals=[10,-10], startFrame=0, frameRange=0):
	'''
	Adding check rig keyframes to given nodeAttr with given vals at startFrame.
	'''
	currFrame = mc.currentTime(q=True)
	currVal = mc.getAttr(nodeAttr)
	
	frameStep = int(frameRange/2)
	if len(vals) > 1:
		frameStep = int(frameRange/4)

	# Step 1 - Start frame
	mc.currentTime(startFrame, u=False)
	mc.setKeyframe(nodeAttr)

	# Step 2 - First value
	mc.currentTime(currFrame + frameStep, u=False)
	mc.setAttr(nodeAttr, currVal+vals[0])
	mc.setKeyframe(nodeAttr, itt='linear', ott='linear')

	# Step 3 - Back to current value
	mc.currentTime(currFrame + 2*frameStep, u=False)
	mc.setAttr(nodeAttr, currVal)
	mc.setKeyframe(nodeAttr, itt='linear', ott='linear')

	if len(vals) > 1:
		# Step 4 - Second value
		mc.currentTime(currFrame + 3*frameStep, u=False)
		mc.setAttr(nodeAttr, currVal+vals[1])
		mc.setKeyframe(nodeAttr, itt='linear', ott='linear')

		# Step 5 - Back to current value
		mc.currentTime(currFrame + frameRange, u=False)
		mc.setAttr(nodeAttr, currVal)
		mc.setKeyframe(nodeAttr, itt='linear', ott='linear')

	mc.currentTime(mc.currentTime(q=True))