# Original script by Jeff Rosenthal
# JeffRosenthal.org
# 8/1/2012

import maya.cmds as mc

def getValue(x, range_, max_):
	value = (1 - x/(range_*max_))/2
	return clamp(value, 0, 1)

def clamp(value, low, high):
	if value < low:
		return low
	if (value > high):
		return high
	return value

def getShapeNode(transform):
	return mc.listRelatives(transform, shapes=True)[0]

def main(percentRange=0.1, direction=1):

	# Fall off value.
	# .1 = 10%
	# percentRange = 0.2

	(sourceObj, targetObj) = mc.ls(sl=True)
	sourceShape = getShapeNode(sourceObj)

	numVerts = mc.polyEvaluate(sourceObj, v=True)

	# figure out width of face (assume X axis)
	rgtX = 0
	lftX = 0
	for i in range(0, numVerts):
		testX = mc.pointPosition(targetObj + ".vtx[" + str(i) + "]", l=1)[0]
		if testX < rgtX:
			rgtX = testX
		if testX > lftX:
			lftX = testX

	for i in range(0, numVerts):
		# get vert positions
		sourcePos = mc.pointPosition(sourceObj + ".vtx[" + str(i) + "]", l=1)
		targetPos = mc.pointPosition(targetObj + ".vtx[" + str(i) + "]", l=1)        
		
		# find difference
		differencePos = (
							sourcePos[0] - targetPos[0],
							sourcePos[1] - targetPos[1],
							sourcePos[2] - targetPos[2]
						)
		
		# get falloff amount from side of object
		testX = mc.pointPosition(sourceObj + ".vtx[" + str(i) + "]", l=1)[0]
		max_ = rgtX
		if direction == -1:
			max_ = lftX

		falloff = getValue(testX, percentRange, max_)
		
		# move vert difference * falloff amount
		mc.xform(
						targetObj + '.vtx[' + str(i) + ']',
						rt=(
								differencePos[0]*falloff,
								differencePos[1]*falloff,
								differencePos[2]*falloff
							)
					)

	mc.select(cl=True)