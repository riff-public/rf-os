# Weight puller
import maya.cmds as mc
import maya.mel as mm
from functools import partial

def run():
	# WeightPuller call back
	ui = WeightPuller()
	ui.show()

def pull(srch='', rep='', mult=1, opr='mult'):

	oSels = mc.ls(sl=True, fl=True)
	mm.eval('PolySelectConvert 3;')
	sels = mc.ls(sl=True, fl=True)
	
	# Collect all skin cluster nodes as a dict
	skns = []
	sknDct = {}
	
	for sel in sels:
		skn = mm.eval('findRelatedSkinCluster %s' % sel.split('.')[0])
		sknDct[ sel ] = skn
		
		if not skn in skns:
			skns.append(skn)
	
	# Disable normalize weight for all skin cluster nodes
	for skn in skns:
		if mc.getAttr('%s.normalizeWeights' % skn):
			mc.setAttr('%s.normalizeWeights' % skn, 0)
	
	# Iterate through selected points
	for sel in sels:
		
		skn = sknDct[sel]
		infs = mc.skinCluster(skn, q=True, inf=True)
		wghtVals = mc.skinPercent(skn, sel, q=True, v=True)
		weightDict = dict(zip(infs, wghtVals))
		
		# Iterate through all influence nodes
		for ix in range(len(infs)):
			
			# If keyword is found in the current influence node
			if srch in infs[ix]:
				srchInf = infs[ix]
				repInf = infs[ix].replace(srch, rep)
				pullVal = wghtVals[ix] * mult
				# If replaced influence is member of object's influences
				if repInf in infs:
					if opr == 'mult':
						mc.skinPercent(
										skn,
										sel,
										transformValue=[
															(infs[ix], wghtVals[ix] - pullVal),
															(repInf, wghtVals[ix]*mult)
														]
										)
					elif opr == 'add':
						mc.skinPercent(
										skn,
										sel,
										transformValue=[
															(repInf, weightDict[repInf] + pullVal),
															(srchInf, weightDict[srchInf] - pullVal)
														]
										)

				else:
					print '%s has no %s as its influence' % (skn, repInf)
	
	# Enable normalize weights
	for skn in skns:
		if not mc.getAttr('%s.normalizeWeights' % skn):
			mc.setAttr('%s.normalizeWeights' % skn, 1)

	mc.select(oSels, r=True)

class WeightPuller(object):
	
	def __init__(self):
		
		self.ui = 'pkWeightPuller'
		self.win = '%sWin' % self.ui
	
	def show(self):
		
		oSrch = '_L_'
		oRep = '_R_'
		oMul = 1
		
		if mc.window(self.win, exists=True):
			oSrch = mc.textField('%sSrchTF'%self.ui, q=True, tx=True)
			oRep = mc.textField('%sRepTF'%self.ui, q=True, tx=True)
			oMul = mc.floatField('%sMultFF'%self.ui, q=True, v=True)
			mc.deleteUI(self.win)
		
		mc.window(self.win, t='pkWeightPuller', rtf=True)
		
		mc.columnLayout('%sMainCL'%self.ui, adj=True)
		
		mc.text(l='Search for', align='center')
		mc.textField('%sSrchTF'%self.ui, tx=oSrch)
		mc.text(l='Replace with', align='center')
		mc.textField('%sRepTF'%self.ui, tx=oRep)
		mc.button('%sSwapBUT'%self.ui, l='Swap', c=partial(self.swap))
		mc.floatField('%sMultFF'%self.ui, minValue=0, maxValue=1, v=oMul)

		mc.separator()
		mc.button('%sBUT'%self.ui, h=50, l='Pull', c=partial(self.doMult))
		mc.separator()
		mc.button('%sAddBUT'%self.ui, h=50, l='Add', c=partial(self.doAdd))
		
		mc.showWindow(self.win)
		mc.window(self.win, e=True, w=180)
		mc.window(self.win, e=True, h=250)
		
	def swap(self, arg=None):
		# Swap search and replace text field's value
		srch = mc.textField('%sSrchTF'%self.ui, q=True, tx=True)
		rep = mc.textField('%sRepTF'%self.ui, q=True, tx=True)
		
		mc.textField('%sSrchTF'%self.ui, edit=True, tx=rep)
		mc.textField('%sRepTF'%self.ui, edit=True, tx=srch)

	def doAdd(self, *args):
		# Info
		srch = mc.textField('%sSrchTF'%self.ui, q=True, tx=True)
		rep = mc.textField('%sRepTF'%self.ui, q=True, tx=True)
		mult = mc.floatField('%sMultFF'%self.ui, q=True, v=True)

		pull(srch, rep, mult, 'add')

	def doMult(self, *args):

		# Info
		srch = mc.textField('%sSrchTF'%self.ui, q=True, tx=True)
		rep = mc.textField('%sRepTF'%self.ui, q=True, tx=True)
		mult = mc.floatField('%sMultFF'%self.ui, q=True, v=True)

		pull(srch, rep, mult, 'mult')
