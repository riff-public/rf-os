# Leg rig module
import maya.cmds as mc

import lpRig.core as lrc
import lpRig.rigTools as lrr
import lpRig.ribbon as lrb
reload(lrc)
reload(lrr)
reload(lrb)

class LegRig(object):

	def __init__(
					self,
					parent='Pelvis_Jnt',
					ctrlGrp='',
					jntGrp='',
					ikhGrp='',
					skinGrp='',
					stillGrp='',
					ribbon=True,
					tmpJnt=[
								'UpperLeg_L_Jnt',
								'LowerLeg_Jnt',
								'Ankle_L_Jnt',
								'Ball_L_Jnt',
								'Toe_L_Jnt',
								'Heel_L_Jnt',
								'FootIn_L_Jnt',
								'FootOut_L_Jnt',
								'KneeIk_L_Jnt'
							],
					part='',
					side=''
				):

		if side:
			side = '_%s_' % side
		else:
			side = '_'

		# Skin joints
		self.UpLeg_Jnt = lrc.Dag(tmpJnt[0])
		self.LowLeg_Jnt = lrc.Dag(tmpJnt[1])
		self.Ankle_Jnt = lrc.Dag(tmpJnt[2])
		self.Ball_Jnt = lrc.Dag(tmpJnt[3])
		self.Toe_Jnt = lrc.Dag(tmpJnt[4])
		self.Heel_Jnt = lrc.Dag(tmpJnt[5])
		self.FootIn_Jnt = lrc.Dag(tmpJnt[6])
		self.FootOut_Jnt = lrc.Dag(tmpJnt[7])
		self.KneeIk_Jnt = lrc.Dag(tmpJnt[8])

		self.Ball_Jnt.attr('ssc').v = 0

		self.UpLeg_Jnt.rotateOrder = 'yzx'
		self.LowLeg_Jnt.rotateOrder = 'yzx'
		self.Ankle_Jnt.rotateOrder = 'yzx'
		self.Ball_Jnt.rotateOrder = 'yzx'
		self.Toe_Jnt.rotateOrder = 'yzx'

		# Main group
		self.Ctrl_Grp = lrc.Null()
		self.Ctrl_Grp.name = 'Leg%sCtrl%sGrp' % (part, side)

		mc.parentConstraint(parent, self.Ctrl_Grp)
		mc.scaleConstraint(parent, self.Ctrl_Grp)

		self.Jnt_Grp = lrc.Null()
		self.Jnt_Grp.name = 'Leg%sJnt%sGrp' % (part, side)

		mc.parentConstraint(parent, self.Jnt_Grp)
		mc.scaleConstraint(parent, self.Jnt_Grp)

		# Length
		self.upLegLen = lrc.distance(self.UpLeg_Jnt, self.LowLeg_Jnt)
		self.lowLegLen = lrc.distance(self.LowLeg_Jnt, self.Ankle_Jnt)

		# Main Controller
		self.Leg_Ctrl = lrc.Control('stick')
		self.Leg_Ctrl.lockHideAttrs('tx', 'ty', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz', 'v')
		self.Leg_Ctrl.name = 'Leg%s%sCtrl' % (part, side)

		self.LegCtrlZr_Grp = lrc.group(self.Leg_Ctrl)
		self.LegCtrlZr_Grp.name = 'LegCtrlZr%s%sGrp' % (part, side)

		mc.parentConstraint(self.Ankle_Jnt, self.LegCtrlZr_Grp)
		self.LegCtrlZr_Grp.parent(self.Ctrl_Grp)

		# Main Controller - Color
		if side == '_R_':
			self.Leg_Ctrl.color = 'blue'
		else:		
			self.Leg_Ctrl.color = 'red'

		# Non-roll Joints
		(self.UpLegNr_Jnt,
		self.UpLegNrJntZr_Grp,
		self.UpLegTipNr_Jnt,
		self.UpLegNr_Ikh,
		self.UpLegNrIkhZr_Grp) = lrr.createNonRollJointChain(parent, self.UpLeg_Jnt, self.LowLeg_Jnt, 'UpLeg%s' % part, side)
		self.UpLegNrJntZr_Grp.parent(self.Jnt_Grp)
		self.UpLegNrIkhZr_Grp.parent(self.Jnt_Grp)
		
		(self.LowLegNr_Jnt,
		self.LowLegNrJntZr_Grp,
		self.LowLegTipNr_Jnt,
		self.LowLegNr_Ikh,
		self.LowLegNrIkhZr_Grp) = lrr.createNonRollJointChain(self.UpLeg_Jnt, self.LowLeg_Jnt, self.Ankle_Jnt, 'LowLeg%s' % part, side)
		self.LowLegNrJntZr_Grp.parent(self.Jnt_Grp)
		self.LowLegNrIkhZr_Grp.parent(self.Jnt_Grp)
		
		# Foot scale
		self.Leg_Ctrl.add(ln='footScale', dv=1, k=True)
		self.Leg_Ctrl.attr('footScale') >> self.Ankle_Jnt.attr('sx')
		self.Leg_Ctrl.attr('footScale') >> self.Ankle_Jnt.attr('sy')
		self.Leg_Ctrl.attr('footScale') >> self.Ankle_Jnt.attr('sz')

		# FK rig
		# FK rig - Main groups
		self.LegFkCtrl_Grp = lrc.Null()
		self.LegFkCtrl_Grp.name = 'LegFkCtrl%s%sGrp' % (part, side)
		self.LegFkCtrl_Grp.snap(self.Ctrl_Grp)
		self.LegFkCtrl_Grp.parent(self.Ctrl_Grp)
		
		self.LegFkJnt_Grp = lrc.Null()
		self.LegFkJnt_Grp.name = 'LegFkJnt%s%sGrp' % (part, side)
		self.LegFkJnt_Grp.snap(self.Jnt_Grp)
		self.LegFkJnt_Grp.parent(self.Jnt_Grp)
		
		# FK rig - Joints
		self.UpLegFk_Jnt = lrr.jointAt(self.UpLeg_Jnt)
		self.LowLegFk_Jnt = lrr.jointAt(self.LowLeg_Jnt)
		self.AnkleFk_Jnt = lrr.jointAt(self.Ankle_Jnt)
		self.BallFk_Jnt = lrr.jointAt(self.Ball_Jnt)
		self.ToeFk_Jnt = lrr.jointAt(self.Toe_Jnt)

		self.UpLegFk_Jnt.name = 'UpLegFk%s%sJnt' % (part, side)
		self.LowLegFk_Jnt.name = 'LowLegFk%s%sJnt' % (part, side)
		self.AnkleFk_Jnt.name = 'AnkleFk%s%sJnt' % (part, side)
		self.BallFk_Jnt.name = 'BallFk%s%sJnt' % (part, side)
		self.ToeFk_Jnt.name = 'ToeFk%s%sJnt' % (part, side)

		self.ToeFk_Jnt.parent(self.BallFk_Jnt)
		self.BallFk_Jnt.parent(self.AnkleFk_Jnt)
		self.AnkleFk_Jnt.parent(self.LowLegFk_Jnt)
		self.LowLegFk_Jnt.parent(self.UpLegFk_Jnt)
		self.UpLegFk_Jnt.parent(self.LegFkJnt_Grp)
		self.BallFk_Jnt.attr('ssc').v = 0

		self.UpLegFk_Jnt.rotateOrder = 'yzx'
		self.LowLegFk_Jnt.rotateOrder = 'yzx'
		self.AnkleFk_Jnt.rotateOrder = 'yzx'
		self.BallFk_Jnt.rotateOrder = 'yzx'
		self.ToeFk_Jnt.rotateOrder = 'yzx'

		# FK rig - Controllers
		self.UpLegFk_Ctrl = lrr.jointControl('cube')
		self.UpLegFk_Ctrl.name = 'UpLegFk%s%sCtrl' % (part, side)
		self.UpLegFk_Ctrl.lockHideAttrs('sx', 'sy', 'sz', 'v')

		self.UpLegFkCtrlZr_Grp = lrc.group(self.UpLegFk_Ctrl)
		self.UpLegFkCtrlZr_Grp.name = 'UpLegFkCtrlZr%s%sGrp' % (part, side)

		self.LowLegFk_Ctrl = lrr.jointControl('cube')
		self.LowLegFk_Ctrl.name = 'LowLegFk%s%sCtrl' % (part, side)
		self.LowLegFk_Ctrl.lockHideAttrs('sx', 'sy', 'sz', 'v')

		self.LowLegFkGmbl_Ctrl = lrc.addGimbal(self.LowLegFk_Ctrl)
		self.LowLegFkGmbl_Ctrl.name = 'LowLegFkGmbl%s%sCtrl' % (part, side)
		
		self.LowLegFkCtrlZr_Grp = lrc.group(self.LowLegFk_Ctrl)
		self.LowLegFkCtrlZr_Grp.name = 'LowLegFkCtrlZr%s%sGrp' % (part, side)

		self.AnkleFk_Ctrl = lrr.jointControl('cube')
		self.AnkleFk_Ctrl.name = 'AnkleFk%s%sCtrl' % (part, side)
		self.AnkleFk_Ctrl.lockHideAttrs('sx', 'sy', 'sz', 'v')

		self.AnkleFkGmbl_Ctrl = lrc.addGimbal(self.AnkleFk_Ctrl)
		self.AnkleFkGmbl_Ctrl.name = 'AnkleFkGmbl%s%sCtrl' % (part, side)

		self.AnkleFkCtrlZr_Grp = lrc.group(self.AnkleFk_Ctrl)
		self.AnkleFkCtrlZr_Grp.name = 'AnkleFkCtrlZr%s%sGrp' % (part, side)

		self.AnkleScaOfst_Grp = lrc.Null()
		self.AnkleScaOfst_Grp.name = 'AnkleScaOfst%s%sGrp' % (part, side)
		self.AnkleScaOfst_Grp.parent(self.AnkleFkGmbl_Ctrl)

		self.ToeFk_Ctrl = lrr.jointControl('cube')
		self.ToeFk_Ctrl.name = 'ToeFk%s%sCtrl' % (part, side)
		self.ToeFk_Ctrl.lockHideAttrs('sx', 'sy', 'sz', 'v')

		self.ToeFkGmbl_Ctrl = lrc.addGimbal(self.ToeFk_Ctrl)
		self.ToeFkGmbl_Ctrl.name = 'ToeFkGmbl%s%sCtrl' % (part, side)
		
		self.ToeFkCtrlZr_Grp = lrc.group(self.ToeFk_Ctrl)
		self.ToeFkCtrlZr_Grp.name = 'ToeFkCtrlZr%s%sGrp' % (part, side)

		# FK rig - Controllers - Color
		if side == '_R_':
			self.UpLegFk_Ctrl.color = 'blue'
			self.LowLegFk_Ctrl.color = 'blue'
			self.AnkleFk_Ctrl.color = 'blue'
			self.ToeFk_Ctrl.color = 'blue'
		else:
			self.UpLegFk_Ctrl.color = 'red'
			self.LowLegFk_Ctrl.color = 'red'
			self.AnkleFk_Ctrl.color = 'red'
			self.ToeFk_Ctrl.color = 'red'

		# FK rig - Controllers - Positioning
		self.UpLegFkCtrlZr_Grp.snapPoint(self.UpLeg_Jnt)
		self.UpLegFk_Ctrl.snapOrient(self.UpLeg_Jnt)
		self.UpLegFk_Ctrl.freeze(r=True)
		
		self.UpLegFkGmbl_Ctrl = lrc.addGimbal(self.UpLegFk_Ctrl)
		self.UpLegFkGmbl_Ctrl.name = 'UpLegFkGmbl%s%sCtrl' % (part, side)

		self.LowLegFkCtrlZr_Grp.snap(self.LowLeg_Jnt)
		self.AnkleFkCtrlZr_Grp.snap(self.Ankle_Jnt)
		self.ToeFkCtrlZr_Grp.snap(self.Ball_Jnt)
		
		self.ToeFkCtrlZr_Grp.parent(self.AnkleScaOfst_Grp)
		self.AnkleFkCtrlZr_Grp.parent(self.LowLegFkGmbl_Ctrl)
		self.LowLegFkCtrlZr_Grp.parent(self.UpLegFkGmbl_Ctrl)
		self.UpLegFkCtrlZr_Grp.parent(self.LegFkCtrl_Grp)

		self.UpLegFk_Ctrl.rotateOrder = 'yzx'
		self.UpLegFkGmbl_Ctrl.rotateOrder = 'yzx'
		self.LowLegFk_Ctrl.rotateOrder = 'yzx'
		self.LowLegFkGmbl_Ctrl.rotateOrder = 'yzx'
		self.AnkleFk_Ctrl.rotateOrder = 'yzx'
		self.AnkleFkGmbl_Ctrl.rotateOrder = 'yzx'
		self.ToeFk_Ctrl.rotateOrder = 'yzx'
		self.ToeFkGmbl_Ctrl.rotateOrder = 'yzx'

		# FK rig - Controllers - Strt setup
		(self.UpLegFkStrt_Add,
		self.UpLegFkStrt_Mul) = lrr.fkStretch(
													ctrl=self.UpLegFk_Ctrl,
													target=self.LowLegFkCtrlZr_Grp
												)
		self.UpLegFkStrt_Add.name = 'UpLegFkStrt%s%sAdd' % (part, side)
		self.UpLegFkStrt_Mul.name = 'UpLegFkStrt%s%sMul' % (part, side)

		(self.LowLegFkStrt_Add,
		self.LowLegFkStrt_Mul) = lrr.fkStretch(
													ctrl=self.LowLegFk_Ctrl,
													target=self.AnkleFkCtrlZr_Grp
												)
		self.LowLegFkStrt_Add.name = 'LowLegFkStrt%s%sAdd' % (part, side)
		self.LowLegFkStrt_Mul.name = 'LowLegFkStrt%s%sMul' % (part, side)

		(self.ToeFkStrt_Add,
		self.ToeFkStrt_Mul) = lrr.fkStretch(
													ctrl=self.ToeFk_Ctrl,
													target=self.ToeFk_Jnt
												)
		self.ToeFkStrt_Add.name = 'ToeFkStrt%s%sAdd' % (part, side)
		self.ToeFkStrt_Mul.name = 'ToeFkStrt%s%sMul' % (part, side)
		
		# FK rig - Connecting to joints
		mc.parentConstraint(self.UpLegFkGmbl_Ctrl, self.UpLegFk_Jnt)
		mc.parentConstraint(self.LowLegFkGmbl_Ctrl, self.LowLegFk_Jnt)
		mc.parentConstraint(self.AnkleFkGmbl_Ctrl, self.AnkleFk_Jnt)
		mc.parentConstraint(self.ToeFkGmbl_Ctrl, self.BallFk_Jnt)
		
		# FK rig - local/world setup
		(self.UpLegFkCtrlLoc_Grp,
		self.UpLegFkCtrlWor_Grp,
		self.UpLegFkCtrlWor_Grp_orientConstraint,
		self.UpLegFkCtrlZr_Grp_orientConstraint,
		self.UpLegFkCtrlZrGrpOri_Rev) = lrr.oriLocWor(
															self.UpLegFk_Ctrl,
															self.LegFkCtrl_Grp,
															ctrlGrp,
															self.UpLegFkCtrlZr_Grp
														)
		self.UpLegFkCtrlLoc_Grp.name = 'UpLegFkCtrlLoc%s%sGrp' % (part, side)
		self.UpLegFkCtrlWor_Grp.name = 'UpLegFkCtrlWor%s%sGrp' % (part, side)
		self.UpLegFkCtrlZrGrpOri_Rev.name = 'UpLegFkCtrlZrGrpOri%s%sRev' % (part, side)

		# FK rig - scale control
		self.Leg_Ctrl.attr('footScale') >> self.AnkleFk_Jnt.attr('sx')
		self.Leg_Ctrl.attr('footScale') >> self.AnkleFk_Jnt.attr('sy')
		self.Leg_Ctrl.attr('footScale') >> self.AnkleFk_Jnt.attr('sz')
		self.Leg_Ctrl.attr('footScale') >> self.AnkleScaOfst_Grp.attr('sx')
		self.Leg_Ctrl.attr('footScale') >> self.AnkleScaOfst_Grp.attr('sy')
		self.Leg_Ctrl.attr('footScale') >> self.AnkleScaOfst_Grp.attr('sz')

		# IK rig
		# IK rig - Main groups
		self.LegIkCtrl_Grp = lrc.Null()
		self.LegIkCtrl_Grp.name = 'LegIkCtrl%s%sGrp' % (part, side)
		self.LegIkCtrl_Grp.snap(self.Ctrl_Grp)
		self.LegIkCtrl_Grp.parent(self.Ctrl_Grp)
		
		self.LegIkJnt_Grp = lrc.Null()
		self.LegIkJnt_Grp.name = 'LegIkJnt%s%sGrp' % (part, side)
		self.LegIkJnt_Grp.snap(self.Jnt_Grp)
		self.LegIkJnt_Grp.parent(self.Jnt_Grp)

		# IK rig - Joints
		self.UpLegIk_Jnt = lrr.jointAt(self.UpLeg_Jnt)
		self.LowLegIk_Jnt = lrr.jointAt(self.LowLeg_Jnt)
		self.AnkleIk_Jnt = lrr.jointAt(self.Ankle_Jnt)
		self.BallIk_Jnt = lrr.jointAt(self.Ball_Jnt)
		self.ToeIk_Jnt = lrr.jointAt(self.Toe_Jnt)

		self.UpLegIk_Jnt.name = 'UpLegIk%s%sJnt' % (part, side)
		self.LowLegIk_Jnt.name = 'LowLegIk%s%sJnt' % (part, side)
		self.AnkleIk_Jnt.name = 'AnkleIk%s%sJnt' % (part, side)
		self.BallIk_Jnt.name = 'BallIk%s%sJnt' % (part, side)
		self.ToeIk_Jnt.name = 'ToeIk%s%sJnt' % (part, side)

		self.ToeIk_Jnt.parent(self.BallIk_Jnt)
		self.BallIk_Jnt.parent(self.AnkleIk_Jnt)
		self.AnkleIk_Jnt.parent(self.LowLegIk_Jnt)
		self.LowLegIk_Jnt.parent(self.UpLegIk_Jnt)
		self.UpLegIk_Jnt.parent(self.LegIkJnt_Grp)
		self.BallIk_Jnt.attr('ssc').v = 0

		self.UpLegIk_Jnt.rotateOrder = 'yzx'
		self.LowLegIk_Jnt.rotateOrder = 'yzx'
		self.AnkleIk_Jnt.rotateOrder = 'yzx'
		self.BallIk_Jnt.rotateOrder = 'yzx'
		self.ToeIk_Jnt.rotateOrder = 'yzx'

		# IK rig - Controllers
		self.LegIkRoot_Ctrl = lrr.jointControl('cube')
		self.LegIkRoot_Ctrl.name = 'LegIkRoot%s%sCtrl' % (part, side)
		self.LegIkRoot_Ctrl.lockHideAttrs('rx', 'ry', 'rz', 'sx', 'sy', 'sz', 'v')

		self.LegIkRootCtrlZr_Grp = lrc.group(self.LegIkRoot_Ctrl)
		self.LegIkRootCtrlZr_Grp.name = 'LegIkRootCtrlZr%s%sGrp' % (part, side)
		
		self.LegIk_Ctrl = lrr.jointControl('cube')
		self.LegIk_Ctrl.name = 'LegIk%s%sCtrl' % (part, side)
		self.LegIk_Ctrl.lockHideAttrs('sx', 'sy', 'sz', 'v')
		self.LegIk_Ctrl.rotateOrder = 'xzy'

		self.LegIkCtrlLock_Grp = lrc.group(self.LegIk_Ctrl)
		self.LegIkCtrlLock_Grp.name = 'LegIkCtrlLock%s%sGrp' % (part, side)

		self.LegIkCtrlZr_Grp = lrc.group(self.LegIkCtrlLock_Grp)
		self.LegIkCtrlZr_Grp.name = 'LegIkCtrlZr%s%sGrp' % (part, side)
		
		self.KneeIk_Ctrl = lrc.Control('sphere')
		self.KneeIk_Ctrl.name = 'KneeIk%s%sCtrl' % (part, side)
		self.KneeIk_Ctrl.lockHideAttrs('rx', 'ry', 'rz', 'sx', 'sy', 'sz', 'v')

		self.KneeIkCtrlZr_Grp = lrc.group(self.KneeIk_Ctrl)
		self.KneeIkCtrlZr_Grp.name = 'KneeIkCtrlZr%s%sGrp' % (part, side)

		# IK Rig - Controllers - Color
		if side == '_R_':
			self.LegIkRoot_Ctrl.color = 'blue'
			self.LegIk_Ctrl.color = 'blue'
			self.KneeIk_Ctrl.color = 'blue'
		else:
			self.LegIkRoot_Ctrl.color = 'red'
			self.LegIk_Ctrl.color = 'red'
			self.KneeIk_Ctrl.color = 'red'

		# IK rig - Controllers - Positioning
		self.LegIkRootCtrlZr_Grp.snapPoint(self.UpLeg_Jnt)
		
		self.LegIkCtrlZr_Grp.snapPoint(self.Ankle_Jnt)
		self.LegIk_Ctrl.snapOrient(self.Ankle_Jnt)
		self.LegIk_Ctrl.freeze(t=False, r=True, s=False)
		
		self.LegIkCon_Ctrl = lrc.addConCtrl(self.LegIk_Ctrl)
		self.LegIkGmbl_Ctrl = lrc.addGimbal(self.LegIk_Ctrl)

		self.LegIkCon_Ctrl.name = 'LegIkCon%s%sCtrl' % (part, side)
		self.LegIkGmbl_Ctrl.name = 'LegIkGmbl%s%sCtrl' % (part, side)
		self.LegIkGmbl_Ctrl.rotateOrder = 'xzy'
		
		self.KneeIkCtrlZr_Grp.snapPoint(self.KneeIk_Jnt)
		
		self.LegIkRootCtrlZr_Grp.parent(self.LegIkCtrl_Grp)
		self.LegIkCtrlZr_Grp.parent(self.LegIkRoot_Ctrl)
		self.KneeIkCtrlZr_Grp.parent(self.LegIkRoot_Ctrl)
		
		# IK rig - Controllers - Root Control Vis
		legIkCtrlShp = lrc.Dag(self.LegIk_Ctrl.shape)
		legIkCtrlRootShp = lrc.Dag(self.LegIkRoot_Ctrl.shape)
		legIkCtrlShp.add(ln='rootCtrl', min=0, max=1, dv=0, k=True)

		legIkCtrlShp.attr('rootCtrl') >> legIkCtrlRootShp.attr('v')

		# IK rig - Knee curve
		(self.KneeIkCtrl_Crv,
		self.KneeIkCtrl1_Cls,
		self.KneeIkCtrl2_Cls) = lrr.crvGuide(ctrl=self.KneeIk_Ctrl, target=self.LowLegIk_Jnt)

		self.KneeIkCtrl_Crv.name = 'KneeIkCtrl%s%sCrv' % (part, side)
		self.KneeIkCtrl1_Cls.name = 'KneeIkCtrl1%s%sCls' % (part, side)
		self.KneeIkCtrl2_Cls.name = 'KneeIkCtrl2%s%sCls' % (part, side)

		lrr.renameClsElms(self.KneeIkCtrl1_Cls)
		lrr.renameClsElms(self.KneeIkCtrl2_Cls)

		self.KneeIkCtrl_Crv.attr('inheritsTransform').v = 0
		self.KneeIkCtrl_Crv.attr('overrideEnabled').v = 1
		self.KneeIkCtrl_Crv.attr('overrideDisplayType').v = 2
		
		self.KneeIkCtrl_Crv.parent(self.LegIkCtrl_Grp)
		self.KneeIkCtrl_Crv.attr('t').v = (0,0,0)
		self.KneeIkCtrl_Crv.attr('r').v = (0,0,0)

		# IK rig - local/world setup
		(self.LegIkCtrlLoc_Grp,
		self.LegIkCtrlWor_Grp,
		self.LegIkCtrlWor_Grp_parentConstraint,
		self.LegIkCtrlZr_Grp_parentConstraint,
		self.LegIkCtrlZrGrpPar_Rev) = lrr.parLocWor(
														self.LegIk_Ctrl,
														self.LegIkRoot_Ctrl,
														ctrlGrp,
														self.LegIkCtrlZr_Grp
													)
		self.LegIkCtrlLoc_Grp.name = 'LegIkCtrlLoc%s%sGrp' % (part, side)
		self.LegIkCtrlWor_Grp.name = 'LegIkCtrlWor%s%sGrp' % (part, side)
		self.LegIkCtrlZrGrpPar_Rev.name = 'LegIkCtrlZrGrpPar%s%sRev' % (part, side)

		(self.KneeIkCtrlLoc_Grp,
		self.KneeIkCtrlWor_Grp,
		self.KneeIkCtrlWor_Grp_parentConstraint,
		self.KneeIkCtrlZr_Grp_parentConstraint,
		self.KneeIkCtrlZrGrpPar_Rev) = lrr.parLocWor(
														self.KneeIk_Ctrl,
														self.LegIkGmbl_Ctrl,
														ctrlGrp,
														self.KneeIkCtrlZr_Grp
													)
		self.KneeIkCtrlLoc_Grp.name = 'KneeIkCtrlLoc%s%sGrp' % (part, side)
		self.KneeIkCtrlWor_Grp.name = 'KneeIkCtrlWor%s%sGrp' % (part, side)
		self.KneeIkCtrlZrGrpPar_Rev.name = 'KneeIkCtrlZrGrpPar%s%sRev' % (part, side)

		# IK rig - IK handles
		self.LegIk_Ikh = lrc.IkRp(sj=self.UpLegIk_Jnt, ee=self.AnkleIk_Jnt)
		self.BallIk_Ikh = lrc.IkRp(sj=self.AnkleIk_Jnt, ee=self.BallIk_Jnt)
		self.ToeIk_Ikh = lrc.IkRp(sj=self.BallIk_Jnt, ee=self.ToeIk_Jnt)

		self.LegIk_Ikh.name = 'LegIk%s%sIkh' % (part, side)
		self.BallIk_Ikh.name = 'BallIk%s%sIkh' % (part, side)
		self.ToeIk_Ikh.name = 'ToeIk%s%sIkh' % (part, side)

		lrc.poleVectorConstraint(self.KneeIk_Ctrl, self.LegIk_Ikh)
		
		self.LegIkIkh_Grp = lrc.Null()
		self.LegIkIkhZr_Grp = lrc.Null()
		self.BallIkIkhZr_Grp = lrc.Null()
		self.ToeIkIkhZr_Grp = lrc.Null()

		self.LegIkIkhZr_Grp.snap(self.LegIk_Ikh)
		self.BallIkIkhZr_Grp.snap(self.BallIk_Ikh)
		self.ToeIkIkhZr_Grp.snap(self.ToeIk_Ikh)

		self.LegIk_Ikh.parent(self.LegIkIkhZr_Grp)
		self.BallIk_Ikh.parent(self.BallIkIkhZr_Grp)
		self.ToeIk_Ikh.parent(self.ToeIkIkhZr_Grp)

		self.LegIkIkh_Grp.name = 'LegIkIkh%s%sGrp' % (part, side)
		self.LegIkIkhZr_Grp.name = 'LegIkIkhZr%s%sGrp' % (part, side)
		self.BallIkIkhZr_Grp.name = 'BallIkIkhZr%s%sGrp' % (part, side)
		self.ToeIkIkhZr_Grp.name = 'ToeIkIkhZr%s%sGrp' % (part, side)
		
		self.LegIkIkhZr_Grp.parent(self.LegIkIkh_Grp)
		self.BallIkIkhZr_Grp.parent(self.LegIkIkh_Grp)
		self.ToeIkIkhZr_Grp.parent(self.LegIkIkh_Grp)
		self.LegIkIkh_Grp.parent(ikhGrp)

		# IK rig - Pivot groups
		self.AnkleRollIkPiv_Grp = lrc.Null()
		self.AnkleRollIk_Grp = lrc.Null()
		self.ToeBendIkPiv_Grp = lrc.Null()
		self.FootInIkPiv_Grp = lrc.Null()
		self.FootOutIkPiv_Grp = lrc.Null()
		self.FootMidIkPiv_Grp = lrc.Null()
		self.HeelIkPiv_Grp = lrc.Null()
		self.ToeIkPiv_Grp = lrc.Null()
		self.LegIkPiv_Grp = lrc.Null()

		self.AnkleRollIkPivZr_Grp = lrc.group(self.AnkleRollIkPiv_Grp)
		self.AnkleRollIkZr_Grp = lrc.group(self.AnkleRollIk_Grp)
		self.ToeBendIkPivZr_Grp = lrc.group(self.ToeBendIkPiv_Grp)
		self.FootInIkPivZr_Grp = lrc.group(self.FootInIkPiv_Grp)
		self.FootOutIkPivZr_Grp = lrc.group(self.FootOutIkPiv_Grp)
		self.FootMidIkPivZr_Grp = lrc.group(self.FootMidIkPiv_Grp)
		self.HeelIkPivZr_Grp = lrc.group(self.HeelIkPiv_Grp)
		self.ToeIkPivZr_Grp = lrc.group(self.ToeIkPiv_Grp)
		self.LegIkPivZr_Grp = lrc.group(self.LegIkPiv_Grp)
		
		self.LegIkIkhPiv_Grp = lrc.Null()
		self.BallIkIkhPiv_Grp = lrc.Null()
		self.ToeIkIkhPiv_Grp = lrc.Null()

		self.AnkleRollIkPiv_Grp.name = 'AnkleRollIkPiv%s%sGrp' % (part, side)
		self.AnkleRollIk_Grp.name = 'AnkleRollIk%s%sGrp' % (part, side)
		self.ToeBendIkPiv_Grp.name = 'ToeBendIkPiv%s%sGrp' % (part, side)
		self.FootInIkPiv_Grp.name = 'FootInIkPiv%s%sGrp' % (part, side)
		self.FootOutIkPiv_Grp.name = 'FootOutIkPiv%s%sGrp' % (part, side)
		self.FootMidIkPiv_Grp.name = 'FootMidIkPiv%s%sGrp' % (part, side)
		self.HeelIkPiv_Grp.name = 'HeelIkPiv%s%sGrp' % (part, side)
		self.ToeIkPiv_Grp.name = 'ToeIkPiv%s%sGrp' % (part, side)
		self.LegIkPiv_Grp.name = 'LegIkPiv%s%sGrp' % (part, side)

		self.AnkleRollIkPivZr_Grp.name = 'AnkleRollIkPivZr%s%sGrp' % (part, side)
		self.AnkleRollIkZr_Grp.name = 'AnkleRollIkZr%s%sGrp' % (part, side)
		self.ToeBendIkPivZr_Grp.name = 'ToeBendIkPivZr%s%sGrp' % (part, side)
		self.FootInIkPivZr_Grp.name = 'FootInIkPivZr%s%sGrp' % (part, side)
		self.FootOutIkPivZr_Grp.name = 'FootOutIkPivZr%s%sGrp' % (part, side)
		self.FootMidIkPivZr_Grp.name = 'FootMidIkPivZr%s%sGrp' % (part, side)
		self.HeelIkPivZr_Grp.name = 'HeelIkPivZr%s%sGrp' % (part, side)
		self.ToeIkPivZr_Grp.name = 'ToeIkPivZr%s%sGrp' % (part, side)
		self.LegIkPivZr_Grp.name = 'LegIkPivZr%s%sGrp' % (part, side)

		self.LegIkIkhPiv_Grp.name = 'LegIkIkhPiv%s%sGrp' % (part, side)
		self.BallIkIkhPiv_Grp.name = 'BallIkIkhPiv%s%sGrp' % (part, side)
		self.ToeIkIkhPiv_Grp.name = 'ToeIkIkhPiv%s%sGrp' % (part, side)

		# IK rig - Pivot groups - Positioning
		# self.LegIkPiv_Grp.snap(self.Ankle_Jnt)
		self.LegIkPivZr_Grp.snap(self.Ankle_Jnt)
		self.AnkleRollIkPivZr_Grp.snap(self.Ball_Jnt)
		self.AnkleRollIkZr_Grp.snap(self.Ball_Jnt)
		self.ToeBendIkPivZr_Grp.snap(self.Ball_Jnt)
		self.FootInIkPivZr_Grp.snap(self.FootIn_Jnt)
		self.FootOutIkPivZr_Grp.snap(self.FootOut_Jnt)
		self.FootMidIkPivZr_Grp.snap(self.Ball_Jnt)
		self.HeelIkPivZr_Grp.snap(self.Heel_Jnt)
		self.ToeIkPivZr_Grp.snap(self.Toe_Jnt)
		
		self.LegIkIkhPiv_Grp.snap(self.LegIkIkhZr_Grp)
		self.BallIkIkhPiv_Grp.snap(self.BallIkIkhZr_Grp)
		self.ToeIkIkhPiv_Grp.snap(self.ToeIkIkhZr_Grp)
		
		self.AnkleRollIkPivZr_Grp.parent(self.FootInIkPiv_Grp)
		self.AnkleRollIkZr_Grp.parent(self.AnkleRollIkPiv_Grp)
		self.ToeBendIkPivZr_Grp.parent(self.FootInIkPiv_Grp)
		self.FootInIkPivZr_Grp.parent(self.FootOutIkPiv_Grp)
		self.FootOutIkPivZr_Grp.parent(self.HeelIkPiv_Grp)
		self.HeelIkPivZr_Grp.parent(self.ToeIkPiv_Grp)
		self.ToeIkPivZr_Grp.parent(self.FootMidIkPiv_Grp)
		self.FootMidIkPivZr_Grp.parent(self.LegIkPiv_Grp)
		self.LegIkPivZr_Grp.parent(self.LegIkGmbl_Ctrl)
		
		self.LegIkIkhPiv_Grp.parent(self.AnkleRollIk_Grp)
		self.BallIkIkhPiv_Grp.parent(self.AnkleRollIk_Grp)
		self.ToeIkIkhPiv_Grp.parent(self.ToeBendIkPiv_Grp)
		mc.pointConstraint(self.LegIkRoot_Ctrl, self.UpLegIk_Jnt)

		mc.parentConstraint(self.LegIkIkhPiv_Grp, self.LegIkIkhZr_Grp)
		mc.parentConstraint(self.BallIkIkhPiv_Grp, self.BallIkIkhZr_Grp)
		mc.parentConstraint(self.ToeIkIkhPiv_Grp, self.ToeIkIkhZr_Grp)
		
		self.LegIk_Ctrl.add(ln='twist', k=True)
		self.LegIk_Ctrl.attr('twist') >> self.LegIk_Ikh.attr('twist')

		# IK rig - Strt attributes
		ikCtrlShp = lrc.Dag(self.LegIk_Ctrl.shape)

		self.LegIk_Ctrl.add(ln='__stretch__', k=True)
		self.LegIk_Ctrl.attr('__stretch__').set(l=True)
		self.LegIk_Ctrl.add(ln='autoStretch', min=0, max=1, k=True)
		self.LegIk_Ctrl.add(ln='upLegStretch', k=True)
		self.LegIk_Ctrl.add(ln='lowLegStretch', k=True)
		self.LegIk_Ctrl.add(ln='toeStretch', k=True)

		# IK rig - Roll attributes
		self.LegIk_Ctrl.add(ln='__foot__', k=True)
		self.LegIk_Ctrl.attr('__foot__').set(l=True)
		
		attrs = (
					'heelRoll',
					'ballRoll',
					'footRoll',
					'toeRoll',
					'heelTwist',
					'toeTwist',
					'footRock',
					'toeBend'
				)
		for attr in attrs :
			self.LegIk_Ctrl.add(ln=attr, k=True)

		# self.LegIk_Ctrl.attr('heelRoll') >> self.HeelIkPiv_Grp.attr('rx')
		# self.LegIk_Ctrl.attr('ballRoll') >> self.AnkleRollIkPiv_Grp.attr('rx')
		# self.LegIk_Ctrl.attr('toeRoll') >> self.ToeIkPiv_Grp.attr('rx')

		self.LegIk_Ctrl.attr('footRoll') >> self.FootMidIkPiv_Grp.attr('rx')
		self.LegIk_Ctrl.attr('heelTwist') >> self.HeelIkPiv_Grp.attr('rz')
		self.LegIk_Ctrl.attr('toeTwist') >> self.ToeIkPiv_Grp.attr('rz')
		self.LegIk_Ctrl.attr('toeBend') >> self.ToeBendIkPiv_Grp.attr('rx')
		self.LegIk_Ctrl.attr('footRock') >> self.FootInIkPiv_Grp.attr('ry')
		self.LegIk_Ctrl.attr('footRock') >> self.FootOutIkPiv_Grp.attr('ry')
		
		self.FootOutIkPiv_Grp.attr('xrye').v = 1
		self.FootOutIkPiv_Grp.attr('xryl').v = 0
		self.FootInIkPiv_Grp.attr('mrye').v = 1
		self.FootInIkPiv_Grp.attr('mryl').v = 0

		# IK rig - Roll attributes - BallRoll
		ikCtrlShp.add(ln='toeRollBreak', k=True, dv=-60)

		self.BallRollIk_Clamp = lrc.Clamp()
		self.BallRollIk_Clamp.name = 'BallRollIk%s%sClamp' % (part, side)

		self.LegIk_Ctrl.attr('ballRoll') >> self.BallRollIk_Clamp.attr('inputR')
		ikCtrlShp.attr('toeRollBreak') >> self.BallRollIk_Clamp.attr('minR')
		self.BallRollIk_Clamp.attr('outputR') >> self.AnkleRollIkPiv_Grp.attr('rx')

		# IK rig - Roll attributes - ToeRoll
		self.ToeRollIk_Clamp = lrc.Clamp()
		self.ToeRollIk_Clamp.name = 'ToeRollIk%s%sClamp' % (part, side)

		self.ToeIkPivColl_Pma = lrc.PlusMinusAverage()
		self.ToeIkPivColl_Pma.name = 'ToeIkPivColl%s%sPma' % (part, side)

		self.ToeIkPivInv_Mdv = lrc.MultiplyDivide()
		self.ToeIkPivInv_Mdv.name = 'ToeIkPivInv%s%sMdv' % (part, side)
		self.ToeIkPivInv_Mdv.attr('i2x').v = -1

		self.LegIk_Ctrl.attr('ballRoll') >> self.ToeRollIk_Clamp.attr('inputR')
		ikCtrlShp.attr('toeRollBreak') >> self.ToeRollIk_Clamp.attr('maxR')
		self.ToeRollIk_Clamp.attr('minR').v = -180
		self.ToeRollIk_Clamp.attr('outputR') >> self.ToeIkPivColl_Pma.last1D()

		ikCtrlShp.attr('toeRollBreak') >> self.ToeIkPivInv_Mdv.attr('i1x')
		self.ToeIkPivInv_Mdv.attr('ox') >> self.ToeIkPivColl_Pma.last1D()

		self.LegIk_Ctrl.attr('toeRoll') >> self.ToeIkPivColl_Pma.last1D()
		self.ToeIkPivColl_Pma.attr('output1D') >> self.ToeIkPiv_Grp.attr('rx')

		# IK rig - Roll attributes - HeelRoll
		self.HeelRollIk_Clamp = lrc.Clamp()
		self.HeelRollIk_Clamp.name = 'HeelRollIk%s%sClamp' % (part, side)

		self.HeelIkPivColl_Pma = lrc.PlusMinusAverage()
		self.HeelIkPivColl_Pma.name = 'HeelIkPivColl%s%sPma' % (part, side)

		self.LegIk_Ctrl.attr('ballRoll') >> self.HeelRollIk_Clamp.attr('inputR')
		self.HeelRollIk_Clamp.attr('maxR').v = 360
		self.HeelRollIk_Clamp.attr('outputR') >> self.HeelIkPivColl_Pma.last1D()
		self.LegIk_Ctrl.attr('heelRoll') >> self.HeelIkPivColl_Pma.last1D()

		self.HeelIkPivColl_Pma.attr('output1D') >> self.HeelIkPiv_Grp.attr('rx')
		
		# IK rig - Stretch setup
		self.UpLegIkJntPnt_Grp = lrc.Null()
		self.LegIkCtrlPnt_Grp = lrc.Null()
		self.KneeIkCtrlPnt_Grp = lrc.Null()

		self.UpLegIkJntPnt_Grp.name = 'UpLegIkJntPnt%s%sGrp' % (part, side)
		self.LegIkCtrlPnt_Grp.name = 'LegIkCtrlPnt%s%sGrp' % (part, side)
		self.KneeIkCtrlPnt_Grp.name = 'KneeIkCtrlPnt%s%sGrp' % (part, side)

		lrc.pointConstraint(self.LegIkRoot_Ctrl, self.UpLegIkJntPnt_Grp)
		lrc.pointConstraint(self.LegIkIkhZr_Grp, self.LegIkCtrlPnt_Grp)
		lrc.pointConstraint(self.KneeIk_Ctrl, self.KneeIkCtrlPnt_Grp)
		
		self.UpLegIkJntPnt_Grp.parent(self.LegIkRoot_Ctrl)
		self.LegIkCtrlPnt_Grp.parent(self.LegIkRoot_Ctrl)
		self.KneeIkCtrlPnt_Grp.parent(self.LegIkRoot_Ctrl)
		
		self.LegIkAtStrt_Dist = lrc.DistanceBetween()
		self.UpLegIkLock_Dist = lrc.DistanceBetween()
		self.LowLegIkLock_Dist = lrc.DistanceBetween()

		self.LegIkAtStrt_Dist.name = 'LegIkAtStrt%s%sDist' % (part, side)
		self.UpLegIkLock_Dist.name = 'UpLegIkLock%s%sDist' % (part, side)
		self.LowLegIkLock_Dist.name = 'LowLegIkLock%s%sDist' % (part, side)

		self.UpLegIkJntPnt_Grp.attr('t') >> self.LegIkAtStrt_Dist.attr('p1')
		self.LegIkCtrlPnt_Grp.attr('t') >> self.LegIkAtStrt_Dist.attr('p2')
		self.UpLegIkJntPnt_Grp.attr('t') >> self.UpLegIkLock_Dist.attr('p1')
		self.KneeIkCtrlPnt_Grp.attr('t') >> self.UpLegIkLock_Dist.attr('p2')
		self.KneeIkCtrlPnt_Grp.attr('t') >> self.LowLegIkLock_Dist.attr('p1')
		self.LegIkCtrlPnt_Grp.attr('t') >> self.LowLegIkLock_Dist.attr('p2')
		
		self.LegIkAtStrt_Cnd = lrc.Condition()
		self.LegIkAtStrt_Mul = lrc.MultDoubleLinear()
		self.LegIkAtStrtDiv_Mdv = lrc.MultiplyDivide()
		
		self.UpLegIkAtStrt_Mul = lrc.MultDoubleLinear()
		self.UpLegIkStrt_Mul = lrc.MultDoubleLinear()
		self.UpLegIkAtStrt_Add = lrc.AddDoubleLinear()
		self.UpLegIkAtStrt_Blend = lrc.BlendTwoAttr()
		
		self.LowLegIkAtStrt_Mul = lrc.MultDoubleLinear()
		self.LowLegIkStrt_Mul = lrc.MultDoubleLinear()
		self.LowLegIkAtStrt_Add = lrc.AddDoubleLinear()
		self.LowLegIkAtStrt_Blend = lrc.BlendTwoAttr()
		
		self.LegIkAtStrt_Cnd.name = 'LegIkAtStrt%s%sCnd' % (part, side)
		self.LegIkAtStrt_Mul.name = 'LegIkAtStrt%s%sMul' % (part, side)
		self.LegIkAtStrtDiv_Mdv.name = 'LegIkAtStrtDiv%s%sMdv' % (part, side)

		self.UpLegIkAtStrt_Mul.name = 'UpLegIkAtStrt%s%sMul' % (part, side)
		self.UpLegIkStrt_Mul.name = 'UpLegIkStrt%s%sMul' % (part, side)
		self.UpLegIkAtStrt_Add.name = 'UpLegIkAtStrt%s%sAdd' % (part, side)
		self.UpLegIkAtStrt_Blend.name = 'UpLegIkAtStrt%s%sBlend' % (part, side)

		self.LowLegIkAtStrt_Mul.name = 'LowLegIkAtStrt%s%sMul' % (part, side)
		self.LowLegIkStrt_Mul.name = 'LowLegIkStrt%s%sMul' % (part, side)
		self.LowLegIkAtStrt_Add.name = 'LowLegIkAtStrt%s%sAdd' % (part, side)
		self.LowLegIkAtStrt_Blend.name = 'LowLegIkAtStrt%s%sBlend' % (part, side)

		# IK Stretch - Auto stretch
		ikCtrlDist = self.LegIkAtStrt_Dist.attr('d').v
		upLegDist = abs(self.LowLegIk_Jnt.attr('ty').v)
		lowLegDist = abs(self.AnkleIk_Jnt.attr('ty').v)

		ikCtrlShp.add(ln='atStrtDiv')
		ikCtrlShp.attr('atStrtDiv').v = ikCtrlDist

		ikCtrlShp.add(ln='atStrtCon')
		ikCtrlShp.attr('atStrtCon').v = upLegDist + lowLegDist

		ikCtrlShp.add(ln='upLegLen')
		ikCtrlShp.attr('upLegLen').v = upLegDist

		ikCtrlShp.add(ln='lowLegLen')
		ikCtrlShp.attr('lowLegLen').v = lowLegDist
		
		self.LegIkAtStrt_Dist.attr('d') >> self.LegIkAtStrt_Cnd.attr('ft')
		# self.LegIkAtStrt_Cnd.attr('st').v = upLegDist + lowLegDist
		ikCtrlShp.attr('atStrtCon') >> self.LegIkAtStrt_Cnd.attr('st')
		self.LegIkAtStrt_Cnd.attr('op').v = 2
		self.LegIkAtStrt_Cnd.attr('cfr').v = 1
		self.LegIkAtStrt_Dist.attr('d') >> self.LegIkAtStrt_Mul.attr('i1')

		self.LegIkAtStrtDiv_Mdv.attr('op').v = 2
		self.LegIkAtStrtDiv_Mdv.attr('i1x').v = 1
		# self.LegIkAtStrtDiv_Mdv.attr('i2x').v = ikCtrlDist
		ikCtrlShp.attr('atStrtDiv') >> self.LegIkAtStrtDiv_Mdv.attr('i2x')
		self.LegIkAtStrtDiv_Mdv.attr('ox') >> self.LegIkAtStrt_Mul.attr('i2')
		self.LegIkAtStrt_Mul.attr('o') >> self.LegIkAtStrt_Cnd.attr('ctr')
		
		self.LegIkAtStrt_Cnd.attr('ocr') >> self.UpLegIkAtStrt_Mul.attr('i1')
		# self.UpLegIkAtStrt_Mul.attr('i2').v = upLegDist
		ikCtrlShp.attr('upLegLen') >> self.UpLegIkAtStrt_Mul.attr('i2')
		self.LegIk_Ctrl.attr('autoStretch') >> self.UpLegIkAtStrt_Blend.attr('ab')
		# self.UpLegIkAtStrt_Blend.add(ln = 'default', dv = upLegDist, k = True)
		# self.UpLegIkAtStrt_Blend.attr('default') >> self.UpLegIkAtStrt_Blend.last()
		ikCtrlShp.attr('upLegLen') >> self.UpLegIkAtStrt_Blend.last()
		self.UpLegIkAtStrt_Mul.attr('o') >> self.UpLegIkAtStrt_Blend.last()
		self.UpLegIkAtStrt_Blend.attr('o') >> self.UpLegIkAtStrt_Add.attr('i1')
		self.LegIk_Ctrl.attr('upLegStretch') >> self.UpLegIkStrt_Mul.attr('i1')
		# self.UpLegIkStrt_Mul.attr('i2').v = upLegDist
		ikCtrlShp.attr('upLegLen') >> self.UpLegIkStrt_Mul.attr('i2')
		self.UpLegIkStrt_Mul.attr('o') >> self.UpLegIkAtStrt_Add.attr('i2')
		
		self.LegIkAtStrt_Cnd.attr('ocr') >> self.LowLegIkAtStrt_Mul.attr('i1')
		# self.LowLegIkAtStrt_Mul.attr('i2').v = lowLegDist
		ikCtrlShp.attr('lowLegLen') >> self.LowLegIkAtStrt_Mul.attr('i2')
		self.LegIk_Ctrl.attr('autoStretch') >> self.LowLegIkAtStrt_Blend.attr('ab')
		# self.LowLegIkAtStrt_Blend.add(ln = 'default', dv = lowLegDist, k = True)
		# self.LowLegIkAtStrt_Blend.attr('default') >> self.LowLegIkAtStrt_Blend.last()
		ikCtrlShp.attr('lowLegLen') >> self.LowLegIkAtStrt_Blend.last()
		self.LowLegIkAtStrt_Mul.attr('o') >> self.LowLegIkAtStrt_Blend.last()
		self.LowLegIkAtStrt_Blend.attr('o') >> self.LowLegIkAtStrt_Add.attr('i1')
		self.LegIk_Ctrl.attr('lowLegStretch') >> self.LowLegIkStrt_Mul.attr('i1')
		# self.LowLegIkStrt_Mul.attr('i2').v = lowLegDist
		ikCtrlShp.attr('lowLegLen') >> self.LowLegIkStrt_Mul.attr('i2')
		self.LowLegIkStrt_Mul.attr('o') >> self.LowLegIkAtStrt_Add.attr('i2')

		# IK rig - Toe stretch
		(self.LegIkToeStrt_Add,
		self.LegIkToeStrt_Mul) = lrr.fkStretch(
													ctrl=self.LegIk_Ctrl,
													attr='toeStretch',
													target=self.ToeIk_Jnt
												)

		self.LegIkToeStrt_Add.name = 'LegIkToeStrt%s%sAdd' % (part, side)
		self.LegIkToeStrt_Mul.name = 'LegIkToeStrt%s%sMul' % (part, side)

		# IK rig - Knee lock
		self.KneeIk_Ctrl.add(ln='lock', min=0, max=1, k=True)
		
		self.UpLegIkLockLen_Mul = lrc.MultDoubleLinear()
		self.UpLegIkLockDiv_Mdv = lrc.MultiplyDivide()
		self.UpLegIkLock_Mul = lrc.MultDoubleLinear()
		self.UpLegIkLock_Blend = lrc.BlendTwoAttr()

		self.UpLegIkLockLen_Mul.name = 'UpLegIkLockLen%s%sMul' % (part, side)
		self.UpLegIkLockDiv_Mdv.name = 'UpLegIkLockDiv%s%sMdv' % (part, side)
		self.UpLegIkLock_Mul.name = 'UpLegIkLock%s%sMul' % (part, side)
		self.UpLegIkLock_Blend.name = 'UpLegIkLock%s%sBlend' % (part, side)
		
		self.LowLegIkLockLen_Mul = lrc.MultDoubleLinear()
		self.LowLegIkLockDiv_Mdv = lrc.MultiplyDivide()
		self.LowLegIkLock_Mul = lrc.MultDoubleLinear()
		self.LowLegIkLock_Blend = lrc.BlendTwoAttr()

		self.LowLegIkLockLen_Mul.name = 'LowLegIkLockLen%s%sMul' % (part, side)
		self.LowLegIkLockDiv_Mdv.name = 'LowLegIkLockDiv%s%sMdv' % (part, side)
		self.LowLegIkLock_Mul.name = 'LowLegIkLock%s%sMul' % (part, side)
		self.LowLegIkLock_Blend.name = 'LowLegIkLock%s%sBlend' % (part, side)

		self.UpLegIkLock_Dist.attr('d') >> self.UpLegIkLockLen_Mul.attr('i1')
		self.UpLegIkLockDiv_Mdv.attr('op').v = 2
		self.UpLegIkLockDiv_Mdv.attr('i1x').v = 1
		self.UpLegIkLockDiv_Mdv.attr('i2x').v = upLegDist
		self.UpLegIkLockDiv_Mdv.attr('ox') >> self.UpLegIkLockLen_Mul.attr('i2')

		self.UpLegIkLockLen_Mul.attr('o') >> self.UpLegIkLock_Mul.attr('i1')
		self.UpLegIkLock_Mul.attr('i2').v = upLegDist
		self.UpLegIkAtStrt_Add.attr('o') >> self.UpLegIkLock_Blend.last()
		self.UpLegIkLock_Mul.attr('o') >> self.UpLegIkLock_Blend.last()
		self.KneeIk_Ctrl.attr('lock') >> self.UpLegIkLock_Blend.attr('ab')
		
		self.LowLegIkLock_Dist.attr('d') >> self.LowLegIkLockLen_Mul.attr('i1')
		self.LowLegIkLockDiv_Mdv.attr('op').v = 2
		self.LowLegIkLockDiv_Mdv.attr('i1x').v = 1
		self.LowLegIkLockDiv_Mdv.attr('i2x').v = lowLegDist
		self.LowLegIkLockDiv_Mdv.attr('ox') >> self.LowLegIkLockLen_Mul.attr('i2')

		self.LowLegIkLockLen_Mul.attr('o') >> self.LowLegIkLock_Mul.attr('i1')
		self.LowLegIkLock_Mul.attr('i2').v = lowLegDist
		self.LowLegIkAtStrt_Add.attr('o') >> self.LowLegIkLock_Blend.last()
		self.LowLegIkLock_Mul.attr('o') >> self.LowLegIkLock_Blend.last()
		self.KneeIk_Ctrl.attr('lock') >> self.LowLegIkLock_Blend.attr('ab')

		# IK rig - Auto stretch - Finalize
		self.UpLegIkFinStrtVal_Mul = lrc.MultDoubleLinear()
		self.LowLegIkFinStrtVal_Mul = lrc.MultDoubleLinear()

		self.UpLegIkFinStrtVal_Mul.name = 'UpLegIkFinStrtVal%s%sMul' % (part, side)
		self.LowLegIkFinStrtVal_Mul.name = 'LowLegIkFinStrtVal%s%sMul' % (part, side)

		if self.LowLegIk_Jnt.attr('ty').v > 0 :
			self.UpLegIkFinStrtVal_Mul.attr('i2').v = 1
		else :
			self.UpLegIkFinStrtVal_Mul.attr('i2').v = -1

		if self.AnkleIk_Jnt.attr('ty').v > 0 :
			self.LowLegIkFinStrtVal_Mul.attr('i2').v = 1
		else :
			self.LowLegIkFinStrtVal_Mul.attr('i2').v = -1
		
		self.UpLegIkLock_Blend.attr('o') >> self.UpLegIkFinStrtVal_Mul.attr('i1')
		self.UpLegIkFinStrtVal_Mul.attr('o') >> self.LowLegIk_Jnt.attr('ty')

		self.LowLegIkLock_Blend.attr('o') >> self.LowLegIkFinStrtVal_Mul.attr('i1')
		self.LowLegIkFinStrtVal_Mul.attr('o') >> self.AnkleIk_Jnt.attr('ty')

		# IK rig - Foot scale
		self.Leg_Ctrl.attr('footScale') >> self.AnkleIk_Jnt.attr('sx')
		self.Leg_Ctrl.attr('footScale') >> self.AnkleIk_Jnt.attr('sy')
		self.Leg_Ctrl.attr('footScale') >> self.AnkleIk_Jnt.attr('sz')
		self.Leg_Ctrl.attr('footScale') >> self.LegIkPiv_Grp.attr('sx')
		self.Leg_Ctrl.attr('footScale') >> self.LegIkPiv_Grp.attr('sy')
		self.Leg_Ctrl.attr('footScale') >> self.LegIkPiv_Grp.attr('sz')
		
		# FK/IK blending
		self.LegFkIk_Rev = lrc.Reverse()
		self.LegFkIk_Rev.name = 'LegFkIk%s%sRev' % (part, side)

		self.Leg_Ctrl.add(ln='fkIk', min=0, max=1, k=True)
		self.Leg_Ctrl.attr('fkIk') >> self.LegFkIk_Rev.attr('ix')
		self.Leg_Ctrl.attr('fkIk') >> self.LegIkCtrl_Grp.attr('v')
		self.LegFkIk_Rev.attr('ox') >> self.LegFkCtrl_Grp.attr('v')

		upLegParCons = lrc.parentConstraint(self.UpLegFk_Jnt, self.UpLegIk_Jnt, self.UpLeg_Jnt)
		lowLegParCons = lrc.parentConstraint(self.LowLegFk_Jnt, self.LowLegIk_Jnt, self.LowLeg_Jnt)
		ankleParCons = lrc.parentConstraint(self.AnkleFk_Jnt, self.AnkleIk_Jnt, self.Ankle_Jnt)
		ballParCons = lrc.parentConstraint(self.BallFk_Jnt, self.BallIk_Jnt, self.Ball_Jnt)
		toeParCons = lrc.parentConstraint(self.ToeFk_Jnt, self.ToeIk_Jnt, self.Toe_Jnt)
		
		self.Leg_Ctrl.attr('fkIk') >> upLegParCons.attr('w1')
		self.Leg_Ctrl.attr('fkIk') >> lowLegParCons.attr('w1')
		self.Leg_Ctrl.attr('fkIk') >> ankleParCons.attr('w1')
		self.Leg_Ctrl.attr('fkIk') >> ballParCons.attr('w1')
		self.Leg_Ctrl.attr('fkIk') >> toeParCons.attr('w1')
		
		self.LegFkIk_Rev.attr('ox') >> upLegParCons.attr('w0')
		self.LegFkIk_Rev.attr('ox') >> lowLegParCons.attr('w0')
		self.LegFkIk_Rev.attr('ox') >> ankleParCons.attr('w0')
		self.LegFkIk_Rev.attr('ox') >> ballParCons.attr('w0')
		self.LegFkIk_Rev.attr('ox') >> toeParCons.attr('w0')

		# Grouping
		self.Ctrl_Grp.parent(ctrlGrp)
		self.Jnt_Grp.parent(jntGrp)

		# Ribbon
		self.LegRbnCtrl_Grp = lrc.Null()
		self.LegRbnCtrl_Grp.name = 'LegRbnCtrl%s%sGrp' % (part, side)

		self.LegRbnCtrl_Grp.snap(self.Ctrl_Grp)

		rbnAx = 'y-'
		rbnAim = (0, -1, 0)
		rbnUp = (0, 0, 1)
		rbnAmp = 1

		if side == '_R_' :
			rbnUp = (0, 0, -1)
			rbnAmp = -1
				
		# Ribbon control
		self.LegRbn_Ctrl = lrc.Control('plus')
		self.LegRbn_Ctrl.name = 'LegRbn%s%sCtrl' % (part, side)
		self.LegRbn_Ctrl.lockHideAttrs('rx', 'ry', 'rz', 'sx', 'sy', 'sz', 'v')

		self.LegRbnCtrlZr_Grp = lrc.group(self.LegRbn_Ctrl)
		self.LegRbnCtrlZr_Grp.name = 'LegRbnCtrlZr%s%sGrp' % (part, side)

		mc.pointConstraint(self.LowLeg_Jnt, self.LegRbnCtrlZr_Grp)
		mc.orientConstraint(self.UpLeg_Jnt, self.LegRbnCtrlZr_Grp, mo=True)

		# Ribbon Control - Color
		if side == '_R_':
			self.LegRbn_Ctrl.color = 'blue'
		else:
			self.LegRbn_Ctrl.color = 'red'
		
		# Ribbon - Upper leg
		if ribbon :
			self.UpLegRbn = lrb.RibbonIkHi(
												size=self.upLegLen,
												ax=rbnAx,
												part='UpLeg%s' % part,
												side=side.replace('_','')
											)
		else :
			self.UpLegRbn = lrb.RibbonIkLow(
												size=self.upLegLen,
												ax=rbnAx,
												part='UpLeg%s' % part,
												side=side.replace('_','')
											)
		
		self.UpLegRbn.Ctrl_Grp.snapPoint(self.UpLeg_Jnt)
		mc.delete(
					mc.aimConstraint(
										self.LowLeg_Jnt,
										self.UpLegRbn.Ctrl_Grp,
										aim=rbnAim, u=rbnUp,
										wut='objectrotation', wuo=self.UpLeg_Jnt,
										wu=(0,0,1)
									)
				)
		mc.pointConstraint(self.LegRbn_Ctrl, self.UpLegRbn.RbnEnd_Ctrl)

		self.UpLegRbn.RbnRootTws_Grp.rotateOrder = 'yzx'
		self.UpLegRbn.RbnEndTws_Grp.rotateOrder = 'yzx'

		lrc.parentConstraint(
									self.UpLeg_Jnt,
									self.UpLegRbn.Ctrl_Grp,
									mo=True
								)

		mc.parentConstraint(
								self.UpLegNr_Jnt,
								self.UpLegRbn.RbnRootTwsZr_Grp
							)
		mc.parentConstraint(
								self.UpLeg_Jnt,
								self.UpLegRbn.RbnRootTws_Grp
							)

		mc.parentConstraint(
								self.LowLegNr_Jnt,
								self.UpLegRbn.RbnEndTwsZr_Grp
							)
		mc.parentConstraint(
								self.LowLeg_Jnt,
								self.UpLegRbn.RbnEndTws_Grp
							)

		upLegRbnShp = lrc.Dag(self.UpLegRbn.Rbn_Ctrl.shape)
		upLegRbnShp.attr('rootTwistAmp').v = 1 * rbnAmp
		upLegRbnShp.attr('endTwistAmp').v = -1 * rbnAmp

		# Ribbon - Lower leg
		if ribbon :
			self.LowLegRbn = lrb.RibbonIkHi(
												size=self.lowLegLen,
												ax=rbnAx,
												part='LowLeg%s' % part,
												side=side.replace('_','')
											)
		else :
			self.LowLegRbn = lrb.RibbonIkLow(
												size=self.lowLegLen,
												ax=rbnAx,
												part='LowLeg%s' % part,
												side=side.replace('_','')
											)

		self.LowLegRbn.Ctrl_Grp.snapPoint(self.LowLeg_Jnt)
		mc.delete(
					mc.aimConstraint(
										self.Ankle_Jnt,
										self.LowLegRbn.Ctrl_Grp,
										aim=rbnAim, u=rbnUp,
										wut='objectrotation', wuo=self.LowLeg_Jnt,
										wu=(0,0,1)
									)
				)
		mc.parentConstraint(self.LowLeg_Jnt, self.LowLegRbn.Ctrl_Grp, mo = True)
		mc.pointConstraint(self.LegRbn_Ctrl, self.LowLegRbn.RbnRoot_Ctrl)
		mc.pointConstraint(self.Ankle_Jnt, self.LowLegRbn.RbnEnd_Ctrl)
		
		lowLegRbnShp = lrc.Dag(self.LowLegRbn.Rbn_Ctrl.shape)
		lowLegRbnShp.attr('rootTwistAmp').v = 1 * rbnAmp
		lowLegRbnShp.attr('endTwistAmp').v = -1 * rbnAmp
		
		self.LowLegRbn.RbnRootTwsZr_Grp.snap(self.LowLeg_Jnt)
		self.LowLegRbn.RbnEndTwsZr_Grp.snap(self.Ankle_Jnt)

		mc.parentConstraint(
								self.LowLeg_Jnt,
								self.LowLegRbn.RbnEndTwsZr_Grp,
								mo=True
							)
		mc.parentConstraint(
								self.Ankle_Jnt,
								self.LowLegRbn.RbnEndTws_Grp,
								mo=True
							)
		
		self.LowLegRbn.RbnRootTws_Grp.rotateOrder = 'yzx'
		self.LowLegRbn.RbnEndTws_Grp.rotateOrder = 'yzx'
		
		# Ribbon - Grouping
		self.LegRbnCtrlZr_Grp.parent(self.LegRbnCtrl_Grp)
		self.UpLegRbn.Ctrl_Grp.parent(self.LegRbnCtrl_Grp)
		self.UpLegRbn.Skin_Grp.parent(skinGrp)
		self.UpLegRbn.Jnt_Grp.parent(jntGrp)
		self.LowLegRbn.Ctrl_Grp.parent(self.LegRbnCtrl_Grp)
		self.LowLegRbn.Skin_Grp.parent(skinGrp)
		self.LowLegRbn.Jnt_Grp.parent(jntGrp)
		self.LegRbnCtrl_Grp.parent(self.Ctrl_Grp)
		
		if ribbon :
			self.UpLegRbn.Still_Grp.parent(stillGrp)
			self.LowLegRbn.Still_Grp.parent(stillGrp)
		
		self.UpLegRbn.RbnRoot_Ctrl.lockHideAttrs('tx', 'ty', 'tz')
		self.UpLegRbn.RbnEnd_Ctrl.lockHideAttrs('tx', 'ty', 'tz')
		self.LowLegRbn.RbnRoot_Ctrl.lockHideAttrs('tx', 'ty', 'tz')
		self.LowLegRbn.RbnEnd_Ctrl.lockHideAttrs('tx', 'ty', 'tz')
		
		self.UpLegRbn.RbnRoot_Ctrl.hide()
		self.UpLegRbn.RbnEnd_Ctrl.hide()
		self.LowLegRbn.RbnRoot_Ctrl.hide()
		self.LowLegRbn.RbnEnd_Ctrl.hide()
		
		# Set default
		self.Leg_Ctrl.attr('fkIk').v = 1
		self.LegIk_Ctrl.attr('localWorld').v = 1
		self.UpLegFk_Ctrl.attr('localWorld').v = 1
		
		self.UpLegRbn.Rbn_Ctrl.attr('autoTwist').v = 0
		self.LowLegRbn.Rbn_Ctrl.attr('autoTwist').v = 0