import maya.cmds as mc
import maya.mel as mm
from functools import partial

def run():
	# AttributeMover call back
	ui = AttributeMover()
	ui.show()

	return ui

def moveAttr(mode='down', *args):

	objs = mc.channelBox('mainChannelBox', q=True, mol=True)
	attrs = mc.channelBox('mainChannelBox', q=True, sma=True)

	if (not attrs) or (not objs):
		# Quit, if there is no object or attribute selected.
		return False

	for obj in objs:

		udAttrs = mc.listAttr(obj, ud=True)
		
		if not attrs[0] in udAttrs:
			# Quit, if selected attribute is not a ud attr.
			return False

		# Unlock all locked attributes.
		lockAttrs = mc.listAttr(obj, ud=True, l=True)
		if lockAttrs:
			for attr in lockAttrs:
				mc.setAttr('{obj}.{attr}'.format(obj=obj, attr=attr), l=False)

		# Move down
		if mode == 'down':

			if len(attrs) > 1:
				attrs.reverse()

			for attr in attrs:
				attrIdx = udAttrs.index(attr)
				mc.deleteAttr(obj, at=udAttrs[attrIdx])
				mc.undo()

				for ix in range(attrIdx+2, len(udAttrs), 1):
					mc.deleteAttr(obj, at=udAttrs[ix])
					mc.undo()

		# Move up
		if mode == 'up':
			for attr in attrs:
				attrIdx = udAttrs.index(attr)
				if udAttrs[attrIdx-1]:
					mc.deleteAttr(obj, at=udAttrs[attrIdx-1])
					mc.undo()

				for ix in range(attrIdx+1, len(udAttrs), 1):
					mc.deleteAttr(obj, at=udAttrs[ix])
					mc.undo()

		# Relock attrs
		if lockAttrs:
			for attr in lockAttrs:
				mc.setAttr('{obj}.{attr}'.format(obj=obj, attr=attr), l=True)

class AttributeMover():
	
	def __init__(self):
		self.ui = 'pkAttributeMover'
		self.win = '%sWin' % self.ui

	def show(self):

		if mc.window(self.win, exists=True):
			mc.deleteUI(self.win)

		mc.window(self.win, t='pkAttributeMover', rtf=True)
		mc.rowLayout(adj=1, nc=2)

		mc.button(h=100, w=100, l='Up', c=partial(moveAttr, 'up'))
		mc.button(h=100, w=100, l='Dn', c=partial(moveAttr, 'down'))

		mc.showWindow(self.win)
		mc.window(self.win, e=True, w=200)
		mc.window(self.win, e=True, h=100)