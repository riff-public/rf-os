import maya.cmds as mc
import maya.mel as mm
import maya.cmds as mc
import maya.OpenMaya as om

# 'maya.OpenMaya.MItSelectionList'
# 'getPlug', 'getStrings', 'hasComponents', 'isDone', 'itemType', 'kAnimSelectionItem', 
# 'kDNselectionItem', 'kDagSelectionItem', 'kPlugSelectionItem', 'kUnknownItem', 'next', 
# 'reset', 'setFilter', 'this'

# 'maya.OpenMaya.MPointArray'
# 'append', 'assign', 'className', 'clear', 'copy', 'get', 'insert', 'length', 'remove', 
# 'set', 'setLength', 'setSizeIncrement', 'sizeIncrement', 'this'

# 'maya.OpenMaya.MPoint'
# 'assign', 'cartesianize', 'className', 'distanceTo', 'get', 'homogenize', 
# 'isEquivalent', 'origin', 'rationalize', 'this', 'w', 'x', 'y', 'z'

# maya.OpenMaya.MItMeshVertex
# 'className', 'connectedToEdge', 'connectedToFace', 'count', 'currentItem', 
# 'geomChanged', 'getColor', 'getColorIndices', 'getColors', 'getConnectedEdges', 
# 'getConnectedFaces', 'getConnectedVertices', 'getNormal', 'getNormalIndices', 
# 'getNormals', 'getOppositeVertex', 'getUV', 'getUVIndices', 'getUVs', 'hasColor', 
# 'index', 'isDone', 'next', 'numConnectedEdges', 'numConnectedFaces', 'numUVs', 
# 'onBoundary', 'position', 'reset', 'setIndex', 'setPosition', 'setUV', 'setUVs', 
# 'this', 'translateBy', 'updateSurface', 'vertex'

# maya.OpenMaya.MPoint
# 'assign', 'cartesianize', 'className', 'distanceTo', 'get', 'homogenize', 'isEquivalent', 
# 'origin', 'rationalize', 'this', 'w', 'x', 'y', 'z'

def print_obj(obj):

	print str(type(obj)).split('\'')[-2]
	print [ x for x in dir(obj) if not x.startswith('__')]

def main():

	sel_list = om.MSelectionList()
	om.MGlobal.getActiveSelectionList(sel_list)
	sel_iter = om.MItSelectionList(sel_list)

	vtx_iter = None

	# Populating reference position and selected vertices.
	ref_pnt_ary = om.MPointArray()
	while not sel_iter.isDone():

		curr_dag_path = om.MDagPath()
		curr_comp = om.MObject()
		sel_iter.getDagPath(curr_dag_path, curr_comp)

		if sel_iter.hasComponents():
			vtx_iter = om.MItMeshVertex(curr_dag_path, curr_comp)
			
		else:
			curr_mfn = om.MFnMesh(curr_dag_path)
			curr_mfn.getPoints(ref_pnt_ary, om.MSpace.kWorld)

		sel_iter.next()

	while not vtx_iter.isDone():
		
		curr_pnt = vtx_iter.position(om.MSpace.kWorld)
		
		tar_pos = None
		min_dist = None
		for ix in xrange(ref_pnt_ary.length()):
			
			curr_dist = curr_pnt.distanceTo(ref_pnt_ary[ix])
			
			if (min_dist == None) or (curr_dist < min_dist):
				min_dist = curr_dist
				tar_pos = ref_pnt_ary[ix]
			
		vtx_iter.setPosition(tar_pos, om.MSpace.kWorld)

		vtx_iter.next()

	return True
