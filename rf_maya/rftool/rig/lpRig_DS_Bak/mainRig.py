'''

How to run:

import sys

sys.path.append(r'X:\HW\Character\Extras\LeafBlower\Rig\rigData')

import lbMainRig
reload(lbMainRig)

lbMainRig.main()

'''

import maya.cmds as mc

import sys
sys.path.append(r'X:\Lani Pixels\Libraries\Maya\mayaLpPython')

from lpRig import rigTools as lrr
from lpRig import core as lrc

from lpRig import rigData
reload(rigData)

from lpRig import mainGroup
reload(mainGroup)
from lpRig import rootRig
reload(rootRig)
from lpRig import torsoRig
reload(torsoRig)
from lpRig import neckRig
reload(neckRig)
from lpRig import headRig
reload(headRig)
from lpRig import clavRig
reload(clavRig)
from lpRig import armRig
reload(armRig)
from lpRig import thumbRig
reload(thumbRig)
from lpRig import fingerRig
reload(fingerRig)
from lpRig import legRig
reload(legRig)
from lpRig import fkRig
reload(fkRig)
from lpRig import pointRig
reload(pointRig)
from lpRig import nonRollRig
reload(nonRollRig)
import lpRig.ribbon as lrb
reload(lrb)

def main():

	print 'Creating Main Groups'
	mgObj = mainGroup.MainGroup('LeafBlower')

	print 'Creating Root Rig'
	rootObj = rootRig.RootRig(
								ctrlGrp=mgObj.Ctrl_Grp,
								skinGrp=mgObj.Skin_Grp,
								tmpJnt='Root_Jnt',
								part=''
							)

	print 'Creating Torso Rig'
	torsoObj = torsoRig.TorsoRig(
									parent=rootObj.Root_Jnt,
									ctrlGrp=mgObj.Ctrl_Grp,
									jntGrp=mgObj.Jnt_Grp,
									skinGrp=mgObj.Skin_Grp,
									stillGrp=mgObj.Still_Grp,
									tmpJnt=[
												'Pelvis_Jnt',
												'Spine_Jnt',
												'Chest_Jnt',
												'ChestTip_Jnt',
												'SpineMid_Jnt'
											],
									part=''
								)

	print 'Creating Left Clav Rig'
	lftClavObj = clavRig.ClavRig(
									parent=torsoObj.Chest_Jnt,
									ctrlGrp=mgObj.Ctrl_Grp,
									ax='y',
									tmpJnt=[
												'Clav_L_Jnt',
												'ClavTip_L_Jnt'
											],
									part='',
									side='L'
								)

	print 'Creating Right Clav Rig'
	rgtClavObj = clavRig.ClavRig(
									parent=torsoObj.Chest_Jnt,
									ctrlGrp=mgObj.Ctrl_Grp,
									ax='y',
									tmpJnt=[
												'Clav_R_Jnt',
												'ClavTip_R_Jnt'
											],
									part='',
									side='R'
								)

	print 'Creating Neck Rig'
	neckObj = neckRig.NeckRig(
								parent=torsoObj.ChestTip_Jnt,
								ctrlGrp=mgObj.Ctrl_Grp,
								skinGrp=mgObj.Skin_Grp,
								jntGrp=mgObj.Jnt_Grp,
								stillGrp=mgObj.Still_Grp,
								ax='y',
								tmpJnt=[
										'Neck_Jnt',
										'NeckTip_Jnt'
										],
								part='',
								headJnt='Head_Jnt'
							)

	print 'Creating Left Arm Rig'
	lftArmObj = armRig.ArmRig(
								parent=lftClavObj.ClavTip_Jnt,
								ctrlGrp=mgObj.Ctrl_Grp,
								jntGrp=mgObj.Jnt_Grp,
								ikhGrp=mgObj.Ikh_Grp,
								skinGrp=mgObj.Skin_Grp,
								stillGrp=mgObj.Still_Grp,
								ribbon=True,
								tmpJnt=[
											'UpArm_L_Jnt',
											'Forearm_L_Jnt',
											'Wrist_L_Jnt',
											'Hand_L_Jnt',
											'ElbowIk_L_Jnt'
										],
								part='',
								side='L'
							)

	print 'Creating Head Rig'
	headObj = headRig.HeadRig(
								parent=neckObj.NeckTip_Jnt,
								ctrlGrp=mgObj.Ctrl_Grp,
								tmpJnt=[
											'Head_Jnt',
											'HeadTip_Jnt',
											'Eye_L_Jnt',
											'Eye_R_Jnt',
											'Jaw_1_Jnt',
											'Jaw_2_Jnt',
											'Jaw_3_Jnt',
											'EyeTar_Jnt',
											'EyeTar_L_Jnt',
											'EyeTar_R_Jnt'
										],
								part=''
							)

	print 'Creating Right Arm Rig'
	rgtArmObj = armRig.ArmRig(
								parent=rgtClavObj.ClavTip_Jnt,
								ctrlGrp=mgObj.Ctrl_Grp,
								jntGrp=mgObj.Jnt_Grp,
								ikhGrp=mgObj.Ikh_Grp,
								skinGrp=mgObj.Skin_Grp,
								stillGrp=mgObj.Still_Grp,
								ribbon=True,
								tmpJnt=[
											'UpArm_R_Jnt',
											'Forearm_R_Jnt',
											'Wrist_R_Jnt',
											'Hand_R_Jnt',
											'ElbowIk_R_Jnt'
										],
								part='',
								side='R'
							)

	print 'Creating Left Thumb Rig'
	lftThumbObj = thumbRig.ThumbRig(
										thumb='Thumb',
										parent=lftArmObj.Hand_Jnt,
										armCtrl=lftArmObj.Arm_Ctrl,
										ctrlGrp=mgObj.Ctrl_Grp,
										tmpJnt=(
													'Thumb_1_L_Jnt',
													'Thumb_2_L_Jnt',
													'Thumb_3_L_Jnt',
													'Thumb_4_L_Jnt'
												),
										side='L'
									)
	ctrlShp = lrc.Dag(lftArmObj.Arm_Ctrl.shape)
	fngr = 'Thumb'
	ctrlShp.attr('%sFlatRx_1' % fngr).value = -2
	ctrlShp.attr('%sFlatRz_1' % fngr).value = -0.5
	ctrlShp.attr('%sFlatRx_2' % fngr).value = -0.3
	ctrlShp.attr('%sFlatRx_3' % fngr).value = 3.3

	print 'Creating Left Index Rig'
	lftIndexObj = fingerRig.FingerRig(
											fngr='Index',
											parent=lftArmObj.Hand_Jnt,
											armCtrl=lftArmObj.Arm_Ctrl,
											ctrlGrp=mgObj.Ctrl_Grp,
											tmpJnt=(
														'Index_1_L_Jnt',
														'Index_2_L_Jnt',
														'Index_3_L_Jnt',
														'Index_4_L_Jnt',
														'Index_5_L_Jnt'
													),
											side='L'
										)
	ctrlShp = lrc.Dag(lftArmObj.Arm_Ctrl.shape)
	fngr = 'Index'
	ctrlShp.attr('%sFlatRx_1' % fngr).value = -2
	ctrlShp.attr('%sFlatRz_1' % fngr).value = -0.5
	ctrlShp.attr('%sFlatRx_2' % fngr).value = -0.3
	ctrlShp.attr('%sFlatRx_3' % fngr).value = 3.3
	ctrlShp.attr('%sFlatRx_4' % fngr).value = 1
	ctrlShp.attr('%sCupRx_1' % fngr).value = 0
	ctrlShp.attr('%sCupRx_2' % fngr).value = -1
	ctrlShp.attr('%sCupRx_3' % fngr).value = -1
	ctrlShp.attr('%sCupRx_4' % fngr).value = -1
	ctrlShp.attr('%sSprd_2' % fngr).value = -9
	ctrlShp.attr('%sBsSprd_1' % fngr).value = -9

	print 'Creating Left Middle Rig'
	lftMiddleObj = fingerRig.FingerRig(
											fngr='Middle',
											parent=lftArmObj.Hand_Jnt,
											armCtrl=lftArmObj.Arm_Ctrl,
											ctrlGrp=mgObj.Ctrl_Grp,
											tmpJnt=(
														'Middle_1_L_Jnt',
														'Middle_2_L_Jnt',
														'Middle_3_L_Jnt',
														'Middle_4_L_Jnt',
														'Middle_5_L_Jnt'
													),
											side='L'
										)
	ctrlShp = lrc.Dag(lftArmObj.Arm_Ctrl.shape)
	fngr = 'Middle'
	ctrlShp.attr('%sFlatRx_1' % fngr).value = -2
	ctrlShp.attr('%sFlatRz_1' % fngr).value = -0.2
	ctrlShp.attr('%sFlatRx_2' % fngr).value = -0.4
	ctrlShp.attr('%sFlatRx_3' % fngr).value = 3.5
	ctrlShp.attr('%sFlatRx_4' % fngr).value = 1.25
	ctrlShp.attr('%sCupRx_1' % fngr).value = 0
	ctrlShp.attr('%sCupRx_2' % fngr).value = -2.5
	ctrlShp.attr('%sCupRx_3' % fngr).value = -2.5
	ctrlShp.attr('%sCupRx_4' % fngr).value = -2.5
	ctrlShp.attr('%sSprd_2' % fngr).value = -4.5
	ctrlShp.attr('%sBsSprd_1' % fngr).value = -4.5

	print 'Creating Left Ring Rig'
	lftRingObj = fingerRig.FingerRig(
											fngr='Ring',
											parent=lftArmObj.Hand_Jnt,
											armCtrl=lftArmObj.Arm_Ctrl,
											ctrlGrp=mgObj.Ctrl_Grp,
											tmpJnt=(
														'Ring_1_L_Jnt',
														'Ring_2_L_Jnt',
														'Ring_3_L_Jnt',
														'Ring_4_L_Jnt',
														'Ring_5_L_Jnt'
													),
											side='L'
										)
	ctrlShp = lrc.Dag(lftArmObj.Arm_Ctrl.shape)
	fngr = 'Ring'
	ctrlShp.attr('%sFlatRx_1' % fngr).value = -2
	ctrlShp.attr('%sFlatRx_2' % fngr).value = -0.2
	ctrlShp.attr('%sFlatRx_3' % fngr).value = 2.5
	ctrlShp.attr('%sFlatRx_4' % fngr).value = 2.5
	ctrlShp.attr('%sCupRx_1' % fngr).value = 0
	ctrlShp.attr('%sCupRx_2' % fngr).value = -3.5
	ctrlShp.attr('%sCupRx_3' % fngr).value = -3.5
	ctrlShp.attr('%sCupRx_4' % fngr).value = -3.5
	ctrlShp.attr('%sSprd_2' % fngr).value = 4.5
	ctrlShp.attr('%sBsSprd_1' % fngr).value = 4.5

	print 'Creating Left Pinky Base Rig'
	lftPinkyBaseObj = fkRig.FkRig(
									parent=lftArmObj.Hand_Jnt,
									ctrlGrp=mgObj.Ctrl_Grp,
									tmpJnt=['PinkyBase_L_Jnt', 'PinkyBaseTip_L_Jnt'],
									part='PinkyBase',
									side='L',
									ax='y',
									shape='cube'
									)
	mult = lrr.attrAmper(lftArmObj.Arm_Ctrl.attr('cup'), lftPinkyBaseObj.OfstGrps[0].attr('tx'), -0)
	mult.name = 'PinkyBsCupTxAmp_L_Mult'
	mult = lrr.attrAmper(lftArmObj.Arm_Ctrl.attr('cup'), lftPinkyBaseObj.OfstGrps[0].attr('tz'), -0)
	mult.name = 'PinkyBsCupTzAmp_L_Mult'

	print 'Creating Left Pinky Rig'
	lftPinkyObj = fingerRig.FingerRig(
											fngr='Pinky',
											parent=lftPinkyBaseObj.Jnts[-1],
											armCtrl=lftArmObj.Arm_Ctrl,
											ctrlGrp=mgObj.Ctrl_Grp,
											tmpJnt=(
														'Pinky_1_L_Jnt',
														'Pinky_2_L_Jnt',
														'Pinky_3_L_Jnt',
														'Pinky_4_L_Jnt',
														'Pinky_5_L_Jnt'
													),
											side='L'
										)
	ctrlShp = lrc.Dag(lftArmObj.Arm_Ctrl.shape)
	fngr = 'Pinky'
	ctrlShp.attr('%sFlatRx_1' % fngr).value = -2
	ctrlShp.attr('%sFlatRz_1' % fngr).value = 0.8
	ctrlShp.attr('%sFlatRx_2' % fngr).value = -0.5
	ctrlShp.attr('%sFlatRx_3' % fngr).value = 4.3
	ctrlShp.attr('%sFlatRx_4' % fngr).value = 0.65
	ctrlShp.attr('%sCupRx_1' % fngr).value = 0
	ctrlShp.attr('%sCupRx_2' % fngr).value = -4.5
	ctrlShp.attr('%sCupRx_3' % fngr).value = -4.5
	ctrlShp.attr('%sCupRx_4' % fngr).value = -4.5
	ctrlShp.attr('%sSprd_2' % fngr).value = 9
	ctrlShp.attr('%sBsSprd_1' % fngr).value = 9

	print 'Creating Right Thumb Rig'
	rgtThumbObj = thumbRig.ThumbRig(
										thumb='Thumb',
										parent=rgtArmObj.Hand_Jnt,
										armCtrl=rgtArmObj.Arm_Ctrl,
										ctrlGrp=mgObj.Ctrl_Grp,
										tmpJnt=(
													'Thumb_1_R_Jnt',
													'Thumb_2_R_Jnt',
													'Thumb_3_R_Jnt',
													'Thumb_4_R_Jnt'
												),
										side='R'
									)
	ctrlShp = lrc.Dag(rgtArmObj.Arm_Ctrl.shape)
	fngr = 'Thumb'
	ctrlShp.attr('%sFlatRx_1' % fngr).value = -2
	ctrlShp.attr('%sFlatRz_1' % fngr).value = -0.5
	ctrlShp.attr('%sFlatRx_2' % fngr).value = -0.3
	ctrlShp.attr('%sFlatRx_3' % fngr).value = 3.3

	print 'Creating Right Index Rig'
	rgtIndexObj = fingerRig.FingerRig(
											fngr='Index',
											parent=rgtArmObj.Hand_Jnt,
											armCtrl=rgtArmObj.Arm_Ctrl,
											ctrlGrp=mgObj.Ctrl_Grp,
											tmpJnt=(
														'Index_1_R_Jnt',
														'Index_2_R_Jnt',
														'Index_3_R_Jnt',
														'Index_4_R_Jnt',
														'Index_5_R_Jnt'
													),
											side='R'
										)
	ctrlShp = lrc.Dag(rgtArmObj.Arm_Ctrl.shape)
	fngr = 'Index'
	ctrlShp.attr('%sFlatRx_1' % fngr).value = -2
	ctrlShp.attr('%sFlatRz_1' % fngr).value = -0.5
	ctrlShp.attr('%sFlatRx_2' % fngr).value = -0.3
	ctrlShp.attr('%sFlatRx_3' % fngr).value = 3.3
	ctrlShp.attr('%sFlatRx_4' % fngr).value = 1
	ctrlShp.attr('%sCupRx_1' % fngr).value = 0
	ctrlShp.attr('%sCupRx_2' % fngr).value = -1
	ctrlShp.attr('%sCupRx_3' % fngr).value = -1
	ctrlShp.attr('%sCupRx_4' % fngr).value = -1
	ctrlShp.attr('%sSprd_2' % fngr).value = -9
	ctrlShp.attr('%sBsSprd_1' % fngr).value = -9

	print 'Creating Right Middle Rig'
	rgtMiddleObj = fingerRig.FingerRig(
											fngr='Middle',
											parent=rgtArmObj.Hand_Jnt,
											armCtrl=rgtArmObj.Arm_Ctrl,
											ctrlGrp=mgObj.Ctrl_Grp,
											tmpJnt=(
														'Middle_1_R_Jnt',
														'Middle_2_R_Jnt',
														'Middle_3_R_Jnt',
														'Middle_4_R_Jnt',
														'Middle_5_R_Jnt'
													),
											side='R'
										)
	ctrlShp = lrc.Dag(rgtArmObj.Arm_Ctrl.shape)
	fngr = 'Middle'
	ctrlShp.attr('%sFlatRx_1' % fngr).value = -2
	ctrlShp.attr('%sFlatRz_1' % fngr).value = -0.2
	ctrlShp.attr('%sFlatRx_2' % fngr).value = -0.4
	ctrlShp.attr('%sFlatRx_3' % fngr).value = 3.5
	ctrlShp.attr('%sFlatRx_4' % fngr).value = 1.25
	ctrlShp.attr('%sCupRx_1' % fngr).value = 0
	ctrlShp.attr('%sCupRx_2' % fngr).value = -2.5
	ctrlShp.attr('%sCupRx_3' % fngr).value = -2.5
	ctrlShp.attr('%sCupRx_4' % fngr).value = -2.5
	ctrlShp.attr('%sSprd_2' % fngr).value = -4.5
	ctrlShp.attr('%sBsSprd_1' % fngr).value = -4.5

	print 'Creating Right Ring Rig'
	rgtRingObj = fingerRig.FingerRig(
											fngr='Ring',
											parent=rgtArmObj.Hand_Jnt,
											armCtrl=rgtArmObj.Arm_Ctrl,
											ctrlGrp=mgObj.Ctrl_Grp,
											tmpJnt=(
														'Ring_1_R_Jnt',
														'Ring_2_R_Jnt',
														'Ring_3_R_Jnt',
														'Ring_4_R_Jnt',
														'Ring_5_R_Jnt'
													),
											side='R'
										)
	ctrlShp = lrc.Dag(rgtArmObj.Arm_Ctrl.shape)
	fngr = 'Ring'
	ctrlShp.attr('%sFlatRx_1' % fngr).value = -2
	ctrlShp.attr('%sFlatRx_2' % fngr).value = -0.2
	ctrlShp.attr('%sFlatRx_3' % fngr).value = 2.5
	ctrlShp.attr('%sFlatRx_4' % fngr).value = 2.5
	ctrlShp.attr('%sCupRx_1' % fngr).value = 0
	ctrlShp.attr('%sCupRx_2' % fngr).value = -3.5
	ctrlShp.attr('%sCupRx_3' % fngr).value = -3.5
	ctrlShp.attr('%sCupRx_4' % fngr).value = -3.5
	ctrlShp.attr('%sSprd_2' % fngr).value = 4.5
	ctrlShp.attr('%sBsSprd_1' % fngr).value = 4.5

	print 'Creating Right Pinky Base Rig'
	rgtPinkyBaseObj = fkRig.FkRig(
									parent=rgtArmObj.Hand_Jnt,
									ctrlGrp=mgObj.Ctrl_Grp,
									tmpJnt=['PinkyBase_R_Jnt', 'PinkyBaseTip_R_Jnt'],
									part='PinkyBase',
									side='R',
									ax='y',
									shape='cube'
									)
	mult = lrr.attrAmper(rgtArmObj.Arm_Ctrl.attr('cup'), rgtPinkyBaseObj.OfstGrps[0].attr('tx'), 0)
	mult.name = 'PinkyBsCupTxAmp_R_Mult'
	mult = lrr.attrAmper(rgtArmObj.Arm_Ctrl.attr('cup'), rgtPinkyBaseObj.OfstGrps[0].attr('tz'), 0)
	mult.name = 'PinkyBsCupTzAmp_R_Mult'

	print 'Creating Right Pinky Rig'
	rgtPinkyObj = fingerRig.FingerRig(
											fngr='Pinky',
											parent=rgtPinkyBaseObj.Jnts[-1],
											armCtrl=rgtArmObj.Arm_Ctrl,
											ctrlGrp=mgObj.Ctrl_Grp,
											tmpJnt=(
														'Pinky_1_R_Jnt',
														'Pinky_2_R_Jnt',
														'Pinky_3_R_Jnt',
														'Pinky_4_R_Jnt',
														'Pinky_5_R_Jnt'
													),
											side='R'
										)
	ctrlShp = lrc.Dag(rgtArmObj.Arm_Ctrl.shape)
	fngr = 'Pinky'
	ctrlShp.attr('%sFlatRx_1' % fngr).value = -2
	ctrlShp.attr('%sFlatRz_1' % fngr).value = 0.8
	ctrlShp.attr('%sFlatRx_2' % fngr).value = -0.5
	ctrlShp.attr('%sFlatRx_3' % fngr).value = 4.3
	ctrlShp.attr('%sFlatRx_4' % fngr).value = 0.65
	ctrlShp.attr('%sCupRx_1' % fngr).value = 0
	ctrlShp.attr('%sCupRx_2' % fngr).value = -4.5
	ctrlShp.attr('%sCupRx_3' % fngr).value = -4.5
	ctrlShp.attr('%sCupRx_4' % fngr).value = -4.5
	ctrlShp.attr('%sSprd_2' % fngr).value = 9
	ctrlShp.attr('%sBsSprd_1' % fngr).value = 9

	print 'Creating Left Leg Rig'
	lftLegObj = legRig.LegRig(
								parent=torsoObj.Pelvis_Jnt,
								ctrlGrp=mgObj.Ctrl_Grp,
								jntGrp=mgObj.Jnt_Grp,
								ikhGrp=mgObj.Ikh_Grp,
								skinGrp=mgObj.Skin_Grp,
								stillGrp=mgObj.Still_Grp,
								ribbon=True,
								tmpJnt=[
											'UpLeg_L_Jnt',
											'LowLeg_L_Jnt',
											'Ankle_L_Jnt',
											'Ball_L_Jnt',
											'Toe_L_Jnt',
											'HeelPiv_L_Jnt',
											'FootInPiv_L_Jnt',
											'FootOutPiv_L_Jnt',
											'KneeIk_L_Jnt'
										],
								part='',
								side='L'
							)

	print 'Creating Right Leg Rig'
	rgtLegObj = legRig.LegRig(
								parent=torsoObj.Pelvis_Jnt,
								ctrlGrp=mgObj.Ctrl_Grp,
								jntGrp=mgObj.Jnt_Grp,
								ikhGrp=mgObj.Ikh_Grp,
								skinGrp=mgObj.Skin_Grp,
								stillGrp=mgObj.Still_Grp,
								ribbon=True,
								tmpJnt=[
											'UpLeg_R_Jnt',
											'LowLeg_R_Jnt',
											'Ankle_R_Jnt',
											'Ball_R_Jnt',
											'Toe_R_Jnt',
											'HeelPiv_R_Jnt',
											'FootInPiv_R_Jnt',
											'FootOutPiv_R_Jnt',
											'KneeIk_R_Jnt'
										],
								part='',
								side='R'
							)

	print 'Creating Left Sleeve Rig'
	lftSlvObj = nonRollRig.NonRollRig(
										ctrlGrp=mgObj.Ctrl_Grp,
										jntGrp=mgObj.Jnt_Grp,
										ikhGrp=mgObj.Ikh_Grp,
										parent=lftClavObj.ClavTip_Jnt,
										rootPar=lftArmObj.UpArm_Jnt,
										tipPar=lftArmObj.Forearm_Jnt,
										rootJnt='Sleeve_L_Jnt',
										tipJnt='SleeveTip_L_Jnt',
										part='Sleeve',
										side='L'
									)
	
	print 'Creating Right Sleeve Rig'
	rgtSlvObj = nonRollRig.NonRollRig(
										ctrlGrp=mgObj.Ctrl_Grp,
										jntGrp=mgObj.Jnt_Grp,
										ikhGrp=mgObj.Ikh_Grp,
										parent=rgtClavObj.ClavTip_Jnt,
										rootPar=rgtArmObj.UpArm_Jnt,
										tipPar=rgtArmObj.Forearm_Jnt,
										rootJnt='Sleeve_R_Jnt',
										tipJnt='SleeveTip_R_Jnt',
										part='Sleeve',
										side='R'
									)

	print 'Creating Eyeglasses Rig'
	egObj = pointRig.PointRig(
								parent=headObj.Head_Jnt,
								ctrlGrp=mgObj.Ctrl_Grp,
								tmpJnt='Eyeglasses_Jnt',
								part='Eg',
								side='',
								shape='cube'
							)

	print 'Creating Headphone Rig'
	hpObj = pointRig.PointRig(
								parent=headObj.Head_Jnt,
								ctrlGrp=mgObj.Ctrl_Grp,
								tmpJnt='Headphone_Jnt',
								part='Hp',
								side='',
								shape='cube'
							)

	print 'Creating Backpack Rig'
	bpObj = pointRig.PointRig(
								parent=torsoObj.Chest_Jnt,
								ctrlGrp=mgObj.Ctrl_Grp,
								tmpJnt='Backpack_Jnt',
								part='Backpack',
								side='',
								shape='cube'
							)

	print 'Creating BlowerTube Rig'
	blowerTubeObj = fkRig.FkRig(
								parent=bpObj.Jnt,
								ctrlGrp=mgObj.Ctrl_Grp,
								tmpJnt=[
											'BlowerTubeA_Jnt',
											'BlowerTubeB_Jnt',
											'BlowerTubeC_Jnt'
										],
								part='BlowerTube',
								side='',
								ax='y',
								shape='circle'
							)

	print 'Creating Blower Rig'
	blowerObj = pointRig.PointRig(
									parent=rootObj.Root_Jnt,
									ctrlGrp=mgObj.Ctrl_Grp,
									tmpJnt='Blower_Jnt',
									part='Blower',
									side='',
									shape='cube'
								)

	print 'Creating BlowerTrigger Rig'
	blowerTriggerObj = pointRig.PointRig(
										parent=blowerObj.Jnt,
										ctrlGrp=mgObj.Ctrl_Grp,
										tmpJnt='BlowerTrigger_Jnt',
										part='BlowerTrigger',
										side='',
										shape='circle'
									)

	print 'Creating Tube Rig'
	tubeLen = lrc.distance('BlowerTubeC_Jnt', 'BlowerA_Jnt')
	tubeRbnObj = lrb.RibbonIkHi(
									size=tubeLen,
									ax='y+',
									part='Tube',
									side=''
								)
	tubeRbnObj.Ctrl_Grp.snapPoint('BlowerTubeC_Jnt')
	mc.delete(
				mc.aimConstraint(
									'BlowerA_Jnt',
									tubeRbnObj.Ctrl_Grp,
									aim=(0, 1, 0), u=(1, 0, 0),
									wut='objectrotation',
									wuo='BlowerTubeC_Jnt',
									wu=(1, 0, 0)
								)
			)
	mc.parentConstraint('BlowerTubeC_Jnt', tubeRbnObj.Ctrl_Grp, mo=True)
	mc.pointConstraint('BlowerTubeC_Jnt', tubeRbnObj.RbnRoot_Ctrl)
	mc.pointConstraint('BlowerA_Jnt', tubeRbnObj.RbnEnd_Ctrl)
	tubeRbnObj.RbnRoot_Ctrl.lockHideAttrs('tx', 'ty', 'rz')
	tubeRbnObj.RbnRoot_Ctrl.hide()
	tubeRbnObj.RbnEnd_Ctrl.lockHideAttrs('tx', 'ty', 'rz')
	tubeRbnObj.RbnEnd_Ctrl.hide()
	
	tubeRbnObj.Ctrl_Grp.parent(mgObj.Ctrl_Grp)
	tubeRbnObj.Jnt_Grp.parent(mgObj.Jnt_Grp)
	tubeRbnObj.Skin_Grp.parent(mgObj.Skin_Grp)
	tubeRbnObj.Still_Grp.parent(mgObj.Still_Grp)
	
	rigData.readCtrlShape()
	mc.delete('TmpJnt_Grp')
	mc.delete('PivJnt_Grp')