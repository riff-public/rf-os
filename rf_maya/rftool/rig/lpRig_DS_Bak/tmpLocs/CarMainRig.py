import os
import sys
sys.path.append(r'X:\Lani Pixels\Libraries\Maya\mayaLpPython')

import maya.cmds as mc

from lpRig import rigTools as lrr
from lpRig import core as lrc

from lpRig import rigData
reload(rigData)

from lpRig import mainGroup
reload(mainGroup)
from lpRig import rootRig
reload(rootRig)
from lpRig import carRig
reload(carRig)
from lpRig import pointRig
reload(pointRig)

def main():

	sn = os.path.normpath(mc.file(q=True, l=True)[0])
	fldPath, fn  = os.path.split(sn)
	assetName = fn.split('_')[0]

	print 'Creating Main Groups'
	mgObj = mainGroup.MainGroup(assetName)

	print 'Creating Root Rig'
	rootObj = rootRig.RootRig(
								ctrlGrp=mgObj.Ctrl_Grp,
								skinGrp=mgObj.Skin_Grp,
								tmpJnt='Root_Jnt',
								part=''
							)

	print 'Front Left Wheel'
	chasObj = carRig.ChassisRig(
									parent=rootObj.Root_Jnt,
									ctrlGrp=mgObj.Ctrl_Grp,
									parentCtrl=rootObj.Root_Ctrl,
									tmpJnt='Chassis_Jnt',
									part='Chassis',
									side=''
								)

	print 'Front Left Wheel'
	flWhObj = carRig.WheelRig(
									parent=rootObj.Root_Jnt,
									ctrlGrp=mgObj.Ctrl_Grp,
									parentCtrl=rootObj.Root_Ctrl,
									chasRig=chasObj,
									tmpJnt=['WheelFr_L_Jnt', 'WheelFrTip_L_Jnt'],
									part='WheelFr',
									side='L'
								)

	print 'Front Right Wheel'
	frWhObj = carRig.WheelRig(
									parent=rootObj.Root_Jnt,
									ctrlGrp=mgObj.Ctrl_Grp,
									parentCtrl=rootObj.Root_Ctrl,
									chasRig=chasObj,
									tmpJnt=['WheelFr_R_Jnt', 'WheelFrTip_R_Jnt'],
									part='WheelFr',
									side='R'
								)

	print 'Back Left Wheel'
	blWhObj = carRig.WheelRig(
									parent=rootObj.Root_Jnt,
									ctrlGrp=mgObj.Ctrl_Grp,
									parentCtrl=rootObj.Root_Ctrl,
									chasRig=chasObj,
									tmpJnt=['WheelBk_L_Jnt', 'WheelBkTip_L_Jnt'],
									part='WheelBk',
									side='L'
								)

	print 'Back Right Wheel'
	brWhObj = carRig.WheelRig(
									parent=rootObj.Root_Jnt,
									ctrlGrp=mgObj.Ctrl_Grp,
									parentCtrl=rootObj.Root_Ctrl,
									chasRig=chasObj,
									tmpJnt=['WheelBk_R_Jnt', 'WheelBkTip_R_Jnt'],
									part='WheelBk',
									side='R'
								)

	print 'Front Left Door'
	flDoorObj = pointRig.PointRig(
									parent=chasObj.Chas_Jnt,
									ctrlGrp=mgObj.Ctrl_Grp,
									tmpJnt='DoorFr_L_Jnt',
									part='DoorFr',
									side='L',
									shape='circle'
								)

	print 'Front Right Door'
	frDoorObj = pointRig.PointRig(
									parent=chasObj.Chas_Jnt,
									ctrlGrp=mgObj.Ctrl_Grp,
									tmpJnt='DoorFr_R_Jnt',
									part='DoorFr',
									side='R',
									shape='circle'
								)

	print 'Front Left Door'
	blDoorObj = pointRig.PointRig(
									parent=chasObj.Chas_Jnt,
									ctrlGrp=mgObj.Ctrl_Grp,
									tmpJnt='DoorBk_L_Jnt',
									part='DoorBk',
									side='L',
									shape='circle'
								)

	print 'Front Right Door'
	bkDoorObj = pointRig.PointRig(
									parent=chasObj.Chas_Jnt,
									ctrlGrp=mgObj.Ctrl_Grp,
									tmpJnt='DoorBk_R_Jnt',
									part='DoorBk',
									side='R',
									shape='circle'
								)

	rigData.readCtrlShape()