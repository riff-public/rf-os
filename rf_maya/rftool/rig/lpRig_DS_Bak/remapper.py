from functools import partial
import re

import maya.cmds as mc
import maya.mel as mm

import lpRig.core as lrc
reload(lrc)
import lpRig.rigTools as lrr
reload(lrr)

def run():

	ui = Remapper()
	ui.show()

	return ui

class Remapper(object):

	def __init__(self):
		
		self.ui = 'pkRemapper'
		self.win = '%sWin' % self.ui

		self.main_cl = '%sMainCl' % self.ui
		self.driver_tfbg = '%sDriverTfbg' % self.ui
		self.driven_tfbg = '%sDrivenTfbg' % self.ui
		self.frames_tfbg = '%sFramesTfbg' % self.ui
		
		self.mirror_rl = '%sMirrorRl' % self.ui
		self.mirror_cb = '%sMirrorCb' % self.ui
		self.inverse_cb = '%sInverseCb' % self.ui
	
	def show(self):

		w_win = 250
		h_win = 250

		cw_tfbg = [1, 50]
		h_tfbg = 35

		cw_cbg = [1, 50]

		curr_driver = ''
		curr_driven = ''
		curr_frames = ''

		if mc.window(self.win, exists=True):

			curr_driver = mc.textFieldButtonGrp(self.driver_tfbg, q=True, tx=True)
			curr_driven = mc.textFieldButtonGrp(self.driven_tfbg, q=True, tx=True)
			curr_frames = mc.textFieldButtonGrp(self.frames_tfbg, q=True, tx=True)

			mc.deleteUI(self.win)

		mc.window(self.win, t=self.ui, rtf=True)

		mc.columnLayout(self.main_cl, adj=True)

		mc.textFieldButtonGrp(self.driver_tfbg, l='Driver', tx=curr_driver, 
								bc=partial(self.get_selected, self.driver_tfbg), 
								cw=cw_tfbg, h=h_tfbg, bl='Get', ed=True)

		mc.separator()
		mc.textFieldButtonGrp(self.driven_tfbg, l='Driven', tx=curr_driven, 
								bc=partial(self.get_selected, self.driven_tfbg), 
								cw=cw_tfbg, h=h_tfbg, bl='Get', ed=True)
		mc.separator()
		mc.textFieldButtonGrp(self.frames_tfbg, l='Frames', tx=curr_frames, 
								bc=partial(self.get_selected_frame), 
								cw=cw_tfbg, h=h_tfbg, bl='Get', ed=True)

		mc.separator()
		mc.rowLayout(self.mirror_rl, adj=True, nc=2)

		mc.checkBox(self.mirror_cb, label='Mirror', 
						cc=partial(self.mirror_status_changed))
		mc.checkBox(self.inverse_cb, label='Inverse Outputs', ed=False)

		mc.setParent('..')

		mc.separator()
		mc.button(l='Remap', c=partial(self.remap), h=35)


		mc.showWindow(self.win)
		mc.window(self.win, e=True, w=w_win)
		mc.window(self.win, e=True, h=h_win)

	def mirror_status_changed(self, *args):

		val = mc.checkBox(self.mirror_cb, q=True, v=True)
		mc.checkBox(self.inverse_cb, e=True, ed=val)

	def get_selected_frame(self, *args):

		driver = mc.textFieldButtonGrp(self.driver_tfbg, q=True, tx=True)

		driver_node, driver_attr = driver.split('.')

		pb = mm.eval('$tmpVar=$gPlayBackSlider')
		range_str = mc.timeControl(pb, q=True, rng=True)

		frames = [int(x.replace('"', '')) for x in range_str.split(':')]

		keyframes = mc.keyframe(driver_node, q=True, time=(frames[0], frames[1]),
									timeChange=True)

		frames_str = ''
		for keyframe in keyframes:
			frames_str += '%s, ' % str(int(keyframe))

		mc.textFieldButtonGrp(self.frames_tfbg, e=True, tx=frames_str)

	def get_selected(self, tfbg=None, *args):

		node = mc.ls(sl=True)[0]
		attr = mc.channelBox('mainChannelBox', q=True, sma=True)[0]

		mc.textFieldButtonGrp(tfbg, e=True, tx='%s.%s' % (node, attr))

	def remap(self, *args):

		mirror = mc.checkBox(self.mirror_cb, q=True, v=True)
		inverse = mc.checkBox(self.inverse_cb, q=True, v=True)
		driver = mc.textFieldButtonGrp(self.driver_tfbg, q=True, tx=True)
		driven = mc.textFieldButtonGrp(self.driven_tfbg, q=True, tx=True)
		frames_str = mc.textFieldButtonGrp(self.frames_tfbg, q=True, tx=True)

		frames = re.findall(r'\d+', frames_str)

		in_vals = []
		out_vals = []
		for frame in frames:

			in_val = mc.getAttr(driver, t=int(frame))
			out_val = mc.getAttr(driven, t=int(frame))

			in_vals.append(in_val)
			out_vals.append(out_val)

		lrr.remapDriven(driver=driver, driven=driven, 
							inVals=in_vals, outVals=out_vals)

		if mirror:
			
			if '_L_' in driver:
				driver = driver.replace('_L_', '_R_')
				driven = driven.replace('_L_', '_R_')
			
			elif '_R_' in driver:
				driver = driver.replace('_R_', '_L_')
				driven = driven.replace('_R_', '_L_')
			
			if inverse:
				out_vals = [-x for x in out_vals]
			
			lrr.remapDriven(driver=driver, driven=driven, 
								inVals=in_vals, outVals=out_vals)