import maya.cmds as mc

import lpRig.core as lrc
reload(lrc)
import lpRig.rigTools as lrr
reload(lrr)

class NoseRig(object):

	def __init__(
					self,
					nsBdgTmpLoc='',
					nsTmpLoc='',
					nsTipTmpLoc='',
					part=''
				):

		# Main Groups
		self.Ctrl_Grp = lrc.Null()
		self.Ctrl_Grp.name = 'NsRig%sCtrl_Grp' % part

		self.Skin_Grp = lrc.Null()
		self.Skin_Grp.name = 'NsRig%sSkin_Grp' % part

		# Joints
		self.Bdg_Jnt, self.BdgJntZr_Grp = self.createJoint(nsBdgTmpLoc, 'BdgNsRig{}'.format(part), '_')
		self.BdgJntZr_Grp.parent(self.Skin_Grp)

		self.Ns_Jnt, self.NsJntZr_Grp = self.createJoint(nsTmpLoc, 'NsRig{}'.format(part), '_')
		self.NsJntZr_Grp.parent(self.Bdg_Jnt)
		
		self.Tip_Jnt, self.TipJntZr_Grp = self.createJoint(nsTipTmpLoc, 'TipNsRig{}'.format(part), '_')
		self.TipJntZr_Grp.parent(self.Ns_Jnt)

		# self.Nst_L_Jnt, self.NstJntZr_L_Grp = self.createJoint(lftNstTmpLoc, 'NstNsRig{}'.format(part), '_L_')
		# self.NstJntZr_L_Grp.parent(self.Ns_Jnt)

		# self.Nst_R_Jnt, self.NstJntZr_R_Grp = self.createJoint(rgtNstTmpLoc, 'NstNsRig{}'.format(part), '_R_')
		# self.NstJntZr_R_Grp.parent(self.Ns_Jnt)

		# Controls
		self.Bdg_Ctrl, self.BdgCtrlZr_Grp = self.createControl(self.Bdg_Jnt, 'BdgNsRig{}'.format(part), '_')
		self.BdgCtrlZr_Grp.parent(self.Ctrl_Grp)
		
		self.Ns_Ctrl, self.NsCtrlZr_Grp = self.createControl(self.Ns_Jnt, 'NsNsRig{}'.format(part), '_')
		self.NsCtrlZr_Grp.parent(self.Bdg_Ctrl)
		
		self.Tip_Ctrl, self.TipCtrlZr_Grp = self.createControl(self.Tip_Jnt, 'TipNsRig{}'.format(part), '_')
		self.TipCtrlZr_Grp.parent(self.Ns_Ctrl)
		
		# self.Nst_L_Ctrl, self.NstCtrlZr_L_Grp = self.createControl(self.Nst_L_Jnt, 'NstNsRig{}'.format(part), '_L_')
		# self.NstCtrlZr_L_Grp.parent(self.Ns_Ctrl)

		# self.Nst_R_Ctrl, self.NstCtrlZr_R_Grp = self.createControl(self.Nst_R_Jnt, 'NstNsRig{}'.format(part), '_R_')
		# self.NstCtrlZr_R_Grp.parent(self.Ns_Ctrl)

	def createJoint(self, loc='', part='', side=''):

		jnt = lrc.Joint()
		jnt.name = '{part}{side}Jnt'.format(part=part, side=side)

		zrGrp = lrc.group(jnt)
		zrGrp.name = '{part}JntZr{side}Grp'.format(part=part, side=side)

		zrGrp.snap(loc)

		return jnt, zrGrp

	def createControl(self, jnt='', part='', side=''):

		col = 'yellow'

		if side == '_L_':
			col = 'red'
		elif side == '_R_':
			col = 'blue'

		jnt = lrc.Dag(jnt)

		ctrl = lrc.Control('nrbCircle')
		ctrl.name = '{part}{side}Ctrl'.format(part=part, side=side)
		ctrl.lockHideAttrs('v')
		ctrl.color = col

		zrGrp = lrc.group(ctrl)
		zrGrp.name = '{part}CtrlZr{side}Grp'.format(part=part, side=side)

		zrGrp.snap(jnt)

		ctrl.attr('t') >> jnt.attr('t')
		ctrl.attr('r') >> jnt.attr('r')
		ctrl.attr('s') >> jnt.attr('s')

		return ctrl, zrGrp

def main():

	nsRig = NoseRig(
						nsBdgTmpLoc='NsBdgTmp_Loc',
						nsTmpLoc='NsTmp_Loc',
						nsTipTmpLoc='NsTipTmp_Loc',
						part=''
					)