import maya.cmds as mc

from lpRig import core as lrc
reload(lrc)
from lpRig import rigTools as lrr
reload(lrr)

class FkRig(object):

	def __init__(
					self,
					parent='',
					ctrlGrp='',
					tmpJnt=[],
					part='',
					side='',
					ax='y',
					shape='circle'
				):

		if side:
			side = '_%s_' % side
		else:
			side = '_'

		# Main group
		self.Ctrl_Grp = lrc.Null()
		self.Ctrl_Grp.name = '%sCtrl%sGrp' % (part, side)

		mc.parentConstraint(parent, self.Ctrl_Grp)
		mc.scaleConstraint(parent, self.Ctrl_Grp)
		
		self.Jnts = tmpJnt
		mc.setAttr('%s.ssc' % self.Jnts[0], 0)
		
		self.Ctrls = []
		self.Gmbls = []
		self.OfstGrps = []
		self.OriGrps = []
		self.ZrGrps = []
		self.Adds = []
		self.Muls = []
		self.SqsAdds = []
		
		for ix in range(0, (len(tmpJnt) - 1)) :
						
			# Create controllers
			ctrl = lrr.jointControl(shape)
			gmbl = lrc.addGimbal(ctrl)
			ofst = lrc.group(ctrl)
			ori = lrc.group(ofst)
			zrGrp = lrc.group(ori)
			
			ctrl.name = '%s_%d%sCtrl' % (part, (ix+1), side)
			gmbl.name = '%sGmbl_%d%sCtrl' % (part, (ix+1), side)
			ofst.name = '%sCtrlOfst_%d%sGrp' % (part, (ix+1), side)
			ori.name = '%sCtrlOri_%d%sGrp' % (part, (ix+1), side)
			zrGrp.name = '%sCtrlZr_%d%sGrp' % (part, (ix+1), side)
			
			ctrl.lockHideAttrs('sx', 'sy', 'sz', 'v')
			ofst.lockHideAttrs('sx', 'sy', 'sz', 'v')
			
			self.Ctrls.append(ctrl)
			self.Gmbls.append(gmbl)
			self.OfstGrps.append(ofst)
			self.OriGrps.append(ori)
			self.ZrGrps.append(zrGrp)
			
			ctrl.color = 'yellow'
			if side == '_R_':
				ctrl.color = 'blue'
			elif side == '_L_':
				ctrl.color = 'red'
			
			ctrl.rotateOrder = 'yzx'
			gmbl.rotateOrder = 'yzx'
			
			# zrGrp.snapPoint(tmpJnt[ix])
			# ctrl.snap(tmpJnt[ix])
			# ctrl.freeze(r=True)

			zrGrp.snap(tmpJnt[ix])
	
			if not ix == 0 :
				self.ZrGrps[ix].parent(self.Gmbls[ix - 1])
			else :
				self.ZrGrps[ix].parent(self.Ctrl_Grp)

				# Ori loc/wor
				(locGrp,
				worGrp,
				worGrpParCons,
				oriGrpParCons,
				oriGrpParConsRev) = lrr.orientLocalWorldCtrl(
																ctrl,
																zrGrp,
																ctrlGrp,
																ori
															)

				locGrp.name = '%sCtrlLoc_%d%sgrp' % (part, (ix+1), side)
				worGrp.name = '%sCtrlWor_%d%sgrp' % (part, (ix+1), side)
				oriGrpParConsRev.name = '%sCtrlLocWorOri_%d%srev' % (part, (ix+1), side)
			
			# Connect to joint
			mc.parentConstraint(gmbl, tmpJnt[ix])
		
		# Squash and stretch
		for ix in range(0, (len(tmpJnt) - 1)) :
			
			# Stretch
			if ix < (len(tmpJnt) - 2) :
				# Connect to next zero group
				currAdd, currMul = lrr.fkStretch(
													ctrl=self.Ctrls[ix],
													target=self.ZrGrps[ix + 1],
													ax=ax
												)
			
			else :
				# Connect to the last joint
				lastJnt = tmpJnt[ix + 1]
				currAdd, currMul = lrr.fkStretch(
													ctrl=self.Ctrls[ix],
													target=lastJnt,
													ax=ax
												)
			
			currAdd.name = '%sStrt_%d%sAdd' % (part, (ix+1), side)
			currMul.name = '%sStrt_%d%sMul' % (part, (ix+1), side)
			self.Adds.append(currAdd)
			self.Muls.append(currMul)
			
			# Squash
			currJnt = lrc.Dag(tmpJnt[ix])
			self.Ctrls[ix].add(ln='squash', k=True)
			
			sqAdd = lrc.AddDoubleLinear()
			sqAdd.name = '%sSqs_%d%sAdd' % (part, (ix+1), side)
			sqAdd.add(ln='default', dv=1, min=1, max=1, k=True)
			
			sqAdd.attr('default') >> sqAdd.attr('i1')
			self.Ctrls[ix].attr('squash') >> sqAdd.attr('i2')
			
			for eachAx in ('x', 'y', 'z') :
				if not ax == eachAx :
					sqAdd.attr('o') >> currJnt.attr('s%s' % eachAx)

			# Tip Squash
			if ix == (len(tmpJnt) - 2) :
				
				tipJnt = lrc.Dag(tmpJnt[ix+1])
				self.Ctrls[ix].add(ln='tipSquash', k=True)
				
				tipSqAdd = lrc.AddDoubleLinear()
				tipSqAdd.name = '%sTipSqs%sAdd' % (part, side)
				tipSqAdd.add(ln='default', dv=1, min=1, max=1, k=True)
				
				tipSqAdd.attr('default') >> tipSqAdd.attr('i1')
				self.Ctrls[ix].attr('tipSquash') >> tipSqAdd.attr('i2')
				
				for eachAx in ('x', 'y', 'z') :
					if not ax == eachAx :
						tipSqAdd.attr('o') >> tipJnt.attr('s%s' % eachAx)
				
				tipSqAmp = lrr.attrAmper(self.Ctrls[ix].attr('tipSquash'), tipSqAdd.attr('i2'), 0.1)
				tipSqAmp.name = '%sTipSqsAmp%sMul' % (part, side)
		
		self.Ctrl_Grp.parent(ctrlGrp)