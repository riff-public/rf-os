import re

import maya.cmds as mc

from lpRig import core as lrc
reload(lrc)

def callback():
    
    sels = mc.ls(sl=True)
    sel = lrc.Dag(sels[0])
    side = ''
    part = ''
    
    if '_L_' in sel.name:
        side = 'L'
    elif '_R_' in sel.name:
        side = 'R'

    ns = ''
    if ':' in sel.name:
        ns = ':'.join(sel.name.split(':')[0:-1])
        ns = '%s:' % ns

    if sel.attr('localWorld').exists:

        localWorld(sel)

    elif 'Spine' in sel.name:

        if sel.attr('fkIk').v:
            spineIkToFk(ns)
        else:
            spineFkToIk(ns)

    elif 'Leg' in sel.name:

        exp = r'.+(Leg)(.+)_([L|R])_(Ctrl)'
        matchObj = re.search(exp, sel.name)

        if matchObj:
            part = matchObj.group(2)

        midLegNm = '%sMidLegFk%s_L_Ctrl' % (ns, part)
        
        if mc.objExists(midLegNm):
            if sel.attr('fkIk').v:
                animalLegIkToFk(ns, part, side)
            else:
                animalLegFkToIk(ns, part, side)
        else:
            if sel.attr('fkIk').v:
                legIkToFk(ns, part, side)
            else:
                legFkToIk(ns, part, side)

    elif 'Arm' in sel.name:

        exp = r'.+(:Arm)(.+)_([L|R])_(Ctrl)'
        matchObj = re.search(exp, sel.name)

        if matchObj:
            part = matchObj.group(2)

        if sel.attr('fkIk').v:
            armIkToFk(ns, part, side)
        else:
            armFkToIk(ns, part, side)


    mc.select(sels, r=True)

def localWorld(ctrl=''):
    
    # Control
    ctrl = lrc.Dag(ctrl)
    grp = lrc.Dag(ctrl.getParent())
    
    # Template control
    prxy = lrc.Joint()
    tmp = lrc.Joint()
    tmpGrp = lrc.group(tmp)
    
    # Rotate order adjustment
    prxy.attr('rotateOrder').value = ctrl.attr('rotateOrder').value
    tmp.attr('rotateOrder').value = ctrl.attr('rotateOrder').value
    
    # Joint orient adjustment
    if ctrl.type == 'joint':
        prxy.attr('jointOrient').value = ctrl.attr('jointOrient').value
        tmp.attr('jointOrient').value = ctrl.attr('jointOrient').value
    
    # Snaping controls
    prxy.snap(ctrl)
    tmpGrp.snap(ctrl)
    
    tmpCons = lrc.parentConstraint(prxy, tmp)
    
    if ctrl.attr('localWorld').value:
        ctrl.attr('localWorld').value = 0
    else:
        ctrl.attr('localWorld').value = 1

    tmpGrp.snap(grp)
    
    attrs = ('tx','ty','tz','rx','ry','rz')
    
    for attr in attrs:
        if not ctrl.attr(attr).lock:
            ctrl.attr(attr).value = tmp.attr(attr).value
    
    # Cleanup
    mc.delete(tmpGrp)
    mc.delete(prxy)

def spineFkToIk(ns='', part=''):

    # Spine control
    spi = lrc.Dag(mc.ls('%sSpine%s_Ctrl' % (ns, part))[0])
    
    # FK controls
    spi1FkCtrl = lrc.Dag(mc.ls('%sSpine%sFk_1_Ctrl' % (ns, part))[0])
    spi2FkCtrl = lrc.Dag(mc.ls('%sSpine%sFk_2_Ctrl' % (ns, part))[0])
    
    # IK controls
    spiIkRootCtrl = lrc.Dag(mc.ls('%sSpine%sIkRoot_Ctrl' % (ns, part))[0])
    spiIkCtrl = lrc.Dag(mc.ls('%sSpine%sIk_Ctrl' % (ns, part))[0])
    
    # Fk joints
    spi1Fk = lrc.Dag(mc.ls('%sSpine%sFk_1_Jnt' % (ns, part))[0])
    spi2Fk = lrc.Dag(mc.ls('%sSpine%sFk_2_Jnt' % (ns, part))[0])
    spi3Fk = lrc.Dag(mc.ls('%sSpine%sFk_3_Jnt' % (ns, part))[0])
    
    # Ik joints
    spi1Ik = lrc.Dag(mc.ls('%sSpine%sIk_1_Jnt' % (ns, part))[0])
    spi2Ik = lrc.Dag(mc.ls('%sSpine%sIk_2_Jnt' % (ns, part))[0])
    spi3Ik = lrc.Dag(mc.ls('%sSpine%sIk_3_Jnt' % (ns, part))[0])
    
    # Template objects
    spi1 = lrc.Joint()
    spi1Grp = lrc.group(spi1)
    spi2 = lrc.Joint()
    spi2Grp = lrc.group(spi2)
    
    # Rotate order adjustment
    spi1.attr('rotateOrder').value = spiIkRootCtrl.attr('rotateOrder').value
    spi2.attr('rotateOrder').value = spiIkCtrl.attr('rotateOrder').value
    
    # Joint orient adjustment
    spi1.attr('jointOrient').value = spiIkRootCtrl.attr('jointOrient').value
    spi2.attr('jointOrient').value = spiIkCtrl.attr('jointOrient').value
    
    # Snaping controls
    spi1Grp.snap(spiIkRootCtrl.getParent())
    spi1.snap(spi1Fk)
    spiIkRootCtrl.attr('t').value = spi1.attr('t').value
    
    spi2Grp.snap(spiIkCtrl.getParent())
    spi2.snap(spi2Fk)
    spiIkCtrl.attr('t').value = spi2.attr('t').value
    spiIkCtrl.attr('r').value = spi2.attr('r').value
    
    # Adjusting stretch
    spiIkCtrl.attr('autoStretch').value = 1
    spiIkCtrl.attr('lowerStretch').value = spi1FkCtrl.attr('stretch').value
    spiIkCtrl.attr('upperStretch').value = spi2FkCtrl.attr('stretch').value
    
    spi.attr('fkIk').value = 1
    
    # Cleanup
    mc.delete(spi1Grp)
    mc.delete(spi2Grp)

def spineIkToFk(ns='', part=''):

    # Spine control
    spi = lrc.Dag(mc.ls('%sSpine%s_Ctrl' % (ns, part))[0])
    
    # FK controls
    spi1FkCtrl = lrc.Dag(mc.ls('%sSpine%sFk_1_Ctrl' % (ns, part))[0])
    spi2FkCtrl = lrc.Dag(mc.ls('%sSpine%sFk_2_Ctrl' % (ns, part))[0])
    
    # IK controls
    spiIkRootCtrl = lrc.Dag(mc.ls('%sSpine%sIkRoot_Ctrl' % (ns, part))[0])
    spiIkCtrl = lrc.Dag(mc.ls('%sSpine%sIk_Ctrl' % (ns, part))[0])
    
    # Fk joints
    spi1Fk = lrc.Dag(mc.ls('%sSpine%sFk_1_Jnt' % (ns, part))[0])
    spi2Fk = lrc.Dag(mc.ls('%sSpine%sFk_2_Jnt' % (ns, part))[0])
    spi3Fk = lrc.Dag(mc.ls('%sSpine%sFk_3_Jnt' % (ns, part))[0])
    
    # Ik joints
    spi1Ik = lrc.Dag(mc.ls('%sSpine%sIk_1_Jnt' % (ns, part))[0])
    spi2Ik = lrc.Dag(mc.ls('%sSpine%sIk_2_Jnt' % (ns, part))[0])
    spi3Ik = lrc.Dag(mc.ls('%sSpine%sIk_3_Jnt' % (ns, part))[0])
    
    # Template objects
    spi1 = lrc.Joint()
    spi1Grp = lrc.group(spi1)
    spi2 = lrc.Joint()
    spi2Grp = lrc.group(spi2)
    
    # Rotate order adjustment
    spi1.attr('rotateOrder').value = spi1FkCtrl.attr('rotateOrder').value
    spi2.attr('rotateOrder').value = spi2FkCtrl.attr('rotateOrder').value
    
    # Joint orient adjustment
    spi1.attr('jointOrient').value = spi1FkCtrl.attr('jointOrient').value
    spi2.attr('jointOrient').value = spi2FkCtrl.attr('jointOrient').value
    
    # Snaping controls
    spi1Grp.snap(spi1FkCtrl.getParent())
    spi1.snap(spi1Ik)
    spi1FkCtrl.attr('t').value = spi1.attr('t').value
    spi1FkCtrl.attr('r').value = spi1.attr('r').value
    
    spi1FkCtrl.attr('stretch').value = 0
    # spi1FkCtrl.attr('stretch').value = ((spi2Ik.attr('ty').value/spi2Fk.attr('ty').value) - 1) * 10
    
    spi2Grp.snap(spi2FkCtrl.getParent())
    spi2.snap(spi2Ik)
    spi2FkCtrl.attr('t').value = spi2.attr('t').value
    spi2FkCtrl.attr('r').value = spi2.attr('r').value
    
    spi2FkCtrl.attr('stretch').value = spiIkCtrl.attr('upperStretch').value
    
    spi.attr('fkIk').value = 0
    
    # Cleanup
    mc.delete(spi1Grp)
    mc.delete(spi2Grp)

def animalLegFkToIk(ns='', part='', side=''):

    if side:
        side = '_%s_' % side
    else:
        side = '_'

    # Leg controller
    leg = lrc.Dag(mc.ls('%sLeg%s%sCtrl' % (ns, part, side))[0])
    
    # FK controls
    upLegFkCtrl = lrc.Dag(mc.ls('%sUpLegFk%s%sCtrl' % (ns, part, side))[0])
    midLegFkCtrl = lrc.Dag(mc.ls('%sMidLegFk%s%sCtrl' % (ns, part, side))[0])
    lowLegFkCtrl = lrc.Dag(mc.ls('%sLowLegFk%s%sCtrl' % (ns, part, side))[0])
    ankleFkCtrl = lrc.Dag(mc.ls('%sAnkleFk%s%sCtrl' % (ns, part, side))[0])
    toeFkCtrl = lrc.Dag(mc.ls('%sToeFk%s%sCtrl' % (ns, part, side))[0])
    
    # IK controls
    legIkRootCtrl = lrc.Dag(mc.ls('%sLegIkRoot%s%sCtrl' % (ns, part, side))[0])
    legIkCtrl = lrc.Dag(mc.ls('%sLegIk%s%sCtrl' % (ns, part, side))[0])
    kneeIkCtrl = lrc.Dag(mc.ls('%sKneeIk%s%sCtrl' % (ns, part, side))[0])
    
    # Fk joints
    upLegFk = lrc.Dag(mc.ls('%sUpLegFk%s%sJnt' % (ns, part, side))[0])
    midLegFk = lrc.Dag(mc.ls('%sMidLegFk%s%sJnt' % (ns, part, side))[0])
    lowLegFk = lrc.Dag(mc.ls('%sLowLegFk%s%sJnt' % (ns, part, side))[0])
    ankleFk = lrc.Dag(mc.ls('%sAnkleFk%s%sJnt' % (ns, part, side))[0])
    ballFk = lrc.Dag(mc.ls('%sBallFk%s%sJnt' % (ns, part, side))[0])
    toeFk = lrc.Dag(mc.ls('%sToeFk%s%sJnt' % (ns, part, side))[0])
    
    # Ik joints
    upLegIk = lrc.Dag(mc.ls('%sUpLegIk%s%sJnt' % (ns, part, side))[0])
    midLegIk = lrc.Dag(mc.ls('%sMidLegIk%s%sJnt' % (ns, part, side))[0])
    lowLegIk = lrc.Dag(mc.ls('%sLowLegIk%s%sJnt' % (ns, part, side))[0])
    ankleIk = lrc.Dag(mc.ls('%sAnkleIk%s%sJnt' % (ns, part, side))[0])
    ballIk = lrc.Dag(mc.ls('%sBallIk%s%sJnt' % (ns, part, side))[0])
    toeIk = lrc.Dag(mc.ls('%sToeIk%s%sJnt' % (ns, part, side))[0])
    
    # Template objects
    upLeg = lrc.Joint()
    upLegGrp = lrc.group(upLeg)
    midLeg = lrc.Joint()
    midLegGrp = lrc.group(midLeg)
    lowLeg = lrc.Joint()
    lowLegGrp = lrc.group(lowLeg)
    ankle = lrc.Joint()
    ankleGrp = lrc.group(ankle)
    
    # Rotate order adjustment
    upLeg.attr('rotateOrder').value = legIkRootCtrl.attr('rotateOrder').value
    ankle.attr('rotateOrder').value = legIkCtrl.attr('rotateOrder').value
    
    # Joint orient adjustment
    upLeg.attr('jointOrient').value = legIkRootCtrl.attr('jointOrient').value
    ankle.attr('jointOrient').value = legIkCtrl.attr('jointOrient').value
    
    # Snaping controls
    upLegGrp.snap(legIkRootCtrl.getParent())
    upLeg.snap(upLegFk)
    legIkRootCtrl.attr('t').value = upLeg.attr('t').value
    
    ankleGrp.snap(legIkCtrl.getParent())
    ankle.snap(ankleFk)
    legIkCtrl.attr('t').value = ankle.attr('t').value
    legIkCtrl.attr('r').value = ankle.attr('r').value
    
    lowLegGrp.snap(kneeIkCtrl.getParent())
    lowLeg.snap(lowLegFk)
    kneeIkCtrl.attr('t').value = lowLeg.attr('t').value
    
    # Adjusting stretch
    legIkCtrl.attr('autoStretch').value = 0
    legIkCtrl.attr('upLegStretch').value = upLegFkCtrl.attr('stretch').value
    legIkCtrl.attr('midLegStretch').value = midLegFkCtrl.attr('stretch').value
    legIkCtrl.attr('lowLegStretch').value = lowLegFkCtrl.attr('stretch').value
    
    leg.attr('fkIk').value = 1
    
    # Cleanup
    mc.delete(upLegGrp)
    mc.delete(midLegGrp)
    mc.delete(lowLegGrp)
    mc.delete(ankleGrp)

def animalLegIkToFk(ns='', part='', side=''):

    if side:
        side = '_%s_' % side
    else:
        side = '_'
    
    # Leg controller
    leg = lrc.Dag(mc.ls('%sLeg%s%sCtrl' % (ns, part, side))[0])
    
    # FK controls
    upLegFkCtrl = lrc.Dag(mc.ls('%sUpLegFk%s%sCtrl' % (ns, part, side))[0])
    midLegFkCtrl = lrc.Dag(mc.ls('%sMidLegFk%s%sCtrl' % (ns, part, side))[0])
    lowLegFkCtrl = lrc.Dag(mc.ls('%sLowLegFk%s%sCtrl' % (ns, part, side))[0])
    ankleFkCtrl = lrc.Dag(mc.ls('%sAnkleFk%s%sCtrl' % (ns, part, side))[0])
    toeFkCtrl = lrc.Dag(mc.ls('%sToeFk%s%sCtrl' % (ns, part, side))[0])
    
    # IK controls
    legIkRootCtrl = lrc.Dag(mc.ls('%sLegIkRoot%s%sCtrl' % (ns, part, side))[0])
    legIkCtrl = lrc.Dag(mc.ls('%sLegIk%s%sCtrl' % (ns, part, side))[0])
    kneeIkCtrl = lrc.Dag(mc.ls('%sKneeIk%s%sCtrl' % (ns, part, side))[0])
    
    # Fk joints
    upLegFk = lrc.Dag(mc.ls('%sUpLegFk%s%sJnt' % (ns, part, side))[0])
    midLegFk = lrc.Dag(mc.ls('%sMidLegFk%s%sJnt' % (ns, part, side))[0])
    lowLegFk = lrc.Dag(mc.ls('%sLowLegFk%s%sJnt' % (ns, part, side))[0])
    ankleFk = lrc.Dag(mc.ls('%sAnkleFk%s%sJnt' % (ns, part, side))[0])
    ballFk = lrc.Dag(mc.ls('%sBallFk%s%sJnt' % (ns, part, side))[0])
    toeFk = lrc.Dag(mc.ls('%sToeFk%s%sJnt' % (ns, part, side))[0])
    
    # Ik joints
    upLegIk = lrc.Dag(mc.ls('%sUpLegIk%s%sJnt' % (ns, part, side))[0])
    midLegIk = lrc.Dag(mc.ls('%sMidLegIk%s%sJnt' % (ns, part, side))[0])
    lowLegIk = lrc.Dag(mc.ls('%sLowLegIk%s%sJnt' % (ns, part, side))[0])
    ankleIk = lrc.Dag(mc.ls('%sAnkleIk%s%sJnt' % (ns, part, side))[0])
    ballIk = lrc.Dag(mc.ls('%sBallIk%s%sJnt' % (ns, part, side))[0])
    toeIk = lrc.Dag(mc.ls('%sToeIk%s%sJnt' % (ns, part, side))[0])
    
    # Template objects
    upLeg = lrc.Joint()
    upLegGrp = lrc.group(upLeg)
    midLeg = lrc.Joint()
    midLegGrp = lrc.group(midLeg)
    lowLeg = lrc.Joint()
    lowLegGrp = lrc.group(lowLeg)
    ankle = lrc.Joint()
    ankleGrp = lrc.group(ankle)
    
    # Rotate order adjustment
    upLeg.attr('rotateOrder').value = upLegFkCtrl.attr('rotateOrder').value
    midLeg.attr('rotateOrder').value = midLegFkCtrl.attr('rotateOrder').value
    lowLeg.attr('rotateOrder').value = lowLegFkCtrl.attr('rotateOrder').value
    ankle.attr('rotateOrder').value = ankleFkCtrl.attr('rotateOrder').value
    
    # Joint orient adjustment
    upLeg.attr('jointOrient').value = upLegFkCtrl.attr('jointOrient').value
    midLeg.attr('jointOrient').value = midLegFkCtrl.attr('jointOrient').value
    lowLeg.attr('jointOrient').value = lowLegFkCtrl.attr('jointOrient').value
    ankle.attr('jointOrient').value = ankleFkCtrl.attr('jointOrient').value
    
    # Snaping controls
    upLegGrp.snap(upLegFkCtrl.getParent())
    upLeg.snap(upLegIk)
    upLegFkCtrl.attr('t').value = upLeg.attr('t').value
    upLegFkCtrl.attr('r').value = upLeg.attr('r').value
    
    upLegFkCtrl.attr('stretch').value = 0
    upLegFkCtrl.attr('stretch').value = ((midLegIk.attr('ty').value/midLegFk.attr('ty').value) - 1) * 10

    midLegGrp.snap(midLegFkCtrl.getParent())
    midLeg.snap(midLegIk)
    midLegFkCtrl.attr('r').value = midLeg.attr('r').value

    midLegFkCtrl.attr('stretch').value = 0
    midLegFkCtrl.attr('stretch').value = ((lowLegIk.attr('ty').value/lowLegFk.attr('ty').value) - 1) * 10
    
    lowLegGrp.snap(lowLegFkCtrl.getParent())
    lowLeg.snap(lowLegIk)
    lowLegFkCtrl.attr('r').value = lowLeg.attr('r').value
    
    lowLegFkCtrl.attr('stretch').value = 0
    lowLegFkCtrl.attr('stretch').value = ((ankleIk.attr('ty').value/ankleFk.attr('ty').value) - 1) * 10
    
    ankleGrp.snap(ankleFkCtrl.getParent())
    ankle.snap(ankleIk)
    ankleFkCtrl.attr('r').value = ankle.attr('r').value
    
    toeFkCtrl.attr('r').value = (0,0,0)
    toeFkCtrl.attr('rx').value = -legIkCtrl.attr('toeBend').value
    
    leg.attr('fkIk').value = 0
    
    # Cleanup
    mc.delete(upLegGrp)
    mc.delete(midLegGrp)
    mc.delete(lowLegGrp)
    mc.delete(ankleGrp)

def legFkToIk(ns='', part='', side=''):

    if side:
        side = '_%s_' % side
    else:
        side = '_'
    
    # Leg controller
    leg = lrc.Dag(mc.ls('%sLeg%s%sCtrl' % (ns, part, side))[0])
    
    # FK controls
    upLegFkCtrl = lrc.Dag(mc.ls('%sUpLegFk%s%sCtrl' % (ns, part, side))[0])
    lowLegFkCtrl = lrc.Dag(mc.ls('%sLowLegFk%s%sCtrl' % (ns, part, side))[0])
    ankleFkCtrl = lrc.Dag(mc.ls('%sAnkleFk%s%sCtrl' % (ns, part, side))[0])
    toeFkCtrl = lrc.Dag(mc.ls('%sToeFk%s%sCtrl' % (ns, part, side))[0])
    
    # IK controls
    legIkRootCtrl = lrc.Dag(mc.ls('%sLegIkRoot%s%sCtrl' % (ns, part, side))[0])
    legIkCtrl = lrc.Dag(mc.ls('%sLegIk%s%sCtrl' % (ns, part, side))[0])
    kneeIkCtrl = lrc.Dag(mc.ls('%sKneeIk%s%sCtrl' % (ns, part, side))[0])
    
    # Fk joints
    upLegFk = lrc.Dag(mc.ls('%sUpLegFk%s%sJnt' % (ns, part, side))[0])
    lowLegFk = lrc.Dag(mc.ls('%sLowLegFk%s%sJnt' % (ns, part, side))[0])
    ankleFk = lrc.Dag(mc.ls('%sAnkleFk%s%sJnt' % (ns, part, side))[0])
    ballFk = lrc.Dag(mc.ls('%sBallFk%s%sJnt' % (ns, part, side))[0])
    toeFk = lrc.Dag(mc.ls('%sToeFk%s%sJnt' % (ns, part, side))[0])
    
    # Ik joints
    upLegIk = lrc.Dag(mc.ls('%sUpLegIk%s%sJnt' % (ns, part, side))[0])
    lowLegIk = lrc.Dag(mc.ls('%sLowLegIk%s%sJnt' % (ns, part, side))[0])
    ankleIk = lrc.Dag(mc.ls('%sAnkleIk%s%sJnt' % (ns, part, side))[0])
    ballIk = lrc.Dag(mc.ls('%sBallIk%s%sJnt' % (ns, part, side))[0])
    toeIk = lrc.Dag(mc.ls('%sToeIk%s%sJnt' % (ns, part, side))[0])
    
    # Template objects
    upLeg = lrc.Joint()
    upLegGrp = lrc.group(upLeg)
    lowLeg = lrc.Joint()
    lowLegGrp = lrc.group(lowLeg)
    ankle = lrc.Joint()
    ankleGrp = lrc.group(ankle)
    
    # Rotate order adjustment
    upLeg.attr('rotateOrder').value = legIkRootCtrl.attr('rotateOrder').value
    ankle.attr('rotateOrder').value = legIkCtrl.attr('rotateOrder').value
    
    # Joint orient adjustment
    upLeg.attr('jointOrient').value = legIkRootCtrl.attr('jointOrient').value
    ankle.attr('jointOrient').value = legIkCtrl.attr('jointOrient').value
    
    # Snaping controls
    upLegGrp.snap(legIkRootCtrl.getParent())
    upLeg.snap(upLegFk)
    legIkRootCtrl.attr('t').value = upLeg.attr('t').value
    
    ankleGrp.snap(legIkCtrl.getParent())
    ankle.snap(ankleFk)
    legIkCtrl.attr('t').value = ankle.attr('t').value
    legIkCtrl.attr('r').value = ankle.attr('r').value
    
    lowLegGrp.snap(kneeIkCtrl.getParent())
    lowLeg.snap(lowLegFk)
    kneeIkCtrl.attr('t').value = lowLeg.attr('t').value
    
    # Adjusting stretch
    legIkCtrl.attr('autoStretch').value = 0
    legIkCtrl.attr('upLegStretch').value = upLegFkCtrl.attr('stretch').value
    legIkCtrl.attr('lowLegStretch').value = lowLegFkCtrl.attr('stretch').value
    legIkCtrl.attr('toeStretch').value = toeFkCtrl.attr('stretch').value
    
    leg.attr('fkIk').value = 1
    
    # Cleanup
    mc.delete(upLegGrp)
    mc.delete(lowLegGrp)
    mc.delete(ankleGrp)

def legIkToFk(ns='', part='', side=''):

    if side:
        side = '_%s_' % side
    else:
        side = '_'
    
    # Leg control
    leg = lrc.Dag(mc.ls('%sLeg%s%sCtrl' % (ns, part, side))[0])
    
    # FK controls
    upLegFkCtrl = lrc.Dag(mc.ls('%sUpLegFk%s%sCtrl' % (ns, part, side))[0])
    lowLegFkCtrl = lrc.Dag(mc.ls('%sLowLegFk%s%sCtrl' % (ns, part, side))[0])
    ankleFkCtrl = lrc.Dag(mc.ls('%sAnkleFk%s%sCtrl' % (ns, part, side))[0])
    toeFkCtrl = lrc.Dag(mc.ls('%sToeFk%s%sCtrl' % (ns, part, side))[0])
    
    # IK controls
    legIkRootCtrl = lrc.Dag(mc.ls('%sLegIkRoot%s%sCtrl' % (ns, part, side))[0])
    legIkCtrl = lrc.Dag(mc.ls('%sLegIk%s%sCtrl' % (ns, part, side))[0])
    kneeIkCtrl = lrc.Dag(mc.ls('%sKneeIk%s%sCtrl' % (ns, part, side))[0])
    
    # Fk joints
    upLegFk = lrc.Dag(mc.ls('%sUpLegFk%s%sJnt' % (ns, part, side))[0])
    lowLegFk = lrc.Dag(mc.ls('%sLowLegFk%s%sJnt' % (ns, part, side))[0])
    ankleFk = lrc.Dag(mc.ls('%sAnkleFk%s%sJnt' % (ns, part, side))[0])
    ballFk = lrc.Dag(mc.ls('%sBallFk%s%sJnt' % (ns, part, side))[0])
    toeFk = lrc.Dag(mc.ls('%sToeFk%s%sJnt' % (ns, part, side))[0])
    
    # Ik joints
    upLegIk = lrc.Dag(mc.ls('%sUpLegIk%s%sJnt' % (ns, part, side))[0])
    lowLegIk = lrc.Dag(mc.ls('%sLowLegIk%s%sJnt' % (ns, part, side))[0])
    ankleIk = lrc.Dag(mc.ls('%sAnkleIk%s%sJnt' % (ns, part, side))[0])
    ballIk = lrc.Dag(mc.ls('%sBallIk%s%sJnt' % (ns, part, side))[0])
    toeIk = lrc.Dag(mc.ls('%sToeIk%s%sJnt' % (ns, part, side))[0])
    
    # Template objects
    upLeg = lrc.Joint()
    upLegGrp = lrc.group(upLeg)
    lowLeg = lrc.Joint()
    lowLegGrp = lrc.group(lowLeg)
    ankle = lrc.Joint()
    ankleGrp = lrc.group(ankle)
    
    # Rotate order adjustment
    upLeg.attr('rotateOrder').value = upLegFkCtrl.attr('rotateOrder').value
    lowLeg.attr('rotateOrder').value = lowLegFkCtrl.attr('rotateOrder').value
    ankle.attr('rotateOrder').value = ankleFkCtrl.attr('rotateOrder').value
    
    # Joint orient adjustment
    upLeg.attr('jointOrient').value = upLegFkCtrl.attr('jointOrient').value
    lowLeg.attr('jointOrient').value = lowLegFkCtrl.attr('jointOrient').value
    ankle.attr('jointOrient').value = ankleFkCtrl.attr('jointOrient').value
    
    # Snaping controls
    upLegGrp.snap(upLegFkCtrl.getParent())
    upLeg.snap(upLegIk)
    upLegFkCtrl.attr('t').value = upLeg.attr('t').value
    upLegFkCtrl.attr('r').value = upLeg.attr('r').value
    
    upLegFkCtrl.attr('stretch').value = 0
    upLegFkCtrl.attr('stretch').value = ((lowLegIk.attr('ty').value/lowLegFk.attr('ty').value) - 1) * 10
    
    lowLegGrp.snap(lowLegFkCtrl.getParent())
    lowLeg.snap(lowLegIk)
    lowLegFkCtrl.attr('r').value = lowLeg.attr('r').value
    
    lowLegFkCtrl.attr('stretch').value = 0
    lowLegFkCtrl.attr('stretch').value = ((ankleIk.attr('ty').value/ankleFk.attr('ty').value) - 1) * 10
    
    ankleGrp.snap(ankleFkCtrl.getParent())
    ankle.snap(ankleIk)
    ankleFkCtrl.attr('r').value = ankle.attr('r').value
    
    toeFkCtrl.attr('r').value = (0,0,0)
    toeFkCtrl.attr('rx').value = -legIkCtrl.attr('toeBend').value
    toeFkCtrl.attr('stretch').value = legIkCtrl.attr('toeStretch').value
    
    leg.attr('fkIk').value = 0
    
    # Cleanup
    mc.delete(upLegGrp)
    mc.delete(lowLegGrp)
    mc.delete(ankleGrp)

def armIkToFk(prefix='', part='', side=''):
    
    if side:
        side = '_%s_' % side
    else:
        side = '_'
    
    # Arm control
    arm = lrc.Dag(mc.ls('%sArm%s%sCtrl' % (prefix, part, side))[0])
    
    # FK controls
    upArmFkCtrl = lrc.Dag(mc.ls('%s*UpArmFk%s%sCtrl' % (prefix, part, side))[0])
    forearmFkCtrl = lrc.Dag(mc.ls('%s*ForearmFk%s%sCtrl' % (prefix, part, side))[0])
    wristFkCtrl = lrc.Dag(mc.ls('%s*WristFk%s%sCtrl' % (prefix, part, side))[0])
    
    # IK controls
    armIkRootCtrl = lrc.Dag(mc.ls('%s*ArmIkRoot%s%sCtrl' % (prefix, part, side))[0])
    armIkCtrl = lrc.Dag(mc.ls('%s*ArmIk%s%sCtrl' % (prefix, part, side))[0])
    elbowIkCtrl = lrc.Dag(mc.ls('%s*ElbowIk%s%sCtrl' % (prefix, part, side))[0])
    
    # Fk joints
    upArmFk = lrc.Dag(mc.ls('%s*UpArmFk%s%sJnt' % (prefix, part, side))[0])
    forearmFk = lrc.Dag(mc.ls('%s*ForearmFk%s%sJnt' % (prefix, part, side))[0])
    wristFk = lrc.Dag(mc.ls('%s*WristFk%s%sJnt' % (prefix, part, side))[0])
    handFk = lrc.Dag(mc.ls('%s*HandFk%s%sJnt' % (prefix, part, side))[0])
    
    # Ik joints
    upArmIk = lrc.Dag(mc.ls('%s*UpArmIk%s%sJnt' % (prefix, part, side))[0])
    forearmIk = lrc.Dag(mc.ls('%s*ForearmIk%s%sJnt' % (prefix, part, side))[0])
    wristIk = lrc.Dag(mc.ls('%s*WristIk%s%sJnt' % (prefix, part, side))[0])
    handIk = lrc.Dag(mc.ls('%s*HandIk%s%sJnt' % (prefix, part, side))[0])
    
    # Template objects
    upArm = lrc.Joint()
    upArmGrp = lrc.group(upArm)
    forearm = lrc.Joint()
    forearmGrp = lrc.group(forearm)
    wrist = lrc.Joint()
    wristGrp = lrc.group(wrist)
    
    # Rotate order adjustment
    upArm.attr('rotateOrder').value = upArmFkCtrl.attr('rotateOrder').value
    forearm.attr('rotateOrder').value = forearmFkCtrl.attr('rotateOrder').value
    wrist.attr('rotateOrder').value = wristFkCtrl.attr('rotateOrder').value
    
    # Joint orient adjustment
    upArm.attr('jointOrient').value = upArmFkCtrl.attr('jointOrient').value
    forearm.attr('jointOrient').value = forearmFkCtrl.attr('jointOrient').value
    wrist.attr('jointOrient').value = wristFkCtrl.attr('jointOrient').value
    
    # Snaping controls
    upArmGrp.snap(upArmFkCtrl.getParent())
    upArm.snap(upArmIk)
    upArmFkCtrl.attr('r').value = upArm.attr('r').value
    upArmFkCtrl.attr('t').value = upArm.attr('t').value
    
    upArmFkCtrl.attr('stretch').value = 0
    upArmFkCtrl.attr('stretch').value = ((forearmIk.attr('ty').value/forearmFk.attr('ty').value) - 1) * 10
    
    forearmGrp.snap(forearmFkCtrl.getParent())
    forearm.snap(forearmIk)
    forearmFkCtrl.attr('r').value = forearm.attr('r').value
    
    forearmFkCtrl.attr('stretch').value = 0
    forearmFkCtrl.attr('stretch').value = ((wristIk.attr('ty').value/wristFk.attr('ty').value) - 1) * 10
    
    wristGrp.snap(wristFkCtrl.getParent())
    wrist.snap(wristIk)
    wristFkCtrl.attr('r').value = wrist.attr('r').value

    # Reset translate values
    forearmFkCtrl.attr('t').value = (0,0,0)
    wristFkCtrl.attr('t').value = (0,0,0)
        
    arm.attr('fkIk').value = 0
    
    # Cleanup
    mc.delete(upArmGrp)
    mc.delete(forearmGrp)
    mc.delete(wristGrp)

def armFkToIk(prefix = '', part = '', side = ''):

    if side:
        side = '_%s_' % side
    else:
        side = '_'
    
    # Arm control
    print part
    arm = lrc.Dag(mc.ls('%s*Arm%s%sCtrl' % (prefix, part, side))[0])
    
    # FK controls
    upArmFkCtrl = lrc.Dag(mc.ls('%s*UpArmFk%s%sCtrl' % (prefix, part, side))[0])
    forearmFkCtrl = lrc.Dag(mc.ls('%s*ForearmFk%s%sCtrl' % (prefix, part, side))[0])
    wristFkCtrl = lrc.Dag(mc.ls('%s*WristFk%s%sCtrl' % (prefix, part, side))[0])
    
    # IK controls
    armIkRootCtrl = lrc.Dag(mc.ls('%s*ArmIkRoot%s%sCtrl' % (prefix, part, side))[0])
    armIkCtrl = lrc.Dag(mc.ls('%s*ArmIk%s%sCtrl' % (prefix, part, side))[0])
    elbowIkCtrl = lrc.Dag(mc.ls('%s*ElbowIk%s%sCtrl' % (prefix, part, side))[0])
    
    # Fk joints
    upArmFk = lrc.Dag(mc.ls('%s*UpArmFk%s%sJnt' % (prefix, part, side))[0])
    forearmFk = lrc.Dag(mc.ls('%s*ForearmFk%s%sJnt' % (prefix, part, side))[0])
    wristFk = lrc.Dag(mc.ls('%s*WristFk%s%sJnt' % (prefix, part, side))[0])
    handFk = lrc.Dag(mc.ls('%s*HandFk%s%sJnt' % (prefix, part, side))[0])
    
    # Ik joints
    upArmIk = lrc.Dag(mc.ls('%s*UpArmIk%s%sJnt' % (prefix, part, side))[0])
    forearmIk = lrc.Dag(mc.ls('%s*ForearmIk%s%sJnt' % (prefix, part, side))[0])
    wristIk = lrc.Dag(mc.ls('%s*WristIk%s%sJnt' % (prefix, part, side))[0])
    handIk = lrc.Dag(mc.ls('%s*HandIk%s%sJnt' % (prefix, part, side))[0])
    
    # Template objects
    upArm = lrc.Joint()
    upArmGrp = lrc.group(upArm)
    forearm = lrc.Joint()
    forearmGrp = lrc.group(forearm)
    wrist = lrc.Joint()
    wristGrp = lrc.group(wrist)
    
    # Rotate order adjustment
    upArm.attr('rotateOrder').value = armIkRootCtrl.attr('rotateOrder').value
    wrist.attr('rotateOrder').value = armIkCtrl.attr('rotateOrder').value
    forearm.attr('rotateOrder').value = elbowIkCtrl.attr('rotateOrder').value
    
    # Joint orient adjustment
    upArm.attr('jointOrient').value = armIkRootCtrl.attr('jointOrient').value
    wrist.attr('jointOrient').value = armIkCtrl.attr('jointOrient').value
    
    # Snaping controls
    upArmGrp.snap(armIkRootCtrl.getParent())
    upArm.snap(upArmFk)
    armIkRootCtrl.attr('t').value = upArm.attr('t').value
    #armIkRootCtrl.attr('r').value = upArm.attr('r').value
    
    wristGrp.snap(armIkCtrl.getParent())
    mc.delete(mc.scaleConstraint(armIkCtrl.getParent(), wristGrp))
    wrist.snap(wristFk)
    armIkCtrl.attr('t').value = wrist.attr('t').value
    armIkCtrl.attr('r').value = wrist.attr('r').value
    
    forearmGrp.snap(elbowIkCtrl.getParent())
    mc.delete(mc.scaleConstraint(elbowIkCtrl.getParent(), forearmGrp))
    forearm.snap(forearmFk)
    elbowIkCtrl.attr('t').value = forearm.attr('t').value
    #elbowIkCtrl.attr('r').value = forearm.attr('r').value
    
    # Adjusting stretch
    armIkCtrl.attr('autoStretch').value = 0
    armIkCtrl.attr('upArmStretch').value = upArmFkCtrl.attr('stretch').value
    armIkCtrl.attr('forearmStretch').value = forearmFkCtrl.attr('stretch').value
        
    arm.attr('fkIk').value = 1
    
    # Cleanup
    mc.delete(upArmGrp)
    mc.delete(forearmGrp)
    mc.delete(wristGrp)