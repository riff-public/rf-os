import os
import sys

import maya.cmds as mc

scn_path = os.path.normpath(mc.file(q=True, l=True)[0])

scn_fld_path, scn_nm_ext = os.path.split(scn_path)

rd_path = os.path.join(scn_fld_path, 'rigData')
mr_path = os.path.join(rd_path, 'mainRig.py')

if os.path.exists(mr_path):
    sys.path.append(rd_path)
    
    import mainRig
    reload(mainRig)
    
    try:
        mainRig.main()
    except:
        pass
    print mainRig
    del(mainRig)