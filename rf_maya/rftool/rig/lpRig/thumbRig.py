import maya.cmds as mc

import lpRig.core as lrc
reload(lrc)
import lpRig.rigTools as lrr
reload(lrr)

class ThumbRig(object):

	def __init__(
					self,
					thumb='Thumb',
					parent='Hand_L_Jnt',
					armCtrl='Arm_L_Ctrl',
					ctrlGrp='',
					tmpJnt=(
								'Thumb_1_L_Jnt',
								'Thumb_2_L_Jnt',
								'Thumb_3_L_Jnt',
								'Thumb_4_L_Jnt',
								'Thumb_5_L_Jnt'
							),
					side=''
				):

		if side:
			side = '_%s_' % side
		else:
			side = '_'

		self.Thumb_1_Jnt = lrc.Dag(tmpJnt[0])
		self.Thumb_2_Jnt = lrc.Dag(tmpJnt[1])
		self.Thumb_3_Jnt = lrc.Dag(tmpJnt[2])
		self.Thumb_4_Jnt = lrc.Dag(tmpJnt[3])

		self.Thumb_1_Jnt.attr('ssc').v = False

		# Main group
		self.ThumbCtrl_Grp = lrc.Null()
		self.ThumbCtrl_Grp.name = '%sCtrl%sGrp' % (thumb, side)

		mc.parentConstraint(parent, self.ThumbCtrl_Grp)
		mc.scaleConstraint(parent, self.ThumbCtrl_Grp)

		self.Thumb_Ctrl = lrc.Control('stick')
		self.Thumb_Ctrl.name = '%s%sCtrl' % (thumb, side)
		self.Thumb_Ctrl.lockHideAttrs('tx', 'ty', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz', 'v')
		
		self.ThumbCtrlZr_Grp = lrc.group(self.Thumb_Ctrl)
		self.ThumbCtrlZr_Grp.name = '%sCtrlZr%sGrp' % (thumb, side)
		self.ThumbCtrlZr_Grp.snap(self.Thumb_1_Jnt)
		self.ThumbCtrlZr_Grp.parent(self.ThumbCtrl_Grp)

		# Controllers - Thumb 1
		self.Thumb_1_Ctrl = lrr.jointControl('cube')
		self.Thumb_1_Ctrl.name = '%s_1%sCtrl' % (thumb, side)
		self.Thumb_1_Ctrl.lockHideAttrs('sx', 'sy', 'sz', 'v')
		self.Thumb_1_Ctrl.scaleShape(0.5)

		self.ThumbCtrlTws_1_Grp = lrc.group(self.Thumb_1_Ctrl)
		self.ThumbCtrlSdk_1_Grp = lrc.group(self.ThumbCtrlTws_1_Grp)
		self.ThumbCtrlZr_1_Grp = lrc.group(self.ThumbCtrlSdk_1_Grp)

		self.ThumbCtrlTws_1_Grp.name = '%sCtrlTws_1%sGrp' % (thumb, side)
		self.ThumbCtrlSdk_1_Grp.name = '%sCtrlSdk_1%sGrp' % (thumb, side)
		self.ThumbCtrlZr_1_Grp.name = '%sCtrlZr_1%sGrp' % (thumb, side)

		# Controllers - Thumb 2
		self.Thumb_2_Ctrl = lrr.jointControl('cube')
		self.Thumb_2_Ctrl.name = '%s_2%sCtrl' % (thumb, side)
		self.Thumb_2_Ctrl.lockHideAttrs('sx', 'sy', 'sz', 'v')
		self.Thumb_2_Ctrl.scaleShape(0.5)

		self.ThumbCtrlTws_2_Grp = lrc.group(self.Thumb_2_Ctrl)
		self.ThumbCtrlSdk_2_Grp = lrc.group(self.ThumbCtrlTws_2_Grp)
		self.ThumbCtrlZr_2_Grp = lrc.group(self.ThumbCtrlSdk_2_Grp)
		
		self.ThumbCtrlTws_2_Grp.name = '%sCtrlTws_2%sGrp' % (thumb, side)
		self.ThumbCtrlSdk_2_Grp.name = '%sCtrlSdk_2%sGrp' % (thumb, side)
		self.ThumbCtrlZr_2_Grp.name = '%sCtrlZr_2%sGrp' % (thumb, side)

		# Controllers - Thumb 3
		self.Thumb_3_Ctrl = lrr.jointControl('cube')
		self.Thumb_3_Ctrl.name = '%s_3%sCtrl' % (thumb, side)
		self.Thumb_3_Ctrl.lockHideAttrs('sx', 'sy', 'sz', 'v')
		self.Thumb_3_Ctrl.scaleShape(0.5)

		self.ThumbCtrlTws_3_Grp = lrc.group(self.Thumb_3_Ctrl)
		self.ThumbCtrlSdk_3_Grp = lrc.group(self.ThumbCtrlTws_3_Grp)
		self.ThumbCtrlZr_3_Grp = lrc.group(self.ThumbCtrlSdk_3_Grp)
		
		self.ThumbCtrlTws_3_Grp.name = '%sCtrlTws_3%sGrp' % (thumb, side)
		self.ThumbCtrlSdk_3_Grp.name = '%sCtrlSdk_3%sGrp' % (thumb, side)
		self.ThumbCtrlZr_3_Grp.name = '%sCtrlZr_3%sGrp' % (thumb, side)

		if side == '_R_':
			self.Thumb_Ctrl.color = 'darkBlue'
			self.Thumb_1_Ctrl.color = 'darkBlue'
			self.Thumb_2_Ctrl.color = 'darkBlue'
			self.Thumb_3_Ctrl.color = 'darkBlue'
		else:
			self.Thumb_Ctrl.color = 'darkRed'
			self.Thumb_1_Ctrl.color = 'darkRed'
			self.Thumb_2_Ctrl.color = 'darkRed'
			self.Thumb_3_Ctrl.color = 'darkRed'
		
		# Controllers - parenting and positioning
		self.ThumbCtrlZr_1_Grp.rotateOrder = 'xzy'
		self.ThumbCtrlZr_2_Grp.rotateOrder = 'xzy'
		self.ThumbCtrlZr_3_Grp.rotateOrder = 'xzy'
		
		self.ThumbCtrlZr_1_Grp.snap(self.Thumb_1_Jnt)
		self.ThumbCtrlZr_2_Grp.snap(self.Thumb_2_Jnt)
		self.ThumbCtrlZr_3_Grp.snap(self.Thumb_3_Jnt)
		
		self.ThumbCtrlZr_3_Grp.parent(self.Thumb_2_Ctrl)
		self.ThumbCtrlZr_2_Grp.parent(self.Thumb_1_Ctrl)
		self.ThumbCtrlZr_1_Grp.parent(self.Thumb_Ctrl)

		# Stertch setup
		(self.ThumbStrt_2_Add,
		self.ThumbStrt_2_Mul) = lrr.fkStretch(
												ctrl=self.Thumb_2_Ctrl,
												target=self.ThumbCtrlZr_3_Grp
											)
		self.ThumbStrt_2_Add.name = "%sStrt_2%sAdd" % (thumb, side)
		self.ThumbStrt_2_Mul.name = "%sStrt_2%sMul" % (thumb, side)

		(self.ThumbStrt_3_Add,
		self.ThumbStrt_3_Mul) = lrr.fkStretch(
												ctrl=self.Thumb_3_Ctrl,
												target=self.Thumb_4_Jnt
											)
		self.ThumbStrt_3_Add.name = "%sStrt_3%sAdd" % (thumb, side)
		self.ThumbStrt_3_Mul.name = "%sStrt_3%sMul" % (thumb, side)

		# Stretch collector
		self.ThumbCtrlStrt_2_Pma = lrc.PlusMinusAverage()
		self.ThumbCtrlStrt_3_Pma = lrc.PlusMinusAverage()
		self.ThumbCtrlStrt_4_Pma = lrc.PlusMinusAverage()

		self.ThumbCtrlStrt_2_Pma.name = "%sCtrlStrt_2%sPma" % (thumb, side)
		self.ThumbCtrlStrt_3_Pma.name = "%sCtrlStrt_3%sPma" % (thumb, side)

		self.ThumbCtrlStrt_2_Pma.attr('o1') >> self.ThumbCtrlZr_3_Grp.attr('ty')
		self.ThumbCtrlStrt_3_Pma.attr('o1') >> self.Thumb_4_Jnt.attr('ty')
		
		self.ThumbStrt_2_Add.attr('o') >> self.ThumbCtrlStrt_2_Pma.last1D()
		self.ThumbStrt_3_Add.attr('o') >> self.ThumbCtrlStrt_3_Pma.last1D()

		# Squash setup
		(self.ThumbSqs_2_Add,
		self.ThumbSqs_2_Mul) = lrr.fkSquash(
												ctrl=self.Thumb_2_Ctrl,
												target=self.Thumb_2_Jnt
											)
		self.ThumbSqs_2_Add.name = "%sSqs_2%sAdd" % (thumb, side)
		self.ThumbSqs_2_Mul.name = "%sSqs_2%sMul" % (thumb, side)

		(self.ThumbSqs_3_Add,
		self.ThumbSqs_3_Mul) = lrr.fkSquash(
												ctrl=self.Thumb_3_Ctrl,
												target=self.Thumb_3_Jnt
											)
		self.ThumbSqs_3_Add.name = "%sSqs_3%sAdd" % (thumb, side)
		self.ThumbSqs_3_Mul.name = "%sSqs_3%sMul" % (thumb, side)

		(self.ThumbSqsTip_3_Add,
		self.ThumbSqsTip_3_Mul) = lrr.fkSquash(
												ctrl=self.Thumb_3_Ctrl,
												target=self.Thumb_4_Jnt
											)
		self.ThumbSqsTip_3_Add.name = "%sSqsTip_3%sAdd" % (thumb, side)
		self.ThumbSqsTip_3_Mul.name = "%sSqsTip_3%sMul" % (thumb, side)

		# Squash setup - Collectors
		self.ThumbSqs_2_Pma = lrc.PlusMinusAverage()
		self.ThumbSqs_3_Pma = lrc.PlusMinusAverage()
		self.ThumbSqsTip_3_Pma = lrc.PlusMinusAverage()

		self.ThumbSqs_2_Pma.name = "%sSqs_2%sPma" % (thumb, side)
		self.ThumbSqs_3_Pma.name = "%sSqs_3%sPma" % (thumb, side)
		self.ThumbSqsTip_3_Pma.name = "%sSqsTip_3%sPma" % (thumb, side)

		self.ThumbSqs_2_Pma.attr('o1') >> self.Thumb_2_Jnt.attr('sx')
		self.ThumbSqs_2_Pma.attr('o1') >> self.Thumb_2_Jnt.attr('sz')
		self.ThumbSqs_3_Pma.attr('o1') >> self.Thumb_3_Jnt.attr('sx')
		self.ThumbSqs_3_Pma.attr('o1') >> self.Thumb_3_Jnt.attr('sz')
		self.ThumbSqsTip_3_Pma.attr('o1') >> self.Thumb_4_Jnt.attr('sx')
		self.ThumbSqsTip_3_Pma.attr('o1') >> self.Thumb_4_Jnt.attr('sz')
		
		self.ThumbSqs_2_Add.attr('o') >> self.ThumbSqs_2_Pma.last1D()
		self.ThumbSqs_3_Add.attr('o') >> self.ThumbSqs_3_Pma.last1D()
		self.ThumbSqsTip_3_Add.attr('o') >> self.ThumbSqsTip_3_Pma.last1D()

		# Connect to joints
		mc.parentConstraint(self.Thumb_1_Ctrl, self.Thumb_1_Jnt)
		mc.parentConstraint(self.Thumb_2_Ctrl, self.Thumb_2_Jnt)
		mc.parentConstraint(self.Thumb_3_Ctrl, self.Thumb_3_Jnt)
				
		# Rotation collectors
		self.ThumbSdkRx_1_Pma = lrc.PlusMinusAverage()
		self.ThumbSdkRx_2_Pma = lrc.PlusMinusAverage()
		self.ThumbSdkRx_3_Pma = lrc.PlusMinusAverage()

		self.ThumbSdkRy_1_Pma = lrc.PlusMinusAverage()
		self.ThumbSdkRy_2_Pma = lrc.PlusMinusAverage()
		self.ThumbSdkRy_3_Pma = lrc.PlusMinusAverage()

		self.ThumbSdkRz_1_Pma = lrc.PlusMinusAverage()
		self.ThumbSdkRz_2_Pma = lrc.PlusMinusAverage()
		self.ThumbSdkRz_3_Pma = lrc.PlusMinusAverage()

		self.ThumbSdkRx_1_Pma.name = "%sSdkRx_1%sPma" % (thumb, side)
		self.ThumbSdkRx_2_Pma.name = "%sSdkRx_2%sPma" % (thumb, side)
		self.ThumbSdkRx_3_Pma.name = "%sSdkRx_3%sPma" % (thumb, side)

		self.ThumbSdkRy_1_Pma.name = "%sSdkRy_1%sPma" % (thumb, side)
		self.ThumbSdkRy_2_Pma.name = "%sSdkRy_2%sPma" % (thumb, side)
		self.ThumbSdkRy_3_Pma.name = "%sSdkRy_3%sPma" % (thumb, side)

		self.ThumbSdkRz_1_Pma.name = "%sSdkRz_1%sPma" % (thumb, side)
		self.ThumbSdkRz_2_Pma.name = "%sSdkRz_2%sPma" % (thumb, side)
		self.ThumbSdkRz_3_Pma.name = "%sSdkRz_3%sPma" % (thumb, side)
		
		self.ThumbSdkRx_1_Pma.attr('o1') >> self.ThumbCtrlSdk_1_Grp.attr('rx')
		self.ThumbSdkRx_2_Pma.attr('o1') >> self.ThumbCtrlSdk_2_Grp.attr('rx')
		self.ThumbSdkRx_3_Pma.attr('o1') >> self.ThumbCtrlSdk_3_Grp.attr('rx')

		self.ThumbSdkRy_1_Pma.attr('o1') >> self.ThumbCtrlTws_1_Grp.attr('ry')
		self.ThumbSdkRy_2_Pma.attr('o1') >> self.ThumbCtrlTws_2_Grp.attr('ry')
		self.ThumbSdkRy_3_Pma.attr('o1') >> self.ThumbCtrlTws_3_Grp.attr('ry')

		self.ThumbSdkRz_1_Pma.attr('o1') >> self.ThumbCtrlSdk_1_Grp.attr('rz')
		self.ThumbSdkRz_2_Pma.attr('o1') >> self.ThumbCtrlSdk_2_Grp.attr('rz')
		self.ThumbSdkRz_3_Pma.attr('o1') >> self.ThumbCtrlSdk_3_Grp.attr('rz')
			
		# Hand attributes
		handCtrl = lrc.Dag(armCtrl)

		if not handCtrl.attr('__hand__').exists :
			handCtrl.add(ln = '__hand__', k = True)
			handCtrl.attr('__hand__').set(l = True)

		# Hand attributes - flat
		if not handCtrl.attr('flat').exists :
			handCtrl.add(ln='flat', k=True)

		self.ThumbFlatRxAmp_1_Mul = lrr.attrAmper(
													handCtrl.attr('flat'),
													self.ThumbSdkRx_1_Pma.last1D(),
													-2,
													'%sFlatRx_1' % thumb
												)
		self.ThumbFlatRzAmp_1_Mul = lrr.attrAmper(
													handCtrl.attr('flat'),
													self.ThumbSdkRz_1_Pma.last1D(),
													0,
													'%sFlatRz_1' % thumb
												)
		self.ThumbFlatRxAmp_2_Mul = lrr.attrAmper(
													handCtrl.attr('flat'),
													self.ThumbSdkRx_2_Pma.last1D(),
													-1,
													'%sFlatRx_2' % thumb
												)
		self.ThumbFlatRxAmp_3_Mul = lrr.attrAmper(
													handCtrl.attr('flat'),
													self.ThumbSdkRx_3_Pma.last1D(),
													2,
													'%sFlatRx_3' % thumb
												)

		self.ThumbFlatRxAmp_1_Mul.name = "%sFlatRxAmp_1%sMul" % (thumb, side)
		self.ThumbFlatRzAmp_1_Mul.name = "%sFlatRzAmp_1%sMul" % (thumb, side)
		self.ThumbFlatRxAmp_2_Mul.name = "%sFlatRxAmp_2%sMul" % (thumb, side)
		self.ThumbFlatRxAmp_3_Mul.name = "%sFlatRxAmp_3%sMul" % (thumb, side)

		# Hand attributes - fist
		if not handCtrl.attr('fist').exists :
			handCtrl.add(ln='fist', k=True)

		self.ThumbFistRxAmp_1_Mul = lrr.attrAmper(handCtrl.attr('fist'), self.ThumbSdkRx_1_Pma.last1D(), 0, '%sFistRx_1'%thumb)
		self.ThumbFistRzAmp_1_Mul = lrr.attrAmper(handCtrl.attr('fist'), self.ThumbSdkRz_1_Pma.last1D(), 0, '%sFistRz_1'%thumb)
		self.ThumbFistRxAmp_2_Mul = lrr.attrAmper(handCtrl.attr('fist'), self.ThumbSdkRx_2_Pma.last1D(), -9, '%sFistRx_2'%thumb)
		self.ThumbFistRyAmp_2_Mul = lrr.attrAmper(handCtrl.attr('fist'), self.ThumbSdkRy_2_Pma.last1D(), 0, '%sFistRy_2'%thumb)
		self.ThumbFistRzAmp_2_Mul = lrr.attrAmper(handCtrl.attr('fist'), self.ThumbSdkRz_2_Pma.last1D(), 0, '%sFistRz_2'%thumb)
		self.ThumbFistRxAmp_3_Mul = lrr.attrAmper(handCtrl.attr('fist'), self.ThumbSdkRx_3_Pma.last1D(), -9, '%sFistRx_3'%thumb)

		self.ThumbFistRxAmp_1_Mul.name = "%sFistRxAmp_1%sMul" % (thumb, side)
		self.ThumbFistRzAmp_1_Mul.name = "%sFistRzAmp_1%sMul" % (thumb, side)
		self.ThumbFistRxAmp_2_Mul.name = "%sFistRxAmp_2%sMul" % (thumb, side)
		self.ThumbFistRyAmp_2_Mul.name = "%sFistRyAmp_2%sMul" % (thumb, side)
		self.ThumbFistRzAmp_2_Mul.name = "%sFistRzAmp_2%sMul" % (thumb, side)
		self.ThumbFistRxAmp_3_Mul.name = "%sFistRxAmp_3%sMul" % (thumb, side)
				
		# Hand attributes - cup
		if not handCtrl.attr('cup').exists :
			handCtrl.add(ln='cup', k=True)

		self.ThumbCupRxAmp_1_Mul = lrr.attrAmper(handCtrl.attr('cup'), self.ThumbSdkRx_1_Pma.last1D(), 0, '%sCupRx_1'%thumb)
		self.ThumbCupRxAmp_2_Mul = lrr.attrAmper(handCtrl.attr('cup'), self.ThumbSdkRx_2_Pma.last1D(), 0, '%sCupRx_2'%thumb)
		self.ThumbCupRxAmp_3_Mul = lrr.attrAmper(handCtrl.attr('cup'), self.ThumbSdkRx_3_Pma.last1D(), 0, '%sCupRx_3'%thumb)

		self.ThumbCupRxAmp_1_Mul.name = "%sCupRxAmp_1%sMul" % (thumb, side)
		self.ThumbCupRxAmp_2_Mul.name = "%sCupRxAmp_2%sMul" % (thumb, side)
		self.ThumbCupRxAmp_3_Mul.name = "%sCupRxAmp_3%sMul" % (thumb, side)

		# Hand attributes - spread
		if not handCtrl.attr('spread').exists :
			handCtrl.add(ln='spread', k=True)

		self.ThumbSprdAmp_Mul = lrr.attrAmper(handCtrl.attr('spread'), self.ThumbSdkRz_2_Pma.last1D(), 0, '%sSprd_2'%thumb)
		self.ThumbSprdAmp_Mul.name = "%sSprdAmp_1%sMul" % (thumb, side)

		# Hand attributes - break
		if not handCtrl.attr('break').exists :
			handCtrl.add(ln='break', k=True)

		self.ThumbBrkAmp_2_Mul = lrr.attrAmper(handCtrl.attr('break'), self.ThumbSdkRz_2_Pma.last1D(), 0, '%sBrk_2'%thumb)
		self.ThumbBrkAmp_2_Mul.name = "%sBrkAmp_1%sMul" % (thumb, side)
		
		# Hand attributes - flex
		if not handCtrl.attr('flex').exists :
			handCtrl.add(ln='flex', k=True)

		self.ThumbFlexAmp_2_Mul = lrr.attrAmper(handCtrl.attr('flex'), self.ThumbSdkRx_2_Pma.last1D(), 0, '%sFlex_2'%thumb)
		self.ThumbFlexAmp_2_Mul.name = "%sFlexAmp_1%sMul" % (thumb, side)

		# Hand attributes - baseSpread
		if not handCtrl.attr('baseSpread').exists :
			handCtrl.add(ln='baseSpread', k=True)

		self.ThumbBsSprdAmp_1_Mul = lrr.attrAmper(handCtrl.attr('baseSpread'), self.ThumbSdkRz_1_Pma.last1D(), 0, '%sBsSprd_1'%thumb)
		self.ThumbBsSprdAmp_1_Mul.name = "%sBsSprdAmp_1%sMul" % (thumb, side)

		# Hand attributes - baseBreak
		if not handCtrl.attr('baseBreak').exists :
			handCtrl.add(ln='baseBreak', k=True)

		self.ThumbBsBrkAmp_1_Mul = lrr.attrAmper(handCtrl.attr('baseBreak'), self.ThumbSdkRz_1_Pma.last1D(), 0, '%sBsBrk_1'%thumb)
		self.ThumbBsBrkAmp_1_Mul.name = "%sBsBrkAmp_1%sMul" % (thumb, side)

		# Hand attributes - baseFlex
		if not handCtrl.attr('baseFlex').exists :
			handCtrl.add(ln='baseFlex', k=True)

		self.ThumbBsFlexAmp_1_Mul = lrr.attrAmper(handCtrl.attr('baseFlex'), self.ThumbSdkRx_1_Pma.last1D(), 0, '%sBsFlex_1'%thumb)
		self.ThumbBsFlexAmp_1_Mul.name = "%sBsFlexAmp_1%sMul" % (thumb, side)

		# Thumb attributes
		self.Thumb_Ctrl.add(ln='__%s__' % thumb, k=True)
		self.Thumb_Ctrl.attr('__%s__' % thumb).set(l=True)

		# Thumb attributes - fold
		self.Thumb_Ctrl.add(ln='fold', k=True)
		
		self.ThumbFoldAmp_2_Mul = lrr.attrAmper(self.Thumb_Ctrl.attr('fold'), self.ThumbSdkRx_2_Pma.last1D(),-9)
		self.ThumbFoldAmp_3_Mul = lrr.attrAmper(self.Thumb_Ctrl.attr('fold'), self.ThumbSdkRx_3_Pma.last1D(),-9)

		self.ThumbFoldAmp_2_Mul.name = "%sFldAmp_2%sMul" % (thumb, side)
		self.ThumbFoldAmp_3_Mul.name = "%sFldAmp_3%sMul" % (thumb, side)
		
		# Thumb attributes - stretch
		self.Thumb_Ctrl.add(ln='stretch', k=True)
		
		self.ThumbStrtAmp_2_Mul = lrr.attrAmper(self.Thumb_Ctrl.attr('stretch'), self.ThumbCtrlStrt_2_Pma.last1D(), self.ThumbStrt_2_Mul.attr('i1').v)
		self.ThumbStrtAmp_3_Mul = lrr.attrAmper(self.Thumb_Ctrl.attr('stretch'), self.ThumbCtrlStrt_3_Pma.last1D(), self.ThumbStrt_3_Mul.attr('i1').v)

		self.ThumbStrtAmp_2_Mul.name = "%sStrtAmp_2%sMul" % (thumb, side)
		self.ThumbStrtAmp_3_Mul.name = "%sStrtAmp_3%sMul" % (thumb, side)

		# Thumb attributes - squash
		self.Thumb_Ctrl.add(ln='squash', k=True)
		
		self.ThumbSqsAmp_2_Mul = lrr.attrAmper(self.Thumb_Ctrl.attr('squash'), self.ThumbSqs_2_Pma.last1D(), 0.1)
		self.ThumbSqsAmp_3_Mul = lrr.attrAmper(self.Thumb_Ctrl.attr('squash'), self.ThumbSqs_3_Pma.last1D(), 0.1)
		self.ThumbSqsTipAmp_3_Mul = lrr.attrAmper(self.Thumb_Ctrl.attr('squash'), self.ThumbSqsTip_3_Pma.last1D(), 0.1)

		self.ThumbSqsAmp_2_Mul.name = "%sSqsAmp_2%sMul" % (thumb, side)
		self.ThumbSqsAmp_3_Mul.name = "%sSqsAmp_3%sMul" % (thumb, side)
		self.ThumbSqsTipAmp_3_Mul.name = "%sSqsTipAmp_4%sMul" % (thumb, side)

		# thumb attribute - __thumb1__
		self.Thumb_Ctrl.add(ln = '__thumb1__', k = True)
		self.Thumb_Ctrl.attr('__thumb1__').set(l = True)
		
		# thumb attribute - __thumb1__ - thumb1Fold
		self.Thumb_Ctrl.add(ln = 'thumb1Fold', k = True)
		self.ThumbFoldAmp_1_Mul = lrr.attrAmper(self.Thumb_Ctrl.attr('thumb1Fold'), self.ThumbSdkRx_1_Pma.last1D(), -9)
		self.ThumbFoldAmp_1_Mul.name = "%sFoldAmp_1%sMul" % (thumb, side)

		# thumb attribute - __thumb1__ - thumb1Spread
		self.Thumb_Ctrl.add(ln = 'thumb1Spread', k = True)
		self.ThumbSprdAmp_1_Mul = lrr.attrAmper(self.Thumb_Ctrl.attr('thumb1Spread'), self.ThumbSdkRz_1_Pma.last1D(), -9)
		self.ThumbSprdAmp_1_Mul.name = "%sSprdAmp_1%sMul" % (thumb, side)

		# thumb attribute - __thumb1__ - thumb1Twist
		self.Thumb_Ctrl.add(ln = 'thumb1Twist', k = True)
		self.ThumbTwsAmp_1_Mul = lrr.attrAmper(self.Thumb_Ctrl.attr('thumb1Twist'), self.ThumbSdkRy_1_Pma.last1D(), -9)
		self.ThumbTwsAmp_1_Mul.name = "%sTwsAmp_1%sMul" % (thumb, side)

		# thumb attribute - __thumb2__
		self.Thumb_Ctrl.add(ln = '__thumb2__', k = True)
		self.Thumb_Ctrl.attr('__thumb2__').set(l = True)
		
		# thumb attribute - __thumb2__ - thumb2Fold
		self.Thumb_Ctrl.add(ln = 'thumb2Fold', k = True)
		self.ThumbFoldAmp_2_Mul = lrr.attrAmper(self.Thumb_Ctrl.attr('thumb2Fold'), self.ThumbSdkRx_2_Pma.last1D(), -9)
		self.ThumbFoldAmp_2_Mul.name = "%sFoldAmp_2%sMul" % (thumb, side)

		# thumb attribute - __thumb2__ - thumb2Spread
		self.Thumb_Ctrl.add(ln = 'thumb2Spread', k = True)
		self.ThumbSprdAmp_2_Mul = lrr.attrAmper(self.Thumb_Ctrl.attr('thumb2Spread'), self.ThumbSdkRz_2_Pma.last1D(), -9)
		self.ThumbSprdAmp_2_Mul.name = "%sSprdAmp_2%sMul" % (thumb, side)

		# thumb attribute - __thumb2__ - thumb2Twist
		self.Thumb_Ctrl.add(ln = 'thumb2Twist', k = True)
		self.ThumbTwsAmp_2_Mul = lrr.attrAmper(self.Thumb_Ctrl.attr('thumb2Twist'), self.ThumbSdkRy_2_Pma.last1D(), -9)
		self.ThumbTwsAmp_2_Mul.name = "%sTwsAmp_2%sMul" % (thumb, side)
		
		# thumb attribute - __thumb3__
		self.Thumb_Ctrl.add(ln = '__thumb3__', k = True)
		self.Thumb_Ctrl.attr('__thumb3__').set(l = True)
		
		# thumb attribute - __thumb3__ - thumb3Fold
		self.Thumb_Ctrl.add(ln = 'thumb3Fold', k = True)
		self.ThumbFoldAmp_3_Mul = lrr.attrAmper(self.Thumb_Ctrl.attr('thumb3Fold'), self.ThumbSdkRx_3_Pma.last1D(), -9)
		self.ThumbFoldAmp_3_Mul.name = "%sFoldAmp_3%sMul" % (thumb, side)
		
		# thumb attribute - __thumb3__ - thumb3Spread
		self.Thumb_Ctrl.add(ln = 'thumb3Spread', k = True)
		self.ThumbSprdAmp_3_Mul = lrr.attrAmper(self.Thumb_Ctrl.attr('thumb3Spread'), self.ThumbSdkRz_3_Pma.last1D(), -9)
		self.ThumbSprdAmp_3_Mul.name = "%sSprdAmp_3%sMul" % (thumb, side)
		
		# thumb attribute - __thumb3__ - thumb3Twist
		self.Thumb_Ctrl.add(ln = 'thumb3Twist', k = True)
		self.ThumbTwsAmp_3_Mul = lrr.attrAmper(self.Thumb_Ctrl.attr('thumb3Twist'), self.ThumbSdkRy_3_Pma.last1D(), -9)
		self.ThumbTwsAmp_3_Mul.name = "%sTwsAmp_3%sMul" % (thumb, side)
		
		# Grouping
		self.ThumbCtrl_Grp.parent(ctrlGrp)