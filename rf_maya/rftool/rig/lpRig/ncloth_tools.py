import os
import shutil
import re
import json

import pymel.core as pmc

PV_ATTRS = ['bendAngleDropoffPerVertex', 'bendPerVertex', 'compressionPerVertex', 
				'deformPerVertex', 'dragPerVertex', 'inputAttractPerVertex', 
				'liftPerVertex', 'restLengthScalePerVertex', 'restitutionAnglePerVertex',
				'rigidityPerVertex', 'stretchPerVertex', 'tangentialDragPerVertex',
				'stretchPerVertex', 'tangentialDragPerVertex', 'wrinklePerVertex']

DYN_ATTRS = ['thickness', 'bounce', 'friction', 'damp', 'stickiness', 'collideStrength', 
			'collisionFlag', 'selfCollisionFlag', 'maxSelfCollisionIterations', 'maxIterations', 
			'pointMass', 'restLengthScale', 'collide', 'selfCollide', 'collisionLayer', 
			'pushOut', 'pushOutRadius', 'crossoverPush', 'trappedCheck', 'nucleusId', 
			'localSpaceOutput', 'scalingRelation', 'stretchResistance', 'compressionResistance', 
			'bendSolver', 'bendResistance', 'bendAngleDropoff', 'restitutionTension', 
			'restitutionAngle', 'shearResistance', 'rigidity', 'deformResistance', 
			'inputMeshAttract', 'inputAttractDamp', 'inputMotionDrag', 'bendAngleScale', 
			'sortLinks', 'evaluationOrder', 'addCrossLinks', 'stretchDamp', 
			'selfCollideWidthScale', 'selfCollisionSoftness', 'selfCrossoverPush', 
			'selfTrappedCheck', 'ignoreSolverGravity', 'lift', 'drag', 'tangentialDrag']

def disable_all_neucleuses():
	"""Disables all nucleus nodes in the scene.
	"""
	ncnodes = pmc.ls(type='nucleus')
	if not ncnodes:
		return True
	for ncnode in ncnodes:
		ncnode.enable.set(0)

def enable_all_neucleuses():
	"""Enables all nucleus nodes in the scene.
	"""
	ncnodes = pmc.ls(type='nucleus')
	if not ncnodes:
		return True
	for ncnode in ncnodes:
		ncnode.enable.set(1)

def get_current_cloth_data_path():
	"""Gets cloth data folder path of CWD.
	"""
	scenename = os.path.normpath(pmc.sceneName())
	folderpath, filename_ext = os.path.split(scenename)
	filename, fileext = os.path.splitext(filename_ext)

	datapath = os.path.join(folderpath, 'clothdata')

	if not os.path.exists(datapath):
		os.makedirs(datapath)
	
	return datapath

def export_dynattrs_to_cwd(nclothnode=None):
	"""Exports dynamic attribute values to current data file path.
	
	:param nclothnode: pymel.core.nodetypes.NCloth
	"""
	filepath = os.path.join(get_current_cloth_data_path(), 
							'%sDynAttr.dat' % nclothnode.name())

	return export_dynattrs(nclothnode, filepath)

def export_dynattrs(nclothnode=None, datapath=None):
	"""Exports dynamic attribute values to the given data file path.

	:param nclothnode: pymel.core.nodetypes.NCloth
	:param datapath: a path to data file
	"""
	output = {}

	for dynattr in DYN_ATTRS:
		
		val = nclothnode.attr(dynattr).get()
		output[dynattr] = val
		
	with open(datapath, 'w') as dataobj:
		json.dump(output, dataobj, indent=4, sort_keys=True)

	return datapath

def import_current_dynattrs(nclothnode=None, attrs=DYN_ATTRS):
	"""Imports dynamic attribute values from data file of CWD.
	
	:param nclothnode: pymel.core.nodetypes.NCloth
	:param attrs: A list of dynamic attributes that will be imported.
	"""
	filepath = os.path.join(get_current_cloth_data_path(), 
							'%sDynAttr.dat' % nclothnode.name())

	return import_dynattrs(nclothnode, filepath)

def import_dynattrs(nclothnode=None, datapath=None, attrs=DYN_ATTRS):
	"""Imports dynamic attributes those are in the given attrs 
	from the given datapath.
	
	:param nclothnode: pymel.core.nodetypes.NCloth
	:param datapath: a path to data file
	:param attrs: A list of dynamic attributes that will be imported.
	"""
	if not os.path.exists(datapath):
		return False

	with open(datapath, 'r') as dataobj:

		data = json.load(dataobj)

		for attr in data.keys():
			if not attr in attrs:
				continue
			nclothnode.attr(attr).set(data[attr])

	return datapath

def export_pvattrs_to_cwd(nclothnode=None):
	"""Exports per-vertex attribute values to data file of CWD.
	
	:param nclothnode: pymel.core.nodetypes.NCloth
	"""
	filepath = os.path.join(get_current_cloth_data_path(), 
							'%sPv.dat' % nclothnode.name())

	return export_pvattrs(nclothnode, filepath)

def export_pvattrs(nclothnode=None, datapath=None):
	"""Exports per-vertex attribute values to the given data file path.
	
	:param nclothnode: pymel.core.nodetypes.NCloth
	:param datapath: a path to data file
	"""
	output = {}

	for pvattr in PV_ATTRS:

		vals = nclothnode.attr(pvattr).get()

		if not vals:
			continue

		output[pvattr] = vals

	with open(datapath, 'w') as dataobj:
		json.dump(output, dataobj, indent=4, sort_keys=True)

	return datapath

def import_current_pvattrs(nclothnode=None, attrs=PV_ATTRS):
	"""Imports per-vertex attributes those are in the given attrs 
	from the data file of CWD.
	
	:param nclothnode: pymel.core.nodetypes.NCloth
	:param attrs: A list of per-vertex attributes that will be imported.
	"""
	filepath = os.path.join(get_current_cloth_data_path(), 
							'%sPv.dat' % nclothnode.name())

	return import_pvattrs(nclothnode, filepath)

def import_pvattrs(nclothnode=None, datapath=None, attrs=PV_ATTRS):
	"""Imports per-vertex attributes those are in the given attrs 
	from the given datapath.
	
	:param nclothnode: pymel.core.nodetypes.NCloth
	:param datapath: a path to data file
	:param attrs: A list of per-vertex attributes that will be imported.
	"""
	if not os.path.exists(datapath):
		return False

	with open(datapath, 'r') as dataobj:

		data = json.load(dataobj)

		for attr in data.keys():
			if not attr in attrs:
				continue
			nclothnode.attr(attr).set(data[attr], type='doubleArray')

	return datapath

def export_all_ncloth_pvattrs_to_cwd():
	"""Exports all nCloth per-vertex attributes to data folder of CWD.
	"""
	clothnodes = pmc.ls(type='nCloth')
	for clothnode in clothnodes:
		export_pvattrs_to_cwd(clothnode)

	return True

def export_all_ncloth_dynattrs_to_cwd():
	"""Exports all nCloth dynamic attributes to data folder of CWD.
	"""
	clothnodes = pmc.ls(type='nCloth')
	for clothnode in clothnodes:
		export_dynattrs_to_cwd(clothnode)
	
	return True
