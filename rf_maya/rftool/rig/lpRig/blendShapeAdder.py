from functools import partial

import maya.cmds as mc
import maya.mel as mm
import re

def run():
	# BlendShapeAdder call back
	ui = BlendShapeAdder()
	ui.show()

	return ui

class BlendShapeLister(object):

	def __init__(self, name=''):

		self.ui = '%sBshLister' % name

		width = 200

		self.mainCl = mc.columnLayout('%sMainCl' % self.ui, adj=True, w=width)
		self.label = mc.text('%sLabelText' % self.ui, label=name)
		self.bshTsl = mc.textScrollList('%sBshTsl' % self.ui, sc=partial(self.tslSelected))

		self.getBut = mc.button('%sGetBut' % self.ui, l='Get', h=40, c=partial(self.get))

		self.adRmRl = mc.rowLayout('%sAdRmRl' % self.ui, nc=2, adj=1)
		self.addBut = mc.button('%sAddBut' % self.ui, l='Add', h=20, w=width/2 - 5, c=partial(self.add))
		self.remBut = mc.button('%sRemBut' % self.ui, l='Remove', h=20, w=width/2 - 5, c=partial(self.rem))
		mc.setParent('..')

		self.upDnRl = mc.rowLayout('%sUpDnRl' % self.ui, nc=2, adj=1)
		self.upBut = mc.button('%sUpBut' % self.ui, l='Up', h=20, w=width/2 - 5, c=partial(self.mv, -1))
		self.dnBut = mc.button('%sDownBut' % self.ui, l='Down', h=20, w=width/2 - 5, c=partial(self.mv, 1))

	def get(self, *args):
		mc.textScrollList(self.bshTsl, e=True, ra=True)
		for sel in mc.ls(sl=True):
			if not mc.listRelatives(sel, s=True): continue
			mc.textScrollList(self.bshTsl, e=True, a=sel)

	def add(self, *args):
		ra = mc.textScrollList(self.bshTsl, q=True, ai=True) or []
		for sel in mc.ls(sl=True):
			if not mc.listRelatives(sel, s=True): continue
			if not sel in ra:
				mc.textScrollList(self.bshTsl, e=True, a=sel)

	def rem(self, *args):
		sel = mc.textScrollList(self.bshTsl, q=True, si=True)
		mc.textScrollList(self.bshTsl, e=True, ri=sel)

	def mv(self, *args):
		
		add = args[0]

		ai = mc.textScrollList(self.bshTsl, q=True, ai=True)
		si = mc.textScrollList(self.bshTsl, q=True, si=True)[0]

		idx = None
		for ix, item in enumerate(ai):
			if si == item:
				idx = ix

		if idx == None: return False # If no selected item found.
		if idx == len(ai)-1 and add == 1: return False # If select the last item.
		if idx == 0 and add == -1: return False # If select the first item.

		newIdx = idx + add		
		tmpItem = ai[newIdx]
		ai[newIdx] = si
		ai[idx] = tmpItem

		mc.textScrollList(self.bshTsl, e=True, ra=True)
		for item in ai:
			mc.textScrollList(self.bshTsl, e=True, a=item)

		mc.textScrollList(self.bshTsl, e=True, si=si)

	def tslSelected(self, *args):
		mc.select(mc.textScrollList(self.bshTsl, q=True, si=True), r=True)

class BlendShapeAdder(object):

	def __init__(self):

		self.ui = 'pkBlendShapeAdder'
		self.win = '%sWin' % self.ui

	def show(self):

		if mc.window(self.win, exists=True):
			mc.deleteUI(self.win)

		mc.window(self.win, t=self.ui, rtf=True)
		self.mainCl = mc.columnLayout('%sMainCl' % self.ui, adj=True)
		self.mainRl = mc.rowLayout('%sMainRl' % self.ui, adj=True, nc=2)
		
		self.srcLister = BlendShapeLister('src')

		mc.setParent(self.mainRl)
		self.tarLister = BlendShapeLister('tar')

		mc.setParent(self.mainCl)
		self.addBut = mc.button('%sAddBut' % self.ui, l='Add', h=40, c=partial(self.add))

		mc.showWindow(self.win)
		mc.window(self.win, e=True, w=400)
		mc.window(self.win, e=True, h=250)

	def add(self, *args):

		srcObjs = mc.textScrollList(self.srcLister.bshTsl, q=True, ai=True)
		tarObjs = mc.textScrollList(self.tarLister.bshTsl, q=True, ai=True)

		for src, tar in zip(srcObjs, tarObjs):
			addBlendShape(src, tar)

def addBlendShape(src='', tar=''):
	'''
	Add src shape to tar.
	'''
	bshs = findRelatedBlendshape(tar)

	if not bshs:
		# Create front of chain blend shape if no blend shape is connected to the target.
		addFrontOfChainBlendShape(src, tar)
	else:
		appendBlendShape(bshs[0], src, tar)

	return True

def findRelatedBlendshape(obj=''):
	'''
	Find the blend shape nodes that connected to the shape node of given transform node.
	'''
	chdn = mc.listRelatives(obj, ad=True)

	# Quit if there is no related shape node.
	if not chdn: return False
	
	# Find related objectSet.
	objSets = mc.listConnections(chdn[0], type='objectSet', d=True, s=False)

	# Quit, if current shape has no related objectSet.
	if not objSets: return False
	
	bshs = []
	
	for objSet in objSets:

		relBshs = mc.listConnections(objSet, type='blendShape', s=True, d=False)

		# Continue, if current object set has no related blend shape node.
		if not relBshs: continue

		for relBsh in relBshs:
			if not relBsh in bshs:
				bshs.append(relBsh)

	return bshs

def addFrontOfChainBlendShape(src='', tar=''):
	'''
	Create blend shape node with 'frontOfChain' option to 'tar' object.
	'''
	localName = tar.split(':')[-1]
	tmpNameList = localName.split('_')

	if len(tmpNameList) > 1:
		tmpNameList[-1] = 'Bsn'
	else:
		tmpNameList.append('Bsn')

	bsn = '_'.join(tmpNameList)
	mc.blendShape(src, tar, frontOfChain=True, origin='local', n=bsn)

	mc.setAttr('%s.w[0]' % bsn, 1)

def appendBlendShape(bsn='', bshObj='', targetObj=''):
	'''
	Add bshObj to bsn as the last blend shape attr.
	'''
	# Build the alias attribute dictionary.
	# Keys are weight numbers and values are attribute names.
	bshAttrs = mc.aliasAttr(bsn, q=True)
	
	bshIdx = 0
	if bshAttrs:
		bshAttrDict = {}
		for ix in range(1, len(bshAttrs), 2):
			exp = r'([0-9]+)'
			m = re.search(exp, bshAttrs[ix])
			bshAttrDict[int(m.group(1))] = bshAttrs[ix-1]

		bshIdx = sorted(bshAttrDict.keys())[-1] + 1

	mc.blendShape(bsn, e=True, t=(targetObj, bshIdx, bshObj, 1))

	mc.setAttr('%s.weight[%s]' % (bsn, bshIdx), 1)