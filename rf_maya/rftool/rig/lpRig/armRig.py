import maya.cmds as mc

import lpRig.core as lrc
import lpRig.rigTools as lrr
import lpRig.ribbon as lrb
reload(lrc)
reload(lrr)
reload(lrb)

from lpRig import aimRig
reload(aimRig)

class ArmRig(object):

	def __init__(
					self,
					parent='ClavTip_L_Jnt',
					ctrlGrp='',
					jntGrp='',
					ikhGrp='',
					skinGrp='',
					stillGrp='',
					ribbon=True,
					tmpJnt=[
								'UpperArm_L_Jnt',
								'Forearm_L_Jnt',
								'Wrist_L_Jnt',
								'Hand_L_Jnt',
								'ElbowIk_L_Jnt'
							],
					part='',
					side=''
				):

		if side:
			side = '_%s_' % side
		else:
			side = '_'

		# Skin joints
		self.UpArm_Jnt = lrc.Dag(tmpJnt[0])
		self.Forearm_Jnt = lrc.Dag(tmpJnt[1])
		self.Wrist_Jnt = lrc.Dag(tmpJnt[2])
		self.Hand_Jnt = lrc.Dag(tmpJnt[3])
		self.ElbowIk_Jnt = lrc.Dag(tmpJnt[4])

		self.Hand_Jnt.attr('ssc').v = 0

		self.UpArm_Jnt.rotateOrder = 'yxz'
		self.Forearm_Jnt.rotateOrder = 'yzx'
		self.Wrist_Jnt.rotateOrder = 'yzx'
		self.Hand_Jnt.rotateOrder = 'yzx'

		# Main group
		self.Ctrl_Grp = lrc.Null()
		self.Ctrl_Grp.name = 'Arm%sCtrl%sGrp' % (part, side)

		mc.parentConstraint(parent, self.Ctrl_Grp)
		mc.scaleConstraint(parent, self.Ctrl_Grp)

		self.Jnt_Grp = lrc.Null()
		self.Jnt_Grp.name = 'Arm%sJnt%sGrp' % (part, side)

		mc.parentConstraint(parent, self.Jnt_Grp)
		mc.scaleConstraint(parent, self.Jnt_Grp)

		# Length
		self.UpArmLen = lrc.distance(self.UpArm_Jnt, self.Forearm_Jnt)
		self.ForearmLen = lrc.distance(self.Forearm_Jnt, self.Wrist_Jnt)

		# Main Color
		self.Arm_Ctrl = lrc.Control('stick')
		self.Arm_Ctrl.lockHideAttrs('tx', 'ty', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz', 'v')
		self.Arm_Ctrl.name = 'Arm%s%sCtrl' % (part, side)

		self.ArmCtrlZr_Grp = lrc.group(self.Arm_Ctrl)
		self.ArmCtrlZr_Grp.name = 'ArmCtrlZr%s%sGrp' % (part, side)

		mc.parentConstraint(self.Wrist_Jnt, self.ArmCtrlZr_Grp)
		self.ArmCtrlZr_Grp.parent(self.Ctrl_Grp)

		# Main Color - Color
		if side == '_R_':
			self.Arm_Ctrl.color = 'blue'
		else:
			self.Arm_Ctrl.color = 'red'

		# Non-roll Joints
		(self.UpArmNr_Jnt,
		self.UpArmNrJntZr_Grp,
		self.UpArmTipNr_Jnt,
		self.UpArmNr_Ikh,
		self.UpArmNrIkhZr_Grp) = lrr.createNonRollJointChain(parent, self.UpArm_Jnt, self.Forearm_Jnt, 'UpArm%s' % part, side)
		self.UpArmNrJntZr_Grp.parent(self.Jnt_Grp)
		self.UpArmNrIkhZr_Grp.parent(self.Jnt_Grp)

		(self.ForearmNr_Jnt,
		self.ForearmNrJntZr_Grp,
		self.ForearmTipNr_Jnt,
		self.ForearmNr_Ikh,
		self.ForearmNrIkhZr_Grp) = lrr.createNonRollJointChain(self.UpArm_Jnt, self.Forearm_Jnt, self.Wrist_Jnt, 'Forearm%s' % part, side)
		self.ForearmNrJntZr_Grp.parent(self.Jnt_Grp)
		self.ForearmNrIkhZr_Grp.parent(self.Jnt_Grp)
		
		# Aa rig
		self.aaUpArm = lrr.addAa(self.UpArm_Jnt, self.Forearm_Jnt, 'UpArm%s' % part, side, (0, 0, 1))
		self.aaUpArm.orig.parent(self.Jnt_Grp)

		self.aaForearm = lrr.addAa(self.Forearm_Jnt, self.Wrist_Jnt, 'Forearm%s' % part, side, (0, 0, 1))
		self.aaForearm.orig.parent(self.Jnt_Grp)

		self.aaWrist = lrr.addAa(self.Wrist_Jnt, self.Hand_Jnt, 'Wrist%s' % part, side, (0, 0, 1))
		self.aaWrist.orig.parent(self.Jnt_Grp)
		
		# Arm scale
		self.Arm_Ctrl.add(ln='handScale', dv=1, k=True)
		self.Arm_Ctrl.attr('handScale') >> self.Wrist_Jnt.attr('sx')
		self.Arm_Ctrl.attr('handScale') >> self.Wrist_Jnt.attr('sy')
		self.Arm_Ctrl.attr('handScale') >> self.Wrist_Jnt.attr('sz')

		# FK Rig
		# FK Rig - Main groups
		self.ArmFkCtrl_Grp = lrc.Null()
		self.ArmFkCtrl_Grp.name = 'ArmFkCtrl%s%sGrp' % (part, side)
		self.ArmFkCtrl_Grp.snap(self.Ctrl_Grp)
		self.ArmFkCtrl_Grp.parent(self.Ctrl_Grp)
		
		self.ArmFkJnt_Grp = lrc.Null()
		self.ArmFkJnt_Grp.name = 'ArmFkJnt%s%sGrp' % (part, side)
		self.ArmFkJnt_Grp.snap(self.Jnt_Grp)
		self.ArmFkJnt_Grp.parent(self.Jnt_Grp)

		# FK Rig - Joints
		self.UpArmFk_Jnt = lrr.jointAt(self.UpArm_Jnt)
		self.ForearmFk_Jnt = lrr.jointAt(self.Forearm_Jnt)
		self.WristFk_Jnt = lrr.jointAt(self.Wrist_Jnt)
		self.HandFk_Jnt = lrr.jointAt(self.Hand_Jnt)

		self.UpArmFk_Jnt.name = 'UpArmFk%s%sJnt' % (part, side)
		self.ForearmFk_Jnt.name = 'ForearmFk%s%sJnt' % (part, side)
		self.WristFk_Jnt.name = 'WristFk%s%sJnt' % (part, side)
		self.HandFk_Jnt.name = 'HandFk%s%sJnt' % (part, side)

		self.HandFk_Jnt.parent(self.WristFk_Jnt)
		self.WristFk_Jnt.parent(self.ForearmFk_Jnt)
		self.ForearmFk_Jnt.parent(self.UpArmFk_Jnt)
		self.UpArmFk_Jnt.parent(self.ArmFkJnt_Grp)
		self.HandFk_Jnt.attr('ssc').v = 0

		self.UpArmFk_Jnt.rotateOrder = 'yxz'
		self.ForearmFk_Jnt.rotateOrder = 'yzx'
		self.WristFk_Jnt.rotateOrder = 'yzx'
		self.HandFk_Jnt.rotateOrder = 'yzx'

		# FK Rig - Controllers
		self.UpArmFk_Ctrl = lrr.jointControl('cube')
		self.UpArmFk_Ctrl.name = 'UpArmFk%s%sCtrl' % (part, side)
		self.UpArmFk_Ctrl.lockHideAttrs('sx', 'sy', 'sz', 'v')

		self.UpArmFkCtrlZr_Grp = lrc.group(self.UpArmFk_Ctrl)
		self.UpArmFkCtrlZr_Grp.name = 'UpArmFkCtrlZr%s%sGrp' % (part, side)

		self.ForearmFk_Ctrl = lrr.jointControl('cube')
		self.ForearmFk_Ctrl.name = 'ForearmFk%s%sCtrl' % (part, side)
		self.ForearmFk_Ctrl.lockHideAttrs('sx', 'sy', 'sz', 'v')

		self.ForearmFkGmbl_Ctrl = lrc.addGimbal(self.ForearmFk_Ctrl)
		self.ForearmFkGmbl_Ctrl.name = 'ForearmFkGmbl%s%sCtrl' % (part, side)
		
		self.ForearmFkCtrlZr_Grp = lrc.group(self.ForearmFk_Ctrl)
		self.ForearmFkCtrlZr_Grp.name = 'ForearmFkCtrlZr%s%sGrp' % (part, side)

		self.WristFk_Ctrl = lrr.jointControl('cube')
		self.WristFk_Ctrl.name = 'WristFk%s%sCtrl' % (part, side)
		self.WristFk_Ctrl.lockHideAttrs('sx', 'sy', 'sz', 'v')

		self.WristFkGmbl_Ctrl = lrc.addGimbal(self.WristFk_Ctrl)
		self.WristFkGmbl_Ctrl.name = 'WristFkGmbl%s%sCtrl' % (part, side)

		self.WristFkCtrlZr_Grp = lrc.group(self.WristFk_Ctrl)
		self.WristFkCtrlZr_Grp.name = 'WristFkCtrlZr%s%sGrp' % (part, side)

		self.WristScaOfst_Grp = lrc.Null()
		self.WristScaOfst_Grp.name = 'WristScaOfst%s%sGrp' % (part, side)
		self.WristScaOfst_Grp.parent(self.WristFkGmbl_Ctrl)

		# FK Rig - Controllers -Color
		if side == '_R_':
			self.UpArmFk_Ctrl.color = 'blue'
			self.ForearmFk_Ctrl.color = 'blue'
			self.WristFk_Ctrl.color = 'blue'
		else:
			self.UpArmFk_Ctrl.color = 'red'
			self.ForearmFk_Ctrl.color = 'red'
			self.WristFk_Ctrl.color = 'red'

		# FK Rig - Controllers - Positioning
		self.UpArmFkCtrlZr_Grp.snapPoint(self.UpArm_Jnt)
		self.UpArmFk_Ctrl.snapOrient(self.UpArm_Jnt)
		self.UpArmFk_Ctrl.freeze(r=True)
		
		self.UpArmFkGmbl_Ctrl = lrc.addGimbal(self.UpArmFk_Ctrl)
		self.UpArmFkGmbl_Ctrl.name = 'UpArmFkGmbl%s%sCtrl' % (part, side)

		self.ForearmFkCtrlZr_Grp.snap(self.Forearm_Jnt)
		self.WristFkCtrlZr_Grp.snap(self.Wrist_Jnt)
		
		self.WristFkCtrlZr_Grp.parent(self.ForearmFkGmbl_Ctrl)
		self.ForearmFkCtrlZr_Grp.parent(self.UpArmFkGmbl_Ctrl)
		self.UpArmFkCtrlZr_Grp.parent(self.ArmFkCtrl_Grp)

		self.UpArmFk_Ctrl.rotateOrder = 'yxz'
		self.UpArmFkGmbl_Ctrl.rotateOrder = 'yzx'
		self.ForearmFk_Ctrl.rotateOrder = 'yzx'
		self.ForearmFkGmbl_Ctrl.rotateOrder = 'yzx'
		self.WristFk_Ctrl.rotateOrder = 'yzx'
		self.WristFkGmbl_Ctrl.rotateOrder = 'yzx'

		# FK Rig - Controllers - Strt setup
		(self.UpArmFkStrt_Add,
		self.UpArmFkStrt_Mul) = lrr.fkStretch(
													ctrl=self.UpArmFk_Ctrl,
													target=self.ForearmFkCtrlZr_Grp
												)
		self.UpArmFkStrt_Add.name = 'UpArmFkStrt%s%sAdd' % (part, side)
		self.UpArmFkStrt_Mul.name = 'UpArmFkStrt%s%sMul' % (part, side)

		(self.ForearmFkStrt_Add,
		self.ForearmFkStrt_Mul) = lrr.fkStretch(
													ctrl=self.ForearmFk_Ctrl,
													target=self.WristFkCtrlZr_Grp
												)
		self.ForearmFkStrt_Add.name = 'ForearmFkStrt%s%sAdd' % (part, side)
		self.ForearmFkStrt_Mul.name = 'ForearmFkStrt%s%sMul' % (part, side)
		
		# FK Rig - Connecting to joints
		mc.parentConstraint(self.UpArmFkGmbl_Ctrl, self.UpArmFk_Jnt)
		mc.parentConstraint(self.ForearmFkGmbl_Ctrl, self.ForearmFk_Jnt)
		mc.parentConstraint(self.WristFkGmbl_Ctrl, self.WristFk_Jnt)
		
		# FK Rig - local/world setup
		(self.UpArmFkCtrlLoc_Grp,
		self.UpArmFkCtrlWor_Grp,
		self.UpArmFkCtrlWor_Grp_orientConstraint,
		self.UpArmFkCtrlZr_Grp_orientConstraint,
		self.UpArmFkCtrlZrGrpOri_Rev) = lrr.oriLocWor(
															self.UpArmFk_Ctrl,
															self.ArmFkCtrl_Grp,
															ctrlGrp,
															self.UpArmFkCtrlZr_Grp
														)
		self.UpArmFkCtrlLoc_Grp.name = 'UpArmFkCtrlLoc%s%sGrp' % (part, side)
		self.UpArmFkCtrlWor_Grp.name = 'UpArmFkCtrlWor%s%sGrp' % (part, side)
		self.UpArmFkCtrlZrGrpOri_Rev.name = 'UpArmFkCtrlZrGrpOri%s%sRev' % (part, side)

		# FK Rig - scale control
		self.Arm_Ctrl.attr('handScale') >> self.WristFk_Jnt.attr('sx')
		self.Arm_Ctrl.attr('handScale') >> self.WristFk_Jnt.attr('sy')
		self.Arm_Ctrl.attr('handScale') >> self.WristFk_Jnt.attr('sz')
		self.Arm_Ctrl.attr('handScale') >> self.WristScaOfst_Grp.attr('sx')
		self.Arm_Ctrl.attr('handScale') >> self.WristScaOfst_Grp.attr('sy')
		self.Arm_Ctrl.attr('handScale') >> self.WristScaOfst_Grp.attr('sz')

		# IK Rig
		# IK Rig - Main groups
		self.ArmIkCtrl_Grp = lrc.Null()
		self.ArmIkCtrl_Grp.name = 'ArmIkCtrl%s%sGrp' % (part, side)
		self.ArmIkCtrl_Grp.snap(self.Ctrl_Grp)
		self.ArmIkCtrl_Grp.parent(self.Ctrl_Grp)
		
		self.ArmIkJnt_Grp = lrc.Null()
		self.ArmIkJnt_Grp.name = 'ArmIkJnt%s%sGrp' % (part, side)
		self.ArmIkJnt_Grp.snap(self.Jnt_Grp)
		self.ArmIkJnt_Grp.parent(self.Jnt_Grp)

		# IK Rig - Joints
		self.UpArmIk_Jnt = lrr.jointAt(self.UpArm_Jnt)
		self.ForearmIk_Jnt = lrr.jointAt(self.Forearm_Jnt)
		self.WristIk_Jnt = lrr.jointAt(self.Wrist_Jnt)
		self.HandIk_Jnt = lrr.jointAt(self.Hand_Jnt)
		
		self.UpArmIk_Jnt.name = 'UpArmIk%s%sJnt' % (part, side)
		self.ForearmIk_Jnt.name = 'ForearmIk%s%sJnt' % (part, side)
		self.WristIk_Jnt.name = 'WristIk%s%sJnt' % (part, side)
		self.HandIk_Jnt.name = 'HandIk%s%sJnt' % (part, side)

		self.HandIk_Jnt.parent(self.WristIk_Jnt)
		self.WristIk_Jnt.parent(self.ForearmIk_Jnt)
		self.ForearmIk_Jnt.parent(self.UpArmIk_Jnt)
		self.UpArmIk_Jnt.parent(self.ArmIkJnt_Grp)
		self.HandIk_Jnt.attr('ssc').v = 0

		self.UpArmIk_Jnt.rotateOrder = 'yzx'
		self.ForearmIk_Jnt.rotateOrder = 'yzx'
		self.WristIk_Jnt.rotateOrder = 'yzx'
		self.HandIk_Jnt.rotateOrder = 'yzx'

		# IK Rig - Controllers
		self.ArmIkRoot_Ctrl = lrr.jointControl('cube')
		self.ArmIkRoot_Ctrl.name = 'ArmIkRoot%s%sCtrl' % (part, side)
		self.ArmIkRoot_Ctrl.lockHideAttrs('rx', 'ry', 'rz', 'sx', 'sy', 'sz', 'v')

		self.ArmIkRootCtrlZr_Grp = lrc.group(self.ArmIkRoot_Ctrl)
		self.ArmIkRootCtrlZr_Grp.name = 'ArmIkRootCtrlZr%s%sGrp' % (part, side)
		
		self.ArmIk_Ctrl = lrr.jointControl('cube')
		self.ArmIk_Ctrl.name = 'ArmIk%s%sCtrl' % (part, side)
		self.ArmIk_Ctrl.lockHideAttrs('sx', 'sy', 'sz', 'v')
		self.ArmIk_Ctrl.rotateOrder = 'xzy'

		self.ArmIkCtrlLock_Grp = lrc.group(self.ArmIk_Ctrl)
		self.ArmIkCtrlLock_Grp.name = 'ArmIkCtrlLock%s%sGrp' % (part, side)

		self.ArmIkCtrlZr_Grp = lrc.group(self.ArmIkCtrlLock_Grp)
		self.ArmIkCtrlZr_Grp.name = 'ArmIkCtrlZr%s%sGrp' % (part, side)
		
		self.ElbowIk_Ctrl = lrc.Control('sphere')
		self.ElbowIk_Ctrl.name = 'ElbowIk%s%sCtrl' % (part, side)
		self.ElbowIk_Ctrl.lockHideAttrs('rx', 'ry', 'rz', 'sx', 'sy', 'sz', 'v')

		self.ElbowIkCtrlZr_Grp = lrc.group(self.ElbowIk_Ctrl)
		self.ElbowIkCtrlZr_Grp.name = 'ElbowIkCtrlZr%s%sGrp' % (part, side)

		# IK Rig - Controllers - Color
		if side == '_R_':
			self.ArmIkRoot_Ctrl.color = 'blue'
			self.ArmIk_Ctrl.color = 'blue'
			self.ElbowIk_Ctrl.color = 'blue'
		else:
			self.ArmIkRoot_Ctrl.color = 'red'
			self.ArmIk_Ctrl.color = 'red'
			self.ElbowIk_Ctrl.color = 'red'

		# IK Rig - Controllers - Positioning
		self.ArmIkRootCtrlZr_Grp.snapPoint(self.UpArm_Jnt)
		
		self.ArmIkCtrlZr_Grp.snapPoint(self.Wrist_Jnt)
		self.ArmIk_Ctrl.snapOrient(self.Wrist_Jnt)
		self.ArmIk_Ctrl.freeze(t=False, r=True, s=False)
		
		self.ArmIkCon_Ctrl = lrc.addConCtrl(self.ArmIk_Ctrl)
		self.ArmIkGmbl_Ctrl = lrc.addGimbal(self.ArmIk_Ctrl)

		self.ArmIkCon_Ctrl.name = 'ArmIkCon%s%sCtrl' % (part, side)
		self.ArmIkGmbl_Ctrl.name = 'ArmIkGmbl%s%sCtrl' % (part, side)
		self.ArmIkGmbl_Ctrl.rotateOrder = 'xzy'
		
		self.ElbowIkCtrlZr_Grp.snapPoint(self.ElbowIk_Jnt)
		
		self.ArmIkRootCtrlZr_Grp.parent(self.ArmIkCtrl_Grp)
		self.ArmIkCtrlZr_Grp.parent(self.ArmIkRoot_Ctrl)
		self.ElbowIkCtrlZr_Grp.parent(self.ArmIkRoot_Ctrl)

		# IK Rig - Elbow curve
		(self.ElbowIkCtrl_Crv,
		self.ElbowIkCtrl1_Cls,
		self.ElbowIkCtrl2_Cls) = lrr.crvGuide(ctrl=self.ElbowIk_Ctrl, target=self.ForearmIk_Jnt)

		self.ElbowIkCtrl_Crv.name = 'ElbowIkCtrl%s%sCrv' % (part, side)
		self.ElbowIkCtrl1_Cls.name = 'ElbowIkCtrl1%s%sCls' % (part, side)
		self.ElbowIkCtrl2_Cls.name = 'ElbowIkCtrl2%s%sCls' % (part, side)

		lrr.renameClsElms(self.ElbowIkCtrl1_Cls)
		lrr.renameClsElms(self.ElbowIkCtrl2_Cls)

		self.ElbowIkCtrl_Crv.attr('inheritsTransform').v = 0
		self.ElbowIkCtrl_Crv.attr('overrideEnabled').v = 1
		self.ElbowIkCtrl_Crv.attr('overrideDisplayType').v = 2
		
		self.ElbowIkCtrl_Crv.parent(self.ArmIkCtrl_Grp)
		self.ElbowIkCtrl_Crv.attr('t').v = (0,0,0)
		self.ElbowIkCtrl_Crv.attr('r').v = (0,0,0)

		# IK Rig - local/world setup
		(self.ArmIkCtrlLoc_Grp,
		self.ArmIkCtrlWor_Grp,
		self.ArmIkCtrlWor_Grp_parentConstraint,
		self.ArmIkCtrlZr_Grp_parentConstraint,
		self.ArmIkCtrlZrGrpPar_Rev) = lrr.parLocWor(
														self.ArmIk_Ctrl,
														self.ArmIkRoot_Ctrl,
														ctrlGrp,
														self.ArmIkCtrlZr_Grp
													)
		self.ArmIkCtrlLoc_Grp.name = 'ArmIkCtrlLoc%s%sGrp' % (part, side)
		self.ArmIkCtrlWor_Grp.name = 'ArmIkCtrlWor%s%sGrp' % (part, side)
		self.ArmIkCtrlZrGrpPar_Rev.name = 'ArmIkCtrlZrGrpPar%s%sRev' % (part, side)

		(self.ElbowIkCtrlLoc_Grp,
		self.ElbowIkCtrlWor_Grp,
		self.ElbowIkCtrlWor_Grp_parentConstraint,
		self.ElbowIkCtrlZr_Grp_parentConstraint,
		self.ElbowIkCtrlZrGrpPar_Rev) = lrr.parLocWor(
														self.ElbowIk_Ctrl,
														self.ArmIkGmbl_Ctrl,
														ctrlGrp,
														self.ElbowIkCtrlZr_Grp
													)
		self.ElbowIkCtrlLoc_Grp.name = 'ElbowIkCtrlLoc%s%sGrp' % (part, side)
		self.ElbowIkCtrlWor_Grp.name = 'ElbowIkCtrlWor%s%sGrp' % (part, side)
		self.ElbowIkCtrlZrGrpPar_Rev.name = 'ElbowIkCtrlZrGrpPar%s%sRev' % (part, side)

		# IK Rig - IK handles
		self.ArmIk_Ikh = lrc.IkRp(sj=self.UpArmIk_Jnt, ee=self.WristIk_Jnt)
		self.HandIk_Ikh = lrc.IkRp(sj=self.WristIk_Jnt, ee=self.HandIk_Jnt)

		self.ArmIk_Ikh.name = 'ArmIk%s%sIkh' % (part, side)
		self.HandIk_Ikh.name = 'HandIk%s%sIkh' % (part, side)

		lrc.poleVectorConstraint(self.ElbowIk_Ctrl, self.ArmIk_Ikh)
		
		self.ArmIkIkh_Grp = lrc.Null()
		self.ArmIkIkhZr_Grp = lrr.groupAt(self.ArmIk_Ikh)
		self.HandIkIkhZr_Grp = lrr.groupAt(self.HandIk_Ikh)

		self.ArmIkIkh_Grp.name = 'ArmIkIkh%s%sGrp' % (part, side)
		self.ArmIkIkhZr_Grp.name = 'ArmIkIkhZr%s%sGrp' % (part, side)
		self.HandIkIkhZr_Grp.name = 'HandIkIkhZr%s%sGrp' % (part, side)
		
		self.ArmIkIkhZr_Grp.parent(self.ArmIkIkh_Grp)
		self.HandIkIkhZr_Grp.parent(self.ArmIkIkh_Grp)
		self.ArmIkIkh_Grp.parent(ikhGrp)

		# IK Rig - Pivot groups
		self.HandIkPiv_Grp = lrc.Null()
		self.ArmIkIkhPiv_Grp = lrc.Null()
		self.HandIkIkhPiv_Grp = lrc.Null()

		self.HandIkPiv_Grp.snapPoint(self.Wrist_Jnt)
		self.ArmIkIkhPiv_Grp.snap(self.ArmIkIkhZr_Grp)
		self.HandIkIkhPiv_Grp.snap(self.HandIkIkhZr_Grp)

		self.HandIkPiv_Grp.name = "HandIkPiv%s%sGrp" % (part, side)
		self.ArmIkIkhPiv_Grp.name = "ArmIkIkhPiv%s%sGrp" % (part, side)
		self.HandIkIkhPiv_Grp.name = "HandIkIkhPiv%s%sGrp" % (part, side)

		mc.pointConstraint(self.ArmIkRoot_Ctrl, self.UpArmIk_Jnt)
		self.ArmIkIkhPiv_Grp.parent(self.HandIkPiv_Grp)
		self.HandIkIkhPiv_Grp.parent(self.HandIkPiv_Grp)
		self.HandIkPiv_Grp.parent(self.ArmIkGmbl_Ctrl)
		
		mc.parentConstraint(self.ArmIkIkhPiv_Grp, self.ArmIkIkhZr_Grp)
		mc.parentConstraint(self.HandIkIkhPiv_Grp, self.HandIkIkhZr_Grp)

		self.ArmIk_Ctrl.add(ln='twist', k=True)
		self.ArmIk_Ctrl.attr('twist') >> self.ArmIk_Ikh.attr('twist')

		# IK Rig - Strt setup
		self.ArmIk_Ctrl.add(ln='__stretch__' , k=True)
		self.ArmIk_Ctrl.attr('__stretch__').set(l=True)
		self.ArmIk_Ctrl.add(ln='autoStretch' , min=0 , max=1 , k=True)
		self.ArmIk_Ctrl.add(ln='upArmStretch' , k=True)
		self.ArmIk_Ctrl.add(ln='forearmStretch' , k=True)

		self.UpArmIkJntPnt_Grp = lrc.Null()
		self.ArmIkCtrlPnt_Grp = lrc.Null()
		self.ElbowIkCtrlPnt_Grp = lrc.Null()

		self.UpArmIkJntPnt_Grp.name = 'UpArmIkJntPnt%s%sGrp' % (part, side)
		self.ArmIkCtrlPnt_Grp.name = 'ArmIkCtrlPnt%s%sGrp' % (part, side)
		self.ElbowIkCtrlPnt_Grp.name = 'ElbowIkCtrlPnt%s%sGrp' % (part, side)

		lrc.pointConstraint(self.ArmIkRoot_Ctrl, self.UpArmIkJntPnt_Grp)
		lrc.pointConstraint(self.ArmIkIkhZr_Grp, self.ArmIkCtrlPnt_Grp)
		lrc.pointConstraint(self.ElbowIk_Ctrl, self.ElbowIkCtrlPnt_Grp)
		
		self.UpArmIkJntPnt_Grp.parent(self.ArmIkRoot_Ctrl)
		self.ArmIkCtrlPnt_Grp.parent(self.ArmIkRoot_Ctrl)
		self.ElbowIkCtrlPnt_Grp.parent(self.ArmIkRoot_Ctrl)
		
		self.ArmIkAtStrt_Dist = lrc.DistanceBetween()
		self.UpArmIkLock_Dist = lrc.DistanceBetween()
		self.ForearmIkLock_Dist = lrc.DistanceBetween()

		self.ArmIkAtStrt_Dist.name = 'ArmIkAtStrt%s%sDist' % (part, side)
		self.UpArmIkLock_Dist.name = 'UpArmIkLock%s%sDist' % (part, side)
		self.ForearmIkLock_Dist.name = 'ForearmIkLock%s%sDist' % (part, side)

		self.UpArmIkJntPnt_Grp.attr('t') >> self.ArmIkAtStrt_Dist.attr('p1')
		self.ArmIkCtrlPnt_Grp.attr('t') >> self.ArmIkAtStrt_Dist.attr('p2')
		self.UpArmIkJntPnt_Grp.attr('t') >> self.UpArmIkLock_Dist.attr('p1')
		self.ElbowIkCtrlPnt_Grp.attr('t') >> self.UpArmIkLock_Dist.attr('p2')
		self.ElbowIkCtrlPnt_Grp.attr('t') >> self.ForearmIkLock_Dist.attr('p1')
		self.ArmIkCtrlPnt_Grp.attr('t') >> self.ForearmIkLock_Dist.attr('p2')

		self.ArmIkAtStrt_Cnd = lrc.Condition()
		self.ArmIkAtStrt_Mul = lrc.MultDoubleLinear()
		self.ArmIkAtStrtDiv_Mdv = lrc.MultiplyDivide()
		
		self.UpArmIkAtStrt_Mul = lrc.MultDoubleLinear()
		self.UpArmIkStrt_Mul = lrc.MultDoubleLinear()
		self.UpArmIkAtStrt_Add = lrc.AddDoubleLinear()
		self.UpArmIkAtStrt_Blend = lrc.BlendTwoAttr()
		
		self.ForearmIkAtStrt_Mul = lrc.MultDoubleLinear()
		self.ForearmIkStrt_Mul = lrc.MultDoubleLinear()
		self.ForearmIkAtStrt_Add = lrc.AddDoubleLinear()
		self.ForearmIkAtStrt_Blend = lrc.BlendTwoAttr()
		
		self.ArmIkAtStrt_Cnd.name = "ArmIkAtStrt%s%sCnd" % (part, side)
		self.ArmIkAtStrt_Mul.name = "ArmIkAtStrt%s%sMul" % (part, side)
		self.ArmIkAtStrtDiv_Mdv.name = "ArmIkAtStrtDiv%s%sMdv" % (part, side)

		self.UpArmIkAtStrt_Mul.name = "UpArmIkAtStrt%s%sMul" % (part, side)
		self.UpArmIkStrt_Mul.name = "UpArmIkStrt%s%sMul" % (part, side)
		self.UpArmIkAtStrt_Add.name = "UpArmIkAtStrt%s%sAdd" % (part, side)
		self.UpArmIkAtStrt_Blend.name = "UpArmIkAtStrt%s%sBlend" % (part, side)

		self.ForearmIkAtStrt_Mul.name = "ForearmIkAtStrt%s%sMul" % (part, side)
		self.ForearmIkStrt_Mul.name = "ForearmIkStrt%s%sMul" % (part, side)
		self.ForearmIkAtStrt_Add.name = "ForearmIkAtStrt%s%sAdd" % (part, side)
		self.ForearmIkAtStrt_Blend.name = "ForearmIkAtStrt%s%sBlend" % (part, side)

		# IK Rig - Auto stretch
		ikCtrlShp = lrc.Dag(self.ArmIk_Ctrl.shape)
		ikCtrlDist = self.ArmIkAtStrt_Dist.attr('d').value
		upArmDist = abs(self.Forearm_Jnt.attr('ty').value)
		forearmDist = abs(self.Wrist_Jnt.attr('ty').value)

		ikCtrlShp.add(ln='atStrtDiv')
		ikCtrlShp.attr('atStrtDiv').v = upArmDist + forearmDist

		ikCtrlShp.add(ln='atStrtCon')
		ikCtrlShp.attr('atStrtCon').v = upArmDist + forearmDist

		ikCtrlShp.add(ln='upArmLen')
		ikCtrlShp.attr('upArmLen').v = upArmDist

		ikCtrlShp.add(ln='forearmLen')
		ikCtrlShp.attr('forearmLen').v = forearmDist

		self.ArmIkAtStrtDiv_Mdv.attr('op').v = 2
		self.ArmIkAtStrtDiv_Mdv.attr('i1x').v = 1
		# self.ArmIkAtStrtDiv_Mdv.attr('i2x').v = abs(upArmDist + forearmDist)
		ikCtrlShp.attr('atStrtDiv') >> self.ArmIkAtStrtDiv_Mdv.attr('i2x')
		self.ArmIkAtStrtDiv_Mdv.attr('ox') >> self.ArmIkAtStrt_Mul.attr('i2')
		
		self.ArmIkAtStrt_Dist.attr('d') >> self.ArmIkAtStrt_Cnd.attr('ft')
		# self.ArmIkAtStrt_Cnd.attr('st').value = ikCtrlDist
		ikCtrlShp.attr('atStrtCon') >> self.ArmIkAtStrt_Cnd.attr('st')
		self.ArmIkAtStrt_Cnd.attr('op').value = 2
		self.ArmIkAtStrt_Cnd.attr('cfr').value = 1
		self.ArmIkAtStrt_Dist.attr('d') >> self.ArmIkAtStrt_Mul.attr('i1')
		self.ArmIkAtStrt_Mul.attr('o') >> self.ArmIkAtStrt_Cnd.attr('ctr')

		# IK Rig - Auto stretch - Upper arm
		self.ArmIkAtStrt_Cnd.attr('ocr') >> self.UpArmIkAtStrt_Mul.attr('i1')
		# self.UpArmIkAtStrt_Mul.attr('i2').value = upArmDist
		ikCtrlShp.attr('upArmLen') >> self.UpArmIkAtStrt_Mul.attr('i2')
		
		self.ArmIk_Ctrl.attr('autoStretch') >> self.UpArmIkAtStrt_Blend.attr('ab')
		# self.UpArmIkAtStrt_Blend.add(ln = 'default' , dv = upArmDist , k = True)
		# self.UpArmIkAtStrt_Blend.attr('default') >> self.UpArmIkAtStrt_Blend.last()
		ikCtrlShp.attr('upArmLen') >> self.UpArmIkAtStrt_Blend.last()
		self.UpArmIkAtStrt_Mul.attr('o') >> self.UpArmIkAtStrt_Blend.last()
		self.UpArmIkAtStrt_Blend.attr('o') >> self.UpArmIkAtStrt_Add.attr('i1')
		
		self.ArmIk_Ctrl.attr('upArmStretch') >> self.UpArmIkStrt_Mul.attr('i1')
		# self.UpArmIkStrt_Mul.attr('i2').value = upArmDist
		ikCtrlShp.attr('upArmLen') >> self.UpArmIkStrt_Mul.attr('i2')
		self.UpArmIkStrt_Mul.attr('o') >> self.UpArmIkAtStrt_Add.attr('i2')
		
		# IK Rig - Auto stretch - forearm
		self.ArmIkAtStrt_Cnd.attr('ocr') >> self.ForearmIkAtStrt_Mul.attr('i1')
		# self.ForearmIkAtStrt_Mul.attr('i2').value = forearmDist
		ikCtrlShp.attr('forearmLen') >> self.ForearmIkAtStrt_Mul.attr('i2')
		
		self.ArmIk_Ctrl.attr('autoStretch') >> self.ForearmIkAtStrt_Blend.attr('ab')
		# self.ForearmIkAtStrt_Blend.add(ln = 'default' , dv = forearmDist , k = True)
		# self.ForearmIkAtStrt_Blend.attr('default') >> self.ForearmIkAtStrt_Blend.last()
		ikCtrlShp.attr('forearmLen') >> self.ForearmIkAtStrt_Blend.last()
		self.ForearmIkAtStrt_Mul.attr('o') >> self.ForearmIkAtStrt_Blend.last()
		self.ForearmIkAtStrt_Blend.attr('o') >> self.ForearmIkAtStrt_Add.attr('i1')
		
		self.ArmIk_Ctrl.attr('forearmStretch') >> self.ForearmIkStrt_Mul.attr('i1')
		# self.ForearmIkStrt_Mul.attr('i2').value = forearmDist
		ikCtrlShp.attr('forearmLen') >> self.ForearmIkStrt_Mul.attr('i2')
		self.ForearmIkStrt_Mul.attr('o') >> self.ForearmIkAtStrt_Add.attr('i2')

		# IK lock - Controls
		self.ElbowIk_Ctrl.add(ln = 'lock' , min = 0 , max = 1 , k = True)
		
		# IK lock - Stretch, upper arm stretch
		self.UpArmIkLockLen_Mul = lrc.MultDoubleLinear()
		self.UpArmIkLock_Mul = lrc.MultDoubleLinear()
		self.UpArmIkLock_Blend = lrc.BlendTwoAttr()
		
		self.ForearmIkLockLen_Mul = lrc.MultDoubleLinear()
		self.ForearmIkLock_Mul = lrc.MultDoubleLinear()
		self.ForearmIkLock_Blend = lrc.BlendTwoAttr()

		self.UpArmIkLockLen_Mul.name = "UpArmIkLockLen%s%sMul" % (part, side)
		self.UpArmIkLock_Mul.name = "UpArmIkLock%s%sMul" % (part, side)
		self.UpArmIkLock_Blend.name = "UpArmIkLock%s%sBlend" % (part, side)

		self.ForearmIkLockLen_Mul.name = "ForearmIkLockLen%s%sMul" % (part, side)
		self.ForearmIkLock_Mul.name = "ForearmIkLock%s%sMul" % (part, side)
		self.ForearmIkLock_Blend.name = "ForearmIkLock%s%sBlend" % (part, side)
		print upArmDist, '...upArmDist..6'
		self.UpArmIkLock_Dist.attr('d') >> self.UpArmIkLockLen_Mul.attr('i1')
		self.UpArmIkLockLen_Mul.attr('i2').value = abs(1/upArmDist)
		self.UpArmIkLockLen_Mul.attr('o') >> self.UpArmIkLock_Mul.attr('i1')
		self.UpArmIkLock_Mul.attr('i2').value = upArmDist
		self.UpArmIkAtStrt_Add.attr('o') >> self.UpArmIkLock_Blend.last()
		self.UpArmIkLock_Mul.attr('o') >> self.UpArmIkLock_Blend.last()
		self.ElbowIk_Ctrl.attr('lock') >> self.UpArmIkLock_Blend.attr('ab')
		# self.UpArmIkLock_Blend.attr('o') >> self.ForearmIk_Jnt.attr('ty')
		
		self.ForearmIkLock_Dist.attr('d') >> self.ForearmIkLockLen_Mul.attr('i1')
		self.ForearmIkLockLen_Mul.attr('i2').value = abs(1/forearmDist)
		self.ForearmIkLockLen_Mul.attr('o') >> self.ForearmIkLock_Mul.attr('i1')
		self.ForearmIkLock_Mul.attr('i2').value = forearmDist
		self.ForearmIkAtStrt_Add.attr('o') >> self.ForearmIkLock_Blend.last()
		self.ForearmIkLock_Mul.attr('o') >> self.ForearmIkLock_Blend.last()
		self.ElbowIk_Ctrl.attr('lock') >> self.ForearmIkLock_Blend.attr('ab')
		# self.ForearmIkLock_Blend.attr('o') >> self.WristIk_Jnt.attr('ty')

		# IK rig - Auto stretch - Finalize
		self.UpArmIkFinStrtVal_Mul = lrc.MultDoubleLinear()
		self.ForearmIkFinStrtVal_Mul = lrc.MultDoubleLinear()

		self.UpArmIkFinStrtVal_Mul.name = 'UpArmIkFinStrtVal%s%sMul' % (part, side)
		self.ForearmIkFinStrtVal_Mul.name = 'ForearmIkFinStrtVal%s%sMul' % (part, side)

		if self.ForearmIk_Jnt.attr('ty').v > 0 :
			self.UpArmIkFinStrtVal_Mul.attr('i2').v = 1
		else :
			self.UpArmIkFinStrtVal_Mul.attr('i2').v = -1

		if self.WristIk_Jnt.attr('ty').v > 0 :
			self.ForearmIkFinStrtVal_Mul.attr('i2').v = 1
		else :
			self.ForearmIkFinStrtVal_Mul.attr('i2').v = -1
		
		self.UpArmIkLock_Blend.attr('o') >> self.UpArmIkFinStrtVal_Mul.attr('i1')
		self.UpArmIkFinStrtVal_Mul.attr('o') >> self.ForearmIk_Jnt.attr('ty')

		self.ForearmIkLock_Blend.attr('o') >> self.ForearmIkFinStrtVal_Mul.attr('i1')
		self.ForearmIkFinStrtVal_Mul.attr('o') >> self.WristIk_Jnt.attr('ty')
		
		# FK/IK blending
		self.ArmFkIk_Rev = lrc.Reverse()
		self.ArmFkIk_Rev.name = 'ArmFkIk%s%sRev' % (part, side)

		self.Arm_Ctrl.add(ln = 'fkIk' , min = 0 , max = 1 , k = True)
		self.Arm_Ctrl.attr('fkIk') >> self.ArmFkIk_Rev.attr('ix')
		self.Arm_Ctrl.attr('fkIk') >> self.ArmIkCtrl_Grp.attr('v')
		self.ArmFkIk_Rev.attr('ox') >> self.ArmFkCtrl_Grp.attr('v')
		
		upArmJntParCons = lrc.parentConstraint(self.UpArmFk_Jnt , self.UpArmIk_Jnt , self.UpArm_Jnt)
		forermJntParCons = lrc.parentConstraint(self.ForearmFk_Jnt , self.ForearmIk_Jnt , self.Forearm_Jnt)
		wristJntParCons = lrc.parentConstraint(self.WristFk_Jnt , self.WristIk_Jnt , self.Wrist_Jnt)
		handJntParCons = lrc.parentConstraint(self.HandFk_Jnt , self.HandIk_Jnt , self.Hand_Jnt)
		
		self.Arm_Ctrl.attr('fkIk') >> upArmJntParCons.attr('w1')
		self.Arm_Ctrl.attr('fkIk') >> forermJntParCons.attr('w1')
		self.Arm_Ctrl.attr('fkIk') >> wristJntParCons.attr('w1')
		self.Arm_Ctrl.attr('fkIk') >> handJntParCons.attr('w1')
		
		self.ArmFkIk_Rev.attr('ox') >> upArmJntParCons.attr('w0')
		self.ArmFkIk_Rev.attr('ox') >> forermJntParCons.attr('w0')
		self.ArmFkIk_Rev.attr('ox') >> wristJntParCons.attr('w0')
		self.ArmFkIk_Rev.attr('ox') >> handJntParCons.attr('w0')
		
		# Grouping
		self.Ctrl_Grp.parent(ctrlGrp)
		self.Jnt_Grp.parent(jntGrp)

		# Ribbon
		self.ArmRbnCtrl_Grp = lrc.Null()
		self.ArmRbnCtrl_Grp.name = 'ArmRbnCtrl%s%sGrp' % (part, side)

		self.ArmRbnCtrl_Grp.snap(self.Ctrl_Grp)

		rbnAx = 'y-'
		rbnAim = (0, -1, 0)
		rbnUp = (0, 0, 1)
		rbnAmp = 1

		if side == '_R_' :
			rbnUp = (0, 0, -1)
			rbnAmp = -1

		# Ribbon Control
		self.ArmRbn_Ctrl = lrc.Control('plus')
		self.ArmRbn_Ctrl.name = 'ArmRbn%s%sCtrl' % (part, side)
		self.ArmRbn_Ctrl.lockHideAttrs('rx', 'ry', 'rz', 'sx', 'sy', 'sz', 'v')

		self.ArmRbnCtrlZr_Grp = lrc.group(self.ArmRbn_Ctrl)
		self.ArmRbnCtrlZr_Grp.name = 'ArmRbnCtrlZr%s%sGrp' % (part, side)

		mc.pointConstraint(self.Forearm_Jnt, self.ArmRbnCtrlZr_Grp)
		mc.orientConstraint(self.UpArm_Jnt, self.ArmRbnCtrlZr_Grp, mo=True)

		# Ribbon Control - Color
		if side == '_R_':
			self.ArmRbn_Ctrl.color = 'blue'
		else:
			self.ArmRbn_Ctrl.color = 'red'
		
		# Ribbon - Upper Arm
		if ribbon :
			self.UpArmRbn = lrb.RibbonIkHi(
												size=self.UpArmLen,
												ax=rbnAx,
												part='UpArm%s' % part,
												side=side.replace('_','')
											)
		else :
			self.UpArmRbn = lrb.RibbonIkLow(
												size=self.UpArmLen,
												ax=rbnAx,
												part='UpArm%s' % part,
												side=side.replace('_','')
											)
		
		self.UpArmRbn.Ctrl_Grp.snapPoint(self.UpArm_Jnt)
		mc.delete(
					mc.aimConstraint(
										self.Forearm_Jnt,
										self.UpArmRbn.Ctrl_Grp,
										aim=rbnAim, u=rbnUp,
										wut='objectrotation',
										wuo=self.UpArm_Jnt,
										wu=(0,0,1)
									)
				)
		mc.pointConstraint(self.ArmRbn_Ctrl, self.UpArmRbn.RbnEnd_Ctrl)

		self.UpArmRbn.RbnRootTws_Grp.rotateOrder = 'xzy'
		self.UpArmRbn.RbnEndTws_Grp.rotateOrder = 'xzy'
		
		lrc.parentConstraint(
									self.UpArm_Jnt,
									self.UpArmRbn.Ctrl_Grp,
									mo=True
								)
		mc.parentConstraint(
								self.UpArmNr_Jnt,
								self.UpArmRbn.RbnRootTwsZr_Grp
							)
		mc.parentConstraint(
								self.UpArm_Jnt,
								self.UpArmRbn.RbnRootTws_Grp
							)

		mc.parentConstraint(
								self.ForearmNr_Jnt,
								self.UpArmRbn.RbnEndTwsZr_Grp
							)
		mc.parentConstraint(
								self.Forearm_Jnt,
								self.UpArmRbn.RbnEndTws_Grp
							)
		upArmRbnShp = lrc.Dag(self.UpArmRbn.Rbn_Ctrl.shape)
		upArmRbnShp.attr('rootTwistAmp').v = 1 * rbnAmp
		upArmRbnShp.attr('endTwistAmp').v = -1 * rbnAmp

		# Ribbon - Forearm
		if ribbon :
			self.ForearmRbn = lrb.RibbonIkHi(
												size=self.ForearmLen,
												ax=rbnAx,
												part='Forearm%s' % part,
												side=side.replace('_','')
											)
		else :
			self.ForearmRbn = lrb.RibbonIkLow(
												size=self.ForearmLen,
												ax=rbnAx,
												part='Forearm%s' % part,
												side=side.replace('_','')
											)

		self.ForearmRbn.Ctrl_Grp.snapPoint(self.Forearm_Jnt)
		mc.delete(mc.aimConstraint(self.Wrist_Jnt, self.ForearmRbn.Ctrl_Grp, aim=rbnAim, u=rbnUp, wut='objectrotation', wuo=self.Forearm_Jnt, wu=(0,0,1)))
		mc.parentConstraint(self.Forearm_Jnt, self.ForearmRbn.Ctrl_Grp, mo=True)
		mc.pointConstraint(self.ArmRbn_Ctrl, self.ForearmRbn.RbnRoot_Ctrl)
		mc.pointConstraint(self.Wrist_Jnt, self.ForearmRbn.RbnEnd_Ctrl)
		
		armRbnShp = lrc.Dag(self.ForearmRbn.Rbn_Ctrl.shape)
		armRbnShp.attr('rootTwistAmp').v = 1 * rbnAmp
		armRbnShp.attr('endTwistAmp').v = -1 * rbnAmp
		
		self.ForearmRbn.RbnRootTwsZr_Grp.snap(self.Forearm_Jnt)
		self.ForearmRbn.RbnEndTwsZr_Grp.snap(self.Wrist_Jnt)

		mc.parentConstraint(
								self.Forearm_Jnt,
								self.ForearmRbn.RbnEndTwsZr_Grp,
								mo=True
							)
		mc.parentConstraint(
								self.Wrist_Jnt,
								self.ForearmRbn.RbnEndTws_Grp,
								mo=True
							)
		
		self.ForearmRbn.RbnRootTws_Grp.rotateOrder = 'xzy'
		self.ForearmRbn.RbnEndTws_Grp.rotateOrder = 'xzy'

		# Ribbon - Grouping
		self.ArmRbnCtrlZr_Grp.parent(self.ArmRbnCtrl_Grp)
		self.UpArmRbn.Ctrl_Grp.parent(self.ArmRbnCtrl_Grp)
		self.UpArmRbn.Skin_Grp.parent(skinGrp)
		self.UpArmRbn.Jnt_Grp.parent(jntGrp)
		self.ForearmRbn.Ctrl_Grp.parent(self.ArmRbnCtrl_Grp)
		self.ForearmRbn.Skin_Grp.parent(skinGrp)
		self.ForearmRbn.Jnt_Grp.parent(jntGrp)
		self.ArmRbnCtrl_Grp.parent(self.Ctrl_Grp)

		if ribbon :
			self.UpArmRbn.Still_Grp.parent(stillGrp)
			self.ForearmRbn.Still_Grp.parent(stillGrp)

		self.UpArmRbn.RbnRoot_Ctrl.lockHideAttrs('tx', 'ty', 'tz')
		self.UpArmRbn.RbnEnd_Ctrl.lockHideAttrs('tx', 'ty', 'tz')
		self.ForearmRbn.RbnRoot_Ctrl.lockHideAttrs('tx', 'ty', 'tz')
		self.ForearmRbn.RbnEnd_Ctrl.lockHideAttrs('tx', 'ty', 'tz')
		
		self.UpArmRbn.RbnRoot_Ctrl.hide()
		self.UpArmRbn.RbnEnd_Ctrl.hide()
		self.ForearmRbn.RbnRoot_Ctrl.hide()
		self.ForearmRbn.RbnEnd_Ctrl.hide()
		
		# Set default
		self.Arm_Ctrl.attr('fkIk').v = 0
		self.ArmIk_Ctrl.attr('localWorld').v = 1
		self.UpArmFk_Ctrl.attr('localWorld').v = 1

		self.UpArmRbn.Rbn_Ctrl.attr('autoTwist').v = 1
		self.ForearmRbn.Rbn_Ctrl.attr('autoTwist').v = 1
