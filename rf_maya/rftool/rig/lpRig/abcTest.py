import os
import sys
sys.path.append(r'Y:\peck\apps')

import alembic

# print 'alembic'
# print dir(alembic)

# print 'alembic.Abc'
# print dir(alembic.Abc)

kWrapExisting = alembic.Abc.WrapExistingFlag.kWrapExisting

abcFlPath = os.path.normpath(r'Y:\peck\b.abc')

iarch = alembic.Abc.IArchive(abcFlPath)

top = iarch.getTop()

def getChdnRecur(obj, result=[]):

	chdn = obj.children

	if chdn:
		for chd in chdn:
			result.append(chd)
			getChdnRecur(chd, result)

	return result

results = getChdnRecur(top)

for each in results:
	
	name = each.getName()
	
	type_ = each.getMetaData()
	
	if alembic.AbcGeom.IPolyMesh.matches(type_):
		
		# ['__class__', '__delattr__', '__dict__', '__doc__', '__format__', 
		# '__getattribute__', '__hash__', '__init__', '__instance_size__', 
		# '__module__', '__new__', '__nonzero__', '__reduce__', '__reduce_ex__', 
		# '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', 
		# '__weakref__', 'children', 'getArchive', 'getChild', 'getChildHeader', 
		# 'getChildrenHash', 'getFullName', 'getHeader', 'getMetaData', 'getName', 
		# 'getNumChildren', 'getParent', 'getProperties', 'getPropertiesHash', 'getSchema', 
		# 'getSchemaObjTitle', 'getSchemaTitle', 'instanceSourcePath', 'isChildInstance', 
		# 'isInstanceDescendant', 'isInstanceRoot', 'matches', 'reset', 'valid']
		iMesh = alembic.AbcGeom.IPolyMesh(each, kWrapExisting)
		
		# ['__class__', '__delattr__', '__dict__', '__doc__', '__format__', 
		# '__getattribute__', '__hash__', '__init__', '__instance_size__', 
		# '__module__', '__new__', '__nonzero__', '__reduce__', '__reduce_ex__', 
		# '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', 
		# '__weakref__', 'getArbGeomParams', 'getChildBoundsProperty', 'getFaceCountsProperty', 
		# 'getFaceIndicesProperty', 'getFaceSet', 'getFaceSetNames', 'getNormalsParam', 
		# 'getNumSamples', 'getPositionsProperty', 'getSelfBoundsProperty', 'getTimeSampling', 
		# 'getTopologyVariance', 'getUVsParam', 'getUserProperties', 'getValue', 
		# 'getVelocitiesProperty', 'hasFaceSet', 'init', 'isConstant', 'reset', 'valid']
		meshSc = iMesh.getSchema()
		
		# ['__class__', '__delattr__', '__dict__', '__doc__', '__format__', 
		# '__getattribute__', '__hash__', '__init__', '__instance_size__', 
		# '__module__', '__new__', '__nonzero__', '__reduce__', '__reduce_ex__', 
		# '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', 
		# '__weakref__', 'getFaceCounts', 'getFaceIndices', 'getPositions', 'getSelfBounds', 
		# 'getVelocities', 'reset', 'valid']
		meshVal = meshSc.getValue()
		# print len(meshVal.getPositions())
		
		# ['__class__', '__delattr__', '__dict__', '__doc__', '__format__', 
		# '__getattribute__', '__hash__', '__init__', '__instance_size__', 
		# '__module__', '__new__', '__nonzero__', '__reduce__', '__reduce_ex__', 
		# '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', 
		# '__weakref__', 'getArrayExtent', 'getDataType', 'getExpandedValue', 
		# 'getHeader', 'getIndexProperty', 'getIndexedValue', 'getInterpretation', 
		# 'getMetaData', 'getName', 'getNumSamples', 'getParent', 'getScope', 
		# 'getTimeSampling', 'getValueProperty', 'isConstant', 'isIndexed', 'matches', 
		# 'reset', 'valid']
		uvParam = meshSc.getUVsParam()
		
		# ['__class__', '__delattr__', '__dict__', '__doc__', '__format__', '__getattribute__', 
		# '__hash__', '__init__', '__instance_size__', '__module__', '__new__', '__reduce__', 
		# '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', 
		# '__weakref__', 'getIndices', 'getScope', 'getVals', 'isIndexed', 'reset', 'valid']
		uv = uvParam.getIndexedValue()
		print len(uv.getVals())