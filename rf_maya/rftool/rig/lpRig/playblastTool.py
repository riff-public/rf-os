import os
from functools import partial
import subprocess

import maya.cmds as mc
import maya.mel as mm

USR_HOME = os.path.normpath(os.path.expanduser('~'))
TMP_DIR = os.path.join(USR_HOME, 'pkPlayblastTmp')
FFMPEG = r'C:\ffmpeg\bin\ffmpeg.exe'
QT = r'C:\Program Files (x86)\QuickTime\QuickTimePlayer.exe'

def run():

	ui = PlayblastTool()
	ui.show()

def removeTemp():
	fls = [os.path.join(TMP_DIR, f) for f in os.listdir(TMP_DIR)
			if os.path.isfile(os.path.join(TMP_DIR, f))]
	if not fls:
		return False

	[os.remove(f) for f in fls]

def playImgSeq(w=1280, h=720, fps=24, suffix=''):

	print 'Removing temp files.'
	removeTemp()

	sf = int(mc.playbackOptions(q=True, min=True))
	ef = int(mc.playbackOptions(q=True, max=True))

	tmpPb = os.path.join(TMP_DIR, 'tmp')

	pbArgs = {
				'format': 'image',
				'filename': tmpPb,
				'sequenceTime': 0,
				'forceOverwrite': True,
				'clearCache': True,
				'viewer': False,
				'showOrnaments': True,
				'fp': 4,
				'percent': 100,
				'quality': 100,
				'compression': 'jpg',
				'width': int(w),
				'height': int(h),
				'offScreen': True
			}

	mc.playblast(**pbArgs)

	sl = os.path.normpath(mc.file(q=True, l=True)[0])
	scnFldPath, scnNmExt = os.path.split(sl)
	scnNm, scnExt = os.path.splitext(scnNmExt)

	outputNmExt = '%s%s.%s' % (scnNm, suffix, 'mp4')
	output = os.path.join(scnFldPath, outputNmExt)

	if os.path.exists('%s.mov' % output):
		print 'Removing %s' % '%s.mov' % output
		os.remove('%s.mov' % output)

	print 'Converting playblast file to {output}'.format(output=output)
	cmd = '"{ffmpeg}" -start_number {sf} -f image2 \
			-i "{tmpPb}.%04d.jpg" -r {fps} '.format(ffmpeg=FFMPEG, sf=sf, tmpPb=tmpPb, fps=fps)
	cmd += '-vcodec libx264 -b:v 9600k {output}'.format(output=output)
	subprocess.call(cmd)
	
	removeTemp()
	
	# if os.path.exists(QT):
	# 	subprocess.Popen([QT, output])

class PlayblastTool(object):

	def __init__(self):

		self.ui = 'pkPlayblastTool'
		self.win = '%sWin' % self.ui

	def show(self):

		if mc.window(self.win, exists=True):
			mc.deleteUI(self.win)

		mc.window(self.win, t='pkPlayblastTool')
		self.mainCl = mc.columnLayout(adj=True)

		self.fpsOm = mc.optionMenu(l='fps')
		mc.menuItem(l='24')
		mc.menuItem(l='30')

		self.resOm = mc.optionMenu(l='resolution')
		mc.menuItem(l='1280x720')
		mc.menuItem(l='960x540')
		mc.menuItem(l='1920x1080')

		mc.separator()

		self.playBut = mc.button(l='shoot', c=partial(self.shoot), h=50)

		mc.showWindow(self.win)

	def shoot(self, *args):

		sl = os.path.normpath(mc.file(q=True, l=True)[0])
		ext = os.path.splitext(sl)[-1]
		output = sl.replace(ext, '.mov')
		tmpPb = os.path.join(TMP_DIR, 'tmp.avi')
		fps = mc.optionMenu(self.fpsOm, q=True, v=True)
		width, height = mc.optionMenu(self.resOm, q=True, v=True).split('x')

		pbArgs = {
					'format': 'avi',
					'filename': tmpPb,
					'sequenceTime': 0,
					'forceOverwrite': True,
					'clearCache': True,
					'viewer': False,
					'showOrnaments': True,
					'fp': 4,
					'percent': 100,
					'quality': 100,
					'compression': 'none',
					'width': int(width),
					'height': int(height),
					'offScreen': True
				}

		pbSl = mm.eval('$tmpVar=$gPlayBackSlider')

		if mc.timeControl(pbSl, q=True, s=True):
			pbArgs['sound'] = mc.timeControl(pbSl, q=True, s=True)

		ow = mc.getAttr('defaultResolution.width')
		oh = mc.getAttr('defaultResolution.height')

		print 'Saving temp playbalst file to {tmpPb}'.format(tmpPb=tmpPb)
		mc.playblast(**pbArgs)
		
		print 'Converting playblast file to {output}'.format(output=output)
		cmd = '"{ffmpeg}" -y -i "{tmpPb}" -r {fps} '.format(ffmpeg=FFMPEG, tmpPb=tmpPb, fps=fps)
		cmd += '-vcodec libx264 -vprofile baseline -crf 22 -bf 0 -pix_fmt yuv420p -f mov {output}'.format(output=output)
		subprocess.call(cmd)

		removeTemp()
		