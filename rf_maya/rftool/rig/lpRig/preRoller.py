import os
import pymel.core as pmc
import re


DEF_VALS = {'sx': 1, 'sy': 1, 'sz': 1,
			'scaleX': 1, 'scaleY': 1, 'scaleZ': 1,
			'footScale': 1, 'handScale': 1}

SKIP_ATTRS = ['localWorld', 'autoStretch']

def is_ribbon_tip(obj):
	"""Checks if current obj is a ribbon root or end.

	:param obj: PyNode
	"""
	exp = r'(.+Rbn.+[Root|End].+Ctrl)'
	matches = re.search(exp, str(obj.name()))

	if matches:
		return True
	else:
		return False

def is_ctrl(obj):
	"""Checks if current obj is a controller, returns True.

	:param obj: PyNode
	"""
	if not obj.name().endswith('_Ctrl'):
		return False

	if is_ribbon_tip(obj):
		return False

	shape = obj.getShape()

	if not shape:
		return False

	if isinstance(shape, pmc.nodetypes.NurbsCurve):
		return True
	else:
		return False

def get_all_ctrls(ns=''):
	"""Gets all controllers of the asset that has given namespace.

	:param ns: A name space of an asset i.e. 'Susan_Hi:'
	"""
	ctrlgrp = '%sPlace_Ctrl' % ns

	if not pmc.objExists(ctrlgrp):
		return None

	ctrls = [x for x in pmc.listRelatives(ctrlgrp, ad=True, type='transform')
				if is_ctrl(x)]
	ctrls.append(pmc.PyNode('%sPlace_Ctrl' % ns))
	return ctrls

def set_or_add_key_to_all_channels(ctrl, frames=[101]):
	"""Sets keyframe to unkeyed attributes of given controller at given frame.

	:param ctrl: PyNode
	:param frames: A list of frame numbers
	"""
	keyttrs = pmc.listAttr(ctrl, k=True)

	for keyttr in keyttrs:
		keycount = pmc.keyframe(ctrl.attr(keyttr), q=True, keyframeCount=True)

		if not keycount:
			pmc.setKeyframe(ctrl.attr(keyttr), t=frames)
		else:
			pmc.setKeyframe(ctrl.attr(keyttr), t=frames, i=True)

def set_default_key_to_all_channels(ctrl, frames=[101]):
	"""Sets default keyframe value to all channels of given contrller at given frame.

	:param ctrl: PyNode
	:param frames: A list of frame numbers
	"""
	keyattrs = pmc.listAttr(ctrl, k=True)

	for keyattr in keyattrs:

		if keyattr in SKIP_ATTRS:
			continue

		if ctrl.attr(keyattr).get(l=True):
			continue

		val = 0
		if keyattr in DEF_VALS.keys():
			val = DEF_VALS[keyattr]

		pmc.setKeyframe(ctrl.attr(keyattr), v=val, t=frames)

	return True

def do_preroll_to_selected():
	"""Do the pre-rolls for all selected assets.
	"""
	sels = pmc.ls(sl=True)
	nses = []

	for sel in sels:
		if not ':' in sel.name():
			continue

		currns = '%s:' % ':'.join(sel.name().split(':')[:-1])
		if not currns in nses:
			nses.append(currns)

	for ns in nses:
		do_preroll(ns)

def has_preroll(ns=''):
	"""If the asset with given namespace has preroll, return True,
	else return False.
	
	:param ns: Namespace as a string, ':' need to be included.
	"""
	ctrls = get_all_ctrls(ns)

	if not ctrls:
		return False

	keys = pmc.keyframe(ctrls, time=(0, 100), q=True, tc=True)
	if not keys:
		return False

	tcs = set(keys)
	
	return set([0.0, 5.0, 25.0, 75.0, 90.0]) <= tcs

def do_preroll(ns=''):
	"""Do the pre-rolls for the asset that has given namespace.

	:param ns: Namespace as a string, ':' need to be included.
	"""
	ctrls = get_all_ctrls(ns)

	if not ctrls:
		return False

	for ctrl in ctrls:

		set_or_add_key_to_all_channels(ctrl, [101])

		pmc.cutKey(ctrl, t=(0, 100))

		set_or_add_key_to_all_channels(ctrl, [75, 90])
		set_default_key_to_all_channels(ctrl, [0, 5, 25])

	# Positioning placement controller.
	attrs = ['tx', 'ty', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz']
	for attr in attrs:
		pmc.setKeyframe(ctrls[-1].attr(attr), v=ctrls[-1].attr(attr).get(), t=[5, 25])

	armctrls = pmc.ls('%sUpArm*Fk_*_Ctrl' % ns)
	if armctrls:
		pmc.setKeyframe(armctrls, at='rz', v=-45, t=[5, 25])

	return True

def do_preroll_to_all_chars():
	"""Do the pre-rolls for all characters.
	"""
	do_types = ['Character']

	refs = pmc.listReferences(namespaces=True)

	for ref in refs:
		currpath = os.path.normpath(ref[1].path)
		pathtokens = currpath.split(os.sep)

		if pathtokens[2] in do_types:
			
			currns = '%s:' % ref[0]
			
			if has_preroll(currns):
				print '%s already has prerolls.' % currns
				continue
			
			do_preroll(currns)
			print 'Pre-rolls for %s has been made.' % ref[1].refNode

	return True
