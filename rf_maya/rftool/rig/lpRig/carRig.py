import maya.cmds as mc
import maya.mel as mm

import lpRig.core as lrc
import lpRig.rigTools as lrr

class ChassisRig(object):
	"""
	Contractor: Chas_Jnt
	"""
	def __init__(
					self,
					parent='Root_Jnt',
					parentCtrl='Root_Ctrl',
					ctrlGrp='',
					tmpJnt='',
					part='',
					side=''
				):

		# Side definition
		if side:
			side = '_%s_' % side
		else:
			side = '_'

		# Color difinition
		col = 'yellow'
		darkCol = 'softYellow'
		if side == '_R_':
			col = 'blue'
			darkCol = 'darkBlue'
		elif side == '_L_':
			col = 'red'
			darkCol = 'darkRed'

		parentCtrl = lrc.Dag(parentCtrl.name)

		self.Chas_Jnt = lrc.Dag(tmpJnt)
		self.Chas_Jnt.attr('ssc').v = 0

		# Main group
		self.Ctrl_Grp = lrc.Null()
		self.Ctrl_Grp.name = 'Chas%sCtrl%sGrp' % (part, side)
		self.Ctrl_Grp.parent(ctrlGrp)

		mc.parentConstraint(parent, self.Ctrl_Grp, mo=True)
		mc.scaleConstraint(parent, self.Ctrl_Grp, mo=True)

		# Add controllers to parent controller.
		if not parentCtrl.attr('__shake__').exists:
			parentCtrl.add(ln='__shake__', dv=0, k=True)
			parentCtrl.attr('__shake__').lock = True

		if not parentCtrl.attr('shake').exists:
			parentCtrl.add(ln='shake', dv=0, k=True)

		if not parentCtrl.attr('shakeAmp').exists:
			parentCtrl.add(ln='shakeAmp', dv=5, k=True)

		if not parentCtrl.attr('shakeFreq').exists:
			parentCtrl.add(ln='shakeFreq', dv=1, k=True)

		# Main control
		self.Chas_Ctrl = lrc.Control('circle')
		self.Chas_Ctrl.lockHideAttrs('v')
		self.Chas_Ctrl.color = darkCol
		self.Chas_Ctrl.name = 'Chas%s%sCtrl' % (part, side)

		self.ChasGmbl_Ctrl = lrc.addGimbal(self.Chas_Ctrl)
		self.ChasGmbl_Ctrl.name = 'Chas%sGmbl%sCtrl' % (part, side)
		
		self.ChasCtrlRot_Grp = lrc.group(self.Chas_Ctrl)
		self.ChasCtrlRot_Grp.name = 'Chas%sCtrlRot%sGrp' % (part, side)
		
		self.ChasCtrlZr_Grp = lrc.group(self.ChasCtrlRot_Grp)
		self.ChasCtrlZr_Grp.name = 'Chas%sCtrlZr%sGrp' % (part, side)
		
		self.ChasCtrlZr_Grp.snap(self.Chas_Jnt)
		self.ChasCtrlZr_Grp.parent(self.Ctrl_Grp)

		# Shake rig
		self.ChasShake_Frac = lrc.Fractal()
		self.ChasShake_Frac.name = 'Chas%sShake%sFrac' % (part, side)

		self.ChasShake_Frac.attr('animated').v = 1
		parentCtrl.attr('shakeAmp') >> self.ChasShake_Frac.attr('a')

		# Shake On/Off
		self.ChasShake_Cnd = lrc.Condition()
		self.ChasShake_Cnd.name = 'Chas%sShake%sCnd' % (part, side)

		parentCtrl.attr('shake') >> self.ChasShake_Cnd.attr('ft')
		self.ChasShake_Cnd.attr('oc') >> self.ChasShake_Frac.attr('colorGain')

		# Shake frequency
		self.ChasShake_Mdv = lrc.MultiplyDivide()
		self.ChasShake_Mdv.name = 'Chas%sShake%sMdv' % (part, side)

		self.ChasShakeFreq_Mdv = lrc.MultiplyDivide()
		self.ChasShakeFreq_Mdv.name = 'Chas%sShakeFreq%sMdv' % (part, side)

		parentCtrl.attr('shake') >> self.ChasShake_Mdv.attr('i1x')
		parentCtrl.attr('shakeFreq') >> self.ChasShakeFreq_Mdv.attr('i1x')
		self.ChasShakeFreq_Mdv.attr('i2x').v = 0.1
		self.ChasShakeFreq_Mdv.attr('ox') >> self.ChasShake_Mdv.attr('i2x')
		self.ChasShake_Mdv.attr('ox') >> self.ChasShake_Frac.attr('time')

		# Shake remap to -+
		self.ChasShake_Rem = lrc.RemapValue()
		self.ChasShake_Rem.name = 'Chas%sShake%sRem' % (part, side)

		self.ChasShakeAmpMin_Mdv = lrc.MultiplyDivide()
		self.ChasShakeAmpMin_Mdv.name = 'Chas%sShakeAmpMin%sMdv' % (part, side)

		parentCtrl.attr('shakeAmp') >> self.ChasShakeAmpMin_Mdv.attr('i1x')
		self.ChasShakeAmpMin_Mdv.attr('i2x').v = -1

		parentCtrl.attr('shakeAmp') >> self.ChasShake_Rem.attr('inputMax')
		parentCtrl.attr('shakeAmp') >> self.ChasShake_Rem.attr('outputMax')
		self.ChasShakeAmpMin_Mdv.attr('ox') >> self.ChasShake_Rem.attr('inputMin')
		self.ChasShakeAmpMin_Mdv.attr('ox') >> self.ChasShake_Rem.attr('outputMin')

		self.ChasShake_Frac.attr('outColorR') >> self.ChasShake_Rem.attr('inputValue')
		self.ChasShake_Rem.attr('outValue ') >> self.ChasCtrlRot_Grp.attr('rz')

		# Connect to joint
		mc.parentConstraint(self.Chas_Ctrl, self.Chas_Jnt)
		mc.scaleConstraint(self.Chas_Ctrl, self.Chas_Jnt)

class WheelRig(object):
	"""
	Contractor: Wheel_Jnt
	Contractor: WheelTip_Jnt
	"""
	def __init__(
					self,
					parent='Chas_Jnt',
					parentCtrl='Root_Ctrl',
					chasRig=None,
					ctrlGrp='',
					tmpJnt=['', ''],
					part='',
					side=''
				):

		# Side definition
		if side:
			side = '_%s_' % side
		else:
			side = '_'

		# Color difinition
		col = 'yellow'
		darkCol = 'softYellow'
		if side == '_R_':
			col = 'blue'
			darkCol = 'darkBlue'
		elif side == '_L_':
			col = 'red'
			darkCol = 'darkRed'

		parentCtrl = lrc.Dag(parentCtrl.name)

		self.Wheel_Jnt = lrc.Dag(tmpJnt[0])
		self.WheelTip_Jnt = lrc.Dag(tmpJnt[1])

		self.Wheel_Jnt.attr('ssc').v = 0

		rad = lrc.distance(self.Wheel_Jnt, self.WheelTip_Jnt)

		# Main group
		self.Ctrl_Grp = lrc.Null()
		self.Ctrl_Grp.name = 'Wheel%sCtrl%sGrp' % (part, side)
		self.Ctrl_Grp.parent(ctrlGrp)

		mc.parentConstraint(parent, self.Ctrl_Grp, mo=True)
		mc.scaleConstraint(parent, self.Ctrl_Grp, mo=True)

		# Main control
		self.Wheel_Ctrl = lrc.Control('circle')
		self.Wheel_Ctrl.lockHideAttrs('v')
		self.Wheel_Ctrl.color = col
		self.Wheel_Ctrl.name = 'Wheel%s%sCtrl' % (part, side)
				
		self.WheelGmbl_Ctrl = lrc.addGimbal(self.Wheel_Ctrl)
		self.WheelGmbl_Ctrl.name = 'Wheel%sGmbl%sCtrl' % (part, side)
		
		self.WheelCtrlRot_Grp = lrc.group(self.Wheel_Ctrl)
		self.WheelCtrlRot_Grp.name = 'Wheel%sCtrlRot%sGrp' % (part, side)
		
		self.WheelCtrlStr_Grp = lrc.group(self.WheelCtrlRot_Grp)
		self.WheelCtrlStr_Grp.name = 'Wheel%sCtrlStr%sGrp' % (part, side)
		
		self.WheelCtrlZr_Grp = lrc.group(self.WheelCtrlStr_Grp)
		self.WheelCtrlZr_Grp.name = 'Wheel%sCtrlZr%sGrp' % (part, side)
		
		self.WheelCtrlZr_Grp.snap(self.Wheel_Jnt)
		self.WheelCtrlZr_Grp.parent(self.Ctrl_Grp)

		self.Wheel_Ctrl.add(ln='wheelRot', dv=1, min=0, max=1, k=True)
		self.Wheel_Ctrl.add(ln='wheelMult', dv=1, k=True)
		
		# Distance groups
		self.WheelDist_Grp = lrc.Null()
		self.WheelDist_Grp.name = 'Wheel%sDist%sGrp' % (part, side)
		self.WheelDist_Grp.parent(self.Ctrl_Grp)

		self.WheelOrigDist_Grp = lrc.Null()
		self.WheelOrigDist_Grp.name = 'Wheel%sOrigDist%sGrp' % (part, side)
		self.WheelOrigDist_Grp.snap(parentCtrl)
		self.WheelOrigDist_Grp.parent(self.WheelDist_Grp)
		
		self.WheelTarDist_Grp = lrc.Null()
		self.WheelTarDist_Grp.name = 'Wheel%sTarDist%sGrp' % (part, side)
		self.WheelTarDist_Grp.snap(parentCtrl)
		self.WheelTarDist_Grp.parent(self.WheelDist_Grp)
		
		mc.parentConstraint(ctrlGrp, self.WheelDist_Grp, mo=True)
		mc.parentConstraint(parentCtrl, self.WheelTarDist_Grp, mo=True)

		# Find the distance
		self.Wheel_Dist = lrc.DistanceBetween()
		self.Wheel_Dist.name = 'Wheel%s%sDist' % (part, side)

		self.WheelOrigDist_Grp.attr('tx') >> self.Wheel_Dist.attr('p1x')
		self.WheelOrigDist_Grp.attr('tz') >> self.Wheel_Dist.attr('p1z')
		self.WheelTarDist_Grp.attr('tx') >> self.Wheel_Dist.attr('p2x')
		self.WheelTarDist_Grp.attr('tz') >> self.Wheel_Dist.attr('p2z')
		
		# Find the degrees
		self.WheelDegree_Mdv = lrc.MultiplyDivide()
		self.WheelDegree_Mdv.name = 'Wheel%sDegree%sMdv' % (part, side)

		self.Wheel_Dist.attr('d') >> self.WheelDegree_Mdv.attr('i1x')
		self.WheelDegree_Mdv.attr('op').v = 2
		self.WheelDegree_Mdv.attr('i2x').v = float(2.00*rad)*float(22.00/7.00)

		# Calculate the rotate values
		self.WheelRot_Mdv = lrc.MultiplyDivide()
		self.WheelRot_Mdv.name = 'Wheel%sRot%sMdv' % (part, side)

		self.WheelDegree_Mdv.attr('ox') >> self.WheelRot_Mdv.attr('i1x')
		self.WheelRot_Mdv.attr('i2x').v = 360

		# Rotate multiplier
		self.WheelMult_Mdv = lrc.MultiplyDivide()
		self.WheelMult_Mdv.name = 'Wheel%sMult%sMdv' % (part, side)

		self.WheelRot_Mdv.attr('ox') >> self.WheelMult_Mdv.attr('i1x')
		self.Wheel_Ctrl.attr('wheelMult') >> self.WheelMult_Mdv.attr('i2x')

		self.WheelAtRot_Mdv = lrc.MultiplyDivide()
		self.WheelAtRot_Mdv.name = 'Wheel%sAtRot%sMdv' % (part, side)

		# Auto On/Off
		self.WheelMult_Mdv.attr('ox') >> self.WheelAtRot_Mdv.attr('i1x')
		self.Wheel_Ctrl.attr('wheelRot') >> self.WheelAtRot_Mdv.attr('i2x')

		self.WheelAtRot_Mdv.attr('ox') >> self.WheelCtrlRot_Grp.attr('rx')

		# Connect to joint
		mc.parentConstraint(self.Wheel_Ctrl, self.Wheel_Jnt)
		mc.scaleConstraint(self.Wheel_Ctrl, self.Wheel_Jnt)

		if chasRig:
			
			if not parentCtrl.attr('wheelShake').exists:
				parentCtrl.add(ln='wheelShake', dv=0.1, k=True)

			self.WheeShakelMult_Mdv = lrc.MultiplyDivide()
			self.WheeShakelMult_Mdv.name = 'WheeShakel%sMult%sMdv' % (part, side)

			chasRig.ChasShake_Rem.attr('outValue') >> self.WheeShakelMult_Mdv.attr('i1x')
			parentCtrl.attr('wheelShake') >> self.WheeShakelMult_Mdv.attr('i2x')

			self.WheeShakelMult_Mdv.attr('ox') >> self.WheelCtrlRot_Grp.attr('ry')
