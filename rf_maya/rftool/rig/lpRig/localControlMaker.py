import maya.cmds as mc
import maya.mel as mm
from functools import partial

import lpRig.core as lrc
reload(lrc)
import lpRig.rigTools as lrr
reload(lrr)

def run():
	ui = LocalControlMaker()
	ui.show()
	return ui

class LocalControlMaker(object):

	def __init__(self):

		self.ui = 'localControlMaker'
		self.win = '%sWin' % self.ui

	def show(self):

		if mc.window(self.win, exists=True):
			mc.deleteUI(self.win)

		mc.window(self.win, t='localControlMaker')
		mc.columnLayout('%sMainCL'%self.ui, adj=True)

		# Shape option menu
		self.crvShpTfg = mc.textFieldGrp('%sCrvShpTfg' % self.ui, label='curve:', text='circle', cw=[1, 40], editable=False)
		mc.popupMenu()
		for each in ('circle', 'square', 'cube', 'nrbCircle'):
			mc.menuItem(l=each, c=partial(self.setShp, each))

		mc.separator()

		# Name
		self.prefixTfg = mc.textFieldGrp('%sPrefixTfg' % self.ui, label='prefix:', cw=[1, 40])
		self.partTfg = mc.textFieldGrp('%sPartTfg' % self.ui, label='part:', text='FclRig', cw=[1, 40])
		
		mc.separator()

		# Side
		self.sideTfg = mc.textFieldGrp('%sSideTfg' % self.ui, label='side:', text='', cw=[1, 40], editable=False)
		mc.popupMenu()
		for each in ('...', 'L', 'R'):
			mc.menuItem(l=each, c=partial(self.setSide, each))

		mc.separator()

		# Create
		mc.button(l='Create', h=50, c=partial(self.createTriggered))

		mc.showWindow(self.win)

		# Startup
		# mc.textFieldGrp(self.prefixTfg, e=True, text='EyeSocketUpA')
		# mc.textFieldGrp(self.sideTfg, e=True, text='L')

	def createTriggered(self, *args):

		sel = mc.ls(sl=True)[0]
		pos = mc.xform(sel, q=True, t=True, ws=True)
		ori = mc.xform(sel, q=True, ro=True, ws=True)

		shp = mc.textFieldGrp(self.crvShpTfg, q=True, text=True)

		prefix = mc.textFieldGrp(self.prefixTfg, q=True, text=True)
		part = mc.textFieldGrp(self.partTfg, q=True, text=True)
		side = mc.textFieldGrp(self.sideTfg, q=True, text=True)

		# Define color
		col = 'yellow'

		if side == 'L':
			col = 'red'
		elif side == 'R':
			col = 'blue'

		# Define side
		if side:
			side = '_%s_' % side
		else:
			side = '_'
		
		ctrl = lrc.Control(shp)
		ctrl.lockHideAttrs('v')
		ctrl.name = '%s%s%sCtrl' % (prefix, part, side)
		ctrl.color = col
		
		ctrlInvGrp = lrc.group(ctrl)
		ctrlInvGrp.name = '%s%sCtrlInv%sGrp' % (prefix, part, side)
		
		ctrlZrGrp = lrc.group(ctrlInvGrp)
		ctrlZrGrp.name = '%s%sCtrlZr%sGrp' % (prefix, part, side)
		
		mc.xform(ctrlZrGrp, t=pos, ws=True)
		mc.xform(ctrlZrGrp, ro=ori, ws=True)

		jntNm = '%s%s%sJnt' % (prefix, part, side)
		jnt = lrc.Dag(jntNm)

		if not mc.objExists(jntNm):
			
			jnt = lrc.Joint()
			jnt.name = jntNm
			
			jntZrGrp = lrc.group(jnt)
			jntZrGrp.name = '%s%sJntZr%sGrp' % (prefix, part, side)
			jntZrGrp.snap(ctrl)
		
		ctrl.attr('t') >> jnt.attr('t')
		ctrl.attr('r') >> jnt.attr('r')
		ctrl.attr('s') >> jnt.attr('s')

	def setSide(self, side='', *args):
		if side == '...': side = ''
		mc.textFieldGrp(self.sideTfg, e=True, text=side)

	def setShp(self, shp='', *args):

		mc.textFieldGrp(self.crvShpTfg, e=True, text=shp)