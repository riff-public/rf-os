from functools import partial
import re

import maya.OpenMaya as om
import maya.OpenMayaAnim as oma

import pymel.core as pmc

def id_from_str(comp_str=''):
	"""Get component id(int) from the component name.
	"""
	exp = r'(.+\[)([0-9]+)(\])'
	re_obj = re.search(exp, comp_str)

	if not re_obj:
		return None
	
	comp_id = int(re_obj.group(2))

	return comp_id

def get_soft_selection_data():
	"""Gets a list of components and a light of soft selection
	weght values.
	"""
	compos = []
	weights = []

	sel = om.MSelectionList()
	softsel = om.MRichSelection()
	om.MGlobal.getRichSelection(softsel)
	softsel.getSelection(sel)

	dagpath = om.MDagPath()
	compo = om.MObject()

	seliter = om.MItSelectionList(sel, om.MFn.kMeshVertComponent)
	while not seliter.isDone():

		seliter.getDagPath(dagpath, compo)
		nodepath = dagpath.fullPathName()
		fncomp = om.MFnSingleIndexedComponent(compo)

		get_weight = lambda x: fncomp.weight(x).influence() if fncomp.hasWeights() else 1.0

		for ix in range(fncomp.elementCount()):
			compos.append('%s.vtx[%i]' % (nodepath, fncomp.element(ix)))
			weights.append(get_weight(ix))
		
		seliter.next()

	return compos, weights

def soft_cluster():
	"""Creates a cluster regarding the data from soft selection.
	"""
	if pmc.symmetricModelling(q=True, s=True):
		print 'soft_cluster does not work in symmetrcical selection mode.'
		return False
	
	compos, weights = get_soft_selection_data()
	print compos, weights

	cluster, cluster_xform = pmc.cluster(compos)

	for compo, weight in zip(compos, weights):

		vtxid = id_from_str(compo)
		wlattr = 'weightList[0].weights[%i]' % vtxid
		cluster.attr(wlattr).set(weight)