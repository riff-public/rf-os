# Maya modules
import maya.cmds as mc
import maya.mel as mm

import lpRig.core as lrc
reload(lrc)
import lpRig.rigTools as lrr
reload(lrr)
import lpRig.ribbon as lrrb
reload(lrrb)

class SpineRig(object):

	def __init__(
					self,
					parent='',
					ctrlGrp='',
					jntGrp='',
					ikhGrp='',
					skinGrp='',
					stillGrp='',
					ribbon=True,
					ax='y',
					tmpJnt=[
								'Spine_1_Jnt',
								'Spine_2_Jnt',
								'Spine_3_Jnt',
								'ChestPiv_Grp',
							],
					part='',
				):

		# Skin joints
		self.Spine_1_Jnt = lrc.Dag(tmpJnt[0])
		self.Spine_2_Jnt = lrc.Dag(tmpJnt[1])
		self.Spine_3_Jnt = lrc.Dag(tmpJnt[2])
		self.ChestPiv_Grp = lrc.Dag(tmpJnt[3])
		
		self.Spine_1_Jnt.rotateOrder = 'yzx'
		self.Spine_2_Jnt.rotateOrder = 'yzx'
		self.Spine_3_Jnt.rotateOrder = 'yzx'
		
		# Main groups
		self.Ctrl_Grp = lrc.Null()
		self.Jnt_Grp = lrc.Null()
		
		self.Ctrl_Grp.name = 'Spine%sCtrl_Grp' % part
		self.Jnt_Grp.name = 'Spine%sJnt_Grp' % part
		
		mc.parentConstraint(parent, self.Ctrl_Grp)
		mc.parentConstraint(parent, self.Jnt_Grp)
		
		# Length
		self.upSpineLen = lrc.distance(self.Spine_2_Jnt, self.Spine_3_Jnt)
		self.lowSpineLen = lrc.distance(self.Spine_1_Jnt, self.Spine_2_Jnt)

		# Main Controller
		self.Spine_Ctrl = lrc.Control('stick')
		self.Spine_Ctrl.name = 'Spine%s_Ctrl' % part
		self.Spine_Ctrl.color = 'green'
		self.Spine_Ctrl.lockHideAttrs('tx', 'ty', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz', 'v')

		self.SpineCtrlZr_Grp = lrc.group(self.Spine_Ctrl)
		self.SpineCtrlZr_Grp.name = 'Spine%sCtrlZr_Grp' % part

		mc.parentConstraint(self.Spine_1_Jnt, self.SpineCtrlZr_Grp)
		self.SpineCtrlZr_Grp.parent(self.Ctrl_Grp)
		
		# FK rig
		# FK rig - Main groups
		self.SpineFkCtrl_Grp = lrc.Null()
		self.SpineFkCtrl_Grp.name = 'Spine%sFkCtrl_Grp' % part
		self.SpineFkCtrl_Grp.snap(parent)
		self.SpineFkCtrl_Grp.parent(self.Ctrl_Grp)
		
		self.SpineFkJnt_Grp = lrc.Null()
		self.SpineFkJnt_Grp.name = 'Spine%sFkJnt_Grp' % part
		self.SpineFkJnt_Grp.snap(parent)
		self.SpineFkJnt_Grp.parent(self.Jnt_Grp)

		# FK rig - joints
		self.SpineFk_1_Jnt = lrr.jointAt(self.Spine_1_Jnt)
		self.SpineFk_2_Jnt = lrr.jointAt(self.Spine_2_Jnt)
		self.SpineFk_3_Jnt = lrr.jointAt(self.Spine_3_Jnt)

		self.SpineFk_1_Jnt.name = 'Spine%sFk_1_Jnt' % part
		self.SpineFk_2_Jnt.name = 'Spine%sFk_2_Jnt' % part
		self.SpineFk_3_Jnt.name = 'Spine%sFk_3_Jnt' % part
		
		self.SpineFk_3_Jnt.parent(self.SpineFk_2_Jnt)
		self.SpineFk_2_Jnt.parent(self.SpineFk_1_Jnt)
		self.SpineFk_1_Jnt.parent(self.SpineFkJnt_Grp)
		
		self.SpineFk_1_Jnt.rotateOrder = 'yzx'
		self.SpineFk_2_Jnt.rotateOrder = 'yzx'
		self.SpineFk_3_Jnt.rotateOrder = 'yzx'

		# FK rig - controllers
		self.SpineFk_1_Ctrl = lrr.jointControl('circle')
		self.SpineFk_1_Ctrl.lockHideAttrs('sx', 'sy', 'sz', 'v')
		self.SpineFk_1_Ctrl.name = 'Spine%sFk_1_Ctrl' % part
		self.SpineFk_1_Ctrl.color = 'blue'
		self.SpineFk_1_Ctrl.rotateOrder = 'xzy'

		self.SpineFkGmbl_1_Ctrl = lrc.addGimbal(self.SpineFk_1_Ctrl)
		self.SpineFkGmbl_1_Ctrl.name = 'Spine%sFkGmbl_1_Ctrl' % part
		self.SpineFkGmbl_1_Ctrl.rotateOrder = 'xzy'

		self.SpineFkCtrlZr_1_Grp = lrc.group(self.SpineFk_1_Ctrl)
		self.SpineFkCtrlZr_1_Grp.name = 'Spine%sFkCtrlZr_1_Grp' % part

		self.SpineFkCtrlZr_1_Grp.snapPoint(self.SpineFk_1_Jnt)
		self.SpineFk_1_Ctrl.snapOrient(self.SpineFk_1_Jnt)
		self.SpineFk_1_Ctrl.freeze(r=True)
		
		self.SpineFk_2_Ctrl = lrr.jointControl('circle')
		self.SpineFk_2_Ctrl.lockHideAttrs('sx', 'sy', 'sz', 'v')
		self.SpineFk_2_Ctrl.name = 'Spine%sFk_2_Ctrl' % part
		self.SpineFk_2_Ctrl.color = 'blue'
		self.SpineFk_2_Ctrl.rotateOrder = 'xzy'
		
		self.SpineFkGmbl_2_Ctrl = lrc.addGimbal(self.SpineFk_2_Ctrl)
		self.SpineFkGmbl_2_Ctrl.name = 'Spine%sFkGmbl_2_Ctrl' % part
		self.SpineFkGmbl_2_Ctrl.rotateOrder = 'xzy'

		self.SpineFkCtrlZr_2_Grp = lrc.group(self.SpineFk_2_Ctrl)
		self.SpineFkCtrlZr_2_Grp.name = 'Spine%sFkCtrlZr_2_Grp' % part

		self.SpineFkCtrlZr_2_Grp.snapPoint(self.SpineFk_2_Jnt)
		self.SpineFk_2_Ctrl.snapOrient(self.SpineFk_2_Jnt)
		self.SpineFk_2_Ctrl.freeze(r=True)

		self.SpineFkCtrlZr_2_Grp.parent(self.SpineFkGmbl_1_Ctrl)
		self.SpineFkCtrlZr_1_Grp.parent(self.SpineFkCtrl_Grp)

		# FK rig - stretch setup
		(self.SpineFkStrt_1_Add,
		self.SpineFkStrt_1_Mul) = lrr.fkStretch(
													ctrl=self.SpineFk_1_Ctrl,
													target=self.SpineFkCtrlZr_2_Grp,
													ax=ax
												)
		self.SpineFkStrt_1_Add.name = 'Spine%sFkStrt_1_Add' % part
		self.SpineFkStrt_1_Mul.name = 'Spine%sFkStrt_1_Mul' % part

		(self.SpineFkStrt_2_Add,
		self.SpineFkStrt_2_Mul) = lrr.fkStretch(
													ctrl=self.SpineFk_2_Ctrl,
													target=self.SpineFk_3_Jnt,
													ax=ax
												)
		self.SpineFkStrt_2_Add.name = 'Spine%sFkStrt_2_Add' % part
		self.SpineFkStrt_2_Mul.name = 'Spine%sFkStrt_2_Mul' % part

		# FK rig - local/world setup
		(self.SpineFkCtrlLoc_1_Grp,
		self.SpineFkCtrlWor_1_Grp,
		self.SpineFkCtrlWor_1_Grp_orientConstraint,
		self.SpineFkCtrlZr_1_Grp_orientConstraint,
		self.SpineFkCtrlZrGrpOri_1_Rev) = lrr.orientLocalWorldCtrl(
																			self.SpineFk_1_Ctrl,
																			self.SpineFkCtrl_Grp,
																			ctrlGrp,
																			self.SpineFkCtrlZr_1_Grp
																		)
		self.SpineFkCtrlLoc_1_Grp.name = 'Spine%sFkCtrlLoc_1_Grp' % part
		self.SpineFkCtrlWor_1_Grp.name = 'Spine%sFkCtrlWor_1_Grp' % part
		self.SpineFkCtrlZrGrpOri_1_Rev.name = 'Spine%sFkCtrlZrGrpOri_1_Rev' % part
		
		(self.SpineFkCtrlLoc_2_Grp,
		self.SpineFkCtrlWor_2_Grp,
		self.SpineFkCtrlWor_2_Grp_orientConstraint,
		self.SpineFkCtrlZr_2_Grp_orientConstraint,
		self.SpineFkCtrlZrGrpOri_2_Rev) = lrr.orientLocalWorldCtrl(
																			self.SpineFk_2_Ctrl,
																			self.SpineFkGmbl_1_Ctrl,
																			ctrlGrp,
																			self.SpineFkCtrlZr_2_Grp
																		)
		self.SpineFkCtrlLoc_2_Grp.name = 'Spine%sFkCtrlLoc_2_Grp' % part
		self.SpineFkCtrlWor_2_Grp.name = 'Spine%sFkCtrlWor_2_Grp' % part
		self.SpineFkCtrlZrGrpOri_2_Rev.name = 'Spine%sFkCtrlZrGrpOri_2_Rev' % part

		# FK rig - connect to joint
		mc.parentConstraint(self.SpineFkGmbl_1_Ctrl, self.SpineFk_1_Jnt)
		mc.parentConstraint(self.SpineFkGmbl_2_Ctrl, self.SpineFk_2_Jnt)

		# IK rig
		# IK rig - Main groups
		self.SpineIkCtrl_Grp = lrc.Null()
		self.SpineIkCtrl_Grp.name = 'Spine%sIkCtrl_Grp' % part
		self.SpineIkCtrl_Grp.snap(parent)
		self.SpineIkCtrl_Grp.parent(self.Ctrl_Grp)
		
		self.SpineIkJnt_Grp = lrc.Null()
		self.SpineIkJnt_Grp.name = 'Spine%sIkJnt_Grp' % part
		self.SpineIkJnt_Grp.snap(parent)
		self.SpineIkJnt_Grp.parent(self.Jnt_Grp)

		# IK rig - Joints
		self.SpineIk_1_Jnt = lrr.jointAt(self.Spine_1_Jnt)
		self.SpineIk_2_Jnt = lrr.jointAt(self.Spine_2_Jnt)
		self.SpineIk_3_Jnt = lrr.jointAt(self.Spine_3_Jnt)

		self.SpineIk_1_Jnt.name = 'Spine%sIk_1_Jnt' % part
		self.SpineIk_2_Jnt.name = 'Spine%sIk_2_Jnt' % part
		self.SpineIk_3_Jnt.name = 'Spine%sIk_3_Jnt' % part

		self.SpineIk_3_Jnt.parent(self.SpineIk_2_Jnt)
		self.SpineIk_2_Jnt.parent(self.SpineIk_1_Jnt)
		self.SpineIk_1_Jnt.parent(self.SpineIkJnt_Grp)

		self.SpineIk_1_Jnt.rotateOrder = 'yzx'
		self.SpineIk_2_Jnt.rotateOrder = 'yzx'
		self.SpineIk_3_Jnt.rotateOrder = 'yzx'

		# IK rig - Controllers
		self.SpineIk_Ctrl = lrr.jointControl('cube')
		self.SpineIk_Ctrl.lockHideAttrs('sx', 'sy', 'sz', 'v')
		self.SpineIk_Ctrl.color = 'blue'
		self.SpineIk_Ctrl.rotateOrder = 'xzy'
		self.SpineIkOfst_Ctrl = lrc.group(self.SpineIk_Ctrl)
		self.SpineIkCtrlZr_Grp = lrc.group(self.SpineIkOfst_Ctrl)
		
		self.SpineIkRoot_Ctrl = lrr.jointControl('plus')
		self.SpineIkRoot_Ctrl.lockHideAttrs('rx', 'ry', 'rz', 'sx', 'sy', 'sz', 'v')
		self.SpineIkRoot_Ctrl.color = 'blue'
		self.SpineIkRoot_Ctrl.rotateOrder = 'xzy'
		self.SpineIkRootCtrlZr_Grp = lrc.group(self.SpineIkRoot_Ctrl)
		
		self.SpineIkPol_Grp = lrc.Null()

		self.SpineIk_Ctrl.name = 'Spine%sIk_Ctrl' % part
		self.SpineIkOfst_Ctrl.name = 'Spine%sIkOfst_Ctrl' % part
		self.SpineIkCtrlZr_Grp.name = 'Spine%sIkCtrlZr_Grp' % part
		self.SpineIkRoot_Ctrl.name = 'Spine%sIkRoot_Ctrl' % part
		self.SpineIkRootCtrlZr_Grp.name = 'Spine%sIkRootCtrlZr_Grp' % part
		self.SpineIkPol_Grp.name = 'Spine%sIkPol_Grp' % part

		# IK rig - Positioning
		self.SpineIkCtrlZr_Grp.snapPoint(self.Spine_2_Jnt)
		self.SpineIk_Ctrl.snapOrient(self.Spine_2_Jnt)
		self.SpineIk_Ctrl.freeze(r=True)
		
		self.SpineIkCon_Ctrl = lrc.addConCtrl(self.SpineIk_Ctrl)
		self.SpineIkGmbl_Ctrl = lrc.addGimbal(self.SpineIk_Ctrl)

		self.SpineIkCon_Ctrl.name = 'Spine%sIkCon_Ctrl' % part
		self.SpineIkGmbl_Ctrl.name = 'Spine%sIkGmbl_Ctrl' % part
		
		self.SpineIkRootCtrlZr_Grp.snapPoint(self.SpineIk_1_Jnt)
		
		self.SpineIkPol_Grp.snap(self.SpineIkRoot_Ctrl)
		self.SpineIkPol_Grp.parent(self.SpineIkRoot_Ctrl)
		
		self.SpineIkCtrlZr_Grp.parent(self.SpineIkRoot_Ctrl)
		self.SpineIkRootCtrlZr_Grp.parent(self.SpineIkCtrl_Grp)

		# mc.orientConstraint(ctrlGrp, self.SpineIkRootCtrlZr_Grp)
		mc.pointConstraint(self.SpineIkRoot_Ctrl, self.SpineIk_1_Jnt)

		# IK rig - Polvector
		ikCtrlShp = lrc.Dag(self.SpineIk_Ctrl.shape)
		ikCtrlShp.add(ln='polVector', at='double3', k=False)
		ikCtrlShp.add(p='polVector', ln='polVectorX', at='double', k=False)
		ikCtrlShp.add(p='polVector', ln='polVectorY', at='double', k=False)
		ikCtrlShp.add(p='polVector', ln='polVectorZ', at='double', k=False)
		
		ikCtrlShp.attr('polVectorX') >> self.SpineIkPol_Grp.attr('tx')
		ikCtrlShp.attr('polVectorY') >> self.SpineIkPol_Grp.attr('ty')
		ikCtrlShp.attr('polVectorZ') >> self.SpineIkPol_Grp.attr('tz')

		# IK rig - IK handles
		self.SpineIk_1_Ikh = lrc.IkRp(self.SpineIk_1_Jnt, self.SpineIk_2_Jnt)
		self.SpineIk_1_Ikh.name = 'Spine%sIk_1_Ikh' % part
		self.SpineIk_2_Ikh = lrc.IkRp(self.SpineIk_2_Jnt, self.SpineIk_3_Jnt)
		self.SpineIk_2_Ikh.name = 'Spine%sIk_2_Ikh' % part
		mc.poleVectorConstraint(self.SpineIkPol_Grp, self.SpineIk_1_Ikh)
		
		self.SpineIkh_Grp = lrc.Null()
		self.SpineIkhZr_1_Grp = lrc.Null()
		self.SpineIkhZr_2_Grp = lrc.Null()

		self.SpineIkhZr_1_Grp.snap(self.SpineIk_1_Ikh)
		self.SpineIkhZr_2_Grp.snap(self.SpineIk_2_Ikh)

		self.SpineIk_1_Ikh.parent(self.SpineIkhZr_1_Grp)
		self.SpineIk_2_Ikh.parent(self.SpineIkhZr_2_Grp)

		self.SpineIkh_Grp.name = 'Spine%sIkh_Grp' % part
		self.SpineIkhZr_1_Grp.name = 'Spine%sIkhZr_1_Grp' % part
		self.SpineIkhZr_2_Grp.name = 'Spine%sIkhZr_2_Grp' % part

		self.SpineIkhZr_1_Grp.parent(self.SpineIkh_Grp)
		self.SpineIkhZr_2_Grp.parent(self.SpineIkh_Grp)
		self.SpineIkh_Grp.parent(ikhGrp)

		# IK rig - IK handles - Pivots
		self.SpineIkhPiv_1_Grp = lrc.Null()
		self.SpineIkhPiv_2_Grp = lrc.Null()

		self.SpineIkhPiv_1_Grp.name = 'Spine%sIkhPiv_1_Grp' % part
		self.SpineIkhPiv_2_Grp.name = 'Spine%sIkhPiv_2_Grp' % part
		
		self.SpineIkhPiv_1_Grp.snap(self.SpineIkhZr_1_Grp)
		self.SpineIkhPiv_2_Grp.snap(self.SpineIkhZr_2_Grp)
		
		self.SpineIkhPiv_1_Grp.parent(self.SpineIkGmbl_Ctrl)
		self.SpineIkhPiv_2_Grp.parent(self.SpineIkGmbl_Ctrl)
		
		mc.parentConstraint(self.SpineIkhPiv_1_Grp, self.SpineIkhZr_1_Grp)
		mc.parentConstraint(self.SpineIkhPiv_2_Grp, self.SpineIkhZr_2_Grp)

		# IK rig - local/world setup
		(self.SpineIkCtrlLoc_Grp,
		self.SpineIkCtrlWor_Grp,
		self.SpineIkCtrlWor_grp_parentConstraint,
		self.SpineIkCtrlZr_Grp_parentConstraint,
		self.SpineIkCtrlZrGrpPar_Rev) = lrr.parentLocalWorldCtrl(
																	self.SpineIk_Ctrl,
																	self.SpineIkRoot_Ctrl,
																	ctrlGrp,
																	self.SpineIkCtrlZr_Grp
																)
		self.SpineIkCtrlLoc_Grp.name = 'Spine%sIkCtrlLoc_Grp' % part
		self.SpineIkCtrlWor_Grp.name = 'Spine%sIkCtrlWor_Grp' % part
		self.SpineIkCtrlZrGrpPar_Rev.name = 'Spine%sIkCtrlZrGrpPar_Rev' % part

		# IK rig - Twist setup
		self.SpineIk_Ctrl.add(ln = 'twist', k = True)
		self.SpineIkTws_Add = lrc.AddDoubleLinear()
		self.SpineIkTws_Add.name = 'Spine%sIkTws_Add' % part
		self.SpineIk_Ctrl.attr('twist') >> self.SpineIkTws_Add.attr('i1')
		self.SpineIkTws_Add.attr('o') >> self.SpineIk_1_Ikh.attr('twist')

		# IK rig - Stretch setup
		self.SpineIk_Ctrl.add(ln = '__stretch__', k = True)
		self.SpineIk_Ctrl.attr('__stretch__').set(l = True)
		
		# IK rig - Stretch setup - Auto-Stretch setup
		spiLen = self.Spine_2_Jnt.attr('t%s' % ax).v

		self.SpineIkStrtPnt_1_Grp = lrc.Null()
		self.SpineIkStrtPnt_2_Grp = lrc.Null()

		self.SpineIkStrtPnt_1_Grp.name = 'Spine%sIkStrtPnt_1_Grp' % part
		self.SpineIkStrtPnt_2_Grp.name = 'Spine%sIkStrtPnt_2_Grp' % part

		mc.pointConstraint(self.SpineIkhPiv_1_Grp, self.SpineIkStrtPnt_1_Grp)
		mc.pointConstraint(self.SpineIkRoot_Ctrl, self.SpineIkStrtPnt_2_Grp)

		self.SpineIkStrt_1_Mdv = lrc.MultiplyDivide()
		self.SpineIkStrt_1_Mul = lrc.MultDoubleLinear()
		self.SpineIkStrt_1_Blend = lrc.BlendTwoAttr()
		self.SpineIkStrt_1_Dist = lrc.DistanceBetween()

		self.SpineIkStrt_1_Mdv.name = 'Spine%sIkStrt_1_Mdv' % part
		self.SpineIkStrt_1_Mul.name = 'Spine%sIkStrt_1_Mul' % part
		self.SpineIkStrt_1_Blend.name = 'Spine%sIkStrt_1_Blend' % part
		self.SpineIkStrt_1_Dist.name = 'Spine%sIkStrt_1_Dist' % part

		self.SpineIk_Ctrl.add(ln = 'autoStretch', k = True, min = 0, max = 1)
		self.SpineIk_Ctrl.attr('autoStretch') >> self.SpineIkStrt_1_Blend.attr('attributesBlender')
		
		self.SpineIkStrtPnt_1_Grp.attr('t') >> self.SpineIkStrt_1_Dist.attr('point1')
		self.SpineIkStrtPnt_2_Grp.attr('t') >> self.SpineIkStrt_1_Dist.attr('point2')

		self.SpineIkStrt_1_Mdv.attr('operation').value = 2
		self.SpineIkStrt_1_Dist.attr('distance') >> self.SpineIkStrt_1_Mdv.attr('i1x')
		self.SpineIkStrt_1_Mdv.attr('i2x').value = spiLen

		self.SpineIkStrt_1_Mdv.attr('ox') >> self.SpineIkStrt_1_Mul.attr('i1')
		self.SpineIkStrt_1_Mul.attr('i2').value = spiLen
		self.SpineIkStrt_1_Blend.add(ln = 'default', dv = spiLen, k = True)
		self.SpineIkStrt_1_Blend.attr('default') >> self.SpineIkStrt_1_Blend.attr('i[0]')
		self.SpineIkStrt_1_Mul.attr('o') >> self.SpineIkStrt_1_Blend.attr('i[1]')
		self.SpineIkStrtPnt_1_Grp.parent(self.SpineIkCtrl_Grp)
		self.SpineIkStrtPnt_2_Grp.parent(self.SpineIkCtrl_Grp)

		# IK rig - Stretch setup - Stretch setup
		self.SpineIk_Ctrl.add(ln = 'lowerStretch', k = True)
		self.SpineIk_Ctrl.add(ln = 'upperStretch', k = True)
		
		(self.SpineIkLowStrt_Add,
		self.SpineIkLowStrt_Mul) = lrr.fkStretch(
														ctrl=self.SpineIk_Ctrl,
														attr='lowerStretch',
														target=self.SpineIk_2_Jnt,
														ax=ax
													)
		self.SpineIkStrt_1_Blend.attr('o') >> self.SpineIkLowStrt_Add.attr('default')
		self.SpineIkLowStrt_Add.name = 'Spine%sIkLowStrt_Add' % part
		self.SpineIkLowStrt_Mul.name = 'Spine%sIkLowStrt_Mul' % part

		(self.SpineIkUpStrt_Add,
		self.SpineIkUpStrt_Mul) = lrr.fkStretch(
													ctrl=self.SpineIk_Ctrl,
													attr='upperStretch',
													target=self.SpineIk_3_Jnt,
													ax=ax
												)
		self.SpineIkUpStrt_Add.name = 'Spine%sIkUpStrt_Add' % part
		self.SpineIkUpStrt_Mul.name = 'Spine%sIkUpStrt_Mul' % part
		
		# IK rig - Chest controller
		self.ChestIk_Ctrl = lrr.jointControl('circle')
		self.ChestIk_Ctrl.lockHideAttrs('sx', 'sy', 'sz', 'v')
		self.ChestIk_Ctrl.color = 'blue'

		self.ChestIkOfst_Ctrl = lrc.group(self.ChestIk_Ctrl)
		self.ChestIkCtrlZr_Grp = lrc.group(self.ChestIkOfst_Ctrl)

		self.ChestIkCon_Ctrl = lrc.addConCtrl(self.ChestIk_Ctrl)
		self.ChestIkGmbl_Ctrl = lrc.addGimbal(self.ChestIk_Ctrl)

		self.ChestIk_Ctrl.name = 'Chest%sIk_Ctrl' % part
		self.ChestIkOfst_Ctrl.name = 'Chest%sIkOfst_Ctrl' % part
		self.ChestIkCtrlZr_Grp.name = 'Chest%sIkCtrlZr_Grp' % part

		self.ChestIkCon_Ctrl.name = 'Chest%sIkCon_Ctrl' % part
		self.ChestIkGmbl_Ctrl.name = 'Chest%sIkGmbl_Ctrl' % part

		self.ChestIkCtrlZr_Grp.snapPoint(self.ChestPiv_Grp)
		self.ChestIkCtrlZr_Grp.snapOrient(self.ChestPiv_Grp)
		self.ChestIkCtrlZr_Grp.freeze(r=True)

		self.ChestIkCtrlZr_Grp.parent(self.SpineIkGmbl_Ctrl)

		self.ChestIk_Ctrl.rotateOrder = 'xzy'
		self.ChestIkGmbl_Ctrl.rotateOrder = 'xzy'

		self.SpineIkhPiv_1_Grp.parent(self.ChestIkGmbl_Ctrl)
		self.SpineIkhPiv_2_Grp.parent(self.ChestIkGmbl_Ctrl)

		# FK/IK blending
		self.SpineFkIk_Rev = lrc.Reverse()
		self.SpineFkIk_Rev.name = 'Spine%sFkIk_Rev' % part

		self.Spine_Ctrl.add(ln='fkIk', min=0, max=1, k=True)
		self.Spine_Ctrl.attr('fkIk') >> self.SpineFkIk_Rev.attr('ix')
		self.Spine_Ctrl.attr('fkIk') >> self.SpineIkCtrl_Grp.attr('v')
		self.SpineFkIk_Rev.attr('ox') >> self.SpineFkCtrl_Grp.attr('v')
		
		spine1JntParCons = lrc.parentConstraint(self.SpineFk_1_Jnt, self.SpineIk_1_Jnt, self.Spine_1_Jnt)
		spine2JntParCons = lrc.parentConstraint(self.SpineFk_2_Jnt, self.SpineIk_2_Jnt, self.Spine_2_Jnt)
		spine3JntParCons = lrc.parentConstraint(self.SpineFk_3_Jnt, self.SpineIk_3_Jnt, self.Spine_3_Jnt)
		
		self.Spine_Ctrl.attr('fkIk') >> spine1JntParCons.attr('w1')
		self.Spine_Ctrl.attr('fkIk') >> spine2JntParCons.attr('w1')
		self.Spine_Ctrl.attr('fkIk') >> spine3JntParCons.attr('w1')
		
		self.SpineFkIk_Rev.attr('ox') >> spine1JntParCons.attr('w0')
		self.SpineFkIk_Rev.attr('ox') >> spine2JntParCons.attr('w0')
		self.SpineFkIk_Rev.attr('ox') >> spine3JntParCons.attr('w0')
		
		# Grouping
		self.Ctrl_Grp.parent(ctrlGrp)
		self.Jnt_Grp.parent(jntGrp)

		# Ribbon
		self.SpineRbnCtrl_Grp = lrc.Null()
		self.SpineRbnCtrl_Grp.name = 'Spine%sRbnCtrl_Grp' % part
		self.SpineRbnCtrl_Grp.snap(self.Ctrl_Grp)

		# Ribbon - Pivots
		self.SpineRbnSpc_Grp = lrc.Null()
		self.SpineRbnSpc_Grp.name = 'Spine%sRbnSpc_Grp' % part
		self.SpineRbnSpc_Grp.snap(self.SpineRbnCtrl_Grp)

		# Ribbon - Pivots - Spine 1
		self.SpineRbnSpc_1_Grp = lrc.Null()
		self.SpineRbnSpcZr_1_Grp = lrc.group(self.SpineRbnSpc_1_Grp)

		self.SpineRbnSpc_1_Grp.name = 'Spine%sRbnSpc_1_Grp' % part
		self.SpineRbnSpcZr_1_Grp.name = 'Spine%sRbnSpcZr_1_Grp' % part
		
		self.SpineRbnSpc_1_Grp.rotateOrder = 'yzx'

		self.SpineRbnSpcZr_1_Grp.snap(self.Spine_1_Jnt)
		lrc.parentConstraint(parent, self.SpineRbnSpcZr_1_Grp, mo = True)
		lrc.parentConstraint(self.Spine_1_Jnt, self.SpineRbnSpc_1_Grp)
		
		# Ribbon - Pivots - Spine 2
		self.SpineRbnSpc_2_Grp = lrc.Null()
		self.SpineRbnSpcZr_2_Grp = lrc.group(self.SpineRbnSpc_2_Grp)

		self.SpineRbnSpc_2_Grp.name = 'Spine%sRbnSpc_2_Grp' % part
		self.SpineRbnSpcZr_2_Grp.name = 'Spine%sRbnSpcZr_2_Grp' % part
		
		self.SpineRbnSpc_2_Grp.rotateOrder = 'yzx'
		
		self.SpineRbnSpcZr_2_Grp.snap(self.Spine_2_Jnt)
		lrc.parentConstraint(self.Spine_1_Jnt, self.SpineRbnSpcZr_2_Grp, mo = True)
		lrc.parentConstraint(self.Spine_2_Jnt, self.SpineRbnSpc_2_Grp)
		
		# Ribbon - Pivots - Spine 2
		self.SpineRbnSpc_3_Grp = lrc.Null()
		self.SpineRbnSpcZr_3_Grp = lrc.group(self.SpineRbnSpc_3_Grp)

		self.SpineRbnSpc_3_Grp.name = 'Spine%sRbnSpc_3_Grp' % part
		self.SpineRbnSpcZr_3_Grp.name = 'Spine%sRbnSpcZr_3_Grp' % part
		
		self.SpineRbnSpc_3_Grp.rotateOrder = 'yzx'

		self.SpineRbnSpcZr_3_Grp.snap(self.Spine_3_Jnt)
		lrc.parentConstraint(self.Spine_2_Jnt, self.SpineRbnSpcZr_3_Grp, mo=True)
		lrc.parentConstraint(self.Spine_3_Jnt, self.SpineRbnSpc_3_Grp)

		# Ribbon - Pivots - Grouping
		self.SpineRbnSpcZr_1_Grp.parent(self.SpineRbnSpc_Grp)
		self.SpineRbnSpcZr_2_Grp.parent(self.SpineRbnSpc_Grp)
		self.SpineRbnSpcZr_3_Grp.parent(self.SpineRbnSpc_Grp)
		self.SpineRbnSpc_Grp.parent(self.SpineRbnCtrl_Grp)
		
		# Ribbon - Middle Controller
		self.SpineRbn_Ctrl = lrc.Control('plus')
		self.SpineRbn_Ctrl.name = 'Spine%sRbn_Ctrl' % part
		self.SpineRbn_Ctrl.lockHideAttrs('rx', 'ry', 'rz', 'sx', 'sy', 'sz', 'v')
		self.SpineRbn_Ctrl.color = 'yellow'
		
		self.SpineRbnCtrlZr_Grp = lrc.group(self.SpineRbn_Ctrl)
		self.SpineRbnCtrlZr_Grp.name = 'Spine%sRbnCtrlZr_Grp' % part
		
		mc.pointConstraint(self.SpineRbnSpc_2_Grp, self.SpineRbnCtrlZr_Grp)
		mc.orientConstraint(self.SpineRbnSpc_2_Grp, self.SpineRbnCtrlZr_Grp, mo=True)
		
		aimVec = (0, 1, 0)
		upVec = (1, 0, 0)
		
		if ax =='z':
			aimVec = (0, 0, 1)
			upVec = (1, 0, 0)

		# Ribbon - Lower Spine
		if ribbon :
			self.LowSpineRbn = lrrb.RibbonIkHi(size=self.lowSpineLen, ax='%s+' % ax, part='Spine%sLow' % part, side='')
		else :
			self.LowSpineRbn = lrrb.RibbonIkLow(size=self.lowSpineLen, ax='%s+' % ax, part='Spine%sLow' % part, side='')
		
		self.LowSpineRbn.Ctrl_Grp.snap(self.Spine_1_Jnt)
		mc.delete(mc.aimConstraint(self.Spine_2_Jnt, self.LowSpineRbn.Ctrl_Grp, aim=aimVec, u=upVec, wut='vector', wu=upVec))
		lrc.parentConstraint(self.SpineRbnSpc_1_Grp, self.LowSpineRbn.Ctrl_Grp, mo=True)
		lrc.pointConstraint(self.SpineRbn_Ctrl, self.LowSpineRbn.RbnEnd_Ctrl)
		
		# Ribbon - Lower Spine - Twisting
		# lowSpineRbnShp = lrc.Dag(self.LowSpineRbn.Rbn_Ctrl.shape)
		# self.SpineRbnSpc_2_Grp.attr('ry') >> lowSpineRbnShp.attr('endTwist')
		
		# Ribbon - Upper Spine
		if ribbon :
			self.UpSpineRbn = lrrb.RibbonIkHi(size=self.upSpineLen, ax='%s+' % ax, part='Spine%sUp' % part, side='')
		else :
			self.UpSpineRbn = lrrb.RibbonIkLow(size=self.upSpineLen, ax='%s+' % ax, part='Spine%sUp' % part, side='')
		
		self.UpSpineRbn.Ctrl_Grp.snap(self.Spine_2_Jnt)
		mc.delete(mc.aimConstraint(self.Spine_3_Jnt, self.UpSpineRbn.Ctrl_Grp, aim=aimVec, u=upVec, wut='vector', wu=upVec))
		lrc.parentConstraint(self.SpineRbnSpc_2_Grp, self.UpSpineRbn.Ctrl_Grp, mo=True)
		lrc.pointConstraint(self.SpineRbnSpc_3_Grp, self.UpSpineRbn.RbnEnd_Ctrl)
		lrc.pointConstraint(self.SpineRbn_Ctrl, self.UpSpineRbn.RbnRoot_Ctrl)
		
		# Ribbon - Grouping
		self.SpineRbnCtrlZr_Grp.parent(self.SpineRbnCtrl_Grp)
		self.LowSpineRbn.Ctrl_Grp.parent(self.SpineRbnCtrl_Grp)
		self.LowSpineRbn.Skin_Grp.parent(skinGrp)
		self.UpSpineRbn.Ctrl_Grp.parent(self.SpineRbnCtrl_Grp)
		self.UpSpineRbn.Skin_Grp.parent(skinGrp)
		self.SpineRbnCtrl_Grp.parent(self.Ctrl_Grp)
		
		self.LowSpineRbn.Jnt_Grp.parent(jntGrp)
		self.UpSpineRbn.Jnt_Grp.parent(jntGrp)
		
		if ribbon :
			self.LowSpineRbn.Still_Grp.parent(stillGrp)
			self.UpSpineRbn.Still_Grp.parent(stillGrp)
		
		for attr in ('tx','ty','tz'):
			self.LowSpineRbn.RbnRoot_Ctrl.attr(attr).lockHide()
			self.LowSpineRbn.RbnEnd_Ctrl.attr(attr).lockHide()
			self.UpSpineRbn.RbnRoot_Ctrl.attr(attr).lockHide()
			self.UpSpineRbn.RbnEnd_Ctrl.attr(attr).lockHide()
		
		self.LowSpineRbn.RbnRoot_Ctrl.hide()
		self.LowSpineRbn.RbnEnd_Ctrl.hide()
		self.UpSpineRbn.RbnRoot_Ctrl.hide()
		self.UpSpineRbn.RbnEnd_Ctrl.hide()