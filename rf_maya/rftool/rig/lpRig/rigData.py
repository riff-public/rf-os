import os
import re
import pickle
from functools import partial

import maya.cmds as mc
import maya.mel as mm
import maya.api.OpenMaya as om

def getDataFld():

	scnPath = os.path.normpath(mc.file(q=True, l=True)[0])
	fldPath = os.path.split(scnPath)[0]

	dataFld = os.path.join(fldPath, 'rigData')

	if not os.path.exists(dataFld):
		os.mkdir(dataFld)

	return dataFld

def getFirstParentJoint(obj=''):

	jnt = None

	ln = mc.ls(obj, l=True)[0]

	elms = ln.split('|')

	for ix in range(-2, -len(elms), -1):
		currLn = '|'.join(elms[:ix+1])
		if mc.nodeType(currLn) == 'joint':
			jnt = currLn
			break

	return jnt

def writeSelectedHierarchy():

	suffix = 'Skel'
	sel = mc.ls(sl=True)[0]
	flNm = sel
	if ':' in sel:
		flNm = sel.split(':')[-1]

		
	dataFld = getDataFld()
	flNmExt = '%s%s.dat' % (flNm, suffix)
	
	flPath = '%s/%s' % (dataFld, flNmExt)
	writeHierarchy(root=sel, flPath=flPath)
		
	mc.confirmDialog(title='Progress', message='Exporting hierarchy is done.')

def writeHierarchy(root='', flPath=''):

	skelDict = {}
	infTypes = ('joint', 'transform')

	rootNm = root.split(':')[-1]
	rootPos = mc.xform(root, q=True, t=True)
	rootOri = mc.xform(root, q=True, ro=True)

	rootInfo = {}
	rootInfo['name'] = rootNm
	rootInfo['type'] = mc.nodeType(root)
	rootInfo['pos'] = rootPos
	rootInfo['ori'] = rootOri

	skelDict[0] = rootInfo

	for ix, chd in enumerate(sorted(mc.listRelatives(root, ad=True, f=True))):

		type_ = mc.nodeType(chd)

		if not type_ in infTypes: continue

		infoDict = {}
		infoDict['name'] = chd.split(':')[-1]
		infoDict['type'] = type_
		infoDict['pos'] = mc.xform(chd, q=True, t=True, ws=True)
		infoDict['ori'] = mc.xform(chd, q=True, ro=True, ws=True)
		infoDict['parent'] = mc.listRelatives(chd, p=True, f=True)[0].split(':')[-1]
		
		skelDict[ix+1] = infoDict

	flObj = open(flPath, 'w')
	pickle.dump(skelDict, flObj)
	flObj.close()

	return True

def readHierarchy(flPath=''):
	
	print 'Loading %s' % flPath
	flObj = open(flPath, 'r')
	skelDict = pickle.load(flObj)
	flObj.close()

	for key in sorted(skelDict.keys()):

		obj = mc.createNode(skelDict[key]['type'], n=skelDict[key]['name'])
		mc.xform(obj, t=skelDict[key]['pos'], ws=True)
		mc.xform(obj, ro=skelDict[key]['ori'], ws=True)

		if key == 0:
			continue

		if skelDict[key]['type'] == 'joint':
			mc.makeIdentity(obj, a=True, r=True, s=True)

		mc.parent(obj, skelDict[key]['parent'])

	# Find parents for the ribbon joints.
	for obj in mc.listRelatives(skelDict[0]['name']):

		exp = r'(.+)Skin(.+)Grp'
		reObj = re.match(exp, obj)

		if not reObj:
			continue

		skinNm = reObj.group(1)
		if skinNm.startswith('Rbn'):
			skinNm = skinNm.replace('Rbn', '')

		currPar = '%s%sJnt' % (skinNm, reObj.group(2))
		if mc.objExists(currPar):
			mc.parent(obj, currPar)

def buildSkelGrp():

	dataFld = getDataFld()	
	flPath = os.path.join(dataFld, 'Skin_GrpSkel.dat')
	readHierarchy(flPath=flPath)

def writeSelectedGeo(fn='geo.dat'):

	flPath = os.path.join(getDataFld(), fn)
	flObj = open(flPath, 'w')

	geoDict = {}

	for sel in mc.ls(sl=True):

		shps = mc.listRelatives(sel, s=True)

		if not shps:
			print '%s has no related shape.' % sel
			continue

		if not mc.nodeType(shps[0]) == 'mesh':
			print '%s has no related mesh.' % sel
			continue

		vtxPosDict = {}
		mshObj = om.MFnMesh(om.MGlobal.getSelectionListByName(sel).getDagPath(0))
		vtxPosObj = mshObj.getPoints(space=om.MSpace.kObject)

		for ix in range(mshObj.numVertices):
			vtxPosDict[ix] = [vtxPosObj[ix][0], vtxPosObj[ix][1], vtxPosObj[ix][2]]

		geoDict[sel] = vtxPosDict

		print 'Writing %s data has been done.' % sel

	pickle.dump(geoDict, flObj)
	flObj.close()

	print 'Writing geo data has been done.'

def readGeo(fn='geo.dat'):

	flPath = os.path.join(getDataFld(), fn)
	flObj = open(flPath, 'r')
	geoDict = pickle.load(flObj)
	flObj.close()

	for key in geoDict.keys():

		mshObj = om.MFnMesh(om.MGlobal.getSelectionListByName(key).getDagPath(0))

		for ix in range(mshObj.numVertices):

			vtx = '{obj}.vtx[{no}]'.format(obj=key, no=ix)
			mc.xform(vtx, os=True, t=geoDict[key][ix])

		print 'Reading %s data has been done.' % key

	print 'Reading geo data has been done.'

def writeWeight(geo='', flPath=''):

	skn = mm.eval('findRelatedSkinCluster("%s")' % geo)

	if skn:

		sknMeth = mc.getAttr('%s.skinningMethod' % skn)
		infs = mc.skinCluster(skn, q = True, inf = True)
		sknSet = mc.listConnections('%s.message' % skn, d=True, s=False)[0]
		
		flObj = open(flPath, 'w')
		wDct = {}

		wDct['influences'] = infs
		wDct['name'] = skn
		wDct['set'] = sknSet
		wDct['skinningMethod'] = sknMeth

		for ix in xrange(mc.polyEvaluate(geo, v = True)):

			currVtx = '%s.vtx[%d]' % (geo, ix)
			skinVal = mc.skinPercent(skn, currVtx, q = True, v = True)
			wDct[ix] = skinVal

		pickle.dump(wDct, flObj)
		flObj.close()
	else:
		print '%s has no related skinCluster node.' % geo

def writeSelectedWeight():

	suffix = 'Weight'
	for sel in mc.ls(sl=True):
		
		dataFld = getDataFld()
		flNmExt = '%s%s.dat' % (sel, suffix)
		flPath = os.path.join(dataFld, flNmExt)
		writeWeight(sel, flPath)
	
	mc.confirmDialog(title='Progress', message='Exporting weight is done.')

def importWeight(geo='', flPath='', srcJnt='', toJnt=''):
	'''
	Import skinweight value from 'flPath'.
	Use the value from 'srcJnt' and assign to 'toJnt'.
	'''
	flObj = open(flPath, 'r')
	wDct = pickle.load(flObj)
	flObj.close()
	
	skn = mm.eval('findRelatedSkinCluster "%s"' % geo)
	infs = mc.skinCluster(skn, q=True, inf=True)

	# Search for unhold joint indices
	uhIdcs = []
	for ix, inf in enumerate(infs):
		if not mc.getAttr('%s.liw' % inf):
			uhIdcs.append(ix)

	srcIdx = wDct['influences'].index(srcJnt)
	currIdx = infs.index(toJnt)

	for vtxIdx in xrange(mc.polyEvaluate(geo, v=True)):

		vtxNm = '%s.vtx[%d]' % (geo, vtxIdx)
		skinVals = mc.skinPercent(skn, vtxNm, q=True, v=True)

		srcVal = wDct[vtxIdx][srcIdx]

		# Pass if current vtx doesn't have source value
		if not srcVal: continue

		# Calculate unhold value
		uhVal = 0
		for uhIdx in uhIdcs:
			uhVal += skinVals[uhIdx]

		# Reduce weight value from each unhold joint
		reduceSum = 0
		for uhIdx in uhIdcs:
			if uhVal:
				reduceVal = (skinVals[uhIdx]/uhVal) * srcVal
				wlAttr = '%s.weightList[%s].weights[%s]' % (skn, vtxIdx, uhIdx)
				mc.setAttr(wlAttr, skinVals[uhIdx] - reduceVal)

				reduceSum += reduceVal

		# Normalize source weight value
		setVal = srcVal - (srcVal - reduceSum)

		# Set weight value
		wlAttr = '%s.weightList[%s].weights[%s]' % (skn, vtxIdx, currIdx)
		mc.setAttr(wlAttr, setVal)

def readWeight(geo='', flPath='', searchFor='', replaceWith='', prefix='', suffix=''):
	
	print 'Loading %s' % flPath
	flObj = open(flPath, 'r')
	wDct = pickle.load(flObj)
	flObj.close()
	
	# infs = wDct['influences']
	infs = []
	
	for oInf in wDct['influences']:

		if searchFor:
			oInf = oInf.replace(searchFor, replaceWith)

		currInf = '%s%s%s' % (prefix, oInf, suffix)

		infs.append(currInf)
	
	for inf in infs:
		if not mc.objExists(inf):
			print 'Cannot find %s ' % inf

	oSkn = mm.eval('findRelatedSkinCluster "%s"' % geo)
	if oSkn:
		mc.skinCluster(oSkn, e = True, ub = True)

	tmpSkn = mc.skinCluster(infs, geo, tsb=True)[0]
	skn = mc.rename(tmpSkn, wDct['name'])
	mc.setAttr('%s.skinningMethod' % skn, wDct['skinningMethod'])

	sknSet = mc.listConnections('%s.message' % skn, d=True, s=False)[0]
	mc.rename(sknSet, wDct['set'])
	
	for inf in infs:
		mc.setAttr('%s.liw' % inf, False)
	
	mc.setAttr('%s.normalizeWeights' % skn, False)
	mc.skinPercent(skn, geo, nrm=False, prw=100)
	mc.setAttr('%s.normalizeWeights' % skn, True)
	
	vtxNo = mc.polyEvaluate(geo, v = True)
	
	for ix in xrange(vtxNo):		
		for iy in xrange(len(infs)):
			wVal = wDct[ix][iy]
			if wVal:
				wlAttr = '%s.weightList[%s].weights[%s]' % (skn, ix, iy)
				mc.setAttr(wlAttr, wVal)
		
		# Percent calculation
		if ix == (vtxNo - 1):
			print '100%% done.'
		else:
			prePrcnt = 0
			if ix > 0:
				prePrcnt = int((float(ix - 1) / vtxNo) * 100.00)
			
			prcnt = int((float(ix) / vtxNo) * 100.00)

			if not prcnt == prePrcnt:
				print '%s%% done.' % str(prcnt)

def readSelectedWeight(searchFor='', replaceWith='', prefix='', suffix=''):
	
	sels = 	mc.ls(sl=True)
	for sel in sels:
		
		dataFld = getDataFld()

		flNm = '%sWeight.dat' % sel
		
		try:
			print 'Importing %s.' % sel

			readWeight(sel, os.path.join(dataFld, flNm), searchFor, replaceWith, prefix, suffix)
			print 'Importing %s done.' % flNm
		except:
			print 'Cannot find weight file for %s' % sel
	
	mc.select(sels)
	mc.confirmDialog(title='Progress', message='Importing weight is done.')

def writeCtrlShape(fn='ctrlShape.dat'):

	flPath = os.path.join(getDataFld(), fn)
	flObj = open(flPath, 'w')
	
	ctrlDict = {}
	
	for ctrl in mc.ls('*Ctrl'):
		
		shapes = mc.listRelatives(ctrl, s=True)

		if not shapes: continue # Continue if current controller has no shape.
		
		if mc.nodeType(shapes[0]) == 'nurbsCurve':
			
			cv = mc.getAttr('%s.spans' % shapes[0]) + mc.getAttr('%s.degree' % shapes[0])
		
			for ix in range(0, cv):
				cvName = '%s.cv[%s]' % (shapes[0], str(ix))
				ctrlDict[cvName] = mc.xform(cvName, q = True, os = True, t = True)

			# Write color property
			if mc.getAttr('%s.overrideEnabled' % shapes[0]):
				colVal = mc.getAttr('%s.overrideColor' % shapes[0])
				ctrlDict[shapes[0]] = colVal
		
	pickle.dump(ctrlDict, flObj)
	flObj.close()

def readCtrlShape(fn='ctrlShape.dat', searchFor='', replaceWith='', prefix='', suffix='', readCol=True):

	flPath = os.path.join(getDataFld(), fn)
	flObj = open(flPath, 'r')
	ctrlDict = pickle.load(flObj)
	flObj.close()

	for key in ctrlDict.keys():

		curr = '%s%s%s' % (prefix, key.replace(searchFor, replaceWith), suffix)

		if '.' in curr:
			# If current is cv, read the position.
			if mc.objExists(curr):
				mc.xform(curr, os=True, t=ctrlDict[curr])
		else:
			# If current is object, read the color.
			if mc.objExists(curr) and readCol:
				mc.setAttr('%s.overrideEnabled' % curr, 1)
				mc.setAttr('%s.overrideColor' % curr, ctrlDict[curr])

def weightImporter():
	'''
	Select the bound geometry then run the script.
	'''
	loc = mc.file(q=True, l=True)[0]
	fldPath, flNm = os.path.split(loc)
	rigDataPath = os.path.join(fldPath, 'rigData')
	mask = os.path.join(rigDataPath, '*Weight.dat')

	wgtFlPath = mc.fileDialog(dm=mask)

	if wgtFlPath:
		wgtImptr = WeightImporter()
		wgtImptr.show(wgtFlPath)

		return wgtImptr

	else:
		return None

class WeightImporter(object):

	def __init__(self):

		self.ui = 'pkWeightImporter'
		self.win = '%sWin' % self.ui
		self.wgtFlPath = ''
		self.geo = ''

	def show(self, wgtFlPath=''):

		self.wgtFlPath = wgtFlPath

		flObj = open(wgtFlPath, 'r')
		wgtDict = pickle.load(flObj)
		flObj.close()

		if mc.window(self.win, exists=True):
			mc.deleteUI(self.win)

		mc.window(self.win, t=self.ui, rtf=True)
		self.mainCl = mc.columnLayout(adj=True)

		self.infRl = mc.rowLayout(nc=2)

		self.infTsl = mc.textScrollList(nr=30)
		self.currInfTsl = mc.textScrollList(nr=30)
		
		mc.setParent('..')

		self.impBut = mc.button(l='Import', h=40, c=partial(self.importClicked))

		for inf in sorted(wgtDict['influences']):
			mc.textScrollList(self.infTsl, e=True, a=inf)

		self.geo = mc.ls(sl=True)[0]
		skn = mm.eval('findRelatedSkinCluster("%s")' % self.geo)

		for inf in sorted(mc.skinCluster(skn, q=True, inf=True)):
			mc.textScrollList(self.currInfTsl, e=True, a=inf)

		mc.showWindow(self.win)
		mc.window(self.win, e=True, w=518)
		mc.window(self.win, e=True, h=438)

	def importClicked(self, *args):

		srcInf = mc.textScrollList(self.infTsl, q=True, si=True)[0]
		tarInf = mc.textScrollList(self.currInfTsl, q=True, si=True)[0]
		
		importWeight(
						self.geo,
						self.wgtFlPath,
						srcInf,
						tarInf
					)