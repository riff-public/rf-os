import maya.OpenMaya as om
import maya.OpenMayaAnim as oma

import pymel.core as pmc

# maya.OpenMaya.MPoint
# 'assign', 'cartesianize', 'className', 'distanceTo', 'get', 'homogenize', 
# 'isEquivalent', 'origin', 'rationalize', 'this', 'w', 'x', 'y', 'z'

def print_obj(obj):
	"""Print each attribute/method of given object.
	"""
	print str(type(obj)).split('\'')[-2]
	print [ x for x in dir(obj) if not x.startswith('__')]

def dag_path_from_dag_str(dag_str=''):
	"""Get Dag Path object from dag_str.
	"""
	sel_list = om.MSelectionList()
	sel_list.add(dag_str)

	dag_path = om.MDagPath()

	sel_list.getDagPath(0, dag_path)

	return dag_path

def get_vertices_in_range(mesh='', post=None, radius=0):
	"""Gets all vertices those are 
	with in given radius away from given posts.

	:param mesh: Name of the mesh transform
	:param posts: Any sequence data type with 3 elements
	:param radius: Float
	"""
	dagpath = dag_path_from_dag_str(mesh)
	comp = om.MObject()
	vtxiter = om.MItMeshVertex(dagpath, comp)

	ompost = om.MPoint(post[0], post[1], post[2])

	vtcs = []
	while not vtxiter.isDone():

		vtxstr = '{dag}.vtx[{idx}]'.format(dag=dagpath.fullPathName(), 
												idx=vtxiter.index())

		currpost = vtxiter.position()

		if currpost.distanceTo(ompost) < radius:
			vtcs.append(vtxstr)

		vtxiter.next()

	return vtcs


def get_center_face(face):
	"""Gets center position of the given face.

	:param face: pymel.core.nodetypes.MeshFace
	"""
	facepnts = face.getPoints(space='world')
	postsum = pmc.dt.Point([0, 0, 0])
	for ix in facepnts:
		postsum += ix

	return postsum/len(facepnts)

def push():
	"""Select a face, set soft select distance then run the script.
	"""
	sel = pmc.ls(sl=True)[0]
	geo = pmc.listTransforms(sel.node())[0]
	
	# Check, if the first selection is not a poly face.
	if not isinstance(sel, pmc.MeshFace):
		msg = 'The first selection is not a poly face.'
		pmc.confirmDialog(title='Confirm', message=msg, button=['Okay'], 
							defaultButton='Okay', cancelButton='Okay', 
							dismissString='Okay')
		return False

	# Getting distance of soft selection.
	seldist = pmc.softSelect(q=True, softSelectDistance=True)

	# Center position of the selected face.
	facepost = get_center_face(sel)

	# Adding cMuscleSurfAttach
	pmc.mel.eval('cMuscleSurfAttachSetup();')
	rvt = pmc.ls(sl=True)[0]

	# Creating cluster
	vtcs = get_vertices_in_range(geo.name(), facepost, seldist)
	pmc.select(vtcs, r=True)

	clstr, clstr_xform = pmc.cluster(before=True)

	# Handle
	loc = pmc.spaceLocator()
	loc.setTransformation(rvt.getTransformation())
	pmc.parent(loc, rvt)

	# Local loc
	local_loc = pmc.spaceLocator()
	local_loc_grp = pmc.group(local_loc)
	local_loc_grp.setTransformation(rvt.getTransformation())

	loc.attr('t') >> local_loc.attr('t')
	pmc.pointConstraint(local_loc, clstr_xform, mo=True)

		