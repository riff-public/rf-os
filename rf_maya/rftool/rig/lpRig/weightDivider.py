import maya.cmds as mc
import maya.mel as mm

from functools import partial
import re

def run():
	# WeightDivider call back
	ui = WeightDivider()
	ui.show()

	return ui

def divideSelected(upVec=(0, 0, 1)):
	'''
	Select geo then source joint then divided joints.
	'''
	sels = mc.ls(sl=True)

	geo = sels[0]
	skn = mm.eval('findRelatedSkinCluster %s' % geo)
	src = sels[1]

	jnts = sels[2:]

	locs = []
	for ix, jnt in enumerate(jnts):
		currLoc = mc.spaceLocator()[0]
		locs.append(currLoc)

		mc.delete(mc.pointConstraint(jnt, currLoc))

	mc.delete(mc.aimConstraint(jnts[-1], locs[0], 
				aim=(1, 0, 0), u=upVec, wut='objectrotation', 
				wuo=src, wu=upVec))

	mc.select(cl=True)
	mc.skinCluster(skn, e=True, selectInfluenceVerts=src)
	mm.eval('PolySelectConvert 3;')
	affectedVtcs = mc.ls(sl=True, fl=True)

	divideWeight(refOriObj=locs[0], refAx='x', refPosts=locs, 
					jnts=jnts, srcJnt=src, vtcs=affectedVtcs)

	# Cleanup
	mc.delete(locs)

def divideToRibbon(geo='', rootJnt='', tipJnt='', rbnPart='', rbnSide='', rbnNs=''):

	skn = mm.eval('findRelatedSkinCluster %s' % geo)
	if not skn:
		print 'No related skin cluster.'
		return False

	rbnJnts = []

	side = '_'
	if rbnSide:
		side = '_%s_' % rbnSide

	for ix in range(5):
		currJnt = '{ns}Rbn{part}Dtl_{no}{side}Jnt'.format(ns=rbnNs, 
															part=rbnPart, no=ix+1, side=side)
		rbnJnts.append(currJnt)

	locs = []
	for ix in range(5):
		locs.append(mc.spaceLocator()[0])

	mc.delete(mc.pointConstraint(rootJnt, locs[0]))
	mc.delete(mc.aimConstraint(tipJnt, locs[0], 
								aim=(1, 0, 0), u=(0, 0, 1), wut='objectrotation', 
								wuo=rootJnt, wu=(0, 0, 1)))

	for ix in range(1, 5):
		mc.delete(mc.pointConstraint(rbnJnts[ix], locs[ix]))

	mc.select(cl=True)
	mc.skinCluster(skn, e=True, selectInfluenceVerts=rootJnt)
	affectedVtcs = []

	for sel in mc.ls(sl=True, fl=True):
		if '.vtx' in sel:
			affectedVtcs.append(sel)

	if not affectedVtcs:
		print 'No affected vertices.'
		mc.delete(locs)
		return False

	divideWeight(refOriObj=locs[0], refAx='x', refPosts=locs, 
					jnts=rbnJnts, srcJnt=rootJnt, vtcs=affectedVtcs)

	# Cleanup
	mc.delete(locs)

	print 'Dividing skin weight from {root} to ribbon joints has been done.'.format(root=rootJnt)

def autoDivideCharacter(geos=[], ns=''):

	# Neck
	neckJnt = '%sNeck_Jnt' % ns
	hdJnt = '%sHead_Jnt' % ns
	if mc.objExists(neckJnt) and mc.objExists(hdJnt):
		for geo in geos:
			divideToRibbon(geo=geo, rootJnt=neckJnt, tipJnt=hdJnt, 
							rbnPart='Neck', rbnSide='', rbnNs=ns)

	for side in ('L', 'R'):

		# UpArm
		upArmJnt = '%sUpArm_%s_Jnt' % (ns, side)
		forearmJnt = '%sForearm_%s_Jnt' % (ns, side)
		if mc.objExists(upArmJnt) and mc.objExists(forearmJnt):
			for geo in geos:
				divideToRibbon(geo=geo, rootJnt=upArmJnt, tipJnt=forearmJnt, 
								rbnPart='UpArm', rbnSide=side, rbnNs=ns)

		# Forearm
		wristJnt = '%sWrist_%s_Jnt' % (ns, side)
		if mc.objExists(forearmJnt) and mc.objExists(wristJnt):
			for geo in geos:
				divideToRibbon(geo=geo, rootJnt=forearmJnt, tipJnt=wristJnt, 
								rbnPart='Forearm', rbnSide=side, rbnNs=ns)

		# UpLeg
		upLegJnt = '%sUpLeg_%s_Jnt' % (ns, side)
		lowLegJnt = '%sLowLeg_%s_Jnt' % (ns, side)
		if mc.objExists(upLegJnt) and mc.objExists(lowLegJnt):
			for geo in geos:
				divideToRibbon(geo=geo, rootJnt=upLegJnt, tipJnt=lowLegJnt, 
								rbnPart='UpLeg', rbnSide=side, rbnNs=ns)

		# LowLeg
		ankleJnt = '%sAnkle_%s_Jnt' % (ns, side)
		if mc.objExists(lowLegJnt) and mc.objExists(ankleJnt):
			for geo in geos:
				divideToRibbon(geo=geo, rootJnt=lowLegJnt, tipJnt=ankleJnt, 
								rbnPart='LowLeg', rbnSide=side, rbnNs=ns)

def transferWeightTo(vtx='', jnt='', srcJnt='', pct=0):
	'''
	Transfering skin weight value of vtx from srcJnt to jnt with pct percent.
	'''
	geo = vtx.split('.')[0]
	skn = mm.eval('findRelatedSkinCluster "%s"' % geo)
	infs = mc.skinCluster(skn, q=True, inf=True)
	skinVals = mc.skinPercent(skn, vtx, q=True, v=True)
	wlIdx = re.findall(r'\d+', vtx)[0]

	wIdx = None
	srcIdx = None
	for ix, inf in enumerate(infs):
		if inf == jnt:
			wIdx = ix
		if inf == srcJnt:
			srcIdx = ix
	
	if wIdx == None:
		print 'Target "{jnt}" is not an influence in {skn}'.format(jnt=jnt, skn=skn)
		return False

	if srcIdx == None:
		print 'Source "{jnt}" is not an influence in {skn}'.format(jnt=srcJnt, skn=skn)
		return False

	srcWlAttr = '{skn}.weightList[{wlIdx}].weights[{wIdx}]'.format(skn=skn, wlIdx=wlIdx, wIdx=srcIdx)
	wlAttr = '{skn}.weightList[{wlIdx}].weights[{wIdx}]'.format(skn=skn, wlIdx=wlIdx, wIdx=wIdx)
	
	val = skinVals[srcIdx]*pct
	mc.setAttr(wlAttr, val)
	mc.setAttr(srcWlAttr, skinVals[srcIdx]-val)
	
	return True

def divideWeight(refOriObj='', refAx='', refPosts=[], jnts=[], srcJnt='', vtcs=[]):
	'''
	A tool to divide skin weight value from one joint to the others based on distance and direction of reference objects.
	'''
	mesh = mc.ls(vtcs[0], o=True)[0]
	geo = mc.listRelatives(mesh, p=True)[0]
	skn = mm.eval('findRelatedSkinCluster "%s"' % geo)
	
	refGrp = mc.group(em=True)
	mc.delete(mc.parentConstraint(refOriObj, refGrp))
	
	refObjs = []
	refObjDists = []
	for refPost in refPosts:
		obj = mc.group(em=True)
		mc.delete(mc.parentConstraint(refPost, obj))
		mc.parent(obj, refGrp)
		refObjs.append(refGrp)
		refObjDists.append(mc.getAttr('{obj}.t{ax}'.format(obj=obj, ax=refAx)))
	
	for vtx in vtcs:
		# Getting information for each vertex.
		vtxPost = mc.xform(vtx, q=True, t=True, ws=True)
		vtxGrp = mc.group(em=True)
		mc.xform(vtxGrp, t=vtxPost, ws=True)
		mc.parent(vtxGrp, refGrp)
		currDist = mc.getAttr('{obj}.t{ax}'.format(obj=vtxGrp, ax=refAx))
		
		if currDist <= refObjDists[0]:
			# Assign 100% to the first influence.
			transferWeightTo(vtx, jnts[0], srcJnt, 1)
			continue
		
		if currDist >= refObjDists[-1]:
			# Assign 100% to the last influence.
			transferWeightTo(vtx, jnts[-1], srcJnt, 1)
			continue
		
		for ix in range(len(refObjs)-1):
			# Compare between current ref obj and next ref obj.
			currRefDist = refObjDists[ix]
			nextRefDist = refObjDists[ix+1]
			
			if currRefDist <= currDist < nextRefDist:
				
				currRange = nextRefDist - currRefDist
				currLen = currDist - currRefDist
				currRefPct = 1 - float((1/currRange)*currLen)
				
				transferWeightTo(vtx, jnts[ix], srcJnt, currRefPct)
				if currRefPct < 1:
					transferWeightTo(vtx, jnts[ix+1], srcJnt, 1)
	mc.delete(refGrp)

class WeightDivider(object):
	'''
	A tool to divide skin weight value from one joint to the others based on distance and direction of reference objects.
	'''
	def __init__(self):
		
		self.ui = 'pkWeightDivider'
		self.win = '%sWin' % self.ui

	def show(self):
		
		oRef = ''
		oPosts = ''
		oJnts = ''
		oSrc = ''
		
		self.refObjTfg = '%sRefObjTfg' % self.ui
		self.postsTfg = '%sPostsTfg' % self.ui
		self.jntsTfg = '%sJntsTfg' % self.ui
		self.srcTfg = '%sSrcTfg' % self.ui
		
		if mc.window(self.win, exists=True):
			oRef = mc.textFieldButtonGrp(self.refObjTfg, q=True, tx=True)
			oPosts = mc.textFieldButtonGrp(self.postsTfg, q=True, tx=True)
			oJnts = mc.textFieldButtonGrp(self.jntsTfg, q=True, tx=True)
			oSrc = mc.textFieldButtonGrp(self.srcTfg, q=True, tx=True)
			mc.deleteUI(self.win)
			
		mc.window(self.win, t='pkWeightDivider', rtf=True)
		self.mainCl = mc.columnLayout(adj=True)
		
		mc.textFieldButtonGrp(self.refObjTfg, l='Ref Obj', tx=oRef, bl='Get', ed=False)
		mc.textFieldButtonGrp(self.refObjTfg, e=True, bc=partial(self.getSelectedObj, self.refObjTfg))
		mc.textFieldButtonGrp(self.refObjTfg, e=True, cw=[1, 50], h=35)
		mc.separator()
		
		mc.textFieldButtonGrp(self.postsTfg, l='Post Objs', tx=oPosts, bl='Get', ed=False)
		mc.textFieldButtonGrp(self.postsTfg, e=True, bc=partial(self.getSelectedList, self.postsTfg))
		mc.textFieldButtonGrp(self.postsTfg, e=True, cw=[1, 50], h=35)
		mc.separator()
		
		mc.textFieldButtonGrp(self.jntsTfg, l='Jnts', tx=oJnts, bl='Get', ed=False)
		mc.textFieldButtonGrp(self.jntsTfg, e=True, bc=partial(self.getSelectedList, self.jntsTfg))
		mc.textFieldButtonGrp(self.jntsTfg, e=True, cw=[1, 50], h=35)
		mc.separator()
		
		mc.textFieldButtonGrp(self.srcTfg, l='Source', tx=oSrc, bl='Get', ed=False)
		mc.textFieldButtonGrp(self.srcTfg, e=True, bc=partial(self.getSelectedObj, self.srcTfg))
		mc.textFieldButtonGrp(self.srcTfg, e=True, cw=[1, 50], h=35)
		mc.separator()
		
		self.divButton = mc.button(l='Divide', c=partial(self.div), h=35)
		
		mc.showWindow(self.win)
		mc.window(self.win, e=True, w=250)
		mc.window(self.win, e=True, h=250)

	def getSelectedObj(self, *args):
		mc.textFieldButtonGrp(args[0], e=True, tx=mc.ls(sl=True)[0])

	def getSelectedList(self, *args):
		mc.textFieldButtonGrp(args[0], e=True, tx=', '.join(mc.ls(sl=True)))

	def div(self, *args):
		
		refObj = mc.textFieldButtonGrp(self.refObjTfg, q=True, tx=True)
		posts = mc.textFieldButtonGrp(self.postsTfg, q=True, tx=True).split(', ')
		jnts = mc.textFieldButtonGrp(self.jntsTfg, q=True, tx=True).split(', ')
		src = mc.textFieldButtonGrp(self.srcTfg, q=True, tx=True)
		
		divideWeight(
						refOriObj=refObj, refAx='x',
						refPosts=posts, jnts=jnts,
						srcJnt=src, vtcs=mc.ls(sl=True, fl=True)
					)