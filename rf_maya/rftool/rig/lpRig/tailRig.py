import maya.cmds as mc

from lpRig import core as lrc
from lpRig import rigTools as lrr

import splineIkRig
reload(splineIkRig)

import fkRig
reload(fkRig)

class TailRig(object):

	def __init__(
					self,
					parent='',
					ctrlGrp='',
					jntGrp='',
					ikhGrp='',
					stillGrp='',
					tmpJnt=[],
					ax='y',
					aimVec=(0,1,0),
					upVec=(1,0,0),
					ikUpJnts=[],
					ikCtrlJnts=[],
					ikCrv='',
					ikUpCrv='',
					part='',
					side='',
				):

		if side:
			side = '_%s_' % side
		else:
			side = '_'

		# Main groups
		self.Ctrl_Grp = lrc.Null()
		self.Ctrl_Grp.name = '%sCtrl%sGrp' % (part, side)

		self.Ctrl = lrc.Control('stick')
		self.Ctrl.name = '%s%sCtrl' % (part, side)
		self.Ctrl.lockHideAttrs('tx', 'ty', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz', 'v')
		self.Ctrl.color = 'green'

		self.CtrlZr_Grp = lrc.group(self.Ctrl)
		self.CtrlZr_Grp.name = '%sCtrlZr%sGrp' % (part, side)

		self.Ctrl.add(ln='fkIk', min=0, max=1, dv=0, k=True)
		mc.parentConstraint(tmpJnt[0], self.CtrlZr_Grp)
		self.CtrlZr_Grp.parent(self.Ctrl_Grp)

		self.Jnt_Grp = lrc.Null()
		self.Jnt_Grp.name = '%sJnt%sGrp' % (part, side)

		# FK rig
		self.FkJnt_Grp = lrc.Null()
		self.FkJnt_Grp.name = '%sFkJnt%sGrp' % (part, side)
		self.FkJnts = []

		for jnt in tmpJnt:
			idx = tmpJnt.index(jnt)
			currJnt = lrr.jointAt(jnt)
			currJnt.name = '%sFk_%d%sJnt' % (part, (idx+1), side)
			self.FkJnts.append(currJnt)

			if not idx == 0:
				currJnt.parent(self.FkJnts[ idx-1 ])
			else:
				zrGrp = lrr.groupAt(currJnt)
				zrGrp.name = '%sFkJntZr_%d%sGrp' % (part, (idx+1), side)
				zrGrp.parent(self.FkJnt_Grp)

		self.FkRig = fkRig.FkRig(
									parent=parent,
									ctrlGrp=ctrlGrp,
									tmpJnt=self.FkJnts,
									part='%sFk' % part,
									side=side.replace('_', ''),
									ax=ax,
									shape='circle'
								)

		self.FkRig.Ctrl_Grp.parent(self.Ctrl_Grp)

		# IK rig
		# self.IkJnt_Grp = lrc.Null()
		# self.IkJnt_Grp.name = '%sIkJnt%sGrp' % (part, side)
		self.IkJnts = []

		for jnt in tmpJnt:
			idx = tmpJnt.index(jnt)
			currJnt = lrr.jointAt(jnt)
			currJnt.name = '%sIk_%d%sJnt' % (part, (idx+1), side)
			self.IkJnts.append(currJnt)

			if not idx == 0:
				mc.parent(currJnt, self.IkJnts[ idx-1 ])
				
		self.IkRig = splineIkRig.SplineIkRig(
												ikJnts=self.IkJnts,
												ikUpJnts=ikUpJnts,
												ikCtrlJnts=ikCtrlJnts,
												ikCrv=ikCrv,
												ikUpCrv=ikUpCrv,
												ax=ax,
												aimVec=aimVec,
												upVec=upVec,
												part='%sIk' % part,
												side=side.replace('_','')
											)

		mc.parentConstraint(parent, self.IkRig.Ctrl_Grp, mo=True)
		mc.scaleConstraint(parent, self.IkRig.Ctrl_Grp, mo=True)
		mc.parentConstraint(ctrlGrp, self.IkRig.WorOri_Grp, mo=True)
		mc.scaleConstraint(ctrlGrp, self.IkRig.WorOri_Grp, mo=True)

		# FK/IK
		self.Rev = lrc.Reverse()
		self.Rev.name = '%sFkIk%sRev' % (part, side)

		self.Ctrl.attr('fkIk') >> self.Rev.attr('ix')
		self.Rev.attr('ox') >> self.FkRig.Ctrl_Grp.attr('v')
		self.Ctrl.attr('fkIk') >> self.IkRig.Ctrl_Grp.attr('v')
		self.Ctrl.attr('fkIk') >> self.IkRig.Still_Grp.attr('v')

		for ix in range(0, len(tmpJnt)):
			parCon = lrc.parentConstraint(self.FkJnts[ix], self.IkJnts[ix], tmpJnt[ix])

			self.Ctrl.attr('fkIk') >> parCon.attr('w1')
			self.Rev.attr('ox') >> parCon.attr('w0')
				
		# Grouping
		self.FkJnt_Grp.parent(self.Jnt_Grp)

		self.IkRig.IkJnts[0].parent(self.IkRig.Jnt_Grp)
		self.IkRig.Jnt_Grp.parent(self.Jnt_Grp)
		self.IkRig.Still_Grp.parent(stillGrp)
		self.IkRig.Ikh_Grp.parent(ikhGrp)
		self.IkRig.Ctrl_Grp.parent(self.Ctrl_Grp)

		self.Jnt_Grp.parent(jntGrp)
		self.Ctrl_Grp.parent(ctrlGrp)