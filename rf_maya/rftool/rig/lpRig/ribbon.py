# Maya modules
import maya.cmds as mc

from lpRig import core as lrc
reload(lrc)
from lpRig import rigTools as lrr
reload(lrr)

def createRibbon(locA='', locB='', ax='y+', part='', side='_', hi=True):

	dist = lrc.distance(locA, locB)

	kwargs = {'size': dist, 'ax': 'y+', 'part':part, 
				'side': side.replace('_', '')}

	rbn = None
	if hi:
		rbn = RibbonIkHi(**kwargs)
	else:
		rbn = RibbonIkLow(**kwargs)

	rbn.Ctrl_Grp.snapPoint(locA)
	mc.delete(
				mc.aimConstraint(
									locB,
									rbn.Ctrl_Grp,
									aim=rbn.aimVec, u=rbn.upVec,
									wut='objectrotation',
									wuo=locA,
									wu=rbn.upVec
								)
			)
	mc.parentConstraint(locA, rbn.Ctrl_Grp, mo=True)
	mc.pointConstraint(locB, rbn.RbnEnd_Ctrl)
	rbn.RbnRoot_Ctrl.lockHideAttrs('tx', 'ty', 'tz')
	rbn.RbnRoot_Ctrl.hide()
	rbn.RbnEnd_Ctrl.lockHideAttrs('tx', 'ty', 'tz')
	rbn.RbnEnd_Ctrl.hide()

	for grp in ('Ctrl_Grp', 'Jnt_Grp', 'Skin_Grp', 'Still_Grp'):
		if not hasattr(rbn, grp) or not mc.objExists(grp):
			continue
		exec('rbn.%s.parent(grp)' % grp)
	
	return rbn

class RibbonIk(object):
	'''
	Base class for other ribbon ik objects
	Connector: self.RbnEndTwsZr_Grp, self.RbnEndTws_Grp, self.RbnRootTwsZr_Grp, self.RbnRootTws_Grp
	'''
	def __init__(
					self,
					size=1,
					ax='y+',
					part='',
					side='L'
				):

		rbnInfo = self.rbnInfo(ax)

		self.rotate = rbnInfo['rotate']
		self.aimVec = rbnInfo['aimVec']
		self.invAimVec = rbnInfo['invAimVec']
		self.upVec = rbnInfo['upVec']
		self.twsAx = rbnInfo['twsAx']
		self.sqshAx = rbnInfo['sqshAx']
		self.ro = rbnInfo['ro']

		if side:
			side = '_%s_' % side
		else:
			side = '_'

		# Main groups
		self.Ctrl_Grp = lrc.Null()
		self.Jnt_Grp = lrc.Null()
		self.Skin_Grp = lrc.Null()

		self.Ctrl_Grp.name = 'Rbn%sCtrl%sGrp' % (part, side)
		self.Jnt_Grp.name = 'Rbn%sJnt%sGrp' % (part, side)
		self.Skin_Grp.name = 'Rbn%sSkin%sGrp' % (part, side)

		mc.parentConstraint(self.Ctrl_Grp, self.Skin_Grp)
		mc.scaleConstraint(self.Ctrl_Grp, self.Skin_Grp)

		# Ribbon joints

		# Ribbon joints - Root joint
		self.RbnRoot_Jnt = lrc.Joint()
		self.RbnRoot_Jnt.name = 'Rbn%sRoot%sJnt' % (part, side)

		self.RbnRoot_Ctrl = lrc.Control('plus')
		self.RbnRoot_Ctrl.lockHideAttrs('rx', 'ry', 'rz', 'sx', 'sy', 'sz', 'v')
		self.RbnRoot_Ctrl.name = 'Rbn%sRoot%sCtrl' % (part, side)
		self.RbnRoot_Ctrl.scaleShape(size*0.1)

		if side == '_R_':
			self.RbnRoot_Ctrl.color = 'darkBlue'
		elif side == '_L_':
			self.RbnRoot_Ctrl.color = 'darkRed'
		else:
			self.RbnRoot_Ctrl.color = 'softYellow'

		self.RbnRootCtrlOfst_Grp = lrc.group(self.RbnRoot_Ctrl)
		self.RbnRootCtrlZr_Grp = lrc.group(self.RbnRootCtrlOfst_Grp)
		self.RbnRootCtrlAim_Grp = lrc.Null()
		self.RbnRootCtrlAimZr_Grp = lrc.group(self.RbnRootCtrlAim_Grp)
		
		self.RbnRootCtrlOfst_Grp.name = 'Rbn%sRootCtrlOfst%sGrp' % (part, side)
		self.RbnRootCtrlZr_Grp.name = 'Rbn%sRootCtrlZr%sGrp' % (part, side)
		self.RbnRootCtrlAim_Grp.name = 'Rbn%sRootCtrlAim%sGrp' % (part, side)
		self.RbnRootCtrlAimZr_Grp.name = 'Rbn%sRootCtrlAimZr%sGrp' % (part, side)
		
		mc.parentConstraint(self.RbnRootCtrlAim_Grp, self.RbnRoot_Jnt)
		
		self.RbnRoot_Jnt.parent(self.Jnt_Grp)
		self.RbnRootCtrlAimZr_Grp.parent(self.RbnRoot_Ctrl)
		
		# Ribbon joints - End joint
		self.RbnEnd_Jnt = lrc.Joint()
		self.RbnEnd_Jnt.name = 'Rbn%sEnd%sJnt' % (part, side)
		
		self.RbnEnd_Ctrl = lrc.Control('plus')
		self.RbnEnd_Ctrl.lockHideAttrs('rx', 'ry', 'rz', 'sx', 'sy', 'sz', 'v')
		self.RbnEnd_Ctrl.name = 'Rbn%sEnd%sCtrl' % (part, side)
		self.RbnEnd_Ctrl.scaleShape(size*0.1)

		if side == '_R_':
			self.RbnEnd_Ctrl.color = 'darkBlue'
		elif side == '_L_':
			self.RbnEnd_Ctrl.color = 'darkRed'
		else:
			self.RbnEnd_Ctrl.color = 'softYellow'

		self.RbnEndCtrlOfst_Grp = lrc.group(self.RbnEnd_Ctrl)
		self.RbnEndCtrlZr_Grp = lrc.group(self.RbnEndCtrlOfst_Grp)
		self.RbnEndCtrlAim_Grp = lrc.Null()
		self.RbnEndCtrlAimZr_Grp = lrc.group(self.RbnEndCtrlAim_Grp)

		self.RbnEndCtrlOfst_Grp.name = 'Rbn%sEndCtrlOfst%sGrp' % (part, side)
		self.RbnEndCtrlZr_Grp.name = 'Rbn%sEndCtrlZr%sGrp' % (part, side)
		self.RbnEndCtrlAim_Grp.name = 'Rbn%sEndCtrlAim%sGrp' % (part, side)
		self.RbnEndCtrlAimZr_Grp.name = 'Rbn%sEndCtrlAimZr%sGrp' % (part, side)

		mc.parentConstraint(self.RbnEndCtrlAim_Grp, self.RbnEnd_Jnt)

		self.RbnEnd_Jnt.parent(self.Jnt_Grp)
		self.RbnEndCtrlAimZr_Grp.parent(self.RbnEnd_Ctrl)

		endPos = (self.aimVec[0]*size, self.aimVec[1]*size, self.aimVec[2]*size)
		self.RbnEndCtrlZr_Grp.attr('t').v = endPos

		# Ribbon joints - Middle joint
		self.RbnMid_Jnt = lrc.Joint()
		self.RbnMid_Jnt.name = 'Rbn%sMid%sJnt' % (part, side)

		self.RbnMidJntOfst_Grp = lrc.Null()
		self.RbnMidJntOfst_Grp.name = 'Rbn%sMidJntOfst%sGrp' % (part, side)

		self.RbnMidJntZr_Grp = lrc.group(self.RbnMidJntOfst_Grp)
		self.RbnMidJntZr_Grp.name = 'Rbn%sMidJntZr%sGrp' % (part, side)

		self.Rbn_Ctrl = lrc.Control('circle')
		self.Rbn_Ctrl.lockHideAttrs('sx', 'sy', 'sz', 'v')
		self.Rbn_Ctrl.name = 'Rbn%s%sCtrl' % (part, side)
		self.Rbn_Ctrl.attr('ro').v = self.ro
		self.Rbn_Ctrl.scaleShape(size*0.25)

		if side == '_R_':
			self.Rbn_Ctrl.color = 'darkBlue'
		elif side == '_L_':
			self.Rbn_Ctrl.color = 'darkRed'
		else:
			self.Rbn_Ctrl.color = 'softYellow'
		
		self.RbnCtrlOfst_Grp = lrc.group(self.Rbn_Ctrl)
		self.RbnCtrlAim_Grp = lrc.group(self.RbnCtrlOfst_Grp)
		self.RbnCtrlZr_Grp = lrc.group(self.RbnCtrlAim_Grp)
		
		self.RbnCtrlOfst_Grp.name = 'Rbn%sCtrlOfst%sGrp' % (part, side)
		self.RbnCtrlAim_Grp.name = 'Rbn%sCtrlAim%sGrp' % (part, side)
		self.RbnCtrlZr_Grp.name = 'Rbn%sCtrlZr%sGrp' % (part, side)
		
		mc.parentConstraint(self.RbnMidJntOfst_Grp, self.RbnMid_Jnt)
		
		self.RbnMidJntZr_Grp.parent(self.Rbn_Ctrl)
		self.RbnMid_Jnt.parent(self.Jnt_Grp)

		self.Rbn_Ctrl.add(ln='autoTwist', at='float', k=True, min=0, max=1)
		self.Rbn_Ctrl.add(ln='autoSquash', at='float', k=True, min=0, max=1)

		rbnCtrlShp=lrc.Dag(self.Rbn_Ctrl.shape)
		rbnCtrlShp.add(ln='restLength', at='float', k=True, min=0)
		rbnCtrlShp.add(ln='upVector', sn='u', at='double3', k=False)
		rbnCtrlShp.add(p='upVector', ln='upVectorX', sn='ux', at='double', k=False)
		rbnCtrlShp.add(p='upVector', ln='upVectorY', sn='uy', at='double', k=False)
		rbnCtrlShp.add(p='upVector', ln='upVectorZ', sn='uz', at='double', k=False)
		rbnCtrlShp.attr('u').v = self.upVec

		# Ribbon rig
		rootCtrlAimCon = lrc.aimConstraint(
											self.RbnEnd_Ctrl,
											self.RbnRootCtrlAimZr_Grp,
											aim=self.aimVec,
											u=self.upVec,
											wut='objectrotation',
											wuo='%s' % self.RbnRoot_Ctrl,
											wu=self.upVec
										)
		endCtrlAimCon = lrc.aimConstraint(
											self.RbnRoot_Ctrl,
											self.RbnEndCtrlAimZr_Grp,
											aim=self.invAimVec,
											u=self.upVec,
											wut='objectrotation',
											wuo='%s' % self.RbnEnd_Ctrl,
											wu=self.upVec
										)
		mc.pointConstraint(
								self.RbnRoot_Ctrl,
								self.RbnEnd_Ctrl,
								self.RbnCtrlZr_Grp
							)
		midCtrlAimCon = lrc.aimConstraint(
											self.RbnEnd_Ctrl,
											self.RbnCtrlAim_Grp,
											aim=self.aimVec,
											u=self.upVec,
											wut='objectrotation',
											wuo='%s' % self.RbnCtrlZr_Grp,
											wu=self.upVec
										)

		mc.parent(self.RbnRootCtrlZr_Grp, self.RbnEndCtrlZr_Grp, self.RbnCtrlZr_Grp, self.Ctrl_Grp)

		rbnCtrlShp.attr('u') >> rootCtrlAimCon.attr('u')
		rbnCtrlShp.attr('u') >> rootCtrlAimCon.attr('wu')
		rbnCtrlShp.attr('u') >> endCtrlAimCon.attr('u')
		rbnCtrlShp.attr('u') >> endCtrlAimCon.attr('wu')
		rbnCtrlShp.attr('u') >> midCtrlAimCon.attr('u')
		rbnCtrlShp.attr('u') >> midCtrlAimCon.attr('wu')

		# Twist nodes
		self.RbnRootTws_Grp = lrc.Null()
		self.RbnRootTwsZr_Grp = lrc.group(self.RbnRootTws_Grp)
		self.RbnEndTws_Grp = lrc.Null()
		self.RbnEndTwsZr_Grp = lrc.group(self.RbnEndTws_Grp)

		self.RbnRootTws_Grp.name = 'Rbn%sRootTws%sGrp' % (part, side)
		self.RbnRootTwsZr_Grp.name = 'Rbn%sRootTwsZr%sGrp' % (part, side)
		self.RbnEndTws_Grp.name = 'Rbn%sEndTws%sGrp' % (part, side)
		self.RbnEndTwsZr_Grp.name = 'Rbn%sEndTwsZr%sGrp' % (part, side)

		self.RbnEndTwsZr_Grp.attr('t').v = endPos
		self.RbnRootTws_Grp.attr('ro').v = self.ro
		self.RbnEndTwsZr_Grp.attr('ro').v = self.ro

		self.RbnRootTwsZr_Grp.parent(self.Ctrl_Grp)
		self.RbnEndTwsZr_Grp.parent(self.Ctrl_Grp)

		# Twist distribution
		self.RbnRootTws_Pma = lrc.PlusMinusAverage()
		self.RbnEndTws_Pma = lrc.PlusMinusAverage()

		self.RbnRootTws_Pma.name = 'Rbn%sRootTws%sPma' % (part, side)
		self.RbnEndTws_Pma.name = 'Rbn%sEndTws%sPma' % (part, side)

		rbnCtrlShp.add(ln='rootTwistAmp', dv=1)
		rbnCtrlShp.add(ln='endTwistAmp', dv=1)

		self.RbnRootTwsAmp_Mul = lrc.MultDoubleLinear()
		self.RbnEndTwsAmp_Mul = lrc.MultDoubleLinear()
		self.RbnRootAtTws_Mul = lrc.MultDoubleLinear()
		self.RbnEndAtTws_Mul = lrc.MultDoubleLinear()

		self.RbnRootTwsAmp_Mul.name = 'Rbn%sRootTwsAmp%sMul' % (part, side)
		self.RbnEndTwsAmp_Mul.name = 'Rbn%sEndTwsAmp%sMul' % (part, side)
		self.RbnRootAtTws_Mul.name = 'Rbn%sRootAtTws%sMul' % (part, side)
		self.RbnEndAtTws_Mul.name = 'Rbn%sEndAtTws%sMul' % (part, side)

		self.RbnRootTws_Grp.attr(self.twsAx) >> self.RbnRootTwsAmp_Mul.attr('i1')
		self.RbnEndTws_Grp.attr(self.twsAx) >> self.RbnEndTwsAmp_Mul.attr('i1')
		rbnCtrlShp.attr('rootTwistAmp') >> self.RbnRootTwsAmp_Mul.attr('i2')
		rbnCtrlShp.attr('endTwistAmp') >> self.RbnEndTwsAmp_Mul.attr('i2')
		
		self.Rbn_Ctrl.attr('autoTwist') >> self.RbnRootAtTws_Mul.attr('i1')
		self.Rbn_Ctrl.attr('autoTwist') >> self.RbnEndAtTws_Mul.attr('i1')
		self.RbnRootTwsAmp_Mul.attr('o') >> self.RbnRootAtTws_Mul.attr('i2')
		self.RbnEndTwsAmp_Mul.attr('o') >> self.RbnEndAtTws_Mul.attr('i2')
		
		self.RbnRootAtTws_Mul.attr('o') >> self.RbnRootTws_Pma.last1D()
		self.RbnEndAtTws_Mul.attr('o') >> self.RbnEndTws_Pma.last1D()
		
		# Atr squash nodes
		self.RbnRootPos_Grp = lrc.Null()
		self.RbnMidPos_Grp = lrc.Null()
		self.RbnEndPos_Grp = lrc.Null()

		self.RbnRootPos_Grp.name = 'Rbn%sRootPos%sGrp' % (part, side)
		self.RbnMidPos_Grp.name = 'Rbn%sMidPos%sGrp' % (part, side)
		self.RbnEndPos_Grp.name = 'Rbn%sEndPos%sGrp' % (part, side)

		mc.pointConstraint(self.RbnRoot_Jnt, self.RbnRootPos_Grp)
		mc.pointConstraint(self.RbnMid_Jnt, self.RbnMidPos_Grp)
		mc.pointConstraint(self.RbnEnd_Jnt, self.RbnEndPos_Grp)
		
		mc.parent(self.RbnRootPos_Grp, self.RbnMidPos_Grp, self.RbnEndPos_Grp, self.Ctrl_Grp)

		self.RbnRoot_Dist = lrc.DistanceBetween()
		self.RbnEnd_Dist = lrc.DistanceBetween()

		self.RbnRoot_Dist.name = 'Rbn%sRoot%sDist' % (part, side)
		self.RbnEnd_Dist.name = 'Rbn%sEnd%sDist' % (part, side)

		self.RbnRootPos_Grp.attr('t') >> self.RbnRoot_Dist.attr('p1')
		self.RbnMidPos_Grp.attr('t') >> self.RbnRoot_Dist.attr('p2')
		self.RbnMidPos_Grp.attr('t') >> self.RbnEnd_Dist.attr('p1')
		self.RbnEndPos_Grp.attr('t') >> self.RbnEnd_Dist.attr('p2')

		self.RbnLen_Add = lrc.AddDoubleLinear()
		self.RbnLen_Add.name = 'Rbn%sLen%sAdd' % (part, side)

		self.RbnRoot_Dist.attr('d') >> self.RbnLen_Add.attr('i1')
		self.RbnEnd_Dist.attr('d') >> self.RbnLen_Add.attr('i2')

		rbnCtrlShp.attr('restLength').v = size
		rbnCtrlShp.attr('restLength').lock = True

		self.RbnSqsDiv_Mdv = lrc.MultiplyDivide()
		self.RbnSqsPow_Mdv = lrc.MultiplyDivide()
		self.RbnSqsNorm_Mdv = lrc.MultiplyDivide()

		self.RbnSqsDiv_Mdv.name = 'Rbn%sSqsDiv%sMdv' % (part, side)
		self.RbnSqsPow_Mdv.name = 'Rbn%sSqsPow%sMdv' % (part, side)
		self.RbnSqsNorm_Mdv.name = 'Rbn%sSqsNorm%sMdv' % (part, side)

		self.RbnSqsNorm_Mdv.attr('operation').v = 2
		self.RbnLen_Add.attr('o') >> self.RbnSqsNorm_Mdv.attr('i1x')
		rbnCtrlShp.attr('restLength') >> self.RbnSqsNorm_Mdv.attr('i2x')
		
		self.RbnSqsPow_Mdv.attr('operation').v = 3
		self.RbnSqsNorm_Mdv.attr('ox') >> self.RbnSqsPow_Mdv.attr('i1x')
		self.RbnSqsPow_Mdv.attr('i2x').v = 2
		
		self.RbnSqsDiv_Mdv.attr('operation').v = 2
		self.RbnSqsDiv_Mdv.attr('i1x').v = 1
		self.RbnSqsPow_Mdv.attr('ox') >> self.RbnSqsDiv_Mdv.attr('i2x')
		
	def rbnInfo(self, ax='y+'):
		'''
		Generating information dict, regarding given 'ax'
		'''
		twsAx =  'rx'
		sqshAx = ('sy', 'sz')
		aimVec = (1, 0, 0)
		invAimVec = (-1, 0, 0)
		upVec = (0, 0, 1)
		rotate = (0, 0, -90)
		ro	= 0
		
		if ax == 'x+':
			twsAx = 'rx'
			sqshAx = ('sy', 'sz')
			aimVec = (1, 0, 0)
			invAimVec = (-1, 0, 0)
			upVec = (0, 0, 1)
			rotate = (0, 0, -90)
			ro	= 0
		
		elif ax == 'x-':
			twsAx =  'rx'
			sqshAx = ('sy', 'sz')
			aimVec = (-1, 0, 0)
			invAimVec = (1, 0, 0)
			upVec = (0, 0, 1)
			rotate = (0, 0, 90)
			ro = 0
		
		elif ax == 'y+':
			twsAx =  'ry'
			sqshAx = ('sx', 'sz')
			aimVec = (0, 1, 0)
			invAimVec = (0, -1, 0)
			upVec = (0, 0, 1)
			rotate = (0, 0, 0)
			ro = 1
		
		elif ax == 'y-':
			twsAx = 'ry'
			sqshAx = ('sx', 'sz')
			aimVec = (0, -1, 0)
			invAimVec = (0, 1, 0)
			upVec = (0, 0, 1)
			rotate = (0, 0, 180)
			ro = 1
		
		elif ax == 'z+':
			twsAx =  'rz'
			sqshAx = ('sx', 'sy')
			aimVec = (0, 0, 1)
			invAimVec = (0, 0, -1)
			upVec = (0, 1, 0)
			rotate = (90, 0, 180)
			ro = 2
		
		elif ax == 'z-':
			twsAx =  'rz'
			sqshAx = ('sx', 'sy')
			aimVec = (0, 0, -1)
			invAimVec = (0, 0, 1)
			upVec = (0, 1, 0)
			rotate = (-90, 0, 0)
			ro = 2
		
		return {
					'twsAx': twsAx,
					'sqshAx': sqshAx,
					'upVec': upVec,
					'rotate': rotate,
					'aimVec': aimVec,
					'invAimVec': invAimVec,
					'ro': ro
				}

class RibbonIkHi(RibbonIk):

	def __init__(
					self,
					size=1,
					ax='y+',
					part='',
					side='L'
				):

		super(RibbonIkHi, self).__init__(size, ax, part, side)

		if side:
			side = '_%s_' % side
		else:
			side = '_'
		
		# Main group
		self.Still_Grp = lrc.Null()
		self.Still_Grp.name = 'Rbn%sStill%sGrp' % (part, side)

		# Ribbon geo
		self.Rbn_Nrb = lrr.ribbonSurface()
		self.Rbn_Nrb.name = 'Rbn%s%sNrb' % (part, side)
		self.Rbn_Nrb.scaleShape(size)
		self.Rbn_Nrb.rotateShape(self.rotate)

		self.Rbn_Nrb.parent(self.Still_Grp)

		# Point on surface
		self.RbnPos_Grp = lrc.Null()
		self.RbnDtlCtrl_Grp = lrc.Null()

		self.RbnPos_Grp.name = 'Rbn%sPos%sGrp' % (part, side)
		self.RbnDtlCtrl_Grp.name = 'Rbn%sDtlCtrl%sGrp' % (part, side)

		rbnCtrlShp = lrc.Dag(self.Rbn_Ctrl.shape)

		rbnCtrlShp.add(
							ln='detail',
							at='double',
							k=True,
							min=0,
							max=1
						)
		rbnCtrlShp.attr('detail') >> self.RbnDtlCtrl_Grp.attr('v')
		self.RbnPos_Grp.parent(self.Still_Grp)
		self.RbnDtlCtrl_Grp.parent(self.Ctrl_Grp)

		# Added two poss
		# Added two poss - Root Pos
		self.RbnRootDtlPos_Grp, self.RbnRootDtl_Posi = self.createPosi(self.Rbn_Nrb, '%sRootDtl' % part, '', side)
		self.RbnRootDtlPos_Grp.parent(self.RbnPos_Grp)
		self.RbnRootDtl_Posi.attr('parameterV').v = 0.01

		# Added two poss - End Pos
		self.RbnEndDtlPos_Grp, self.RbnEndDtl_Posi = self.createPosi(self.Rbn_Nrb, '%sEndDtl' % part, '', side)
		self.RbnEndDtlPos_Grp.parent(self.RbnPos_Grp)
		self.RbnEndDtl_Posi.attr('parameterV').v = 0.99

		poss = []
		posis = []

		for ix in range(5):

			pos, posi = self.createPosi(self.Rbn_Nrb, part, '_%s' % str(ix+1), side)
			
			aimCon = lrc.AimConstraint()
			aimCon.name = '%s_aimConstraint' % pos
			aimCon.attr('a').v = self.aimVec
			aimCon.attr('u').v = self.upVec
			
			posi.attr('n') >> aimCon.attr('wu')
			posi.attr('tv') >> aimCon.attr('tg[0].tt')
			aimCon.attr('cr') >> pos.attr('r')
			
			aimCon.parent(pos)
			pos.parent(self.RbnPos_Grp)
			
			poss.append(pos)
			posis.append(posi)

		posis[0].attr('parameterV').v = 0.1
		posis[1].attr('parameterV').v = 0.25
		posis[2].attr('parameterV').v = 0.5
		posis[3].attr('parameterV').v = 0.75
		posis[4].attr('parameterV').v = 0.9

		(self.RbnPos_1_Grp,
		self.RbnPos_2_Grp,
		self.RbnPos_3_Grp,
		self.RbnPos_4_Grp,
		self.RbnPos_5_Grp
		) = poss
		(self.Rbn_1_Posi,
		self.Rbn_2_Posi,
		self.Rbn_3_Posi,
		self.Rbn_4_Posi,
		self.Rbn_5_Posi
		) = posis

		# Detail controllers and skin joints
		dtlCtrls = []
		dtlJnts = []
		dtlTwss = []
		dtlZrs = []
		sqsPmas = []
		atSqsBlnds = []
		atSqsAdds = []
		atSqsMuls = []
		atSqsAmpPmas = []

		atSqsMulDict = {0: 0.33, 1: 0.66, 2: 1, 3: 0.66, 4: 0.33}

		for ix in range(5) :
			
			dtlCtrl = lrc.Control('circle')
			dtlJnt = lrc.Joint()
			dtlTwst = lrc.Null()
			dtlZr = lrc.Null()

			dtlCtrl.name = 'Rbn%sDtl_%s%sCtrl' % (part, ix+1, side)
			dtlJnt.name = 'Rbn%sDtl_%s%sJnt' % (part, ix+1, side)
			dtlTwst.name = 'Rbn%sDtl_%s%sTws' % (part, ix+1, side)
			dtlZr.name = 'Rbn%sDtlCtrlZr_%s%sGrp' % (part, ix+1, side)

			sqshPma = lrc.PlusMinusAverage()
			atSqsMul = lrc.MultDoubleLinear()
			atSqsAdd = lrc.AddDoubleLinear()
			atSqsBlnd = lrc.BlendTwoAttr()
			atSqsAmpPma = lrc.PlusMinusAverage()

			sqshPma.name = 'Rbn%sDtlSqs_%s%sPma' % (part, ix+1, side)
			atSqsMul.name = 'Rbn%sDtlAtSqs_%s%sMul' % (part, ix+1, side)
			atSqsAdd.name = 'Rbn%sDtlAtSqs_%s%sAdd' % (part, ix+1, side)
			atSqsBlnd.name = 'Rbn%sDtlAtSqs_%s%sBlend' % (part, ix+1, side)
			atSqsAmpPma.name = 'Rbn%sDtlAtSqs_%s%sPma' % (part, ix+1, side)
			
			dtlCtrls.append(dtlCtrl)
			dtlJnts.append(dtlJnt)
			dtlTwss.append(dtlTwst)
			dtlZrs.append(dtlZr)
			sqsPmas.append(sqshPma)
			atSqsMuls.append(atSqsMul)
			atSqsAdds.append(atSqsAdd)
			atSqsBlnds.append(atSqsBlnd)
			atSqsAmpPmas.append(atSqsAmpPma)

			# Shape adjustment
			dtlCtrl.rotateShape(self.rotate)
			dtlCtrl.scaleShape(size * 0.25)
			dtlCtrl.color = 'softBlue'

			# Hierarchy setup
			dtlCtrl.parent(dtlTwst)
			dtlTwst.parent(dtlZr)
			
			dtlZr.parent(self.RbnDtlCtrl_Grp)
			dtlJnt.parent(self.Skin_Grp)

			# Sqs setup
			currAtSqsAmp = atSqsMulDict[ix]
			currAmpAttr = 'autoSquash%sAmp' % str(1+ix)
			rbnCtrlShp.add(ln=currAmpAttr, dv=currAtSqsAmp)
			dtlCtrl.add(ln='squash', k=True)

			self.Rbn_Ctrl.attr('autoSquash') >> atSqsBlnd.attr('ab')
			atSqsBlnd.add(ln='default', min=1, max=1, dv=1, k=True)
			atSqsBlnd.attr('default') >> atSqsBlnd.last()

			atSqsMul.add(ln='amp', k=True)
			atSqsMul.attr('amp').v = currAtSqsAmp
			atSqsMul.attr('amp') >> atSqsMul.attr('i1')
			self.RbnSqsDiv_Mdv.attr('ox') >> atSqsMul.attr('i2')

			atSqsAmpPma.add(ln='default', k=True)
			atSqsAmpPma.attr('operation').v = 2
			atSqsAmpPma.attr('default').v = 1
			atSqsAmpPma.attr('default') >> atSqsAmpPma.last1D()
			rbnCtrlShp.attr(currAmpAttr) >> atSqsAmpPma.last1D()

			atSqsAmpPma.attr('output1D') >> atSqsAdd.attr('i1')
			atSqsMul.attr('o') >> atSqsAdd.attr('i2')
			atSqsAdd.attr('o') >> atSqsBlnd.last()

			dtlCtrl.attr('squash') >> sqshPma.last1D()
			atSqsBlnd.attr('o') >> sqshPma.last1D()
			sqshPma.attr('output1D') >> dtlJnt.attr(self.sqshAx[0])
			sqshPma.attr('output1D') >> dtlJnt.attr(self.sqshAx[1])
			dtlCtrl.lockHideAttrs('sx', 'sy', 'sz', 'v')

			# if 0 < ix < 4:
			# 	mc.parentConstraint(poss[ix], dtlZr)
			# 	mc.parentConstraint(dtlCtrl, dtlJnt)

			# else:
			# 	mc.parentConstraint(poss[ix], dtlZr)
			# 	mc.parentConstraint(dtlCtrl, dtlJnt)

			if ix == 0:
				mc.pointConstraint(self.RbnRootDtlPos_Grp, dtlZr)
				mc.orientConstraint(poss[ix], dtlZr)
				mc.parentConstraint(dtlCtrl, dtlJnt)

			elif ix == 4:
				mc.pointConstraint(self.RbnEndDtlPos_Grp, dtlZr)
				mc.orientConstraint(poss[ix], dtlZr)
				mc.parentConstraint(dtlCtrl, dtlJnt)

			else:
				mc.parentConstraint(poss[ix], dtlZr)
				mc.parentConstraint(dtlCtrl, dtlJnt)

		(self.RbnDtl_1_Ctrl, 
		self.RbnDtl_2_Ctrl, 
		self.RbnDtl_3_Ctrl, 
		self.RbnDtl_4_Ctrl, 
		self.RbnDtl_5_Ctrl
		) = dtlCtrls
		(self.RbnDtl_1_Jnt, 
		self.RbnDtl_2_Jnt, 
		self.RbnDtl_3_Jnt, 
		self.RbnDtl_4_Jnt, 
		self.RbnDtl_5_Jnt
		) = dtlJnts
		(self.RbnDtlTws_1_Grp, 
		self.RbnDtlTws_2_Grp, 
		self.RbnDtlTws_3_Grp, 
		self.RbnDtlTws_4_Grp, 
		self.RbnDtlTws_5_Grp
		) = dtlTwss
		(self.RbnDtlZr_1_Grp, 
		self.RbnDtlZr_2_Grp, 
		self.RbnDtlZr_3_Grp, 
		self.RbnDtlZr_4_Grp, 
		self.RbnDtlZr_5_Grp
		) = dtlZrs
		(self.RbnDtlSqs_1_Pma, 
		self.RbnDtlSqs_2_Pma, 
		self.RbnDtlSqs_3_Pma, 
		self.RbnDtlSqs_4_Pma, 
		self.RbnDtlSqs_5_Pma
		) = sqsPmas
		(self.RbnDtlAtSqs_1_Blnd, 
		self.RbnDtlAtSqs_2_Blnd,
		self.RbnDtlAtSqs_3_Blnd,
		self.RbnDtlAtSqs_4_Blnd,
		self.RbnDtlAtSqs_5_Blnd
		) = atSqsBlnds
		(self.RbnDtlAtSqs_1_Mul, 
		self.RbnDtlAtSqs_2_Mul, 
		self.RbnDtlAtSqs_3_Mul, 
		self.RbnDtlAtSqs_4_Mul, 
		self.RbnDtlAtSqs_5_Mul
		) = atSqsMuls
		(self.RbnDtlAtSqs_1_Add, 
		self.RbnDtlAtSqs_2_Add, 
		self.RbnDtlAtSqs_3_Add, 
		self.RbnDtlAtSqs_4_Add, 
		self.RbnDtlAtSqs_5_Add
		) = atSqsAdds
		(self.RbnDtlAtSqsAmp_1_Pma, 
		self.RbnDtlAtSqsAmp_2_Pma, 
		self.RbnDtlAtSqsAmp_3_Pma, 
		self.RbnDtlAtSqsAmp_4_Pma, 
		self.RbnDtlAtSqsAmp_5_Pma
		) = atSqsAmpPmas

		# Twist distribution
		self.Rbn_Ctrl.add(ln='rootTwist', at='float', k=True)
		self.Rbn_Ctrl.add(ln='endTwist', at='float', k=True)

		self.Rbn_Ctrl.attr('rootTwist') >> self.RbnRootTws_Pma.last1D()
		self.Rbn_Ctrl.attr('endTwist') 	>> self.RbnEndTws_Pma.last1D()

		# Twist on joint 1
		self.RbnRootTws_Pma.attr('o1') >> dtlTwss[0].attr(self.twsAx)

		# Twist on joint 2-4
		twsMdvs = []
		twsPmas = []
		for ix in range(3) :
			
			endVal = 0.25*(ix+1)
			startVal = 0.25*(4-(ix+1))
			
			twsMdv = lrc.MultiplyDivide()
			twsPma = lrc.PlusMinusAverage()
			twsMdv.name = 'Rbn%sDtlTws_%s%sMdv' % (part, ix+2, side)
			twsPma.name = 'Rbn%sDtlTws_%s%sPma' % (part, ix+2, side)
			twsMdvs.append(twsMdv)
			twsPmas.append(twsPma)
			
			twsMdv.attr('i1x').v = endVal
			twsMdv.attr('i1y').v = startVal
			self.RbnEndTws_Pma.attr('o1') >> twsMdv.attr('i2x')
			self.RbnRootTws_Pma.attr('o1') >> twsMdv.attr('i2y')
			twsMdv.attr('ox') >> twsPma.last1D()
			twsMdv.attr('oy') >> twsPma.last1D()
			twsPma.attr('output1D') >> dtlTwss[ix+1].attr(self.twsAx)

		(self.RbnDtlTws_2_Mdv,
		self.RbnDtlTws_3_Mdv,
		self.RbnDtlTws_4_Mdv) = twsMdvs
		(self.RbnDtlTws_2_Pma,
		self.RbnDtlTws_3_Pma,
		self.RbnDtlTws_4_Pma) = twsPmas

		# Twist on joint 5
		self.RbnEndTws_Pma.attr('o1') >> dtlTwss[4].attr(self.twsAx)

		# Binding skin
		skc = mc.skinCluster(
								self.RbnRoot_Jnt,
								self.RbnMid_Jnt,
								self.RbnEnd_Jnt,
								self.Rbn_Nrb,
								dr=7,
								mi=2
							)[0]

		mc.skinPercent(skc, '%s.cv[0:3][7]' % self.Rbn_Nrb, tv=[self.RbnEnd_Jnt, 1])
		mc.skinPercent(skc, '%s.cv[0:3][6]' % self.Rbn_Nrb, tv=[self.RbnMid_Jnt, 0.2])
		mc.skinPercent(skc, '%s.cv[0:3][5]' % self.Rbn_Nrb, tv=[self.RbnMid_Jnt, 0.5])
		mc.skinPercent(skc, '%s.cv[0:3][4]' % self.Rbn_Nrb, tv=[self.RbnEnd_Jnt, 0.1])
		mc.skinPercent(skc, '%s.cv[0:3][3]' % self.Rbn_Nrb, tv=[self.RbnRoot_Jnt, 0.1])
		mc.skinPercent(skc, '%s.cv[0:3][2]' % self.Rbn_Nrb, tv=[self.RbnRoot_Jnt, 0.5])
		mc.skinPercent(skc, '%s.cv[0:3][1]' % self.Rbn_Nrb, tv=[self.RbnRoot_Jnt, 0.8])
		mc.skinPercent(skc, '%s.cv[0:3][0]' % self.Rbn_Nrb, tv=[self.RbnRoot_Jnt, 1])
		
		# Squash
		self.Rbn_Ctrl.add(ln='squash',  at='float',  k=True)
		
		# Squash - Amplifier
		self.RbnSqsAmp_Mul = lrc.MultDoubleLinear()
		self.RbnSqsAmp_Mul.name = 'Rbn%sSqsAmp%sJnt' % (part, side)
		self.RbnSqsAmp_Mul.attr('i2').v = 0.1
		self.Rbn_Ctrl.attr('squash') >> self.RbnSqsAmp_Mul.attr('i1')
		
		for ix in range(5) :
			self.RbnSqsAmp_Mul.attr('o') >> sqsPmas[ix].last1D()

	def createPosi(self, nrbObj=None, part='', no='', side=''):

		pos = lrc.Null()
		pos.name = 'Rbn{part}Pos{no}{side}Grp'.format(part=part, no=no, side=side)

		posi = lrc.PointOnSurfaceInfo()
		posi.name = 'Rbn{part}Pos{no}{side}Posi'.format(part=part, no=no, side=side)

		nrbObj.attr('worldSpace[0]') >> posi.attr('is')
		posi.attr('position') >> pos.attr('t')
		posi.attr('parameterU').v = 0.5
		posi.attr('turnOnPercentage').v = 1

		return pos, posi

class RibbonIkLow(RibbonIk):

	def __init__(self, size=1, ax='y+', part='', side='L'):

		super(RibbonIkLow, self).__init__(size, ax, part, side)

		if side:
			side = '_%s_' % side
		else:
			side = '_'
		
		# Skin joint
		self.Rbn_Jnt = lrc.Joint()
		self.RbnJntOfst_Grp = lrc.group(self.Rbn_Jnt)
		self.RbnJntZr_Grp = lrc.group(self.RbnJntOfst_Grp)

		self.Rbn_Jnt.name = 'Rbn%s%sJnt' % (part, side)
		self.RbnJntOfst_Grp.name = 'Rbn%sJntOfst%sGrp' % (part, side)
		self.RbnJntZr_Grp.name = 'Rbn%sJntZr%sGrp' % (part, side)

		mc.parentConstraint(self.Rbn_Ctrl, self.RbnJntZr_Grp)
		self.RbnJntZr_Grp.parent(self.Skin_Grp)

		# Squash
		self.Rbn_Ctrl.add(ln='squash',  at='float',  k=True)

		# Squash - Amplifier
		self.RbnAtSqs_Blnd = lrc.BlendTwoAttr()
		self.RbnSqsAmp_Mul = lrc.MultDoubleLinear()
		self.RbnAtSqs_Blnd.name = 'Rbn%sAtSqs%sBlnd' % (part, side)
		self.RbnSqsAmp_Mul.name = 'Rbn%sSqsAmp%sMul' % (part, side)

		self.RbnSqsAmp_Mul.attr('i2').v = 0.1
		self.Rbn_Ctrl.attr('squash') >> self.RbnSqsAmp_Mul.attr('i1')

		self.RbnSqs_Add = lrc.AddDoubleLinear()
		self.RbnSqs_Add.name = 'Rbn%sSqs%sAdd' % (part, side)

		self.RbnAtSqs_Blnd.add(ln='default',  dv=1,  min=1,  max=1,  k=True)
		self.Rbn_Ctrl.attr('autoSquash') >> self.RbnAtSqs_Blnd.attr('ab')
		self.RbnAtSqs_Blnd.attr('default') >> self.RbnAtSqs_Blnd.last()
		self.RbnSqsDiv_Mdv.attr('ox') >> self.RbnAtSqs_Blnd.last()
		
		self.RbnSqsAmp_Mul.attr('o') >> self.RbnSqs_Add.attr('i1')
		self.RbnAtSqs_Blnd.attr('o') >> self.RbnSqs_Add.attr('i2')
		self.RbnSqs_Add.attr('o') >> self.Rbn_Jnt.attr(self.sqshAx[0])
		self.RbnSqs_Add.attr('o') >> self.Rbn_Jnt.attr(self.sqshAx[1])
		
		# Twist distribution
		self.RbnTws_Mdv = lrc.MultiplyDivide()
		self.RbnTws_Pma = lrc.PlusMinusAverage()
		self.RbnTws_Mdv.name = 'Rbn%sTws%sMdv' % (part, side)
		self.RbnTws_Pma.name = 'Rbn%sTws%sPma' % (part, side)
		
		self.RbnTws_Mdv.attr('i1x').v = 0.5
		self.RbnTws_Mdv.attr('i1y').v = 0.5
		
		self.RbnRootTws_Pma.attr('o1') >> self.RbnTws_Mdv.attr('i2y')
		self.RbnEndTws_Pma.attr('o1') >> self.RbnTws_Mdv.attr('i2x')
		self.RbnTws_Mdv.attr('ox') >> self.RbnTws_Pma.last1D()
		self.RbnTws_Mdv.attr('oy') >> self.RbnTws_Pma.last1D()
		self.RbnTws_Pma.attr('output1D') >> self.RbnJntOfst_Grp.attr(self.twsAx)

class RibbonSurface(object):
	
	def __init__(self, part='', size=0, vPatch=5, jntNo=5, side='L'):
		
		if side:
			side = '_%s_' % side
		else:
			side = '_'
		
		self.Still_Grp = lrc.Null()
		self.Skin_Grp = lrc.Null()

		self.Still_Grp.name = 'RbnStill%s%sGrp' % (part, side)
		self.Skin_Grp.name = 'RbnSkin%s%sGrp' % (part, side)

		self.Rbn_Nrb = lrr.ribbonSurface(vPatch)
		self.Rbn_Nrb.name = 'Rbn%s%sNrb' % (part, side)
		self.Rbn_Nrb.scaleShape(size)

		self.RbnNrbZr_Grp = lrc.group(self.Rbn_Nrb)
		self.RbnNrbZr_Grp.name = 'Rbn%sZr%sNrb' % (part, side)
		self.RbnNrbZr_Grp.parent(self.Still_Grp)

		self.skinJnts = []
		self.skinJntZrGrps = []
		self.poss = []
		self.posis = []

		for ix in range(jntNo):

			pos = lrc.Null()
			posi = lrc.PointOnSurfaceInfo()

			pos.name = 'RbnPos%s_%s%sGrp' % (part, ix+1, side)
			posi.name = 'Rbn%s_%s%sPosi' % (part, ix+1, side)
			
			self.Rbn_Nrb.attr('worldSpace[0]') >> posi.attr('is')
			posi.attr('position') >> pos.attr('t')
			posi.attr('parameterU').v = 0.5
			posi.attr('turnOnPercentage').v = 1
			
			aimCon = lrc.AimConstraint()
			aimCon.name = '%s_aimConstraint' % pos
			aimCon.attr('a').v = (0, 1, 0)
			aimCon.attr('u').v = (1, 0, 0)
			
			posi.attr('n') >> aimCon.attr('wu')
			posi.attr('tv') >> aimCon.attr('tg[0].tt')
			aimCon.attr('cr') >> pos.attr('r')
			
			aimCon.parent(pos)
			aimCon.attr('t').v = (0, 0, 0)
			aimCon.attr('r').v = (0, 0, 0)
			pos.parent(self.Still_Grp)

			skinJnt = lrc.Joint()
			skinJntZrGrp = lrc.group(skinJnt)

			skinJnt.name = 'Skin%s_%s%sJnt' % (part, ix+1, side)
			skinJntZrGrp.name = 'SkinJntZr%s_%s%sGrp' % (part, ix+1, side)

			skinJntZrGrp.parent(self.Skin_Grp)
			
			mc.parentConstraint(pos, skinJntZrGrp)

			self.posis.append(posi)
			self.skinJnts.append(skinJnt)
			self.skinJntZrGrps.append(skinJntZrGrp)

			vVal = 0

			if ix == 0:
				vVal = 0.01

			elif ix == (jntNo-1):
				vVal = 0.99
			
			else:
				vVal = (1.00/float(jntNo-1))*(ix)
			
			posi.attr('parameterV').v = vVal
