import maya.cmds as mc

from lpRig import core as lrc
reload(lrc)
from lpRig import rigTools as lrr
reload(lrr)

class SplineIkRig(object):

	def __init__(
					self,
					ikJnts=[],
					ikUpJnts=[],
					ikCtrlJnts=[],
					ikCrv='',
					ikUpCrv='',
					ax='y',
					aimVec=(0, 1, 0),
					upVec=(1, 0, 0),
					part='',
					side=''
				):

		if side:
			side = '_%s_' % side
		else:
			side = '_'

		self.Ik_Crv = lrc.Dag(ikCrv)
		self.IkUp_Crv = lrc.Dag(ikUpCrv)
		self.IkJnts = ikJnts
		self.IkUpJnts = ikUpJnts
		self.IkPosJnts = []

		self.IkOrig_Crv = lrc.Dag(mc.duplicate(self.Ik_Crv, rr=True)[0])
		self.IkUpOrig_Crv = lrc.Dag(mc.duplicate(self.IkUp_Crv, rr=True)[0])
		self.IkOrig_Crv.name = '%sOrig%sCrv' % (part, side)
		self.IkUpOrig_Crv.name = '%sUpOrig%sCrv' % (part, side)

		# Pos joints
		for ix in range(len(self.IkJnts)):
			
			jnt = lrr.jointAt(self.IkJnts[ix])
			jnt.name = '%sPos_%s%sJnt' % (part, (ix+1), side)
			self.IkPosJnts.append(jnt)
			
			if ix > 0:
				self.IkPosJnts[ix].parent(self.IkPosJnts[ix - 1])

		# Main groups
		self.Ctrl_Grp = lrc.Null()
		self.Jnt_Grp = lrc.Null()
		self.Still_Grp = lrc.Null()
		self.Ikh_Grp = lrc.Null()
		self.WorOri_Grp = lrc.Null()

		self.Ctrl_Grp.name = '%sCtrl%sGrp' % (part, side)
		self.Jnt_Grp.name = '%sJnt%sGrp' % (part, side)
		self.Still_Grp.name = '%sStill%sGrp' % (part, side)
		self.Ikh_Grp.name = '%sIkh%sGrp' % (part, side)
		self.WorOri_Grp.name = '%sWorOri%sGrp' % (part, side)

		mc.parent(self.IkPosJnts[0], self.Jnt_Grp)
		mc.parent(self.IkUpJnts[0], self.Jnt_Grp)
		mc.parent(self.WorOri_Grp, self.Still_Grp)

		# IK Controllers
		self.Ctrls = []
		self.GmblCtrls = []
		self.CtrlZrs = []

		self.CtrlLocs = []
		self.CtrlWors = []

		for ix in range(len(ikCtrlJnts)):
			
			currCtrl = lrr.jointControl('cube')
			currCtrl.name = '%s_%s%sCtrl' % (part, (ix+1), side)
			currCtrl.color = 'blue'
			currCtrl.lockHideAttrs('sx', 'sy', 'sz', 'v')

			currGmbl = lrc.addGimbal(currCtrl)
			currGmbl.name = '%sGmbl_%s%sCtrl' % (part, (ix+1), side)

			currZr = lrr.groupAt(currCtrl)
			currZr.name = '%sCtrlZr_%s%sGrp' % (part, (ix+1), side)
			
			currZr.snap(ikCtrlJnts[ix])
			
			mc.parentConstraint(currCtrl, ikCtrlJnts[ix])
			mc.parent(ikCtrlJnts[ix], self.Jnt_Grp)
			
			self.Ctrls.append(currCtrl)
			self.GmblCtrls.append(currGmbl)
			self.CtrlZrs.append(currZr)
			
			if not ix == 0:
				mc.parent(currZr, self.GmblCtrls[0])
				(locGrp,
				worGrp,
				worParCon,
				zrParCon,
				locWorRev) = lrr.parLocWor(
												currCtrl, 
												self.GmblCtrls[0], 
												self.WorOri_Grp, 
												currZr
											)
				locGrp.name = '%sCtrlLocOri_%s%sGrp' % (part, (ix+1), side)
				worGrp.name = '%sCtrlWorOri_%s%sGrp' % (part, (ix+1), side)
				locWorRev.name = '%sCtrlLocWorOri_%s%sRev' % (part, (ix+1), side)
				
				self.CtrlLocs.append(locGrp)
				self.CtrlWors.append(worGrp)


		mc.parent(self.CtrlZrs[0], self.Ctrl_Grp)

		# IK handles
		self.Ikh = lrc.Ik(
								mc.ikHandle(
												sol='ikSplineSolver',
												createCurve=False,
												parentCurve=False,
												curve=self.Ik_Crv,
												startJoint=self.IkPosJnts[0],
												endEffector=self.IkPosJnts[-1]
											)[0]
							)
		self.UpIkh = lrc.Ik(
								mc.ikHandle(
												sol='ikSplineSolver',
												createCurve=False,
												parentCurve=False,
												curve=self.IkUp_Crv,
												startJoint=self.IkUpJnts[0],
												endEffector=self.IkUpJnts[-1]
											)[0]
							)
		
		self.Ikh.name = '%s%sIkh' % (part, side)
		self.UpIkh.name = '%sUp%sIkh' % (part, side)
		
		self.Ikh.parent(self.Ikh_Grp)
		self.UpIkh.parent(self.Ikh_Grp)
		
		self.Ik_Crv.parent(self.Still_Grp)
		self.IkUp_Crv.parent(self.Still_Grp)
		
		upCrvShp = lrc.Dag(self.IkUp_Crv)

		# Ori joints
		self.IkOriJnts = []

		for ix in range(len(self.IkPosJnts)):

			jnt = lrr.jointAt(self.IkPosJnts[ix])
			jnt.name = '%sOri_%s%sJnt' % (part, (ix+1), side)
			jnt.parent(self.IkPosJnts[ix])
			self.IkOriJnts.append(jnt)

		# Aim constraints
		for ix in range(len(self.IkOriJnts)):
			if ix <  (len(self.IkOriJnts) - 1):
				mc.aimConstraint(
									self.IkOriJnts[ix+1], 
									self.IkOriJnts[ix], 
									aim = aimVec, 
									u = upVec, 
									wut = 'object', 
									wuo = self.IkUpJnts[ix], 
									wu = upVec
								)

		# Detail controllers
		self.DtlCtrl_Grp = lrc.group(em=True)
		self.DtlCtrl_Grp.name = '%sDtlCtrl%sGrp' % (part, side)
		self.DtlCtrl_Grp.parent(self.Ctrl_Grp)
		
		self.DtlCtrls = []
		self.DtlCtrlOriGrps = []
		self.DtlCtrlZrGrps = []
		
		for ix in range(len(self.IkOriJnts)):
			
			ctrl = lrc.Control('circle')
			ctrl.name = '%sDtl_%s%sCtrl' % (part, (ix+1), side)
			ctrl.lockHideAttrs('v')
			ctrl.color = 'softBlue'

			ctrlOriGrp = lrc.group(ctrl)
			ctrlOriGrp.name = '%sDtlCtrlOri_%s%sGrp' % (part, (ix+1), side)

			ctrlZrGrp = lrc.group(ctrlOriGrp)
			ctrlZrGrp.name = '%sDtlCtrlZr_%s%sGrp' % (part, (ix+1), side)
			
			mc.parentConstraint(self.IkOriJnts[ix], ctrlZrGrp)
			ctrlOriGrp.snap(self.IkJnts[ix])
			mc.parentConstraint(ctrl, self.IkJnts[ix])
			mc.scaleConstraint(ctrl, self.IkJnts[ix])
			ctrlZrGrp.parent(self.DtlCtrl_Grp)
			
			self.DtlCtrls.append(ctrl)
			self.DtlCtrlOriGrps.append(ctrlOriGrp)
			self.DtlCtrlZrGrps.append(ctrlZrGrp)
		
		ikCtrlShp = lrc.Dag(self.Ctrls[0].shape)
		ikCtrlShp.add(ln='detailVis', k=True, min=0, max=1)
		ikCtrlShp.attr('detailVis') >> self.DtlCtrl_Grp.attr('v')
		
		# Auto-Stretch
		self.Ctrls[0].add(ln='autoStretch', min=0, max=1, k=True)
		self.IkOrig_Crv.parent(self.Ctrl_Grp)
		self.IkUpOrig_Crv.parent(self.Ctrl_Grp)
		self.IkOrig_Crv.attr('v').v = 0
		self.IkUpOrig_Crv.attr('v').v = 0
		
		self.CrvLen_Cif = lrc.Dag(mc.arclen(self.Ik_Crv, ch=True))
		self.CrvLen_Cif.name = '%sCrvLen%sCif' % (part, side)

		self.UpCrvLen_Cif = lrc.Dag(mc.arclen(self.IkUp_Crv, ch=True))
		self.UpCrvLen_Cif.name = '%sUpCrvLen%sCif' % (part, side)

		self.OrigCrvLen_Cif = lrc.Dag(mc.arclen(self.IkOrig_Crv, ch=True))
		self.OrigCrvLen_Cif.name = '%sOrigCrvLen%sCif' % (part, side)

		self.UpOrigCrvLen_Cif = lrc.Dag(mc.arclen(self.IkUpOrig_Crv, ch=True))
		self.UpOrigCrvLen_Cif.name = '%sUpOrigCrvLen%sCif' % (part, side)
				
		self.Strt_Mdv = lrc.MultiplyDivide()
		self.Strt_Mdv.name = '%sStrt%sMdv' % (part, side)
		
		self.UpStrt_Mdv = lrc.MultiplyDivide()
		self.UpStrt_Mdv.name = '%sUpStrt%sMdv' % (part, side)
		
		self.Strt_Mdv.attr('operation').v = 2
		self.UpStrt_Mdv.attr('operation').v = 2
		self.CrvLen_Cif.attr('arcLength') >> self.Strt_Mdv.attr('i1x')
		self.UpCrvLen_Cif.attr('arcLength') >> self.UpStrt_Mdv.attr('i1x')
		
		self.Strt_Bc = lrc.BlendColors()
		self.Strt_Bc.name = '%sStrt%sBc' % (part, side)

		self.UpStrt_Bc = lrc.BlendColors()
		self.UpStrt_Bc.name = '%sUpStrt%sBc' % (part, side)
		
		self.Ctrls[0].attr('autoStretch') >> self.Strt_Bc.attr('blender')
		self.Ctrls[0].attr('autoStretch') >> self.UpStrt_Bc.attr('blender')
		self.CrvLen_Cif.attr('arcLength') >> self.Strt_Bc.attr('color2R')
		self.UpCrvLen_Cif.attr('arcLength') >> self.UpStrt_Bc.attr('color2R')
		self.OrigCrvLen_Cif.attr('arcLength') >> self.Strt_Bc.attr('color1R')
		self.UpOrigCrvLen_Cif.attr('arcLength') >> self.UpStrt_Bc.attr('color1R')
		
		self.Strt_Bc.attr('outputR') >> self.Strt_Mdv.attr('i2x')
		self.UpStrt_Bc.attr('outputR') >> self.UpStrt_Mdv.attr('i2x')

		self.IkPosMdvs = []
		self.IkUpMdvs = []
		
		for ix in range(1, len(self.IkPosJnts)):
			
			posJnt = lrc.Dag(self.IkPosJnts[ix])
			upJnt = lrc.Dag(self.IkUpJnts[ix])
			
			posMdv = lrc.MultiplyDivide()
			posMdv.name = '%sPosStrt_%s%sMdv' % (part, (ix+1), side)
			
			upMdv = lrc.MultiplyDivide()
			upMdv.name = '%sUpPosStrt_%s%sMdv' % (part, (ix+1), side)
			
			origLen = posJnt.attr('t%s'%ax).v
			origUpLen = upJnt.attr('t%s'%ax).v
			
			posMdv.attr('i2x').v = origLen
			upMdv.attr('i2x').v = origUpLen
			
			self.Strt_Mdv.attr('ox') >> posMdv.attr('i1x')
			self.UpStrt_Mdv.attr('ox') >> upMdv.attr('i1x')
			
			posMdv.attr('ox') >> posJnt.attr('t%s'%ax)
			upMdv.attr('ox') >> upJnt.attr('t%s'%ax)
			
			self.IkPosMdvs.append(posMdv)
			self.IkUpMdvs.append(upMdv)
