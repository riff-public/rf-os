import re

import maya.cmds as mc
import maya.mel as mm

from lpRig import core as lrc
reload(lrc)
from lpRig import rigTools as lrr
reload(lrr)

def getAttrMax(attr='node.attr'):
	# Return max value of given attribute
	node = lrc.Dag(attr.split('.')[0])
	attr = attr.split('.')[1]
	
	tfAttrs = ('tx', 'ty', 'tz', 'sx', 'sy', 'sz', 'rx', 'ry ', 'rz')

	maxVal = 0
	
	if attr in tfAttrs:
		maxVal = node.attr('x%sl'%attr).v
	else:
		try:
			maxVal = mc.attributeQuery(attr, node=node, max=True)[0]
		except:
			maxVal = 10

	return maxVal

def twoWaysConnect(
						attrOut='ctrl.tx',
						attrIn=('bsh.outBsh', 'bsh.inBsh'),
						outAmper=1
					):
	# Connecting driver attribute to driven blend shapes with two cases
	# attrIn[1] for direct connection
	# attrIn[2] for inverse connection
	
	# Getting informations
	ctrl = lrc.Dag(attrOut.split('.')[0])
	ctrlAttr = attrOut.split('.')[1]
	bsh = lrc.Dag(attrIn[0].split('.')[0])
	bshAttrA = attrIn[0].split('.')[1]
	bshAttrB = attrIn[1].split('.')[1]
	
	clmp = lrc.Clamp()
	mul = lrc.MultDoubleLinear()
	
	clmp.attr('maxR').v = 100
	clmp.attr('minG').v = -100
	
	clmp.attr('outputG') >> mul.attr('i1')
	mul.attr('i2').v = -1
	mul.attr('o') >> bsh.attr(bshAttrB)
	clmp.attr('outputR') >> bsh.attr(bshAttrA)
	
	if not outAmper == 1:
		
		amp = lrr.attrAmper(
								ctrlAttr=ctrl.attr(ctrlAttr),
								targetAttr=clmp.attr('inputR'),
								dv=outAmper
							)
		amp.attr('o') >> clmp.attr('inputG')
		
		return clmp, mul, amp
	else:
		ctrl.attr(ctrlAttr) >> clmp.attr('inputR')
		ctrl.attr(ctrlAttr) >> clmp.attr('inputG')
		
		return clmp, mul

def ctrlToBsh(
				bshCtrl='',
				bshNode='',
				infoDict={}
			):
	
	# Connecting blend shape controller to blend shape name based on infoDict
	# Format of infoDict is:
	#{0: {'srcAttr': ('first_bsh', 'second_bsh')}}
	#{0: {'srcAttr': None}}
	#{0: {'srcAttr': 'bshNm_bsh'}}

	# Getting side from blend shape controller.
	sides = ('_L_', '_R_')
	currSide = '_'
	
	for side in sides:
		if side in bshCtrl:
			currSide = side
	
	# Get current name from given controller.
	ctrlName = bshCtrl.split(currSide)[0]
	
	for idx in sorted(infoDict.keys()):
		# Iterate to every key, control attribute, in infoDict.

		sourceAttr, targetAttr = infoDict[idx].popitem()
		
		attrOut = '%s.%s' % (bshCtrl, sourceAttr)
		if not mc.objExists(attrOut):
			# If source attribute does not exists, add the attribute.
			if targetAttr:
				mc.addAttr(bshCtrl, ln=sourceAttr, dv=0, k=True)
			else:
				# This is a seperator.
				mc.addAttr(bshCtrl, ln=sourceAttr, dv=0, k=True)
				mc.setAttr('%s.%s' % (bshCtrl, sourceAttr), l=True)

		outAmper = 1.00/getAttrMax(attrOut)

		if type(targetAttr) is type(()):
			# If there are two source attributes
			attrIn = (
						'%s.%s' % (bshNode, targetAttr[0]),
						'%s.%s' % (bshNode, targetAttr[1])
					)

			if outAmper == 1:
				# If amplifer is 1.
				clmp, mul = twoWaysConnect(
												attrOut=attrOut,
												attrIn=attrIn,
												outAmper=outAmper
											)
				
			else:
				clmp, mul, amp = twoWaysConnect(
													attrOut=attrOut,
													attrIn=attrIn,
													outAmper=outAmper
												)
				
				amp.name = '%s%sAmp%sMul' % (
												ctrlName,
												'%s%s'% (
															sourceAttr[0].capitalize(),
															sourceAttr[1:]
														),
												currSide
											)
			
			clmp.name = '%s%s%sClmp' % (
										ctrlName,
										'%s%s'% (
													sourceAttr[0].capitalize(),
													sourceAttr[1:]
												),
										currSide
										)
			mul.name = '%s%s%sMul' % (
										ctrlName,
										'%s%s'% (
													sourceAttr[0].capitalize(),
													sourceAttr[1:]
												),
										currSide
									)
		
		elif type(targetAttr) is type(''):
			# If there is only one source attribute
			attrIn = '%s.%s' % (bshNode, targetAttr)
			mul = lrr.attrAmper(
									ctrlAttr=attrOut,
									targetAttr=attrIn,
									dv=outAmper,
									ampAttr=''
								)
			mul.name = '%s%sAmp%sMul' % (
											ctrlName,
											'%s%s'% (
														sourceAttr[0].capitalize(),
														sourceAttr[1:]
													),
											currSide
										)

def connectInbetween(
						blendShapeObject='',
						blendShapeNode='',
						inbetweenBlendShape='',
						targetBlendShapeName='',
						weightValue=''
					):
	
	# Add inbetween blend shape.

	aliasAttrList = mc.aliasAttr(blendShapeNode, q=True)

	for each in aliasAttrList:

		if each == targetBlendShapeName:

			idx = aliasAttrList.index(each)
			wStr = str(aliasAttrList[idx+1])
			wIdx = re.findall(r'\d+', wStr)[0]

			cmd = 'blendShape -e  -ib -t %s %s %s %s %s;' % (
																blendShapeObject,
																wIdx,
																inbetweenBlendShape,
																weightValue,
																blendShapeNode
															)
			
			mm.eval(cmd)

def dupBlendShapeList(bshBuf='', bshLists=[[]]):

	bshs = []
	txtCrvs = []

	mc.select(bshBuf, r=True)
	bb = mc.exactWorldBoundingBox()

	yOffset = float(abs(bb[1] - bb[4]) * 1.2)
	xOffset = float(abs(bb[0] - bb[3]) * 1.4)

	xVal = 0

	for bshList in bshLists:

		xVal += xOffset
		yVal = 0

		for bsh in bshList:

			bshNm, txtCrv = dupBlendShapeObject(bshBuf, bsh)
			bshs.append(bshNm)
			txtCrvs.append(txtCrv)

			mc.move(xVal, yVal, 0, bshNm, r=True)

			yVal += yOffset

	return bshs, txtCrvs

def dupBlendShapeObject(obj='', bshName=''):

	# Duplicate a given obj, rename it to bshName and put text curves under it.
	# Return bshName and txtCrv
	
	mc.select(obj, r=True)
	bb = mc.exactWorldBoundingBox()

	if mc.objExists(bshName): return False

	duppedNode = mc.duplicate(obj, rr=True)[0]
	mc.rename(duppedNode, bshName)

	chdn = mc.listRelatives(bshName, ad=True, type='transform', f=True)
	if chdn:
		for chd in chdn:
			lastName = chd.split('|')[-1]
			mc.rename(chd, '%s_%s' % (lastName, bshName))

	txtCrv = mc.textCurves(f='Courier', t=bshName)[0]
	mc.select(txtCrv, r=True)
	txtBb = mc.exactWorldBoundingBox()
	mc.xform(txtCrv, cp=True)

	# vol = abs(bb[0] - bb[3]) * abs(bb[1] - bb[4]) * abs(bb[2] - bb[5])
	objWidth = abs(bb[0] - bb[3])
	txtWidth = abs(txtBb[0] - txtBb[3])

	tmpLoc = mc.spaceLocator()
	sclVal = objWidth/txtWidth
	mc.scale(sclVal, sclVal, sclVal, tmpLoc, r=True)
	mc.move(0, bb[1], bb[5], tmpLoc, r=True)

	mc.delete(mc.parentConstraint(tmpLoc, txtCrv))	
	mc.delete(mc.scaleConstraint(tmpLoc, txtCrv))	
	mc.parentConstraint(bshName, txtCrv, mo=True)

	mc.setAttr('%s.overrideEnabled' % txtCrv, 1)
	mc.setAttr('%s.overrideDisplayType' % txtCrv, 2)
	
	mc.delete(tmpLoc)
	return bshName, txtCrv
