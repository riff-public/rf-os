import maya.cmds as mc
import maya.mel as mm

from lpRig import bshTools as bshTools
reload(bshTools)
from lpRig import core as lrc
reload(lrc)
from lpRig import rigTools as lrr
reload(lrr)

def doApplyVertexMove(geos=['body', 'eb']):

	for side in ('L', 'R'):

		sideJnt = 'eb_%s_jnt' % side
		inJnt = 'ebIn_%s_jnt' % side
		midJnt = 'ebMid_%s_jnt' % side
		outJnt = 'ebOut_%s_jnt' % side

		sideGeos = ['%sSide' % x for x in geos]
		eachGeos = ['%sEach' % x for x in geos]
		upGeos = ['%sUp' % x for x in geos]
		dnGeos = ['%sDn' % x for x in geos]
		inGeos = ['%sIn' % x for x in geos]
		outGeos = ['%sOut' % x for x in geos]
		pushGeos = ['%sPush' % x for x in geos]
		pullGeos = ['%sPull' % x for x in geos]

		applyVertexMove(sideJnt, inJnt, midJnt, outJnt, sideGeos, eachGeos, upGeos, 
							dnGeos, inGeos, outGeos, pushGeos, pullGeos, side)

def applyVertexMove(
						sideJnt='',
						inJnt='',
						midJnt='',
						outJnt='',
						sideGeos=[],
						eachGeos=[],
						upGeos=[],
						dnGeos=[],
						inGeos=[],
						outGeos=[],
						pushGeos=[],
						pullGeos=[],
						side='',
					):
	tars = ('Up', 'Dn', 'In','Out', 'Push', 'Pull')
	srcs = (upGeos, dnGeos, inGeos, outGeos, pushGeos, pullGeos)

	for iw in xrange(len(tars)):

		currTarGrp = '{tar}EbRig_{side}_Bsh'.format(tar=tars[iw], side=side)

		if not mc.objExists(currTarGrp):
			print '{obj} is missing.'.format(obj=currTarGrp)
			continue

		currTarGeos = mc.listRelatives(currTarGrp, type='transform', ad=True)[:2]

		for ix in xrange(len(currTarGeos)):

			skinGeo = sideGeos[ix]
			srcGeo = srcs[iw][ix]
			tarGeo = currTarGeos[ix]

			lrr.applyVertexMoveWithSkinWeight(
													baseObj=skinGeo,
													srcObj=srcGeo,
													tarObj=tarGeo,
													skinGeo=skinGeo,
													jnt=sideJnt
												)

		if tars[iw] in ('Push', 'Pull'):
			continue

		jnts = (inJnt, midJnt, outJnt)
		parts = ('In', 'Mid', 'Out')

		for iy in xrange(len(parts)):

			currEachTarGrp = '{tar}{part}EbRig_{side}_Bsh'.format(tar=tars[iw], 
								part=parts[iy], side=side)

			if not mc.objExists(currTarGrp):
				print '{obj} is missing.'.format(obj=currEachTarGrp)
				continue

			currEachTarGeos = mc.listRelatives(currEachTarGrp, type='transform', ad=True)[:2]
			currJnt = jnts[iy]

			for iz in xrange(len(currEachTarGeos)):
				
				skinEachGeo = eachGeos[iz]
				srcEachGeo = srcs[iw][iz]
				tarEachGeo = currEachTarGeos[iz]

				lrr.applyVertexMoveWithSkinWeight(
														baseObj=skinEachGeo,
														srcObj=srcEachGeo,
														tarObj=tarEachGeo,
														skinGeo=skinEachGeo,
														jnt=currJnt
													)

def createBsh(bshBuf='', part='', inbetween=False, lite=False):

	ctrl = ''
	ctrlNm = '%sCtrl_Grp' % part

	for each in mc.ls():
		if each.endswith(ctrlNm):
			ctrl = each
			break

	ctrlNs = ctrl.replace(ctrlNm, '')
	
	bshGrp = lrc.Null()
	bshGrp.name = '%sBsh_Grp' % part
	
	# Shapes
	shps = [
				'Up', # 0
				'UpIn', # 1
				'UpMid', # 2
				'UpOut', # 3
				'Dn', # 4
				'DnIn', # 5
				'DnMid', # 6
				'DnOut', # 7
				'In', # 8
				'InIn', # 9
				'InMid', # 10
				'InOut', # 11
				'Out', # 12
				'OutIn', # 13
				'OutMid', # 14
				'OutOut', # 15
				'RotDn', # 16
				'RotDnIn', # 17
				'RotDnMid', # 18
				'RotDnOut', # 19
				'RotUp', # 20
				'RotUpIn', # 21
				'RotUpMid', # 22
				'RotUpOut', # 23
				'Push', # 24
				'Pull' # 25
			]
	if lite:
		shps = shps[:16]

	inbShps = [
				'UpInb', # 0
				'UpInInb', # 1
				'UpMidInb', # 2
				'UpOutInb', # 3
				'DnInb', # 4
				'DnInInb', # 5
				'DnMidInb', # 6
				'DnOutInb', # 7
				]

	# Populate blend shape names from shape names.
	lftShps = ['%s%s_L_Bsh' % (shp, part) for shp in shps]
	rgtShps = ['%s%s_R_Bsh' % (shp, part) for shp in shps]
	lftInbShps = ['%s%s_L_Bsh' % (shp, part) for shp in inbShps]
	rgtInbShps = ['%s%s_R_Bsh' % (shp, part) for shp in inbShps]
	
	# Duplicate blend shape objects.
	bshList = [lftShps, rgtShps]
	if inbetween:
		bshList.append(lftInbShps)
		bshList.append(rgtInbShps)
	
	dupBshs, txtCrvs = bshTools.dupBlendShapeList(bshBuf, bshList)
	mc.parent(dupBshs, txtCrvs, bshGrp)
	
	# Create blend shape.
	bsn = lrc.Node('%sBuf_Bsn' % part)
	mc.blendShape(lftShps+rgtShps, bshBuf, n=bsn)

	if inbetween:
		for ix in xrange(8):
			bshTools.connectInbetween(bshBuf, bsn, lftInbShps[ix], lftShps[ix], 0.5)
			bshTools.connectInbetween(bshBuf, bsn, rgtInbShps[ix], rgtShps[ix], 0.5)

	for ix in range(2):

		# Iterate through each side and each blend shape name list.
		side = ('_L_', '_R_')[ix]
		bshNms = (lftShps, rgtShps)[ix]

		# All
		ctrl = lrc.Dag('%sAll%s%sCtrl' % (ctrlNs, part, side))

		infoDict = {
						0: {'ty': (bshNms[0], bshNms[4])},
						1: {'tx': (bshNms[12], bshNms[8])}
					}

		if not lite:
			infoDict[2] = {'tz': (bshNms[25], bshNms[24])}
			infoDict[3] = {'rz': (bshNms[16], bshNms[20])}
		
		bshTools.ctrlToBsh(ctrl.name, bsn, infoDict)

		# In
		ctrl = lrc.Dag('%sIn%s%sCtrl' % (ctrlNs, part, side))

		infoDict = {
						0: {'ty': (bshNms[1], bshNms[5])},
						1: {'tx': (bshNms[13], bshNms[9])}
					}

		if not lite:
			infoDict[2] = {'rz': (bshNms[17], bshNms[21])}

		bshTools.ctrlToBsh(ctrl.name, bsn, infoDict)

		# Mid
		ctrl = lrc.Dag('%sMid%s%sCtrl' % (ctrlNs, part, side))

		infoDict = {
						0: {'ty': (bshNms[2], bshNms[6])},
						1: {'tx': (bshNms[14], bshNms[10])}
					}

		if not lite:
			infoDict[2] = {'rz': (bshNms[18], bshNms[22])}

		bshTools.ctrlToBsh(ctrl.name, bsn, infoDict)

		# Out
		ctrl = lrc.Dag('%sOut%s%sCtrl' % (ctrlNs, part, side))

		infoDict = {
						0: {'ty': (bshNms[3], bshNms[7])},
						1: {'tx': (bshNms[15], bshNms[11])}
					}
		
		if not lite:
			infoDict[2] = {'rz': (bshNms[19], bshNms[23])}
		
		bshTools.ctrlToBsh(ctrl.name, bsn, infoDict)

class EyebrowRig(object):

	def __init__(
					self,
					allTmpLoc='',
					inTmpLoc='',
					midTmpLoc='',
					outTmpLoc='',
					part='',
					side='',
					lite=False
				):

		if side:
			side = '_%s_' % side
		else:
			side = '_'

		# Define color
		col = 'red'
		darkCol = 'darkRed'
		softCol = 'softRed'
		if side == '_R_':
			col = 'blue'
			darkCol = 'darkBlue'
			softCol = 'softBlue'

		# Main Group
		self.Ctrl_Grp = lrc.Null()
		self.Ctrl_Grp.name = '%sCtrl%sGrp' % (part, side)

		# Tmp Locators
		allLoc = lrc.Dag(allTmpLoc)
		inLoc = lrc.Dag(inTmpLoc)
		midLoc = lrc.Dag(midTmpLoc)
		outLoc = lrc.Dag(outTmpLoc)

		# Main Controller
		self.All_Ctrl, self.AllCtrlInv_Grp, self.AllCtrlZr_Grp = self.createMainControl(allLoc, 'All', part, side, ['rx', 'ry', 's'])
		self.All_Ctrl.color = col
		self.AllCtrlZr_Grp.parent(self.Ctrl_Grp)
		
		# Part Controllers
		self.In_Ctrl, self.InCtrlInv_Grp, self.InCtrlZr_Grp = self.createMainControl(inLoc, 'In', part, side, ['tz', 'rx', 'ry', 's'])
		self.In_Ctrl.color = darkCol
		self.InCtrlZr_Grp.parent(self.Ctrl_Grp)

		self.Mid_Ctrl, self.MidCtrlInv_Grp, self.MidCtrlZr_Grp = self.createMainControl(midLoc, 'Mid', part, side, ['tz', 'rx', 'ry', 's'])
		self.Mid_Ctrl.color = darkCol
		self.MidCtrlZr_Grp.parent(self.Ctrl_Grp)

		self.Out_Ctrl, self.OutCtrlInv_Grp, self.OutCtrlZr_Grp = self.createMainControl(outLoc, 'Out', part, side, ['tz', 'rx', 'ry', 's'])
		self.Out_Ctrl.color = darkCol
		self.OutCtrlZr_Grp.parent(self.Ctrl_Grp)

		if side == '_R_':
			for each in (self.AllCtrlZr_Grp, self.InCtrlZr_Grp, self.MidCtrlZr_Grp, self.OutCtrlZr_Grp):
				each.attr('ry').v = 180

		if lite:
			self.All_Ctrl.lockHideAttrs('rz')
			self.All_Ctrl.lockHideAttrs('tz')
			self.In_Ctrl.lockHideAttrs('rz')
			self.Mid_Ctrl.lockHideAttrs('rz')
			self.Out_Ctrl.lockHideAttrs('rz')

	def createMainControl(self, loc, ctrlPart, part, side, lockAttrs):

		ctrl = lrc.Control('nrbCircle')
		ctrl.scaleShape(0.25)
		invGrp = lrc.group(ctrl)
		zrGrp = lrc.group(invGrp)

		zrGrp.snap(loc)

		ctrl.name = '%s%s%sCtrl' % (ctrlPart, part, side)
		invGrp.name = '%s%sCtrlInv%sGrp' % (ctrlPart, part, side)
		zrGrp.name = '%s%sCtrlZr%sGrp' % (ctrlPart, part, side)

		for attr in lockAttrs:
			ctrl.lockHideAttrs(attr)

		ctrl.attr('v').l = True
		ctrl.attr('v').h = True

		return ctrl, invGrp, zrGrp

class EbDetailRig(object):

	def __init__(
					self,
					lowerCurve='',
					upperCurve='',
					tmpLftLocs=[],
					tmpRgtLocs=[],
					offsetCurve=0.1,
					jntNo=13,
					part=''
				):

		# Main Groups
		self.Ctrl_Grp = lrc.Null()
		self.Ctrl_Grp.name = 'EbRbn%sCtrl_Grp' % part

		self.Jnt_Grp = lrc.Null()
		self.Jnt_Grp.name = 'EbRbn%sJnt_Grp' % part

		self.Skin_Grp = lrc.Null()
		self.Skin_Grp.name = 'EbRbn%sSkin_Grp' % part

		self.Still_Grp = lrc.Null()
		self.Still_Grp.name = 'EbRbn%sStill_Grp' % part

		# Ribbon Surface
		lowCrv = lrc.Dag(mc.rebuildCurve(lowerCurve, ch=False, s=8)[0])
		upCrv = lrc.Dag(mc.rebuildCurve(upperCurve, ch=False, s=8)[0])

		self.Rbn_Nrb = lrc.Dag(mc.loft(lowCrv, upCrv, ch=False, d=3)[0])
		self.Rbn_Nrb.name = 'EbRbn%s_Nrb' % part
		self.Rbn_Nrb.parent(self.Still_Grp)

		# Controllers and control joints
		tmpLocs = (tmpLftLocs, tmpRgtLocs)

		self.LftCtrls = []
		self.RgtCtrls = []
		ctrls = (self.LftCtrls, self.RgtCtrls)

		self.LftCtrlInvGrps = []
		self.RgtCtrlInvGrps = []
		ctrlInvGrps = (self.LftCtrlInvGrps, self.RgtCtrlInvGrps)

		self.LftCtrlZrGrps = []
		self.RgtCtrlZrGrps = []
		ctrlZrGrps = (self.LftCtrlZrGrps, self.RgtCtrlZrGrps)

		self.LftJnts = []
		self.RgtJnts = []
		jnts = (self.LftJnts, self.RgtJnts)

		self.LftJntZrGrps = []
		self.RgtJntZrGrps = []
		jntZrGrps = (self.LftJntZrGrps, self.RgtJntZrGrps)

		sides = ('_L_', '_R_')
		colors = ('darkRed', 'darkBlue')

		for ix in xrange(2):
			
			currCol = colors[ix]
			currSide = sides[ix]
			currSideLocs = tmpLocs[ix]

			currSideCtrls = ctrls[ix]
			currSideCtrlInvGrps = ctrlInvGrps[ix]
			currSideCtrlZrGrps = ctrlZrGrps[ix]

			currSideJnts = jnts[ix]
			currSideJntZrGrps = jntZrGrps[ix]

			if not currSideLocs:
				continue

			for iy in xrange(len(currSideLocs)):

				currLoc = currSideLocs[iy]
				currIdx = iy+1

				ctrl = lrc.Control('cube')
				ctrl.name = 'EbRbn%s_%s%sCtrl' % (part, currIdx, currSide)
				ctrl.lockHideAttrs('sx', 'sy', 'sz', 'v')
				ctrl.color = currCol
				currSideCtrls.append(ctrl)

				ctrlInvGrp = lrc.group(ctrl)
				ctrlInvGrp.name = 'EbRbn%sCtrlInv_%s%sGrp' % (part, currIdx, currSide)
				currSideCtrlInvGrps.append(ctrlInvGrp)

				ctrlZrGrp = lrc.group(ctrlInvGrp)
				ctrlZrGrp.name = 'EbRbn%sCtrlZr_%s%sGrp' % (part, currIdx, currSide)
				ctrlZrGrp.snap(currLoc)
				ctrlZrGrp.parent(self.Ctrl_Grp)
				currSideCtrlZrGrps.append(ctrlZrGrp)

				jnt = lrc.Joint()
				jnt.name = 'EbRbn%s_%s%sJnt' % (part, currIdx, currSide)
				currSideJnts.append(jnt)

				jntZrGrp = lrc.group(jnt)
				jntZrGrp.name = 'EbRbn%sJntZr_%s%sGrp' % (part, currIdx, currSide)
				jntZrGrp.snap(currLoc)
				jntZrGrp.parent(self.Jnt_Grp)
				currSideJntZrGrps.append(jntZrGrp)

				ctrl.attr('t') >> jnt.attr('t')
				ctrl.attr('r') >> jnt.attr('r')

		skc = mc.skinCluster(
								self.LftJnts + self.RgtJnts,
								self.Rbn_Nrb,
								dr=7,
								mi=2
							)[0]
		# Skin jnts
		self.SkinJnts = []
		self.SkinJntZrGrps = []
		self.Poss = []
		self.Posis = []

		for ix in range(jntNo):

			idx = ix+1

			pos = lrc.Null()
			posi = lrc.PointOnSurfaceInfo()

			pos.name = 'EbRbnPos%s_%s_Grp' % (part, idx)
			posi.name = 'EbRbn%s_%s_Posi' % (part, idx)
			
			self.Rbn_Nrb.attr('worldSpace[0]') >> posi.attr('is')
			posi.attr('position') >> pos.attr('t')
			posi.attr('parameterU').v = 0.5
			posi.attr('turnOnPercentage').v = True
			
			pos.parent(self.Still_Grp)

			skinJnt = lrc.Joint()
			skinJntOffGrp = lrc.group(skinJnt)
			skinJntZrGrp = lrc.group(skinJntOffGrp)

			skinJnt.name = 'EbRbnSkin%s_%s_Jnt' % (part, idx)
			skinJntOffGrp.name = 'EbRbnSkinJntOff%s_%s_Grp' % (part, idx)
			skinJntZrGrp.name = 'EbRbnSkinJntZr%s_%s_Grp' % (part, idx)

			skinJntZrGrp.parent(self.Skin_Grp)
			
			mc.pointConstraint(pos, skinJntZrGrp)

			aimCon = lrc.AimConstraint()
			aimCon.name = '%s_aimConstraint' % pos
			aimCon.attr('a').v = (0, 1, 0)
			aimCon.attr('u').v = (1, 0, 0)

			aimCon.parent(pos)
			aimCon.attr('t').v = (0, 0, 0)
			aimCon.attr('r').v = (0, 0, 0)
			
			posi.attr('n') >> aimCon.attr('wu')
			posi.attr('tv') >> aimCon.attr('tg[0].tt')
			aimCon.attr('cr') >> pos.attr('r')
			
			mc.orientConstraint(pos, skinJntZrGrp)

			self.Posis.append(posi)
			self.SkinJnts.append(skinJnt)
			self.SkinJntZrGrps.append(skinJntZrGrp)

			par = 0
			if ix == 0:
				par = 0.001
			elif ix == jntNo-1:
				par = 0.999
			else:
				par = (1.0000/(jntNo-1.0000))*ix
			posi.attr('parameterV').v = par

def main(detail=True, lite=False):

	ctrlGrp = lrc.Null()
	ctrlGrp.name = 'EbRigCtrl_Grp'

	stillGrp = lrc.Null()
	stillGrp.name = 'EbRigStill_Grp'

	ebLftRigObj = EyebrowRig(
								allTmpLoc='AllEbRigTmpPos_L_Loc',
								inTmpLoc='InEbRigTmpPos_L_Loc',
								midTmpLoc='MidEbRigTmpPos_L_Loc',
								outTmpLoc='OutEbRigTmpPos_L_Loc',
								part='EbRig',
								side='L',
								lite=lite
							)
	ebLftRigObj.Ctrl_Grp.parent(ctrlGrp)

	ebRgtRigObj = EyebrowRig(
								allTmpLoc='AllEbRigTmpPos_R_Loc',
								inTmpLoc='InEbRigTmpPos_R_Loc',
								midTmpLoc='MidEbRigTmpPos_R_Loc',
								outTmpLoc='OutEbRigTmpPos_R_Loc',
								part='EbRig',
								side='R',
								lite=lite
							)
	ebRgtRigObj.Ctrl_Grp.parent(ctrlGrp)

	if not detail:
		return True

	ebRbnRigObj = EbDetailRig(
								lowerCurve='EbRbnLowTmp_Crv',
								upperCurve='EbRbnUpTmp_Crv',
								tmpLftLocs=[# Order them from inner to outer
												'EbRbnTmp_1_L_Loc',
												'EbRbnTmp_2_L_Loc',
												'EbRbnTmp_3_L_Loc',
												'EbRbnTmp_4_L_Loc'
											],
								tmpRgtLocs=[# Order them from inner to outer
												'EbRbnTmp_1_R_Loc',
												'EbRbnTmp_2_R_Loc',
												'EbRbnTmp_3_R_Loc',
												'EbRbnTmp_4_R_Loc'
											],
								offsetCurve=0.1,
								jntNo=13,
								part=''
							)
	ebRbnRigObj.Ctrl_Grp.parent(ctrlGrp)
	ebRbnRigObj.Jnt_Grp.parent(stillGrp)
	ebRbnRigObj.Skin_Grp.parent(stillGrp)
	ebRbnRigObj.Still_Grp.parent(stillGrp)

	return True