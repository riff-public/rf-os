import maya.cmds as mc
import maya.mel as mm
import maya.cmds as mc
import maya.OpenMaya as om
import maya.OpenMayaAnim as oma


# import maya.OpenMaya as api

# sel = api.MSelectionList()
# api.MGlobal.getActiveSelectionList(sel)
# compIter = api.MItSelectionList(sel, api.MFn.kMeshVertComponent)

# component = api.MObject()
# meshDagPath = api.MDagPath()

# vList = []
# while not compIter.isDone():
# 	compIter.getDagPath(meshDagPath, component)
# 	if not component.isNull():
# 		vertIter = api.MItMeshVertex(meshDagPath, component)
# 		while not vertIter.isDone():
# 			vList.append(vertIter.index())
# 			vertIter.next()
# 	compIter.next()

#--------------------------------------------

# Let's say you have a python function and you pass in the name of a mesh you want to create an MFnMesh function set with. Do you have to use a selection list like this?

# import maya.OpenMaya as om

# # select the mesh - really this would be some name or even full path passed in
# om.MGlobal.selectByName( "pCube1" )

# # grab the active selection list which will now have "pCube1"
# sel = om.MSelectionList()
# om.MGlobal.getActiveSelectionList( sel )

# # return the MDagPath of only element 0 in the active selection list
# dagPathToMesh = om.MDagPath()
# sel.getDagPath( 0, dagPathToMesh )

# # make sure the path is to the shape node (not the transform)
# dagPathToMesh.extendToShape()

# # finally create the mesh function set
# mFnMesh = om.MFnMesh( dagPathToMesh )
# It seems horribly wordy and I feel like using the global selection stuff is really roundabout.


# Is there some way to just directly create an MDagPath object if you've already passed in the path to the shape in a function? Hypothetically, something like this would be nice:
# import maya.OpenMaya as om

# pathToMesh = om.MDagPath()
# pathToMesh.setDagPath( "|group|transform|shape" )

# mFnMesh = om.MFnMesh( pathToMesh )

# Thanks.
# -shawn

#--------------------------------------------

# You don't have to actually select the object to retrieve its dag path. You should just add it to the MSelectionList, then get what you need:


# import maya.OpenMaya as om

# sel = om.MSelectionList()
# sel.add("pCube1")

# # return the MDagPath of only element 0 in the active selection list
# dagPathToMesh = om.MDagPath()
# sel.getDagPath( 0, dagPathToMesh )

# # make sure the path is to the shape node (not the transform)
# dagPathToMesh.extendToShape()

# # finally create the mesh function set
# mFnMesh = om.MFnMesh( dagPathToMesh )



# It's still not as simple as you'd like, but that's just part of dealing with the api.


# Stev

# <class 'maya.OpenMaya.MItSelectionList'>
# 'className', 'getDagPath', 'getDependNode', 'getPlug', 'getStrings', 'hasComponents', 
# 'isDone', 'itemType', 'kAnimSelectionItem', 'kDNselectionItem', 'kDagSelectionItem', 
# 'kPlugSelectionItem', 'kUnknownItem', 'next', 'reset', 'setFilter', 'this'

def main():
	
	sel_list = om.MSelectionList()
	om.MGlobal.getActiveSelectionList(sel_list)
	
	sel_iter = om.MItSelectionList(sel_list)
	while not sel_iter.isDone():
		curr_dag_path = om.MDagPath()
		sel_iter.getDagPath(curr_dag_path)
		
		print curr_dag_path.fullPathName()
		print sel_iter.hasComponents()
		
		sel_iter.next()