# Maya modules
import maya.cmds as mc
import maya.mel as mm

# Custom modules
import lpRig.core as lrc
import lpRig.rigTools as lrr
reload(lrc)
reload(lrr)

class TorsoRig(object):

	def __init__(
					self,
					parent='',
					ctrlGrp='',
					jntGrp='',
					skinGrp='',
					stillGrp='',
					tmpJnt=(
								'Pelvis_Jnt',
								'Spine_Jnt',
								'Chest_Jnt',
								'ChestTip_Jnt',
								'SpineMid_Jnt'
							),
					part=''
				):
		
		self.Pelvis_Jnt = lrc.Dag(tmpJnt[0])
		self.Spine_Jnt = lrc.Dag(tmpJnt[1])
		self.Chest_Jnt = lrc.Dag(tmpJnt[2])
		self.ChestSca_Jnt = lrr.jointAt(self.Chest_Jnt)
		self.ChestSca_Jnt.parent(self.Chest_Jnt)
		self.ChestSca_Jnt.name = 'ChestSca%s_Jnt' % part
		self.ChestTip_Jnt = lrc.Dag(tmpJnt[3])
		self.SpineMid_Jnt = lrc.Dag(tmpJnt[4])
		
		self.Pelvis_Jnt.rotateOrder = 'yzx'
		self.Spine_Jnt.rotateOrder = 'yzx'
		self.Chest_Jnt.rotateOrder = 'yzx'
		self.ChestTip_Jnt.rotateOrder = 'yzx'
		
		self.Pelvis_Jnt.attr('ssc').v = 0
		self.Chest_Jnt.attr('ssc').v = 0
		
		# Main group
		self.Ctrl_Grp = lrc.Null()
		self.Ctrl_Grp.name = 'SpineCtrl%s_Grp' % part
		mc.parentConstraint(parent, self.Ctrl_Grp)
		self.Ctrl_Grp.parent(ctrlGrp)

		self.Jnt_Grp = lrc.Null()
		self.Jnt_Grp.name = 'SpineJnt%s_Grp' % part
		self.Jnt_Grp.parent(jntGrp)
		
		self.Skin_Grp = lrc.Null()
		self.Skin_Grp.name = 'SpineSkin%s_Grp' % part
		self.Skin_Grp.parent(skinGrp)
		
		self.Still_Grp = lrc.Null()
		self.Still_Grp.name = 'SpineStill%s_Grp' % part
		self.Still_Grp.parent(stillGrp)

		# Ribbon joints
		self.PelvisRbn_Jnt = lrc.Joint()
		self.PelvisRbn_Jnt.rotateOrder = 'yzx'
		self.PelvisRbn_Jnt.name = 'PelvisRbn%s_Jnt' % part

		self.PelvisRbnJntZr_Grp = lrc.group(self.PelvisRbn_Jnt)
		self.PelvisRbnJntZr_Grp.name = 'PelvisRbnJntZr%s_Grp' % part
		
		self.ChestRbn_Jnt = lrc.Joint()
		self.ChestRbn_Jnt.rotateOrder = 'yzx'
		self.ChestRbn_Jnt.name = 'ChestRbn%s_Jnt' % part

		self.ChestRbnJntZr_Grp = lrc.group(self.ChestRbn_Jnt)
		self.ChestRbnJntZr_Grp.name = 'ChestRbnJntZr%s_Grp' % part

		self.PelvisRbnJntZr_Grp.snap(self.Pelvis_Jnt)
		self.ChestRbnJntZr_Grp.snap(self.Chest_Jnt)
		
		self.PelvisRbnJntZr_Grp.parent(self.Jnt_Grp)
		self.ChestRbnJntZr_Grp.parent(self.Jnt_Grp)

		# Controller
		# Controller - Spine
		self.Spine_Ctrl = lrr.jointControl('square')
		self.Spine_Ctrl.name = 'Spine%s_Ctrl' % part
		self.Spine_Ctrl.color = 'softYellow'
		self.Spine_Ctrl.lockHideAttrs('v')

		self.SpineGmbl_Ctrl = lrc.addGimbal(self.Spine_Ctrl)
		self.SpineGmbl_Ctrl.name = 'SpineGmbl%s_Ctrl' % part
		self.SpineGmbl_Ctrl.scaleShape(0.8)

		self.SpineCtrlOfst_Grp = lrc.group(self.Spine_Ctrl)
		self.SpineCtrlOfst_Grp.name = 'SpineCtrlOfst%s_Ctrl' % part
		self.SpineCtrlZr_Grp = lrc.group(self.SpineCtrlOfst_Grp)
		self.SpineCtrlZr_Grp.name = 'SpineCtrlZr%s_Grp' % part

		self.SpineCtrlZr_Grp.snap(self.Spine_Jnt)
		self.SpineCtrlZr_Grp.parent(self.Ctrl_Grp)

		# Controller - SpineMid
		self.SpineMid_Ctrl = lrr.jointControl('square')
		self.SpineMid_Ctrl.name = 'SpineMid%s_Ctrl' % part
		self.SpineMid_Ctrl.color = 'softYellow'
		self.SpineMid_Ctrl.lockHideAttrs('v')

		self.SpineMidGmbl_Ctrl = lrc.addGimbal(self.SpineMid_Ctrl)
		self.SpineMidGmbl_Ctrl.name = 'SpineMidGmbl%s_Ctrl' % part
		self.SpineMidGmbl_Ctrl.scaleShape(0.8)

		self.SpineMidCtrlOfst_Grp = lrc.group(self.SpineMid_Ctrl)
		self.SpineMidCtrlOfst_Grp.name = 'SpineMidCtrlOfst%s_Ctrl' % part
		self.SpineMidCtrlZr_Grp = lrc.group(self.SpineMidCtrlOfst_Grp)
		self.SpineMidCtrlZr_Grp.name = 'SpineMidCtrlZr%s_Grp' % part

		self.SpineMidCtrlZr_Grp.snap(self.SpineMid_Jnt)
		self.SpineMidCtrlZr_Grp.parent(self.SpineGmbl_Ctrl)

		# Controller - Pelvis
		self.Pelvis_Ctrl = lrr.jointControl('cube')
		self.Pelvis_Ctrl.name = 'Pelvis%s_Ctrl' % part
		self.Pelvis_Ctrl.color = 'yellow'
		self.Pelvis_Ctrl.lockHideAttrs('v')

		self.PelvisGmbl_Ctrl = lrc.addGimbal(self.Pelvis_Ctrl)
		self.PelvisGmbl_Ctrl.name = 'PelvisGmbl%s_Ctrl' % part
		self.PelvisGmbl_Ctrl.scaleShape(0.8)

		self.PelvisCtrlOfst_Grp = lrc.group(self.Pelvis_Ctrl)
		self.PelvisCtrlOfst_Grp.name = 'PelvisCtrlOfst%s_Grp' % part
		self.PelvisCtrlZr_Grp = lrc.group(self.PelvisCtrlOfst_Grp)
		self.PelvisCtrlZr_Grp.name = 'PelvisCtrlZr%s_Grp' % part

		self.PelvisCtrlZr_Grp.snap(self.Pelvis_Jnt)
		self.PelvisCtrlZr_Grp.parent(self.Ctrl_Grp)

		# Controller - Chest
		self.Chest_Ctrl = lrr.jointControl('cube')
		self.Chest_Ctrl.name = 'Chest%s_Ctrl' % part
		self.Chest_Ctrl.color = 'yellow'
		self.Chest_Ctrl.lockHideAttrs('v')

		self.ChestGmbl_Ctrl = lrc.addGimbal(self.Chest_Ctrl)
		self.ChestGmbl_Ctrl.name = 'ChestGmbl%s_Ctrl' % part
		self.ChestGmbl_Ctrl.scaleShape(0.8)

		self.ChestCtrlOfst_Grp = lrc.group(self.Chest_Ctrl)
		self.ChestCtrlOfst_Grp.name = 'ChestCtrlOfst%s_Ctrl' % part
		self.ChestCtrlZr_Grp = lrc.group(self.ChestCtrlOfst_Grp)
		self.ChestCtrlZr_Grp.name = 'ChestCtrlZr%s_Grp' % part

		self.ChestCtrlZr_Grp.snap(self.Chest_Jnt)
		self.ChestCtrlZr_Grp.parent(self.SpineMidGmbl_Ctrl)

		chestCtrlShp = lrc.Dag(self.Chest_Ctrl.shape)

		# Controller - Connect to joint
		# mc.parentConstraint(self.SpineGmbl_Ctrl, self.SpineRbnJntZr_Grp)
		mc.parentConstraint(self.ChestGmbl_Ctrl, self.ChestRbnJntZr_Grp)

		mc.parentConstraint(self.PelvisGmbl_Ctrl, self.Pelvis_Jnt)
		mc.scaleConstraint(self.PelvisGmbl_Ctrl, self.Pelvis_Jnt)
		mc.parentConstraint(self.SpineGmbl_Ctrl, self.Spine_Jnt)
		mc.parentConstraint(self.ChestGmbl_Ctrl, self.Chest_Jnt)
		mc.scaleConstraint(self.ChestGmbl_Ctrl, self.Chest_Jnt)

		# Controller - Connect to joint - Independent hips
		self.Pelvis_Ctrl.add(ln='indepHip', k=True, min=0, max=1)

		tmpCons = lrc.parentConstraint(self.PelvisGmbl_Ctrl, parent, self.PelvisRbnJntZr_Grp, mo=True)

		self.PelvisIndep_Rev = lrc.Reverse()
		self.PelvisIndep_Rev.name = 'PelvisIndep%s_Rev' % part

		self.Pelvis_Ctrl.attr('indepHip') >> tmpCons.attr('w1')
		self.Pelvis_Ctrl.attr('indepHip') >> self.PelvisIndep_Rev.attr('ix')
		self.PelvisIndep_Rev.attr('ox') >> tmpCons.attr('w0')

		# Ribbon
		self.SpineLen = lrc.distance(self.Spine_Jnt, self.Chest_Jnt)

		self.SpineRbn_Nrb = lrr.ribbonSurface()
		self.SpineRbn_Nrb.name = 'SpineRbn%s_Nrb' % part
		self.SpineRbn_Nrb.scaleShape(self.SpineLen)

		self.SpineRbn_Nrb.snapPoint(self.Spine_Jnt)
		lrr.localAim(self.Chest_Jnt, self.SpineRbn_Nrb)
		self.SpineRbn_Nrb.parent(self.Still_Grp)

		mc.rebuildSurface(self.SpineRbn_Nrb, ch=False, rpo=1, rt=0, 
							end=1, kr=0, kcp=0, kc=0, du=1, su=1, sv=3, 
							dv=3, fr=2, dir=2)

		(self.SpineRbn_Crv,
		self.SpineRbnCrv_Cfs) = lrc.duplicateCurve(
														'%s.u[0.5]' % self.SpineRbn_Nrb,
														ch=True,
														rn=0,
														local=0
													)

		self.SpineRbn_Crv.name = 'SpineRbn%s_Crv' % part
		self.SpineRbnCrv_Cfs.name = 'SpineRbnCrv%s_Cfs' % part

		self.SpineRbn_Crv.parent(self.Still_Grp)

		self.SpineRbnCrv_Cif = lrc.arclen(self.SpineRbn_Crv, ch=True)
		self.SpineRbnCrv_Cif.name = 'SpineRbnCrv%s_Cif' % part

		# Chest squash
		self.Chest_Ctrl.add(ln='squashX', k=True)
		self.Chest_Ctrl.add(ln='squashZ', k=True)

		self.ChestSqsX_Add = lrc.AddDoubleLinear()
		self.ChestSqsX_Add.name = 'ChestSqsX%s_Add' % part
		self.ChestSqsX_Add.add(ln='default', k=True, dv=1, min=1, max=1)
		self.ChestSqsX_Add.attr('default') >> self.ChestSqsX_Add.attr('i1')
		self.ChestSqsX_Add.attr('o') >> self.ChestSca_Jnt.attr('sx')

		self.ChestSqsX_Mul = lrr.attrAmper('%s.squashX' % self.Chest_Ctrl, '%s.i2' % self.ChestSqsX_Add, dv=0.1)
		self.ChestSqsX_Mul.name = 'ChestSqsX%s_Mul' % part

		self.ChestSqsZ_Add = lrc.AddDoubleLinear()
		self.ChestSqsZ_Add.name = 'ChestSqsZ%s_Add' % part
		self.ChestSqsZ_Add.add(ln='default', k=True, dv=1, min=1, max=1)
		self.ChestSqsZ_Add.attr('default') >> self.ChestSqsZ_Add.attr('i1')
		self.ChestSqsZ_Add.attr('o') >> self.ChestSca_Jnt.attr('sz')

		self.ChestSqsZ_Mul = lrr.attrAmper('%s.squashZ' % self.Chest_Ctrl, '%s.i2' % self.ChestSqsZ_Add, dv=0.1)
		self.ChestSqsZ_Mul.name = 'ChestSqsZ%s_Mul' % part

		# Squash feedback
		chestCtrlShp.add(ln='restLength', dv=self.SpineRbnCrv_Cif.attr('arcLength').v)
		self.Chest_Ctrl.add(ln='length', k=True)
		self.Chest_Ctrl.attr('length').cb = True

		self.SpineLenFb_Mdv = lrc.MultiplyDivide()
		self.SpineLenFb_Mdv.name = 'SpineLenFb%s_Mdv' % part

		self.SpineLenFb_Mdv.attr('operation').v = 2
		self.SpineRbnCrv_Cif.attr('arcLength') >> self.SpineLenFb_Mdv.attr('i1x')
		chestCtrlShp.attr('restLength') >> self.SpineLenFb_Mdv.attr('i2x')
		self.SpineLenFb_Mdv.attr('ox') >> self.Chest_Ctrl.attr('length')

		# Squash rig
		self.Chest_Ctrl.add(ln='autoSquash', k=True, min=0, max=1)
		self.SpineSqsDiv_Mdv = lrc.MultiplyDivide()
		self.SpineSqsPow_Mdv = lrc.MultiplyDivide()
		self.SpineSqsNorm_Mdv = lrc.MultiplyDivide()

		self.SpineSqsDiv_Mdv.name = 'SpineSqsDiv%s_Mdv' % part
		self.SpineSqsPow_Mdv.name = 'SpineSqsPow%s_Mdv' % part
		self.SpineSqsNorm_Mdv.name = 'SpineSqsNorm%s_Mdv' % part

		self.SpineSqsNorm_Mdv.attr('operation').v = 2
		self.SpineRbnCrv_Cif.attr('arcLength') >> self.SpineSqsNorm_Mdv.attr('i1x')
		chestCtrlShp.attr('restLength') >> self.SpineSqsNorm_Mdv.attr('i2x')
		
		self.SpineSqsPow_Mdv.attr('operation').v = 3
		self.SpineSqsNorm_Mdv.attr('ox') >> self.SpineSqsPow_Mdv.attr('i1x')
		self.SpineSqsPow_Mdv.attr('i2x').v = 2
		
		self.SpineSqsDiv_Mdv.attr('operation').v = 2
		self.SpineSqsDiv_Mdv.attr('i1x').v = 1
		self.SpineSqsPow_Mdv.attr('ox') >> self.SpineSqsDiv_Mdv.attr('i2x')
		
		skinJnts = []
		skinJntZrGrps = []
		poss = []
		posis = []

		atSqsMulDict = {0: 0.33, 1: 0.66, 2: 1, 3: 0.66, 4: 0.33}
		self.sqsXPmas = []
		self.sqsZPmas = []
		atSqsBlnds = []
		atSqsAdds = []
		atSqsMuls = []
		atSqsAmpPmas = []

		for ix in range(5):

			pos = lrc.Null()
			posi = lrc.PointOnSurfaceInfo()

			pos.name = 'SpineRbnPos%s_%s_Grp' % (part, ix+1)
			posi.name = 'SpineRbn%s_%s_Posi' % (part, ix+1)
			
			self.SpineRbn_Nrb.attr('worldSpace[0]') >> posi.attr('is')
			posi.attr('position') >> pos.attr('t')
			posi.attr('parameterU').v = 0.5
			posi.attr('turnOnPercentage').v = 1
			
			aimCon = lrc.AimConstraint()
			aimCon.name = '%s_aimConstraint' % pos
			aimCon.attr('a').v = (0, 1, 0)
			aimCon.attr('u').v = (1, 0, 0)
			
			posi.attr('n') >> aimCon.attr('wu')
			posi.attr('tv') >> aimCon.attr('tg[0].tt')
			aimCon.attr('cr') >> pos.attr('r')
			
			aimCon.parent(pos)
			aimCon.attr('t').v = (0, 0, 0)
			aimCon.attr('r').v = (0, 0, 0)
			pos.parent(self.Still_Grp)
			
			skinJnt = lrc.Joint()
			skinJntZrGrp = lrc.group(skinJnt)

			skinJnt.name = 'SpineSkin%s_%s_Jnt' % (part, ix+1)
			skinJntZrGrp.name = 'SpineSkinJntZr%s_%s_Grp' % (part, ix+1)

			skinJntZrGrp.parent(self.Skin_Grp)
			
			mc.parentConstraint(pos, skinJntZrGrp)

			poss.append(pos)
			posis.append(posi)
			skinJnts.append(skinJnt)
			skinJntZrGrps.append(skinJntZrGrp)

			# Auto squash
			sqsXPma = lrc.PlusMinusAverage()
			sqsZPma = lrc.PlusMinusAverage()
			atSqsMul = lrc.MultDoubleLinear()
			atSqsAdd = lrc.AddDoubleLinear()
			atSqsBlnd = lrc.BlendTwoAttr()
			atSqsAmpPma = lrc.PlusMinusAverage()

			sqsXPma.name = 'SpineSkinSqsX%s_%s_Pma' % (part, ix+1)
			sqsZPma.name = 'SpineSkinSqsZ%s_%s_Pma' % (part, ix+1)
			atSqsMul.name = 'SpineSkinAtSqs%s_%s_Mul' % (part, ix+1)
			atSqsAdd.name = 'SpineSkinAtSqs%s_%s_Add' % (part, ix+1)
			atSqsBlnd.name = 'SpineSkinAtSqs%s_%s_Blend' % (part, ix+1)
			atSqsAmpPma.name = 'SpineSkinAtSqs%s_%s_Pma' % (part, ix+1)

			self.sqsXPmas.append(sqsXPma)
			self.sqsZPmas.append(sqsZPma)
			atSqsMuls.append(atSqsMul)
			atSqsAdds.append(atSqsAdd)
			atSqsBlnds.append(atSqsBlnd)
			atSqsAmpPmas.append(atSqsAmpPma)

			currSqsAttr = 'spine%sSquash' % str(1+ix)

			currAtSqsAmp = atSqsMulDict[ix]
			currAmpAttr = 'autoSquash%sAmp' % str(1+ix)
			chestCtrlShp.add(ln=currAmpAttr, dv=currAtSqsAmp)
			self.Chest_Ctrl.add(ln=currSqsAttr, k=True)

			self.Chest_Ctrl.attr('autoSquash') >> atSqsBlnd.attr('ab')
			atSqsBlnd.add(ln='default', min=1, max=1, dv=1, k=True)
			atSqsBlnd.attr('default') >> atSqsBlnd.last()

			atSqsMul.add(ln='amp', k=True)
			atSqsMul.attr('amp').v = currAtSqsAmp
			atSqsMul.attr('amp') >> atSqsMul.attr('i1')
			self.SpineSqsDiv_Mdv.attr('ox') >> atSqsMul.attr('i2')

			atSqsAmpPma.add(ln='default', k=True)
			atSqsAmpPma.attr('operation').v = 2
			atSqsAmpPma.attr('default').v = 1
			atSqsAmpPma.attr('default') >> atSqsAmpPma.last1D()
			chestCtrlShp.attr(currAmpAttr) >> atSqsAmpPma.last1D()

			atSqsAmpPma.attr('output1D') >> atSqsAdd.attr('i1')
			atSqsMul.attr('o') >> atSqsAdd.attr('i2')
			atSqsAdd.attr('o') >> atSqsBlnd.last()

			self.Chest_Ctrl.attr(currSqsAttr) >> sqsXPma.last1D()
			self.Chest_Ctrl.attr(currSqsAttr) >> sqsZPma.last1D()
			atSqsBlnd.attr('o') >> sqsXPma.last1D()
			atSqsBlnd.attr('o') >> sqsZPma.last1D()
			sqsXPma.attr('output1D') >> skinJnt.attr('sx')
			sqsZPma.attr('output1D') >> skinJnt.attr('sz')

		posis[0].attr('parameterV').v = 0.05
		posis[1].attr('parameterV').v = 0.3
		posis[2].attr('parameterV').v = 0.5
		posis[3].attr('parameterV').v = 0.7
		posis[4].attr('parameterV').v = 0.95

		(self.SpineRbnPos_1_Grp,
		self.SpineRbnPos_2_Grp,
		self.SpineRbnPos_3_Grp,
		self.SpineRbnPos_4_Grp,
		self.SpineRbnPos_5_Grp
		) = poss
		(self.SpineRbn_1_Posi,
		self.SpineRbn_2_Posi,
		self.SpineRbn_3_Posi,
		self.SpineRbn_4_Posi,
		self.SpineRbn_5_Posi
		) = posis

		# Binding skin
		self.rbnSkin = mc.skinCluster(
										self.PelvisRbn_Jnt,
										self.ChestRbn_Jnt,
										self.SpineRbn_Nrb,
										dr=7,
										mi=2
									)[0]

		mc.skinPercent(
							self.rbnSkin,
							'%s.cv[0:1][0]' % self.SpineRbn_Nrb,
							tv=[
									(self.PelvisRbn_Jnt, 1),
									(self.ChestRbn_Jnt, 0),
								]
						)

		mc.skinPercent(
							self.rbnSkin,
							'%s.cv[0:1][1]' % self.SpineRbn_Nrb,
							tv=[
									(self.PelvisRbn_Jnt, 1),
									(self.ChestRbn_Jnt, 0)
								]
						)

		mc.skinPercent(
							self.rbnSkin,
							'%s.cv[0:1][2]' % self.SpineRbn_Nrb,
							tv=[
									(self.PelvisRbn_Jnt, 1),
									(self.ChestRbn_Jnt, 0)
								]
						)

		mc.skinPercent(
							self.rbnSkin,
							'%s.cv[0:1][3]' % self.SpineRbn_Nrb,
							tv=[
									(self.PelvisRbn_Jnt, 0),
									(self.ChestRbn_Jnt, 1)
								]
						)

		mc.skinPercent(
							self.rbnSkin,
							'%s.cv[0:1][4]' % self.SpineRbn_Nrb,
							tv=[
									(self.PelvisRbn_Jnt, 0),
									(self.ChestRbn_Jnt, 1)
								]
						)

		mc.skinPercent(
							self.rbnSkin,
							'%s.cv[0:1][5]' % self.SpineRbn_Nrb,
							tv=[
									(self.PelvisRbn_Jnt, 0),
									(self.ChestRbn_Jnt, 1)
								]
						)

class TorsoRig2(TorsoRig):
	"""Adding middle spine ribbon controller.
	"""
	def __init__(
					self,
					parent='',
					ctrlGrp='',
					jntGrp='',
					skinGrp='',
					stillGrp='',
					tmpJnt=(
								'Pelvis_Jnt',
								'Spine_Jnt',
								'Chest_Jnt',
								'ChestTip_Jnt',
								'SpineMid_Jnt'
							),
					part=''
				):

		super(TorsoRig2, self).__init__(parent=parent, ctrlGrp=ctrlGrp, jntGrp=jntGrp, 
											skinGrp=skinGrp, stillGrp=stillGrp, 
											tmpJnt=tmpJnt, part=part)
		# Ribbon joint
		self.SpineRbn_Jnt = lrc.Joint()
		self.SpineRbn_Jnt.rotateOrder = 'yzx'
		self.SpineRbn_Jnt.name = 'SpineRbn%s_Jnt' % part

		self.SpineRbnJntZr_Grp = lrc.group(self.SpineRbn_Jnt)
		self.SpineRbnJntZr_Grp.name = 'SpineRbnJntZr%s_Grp' % part
		self.SpineRbnJntZr_Grp.snap(self.SpineMid_Jnt)

		self.SpineRbnJntZr_Grp.parent(self.Jnt_Grp)

		# Control ribbon
		self.SpineRbnCtrl_Nrb = lrr.ribbonSurface()
		self.SpineRbnCtrl_Nrb.name = 'SpineRbnCtrl%s_Nrb' % part
		self.SpineRbnCtrl_Nrb.scaleShape(self.SpineLen)

		self.SpineRbnCtrl_Nrb.snapPoint(self.Spine_Jnt)
		lrr.localAim(self.Chest_Jnt, self.SpineRbnCtrl_Nrb)
		self.SpineRbnCtrl_Nrb.parent(self.Still_Grp)

		mc.rebuildSurface(self.SpineRbnCtrl_Nrb, ch=False, rpo=1, rt=0, 
							end=1, kr=0, kcp=0, kc=0, du=1, su=1, sv=3, 
							dv=3, fr=2, dir=2)

		# Binding skin
		self.ctrlRbnSkin = mc.skinCluster(
										self.PelvisRbn_Jnt,
										self.ChestRbn_Jnt,
										self.SpineRbnCtrl_Nrb,
										dr=7,
										mi=2
									)[0]

		mc.skinPercent(
							self.ctrlRbnSkin,
							'%s.cv[0:1][0]' % self.SpineRbnCtrl_Nrb,
							tv=[
									(self.PelvisRbn_Jnt, 1),
									(self.ChestRbn_Jnt, 0),
								]
						)

		mc.skinPercent(
							self.ctrlRbnSkin,
							'%s.cv[0:1][1]' % self.SpineRbnCtrl_Nrb,
							tv=[
									(self.PelvisRbn_Jnt, 1),
									(self.ChestRbn_Jnt, 0)
								]
						)

		mc.skinPercent(
							self.ctrlRbnSkin,
							'%s.cv[0:1][2]' % self.SpineRbnCtrl_Nrb,
							tv=[
									(self.PelvisRbn_Jnt, 1),
									(self.ChestRbn_Jnt, 0)
								]
						)

		mc.skinPercent(
							self.ctrlRbnSkin,
							'%s.cv[0:1][3]' % self.SpineRbnCtrl_Nrb,
							tv=[
									(self.PelvisRbn_Jnt, 0),
									(self.ChestRbn_Jnt, 1)
								]
						)

		mc.skinPercent(
							self.ctrlRbnSkin,
							'%s.cv[0:1][4]' % self.SpineRbnCtrl_Nrb,
							tv=[
									(self.PelvisRbn_Jnt, 0),
									(self.ChestRbn_Jnt, 1)
								]
						)

		mc.skinPercent(
							self.ctrlRbnSkin,
							'%s.cv[0:1][5]' % self.SpineRbnCtrl_Nrb,
							tv=[
									(self.PelvisRbn_Jnt, 0),
									(self.ChestRbn_Jnt, 1)
								]
						)

		# Controller
		self.SpineRbn_Ctrl = lrr.jointControl('circle')
		self.SpineRbn_Ctrl.name = 'SpineRbn%s_Ctrl' % part
		self.SpineRbn_Ctrl.color = 'yellow'
		self.SpineRbn_Ctrl.lockHideAttrs('s', 'v')

		self.SpineRbnCtrlZr_Grp = lrc.group(self.SpineRbn_Ctrl)
		self.SpineRbnCtrlZr_Grp.name = 'SpineRbnCtrlZr%s_Grp' % part

		self.SpineRbnCtrlZr_Grp.snap(self.SpineRbn_Jnt)
		self.SpineRbnCtrlZr_Grp.parent(self.Ctrl_Grp)
		mc.parentConstraint(self.SpineRbn_Ctrl, self.SpineRbn_Jnt)
		# mc.parentConstraint(self.ChestRbn_Jnt, self.PelvisRbn_Jnt, 
		# 						self.SpineRbnCtrlZr_Grp, mo=True)

		self.SpineRbnCtrlPos_Grp = lrc.Null()
		self.SpineRbnCtrlPos_Grp.name = 'SpineRbnCtrlPos%s_Grp' % part

		self.SpineRbnCtrl_Posi = lrc.PointOnSurfaceInfo()
		self.SpineRbnCtrl_Posi.name = 'SpineRbnCtrl%s_Posi' % part

		self.SpineRbnCtrl_Nrb.attr('worldSpace[0]') >> self.SpineRbnCtrl_Posi.attr('is')
		self.SpineRbnCtrl_Posi.attr('position') >> self.SpineRbnCtrlPos_Grp.attr('t')
		self.SpineRbnCtrl_Posi.attr('turnOnPercentage').v = 1
		self.SpineRbnCtrl_Posi.attr('parameterU').v = 0.5
		self.SpineRbnCtrl_Posi.attr('parameterV').v = 0.5
		
		aimCon = lrc.AimConstraint()
		aimCon.name = '%s_aimConstraint' % self.SpineRbnCtrlPos_Grp
		aimCon.attr('a').v = (0, 1, 0)
		aimCon.attr('u').v = (1, 0, 0)
		
		self.SpineRbnCtrl_Posi.attr('n') >> aimCon.attr('wu')
		self.SpineRbnCtrl_Posi.attr('tv') >> aimCon.attr('tg[0].tt')
		aimCon.attr('cr') >> self.SpineRbnCtrlPos_Grp.attr('r')
		
		aimCon.parent(self.SpineRbnCtrlPos_Grp)
		aimCon.attr('t').v = (0, 0, 0)
		aimCon.attr('r').v = (0, 0, 0)
		self.SpineRbnCtrlPos_Grp.parent(self.Still_Grp)

		mc.parentConstraint(self.SpineRbnCtrlPos_Grp, self.SpineRbnCtrlZr_Grp)

		# Editing skin weight values
		mc.skinCluster(self.rbnSkin, edit=True, ai=self.SpineRbn_Jnt)
		mc.skinPercent(
							self.rbnSkin,
							'%s.cv[0:1][0]' % self.SpineRbn_Nrb,
							tv=[
									(self.PelvisRbn_Jnt, 1),
									(self.SpineRbn_Jnt, 0),
									(self.ChestRbn_Jnt, 0),
								]
						)

		mc.skinPercent(
							self.rbnSkin,
							'%s.cv[0:1][1]' % self.SpineRbn_Nrb,
							tv=[
									(self.PelvisRbn_Jnt, 0.8),
									(self.SpineRbn_Jnt, 0.2),
									(self.ChestRbn_Jnt, 0)
								]
						)

		mc.skinPercent(
							self.rbnSkin,
							'%s.cv[0:1][2]' % self.SpineRbn_Nrb,
							tv=[
									(self.PelvisRbn_Jnt, 0.1),
									(self.SpineRbn_Jnt, 0.9),
									(self.ChestRbn_Jnt, 0)
								]
						)

		mc.skinPercent(
							self.rbnSkin,
							'%s.cv[0:1][3]' % self.SpineRbn_Nrb,
							tv=[
									(self.PelvisRbn_Jnt, 0),
									(self.SpineRbn_Jnt, 0.9),
									(self.ChestRbn_Jnt, 0.1)
								]
						)

		mc.skinPercent(
							self.rbnSkin,
							'%s.cv[0:1][4]' % self.SpineRbn_Nrb,
							tv=[
									(self.PelvisRbn_Jnt, 0),
									(self.SpineRbn_Jnt, 0.2),
									(self.ChestRbn_Jnt, 0.8)
								]
						)

		mc.skinPercent(
							self.rbnSkin,
							'%s.cv[0:1][5]' % self.SpineRbn_Nrb,
							tv=[
									(self.PelvisRbn_Jnt, 0),
									(self.SpineRbn_Jnt, 0),
									(self.ChestRbn_Jnt, 1)
								]
						)

		# Scaling
		self.SpineRbn_Ctrl.add(ln='squash', k=True)

		self.SpineRbn_Ctrl.add(ln='squashAmp1x', dv=0.1)
		self.SpineRbn_Ctrl.add(ln='squashAmp1z', dv=0.1)
		self.SpineRbn_Ctrl.add(ln='squashAmp2x', dv=0.5)
		self.SpineRbn_Ctrl.add(ln='squashAmp2z', dv=0.5)
		self.SpineRbn_Ctrl.add(ln='squashAmp3x', dv=1)
		self.SpineRbn_Ctrl.add(ln='squashAmp3z', dv=1)
		self.SpineRbn_Ctrl.add(ln='squashAmp4x', dv=0.5)
		self.SpineRbn_Ctrl.add(ln='squashAmp4z', dv=0.5)
		self.SpineRbn_Ctrl.add(ln='squashAmp5x', dv=0.1)
		self.SpineRbn_Ctrl.add(ln='squashAmp5z', dv=0.1)

		self.SpineRbnSqs_1_Mdv = lrc.MultiplyDivide()
		self.SpineRbnSqs_1_Mdv.name = 'SpineRbnSqs%s_1_Mdv' % part

		self.SpineRbnSqs_2_Mdv = lrc.MultiplyDivide()
		self.SpineRbnSqs_2_Mdv.name = 'SpineRbnSqs%s_2_Mdv' % part

		self.SpineRbnSqs_3_Mdv = lrc.MultiplyDivide()
		self.SpineRbnSqs_3_Mdv.name = 'SpineRbnSqs%s_3_Mdv' % part

		self.SpineRbnSqs_4_Mdv = lrc.MultiplyDivide()
		self.SpineRbnSqs_4_Mdv.name = 'SpineRbnSqs%s_4_Mdv' % part

		self.SpineRbnSqs_5_Mdv = lrc.MultiplyDivide()
		self.SpineRbnSqs_5_Mdv.name = 'SpineRbnSqs%s_5_Mdv' % part

		self.SpineRbn_Ctrl.attr('squash') >> self.SpineRbnSqs_1_Mdv.attr('i1x')
		self.SpineRbn_Ctrl.attr('squash') >> self.SpineRbnSqs_1_Mdv.attr('i1z')
		self.SpineRbn_Ctrl.attr('squashAmp1x') >> self.SpineRbnSqs_1_Mdv.attr('i2x')
		self.SpineRbn_Ctrl.attr('squashAmp1z') >> self.SpineRbnSqs_1_Mdv.attr('i2z')
		self.SpineRbnSqs_1_Mdv.attr('ox') >> self.sqsXPmas[0].last1D()
		self.SpineRbnSqs_1_Mdv.attr('oz') >> self.sqsZPmas[0].last1D()

		self.SpineRbn_Ctrl.attr('squash') >> self.SpineRbnSqs_2_Mdv.attr('i1x')
		self.SpineRbn_Ctrl.attr('squash') >> self.SpineRbnSqs_2_Mdv.attr('i1z')
		self.SpineRbn_Ctrl.attr('squashAmp2x') >> self.SpineRbnSqs_2_Mdv.attr('i2x')
		self.SpineRbn_Ctrl.attr('squashAmp2z') >> self.SpineRbnSqs_2_Mdv.attr('i2z')
		self.SpineRbnSqs_2_Mdv.attr('ox') >> self.sqsXPmas[1].last1D()
		self.SpineRbnSqs_2_Mdv.attr('oz') >> self.sqsZPmas[1].last1D()

		self.SpineRbn_Ctrl.attr('squash') >> self.SpineRbnSqs_3_Mdv.attr('i1x')
		self.SpineRbn_Ctrl.attr('squash') >> self.SpineRbnSqs_3_Mdv.attr('i1z')
		self.SpineRbn_Ctrl.attr('squashAmp3x') >> self.SpineRbnSqs_3_Mdv.attr('i2x')
		self.SpineRbn_Ctrl.attr('squashAmp3z') >> self.SpineRbnSqs_3_Mdv.attr('i2z')
		self.SpineRbnSqs_3_Mdv.attr('ox') >> self.sqsXPmas[2].last1D()
		self.SpineRbnSqs_3_Mdv.attr('oz') >> self.sqsZPmas[2].last1D()

		self.SpineRbn_Ctrl.attr('squash') >> self.SpineRbnSqs_4_Mdv.attr('i1x')
		self.SpineRbn_Ctrl.attr('squash') >> self.SpineRbnSqs_4_Mdv.attr('i1z')
		self.SpineRbn_Ctrl.attr('squashAmp4x') >> self.SpineRbnSqs_4_Mdv.attr('i2x')
		self.SpineRbn_Ctrl.attr('squashAmp4z') >> self.SpineRbnSqs_4_Mdv.attr('i2z')
		self.SpineRbnSqs_4_Mdv.attr('ox') >> self.sqsXPmas[3].last1D()
		self.SpineRbnSqs_4_Mdv.attr('oz') >> self.sqsZPmas[3].last1D()

		self.SpineRbn_Ctrl.attr('squash') >> self.SpineRbnSqs_5_Mdv.attr('i1x')
		self.SpineRbn_Ctrl.attr('squash') >> self.SpineRbnSqs_5_Mdv.attr('i1z')
		self.SpineRbn_Ctrl.attr('squashAmp5x') >> self.SpineRbnSqs_5_Mdv.attr('i2x')
		self.SpineRbn_Ctrl.attr('squashAmp5z') >> self.SpineRbnSqs_5_Mdv.attr('i2z')
		self.SpineRbnSqs_5_Mdv.attr('ox') >> self.sqsXPmas[4].last1D()
		self.SpineRbnSqs_5_Mdv.attr('oz') >> self.sqsZPmas[4].last1D()
