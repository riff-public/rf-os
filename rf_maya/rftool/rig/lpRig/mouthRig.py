import maya.cmds as mc
import maya.mel as mm

from lpRig import bshTools as bshTools
reload(bshTools)
from lpRig import core as lrc
reload(lrc)
from lpRig import rigTools as lrr
reload(lrr)

def createBsh(bshBuf='', part='', lite=False):

	ctrl = ''
	ctrlNm = 'Mth%s_Ctrl' % part

	for each in mc.ls():
		if each.endswith(ctrlNm):
			ctrl = each
			break

	ctrlNs = ctrl.replace(ctrlNm, '')

	bshGrp = lrc.Null()
	bshGrp.name = '%sBsh_Grp' % part

	# Mouth Shapes
	mthShps = [
					'LipUpRollOut',# 0
					'LipUpRollIn',# 1
					'LipUpPuff',# 2
					'LipLowRollOut',# 3
					'LipLowRollIn',# 4
					'LipLowPuff',# 5
					'U',# 6
					'LipUpThick',# 7
					'LipLowThick',# 8
					'JawOpen'# 9
				]

	if lite:
		mthShps.append('Up') # 10
		mthShps.append('Dn') # 11
		mthShps.append('Lft') # 12
		mthShps.append('Rgt') # 13

	# Populate blend shape names from shape names.
	mthBshNms = ['%s%s_Bsh' % (mthShp, part) for mthShp in mthShps]

	# Mouth Shapes(each side)
	mthSideShps = [
					'CnrOut',# 0
					'CnrIn',# 1
					'CnrInUp',# 2
					'CnrUp',# 3
					'CnrOutUp',# 4
					'CnrInDn',# 5
					'CnrDn',# 6
					'CnrOutDn',# 7
					'CnrDnOpen',# 8
					'CnrOpen',# 9
					'CnrUpOpen',# 10
					'CnrInDnOpen',# 11
					'CnrInOpen',# 12
					'CnrInUpOpen',# 13
					'CnrOutDnOpen',# 14
					'CnrOutOpen',# 15
					'CnrOutUpOpen',# 16
					'Cheek',# 17
					'Puff' # 18
				]

	if lite:
		mthSideShps.append('Seal') #19
		mthSideShps.append('Apart') #20
	
	# Populate blend shape names from shape names.
	mthLftBshNms = ['%s%s_L_Bsh' % (mthSideShp, part) for mthSideShp in mthSideShps]
	mthRgtBshNms = ['%s%s_R_Bsh' % (mthSideShp, part) for mthSideShp in mthSideShps]
	
	# Duplicate blend shape objects.
	dupBshs, txtCrvs = bshTools.dupBlendShapeList(bshBuf, [mthBshNms, mthLftBshNms, mthRgtBshNms])
	mc.parent(dupBshs, txtCrvs, bshGrp)

	# Create blend shape.
	bsn = lrc.Node('%sBuf_Bsn' % part)
	mc.blendShape(mthBshNms+mthLftBshNms+mthRgtBshNms, bshBuf, n=bsn)

	# Connections
	ctrl = lrc.Dag('%sMth%s_Ctrl' % (ctrlNs, part))
	ctrl.add(ln='lipUpRoll', dv=0, min=-1, max=1, k=True)
	ctrl.add(ln='lipLowRoll', dv=0, min=-1, max=1, k=True)
	ctrl.add(ln='lipUpPuff', dv=0, min=-1, max=1, k=True)
	ctrl.add(ln='lipLowPuff', dv=0, min=-1, max=1, k=True)
	ctrl.add(ln='lipUpThick', dv=0, min=-1, max=1, k=True)
	ctrl.add(ln='lipLowThick', dv=0, min=-1, max=1, k=True)
	ctrl.add(ln='u', dv=0, min=-1, max=1, k=True)
	infoDict = {
					0: {'lipUpRoll': (mthBshNms[0], mthBshNms[1])},
					1: {'lipLowRoll': (mthBshNms[3], mthBshNms[4])},
					2: {'lipUpPuff': mthBshNms[2]},
					3: {'lipLowPuff': mthBshNms[5]},
					4: {'lipUpThick': mthBshNms[7]},
					5: {'lipLowThick': mthBshNms[8]},
					6: {'u': mthBshNms[6]}
				}
	bshTools.ctrlToBsh(ctrl.name, bsn, infoDict)

	if lite:
		infoDict = {
						0: {'ud': (mthBshNms[10], mthBshNms[11])},
						1: {'lr': (mthBshNms[12], mthBshNms[13])},
					}
		bshTools.ctrlToBsh(ctrl.name, bsn, infoDict)

	jawCtrl = lrc.Dag('%sJaw%s_Ctrl' % (ctrlNs, part))
	jawCtrlShp = lrc.Dag(jawCtrl.shape)
	jawCtrlShp.add(ln='__jaw__', k=True)
	jawCtrlShp.add(ln='jawOpen', k=True, dv=27)

	for ix in range(2):
		# Corrective blend shapes principle.
		# Open -> CnrOpen 1
		
		# CnrInOpen -> CnrOpen 1 CnrInOpen 1
		# CnrInUpOpen -> CnrOpen 1 CnrInUpOpen 1 CnrInUp 1
		# CnrInDnOpen -> CnrOpen 1 CnrInDnOpen 1 CnrInDn 1
		# CnrOutOpen -> CnrOpen 1 CnrOutOpen 1
		# CnrOutUpOpen -> CnrOpen 1 CnrOutUpOpen 1 CnrOutUp 1
		# CnrOutDnOpen -> CnrOpen 1 CnrOutDnOpen 1 CnrOutDn 1
		
		# CnrUpOpen -> CnrOpen 0 CnrUpOpen 1 CnrUp 1
		# CnrDnOpen -> CnrOpen 0 CnrDnOpen 1 CnrDn 1

		# Iterate through each side and each blend shape name list.
		side = ('_L_', '_R_')[ix]
		bshNms = (mthLftBshNms, mthRgtBshNms)[ix]
		sealAttr = ('lftSeal', 'rgtSeal')[ix]

		ctrl = lrc.Dag('%sMth%s%sCtrl' % (ctrlNs, part, side))
		ctrlShp = lrc.Dag(ctrl.shape)
		ctrl.add(ln='cheek', dv=0, k=True)
		ctrl.add(ln='puff', dv=0, k=True)

		slRev = None
		# Seal remap
		if not lite:
			slRem = lrc.RemapValue()
			slRem.name = 'LipSeal%s%sRem' % (part, side)

			slRev = lrc.Reverse()
			slRev.name = 'LipSealRem%s%sRev' % (part, side)

			jawCtrlShp.attr('upCnr%sMin' % side) >> slRem.attr('inputMin')
			jawCtrlShp.attr('upCnr%sMax' % side) >> slRem.attr('inputMax')
			jawCtrl.attr(sealAttr) >> slRem.attr('inputValue')
			slRem.attr('outValue') >> slRev.attr('ix')

		# Clamp valuses for each channel
		cnrOutClamp = lrc.Clamp()
		cnrOutClamp.name = 'CnrOut%s%sClamp' % (part, side)
		cnrOutClamp.attr('maxR').v = 100
		ctrl.attr('tx') >> cnrOutClamp.attr('ipr')

		cnrInClamp = lrc.Clamp()
		cnrInClamp.name = 'CnrIn%s%sClamp' % (part, side)
		cnrInClamp.attr('minR').v = -100
		ctrl.attr('tx') >> cnrInClamp.attr('ipr')

		cnrUpClamp = lrc.Clamp()
		cnrUpClamp.name = 'CnrUp%s%sClamp' % (part, side)
		cnrUpClamp.attr('maxR').v = 100
		ctrl.attr('ty') >> cnrUpClamp.attr('ipr')

		cnrDnClamp = lrc.Clamp()
		cnrDnClamp.name = 'CnrDn%s%sClamp' % (part, side)
		cnrDnClamp.attr('minR').v = -100
		ctrl.attr('ty') >> cnrDnClamp.attr('ipr')

		# Inverter - Invert values for corner mouth in and down.
		cnrInInvMdv = lrc.MultiplyDivide()
		cnrInInvMdv.name = 'CnrInInv%s%sMdv' % (part, side)
		cnrInInvMdv.attr('i2x').v = -1
		cnrInClamp.attr('opr') >> cnrInInvMdv.attr('i1x')

		cnrDnInvMdv = lrc.MultiplyDivide()
		cnrDnInvMdv.name = 'CnrDnInv%s%sMdv' % (part, side)
		cnrDnInvMdv.attr('i2x').v = -1
		cnrDnClamp.attr('opr') >> cnrDnInvMdv.attr('i1x')

		# Multiplier
		cnrInUpMultMdv = lrc.MultiplyDivide()
		cnrInUpMultMdv.name = 'CnrInUpMult%s%sMdv' % (part, side)
		cnrInInvMdv.attr('ox') >> cnrInUpMultMdv.attr('i1x')
		cnrUpClamp.attr('opr') >> cnrInUpMultMdv.attr('i2x')
		cnrInUpMultMdv.attr('ox') >> bsn.attr(bshNms[2])

		cnrOutUpMultMdv = lrc.MultiplyDivide()
		cnrOutUpMultMdv.name = 'CnrOutUpMult%s%sMdv' % (part, side)
		cnrOutClamp.attr('opr') >> cnrOutUpMultMdv.attr('i1x')
		cnrUpClamp.attr('opr') >> cnrOutUpMultMdv.attr('i2x')
		cnrOutUpMultMdv.attr('ox') >> bsn.attr(bshNms[4])

		cnrInDnMultMdv = lrc.MultiplyDivide()
		cnrInDnMultMdv.name = 'CnrInDnMult%s%sMdv' % (part, side)
		cnrInInvMdv.attr('ox') >> cnrInDnMultMdv.attr('i1x')
		cnrDnInvMdv.attr('ox') >> cnrInDnMultMdv.attr('i2x')
		cnrInDnMultMdv.attr('ox') >> bsn.attr(bshNms[5])

		cnrOutDnMultMdv = lrc.MultiplyDivide()
		cnrOutDnMultMdv.name = 'CnrOutDnMult%s%sMdv' % (part, side)
		cnrOutClamp.attr('opr') >> cnrOutDnMultMdv.attr('i1x')
		cnrDnInvMdv.attr('ox') >> cnrOutDnMultMdv.attr('i2x')
		cnrOutDnMultMdv.attr('ox') >> bsn.attr(bshNms[7])

		# Subtractor
		# Subtractor - Corner Up
		cnrUpSubPma = lrc.PlusMinusAverage()
		cnrUpSubPma.name = 'CnrUpSub%s%sPma' % (part, side)
		cnrUpSubPma.attr('op').v = 2

		cnrUpClamp.attr('opr') >> cnrUpSubPma.last1D()
		cnrInUpMultMdv.attr('ox') >> cnrUpSubPma.last1D()
		cnrOutUpMultMdv.attr('ox') >> cnrUpSubPma.last1D()

		cnrUpSubPma.attr('o1') >> bsn.attr(bshNms[3])

		# Subtractor - Corner Down
		cnrDnSubPma = lrc.PlusMinusAverage()
		cnrDnSubPma.name = 'CnrDnSub%s%sPma' % (part, side)
		cnrDnSubPma.attr('op').v = 2

		cnrDnInvMdv.attr('ox') >> cnrDnSubPma.last1D()
		cnrInDnMultMdv.attr('ox') >> cnrDnSubPma.last1D()
		cnrOutDnMultMdv.attr('ox') >> cnrDnSubPma.last1D()

		cnrDnSubPma.attr('o1') >> bsn.attr(bshNms[6])

		# Subtractor - Corner In
		cnrInSubPma = lrc.PlusMinusAverage()
		cnrInSubPma.name = 'CnrInSub%s%sPma' % (part, side)
		cnrInSubPma.attr('op').v = 2

		cnrInInvMdv.attr('ox') >> cnrInSubPma.last1D()
		cnrInUpMultMdv.attr('ox') >> cnrInSubPma.last1D()
		cnrInDnMultMdv.attr('ox') >> cnrInSubPma.last1D()

		cnrInSubPma.attr('o1') >> bsn.attr(bshNms[1])

		# Subtractor - Corner Out
		cnrOutSubPma = lrc.PlusMinusAverage()
		cnrOutSubPma.name = 'CnrOutSub%s%sPma' % (part, side)
		cnrOutSubPma.attr('op').v = 2

		cnrOutClamp.attr('opr') >> cnrOutSubPma.last1D()
		cnrOutUpMultMdv.attr('ox') >> cnrOutSubPma.last1D()
		cnrOutDnMultMdv.attr('ox') >> cnrOutSubPma.last1D()

		cnrOutSubPma.attr('o1') >> bsn.attr(bshNms[0])

		# Corrective blend shapes
		jnt = lrc.Dag('%sJaw%s_Jnt' % (ctrlNs, part))

		# Corrective blend shapes - Clamp values - Jaw Open
		openClamp = lrc.Clamp()
		openClamp.name = 'Open%s%sClamp' % (part, side)
		openClamp.attr('maxR').v = 1000
		jnt.attr('rx') >> openClamp.attr('ipr')

		# Corrective blend shapes - Normalization values - Jaw Open
		openNormMdv = lrc.MultiplyDivide()
		openNormMdv.name = 'OpenNorm%s%sMdv' % (part, side)
		openNormMdv.attr('op').v = 2
		jawCtrlShp.attr('jawOpen') >> openNormMdv.attr('i2x')
		openClamp.attr('opr') >> openNormMdv.attr('i1x')

		# Corner corrective blend shapes
		# Corner Open
		(cnrUpOpenMultMdv,
		cnrUpOpenSubPma,
		cnrDnOpenMultMdv,
		cnrDnOpenSubPma,
		cnrOpenSubPma) = threeWaysBshConnect(
												mainNm='CnrOpen',
												fstNm='CnrUpOpen',
												scdNm='CnrDnOpen',
												mainDvr=openNormMdv.attr('ox'),
												fstDvr=cnrUpClamp.attr('opr'),
												scdDvr=cnrDnInvMdv.attr('ox'),
												mainBsh=bsn.attr(bshNms[9]),
												fstBsh=bsn.attr(bshNms[10]),
												scdBsh=bsn.attr(bshNms[8]),
												part=part,
												side=side
											)
		if not lite:
			cnrOpenSealMdv = insertMultiplier(
												str(cnrOpenSubPma.attr('o1')),
												str(bsn.attr(bshNms[9])),
												str(slRev.attr('ox')),
												'CnrOpenSeal',
												part,
												side
											)
			cnrUpOpenSealMdv = insertMultiplier(
													str(cnrUpOpenSubPma.attr('o1')),
													str(bsn.attr(bshNms[10])),
													str(slRev.attr('ox')),
													'CnrUpOpenSeal',
													part,
													side
												)
			cnrDnOpenSealMdv = insertMultiplier(
													str(cnrDnOpenSubPma.attr('o1')),
													str(bsn.attr(bshNms[8])),
													str(slRev.attr('ox')),
													'CnrDnOpenSeal',
													part,
													side
												)

		# Corner In Open
		openInMultMdv = lrc.MultiplyDivide()
		openInMultMdv.name = 'OpenInMult%s%sMdv' % (part, side)
		openNormMdv.attr('ox') >> openInMultMdv.attr('i1x')
		cnrInInvMdv.attr('ox') >> openInMultMdv.attr('i2x')

		(cnrInUpOpenMultMdv,
		cnrInUpOpenSubPma,
		cnrInDnOpenMultMdv,
		cnrInDnOpenSubPma,
		cnrInSubPma) = threeWaysBshConnect(
												mainNm='CnrInOpen',
												fstNm='CnrInUpOpen',
												scdNm='CnrInDnOpen',
												mainDvr=openInMultMdv.attr('ox'),
												fstDvr=cnrInUpMultMdv.attr('ox'),
												scdDvr=cnrInDnMultMdv.attr('ox'),
												mainBsh=bsn.attr(bshNms[12]),
												fstBsh=bsn.attr(bshNms[13]),
												scdBsh=bsn.attr(bshNms[11]),
												part=part,
												side=side
											)
		cnrInDnOpenMultMdv.attr('ox') >> cnrDnOpenSubPma.last1D()
		cnrInUpOpenMultMdv.attr('ox') >> cnrUpOpenSubPma.last1D()

		if not lite:
			cnrInOpenSealMdv = insertMultiplier(
													str(cnrInSubPma.attr('o1')),
													str(bsn.attr(bshNms[12])),
													str(slRev.attr('ox')),
													'CnrInOpenSeal',
													part,
													side
												)
			cnrInUpOpenSealMdv = insertMultiplier(
													str(cnrInUpOpenSubPma.attr('o1')),
													str(bsn.attr(bshNms[13])),
													str(slRev.attr('ox')),
													'CnrInUpOpenSeal',
													part,
													side
												)
			cnrInDnOpenSealMdv = insertMultiplier(
													str(cnrInDnOpenSubPma.attr('o1')),
													str(bsn.attr(bshNms[11])),
													str(slRev.attr('ox')),
													'CnrInDnOpenSeal',
													part,
													side
												)

		# Corner Out Open
		openOutMultMdv = lrc.MultiplyDivide()
		openOutMultMdv.name = 'OpenInMult%s%sMdv' % (part, side)
		openNormMdv.attr('ox') >> openOutMultMdv.attr('i1x')
		cnrOutClamp.attr('opr') >> openOutMultMdv.attr('i2x')
		
		(cnrOutUpOpenMultMdv,
		cnrOutUpOpenSubPma,
		cnrOutDnOpenMultMdv,
		cnrOutDnOpenSubPma,
		cnrOutSubPma) = threeWaysBshConnect(
												mainNm='CnrOutOpen',
												fstNm='CnrOutUpOpen',
												scdNm='CnrOutDnOpen',
												mainDvr=openOutMultMdv.attr('ox'),
												fstDvr=cnrOutUpMultMdv.attr('ox'),
												scdDvr=cnrOutDnMultMdv.attr('ox'),
												mainBsh=bsn.attr(bshNms[15]),
												fstBsh=bsn.attr(bshNms[16]),
												scdBsh=bsn.attr(bshNms[14]),
												part=part,
												side=side
											)
		cnrOutDnOpenMultMdv.attr('ox') >> cnrDnOpenSubPma.last1D()
		cnrOutUpOpenMultMdv.attr('ox') >> cnrUpOpenSubPma.last1D()

		if not lite:
			cnrOutOpenSealMdv = insertMultiplier(
													str(cnrOutSubPma.attr('o1')),
													str(bsn.attr(bshNms[15])),
													str(slRev.attr('ox')),
													'CnrOutOpenSeal',
													part,
													side
												)
			cnrOutUpOpenSealMdv = insertMultiplier(
													str(cnrOutUpOpenSubPma.attr('o1')),
													str(bsn.attr(bshNms[16])),
													str(slRev.attr('ox')),
													'CnrOutUpOpenSeal',
													part,
													side
												)
			cnrOutDnOpenSealMdv = insertMultiplier(
													str(cnrOutDnOpenSubPma.attr('o1')),
													str(bsn.attr(bshNms[14])),
													str(slRev.attr('ox')),
													'CnrOutDnOpenSeal',
													part,
													side
												)

		# Puff
		puffCollPma = lrc.PlusMinusAverage()
		puffCollPma.name = 'PuffColl%s%sPma' % (part, side)
		ctrl.attr('puff') >> puffCollPma.last1D()
		puffCollPma.attr('o1') >> bsn.attr(bshNms[18])

		mul = lrr.attrAmper(
								ctrlAttr=ctrl.attr('tx'),
								targetAttr=puffCollPma.last1D(),
								dv=0.2,
								ampAttr='cnrOutPuffAmp'
							)
		mul.name = 'CnrOutPuffAmp%s%sMul' % (part, side)
		
		# Cheek
		cheekCollPma = lrc.PlusMinusAverage()
		cheekCollPma.name = 'CheekColl%s%sPma' % (part, side)
		ctrl.attr('cheek') >> cheekCollPma.last1D()
		cheekCollPma.attr('o1') >> bsn.attr(bshNms[17])
		
		mul = lrr.attrAmper(
								ctrlAttr=ctrl.attr('ty'),
								targetAttr=cheekCollPma.last1D(),
								dv=0.2,
								ampAttr='cnrUpCheekAmp'
							)
		mul.name = 'CnrUpCheekAmp%s%sMul' % (part, side)

		if lite:
			ctrl.add(ln='seal', dv=0, k=True)
			infoDict = {
							0: {'seal': (bshNms[19], bshNms[20])}
						}
			bshTools.ctrlToBsh(ctrl.name, bsn, infoDict)

	# Jaw Corrective blend shapes
	jnt = lrc.Dag('%sJaw%s_Jnt' % (ctrlNs, part))

	# Corrective blend shapes - Clamp values - Jaw Open
	openClamp = lrc.Clamp()
	openClamp.name = 'Open%s_Clamp' % part
	openClamp.attr('maxR').v = 1000
	jnt.attr('rx') >> openClamp.attr('ipr')

	# Corrective blend shapes - Normalization values - Jaw Open
	openNormMdv = lrc.MultiplyDivide()
	openNormMdv.name = 'OpenNorm%s_Mdv' % part
	openNormMdv.attr('op').v = 2
	jawCtrlShp.attr('jawOpen') >> openNormMdv.attr('i2x')
	openClamp.attr('opr') >> openNormMdv.attr('i1x')

	openNormMdv.attr('ox') >> bsn.attr(mthBshNms[9])

def insertMultiplier(
						outNodeAttr='',
						tarNodeAttr='',
						multNodeAttr='',
						mulPart='',
						part='',
						side=''
					):
	'''
	Insert multiplyDivide node as a multiplier between outNodeAttr and its target.
	'''
	mul = lrc.MultiplyDivide()
	mul.name = '%s%s%sMdv' % (mulPart, part, side)

	mc.connectAttr(outNodeAttr, str(mul.attr('i1x')))
	mc.connectAttr(multNodeAttr, str(mul.attr('i2x')))
	mc.connectAttr(str(mul.attr('ox')), tarNodeAttr, f=True)
	
	return mul

def threeWaysBshConnect(
							mainNm='CnrOpen',
							fstNm='CnrUpOpen',
							scdNm='CnrDnOpen',
							mainDvr='OpenNormMthRig_L_Mdv.ox',
							fstDvr='CnrUpMthRig_L_Clamp.opr',
							scdDvr='CnrDnInvMthRig_L_Mult.ox',
							mainBsh='MthRigBuf_Bsn.CnrOpenMthRig_L_Bsh',
							fstBsh='MthRigBuf_Bsn.CnrUpOpenMthRig_L_Bsh',
							scdBsh='MthRigBuf_Bsn.CnrDnOpenMthRig_L_Bsh',
							part='MthRig',
							side='_L_'
						):
		'''
		Create the connection to three corrective blend shapes.
		'''

		# First
		fstMult = lrc.MultiplyDivide()
		fstMult.name = '%sMult%s%sMdv' % (fstNm, part, side)

		mc.connectAttr(mainDvr, fstMult.attr('i1x'))
		mc.connectAttr(fstDvr, fstMult.attr('i2x'))
		
		fstPma = lrc.PlusMinusAverage()
		fstPma.name = '%sSub%s%sPma' % (fstNm, part, side)
		fstPma.attr('op').v = 2

		mc.connectAttr(fstMult.attr('ox'), fstPma.last1D())
		mc.connectAttr(fstPma.attr('o1'), fstBsh)

		# Second
		scdMult = lrc.MultiplyDivide()
		scdMult.name = '%sMult%s%sMdv' % (scdNm, part, side)

		mc.connectAttr(mainDvr, scdMult.attr('i1x'))
		mc.connectAttr(scdDvr, scdMult.attr('i2x'))

		scdPma = lrc.PlusMinusAverage()
		scdPma.name = '%sSub%s%sPma' % (scdNm, part, side)
		scdPma.attr('op').v = 2

		mc.connectAttr(scdMult.attr('ox'), scdPma.last1D())
		mc.connectAttr(scdPma.attr('o1'), scdBsh)

		# Main
		mainPma = lrc.PlusMinusAverage()
		mainPma.name = '%sSub%s%sPma' % (mainNm, part, side)
		mainPma.attr('op').v = 2

		mc.connectAttr(mainDvr, mainPma.last1D())
		mc.connectAttr(fstPma.attr('o1'), mainPma.last1D())
		mc.connectAttr(scdPma.attr('o1'), mainPma.last1D())
		mc.connectAttr(mainPma.attr('o1'), mainBsh)
		
		return fstMult, fstPma, scdMult, scdPma, mainPma

class LipDetailRig(object):

	def __init__(
					self,
					cntTmpLoc='',
					upMidTmpLoc='',
					lowMidTmpLoc='',
					lftCnrTmpLoc='',
					rgtCnrTmpLoc='',
					upALftTmpLoc='',
					upBLftTmpLoc='',
					lowALftTmpLoc='',
					lowBLftTmpLoc='',
					upARgtTmpLoc='',
					upBRgtTmpLoc='',
					lowARgtTmpLoc='',
					lowBRgtTmpLoc='',
					rbnTmpNrb='',
					part='',
					mthRigObj=None
				):

		cntLoc = lrc.Dag(cntTmpLoc)
		upMidLoc = lrc.Dag(upMidTmpLoc)
		lowMidLoc = lrc.Dag(lowMidTmpLoc)
		lftCnrLoc = lrc.Dag(lftCnrTmpLoc)
		rgtCnrLoc = lrc.Dag(rgtCnrTmpLoc)
		upALftLoc = lrc.Dag(upALftTmpLoc)
		upBLftLoc = lrc.Dag(upBLftTmpLoc)
		lowALftLoc = lrc.Dag(lowALftTmpLoc)
		lowBLftLoc = lrc.Dag(lowBLftTmpLoc)
		upARgtLoc = lrc.Dag(upARgtTmpLoc)
		upBRgtLoc = lrc.Dag(upBRgtTmpLoc)
		lowARgtLoc = lrc.Dag(lowARgtTmpLoc)
		lowBRgtLoc = lrc.Dag(lowBRgtTmpLoc)
		rbnNrb = lrc.Dag(rbnTmpNrb)

		# Main Groups
		self.Ctrl_Grp = lrc.Null()
		self.Ctrl_Grp.name = '%sCtrl_Grp' % part

		self.Jnt_Grp = lrc.Null()
		self.Jnt_Grp.name = '%sJnt_Grp' % part

		self.Skin_Grp = lrc.Null()
		self.Skin_Grp.name = '%sSkin_Grp' % part

		self.Still_Grp = lrc.Null()
		self.Still_Grp.name = '%sStill_Grp' % part

		# Main Joints
		# Main Jnts - Mth Ori
		self.MthJntOri_Grp = lrc.Null()
		self.MthJntOri_Grp.name = '%sMthJntOri_Grp' % part

		self.MthJntOriZr_Grp = lrc.group(self.MthJntOri_Grp)
		self.MthJntOriZr_Grp.name = '%sMthJntOriZr_Grp' % part
		self.MthJntOriZr_Grp.parent(self.Jnt_Grp)

		self.MthJntOriZr_Grp.snap(mthRigObj.Mth_Ctrl)
		mthRigObj.Mth_Ctrl.attr('t') >> self.MthJntOri_Grp.attr('t')
		mthRigObj.Mth_Ctrl.attr('r') >> self.MthJntOri_Grp.attr('r')
		mthRigObj.Mth_Ctrl.attr('s') >> self.MthJntOri_Grp.attr('s')

		# Upper Lip Part
		self.UpPart_Grp = lrc.Null()
		self.UpPart_Grp.name = 'UpPart%s_Grp' % part

		self.UpPartZr_Grp = lrc.group(self.UpPart_Grp)
		self.UpPartZr_Grp.name = 'UpPartZr%s_Grp' % part
		self.UpPartZr_Grp.snap(upMidTmpLoc)
		self.UpPartZr_Grp.parent(self.MthJntOri_Grp)

		mthRigObj.LipUp_Ctrl.attr('t') >> self.UpPart_Grp.attr('t')
		mthRigObj.LipUp_Ctrl.attr('r') >> self.UpPart_Grp.attr('r')
		mthRigObj.LipUp_Ctrl.attr('s') >> self.UpPart_Grp.attr('s')

		# Main Jnts - Low Ori
		self.LowJntOri_Grp = lrc.Null()
		self.LowJntOri_Grp.name = '%sLowJntOri_Grp' % part

		self.LowJntOriZr_Grp = lrc.group(self.LowJntOri_Grp)
		self.LowJntOriZr_Grp.name = '%sLowJntOriZr_Grp' % part
		self.LowJntOriZr_Grp.parent(self.MthJntOri_Grp)

		self.LowJntOriZr_Grp.snap(mthRigObj.Jaw_Jnt)

		# Lower Lip Part
		self.LowPart_Grp = lrc.Null()
		self.LowPart_Grp.name = 'LowPart%s_Grp' % part

		self.LowPartZr_Grp = lrc.group(self.LowPart_Grp)
		self.LowPartZr_Grp.name = 'LowPartZr%s_Grp' % part
		self.LowPartZr_Grp.snap(lowMidTmpLoc)
		self.LowPartZr_Grp.parent(self.LowJntOri_Grp)

		mthRigObj.LipLow_Ctrl.attr('t') >> self.LowPart_Grp.attr('t')
		mthRigObj.LipLow_Ctrl.attr('r') >> self.LowPart_Grp.attr('r')
		mthRigObj.LipLow_Ctrl.attr('s') >> self.LowPart_Grp.attr('s')

		# Main Jnts - Up
		self.UpMid_Jnt, self.UpMidJntZr_Grp = self.createMainJoint(upMidLoc, 'UpMid', part, '_')
		self.UpMidJntZr_Grp.parent(self.UpPart_Grp)

		self.Cnr_L_Jnt, self.CnrJntZr_L_Grp = self.createMainJoint(lftCnrLoc, 'Cnr', part, '_L_')
		self.CnrJntZr_L_Grp.parent(self.MthJntOri_Grp)

		self.Cnr_R_Jnt, self.CnrJntZr_R_Grp = self.createMainJoint(rgtCnrLoc, 'Cnr', part, '_R_')
		self.CnrJntZr_R_Grp.parent(self.MthJntOri_Grp)

		self.UpA_L_Jnt, self.UpAJntZr_L_Grp = self.createMainJoint(upALftLoc, 'UpA', part, '_L_')
		self.UpAJntZr_L_Grp.parent(self.UpPart_Grp)

		self.UpB_L_Jnt, self.UpBJntZr_L_Grp = self.createMainJoint(upBLftLoc, 'UpB', part, '_L_')
		self.UpBJntZr_L_Grp.parent(self.UpPart_Grp)

		self.UpA_R_Jnt, self.UpAJntZr_R_Grp = self.createMainJoint(upARgtLoc, 'UpA', part, '_R_')
		self.UpAJntZr_R_Grp.parent(self.UpPart_Grp)

		self.UpB_R_Jnt, self.UpBJntZr_R_Grp = self.createMainJoint(upBRgtLoc, 'UpB', part, '_R_')
		self.UpBJntZr_R_Grp.parent(self.UpPart_Grp)

		# Main Jnts - Low
		self.LowMid_Jnt, self.LowMidJntZr_Grp = self.createMainJoint(lowMidLoc, 'LowMid', part, '_')
		self.LowMidJntZr_Grp.parent(self.LowPart_Grp)

		self.LowA_L_Jnt, self.LowAJntZr_L_Grp = self.createMainJoint(lowALftLoc, 'LowA', part, '_L_')
		self.LowAJntZr_L_Grp.parent(self.LowPart_Grp)

		self.LowB_L_Jnt, self.LowBJntZr_L_Grp = self.createMainJoint(lowBLftLoc, 'LowB', part, '_L_')
		self.LowBJntZr_L_Grp.parent(self.LowPart_Grp)

		self.LowA_R_Jnt, self.LowAJntZr_R_Grp = self.createMainJoint(lowARgtLoc, 'LowA', part, '_R_')
		self.LowAJntZr_R_Grp.parent(self.LowPart_Grp)

		self.LowB_R_Jnt, self.LowBJntZr_R_Grp = self.createMainJoint(lowBRgtLoc, 'LowB', part, '_R_')
		self.LowBJntZr_R_Grp.parent(self.LowPart_Grp)

		# Main Controllers
		# Main Controllers - Mth Ori
		self.MthCtrlOri_Grp = lrc.Null()
		self.MthCtrlOri_Grp.name = '%sMthCtrlOri_Grp' % part

		self.MthCtrlOriZr_Grp = lrc.group(self.MthCtrlOri_Grp)
		self.MthCtrlOriZr_Grp.name = '%sMthCtrlOriZr_Grp' % part
		self.MthCtrlOriZr_Grp.parent(self.Ctrl_Grp)

		self.MthCtrlOriZr_Grp.snap(mthRigObj.Mth_Ctrl)
		mthRigObj.Mth_Ctrl.attr('t') >> self.MthCtrlOri_Grp.attr('t')
		mthRigObj.Mth_Ctrl.attr('r') >> self.MthCtrlOri_Grp.attr('r')
		mthRigObj.Mth_Ctrl.attr('s') >> self.MthCtrlOri_Grp.attr('s')

		# Main Controllers - Low Ori
		self.LowCtrlOri_Grp = lrc.Null()
		self.LowCtrlOri_Grp.name = '%sLowCtrlOri_Grp' % part

		self.LowCtrlOriZr_Grp = lrc.group(self.LowCtrlOri_Grp)
		self.LowCtrlOriZr_Grp.name = '%sLowCtrlOriZr_Grp' % part
		self.LowCtrlOriZr_Grp.parent(self.MthCtrlOri_Grp)

		self.LowCtrlOriZr_Grp.snap(mthRigObj.Jaw_Ctrl)
		mthRigObj.Jaw_Ctrl.attr('r') >> self.LowCtrlOri_Grp.attr('r')

		(self.UpMid_Ctrl,
		self.UpMidCtrlInv_Grp,
		self.UpMidCtrlZr_Grp) = self.createMainControl(self.UpMid_Jnt, 'UpMid', part, '_', ['r', 's'])
		self.UpMidCtrlZr_Grp.parent(self.MthCtrlOri_Grp)
		self.UpMid_Ctrl.color = 'yellow'

		(self.LowMid_Ctrl,
		self.LowMidCtrlInv_Grp,
		self.LowMidCtrlZr_Grp) = self.createMainControl(self.LowMid_Jnt, 'LowMid', part, '_', ['r', 's'])
		self.LowMidCtrlZr_Grp.parent(self.LowCtrlOri_Grp)
		self.LowMid_Ctrl.color = 'softYellow'

		(self.Cnr_L_Ctrl,
		self.CnrCtrlInv_L_Grp,
		self.CnrCtrlZr_L_Grp) = self.createMainControl(self.Cnr_L_Jnt, 'Cnr', part, '_L_', ['r', 's'])
		self.CnrCtrlZr_L_Grp.parent(self.MthCtrlOri_Grp)
		self.Cnr_L_Ctrl.color = 'red'

		(self.Cnr_R_Ctrl,
		self.CnrCtrlInv_R_Grp,
		self.CnrCtrlZr_R_Grp) = self.createMainControl(self.Cnr_R_Jnt, 'Cnr', part, '_R_', ['r', 's'])
		self.CnrCtrlZr_R_Grp.parent(self.MthCtrlOri_Grp)
		self.Cnr_R_Ctrl.color = 'blue'

		(self.UpA_L_Ctrl,
		self.UpACtrlInv_L_Grp,
		self.UpACtrlZr_L_Grp) = self.createMainControl(self.UpA_L_Jnt, 'UpA', part, '_L_', ['r', 's'])
		self.UpACtrlZr_L_Grp.parent(self.MthCtrlOri_Grp)
		self.UpA_L_Ctrl.color = 'softRed'

		(self.UpB_L_Ctrl,
		self.UpBCtrlInv_L_Grp,
		self.UpBCtrlZr_L_Grp) = self.createMainControl(self.UpB_L_Jnt, 'UpB', part, '_L_', ['r', 's'])
		self.UpBCtrlZr_L_Grp.parent(self.MthCtrlOri_Grp)
		self.UpB_L_Ctrl.color = 'softRed'

		(self.UpA_R_Ctrl,
		self.UpACtrlInv_R_Grp,
		self.UpACtrlZr_R_Grp) = self.createMainControl(self.UpA_R_Jnt, 'UpA', part, '_R_', ['r', 's'])
		self.UpACtrlZr_R_Grp.parent(self.MthCtrlOri_Grp)
		self.UpA_R_Ctrl.color = 'softBlue'

		(self.UpB_R_Ctrl,
		self.UpBCtrlInv_R_Grp,
		self.UpBCtrlZr_R_Grp) = self.createMainControl(self.UpB_R_Jnt, 'UpB', part, '_R_', ['r', 's'])
		self.UpBCtrlZr_R_Grp.parent(self.MthCtrlOri_Grp)
		self.UpB_R_Ctrl.color = 'softBlue'

		(self.LowA_L_Ctrl,
		self.LowACtrlInv_L_Grp,
		self.LowACtrlZr_L_Grp) = self.createMainControl(self.LowA_L_Jnt, 'LowA', part, '_L_', ['r', 's'])
		self.LowACtrlZr_L_Grp.parent(self.LowCtrlOri_Grp)
		self.LowA_L_Ctrl.color = 'darkRed'

		(self.LowB_L_Ctrl,
		self.LowBCtrlInv_L_Grp,
		self.LowBCtrlZr_L_Grp) = self.createMainControl(self.LowB_L_Jnt, 'LowB', part, '_L_', ['r', 's'])
		self.LowBCtrlZr_L_Grp.parent(self.LowCtrlOri_Grp)
		self.LowB_L_Ctrl.color = 'darkRed'

		(self.LowA_R_Ctrl,
		self.LowACtrlInv_R_Grp,
		self.LowACtrlZr_R_Grp) = self.createMainControl(self.LowA_R_Jnt, 'LowA', part, '_R_', ['r', 's'])
		self.LowACtrlZr_R_Grp.parent(self.LowCtrlOri_Grp)
		self.LowA_R_Ctrl.color = 'darkBlue'

		(self.LowB_R_Ctrl,
		self.LowBCtrlInv_R_Grp,
		self.LowBCtrlZr_R_Grp) = self.createMainControl(self.LowB_R_Jnt, 'LowB', part, '_R_', ['r', 's'])
		self.LowBCtrlZr_R_Grp.parent(self.LowCtrlOri_Grp)
		self.LowB_R_Ctrl.color = 'darkBlue'

		# Ribbon Surface
		self.Rbn_Nrb = lrc.Dag(mc.rebuildSurface(rbnNrb, ch=False, sv=1, dv=2, dir=1)[0])
		self.Rbn_Nrb.name = '%sRbn_Nrb' % part
		self.Rbn_Nrb.parent(self.Still_Grp)

		rbnShp = lrc.Dag(self.Rbn_Nrb.shape)
		midV = float(rbnShp.attr('maxValueV').v/2.000)

		# Skin Joints
		self.Hd_Jnt, self.HdJntZr_Grp = self.createMainJoint(cntLoc, 'Hd', part, '_')
		self.HdJntZr_Grp.parent(self.Skin_Grp)

		# Skin Joints- Mth Ori
		self.MthSkinJntOri_Grp = lrc.Null()
		self.MthSkinJntOri_Grp.name = '%sMthSkinJntOri_Grp' % part

		self.MthSkinJntOriZr_Grp = lrc.group(self.MthSkinJntOri_Grp)
		self.MthSkinJntOriZr_Grp.name = '%sMthSkinJntOriZr_Grp' % part
		self.MthSkinJntOriZr_Grp.parent(self.Skin_Grp)

		self.MthSkinJntOriZr_Grp.snap(mthRigObj.Mth_Ctrl)
		mthRigObj.Mth_Ctrl.attr('r') >> self.MthSkinJntOri_Grp.attr('r')
		mthRigObj.Mth_Ctrl.attr('s') >> self.MthSkinJntOri_Grp.attr('s')

		mthCtrlShp = lrc.Dag(mthRigObj.Mth_Ctrl.shape)
		mthCtrlShp.add(ln='detail', min=0, max=1, dv=0, k=True)

		self.skinJnts = []
		self.skinJntZrGrps = []
		self.poss = []
		self.posis = []

		no = 24
		for ix in range(no):

			pos = lrc.Null()
			posi = lrc.PointOnSurfaceInfo()

			pos.name = '%sRbnPos_%s_Grp' % (part, ix+1)
			posi.name = '%sRbn_%s_Posi' % (part, ix+1)
			
			self.Rbn_Nrb.attr('worldSpace[0]') >> posi.attr('is')
			posi.attr('position') >> pos.attr('t')
			posi.attr('parameterV').v = midV
			
			pos.parent(self.Still_Grp)

			skinJnt = lrc.Joint()
			skinJntOffGrp = lrc.group(skinJnt)
			skinJntZrGrp = lrc.group(skinJntOffGrp)

			skinJnt.name = '%sRbnSkin_%s_Jnt' % (part, ix+1)
			skinJntOffGrp.name = '%sRbnSkinJntOff_%s_Grp' % (part, ix+1)
			skinJntZrGrp.name = '%sRbnSkinJntZr_%s_Grp' % (part, ix+1)

			skinJntZrGrp.parent(self.MthSkinJntOri_Grp)
			
			mc.pointConstraint(pos, skinJntZrGrp)

			if not ix in (10, 22):
				aimCon = lrc.AimConstraint()
				aimCon.name = '%s_aimConstraint' % pos
				aimCon.attr('a').v = (0, 1, 0)
				aimCon.attr('u').v = (1, 0, 0)

				aimCon.parent(pos)
				aimCon.attr('t').v = (0, 0, 0)
				aimCon.attr('r').v = (0, 0, 0)
				
				posi.attr('n') >> aimCon.attr('wu')
				posi.attr('tu') >> aimCon.attr('tg[0].tt')
				aimCon.attr('cr') >> pos.attr('r')
				
				# mc.orientConstraint(pos, skinJntZrGrp)
				mc.delete(mc.orientConstraint(pos, skinJntZrGrp))
				mc.delete(aimCon)

			self.posis.append(posi)
			self.skinJnts.append(skinJnt)
			self.skinJntZrGrps.append(skinJntZrGrp)

			posi.attr('parameterU').v = float(ix/2.00)

			ctrl = lrc.Control('cube')
			ctrl.name = '%sDtl_%s_Ctrl' % (part, ix+1)
			ctrl.lockHideAttrs('v')
			ctrl.color = 'yellow'

			ctrlInvGrp = lrc.group(ctrl)
			ctrlInvGrp.name = '%sDtlCtrlInv_%s_Grp' % (part, ix+1)

			ctrlZrGrp = lrc.group(ctrlInvGrp)
			ctrlZrGrp.name = '%sDtlCtrlZr_%s_Grp' % (part, ix+1)

			ctrlZrGrp.snapPoint(pos)
			skinJntOffGrp.snapOrient(ctrl)

			mthCtrlShp.attr('detail') >> ctrlZrGrp.attr('v')
			ctrl.attr('t') >> skinJnt.attr('t')
			ctrl.attr('r') >> skinJnt.attr('r')
			ctrl.attr('s') >> skinJnt.attr('s')

			if 10 <= ix <= 22:
				ctrlZrGrp.parent(self.LowCtrlOri_Grp)
			else:
				ctrlZrGrp.parent(self.MthCtrlOri_Grp)

		# Binding skin
		ctrlJnts = [
						self.Cnr_L_Jnt,
						self.UpB_L_Jnt,
						self.UpA_L_Jnt,
						self.UpMid_Jnt,
						self.UpA_R_Jnt,
						self.UpB_R_Jnt,
						self.Cnr_R_Jnt,
						self.LowB_R_Jnt,
						self.LowA_R_Jnt,
						self.LowMid_Jnt,
						self.LowA_L_Jnt,
						self.LowB_L_Jnt
					]
		skc = mc.skinCluster(
								ctrlJnts,
								self.Rbn_Nrb,
								dr=7,
								mi=2
							)[0]

		for ix, ctrlJnt in enumerate(ctrlJnts):
			mc.skinPercent(
								skc,
								'%s.cv[%s][0:2]' % (self.Rbn_Nrb, ix),
								tv=[
										(ctrlJnt, 1)
									]
							)

	def createMainJoint(self, tmpObj, jntPart, part, side):

		jnt = lrc.Joint()
		zrGrp = lrc.group(jnt)

		zrGrp.snap(tmpObj)

		jnt.name = '%s%s%sJnt' % (jntPart, part, side)
		zrGrp.name = '%s%sJntZr%sGrp' % (jntPart, part, side)

		return jnt, zrGrp

	def createMainControl(self, jnt, ctrlPart, part, side, lockAttrs):

		ctrl = lrc.Control('nrbCircle')
		ctrl.scaleShape(0.25)
		invGrp = lrc.group(ctrl)
		zrGrp = lrc.group(invGrp)

		zrGrp.snap(jnt)

		ctrl.name = '%s%s%sCtrl' % (ctrlPart, part, side)
		invGrp.name = '%s%sCtrlInv%sGrp' % (ctrlPart, part, side)
		zrGrp.name = '%s%sCtrlZr%sGrp' % (ctrlPart, part, side)

		ctrl.attr('t') >> jnt.attr('t')
		ctrl.attr('r') >> jnt.attr('r')
		ctrl.attr('s') >> jnt.attr('s')

		for attr in lockAttrs:
			ctrl.lockHideAttrs(attr)

		ctrl.attr('v').l = True
		ctrl.attr('v').h = True

		return ctrl, invGrp, zrGrp

class LipPartRibbon(object):

	def __init__(
					self,
					tmpLocs=[],
					part='',
					offsetCurve=0.1
				):

		# Main Group
		self.Jnt_Grp = lrc.Null()
		self.Jnt_Grp.name = 'LipRbn%sJnt_Grp' % part

		self.Skin_Grp = lrc.Null()
		self.Skin_Grp.name = 'LipRbn%sSkin_Grp' % part

		self.Still_Grp = lrc.Null()
		self.Still_Grp.name = 'LipRbn%sStill_Grp' % part

		# Main Joint
		self.Mid_Jnt = lrr.jointAt(tmpLocs[0])
		self.Cnr_L_Jnt = lrr.jointAt(tmpLocs[1])
		self.Cnr_R_Jnt = lrr.jointAt(tmpLocs[2])
		self.A_L_Jnt = lrr.jointAt(tmpLocs[3])
		self.B_L_Jnt = lrr.jointAt(tmpLocs[4])
		self.A_R_Jnt = lrr.jointAt(tmpLocs[5])
		self.B_R_Jnt = lrr.jointAt(tmpLocs[6])

		self.Mid_Jnt.name = 'LipRbnMid%s_Jnt' % part
		self.Cnr_L_Jnt.name = 'LipRbnCnr%s_L_Jnt' % part
		self.Cnr_R_Jnt.name = 'LipRbnCnr%s_R_Jnt' % part
		self.A_L_Jnt.name = 'LipRbnA%s_L_Jnt' % part
		self.B_L_Jnt.name = 'LipRbnB%s_L_Jnt' % part
		self.A_R_Jnt.name = 'LipRbnA%s_R_Jnt' % part
		self.B_R_Jnt.name = 'LipRbnB%s_R_Jnt' % part

		self.MidJntOfst_Grp = lrr.groupAt(self.Mid_Jnt)
		self.CnrJntOfst_L_Grp = lrr.groupAt(self.Cnr_L_Jnt)
		self.CnrJntOfst_R_Grp = lrr.groupAt(self.Cnr_R_Jnt)
		self.AJntOfst_L_Grp = lrr.groupAt(self.A_L_Jnt)
		self.BJntOfst_L_Grp = lrr.groupAt(self.B_L_Jnt)
		self.AJntOfst_R_Grp = lrr.groupAt(self.A_R_Jnt)
		self.BJntOfst_R_Grp = lrr.groupAt(self.B_R_Jnt)

		self.MidJntOfst_Grp.name = 'LipRbnMidJntOfst%s_Grp' % part
		self.CnrJntOfst_L_Grp.name = 'LipRbnCnrJntOfst%s_L_Grp' % part
		self.CnrJntOfst_R_Grp.name = 'LipRbnCnrJntOfst%s_R_Grp' % part
		self.AJntOfst_L_Grp.name = 'LipRbnAJntOfst%s_L_Grp' % part
		self.BJntOfst_L_Grp.name = 'LipRbnBJntOfst%s_L_Grp' % part
		self.AJntOfst_R_Grp.name = 'LipRbnAJntOfst%s_R_Grp' % part
		self.BJntOfst_R_Grp.name = 'LipRbnBJntOfst%s_R_Grp' % part

		self.MidJntZr_Grp = lrr.groupAt(self.MidJntOfst_Grp)
		self.CnrJntZr_L_Grp = lrr.groupAt(self.CnrJntOfst_L_Grp)
		self.CnrJntZr_R_Grp = lrr.groupAt(self.CnrJntOfst_R_Grp)
		self.AJntZr_L_Grp = lrr.groupAt(self.AJntOfst_L_Grp)
		self.BJntZr_L_Grp = lrr.groupAt(self.BJntOfst_L_Grp)
		self.AJntZr_R_Grp = lrr.groupAt(self.AJntOfst_R_Grp)
		self.BJntZr_R_Grp = lrr.groupAt(self.BJntOfst_R_Grp)
		
		self.MidJntZr_Grp.name = 'LipRbnMidJntZr%s_Grp' % part
		self.CnrJntZr_L_Grp.name = 'LipRbnCnrJntZr%s_L_Grp' % part
		self.CnrJntZr_R_Grp.name = 'LipRbnCnrJntZr%s_R_Grp' % part
		self.AJntZr_L_Grp.name = 'LipRbnAJntZr%s_L_Grp' % part
		self.BJntZr_L_Grp.name = 'LipRbnBJntZr%s_L_Grp' % part
		self.AJntZr_R_Grp.name = 'LipRbnAJntZr%s_R_Grp' % part
		self.BJntZr_R_Grp.name = 'LipRbnBJntZr%s_R_Grp' % part

		self.MidJntZr_Grp.parent(self.Jnt_Grp)
		self.CnrJntZr_L_Grp.parent(self.Jnt_Grp)
		self.CnrJntZr_R_Grp.parent(self.Jnt_Grp)
		self.AJntZr_L_Grp.parent(self.Jnt_Grp)
		self.BJntZr_L_Grp.parent(self.Jnt_Grp)
		self.AJntZr_R_Grp.parent(self.Jnt_Grp)
		self.BJntZr_R_Grp.parent(self.Jnt_Grp)

		# Ribbon Surface
		crvPars = []
		for ix in [1, 4, 3, 0, 5, 6, 2]:
			crvPars.append(mc.xform(tmpLocs[ix], q=True, t=True, ws=True))

		tmpCrv = mc.curve(d=1, p=crvPars)
		mc.rebuildCurve(tmpCrv, ch=False, s=7, d=3)

		for ix, iy in zip((9, 8, 7, 6, 5), (0, 1, 2, 3, 4)):
			currPost = mc.xform('%s.cv[%s]' % (tmpCrv, str(iy)), q=True, ws=True, t=True)
			currPost = [-currPost[0], currPost[1], currPost[2]]
			mc.xform('%s.cv[%s]' % (tmpCrv, str(ix)), t=currPost, ws=True)

		crvIn = mc.offsetCurve(tmpCrv, ch=False, d=-offsetCurve)[0]
		crvOut = mc.offsetCurve(tmpCrv, ch=False, d=offsetCurve)[0]

		self.Rbn_Nrb = lrc.Dag(mc.loft(crvIn, crvOut, ch=False, d=3)[0])
		self.Rbn_Nrb.name = 'LipRbn%s_Nrb' % part
		self.Rbn_Nrb.parent(self.Still_Grp)

		mc.delete(tmpCrv, crvIn, crvOut)

		# Binding skin
		skc = mc.skinCluster(
								self.Mid_Jnt.name,
								self.Cnr_L_Jnt.name,
								self.Cnr_R_Jnt.name,
								self.A_L_Jnt.name,
								self.B_L_Jnt.name,
								self.A_R_Jnt.name,
								self.B_R_Jnt.name,
								self.Rbn_Nrb,
								dr=7,
								mi=2
							)[0]

		mc.skinPercent(
							skc,
							'%s.cv[0:3][0]' % self.Rbn_Nrb,
							tv=[
									(self.Cnr_L_Jnt, 1)
								]
						)
		mc.skinPercent(
							skc,
							'%s.cv[0:3][1]' % self.Rbn_Nrb,
							tv=[
									(self.Cnr_L_Jnt, 0.6),
									(self.B_L_Jnt, 0.4),
									(self.A_L_Jnt, 0),
									(self.Mid_Jnt, 0),
									(self.A_R_Jnt, 0),
									(self.B_R_Jnt, 0),
									(self.Cnr_R_Jnt, 0),
								]
						)
		mc.skinPercent(
							skc,
							'%s.cv[0:3][2]' % self.Rbn_Nrb,
							tv=[
									(self.Cnr_L_Jnt, 0),
									(self.B_L_Jnt, 0.8),
									(self.A_L_Jnt, 0.2),
									(self.Mid_Jnt, 0),
									(self.A_R_Jnt, 0),
									(self.B_R_Jnt, 0),
									(self.Cnr_R_Jnt, 0),
								]
						)
		mc.skinPercent(
							skc,
							'%s.cv[0:3][3]' % self.Rbn_Nrb,
							tv=[
									(self.Cnr_L_Jnt, 0),
									(self.B_L_Jnt, 0.2),
									(self.A_L_Jnt, 0.8),
									(self.Mid_Jnt, 0),
									(self.A_R_Jnt, 0),
									(self.B_R_Jnt, 0),
									(self.Cnr_R_Jnt, 0),
								]
						)
		mc.skinPercent(
							skc,
							'%s.cv[0:3][4]' % self.Rbn_Nrb,
							tv=[
									(self.Cnr_L_Jnt, 0),
									(self.B_L_Jnt, 0.0),
									(self.A_L_Jnt, 0.3),
									(self.Mid_Jnt, 0.7),
									(self.A_R_Jnt, 0),
									(self.B_R_Jnt, 0),
									(self.Cnr_R_Jnt, 0),
								]
						)
		mc.skinPercent(
							skc,
							'%s.cv[0:3][5]' % self.Rbn_Nrb,
							tv=[
									(self.Cnr_L_Jnt, 0),
									(self.B_L_Jnt, 0.0),
									(self.A_L_Jnt, 0),
									(self.Mid_Jnt, 0.7),
									(self.A_R_Jnt, 0.3),
									(self.B_R_Jnt, 0),
									(self.Cnr_R_Jnt, 0),
								]
						)
		mc.skinPercent(
							skc,
							'%s.cv[0:3][6]' % self.Rbn_Nrb,
							tv=[
									(self.Cnr_L_Jnt, 0),
									(self.B_L_Jnt, 0.0),
									(self.A_L_Jnt, 0),
									(self.Mid_Jnt, 0),
									(self.A_R_Jnt, 0.8),
									(self.B_R_Jnt, 0.2),
									(self.Cnr_R_Jnt, 0),
								]
						)
		mc.skinPercent(
							skc,
							'%s.cv[0:3][7]' % self.Rbn_Nrb,
							tv=[
									(self.Cnr_L_Jnt, 0),
									(self.B_L_Jnt, 0.0),
									(self.A_L_Jnt, 0),
									(self.Mid_Jnt, 0),
									(self.A_R_Jnt, 0.2),
									(self.B_R_Jnt, 0.8),
									(self.Cnr_R_Jnt, 0),
								]
						)
		mc.skinPercent(
							skc,
							'%s.cv[0:3][8]' % self.Rbn_Nrb,
							tv=[
									(self.Cnr_L_Jnt, 0),
									(self.B_L_Jnt, 0.0),
									(self.A_L_Jnt, 0),
									(self.Mid_Jnt, 0),
									(self.A_R_Jnt, 0),
									(self.B_R_Jnt, 0.4),
									(self.Cnr_R_Jnt, 0.6),
								]
						)

		# Skin Joints
		self.skinJnts = []
		self.skinJntZrGrps = []
		self.poss = []
		self.posis = []

		no = 9
		for ix in range(no):

			pos = lrc.Null()
			posi = lrc.PointOnSurfaceInfo()

			pos.name = 'LipRbn%sPos_%sGrp' % (part, ix+1)
			posi.name = 'LipRbn%s_%sPosi' % (part, ix+1)
			
			self.Rbn_Nrb.attr('worldSpace[0]') >> posi.attr('is')
			posi.attr('position') >> pos.attr('t')
			posi.attr('parameterU').v = 0.5
			posi.attr('turnOnPercentage').v = 1
			
			aimCon = lrc.AimConstraint()
			aimCon.name = '%s_aimConstraint' % pos
			aimCon.attr('a').v = (0, 1, 0)
			aimCon.attr('u').v = (1, 0, 0)
			
			posi.attr('n') >> aimCon.attr('wu')
			posi.attr('tv') >> aimCon.attr('tg[0].tt')
			aimCon.attr('cr') >> pos.attr('r')
			
			aimCon.parent(pos)
			aimCon.attr('t').v = (0, 0, 0)
			aimCon.attr('r').v = (0, 0, 0)
			pos.parent(self.Still_Grp)

			skinJnt = lrc.Joint()
			skinJntZrGrp = lrc.group(skinJnt)

			skinJnt.name = 'LipRbn%sSkin_%s_Jnt' % (part, ix+1)
			skinJntZrGrp.name = 'LipRbn%sSkinJntZr_%s_Grp' % (part, ix+1)

			skinJntZrGrp.parent(self.Skin_Grp)
			
			mc.parentConstraint(pos, skinJntZrGrp)

			self.posis.append(posi)
			self.skinJnts.append(skinJnt)
			self.skinJntZrGrps.append(skinJntZrGrp)
			
		vVals = [0.01, 0.08, 0.18, 0.32, 0.5, 0.68, 0.82, 0.92, 0.99]
		for ix, posi in enumerate(self.posis):
			posi.attr('parameterV').v = vVals[ix]

class MouthRigLite(object):

	def __init__(
					self,
					headTmpLoc='HdMthRigTmp_Loc',
					jawTmpLoc='JawMthRigTmp_Loc',
					cntMthTmpLoc='CntMthRigTmp_Loc',
					lftCnrTmpLoc='CnrMthRigTmp_L_Loc',
					rgtCnrTmpLoc='CnrMthRigTmp_R_Loc',
					part='MthRig'
				):

		# Tmp Locs
		headLoc = lrc.Dag(headTmpLoc)
		jawLoc = lrc.Dag(jawTmpLoc)

		# Main Group
		self.Ctrl_Grp = lrc.Null()
		self.Ctrl_Grp.name = '%sCtrl_Grp' % part

		self.Jnt_Grp = lrc.Null()
		self.Jnt_Grp.name = '%sJnt_Grp' % part

		self.Skin_Grp = lrc.Null()
		self.Skin_Grp.name = '%sSkin_Grp' % part

		self.Still_Grp = lrc.Null()
		self.Still_Grp.name = '%sStill_Grp' % part

		# Skin Joints - Head
		self.Hd_Jnt = lrc.Joint()
		self.Hd_Jnt.name = 'Hd%s_Jnt' % part

		self.HdJntZr_Grp = lrc.group(self.Hd_Jnt)
		self.HdJntZr_Grp.name = 'Hd%sJntZr_Grp' % part

		self.HdJntZr_Grp.snap(headLoc)
		self.HdJntZr_Grp.parent(self.Skin_Grp)

		# Skin Joints - Jaw
		self.Jaw_Jnt = lrc.Joint()
		self.Jaw_Jnt.name = 'Jaw%s_Jnt' % part

		self.JawJntZr_Grp = lrc.group(self.Jaw_Jnt)
		self.JawJntZr_Grp.name = 'Jaw%sJntZr_Grp' % part

		self.JawJntZr_Grp.snap(jawLoc)
		self.JawJntZr_Grp.parent(self.Hd_Jnt)

		# Jaw Control
		self.Jaw_Ctrl = lrc.Control('nrbCircle')
		self.Jaw_Ctrl.name = 'Jaw%s_Ctrl' % part
		self.Jaw_Ctrl.color = 'yellow'
		self.Jaw_Ctrl.lockHideAttrs('v')
		
		self.JawCtrlZr_Grp = lrc.group(self.Jaw_Ctrl)
		self.JawCtrlZr_Grp.name = 'Jaw%sCtrlZr_Grp' % part
		self.JawCtrlZr_Grp.snap(jawLoc)
		self.JawCtrlZr_Grp.parent(self.Ctrl_Grp)
		
		self.Jaw_Ctrl.attr('t') >> self.Jaw_Jnt.attr('t')
		self.Jaw_Ctrl.attr('ry') >> self.Jaw_Jnt.attr('ry')
		self.Jaw_Ctrl.attr('rz') >> self.Jaw_Jnt.attr('rz')
		self.Jaw_Ctrl.attr('s') >> self.Jaw_Jnt.attr('s')
		
		self.Jaw_Ctrl.attr('rx') >> self.Jaw_Jnt.attr('rx')

		# Blend Shape Controller - Mth
		self.Mth_Ctrl = lrc.Control('nrbCircle')
		self.Mth_Ctrl.name = 'Mth%s_Ctrl' % part
		self.Mth_Ctrl.color = 'yellow'
		self.Mth_Ctrl.lockHideAttrs('t', 'r', 's', 'v')

		self.MthCtrlZr_Grp = lrc.group(self.Mth_Ctrl)
		self.MthCtrlZr_Grp.name = 'Mth%sCtrlZr_Grp' % part
		self.MthCtrlZr_Grp.snap(cntMthTmpLoc)
		self.MthCtrlZr_Grp.parent(self.Ctrl_Grp)

		# Blend Shape Controller - Mth _L_
		self.Mth_L_Ctrl = lrc.Control('nrbCircle')
		self.Mth_L_Ctrl.name = 'Mth%s_L_Ctrl' % part
		self.Mth_L_Ctrl.color = 'red'
		self.Mth_L_Ctrl.lockHideAttrs('tz', 'r', 's', 'v')

		self.MthCtrlInv_L_Grp = lrc.group(self.Mth_L_Ctrl)
		self.MthCtrlInv_L_Grp.name = 'Mth%sCtrlInv_L_Grp' % part

		self.MthCtrlZr_L_Grp = lrc.group(self.MthCtrlInv_L_Grp)
		self.MthCtrlZr_L_Grp.name = 'Mth%sCtrlZr_L_Grp' % part
		self.MthCtrlZr_L_Grp.snap(lftCnrTmpLoc)
		self.MthCtrlZr_L_Grp.parent(self.Ctrl_Grp)

		# Blend Shape Controller - Mth _R_
		self.Mth_R_Ctrl = lrc.Control('nrbCircle')
		self.Mth_R_Ctrl.name = 'Mth%s_R_Ctrl' % part
		self.Mth_R_Ctrl.color = 'blue'
		self.Mth_R_Ctrl.lockHideAttrs('tz', 'r', 's', 'v')

		self.MthCtrlInv_R_Grp = lrc.group(self.Mth_R_Ctrl)
		self.MthCtrlInv_R_Grp.name = 'Mth%sCtrlInv_R_Grp' % part

		self.MthCtrlZr_R_Grp = lrc.group(self.MthCtrlInv_R_Grp)
		self.MthCtrlZr_R_Grp.name = 'Mth%sCtrlZr_R_Grp' % part
		self.MthCtrlZr_R_Grp.snap(rgtCnrTmpLoc)
		self.MthCtrlZr_R_Grp.parent(self.Ctrl_Grp)
		self.MthCtrlZr_R_Grp.attr('ry').v = 180

class MouthRig(object):
	'''
	Lip curves are 1 degree NURBs curves with 10 spans(11 cvs) each.
	Each cv represents the position of each mouth joint.
	The first cv for each curve is the left most position of each mouth.
	'''
	def __init__(
					self,
					headTmpLoc='HdMthRigTmp_Loc',
					jawTmpLoc='JawMthRigTmp_Loc',
					lipUpTmpLocs=[
									'LipUpMidMthRigTmp_Loc',
									'LipUpCnrMthRigTmp_L_Loc',
									'LipUpCnrMthRigTmp_R_Loc',
									'LipUpAMthRigTmp_L_Loc',
									'LipUpBMthRigTmp_L_Loc',
									'LipUpAMthRigTmp_R_Loc',
									'LipUpBMthRigTmp_R_Loc'
								],
					lipLowTmpLocs=[
									'LipLowMidMthRigTmp_Loc',
									'LipLowCnrMthRigTmp_L_Loc',
									'LipLowCnrMthRigTmp_R_Loc',
									'LipLowAMthRigTmp_L_Loc',
									'LipLowBMthRigTmp_L_Loc',
									'LipLowAMthRigTmp_R_Loc',
									'LipLowBMthRigTmp_R_Loc'
								],
					cntMthTmpLoc='CntMthRigTmp_Loc',
					lftCnrTmpLoc='CnrMthRigTmp_L_Loc',
					rgtCnrTmpLoc='CnrMthRigTmp_R_Loc',
					part='MthRig',
					offsetCurve=0.1
				):

		# Tmp Locs
		headLoc = lrc.Dag(headTmpLoc)
		jawLoc = lrc.Dag(jawTmpLoc)

		# Main Group
		self.Ctrl_Grp = lrc.Null()
		self.Ctrl_Grp.name = '%sCtrl_Grp' % part

		self.Jnt_Grp = lrc.Null()
		self.Jnt_Grp.name = '%sJnt_Grp' % part

		self.Skin_Grp = lrc.Null()
		self.Skin_Grp.name = '%sSkin_Grp' % part

		self.Still_Grp = lrc.Null()
		self.Still_Grp.name = '%sStill_Grp' % part

		# Skin Joints - Head
		self.Hd_Jnt = lrc.Joint()
		self.Hd_Jnt.name = 'Hd%s_Jnt' % part

		self.HdJntZr_Grp = lrc.group(self.Hd_Jnt)
		self.HdJntZr_Grp.name = 'Hd%sJntZr_Grp' % part

		self.HdJntZr_Grp.snap(headLoc)
		self.HdJntZr_Grp.parent(self.Skin_Grp)

		# Skin Joints - Jaw
		self.Jaw_Jnt = lrc.Joint()
		self.Jaw_Jnt.name = 'Jaw%s_Jnt' % part

		self.JawJntZr_Grp = lrc.group(self.Jaw_Jnt)
		self.JawJntZr_Grp.name = 'Jaw%sJntZr_Grp' % part

		self.JawJntZr_Grp.snap(jawLoc)
		self.JawJntZr_Grp.parent(self.Hd_Jnt)

		# Jaw Control
		self.Jaw_Ctrl = lrc.Control('nrbCircle')
		self.Jaw_Ctrl.name = 'Jaw%s_Ctrl' % part
		self.Jaw_Ctrl.color = 'yellow'
		self.Jaw_Ctrl.lockHideAttrs('v')

		self.JawCtrlZr_Grp = lrc.group(self.Jaw_Ctrl)
		self.JawCtrlZr_Grp.name = 'Jaw%sCtrlZr_Grp' % part
		self.JawCtrlZr_Grp.snap(jawLoc)
		self.JawCtrlZr_Grp.parent(self.Ctrl_Grp)

		self.Jaw_Ctrl.attr('tx') >> self.Jaw_Jnt.attr('tx')
		self.Jaw_Ctrl.attr('ry') >> self.Jaw_Jnt.attr('ry')
		self.Jaw_Ctrl.attr('rz') >> self.Jaw_Jnt.attr('rz')
		self.Jaw_Ctrl.attr('s') >> self.Jaw_Jnt.attr('s')

		self.Jaw_Ctrl.attr('rx') >> self.Jaw_Jnt.attr('rx')

		# Offset translation when jaw opens.
		jawCtrlShp = lrc.Dag(self.Jaw_Ctrl.shape)
		jawCtrlShp.add(ln='openTyAmp', k=True)
		jawCtrlShp.add(ln='openTzAmp', k=True)
		jawCtrlShp.add(ln='closeTyAmp', k=True)
		jawCtrlShp.add(ln='closeTzAmp', k=True)

		self.JawJntOpen_Clamp = lrc.Clamp()
		self.JawJntOpen_Clamp.name = 'Jaw%sJntTyOpen_Clamp' % part
		self.JawJntOpen_Clamp.attr('maxR').v = 360

		self.JawJntClose_Clamp = lrc.Clamp()
		self.JawJntClose_Clamp.name = 'Jaw%sJntTyClose_Clamp' % part
		self.JawJntClose_Clamp.attr('minR').v = -360

		self.Jaw_Ctrl.attr('rx') >> self.JawJntOpen_Clamp.attr('inputR')
		self.Jaw_Ctrl.attr('rx') >> self.JawJntClose_Clamp.attr('inputR')

		self.JawJntTyColl_Pma = lrc.PlusMinusAverage()
		self.JawJntTyColl_Pma.name = 'Jaw%sJntTyColl_Pma' % part

		self.JawJntTyColl_Pma.attr('output1D') >> self.Jaw_Jnt.attr('ty')

		self.JawJntTzColl_Pma = lrc.PlusMinusAverage()
		self.JawJntTzColl_Pma.name = 'Jaw%sJntTzColl_Pma' % part

		self.JawJntTzColl_Pma.attr('output1D') >> self.Jaw_Jnt.attr('tz')

		self.JawJntOpenTyAmp_Mdv = lrc.MultiplyDivide()
		self.JawJntOpenTyAmp_Mdv.name = 'Jaw%sJntOpenTyAmp_Mdv' % part

		self.JawJntOpen_Clamp.attr('outputR') >> self.JawJntOpenTyAmp_Mdv.attr('i1x')
		jawCtrlShp.attr('openTyAmp') >> self.JawJntOpenTyAmp_Mdv.attr('i2x')
		self.Jaw_Ctrl.attr('ty') >> self.JawJntTyColl_Pma.last1D()
		self.JawJntOpenTyAmp_Mdv.attr('ox') >> self.JawJntTyColl_Pma.last1D()

		self.JawJntOpenTzAmp_Mdv = lrc.MultiplyDivide()
		self.JawJntOpenTzAmp_Mdv.name = 'Jaw%sJntOpenTzAmp_Mdv' % part

		self.JawJntOpen_Clamp.attr('outputR') >> self.JawJntOpenTzAmp_Mdv.attr('i1x')
		jawCtrlShp.attr('openTzAmp') >> self.JawJntOpenTzAmp_Mdv.attr('i2x')
		self.Jaw_Ctrl.attr('tz') >> self.JawJntTzColl_Pma.last1D()
		self.JawJntOpenTzAmp_Mdv.attr('ox') >> self.JawJntTzColl_Pma.last1D()

		self.JawJntCloseTyAmp_Mdv = lrc.MultiplyDivide()
		self.JawJntCloseTyAmp_Mdv.name = 'Jaw%sJntCloseTyAmp_Mdv' % part

		self.JawJntClose_Clamp.attr('outputR') >> self.JawJntCloseTyAmp_Mdv.attr('i1x')
		jawCtrlShp.attr('closeTyAmp') >> self.JawJntCloseTyAmp_Mdv.attr('i2x')
		self.Jaw_Ctrl.attr('ty') >> self.JawJntTyColl_Pma.last1D()
		self.JawJntCloseTyAmp_Mdv.attr('ox') >> self.JawJntTyColl_Pma.last1D()

		self.JawJntCloseTzAmp_Mdv = lrc.MultiplyDivide()
		self.JawJntCloseTzAmp_Mdv.name = 'Jaw%sJntCloseTzAmp_Mdv' % part

		self.JawJntClose_Clamp.attr('outputR') >> self.JawJntCloseTzAmp_Mdv.attr('i1x')
		jawCtrlShp.attr('closeTzAmp') >> self.JawJntCloseTzAmp_Mdv.attr('i2x')
		self.Jaw_Ctrl.attr('tz') >> self.JawJntTzColl_Pma.last1D()
		self.JawJntCloseTzAmp_Mdv.attr('ox') >> self.JawJntTzColl_Pma.last1D()
		
		# Lip Ribbon - Up
		self.upRbn = LipPartRibbon(
									tmpLocs=lipUpTmpLocs,
									part='Up%s' % part,
									offsetCurve=offsetCurve
								)
		self.upRbn.Jnt_Grp.parent(self.Jnt_Grp)
		self.upRbn.Skin_Grp.parent(self.Skin_Grp)
		self.upRbn.Still_Grp.parent(self.Still_Grp)

		# Lip Ribbon - Low
		self.lowRbn = LipPartRibbon(
									tmpLocs=lipLowTmpLocs,
									part='Low%s' % part,
									offsetCurve=offsetCurve
								)
		self.lowRbn.Jnt_Grp.parent(self.Jnt_Grp)
		self.lowRbn.Skin_Grp.parent(self.Skin_Grp)
		self.lowRbn.Still_Grp.parent(self.Still_Grp)

		# Lip Constraint - Up up
		self.UpCnr_L_Rev = self.lipConstraint(
												self.upRbn.CnrJntZr_L_Grp,
												self.Hd_Jnt,
												self.Jaw_Jnt,
												'UpCnr',
												part,
												'_L_'
											)
		self.UpB_L_Rev = self.lipConstraint(
												self.upRbn.BJntZr_L_Grp,
												self.Hd_Jnt,
												self.Jaw_Jnt,
												'UpB',
												part,
												'_L_'
											)
		self.UpA_L_Rev = self.lipConstraint(
												self.upRbn.AJntZr_L_Grp,
												self.Hd_Jnt,
												self.Jaw_Jnt,
												'UpA',
												part,
												'_L_'
											)
		self.UpMid_Rev = self.lipConstraint(
												self.upRbn.MidJntZr_Grp,
												self.Hd_Jnt,
												self.Jaw_Jnt,
												'UpMid',
												part,
												''
											)
		self.UpA_R_Rev = self.lipConstraint(
												self.upRbn.AJntZr_R_Grp,
												self.Hd_Jnt,
												self.Jaw_Jnt,
												'UpA',
												part,
												'_R_'
											)
		self.UpB_R_Rev = self.lipConstraint(
												self.upRbn.BJntZr_R_Grp,
												self.Hd_Jnt,
												self.Jaw_Jnt,
												'UpB',
												part,
												'_R_'
											)
		self.UpCnr_R_Rev = self.lipConstraint(
												self.upRbn.CnrJntZr_R_Grp,
												self.Hd_Jnt,
												self.Jaw_Jnt,
												'UpCnr',
												part,
												'_R_'
											)

		# Lip Constraint - Low low
		self.LowCnr_L_Rev = self.lipConstraint(
												self.lowRbn.CnrJntZr_L_Grp,
												self.Hd_Jnt,
												self.Jaw_Jnt,
												'LowCnr',
												part,
												'_L_'
											)
		self.LowB_L_Rev = self.lipConstraint(
												self.lowRbn.BJntZr_L_Grp,
												self.Hd_Jnt,
												self.Jaw_Jnt,
												'LowB',
												part,
												'_L_'
											)
		self.LowA_L_Rev = self.lipConstraint(
												self.lowRbn.AJntZr_L_Grp,
												self.Hd_Jnt,
												self.Jaw_Jnt,
												'LowA',
												part,
												'_L_'
											)
		self.LowMid_Rev = self.lipConstraint(
												self.lowRbn.MidJntZr_Grp,
												self.Hd_Jnt,
												self.Jaw_Jnt,
												'LowMid',
												part,
												''
											)
		self.LowA_R_Rev = self.lipConstraint(
												self.lowRbn.AJntZr_R_Grp,
												self.Hd_Jnt,
												self.Jaw_Jnt,
												'LowA',
												part,
												'_R_'
											)
		self.LowB_R_Rev = self.lipConstraint(
												self.lowRbn.BJntZr_R_Grp,
												self.Hd_Jnt,
												self.Jaw_Jnt,
												'LowB',
												part,
												'_R_'
											)
		self.LowCnr_R_Rev = self.lipConstraint(
												self.lowRbn.CnrJntZr_R_Grp,
												self.Hd_Jnt,
												self.Jaw_Jnt,
												'LowCnr',
												part,
												'_R_'
											)

		# Seal Rig
		jawCtrlShp = lrc.Dag(self.Jaw_Ctrl.shape)
		self.Jaw_Ctrl.add(ln='lftSeal', max=10, k=True)
		self.Jaw_Ctrl.add(ln='rgtSeal', max=10, k=True)

		# Seal Rig - Up
		cnrStart = 0
		cnrEnd = 3
		cnrHd = 0.53
		cnrSeal = 0.5

		bStart = 2
		bEnd = 5
		bHd = 0.85
		bSeal = 0.5

		aStart = 4
		aEnd = 8
		aHd = 0.95
		aSeal = 0.5

		midStart = 5
		midEnd = 10
		midHd = 0.98
		midSeal = 0.5

		# Seal Rig - UpCnr _L_
		self.UpCnrSeal_L_Rem = self.sealRemap(
													self.upRbn.CnrJntZr_L_Grp.attr('toW0'),
													self.Jaw_Ctrl.attr('lftSeal'),
													'UpCnr',
													part,
													'_L_',
													cnrStart,
													cnrEnd,
													cnrHd,
													cnrSeal
												)

		# Seal Rig - LowCnr _L_
		self.LowCnrSeal_L_Rem = self.sealRemap(
													self.lowRbn.CnrJntZr_L_Grp.attr('toW0'),
													self.Jaw_Ctrl.attr('lftSeal'),
													'LowCnr',
													part,
													'_L_',
													cnrStart,
													cnrEnd,
													1-cnrHd,
													cnrSeal
												)

		# Seal Rig - UpCnr _R_
		self.UpCnrSeal_R_Rem = self.sealRemap(
													self.upRbn.CnrJntZr_R_Grp.attr('toW0'),
													self.Jaw_Ctrl.attr('rgtSeal'),
													'UpCnr',
													part,
													'_R_',
													cnrStart,
													cnrEnd,
													cnrHd,
													cnrSeal
												)

		# Seal Rig - LowCnr _R_
		self.LowCnrSeal_R_Rem = self.sealRemap(
													self.lowRbn.CnrJntZr_R_Grp.attr('toW0'),
													self.Jaw_Ctrl.attr('rgtSeal'),
													'LowCnr',
													part,
													'_R_',
													cnrStart,
													cnrEnd,
													1-cnrHd,
													cnrSeal
												)

		# Seal Rig - UpB _L_
		self.UpBSeal_L_Rem = self.sealRemap(
													self.upRbn.BJntZr_L_Grp.attr('toW0'),
													self.Jaw_Ctrl.attr('lftSeal'),
													'UpB',
													part,
													'_L_',
													bStart,
													bEnd,
													bHd,
													bSeal
												)

		# Seal Rig - LowB _L_
		self.LowBSeal_L_Rem = self.sealRemap(
													self.lowRbn.BJntZr_L_Grp.attr('toW0'),
													self.Jaw_Ctrl.attr('lftSeal'),
													'LowB',
													part,
													'_L_',
													bStart,
													bEnd,
													1-bHd,
													bSeal
												)

		# Seal Rig - UpB _R_
		self.UpBSeal_R_Rem = self.sealRemap(
													self.upRbn.BJntZr_R_Grp.attr('toW0'),
													self.Jaw_Ctrl.attr('rgtSeal'),
													'UpB',
													part,
													'_R_',
													bStart,
													bEnd,
													bHd,
													bSeal
												)

		# Seal Rig - LowB _R_
		self.LowBSeal_R_Rem = self.sealRemap(
													self.lowRbn.BJntZr_R_Grp.attr('toW0'),
													self.Jaw_Ctrl.attr('rgtSeal'),
													'LowB',
													part,
													'_R_',
													bStart,
													bEnd,
													1-bHd,
													bSeal
												)

		# Seal Rig - UpA _L_
		self.UpASeal_L_Rem = self.sealRemap(
													self.upRbn.AJntZr_L_Grp.attr('toW0'),
													self.Jaw_Ctrl.attr('lftSeal'),
													'UpA',
													part,
													'_L_',
													aStart,
													aEnd,
													aHd,
													aSeal
												)

		# Seal Rig - LowA _L_
		self.LowASeal_L_Rem = self.sealRemap(
													self.lowRbn.AJntZr_L_Grp.attr('toW0'),
													self.Jaw_Ctrl.attr('lftSeal'),
													'LowA',
													part,
													'_L_',
													aStart,
													aEnd,
													1-aHd,
													aSeal
												)

		# Seal Rig - UpA _R_
		self.UpASeal_R_Rem = self.sealRemap(
													self.upRbn.AJntZr_R_Grp.attr('toW0'),
													self.Jaw_Ctrl.attr('rgtSeal'),
													'UpA',
													part,
													'_R_',
													aStart,
													aEnd,
													aHd,
													aSeal
												)

		# Seal Rig - LowA _R_
		self.LowASeal_R_Rem = self.sealRemap(
													self.lowRbn.AJntZr_R_Grp.attr('toW0'),
													self.Jaw_Ctrl.attr('rgtSeal'),
													'LowA',
													part,
													'_R_',
													aStart,
													aEnd,
													1-aHd,
													aSeal
												)

		# Seal Rig - UpMid
		self.UpMidSealSum_Add = lrc.AddDoubleLinear()
		self.UpMidSealSum_Add.name = 'LipUpMid%sSealSum_Add' % part
		
		self.UpMidSeal_L_Rem = self.sealRemap(
													self.UpMidSealSum_Add.attr('i1'),
													self.Jaw_Ctrl.attr('lftSeal'),
													'UpMid',
													part,
													'_L_',
													midStart,
													midEnd,
													midHd/2,
													midSeal/2
												)
		self.UpMidSeal_R_Rem = self.sealRemap(
													self.UpMidSealSum_Add.attr('i2'),
													self.Jaw_Ctrl.attr('rgtSeal'),
													'UpMid',
													part,
													'_R_',
													midStart,
													midEnd,
													midHd/2,
													midSeal/2
												)
		
		self.UpMidSealSum_Add.attr('o') >> self.upRbn.MidJntZr_Grp.attr('toW0')

		# Seal Rig - LowMid
		self.LowMidSealSum_Add = lrc.AddDoubleLinear()
		self.LowMidSealSum_Add.name = 'LipLowMid%sSealSum_Add' % part
		
		self.LowMidSeal_L_Rem = self.sealRemap(
													self.LowMidSealSum_Add.attr('i1'),
													self.Jaw_Ctrl.attr('lftSeal'),
													'LowMid',
													part,
													'_L_',
													midStart,
													midEnd,
													(1-midHd)/2,
													midSeal/2
												)
		self.LowMidSeal_R_Rem = self.sealRemap(
													self.LowMidSealSum_Add.attr('i2'),
													self.Jaw_Ctrl.attr('rgtSeal'),
													'LowMid',
													part,
													'_R_',
													midStart,
													midEnd,
													(1-midHd)/2,
													midSeal/2
												)
		
		self.LowMidSealSum_Add.attr('o') >> self.lowRbn.MidJntZr_Grp.attr('toW0')

		# Open Rig - UpCnr _L_
		self.UpCnrOpen_L_Clamp, self.UpCnrOpen_L_Mult = self.openMult(
																			self.upRbn.Cnr_L_Jnt.attr('ty'),
																			self.Jaw_Ctrl.attr('lftSeal'),
																			'UpCnrOpen',
																			part,
																			'_L_',
																			-0.1
																		)
		# Open Rig - LowCnr _L_
		self.LowCnrOpen_L_Clamp, self.LowCnrOpen_L_Mult = self.openMult(
																			self.lowRbn.Cnr_L_Jnt.attr('ty'),
																			self.Jaw_Ctrl.attr('lftSeal'),
																			'LowCnrOpen',
																			part,
																			'_L_',
																			0.1
																		)

		# Open Rig - UpB _L_
		self.UpBOpen_L_Clamp, self.UpBOpen_L_Mult = self.openMult(
																			self.upRbn.B_L_Jnt.attr('ty'),
																			self.Jaw_Ctrl.attr('lftSeal'),
																			'UpBOpen',
																			part,
																			'_L_',
																			-0.2
																		)
		# Open Rig - LowB _L_
		self.LowBOpen_L_Clamp, self.LowBOpen_L_Mult = self.openMult(
																			self.lowRbn.B_L_Jnt.attr('ty'),
																			self.Jaw_Ctrl.attr('lftSeal'),
																			'LowBOpen',
																			part,
																			'_L_',
																			0.2
																		)

		# Open Rig - UpCnr _R_
		self.UpCnrOpen_R_Clamp, self.UpCnrOpen_R_Mult = self.openMult(
																			self.upRbn.Cnr_R_Jnt.attr('ty'),
																			self.Jaw_Ctrl.attr('rgtSeal'),
																			'UpCnrOpen',
																			part,
																			'_R_',
																			-0.1
																		)
		# Open Rig - LowCnr _R_
		self.LowCnrOpen_R_Clamp, self.LowCnrOpen_R_Mult = self.openMult(
																			self.lowRbn.Cnr_R_Jnt.attr('ty'),
																			self.Jaw_Ctrl.attr('rgtSeal'),
																			'LowCnrOpen',
																			part,
																			'_R_',
																			0.1
																		)

		# Open Rig - UpB _R_
		self.UpBOpen_R_Clamp, self.UpBOpen_R_Mult = self.openMult(
																			self.upRbn.B_R_Jnt.attr('ty'),
																			self.Jaw_Ctrl.attr('rgtSeal'),
																			'UpBOpen',
																			part,
																			'_R_',
																			-0.2
																		)
		# Open Rig - LowB _R_
		self.LowBOpen_R_Clamp, self.LowBOpen_R_Mult = self.openMult(
																			self.lowRbn.B_R_Jnt.attr('ty'),
																			self.Jaw_Ctrl.attr('rgtSeal'),
																			'LowBOpen',
																			part,
																			'_R_',
																			0.2
																		)

		# Blend Shape Controller - Mth
		self.Mth_Ctrl = lrc.Control('nrbCircle')
		self.Mth_Ctrl.name = 'Mth%s_Ctrl' % part
		self.Mth_Ctrl.color = 'yellow'
		self.Mth_Ctrl.lockHideAttrs('v')

		self.MthCtrlZr_Grp = lrc.group(self.Mth_Ctrl)
		self.MthCtrlZr_Grp.name = 'Mth%sCtrlZr_Grp' % part
		self.MthCtrlZr_Grp.snap(cntMthTmpLoc)
		self.MthCtrlZr_Grp.parent(self.Ctrl_Grp)

		# Lip Part Controller - Up
		self.LipUp_Ctrl = lrc.Control('nrbCircle')
		self.LipUp_Ctrl.name = 'LipUpMth%s_Ctrl' % part
		self.LipUp_Ctrl.color = 'yellow'
		self.LipUp_Ctrl.lockHideAttrs('v')

		self.LipUpCtrlInv_Grp = lrc.group(self.LipUp_Ctrl)
		self.LipUpCtrlInv_Grp.name = 'LipUp%sCtrlInv_Grp' % part

		self.LipUpCtrlZr_Grp = lrc.group(self.LipUpCtrlInv_Grp)
		self.LipUpCtrlZr_Grp.name = 'LipUp%sCtrlZr_Grp' % part
		self.LipUpCtrlZr_Grp.snap(lipUpTmpLocs[0])
		self.LipUpCtrlZr_Grp.parent(self.Ctrl_Grp)

		# Lip Part Controller - Low
		self.LipLow_Ctrl = lrc.Control('nrbCircle')
		self.LipLow_Ctrl.name = 'LipLowMth%s_Ctrl' % part
		self.LipLow_Ctrl.color = 'yellow'
		self.LipLow_Ctrl.lockHideAttrs('v')

		self.LipLowCtrlInv_Grp = lrc.group(self.LipLow_Ctrl)
		self.LipLowCtrlInv_Grp.name = 'LipLow%sCtrlInv_Grp' % part

		self.LipLowCtrlZr_Grp = lrc.group(self.LipLowCtrlInv_Grp)
		self.LipLowCtrlZr_Grp.name = 'LipLow%sCtrlZr_Grp' % part
		self.LipLowCtrlZr_Grp.snap(lipLowTmpLocs[0])
		self.LipLowCtrlZr_Grp.parent(self.Ctrl_Grp)

		# Blend Shape Controller - Mth _L_
		self.Mth_L_Ctrl = lrc.Control('nrbCircle')
		self.Mth_L_Ctrl.name = 'Mth%s_L_Ctrl' % part
		self.Mth_L_Ctrl.color = 'red'
		self.Mth_L_Ctrl.lockHideAttrs('tz', 'r', 's', 'v')

		self.MthCtrlInv_L_Grp = lrc.group(self.Mth_L_Ctrl)
		self.MthCtrlInv_L_Grp.name = 'Mth%sCtrlInv_L_Grp' % part

		self.MthCtrlZr_L_Grp = lrc.group(self.MthCtrlInv_L_Grp)
		self.MthCtrlZr_L_Grp.name = 'Mth%sCtrlZr_L_Grp' % part
		self.MthCtrlZr_L_Grp.snap(lftCnrTmpLoc)
		self.MthCtrlZr_L_Grp.parent(self.Ctrl_Grp)

		# Blend Shape Controller - Mth _R_
		self.Mth_R_Ctrl = lrc.Control('nrbCircle')
		self.Mth_R_Ctrl.name = 'Mth%s_R_Ctrl' % part
		self.Mth_R_Ctrl.color = 'blue'
		self.Mth_R_Ctrl.lockHideAttrs('tz', 'r', 's', 'v')

		self.MthCtrlInv_R_Grp = lrc.group(self.Mth_R_Ctrl)
		self.MthCtrlInv_R_Grp.name = 'Mth%sCtrlInv_R_Grp' % part

		self.MthCtrlZr_R_Grp = lrc.group(self.MthCtrlInv_R_Grp)
		self.MthCtrlZr_R_Grp.name = 'Mth%sCtrlZr_R_Grp' % part
		self.MthCtrlZr_R_Grp.snap(rgtCnrTmpLoc)
		self.MthCtrlZr_R_Grp.parent(self.Ctrl_Grp)
		self.MthCtrlZr_R_Grp.attr('ry').v = 180

	def openMult(self, driven, driver, lipPart, part, side, mult):

		ctrl = lrc.Dag(str(driver).split('.')[0])
		ctrlShp = lrc.Dag(ctrl.shape)

		attrPrefix = '%s%s' % (lipPart[0].lower(), lipPart[1:])

		# Multiplier
		ctrlShp.add(ln='__%s%s__' % (attrPrefix, side), k=True)
		ctrlShp.attr('__%s%s__' % (attrPrefix, side)).lock = True

		openMultAttr = '%s%sopenMult' % (attrPrefix, side)
		ctrlShp.add(ln=openMultAttr, dv=mult, k=True)

		clamp = lrc.Clamp()
		clamp.name = 'Lip%s%sOpen%sClamp' % (lipPart, part, side)

		mult = lrc.MultiplyDivide()
		mult.name = 'Lip%s%sOpen%sMult' % (lipPart, part, side)

		clamp.attr('minR').v = -100
		clamp.attr('maxR').v = 0
		driver >> clamp.attr('inputR')
		clamp.attr('outputR') >> mult.attr('i1x')
		ctrlShp.attr(openMultAttr) >> mult.attr('i2x')
		mult.attr('ox') >> driven

		return clamp, mult

	def sealRemap(self, driven, driver, lipPart, part, side, iMin, iMax, openVal, sealVal):
		'''
		Remap the driver value to driven value.
		'''
		ctrl = lrc.Dag(str(driver).split('.')[0])
		ctrlShp = lrc.Dag(ctrl.shape)

		attrPrefix = '%s%s' % (lipPart[0].lower(), lipPart[1:])

		minAttr = '%s%sMin' % (attrPrefix, side)
		maxAttr = '%s%sMax' % (attrPrefix, side)
		openAttr = '%s%sOpen' % (attrPrefix, side)
		sealAttr = '%s%sSeal' % (attrPrefix, side)

		# ReMap Attrs
		ctrlShp.add(ln='__%s%s__' % (attrPrefix, side), k=True)
		ctrlShp.attr('__%s%s__' % (attrPrefix, side)).lock = True
		ctrlShp.add(ln=minAttr, dv=iMin, k=True)
		ctrlShp.add(ln=maxAttr, dv=iMax, k=True)
		ctrlShp.add(ln=openAttr, dv=openVal, k=True)
		ctrlShp.add(ln=sealAttr, dv=sealVal, k=True)

		rem = lrc.RemapValue()
		rem.name = 'Lip%s%sSeal%sRem' % (lipPart, part, side)

		driver >> rem.attr('inputValue')
		ctrlShp.attr(minAttr) >> rem.attr('inputMin')
		ctrlShp.attr(maxAttr) >> rem.attr('inputMax')
		ctrlShp.attr(openAttr) >> rem.attr('outputMin')
		ctrlShp.attr(sealAttr) >> rem.attr('outputMax')
		rem.attr('outValue') >> driven

		return rem

	def lipJoint(self, posObj, lipPart, part, side, hdJnt):
		'''
		Create a joint, a zero gorup, an offset group.
		Move the zero group to the postion of posObj.
		'''
		pos = mc.xform(posObj, q=True, ws=True, t=True)
		
		jnt = lrc.Joint()
		jnt.name = 'Lip%s%s%sJnt' % (lipPart, part, side)

		offGrp = lrc.group(jnt)
		offGrp.name = 'Lip%s%sJntOff%sGrp' % (lipPart, part, side)

		zrGrp = lrc.group(offGrp)
		zrGrp.name = 'Lip%s%sJntZr%sGrp' % (lipPart, part, side)

		mc.xform(zrGrp, ws=True, t=pos)

		zrGrp.parent(hdJnt)

		return jnt, offGrp, zrGrp

	def lipConstraint(self, lipZrGrp, hdJnt, jawJnt, lipPart, part, side):
		'''
		Constrain zero group to head joint and jaw joint.
		Create anattribute 'toW0' and a reverse node to retain constrain weight value at 1.
		'''
		parCon = lrc.parentConstraint(hdJnt, jawJnt, lipZrGrp, mo=True)

		lipZrGrp.add(ln='toW0', min=0, max=1, dv=0, k=True)

		rev = lrc.Reverse()
		rev.name = 'Lip%s%sJntZrCon%sRev' % (lipPart, part, side)

		lipZrGrp.attr('toW0') >> parCon.attr('w0')
		parCon.attr('w0') >> rev.attr('ix')
		rev.attr('ox') >> parCon.attr('w1')

		return rev

def mainLite():

	mouthRigObj = MouthRigLite(
								headTmpLoc='HdMthRigTmp_Loc',
								jawTmpLoc='JawMthRigTmp_Loc',
								cntMthTmpLoc='CntMthRigTmp_Loc',
								lftCnrTmpLoc='CnrMthRigTmp_L_Loc',
								rgtCnrTmpLoc='CnrMthRigTmp_R_Loc',
								part='MthRig'
							)

def main(lipRig=True, offsetCurve=0.05):

	mouthRigObj = MouthRig(
								headTmpLoc='HdMthRigTmp_Loc',
								jawTmpLoc='JawMthRigTmp_Loc',
								lipUpTmpLocs=[
												'LipUpMidMthRigTmp_Loc',
												'LipUpCnrMthRigTmp_L_Loc',
												'LipUpCnrMthRigTmp_R_Loc',
												'LipUpAMthRigTmp_L_Loc',
												'LipUpBMthRigTmp_L_Loc',
												'LipUpAMthRigTmp_R_Loc',
												'LipUpBMthRigTmp_R_Loc'
											],
								lipLowTmpLocs=[
												'LipLowMidMthRigTmp_Loc',
												'LipLowCnrMthRigTmp_L_Loc',
												'LipLowCnrMthRigTmp_R_Loc',
												'LipLowAMthRigTmp_L_Loc',
												'LipLowBMthRigTmp_L_Loc',
												'LipLowAMthRigTmp_R_Loc',
												'LipLowBMthRigTmp_R_Loc'
											],
								cntMthTmpLoc='CntMthRigTmp_Loc',
								lftCnrTmpLoc='CnrMthRigTmp_L_Loc',
								rgtCnrTmpLoc='CnrMthRigTmp_R_Loc',
								part='MthRig',
								offsetCurve=offsetCurve
							)

	if not lipRig:
		return False

	lipRigObj = LipDetailRig(
								cntTmpLoc='CntLipRigTmp_Loc',
								upMidTmpLoc='UpMidLipRigTmp_Loc',
								lowMidTmpLoc='LowMidLipRigTmp_Loc',
								lftCnrTmpLoc='CnrLipRigTmp_L_Loc',
								rgtCnrTmpLoc='CnrLipRigTmp_R_Loc',
								upALftTmpLoc='UpALipRigTmp_L_Loc',
								upBLftTmpLoc='UpBLipRigTmp_L_Loc',
								lowALftTmpLoc='LowALipRigTmp_L_Loc',
								lowBLftTmpLoc='LowBLipRigTmp_L_Loc',
								upARgtTmpLoc='UpALipRigTmp_R_Loc',
								upBRgtTmpLoc='UpBLipRigTmp_R_Loc',
								lowARgtTmpLoc='LowALipRigTmp_R_Loc',
								lowBRgtTmpLoc='LowBLipRigTmp_R_Loc',
								rbnTmpNrb='RbnLipRigTmp_Nrb',
								part='LipRig',
								mthRigObj=mouthRigObj
							)
	lipRigObj.Ctrl_Grp.parent(mouthRigObj.Ctrl_Grp)
	lipRigObj.Jnt_Grp.parent(mouthRigObj.Jnt_Grp)
	lipRigObj.Skin_Grp.parent(mouthRigObj.Skin_Grp)
	lipRigObj.Still_Grp.parent(mouthRigObj.Still_Grp)

	mc.select(lipRigObj.Skin_Grp, r=True)