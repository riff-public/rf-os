import maya.cmds as mc
import maya.mel as mm

import lpRig.core as lrc
reload(lrc)
import lpRig.rigTools as lrr
reload(lrr)
import lpRig.ribbon as lrb
reload(lrb)

class HindLegRig(object):

	def __init__(
					self,
					parent='Pelvis_Jnt',
					ctrlGrp='',
					jntGrp='',
					ikhGrp='',
					skinGrp='',
					stillGrp='',
					ribbon=True,
					tmpJnt=[
								'UpperLeg_L_Jnt',
								'MiddleLeg_L_Jnt',
								'LowerLeg_Jnt',
								'Ankle_L_Jnt',
								'Ball_L_Jnt',
								'Toe_L_Jnt',
								'Heel_L_Jnt',
								'FootIn_L_Jnt',
								'FootOut_L_Jnt',
								'KneeIk_L_Jnt',
								'UpKneeIk_L_Jnt',
								'LowKneeIk_L_Jnt'
							],
					part='',
					side=''
				):

		if side:
			side = '_%s_' % side
		else:
			side = '_'

		# Skin joints
		self.UpLeg_Jnt = lrc.Dag(tmpJnt[0])
		self.MidLeg_Jnt = lrc.Dag(tmpJnt[1])
		self.LowLeg_Jnt = lrc.Dag(tmpJnt[2])
		self.Ankle_Jnt = lrc.Dag(tmpJnt[3])
		self.Ball_Jnt = lrc.Dag(tmpJnt[4])
		self.Toe_Jnt = lrc.Dag(tmpJnt[5])
		self.Heel_Jnt = lrc.Dag(tmpJnt[6])
		self.FootIn_Jnt = lrc.Dag(tmpJnt[7])
		self.FootOut_Jnt = lrc.Dag(tmpJnt[8])
		self.KneeIk_Jnt = lrc.Dag(tmpJnt[9])
		self.UpKneeIk_Jnt = lrc.Dag(tmpJnt[10])
		self.LowKneeIk_Jnt = lrc.Dag(tmpJnt[11])

		self.Ball_Jnt.attr('ssc').v = 0

		self.UpLeg_Jnt.rotateOrder = 'yzx'
		self.MidLeg_Jnt.rotateOrder = 'yzx'
		self.LowLeg_Jnt.rotateOrder = 'yzx'
		self.Ankle_Jnt.rotateOrder = 'yzx'
		self.Ball_Jnt.rotateOrder = 'yzx'
		self.Toe_Jnt.rotateOrder = 'yzx'

		# Main group
		self.Ctrl_Grp = lrc.Null()
		self.Ctrl_Grp.name = 'Leg%sCtrl%sGrp' % (part, side)

		mc.parentConstraint(parent, self.Ctrl_Grp)
		mc.scaleConstraint(parent, self.Ctrl_Grp)

		self.Jnt_Grp = lrc.Null()
		self.Jnt_Grp.name = 'Leg%sJnt%sGrp' % (part, side)

		mc.parentConstraint(parent, self.Jnt_Grp)
		mc.scaleConstraint(parent, self.Jnt_Grp)

		# Length
		self.upLegLen = lrc.distance(self.UpLeg_Jnt, self.MidLeg_Jnt)
		self.midLegLen = lrc.distance(self.MidLeg_Jnt, self.LowLeg_Jnt)
		self.lowLegLen = lrc.distance(self.LowLeg_Jnt, self.Ankle_Jnt)

		# Main Controller
		self.Leg_Ctrl = lrc.Control('stick')
		self.Leg_Ctrl.lockHideAttrs('tx', 'ty', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz', 'v')
		self.Leg_Ctrl.name = 'Leg%s%sCtrl' % (part, side)

		self.LegCtrlZr_Grp = lrc.group(self.Leg_Ctrl)
		self.LegCtrlZr_Grp.name = 'LegCtrlZr%s%sGrp' % (part, side)

		mc.parentConstraint(self.Ankle_Jnt, self.LegCtrlZr_Grp)
		self.LegCtrlZr_Grp.parent(self.Ctrl_Grp)

		# Main Controller
		if side == '_R_':
			self.Leg_Ctrl.color = 'blue'
		else:
			self.Leg_Ctrl.color = 'red'

		# Non-roll Joints
		(self.UpLegNr_Jnt,
		self.UpLegNrJntZr_Grp,
		self.UpLegTipNr_Jnt,
		self.UpLegNr_Ikh,
		self.UpLegNrIkhZr_Grp) = lrr.createNonRollJointChain(parent, self.UpLeg_Jnt, self.MidLeg_Jnt, 'UpLeg%s' % part, side)
		self.UpLegNrJntZr_Grp.parent(self.Jnt_Grp)
		self.UpLegNrIkhZr_Grp.parent(self.Jnt_Grp)

		(self.MidLegNr_Jnt,
		self.MidLegNrJntZr_Grp,
		self.MidLegTipNr_Jnt,
		self.MidLegNr_Ikh,
		self.MidLegNrIkhZr_Grp) = lrr.createNonRollJointChain(self.UpLeg_Jnt, self.MidLeg_Jnt, self.LowLeg_Jnt, 'MidLeg%s' % part, side)
		self.MidLegNrJntZr_Grp.parent(self.Jnt_Grp)
		self.MidLegNrIkhZr_Grp.parent(self.Jnt_Grp)

		(self.LowLegNr_Jnt,
		self.LowLegNrJntZr_Grp,
		self.LowLegTipNr_Jnt,
		self.LowLegNr_Ikh,
		self.LowLegNrIkhZr_Grp) = lrr.createNonRollJointChain(self.MidLeg_Jnt, self.LowLeg_Jnt, self.Ankle_Jnt, 'LowLeg%s' % part, side)
		self.LowLegNrJntZr_Grp.parent(self.Jnt_Grp)
		self.LowLegNrIkhZr_Grp.parent(self.Jnt_Grp)

		(self.AnkleNr_Jnt,
		self.AnkleNrJntZr_Grp,
		self.AnkleTipNr_Jnt,
		self.AnkleNr_Ikh,
		self.AnkleNrIkhZr_Grp) = lrr.createNonRollJointChain(self.LowLeg_Jnt, self.Ankle_Jnt, self.Ball_Jnt, 'Ankle%s' % part, side)
		self.AnkleNrJntZr_Grp.parent(self.Jnt_Grp)
		self.AnkleNrIkhZr_Grp.parent(self.Jnt_Grp)

		# Aa rig
		self.aaUpLeg = lrr.addAa(self.UpLeg_Jnt, self.LowLeg_Jnt, 'UpLeg%s' % part, side, (0, 0, 1))
		self.aaUpLeg.orig.parent(self.Jnt_Grp)
		
		# Foot scale
		self.Leg_Ctrl.add(ln='footScale', dv=1, k=True)
		self.Leg_Ctrl.attr('footScale') >> self.Ankle_Jnt.attr('sx')
		self.Leg_Ctrl.attr('footScale') >> self.Ankle_Jnt.attr('sy')
		self.Leg_Ctrl.attr('footScale') >> self.Ankle_Jnt.attr('sz')
		
		# FK rig
		# FK rig - Main groups
		self.LegFkCtrl_Grp = lrc.Null()
		self.LegFkCtrl_Grp.name = 'LegFkCtrl%s%sGrp' % (part, side)
		self.LegFkCtrl_Grp.snap(self.Ctrl_Grp)
		self.LegFkCtrl_Grp.parent(self.Ctrl_Grp)
		
		self.LegFkJnt_Grp = lrc.Null()
		self.LegFkJnt_Grp.name = 'LegFkJnt%s%sGrp' % (part, side)
		self.LegFkJnt_Grp.snap(self.Jnt_Grp)
		self.LegFkJnt_Grp.parent(self.Jnt_Grp)
		
		# FK rig - Joints
		self.UpLegFk_Jnt = lrr.jointAt(self.UpLeg_Jnt)
		self.MidLegFk_Jnt = lrr.jointAt(self.MidLeg_Jnt)
		self.LowLegFk_Jnt = lrr.jointAt(self.LowLeg_Jnt)
		self.AnkleFk_Jnt = lrr.jointAt(self.Ankle_Jnt)
		self.BallFk_Jnt = lrr.jointAt(self.Ball_Jnt)
		self.ToeFk_Jnt = lrr.jointAt(self.Toe_Jnt)
		
		self.UpLegFk_Jnt.name = 'UpLegFk%s%sJnt' % (part, side)
		self.MidLegFk_Jnt.name = 'MidLegFk%s%sJnt' % (part, side)
		self.LowLegFk_Jnt.name = 'LowLegFk%s%sJnt' % (part, side)
		self.AnkleFk_Jnt.name = 'AnkleFk%s%sJnt' % (part, side)
		self.BallFk_Jnt.name = 'BallFk%s%sJnt' % (part, side)
		self.ToeFk_Jnt.name = 'ToeFk%s%sJnt' % (part, side)

		self.ToeFk_Jnt.parent(self.BallFk_Jnt)
		self.BallFk_Jnt.parent(self.AnkleFk_Jnt)
		self.AnkleFk_Jnt.parent(self.LowLegFk_Jnt)
		self.LowLegFk_Jnt.parent(self.MidLegFk_Jnt)
		self.MidLegFk_Jnt.parent(self.UpLegFk_Jnt)
		self.UpLegFk_Jnt.parent(self.LegFkJnt_Grp)
		self.BallFk_Jnt.attr('ssc').v = 0

		self.UpLegFk_Jnt.rotateOrder = 'yzx'
		self.MidLegFk_Jnt.rotateOrder = 'yzx'
		self.LowLegFk_Jnt.rotateOrder = 'yzx'
		self.AnkleFk_Jnt.rotateOrder = 'yzx'
		self.BallFk_Jnt.rotateOrder = 'yzx'
		self.ToeFk_Jnt.rotateOrder = 'yzx'

		# FK rig - Controllers
		self.UpLegFk_Ctrl = lrr.jointControl('square')
		self.UpLegFk_Ctrl.name = 'UpLegFk%s%sCtrl' % (part, side)
		self.UpLegFk_Ctrl.lockHideAttrs('sx', 'sy', 'sz', 'v')

		self.UpLegFkCtrlZr_Grp = lrc.group(self.UpLegFk_Ctrl)
		self.UpLegFkCtrlZr_Grp.name = 'UpLegFkCtrlZr%s%sGrp' % (part, side)

		self.MidLegFk_Ctrl = lrr.jointControl('square')
		self.MidLegFk_Ctrl.name = 'MidLegFk%s%sCtrl' % (part, side)
		self.MidLegFk_Ctrl.lockHideAttrs('sx', 'sy', 'sz', 'v')
		
		self.MidLegFkGmbl_Ctrl = lrc.addGimbal(self.MidLegFk_Ctrl)
		self.MidLegFkGmbl_Ctrl.name = 'MidLegFkGmbl%s%sCtrl' % (part, side)
		
		self.MidLegFkCtrlZr_Grp = lrc.group(self.MidLegFk_Ctrl)
		self.MidLegFkCtrlZr_Grp.name = 'MidLegFkCtrlZr%s%sGrp' % (part, side)

		self.LowLegFk_Ctrl = lrr.jointControl('square')
		self.LowLegFk_Ctrl.name = 'LowLegFk%s%sCtrl' % (part, side)
		self.LowLegFk_Ctrl.lockHideAttrs('sx', 'sy', 'sz', 'v')
		
		self.LowLegFkGmbl_Ctrl = lrc.addGimbal(self.LowLegFk_Ctrl)
		self.LowLegFkGmbl_Ctrl.name = 'LowLegFkGmbl%s%sCtrl' % (part, side)
		
		self.LowLegFkCtrlZr_Grp = lrc.group(self.LowLegFk_Ctrl)
		self.LowLegFkCtrlZr_Grp.name = 'LowLegFkCtrlZr%s%sGrp' % (part, side)

		self.AnkleFk_Ctrl = lrr.jointControl('square')
		self.AnkleFk_Ctrl.name = 'AnkleFk%s%sCtrl' % (part, side)
		self.AnkleFk_Ctrl.lockHideAttrs('sx', 'sy', 'sz', 'v')
		
		self.AnkleFkGmbl_Ctrl = lrc.addGimbal(self.AnkleFk_Ctrl)
		self.AnkleFkGmbl_Ctrl.name = 'AnkleFkGmbl%s%sCtrl' % (part, side)

		self.AnkleFkCtrlZr_Grp = lrc.group(self.AnkleFk_Ctrl)
		self.AnkleFkCtrlZr_Grp.name = 'AnkleFkCtrlZr%s%sGrp' % (part, side)

		self.AnkleScaOfst_Grp = lrc.Null()
		self.AnkleScaOfst_Grp.name = 'AnkleScaOfst%s%sGrp' % (part, side)
		self.AnkleScaOfst_Grp.parent(self.AnkleFkGmbl_Ctrl)

		self.ToeFk_Ctrl = lrr.jointControl('square')
		self.ToeFk_Ctrl.name = 'ToeFk%s%sCtrl' % (part, side)
		self.ToeFk_Ctrl.lockHideAttrs('sx', 'sy', 'sz', 'v')
		
		self.ToeFkGmbl_Ctrl = lrc.addGimbal(self.ToeFk_Ctrl)
		self.ToeFkGmbl_Ctrl.name = 'ToeFkGmbl%s%sCtrl' % (part, side)
		
		self.ToeFkCtrlZr_Grp = lrc.group(self.ToeFk_Ctrl)
		self.ToeFkCtrlZr_Grp.name = 'ToeFkCtrlZr%s%sGrp' % (part, side)

		# FK rig - Controllers - Color			
		if side == '_R_':
			self.UpLegFk_Ctrl.color = 'blue'
			self.MidLegFk_Ctrl.color = 'blue'
			self.LowLegFk_Ctrl.color = 'blue'
			self.AnkleFk_Ctrl.color = 'blue'
			self.ToeFk_Ctrl.color = 'blue'
		else:
			self.UpLegFk_Ctrl.color = 'red'
			self.MidLegFk_Ctrl.color = 'red'
			self.LowLegFk_Ctrl.color = 'red'
			self.AnkleFk_Ctrl.color = 'red'
			self.ToeFk_Ctrl.color = 'red'

		# FK rig - Controllers - Positioning
		self.UpLegFkCtrlZr_Grp.snapPoint(self.UpLeg_Jnt)
		self.UpLegFk_Ctrl.snapOrient(self.UpLeg_Jnt)
		self.UpLegFk_Ctrl.freeze(r=True)
		
		self.UpLegFkGmbl_Ctrl = lrc.addGimbal(self.UpLegFk_Ctrl)
		self.UpLegFkGmbl_Ctrl.name = 'UpLegFkGmbl%s%sCtrl' % (part, side)

		self.MidLegFkCtrlZr_Grp.snap(self.MidLeg_Jnt)
		self.LowLegFkCtrlZr_Grp.snap(self.LowLeg_Jnt)
		self.AnkleFkCtrlZr_Grp.snap(self.Ankle_Jnt)
		self.ToeFkCtrlZr_Grp.snap(self.Ball_Jnt)
		
		self.ToeFkCtrlZr_Grp.parent(self.AnkleScaOfst_Grp)
		self.AnkleFkCtrlZr_Grp.parent(self.LowLegFkGmbl_Ctrl)
		self.LowLegFkCtrlZr_Grp.parent(self.MidLegFkGmbl_Ctrl)
		self.MidLegFkCtrlZr_Grp.parent(self.UpLegFkGmbl_Ctrl)
		self.UpLegFkCtrlZr_Grp.parent(self.LegFkCtrl_Grp)

		self.UpLegFk_Ctrl.rotateOrder = 'yzx'
		self.UpLegFkGmbl_Ctrl.rotateOrder = 'yzx'
		self.LowLegFk_Ctrl.rotateOrder = 'yzx'
		self.LowLegFkGmbl_Ctrl.rotateOrder = 'yzx'
		self.AnkleFk_Ctrl.rotateOrder = 'yzx'
		self.AnkleFkGmbl_Ctrl.rotateOrder = 'yzx'
		self.ToeFk_Ctrl.rotateOrder = 'yzx'
		self.ToeFkGmbl_Ctrl.rotateOrder = 'yzx'

		# FK rig - Controllers - Strt setup
		(self.UpLegFkStrt_Add,
		self.UpLegFkStrt_Mul) = lrr.fkStretch(
													ctrl=self.UpLegFk_Ctrl,
													target=self.MidLegFkCtrlZr_Grp
												)
		self.UpLegFkStrt_Add.name = 'UpLegFkStrt%s%sAdd' % (part, side)
		self.UpLegFkStrt_Mul.name = 'UpLegFkStrt%s%sMul' % (part, side)

		(self.MidLegFkStrt_Add,
		self.MidLegFkStrt_Mul) = lrr.fkStretch(
													ctrl=self.MidLegFk_Ctrl,
													target=self.LowLegFkCtrlZr_Grp
												)
		self.MidLegFkStrt_Add.name = 'MidLegFkStrt%s%sAdd' % (part, side)
		self.MidLegFkStrt_Mul.name = 'MidLegFkStrt%s%sMul' % (part, side)

		(self.LowLegFkStrt_Add,
		self.LowLegFkStrt_Mul) = lrr.fkStretch(
													ctrl=self.LowLegFk_Ctrl,
													target=self.AnkleFkCtrlZr_Grp
												)
		self.LowLegFkStrt_Add.name = 'LowLegFkStrt%s%sAdd' % (part, side)
		self.LowLegFkStrt_Mul.name = 'LowLegFkStrt%s%sMul' % (part, side)

		(self.ToeFkStrt_Add,
		self.ToeFkStrt_Mul) = lrr.fkStretch(
													ctrl=self.ToeFk_Ctrl,
													target=self.ToeFk_Jnt
												)
		self.ToeFkStrt_Add.name = 'ToeFkStrt%s%sAdd' % (part, side)
		self.ToeFkStrt_Mul.name = 'ToeFkStrt%s%sMul' % (part, side)
		
		# FK rig - Connecting to joints
		mc.parentConstraint(self.UpLegFkGmbl_Ctrl, self.UpLegFk_Jnt)
		mc.parentConstraint(self.MidLegFkGmbl_Ctrl, self.MidLegFk_Jnt)
		mc.parentConstraint(self.LowLegFkGmbl_Ctrl, self.LowLegFk_Jnt)
		mc.parentConstraint(self.AnkleFkGmbl_Ctrl, self.AnkleFk_Jnt)
		mc.parentConstraint(self.ToeFkGmbl_Ctrl, self.BallFk_Jnt)

		# FK rig - local/world setup
		(self.UpLegFkCtrlLoc_Grp,
		self.UpLegFkCtrlWor_Grp,
		self.UpLegFkCtrlWor_Grp_orientConstraint,
		self.UpLegFkCtrlZr_Grp_orientConstraint,
		self.UpLegFkCtrlZrGrpOri_Rev) = lrr.oriLocWor(
															self.UpLegFk_Ctrl,
															self.LegFkCtrl_Grp,
															ctrlGrp,
															self.UpLegFkCtrlZr_Grp
														)

		self.UpLegFkCtrlLoc_Grp.name = 'UpLegFkCtrlLoc%s%sGrp' % (part, side)
		self.UpLegFkCtrlWor_Grp.name = 'UpLegFkCtrlWor%s%sGrp' % (part, side)
		self.UpLegFkCtrlZrGrpOri_Rev.name = 'UpLegFkCtrlZrGrpOri%s%sRev' % (part, side)

		# IK rig
		# IK rig - Main groups
		self.LegIkCtrl_Grp = lrc.Null()
		self.LegIkCtrl_Grp.name = 'LegIkCtrl%s%sGrp' % (part, side)
		self.LegIkCtrl_Grp.snap(self.Ctrl_Grp)
		self.LegIkCtrl_Grp.parent(self.Ctrl_Grp)
		
		self.LegIkJnt_Grp = lrc.Null()
		self.LegIkJnt_Grp.name = 'LegIkJnt%s%sGrp' % (part, side)
		self.LegIkJnt_Grp.snap(self.Jnt_Grp)
		self.LegIkJnt_Grp.parent(self.Jnt_Grp)

		# IK rig - Joints
		self.UpLegIk_Jnt = lrr.jointAt(self.UpLeg_Jnt)
		self.MidLegIk_Jnt = lrr.jointAt(self.MidLeg_Jnt)
		self.LowLegIk_Jnt = lrr.jointAt(self.LowLeg_Jnt)
		self.AnkleIk_Jnt = lrr.jointAt(self.Ankle_Jnt)
		self.BallIk_Jnt = lrr.jointAt(self.Ball_Jnt)
		self.ToeIk_Jnt = lrr.jointAt(self.Toe_Jnt)

		self.UpLegIk_Jnt.name = 'UpLegIk%s%sJnt' % (part, side)
		self.MidLegIk_Jnt.name = 'MidLegIk%s%sJnt' % (part, side)
		self.LowLegIk_Jnt.name = 'LowLegIk%s%sJnt' % (part, side)
		self.AnkleIk_Jnt.name = 'AnkleIk%s%sJnt' % (part, side)
		self.BallIk_Jnt.name = 'BallIk%s%sJnt' % (part, side)
		self.ToeIk_Jnt.name = 'ToeIk%s%sJnt' % (part, side)

		self.ToeIk_Jnt.parent(self.BallIk_Jnt)
		self.BallIk_Jnt.parent(self.AnkleIk_Jnt)
		self.AnkleIk_Jnt.parent(self.LowLegIk_Jnt)
		self.LowLegIk_Jnt.parent(self.MidLegIk_Jnt)
		self.MidLegIk_Jnt.parent(self.UpLegIk_Jnt)
		self.UpLegIk_Jnt.parent(self.LegIkJnt_Grp)
		self.BallIk_Jnt.attr('ssc').v = 0

		self.UpLegIk_Jnt.rotateOrder = 'yzx'
		self.MidLegIk_Jnt.rotateOrder = 'yzx'
		self.LowLegIk_Jnt.rotateOrder = 'yzx'
		self.AnkleIk_Jnt.rotateOrder = 'yzx'
		self.BallIk_Jnt.rotateOrder = 'yzx'
		self.ToeIk_Jnt.rotateOrder = 'yzx'
		
		# IK rig - L joints
		self.UpLegIkL_Jnt = lrr.jointAt(self.UpLeg_Jnt)
		self.MidLegIkL_Jnt = lrr.jointAt(self.MidLeg_Jnt)
		self.LowLegIkL_Jnt = lrr.jointAt(self.LowLeg_Jnt)
		self.AnkleIkL_Jnt = lrr.jointAt(self.Ankle_Jnt)

		self.UpLegIkL_Jnt.name = 'UpLegIkL%s%sJnt' % (part, side)
		self.MidLegIkL_Jnt.name = 'MidLegIkL%s%sJnt' % (part, side)
		self.LowLegIkL_Jnt.name = 'LowLegIkL%s%sJnt' % (part, side)
		self.AnkleIkL_Jnt.name = 'AnkleIkL%s%sJnt' % (part, side)
		# self.KneeLegIkL_Jnt.name = 'KneeLegIkL%s%sJnt' % (part, side)
		# self.BackKneeLegIkL_Jnt.name = 'BackKneeLegIkL%s%sJnt' % (part, side)

		self.AnkleIkL_Jnt.parent(self.LowLegIkL_Jnt)
		self.LowLegIkL_Jnt.parent(self.MidLegIkL_Jnt)
		self.MidLegIkL_Jnt.parent(self.UpLegIkL_Jnt)
		self.UpLegIkL_Jnt.parent(self.LegIkJnt_Grp)

		self.UpLegIk_Jnt.rotateOrder = 'yzx'
		self.MidLegIk_Jnt.rotateOrder = 'yzx'
		self.LowLegIk_Jnt.rotateOrder = 'yzx'
		self.AnkleIk_Jnt.rotateOrder = 'yzx'
		self.BallIk_Jnt.rotateOrder = 'yzx'
		self.ToeIk_Jnt.rotateOrder = 'yzx'
		
		self.UpLegIkL_Jnt.rotateOrder = 'yzx'
		self.MidLegIkL_Jnt.rotateOrder = 'yzx'
		self.LowLegIkL_Jnt.rotateOrder = 'yzx'
		self.AnkleIkL_Jnt.rotateOrder = 'yzx'

		# IK rig - Controllers
		self.LegIkRoot_Ctrl = lrr.jointControl('cube')
		self.LegIkRoot_Ctrl.name = 'LegIkRoot%s%sCtrl' % (part, side)
		self.LegIkRoot_Ctrl.lockHideAttrs('rx', 'ry', 'rz', 'sx', 'sy', 'sz', 'v')
		
		self.LegIkRootCtrlZr_Grp = lrc.group(self.LegIkRoot_Ctrl)
		self.LegIkRootCtrlZr_Grp.name = 'LegIkRootCtrlZr%s%sGrp' % (part, side)
		
		self.LegIk_Ctrl = lrr.jointControl('cube')
		self.LegIk_Ctrl.name = 'LegIk%s%sCtrl' % (part, side)
		self.LegIk_Ctrl.lockHideAttrs('sx', 'sy', 'sz', 'v')
		self.LegIk_Ctrl.rotateOrder = 'xzy'

		self.LegIkCtrlLock_Grp = lrc.group(self.LegIk_Ctrl)
		self.LegIkCtrlLock_Grp.name = 'LegIkCtrlLock%s%sGrp' % (part, side)

		self.LegIkCtrlZr_Grp = lrc.group(self.LegIkCtrlLock_Grp)
		self.LegIkCtrlZr_Grp.name = 'LegIkCtrlZr%s%sGrp' % (part, side)
		
		self.KneeIk_Ctrl = lrc.Control('sphere')
		self.KneeIk_Ctrl.name = 'KneeIk%s%sCtrl' % (part, side)
		self.KneeIk_Ctrl.lockHideAttrs('rx', 'ry', 'rz', 'sx', 'sy', 'sz', 'v')

		self.KneeIkCtrlZr_Grp = lrc.group(self.KneeIk_Ctrl)
		self.KneeIkCtrlZr_Grp.name = 'KneeIkCtrlZr%s%sGrp' % (part, side)

		self.UpKneeIk_Ctrl = lrc.Control('sphere')
		self.UpKneeIk_Ctrl.name = 'UpKneeIk%s%sCtrl' % (part, side)
		self.UpKneeIk_Ctrl.lockHideAttrs('rx', 'ry', 'rz', 'sx', 'sy', 'sz', 'v')

		self.UpKneeIkCtrlZr_Grp = lrc.group(self.UpKneeIk_Ctrl)
		self.UpKneeIkCtrlZr_Grp.name = 'UpKneeIkCtrlZr%s%sGrp' % (part, side)

		self.LowKneeIk_Ctrl = lrc.Control('sphere')
		self.LowKneeIk_Ctrl.name = 'LowKneeIk%s%sCtrl' % (part, side)
		self.LowKneeIk_Ctrl.lockHideAttrs('rx', 'ry', 'rz', 'sx', 'sy', 'sz', 'v')

		self.LowKneeIkCtrlZr_Grp = lrc.group(self.LowKneeIk_Ctrl)
		self.LowKneeIkCtrlZr_Grp.name = 'LowKneeIkCtrlZr%s%sGrp' % (part, side)

		self.KneeIk_Ctrl.add(ln='kneeCtrlVis', min=0, max=1, k=True)
		self.KneeIk_Ctrl.attr('kneeCtrlVis') >> self.LowKneeIkCtrlZr_Grp.attr('v')
		self.KneeIk_Ctrl.attr('kneeCtrlVis') >> self.UpKneeIkCtrlZr_Grp.attr('v')

		# IK rig - Controllers - Color
		if side == '_R_':
			self.LegIkRoot_Ctrl.color = 'blue'
			self.LegIk_Ctrl.color = 'blue'
			self.KneeIk_Ctrl.color = 'blue'
		else:
			self.LegIkRoot_Ctrl.color = 'red'
			self.LegIk_Ctrl.color = 'red'
			self.KneeIk_Ctrl.color = 'red'

		self.UpKneeIk_Ctrl.color = 'softBlue'
		self.LowKneeIk_Ctrl.color = 'softBlue'

		# IK rig - Controllers - Positioning
		self.LegIkRootCtrlZr_Grp.snapPoint(self.UpLeg_Jnt)
		
		self.LegIkCtrlZr_Grp.snapPoint(self.Ankle_Jnt)
		self.LegIk_Ctrl.snapOrient(self.Ankle_Jnt)
		self.LegIk_Ctrl.freeze(t = False, r = True, s = False)
		
		self.LegIkCon_Ctrl = lrc.addConCtrl(self.LegIk_Ctrl)
		self.LegIkGmbl_Ctrl = lrc.addGimbal(self.LegIk_Ctrl)

		self.LegIkCon_Ctrl.name = 'LegIkCon%s%sCtrl' % (part, side)
		self.LegIkGmbl_Ctrl.name = 'LegIkGmbl%s%sCtrl' % (part, side)
		self.LegIkGmbl_Ctrl.rotateOrder = 'xzy'
		
		self.KneeIkCtrlZr_Grp.snapPoint(self.KneeIk_Jnt)
		self.UpKneeIkCtrlZr_Grp.snapPoint(self.UpKneeIk_Jnt)
		self.LowKneeIkCtrlZr_Grp.snapPoint(self.LowKneeIk_Jnt)
		
		self.LegIkRootCtrlZr_Grp.parent(self.LegIkCtrl_Grp)
		self.LegIkCtrlZr_Grp.parent(self.LegIkRoot_Ctrl)
		self.KneeIkCtrlZr_Grp.parent(self.LegIkRoot_Ctrl)
		self.UpKneeIkCtrlZr_Grp.parent(self.LegIkRoot_Ctrl)
		self.LowKneeIkCtrlZr_Grp.parent(self.LegIkRoot_Ctrl)
		
		mc.parentConstraint(self.MidLegIkL_Jnt, self.UpKneeIkCtrlZr_Grp, mo=True)
		mc.parentConstraint(self.LowLegIkL_Jnt, self.LowKneeIkCtrlZr_Grp, mo=True)
		
		# IK rig - Knee curve
		(self.KneeIkCtrl_Crv,
		self.KneeIkCtrl1_Cls,
		self.KneeIkCtrl2_Cls) = lrr.crvGuide(ctrl=self.KneeIk_Ctrl, target=self.LowLegIk_Jnt)

		self.KneeIkCtrl_Crv.name = 'KneeIkCtrl%s%sCrv' % (part, side)
		self.KneeIkCtrl1_Cls.name = 'KneeIkCtrl1%s%sCls' % (part, side)
		self.KneeIkCtrl2_Cls.name = 'KneeIkCtrl2%s%sCls' % (part, side)

		lrr.renameClsElms(self.KneeIkCtrl1_Cls)
		lrr.renameClsElms(self.KneeIkCtrl2_Cls)

		self.KneeIkCtrl_Crv.attr('inheritsTransform').value = 0
		self.KneeIkCtrl_Crv.attr('overrideEnabled').value = 1
		self.KneeIkCtrl_Crv.attr('overrideDisplayType').value = 2
		
		self.KneeIkCtrl_Crv.parent(self.LegIkCtrl_Grp)
		self.KneeIkCtrl_Crv.attr('t').value = (0,0,0)
		self.KneeIkCtrl_Crv.attr('r').value = (0,0,0)

		(self.UpKneeIkCtrl_Crv,
		self.UpKneeIkCtrl1_Cls,
		self.UpKneeIkCtrl2_Cls) = lrr.crvGuide(ctrl=self.UpKneeIk_Ctrl, target=self.MidLegIk_Jnt)

		self.UpKneeIkCtrl_Crv.name = 'UpKneeIkCtrl%s%sCrv' % (part, side)
		self.UpKneeIkCtrl1_Cls.name = 'UpKneeIkCtrl1%s%sCls' % (part, side)
		self.UpKneeIkCtrl2_Cls.name = 'UpKneeIkCtrl2%s%sCls' % (part, side)

		lrr.renameClsElms(self.UpKneeIkCtrl1_Cls)
		lrr.renameClsElms(self.UpKneeIkCtrl2_Cls)

		self.UpKneeIkCtrl_Crv.attr('inheritsTransform').value = 0
		self.UpKneeIkCtrl_Crv.attr('overrideEnabled').value = 1
		self.UpKneeIkCtrl_Crv.attr('overrideDisplayType').value = 2
		
		self.UpKneeIkCtrl_Crv.parent(self.LegIkCtrl_Grp)
		self.UpKneeIkCtrl_Crv.attr('t').value = (0,0,0)
		self.UpKneeIkCtrl_Crv.attr('r').value = (0,0,0)

		(self.LowKneeIkCtrl_Crv,
		self.LowKneeIkCtrl1_Cls,
		self.LowKneeIkCtrl2_Cls) = lrr.crvGuide(ctrl=self.LowKneeIk_Ctrl, target=self.LowLegIk_Jnt)
		
		self.LowKneeIkCtrl_Crv.name = 'LowKneeIkCtrl%s%sCrv' % (part, side)
		self.LowKneeIkCtrl1_Cls.name = 'LowKneeIkCtrl1%s%sCls' % (part, side)
		self.LowKneeIkCtrl2_Cls.name = 'LowKneeIkCtrl2%s%sCls' % (part, side)
		
		lrr.renameClsElms(self.LowKneeIkCtrl1_Cls)
		lrr.renameClsElms(self.LowKneeIkCtrl2_Cls)
		
		self.LowKneeIkCtrl_Crv.attr('inheritsTransform').value = 0
		self.LowKneeIkCtrl_Crv.attr('overrideEnabled').value = 1
		self.LowKneeIkCtrl_Crv.attr('overrideDisplayType').value = 2
		
		self.LowKneeIkCtrl_Crv.parent(self.LegIkCtrl_Grp)
		self.LowKneeIkCtrl_Crv.attr('t').value = (0,0,0)
		self.LowKneeIkCtrl_Crv.attr('r').value = (0,0,0)
		
		self.KneeIk_Ctrl.attr('kneeCtrlVis') >> self.UpKneeIkCtrl_Crv.attr('v')
		self.KneeIk_Ctrl.attr('kneeCtrlVis') >> self.LowKneeIkCtrl_Crv.attr('v')
		
		# IK rig - local/world setup
		(self.LegIkCtrlLoc_Grp,
		self.LegIkCtrlWor_Grp,
		self.LegIkCtrlWor_Grp_parentConstraint,
		self.LegIkCtrlZr_Grp_parentConstraint,
		self.LegIkCtrlZrGrpPar_Rev) = lrr.parLocWor(
														self.LegIk_Ctrl,
														self.LegIkRoot_Ctrl,
														ctrlGrp,
														self.LegIkCtrlZr_Grp
													)
		self.LegIkCtrlLoc_Grp.name = 'LegIkCtrlLoc%s%sGrp' % (part, side)
		self.LegIkCtrlWor_Grp.name = 'LegIkCtrlWor%s%sGrp' % (part, side)
		self.LegIkCtrlZrGrpPar_Rev.name = 'LegIkCtrlZrGrpPar%s%sRev' % (part, side)

		(self.KneeIkCtrlLoc_Grp,
		self.KneeIkCtrlWor_Grp,
		self.KneeIkCtrlWor_Grp_parentConstraint,
		self.KneeIkCtrlZr_Grp_parentConstraint,
		self.KneeIkCtrlZrGrpPar_Rev) = lrr.parLocWor(
														self.KneeIk_Ctrl,
														self.LegIkGmbl_Ctrl,
														ctrlGrp,
														self.KneeIkCtrlZr_Grp
													)
		self.KneeIkCtrlLoc_Grp.name = 'KneeIkCtrlLoc%s%sGrp' % (part, side)
		self.KneeIkCtrlWor_Grp.name = 'KneeIkCtrlWor%s%sGrp' % (part, side)
		self.KneeIkCtrlZrGrpPar_Rev.name = 'KneeIkCtrlZrGrpPar%s%sRev' % (part, side)
		
		# IK rig - IK handles
		self.LegIkL_Ikh = lrc.IkRp(sj=self.UpLegIkL_Jnt, ee=self.AnkleIkL_Jnt)
		
		self.LegIk_Ctrl.add(ln = 'twist', k = True)
		
		self.LegIkTwsOfst_Add = lrc.AddDoubleLinear()
		self.LegIkTwsOfst_Add.name = 'LegIkTwsOfst%s%sAdd' % (part, side)
		
		self.LegIkTwsOfst_Add.add(ln='offset', k=True, dv=0)
		self.LegIkTwsOfst_Add.attr('offset') >> self.LegIkTwsOfst_Add.attr('i1')
		self.LegIk_Ctrl.attr('twist') >> self.LegIkTwsOfst_Add.attr('i2')
		self.LegIkTwsOfst_Add.attr('o') >> self.LegIkL_Ikh.attr('twist')

		self.LowLegIk_Ikh = lrc.IkRp(sj=self.UpLegIk_Jnt, ee=self.LowLegIk_Jnt)
		self.AnkleIk_Ikh = lrc.IkRp(sj=self.LowLegIk_Jnt, ee=self.AnkleIk_Jnt)
		self.BallIk_Ikh = lrc.IkRp(sj=self.AnkleIk_Jnt, ee=self.BallIk_Jnt)
		self.ToeIk_Ikh = lrc.IkRp(sj=self.BallIk_Jnt, ee=self.ToeIk_Jnt)

		self.LegIkL_Ikh.name = 'LegIkL%s%sIkh' % (part, side)
		self.LowLegIk_Ikh.name = 'LowLegIk%s%sIkh' % (part, side)
		self.AnkleIk_Ikh.name = 'AnkleIk%s%sIkh' % (part, side)
		self.BallIk_Ikh.name = 'BallIk%s%sIkh' % (part, side)
		self.ToeIk_Ikh.name = 'ToeIk%s%sIkh' % (part, side)

		mc.poleVectorConstraint(self.KneeIk_Ctrl, self.LegIkL_Ikh)
		mc.poleVectorConstraint(self.UpKneeIk_Ctrl, self.LowLegIk_Ikh)

		if abs(self.UpLegIkL_Jnt.attr('ry').v) > 10 :
			self.LegIkTwsOfst_Add.attr('offset').v = 180

		mc.poleVectorConstraint(self.LowKneeIk_Ctrl, self.AnkleIk_Ikh)

		self.LegIk_Ctrl.add(ln = 'upTwist', k = True)
		
		self.LegUpIkTwsOfst_Add = lrc.AddDoubleLinear()
		self.LegUpIkTwsOfst_Add.name = 'LegUpIkTwsOfst%s%sAdd' % (part, side)
		
		self.LegUpIkTwsOfst_Add.add(ln='offset', k=True, dv=0)
		self.LegUpIkTwsOfst_Add.attr('offset') >> self.LegUpIkTwsOfst_Add.attr('i1')
		self.LegIk_Ctrl.attr('upTwist') >> self.LegUpIkTwsOfst_Add.attr('i2')
		self.LegUpIkTwsOfst_Add.attr('o') >> self.LowLegIk_Ikh.attr('twist')
		
		self.LegIkh_Grp = lrc.Null()
		self.LegIkh_Grp.name = 'LegIkh%s%sGrp' % (part, side)

		self.LegIkLIkhZr_Grp = lrr.groupAt(self.LegIkL_Ikh )
		self.LowLegIkIkhZr_Grp = lrr.groupAt(self.LowLegIk_Ikh)
		self.AnkleIkIkhZr_Grp = lrr.groupAt(self.AnkleIk_Ikh)
		self.BallIkIkhZr_Grp = lrr.groupAt(self.BallIk_Ikh )
		self.ToeIkIkhZr_Grp = lrr.groupAt(self.ToeIk_Ikh )

		self.LegIkLIkhZr_Grp.name = 'LegIkLIkhZr%s%sGrp' % (part, side)
		self.LowLegIkIkhZr_Grp.name = 'LowLegIkIkhZr%s%sGrp' % (part, side)
		self.AnkleIkIkhZr_Grp.name = 'AnkleIkIkhZr%s%sGrp' % (part, side)
		self.BallIkIkhZr_Grp.name = 'BallIkIkhZr%s%sGrp' % (part, side)
		self.ToeIkIkhZr_Grp.name = 'ToeIkIkhZr%s%sGrp' % (part, side)
		
		self.LegIkLIkhZr_Grp.parent(self.LegIkh_Grp)
		self.LowLegIkIkhZr_Grp.parent(self.LegIkh_Grp)
		self.AnkleIkIkhZr_Grp.parent(self.LegIkh_Grp)
		self.BallIkIkhZr_Grp.parent(self.LegIkh_Grp)
		self.ToeIkIkhZr_Grp.parent(self.LegIkh_Grp)
		self.LegIkh_Grp.parent(ikhGrp)

		# IK rig - IK handles - Pivots
		self.LegFlexIkPiv_Grp = lrc.Null()
		self.LegFlexIkPivZr_Grp = lrc.group(self.LegFlexIkPiv_Grp)

		self.AnkleRollIkPiv_Grp = lrc.Null()
		self.AnkleRollIk_Grp = lrc.Null()
		self.ToeBendIkPiv_Grp = lrc.Null()
		self.FootInIkPiv_Grp = lrc.Null()
		self.FootOutIkPiv_Grp = lrc.Null()
		self.HeelIkPiv_Grp = lrc.Null()
		self.ToeIkPiv_Grp = lrc.Null()
		self.LegIkPiv_Grp = lrc.Null()

		self.AnkleRollIkPivZr_Grp = lrc.group(self.AnkleRollIkPiv_Grp )
		self.AnkleRollIkZr_Grp = lrc.group(self.AnkleRollIk_Grp)
		self.ToeBendIkPivZr_Grp = lrc.group(self.ToeBendIkPiv_Grp)
		self.FootInIkPivZr_Grp = lrc.group(self.FootInIkPiv_Grp)
		self.FootOutIkPivZr_Grp = lrc.group(self.FootOutIkPiv_Grp)
		self.HeelIkPivZr_Grp = lrc.group(self.HeelIkPiv_Grp)
		self.ToeIkPivZr_Grp = lrc.group(self.ToeIkPiv_Grp)
		self.LegIkPivZr_Grp = lrc.group(self.LegIkPiv_Grp)
		
		self.LegIkLIkhPiv_Grp = lrc.Null()
		self.LowLegIkIkhPiv_Grp = lrc.Null()
		self.AnkleIkIkhPiv_Grp = lrc.Null()
		self.BallIkIkhPiv_Grp = lrc.Null()
		self.ToeIkIkhPiv_Grp = lrc.Null()
		
		self.LegFlexIkPiv_Grp.name = 'LegFlexIkPiv%s%sGrp' % (part, side)
		self.LegFlexIkPivZr_Grp.name = 'LegFlexIkPivZr%s%sGrp' % (part, side)
		self.AnkleRollIkPiv_Grp.name = 'AnkleRollIkPiv%s%sGrp' % (part, side)
		self.AnkleRollIk_Grp.name = 'AnkleRollIk%s%sGrp' % (part, side)
		self.ToeBendIkPiv_Grp.name = 'ToeBendIkPiv%s%sGrp' % (part, side)
		self.FootInIkPiv_Grp.name = 'FootInIkPiv%s%sGrp' % (part, side)
		self.FootOutIkPiv_Grp.name = 'FootOutIkPiv%s%sGrp' % (part, side)
		self.HeelIkPiv_Grp.name = 'HeelIkPiv%s%sGrp' % (part, side)
		self.ToeIkPiv_Grp.name = 'ToeIkPiv%s%sGrp' % (part, side)
		self.LegIkPiv_Grp.name = 'LegIkPiv%s%sGrp' % (part, side)
		self.AnkleRollIkPivZr_Grp.name = 'AnkleRollIkPivZr%s%sGrp' % (part, side)
		self.AnkleRollIkZr_Grp.name = 'AnkleRollIkZr%s%sGrp' % (part, side)
		self.ToeBendIkPivZr_Grp.name = 'ToeBendIkPivZr%s%sGrp' % (part, side)
		self.FootInIkPivZr_Grp.name = 'FootInIkPivZr%s%sGrp' % (part, side)
		self.FootOutIkPivZr_Grp.name = 'FootOutIkPivZr%s%sGrp' % (part, side)
		self.HeelIkPivZr_Grp.name = 'HeelIkPivZr%s%sGrp' % (part, side)
		self.ToeIkPivZr_Grp.name = 'ToeIkPivZr%s%sGrp' % (part, side)
		self.LegIkPivZr_Grp.name = 'LegIkPivZr%s%sGrp' % (part, side)
		self.LegIkLIkhPiv_Grp.name = 'LegIkLIkhPiv%s%sGrp' % (part, side)
		self.LowLegIkIkhPiv_Grp.name = 'LowLegIkIkhPiv%s%sGrp' % (part, side)
		self.AnkleIkIkhPiv_Grp.name = 'AnkleIkIkhPiv%s%sGrp' % (part, side)
		self.BallIkIkhPiv_Grp.name = 'BallIkIkhPiv%s%sGrp' % (part, side)
		self.ToeIkIkhPiv_Grp.name = 'ToeIkIkhPiv%s%sGrp' % (part, side)
		
		# IK rig - IK handles - positioning
		self.LegFlexIkPivZr_Grp.snap(self.Ankle_Jnt)
		self.LegFlexIkPivZr_Grp.snapOrient(self.LowLeg_Jnt)
		self.AnkleRollIkPivZr_Grp.snap(self.Ball_Jnt)
		self.AnkleRollIkZr_Grp.snap(self.Ball_Jnt)
		self.ToeBendIkPivZr_Grp.snap(self.Ball_Jnt)
		self.FootInIkPivZr_Grp.snap(self.FootIn_Jnt)
		self.FootOutIkPivZr_Grp.snap(self.FootOut_Jnt)
		self.HeelIkPivZr_Grp.snap(self.Heel_Jnt)
		self.ToeIkPivZr_Grp.snap(self.Toe_Jnt)
		self.LegIkPivZr_Grp.snap(self.Ankle_Jnt)
		
		self.LegIkLIkhPiv_Grp.snap(self.LegIkLIkhZr_Grp)
		self.LowLegIkIkhPiv_Grp.snap(self.LowLegIkIkhZr_Grp)
		self.AnkleIkIkhPiv_Grp.snap(self.AnkleIkIkhZr_Grp)
		self.BallIkIkhPiv_Grp.snap(self.BallIkIkhZr_Grp)
		self.ToeIkIkhPiv_Grp.snap(self.ToeIkIkhZr_Grp)
		
		self.LegFlexIkPivZr_Grp.parent(self.LowLegIkL_Jnt)
		self.AnkleRollIkPivZr_Grp.parent(self.FootInIkPiv_Grp)
		self.AnkleRollIkZr_Grp.parent(self.AnkleRollIkPiv_Grp)
		self.ToeBendIkPivZr_Grp.parent(self.FootInIkPiv_Grp)
		self.FootInIkPivZr_Grp.parent(self.FootOutIkPiv_Grp)
		self.FootOutIkPivZr_Grp.parent(self.HeelIkPiv_Grp)
		self.HeelIkPivZr_Grp.parent(self.ToeIkPiv_Grp)
		self.ToeIkPivZr_Grp.parent(self.LegIkPiv_Grp)
		self.LegIkPivZr_Grp.parent(self.LegIkGmbl_Ctrl)

		self.LegIkLIkhPiv_Grp.parent(self.AnkleRollIk_Grp)
		self.LowLegIkIkhPiv_Grp.parent(self.LegFlexIkPiv_Grp)
		self.AnkleIkIkhPiv_Grp.parent(self.AnkleIkL_Jnt)
		self.BallIkIkhPiv_Grp.parent(self.AnkleRollIk_Grp)
		self.ToeIkIkhPiv_Grp.parent(self.ToeBendIkPiv_Grp)
		
		mc.pointConstraint(self.LegIkRoot_Ctrl, self.UpLegIk_Jnt)
		mc.pointConstraint(self.LegIkRoot_Ctrl, self.UpLegIkL_Jnt)
		
		mc.parentConstraint(self.LegIkLIkhPiv_Grp, self.LegIkLIkhZr_Grp)
		mc.parentConstraint(self.LowLegIkIkhPiv_Grp, self.LowLegIkIkhZr_Grp)
		mc.parentConstraint(self.AnkleIkIkhPiv_Grp, self.AnkleIkIkhZr_Grp)
		mc.parentConstraint(self.BallIkIkhPiv_Grp, self.BallIkIkhZr_Grp)
		mc.parentConstraint(self.ToeIkIkhPiv_Grp, self.ToeIkIkhZr_Grp)

		# IK rig - Stretch attributes
		self.LegIk_Ctrl.add(ln='__stretch__', k=True)
		self.LegIk_Ctrl.attr('__stretch__').set(l=True)
		self.LegIk_Ctrl.add(ln='autoStretch', min=0, max=1, k=True)
		self.LegIk_Ctrl.add(ln='upLegStretch', k=True)
		self.LegIk_Ctrl.add(ln='midLegStretch', k=True)
		self.LegIk_Ctrl.add(ln='lowLegStretch', k=True)
		self.LegIk_Ctrl.add(ln='toeStretch', k=True)

		self.LegIk_Ctrl.add(ln='__foot__', k=True)
		self.LegIk_Ctrl.attr('__foot__').set(l=True)
		
		# IK rig - Foot roll attributes
		attrs = ('legFlex', 'legBend', 'heelRoll', 'ballRoll', 
					'toeRoll', 'heelTwist', 'toeTwist', 'footRock', 'toeBend')
		for attr in attrs :
			self.LegIk_Ctrl.add(ln=attr, k=True)

		self.BallRollColl_Pma = lrc.PlusMinusAverage()
		self.BallRollColl_Pma.name = 'BallRollColl%s%sPma' % (part, side)

		self.ToeRollColl_Pma = lrc.PlusMinusAverage()
		self.ToeRollColl_Pma.name = 'ToeRollColl%s%sPma' % (part, side)

		self.HeelRollColl_Pma = lrc.PlusMinusAverage()
		self.HeelRollColl_Pma.name = 'HeelRollColl%s%sPma' % (part, side)

		self.LegIk_Ctrl.attr('legFlex') >> self.LegFlexIkPiv_Grp.attr('rx')
		self.LegIk_Ctrl.attr('legBend') >> self.LegFlexIkPiv_Grp.attr('rz')
		# self.LegIk_Ctrl.attr('heelRoll') >> self.HeelIkPiv_Grp.attr('rx')
		# self.LegIk_Ctrl.attr('ballRoll') >> self.AnkleRollIkPiv_Grp.attr('rx')
		# self.LegIk_Ctrl.attr('toeRoll') >> self.ToeIkPiv_Grp.attr('rx')
		self.LegIk_Ctrl.attr('heelTwist') >> self.HeelIkPiv_Grp.attr('rz')
		self.LegIk_Ctrl.attr('toeTwist') >> self.ToeIkPiv_Grp.attr('rz')
		self.LegIk_Ctrl.attr('toeBend') >> self.ToeBendIkPiv_Grp.attr('rx')
		
		self.LegIk_Ctrl.attr('footRock') >> self.FootInIkPiv_Grp.attr('ry')
		self.LegIk_Ctrl.attr('footRock') >> self.FootOutIkPiv_Grp.attr('ry')

		self.FootOutIkPiv_Grp.attr('xrye').value = 1
		self.FootOutIkPiv_Grp.attr('xryl').value = 0
		self.FootInIkPiv_Grp.attr('mrye').value = 1
		self.FootInIkPiv_Grp.attr('mryl').value = 0

		# IK rig - Roll attributes - BallRoll
		legIkCtrlShp = lrc.Dag(self.LegIk_Ctrl.shape)
		legIkCtrlShp.add(ln='toeRollBreak', k=True, dv=-60)

		self.BallRollIk_Clamp = lrc.Clamp()
		self.BallRollIk_Clamp.name = 'BallRollIk%s%sClamp' % (part, side)

		self.LegIk_Ctrl.attr('ballRoll') >> self.BallRollIk_Clamp.attr('inputR')
		legIkCtrlShp.attr('toeRollBreak') >> self.BallRollIk_Clamp.attr('minR')
		self.BallRollIk_Clamp.attr('outputR') >> self.AnkleRollIkPiv_Grp.attr('rx')

		# IK rig - Roll attributes - ToeRoll
		self.ToeRollIk_Clamp = lrc.Clamp()
		self.ToeRollIk_Clamp.name = 'ToeRollIk%s%sClamp' % (part, side)

		self.ToeIkPivColl_Pma = lrc.PlusMinusAverage()
		self.ToeIkPivColl_Pma.name = 'ToeIkPivColl%s%sPma' % (part, side)

		self.ToeIkPivInv_Mdv = lrc.MultiplyDivide()
		self.ToeIkPivInv_Mdv.name = 'ToeIkPivInv%s%sMdv' % (part, side)
		self.ToeIkPivInv_Mdv.attr('i2x').v = -1

		self.LegIk_Ctrl.attr('ballRoll') >> self.ToeRollIk_Clamp.attr('inputR')
		legIkCtrlShp.attr('toeRollBreak') >> self.ToeRollIk_Clamp.attr('maxR')
		self.ToeRollIk_Clamp.attr('minR').v = -180
		self.ToeRollIk_Clamp.attr('outputR') >> self.ToeIkPivColl_Pma.last1D()

		legIkCtrlShp.attr('toeRollBreak') >> self.ToeIkPivInv_Mdv.attr('i1x')
		self.ToeIkPivInv_Mdv.attr('ox') >> self.ToeIkPivColl_Pma.last1D()

		self.LegIk_Ctrl.attr('toeRoll') >> self.ToeIkPivColl_Pma.last1D()
		self.ToeIkPivColl_Pma.attr('output1D') >> self.ToeIkPiv_Grp.attr('rx')

		# IK rig - Roll attributes - HeelRoll
		self.HeelRollIk_Clamp = lrc.Clamp()
		self.HeelRollIk_Clamp.name = 'HeelRollIk%s%sClamp' % (part, side)

		self.HeelIkPivColl_Pma = lrc.PlusMinusAverage()
		self.HeelIkPivColl_Pma.name = 'HeelIkPivColl%s%sPma' % (part, side)
		
		self.HeelIkPivInv_Mdv = lrc.MultiplyDivide()
		self.HeelIkPivInv_Mdv.name = 'HeelIkPivInv%s%sMdv' % (part, side)
		self.HeelIkPivInv_Mdv.attr('i2x').v = 1

		self.LegIk_Ctrl.attr('ballRoll') >> self.HeelRollIk_Clamp.attr('inputR')
		self.HeelRollIk_Clamp.attr('maxR').v = 360
		self.HeelRollIk_Clamp.attr('outputR') >> self.HeelIkPivInv_Mdv.attr('i1x')
		self.HeelIkPivInv_Mdv.attr('ox') >> self.HeelIkPivColl_Pma.last1D()
		self.LegIk_Ctrl.attr('heelRoll') >> self.HeelIkPivColl_Pma.last1D()
		
		self.HeelIkPivColl_Pma.attr('output1D') >> self.HeelIkPiv_Grp.attr('rx')
		
		# IK rig - Stretch setup
		self.UpLegIkJntPnt_Grp = lrc.Null()
		self.LegIkCtrlPnt_Grp = lrc.Null()
		
		self.UpLegIkJntPnt_Grp.name = 'UpLegIkJntPnt%s%sGrp' % (part, side)
		self.LegIkCtrlPnt_Grp.name = 'LegIkCtrlPnt%s%sGrp' % (part, side)
		
		mc.pointConstraint(self.LegIkRoot_Ctrl, self.UpLegIkJntPnt_Grp)
		mc.pointConstraint(self.LegIkLIkhZr_Grp, self.LegIkCtrlPnt_Grp)
		
		self.UpLegIkJntPnt_Grp.parent(self.LegIkRoot_Ctrl)
		self.LegIkCtrlPnt_Grp.parent(self.LegIkRoot_Ctrl)
		
		self.LegIkAtStrt_Dist = lrc.DistanceBetween()
		self.LegIkAtStrt_Dist.name = 'LegIkAtStrt%s%sDist' % (part, side)

		self.UpLegIkJntPnt_Grp.attr('t') >> self.LegIkAtStrt_Dist.attr('p1')
		self.LegIkCtrlPnt_Grp.attr('t') >> self.LegIkAtStrt_Dist.attr('p2')
		
		self.LegIkAtStrt_Cnd = lrc.Condition()
		self.LegIkAtStrt_Mdv = lrc.MultiplyDivide()

		self.UpLegIkAtStrt_Mul = lrc.MultDoubleLinear()
		self.UpLegIkStretch_Mul = lrc.MultDoubleLinear()
		self.UpLegIkAtStrt_Add = lrc.AddDoubleLinear()
		self.UpLegIkAtStrt_Blnd = lrc.BlendTwoAttr()
		
		self.MidLegIkAtStrt_Mul = lrc.MultDoubleLinear()
		self.MidLegIkStretch_Mul = lrc.MultDoubleLinear()
		self.MidLegIkAtStrt_Add = lrc.AddDoubleLinear()
		self.MidLegIkAtStrt_Blnd = lrc.BlendTwoAttr()
		
		self.LowLegIkAtStrt_Mul = lrc.MultDoubleLinear()
		self.LowLegIkStretch_Mul = lrc.MultDoubleLinear()
		self.LowLegIkAtStrt_Add = lrc.AddDoubleLinear()
		self.LowLegIkAtStrt_Blnd = lrc.BlendTwoAttr()
		
		self.LegIkAtStrt_Cnd.name = 'LegIkAtStrt%s%sCnd' % (part, side)
		self.LegIkAtStrt_Mdv.name = 'LegIkAtStrt%s%sMdv' % (part, side)
		self.UpLegIkAtStrt_Mul.name = 'UpLegIkAtStrt%s%sMul' % (part, side)
		self.UpLegIkStretch_Mul.name = 'UpLegIkStretch%s%sMul' % (part, side)
		self.UpLegIkAtStrt_Add.name = 'UpLegIkAtStrt%s%sAdd' % (part, side)
		self.UpLegIkAtStrt_Blnd.name = 'UpLegIkAtStrt%s%sBlnd' % (part, side)
		self.MidLegIkAtStrt_Mul.name = 'MidLegIkAtStrt%s%sMul' % (part, side)
		self.MidLegIkStretch_Mul.name = 'MidLegIkStretch%s%sMul' % (part, side)
		self.MidLegIkAtStrt_Add.name = 'MidLegIkAtStrt%s%sAdd' % (part, side)
		self.MidLegIkAtStrt_Blnd.name = 'MidLegIkAtStrt%s%sBlnd' % (part, side)
		self.LowLegIkAtStrt_Mul.name = 'LowLegIkAtStrt%s%sMul' % (part, side)
		self.LowLegIkStretch_Mul.name = 'LowLegIkStretch%s%sMul' % (part, side)
		self.LowLegIkAtStrt_Add.name = 'LowLegIkAtStrt%s%sAdd' % (part, side)
		self.LowLegIkAtStrt_Blnd.name = 'LowLegIkAtStrt%s%sBlnd' % (part, side)
		
		# IK rig - Auto stretch setup
		upLegDist = abs(self.MidLegIk_Jnt.attr('ty').value)
		midLegDist = abs(self.LowLegIk_Jnt.attr('ty').value)
		lowLegDist = abs(self.AnkleIk_Jnt.attr('ty').value)
		ikCtrlDist = upLegDist + midLegDist + lowLegDist

		legIkCtrlShp.add(ln='atStrtDiv')
		legIkCtrlShp.attr('atStrtDiv').v = ikCtrlDist

		legIkCtrlShp.add(ln='atStrtCon')
		legIkCtrlShp.attr('atStrtCon').v = ikCtrlDist

		legIkCtrlShp.add(ln='upLegLen')
		legIkCtrlShp.attr('upLegLen').v = upLegDist

		legIkCtrlShp.add(ln='midLegLen')
		legIkCtrlShp.attr('midLegLen').v = midLegDist
		
		legIkCtrlShp.add(ln='lowLegLen')
		legIkCtrlShp.attr('lowLegLen').v = lowLegDist

		self.LegIkAtStrt_Dist.attr('d') >> self.LegIkAtStrt_Cnd.attr('ft')
		# self.LegIkAtStrt_Cnd.attr('st').value = ikCtrlDist
		legIkCtrlShp.attr('atStrtCon') >> self.LegIkAtStrt_Cnd.attr('st')
		self.LegIkAtStrt_Cnd.attr('op').value = 2
		self.LegIkAtStrt_Cnd.attr('cfr').value = 1
		
		self.LegIkAtStrt_Mdv.attr('operation').v = 2
		self.LegIkAtStrt_Dist.attr('d') >> self.LegIkAtStrt_Mdv.attr('i1x')
		# self.LegIkAtStrt_Mdv.attr('i2x').v = ikCtrlDist
		legIkCtrlShp.attr('atStrtDiv') >> self.LegIkAtStrt_Mdv.attr('i2x')
		self.LegIkAtStrt_Mdv.attr('ox') >> self.LegIkAtStrt_Cnd.attr('ctr')
		
		self.LegIkAtStrt_Cnd.attr('ocr') >> self.UpLegIkAtStrt_Mul.attr('i1')
		# self.UpLegIkAtStrt_Mul.attr('i2').value = upLegDist
		legIkCtrlShp.attr('upLegLen') >> self.UpLegIkAtStrt_Mul.attr('i2')
		self.LegIk_Ctrl.attr('autoStretch') >> self.UpLegIkAtStrt_Blnd.attr('ab')
		# self.UpLegIkAtStrt_Blnd.add(ln = 'default', dv = upLegDist, k = True)
		# self.UpLegIkAtStrt_Blnd.attr('default') >> self.UpLegIkAtStrt_Blnd.last()
		legIkCtrlShp.attr('upLegLen') >> self.UpLegIkAtStrt_Blnd.last()
		self.UpLegIkAtStrt_Mul.attr('o') >> self.UpLegIkAtStrt_Blnd.last()
		self.UpLegIkAtStrt_Blnd.attr('o') >> self.UpLegIkAtStrt_Add.attr('i1')
		self.LegIk_Ctrl.attr('upLegStretch') >> self.UpLegIkStretch_Mul.attr('i1')
		# self.UpLegIkStretch_Mul.attr('i2').value = upLegDist
		legIkCtrlShp.attr('upLegLen') >> self.UpLegIkStretch_Mul.attr('i2')
		self.UpLegIkStretch_Mul.attr('o') >> self.UpLegIkAtStrt_Add.attr('i2')
		
		self.LegIkAtStrt_Cnd.attr('ocr') >> self.MidLegIkAtStrt_Mul.attr('i1')
		self.MidLegIkAtStrt_Mul.attr('i2').value = midLegDist
		legIkCtrlShp.attr('midLegLen') >> self.MidLegIkAtStrt_Mul.attr('i2')
		self.LegIk_Ctrl.attr('autoStretch') >> self.MidLegIkAtStrt_Blnd.attr('ab')
		# self.MidLegIkAtStrt_Blnd.add(ln = 'default', dv = midLegDist, k = True)
		# self.MidLegIkAtStrt_Blnd.attr('default') >> self.MidLegIkAtStrt_Blnd.last()
		legIkCtrlShp.attr('midLegLen') >> self.MidLegIkAtStrt_Blnd.last()
		self.MidLegIkAtStrt_Mul.attr('o') >> self.MidLegIkAtStrt_Blnd.last()
		self.MidLegIkAtStrt_Blnd.attr('o') >> self.MidLegIkAtStrt_Add.attr('i1')
		self.LegIk_Ctrl.attr('midLegStretch') >> self.MidLegIkStretch_Mul.attr('i1')
		# self.MidLegIkStretch_Mul.attr('i2').value = midLegDist
		legIkCtrlShp.attr('midLegLen') >> self.MidLegIkStretch_Mul.attr('i2')
		self.MidLegIkStretch_Mul.attr('o') >> self.MidLegIkAtStrt_Add.attr('i2')
		
		self.LegIkAtStrt_Cnd.attr('ocr') >> self.LowLegIkAtStrt_Mul.attr('i1')
		# self.LowLegIkAtStrt_Mul.attr('i2').value = lowLegDist
		legIkCtrlShp.attr('lowLegLen') >> self.LowLegIkAtStrt_Mul.attr('i2')
		self.LegIk_Ctrl.attr('autoStretch') >> self.LowLegIkAtStrt_Blnd.attr('ab')
		# self.LowLegIkAtStrt_Blnd.add(ln = 'default', dv = lowLegDist, k = True)
		# self.LowLegIkAtStrt_Blnd.attr('default') >> self.LowLegIkAtStrt_Blnd.last()
		legIkCtrlShp.attr('lowLegLen') >> self.LowLegIkAtStrt_Blnd.last()
		self.LowLegIkAtStrt_Mul.attr('o') >> self.LowLegIkAtStrt_Blnd.last()
		self.LowLegIkAtStrt_Blnd.attr('o') >> self.LowLegIkAtStrt_Add.attr('i1')
		self.LegIk_Ctrl.attr('lowLegStretch') >> self.LowLegIkStretch_Mul.attr('i1')
		# self.LowLegIkStretch_Mul.attr('i2').value = lowLegDist
		legIkCtrlShp.attr('lowLegLen') >> self.LowLegIkStretch_Mul.attr('i2')
		self.LowLegIkStretch_Mul.attr('o') >> self.LowLegIkAtStrt_Add.attr('i2')

		# IK rig - Toe stretch
		(self.LegIkToeStretch_Add,
		self.LegIkToeStretch_Mul) = lrr.fkStretch(
														ctrl=self.LegIk_Ctrl,
														attr='toeStretch',
														target=self.ToeIk_Jnt
													)

		self.LegIkToeStretch_Add.name = 'LegIkToeStretch%s%sAdd' % (part, side)
		self.LegIkToeStretch_Mul.name = 'LegIkToeStretch%s%sMul' % (part, side)

		# IK rig - Auto stretch - Finalize amplifier
		self.UpLegIkFinStrtVal_Mul = lrc.MultDoubleLinear()
		self.MidLegIkFinStrtVal_Mul = lrc.MultDoubleLinear()
		self.LowLegIkFinStrtVal_Mul = lrc.MultDoubleLinear()

		self.UpLegIkFinStrtVal_Mul.name = 'UpLegIkFinStrtVal%s%sMul' % (part, side)
		self.MidLegIkFinStrtVal_Mul.name = 'MidLegIkFinStrtVal%s%sMul' % (part, side)
		self.LowLegIkFinStrtVal_Mul.name = 'LowLegIkFinStrtVal%s%sMul' % (part, side)

		if self.MidLegIk_Jnt.attr('ty').value > 0 :
			self.UpLegIkFinStrtVal_Mul.attr('i2').v = 1
		else :
			self.UpLegIkFinStrtVal_Mul.attr('i2').v = -1

		if self.LowLegIk_Jnt.attr('ty').value > 0 :
			self.MidLegIkFinStrtVal_Mul.attr('i2').v = 1
		else :
			self.MidLegIkFinStrtVal_Mul.attr('i2').v = -1

		if self.AnkleIk_Jnt.attr('ty').value > 0 :
			self.LowLegIkFinStrtVal_Mul.attr('i2').v = 1
		else :
			self.LowLegIkFinStrtVal_Mul.attr('i2').v = -1

		self.UpLegIkAtStrt_Add.attr('o') >> self.UpLegIkFinStrtVal_Mul.attr('i1')
		self.UpLegIkFinStrtVal_Mul.attr('o') >> self.MidLegIk_Jnt.attr('ty')
		self.UpLegIkFinStrtVal_Mul.attr('o') >> self.MidLegIkL_Jnt.attr('ty')

		self.MidLegIkAtStrt_Add.attr('o') >> self.MidLegIkFinStrtVal_Mul.attr('i1')
		self.MidLegIkFinStrtVal_Mul.attr('o') >> self.LowLegIk_Jnt.attr('ty')
		self.MidLegIkFinStrtVal_Mul.attr('o') >> self.LowLegIkL_Jnt.attr('ty')

		self.LowLegIkAtStrt_Add.attr('o') >> self.LowLegIkFinStrtVal_Mul.attr('i1')
		self.LowLegIkFinStrtVal_Mul.attr('o') >> self.AnkleIk_Jnt.attr('ty')
		self.LowLegIkFinStrtVal_Mul.attr('o') >> self.AnkleIkL_Jnt.attr('ty')

		# IK rig - Foot scale
		self.Leg_Ctrl.attr('footScale') >> self.AnkleIk_Jnt.attr('sx')
		self.Leg_Ctrl.attr('footScale') >> self.AnkleIk_Jnt.attr('sy')
		self.Leg_Ctrl.attr('footScale') >> self.AnkleIk_Jnt.attr('sz')
		self.Leg_Ctrl.attr('footScale') >> self.LegIkPiv_Grp.attr('sx')
		self.Leg_Ctrl.attr('footScale') >> self.LegIkPiv_Grp.attr('sy')
		self.Leg_Ctrl.attr('footScale') >> self.LegIkPiv_Grp.attr('sz')

		# FK/IK blending
		self.LegFkIk_Rev = lrc.Reverse()
		self.LegFkIk_Rev.name = 'LegFkIk%s%sRev' % (part, side)

		self.Leg_Ctrl.add(ln='fkIk', min=0, max=1, k=True)
		self.Leg_Ctrl.attr('fkIk') >> self.LegFkIk_Rev.attr('ix')
		self.Leg_Ctrl.attr('fkIk') >> self.LegIkCtrl_Grp.attr('v')
		self.LegFkIk_Rev.attr('ox') >> self.LegFkCtrl_Grp.attr('v')
		
		upLegParCons = lrc.parentConstraint(
												self.UpLegFk_Jnt,
												self.UpLegIk_Jnt,
												self.UpLeg_Jnt
											)
		midLegParCons = lrc.parentConstraint(
												self.MidLegFk_Jnt,
												self.MidLegIk_Jnt,
												self.MidLeg_Jnt
											)
		lowLegParCons = lrc.parentConstraint(
												self.LowLegFk_Jnt,
												self.LowLegIk_Jnt,
												self.LowLeg_Jnt
											)
		ankleParCons = lrc.parentConstraint(
												self.AnkleFk_Jnt,
												self.AnkleIk_Jnt,
												self.Ankle_Jnt
											)
		ballParCons = lrc.parentConstraint(
												self.BallFk_Jnt,
												self.BallIk_Jnt,
												self.Ball_Jnt
											)
		toeParCons = lrc.parentConstraint(
												self.ToeFk_Jnt,
												self.ToeIk_Jnt,
												self.Toe_Jnt
											)
	
		self.Leg_Ctrl.attr('fkIk') >> upLegParCons.attr('w1')
		self.Leg_Ctrl.attr('fkIk') >> midLegParCons.attr('w1')
		self.Leg_Ctrl.attr('fkIk') >> lowLegParCons.attr('w1')
		self.Leg_Ctrl.attr('fkIk') >> ankleParCons.attr('w1')
		self.Leg_Ctrl.attr('fkIk') >> ballParCons.attr('w1')
		self.Leg_Ctrl.attr('fkIk') >> toeParCons.attr('w1')
		
		self.LegFkIk_Rev.attr('ox') >> upLegParCons.attr('w0')
		self.LegFkIk_Rev.attr('ox') >> midLegParCons.attr('w0')
		self.LegFkIk_Rev.attr('ox') >> lowLegParCons.attr('w0')
		self.LegFkIk_Rev.attr('ox') >> ankleParCons.attr('w0')
		self.LegFkIk_Rev.attr('ox') >> ballParCons.attr('w0')
		self.LegFkIk_Rev.attr('ox') >> toeParCons.attr('w0')
		
		# Grouping
		self.Ctrl_Grp.parent(ctrlGrp)
		self.Jnt_Grp.parent(jntGrp)

		# Ribbon
		self.LegRbnCtrl_Grp = lrc.Null()
		self.LegRbnCtrl_Grp.name = 'LegRbnCtrl%s%sGrp' % (part, side)

		self.LegRbnCtrl_Grp.snap(self.Ctrl_Grp)

		rbnAx = 'y-'
		rbnAim = (0,-1,0)
		if side == 'RGT' :
			rbnUp = (0,0,-1)
			rbnAmp = -1
		else :
			rbnUp = (0,0,1)
			rbnAmp = 1

		# Ribbon Control
		self.UpLegRbn_Ctrl = lrc.Control('plus')
		self.UpLegRbn_Ctrl.name = 'UpLegRbn%s%sCtrl' % (part, side)
		self.UpLegRbn_Ctrl.lockHideAttrs('rx', 'ry', 'rz', 'sx', 'sy', 'sz', 'v')

		self.UpLegRbnCtrlZr_Grp = lrc.group(self.UpLegRbn_Ctrl)
		self.UpLegRbnCtrlZr_Grp.name = 'UpLegRbnCtrlZr%s%sGrp' % (part, side)

		mc.pointConstraint(self.MidLeg_Jnt, self.UpLegRbnCtrlZr_Grp)
		mc.orientConstraint(self.UpLeg_Jnt, self.UpLegRbnCtrlZr_Grp, mo=True)

		self.LowLegRbn_Ctrl = lrc.Control('plus')
		self.LowLegRbn_Ctrl.name = 'LowLegRbn%s%sCtrl' % (part, side)
		self.LowLegRbn_Ctrl.lockHideAttrs('rx', 'ry', 'rz', 'sx', 'sy', 'sz', 'v')

		self.LowLegRbnCtrlZr_Grp = lrc.group(self.LowLegRbn_Ctrl)
		self.LowLegRbnCtrlZr_Grp.name = 'LowLegRbnCtrlZr%s%sGrp' % (part, side)

		mc.pointConstraint(self.LowLeg_Jnt, self.LowLegRbnCtrlZr_Grp)
		mc.orientConstraint(self.MidLeg_Jnt, self.LowLegRbnCtrlZr_Grp, mo=True)

		# Ribbon Control - Color
		if side == '_R_':
			self.UpLegRbn_Ctrl.color = 'blue'
			self.LowLegRbn_Ctrl.color = 'blue'
		else:
			self.UpLegRbn_Ctrl.color = 'red'
			self.LowLegRbn_Ctrl.color = 'red'

		# Ribbon - Upper leg
		if ribbon :
			self.UpLegRbn = lrb.RibbonIkHi(
												size=self.upLegLen,
												ax=rbnAx,
												part='UpLeg%s' % part,
												side=side.replace('_','')
											)
		else :
			self.UpLegRbn = lrb.RibbonIkLow(
												size=self.upLegLen,
												ax=rbnAx,
												part='UpLeg%s' % part,
												side=side.replace('_','')
											)
		
		self.UpLegRbn.Ctrl_Grp.snapPoint(self.UpLeg_Jnt)
		mc.delete(
					mc.aimConstraint(
										self.MidLeg_Jnt,
										self.UpLegRbn.Ctrl_Grp,
										aim=rbnAim,
										u=rbnUp,
										wut='objectrotation',
										wuo=self.UpLeg_Jnt,
										wu=(0,0,1)
									)
					)
		mc.pointConstraint(self.UpLegRbn_Ctrl, self.UpLegRbn.RbnEnd_Ctrl)

		self.UpLegRbn.RbnRootTws_Grp.rotateOrder = 'yzx'
		self.UpLegRbn.RbnEndTws_Grp.rotateOrder = 'yzx'

		lrc.parentConstraint(
									self.UpLeg_Jnt,
									self.UpLegRbn.Ctrl_Grp,
									mo=True
								)

		mc.parentConstraint(
								self.UpLegNr_Jnt,
								self.UpLegRbn.RbnRootTwsZr_Grp
							)
		mc.parentConstraint(
								self.UpLeg_Jnt,
								self.UpLegRbn.RbnRootTws_Grp
							)

		mc.parentConstraint(
								self.MidLegNr_Jnt,
								self.UpLegRbn.RbnEndTwsZr_Grp
							)
		mc.parentConstraint(
								self.MidLeg_Jnt,
								self.UpLegRbn.RbnEndTws_Grp
							)

		upLegRbnShp = lrc.Dag(self.UpLegRbn.Rbn_Ctrl.shape)
		upLegRbnShp.attr('endTwistAmp').v = -1 * rbnAmp

		# Ribbon - Middle leg
		if ribbon :
			self.MidLegRbn = lrb.RibbonIkHi(
												size=self.upLegLen,
												ax=rbnAx,
												part='MidLeg%s' % part,
												side=side.replace('_','')
											)
		else :
			self.MidLegRbn = lrb.RibbonIkLow(
												size=self.upLegLen,
												ax=rbnAx,
												part='MidLeg%s' % part,
												side=side.replace('_','')
											)
		
		self.MidLegRbn.Ctrl_Grp.snapPoint(self.MidLeg_Jnt)
		mc.delete(
					mc.aimConstraint(
										self.LowLeg_Jnt,
										self.MidLegRbn.Ctrl_Grp,
										aim=rbnAim,
										u=rbnUp,
										wut='objectrotation',
										wuo=self.MidLeg_Jnt,
										wu=(0,0,1)
									)
				)

		mc.parentConstraint(self.MidLeg_Jnt, self.MidLegRbn.Ctrl_Grp, mo=True)
		mc.pointConstraint(self.UpLegRbn_Ctrl, self.MidLegRbn.RbnRoot_Ctrl)
		mc.pointConstraint(self.LowLegRbn_Ctrl, self.MidLegRbn.RbnEnd_Ctrl)

		midLegRbnShp = lrc.Dag(self.MidLegRbn.Rbn_Ctrl.shape)
		midLegRbnShp.attr('endTwistAmp').v = -1 * rbnAmp

		self.MidLegRbn.RbnRootTwsZr_Grp.snap(self.MidLeg_Jnt)
		self.MidLegRbn.RbnEndTwsZr_Grp.snap(self.LowLeg_Jnt)

		mc.parentConstraint(
								self.LowLegNr_Jnt,
								self.MidLegRbn.RbnEndTwsZr_Grp
							)
		mc.parentConstraint(
								self.LowLeg_Jnt,
								self.MidLegRbn.RbnEndTws_Grp
							)

		self.MidLegRbn.RbnRootTws_Grp.rotateOrder = 'yzx'
		self.MidLegRbn.RbnEndTws_Grp.rotateOrder = 'yzx'

		# Ribbon - Lower leg
		if ribbon :
			self.LowLegRbn = lrb.RibbonIkHi(
												size=self.upLegLen,
												ax=rbnAx,
												part='LowLeg%s' % part,
												side=side.replace('_','')
											)
		else :
			self.LowLegRbn = lrb.RibbonIkLow(
												size=self.upLegLen,
												ax=rbnAx,
												part='LowLeg%s' % part,
												side=side.replace('_','')
											)
		
		self.LowLegRbn.Ctrl_Grp.snapPoint(self.LowLeg_Jnt)
		mc.delete(
					mc.aimConstraint(
										self.Ankle_Jnt,
										self.LowLegRbn.Ctrl_Grp,
										aim=rbnAim,
										u=rbnUp,
										wut='objectrotation',
										wuo=self.LowLeg_Jnt,
										wu=(0,0,1)
									)
				)

		mc.parentConstraint(self.LowLeg_Jnt, self.LowLegRbn.Ctrl_Grp, mo=True)
		mc.pointConstraint(self.LowLegRbn_Ctrl, self.LowLegRbn.RbnRoot_Ctrl)
		mc.pointConstraint(self.Ankle_Jnt, self.LowLegRbn.RbnEnd_Ctrl)

		lowLegRbnShp = lrc.Dag(self.LowLegRbn.Rbn_Ctrl.shape)
		lowLegRbnShp.attr('endTwistAmp').v = -1 * rbnAmp

		self.LowLegRbn.RbnRootTwsZr_Grp.snap(self.LowLeg_Jnt)
		self.LowLegRbn.RbnEndTwsZr_Grp.snap(self.Ankle_Jnt)

		mc.parentConstraint(
								self.LowLeg_Jnt,
								self.LowLegRbn.RbnEndTwsZr_Grp,
								mo=True
							)
		mc.parentConstraint(
								self.Ankle_Jnt,
								self.LowLegRbn.RbnEndTws_Grp,
								mo=True
							)

		self.LowLegRbn.RbnRootTws_Grp.rotateOrder = 'yzx'
		self.LowLegRbn.RbnEndTws_Grp.rotateOrder = 'yzx'

		# Ribbon - Grouping
		self.UpLegRbnCtrlZr_Grp.parent(self.LegRbnCtrl_Grp)
		self.LowLegRbnCtrlZr_Grp.parent(self.LegRbnCtrl_Grp)
		self.UpLegRbn.Ctrl_Grp.parent(self.LegRbnCtrl_Grp)
		self.UpLegRbn.Skin_Grp.parent(skinGrp)
		self.UpLegRbn.Jnt_Grp.parent(jntGrp)
		self.MidLegRbn.Ctrl_Grp.parent(self.LegRbnCtrl_Grp)
		self.MidLegRbn.Skin_Grp.parent(skinGrp)
		self.MidLegRbn.Jnt_Grp.parent(jntGrp)
		self.LowLegRbn.Ctrl_Grp.parent(self.LegRbnCtrl_Grp)
		self.LowLegRbn.Skin_Grp.parent(skinGrp)
		self.LowLegRbn.Jnt_Grp.parent(jntGrp)
		self.LegRbnCtrl_Grp.parent(self.Ctrl_Grp)

		if ribbon :
			self.UpLegRbn.Still_Grp.parent(stillGrp)
			self.MidLegRbn.Still_Grp.parent(stillGrp)
			self.LowLegRbn.Still_Grp.parent(stillGrp)

		self.UpLegRbn.RbnRoot_Ctrl.lockHideAttrs('tx', 'ty', 'tz')
		self.UpLegRbn.RbnEnd_Ctrl.lockHideAttrs('tx', 'ty', 'tz')
		self.MidLegRbn.RbnRoot_Ctrl.lockHideAttrs('tx', 'ty', 'tz')
		self.MidLegRbn.RbnEnd_Ctrl.lockHideAttrs('tx', 'ty', 'tz')
		self.LowLegRbn.RbnRoot_Ctrl.lockHideAttrs('tx', 'ty', 'tz')
		self.LowLegRbn.RbnEnd_Ctrl.lockHideAttrs('tx', 'ty', 'tz')
		
		self.UpLegRbn.RbnRoot_Ctrl.hide()
		self.UpLegRbn.RbnEnd_Ctrl.hide()
		self.MidLegRbn.RbnRoot_Ctrl.hide()
		self.MidLegRbn.RbnEnd_Ctrl.hide()
		self.LowLegRbn.RbnRoot_Ctrl.hide()
		self.LowLegRbn.RbnEnd_Ctrl.hide()

		# Set default
		self.Leg_Ctrl.attr('fkIk').value = 1
		self.LegIk_Ctrl.attr('localWorld').value = 1
		self.UpLegFk_Ctrl.attr('localWorld').value = 1