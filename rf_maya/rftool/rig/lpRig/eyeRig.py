import maya.cmds as mc

from lpRig import bshTools as bshTools
reload(bshTools)
import lpRig.core as lrc
reload(lrc)
import lpRig.rigTools as lrr
reload(lrr)

def doCreateBsh():
	
	eyeRigCtrl = mc.ls('*:EyeRigCtrl_Grp')[0]
	ctrlNs = '{}:'.format(eyeRigCtrl.split(':')[0])

	stillGrp = '{}EyeRigStill_Grp'.format(ctrlNs)
	
	lftBshBufGrp = lrc.Null()
	lftBshBufGrp.name = 'LidRigBshBuf_L_Bsh'

	rgtBshBufGrp = lrc.Null()
	rgtBshBufGrp.name = 'LidRigBshBuf_R_Bsh'
	
	lftLidCrv = lrc.Dag(mc.duplicate('{}LidRigTar_L_Crv'.format(ctrlNs))[0])
	lftLidCrv.name = 'LidCrvRigBshBuf_L_Bsh'
	lftLidCrv.parent(lftBshBufGrp)
	
	rgtLidCrv = lrc.Dag(mc.duplicate('{}LidRigTar_R_Crv'.format(ctrlNs))[0])
	rgtLidCrv.name = 'LidCrvRigBshBuf_R_Bsh'
	rgtLidCrv.parent(rgtBshBufGrp)
	
	lftBshGrp = createBsh(bshBuf=lftBshBufGrp, ctrlNs=ctrlNs, part='EyeRig', side='L')
	rgtBshGrp = createBsh(bshBuf=rgtBshBufGrp, ctrlNs=ctrlNs, part='EyeRig', side='R')
	
	lftBshGrp.attr('tx').v = 10
	rgtBshGrp.attr('tx').v = 10

	lftBshBufGrp.parent(stillGrp)
	rgtBshBufGrp.parent(stillGrp)

	mc.select(lftLidCrv, r=True)
	mc.select('{}LidRigTar_L_Crv'.format(ctrlNs), add=True)
	lrr.doAddBlendShape()

	mc.select(rgtLidCrv, r=True)
	mc.select('{}LidRigTar_R_Crv'.format(ctrlNs), add=True)
	lrr.doAddBlendShape()

def createBsh(bshBuf='', ctrlNs='', part='', side=''):
	'''
	Create a blend shape node and connect to the controllers.
	'''
	if side:
		side = '_%s_' % side
	else:
		side = '_'

	bshGrp = lrc.Null()
	bshGrp.name = '%sCrvBsh%sGrp' % (part, side)

	bsn = lrc.Dag('%sCrvBuf%sBsn' % (part, side))
	shps = [
				'LidUpDn',
				'LidUpDnInb',
				'LidLowUp',
				'LidLowUpInb'
			]

	# Populate blend shape names from shape names.
	bshNms = ['%s%s%sBsh' % (shp, part, side) for shp in shps]

	# Duplicate blend shape objects.
	dupBshs, txtCrvs = bshTools.dupBlendShapeList(bshBuf, [bshNms])
	mc.parent(dupBshs, txtCrvs, bshGrp)

	# Create blend shape.
	bshs = [bshNms[0], bshNms[2]]
	mc.blendShape(bshs, bshBuf, n=bsn)

	# Connect inbetween blend shapes.
	bshTools.connectInbetween(
								bshBuf,
								bsn,
								bshNms[1],
								bshNms[0],
								0.5
							)
	bshTools.connectInbetween(
								bshBuf,
								bsn,
								bshNms[3],
								bshNms[2],
								0.5
							)

	# Connection
	ctrl = lrc.Dag('%sLidUp%s%sCtrl' % (ctrlNs, part, side))
	upAmp = lrr.attrAmper(
							ctrl.attr('ty'),
							bsn.attr(bshs[0]),
							-1,
							''
						)
	upAmp.name = '%sUpAmp%sMul' % (part, side)

	ctrl = lrc.Dag('%sLidLow%s%sCtrl' % (ctrlNs, part, side))
	lowAmp = lrr.attrAmper(
								ctrl.attr('ty'),
								bsn.attr(bshs[1]),
								1,
								''
							)
	lowAmp.name = '%sLowAmp%sMul' % (part, side)

	return bshGrp

def createLidBsh(bshBuf='', part=''):
	"""Create blend shape geos for eyelids.
	"""
	bshGrp = lrc.Null()
	bshGrp.name = '%sBsh_Grp' % part

	# Eyelids shapes
	lidSideShps = [
						'UpLidDn',
						'LowLidUp',
						'UpLidTwsDn',
						'UpLidTwsUp',
						'LowLidTwsDn',
						'LowLidTwsUp',
					]
	# Inbetween blend shapes.
	lidSideInbShps = [
						'UpLidDnInb',
						'LowLidUpInb'
					]
	
	# Populate names of blend shapes.
	lidLftBshNms = ['%s%s_L_Bsh' % (lidSideShp, part) for lidSideShp in lidSideShps]
	lidRgtBshNms = ['%s%s_R_Bsh' % (lidSideShp, part) for lidSideShp in lidSideShps]
	lidLftInbBshNms = ['%s%s_L_Bsh' % (lidSideInbShp, part) for lidSideInbShp in lidSideInbShps]
	lidRgtInbBshNms = ['%s%s_R_Bsh' % (lidSideInbShp, part) for lidSideInbShp in lidSideInbShps]

	allBshNms = [lidLftBshNms, lidRgtBshNms, lidLftInbBshNms, lidRgtInbBshNms]

	# Duplicate blend shape objects.
	dupBshs, txtCrvs = bshTools.dupBlendShapeList(bshBuf, allBshNms)
	mc.parent(dupBshs, txtCrvs, bshGrp)

	# Create blend shape.
	bsn = lrc.Node('%sBuf_Bsn' % part)
	mc.blendShape(lidLftBshNms + lidRgtBshNms, bshBuf, n=bsn)

	for ix in xrange(2):
		bshTools.connectInbetween(bshBuf, bsn, lidLftInbBshNms[ix], lidLftBshNms[ix], 0.5)
		bshTools.connectInbetween(bshBuf, bsn, lidRgtInbBshNms[ix], lidRgtBshNms[ix], 0.5)

	sides = ('_L_', '_R_')
	for ix in xrange(2):
		
		upLidCtrl = mc.ls('*:LidUp*%sCtrl' % sides[ix])[0]
		lowLidCtrl = mc.ls('*:LidLow*%sCtrl' % sides[ix])[0]
		
		# Connections
		upLidInfoDict = {
							0: {'ty': allBshNms[ix][0]},
							1: {'rz': (allBshNms[ix][2], allBshNms[ix][3])}
						}
		lowLidInfoDict = {
							0: {'ty': allBshNms[ix][1]},
							1: {'rz': (allBshNms[ix][4], allBshNms[ix][5])}
						}
		bshTools.ctrlToBsh(upLidCtrl, bsn, upLidInfoDict)
		bshTools.ctrlToBsh(lowLidCtrl, bsn, lowLidInfoDict)

		upLidAmp = mc.listConnections('%s.ty' % upLidCtrl, d=True, s=False)[0]
		mc.setAttr('%s.amp' % upLidAmp, -1)

		if sides[ix] == '_R_':
			upLidRzAmp = mc.listConnections('%s.rz' % upLidCtrl, d=True, s=False)[0]
			lowLidRzAmp = mc.listConnections('%s.rz' % lowLidCtrl, d=True, s=False)[0]

			mc.setAttr('%s.amp' % upLidRzAmp, -mc.getAttr('%s.amp' % upLidRzAmp))
			mc.setAttr('%s.amp' % lowLidRzAmp, -mc.getAttr('%s.amp' % lowLidRzAmp))

def createRpJoint(
					tmpCntObj='',
					tmpObj='',
					tmpTarObj=''
				):
	
	tmpPos = mc.xform(tmpObj, q=True, t=True, ws=True)
	tmpCntPos = mc.xform(tmpCntObj, q=True, t=True, ws=True)
	tmpTarPos = mc.xform(tmpTarObj, q=True, t=True, ws=True)

	# Vector from jnt to center
	cntVect = [
					tmpCntPos[0] - tmpPos[0],
					tmpCntPos[1] - tmpPos[1],
					tmpCntPos[2] - tmpPos[2],
				]

	# Vector from jnt to target
	tarVect = [
					tmpTarPos[0] - tmpPos[0],
					tmpTarPos[1] - tmpPos[1],
					tmpTarPos[2] - tmpPos[2],
				]

	upVec = lrc.cross(tarVect, cntVect)

	cntJnt = lrr.jointAt(tmpCntObj)
	jnt = lrr.jointAt(tmpObj)
	
	mc.delete(
				lrc.aimConstraint(
									jnt,
									cntJnt,
									aim=[0, 1, 0],
									u=[1, 0, 0],
									wut='vector',
									wu=upVec
								)
				)

	jnt.snapOrient(cntJnt)
	cntJnt.freeze()
	jnt.freeze()

	jntZrGrp = lrr.groupAt(jnt)
	jntZrGrp.parent(cntJnt)

	cntJntSdkGrp = lrr.groupAt(cntJnt)
	cntJntZrGrp = lrr.groupAt(cntJntSdkGrp)
	
	return jnt, jntZrGrp, cntJnt, cntJntSdkGrp, cntJntZrGrp

class LidRig(object):

	def __init__(
					self,
					cntTmpLoc='CntLidRigTmp_L_Loc',
					upTmpLoc='UpLidRigTmp_L_Loc',
					lowTmpLoc='LowLidRigTmp_L_Loc',
					ctrlOriTmpLoc='CtrlOriLidRigTmp_L_Loc',
					ctrlPosTmpCrv='CtrlPosLidRigTmp_L_Crv',
					upTmpCrv='UpLidRigTmp_L_Crv',
					lowTmpCrv='LowLidRigTmp_L_Crv',
					upSktTmpCrv='UpSktLidRigTmp_L_Crv',
					lowSktTmpCrv='LowSktLidRigTmp_L_Crv',
					up25TmpCrv='Up25LidRigTmp_L_Crv',
					low25TmpCrv='Low25LidRigTmp_L_Crv',
					up50TmpCrv='Up50LidRigTmp_L_Crv',
					low50TmpCrv='Low50LidRigTmp_L_Crv',
					up75TmpCrv='Up75LidRigTmp_L_Crv',
					low75TmpCrv='Low75LidRigTmp_L_Crv',
					tarTmpCrv='LidRigTmp_L_Crv',
					tarSktTmpCrv='SktLidRigTmp_L_Crv',
					sktOriTmpLoc='SktCtrlOriLidRigTmp_L_Loc',
					sktPosTmpCrv='SktCtrlPosLidRigTmp_L_Crv',
					part='LidRig',
					side='L',
					do25=True,
					do50=True,
					do75=True,
					eyeRigObj=None
				):

		if side:
			side = '_%s_' % side
		else:
			side = '_'

		# Define color
		col = 'red'
		darkCol = 'darkRed'
		softCol = 'softRed'
		if side == '_R_':
			col = 'blue'
			darkCol = 'darkBlue'
			softCol = 'softBlue'

		# Main Group
		self.Ctrl_Grp = lrc.Null()
		self.Ctrl_Grp.name = '%sCtrl%sGrp' % (part, side)

		self.Jnt_Grp = lrc.Null()
		self.Jnt_Grp.name = '%sJnt%sGrp' % (part, side)

		self.Skin_Grp = lrc.Null()
		self.Skin_Grp.name = '%sSkin%sGrp' % (part, side)

		self.Still_Grp = lrc.Null()
		self.Still_Grp.name = '%sStill%sGrp' % (part, side)

		self.Rvt_Grp = lrc.Null()
		self.Rvt_Grp.name = '%sRvt%sGrp' % (part, side)
		self.Rvt_Grp.parent(self.Still_Grp)

		self.Tar_Grp = lrc.Null()
		self.Tar_Grp.name = '%sTar%sGrp' % (part, side)
		self.Tar_Grp.parent(self.Still_Grp)

		# Tmp Curves
		upCrv = lrc.Dag(upTmpCrv)
		lowCrv = lrc.Dag(lowTmpCrv)

		upSktCrv = lrc.Dag(upSktTmpCrv)
		lowSktCrv = lrc.Dag(lowSktTmpCrv)

		up25Crv = None
		low25Crv = None
		up50Crv = None
		low50Crv = None
		up75Crv = None
		low75Crv = None

		if do25:
			up25Crv = lrc.Dag(up25TmpCrv)
			low25Crv = lrc.Dag(low25TmpCrv)

		if do50:
			up50Crv = lrc.Dag(up50TmpCrv)
			low50Crv = lrc.Dag(low50TmpCrv)

		if do75:
			up75Crv = lrc.Dag(up75TmpCrv)
			low75Crv = lrc.Dag(low75TmpCrv)
		
		# tmpObjs
		tmpObjs = self.locatorOnCurve([upCrv, lowCrv])
		tmpSktObjs = self.locatorOnCurve([upSktCrv, lowSktCrv])

		tmp25Objs = []
		if do25: tmp25Objs = self.locatorOnCurve([up25Crv, low25Crv])

		tmp50Objs = []
		if do50: tmp50Objs = self.locatorOnCurve([up50Crv, low50Crv])

		tmp75Objs = []
		if do75: tmp75Objs = self.locatorOnCurve([up75Crv, low75Crv])

		# Target Curves
		self.Tar_Crv = lrc.Dag(tarTmpCrv)
		self.Tar_Crv.name = '%sTar%sCrv' % (part, side)
		self.Tar_Crv.parent(self.Still_Grp)
		# self.Tar_Crv.freeze(t=True, r=True, s=True)
		mc.delete(self.Tar_Crv, ch=True)

		# Target Skin Curves
		self.TarSkin_Crv = lrc.Dag(mc.duplicate(self.Tar_Crv, rr=True)[0])
		self.TarSkin_Crv.name = '%sTarSkin%sCrv' % (part, side)

		# Target Blend Shape
		self.TarCrv_Bsn = lrc.Dag(mc.blendShape(self.TarSkin_Crv, self.Tar_Crv, origin='local')[0])
		self.TarCrv_Bsn.name = '%sTarCrv%sBsn' % (part, side)
		self.TarCrv_Bsn.attr('w[0]').v = 1

		self.SktTar_Crv = lrc.Dag(tarSktTmpCrv)
		self.SktTar_Crv.name = 'Skt%sTar%sCrv' % (part, side)
		self.SktTar_Crv.parent(self.Still_Grp)
		# self.SktTar_Crv.freeze(t=True, r=True, s=True)
		mc.delete(self.SktTar_Crv, ch=True)

		# tarObjs - Genereate target objects base on tmpObjs
		tmpTarObjs = self.populateTarObjs(tmpObjs)
		tmpSktTarObjs = self.populateTarObjs(tmpSktObjs)

		# Main Tar Jnt Grps
		self.TarCnt_Jnt = lrc.Joint()
		self.TarCnt_Jnt.attr('drawStyle').v = 2
		self.TarCnt_Jnt.name = 'TarCnt%s%sJnt' % (part, side)

		self.TarCntJntOfst_Grp = lrc.group(self.TarCnt_Jnt)
		self.TarCntJntOfst_Grp.name = 'TarCnt%sJntOfst%sGrp' % (part, side)

		self.TarCntJntZr_Grp = lrc.group(self.TarCntJntOfst_Grp)
		self.TarCntJntZr_Grp.name = 'TarCnt%sJntZr%sGrp' % (part, side)
		self.TarCntJntZr_Grp.snap(cntTmpLoc)
		self.TarCntJntZr_Grp.parent(self.Skin_Grp)

		self.TarJnt_Grp = lrc.Null()
		self.TarJnt_Grp.name = 'TarJnt%s%sGrp' % (part, side)
		self.TarJnt_Grp.snap(self.TarCnt_Jnt)
		self.TarJnt_Grp.parent(self.TarCnt_Jnt)

		self.TarCtrl_Grp = lrc.Null()
		self.TarCtrl_Grp.name = 'TarCtrl%s%sGrp' % (part, side)
		self.TarCtrl_Grp.attr('v').v = 0
		self.TarCtrl_Grp.parent(self.Ctrl_Grp)

		self.TarSktJnt_Grp = lrc.Null()
		self.TarSktJnt_Grp.name = 'TarSktJnt%s%sGrp' % (part, side)
		self.TarSktJnt_Grp.snap(self.TarCnt_Jnt)
		self.TarSktJnt_Grp.parent(self.TarCnt_Jnt)

		self.TarSktCtrl_Grp = lrc.Null()
		self.TarSktCtrl_Grp.name = 'TarSktCtrl%s%sGrp' % (part, side)
		self.TarSktCtrl_Grp.attr('v').v = 0
		self.TarSktCtrl_Grp.parent(self.Ctrl_Grp)
		
		self.TarJnt25_Grp = lrc.Null()
		self.TarJnt25_Grp.name = 'TarJnt25%s%sGrp' % (part, side)
		self.TarJnt25_Grp.snap(self.TarCnt_Jnt)
		self.TarJnt25_Grp.parent(self.TarCnt_Jnt)

		self.TarJnt50_Grp = lrc.Null()
		self.TarJnt50_Grp.name = 'TarJnt50%s%sGrp' % (part, side)
		self.TarJnt50_Grp.snap(self.TarCnt_Jnt)
		self.TarJnt50_Grp.parent(self.TarCnt_Jnt)

		self.TarJnt75_Grp = lrc.Null()
		self.TarJnt75_Grp.name = 'TarJnt75%s%sGrp' % (part, side)
		self.TarJnt75_Grp.snap(self.TarCnt_Jnt)
		self.TarJnt75_Grp.parent(self.TarCnt_Jnt)

		# Generate joints
		for ix in range(len(tmpObjs)):

			(jnt,
			jntZrGrp,
			cntJnt,
			cntJntSdkGrp,
			cntJntZrGrp,
			tar,
			tarZrGrp,
			rvt,
			ctrl,
			ctrlInvGrp,
			ctrlZrGrp) = self.createMainLidJoint(
												self.TarCnt_Jnt,
												tmpObjs[ix],
												tmpTarObjs[ix],
												'',
												part,
												ix+1,
												side,
												lrc.Dag(self.Tar_Crv.shape)
											)
			cntJntZrGrp.parent(self.TarJnt_Grp)
			rvt.parent(self.Rvt_Grp)
			tarZrGrp.parent(self.Tar_Grp)
			ctrlZrGrp.parent(self.TarCtrl_Grp)

			(jntSkt,
			jntSktZrGrp,
			cntSktJnt,
			cntSktJntSdkGrp,
			cntSktJntZrGrp,
			tarSkt,
			tarSktZrGrp,
			rvtSkt,
			ctrlSkt,
			ctrlSktInvGrp,
			ctrlSktZrGrp) = self.createMainLidJoint(
														self.TarCnt_Jnt,
														tmpSktObjs[ix],
														tmpSktTarObjs[ix],
														'Skt',
														part,
														ix+1,
														side,
														lrc.Dag(self.SktTar_Crv.shape)
													)
			cntSktJntZrGrp.parent(self.TarSktJnt_Grp)
			rvtSkt.parent(self.Rvt_Grp)
			tarSktZrGrp.parent(self.Tar_Grp)
			ctrlSktZrGrp.parent(self.TarSktCtrl_Grp)

			if do25:
				(
				jnt25,
				jnt25ZrGrp,
				cntJnt25,
				cntJnt25SdkGrp,
				cntJnt25ZrGrp
				) = self.createPrcLidJoint(
											self.TarCnt_Jnt,
											tmp25Objs[ix],
											cntJnt,
											jnt,
											part,
											ix+1,
											side,
											25,
											[cntJntSdkGrp, cntSktJntSdkGrp]
										)
				cntJnt25ZrGrp.parent(self.TarJnt25_Grp)

			if do50:
				(
				jnt50,
				jnt50ZrGrp,
				cntJnt50,
				cntJnt50SdkGrp,
				cntJnt50ZrGrp
				) = self.createPrcLidJoint(
											self.TarCnt_Jnt,
											tmp50Objs[ix],
											cntJnt,
											jnt,
											part,
											ix+1,
											side,
											50,
											[cntJntSdkGrp, cntSktJntSdkGrp]
										)
				cntJnt50ZrGrp.parent(self.TarJnt50_Grp)

			if do75:
				(
				jnt75,
				jnt75ZrGrp,
				cntJnt75,
				cntJnt75SdkGrp,
				cntJnt75ZrGrp
				) = self.createPrcLidJoint(
											self.TarCnt_Jnt,
											tmp75Objs[ix],
											cntJnt,
											jnt,
											part,
											ix+1,
											side,
											75,
											[cntJntSdkGrp, cntSktJntSdkGrp]
										)
				cntJnt75ZrGrp.parent(self.TarJnt75_Grp)

		# Main Lid Control Joints
		self.Cnt_Jnt, self.CntJntZr_Grp = self.createMainJoint(cntTmpLoc, 'Cnt', part, side)
		self.Cnt_Jnt.attr('drawStyle').v = 2
		self.CntJntOff_Grp = lrc.group(self.Cnt_Jnt)
		self.CntJntOff_Grp.name = 'Cnt%sJntOff%sGrp' % (part, side)
		self.CntJntZr_Grp.parent(self.Jnt_Grp)

		self.PartCnt_Jnt, self.PartCntJntZr_Grp = self.createMainJoint(cntTmpLoc, 'PartCnt', part, side)
		self.PartCntJntZr_Grp.parent(self.Cnt_Jnt)

		self.Up_Jnt, self.UpJntZr_Grp = self.createMainJoint(upTmpLoc, 'Up', part, side)
		self.UpJntZr_Grp.parent(self.PartCnt_Jnt)

		self.Low_Jnt, self.LowJntZr_Grp = self.createMainJoint(lowTmpLoc, 'Low', part, side)
		self.LowJntZr_Grp.parent(self.PartCnt_Jnt)

		# Lid Control Joints
		self.CnrIn_Jnt, self.CnrInJntZr_Grp = self.createControlJoint(ctrlOriTmpLoc, '%s.cv[0]' % ctrlPosTmpCrv, 'CnrIn', part, side)
		self.CnrInJntZr_Grp.parent(self.Cnt_Jnt)

		self.CnrInUp_Jnt, self.CnrInUpJntZr_Grp = self.createControlJoint(ctrlOriTmpLoc, '%s.cv[1]' % ctrlPosTmpCrv, 'CnrInUp', part, side)
		self.CnrInUpJntZr_Grp.parent(self.Cnt_Jnt)

		self.CnrInDn_Jnt, self.CnrInDnJntZr_Grp = self.createControlJoint(ctrlOriTmpLoc, '%s.cv[11]' % ctrlPosTmpCrv, 'CnrInDn', part, side)
		self.CnrInDnJntZr_Grp.parent(self.Cnt_Jnt)

		self.CnrOut_Jnt, self.CnrOutJntZr_Grp = self.createControlJoint(ctrlOriTmpLoc, '%s.cv[6]' % ctrlPosTmpCrv, 'CnrOut', part, side)
		self.CnrOutJntZr_Grp.parent(self.Cnt_Jnt)

		self.CnrOutUp_Jnt, self.CnrOutUpJntZr_Grp = self.createControlJoint(ctrlOriTmpLoc, '%s.cv[5]' % ctrlPosTmpCrv, 'CnrOutUp', part, side)
		self.CnrOutUpJntZr_Grp.parent(self.Cnt_Jnt)

		self.CnrOutDn_Jnt, self.CnrOutDnJntZr_Grp = self.createControlJoint(ctrlOriTmpLoc, '%s.cv[7]' % ctrlPosTmpCrv, 'CnrOutDn', part, side)
		self.CnrOutDnJntZr_Grp.parent(self.Cnt_Jnt)

		self.UpIn_Jnt, self.UpInJntZr_Grp = self.createControlJoint(ctrlOriTmpLoc, '%s.cv[2]' % ctrlPosTmpCrv, 'UpIn', part, side)
		self.UpInJntZr_Grp.parent(self.Up_Jnt)

		self.UpMid_Jnt, self.UpMidJntZr_Grp = self.createControlJoint(ctrlOriTmpLoc, '%s.cv[3]' % ctrlPosTmpCrv, 'UpMid', part, side)
		self.UpMidJntZr_Grp.parent(self.Up_Jnt)

		self.UpOut_Jnt, self.UpOutJntZr_Grp = self.createControlJoint(ctrlOriTmpLoc, '%s.cv[4]' % ctrlPosTmpCrv, 'UpOut', part, side)
		self.UpOutJntZr_Grp.parent(self.Up_Jnt)

		self.LowOut_Jnt, self.LowOutJntZr_Grp = self.createControlJoint(ctrlOriTmpLoc, '%s.cv[8]' % ctrlPosTmpCrv, 'LowOut', part, side)
		self.LowOutJntZr_Grp.parent(self.Low_Jnt)

		self.LowMid_Jnt, self.LowMidJntZr_Grp = self.createControlJoint(ctrlOriTmpLoc, '%s.cv[9]' % ctrlPosTmpCrv, 'LowMid', part, side)
		self.LowMidJntZr_Grp.parent(self.Low_Jnt)

		self.LowIn_Jnt, self.LowInJntZr_Grp = self.createControlJoint(ctrlOriTmpLoc, '%s.cv[10]' % ctrlPosTmpCrv, 'LowIn', part, side)
		self.LowInJntZr_Grp.parent(self.Low_Jnt)

		# Socket Joints
		self.CntSkt_Jnt, self.CntSktJntZr_Grp = self.createMainJoint(cntTmpLoc, 'CntSkt', part, side)
		self.CntSkt_Jnt.attr('drawStyle').v = 2
		self.CntSktJntOff_Grp = lrc.group(self.CntSkt_Jnt)
		self.CntSktJntOff_Grp.name = 'Cnt%sSktJntOff%sGrp' % (part, side)
		self.CntSktJntZr_Grp.parent(self.Jnt_Grp)
		
		self.CnrInSkt_Jnt, self.CnrInSktJntZr_Grp = self.createControlJoint(sktOriTmpLoc, '%s.cv[0]' % sktPosTmpCrv, 'CnrInSkt', part, side)
		self.CnrInSktJntZr_Grp.parent(self.CntSkt_Jnt)

		self.CnrOutSkt_Jnt, self.CnrOutSktJntZr_Grp = self.createControlJoint(sktOriTmpLoc, '%s.cv[4]' % sktPosTmpCrv, 'CnrOutSkt', part, side)
		self.CnrOutSktJntZr_Grp.parent(self.CntSkt_Jnt)

		self.UpInSkt_Jnt, self.UpInSktJntZr_Grp = self.createControlJoint(sktOriTmpLoc, '%s.cv[1]' % sktPosTmpCrv, 'UpInSkt', part, side)
		self.UpInSktJntZr_Grp.parent(self.CntSkt_Jnt)
		
		self.UpMidSkt_Jnt, self.UpMidSktJntZr_Grp = self.createControlJoint(sktOriTmpLoc, '%s.cv[2]' % sktPosTmpCrv, 'UpMidSkt', part, side)
		self.UpMidSktJntZr_Grp.parent(self.CntSkt_Jnt)
		
		self.UpOutSkt_Jnt, self.UpOutSktJntZr_Grp = self.createControlJoint(sktOriTmpLoc, '%s.cv[3]' % sktPosTmpCrv, 'UpOutSkt', part, side)
		self.UpOutSktJntZr_Grp.parent(self.CntSkt_Jnt)
		
		self.LowInSkt_Jnt, self.LowInSktJntZr_Grp = self.createControlJoint(sktOriTmpLoc, '%s.cv[7]' % sktPosTmpCrv, 'LowInSkt', part, side)
		self.LowInSktJntZr_Grp.parent(self.CntSkt_Jnt)
		
		self.LowMidSkt_Jnt, self.LowMidSktJntZr_Grp = self.createControlJoint(sktOriTmpLoc, '%s.cv[6]' % sktPosTmpCrv, 'LowMidSkt', part, side)
		self.LowMidSktJntZr_Grp.parent(self.CntSkt_Jnt)
		
		self.LowOutSkt_Jnt, self.LowOutSktJntZr_Grp = self.createControlJoint(sktOriTmpLoc, '%s.cv[5]' % sktPosTmpCrv, 'LowOutSkt', part, side)
		self.LowOutSktJntZr_Grp.parent(self.CntSkt_Jnt)
		
		# Main Controls - Center
		(self.Cnt_Ctrl,
		self.CntCtrlInv_Grp,
		self.CntCtrlZr_Grp, 
		self.CntCtrlBfrT_Pma) = self.createMainControl(self.Cnt_Jnt, 'Cnt', part, side, ('s'))
		self.Cnt_Ctrl.color = col
		self.CntCtrlZr_Grp.parent(self.Ctrl_Grp)
		self.Cnt_Ctrl.attr('t') >> self.TarCntJntOfst_Grp.attr('t')
		self.Cnt_Ctrl.attr('r') >> self.TarCntJntOfst_Grp.attr('r')
		self.Cnt_Ctrl.attr('t') >> self.CntSktJntOff_Grp.attr('t')
		self.Cnt_Ctrl.attr('r') >> self.CntSktJntOff_Grp.attr('r')

		(self.CnrIn_Ctrl, 
		self.CnrInCtrlInv_Grp, 
		self.CnrInCtrlZr_Grp, 
		self.CnrInCtrlBfrT_Pma) = self.createMainControl(self.CnrIn_Jnt, 'CnrIn', part, side, ('tz', 'r', 's'))
		self.CnrIn_Ctrl.color = col
		self.CnrInCtrlZr_Grp.parent(self.Cnt_Ctrl)

		(self.CnrInUp_Ctrl,
		self.CnrInUpCtrlInUpv_Grp,
		self.CnrInUpCtrlZr_Grp, 
		self.CnrInUpCtrlBfrT_Pma) = self.createMainControl(self.CnrInUp_Jnt, 'CnrInUp', part, side, ('tz', 'r', 's'))
		self.CnrInUp_Ctrl.color = softCol
		self.CnrInUpCtrlZr_Grp.parent(self.Cnt_Ctrl)

		(self.CnrInDn_Ctrl,
		self.CnrInDnCtrlInDnv_Grp,
		self.CnrInDnCtrlZr_Grp, 
		self.CnrInDnCtrlBfrT_Pma) = self.createMainControl(self.CnrInDn_Jnt, 'CnrInDn', part, side, ('tz', 'r', 's'))
		self.CnrInDn_Ctrl.color = softCol
		self.CnrInDnCtrlZr_Grp.parent(self.Cnt_Ctrl)

		(self.CnrOut_Ctrl,
		self.CnrOutCtrlInv_Grp, 
		self.CnrOutCtrlZr_Grp, 
		self.CnrOutCtrlBfrT_Pma) = self.createMainControl(self.CnrOut_Jnt, 'CnrOut', part, side, ('tz', 'r', 's'))
		self.CnrOut_Ctrl.color = col
		self.CnrOutCtrlZr_Grp.parent(self.Cnt_Ctrl)

		(self.CnrOutUp_Ctrl,
		self.CnrOutUpCtrlOutUpv_Grp,
		self.CnrOutUpCtrlZr_Grp, 
		self.CnrOutUpCtrlBfrT_Pma) = self.createMainControl(self.CnrOutUp_Jnt, 'CnrOutUp', part, side, ('tz', 'r', 's'))
		self.CnrOutUp_Ctrl.color = softCol
		self.CnrOutUpCtrlZr_Grp.parent(self.Cnt_Ctrl)

		(self.CnrOutDn_Ctrl,
		self.CnrOutDnCtrlOutDnv_Grp,
		self.CnrOutDnCtrlZr_Grp, 
		self.CnrOutDnCtrlBfrT_Pma) = self.createMainControl(self.CnrOutDn_Jnt, 'CnrOutDn', part, side, ('tz', 'r', 's'))
		self.CnrOutDn_Ctrl.color = softCol
		self.CnrOutDnCtrlZr_Grp.parent(self.Cnt_Ctrl)

		# Main Controls - Up
		self.UpCtrl_Grp = lrc.Null()
		self.UpCtrl_Grp.name = '%sUpCtrl%sGrp' % (part, side)

		self.UpCtrlZro_Grp = lrc.group(self.UpCtrl_Grp)
		self.UpCtrlZro_Grp.name = '%sUpCtrlZro%sGrp' % (part, side)
		self.UpCtrlZro_Grp.snap(self.Up_Jnt)
		self.UpCtrlZro_Grp.parent(self.Cnt_Ctrl)

		(self.UpIn_Ctrl,
		self.UpInCtrlInv_Grp,
		self.UpInCtrlZr_Grp, 
		self.UpInCtrlBfrT_Pma) = self.createMainControl(self.UpIn_Jnt, 'UpIn', part, side, ('tz', 'r', 's'))
		self.UpIn_Ctrl.color = darkCol
		self.UpInCtrlZr_Grp.parent(self.UpCtrl_Grp)

		(self.UpMid_Ctrl,
		self.UpMidCtrlInv_Grp,
		self.UpMidCtrlZr_Grp, 
		self.UpMidCtrlBfrT_Pma) = self.createMainControl(self.UpMid_Jnt, 'UpMid', part, side, ('tz', 'r', 's'))
		self.UpMid_Ctrl.color = col
		self.UpMidCtrlZr_Grp.parent(self.UpCtrl_Grp)

		(self.UpOut_Ctrl,
		self.UpOutCtrlInv_Grp,
		self.UpOutCtrlZr_Grp, 
		self.UpOutCtrlBfrT_Pma) = self.createMainControl(self.UpOut_Jnt, 'UpOut', part, side, ('tz', 'r', 's'))
		self.UpOut_Ctrl.color = darkCol
		self.UpOutCtrlZr_Grp.parent(self.UpCtrl_Grp)

		# Main Controls - Low
		self.LowCtrl_Grp = lrc.Null()
		self.LowCtrl_Grp.name = '%sLowCtrl%sGrp' % (part, side)
		
		self.LowCtrlZro_Grp = lrc.group(self.LowCtrl_Grp)
		self.LowCtrlZro_Grp.name = '%sLowCtrlZro%sGrp' % (part, side)
		self.LowCtrlZro_Grp.snap(self.Low_Jnt)
		self.LowCtrlZro_Grp.parent(self.Cnt_Ctrl)

		(self.LowOut_Ctrl,
		self.LowOutCtrlInv_Grp,
		self.LowOutCtrlZr_Grp, 
		self.LowOutCtrlBfrT_Pma) = self.createMainControl(self.LowOut_Jnt, 'LowOut', part, side, ('tz', 'r', 's'))
		self.LowOut_Ctrl.color = darkCol
		self.LowOutCtrlZr_Grp.parent(self.LowCtrl_Grp)

		(self.LowMid_Ctrl,
		self.LowMidCtrlInv_Grp,
		self.LowMidCtrlZr_Grp, 
		self.LowMidCtrlBfrT_Pma) = self.createMainControl(self.LowMid_Jnt, 'LowMid', part, side, ('tz', 'r', 's'))
		self.LowMid_Ctrl.color = col
		self.LowMidCtrlZr_Grp.parent(self.LowCtrl_Grp)

		(self.LowIn_Ctrl,
		self.LowInCtrlInv_Grp,
		self.LowInCtrlZr_Grp, 
		self.LowInCtrlBfrT_Pma) = self.createMainControl(self.LowIn_Jnt, 'LowIn', part, side, ('tz', 'r', 's'))
		self.LowIn_Ctrl.color = darkCol
		self.LowInCtrlZr_Grp.parent(self.LowCtrl_Grp)

		# Socket Controls
		(self.CnrInSkt_Ctrl,
		self.CnrInSktCtrlInv_Grp,
		self.CnrInSktCtrlZr_Grp, 
		self.CnrInSktCtrlBfrT_Pma) = self.createMainControl(self.CnrInSkt_Jnt, 'CnrInSkt', part, side, ('r', 's'))
		self.CnrInSkt_Ctrl.color = col
		self.CnrInSktCtrlZr_Grp.parent(self.Cnt_Ctrl)

		(self.CnrOutSkt_Ctrl,
		self.CnrOutSktCtrlInv_Grp, 
		self.CnrOutSktCtrlZr_Grp, 
		self.CnrOutSktCtrlBfrT_Pma) = self.createMainControl(self.CnrOutSkt_Jnt, 'CnrOutSkt', part, side, ('r', 's'))
		self.CnrOutSkt_Ctrl.color = col
		self.CnrOutSktCtrlZr_Grp.parent(self.Cnt_Ctrl)

		# Socket Controls - Up
		(self.UpInSkt_Ctrl,
		self.UpInSktCtrlInv_Grp,
		self.UpInSktCtrlZr_Grp, 
		self.UpInSktCtrlBfrT_Pma) = self.createMainControl(self.UpInSkt_Jnt, 'UpInSkt', part, side, ('r', 's'))
		self.UpInSkt_Ctrl.color = darkCol
		self.UpInSktCtrlZr_Grp.parent(self.Cnt_Ctrl)

		(self.UpMidSkt_Ctrl,
		self.UpMidSktCtrlInv_Grp,
		self.UpMidSktCtrlZr_Grp, 
		self.UpMidSktCtrlBfrT_Pma) = self.createMainControl(self.UpMidSkt_Jnt, 'UpMidSkt', part, side, ('r', 's'))
		self.UpMidSkt_Ctrl.color = col
		self.UpMidSktCtrlZr_Grp.parent(self.Cnt_Ctrl)

		(self.UpOutSkt_Ctrl,
		self.UpOutSktCtrlInv_Grp,
		self.UpOutSktCtrlZr_Grp, 
		self.UpOutSktCtrlBfrT_Pma) = self.createMainControl(self.UpOutSkt_Jnt, 'UpOutSkt', part, side, ('r', 's'))
		self.UpOutSkt_Ctrl.color = darkCol
		self.UpOutSktCtrlZr_Grp.parent(self.Cnt_Ctrl)

		# Main Controls - Low
		(self.LowOutSkt_Ctrl,
		self.LowOutSktCtrlInv_Grp,
		self.LowOutSktCtrlZr_Grp, 
		self.LowOutSktCtrlBfrT_Pma) = self.createMainControl(self.LowOutSkt_Jnt, 'LowOutSkt', part, side, ('r', 's'))
		self.LowOutSkt_Ctrl.color = darkCol
		self.LowOutSktCtrlZr_Grp.parent(self.Cnt_Ctrl)

		(self.LowMidSkt_Ctrl,
		self.LowMidSktCtrlInv_Grp,
		self.LowMidSktCtrlZr_Grp, 
		self.LowMidSktCtrlBfrT_Pma) = self.createMainControl(self.LowMidSkt_Jnt, 'LowMidSkt', part, side, ('r', 's'))
		self.LowMidSkt_Ctrl.color = col
		self.LowMidSktCtrlZr_Grp.parent(self.Cnt_Ctrl)

		(self.LowInSkt_Ctrl,
		self.LowInSktCtrlInv_Grp,
		self.LowInSktCtrlZr_Grp, 
		self.LowInSktCtrlBfrT_Pma) = self.createMainControl(self.LowInSkt_Jnt, 'LowInSkt', part, side, ('r', 's'))
		self.LowInSkt_Ctrl.color = darkCol
		self.LowInSktCtrlZr_Grp.parent(self.Cnt_Ctrl)

		# Skinning - Lid
		infs = (
					self.CnrIn_Jnt,
					self.CnrInUp_Jnt,
					self.UpIn_Jnt,
					self.UpMid_Jnt,
					self.UpOut_Jnt,
					self.CnrOutUp_Jnt,
					self.CnrOut_Jnt,
					self.CnrOutDn_Jnt,
					self.LowOut_Jnt,
					self.LowMid_Jnt,
					self.LowIn_Jnt,
					self.CnrInDn_Jnt
				)
		skc = mc.skinCluster(self.TarSkin_Crv, infs, dr=1, mi=1)[0]

		for ix, inf in enumerate(infs):
			mc.skinPercent(skc, '%s.cv[%s]' % (self.TarSkin_Crv, ix), tv=[(inf, 1)])
		
		# Skinning - Skt
		infs = (
					self.CnrInSkt_Jnt,
					self.UpInSkt_Jnt,
					self.UpMidSkt_Jnt,
					self.UpOutSkt_Jnt,
					self.CnrOutSkt_Jnt,
					self.LowOutSkt_Jnt,
					self.LowMidSkt_Jnt,
					self.LowInSkt_Jnt
				)
		skc = mc.skinCluster(self.SktTar_Crv, infs, dr=1, mi=1)[0]

		for ix, inf in enumerate(infs):
			mc.skinPercent(skc, '%s.cv[%s]' % (self.SktTar_Crv, ix), tv=[(inf, 1)])

		# Cleanup
		mc.delete(tmpObjs)
		mc.delete(tmpSktObjs)
		if do25:
			mc.delete(tmp25Objs)
		if do50:
			mc.delete(tmp50Objs)
		if do75:
			mc.delete(tmp75Objs)

		# Connect to Eye Module
		eyeRigObj.Cnt_Ctrl.attr('t') >> self.CntJntOff_Grp.attr('t')
		eyeRigObj.Cnt_Ctrl.attr('s') >> self.CntJntOff_Grp.attr('s')

		eyeRigObj.Cnt_Ctrl.attr('s') >> self.CntCtrlInv_Grp.attr('s')

		eyeRigObj.Cnt_Ctrl.attr('t') >> self.TarCnt_Jnt.attr('t')
		eyeRigObj.Cnt_Ctrl.attr('s') >> self.TarCnt_Jnt.attr('s')

		eyeRigObj.LidUp_Ctrl.attr('tx') >> self.Up_Jnt.attr('tx')
		eyeRigObj.LidUp_Ctrl.attr('r') >> self.Up_Jnt.attr('r')
		eyeRigObj.LidLow_Ctrl.attr('tx') >> self.Low_Jnt.attr('tx')
		eyeRigObj.LidLow_Ctrl.attr('r') >> self.Low_Jnt.attr('r')

		eyeRigObj.LidUp_Ctrl.attr('r') >> self.UpCtrl_Grp.attr('r')
		eyeRigObj.LidLow_Ctrl.attr('r') >> self.LowCtrl_Grp.attr('r')

		self.Cnt_Ctrl.attr('r') >> eyeRigObj.LidCnt_Grp.attr('r')

		# Connect to Eye Module - Lid Follow
		eyeRigObj.Cnt_Ctrl.add(ln='lidFollow', min=0, max=1, dv=0.25, k=True)

		self.Flw_Mdv = lrc.MultiplyDivide()
		self.Flw_Mdv.name = 'Flw%s%sMdv' % (part, side)

		eyeRigObj.Cnt_Ctrl.attr('r') >> self.Flw_Mdv.attr('i1')
		eyeRigObj.Cnt_Ctrl.attr('lidFollow') >> self.Flw_Mdv.attr('i2x')
		eyeRigObj.Cnt_Ctrl.attr('lidFollow') >> self.Flw_Mdv.attr('i2y')
		eyeRigObj.Cnt_Ctrl.attr('lidFollow') >> self.Flw_Mdv.attr('i2z')
		self.Flw_Mdv.attr('o') >> eyeRigObj.LidCntFlw_Grp.attr('r')
		self.Flw_Mdv.attr('o') >> self.PartCnt_Jnt.attr('r')

		# Socket Follow
		# Socket Follow - Up In
		eyeRigObj.LidUp_Ctrl.add(ln='sktInFollow', dv=0.3)
		self.LidUpSktInFollow_Mdv = lrc.MultiplyDivide()
		self.LidUpSktInFollow_Mdv.name = 'LidUpSktInFollow%s%sMdv' % (part, side)

		eyeRigObj.LidUp_Ctrl.attr('ty') >> self.LidUpSktInFollow_Mdv.attr('i1y')
		eyeRigObj.LidUp_Ctrl.attr('sktInFollow') >> self.LidUpSktInFollow_Mdv.attr('i2y')
		self.LidUpSktInFollow_Mdv.attr('o') >> self.UpInSktCtrlBfrT_Pma.last3D()

		# Socket Follow - Up Mid
		eyeRigObj.LidUp_Ctrl.add(ln='sktMidFollow', dv=0.6)
		self.LidUpSktMidFollow_Mdv = lrc.MultiplyDivide()
		self.LidUpSktMidFollow_Mdv.name = 'LidUpSktMidFollow%s%sMdv' % (part, side)

		eyeRigObj.LidUp_Ctrl.attr('ty') >> self.LidUpSktMidFollow_Mdv.attr('i1y')
		eyeRigObj.LidUp_Ctrl.attr('sktMidFollow') >> self.LidUpSktMidFollow_Mdv.attr('i2y')
		self.LidUpSktMidFollow_Mdv.attr('o') >> self.UpMidSktCtrlBfrT_Pma.last3D()

		# Socket Follow - Up Out
		eyeRigObj.LidUp_Ctrl.add(ln='sktOutFollow', dv=0.3)
		self.LidUpSktOutFollow_Mdv = lrc.MultiplyDivide()
		self.LidUpSktOutFollow_Mdv.name = 'LidUpSktOutFollow%s%sMdv' % (part, side)

		eyeRigObj.LidUp_Ctrl.attr('ty') >> self.LidUpSktOutFollow_Mdv.attr('i1y')
		eyeRigObj.LidUp_Ctrl.attr('sktOutFollow') >> self.LidUpSktOutFollow_Mdv.attr('i2y')
		self.LidUpSktOutFollow_Mdv.attr('o') >> self.UpOutSktCtrlBfrT_Pma.last3D()

		# Socket Follow - Low In
		eyeRigObj.LidLow_Ctrl.add(ln='sktInFollow', dv=0.3)
		self.LidLowSktInFollow_Mdv = lrc.MultiplyDivide()
		self.LidLowSktInFollow_Mdv.name = 'LidLowSktInFollow%s%sMdv' % (part, side)

		eyeRigObj.LidLow_Ctrl.attr('ty') >> self.LidLowSktInFollow_Mdv.attr('i1y')
		eyeRigObj.LidLow_Ctrl.attr('sktInFollow') >> self.LidLowSktInFollow_Mdv.attr('i2y')
		self.LidLowSktInFollow_Mdv.attr('o') >> self.LowInSktCtrlBfrT_Pma.last3D()

		# Socket Follow - Low Mid
		eyeRigObj.LidLow_Ctrl.add(ln='sktMidFollow', dv=0.6)
		self.LidLowSktMidFollow_Mdv = lrc.MultiplyDivide()
		self.LidLowSktMidFollow_Mdv.name = 'LidLowSktMidFollow%s%sMdv' % (part, side)

		eyeRigObj.LidLow_Ctrl.attr('ty') >> self.LidLowSktMidFollow_Mdv.attr('i1y')
		eyeRigObj.LidLow_Ctrl.attr('sktMidFollow') >> self.LidLowSktMidFollow_Mdv.attr('i2y')
		self.LidLowSktMidFollow_Mdv.attr('o') >> self.LowMidSktCtrlBfrT_Pma.last3D()

		# Socket Follow - Low Out
		eyeRigObj.LidLow_Ctrl.add(ln='sktOutFollow', dv=0.3)
		self.LidLowSktOutFollow_Mdv = lrc.MultiplyDivide()
		self.LidLowSktOutFollow_Mdv.name = 'LidLowSktOutFollow%s%sMdv' % (part, side)

		eyeRigObj.LidLow_Ctrl.attr('ty') >> self.LidLowSktOutFollow_Mdv.attr('i1y')
		eyeRigObj.LidLow_Ctrl.attr('sktOutFollow') >> self.LidLowSktOutFollow_Mdv.attr('i2y')
		self.LidLowSktOutFollow_Mdv.attr('o') >> self.LowOutSktCtrlBfrT_Pma.last3D()

		# Offsetting Translation
		for ix in xrange(len(tmpObjs)):

			# Getting current lid controller.
			currCtrl = eyeRigObj.LidUp_Ctrl
			if ix > len(tmpObjs)/2:
				currCtrl = eyeRigObj.LidLow_Ctrl
			currCtrlShp = lrc.Dag(currCtrl.shape)

			for iy in range(4):

				currIdx = ''
				if iy:
					currIdx = str(iy*25)

				currZrGrp = lrc.Dag('Tip%s%sJntZr_%s%sGrp' % (currIdx, part, str(ix+1), side))

				if not mc.objExists(currZrGrp):
					continue

				currAttr = 'Jnt%sPct%sOffAmp' % (str(ix+1), currIdx)
				currCtrlShp.add(ln=currAttr, k=True)

				currPma = lrc.PlusMinusAverage()
				currPma.name = 'LidCloseOffset%s%s%sPma' % (currIdx, part, side)
				currPma.add(ln='default', k=True)
				currPma.attr('default').v = currZrGrp.attr('ty').v

				currMdv = lrc.MultiplyDivide()
				currMdv.name = 'LidCloseOffset%s%s%sMdv' % (currIdx, part, side)
				currCtrl.attr('ty') >> currMdv.attr('i1x')
				currCtrlShp.attr(currAttr) >> currMdv.attr('i2x')

				currPma.attr('default') >> currPma.last1D()
				currMdv.attr('ox') >> currPma.last1D()
				currPma.attr('output1D') >> currZrGrp.attr('ty')

			# For socket
			currZrGrp = lrc.Dag('TipSkt%sJntZr_%s%sGrp' % (part, str(ix+1), side))
			
			currAttr = 'JntSkt%sOffAmp' % str(ix+1)
			currCtrlShp.add(ln=currAttr, k=True)

			currPma = lrc.PlusMinusAverage()
			currPma.name = 'LidCloseOffsetSkt%s%sPma' % (part, side)
			currPma.add(ln='default', k=True)
			currPma.attr('default').v = currZrGrp.attr('ty').v

			currMdv = lrc.MultiplyDivide()
			currMdv.name = 'LidCloseOffsetSkt%s%sMdv' % (part, side)
			currCtrl.attr('ty') >> currMdv.attr('i1x')
			currCtrlShp.attr(currAttr) >> currMdv.attr('i2x')

			currPma.attr('default') >> currPma.last1D()
			currMdv.attr('ox') >> currPma.last1D()
			currPma.attr('output1D') >> currZrGrp.attr('ty')

		# Cleanup
		self.Ctrl_Grp.parent(eyeRigObj.Ctrl_Grp)
		self.Jnt_Grp.parent(eyeRigObj.Jnt_Grp)
		self.Skin_Grp.parent(eyeRigObj.Skin_Grp)
		self.Still_Grp.parent(eyeRigObj.Still_Grp)

	def createMainJoint(self, tmpObj, jntPart, part, side):

		jnt = lrc.Joint()
		zrGrp = lrc.group(jnt)

		zrGrp.snap(tmpObj)

		jnt.name = '%s%s%sJnt' % (jntPart, part, side)
		zrGrp.name = '%s%sJntZr%sGrp' % (jntPart, part, side)

		return jnt, zrGrp

	def createControlJoint(self, oriLoc, cv, jntPart, part, side):

		jnt = lrc.Joint()
		zrGrp = lrc.group(jnt)

		pos = mc.xform(cv, q=True, t=True, ws=True)
		ori = mc.xform(oriLoc, q=True, ro=True, ws=True)

		jnt.name = '%s%s%sJnt' % (jntPart, part, side)
		zrGrp.name = '%s%sJntZr%sGrp' % (jntPart, part, side)

		mc.xform(zrGrp, ro=ori, t=pos, ws=True)

		return jnt, zrGrp

	def createMainControl(self, jnt, ctrlPart, part, side, lockAttrs):

		ctrl = lrc.Control('nrbCircle')
		ctrl.scaleShape(0.25)
		invGrp = lrc.group(ctrl)
		zrGrp = lrc.group(invGrp)

		bfrT = lrc.PlusMinusAverage()

		zrGrp.snap(jnt)

		ctrl.name = '%s%s%sCtrl' % (ctrlPart, part, side)
		invGrp.name = '%s%sCtrlInv%sGrp' % (ctrlPart, part, side)
		zrGrp.name = '%s%sCtrlZr%sGrp' % (ctrlPart, part, side)

		bfrT.name = '%s%sBfrT%sPma' % (ctrlPart, part, side)

		ctrl.attr('t') >> bfrT.last3D()
		ctrl.attr('r') >> jnt.attr('r')
		ctrl.attr('s') >> jnt.attr('s')

		bfrT.attr('output3D') >> jnt.attr('t')

		for attr in lockAttrs:
			ctrl.lockHideAttrs(attr)

		ctrl.attr('v').l = True
		ctrl.attr('v').h = True

		return ctrl, invGrp, zrGrp, bfrT

	def createPrcLidJoint(self, cntObj, endObj, cntOri, endOri, part, idx, side, prc, srcObjs):

		jnt = lrc.Joint()
		jntZrGrp = lrc.group(jnt)

		cntJnt = lrc.Joint()
		cntJntSdkGrp = lrc.group(cntJnt)
		cntJntZrGrp = lrc.group(cntJntSdkGrp)

		cntJntZrGrp.snap(cntOri)
		jntZrGrp.snapPoint(endObj)
		jntZrGrp.snapOrient(endOri)
		jntZrGrp.parent(cntJnt)

		jnt.name = 'Tip%s%s_%s%sJnt' % (str(prc), part, idx, side)
		jntZrGrp.name = 'Tip%s%sJntZr_%s%sGrp' % (str(prc), part, idx, side)
		cntJnt.name = 'Cnt%s%s_%s%sJnt' % (str(prc), part, idx, side)
		cntJntSdkGrp.name = 'Cnt%s%sJntSdk_%s%sGrp' % (str(prc), part, idx, side)
		cntJntZrGrp.name = 'Cnt%s%sJntZr_%s%sGrp' % (str(prc), part, idx, side)

		cntJnt.attr('drawStyle').v = 2

		cntJntZrGrp.parent(cntObj)

		rotColl = lrc.PlusMinusAverage()
		rotColl.name = 'Cnt%s%sJntRotColl_%s%sPma' % (str(prc), part, idx, side)
		
		rotAmpA = lrc.MultiplyDivide()
		rotAmpA.name = 'Cnt%s%sJntRotAmpA_%s%sMdv' % (str(prc), part, idx, side)

		rotAmpB = lrc.MultiplyDivide()
		rotAmpB.name = 'Cnt%s%sJntRotAmpB_%s%sMdv' % (str(prc), part, idx, side)
		
		srcObjs[0].attr('r') >> rotAmpA.attr('i1')
		srcObjs[1].attr('r') >> rotAmpB.attr('i1')

		rotAmpA.attr('i2').v = (float(prc)/100.00, float(prc)/100.00, float(prc)/100.00)
		rotAmpB.attr('i2').v = (1.00-float(prc)/100.00, 1.00-float(prc)/100.00, 1.00-float(prc)/100.00)
				
		rotAmpA.attr('o') >> rotColl.last3D()
		rotAmpB.attr('o') >> rotColl.last3D()
		rotColl.attr('output3D') >> cntJntSdkGrp.attr('r')

		return jnt, jntZrGrp, cntJnt, cntJntSdkGrp, cntJntZrGrp

	def createMainLidJoint(self, cntObj, endObj, tarObj, jntPart, part, idx, side, crvShpObj):

		# Target
		tar = lrc.Null()
		tar.name = 'Tar%s%s_%s_%sGrp' % (jntPart, part, idx, side)

		tarZrGrp = lrc.group(tar)
		tarZrGrp.snap(endObj)
		tarZrGrp.name = 'Tar%s%sZr_%s_%sGrp' % (jntPart, part, idx, side)

		# Target on curve
		rvt = lrc.Null()
		rvt.name = 'Tar%s%sRvt_%s%sGrp' % (jntPart, part, idx, side)

		# Finding nearest point on curve
		npoc = lrc.NearestPointOnCurve()

		crvShpObj.attr('worldSpace[0]') >> npoc.attr('ic')
		npoc.attr('inPosition').v = mc.xform(endObj, q=True, t=True, ws=True)
		par = npoc.attr('parameter').v

		mc.delete(npoc)

		# Setup point on curve info
		poci = lrc.PointOnCurveInfo()
		poci.name = 'Tar%s%sRvt_%s%sPoci' % (jntPart, part, idx, side)

		crvShpObj.attr('worldSpace[0]') >> poci.attr('ic')
		poci.attr('position') >> rvt.attr('t')

		poci.attr('parameter').value = par

		mc.parentConstraint(rvt, tarZrGrp, mo=True)

		# Create joints
		(
		jnt,
		jntZrGrp,
		cntJnt,
		cntJntSdkGrp,
		cntJntZrGrp
		) = createRpJoint(
							tmpCntObj=cntObj,
							tmpObj=endObj,
							tmpTarObj=tarObj
						)

		jnt.name = 'Tip%s%s_%s%sJnt' % (jntPart, part, idx, side)
		jntZrGrp.name = 'Tip%s%sJntZr_%s%sGrp' % (jntPart, part, idx, side)
		cntJnt.name = 'Cnt%s%s_%s%sJnt' % (jntPart, part, idx, side)
		cntJntSdkGrp.name = 'Cnt%s%sJntSdk_%s%sGrp' % (jntPart, part, idx, side)
		cntJntZrGrp.name = 'Cnt%s%sJntZr_%s%sGrp' % (jntPart, part, idx, side)

		cntJnt.attr('drawStyle').v = 2

		cntJntZrGrp.parent(cntObj)

		lrc.aimConstraint(
								tar,
								cntJntSdkGrp,
								aim=(0, 1, 0),
								u=(1, 0, 0),
								wut='objectrotation',
								wuo=cntJntZrGrp,
								wu=(1, 0, 0)
							)

		# Ctrl
		ctrl = lrc.Control('cube')
		ctrl.name = 'Tip%s%s_%s%sCtrl' % (jntPart, part, idx, side)
		ctrl.color = 'yellow'
		ctrl.lockHideAttrs('v')

		ctrlInvGrp = lrc.group(ctrl)
		ctrlInvGrp.name = 'Tip%s%sCtrlInv_%s%sGrp' % (jntPart, part, idx, side)

		ctrlZrGrp = lrc.group(ctrlInvGrp)
		ctrlZrGrp.name = 'Tip%s%sCtrlZr_%s%sGrp' % (jntPart, part, idx, side)

		ctrlZrGrp.snap(jnt)
		lrc.orientConstraint(jntZrGrp, ctrlZrGrp)

		ctrl.attr('t') >> jnt.attr('t')
		ctrl.attr('s') >> jnt.attr('s')
		ctrl.attr('r') >> cntJnt.attr('r')

		return jnt, jntZrGrp, cntJnt, cntJntSdkGrp, cntJntZrGrp, tar, tarZrGrp, rvt, ctrl, ctrlInvGrp, ctrlZrGrp

	def populateTarObjs(self, objs=[]):

		'''
		Populate lid target objects based on given objs
		'''

		tarObjs = []

		# tarObjs - Start with outer corner
		firstTar = (len(objs)/2)
		tarObjs.append(objs[firstTar])

		# tarObjs - Upper lid
		for ix in range(len(objs)-1, firstTar, -1):
			tarObjs.append(objs[ix])

		# tarObjs - For inner corner
		tarObjs.append(objs[0])

		# tarObjs - Lower lid
		for ix in range(firstTar-1, 0, -1):
			tarObjs.append(objs[ix])

		return tarObjs

	def locatorOnCurve(self, crvObjs=[]):

		locs = []
		
		for crv in crvObjs:
			for ix in range(crv.attr('spans').v):
				loc = lrc.Locator()
				loc.attr('t').v = mc.xform('%s.cv[%s]' % (crv, ix), q=True, ws=True, t=True)
				locs.append(loc)

		return locs

class EyeRig(object):

	def __init__(
					self,
					cntTmpLoc='',
					irisTmpLoc='',
					pupilTmpLoc='',
					lidUpTmpLoc='',
					lidLowTmpLoc='',
					part='',
					side=''
				):

		if side:
			side = '_%s_' % side
		else:
			side = '_'

		# Define color
		col = 'red'
		darkCol = 'darkRed'
		if side == '_R_':
			col = 'blue'
			darkCol = 'darkBlue'

		# Main Group
		self.Ctrl_Grp = lrc.Null()
		self.Ctrl_Grp.name = '%sCtrl%sGrp' % (part, side)

		self.Jnt_Grp = lrc.Null()
		self.Jnt_Grp.name = '%sJnt%sGrp' % (part, side)

		self.Skin_Grp = lrc.Null()
		self.Skin_Grp.name = '%sSkin%sGrp' % (part, side)

		self.Still_Grp = lrc.Null()
		self.Still_Grp.name = '%sStill%sGrp' % (part, side)

		# Joints
		self.Cnt_Jnt = lrr.jointAt(cntTmpLoc)
		self.Cnt_Jnt.name = 'Cnt%s%sJnt' % (part, side)

		self.CntJntZr_Grp = lrr.groupAt(self.Cnt_Jnt)
		self.CntJntZr_Grp.name = 'Cnt%sJntZr%sGrp' % (part, side)
		self.CntJntZr_Grp.parent(self.Skin_Grp)

		self.Iris_Jnt = lrr.jointAt(irisTmpLoc)
		self.Iris_Jnt.name = 'Iris%s%sJnt' % (part, side)

		self.IrisJntZr_Grp = lrr.groupAt(self.Iris_Jnt)
		self.IrisJntZr_Grp.name = 'Iris%sJntZr%sGrp' % (part, side)
		self.IrisJntZr_Grp.parent(self.Cnt_Jnt)

		self.Pu_Jnt = lrr.jointAt(pupilTmpLoc)
		self.Pu_Jnt.name = 'Pu%s%sJnt' % (part, side)

		self.PuJntZr_Grp = lrr.groupAt(self.Pu_Jnt)
		self.PuJntZr_Grp.name = 'Pu%sJntZr%sGrp' % (part, side)
		self.PuJntZr_Grp.parent(self.Iris_Jnt)

		# Controller - Cnt
		self.Cnt_Ctrl = lrc.Control('sphere')
		self.Cnt_Ctrl.name = 'Cnt%s%sCtrl' % (part, side)
		self.Cnt_Ctrl.lockHideAttrs('v')
		self.Cnt_Ctrl.color = col
		
		self.CntCtrlOri_Grp = lrc.group(self.Cnt_Ctrl)
		self.CntCtrlOri_Grp.name = 'Cnt%sCtrlOri%sGrp' % (part, side)
		
		self.CntCtrlZr_Grp = lrc.group(self.CntCtrlOri_Grp)
		self.CntCtrlZr_Grp.name = 'Cnt%sCtrlZr%sGrp' % (part, side)
		self.CntCtrlZr_Grp.snap(self.Cnt_Jnt)
		self.CntCtrlZr_Grp.parent(self.Ctrl_Grp)

		# Accumilate rotation
		self.JntTransAccum_Pma = lrc.PlusMinusAverage()
		self.JntTransAccum_Pma.name = '%sJntTransAccum%sPma' % (part, side)

		self.JntRotAccum_Pma = lrc.PlusMinusAverage()
		self.JntRotAccum_Pma.name = '%sJntRotAccum%sPma' % (part, side)

		self.JntScaAccum_Pma = lrc.PlusMinusAverage()
		self.JntScaAccum_Pma.name = '%sJntScaAccum%sPma' % (part, side)

		# self.CntCtrlOri_Grp.attr('t') >> self.JntTransAccum_Pma.last3D()
		self.CntCtrlOri_Grp.attr('r') >> self.JntRotAccum_Pma.last3D()
		# self.CntCtrlOri_Grp.attr('s') >> self.JntScaAccum_Pma.last3D()

		self.Cnt_Ctrl.attr('t') >> self.JntTransAccum_Pma.last3D()
		self.Cnt_Ctrl.attr('r') >> self.JntRotAccum_Pma.last3D()
		self.Cnt_Ctrl.attr('s') >> self.JntScaAccum_Pma.last3D()

		self.JntTransAccum_Pma.attr('output3D') >> self.Cnt_Jnt.attr('t')
		self.JntRotAccum_Pma.attr('output3D') >> self.Cnt_Jnt.attr('r')
		self.JntScaAccum_Pma.attr('output3D') >> self.Cnt_Jnt.attr('s')

		# Controller - Iris
		self.Iris_Ctrl = lrc.Control('circle')
		self.Iris_Ctrl.name = 'Iris%s%sCtrl' % (part, side)
		self.Iris_Ctrl.lockHideAttrs('v')
		self.Iris_Ctrl.color = col

		self.IrisCtrlZr_Grp = lrc.group(self.Iris_Ctrl)
		self.IrisCtrlZr_Grp.name = 'Iris%sCtrlZr%sGrp' % (part, side)
		self.IrisCtrlZr_Grp.snap(self.Iris_Jnt)
		self.IrisCtrlZr_Grp.parent(self.Cnt_Ctrl)

		self.Iris_Ctrl.attr('t') >> self.Iris_Jnt.attr('t')
		self.Iris_Ctrl.attr('r') >> self.Iris_Jnt.attr('r')
		self.Iris_Ctrl.attr('s') >> self.Iris_Jnt.attr('s')

		# Controller - Pu
		self.Pu_Ctrl = lrc.Control('circle')
		self.Pu_Ctrl.name = 'Pu%s%sCtrl' % (part, side)
		self.Pu_Ctrl.lockHideAttrs('v')
		self.Pu_Ctrl.color = darkCol

		self.PuCtrlZr_Grp = lrc.group(self.Pu_Ctrl)
		self.PuCtrlZr_Grp.name = 'Pu%sCtrlZr%sGrp' % (part, side)
		self.PuCtrlZr_Grp.snap(self.Pu_Jnt)
		self.PuCtrlZr_Grp.parent(self.Iris_Ctrl)

		self.Pu_Ctrl.attr('t') >> self.Pu_Jnt.attr('t')
		self.Pu_Ctrl.attr('r') >> self.Pu_Jnt.attr('r')
		self.Pu_Ctrl.attr('s') >> self.Pu_Jnt.attr('s')

		# Lid Grp - Cnt
		self.LidCnt_Grp = lrc.Null()
		self.LidCnt_Grp.name = 'LidCnt%s%sGrp' % (part, side)

		self.LidCntFlw_Grp = lrc.group(self.LidCnt_Grp)
		self.LidCntFlw_Grp.name = 'LidCntFlw%s%sGrp' % (part, side)

		self.LidCntZr_Grp = lrc.group(self.LidCntFlw_Grp)
		self.LidCntZr_Grp.name = 'LidCnt%sZr%sGrp' % (part, side)
		self.LidCntZr_Grp.snap(cntTmpLoc)
		self.LidCntZr_Grp.parent(self.Ctrl_Grp)

		self.Cnt_Ctrl.attr('s') >> self.LidCntFlw_Grp.attr('s')

		# Lid Control - Up
		self.LidUp_Ctrl = lrc.Control('nrbCircle')
		self.LidUp_Ctrl.name = 'LidUp%s%sCtrl' % (part, side)
		mc.transformLimits(self.LidUp_Ctrl, ty=(-1, 1))
		self.LidUp_Ctrl.lockHideAttrs('tz', 'rx', 'ry', 'sx', 'sy', 'sz','v')
		self.LidUp_Ctrl.color = col
		self.LidUp_Ctrl.scaleShape(0.5)

		self.LidUpCtrlInv_Grp = lrc.group(self.LidUp_Ctrl)
		self.LidUpCtrlInv_Grp.name = 'LidUp%sCtrlInv%sGrp' % (part, side)

		self.LidUpCtrlZr_Grp = lrc.group(self.LidUpCtrlInv_Grp)
		self.LidUpCtrlZr_Grp.name = 'LidUp%sCtrlZr%sGrp' % (part, side)
		self.LidUpCtrlZr_Grp.snap(lidUpTmpLoc)
		self.LidUpCtrlZr_Grp.parent(self.LidCnt_Grp)

		# Lid Control - Low
		self.LidLow_Ctrl = lrc.Control('nrbCircle')
		self.LidLow_Ctrl.name = 'LidLow%s%sCtrl' % (part, side)
		mc.transformLimits(self.LidLow_Ctrl, ty=(-1, 1))
		self.LidLow_Ctrl.lockHideAttrs('tz', 'rx', 'ry', 'sx', 'sy', 'sz','v')
		self.LidLow_Ctrl.color = darkCol
		self.LidLow_Ctrl.scaleShape(0.5)

		self.LidLowCtrlInv_Grp = lrc.group(self.LidLow_Ctrl)
		self.LidLowCtrlInv_Grp.name = 'LidLow%sCtrlInv%sGrp' % (part, side)

		self.LidLowCtrlZr_Grp = lrc.group(self.LidLowCtrlInv_Grp)
		self.LidLowCtrlZr_Grp.name = 'LidLow%sCtrlZr%sGrp' % (part, side)
		self.LidLowCtrlZr_Grp.snap(lidLowTmpLoc)
		self.LidLowCtrlZr_Grp.parent(self.LidCnt_Grp)

def main(do25=True, do50=True, do75=True):

	ctrlGrp = lrc.Null()
	ctrlGrp.name = 'EyeRigCtrl_Grp'

	jntGrp = lrc.Null()
	jntGrp.name = 'EyeRigJnt_Grp'

	skinGrp = lrc.Null()
	skinGrp.name = 'EyeRigSkin_Grp'

	stillGrp = lrc.Null()
	stillGrp.name = 'EyeRigStill_Grp'

	eyeLftRigObj = EyeRig(
							cntTmpLoc='CntEyeRigTmp_L_Loc',
							irisTmpLoc='IrisEyeRigTmp_L_Loc',
							pupilTmpLoc='PupilEyeRigTmp_L_Loc',
							lidUpTmpLoc='LidUpEyeRigTmp_L_Loc',
							lidLowTmpLoc='LidLowEyeRigTmp_L_Loc',
							part='EyeRig',
							side='L'
						)

	lidLftRigObj = LidRig(
							cntTmpLoc='CntLidRigTmp_L_Loc',
							upTmpLoc='UpLidRigTmp_L_Loc',
							lowTmpLoc='LowLidRigTmp_L_Loc',
							ctrlOriTmpLoc='CtrlOriLidRigTmp_L_Loc',
							ctrlPosTmpCrv='CtrlPosLidRigTmp_L_Crv',
							upTmpCrv='UpLidRigTmp_L_Crv',
							lowTmpCrv='LowLidRigTmp_L_Crv',
							upSktTmpCrv='UpSktLidRigTmp_L_Crv',
							lowSktTmpCrv='LowSktLidRigTmp_L_Crv',
							up25TmpCrv='Up25LidRigTmp_L_Crv',
							low25TmpCrv='Low25LidRigTmp_L_Crv',
							up50TmpCrv='Up50LidRigTmp_L_Crv',
							low50TmpCrv='Low50LidRigTmp_L_Crv',
							up75TmpCrv='Up75LidRigTmp_L_Crv',
							low75TmpCrv='Low75LidRigTmp_L_Crv',
							tarTmpCrv='LidRigTmp_L_Crv',
							tarSktTmpCrv='SktLidRigTmp_L_Crv',
							sktOriTmpLoc='SktCtrlOriLidRigTmp_L_Loc',
							sktPosTmpCrv='SktCtrlPosLidRigTmp_L_Crv',
							part='LidRig',
							side='L',
							do25=do25,
							do50=do50,
							do75=do75,
							eyeRigObj=eyeLftRigObj
						)
	eyeLftRigObj.Ctrl_Grp.parent(ctrlGrp)
	eyeLftRigObj.Jnt_Grp.parent(jntGrp)
	eyeLftRigObj.Skin_Grp.parent(skinGrp)
	eyeLftRigObj.Still_Grp.parent(stillGrp)

	eyeRgtRigObj = EyeRig(
							cntTmpLoc='CntEyeRigTmp_R_Loc',
							irisTmpLoc='IrisEyeRigTmp_R_Loc',
							pupilTmpLoc='PupilEyeRigTmp_R_Loc',
							lidUpTmpLoc='LidUpEyeRigTmp_R_Loc',
							lidLowTmpLoc='LidLowEyeRigTmp_R_Loc',
							part='EyeRig',
							side='R'
						)

	lidRgtRigObj = LidRig(
							cntTmpLoc='CntLidRigTmp_R_Loc',
							upTmpLoc='UpLidRigTmp_R_Loc',
							lowTmpLoc='LowLidRigTmp_R_Loc',
							ctrlOriTmpLoc='CtrlOriLidRigTmp_R_Loc',
							ctrlPosTmpCrv='CtrlPosLidRigTmp_R_Crv',
							upTmpCrv='UpLidRigTmp_R_Crv',
							lowTmpCrv='LowLidRigTmp_R_Crv',
							upSktTmpCrv='UpSktLidRigTmp_R_Crv',
							lowSktTmpCrv='LowSktLidRigTmp_R_Crv',
							up25TmpCrv='Up25LidRigTmp_R_Crv',
							low25TmpCrv='Low25LidRigTmp_R_Crv',
							up50TmpCrv='Up50LidRigTmp_R_Crv',
							low50TmpCrv='Low50LidRigTmp_R_Crv',
							up75TmpCrv='Up75LidRigTmp_R_Crv',
							low75TmpCrv='Low75LidRigTmp_R_Crv',
							tarTmpCrv='LidRigTmp_R_Crv',
							tarSktTmpCrv='SktLidRigTmp_R_Crv',
							sktOriTmpLoc='SktCtrlOriLidRigTmp_R_Loc',
							sktPosTmpCrv='SktCtrlPosLidRigTmp_R_Crv',
							part='LidRig',
							side='R',
							do25=do25,
							do50=do50,
							do75=do75,
							eyeRigObj=eyeRgtRigObj
						)
	eyeRgtRigObj.Ctrl_Grp.parent(ctrlGrp)
	eyeRgtRigObj.Jnt_Grp.parent(jntGrp)
	eyeRgtRigObj.Skin_Grp.parent(skinGrp)
	eyeRgtRigObj.Still_Grp.parent(stillGrp)

def mainLite():

	ctrlGrp = lrc.Null()
	ctrlGrp.name = 'EyeRigCtrl_Grp'

	jntGrp = lrc.Null()
	jntGrp.name = 'EyeRigJnt_Grp'

	skinGrp = lrc.Null()
	skinGrp.name = 'EyeRigSkin_Grp'

	stillGrp = lrc.Null()
	stillGrp.name = 'EyeRigStill_Grp'

	eyeLftRigObj = EyeRig(
							cntTmpLoc='CntEyeRigTmp_L_Loc',
							irisTmpLoc='IrisEyeRigTmp_L_Loc',
							pupilTmpLoc='PupilEyeRigTmp_L_Loc',
							lidUpTmpLoc='LidUpEyeRigTmp_L_Loc',
							lidLowTmpLoc='LidLowEyeRigTmp_L_Loc',
							part='EyeRig',
							side='L'
						)

	eyeLftRigObj.Ctrl_Grp.parent(ctrlGrp)
	eyeLftRigObj.Jnt_Grp.parent(jntGrp)
	eyeLftRigObj.Skin_Grp.parent(skinGrp)
	eyeLftRigObj.Still_Grp.parent(stillGrp)

	eyeRgtRigObj = EyeRig(
							cntTmpLoc='CntEyeRigTmp_R_Loc',
							irisTmpLoc='IrisEyeRigTmp_R_Loc',
							pupilTmpLoc='PupilEyeRigTmp_R_Loc',
							lidUpTmpLoc='LidUpEyeRigTmp_R_Loc',
							lidLowTmpLoc='LidLowEyeRigTmp_R_Loc',
							part='EyeRig',
							side='R'
						)

	eyeRgtRigObj.Ctrl_Grp.parent(ctrlGrp)
	eyeRgtRigObj.Jnt_Grp.parent(jntGrp)
	eyeRgtRigObj.Skin_Grp.parent(skinGrp)
	eyeRgtRigObj.Still_Grp.parent(stillGrp)
