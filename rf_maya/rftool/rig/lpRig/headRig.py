import maya.cmds as mc

import lpRig.core as lrc
reload(lrc)
import lpRig.rigTools as lrr
reload(lrr)

class EyeRig(object):

	def __init__(
					self,
					eyeJnt=None,
					tarJnt=None,
					hdJnt=None,
					part='',
					side=''
				):

		if side:
			side = '_%s_' % side
		else:
			side = '_'

		# Define color
		col = 'red'
		if side == '_R_': col = 'blue'

		# Eye joint
		self.Eye_Jnt = lrc.Dag(eyeJnt)

		self.Eye_Jnt.attr('ssc').v = 0

		# Eye controller
		self.Eye_Ctrl = lrr.jointControl('sphere')
		self.Eye_Ctrl.name = "Eye%s%sCtrl" % (part, side)
		self.Eye_Ctrl.color = col
		self.Eye_Ctrl.lockHideAttrs('v')

		self.EyeGmbl_Ctrl = lrc.addGimbal(self.Eye_Ctrl)
		self.EyeGmbl_Ctrl.name = "EyeGmbl%s%sCtrl" % (part, side)

		self.EyeCtrlAim_Grp = lrc.group(self.Eye_Ctrl)
		self.EyeCtrlAim_Grp.name = "EyeCtrlAim%s%sGrp" % (part, side)

		self.EyeCtrlZr_Grp = lrc.group(self.EyeCtrlAim_Grp)
		self.EyeCtrlZr_Grp.name = "EyeCtrlZr%s%sGrp" % (part, side)

		self.EyeCtrlZr_Grp.snap(self.Eye_Jnt)

		# Eye target controller
		self.Tar_Ctrl = lrc.Control('plus')
		self.Tar_Ctrl.name = "EyeTar%s%sCtrl" % (part, side)
		self.Tar_Ctrl.color = col
		self.Tar_Ctrl.lockHideAttrs('rx', 'ry', 'rz', 'sx', 'sy', 'sz', 'v')

		self.TarCtrlZr_Grp = lrc.group(self.Tar_Ctrl)
		self.TarCtrlZr_Grp.name = "EyeTarCtrlZr%s%sGrp" % (part, side)
		
		self.TarCtrlZr_Grp.snap(tarJnt)
		
		mc.aimConstraint(
							self.Tar_Ctrl,
							self.EyeCtrlAim_Grp,
							aimVector=(0, 0, 1),
							upVector=(0, 1, 0),
							worldUpType="objectrotation",
							worldUpVector=(0, 1, 0),
							worldUpObject=hdJnt
						)
		
		# Lid joint
		# self.Lid_Jnt = lrr.jointAt(self.Eye_Jnt)
		# self.Lid_Jnt.name = "Lid%s%sJnt" % (part, side)
		# self.Lid_Jnt.attr('ssc').v = 0

		# self.LidJntZr_Grp = lrr.groupAt(self.Lid_Jnt)
		# self.LidJntZr_Grp.name = "LidJntZr%s%sJnt" % (part, side)

		# self.LidJntZr_Grp.parent(hdJnt)

		# lrc.pointConstraint(self.Eye_Jnt, self.Lid_Jnt)
		# lrc.scaleConstraint(self.Eye_Jnt, self.Lid_Jnt)

		# Lid Control
		# self.Eye_Ctrl.add(ln='lidFollow', min=0, max=1, k=True)
		# lidOriCons = lrc.orientConstraint(
		# 									self.LidJntZr_Grp,
		# 									self.Eye_Jnt,
		# 									self.Lid_Jnt,
		# 									mo=True
		# 								)
		
		# self.LidFollow_Rev = lrc.Reverse()
		# self.LidFollow_Rev.name = "LidFollow%s%sRev" % (part, side)

		# self.Eye_Ctrl.attr('lidFollow') >> lidOriCons.attr('w1')
		# self.Eye_Ctrl.attr('lidFollow') >> self.LidFollow_Rev.attr('ix')
		# self.LidFollow_Rev.attr('ox') >> lidOriCons.attr('w0')

		# Eye Guidance Curve
		(self.EyeCtrl_Crv,
		self.EyeCtrlA_Cls,
		self.EyeCtrlB_Cls) = lrr.crvGuide(
												ctrl = self.Eye_Ctrl,
												target = self.Tar_Ctrl
											)
		self.EyeCtrl_Crv.name = 'EyeCtrl%s%sCrv' % (part, side)
		self.EyeCtrlA_Cls.name = 'EyeCtrl1%s%sCls' % (part, side)
		self.EyeCtrlB_Cls.name = 'EyeCtrl2%s%sCls' % (part, side)

		lrr.renameClsElms(self.EyeCtrlA_Cls)
		lrr.renameClsElms(self.EyeCtrlB_Cls)
		
		self.EyeCtrl_Crv.attr('inheritsTransform').v = 0
		self.EyeCtrl_Crv.attr('overrideEnabled').v = 1
		self.EyeCtrl_Crv.attr('overrideDisplayType').v = 2
		
		# self.EyeCtrl_Crv.parent(self.Ctrl_Grp)
		self.EyeCtrl_Crv.attr('t').v = (0,0,0)
		self.EyeCtrl_Crv.attr('r').v = (0,0,0)

		# Connect to joint
		mc.parentConstraint(self.EyeGmbl_Ctrl, self.Eye_Jnt, mo=True)
		mc.scaleConstraint(self.EyeGmbl_Ctrl, self.Eye_Jnt)

class IrisRig(object):

	def __init__(
					self,
					parent='',
					eyeGmbl='EyeGmbl_L_Ctrl',
					tmpJnt=['Iris_L_Jnt', 'Pupil_L_Jnt'],
					part='',
					side=''
				):

		if side:
			side = '_%s_' % side
		else:
			side = '_'
		
		self.Iris_Jnt = lrc.Dag(tmpJnt[0])
		self.Pupil_Jnt = lrc.Dag(tmpJnt[1])
				
		self.Iris_Jnt.attr('ssc').v = 0
		self.Pupil_Jnt.attr('ssc').v = 0
		
		# Controllers
		self.Iris_Ctrl = lrc.Control('circle')
		self.Iris_Ctrl.name = 'Iris%s%sCtrl' % (part, side)
		self.Iris_Ctrl.lockHideAttrs('v')
		self.Iris_Ctrl.rotateShape((90, 0, 0))
		self.Iris_Ctrl.scaleShape(0.25)
		
		self.IrisCtrlZr_Grp = lrc.group(self.Iris_Ctrl)
		self.IrisCtrlZr_Grp.name = 'IrisCtrlZr%s%sGrp' % (part, side)
		
		self.Pupil_Ctrl = lrc.Control('circle')
		self.Pupil_Ctrl.name = 'Pupil%s%sCtrl' % (part, side)
		self.Pupil_Ctrl.lockHideAttrs('v')
		self.Pupil_Ctrl.rotateShape((90, 0, 0))
		self.Pupil_Ctrl.scaleShape(0.15)
		
		self.PupilCtrlZr_Grp = lrc.group(self.Pupil_Ctrl)
		self.PupilCtrlZr_Grp.name = 'PupilCtrlZr%s%sGrp' % (part, side)

		if side == '_R_':
			self.Iris_Ctrl.color = 'blue'
			self.Pupil_Ctrl.color = 'darkBlue'
		else:
			self.Iris_Ctrl.color = 'red'
			self.Pupil_Ctrl.color = 'darkRed'
		
		# Controllers - parenting and positioning
		tmpObj = lrc.Null()
		tmpObj.snap(eyeGmbl)
		tmpTar = lrc.Null()
		tmpTar.snapPoint(self.Iris_Jnt)
		tmpTar.parent(tmpObj)
		tmpTar.attr('ty').v = 0
		tmpTar.parent()
		lrr.localAim(
						tmpTar,
						tmpObj,
						(0,0,1),
						(0,1,0)
					)
		tmpTar.snap(self.Iris_Jnt)
		lrr.localAim(
						tmpTar,
						tmpObj,
						(0,0,1),
						(1,0,0)
					)
		
		self.PupilCtrlZr_Grp.parent(self.Iris_Ctrl)
		self.IrisCtrlZr_Grp.snapPoint(self.Iris_Jnt)
		self.IrisCtrlZr_Grp.snapOrient(tmpObj)
		self.PupilCtrlZr_Grp.snapPoint(self.Pupil_Jnt)
		self.IrisCtrlZr_Grp.parent(eyeGmbl)
		
		mc.delete(tmpObj)
		mc.delete(tmpTar)
		
		# Controllers - connect to joint
		mc.parentConstraint(self.Iris_Ctrl, self.Iris_Jnt)
		mc.parentConstraint(self.Pupil_Ctrl, self.Pupil_Jnt)
		mc.scaleConstraint(self.Iris_Ctrl, self.Iris_Jnt)
		mc.scaleConstraint(self.Pupil_Ctrl, self.Pupil_Jnt)

class EyeSpecRig(object):

	def __init__(
					self,
					parent='',
					ws='Head_Jnt',
					eyeGmbl='EyeGmbl_L_Ctrl',
					tmpJnt='EyeSpec_L_Jnt',
					part='',
					side=''
				):

		if side:
			side = '_%s_' % side
		else:
			side = '_'

		self.EyeSpec_Jnt = lrc.Dag(tmpJnt)
		self.EyeSpec_Jnt.attr('ssc').v = 0

		# Controller
		self.EyeSpec_Ctrl = lrc.Control('circle')
		self.EyeSpec_Ctrl.name = 'EyeSpec%s%sCtrl' % (part, side)
		self.EyeSpec_Ctrl.lockHideAttrs('sx', 'sy', 'sz', 'v')
		self.EyeSpec_Ctrl.rotateShape((90, 0, 0))
		self.EyeSpec_Ctrl.scaleShape(0.25)
		self.EyeSpec_Ctrl.moveShape(
										(
											0,
											0,
											lrc.distance(eyeGmbl, self.EyeSpec_Jnt)
										)
									)

		if side == '_R_':
			self.EyeSpec_Ctrl.color = 'blue'
		else:
			self.EyeSpec_Ctrl.color = 'red'

		self.EyeSpecCtrlZr_Grp = lrc.group(self.EyeSpec_Ctrl)
		self.EyeSpecCtrlZr_Grp.name = 'EyeSpecCtrlZr%s%sGrp' % (part, side)
		
		self.EyeSpecPos_Grp = lrc.Null()
		self.EyeSpecPos_Grp.name = 'EyeSpecPos%s%sGrp' % (part, side)

		self.EyeSpecPosZr_Grp = lrc.group(self.EyeSpecPos_Grp)
		self.EyeSpecPosZr_Grp.name = 'EyeSpecPosZr%s%sGrp' % (part, side)

		self.EyeSpecPosZr_Grp.parent(self.EyeSpec_Ctrl)

		self.EyeSpecCtrlZr_Grp.snap(eyeGmbl)

		tmpObj = lrc.Null()
		tmpObj.snap(eyeGmbl)
		tmpTar = lrc.Null()
		tmpTar.snapPoint(self.EyeSpec_Jnt)
		tmpTar.parent(tmpObj)
		tmpTar.attr('ty').v = 0
		tmpTar.parent()
		lrr.localAim(
						tmpTar,
						tmpObj,
						(0,0,1),
						(0,1,0)
					)
		tmpTar.snap(self.EyeSpec_Jnt)
		lrr.localAim(
						tmpTar,
						tmpObj,
						(0,0,1),
						(1,0,0)
					)
		
		self.EyeSpecCtrlZr_Grp.snapOrient(tmpObj)
		self.EyeSpecPosZr_Grp.snapPoint(self.EyeSpec_Jnt)
		self.EyeSpecPosZr_Grp.attr('r').v = (0,0,0)
		self.EyeSpecPosZr_Grp.lockAttrs('tx', 'ty', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz')
		mc.delete(tmpObj)
		mc.delete(tmpTar)
		
		(self.EyeSpecCtrlLoc_Grp,
		self.EyeSpecCtrlWor_Grp,
		self.EyeSpecCtrlWor_Grp_orientConstraint,
		self.EyeSpecCtrlZr_Grp_orientConstraint,
		self.EyeSpecCtrlZrGrpOri_Rev) = lrr.oriLocWor(
															self.EyeSpec_Ctrl,
															eyeGmbl,
															ws,
															self.EyeSpecCtrlZr_Grp
														)

		self.EyeSpecCtrlLoc_Grp.name = "EyeSpecCtrlLoc%s%sGrp" % (part, side)
		self.EyeSpecCtrlWor_Grp.name = "EyeSpecCtrlWor%s%sGrp" % (part, side)
		self.EyeSpecCtrlZrGrpOri_Rev.name = "EyeSpecCtrlZrGrpOri%s%sRev" % (part, side)

		self.EyeSpecCtrlZr_Grp.parent(eyeGmbl)

		# Offset and scale control
		self.EyeSpec_Ctrl.add(ln='offset', k=True)
		self.EyeSpecOffsetAmp_Mul = lrr.attrAmper(
													self.EyeSpec_Ctrl.attr('offset'),
													self.EyeSpecPos_Grp.attr('tz'),
													dv = 0.1
												)
		self.EyeSpecOffsetAmp_Mul.name = "EyeSpecOffsetAmp%s%sMul" % (part, side)

		self.EyeSpec_Ctrl.add(ln='size', k=True)
		self.EyeSpecSizeAmp_Mul = lrr.attrAmper(
													self.EyeSpec_Ctrl.attr('size'),
													self.EyeSpecPos_Grp.attr('sx'),
													dv = 0.1
												)
		self.EyeSpecSizeAmp_Mul.name = "EyeSpecSizeAmp%s%sMul" % (part, side)

		self.EyeSpecSize_Add = lrc.AddDoubleLinear()
		self.EyeSpecSize_Add.name = "EyeSpecSize%s%sAdd" % (part, side)

		self.EyeSpecSize_Add.add(ln='default', k=True, dv=1)
		self.EyeSpecSize_Add.attr('default') >> self.EyeSpecSize_Add.attr('i1')

		self.EyeSpecSizeAmp_Mul.attr('o') >> self.EyeSpecSize_Add.attr('i2')

		self.EyeSpecSize_Add.attr('o') >> self.EyeSpecPos_Grp.attr('sx')
		self.EyeSpecSize_Add.attr('o') >> self.EyeSpecPos_Grp.attr('sy')
		self.EyeSpecSize_Add.attr('o') >> self.EyeSpecPos_Grp.attr('sz')

		mc.parentConstraint(self.EyeSpecPos_Grp, self.EyeSpec_Jnt)
		mc.scaleConstraint(self.EyeSpecPos_Grp, self.EyeSpec_Jnt)
		
		self.EyeSpec_Ctrl.attr('localWorld').v = 1

class HeadRig(object):

	def __init__(
					self,
					parent='',
					ctrlGrp='',
					tmpJnt=[
								'Head_Jnt',
								'HeadTip_Jnt',
								'Eye_L_Jnt',
								'Eye_R_Jnt',
								'JawA_Jnt',
								'JawB_Jnt',
								'JawBTip_Jnt',
								'EyeTar_Jnt',
								'EyeTar_L_Jnt',
								'EyeTar_R_Jnt'
							],
					part=''
				):

		# Skin joints
		self.Head_Jnt = lrc.Dag(tmpJnt[0])
		self.HeadTip_Jnt = lrc.Dag(tmpJnt[1])
		self.Eye_L_Jnt = lrc.Dag(tmpJnt[2])
		self.Eye_R_Jnt = lrc.Dag(tmpJnt[3])
		self.JawA_Jnt = lrc.Dag(tmpJnt[4])
		self.JawB_Jnt = lrc.Dag(tmpJnt[5])
		self.JawBTip_Jnt = lrc.Dag(tmpJnt[6])
		self.EyeTar_Jnt = lrc.Dag(tmpJnt[7])
		self.EyeTar_L_Jnt = lrc.Dag(tmpJnt[8])
		self.EyeTar_R_Jnt = lrc.Dag(tmpJnt[9])

		self.Head_Jnt.attr('ssc').v = 0
		self.HeadTip_Jnt.attr('ssc').v = 0
		
		# Main group
		self.Ctrl_Grp = lrc.Null()
		self.Ctrl_Grp.name = "HeadCtrl%s_Grp" % part

		mc.parentConstraint(parent, self.Ctrl_Grp)

		# Head rig
		# Head rig - controllers
		self.Head_Ctrl = lrr.jointControl('cube')
		self.Head_Ctrl.name = 'Head%s_Ctrl' % part
		self.Head_Ctrl.color = 'yellow'
		self.Head_Ctrl.lockHideAttrs('v')

		self.HeadCtrlZr_Grp = lrc.group(self.Head_Ctrl)
		self.HeadCtrlZr_Grp.name = "HeadCtrlZr%s_Grp" % part

		self.HeadCtrlZr_Grp.snapPoint(self.Head_Jnt)
		self.Head_Ctrl.snapOrient(self.Head_Jnt)
		self.Head_Ctrl.freeze(t=False, r=True, s=False)
		self.HeadCtrlZr_Grp.parent(self.Ctrl_Grp)
		
		self.HeadGmbl_Ctrl = lrc.addGimbal(self.Head_Ctrl)
		self.HeadGmbl_Ctrl.name = "HeadGmbl%s_Ctrl" % part

		self.Head_Ctrl.rotateOrder = 'xzy'
		self.HeadGmbl_Ctrl.rotateOrder = 'xzy'

		# Head rig - local/world setup
		(self.HeadCtrlLoc_Grp,
		self.HeadCtrlWor_Grp,
		self.HeadCtrlWor_Grp_orientConstraint,
		self.HeadCtrlZr_Grp_orientConstraint,
		self.HeadCtrlZrGrpOri_Rev) = lrr.oriLocWor(
														self.Head_Ctrl,
														self.Ctrl_Grp,
														ctrlGrp,
														self.HeadCtrlZr_Grp
													)

		self.HeadCtrlLoc_Grp.name = "HeadCtrlLoc%s_Grp" % part
		self.HeadCtrlWor_Grp.name = "HeadCtrlWor%s_Grp" % part
		self.HeadCtrlZrGrpOri_Rev.name = "HeadCtrlZrGrpOri%s_Rev" % part
		
		# Head rig - Connect to joint
		mc.parentConstraint(self.HeadGmbl_Ctrl, self.Head_Jnt)
		mc.scaleConstraint(self.HeadGmbl_Ctrl, self.Head_Jnt)

		# Head rig - Set default
		self.Head_Ctrl.attr('localWorld').v = 1

		# Jaw rig
		if self.JawA_Jnt.exists and self.JawB_Jnt.exists and self.JawBTip_Jnt.exists:

			self.JawA_Jnt.attr('ssc').v = 0
			self.JawB_Jnt.attr('ssc').v = 0

			# Jaw rig - Controllers
			self.JawA_Ctrl = lrr.jointControl('square')
			self.JawA_Ctrl.name = "JawA%s_Ctrl" % part
			self.JawA_Ctrl.color = 'red'
			self.JawA_Ctrl.lockHideAttrs('v')
			self.JawA_Ctrl.rotateOrder = 'zyx'

			self.JawACtrlZr_Grp = lrc.group(self.JawA_Ctrl)
			self.JawACtrlZr_Grp.name = "JawACtrlZr%s_Grp" % part

			self.JawAGmbl_Ctrl = lrc.addGimbal(self.JawA_Ctrl)
			self.JawAGmbl_Ctrl.name = "JawAGmbl%s_Ctrl" % part

			self.JawACtrlZr_Grp.snap(self.JawA_Jnt)
			self.JawACtrlZr_Grp.parent(self.HeadGmbl_Ctrl)

			mc.parentConstraint(self.JawAGmbl_Ctrl, self.JawA_Jnt)
			mc.scaleConstraint(self.JawAGmbl_Ctrl, self.JawA_Jnt)

			self.JawB_Ctrl = lrr.jointControl('square')
			self.JawB_Ctrl.name = "JawB%s_Ctrl" % part
			self.JawB_Ctrl.color = 'red'
			self.JawB_Ctrl.lockHideAttrs('v')
			self.JawB_Ctrl.rotateOrder = 'zyx'
			
			self.JawBCtrlZr_Grp = lrc.group(self.JawB_Ctrl)
			
			self.JawBGmbl_Ctrl = lrc.addGimbal(self.JawB_Ctrl)
			self.JawBGmbl_Ctrl.name = "JawBGmbl%s_Ctrl" % part
			
			self.JawBCtrlZr_Grp.snap(self.JawB_Jnt)
			self.JawBCtrlZr_Grp.parent(self.JawAGmbl_Ctrl)
			
			mc.parentConstraint(self.JawBGmbl_Ctrl, self.JawB_Jnt)
			mc.scaleConstraint(self.JawBGmbl_Ctrl, self.JawB_Jnt)
		
		# Eye Rig
		if self.Eye_L_Jnt.exists or self.Eye_R_Jnt.exists:

			self.Eye_Ctrl = lrc.Control('capsule')
			self.Eye_Ctrl.name = "Eye%s_Ctrl" % part
			self.Eye_Ctrl.color = 'yellow'
			self.Eye_Ctrl.lockHideAttrs('sx', 'sy', 'sz', 'v')

			self.EyeCtrlOfst_Grp = lrc.group(self.Eye_Ctrl)
			self.EyeCtrlOfst_Grp.name = "EyeCtrlOfst%s_Grp" % part

			self.EyeCtrlZr_Grp = lrc.group(self.EyeCtrlOfst_Grp)
			self.EyeCtrlZr_Grp.name = "EyeCtrlZr%s_Grp" % part

			self.EyeCtrlZr_Grp.snap(self.EyeTar_Jnt)
			self.EyeCtrlZr_Grp.parent(self.HeadGmbl_Ctrl)
			
			(self.EyeCtrlLoc_Grp,
			self.EyeCtrlWor_Grp,
			self.EyeCtrlWor_Grp_parentConstraint,
			self.EyeCtrlZr_Grp_parentConstraint,
			self.EyeCtrlZroGrpPar_Rev) = lrr.parLocWor(
															self.Eye_Ctrl,
															self.HeadGmbl_Ctrl,
															ctrlGrp,
															self.EyeCtrlZr_Grp
														)

			self.EyeCtrlLoc_Grp.name = "EyeCtrlLoc%s_Grp" % part
			self.EyeCtrlWor_Grp.name = "EyeCtrlWor%s_Grp" % part
			self.EyeCtrlZroGrpPar_Rev.name = "EyeCtrlZroGrpPar%s_Rev" % part

		# Left Eye
		if self.Eye_L_Jnt.exists:

			self.eyeLft = EyeRig(
									self.Eye_L_Jnt,
									self.EyeTar_L_Jnt,
									self.Head_Jnt,
									part,
									'L'
								)

			self.eyeLft.EyeCtrlZr_Grp.parent(self.HeadGmbl_Ctrl)
			self.eyeLft.EyeCtrl_Crv.parent(self.Ctrl_Grp, r=True)
			self.eyeLft.TarCtrlZr_Grp.parent(self.Eye_Ctrl)

			self.Eye_L_Jnt = self.eyeLft.Eye_Jnt
			self.EyeGmbl_L_Ctrl = self.eyeLft.EyeGmbl_Ctrl

		# Right Eye
		if self.Eye_R_Jnt.exists:
			
			self.eyeRgt = EyeRig(
									self.Eye_R_Jnt,
									self.EyeTar_R_Jnt,
									self.Head_Jnt,
									part,
									'R'
								)

			self.eyeRgt.EyeCtrlZr_Grp.parent(self.HeadGmbl_Ctrl)
			self.eyeRgt.EyeCtrl_Crv.parent(self.Ctrl_Grp, r=True)
			self.eyeRgt.TarCtrlZr_Grp.parent(self.Eye_Ctrl)

			self.Eye_R_Jnt = self.eyeRgt.Eye_Jnt
			self.EyeGmbl_R_Ctrl = self.eyeRgt.EyeGmbl_Ctrl

		self.Ctrl_Grp.parent(ctrlGrp)
