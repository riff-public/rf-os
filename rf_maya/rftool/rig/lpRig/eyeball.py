
import maya.cmds as mc
import maya.mel as mm
from functools import partial

def ui():
	
	ui = 'eyeball'
	win = '%sWin' % ui
	
	if mc.window(win, exists=True):
		mc.deleteUI(win)
	
	mc.window(win, title="eyeball", width=200)
	mc.columnLayout(adj=True)
	mc.button(l='Initialize', h=50, c=partial(init))
	mc.separator()
	mc.checkBox('%sFlatCb' % ui, label='Flatten Pupil', v=True)
	mc.checkBox('%sMirCb' % ui, label='Mirror', v=True)
	mc.separator()
	mc.button(l='Finalize Selected', h=50, c=partial(finalize))
	
	mc.showWindow(win)

def finalize(*args):
	
	ctrl = mc.ls(sl=True)[0]
	
	(eyeballGeo, irisGeo, pupilGeo) = mc.listRelatives(ctrl, type='transform')[:3]
	
	irisScale = mc.getAttr('%s.irisScale' % ctrl)
	
	ui = 'eyeball'
	flat = mc.checkBox('%sFlatCb' % ui, q=True, v=True)
	mir = mc.checkBox('%sMirCb' % ui, q=True, v=True)

	# Shaders
	cornMat = 'CornTmp_Mat'
	eyeballMat = 'EyeballTmp_Mat'
	irisMat = 'IrisTmp_Mat'
	pupilMat = 'PupilTmp_Mat'

	for mat in (cornMat, eyeballMat, irisMat, pupilMat):
		if not mc.objExists(mat):
			mc.shadingNode('blinn', asShader=True, name=mat)

	mc.setAttr('%s.transparency' % cornMat, 0.9, 0.9, 0.9, type='double3')
	mc.setAttr('%s.color' % pupilMat, 0.1, 0.1, 0.1, type='double3')
	mc.setAttr('%s.specularColor' % pupilMat, 0.1, 0.1, 0.1, type='double3')
	mc.setAttr('%s.color' % irisMat, 0.1, 0.1, 0.25, type='double3')
	mc.setAttr('%s.color' % eyeballMat, 0.765, 0.725, 0.65, type='double3')
	
	# Create Cornea
	cornGeo, cornSphere = mc.sphere(ax=(0, 0, 1), s=16, nsp=16)
	
	mc.select(cornGeo)
	mc.hyperShade(assign=cornMat)
	
	# Reset cornea's transformation values
	mc.parent(cornGeo, ctrl)
	for attr in ('t', 'r', 's'):
		for ax in ('x', 'y', 'z'):
			currAttr = '{0}.{1}{2}'.format(cornGeo, attr, ax)
			val = 0
			if attr == 's':
				val = 1.01

			mc.setAttr(currAttr, val)

	# NURBs to Poly - Cornea
	cornFrontGeo, cornGeo, cornDetach = mc.detachSurface('{0}.u[{1}]'.format(cornGeo, irisScale), rpo=True)
	mc.parent(cornFrontGeo, ctrl)

	cornPoly = mc.nurbsToPoly(cornGeo, ch=False, f=3, pt=1, pc=300, chr=0.1, ft=0.01, mel=0.001, d=0.1, ut=1, un=3, vt=1, vn=3, uch=0, ucr=0, cht=0.2, es=0, ntr=0, mrt=0, uss=1)
	cornFrontPoly = mc.nurbsToPoly(cornFrontGeo, ch=False, f=3, pt=1, pc=300, chr=0.1, ft=0.01, mel=0.001, d=0.1, ut=1, un=3, vt=1, vn=3, uch=0, ucr=0, cht=0.2, es=0, ntr=0, mrt=0, uss=1)

	outerPoly = mc.polyUnite(cornPoly, cornFrontPoly, ch=False)[0]
	mc.polyMergeVertex(outerPoly, d=0.0001, am=1, ch=False)
	mc.polyNormal(outerPoly, normalMode=0, userNormalMode=0, ch=False)
	
	mc.delete(['{0}.e[{1}]'.format(outerPoly, ix) for ix in (641, 642, 644, 646, 648, 650, 652, 654)])
	mc.delete(['{0}.e[{1}]'.format(outerPoly, ix) for ix in (23, 19, 15, 11, 7, 3, 2, 27)])
	mc.select(outerPoly)
	mc.hyperShade(assign=cornMat)

	# NURBs to Poly - Eyeball
	eyeballPoly = mc.nurbsToPoly(eyeballGeo, ch=False, f=3, pt=1, pc=300, chr=0.1, ft=0.01, mel=0.001, d=0.1, ut=1, un=3, vt=1, vn=3, uch=0, ucr=0, cht=0.2, es=0, ntr=0, mrt=0, uss=1)[0]
	mc.polyMergeVertex(eyeballPoly, d=0.0001, am=1, ch=False)
	mc.polyNormal(eyeballPoly, normalMode=0, userNormalMode=0, ch=False)
	mc.delete(['{0}.e[{1}]'.format(eyeballPoly, ix) for ix in (23, 19, 15, 11, 7, 3, 2, 27)])

	mc.select(eyeballPoly)
	mc.hyperShade(assign=eyeballMat)

	# NURBs to Poly - Iris
	irisPoly = mc.nurbsToPoly(irisGeo, ch=False, f=3, pt=1, pc=300, chr=0.1, ft=0.01, mel=0.001, d=0.1, ut=1, un=3, vt=1, vn=3, uch=0, ucr=0, cht=0.2, es=0, ntr=0, mrt=0, uss=1)[0]
	mc.polyMergeVertex(irisPoly, d=0.0001, am=1, ch=False)
	mc.polyNormal(irisPoly, normalMode=0, userNormalMode=0, ch=False)
	
	mc.select(irisPoly)
	mc.hyperShade(assign=irisMat)

	# NURBs to Poly - Pupil
	pupilPoly = mc.nurbsToPoly(pupilGeo, ch=False, f=3, pt=1, pc=300, chr=0.1, ft=0.01, mel=0.001, d=0.1, ut=1, un=3, vt=1, vn=3, uch=0, ucr=0, cht=0.2, es=0, ntr=0, mrt=0, uss=1)[0]
	mc.polyMergeVertex(pupilPoly, d=0.0001, am=1, ch=False)
	mc.polyNormal(pupilPoly, normalMode=0, userNormalMode=0, ch=False)
	mc.delete(['{0}.e[{1}]'.format(pupilPoly, ix) for ix in (62, 49, 50, 52, 54, 56, 58, 60)])
	
	mc.select(pupilPoly)
	mc.hyperShade(assign=pupilMat)

	# Flatten Pupil
	if flat:
		pupilMidPost = midPost(['{0}.vtx[{1}]'.format(pupilPoly, ix) for ix in range(0, 16)])
		pupilMidLoc = mc.spaceLocator()[0]
		mc.xform(pupilMidLoc, ws=True, t=pupilMidPost)
		mc.parent(pupilMidLoc, ctrl)

		for attr in ('rx', 'ry', 'rz'):
			mc.setAttr('{0}.{1}'.format(pupilMidLoc, attr), 0)

		for ix in range(16, 33):
			currVtx = '{0}.vtx[{1}]'.format(pupilPoly, ix)
			currVtxPost = mc.xform(currVtx, q=True, ws=True, t=True)

			currLoc = mc.spaceLocator()[0]

			mc.xform(currLoc, ws=True, t=currVtxPost)
			mc.parent(currLoc, pupilMidLoc)

			mc.setAttr('%s.tz' % currLoc, 0)

			mc.xform(currVtx, ws=True, t=mc.xform(currLoc, q=True, ws=True, t=True))

	lftOuter = mc.rename(outerPoly, 'OuterEye_L_Geo')
	lftEyeball = mc.rename(eyeballPoly, 'Eyeball_L_Geo')
	lftIris = mc.rename(irisPoly, 'Iris_L_Geo')
	lftPupil = mc.rename(pupilPoly, 'Pupil_L_Geo')

	lftGeoGrp = mc.group(em=True, n='EyeGeo_L_Grp')
	mc.delete(mc.parentConstraint(ctrl, lftGeoGrp))
	mc.parent(lftOuter, lftEyeball, lftIris, lftPupil, lftGeoGrp)

	# Attrs
	for attr in ('scaleX', 'scaleY', 'scaleZ', 'irisScale', 'pupilScale', 'pupilDepth', 'irisCurvePush', 'irisCurveScale'):
		val = mc.getAttr('%s.%s' % (ctrl, attr))

		if attr.startswith('scale'):
			attr = attr.replace('scale', 'size')

		mc.addAttr(lftGeoGrp, ln=attr, k=True, dv=val)
		mc.setAttr('%s.%s' % (lftGeoGrp, attr), l=True)

	lftGeoGrpPost = mc.xform(lftGeoGrp, q=True, t=True, ws=True)
	lftGeoGrpOri = mc.xform(lftGeoGrp, q=True, ro=True, ws=True)
	for each in (lftOuter, lftEyeball, lftIris, lftPupil):
		mc.xform(each, rp=lftGeoGrpPost, sp=lftGeoGrpPost)
		mc.makeIdentity(each, apply=True, t=True, r=True, s=True, n=0, pn=True)

	# UVs
	# UVs - Iris
	irisUv = mc.polyProjection(
									'%s.f[0:31]' % lftIris,
									ch=True,
									type='Planar',
									ibd=True,
									kir=True,
									md='z'
								)[0]

	mc.setAttr('%s.pcx' % irisUv, lftGeoGrpPost[0])
	mc.setAttr('%s.pcy' % irisUv, lftGeoGrpPost[1])
	mc.setAttr('%s.pcz' % irisUv, lftGeoGrpPost[2])
	mc.setAttr('%s.rx' % irisUv, lftGeoGrpOri[0])
	mc.setAttr('%s.ry' % irisUv, lftGeoGrpOri[1])
	mc.setAttr('%s.rz' % irisUv, lftGeoGrpOri[2])
	mc.setAttr('%s.imageScaleV' % irisUv, 0.95)
	mc.setAttr('%s.imageScaleU' % irisUv, 0.95)

	mc.delete(lftIris, ch=True)

	# UVs - Eyeball
	oriGrp = mc.group(em=True)
	tarGrp = mc.group(em=True)
	tarPos = mc.xform('%s.vtx[32]' % lftPupil, q=True, t=True, ws=True)
	
	mc.xform(tarGrp, ws=True, t=tarPos)
	mc.delete(mc.pointConstraint(ctrl, oriGrp))
	mc.delete(
				mc.aimConstraint(
									tarGrp,
									oriGrp,
									aimVector=[0, 1, 0],
									upVector=[0, 0, -1],
									worldUpType='objectrotation',
									worldUpObject=lftGeoGrp,
									worldUpVector=(0, 1, 0)
								)
			)
	uvOri = mc.xform(oriGrp, q=True, ro=True, ws=True)
	mc.delete(oriGrp)
	mc.delete(tarGrp)
	
	eyeballUv = mc.polyProjection(
									'%s.f[0:215]' % lftEyeball,
									ch=True,
									type='Cylindrical',
									ibd=True,
									sf=True,
								)[0]
	mc.setAttr('%s.pcx' % eyeballUv, lftGeoGrpPost[0])	
	mc.setAttr('%s.rx' % eyeballUv, uvOri[0])
	mc.setAttr('%s.ry' % eyeballUv, uvOri[1])
	mc.setAttr('%s.rz' % eyeballUv, uvOri[2])
	mc.setAttr('%s.projectionHorizontalSweep' % eyeballUv, 360)
	mc.setAttr('%s.imageScaleV' % eyeballUv, 0.9)
	mc.setAttr('%s.imageScaleU' % eyeballUv, 0.9)

	mc.delete(lftEyeball, ch=True)

	if mir:
		
		pivGrp = mc.group(em=True)

		rgtOuter = mc.rename(mc.duplicate(lftOuter)[0], lftOuter.replace('_L_', '_R_'))
		rgtEyeball = mc.rename(mc.duplicate(lftEyeball)[0], lftEyeball.replace('_L_', '_R_'))
		rgtIris = mc.rename(mc.duplicate(lftIris)[0], lftIris.replace('_L_', '_R_'))
		rgtPupil = mc.rename(mc.duplicate(lftPupil)[0], lftPupil.replace('_L_', '_R_'))
		
		mc.parent(rgtOuter, rgtEyeball, rgtIris, rgtPupil, pivGrp)
		mc.setAttr('%s.sx' % pivGrp, -1)
		
		mc.makeIdentity(pivGrp, apply=True, t=False, r=False, s=True, n=2, pn=True)
		
		for each in (rgtOuter, rgtEyeball, rgtIris, rgtPupil):
			mc.parent(each, w=True)
		
		mc.delete(pivGrp)

		rgtGeoGrp = mc.group(em=True, n='EyeGeo_R_Grp')
		mc.delete(mc.parentConstraint(ctrl, rgtGeoGrp))
		mc.setAttr('%s.tx' % rgtGeoGrp, -mc.getAttr('%s.tx' % rgtGeoGrp))
		mirrorOrient(rgtGeoGrp, lftGeoGrp, (0, 0, 1), (0, 1, 0))
		mc.parent(rgtOuter, rgtEyeball, rgtIris, rgtPupil, rgtGeoGrp)

		# Attrs
		for attr in ('irisScale', 'pupilScale', 'pupilDepth', 'irisCurvePush', 'irisCurveScale'):
			val = mc.getAttr('%s.%s' % (ctrl, attr))
			mc.addAttr(rgtGeoGrp, ln=attr, k=True, dv=val)
			mc.setAttr('%s.%s' % (rgtGeoGrp, attr), l=True)

		rgtGeoGrpPost = mc.xform(rgtGeoGrp, q=True, t=True, ws=True)
		for each in (rgtOuter, rgtEyeball, rgtIris, rgtPupil):
			mc.xform(each, rp=rgtGeoGrpPost, sp=rgtGeoGrpPost)
			mc.makeIdentity(each, apply=True, t=True, r=True, s=True, n=0, pn=True)
	
	mc.delete(ctrl)

def init(*args):

	ctrl, ctrlCircle = mc.circle(nr=(0, 1, 0), r=2)

	mc.addAttr(ctrl, ln='irisScale', k=True, dv=13.5)
	mc.addAttr(ctrl, ln='pupilScale', k=True, dv=14.5)
	mc.addAttr(ctrl, ln='pupilDepth', k=True, dv=-2.5)
	mc.addAttr(ctrl, ln='irisCurvePush', k=True, dv=0)
	mc.addAttr(ctrl, ln='irisCurveScale', k=True, dv=0)

	sphGeo, sphNd = mc.sphere(ax=(0, 0, 1), s=16, nsp=16)
	outGeo, outSphNd = mc.sphere(r=1.02, ax=(0, 0, 1), s=16, nsp=16)

	irisTmpGeo, eyeballGeo, irisDetach = mc.detachSurface(
															'%s.u[14]' % sphGeo,
															rpo=True
														)
	pupilGeo, irisGeo, pupilDetach = mc.detachSurface(
															'%s.u[15]' % irisTmpGeo,
															rpo=True
														)

	mc.connectAttr('%s.irisScale' % ctrl, '%s.parameter[0]' % irisDetach)
	mc.connectAttr('%s.pupilScale' % ctrl, '%s.parameter[0]' % pupilDetach)

	mc.parent(sphGeo, ctrl)
	mc.parent(irisGeo, ctrl)
	mc.parent(pupilGeo, ctrl)
	mc.parent(outGeo, ctrl)

	mc.rebuildSurface(eyeballGeo, rt=0, end=1,su=12, du=3, sv=16, dv=3)
	mc.rebuildSurface(irisGeo, rt=0, end=1,su=2, du=1, sv=16, dv=3)
	mc.rebuildSurface(pupilGeo, rt=0, end=1,su=2, du=1, sv=16, dv=3)
	
	# Iris clusters
	# Iris clusters - Depth cluster
	mc.select('%s.cv[1:2][0:16]' % irisGeo, r=True)
	irisCluster, irisClusterHandle = mc.cluster(rel=True)

	mc.select('%s.cv[1][0:16]' % irisGeo, r=True)
	mc.percent(irisCluster, v=0.5)
	mc.parent(irisClusterHandle, ctrl)
	mc.setAttr('%s.v' % irisClusterHandle, 0)

	pupilDepthMult = mc.createNode('multiplyDivide')
	mc.setAttr('%s.i2x' % pupilDepthMult, 0.1)
	mc.connectAttr('%s.pupilDepth' % ctrl, '%s.i1x' % pupilDepthMult)

	mc.connectAttr('%s.ox' % pupilDepthMult, '%s.tz' % irisClusterHandle)
	mc.connectAttr('%s.ox' % pupilDepthMult, '%s.tz' % pupilGeo)

	# Iris clusters - Curve cluster push
	mc.select('%s.cv[1][0:16]' % irisGeo, r=True)
	irisCurveCluster, irisCurveClusterHandle = mc.cluster(rel=True)
	mc.parent(irisCurveClusterHandle, ctrl)
	mc.setAttr('%s.v' % irisCurveClusterHandle, 0)

	irisCurveMult = mc.createNode('multiplyDivide')

	mc.connectAttr('%s.irisCurvePush' % ctrl, '%s.i1x' % irisCurveMult)
	mc.setAttr('%s.i2x' % irisCurveMult, 0.1)
	
	mc.connectAttr('%s.ox' % irisCurveMult, '%s.tz' % irisCurveClusterHandle)

	# Iris clusters - Curve cluster scale
	mc.select('%s.cv[1][0:16]' % irisGeo, r=True)
	irisCurveScaleCluster, irisCurveScaleClusterHandle = mc.cluster(rel=True)
	mc.parent(irisCurveScaleClusterHandle, ctrl)
	mc.setAttr('%s.v' % irisCurveScaleClusterHandle, 0)

	irisCurvePma = mc.createNode('plusMinusAverage')
	mc.addAttr(irisCurvePma, ln='default', dv=1)
	mc.connectAttr('%s.default' % irisCurvePma, '%s.input1D[1]' % irisCurvePma)

	mc.connectAttr('%s.irisCurveScale' % ctrl, '%s.i1y' % irisCurveMult)
	mc.setAttr('%s.i2y' % irisCurveMult, 0.1)
	mc.connectAttr('%s.oy' % irisCurveMult, '%s.input1D[0]' % irisCurvePma)
	
	mc.connectAttr('%s.output1D' % irisCurvePma, '%s.sx' % irisCurveClusterHandle)
	mc.connectAttr('%s.output1D' % irisCurvePma, '%s.sy' % irisCurveClusterHandle)
	
	# Shaders
	cornMat = 'CornTmp_Mat'
	eyeballMat = 'EyeballTmp_Mat'
	irisMat = 'IrisTmp_Mat'
	pupilMat = 'PupilTmp_Mat'

	for mat in (cornMat, eyeballMat, irisMat, pupilMat):
		if not mc.objExists(mat):
			mc.shadingNode('blinn', asShader=True, name=mat)

	mc.setAttr('%s.transparency' % cornMat, 0.9, 0.9, 0.9, type='double3')
	mc.setAttr('%s.color' % pupilMat, 0.1, 0.1, 0.1, type='double3')
	mc.setAttr('%s.specularColor' % pupilMat, 0.1, 0.1, 0.1, type='double3')
	mc.setAttr('%s.color' % irisMat, 0.1, 0.1, 0.25, type='double3')
	mc.setAttr('%s.color' % eyeballMat, 0.765, 0.725, 0.65, type='double3')

	mc.select(outGeo)
	mc.hyperShade(assign=cornMat)
	mc.select(eyeballGeo)
	mc.hyperShade(assign=eyeballMat)
	mc.select(irisGeo)
	mc.hyperShade(assign=irisMat)
	mc.select(pupilGeo)
	mc.hyperShade(assign=pupilMat)
	
	for geo in (eyeballGeo, irisGeo, pupilGeo, irisClusterHandle, irisCurveClusterHandle):
		for attr in ('tx', 'ty', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz', 'v'):
			mc.setAttr('{0}.{1}'.format(geo, attr), l=True)

	mc.select(ctrl, r=True)

def midPost(objs=[]):

	no = len(objs)
	posSum = [0,0,0]
		
	for obj in objs:
		
		currPos = mc.xform(obj, q=True, t=True, ws=True)
		posSum[0] += currPos[0]
		posSum[1] += currPos[1]
		posSum[2] += currPos[2]
	
	return [posSum[0]/no, posSum[1]/no, posSum[2]/no]

def mirrorOrient(node, target, aimVec=(1,0,0), upVec=(0,1,0)):
	'''
	Match the orientation of given node to the target using aim constraint
	'''
	tmpTarget = mc.group(em=True)
	tmpUp = mc.group(em=True)
	
	mc.parent(tmpTarget, target)
	mc.parent(tmpUp, target)
	
	for each in (tmpTarget, tmpUp):
		for attr in ('tx', 'ty', 'tz', 'rx', 'ry', 'rz'):
			mc.setAttr('%s.%s' % (each, attr), 0)
		for attr in ('sx', 'sy', 'sz'):
			mc.setAttr('%s.%s' % (each, attr), 1)
	
	mc.setAttr('%s.t' % tmpTarget, aimVec[0], aimVec[1], aimVec[2])
	mc.setAttr('%s.t' % tmpUp, upVec[0], upVec[1], upVec[2])
	
	piv = mc.group(em=True)
	mc.parent(tmpTarget, piv)
	mc.parent(tmpUp, piv)
	
	mc.setAttr('%s.sx' % piv, -1)
	
	mc.delete(
				mc.aimConstraint(
									tmpTarget,
									node,
									aim=aimVec,
									upVector=upVec,
									wuo=tmpUp,
									wut='object',
								)
			)

	mc.delete(piv)