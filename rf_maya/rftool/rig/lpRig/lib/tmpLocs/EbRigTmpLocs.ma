//Maya ASCII 2017 scene
//Name: ebRigTmpLocs.ma
//Last modified: Thu, Jan 12, 2017 10:27:30 AM
//Codeset: 1252
requires maya "2017";
requires "stereoCamera" "10.0";
requires -nodeType "ilrOptionsNode" -nodeType "ilrUIOptionsNode" -nodeType "ilrBakeLayerManager"
		 -nodeType "ilrBakeLayer" "Turtle" "2017.0.0";
requires -nodeType "renderSetup" "renderSetup.py" "1.0";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2017";
fileInfo "version" "2017";
fileInfo "cutIdentifier" "201606150345-997974";
fileInfo "osv" "Microsoft Windows 7 Business Edition, 64-bit Windows 7 Service Pack 1 (Build 7601)\n";
createNode transform -s -n "persp";
	rename -uid "49C986BF-4C8A-4190-7C00-B1871664275E";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 13.860006389014316 129.47497529900446 45.231293375907121 ;
	setAttr ".r" -type "double3" 348.4731879180855 -14022.483646030458 0 ;
	setAttr ".rp" -type "double3" 8.8817841970012523e-016 -1.2434497875801753e-014 -2.8421709430404007e-014 ;
	setAttr ".rpt" -type "double3" 4.1697767962532592e-016 -1.2826642696004263e-014 
		2.2044624614758433e-014 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "EAC496B2-460A-DFCF-4815-A789D759A273";
	setAttr -k off ".v";
	setAttr ".fl" 59.999999999999979;
	setAttr ".ncp" 1;
	setAttr ".fcp" 100000;
	setAttr ".coi" 46.578019659408959;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 0.12379102071289516 120.16745371260194 1.7089043713202905 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
	setAttr ".dgm" no;
createNode transform -s -n "top";
	rename -uid "298F6EB3-441F-827D-7DB8-3580D85FECBC";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 24.340515355630785 144.49660396159672 -4.2597243682235071 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "FED589E2-4D6F-2C0A-1F51-FFBEC2E882D4";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 78.953530218014933;
	setAttr ".ow" 81.998831622685017;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".tp" -type "double3" 0 65.543073743581772 -4.3494377136230469 ;
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "DEE05FEE-4706-1187-54D8-09B29EAE27D3";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -1.840437521890407 119.42391256753716 174.45140026111298 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "464F046C-4C92-F7C3-0FF5-69B3415ED5A9";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 174.33130983967263;
	setAttr ".ow" 25.059973941322507;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".tp" -type "double3" 4.6876024156808853 116.50970411300659 0.12009042144034465 ;
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "A9285762-499B-4F4D-773A-13B84D5D2115";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 241.77495170658042 119.2265467482008 4.6414305447839048 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "30D68B55-49F0-60F2-6229-9CA1D3273E9F";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 241.77495170658037;
	setAttr ".ow" 26.038682713016652;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".tp" -type "double3" 0 68.549253821372986 -5.6701741218566895 ;
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "EbRbnTmpLoc_Grp";
	rename -uid "30E3E6E6-4A58-D6B7-EFA8-DC87388C365F";
	setAttr ".t" -type "double3" 0 -25.351961007945619 0.53169381082470046 ;
	setAttr ".rp" -type "double3" 0 25.351961007945619 -0.53169381082470046 ;
	setAttr ".sp" -type "double3" 0 25.351961007945619 -0.53169381082470046 ;
createNode transform -n "EbRbnTmp_1_L_Loc" -p "EbRbnTmpLoc_Grp";
	rename -uid "546F998A-4F7A-F867-13E4-AC851C824BCE";
	setAttr ".t" -type "double3" 2.0657664040357808 146.16400324427374 3.4908505731779842 ;
createNode locator -n "EbRbnTmp_1_L_LocShape" -p "EbRbnTmp_1_L_Loc";
	rename -uid "A6E66E81-4E34-FE38-B4DC-DABA83CB87FB";
	setAttr -k off ".v";
	setAttr ".lp" -type "double3" 0 0 -1.7763568394002505e-015 ;
createNode transform -n "EbRbnTmp_2_L_Loc" -p "EbRbnTmpLoc_Grp";
	rename -uid "68880D4C-4A90-A1DE-8EAE-AB949A9749CF";
	setAttr ".t" -type "double3" 4.6334448807030633 146.16900493803448 2.625132841589378 ;
	setAttr ".r" -type "double3" 0 15.465647521752912 0 ;
createNode locator -n "EbRbnTmp_2_L_LocShape" -p "EbRbnTmp_2_L_Loc";
	rename -uid "94AA46FB-4E04-2A16-7B29-B3BBA53467AE";
	setAttr -k off ".v";
	setAttr ".lp" -type "double3" -4.4408920985006262e-016 0 -1.7763568394002505e-015 ;
createNode transform -n "EbRbnTmp_3_L_Loc" -p "EbRbnTmpLoc_Grp";
	rename -uid "E5EE48D2-463E-2415-B488-BC8E6EDBD425";
	setAttr ".t" -type "double3" 6.8805270376436338 145.98468561090667 1.2905329238706951 ;
	setAttr ".r" -type "double3" -16.138337412452266 26.558871374763608 -22.441656927381114 ;
createNode locator -n "EbRbnTmp_3_L_LocShape" -p "EbRbnTmp_3_L_Loc";
	rename -uid "67F7450B-42B5-9CF4-608F-82BB1A28A177";
	setAttr -k off ".v";
	setAttr ".lp" -type "double3" 0 0 -1.7763568394002505e-015 ;
createNode transform -n "EbRbnTmp_4_L_Loc" -p "EbRbnTmpLoc_Grp";
	rename -uid "1B5617FE-4551-1AC6-FD43-3092E1CCCC23";
	setAttr ".t" -type "double3" 8.2742591059373058 144.85319253161788 -0.42215532941300682 ;
	setAttr ".r" -type "double3" -51.107950409040022 28.878494192802396 -65.852194007282108 ;
createNode locator -n "EbRbnTmp_4_L_LocShape" -p "EbRbnTmp_4_L_Loc";
	rename -uid "F58317BE-4E71-A42C-866A-BFA99EB42404";
	setAttr -k off ".v";
	setAttr ".lp" -type "double3" 0 -2.8421709430404007e-014 -1.7763568394002505e-015 ;
createNode transform -n "EbRbnTmp_1_R_Loc" -p "EbRbnTmpLoc_Grp";
	rename -uid "E3F8EE87-4BB6-0112-B689-4F9E2899845B";
	setAttr ".t" -type "double3" -2.065766 146.16400324427374 3.4908505731779842 ;
createNode locator -n "EbRbnTmp_1_R_LocShape" -p "EbRbnTmp_1_R_Loc";
	rename -uid "47EEADAB-4946-086A-CBF8-DEB7A3971A4A";
	setAttr -k off ".v";
	setAttr ".lp" -type "double3" 0 0 -1.7763568394002505e-015 ;
createNode transform -n "EbRbnTmp_2_R_Loc" -p "EbRbnTmpLoc_Grp";
	rename -uid "FE441873-4D02-3AA9-77CD-2AA2359BFD2E";
	setAttr ".t" -type "double3" -4.633445 146.16900493803448 2.625132841589378 ;
	setAttr ".r" -type "double3" -8.5260125064606385 -8.6372735216183898 -0.050283428589171668 ;
createNode locator -n "EbRbnTmp_2_R_LocShape" -p "EbRbnTmp_2_R_Loc";
	rename -uid "BAB6255F-42EA-5C29-3098-90BE007EF7BD";
	setAttr -k off ".v";
	setAttr ".lp" -type "double3" -4.4408920985006262e-016 0 -1.7763568394002505e-015 ;
createNode transform -n "EbRbnTmp_3_R_Loc" -p "EbRbnTmpLoc_Grp";
	rename -uid "F9FB2581-431F-2E58-B0AC-94B4D58F514F";
	setAttr ".t" -type "double3" -6.880527 145.98468561090667 1.2905329238706951 ;
	setAttr ".r" -type "double3" -15.595117939083993 -15.473586748341944 1.2927285890388025 ;
createNode locator -n "EbRbnTmp_3_R_LocShape" -p "EbRbnTmp_3_R_Loc";
	rename -uid "671B4E53-40BA-15F3-26D6-1988D907B894";
	setAttr -k off ".v";
	setAttr ".lp" -type "double3" 0 0 -1.7763568394002505e-015 ;
createNode transform -n "EbRbnTmp_4_R_Loc" -p "EbRbnTmpLoc_Grp";
	rename -uid "B3194EFD-4603-F176-A93D-3183C6A3C31B";
	setAttr ".t" -type "double3" -8.274259 144.85319253161788 -0.42215532941300682 ;
	setAttr ".r" -type "double3" -26.945822429800323 -22.532796952445349 7.9889057163531394 ;
createNode locator -n "EbRbnTmp_4_R_LocShape" -p "EbRbnTmp_4_R_Loc";
	rename -uid "2F6261FD-46A3-6611-DE2B-BAB381CC1CB1";
	setAttr -k off ".v";
	setAttr ".lp" -type "double3" 0 -2.8421709430404007e-014 -1.7763568394002505e-015 ;
createNode transform -n "EbRbnUpTmp_Crv" -p "EbRbnTmpLoc_Grp";
	rename -uid "3DC16A7C-4CAA-8027-C613-B4882C77A614";
	setAttr ".t" -type "double3" 0 25.351961007945619 -0.53169381082470046 ;
createNode nurbsCurve -n "EbRbnUpTmp_CrvShape" -p "EbRbnUpTmp_Crv";
	rename -uid "4007F816-4D8E-9FC6-0809-579672278938";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		1 18 0 no 3
		19 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18
		19
		-8.5511207580566406 119.97167205810547 -0.48117166757583618
		-8.0769891738891602 120.88627624511719 0.38418197631835938
		-7.3248200416564941 121.64150238037109 1.2890836000442505
		-6.3072967529296875 122.11228179931641 2.1551580429077148
		-5.0905532836914062 122.26865386962891 2.8565127849578857
		-3.7631373405456543 122.27426910400391 3.3370590209960938
		-2.4673280715942383 122.23667144775391 3.6831674575805664
		-1.4993948936462402 122.19096374511719 3.8300731182098389
		-0.72982132434844971 122.15846252441406 3.9035804271697998
		-1.9905598515276179e-017 122.14527893066406 3.9298808574676514
		0.72982132434844971 122.15846252441406 3.9035804271697998
		1.4993948936462402 122.19096374511719 3.8300731182098389
		2.4673280715942383 122.23667144775391 3.6831674575805664
		3.7631373405456543 122.27426910400391 3.3370590209960938
		5.0905532836914062 122.26865386962891 2.8565127849578857
		6.3072967529296875 122.11228179931641 2.1551580429077148
		7.3248200416564941 121.64150238037109 1.2890836000442505
		8.0769891738891602 120.88627624511719 0.38418197631835938
		8.5511207580566406 119.97167205810547 -0.48117166757583618
		;
	setAttr ".dcv" yes;
createNode transform -n "EbRbnLowTmp_Crv" -p "EbRbnTmpLoc_Grp";
	rename -uid "E4230A69-4494-61E5-C1FD-DDB94E4D7987";
	setAttr ".t" -type "double3" 0 25.351961007945619 -0.53169381082470046 ;
createNode nurbsCurve -n "EbRbnLowTmp_CrvShape" -p "EbRbnLowTmp_Crv";
	rename -uid "3F229C66-4D6B-2E97-676E-DF88893D8B89";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		1 18 0 no 3
		19 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18
		19
		8.1057167053222656 119.21368408203125 0.37906700372695923
		7.6220731735229492 119.78398895263672 1.1340224742889404
		6.8601250648498535 120.10370635986328 1.9223549365997314
		5.815941333770752 120.18731689453125 2.5941905975341797
		4.6219415664672852 120.12969207763672 3.066763162612915
		3.3624951839447021 119.96382904052734 3.4722352027893066
		1.7810323238372803 119.66514587402344 3.8614962100982666
		1.0797156095504761 119.54208374023437 3.9514408111572266
		0.49936261773109436 119.52732086181641 3.9920001029968262
		2.1986192675141008e-017 119.52861022949219 4.0066919326782227
		-0.49936261773109436 119.52732086181641 3.9920001029968262
		-1.0797156095504761 119.54208374023437 3.9514408111572266
		-1.7810323238372803 119.66514587402344 3.8614962100982666
		-3.3624951839447021 119.96382904052734 3.4722352027893066
		-4.6219415664672852 120.12969207763672 3.066763162612915
		-5.815941333770752 120.18731689453125 2.5941905975341797
		-6.8601250648498535 120.10370635986328 1.9223549365997314
		-7.6220731735229492 119.78398895263672 1.1340224742889404
		-8.1057167053222656 119.21368408203125 0.37906700372695923
		;
	setAttr ".dcv" yes;
createNode transform -n "EbRigTmpLoc_Grp";
	rename -uid "E5FAC65E-440F-7107-7C67-6B9390A14CCE";
	setAttr ".t" -type "double3" 0 -25.351961007945619 0.53169381082470046 ;
	setAttr ".rp" -type "double3" 0 25.351961007945619 -0.53169381082470046 ;
	setAttr ".sp" -type "double3" 0 25.351961007945619 -0.53169381082470046 ;
createNode transform -n "AllEbRigTmpPos_L_Loc" -p "EbRigTmpLoc_Grp";
	rename -uid "9E65CC4B-4CA3-A35D-39E5-12A1AFB39374";
	setAttr ".t" -type "double3" 3.5122940540313721 146.20397409497986 3.0062586122710027 ;
createNode locator -n "AllEbRigTmpPos_L_LocShape" -p "AllEbRigTmpPos_L_Loc";
	rename -uid "4D536D14-46B3-D326-F5F4-31907EA5A898";
	setAttr -k off ".v";
	setAttr ".lp" -type "double3" -4.4408920985006262e-016 0 -1.7763568394002505e-015 ;
createNode transform -n "InEbRigTmpPos_L_Loc" -p "EbRigTmpLoc_Grp";
	rename -uid "395AF6A7-43B4-C8FD-6411-8C812CAF021F";
	setAttr ".t" -type "double3" 2.0859112127789055 146.12788969075518 3.3959826284605787 ;
createNode locator -n "InEbRigTmpPos_L_LocShape" -p "InEbRigTmpPos_L_Loc";
	rename -uid "131F6E06-4169-5875-BC61-E2AD4FC0F35A";
	setAttr -k off ".v";
	setAttr ".lp" -type "double3" 0 0 -1.7763568394002505e-015 ;
createNode transform -n "MidEbRigTmpPos_L_Loc" -p "EbRigTmpLoc_Grp";
	rename -uid "DD702D9F-43E3-E145-5A1B-898BB5F72000";
	setAttr ".t" -type "double3" 5.9633423448032543 146.1453336955575 1.9617654138762273 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
createNode locator -n "MidEbRigTmpPos_L_LocShape" -p "MidEbRigTmpPos_L_Loc";
	rename -uid "6E5AF8DA-49BC-1E10-595E-6B8174B8A011";
	setAttr -k off ".v";
	setAttr ".lp" -type "double3" 0 0 -1.7763568394002505e-015 ;
createNode transform -n "OutEbRigTmpPos_L_Loc" -p "EbRigTmpLoc_Grp";
	rename -uid "6B948F40-4A46-4D77-BA98-F8AD0E538F70";
	setAttr ".t" -type "double3" 7.8123190200506762 145.62234610431722 0.290318792458228 ;
createNode locator -n "OutEbRigTmpPos_L_LocShape" -p "OutEbRigTmpPos_L_Loc";
	rename -uid "26B0FE38-4FDA-18CD-6E48-8C99CA4DFA07";
	setAttr -k off ".v";
	setAttr ".lp" -type "double3" 0 -2.8421709430404007e-014 -1.7763568394002505e-015 ;
createNode transform -n "AllEbRigTmpPos_R_Loc" -p "EbRigTmpLoc_Grp";
	rename -uid "68540EB6-4543-B029-141D-EFA1D308C044";
	setAttr ".t" -type "double3" -3.512294 146.20397409497986 3.0062586122710027 ;
createNode locator -n "AllEbRigTmpPos_R_LocShape" -p "AllEbRigTmpPos_R_Loc";
	rename -uid "F536C4B3-4993-4DAD-13B1-9F82C3B81EDE";
	setAttr -k off ".v";
	setAttr ".lp" -type "double3" -4.4408920985006262e-016 0 -1.7763568394002505e-015 ;
createNode transform -n "InEbRigTmpPos_R_Loc" -p "EbRigTmpLoc_Grp";
	rename -uid "D179E03C-4E49-98EB-948F-43AB5E456126";
	setAttr ".t" -type "double3" -2.085911 146.12788969075518 3.3959826284605787 ;
createNode locator -n "InEbRigTmpPos_R_LocShape" -p "InEbRigTmpPos_R_Loc";
	rename -uid "FEBB452B-4068-4FE0-C18F-078DE9980297";
	setAttr -k off ".v";
	setAttr ".lp" -type "double3" 0 0 -1.7763568394002505e-015 ;
createNode transform -n "MidEbRigTmpPos_R_Loc" -p "EbRigTmpLoc_Grp";
	rename -uid "FCE90171-4725-0122-E667-799098C9AEDE";
	setAttr ".t" -type "double3" -5.963342 146.1453336955575 1.9617654138762273 ;
	setAttr ".s" -type "double3" 1 1 0.99999999999999989 ;
createNode locator -n "MidEbRigTmpPos_R_LocShape" -p "MidEbRigTmpPos_R_Loc";
	rename -uid "B18BAC79-412C-9A86-4CFB-4D9ECA4BEDB0";
	setAttr -k off ".v";
	setAttr ".lp" -type "double3" 0 0 -1.7763568394002505e-015 ;
createNode transform -n "OutEbRigTmpPos_R_Loc" -p "EbRigTmpLoc_Grp";
	rename -uid "1A1BA8B7-4035-B00B-14A6-ACA34FFE0A78";
	setAttr ".t" -type "double3" -7.812319 145.62234610431722 0.290318792458228 ;
createNode locator -n "OutEbRigTmpPos_R_LocShape" -p "OutEbRigTmpPos_R_Loc";
	rename -uid "4738ED2A-44BC-F71C-1DAC-65BA4B4C522D";
	setAttr -k off ".v";
	setAttr ".lp" -type "double3" 0 -2.8421709430404007e-014 -1.7763568394002505e-015 ;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "E1809779-4312-08B6-9B28-F7B00DEDE6AF";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode displayLayerManager -n "layerManager";
	rename -uid "E7331ADA-48C3-7C43-4958-B4B291C1C3D4";
	setAttr ".cdl" 8;
	setAttr -s 8 ".dli[1:7]"  8 7 3 4 5 6 1;
createNode displayLayer -n "defaultLayer";
	rename -uid "C727DD4C-46DC-70CF-E3DB-EEB0233D4E25";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "591A97E1-4086-2B77-9156-BB96DDCBB041";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "EBE06E2E-41B9-59FA-6C4A-DFAD766C2862";
	setAttr ".g" yes;
createNode ilrOptionsNode -s -n "TurtleRenderOptions";
	rename -uid "CC8990E2-41BA-1A03-0E85-CFBAD4FA743C";
lockNode -l 1 ;
createNode ilrUIOptionsNode -s -n "TurtleUIOptions";
	rename -uid "9CCC2772-4E6F-6E70-4F30-459E766E3C48";
lockNode -l 1 ;
createNode ilrBakeLayerManager -s -n "TurtleBakeLayerManager";
	rename -uid "AAFC2330-42F5-7B61-2327-28B5A2BADD44";
lockNode -l 1 ;
createNode ilrBakeLayer -s -n "TurtleDefaultBakeLayer";
	rename -uid "89707F4B-4615-836D-0CED-85AFE7CE974D";
lockNode -l 1 ;
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "1538C9FC-4570-FF21-3255-EDB4B147A948";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "8F92DE1A-493B-FAAE-DB69-E9AE37286379";
createNode renderSetup -n "renderSetup";
	rename -uid "111C6DDC-4E8F-72B6-E7E1-59B25D32EC92";
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "0DEEF6AA-4E79-9B46-6CFF-15ACBB504DC9";
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" 1928.5713519368883 -1674.9489130353463 ;
	setAttr ".tgi[0].vh" -type "double2" 5810.7140548172538 1549.0781697376813 ;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "B0B5F573-446A-A714-D701-3B9EE4FC4270";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode objectSet -s -n "lightEditorRoot";
	rename -uid "7E6CE7C9-493E-B4EE-59A2-658984B4DE31";
	addAttr -ci true -sn "isolate" -ln "isolate" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "wasEnabled" -ln "wasEnabled" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "childIndex" -ln "childIndex" -dv -1 -at "long";
	addAttr -ci true -sn "lightGroup" -ln "lightGroup" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "visibility" -ln "visibility" -dv 1 -min 0 -max 1 -at "bool";
lockNode -l 1 ;
select -ne :time1;
	setAttr -av -k on ".cch";
	setAttr -av -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 0;
	setAttr -av -k on ".unw";
	setAttr -k on ".etw";
	setAttr -k on ".tps";
	setAttr -av -k on ".tms";
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr -k on ".mbsof";
	setAttr ".msaa" yes;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
select -ne :renderGlobalsList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
select -ne :defaultShaderList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
	setAttr -k on ".ihi";
select -ne :initialShadingGroup;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
select -ne :initialParticleSE;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
select -ne :defaultResolution;
	setAttr -av -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -av -k on ".w";
	setAttr -av -k on ".h";
	setAttr -av -k on ".pa" 1;
	setAttr -av -k on ".al";
	setAttr -av -k on ".dar";
	setAttr -av -k on ".ldar";
	setAttr -k on ".dpi";
	setAttr -av -k on ".off";
	setAttr -av -k on ".fld";
	setAttr -av -k on ".zsl";
	setAttr -k on ".isu";
	setAttr -k on ".pdu";
select -ne :hardwareRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k off -cb on ".ctrs" 256;
	setAttr -av -k off -cb on ".btrs" 512;
	setAttr -k off -cb on ".fbfm";
	setAttr -k off -cb on ".ehql";
	setAttr -k off -cb on ".eams";
	setAttr -k off -cb on ".eeaa";
	setAttr -k off -cb on ".engm";
	setAttr -k off -cb on ".mes";
	setAttr -k off -cb on ".emb";
	setAttr -av -k off -cb on ".mbbf";
	setAttr -k off -cb on ".mbs";
	setAttr -k off -cb on ".trm";
	setAttr -k off -cb on ".tshc";
	setAttr -k off -cb on ".enpt";
	setAttr -k off -cb on ".clmt";
	setAttr -k off -cb on ".tcov";
	setAttr -k off -cb on ".lith";
	setAttr -k off -cb on ".sobc";
	setAttr -k off -cb on ".cuth";
	setAttr -k off -cb on ".hgcd";
	setAttr -k off -cb on ".hgci";
	setAttr -k off -cb on ".mgcs";
	setAttr -k off -cb on ".twa";
	setAttr -k off -cb on ".twz";
	setAttr -cb on ".hwcc";
	setAttr -cb on ".hwdp";
	setAttr -cb on ".hwql";
	setAttr -k on ".hwfr";
	setAttr -k on ".soll";
	setAttr -k on ".sosl";
	setAttr -k on ".bswa";
	setAttr -k on ".shml";
	setAttr -k on ".hwel";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr ":TurtleDefaultBakeLayer.idx" ":TurtleBakeLayerManager.bli[0]";
connectAttr ":TurtleRenderOptions.msg" ":TurtleDefaultBakeLayer.rset";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr ":perspShape.msg" ":defaultRenderGlobals.sc";
// End of ebRigTmpLocs.ma
