//Maya ASCII 2017ff05 scene
//Name: MthRigTmpLocs.ma
//Last modified: Mon, Oct 08, 2018 10:48:36 AM
//Codeset: 1252
requires maya "2017ff05";
requires "stereoCamera" "10.0";
requires -nodeType "ilrOptionsNode" -nodeType "ilrUIOptionsNode" -nodeType "ilrBakeLayerManager"
		 -nodeType "ilrBakeLayer" "Turtle" "2017.0.0";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2017";
fileInfo "version" "2017";
fileInfo "cutIdentifier" "201710312130-1018716";
fileInfo "osv" "Microsoft Windows 8 Business Edition, 64-bit  (Build 9200)\n";
createNode transform -s -n "persp";
	rename -uid "49C986BF-4C8A-4190-7C00-B1871664275E";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -6.5591818689612618 109.17909143692107 86.583690310350079 ;
	setAttr ".r" -type "double3" 359.87318792144441 -14044.883646023894 2.3379955978768449e-018 ;
	setAttr ".rp" -type "double3" 8.8817841970012523e-016 -1.2434497875801753e-014 -2.8421709430404007e-014 ;
	setAttr ".rpt" -type "double3" 4.1697767962532592e-016 -1.2826642696004263e-014 
		2.2044624614758433e-014 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "EAC496B2-460A-DFCF-4815-A789D759A273";
	setAttr -k off ".v";
	setAttr ".fl" 59.999999999999979;
	setAttr ".fcp" 100000;
	setAttr ".coi" 82.920586392208094;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 0 110.52649491433127 -0.37922209459881007 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
	setAttr ".dgm" no;
createNode transform -s -n "top";
	rename -uid "298F6EB3-441F-827D-7DB8-3580D85FECBC";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0.39849162351696166 152.51375008139911 2.3321838948997113 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "FED589E2-4D6F-2C0A-1F51-FFBEC2E882D4";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 43.346340457141736;
	setAttr ".ow" 40.20477127608099;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".tp" -type "double3" -2.3054170965098586 109.16740962425737 3.9590575113670599 ;
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "DEE05FEE-4706-1187-54D8-09B29EAE27D3";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1.9956895886981314 108.8585664831151 174.61190284817198 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "464F046C-4C92-F7C3-0FF5-69B3415ED5A9";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 169.99359508800936;
	setAttr ".ow" 8.4687157051694317;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".tp" -type "double3" -4.7073364317640198e-008 108.45880515524655 4.6183077601626099 ;
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "A9285762-499B-4F4D-773A-13B84D5D2115";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 241.78586698310889 110.07653824135811 -2.7607084388661827 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "30D68B55-49F0-60F2-6229-9CA1D3273E9F";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 241.78586698310883;
	setAttr ".ow" 24.320447244770328;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".tp" -type "double3" 0 108.90159092886987 1.1102703598211168 ;
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "Export_Grp";
	rename -uid "B524402B-4E22-BCA3-DF94-AF9D37F91FB3";
createNode transform -n "MthRigTmpLoc_Grp" -p "Export_Grp";
	rename -uid "18B390E6-456F-3A2B-2893-8DAE39886966";
createNode transform -n "LipUpMidMthRigTmp_Loc" -p "MthRigTmpLoc_Grp";
	rename -uid "00C0DF65-4B85-28B5-D31A-4C99509F7EC2";
	setAttr ".t" -type "double3" 0 109.14443206787109 5.2239036560058585 ;
createNode locator -n "LipUpMidMthRigTmp_LocShape" -p "LipUpMidMthRigTmp_Loc";
	rename -uid "FFDC6BF9-4357-F0E5-CF32-0AA9B6B191F1";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.2 0.2 0.2 ;
createNode transform -n "LipUpCnrMthRigTmp_L_Loc" -p "MthRigTmpLoc_Grp";
	rename -uid "11B0A80A-4FCB-055A-0731-E5ADB10DB050";
	setAttr ".t" -type "double3" 2.8588489137973236 109.17370680548007 3.9414627143687548 ;
	setAttr ".r" -type "double3" 0 40 0 ;
createNode locator -n "LipUpCnrMthRigTmp_L_LocShape" -p "LipUpCnrMthRigTmp_L_Loc";
	rename -uid "A03BBA31-49D8-292E-6825-E2BD196C8DE3";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.2 0.2 0.2 ;
createNode transform -n "LipUpAMthRigTmp_L_Loc" -p "MthRigTmpLoc_Grp";
	rename -uid "9016941F-45D0-A219-492C-ECBEEC1F313D";
	setAttr ".t" -type "double3" 1.3894819021224976 109.14225006103516 4.9180555343627921 ;
	setAttr ".r" -type "double3" 0 25 0 ;
createNode locator -n "LipUpAMthRigTmp_L_LocShape" -p "LipUpAMthRigTmp_L_Loc";
	rename -uid "8A6725DA-4EFF-68B3-44FA-2EA2052848A4";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.2 0.2 0.2 ;
createNode transform -n "LipUpBMthRigTmp_L_Loc" -p "MthRigTmpLoc_Grp";
	rename -uid "9BDCB7B6-4835-5FE0-4F43-46A23C9277D6";
	setAttr ".t" -type "double3" 2.4662172794342041 109.15451049804687 4.3871808052062979 ;
	setAttr ".r" -type "double3" 0 38.000000000000036 0 ;
createNode locator -n "LipUpBMthRigTmp_L_LocShape" -p "LipUpBMthRigTmp_L_Loc";
	rename -uid "5F146C71-49E1-B51C-A84B-31A5394A2F6A";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.2 0.2 0.2 ;
createNode transform -n "LipLowMidMthRigTmp_Loc" -p "MthRigTmpLoc_Grp";
	rename -uid "4CCBEAC9-47BF-1297-CC77-928F0470D36E";
	setAttr ".t" -type "double3" 9.3132257461547852e-010 109.04099273681641 5.1282906532287589 ;
createNode locator -n "LipLowMidMthRigTmp_LocShape" -p "LipLowMidMthRigTmp_Loc";
	rename -uid "C9568187-438E-B6FB-46E5-C4A98333E644";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.2 0.2 0.2 ;
createNode transform -n "LipLowCnrMthRigTmp_L_Loc" -p "MthRigTmpLoc_Grp";
	rename -uid "8C95BAD1-49E7-8DEA-CB2B-0DBBB1AB92FD";
	setAttr ".t" -type "double3" 2.8769197416597958 109.1672864637205 3.8659226926212922 ;
	setAttr ".r" -type "double3" 0 40 0 ;
createNode locator -n "LipLowCnrMthRigTmp_L_LocShape" -p "LipLowCnrMthRigTmp_L_Loc";
	rename -uid "B38AEA13-429E-E59A-2091-DFA41A1A3C01";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.2 0.2 0.2 ;
createNode transform -n "LipLowAMthRigTmp_L_Loc" -p "MthRigTmpLoc_Grp";
	rename -uid "53600932-4ABD-FFBF-62C2-48B38621C86E";
	setAttr ".t" -type "double3" 1.4123419523239136 109.06081390380859 4.809952735900878 ;
	setAttr ".r" -type "double3" 0 25 0 ;
createNode locator -n "LipLowAMthRigTmp_L_LocShape" -p "LipLowAMthRigTmp_L_Loc";
	rename -uid "B03530D8-4B6C-8D50-8FE9-24BF0696EFC1";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.2 0.2 0.2 ;
createNode transform -n "LipLowBMthRigTmp_L_Loc" -p "MthRigTmpLoc_Grp";
	rename -uid "646DFB24-4046-C1D1-8313-6CA5082AB925";
	setAttr ".t" -type "double3" 2.4556481838226318 109.14179229736328 4.2521190643310538 ;
	setAttr ".r" -type "double3" 0 38.000000000000036 0 ;
createNode locator -n "LipLowBMthRigTmp_L_LocShape" -p "LipLowBMthRigTmp_L_Loc";
	rename -uid "62FF863C-4456-1B36-6C39-5BBF428AAC1B";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.2 0.2 0.2 ;
createNode transform -n "JawMthRigTmp_Loc" -p "MthRigTmpLoc_Grp";
	rename -uid "DDE9F2D7-434B-875F-766D-0B9DF42560CA";
	setAttr ".t" -type "double3" 0 112.30528228577782 -4.3924487228222153 ;
createNode locator -n "JawMthRigTmp_LocShape" -p "JawMthRigTmp_Loc";
	rename -uid "065A02D3-48BE-DD31-8971-65BFB47046CD";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.2 0.2 0.2 ;
createNode transform -n "HdMthRigTmp_Loc" -p "MthRigTmpLoc_Grp";
	rename -uid "7E44C621-4E18-D77D-0E77-42AF6A7A6323";
	setAttr ".t" -type "double3" 0 112.59135541603911 -6.2476811162300896 ;
createNode locator -n "HdMthRigTmp_LocShape" -p "HdMthRigTmp_Loc";
	rename -uid "B647D46E-4715-434E-A288-3AB9D6AC8C26";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.2 0.2 0.2 ;
createNode transform -n "CntMthRigTmp_Loc" -p "MthRigTmpLoc_Grp";
	rename -uid "307D460E-41B4-2B1E-FFB7-8B8DE3A6FE29";
	setAttr ".t" -type "double3" -9.3132257461547852e-010 109.28580474853516 5.3892369270324698 ;
createNode locator -n "CntMthRigTmp_LocShape" -p "CntMthRigTmp_Loc";
	rename -uid "BDDF32BA-436D-2546-A69E-D69AD1DBA06E";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.2 0.2 0.2 ;
createNode transform -n "CnrMthRigTmp_L_Loc" -p "MthRigTmpLoc_Grp";
	rename -uid "3B5029A3-4DEA-6A47-AF37-79867FFF223B";
	setAttr ".t" -type "double3" 3.0048403739929199 109.18018341064453 4.0776195526123038 ;
createNode locator -n "CnrMthRigTmp_L_LocShape" -p "CnrMthRigTmp_L_Loc";
	rename -uid "D476E43F-4E74-7002-F33B-9292E3DE0559";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.2 0.2 0.2 ;
createNode transform -n "LipUpCnrMthRigTmp_R_Loc" -p "MthRigTmpLoc_Grp";
	rename -uid "ED5C7AED-47BE-8E51-9983-24BED3160EE6";
	setAttr ".t" -type "double3" -2.858849 109.17370680548007 3.9414627143687548 ;
	setAttr ".r" -type "double3" 0 -40 0 ;
createNode locator -n "LipUpCnrMthRigTmp_R_LocShape" -p "LipUpCnrMthRigTmp_R_Loc";
	rename -uid "11327247-4876-C2A8-6D25-F39C1607630B";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.2 0.2 0.2 ;
createNode transform -n "LipUpAMthRigTmp_R_Loc" -p "MthRigTmpLoc_Grp";
	rename -uid "FBA5E6BE-4C58-5825-671D-CDB0FA356B18";
	setAttr ".t" -type "double3" -1.389482 109.14225006103516 4.9180555343627921 ;
	setAttr ".r" -type "double3" 0 -25 0 ;
createNode locator -n "LipUpAMthRigTmp_R_LocShape" -p "LipUpAMthRigTmp_R_Loc";
	rename -uid "55DDAA3A-4E7F-CD84-CED3-62A3DCE8D489";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.2 0.2 0.2 ;
createNode transform -n "LipUpBMthRigTmp_R_Loc" -p "MthRigTmpLoc_Grp";
	rename -uid "C91B4627-4CC9-4986-4230-559D35D3FF03";
	setAttr ".t" -type "double3" -2.466217 109.15451049804687 4.3871808052062979 ;
	setAttr ".r" -type "double3" 0 -38.000000000000036 0 ;
createNode locator -n "LipUpBMthRigTmp_R_LocShape" -p "LipUpBMthRigTmp_R_Loc";
	rename -uid "F34ACC1F-4917-64FC-FEB0-6B8DBAF4FAD1";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.2 0.2 0.2 ;
createNode transform -n "CnrMthRigTmp_R_Loc" -p "MthRigTmpLoc_Grp";
	rename -uid "A8C96ED3-479F-7064-C88F-A0B018A0B65C";
	setAttr ".t" -type "double3" -3.00484 109.18018341064453 4.0776195526123038 ;
createNode locator -n "CnrMthRigTmp_R_LocShape" -p "CnrMthRigTmp_R_Loc";
	rename -uid "3024441D-49DF-E453-F7D1-D49FEEF990C0";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.2 0.2 0.2 ;
createNode transform -n "LipLowBMthRigTmp_R_Loc" -p "MthRigTmpLoc_Grp";
	rename -uid "9F151EEE-41FB-B1EA-C1CA-4BA2AA09CABE";
	setAttr ".t" -type "double3" -2.455648 109.14179229736328 4.2521190643310538 ;
	setAttr ".r" -type "double3" 0 -38.000000000000036 0 ;
createNode locator -n "LipLowBMthRigTmp_R_LocShape" -p "LipLowBMthRigTmp_R_Loc";
	rename -uid "CA934769-47CC-34E3-621C-9E8DA89F2D3F";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.2 0.2 0.2 ;
createNode transform -n "LipLowAMthRigTmp_R_Loc" -p "MthRigTmpLoc_Grp";
	rename -uid "DFCB599B-4869-3BB2-2F00-02B5B0670F24";
	setAttr ".t" -type "double3" -1.412342 109.06081390380859 4.809952735900878 ;
	setAttr ".r" -type "double3" 0 -25 0 ;
createNode locator -n "LipLowAMthRigTmp_R_LocShape" -p "LipLowAMthRigTmp_R_Loc";
	rename -uid "91EF6D4A-4FB5-635E-7FAA-98B5CDE1F61F";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.2 0.2 0.2 ;
createNode transform -n "LipLowCnrMthRigTmp_R_Loc" -p "MthRigTmpLoc_Grp";
	rename -uid "72B4AEA0-45A0-FC54-C5EF-1E88E5D617A8";
	setAttr ".t" -type "double3" -2.87692 109.1672864637205 3.8659226926212922 ;
	setAttr ".r" -type "double3" 0 -40 0 ;
createNode locator -n "LipLowCnrMthRigTmp_R_LocShape" -p "LipLowCnrMthRigTmp_R_Loc";
	rename -uid "4311CE0A-42BD-84D7-CFED-B8A594F47494";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.2 0.2 0.2 ;
createNode transform -n "LipRigTmpLoc_Grp" -p "Export_Grp";
	rename -uid "8F46A744-4DAD-AB66-DC6E-5999B24D5351";
createNode transform -n "CntLipRigTmp_Loc" -p "LipRigTmpLoc_Grp";
	rename -uid "4EE7AC52-4617-C1F3-4D07-7197AE06E409";
	setAttr ".t" -type "double3" 1.3363055646499247e-017 109.11369298420183 1.1102703598211168 ;
createNode locator -n "CntLipRigTmp_LocShape" -p "CntLipRigTmp_Loc";
	rename -uid "F0A314B1-4E9B-95EF-F686-B682869A65E7";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.3 0.3 0.3 ;
createNode transform -n "UpMidLipRigTmp_Loc" -p "LipRigTmpLoc_Grp";
	rename -uid "04C274F6-4C13-5F26-DE20-27BA974C427E";
	setAttr ".t" -type "double3" -9.3132257461547852e-010 109.28580474853516 5.3892369270324698 ;
createNode locator -n "UpMidLipRigTmp_LocShape" -p "UpMidLipRigTmp_Loc";
	rename -uid "8D1DF185-407B-0F8E-1197-4FB601933A13";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.3 0.3 0.3 ;
createNode transform -n "LowMidLipRigTmp_Loc" -p "LipRigTmpLoc_Grp";
	rename -uid "615C0562-43B2-2F98-0023-5B8B9E579F54";
	setAttr ".t" -type "double3" 3.7252902984619141e-009 108.72114562988281 5.1793813705444327 ;
createNode locator -n "LowMidLipRigTmp_LocShape" -p "LowMidLipRigTmp_Loc";
	rename -uid "C84312FC-4440-133B-60EF-6ABEF99E3790";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.3 0.3 0.3 ;
createNode transform -n "CnrLipRigTmp_L_Loc" -p "LipRigTmpLoc_Grp";
	rename -uid "0A9CC324-4DCF-9433-C81F-23A10B72B341";
	setAttr ".t" -type "double3" 2.9360613822937012 109.17047119140625 4.0508427619934073 ;
createNode locator -n "CnrLipRigTmp_L_LocShape" -p "CnrLipRigTmp_L_Loc";
	rename -uid "20331397-441E-78E2-8876-DAA8529D5A68";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.3 0.3 0.3 ;
createNode transform -n "UpALipRigTmp_L_Loc" -p "LipRigTmpLoc_Grp";
	rename -uid "6F9E59E1-4544-2253-9275-029E38FE366D";
	setAttr ".t" -type "double3" 1.4090820550918579 109.27297973632812 5.0716524124145499 ;
	setAttr ".r" -type "double3" 0 25 0 ;
createNode locator -n "UpALipRigTmp_L_LocShape" -p "UpALipRigTmp_L_Loc";
	rename -uid "6029BE8A-4153-7055-EF4E-7A887D451980";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.3 0.3 0.3 ;
createNode transform -n "UpBLipRigTmp_L_Loc" -p "LipRigTmpLoc_Grp";
	rename -uid "279E4A6A-41F9-1A36-26AE-E293C2B7A354";
	setAttr ".t" -type "double3" 2.5048284530639648 109.22332763671875 4.499189853668212 ;
	setAttr ".r" -type "double3" 0 38.000000000000036 0 ;
createNode locator -n "UpBLipRigTmp_L_LocShape" -p "UpBLipRigTmp_L_Loc";
	rename -uid "5E4C6FD5-4979-0F35-8210-CD930E3C4577";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.3 0.3 0.3 ;
createNode transform -n "LowALipRigTmp_L_Loc" -p "LipRigTmpLoc_Grp";
	rename -uid "84ECBE11-40E9-A31A-A760-489F85DDFCD2";
	setAttr ".t" -type "double3" 1.4639215469360352 108.82005310058594 4.866644859313964 ;
	setAttr ".r" -type "double3" 0 25 0 ;
createNode locator -n "LowALipRigTmp_L_LocShape" -p "LowALipRigTmp_L_Loc";
	rename -uid "BDF82B9C-43D7-7AD3-7287-63BEE7D395E7";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.3 0.3 0.3 ;
createNode transform -n "LowBLipRigTmp_L_Loc" -p "LipRigTmpLoc_Grp";
	rename -uid "DD799486-418D-E58D-EC9E-95AF29D056D7";
	setAttr ".t" -type "double3" 2.5121803283691406 109.04299926757812 4.3193507194519034 ;
	setAttr ".r" -type "double3" 0 38.000000000000036 0 ;
createNode locator -n "LowBLipRigTmp_L_LocShape" -p "LowBLipRigTmp_L_Loc";
	rename -uid "A2BBD3F1-4B85-AE9E-3792-DE87797508B1";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.3 0.3 0.3 ;
createNode transform -n "LowBLipRigTmp_R_Loc" -p "LipRigTmpLoc_Grp";
	rename -uid "2887EADD-4769-8675-D791-C3854A6888EC";
	setAttr ".t" -type "double3" -2.51218 109.04299926757812 4.3193507194519034 ;
	setAttr ".r" -type "double3" 0 -38.000000000000036 0 ;
createNode locator -n "LowBLipRigTmp_R_LocShape" -p "LowBLipRigTmp_R_Loc";
	rename -uid "099CB24E-4528-810D-C20D-C080D0B857BB";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.3 0.3 0.3 ;
createNode transform -n "LowALipRigTmp_R_Loc" -p "LipRigTmpLoc_Grp";
	rename -uid "2157FB2B-4074-73BA-B41D-16B11678BAE1";
	setAttr ".t" -type "double3" -1.463922 108.82005310058594 4.866644859313964 ;
	setAttr ".r" -type "double3" 0 -25 0 ;
createNode locator -n "LowALipRigTmp_R_LocShape" -p "LowALipRigTmp_R_Loc";
	rename -uid "FC1E89A2-4DC5-0516-FE78-CC8DB39B63AE";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.3 0.3 0.3 ;
createNode transform -n "UpBLipRigTmp_R_Loc" -p "LipRigTmpLoc_Grp";
	rename -uid "3A3D6382-4967-F31D-A7FD-6EAD3EFE5AFB";
	setAttr ".t" -type "double3" -2.504828 109.22332763671875 4.499189853668212 ;
	setAttr ".r" -type "double3" 0 -38.000000000000036 0 ;
createNode locator -n "UpBLipRigTmp_R_LocShape" -p "UpBLipRigTmp_R_Loc";
	rename -uid "34431FC0-41C0-CEFC-77B7-B19690C888EF";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.3 0.3 0.3 ;
createNode transform -n "UpALipRigTmp_R_Loc" -p "LipRigTmpLoc_Grp";
	rename -uid "4FF1E2F8-4960-3BC3-7BE0-92B7F2BADF27";
	setAttr ".t" -type "double3" -1.409082 109.27297973632812 5.0716524124145499 ;
	setAttr ".r" -type "double3" 0 -25 0 ;
createNode locator -n "UpALipRigTmp_R_LocShape" -p "UpALipRigTmp_R_Loc";
	rename -uid "ECAC05BF-47E9-270F-AF5A-599BABEF026C";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.3 0.3 0.3 ;
createNode transform -n "CnrLipRigTmp_R_Loc" -p "LipRigTmpLoc_Grp";
	rename -uid "92066684-4B6A-5C00-5D1B-47B90A6875CD";
	setAttr ".t" -type "double3" -2.936061 109.17047119140625 4.0508427619934073 ;
createNode locator -n "CnrLipRigTmp_R_LocShape" -p "CnrLipRigTmp_R_Loc";
	rename -uid "E5ABE235-4711-DF76-B9C2-00A11548BFE0";
	setAttr -k off ".v";
	setAttr ".los" -type "double3" 0.3 0.3 0.3 ;
createNode transform -n "RbnLipRigTmp_Nrb" -p "LipRigTmpLoc_Grp";
	rename -uid "0B049245-4348-CF7A-AF8B-BE99D8AD020B";
createNode nurbsSurface -n "RbnLipRigTmp_NrbShape" -p "RbnLipRigTmp_Nrb";
	rename -uid "2F5BBDE3-4507-77C1-B7BB-23B58F02C6E4";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		3 3 2 0 no 
		17 -2 -1 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14
		6 0 0 0 1 1 1
		
		60
		3.1747086409591865 109.16290283203125 3.760765408020013
		3.2458617150489348 109.18195074138714 3.7037689168865362
		3.3170147891386832 109.20099865074303 3.6467724257530585
		3.388167863228432 109.22004656009892 3.5897759346195812
		2.2972116599858832 109.15121027705807 4.5560073009639463
		2.2771324862185942 109.2279896295115 4.6162941526896466
		2.2570533124513052 109.30476898196491 4.6765810044153477
		2.2369741386840167 109.38154833441831 4.736867856141048
		1.4311761856079102 109.05900967234849 4.9025911615072495
		1.4375858635159122 109.22525210176586 4.9606291785003043
		1.4439955414239138 109.39149453118326 5.0186671954933573
		1.4504052193319155 109.55773696060062 5.0767052124864112
		1.1102230246251565e-016 109.07209833803772 5.1958634046693142
		7.4014868308343765e-017 109.20428113486246 5.2916169256277499
		3.7007434154171883e-017 109.33646393168719 5.3873704465861856
		0 109.46864672851193 5.4831239675446213
		-1.431176 109.05900967234849 4.8775703169752447
		-1.4205735742379286 109.22525210176586 4.9598191375027376
		-1.4099711484758572 109.39149453118326 5.0420679580302306
		-1.3993687227137859 109.55773696060062 5.1243167785577235
		-2.274009708716199 109.16740962425736 4.5557775510402472
		-2.2616644619581319 109.23878919431102 4.5992857388362012
		-2.2493192152000647 109.31016876436468 4.6427939266321552
		-2.2369739684419976 109.38154833441831 4.6863021144281092
		-3.0868279545549462 109.18336935414372 3.8332693048394995
		-3.1872745907794418 109.19559508946212 3.7521048480995263
		-3.2877212270039369 109.20782082478051 3.670940391359554
		-3.388167863228432 109.22004656009892 3.5897759346195812
		-2.2406859482500501 109.18326199168443 4.4407274521810454
		-2.2388670991262085 109.04762434456347 4.4321322938462133
		-2.237048250002367 108.91198669744252 4.4235371355113804
		-2.2352294008785254 108.77634905032157 4.4149419771765483
		-1.4105399999999999 109.10617458934068 4.7610182334466398
		-1.4128618158310564 108.88822015300828 4.7500674591140468
		-1.4151836316621129 108.67026571667589 4.739116684781453
		-1.4175054474931696 108.45231128034351 4.72816591044886
		-6.2389455809656771e-016 109.09420706370209 5.1326529173035915
		-4.1592970539771181e-016 108.81668284667587 5.0983359679295921
		-2.0796485269885593e-016 108.53915862964963 5.0640190185555918
		0 108.26163441262342 5.0297020691815923
		1.4105401039123535 109.10617458934068 4.7860390779786446
		1.4038631568043953 108.88822015300828 4.7815031159175385
		1.3971862096964369 108.67026571667589 4.7769671538564316
		1.390509262588479 108.45231128034351 4.7724311917953255
		2.2720932379962502 109.18326199168443 4.4404421768553863
		2.2598051799385517 109.04762434456347 4.4319421102957737
		2.2475171218808536 108.91198669744252 4.423442043736161
		2.2352290638231547 108.77634905032157 4.4149419771765483
		3.1747086409591865 109.16290283203125 3.760765408020013
		3.2458617150489348 109.18195074138714 3.7037689168865362
		3.3170147891386832 109.20099865074303 3.6467724257530585
		3.388167863228432 109.22004656009892 3.5897759346195812
		2.2972116599858832 109.15121027705807 4.5560073009639463
		2.2771324862185942 109.2279896295115 4.6162941526896466
		2.2570533124513052 109.30476898196491 4.6765810044153477
		2.2369741386840167 109.38154833441831 4.736867856141048
		1.4311761856079102 109.05900967234849 4.9025911615072495
		1.4375858635159122 109.22525210176586 4.9606291785003043
		1.4439955414239138 109.39149453118326 5.0186671954933573
		1.4504052193319155 109.55773696060062 5.0767052124864112
		
		;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "75C8C7B7-476A-FA4F-B74C-7DAC85D30986";
	setAttr -s 12 ".lnk";
	setAttr -s 12 ".slnk";
createNode displayLayerManager -n "layerManager";
	rename -uid "34E8E699-426E-2561-6909-44BBFE93C72F";
	setAttr ".cdl" 8;
	setAttr -s 8 ".dli[1:7]"  2 7 3 4 5 6 1;
createNode displayLayer -n "defaultLayer";
	rename -uid "C727DD4C-46DC-70CF-E3DB-EEB0233D4E25";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "1A376E06-4C32-A00B-9A75-9D839C9DB835";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "EBE06E2E-41B9-59FA-6C4A-DFAD766C2862";
	setAttr ".g" yes;
createNode ilrOptionsNode -s -n "TurtleRenderOptions";
	rename -uid "CC8990E2-41BA-1A03-0E85-CFBAD4FA743C";
lockNode -l 1 ;
createNode ilrUIOptionsNode -s -n "TurtleUIOptions";
	rename -uid "9CCC2772-4E6F-6E70-4F30-459E766E3C48";
lockNode -l 1 ;
createNode ilrBakeLayerManager -s -n "TurtleBakeLayerManager";
	rename -uid "AAFC2330-42F5-7B61-2327-28B5A2BADD44";
lockNode -l 1 ;
createNode ilrBakeLayer -s -n "TurtleDefaultBakeLayer";
	rename -uid "89707F4B-4615-836D-0CED-85AFE7CE974D";
lockNode -l 1 ;
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "D49B07CC-4ECC-06DD-3D45-749E53C33406";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "D0637BD5-485A-5FBC-1BDC-7D879CB9EC18";
createNode shadingEngine -n "CornTmp_MatSG1";
	rename -uid "15B6E08F-43EB-B30B-1C82-AE8416966C26";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo10";
	rename -uid "7AB99925-47B9-4DE7-14F0-0CAF2A0D61B5";
createNode shadingEngine -n "EyeballTmp_MatSG1";
	rename -uid "36D48727-4B3C-261E-B115-5ABBD76D67DF";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo11";
	rename -uid "7AE95C4A-412E-6F92-B34F-D0809B71AD59";
createNode shadingEngine -n "IrisTmp_MatSG1";
	rename -uid "3CAF6085-4A17-9512-4295-108EE2923AE1";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo12";
	rename -uid "560564B3-464B-E127-209D-ECB30A4D6DE7";
createNode shadingEngine -n "PupilTmp_MatSG1";
	rename -uid "EA5D0EB2-4B1F-E6A9-6401-44B7DE643E20";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo13";
	rename -uid "4529CAC8-4950-D455-C437-AAAA1E8F79E2";
createNode shadingEngine -n "RedTmp_MatSG";
	rename -uid "59677213-4BC4-0D80-0B63-A8A6C1F5B28C";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo14";
	rename -uid "ED40EA2F-4630-9425-CE67-8797EED0ADE2";
createNode shadingEngine -n "BlueTmp_MatSG";
	rename -uid "BCDFD418-4A24-DBEB-157E-FABCD87D2A94";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo15";
	rename -uid "08034E11-4445-CB1C-B57F-00ABF6015A20";
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "0DEEF6AA-4E79-9B46-6CFF-15ACBB504DC9";
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" 1928.5713519368883 -1674.9489130353463 ;
	setAttr ".tgi[0].vh" -type "double2" 5810.7140548172538 1549.0781697376813 ;
createNode shadingEngine -n "lambert2SG";
	rename -uid "95F1B622-4ED3-0785-CE01-B5BA9D55B9FE";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo16";
	rename -uid "85DCB6B7-4220-9489-AFFF-EB89A458D9CC";
createNode shadingEngine -n "lambert3SG";
	rename -uid "59E46F99-4F51-C09C-61B4-36857B9628C2";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo17";
	rename -uid "C7F9BD93-4D87-DA49-B273-16B0A4D2992A";
createNode shadingEngine -n "lambert4SG";
	rename -uid "E581F82B-47EA-2D1A-26C8-329A86D66A53";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo18";
	rename -uid "20D05B8C-4B48-B528-9D60-46874836FFEA";
createNode shadingEngine -n "lambert5SG";
	rename -uid "BBF7D82C-42F8-252F-D1F3-3BB3441D4F7E";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo19";
	rename -uid "11194AB2-4295-4F38-DF5D-19A39197A011";
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "F7B6C362-4B7A-908E-957C-D887FB46EEBE";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
select -ne :time1;
	setAttr -av -k on ".cch";
	setAttr -av -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 0;
	setAttr -av -k on ".unw";
	setAttr -k on ".etw";
	setAttr -k on ".tps";
	setAttr -av -k on ".tms";
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr -k on ".mbsof";
	setAttr ".msaa" yes;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 12 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
select -ne :renderGlobalsList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
select -ne :defaultShaderList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
	setAttr -k on ".ihi";
select -ne :initialShadingGroup;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
select -ne :initialParticleSE;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
select -ne :defaultResolution;
	setAttr -av -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -av -k on ".w";
	setAttr -av -k on ".h";
	setAttr -av -k on ".pa" 1;
	setAttr -av -k on ".al";
	setAttr -av -k on ".dar";
	setAttr -av -k on ".ldar";
	setAttr -k on ".dpi";
	setAttr -av -k on ".off";
	setAttr -av -k on ".fld";
	setAttr -av -k on ".zsl";
	setAttr -k on ".isu";
	setAttr -k on ".pdu";
select -ne :hardwareRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k off -cb on ".ctrs" 256;
	setAttr -av -k off -cb on ".btrs" 512;
	setAttr -k off -cb on ".fbfm";
	setAttr -k off -cb on ".ehql";
	setAttr -k off -cb on ".eams";
	setAttr -k off -cb on ".eeaa";
	setAttr -k off -cb on ".engm";
	setAttr -k off -cb on ".mes";
	setAttr -k off -cb on ".emb";
	setAttr -av -k off -cb on ".mbbf";
	setAttr -k off -cb on ".mbs";
	setAttr -k off -cb on ".trm";
	setAttr -k off -cb on ".tshc";
	setAttr -k off -cb on ".enpt";
	setAttr -k off -cb on ".clmt";
	setAttr -k off -cb on ".tcov";
	setAttr -k off -cb on ".lith";
	setAttr -k off -cb on ".sobc";
	setAttr -k off -cb on ".cuth";
	setAttr -k off -cb on ".hgcd";
	setAttr -k off -cb on ".hgci";
	setAttr -k off -cb on ".mgcs";
	setAttr -k off -cb on ".twa";
	setAttr -k off -cb on ".twz";
	setAttr -cb on ".hwcc";
	setAttr -cb on ".hwdp";
	setAttr -cb on ".hwql";
	setAttr -k on ".hwfr";
	setAttr -k on ".soll";
	setAttr -k on ".sosl";
	setAttr -k on ".bswa";
	setAttr -k on ".shml";
	setAttr -k on ".hwel";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "CornTmp_MatSG1.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "EyeballTmp_MatSG1.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "IrisTmp_MatSG1.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "PupilTmp_MatSG1.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "RedTmp_MatSG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "BlueTmp_MatSG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert2SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert3SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert4SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert5SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "CornTmp_MatSG1.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "EyeballTmp_MatSG1.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "IrisTmp_MatSG1.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "PupilTmp_MatSG1.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "RedTmp_MatSG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "BlueTmp_MatSG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert2SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert3SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert4SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert5SG.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr ":TurtleDefaultBakeLayer.idx" ":TurtleBakeLayerManager.bli[0]";
connectAttr ":TurtleRenderOptions.msg" ":TurtleDefaultBakeLayer.rset";
connectAttr "CornTmp_MatSG1.msg" "materialInfo10.sg";
connectAttr "EyeballTmp_MatSG1.msg" "materialInfo11.sg";
connectAttr "IrisTmp_MatSG1.msg" "materialInfo12.sg";
connectAttr "PupilTmp_MatSG1.msg" "materialInfo13.sg";
connectAttr "RedTmp_MatSG.msg" "materialInfo14.sg";
connectAttr "BlueTmp_MatSG.msg" "materialInfo15.sg";
connectAttr "lambert2SG.msg" "materialInfo16.sg";
connectAttr "lambert3SG.msg" "materialInfo17.sg";
connectAttr "lambert4SG.msg" "materialInfo18.sg";
connectAttr "lambert5SG.msg" "materialInfo19.sg";
connectAttr "CornTmp_MatSG1.pa" ":renderPartition.st" -na;
connectAttr "EyeballTmp_MatSG1.pa" ":renderPartition.st" -na;
connectAttr "IrisTmp_MatSG1.pa" ":renderPartition.st" -na;
connectAttr "PupilTmp_MatSG1.pa" ":renderPartition.st" -na;
connectAttr "RedTmp_MatSG.pa" ":renderPartition.st" -na;
connectAttr "BlueTmp_MatSG.pa" ":renderPartition.st" -na;
connectAttr "lambert2SG.pa" ":renderPartition.st" -na;
connectAttr "lambert3SG.pa" ":renderPartition.st" -na;
connectAttr "lambert4SG.pa" ":renderPartition.st" -na;
connectAttr "lambert5SG.pa" ":renderPartition.st" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "RbnLipRigTmp_NrbShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr ":perspShape.msg" ":defaultRenderGlobals.sc";
// End of MthRigTmpLocs.ma
