import maya.cmds as mc

import os
import sys
sys.path.append(r'X:\Lani Pixels\Libraries\Maya\mayaLpPython')

from lpRig import rigTools as lrr
from lpRig import core as lrc

from lpRig import rigData
reload(rigData)
from lpRig import mainGroup
reload(mainGroup)

from lpRig import rootRig
reload(rootRig)
from lpRig import wingRig
reload(wingRig)
from lpRig import pelvisRig
reload(pelvisRig)
from lpRig import spineRig
reload(spineRig)
from lpRig import neckRig
reload(neckRig)
from lpRig import headRig
reload(headRig)
from lpRig import hindLegRig
reload(hindLegRig)
from lpRig import thumbRig
reload(thumbRig)
from lpRig import fingerRig
reload(fingerRig)

def main():

	print 'Rigging Rooster'

	sn = os.path.normpath(mc.file(q=True, l=True)[0])
	fldPath, fn  = os.path.split(sn)
	assetName = fn.split('_')[0]

	print 'Creating Main Groups'
	mgObj = mainGroup.MainGroup(assetName)

	print 'Creating Root Rig'
	rootObj = rootRig.RootRig(
								ctrlGrp=mgObj.Ctrl_Grp,
								skinGrp=mgObj.Skin_Grp,
								tmpJnt='Root_Jnt',
								part=''
							)

	print 'Creating Pelvis Rig'
	pelvisObj = pelvisRig.PelvisRig(
										parent=rootObj.Root_Jnt,
										ctrlGrp=mgObj.Ctrl_Grp,
										tmpJnt='Pelvis_Jnt',
										part=''
									)

	print 'Creating Spine Rig'
	spineObj = spineRig.SpineRig(
									parent=rootObj.Root_Jnt,
									ctrlGrp=mgObj.Ctrl_Grp,
									jntGrp=mgObj.Jnt_Grp,
									ikhGrp=mgObj.Ikh_Grp,
									skinGrp=mgObj.Skin_Grp,
									stillGrp=mgObj.Still_Grp,
									ribbon=False,
									ax='z',
									tmpJnt=[
												'Spine_1_Jnt',
												'Spine_2_Jnt',
												'Spine_3_Jnt',
												'ChestPiv_Jnt',
											],
									part=''
								)

	# Spine fix
	spinePos = lrc.Null()
	spinePosZr = lrc.group(spinePos)

	spinePos.name = 'SpinePos_Grp'
	spinePosZr.name = 'SpinePosZr_Grp'

	mc.pointConstraint(pelvisObj.Pelvis_Jnt, spinePosZr)
	spinePos.snapPoint(spineObj.Spine_1_Jnt)
	mc.pointConstraint(spinePos, spineObj.SpineIkRootCtrlZr_Grp)

	spinePosZr.parent(mgObj.Ctrl_Grp)

	mc.parentConstraint(rootObj.Root_Jnt, spineObj.SpineIkCtrlLoc_Grp, mo=True)

	print 'Creating Neck Rig'
	neckObj = neckRig.NeckRig(
								parent=spineObj.Spine_2_Jnt,
								ctrlGrp=mgObj.Ctrl_Grp,
								skinGrp=mgObj.Skin_Grp,
								jntGrp=mgObj.Jnt_Grp,
								stillGrp=mgObj.Still_Grp,
								ax='y',
								tmpJnt=[
										'Neck_Jnt',
										'NeckTip_Jnt'
										],
								part='',
								headJnt='Head_Jnt'
							)

	print 'Creating Head Rig'
	headObj = headRig.HeadRig(
								parent=neckObj.NeckTip_Jnt,
								ctrlGrp=mgObj.Ctrl_Grp,
								tmpJnt=[
											'Head_Jnt',
											'HeadTip_Jnt',
											'Eye_L_Jnt',
											'Eye_R_Jnt',
											'',
											'',
											'',
											'EyeTar_Jnt',
											'EyeTar_L_Jnt',
											'EyeTar_R_Jnt'
										],
								part=''
							)

	print 'Creating Left Wing Rig'
	lftWing = wingRig.WingRig(
								parent=spineObj.Spine_2_Jnt,
								ctrlGrp=mgObj.Ctrl_Grp,
								jntGrp=mgObj.Jnt_Grp,
								ikhGrp=mgObj.Ikh_Grp,
								skinGrp=mgObj.Skin_Grp,
								stillGrp=mgObj.Still_Grp,
								ribbon=True,
								tmpJnt=[
											'UpArm_L_Jnt',
											'Forearm_L_Jnt',
											'Wrist_L_Jnt',
											'Hand_L_Jnt',
											'ElbowIk_L_Jnt',
											'UpArmWing_L_Jnt', 'UpArmWingTip_L_Jnt',
											'ForearmWing_L_Jnt', 'ForearmWingTip_L_Jnt',
											'WristWing_L_Jnt', 'WristWingTip_L_Jnt',
											'HandWing_L_Jnt', 'HandWingTip_L_Jnt'
										],
								part='',
								side='L',
								primNo=7,
								secNo=7,
								scapNo=4
							)

	print 'Creating Right Wing Rig'
	rgtWing = wingRig.WingRig(
								parent=spineObj.Spine_2_Jnt,
								ctrlGrp=mgObj.Ctrl_Grp,
								jntGrp=mgObj.Jnt_Grp,
								ikhGrp=mgObj.Ikh_Grp,
								skinGrp=mgObj.Skin_Grp,
								stillGrp=mgObj.Still_Grp,
								ribbon=True,
								tmpJnt=[
											'UpArm_R_Jnt',
											'Forearm_R_Jnt',
											'Wrist_R_Jnt',
											'Hand_R_Jnt',
											'ElbowIk_R_Jnt',
											'UpArmWing_R_Jnt', 'UpArmWingTip_R_Jnt',
											'ForearmWing_R_Jnt', 'ForearmWingTip_R_Jnt',
											'WristWing_R_Jnt', 'WristWingTip_R_Jnt',
											'HandWing_R_Jnt', 'HandWingTip_R_Jnt'
										],
								part='',
								side='R',
								primNo=7,
								secNo=7,
								scapNo=4
							)

	print 'Creating Left Leg Rig'
	lftLegObj = hindLegRig.HindLegRig(
											parent=pelvisObj.Pelvis_Jnt,
											ctrlGrp=mgObj.Ctrl_Grp,
											jntGrp=mgObj.Jnt_Grp,
											ikhGrp=mgObj.Ikh_Grp,
											skinGrp=mgObj.Skin_Grp,
											stillGrp=mgObj.Still_Grp,
											ribbon=True,
											tmpJnt=[
														'UpperLeg_L_Jnt',
														'MiddleLeg_L_Jnt',
														'LowerLeg_L_Jnt',
														'Ankle_L_Jnt',
														'Ball_L_Jnt',
														'Toe_L_Jnt',
														'Heel_L_Jnt',
														'FootIn_L_Jnt',
														'FootOut_L_Jnt',
														'KneeIk_L_Jnt',
														'UpKneeIk_L_Jnt',
														'LowKneeIk_L_Jnt'
													],
											part='',
											side='L'
										)

	print 'Creating Left Thumb Toe Rig'
	lftThumbToeObj = thumbRig.ThumbRig(
										thumb='Thumb',
										parent=lftLegObj.Toe_Jnt,
										armCtrl=lftLegObj.Leg_Ctrl,
										ctrlGrp=mgObj.Ctrl_Grp,
										tmpJnt=(
													'ThumbToe_1_L_Jnt',
													'ThumbToe_2_L_Jnt',
													'ThumbToe_3_L_Jnt',
													'ThumbToe_4_L_Jnt'
												),
										side='L'
									)
	ctrlShp = lrc.Dag(lftLegObj.Leg_Ctrl.shape)
	fngr = 'Thumb'
	ctrlShp.attr('%sFlatRx_1' % fngr).value = -2
	ctrlShp.attr('%sFlatRz_1' % fngr).value = -0.5
	ctrlShp.attr('%sFlatRx_2' % fngr).value = -0.3
	ctrlShp.attr('%sFlatRx_3' % fngr).value = 3.3

	print 'Creating Left Index Toe Rig'
	lftIdxToebj = fingerRig.FingerRig(
											fngr='Index',
											parent=lftLegObj.Toe_Jnt,
											armCtrl=lftLegObj.Leg_Ctrl,
											ctrlGrp=mgObj.Ctrl_Grp,
											tmpJnt=(
														'IndexToe_1_L_Jnt',
														'IndexToe_2_L_Jnt',
														'IndexToe_3_L_Jnt',
														'IndexToe_4_L_Jnt',
														'IndexToe_5_L_Jnt'
													),
											side='L'
										)
	ctrlShp = lrc.Dag(lftLegObj.Leg_Ctrl.shape)
	fngr = 'Index'
	ctrlShp.attr('%sFlatRx_1' % fngr).value = -2
	ctrlShp.attr('%sFlatRz_1' % fngr).value = -0.5
	ctrlShp.attr('%sFlatRx_2' % fngr).value = -0.3
	ctrlShp.attr('%sFlatRx_3' % fngr).value = 3.3
	ctrlShp.attr('%sFlatRx_4' % fngr).value = 1
	ctrlShp.attr('%sCupRx_1' % fngr).value = 0
	ctrlShp.attr('%sCupRx_2' % fngr).value = -1
	ctrlShp.attr('%sCupRx_3' % fngr).value = -1
	ctrlShp.attr('%sCupRx_4' % fngr).value = -1
	ctrlShp.attr('%sSprd_2' % fngr).value = -9
	ctrlShp.attr('%sBsSprd_1' % fngr).value = -9

	print 'Creating Left Middle Toe Rig'
	lftMiddleToeObj = fingerRig.FingerRig(
											fngr='Middle',
											parent=lftLegObj.Toe_Jnt,
											armCtrl=lftLegObj.Leg_Ctrl,
											ctrlGrp=mgObj.Ctrl_Grp,
											tmpJnt=(
														'MiddleToe_1_L_Jnt',
														'MiddleToe_2_L_Jnt',
														'MiddleToe_3_L_Jnt',
														'MiddleToe_4_L_Jnt',
														'MiddleToe_5_L_Jnt'
													),
											side='L'
										)
	ctrlShp = lrc.Dag(lftLegObj.Leg_Ctrl.shape)
	fngr = 'Middle'
	ctrlShp.attr('%sFlatRx_1' % fngr).value = -2
	ctrlShp.attr('%sFlatRz_1' % fngr).value = -0.2
	ctrlShp.attr('%sFlatRx_2' % fngr).value = -0.4
	ctrlShp.attr('%sFlatRx_3' % fngr).value = 3.5
	ctrlShp.attr('%sFlatRx_4' % fngr).value = 1.25
	ctrlShp.attr('%sCupRx_1' % fngr).value = 0
	ctrlShp.attr('%sCupRx_2' % fngr).value = -2.5
	ctrlShp.attr('%sCupRx_3' % fngr).value = -2.5
	ctrlShp.attr('%sCupRx_4' % fngr).value = -2.5
	ctrlShp.attr('%sSprd_2' % fngr).value = -4.5
	ctrlShp.attr('%sBsSprd_1' % fngr).value = -4.5

	print 'Creating Left Pinky Toe Rig'
	lftPinkyToeObj = fingerRig.FingerRig(
											fngr='Pinky',
											parent=lftLegObj.Toe_Jnt,
											armCtrl=lftLegObj.Leg_Ctrl,
											ctrlGrp=mgObj.Ctrl_Grp,
											tmpJnt=(
														'PinkyToe_1_L_Jnt',
														'PinkyToe_2_L_Jnt',
														'PinkyToe_3_L_Jnt',
														'PinkyToe_4_L_Jnt',
														'PinkyToe_5_L_Jnt'
													),
											side='L'
										)
	ctrlShp = lrc.Dag(lftLegObj.Leg_Ctrl.shape)
	fngr = 'Pinky'
	ctrlShp.attr('%sFlatRx_1' % fngr).value = -2
	ctrlShp.attr('%sFlatRz_1' % fngr).value = 0.8
	ctrlShp.attr('%sFlatRx_2' % fngr).value = -0.5
	ctrlShp.attr('%sFlatRx_3' % fngr).value = 4.3
	ctrlShp.attr('%sFlatRx_4' % fngr).value = 0.65
	ctrlShp.attr('%sCupRx_1' % fngr).value = 0
	ctrlShp.attr('%sCupRx_2' % fngr).value = -4.5
	ctrlShp.attr('%sCupRx_3' % fngr).value = -4.5
	ctrlShp.attr('%sCupRx_4' % fngr).value = -4.5
	ctrlShp.attr('%sSprd_2' % fngr).value = 9
	ctrlShp.attr('%sBsSprd_1' % fngr).value = 9

	print 'Creating Right Leg Rig'
	rgtLegObj = hindLegRig.HindLegRig(
											parent=pelvisObj.Pelvis_Jnt,
											ctrlGrp=mgObj.Ctrl_Grp,
											jntGrp=mgObj.Jnt_Grp,
											ikhGrp=mgObj.Ikh_Grp,
											skinGrp=mgObj.Skin_Grp,
											stillGrp=mgObj.Still_Grp,
											ribbon=True,
											tmpJnt=[
														'UpperLeg_R_Jnt',
														'MiddleLeg_R_Jnt',
														'LowerLeg_R_Jnt',
														'Ankle_R_Jnt',
														'Ball_R_Jnt',
														'Toe_R_Jnt',
														'Heel_R_Jnt',
														'FootIn_R_Jnt',
														'FootOut_R_Jnt',
														'KneeIk_R_Jnt',
														'UpKneeIk_R_Jnt',
														'LowKneeIk_R_Jnt'
													],
											part='',
											side='R'
										)

	print 'Creating Right Thumb Toe Rig'
	rgtThumbToeObj = thumbRig.ThumbRig(
										thumb='Thumb',
										parent=rgtLegObj.Toe_Jnt,
										armCtrl=rgtLegObj.Leg_Ctrl,
										ctrlGrp=mgObj.Ctrl_Grp,
										tmpJnt=(
													'ThumbToe_1_R_Jnt',
													'ThumbToe_2_R_Jnt',
													'ThumbToe_3_R_Jnt',
													'ThumbToe_4_R_Jnt'
												),
										side='R'
									)
	ctrlShp = lrc.Dag(rgtLegObj.Leg_Ctrl.shape)
	fngr = 'Thumb'
	ctrlShp.attr('%sFlatRx_1' % fngr).value = -2
	ctrlShp.attr('%sFlatRz_1' % fngr).value = -0.5
	ctrlShp.attr('%sFlatRx_2' % fngr).value = -0.3
	ctrlShp.attr('%sFlatRx_3' % fngr).value = 3.3

	print 'Creating Right Index Toe Rig'
	rgtIdxToebj = fingerRig.FingerRig(
											fngr='Index',
											parent=rgtLegObj.Toe_Jnt,
											armCtrl=rgtLegObj.Leg_Ctrl,
											ctrlGrp=mgObj.Ctrl_Grp,
											tmpJnt=(
														'IndexToe_1_R_Jnt',
														'IndexToe_2_R_Jnt',
														'IndexToe_3_R_Jnt',
														'IndexToe_4_R_Jnt',
														'IndexToe_5_R_Jnt'
													),
											side='R'
										)
	ctrlShp = lrc.Dag(rgtLegObj.Leg_Ctrl.shape)
	fngr = 'Index'
	ctrlShp.attr('%sFlatRx_1' % fngr).value = -2
	ctrlShp.attr('%sFlatRz_1' % fngr).value = -0.5
	ctrlShp.attr('%sFlatRx_2' % fngr).value = -0.3
	ctrlShp.attr('%sFlatRx_3' % fngr).value = 3.3
	ctrlShp.attr('%sFlatRx_4' % fngr).value = 1
	ctrlShp.attr('%sCupRx_1' % fngr).value = 0
	ctrlShp.attr('%sCupRx_2' % fngr).value = -1
	ctrlShp.attr('%sCupRx_3' % fngr).value = -1
	ctrlShp.attr('%sCupRx_4' % fngr).value = -1
	ctrlShp.attr('%sSprd_2' % fngr).value = -9
	ctrlShp.attr('%sBsSprd_1' % fngr).value = -9

	print 'Creating Right Middle Toe Rig'
	rgtMiddleToeObj = fingerRig.FingerRig(
											fngr='Middle',
											parent=rgtLegObj.Toe_Jnt,
											armCtrl=rgtLegObj.Leg_Ctrl,
											ctrlGrp=mgObj.Ctrl_Grp,
											tmpJnt=(
														'MiddleToe_1_R_Jnt',
														'MiddleToe_2_R_Jnt',
														'MiddleToe_3_R_Jnt',
														'MiddleToe_4_R_Jnt',
														'MiddleToe_5_R_Jnt'
													),
											side='R'
										)
	ctrlShp = lrc.Dag(rgtLegObj.Leg_Ctrl.shape)
	fngr = 'Middle'
	ctrlShp.attr('%sFlatRx_1' % fngr).value = -2
	ctrlShp.attr('%sFlatRz_1' % fngr).value = -0.2
	ctrlShp.attr('%sFlatRx_2' % fngr).value = -0.4
	ctrlShp.attr('%sFlatRx_3' % fngr).value = 3.5
	ctrlShp.attr('%sFlatRx_4' % fngr).value = 1.25
	ctrlShp.attr('%sCupRx_1' % fngr).value = 0
	ctrlShp.attr('%sCupRx_2' % fngr).value = -2.5
	ctrlShp.attr('%sCupRx_3' % fngr).value = -2.5
	ctrlShp.attr('%sCupRx_4' % fngr).value = -2.5
	ctrlShp.attr('%sSprd_2' % fngr).value = -4.5
	ctrlShp.attr('%sBsSprd_1' % fngr).value = -4.5

	print 'Creating Right Pinky Toe Rig'
	rgtPinkyToeObj = fingerRig.FingerRig(
											fngr='Pinky',
											parent=rgtLegObj.Toe_Jnt,
											armCtrl=rgtLegObj.Leg_Ctrl,
											ctrlGrp=mgObj.Ctrl_Grp,
											tmpJnt=(
														'PinkyToe_1_R_Jnt',
														'PinkyToe_2_R_Jnt',
														'PinkyToe_3_R_Jnt',
														'PinkyToe_4_R_Jnt',
														'PinkyToe_5_R_Jnt'
													),
											side='R'
										)
	ctrlShp = lrc.Dag(rgtLegObj.Leg_Ctrl.shape)
	fngr = 'Pinky'
	ctrlShp.attr('%sFlatRx_1' % fngr).value = -2
	ctrlShp.attr('%sFlatRz_1' % fngr).value = 0.8
	ctrlShp.attr('%sFlatRx_2' % fngr).value = -0.5
	ctrlShp.attr('%sFlatRx_3' % fngr).value = 4.3
	ctrlShp.attr('%sFlatRx_4' % fngr).value = 0.65
	ctrlShp.attr('%sCupRx_1' % fngr).value = 0
	ctrlShp.attr('%sCupRx_2' % fngr).value = -4.5
	ctrlShp.attr('%sCupRx_3' % fngr).value = -4.5
	ctrlShp.attr('%sCupRx_4' % fngr).value = -4.5
	ctrlShp.attr('%sSprd_2' % fngr).value = 9
	ctrlShp.attr('%sBsSprd_1' % fngr).value = 9

	try:
		rigData.readCtrlShape()
	except:
		pass

	mc.delete('TmpJnt_Grp')
	mc.delete('PivJnt_Grp')