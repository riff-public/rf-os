from functools import partial

import maya.cmds as mc
import maya.mel as mm

class SelBkmkr(object):

	def __init__(self):

		self.ui = 'selBkmkr'
		self.win = '%sWin' % self.ui

	def show(self):

		if mc.window(self.win, exists=True):
			mc.deleteUI(self.win)

		mc.window(self.win, t=self.ui, rtf=True)

		# Main Column Layout
		self.mainCl = '%sMainCl' % self.ui
		mc.columnLayout(self.mainCl, adj=True)

		mc.separator()

		# Step
		self.stepIfg = '%sStepIfg' % self.ui
		mc.intFieldGrp(self.stepIfg, numberOfFields=1, label='Expand', extraLabel='Steps')

		mc.separator()

		# Button - Add
		mc.button(l='Add', h=50, c=partial(self.addClicked))

		mc.separator()

		mc.showWindow(self.win)
		mc.window(self.win, e=True, w=310 )
		mc.window(self.win, e=True, h=485 )

	def addClicked(self, *args):

		step = mc.intFieldGrp(self.stepIfg, q=True, v1=True)
		sels = mc.ls(sl=True, fl=True, l=True)

		if not step:
			result = mc.promptDialog(
										title='Enter Bookmark Name',
										message='Enter Name:',
										button=['OK', 'Cancel'],
										defaultButton='OK',
										cancelButton='Cancel',
										dismissString='Cancel'
									)
			nm = mc.promptDialog(query=True, text=True)
			self.addSelection(sels=sels, bkmrkNm=nm)

		else:

			allSels = []

			for ix in range(step+1):

				currBkMrks = []
				currSels = mc.ls(sl=True, fl=True, l=True)

				for currSel in currSels:
					if not currSel in allSels:
						currBkMrks.append(currSel)
						allSels.append(currSel)

				self.addSelection(sels=currBkMrks, bkmrkNm=str(ix))
				mm.eval('GrowPolygonSelectionRegion;')

			mc.select(sels, r=True)

	def selClicked(self, *args):
		#0, shift 1, ctrl 4
		mod = mc.getModifiers()

		if mod == 1:
			mc.select(args[0], add=True)
		elif mod == 4:
			mc.select(args[0], d=True)
		else:
			mc.select(args[0], r=True)

	def removeClicked(self, *args):

		mc.deleteUI(args[0])
		mc.deleteUI(args[1])

	def addSelection(self, sels=[], bkmrkNm='', *args):

		mc.setParent(self.mainCl)

		currRl = mc.rowLayout(nc=2, cw2=[200, 100])

		mc.button(l=bkmrkNm, w=200, h=50, c=partial(self.selClicked, sels))
		rmBut = mc.button(l='Remove', w=105, h=50)

		mc.setParent('..')
		currSep = mc.separator()

		mc.button(rmBut, e=True, c=partial(self.removeClicked, currRl, currSep))

def run() :
	ui = SelBkmkr()
	ui.show()