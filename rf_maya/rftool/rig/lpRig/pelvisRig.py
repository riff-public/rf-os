# Maya modules
import maya.cmds as mc
import maya.mel as mm

# Custom modules
import lpRig.core as lrc
reload(lrc)
import lpRig.rigTools as lrr
reload(lrr)

class PelvisRig(object):
	
	'''
	Connector: Pelvis_Jnt
	'''
	
	def __init__(
					self,
					parent='Root_Jnt',
					ctrlGrp='Ctrl_Grp',
					tmpJnt='Pelvis_Jnt',
					part=''
				):

		self.Pelvis_Jnt = lrc.Dag(tmpJnt)

		self.PelvisSca_Jnt = lrr.jointAt(self.Pelvis_Jnt)
		self.PelvisSca_Jnt.name = 'Pelvis%sSca_Jnt' % part

		self.PelvisSca_Jnt.parent(self.Pelvis_Jnt)
		self.PelvisSca_Jnt.attr('ssc').v = 0
		
		# Main group
		self.Ctrl_Grp = lrc.Null()
		self.Ctrl_Grp.name = 'PelvisCtrl%s_Grp' % part
		mc.parentConstraint(parent, self.Ctrl_Grp)
		self.Ctrl_Grp.parent(ctrlGrp)
		
		# Controller
		self.Pelvis_Ctrl = lrr.jointControl('cube')
		self.Pelvis_Ctrl.name = 'Pelvis%s_Ctrl' % part
		self.Pelvis_Ctrl.color = 'red'
		self.Pelvis_Ctrl.lockHideAttrs('v')

		self.PelvisCtrlZr_Grp = lrc.group(self.Pelvis_Ctrl)
		self.PelvisCtrlZr_Grp.name = 'Pelvis%sCtrlZr_Grp' % part

		self.PelvisCtrlZr_Grp.snapPoint(self.Pelvis_Jnt)
		self.Pelvis_Ctrl.snapOrient(self.Pelvis_Jnt)
		self.Pelvis_Ctrl.freeze()
		self.PelvisCtrlZr_Grp.parent(self.Ctrl_Grp)

		# Positioning
		self.PelvisCtrlZr_Grp.snapPoint( self.Pelvis_Jnt )
		self.Pelvis_Ctrl.snapOrient( self.Pelvis_Jnt )
		self.Pelvis_Ctrl.freeze()

		# Adding Gimbal
		self.PelvisGmbl_Ctrl = lrc.addGimbal(self.Pelvis_Ctrl)
		self.PelvisGmbl_Ctrl.name = 'Pelvis%sGmbl_Ctrl' % part
		self.PelvisGmbl_Ctrl.scaleShape(0.8)

		# Rotate order adjustment
		self.Pelvis_Ctrl.rotateOrder = 'xzy'
		self.PelvisGmbl_Ctrl.rotaeOrder = 'xzy'

		# Connect to joint
		mc.parentConstraint(self.PelvisGmbl_Ctrl, self.Pelvis_Jnt)
		mc.scaleConstraint(self.PelvisGmbl_Ctrl, self.PelvisSca_Jnt)