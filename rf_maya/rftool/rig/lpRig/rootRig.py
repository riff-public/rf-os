# Root rig module
import maya.cmds as mc
import lpRig.core as lrc
import lpRig.rigTools as lrr
reload(lrc)
reload(lrr)

class RootRig(object):
	
	'''
	Connector: Root_Jnt
	'''
	
	def __init__(
					self,
					ctrlGrp='Ctrl_Grp',
					skinGrp='Skin_Grp',
					tmpJnt='Root_Jnt',
					part=''
				):

		# Skin Joint
		self.Root_Jnt = lrc.Dag(tmpJnt)
		self.Root_Jnt.parent(skinGrp)
		
		# Main Group
		self.Ctrl_Grp = lrc.Null()
		self.Ctrl_Grp.name = 'RootCtrl%s_Grp' % part
		
		# Controllers
		self.Root_Ctrl = lrr.jointControl('circle')
		self.Root_Ctrl.name = 'Root%s_Ctrl' % part
		self.Root_Ctrl.color = 'green'
		self.Root_Ctrl.rotateOrder = 'xzy'
		self.Root_Ctrl.lockHideAttrs('sx', 'sy', 'sz', 'v')
		self.Root_Ctrl.scaleShape(1.25)
		
		self.RootGmbl_Ctrl = lrc.addGimbal(self.Root_Ctrl)
		self.RootGmbl_Ctrl.name = 'Root%sGmbl_Ctrl' % part
		self.RootGmbl_Ctrl.scaleShape(0.8)
		self.RootGmbl_Ctrl.rotateOrder = 'xzy'
		
		self.RootCtrlZr_Grp = lrc.group(self.Root_Ctrl)
		self.RootCtrlZr_Grp.name = 'Root%sCtrlZr_Grp' % part
		self.RootCtrlZr_Grp.snap(tmpJnt)
		
		self.Root_JntParentConstraint = mc.parentConstraint(self.RootGmbl_Ctrl, self.Root_Jnt)
		
		self.RootCtrlZr_Grp.parent(self.Ctrl_Grp)
		self.Ctrl_Grp.parent(ctrlGrp)
