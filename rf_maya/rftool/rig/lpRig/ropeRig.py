# Maya modules
import maya.cmds as mc
import maya.mel as mm

# Custom modules
import lpRig.core as lrc
import lpRig.rigTools as lrr
reload(lrc)
reload(lrr)

class RopeRig(object):
	"""
	:Contractor: skinJnts
	"""
	def __init__(
					self,
					parent='',
					ctrlGrp='',
					jntGrp='',
					ikhGrp='',
					skinGrp='',
					stillGrp='',
					tmpLocs=[],
					ctrlNo=4,
					jntNo=12,
					part='',
					test=False
				):
		col = 'yellow'
		secCol = 'softYellow'
		
		locA = lrc.Dag(tmpLocs[0])
		locB = lrc.Dag(tmpLocs[1])
		
		# Main groups
		self.Ctrl_Grp = lrc.Null()
		self.Ctrl_Grp.name = 'RpCtrl%s_Grp' % part
		if mc.objExists(parent):
			mc.parentConstraint(parent, self.Ctrl_Grp)
		if mc.objExists(ctrlGrp):
			self.Ctrl_Grp.parent(ctrlGrp)
		
		self.Jnt_Grp = lrc.Null()
		self.Jnt_Grp.name = 'RpJnt%s_Grp' % part
		if mc.objExists(jntGrp):
			self.Jnt_Grp.parent(jntGrp)

		self.Ikh_Grp = lrc.Null()
		self.Ikh_Grp.name = 'RpIkh%s_Grp' % part
		if mc.objExists(ikhGrp):
			self.Ikh_Grp.parent(ikhGrp)
		
		self.Skin_Grp = lrc.Null()
		self.Skin_Grp.name = 'RpSkin%s_Grp' % part
		if mc.objExists(skinGrp):
			self.Skin_Grp.parent(skinGrp)
		
		self.Still_Grp = lrc.Null()
		self.Still_Grp.name = 'RpStill%s_Grp' % part
		if mc.objExists(skinGrp):
			self.Still_Grp.parent(stillGrp)
		
		# IK Curve
		self.Ik_Crv = lrc.Dag(mc.curve(d=1, p=[locA.getWs(), locB.getWs()]))
		mc.rebuildCurve(self.Ik_Crv, ch=False, s=ctrlNo-1, d=3)
		self.Ik_Crv.name = 'RpIk%s_Crv' % part
		self.Ik_Crv.parent(self.Still_Grp)

		self.IkLen_Crv = lrc.Dag(mc.duplicate(self.Ik_Crv, rr=True)[0])
		self.IkLen_Crv.name = 'RpIkLen%s_Crv' % part
		self.IkLen_Crv.add(ln='origLen', k=True)
		self.IkLen_Crv.hide()
		self.IkLen_Crv.parent(self.Jnt_Grp)

		self.IkLenCrv_Cif = lrc.Node(mc.arclen(self.IkLen_Crv, ch=True))
		self.IkLenCrv_Cif.name = 'RpIkLenCrv%s_Cif' % part

		self.IkLen_Crv.attr('origLen').v = self.IkLenCrv_Cif.attr('arcLength').v

		self.IkLen_Mdv = lrc.MultiplyDivide()
		self.IkLen_Mdv.name = 'RpIkLen%s_Mdv' % part
		self.IkLen_Mdv.attr('operation').v = 2

		self.IkLenCrv_Cif.attr('arcLength') >> self.IkLen_Mdv.attr('i1x')
		self.IkLen_Crv.attr('origLen') >> self.IkLen_Mdv.attr('i2x')

		# Main controllers
		self.ctrlObjs = []
		self.twAccumPmas = []
		fstCtrlShp = None

		tmpCtrlGrp = lrc.Null()
		tmpCtrlGrp.snap(locA)

		ctrlOfst = lrc.distance(locA, locB)/(ctrlNo - 1)

		for ix in xrange(ctrlNo):

			idx = ix + 1
			currOfst = ctrlOfst * ix

			currCtrlObj = lrc.SplineFkControl()
			currCtrlObj.rename('Rp%s' % part, idx, '')
			currCtrlObj.ctrl.color = col
			currCtrlObj.ctrl.add(ln='squash', k=True)
			currCtrlObj.postCtrl.color = secCol
			currCtrlObj.zrGrp.snap(tmpCtrlGrp)
			currCtrlObj.zrGrp.parent(tmpCtrlGrp)

			currCtrlObj.zrGrp.attr('ty').v = currOfst

			self.ctrlObjs.append(currCtrlObj)

			# Accumulating twist value
			currCtrlObj.ctrl.add(ln='twist', k=True)
			currCtrlObj.ctrl.add(ln='accumTw')
			currAccumTwPma = lrc.PlusMinusAverage()
			currAccumTwPma.name = 'RpCtrlTwAccum%s_%s_Pma' % (part, idx)

			currCtrlObj.ctrl.attr('twist') >> currAccumTwPma.last1D()
			currAccumTwPma.attr('output1D') >> currCtrlObj.ctrl.attr('accumTw')

			if ix == 0:
				# For the first controller
				currCtrlObj.zrGrp.parent(self.Ctrl_Grp)
				
				fstCtrlShp = lrc.Dag(currCtrlObj.ctrl.shape)
				fstCtrlShp.add(ln='detail', k=True, min=0, max=1)
				
				# Local/World
				if ctrlGrp:
					lrr.doOriLocWar(str(currCtrlObj.ctrl), currCtrlObj.zrGrp, 
										ctrlGrp, currCtrlObj.oriGrp)
			else:
				currCtrlObj.zrGrp.parent(self.ctrlObjs[ix-1].ctrl)

				# Accumulating twist value
				for ctrlObj in self.ctrlObjs[1:]:
					ctrlObj.twJnt.attr('ry') >> currAccumTwPma.last1D()

		mc.delete(tmpCtrlGrp)

		# Binding IK curve.
		jnts = [x.jnt for x in self.ctrlObjs]
		skc = mc.skinCluster(jnts, self.Ik_Crv, dr=100, mi=1)

		# IK/Skin Joints
		self.IkJnt_Grp = lrc.Null()
		self.IkJnt_Grp.name = 'RpIkJnt%s_Grp' % part
		self.IkJnt_Grp.parent(self.Ctrl_Grp)

		self.IkJntZr_1_Grp = lrc.Null()
		self.IkJntZr_1_Grp.name = 'RpIkJntZr%s_1_Grp' % part
		self.IkJntZr_1_Grp.snap(locA)
		self.IkJntZr_1_Grp.parent(self.IkJnt_Grp)
		mc.parentConstraint(self.ctrlObjs[0].ctrl, self.IkJntZr_1_Grp)

		self.ikJnts = []
		self.ikOffGrps = []
		self.ikTwGrps = []
		self.skinSpcGrps = []

		self.skinJnts = []
		self.pocis = []
		self.ikDists = []

		jntOfst = lrc.distance(locA, locB)/(jntNo - 1)
		for ix in xrange(jntNo):

			idx = ix + 1
			currOfst = jntOfst*ix

			# IK joint
			currIkJnt = lrc.Joint()
			currIkJnt.name = 'RpIk%s_%s_Jnt' % (part, idx)
			currIkJnt.drawStyle = 'none'
			self.ikJnts.append(currIkJnt)

			currIkOffGrp = lrc.Null()
			currIkOffGrp.name = 'RpIkJntOff%s_%s_Grp' % (part, idx)
			currIkOffGrp.parent(currIkJnt)
			self.ikOffGrps.append(currIkOffGrp)

			# Twist group
			currIkTwGrp = lrc.Null()
			currIkTwGrp.name = 'RpIkJntTw%s_%s_Grp' % (part, idx)
			currIkTwGrp.parent(currIkOffGrp)
			self.ikTwGrps.append(currIkTwGrp)

			# Detail control
			currIkCtrlZrGrp = lrc.Null()
			currIkCtrlZrGrp.name = 'RpIkCtrlZr%s_%s_Grp' % (part, idx)
			currIkCtrlZrGrp.parent(currIkTwGrp)
			fstCtrlShp.attr('detail') >> currIkCtrlZrGrp.attr('v')

			currIkCtrl = lrc.Control('sphere')
			currIkCtrl.name = 'RpIk%s_%s_Ctrl' % (part, idx)
			currIkCtrl.color = col
			currIkCtrl.lockHideAttrs('v')
			currIkCtrl.parent(currIkCtrlZrGrp)

			# Skin space group
			currSkinSpcGrp = lrc.Null()
			currSkinSpcGrp.name = 'RpSkinSpc%s_%s_Grp' % (part, idx)
			currSkinSpcGrp.parent(currIkCtrl)
			self.skinSpcGrps.append(currSkinSpcGrp)

			# Positioning IK joint
			currIkJnt.snap(self.IkJntZr_1_Grp)
			currIkJnt.parent(self.IkJntZr_1_Grp)
			currIkJnt.freeze(r=True)
			currIkJnt.attr('ty').v = currOfst

			# Skin joint
			currSkinJnt = lrc.Joint()
			currSkinJnt.name = 'Rp%s_%s_Jnt' % (part, idx)
			currSkinJnt.snap(currSkinSpcGrp)
			currSkinJnt.freeze(r=True)
			mc.parentConstraint(currSkinSpcGrp, currSkinJnt)
			mc.scaleConstraint(currSkinSpcGrp, currSkinJnt)
			currSkinJnt.parent(self.Skin_Grp)
			self.skinJnts.append(currSkinJnt)

			# Poci
			currPoci = lrc.PointOnCurveInfo()
			currPoci.name = 'RpIk%s_%s_Poci' % (part, idx)
			self.Ik_Crv.attr('worldSpace[0]') >> currPoci.attr('ic')
			currPoci.attr('turnOnPercentage').v = 1
			currPoci.attr('parameter').v = (1.00/(jntNo - 1.00))*ix
			self.pocis.append(currPoci)

			# Finding current joint position and influence controllers.
			currJntPost = (float(ctrlNo-1)/float(jntNo-1)) * ix
			infs, wgtVal = self._findInfluenceWeight(currJntPost)

			# Calculating twist
			currIkTwGrp.add(ln='fstWgt', k=True)
			currIkTwGrp.add(ln='scdWgt', k=True)
			currIkTwGrp.add(ln='fstTwVal', k=True)
			currIkTwGrp.add(ln='scdTwVal', k=True)

			fstTwMdv = lrc.MultiplyDivide()
			fstTwMdv.name = 'RpIkFstTw%s_%s_Mdv' % (part, idx)

			scdTwMdv = lrc.MultiplyDivide()
			scdTwMdv.name = 'RpIkScdTw%s_%s_Mdv' % (part, idx)

			currIkTwGrp.attr('fstWgt') >> fstTwMdv.attr('i1x')
			currIkTwGrp.attr('fstTwVal') >> fstTwMdv.attr('i2x')
			currIkTwGrp.attr('scdWgt') >> scdTwMdv.attr('i1x')
			currIkTwGrp.attr('scdTwVal') >> scdTwMdv.attr('i2x')

			sumTwPma = lrc.PlusMinusAverage()
			sumTwPma.name = 'RpIkSumTw%s_%s_Pma' % (part, idx)

			fstTwMdv.attr('ox') >> sumTwPma.last1D()
			scdTwMdv.attr('ox') >> sumTwPma.last1D()
			sumTwPma.attr('output1D') >> currIkTwGrp.attr('ry')

			# Calculating squash
			currSkinJnt.add(ln='fstWgt', k=True)
			currSkinJnt.add(ln='scdWgt', k=True)
			currSkinJnt.add(ln='fstSqsVal', k=True)
			currSkinJnt.add(ln='scdSqsVal', k=True)

			fstSqsMdv = lrc.MultiplyDivide()
			fstSqsMdv.name = 'RpIkFstSqs%s_%s_Mdv' % (part, idx)

			scdSqsMdv = lrc.MultiplyDivide()
			scdSqsMdv.name = 'RpIkScdSqs%s_%s_Mdv' % (part, idx)

			currSkinJnt.attr('fstWgt') >> fstSqsMdv.attr('i1x')
			currSkinJnt.attr('fstSqsVal') >> fstSqsMdv.attr('i2x')
			currSkinJnt.attr('scdWgt') >> scdSqsMdv.attr('i1x')
			currSkinJnt.attr('scdSqsVal') >> scdSqsMdv.attr('i2x')

			sumSqsPma = lrc.PlusMinusAverage()
			sumSqsPma.name = 'RpIkSumSqs%s_%s_Pma' % (part, idx)
			sumSqsPma.add(ln='default', dv=1)

			sumSqsPma.attr('default') >> sumSqsPma.last1D()
			fstSqsMdv.attr('ox') >> sumSqsPma.last1D()
			scdSqsMdv.attr('ox') >> sumSqsPma.last1D()
			# sumSqsPma.attr('output1D') >> currSkinJnt.attr('sx')
			# sumSqsPma.attr('output1D') >> currSkinJnt.attr('sz')
			sumSqsPma.attr('output1D') >> currIkCtrlZrGrp.attr('sx')
			sumSqsPma.attr('output1D') >> currIkCtrlZrGrp.attr('sz')

			if len(infs) == 1:
				# If current joint has only one influence control.
				# For twist
				currIkTwGrp.attr('fstWgt').v = 1
				self.ctrlObjs[infs[0]].ctrl.attr('accumTw') >> currIkTwGrp.attr('fstTwVal')

				# For squash
				currSkinJnt.attr('fstWgt').v = 1
				self.ctrlObjs[infs[0]].ctrl.attr('squash') >> currSkinJnt.attr('fstSqsVal')
			else:
				# If current joint has more than one influence control.
				# For twist
				currIkTwGrp.attr('fstWgt').v = wgtVal
				currIkTwGrp.attr('scdWgt').v = 1 - wgtVal
				self.ctrlObjs[infs[0]].ctrl.attr('accumTw') >> currIkTwGrp.attr('fstTwVal')
				self.ctrlObjs[infs[1]].ctrl.attr('accumTw') >> currIkTwGrp.attr('scdTwVal')

				# For squash
				currSkinJnt.attr('fstWgt').v = wgtVal
				currSkinJnt.attr('scdWgt').v = 1 - wgtVal
				self.ctrlObjs[infs[0]].ctrl.attr('squash') >> currSkinJnt.attr('fstSqsVal')
				self.ctrlObjs[infs[1]].ctrl.attr('squash') >> currSkinJnt.attr('scdSqsVal')

			# From the second joint
			if not ix == 0:

				# Orig distance
				currIkJnt.add(ln='origDist', k=True, dv=jntOfst)
				currIkJnt.attr('origDist').lock = True

				# Parenting
				currIkJnt.parent(self.ikJnts[ix-1])
				currSkinJnt.parent(self.skinJnts[ix-1])

				# Distance Between
				currDist = lrc.DistanceBetween()
				currDist.name = 'RpIk%s_%s_Dist' % (part, idx)
				self.ikDists.append(currDist)

				self.pocis[ix-1].attr('result.position') >> currDist.attr('p1')
				self.pocis[ix].attr('result.position') >> currDist.attr('p2')

				# Stetch Multiply Divide
				currDivMdv = lrc.MultiplyDivide()
				currDivMdv.name = 'RpIkStrchDiv%s_%s_Mdv' % (part, idx)

				currDivMdv.attr('operation').v = 2
				currDist.attr('d') >> currDivMdv.attr('i1x')

				currScaMdv = lrc.MultiplyDivide()
				currScaMdv.name = 'RpIkStrchSca%s_%s_Mdv' % (part, idx)

				currIkJnt.attr('origDist') >> currScaMdv.attr('i1x')
				self.IkLen_Mdv.attr('ox') >> currScaMdv.attr('i2x')

				currScaMdv.attr('ox') >> currDivMdv.attr('i2x')

				currMulMdv = lrc.MultiplyDivide()
				currMulMdv.name = 'RpIkStrchMul%s_%s_Mdv' % (part, idx)
				currDivMdv.attr('ox') >> currMulMdv.attr('i1x')
				currIkJnt.attr('origDist') >> currMulMdv.attr('i2x')
				currMulMdv.attr('ox') >> currIkJnt.attr('ty')
				
			if test:
				currSkinJnt.attr('displayLocalAxis').v = True

		# Spline IK
		self.Rp_Ikh = lrc.IkShoot(sj=self.ikJnts[0], ee=self.ikJnts[-1], c=self.Ik_Crv, 
									sol='ikSplineSolver', ccv=False, pcv=False)
		self.Rp_Ikh.name = 'Rp%s_Ikh' % part
		self.Rp_Ikh.parent(self.Ikh_Grp)

	def _findInfluenceWeight(self, jntPost=0):
		"""Find inluence controllers and weight to 
		the first influence from joint position.
		"""
		mod = jntPost % 1

		if not mod:
			return [int(jntPost - mod)], 0
		else:
			return [int(jntPost - mod), int((jntPost - mod) + 1)], 1 - mod