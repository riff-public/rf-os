# Maya modules
import maya.cmds as mc
import maya.mel as mm

# Custom modules
import lpRig.core as lrc
import lpRig.rigTools as lrr
reload(lrc)
reload(lrr)

class HumanSpineRig(object):
	
	'''
	Connector: Spine_5_Jnt
	'''
	
	def __init__(
					self ,
					parent='Root_Jnt',
					ctrlGrp='Ctrl_Grp',
					jntGrp='Jnt_Grp',
					skinGrp='Skin_Grp',
					stillGrp='Still_Grp',
					ax='y',
					tmpJnt=(
								'Spine_1_Jnt',
								'Spine_2_Jnt',
								'Spine_3_Jnt',
								'Spine_4_Jnt',
								'Spine_5_Jnt'
							),
					part=''
				):

		# Template nodes
		self.Spine_1_Jnt  = lrc.Dag(tmpJnt[0])
		self.Spine_2_Jnt  = lrc.Dag(tmpJnt[1])
		self.Spine_3_Jnt  = lrc.Dag(tmpJnt[2])
		self.Spine_4_Jnt  = lrc.Dag(tmpJnt[3])
		self.Spine_5_Jnt  = lrc.Dag(tmpJnt[4])
		
		# Skin joints - Rotate order adjustment
		self.Spine_1_Jnt.rotateOrder = 'yzx'
		self.Spine_2_Jnt.rotateOrder = 'yzx'
		self.Spine_3_Jnt.rotateOrder = 'yzx'
		self.Spine_4_Jnt.rotateOrder = 'yzx'
		self.Spine_5_Jnt.rotateOrder = 'yzx'

		# Scale joints
		self.SpineSca_1_Jnt = lrr.jointAt(self.Spine_1_Jnt)
		self.SpineSca_2_Jnt = lrr.jointAt(self.Spine_2_Jnt)
		self.SpineSca_3_Jnt = lrr.jointAt(self.Spine_3_Jnt)
		self.SpineSca_4_Jnt = lrr.jointAt(self.Spine_4_Jnt)

		self.SpineSca_1_Jnt.parent(self.Spine_1_Jnt)
		self.SpineSca_2_Jnt.parent(self.Spine_2_Jnt)
		self.SpineSca_3_Jnt.parent(self.Spine_3_Jnt)
		self.SpineSca_4_Jnt.parent(self.Spine_4_Jnt)

		self.SpineSca_1_Jnt.name = 'Spine%sSca_1_Jnt' % part
		self.SpineSca_2_Jnt.name = 'Spine%sSca_2_Jnt' % part
		self.SpineSca_3_Jnt.name = 'Spine%sSca_3_Jnt' % part
		self.SpineSca_4_Jnt.name = 'Spine%sSca_4_Jnt' % part

		self.SpineSca_1_Jnt.attr('radius').v = self.SpineSca_1_Jnt.attr('radius').v*2
		self.SpineSca_2_Jnt.attr('radius').v = self.SpineSca_2_Jnt.attr('radius').v*2
		self.SpineSca_3_Jnt.attr('radius').v = self.SpineSca_3_Jnt.attr('radius').v*2
		self.SpineSca_4_Jnt.attr('radius').v = self.SpineSca_4_Jnt.attr('radius').v*2

		# Main group
		self.SpineCtrl_Grp = lrc.Null()
		self.SpineCtrl_Grp.name = 'SpineCtrl%s_Grp' % part
		mc.parentConstraint(parent, self.SpineCtrl_Grp)
		self.SpineCtrl_Grp .parent(ctrlGrp)

		# Controller
		# Controller - Spine_1_Ctrl
		self.Spine_1_Ctrl = lrr.jointControl('circle')
		self.Spine_1_Ctrl.name = 'Spine%s_1_Ctrl' % part
		self.Spine_1_Ctrl.color = 'red'
		self.Spine_1_Ctrl.lockHideAttrs('v')

		self.SpineGmbl_1_Ctrl = lrc.addGimbal(self.Spine_1_Ctrl)
		self.SpineGmbl_1_Ctrl.name = 'Spine%sGmbl_1_Ctrl' % part
		self.SpineGmbl_1_Ctrl.scaleShape(0.8)

		self.SpineCtrlOfst_1_Grp = lrc.group(self.Spine_1_Ctrl)
		self.SpineCtrlOfst_1_Grp.name = 'Spine%sCtrlOfst_1_Ctrl' % part
		self.SpineCtrlZr_1_Grp = lrc.group(self.SpineCtrlOfst_1_Grp)
		self.SpineCtrlZr_1_Grp.name = 'Spine%sCtrlZr_1_Ctrl' % part

		# Controller - Spine_2_Ctrl
		self.Spine_2_Ctrl = lrr.jointControl('circle')
		self.Spine_2_Ctrl.name = 'Spine%s_2_Ctrl' % part
		self.Spine_2_Ctrl.color = 'red'
		self.Spine_2_Ctrl.lockHideAttrs('v')

		self.SpineGmbl_2_Ctrl = lrc.addGimbal(self.Spine_2_Ctrl)
		self.SpineGmbl_2_Ctrl.name = 'Spine%sGmbl_2_Ctrl' % part
		self.SpineGmbl_2_Ctrl.scaleShape(0.8)

		self.SpineCtrlOfst_2_Grp = lrc.group(self.Spine_2_Ctrl)
		self.SpineCtrlOfst_2_Grp.name = 'Spine%sCtrlOfst_2_Ctrl' % part
		self.SpineCtrlZr_2_Grp = lrc.group(self.SpineCtrlOfst_2_Grp)
		self.SpineCtrlZr_2_Grp.name = 'Spine%sCtrlZr_2_Ctrl' % part

		# Controller - Spine_3_Ctrl
		self.Spine_3_Ctrl = lrr.jointControl('circle')
		self.Spine_3_Ctrl.name = 'Spine%s_3_Ctrl' % part
		self.Spine_3_Ctrl.color = 'red'
		self.Spine_3_Ctrl.lockHideAttrs('v')

		self.SpineGmbl_3_Ctrl = lrc.addGimbal(self.Spine_3_Ctrl)
		self.SpineGmbl_3_Ctrl.name = 'Spine%sGmbl_3_Ctrl' % part
		self.SpineGmbl_3_Ctrl.scaleShape(0.8)

		self.SpineCtrlOfst_3_Grp = lrc.group(self.Spine_3_Ctrl)
		self.SpineCtrlOfst_3_Grp.name = 'Spine%sCtrlOfst_3_Ctrl' % part
		self.SpineCtrlZr_3_Grp = lrc.group(self.SpineCtrlOfst_3_Grp)
		self.SpineCtrlZr_3_Grp.name = 'Spine%sCtrlZr_3_Ctrl' % part

		# Controller - Spine_4_Ctrl
		self.Spine_4_Ctrl = lrr.jointControl('circle')
		self.Spine_4_Ctrl.name = 'Spine%s_4_Ctrl' % part
		self.Spine_4_Ctrl.color = 'red'
		self.Spine_4_Ctrl.lockHideAttrs('v')

		self.SpineGmbl_4_Ctrl = lrc.addGimbal(self.Spine_4_Ctrl)
		self.SpineGmbl_4_Ctrl.name = 'Spine%sGmbl_4_Ctrl' % part
		self.SpineGmbl_4_Ctrl.scaleShape(0.8)

		self.SpineCtrlOfst_4_Grp = lrc.group(self.Spine_4_Ctrl)
		self.SpineCtrlOfst_4_Grp.name = 'Spine%sCtrlOfst_4_Ctrl' % part
		self.SpineCtrlZr_4_Grp = lrc.group(self.SpineCtrlOfst_4_Grp)
		self.SpineCtrlZr_4_Grp.name = 'Spine%sCtrlZr_4_Ctrl' % part

		# Controller - Rotate order adjustment
		self.Spine_1_Ctrl.rotateOrder = 'yzx'
		self.SpineGmbl_1_Ctrl.rotateOrder = 'yzx'
		self.Spine_2_Ctrl.rotateOrder = 'yzx'
		self.SpineGmbl_2_Ctrl.rotateOrder = 'yzx'
		self.Spine_3_Ctrl.rotateOrder = 'yzx'
		self.SpineGmbl_3_Ctrl.rotateOrder = 'yzx'
		self.Spine_4_Ctrl.rotateOrder = 'yzx'
		self.SpineGmbl_4_Ctrl.rotateOrder = 'yzx'

		# Controller - Parenting and positioning
		self.SpineCtrlZr_1_Grp.snapPoint(self.Spine_1_Jnt)
		self.Spine_1_Ctrl.snapOrient(self.Spine_1_Jnt)
		self.Spine_1_Ctrl.freeze(r=True)

		self.SpineCtrlZr_2_Grp.snapPoint(self.Spine_1_Jnt)
		self.SpineCtrlZr_2_Grp.snapOrient(self.Spine_1_Jnt)
		self.SpineCtrlOfst_2_Grp.snapPoint(self.Spine_2_Jnt)
		self.Spine_2_Ctrl.snapOrient(self.Spine_2_Jnt)
		self.Spine_2_Ctrl.freeze(r=True)

		self.SpineCtrlZr_3_Grp.snapPoint(self.Spine_2_Jnt)
		self.SpineCtrlZr_3_Grp.snapOrient(self.Spine_2_Jnt)
		self.SpineCtrlOfst_3_Grp.snapPoint(self.Spine_3_Jnt)
		self.Spine_3_Ctrl.snapOrient(self.Spine_3_Jnt)
		self.Spine_3_Ctrl.freeze(r=True)

		self.SpineCtrlZr_4_Grp.snapPoint(self.Spine_3_Jnt)
		self.SpineCtrlZr_4_Grp.snapOrient(self.Spine_3_Jnt)
		self.SpineCtrlOfst_4_Grp.snapPoint(self.Spine_4_Jnt)
		self.Spine_4_Ctrl.snapOrient(self.Spine_4_Jnt)
		self.Spine_4_Ctrl.freeze(r=True)

		mc.pointConstraint(self.SpineGmbl_1_Ctrl, self.SpineCtrlZr_2_Grp)
		mc.orientConstraint(self.SpineGmbl_1_Ctrl, self.SpineCtrlZr_2_Grp)
		mc.pointConstraint(self.SpineGmbl_2_Ctrl, self.SpineCtrlZr_3_Grp)
		mc.orientConstraint(self.SpineGmbl_2_Ctrl, self.SpineCtrlZr_3_Grp)
		mc.pointConstraint(self.SpineGmbl_3_Ctrl, self.SpineCtrlZr_4_Grp)
		mc.orientConstraint(self.SpineGmbl_3_Ctrl, self.SpineCtrlZr_4_Grp)

		self.SpineCtrlZr_1_Grp.parent(self.SpineCtrl_Grp)
		self.SpineCtrlZr_2_Grp.parent(self.SpineCtrl_Grp)
		self.SpineCtrlZr_3_Grp.parent(self.SpineCtrl_Grp)
		self.SpineCtrlZr_4_Grp.parent(self.SpineCtrl_Grp)

		# Stretch
		(self.SpineStretch_1_Add,
		self.SpineStretch_1_Mul) = lrr.fkStretch(
														ctrl=self.Spine_1_Ctrl,
														target=self.SpineCtrlOfst_2_Grp,
														ax=ax
													)
		self.SpineStretch_1_Add.name = 'Spine%sStretch_1_Add' % part
		self.SpineStretch_1_Mul.name = 'Spine%sStretch_1_Mul' % part

		(self.SpineStretch_2_Add,
		self.SpineStretch_2_Mul) = lrr.fkStretch(
														ctrl=self.Spine_2_Ctrl,
														target=self.SpineCtrlOfst_3_Grp,
														ax=ax
													)
		self.SpineStretch_2_Add.name = 'Spine%sStretch_2_Add' % part
		self.SpineStretch_2_Mul.name = 'Spine%sStretch_2_Mul' % part

		(self.SpineStretch_3_Add,
		self.SpineStretch_3_Mul) = lrr.fkStretch(
														ctrl=self.Spine_3_Ctrl,
														target=self.SpineCtrlOfst_4_Grp,
														ax=ax
													)
		self.SpineStretch_3_Add.name = 'Spine%sStretch_3_Add' % part
		self.SpineStretch_3_Mul.name = 'Spine%sStretch_3_Mul' % part

		(self.SpineStretch_4_Add,
		self.SpineStretch_4_Mul) = lrr.fkStretch(
														ctrl=self.Spine_4_Ctrl,
														target=self.Spine_5_Jnt,
														ax=ax
													)
		self.SpineStretch_4_Add.name = 'Spine%sStretch_4_Add' % part
		self.SpineStretch_4_Mul.name = 'Spine%sStretch_4_Mul' % part

		# Control - local/world setup
		(self.SpineCtrlLoc_1_Grp,
		self.SpineCtrlWor_1_Grp,
		self.SpineCtrlWor_1_GrpOrientConstraint,
		self.SpineCtrlZr_1_GrpOrientConstraint,
		self.SpineCtrlZrGrpOriCons_Rev) = lrr.orientLocalWorldCtrl(
																			self.Spine_1_Ctrl,
																			self.SpineCtrl_Grp,
																			ctrlGrp,
																			self.SpineCtrlZr_1_Grp
																		)

		self.SpineCtrlLoc_1_Grp.name = 'Spine%sCtrlLoc_1_Grp' % part
		self.SpineCtrlWor_1_Grp.name = 'Spine%sCtrlWor_1_Grp' % part
		self.SpineCtrlWor_1_GrpOrientConstraint.name = 'Spine%sCtrlWor_1_GrpOrientConstraint' % part
		self.SpineCtrlZr_1_GrpOrientConstraint.name = 'Spine%sCtrlZr_1_GrpOrientConstraint' % part
		self.SpineCtrlZrGrpOriCons_Rev.name = 'Spine%sCtrlZrGrpOriCons_Rev' % part

		# Connect to joint
		lrc.parentConstraint(self.SpineGmbl_1_Ctrl, self.Spine_1_Jnt)
		lrc.parentConstraint(self.SpineGmbl_2_Ctrl, self.Spine_2_Jnt)
		lrc.parentConstraint(self.SpineGmbl_3_Ctrl, self.Spine_3_Jnt)
		lrc.parentConstraint(self.SpineGmbl_4_Ctrl, self.Spine_4_Jnt)

		lrc.scaleConstraint(self.SpineGmbl_1_Ctrl, self.SpineSca_1_Jnt)
		lrc.scaleConstraint(self.SpineGmbl_2_Ctrl, self.SpineSca_2_Jnt)
		lrc.scaleConstraint(self.SpineGmbl_3_Ctrl, self.SpineSca_3_Jnt)
		lrc.scaleConstraint(self.SpineGmbl_4_Ctrl, self.SpineSca_4_Jnt)
