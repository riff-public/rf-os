# Maya modules
import maya.cmds as mc
import maya.mel as mm

# Custom modules
import lpRig.core as lrc
import lpRig.rigTools as lrr
reload(lrc)
reload(lrr)

def test():

	import mainGroup

	mgObj = mainGroup.MainGroup('Test')

	rigObj = RubberBandRig(
								parent=mgObj.Ctrl_Grp,
								ctrlGrp=mgObj.Ctrl_Grp,
								jntGrp=mgObj.Jnt_Grp,
								skinGrp=mgObj.Skin_Grp,
								stillGrp=mgObj.Still_Grp,
								crv='nurbsCircle5',
								jntNo=24,
								part='Necklace'
							)

class RubberLineRig(object):

	def  __init__(
					self,
					parent='',
					ctrlGrp='',
					jntGrp='',
					skinGrp='',
					stillGrp='',
					crv='',
					spans=0,
					jntNo=0,
					part=''
				):

		col = 'yellow'
		secCol = 'softYellow'

		self.crv = lrc.Dag(crv)
		mc.xform(self.crv, cp=True)
		self.crvShp = lrc.Dag(self.crv.shape)
		self.crvSpan = self.crvShp.attr('spans').v

		# Main groups
		self.Ctrl_Grp = lrc.Null()
		self.Ctrl_Grp.name = 'RbbCtrl%s_Grp' % part
		if mc.objExists(parent):
			mc.parentConstraint(parent, self.Ctrl_Grp)
			mc.scaleConstraint(parent, self.Ctrl_Grp)
		if mc.objExists(ctrlGrp):
			self.Ctrl_Grp.parent(ctrlGrp)

		self.Jnt_Grp = lrc.Null()
		self.Jnt_Grp.name = 'RbbJnt%s_Grp' % part
		if mc.objExists(jntGrp):
			self.Jnt_Grp.parent(jntGrp)

		self.Skin_Grp = lrc.Null()
		self.Skin_Grp.name = 'RbbSkin%s_Grp' % part
		if mc.objExists(skinGrp):
			self.Skin_Grp.parent(skinGrp)
		if mc.objExists(parent):
			mc.parentConstraint(parent, self.Skin_Grp)
			mc.scaleConstraint(parent, self.Skin_Grp)

		self.Still_Grp = lrc.Null()
		self.Still_Grp.name = 'RbbStill%s_Grp' % part
		if mc.objExists(skinGrp):
			self.Still_Grp.parent(stillGrp)

		# Creating NURBs surface
		# rebuildCurve -ch 1 -rpo 1 -rt 0 -end 1 -kr 0 -kcp 0 -kep 1 -kt 0 -s 6 
		# -d 3 -tol 1e-006 "nurbsCircle5";

		# offsetCurve  -ch on -rn false -cb 1 -st true -cl false -d 1 -tol 0.01 -sd 5 
		# -ugn false  "nurbsCircle5" ;
		rbcCmd = 'rebuildCurve -ch 0 -rpo 1 -rt 0 -end 1 -kr 0 -kcp 0 -kep 1 -kt 0 -s %s \
					-d 3 -tol 1e-006' % spans

		mm.eval('%s %s' % (rbcCmd, self.crv))

		ofstCmd = 'offsetCurve  -ch 0 -rn false -cb 1 -st true -cl false -tol 0.01 \
					-sd 5 -ugn false'

		crvA = lrc.Dag(mm.eval('%s -d -0.05 %s' % (ofstCmd, self.crv))[0])
		mm.eval('%s %s' % (rbcCmd, crvA))

		crvB = lrc.Dag(mm.eval('%s -d 0.05 %s' % (ofstCmd, self.crv))[0])
		mm.eval('%s %s' % (rbcCmd, crvB))

		self.Rbl_Nrb = lrc.Dag(mc.loft(crvB, crvA, ch=False)[0])
		self.Rbl_Nrb.name = 'Rbl%s_Nrb' % part
		rbsCmd = 'rebuildSurface -ch 0 -rpo 1 -rt 0 -end 1 -kr 2 -kcp 0 -kc 0 \
					-su 1 -du 3 -sv %s -dv 3 -tol 1e-006 -fr 0  -dir 2 \
					"%s";' % (spans, self.Rbl_Nrb)

		mm.eval(rbsCmd)
		self.Rbl_Nrb.parent(self.Still_Grp)

		# Main controllers
		self.Rbl_Ctrl = lrc.Control('square')
		self.Rbl_Ctrl.name = 'Rbl%s_Ctrl' % part
		self.Rbl_Ctrl.lockHideAttrs('v')
		self.Rbl_Ctrl.color = col
		self.Rbl_Ctrl.moveShape((0, -1, 0))
		self.Rbl_Ctrl.rotateShape((90, 0, 0))

		self.RblCtrlZr_Grp = lrc.group(self.Rbl_Ctrl)
		self.RblCtrlZr_Grp.name = '%sRblCtrlZr_Grp' % part
		self.RblCtrlZr_Grp.snap(self.crv)
		self.RblCtrlZr_Grp.parent(self.Ctrl_Grp)

		self.ctrls = []
		self.ofstGrps = []
		self.jnts = []

		for ix in xrange(spans + 1):

			ctrl = lrc.Control('sphere')
			ctrl.name = '%sRbl_%s_Ctrl' % (part, ix+1)
			ctrl.lockHideAttrs('s', 'v')
			ctrl.color = col
			ctrl.scaleShape(3)
			self.ctrls.append(ctrl)

			ctrlOfstGrp = lrc.group(ctrl)
			ctrlOfstGrp.name = '%sRblCtrlOfst_%s_Grp' % (part, ix+1)
			self.ofstGrps.append(ctrlOfstGrp)

			ctrlZrGrp = lrc.group(ctrlOfstGrp)
			ctrlZrGrp.name = '%sRblCtrlZr_%s_Grp' % (part, ix+1)
			ctrlZrGrp.parent(self.Rbl_Ctrl)
			
			# Positioning
			posi = lrc.PointOnSurfaceInfo()
			self.Rbl_Nrb.attr('worldSpace[0]') >> posi.attr('is')
			posi.attr('parameterU').v = 0.5
			posi.attr('parameterV').v = ix
			mc.xform(ctrlZrGrp, t=posi.attr('position').v, ws=True)

			# Orientation
			aimCon = lrc.AimConstraint()
			aimCon.attr('a').v = (0, 0, 1)
			aimCon.attr('u').v = (0, 1, 0)

			aimCon.attr('t').v = (0, 0, 0)
			aimCon.attr('r').v = (0, 0, 0)
			
			posi.attr('n') >> aimCon.attr('wu')
			posi.attr('tv') >> aimCon.attr('tg[0].tt')

			ctrlZrGrp.attr('r').v = aimCon.attr('cr').v

			mc.delete(posi)

			# Control joints
			jnt = lrc.Joint()
			jnt.name = '%sRblCtrl_%s_Jnt' % (part, ix+1)
			self.jnts.append(jnt)

			jntOffGrp = lrc.group(jnt)
			jntOffGrp.name = '%sRblCtrlJntOff_%s_Grp' % (part, ix+1)

			jntZrGrp = lrc.group(jntOffGrp)
			jntZrGrp.name = '%sRblCtrlJntZr_%s_Grp' % (part, ix+1)
			jntZrGrp.parent(self.Jnt_Grp)

			mc.parentConstraint(ctrl, jntZrGrp)
			mc.scaleConstraint(ctrl, jntZrGrp)

		# Binding skin
		skc = mc.skinCluster(self.jnts, self.Rbl_Nrb)[0]
		for ix in range(spans):
			currIdx = ix + 1
			if ix == 0:
				currIdx = '0:1'
			elif ix == len(self.jnts) - 1:
				currIdx = '%s:%s' % (spans + 1, spans + 2)

			currCvs = '%s.cv[0:3][%s]' % (self.Rbl_Nrb, currIdx)
			mc.skinPercent(skc, currCvs, tv=[(self.jnts[ix], 1)])

		# Generating POSIs
		self.DtlCtrl_Grp = lrc.Null()
		self.DtlCtrl_Grp.name = 'RblDtlCtrl%s_Grp' % part
		self.DtlCtrl_Grp.parent(self.Rbl_Ctrl)

		self.skinJnts = []
		self.dtlCtrls = []

		for ix in xrange(jntNo):

			# Point on surface
			pos = lrc.Null()
			pos.name = '%sRblPos_%s_Grp' % (part, ix+1)
			pos.parent(self.Still_Grp)

			posi = lrc.PointOnSurfaceInfo()
			posi.name = '%sRbl_%s_Posi' % (part, ix+1)
			posi.add(ln='offset', dv=0, k=True)
			
			self.Rbl_Nrb.attr('worldSpace[0]') >> posi.attr('is')
			posi.attr('position') >> pos.attr('t')
			posi.attr('parameterU').v = 0.5
			div = float(jntNo)
			mult = float(ix)
			posi.attr('parameterV').v = (spans/(div-1))*mult

			aimCon = lrc.AimConstraint()
			aimCon.name = '%s_aimConstraint' % pos
			aimCon.attr('a').v = (0, 1, 0)
			aimCon.attr('u').v = (1, 0, 0)

			aimCon.parent(pos)
			aimCon.attr('t').v = (0, 0, 0)
			aimCon.attr('r').v = (0, 0, 0)
			
			posi.attr('n') >> aimCon.attr('wu')
			posi.attr('tv') >> aimCon.attr('tg[0].tt')
			aimCon.attr('cr') >> pos.attr('r')			
			
			# Skin joint
			skinJnt = lrc.Joint()
			skinJnt.name = '%sRblSkin_%s_Jnt' % (part, ix+1)
			self.skinJnts.append(skinJnt)

			skinJntOffGrp = lrc.group(skinJnt)
			skinJntOffGrp.name = '%sRblSkinJntOff_%s_Grp' % (part, ix+1)

			skinJntZrGrp = lrc.group(skinJntOffGrp)
			skinJntZrGrp.name = '%sRblSkinJntZr_%s_Grp' % (part, ix+1)
			skinJntZrGrp.parent(self.Skin_Grp)
			
			# Detail controller
			ctrl = lrc.Control('cube')
			ctrl.name = '%sRblDtl_%s_Ctrl' % (part, ix+1)
			ctrl.lockHideAttrs('v')
			ctrl.color = secCol
			ctrl.moveShape((0, -1, 0))
			self.dtlCtrls.append(ctrl)

			ctrlOfstGrp = lrc.group(ctrl)
			ctrlOfstGrp.name = '%sRblDtlCtrlOfst_%s_Grp' % (part, ix+1)

			ctrlZrGrp = lrc.group(ctrlOfstGrp)
			ctrlZrGrp.name = '%sRblDtlCtrlZr_%s_Grp' % (part, ix+1)
			ctrlZrGrp.parent(self.DtlCtrl_Grp)

			mc.parentConstraint(pos, ctrlZrGrp)
			mc.parentConstraint(ctrl, skinJntZrGrp)
			mc.scaleConstraint(ctrl, skinJntZrGrp)

			# Adding offset along V
			ctrlShp = lrc.Dag(ctrl.shape)
			ctrlShp.add(ln='offsetV', k=True)

			ofstUMdv = lrc.MultiplyDivide()
			ofstUMdv.name = '%sRblCtrlOfstV_%s_Mdv' % (part, ix+1)
			ofstUMdv.attr('i2x').v = 0.1
			ctrlShp.attr('offsetV') >> ofstUMdv.attr('i1x')

			ofstUPma = lrc.PlusMinusAverage()
			ofstUPma.name = '%sRblCtrlOfstV_%s_Pma' % (part, ix+1)
			ofstUPma.add(ln='default', k=True)
			ofstUPma.attr('default').v = posi.attr('parameterV').v
			ofstUPma.attr('default') >> ofstUPma.last1D()

			ofstUMdv.attr('ox') >> ofstUPma.last1D()
			ofstUPma.attr('output1D') >> posi.attr('parameterV')

		# Cleanup
		mc.delete(self.crv)
		mc.delete(crvA)
		mc.delete(crvB)

class RubberBandRig(object):

	def __init__(
					self,
					parent='',
					ctrlGrp='',
					jntGrp='',
					skinGrp='',
					stillGrp='',
					crv='',
					jntNo=0,
					part=''
				):

		col = 'yellow'
		secCol = 'softYellow'

		self.crv = lrc.Dag(crv)
		mc.xform(self.crv, cp=True)
		self.crvShp = lrc.Dag(self.crv.shape)
		self.crvSpan = self.crvShp.attr('spans').v

		# Main groups
		self.Ctrl_Grp = lrc.Null()
		self.Ctrl_Grp.name = 'RbbCtrl%s_Grp' % part
		if mc.objExists(parent):
			mc.parentConstraint(parent, self.Ctrl_Grp)
			mc.scaleConstraint(parent, self.Ctrl_Grp)
		if mc.objExists(ctrlGrp):
			self.Ctrl_Grp.parent(ctrlGrp)
		
		self.Jnt_Grp = lrc.Null()
		self.Jnt_Grp.name = 'RbbJnt%s_Grp' % part
		if mc.objExists(jntGrp):
			self.Jnt_Grp.parent(jntGrp)

		self.Skin_Grp = lrc.Null()
		self.Skin_Grp.name = 'RbbSkin%s_Grp' % part
		if mc.objExists(skinGrp):
			self.Skin_Grp.parent(skinGrp)
		if mc.objExists(parent):
			mc.parentConstraint(parent, self.Skin_Grp)
			mc.scaleConstraint(parent, self.Skin_Grp)

		self.Still_Grp = lrc.Null()
		self.Still_Grp.name = 'RbbStill%s_Grp' % part
		if mc.objExists(skinGrp):
			self.Still_Grp.parent(stillGrp)

		# Creating NURBs surface
		crvA = lrc.Dag(mc.duplicate(self.crv, rr=True)[0])
		crvA.scaleShape(0.9)

		crvB = lrc.Dag(mc.duplicate(self.crv, rr=True)[0])
		crvB.scaleShape(1.1)

		self.Rbb_Nrb = lrc.Dag(mc.loft(crvA, crvB, ch=False)[0])
		cmd = 'rebuildSurface -ch 0 -rpo 1 -rt 0 -end 1 -kr 2 -kcp 0 -kc 0 \
				-su 1 -du 3 -sv %s -dv 3 -tol 1e-006 -fr 0  -dir 2 \
				"%s";' % (self.crvSpan, self.Rbb_Nrb)

		mm.eval(cmd)
		self.Rbb_Nrb.name = 'Rbb%s_Nrb' % part
		self.Rbb_Nrb.parent(self.Still_Grp)

		# Main controllers
		self.Rbb_Ctrl = lrc.Control('square')
		self.Rbb_Ctrl.name = 'Rbb%s_Ctrl' % part
		self.Rbb_Ctrl.lockHideAttrs('v')
		self.Rbb_Ctrl.color = col
		self.Rbb_Ctrl.moveShape((0, -1, 0))
		self.Rbb_Ctrl.rotateShape((90, 0, 0))

		self.RbbCtrlZr_Grp = lrc.group(self.Rbb_Ctrl)
		self.RbbCtrlZr_Grp.name = '%sRbbCtrlZr_Grp' % part
		self.RbbCtrlZr_Grp.snap(self.crv)
		self.RbbCtrlZr_Grp.parent(self.Ctrl_Grp)

		self.ctrls = []
		self.ofstGrps = []
		self.jnts = []

		for ix in xrange(self.crvSpan):

			ctrl = lrc.Control('sphere')
			ctrl.name = '%sRbb_%s_Ctrl' % (part, ix+1)
			ctrl.lockHideAttrs('s', 'v')
			ctrl.color = 'yellow'
			self.ctrls.append(ctrl)

			ctrlOfstGrp = lrc.group(ctrl)
			ctrlOfstGrp.name = '%sRbbCtrlOfst_%s_Grp' % (part, ix+1)
			self.ofstGrps.append(ctrlOfstGrp)

			ctrlZrGrp = lrc.group(ctrlOfstGrp)
			ctrlZrGrp.name = '%sRbbCtrlZr_%s_Grp' % (part, ix+1)
			ctrlZrGrp.parent(self.Rbb_Ctrl)
			
			# Positioning
			posi = lrc.PointOnSurfaceInfo()
			self.Rbb_Nrb.attr('worldSpace[0]') >> posi.attr('is')
			posi.attr('parameterU').v = 0.5
			posi.attr('parameterV').v = (self.crvSpan - 1) + ix
			mc.xform(ctrlZrGrp, t=posi.attr('position').v, ws=True)

			# Orientation
			aimCon = lrc.AimConstraint()
			aimCon.attr('a').v = (0, 0, 1)
			aimCon.attr('u').v = (0, 1, 0)

			aimCon.attr('t').v = (0, 0, 0)
			aimCon.attr('r').v = (0, 0, 0)
			
			posi.attr('n') >> aimCon.attr('wu')
			posi.attr('tv') >> aimCon.attr('tg[0].tt')

			ctrlZrGrp.attr('r').v = aimCon.attr('cr').v

			mc.delete(posi)

			# Control joints
			jnt = lrc.Joint()
			jnt.name = '%sRbbCtrl_%s_Jnt' % (part, ix+1)
			self.jnts.append(jnt)

			jntOffGrp = lrc.group(jnt)
			jntOffGrp.name = '%sRbbCtrlJntOff_%s_Grp' % (part, ix+1)

			jntZrGrp = lrc.group(jntOffGrp)
			jntZrGrp.name = '%sRbbCtrlJntZr_%s_Grp' % (part, ix+1)
			jntZrGrp.parent(self.Jnt_Grp)

			mc.parentConstraint(ctrl, jntZrGrp)
			mc.scaleConstraint(ctrl, jntZrGrp)

		# Binding skin
		skc = mc.skinCluster(self.jnts, self.Rbb_Nrb)[0]
		for ix, jnt in enumerate(self.jnts):
			currCvs = '%s.cv[0:3][%s]' % (self.Rbb_Nrb, ix)
			mc.skinPercent(skc, currCvs, tv=[(jnt, 1)])
		
		# Generating POSIs
		self.DtlCtrl_Grp = lrc.Null()
		self.DtlCtrl_Grp.name = 'RbbDtlCtrl%s_Grp' % part
		self.DtlCtrl_Grp.parent(self.Rbb_Ctrl)

		self.skinJnts = []
		self.dtlCtrls = []

		for ix in xrange(jntNo):
			
			# Point on surface
			pos = lrc.Null()
			pos.name = '%sRbbPos_%s_Grp' % (part, ix+1)
			pos.parent(self.Still_Grp)

			posi = lrc.PointOnSurfaceInfo()
			posi.name = '%sRbb_%s_Posi' % (part, ix+1)
			posi.add(ln='offset', dv=0, k=True)
			
			self.Rbb_Nrb.attr('worldSpace[0]') >> posi.attr('is')
			posi.attr('position') >> pos.attr('t')
			posi.attr('parameterU').v = 0.5
			div = float(jntNo)
			mult = float(ix)
			posi.attr('parameterV').v = (self.crvSpan/div)*mult

			aimCon = lrc.AimConstraint()
			aimCon.name = '%s_aimConstraint' % pos
			aimCon.attr('a').v = (0, 1, 0)
			aimCon.attr('u').v = (1, 0, 0)

			aimCon.parent(pos)
			aimCon.attr('t').v = (0, 0, 0)
			aimCon.attr('r').v = (0, 0, 0)
			
			posi.attr('n') >> aimCon.attr('wu')
			posi.attr('tv') >> aimCon.attr('tg[0].tt')
			aimCon.attr('cr') >> pos.attr('r')			
			
			# Skin joint
			skinJnt = lrc.Joint()
			skinJnt.name = '%sRbbSkin_%s_Jnt' % (part, ix+1)
			self.skinJnts.append(skinJnt)

			skinJntOffGrp = lrc.group(skinJnt)
			skinJntOffGrp.name = '%sRbbSkinJntOff_%s_Grp' % (part, ix+1)

			skinJntZrGrp = lrc.group(skinJntOffGrp)
			skinJntZrGrp.name = '%sRbbSkinJntZr_%s_Grp' % (part, ix+1)
			skinJntZrGrp.parent(self.Skin_Grp)
			
			# Detail controller
			ctrl = lrc.Control('cube')
			ctrl.name = '%sRbbDtl_%s_Ctrl' % (part, ix+1)
			ctrl.lockHideAttrs('v')
			ctrl.color = 'yellow'
			ctrl.moveShape((0, -1, 0))
			self.dtlCtrls.append(ctrl)

			ctrlOfstGrp = lrc.group(ctrl)
			ctrlOfstGrp.name = '%sRbbDtlCtrlOfst_%s_Grp' % (part, ix+1)

			ctrlZrGrp = lrc.group(ctrlOfstGrp)
			ctrlZrGrp.name = '%sRbbDtlCtrlZr_%s_Grp' % (part, ix+1)
			ctrlZrGrp.parent(self.DtlCtrl_Grp)

			mc.parentConstraint(pos, ctrlZrGrp)
			mc.parentConstraint(ctrl, skinJntZrGrp)
			mc.scaleConstraint(ctrl, skinJntZrGrp)

			# Adding offset along V
			ctrlShp = lrc.Dag(ctrl.shape)
			ctrlShp.add(ln='offsetV', k=True)

			ofstUMdv = lrc.MultiplyDivide()
			ofstUMdv.name = '%sRbbCtrlOfstV_%s_Mdv' % (part, ix+1)
			ofstUMdv.attr('i2x').v = 0.1
			ctrlShp.attr('offsetV') >> ofstUMdv.attr('i1x')

			ofstUPma = lrc.PlusMinusAverage()
			ofstUPma.name = '%sRbbCtrlOfstV_%s_Pma' % (part, ix+1)
			ofstUPma.add(ln='default', k=True)
			ofstUPma.attr('default').v = posi.attr('parameterV').v
			ofstUPma.attr('default') >> ofstUPma.last1D()

			ofstUMdv.attr('ox') >> ofstUPma.last1D()
			ofstUPma.attr('output1D') >> posi.attr('parameterV')

		# Cleanup
		mc.delete(self.crv.name)
		mc.delete(crvA)
		mc.delete(crvB)