# sets -rm sculpt1Set orig1.vtx[145] ;
# select -d orig1.vtx[145] ;
setNm = 'sculpt1Set'
sels = mc.ls(sl=True, fl=True)
for sel in sels:
	mc.sets(sel, rm=setNm)

# Mirror selected shapes
import maya.cmds as mc
import maya.mel as mm

orig = 'orig'

for sel in mc.ls(sl=True):
	
	rgt = sel.replace('_L_', '_R_')

	if mc.objExists(orig):
		mc.select(orig, r=True)
		mc.select(rgt, add=True)
		mm.eval('copyShape;')
	
	mc.select(sel, r=True)
	mc.select(rgt, add=True)
		
	mm.eval('mirrorShape2 YZ 0 0 1 0.001;')

# Reset shape
orig = 'ebOrig'

for sel in mc.ls(sl=True):
	sel = mc.ls(sl=True)
	
	mc.select(orig, r=True)
	mc.select(sel, add=True)
	
	cmd = 'copyShape;'
	mm.eval(cmd)

# Mirror name
sels = mc.ls(sl=True)
mc.rename(sels[1], sels[0].replace('_L_', '_R_'))

# Parent constrain and hide
sels = mc.ls(sl=True)

mc.parentConstraint(sels[1], sels[0], mo=True)
mc.scaleConstraint(sels[1], sels[0], mo=True)

mc.setAttr('{}.v'.format(sels[0]), 0)

# Run main rig
import os
import sys

sn = os.path.normpath(mc.file(q=True, l=True)[0])
rigFldPath, flNmExt = os.path.split(sn)
assetFldPath, rigFld = os.path.split(rigFldPath)
typeFldPath, assetNm = os.path.split(assetFldPath)
rigDataFldPath = os.path.join(rigFldPath, 'rigData')

sys.path.append(rigDataFldPath)

mdlNm = '{}MainRig'.format(assetNm)

exec('import {}'.format(mdlNm))
exec('reload({})'.format(mdlNm))
exec('{}.main()'.format(mdlNm))

# Delete children constrain nodes
mc.delete(mc.listRelatives(mc.ls(sl=True)[0], ad=True, type='constraint'))

# Store constraint targets to dict
sels = mc.ls(sl=True)
geoToConTarDict = {}

for sel in sels:
	cons = mc.listConnections(sel, d=True, s=False, type='parentConstraint')
	jnts = mc.listConnections(cons[0], d=False, s=True, type='joint')
	
	geoToConTarDict[sel] = jnts[0]

# Constrain back to target
for key in geoToConTarDict.keys():
	
	tar = geoToConTarDict[key]
	
	mc.parentConstraint(tar, key, mo=True)
	mc.scaleConstraint(tar, key, mo=True)

# Constraint zerogroup to the second selected object.
# Then connect xform from controller to the second selected object.
sels = mc.ls(sl=True)
mc.delete(mc.parentConstraint(sels[0], sels[1]))

for attr in ('t', 'r', 's'):
	
	ctrl = mc.listRelatives(sels[1], type='transform')[0]
	
	mc.connectAttr(
						'{0}.{1}'.format(ctrl, attr),
						'{0}.{1}'.format(sels[0], attr)
					)

# Mirror locs
for sel in mc.ls(sl=True):
	dup = mc.duplicate(sel, rr=True)[0]
	for attr in ('tx', 'ry'):
		mc.setAttr('%s.%s' % (dup, attr), -mc.getAttr('%s.%s' % (sel, attr)))
	
	mc.rename(dup, sel.replace('_L_', '_R_'))

# Remove pasted
for each in mc.ls():
	if 'pasted__' in each:
		try:
			mc.rename(each, each.replace('pasted__', ''))
		except:
			pass

# Transfer blend shapes
bsn = 'MthRigBuf_Bsn_old'

pfxs = ['HeadMthRig_Geo']

bshs = mc.aliasAttr(bsn , q=True)

for ix in range(0, len(bshs), 2):
	
	bshAttr = '%s.%s' % (bsn, bshs[ix])
	
	if mc.getAttr(bshAttr, l=True): continue
	
	mc.setAttr(bshAttr, 1)
	
	for pfx in pfxs:
		
		src = '%s_new' % pfx
		tar = '%s_%s' % (pfx, bshs[ix])

		if not mc.objExists(tar):
			continue
		
		mc.select(src, r=True)
		mc.select(tar, add=True)
		
		cmd = 'copyShape;'
		mm.eval(cmd)
	
	mc.setAttr(bshAttr, 0)

# Get adjacent vertices
cnt = 'locator1'
selVtx = 'HeadJawRig_Geo.vtx[767]'

cntPos = mc.xform(cnt, q=True, t=True, ws=True)
vtxPos = mc.pointPosition(selVtx)
mainVec = lrc.diff(vtxPos, cntPos)

# Get adjacent vtcs and their vector from center object
adjVtcs = []
adjVtxVecs = []

fcs = mc.polyListComponentConversion(selVtx, tf=True)
for fc in fcs:
	for vtx in mc.ls(mc.polyListComponentConversion(fc, tv=True), fl=True):
				
		if vtx == selVtx:
			continue
		
		if not vtx in adjVtcs:
			adjVtcs.append(vtx)
			currVec = lrc.diff(mc.pointPosition(vtx), cntPos)
			angle = lrc.angle(mainVec, currVec)
			print vtx, angle

# Copy component selections.
sels = mc.ls(sl=True, fl=True)
tarGeo = sels[-1]

tarCmps = []

for sel in sels[:-1]:
	
	tarCmps.append('%s.%s' % (tarGeo, sel.split('.')[-1]))

mc.select(tarCmps, r=True)

# go to rest pose of the skinned obj
# select skinned obj, blend shape obj, jnt, target obj
import maya.cmds as mc
from lpRig import rigTools as lrr

sels = mc.ls(sl=True)

lrr.applyVertexMoveWithSkinWeight(
									baseObj=sels[0],
									srcObj=sels[1],
									tarObj=sels[3],
									skinGeo=sels[0],
									jnt=sels[2]
									)

# Remove selected vtcs from set.
setNm = 'sculpt1Set'

for sel in mc.ls(sl=True, fl=True):
	
	mc.sets(sel, rm=setNm)

# Reset shapes
origs = ['bodyOrig', 'ebOrig']
vtxNoToOrig = {}

for orig in origs:
	
	vtxNoToOrig[mc.polyEvaluate(orig, v=True)] = orig

for sel in mc.ls(sl=True):
	
	vtxNo = mc.polyEvaluate(sel, v=True)
	currOrig = vtxNoToOrig[vtxNo]
	
	mc.select(currOrig, r=True)
	mc.select(sel, add=True)
	
	cmd = 'copyShape;'
	mm.eval(cmd)

# go to rest pose of the skinned obj
# select skinned obj, blend shape obj, jnt, target obj
import maya.cmds as mc
from lpRig import rigTools as lrr

sels = mc.ls(sl=True)

lrr.applyVertexMoveWithSkinWeight(
									baseObj=sels[0],
									srcObj=sels[1],
									tarObj=sels[3],
									skinGeo=sels[0],
									jnt=sels[2]
									)

# Copy Eb rot shapes
startFrame = 115

shps = ['_RotDnInEbRig_L_Bsh', '_RotDnMidEbRig_L_Bsh' ,'_RotDnOutEbRig_L_Bsh',
		'_RotUpInEbRig_L_Bsh', '_RotUpMidEbRig_L_Bsh' ,'_RotUpOutEbRig_L_Bsh']

srcs = ['bodyRotSkin', 'ebRotSkin']
geos = ['BodyEbRig_Geo', 'EyebrowEbRig_Geo']

for idx, shp in enumerate(shps):
	
	mc.currentTime(startFrame + (idx*10))
	
	for geoIdx in range(len(geos)):
		
		currSrc = srcs[geoIdx]
		currGeo = '%s%s' % (geos[geoIdx], shp)
		
		mc.select(currSrc, r=True)
		mc.select(currGeo, add=True)
		
		cmd = 'copyShape;'
		mm.eval(cmd)
		
		mc.select(currGeo, r=True)
		mc.select(currGeo.replace('_L_', '_R_', add=True))
		
		cmd = 'mirrorShape2 "YZ" 0 0 1 0.001;'
		mm.eval(cmd)

# Auto divider
from lpRig import weightDivider
reload(weightDivider)
import time

startTime = time.time()
weightDivider.autoDivideCharacter(['BodyProxy_Geo'], 'Susan_Ctrl:')
print time.time() - startTime

# Copy component selection to the last selected object.
sels = mc.ls(sl=True)
tar = sels[-1]

newSels = []

for sel in sels[:-1]:
	
	currObj, currComp = sel.split('.')
	newSel = '%s.%s' % (tar, currComp)
	
	newSels.append(newSel)

mc.select(newSels, r=True)

# Find keyword in python files.
fld_path = r'X:\Lani Pixels\Libraries\Maya\mayaLpPython\lpRig'

py_files = [x for x in os.listdir(fld_path) if x.endswith('.py')]

keyword = 'enableRenderStatToSelected'

for py_file in py_files:
	
	curr_py_path = os.path.join(fld_path, py_file)
	
	with open(curr_py_path, 'r') as file_obj:
		
		for line in file_obj:
			if keyword in line:
				print '{keyword} has been found in {py_file}'.format(keyword=keyword, 
																		py_file=py_file)

print 'Checking keyword is done'

# Get left side shape from symetrical corrected shape.
from lpRig import applyLeftShape

orig = 'orig'
skinnedShpObj = 'Body_Geo'
per = 0.15

sculptedObj = mc.ls(sl=True)[0]

mc.select(sculptedObj, r=True)
mc.select(skinnedShpObj, add=True)

mm.eval('BSpiritCorrectiveShape')

correctedObj = mc.ls(sl=True)[0]

outObj = mc.duplicate(orig, rr=True)[0]

mc.select(correctedObj, r=True)
mc.select(outObj, add=True)

applyLeftShape.main(per, 1)

mc.delete(correctedObj)

mc.select(outObj, r=True)
cmds.showHidden(outObj)

# Get 40
orig = 'orig'
orignum = '80'
nums = ['60', '40']
pers = [0.5, 0.2]

srcObj = mc.ls(sl=True)[0]

for ix in range(len(nums)):

	tarNm = srcObj.replace(orignum, nums[ix])
	
	dupObj = mc.duplicate(srcObj, rr=True)[0]
	
	tarNm = mc.rename(dupObj, tarNm)
	
	mc.select(orig, r=True)
	mc.select(tarNm, add=True)
	
	lrr.doAddBlendShape(1)
	
	mc.delete(tarNm, ch=True)
	
	mc.select(srcObj, r=True)
	mc.select(tarNm, add=True)
	
	lrr.doAddBlendShape(pers[ix])
	
	mc.delete(tarNm, ch=True)
# Create point rig to selected joints.
from lpRig import pointRig

_parent = 'Root_Jnt'

sels = mc.ls(sl=True)
ctrlShp = 'cube'

for sel in sels:
	
	ndNm, ndIdx, ndSide, ndType = lrr.extractName(sel)
	rigObj = pointRig.PointRig(_parent, 'Ctrl_Grp', sel, ndNm, ndSide.replace('_', ''), ctrlShp)

# Duplicate right shapes
orig = 'orig'

for sel in mc.ls(sl=True):
	
	rgt = sel.replace('_L_', '_R_')
	
	if 'ZpAngle' in rgt:
		rgt = rgt.replace('ZpAngle', 'ZmAngle')
	elif 'ZmAngle' in rgt:
		rgt = rgt.replace('ZmAngle', 'ZpAngle')
	
	currObj = mc.duplicate(sel, rr=True)[0]
	mc.rename(currObj, rgt)
	
	mc.select(orig, r=True)
	mc.select(rgt, add=True)
	
	cmd = 'copyShape;'
	mm.eval(cmd)
	
	mc.select(sel, r=True)
	mc.select(rgt, add=True)
	
	cmd = 'mirrorShape2 "YZ" 0 0 1 0.001;'
	mm.eval(cmd)
	
	print sel


# Check geo
from publishCmds import CharacterRig
print CharacterRig.checkVertexCount()

# transfer UVs
sel = mc.ls(sl=True)[0]
srch = '_old'
rep = ''

#tar = sel.replace(srch, rep)

tar = mc.ls(sl=True)[1]

mc.select(tar, add=True)

cmd = 'transferAttributes -transferPositions 0 -transferNormals 0 -transferUVs 2 -transferColors 0 -sampleSpace 0 -sourceUvSpace "map1" -targetUvSpace "map1" -searchMethod 3-flipUVs 0 -colorBorders 1 ;'

mm.eval(cmd)

lrr.dupShade()

# mc.select(tar, r=True)

# smooth skin selected vtcs
import glTools.tools.smoothWeights as smoothWeights

sels = mc.ls(sl=True)
mm.eval('PolySelectConvert 3;')
expandVtcs = mc.ls(sl=True)
smoothWeights.smoothWeights(vtxList=expandVtcs, faceConnectivity=False, 
											showProgress=False, debug=False)

mc.select(sels, r=True)

# copy color
sels = mc.ls(sl=True)

col = mc.getAttr('%s.color' % sels[0])[0]

mc.setAttr('%s.color' % sels[1], col[0], col[1], col[2], type='double3')


# copy partial shape
# sels = mc.ls(sl=True)
# vtcs = mc.ls(sl=True)

for sel in sels:
	
	mc.select(vtcs, r=True)
	mc.select(sel, add=True)
	
	mm.eval('copyShape')

# Transfer uv
ns = 'Patrick_Shading:'

for sel in mc.ls(sl=True):
	
	src = '%s%s' % (ns, sel)
	
	mc.select(src, r=True)
	mc.select(sel, add=True)
	
	lrr.transferUvToOrig()

# Pull weight
from lpRig import weightPuller

#lastIdx = 14
src = 6
mx = 20

lastIdx += 1
if lastIdx > mx:
	lastIdx -= mx
weightPuller.pull(srch='_%s_' % str(src), rep='_%s_' % str(lastIdx), 
					mult=1, opr='mult')

# Using skin_tools to adjust skin weight values on ribbon joints.
from lpRig import skin_tools
reload(skin_tools)

# Select geo, first inf and second inf.
sels = mc.ls(sl=True)
skin_tools.unlock_selected_joints_from_selected_geos()
skin_tools.select_affected_vertices(sels[0], sels[1])

# Print out help text.
mdlNm = 'pointRig'

exec('from lpRig import %s' % mdlNm)
exec('print help(%s)' % mdlNm)

# Playblast pumpkin soldier
from lpRig import playblastTool
reload(playblastTool)

mc.select(cl=True)
mc.setAttr('Head_2_Geo.v', 0)
playblastTool.playImgSeq(suffix='_shade')
mc.setAttr('Head_2_Geo.v', 1)
playblastTool.playImgSeq(suffix='_checker')

# Assign trees tmp shade
sel = mc.ls(sl=True)[0]

leafMat = mc.shadingNode('lambert', asShader=True, n='LeafsTmp_Mat')
mc.setAttr('%s.color' % leafMat, 1.390806, 0.183318, 0.0487474)

trunkMat = mc.shadingNode('lambert', asShader=True, n='TrunksTmp_Mat')
mc.setAttr('%s.color' % trunkMat, 0.271393, 0.209473, 0.134049)

mc.select(sel, r=True)
mc.hyperShade(assign=trunkMat)

for geoShp in mc.ls(type='mesh'):
	geoXform = mc.listRelatives(geoShp, p=True)[0]
	
	if geoXform == sel:
		continue
	mc.select(geoXform, r=True)
	mc.hyperShade(assign=leafMat)

# Move all selected vertices to mid post
sels = mc.ls(sl=True, fl=True)

sel_no = len(sels)

mid_post = [0, 0, 0]

for sel in sels:
	
	curr_post = mc.xform(sel, q=True, t=True, ws=True)
	mid_post[0] = mid_post[0] + curr_post[0]/sel_no
	mid_post[1] = mid_post[1] + curr_post[1]/sel_no
	mid_post[2] = mid_post[2] + curr_post[2]/sel_no

mc.scale(0, 0, 0, r=True, p=mid_post)

# Copy shape attributes
import pymel.core as pmc

src, tar = pmc.ls(sl=True)

srcShp = pmc.listRelatives(src, s=True)[0]
tarShp = pmc.listRelatives(tar, s=True)[0]

udAttrs = [ x for x in pmc.listAttr(srcShp, ud=True)
			if not x.startswith('__')]

for udAttr in udAttrs:
	tarShp.attr(udAttr).set(srcShp.attr(udAttr).get())

# Pub check
import sys
sys.path.append(r'X:\Lani Pixels\Libraries\Maya\mayaLpPython')
from publishCmds import _Rig

_Rig.main()

# Select part geo then full geo.
# Script will select vertices on full geo that have
# the same positions as parteo
from lpRig import skin_tools
reload(skin_tools)
import time

sels = mc.ls(sl=True)

startTime = time.time()
mc.select(skin_tools.select_vertices_based_on_world_position(sels[0], sels[1], 0.1), r=True)

print time.time() - startTime

# Select shaped proxy shape, proxy skin then orig shape
px, pxskin, og = mc.ls(sl=True)

skin = mm.eval('findRelatedSkinCluster %s' % pxskin)
mc.setAttr('%s.envelope' % skin, 0)

tmppx = mc.duplicate(pxskin, rr=True)[0]
mc.setAttr('%s.envelope' % skin, 1)

mc.select([px, pxskin], r=True)
lrr.call_bspirit_corrective_shape()

px_absbsh = mc.ls(sl=True)[0]

tmpog = mc.duplicate(og, rr=True)[0]

mc.select([tmpog, tmppx], r=True)
mc.CreateWrap()

mc.select([px_absbsh, tmppx], r=True)
lrr.doAddBlendShape()

result = mc.duplicate(tmpog, rr=True)[0]

mc.delete([px_absbsh, tmppx, tmpog])

# Tmp wrap
# Select shaped proxy shapes, proxy orig then orig shape
sels = mc.ls(sl=True)

pxs = sels[:-2]
pxog, og = sels[-2:]

for px in pxs:
	tmppx = mc.duplicate(pxog, rr=True)[0]
	tmpog = mc.duplicate(og, rr=True)[0]
	
	mc.select([tmpog, tmppx], r=True)
	mc.CreateWrap()
	
	mc.select([px, tmppx], r=True)
	lrr.doAddBlendShape()
	
	result = mc.duplicate(tmpog, rr=True)[0]
	
	mc.delete([tmppx, tmpog])

# Find aim rotation using maya api
import math
from maya import OpenMaya as om

eyeAim = om.MVector.xAxis
eyeUp = om.MVector.yAxis

selList = om.MSelectionList()
selList.add('eye')
selList.add('target')

eyeDag = om.MDagPath()
targetDag = om.MDagPath()

selList.getDagPath(0, eyeDag)
selList.getDagPath(1, targetDag)

# Getting position of the eye and target.
transformFn = om.MFnTransform(eyeDag)

eyePivotPOS = transformFn.rotatePivot(om.MSpace.kWorld)
transformFn.setObject(targetDag)
targetPivotPOS = transformFn.rotatePivot(om.MSpace.kWorld)

# Getting aim vector
aimVector = targetPivotPOS - eyePivotPOS

# Getting all vectors
eyeU = aimVector.normal()

worldUp = om.MGlobal.upAxis() # Assuming that world up is world up vector.

eyeV = worldUp # This needs to comback to fix orthogonal issues.
eyeW = (eyeU ^ eyeV)

# Now, we make sure that the eyeV is orthogonal to
# both U and W by doing the cross product again.
eyeV = eyeW ^ eyeU

# Getting rotation.
quaternion = om.MQuaternion()

# Here, we're moving the eyeU of our object to match
# the aim vector we already calculated.
quaternionU = om.MQuaternion(eyeAim, eyeU)
quaternion = quaternionU
# From here, we are guaranteed that the object will
# already aim with the desired axis to our object.
# So the only thing left to do is to align the up.

# To set the object up
upRotated = eyeUp.rotateBy(quaternion)

# Getting the angle difference between the up rotated axis
# and the eyeV
angle = math.acos(upRotated*eyeV)

# Set quaternionV
quaternionV = om.MQuaternion(angle, eyeU)

# Now, we need to know if the rotatoin was in the correct
# dierction.
if not eyeV.isEquivalent(upRotated.rotateBy(quarternionV), 1.0e-5):
	angle = (2*math.pi) - angle
	quarternionV = om.MQuaternion(angle, eyeU)

quaternion *= quaternionV

transformFn.setObject(eyeDag)
transformFn.setRotation(quaternion)

# Transfer shading sets from object to another.
mc.transferShadingSets('source', 'target', spa=1, sm=3)


# Set all namespace of ref node to _Hi
import os

refs = pmc.ls(type='reference')

for ref in refs:
	
	if ref.parentReference():
		continue
	
	filename = os.path.normpath(ref.fileName(False, False, False))
	
	if 'Shot' in filename:
		continue
	
	pathtokens = filename.split(os.sep)
	assetname = pathtokens[-3]
		
	mc.file(filename, e=True, namespace='%s_Hi' % assetname)













