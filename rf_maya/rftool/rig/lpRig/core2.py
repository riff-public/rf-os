import pymel.core as pmc

def group(*args, **kwargs):
	return Dag(pmc.group(*args, **kwargs))

class Color():
	"""Indices of colors those are used in pkrig module.
	"""
	brown = 11
	green = 14
	dark_green = 7
	soft_yellow = 21
	yellow = 17
	soft_blue = 18
	blue = 6
	dark_blue = 15
	soft_gray = 3
	gray = 2
	black = 1
	white = 16
	soft_red = 31
	red = 13
	dark_red = 12
	none = None

class CurveParam():
	"""Parameters of one degree curve shapes those are used
	in pkrig module.
	"""
	cross = [(0, 0, -2.193), (0.852, 0, -1.267), (0.511, 0, -1.267), (0.511, 0, -0.633), 
		(0.633, 0, -0.511), (1.267, 0, -0.511), (1.267, 0, -0.852), (2.193, 0, 0), 
		(1.267, 0, 0.852), (1.267, 0, 0.511), (0.633, 0, 0.511), (0.511, 0, 0.633), 
		(0.511, 0, 1.267), (0.852, 0, 1.267), (0, 0, 2.193), (-0.852, 0, 1.267), 
		(-0.511, 0, 1.267), (-0.511, 0, 0.633), (-0.633, 0, 0.511), (-1.267, 0, 0.511), 
		(-1.267, 0, 0.852), (-2.193, 0, 0), (-1.267, 0, -0.852), (-1.267, 0, -0.511), 
		(-0.633, 0, -0.511), (-0.511, 0, -0.633), (-0.511, 0, -1.267), (-0.852, 0, -1.267), 
		(0, 0, -2.193)]
	plus = [(0, 1, 0), (0, -1, 0), (0, 0, 0), (-1, 0, 0), (1, 0, 0), (0, 0, 0), 
		(0, 0, -1), (0, 0, 1)]
	circle = [(1.125, 0, 0), (1.004, 0, 0), (0.992, 0, -0.157), (0.955, 0, -0.31), 
		(0.895, 0, -0.456), (0.812, 0, -0.59), (0.71, 0, -0.71), (0.59, 0, -0.812), 
		(0.456, 0, -0.895), (0.31, 0, -0.955), (0.157, 0, -0.992), (0, 0, -1.004), 
		(0, 0, -1.125), (0, 0, -1.004), (-0.157, 0, -0.992), (-0.31, 0, -0.955), 
		(-0.456, 0, -0.895), (-0.59, 0, -0.812), (-0.71, 0, -0.71), (-0.812, 0, -0.59), 
		(-0.895, 0, -0.456), (-0.955, 0, -0.31), (-0.992, 0, -0.157), (-1.004, 0, 0), 
		(-1.125, 0, 0), (-1.004, 0, 0), (-0.992, 0, 0.157), (-0.955, 0, 0.31), 
		(-0.895, 0, 0.456), (-0.812, 0, 0.59), (-0.71, 0, 0.71), (-0.59, 0, 0.812), 
		(-0.456, 0, 0.895), (-0.31, 0, 0.955), (-0.157, 0, 0.992), (0, 0, 1.004), 
		(0, 0, 1.125), (0, 0, 1.004), (0.157, 0, 0.992), (0.31, 0, 0.955), 
		(0.456, 0, 0.895), (0.59, 0, 0.812), (0.71, 0, 0.71), (0.812, 0, 0.59), 
		(0.895, 0, 0.456), (0.955, 0, 0.31), (0.992, 0, 0.157), (1.004, 0, 0)]
	cube = [(-1, 0, -1), (-1, 0, 1), (-1, 2, 1), (-1, 0, 1), (1, 0, 1), (1, 2, 1), 
		(1, 0, 1), (1, 0, -1), (1, 2, -1), (1, 0, -1), (-1, 0, -1), (-1, 2, -1), (1, 2, -1), 
		(1, 2, 1), (-1, 2, 1), (-1, 2, -1)]
	capsule = [(-2.011, 0, 0), (-1.977, 0.262, 0), (-1.876, 0.506, 0), (-1.715, 0.715, 0), 
		(-1.506, 0.876, 0), (-1.262, 0.977, 0), (-1, 1.011, 0), (1, 1.011, 0), 
		(1.262, 0.977, 0), (1.506, 0.876, 0), (1.715, 0.715, 0), (1.876, 0.506, 0), 
		(1.977, 0.262, 0), (2.011, 0, 0), (1.977, -0.262, 0), (1.876, -0.506, 0), 
		(1.715, -0.715, 0), (1.506, -0.876, 0), (1.262, -0.977, 0), (1, -1.011, 0), 
		(-1, -1.011, 0), (-1.262, -0.977, 0), (-1.506, -0.876, 0), (-1.715, -0.715, 0), 
		(-1.876, -0.506, 0), (-1.977, -0.262, 0), (-2.011, 0, 0)]
	stick = [(0, 0, 0), (0, 1.499, 0), (0.052, 1.502, 0), (0.104, 1.51, 0), (0.155, 1.524, 0), 
		(0.204, 1.542, 0), (0.25, 1.566, 0), (0.294, 1.595, 0), (0.335, 1.628, 0), 
		(0.372, 1.665, 0), (0.405, 1.706, 0), (0.434, 1.75, 0), (0.458, 1.796, 0), 
		(0.476, 1.845, 0), (0.49, 1.896, 0), (0.498, 1.948, 0), (0.501, 2, 0), 
		(0.498, 2.052, 0), (0.49, 2.104, 0), (0.476, 2.155, 0), (0.458, 2.204, 0), 
		(0.434, 2.25, 0), (0.405, 2.294, 0), (0.372, 2.335, 0), (0.335, 2.372, 0), 
		(0.294, 2.405, 0), (0.25, 2.434, 0), (0.204, 2.458, 0), (0.155, 2.476, 0), 
		(0.104, 2.49, 0), (0.052, 2.498, 0), (0, 2.501, 0), (-0.052, 2.498, 0), 
		(-0.104, 2.49, 0), (-0.155, 2.476, 0), (-0.204, 2.458, 0), (-0.25, 2.434, 0), 
		(-0.294, 2.405, 0), (-0.335, 2.372, 0), (-0.372, 2.335, 0), (-0.405, 2.294, 0), 
		(-0.434, 2.25, 0), (-0.458, 2.204, 0), (-0.476, 2.155, 0), (-0.49, 2.104, 0), 
		(-0.498, 2.052, 0), (-0.501, 2, 0), (-0.498, 1.948, 0), (-0.49, 1.896, 0), 
		(-0.476, 1.845, 0), (-0.458, 1.796, 0), (-0.434, 1.75, 0), (-0.405, 1.706, 0), 
		(-0.372, 1.665, 0), (-0.335, 1.628, 0), (-0.294, 1.595, 0), (-0.25, 1.566, 0), 
		(-0.204, 1.542, 0), (-0.155, 1.524, 0), (-0.104, 1.51, 0), (-0.052, 1.502, 0), 
		(0, 1.499, 0)]
	sphere = [(-1.5, 0, 0), (-1, 0, 0), (-0.966, 0.259, 0), (-0.866, 0.5, 0), 
		(-0.707, 0.707, 0), (-0.5, 0.866, 0), (-0.259, 0.966, 0), (0, 1, 0), 
		(0, 1.5, 0), (0, 1, 0), (0.259, 0.966, 0), (0.5, 0.866, 0), (0.707, 0.707, 0), 
		(0.866, 0.5, 0), (0.966, 0.259, 0), (1, 0, 0), (1.5, 0, 0), (1, 0, 0), 
		(0.966, -0.259, 0), (0.866, -0.5, 0), (0.707, -0.707, 0), (0.5, -0.866, 0), 
		(0.259, -0.966, 0), (0, -1, 0), (0, -1.5, 0), (0, -1, 0), (-0.259, -0.966, 0), 
		(-0.5, -0.866, 0), (-0.707, -0.707, 0), (-0.866, -0.5, 0), (-0.966, -0.259, 0), 
		(-1, 0, 0), (-0.951, 0, 0.309), (-0.809, 0, 0.588), (-0.588, 0, 0.809), 
		(-0.309, 0, 0.951), (-0.0, 0, 1), (0, 0, 1.5), (-0.0, 0, 1), (0.309, 0, 0.951), 
		(0.588, 0, 0.809), (0.809, 0, 0.588), (0.951, 0, 0.309), (1, 0, 0), 
		(0.951, 0, -0.309), (0.809, 0, -0.588), (0.588, 0, -0.809), (0.309, 0, -0.951), 
		(0, 0, -1), (0, 0, -1.5), (0, 0, -1), (0, 0.259, -0.966), (0, 0.5, -0.866), 
		(0, 0.707, -0.707), (0, 0.866, -0.5), (0, 0.966, -0.259), (0, 1, 0), 
		(-0.0, 0.966, 0.259), (-0.0, 0.866, 0.5), (-0.0, 0.707, 0.707), (-0.0, 0.5, 0.866), 
		(-0.0, 0.259, 0.966), (-0.0, 0, 1), (-0.0, -0.259, 0.966), (-0.0, -0.5, 0.866), 
		(-0.0, -0.707, 0.707), (-0.0, -0.866, 0.5), (-0.0, -0.966, 0.259), (0, -1, 0), 
		(0, -0.966, -0.259), (0, -0.866, -0.5), (0, -0.707, -0.707), (0, -0.5, -0.866), 
		(0, -0.259, -0.966), (0, 0, -1), (-0.309, 0, -0.951), (-0.588, 0, -0.809), 
		(-0.809, 0, -0.588), (-0.951, 0, -0.309), (-1, 0, 0)]
	square = [(-1, 0, 0), (-1, 2, 0), (1, 2, 0), (1, 0, 0), (-1, 0, 0)]
	none = [(0, 0, 0), (0, 0, 0)]

class Dg(object):
	"""Base class for all maya nodes in pkrig.
	"""
	def __init__(self, py_node):
		self.py_node = py_node
		self.py_node.add_pkr_attr()

	# Custom methods
	def add_pkr_attr():
		"""Adds pkrType attribute to current PyNode.
		"""
		if self.py_node.hasAttr(self.py_node.attr('pkrType')):
			return True

		self.py_node.addAttr(ln='pkrType', dt='string')
		return True

	# PyMel methods
	def __repr__(self):
		return self.py_node.__repr__()

	def __str__(self):
		return self.py_node.name()

	def addAttr(self, *args, **kwargs):
		return self.py_node.addAttr(*args, **kwargs)

	def addAttribute(self, *args, **kwargs):
		return self.py_node.addAttribute(*args, **kwargs)

	def attr(self, *args, **kwargs):
		return self.py_node.attr(*args, **kwargs)

	def attrInfo(self, *args, **kwargs):
		return self.py_node.attrInfo(*args, **kwargs)

	def attribute(self, *args, **kwargs):
		return self.py_node.attribute(*args, **kwargs)

	def connectAttr(self, *args, **kwargs):
		return self.py_node.connectAttr(*args, **kwargs)

	def connections(self, *args, **kwargs):
		return self.py_node.connections(*args, **kwargs)

	def deleteAttr(self, *args, **kwargs):
		return self.py_node.deleteAttr(*args, **kwargs)

	def exists(self, *args, **kwargs):
		return self.py_node.exists(*args, **kwargs)

	def getAttr(self, *args, **kwargs):
		return self.py_node.getAttr(*args, **kwargs)

	def getConnections(self, *args, **kwargs):
		return self.py_node.getConnections(*args, **kwargs)

	def getName(self, *args, **kwargs):
		return self.py_node.getName(*args, **kwargs)

	def hasAttr(self, *args, **kwargs):
		return self.py_node.hasAttr(*args, **kwargs)

	def hasAttribute(self, *args, **kwargs):
		return self.py_node.hasAttribute(*args, **kwargs)

	def isDefaultNode(self, *args, **kwargs):
		return self.py_node.isDefaultNode(*args, **kwargs)

	def isFromReferencedFile(self, *args, **kwargs):
		return self.py_node.isFromReferencedFile(*args, **kwargs)

	def isLocked(self, *args, **kwargs):
		return self.py_node.isLocked(*args, **kwargs)

	def isReadOnly(self, *args, **kwargs):
		return self.py_node.isReadOnly(*args, **kwargs)

	def isReferenced(self, *args, **kwargs):
		return self.py_node.isReferenced(*args, **kwargs)

	def isUniquelyNamed(self, *args, **kwargs):
		return self.py_node.isUniquelyNamed(*args, **kwargs)

	def listAliases(self, *args, **kwargs):
		return self.py_node.listAliases(*args, **kwargs)

	def listAnimatable(self, *args, **kwargs):
		return self.py_node.listAnimatable(*args, **kwargs)

	def listAttr(self, *args, **kwargs):
		return self.py_node.listAttr(*args, **kwargs)

	def listConnections(self, *args, **kwargs):
		return self.py_node.listConnections(*args, **kwargs)

	def listFuture(self, *args, **kwargs):
		return self.py_node.listFuture(*args, **kwargs)

	def listHistory(self, *args, **kwargs):
		return self.py_node.listHistory(*args, **kwargs)

	def lock(self, *args, **kwargs):
		return self.py_node.lock(*args, **kwargs)

	def longName(self, *args, **kwargs):
		return self.py_node.longName(*args, **kwargs)

	def name(self, *args, **kwargs):
		return self.py_node.name(*args, **kwargs)

	def namespace(self, *args, **kwargs):
		return self.py_node.namespace(*args, **kwargs)

	def nodeType(self, *args, **kwargs):
		return self.py_node.nodeType(*args, **kwargs)

	def objExists(self, *args, **kwargs):
		return self.py_node.objExists(*args, **kwargs)

	def parentNamespace(self, *args, **kwargs):
		return self.py_node.parentNamespace(*args, **kwargs)

	def referenceFile(self, *args, **kwargs):
		return self.py_node.referenceFile(*args, **kwargs)

	def removeAttribute(self, *args, **kwargs):
		return self.py_node.removeAttribute(*args, **kwargs)

	def rename(self, *args, **kwargs):
		return self.py_node.rename(*args, **kwargs)

	def setAttr(self, *args, **kwargs):
		return self.py_node.setAttr(*args, **kwargs)

	def setLocked(self, *args, **kwargs):
		return self.py_node.setLocked(*args, **kwargs)

	def setName(self, *args, **kwargs):
		return self.py_node.setName(*args, **kwargs)

	def type(self, *args, **kwargs):
		return self.py_node.type(*args, **kwargs)

	def typeName(self, *args, **kwargs):
		return self.py_node.typeName(*args, **kwargs)

	def unlock(self, *args, **kwargs):
		return self.py_node.unlock(*args, **kwargs)

class Dag(Dg):
	"""Base class for all DAG objects in pkrig.
	"""
	def __init__(self, py_node):
		super(Dag, self).__init__(py_node)

	# Custom methods
	def create_curve(self, curve_type):
		"""Creates a curve shape to py_node.
		"""
		curve = pmc.curve(d=1, p=curve_type)
		shape = curve.getShape()
		pmc.parent(shape, self.py_node, s=True, r=True)
		pmc.delete(curve)
		shape.rename('%sShape' % self.py_node.name())

	def set_color(self, color_index):
		"""Sets overrideColor attribute.
		"""
		shape = self.py_node.getShape()
		if color_index:
			shape.attr('overrideEnabled').set(True)
			shape.attr('overrideColor').set(color_index)
		else:
			shape.attr('overrideColor').set(0)
			shape.attr('overrideEnabled').set(False)

	def metalink(self):
		"""Creates pkrType attr and makes a connection to meta node.
		"""
		parents = self.getAllParents()

		if not parents:
			print '%s has no parent node.' % self.py_node.name()
			return False

		meta

	# PyMel methods
	def activeColor(self, *args, **kwargs):
		return self.py_node.activeColor(*args, **kwargs)

	def addChild(self, *args, **kwargs):
		return self.py_node.addChild(*args, **kwargs)

	def boundingBox(self, *args, **kwargs):
		return self.py_node.boundingBox(*args, **kwargs)

	def center(self, *args, **kwargs):
		return self.py_node.center(*args, **kwargs)

	def centerPivots(self, *args, **kwargs):
		return self.py_node.centerPivots(*args, **kwargs)

	def centerPivotsOnComponents(self, *args, **kwargs):
		return self.py_node.centerPivotsOnComponents(*args, **kwargs)

	def childAtIndex(self, *args, **kwargs):
		return self.py_node.childAtIndex(*args, **kwargs)

	def drawOverrideColor(self, *args, **kwargs):
		return self.py_node.drawOverrideColor(*args, **kwargs)

	def drawOverrideEnabled(self, *args, **kwargs):
		return self.py_node.drawOverrideEnabled(*args, **kwargs)

	def firstParent(self, *args, **kwargs):
		return self.py_node.firstParent(*args, **kwargs)

	def firstParent2(self, *args, **kwargs):
		return self.py_node.firstParent2(*args, **kwargs)

	def fullPath(self, *args, **kwargs):
		return self.py_node.fullPath(*args, **kwargs)

	def getAllParents(self, *args, **kwargs):
		return self.py_node.getAllParents(*args, **kwargs)

	def getBoundingBox(self, *args, **kwargs):
		return self.py_node.getBoundingBox(*args, **kwargs)

	def getBoundingBoxInvisible(self, *args, **kwargs):
		return self.py_node.getBoundingBoxInvisible(*args, **kwargs)

	def getBoundingBoxMax(self, *args, **kwargs):
		return self.py_node.getBoundingBoxMax(*args, **kwargs)

	def getBoundingBoxMin(self, *args, **kwargs):
		return self.py_node.getBoundingBoxMin(*args, **kwargs)

	def getChildren(self, *args, **kwargs):
		return self.py_node.getChildren(*args, **kwargs)

	def getLimit(self, *args, **kwargs):
		return self.py_node.getLimit(*args, **kwargs)

	def getMatrix(self, *args, **kwargs):
		return self.py_node.getMatrix(*args, **kwargs)

	def getObjectColor(self, *args, **kwargs):
		return self.py_node.getObjectColor(*args, **kwargs)

	def getObjectColorType(self, *args, **kwargs):
		return self.py_node.getObjectColorType(*args, **kwargs)

	def getParent(self, *args, **kwargs):
		return self.py_node.getParent(*args, **kwargs)

	def getPivots(self, *args, **kwargs):
		return self.py_node.getPivots(*args, **kwargs)

	def getRestPosition(self, *args, **kwargs):
		return self.py_node.getRestPosition(*args, **kwargs)

	def getRootTransform(self, *args, **kwargs):
		return self.py_node.getRootTransform(*args, **kwargs)

	def getRotateAxis(self, *args, **kwargs):
		return self.py_node.getRotateAxis(*args, **kwargs)

	def getRotatePivot(self, *args, **kwargs):
		return self.py_node.getRotatePivot(*args, **kwargs)

	def getRotatePivotTranslation(self, *args, **kwargs):
		return self.py_node.getRotatePivotTranslation(*args, **kwargs)

	def getRotation(self, *args, **kwargs):
		return self.py_node.getRotation(*args, **kwargs)

	def getRotationOrder(self, *args, **kwargs):
		return self.py_node.getRotationOrder(*args, **kwargs)

	def getScale(self, *args, **kwargs):
		return self.py_node.getScale(*args, **kwargs)

	def getScalePivot(self, *args, **kwargs):
		return self.py_node.getScalePivot(*args, **kwargs)

	def getScalePivotTranslation(self, *args, **kwargs):
		return self.py_node.getScalePivotTranslation(*args, **kwargs)

	def getShape(self, *args, **kwargs):
		return self.py_node.getShape(*args, **kwargs)

	def getShapes(self, *args, **kwargs):
		return self.py_node.getShapes(*args, **kwargs)

	def getSiblings(self, *args, **kwargs):
		return self.py_node.getSiblings(*args, **kwargs)

	def getTransformation(self, *args, **kwargs):
		return self.py_node.getTransformation(*args, **kwargs)

	def getTranslation(self, *args, **kwargs):
		return self.py_node.getTranslation(*args, **kwargs)

	def hasChild(self, *args, **kwargs):
		return self.py_node.hasChild(*args, **kwargs)

	def hide(self, *args, **kwargs):
		return self.py_node.hide(*args, **kwargs)

	def history(self, *args, **kwargs):
		return self.py_node.history(*args, **kwargs)

	def isIntermediate(self, *args, **kwargs):
		return self.py_node.isIntermediate(*args, **kwargs)

	def isParentOf(self, *args, **kwargs):
		return self.py_node.isParentOf(*args, **kwargs)

	def listRelatives(self, *args, **kwargs):
		return self.py_node.listRelatives(*args, **kwargs)

	def setIntermediate(self, *args, **kwargs):
		return self.py_node.setIntermediate(*args, **kwargs)

	def setLocked(self, *args, **kwargs):
		return self.py_node.setLocked(*args, **kwargs)

	def setMatrix(self, *args, **kwargs):
		return self.py_node.setMatrix(*args, **kwargs)

	def setParent(self, *args, **kwargs):
		return self.py_node.setParent(*args, **kwargs)

	def setPivots(self, *args, **kwargs):
		return self.py_node.setPivots(*args, **kwargs)

	def setRestPosition(self, *args, **kwargs):
		return self.py_node.setRestPosition(*args, **kwargs)

	def setRotateAxis(self, *args, **kwargs):
		return self.py_node.setRotateAxis(*args, **kwargs)

	def setRotatePivot(self, *args, **kwargs):
		return self.py_node.setRotatePivot(*args, **kwargs)

	def setRotatePivotTranslation(self, *args, **kwargs):
		return self.py_node.setRotatePivotTranslation(*args, **kwargs)

	def setRotation(self, *args, **kwargs):
		return self.py_node.setRotation(*args, **kwargs)

	def setRotationOrder(self, *args, **kwargs):
		return self.py_node.setRotationOrder(*args, **kwargs)

	def setScale(self, *args, **kwargs):
		return self.py_node.setScale(*args, **kwargs)

	def setScalePivot(self, *args, **kwargs):
		return self.py_node.setScalePivot(*args, **kwargs)

	def setScalePivotTranslation(self, *args, **kwargs):
		return self.py_node.setScalePivotTranslation(*args, **kwargs)

	def setTransformation(self, *args, **kwargs):
		return self.py_node.setTransformation(*args, **kwargs)

	def setTranslation(self, *args, **kwargs):
		return self.py_node.setTranslation(*args, **kwargs)

	def shortName(self, *args, **kwargs):
		return self.py_node.shortName(*args, **kwargs)

	def show(self, *args, **kwargs):
		return self.py_node.show(*args, **kwargs)

	def sources(self, *args, **kwargs):
		return self.py_node.sources(*args, **kwargs)

	def transformationMatrix(self, *args, **kwargs):
		return self.py_node.transformationMatrix(*args, **kwargs)

	def ungroup(self, *args, **kwargs):
		return self.py_node.ungroup(*args, **kwargs)

	def unlock(self, *args, **kwargs):
		return self.py_node.unlock(*args, **kwargs)

	def zeroTransformPivots(self, *args, **kwargs):
		return self.py_node.zeroTransformPivots(*args, **kwargs)

class Transform(Dag):

	def __init__(self):
		super(Transform, self).__init__(pmc.createNode('transform'))
