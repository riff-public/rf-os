import maya.cmds as mc

import lpRig.core as lrc
reload(lrc)
import lpRig.rigTools as lrr
reload(lrr)

class TtRigLite(object):
	
	def __init__(
					self,
					jawPost='',
					mainUpTthTmpLoc='',
					mainLowTthTmpLoc='',
					tngTmpLocs=[],
					part='',
					offsetCurve=0.1
				):
		
		# Main Groups
		self.Ctrl_Grp = lrc.Null()
		self.Ctrl_Grp.name = 'TtRig%sCtrl_Grp' % part

		self.Jnt_Grp = lrc.Null()
		self.Jnt_Grp.name = 'TtRig%sJnt_Grp' % part

		self.Skin_Grp = lrc.Null()
		self.Skin_Grp.name = 'TtRig%sSkin_Grp' % part

		self.Still_Grp = lrc.Null()
		self.Still_Grp.name = 'TtRig%sStill_Grp' % part

		# Jaw Post
		self.CtrlJawPost_Grp = lrc.Null()
		self.CtrlJawPost_Grp.name = 'TtRig%sCtrlJawPost_Grp' % part

		self.CtrlJawPostZr_Grp = lrc.group(self.CtrlJawPost_Grp )
		self.CtrlJawPostZr_Grp.name = 'TtRig%sCtrlJawPostZr_Grp' % part

		self.CtrlJawPostZr_Grp.snap(jawPost)
		self.CtrlJawPostZr_Grp.parent(self.Ctrl_Grp)

		self.JntJawPost_Grp = lrc.Null()
		self.JntJawPost_Grp.name = 'TtRig%sJntJawPost_Grp' % part

		self.JntJawPostZr_Grp = lrc.group(self.JntJawPost_Grp )
		self.JntJawPostZr_Grp.name = 'TtRig%sJntJawPostZr_Grp' % part

		self.JntJawPostZr_Grp.snap(jawPost)
		self.JntJawPostZr_Grp.parent(self.Jnt_Grp)

		self.SkinJawPost_Grp = lrc.Null()
		self.SkinJawPost_Grp.name = 'TtRig%sSkinJawPost_Grp' % part

		self.SkinJawPostZr_Grp = lrc.group(self.SkinJawPost_Grp )
		self.SkinJawPostZr_Grp.name = 'TtRig%sSkinJawPostZr_Grp' % part

		self.SkinJawPostZr_Grp.snap(jawPost)
		self.SkinJawPostZr_Grp.parent(self.Skin_Grp)
		
		for attr in ('t', 'r', 's'):
			self.CtrlJawPost_Grp.attr(attr) >> self.JntJawPost_Grp.attr(attr)
			self.CtrlJawPost_Grp.attr(attr) >> self.SkinJawPost_Grp.attr(attr)

		# Up Teeth
		self.UpTeeth = TeethRigLite(
										mainTmpLoc='MainUpTthTmp_Loc',
										part='UpTth%sRig' % part
									)

		self.UpTeeth.Ctrl_Grp.parent(self.Ctrl_Grp)
		self.UpTeeth.Skin_Grp.parent(self.Skin_Grp)

		# Low Teeth
		self.LowTeeth = TeethRigLite(
										mainTmpLoc='MainLowTthTmp_Loc',
										part='LowTth%sRig' % part
									)

		self.LowTeeth.Ctrl_Grp.parent(self.CtrlJawPost_Grp)
		self.LowTeeth.Skin_Grp.parent(self.SkinJawPost_Grp)

		# Tongue
		self.Tng = TongueRig(
									tmpLocs=[
												'TongueTmp_1_Jnt',
												'TongueTmp_2_Jnt',
												'TongueTmp_3_Jnt',
												'TongueTmp_4_Jnt',
												'TongueTmp_5_Jnt',
												'TongueTmp_6_Jnt'
											],
									part='Tng%sRig' % part
								)

		self.Tng.Ctrl_Grp.parent(self.CtrlJawPost_Grp)
		self.Tng.Skin_Grp.parent(self.SkinJawPost_Grp)

class TtRig(object):

	def __init__(
					self,
					jawPost='',
					mainUpTthTmpLoc='',
					cntUpTthTmpLoc='',
					lftUpTthTmpLoc='',
					rgtUpTthTmpLoc='',
					lftMidUpTthTmpLoc='',
					rgtMidUpTthTmpLoc='',
					mainLowTthTmpLoc='',
					cntLowTthTmpLoc='',
					lftLowTthTmpLoc='',
					rgtLowTthTmpLoc='',
					lftMidLowTthTmpLoc='',
					rgtMidLowTthTmpLoc='',
					tngTmpLocs=[],
					part='',
					offsetCurve=0.1
				):

		# Main Groups
		self.Ctrl_Grp = lrc.Null()
		self.Ctrl_Grp.name = 'TtRig%sCtrl_Grp' % part

		self.Jnt_Grp = lrc.Null()
		self.Jnt_Grp.name = 'TtRig%sJnt_Grp' % part

		self.Skin_Grp = lrc.Null()
		self.Skin_Grp.name = 'TtRig%sSkin_Grp' % part

		self.Still_Grp = lrc.Null()
		self.Still_Grp.name = 'TtRig%sStill_Grp' % part

		# Jaw Post
		self.CtrlJawPost_Grp = lrc.Null()
		self.CtrlJawPost_Grp.name = 'TtRig%sCtrlJawPost_Grp' % part

		self.CtrlJawPostZr_Grp = lrc.group(self.CtrlJawPost_Grp )
		self.CtrlJawPostZr_Grp.name = 'TtRig%sCtrlJawPostZr_Grp' % part

		self.CtrlJawPostZr_Grp.snap(jawPost)
		self.CtrlJawPostZr_Grp.parent(self.Ctrl_Grp)

		self.JntJawPost_Grp = lrc.Null()
		self.JntJawPost_Grp.name = 'TtRig%sJntJawPost_Grp' % part

		self.JntJawPostZr_Grp = lrc.group(self.JntJawPost_Grp )
		self.JntJawPostZr_Grp.name = 'TtRig%sJntJawPostZr_Grp' % part

		self.JntJawPostZr_Grp.snap(jawPost)
		self.JntJawPostZr_Grp.parent(self.Jnt_Grp)

		self.SkinJawPost_Grp = lrc.Null()
		self.SkinJawPost_Grp.name = 'TtRig%sSkinJawPost_Grp' % part

		self.SkinJawPostZr_Grp = lrc.group(self.SkinJawPost_Grp )
		self.SkinJawPostZr_Grp.name = 'TtRig%sSkinJawPostZr_Grp' % part

		self.SkinJawPostZr_Grp.snap(jawPost)
		self.SkinJawPostZr_Grp.parent(self.Skin_Grp)
		
		for attr in ('t', 'r', 's'):
			self.CtrlJawPost_Grp.attr(attr) >> self.JntJawPost_Grp.attr(attr)
			self.CtrlJawPost_Grp.attr(attr) >> self.SkinJawPost_Grp.attr(attr)

		# Up Teeth
		self.UpTeeth = TeethRig(
									mainTmpLoc='MainUpTthTmp_Loc',
									cntTmpLoc='CntUpTthTmp_Loc',
									lftTmpLoc='InUpTthTmp_L_Loc',
									rgtTmpLoc='InUpTthTmp_R_Loc',
									lftMidTmpLoc='MidUpTthTmp_L_Loc',
									rgtMidTmpLoc='MidUpTthTmp_R_Loc',
									part='UpTth%sRig' % part,
									offsetCurve=offsetCurve
								)

		self.UpTeeth.Ctrl_Grp.parent(self.Ctrl_Grp)
		self.UpTeeth.Jnt_Grp.parent(self.Jnt_Grp)
		self.UpTeeth.Skin_Grp.parent(self.Skin_Grp)
		self.UpTeeth.Still_Grp.parent(self.Still_Grp)

		# Low Teeth
		self.LowTeeth = TeethRig(
									mainTmpLoc='MainLowTthTmp_Loc',
									cntTmpLoc='CntLowTthTmp_Loc',
									lftTmpLoc='InLowTthTmp_L_Loc',
									rgtTmpLoc='InLowTthTmp_R_Loc',
									lftMidTmpLoc='MidLowTthTmp_L_Loc',
									rgtMidTmpLoc='MidLowTthTmp_R_Loc',
									part='LowTth%sRig' % part,
									offsetCurve=offsetCurve
								)

		self.LowTeeth.Ctrl_Grp.parent(self.CtrlJawPost_Grp)
		self.LowTeeth.Jnt_Grp.parent(self.JntJawPost_Grp)
		self.LowTeeth.Skin_Grp.parent(self.Skin_Grp)
		self.LowTeeth.Still_Grp.parent(self.Still_Grp)

		# Tongue
		self.Tng = TongueRig2(
									tmpLocs=[
												'TongueTmp_1_Jnt',
												'TongueTmp_2_Jnt',
												'TongueTmp_3_Jnt',
												'TongueTmp_4_Jnt',
												'TongueTmp_5_Jnt',
												'TongueTmp_6_Jnt'
											],
									part='Tng%sRig' % part
								)

		self.Tng.Ctrl_Grp.parent(self.CtrlJawPost_Grp)
		self.Tng.Skin_Grp.parent(self.SkinJawPost_Grp)

class TongueRig(object):

	def __init__(
					self,
					tmpLocs=[],
					part=''
				):

		# Main Groups
		self.Ctrl_Grp = lrc.Null()
		self.Ctrl_Grp.name = 'Ctrl%s_Grp' % part

		self.Skin_Grp = lrc.Null()
		self.Skin_Grp.name = 'Skin%s_Grp' % part
				
		self.Jnts = []
		self.JntZrGrps = []
		self.Ctrls = []
		self.CtrlZrGrps = []

		for ix, tmpLoc in enumerate(tmpLocs):

			idx = ix + 1

			jnt = lrc.Joint()
			jnt.name = '{part}_{idx}_Jnt'.format(part=part, idx=idx)

			jntZr = lrc.group(jnt)
			jntZr.name = '{part}JntZr_{idx}_Grp'.format(part=part, idx=idx)

			jntZr.snap(tmpLoc)

			ctrl = lrc.Control('teethShape')
			ctrl.color = 'softYellow'
			ctrl.lockHideAttrs('v')
			ctrl.name = '{part}_{idx}_Ctrl'.format(part=part, idx=idx)

			ctrlZr = lrc.group(ctrl)
			ctrlZr.name = '{part}CtrlZr_{idx}_Grp'.format(part=part, idx=idx)

			ctrlZr.snap(tmpLoc)

			for attr in ('t', 'r', 's'):
				ctrl.attr(attr) >> jnt.attr(attr)

			self.Jnts.append(jnt)
			self.JntZrGrps.append(jntZr)
			self.Ctrls.append(ctrl)
			self.CtrlZrGrps.append(ctrlZr)

			if not ix: continue

			jntZr.parent(self.Jnts[ix-1])
			ctrlZr.parent(self.Ctrls[ix-1])

		self.CtrlZrGrps[0].parent(self.Ctrl_Grp)
		self.JntZrGrps[0].parent(self.Skin_Grp)

class TongueRig2(object):

	def __init__(
					self,
					tmpLocs=[],
					part=''
				):

		# Main Groups
		self.Ctrl_Grp = lrc.Null()
		self.Ctrl_Grp.name = 'Ctrl%s_Grp' % part

		self.Skin_Grp = lrc.Null()
		self.Skin_Grp.name = 'Skin%s_Grp' % part
				
		self.Jnts = []
		self.JntZrGrps = []
		self.Ctrls = []
		self.CtrlZrGrps = []

		for ix, tmpLoc in enumerate(tmpLocs):

			idx = ix + 1

			jnt = lrc.Joint()
			jnt.name = '{part}_{idx}_Jnt'.format(part=part, idx=idx)

			jntSca = lrc.Joint()
			jntSca.name = '{part}Sca_{idx}_Jnt'.format(part=part, idx=idx)
			jntSca.parent(jnt)

			jntZr = lrc.group(jnt)
			jntZr.name = '{part}JntZr_{idx}_Grp'.format(part=part, idx=idx)

			jntZr.snap(tmpLoc)

			ctrl = lrc.Control('nrbCircle')
			ctrl.color = 'softYellow'
			ctrl.lockHideAttrs('v')
			ctrl.name = '{part}_{idx}_Ctrl'.format(part=part, idx=idx)

			ctrlZr = lrc.group(ctrl)
			ctrlZr.name = '{part}CtrlZr_{idx}_Grp'.format(part=part, idx=idx)

			ctrlZr.snap(tmpLoc)

			for attr in ('t', 'r'):
				ctrl.attr(attr) >> jnt.attr(attr)

			for attr in ('sx', 'sy', 'sz'):
				ctrl.attr(attr) >> jntSca.attr(attr)

			ctrl.lockHideAttrs('sy')

			self.Jnts.append(jnt)
			self.JntZrGrps.append(jntZr)
			self.Ctrls.append(ctrl)
			self.CtrlZrGrps.append(ctrlZr)

			if not ix: continue

			jntZr.parent(self.Jnts[ix-1])
			# ctrlZr.parent(self.Ctrls[ix-1])

		for i in range(len(self.CtrlZrGrps)):

			if i != 0:

				mc.parentConstraint(self.Ctrls[i-1],self.CtrlZrGrps[i],mo = True)

		for a in range(len(self.CtrlZrGrps)):

			self.CtrlZrGrps[a].parent(self.Ctrl_Grp)
			
		self.JntZrGrps[0].parent(self.Skin_Grp)


class TeethRigLite(object):

	def __init__(
					self,
					mainTmpLoc='',
					part='',
				):

		# Tmp Locs
		mainLoc = lrc.Dag(mainTmpLoc)

		# Main Groups
		self.Ctrl_Grp = lrc.Null()
		self.Ctrl_Grp.name = 'Ctrl%s_Grp' % part

		self.Skin_Grp = lrc.Null()
		self.Skin_Grp.name = 'Skin%s_Grp' % part

		# Main Jnts
		self.MainJnt_Grp = lrc.Null()
		self.MainJnt_Grp.name = 'MainJnt%s_Grp' % part
		self.MainJntZr_Grp = lrc.group(self.MainJnt_Grp)
		self.MainJntZr_Grp.name = 'MainJnt%sZr_Grp' % part
		self.MainJntZr_Grp.snap(mainLoc)
		self.MainJntZr_Grp.parent(self.Skin_Grp)

		self.Cnt_Jnt = lrc.Joint()
		self.Cnt_Jnt.name = 'Cnt%s_Jnt' % part

		self.CntJntZr_Grp = lrc.group(self.Cnt_Jnt)
		self.CntJntZr_Grp.name = 'CntJnt%sZr_Grp' % part
		self.CntJntZr_Grp.snap(mainLoc)
		self.CntJntZr_Grp.parent(self.MainJnt_Grp)

		# Main Controllers
		self.Main_Ctrl = lrc.Control('nrbCircle')
		self.Main_Ctrl.name = 'Main%s_Ctrl' % part
		self.Main_Ctrl.lockHideAttrs('v')
		self.Main_Ctrl.color = 'yellow'

		self.MainCtrlZr_Grp = lrc.group(self.Main_Ctrl)
		self.MainCtrlZr_Grp.name = 'MainCtrlZr%s_Grp' % part
		self.MainCtrlZr_Grp.snap(mainLoc)
		self.MainCtrlZr_Grp.parent(self.Ctrl_Grp)

		self.Main_Ctrl.attr('t') >> self.MainJnt_Grp.attr('t')
		self.Main_Ctrl.attr('r') >> self.MainJnt_Grp.attr('r')
		self.Main_Ctrl.attr('s') >> self.MainJnt_Grp.attr('s')

class TeethRig(object):

	def __init__(
					self,
					mainTmpLoc='',
					cntTmpLoc='',
					lftTmpLoc='',
					rgtTmpLoc='',
					lftMidTmpLoc='',
					rgtMidTmpLoc='',
					part='',
					offsetCurve=0.1
				):
		
		# Tmp Locs
		mainLoc = lrc.Dag(mainTmpLoc)
		cntLoc = lrc.Dag(cntTmpLoc)
		lftLoc = lrc.Dag(lftTmpLoc)
		rgtLoc = lrc.Dag(rgtTmpLoc)
		lftMidLoc = lrc.Dag(lftMidTmpLoc)
		rgtMidLoc = lrc.Dag(rgtMidTmpLoc)

		# Main Groups
		self.Ctrl_Grp = lrc.Null()
		self.Ctrl_Grp.name = 'Ctrl%s_Grp' % part

		self.Jnt_Grp = lrc.Null()
		self.Jnt_Grp.name = 'Jnt%s_Grp' % part

		self.Skin_Grp = lrc.Null()
		self.Skin_Grp.name = 'Skin%s_Grp' % part

		self.Still_Grp = lrc.Null()
		self.Still_Grp.name = 'Still%s_Grp' % part
		
		# Main Jnts
		self.MainJnt_Grp = lrc.Null()
		self.MainJnt_Grp.name = 'MainJnt%s_Grp' % part
		self.MainJntZr_Grp = lrc.group(self.MainJnt_Grp)
		self.MainJntZr_Grp.name = 'MainJnt%sZr_Grp' % part
		self.MainJntZr_Grp.snap(mainLoc)
		self.MainJntZr_Grp.parent(self.Jnt_Grp)

		self.Cnt_Jnt, self.CntJntZr_Grp = self.createMainJoint('Cnt', part, '_', cntLoc)
		self.In_L_Jnt, self.InJntZr_L_Grp = self.createMainJoint('In', part, '_L_', lftLoc)
		self.In_R_Jnt, self.InJntZr_R_Grp = self.createMainJoint('In', part, '_R_', rgtLoc)
		self.Mid_L_Jnt, self.MidJntZr_L_Grp = self.createMainJoint('Mid', part, '_L_', lftMidLoc)
		self.Mid_R_Jnt, self.MidJntZr_R_Grp = self.createMainJoint('Mid', part, '_R_', rgtMidLoc)

		# Main Controllers
		self.Main_Ctrl = lrc.Control('nrbCircle')
		self.Main_Ctrl.name = 'Main%s_Ctrl' % part
		self.Main_Ctrl.lockHideAttrs('v')
		self.Main_Ctrl.color = 'yellow'

		self.MainCtrlZr_Grp = lrc.group(self.Main_Ctrl)
		self.MainCtrlZr_Grp.name = 'MainCtrlZr%s_Grp' % part
		self.MainCtrlZr_Grp.snap(mainLoc)
		self.MainCtrlZr_Grp.parent(self.Ctrl_Grp)

		self.Main_Ctrl.attr('t') >> self.MainJnt_Grp.attr('t')
		self.Main_Ctrl.attr('r') >> self.MainJnt_Grp.attr('r')
		self.Main_Ctrl.attr('s') >> self.MainJnt_Grp.attr('s')

		self.Cnt_Ctrl, self.CntCtrlZr_Grp = self.createMainControl('Cnt', part, '_', self.Cnt_Jnt)
		self.In_L_Ctrl, self.InCtrlZr_L_Grp = self.createMainControl('In', part, '_L_', self.In_L_Jnt)
		self.In_R_Ctrl, self.InCtrlZr_R_Grp = self.createMainControl('In', part, '_R_', self.In_R_Jnt)
		self.Mid_L_Ctrl, self.MidCtrlZr_L_Grp = self.createMainControl('Mid', part, '_L_', self.Mid_L_Jnt)
		self.Mid_R_Ctrl, self.MidCtrlZr_R_Grp = self.createMainControl('Mid', part, '_R_', self.Mid_R_Jnt)

		# Ribbon Surface
		tmpCrvs = []
		for loc in (lftLoc, lftMidLoc, cntLoc, rgtMidLoc, rgtLoc):
			tmpCrv = lrc.curve(d=1, p=[(0, 0, -offsetCurve), (0, 0, offsetCurve)])
			tmpCrv.snap(loc)
			tmpCrvs.append(tmpCrv)

		self.Rbn_Nrb = lrc.Dag(mc.loft(tmpCrvs, ch=False, d=3)[0])
		self.Rbn_Nrb.name = 'Rbn%s_Nrb' % part
		self.Rbn_Nrb.parent(self.Still_Grp)

		mc.delete(tmpCrvs)

		# Binding skin
		skc = mc.skinCluster(
								self.Cnt_Jnt.name,
								self.In_L_Jnt.name,
								self.In_R_Jnt.name,
								self.Mid_L_Jnt.name,
								self.Mid_R_Jnt.name,
								self.Rbn_Nrb,
								dr=7,
								mi=2
							)[0]

		mc.skinPercent(
							skc,
							'%s.cv[0][0:1]' % self.Rbn_Nrb,
							tv=[
									(self.In_L_Jnt, 1)
								]
						)
		mc.skinPercent(
							skc,
							'%s.cv[1][0:1]' % self.Rbn_Nrb,
							tv=[
									(self.In_L_Jnt, 0.8),
									(self.Mid_L_Jnt, 0.2)
								]
						)
		mc.skinPercent(
							skc,
							'%s.cv[2][0:1]' % self.Rbn_Nrb,
							tv=[
									(self.Mid_L_Jnt, 1)
								]
						)
		mc.skinPercent(
							skc,
							'%s.cv[3][0:1]' % self.Rbn_Nrb,
							tv=[
									(self.Cnt_Jnt, 1)
								]
						)
		mc.skinPercent(
							skc,
							'%s.cv[4][0:1]' % self.Rbn_Nrb,
							tv=[
									(self.Mid_R_Jnt, 1)
								]
						)
		mc.skinPercent(
							skc,
							'%s.cv[5][0:1]' % self.Rbn_Nrb,
							tv=[
									(self.In_R_Jnt, 0.8),
									(self.Mid_R_Jnt, 0.2)
								]
						)

		self.skinJnts = []
		self.skinJntZrGrps = []
		self.poss = []
		self.posis = []

		no = 9
		for ix in range(no):

			pos = lrc.Null()
			posi = lrc.PointOnSurfaceInfo()

			pos.name = 'RbnPos%s_%s_Grp' % (part, ix+1)
			posi.name = 'Rbn%s_%s_Posi' % (part, ix+1)
			
			self.Rbn_Nrb.attr('worldSpace[0]') >> posi.attr('is')
			posi.attr('position') >> pos.attr('t')
			posi.attr('parameterV').v = 0.5
			
			pos.parent(self.Still_Grp)

			skinJnt = lrc.Joint()
			skinJntOffGrp = lrc.group(skinJnt)
			skinJntZrGrp = lrc.group(skinJntOffGrp)

			skinJnt.name = 'RbnSkin%s_%s_Jnt' % (part, ix+1)
			skinJntOffGrp.name = 'RbnSkinJntOff%s_%s_Grp' % (part, ix+1)
			skinJntZrGrp.name = 'RbnSkinJntZr%s_%s_Grp' % (part, ix+1)

			skinJntZrGrp.parent(self.Skin_Grp)
			
			mc.pointConstraint(pos, skinJntZrGrp)

			aimCon = lrc.AimConstraint()
			aimCon.name = '%s_aimConstraint' % pos
			aimCon.attr('a').v = (0, 1, 0)
			aimCon.attr('u').v = (1, 0, 0)

			aimCon.parent(pos)
			aimCon.attr('t').v = (0, 0, 0)
			aimCon.attr('r').v = (0, 0, 0)
			
			posi.attr('n') >> aimCon.attr('wu')
			posi.attr('tu') >> aimCon.attr('tg[0].tt')
			aimCon.attr('cr') >> pos.attr('r')
			
			mc.orientConstraint(pos, skinJntZrGrp)

			self.posis.append(posi)
			self.skinJnts.append(skinJnt)
			self.skinJntZrGrps.append(skinJntZrGrp)

			posi.attr('turnOnPercentage').v = True
			posi.attr('parameterU').v = (1.000/float(no+1.00)) * float(ix+1.00)

	def createMainJoint(self, jntPart, part, side, loc):

		jnt = lrr.jointAt(loc)
		jnt.name = '%s%s%sJnt' % (jntPart, part, side)

		zrGrp = lrr.groupAt(jnt)
		zrGrp.name = '%s%sJntZr%sGrp' % (jntPart, part, side)

		zrGrp.parent(self.MainJnt_Grp)

		return jnt, zrGrp

	def createMainControl(self, ctrlPart, part, side, jnt):

		col = 'yellow'
		if side == '_R_':
			col = 'red'
		elif side == '_L_':
			col = 'blue'

		ctrl = lrc.Control('cube')
		ctrl.name = '%s%s%sCtrl' % (ctrlPart, part, side)
		ctrl.color = col
		ctrl.lockHideAttrs('rx', 'ry', 'rz', 'sx', 'sy', 'sz', 'v')

		zrGrp = lrc.group(ctrl)
		zrGrp.name = '%s%sCtrlZr%sGrp' % (ctrlPart, part, side)
		zrGrp.snap(jnt)
		zrGrp.parent(self.Main_Ctrl)

		ctrl.attr('t') >> jnt.attr('t')

		return ctrl, zrGrp

def main():

	ttObj = TtRig(
					jawPost='JawPostTmp_Loc',
					mainUpTthTmpLoc='MainUpTthTmp_Loc',
					cntUpTthTmpLoc='CntUpTthTmp_Loc',
					lftUpTthTmpLoc='InUpTthTmp_L_Loc',
					rgtUpTthTmpLoc='InUpTthTmp_R_Loc',
					lftMidUpTthTmpLoc='MidUpTthTmp_L_Loc',
					rgtMidUpTthTmpLoc='MidUpTthTmp_R_Loc',
					mainLowTthTmpLoc='MainLowTthTmp_Loc',
					cntLowTthTmpLoc='CntLowTthTmp_Loc',
					lftLowTthTmpLoc='InLowTthTmp_L_Loc',
					rgtLowTthTmpLoc='InLowTthTmp_R_Loc',
					lftMidLowTthTmpLoc='MidLowTthTmp_L_Loc',
					rgtMidLowTthTmpLoc='MidLowTthTmp_R_Loc',
					tngTmpLocs=[
									'TongueTmp_1_Jnt',
									'TongueTmp_2_Jnt',
									'TongueTmp_3_Jnt',
									'TongueTmp_4_Jnt',
									'TongueTmp_5_Jnt',
									'TongueTmp_6_Jnt'
								],
					part='',
					offsetCurve=0.025
				)

def mainLite():

	ttObj = TtRigLite(
					jawPost='JawPostTmp_Loc',
					mainUpTthTmpLoc='MainUpTthTmp_Loc',
					mainLowTthTmpLoc='MainLowTthTmp_Loc',
					tngTmpLocs=[
									'TongueTmp_1_Jnt',
									'TongueTmp_2_Jnt',
									'TongueTmp_3_Jnt',
									'TongueTmp_4_Jnt',
									'TongueTmp_5_Jnt',
									'TongueTmp_6_Jnt'
								],
					part='',
					offsetCurve=0.025
				)