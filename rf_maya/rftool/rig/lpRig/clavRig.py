import maya.cmds as mc

import lpRig.core as lrc
reload(lrc)
import lpRig.rigTools as lrr
reload(lrr)

class ClavRig(object):

	def __init__(
					self,
					parent='',
					ctrlGrp='',
					ax='y',
					tmpJnt=[
								'Clav_1_L_Jnt',
								'Clav_2_L_Jnt'
							],
					part='',
					side='L'
				):

		if side:
			side = '_%s_' % side
		else:
			side = '_'

		# Skin joints
		self.Clav_Jnt = lrc.Dag(tmpJnt[0])
		self.ClavTip_Jnt = lrc.Dag(tmpJnt[1])

		self.ClavSca_Jnt = lrr.jointAt(self.Clav_Jnt)
		self.ClavSca_Jnt.name = "ClavSca%s%sJnt" % (part, side)
		self.ClavSca_Jnt.attr('ssc').v = 0
		self.ClavSca_Jnt.parent(self.Clav_Jnt)

		# Main Group
		self.Ctrl_Grp = lrc.Null()
		self.Ctrl_Grp.name = "ClavCtrl%s%sGrp" % (part, side)

		mc.parentConstraint(parent, self.Ctrl_Grp)

		# Controller
		self.Clav_Ctrl = lrr.jointControl('circle')
		self.Clav_Ctrl.name = "Clav%s%sCtrl" % (part, side)
		self.Clav_Ctrl.lockHideAttrs('v')
		self.Clav_Ctrl.rotateOrder = 'xyz'

		self.ClavCtrlZr_Grp = lrc.group(self.Clav_Ctrl)
		self.ClavCtrlZr_Grp.name = "ClavCtrlZr%s%sGrp" % (part, side)
		
		# Controller - Positioning
		self.ClavCtrlZr_Grp.snapPoint(self.Clav_Jnt)
		self.Clav_Ctrl.snapOrient(self.Clav_Jnt)
		self.Clav_Ctrl.freeze(r=True, t=False, s=False)
		self.ClavCtrlZr_Grp.parent(self.Ctrl_Grp)
		
		self.ClavGmbl_Ctrl = lrc.addGimbal(self.Clav_Ctrl)
		self.ClavGmbl_Ctrl.name = "ClavGmbl%s%sCtrl" % (part, side)
		self.ClavGmbl_Ctrl.rotateOrder = 'xyz'
		
		# Controller - Color
		if side == '_R_':
			self.Clav_Ctrl.color = 'blue'
		else:
			self.Clav_Ctrl.color = 'red'
		
		# Stretch setup
		(self.ClavStrt_Add,
		self.ClavStrt_Mul) = lrr.fkStretch(
												ctrl=self.Clav_Ctrl,
												target=self.ClavTip_Jnt,
												ax=ax
											)

		self.ClavStrt_Add.name = "ClavStrt%s%sAdd" % (part, side)
		self.ClavStrt_Mul.name = "ClavStrt%s%sMul" % (part, side)

		# Connect to joint
		mc.parentConstraint(self.ClavGmbl_Ctrl, self.Clav_Jnt)
		mc.scaleConstraint(self.ClavGmbl_Ctrl, self.ClavSca_Jnt)

		# Grouping
		self.Ctrl_Grp.parent(ctrlGrp)