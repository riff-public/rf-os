import os
import json
import pickle
import maya.cmds as mc
import maya.mel as mel
import pymel.core as pmc
from rf_maya.rftool.rig.MocapX import customMocapX as cmx
reload(cmx)
from rf_utils.pipeline import file_info 
from rf_utils.pipeline import create_asset
from rf_utils.context import context_info
from rf_utils.sg import sg_process
from rf_utils import file_utils

def read_pickle_data(file_path):
	with open(file_path, 'r') as attr_file:
		return pickle.load(attr_file)

def write_pickle_data(data, file_path):
	with open(file_path, 'w') as attr_file:
		pickle.dump(data,attr_file)

def create_folder(path):
	if '\\' in path:
		path = path.replace('\\','/')
	print 'path: %s'%path

	for level in range(len(path.split('/'))):
		print level
		path_check = '/'.join(path.split('/')[:level+1]).replace('\\','/')
		print path_check
		if not os.path.isdir(path_check):
			print 'print create folder'
			os.mkdir(path_check)

	return path_check




class mocapXRig():
	def __init__(self):
		self.winName = 'mocapXRig'
		self.poseLibName = 'PoseLib'
		self.collectionName = 'AttributeCollection'
		self.poseList = cmx.poseList
		self.eyePoseRotList =cmx.eyePoseRotList
		self.headPoseList=cmx.headPoseList


	def getNamespace(self,*args):
		character = pmc.selected()[0]
		self.ns = pmc.selected()[0].namespace()
		mc.textField('ns',e=1,tx=self.ns,bgc=[.5,1,.5])
		self.MocapX = self.ns+'MocapX'

		ref_file = character.referenceFile().path
		f = file_info.File(ref_file)
		path = f.redirect()

		tmp_mocap_context =  context_info.PathToContext(path = path)
		self.project = tmp_mocap_context.project
		self.asset = tmp_mocap_context.entity_name

		tmp_asset_context = create_asset.get_context(self.project,self.asset,'char','all')
		asset_context = context_info.ContextPathInfo(context = tmp_asset_context)
		asset_context.context.update(step ='mocapX')

		self.asset_mocap_path = asset_context.path.scheme(key='publishStepPath').abs_path()
		self.char_folder_path = asset_context.path.scheme(key='publishAssetTypePath').abs_path()
		self.ctrl_path = os.path.join(self.asset_mocap_path,'ctrl_list').replace('\\','/')
		self.head_path = os.path.join(self.asset_mocap_path,'head_ctrl').replace('\\','/')
		self.eye_path = os.path.join(self.asset_mocap_path,'eye_ctrl').replace('\\','/')
		self.fcl_path = os.path.join(self.asset_mocap_path,'fcl_grp').replace('\\','/')
		self.ignore_path = os.path.join(self.asset_mocap_path,'ignore_ctrl').replace('\\','/')
		self.left_path = os.path.join(self.asset_mocap_path,'left_ctrl').replace('\\','/')
		self.right_path = os.path.join(self.asset_mocap_path,'right_ctrl').replace('\\','/')
		self.key_path = os.path.join(self.asset_mocap_path,'mocapX_key.ma').replace('\\','/')


		if os.path.isdir(self.asset_mocap_path):
			if os.path.isfile(self.ctrl_path):
				mc.nodeIconButton('loadCtrlAsset',e=1,en=1)
				try:
					self.ctrlList = read_pickle_data(self.ctrl_path)
					self.all_ctrl_list = self.list_add_ns(self.ctrlList)
					mc.textScrollList('ctrlListField',e=1,ra=1)
					mc.textScrollList('ctrlListField',e=1,a=self.ctrlList)
					mc.nodeIconButton('loadCtrlAsset',e=1,en=1)

				except:
					mc.warning('cannot read ctrl list')
					mc.nodeIconButton('loadCtrlAsset',e=1,en=0)

			if os.path.isfile(self.head_path):
				mc.nodeIconButton('loadHeadAsset',e=1,en=1)
				try:
					#mc.button('createHeadCtrlList',e=1)#,bgc=[.5,1,.5]
					self.headCtrlList = read_pickle_data(self.head_path)
					mc.textField('HeadCtrlField',e=1,text= ','.join(self.headCtrlList))
				except:
					mc.warning('cannot read head list')
					mc.nodeIconButton('loadHeadAsset',e=1,en=0)

			if os.path.isfile(self.eye_path):
				mc.nodeIconButton('loadEyeAsset',e=1,en=1)
				try:
					self.eyeCtrlList = read_pickle_data(self.eye_path)
					mc.textField('EyeCtrlField',e=1,text=', '.join(self.eyeCtrlList))
					#mc.button('createEyeCtrlList',e=1)#,bgc=[.5,1,.5]
				except:
					mc.warning('cannot read eye list')
					mc.nodeIconButton('loadEyeAsset',e=1,en=0)

			if os.path.isfile(self.fcl_path):
				mc.nodeIconButton('loadFclAsset',e=1,en=1)
				try:
					self.fclGrp = read_pickle_data(self.fcl_path)
					mc.textField('fclGrpField',e=1,text=self.fclGrp)
					#mc.button('createEyeCtrlList',e=1)#,bgc=[.5,1,.5]
				except:
					mc.warning('cannot read eye list')
					mc.nodeIconButton('loadEyeAsset',e=1,en=0)

			if os.path.isfile(self.ignore_path):
				mc.nodeIconButton('loadFclAsset',e=1,en=1)
				try:
					mc.textScrollList(self.ignoreMirrorTsl,e=1,ra=1)
					self.ignoreList = read_pickle_data(self.ignore_path)
					mc.textScrollList('IgnoreMirrorTsl',e=1, a = self.ignoreList)			
				except:
					mc.warning('cannot read ignore ctrl list')
					mc.nodeIconButton('leftReload',e=1,en=0)

			if os.path.isfile(self.left_path):
				mc.nodeIconButton('leftReload',e=1,en=1)
				try:
					self.leftCtrlList = read_pickle_data(self.left_path)
					self.reloadLeft()
				except:
					mc.warning('cannot read left ctrl list')
					mc.nodeIconButton('leftReload',e=1,en=0)

			if os.path.isfile(self.right_path):
				mc.nodeIconButton('rightReload',e=1,en=1)
				try:
					self.rightCtrlList = read_pickle_data(self.right_path)
					self.reloadRight()
				except:
					mc.warning('cannot read right ctrl list')
					mc.nodeIconButton('rightReload',e=1,en=0)

		elif self.check_temp_folder():
			mc.nodeIconButton('loadTmp',e=1,en=1)
			load_tmp_confirm = mc.confirmDialog(m ='This asset has no MocapX data.\nDo you want to load preset from project Template ?',
								button = ['yes','no'])

			if load_tmp_confirm == 'yes':
				self.load_tmp_folder()
			else:
				pass
		try:
			if self.ctrlList:
				mc.button('createMCXTmp',bgc=[.5,1,.5],e=1,en=1)
		except:
			pass

		if self.check_temp_folder():
			mc.nodeIconButton('loadTmp',e=1,en=1)

	def check_temp_folder(self,*args):
		tmp_mocap_asset = create_asset.get_context(self.project,'mocap','char','all')
		asset_context = context_info.ContextPathInfo(context = tmp_mocap_asset)
		asset_context.context.update(step ='mocapX')

		self.tmp_mocap_work = asset_context.path.scheme(key='workAssetNamePath').abs_path()
		self.tmp_mocap_path = asset_context.path.scheme(key='publishStepPath').abs_path()
		self.tmp_ctrl_path = os.path.join(self.tmp_mocap_path,'ctrl_list').replace('\\','/')
		self.tmp_head_path = os.path.join(self.tmp_mocap_path,'head_ctrl').replace('\\','/')
		self.tmp_eye_path = os.path.join(self.tmp_mocap_path,'eye_ctrl').replace('\\','/')
		self.tmp_fcl_path = os.path.join(self.tmp_mocap_path,'fcl_grp').replace('\\','/')
		self.tmp_ignore_path = os.path.join(self.tmp_mocap_path,'ignore_grp').replace('\\','/')
		self.tmp_left_path = os.path.join(self.tmp_mocap_path,'left_ctrl').replace('\\','/')
		self.tmp_right_path = os.path.join(self.tmp_mocap_path,'right_ctrl').replace('\\','/')
		self.tmp_key_path = os.path.join(self.tmp_mocap_path,'mocapX_key.ma').replace('\\','/')
		print self.tmp_key_path

		check = 0

		for f in [self.tmp_ctrl_path,self.tmp_head_path,self.tmp_eye_path,
				self.tmp_left_path,self.tmp_right_path]:
			if os.path.isfile(f):
				check =1
			else:
				return False

		if check ==1:
			return True
		else:
			return False

	def load_tmp_folder(self):
		mc.nodeIconButton('loadTmp',e=1,en=1)
		
		if os.path.isfile(self.tmp_ctrl_path):
			self.ctrlList = read_pickle_data(self.tmp_ctrl_path)
			self.all_ctrl_list = self.list_add_ns(self.ctrlList)
			mc.textScrollList('ctrlListField',e=1,ra=1)
			mc.textScrollList('ctrlListField',e=1,a=self.ctrlList)
		
		if os.path.isfile(self.tmp_head_path):
			self.headCtrlList = read_pickle_data(self.tmp_head_path)
			mc.textField('HeadCtrlField',e=1,text= ','.join(self.headCtrlList))

		if os.path.isfile(self.tmp_eye_path):
			self.eyeCtrlList = read_pickle_data(self.tmp_eye_path)
			mc.textField('EyeCtrlField',e=1,text= ', '.join(self.eyeCtrlList))

		if os.path.isfile(self.tmp_fcl_path):
			self.fclGrp = read_pickle_data(self.tmp_fcl_path)
			mc.textField('fclGrpField',e=1,text=self.fclGrp)

		if os.path.isfile(self.tmp_ignore_path):
			self.ignoreList = read_pickle_data(self.tmp_ignore_path)
			mc.textScrollList(self.ignoreMirrorTsl,e=1,ra=1)
			mc.textScrollList('IgnoreMirrorTsl',e=1,a =(self.ignoreList))

		if os.path.isfile(self.tmp_left_path):
			self.leftCtrlList = read_pickle_data(self.tmp_left_path)
			self.reloadLeft()

		if os.path.isfile(self.tmp_right_path):
			self.rightCtrlList = read_pickle_data(self.tmp_right_path)
			self.reloadRight()


	def template_extend(self,*args):
		self.adjColumn()
		if not mc.frameLayout('tmpLayout',q=1,cl=1):
			h = mc.window(self.winName,q=1,h=1) +180
		else:
			h = mc.window(self.winName,q=1,h=1)-180
		mc.window(self.winName,e=1,h=h)
		
	def selCtrlList(self,*args):
		mc.select(self.all_ctrl_list)

	def createMocapXSys(self,*args):
		cmx.createMocapXSystem(self.poseLibName,self.collectionName,self.poseList,'',self.all_ctrl_list)
		cmx.multipleUpdatePose(self.all_ctrl_list,self.poseList)
		cmx.deleteKey(self.all_ctrl_list)

	def connectPose(self,*args):
		cmx.connectAll('','MocapX',self.all_ctrl_list,self.list_add_ns(self.headCtrlList),self.list_add_ns(self.eyeCtrlList))

	def createMocapTmp(self,*args):
		mc.refresh(suspend=1)
		cmx.createFclKey(self.all_ctrl_list)
		mc.refresh(suspend=0)
		cmx.importMocapXTmp()
 
	def create_mocapX(self,*args):
		self.createMocapXSys()
		self.createMocapXDevice()
		self.connectPose()
		

	def createMocapXDevice(self,*args):
		cmx.createMocapXDevice()

	def clearSideMode(self,*args):
		if mc.radioButtonGrp('clearFrameMode',q=1,sl=1) ==1:
			mc.radioButtonGrp('clearSideMode',e=1,en=0)

		elif mc.radioButtonGrp('clearFrameMode',q=1,sl=1) ==2:
			mc.radioButtonGrp('clearSideMode',e=1,en=1)

	def clearFrame(self,*args):
		print self.ctrlList
		mode =  mc.radioButtonGrp('clearFrameMode',q=1,sl=1)
		if mc.radioButtonGrp('clearFrameMode',q=1,sl=1) ==1:
			cur_time = mc.currentTime(q=1)
			cmx.resetFacePose(self.ns,self.ctrlList,cur_time)
		elif mc.radioButtonGrp('clearFrameMode',q=1,sl=1) ==2:
			
			if mc.radioButtonGrp('clearSideMode',q=1,sl=1) ==1:
				cmx.resetFacePoseFrame(self.ns, self.ctrlList, mode=0)

			elif mc.radioButtonGrp('clearSideMode',q=1,sl=1) ==2:
				cmx.resetFacePoseFrame(self.ns, self.ctrlList, mode=1)

			elif mc.radioButtonGrp('clearSideMode',q=1,sl=1) ==3:
				cmx.resetFacePoseFrame(self.ns, self.ctrlList, mode=2)

			elif mc.radioButtonGrp('clearSideMode',q=1,sl=1) ==4:
				cmx.resetFacePoseFrame(self.ns, self.ctrlList, mode=3)
				
	def mirrorSideMode(self,*args):
		if mc.radioButtonGrp('mirrorFrameMode',q=1,sl=1) ==1:
			mc.radioButtonGrp('mirrorSideMode',e=1,en=0)

		elif mc.radioButtonGrp('mirrorFrameMode',q=1,sl=1) ==2:
			mc.radioButtonGrp('mirrorSideMode',e=1,en=1)
			mc.radioButtonGrp('mirrorSideMode',e=1,)

	def convert_back_to_key(self,*args):
		cmx.convert_back_to_key(self.ns)
		mc.playbackOptions(min=1,ast=1)
		mc.playbackOptions(max=55,aet=55)

	def list_split_ns(self,obj_list):
		non_ns_list = []
		for obj in obj_list:
			non_ns_list.append(obj.split(self.ns)[-1])

		return non_ns_list

	def list_add_ns(self,obj_list):
		ns_list = []
		for obj in obj_list:
			ns_list.append('{}{}'.format(self.ns,obj))
		return ns_list

	#First Layout 
	# Create CtrlList Function 
	def reload_ctrl_textField(self,*args):
		self.all_ctrl_list = mc.ls(sl=True)
		self.ctrlList = self.list_split_ns(self.all_ctrl_list)
		mc.textScrollList('ctrlListField',e=1,ra=1)
		mc.textScrollList('ctrlListField',e=1,a=self.ctrlList)

	# Create HeadCtrlList Function 
	def reload_head_textField(self,*args):
		self.headCtrlList = self.list_split_ns(mc.ls(sl=True))
		mc.textField('HeadCtrlField',e=1,text= ','.join(self.headCtrlList))
	# Create EyeCtrlList Function 
	def reload_eye_textField(self,*args):
		self.eyeCtrlList = self.list_split_ns(mc.ls(sl=True))
		mc.textField('EyeCtrlField',e=1,text= ','.join(self.eyeCtrlList))
		
	def reload_fclGrp_textField(self,*args):
		self.fclGrp = self.list_split_ns(mc.ls(sl=True))[0]
		mc.textField('fclGrpField',e=1 , text=(self.fclGrp))

	def reload_ignoreList_scroll(self,*args):
		self.ignoreList = self.list_add_ns(mc.textScrollList('IgnoreMirrorTsl',q=1,ai=1))
		mc.textScrollList('IgnoreMirrorTsl',e=1 , text=','.join(self.ignoreList))

	def save_ctrl_list(self,*args):
		if not os.path.isdir(self.asset_mocap_path):
			create_folder(self.asset_mocap_path)

		write_pickle_data(self.ctrlList,self.ctrl_path)
		mc.nodeIconButton('loadCtrlAsset',e=1,en=1)
	
	def save_head_list(self,*args):
		if not os.path.isdir(self.asset_mocap_path):
			create_folder(self.asset_mocap_path)

		write_pickle_data(self.headCtrlList,self.head_path)
		mc.nodeIconButton('loadHeadAsset',e=1,en=1)
	
	def save_eye_list(self,*args):
		if not os.path.isdir(self.asset_mocap_path):
			create_folder(self.asset_mocap_path)

		write_pickle_data(self.eyeCtrlList,self.eye_path)
		mc.nodeIconButton('loadEyeAsset',e=1,en=1)

	def save_fcl_grp(self,*args):
		if not os.path.isdir(self.asset_mocap_path):
			create_folder(self.asset_mocap_path)

		write_pickle_data(self.fclGrp,self.fcl_path)
		mc.nodeIconButton('loadFclAsset',e=1,en=1)

	# Create LeftCtrlList Function 
	def save_left_ctrl(self,*args):
		if not os.path.isdir(self.asset_mocap_path):
			create_folder(self.tmp_mocap_data_path)
		write_pickle_data(self.leftCtrlList,self.left_path)	
		mc.nodeIconButton('lefttReload',e=1,en=1)
	
	# Create RightCtrlList Function
	def save_right_ctrl(self,*args):
		if not os.path.isdir(self.asset_mocap_path):
			create_folder(self.tmp_mocap_data_path)
		write_pickle_data(self.rightCtrlList,self.right_path)
		mc.nodeIconButton('rightReload',e=1,en=1)

	def save_fcl_key(self,*args):
		if not os.path.isdir(self.asset_mocap_path):
			create_folder(self.tmp_mocap_data_path)
		cmx.export_fclKey(self.ns,self.list_add_ns(self.ctrlList),self.key_path)


	def load_ctrl_list(self,*args):
		self.ctrlList = read_pickle_data(self.ctrl_path)
		self.all_ctrl_list = self.list_add_ns(self.ctrlList)
		mc.textScrollList('ctrlListField',e=1,ra=1)
		mc.textScrollList('ctrlListField',e=1,a=self.ctrlList)
	
	def load_head_list(self,*args):
		self.headCtrlList = read_pickle_data(self.head_path)
		mc.textField('HeadCtrlField',e=1,text= ','.join(self.headCtrlList))
	
	def load_eye_list(self,*args):
		self.eyeCtrlList = read_pickle_data(self.eye_path)
		mc.textField('EyeCtrlField',e=1,text= ','.join(self.eyeCtrlList))

	def load_fcl_list(self,*args):
		self.fclGrp = read_pickle_data(self.fcl_path)
		mc.textField('fclGrpField',e=1,text= (self.fclGrp))

	def load_fcl_key(self,*args):
		cmx.import_fclKey(self.ns,  self.list_add_ns(self.ctrlList), self.key_path,0,self.headCtrlList[0],1)

	def save_tmp_fcl_key(self,*args):
		if not os.path.isdir(self.asset_mocap_path):
			create_folder(self.tmp_mocap_data_path)
		cmx.export_fclKey(self.ns,self.list_add_ns(self.ctrlList), self.tmp_key_path)

	def load_tmp_fcl_key(self,*args):
		cmx.import_fclKey(self.ns,self.list_add_ns(self.ctrlList), self.tmp_key_path,0,self.headCtrlList[0],1)
	

	def load_from_file(self,*args):
		folder_path = mc.fileDialog2(ds=1,cap= 'Select Mocap Data Folder.',fileMode=3 , dir = self.char_folder_path)
		if folder_path != None:
			data_path = os.path.join(folder_path[0],'mocapX').replace('\\','/')
			browse_tmp_ctrl_path = os.path.join(data_path,'ctrl_list').replace('\\','/')
			browse_tmp_head_path = os.path.join(data_path,'head_ctrl').replace('\\','/')
			browse_tmp_eye_path = os.path.join(data_path,'eye_ctrl').replace('\\','/')
			browse_tmp_fcl_path = os.path.join(data_path,'fcl_grp').replace('\\','/')
			browse_tmp_ignore_path = os.path.join(data_path,'ignore_ctrl').replace('\\','/')
			browse_tmp_left_path = os.path.join(data_path,'left_ctrl').replace('\\','/')
			browse_tmp_right_path = os.path.join(data_path,'right_ctrl').replace('\\','/')
	
			try:
				if os.path.isfile(browse_tmp_ctrl_path):
					self.ctrlList = read_pickle_data(browse_tmp_ctrl_path)
					self.all_ctrl_list = self.list_add_ns(self.ctrlList)
					mc.textScrollList('ctrlListField',e=1,ra=1)
					mc.textScrollList('ctrlListField',e=1,a=self.ctrlList)
				
				if os.path.isfile(browse_tmp_head_path):
					self.headCtrlList = read_pickle_data(browse_tmp_head_path)
					mc.textField('HeadCtrlField',e=1,text= ','.join(self.headCtrlList))

				if os.path.isfile(browse_tmp_eye_path):
					self.eyeCtrlList = read_pickle_data(browse_tmp_eye_path)
					mc.textField('EyeCtrlField',e=1,text= ', '.join(self.eyeCtrlList))

				if os.path.isfile(browse_tmp_fcl_path):
					self.fclGrp = read_pickle_data(browse_tmp_fcl_path)
					mc.textField('fclGrpField',e=1,text=self.fclGrp)

				if os.path.isfile(browse_tmp_ignore_path):
					self.ignoreList = read_pickle_data(browse_tmp_ignore_path)
					mc.textScrollList(self.ignoreMirrorTsl,e=1,ra=1)
					mc.textScrollList('IgnoreMirrorTsl',e=1,a =(self.ignoreList))

				if os.path.isfile(browse_tmp_left_path):
					self.leftCtrlList = read_pickle_data(browse_tmp_left_path)
					self.reloadLeft()

				if os.path.isfile(browse_tmp_right_path):
					self.rightCtrlList = read_pickle_data(browse_tmp_right_path)
					self.reloadRight()

			except:
				mc.confirmDialog(m = 'There is something wrong\n with data in mocap folder.')
				return

		else:
			mc.confirmDialog(m = 'This folder does not has mocap data.')
			return



	def load_key_from_file(self,*args):
		folder_path = mc.fileDialog2(ds=1,cap= 'Select Mocap Data Folder.',fileMode=3 , dir = self.char_folder_path)
		if folder_path != None:
			data_path = os.path.join(folder_path[0],'mocapX').replace('\\','/')
			browse_tmp_key_path = os.path.join(data_path,'mocapX_key.ma').replace('\\','/')
			try:
				cmx.import_fclKey(self.ns,  self.list_add_ns(self.ctrlList), browse_tmp_key_path)
			except:
				mc.confirmDialog(m = 'There is something wrong\n with data in mocap folder.')
				return

		else:
			mc.confirmDialog(m = 'This folder does not has mocap data.')

	# SecondLayout Function 
	def renum(self,scrollName,ctrlList):
		scrollList = []
		for number , ctrl in enumerate(ctrlList):
			noNs = ctrl.split(self.ns)[-1]
			ctrl = '{}   :   {}'.format(number,noNs)
			scrollList.append(ctrl)
		mc.textScrollList(scrollName,e=1,removeAll=1)
		mc.textScrollList(scrollName,e=1,append = scrollList)

	def renumFromScrollName(self,scrollName):
		scrollList = mc.textScrollList(scrollName,q=1,ai=1)
		newList = []
		for i in scrollList:
			ctrl = i.split('   :   ')[-1]
			newList.append(ctrl)
		self.renum(scrollName,newList)
	
	# reload ScrollList
	def reloadList(self,ctrlListAttr):
		getCtrlList = ctrlListAttr
		scrollList = []
		for number , ctrl in enumerate(getCtrlList):
			noNs = ctrl.split(self.ns)[-1]
			ctrl = '{}   :   {}'.format(number,noNs)
			scrollList.append(ctrl)
		return scrollList

	def reloadLeft(self,*args):
		scrollList = self.reloadList(self.leftCtrlList)
		mc.textScrollList('leftCtrl',e=1,removeAll=1)
		mc.textScrollList('leftCtrl',e=1,append = scrollList)

	def reloadRight(self,*args):
		scrollList = self.reloadList(self.rightCtrlList)
		mc.textScrollList('rightCtrl',e=1,removeAll=1)
		mc.textScrollList('rightCtrl',e=1,append = scrollList)

	# update ScrollList 
	def updateLeft(self,*args):
		textList = mc.textScrollList('leftCtrl',q=1,ai=1)
		ctrlList = [i.split('   :   ')[-1] for i in textList]
		ctrlStr = ','.join(ctrlList)
		self.leftCtrlList = ctrlList
		if not os.path.isdir(self.asset_mocap_path):
			create_folder(self.asset_mocap_path)
		write_pickle_data(self.leftCtrlList,self.left_path)
		mc.nodeIconButton('leftReload',e=1,en=1)

	def updateRight(self,*args):
		textList = mc.textScrollList('rightCtrl',q=1,ai=1)
		ctrlList = [i.split('   :   ')[-1] for i in textList]
		ctrlStr = ','.join(ctrlList)
		self.rightCtrlList = ctrlList
		if not os.path.isdir(self.asset_mocap_path):
			create_folder(self.asset_mocap_path)
		write_pickle_data(self.rightCtrlList,self.right_path)
		mc.nodeIconButton('rightReload',e=1,en=1)
	
	# swap Left Right
	def toRight(self,*args):
		strList = mc.textScrollList('leftCtrl',q=1,si=1)
		for selected in strList:
			nowLen = mc.textScrollList('rightCtrl',q=1,ni=1)
			selStr = selected.split('   :   ')[-1]
			addStr = '{}   :   {}'.format(nowLen+1,selStr)
			mc.textScrollList('rightCtrl',e=1,append = addStr)
			mc.textScrollList('leftCtrl',e=1,ri = selected)

		textList = mc.textScrollList('leftCtrl',q=1,ai=1)
		if textList !=None:
			ctrlList = [i.split('   :   ')[-1] for i in textList]
			self.renum('leftCtrl',ctrlList)

	def toLeft(self,*args):
		strList = mc.textScrollList('rightCtrl',q=1,si=1)
		for selected in strList:
			nowLen = mc.textScrollList('leftCtrl',q=1,ni=1)
			selStr = selected.split('   :   ')[-1]
			addStr = '{}   :   {}'.format(nowLen+1,selStr)
			mc.textScrollList('leftCtrl',e=1,append = addStr)
			mc.textScrollList('rightCtrl',e=1,ri = selected)
		
		textList = mc.textScrollList('rightCtrl',q=1,ai=1)
		if textList !=None:
			ctrlList = [i.split('   :   ')[-1] for i in textList]
			self.renum('rightCtrl',ctrlList)

	# sort ScrollList 
	def sortScrollList(self,scrollName):
		textList = mc.textScrollList(scrollName,q=1,ai=1)
		ctrlList = [i.split('   :   ')[-1] for i in textList]
		scrollList = []
		for number , ctrl in enumerate(sorted(ctrlList)):
			noNs = ctrl.split(self.ns)[-1]
			ctrl = '{}   :   {}'.format(number,noNs)
			scrollList.append(ctrl)
		textList = mc.textScrollList(scrollName,e=1,removeAll=1)
		textList = mc.textScrollList(scrollName,e=1,a=scrollList)

	def sortLeft(self,*args):
		self.sortScrollList('leftCtrl')

	def sortRight(self,*args):
		self.sortScrollList('rightCtrl')

	# updown ScrollList 
	def upScroll(self,scrollName):
		intList = mc.textScrollList(scrollName,q=1,sii=1)
		strList = mc.textScrollList(scrollName,q=1,si=1)
		selList = []
		for num in range(len(strList)):
			idx = intList[num]
			if 1 not  in intList:
				if idx  >= 2:
					mc.textScrollList(scrollName,e=1,rii=idx)
					mc.textScrollList(scrollName,e=1,ap=[idx-1,strList[num]])
					selList.append(idx-1)

				else:
					selList.append(idx)
					pass

				self.renumFromScrollName(scrollName)
				mc.textScrollList(scrollName,e=1,sii=selList)
			else:
				mc.textScrollList(scrollName,e=1,sii = intList)
	
	def upLeft(self,*args):
		self.upScroll('leftCtrl')

	def upRight(self,*args):
		self.upScroll('rightCtrl')

	def downScroll(self,scrollName):
		intList = mc.textScrollList(scrollName,q=1,sii=1)
		strList = mc.textScrollList(scrollName,q=1,si=1)
		maxNum = mc.textScrollList(scrollName,q=1,ni=1)
		selList = []

		for num in range(len(strList))[::-1]:
			idx = intList[num]
			if maxNum not in intList:
				if idx  <= maxNum-1:
					print 'idx : {} < strList : {}'.format(idx+1,strList[num])
					mc.textScrollList(scrollName,e=1,rii=idx)
					mc.textScrollList(scrollName,e=1,ap=[idx+1,strList[num]])
					selList.append(idx+1)
				else:
					print 'there something in this case : idx: {} and strList :{}'.format(idx+1,strList[num])
					selList.append(idx)
				print selList

				self.renumFromScrollName(scrollName)

				mc.textScrollList(scrollName,e=1,sii=selList)
			else:
				mc.textScrollList(scrollName,e=1,sii = intList)

	def downLeft(self,*args):
		self.downScroll('leftCtrl')

	def downRight(self,*args):
		self.downScroll('rightCtrl')

	# add / remove 
	def addCtrl(self,scrollName):
		sel = mc.ls(sl=True)
		if sel != []:
			for i in sel:
				ctrl = i.split(self.ns)[-1]
				mc.textScrollList(scrollName,e=1,a=ctrl)
			self.renumFromScrollName(scrollName)

	def addLeft(self,*args):
		self.addCtrl('leftCtrl')
	

	def addRight(self,*args):
		self.addCtrl('rightCtrl')
	

	def removeCtrl(self,scrollName):
		intList = mc.textScrollList(scrollName,q=1,sii=1)
		if intList != None:
			for idx in intList[::-1]:
				mc.textScrollList(scrollName,e=1,rii=idx)
		if mc.textScrollList(scrollName,q=1,ai=1) != None:
			self.renumFromScrollName(scrollName)
			mc.textScrollList(scrollName,e=1,sii=intList[0]-1)
	
	def remLeft(self,*args):
		self.removeCtrl('leftCtrl')

	def remRight(self,*args):
		self.removeCtrl('rightCtrl')	

	# select Command 
	def selectFromScroll(self,*args):
		leftList = mc.textScrollList('leftCtrl',q=1,si=1)
		rightList = mc.textScrollList('rightCtrl',q=1,si=1)

		if leftList == None:
			leftList = []
		
		if rightList == None:
			rightList = []			
		
		selList = []
		itemList = list(set(leftList) | set(rightList))
		if itemList != None:
			for i in itemList:
				ctrlName = i.split('   :   ')[-1]
				ctrl = '{}{}'.format(self.ns,ctrlName)
				selList.append(ctrl)
			mc.select(selList)


	# mirror Face Frame 
	def mirAllFrameL(self,*args):
		cmx.mirAllFrame(self.ns,2,self.ctrlList,rTol=0)

	def mirFrame(self,*args):
		mir_attr = []
		translate_ax_mir = mc.checkBoxGrp('translateAx',va4=1,q=1)
		rotate_ax_mir = mc.checkBoxGrp('rotateAx',va4=1,q=1)
		scale_ax_mir = mc.checkBoxGrp('scaleAx',va4=1,q=1)

		for idx , ax in enumerate('xyz'):
			if translate_ax_mir[idx]:
				mir_attr.append('t{}'.format(ax))
			if rotate_ax_mir[idx]:
				mir_attr.append('r{}'.format(ax))
			if scale_ax_mir[idx]:
				mir_attr.append('s{}'.format(ax))

		if mc.radioButtonGrp('mirrorFrameMode',q=1,sl=1) == 1:
			cur_frame = mc.currentTime(q=1)
			if self.check_mir_side(cur_frame) == 2:
				mir_idx = cmx.leftFrame.index(cur_frame)
				cmx.mirror_frame(self.list_add_ns(self.leftCtrlList),self.list_add_ns(self.rightCtrlList), [cur_frame],
				[cmx.rightFrame[mir_idx]], mir_attr, self.list_add_ns(self.ignoreList))

			elif self.check_mir_side(cur_frame) == 3:
				mir_idx = cmx.rightFrame.index(cur_frame)
				cmx.mirror_frame(self.list_add_ns(self.rightCtrlList),self.list_add_ns(self.leftCtrlList), [cur_frame],
				[cmx.leftFrame[mir_idx]], mir_attr, self.list_add_ns(self.ignoreList))
			else:
				mc.warning('current frame cannot be mirrored.')
				return
		elif mc.radioButtonGrp('mirrorFrameMode',q=1,sl=1) == 2:
			mirMode = mc.radioButtonGrp('mirrorSideMode',q=1,sl=1)
			if mirMode ==1:
				cmx.mirror_frame(self.list_add_ns(self.leftCtrlList),self.list_add_ns(self.rightCtrlList), cmx.leftFrame,
				 cmx.rightFrame, mir_attr, self.list_add_ns(self.ignoreList))
			elif mirMode ==2:
				cmx.mirror_frame(self.list_add_ns(self.rightCtrlList),self.list_add_ns(self.leftCtrlList), cmx.rightFrame,
				cmx.leftFrame, mir_attr, self.list_add_ns(self.ignoreList))

	
	def check_mir_side(self,frame):
		if frame in cmx.midFrame:
			return 1
		elif frame in cmx.leftFrame:
			return 2
		elif frame in cmx.rightFrame:
			return 3

	# pair Face Frame 
	def pairScroll(self,oriScrollName,targetScrollName):
		oriIdxList = mc.textScrollList(oriScrollName,q=1,sii=1)
		tarIdxList = mc.textScrollList(targetScrollName,q=1,sii=1)
		tarStrList = mc.textScrollList(targetScrollName,q=1,si=1)
		if len(oriIdxList)==1 and len(tarIdxList)==1:
			mc.textScrollList(targetScrollName,e=1,rii=tarIdxList[0])
			mc.textScrollList(targetScrollName,e=1,ap=[oriIdxList[0],tarStrList[0]])
			self.renumFromScrollName(targetScrollName)
			mc.textScrollList(targetScrollName,e=1,sii=oriIdxList[0])
		else:
			mc.confirmDialog(m='please select only one item from both scrollList.')

	def pairLToR(self,*args):
		self.pairScroll('leftCtrl','rightCtrl')


	def pairRToL(self,*args):
		self.pairScroll('rightCtrl','leftCtrl')
	
	# find Item
	def findItem(self,ScrollName):
		sel = mc.ls(sl=True)
		strList = []
		deSelList = []
		referList = mc.textScrollList(ScrollName,q=1,ai=1)

		for i in sel:
			ctrl = '   :   '+i.split(self.ns)[-1]
			for lists in referList:
				if ctrl in lists:
					strList.append(lists)

		mc.textScrollList(ScrollName,e=1,si=strList)

	def findLeft(self,*args):
		self.findItem('leftCtrl')

	def findRight(self,*args):
		self.findItem('rightCtrl')

	# template
	def load_template(self,*args):
		if self.check_temp_folder():
			self.load_tmp_folder()
		else:
			mc.confirmDialog(m='Project template folder has not created yet.')

	# ignore tab command
	def addObject(self, *args):
		obj = mc.ls(sl=True)
		if obj:
			mc.textScrollList(self.ignoreMirrorTsl, e=True, a = self.list_split_ns(obj))
			
	def removeObject(self, *args):
		obj = mc.textScrollList(self.ignoreMirrorTsl, q=True, si=True)
		if obj:
			mc.textScrollList(self.ignoreMirrorTsl, e=True, ri=obj)
		
	def removeAllObject(self, *args):
		obj = mc.textScrollList(self.ignoreMirrorTsl, q=True, ai=True)
		if obj:
			mc.textScrollList(self.ignoreMirrorTsl, e=True, ra=True)
		
	def sortObject(self, *args):
		obj = mc.textScrollList(self.ignoreMirrorTsl, q=True, ai=True)
		if obj:
			self.removeAllObject()
			sort = sorted(obj)
			mc.textScrollList(self.ignoreMirrorTsl, e=True, a=sort)

	def save_ignore_list(self,*args):
		self.ignoreList = mc.textScrollList(self.ignoreMirrorTsl,q=1,ai=1)
		write_pickle_data(self.ignoreList, self.ignore_path)
		mc.nodeIconButton('loadIgnore_button',e=1,en=1)

	def load_ignore_list(self,*args):
		mc.textScrollList(self.ignoreMirrorTsl,e=1,ra=1)
		self.ignoreList = read_pickle_data(self.ignore_path)
		mc.textScrollList(self.ignoreMirrorTsl, e=True, a = self.ignoreList)


	def save_template(self,*args):
		# confirmation to save template
		print self.tmp_mocap_path
		if not os.path.isdir(self.tmp_mocap_path):
			confirm = mc.confirmDialog(m = 'Project template folder has not created yet.\nCreate a template Folder ?' ,
			 button = ['create','cancel'])
			#print confirm
			
			if confirm =='create':
				create_folder(self.tmp_mocap_path)
		else:
			
			if self.check_temp_folder():
				overwrite = mc.confirmDialog(m = 'The mocap data is already exists.\nDo you want to overwrite it ?' ,
				 button = ['confirm','cancel'])
			else:
				overwrite = mc.confirmDialog(m = 'The mocap data is not exists.\nDo you want to create data ?' ,
				 button = ['confirm','cancel'])

			if overwrite =='confirm':
				with open(self.tmp_ctrl_path,'w') as f:
					write_pickle_data(self.ctrlList,self.tmp_ctrl_path)
				
				with open(self.tmp_head_path,'w') as f:
					write_pickle_data(self.headCtrlList,self.tmp_head_path)	
				
				with open(self.tmp_eye_path,'w') as f:	
					write_pickle_data(self.eyeCtrlList,self.tmp_eye_path)
				
				with open(self.tmp_fcl_path,'w') as f:	
					write_pickle_data(self.fclGrp,self.tmp_fcl_path)
				
				with open(self.tmp_ignore_path,'w') as f:	
					write_pickle_data(self.ignoreList,self.tmp_ignore_path)
				
				with open(self.tmp_left_path,'w') as f:	
					write_pickle_data(self.leftCtrlList,self.tmp_left_path)	
				
				with open(self.tmp_right_path,'w') as f:	
					write_pickle_data(self.rightCtrlList,self.tmp_right_path)
		
			if self.check_temp_folder():
				mc.nodeIconButton('loadTmp',e=1,en=1)


	# adj Column
	def adjColumn(self,*args):
		tabIdx = mc.tabLayout('tmpSetupTab',q=1,sti=1)
		#print tabIdx
		if tabIdx ==1:
			mc.nodeIconButton('loadTmp',e=1,w=170 )
			mc.nodeIconButton('saveTmp',e=1,w=170 )
			mc.nodeIconButton('browseFile',e=1,w=355 )

			mc.tabLayout('tmpSetupTab',e=1,w=380)
			mc.columnLayout('mainCol',e=1,w=380)
			mc.window(self.winName,e=1,w=380)

		elif tabIdx ==2:
			mc.nodeIconButton('loadTmp',e=1,w=210 )
			mc.nodeIconButton('saveTmp',e=1,w=210 )
			mc.nodeIconButton('browseFile',e=1,w=430 )

			mc.tabLayout('tmpSetupTab',e=1,w=480)
			mc.columnLayout('mainCol',e=1,w=480)
			mc.window(self.winName,e=1,w=480)
			

		elif tabIdx ==3:
			mc.nodeIconButton('loadTmp',e=1,w=170 )
			mc.nodeIconButton('saveTmp',e=1,w=170 )
			mc.nodeIconButton('browseFile',e=1,w=360 )

			mc.tabLayout('tmpSetupTab',e=1,w=380)
			mc.columnLayout('mainCol',e=1,w=380)
			mc.window(self.winName,e=1,w=360)
	
		elif tabIdx ==4:
			mc.nodeIconButton('loadTmp',e=1,w=170 )
			mc.nodeIconButton('saveTmp',e=1,w=170 )
			mc.nodeIconButton('browseFile',e=1,w=360 )

			mc.tabLayout('tmpSetupTab',e=1,w=380)
			mc.columnLayout('mainCol',e=1,w=380)
			mc.window(self.winName,e=1,w=380)

	def check_realtime(self,*args):
		try:
			cmx.nd.set_realtime_device_conn_type('RealtimeDevice',1,1)
			cmx.cm.establish_connection('RealtimeDevice')
			cmx.set_source('MocapX','RealtimeDevice')
			cmx.cm.start_session('RealtimeDevice')
		except:
			self.disconnect_mocap()
			mc.confirmDialog(m= 'Something wrong please try agiain.')
		#commands.start_session(self.realtime_device)

	def disconnect_mocap(self,*args):
		cmx.cm.break_connection('RealtimeDevice')
		mc.confirmDialog(m= 'Done.')


	def check_file(self,*args):
		if mc.objExists('MocapX') and mc.objExists('ClipReader'):
			fileDialog = mc.fileDialog2(ds=1,cap= 'Select MocapX file.',fileMode=1 ,dir = r'P:\Library\MocapX')
			if fileDialog:
				#cmx.cm.break_connection('ClipReader')
				mocapX_file = fileDialog[0]
				#cmx.cm.establish_connection('ClipReader')
				cmx.utils.clipreader_load_clip('ClipReader',mocapX_file)
				#cmx.cm.set_active_source('MocapX','ClipReader')
				cmx.cm.establish_connection('ClipReader')
				cmx.set_source('MocapX','ClipReader')
				#mc.setAttr('ClipReader.clipFilepath',mocapX_file,type = 'string')
				
				
				#cmx.connectClipReader('MocapX','ClipReader',cmx.poseList)
				#cmx.connectAll('','MocapX',self.all_ctrl_list,self.list_add_ns(self.headCtrlList),self.list_add_ns(self.eyeCtrlList))

		else:
			mc.confirmDialog(m = 'MocapX system not found.\n Plase make sure taht MocapX system already created.')

		
	def check_fbx(self,*args):
		if mc.objExists('MocapX') and mc.objExists('ClipReader'):
			fileDialog = mc.fileDialog2(ds=1,cap= 'Select MocapX file.',fileMode=1 ,dir = r'P:\Library\MocapX')
			if fileDialog:
				mocapX_file = fileDialog[0]
				mc.file(mocapX_file, r=1, namespace = 'fbxMocapX')
				cmx.connect_mocapX_fbx('fbxMocapX:Reference')

		else:
			mc.confirmDialog(m = 'MocapX system not found.\n Plase make sure taht MocapX system already created.')


	# UI Layout Part 
	def showMocapXRigUI(self):
		if mc.window(self.winName,exists=1):
			#print 'delUI'
			mc.deleteUI(self.winName)

		ui = mc.window(self.winName,title = 'mocapXRig')

		# first layout
		mainLayout = mc.columnLayout('mainCol',cal='center',co=['both',15],w=400,adj=1)
		nsColumn = mc.columnLayout('nsColumn',cal='center',adj=1)
		mc.separator(h=10,style ='none')
		mc.text('Please fill namespace of character.')
		nsTxField =mc.textField('ns',w=200,ed=0,bgc=[1,.5,.5])
		mc.separator(h=5,style ='none')
		mc.button('getNS',l='get namespace',c = self.getNamespace)
		mc.separator(h=5,style ='none')
		template_layout = mc.frameLayout('tmpLayout',label = 'Template for project', cl=1, cll=1 ,cc = self.template_extend)
		mc.columnLayout('tmpColumn',co= ['left',10],bgc = [.225,.225,.225])
		mc.separator(style ='none', h = 10)
		mc.rowLayout('resetRow2',nc=3)		#mc.separator(style ='none', w = 10)
		mc.nodeIconButton('loadTmp',l='Load Project Template',w=170,h=40 ,mw=10, st =  "iconAndTextHorizontal" ,
		i = 'openScript.png',bgc = [0.35,0.35,0.35] ,c =self.load_template,en=0)
		mc.separator(style ='none', w = 10)
		mc.nodeIconButton('saveTmp',l='Save Project Template',w=170,h=40,mw=10 , st =  "iconAndTextHorizontal" ,  
		i = 'save.png',bgc = [0.35,0.35,0.35], c =self.save_template)
		mc.setParent('tmpColumn')
		mc.separator(style ='none', h = 8)
		mc.nodeIconButton('browseFile',l='Browse from Folder',w=355,h=35 ,st =  "iconAndTextHorizontal" ,  
		i = 'zoom.png' ,bgc = [0.35,0.35,0.35], c = self.load_from_file)
		mc.separator(style ='none', w = 25)
		mc.separator(h=12,style='singleDash')
		mc.setParent(template_layout)

		mc.setParent(mainLayout)
		mc.separator(h=14,style ='none')
		tmpTabLayout = mc.tabLayout('tmpSetupTab',p=mainLayout,cr=1,sc=self.adjColumn,w=320)

		tmpSetupAnimColumn= mc.columnLayout('tmpSetup',cal='center',co=['both',15] ,adj=1)
		mc.separator(h=15,style='none')
		mc.text('tmpSetUpText',l='Set up controller attribute for further process.')
		mc.text('tmpSetUpText03',l='Select Controller and assgin Attribute to each part.')
		mc.separator(h=5,style = 'none')
		mc.separator(h=10,style = 'singleDash',w=120)
		mc.separator(h=10,style = 'none')
		mc.text('tmpSetUpText01',l='Controller Attribute for connecting with MocapX.')
		mc.separator(h=10,style = 'none')

		tmpSetupColumn= mc.columnLayout('tmpSetup',cal='center',adj=1)		
		mc.textScrollList('ctrlListField',h=100)
		mc.rowLayout('ctrlListColumn',adj=1,numberOfColumns = 3)
		mc.button('createCtrlList',l='allCtrlList',w=100,h=35,c= self.reload_ctrl_textField,)#bgc=[1,.5,.5])
		mc.nodeIconButton('loadCtrlAsset',l='',w=45,h=35, i = 'openScript.png',en=0,c=self.load_ctrl_list)	
		mc.nodeIconButton('saveCtrlAsset',l='',w=45,h=35, i = 'save.png',c= self.save_ctrl_list)		
	
		mc.setParent(tmpSetupColumn)
		mc.separator(h=10,style ='singleDash',w=100)
		mc.separator(h=2,style ='none')
		mc.text(l='Select HeadCtrl.')
		mc.separator(h=5,style ='none')
		mc.textField('HeadCtrlField')
		mc.rowLayout('headListColumn',adj=1,numberOfColumns = 3)
		mc.button('createHeadCtrlList',l='HeadCtrlList',w=100,h=35,c= self.reload_head_textField,)#bgc=[1,.5,.5])		
		mc.nodeIconButton('loadHeadAsset',l='',w=45,h=35, i = 'openScript.png',en=0,c=self.load_head_list)	
		mc.nodeIconButton('saveHeadAsset',l='',w=45,h=35, i = 'save.png',c= self.save_head_list)	
		
		mc.setParent(tmpSetupColumn)
		mc.separator(h=8,style ='none')
		mc.separator(h=10,style ='singleDash',w=100)
		mc.text(l='Select LeftEyeCtrl then select RightEyeCtrl.')
		mc.separator(h=5,style ='none')
		mc.textField('EyeCtrlField')
		mc.rowLayout('eyeListColumn',adj=1,numberOfColumns = 3)	
		mc.button('createEyeCtrlList',l='EyeCtrlList',w=100,h=35,c= self.reload_eye_textField,)#bgc=[1,.5,.5])		
		mc.nodeIconButton('loadEyeAsset',l='',w=45,h=35, i = 'openScript.png',en=0,c=self.load_eye_list)	
		mc.nodeIconButton('saveEyeAsset',l='',w=45,h=35, i = 'save.png',c= self.save_eye_list)	

		mc.setParent(tmpSetupColumn)
		mc.separator(h=5,style ='none')
		mc.separator(h=5,style ='singleDash',w=120)
		mc.separator(h=5,style ='none')
		mc.text(l='Select facial control group.')
		mc.separator(h=5,style ='none')
		mc.textField('fclGrpField')
		mc.rowLayout('fclGrpRow',adj=1,numberOfColumns = 3)
		mc.button('createfclGrp',l='facial control group',w=100,h=35,c= self.reload_fclGrp_textField,)#bgc=[1,.5,.5])		
		mc.nodeIconButton('loadFclAsset',l='',w=45,h=35, i = 'openScript.png',en=0 ,c=self.load_fcl_list)	
		mc.nodeIconButton('saveFclAsset',l='',w=45,h=35, i = 'save.png', c= self.save_fcl_grp)			
		
		mc.setParent(tmpSetupColumn)
		mc.separator(h=8,style ='none')
		mc.setParent(tmpSetupColumn)
		mc.separator(h=10,style ='singleDash',w=120)


		#second layout 
		mc.setParent(tmpTabLayout)
		pairTool = mc.columnLayout('pairTool',cal='center',co=['both',15],w=300,adj=1)
		instruct_column = mc.columnLayout('pairInstruction',cal='center',adj=1)
		mc.separator(h=20,style='none')
		mc.text(l='Pair controller between left and right controller.\nParing is for mirroring pose of MocapX in further process.')
		mc.separator(h=20,style='singleDash')
		mc.setParent(pairTool)
		pairRowLayout = mc.rowLayout(numberOfColumns=7)
		mc.separator(h=12,style='none')


		# left CtrlColum
		leftCtrlColumn = mc.columnLayout('leftCtrlColumn',cal='center',w=200,adj=1)
		mc.separator(h=6,style ='none')
		mc.text('leftHead', l='Left Ctrl')
		mc.separator(h=3,style ='none')
		leftAddRemoveColumn = mc.columnLayout('leftAddRemoveColumn',cal='center',adj=1)
		leftAddRemoveRow = mc.rowLayout('leftAddRemoveRow',nc=2,w=180)
		mc.button('leftAddButton',l='add',w=100,c=self.addLeft)
		mc.button('leftRemoveAddButton',l='remove',w=100,c=self.remLeft)
		mc.setParent(leftCtrlColumn)
		mc.button('leftFind',l='Find',w=100,c=self.findLeft)
		mc.separator(h=3,style ='none')
		mc.textScrollList('leftCtrl',w=180,h=250,allowMultiSelection=1,shi=2,sc=self.selectFromScroll)
		
		leftOrderColumn = mc.columnLayout('leftOrderColumn',cal='center',adj=1)
		leftOrderRow = mc.rowLayout('leftOrderRow',nc=2,w=200)
		mc.button('leftUp',l=u"\u25B2",w=100,h=18,c=self.upLeft)
		mc.button('leftDown',l=u"\u25BC",w=100,h=18,c=self.downLeft)
		mc.setParent(leftCtrlColumn)
		mc.button('leftSort',l='Sort',w=100,c=self.sortLeft)
		mc.separator(h=2,style='none')
		lCtrlPair = mc.button('leftPair',l='Pair',w=75,c=self.pairLToR)			
		

		mc.separator(h=10,style='none')
		mc.setParent(leftCtrlColumn)
		leftReload = mc.nodeIconButton('leftReload',l='Reload',w=75,mw=65,en=0,c=self.reloadLeft, st =  "iconAndTextHorizontal" ,  i = 'openScript.png',)
		mc.separator(h=1,style='none')
		lCtrlUpdate = mc.nodeIconButton('leftUpdate',l='Update',w=75,mw=65,c=self.updateLeft, st =  "iconAndTextHorizontal" ,  i = 'save.png')
		mc.separator(h=1,style='none')


		mc.setParent(pairRowLayout)
		mc.columnLayout('swapColumn2',cal='center',w=28,adj=1,co=['both',1])
		mc.button('toRight',l='>',w=20,h=60,c=self.toRight)
		mc.separator(style='none',h=30)
		mc.button('toLeft',l='<',w=20,h=60,c=self.toLeft)
		mc.setParent(pairRowLayout)
		

		# right CtrlColum
		rightCtrlColumn = mc.columnLayout('rightCtrlColumn',cal='center',w=200,adj=1)
		mc.separator(h=6,style ='none')
		mc.text('rightHead', l='Right Ctrl')
		mc.separator(h=3,style ='none')
		rightAddRemoveColumn = mc.columnLayout('rightAddRemoveColumn',cal='center',adj=1)
		rightAddRemoveRow = mc.rowLayout('rightAddRemoveRow',nc=2,w=180)
		mc.button('rightAddButton',l='add',w=100,c=self.addRight)
		mc.button('rightRemoveAddButton',l='remove',w=100,c=self.remRight)
		mc.setParent(rightCtrlColumn)
		mc.button('rightFind',l='Find',w=100,c=self.findRight)
		mc.separator(h=3,style ='none')
		mc.textScrollList('rightCtrl',w=180,h=250,allowMultiSelection=1,shi=1,sc=self.selectFromScroll)
		#mc.textScrollList('rightCtrl',e=1,append = ['test6','test5','test4','test3','test2'])
		
		rightOrderColumn = mc.columnLayout('rightOrderColumn',cal='center',adj=1)
		rightOrderRow = mc.rowLayout('rightOrderRow',nc=2,w=200)
		mc.button('rightUp',l=u"\u25B2",w=100,h=18,c=self.upRight)
		mc.button('rightDown',l=u"\u25BC",w=100,h=18,c=self.downRight)
		mc.setParent(rightCtrlColumn)
		
		mc.button('rightSort',l='Sort',w=100,c=self.sortRight)
		mc.separator(h=2,style='none')
		rCtrlPair = mc.button('rightPair',l='Pair',w=75,c=self.pairRToL)	
		
		mc.separator(h=10,style='none')
		mc.setParent(rightCtrlColumn)
		rightReload = mc.nodeIconButton('rightReload',l='Reload',w=75,mw=65,en=0,c=self.reloadRight, st =  "iconAndTextHorizontal" ,  i = 'openScript.png',)
		mc.separator(h=1,style='none')
		rCtrlUpdate = mc.nodeIconButton('rightUpdate',l='Update',w=75,mw=65,c=self.updateRight, st =  "iconAndTextHorizontal" ,  i = 'save.png',)
		mc.setParent(pairRowLayout)
		mc.separator(w=30,style='none',h=30)


		# ignore mirror tab
		mc.setParent(tmpTabLayout)
		self.ignoreMirrorUI = mc.columnLayout("IgnoreMirrorUI", w=370, cal="center", cat=["both", 10],adj=1)
		mc.separator(st="in", h=10, p=self.ignoreMirrorUI)
		
		mc.text("IgnoreMirrorText", l="Ignore Mirror Na Ja", w=360, p=self.ignoreMirrorUI)
		mc.separator(st="none", h=10, p=self.ignoreMirrorUI)
		
		self.addRemoveUI = mc.rowLayout("AddRemoveUI", w=360, nc=3, p=self.ignoreMirrorUI)
		self.addButton = mc.button("AddButton", l="Add", w=173, h=30, al="center", c=self.addObject, p=self.addRemoveUI)
		mc.separator(st='none',w=1)
		self.removeButton = mc.button("RemoveButton", l="Remove", w=173, h=30, al="center", c=self.removeObject, p=self.addRemoveUI)

		mc.setParent(self.ignoreMirrorUI)
		mc.separator(st='none',h=2)
		self.ignoreMirrorTsl = mc.textScrollList("IgnoreMirrorTsl", w=330, h=200, ams=True, p=self.ignoreMirrorUI)
		mc.separator(st='none',h=2)
		self.sortButton = mc.button("SortButton", l="Sort", w=163, h=30, al="center",  c=self.sortObject)
		mc.separator(st='none',h=2)
		self.removeAllButton = mc.button("RemoveAllButton", l="Remove All", w=163, h=30, al="center",  c=self.removeAllObject)
		
		mc.separator(st="none", h=2, p=self.ignoreMirrorUI)
		self.saveLoadUI = mc.rowLayout("SaveLoadUI", w=360, nc=3, p=self.ignoreMirrorUI)
		self.loadButton = mc.nodeIconButton("loadIgnore_button", l="Load", st="iconAndTextVertical", w=173, h=50, i="openScript", al="center", en=1,c=self.load_ignore_list)
		mc.separator(st="none", w=1, p=self.saveLoadUI)
		self.saveButton = mc.nodeIconButton("saveIgnore_button", l="Save", st="iconAndTextVertical", w=173, h=50, i="save", al="center", c=self.save_ignore_list)
		mc.setParent(self.ignoreMirrorUI)
		mc.separator(st="none", h=5, p=self.ignoreMirrorUI)
		mc.separator(st="in", w=340, p=self.ignoreMirrorUI)
		

		# mirror Tab
		mc.setParent(tmpTabLayout)
		keyFrameTab = mc.columnLayout('keyFrameTab',co=['both',10],adj=1,cal='center')
		mc.separator(h=20,style='none')
		mc.text(l='Posing the facial expression of character rig \nto match with MocapX Guide.')
		mc.separator(h=20,style='singleDash')
		mc.setParent(keyFrameTab)
		mc.separator(h=5,style ='none')
		
		mc.button('selAllCtrl',l='select all Control',h=35,w=150,c=self.selCtrlList,)#bgc=[1,.5,.5])
		mc.separator(h=10,style ='none')
		
		mc.button('createMCXTmp',l='Create MocapX Tmp',w=150,h=35,c=self.createMocapTmp ,en=1,bgc=[1,.5,.5])
		mc.separator(h=5,style='none')
		projectKey = mc.rowLayout('projectKey',nc=3)
		
		mc.nodeIconButton('importProjectKey',l='import project preset key',h=50,w=167,
			st =  "iconAndTextVertical",i = 'openScript.png', c= self.load_tmp_fcl_key)
		mc.separator(hr=0,w=10,h=50,style = 'singleDash')
		
		mc.nodeIconButton('exportProjectKey',l='export project preset key',h=50,w=167,
			st =  "iconAndTextVertical",i = 'save.png', c= self.save_tmp_fcl_key)
		mc.setParent(keyFrameTab)
		mc.separator(h=15,style ='singleDash')
		
		resetLayout = mc.columnLayout('resetLayout',adj=1,cal='left')
		mc.radioButtonGrp( 'clearFrameMode',l='Side :', labelArray2=['current frame', 'every frame',], 
			columnAlign=[[1, 'left'],	[2, 'left']], 
			nrb=2 , cw = [[1,70],[2,120]] ,sl=1, cc = self.clearSideMode)		
		
		mc.separator(h=2,style='none')
		mc.radioButtonGrp( 'clearSideMode',l='clear frame : ', labelArray4=['left', 'right','mid','all'],
			columnAlign=[[1, 'left'],	[2, 'left'],	[3, 'left']], 
			nrb=4 , cw = [[1,70],[2,60],[3,60],[4,60]] ,en=0,sl=1)
		

		mc.nodeIconButton('clear',l='reset fcl key',h=50,c=self.clearFrame,
			st =  "iconAndTextVertical",i = 'deleteCache.png')

		mc.separator(h=10,style='none')
		mirrorColumn = mc.columnLayout('mirrorColumn',cal='left',adj=1)
		mc.separator(w=400,h=6,style ='singleDash')
		mc.separator(h=5,style='none')
		
		mc.checkBoxGrp( 'translateAx',l='Mirror Translate Axis :', labelArray3=['TX', 'TY', 'TZ'], 
			columnAlign=[[1, 'left'],	[2, 'left'],	[3, 'left']], va3 = [1,0,0],
			ncb=3,cw = [[1,110],[2,60],[3,60]] )
	
		mc.checkBoxGrp( 'rotateAx',l='Mirror Rotate Axis :', labelArray3=['RX', 'RY', 'RZ'], 
			columnAlign=[[1, 'left'],	[2, 'left'],	[3, 'left']],  va3 = [0,1,1],
			 ncb=3,cw = [[1,110],[2,60],[3,60]] )

		mc.checkBoxGrp( 'scaleAx',l='Mirror Scale Axis :', labelArray3=['SX', 'SY', 'SZ'], 
			columnAlign=[[1, 'left'],	[2, 'left'],	[3, 'left']],
			ncb=3,cw = [[1,110],[2,60],[3,60]] )

		mc.separator(h=2,style='none')
		
		mc.radioButtonGrp( 'mirrorFrameMode',l='Mode :', labelArray2=['single frame', 'every frame',], 
			columnAlign=[[1, 'left'],	[2, 'left']],sl=1, 
			nrb=2 , cw = [[1,70],[2,120]] ,cc =self.mirrorSideMode )


		mc.radioButtonGrp( 'mirrorSideMode',l='Side :', labelArray2=['Left to Right', 'Right to Left',], 
			columnAlign=[[1, 'left'],	[2, 'left']],sl=1, 
			nrb=2 , cw = [[1,70],[2,120]],en=0 )
		mc.separator(h=2,style='none')


		mc.separator(h=5,style='none')
		mc.nodeIconButton('mirfaceL',l='mirror fcl key ',h=50,
			c=self.mirFrame,st =  "iconAndTextVertical",i = 'flipU.png')
		mc.separator(h=5,style='none')
		mc.separator(h=10,style ='singleDash')
		mc.setParent(keyFrameTab)
		mc.separator(h=5,style ='none')

		mc.setParent(mirrorColumn)
		assettKey = mc.rowLayout('assetKey',nc=3)
		mc.nodeIconButton('loadAssetKey',l='load asset fcl key',h=50,w=167,
			st =  "iconAndTextVertical",i = 'openScript.png', c = self.load_fcl_key)
		mc.separator(hr=0,w=10,h=50,style = 'singleDash')
		mc.nodeIconButton('saveAssetKey',l='save asset fcl key',h=50,w=167,
			st =  "iconAndTextVertical",i = 'save.png', c= self.save_fcl_key)

		mc.setParent(mirrorColumn)
		mc.separator(h=10,style ='singleDash')
		mc.nodeIconButton('browseKeyAsset',l= 'Browse from Asset',st = 'iconAndTextHorizontal',
		i = 'zoom.png', c= self.load_key_from_file)

		mc.setParent(keyFrameTab)
		mc.separator(h=10,style ='none')

		# mocapX tab 
		mc.separator(h=14,style ='none',p=mainLayout)
		mc.setParent(tmpTabLayout)
		mc.columnLayout('mocapXTool',cal='center',co=['both',15],w=300,adj=1)
		mc.separator(h=8,style ='none')
		mc.text('mocapXcap',l='Create MocapX System',fn = 'boldLabelFont')
		
		# create mocapX tab
		mc.separator(h=8,style ='none')
		mc.button('createMocapX',l='Create MocapX System',w=150,h=35,c=self.create_mocapX)


		# create mocapX tab
		mc.separator(h=20,style ='singleDash')
		mc.text('mocapCheckUp',l='MocapX Realtime Check UP',fn = 'boldLabelFont')
		mc.separator(h=4,style ='none')
		mc.rowLayout('checkUpRow',nc = 3)
		mc.button('realtimeMocapX',l='MocapX check Realtime',w=168,h=35,c=self.check_realtime)
		mc.separator(w=4,style = 'none')
		mc.button('stopConnect',l='Turn off Realtime',w=168,h=35,c=self.disconnect_mocap)

		mc.setParent('mocapXTool')
		# create mocapX tab
		mc.separator(h=20,style ='singleDash')
		mc.text('recCheck',l='MocapX Recorded Check UP (mcpx)',fn = 'boldLabelFont')
		mc.separator(h=4,style = 'none')
		mc.button('fileMocapX',l='import mocapData from mcpx file',w=168,h=35,c=self.check_file)
		mc.separator(h=4,style ='none')
		mc.button('fbxMocapX',l='import mocapData from fbx file',w=150,h=35,c=self.check_fbx)
		
		mc.setParent('mocapXTool')
		# convert mocapX back to setup Tmp
		mc.separator(h=20,style ='singleDash')
		mc.text('Edit MocapX',l='Edit MocapX',fn = 'boldLabelFont')
		mc.separator(h=4,style ='none')
		mc.button('convert',l='Convert back to key frame',w=150,h=35,c=self.convert_back_to_key)

		self.adjColumn()
	
	def show(self):
		mc.showWindow(self.winName)



def show():
	x = mocapXRig()
	x.showMocapXRigUI()
	x.show()
	return x