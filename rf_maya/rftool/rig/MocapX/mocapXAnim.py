import maya.cmds as mc
import pymel.core as pmc
import customMocapX as cmx
reload(cmx)


class mocapXAnim():
	def __init__(self):
		self.winName = 'mocapXAnim'
		self.poseList = cmx.poseList
		self.eyePoseRotList =cmx.eyePoseRotList
		self.headPoseList=cmx.headPoseList
	def getNamespace(self,*args):
		ns = pmc.selected()[0].namespace()

		mc.textField('ns',w=20,e=1,tx=ns,bgc=[.5,1,.5])
		self.ctrlList = cmx.getAttrFromCtrlListAttr(ns,'ctrlList')
		self.eyeRotCtrl = cmx.getAttrFromCtrlListAttr(ns,'eyeCtrlList')
		self.headCtrl = cmx.getAttrFromCtrlListAttr(ns,'headCtrlList')
		self.MocapX = ns+'MocapX'

	def selCtrlList(self,*args):
		mc.select(self.ctrlList)
		

	def disConHead(self,*args):
		#MocapX = cmx.getMocapX()
		cmx.disconnectHead(self.MocapX,self.headPoseList,self.headCtrl)
		for ctrl in self.headCtrl:
			cmx.setZero([ctrl])
	
	def conHead(self,*args):
		#MocapX = cmx.getMocapX()
		cmx.connectHead(self.MocapX,self.headPoseList,self.headCtrl)


	def disConEye(self,*args):
		#MocapX = cmx.getMocapX()
		cmx.disconnectEye(self.MocapX,self.eyePoseRotList,self.eyeRotCtrl)
		cmx.setZero(self.eyeRotCtrl)

	def conEye(self,*args):
		#MocapX = cmx.getMocapX()
		cmx.connectEye(self.MocapX,self.eyePoseRotList,self.eyeRotCtrl)	
	

	def MocapXAnimUI(self):
		ui = mc.window(self.winName,title = 'mocapXAnim')
		mainLayout = mc.columnLayout('mainCol',cal='center',co=['both',28],adj=1)
		mc.separator(h=10,style ='none')
		mc.text('Please fill namespace of character.')
		mc.separator(h=5,style ='none')
		#mc.rowLayout('nsRow',nc=3)
		#mc.text('nsText',l='namespace:  ')
		nsTxField =mc.textField('ns',w=20,ed=0,bgc=[1,.5,.5])
		#mc.setParent(mainLayout)
		mc.separator(h=5,style ='none')
		mc.button('getNS',l='get namespace',c = self.getNamespace)
		mc.separator(h=10,style ='none')
		mc.text('warning',l='Please check namespace')
		mc.text('warning2',l='before prese button.')
		mc.separator(h=8,style ='none')
		mc.button('selCtrlList',l='select all control',w=120,h=60,c=self.selCtrlList)
		mc.separator(h=10,style ='none')
		mc.button('conHead',l='connectHead',w=120,h=30,c= self.conHead)
		mc.separator(h=1,style ='none')
		mc.button('disConHead',l='disconnectHead',w=120,h=30,c= self.disConHead)
		mc.separator(h=10,style ='none')
		mc.button('conEye',l='connectEye',w=120,h=30,c= self.conEye)
		mc.separator(h=1,style ='none')
		mc.button('disConEye',l='disconnectEye',w=120,h=30,c= self.disConEye)
		mc.separator(h=14,style ='none')


	def showUI(self):
		if mc.window(self.winName,exists=1):
			print 'delUI'
			mc.deleteUI(self.winName)
		self.MocapXAnimUI()
		mc.showWindow(self.winName)			
#y = mcxa.mocapXAnim()
#y.showMocapXAnimUI()