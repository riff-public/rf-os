import maya.cmds as mc
import maya.mel as mel
import pymel.core as pmc
import customMocapX as cmx
reload(cmx)
##### facial pose Part ##############
#ctrlList,eyeRotCtrl,headCtrl = createListFromDir()
#createCtrlListAttr()
#createFclKey(ctrlList)
#importMocapXTmp()
#selectAllCtrl()
#mirAllFrame(2,ctrlList)

class mocapXRig():
	def __init__(self):
		self.winName = 'mocapXRig'
		self.poseLibName = 'PoseLib'
		self.collectionName = 'AttributeCollection'
		self.poseList = cmx.poseList
		self.eyePoseRotList =cmx.eyePoseRotList
		self.headPoseList=cmx.headPoseList

	def getNamespace(self,*args):
		self.ns = pmc.selected()[0].namespace()
		mc.textField('ns',e=1,tx=self.ns,bgc=[.5,1,.5])
		self.ctrlList = cmx.getAttrFromCtrlListAttr(self.ns,'ctrlList')
		self.eyeCtrlList = cmx.getAttrFromCtrlListAttr(self.ns,'eyeCtrlList')
		self.headCtrlList = cmx.getAttrFromCtrlListAttr(self.ns,'headCtrlList')
		self.MocapX = self.ns+'MocapX'
	

	def selCtrlList(self,*args):
		if mc.ls(sl=True) != []:
			cmx.selectAllCtrl(self.ns)
		else:
			mc.confirmDialog(m='please select something from rig')

	def createMocapXSys(self,*args):
		cmx.createMocapXSystem(self.poseLibName,self.collectionName,self.poseList,'',self.ctrlList)
		cmx.multipleUpdatePose(self.ctrlList,self.poseList)
		cmx.deleteKey(self.ctrlList)

	def connectPose(self,*args):
		cmx.connectAll(self.ns,'MocapX')


	def createMocapTmp(self,*args):
		cmx.createFclKey(self.ctrlList)
		cmx.importMocapXTmp()

	def createMocapXDevice(self,*args):
		cmx.createMocapXDevice()

	def clearFrame(self,lTor):
		cmx.resetFacePostFrame(self.ctrlList,lTor)

	def resetFaceL(self,*args):
		self.clearFrame(0)
	def resetFaceR(self,*args):
		self.clearFrame(1)
	def resetFaceMid(self,*args):
		self.clearFrame(2)
	def resetFaceAll(self,*args):
		self.clearFrame(3)


	def convert_back_to_key(self,*args):
		cmx.convert_back_to_key(self.ns)

	###################### First Layout ########################################
	##### Create CtrlList Function ######
	def createCtrlListAttr(self,*args):
		cmx.createCtrlListAttr(self.ns,'ctrlList')
		self.ctrlList = cmx.getAttrFromCtrlListAttr(self.ns,'ctrlList')
	def addCtrlList(self,*args):
		cmx.modCtrlList(self.ns,'ctrlList','add')
		self.ctrlList = cmx.getAttrFromCtrlListAttr(self.ns,'ctrlList')
	def removeCtrlList(self,*args):
		cmx.modCtrlList(self.ns,'ctrlList','remove')
		self.ctrlList = cmx.getAttrFromCtrlListAttr(self.ns,'ctrlList')
	
	###### Create HeadCtrlList Function ####
	def createHeadCtrlListAttr(self,*args):
		cmx.createCtrlListAttr(self.ns,'headCtrlList')
		self.headCtrlList = cmx.getAttrFromCtrlListAttr(self.ns,'headCtrlList')
	def addHeadCtrlList(self,*args):
		cmx.modCtrlList(self.ns,'headCtrlList','add')
		self.headCtrlList = cmx.getAttrFromCtrlListAttr(self.ns,'headCtrlList')
	def removeHeadCtrlList(self,*args):
		cmx.modCtrlList(self.ns,'headCtrlList','remove')
		self.headCtrlList = cmx.getAttrFromCtrlListAttr(self.ns,'headCtrlList')
	
	################## Create EyeCtrlList Function ##############################
	def createEyeCtrlListAttr(self,*args):
		cmx.createCtrlListAttr(self.ns,'eyeCtrlList')
		self.eyeCtrlList = cmx.getAttrFromCtrlListAttr(self.ns,'eyeCtrlList')
	def addEyeCtrlList(self,*args):
		cmx.modCtrlList(self.ns,'eyeCtrlList','add')
		self.eyeCtrlList = cmx.getAttrFromCtrlListAttr(self.ns,'eyeCtrlList')
	def removeEyeCtrlList(self,*args):
		cmx.modCtrlList(self.ns,'eyeCtrlList','remove')
		self.eyeCtrlList = cmx.getAttrFromCtrlListAttr(self.ns,'eyeCtrlList')

	
	################## Create MidCtrlList Function ##############################
	def createMidCtrlListAttr(self,*args):
		cmx.createCtrlListAttr(self.ns,'midCtrlList')
		self.midCtrlList = cmx.getAttrFromCtrlListAttr(self.ns,'midCtrlList')
	def addMidCtrlList(self,*args):
		cmx.modCtrlList(self.ns,'midCtrlList','add')
		self.midCtrlList = cmx.getAttrFromCtrlListAttr(self.ns,'midCtrlList')
	def removeMidCtrlList(self,*args):
		cmx.modCtrlList(self.ns,'midCtrlList','remove')
		self.midCtrlList = cmx.getAttrFromCtrlListAttr(self.ns,'midCtrlList')


	
	################## Create LeftCtrlList Function ##############################
	def createLeftCtrlListAttr(self,*args):
		cmx.createCtrlListAttr(self.ns,'leftCtrlList')
		self.leftCtrlList = cmx.getAttrFromCtrlListAttr(self.ns,'leftCtrlList')
	def addLeftCtrlList(self,*args):
		cmx.modCtrlList(self.ns,'leftCtrlList','add')
		self.leftCtrlList = cmx.getAttrFromCtrlListAttr(self.ns,'leftCtrlList')		
	def removeLeftCtrlList(self,*args):
		cmx.modCtrlList(self.ns,'leftCtrlList','remove')
		self.leftCtrlList = cmx.getAttrFromCtrlListAttr(self.ns,'leftCtrlList')		


	################## Create RightCtrlList Function ##############################
	def createRightCtrlListAttr(self,*args):
		cmx.createCtrlListAttr(self.ns,'rightCtrlList')
		self.rightCtrlList = cmx.getAttrFromCtrlListAttr(self.ns,'rightCtrlList')		
	def addRightCtrlList(self,*args):
		cmx.modCtrlList(self.ns,'rightCtrlList','add')
		self.rightCtrlList = cmx.getAttrFromCtrlListAttr(self.ns,'rightCtrlList')
	def removeRightCtrlList(self,*args):
		cmx.modCtrlList(self.ns,'rightCtrlList','remove')
		self.rightCtrlList = cmx.getAttrFromCtrlListAttr(self.ns,'rightCtrlList')



	################## SecondLayout Function ################################################
	#################### renumber ScrollList #################################
	def renum(self,scrollName,ctrlList):

		scrollList = []
		for number , ctrl in enumerate(ctrlList):
			noNs = ctrl.split(self.ns)[-1]
			ctrl = '{}   :   {}'.format(number,noNs)
			scrollList.append(ctrl)
		mc.textScrollList(scrollName,e=1,removeAll=1)
		mc.textScrollList(scrollName,e=1,append = scrollList)

	def renumFromScrollName(self,scrollName):
		scrollList = mc.textScrollList(scrollName,q=1,ai=1)
		newList = []
		for i in scrollList:
			ctrl = i.split('   :   ')[-1]
			newList.append(ctrl)
		self.renum(scrollName,newList)
	
	#################### reload ScrollList ##################################################
	def reloadList(self,ctrlListAttr):
		getCtrlList = cmx.getAttrFromCtrlListAttr(self.ns,ctrlListAttr)
		scrollList = []
		for number , ctrl in enumerate(getCtrlList):
			noNs = ctrl.split(self.ns)[-1]
			ctrl = '{}   :   {}'.format(number,noNs)
			scrollList.append(ctrl)
		return scrollList

	def reloadLeft(self,*args):
		scrollList = self.reloadList('leftCtrlList')
		mc.textScrollList('leftCtrl',e=1,removeAll=1)
		mc.textScrollList('leftCtrl',e=1,append = scrollList)

	def reloadRight(self,*args):
		scrollList = self.reloadList('rightCtrlList')
		mc.textScrollList('rightCtrl',e=1,removeAll=1)
		mc.textScrollList('rightCtrl',e=1,append = scrollList)

	def reloadMid(self,*args):
		scrollList = self.reloadList('midCtrlList')
		mc.textScrollList('midCtrl',e=1,removeAll=1)
		mc.textScrollList('midCtrl',e=1,append = scrollList)


	################### update ScrollList ####################################################
	def updateLeft(self,*args):
		textList = mc.textScrollList('leftCtrl',q=1,ai=1)
		ctrlList = [i.split('   :   ')[-1] for i in textList]
		ctrlStr = ','.join(ctrlList)
		geoGrp = '{}Geo_Grp'.format(self.ns)
		mc.setAttr(geoGrp+'.leftCtrlList',ctrlStr,type='string')
		self.leftCtrlList = cmx.getAttrFromCtrlListAttr(self.ns,'leftCtrlList')

	def updateRight(self,*args):
		textList = mc.textScrollList('rightCtrl',q=1,ai=1)
		ctrlList = [i.split('   :   ')[-1] for i in textList]
		ctrlStr = ','.join(ctrlList)
		geoGrp = '{}Geo_Grp'.format(self.ns)
		mc.setAttr(geoGrp+'.rightCtrlList',ctrlStr,type='string')
		self.rightCtrlList = cmx.getAttrFromCtrlListAttr(self.ns,'rightCtrlList')

	def updateMid(self,*args):
		textList = mc.textScrollList('midCtrl',q=1,ai=1)
		ctrlList = [i.split('   :   ')[-1] for i in textList]
		ctrlStr = ','.join(ctrlList)
		geoGrp = '{}Geo_Grp'.format(self.ns)
		mc.setAttr(geoGrp+'.midCtrlList',ctrlStr,type='string')
		self.midCtrlList = cmx.getAttrFromCtrlListAttr(self.ns,'midCtrlList')
	

	################### swap Left Right ######################################################
	def toRight(self,*args):
		strList = mc.textScrollList('leftCtrl',q=1,si=1)
		for selected in strList:
			nowLen = mc.textScrollList('rightCtrl',q=1,ni=1)
			selStr = selected.split('   :   ')[-1]
			addStr = '{}   :   {}'.format(nowLen+1,selStr)
			mc.textScrollList('rightCtrl',e=1,append = addStr)
			mc.textScrollList('leftCtrl',e=1,ri = selected)

		textList = mc.textScrollList('leftCtrl',q=1,ai=1)
		if textList !=None:
			ctrlList = [i.split('   :   ')[-1] for i in textList]
			self.renum('leftCtrl',ctrlList)

	def toLeft(self,*args):
		strList = mc.textScrollList('rightCtrl',q=1,si=1)
		for selected in strList:
			nowLen = mc.textScrollList('leftCtrl',q=1,ni=1)
			selStr = selected.split('   :   ')[-1]
			addStr = '{}   :   {}'.format(nowLen+1,selStr)
			mc.textScrollList('leftCtrl',e=1,append = addStr)
			mc.textScrollList('rightCtrl',e=1,ri = selected)
		
		textList = mc.textScrollList('rightCtrl',q=1,ai=1)
		if textList !=None:
			ctrlList = [i.split('   :   ')[-1] for i in textList]
			self.renum('rightCtrl',ctrlList)

	################### sort ScrollList ######################################################
	def sortScrollList(self,scrollName):
		textList = mc.textScrollList(scrollName,q=1,ai=1)
		ctrlList = [i.split('   :   ')[-1] for i in textList]
		scrollList = []
		for number , ctrl in enumerate(sorted(ctrlList)):
			noNs = ctrl.split(self.ns)[-1]
			ctrl = '{}   :   {}'.format(number,noNs)
			scrollList.append(ctrl)
		textList = mc.textScrollList(scrollName,e=1,removeAll=1)
		textList = mc.textScrollList(scrollName,e=1,a=scrollList)

	def sortLeft(self,*args):
		self.sortScrollList('leftCtrl')

	def sortRight(self,*args):
		self.sortScrollList('rightCtrl')

	def sortMid(self,*args):
		self.sortScrollList('midCtrl')

	################### updown ScrollList ####################################################
	def upScroll(self,scrollName):
		intList = mc.textScrollList(scrollName,q=1,sii=1)
		strList = mc.textScrollList(scrollName,q=1,si=1)
		selList = []
		for num in range(len(strList)):
			idx = intList[num]
			if 1 not  in intList:
				if idx  >= 2:
					mc.textScrollList(scrollName,e=1,rii=idx)
					mc.textScrollList(scrollName,e=1,ap=[idx-1,strList[num]])
					selList.append(idx-1)

				else:
					selList.append(idx)
					pass

				self.renumFromScrollName(scrollName)
				mc.textScrollList(scrollName,e=1,sii=selList)
			else:
				mc.textScrollList(scrollName,e=1,sii = intList)
	
	def upLeft(self,*args):
		self.upScroll('leftCtrl')

	def upRight(self,*args):
		self.upScroll('rightCtrl')

	def upMid(self,*args):
		self.upScroll('midCtrl')

	def downScroll(self,scrollName):
		intList = mc.textScrollList(scrollName,q=1,sii=1)
		strList = mc.textScrollList(scrollName,q=1,si=1)
		maxNum = mc.textScrollList(scrollName,q=1,ni=1)
		selList = []

		for num in range(len(strList))[::-1]:
			idx = intList[num]
			if maxNum not in intList:
				if idx  <= maxNum-1:
					print 'idx : {} < strList : {}'.format(idx+1,strList[num])
					mc.textScrollList(scrollName,e=1,rii=idx)
					mc.textScrollList(scrollName,e=1,ap=[idx+1,strList[num]])
					selList.append(idx+1)
				else:
					print 'there something in this case : idx: {} and strList :{}'.format(idx+1,strList[num])
					selList.append(idx)
				print selList

				self.renumFromScrollName(scrollName)

				mc.textScrollList(scrollName,e=1,sii=selList)
			else:
				mc.textScrollList(scrollName,e=1,sii = intList)

	def downLeft(self,*args):
		self.downScroll('leftCtrl')

	def downRight(self,*args):
		self.downScroll('rightCtrl')

	def downMid(self,*args):
		self.downScroll('midCtrl')



	################### add / remove #########################################################
	def addCtrl(self,scrollName):
		sel = mc.ls(sl=True)
		if sel != []:
			for i in sel:
				ctrl = i.split(self.ns)[-1]
				mc.textScrollList(scrollName,e=1,a=ctrl)
			self.renumFromScrollName(scrollName)

	def addLeft(self,*args):
		self.addCtrl('leftCtrl')
	

	def addRight(self,*args):
		self.addCtrl('rightCtrl')
	

	def addMid(self,*args):
		self.addCtrl('midCtrl')


	def removeCtrl(self,scrollName):
		intList = mc.textScrollList(scrollName,q=1,sii=1)
		if intList != None:
			for idx in intList[::-1]:
				mc.textScrollList(scrollName,e=1,rii=idx)
		if mc.textScrollList(scrollName,q=1,ai=1) != None:
			self.renumFromScrollName(scrollName)
			mc.textScrollList(scrollName,e=1,sii=intList[0]-1)
	def remLeft(self,*args):
		self.removeCtrl('leftCtrl')

	def remRight(self,*args):
		self.removeCtrl('rightCtrl')	

	def remMid(self,*args):
		self.removeCtrl('midCtrl')

	################### select Command #########################################################

	def selectFromScroll(self,*args):
		leftList = mc.textScrollList('leftCtrl',q=1,si=1)
		rightList = mc.textScrollList('rightCtrl',q=1,si=1)
		midList = mc.textScrollList('midCtrl',q=1,si=1)
		if leftList == None:
			leftList = []
		
		if rightList == None:
			rightList = []			
		
		if midList == None:
			midList = []
		selList = []
		itemList = list(set(leftList) | set(rightList) | set(midList))
		if itemList != None:
			for i in itemList:
				ctrlName = i.split('   :   ')[-1]
				ctrl = '{}{}'.format(self.ns,ctrlName)
				selList.append(ctrl)
			mc.select(selList)


	##################### mirror Face Frame #################################################

	def mirAllFrameL(self,*args):
		cmx.mirAllFrame(self.ns,2,self.ctrlList,rTol=0)
	def mirAllFrameR(self,*args):
		cmx.mirAllFrame(self.ns,22,self.ctrlList,rTol=1)


	##################### pair Face Frame ###################################################
	def pairScroll(self,oriScrollName,targetScrollName):
		oriIdxList = mc.textScrollList(oriScrollName,q=1,sii=1)
		tarIdxList = mc.textScrollList(targetScrollName,q=1,sii=1)
		tarStrList = mc.textScrollList(targetScrollName,q=1,si=1)
		if len(oriIdxList)==1 and len(tarIdxList)==1:
			mc.textScrollList(targetScrollName,e=1,rii=tarIdxList[0])
			mc.textScrollList(targetScrollName,e=1,ap=[oriIdxList[0],tarStrList[0]])
			self.renumFromScrollName(targetScrollName)
			mc.textScrollList(targetScrollName,e=1,sii=oriIdxList[0])
		else:
			mc.confirmDialog(m='please select only one item from both scrollList.')

	def pairLToR(self,*args):
		self.pairScroll('leftCtrl','rightCtrl')


	def pairRToL(self,*args):
		self.pairScroll('rightCtrl','leftCtrl')
	
	##################### find Item ###########################################################
	def findItem(self,ScrollName):
		sel = mc.ls(sl=True)
		strList = []
		deSelList = []
		referList = mc.textScrollList(ScrollName,q=1,ai=1)

		for i in sel:
			ctrl = '   :   '+i.split(self.ns)[-1]
			for lists in referList:
				if ctrl in lists:
					strList.append(lists)
				#else:
					#deSelList.append(i)
		#print deSelList
		#mc.select(deSelList,d=1)
		mc.textScrollList(ScrollName,e=1,si=strList)

	def findLeft(self,*args):
		self.findItem('leftCtrl')

	def findRight(self,*args):
		self.findItem('rightCtrl')

	def findMid(self,*args):
		self.findItem('midCtrl')

	############## adj Column ####################################
	def adjColumn(self,*args):
		tabIdx = mc.tabLayout('tmpSetupTab',q=1,sti=1)
		
		if tabIdx ==1:
			mc.tabLayout('tmpSetupTab',e=1,w=320)
			mc.columnLayout('mainCol',e=1,w=350)
			mc.window(self.winName,e=1,w=350)
		elif tabIdx ==2:
			mc.tabLayout('tmpSetupTab',e=1,w=720)
			mc.columnLayout('mainCol',e=1,w=730)
			mc.window(self.winName,e=1,w=730)
		elif tabIdx ==3:
			mc.tabLayout('tmpSetupTab',e=1,w=300)
			mc.columnLayout('mainCol',e=1,w=330)
			mc.window(self.winName,e=1,w=330)

	############################# UI Layout Part ################################################
	def showMocapXRigUI(self):
		if mc.window(self.winName,exists=1):
			#print 'delUI'
			mc.deleteUI(self.winName)

		ui = mc.window(self.winName,title = 'mocapXRig',w=350)

		######### first layout ############################################################
		###################################################################################
		###################################################################################
		
		mainLayout = mc.columnLayout('mainCol',cal='center',co=['both',15],w=350,adj=1)
		nsColumn = mc.columnLayout('nsColumn',cal='center',adj=1)
		mc.separator(h=10,style ='none')
		mc.text('Please fill namespace of character.')
		nsTxField =mc.textField('ns',w=200,ed=0,bgc=[1,.5,.5])
		mc.separator(h=5,style ='none')
		mc.button('getNS',l='get namespace',c = self.getNamespace)
		

		mc.setParent(mainLayout)
		mc.separator(h=14,style ='none')
		tmpTabLayout = mc.tabLayout('tmpSetupTab',p=mainLayout,cr=1,sc=self.adjColumn,w=320)

		tmpSetupAnimColumn= mc.columnLayout('tmpSetup',cal='center',co=['both',15] ,adj=1)
		mc.separator(h=15,style='none')
		mc.text('tmpSetUpText',l='Set up controller attribute for further process.')
		mc.text('tmpSetUpText03',l='Select Controller and assgin Attribute to each part.')
		mc.separator(h=5,style = 'none')
		mc.separator(h=10,style = 'singleDash',w=120)
		mc.separator(h=10,style = 'none')
		mc.text('tmpSetUpText01',l='Controller Attribute for connecting with MocapX.')
		mc.separator(h=10,style = 'none')
		#tmpSetUpRow = mc.rowLayout('tmpSetupRow',nc=3)



		tmpSetupColumn= mc.columnLayout('tmpSetup',cal='center',adj=1)		
		#ctrlListRow = mc.rowLayout('ctrlListRow',nc=3,p=tmpSetupColumn)
		mc.columnLayout('ctrlListColumn',cal='center',adj=1)
		mc.button('createCtrlList',l='allCtrlList',w=100,h=35,c= self.createCtrlListAttr)	
		#mc.setParent(ctrlListRow)
		#mc.separator(w=2,style='none')
		#mc.setParent(ctrlListRow)
		mc.columnLayout('ctrlListAdd/Remove',cal='center')
		#mc.separator(h=6,style ='none')
		#mc.button('add',l='add',w=70,h=25,c= self.addCtrlList)
		#mc.separator(h=5,style ='none')
		#mc.button('remove',l='remove',w=70,h=25,c=self.removeCtrlList)
		#mc.separator(h=8,style ='none')
		mc.setParent(tmpSetupColumn)
		mc.separator(h=10,style ='singleDash',w=100)




		HeadCtrlListRow = mc.rowLayout('HeadCtrlListRow',nc=3,p=tmpSetupColumn,adj=1)
		mc.columnLayout('HeadCtrlListColumn',cal='center',adj=1)
		mc.button('createHeadCtrlList',l='HeadCtrlList',w=100,h=35,c= self.createHeadCtrlListAttr)		
		mc.setParent(HeadCtrlListRow)
		mc.separator(w=2,style='none')
		mc.setParent(HeadCtrlListRow)
		mc.columnLayout('HeadCtrlListAdd/Remove',cal='center',adj=1)

		mc.separator(h=6,style ='none')
		#mc.button('addHeadCtrlList',l='add',w=70,h=25,c= self.addHeadCtrlList)
		#mc.separator(h=5,style ='none')
		#mc.button('removeHeadCtrlList',l='remove',w=70,h=25,c= self.removeHeadCtrlList)
		mc.separator(h=8,style ='none')
		mc.setParent(tmpSetupColumn)
		mc.separator(h=10,style ='singleDash',w=100)



		EyeCtrlListRow = mc.rowLayout('EyeCtrlListRow',nc=3,p=tmpSetupColumn,adj=1)
		mc.columnLayout('EyeCtrlListColumn',cal='center',adj=1)

		mc.button('createEyeCtrlList',l='EyeCtrlList',w=100,h=35,c= self.createEyeCtrlListAttr)		
		mc.setParent(EyeCtrlListRow)
		mc.separator(w=2,style='none')
		mc.setParent(EyeCtrlListRow)
		mc.columnLayout('EyeCtrlListAdd/Remove',cal='center',adj=1)

		mc.separator(h=6,style ='none')
		#mc.button('addEyeCtrlList',l='add',w=70,h=25,c= self.addEyeCtrlList)
		#mc.separator(h=5,style ='none')
		#mc.button('removeEyeCtrlList',l='remove',w=70,h=25,c= self.removeEyeCtrlList)
		mc.separator(h=8,style ='none')
		mc.setParent(tmpSetupColumn)
		mc.separator(h=10,style ='singleDash',w=120)


		#mc.setParent(tmpSetUpRow)
		#mc.separator(h=250,w=20,style ='single')
		#mc.setParent(tmpSetUpRow)



		#tmpSetUpRigColumn = mc.columnLayout('tmpSetupRig',cal='center',p=tmpSetUpRow)
		mc.separator(h=10,style = 'none')
		mc.text('tmpSetUpText02',l='Controller Attribute for set up facial pose.')
		mc.separator(h=10,style = 'none')
		MidCtrlListRow = mc.rowLayout('MidCtrlListRow',nc=3,p=tmpSetupColumn,adj=1)
		mc.columnLayout('MidCtrlListColumn',cal='center',adj=1)

		mc.button('createMidCtrlList',l='MidCtrlList',w=100,h=35,c= self.createMidCtrlListAttr)		
		mc.setParent(MidCtrlListRow)
		mc.separator(w=2,style='none')
		mc.setParent(MidCtrlListRow)
		mc.columnLayout('MidCtrlListAdd/Remove',cal='center')

		mc.separator(h=6,style ='none')
		#mc.button('addMidCtrlList',l='add',w=70,h=25,c= self.addMidCtrlList)
		#mc.separator(h=5,style ='none')
		#mc.button('removeMidCtrlList',l='remove',w=70,h=25,c= self.removeMidCtrlList)
		mc.separator(h=8,style ='none')
		mc.setParent(tmpSetupColumn)
		mc.separator(h=10,style ='singleDash',w=100)



		LeftCtrlListRow = mc.rowLayout('LeftCtrlListRow',nc=3,p=tmpSetupColumn,adj=1)
		mc.columnLayout('LeftCtrlListColumn',cal='center',adj=1)

		mc.button('createLeftCtrlList',l='LeftCtrlList',w=100,h=35,c= self.createLeftCtrlListAttr)		
		mc.setParent(LeftCtrlListRow)
		mc.separator(w=2,style='none')
		mc.setParent(LeftCtrlListRow)
		mc.columnLayout('LeftCtrlListAdd/Remove',cal='center',adj=1)

		mc.separator(h=6,style ='none')
		#mc.button('addLeftCtrlList',l='add',w=70,h=25,c= self.addLeftCtrlList)
		#mc.separator(h=5,style ='none')
		#mc.button('removeLeftCtrlList',l='remove',w=70,h=25,c= self.removeLeftCtrlList)
		mc.separator(h=8,style ='none')
		mc.setParent(tmpSetupColumn)
		mc.separator(h=10,style ='singleDash',w=100)




		RightCtrlListRow = mc.rowLayout('RightCtrlListRow',nc=3,p=tmpSetupColumn,adj=1)
		mc.columnLayout('RightCtrlListColumn',cal='center',adj=1)
		mc.button('createRightCtrlList',l='RightCtrlList',w=100,h=35,c= self.createRightCtrlListAttr)		
		mc.setParent(RightCtrlListRow)
		mc.separator(w=2,style='none')
		mc.setParent(RightCtrlListRow)
		mc.columnLayout('RightCtrlListAdd/Remove',cal='center')
		mc.separator(h=6,style ='none')
		#mc.button('addRightCtrlList',l='add',w=70,h=25,c= self.addRightCtrlList)
		#mc.separator(h=5,style ='none')
		#mc.button('removeRightCtrlList',l='remove',w=70,h=25,c= self.removeRightCtrlList)
		mc.separator(h=8,style ='none')
		mc.setParent(tmpSetupColumn)
		mc.separator(h=10,style ='singleDash',w=100)

		mc.separator(h=8,style ='none')
		mc.setParent(tmpSetupColumn)
		mc.separator(h=5,style ='none')
		mc.button('selAllCtrl',l='select all Control',h=35,w=150,c=self.selCtrlList)
		mc.separator(h=25,style ='none')
		mc.button('createMCXTmp',l='MocapX Tmp',w=150,h=35,c=self.createMocapTmp)
		mc.separator(h=5,style ='none')
		

		######### second layout ############################################################
		###################################################################################
		###################################################################################
		###################################################################################

		mc.setParent(tmpTabLayout)
		mirrorTool = mc.columnLayout('mirrorTool',cal='center',co=['both',15],w=300,adj=1)
		mirRowLayout = mc.rowLayout(numberOfColumns=7)
		mc.separator(h=12,style='none')


		##### left CtrlColum #####################################################################
		leftCtrlColumn = mc.columnLayout('leftCtrlColumn',cal='center',w=200,adj=1)
		mc.separator(h=6,style ='none')
		mc.text('leftHead', l='Left Ctrl')
		mc.separator(h=3,style ='none')
		leftAddRemoveColumn = mc.columnLayout('leftAddRemoveColumn',cal='center',adj=1)
		leftAddRemoveRow = mc.rowLayout('leftAddRemoveRow',nc=2,w=180)
		mc.button('leftAddButton',l='add',w=100,c=self.addLeft)
		mc.button('leftRemoveAddButton',l='remove',w=100,c=self.remLeft)
		mc.setParent(leftCtrlColumn)
		mc.button('leftFind',l='Find',w=100,c=self.findLeft)
		mc.separator(h=3,style ='none')
		mc.textScrollList('leftCtrl',w=180,h=250,allowMultiSelection=1,shi=2,sc=self.selectFromScroll)
		#mc.textScrollList('leftCtrl',e=1,append = ['test6','test5','test4','test3','test2'])
		
		leftOrderColumn = mc.columnLayout('leftOrderColumn',cal='center',adj=1)
		leftOrderRow = mc.rowLayout('leftOrderRow',nc=2,w=200)
		mc.button('leftUp',l=u"\u25B2",w=100,h=18,c=self.upLeft)
		mc.button('leftDown',l=u"\u25BC",w=100,h=18,c=self.downLeft)
		mc.setParent(leftCtrlColumn)
		mc.button('leftSort',l='Sort',w=100,c=self.sortLeft)
		mc.separator(h=2,style='none')
		lCtrlPair = mc.button('leftPair',l='Pair',w=75,c=self.pairLToR)			
		


		mc.separator(h=10,style='none')
		mc.setParent(leftCtrlColumn)
		leftReload = mc.button('leftReload',l='Reload',w=75,c=self.reloadLeft)
		mc.separator(h=1,style='none')
		lCtrlUpdate = mc.button('leftUpdate',l='Update',w=75,c=self.updateLeft)
		mc.separator(h=1,style='none')


		mc.setParent(mirRowLayout)
		mc.columnLayout('swapColumn2',cal='center',w=28,adj=1,co=['both',1])
		mc.button('toRight',l='>',w=20,h=60,c=self.toRight)
		mc.separator(style='none',h=30)
		mc.button('toLeft',l='<',w=20,h=60,c=self.toLeft)
		mc.setParent(mirRowLayout)
		



		##### right CtrlColum #####################################################################
		rightCtrlColumn = mc.columnLayout('rightCtrlColumn',cal='center',w=200,adj=1)
		mc.separator(h=6,style ='none')
		mc.text('rightHead', l='Right Ctrl')
		mc.separator(h=3,style ='none')
		rightAddRemoveColumn = mc.columnLayout('rightAddRemoveColumn',cal='center',adj=1)
		rightAddRemoveRow = mc.rowLayout('rightAddRemoveRow',nc=2,w=180)
		mc.button('rightAddButton',l='add',w=100,c=self.addRight)
		mc.button('rightRemoveAddButton',l='remove',w=100,c=self.remRight)
		mc.setParent(rightCtrlColumn)
		mc.button('rightFind',l='Find',w=100,c=self.findRight)
		mc.separator(h=3,style ='none')
		mc.textScrollList('rightCtrl',w=180,h=250,allowMultiSelection=1,shi=1,sc=self.selectFromScroll)
		#mc.textScrollList('rightCtrl',e=1,append = ['test6','test5','test4','test3','test2'])
		
		rightOrderColumn = mc.columnLayout('rightOrderColumn',cal='center',adj=1)
		rightOrderRow = mc.rowLayout('rightOrderRow',nc=2,w=200)
		mc.button('rightUp',l=u"\u25B2",w=100,h=18,c=self.upRight)
		mc.button('rightDown',l=u"\u25BC",w=100,h=18,c=self.downRight)
		mc.setParent(rightCtrlColumn)
		
		mc.button('rightSort',l='Sort',w=100,c=self.sortRight)
		mc.separator(h=2,style='none')
		rCtrlPair = mc.button('rightPair',l='Pair',w=75,c=self.pairRToL)	
		
		mc.separator(h=10,style='none')
		mc.setParent(rightCtrlColumn)
		rightReload = mc.button('rightReload',l='Reload',w=75,c=self.reloadRight)
		mc.separator(h=1,style='none')
		rCtrlUpdate = mc.button('rightUpdate',l='Update',w=75,c=self.updateRight)
		mc.setParent(mirRowLayout)
		mc.separator(w=30,style='none',h=30)

	

		##### mid CtrlColum #####################################################################
		mc.setParent(mirRowLayout)
		midCtrlColumn = mc.columnLayout('midCtrlColumn',cal='center',w=200,adj=1)
		mc.separator(h=6,style ='none')
		mc.text('midHead', l='Mid Ctrl')
		mc.separator(h=3,style ='none')
		midAddRemoveColumn = mc.columnLayout('midAddRemoveColumn',cal='center',adj=1)
		midAddRemoveRow = mc.rowLayout('midAddRemoveRow',nc=2,w=180)
		mc.button('midAddButton',l='add',w=100,c=self.addMid)
		mc.button('midRemoveAddButton',l='remove',w=100,c=self.remMid)
		mc.setParent(midCtrlColumn)
		mc.button('midFind',l='Find',w=100,c=self.findMid)
		mc.separator(h=3,style ='none')
		mc.textScrollList('midCtrl',w=180,h=275,allowMultiSelection=1,shi=1,sc=self.selectFromScroll)
		#mc.textScrollList('midCtrl',e=1,append = ['test6','test5','test4','test3','test2'])
		
		midOrderColumn = mc.columnLayout('midOrderColumn',cal='center',adj=1)
		midOrderRow = mc.rowLayout('midOrderRow',nc=2,w=200)
		mc.button('midUp',l=u"\u25B2",w=100,h=18,c=self.upMid)
		mc.button('midDown',l=u"\u25BC",w=100,h=18,c=self.downMid)
		mc.setParent(midCtrlColumn)
		mc.button('midSort',l='Sort',w=100,c=self.sortMid)
		mc.separator(h=10,style='none')
		mc.setParent(midCtrlColumn)
		midReload = mc.button('midReload',l='Reload',w=75,c=self.reloadMid)
		mc.separator(h=1,style='none')
		midCtrlUpdate = mc.button('midUpdate',l='Update',w=75,c=self.updateMid)

		########## Foot ########################################

		mc.setParent(mirRowLayout)
		mc.separator(h=8,style='none')
		mc.setParent('mirrorTool')
		mc.separator(w=20,style='none')
		mc.separator(h=6,style ='none')
		mc.separator(h=10,style ='singleDash',w=200)
		mc.separator(h=6,style ='none')
		resetRow = mc.rowLayout('resteRow',nc=8)
		mc.button('clearL',l='reset left facial',w=166,h=35,c=self.resetFaceL)
		mc.separator(style ='none')
		mc.button('clearR',l='reset right facial',w=166,h=35,c=self.resetFaceR)
		mc.separator(style ='none')
		mc.button('clearM',l='reset Mid facial',w=166,h=35,c=self.resetFaceMid)
		mc.separator(style ='none')
		mc.button('clearA',l='reset All facial',w=166,h=35,c=self.resetFaceAll)
		mc.separator(style ='none')
		mc.setParent(mirrorTool)
		mc.separator(h=6,style ='none')
		mc.rowLayout('mirFaceRow',nc=3)
		mc.button('mirfaceL',l='mirrorFaceFrame Left to Right ',w=335,h=35,c=self.mirAllFrameL)
		mc.separator(w=3,style='none')
		mc.button('mirfaceR',l='mirrorFaceFrame Right to Left ',w=335,h=35,c=self.mirAllFrameR)
		mc.setParent(mirrorTool)
		mc.separator(h=15,style ='none')


	
		######### Third layout ############################################################
		###################################################################################
		###################################################################################
		###################################################################################
		###################################################################################
		mc.separator(h=14,style ='none',p=mainLayout)
		mc.setParent(tmpTabLayout)
		mc.columnLayout('mocapXTool',cal='center',co=['both',15],w=300,adj=1)
		mc.separator(h=8,style ='none')
		mc.text('mocapXcap',l='Create MocapX System')
		mc.separator(h=8,style ='none')
		mc.button('createMocapX',l='Create MocapX System',w=150,h=35,c=self.createMocapXSys)
		#mc.separator(h=10,style ='singleDash',w=200)
		mc.separator(h=8,style ='none')
		mc.button('createAdapter',l='Create MocapX Adapter',w=150,h=35,c=self.createMocapXDevice)
		mc.separator(h=8,style ='none')
		mc.button('connectAll',l='Connect MocapX',w=150,h=35,c=self.connectPose)

		mc.separator(h=8,style ='none')
		mc.button('convert',l='Convert back to key frame',w=150,h=35,c=self.convert_back_to_key)
		#mc.separator(h=10,style ='singleDash',w=200)


		mc.showWindow(self.winName)

def show():
	x = mocapXRig().showMocapXRigUI()
	return x