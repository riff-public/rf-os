import maya.cmds as mc
import maya.mel as mel
import pymel.core as pmc
import sys
import os
import json
import pickle
#sys.path.append('C:/Users/pornpavis.t/Desktop/mocapx_1.1.5_Maya2018_win/scripts/mocapx')

#sys.path.append(r'D:\Script\core\rf_maya\rftool\rig\MocapX')
from rf_maya.mocapx import commands as cm
reload(cm)

from rf_maya.mocapx.lib import utils
reload(cm)

#import fclCtrlList as fcll
#reload(fcll)
from mocapx.lib import nodes as nd
reload(nd)

from rf_app.mocap.function import animData
reload(animData)


#poseList = [ 'browDown_L', 'browOuterUp_L', 'cheekSquint_L', 'eyeBlink_L', 'eyeLookDown_L', 'eyeLookIn_L', 'eyeLookOut_L', 'eyeLookUp_L', 'eyeSquint_L', 'eyeWide_L', 'mouthDimple_L', 'mouthFrown_L', 'mouthLowerDown_L', 'mouthPress_L', 'mouthSmile_L', 'mouthStretch_L', 'mouthUpperUp_L', 'noseSneer_L', 'jawLeft', 'mouthLeft', 'browDown_R', 'browOuterUp_R', 'cheekSquint_R', 'eyeBlink_R', 'eyeLookDown_R', 'eyeLookIn_R', 'eyeLookOut_R', 'eyeLookUp_R', 'eyeSquint_R', 'eyeWide_R', 'mouthDimple_R', 'mouthFrown_R', 'mouthLowerDown_R', 'mouthPress_R', 'mouthSmile_R', 'mouthStretch_R', 'mouthUpperUp_R', 'noseSneer_R', 'jawRight', 'mouthRight', 'browInnerUp', 'mouthClose', 'cheekPuff', 'jawOpen', 'mouthRollLower', 'tongueOut', 'mouthPucker', 'mouthShrugUpper', 'mouthFunnel', 'jawForward', 'mouthShrugLower', 'mouthRollUpper']
poseList = [ "eyeBlink_L", "eyeBlink_R", "eyeLookDown_L", "eyeLookDown_R", "eyeLookIn_L", "eyeLookIn_R", "eyeLookOut_L", "eyeLookOut_R", "eyeLookUp_L", "eyeLookUp_R", "eyeSquint_L", "eyeSquint_R", "eyeWide_L", "eyeWide_R", "jawForward", "jawLeft", "jawRight", "jawOpen", "mouthClose", "mouthFunnel", "mouthPucker", "mouthLeft", "mouthRight", "mouthSmile_L", "mouthSmile_R", "mouthFrown_L", "mouthFrown_R", "mouthDimple_L", "mouthDimple_R", "mouthStretch_L", "mouthStretch_R", "mouthRollLower", "mouthRollUpper", "mouthShrugLower", "mouthShrugUpper", "mouthPress_L", "mouthPress_R", "mouthLowerDown_L", "mouthLowerDown_R", "mouthUpperUp_L", "mouthUpperUp_R", "browDown_L", "browDown_R", "browInnerUp", "browOuterUp_L", "browOuterUp_R", "cheekPuff", "cheekSquint_L", "cheekSquint_R", "noseSneer_L", "noseSneer_R", "tongueOut"]

poseLibName = 'PoseLib'
collectionName = 'AttributeCollection'
eyePoseList=[ 'leftEyeTransformTranslateX','leftEyeTransformTranslateY','leftEyeTransformTranslateZ','rightEyeTransformTranslateX','rightEyeTransformTranslateY','rightEyeTransformTranslateZ']
eyePoseRotList=[ 'leftEyeTransformRotateX','leftEyeTransformRotateY','leftEyeTransformRotateZ','rightEyeTransformRotateX','rightEyeTransformRotateY','rightEyeTransformRotateZ']
headPoseList = ['transformRotateX','transformRotateY','transformRotateZ']

attrList = ['mouthUpperUp_R','mouthPress_L','mouthLowerDown_L','browDown_L','cheekPuff','mouthShrugLower','eyeLookUp_R','jawLeft','eyeBlink_L','eyeLookIn_L','eyeLookOut_R','mouthShrugUpper','mouthFrown_L','jawForward','eyeSquint_R','mouthStretch_L','eyeWide_L','jawRight','jawOpen','cheekSquint_R','noseSneer_R','browOuterUp_L','eyeWide_R','eyeLookDown_R','browOuterUp_R','mouthSmile_R','mouthPress_R','mouthClose','cheekSquint_L','eyeLookDown_L','mouthRight','mouthRollUpper','eyeSquint_L','mouthRollLower','mouthStretch_R','mouthDimple_L','mouthUpperUp_L','mouthPucker','noseSneer_L','browDown_R','browInnerUp','mouthLowerDown_R','eyeLookUp_L','eyeLookIn_R','mouthFunnel','mouthFrown_R','eyeLookOut_L','mouthLeft','mouthDimple_R','eyeBlink_R','mouthSmile_L','tongueOut','lookAtPointX','lookAtPointY','lookAtPointZ','transformTranslateX','transformTranslateY','transformTranslateZ','transformRotateX','transformRotateY','transformRotateZ','leftEyeTransformTranslateX','leftEyeTransformTranslateY','leftEyeTransformTranslateZ','leftEyeTransformRotateX','leftEyeTransformRotateY','leftEyeTransformRotateZ','rightEyeTransformTranslateX','rightEyeTransformTranslateY','rightEyeTransformTranslateZ','rightEyeTransformRotateX','rightEyeTransformRotateY','rightEyeTransformRotateZ']

rightFrame = [3.0, 5.0, 7.0, 9.0, 11.0, 13.0, 15.0, 18.0,24.0, 26.0, 28.0, 30.0, 32.0, 38.0, 40.0, 42.0, 44.0, 47.0, 50.0, 52.0]
leftFrame = [2.0, 4.0, 6.0, 8.0, 10.0, 12.0, 14.0, 17.0, 23.0 , 25.0, 27.0, 29.0, 31.0, 37.0, 39.0, 41.0, 43.0, 46.0, 49.0, 51.0]
midFrame = [16.0,19.0, 20.0, 21.0, 22.0 ,33.0, 34.0, 35.0, 36.0, 45.0, 48.0, 53.0]

# def createCtrlListAttr(ns,attrName = 'ctrlList',sort=1):
# 	geoGrp = '{}Geo_Grp'.format(ns)
# 	ctrlList = mc.ls(sl=True)
# 	if not mc.objExists(geoGrp+'.{}'.format(attrName)):
# 		mc.addAttr(geoGrp,ln=attrName,k=0,dt='string')
# 	ctrlAttrList = [] 
# 	for i in ctrlList:
# 		ctrl = i.split(ns)[-1]
# 		ctrlAttrList.append(ctrl)
# 	if sort ==1:
# 		ctrlAttrList.sort()
# 	else:
# 		pass
# 	ctrlStr = ','.join(ctrlAttrList)
# 	mc.setAttr(geoGrp+'.'+attrName,ctrlStr,type='string')

# def getAttrFromCtrlListAttr( ns , attrName = 'ctrlList'):
# 	geoGrp = '{}Geo_Grp'.format(ns)
# 	if mc.objExists(geoGrp+'.{}'.format(attrName)):
# 		ctrlStr = mc.getAttr(geoGrp+'.{}'.format(attrName))
# 		ctrlStrList = ctrlStr.split(',')
# 		selList = []
# 		for i in ctrlStrList:
# 			ctrl = ns+i
# 			selList.append(ctrl)
# 		return selList
# 	else:
# 		return None

def getAttrFromCtrlListAttr( ns , ctrl_list = []):
	ns_list=  []
	if ctrl_list:
		for ctrl in ctrl_list:
			ctrl = ns + ctrl
			ns_list.append(ctrl)
		return ns_list
	else:
		return None

# def createAllCtrlList():
# 	createCtrlListAttr('ctrlList')

# def createLeftCtrlList():
# 	createCtrlListAttr('leftCtrlList')

# def createRightCtrlList():
# 	createCtrlListAttr('rightCtrlList')

# def createMidCtrlList():
# 	createCtrlListAttr('midCtrlList')

# def createEyeCtrlList():
# 	createCtrlListAttr('eyeCtrlList')

# def createHeadCtrlList():
# 	createCtrlListAttr('headCtrlList')

def getRigDataFld():
	scnPath = os.path.normpath( mc.file( q = True , l = True )[0])
	#print scnPath
	tmpAry = scnPath.split( '\\' )
	#print tmpAry
	tmpAry[3] = 'publ'
	tmpAry[-3] = 'data'
	
	dataFld = '\\'.join( tmpAry[0:-2] )
	
	return dataFld


# def createCtrlList(asset):
# 	ctrlList = []
# 	eyeRotCtrl = []
# 	for i in oriList:
# 		ctrl = i.replace('<Asset>',asset)
# 		ctrlList.append(ctrl)

# 	for j in eyeRotOriCtrl:
# 		ctrl = j.replace('<Asset>',asset)
# 		eyeRotCtrl.append(ctrl)
	
# 	headCtrl= headOriCtrl.replace('<Asset>',asset)

# 	return ctrlList,eyeRotCtrl,headCtrl

# def createListFromDir():
# 	filePathName = getRigDataFld()
# 	asset = filePathName.split('\\')[5]
# 	ctrlList,eyeRotCtrl,headCtrl = createCtrlList(asset)
# 	return ctrlList,eyeRotCtrl,headCtrl

# def createListFromSel(ns):
# 	if mc.ls(sl=True)!= []:
# 		asset = ns
# 		ctrlList,eyeRotCtrl,headCtrl = createCtrlList(asset)
# 		return ctrlList,eyeRotCtrl,headCtrl

def createText(textValue):
	text = mc.textCurves(n=textValue+'_Ctrl',t=textValue)
	mc.delete(text[-1])
	shape = mc.listRelatives(text[0],c=1,ad=1,type ='shape')
	mc.select(shape)
	num=1
	for cv in shape:
		transform = pmc.PyNode(cv.replace('Shape',''))
		transform.rename(textValue+str(num))
		par = transform.getParent().nodeName()
		mc.makeIdentity(par,s=1,t=1,r=1,a=1)
		shape = transform.getShape().nodeName()
		mc.parent(shape,text[0],r=1,s=1)

		num+=1
	text[0] = mc.rename(text[0],text[0].replace('Shape',''))
	transformChild = mc.listRelatives(text[0],c=1,ad=1,type='transform')
	mc.delete(transformChild)
	return text[0]

def createMocapXSystem(poseLibName,collectionName,poseList,clipreader='',ctrlList=[]):
	mc.refresh(suspend=1)
	mc.currentTime(1)
	cm.create_empty_collection(collection_name=collectionName)
	cm.add_controls_to_collection(collectionName, control_list=ctrlList)
	cm.create_empty_poselib(poselib_name = poseLibName)
	for i in poseList:
		cm.create_empty_pose(pose_name = i)
		cm.add_pose_to_poselib(poseLibName, pose_list=poseList, selected_poses=False)
	if clipreader !='':
		for i in poseList:
			mc.connectAttr('{}.{}'.format(clipReader,i),i+'.weight')
	mc.refresh(suspend=0)

def createMocapXDevice():
	default = cm.create_clipreader()
	mc.rename(default,'Default')
	cm.create_realtime_device(adapter_name='MocapX')
	cm.create_clipreader(adapter_name = 'MocapX')
	createPoseValue()
	nd.set_active_source('MocapX','Default')

def connectClipReader(clipReader,PoseLibName,poseList):
	for i in poseList:
		if not mc.connectionInfo('{}.{}'.format(clipReader,i),isSource=1): 
			mc.connectAttr('{}.{}'.format(clipReader,i),i+'.weight')

def disConnectClipReader(clipReader,PoseLibName,poseList):
	for i in poseList:
		if mc.connectionInfo('{}.{}'.format(clipReader,i),isSource=1):
			mc.disconnectAttr('{}.{}'.format(clipReader,i),i+'.weight')

def createFacePoseText():
	grp = mc.createNode('transform',n='Text_Grp')
	mc.currentTime(1)
	text = createText('BaseFcl')
	mc.setKeyframe(text+'.v',v=1,s=0)
	mc.currentTime(0)
	mc.setKeyframe(text+'.v',v=0,s=0)
	mc.currentTime(2)
	mc.setKeyframe(text+'.v',v=0,s=0)

	mc.parent(text,grp)
	for i in range(len(poseList)):
		mc.currentTime(i+2)
		text = poseList[i]+'_Ctrl'
		if not mc.objExists(text):
			text = createText(poseList[i])
		mc.setKeyframe(text+'.v',v=1,s=0)
		mc.currentTime(i+1)
		mc.setKeyframe(text+'.v',v=0,s=0)
		mc.currentTime(i+3)
		mc.setKeyframe(text+'.v',v=0,s=0)
		mc.parent(text,grp)
	
def resetFacePoseFrame(ns,ctrlList,mode=1):
	frameRange = range(55)
	mc.refresh(suspend=1)
	if mode ==0:
		newRange = [2, 4, 6, 8, 10, 12, 14, 25, 27, 29, 31, 37, 39, 41, 43, 46, 49, 51]
	elif mode ==1:
		newRange = [3, 5, 7, 9, 11, 13, 15, 26, 28, 30, 32, 38, 40, 42, 44, 47, 50, 52]
	elif mode ==2:
		newRange = [16, 17, 18, 19, 20, 21, 22, 23, 24, 33, 34, 35, 36, 45, 48, 53]
	elif mode ==3:
		newRange = frameRange
	for frame in newRange:
		resetFacePose(ns,ctrlList,frame)
	mc.refresh(suspend=0)

def resetFacePose(ns,ctrlList,frame):
	reset_list = []
	for ctrl in ctrlList:
		reset_list.append('{}{}'.format(ns,ctrl))
	mc.currentTime(1)
	mc.select(reset_list)
	mel.eval('timeSliderCopyKey;')
	mc.currentTime(frame)
	mel.eval('timeSliderPasteKey false;')

def single_mirror_frame(ori_list, target_list, ori_frame, target_frame, attr_list,ignore_list):
	mc.copyKey(ori_list, t = (ori_frame,ori_frame), s=0)
	mc.pasteKey(target_list,t = (target_frame,target_frame))
	mc.copyKey(target_list, t = (ori_frame,ori_frame), s=0)
	mc.pasteKey(ori_list,t = (target_frame,target_frame))
	for ctrl in list(set(target_list) | set(ori_list)):
		if ctrl not in ignore_list:
			for attr in attr_list:
				try:
					mc.setKeyframe('{}'.format(ctrl),at = attr ,v = mc.getAttr('{}.{}'.format(ctrl,attr),t = target_frame )*-1 , t= (target_frame,target_frame))
				except Exception as e:
					print e
					mc.warning('{}.{} : has some issue.'.format(ctrl,attr))
	# for ctrl in ori_list:
	# 	if ctrl not in ignore_list:
	# 		for attr in attr_list:
	# 			try:
	# 				mc.setKeyframe('{}'.format(ctrl),at = attr ,v = mc.getAttr('{}.{}'.format(ctrl,attr))*-1.0 , t= (target_frame,target_frame))
	# 			except Exception as e:
	# 				print e
	# 				mc.warning('{}.{} : has some issue.'.format(ctrl,attr))

def mirror_frame(ori_list, target_list, ori_frame_list, target_frame_list, attr_list, ignore_list):
	if len(ori_frame_list) == len(target_frame_list) and len(ori_list) == len(target_list):
		for frame in range(len(ori_frame_list)):
			single_mirror_frame(ori_list,target_list, ori_frame_list[frame], target_frame_list[frame] , attr_list,ignore_list)


def export_fclKey(ns,ctrl_list,path):
	x = animData.MocapXAnimAsset(ctrl_list)
	try:
		x.export_anim(ns, path, 0,55)
	except:
		mc.refresh(suspend=0)
	
def import_fclKey(ns,ctrl_list,path,time_offset = 0 ,head_ctrl='Head_Ctrl' , head_bake =0):
	try:
		char_anim = animData.MocapXAnimAsset(ctrl_list)
		print path
		import_anim_data = animData.core.AnimData.init_from_file(path)
		
		char_anim.import_anim(import_anim_data, ns, False, time_offset, head_ctrl, head_bake)
	except:
		print 'import error some how'
		mc.refresh(suspend =0)

def multipleUpdatePose(ctrlList,poseList):
	mc.refresh(suspend=1)
	for i in range(len(poseList)):
		#print i
		mc.currentTime(i+2)
		#print i
		#if mc.getAttr(poseList[i]+'_Ctrl.v') == 1:
			#print 'run'
		cm.update_controls_in_pose(poseList[i],ctrlList)
	mc.refresh(suspend=0)

def connectEye(device,eyePoseList,eyeCtrl):

	for i in eyePoseList:
		if 'left' in i:
			ctrl = eyeCtrl[0]
			attr= i.split('Transform')[-1].lower()
			conAttr = attr[:-1]+attr[-1].upper()
			mc.connectAttr('{}.{}'.format(device,i),'{}.{}'.format(ctrl,conAttr))
		elif 'right' in i:
			ctrl = eyeCtrl[1]
			attr= i.split('Transform')[-1].lower()
			conAttr = attr[:-1]+attr[-1].upper()
			mc.connectAttr('{}.{}'.format(device,i),'{}.{}'.format(ctrl,conAttr))

def connectHead(device,headPoseList,headCtrl):
	for i in headPoseList:
		for ctrl in headCtrl:
			attr = i.split('transform')[-1].lower()
			conAttr = attr[:-1]+attr[-1].upper()
			mc.connectAttr('{}.{}'.format(device,i),'{}.{}'.format(ctrl,conAttr))


def connectForPublish():
	filePathName = getRigDataFld()
	asset = filePathName.split('\\')[5]

	mc.select('*:Geo_Grp')
	ns = pmc.selected()[0].namespace()
	print ns
	geoGrp = ns+'Geo_Grp'
	mc.select(d=1)
	
	if not mc.objExists(geoGrp+'.MocapX'):
		mc.addAttr(geoGrp,ln='MocapX',k=1)
	
	if not mc.objExists(geoGrp+'.AttrCollection'):
		mc.addAttr(geoGrp,ln='AttrCollection',k=1)
	
	if not mc.objExists(geoGrp+'.PoseLib'):
		mc.addAttr(geoGrp,ln='PoseLib',k=1)
	
	if not mc.objExists('MocapX.PoseLib'):
		mc.addAttr('MocapX',ln='connect',k=1)
	
	if not mc.objExists('AttributeCollection.connect'):	
		mc.addAttr('AttributeCollection',ln='connect',k=1)
	
	mc.connectAttr('AttributeCollection.connect',geoGrp+'.AttrCollection')
	mc.connectAttr('MocapX.connect',geoGrp+'.MocapX')
	#mc.connectAttr('PoseLib.connect',geoGrp+'.PoseLib')

def createPoseValue():
	if mc.objExists('Default'):
		for i in attrList:
			mc.addAttr('Default',ln=i,h=1)


def connectAll(ns,device,ctrlList,headCtrl,eyeRotCtrl):
	if ctrlList and headCtrl and eyeRotCtrl:
		connectClipReader(device,poseLibName,poseList)
		connectEye(device,eyePoseRotList,eyeRotCtrl)
		connectHead(device,headPoseList,headCtrl)


def disconnectHead(device,PoseList,headCtrl):
	for i in PoseList:
		for ctrl in headCtrl:
			attr = i.split('transform')[-1].lower()
			conAttr = attr[:-1]+attr[-1].upper()
			pmc.disconnectAttr('{}.{}'.format(ctrl,conAttr))	

def disconnectEye(device,eyePoseList,eyeCtrl):
	for i in eyePoseList:
		print i
		print eyeCtrl
		if 'left' in i:
			ctrl = eyeCtrl[0]
			attr= i.split('Transform')[-1].lower()
			conAttr = attr[:-1]+attr[-1].upper()

			pmc.disconnectAttr('{}.{}'.format(ctrl,conAttr))
		
		elif 'right' in i:
			ctrl = eyeCtrl[1]
			attr= i.split('Transform')[-1].lower()
			conAttr = attr[:-1]+attr[-1].upper()
			print ctrl
			print attr
			#if mc.isConnected('{}.{}'.format(device,i),,iuc=1):
			pmc.disconnectAttr('{}.{}'.format(ctrl,conAttr))


def selectAllCtrl(ns):
	selList = getAttrFromCtrlListAttr(ns,'ctrlList')
	mc.select(selList)
	return selList


def resetDevice(device):
	for i in attrList:
		mc.setAttr(device+'.'+i,0)


def deleteKey(ctrlList):
	mc.currentTime(1)
	mc.cutKey(ctrlList,cl=1)

def createFclKey(ctrlList):
	mc.refresh(suspend=1)
	for i in range(55)[1:]:
		#mc.currentTime(i)
		mc.setKeyframe(ctrlList,s=0,t=i)
	mc.refresh(suspend=0)

def importMocapXTmp():
	thisModulePath =  os.path.dirname(sys.modules[__name__].__file__)
	mc.file(thisModulePath+'\\mocapXTmp.ma',i=1)


# def modCtrlList(ns,attr='ctrlList',mode = 'add'):
# 	addCtrlList = mc.ls(sl=True)
# 	#ns = pmc.selected()[0].namespace()
# 	geoGrp = '{}Geo_Grp'.format(ns)
# 	if mc.objExists(geoGrp+'.{}'.format(attr)):
# 		existsCtrlStr = mc.getAttr(geoGrp+'.{}'.format(attr))
# 		existsCtrlList =  existsCtrlStr.split(',')
# 		addCtrlAttrList = []
# 		for i in addCtrlList:
# 			ctrl= i.split(ns)[-1]
# 			addCtrlAttrList.append(ctrl)
# 		if mode == 'add':
# 			newCtrlList = list( set(existsCtrlList)|set(addCtrlAttrList) )
# 		elif mode =='remove':
# 			newCtrlList = list( set(existsCtrlList) - set(addCtrlAttrList) )
# 		ctrlStr = ','.join(newCtrlList)
# 		mc.setAttr(geoGrp+'.{}'.format(attr),ctrlStr,type='string')
# 	else:
# 		mc.confirmDialog(m='ctrlList Attribute not found.')

def setZero(ctrlList):
	for i in ctrlList:
		listAttr = mc.listAttr(i,k=1,v=1)
		print listAttr
		for attr in listAttr:
			if ('scale' in attr) or ('scale' in attr):
				mc.setAttr('{}.{}'.format(i,attr),1)
			else:
				mc.setAttr('{}.{}'.format(i,attr),0)

def getMocapX():
	ns = pmc.selected()[0].namespace()
	return '{}MocapX'.format(ns)

def set_source(mocapX = '',source = ''):
	connectAttr_name = mc.listConnections(mocapX+'.activeSource',s=1,d=0,p=1)
	print connectAttr_name
	
	if connectAttr_name != None:
		mc.disconnectAttr(connectAttr_name[0],mocapX+'.activeSource')
	mc.connectAttr(source+'.message', mocapX+'.activeSource')

def set_clipReader_file(clipReader = '',path = ''):
	utils.clipreader_load_clip(clipReader, path)

def set_realtime_usb(realtimeDevice):
	nd.set_realtime_device_conn_type(realtimeDevice,1)

def disconnect_head_eye(ns):
	mocX = mc.ls(type = 'MCPXAdapter')
	clipReader  = mc.ls(type='MCPXClipReader') 
	
	if mocX:
		mocX = mocX[0]
	for ax in 'XYZ':
		hdAttr = mc.listConnections('{}.transformRotate{}'.format(mocX,ax),d=1,s=0,p=1,scn=1)
		if hdAttr:
			mc.disconnectAttr('{}.transformRotate{}'.format(mocX,ax),hdAttr[0])
			mc.setAttr(hdAttr[0],0)

	for side in ['left','right']:
		for ax in 'XYZ':
			eyeAttr = mc.listConnections('{}.{}EyeTransformRotate{}'.format(mocX,side,ax),d=1,s=0,p=1,scn=1)
			if eyeAttr:
				mc.disconnectAttr('{}.{}EyeTransformRotate{}'.format(mocX,side,ax),eyeAttr[0])
				mc.setAttr(eyeAttr[0],0)
		

def convert_back_to_key(ns):

	mocX = mc.ls(type = 'MCPXAdapter')
	clipReader  = mc.ls(type='MCPXClipReader') 
	
	if mocX:
		mocX = mocX[0]
	
	rest_attr_list = []
	rest_val_list = []
	for i in mc.getAttr('AttributeCollection.attrs',mi=1):
		rest_val_list.append(mc.getAttr('AttributeCollection.attrs[{}].restValue'.format(i)))
		rest_attr_list.append(mc.listConnections('AttributeCollection.attrs[{}].plug'.format(i),s=1,d=0,p=1,scn=1)[0])

	pose_key_list = []
	for pose in poseList:
		attr_list = []
		idx_list = []
		val_list = []

		if mc.getAttr(pose+'.attrs',mi=1):
			for idx in mc.getAttr(pose+'.attrs',mi=1):
				#idx_list.append(idx)
				val_list.append(mc.getAttr(pose+'.attrs[{}].pose'.format(idx)))
				attr = mc.listConnections(pose+'.attrs[{}].rest'.format(idx),s=1,d=0,p=1)[0]
				attr_list.append(mc.listConnections(attr.replace('.restValue','.plug'),s=1,d=0,p=1,scn=1)[0])
		else:
			pass
		pose_key_list.append([attr_list,val_list])

	for ax in 'XYZ':
		hdAttr = mc.listConnections('{}.transformRotate{}'.format(mocX,ax),d=1,s=0,p=1,scn=1)
		if hdAttr:
			mc.disconnectAttr('{}.transformRotate{}'.format(mocX,ax),hdAttr[0])
			mc.setAttr(hdAttr[0],0)

	for side in ['left','right']:
		for ax in 'XYZ':
			eyeAttr = mc.listConnections('{}.{}EyeTransformRotate{}'.format(mocX,side,ax),d=1,s=0,p=1,scn=1)
			if eyeAttr:
				mc.disconnectAttr('{}.{}EyeTransformRotate{}'.format(mocX,side,ax),eyeAttr[0])
				mc.setAttr(eyeAttr[0],0)
		
	
	for rest_idx,attr in enumerate(rest_attr_list):
		mc.setKeyframe(attr, v= rest_val_list[rest_idx],t=1,s=0)
	
	for idx,pose_data in enumerate(pose_key_list):
		frame = idx+2
		for rest_idx,attr in enumerate(rest_attr_list):
			mc.setKeyframe(attr, v= rest_val_list[rest_idx],t=frame,s=0)
		for num in range(len(pose_data[0])):
			try:
				mc.setKeyframe(pose_data[0][num], v= pose_data[1][num],t=frame)
			except:
				mc.setKeyframe(attr, v= rest_val_list[rest_idx],t=frame,s=0)


	for clip in clipReader:
		if 'Default' in clipReader and mocX:
			try:
				set_source(mocX,clip)
			except:
				break

	for cleanUp in ['AttributeCollection',':*MocapX','MocapX',
	'RealtimeDevice','ClipReader','Default','*:RealtimeDevice',
	'*:ClipReader','*:Default']:	
		try:
			mc.delete(cleanUp)
			mc.delete('PoseLib')
		except:
			pass
			
def bake_key():
	cm.bake_channels()


def connect_mocapX_fbx(obj,isMocapX = True):
	ref_jnt = obj
	exception = ["Pose_0", "filmboxTypeID"]
	
	# find source and destination
	attr = mc.listAttr(ref_jnt, ud=True)
	src = [i for i in attr if i not in exception]

	if isMocapX:

		des = []
		for i in src:
			if "Left" in i:
				if "jawLeft" in i or "mouthLeft" in i:
					des.append(i)
					
				else:
					des.append(i.replace("Left", "_L"))
					
			elif "Right" in i:
				if "jawRight" in i or "mouthRight" in i:
					des.append(i)
				
				else:
					des.append(i.replace("Right", "_R"))
					
			else:
				des.append(i)
				
	else:
		des = scr

	# connect face attributes
	for s, p in zip(src, des):
		if mc.objExists(p):
			clean_poseLib(p)
			mc.connectAttr("{}.{}".format(ref_jnt, s), "{}.weight".format(p))	

def clean_poseLib(pose):
	inputs = mc.listConnections(pose+'.weight',s=1,d=0,p=1)
	if inputs:
		for inp in inputs:
			mc.disconnectAttr(inp,pose+'.weight')
#from mocapx.lib import utils
#utils.clipreader_load_clip('jaxTwo_001:ClipReader',r'P:\Library\Mocap\DarkHorse\01-07-20\mcpx\test.mcpx')

#def bake_pose(mocapX = '', path =''):

#def mirrorTransform(obj,target,axis='x'):
#resetDevice('RealtimeDevice')
#resetDevice('ClipReader')
#createFacePoseText()
#disConnectClipReader('ClipReader',poseLibName,poseList)
#connectClipReader('RealtimeDevice',poseLibName,poseList)
#mirAllFrame(2,ctrlList)
#createFacePoseText(ctrlList)
#connectForPublish()
#connectAll('MocapX')
#disConnectClipReader('MocapX',poseLibName,poseList)
#connectEye(eyePoseList,eyeCtrl)


#ctrlList,eyeRotCtrl,headCtrl = createListFromDir()

##### facial pose Part ##############
#ctrlList,eyeRotCtrl,headCtrl = createListFromDir()
#createCtrlListAttr()
#createFclKey(ctrlList)
#importMocapXTmp()
#selectAllCtrl()
#mirAllFrame(2,ctrlList)

##### MocapX part ##################

#ctrlList,eyeRotCtrl,headCtrl = createListFromDir()
#createCtrlListAttr()
#createMocapXSystem(poseLibName,collectionName,poseList,'',ctrlList)
#multipleUpdatePose(ctrlList,poseList)
#deleteKey(ctrlList)
#cm.get_adapter_node(create_node=True)
#cm.create_realtime_device('MocapX')
#cm.create_clipreader('MocapX')
#createPoseValue()
#cm.set_active_source('MocapX','RealtimeDevice')
#connectAll('MocapX')
#connectClipReader('MocapX',poseLibName,poseList)
#connectEye('MocapX',eyePoseRotList,eyeRotCtrl)
#connectHead('MocapX',headPoseList,headCtrl)
'''
######## anim Part #################
selectAllCtrl()
disconnectAddition('MocapX',headPoseList,headCtrl)
disconnectAddition('MocapX',eyePoseRotList,eyeRotCtrl)
'''



# def getCtrlDict(ctrl1):
# 	attrList = mc.listAttr(ctrl1,k=1,u=1)
# 	ctrlAttrDict={}
# 	for attr in attrList:
# 		ctrlAttrDict[attr] = mc.getAttr('{}.{}'.format(ctrl1,attr))
# 	return ctrlAttrDict

# def createMirrorDictCtrl(LeftCtrlList,RightCtrlList,midCtrlList,Side='l'):
# 	ctrlDict = {}
# 	for idx in range(len(LeftCtrlList)):
# 		if Side.lower() == 'l':
# 			opCtrl = RightCtrlList[idx]
# 			ctrlAttrDict = getCtrlDict(LeftCtrlList[idx])
# 			ctrlDict[opCtrl] = ctrlAttrDict
# 			ctrlAttrDict = getCtrlDict(RightCtrlList[idx])
# 			ctrlDict[LeftCtrlList[idx]] = ctrlAttrDict
			
# 		if Side.lower() == 'r':
# 			opCtrl = LeftCtrlList[idx]
# 			ctrlAttrDict = getCtrlDict(RightCtrlList[idx])
# 			ctrlDict[opCtrl] = ctrlAttrDict
# 			ctrlAttrDict = getCtrlDict(LeftCtrlList[idx])
# 			ctrlDict[RightCtrlList[idx]] = ctrlAttrDict
	
# 	for ctrl in midCtrlList:
# 		attrList = mc.listAttr(ctrl,k=1,u=1)
# 		ctrlAttrDict={}
# 		for attr in attrList:

# 			if Side.lower() == 'l':
# 				search = 'Lft'
# 				replace = 'Rgt'
# 				search2 = 'Left'
# 				replace2 =  'Right'
# 			elif Side.lower() =='r':
# 				search = 'Rgt'
# 				replace = 'Lft'
# 				search2 = 'Right'
# 				replace2 =  'Left'		
			
# 			attrData = mc.getAttr('{}.{}'.format(ctrl,attr))

# 			if search in attr:
# 				attr = attr.replace(search,replace)
				
# 			elif replace in attr:
# 				attr =attr.replace(replace,search)
			
# 			elif search2 in attr:
# 				attr =attr.replace(search2,replace2)

# 			elif replace2 in attr:
# 				attr =attr.replace(replace2,search2)

# 			if (attr != 'rotateY') and (attr != 'translateX'):
# 				ctrlAttrDict[attr] = attrData
# 			else:
# 				ctrlAttrDict[attr] = -(attrData)

# 		ctrlDict[ctrl] = ctrlAttrDict			
# 	return ctrlDict



# def mirFrameCtrl(frame,LeftCtrlList,RightCtrlList,midCtrlList,offSetFrame = 20,Side='l'):
# 	mirFrame = int(frame+offSetFrame)
# 	mc.currentTime(frame)
# 	ctrlDict = createMirrorDictCtrl(LeftCtrlList,RightCtrlList,midCtrlList,Side)
# 	mc.currentTime(frame+offSetFrame)
# 	if ctrlDict != None:
# 		for ctrl in ctrlDict.keys():
# 			for attr in ctrlDict[ctrl].keys():
# 				value = ctrlDict[ctrl][attr]
# 				mc.setAttr('{}.{}'.format(ctrl,attr),value)


# def mirAllFrame(ns,startFrame,ctrlList,rTol=0):
# 	LeftCtrlList = getAttrFromCtrlListAttr(ns,'leftCtrlList')
# 	RightCtrlList = getAttrFromCtrlListAttr(ns,'rightCtrlList')
# 	midCtrlList = getAttrFromCtrlListAttr(ns,'midCtrlList')
# 	if len(LeftCtrlList) == len(RightCtrlList):
# 		if rTol==0:
# 			offsetFrame = 20
# 			side = 'l'
# 		else:
# 			offsetFrame = -20
# 			side = 'r'
# 		for i in range(20):
# 			mirFrameCtrl(i+startFrame,LeftCtrlList,RightCtrlList,midCtrlList,offsetFrame,side)
# 	else:
# 		mc.confirmDialog(m='controller on left and right not equal.')
# 		return None