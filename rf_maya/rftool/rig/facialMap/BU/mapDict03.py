import pymel.core as pmc
import maya.cmds as mc
import rf_utils.file_utils as fu
import re

def searchNameMap(name,types,part,lastSearch):
	for i in mapList:
		if re.search('^{}_{}.*{}.*{}.png$'.format(name,types,part,lastSearch),i):
			print i
		else:
			print 'notfound'


def combineDictMap(mapList=[]):
	if mapList!=[]:
		comDict ={}
		for mapFile in mapList:
			comDict[mapFile] = genDictMap(mapFile)
	return comDict

def genDictMap(mapFile):
	Dict = {}
	x = pmc.PyNode(mapFile)

	for node in x.history():
		#if  pmc.PyNode(node).referenceFile() == None:
		con = node.inputs(c=1,p=1)
		types = node.nodeType()
		if types != 'place2dTexture':
			nodeDict = {}
			nodeDict['types'] = types 
			nodeDict['con'] = []
			for source,destination in con:
				connectionPair = [source.name(),destination.name()]
				if (source.node().nodeType() != 'place2dTexture') and (destination.node().nodeType() !='place2dTexture'):
					nodeDict['con'].append(connectionPair)
			Dict[node.name()] = nodeDict
	return Dict

def runDictMap(mapDict):
	for key in mapDict.keys():
		#print key
		if not mc.objExists(key):
			mc.createNode(mapDict[key]['types'],n= key )
	
	for key in mapDict.keys():	
		for conList in mapDict[key]['con']:
			if not mc.objExists(str(conList[1])):
				print conList[1]
				nodeName =conList[1].split('.')[0]
				attrName = '.'.join(conList[1].split('.')[1:])
				print attrName
				mc.addAttr(nodeName,ln = attrName,dv=0,k=1)
			if not mc.objExists(str(conList[0])):
				nodeName =conList[0].split('.')[0]
				attrName = '.'.join(conList[0].split('.')[1:])
				print attrName
				mc.addAttr(nodeName,ln = attrName,dv=0,k=1)
			if not mc.isConnected(conList[1],conList[0]):
				mc.connectAttr(conList[1],conList[0])
				#print conList[1] + ': connect '+ conList[0]
			else:
				pass

def createNodeDictMap(mapDict):
	for key in mapDict.keys():
		#print key
		if not mc.objExists(key):
			mc.createNode(mapDict[key]['types'],n= key )

def connectNodeDictMap(mapDict):
	for key in mapDict.keys():	
		for conList in mapDict[key]['con']:
			if not mc.objExists(str(conList[1])):
				print conList[1]
				nodeName =conList[1].split('.')[0]
				attrName = '.'.join(conList[1].split('.')[1:])
				print attrName
				mc.addAttr(nodeName,ln = attrName,dv=0,k=1)
			if not mc.objExists(str(conList[0])):
				nodeName =conList[0].split('.')[0]
				attrName = '.'.join(conList[0].split('.')[1:])
				print attrName
				mc.addAttr(nodeName,ln = attrName,dv=0,k=1)
			if not mc.isConnected(conList[1],conList[0]):
				mc.connectAttr(conList[1],conList[0])
				#print conList[1] + ': connect '+ conList[0]
			else:
				pass    

def writeMapDict(filePath,Dict):
	fu.ymlDumper(filePath,Dict,flowStyle=False)

def readMapDict(filePath):
	mapDict = fu.ymlLoader(filePath)
	return mapDict

def renameNode(fileNode):
	filePath = mc.getAttr(fileNode+'.fileTextureName')
	fileName = filePath.split('/')[-1]
	if mc.getAttr(fileNode+'.uvTilingMode') ==3:
		nodeName = fileName.split('.')[0]+'_1'
	else:
		nodeName = fileName.replace('.png','_1').replace('.','_')
	if fileNode != nodeName:
		mc.rename(fileNode,nodeName)


def selectMapFile(char = '',types = ''):
	mc.select(mc.ls('{}_mdlMainMtr:{}*_*{}*'.format(char,char,types),type='file'))

def readCombineMap(combineDict):
	for key in combineDict.keys():
		#print combineDict[key]
		#print combineDict[key].keys()
		for key2 in combineDict[key].keys():
			print combineDict[key][key2]
		createNodeDictMap(combineDict[key])
	for key in combineDict.keys():
		#print combineDict[key]
		#print combineDict[key].keys()
		for key2 in combineDict[key].keys():
			print combineDict[key][key2]
		connectNodeDictMap(combineDict[key])
		
		
		
def createCombineMap(assetName = '',types= ''):
	selectMapFile(assetName,types)
	fileNode =mc.ls(sl=True)
	return combineDictMap(fileNode)

def readCombineDict(filePath):
	fileDict = readMapDict(filePath)
	readCombineMap(fileDict)

#createCombineMap('jax','Normal')
def search_replace_keys(srcFile, searchReplaceDict,filePath) :
	# open file
	f = open(srcFile, 'r')
	data = f.read()
	f.close()

	for search, replace in searchReplaceDict.iteritems():
		data = data.replace(search, replace)

	replaceData = data
	print replaceData
	# write back
	f = open(filePath,'w')
	f.write( replaceData)
	f.close()
	return True
	
def reWriteFacialMap(asset,types,facialRig,srcFile,filePath):
	Dict = {'<Asset>':asset,'<Types>':types,'<FaceRig>':facialRig}
	md = '{}_md'.format(asset)
	#Dict = {'jaxMorkon_mdlMainMtr:jaxMorkon':asset,'Normal':types,'jax_md':facialRig,'jaxMorkon_md':md}
	search_replace_keys(srcFile,Dict,filePath)
	
	return True

#reWriteFacialMap('BerserkerLeader','Normal','BerserkerLeader_md','C:/Users/pornpavis.t/Desktop/TestCache/ConnectWithGeoGrpTmp','C:/Users/pornpavis.t/Desktop/TestCache/BerserkerLeadGeoGrpTmp')


def connectMaptoGeoGrp(mapFile,mapAttr,Asset,Types):
	  #mapName = mapFile.split(':')[-1].split('_')[-2]
	  sourceCon = mc.listConnections(mapFile+'.alphaGain',s=1,p=1)
	  #outputCon = mc.listConnections(mapFile)
	  if sourceCon !=None:
			#print mapFile
			mc.disconnectAttr(sourceCon[0],mapFile+'.alphaGain')
			#attrName = re.split('.*{}.*{}_'.format(Asset,Types),mapFile)[-1]
			geoGrp = '{}_md:Geo_Grp'.format(Asset)
			geoAttr = '{}'.format(mapAttr).replace('_1','')
			if not mc.objExists('{}.{}'.format(geoGrp,geoAttr)):
				mc.addAttr(geoGrp,ln=geoAttr,dv=0,k=1)
			mc.connectAttr(sourceCon[0],'{}.{}'.format(geoGrp,geoAttr))
			mc.connectAttr('{}.{}'.format(geoGrp,geoAttr),mapFile+'.alphaGain')

def allConnectMaptoGeoGrp(Asset,Types):
	attrList = ['sc__MouthCrnUpL',
	'sc__MouthCrnUpR',
	'sc__MouthU',
	'sc__MouthUpL',
	'sc__MouthUpR',
	'sc__NeckL',
	'sc__NeckR',
	'sc__NosecheekUpL',
	'sc__NosecheekUpR',
	'sc__UnderEyeUpL',
	'sc__UnderEyeUpR',
	'sc__UpperNoseL',
	'sc__UpperNoseR',
	'sc__Chin',
	'sc__ForeheadDnL',
	'sc__ForeheadDnR',
	'sc__ForeheadDnMid',
	'sc__ForeheadUpL',
	'sc__ForeheadUpR',
	'sc__MouthCrnDnL',
	'sc__MouthCrnDnR']
	mapList = mc.ls('*{}*_*{}*'.format(Asset,Types),type='file')

	for i in attrList:
		mapName = i.split('sc__')[-1]
		for j in mapList:
			if re.search('{}_'.format(mapName),j):
				connectMaptoGeoGrp(j,i,Asset,Types)

def disconnectOutputMap(asset,types):
	inputList = []
	mapList = ['{}_mdlMainMtr:{}_{}_Chin_1'.format(asset,asset,types),
	'{}_mdlMainMtr:{}_{}_NeckR_1'.format(asset,asset,types),
	'{}_mdlMainMtr:{}_{}_NeckL_1'.format(asset,asset,types),
	'{}_mdlMainMtr:{}_{}_MouthUpL_1'.format(asset,asset,types),
	'{}_mdlMainMtr:{}_{}_MouthU_1'.format(asset,asset,types),
	'{}_mdlMainMtr:{}_{}_MouthUpR_1'.format(asset,asset,types),
	'{}_mdlMainMtr:{}_{}_ForeheadDnR_1'.format(asset,asset,types),
	'{}_mdlMainMtr:{}_{}_ForeheadDnL_1'.format(asset,asset,types),
	'{}_mdlMainMtr:{}_{}_MouthCrnDnR_1'.format(asset,asset,types),
	'{}_mdlMainMtr:{}_{}_MouthCrnDnL_1'.format(asset,asset,types)]
	for mapFile in mapList:
		desCon = pmc.PyNode(mapFile).outputs(c=1,p=1,t='layeredTexture')
		desNode = pmc.PyNode(mapFile).outputs(p=0,t='layeredTexture')
		print desCon
		#desNode =  list(set(desNode))
		#print desNode
		
		for source,destination in desCon:
			mc.disconnectAttr(source.name(),destination.name())
			num = re.findall('[[0-9]*]'.format(destination.nodeName()),destination.name())[0]
			if num not in inputList:
				inputList.append(num)
		print inputList
		
		for i in inputList:
			delMap = destination.nodeName()+'.inputs{}'.format(i)
			print delMap
			mel.eval('removeMultiInstance -break true "{}" ;'.format(delMap))


#reWriteFacialMap('jaxMorkon_mdlMainMtr:jaxMorkon','Normal','jax_md','C:/Users/pornpavis.t/Desktop/TestCache/facialTmp.yml','C:/Users/pornpavis.t/Desktop/TestCache/JaxTmp.yml')
#selectMapFile('jaxMorkon','Normal')
#allConnectMaptoGeoGrp(':*jaxMorkon','Normal')
#x=createCombineMap('jaxMorkon','Normal')
#readCombineMap(x)
#readCombineDict('C:/Users/pornpavis.t/Desktop/TestCache/JaxTmp.yml')
#disconnectOutputMap('jaxMorkon','Normal')
#writeMapDict('C:/Users/pornpavis.t/Desktop/TestCache/JaxConnectWithGeoGrp.yml',x)
#reWriteFacialMap('<Asset>','<Types>','<FaceRig>','C:/Users/pornpavis.t/Desktop/TestCache/JaxConnectWithGeoGrp.yml','C:/Users/pornpavis.t/Desktop/TestCache/ConnectWithGeoGrpTmp.yml')
