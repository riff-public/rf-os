import pymel.core as pmc
import maya.cmds as mc
import rf_utils.file_utils as fu
import re
import os
import sys
import maya.mel as mel


def searchNameMap(,name,types,lastSearch,partMap=partMap):
	'''
	search file map by name.
	'''

	for i in partMap:
		if re.search('^{}_{}.*{}.*{}.png$'.format(name,types,i,lastSearch),i):
			returnMapList.append(i)
		else:
			print 'notfound'

	return returnMapList

def selectMapFile(char = '',types = ''):
	'''
	select all Map file base on Character and types input.
	'''
	mc.select(mc.ls('{}_mdlMainMtr:{}*_*{}*'.format(char,char,types),type='file'))


def renameNode(fileNode):
	'''
	this function use for renaming mapFile Node base on picture file Name.
	'''

	filePath = mc.getAttr(fileNode+'.fileTextureName')
	fileName = filePath.split('/')[-1]
	if mc.getAttr(fileNode+'.uvTilingMode') ==3:
		nodeName = fileName.split('.')[0]+'_1'
	else:
		nodeName = fileName.replace('.png','_1').replace('.','_')
	if fileNode != nodeName:
		mc.rename(fileNode,nodeName)

def combineDictMap(mapList=[]):
	'''
	create Dictionary that contain all Dictionary of Connection of each map file.
	'''
	if mapList!=[]:
		comDict ={}
		for mapFile in mapList:
			comDict[mapFile] = genDictMap(mapFile)
	return comDict

def genDictMap(mapFile):
	'''
	create Dictionary that contain Connection of node for each map file.
	'''
	Dict = {}
	x = pmc.PyNode(mapFile)

	for node in x.history():
		#if  pmc.PyNode(node).referenceFile() == None:
		con = node.inputs(c=1,p=1)
		types = node.nodeType()
		if types != 'place2dTexture':
			nodeDict = {}
			nodeDict['types'] = types 
			nodeDict['con'] = []
			for source,destination in con:
				connectionPair = [source.name(),destination.name()]
				if (source.node().nodeType() != 'place2dTexture') and (destination.node().nodeType() !='place2dTexture'):
					nodeDict['con'].append(connectionPair)
			Dict[node.name()] = nodeDict
	return Dict

def createNodeDictMap(mapDict):
	'''
	this function will createNode base on data on input Dictionary.
	'''
	for key in mapDict.keys():
		#print key
		if not mc.objExists(key):
			mc.createNode(mapDict[key]['types'],n= key )

def connectNodeDictMap(mapDict):
	'''
	this function will connectNode base on data on inpunt Dictionary.
	'''
	for key in mapDict.keys():	
		for conList in mapDict[key]['con']:
			if not mc.objExists(str(conList[1])):
				print conList[1]
				nodeName =conList[1].split('.')[0]
				attrName = '.'.join(conList[1].split('.')[1:])
				print attrName
				mc.addAttr(nodeName,ln = attrName,dv=0,k=1)
			if not mc.objExists(str(conList[0])):
				nodeName =conList[0].split('.')[0]
				attrName = '.'.join(conList[0].split('.')[1:])
				print attrName
				mc.addAttr(nodeName,ln = attrName,dv=0,k=1)
			if not mc.isConnected(conList[1],conList[0]):
				mc.connectAttr(conList[1],conList[0])
			else:
				pass    

def createCombineMap(assetName = '',types= ''):
	'''
	This Function will create combineDict which contain
	multiple mapFile Dictionary Data.
	'''
	selectMapFile(assetName,types)
	fileNode =mc.ls(sl=True)
	return combineDictMap(fileNode)

def readCombineDict(filePath):
	'''
	This Function will read combineDict frome FilePath.
	'''
	fileDict = readMapDict(filePath)
	readCombineMap(fileDict)

def readCombineMap(combineDict):
	'''
	This function will read combineDict that contain
	multiple mapFile connection Dictionary

	This Function will createNode in Dictionary that
	Does not exists in the scene before do the connection between Node

	This Function will Connect Node base on Dictionary Data
	if which attribute does not exists it will add Attr base on DictionaryData.
	'''
	for key in combineDict.keys():
		for key2 in combineDict[key].keys():
			print combineDict[key][key2]
		createNodeDictMap(combineDict[key])
	
	for key in combineDict.keys():
		for key2 in combineDict[key].keys():
			print combineDict[key][key2]
		connectNodeDictMap(combineDict[key])
def writeMapDict(filePath,Dict):
	'''
	this function will write Dictionary as yml file
	please put FilePath include fileName for directory of saved file.
	'''
	fu.ymlDumper(filePath,Dict,flowStyle=False)

def readMapDict(filePath):
	'''
	this function will read Dictionary from yml file
	please put FilePath include fileName for directory of saved file.
	'''	
	mapDict = fu.ymlLoader(filePath)
	return mapDict
		

def search_replace_keys(srcFile, searchReplaceDict,filePath) :
	'''
	ThisFunction will do search and replace string from dictionary
	for adaptation same data for another asset.
	'''
	f = open(srcFile, 'r')
	data = f.read()
	f.close()

	for search, replace in searchReplaceDict.iteritems():
		data = data.replace(search, replace)

	replaceData = data
	print replaceData
	# write back
	f = open(filePath,'w')
	f.write( replaceData)
	f.close()
	return True
	
def reWriteFacialMap(asset,types,facialRig,srcFile,filePath):
	'''
	this function will do search and replace string of Tmp CombineDict
	by using search_replace_keys function.
	'''
	Dict = {'<Asset>':asset,'<Types>':types,'<FaceRig>':facialRig}
	md = '{}_md'.format(asset)
	#Dict = {'jaxMorkon_mdlMainMtr:jaxMorkon':'<Asset>_mdlMainMtr:<Asset>','{}':types,'jax_md':facialRig,'jaxMorkon_md':md}
	search_replace_keys(srcFile,Dict,filePath)
	
	return True


def connectMaptoGeoGrp(mapFile,mapAttr,Asset,Types):
	'''
	this function will disconnect mapFileAttribute and 
	reconnect attribute to Geo_Grp base on shading connection publish system
	'''
	sourceCon = mc.listConnections(mapFile+'.alphaGain',s=1,p=1)
	if sourceCon !=None:
		mc.disconnectAttr(sourceCon[0],mapFile+'.alphaGain')
		geoGrp = '{}_md:Geo_Grp'.format(Asset)
		geoAttr = '{}'.format(mapAttr).replace('_1','')
		if not mc.objExists('{}.{}'.format(geoGrp,geoAttr)):
			mc.addAttr(geoGrp,ln=geoAttr,dv=0,k=1)
		mc.connectAttr(sourceCon[0],'{}.{}'.format(geoGrp,geoAttr))
		mc.connectAttr('{}.{}'.format(geoGrp,geoAttr),mapFile+'.alphaGain')

def allConnectMaptoGeoGrp(Asset,Types):
	'''
	this function will connect mapFileAttr to GeoGrp
	'''
	attrList = ['sc__MouthCrnUpL',
	'sc__MouthCrnUpR',
	'sc__MouthU',
	'sc__MouthUpL',
	'sc__MouthUpR',
	'sc__NeckL',
	'sc__NeckR',
	'sc__NosecheekUpL',
	'sc__NosecheekUpR',
	'sc__UnderEyeUpL',
	'sc__UnderEyeUpR',
	'sc__UpperNoseL',
	'sc__UpperNoseR',
	'sc__Chin',
	'sc__ForeheadDnL',
	'sc__ForeheadDnR',
	'sc__ForeheadDnMid',
	'sc__ForeheadUpL',
	'sc__ForeheadUpR',
	'sc__MouthCrnDnL',
	'sc__MouthCrnDnR']
	mapList = mc.ls('{}_mdlMainMtr:{}*_*{}*'.format(Asset,Asset,types),type='file')

	for i in attrList:
		mapName = i.split('sc__')[-1]
		for j in mapList:
			if re.search('{}_'.format(mapName),j):
				connectMaptoGeoGrp(j,i,Asset,Types)

def disconnectOutputMap(asset,types):
	'''
	use for disconnect attr of textureMap from layeredTexture node
	'''
	inputList = []
	mapList = ['{}_mdlMainMtr:{}_{}_Chin_1'.format(asset,asset,types),
	'{}_mdlMainMtr:{}_{}_NeckR_1'.format(asset,asset,types),
	'{}_mdlMainMtr:{}_{}_NeckL_1'.format(asset,asset,types),
	'{}_mdlMainMtr:{}_{}_MouthUpL_1'.format(asset,asset,types),
	'{}_mdlMainMtr:{}_{}_MouthU_1'.format(asset,asset,types),
	'{}_mdlMainMtr:{}_{}_MouthUpR_1'.format(asset,asset,types),
	'{}_mdlMainMtr:{}_{}_ForeheadDnR_1'.format(asset,asset,types),
	'{}_mdlMainMtr:{}_{}_ForeheadDnL_1'.format(asset,asset,types),
	'{}_mdlMainMtr:{}_{}_MouthCrnDnR_1'.format(asset,asset,types),
	'{}_mdlMainMtr:{}_{}_MouthCrnDnL_1'.format(asset,asset,types)]
	for mapFile in mapList:
		desCon = pmc.PyNode(mapFile).outputs(c=1,p=1,t='layeredTexture')
		desNode = pmc.PyNode(mapFile).outputs(p=0,t='layeredTexture')
		print desCon
		#desNode =  list(set(desNode))
		#print desNode
		
		for source,destination in desCon:
			mc.disconnectAttr(source.name(),destination.name())
			num = re.findall('[[0-9]*]'.format(destination.nodeName()),destination.name())[0]
			if num not in inputList:
				inputList.append(num)
		print inputList
		
		for i in inputList:
			delMap = destination.nodeName()+'.inputs{}'.format(i)
			print delMap
			mel.eval('removeMultiInstance -break true "{}" ;'.format(delMap))



def getRigDataFld():
    scnPath = os.path.normpath( mc.file( q = True , l = True )[0])
    #print scnPath
    tmpAry = scnPath.split( '\\' )
    #print tmpAry
    tmpAry[3] = 'publ'
    tmpAry[-3] = 'data'
    
    dataFld = '\\'.join( tmpAry[0:-2] )
    
    if not os.path.isdir( dataFld ) :
        os.mkdir( dataFld )
    
    return dataFld

def createCombineFromTmpDict(Asset,Types,fcRig,geo=0):
	
	filePathName = getRigDataFld()
	project = filePathName.split('\\')[1]
	
	#print outputPath
	thisModulePath =  os.path.dirname(sys.modules[__name__].__file__)
	#print thisModulePath
	if geo ==0:
		TmpPath = thisModulePath + '\\{}'.format(project)+'\\facialTmp.yml'
		ymlFile = '{}_{}_FclCombineDictMap.yml'.format(Asset,Types)
	else:
		TmpPath = thisModulePath + '\\{}'.format(project)+'\\facialTmpWithGeo.yml'
		ymlFile = '{}_{}_FclCombineDictMapWithGeo.yml'.format(Asset,Types)
	#print TmpPath
	print filePathName +'\\facialMap'
	if not os.path.exists(filePathName +'\\facialMap'):
		print 'create faceialMapFolder'
		os.mkdir(filePathName+'\\facialMap')
	outputPath = (filePathName)+'\\facialMap\\{}'.format(ymlFile)
	print outputPath
	reWriteFacialMap(Asset,Types,fcRig,TmpPath,outputPath)


def readDictFromRigData(types = '',geo=0):
	filePathName = getRigDataFld()
	asset = filePathName.split('\\')[5]
	if geo ==0:
		combineDict = '{}_{}_FclCombineDictMap.yml'.format(asset,types)
	else:
		combineDict = '{}_{}_FclCombineDictMapWithGeo.yml'.format(asset,types)

	combineDictPath = filePathName+'\\facialMap\\'+combineDict
	print combineDictPath
	readCombineDict(combineDictPath)
	if geo ==1:
		disconnectOutputMap(asset,types)

def deleteUnUseMapFile(asset,types):
    mapList =mc.ls('{}_mdlMainMtr:{}*_*{}*'.format(asset,asset,types),type='file')
    for i in mapList:
        if mc.getAttr(i+'.fileTextureName')=='':
            mc.delete(i)


'''
def runDictMap(mapDict):
	##### This Function is no longer be used in this task ############
	##### This Funtion is use for re Create node and re Connect node base on
	##### Data that write in mapDict.

	
	for key in mapDict.keys():
		#print key
		if not mc.objExists(key):
			mc.createNode(mapDict[key]['types'],n= key )
	
	for key in mapDict.keys():	
		for conList in mapDict[key]['con']:
			if not mc.objExists(str(conList[1])):
				print conList[1]
				nodeName =conList[1].split('.')[0]
				attrName = '.'.join(conList[1].split('.')[1:])
				print attrName
				mc.addAttr(nodeName,ln = attrName,dv=0,k=1)
			if not mc.objExists(str(conList[0])):
				nodeName =conList[0].split('.')[0]
				attrName = '.'.join(conList[0].split('.')[1:])
				print attrName
				mc.addAttr(nodeName,ln = attrName,dv=0,k=1)
			if not mc.isConnected(conList[1],conList[0]):
				mc.connectAttr(conList[1],conList[0])
				#print conList[1] + ': connect '+ conList[0]
			else:
				pass
'''


		


#reWriteFacialMap('jaxMorkon_mdlMainMtr:jaxMorkon','{}','jax_md','C:/Users/pornpavis.t/Desktop/TestCache/facialTmp.yml','C:/Users/pornpavis.t/Desktop/TestCache/JaxTmp.yml')
#selectMapFile('jaxMorkon','{}')
#allConnectMaptoGeoGrp(':*jaxMorkon','{}')
#x=createCombineMap('jaxMorkon','{}')
#readCombineMap(x)
#readCombineDict('C:/Users/pornpavis.t/Desktop/TestCache/JaxConnectWithGeoGrp.yml')
#disconnectOutputMap('jaxMorkon','{}')
#writeMapDict('C:/Users/pornpavis.t/Desktop/TestCache/JaxConnectWithGeoGrp.yml',x)
#reWriteFacialMap('<Asset>','<Types>','<FaceRig>','C:/Users/pornpavis.t/Desktop/TestCache/JaxConnectWithGeoGrp.yml','C:/Users/pornpavis.t/Desktop/TestCache/ConnectWithGeoGrpTmp.yml')
#reWriteFacialMap('BerserkerLeader','DM','BerserkerLeader_md','C:/Users/pornpavis.t/Desktop/TestCache/ConnectWithGeoGrpTmp.yml','C:/Users/pornpavis.t/Desktop/TestCache/BerserkerLeadGeoGrpTmp.yml')$