import maya.cmds as mc

# check if window exists
if mc.window ( 'connectOAUI' , exists = True ) :
	mc.deleteUI ( 'connectOAUI' ) 
else : pass

class ConnectOA(object):
	def __init__(self):
		self.winName = 'connectOAUI'
					
	def buildconnectOAUI(self):
		winWidth = 360
		winWidthBut = [winWidth*0.1,winWidth*0.8,winWidth*0.1]
		winWidthOri = [winWidth*0.1,winWidth*0.2,winWidth*0.6,winWidth*0.1]
		winWidthCB = [winWidth*0.25,winWidth*0.2,winWidth*0.2,winWidth*0.2]
		#window = mc.window('connectOAUI',title = 'Connect OA' , width = winWidth)
		self.mainCL = mc.columnLayout('addRigToOA')
		mc.rowLayout()
		mc.text(l='',h=19)
		mc.setParent('..')
		mc.text(l="                                             ..:: Add Rig To OA ::..",al='center')
		mc.text(l='',h=12)
		mc.rowLayout(numberOfColumns=4,columnWidth4=winWidthOri)
		mc.text(label = '', align='center',width=winWidthOri[0])
		mc.text(label = 'name space' ,width=winWidthOri[1])
		mc.textField('nameSpace' ,ec='namespace:', ed = True,width=winWidthOri[2])
		mc.text(label = '', align='center',width=winWidthOri[3])
		mc.setParent('..')
		mc.rowLayout()
		mc.text(l='',h=5)
		mc.setParent('..')
		mc.rowLayout(numberOfColumns=2,columnWidth2=[winWidth*0.08,winWidth*0.9])
		mc.text(label = '', align='center',width=winWidth*0.08)
		mc.checkBoxGrp('grpConnect',l='Grp Connect',numberOfCheckBoxes=3,la3=['FacialRig','AddRig','BpmRig'],cl4=['left','left','left','left'],columnWidth4=winWidthCB,v1=True)
		mc.setParent('..')
		mc.rowLayout()
		mc.text(l='',h=5)
		mc.setParent('..')
		mc.rowLayout(numberOfColumns=3,columnWidth3=winWidthBut)
		mc.text(label = '', align='center',width=winWidthBut[0])
		mc.button('connectCtrl',l='Connect', width=winWidthBut[1] , height = 40 , al='center' , c = self.connectAnimToOA_cmd)
		mc.text(label = '', align='center',width=winWidthBut[0])
		mc.setParent('..')
		mc.rowLayout()
		mc.text(l='',h=5)
		mc.setParent('..')
		mc.rowLayout(numberOfColumns=3,columnWidth3=winWidthBut)
		mc.text(label = '', align='center',width=winWidthBut[0])
		mc.button('Bake Animation', width=winWidthBut[1] , height = 40 , al='center' ,c = self.bakeAnimCtrl)
		mc.text(label = '', align='center',width=winWidthBut[0])
		mc.setParent('..')
		mc.rowLayout()
		mc.text(l='',h=5)
		mc.setParent('..')
		mc.rowLayout(numberOfColumns=3,columnWidth3=winWidthBut)
		mc.text(label = '', align='center',width=winWidthBut[0])
		mc.button('Select OA Ctrl', width=winWidthBut[1] , height = 40 , al='center' ,c = self.selectOACtrl_cmd)
		mc.text(label = '', align='center',width=winWidthBut[0])
		mc.setParent('..')
		mc.rowLayout()
		mc.text(l='',h=5)
		mc.setParent('..')
		#mc.showWindow()
		return self.mainCL
		
	def connectAnimToOA_cmd(self, *args):
		NS = mc.textField('nameSpace',q=True, tx=True)
		locoName = mc.ls('Rig_Grp_*')[0].split('Rig_Grp_')[-1]
		grpConnect = mc.checkBoxGrp('grpConnect',query=True, va3=True)
		grp = []
		if grpConnect[0] == True:
			grp.append('FacialCtrl_Grp')
		if grpConnect[1] == True: 
			addrigGrp = ['AddRigCtrlPar_Grp','AddRigCtrlPars_Grp']
			for each in addrigGrp:
				obj = mc.objExists(each)
				if obj == True:
					grp.append(each)
		if grpConnect[2] == True:
			grp.append('BpmRigCtrlPar_Grp')
		for eachGrp in grp:
			grpOA = '{}_{}'.format(eachGrp,locoName)
			grpC = mc.listRelatives('{}{}'.format(NS,eachGrp),ad=True,type = 'transform')	
			Ctrl = []
			LocoCtrl = []
			for each in grpC:
				if '_Ctrl' in each:
					Ctrl.append(each)
				if '_ctrl' in each:
					Ctrl.append(each)
				else: pass
			for eachCnnt in Ctrl:
				attrCtrl = mc.listAttr(eachCnnt,k=True,u=True)
				obj = eachCnnt.split(':')[-1]
				objLoco = '{}_{}'.format(obj,locoName)
				LocoCtrl.append(objLoco)
				for cnnt in attrCtrl:
					mc.connectAttr('{}.{}'.format(eachCnnt,cnnt) , '{}.{}'.format(objLoco,cnnt))
		print "-----------DONE-----------"

	def selectOACtrl_cmd(self,*args):
		locoName = mc.ls('Rig_Grp_*')[0].split('Rig_Grp_')[-1]
		grpConnect = mc.checkBoxGrp('grpConnect',query=True, va3=True)
		grp = []
		if grpConnect[0] == True:
			grp.append('FacialCtrl_Grp_{}'.format(locoName))
		if grpConnect[1] == True: 
			addrigGrp = ['AddRigCtrlPar_Grp_{}'.format(locoName),'AddRigCtrlPars_Grp_{}'.format(locoName)]
			for each in addrigGrp:
				obj = mc.objExists(each)
				if obj == True:
					grp.append(each)
		if grpConnect[2] == True:
			grp.append('BpmRigCtrlPar_Grp_{}'.format(locoName))
		grpC = mc.listRelatives(grp,ad=True,type = 'transform')
		Ctrl = []
		for each in grpC:
			if '_Ctrl' in each:
				Ctrl.append(each)
			if '_ctrl' in each:
				Ctrl.append(each)
			else: pass
		mc.select(Ctrl)
		result = Ctrl
		return result

	def bakeAnimCtrl(self,*args):
		stFrm = mc.playbackOptions(q = True , ast = True )
		EdFrm = mc.playbackOptions(q = True , aet = True )
		ctrl = self.selectOACtrl_cmd()
		print ctrl
		mc.bakeResults(ctrl,simulation=True,t=( stFrm , EdFrm ))


#A = ConnectOA()
#A
