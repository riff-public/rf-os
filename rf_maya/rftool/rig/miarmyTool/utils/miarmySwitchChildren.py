import maya.cmds as mc
import re
from collections import OrderedDict
import sys


class MiarmySwitchChildren:
	def __init__(self,**kwargs):
		self.sel_agentGrp_0 = []
		self.sel_agentGrp_1 = []
		self.sel_child_0 = []
		self.sel_child_1 = []
		self.normalLs = []
		
		self.func = []
		self.motherScLs = None

		self.suffixNameRad = None
		self.sameRad = None
		self.noSameRad = None

		self.winLayout = None

		self.height = 800 
		self.width = 320
		self.contentList = ['Setup','OriginalAgent','Action','StoryList','TransitionMap','Decision','Geometry','LODMid','LODFar','PlacerShape','RandomShader','FootMap','Misc']

	def selectTwoAgents(self,*args):
		sel_agentGrp = mc.ls(sl=1)
		if len(sel_agentGrp) == 2:
			self.sel_agentGrp_0 = sel_agentGrp[0]
			self.sel_agentGrp_1 = sel_agentGrp[1]
			if mc.nodeType(self.sel_agentGrp_0) == "McdAgentGroup" and mc.nodeType(self.sel_agentGrp_1) == "McdAgentGroup":
				self.sel_child_0 = mc.listRelatives(self.sel_agentGrp_0,c=1,pa=1,f=1)
				self.sel_child_1 = mc.listRelatives(self.sel_agentGrp_1,c=1,pa=1,f=1)
				for x in self.sel_child_0:
					normal = x.split("%s|"%self.sel_agentGrp_0)
					self.normalLs.append(normal[1])
			else:
				mc.confirmDialog(m="These selections are not McdAgentGroup")
		else:
			mc.confirmDialog(m="Please select 2 agent groups")
			
		dicts = {"agents":[{"mother":self.sel_child_0},{"child":self.sel_child_1},{"normal":self.normalLs}]}
		return dicts

	def reorderObject(self,*args):
		get_truePosi = []
		itemIndex = mc.textScrollList(self.motherScLs,q=1,sii=1)
		for x in itemIndex:
			truePosi = len(self.sel_child_0) - x
			get_truePosi.append(truePosi)
		return get_truePosi
	'''
	def switchSelectedItemInAgent(self,*args):
		mother = self.selectTwoAgents()["agents"][0]["mother"]
		child = self.selectTwoAgents()["agents"][1]["child"]
		selectItem = mc.textScrollList(self.motherScLs,q=1,si=1)
		reposi = self.reorderObject()
		get_mother = []
		get_child = []
		if len(selectItem) > 0:
			if mc.radioButton(self.sameRad,q=1,sl=1):
				for x,mot in enumerate(child):
					for cld in selectItem:
						if cld in mot:
							print cld
							get_mother.append(mother[x])
							get_child.append(child[x])
				for x,obj in enumerate(get_mother):
					mc.delete(obj)
					mc.parent(get_child[x],self.sel_agentGrp_0)
					mc.reorder(obj,r=reposi[x]*-1)
			if mc.radioButton(self.noSameRad,q=1,sl=1):
				get_name = []
				for obj in selectItem:
					name = re.findall(".*_",obj)
					get_name.extend(name)
				for x,tar in enumerate(mother):
					for y,ob in enumerate(get_name):
						mot = re.findall(".*{}.*".format(ob),tar)
						get_mother.extend(mot)
						if re.findall(".*{}.*".format(ob),child[x]):
							cld = re.findall(".*{}.*".format(ob),child[x])
							get_child.extend(cld)
						elif re.findall("{}.*".format(ob),child[x]):
							cld = re.findall("{}.*".format(ob),child[x])
							get_child.extend(cld)
				for x,obj in enumerate(get_mother):
					mc.delete(obj)
					mc.parent(get_child[x],self.sel_agentGrp_0)
					mc.reorder(get_child[x],r=reposi[x]*-1)
	'''
	def switchSelectedItemInAgent(self,*args):
		self.func = self.selectTwoAgents()
		sel = mc.ls(sl=True)
		if len(sel) ==2:
			mother = sel[0]
			child = sel[1]
			agNameList = []
			for i in sel:
				if '|' in i:
					agNode = i.split('|')[-1]
					agName = agNode.split('Agent_')[-1]
					agNameList.append(agName)
			#suffixCheck = mc.radioCollection('suffixNameRad',q=1,sl=1)
			motherContent = mc.listRelatives(mother,c=1,f=1)
			childContent = mc.listRelatives(child,c=1,f=1)
			
			selCount = mc.textScrollList(self.motherScLs,q=1,sii=1)
			if selCount != None:
				for num in selCount:
					content = self.contentList[int(num)-1]
					motherFound = 0
					childFound = 0
					for motherCon in motherContent:
						if content in motherCon:
							#print content
							print motherCon
							motherDel = motherCon
							motherFound =1
							break
					for childCon in childContent:
						if content in childCon:
							#print content
							print childCon
							childPar = childCon
							childFound = 1
							break
					#print motherFound
					#print childFound
					if (motherFound ==1) and (childFound ==1):
						mc.delete(motherDel)
						mc.parent(childPar,mother)
					else:
						print 'content not found'
						
				
				worldObj = mc.parent(mc.listRelatives(mother,c=1,f=1),w=1)
				
				for content in self.contentList:
					for obj in worldObj:
						if content in obj:
							mc.parent(obj,mother)
			else:
				mc.confirmDialog(m='please select atleast 1 content')
		else:
			mc.confirmDialog(m='please select 2 agents')
	
	def switchAllTransform(self,*args):
		self.func = self.selectTwoAgents()
		mc.delete(self.sel_child_0)
		mc.parent(self.sel_child_1,self.sel_agentGrp_0)
		if mc.objExists(self.sel_agentGrp_1):
			mc.delete(self.sel_agentGrp_1)
			#sys.stdout.write("Finished")
			print ('Finished '),
		
	def deleteImportedAgent(self,*args):
		#self.func = self.selectTwoAgents()
		if mc.objExists(self.sel_agentGrp_1):
			mc.delete(self.sel_agentGrp_1)
	#-------------------------------------------------------------------------

	def switchMiarmyChildrenUI(self,*args):
		self.winLayout = mc.columnLayout('switchMMChild',adj=1)
		mainLayout = mc.columnLayout(cal="center",co=["both",10],adj=1,h=self.height,w=self.width)
		
		#self.func = self.selectTwoAgents()
		textRow = mc.columnLayout(p=mainLayout)
		mc.separator(h=18)
		mc.text(l="					        ..:: Name List ::..")
		mc.separator(h=14)
		self.motherScLs = mc.textScrollList(w=360, ams=1, a = self.contentList)
		mc.setParent("..")
		 
		scrollLsRow = mc.rowLayout(nc=2,p=mainLayout)
		#self.suffixNameRad = mc.radioCollection('suffixNameRad')
		#self.sameRad = mc.radioButton('sameSuffix',l="Same suffix name",sl=1)
		#self.noSameRad = mc.radioButton('notSameSuffix',l="Not same suffix name")
		mc.setParent("..")

		buttonRow = mc.columnLayout(adj=1,p=mainLayout)
		mc.separator(h=10)
		mc.button(l="Switch by selection",h=50,c = self.switchSelectedItemInAgent)
		mc.separator(h=10)
		mc.button(l="Switch all",h=50,c = self.switchAllTransform)
		mc.separator(h=10)
		mc.button(l="Delete Secondary agent",ann="Delete a imported agent",h=50,c = self.deleteImportedAgent)

		return self.winLayout