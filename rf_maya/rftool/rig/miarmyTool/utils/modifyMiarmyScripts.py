import sys, re
import maya.cmds as mc
# sys.path.append(r'C:\Program Files\Basefount\Miarmy\Maya2018\scripts')

from rf_utils import publish_info
from rf_utils.context import context_info
import maya.cmds as cmds
from McdGeneral import *
from McdAgentManagerOldGUI import *
from McdGeneral import *


import McdActionFunctions as maf
reload(maf)
import McdSimpleCmd as msc
reload(msc)
from utaTools.utapy import utaCore
reload(utaCore)





def renameMM(name,newCreatureName):
	globalNode = McdGetMcdGlobalNode()
	orgActiveAgentName = cmds.getAttr(globalNode + ".activeAgentName")
	cmds.select(clear = True)
	#get id and name
	agentNode = "Agent_" + name
	#name = cmds.textField("tf_am_agtn" + str(index), q = True, tx = True)
	#try:
		#cmds.select(agentNode)
	#except:
		# ----------- referencing pipeline tracker : block changing agent content name -------------------
		#if isReferenceScene():
			#cmds.confirmDialog(t = "Error", m = "Cannot change the names of referenced agent")
			#return
		#cmds.confirmDialog(t = "Error", m = "Incorrect for naming Agent Node, Miarmy > Miarmy Contents Check.")
		#return
	#result = cmds.promptDialog( title='Modify Name:', message='Enter New Creature Name:', \
							  #button=['OK', 'Cancel'], defaultButton='OK', cancelButton='Cancel', dismissString='Cancel')
	#newCreatureName = ""
	#if result == "OK":
		#newCreatureName = cmds.promptDialog(query=True, text=True)

	#if newCreatureName == "":
		#return

	newAgentNode = "Agent_" + newCreatureName;
	exist = cmds.ls(newAgentNode)
	if exist != [] and exist != None:
		cmds.confirmDialog(t = "Error", m = "New name exist, try another one.")
		raise Exception("New name exist, try another one.")


	allGrpsFullPath = cmds.listRelatives(agentNode, f = True, c = True)
	allGrps = cmds.listRelatives(agentNode, c = True)

	for i in range(len(allGrps)):
		if (allGrps[i].find('_' + name)>0):
			prefix = allGrps[i].split('_' + name)[0]
			newName = prefix + '_' + newCreatureName
			cmds.rename(allGrpsFullPath[i], newName)

	newAgentNode = "Agent_" + newCreatureName
	cmds.rename(agentNode, newAgentNode)

	# not chang setup file:
	allChild = cmds.listRelatives(newAgentNode, c = True)
	allChildFilter = []
	for i in range(len(allChild)):
		if allChild[i].find("Setup_")!=0:
			allChildFilter.append(allChild[i])

	geoGroup = ""
	tempList = []
	for i in range(len(allChildFilter)):
		if allChildFilter[i].find("Geometry_") < 0:
			tempList.append(allChildFilter[i])
		else:
			geoGroup = allChildFilter[i]
	allChildFilter = tempList

	cmds.select(allChildFilter)

	cmds.select(hi = True)
	cmds.select(allChildFilter, d = True)
	selObj = cmds.ls(sl = True, l = True)
	nbSelObj = len(selObj)
	
	maxHi = 0
	hiNameContainer = []
	appended = 0
	totalLevel = 0
	for i in range(100):
		thisLevelElement = []
		for j in range(len(selObj)):
			if selObj[j].count('|') == i:
				thisLevelElement.append(selObj[j])
				appended += 1
		hiNameContainer.append(thisLevelElement)
		if appended == nbSelObj:
			totalLevel = i
			break;

	for i in range(totalLevel):
		i = totalLevel -i;
		for j in range(len(hiNameContainer[i])):
			item = hiNameContainer[i][j];
			realName = item.split('|')[-1]
			if realName.find('_' + name) > 0:
				if realName.split('_' + name)[-1] == "":
					prefix = realName.split('_' + name)[0]
					newName = prefix + '_' + newCreatureName
					cmds.rename(item, newName)
				else:
					dealWithOtherPossible(item, newCreatureName)
			else:
				if realName.find("ActionShell_") < 0:
					dealWithOtherPossible(item, newCreatureName)

	#activate and refresh:
	cmds.setAttr(globalNode + ".activeAgentName", newCreatureName, type = "string")
	
	# find the replace name of geometries:
	allGeoNodes = cmds.listRelatives(geoGroup, c = True, p = False, ad = True, path = True)
	if MIsBlank(allGeoNodes):
		return;
	
	allGeoNodes.reverse()
	allGeoNodesTrans = []
	
	findPart = orgActiveAgentName + "Geo"
	replacePart = newCreatureName + "Geo"
	for i in range(len(allGeoNodes)):
		
		if cmds.nodeType(allGeoNodes[i]) == "mesh":
			fullName = allGeoNodes[i]
			nodeName = allGeoNodes[i].split("|")[-1]
			
			if nodeName.find(findPart) > 0:
				newName = nodeName.replace(findPart, replacePart)
				# print ""
				# print allGeoNodes[i]
				# print newName
				# print fullName
				# print newName
				try:
					cmds.rename(fullName, newName, ignoreShape = True)
				except:
					pass
		else:
			allGeoNodesTrans.append(allGeoNodes[i])
			
			
	allGeoNodes = allGeoNodesTrans
	# the 2nd time, only work with geo trans:
	for i in range(len(allGeoNodes)):
		
		fullName = allGeoNodes[i]
		nodeName = allGeoNodes[i].split("|")[-1]
		
		if nodeName.find(findPart) > 0:
			newName = nodeName.replace(findPart, replacePart)
			# print ""
			# print allGeoNodes[i]
			# print newName
			# print fullName
			# print newName
			try:
				cmds.rename(fullName, newName, ignoreShape = True)
			except:
				pass



def McdCreateActionCmd(actionName):
    # parse origianl agent group
    # and the root bone:(select the root bone)
    globalNode = McdGetMcdGlobalNode()
    activeAgentName = cmds.getAttr(globalNode + ".activeAgentName")
    isValid = CheckStringIsValid(activeAgentName)
    if isValid == True:
        miarmyGrp = cmds.ls("Miarmy_Contents")
        # if miarmyGrp == [] or miarmyGrp == None:
        #     cmds.confirmDialog(t = "Error", m = "Cannot find Miarmy_Contents group.")
        #     raise Exception("Cannot find Miarmy_Contents group.")
        
        agentGrp = cmds.ls("Agent_" + activeAgentName)
        # if agentGrp == [] or agentGrp == None:
        #     cmds.confirmDialog(t = "Error", m = "Cannot find Agent group, cannot create action node for this active agent.")
        #     raise Exception("Cannot find Agent group, cannot create action node for this active agent.")
        
        setupGrp = cmds.ls("Setup_" + activeAgentName)
        # if setupGrp == [] or setupGrp == None:
        #     cmds.confirmDialog(t = "Error", m = "Cannot find Agent setup rig group, cannot create action node for this active agent.")
        #     raise Exception("Cannot find Agent setup rig group, cannot create action node for this active agent.")
            
        actGrp = cmds.ls("Action_" + activeAgentName)
        # if actGrp == [] or actGrp == None:
        #     cmds.group(n = "Action_" + activeAgentName, em = True)
        #     try:
        #         cmds.parent("Action_" + activeAgentName, "Agent_" + activeAgentName)
        #     except:
        #         cmds.confirmDialog(t = "Error", m = "May be you have naming problem, check it firstly in \"Miarmy > Miarmy Contents Check.\"")
        #         raise Exception("May be you have naming problem, check it firstly in \"Miarmy > Miarmy Contents Check.\"")
            
        #maf.McdCheckRepeatNameTreeAct(setupGrp, "joint")
            
        cmds.select("Setup_" + activeAgentName)
        # set name
        startTime = cmds.playbackOptions(q = True, min = True)
        endTime = cmds.playbackOptions(q = True, max = True)
        
        option = 'Proceed'
        if option == "Proceed":
            newAction = actionName
            isVaild = CheckStringIsValid(newAction)
            if isVaild == True:
                newActionNodeName = newAction + "_action_" + activeAgentName
                actGrp = cmds.ls(newActionNodeName)
                if actGrp == [] or actGrp == None:
                    newNodeName = mel.eval("McdCreateActionCmd;")
                    try:
                        cmds.select(newNodeName)
                        cmds.parent(newNodeName, "Action_" + activeAgentName)
                    except:
                        # cmds.confirmDialog(t = "Error", m = "May be you have naming problem, check it firstly in \"Miarmy > Miarmy Contents Check.\"")
                        # raise Exception("May be you have naming problem, check it firstly in \"Miarmy > Miarmy Contents Check.\"")
                        pass
                    try:
                        selObj = cmds.ls(sl = True)[0]
                        cmds.rename(selObj, newActionNodeName)
                    except:
                        # cmds.confirmDialog(t = "Warning", m = "Naming node error, please rename it manually: <actionName>_action_<agentName>")
                        pass
                    
                    actionNode = cmds.ls(sl = True)[0]
                    #stepIntoActionSetupWizard(actionNode)
                    
                else:
                	pass
                    # cmds.confirmDialog(t = "Abort", m = "Action name exist. System select it(them) automatically.")
                    # cmds.select(actGrp)
            else:
                # cmds.confirmDialog(t = "Abort", m = "The new action name: \"" + newAction + "\" you specified is invalid.")
                pass
        
            try:
                nodeName = newActionNodeName
                lenth = cmds.getAttr(nodeName + ".length")
                setValue = int(0.80 * float(lenth))
                cmds.setAttr(nodeName + ".entryMax", setValue)
            except:
                pass
    if newActionNodeName:
    	return newActionNodeName

def proceessLists (publishInfo):
	process = publishInfo.entity.process
	processSp  = process.split('_')

	if len(processSp) == 1:
	    processName = processSp[0]
	    processSubName = ''
	    processNameSp = processName.split('Rig')[0]
	    processObj = processNameSp.capitalize()

	else:
	    processName = processSp[0]
	    processSubName = '_' + processSp[-1]
	    processNameSp = processName.split('Rig')[0]
	    processObj = processNameSp.capitalize()

	return processName, processNameSp, processObj, processSubName          

def createMcdActionNode():

	asset = context_info.ContextPathInfo()
	asset.context.update(res='md')
	publishInfo = publish_info.PreRegister(asset)


	pass
	processName, processNameSp, processObj, processSubName = utaCore.proceessLists(publishInfo)
	print processName, processNameSp, processObj, processSubName
	McdCreateActionCmd(processName)

def McdSelectMcdGlobalWOconfig(showMsg = True):
    # check load plugin:
    miarmy = ""
    allPlugins = cmds.pluginInfo( query = 1, listPlugins = 1 )

    for i in range(len(allPlugins)):
        if allPlugins[i].find("iarmy") > 0:
            miarmy = allPlugins[i]      
    if miarmy == "":
        cmds.confirmDialog(t = "Error", m = "Not load Miarmy Crowd plugin, please first load it.")
        return
    msc.McdClearUselessNodes()
    cmds.playbackOptions( view = "all" ) # update all, feedback usage
    cmds.playbackOptions( playbackSpeed = 0 ) # play every frame
    cmds.playbackOptions( by = 1.0 ) # by 1.0 for each update
    cmds.playbackOptions( maxPlaybackSpeed = 1 ) # max speed, 24fps
    
    #create McdBrain and McdBrainPost
    allSolveNode = cmds.ls(type = "McdBrain")
    if allSolveNode == [] or allSolveNode == None:
        cmds.createNode("McdBrain")
        
        
    allSolveNode = cmds.ls(type = "McdBrainPost")
    if allSolveNode == [] or allSolveNode == None:
        cmds.createNode("McdBrainPost")
    
    #create McdGlobal and contents
    allMcdGlobal = cmds.ls(type = "McdGlobal");
    allContentsNodes = cmds.ls("Miarmy_Contents")
    if allContentsNodes == [] or allContentsNodes == None:
        if allMcdGlobal != [] and allMcdGlobal != None:
            for i in range(len(allMcdGlobal)):
                try:
                    cmds.delete(allMcdGlobal[i])
                except:
                    pass
    
    allMcdGlobal = cmds.ls(type = "McdGlobal");        
    if allMcdGlobal == [] or allMcdGlobal == None:
        McdCreateMcdGlobal()
        allMcdGlobal = cmds.ls(type = "McdGlobal");
        cmds.select(allMcdGlobal[0]);
    else:
        mel.eval("McdSimpleCommand -exe 1;")
    setupConfig()

def stepIntoActionSetupWizard(actionNode):

    cmds.setAttr(actionNode + ".txState", 0)
    cmds.setAttr(actionNode + ".tyState", 0)
    cmds.setAttr(actionNode + ".tzState", 0)
    cmds.setAttr(actionNode + ".rxState", 0)
    cmds.setAttr(actionNode + ".ryState", 0)
    cmds.setAttr(actionNode + ".rzState", 0)
        
    # rebuild last;
    mel.eval("McdSetAgentDataCmd;")