import maya.cmds as mc
import pymel.core as pm
from collections import OrderedDict
import sys, re
sys.path.append("P:/DarkHorse/Research/Hong/Miarmy/utils")
import mmOaPrepUtils as mmoap
reload(mmoap)

import mmAgentUtils as mmoa
reload(mmoa)

import mmBakeAnimUtils as mmbau
reload(mmbau)

import miarmySwitchChildren as msct
reload(msct)

import miarmyNewPlacement as mmnp
reload(mmnp)

class mmCustomUI():
	def __init__(self):
		self.winName = 'mmUI'
	
	#def getNamespace(self,*args):
		#self.ns = pm.selected()[0].namespace()
		#mc.textField('ns',w=20,e=1,tx=self.ns,bgc=[.5,1,.5])

	#-----------------------------------------------------------------------------
	#----------- OaPrep Function ----------------------------------------#
	#-----------------------------------------------------------------------------

	def runCleanForMiarmy(self,*args):
		mmoap.runCleanForMiarmy()



	#-----------------------------------------------------------------------------
	#----------- Animation Bake Function ----------------------------------------#
	#-----------------------------------------------------------------------------
	def getAllNameSpace(self,*args):
		getName = []
		mesh = mc.ls(type="joint")
		for i in mesh:
			if re.search(":",i):
				name, rem = i.split(":")
				if re.findall(".*[0-9]",name):	
					getName.append(name)
		self.namespaceList = list(OrderedDict.fromkeys(getName))
		return self.namespaceList
	#---------------------------------------------- ^
	#---------------------------------------------- ^
	#---------------------------------------------- ^ get namespace
	
	def addItems(self,*args):
		list_name = []
		sel = mc.ls(sl=1)
		if len(sel) > 0:
			for i in sel:
				if re.search(":",i):
					namespace_name,object_name = i.split(":")
					list_name.append(namespace_name)
					
					addItem =  mc.textScrollList(self.namespaceList,q=1,ai=1)
					if addItem == None:
						addItem = []
					compareItem = [x for x in list_name if x not in addItem]
					mc.textScrollList(self.namespaceList,e=1,a=compareItem)
				else:
					mc.confirmDialog(m="This object has not namespace")
		else:
			mc.confirmDialog(m="Please select object !!!")
	
	def removeItems(self,*args):
		removeItem = mc.textScrollList(self.namespaceList,q=1,si=1)
		if removeItem != None:
			mc.textScrollList(self.namespaceList,e=1,ri=removeItem)
		else:
			mc.confirmDialog(m="Please select items in textScrollList !!!")

	def resetItems(self,*args):
		resetItems = mc.textScrollList(self.namespaceList,q=1,ai=1)
		newItems = self.getAllNameSpace()
		if resetItems == None:
			mc.textScrollList(self.namespaceList,e=1,a=newItems)
		else:
			mc.textScrollList(self.namespaceList,e=1,ra=True)
			mc.textScrollList(self.namespaceList,e=1,a=newItems)
	
	def selectItems(self,*args):
		selectItem = mc.textScrollList(self.namespaceList,q=1,si=1)
		if selectItem != None:
			mmbau.animMiarmyList(selectItem)
		else:
			mc.confirmDialog(m="Please select items in textScrollList !!!")
			
	def selectAllItems(self,*args):
		selectAllItem = mc.textScrollList(self.namespaceList,q=1,ai=1)
		if selectAllItem != None:
			mmbau.animMiarmyList(selectAllItem)
		else:
			mc.confirmDialog(m="There is not any item in textScrollList !!!")
	
	#-----------------------------------------------------------------------------
	#----------- Original Agent Function ----------------------------------------#
	#-----------------------------------------------------------------------------
	def getLocoName(self,*args):
		locoNameUi = pm.textField( "txtImExportNodeField", q=1,text=1)
		return locoNameUi

	def resizePhyButton(self,*args):
		#locoNameUi = pm.textField( "txtImExportNodeField", q=1,text=1)
		sizeUi = pm.textField('sizeTextField',q=1 ,text=1)
		locoName = self.getLocoName()
		mmoa.resizePhyJoint(locoName,size = float(sizeUi))

	def MirrorMMBoxButton(self,*ards):
		mmoa.mirSelectedMMBoxGeo()
	
	def MulMcdCreateDecisionButton(self,*args):
		mmoa.mulMcdCreateDecision(locoName = self.getLocoName())


	def mmOAUI(self):
		ui = mc.window(self.winName,title = 'mmUI')
		mainLayout = mc.tabLayout('mainTab',h=230,w=300)
		prepLayout = mc.columnLayout('OAPrep',cal='center',co=['both',28],adj=1)
		#mc.separator(h=10,style ='none')
		#mc.text('Please fill namespace of character.')
		#mc.separator(h=5,style ='none')
		#nsTxField =mc.textField('ns',w=20,ed=0,bgc=[1,.5,.5])
		mc.separator(h=5,style ='none')
		#mc.button('getNS',l='get namespace',c = self.getNamespace)
		mc.separator(h=10,style ='none')
		mc.separator(h=10,style ='none')
		#mc.text('warning',l='Please check namespace before prese button.')
		#mc.text('warning2',l='')
		mc.separator(h=8,style ='none')
		mc.button('createOA',l='Create OA prepare group',w=120,h=60,c=self.runCleanForMiarmy)
		#mc.separator(h=10,style ='none')
		#mc.button('conHead',l='connectHead',w=120,h=30)
		#mc.separator(h=1,style ='none')
		#mc.button('disConHead',l='disconnectHead',w=120,h=30)
		#mc.separator(h=10,style ='none')
		#mc.button('conEye',l='connectEye',w=120,h=30)
		#mc.separator(h=1,style ='none')
		#mc.button('disConEye',l='disconnectEye',w=120,h=30)
		#mc.separator(h=14,style ='none')
		mc.setParent(mainLayout)

		OALayout = mc.columnLayout('OA',cal='center',co=['both',28],adj=1,p=mainLayout)
		rowLine1 = pm.rowColumnLayout( nc = 5 , p = OALayout , h = 30 )
		mc.separator(h=10,style ='none')
		pm.text( label='LocoName   : ' , p = rowLine1, al = "left")
		pm.rowColumnLayout(p = rowLine1 ,h=8)
		pm.textField( "txtImExportNodeField", text = "loco", p = rowLine1 , editable = True , w = 125)
		mc.separator(h=8,style ='none')
		pm.button ('exportAnimTxt', label =  "GET" , al = "center", w = 40, h=25, p = rowLine1 , bgc = (0.41, 0.41, 0.41), c = self.getLocoName)
		pm.rowColumnLayout (p = rowLine1 ,h = 8)
		
		rowLine2 = pm.rowColumnLayout( nc = 5 , p = OALayout , h = 30 )
		mc.separator(h=10,style ='none')
		pm.text( label='Resize Joint : ' , p = rowLine2, al = "left")
		pm.rowColumnLayout(p = rowLine2 ,h=8)
		pm.textField( "sizeTextField", text = "0.1", p = rowLine2 , editable = True , w = 125)
		mc.separator(h=8,style ='none')
		pm.button ('exportAnimTxt', label =  "GET" , al = "center", w = 40, h=25, p = rowLine2 , bgc = (0.41, 0.41, 0.41), c = self.resizePhyButton)
		pm.rowColumnLayout (p = rowLine2 ,h = 8)
		
		rowLine3 = pm.rowColumnLayout( nc = 4, p = OALayout , h = 30 )
		pm.button ('s', label =  "MirrorMMBox" , al = "center", w = 115, h=25, p = rowLine3 , bgc = (0.41, 0.41, 0.41), c = self.MirrorMMBoxButton)
		#pm.text( label=' ' , p = rowLine3, al = "left")
		mc.separator(w = 10,style ='double' , hr = False)
		pm.button ('a', label =  "Oa Decision" , al = "center", w = 115, h=25, p = rowLine3 , bgc = (0.41, 0.41, 0.41), c = self.MulMcdCreateDecisionButton)
		#pm.rowColumnLayout(p = rowLine3 ,h=8)
		#pm.button ('', label =  "OaDecision" , al = "center", w = 80, h=25, p = rowLine3 , bgc = (0.41, 0.41, 0.41))

		#mc.separator(h=14,style ='none',p=mainColumn)

		mc.setParent(mainLayout)
		AnimLayout = mc.columnLayout('Anim',cal='center',co=['both',28],adj=1,p=mainLayout)
		mc.separator(h=14,style ='none',p=AnimLayout)
		nameSpaceLs = self.getAllNameSpace()
		mc.text(l="Select namespace in a box")
		mc.separator(h=14,style ='none')
		self.namespaceList = mc.textScrollList(nr=6,ams=True,a=nameSpaceLs)
		mc.separator(h=14,style ='none')
		mc.setParent("..")
		mc.rowLayout(nc=3,p=AnimLayout,adj=1)
		mc.button(l="Add Item",w=170,h=50,c = self.addItems)
		mc.button(l="Remove Item",w=170,h=50,bgc=[0.5,0,0],c = self.removeItems)
		mc.button(l="Reset Item",w=170,h=50,bgc=[0.5,0.5,0],c = self.resetItems)
		mc.setParent("..")
		mc.separator(h=14,style ='none')
		mc.rowLayout(nc=2,p=AnimLayout,adj=1)
		mc.button(l="Bake Selected Anim",w=250,h=50,bgc=[0,0.5,0],c = self.selectItems)
		mc.button(l="Bake All Anim",w=250,h=50,bgc=[0,0.5,0],c = self.selectAllItems)

		mc.setParent(mainLayout)
		msct.MiarmySwitchChildren().switchMiarmyChildrenUI()

		mc.setParent(mainLayout)
		mmnp.Replacement().replacementUI()

	def showUI(self):
		if mc.window(self.winName,exists=1):
			print 'delUI'
			mc.deleteUI(self.winName)
		self.mmOAUI()
		mc.showWindow(self.winName)			
#y = mcxa.mocapXAnim()
#y.showMocapXAnimUI()