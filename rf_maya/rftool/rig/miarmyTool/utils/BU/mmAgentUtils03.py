import sys
sys.path.append('C:/Program Files/Basefount/Miarmy/Maya2018/scripts')
import maya.cmds as mc
import pymel.core as pm
import McdGeneral as mg
import McdDecisionEditor as mde
import maya.mel as mel
import McdSimpleCmd as msc
from collections import OrderedDict


def resizePhyJoint(locoName='loco',size = .1):
	pyJntList = mc.ls('*phyJoint_{}*'.format(locoName))
	for i in pyJntList:
		mc.setAttr(i+'.s',size,size,size,type='double3')

def mulMcdCreateDecision(locoName='loco'):
	decisionList = ['adaptTerrain','avoidEachOther','followRoad']
	decisionCommand = ['adapt height of terrain intensity 0.5','avoid near agents by turn and slow action priority 1','follow road priority 1']
	activeAgentName = mg.McdGetActiveAgentName()
	mg.McdCreateDecision('default',activeAgentName,absMode=False)
	for j in range(len(decisionList)):
		mg.McdCreateDecision(decisionList[j],activeAgentName,absMode=False)
		dcName = '{}_decision_{}'.format(decisionList[j],locoName)
		mde.cb_dno_active(0)
		mde.McdMenuFillDecision(decisionCommand[j],0,dcName)
	mde.MakeDecisionDefault('default_decision_{}'.format(locoName))

def mirrorMMBoxGeo(obj=''):
	if obj != '':
		copyGeo = mc.duplicate(obj,rc=1)
		mc.parent(copyGeo,w=1)
		mirGrp = mc.createNode('transform',n='mirGrp')
		mc.parent(copyGeo,mirGrp)
		if '_L_' in obj:
			mirGeo = obj.replace('_L_','_R_')
		elif '_R_' in obj:
			mirGeo = obj.replace('_R_','_L_')
		else:
			print 'there is no side to mirror'
			return
		mc.setAttr(mirGrp+'.sx',-1)
		copyGeoMat = mc.xform(copyGeo,q=1,ws=1,m=1)
		mc.xform(mirGeo,ws=1,m=copyGeoMat)
		mc.delete(mirGrp)

def mirSelectedMMBoxGeo():
	sel = mc.ls(sl=True)
	for i in sel:
		mirrorMMBoxGeo(i)

def createMiarmyOA(locoName='loco'):
	sel = mc.ls(sl=True)
	msc.McdSelectMcdGlobal()
	mc.parent('CharGeo_Grp','Skin_Grp','Setup_loco')
	McdParseRootBoneCreateOAgent(0)
	mc.select(sel)
	mulSnapMMBox(locoName)
	mullimbMMBox()

def orgGeoGrp():
	'''
	organize Miarmy Geo to have same Hiarchy as Geo
	'''
	agent = mc.ls(type= 'McdAgentGroup')[0]
	agentName = ('_').join(agent.split('_')[1:])
	agentGeo = agentName+'Geo'
	agentGeoGrp = 'Geometry_{}'.format(agentName)
	grp = mc.ls(sl=True)[0]
	meshList = mc.listRelatives(grp,c=1,ad=1,type = 'mesh')
	agentGrp = mc.createNode('transform',n='{}_{}'.format(grp,agentGeo))
	mc.parent(agentGrp,agentGeoGrp)
	#print meshList
	
	for i in meshList:
		transform = mc.listRelatives(i,p=1)[0]
		agentGeoName = '{}_{}'.format(transform,agentGeo)
		par = mc.listRelatives(transform,p=1)[0]
		parAgentGrp = '{}_{}'.format(par,agentGeo)
		if not mc.objExists(parAgentGrp):
			parAgentGrp = mc.createNode('transform',n= parAgentGrp)
			mc.parent(parAgentGrp,agentGrp)   
		if not mc.listRelatives(agentGeoName,p=1)[0]== parAgentGrp:
			mc.parent(agentGeoName,parAgentGrp)

def selectRbbSkin():
	sel = mc.ls(sl=True)
	for i in sel:
		name = i.split('_')[0].replace('RbbSkin','')
		print name
		jntList = mc.ls('*{}*'.format(name),type='joint')
		mc.select(jntList,add=1)
	mc.select(sel,d=1)

def findCon(jnt):
	findConList = mc.listConnections(jnt,s=0,d=1,type='parentConstraint',scn=1)
	conList = []
	if findConList!= None:
		for i in findConList:
			if i not in conList:
				conList.append(i)
	#print conList
	return conList


def validGeo(conNode):
	if mc.objExists(conNode):
		parGrp = mc.listRelatives(conNode,p=1)[0]
		meshList = mc.listRelatives(parGrp,c=1,type='mesh',ad=1)
		if meshList != None:
			for mesh in meshList:
				if 'ShapeOrig' in mesh:
					meshList.remove(mesh)
			print meshList
			return meshList
		else:
			return []

def bbFromMesh(mesh):
	oriMesh = pm.PyNode(mesh)
	meshGeo = oriMesh.getParent()
	sizeList = meshGeo.getBoundingBox(space='object',invisible=False)
	w = sizeList.width()
	h = sizeList.height()
	d =  sizeList.depth()
	#print [w,h,d]
	return [w,h,d]

def getBBFromMeshList(meshList):
	rbbList = [0,0,0]
	for m in meshList:
		bbList = bbFromMesh(m)
		w = bbList[0]
		h = bbList[1]
		d = bbList[2]
		if w > rbbList[0]:
			rbbList[0] = w
		if h > rbbList[1]:
			rbbList[1] = h
		if d > rbbList[2]:
			rbbList[2] = d
	
	return rbbList

def createBBBox(rbbList):
	cube1= mc.polyCube()[0]
	mc.setAttr(cube1+'.s',rbbList[0],rbbList[1],rbbList[2],type='double3')
	return cube1

def BBBoxToDumShape(box,dumShape):
	scl=pm.getAttr(box + '.scale')
	pm.setAttr(dumShape + '.scale',scl)
	#mc.delete(box)

def compareBBtoBox(bbList1,bbList2):
	bbList1Volume = bbList1[0]*bbList1[1]*bbList1[2]
	bbList2Volume = bbList2[0]*bbList2[1]*bbList2[2]
	
	if bbList1Volume >= bbList2Volume:
		return bbList1
	else:
		return bbList2
		
def snapMMBox(jnt,locoName):
	x = findCon(jnt)
	splitName = jnt.split('_')
	posDum = splitName[0] + 'Pos_'+'_'.join(splitName[1:])+'_dummyShape_{}'.format(locoName)
	scaDum= splitName[0] + 'Sca_'+'_'.join(splitName[1:])+'_dummyShape_{}'.format(locoName)
	brthDum= splitName[0] + 'Brth_'+'_'.join(splitName[1:])+'_dummyShape_{}'.format(locoName)
	anotherList = [posDum,scaDum,brthDum]
	print anotherList
	dumShape = jnt +'_dummyShape_{}'.format(locoName)

	if x != []:
		sclBB = [0,0,0]
		for i in x:
			mList = validGeo(i)
			print sclBB
			if mList != []:
				bbList = getBBFromMeshList(mList)
				sclBB = compareBBtoBox(bbList,sclBB)
		cube = createBBBox(sclBB)
		BBBoxToDumShape(cube,dumShape)
		for k in anotherList:
			if mc.objExists(k):
				BBBoxToDumShape(cube,k)
		mc.delete(cube)
		   

def mulSnapMMBox(locoName):
	skinJntList = mc.listRelatives('Root_Jnt',c=1,ad=1,type='joint')
	for i in skinJntList:
		snapMMBox(i,locoName)

'''
def snapMMBoxFromJnt(obj,locoName = 'loco'):
	con = mc.listRelatives(obj,c=1,ad=1,type='constraint')
	if con != None:
		jnt = mc.parentConstraint(con[0],q=1,tl=1)[0]
		mmBox = jnt+'_dummyShape_{}'.format(locoName)
		print mmBox
		snapMiarmyBB(mmBox,obj,jnt)
		name = jnt.split('_')[0]
		posJnt=jnt.replace(name+'_',name+'Pos_')
		scaJnt = jnt.replace(name+'_',name+'Sca_')
		brthJnt = jnt.replace(name+'_',name+'Brth_')
		
		if mc.objExists(posJnt):
			mmBox = mmBox.replace(name+'_',name+'Pos_')
			snapMiarmyBB(mmBox,obj,posJnt)
		
		if mc.objExists(scaJnt):
			mmBox = mmBox.replace(name+'_',name+'Sca_')
			snapMiarmyBB(mmBox,obj,scaJnt)
		
		if mc.objExists(brthJnt):
			mmBox = mmBox.replace(name+'_',name+'Brth_')
			snapMiarmyBB(mmBox,obj,brthJnt)
	else:
		#print rbnPrxList
		#print rbnJntList

		print 'There is no Constraint in {}'.format(obj)

def mulSnapMMBox2(locoName  = 'loco'):
	sel =mc.ls(sl=True)
	for i in sel:
		snapMMBoxFromJnt(i,locoName)

def createBBFromObj(obj):
	BB = mc.exactWorldBoundingBox(obj)
	BBWorldPosi = [float((BB[0]+BB[3])/2),float((BB[1]+BB[4])/2),float((BB[2]+BB[5])/2)]
	BBScale = [BB[0]-BB[3],BB[1]-BB[4],BB[2]-BB[5]]
	cube = mc.polyCube()[0]
	mc.setAttr(cube+'.t',BBWorldPosi[0],BBWorldPosi[1],BBWorldPosi[2],type='double3')
	mc.setAttr(cube+'.s',BBScale[0],BBScale[1],BBScale[2],type='double3')
	return cube
'''
def delSelPhyJnt(locoName = 'loco'):
	phyJnt = mc.ls('*phyJoint_{}'.format(locoName))
	mc.select(phyJnt,d=1)

def copyCostumeWeight(ns):
	sel= mc.ls(sl=True)
	noJnt = []
	for i in sel:
		splitList = i.split('_')
		geo = '_'.join(splitList[:-1])
		nsGeo = '{}:{}'.format(ns,geo)
		skinClus = mel.eval('findRelatedSkinCluster "{}" ;'.format(nsGeo))
		nsInfList = mc.skinCluster(skinClus,q=1,inf=1)
		infJntList = []
		for jnt in nsInfList:
			jnt = jnt.split(':')[-1]
			if mc.objExists(jnt):
				infJntList.append(jnt)
		if infJntList !=[]:
			print infJntList
			skn = mc.skinCluster(infJntList,i,tsb=1,dr=4.0)[0]
			mc.copySkinWeights(ss=skinClus,ds=skn,noMirror=1,surfaceAssociation='closestPoint',influenceAssociation='closestJoint')
		else:
			noJnt.append(i)
			print 'there is no Joint in this Rig'
		mc.select(noJnt)

def bindSelToJnt(jnt=''):
	sel = mc.ls(sl=True)
	for i in sel:
		mc.skinCluster(i,jnt)

def renameDummyShapeShapeNode():
    dumList = mc.ls('*_dummyShape_*',s=1)
    for j in dumList:
        mc.rename(j,j+'Shape')

				
def mullimbMMBox(locoName):
	limbMMBox(locoName,part = 'UpArm',side='L',rbn=5)
	limbMMBox(locoName,part = 'Forearm',side='L',rbn=5)
	limbMMBox(locoName,part = 'UpArm',side='R',rbn=5)
	limbMMBox(locoName,part = 'Forearm',side='R',rbn=5)

	limbMMBox(locoName,part = 'UpLeg',side='L',rbn=5)
	limbMMBox(locoName,part = 'LowLeg',side='L',rbn=5)
	limbMMBox(locoName,part = 'UpLeg',side='R',rbn=5)
	limbMMBox(locoName,part = 'LowLeg',side='R',rbn=5)
	limbMMBox(locoName,part = 'Neck',side='C',rbn=5)


def limbMMBox(locoName='loco',part = '',side='',rbn=5):
	if (side != '') and (side.lower() != 'c'):
		jnt = '{}_{}_Jnt'.format(part,side)
		mmBox = '{}_{}_Jnt_dummyShape_{}'.format(part,side,locoName)
	
	elif side.lower() == 'c':
		jnt = '{}_Jnt'.format(part)
		mmBox = '{}_Jnt_dummyShape_{}'.format(part,locoName)		
	
	else:
		jnt = '{}_Jnt'.format(part)
		mmBox ='{}_Jnt_dummyShape_{}'.format(part,locoName)
	rbnJntList = []
	rbnPrxList = []
	for i in range(rbn):
		rbnJnt = jnt.replace(part,'{}{}RbnDtl'.format(part,i+1))
		con = mc.listConnections(rbnJnt,type='parentConstraint',scn=1)
		if con != None:
			conList = list(set(con))[0]
			conGrp = mc.listRelatives(conList,p=1)[0]


			if mc.objExists(conGrp):
				print conGrp
				rbnPrx = createBBFromObj(conGrp)
				rbnPrxList.append(rbnPrx)

	if len(rbnPrxList) >1:
		newLimbGeo = createDupCombineGeo(rbnPrxList)
	elif len(rbnPrxList) == 1:
		newLimbGeo = rbnPrxList[0]
	else:
		newLimbGeo = ''
	
	if newLimbGeo != '':
		#snapMiarmyBB(mmBox,newLimbGeo,jnt)
		sclBB = bbFromMesh(newLimbGeo)
		cube = createBBBox(sclBB)
		BBBoxToDumShape(cube,mmBox)
		mc.delete(newLimbGeo)
		mc.delete(rbnPrxList)
	else:
		mc.delete(rbnPrxList)
		mc.confirmDialog(m='{} has something wrong'.format(part))


locoName = 'elephantDefault'    
mulSnapMMBox(locoName)
mullimbMMBox(locoName)