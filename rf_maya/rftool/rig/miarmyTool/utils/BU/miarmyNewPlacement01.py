import maya.cmds as mc
import sys
sys.path.append('C:/Program Files/Basefount/Miarmy/Maya2018/scripts')
import McdSimpleCmd as msc
import McdPlacementFunctions as mpf

class Replacement:
	def __init__(self):
		self.ls_plm = []

		self.get_locator = None
		self.get_noa = []

		self.get_plm_attr_t = []
		self.get_plm_attr_r = []

		self.get_id = []
		self.get_tslx = []
		self.get_tsly = []
		self.get_tslz = []
		self.get_rtx = []
		self.get_rty = []
		self.get_rtz = []
		self.get_clr = []

	def createLocator(self,*args):
		self.ls_plm = mc.ls("McdPlace*",type="transform")	# list placements' transform
		
		if len(self.ls_plm) > 0:
			if not mc.objExists("McdAgent*"):
				mpf.placementAgent()
				
			ls_mdcAg = mc.ls("McdAgent*",type="transform")	# list McdAgents' transform
			
			self.get_locator = []
			if not mc.objExists("locator*"):
				for x,obj in enumerate(ls_mdcAg):
					loacator = mc.spaceLocator()[0]
					self.get_locator.append(loacator)	
			else: 
				self.get_locator = mc.ls("locator*",type="transform")		# create and list locator
											
			mpf.dePlacementAgent()
			sys.stdout.write("Finish: locators are already created")
	#----------------------------------------------------------------------------------- first button

	def getPlacementAttr(self):
		for i in self.ls_plm:
			plm_attr_t = []
			plm_attr_r = []
			
			attr_t = mc.getAttr("%s.t"%i)
			attr_r = mc.getAttr("%s.r"%i)
			
			plm_attr_t.append(attr_t)
			plm_attr_r.append(attr_r)
			self.get_plm_attr_t.extend(plm_attr_t)
			self.get_plm_attr_r.extend(plm_attr_r)
			
		dicts = {"placementAttr":[{"translate":self.get_plm_attr_t},{"rotate":self.get_plm_attr_r}]}
		return dicts
	# get placement transform translate and rotate
	
	def getMcdAgentAttr(self):
		plm_value = self.getPlacementAttr()
		plm_t = plm_value["placementAttr"][0]["translate"]
		plm_r = plm_value["placementAttr"][1]["rotate"]

		self.get_noa = []
		for x in self.ls_plm:
			noa = mc.getAttr("%s.numOfAgent"%x)
			self.get_noa.append(noa)

		for posi,plm in enumerate(self.ls_plm):
			for num in range(self.get_noa[posi]):
				ids = mc.getAttr("{}.placement[{}].agentPlace[0]".format(plm,num))
				tslx = mc.getAttr("{}.placement[{}].agentPlace[1]".format(plm,num)) + plm_t[posi][0][0]
				tsly = mc.getAttr("{}.placement[{}].agentPlace[2]".format(plm,num)) + plm_t[posi][0][1]
				tslz = mc.getAttr("{}.placement[{}].agentPlace[3]".format(plm,num)) + plm_t[posi][0][2]
				rtx = mc.getAttr("{}.placement[{}].agentPlace[4]".format(plm,num)) + plm_r[posi][0][0]
				rty = mc.getAttr("{}.placement[{}].agentPlace[5]".format(plm,num)) + plm_r[posi][0][1]
				rtz = mc.getAttr("{}.placement[{}].agentPlace[6]".format(plm,num)) + plm_r[posi][0][2]
				clr = mc.getAttr("{}.placement[{}].agentPlace[7]".format(plm,num))
				
				self.get_id.append(ids)
				self.get_tslx.append(tslx)
				self.get_tsly.append(tsly)
				self.get_tslz.append(tslz)
				self.get_rtx.append(rtx)
				self.get_rty.append(rty)
				self.get_rtz.append(rtz)
				self.get_clr.append(clr)
				
		dicts = {"placement":[{"id":self.get_id},{"tx":self.get_tslx},{"ty":self.get_tsly},{"tz":self.get_tslz},{"rx":self.get_rtx},{"ry":self.get_rty},{"rz":self.get_rtz},{"color":self.get_clr}]}
		return dicts
	# get placement id, translate, rotate, color

	def newPositionLocator(self):
		plm_value = self.getMcdAgentAttr()
		tslx = plm_value["placement"][1]["tx"]
		tsly = plm_value["placement"][2]["ty"]
		tslz = plm_value["placement"][3]["tz"]
		rtx = plm_value["placement"][4]["rx"]
		rty = plm_value["placement"][5]["ry"]
		rtz = plm_value["placement"][6]["rz"]

		for x,obj in enumerate(self.get_locator):
			mc.setAttr("%s.t"%obj, self.get_tslx[x], self.get_tsly[x], self.get_tslz[x])
			mc.setAttr("%s.r"%obj, self.get_rtx[x], self.get_rty[x], self.get_rtz[x])
		
		get_lo_t = []
		get_lo_r = []	
		for x in self.get_locator:
			lo_t = mc.getAttr("%s.t"%x)[0]
			lo_r = mc.getAttr("%s.r"%x)[0]
			get_lo_t.append(lo_t)
			get_lo_r.append(lo_r)
			
		dicts = {"locator":[{"translate":get_lo_t},{"rotate":get_lo_r}]}
		return dicts
	# re-position of locator

	def createNewPlacement(self,*args):
		locator = self.newPositionLocator()
		lo_t = locator["locator"][0]["translate"]
		lo_r = locator["locator"][1]["rotate"]
		mc.delete(self.ls_plm)
		for x,obj in enumerate(self.ls_plm):
			mpf.McdCreatePlacementNode()
			mc.setAttr("%s.numOfAgent"%obj,self.get_noa[x])
			mc.setAttr("%s.placeType"%obj,5)
			
		count = 0
		for posi,plm in enumerate(self.ls_plm):
			for num in range(self.get_noa[posi]):
				tslx = mc.setAttr("{}.placement[{}].agentPlace[1]".format(plm,num),lo_t[count][0])
				tsly = mc.setAttr("{}.placement[{}].agentPlace[2]".format(plm,num),lo_t[count][1])
				tslz = mc.setAttr("{}.placement[{}].agentPlace[3]".format(plm,num),lo_t[count][2])
				rtx = mc.setAttr("{}.placement[{}].agentPlace[4]".format(plm,num),lo_r[count][0])
				rty = mc.setAttr("{}.placement[{}].agentPlace[5]".format(plm,num),lo_r[count][1])
				rtz = mc.setAttr("{}.placement[{}].agentPlace[6]".format(plm,num),lo_r[count][2])

				ids = mc.setAttr("{}.placement[{}].agentPlace[0]".format(plm,num),self.get_id[count])
				clr = mc.setAttr("{}.placement[{}].agentPlace[7]".format(plm,num),self.get_clr[count])
				count+=1
		mc.select(cl=1)
		sys.stdout.write("Finish: New placements are already deleted")
	# create new placements
	#---------------------------------------------------------------------------------- second button

	def deleteLocator(self,*args):
		locator = mc.ls("locator*",type="transform")
		if len(locator) > 0:
			mc.delete(locator)
			sys.stdout.write("Finish: locators are already deleted")
		else:
			mc.confirmDialog(m="There is not any McdPlace")
	# delete all locators
	#----------------------------------------------------------------------------------- third button

	def replacementUI(self):
		mainLayout = mc.columnLayout("New Place",cal="center",cat=["both",30],adj=1)
		
		buttonLayout = mc.columnLayout(adj=1)
		mc.separator(h=10,style=None)
		mc.text(l="Replacement Buttons")
		mc.separator(h=10,style=None)
		
		mc.button(l="Create Locator",ann="Create locators that snap with the placements",h=50,c = self.createLocator)
		mc.separator(h=10,style=None)
		
		mc.button(l="Run new placement",ann="Create new placements equal to the locators",h=50,c = self.createNewPlacement)
		mc.separator(h=10,style=None)
		
		mc.button(l="Delete Locator",ann="Delete unused locators",h=50,c = self.deleteLocator)
		mc.separator(h=20,style=None)
		
		return mainLayout
	# create a UI
	#--------------------------------------------------------------------------------------------- UI