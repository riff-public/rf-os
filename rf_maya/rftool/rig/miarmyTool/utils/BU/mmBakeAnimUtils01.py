import sys
sys.path.append('C:/Program Files/Basefount/Miarmy/Maya2018/scripts')
import maya.cmds as mc
import pymel.core as pm
import McdGeneral as mg
import McdDecisionEditor as mde
import maya.mel as mel
import McdGeneral as mg
import McdSimpleCmd as msc



def animMiarmy(NS):
    if NS != '':
        NS = NS[:-1]
        msc.McdSelectMcdGlobal()
        if mc.objExists('Skin_Grp'):
            mc.select('Skin_Grp')
            copyAnimJnt(NS)
            selectJntToExAnim(name = 'Skin_Grp' ,breakKey = 'True')
            deleteCons()
            mc.parent('Skin_Grp','Setup_loco')
        else:
            mc.confirmDialog(m='There is no Skin_Grp in this scene')

    else:
        mc.confirmDialog(m='Please check namespace before Run Script.')


def deleteCons():
    attrs = ['.tx','.ty','.tz','.sx','.sy','.sz','.rx','.ry','.rz']
    selGrp = mc.ls(sl = True)
    #lstBtwGrp = mc.ls('BtwAll__grp')
    
    for attr in attrs:
        sel = mc.listRelatives(ad = True,f = True)
        for i in range(len(sel)):
            if 'ikHndl' in sel[i]:
                a = mc.delete(sel[i])

            elif not 'Constraint' in sel[i]:
                mc.setAttr(sel[i] + attr, lock = 0, k = 1)
    
            else :    
                mc.delete(sel[i] ,icn = True ,cn = True)
                
   # mc.delete(lstBtwGrp)
    mc.select(mc.listRelatives(selGrp ,ad = True))
    
    print 'Clean Connection is Finish!!'


def copyAnimJnt(NS=''):
    if NS!= '':
        sel=mc.ls(sl=True)
        jntList = mc.listRelatives(type='joint',ad=1)
        for jnt in jntList:
            oriAnimJnt = '%s:%s'%(NS,jnt)
            mc.parentConstraint(oriAnimJnt,jnt,mo=0)
        for jnt in jntList:
            x='bake'

def selectJntToExAnim(name = 'Skin_Grp' ,breakKey = 'True'):

    skinGrp = pm.PyNode(name)
    children = skinGrp.getChildren(ad=True)  
    lstTrnsfGrp = []
    for c in children:
        nt = c.nodeType()
        if nt in ('joint', 'transform'):
            lstTrnsfGrp.append(c)
            
    mc.select(lstTrnsfGrp )

    if breakKey == 'True':
        stFrm = mc.playbackOptions(q = True , ast = True )
        EdFrm = mc.playbackOptions(q = True , aet = True )
        pm.bakeResults ( lstTrnsfGrp ,simulation  = True ,time = ( stFrm , EdFrm ))
        
    elif breakKey == 'False':
        print 'Not break key frame'

def deleteGeoGrp(name = 'CharGeo_Grp'):
    mc.delete(name)

