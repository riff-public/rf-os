import maya.cmds as mc
from lpRig import rigTools as lrr
reload(lrr)

class BlendshapFcltoMrmOA(object):
    def __init__(self):
        self.winName='BlendshapeFclToMrm_UI'
    
    def BlendshapeFclToMrmUI(self, *args):

        #windowID = 'BlendshapeFclToMrm_UI'
        
        
        if mc.window(self.winName, exists=True):
            mc.deleteUI(self.winName)
        winWidth = 500
        winHight = 250
        
        CheckScene = mc.objExists('FclRigGeo_Grp_*')
        if CheckScene == True:

            #SCAN SCENE FOR FclRig_Geo
            scanList=mc.listRelatives('FclRigGeo_Grp_*' , type = 'transform' , ad=True)
            Childs = []
            GeoList = []
            for i in scanList:
                if '_Geo' in i :
                    GeoList.append(i)
                    
            
            for each in GeoList:
                
                allName = each.split('_')
                name = ('_').join(allName[0:-1])
            
                if name =='BodyFclRig_Geo':
            
                   name ='BodyFclRigWrapped_Geo'
                
                
                Childs.append(name)
            
            MiarmyGeoList = []
        else : 
            print "NO FclRig_Geo_Grp* In This Scene"
            Childs = []
            MiarmyGeoList = []

        #mc.window(self.winName , title = 'Blendshape from FclRig to Miarmy_Geo' , width = winWidth , height = winHight , sizeable = 0 )
        self.mainCL = mc.columnLayout('BshFcl' , adj=1)
        #utaCore.wintes(window = 'MuscleConnectUI' , path = 'D:/' , pngIcon = 'MuscleConnectIcon.png')

        mc.rowLayout(numberOfColumns=2 , h=20)
        mc.text(label = 'FclRig_Geo :' , w=winWidth*0.5)
        mc.text(label = 'Miarmy_Geo :', w=winWidth*0.5)
        mc.setParent('..')

        mc.rowLayout(numberOfColumns=3 , h=140)
        mc.textScrollList( 'FclRigGeoWeUse' ,w=winWidth*.45  , numberOfRows=8, allowMultiSelection=True,
                    append=Childs,
                    showIndexedItem=4, 
                    h=130)
        mc.button('SCAN', label='SCAN'  , w=.1*winWidth , h = 20,bgc=[1,.85,0] , c = 'scanFcltoMiarmy()') 
        mc.textScrollList( 'MiarmyGeoWeUse' ,w=winWidth*.45  , numberOfRows=8, allowMultiSelection=True,
                    append=MiarmyGeoList,
                    showIndexedItem=4, 
                    h=130)
        mc.setParent('..')


        
       
        mc.rowLayout(numberOfColumns=1 , h=20)
        mc.textFieldGrp( 'notFound' ,ed=False, label='NotFound',columnWidth2=[60,winWidth*0.875] , pht='Display after scan')

        mc.setParent('..') 
        mc.separator(style='none' , h=10)
               
        mc.rowLayout(numberOfColumns=9)
        mc.button('AddFclGeo'  ,label = 'Add', w=.11*winWidth , h = 30 , c = addToDisplayFclGeo()')
        mc.button('RemoveFclGeo' ,label = 'Del' , w=.11*winWidth , h = 30 ,c='removeFclGeo()')
        mc.button('UpFclGeo'  ,l = 'Up', w=.11*winWidth , h = 30 , c = 'upGeoWeUseFclGeo()')
        mc.button('DownFclGeo'  ,l = 'Down', w=.11*winWidth , h = 30 , c = 'downGeoWeUseFclGeo()')  
        mc.text(l='' , w=winWidth*.1)
        mc.button('AddMiaGeo'  ,l = 'Add', w=.11*winWidth , h = 30 , c = 'addToDisplayMiaGeo()')
        mc.button('RemoveMiaGeo' ,l = 'Del' , w=.11*winWidth , h = 30 ,c='removeMiaGeo()')
        mc.button('UpMiaGeo' ,l = 'Up' , w=.11*winWidth , h = 30 , c = 'upGeoWeUseMiaGeo()')
        mc.button('DownMiaGeo' ,l = 'Down' , w=.11*winWidth , h = 30 , c = 'downGeoWeUseMiaGeo()')  
     
        mc.setParent('..')
        mc.separator(style='none' , h=10)
        mc.button('RunBsh' ,l = 'Run Blendshape' , w=winWidth , h = 50 , bgc=[0,1,0] , c = 'runBlendshape()')   
        #mc.showWindow()

        return self.mainCL

    def removeGeo(self,scrollName , *args):
        List = mc.textScrollList( scrollName ,q=True, si=True)

        for each in List:
            mc.textScrollList(scrollName ,edit=True , ri='{}'.format(each))


    def removeFclGeo(self,*args):
        removeGeo('FclRigGeoWeUse')
    def removeMiaGeo(self,*args):
        removeGeo('MiarmyGeoWeUse')


    def addToDisplayGeo(self,scrollName,*args):
        
        sel = mc.ls(sl=True)
        
        for each in sel:
            mc.textScrollList(scrollName ,edit=True,append=['{}'.format(each)])
            print mc.textScrollList(scrollName ,q=True,ai=1)

    def addToDisplayFclGeo(self,*args):
        addToDisplayGeo('FclRigGeoWeUse')
    def addToDisplayMiaGeo(self,*args):
        addToDisplayGeo('MiarmyGeoWeUse')


    def upScroll(self,scrollName):
        intList = mc.textScrollList(scrollName,q=1,sii=1)
        strList = mc.textScrollList(scrollName,q=1,si=1)
        selList = []
        for num in range(len(strList)):
            idx = intList[num]
            if 1 not  in intList:
                if idx  >= 2:
                    mc.textScrollList(scrollName,e=1,rii=idx)
                    mc.textScrollList(scrollName,e=1,ap=[idx-1,strList[num]])
                    selList.append(idx-1)

                else:
                    selList.append(idx)
                    pass

                mc.textScrollList(scrollName,e=1,sii=selList)
            else:
                mc.textScrollList(scrollName,e=1,sii = intList)

    def downScroll(self,scrollName):
            intList = mc.textScrollList(scrollName,q=1,sii=1)
            strList = mc.textScrollList(scrollName,q=1,si=1)
            maxNum = mc.textScrollList(scrollName,q=1,ni=1)
            selList = []

            for num in range(len(strList))[::-1]:
                idx = intList[num]
                if maxNum not in intList:
                    if idx  <= maxNum-1:
                        #print 'idx : {} < strList : {}'.format(idx+1,strList[num])
                        mc.textScrollList(scrollName,e=1,rii=idx)
                        mc.textScrollList(scrollName,e=1,ap=[idx+1,strList[num]])
                        selList.append(idx+1)
                    else:
                        #print 'there something in this case : idx: {} and strList :{}'.format(idx+1,strList[num])
                        selList.append(idx)
                    #print selList

                    mc.textScrollList(scrollName,e=1,sii=selList)
                else:
                    mc.textScrollList(scrollName,e=1,sii = intList)

    def upGeoWeUseFclGeo(self,*args):
            upScroll('FclRigGeoWeUse')
    def downGeoWeUseFclGeo(self,*args):
            downScroll('FclRigGeoWeUse')
    def upGeoWeUseMiaGeo(self,*args):
            upScroll('MiarmyGeoWeUse')
    def downGeoWeUseMiaGeo(self,*args):
            downScroll('MiarmyGeoWeUse')


    def scanFcltoMiarmy(self,*args):
        
        #SCAN SCENE FOR Miarmy_Geo
        agentName = mc.ls('Agent_*')[0].split('Agent_')[-1]
        GeometryList = mc.listRelatives('Geometry_*' , type = 'transform')
        
        objFcl = mc.textScrollList('FclRigGeoWeUse' ,q=True,ai=1)
        
        GeoNameList = []
        MiarmyGeoList = []
            
        for each in objFcl:
                GeoName = each.replace('FclRig','')
                if 'Wrapped' in GeoName:
                    GeoName = 'Body_Geo'
                FullGeoName = GeoName + '_'+ agentName + 'Geo'
                GeoNameList.append(FullGeoName)

        notfoundList = []

        for a in GeoNameList :
                check=0
                for b in GeometryList:
                    #compare = b.split(agentName)[0]
                    if '{}'.format(a) in b:
                        MiarmyGeoList.append(b)
                        check=1
                if check==0:
                    
                    a = a.split('_')[0]
                    notfoundList.append(a)
        
        #CHANGE LIST TO TEXT
        text = ''
        for sel in notfoundList:
                if text == '':
                    text = sel
                else :
                    text = text + ',' + sel
        
        mc.textScrollList( 'MiarmyGeoWeUse' , e=True , ra=True)
        mc.textScrollList( 'MiarmyGeoWeUse' , e=True , append=MiarmyGeoList)
        
        mc.textFieldGrp( 'notFound' ,edit=True , tx='{}'.format(text))      


    def runBlendshape(self,*args):
        agentName = mc.ls('Agent_*')[0].split('Agent_')[-1]
        listFcl = mc.textScrollList('FclRigGeoWeUse' ,q=True,ai=1)
        #mrm geo
        listMrm = mc.textScrollList('MiarmyGeoWeUse' ,q=True,ai=1)
        for obj in range(len(listFcl)):
            mc.select('{}_{}'.format(listFcl[obj],agentName))
            mc.select(listMrm[obj],add=True)
            lrr.doAddBlendShape()
        #textfield loco name
        locoName = ''
        attr = ['tx','ty','tz','rx','ry','rz','sx','sy','sz']
        objBsh = ['Head_Jnt','EyeLFT_Jnt','EyeRGT_Jnt','JawLwr1_Jnt','JawLwr2_Jnt']
        for each in range(len(objBsh)):
            obj = '{}_ogb_{}'.format(objBsh[each],agentName)
            for cnnt in attr:
                mc.connectAttr(obj+'.{}'.format(cnnt) , objBsh[each]+'.{}'.format(cnnt))

        FurFcl = 'BodyFurFcl_Geo_{}'.format(agentName)
        FurGeo = 'BodyFur_Geo_{}Geo'.format(agentName)
        checkFur = mc.objExists(FurGeo)
        if checkFur == True:
            mc.select(FurFcl)
            mc.select(FurGeo,add=True)
            lrr.doAddBlendShape()
        else:pass

    #def showUI(self):
        #mc.showWindow(self.winName)