import sys
sys.path.append('C:/Program Files/Basefount/Miarmy/Maya2018/scripts')
import maya.cmds as mc
import pymel.core as pm
import McdGeneral as mg
import McdDecisionEditor as mde
import maya.mel as mel
import McdSimpleCmd as msc

def resizePhyJoint(locoName='loco',size = .1):
	pyJntList = mc.ls('*phyJoint_{}*'.format(locoName))
	for i in pyJntList:
		mc.setAttr(i+'.s',size,size,size,type='double3')

def mulMcdCreateDecision(locoName='loco'):
	decisionList = ['adaptTerrain','avoidEachOther','followRoad']
	decisionCommand = ['adapt height of terrain intensity 0.5','avoid near agents by turn and slow action priority 1','follow road priority 1']
	activeAgentName = mg.McdGetActiveAgentName()
	mg.McdCreateDecision('default',activeAgentName,absMode=False)
	for j in range(len(decisionList)):
		mg.McdCreateDecision(decisionList[j],activeAgentName,absMode=False)
		dcName = '{}_decision_{}'.format(decisionList[j],locoName)
		mde.cb_dno_active(0)
		mde.McdMenuFillDecision(decisionCommand[j],0,dcName)
	mde.MakeDecisionDefault('default_decision_{}'.format(locoName))

def snapMiarmyBB(mmBox = '' ,prx='',jnt=''):
	##### create cube and snap with Proxy Geo ######
	cube = createBBFromObj(prx)
	mat = mc.xform(cube,ws=1,m=1,q=1)
	mc.xform(mmBox,ws=1,m=mat)
	mc.delete(cube)


def mulSnapMMBox(locoName='loco'):
	'''
	resize MiarmyBoundingBox to fit with ProxyGeo
	'''
	sel=  mc.ls(sl=True)
	for i in sel:
		prx = i
		jnt = i.replace('_Proxy','').replace('_Geo','_Jnt')
		jnt = i.replace('Proxy_','').replace('_Geo','_Jnt')

		if '_C_' in jnt:
			jnt = jnt.replace('_C','')
		if 'Wrist1' in prx:
			jnt = jnt.replace('Wrist1','Wrist')
		if 'Clav1' in prx:
			jnt = jnt.replace('Clav1','Clav')
		if 'HandCup1' in prx:
			jnt = jnt.replace('HandCup1','HandCup')
		if 'Ankle1' in prx:
			jnt = jnt.replace('Ankle1','Ankle')
		if 'Ball1' in prx:
			jnt = jnt.replace('Ball1','Ball')
		if 'Head1' in prx:
			jnt = jnt.replace('Head1','Head')
		if 'Pelvis1' in prx:
			jnt = jnt.replace('Pelvis1','Pelvis')
		jntSplitList= jnt.split('_')
		posJnt = jnt.replace(jntSplitList[0],jntSplitList[0]+'Pos')
		scaJnt = jnt.replace(jntSplitList[0],jntSplitList[0]+'Sca')
		BrthJnt = jnt.replace(jntSplitList[0],jntSplitList[0]+'Brth')
		mmList = [jnt,posJnt,scaJnt,BrthJnt]
		for jnt in mmList:
			mmBox = jnt + '_dummyShape_{}'.format(locoName)
			if mc.objExists(mmBox) and mc.objExists(jnt):
				snapMiarmyBB(mmBox,prx,jnt)

def createDupCombineGeo(objList=[]):
	if objList != []:
		dup = mc.duplicate(objList)
		if len(dup) >1:
			newGeo = mc.polyUnite(dup)
			mc.delete(dup)
			return newGeo[0]
		else:
			return dup[0]
				
def limbMMBox(locoName='loco',part = '',side='',rbn=5):
	if (side != '') and (side.lower() != 'c'):
		jnt = '{}_{}_Jnt'.format(part,side)
		mmBox = '{}_{}_Jnt_dummyShape_{}'.format(part,side,locoName)
	
	elif side.lower() == 'c':
		jnt = '{}_Jnt'.format(part)
		mmBox = '{}_Jnt_dummyShape_{}'.format(part,locoName)		
	
	else:
		jnt = '{}_Jnt'.format(part)
		mmBox ='{}_Jnt_dummyShape_{}'.format(part,locoName)
	rbnJntList = []
	rbnPrxList = []
	for i in range(rbn):
		rbnJnt = jnt.replace(part,'{}{}RbnDtl'.format(part,i+1))
		con = mc.listConnections(rbnJnt,type='parentConstraint',scn=1)
		if con != None:
			conList = list(set(con))[0]
			conGrp = mc.listRelatives(conList,p=1)[0]

		#if side.lower() !='c':
			#rbnPrx = rbnJnt.replace('{}{}RbnDtl'.format(part,i+1),'{}{}RbnDtl_Proxy'.format(part,i+1)).replace('_Jnt','_Geo')
		#else:
			#rbnPrx = rbnJnt.replace('{}{}RbnDtl'.format(part,i+1),'{}{}RbnDtl_Proxy_C'.format(part,i+1)).replace('_Jnt','_Geo')
	
			if mc.objExists(conGrp):
				print conGrp
				rbnPrx = createBBFromObj(conGrp)
				rbnPrxList.append(rbnPrx)
	if len(rbnPrxList) >1:
		newLimbGeo = createDupCombineGeo(rbnPrxList)
	elif len(rbnPrxList) == 1:
		newLimbGeo = rbnPrxList[0]
	else:
		newLimbGeo = ''
	
	if newLimbGeo != '':
		snapMiarmyBB(mmBox,newLimbGeo,jnt)
		mc.delete(newLimbGeo)
		mc.delete(rbnPrxList)
	else:
		mc.delete(rbnPrxList)
		mc.confirmDialog(m='{} has something wrong'.format(part))



def mullimbMMBox():
	limbMMBox(part = 'UpArm',side='L',rbn=5)
	limbMMBox(part = 'Forearm',side='L',rbn=5)
	limbMMBox(part = 'UpArm',side='R',rbn=5)
	limbMMBox(part = 'Forearm',side='R',rbn=5)

	limbMMBox(part = 'UpLeg',side='L',rbn=5)
	limbMMBox(part = 'LowLeg',side='L',rbn=5)
	limbMMBox(part = 'UpLeg',side='R',rbn=5)
	limbMMBox(part = 'LowLeg',side='R',rbn=5)
	limbMMBox(part = 'Neck',side='C',rbn=5)

def orgGeoGrp():
	'''
	organize Miarmy Geo to have same Hiarchy as Geo
	'''
	agent = mc.ls(type= 'McdAgentGroup')[0]
	agentName = ('_').join(agent.split('_')[1:])
	agentGeo = agentName+'Geo'
	agentGeoGrp = 'Geometry_{}'.format(agentName)
	grp = mc.ls(sl=True)[0]
	meshList = mc.listRelatives(grp,c=1,ad=1,type = 'mesh')
	agentGrp = mc.createNode('transform',n='{}_{}'.format(grp,agentGeo))
	mc.parent(agentGrp,agentGeoGrp)
	#print meshList
	
	for i in meshList:
		transform = mc.listRelatives(i,p=1)[0]
		agentGeoName = '{}_{}'.format(transform,agentGeo)
		par = mc.listRelatives(transform,p=1)[0]
		parAgentGrp = '{}_{}'.format(par,agentGeo)
		if not mc.objExists(parAgentGrp):
			parAgentGrp = mc.createNode('transform',n= parAgentGrp)
			mc.parent(parAgentGrp,agentGrp)   
		if not mc.listRelatives(agentGeoName,p=1)[0]== parAgentGrp:
			mc.parent(agentGeoName,parAgentGrp)


def mirrorMMBoxGeo(obj=''):
	if obj != '':
		copyGeo = mc.duplicate(obj,rc=1)
		mc.parent(copyGeo,w=1)
		mirGrp = mc.createNode('transform',n='mirGrp')
		mc.parent(copyGeo,mirGrp)
		if '_L_' in obj:
			mirGeo = obj.replace('_L_','_R_')
		elif '_R_' in obj:
			mirGeo = obj.replace('_R_','_L_')
		else:
			print 'there is no side to mirror'
			return
		mc.setAttr(mirGrp+'.sx',-1)
		copyGeoMat = mc.xform(copyGeo,q=1,ws=1,m=1)
		mc.xform(mirGeo,ws=1,m=copyGeoMat)
		mc.delete(mirGrp)

def mirSelectedMMBoxGeo():
	sel = mc.ls(sl=True)
	for i in sel:
		mirrorMMBoxGeo(i)


def selectRbbSkin():
	sel = mc.ls(sl=True)
	for i in sel:
		name = i.split('_')[0].replace('RbbSkin','')
		print name
		jntList = mc.ls('*{}*'.format(name),type='joint')
		mc.select(jntList,add=1)
	mc.select(sel,d=1)

def snapMMBoxFromJnt(obj,locoName = 'loco'):
	con = mc.listRelatives(obj,c=1,ad=1,type='constraint')
	if con != None:
		jnt = mc.parentConstraint(con[0],q=1,tl=1)[0]
		mmBox = jnt+'_dummyShape_{}'.format(locoName)
		print mmBox
		snapMiarmyBB(mmBox,obj,jnt)
		name = jnt.split('_')[0]
		posJnt=jnt.replace(name+'_',name+'Pos_')
		scaJnt = jnt.replace(name+'_',name+'Sca_')
		brthJnt = jnt.replace(name+'_',name+'Brth_')
		
		if mc.objExists(posJnt):
			mmBox = mmBox.replace(name+'_',name+'Pos_')
			snapMiarmyBB(mmBox,obj,posJnt)
		
		if mc.objExists(scaJnt):
			mmBox = mmBox.replace(name+'_',name+'Sca_')
			snapMiarmyBB(mmBox,obj,scaJnt)
		
		if mc.objExists(brthJnt):
			mmBox = mmBox.replace(name+'_',name+'Brth_')
			snapMiarmyBB(mmBox,obj,brthJnt)
	else:
		#print rbnPrxList
		#print rbnJntList

		print 'There is no Constraint in {}'.format(obj)

def mulSnapMMBox2(locoName  = 'loco'):
	sel =mc.ls(sl=True)
	for i in sel:
		snapMMBoxFromJnt(i,locoName)

def createBBFromObj(obj):
	BB = mc.exactWorldBoundingBox(obj)
	BBWorldPosi = [float((BB[0]+BB[3])/2),float((BB[1]+BB[4])/2),float((BB[2]+BB[5])/2)]
	BBScale = [BB[0]-BB[3],BB[1]-BB[4],BB[2]-BB[5]]
	cube = mc.polyCube()[0]
	mc.setAttr(cube+'.t',BBWorldPosi[0],BBWorldPosi[1],BBWorldPosi[2],type='double3')
	mc.setAttr(cube+'.s',BBScale[0],BBScale[1],BBScale[2],type='double3')
	return cube

def delSelPhyJnt(locoName = 'loco'):
	phyJnt = mc.ls('*phyJoint_{}'.format(locoName))
	mc.select(phyJnt,d=1)

def copyCostumeWeight(ns):
	sel= mc.ls(sl=True)
	noJnt = []
	for i in sel:
		splitList = i.split('_')
		geo = '_'.join(splitList[:-1])
		nsGeo = '{}:{}'.format(ns,geo)
		skinClus = mel.eval('findRelatedSkinCluster "{}" ;'.format(nsGeo))
		nsInfList = mc.skinCluster(skinClus,q=1,inf=1)
		infJntList = []
		for jnt in nsInfList:
			jnt = jnt.split(':')[-1]
			if mc.objExists(jnt):
				infJntList.append(jnt)
		if infJntList !=[]:
			print infJntList
			skn = mc.skinCluster(infJntList,i,tsb=1,dr=4.0)[0]
			mc.copySkinWeights(ss=skinClus,ds=skn,noMirror=1,surfaceAssociation='closestPoint',influenceAssociation='closestJoint')
		else:
			noJnt.append(i)
			print 'there is no Joint in this Rig'
		mc.select(noJnt)

def bindSelToJnt(jnt=''):
	sel = mc.ls(sl=True)
	for i in sel:
		mc.skinCluster(i,jnt)

#mulSnapMMBox2()
#mullimbMMBox()
#mulMcdCreateDecision()
#copyCostumeWeight('owlAdept_md')
#resizePhyJoint(size = .1)
#snapMiarmyBB('ProboscisIKIk4APos_Jnt_duppmyShape_loco','ProboscisIKIk4A_ProxyGeo_C_Grp','ProboscisIKIk4APos_Jnt')
#delSelPhyJnt()
#mirSelectedMMBoxGeo()

def createMiarmyOA(locoName='loco'):
	sel = mc.ls(sl=True)
	msc.McdSelectMcdGlobal()
	mc.parent('CharGeo_Grp','Skin_Grp','Setup_loco')
	McdParseRootBoneCreateOAgent(0)
	mc.select(sel)
	mulSnapMMBox(locoName)
	mullimbMMBox()
	
#def preparePublish():
	