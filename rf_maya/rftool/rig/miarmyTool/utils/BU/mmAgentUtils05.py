import sys
sys.path.append('C:/Program Files/Basefount/Miarmy/Maya2018/scripts')
import maya.cmds as mc
import pymel.core as pm
import McdGeneral as mg
import McdDecisionEditor as mde
import maya.mel as mel
import McdSimpleCmd as msc
from collections import OrderedDict


def resizePhyJoint(locoName='loco',size = .1):
	pyJntList = mc.ls('*phyJoint_{}*'.format(locoName))
	for i in pyJntList:
		mc.setAttr(i+'.s',size,size,size,type='double3')

def mulMcdCreateDecision(locoName='loco'):
	decisionList = ['adaptTerrain','avoidEachOther','followRoad']
	decisionCommand = ['adapt height of terrain intensity 0.5','avoid near agents by turn and slow action priority 1','follow road priority 1']
	activeAgentName = mg.McdGetActiveAgentName()
	mg.McdCreateDecision('default',activeAgentName,absMode=False)
	for j in range(len(decisionList)):
		mg.McdCreateDecision(decisionList[j],activeAgentName,absMode=False)
		dcName = '{}_decision_{}'.format(decisionList[j],locoName)
		mde.cb_dno_active(0)
		mde.McdMenuFillDecision(decisionCommand[j],0,dcName)
	mde.MakeDecisionDefault('default_decision_{}'.format(locoName))

def mirrorMMBoxGeo(obj=''):
	if obj != '':
		copyGeo = mc.duplicate(obj,rc=1)
		mc.parent(copyGeo,w=1)
		mirGrp = mc.createNode('transform',n='mirGrp')
		mc.parent(copyGeo,mirGrp)
		if '_L_' in obj:
			mirGeo = obj.replace('_L_','_R_')
		elif '_R_' in obj:
			mirGeo = obj.replace('_R_','_L_')
		else:
			print 'there is no side to mirror'
			return
		mc.setAttr(mirGrp+'.sx',-1)
		copyGeoMat = mc.xform(copyGeo,q=1,ws=1,m=1)
		mc.xform(mirGeo,ws=1,m=copyGeoMat)
		mc.delete(mirGrp)

def mirSelectedMMBoxGeo():
	sel = mc.ls(sl=True)
	for i in sel:
		mirrorMMBoxGeo(i)

def createMiarmyOA(locoName='loco'):
	sel = mc.ls(sl=True)
	msc.McdSelectMcdGlobal()
	mc.parent('CharGeo_Grp','Skin_Grp','Setup_loco')
	McdParseRootBoneCreateOAgent(0)
	mc.select(sel)
	mulSnapMMBox(locoName)
	mulLimbMMBox(locoName)

def orgGeoGrp():
	'''
	organize Miarmy Geo to have same Hiarchy as Geo
	'''
	agent = mc.ls(type= 'McdAgentGroup')[0]
	agentName = ('_').join(agent.split('_')[1:])
	agentGeo = agentName+'Geo'
	agentGeoGrp = 'Geometry_{}'.format(agentName)
	grp = mc.ls(sl=True)[0]
	meshList = mc.listRelatives(grp,c=1,ad=1,type = 'mesh')
	agentGrp = mc.createNode('transform',n='{}_{}'.format(grp,agentGeo))
	mc.parent(agentGrp,agentGeoGrp)
	#print meshList
	
	for i in meshList:
		transform = mc.listRelatives(i,p=1)[0]
		agentGeoName = '{}_{}'.format(transform,agentGeo)
		par = mc.listRelatives(transform,p=1)[0]
		parAgentGrp = '{}_{}'.format(par,agentGeo)
		if not mc.objExists(parAgentGrp):
			parAgentGrp = mc.createNode('transform',n= parAgentGrp)
			mc.parent(parAgentGrp,agentGrp)   
		if not mc.listRelatives(agentGeoName,p=1)[0]== parAgentGrp:
			mc.parent(agentGeoName,parAgentGrp)

def selectRbbSkin():
	sel = mc.ls(sl=True)
	for i in sel:
		name = i.split('_')[0].replace('RbbSkin','')
		print name
		jntList = mc.ls('*{}*'.format(name),type='joint')
		mc.select(jntList,add=1)
	mc.select(sel,d=1)

def findCon(jnt):
	findConList = mc.listConnections(jnt,s=0,d=1,type='parentConstraint',scn=1)
	conList = []
	if findConList!= None:
		for i in findConList:
			if i not in conList:
				conList.append(i)
	return conList


def validGeo(conNode):
	if mc.objExists(conNode):
		parGrp = mc.listRelatives(conNode,p=1)[0]
		meshList = mc.listRelatives(parGrp,c=1,type='mesh',ad=1)
		if meshList != None:
			for mesh in meshList:
				if 'ShapeOrig' in mesh:
					meshList.remove(mesh)
			print meshList
			return meshList
		else:
			return []

def bbFromMesh(mesh):
	oriMesh = pm.PyNode(mesh)
	if oriMesh.nodeType() == 'transform' and oriMesh.getShape().nodeType() == 'mesh':
		meshGeo = oriMesh
	else:
		meshGeo = oriMesh.getParent()
	sizeList = meshGeo.getBoundingBox(space='object',invisible=False)
	
	w = sizeList.width()
	h = sizeList.height()
	d =  sizeList.depth()
	return [w,h,d]

def bbFromMeshJoint(mesh,joint):
	oriMesh = pm.PyNode(mesh)
	if oriMesh.nodeType() == 'transform' and oriMesh.getShape().nodeType() == 'mesh':
		meshGeo = oriMesh
	else:
		meshGeo = oriMesh.getParent()
	x = pm.duplicate(meshGeo)[0]
	x.setParent(joint)
	sizeList = x.getBoundingBox(space='object',invisible=False)
	
	w = sizeList.width()
	h = sizeList.height()
	d =  sizeList.depth()
	pm.delete(x)
	return [w,h,d]


def getBBFromMeshList(meshList):
	
	rbbList = [0,0,0]
	for m in meshList:
		bbList = bbFromMesh(m)
		w = bbList[0]
		h = bbList[1]
		d = bbList[2]
		if w > rbbList[0]:
			rbbList[0] = w
		if h > rbbList[1]:
			rbbList[1] = h
		if d > rbbList[2]:
			rbbList[2] = d
	
	return rbbList

def getBBFromMeshListJoint(meshList,Joint):
	
	rbbList = [0,0,0]
	for m in meshList:
		bbList = bbFromMeshJoint(m,Joint)
		w = bbList[0]
		h = bbList[1]
		d = bbList[2]

		if w > rbbList[0]:
			rbbList[0] = w
		if h > rbbList[1]:
			rbbList[1] = h
		if d > rbbList[2]:
			rbbList[2] = d
	
	return rbbList

def createBBBox(rbbList):
	cube1= mc.polyCube()[0]
	mc.setAttr(cube1+'.s',rbbList[0],rbbList[1],rbbList[2],type='double3')
	return cube1


def BBBoxToDumShape(box,dumShape):
	#scl=pm.getAttr(box + '.scale')
	#pm.setAttr(dumShape + '.scale',scl)
	#pm.xform(dumShape,ro=[0,0,0],ws=1)
	scl = pm.xform(box,s=1,q=1)
	pm.xform(dumShape,s=scl,a=1)

def compareBBtoBox(bbList1,bbList2):
	bbList1Volume = bbList1[0]*bbList1[1]*bbList1[2]
	bbList2Volume = bbList2[0]*bbList2[1]*bbList2[2]
	
	if bbList1Volume >= bbList2Volume:
		return bbList1
	else:
		return bbList2
		
def snapMMBox(jnt,locoName):
	x = findCon(jnt)
	splitName = jnt.split('_')
	posDum = splitName[0] + 'Pos_'+'_'.join(splitName[1:])+'_dummyShape_{}'.format(locoName)
	scaDum= splitName[0] + 'Sca_'+'_'.join(splitName[1:])+'_dummyShape_{}'.format(locoName)
	brthDum= splitName[0] + 'Brth_'+'_'.join(splitName[1:])+'_dummyShape_{}'.format(locoName)
	anotherList = [posDum,scaDum,brthDum]
	#print anotherList
	dumShape = jnt +'_dummyShape_{}'.format(locoName)
	if mc.objExists(dumShape):
		if x != []:
			sclBB = [0,0,0]
			for i in x:
				mList = validGeo(i)
				if mList != []:
					bbList = getBBFromMeshListJoint(mList,jnt)
					sclBB = compareBBtoBox(bbList,sclBB)

			cube = createBBBox(sclBB)
			BBBoxToDumShape(cube,dumShape)
			for k in anotherList:
				if mc.objExists(k):
					BBBoxToDumShape(cube,k)
			mc.delete(cube)
		   

def mulSnapMMBox(locoName):
	skinJntList = mc.listRelatives('Root_Jnt',c=1,ad=1,type='joint')
	for i in skinJntList:
		snapMMBox(i,locoName)


def createDupCombineGeo(objList=[]):
	if objList != []:
		dup = mc.duplicate(objList,rc=1)
		if len(dup) >1:
			newGeo = mc.polyUnite(dup)
			mc.delete(dup)
			return newGeo[0]
		else:
			return dup[0]

def delSelPhyJnt(locoName = 'loco'):
	phyJnt = mc.ls('*phyJoint_{}'.format(locoName))
	mc.select(phyJnt,d=1)

def copyCostumeWeight(ns):
	sel= mc.ls(sl=True)
	noJnt = []
	for i in sel:
		splitList = i.split('_')
		geo = '_'.join(splitList[:-1])
		nsGeo = '{}:{}'.format(ns,geo)
		skinClus = mel.eval('findRelatedSkinCluster "{}" ;'.format(nsGeo))
		nsInfList = mc.skinCluster(skinClus,q=1,inf=1)
		infJntList = []
		for jnt in nsInfList:
			jnt = jnt.split(':')[-1]
			if mc.objExists(jnt):
				infJntList.append(jnt)
		if infJntList !=[]:
			print infJntList
			skn = mc.skinCluster(infJntList,i,tsb=1,dr=4.0)[0]
			mc.copySkinWeights(ss=skinClus,ds=skn,noMirror=1,surfaceAssociation='closestPoint',influenceAssociation='closestJoint')
		else:
			noJnt.append(i)
			print 'there is no Joint in this Rig'
		mc.select(noJnt)

def bindSelToJnt(jnt=''):
	sel = mc.ls(sl=True)
	for i in sel:
		mc.skinCluster(i,jnt)

def renameDummyShapeShapeNode():
	dumList = mc.ls('*_dummyShape_*',s=1)
	for j in dumList:
		mc.rename(j,j.split('|')[-1]+'Shape')

def findRbnDumShape(loco='loco',part = '',side=''):
	#if side!= '':
		#side = '_{}'.format(side)
	jnt = '{}{}_Jnt'.format(part,side)
	chdJnt = mc.listRelatives(jnt,c=1,type='joint')
	if chdJnt != None:
		rbnJnt = []
		for chd in chdJnt:
			if part in chd:
				rbnJnt.append(chd)
	
		rbnDumShape = [rbn+'_dummyShape_{}'.format(loco) for rbn in rbnJnt ]
		print rbnDumShape
		return rbnDumShape

def limbMMBox(loco,part,side):
	if side!= '':
		side = '_{}'.format(side)
	jnt = '{}{}_Jnt'.format(part,side)
	dumShape = '{}_dummyShape_{}'.format(jnt,loco)
	if mc.objExists(dumShape):
		x = findRbnDumShape(loco,part,side)
		y = createDupCombineGeo (x) 
		rbbList = bbFromMeshJoint(y,jnt)
		cube1 = createBBBox(rbbList)
		BBBoxToDumShape(cube1,dumShape)
		mc.delete(cube1,y)
	else:
		print '{} not exists'.format(dumShape)


def findRbnRoot(sel_all):
	sel_jnt = mc.listRelatives(sel_all,type="joint",c=1,ad=1)
	rbn_jnt = []
	rbn_string = []
	rbn_root_jnt = []
	get_root = []
	for num, jnt in enumerate(sel_jnt):
		if re.findall(".RbnDtl+|.rbnDtl+",jnt):
			rbn = re.findall(".RbnDtl+|.rbnDtl+",jnt)
			rbn_string.extend(rbn)
			rbn_jnt.append(jnt)
	
	if len(rbn_jnt) == len(rbn_string):
		for num in range(len(rbn_jnt)):
			if rbn_string[num] == "":
				root_name = get_root[0]
			else:
				jnt_name,suffix_name = rbn_jnt[num].split(rbn_string[num])
				root_name = "{}{}".format(jnt_name,suffix_name)
				
			if mc.objExists(root_name) == True:
				rbn_root_jnt.append(root_name)

	rbn_root = list(OrderedDict.fromkeys(rbn_root_jnt))
	return rbn_root

def getPartSide(name):
	splitList = name.split('_')
	lenSplit = len(splitList)
	part = ''
	side = ''
	if lenSplit > 2:
		part = splitList[0]
		side = splitList[1]
	elif lenSplit ==2:
		part = splitList[0]
	else:
		pass 
	
	return part,side

def mulLimbMMBox(loco):
	rbnRoot = findRbnRoot('Setup_{}'.format(loco))
	for i in rbnRoot:
		part,side = getPartSide(i)
		if part != '':
			limbMMBox(loco,part,side)
		else:
			pass


#locoName = 'berserkerLow'  
#renameDummyShapeShapeNode()  
#mulSnapMMBox(locoName)
#mulLimbMMBox(locoName)

#createMiarmyOA('loco')