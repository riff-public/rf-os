import sys
sys.path.append('C:/Program Files/Basefount/Miarmy/Maya2018/scripts')
import maya.cmds as mc
import pymel.core as pm
import McdGeneral as mg
import McdDecisionEditor as mde
import maya.mel as mel

############################# Prep Scene Part ############################################
mel.eval("source channelBoxCommand;")

def deleteUnUseNodes():
    mel.eval( 'MLdeleteUnused' )
    
def deleteUnKnowNode():
    unknownNodes = mc.listRelatives(mc.ls(type = 'unknown'))
    for node in unknownNodes:
        print 'deleting: ' + node
        mc.lockNode(node, lock = False)
        mc.delete(node)
        
def deleteUnUseWeight():
    mel.eval("removeUnusedInfluences()")
 
def deleteUnUseGrps(name = '*BtwAll*grp' ):

    lstBtwGrp = mc.ls(name)
    
    for i in range(len(lstBtwGrp)):
        mc.delete(lstBtwGrp[i])
        
    print 'delete btw grp'

def cleanJntSkinGeo(target = 'Skin_Grp'):
    
    skinGrp = pm.PyNode(target)
    children = skinGrp.getChildren(ad=True) 
    to_del = []
    for c in children:
        nt = c.nodeType()
        if nt not in ('joint', 'transform'):
            to_del.append(c)
        else:
            for attr in 'trs':
                cat = c.attr(attr)
                cat.unlock()
                if cat.isConnected():
                    mel.eval('CBdeleteConnection "%s";' %cat.name())
                    # cat.disconnect()
                for axis in 'xyz':
                    jnt_attr = c.attr('%s%s' %(attr, axis))
                    jnt_attr.unlock()
                    if jnt_attr.isConnected():
                        mel.eval('CBdeleteConnection "%s";' %jnt_attr.name())
                        # jnt_attr.disconnect()
    
    if to_del:
        pm.delete(to_del)
    
    cat.name()
    
    print 'clear connection and constraint'



def DupSKinJnt(target = 'Skin_Grp', prefixNames = 'Jnt_'):
    
    nmJntDupGrp = 'Jnt{}' .format(target)
    JntDupGrp = mc.duplicate(target , n = nmJntDupGrp)
    
    cleanJntSkinGeo(target = nmJntDupGrp)
        
    lstInDupGrp = mc.listRelatives(nmJntDupGrp, ad =True, f = True)    
    a = []
    
    for name in lstInDupGrp:
        nm = name.split("|")[-1]
        a.append(nm)
    
    for i in range(len(lstInDupGrp)): 
        mc.rename(lstInDupGrp[i] ,'%s%s' %(prefixNames,a[i]))
        


def deleteCons():
    attrs = ['.tx','.ty','.tz','.sx','.sy','.sz','.rx','.ry','.rz']
    selGrp = mc.ls(sl = True)
    #lstBtwGrp = mc.ls('BtwAll__grp')
    
    for attr in attrs:
        sel = mc.listRelatives(ad = True,f = True)
        for i in range(len(sel)):
            if 'ikHndl' in sel[i]:
                a = mc.delete(sel[i])

            elif not 'Constraint' in sel[i]:
                mc.setAttr(sel[i] + attr, lock = 0, k = 1)
    
            else :    
                mc.delete(sel[i] ,icn = True ,cn = True)
                
   # mc.delete(lstBtwGrp)
    mc.select(mc.listRelatives(selGrp ,ad = True))
    
    print 'Clean Connection is Finish!!'




#Run Clean For Miarmy   
def runCleanForMiarmy():
    
    cleanJntSkinGeo(target = 'Skin_Grp')
    
    splineLst = mc.ls('Spine*Posi_Jnt')

    for i in range(len(splineLst))[::-1][:-1]:
        mc.parent(splineLst[i],splineLst[i-1])
        
    mc.parent('Spine1_Jnt','Spine2_Jnt','Spine3_Jnt','Spine4_Jnt','Spine5_Jnt','Root_Jnt')
    
    mc.parent('Neck*RbnDtl_Jnt','Neck_Jnt')
    mc.parent('UpArm*RbnDtl_L_Jnt','UpArm_L_Jnt')
    mc.parent('Forearm*RbnDtl_L_Jnt','Forearm_L_Jnt')
    mc.parent('UpArm*RbnDtl_R_Jnt','UpArm_R_Jnt')
    mc.parent('Forearm*RbnDtl_R_Jnt','Forearm_R_Jnt')
    mc.parent('UpLeg*RbnDtl_L_Jnt','UpLeg_L_Jnt')
    mc.parent('LowLeg*RbnDtl_L_Jnt','LowLeg_L_Jnt')
    mc.parent('UpLeg*RbnDtl_R_Jnt','UpLeg_R_Jnt')
    mc.parent('LowLeg*RbnDtl_R_Jnt','LowLeg_R_Jnt')

    # mc.parent('MustacheCat*Pos_*_Jnt','JawUpr1_Jnt')

    
    deleteUnUseGrps(name = '*BtwAll*grp' )
    deleteUnUseGrps(name = '*RbnSkin*Grp' )
    deleteUnUseGrps(name = 'SpineSkin_Grp' )
    
    

    
    SknJtGrp = mc.createNode('transform' ,n = 'SkinJnt_Grp')
    GeoGrp = mc.createNode('transform' ,n = 'CharGeo_Grp')
    mc.parent('Skin_Grp' ,SknJtGrp)
    mc.parent('Geo_Grp' ,GeoGrp)
    #a = mc.createNode('transform' ,n = 'Export_Grp')
    #mc.parent('SkinJnt_Grp','CharGeo_Grp','Export_Grp')
    #mc.delete(mc.listRelatives('AllMover_Grp' ,p = True) )
    #deleteUnUseGrps(name = 'Rig_Grp' )
    
    print 'a'
    #deleteUnUseNodes()
    print 'clear unused nodes'
    
    print '= = = = = = D O N E = = = = ='


