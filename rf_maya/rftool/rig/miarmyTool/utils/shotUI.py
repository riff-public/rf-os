import maya.cmds as mc
import pymel.core as pm
from collections import OrderedDict
import sys, re
sys.path.append("P:/DarkHorse/Research/Hong/Miarmy/utils")
sys.path.append(r'C:/Program Files/Basefount/Miarmy/Maya2018/scripts')

import mmOaPrepUtils as mmoap
reload(mmoap)
import mmAgentUtils as mmoa
reload(mmoa)
import mmBakeAnimUtils as mmbau
reload(mmbau)
import miarmySwitchChildren as msct
reload(msct)
import miarmyNewPlacement as mmnp
reload(mmnp)

import McdAgentManager as mmAgent
reload(mmAgent)

class shotCustomUI():
	def __init__(self):
		self.winName = 'mmShotUI'
		self.agentList = mmAgent.McdGetAllAgentTypeNIDWithColor()[0]
	#def getNamespace(self,*args):
		#self.ns = pm.selected()[0].namespace()
		#mc.textField('ns',w=20,e=1,tx=self.ns,bgc=[.5,1,.5])

	#-----------------------------------------------------------------------------
	#----------- OaPrep Function ----------------------------------------#
	#-----------------------------------------------------------------------------

	def swapAgent(*args):
		sel_agn = mc.ls(sl=1)
		if len(sel_agn) ==2:
			getMot_tr = mc.getAttr("{}.t".format(sel_agn[0]))[0]
			getMot_ro = mc.getAttr("{}.r".format(sel_agn[0]))[0]
			getMot_sc = mc.getAttr("{}.s".format(sel_agn[0]))[0]
			
			getChl_tr = mc.getAttr("{}.t".format(sel_agn[1]))[0]
			getChl_ro = mc.getAttr("{}.r".format(sel_agn[1]))[0]
			getChl_sc = mc.getAttr("{}.s".format(sel_agn[1]))[0]
			
			mc.setAttr("{}.t".format(sel_agn[0]),getChl_tr[0],getChl_tr[1],getChl_tr[2])
			mc.setAttr("{}.r".format(sel_agn[0]),getChl_ro[0],getChl_ro[1],getChl_ro[2])
			mc.setAttr("{}.s".format(sel_agn[0]),getChl_sc[0],getChl_sc[1],getChl_sc[2])
			
			mc.setAttr("{}.t".format(sel_agn[1]),getMot_tr[0],getMot_tr[1],getMot_tr[2])
			mc.setAttr("{}.r".format(sel_agn[1]),getMot_ro[0],getMot_ro[1],getMot_ro[2])
			mc.setAttr("{}.s".format(sel_agn[1]),getMot_sc[0],getMot_sc[1],getMot_sc[2])
			
			print "Finish",

	#-----------------------------------------------------------------------------
	#----------- Animation Bake Function ----------------------------------------#
	#-----------------------------------------------------------------------------
	def changeTypeAgent(self,*args):
		sel = mc.ls(sl=True)
		if sel != []:
			agentID = mc.textScrollList('AgentTypeList',q=1,sii=1)[0]-1
			print agentID
			for agent in sel:
				selAgent = agent
				placementList = mc.ls(type = "McdPlace")
				placeID = mc.getAttr('{}.placeId'.format(selAgent))
				AgentID = mc.getAttr('{}.placeAgentId'.format(selAgent))
				mc.setAttr('{}.placement[{}].agentPlace[0]'.format(placementList[placeID],AgentID),agentID)
				mc.setAttr('{}.placement[{}].agentPlace[0]'.format(placementList[placeID],AgentID),agentID)	
		else:
			pass	
	#---------------------------------------------- ^
	#---------------------------------------------- ^
	#---------------------------------------------- ^ get namespace

	def mmShotUI(self):
		ui = mc.window(self.winName,title = 'mmShotUI',s=0)
		#mainLayout = mc.tabLayout('mainTab',sc=self.adjColumn,w=320,h=280)
		prepLayout = mc.columnLayout(cal='center',co=['both',28],adj=1)
		mc.separator(h=20,style ='none')
		mc.textScrollList('AgentTypeList',h=150,w=150, ams=0,a=self.agentList)
		mc.separator(h=20,style ='none')
		mc.button('changeAgent',l='Change Agent Type',w=120,h=40,c=self.changeTypeAgent)		
		mc.separator(h=10,style ='none')
		mc.button('swapAgent',l='Swap Agent',w=120,h=40,c=self.swapAgent)
		mc.separator(h=20,style ='none')
		

	def showUI(self):
		if mc.window(self.winName,exists=1):
			print 'delUI'
			mc.deleteUI(self.winName)
		self.mmShotUI()
		mc.showWindow(self.winName)	