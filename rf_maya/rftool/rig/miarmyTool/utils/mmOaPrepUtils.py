import maya.cmds as mc
import maya.cmds as cmds
import pymel.core as pm
import maya.mel as mel
import re
import sys

from collections import OrderedDict

from rftool.rig.utaTools.utapy import utaCore
reload(utaCore)

# sys.path.append(r'C:\Program Files\Basefount\Miarmy\Maya2018\scripts')
import McdSimpleCmd
reload(McdSimpleCmd)
from rf_utils.context import context_info
reload(context_info)

mel.eval("source channelBoxCommand;")


def deleteUnUseNodes():
	mel.eval( 'MLdeleteUnused' )
	
def deleteUnKnowNode():
	unknownNodes = mc.listRelatives(mc.ls(type = 'unknown'))
	for node in unknownNodes:
		print 'deleting: ' + node
		mc.lockNode(node, lock = False)
		mc.delete(node)
		
def deleteUnUseWeight():
	mel.eval("removeUnusedInfluences()")
	

def cleanJntSkinGeo(target = 'Skin_Grp'):
	
	skinGrp = pm.PyNode(target)
	children = skinGrp.getChildren(ad=True) 
	to_del = []
	for c in children:
		nt = c.nodeType()
		if nt not in ('joint', 'transform', 'nurbsCurve'):
			to_del.append(c)
		else:
			if not nt in 'nurbsCurve':
				for attr in 'trs':
					cat = c.attr(attr)
					cat.unlock()
					if cat.isConnected():
						mel.eval('CBdeleteConnection "%s";' %cat.name())
						# cat.disconnect()
					for axis in 'xyz':
						jnt_attr = c.attr('%s%s' %(attr, axis))
						jnt_attr.unlock()
						if jnt_attr.isConnected():
							mel.eval('CBdeleteConnection "%s";' %jnt_attr.name())
							# jnt_attr.disconnect()
	if to_del:
		pm.delete(to_del)
	#cat.name()
	
	print 'clear connection and constraint'

def deleteUnUseGrps(name = '*BtwAll*Grp' ):

	lstBtwGrp = mc.ls(name)
	
	for i in range(len(lstBtwGrp)):
		mc.delete(lstBtwGrp[i])
		
	print 'delete btw grp'

def DupSKinJnt(target = 'Skin_Grp', prefixNames = 'Jnt_'):
	
	nmJntDupGrp = 'Jnt{}' .format(target)
	JntDupGrp = mc.duplicate(target , n = nmJntDupGrp)
	
	cleanJntSkinGeo(target = nmJntDupGrp)
		
	lstInDupGrp = mc.listRelatives(nmJntDupGrp, ad =True, f = True)    
	a = []
	
	for name in lstInDupGrp:
		nm = name.split("|")[-1]
		a.append(nm)
	
	for i in range(len(lstInDupGrp)): 
		mc.rename(lstInDupGrp[i] ,'%s%s' %(prefixNames,a[i]))
		

def deleteCons():
	attrs = ['.tx','.ty','.tz','.sx','.sy','.sz','.rx','.ry','.rz']
	selGrp = mc.ls(sl = True)
	#lstBtwGrp = mc.ls('BtwAll__grp')
	
	for attr in attrs:
		sel = mc.listRelatives(ad = True,f = True)
		for i in range(len(sel)):
			if 'ikHndl' in sel[i]:
				a = mc.delete(sel[i])

			elif not 'Constraint' in sel[i]:
				mc.setAttr(sel[i] + attr, lock = 0, k = 1)
	
			else :    
				mc.delete(sel[i] ,icn = True ,cn = True)
				
   # mc.delete(lstBtwGrp)
	mc.select(mc.listRelatives(selGrp ,ad = True))
	
	print 'Clean Connection is Finish!!'


#Run Clean For Miarmy
'''
def runCleanForMiarmy():
	
	cleanJntSkinGeo(target = 'Skin_Grp')
	
	splineLst = mc.ls('Spine*Posi_Jnt')

	for i in range(len(splineLst))[::-1][:-1]:
		mc.parent(splineLst[i],splineLst[i-1])
		
	mc.parent('Spine1_Jnt','Spine2_Jnt','Spine3_Jnt','Spine4_Jnt','Spine5_Jnt','Root_Jnt')
	
	mc.parent('Neck*RbnDtl_Jnt','Neck_Jnt')
	mc.parent('UpArm*RbnDtl_L_Jnt','UpArm_L_Jnt')
	mc.parent('Forearm*RbnDtl_L_Jnt','Forearm_L_Jnt')
	mc.parent('UpArm*RbnDtl_R_Jnt','UpArm_R_Jnt')
	mc.parent('Forearm*RbnDtl_R_Jnt','Forearm_R_Jnt')
	mc.parent('UpLeg*RbnDtl_L_Jnt','UpLeg_L_Jnt')
	mc.parent('LowLeg*RbnDtl_L_Jnt','LowLeg_L_Jnt')
	mc.parent('UpLeg*RbnDtl_R_Jnt','UpLeg_R_Jnt')
	mc.parent('LowLeg*RbnDtl_R_Jnt','LowLeg_R_Jnt')

	# mc.parent('MustacheCat*Pos_*_Jnt','JawUpr1_Jnt')

	
	deleteUnUseGrps(name = '*BtwAll*grp' )
	deleteUnUseGrps(name = '*RbnSkin*Grp' )
	deleteUnUseGrps(name = 'SpineSkin_Grp' )
	
	
	
	
	SknJtGrp = mc.createNode('transform' ,n = 'SkinJnt_Grp')
	GeoGrp = mc.createNode('transform' ,n = 'CharGeo_Grp')
	mc.parent('Skin_Grp' ,SknJtGrp)
	mc.parent('Geo_Grp' ,GeoGrp)
	#a = mc.createNode('transform' ,n = 'Export_Grp')
	#mc.parent('SkinJnt_Grp','CharGeo_Grp','Export_Grp')
	#mc.delete(mc.listRelatives('AllMover_Grp' ,p = True) )
	#deleteUnUseGrps(name = 'Rig_Grp' )
	
	print 'a'
	#deleteUnUseNodes()
	print 'clear unused nodes'
	
	print '= = = = = = D O N E = = = = ='

'''
def runCleanForMiarmy():

	entity = context_info.ContextPathInfo() 
	charName = entity.name
	asset = entity.copy()
	asset.context.update(step='rig', process='main', res='md')
	heroPath = asset.path.hero()
	mainFile = asset.publish_name(hero=True)
	rigMainFile = '%s/%s' % (heroPath, mainFile)
	mc.file(rigMainFile,i=True)

	utaCore.lockAttr(listObj = ['AllMover_Ctrl'], attrs = [ 'sy' ],lock = False,keyable = True)
	scale = mc.getAttr('AllMover_Ctrl.sy')
	mc.addAttr('AllMover_CtrlShape',ln='scaleChar',at='double',dv=scale,k=True)
	mc.setAttr('AllMover_CtrlShape.scaleChar',lock=True)
	#mc.setAttr('AllMover_Ctrl.sy',1)
	utaCore.lockAttr(listObj = ['AllMover_Ctrl'], attrs = [ 'sy' ],lock = True,keyable = True)

	skinGrps = []
	skinGrpSel = ['Skin_Grp', 'AddCtrl_Grp']
	for skinGrpSelEach in skinGrpSel:
		if skinGrpSelEach:
			skinGrps.append(skinGrpSelEach)

		else:
			return

	sel_jnt = []
	for skinGrpsEach in skinGrps :
		cleanJntSkinGeo(target = skinGrpsEach)
		mc.select(skinGrpsEach)
		sel_all = mc.ls(sl = True)
		sel_jnts = mc.listRelatives(sel_all,type="joint",c=1,ad=1)
		if sel_jnts != None:
			for each in sel_jnts:
				sel_jnt.append(each)
	sel_skinClst = []
	for x in range(len(sel_jnt)):
		skinClst = mc.listConnections(sel_jnt[x],type="skinCluster")
		if skinClst == None:
			pass
		else:
			for each in skinClst:
				sel_skinClst.append(each)
	rbn_jnt = []
	rbn_string = []
	rbn_root_jnt = []
	not_rbn_root = []   
	get_root = []
	bpm_Jnt = []
	for num, jnt in enumerate(sel_jnt):
		if re.findall(".RbnDtl+|.rbnDtl+",jnt):
			rbn = re.findall(".RbnDtl+|.rbnDtl+",jnt)
			rbn_string.extend(rbn)
			rbn_jnt.append(jnt)
			
			# Spine joint parent with root joint
			# Spine has no "Rbn" in its name 
			# (need 2 elif to get 5 joints spine and 1 root)

		elif re.findall("Spine[0-9]_Jnt",jnt):
			rbn = re.findall("Spine[0-9]_Jnt",jnt)
			rbn_string.extend([""])
			rbn_jnt.append(jnt)

		elif re.findall(".*Root_Jnt",jnt):
			root = re.findall(".*Root_Jnt",jnt)
			get_root.extend(root)

		elif 'Bpm' in jnt:
			if mc.objExists('RobeSubBpmRoot_Jnt'):
				mc.parent('RobeSubBpmRoot_Jnt', w = True)
				mc.parent(jnt, 'RobeSubBpmRoot_Jnt')
				mc.parent('RobeSubBpmRoot_Jnt', get_root)
				bpm_Jnt.append('RobeSubBpmRoot_Jnt')
				bpm_Jnt.append(jnt)

	if len(rbn_jnt) == len(rbn_string):
		for num in range(len(rbn_jnt)):
			if rbn_string[num] == "":
				root_name = get_root[0]
			else:
				jnt_name,suffix_name = rbn_jnt[num].split(rbn_string[num])
				root_name = "{}{}".format(jnt_name,suffix_name)
				
			if mc.objExists(root_name) == True:
				rbn_root_jnt.append(root_name)
			else:
				not_rbn_root.append(root_name)
	rbn_root = list(OrderedDict.fromkeys(rbn_root_jnt))
	mc.select(cl=1)
	

	print "--------------------------------------\n--------------------------------------\n--------------------------------------"
	print "There are %s joints in Skin_Grp"%(len(sel_jnt))
	print "There are %s joints have skin weight"%(len(sel_skinClst))
	print "There are %s ribbon joints"%(len(rbn_jnt))
	print "There are %s ribbon root joints"%(len(rbn_root))
	print "There are %s bpmJnt "%(len(bpm_Jnt))
	for x in rbn_root:
		print x
	print "--------------------------------------\n--------------------------------------\n--------------------------------------"

	if len(rbn_root_jnt) > 0 and len(rbn_jnt) > 0:
		for x, par in enumerate(rbn_root_jnt):
			if mc.listRelatives(rbn_jnt[x],p=True)[0] == par:
				pass
			else:
				mc.parent(rbn_jnt[x],par)
	
	if mc.objExists("*:*BtwAll*Grp") == True:
		deleteUnUseGrps(name = "*:*BtwAll*Grp")

	if mc.objExists("*:*RbnSkin*Grp") == True:
		deleteUnUseGrps(name = "*:*RbnSkin*Grp")
		
	if mc.objExists("*:*SpineSkin_Grp") == True:
		deleteUnUseGrps(name = "*:*SpineSkin_Grp")

	#clean base bpm joint
	# baseBpmGrp = mc.ls('*BaseBpmRigJnt_Grp*')
	# baseBpmJnt = mc.listRelatives(baseBpmGrp,ad=True,type='joint')
	# if baseBpmJnt != []:
	#     for each in baseBpmJnt:
	#         mc.delete(each,cn=True)


	SknJtGrp = mc.createNode('transform' ,n = 'SkinJnt_Grp')
	GeoGrp = mc.createNode('transform' ,n = 'CharGeo_Grp')


	# scale = mc.getAttr(mc.ls('AllMover_Ctrl')[0]+'.sy')
	# scaAttr = ['sx','sy','sz']
	# for eachAttr in scaAttr:
	#     mc.setAttr(SknJtGrp+'.'+eachAttr,scale)
	mc.parent(skinGrpSel[0] ,SknJtGrp)
	mc.parent('Geo_Grp' ,GeoGrp)
	clarBtwJnt()
	## Clean Group Empty
	cleanGroupEmpty()
	addMiarmyDeformAttrToGeo()
	deleteTechGeo()
	charName = renameLocoFromFileName()
	#collectGrp(charName)
	print 'clear unused nodes'
	print "----------------------- Everything is Done !! -----------------------------\n----------------------- Everything is Done !! -----------------------------\n----------------------- Everything is Done !! -----------------------------"



def cleanGroupEmpty():
	rigGrp = mc.ls('Skin_Grp')[0]
	mc.setAttr('{}.visibility'.format(rigGrp), 1)
	jntAll = []
	children = mc.listRelatives(rigGrp, c = True)
	for each in children:
		if '_Jnt' in each:
			if not 'Root_Jnt' in each:
				mc.parent(each, 'Root_Jnt')            
				jntAll.append(each)
		else:
			jntList = mc.listRelatives(each, ad = True, type = 'joint')
			# print jntList, '>>>', 'jntList...'
			if not jntList == None:
				for jntEach in jntList:
					mc.parent(jntEach, 'Root_Jnt')
					jntAll.append(jntEach)
			## Delete Group empty
			mc.delete(each)
		# else:
		#     mc.parent(each, 'Root_Jnt')            
		#     jntAll.append(each)
#add loco name Rig_Grp
def deleteTechGeo():
	if mc.objExists('TechGeo_Grp'):
		techGeo = mc.listRelatives('TechGeo_Grp',c=1)
		mc.delete(techGeo)

def addMiarmyDeformAttrToGeo():
	sels = mc.ls('MdGeo_Grp')
	objLists = utaCore.listGeo(sels = sels, nameing = '_Geo', namePass = '_Grp')
	for x in range(len(objLists)):

		geo = objLists[x].split('|')[-1]
		checkAttr = mc.ls('{}Shape.McdRTDisplay'.format(geo))

		if len(checkAttr) !=0:
			print 'Mark Selected Geo Real Time Displayable is Already : {}'.format(x+1)
			mc.select(cl = True)
		else:
			mc.select(objLists[x])
			McdSimpleCmd.McdAddRTDisplayFlag()
			mc.select(cl = True)


def renameLocoFromFileName():
	asset = context_info.ContextPathInfo() 
	charName = asset.name
		
	rigGrp = mc.ls('Rig_Grp')[0]
	children = cmds.listRelatives(rigGrp, ad = True)
	children.append(rigGrp)
	for each in children:
		obj = each.split('|')[-1]
		newObj = mc.rename(each,'{}_{}_Loco'.format(obj, charName))
	return charName +'_Loco'
		#print newObj

def collectGrp(locoName):
	grpName = 'Rig_Grp_{}'.format(locoName)
	skinGrp=  'SkinJnt_Grp'
	charGrp = 'CharGeo_Grp'
	exportGrp = mc.createNode('transform',n='Export_Grp')
	mc.parent(grpName,skinGrp,charGrp,exportGrp)

def clarBtwJnt():
	if mc.ls('*BtwAll*_Grp'):
		for grp in mc.ls('*BtwAll*_Grp'):
			par = mc.listRelatives(grp,p=1)
			reLocate = []
			if par:
				print par[0]
				for jnt in mc.listRelatives(grp,type='joint',ad=1):
					if mc.listRelatives(jnt,c=1):
						reLocate.append(jnt)
					else:
						pass
			mc.parent(reLocate,par)
			mc.delete(grp)
	else:
		print 'This rig has no btw jnt'