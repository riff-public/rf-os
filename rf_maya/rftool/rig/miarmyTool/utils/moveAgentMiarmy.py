import maya.cmds as mc

class MoveAgent(object):
    def __init__(self):
        self.winName = 'MoveAgentUI'
    def buildMoveAgentUI(self):
        winWidth = 300
        tmpWidth = [winWidth*0.3, winWidth*0.7]
        mc.window(self.winName,title = 'MoveAgent' , width = winWidth)
        mainCL = mc.columnLayout()
        mc.rowLayout(numberOfColumns=2 , columnWidth2=tmpWidth)
        mc.text(label = '  Placement Num', align='left' , width=tmpWidth[0])
        mc.intField('PlacementNum' ,ec='enter',v=0, ed = True , width=tmpWidth[1])
        mc.setParent('..')
        mc.rowLayout(numberOfColumns=2 , columnWidth2=tmpWidth)
        mc.text(label = '  Agent Number', align='left' , width=tmpWidth[0])
        mc.textField('AgentNumber' ,ec='enter', ed = True , width=tmpWidth[1])
        mc.setParent('..')
        mc.rowLayout(numberOfColumns=2 , columnWidth2=tmpWidth)
        mc.text(label = '  chID', align='left' , width=tmpWidth[0])
        mc.radioButtonGrp('chID',la2=['translate','rotate'],numberOfRadioButtons=2,cw2=[(tmpWidth[1]*0.5),(tmpWidth[1]*0.5)])
        mc.setParent('..')
        mc.rowLayout(numberOfColumns=2 , columnWidth2=tmpWidth)
        mc.text(label = '  Axis', align='left' , width=tmpWidth[0])
        mc.radioButtonGrp('Axis',la3=['X','Y','Z'],numberOfRadioButtons=3,cw3=[(tmpWidth[1]*0.33),(tmpWidth[1]*0.33),(tmpWidth[1]*0.33)])
        mc.setParent('..')
        mc.rowLayout(numberOfColumns=2 , columnWidth2=tmpWidth)
        mc.text(label = '  offVal', align='left' , width=tmpWidth[0])
        mc.floatField('offVal' ,ec='enter',v=180, ed = True , width=tmpWidth[1])
        mc.setParent('..')
        mc.rowLayout(numberOfColumns=1)
        mc.button('Run Move Agent', width=winWidth*1.015,c=self.run_cmd)
        mc.setParent('..')
        mc.showWindow()
        

    def addPlaceOffsetRecordFix(self,numPlacementNode=None,AllplacerId=None,offVal=None,chID=None, opt=None,*args):
        # find McdPlace
        placeNode = 'McdPlace{}Shape'.format(numPlacementNode)
            
        for placerId in AllplacerId:
            strCh = str(chID)
            if opt == 0:
                offVal *= -1.0
            
            collectIds = []
            agentCounter = 0
            while(True):
                agentId = mc.getAttr(placeNode + ".userOffset[" + str(agentCounter * 8) + "]")
                if int(agentId) == -1:
                    break
                else:
                    collectIds.append(int(agentId))
                agentCounter += 1;
            
            try:
                optIndex = collectIds.index(placerId)
            except:
                optIndex = -1
                
            if optIndex == -1:
                print "recored inserted..."
                optIndex = len(collectIds)
                mc.setAttr(placeNode + ".userOffset[" + str(optIndex * 8) + "]", int(placerId))
                mc.setAttr(placeNode + ".userOffset[" + str(optIndex * 8 + 1) + "]", 0)
                mc.setAttr(placeNode + ".userOffset[" + str(optIndex * 8 + 2) + "]", 0)
                mc.setAttr(placeNode + ".userOffset[" + str(optIndex * 8 + 3) + "]", 0)
                mc.setAttr(placeNode + ".userOffset[" + str(optIndex * 8 + 4) + "]", 0)
                mc.setAttr(placeNode + ".userOffset[" + str(optIndex * 8 + 5) + "]", 0)
                mc.setAttr(placeNode + ".userOffset[" + str(optIndex * 8 + 6) + "]", 0)
                mc.setAttr(placeNode + ".userOffset[" + str(optIndex * 8 + 7) + "]", -1)
                
                
            # operate on optIndex
            toModVal = mc.getAttr(placeNode + ".userOffset[" + str(optIndex * 8 + chID) + "]")
            toModVal += offVal
            mc.setAttr(placeNode + ".userOffset[" + str(optIndex * 8 + chID) + "]", toModVal)
        

    def run_cmd(self,*args):
        numPlacement = mc.intField('PlacementNum' ,q=True,v=True)
        AllplacerIdText = mc.textField('AgentNumber' ,q=True,text=True)
        offVal = mc.floatField('offVal' ,q=True,v=True)
        AllplacerId = AllplacerIdText.split(',')
        chIDQ = mc.radioButtonGrp('chID',query=True, sl=True)
        Axis = mc.radioButtonGrp('Axis',query=True, sl=True)
        if chIDQ == 1:
            if Axis == 1: chID = 1
            elif Axis == 2: chID = 2
            elif Axis == 3: chID = 3
        elif chIDQ == 2:
            if Axis == 1: chID = 4
            elif Axis == 2: chID = 5
            elif Axis == 3: chID = 6

        self.addPlaceOffsetRecordFix(numPlacementNode=numPlacement,
                            AllplacerId=AllplacerId,
                            offVal=offVal,
                            chID=chID,
                            opt=1)
        print '------Done------'

    def showUI(self):
        if mc.window ( self.winName , exists = True ) :
            mc.deleteUI ( self.winName ) 
        else : pass
        self.buildMoveAgentUI()    