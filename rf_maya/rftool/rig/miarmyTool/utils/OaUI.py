import sys, os
import maya.cmds as mc
import pymel.core as pm
from collections import OrderedDict
import sys, re
sys.path.append("P:/DarkHorse/Research/Hong/Miarmy/utils")
import mmOaPrepUtils as mmoap
reload(mmoap)
import mmAgentUtils as mmoa
reload(mmoa)
import mmBakeAnimUtils as mmbau
reload(mmbau)
import miarmySwitchChildren as msct
reload(msct)
import miarmyNewPlacement as mmnp
reload(mmnp)
import blendshapeFcltoMrm as blndFcl
reload (blndFcl)
import connectCtrlOA
reload (connectCtrlOA)
from utaTools.utapy import utaCore
reload(utaCore)
from utaTools.utapy import oaBatch
reload(oaBatch)

from rf_utils.context import context_info
reload(context_info)


class oaCustomUI():
	def __init__(self):
		self.winName = 'mmOAUI'
	# Path ---------------------------------------
		self.core = '%s/core' % os.environ.get('RFSCRIPT')
		self.path = '{core}/rf_maya/rftool/rig/'.format(core = self.core)


	#-----------------------------------------------------------------------------
	#----------- OaPrep Function ----------------------------------------#
	#-----------------------------------------------------------------------------

	def runCleanForMiarmy(self,*args):
		## Check Path File is Ready
		path = mc.file(q = True, loc = True)
		if not path == 'unknown':
			entity = context_info.ContextPathInfo()
			if 'Oa' in entity.name:
				print entity.name
				mmoap.runCleanForMiarmy()
			elif 'OA' in entity.name:
				print entity.name
				mmoap.runCleanForMiarmy()
			elif entity.type == 'prop':
				print entity.type
				mmoap.runCleanForMiarmy()
			else:
				return

		#mmoap.renameLocoFromFileName()
		#mmoap.deleteTechGeo()

	def runBatchOa(self,*args):
		fromObj = pm.textField("FromTextField", q = True  , text = True)
		toObj = pm.textField("ToTextField", q = True  , text = True)
		processObj = pm.textField("processTextField", q = True  , text = True)

		processLists = processObj.split(',')

		oaBatch.run(    fromObj = fromObj, 
						toObj = toObj,
						batchProcess = processLists )

	def deleteTechGeo(self,*args):
		mmoap.deleteTechGeo()

	def runOaBlendShapeAuto(self,*args):
		blndFcl.BlendshapFcltoMrmOA().runOaBlendShapeAuto()

	def reConRenderLayer(self,*args):
		blndFcl.BlendshapFcltoMrmOA().reConRenderLayer()

	def assignRenderShade(self,*args):
		blndFcl.BlendshapFcltoMrmOA().assignRenderShade()

	#-----------------------------------------------------------------------------
	#----------- Animation Bake Function ----------------------------------------#
	#-----------------------------------------------------------------------------
	def getAllNameSpace(self,*args):

		getName = []
		mesh = mc.ls(type="joint")
		for i in mesh:
			if re.search(":",i):
				name, rem = i.split(":")
				if re.findall(".*[0-9]",name):	
					getName.append(name)
		self.namespaceList = list(OrderedDict.fromkeys(getName))
		return self.namespaceList

	#---------------------------------------------- ^
	#---------------------------------------------- ^
	#---------------------------------------------- ^ get namespace
	
	def addItems(self,*args):
		list_name = []
		sel = mc.ls(sl=1)
		if len(sel) > 0:
			for i in sel:
				if re.search(":",i):
					namespace_name,object_name = i.split(":")
					list_name.append(namespace_name)
					
					addItem =  mc.textScrollList(self.namespaceList,q=1,ai=1)
					if addItem == None:
						addItem = []
					compareItem = [x for x in list_name if x not in addItem]
					mc.textScrollList(self.namespaceList,e=1,a=compareItem)
				else:
					mc.confirmDialog(m="This object has not namespace")
		else:
			mc.confirmDialog(m="Please select object !!!")
	
	def removeItems(self,*args):
		removeItem = mc.textScrollList(self.namespaceList,q=1,si=1)
		if removeItem != None:
			mc.textScrollList(self.namespaceList,e=1,ri=removeItem)
		else:
			mc.confirmDialog(m="Please select items in textScrollList !!!")

	def resetItems(self,*args):
		resetItems = mc.textScrollList(self.namespaceList,q=1,ai=1)
		newItems = self.getAllNameSpace()
		if resetItems == None:
			mc.textScrollList(self.namespaceList,e=1,a=newItems)
		else:
			mc.textScrollList(self.namespaceList,e=1,ra=True)
			mc.textScrollList(self.namespaceList,e=1,a=newItems)
	
	def selectItems(self,*args):
		selectItem = mc.textScrollList(self.namespaceList,q=1,si=1)
		if selectItem != None:
			mmbau.animMiarmyList(selectItem)
		else:
			mc.confirmDialog(m="Please select items in textScrollList !!!")
			
	def selectAllItems(self,*args):
		selectAllItem = mc.textScrollList(self.namespaceList,q=1,ai=1)
		if selectAllItem != None:
			mmbau.animMiarmyList(selectAllItem)
		else:
			mc.confirmDialog(m="There is not any item in textScrollList !!!")
	
	#-----------------------------------------------------------------------------
	#----------- Original Agent Function ----------------------------------------#
	#-----------------------------------------------------------------------------
	def getLocoName(self,*args):
		locoNameUi = pm.textField( "txtImExportNodeField", q=1,text=1)
		return locoNameUi

	def resizePhyButton(self,*args):
		sizeUi = pm.textField('sizeTextField',q=1 ,text=1)
		locoName = self.getLocoName()
		mmoa.resizePhyJoint(locoName,size = float(sizeUi))

	def MirrorMMBoxButton(self,*ards):
		mmoa.mirSelectedMMBoxGeo()
	
	def MulMcdCreateDecisionButton(self,*args):
		mmoa.mulMcdCreateDecision(locoName = self.getLocoName())
	
	def renameDummyShapeShapeNode(self,*args):
		mmoa.renameDummyShapeShapeNode()

	def adjColumn(self,*args):
		tabIdx = mc.tabLayout('mainTab',q=1,sti=1)

		if tabIdx ==1:
			mc.tabLayout('mainTab',e=1,w=380,h=450,bgc = (0.129, 0.129, 0.129))
			mc.columnLayout('Batch',e=1,w=380,h=450,bgc = (0.129, 0.129, 0.129))
			mc.window(self.winName,e=1,w=380,h=450,bgc = (0.129, 0.129, 0.129))

		if tabIdx ==2:
			mc.tabLayout('mainTab',e=1,w=380,h=450,bgc = (0.129, 0.129, 0.129))
			mc.columnLayout('Prep',e=1,w=380,h=450,bgc = (0.129, 0.129, 0.129))
			mc.window(self.winName,e=1,w=380,h=450,bgc = (0.129, 0.129, 0.129))

		elif tabIdx ==3:
			mc.tabLayout('mainTab',e=1,w=380,h=450,bgc = (0.129, 0.129, 0.129))
			mc.columnLayout('OA',e=1,w=380,h=450,bgc = (0.129, 0.129, 0.129))
			mc.window(self.winName,e=1,w=380,h=450,bgc = (0.129, 0.129, 0.129))

		elif tabIdx ==4:
			mc.tabLayout('mainTab',e=1,w=380 , h =360,bgc = (0.129, 0.129, 0.129))
			mc.columnLayout('bakeAnim',e=1,w=380,h =360,bgc = (0.129, 0.129, 0.129))
			mc.window(self.winName,e=1,w=380,h =360,bgc = (0.129, 0.129, 0.129))

		elif tabIdx ==5:
			mc.tabLayout('mainTab',e=1,w=380,h=280,bgc = (0.129, 0.129, 0.129))
			mc.columnLayout('addRigToOa',e=1,w=380,h =280,bgc = (0.129, 0.129, 0.129))
			mc.window(self.winName,e=1,w=380,h=280,bgc = (0.129, 0.129, 0.129))

		elif tabIdx ==6:
			mc.tabLayout('mainTab',e=1,w=380 , h =460,bgc = (0.129, 0.129, 0.129))
			mc.columnLayout('switchMMChild',e=1,w=360,h =460,bgc = (0.129, 0.129, 0.129))
			mc.window(self.winName,e=1,w=380,h =460,bgc = (0.129, 0.129, 0.129))

	def pngImags(self):
		imags = (self.path + "utaTools/utaicon/uiIcon/")
		return imags

	def mmOAUI(self):
		ui = mc.window(self.winName,title = 'Riff Miarmy Tools @Riff_Animation_Studio No.0.1 By TATEAM (11/11/19)',s=0)
		mainLayout = mc.tabLayout('mainTab',sc= self.adjColumn,w=320,h=280,bgc = (0.129, 0.129, 0.129))
		## setWindowIcon
		utaCore.wintes(ui, self.pngImags() ,'RiffStudioUse.png')

	## 1. BatchUser
		batchLayout = mc.columnLayout('Batch',cal='center',adj=1)
		mc.separator(h=20,style ='none')

		## Note
		mc.text(l="..:: Batch Processing ::..",al='center')
		mc.text(l="",al='center')
		mc.text(l="      	1. Copy 'Skel Hero File' ", al='left')
		mc.text(l="      	2. Copy 'Ctrl Hero File.", al='left')
		mc.text(l="      	3. Copy 'BodyRig Work File' and Replace Reference 'Ctrl File'.", al='left')
		mc.text(l="      	4. Copy 'Main Work File' and Replace Reference 'BodyRig File'.", al='left')
		mc.text(l="      	5. Publish Hero File", al='left')
		mc.separator(h=20,style ='none')

		mc.separator(h=10, w = 370)

		batchLayoutRow = pm.rowColumnLayout ( nc = 3 , p = batchLayout , w = 370, cw = [(1,156),(2,58),(3,156)]) ;
		textFromObj = mc.textField('FromTextField' ,text = 'Input Character Name',ec='From:', ed = True, h = 25 , p =batchLayoutRow)
		pm.text( label='>> To >>' , p = batchLayoutRow, al = "center")
		textToObj = mc.textField('ToTextField' ,text = 'Input Character Oa Name' ,ec='To:', ed = True, h = 25 , p =batchLayoutRow)

		mc.separator(h=10, w = 370)

		batchProcessLayoutRow = pm.rowColumnLayout ( nc = 1 , p = batchLayout , w = 370) ;
		processTextObj = mc.textField('processTextField' ,text = "skel,ctrl,bodyRig,main" ,ec='To', ed = True, h = 25 , w = 370, p =batchProcessLayoutRow)

		mc.separator(h=10, w = 370)

		batchLayoutRow2 = pm.rowColumnLayout ( nc = 1, p = batchLayout ) ;
		pm.symbolButton ( 'createBatch' , image = ("%s%s" % (self.pngImags(),"Batch (Custom).png")),h = 200, w = 370, p = batchLayoutRow2 , c = self.runBatchOa , bgc = (0, 0, 0)) ; 
		
		mc.setParent(mainLayout)

	## 2. Prep Process
		prepLayout = mc.columnLayout('Prep',cal='center',adj=1)
		mc.separator(h=20,style ='none')

		## Note
		mc.text(l="..:: Prep Processing ::..",al='center')
		mc.text(l="",al='center')
		mc.text(l="      	1. Import Hero File From Hero Folder.",al='left')
		mc.text(l="      	2. Generate 'Skel_Grp' and Parent chain joint.",al='left')
		mc.text(l="      	3. Generate 'CharGeo_Grp'.",al='left')
		mc.text(l="      	4. Generate 'Rig_Grp' and rename loco name.",al='left')
		mc.separator(h=85,style ='none')
		pm.symbolButton ( 'createPrepFile' , image = ("%s%s" % (self.pngImags(),"miarmy PrepFile.jpg")),h = 200, w = 180, p = prepLayout , c = self.runCleanForMiarmy , bgc = (0, 0, 0)) ; 
		mc.separator(style='none',h=20)
		mc.setParent(mainLayout)

	## 3. Oa Process
		OALayout = mc.columnLayout('OA',cal='center',adj=1)
		mc.separator(h=20,style ='none')

		## Note
		mc.text(l="..:: OA Processing ::..",al='center')
		mc.text(l="",al='center')
		mc.text(l="      	1. Reference Hero prep file in scene.",al='left')
		mc.text(l="      	2. Miarmy Ready.",al='left')
		mc.text(l="      	3. Parent prep file group to Setup_Grp.",al='left')
		mc.text(l="      	4. Generate Original Agent.",al='left')
		mc.text(l="      	5. Sent Active Geo to Original Agent.",al='left')
		mc.text(l="      	6. BlendShape Loco Geo.",al='left')
		mc.text(l="      	7. Connect Fcl joint.",al='left')
		mc.text(l="      	8. Rename Loco Name.",al='left')
		mc.text(l="      	9. Generate Render Layer 'Texture'.",al='left')

		mc.separator(h=20,style ='none')

		pm.symbolButton ( 'GenerateOA' , image = ("%s%s" % (self.pngImags(),"miarmy OAFile Use.jpg")),h = 139, w = 180, p = OALayout , c = self.runOaBlendShapeAuto , bgc = (0, 0, 0)) ; 
		mc.separator(style='none',h=10)

		renderLayout = mc.rowLayout(nc=2,p=OALayout)

		pm.symbolButton ( 'ReConRenderLayer' , image = ("%s%s" % (self.pngImags(),"miarmy ReRenderLayerUSE.jpg")),w = 190 ,h = 50, p = renderLayout , c = self.reConRenderLayer , bgc = (0, 0, 0)) ; 
		pm.symbolButton ( 'Assign Render Shade' , image = ("%s%s" % (self.pngImags(),"miarmyAssignRenderShadeUSE.jpg")),w = 190 ,h = 50, p = renderLayout , c = self.assignRenderShade , bgc = (0, 0, 0)) ; 
		mc.setParent(mainLayout)

	## 4. BakeAnim Process
		AnimLayout = mc.columnLayout('bakeAnim',cal='center',adj=1)
		mc.separator(h=20,style ='none')
		nameSpaceLs = self.getAllNameSpace()
		mc.text(l="..:: Select namespace in a box ::..")
		mc.separator(h=14,style ='none')
		layoutOp1 = mc.rowLayout(nc=1,p=AnimLayout)

		self.namespaceList = mc.textScrollList(nr=6,ams=True,a=nameSpaceLs,w = 370,h=150)
		layoutOp2 = mc.rowLayout(nc=3,p=AnimLayout)
		mc.button(l="Add Item",w=121,h=50,p = layoutOp2 ,c = self.addItems)
		mc.button(l="Remove Item",w=122,h=50,bgc=[0.5,0.5,0.5],c = self.removeItems)
		mc.button(l="Reset Item",w=121,h=50,c = self.resetItems)
		mc.setParent("..")
		mc.separator(h=20,style ='none')
		layoutOp3 = mc.rowLayout(nc=2,p=AnimLayout)
		mc.button(l="Bake Selected Anim",w = 190, h=50,bgc=[0.278,0.272,0.376],c = self.selectItems)
		mc.button(l="Bake All Anim",w = 190,h=50,bgc=[0.376,0.272,0.278],c = self.selectAllItems)

		mc.setParent(mainLayout)

	## 5. addRigToOa
		addRigToOa = mc.columnLayout('addRigToOa',cal='center',adj=1)
		connectCtrlOA.ConnectOA().buildconnectOAUI()
		mc.setParent(mainLayout)

	## 6. switchMMChild
		switchMMChild = mc.columnLayout('switchMMChild',cal='center',co=['both',5],adj=1)
		msct.MiarmySwitchChildren().switchMiarmyChildrenUI()

	def showUI(self):
		if mc.window(self.winName,exists=1):
			print 'delUI'
			mc.deleteUI(self.winName)
		self.mmOAUI()
		mc.showWindow(self.winName)			
