import maya.cmds as mc
import maya.mel as mm
import pymel.core as pm
import os , sys
import shutil
from rf_utils.context import context_info
reload(context_info)


class Run(object):
    def __init__(self):
        self.winName = 'MixamoToMiarmyUI'

    def show(self , *args):
        # check if window exists
        if mc.window ( self.winName , exists = True ) :
            mc.deleteUI ( self.winName ) 
        else : pass

        winWidth = 300

        pm.window(self.winName,title = 'Mixamo To Miarmy UI' , width = winWidth , rtf = True, mxb = False , s=1)
        mainLayout = pm.columnLayout()
        pm.separator(h = 10)
        winLayout = pm.rowColumnLayout( w = winWidth , p = mainLayout , nc = 3 , cw = [(1, winWidth*0.01), (2, winWidth*0.98), (3, winWidth*0.01)]) ;
        space1Layout = pm.rowColumnLayout( p = winLayout , nc = 1  )
        toolLayout = pm.rowColumnLayout( p = winLayout , nc = 1 )
        space2Layout = pm.rowColumnLayout( p = winLayout , nc = 1 )

        rigTab_module = pm.tabLayout( innerMarginWidth = 5 , innerMarginHeight = 5 , p = toolLayout , w=winWidth*0.97)
        with rigTab_module:
            rigRigTab = pm.rowColumnLayout( "rig", nc = 1 , cw=(1 , winWidth*0.97))
            with rigRigTab:
                pm.rowColumnLayout( nc = 1 , cw=(1 , winWidth*0.97))
                pm.separator(vis = False)
                pm.text(l='1. ref fbx file and scaling to createTmpJnt' , al ='left')
                pm.text(l='2. run create tmp jnt (publish skel)' , al ='left')
                pm.text(l='3. run rig (publish ctrl)' , al ='left')
                pm.text(l='4. ref fbx file and scaling to convert joints skin' , al ='left')
                pm.text(l='   select geo from fbx and geo from model 1 to 1' , al ='left')
                pm.text(l='   run transfer weight (publish bodyRig)' , al ='left')
                pm.text(l='5. publish main' , al ='left')
                pm.separator(h = 10, st = 'in')
                pm.setParent('..')


                pm.rowColumnLayout( nc = 1 , cw=(1 , winWidth*0.97))
                pm.button(l='Open Path Mixamo.fbx' , h=40 , c=self.openPathMixamoModelFbxFiles_cmd)
                pm.setParent('..')

                pm.rowColumnLayout( nc = 1 , cw=(1 , winWidth*0.97))
                pm.button(l='Create TmpJnt' , h=40 , c=self.createTmpJnt_cmd)
                pm.setParent('..')

                pm.rowColumnLayout( nc = 1 , cw=(1 , winWidth*0.97))
                pm.button(l='Run Rig' , h=40 , c=self.runRig_cmd)
                pm.setParent('..')

                pm.rowColumnLayout( nc = 1 , cw=(1 , winWidth*0.97))
                pm.button(l='Transfer Weight' , h=40 , c=self.transferWeight_cmd)
                pm.setParent('..')


        with rigTab_module:
            prepRigTab = pm.rowColumnLayout( "prep", nc = 1 , cw=(1 , winWidth*0.97))
            with prepRigTab:
                pm.rowColumnLayout( nc = 1 , cw=(1 , winWidth*0.97))
                pm.separator(vis = False)
                pm.text(l='1. just click run script' , al ='left')
                pm.separator(h = 10, st = 'in')
                pm.setParent('..')

                pm.rowColumnLayout( nc = 1 , cw=(1 , winWidth*0.97))
                pm.button(l='Run Prep' , h=40 , c=self.runPrep_cmd)
                pm.setParent('..')


        with rigTab_module:
            OARigTab = pm.rowColumnLayout( "OA", nc = 1 , cw=(1 , winWidth*0.97))
            with OARigTab:
                pm.rowColumnLayout( nc = 1 , cw=(1 , winWidth*0.97))
                pm.separator(vis = False)
                pm.text(l='1. ref prep file' , al ='left')
                pm.text(l='2. Miarmy > Miarmy Ready!' , al ='left')
                pm.text(l='3. parent Jnt Grp , Geo Grp , Rig Grp to Setup grp' , al ='left')
                pm.text(l='4. Miarmy > Original Agents > Create Original Agent' , al ='left')
                pm.text(l='5. select all geo' , al ='left')
                pm.text(l='6. Original Agents > send active geo to original agent' , al ='left')
                pm.text(l='7. Modifiy Agent name at agent manager' , al ='left')
                pm.text(l='8. export' , al ='left')
                pm.separator(h = 10, st = 'in')
                pm.setParent('..')

                pm.rowColumnLayout( nc = 1 , cw=(1 , winWidth*0.97))
                pm.button(l='Ref Prep' , h=40 , c=self.refPrep_cmd)
                pm.setParent('..')


        with rigTab_module:
            AnimTab = pm.rowColumnLayout( "Anim", nc = 1 , cw=(1 , winWidth*0.97))
            with AnimTab:
                pm.rowColumnLayout( nc = 1 , cw=(1 , winWidth*0.97))
                pm.separator(vis = False)
                pm.text(l='1. create task anim(working file) in cycle-library' , al ='left')
                pm.text(l='2. ref rig from sg_manager' , al ='left')
                pm.text(l='3. import anim fbx' , al ='left')
                pm.text(l='4. open mocapRig UI' , al ='left')
                pm.text(l='5. click rig object and get namespace and load tmp' , al ='left')
                pm.text(l='6. save everytap and write tPose and bindPose' , al ='left')
                pm.text(l='7. click remove rig' , al ='left')
                pm.text(l='8. open Bake App UI' , al ='left')
                pm.text(l='9. click second tab and select project&asset' , al ='left')
                pm.text(l='10. click create human IK Character(3rd button)' , al ='left')
                pm.text(l='11. click set t-pose' , al ='left')
                pm.text(l='12. click create defaultIK in scene until its pass' , al ='left')
                pm.text(l='13. at right ui, select charactername , source character' , al ='left')
                pm.text(l='14. fix pose and export from cycle-library ui' , al ='left')
                pm.text(l='15. set timeslider and export' , al ='left')
                pm.separator(h = 10, st = 'in')
                pm.setParent('..')

                pm.rowColumnLayout( nc = 1 , cw=(1 , winWidth*0.97))
                pm.button(l='Cycle-Library UI' , h=30 , c=self.openCycleLibraryUI_cmd)
                pm.setParent('..')

                pm.rowColumnLayout( nc = 1 , cw=(1 , winWidth*0.97))
                pm.button(l='Open anim path' , h=30 , c=self.openPathMixamoAnimFbxFiles_cmd)
                pm.setParent('..')

                pm.rowColumnLayout( nc = 1 , cw=(1 , winWidth*0.97))
                pm.button(l='MocapRig UI' , h=30 , c=self.openMocapRigUI_cmd)
                pm.setParent('..')

                pm.rowColumnLayout( nc = 1 , cw=(1 , winWidth*0.97))
                pm.button(l='Remove reference' , h=30 , c=self.removeAllRef_cmd)
                pm.setParent('..')

                pm.rowColumnLayout( nc = 1 , cw=(1 , winWidth*0.97))
                pm.button(l='Open Bake app UI' , h=30 , c=self.openBakeAppUI_cmd)
                pm.setParent('..')

                pm.rowColumnLayout( nc = 1 , cw=(1 , winWidth*0.97))
                pm.button(l='Set FBX T-Pose' , h=30 , c=self.setTPose_cmd)
                pm.setParent('..')

                pm.rowColumnLayout( nc = 1 , cw=(1 , winWidth*0.97))
                pm.button(l='Create default IK' , h=30 , c=self.createDefaultIK_cmd)
                pm.setParent('..')



        pm.showWindow()

    def openPathMixamoModelFbxFiles_cmd(self , *args):
        path = "P:/Hanuman/rnd/rigDev/Mixamo/MixamoModel"
        os.startfile(path)


    def openPathMixamoAnimFbxFiles_cmd(self , *args):
        path = "P:/Hanuman/rnd/rigDev/Mixamo/MixamoAnim"
        os.startfile(path)

    def createTmpJnt_cmd(self , *args):
        from rf_maya.rftool.rig.miarmyTool.mixamo import convertJoints
        reload(convertJoints)
        convertJoints.Run()

    def runRig_cmd(self , *args):
        path = self.getDataPath()
        if not os.path.exists(path.split('/data')[0]):
            os.mkdir(path.split('/data')[0])
        if not os.path.exists(path):
            os.mkdir(path)

        mainScriptPath = os.path.join(path , 'mainRig.py')
        ctrlShapePath = os.path.join(path , 'ctrl_shape.dat')

        if not os.path.exists(mainScriptPath):
            print 'copy mainRig.py'
            self.copyTemplateSctipt('mainRig')
        if not os.path.exists(ctrlShapePath):
            print 'copy ctrl_shape.dat'
            self.copyTemplateSctipt('ctrl_shape' , 'dat')

        if os.path.exists(mainScriptPath):
            sys.path.insert(0,path)

            import mainRig
            reload(mainRig)
            try:
                print 'Run mainRig'
                mainRig.main()
            except :
                print 'Error'
                pass
            
            del(mainRig)
            sys.path.remove(path)


    def transferWeight_cmd(self , *args):
        from rf_maya.rftool.rig.miarmyTool.mixamo import transferWeight
        reload(transferWeight)
        transferWeight.Run()

    def runPrep_cmd(self , *args):
        from rf_maya.rftool.rig.miarmyTool.utils import mmOaPrepUtils as mmoap
        reload(mmoap)
        path = mc.file(q = True, loc = True)
        if not path == 'unknown':
            entity = context_info.ContextPathInfo()
            if 'OA' in entity.name:
                print entity.name
                mmoap.runCleanForMiarmy()
            elif entity.type == 'prop':
                print entity.type
                mmoap.runCleanForMiarmy()
            else:
                return

        allGrp = mc.ls(tr=True,o=True)
        expGrp = 'Export_Grp'
        if not mc.objExists(expGrp):
            expGrp = mc.createNode('transform',n='Export_Grp')

        delGrp = [expGrp,'Delete_Grp','persp','top','front','side','bottom']
        for each in allGrp:
            prnt = mc.listRelatives(each,p=True)
            chd = mc.listRelatives(each,c=True,type='camera')
            if prnt or chd:
                delGrp.append(each)

        #remove delGrp from allGrp
        for each in delGrp:
            if each in allGrp:
                allGrp.remove(each)

        mc.parent(allGrp,expGrp)

    def refPrep_cmd(self , *args):
        entity = context_info.ContextPathInfo() 
        projectPath = os.environ['RFPROJECT']
        charName = entity.name
        asset = entity.copy()
        asset.context.update(step='rig', process='prep', res='md')
        heroPath = asset.path.output_hero()
        prepFile = asset.publish_name(hero=True)
        prepFilePath = '%s/%s' % (heroPath, prepFile)
        prepFilePath = (prepFilePath).replace('$RFPUBL',projectPath)
        mc.file(prepFilePath, r=True , namespace='prep_md')

    def copyTemplateSctipt(self , scriptName='' , fileType = 'py' , *args):
        tmpPath = 'O:/Pipeline/core/rf_maya/rftool/rig/miarmyTool/mixamo/{}.{}'.format(scriptName , fileType)
        dataPath = self.getDataPath()
        dst = os.path.join(dataPath , '{}.{}'.format(scriptName , fileType))
        shutil.copy2(tmpPath , dst)

    def getDataPath(self , *args):
        asset = context_info.ContextPathInfo()
        projectPath = os.environ['RFPROJECT']
        fileWorkPath = (asset.path.name()).replace('$RFPROJECT',projectPath)
        filePath = fileWorkPath.replace('work','publ')
        path = filePath + '/rig/data'
        return path

    def openCycleLibraryUI_cmd(self , *args):
        from rf_app.cycle_library import app
        reload(app)
        myApp = app.show()

    def openMocapRigUI_cmd(self , *args):
        from rf_maya.rftool.rig.mocap.mocapRig import mocapRigUI
        reload(mocapRigUI)
        x = mocapRigUI.mocapRig().show_ui()

    def removeAllRef_cmd(self , *args):
        ref = mc.file(r=True,q=True)
        for each in ref:
            mc.file(each , rr=True)

    def openBakeAppUI_cmd(self , *args):
        from rf_app.mocap.cutMocap import bake_app
        reload(bake_app)
        bake_app.show()

    def setTPose_cmd(self , *arhs):
        from rf_utils.context import context_info
        reload(context_info)
        asset = context_info.ContextPathInfo()
        charGrp = 'Char_Grp'
        if not mc.objExists(charGrp):
            mc.createNode('transform',n=charGrp)

        fbxRoot = mc.ls('*:Hips','Hips')
        childJnt = mc.listRelatives(fbxRoot,ad=True,type='joint')
        allFbxJnt = childJnt + fbxRoot
        mc.currentTime(0)
        mc.setKeyframe(allFbxJnt)
        mc.currentTime(-1)
        for eachJnt in allFbxJnt:
            mc.setAttr(eachJnt+'.r',0,0,0)
        mc.setAttr(fbxRoot[0]+'.tx',0)
        mc.setKeyframe(allFbxJnt)

        ref = mc.file(r=True ,q=True)
        for each in ref:
            ns = mc.referenceQuery(each, ns=True)
            ns = ns.split(':')[-1]
            if ns == asset.name:
                prnt = mc.listRelatives('{}:Rig_Grp'.format(ns) , p=True)
                if not prnt:
                    mc.parent('{}:Rig_Grp'.format(ns),charGrp)
                mc.setAttr('{}:Leg_L_Ctrl.fkIk'.format(ns),0)
                mc.setAttr('{}:Leg_R_Ctrl.fkIk'.format(ns),0)

        # scope frame
        anim_curves = mc.ls(type=['animCurveTA', 'animCurveTL', 'animCurveTT', 'animCurveTU'])  
        for each in anim_curves:  
            mc.keyframe(each, edit=True, relative=True, timeChange=1000)
        mc.currentTime(1001)


    def createDefaultIK_cmd(self , *arhs):
        from rf_app.mocap.function import fbxDef
        reload(fbxDef)
        mc.currentTime(999)
        mc.select(cl=True)
        rootFbx = mc.ls('*:Hips','Hips')
        mc.select(rootFbx)
        fbxDef.create_def_in_scene()