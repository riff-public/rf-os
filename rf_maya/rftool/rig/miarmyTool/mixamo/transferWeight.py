import maya.cmds as mc
import maya.mel as mm
import pymel.core as pm
import os , sys
from rf_utils import file_utils
reload(file_utils)

def Run():
    srcGeo = mc.ls( sl = True)[0]
    trgGeo = mc.ls( sl = True)[1]


    path = "P:/Hanuman/rnd/rigDev/Mixamo/rigData"
    jointList = file_utils.ymlLoader("{}/replaceWeightJoint.yml".format(path)) 

    srcList = jointList['sourceJoint']
    trgList = jointList['targetJoint']
    srcJnt = []
    trgJnt = []
    sknJnt = []

    # Copy skinWeight
    srcSkn = mm.eval('findRelatedSkinCluster("{}")'.format(srcGeo))
    if srcSkn:
        jnts = mc.skinCluster(srcSkn , q=True , inf=True)
        trgSkn = mc.skinCluster( jnts , trgGeo , tsb = True )[0]
        mc.select( srcGeo , r=True )
        mc.select( trgGeo , add=True )
        mc.copySkinWeights(nm = True , sa = 'closestPoint' , ia = ('oneToOne','oneToOne')) 

    # Transfer skinWeight
    trgSkn = mm.eval('findRelatedSkinCluster("{}")'.format(trgGeo))
    if trgSkn:
        jnts = mc.skinCluster(trgSkn , q=True , inf=True)
        #List Source Joint
        for jnt in jnts:
            mc.setAttr('{}.lockInfluenceWeights '.format(jnt) , 1)
            srcJnt.append(jnt)
            #List Target Joint
            for j in range(len(srcList)):
                if jnt.endswith(srcList[j]):
                    jntName = 'ctrl_md:' + trgList[j]
                    trgJnt.append(jntName)
                    sknJnt.append(jntName)
                else: 
                    pass
            if ':' in jnt:
                jnt = jnt.split(':')[-1]
            print jnt
            if 'Neck1' in jnt or 'Throat' in jnt:
                jntName = 'ctrl_md:Neck_Jnt'
                print jnt , '-1-' , jntName
                trgJnt.append(jntName)
            elif not jnt in srcList:
                jntName = 'ctrl_md:Head_Jnt'
                print jnt , '-2-' , jntName
                trgJnt.append(jntName)

        mc.skinCluster(trgSkn , ai = sknJnt , lw=True , edit=True , wt=False)

        print len(srcJnt)
        print len(trgJnt)
        for i in range(len(srcJnt)):
            mc.setAttr('{}.lockInfluenceWeights '.format(srcJnt[i]) , 0)
            mc.setAttr('{}.lockInfluenceWeights '.format(trgJnt[i]) , 0)
            mc.skinPercent(trgSkn, '{}.vtx[:]'.format(trgGeo), transformValue=[('{}'.format(trgJnt[i]), 1)])
            mc.skinCluster(trgSkn , inf=srcJnt[i] , e=True , lw=True)
            mc.skinCluster(trgSkn , inf=trgJnt[i] , e=True , lw=True)
            mc.skinCluster(trgSkn , e=True , ri=srcJnt[i])
            print srcJnt[i] , '>>>>>' ,trgJnt[i]

    else:
        print "This object doesn't have skinCluster node."
        return