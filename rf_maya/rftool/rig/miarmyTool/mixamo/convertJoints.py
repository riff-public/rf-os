import maya.cmds as mc
import os,sys
from ncmel import core
reload(core)
from ncmel import rigTools as rt
reload(rt)
from rf_utils.context import context_info
reload(context_info)

def Run():
    jntMixamo = mc.ls('*:Hips' , 'Hips')[0]
    if ':' in jntMixamo:
        namespaceMaximo = '{}:'.format(jntMixamo.split(':')[0])
    else:
        namespaceMaximo = ''

    rootJnt = rt.createNode('joint' , 'Root_TmpJnt')
    rootJnt.snap('{}Hips'.format(namespaceMaximo))

    pelvisJnt = rt.createNode('joint' , 'Pelvis_TmpJnt')
    pelvisJnt.snap('{}Hips'.format(namespaceMaximo))

    spine1Jnt = rt.createNode('joint' , 'Spine1_TmpJnt')
    spine1Jnt.snap('{}Spine'.format(namespaceMaximo))

    spine3Jnt = rt.createNode('joint' , 'Spine3_TmpJnt')
    spine3Jnt.snap('{}Spine1'.format(namespaceMaximo))

    spine5Jnt = rt.createNode('joint' , 'Spine5_TmpJnt')
    spine5Jnt.snap('{}Spine2'.format(namespaceMaximo))

    spine2Jnt = rt.createNode('joint' , 'Spine2_TmpJnt')
    spine4Jnt = rt.createNode('joint' , 'Spine4_TmpJnt')
    mc.delete(mc.parentConstraint(spine1Jnt , spine3Jnt , spine2Jnt , mo=False))
    mc.delete(mc.parentConstraint(spine3Jnt , spine5Jnt , spine4Jnt , mo=False))

    neckJnt = rt.createNode('joint' , 'Neck_TmpJnt')
    neckJnt.snap('{}Neck'.format(namespaceMaximo))

    headJnt = rt.createNode('joint' , 'Head_TmpJnt')
    headJnt.snap('{}Head'.format(namespaceMaximo))

    headEndJnt = rt.createNode('joint' , 'HeadEnd_TmpJnt')
    headEndJnt.snap('{}HeadTop_End'.format(namespaceMaximo))

    #-- L Arm
    clavLJnt = rt.createNode('joint' , 'Clav_L_TmpJnt')
    clavLJnt.snapPoint('{}LeftShoulder'.format(namespaceMaximo))

    upArmLJnt = rt.createNode('joint' , 'UpArm_L_TmpJnt')
    upArmLJnt.snapPoint('{}LeftArm'.format(namespaceMaximo))

    forearmLJnt = rt.createNode('joint' , 'Forearm_L_TmpJnt')
    forearmLJnt.snapPoint('{}LeftForeArm'.format(namespaceMaximo))

    elbowLJnt = rt.createNode('joint' , 'Elbow_L_TmpJnt')
    elbowLJnt.snapPoint(forearmLJnt)
    txDef = elbowLJnt.attr('tz').getVal()
    elbowLJnt.attr('tz').v = txDef-2.5

    wristLJnt = rt.createNode('joint' , 'Wrist_L_TmpJnt')
    wristLJnt.snapPoint('{}LeftHand'.format(namespaceMaximo))

    handLJnt = rt.createNode('joint' , 'Hand_L_TmpJnt')
    handLJnt.snapPoint('{}LeftHand'.format(namespaceMaximo))
    txDef = handLJnt.attr('tx').getVal()
    handLJnt.attr('tx').v = txDef+0.1

    handCupLJnt = rt.createNode('joint' , 'HandCup_L_TmpJnt')
    handCupLJnt.snapPoint(handLJnt)
    txDef = handCupLJnt.attr('tz').getVal()
    handCupLJnt.attr('tz').v = txDef-0.1

    fingerList = ['Thumb' , 'Index' , 'Middle' , 'Ring' , 'Pinky']
    for eachFinger in fingerList:
        allFingers = []
        tmb = []
        for i in range(1,5):
            if mc.objExists('*{}{}'.format(eachFinger,i)) or mc.objExists('*:*{}{}'.format(eachFinger,i)):
                jnt = rt.createNode('joint' , '{}{}_L_TmpJnt'.format(eachFinger,i))
                jnt.snapPoint('{}LeftHand{}{}'.format(namespaceMaximo , eachFinger , i))
                jnt.attr('jointOrientY').v = -90
                jnt.attr('jointOrientZ').v = -90
                allFingers.append(jnt)

                if eachFinger =='Thumb':
                    tmb.append(jnt)

        if tmb:
            mc.delete(mc.aimConstraint(tmb[-2] , tmb[-1],aim=(0,-1,0),u=(-1,0,0)))

            for i in range(len(tmb)-1):
                mc.delete(mc.aimConstraint(tmb[i+1] , tmb[i] , aim=(0,1,0),u=(-1,0,0)))

        for j in range(len(allFingers)-1):
            mc.parent(allFingers[j+1] , allFingers[j])



    #-- L Leg
    upLegLJnt = rt.createNode('joint' , 'UpLeg_L_TmpJnt')
    upLegLJnt.snapPoint('{}LeftUpLeg'.format(namespaceMaximo))

    lowLegLJnt = rt.createNode('joint' , 'LowLeg_L_TmpJnt')
    lowLegLJnt.snapPoint('{}LeftLeg'.format(namespaceMaximo))

    kneeLJnt = rt.createNode('joint' , 'Knee_L_TmpJnt')
    kneeLJnt.snapPoint(lowLegLJnt)
    txDef = kneeLJnt.attr('tz').getVal()
    kneeLJnt.attr('tz').v = txDef+2.5

    ankleLJnt = rt.createNode('joint' , 'Ankle_L_TmpJnt')
    ankleLJnt.snapPoint('{}LeftFoot'.format(namespaceMaximo))

    ballLJnt = rt.createNode('joint' , 'Ball_L_TmpJnt')
    ballLJnt.snapPoint('{}LeftToeBase'.format(namespaceMaximo))

    toeLJnt = rt.createNode('joint' , 'Toe_L_TmpJnt')
    toeLJnt.snapPoint('{}LeftToe_End'.format(namespaceMaximo))

    footInLJnt = rt.createNode('joint' , 'FootIn_L_TmpJnt')
    footInLJnt.snapPoint('{}LeftToeBase'.format(namespaceMaximo))
    txDef = footInLJnt.attr('tx').getVal()
    footInLJnt.attr('tx').v = txDef-0.3

    footOutLJnt = rt.createNode('joint' , 'FootOut_L_TmpJnt')
    footOutLJnt.snapPoint('{}LeftToeBase'.format(namespaceMaximo))
    txDef = footOutLJnt.attr('tx').getVal()
    footOutLJnt.attr('tx').v = txDef+0.3

    heelLJnt = rt.createNode('joint' , 'Heel_L_TmpJnt')
    heelLJnt.snapPoint('{}LeftToeBase'.format(namespaceMaximo))
    txDef = heelLJnt.attr('tz').getVal()
    heelLJnt.attr('tz').v = txDef-1.25

    heelTwsLJnt = rt.createNode('joint' , 'HeelIkTwistPiv_L_TmpJnt')
    heelTwsLJnt.snapPoint('{}LeftToeBase'.format(namespaceMaximo))
    txDef = heelTwsLJnt.attr('tz').getVal()
    heelTwsLJnt.attr('tz').v = txDef-1

    #-- Rotate joint orient
    for each in (clavLJnt , upArmLJnt , forearmLJnt , elbowLJnt , wristLJnt , handLJnt , handCupLJnt):
        each.attr('jointOrientZ').v = -90

    for each in (upLegLJnt , lowLegLJnt , ankleLJnt):
        each.attr('jointOrientZ').v = 180

    for each in (footInLJnt , footOutLJnt , toeLJnt , ballLJnt , heelLJnt):
        each.attr('jointOrientX').v = 90
        each.attr('jointOrientZ').v = 180


    pelvisJnt.parent(rootJnt)
    footInLJnt.parent(ballLJnt)
    footOutLJnt.parent(ballLJnt)
    toeLJnt.parent(ballLJnt)
    heelLJnt.parent(ankleLJnt)
    heelTwsLJnt.parent(ankleLJnt)
    ballLJnt.parent(ankleLJnt)
    ankleLJnt.parent(lowLegLJnt)
    kneeLJnt.parent(lowLegLJnt)
    lowLegLJnt.parent(upLegLJnt)
    upLegLJnt.parent(pelvisJnt)
    spine1Jnt.parent(rootJnt)
    spine2Jnt.parent(spine1Jnt)
    spine3Jnt.parent(spine2Jnt)
    spine4Jnt.parent(spine3Jnt)
    spine5Jnt.parent(spine4Jnt)
    neckJnt.parent(spine5Jnt)
    headJnt.parent(neckJnt)
    headEndJnt.parent(headJnt)
    handCupLJnt.parent(handLJnt)
    handLJnt.parent(wristLJnt)
    wristLJnt.parent(forearmLJnt)
    elbowLJnt.parent(forearmLJnt)
    forearmLJnt.parent(upArmLJnt)
    upArmLJnt.parent(clavLJnt)
    clavLJnt.parent(spine5Jnt)

    if mc.objExists('Thumb1_L_TmpJnt'):
        mc.parent('Thumb1_L_TmpJnt',handLJnt)
    if mc.objExists('Index1_L_TmpJnt'):
        mc.parent('Index1_L_TmpJnt',handLJnt)
    if mc.objExists('Middle1_L_TmpJnt'):
        mc.parent('Middle1_L_TmpJnt',handLJnt)

    if mc.objExists('Ring1_L_TmpJnt'):
        mc.parent('Ring1_L_TmpJnt',handCupLJnt)
    if mc.objExists('Pinky1_L_TmpJnt'):
        mc.parent('Pinky1_L_TmpJnt',handCupLJnt)

    mc.mirrorJoint(clavLJnt,mb=True , sr=('_L_','_R_'))
    mc.mirrorJoint(upLegLJnt,mb=True , sr=('_L_','_R_'))

    allJnt = mc.ls(type='joint')
    for each in allJnt:
        mc.setAttr('{}.radius'.format(each) , 0.1)

    expGrp = rt.createNode('transform' , 'Export_Grp')
    rootJnt.parent(expGrp)

    mc.select(cl=True)

    print '---- Create Joints DONE ----'