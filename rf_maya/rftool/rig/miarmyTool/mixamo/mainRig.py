import maya.cmds as mc

from ncmel import core
reload(core)
from ncmel import rigTools
reload(rigTools)
from ncmel import mainRig
reload(mainRig)
from ncmel import rootRig
reload(rootRig)
from ncmel import pelvisRig
reload(pelvisRig)
from ncmel import spineRig
reload(spineRig)
from ncmel import torsoRig
reload(torsoRig)
from ncmel import neckRig
reload(neckRig)
from ncmel import headRig
reload(headRig)
from ncmel import jawRig
reload(jawRig)
from ncmel import clavicleRig
reload(clavicleRig)
from ncmel import armRig
reload(armRig)
from ncmel import legRig
reload(legRig)
from ncmel import fingerRig
reload(fingerRig)
from ncmel import subRig
reload(subRig)
from ncmel import fkRig
reload(fkRig)
from utaTools.utapy import charTextName
reload(charTextName)
from utils.crvScript import ctrlData
reload(ctrlData)

def main( size = 0.75 ):
    
    print "# Generate >> Main Group"
    main = mainRig.Run( 
                                    assetName = '' ,
                                    size      = size
                       )
    print "##------------------------------------------------------------------------------------"
    print "##-----------------------------MAIN GROUP---------------------------------------------"
    print "##------------------------------------------------------------------------------------"


    print "# Generate >> Root"
    root = rootRig.Run( 
                                    rootTmpJnt = 'Root_TmpJnt' ,
                                    ctrlGrp    =  main.mainCtrlGrp ,
                                    skinGrp    =  main.skinGrp ,
                                    size       =  size
                       )

    print "# Generate >> Torso"
    torso = torsoRig.Run(   
                                    pelvisTmpJnt = 'Pelvis_TmpJnt' ,
                                    spine1TmpJnt = 'Spine1_TmpJnt' ,
                                    spine2TmpJnt = 'Spine2_TmpJnt' ,
                                    spine3TmpJnt = 'Spine3_TmpJnt' ,
                                    spine4TmpJnt = 'Spine4_TmpJnt' ,
                                    spine5TmpJnt = 'Spine5_TmpJnt' ,
                                    parent       =  root.rootJnt ,
                                    ctrlGrp      =  main.mainCtrlGrp ,
                                    skinGrp      =  main.skinGrp ,
                                    jntGrp       =  main.jntGrp ,
                                    stillGrp     =  main.stillGrp ,
                                    elem         = '' ,
                                    axis         = 'y' ,
                                    size         =  size
                         )
    ## SetAttr SpineIk 
    mc.setAttr('%s.fkIk' % 'Spine_Ctrl', 1)

    print "# Generate >> Neck"
    neck = neckRig.Run(    
                                    neckTmpJnt = 'Neck_TmpJnt' ,
                                    headTmpJnt = 'Head_TmpJnt' ,
                                    parent     =  torso.jntDict[-1] ,
                                    ctrlGrp    =  main.mainCtrlGrp ,
                                    skinGrp    =  main.skinGrp ,
                                    elem       = '' ,
                                    side       = '' ,
                                    ribbon     = False ,
                                    head       = True ,
                                    axis       = 'y' ,
                                    size       = size
                       )
                     
    print "# Generate >> Head"
    head = headRig.Run(    
                                    headTmpJnt      = 'Head_TmpJnt' ,
                                    headEndTmpJnt   = 'HeadEnd_TmpJnt' ,
                                    eyeTrgTmpJnt    = 'EyeTrgt_TmpJnt' ,
                                    eyeLftTrgTmpJnt = 'EyeTrgt_L_TmpJnt' ,
                                    eyeRgtTrgTmpJnt = 'EyeTrgt_R_TmpJnt' ,
                                    eyeLftTmpJnt    = 'Eye_L_TmpJnt' ,
                                    eyeRgtTmpJnt    = 'Eye_R_TmpJnt' ,
                                    jawUpr1TmpJnt   = 'JawUpr1_TmpJnt' ,
                                    jawUprEndTmpJnt = 'JawUprEnd_TmpJnt' ,
                                    jawLwr1TmpJnt   = 'JawLwr1_TmpJnt' ,
                                    jawLwr2TmpJnt   = 'JawLwr2_TmpJnt' ,
                                    jawLwrEndTmpJnt = 'JawLwrEnd_TmpJnt' ,
                                    parent          =  neck.neckEndJnt ,
                                    ctrlGrp         =  main.mainCtrlGrp ,
                                    skinGrp         =  main.skinGrp ,
                                    elem            = '' ,
                                    side            = '' ,
                                    eyeRig          = False ,
                                    jawRig          = False ,
                                    size            = size 
                       )
                     
    # print "# Generate >> Eye"
    # eye = eyeRig.Run(    
    #                               eyeTrgTmpJnt    = 'EyeTrgt_TmpJnt' ,
    #                               eyeLftTrgTmpJnt = 'EyeTrgt_L_TmpJnt' ,
    #                               eyeRgtTrgTmpJnt = 'EyeTrgt_R_TmpJnt' ,
    #                               eyeLftTmpJnt    = 'Eye_L_TmpJnt' ,
    #                               eyeRgtTmpJnt    = 'Eye_R_TmpJnt' ,
    #                               parent          =  head.headJnt ,
    #                               ctrlGrp         =  main.mainCtrlGrp ,
    #                               skinGrp         =  main.skinGrp ,
    #                               elem            = '' ,
    #                               side            = '' ,
    #                               size            = size 
    #                  )

    # print "# Generate >> Jaw"
    # jawObj = jawRig.Run(    
    #                               jawUpr1TmpJnt   = 'JawUpr1_TmpJnt' ,
    #                               jawUprEndTmpJnt = 'JawUprEnd_TmpJnt' ,
    #                               jawLwr1TmpJnt   = 'JawLwr1_TmpJnt' ,
    #                               jawLwr2TmpJnt   = 'JawLwr2_TmpJnt' ,
    #                               jawLwrEndTmpJnt = 'JawLwrEnd_TmpJnt' ,
    #                               parent          =  head.headJnt ,
    #                               ctrlGrp         =  main.mainCtrlGrp ,
    #                               skinGrp         =  main.skinGrp ,
    #                               elem            = '' ,
    #                               side            = '' ,
    #                               size            = size 
    #                  )

    print "# Generate >> Clavicle Left"
    clavL = clavicleRig.Run(   
                                    clavTmpJnt  = 'Clav_L_TmpJnt' ,
                                    upArmTmpJnt = 'UpArm_L_TmpJnt' ,
                                    parent      =  torso.jntDict[-1] ,
                                    ctrlGrp     =  main.mainCtrlGrp ,
                                    jntGrp      =  main.jntGrp ,
                                    skinGrp     =  main.skinGrp ,
                                    elem        = '' ,
                                    side        = 'L' ,
                                    axis        = 'y' ,
                                    size        = size
                            )
    
    print "# Generate >> Clavicle Right"
    clavR = clavicleRig.Run(   
                                    clavTmpJnt  = 'Clav_R_TmpJnt' ,
                                    upArmTmpJnt = 'UpArm_R_TmpJnt' ,
                                    parent      =  torso.jntDict[-1] ,
                                    ctrlGrp     =  main.mainCtrlGrp ,
                                    jntGrp      =  main.jntGrp ,
                                    skinGrp     =  main.skinGrp ,
                                    elem        = '' ,
                                    side        = 'R' ,
                                    axis        = 'y' ,
                                    size        = size
                            )
     
    print "# Generate >> Arm Left"
    armL = armRig.Run(              upArmTmpJnt   = 'UpArm_L_TmpJnt' ,
                                    forearmTmpJnt = 'Forearm_L_TmpJnt' ,
                                    wristTmpJnt   = 'Wrist_L_TmpJnt' ,
                                    handTmpJnt    = 'Hand_L_TmpJnt' ,
                                    elbowTmpJnt   = 'Elbow_L_TmpJnt' ,
                                    parent        = 'ClavEnd_L_Jnt' , 
                                    ctrlGrp       =  main.mainCtrlGrp ,
                                    jntGrp        =  main.jntGrp ,
                                    skinGrp       =  main.skinGrp ,
                                    stillGrp      =  main.stillGrp ,
                                    ikhGrp        =  main.ikhGrp ,
                                    elem          = '' ,
                                    side          = 'L' ,
                                    ribbon        = False ,
                                    size          = size
                    )

    rigTools.addMoreSpace( obj = armL.wristIkLocWor ,
                           spaces = [ ('Shoulder' , armL.wristIkLocWor['localSpace'] ) ,
                                      ('Head', head.headJnt ) ,
                                      ('Chest', torso.jntDict[-1] ) ,
                                      ('Pelvis', torso.pelvisJnt ) ] )
    
    print "# Generate >> Arm Right"
    armR = armRig.Run(              
                                    upArmTmpJnt   = 'UpArm_R_TmpJnt' ,
                                    forearmTmpJnt = 'Forearm_R_TmpJnt' ,
                                    wristTmpJnt   = 'Wrist_R_TmpJnt' ,
                                    handTmpJnt    = 'Hand_R_TmpJnt' ,
                                    elbowTmpJnt   = 'Elbow_R_TmpJnt' ,
                                    parent        = 'ClavEnd_R_Jnt' , 
                                    ctrlGrp       =  main.mainCtrlGrp ,
                                    jntGrp        =  main.jntGrp ,
                                    skinGrp       =  main.skinGrp ,
                                    stillGrp      =  main.stillGrp ,
                                    ikhGrp        =  main.ikhGrp ,
                                    elem          = '' ,
                                    side          = 'R' ,
                                    ribbon        = False ,
                                    size          = size
                    )

    rigTools.addMoreSpace( obj = armR.wristIkLocWor ,
                           spaces = [ ('Shoulder' , armR.wristIkLocWor['localSpace'] ) ,
                                      ('Head', head.headJnt ) ,
                                      ('Chest', torso.jntDict[-1] ) ,
                                      ('Pelvis', torso.pelvisJnt ) ] )
    
    print "# Generate >> Leg Left"
    legL = legRig.Run(              
                                    upLegTmpJnt   = 'UpLeg_L_TmpJnt' ,
                                    lowLegTmpJnt  = 'LowLeg_L_TmpJnt' ,
                                    ankleTmpJnt   = 'Ankle_L_TmpJnt' ,
                                    ballTmpJnt    = 'Ball_L_TmpJnt' ,
                                    toeTmpJnt     = 'Toe_L_TmpJnt' ,
                                    heelTmpJnt    = 'Heel_L_TmpJnt' ,
                                    footInTmpJnt  = 'FootIn_L_TmpJnt' ,
                                    footOutTmpJnt = 'FootOut_L_TmpJnt' ,
                                    kneeTmpJnt    = 'Knee_L_TmpJnt' ,
                                    heelIkTwistPivTmpJnt = 'HeelIkTwistPiv_L_TmpJnt',
                                    parent        =  torso.pelvisJnt , 
                                    ctrlGrp       =  main.mainCtrlGrp ,
                                    jntGrp        =  main.jntGrp ,
                                    skinGrp       =  main.skinGrp ,
                                    stillGrp      =  main.stillGrp ,
                                    ikhGrp        =  main.ikhGrp ,
                                    elem          = '' ,
                                    side          = 'L' ,
                                    ribbon        = False ,
                                    foot          = True ,
                                    size          = size
                    )
    
    rigTools.addMoreSpace( obj = legL.kneeIkLocWor ,
                           spaces = [ ('Ankle' , legL.kneeIkLocWor['localSpace'] ) ,
                                      ('Root', root.rootJnt )] )


    print "# Generate >> Leg Right"
    legR = legRig.Run(              
                                    upLegTmpJnt   = 'UpLeg_R_TmpJnt' ,
                                    lowLegTmpJnt  = 'LowLeg_R_TmpJnt' ,
                                    ankleTmpJnt   = 'Ankle_R_TmpJnt' ,
                                    ballTmpJnt    = 'Ball_R_TmpJnt' ,
                                    toeTmpJnt     = 'Toe_R_TmpJnt' ,
                                    heelTmpJnt    = 'Heel_R_TmpJnt' ,
                                    footInTmpJnt  = 'FootIn_R_TmpJnt' ,
                                    footOutTmpJnt = 'FootOut_R_TmpJnt' ,
                                    kneeTmpJnt    = 'Knee_R_TmpJnt' ,
                                    heelIkTwistPivTmpJnt = 'HeelIkTwistPiv_R_TmpJnt',
                                    parent        =  torso.pelvisJnt , 
                                    ctrlGrp       =  main.mainCtrlGrp ,
                                    jntGrp        =  main.jntGrp ,
                                    skinGrp       =  main.skinGrp ,
                                    stillGrp      =  main.stillGrp ,
                                    ikhGrp        =  main.ikhGrp ,
                                    elem          = '' ,
                                    side          = 'R' ,
                                    ribbon        = False ,
                                    foot          = True ,
                                    size          = size
                    )
    rigTools.addMoreSpace( obj = legR.kneeIkLocWor ,
                           spaces = [ ('Ankle' , legR.kneeIkLocWor['localSpace'] ) ,
                                      ('Root', root.rootJnt )] )


    # print "# Generate >> Eyedot Left Rig"
    # eyeDotRigObj_L = subRig.Run(
    #                               name       = 'EyeDot' ,
    #                               tmpJnt     = 'EyeDot_L_TmpJnt' ,
    #                               parent     =  eye.eyeLftJnt ,
    #                               ctrlGrp    =  main.mainCtrlGrp ,
    #                               shape      = 'circle' ,
    #                               side       = 'L' ,
    #                               size       = size  
    #                           )

    # print "# Generate >> Eyedot Right Rig"
    # eyeDotRigObj_R = subRig.Run(
    #                               name       = 'EyeDot' ,
    #                               tmpJnt     = 'EyeDot_R_TmpJnt' ,
    #                               parent     =  eye.eyeRgtJnt ,
    #                               ctrlGrp    =  main.mainCtrlGrp ,
    #                               shape      = 'circle' ,
    #                               side       = 'R' ,
    #                               size       = size  
    #                           )  

    # print "# Generate >> Iris Left Rig"
    # IrisRigObj_L = subRig.Run(
    #                               name       = 'Iris' ,
    #                               tmpJnt     = 'Iris_L_TmpJnt' ,
    #                               parent     = eye.eyeLftJnt,
    #                               ctrlGrp    = main.mainCtrlGrp ,
    #                               shape      = 'circle' ,
    #                               side       = 'L' ,
    #                               size       = size  
    #                           )

    # print "# Generate >> Iris Left Rig"
    # IrisRigObj_R = subRig.Run(
    #                               name       = 'Iris' ,
    #                               tmpJnt     = 'Iris_R_TmpJnt' ,
    #                               parent     = eye.eyeRgtJnt ,
    #                               ctrlGrp    = main.mainCtrlGrp ,
    #                               shape      = 'circle' ,
    #                               side       = 'R' ,
    #                               size       = size  
    #                           )

    # print "# Generate >> Pupil Left Rig"
    # PupilRigObj_L = subRig.Run(
    #                               name       = 'Pupil' ,
    #                               tmpJnt     = 'Pupil_L_TmpJnt' ,
    #                               parent     = 'IrisSub_L_Jnt'  ,
    #                               ctrlGrp    =  main.mainCtrlGrp ,
    #                               shape      = 'circle' ,
    #                               side       = 'L' ,
    #                               size       = size  
    #                           )

    # print "# Generate >> Pupil Right Rig"
    # PupilRigObj_R = subRig.Run(
    #                               name       = 'Pupil' ,
    #                               tmpJnt     = 'Pupil_R_TmpJnt' ,
    #                               parent     = 'IrisSub_R_Jnt' ,
    #                               ctrlGrp    =  main.mainCtrlGrp ,
    #                               shape      = 'circle' ,
    #                               side       = 'R' ,
    #                               size       = size  
    #                           )

    # print "# Generate >> Hand Cup Inner Left"
    # handCupInnerL = fingerRig.HandCup( name       = 'HandCupInner' ,
    #                               handCupTmpJnt = 'HandCupInner_L_TmpJnt' ,
    #                               armCtrl       = armL.armCtrl ,
    #                               parent        = armL.handJnt ,
    #                               elem          = '' ,
    #                               side          = 'L' ,
    #                           )

    if mc.objExists('Thumb1_L_TmpJnt'):
        print "# Generate >> Thumb Left"
        fngrThumbL = fingerRig.Run(         
                                        fngr          = 'Thumb' ,
                                        fngrTmpJnt    =['Thumb1_L_TmpJnt' , 
                                                        'Thumb2_L_TmpJnt' , 
                                                        'Thumb3_L_TmpJnt'  , 
                                                        'Thumb4_L_TmpJnt' ] ,
                                        parent        =  armL.handJnt , 
                                        armCtrl       =  armL.armCtrl ,
                                        ctrlGrp       =  main.mainCtrlGrp ,
                                        elem          = '' ,
                                        side          = 'L' ,
                                        size          = size 
                                 )

        finger = 'Thumb'
        ctrlShape = core.Dag(armL.armCtrl.shape)
        ctrlShape.attr('%s1FistRx' %finger).value = -1
        ctrlShape.attr('%s2FistRx' %finger).value = -9
        ctrlShape.attr('%s3FistRx' %finger).value = -9
        ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
        ctrlShape.attr('%s3SlideRx' %finger).value = -8
        ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
        ctrlShape.attr('%s3ScruchRx' %finger).value = -8

    if mc.objExists('Index1_L_TmpJnt'):
        print "# Generate >> Index Left"
        fngrIndexL = fingerRig.Run(         
                                        fngr          = 'Index' ,
                                        fngrTmpJnt    =['Index1_L_TmpJnt' , 
                                                        'Index2_L_TmpJnt' , 
                                                        'Index3_L_TmpJnt' , 
                                                        'Index4_L_TmpJnt' ] ,
                                        parent        =  armL.handJnt , 
                                        armCtrl       =  armL.armCtrl ,
                                        ctrlGrp       =  main.mainCtrlGrp ,
                                        elem          = '' ,
                                        side          = 'L' ,
                                        size          = size 
                                 )

        finger = 'Index'
        ctrlShape = core.Dag(armL.armCtrl.shape)
        ctrlShape.attr('%s1FistRx' %finger).value = -8
        ctrlShape.attr('%s2FistRx' %finger).value = -9
        ctrlShape.attr('%s3FistRx' %finger).value = -9
        # ctrlShape.attr('%s4FistRx' %finger).value = -9
        ctrlShape.attr('%s2RelaxRx' %finger).value = -1
        ctrlShape.attr('%s3RelaxRx' %finger).value = -1
        # ctrlShape.attr('%s4RelaxRx' %finger).value = -1
        ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
        ctrlShape.attr('%s3SlideRx' %finger).value = -8
        # ctrlShape.attr('%s4SlideRx' %finger).value = 4.5
        ctrlShape.attr('%s1ScruchRx' %finger).value = 1.5
        ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
        ctrlShape.attr('%s3ScruchRx' %finger).value = -8
        # ctrlShape.attr('%s4ScruchRx' %finger).value = -8
        ctrlShape.attr('%sBaseSpread' %finger).value = -4
        ctrlShape.attr('%sSpread' %finger).value = -4
        ctrlShape.attr('%sBaseBreak' %finger).value = -4.5
        ctrlShape.attr('%sBreak' %finger).value = -4.5
        ctrlShape.attr('%sBaseFlex' %finger).value = -9
        ctrlShape.attr('%sFlex' %finger).value = -9

    if mc.objExists('Middle1_L_TmpJnt'):
        print "# Generate >> Middle Left"
        fngrMiddleL = fingerRig.Run(         
                                        fngr          = 'Middle' ,
                                        fngrTmpJnt    =['Middle1_L_TmpJnt' , 
                                                        'Middle2_L_TmpJnt' , 
                                                        'Middle3_L_TmpJnt' , 
                                                        'Middle4_L_TmpJnt'  ] ,
                                        parent        =  armL.handJnt , 
                                        armCtrl       =  armL.armCtrl ,
                                        ctrlGrp       =  main.mainCtrlGrp ,
                                        elem          = '' ,
                                        side          = 'L' ,
                                        size          = size 
                                   )

        finger = 'Middle'
        ctrlShape = core.Dag(armL.armCtrl.shape)
        ctrlShape.attr('%s1FistRx' %finger).value = -8
        ctrlShape.attr('%s2FistRx' %finger).value = -9
        ctrlShape.attr('%s3FistRx' %finger).value = -9
        # ctrlShape.attr('%s4FistRx' %finger).value = -9
        ctrlShape.attr('%s1RelaxRx' %finger).value = -0.5
        ctrlShape.attr('%s2RelaxRx' %finger).value = -2.5
        ctrlShape.attr('%s3RelaxRx' %finger).value = -2.5
        # ctrlShape.attr('%s4RelaxRx' %finger).value = -2.5
        ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
        ctrlShape.attr('%s3SlideRx' %finger).value = -8
        # ctrlShape.attr('%s4SlideRx' %finger).value = 4.5
        ctrlShape.attr('%s1ScruchRx' %finger).value = 1.5
        ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
        ctrlShape.attr('%s3ScruchRx' %finger).value = -8
        # ctrlShape.attr('%s4ScruchRx' %finger).value = -8
        ctrlShape.attr('%sBaseBreak' %finger).value = -4.5
        ctrlShape.attr('%sBreak' %finger).value = -4.5
        ctrlShape.attr('%sBaseFlex' %finger).value = -9
        ctrlShape.attr('%sFlex' %finger).value = -9


    print "# Generate >> Hand Cup Left"
    handCupL = fingerRig.HandCup( 
                                     handCupTmpJnt = 'HandCup_L_TmpJnt' ,
                                     armCtrl       =  armL.armCtrl ,
                                     parent        =  armL.handJnt ,
                                     elem          = '' ,
                                     side          = 'L' ,
                                     size          = size 
                                )

    if mc.objExists('Ring1_L_TmpJnt'):
        print "# Generate >> Ring Left"
        fngrRingL = fingerRig.Run(         
                                        fngr          = 'Ring' ,
                                        fngrTmpJnt    =['Ring1_L_TmpJnt' , 
                                                        'Ring2_L_TmpJnt' , 
                                                        'Ring3_L_TmpJnt' , 
                                                        'Ring4_L_TmpJnt'  ] ,
                                        parent        =  handCupL.handCupJnt , 
                                        armCtrl       =  armL.armCtrl ,
                                        ctrlGrp       =  main.mainCtrlGrp ,
                                        elem          = '' ,
                                        side          = 'L' ,
                                        size          = size 
                                   )

        finger = 'Ring'
        ctrlShape = core.Dag(armL.armCtrl.shape)
        ctrlShape.attr('%s1FistRx' %finger).value = -8
        ctrlShape.attr('%s2FistRx' %finger).value = -9
        ctrlShape.attr('%s3FistRx' %finger).value = -9
        # ctrlShape.attr('%s4FistRx' %finger).value = -9
        ctrlShape.attr('%s1RelaxRx' %finger).value = -1
        ctrlShape.attr('%s2RelaxRx' %finger).value = -5
        ctrlShape.attr('%s3RelaxRx' %finger).value = -5
        # ctrlShape.attr('%s4RelaxRx' %finger).value = -5
        ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
        ctrlShape.attr('%s3SlideRx' %finger).value = -8
        # ctrlShape.attr('%s4SlideRx' %finger).value = 4.5
        ctrlShape.attr('%s1ScruchRx' %finger).value = 1.5
        ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
        ctrlShape.attr('%s3ScruchRx' %finger).value = -8
        # ctrlShape.attr('%s4ScruchRx' %finger).value = -8
        ctrlShape.attr('%sBaseSpread' %finger).value = 3
        ctrlShape.attr('%sSpread' %finger).value = 3
        ctrlShape.attr('%sBaseBreak' %finger).value = -4.5
        ctrlShape.attr('%sBreak' %finger).value = -4.5
        ctrlShape.attr('%sBaseFlex' %finger).value = -9
        ctrlShape.attr('%sFlex' %finger).value = -9

    if mc.objExists('Pinky1_L_TmpJnt'):
        print "# Generate >> Pinky Left"
        fngrPinkyL = fingerRig.Run(         
                                        fngr          = 'Pinky' ,
                                        fngrTmpJnt    =['Pinky1_L_TmpJnt' , 
                                                        'Pinky2_L_TmpJnt' , 
                                                        'Pinky3_L_TmpJnt' , 
                                                        'Pinky4_L_TmpJnt'  ] ,
                                        parent        =  handCupL.handCupJnt , 
                                        armCtrl       =  armL.armCtrl ,
                                        ctrlGrp       =  main.mainCtrlGrp ,
                                        elem          = '' ,
                                        side          = 'L' ,
                                        size          = size 
                                   )

        finger = 'Pinky'
        ctrlShape = core.Dag(armL.armCtrl.shape)
        ctrlShape.attr('%s1FistRx' %finger).value = -8
        ctrlShape.attr('%s2FistRx' %finger).value = -9
        ctrlShape.attr('%s3FistRx' %finger).value = -9
        # ctrlShape.attr('%s4FistRx' %finger).value = -9
        ctrlShape.attr('%s1RelaxRx' %finger).value = -2
        ctrlShape.attr('%s2RelaxRx' %finger).value = -3
        ctrlShape.attr('%s3RelaxRx' %finger).value = -3
        # ctrlShape.attr('%s4RelaxRx' %finger).value = -3
        ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
        ctrlShape.attr('%s3SlideRx' %finger).value = -8
        # ctrlShape.attr('%s4SlideRx' %finger).value = 4.5
        ctrlShape.attr('%s1ScruchRx' %finger).value = 1.5
        ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
        ctrlShape.attr('%s3ScruchRx' %finger).value = -8
        # ctrlShape.attr('%s4ScruchRx' %finger).value = -8
        ctrlShape.attr('%sBaseSpread' %finger).value = 5
        ctrlShape.attr('%sSpread' %finger).value = 5
        ctrlShape.attr('%sBaseBreak' %finger).value = -4.5
        ctrlShape.attr('%sBreak' %finger).value = -4.5
        ctrlShape.attr('%sBaseFlex' %finger).value = -9
        ctrlShape.attr('%sFlex' %finger).value = -9

    # print "# Generate >> Hand Cup Inner Right"
    # handCupInnerR = fingerRig.HandCup( name       = 'HandCupInner' ,
    #                               handCupTmpJnt = 'HandCupInner_R_TmpJnt' ,
    #                               armCtrl       = armR.armCtrl ,
    #                               parent        = armR.handJnt ,
    #                               elem          = '' ,
    #                               side          = 'R' ,
    #                           )

    if mc.objExists('Thumb1_R_TmpJnt'):
        print "# Generate >> Thumb Right"
        fngrThumbR = fingerRig.Run(         
                                        fngr          = 'Thumb' ,
                                        fngrTmpJnt    =['Thumb1_R_TmpJnt' , 
                                                        'Thumb2_R_TmpJnt' , 
                                                        'Thumb3_R_TmpJnt' , 
                                                        'Thumb4_R_TmpJnt' ] ,
                                        parent        =  armR.handJnt , 
                                        armCtrl       =  armR.armCtrl ,
                                        ctrlGrp       =  main.mainCtrlGrp ,
                                        elem          = '' ,
                                        side          = 'R' ,
                                        size          = size 
                                 )

        finger = 'Thumb'
        ctrlShape = core.Dag(armR.armCtrl.shape)
        ctrlShape.attr('%s1FistRx' %finger).value = -1
        ctrlShape.attr('%s2FistRx' %finger).value = -9
        ctrlShape.attr('%s3FistRx' %finger).value = -9
        ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
        ctrlShape.attr('%s3SlideRx' %finger).value = -8
        ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
        ctrlShape.attr('%s3ScruchRx' %finger).value = -8
    
    if mc.objExists('Index1_R_TmpJnt'):
        print "# Generate >> Index Right"
        fngrIndexL = fingerRig.Run(         
                                        fngr          = 'Index' ,
                                        fngrTmpJnt    =['Index1_R_TmpJnt' , 
                                                        'Index2_R_TmpJnt' , 
                                                        'Index3_R_TmpJnt' , 
                                                        'Index4_R_TmpJnt' ] ,
                                        parent        =  armR.handJnt , 
                                        armCtrl       =  armR.armCtrl ,
                                        ctrlGrp       =  main.mainCtrlGrp ,
                                        elem          = '' ,
                                        side          = 'R' ,
                                        size          = size 
                                 )

        finger = 'Index'
        ctrlShape = core.Dag(armR.armCtrl.shape)
        ctrlShape.attr('%s1FistRx' %finger).value = -8
        ctrlShape.attr('%s2FistRx' %finger).value = -9
        ctrlShape.attr('%s3FistRx' %finger).value = -9
        # ctrlShape.attr('%s4FistRx' %finger).value = -9
        ctrlShape.attr('%s2RelaxRx' %finger).value = -1
        ctrlShape.attr('%s3RelaxRx' %finger).value = -1
        # ctrlShape.attr('%s4RelaxRx' %finger).value = -1
        ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
        ctrlShape.attr('%s3SlideRx' %finger).value = -8
        # ctrlShape.attr('%s4SlideRx' %finger).value = 4.5
        ctrlShape.attr('%s1ScruchRx' %finger).value = 1.5
        ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
        ctrlShape.attr('%s3ScruchRx' %finger).value = -8
        # ctrlShape.attr('%s4ScruchRx' %finger).value = -8
        ctrlShape.attr('%sBaseSpread' %finger).value = -4
        ctrlShape.attr('%sSpread' %finger).value = -4
        ctrlShape.attr('%sBaseBreak' %finger).value = -4.5
        ctrlShape.attr('%sBreak' %finger).value = -4.5
        ctrlShape.attr('%sBaseFlex' %finger).value = -9
        ctrlShape.attr('%sFlex' %finger).value = -9
    
    if mc.objExists('Middle1_R_TmpJnt'):
        print "# Generate >> Middle Right"
        fngrMiddleL = fingerRig.Run(         
                                        fngr          = 'Middle' ,
                                        fngrTmpJnt    =['Middle1_R_TmpJnt' , 
                                                        'Middle2_R_TmpJnt' , 
                                                        'Middle3_R_TmpJnt' , 
                                                        'Middle4_R_TmpJnt' ] ,
                                        parent        =  armR.handJnt , 
                                        armCtrl       =  armR.armCtrl ,
                                        ctrlGrp       =  main.mainCtrlGrp ,
                                        elem          = '' ,
                                        side          = 'R' ,
                                        size          = size 
                                   )

        finger = 'Middle'
        ctrlShape = core.Dag(armR.armCtrl.shape)
        ctrlShape.attr('%s1FistRx' %finger).value = -8
        ctrlShape.attr('%s2FistRx' %finger).value = -9
        ctrlShape.attr('%s3FistRx' %finger).value = -9
        # ctrlShape.attr('%s4FistRx' %finger).value = -9
        ctrlShape.attr('%s1RelaxRx' %finger).value = -0.5
        ctrlShape.attr('%s2RelaxRx' %finger).value = -2.5
        ctrlShape.attr('%s3RelaxRx' %finger).value = -2.5
        # ctrlShape.attr('%s4RelaxRx' %finger).value = -2.5
        ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
        ctrlShape.attr('%s3SlideRx' %finger).value = -8
        # ctrlShape.attr('%s4SlideRx' %finger).value = 4.5
        ctrlShape.attr('%s1ScruchRx' %finger).value = 1.5
        ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
        ctrlShape.attr('%s3ScruchRx' %finger).value = -8
        # ctrlShape.attr('%s4ScruchRx' %finger).value = -8
        ctrlShape.attr('%sBaseBreak' %finger).value = -4.5
        ctrlShape.attr('%sBreak' %finger).value = -4.5
        ctrlShape.attr('%sBaseFlex' %finger).value = -9
        ctrlShape.attr('%sFlex' %finger).value = -9

    print "# Generate >> Hand Cup Right"
    handCupR = fingerRig.HandCup( 
                                     handCupTmpJnt = 'HandCup_R_TmpJnt' ,
                                     armCtrl       =  armR.armCtrl ,
                                     parent        =  armR.handJnt ,
                                     elem          = '' ,
                                     side          = 'R' ,
                                     size          = size 
                                )

    if mc.objExists('Ring1_R_TmpJnt'):
        print "# Generate >> Ring Right"
        fngrRingL = fingerRig.Run(         
                                        fngr          = 'Ring' ,
                                        fngrTmpJnt    =['Ring1_R_TmpJnt' , 
                                                        'Ring2_R_TmpJnt' , 
                                                        'Ring3_R_TmpJnt' , 
                                                        'Ring4_R_TmpJnt' ] ,
                                        parent        =  handCupR.handCupJnt , 
                                        armCtrl       =  armR.armCtrl ,
                                        ctrlGrp       =  main.mainCtrlGrp ,
                                        elem          = '' ,
                                        side          = 'R' ,
                                        size          = size 
                                   )

        finger = 'Ring'
        ctrlShape = core.Dag(armR.armCtrl.shape)
        ctrlShape.attr('%s1FistRx' %finger).value = -8
        ctrlShape.attr('%s2FistRx' %finger).value = -9
        ctrlShape.attr('%s3FistRx' %finger).value = -9
        # ctrlShape.attr('%s4FistRx' %finger).value = -9
        ctrlShape.attr('%s1RelaxRx' %finger).value = -1
        ctrlShape.attr('%s2RelaxRx' %finger).value = -5
        ctrlShape.attr('%s3RelaxRx' %finger).value = -5
        # ctrlShape.attr('%s4RelaxRx' %finger).value = -5
        ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
        ctrlShape.attr('%s3SlideRx' %finger).value = -8
        # ctrlShape.attr('%s4SlideRx' %finger).value = 4.5
        ctrlShape.attr('%s1ScruchRx' %finger).value = 1.5
        ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
        ctrlShape.attr('%s3ScruchRx' %finger).value = -8
        # ctrlShape.attr('%s4ScruchRx' %finger).value = -8
        ctrlShape.attr('%sBaseSpread' %finger).value = 3
        ctrlShape.attr('%sSpread' %finger).value = 3
        ctrlShape.attr('%sBaseBreak' %finger).value = -4.5
        ctrlShape.attr('%sBreak' %finger).value = -4.5
        ctrlShape.attr('%sBaseFlex' %finger).value = -9
        ctrlShape.attr('%sFlex' %finger).value = -9
        
    if mc.objExists('Pinky1_R_TmpJnt'):
        print "# Generate >> Pinky Right"
        fngrPinkyL = fingerRig.Run(         
                                        fngr          = 'Pinky' ,
                                        fngrTmpJnt    =['Pinky1_R_TmpJnt' , 
                                                        'Pinky2_R_TmpJnt' , 
                                                        'Pinky3_R_TmpJnt' , 
                                                        'Pinky4_R_TmpJnt' ] ,
                                        parent        =  handCupR.handCupJnt , 
                                        armCtrl       =  armR.armCtrl ,
                                        ctrlGrp       =  main.mainCtrlGrp ,
                                        elem          = '' ,
                                        side          = 'R' ,
                                        size          = size 
                                   )
        
        finger = 'Pinky'
        ctrlShape = core.Dag(armR.armCtrl.shape)
        ctrlShape.attr('%s1FistRx' %finger).value = -8
        ctrlShape.attr('%s2FistRx' %finger).value = -9
        ctrlShape.attr('%s3FistRx' %finger).value = -9
        # ctrlShape.attr('%s4FistRx' %finger).value = -9
        ctrlShape.attr('%s1RelaxRx' %finger).value = -2
        ctrlShape.attr('%s2RelaxRx' %finger).value = -3
        ctrlShape.attr('%s3RelaxRx' %finger).value = -3
        # ctrlShape.attr('%s4RelaxRx' %finger).value = -3
        ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
        ctrlShape.attr('%s3SlideRx' %finger).value = -8
        # ctrlShape.attr('%s4SlideRx' %finger).value = 4.5
        ctrlShape.attr('%s1ScruchRx' %finger).value = 1.5
        ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
        ctrlShape.attr('%s3ScruchRx' %finger).value = -8
        # ctrlShape.attr('%s4ScruchRx' %finger).value = -8
        ctrlShape.attr('%sBaseSpread' %finger).value = 5
        ctrlShape.attr('%sSpread' %finger).value = 5
        ctrlShape.attr('%sBaseBreak' %finger).value = -4.5
        ctrlShape.attr('%sBreak' %finger).value = -4.5
        ctrlShape.attr('%sBaseFlex' %finger).value = -9
        ctrlShape.attr('%sFlex' %finger).value = -9
    
    print '##------------------------------------------------------------------------------------'
    print '##-----------------------------ADD RIG GROUP------------------------------------------'
    print '##------------------------------------------------------------------------------------'



    # print "# Generate >> Set preferred ankle Leg Ik"
    # utaCore.preferredAnkleLegIk()


    print '##------------------------------------------------------------------------------------'
    print '##-----------------------------CLEAR UTILITIE-----------------------------------------'
    print '##------------------------------------------------------------------------------------'

    print "# Generate >> Read Control Shape"
    ctrlData.read_ctrl_dataFld()


    print "# Generate >> Character Text Name"
    charTextName.charTextName(value = '5', size = '1')

    print "# Generate >> Set Defalut Attribute"
    rigTools.jntRadius( r = 0.025 )
    if mc.objExists('TmpJnt_Grp'):
        mc.delete( 'TmpJnt_Grp' )
    if mc.objExists('Export_Grp'):
        mc.delete('Export_Grp')

    print "# Generate >> DONE !! "