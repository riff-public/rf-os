# v.0.0.1 polytag switcher
_title = 'Tool'
_version = 'v.0.0.1'
_des = 'wip'
uiName = 'ToolName'

#Import python modules
import sys
import os
import getpass
import maya.cmds as mc
import maya.mel as mm

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

# framework modules
import rf_config as config
from rf_utils import log_utils

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)



class Tool():
    def __init__(self, parent=None):
        #Setup Window
        self.w = 200
        self.h = 100

        # ui read

    def show(self):
        self.window = mc.window(uiName, title='%s %s-%s' % (_title, _version, _des))
        self.layout = mc.columnLayout(adj=1)
        self.button = mc.button(l='button', c=self.command)
        mc.showWindow()
        mc.window(uiName, e=True, wh=[self.w, self.h])

    def command(self, *args):
        print 'OK'


def show():
    from rftool.utils.ui import maya_win
    logger.info('Run in Maya\n')
    maya_win.deleteUI(uiName)
    myApp = Tool(maya_win.getMayaWindow())
    myApp.show()
    return myApp
