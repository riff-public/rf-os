# v.0.0.1 polytag switcher
_title = 'Tool'
_version = 'v.0.0.1'
_des = 'wip'
uiName = 'ToolName'

#Import python modules
import sys
import os 
import getpass
import xml.etree.ElementTree as xml
from cStringIO import StringIO

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]


# framework modules 
import rf_config as config
from rf_utils import log_utils
from rf_utils.ui import load
user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'

logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui
from maya.app.general.mayaMixin import MayaQWidgetDockableMixin
import maya.cmds as mc 
from rftool.utils.ui import maya_win

import pyside2uic as pysideuic
from shiboken2 import wrapInstance

def loadUiType(uiFile):
    """
    :author: Jason Parks
    Pyside lacks the "loadUiType" command, so we have to convert the ui file to py code in-memory first
    and then execute it in a special frame to retrieve the form_class.
    """
    parsed = xml.parse(uiFile)
    widget_class = parsed.find('widget').get('class')
    form_class = parsed.find('class').text

    with open(uiFile, 'r') as f:
        o = StringIO()
        frame = {}

        pysideuic.compileUi(f, o, indent=0)
        pyc = compile(o.getvalue(), '<string>', 'exec')
        exec pyc in frame

        # Fetch the base_class and form class based on their type in the xml from designer
        form_class = frame['Ui_%s' % form_class]
        base_class = getattr(QtWidgets, widget_class)
    return form_class, base_class


class Tool(MayaQWidgetDockableMixin, QtWidgets.QMainWindow):
    MAYA2014 = 201400
    MAYA2015 = 201500
    MAYA2016 = 201600
    MAYA2016_5 = 201650
    MAYA2017 = 201700

    def __init__(self, dock=True, parent=None):
        #Setup Window
        self.deleteInstances()
        super(Tool, self).__init__(parent)
        self.setWindowFlags(QtCore.Qt.Tool)
        # ui read
        uiFile = '%s/ui.ui' % moduleDir
        # self.ui = load.setup_ui_maya(uiFile, parent)

        form_class, base_class = loadUiType(uiFile)
        self.ui = form_class()
        self.ui.setupUi(self)
        
        # self.ui.show()
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)

        self.ui.switch_pushButton.clicked.connect(self.ac)

    def dockCloseEventTriggered(self):
        self.deleteInstances()

    # Delete any instances of this class
    def deleteInstances(self):

        def delete2016():
            # Go through main window's children to find any previous instances
            for obj in maya_win.getMayaWindow().children():

                if str(type(
                        obj)) == "<class 'maya.app.general.mayaMixin.MayaQDockWidget'>":  # ""<class 'maya.app.general.mayaMixin.MayaQDockWidget'>":

                    if obj.widget().__class__.__name__ == "MyDockingWindow":  # Compare object names

                        obj.setParent(None)
                        obj.deleteLater()

        def delete2017():
            '''
            Look like on 2017 this needs to be a little diffrents, like in this function,
            However, i might be missing something since ive done this very late at night :)
            '''

            for obj in maya_win.getMayaWindow().children():

                if str(type(obj)) == "<class '{}.MyDockingWindow'>".format(os.path.splitext(
                        os.path.basename(__file__)[0])):  # ""<class 'moduleName.mayaMixin.MyDockingWindow'>":

                    if obj.__class__.__name__ == "MyDockingWindow":  # Compare object names

                        obj.setParent(None)
                        obj.deleteLater()

        delete2017()

    def deleteControl(self, control):

        if mc.workspaceControl(control, q=True, exists=True):
            mc.workspaceControl(control, e=True, close=True)
            mc.deleteUI(control, control=True)

    def run(self):
        '''
        2017 docking is a little different...
        '''

        def run2017():
            self.setObjectName("MyMainDockingWindow")

            # The deleteInstances() dose not remove the workspace control, and we need to remove it manually
            workspaceControlName = self.objectName() + 'WorkspaceControl'
            self.deleteControl(workspaceControlName)

            # this class is inheriting MayaQWidgetDockableMixin.show(), which will eventually call maya.mc.workspaceControl.
            # I'm calling it again, since the MayaQWidgetDockableMixin dose not have the option to use the "tabToControl" flag,
            # which was the only way i found i can dock my window next to the channel controls, attributes editor and modelling toolkit.
            self.show(dockable=False, area='right', floating=False)
            mc.workspaceControl(workspaceControlName, e=True, ttc=["AttributeEditor", -1], wp="preferred", mw=420)
            self.raise_()

            # size can be adjusted, of course
            self.setDockableParameters(width=420)

        run2017()

    def deleteControl(self, control):

        if mc.workspaceControl(control, q=True, exists=True):
            mc.workspaceControl(control, e=True, close=True)
            mc.deleteUI(control, control=True)

    def ac(self): 
        print 'OK'


def show():
    # maya_win.deleteUI(uiName)
    myApp = Tool(maya_win.getMayaWindow())
    myApp.run()
    return myApp
