# v.0.0.1 polytag switcher
_title = 'Tool'
_version = 'v.0.0.1'
_des = 'wip'
uiName = 'ToolName'

#Import python modules
import sys
import os 
import getpass

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]


# framework modules 
import rf_config as config
from rf_utils import log_utils
from rf_utils.ui import load
user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'

logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui


class Tool(QtWidgets.QMainWindow):

    def __init__(self, parent=None):
        #Setup Window
        super(Tool, self).__init__(parent)

        # ui read
        uiFile = '%s/ui.ui' % moduleDir
        self.ui = load.setup_ui_maya(uiFile, parent)
        self.setCentralWidget(self.ui)
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))

        self.ui.switch_pushButton.clicked.connect(self.ac)

    def ac(self): 
        print 'OK'


def show():
    from rftool.utils.ui import maya_win
    logger.info('Run in Maya\n')
    maya_win.deleteUI(uiName)
    myApp = Tool(maya_win.getMayaWindow())
    myApp.show()
    return myApp
