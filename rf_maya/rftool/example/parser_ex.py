import os 
import sys
import argparse

def setup_parser(title):
    # create the parser
    parser = argparse.ArgumentParser(description=title, formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('-s', dest='scene', type=str, help='The path to the source scene to be cached', required=True)
    parser.add_argument('-p', dest='process', nargs='+', help='Processes to be cached', default=[])
    return parser


def main(scene, process=[]): 
    print scene
    print process 


if __name__ == "__main__":
    # print sys.argv
    parser = setup_parser('Example parser')
    params = parser.parse_args()    
    main(scene=params.scene, process=params.process)

