import maya.OpenMayaUI as mui
from PySide2 import QtWidgets, QtGui, QtCore
from shiboken2 import wrapInstance
import ui 

class Main(QtWidgets.QMainWindow):
	"""docstring for Main"""
	def __init__(self, parent=None):
		super(Main, self).__init__(parent)
		self.parent = parent
		self.ui = ui.Ui_MainWindow()
		self.ui.setupUi(self)


def show(): 
	ui = Main(getMayaWindow())
	ui.show()


def getMayaWindow():
    """ Maya Qt pointer """
    ptr = mui.MQtUtil.mainWindow()
    return wrapInstance(long(ptr), QtWidgets.QWidget)