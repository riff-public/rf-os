from Qt import QtWidgets, QtCore
import view
import model 
from rftool.utils.ui import maya_win

class Controller(QtCore.QObject): 
    clicked = QtCore.Signal(str)


class App(object):
	"""docstring for App"""
	def __init__(self):
		super(App, self).__init__()
		self.controller = Controller()
		self.ui = view.Ui(self.controller, maya_win.getMayaWindow())
		self.ui.show()
		# signals 
		self.controller.clicked.connect(model.command)
				
# cont = Controller()
# cont.clicked.connect(model.command)
# a = view.Ui(cont, maya_win.getMayaWindow())
# a.show()
def show(): 
	app = App()
	app.ui.show()
