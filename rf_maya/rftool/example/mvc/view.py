from Qt import QtWidgets, QtCore


class Ui(QtWidgets.QMainWindow): 
    def __init__(self, controller, parent=None):
        super(Ui, self).__init__(parent=parent)
        self.controller = controller 
        self.container = QtWidgets.QWidget()
        self.layout = QtWidgets.QVBoxLayout(self.container)
        self.button = QtWidgets.QPushButton('OK')
        self.layout.addWidget(self.button)
        self.setCentralWidget(self.container)
        self.setObjectName('UiTest')
        
        self.button.clicked.connect(self.buttonClicked)
        
    def buttonClicked(self): 
        self.controller.clicked.emit('test')
        