import os
import re
import subprocess
import tempfile
from collections import defaultdict

import pymel.core as pm
import maya.cmds as mc
import maya.OpenMaya as om
import maya.OpenMayaUI as omui
import maya.mel as mel

from rf_utils import file_utils
from rf_utils.context import context_info
from rf_utils import custom_exception
from rf_utils import admin

TURN_CAM_GRP = 'TurnCam_Grp'
TURN_CAM_NAME = 'Turn_Cam'
STILL_CAM_NAME = 'Still_Cam'
NS = 'assetCam'
LIGHT_NS = 'assetLight'
DIFFUSE_LAYER_NAME = 'diffuseLayer'

currPath = str(pm.sceneName())
asset = None
if currPath:
    asset = context_info.ContextPathInfo(path=currPath)

def getLocalCamPath():
    asset.projectInfo.asset.global_asset(key='assetCam')
    asset.context.update(entityType='asset', step='default')
    camPath = '%s/%s' % (asset.path.hero().abs_path(), asset.output_name(outputKey='assetCam', hero=True))
    return camPath 

def getExistingCam(namespace, camType):
    if camType == 'assetTurnCam':
        camGrp = pm.PyNode('%s:%s' %(namespace, TURN_CAM_GRP))
        camTr = pm.PyNode('%s:%s' %(namespace, TURN_CAM_NAME))
        camShp = camTr.getShape()
        
    elif camType == 'assetStillCam':
        camGrp = None
        camTr = pm.PyNode('%s:%s' %(namespace, STILL_CAM_NAME))
        camShp = camTr.getShape()
    return (camGrp, camTr, camShp) 

def get_output_path():
    localDir = '%s%s' %(os.environ['HOMEDRIVE'], os.environ['HOMEPATH'])
    localDir = localDir.replace('\\','/')
    localDir = '%s/%s' %(localDir, asset.name)
    return localDir

def getMayaBinDir():
    version = mc.about(v=True)
    path = 'C:/Program Files/Autodesk/Maya%s/bin' % version
    return path

def loadPlugin(renderer=''):
    renderers = {'arnold':'mtoa.mll', 'redshift':'redshift4maya'}
    plugins = ['AbcExport.mll']
    if renderer:
        if renderer in renderers:
            plugins.append(renderers[renderer])
    for plugin in plugins:
        if not pm.pluginInfo(plugin, q=True, l=True):
            pm.loadPlugin(plugin)
    dr = pm.PyNode('defaultRenderGlobals')
    dr.currentRenderer.set(renderer)

def prepareDir():
    localDir = get_output_path()
    if not os.path.exists(localDir):
        os.makedirs(localDir)

    mel.eval('workspace -fileRule "images" "%s";' %localDir)
    mel.eval('workspace -saveWorkspace;')
    return localDir

def turnOffAllRenderables():
    for cam in pm.ls(type='camera'):
        cam.renderable.set(False)

def getNsFromMa(maPath):
    data = None
    namespaces = set()
    with open(maPath, 'r') as f:
        found = False
        for line in f:
            startswithFile = line.startswith('file')
            if startswithFile:
                found = True
                splits = line.split(' ')
                if '-ns' in splits:
                    index = splits.index('-ns')
                    namespaces.add(splits[index+1][1:-1])
            if found == True and not startswithFile:
                break

    return list(namespaces)

def getNamespace(name):
    i = 1
    exit = False
    localCamPath = getLocalCamPath()
    heroNamespaces = []
    if os.path.exists(localCamPath):
        try:
            heroNamespaces = getNsFromMa(localCamPath)
        except:
            pass

    while not exit:
        ns = '%s%s' %(name, i)
        exists_and_inuse = pm.namespace(ex=ns) and mc.ls('%s:*' %ns, type='transform')
        if ns in heroNamespaces or exists_and_inuse:
            i += 1
            continue
        else:
            exit = True
    return ns
 
def cleanNamespace():
    pm.namespace(setNamespace=':')
    namespaces = [n for n in mc.namespaceInfo(listOnlyNamespaces=True, recurse=True) if n.startswith(NS)]
    removed = set()
    for whole_ns in namespaces :
        if mc.ls('%s:*' %whole_ns, type='transform'):
            continue

        ns = whole_ns.split(':')
        for n in [i for i in ns if i not in removed]:
            try :
                pm.namespace(mv=[n,':'], f=True)
                if n in pm.namespaceInfo(lon=True):
                    pm.namespace(rm=n)
                    print 'removed %s' %n
                    removed.add(n)
            except:
                continue

def import_cam():
    # get local cam path
    camPath = getLocalCamPath()
    if os.path.exists(camPath):
        cleanNamespace() 
        nodes = pm.importFile(camPath, mnp=True, rnn=True, pr=True)
        om.MGlobal.displayInfo('Camera imported from: %s' %camPath)
    
def new_cam(camType):
    camPath = getLocalCamPath()
    result = pm.promptDialog(title='New camera', message='Name:', text=camType, 
        button=('OK', 'Cancle'))
    if result == 'Cancle':
        return
    name = pm.promptDialog(q=True, text=True)

    cleanNamespace() 
    ns = getNamespace(name)
    camPath = asset.projectInfo.asset.global_asset(key=camType)
    pm.createReference(camPath, namespace=ns)
    camGrp, camTr, camShp = getExistingCam(namespace=ns, camType=camType)
    pm.lookThru(camShp)

    om.MGlobal.displayInfo('New camera craeted: %s' %ns)

def import_light():
    lightPath = asset.projectInfo.asset.global_asset(key='lookDevLight')
    if lightPath in [str(r.path) for r in pm.listReferences()]:
        return
    pm.createReference(lightPath, namespace=LIGHT_NS)
    om.MGlobal.displayInfo('Light imported from: %s' %lightPath)

def remove_light():
    lightPath = asset.projectInfo.asset.global_asset(key='lookDevLight')
    for ref in pm.listReferences():
        if str(ref.path) == lightPath:
            ref.remove()

def getAllTurnCams():
    turnCams = pm.ls('*:Turn_CamShape', type='camera')
    return turnCams

def getAllStillCams():
    stillCams = pm.ls('*:Still_CamShape', type='camera')
    return stillCams

def getCurrentCam():
    view = omui.M3dView.active3dView()
    cam = om.MDagPath()
    view.getCamera(cam)
    camPath = cam.fullPathName()
    return camPath

def remove_all_camera():
    turnCams = getAllTurnCams()
    stillCams = getAllStillCams()
    for camShp in turnCams + stillCams:
        refPath = pm.referenceQuery(camShp, f=True)
        fileRef = pm.FileReference(refPath)
        fileRef.remove()

    om.MGlobal.displayInfo('All cameras removed!')

def edit_camera():
    camPath = getCurrentCam()
    if not camPath:
        return

    camShp = pm.PyNode(camPath)
    if not camShp.isReferenced():
        return

    msg = 'This will override camera set by other department'
    msg += '\nAre you sure?'
    result = pm.confirmDialog(title='Warning', message=msg, 
        button=('Yes', 'Cancle'))
    if result == 'Cancle':
        return

    camTr = camShp.getParent()
    camName = camTr.nodeName().split(':')[-1]
    if camName == STILL_CAM_NAME:
        camTr.translate.unlock()
        camTr.rotate.unlock()
        camTr.scale.unlock()
        camShp.verticalFilmAperture.unlock()
        camShp.horizontalFilmAperture.unlock()
        camShp.focalLength.unlock()
    elif camName == TURN_CAM_NAME:
        camGrp = camTr.getParent()
        camGrp.tx.unlock()
        camGrp.ty.unlock()
        camGrp.tz.unlock()
        camTr.ty.unlock()
        camTr.tz.unlock()
        camTr.rx.unlock()
        camShp.verticalFilmAperture.unlock()
        camShp.horizontalFilmAperture.unlock()
        camShp.focalLength.unlock()

    pm.select(camTr)
    om.MGlobal.displayWarning('Unlocked: %s' %camTr.nodeName())

def export_all_cameras(warn=False):
    turnCams = getAllTurnCams()
    stillCams = getAllStillCams()
    cam_to_exports = []
    if turnCams:
        for camShp in turnCams:
            camTr = camShp.getParent()
            camGrp = camTr.getParent()

            # lock the cam
            camGrp.tx.lock()
            camGrp.ty.lock()
            camGrp.tz.lock()
            camTr.ty.lock()
            camTr.tz.lock()
            camTr.rx.lock()
            camShp.verticalFilmAperture.lock()
            camShp.horizontalFilmAperture.lock()
            camShp.focalLength.lock()

            # find top ref node
            allParents = camShp.getAllParents()
            refParents = [i for i in allParents if i.isReferenced()]
            camTop = refParents[-1]
            camTopParent = camTop.getParent()
            pm.parent(camTop, w=True)  # need to unparent

            cam_to_exports.append(camTop)
            if camTopParent:
                pm.parent(camTop, camTopParent)

    if stillCams:
        for camShp in stillCams:
            camTr = camShp.getParent()
            # lock the cam
            camTr.translate.lock()
            camTr.rotate.lock()
            camTr.scale.lock()
            camShp.verticalFilmAperture.lock()
            camShp.horizontalFilmAperture.lock()
            camShp.focalLength.lock()
            pm.parent(camTr, w=True)  # need to unparent

            cam_to_exports.append(camTr)

    if cam_to_exports:
        camPath = getLocalCamPath()
        if warn == True and os.path.exists(camPath):
            msg = 'This will overwrite existing camera.'
            msg += '\nMake sure you have imported old cameras.'
            msg += '\nAre you sure you want to export?'
            result = pm.confirmDialog(title='Warning', message=msg, 
                button=('Yes', 'Cancle'))
            if result == 'Cancle':
                return
        # select all cameras
        pm.select(cam_to_exports)
        # create temp file
        fh, tmpPath = tempfile.mkstemp(suffix='.ma')
        os.close(fh)
        tmpPath = tmpPath.replace('\\' ,'/')
        # export to temp
        pm.exportSelected(tmpPath, f=True, pr=True)
        # copy to hero path using admin rights
        admin.copyfile(tmpPath, camPath)
        # remove temp file
        os.remove(tmpPath)

        om.MGlobal.displayInfo('Camera exported to: %s' %camPath)

def create_turn_images(namespaces=[], renderer='redshift'):
    camShps = []
    if not namespaces:
        camShps = getAllTurnCams()
        if not camShps:
            return
        namespaces = [c.namespace()[:-1] for c in camShps]

    msg = 'This will save your scene and fire a batch render for all turn cameras.'
    msg += '\nWhen the render begin, DO NOT fire other render until it is done.'
    msg += '\n\nCamera list:'
    for n in namespaces:
        msg += '\n  %s:%s' %(n, TURN_CAM_NAME)
    result = pm.confirmDialog(title='Confirm', message=msg, 
        button=('OK', 'Cancle'))
    if result == 'Cancle':
        return

    camPath = getLocalCamPath()
    if not os.path.exists(camPath):
        msg = 'No exported camera found.'
        msg += '\nExport cameras?'
        result = pm.confirmDialog(title='Warning', message=msg, 
            button=('Yes', 'No', 'Cancle'))
        if result == 'Yes':
            export_all_cameras(warn=False)
        elif result == 'Cancle':
            return
    
    # save the scene
    pm.saveFile(f=True)
    prepareDir()
    
    # turn renderable on for all Turn Cameras
    turnOffAllRenderables()
    for cam in camShps:
        cam.renderable.set(True)

    # localDir = get_output_path()
    # # fire the render
    # render = str('%s/Render.exe' %getMayaBinDir())
    # render.replace('\\', '/')
    # currScene = str(pm.sceneName())
    # layers = ['masterLayer']
    # if pm.objExists(DIFFUSE_LAYER_NAME):
    #     layers.append(DIFFUSE_LAYER_NAME)
    # cmdStr = ''

    # # create command strings
    # dr = pm.PyNode('defaultRenderGlobals')
    # for cam in camShps:
    #     for lyr in layers:
    #         sf = dr.startFrame.get()
    #         ef = dr.endFrame.get()
    #         cmdList = ['"%s"' %render, '-r', '"%s"' %renderer, '-cam', '"%s"' %cam.shortName(), '-s', str(sf), '-e', str(ef), '-rd', '"%s"' %localDir]
    #         cmdList.append('-rl')
    #         cmdList.append('"%s"' %lyr)
    #         cmdList.append('"%s"' %currScene)
    #         cmdStr += ' '.join(cmdList)
    #         cmdStr += '\n'

    # cmdStr += 'PAUSE'
    # batPath = '%s/assetCam_temp.bat' %localDir
    # with open(batPath, 'w') as f:
    #     f.write(cmdStr)

    # # print batPath
    # env = asset.projectInfo.get_project_env(dcc='maya', dccVersion=mc.about(v=True), department='Model')
    # if 'PATH' in env:
    #     paths = env['PATH'].split(';')
    #     paths.insert(0, render)
    #     print paths
    #     env['PATH'] = str(';'.join(paths))
    # # print env
    # subprocess.Popen(batPath, env=env) 
    # # os.system(batPath)
    # # P:/ThreeEyes/asset/work/char/eagle/model/main/maya/eagle_mdl_main.v011.TEST.ma

    # ---- using render sequence
    mel.eval('createRenderSequenceOptionVars;')
    mel.eval('optionVar -iv "renderSequenceAllLayers" 1;')
    mel.eval('optionVar -iv "renderSequenceAllCameras" 1;')
    if renderer == 'redshift':
        mel.eval('redshiftBatchRender("");')
    else:
        mel.eval('renderSequence;')

def setup_render_diffuse(renderer='redshift'):
    currentLayer = pm.nt.RenderLayer.currentLayer()

    # create render layer
    if DIFFUSE_LAYER_NAME not in [n for n in pm.ls(type='renderLayer') if n.nodeName()==DIFFUSE_LAYER_NAME]:
        diff_lyr = pm.createRenderLayer(empty=True, n=DIFFUSE_LAYER_NAME)
    else:
        diff_lyr = pm.PyNode(DIFFUSE_LAYER_NAME)

    geoGrps = []
    try:
        geoGrpName = asset.projectInfo.asset.geo_grp()
    except:
        geoGrpName = 'Geo_Grp'

    geoGrps = pm.ls(geoGrpName) + pm.ls('*:%s' %geoGrpName)
    if not geoGrps:
        raise Exception, 'Cannot find: %s' %geoGrpName

    geoGrp = geoGrps[0]
    funcs = {'redshift': create_diffuse_rs, 'arnold': create_diffuse_ai}
    to_assign = funcs[renderer](geoGrp)

    
    diff_lyr.addMembers(geoGrp)
    # set current render layer to diffuse
    diff_lyr.setCurrent()

    # set this layer to use MayaHardware 2.0
    dr = pm.PyNode('defaultRenderGlobals')
    pm.editRenderLayerAdjustment(dr.currentRenderer)
    dr.currentRenderer.set('mayaHardware2')
    dr.imageFormat.set(32)

    # set Hardware renderer 2.0
    hwr = pm.PyNode('hardwareRenderingGlobals')
    pm.editRenderLayerAdjustment(hwr.enableTextureMaxRes)
    pm.editRenderLayerAdjustment(hwr.textureMaxResolution)
    pm.editRenderLayerAdjustment(hwr.multiSampleEnable)
    hwr.enableTextureMaxRes.set(1)
    hwr.textureMaxResolution.set(4096)
    hwr.multiSampleEnable.set(1)
    hwr.multiSampleCount.set(8)

    
    # print to_assign
    # set diffuse render layer to current
    for sfShd, members in to_assign.iteritems():
        existing_members = []
        for m in members:
            if pm.objExists(m):
                existing_members.append(m)
        try:
            pm.select(existing_members, r=True)
            pm.hyperShade(assign=sfShd)
            print 'assigned %s %s' %(sfShd, existing_members)
        except:
            pass

    pm.select(cl=True)
    mel.eval('generateAllUvTilePreviews;')
    # currentLayer.setCurrent()

    om.MGlobal.displayInfo('Diffuse set!')

def create_diffuse_rs(geoGrp):
    # get shaders
    shaders = {}
    for mesh in geoGrp.getChildren(ad=True, type='mesh'):
        sgs = mesh.outputs(type='shadingEngine')
        meshName = mesh.shortName()
        for sg in sgs:
            rsMats = sg.inputs(type=('RedshiftMaterial', 'lambert', 'phong', 'phongE', 'blinn'))
            if not rsMats:
                om.MGlobal.displayWarning('Skipping %s, no RedshiftMaterial found on: %s' %(meshName, sg.nodeName()))
                continue
            rsMat = rsMats[0]
            shaders[rsMat] = sg

    to_assign = defaultdict(list)
    colorAttrNames = {'RedshiftMaterial': 'diffuse_color', 
        'lambert': 'color',
        'phong': 'color', 
        'phongE': 'color', 
        'blinn': 'color'}
    for mat, sg in shaders.iteritems():
        shdTyp = mat.nodeType()
        if shdTyp not in colorAttrNames:
            continue
        colorAttr = mat.attr(colorAttrNames[shdTyp])
        textures = colorAttr.inputs(type='file')

        # create surface shader
        ssName = '%s_DiffSfShd' %mat.nodeName()
        if not pm.objExists(ssName):
            sfShd = pm.shadingNode('surfaceShader', asShader=True, n=ssName)
        else:
            sfShd = pm.ls(ssName)[0]
        if textures:
            pm.connectAttr(textures[0].outColor, sfShd.outColor, f=True)
        else:
            colorValue = colorAttr.get()
            sfShd.outColor.set(colorValue)

        # transparency
        ot_value = (0.0, 0.0, 0.0)
        if shdTyp in ('lambert', 'phong', 'phongE', 'blinn'):
            ot_value == mat.transparency.get()
        elif shdTyp == 'RedshiftMaterial':
            op_value = mat.opacity_color.get()
            rfc_weight = mat.refr_weight.get()
            rfc_color = mat.refr_color.get()
            ot_value = (0, 0, 0)
            if op_value != (1.0, 1.0, 1.0):
                ot_value = op_value
            elif rfc_weight != 0.0:
                ot_value = (rfc_color[0]*rfc_weight, rfc_color[1]*rfc_weight, rfc_color[2]*rfc_weight)
        sfShd.outTransparency.set(ot_value)

        # assignment
        ssSgs = sfShd.outputs(type='shadingEngine')
        ssSgName = '%sSG' %ssName
        if not ssSgs:
            ssSg = pm.sets(renderable=True, noSurfaceShader=True, empty=True, name=ssSgName)
            pm.connectAttr(sfShd.outColor, ssSg.surfaceShader)
        else:
            ssSg = ssSgs[0]

        curr_members = sg.members()
        for mem in curr_members:
            if isinstance(mem, pm.MeshFace):
                node = mem.node()
                parents = node.getAllParents()
                if geoGrp in parents:
                    to_assign[sfShd].append(mem)
            else:
                to_assign[sfShd].append(mem)
    return to_assign

def create_diffuse_ai(geoGrp):
    # get shaders
    shaders = {}
    for mesh in geoGrp.getChildren(ad=True, type='mesh'):
        sgs = mesh.outputs(type='shadingEngine')
        meshName = mesh.shortName()
        for sg in sgs:
            aiMats = sg.inputs(type=('aiStandardSurface', 'lambert', 'phong', 'phongE', 'blinn'))
            if not aiMats:
                om.MGlobal.displayWarning('Skipping %s, no aiStandardSurface found on: %s' %(meshName, sg.nodeName()))
                continue
            aiMat = aiMats[0]
            shaders[aiMat] = sg

    to_assign = defaultdict(list)
    colorAttrNames = {'aiStandardSurface': 'baseColor', 
        'lambert': 'color',
        'phong': 'color', 
        'phongE': 'color', 
        'blinn': 'color'}
    for mat, sg in shaders.iteritems():
        shdTyp = mat.nodeType()
        if shdTyp not in colorAttrNames:
            continue
        colorAttr = mat.attr(colorAttrNames[shdTyp])
        textures = colorAttr.inputs(type='file')

        # create surface shader
        ssName = '%s_DiffSfShd' %mat.nodeName()
        if not pm.objExists(ssName):
            sfShd = pm.shadingNode('surfaceShader', asShader=True, n=ssName)
        else:
            sfShd = pm.ls(ssName)[0]

        # set color
        if textures:
            pm.connectAttr(textures[0].outColor, sfShd.outColor, f=True)
        else:
            colorValue = colorAttr.get()
            sfShd.outColor.set(colorValue)

        # transparency
        ot_value = (0.0, 0.0, 0.0)
        if shdTyp in ('lambert', 'phong', 'phongE', 'blinn'):
            ot_value == mat.transparency.get()
        elif shdTyp == 'aiStandardSurface':
            op_value = mat.opacity.get()
            ot_value = (abs(1.0 - op_value[0]), abs(1.0 - op_value[1]), abs(1.0 - op_value[2]))
        sfShd.outTransparency.set(ot_value)

        # assignment
        ssSgs = sfShd.outputs(type='shadingEngine')
        ssSgName = '%sSG' %ssName
        if not ssSgs:
            ssSg = pm.sets(renderable=True, noSurfaceShader=True, empty=True, name=ssSgName)
            pm.connectAttr(sfShd.outColor, ssSg.surfaceShader)
        else:
            ssSg = ssSgs[0]

        curr_members = sg.members()
        for mem in curr_members:
            if isinstance(mem, pm.MeshFace):
                node = mem.node()
                parents = node.getAllParents()
                if geoGrp in parents:
                    to_assign[sfShd].append(mem)
            else:
                to_assign[sfShd].append(mem)

    return to_assign

def remove_render_diffuse():
    if not pm.objExists(DIFFUSE_LAYER_NAME):
        return
    diff_lyr = pm.PyNode(DIFFUSE_LAYER_NAME)
    members = diff_lyr.listMembers()
    if members:
        diff_lyr.removeMembers(members)

    dr = pm.PyNode('defaultRenderLayer')
    dr.setCurrent()
    pm.delete(diff_lyr)
    mel.eval('MLdeleteUnused;')

    om.MGlobal.displayInfo('Diffuse removed!')

def setup_render_settings_rs():
    loadPlugin('redshift')
    if not pm.objExists('redshiftOptions'):
        pm.createNode('RedshiftOptions')
    prepareDir()

    dr = pm.PyNode('defaultRenderGlobals')
    ds = pm.PyNode('defaultResolution')
    rs = pm.PyNode('redshiftOptions')
    
    # 16 bit PNG
    rs.imageFormat.set(2)
    rs.pngBits.set(16)

    # file gamma mode - Use display gamma
    rs.fileGammaMode.set(2)

    # GI
    rs.primaryGIEngine.set(4)
    rs.secondaryGIEngine.set(2)

    # Samples
    rs.unifiedMinSamples.set(4)
    rs.unifiedMaxSamples.set(12)

    # gamma
    rs.fileGammaMode.set(2)
    rs.samplingGammaMode.set(2)

    # trace depth
    rs.refractionMaxTraceDepth.set(4)

    dr.imageFilePrefix.set('<Camera>/%s_<Camera>_<RenderLayer>_<RenderPass>' %asset.name)
    dr.animation.set(1)
    dr.startFrame.set(1)
    dr.endFrame.set(12)
    dr.putFrameBeforeExt.set(True)

    ds.width.set(2048)
    ds.height.set(2048)
    ds.deviceAspectRatio.set(1.0)
    ds.pixelAspect.set(1.0)
    ds.lockDeviceAspectRatio.set(False)

    # turn on color management
    # set_color_management()

    om.MGlobal.displayInfo('Render settings set!')

def setup_render_settings_ai():
    loadPlugin('arnold')
    import mtoa.core as aicore
    aicore.createOptions()
    prepareDir()
    
    # overwrite file name, frame and resolution settings
    dr = pm.PyNode('defaultRenderGlobals')
    ds = pm.PyNode('defaultResolution')
    ad = pm.PyNode('defaultArnoldDriver')
    aop = pm.PyNode('defaultArnoldRenderOptions')

    ad.halfPrecision.set(1)
    ad.preserveLayerName.set(0)
    ad.exrTiled.set(0)
    ad.autocrop.set(0)
    ad.append.set(0)
    ad.mergeAOVs.set(0)

    ad.aiTranslator.set('png')
    
    dr.imageFilePrefix.set('<Camera>/%s_<Camera>_<RenderLayer>_<RenderPass>' %asset.name)
    dr.animation.set(1)
    dr.startFrame.set(1)
    dr.endFrame.set(12)
    dr.putFrameBeforeExt.set(True)

    ds.width.set(2048)
    ds.height.set(2048)
    ds.deviceAspectRatio.set(1.0)
    ds.pixelAspect.set(1.0)
    ds.lockDeviceAspectRatio.set(False)

    # set_color_management()

    om.MGlobal.displayInfo('Render settings set!')

def set_color_management():
    # turn on color management
    pm.colorManagementPrefs(e=True, cmEnabled=True)
    pm.colorManagementPrefs(e=True, rsn='scene-linear Rec 709/sRGB')
    pm.colorManagementPrefs(e=True, vtn='sRGB gamma')
    pm.colorManagementPrefs(e=True, outputTransformEnabled=True)
    pm.colorManagementPrefs(e=True, outputTransformName='sRGB gamma')

def import_render_settings():
    import maya.app.renderSetup.model.renderSettings as renderSettings
    import json
    filePath = asset.projectInfo.asset.global_asset(key='renderSettings')
    with open(filePath, 'r') as file:
        data = json.load(file)
        renderSettings.decode(data)

def see_output():
    localDir = prepareDir()
    subprocess.Popen(r'explorer "%s"' %localDir.replace('/', '\\'))

def export_for_substance(sels=[]):
    if not sels:
        sels = [i for i in pm.selected(type='transform') if i.getChildren(ad=True, type='mesh')]
        if not sels:
            
            geoGrpName = asset.projectInfo.asset.geo_grp()
            try:
                sels = [pm.PyNode(geoGrpName)]
            except Exception, e:
                om.MGlobal.displayError('Cannot find: %s' %geoGrpName)
                return

            if not sels:
                return
            msg = 'Export %s and all its children?' %geoGrpName
            result = pm.confirmDialog(title='Warning', message=msg, 
                button=('Yes', 'Cancle'))
            if result == 'Cancle':
                return

    loadPlugin()
    localDir = get_output_path()
    if not os.path.exists(localDir):
        os.makedirs(localDir)

    basicFilter = "*.abc"
    outputPath = pm.fileDialog2(fileFilter=basicFilter, dialogStyle=0, dir=localDir)
    if outputPath:
        outputPath = outputPath[0]
    else:
        return

    # get cameras
    turnCams = getAllTurnCams()
    stillCams = getAllStillCams()

    all_cams = []
    f_cams = []
    currTime = pm.currentTime(q=True)

    if stillCams:
        all_cams = [c.getParent() for c in stillCams]
    
    if turnCams:
        turnCams = [c.getParent() for c in turnCams]
        for frame in xrange(1, 13):
            pm.currentTime(frame)
            for cam in turnCams:
                f_cam = cam.duplicate(name='%s_%s' %(cam.nodeName(), str(frame).zfill(2)))[0]
                for attr in 'trs':
                    f_cam.attr(attr).unlock()
                    for axis in 'xyz':
                        f_cam.attr('%s%s' %(attr, axis)).unlock()
                pm.parent(f_cam, w=True)
                f_cams.append(f_cam)

        pm.currentTime(currTime)
        f_cams = sorted(f_cams, key=lambda c: c.nodeName())
        all_cams.extend(f_cams)

    rootArgs = [n.longName() for n in sels]
    if all_cams:
        rootArgs += [n.longName() for n in all_cams]
    rootStr = ' -root '.join(rootArgs)

    pm.AbcExport(j='-frameRange %s %s -uvWrite -worldSpace -writeVisibility -dataFormat ogawa -root %s -f %s' %(currTime, currTime, rootStr, outputPath))
    if f_cams:
        pm.delete(f_cams)

def getSmoothPolyList():
    sels = pm.selected(type='transform')
    if not sels:
        om.MGlobal.displayError('Select a top group.')
        return None, None, None
    topGrp = sels[0]

    if topGrp.isReferenced():
        path = pm.referenceQuery(topGrp, f=True)
    else:
        if not topGrp.hasAttr('publishPath'):
            om.MGlobal.displayError('Invalid top group: %s' %topGrp.shortName())
            return None, None, None
        publishPath = topGrp.publishPath.get()
        path = context_info.RootPath(publishPath).abs_path()

    tmp_asset = context_info.ContextPathInfo(path=path)
    tmp_asset.context.update(step='model', process='main', res='md')
    dataDir = tmp_asset.path.output_hero().abs_path()
    smoothData = tmp_asset.output_name(outputKey='smoothData', hero=True, ext='.yml')
    data = '%s/%s' % (dataDir, smoothData)

    if not os.path.exists(data):
        om.MGlobal.displayError('Smooth data does not exists: %s' %data)
        return

    polyList = file_utils.ymlLoader(data)
    return polyList, topGrp, tmp_asset

def set_smooth_rs():
    polyList, topGrp, tmp_asset = getSmoothPolyList()
    if not polyList:
        return

    polyExists = []
    topGrp_children = topGrp.getChildren(ad=True, type='transform')
    for geo in topGrp_children:
        geo_name = geo.nodeName().split(':')[-1]
        if geo_name in polyList:
            polyExists.append(geo)
            polyList.remove(geo_name)
    
    if polyExists:
        nodeName = '%s_meshParameter' %tmp_asset.name
        if pm.objExists(nodeName):
            pm.delete(nodeName)

        rsMeshNode = pm.createNode('RedshiftMeshParameters', n=nodeName)
        rsMeshNode.enableDisplacement.set(0)
        pm.sets(rsMeshNode, add=polyExists)

    if polyList:
        om.MGlobal.displayError('%s polys missing\n%s' %(len(polyList), '\n'.join(polyList)))
    else:
        om.MGlobal.displayInfo('Smooth set!')

def set_smooth_ai():
    polyList, topGrp, tmp_asset = getSmoothPolyList()
    if not polyList:
        return

    polyExists = []
    topGrp_children = topGrp.getChildren(ad=True, type='transform')
    for geo in topGrp_children:
        geo_name = geo.nodeName().split(':')[-1]
        if geo_name in polyList:
            polyExists.append(geo)
            polyList.remove(geo_name)
    
    if polyExists:
        smoothSetName = 'smoothPly_set'
        if not pm.objExists(smoothSetName):
            smoothSet = pm.sets(name=smoothSetName)
        else:
            smoothSet = pm.nt.ObjectSet(smoothSetName)
        pm.sets(smoothSet, add=polyExists)
        smoothSet.aiOverride.set(1)
        pm.addAttr(smoothSet, ln='aiSubdivType', at='enum', en='none:catclark:linear', dv=1)
        pm.addAttr(smoothSet, ln='aiSubdivIterations', at='byte', dv=3)
 
    if polyList:
        om.MGlobal.displayError('%s polys missing\n%s' %(len(polyList), '\n'.join(polyList)))
    else:
        om.MGlobal.displayInfo('Smooth set!')