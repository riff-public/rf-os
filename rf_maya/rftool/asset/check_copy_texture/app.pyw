# v.0.0.1 polytag switcher
_title = 'RF Copy Texture Check'
_version = 'v.0.0.1'
_des = 'wip'
uiName = 'RFCopyTextureCheck'

#Import python modules
import sys
import os
import getpass
import json
import logging

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)

# import config
import rf_config as config
from rf_utils import log_utils
# sg

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui
from rf_maya.rftool.utils import maya_utils

import ui
reload(ui)


class RFCopyTextureCheck(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        #Setup Window
        super(RFCopyTextureCheck, self).__init__(parent)

        # ui read
        self.ui = ui.checkTextureUI()
        self.setObjectName(uiName)
        # self.modules = modules
        self.setCentralWidget(self.ui)
        self.adjustSize()
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.ui.label.setText('Please check texture files to Copy')
        self.resize(300, 400)
        self.textures = None

        self.init_signals()
        self.list_texture()

    def init_signals(self): 
        self.ui.checkTexture_pushButton.clicked.connect(self.close)
        self.ui.cancel_pushButton.clicked.connect(self.close)
        self.ui.showPath.stateChanged.connect(self.show_path)

    def list_texture(self):
        self.textures = sorted(maya_utils.collect_textures(targetGrp = 'Export_Grp')) if not self.textures else self.textures
        self.ui.file_listWidget.clear()
        self.ui.file_listWidget.add_files(self.textures)
        self.ui.summary.setText('%s files to copy' % len(self.textures))

    def show_path(self, value): 
        state = True if value else False 
        self.ui.file_listWidget.displayNameOnly = not state
        self.list_texture()


    def close(self): 
        from rftool.utils.ui import maya_win
        maya_win.deleteUI(uiName)


def show():
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        maya_win.deleteUI(uiName)
        myApp = RFCopyTextureCheck(maya_win.getMayaWindow())
        myApp.show()
        return myApp
if __name__ == '__main__':
    show()

