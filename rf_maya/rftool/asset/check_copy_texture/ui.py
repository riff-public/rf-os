import os
import sys

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

from rf_utils.widget import file_widget
reload(file_widget)

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

class checkTextureUI(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(checkTextureUI, self).__init__(parent)
        self.resize(500, 1200)
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.label = QtWidgets.QLabel()
        self.verticalLayout.addWidget(self.label)

        self.widget_Table_Layout = QtWidgets.QVBoxLayout()
        self.file_listWidget = file_widget.FileListWidget()
        self.widget_Table_Layout.addWidget(self.file_listWidget)
        self.verticalLayout.addLayout(self.widget_Table_Layout)

        self.summaryLayout = QtWidgets.QHBoxLayout()
        self.summary = QtWidgets.QLabel()
        self.summaryLayout.addWidget(self.summary)
        self.showPath = QtWidgets.QCheckBox('Show Path')
        self.summaryLayout.addWidget(self.showPath)

        self.verticalLayout.addLayout(self.summaryLayout)

        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.checkTexture_pushButton = QtWidgets.QPushButton("Confirm")
        self.checkTexture_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        self.horizontalLayout.addWidget(self.checkTexture_pushButton)
        self.cancel_pushButton = QtWidgets.QPushButton("Cancel")
        self.cancel_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        self.horizontalLayout.addWidget(self.cancel_pushButton)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.setLayout(self.verticalLayout)