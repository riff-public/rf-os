# v.0.0.1 polytag switcher
_title = 'Riff Model Smooth Data'
_version = 'v.0.0.1'
_des = 'wip'
uiName = 'AssetSetupUI'

#Import python modules
import sys
import os
import logging

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

class TextureUi(QtWidgets.QWidget):
    def __init__(self, parent=None) :
        super(TextureUi, self).__init__(parent)

        self.allLayout = QtWidgets.QVBoxLayout()
        self.label = QtWidgets.QLabel('Texture Setup')
        self.allLayout.addWidget(self.label)

        self.layer_pushButton = QtWidgets.QPushButton('Setup layers')
        self.layer_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        self.allLayout.addWidget(self.layer_pushButton)


        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.allLayout.addItem(spacerItem)

        self.allLayout.setSpacing(8)
        self.allLayout.setContentsMargins(2, 2, 2, 2)

        self.allLayout.setStretch(0, 0)
        self.allLayout.setStretch(1, 0)

        self.setLayout(self.allLayout)



class ModelUi(QtWidgets.QWidget):
    def __init__(self, parent=None) :
        super(ModelUi, self).__init__(parent)

        self.allLayout = QtWidgets.QVBoxLayout()
        self.label = QtWidgets.QLabel('Model Setup')
        self.allLayout.addWidget(self.label)

        self.smooth_pushButton = QtWidgets.QPushButton('Set Smooth Ply')
        self.smooth_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        self.allLayout.addWidget(self.smooth_pushButton)

        self.hiddenPly_pushButton = QtWidgets.QPushButton('Set Hidden Ply')
        self.hiddenPly_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        self.allLayout.addWidget(self.hiddenPly_pushButton)


        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.allLayout.addItem(spacerItem)

        self.allLayout.setSpacing(8)
        self.allLayout.setContentsMargins(2, 2, 2, 2)

        self.allLayout.setStretch(0, 0)
        self.allLayout.setStretch(1, 0)

        self.setLayout(self.allLayout)


class RigUi(QtWidgets.QWidget):
    def __init__(self, parent=None) :
        super(RigUi, self).__init__(parent)

        self.allLayout = QtWidgets.QVBoxLayout()
        self.label = QtWidgets.QLabel('Rig Setup')
        self.allLayout.addWidget(self.label)

        # Set Preview Ply
        self.hiddenPly_pushButton = QtWidgets.QPushButton('Set Preview Ply')
        self.hiddenPly_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        self.allLayout.addWidget(self.hiddenPly_pushButton)

        # Setup look layers
        self.setupLookLayers_pushButton = QtWidgets.QPushButton('Setup Look Layers')
        self.setupLookLayers_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        self.allLayout.addWidget(self.setupLookLayers_pushButton)

        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.allLayout.addItem(spacerItem)

        self.allLayout.setSpacing(8)
        self.allLayout.setContentsMargins(2, 2, 2, 2)

        self.allLayout.setStretch(0, 0)
        self.allLayout.setStretch(1, 0)

        self.setLayout(self.allLayout)

class LookdevUi(QtWidgets.QWidget):
    def __init__(self, parent=None) :
        super(LookdevUi, self).__init__(parent)

        self.allLayout = QtWidgets.QVBoxLayout()
        self.label = QtWidgets.QLabel('Lookdev Setup')
        self.allLayout.addWidget(self.label)

        self.layer_pushButton = QtWidgets.QPushButton('Setup layers')
        self.layer_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        self.assignShade_pushButton = QtWidgets.QPushButton('Auto Assign Look Shader')
        self.assignShade_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        self.allLayout.addWidget(self.layer_pushButton)
        self.allLayout.addWidget(self.assignShade_pushButton)


        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.allLayout.addItem(spacerItem)

        self.allLayout.setSpacing(8)
        self.allLayout.setContentsMargins(2, 2, 2, 2)

        self.allLayout.setStretch(0, 0)
        self.allLayout.setStretch(1, 0)

        self.setLayout(self.allLayout)


class GroomUi(QtWidgets.QWidget):
    def __init__(self, parent=None) :
        super(GroomUi, self).__init__(parent)

        self.allLayout = QtWidgets.QVBoxLayout()
        self.label = QtWidgets.QLabel('Groom Setup')
        self.allLayout.addWidget(self.label)

        self.layer_pushButton = QtWidgets.QPushButton('Setup layers')
        self.layer_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        self.allLayout.addWidget(self.layer_pushButton)


        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.allLayout.addItem(spacerItem)

        self.allLayout.setSpacing(8)
        self.allLayout.setContentsMargins(2, 2, 2, 2)

        self.allLayout.setStretch(0, 0)
        self.allLayout.setStretch(1, 0)

        self.setLayout(self.allLayout)
