# v.0.0.1 polytag switcher
_title = 'Riff SG Manager'
_version = 'v.0.0.1'
_des = 'wip'
uiName = 'AssetSetupUi'

#Import python modules
import sys
import os
import getpass
from datetime import datetime
import logging
from functools import partial
from collections import OrderedDict

import maya.cmds as mc

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

# import config
import rf_config as config

# framework modules
from rf_utils import log_utils

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

import ui
reload(ui)
from rf_utils.context import context_info
from rf_utils.sg import sg_process

class Step: 
    model = 'model'
    texture = 'texture'
    rig = 'rig'
    lookdev = 'lookdev'
    groom = 'groom'
    anim = 'anim'

class AssetSetup(QtWidgets.QMainWindow):
    def __init__(self, entity=None, parent=None):
        #Setup Window
        super(AssetSetup, self).__init__(parent)

        # setup context 
        self.entity = context_info.ContextPathInfo() if not entity else entity
        self.entity.context.use_sg(sg_process.sg, project=self.entity.project, entityType=self.entity.entity_type, entityName=self.entity.name)
        self.ui = self.setup_ui(self.entity, parent)
        self.init_functions()


    def setup_ui(self, entity, parent): 
        if entity.step == Step.model: 
            title = 'Model Setup Ui'
            size = [200, 100]
            insUi = ui.ModelUi(parent)

        elif entity.step == Step.texture: 
            title = 'Texture Setup Ui'
            size = [200, 100]
            insUi = ui.TextureUi(parent)

        elif entity.step == Step.rig: 
            title = 'Rig Setup Ui'
            size = [200, 100]
            insUi = ui.RigUi(parent)

        elif entity.step == Step.lookdev: 
            title = 'Texture Setup Ui'
            size = [200, 100]
            insUi = ui.LookdevUi(parent)

        elif entity.step == Step.groom: 
            title = 'Groom Setup Ui'
            size = [200, 100]
            insUi = ui.GroomUi(parent)

        elif entity.step == Step.anim: 
            title = 'Anim Setsup Ui'
            size = [200, 100]
            insUi = ui.LookdevUi(parent)

        else: 
            title = 'Not in asset pipeline'
            size = [200, 40]
            insUi = QtWidgets.QWidget()
            layout = QtWidgets.QVBoxLayout()
            label = QtWidgets.QLabel('Not in Asset pipeline')
            layout.addWidget(label)
            insUi.setLayout(layout)

        self.setCentralWidget(insUi)
        self.setObjectName(uiName)

        # title
        self.setWindowTitle('%s %s-%s' % (title, _version, _des))
        self.resize(size[0], size[1])

        return insUi 

    def init_functions(self): 
        if self.entity.step == Step.model: 
            self.ui.smooth_pushButton.clicked.connect(self.set_smooth)
            self.ui.hiddenPly_pushButton.clicked.connect(self.set_hidden)
        
        if self.entity.step == Step.texture: 
            self.ui.layer_pushButton.clicked.connect(partial(self.setup_layer, Step.texture))

        if self.entity.step == Step.rig: 
            self.ui.hiddenPly_pushButton.clicked.connect(self.set_hidden)
            self.ui.setupLookLayers_pushButton.clicked.connect(self.setup_looks)

        if self.entity.step == Step.lookdev: 
            self.ui.layer_pushButton.clicked.connect(partial(self.setup_layer, Step.lookdev))

        if self.entity.step == Step.groom: 
            self.ui.layer_pushButton.clicked.connect(partial(self.setup_layer, Step.groom))

        if self.entity.step == Step.anim: 
            self.ui.layer_pushButton.clicked.connect(partial(self.setup_layer, Step.lookdev))
            self.ui.assignShade_pushButton.clicked.connect(partial(assign_shader, self.entity))


    def setup_layer(self, step=''): 
        from rf_maya.lib import material_layer 
        reload(material_layer)
        tasks = sg_process.list_looks(self.entity.context.sgEntity)
        looks = [a.get('sg_name') for a in tasks]

        # guessing geo_grp 
        geoGrp = self.entity.projectInfo.asset.geo_grp()
        targets = mc.ls('*:%s' % geoGrp)
        mc.select(targets) if targets else None
        
        for look in looks: 
            material_layer.create_layer(look, mode=step)

    def set_smooth(self): 
        from rf_app.model.smooth_data import app
        reload(app)
        app.show()

    def set_hidden(self): 
        from rf_app.model.geo import geo_vis as app
        reload(app)
        app.show()

    def setup_looks(self):
        from rf_maya.lib import material_layer 
        reload(material_layer)
        from rf_utils.sg import sg_process
        from rf_maya.rftool.shade import shade_utils
        
        tasks = sg_process.list_looks(self.entity.context.sgEntity)
        looks = [a.get('sg_name') for a in tasks]
        
        for look in looks: 
            material_layer.create_layer(look, mode='rig')

        layers = material_layer.list_layer_info(self.entity)
        if layers:
            # assign look to each layer
            for lyr, data in layers.iteritems():
                # set current renderlayer 
                mc.editRenderLayerGlobals(crl=lyr)

                shadeFile = data['path']
                # print shadeFile
                if not os.path.exists(shadeFile):
                    continue

                shadeFileExt = os.path.basename(shadeFile)
                shadeFileName, ext = os.path.splitext(shadeFileExt)
                outputList = self.entity.guess_outputKey(shadeFileExt)
                if outputList:
                    # --- assign shaders
                    outputKey, step = outputList[0]
                    self.entity.context.update(look=lyr)
                    self.entity.extract_name(os.path.basename(shadeFile), outputKey=outputKey)
                    shadeNamespace = self.entity.mtr_namespace
                    if '_texture' in shadeNamespace:
                            shadeNamespace = self.entity.mtr_namespace.replace('_textureMtr','Mtr')
                    if '_preview' in shadeNamespace:
                            shadeNamespace = self.entity.mtr_namespace.replace('_previewMtr','Mtr').replace('mtl','mdl')
                    if '_lookdev' in shadeNamespace:
                            shadeNamespace = self.entity.mtr_namespace.replace('_lookdevMtr','Mtr').replace('mtl','ldv')
                    geoGrpName = self.entity.projectInfo.asset.geo_grp()
                    geoGrps = mc.ls('*:%s' %(geoGrpName), type='transform')
                    if not geoGrps:
                        geoNamespace = ''
                    else:
                        geoGrpNsSplits = geoGrps[0].split(':')
                        geoNamespace = ':'.join(geoGrpNsSplits[:-1])
                    shade_utils.apply_ref_shade(shadeFile=shadeFile,
                                    shadeNamespace=shadeNamespace, 
                                    targetNamespace=geoNamespace,
                                    connectionInfo=[])
                    
            # get back to masterLayer
            mc.editRenderLayerGlobals(crl='defaultRenderLayer')

def setup_layer(entity, step=''): 
    from rf_maya.lib import material_layer 
    reload(material_layer)
    tasks = sg_process.list_looks(entity.context.sgEntity)
    looks = [a.get('sg_name') for a in tasks]

    for look in looks: 
        material_layer.create_layer(look, mode=step)

def assign_shader(entity): 
    from rf_app.publish.asset import varaint_look_miarmy
    from rf_utils import register_entity 
    
    reg = register_entity.Register(entity)
    list_look = reg.get_looks('lookdev', 'md')
    asset_name = entity.name
    # assume one rig grp in the scene 
    geoGrp = entity.projectInfo.asset.geo_grp()
    targets = mc.ls('*:%s' % geoGrp)
    if targets: 
        target = targets[0]
        target_namespace = mc.referenceQuery(target, ns=True)

        # get data 
        shade_data = varaint_look_miarmy.list_shade_path(entity, list_look)
        logger.debug(shade_data)
        logger.debug(asset_name)
        logger.debug(target_namespace)
        logger.debug('varaint_look_miarmy.assign_shader_layer(entity, shade_data, asset_name, target_namespace)')
        # assign through each layer
        varaint_look_miarmy.assign_shader_layer(entity, shade_data, asset_name, target_namespace)


def show(entity=None):
    from rftool.utils.ui import maya_win
    maya_win.deleteUI(uiName)
    myApp = AssetSetup(entity, maya_win.getMayaWindow())
    myApp.show()
    return myApp