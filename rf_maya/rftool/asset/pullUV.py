import os, sys
import argparse
import logging
import subprocess

import pymel.core as pm
from maya.api import OpenMaya as om

from rf_utils import cask
from rf_utils.pipeline import check
reload(check)
from rf_utils import file_utils
from rf_maya.rftool.utils import maya_utils

from nuTools import misc


logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

def setup_parser(title):
    parser = argparse.ArgumentParser(description=title, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-p', dest='path', type=str, help='The path to maya scene to open', default='')
    return parser

def main(geoGrp, abcPath):
    # perform geo and topology check
    checker = check.CheckGeo(checkGrp=geoGrp, inputData=abcPath)
    checker.check()

    # if not all geo matched the ABC, only
    result = True
    if checker.result['missing'] or checker.result['alien'] or checker.result['mismatch topology']:
        om.MGlobal.displayWarning('Not all geo matches the ABC.\n{}'.format(checker.result))
        result = False

    # open the abc archive
    arc = cask.Archive(abcPath)
    for abc, fp in zip(checker.abc_paths, checker.result['match']):
        # skip if obj has no shapes
        curr_tr = pm.PyNode(fp)
        if curr_tr.nodeType() != 'transform':
            continue

        curr_mesh = curr_tr.getShape(ni=True)
        if not curr_mesh:
            continue

        if pm.objExists(curr_tr+'.ignorePullUV'):
            if pm.getAttr(curr_tr +'.ignorePullUV'):
                continue

        # check for alembic input connection
        inMeshConnections = curr_mesh.inMesh.inputs()
        if any([inc for inc in inMeshConnections if inc.nodeType()=='AlembicNode']):
            print('Skipping ABC mesh: {}'.format(curr_mesh.shortName()))
            continue

        arc_tr = arc.top.children[abc.replace('|', '/')]
        # print abc, fp, arc_tr
        src_mesh = arc_tr.children.values()[0]

        # --- get UV values from source abc file
        uv_props = None
        try:  # has to be safe in case the src geo has no UV
            uv_props = src_mesh.properties['.geom/uv']
        except: 
            pass

        toSet = None
        origShp = misc.getOrigShape(obj=curr_tr, includeUnuse=False)

        if origShp:
            origShp.setIntermediate(False)
            toSet = origShp
        else:
            toSet = curr_mesh

        if uv_props:
            faceCounts = list(src_mesh.properties['.geom/.faceCounts'].get_value())
            indices = list(uv_props.properties['.indices'].get_value())
            vals = list(uv_props.properties['.vals'].get_value())

            # convert indices into format pymel can accept via assignUVs
            uv_indices = []
            i = 0
            for fi in faceCounts:
                uv_range = indices[i:i+fi][::-1]
                uv_indices.extend(uv_range)
                i += fi

            # convert vals into format pymel can accept via setUVs
            us, vs = [], []
            for u, v in vals:
                us.append(u)
                vs.append(v)
            # apply the uv to current mesh
            try:
                toSet.clearUVs()
                toSet.setUVs(us, vs)
                toSet.assignUVs(faceCounts, uv_indices)
            except Exception as e:
                pass
  
            # delete history
            #pm.delete(toSet, ch=True)
            
            # force refresh the shape
            pm.dgdirty(curr_mesh.inMesh)

        else:
            # remove uv from the current mesh
            toSet.clearUVs()

        # turn intermediate back on
        if origShp:
            toSet.setIntermediate(True)
            
    arc.close()
    return result

def toRig(entity=None):
    from rf_utils.context import context_info
    from rf_utils import register_entity

    entity = context_info.ContextPathInfo(path=str(pm.sceneName())) if not entity else entity
    reg = register_entity.Register(entity)
    abcPath = reg.get.cache().get('heroFile')
    geoGrp = entity.projectInfo.asset.md_geo_grp()

    result = True
    print geoGrp
    if abcPath: 
        if not os.path.exists(abcPath):
            om.MGlobal.displayError('Abc does not exists: {}'.format(abcPath))
            result = False
        if not pm.objExists(geoGrp):
            om.MGlobal.displayError('Cannot find: {}'.format(geoGrp))
            result = False
        
        if result:
            result = main(geoGrp, abcPath)

    return result

def mergeUV(path):
    pm.openFile(path, f=True)

    result = toRig()
    if result:
        p, ext = os.path.splitext(path)

        if ext == '.mb':
            new_ext = '.ma'
        elif ext == '.ma':
            new_ext = '.mb'

        new_path = p + new_ext
        pm.saveFile(f=True)
        pm.saveAs(new_path, f=True)

    return result

def mergeUV_batch(path): 
    mayapy = maya_utils.pyinterpreter()
    py = sys.modules[__name__].__file__
    fileCheck = file_utils.ExportCheck(path)

    with fileCheck: 
        si = subprocess.STARTUPINFO()
        si.dwFlags |= subprocess.STARTF_USESHOWWINDOW
        p = subprocess.Popen([str(mayapy), str(py), '-p', str(path)], 
                stdout=subprocess.PIPE, 
                stderr=subprocess.STDOUT, 
                startupinfo=si)
        stdout, stderr = p.communicate()
        if stderr: 
            logger.debug(stdout)
            logger.error(stderr)

    return fileCheck.result

if __name__ == "__main__":
    parser = setup_parser(':::Replace Rig:::')
    params = parser.parse_args()

    result = mergeUV(path=params.path)


'''
"C:/Program Files/Autodesk/Maya2017/bin/mayapy.exe" "D:/dev/core/rf_maya/rftool/asset/pullUV.py" -p <path>
'''