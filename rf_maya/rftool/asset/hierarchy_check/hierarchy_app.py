# --------------------------------------------------------------------------
# nameChecker.py : python script
# --------------------------------------------------------------------------
#
# DESCRIPTION:
#  Rename duplicated object automatically by put digit at the end of the name
#
# REQUIRES:
#  Maya 2012 and newer
#
#
# INSTALLATION AND RUNNING:
#  put nameChecker.py into scripts directory
#  run code below 
# -----------------------
#  import nameChecker
#  nameChecker.run()

# -----------------------
#
# AUTHORS:
#  Chanon Vilaiyuk
#  ta.animator@gmail.com

# Code Begin # --------------------------------------------------------------------------

# v.0.0.1 library wip
_title = 'RF Hierarchy Check'
_version = 'v.0.0.1'
_des = 'beta'
uiName = 'HierarchyCheckUI'

import os, sys 
from functools import partial
import maya.cmds as mc
import maya.mel as mm
moduleDir = os.path.dirname(sys.modules[__name__].__file__)

import logging

#Import GUI
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

from Qt import wrapInstance
from Qt import _QtUiTools

#Import maya commands
import maya.cmds as mc
import maya.mel as mm
import maya.OpenMayaUI as mui


# import ui
from rftool.utils.ui import load
from rftool.utils import maya_utils

# If inside Maya open Maya GUI
def getMayaWindow():
    ptr = mui.MQtUtil.mainWindow()
    return wrapInstance(long(ptr), QtWidgets.QWidget)
    # return sip.wrapinstance(long(ptr), QObject)


def show():
    deleteUI(uiName)
    myApp = RFHierarchyChecker(getMayaWindow())
    # myApp.ui.show()
    return myApp

def deleteUI(ui):
    if mc.window(ui, exists=True):
        mc.deleteUI(ui)
        deleteUI(ui)


class Color: 
    green = [0, 200, 0]
    red = [200, 0, 0]
    grey = [100, 100, 100]
    yellow = [200, 200, 0]


class RFHierarchyChecker(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        self.count = 0
        #Setup Window
        super(RFHierarchyChecker, self).__init__(parent)
        # self.ui = ui.Ui_RFHierarchyCheckerUI()
        uiFile = '%s/ui.ui' % moduleDir
        self.ui = load.loadUIMaya(uiFile, self)
        self.ui.show()
        self.ui.setWindowTitle('%s %s - %s' % (_title, _version, _des))

        # cache 
        self.compareData = []
        self.targetData = []

        self.init_signals()


    def init_signals(self): 
        self.ui.compareNode_lineEdit.returnPressed.connect(self.list_node1)
        self.ui.targetNode_lineEdit.returnPressed.connect(self.list_node2)
        self.ui.compare_pushButton.clicked.connect(self.compare)
        self.ui.getSrcNode_pushButton.clicked.connect(self.get_source)
        self.ui.getTargetNode_pushButton.clicked.connect(self.get_target)


    def get_source(self): 
        sels = mc.ls(sl=True, l=True)
        if sels: 
            self.ui.compareNode_lineEdit.setText(sels[0][1:])
            self.list_node1()

    def get_target(self): 
        sels = mc.ls(sl=True, l=True)
        if sels: 
            self.ui.targetNode_lineEdit.setText(sels[0][1:])
            self.list_node2()


    def list_node1(self): 
        node = str(self.ui.compareNode_lineEdit.text())
        data = maya_utils.hierarchy_data(node)
        self.list_data(self.ui.src_listWidget, data)

        return data


    def list_node2(self): 
        node = str(self.ui.targetNode_lineEdit.text())
        data = maya_utils.hierarchy_data(node)
        self.list_data(self.ui.target_listWidget, data)
        
        return data


    def list_data(self, widget, data): 
        widget.clear()
        for obj in data: 
            widget.addItem(obj)


    def read_data(self, widget) :
        if widget.count() > 0: 
            return [str(widget.item(a).text()) for a in range(widget.count())]


    def compare(self): 
        """ compare both data """ 
        namespace = self.detect_namespace(str(self.ui.targetNode_lineEdit.text()))
        data = self.list_node1()
        targetData = self.list_node2()
        targetData = maya_utils.remove_data_namespace(namespace, targetData)

        adds, removes = maya_utils.compare_hierarchy(data, targetData)
        comList = combine_data(targetData, data)

        self.ui.src_listWidget.clear()

        i = 0
        # loop through target
        for obj in targetData: 
            # missing obj (Green)
            print 'loop', obj
            if not obj in data: 
                self.add_item(self.ui.src_listWidget, obj, Color.green)

            # match object
            else: 
                # compare if match the order
                ii = i
                while not (data[ii] == obj): 
                    if not data[ii] in targetData: 
                        # add obj that doesn't match (Red)
                        self.add_item(self.ui.src_listWidget, data[ii], Color.red)
                        i+=1 # skip check item
                        ii+=1

                    else: 
                        self.add_item(self.ui.src_listWidget, data[ii], Color.yellow)
                        ii+=1

                # add item that match order
                self.add_item(self.ui.src_listWidget, obj, None)

                i += 1

        if removes: 
            for obj in removes: 
                if not obj in self.read_data(self.ui.src_listWidget): 
                    self.add_item(self.ui.src_listWidget, obj, Color.red)


    def detect_namespace(self, name): 
        namespace = ''
        if ':' in name: 
            if name[0] == '|': 
                name = name[1:]
            namespace = name.split(':')[0]        
        return '%s:' % namespace 

    def add_item(self, widget, text, color=None): 
        item = QtWidgets.QListWidgetItem(widget)
        item.setText(text)
        
        if color: 
            item.setForeground(QtGui.QColor(color[0], color[1], color[2]))



def combine_data(data1, data2):
    """ combine data2 to data1 """ 

