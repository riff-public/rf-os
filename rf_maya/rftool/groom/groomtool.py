#GroomTool v.3.2.9      add massage  "done!"
                        #add many groom and guideset 
#GroomTool v.3.2.8      add menu "Change Geo"
#GroomTool v.3.2.7      add guideset
#GroomTool v.3.2.7      convert curve rename
#GroomTool v.3.2.6      settiong tool update form toolsetting
#GroomTool v.3.2.5      redioIconbush
#GroomTool v.3.2.4      try and except
#GroomTool v.3.2.3      rebuild curve
#GroomTool v.3.2.2      sp set publish and set tipbase
#GroomTool v.3.2.1      add all bush
#                      add isolate strand
#GroomTool v.3.2        edit attribute paint
#GroomTool v.3.1        add attribute, workflow
#GroomTool v.3        newbush tab
#GroomTool v.2.14    rebuild menu
#                   def Geo to node
#                   def add node shader<has script>
#GroomTool v.2.13    copy other mesh has copy attribute
#                    create groom Def not groom
#GroomTool v.2.12    remove Attribute Groom
#GroomTool v.2.11    dif convert Groom to curve
import maya.cmds as mc
import maya.mel as mel
import sys
from functools import partial


class GroomTool(object):
    def __init__(self, parent=None):
        #Setup Window
        super(GroomTool, self).__init__()

    def GroomUI(self, *args):
        mel.eval('pgYetiSetGroomContext();')
        WinUI="RiffGroomTool"
        if mc.window(WinUI,ex=True):
            mc.deleteUI(WinUI)
        window = mc.window(WinUI,title="RiFF_GroomTool(V.3.2.9)", iconName='Short Name',h=10,w=10,tlc=[550,40],rtf=True)
        form = mc.formLayout()
        tabs = mc.tabLayout(innerMarginWidth=10, innerMarginHeight=10)
        mc.formLayout( form,edit=True,attachForm=(tabs, 'top', 0) )
        child1 = mc.rowColumnLayout('main',nc=2,cal=[(1,"center"),(2,"center")],cw=[(1,15),(2,195)])
        self.outlineUI()
        mc.setParent( '..' )
        child2 = mc.rowColumnLayout(numberOfColumns=3) 
        self.bushUI()
        mc.setParent( '..' )
        child3 = mc.rowColumnLayout(numberOfColumns=2)
        self.publishUI()
        mc.setParent( '..' )
        mc.tabLayout( tabs, edit=True, tabLabel=((child1, '-outliner-'), (child2, '-Bush-'),(child3, '-Publish-')),h=380 )
        mc.showWindow()

    ###################function##############
    def outlineUI(self, *args):
        mc.rowColumnLayout('mode',nc=1,cal=(1,"center"),cw=(1,15))
        b1 = mc.button(label="G\nE\nO",w=15,h=46)
        mc.text(label ="",h=5)
        b2 = mc.button(label="N\nO\nD\nE",w=15,h=161)
        mc.text(label ="",h=5)
        b3 = mc.button(label="G\nR\nO\nO\nM",w=15,h=115)
        mc.text(label ="",h=7)
        #b4 = mc.button(label="R\nI\nG",w=15,h=45)
        mc.setParent( '..' )
        
        mc.rowColumnLayout('coOutline',nc=1,cal=(1,"center"),cw=(1,195))
        mc.button( label='SetGeo',command=self.setgeoGroom ,ann="1.selectGeo\n-set GeoSubdiv\n-set yetiSubdiv\n-rename preset " ,bgc=[1,.9,.9])
        mc.button( label='Create texture Ref',command=('mc.CreateTextureReferenceObject()'),ann="1.selectGeo",bgc=[1,.9,.9])
        mc.text(label ="",h=5)

        mc.button( label='Open graph',command=self.refreshYetiGraph,ann="open yeti graph edittor "  ,bgc=[0.9,1,0.8])
        mc.button( label='Create Node on Mesh',command=self.CreNode,ann="1.selectGeo\n-createYetiNode\n-createNode in yetiGraph "  ,bgc=[0.9,1,0.8])
        mc.button( label='Add Geo',command=self.addGeo,bgc=[.85,1,1],ann="1.selectGeo 2.selectNode\nShortcut:add Geo to Yetinode" )
        mc.button( label='Add Groom',command=self.addGroom,bgc=[.85,1,1],ann="1.selectGroom 2.selectNode\nShortcut:add yetiGroom to Yetinode" )
        mc.button( label='Add GuideSet',command=self.addGruidset,bgc=[.85,1,1],ann="1.select Guideset 2.selectNode\nShortcut:add yetiGroom to Yetinode" )
        mc.button( label='Add Node Preset',command=self.CreNodePreset,ann="1.selectNode\ncreate basic node in graph(import,scater,grow,comb and clumping)",bgc=[0.9,1,0.8] )
        mc.button( label='Add Node shader',command=self.creShaderIDNode,ann="1.selectNode\ncreate attribute<Shader_ID>\ncreate shader<has scirpt shader>)\n0=Purple\n1=Blue\n2=Green\n3=Yellow\n4=Orange",bgc=[0.9,1,0.8] )
        mc.text(label ="",h=5)

        mc.button( label='Create Groom On Mesh',command=self.CreGroom,ann="1.selectGeo",bgc=[.85,1,1] )
        mc.button( label='Copy Groom',command=self.copyGroom,ann="1.selectGroom\ncoppy Groom to each mesh and connect time ",bgc=[.85,1,1] )
        mc.button( label='Change mesh',command=self.changeGeo,ann="1.select Groom 2.select Other mesh\nlink worldmesh[0] with outmesh ",bgc=[.85,1,1] )
        mc.button( label='Copy Groom to other mesh',command=self.copyGroom2Other,ann="1.select Groom 2.select Other mesh\ndublicate Groom ,link worldmesh[0],link current time",bgc=[.85,1,1] )
        
        mc.button( label='Groom To Curve',command=self.Groom2Curv,ann="1.select all curve 2.select Geo\nConvert Cuves to Groom and remove all Curve",bgc=[.85,1,1] )
        
        mc.rowColumnLayout('coCurve',nc=3,cal=[(1,"center"),(2,"center"),(3,"left")],cw=[(1,50),(2,50),(3,95)])
        mc.text(label="stepSize")
        mc.textField('stSize',tx=0.2)
        mc.button( label='Curve To Groom',command=self.Curve2Groom,ann="1.select curve set 2.select Geo\nConvert Cuves to Groom and remove all Curve",bgc=[.85,1,1] )
        mc.setParent( '..' )
        
        mc.text(label ="",h=5)

        #mc.button( label='  convert curve publish  ',command=('GroomSetPublish()'),bgc=[.9,0.9,1],ann='1.selectGroom\n2.selectNode\n- set groom to curv\n- delete strand set\n- create new set\n- add curve to new set\n- rename curve\n- add guide set to yetinode\n- set "number of guide influences =1"\n- create group curv (*CRV_Grp)\n- saveGuidesRestPosition\n-set tipbase Attraction\n-create node\n-fiber to strand(convert)' )
        #mc.button( label='setTip/base attraction ',command=('setTipBase()'),ann="1.selectCurve set\nset tip/base attraction all curv in set ",bgc=[.85,1,1] )

        mc.setParent( '..' )
        
    def bushUI(self, *args):
        #get from tool setting
        pgAtt=mc.pgYetiGroomCtxCommand( 'pgYetiGroomCtxCommand1',q=True,paintValue=True )
        pgObey=mc.pgYetiGroomCtxCommand( 'pgYetiGroomCtxCommand1',q=True,obeySurfaceNormal=True )
        pgMirror=mc.pgYetiGroomCtxCommand ('pgYetiGroomCtxCommand1',q=True, brushMirrorAxis= True )
        if pgMirror!="None":thisMirror=True
        else :thisMirror=False
        pgUpdate=mc.pgYetiGroomCtxCommand ('pgYetiGroomCtxCommand1',q=True, updateFrequency = True )
        if pgUpdate=="Always":thisUpdate=True
        else:thisUpdate=False
        
        
        mc.rowColumnLayout('all',nc=1,cw=(1,210))#start main column
        mc.frameLayout('FrameSetting' ,label='setting Tools')#start Framne setting
        mc.gridLayout( numberOfColumns=3, cellWidthHeight=(60, 20) )
        mc.checkBox(  label='mirror' ,ofc='mirOff()',onc='mirOn()',v=thisMirror)
        mc.checkBox(  label='Obey',v=pgObey,ofc='mel.eval("pgYetiGroomCtxCommand -e -obeySurfaceNormal 0 pgYetiGroomCtxCommand1;")',onc='mel.eval("pgYetiGroomCtxCommand -e -obeySurfaceNormal 1 pgYetiGroomCtxCommand1;")')
        mc.checkBox(  label='Update ',v=thisUpdate,ofc='updateOff()',onc='updateOn()' ) 
        mc.button(  label='isolate ' ,c='isolateGroom()') 
        mc.setParent( '..' )
        mc.setParent( '..' )#end Framne setting
        
        
        
        mc.iconTextRadioCollection(  )#start icontex
        mc.frameLayout( label='blush' )#start Frame bush
        mc.gridLayout( numberOfColumns=5, cellWidthHeight=(40, 40) )
        mc.iconTextRadioButton( st='iconOnly', i1='pgYeti_add.png',onc='YB(2,0)' )
        mc.iconTextRadioButton( st='iconOnly', i1='pgYeti_delete.png' ,onc='YB(2,1)' )
        mc.iconTextRadioButton( st='iconOnly', i1='pgYeti_scatter.png',onc='YB(2,2)')
        mc.iconTextRadioButton( st='iconOnly', i1='pgYeti_comb.png',onc='YB(0,0)')
        mc.iconTextRadioButton( st='iconOnly', i1='pgYeti_direction.png',onc='YB(0,1)')
        mc.iconTextRadioButton( st='iconOnly', i1='pgYeti_straighten.png',onc='YB(0,2)')
        mc.iconTextRadioButton( st='iconOnly', i1='pgYeti_smooth.png',onc='YB(0,3)')
        mc.iconTextRadioButton( st='iconOnly', i1='pgYeti_lift.png',onc='YB(0,4)')
        mc.iconTextRadioButton( st='iconOnly', i1='pgYeti_move.png',onc='YB(0,5)')
        mc.iconTextRadioButton( st='iconOnly', i1='pgYeti_scale.png',onc='YB(0,6)')
        mc.iconTextRadioButton( st='iconOnly', i1='pgYeti_clumping.png',onc='YB(0,7)')
        mc.iconTextRadioButton( st='iconOnly', i1='pgYeti_twist.png',onc='YB(0,8)')
        mc.iconTextRadioButton( st='iconOnly', i1='pgYeti_tangent.png',onc='YB(0,9)')
        mc.iconTextRadioButton( st='iconOnly', i1='pgYeti_sculpt.png',onc='YB(3,0)')
        mc.iconTextRadioButton( st='iconOnly', i1='pgYeti_trim.png',onc='YB(3,1)')
        mc.setParent( '..' )#end Frame bush
        
        mc.frameLayout( label='Attribute Paint Tools' )
        mc.floatSliderGrp("floatAtt",f=True,min=0,max=4,v=pgAtt,cw=[1,30])
        mc.gridLayout( numberOfColumns=6, cellWidthHeight=(35, 35) )
        mc.iconTextRadioButton( st='iconOnly', i1='pgYeti_attr_paint.png',onc='YBatt(1,2)' )
        mc.iconTextRadioButton( st='iconOnly', i1='pgYeti_attr_increase.png',onc='YBatt(1,0)')
        mc.iconTextRadioButton( st='iconOnly', i1='pgYeti_attr_decrease.png',onc='YBatt(1,1)')
        mc.iconTextRadioButton( st='iconOnly', i1='pgYeti_attr_smooth.png',onc='YBatt(1,4)')
        mc.iconTextRadioButton( st='iconOnly', i1='pgYeti_attr_randomize.png',onc='YBatt(1,5)')
        mc.iconTextRadioButton( st='iconOnly', i1='pgYeti_attr_sample.png',onc='YBattSample()')
        mc.setParent( '..' )
        mc.setParent( '..' ) 
        
        
        
        mc.setParent( '..' )#end icon tex
        
        
        
        mc.frameLayout( label='WorkFlow' )
        mc.gridLayout( numberOfColumns=6, cellWidthHeight=(35, 35) )
        mc.symbolButton( image='pgYeti_fill.png',c="mel.eval('pgYetiGroomCtxCommand -e -fill pgYetiGroomCtxCommand1')" )
        mc.symbolButton( image='pgYeti_deselect.png' ,c="mel.eval('pgYetiGroomCtxCommand -e -deselectAll pgYetiGroomCtxCommand1;')")
        mc.symbolButton( image='pgYeti_convert.png' ,c="mel.eval('pgYetiGroomCtxCommand -e -invertSelection pgYetiGroomCtxCommand1;')")
        mc.symbolButton( image='pgYeti_mirror.png',c="mel.eval('pgYetiGroomCtxCommand -e -mirrorStrands pgYetiGroomCtxCommand1;')" )
        mc.symbolButton( image='pgYeti_undo.png',c="mel.eval('pgYetiGroomCtxCommand -e -undo pgYetiGroomCtxCommand1;')" )
        mc.symbolButton( image='pgYeti_redo.png' ,c="mel.eval('pgYetiGroomCtxCommand -e -redo pgYetiGroomCtxCommand1;')")
        mc.setParent( '..' )
        mc.setParent( '..' )
        
        #
       
        mc.setParent( '..' )#endmain column
        
    def publishUI(self, *args):
        mc.rowColumnLayout('all',nc=1,cw=(1,210))
        mc.frameLayout( label='convert curve and set ',bgc=[0,0,0.6] )
        mc.text(l="",h=1)
        mc.button( label='convert curve publish',command=('GroomSetPublish()'),ann='1.selectGroom\n2.selectNode\n- set groom to curv\n- delete strand set\n- create new set\n- add curve to new set\n- rename curve\n- add guide set to yetinode\n- set "number of guide influences =1"\n- create group curv (*CRV_Grp)\n- saveGuidesRestPosition\n-set tipbase Attraction\n-create node\n-fiber to strand(convert)' )
        mc.button( label='setTip/base attraction ',command=('setTipBase()'),ann="1.selectCurve set\nset tip/base attraction all curv in set " )
        mc.text(l="",h=1)
        mc.setParent( '..' )
        
        mc.frameLayout( label='rebuild curves',bgc=[0,0,0.6])
        mc.rowColumnLayout('main',nc=2,cw=[(1,145),(2,110)])
        mc.rowColumnLayout('texcolumn',nc=2,cw=[(1,70),(2,70)])
        mc.text(l="current min")
        mc.text(l="current max")
        
        mc.intField('currentmin',v=0)
        mc.intField('currentmax',v=0)
        mc.floatField('lengthmin')
        mc.floatField('lengthmax')
        
        mc.text(l="new min")
        mc.text(l="new max")
        mc.intField('newmin',v=3)
        mc.intField('newmax',v=5)
        
        mc.setParent( '..' )#end texfield
        mc.rowColumnLayout('buttoncolumn',nc=1)
        mc.text(l="",h=5)
        mc.button('refresh',l="refresh",c=('getCurrentInfo()'),h=46)
        mc.text(l="",h=4)
        mc.button('rebuild',l="rebuild",c='rebuild()',h=30)
        mc.setParent( '..' )#buttoncolumn
        mc.setParent( '..' )
        mc.rowColumnLayout('buttoncolumn',nc=4)
        mc.text(l="smoothness : ")
        mc.textField('smoothField',tx=1,w=47)
        mc.text(l=" ")
        mc.button('smooth',l="Smooth Curves",c='smoothcurve()')
        mc.setParent( '..' )
        #mc.button(l="clean curve",c='cleancurve()')
        mc.setParent( '..' )

        
        
    def smsDone(self, *args):
        mc.warning("Done ~\(>  v  <)/~")  
        
    def addslider(self, *args):
        mc.floatSliderGrp("floatAtt",f=True,  min=0,max=4,cw=[1,30])
        value = mc.floatSliderGrp("floatAtt", q=True, v=True)
        mel.eval('pgYetiGroomCtxCommand -e -paintValue %s pgYetiGroomCtxCommand1;'%value)
        print(value)

    def seThis(self, *args):
        seall = mc.ls(sl=True) 
        se=seall[0]
        return se
        
    def seShape(self, *args):
        seall = mc.ls(sl=True) 
        se=seall[0]+"Shape"
        return se  
        
    def seArray(self, *args):
        se = mc.ls(sl=True) 
        return se
        
    def getShape(x):
        i=x+"Shape"
        return i
        
    def seCurveSet(self, *args):
        mc.select(r=True)
        
    def addName(self, *args):
        text=''
        result = mc.promptDialog(
                title='Rename Object',
                message='Enter Name:(Hair,HairFront,Hair_L)',
                text="Px",
                button=['OK', 'Cancel'],
                defaultButton='OK',
                cancelButton='Cancel',
                dismissString='Cancle')
            
        if result=='OK':
            text = mc.promptDialog(query=True, text=True)
            if text!="":
                if result!='Cancle':
                    return text 
            else :
                text='Px'
                mc.warning( 'add "proxy"' )
           
        else :text="notadd!"
        return text

    #########################################

       
    def setgeoGroom(self, *args):
        
        if len(mc.ls(sl=True))!=1: 
            mc.warning( "select Geo only" )
            sys.exit()
            
        else:
            partName=addName()
            if partName!='notadd!':
                mc.rename(seThis(),(partName+"Groom_Geo"))   
                yetiGeoSubd()
                mc.DeleteHistory
                mel.eval('delete -ch;')
        
    def yetiGeoSubd(self, *args):
        try: 
            mel.eval('displaySmoothness -divisionsU 3 -divisionsV 3 -pointsWire 16 -pointsShaded 4 -polygonObject 3;')
            mc.addAttr (seShape(),ln= "yetiSubdivision" ,at="bool" ,min=0 ,dv =1 ,k =True)
            self.smsDone()
        except:
            mc.warning( "select Geo only" )
        
    ####################################################
    def refreshYetiGraph(self, *args):
        #mel.eval('')---------open yetiGr

        
        mel.eval('pgYetiEnsurePluginLoaded();')
        mel.eval('pgYetiTearOffGraphPanel;')
        mel.eval('timeControl -edit -forceRedraw $gPlayBackSlider;')   
        mel.eval('updateAnimLayerEditor("AnimLayerTab");')
        mel.eval('statusLineUpdateInputField;')
        mel.eval('if (!`exists polyNormalSizeMenuUpdate`) {eval "source buildDisplayMenu";} polyNormalSizeMenuUpdate;')
        mel.eval('dR_updateCounter;')
        mel.eval('pgYetiUpdateAE;')
        mel.eval('autoUpdateAttrEd;')
        mel.eval('pgYetiMayaUI -graphEditorSelectionChangedCB;')
        mel.eval('pgYetiSelectionLocked;')
        mel.eval('dR_updateCommandPanel;')
        mel.eval('pgYetiDoAEUpdate;')

    def CreNode(self, *args):
        text=""
        try:
            se=mc.ls(sl=True)
            if len(se)!=1:
                text="select Geo"
                sys.exist()
            #mel.eval('')---------Creat Yetinode
            
            partName=addName()
            if partName=="notadd!":
                text=""
                sys.exist()
            mel.eval('pgYetiCreateOnMesh;')
            x=mc.select("pgYetiMaya*")
            mc.rename(x,(partName+"_YetiNode"))   
            mc.select((partName+"_YetiNode"))
            self.refreshYetiGraph()   
            #mel.eval('')-------creatNode
            mel.eval('pgYetiGraphCreate "import";')
            mel.eval('pgYetiGraphCreate "scatter";')
            mel.eval('pgYetiGraphCreate "grow";')
            mel.eval('pgYetiGraphCreate "comb";')
            mel.eval('pgYetiGraphCreate "clumping";')
            mel.eval('pgYetiGraphCreate "width";')
            mel.eval('pgYetiGraphCreate "shader";')
            mel.eval('pgYetiGraphCreate "attribute";')
            mel.eval('pgYetiGraphCreate "transform";')
            mel.eval('pgYetiGraphCreate "import";')
            mel.eval('pgYetiGraphCreate "convert";')
            #mel.eval('')-------linknode
            mel.eval('pgYetiGraph -node "%s" -connect "%s" 1 '%(BlastNode("import"),lastNode("convert")))
            mel.eval('pgYetiGraph -node "%s" -connect "%s" 1 '%(lastNode("import"),lastNode("grow")))
            mel.eval('pgYetiGraph -node "%s" -connect "%s" 1 '%(lastNode("import"),lastNode("comb")))
            mel.eval('pgYetiGraph -node "%s" -connect "%s" 1 '%(lastNode("convert"),lastNode("clumping")))
            #mel.eval('')-------setNode
            mel.eval('pgYetiGraph -node "%s" -param "densityMultiplier" -setParamValueScalar 10 '%lastNode("scatter"))
            mel.eval('pgYetiGraph -node "%s" -param "baseWidth" -setParamValueScalar 0.03 '%lastNode("width"))
            mel.eval('pgYetiGraph -node "%s" -param "tipWidth" -setParamValueScalar 0.01 '%lastNode("width"))
            mel.eval('pgYetiGraph -node "%s" -param "attractionBase" -setParamValueScalar 0 ;'%lastNode("clumping"))
            mel.eval('pgYetiGraph -node "%s" -param "maintainLength" -setParamValueBoolean 0 ;'%lastNode("clumping"))
            mel.eval('pgYetiGraph -node "%s" -param "attractionBias" -setParamValueScalar 0.5 ;'%lastNode("clumping"))
            mel.eval('pgYetiGraph -node "%s" -param "diffuseColor" -setParamValueExpr "ccellnoise(N*5)" '%lastNode("shader"))    
            mel.eval('pgYetiGraph -node "%s" -param "addPref" -setParamValueBoolean 1'%lastNode("attribute")) 
            #mel.eval('')-------setRootnode
            mel.eval('pgYetiGraph  -setRootNode "%s"'%lastNode("shader"))
            self.smsDone()
        except:
            if(text!=""):mc.warning( text)
            
        

    def addGeo(self, *args):  
        try:  
            getGeoNode=seArray()
            getGeo=getShape(getGeoNode[0])
            getNode=getShape(getGeoNode[1])
            mel.eval('pgYetiAddGeometry("%s", "%s");'%(getGeo,getNode))
            mc.select(getGeo)
            mel.eval(' HighQualityDisplay;')
            mel.eval('setDisplaySmoothness 3;')
            mel.eval('displaySmoothness -divisionsU 3 -divisionsV 3 -pointsWire 16 -pointsShaded 4 -polygonObject 3;')
            mel.eval('objectDetailsBackfaces();')
            mel.eval('objectDetailsSmoothness();')
            mel.eval('objectDetailsBackfaces();')
            mel.eval('objectDetailsSmoothness();')  
            mel.eval('addAttr -ln "yetiSubdivision"  -at bool  -min 0 -dv 1 -k 1 "%s";'%getGeo)
            self.smsDone()
            
        except:
            mc.warning("1.select *Groom_Geo(or rename mesh to *Groom_Geo),2.select *_YetiNode/or is already connected")  
        
    def addGroom(self, *args):
        try:    
            seAll=seArray()
            getNode=seAll[len(seAll)-1]
            for i in range(len(seAll)-1):
                mel.eval('pgYetiAddGroom("%s", "%s");'%(seAll[i],getNode))
            self.smsDone()
        except:
            mc.warning("1.select *_YetiGroom,2.select *_YetiNode/or is already connected")
            
    def addGruidset(self, *args):
        try:    
            seAll=seArray()
            getNode=seAll[len(seAll)-1]
            for i in range(len(seAll)-1):
                mel.eval('pgYetiAddGuideSet("%s", "%s");'%(seAll[i],getNode))
            self.smsDone()
            
        except:
            mc.warning("1.select Guideset,2.select *_YetiNode/or is already connected")
        
    def CreNodePreset(self, *args):
        try:

            if len(mc.ls(sl=True))!=1: sys.exist()
            self.refreshYetiGraph()
            #mel.eval('')-------creatNode
            mel.eval('pgYetiGraphCreate "import";')
            mel.eval('pgYetiGraphCreate "scatter";')
            mel.eval('pgYetiGraphCreate "grow";')
            mel.eval('pgYetiGraphCreate "comb";')
            mel.eval('pgYetiGraphCreate "clumping";')
            mel.eval('pgYetiGraphCreate "width";')
            mel.eval('pgYetiGraphCreate "import";')
            mel.eval('pgYetiGraphCreate "convert";')
            #connect
            mel.eval('pgYetiGraph -node "%s" -connect "%s" 1 '%(BlastNode("import"),lastNode("convert")))
            mel.eval('pgYetiGraph -node "%s" -connect "%s" 1 '%(lastNode("import"),lastNode("grow")))
            mel.eval('pgYetiGraph -node "%s" -connect "%s" 1 '%(lastNode("import"),lastNode("comb")))
            mel.eval('pgYetiGraph -node "%s" -connect "%s" 1 '%(lastNode("convert"),lastNode("clumping")))
            #set node
            mel.eval('pgYetiGraph -node "%s" -param "densityMultiplier" -setParamValueScalar 50 '%lastNode("scatter"))
            mel.eval('pgYetiGraph -node "%s" -param "baseWidth" -setParamValueScalar 0.03 '%lastNode("width"))
            mel.eval('pgYetiGraph -node "%s" -param "tipWidth" -setParamValueScalar 0.01 '%lastNode("width"))
            mel.eval('pgYetiGraph -node "%s" -param "attractionBase" -setParamValueScalar 0 ;'%lastNode("clumping"))
            mel.eval('pgYetiGraph -node "%s" -param "maintainLength" -setParamValueBoolean 0 ;'%lastNode("clumping"))
            mel.eval('pgYetiGraph -node "%s" -param "attractionBias" -setParamValueScalar 0.5 ;'%lastNode("clumping"))
            mel.eval('pgYetiGraph  -setRootNode "%s"'%lastNode("width"))
            self.smsDone()
        except:
            mc.warning("select YetiNode")
        
    def creShaderIDNode(self, *args):
        try:
            if len(mc.ls(sl=True))!=1: sys.exist()
            self.refreshYetiGraph()   
            mel.eval('pgYetiGraphCreate "attribute";')
            mel.eval('pgYetiGraphCreate "shader";')
            mel.eval('pgYetiGraph -node "%s" -param "diffuseColor" -setParamValueExpr "$purple=[0.3, 0, 0.5];$blue=[0,0.6,1];$green=[0.3,0.6,0];$yellow=[0.980392, 1, 0.607843];$orange=[1, 0.333333, 0];$shader_ID<=0.1?$purple:$shader_ID<=1.1?$blue:$shader_ID<=2.1?$green:$shader_ID<=3.1?$yellow:$orange" '%lastNode("shader"))
            mel.eval('pgYetiGraph -node "%s" -param "attribute" -setParamValueString "shader_ID" '%lastNode("attribute"))
            mel.eval('pgYetiGraph -node "%s" -param "sampleGroom" -setParamValueBoolean 1 '%lastNode("attribute"))
            self.smsDone()
        except:
            mc.warning("select YetiNode")    

        
    def lastNode(x):
        typeNode=x
        All=mel.eval('pgYetiGraph  -listNodes -type "%s";'%typeNode)
        numNode=len(All)
        seIm=All[numNode-1]
        return seIm
        
    def BlastNode(x):
        typeNode=x
        All=mel.eval('pgYetiGraph  -listNodes -type "%s";'%typeNode)
        numNode=len(All)
        if All==None:
            print "No Node"  
        if numNode==1:
            seIm=All[numNode-1]
            return  seIm
        if numNode>=2:
            seIm=All[numNode-2]
            return  seIm

    def setguidenode(self, *args):
        mel.eval('pgYetiGraphCreate "import";')
        mel.eval('pgYetiGraphCreate "guide";')
        mel.eval('pgYetiGraphCreate "convert";')
        
        Limport=lastNode("import")
        Lguide=lastNode("guide")
        Lconvert=lastNode("convert")
        
        mel.eval('pgYetiGraph -node "%s" -disconnect 0  ;'%Lguide)
        mel.eval('pgYetiGraph -node "%s" -connect "%s" 1 ;'%(Limport,Lguide))
        mel.eval('pgYetiGraph -node "%s" -connect "%s" 0 ;'%(Lguide,Lconvert))
        
        mel.eval('pgYetiGraph -node "%s" -param "type" -setParamValueScalar 2'%Limport)
        mel.eval('pgYetiGraph -node "%s" -param "conversion" -setParamValueScalar 1'%Lconvert)    

      
    #############################################  

    def CreGroom(self, *args):
        text=''
        try:
            if len(mc.ls(sl=True))!=1: 
                text="select *Groom_Geo or rename mesh to *Groom_Geo"
                sys.exist()
            partName=addName()
            if partName=="notadd!":
                sys.exist()   
            mel.eval('pgYetiCreateGroomOnMesh;')    
            x=mc.select("pgYetiGroom*")
            mc.rename(x,(partName+"_YetiGroom"))
            self.smsDone()
        except:
            if(text!=""):mc.warning( text)
            
    def copyGroom(self, *args):
        try:
            getitems=mc.ls(sl=True)
            newname=("copy_"+getitems[0])
            mc.duplicate(getitems[0],ic=True,n=("COPY_"+getitems[0]),rr=True)
            self.smsDone()
        except:
            mc.warning("select *_YetiGroom")
            
    def changeGeo(self, *args):
        try:
            txtWarning="select *_YetiGroom,select Other Geo"
            seall=mc.ls(sl=True)
            if len(seall)==2:
                seGroom=seall[0]
                seGeo=seall[1]
                mel.eval('connectAttr -f ("%s"+"Shape.worldMesh[0]") ("%s"+".inputGeometry");'%(seGeo,seGroom))
                self.smsDone()
            else :
                sys.exist() 
        except:
            mc.warning(txtWarning)
            
    def copyGroom2Other(self, *args):
        try:
            seAll=mc.ls(sl=True)
            seGroom=seAll[0]
            seGeo=seAll[1]
            copyGroom()
            mel.eval('connectAttr -f ("%s"+"Shape.worldMesh[0]") ("COPY_"+"%s"+"Shape.inputGeometry");'%(seGeo,seGroom))
            self.smsDone()
        except:
            mc.warning("1.select *_YetiGroom  2.select *Groom_Geo or rename mesh to *Groom_Geo")
        
    def Groom2Curv(self, *args):
        try:
            seGroom=seThis()
            partGroom = seGroom.split("_") 
            mel.eval('pgYetiConvertGroomToCurves;')
            mc.delete(seGroom+"Shape_strand_set")
            crvs = mc.ls(seGroom+"Shape_strand_*" ,type="transform")
            if len(partGroom)==1:
                        sys.exist()
            if len(partGroom)==2:
                        newSet =mc.sets(crvs,name=("set_"+partGroom[0]+"_CRV"))
                        mc.group(crvs,n=(partGroom[0]+"CRV_Grp"))
                        for i in range(len(crvs)): 
                                mc.rename(crvs[i],partGroom[0]+str(i+1)+"_CRV")  
            if len(partGroom)==3:
                        newSet =mc.sets(crvs,name=("set_"+partGroom[0]+partGroom[1]+"_CRV"))
                        mc.group(crvs,n=(partGroom[0]+partGroom[1]+"CRV_Grp"))
                        for i in range(len(crvs)): 
                                mc.rename(crvs[i],partGroom[0]+partGroom[1]+str(i+1)+"_CRV")  
            self.smsDone()
        except:
            mc.warning("1.select *_YetiGroom ")     
        
    def Curve2Groom(self, *args): 
        text=""
        try:
            mel.eval('pgYetiCommand -removeGuidesRestPosition;') 
            sSize = mc.textField('stSize',q=True,tx=True )  
            seAll=seArray()
            if len(seAll)!=2:
                text="1.select curve set   2.select *Groom_Geo or rename mesh to *Groom_Geo"
            seSet=seAll[0]
            seGeo=seAll[1]
            partName=addName()
            if partName=="notadd!":
                sys.exist()  
            convert(seSet,seGeo,sSize) 
            mc.rename("pgYetiGroom",partName+"_YetiGroom")
            mc.select(partName+"_YetiGroomShape")
            removeAtt()
            self.smsDone()
        except:
            if(text!=""):mc.warning(text) 

                
               
    def convert(guide_set,mesh_object,stepSize):
        newPgYetiGroom=mel.eval('createNode pgYetiGroom')
        mel.eval('connectAttr -f ("%s"+"Shape.worldMesh[0]") ("%s"+".inputGeometry");'%(mesh_object,newPgYetiGroom))
        mel.eval('connectAttr -f time1.outTime ("%s"+".currentTime");'%newPgYetiGroom)
        #rename the parent and node
        transforms = mc.listRelatives(newPgYetiGroom,p=True)
        mc.rename(newPgYetiGroom, "tempPgYetiGroomName")
        transform_name = mc.rename(transforms[0],"pgYetiGroom")
        newPgYetiGroom = mc.rename("tempPgYetiGroomName" , transform_name + "Shape" );
        mel.eval('pgYetiCommand -convertFromCurves %s -inputGeometry %s -stepSize %s %s;'%(guide_set,mesh_object,stepSize,newPgYetiGroom))
        removeAtt()
            
     
    def removeAtt(self, *args):
        seAll=seArray()
        seSet=seAll[0]
        print seSet
        attrLists = ["tipAttraction", "density", "staticLength", "lengthWeight", "userVector", 
                "baseAttraction", "weight", "innerRadius", "outerRadius", "attractionBias",
                "twist", "randomAttraction", "userFloat","guideModel","randomTwist"];
                                
        for i in  range(len(attrLists)):
            mel.eval( 'pgYetiCommand -removeAttribute "%s" "%s";'%(attrLists[i],seSet))  
    ######################################################################################
    #-----------------tabbush  
    def YB(a,b):
        mel.eval('pgYetiSetGroomContext();')
        mel.eval('pgYetiContextSetBrush("%s", %s);'%(a,b))
        mel.eval('pgYetiGroomContextValues pgYetiGroomCtxCommand1;')
        
    def YBatt(a,b):
        value = mc.floatSliderGrp("floatAtt", q=True, v=True)
        mel.eval('pgYetiGroomCtxCommand -e -paintValue %s pgYetiGroomCtxCommand1;'%value)
        YB(a,b)
        
    def YBattSample(self, *args):
        YB(1,6)
        value=mc.pgYetiGroomCtxCommand( 'pgYetiGroomCtxCommand1',q=True,paintValue=True )
        print 
        mc.floatSliderGrp("floatAtt", e=True, v=value)

        
          
        
         

    def mirOff(self, *args): mel.eval('pgYetiGroomCtxCommand -e -brushMirrorAxis "None" pgYetiGroomCtxCommand1;')
    def mirOn(self, *args): mel.eval('pgYetiGroomCtxCommand -e -brushMirrorAxis "X" pgYetiGroomCtxCommand1;')   

    def updateOn(self, *args): mel.eval('pgYetiGroomCtxCommand -e -updateFrequency "Always" pgYetiGroomCtxCommand1;') 
    def updateOff(self, *args): mel.eval('pgYetiGroomCtxCommand -e -updateFrequency "On Release" pgYetiGroomCtxCommand1;')

    def isolateGroom(self, *args):
        try:
            seall=mc.ls(sl=True)
            a= str(seShape())
            this=mel.eval('getAttr ("%s"+".isolateSelectedStrands");'%a)
            if (this==1):
                
                mel.eval('setAttr ("%s"+".isolateSelectedStrands") 0;'%a)
            else:
               
                mel.eval('setAttr ("%s"+".isolateSelectedStrands") 1;'%a)
        except:
            mc.warning("select *_YetiGroom")          
              
    ##################################################################################### 
    def GroomSetPublish(self, *args):
        try:
        #---------variable------------
            seall = mc.ls(sl=True)
            seGroom = seall[0]
            seNode = seall[1]
            
            partGroom = seGroom.split("_")
            partG=partGroom[0]
            partS=partGroom[1]
            
            mel.eval("pgYetiConvertGroomToCurves")
            mc.delete(seGroom+"Shape_strand_set")
            crvs = mc.ls(seGroom+"Shape_strand_*" ,type="transform")
            #---------variable------------
            if len(partGroom)==2:
                mc.group(crvs,n=partG+"CRV_Grp")
                mc.xform(partG+"CRV_Grp",piv=[0,0,0])
                newSet =mc.sets(crvs,name=("set_"+partG+"_CRV"))
            
                for i in range(len(crvs)): 
                    mc.rename(crvs[i],str(partG)+str(i+1)+"_CRV")  
                
            else:
                mc.group(crvs,n=partG+partS+"CRV_Grp",w=True)
                newSet =mc.sets(crvs,name=("set_"+partG+"_"+partS+"_CRV"))
                for i in range(len(crvs)): 
                    mc.rename(crvs[i],str(partG)+str(i+1)+"_"+partS+"_CRV")
            #---------variable------------
            #mc.addAttr (newSet,ln= "maxNumberOfGuideInfluences" ,at= "long" ,min=0 ,dv =1 ,k =True)
            mel.eval('pgYetiAddGuideSet("%s","%s");'%(newSet,seNode))
            #mel.eval('setAttr ("%s"+".maxNumberOfGuideInfluences") 1;'%newSet)
            mc.select(newSet)
            mel.eval('pgYetiCommand -saveGuidesRestPosition;')
            #mc.select(newSet,r=True,ne=True)
            #setTipBase()
            mc.select(seNode)
            self.refreshYetiGraph()   
            self.setguidenode()
            #mc.select(newSet,r=True,ne=True)
            self.smsDone()
        except:
            mc.warning("1.select *_YetiGroom    2.select *YetiNode  -Or yetiGroom not have strand")        
      
    def setTipBase(self, *args): 
        try:
            se=mc.ls(sl=True)
            mel.eval('setAttr ("%s"+".maxNumberOfGuideInfluences") 1;'%se[0])
            seCurveSet()
            crv =mc.ls(sl=True,r=True)
            for i in range(len(crv)): 
               mel.eval('setAttr ("%s"+"Shape.tipAttraction") 1;'%crv[i])
               mel.eval('setAttr ("%s"+"Shape.baseAttraction") 1;'%crv[i])
            self.smsDone()
        except:
            mc.warning("1.select GuideSet")        

    def getCurrentInfo(self, *args):
        allCurves_list=mc.ls(sl=True)  
        curveSpan_list = []
        curveLength_list = [] 
        for curve in allCurves_list :

            curveShape = mc.listRelatives ( curve , shapes = True ) ;

            if curveShape != [] :
                curveShape = curveShape [0] ;
                curveSpan_list.append ( mc.getAttr ( curveShape + '.spans' ) ) ;
                curveLength_list.append ( mc.arclen ( curve ) )  
            else :
                pass ;

        curveSpan_list.sort() 
        curveLength_list.sort ( ) 
        mc.intField ( 'currentmin' , e = True , v = curveSpan_list[0] ) ;
        mc.intField ( 'currentmax' , e = True , v = curveSpan_list[-1] ) ;
        mc.floatField ( 'lengthmin' , e = True , v = curveLength_list[0] ) ;
        mc.floatField ( 'lengthmax' , e = True , v = curveLength_list[-1] ) ;
          
        
    def rebuild(self, *args):  
        seCurveSet()
        curvs = mc.ls(sl=True)
        currentminspan=mc.intField('currentmin',q=True,v=True)
        currentmaxspan=mc.intField('currentmax',q=True,v=True)  
        currentminlength=mc.floatField('lengthmin',q=True,v=True)
        currentmaxlength=mc.floatField('lengthmax',q=True,v=True)
        length_info =currentmaxlength-currentminlength

        minSpan=mc.intField('newmin',q=True,v=True)
        maxSpan=mc.intField('newmax',q=True,v=True)
        span_info = maxSpan - minSpan 
        for i in range(len(curvs)): 
            percentage = mc.arclen(curvs[i])
            percentage -= currentminlength
            try:
                percentage /= length_info 
            except ZeroDivisionError:
                percentage = 1

            span = span_info * percentage 

            span = round ( span ) 
            span += minSpan ;
            spam = int ( span ) 
            
            mc.rebuildCurve ( curvs[i] ,
                constructionHistory = 0 ,
                replaceOriginal     = 1 ,
                rebuildType         = 0 , # uniform
                endKnots            = 1 , # multiple end knots
                keepRange           = 0 ,
                keepControlPoints   = 0 ,
                keepEndPoints       = 1 ,
                keepTangents        = 0 ,
                spans               = spam,#edit
                degree              = 3 ,
                tolerance           = 0.01 ,
                ) 
        mc.select(curvs)
        
    def cleancurve(self, *args):
        mc.makeIdentity( apply=True, t=1, r=1, s=1, n=2 )
        mc.xform(piv=[0,0,0])
        mc.DeleteHistory


    def smoothcurve(self, *args):
        smoothness= mc.textField('smoothField',q=True,tx=True )  
        mel.eval('performSmoothCurvePreset 1 1 %s;'%smoothness)


# GroomUI()
