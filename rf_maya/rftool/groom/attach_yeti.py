import os

import maya.cmds as mc
import pymel.core as pm
import maya.mel as mel

from rf_utils.context import context_info
from rf_utils import register_shot
from rf_utils import register_entity

from rf_utils.pipeline import check
reload(check)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

def attach_yeti_to_anim():
    sel = mc.ls(sl=True)
    path_asset = mc.referenceQuery(sel[0], f=True)
    namespace_asset = mc.referenceQuery(sel[0], ns=True)
    asset = context_info.ContextPathInfo(path = path_asset)
    asset.context.update(step='groom',process='main',look='main', res='md')
    #####
    grm_dir_path =asset.path.scheme(key='publishHeroOutputPath').abs_path()
    grm_file = asset.output_name(outputKey='yeti',hero=True)
    gmr_ref_path = os.path.join(grm_dir_path, grm_file)
    #######
    grm_ns = '{name}_grm'.format(name=namespace_asset)
    if os.path.exists(gmr_ref_path):
        mc.file(gmr_ref_path, r=True, ns= grm_ns)
    grm_yetiGeo ='{grm_ns}:YetiGeo_Grp'.format(grm_ns= grm_ns)
    asset_yetiGeo = '{namespace_asset}:YetiGeo_Grp'.format(namespace_asset= namespace_asset)
    if mc.objExists(grm_yetiGeo) and mc.objExists(asset_yetiGeo):
        blend_node = mc.blendShape(asset_yetiGeo, grm_yetiGeo, origin='world', automatic=True)[0]
        mc.setAttr('{bs_node}.YetiGeo_Grp'.format(bs_node=blend_node), 1)

def attach_yeti_to_cache(obj=None, scene=None):
    if not obj:
        sels = pm.selected()
        if not sels:
            msg = 'No selection found'
            logger.warning(msg)
            return False, msg
        obj = sels[0]
    if isinstance(obj, (str, unicode)):
        obj = pm.PyNode(obj)

    if not obj.isReferenced():
        msg = 'Object is not referenced'
        logger.warning(msg)
        return False, msg

    if not scene:
        try:
            scene = context_info.ContextPathInfo(path=mc.file(q=True, sn=True)) 
        except:
            msg = 'Invalid scene path.'
            logger.warning(msg)
            return False, msg
        
    ns = obj.namespace()[:-1]
    reg = register_shot.Register(scene)

    # get TechGeo abc path
    try:
        abc_path = reg.get.cache('%s_Tech' %ns, hero=True)
    except:
        msg = 'Failed getting TechGeo ABC: %s_Tech' %ns
        # logger.warning(msg)
        return False, msg

    if not abc_path:
        msg = 'Failed getting TechGeo ABC: %s_Tech' %ns
        return False, msg

    if not os.path.exists(abc_path):
        msg = 'TechGeo ABC is missing: %s' % abc_path
        logger.warning(msg)
        return False, msg

    assetDescriptionPath = reg.get.asset(ns).get('description')
    if not os.path.exists(assetDescriptionPath):
        msg = 'Asset description is missing: %s' %assetDescriptionPath
        logger.warning(msg)
        return False, msg
        
    # get groom hero path
    assetReg = register_entity.RegisterInfo(assetDescriptionPath)
    groomPath = assetReg.get.groom_yeti().get('heroFile')

    if not os.path.exists(str(groomPath)):
        msg = 'Groom hero file is missing: %s' %groomPath
        logger.warning(msg)
        return False, msg

    # get/create reference to groom hero
    existing_refs = [r for r in pm.listReferences() if r.namespace == ns and r.path == groomPath]
    if not existing_refs:
        logger.info('Creating ref: %s %s' %(ns, groomPath))
        groomRef = pm.createReference(groomPath, mergeNamespacesOnClash=True, namespace=ns)
    else:
        logger.info('Using existing ref: %s %s' %(ns, groomPath))
        groomRef = existing_refs[0]
        groomRef.load()

    # add TechGeo_Grp on top
    techGrp = scene.projectInfo.asset.tech_grp()
    groomGrp = scene.projectInfo.asset.groom_grp()
    groomGeoGrp = scene.projectInfo.asset.groom_geo_grp()

    # make sure groom hero has proper grouping
    yetiGeoGrpFp = '%s|%s' %(groomGrp, groomGeoGrp)
    yetiGrpPath = '|'.join(['%s:%s' %(ns, grp) for grp in yetiGeoGrpFp.split('|')])
    if not mc.objExists(yetiGrpPath):  # Yeti_Grp must sits under world only
        msg = 'Cannot find %s' %yetiGrpPath
        groomRef.remove()
        logger.warning(msg)
        return False, msg
    yetiGrp = pm.PyNode(yetiGrpPath.split('|')[0])
    # yetiGeoGrp = pm.PyNode(yetiGrpPath)
    
    techGrpName = '%s:%s' %(ns, techGrp)
    if not pm.objExists(techGrpName):
        techGeoGrp = pm.group(em=True, n=techGrpName)
    else:
        techGeoGrp = pm.PyNode(techGrpName)
    pm.parent(yetiGrp, techGeoGrp)

    assetYetiGrpName = '%s:Groom_Grp' %ns
    assetYetiGrps = pm.ls(assetYetiGrpName, type='transform')
    if assetYetiGrps:
        assetYetiGrp = assetYetiGrps[0]
    else:
        assetYetiGrp = pm.group(em=True, n=assetYetiGrpName)
    pm.parent(techGeoGrp, assetYetiGrp)

    # attach alembic
    
    # yetiGrpAbcPath = '/'.join(['%s:%s' %(ns, grp) for grp in ('%s/%s/%s' %(techGrp, groomGrp, groomGeoGrp)).split('/')])
    #logger.info('Attaching alembic: %s' %abc_path)

    # --- attach/blendshape groom to follow alembic
    # check for static curves
    static_curves = check.getStaticObjInABC(abc_path, ['Curve'])
    if static_curves:
        logger.info('Static curve found, using blendshape method.')
        ref = pm.createReference(abc_path, ns='techGeoTMP')

        # create blendshape
        try:
            yetiGeoGrp = pm.PyNode(yetiGrpPath)
            techYetiGeoGrp = pm.PyNode('|'.join(['%s:%s' %(ref.namespace, grp) for grp in yetiGeoGrpFp.split('|')]))
            # print techYetiGeoGrp, yetiGeoGrp
            bsh = pm.blendShape(techYetiGeoGrp, yetiGeoGrp, tc=True, origin='local', automatic=True)[0]
            bsh.w[0].set(1.0)
        except Exception, e:
            msg = 'Alembic blendshape failed\n'
            msg += str(e)
            # writeLog(log, result)
            print msg
            logger.warning(msg)
            return False, msg

    else:
        logger.info('No static curve found, using ABC attach method.')
        try:
            mc.AbcImport(abc_path, connect=techGeoGrp.longName())
            # mel.eval('AbcImport -mode "import" -connect "%s" "%s";' %(techGeoGrp.shortName(), abc_path))
            msg = 'Attach success'
            logger.info(msg)
        except Exception, e:
            msg = 'Alembic attach failed'
            logger.warning(msg)
            return False, msg

    for grp in (techGrp, groomGrp, groomGeoGrp):
        try:
            mc.setAttr('%s:%s.visibility' %(ns, grp), True)
        except:
            print 'Cannot set visibility because %s:%s.visibility is Locked or Connected' %(ns, grp)

    return True, techGeoGrp