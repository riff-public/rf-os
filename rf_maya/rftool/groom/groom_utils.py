import pymel.core as pm
import maya.OpenMaya as om

def reorder_curve_groups(grps=[], tolerance=1e-04):
    if not grps:
        grps = pm.selected()
        if len(grps) != 2:
            om.MGlobal.displayError('Please select 2 curve groups')
            return

    baseGrp = pm.PyNode(grps[0])
    diffGrp = pm.PyNode(grps[1])

    baseChildren = baseGrp.getChildren(type='transform')
    diffChildren = diffGrp.getChildren(type='transform')

    lenBase = len(baseChildren)
    lenDiff = len(diffChildren)
    if lenBase != lenDiff:
        om.MGlobal.displayError('Different children count!')
        return

    if len([i for i in baseChildren if i.getShape(ni=True) and i.getShape(ni=True).nodeType()=='nurbsCurve']) != lenBase:
        om.MGlobal.displayError('Not all %s children is a curve' %baseGrp.nodeName())
        return
        
    if len([i for i in diffChildren if i.getShape(ni=True) and i.getShape(ni=True).nodeType()=='nurbsCurve']) != lenDiff:
        om.MGlobal.displayError('Not all %s children is a curve' %diffGrp.nodeName())
        return

    diff_pos = [p.getShape().cv[0].getPosition('world') for p in diffChildren]

    success = 0
    for b, base in enumerate(baseChildren):
        fcv = base.getShape().cv[0]
        fcv_pos = fcv.getPosition('world')
        for i, pos in enumerate(diff_pos):
            if fcv_pos.isEquivalent(pos, tolerance):
                curr_index = diffGrp.getChildren(type='transform').index(diffChildren[i])
                pm.reorder(diffChildren[i], r=((curr_index * -1) + b))
                success += 1
                break

    if success != lenDiff:
        om.MGlobal.displayWarning('Not all curve matches!')

def create_groom_group():
    from rf_utils.context import context_info
    try:
        scene = context_info.ContextPathInfo(path=mc.file(q=True, sn=True))
        yetiGrpName = scene.projectInfo.asset.groom_grp()
        yetiGeoGrpName = scene.projectInfo.asset.groom_geo_grp()
        yetiNodeGrpName = scene.projectInfo.asset.groom_node_grp()
    except Exception:
        yetiGrpName = 'Yeti_Grp'
        yetiGeoGrpName = 'YetiGeo_Grp'
        yetiNodeGrpName = 'YetiNode_Grp'

    # ----- Yeti_Grp
    yetiGrpPath = '|%s' %yetiGrpName
    if not pm.objExists(yetiGrpPath):
        yetiGrp = pm.group(em=True, n=yetiGrpName)
    else:
        yetiGrp = pm.PyNode(yetiGrpPath)

    # ----- YetiGeo_Grp
    yetiGeoGrpPath = '|%s|%s' %(yetiGrpName, yetiGeoGrpName)
    if not pm.objExists(yetiGeoGrpPath):
        yetiGeoGrp = pm.group(em=True, n=yetiGeoGrpName, p=yetiGrp)
    else:
        yetiGeoGrp = pm.PyNode(yetiGeoGrpPath)

    # ----- YetiNode_Grp
    yetiNodeGrpPath = '|%s|%s' %(yetiGrpName, yetiNodeGrpName)
    if not pm.objExists(yetiNodeGrpPath):
        yetiNodeGrp = pm.group(em=True, n=yetiNodeGrpName, p=yetiGrp)
    else:
        yetiNodeGrp = pm.PyNode(yetiNodeGrpPath)
    yetiNodeGrp.inheritsTransform.set(False)

    # ----- Bake_Tex_Ref
    bakeTexRefName = 'Bake_Tex_Ref'
    bakeTexRefPath = '|%s|%s|%s' %(yetiGrpName, yetiNodeGrpName, bakeTexRefName)
    if not pm.objExists(bakeTexRefPath):
        bakeTexRef = pm.group(em=True, n=bakeTexRefName, p=yetiNodeGrp)
    else:
        bakeTexRef = pm.PyNode(bakeTexRefPath)
    bakeTexRef.visibility.set(False)

    # ----- YetiMesh_Grp
    yetiMeshGrpName = 'YetiMesh_Grp'
    yetiMeshGrpPath = '|%s|%s|%s' %(yetiGrpName, yetiGeoGrpName, yetiMeshGrpName)
    if not pm.objExists(yetiMeshGrpPath):
        yetiMeshGrp = pm.group(em=True, n=yetiMeshGrpName, p=yetiGeoGrp)
    else:
        yetiMeshGrp = pm.PyNode(yetiMeshGrpPath)

    # ----- YetiCrv_Grp
    yetiCrvGrpName = 'YetiCrv_Grp'
    yetiCrvGrpPath = '|%s|%s|%s' %(yetiGrpName, yetiGeoGrpName, yetiCrvGrpName)
    if not pm.objExists(yetiCrvGrpPath):
        yetiCrvGrp = pm.group(em=True, n=yetiCrvGrpName, p=yetiGeoGrp)
    else:
        yetiCrvGrp = pm.PyNode(yetiCrvGrpPath)

    print '%s created.' %yetiGrpName,

class SetYetiGrowNodeUI(object):
    def __init__(self):
        self.initUI()

    def initUI(self):
        WINDOW_NAME = 'SetYetiGrowNodeUI_Win'
        if pm.window(WINDOW_NAME, ex=True):
            pm.deleteUI(WINDOW_NAME, window=True)
        with pm.window(WINDOW_NAME, title='Set Grow Node Geometry', mxb=False, sizeable=False) as self.win:
            with pm.columnLayout(adj=True, columnAttach=('left', 5), rowSpacing=10):
                with pm.optionMenu( label='Segment Creation Sytle') as self.optionMenu:
                    pm.menuItem( label='By Count')
                    pm.menuItem( label='By Length')
                self.slider = pm.floatSliderGrp(l='Segment Length', v=0.5, pre=3, field=True, fmn=0.0, fmx=1.0, min=0.0, max=1.0)
                pm.button(l='Set', c=pm.Callback(self.set))
        # set default
        self.optionMenu.setValue('By Length')
        pm.showWindow(self.win)

    def set(self):
        sels = [n for n in pm.selected(type='transform') if n.getShape(type='pgYetiMaya', ni=True)]
        if not sels:
            om.MGlobal.displayError('Select at least 1 yeti node!')
            return

        sytle_enum = {'By Length': 0, 'By Count': 1}
        style_str = self.optionMenu.getValue()
        cre_style = sytle_enum[style_str]
        seg_len = self.slider.getValue()
        for node in sels:
            growNodes = pm.pgYetiGraph(node, listNodes=True, type='grow')
            for gn in growNodes:
                pm.pgYetiGraph(node, node=gn, param='segmentStyle', setParamValueScalar =cre_style)
                pm.pgYetiGraph(node, node=gn, param='segmentLength', setParamValueScalar =seg_len)
                print 'Set %s.%s segmentStyle: %s' %(node.shortName(), gn, style_str)
                print 'Set %s.%s segmentLength: %s' %(node.shortName(), gn, seg_len)