import sys
import os
from glob import glob

import pymel.core as pm
import maya.cmds as mc
import maya.mel as mel
import maya.api.OpenMaya as om

from nuTools import controller
from nuTools import misc

SCALE_CONSTANT = 2.5
EXPRESSION_STRING = '''
$duration = ({file}.endCycleExtension - {file}.startCycleExtension) + 1;
{file}.frameExtension = abs((({ctrl}.offset + (frame * {ctrl}.play)) * {ctrl}.speed) % $duration);
'''

def get_available_namespace(name):
    i = 1
    all_dags = mc.ls(dag=True)
    while True:
        new_name = '{}{}_'.format(name, i)
        if [dag for dag in all_dags if dag.startswith(new_name)]:
            i += 1
        else:
            return new_name

def create_file(name):
    fileNode = pm.shadingNode('file', asTexture=True, isColorManaged=True, n=name)
    fileNode.filterType.set(0)
    fileNode.ignoreColorSpaceFileRules.set(True)
    p2d = pm.shadingNode('place2dTexture', asUtility=True)
    p2d.outUV >> fileNode.uvCoord
    p2d.outUvFilterSize >> fileNode.uvFilterSize
    p2d.vertexCameraOne >> fileNode.vertexCameraOne
    p2d.vertexUvOne >> fileNode.vertexUvOne
    p2d.vertexUvTwo >> fileNode.vertexUvTwo
    p2d.vertexUvThree >> fileNode.vertexUvThree
    p2d.coverage >> fileNode.coverage
    p2d.mirrorU >> fileNode.mirrorU
    p2d.mirrorV >> fileNode.mirrorV
    p2d.noiseUV >> fileNode.noiseUV
    p2d.offset >> fileNode.offset
    p2d.repeatUV >> fileNode.repeatUV
    p2d.rotateFrame >> fileNode.rotateFrame
    p2d.rotateUV >> fileNode.rotateUV
    p2d.stagger >> fileNode.stagger
    p2d.translateFrame >> fileNode.translateFrame
    p2d.wrapU >> fileNode.wrapU
    p2d.wrapV >> fileNode.wrapV

    return fileNode

def create_shader(name, shaderType='lambert'):
    shader = pm.shadingNode(shaderType, asShader=True, n=name)
    sg = pm.sets(renderable=True, noSurfaceShader=True, empty=True, name='{}SG'.format(name))
    pm.connectAttr(shader.outColor, sg.surfaceShader, f=True)
    return shader, sg

def generate(path, name='animatePlane', scale=1.0):
    # get all images
    images = sorted([p.replace('\\', '/') for p in glob(path)])
    # get start end
    start_frame = int(images[0].split('.')[-2])
    end_frame = int(images[-1].split('.')[-2])
    print('start frame', start_frame)
    print('end_frame', end_frame)

    # get size from first image, assuming all images are of the same size
    mImg = om.MImage()
    mImg.readFromFile(images[0])
    w, h = mImg.getSize()
    # w, h = (1280, 720)
    size_ratio = float(w)/float(h)
    print('size_ratio', size_ratio)

    # calculate plane size
    plane_width = (SCALE_CONSTANT*size_ratio)*scale
    plane_height = (plane_width/size_ratio)
    y_offset = pm.dt.Vector(0, plane_height*0.5, 0)
    print('plane size', plane_width, plane_height)

    # open undo stack
    pm.undoInfo(openChunk=True)

    # create plane geo, apply planar UV
    prefix = get_available_namespace(name=name)
    print('prefix', prefix)

    plane_name = '{}AnimPlane_Geo'.format(prefix)
    plane_geo = pm.polyPlane(w=plane_width,
                            h=plane_height, 
                            n=plane_name,
                            ax=(0, 0, 1),
                            sx=1, sy=1, cuv=2, ch=False)[0]
    # move the plane up on grid
    pm.move(plane_geo.f[0], y_offset, r=True)
    pm.polyProjection(plane_geo.f[0],
                    ch=False, 
                    type='Planar',
                    kir=True,  # keep image ratio on
                    ibd=True, 
                    md='z')

    # create shader
    shader_name = '{}AnimPlane_Shd'.format(prefix)
    shader, sg = create_shader(name=shader_name, shaderType='lambert')

    # create file node
    file_name = '{}AnimPlane_File'.format(prefix)
    file_node = create_file(name=file_name)
    file_node.useFrameExtension.set(True)
    file_node.fileTextureName.set(images[0])
    file_node.startCycleExtension.set(start_frame)
    file_node.endCycleExtension.set(end_frame)
    file_node.useHardwareTextureCycling.set(True)
    pm.connectAttr(file_node.outColor, shader.color, f=True)
    pm.connectAttr(file_node.outTransparency, shader.transparency, f=True)

    # assign shader
    pm.select(plane_geo)
    pm.hyperShade(plane_geo, assign=shader)

    # create controllers
    # placement
    placementCtrl = controller.Controller(st='flatDiamond', 
                n='{}AllMover_Ctrl'.format(prefix), scale=SCALE_CONSTANT*scale*1.2, color='yellow')
    placementCtrl.lockAttr(v=True)
    placementCtrl.hideAttr(v=True)

    # sub control
    rootSubCtrl = controller.Controller(st='square', axis='+z',
                n='{}RootSub_Ctrl'.format(prefix), scale=0.6, color='lightBlue')
    pm.scale(rootSubCtrl.cv[0:rootSubCtrl.numCVs()-1], pm.dt.Vector(plane_width, plane_height, 1), r=True)
    rootSubCtrl.lockAttr(v=True)
    rootSubCtrl.hideAttr(v=True)
    rootSubGimbalCtrl = rootSubCtrl.addGimbal()

    # add anim attrs
    misc.addNumAttr(rootSubCtrl, '__ImageSequence__', 'long', lock=True)
    misc.addNumAttr(rootSubCtrl, 'play', 'long', min=0, max=1, dv=0, key=True)
    misc.addNumAttr(rootSubCtrl, 'speed', 'double', dv=1, key=True)
    misc.addNumAttr(rootSubCtrl, 'offset', 'long', dv=0, key=True)

    # group controllers and geo
    zgrp = pm.group(em=True, n='{}RootSubCtrlZro_Grp'.format(prefix))
    pm.parent(rootSubCtrl, zgrp)
    pm.move(zgrp, y_offset, r=True)
    pm.parent(zgrp, placementCtrl)
    pm.parent(plane_geo, rootSubGimbalCtrl)
    misc.lockAttr(plane_geo, t=True, r=True, s=True, v=True)

    # create expression to control image sequence
    exp_str = EXPRESSION_STRING.format(ctrl=rootSubCtrl.shortName(), 
                                        file=file_node.nodeName())
    exp_node = pm.expression(s=exp_str, n='{}AnimPlane_Exp'.format(prefix))

    pm.select(placementCtrl)
    # close undo stack
    pm.undoInfo(closeChunk=True)

    return placementCtrl



# # TEST SNIPPET
# path = 'P:/Hanuman/asset/publ/prop/hnmInteraction01Ui/texture/main/hero/texRen/hnmInteraction01Ui_v001_001.*.png'
# name = 'hnmInteraction01Ui'
# scale = 1.0

# generate(path=path, name=name, scale=1)
