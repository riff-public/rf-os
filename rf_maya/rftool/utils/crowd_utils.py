import maya.cmds as mc
import pymel.core as pm 
import random 
import logging 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())


def random_pick(sources, targets, attrs=None, duplicate=True): 
    """ random placement and attributes 
    sources (list): list of ctrl to move 
    targets (list): list of target to snap 
    attrs (dict): random attribute {'tx': (-10, 10), 'rx': (-360, 360)}
    """ 
    # generate a random list the same number as targets
    output = 'Output_Grp'
    rand_list = random.sample(range(0, len(targets)), len(targets))


    for i, target in enumerate(targets): 
        index = rand_list[i]
        # sources is less than target so mod to limit the output in source range 
        pick_index = index % len(sources) 
        pick = sources[pick_index]
        
        # duplicate 
        dup = mc.duplicate(pick)[0]
        
        # organize 
        if not mc.objExists(output): 
            output = mc.group(n=output, em=True)
        mc.parent(dup, output)

        # snap transform 
        mc.delete(mc.pointConstraint(target, dup))

        # apply random attrs 
        if attrs: 
            for k, v in attrs.items(): 
                mc.setAttr('{}.{}'.format(dup, k), random.uniform(*v))


def shuffle_selection(): 
    return shuffle(mc.ls(sl=True))


def random_scale_selection(values=(0.8, 1.2)): 
    return random_scale(mc.ls(sl=True), values=values)


def random_direction_selection(values=(-360, 360), axis='ry'): 
    return random_direction(mc.ls(sl=True), values=values)


def random_scale(targets, values=(0.8, 1.0)): 
    for target in targets: 
        value = random.uniform(*values)
        mc.setAttr('{}.scale'.format(target), value, value, value)


def random_direction(targets, values=(-360, 360), axis='ry'): 
    for target in targets: 
        value = random.uniform(*values)
        mc.setAttr('{}.{}'.format(target, axis), value)

def random_position(targets, values=(-0.2, 0.2), axis='tx'): 
    for target in targets: 
        value = random.uniform(*values)
        current = mc.getAttr('{}.{}'.format(target, axis))
        newvalue = current + value
        mc.setAttr('{}.{}'.format(target, axis), newvalue)


def shuffle(targets): 
    """ shuffle possition """ 
    def get_xform(obj): 
        temp = mc.spaceLocator()[0]
        mc.delete(mc.parentConstraint(obj, temp))
        xform = mc.xform(temp, q=True, m=True)
        mc.delete(temp)
        return xform

    if len(targets) > 2: 
        list_pos = [get_xform(a) for a in targets]
        random_pos = random.sample(list_pos, len(targets))

        for i, target in enumerate(targets): 
            mc.xform(target, m=random_pos[i])

    elif len(targets) == 2: 
        xform1 = get_xform(targets[0])
        xform2 = get_xform(targets[1])
        mc.xform(targets[0], m=xform2)
        mc.xform(targets[1], m=xform1)

    else: 
        logger.error('Please select more than 1 object')
    

def test_random(part): 
    pants = ['pant1', 'pant2', 'pant3', 'pant4', 'pant5']
    shirts = ['shirt1', 'shirt2', 'shirt3', 'shirt4', 'shirt5', 'shirt6', 'shirt7']
    hairs = ['hair1', 'hair2', 'hair3', 'hair4']
    skins = ['skin1', 'skin2', 'skin3', 'skin4']
    boots = ['boot1', 'boot2', 'boot3', 'boot4']
    socks = ['sock1', 'sock2']
    coats = ['coat1', 'coat2']

    body_plys = ['Body_Geo']
    hair_plys = ['HairA_Geo', 'HairB_Geo', 'HairC_Geo', 'HairD_Geo']
    shirt_plys = ['ShirtA_Geo', 'ShirtC_Geo', 'ShirtH_Geo', 'ShirtI_Geo']
    pant_plys = ['PantA_Geo', 'PantC_Geo', 'PantH_Geo', 'ShortI_Geo']
    boot_plys = ['BootA_Geo', 'BootC_Geo', 'BootH_Geo', 'BootI_Geo']
    sock_plys = ['SockA_Geo', 'SockC_Geo', 'SockH_Geo', 'SockI_Geo']
    coat_plys = ['OvercoatI_Geo']

    if part == 'pant': 
        plys = pant_plys
        shds = pants
    if part == 'shirt': 
        plys = shirt_plys
        shds = shirts
    if part == 'hair': 
        plys = hair_plys
        shds = hairs
    if part == 'skin': 
        plys = body_plys
        shds = skins
    if part == 'boot': 
        plys = boot_plys
        shds = boots
    if part == 'sock': 
        plys = sock_plys
        shds = socks
    if part == 'coat': 
        plys = coat_plys
        shds = coats

    mc.select(cl=True)
    for ply in plys: 
        mc.select(mc.ls(ply), add=True)

    sels = mc.ls(sl=True)
    mc.select(cl=True)
    for ply in sels: 
        shd = random.choice(shds)
        mc.select(ply)
        mc.hyperShade(assign=shd)


def test(): 
    test_random('pant')
    test_random('shirt')
    test_random('hair')
    test_random('skin')
    test_random('boot')
    test_random('sock')
