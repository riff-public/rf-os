#Import python modules
import os, sys

#Import GUI
from Qt import QtCore
from Qt import QtGui
from Qt import QtWidgets
from Qt import QtUiTools
from Qt import wrapInstance
QtUiTools = QtUiTools()

# Import Maya module
import maya.OpenMayaUI as mui
import maya.cmds as mc


moduleFile = sys.modules[__name__].__file__
moduleDir = os.path.dirname(moduleFile)
sys.path.append(moduleDir)


def getMayaWindow():
    ptr = mui.MQtUtil.mainWindow()
    if ptr is not  None:
        # ptr = mui.MQtUtil.mainWindow()
        return wrapInstance(long(ptr), QtWidgets.QMainWindow)

def deleteUI(ui):
    if mc.window(ui, exists=True):
        mc.deleteUI(ui)
        deleteUI(ui)

def run():
    app = MyForm(getMayaWindow())
    return app

class MyForm(QtWidgets.QMainWindow):

    def __init__(self, parent=None):
        self.count = 0
        #Setup Window
        super(MyForm, self).__init__(parent)

        self.mayaUI = 'RefEditorUI'
        deleteUI(self.mayaUI)

        # read .ui directly
        loader = QtUiTools.QUiLoader()
        loader.setWorkingDirectory(moduleDir)

        f = QtCore.QFile("%s/ui.ui" % moduleDir)
        f.open(QtCore.QFile.ReadOnly)

        self.myWidget = loader.load(f, self)
        self.ui = self.myWidget

        f.close()

        self.ui.show()
        self.ui.setWindowTitle('Ref Editor v.1.2.0')

        self.setUI()
        self.initSignals()

    def initSignals(self):
        self.ui.load_pushButton.clicked.connect(self.loadRef)
        self.ui.unload_pushButton.clicked.connect(self.unloadRef)
        self.ui.unloadInstance_pushButton.clicked.connect(self.unloadInstance)
        self.ui.loadInstance_pushButton.clicked.connect(self.loadInstance)
        self.ui.remove_pushButton.clicked.connect(self.removeRef)
        self.ui.removeInstance_pushButton.clicked.connect(self.removeinstance)
        self.ui.removeUnload_pushButton.clicked.connect(self.removeUnloadRef)
        self.ui.selectObj_pushButton.clicked.connect(self.selectViewportObjects)
        self.ui.selectList_pushButton.clicked.connect(self.selectListFromObjects)

    def setUI(self):
        self.ui.listWidget.clear()
        for path in sorted(mc.file(q=True, r=True)):
            item = QtWidgets.QListWidgetItem()
            item.setText(path)
            self.ui.listWidget.addItem(item)

            self.loadState(item, path, mc.referenceQuery(path, isLoaded=True))


    def loadRef(self):
        items = self.getSelectedItems()

        for item in items:
            path = item.data(QtCore.Qt.UserRole)[0]
            isLoad = item.data(QtCore.Qt.UserRole)[1]

            if not isLoad:
                rnNode = mc.referenceQuery(path, referenceNode=True)
                mc.file(path, loadReference=rnNode)
                self.loadState(item, path, True)
            else:
                print 'Already loaded'


    def unloadRef(self):
        items = self.getSelectedItems()

        for item in items:
            path = item.data(QtCore.Qt.UserRole)[0]
            isLoad = item.data(QtCore.Qt.UserRole)[1]

            if isLoad == True:
                rnNode = mc.referenceQuery(path, referenceNode=True)
                mc.file(path, unloadReference=rnNode)
                self.loadState(item, path, False)
            else:
                print 'Already unloaded'

    def removeRef(self):
        items = self.getSelectedItems()
        directRemove = self.ui.removeRefDIrect_checkBox.isChecked()

        for item in items:
            path = item.data(QtCore.Qt.UserRole)[0]
            isLoad = item.data(QtCore.Qt.UserRole)[1]
            rnNode = mc.referenceQuery(path, referenceNode=True)

            if not isLoad == True:
                if not directRemove:
                    mc.file(path, loadReference=rnNode)

            mc.file(path, rr=True)

        self.setUI()

    def loadState(self, item, path, state):
        color = [100, 100, 100]
        value = False
        if state:
            color = [200, 200, 200]
            value = True
        item.setForeground(QtGui.QColor(color[0], color[1], color[2]))
        item.setData(QtCore.Qt.UserRole, [path, value])

    def unloadInstance(self):
        targetList = self.getSameInstances()

        for path in targetList:
            if mc.referenceQuery(path, isLoaded=True):
                rnNode = mc.referenceQuery(path, referenceNode=True)
                mc.file(path, unloadReference=rnNode)

        self.setUI()

    def loadInstance(self):
        targetList = self.getSameInstances()

        for path in targetList:
            if not mc.referenceQuery(path, isLoaded=True):
                rnNode = mc.referenceQuery(path, referenceNode=True)
                mc.file(path, loadReference=rnNode)

        self.setUI()

    def removeinstance(self):
        targetList = self.getSameInstances()
        directRemove = self.ui.removeRefDIrect_checkBox.isChecked()

        for path in targetList:
            rnNode = mc.referenceQuery(path, referenceNode=True)
            if not mc.referenceQuery(path, isLoaded=True):
                if not directRemove:
                    mc.file(path, loadReference=rnNode)

            mc.file(path, rr=True)

        self.setUI()

    def removeUnloadRef(self):
        unloadList = self.getUnloadReferences()
        directRemove = self.ui.removeRefDIrect_checkBox.isChecked()

        for path in unloadList:
            rnNode = mc.referenceQuery(path, referenceNode=True)
            if not mc.referenceQuery(path, isLoaded=True):
                if not directRemove:
                    mc.file(path, loadReference=rnNode)

            mc.file(path, rr=True)

        self.setUI()


    def getSelectedItems(self):

        if self.ui.viewport_checkBox.isChecked():
            self.selectListFromObjects()

        items = self.ui.listWidget.selectedItems()
        return items


    def getSameInstances(self):
        items = [self.ui.listWidget.item(i) for i in range(self.ui.listWidget.count())]
        itemStr = [str(a.text()) for a in items]
        selItems = self.getSelectedItems()
        targetlist = []

        for item in selItems:
            path = item.data(QtCore.Qt.UserRole)[0]
            isLoad = item.data(QtCore.Qt.UserRole)[1]

            for refPath in itemStr:
                if convertPath(refPath) == convertPath(path):
                    if not refPath in targetlist:
                        targetlist.append(refPath)

        return targetlist

    def getUnloadReferences(self):
        items = [self.ui.listWidget.item(i) for i in range(self.ui.listWidget.count())]
        unloadRef = []

        for item in items:
            path, isLoad = item.data(QtCore.Qt.UserRole)

            if not isLoad:
                unloadRef.append(path)

        return unloadRef


    def selectViewportObjects(self):
        items = self.ui.listWidget.selectedItems()
        namespaces = [mc.referenceQuery(str(a.text()), namespace=True) for a in items]
        selections = []

        if namespaces:
            for namespace in namespaces:
                defaultGrp = '%s:Rig_Grp' % namespace.replace(':', '')
                if mc.objExists(defaultGrp):
                    selections.append(defaultGrp)

        mc.select(selections)

    def selectListFromObjects(self):
        paths = []
        self.ui.listWidget.clearSelection()
        selObjs = mc.ls(sl=True, l=True)

        for obj in selObjs:
            path = mc.referenceQuery(obj, f = True)
            if not path in paths:
                paths.append(path)

        for i in range(self.ui.listWidget.count()):
            item = self.ui.listWidget.item(i)
            path = item.data(QtCore.Qt.UserRole)[0]

            if path in paths:
                item.setSelected(True)

def convertPath(path):
    if '{' in path:
        path = path.split('{')[0]

    return path





