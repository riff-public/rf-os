import os
import sys
import shutil
from collections import OrderedDict

try:
    import maya.cmds as mc
    import maya.mel as mm
    import maya.OpenMaya as om
    import pymel.core as pm 
    isMaya = True

except ImportError:
    isMaya = False

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())


def get_path():
    return mc.file(q=True, loc=True)

def setup_asset_viewport_capture():
    mc.grid(toggle=0)
    perspPanel = mc.getPanel( withLabel='Persp View')
    mc.modelEditor(perspPanel,e=True,hud=False)
    mc.setAttr("defaultResolution.width",1024)
    mc.setAttr("defaultResolution.height",1024)

def setup_scene_viewport_playblast():
    mc.grid(toggle=0)
    perspPanel = mc.getPanel( withLabel='Persp View')
    mc.modelEditor(perspPanel,e=True,hud=False)
    mc.setAttr("defaultResolution.width",1024)
    mc.setAttr("defaultResolution.height",1024)

def playblast_capture_1k(image_path, atFrame=None, w=1024, h=1024):
    file_extention = image_path.split('.')[-1]
    index = int(image_path.split('.')[-2])
    image_no_extention = image_path.split('.')[0]
    print image_no_extention

    if not atFrame:
        atFrame = index

    result = mc.playblast(format='image',
                    filename=image_no_extention,
                    st=atFrame,
                    et=atFrame,
                    sequenceTime=0,
                    clearCache=1,
                    viewer=0,
                    showOrnaments=1,
                    fp=4,
                    percent=100,
                    compression=file_extention,
                    quality=100,
                    widthHeight=[w, h])

    if result:
        print 'result', result
        # result = result.replace('####',image_path.split('.')[-2])
        targetFile = result.replace('####',image_path.split('.')[-2]).replace('\\', '/')
        tmpFile = result.replace('####',atFrame).replace('\\', '/')
        print atFrame
        print tmpFile
        print targetFile

        if os.path.exists(tmpFile):
            os.rename(tmpFile, targetFile)

        print 'result', result

        return targetFile

def playblast_avi(mov_path,start,end,resolution,width=960,height=540):
    # playblast  -fmt "avi" -startTime 301 -endTime 325 -sequenceTime 1 -forceOverwrite -filename "movies/s0030.avi" -clearCache 1 -showOrnaments 0 -percent 100 -wh 1024 778 -offScreen -viewer 0 -useTraxSounds -compression "none" -quality 70;
    image_no_extention = mov_path.split('.')[0]
    file_extention = mov_path.split('.')[-1]

    if file_extention == 'avi':
        result = mc.playblast(format=file_extention,
                        filename=mov_path,
                        st=start,
                        et=end,
                        sequenceTime=1,
                        forceOverwrite=True,
                        clearCache=1,
                        viewer=0,
                        showOrnaments=1,
                        percent=100,
                        compression="none",
                        quality=resolution,
                        widthHeight=[width,height],
                        useTraxSounds=True)

    if file_extention == 'jpg':
        result = mc.playblast(format='image',
                    filename=image_no_extention,
                    st=start,
                    et=start,
                    sequenceTime=0,
                    clearCache=1,
                    viewer=0,
                    showOrnaments=1,
                    fp=4,
                    percent=100,
                    compression=file_extention,
                    quality=100,
                    widthHeight=[width,height])
        result = result.replace('####','%04d' %(start))

    return result


def capture_screen(dst, format, st, sequencer, w, h) :
    outputFile = dst.split('.')[0]
    extension = dst.split('.')[-1]

    start = st
    end = start

    result = mc.playblast( format= 'iff' ,
                            filename= outputFile,
                            st=start ,
                            et=end ,
                            forceOverwrite=True ,
                            sequenceTime=sequencer ,
                            clearCache=1 ,
                            viewer= 0 ,
                            showOrnaments=1 ,
                            fp=4 ,
                            widthHeight= [w,h] ,
                            percent=100 ,
                            compression= format ,
                            offScreen=True ,
                            quality=70
                            )


    if result :
        padding = '%04d' % start
        output = result.replace('####', padding)
        if os.path.exists(dst) :
            os.remove(dst)
        os.rename(output, dst)

        return dst

def correct_cam(shotName):
    cam_name = shotName + '_cam'
    st = 0
    et = 0

    if mc.shot(shotName, q=True ,currentCamera=True) == cam_name:
        st = mc.getAttr('%s.startFrame' %(shotName))
        et = mc.getAttr('%s.endFrame' %(shotName))

    return cam_name,st,et

def setup_scene_viewport_playblast(shotName):
    mc.grid(toggle=0)

    mc.setAttr("defaultResolution.width",960)
    mc.setAttr("defaultResolution.height",540)

    if mc.objExists(shotName):
        mc.setAttr('%s_camShape.displayGateMask' %(shotName), True)
        mc.setAttr('%s_camShape.displayResolution' %(shotName), True)
        mc.setAttr('%s_camShape.displayGateMaskOpacity' %(shotName), 1)
        mc.setAttr('%s_camShape.displayGateMaskColor' %(shotName), 0, 0, 0, type='double3')
        mc.setAttr('%s_camShape.overscan' %(shotName),1.0)

def get_current_shot():
    return mc.sequenceManager(q=True,currentShot=True)

def get_current_frame_no(shotName):
    cam_name, sf, ef = correct_cam(shotName)
    ft = (ef - sf) + 1.0
    cf = mc.currentTime(q=True)
    ct = (cf - sf) + 1.0

    return '%d/%d' %(ct,ft)

def get_current_frame(shotName):
    cam_name, sf, ef = correct_cam(shotName)
    cf = mc.currentTime(q=True)

    return '%d/%d' %(cf,ef)

def set_first_frame(shotName):
    st = mc.getAttr('%s.startFrame' %(shotName))
    mc.currentTime(st)
    mc.sequenceManager(currentTime=st)


def HUDClear():
    for hud in mc.headsUpDisplay(listHeadsUpDisplays=True):
        mc.headsUpDisplay(hud,edit=True, visible=False)
        mc.headsUpDisplay(hud,refresh=True)

def HUDPlayblast(user,versionName):

    HUDClear()

    hud_lists = [ 'HUDUsername', 'HUDCurrentTime', 'HUDFilename', 'HUDCurrentShot', 'HUDCurrentFrame', 'HUDOverAll' ]

    # [i for i, j in zip(a, b) if i == j]

    # Left-Bottom
    mc.headsUpDisplay( rp=(5, 3) )
    if not mc.headsUpDisplay('HUDUsername', exists=True):
        mc.headsUpDisplay('HUDUsername', section=5, block=3, label='User : ', command='sp_app.path_info.PathInfo().user', attachToRefresh=True, blockAlignment='left')
    if mc.headsUpDisplay('HUDUsername',exists = True):
        mc.headsUpDisplay('HUDUsername', edit=True, section=5, block=3, label='User : ', command='sp_app.path_info.PathInfo().user', attachToRefresh=True, blockAlignment='left')
    mc.headsUpDisplay( rp=(5, 2) )
    if not mc.headsUpDisplay('HUDCurrentTime',exists = True):
        mc.headsUpDisplay('HUDCurrentTime', section=5, block=2, label='Date/Time : ', command='sp_app.file_utils.get_now_time()', attachToRefresh=True, blockAlignment='left')
    if mc.headsUpDisplay('HUDCurrentTime',exists = True):
        mc.headsUpDisplay('HUDCurrentTime', edit=True, section=5, block=2, label='Date/Time : ', command='sp_app.file_utils.get_now_time()', attachToRefresh=True, blockAlignment='left')
    mc.headsUpDisplay( rp=(5, 1) )
    if not mc.headsUpDisplay('HUDFilename',exists = True):
        mc.headsUpDisplay('HUDFilename', section=5, block=1, label='Filename : ', command='sp_app.path_info.PathInfo().versionName', attachToRefresh=True, blockAlignment='left')
    if mc.headsUpDisplay('HUDFilename',exists = True):
        mc.headsUpDisplay('HUDFilename', edit=True, section=5, block=1, label='Filename : ', command='sp_app.path_info.PathInfo().versionName', attachToRefresh=True, blockAlignment='left')

    # Center-Bottom
    mc.headsUpDisplay( rp=(7, 2) )
    if not mc.headsUpDisplay('HUDCurrentShot',exists = True):
        mc.headsUpDisplay('HUDCurrentShot', section=7, block=2, label='Cam : ', command='sp_app.maya_utils.get_current_shot()', attachToRefresh=True, blockAlignment='center', dataFontSize='large')
    if mc.headsUpDisplay('HUDCurrentShot',exists = True):
        mc.headsUpDisplay('HUDCurrentShot', edit=True, section=7, block=2, label='Cam : ', command='sp_app.maya_utils.get_current_shot()', attachToRefresh=True, blockAlignment='center', dataFontSize='large')
    mc.headsUpDisplay( rp=(7, 1) )
    if not mc.headsUpDisplay('HUDCurrentFrame',exists = True):
        mc.headsUpDisplay('HUDCurrentFrame', section=7, block=1, label='Info : ', command='sp_app.maya_utils.get_current_frame_no(sp_app.maya_utils.get_current_shot())', attachToRefresh=True, blockAlignment='center', dataFontSize='large')
    if mc.headsUpDisplay('HUDCurrentFrame',exists = True):
        mc.headsUpDisplay('HUDCurrentFrame', edit=True, section=7, block=1, label='Info : ', command='sp_app.maya_utils.get_current_frame_no(sp_app.maya_utils.get_current_shot())', attachToRefresh=True, blockAlignment='center', dataFontSize='large')

    # Right-Bottom
    mc.headsUpDisplay( rp=(9, 2) )
    if mc.headsUpDisplay('HUDFocalLength',exists = True):
        mc.headsUpDisplay('HUDFocalLength', edit=True, section=9, block=2, attachToRefresh=True, blockAlignment='right')
    mc.headsUpDisplay( rp=(9, 1) )
    if not mc.headsUpDisplay('HUDOverAll',exists = True):
        mc.headsUpDisplay('HUDOverAll', section=9, block=1, label='Over all : ', command='sp_app.maya_utils.get_current_frame(sp_app.maya_utils.get_current_shot())', attachToRefresh=True, blockAlignment='right', dataFontSize='large')
    if mc.headsUpDisplay('HUDOverAll',exists = True):
        mc.headsUpDisplay('HUDOverAll', edit=True, section=9, block=1, label='Over all : ', command='sp_app.maya_utils.get_current_frame(sp_app.maya_utils.get_current_shot())', attachToRefresh=True, blockAlignment='right', dataFontSize='large')


def create_rig_grp(objs=None, res='md', ctrl=True):
    from rftool.rig.utils import mainCtrl
    reload(mainCtrl)
    if not objs:
        objs = mc.ls(sl=True)

    rigGrp = mainCtrl.rigMain()
    return rigGrp



def create_gpu_cache(objName='',gpuDirname='',gpuBasename=''):
    # gpuCache -startTime 1 -endTime 1 -optimize -optimizationThreshold 40000 -writeMaterials -dataFormat ogawa -directory "C:/Users/vanef/Dropbox/media_server/project/asset/prop/general/glass/prePublish" -fileName "glass_gpu" Md_Geo_Grp;
    return mc.gpuCache(objName,startTime=1,endTime=1,optimize=True,optimizationThreshold=40000,writeMaterials=True,dataFormat="ogawa",fileName=gpuBasename,directory=gpuDirname)

def create_abc_cache(objLongName='',abcPath=''):
    # AbcExport -j "-frameRange 1 1 -uvWrite -worldSpace -writeVisibility -dataFormat ogawa -root |Rig_Grp|Geo_Grp|Md_Geo_Grp -file C:/Users/vanef/Dropbox/media_server/project/asset/prop/general/glass/cache/glass_abc.abc";
    return mc.AbcExport(j="-frameRange 1 1 -uvWrite -worldSpace -writeVisibility -dataFormat ogawa -root %s -file %s" %(objLongName,abcPath))

def check_duplicate_name():

    if mc.objExists('rig_grp'):
        mc.select('rig_grp', hi=True )
        lists = mc.ls(sl=True,type='transform')

        mc.select(clear=True)
        unDup = []

        for list in lists:
            #print list.split('|')[-1]
            if not list.split('|')[-1] in unDup:
                unDup.append(list.split('|')[-1])

            else:
                mc.select(('*%s*') %(list.split('|')[-1]), add=True)

        if mc.ls(sl=True):
            return False

        else:
            return True

# def HUDPlayblast():
#     from rftool.utils import path_info
#     path = path_info.PathInfo()
#     filename = path.versionName

#     mc.headsUpDisplay('HUDFilename', section=1, block=5, label='Filename : ', command='path.versionName')
#     mc.headsUpDisplay('HUDFilename', edit=True, visability=True)
def get_reference_path(obj):
    return mc.referenceQuery(obj, f=True)

def get_reference_node(obj):
    return mc.referenceQuery(obj, referenceNode=True)

def get_reference_namespace(obj):
    return mc.referenceQuery(obj, namespace=True)

def create_reference(assetName, path, force=False):
    create = True if force else True if os.path.exists(path) else False
    if create:
        namespace = get_namespace('%s_001' % assetName)
        result = mc.file(path, r=True, ns=namespace)
        return namespace

def create_reference_asset(namespace, path):
    path = path.split('{')[0]
    return mc.file(path, r=True, ns=namespace)

def import_reference(assetName, path):
    if os.path.exists(path):
        namespace = get_namespace('%s_001' % assetName)
        result = mc.file(path, i=True, ns=namespace)
        return namespace

def flat_all_references(): 
    refs = pm.listReferences()

    for ref in refs: 
        ref.importContents()


def replace_reference(srcPath, dstPath):
    rnNode = mc.referenceQuery(srcPath, referenceNode=True)
    return mc.file(dstPath, loadReference=rnNode, prompt=False)

def replace_reference_key(searchKey, replaceKey, skipKey=''): 
    """ replace reference that match keyword """ 
    refs = mc.file(q=True, r=True)
    # remove skip key refs 
    refs = [a for a in refs if not skipKey in a] if skipKey else refs 
    successRef = []
    unmatchedRef = []
    failedRef = []

    for ref in refs: 
        if searchKey in ref: 
            newPath = ref.replace(searchKey, replaceKey)
            newPath = newPath.split('{')[0]

            if os.path.exists(newPath): 
                replace_reference(ref, newPath)
                logger.debug('Replace : %s' % newPath)
                successRef.append(newPath)
            else: 
                failedRef.append(newPath)
        else: 
            unmatchedRef.append(ref)

    if failedRef: 
        message = '%s/%s assets not exists' % (len(failedRef), len(refs))
        logger.info(message)
        logger.info(failedRef)

    if unmatchedRef: 
        message = '%s/%s assets not match' % (len(unmatchedRef), len(refs))
        logger.info(message)
        logger.info('No refs match key "%s". Switch skipped' % searchKey) if len(unmatchedRef) == len(refs) else None

    if successRef: 
        message = '%s/%s assets switched' % (len(successRef), len(refs))
        logger.info(message)
        logger.info(successRef)

    return successRef, unmatchedRef, failedRef


def create_asm_reference(assetName, path):
    import asm_utils
    if os.path.exists(path):
        namespace = get_namespace('%s_001' % assetName)
        arNode = asm_utils.createARNode()
        asm_utils.setARDefinitionPath(arNode, path)
        asm_utils.setARNamespace(arNode, namespace)
        result = mc.rename(arNode, '%s_AR' % assetName)

        return result

def create_gpu(assetName, path):
    assetPadding = '%s_001' % assetName
    gpuNode = mc.createNode('gpuCache')
    mc.setAttr('%s.cacheFileName' % gpuNode, path, type='string')
    transform = mc.listRelatives(gpuNode, p=True)[0]
    assetNamePadding = get_asset_name(assetPadding)
    return mc.rename(transform, assetNamePadding)

def import_polytag(assetName, path):
    from rftool.polytag import polytag_core
    assetPadding = '%s_001' % assetName
    node = polytag_core.import_asset(path)
    assetNamePadding = get_asset_name(assetPadding)
    return mc.rename(node, assetNamePadding)

def duplicate_reference(path):
    namespace = mc.file(path, q=True, namespace=True)
    newNamespace = get_namespace(namespace)
    result = mc.file(path, r=True, ns=newNamespace)
    return newNamespace

def open_file(path):
    if os.path.exists(path):
        mc.file(path,f=True,o=True)

def import_file(assetName,path):
    if os.path.exists(path):
        namespace = get_namespace('%s_001' % assetName)
        mc.file(path,i=True,ns=namespace)
        # mc.file -import -type "mayaAscii"  -ignoreVersion -ra true -mergeNamespacesOnClash false -namespace "stoolA_rig_md" -options "v=0;p=17;f=0"  -pr "C:/Users/User/Dropbox/publ_server/project/asset/setDress/furniture/stoolA/lib/stoolA_rig_md.ma";


def get_namespace(namespace):
    # asset_001
    if not mc.namespace(ex=namespace) and not mc.objExists(namespace):
        return namespace

    else:
        padding = namespace.split('_')[-1]
        if padding.isdigit():
            incrementPadding = '%03d' % (int(padding) + 1)
            newNamespace = namespace.replace(padding, incrementPadding)

        else:
            newNamespace = '%s_001' % namespace

        return get_namespace(newNamespace)

def get_namespace2(namespace, entity): 
    from rf_utils.pipeline import namespace_control as nc
    ncontrol = nc.NamespaceControl(entity)
    shotNs = ncontrol.list_namespaces()

    namespaces = mc.namespaceInfo(listOnlyNamespaces=True, recurse=True) + shotNs

    if not mc.namespace(ex=namespace) and not mc.objExists(namespace):
        return namespace

    else:
        if mc.objExists(namespace): 
            namespaces.append(namespace)
        while namespace in namespaces: 
            padding = namespace.split('_')[-1]
            if padding.isdigit():
                incrementPadding = '%03d' % (int(padding) + 1)
                newNamespace = namespace.replace(padding, incrementPadding)
            else:
                newNamespace = '%s_001' % namespace
            namespace = newNamespace
    return namespace


def import_reference_without_namespace(filename):
    """ original from rf_maya/rftool/misc/clipboard_plus/utils.py written Pia """ 
    allNs = mc.namespaceInfo(listOnlyNamespaces=True, recurse=True)
    rfns = mc.file(filename,  i=True, type='mayaAscii', options='v=0', pr=True, loadReferenceDepth='all',returnNewNodes=True ,namespace='temp_namespace')
    for rn in rfns:
        if mc.objExists(rn):
            types = mc.nodeType(rn)
            if types == 'reference' and not rn.endswith('sharedReferenceNode') :
                fm = mc.referenceQuery(rn ,filename=True)
                oldNs = mc.file( fm, q=True, namespace=True)
                ns = oldNs
                if ns in allNs:
                    while ns in allNs:
                        version = ''
                        for num in ns[::-1]:
                            if num.isdigit():
                                version = num + version
                            else:
                                break
                        if version:
                            newVersion = int(version) + 1
                            newNs = ns.replace(version, '%03d'%newVersion)
                        else:
                            newNs = ns + '_001'
                        ns = newNs
                    try:
                        mc.namespace(rename=['temp_namespace:%s'%(oldNs),ns])
                    except Exception as e:
                        logger.error(e)
                    allNs.append(ns)
    try:
        mc.namespace(removeNamespace='temp_namespace', mergeNamespaceWithRoot=True)
    except Exception as e:
        logger.error(e)


def get_namespace_from_selection(objects): 
    refPaths = []
    for obj in objects: 
        if mc.referenceQuery(obj, inr=True): 
            refPath = mc.referenceQuery(obj, f=True)

            if not refPath in refPaths: 
                refPaths.append(refPath)

    namespaces = [mc.referenceQuery(a, ns=True)[1:] for a in refPaths]
    return list(set(namespaces))


def get_name_sequencial(name):
    # asset_001
    if not mc.objExists(name):
        return name

    else:
        padding = name.split('_')[-1]
        if padding.isdigit():
            incrementPadding = '%03d' % (int(padding) + 1)
            newName = name.replace(padding, incrementPadding)

        else:
            newName = '%s_001' % name

        return get_name_sequencial(newName)


def get_asset_name(assetName):
    # asset_001
    if not mc.objExists(assetName):
        return assetName

    else:
        padding = assetName.split('_')[-1]
        if padding.isdigit():
            incrementPadding = '%03d' % (int(padding) + 1)
            newassetName = assetName.replace(padding, incrementPadding)

        else:
            newassetName = '%s_001' % assetName

        return get_asset_name(newassetName)


def export_selection(exportPath, obj=None):
    exportResult = False

    if not os.path.exists(exportPath):
        mtime = 0

    else:
        mtime = os.path.getmtime(exportPath)

    if not obj: 
        obj = mc.ls(sl=True)
    mc.select(obj)
    result = mc.file(exportPath, f=True, es=True, type='mayaAscii')

    if os.path.exists(exportPath):
        if not mtime == os.path.getmtime(exportPath):
            exportResult = True

    if exportResult:
        return result


def export_from_namespace(exportPath, namespace): 
    sels = mc.ls('*:%s' % namespace)
    export_selection(exportPath, sels)


def export_gpu(objs, dstDir, filename, time='still'):
    import asm_utils
    gpuPath = '%s/%s.abc' % (dstDir, filename)
    exportResult = False

    if not os.path.exists(gpuPath):
        mtime = 0
    else:
        mtime = os.path.getmtime(gpuPath)

    asm_utils.exportGPUCacheGrp(objs, dstDir, filename, time=time)

    if os.path.exists(gpuPath):
        if not mtime == os.path.getmtime(gpuPath):
            exportResult = True

    if exportResult:
        return gpuPath


def export_geo(objs, dstDir, filename, removeRig=True):
    import pipeline_utils
    geoPath = '%s/%s' % (dstDir, filename)
    exportResult = False

    if not os.path.exists(geoPath):
        mtime = 0
    else:
        mtime = os.path.getmtime(geoPath)

    if removeRig:
        remove_rig(objs, pipeline_utils.config.ctrlGrp)

    result = mc.file(geoPath, f=True, es=True, type='mayaAscii')

    if os.path.exists(geoPath):
        if not mtime == os.path.getmtime(geoPath):
            exportResult = True

    if exportResult:
        return geoPath


def shiftKey(type = 'default', frameRange = [1, 10000], frame = 0) :
    """ shift key frame, shift all use default mode """
    if type == 'default' :
        mm.eval('selectKey  -t (":") `ls -dag`;')

    if type == 'start' :
        mm.eval('selectKey -t ("%s:") `ls -dag`;' % frameRange[0])

    if type == 'range' :
        mm.eval('selectKey  -t ("%s:%s") `ls -dag`;' % (frameRange[0], frameRange[1]))

    mc.keyframe(e = True, iub = 0, an = 'keys', r = True, o = 'over', tc = frame)


def shiftShot(shotName, frame=0):
    """ shift individual shot """
    sequenceStartFrame = mc.getAttr('%s.sequenceStartFrame' % shotName)
    sequenceEndFrame = mc.getAttr('%s.sequenceEndFrame' % shotName)
    startFrame = mc.getAttr('%s.startFrame' % shotName)
    endFrame = mc.getAttr('%s.endFrame' % shotName)

    mc.setAttr('%s.sequenceStartFrame' % shotName, sequenceStartFrame + frame)
    mc.setAttr('%s.startFrame' % shotName, startFrame + frame)

    mc.setAttr('%s.sequenceEndFrame' % shotName, sequenceEndFrame + frame)
    mc.setAttr('%s.endFrame' % shotName, endFrame + frame)


def shiftSequencer(frame=0, mute=False):
    """ shift the whole time line """
    shots = mc.ls(type='shot')
    infoDict = dict()

    if shots:
        for shot in shots:
            sequenceStartFrame = mc.getAttr('%s.sequenceStartFrame' % shot)

            if not sequenceStartFrame in infoDict.keys():
                infoDict.update({sequenceStartFrame: [shot]})
            else:
                infoDict[sequenceStartFrame].append(shot)

        # positive move from last shot
        if frame > 0:
            shotDict = sorted(infoDict.iteritems())[::-1]

        if frame < 0:
            shotDict = sorted(infoDict.iteritems())

        for key, shots in shotDict:
            for shotName in shots:
                shiftShot(shotName, frame=frame)
                mc.shot(shotName, e=True, mute=mute)


def getTextureFile() :
    """ get texture files in the scene """
    fileNodes = mc.ls(type = 'file')
    files = []

    if fileNodes :
        for each in fileNodes :
            fileTexture = mc.getAttr('%s.fileTextureName' % each)

            fileStatus = False
            if os.path.exists(fileTexture) :
                fileStatus = True

            files.append((fileTexture, fileStatus))

    return files


def getAssemblyFiles() :
    """ get assembly node files """
    sels = mc.ls(type = 'assemblyReference')
    copyFiles = []

    if sels :
        for each in sels :
            ad = mc.getAttr('%s.definition' % each)

            if ad :
                fileStatus = False

                if os.path.exists(ad) :
                    fileStatus = True

                copyFiles.append((ad, fileStatus))

            datas = listRepIndex(each, 'data')

            if datas :
                for each in datas :
                    fileStatus = False
                    if os.path.exists(each) :
                        fileStatus = True

                    copyFiles.append((each, fileStatus))

    return copyFiles


def listRepIndex(assemblyNode, listType = 'name') :
    lists = mc.assembly(assemblyNode, q = True, listRepresentations = True)

    if listType == 'name' :
        return lists

    labels = []
    datas = []

    if lists :
        for i in range(len(lists)) :
            label = mc.getAttr('%s.representations[%s].repLabel' % (assemblyNode, i))
            data = mc.getAttr('%s.representations[%s].repData' % (assemblyNode, i))
            labels.append(label)
            datas.append(data)

    if listType == 'label' :
        return labels

    if listType == 'data' :
        return datas


def getMayaSceneAssets(mayaFile, mayaVersion, console=False) :
    """ get maya assets """
    import pipeline_utils

    pyCommand = '%s/core/maya/rftool/utils/get_asset_list.py' % os.getenv('RFSCRIPT')
    pipeline_utils.runMayaPy(pyCommand, mayaVersion, console, mayaFile)

    tmpFile = 'mayaAssetList.txt'
    tmpdir = os.getenv('TMPDIR')
    if not tmpdir:
        tmpdir = os.getenv('TMP')

    tmpPath = '%s/%s' % (tmpdir, tmpFile)

    if os.path.exists(tmpPath) :
        f = open(tmpPath, 'r')
        data = f.read()
        f.close()

        result = eval(data)
        os.remove(tmpPath)

        return result


def exportGPUCacheGrp(exportGrp, exportPath, abcName, time = 'still') :
    startFrame = 1.0
    endFrame = 1.0

    if time == 'still' :
        currentFrame = mc.currentTime(q = True)
        startFrame = currentFrame
        endFrame = currentFrame

    if time == 'animation' :
        startFrame = mc.playbackOptions(q = True, min = True)
        endFrame = mc.playbackOptions(q = True, max = True)

    # export objs
    if mc.objExists(exportGrp) :
        mc.select(exportGrp)
        result = mc.gpuCache(exportGrp,
                                startTime = startFrame,
                                endTime = endFrame,
                                optimize = True,
                                optimizationThreshold = 40000,
                                writeMaterials = True,
                                dataFormat = 'ogawa',
                                directory = exportPath,
                                fileName = abcName,
                                saveMultipleFiles = False
                                )

        gpupath = '%s/%s.abc' % (exportPath,abcName)

        return result


def find_same_poly(sel = True) :
    # find similar polygon by count faces
    sels = mc.ls(sl = True)

    if sels :
        numFace = mc.polyEvaluate(sels[0], f = True, fmt = True)

        matchPoly = []
        matchPoly.append(sels[0])

        mc.select(cl = True)
        allPlys = mc.ls(type = 'mesh', l = True)

        for each in allPlys :
            transform = mc.listRelatives(each, p = True, f = True)[0]
            currentFaceCount = mc.polyEvaluate(each, f = True, fmt = True)

            if currentFaceCount == numFace :
                matchPoly.append(transform)

        if sel :
            mc.select(matchPoly)

        return matchPoly


def find_same_poly2(obj) :
    # find similar polygon by count faces

    numFace = mc.polyEvaluate(obj, f = True, fmt = True)

    matchPoly = []
    matchPoly.append(obj)

    mc.select(cl = True)
    allPlys = mc.ls(type = 'mesh', l = True)

    for each in allPlys :
        transform = mc.listRelatives(each, p = True, f = True)[0]
        currentFaceCount = mc.polyEvaluate(each, f = True, fmt = True)

        if currentFaceCount == numFace :
            if not transform in matchPoly :
                matchPoly.append(transform)

    return matchPoly


def find_keyframe(obj): 
    """ find a key frame on an object """ 
    st = mc.playbackOptions(q=True, min=True)
    et = mc.playbackOptions(q=True, max=True)
    frames = []

    if mc.objExists(obj): 
        data = mc.keyframe(obj, time=(st, et), query=True, valueChange=False, timeChange=True)
        if data: 
            for i in range(0, len(data), 2): 
                frame = data[i]
                if not frame in frames: 
                    frames.append(frame)
            
    return frames 


def remove_rig(rigGrp, ctrlGrp):
    """ remove rig from given rigGrp """
    childs = mc.listRelatives(rigGrp)
    removeObj = [a for a in childs if ctrlGrp in a]

    # show transform
    show_hide_transform([rigGrp], state=True)

    if removeObj:
        mc.delete(removeObj)


def show_hide_transform(objs, attrs=['tx', 'ty', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz'], state=True):
    lock = not state
    for obj in objs:
        for attr in attrs:
            mc.setAttr('%s.%s' %(obj, attr), l=lock, cb=state)
            mc.setAttr('%s.%s' %(obj, attr), k=state)

def lock_transform(objs, state):
    for obj in objs:
        attrs = mc.listAttr(obj, k=True)
        for attr in attrs:
            mc.setAttr('%s.%s' % (obj, attr), l=state)

def lock_transform_rotate(objs):
    for obj in objs:
        pNode = pm.PyNode(obj)
        pNode.attr('t').lock()
        for ax in 'xyz':
            pNode.attr('t'+ax).lock()

        pNode.attr('r').lock()
        for ax in 'xyz':
            pNode.attr('r'+ax).lock()


def remove_plugins():
    unknownPlugins = mc.unknownPlugin(q=True, list=True)

    if unknownPlugins:
        # mm.eval('flushUndo;')
        for unknownPlugin in unknownPlugins:
            try:
                mc.unknownPlugin(unknownPlugin, remove=True)
                logger.info('remove %s' % unknownPlugin)
            except Exception as e:
                logger.error('%s cannot be removed' % unknownPlugin)
                logger.error(e)

        logger.info('Remove %s unknownPlugins' % len(unknownPlugins))
        return
    logger.info('No unknownPlugins')


def get_ref_path(obj) :
    return mc.referenceQuery(obj, f = True)


def get_node_namespace(obj) :
    path = get_ref_path(obj)
    return mc.file(path, q = True, namespace = True)


def list_top_parent(obj):
    parents = mc.listRelatives(obj, p=True)
    # take first parent in the list
    if parents:
        return list_top_parent(parents[0])
    else:
        return obj

def list_all_namespaces():
    exceptionList = ['UI', 'shared']
    ns = mc.namespaceInfo(listOnlyNamespaces=True, recurse=True)
    return [a for a in ns if not a in exceptionList] if ns else []


def find_ply(transform, f=False):
    """ find polygon under transform """
    childs = mc.listRelatives(transform, ad=True, f=True)

    if childs:
        meshes = list(set([mc.listRelatives(a, p=True, f=f)[0] for a in childs if mc.objectType(a, isType='mesh')]))
        return meshes


def add_prefix(transform, prefix, remove='', hi=True):
    """ rename prefix """
    mc.select(transform, hi=True)
    hierarchy = mc.ls(sl=True, l=True)

    for node in hierarchy[::-1]:
        if mc.objectType(node, isType='transform'):
            shortName = node.split('|')[-1]
            if remove:
                shortName = shortName.replace(remove, '')
            newName = '%s%s' % (prefix, shortName)
            mc.rename(node, newName)

    mc.select(cl=True)


def find_parent(obj):
    return mc.listRelatives(obj, p=True)

def parent_order(child, parent, order=0):
    result = child
    if parent:
        childParent = mc.listRelatives(child, p=True)
        if childParent:
            if not childParent[0] == parent:
                result = mc.parent(child, parent)
        else:
            result = mc.parent(child, parent)

    mc.reorder(result, f=True)
    mc.reorder(result, r=order)
    return result


def outliner_order(ply):
    ln = mc.ls(ply, l=True)[0]
    parent = mc.listRelatives(ply, p=True, f=True)
    if parent:
        childs = mc.listRelatives(parent, c=True, f=True)
    if not parent:
        childs = mc.ls(assemblies=True, l=True)

    # print parent, childs

    return childs.index(ln) or 0


def cleanNamespace():
    namespaces = mc.namespaceInfo(lon=True,r=True,an=True)

    nsFile = []
    for f in mc.file(q=True,r=True):
        nsFile.append(mc.file(f,q=True,ns=True))

    if namespaces:
        namespaces = [name for name in namespaces
                                if not ':UI' in name
                                if not ':shared' in name]

    removeNamespace = []

    for name in namespaces:
        flag = 0
        for ns in nsFile:
            if ns in name:
                flag = 1
                break
        if flag == 0:
            removeNamespace.append(name)

    print removeNamespace
    if removeNamespace:
        for name in removeNamespace:
            try:
                if not mc.ls("%s:*"%name[1:]):
                    nm = name.split(':')[1:]
                    for i in range(len(nm)):
                        sub = (':').join(nm[:(len(nm)-i)])
                        mc.namespace(f=True,rm=sub)
                        print sub,'removed.'

                else:
                    #mc.lockNode("%s:*"%name[1:],l=0)
                    mc.namespace(f=True,mv=(name,':'))
                    mc.namespace(f=True,rm=name)
                    print name,'removed.'
            except:
                # print traceback.print_exc()
                pass
        cleanNamespace()
    else:
        print '\n__________________\nNamespace Cleaned.\n_________________\n'


def cleanNamespace2():
    lockName = [u':UI', u':shared']
    names = [n for n in mc.namespaceInfo(lon=True,r=True,an=True) if not 'UI' in n or not 'shared' in n]
    for name in mc.namespaceInfo(lon=True,r=True,an=True):
        try:
            if not mc.ls("%s:*" %name[1:]):
                nm = name.split(':')[1:]
                for i in range(len(nm)):
                    sub = (':').join(nm[:(len(nm)-i)])
                    mc.namespace(f=True,rm=sub)
                    print sub,'removed.'

            else:
                #mc.lockNode("%s:*"%name[1:],l=0)
                mc.namespace(f=True,mv=(name,':'))
                mc.namespace(f=True,rm=name)
        except:
            pass
    if mc.namespaceInfo(lon=True,r=True,an=True):
        if not lockName == mc.namespaceInfo(lon=True,r=True,an=True):
            cleanNamespace()

def removeNamespace(namespace):
    print 'Remove Namespace :',namespace
    try:
        namespace = ':%s'%namespace
        if not mc.ls("%s:*"%namespace):
            nm = namespace.split(':')[1:]
            for i in range(len(nm)):
                sub = (':').join(nm[:(len(nm)-i)])
                mc.namespace(f=True,rm=sub)
                print sub,'removed.'

        else:
            #mc.lockNode("%s:*"%name[1:],l=0)
            mc.namespace(f=True,mv=(namespace ,':'))
            mc.namespace(f=True,rm=namespace)
    except:
        #print traceback.print_exc()
        pass

    namespaces = mc.namespaceInfo(lon=True,r=True,an=True)
    if namespaces:
        namespaces = [name for name in namespaces
                                if not ':UI' in name
                                if not ':shared' in name]
    if namespace in namespaces:
        removeNamespace(namespace)


def find_top_reference(node):
    parentNode = mc.listRelatives(node, p=True)
    if parentNode:
        if mc.referenceQuery(parentNode, inr=True):
            return find_top_reference(parentNode)

        else:
            return node
    else:
        return node

def find_top_reference_transform(namespace): 
    refTopNodes = []
    topNodes = mc.ls(assemblies=True)
    refNodes = mc.ls('%s:*' % namespace, type='transform')
    if refNodes: 
        refTopNodes = [a for a in refNodes if a in topNodes]
    return refTopNodes 


def remove_reference(path):
    rnNode = mc.referenceQuery(path, referenceNode=True)

    if not mc.referenceQuery(path, isLoaded=True):
        mc.file(path, loadReference=rnNode)

    mc.file(path, rr=True)


def remove_unloaded_references():
    refPaths = mc.file(q=True, r=True)

    if refPaths:
        removeRefs = [a for a in refPaths if not mc.referenceQuery(a, isLoaded=True)]

        for path in removeRefs:
            remove_reference(path)

def select_viewport_objects(expand=0):
    import maya.OpenMaya as om
    import maya.OpenMayaUI as omUI
    view = omUI.M3dView.active3dView()
    om.MGlobal.selectFromScreen(0 + (-expand), 0 + (-expand), view.portWidth() + expand, view.portHeight() + expand, om.MGlobal.kAddToList)

def select_viewport_objects_replace(expand=0):
    import maya.OpenMaya as om
    import maya.OpenMayaUI as omUI
    view = omUI.M3dView.active3dView()
    om.MGlobal.selectFromScreen(0 + (-expand), 0 + (-expand), view.portWidth() + expand, view.portHeight() + expand, om.MGlobal.kReplaceList)

def selectionMask(mode, state):
    """available mode Marker = locator, Joint = joint, Surface = geometry Curve = curve, state = 1, 0 """
    mm.eval('setObjectPickMask "%s" %s;' % (mode, state))


def remove_offcam_references(timeline=True, step=10):
    refPaths = get_viewport_references(timeline=True, step=10)

    if refPaths:
        allRefs = mc.file(q=True, r=True)
        removeRefs = [a for a in allRefs if not a in refPaths]

        for path in removeRefs:
            remove_reference(path)


def remove_unselected_references():
    sels = mc.ls(sl=True, l=True)

    if sels:
        refPaths = list(set([mc.referenceQuery(a, f=True) for a in sels if mc.referenceQuery(a, inr=True)]))
        allRefs = mc.file(q=True, r=True)
        removeRefs = [a for a in allRefs if not a in refPaths]

        for path in removeRefs:
            remove_reference(path)


def remove_selected_references():
    sels = mc.ls(sl=True, l=True)

    if sels:
        refPaths = list(set([mc.referenceQuery(a, f=True) for a in sels if mc.referenceQuery(a, inr=True)]))

        for path in refPaths:
            remove_reference(path)



def get_viewport_references(timeline=True, step=10):
    currentTime = int(mc.currentTime(q=True))
    startFrame = int(mc.playbackOptions(q=True, min=True))
    endFrame = int(mc.playbackOptions(q=True, max=True))

    if timeline:
        objs = select_object_range(startFrame, endFrame, step=10)

    else:
        objs = select_object_range(currentTime, currentTime+1, step=1)

    if objs:
        refPaths = list(set([mc.referenceQuery(a, f=True) for a in objs if mc.referenceQuery(a, inr=True)]))
        return refPaths


def select_object_range(startFrame, endFrame, step=10, expand=0):
    # turn off mask
    currentTime = mc.currentTime(q=True)
    selectionMask('Marker', 0)
    selectionMask('Joint', 0)
    selectionMask('Curve', 0)
    selectionMask('Surface', 1)
    allObjs = []

    # clear selection
    mc.select(cl=True)

    for frame in range(startFrame, endFrame, step):
        mc.currentTime(frame)
        print frame
        # select
        select_viewport_objects(expand)
        objs = mc.ls(sl=True, l=True)
        mc.select(objs, add=True)

    mc.currentTime(currentTime)
    sels = mc.ls(sl=True, l=True)
    return sels


def get_transform(type='mesh', reference=False):
    meshes = mc.ls(type=type)

    shapes = [a for a in meshes if mc.referenceQuery(a, inr=True) == reference]
    transforms = [mc.listRelatives(a, p=True, f=True)[0] for a in shapes if mc.listRelatives(a, p=True)]
    return transforms


def remove_displayLayer():
    layers = mc.ls(type='displayLayer')

    if layers:
        nonRefs = [a for a in layers if not mc.referenceQuery(a, inr=True)]

        if nonRefs:
            nonRefs.remove('defaultLayer')
            mc.delete(nonRefs)


def remove_reference_edits(path, successfulEdits, failedEdits, editCommand, keyword=None):
    rnNode = mc.referenceQuery(path, referenceNode=True)
    targetNodes = mc.referenceQuery(rnNode, editNodes=True, editAttrs=True, successfulEdits=successfulEdits, failedEdits=failedEdits, editCommand=editCommand)

    for node in targetNodes:
        remove = True
        if keyword:
            if not keyword in node:
                remove = False

        # print 'remove', remove
        # print node

        if remove:
            mc.referenceEdit(node, failedEdits=failedEdits, successfulEdits=successfulEdits, removeEdits=True, editCommand=editCommand)


def remove_shader_override(path, alwaysLoad=True):
    rnNode = mc.referenceQuery(path, referenceNode=True)
    isLoaded = mc.referenceQuery(path, isLoaded=True)
    
    if isLoaded: 
        # unload
        mc.file(path, unloadReference=rnNode)

    # remove connectAttr / disconnectAttr
    remove_reference_edits(path, successfulEdits=True, failedEdits=True, editCommand='connectAttr', keyword='instObjGroups')
    remove_reference_edits(path, successfulEdits=True, failedEdits=True, editCommand='disconnectAttr', keyword='instObjGroups')

    # load back
    try:
        if isLoaded or alwaysLoad: 
            mc.file(path, loadReference=rnNode, prompt=False)
    except Exception as e:
        logger.error(e)


def reload_reference(path): 
    rnNode = mc.referenceQuery(path, referenceNode=True)
    mc.file(path, unloadReference=rnNode)
    mc.file(path, loadReference=rnNode, prompt=False)


def remove_failed_edits(path):
    rnNode = mc.referenceQuery(path, referenceNode=True)
    failedNodes = mc.referenceQuery(rnNode, editNodes=True, editAttrs=True, successfulEdits=False, failedEdits=True)

    for node in failedNodes:
        mc.referenceEdit(node, failedEdits=True, removeEdits=True)


def compare_hierarchy(data, targetData):
    wrongOrders = []
    removes = [a for a in data if not a in targetData]
    adds = [a for a in targetData if not a in data]

    return adds, removes


def hierarchy_data(node, shape=True, includeRoot=False):
    """ get hierarchy data from top node """
    nodes = mc.ls(node, dag=True, l=True) if shape else mc.ls(node, dag=True, type='transform', l=True)
    if not includeRoot:
        nodes = [('|').join(a.split('|')[2:]) for a in nodes if a.split('|')[2:]]
    return nodes

def remove_data_namespace(namespace, data):
    """ remove namespace from long name """
    newData = []
    if not namespace[-1] == ':':
        namespace = '%s:' % namespace

    for node in data:
        name = ('|').join([a.replace(namespace, '') for a in node.split('|') if namespace in a])
        newData.append(name)
    return newData


def make_space(step=100, x=True, y=False, z=False):
    sels = mc.ls(sl=True)

    for i, sel in enumerate(sels):
        cal = i*step
        xv = cal if x else 0
        yv = cal if y else 0
        zv = cal if z else 0

        mc.xform(sel, ws=True, r=True, t=(xv, yv, zv))


def rebuild_asset(root, obj):
    """ export anim curve from given root, remove reference and re-import and apply animCurve """
    sel = mc.ls(obj)
    tmp = os.environ.get('temp')
    animFile = 'rebuild_tmp.anim'
    path = '%s/%s' % (tmp, animFile)

    if sel:
        filePath = mc.referenceQuery(sel[0], f=True)
        namespace = mc.referenceQuery(sel[0], namespace=True).split(':')[-1]

        srcRoot = '%s:%s' % (namespace, root)
        path = export_anim(srcRoot, path)
        topNode = find_top_reference(srcRoot)
        topParent = mc.listRelatives(topNode, p=True)
        order = outliner_order(topNode)

        mc.file(filePath, rr=True)
        mc.file(filePath.split('{')[0], r=True, ns=namespace)
        dstRoot = '%s:%s' % (namespace, root)
        import_anim(dstRoot, path)
        parent_order(topNode, topParent, order=order)



def export_anim(node, path):
    if not os.path.exists(os.path.dirname(path)):
        os.makedirs(os.path.dirname(path))
    mc.select(node)
    result = mc.file(path, f=True, options='precision=8;intValue=17;nodeNames=1;verboseUnits=0;whichRange=1;range=0:10;options=keys;hierarchy=below;controlPoints=0;shapes=1;helpPictures=0;useChannelBox=0;copyKeyCmd=-animation objects -option keys -hierarchy below -controlPoints 0 -shape 1', typ='animExport', pr=True, es=True)
    return result

def import_anim(node, path):
    mc.select(node)
    result = mc.file(path, i=True, type='animImport', ignoreVersion=True, options='targetTime=4;copies=1;option=replace;pictures=0;connect=0', pr=True, importTimeRange='combine')
    return result


def move_texture_path(dstTexturePath, targetGrp, resize=None, relink=True):
    """ relink texture to the asset including UDIM """
    # under rigGrp
    from stat import S_IREAD, S_IWUSR

    nodes = find_texture_node(targetGrp=targetGrp)
    textures = collect_textures(targetGrp=targetGrp)
    # rsNormalMaps = find_rsNormal_node()
    successCopy = []
    pathDict = dict()

    if not os.path.exists(dstTexturePath):
        os.makedirs(dstTexturePath)
        logger.debug('Create dir for %s' % dstTexturePath)

    for srcTexture in textures:
        basename = os.path.basename(srcTexture)
        dstPath = '%s/%s' % (dstTexturePath, basename)

        # copy
        if os.path.exists(srcTexture):
            if os.path.exists(dstPath):
                os.chmod(dstPath, S_IWUSR|S_IREAD)
                if not os.stat(srcTexture) == os.stat(dstPath): ###check same file#os.path.samefile(srcTexture, dstPath):
                    if not dstTexturePath in srcTexture:
                        copy_resize(srcTexture, dstPath, resize=resize)
                        logger.debug('Copy success : %s -> %s' % (srcTexture, dstPath))
            else:
                if os.path.exists(srcTexture):
                    copy_resize(srcTexture, dstPath, resize=resize)
                    logger.debug('Copy success : %s -> %s' % (srcTexture, dstPath))
        else:
            logger.warning('Texture missing : %s' % srcTexture)

        if os.path.exists(dstPath):
            successCopy.append(dstPath)

    logger.debug('%s/%s files copied' % (len(successCopy), len(textures)))


    # relink
    for node in nodes:
        path = ''
        if mc.objectType(node, isType ='file'):
            path = mc.getAttr('%s.fileTextureName' % node)
        elif mc.objectType(node, isType ='RedshiftNormalMap') :
            path = mc.getAttr('%s.tex0' % node)
        elif mc.objectType(node, isType ='ReshiftSprite') :
            path = mc.getAttr('%s.tex0' % node)

        # skips file node with empty path string
        if not path:
            continue
        
        if not dstTexturePath in path:        
            newPath = '%s/%s' % (dstTexturePath, os.path.basename(path))
        else:
            newPath = path
            
        if os.path.exists(newPath):
            if relink:
                if mc.objectType(node, isType ='file'):
                    mc.setAttr('%s.fileTextureName' % node, newPath, type='string')
                    logger.info('relink success %s' % newPath)
                elif mc.objectType(node, isType ='RedshiftNormalMap'):
                    mc.setAttr('%s.tex0' % node, newPath, type='string')
                    logger.info('relink success %s' %  newPath )
                elif mc.objectType(node, isType ='RedshiftSprite'):
                    mc.setAttr('%s.tex0' % node, newPath, type='string')
                    logger.info('relink success %s' %  newPath )

        pathDict[path] = newPath

    # in case of Redshift 
    # for node in rsNormalMaps: 
    #     path = mc.getAttr('%s.tex0' % node)
    #     newPath = '%s/%s' % (dstTexturePath, os.path.basename(path))
    #     pathDict[path] = newPath

    return pathDict


def copy_resize(src, dst, resize=False):
    """ copy and resize """
    from rf_utils import file_utils
    if not resize:
        return file_utils.copy(src, dst)
        # return shutil.copy2(src, dst)

    resize_texture(src, dst, resize, resize)

    if os.path.exists(dst):
        return dst


def collect_textures(targetGrp=None, objs=None):
    nodes = find_texture_node(targetGrp=targetGrp, objs=objs)
    allTextures = []
    if nodes:
        for node in nodes:
            if mc.objectType(node, isType ='file'):
                allTextures += get_texture_files(node)
            if mc.objectType(node, isType ='RedshiftNormalMap'):
                allTextures += get_texture_files(node, textureAttr='tex0')
            if mc.objectType(node, isType ='RedshiftSprite'):
                allTextures += get_texture_files(node, textureAttr='tex0')

    return allTextures

def find_rsNormal_node(): 
    nodes = mc.ls(type='RedshiftNormalMap')
    return nodes 


def resize_texture(sourceImage, outputImage, width, height):
    """ resize texture """
    format = sourceImage.split('.')[-1]
    image = om.MImage()
    image.readFromFile('%s' %sourceImage)
    image.resize(width, height)
    try:
        image.writeToFile('%s' % outputImage , format)
    except RuntimeError, e:
        print 'Failed to resize: %s' %outputImage

def get_texture_size(sourceImage):
    """ get image dimension """
    image = om.MImage()
    image.readFromFile('%s' % sourceImage)

    scriptUtil = om.MScriptUtil()
    width = om.MScriptUtil()
    height = om.MScriptUtil()
    width.createFromInt(0)
    height.createFromInt(0)
    pWidth = width.asUintPtr()
    pHeight = height.asUintPtr()
    image.getSize(pWidth, pHeight)
    vWidth = width.getUint(pWidth)
    vHeight = height.getUint(pHeight)

    return vWidth, vHeight


def find_texture_node(targetGrp=None, objs=None):
    from rftool.shade import shade_utils
    matteInfo = shade_utils.list_shader(targetGrp=targetGrp, objs=objs)
    shadingEngines = [v['shadingEngine'] for k, v in matteInfo.iteritems()]
    fileNodes = []

    if matteInfo:
        for shader in matteInfo:
            downstreamNodes = mc.listHistory(shader)
            fileNodes += [a for a in downstreamNodes if mc.objectType(a, isType='file') or mc.objectType(a, isType='RedshiftNormalMap') or mc.objectType(a, isType='RedshiftSprite')]

    # find displacement connected to shader 
    files = [a for a in mc.ls(type='file') if find_se_from_file(a) in shadingEngines]
    fileNodes += files

    return list(set(fileNodes))


def find_se_from_file(node): 
    nodes = mc.listHistory(node, f=True, ac=True)
    result = [a for a in nodes if mc.objectType(a, isType='shadingEngine')]
    return result[0] if result else None


def get_linked_texture(fileNode):
    """ get linked texture from file node """
    path = mc.getAttr('%s.fileTextureName' % fileNode)
    files = [path]
    mode = mc.getAttr('%s.uvTilingMode' % fileNode)

    if mode == 3:
        files = find_udim_files(path)

    return files


# def find_udim_files(path):
#     # detect UDIM format
#     # AncienStone_Diffuse.<UDIM>.png
#     from rf_utils import file_utils
#     key = '.'
#     path = path.replace('\\', '/')

#     basename = os.path.basename(path)
#     prefixKey = basename.split(key)[0]
#     dirname = os.path.dirname(path)
#     ext = basename.split('.')[-1]
#     files = file_utils.listFile(dirname, ex=ext)

#     if files:
#         udimFiles = ['%s/%s' % (dirname, a) for a in files if prefixKey in a]
#         return udimFiles


def flat_ref():
    """ flat one level reference """
    refs = mc.file(q=True, r=True)
    for ref in refs:
        childs = find_child(ref)
        if childs:
            mc.file(ref, importReference=True)

def flat_all_ref():
    """ flat all references """
    refs = [a for a in mc.file(q=True, r=True) if find_child(a)]
    if refs:
        for ref in refs:
            mc.file(ref, importReference=True)
        return flat_all_ref()
    else:
        return True


def find_child(ref):
    """ find child references """
    rnNode = mc.referenceQuery(ref, referenceNode=True)
    childs = mc.referenceQuery(rnNode, f=True, ch=True)
    return childs if childs else False

def list_all_ref_namespace():
    ref_namespaces = []
    import pymel.core as pm
    refs = pm.listReferences()
    for ref in refs:
        ref_namespaces.append(ref.namespace)
    return ref_namespaces


def find_udim_files(path):
    import maya.app.general.fileTexturePathResolver as ftpr
    useFrame = False
    frameNumber = None
    uvMode = 3 # UDIM

    pattern = ftpr.getFilePatternString(path , useFrame , uvMode)
    allFiles = ftpr.findAllFilesForPattern(pattern , frameNumber)
    return allFiles


def get_texture_files(node, textureAttr='fileTextureName'):
    import maya.app.general.fileTexturePathResolver as ftpr
    txrPath = mc.getAttr('%s.%s' % (node, textureAttr))
    uvMode = mc.getAttr('%s.uvTilingMode' % node) if mc.objExists('%s.uvTilingMode' % node) else 0

    useFrame = mc.getAttr('%s.useFrameExtension'%(node)) if mc.objExists('%s.useFrameExtension'%(node)) else False
    frameNumber = None

    pattern = ftpr.getFilePatternString(txrPath , useFrame , uvMode)
    allFiles = ftpr.findAllFilesForPattern(pattern , frameNumber)
    return allFiles

def relink_texture_node(nodes, replaceDict): 
    import maya.app.general.fileTexturePathResolver as ftpr
    for node in nodes: 
        txrPath = mc.getAttr('%s.fileTextureName' % node)
        uvMode = mc.getAttr('%s.uvTilingMode' % node) if mc.objExists('%s.uvTilingMode' % node) else 0

        if not uvMode == 0: 
            allFiles = ftpr.findAllFilesForPattern(txrPath , None)
            if allFiles: 
                if all(a in replaceDict.keys() for a in allFiles): 
                    newPath = replaceDict[allFiles[0]]
                    # find udim pattern 
                    newPath = ftpr.getFilePatternString(newPath , False , uvMode)
                    # relink 
                    mc.setAttr('%s.fileTextureName' % node, newPath, type='string')
                    logger.debug('Relinked %s' % newPath)

        else: 
            if txrPath in replaceDict.keys(): 
                newPath = replaceDict[txrPath]

                # relink 
                mc.setAttr('%s.fileTextureName' % node, newPath, type='string')
                logger.debug('Relinked %s' % newPath)



def check_poly_key():
    sels = mc.ls(sl=True)
    sels = [a for a in sels if mc.referenceQuery(a, inr=True)]
    if sels:
        for obj in sels:
            shape = mc.listRelatives(obj, s=True, f=True)
            if shape:
                if mc.objectType(shape[0], isType='mesh'):
                    if mc.listConnections('%s.rx' % obj):
                        logger.warning('Polygon object "%s" has key frame' % obj)
                        return obj
            else:
                logger.warning('Polygon object "%s" has key frame' % obj)
                return obj


def create_center_loc():
    cluster = mc.cluster()
    loc = mc.spaceLocator()
    mc.delete(mc.parentConstraint(cluster, loc))
    mc.delete(cluster)


def generate_preview(res=256): 
    mc.setAttr('hardwareRenderingGlobals.textureMaxResolution', res)
    for fileNode in mc.ls(type='file'): 
        # mm.eval('generateUvTilePreview %s;' % fileNode)
        if not mc.getAttr('%s.uvTilingMode' % fileNode) == 0 and not mc.getAttr('%s.uvTileProxyQuality' % fileNode) == 0: 
            mc.ogs(regenerateUVTilePreview=fileNode)


def convert_rdi_info(rdi_line, info):
    rdi_split = rdi_line.split(' ')
    key = rdi_line.split('"mayaAscii"')[1].strip(' ''\"')
    attr = {}
    for idx, val in enumerate(rdi_split):
        if "-" in val:
            if "mayaAscii" in rdi_split[idx + 1]:
                rdi_split[idx + 1] = "mayaAscii"
            attr_val = {
                val: rdi_split[idx + 1].strip('\"')
            }
            attr.update(attr_val)

    info.update({key: attr})


def get_file_reference_info(path):
    txt_file = ""
    info = {}
    start_read = False
    with open(path, 'r') as _file:
        for line in _file:
            if "file -rdi" in line:
                start_read = True
            if start_read:
                txt_file += "".join(line.strip())
            if "requires" in line:
                break

        txt_file_filtered = txt_file.split(";")
        for line in txt_file_filtered:
            if "file -rdi" in line:
                convert_rdi_info(line, info)
    # file -rdi 1 -ns "warriorA_001" -dr 1 -rfn "warriorA_001RN" -typ "mayaAscii"
    #      "P:/SevenChickMovie/asset/publ/char/warriorA/hero/warriorA_rig_main_md.hero.ma";
    # file -rdi 1 -ns "arthur_mdlMainMtr" -rfn "arthur_mdlMainMtrRN" -typ "mayaAscii"
    #      "$RFPROJECT/SevenChickMovie/asset/publ/char/arthur/hero/arthur_mtr_rig_main_md.hero.ma";
    # info = {'$RFPROJECT/SevenChickMovie/asset/publ/char/arthur/hero/arthur_mtr_rig_main_md.hero.ma': 
    # {'ns': 'arthur_001', 'dr': 1, 'rfn': 'arthur_001RN', 'typ': 'mayaAscii'}, 
    #         'P:/SevenChickMovie/asset/publ/char/warriorA/hero/warriorA_rig_main_md.hero.ma': 
    # {'ns': 'arthur_001', 'dr': 1, 'rfn': 'warriorA_001RN', 'typ': 'mayaAscii'}}
    return info 

def removeDagPathNamespace(path):
    root = False
    if path.startswith('|'):
        root = True
        path = path[1:]
    splits = path.split('|')
    cleaned_elems = []
    for s in splits:
        if '.f[' in s:
            fSplit = s.split('.f[')
            elem = fSplit[0]
            fidx = '.f[%s' %fSplit[1]
        else:
            elem = s
            fidx = ''

        elem = '%s%s' %(elem.split(':')[-1], fidx)
        cleaned_elems.append(elem)

    result = '|'.join(cleaned_elems)
    if root:
        result = '|%s' %(result)
    return result

def addDagPathNamespace(path, namespace):
    root = False
    if path.startswith('|'):
        root = True
        path = path[1:]
    splits = path.split('|')

    result = '|'.join(['%s%s' %(namespace, s) for s in splits])
    if root:
        result = '|%s' %(result)
    return result


def viewport_display(panel='', **kwargs): 
    # -allObjects(-alo)   
    # -cameras(-ca)   
    # -controlVertices(-cv)   
    # -deformers(-df)   
    # -dimensions(-dim)   
    # -dynamicConstraints(-dc)   
    # -dynamics(-dy)   
    # -fluids(-fl)   
    # -follicles(-fo)   
    # -grid(-gr)   
    # -hairSystems(-hs)   
    # -handles(-ha)   
    # -hulls(-hu)   
    # -ikHandles(-ikh)   
    # -imagePlane(-imp)   
    # -joints(-j)   
    # -lights(-lt)   
    # -locators(-lc)   
    # -manipulators(-m)   
    # -motionTrails(-mt)   
    # -nCloths(-ncl)   
    # -nParticles(-npa)   
    # -nRigids(-nr)   
    # -nurbsCurves(-nc)   
    # -nurbsSurfaces(-ns)   
    # -pivots(-pv)   
    # -planes(-pl)   
    # -pluginObjects(-po)   
    # -pluginShapes(-ps) 
    # -polymeshes(-pm)   
    # -shadows(-sdw)   
    # -strokes(-str)   
    # -subdivSurfaces(-sds)   
    # -textures(-tx)   
    
    panel = get_panel() if not panel else panel
    for k, v in kwargs.iteritems(): 
        eval('mc.modelEditor(panel, e=True, %s=v)' % (k))


def get_parents(obj): 
    ln = mc.ls(obj, l=True)
    elems = ln[0].split('|')
    parents = [ln[0]]
    
    for i in range(len(elems))[::-1]: 
        tempList = elems[0: i]
        if tempList: 
            upperParent = ('|').join(tempList)
            parents.append(upperParent)

    return parents

def get_panel():
    vis_panel = mc.getPanel(vis =1)
    for view in vis_panel:
        if mc.getPanel(typeOf=view) == 'modelPanel':
            current_view = view

    return current_view

def lock_ref_cam(rnNode='', namespace='', camera='', state=1): 
    ns = ''
    cam = ''
    camNode = None
    if rnNode: 
        ns = pm.referenceQuery(rnNode, ns=True)
    if namespace: 
        ns = namespace if namespace[0] == ':' else ':%s' % namespace
    if ns: 
        cam = [a for a in pm.ls('%s:*' % ns[1:]) if a.nodeType() == 'camera']
    if cam: 
        camNode = cam[0].getParent()

    if camera: 
        node = pm.PyNode(camera)
        if node.nodeType() == 'camera': 
            camNode = node.getParent()
        elif node.getShape().nodeType() == 'camera': 
            camNode = node

    if camNode: 
        attrs = ['t', 'r', 's', 'tx', 'ty', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz']

        for attr in attrs: 
            camNode.attr(attr).lock() if state else camNode.attr(attr).unlock()


def list_top_namespace(namespace): 
    contents = mc.ls('%s:*' % namespace, l=True)
    topNodes = []
    singleNodes = []
    minCount = 100
    for content in contents: 
        if '|' in content:
            count = len(content.split('|'))
            if count < minCount: 
                minCount = count
                topNodes = [content] 
            elif count == minCount: 
                topNodes.append(content)
        if not '|' in content:
            singleNodes.append(content)

    return singleNodes+topNodes

def getCurrentViewportRenderer():
    import maya.OpenMayaUI as omui
    view = omui.M3dView.active3dView()
    renderer = view.rendererString()
    if renderer == 'base_OpenGL_Renderer':
        return 'legacy'
    elif renderer == 'vp2Renderer':
        return 'vp2'


def fix_shade(rigGrps, *args):
    from rftool.shade import shade_utils
    for rigGrp in rigGrps:
        trs = find_ply(rigGrp)
        # maya_utils.remove_shader_override
        if trs:
            for tr in trs:
                shapes = mc.listRelatives(tr, s=True)
                if not shapes:
                    continue
                sgs = mc.listConnections(shapes[0], type='shadingEngine')
                if sgs and 'initialShadingGroup' not in sgs:
                    shade_utils.reconnect_selected_shader(tr)
                    break
            else:
                shade_utils.reconnect_selected_shader(trs[0])


def fix_namespace(): 
    """ fix selection namespace """ 
    pass 


def list_node_downstream(rootnode): 
    import maya.OpenMaya as om
    # rootnode = mc.ls(sl=True)[0]
    nodes = []
    sellist = om.MSelectionList()
    sellist.add(rootnode)
    mobject = om.MObject()
    sellist.getDependNode(0, mobject)

    dgiter = om.MItDependencyGraph(
        mobject, om.MItDependencyGraph.kDownstream, om.MItDependencyGraph.kPlugLevel)

    while not dgiter.isDone():
        current_item = dgiter.currentItem()
        dgfn = om.MFnDependencyNode(current_item)
        nodes.append(dgfn.name())
        dgiter.next()

    return nodes 
    

class LatticeParent():
    def __init__(self, root_ctrl, geo_grp):
        self.root_ctrl = root_ctrl if isinstance(root_ctrl, pm.PyNode) else pm.PyNode(root_ctrl)
        self.geo_grp = geo_grp if isinstance(geo_grp, pm.PyNode) else pm.PyNode(geo_grp)
        self.parents = []

    def __enter__(self):
        # find all geo under geo_grp
        ffd_nodes = set()
        has_ffd = False
        for c in self.geo_grp.getChildren(ad=True, type='transform'):
            shp = c.getShape(ni=True)
            if not shp:
                continue
            if not shp.isReferenced():  # Deformed shape is not referenced
                hiss = shp.listHistory()
                for his in hiss:
                    his_type = pm.nodeType(his)
                    if his_type == 'ffd':
                        has_ffd = True
                        ffd_nodes.add(his)
            if has_ffd:
                break
        if has_ffd:
            for node in ffd_nodes:
                base_ffds = node.baseLatticeMatrix.inputs()
                if base_ffds:
                    base_parent = base_ffds[0].getParent()
                    self.parents.append([base_ffds[0], base_parent])
                    pm.parent(base_ffds[0], self.root_ctrl)

                deform_ffds = node.deformedLatticeMatrix.inputs()
                if deform_ffds:
                    deform_parent = deform_ffds[0].getParent()
                    self.parents.append([deform_ffds[0], deform_parent])
                    pm.parent(deform_ffds[0], self.root_ctrl)

    def __exit__(self, *args):
        for child, parent in self.parents:
            if not parent:
                pm.parent(child, w=True)
            else:
                pm.parent(child, parent)

class FreezeViewport():
    VP2_NAME = 'vp2Renderer'
    def __init__(self, mode='vp2'): 
        self.mode = mode 

    def __enter__(self):
        if mc.about(batch=True):
            return
        if self.mode == 'vp2': 
            # collect auto keyframe setting
            self.resetAutoKey = mc.autoKeyframe(query=True, state=True)
            # turn off auto keyframe
            mc.autoKeyframe(state=False)

            # collect old rederer settings and switch all to VP2
            self.renderers = {}
            for panel in mc.lsUI(editors=True):
                if panel.find('modelPanel') != -1:
                    renderer = mc.modelEditor(panel, q=True, rnm=True)
                    self.renderers[panel] = renderer
                    if renderer != self.VP2_NAME:  # switch to VP2
                        try:
                            mc.modelEditor(panel, e=True, rendererName=self.VP2_NAME)
                        except:
                            pass
            # pause VP2
            if not mc.ogs(query=True, pause=True):
                mc.ogs(pause=True)

        elif self.mode == 'legacy': 
            mc.refresh(su=True)

    def __exit__(self, *args):
        if mc.about(batch=True):
            return
        if self.mode == 'vp2': 
            # un-pause VP2
            if mc.ogs(query=True, pause=True):
                mc.ogs(pause=True)
                
            # restore old settings
            for panel, renderer in self.renderers.iteritems():
                try:
                    mc.modelEditor(panel, e=True, rendererName=renderer)
                except:
                    pass
            mc.autoKeyframe(state=self.resetAutoKey)

        elif self.mode == 'legacy': 
            mc.refresh(su=False)
            mc.refresh(f=True)


class IsolateGeo():
    """docstring for IsolateGeoRes"""
    def __init__(self, grp='Geo_Grp', isolateGrp='MdGeo_Grp', run=True):
        self.run = run 
        self.grp = grp
        self.isolateGrp = isolateGrp
        self.tmpGrpName = 'tmpIsolateGrp'

    def __enter__(self): 
        # unparent other groups to tmpGrp 
        if self.run: 
            self.grp = pm.PyNode(self.grp) if pm.objExists(self.grp) else None 
            self.isolateGrp = pm.PyNode(self.isolateGrp) if pm.objExists(self.isolateGrp) else None 
            self.run = all([self.grp, self.isolateGrp])
                
            if self.run: 
                self.tmpGrp = toggle_group(self.tmpGrpName, True)
                self.members = pm.listRelatives(self.grp, c=True)
                for member in self.members: 
                    if not member == self.isolateGrp: 
                        pm.parent(member, self.tmpGrp)

    def __exit__(self, *args): 
        # reparent back to original group 
        if self.run: 
            objs = pm.listRelatives(self.tmpGrp, c=True)
            members = [str(a) for a in self.members]

            for obj in objs: 
                order = members.index(str(obj))
                pm.parent(obj, self.grp)
                pm.reorder(obj, f=True)
                pm.reorder(obj, r=order)

            toggle_group(self.tmpGrp, False)

class DisconnectImgSeq(): 
    def __init__(self, nodes): 
        self.nodes = nodes
        self.expList = {}
        for node in self.nodes:
            # val = mc.getAttr('%s.useFrameExtension' % node)
            print node
            expression = mc.listConnections('%s.frameExtension'% node ,type='expression')
            if expression:
                exp = expression[0]
                # if val == 1:
                #     print 'CHECK'
                #     print node
                text = mc.expression(exp ,q=True, s=True)
                self.expList[exp] = text

    def __enter__(self): 
        for exp, text in self.expList.iteritems():
            mc.expression(exp ,e=True, s='')

    def __exit__(self, *args): 
        for exp, text in self.expList.iteritems():
            mc.expression(exp ,e=True, s=text)
            
class MoveToWorld(): 
    def __init__(self, objs): 
        self.objs = [pm.PyNode(node) for node in objs] 

    def save_home(self): 
        """ remember current hierarchy """
        info = OrderedDict()  # {child: (parent, 0), ...}
        for obj in self.objs: 
            parent = obj.getParent()
            if parent: 
                order = parent.getChildren(type='transform').index(obj)
                info[obj] = (parent, order)
        return info

    def __enter__(self): 
        self.homeInfo = self.save_home()
        for obj in self.objs:
            pm.parent(obj, w=True)

    def __exit__(self, *args): 
        for child, value in self.homeInfo.iteritems(): 
            if pm.objExists(child): 
                parent, order = value 
                child.setParent(parent)
                currentIndx = len(parent.getChildren(type='transform')) - 1
                pm.reorder(child, r=((currentIndx * -1) + order))
            else: 
                logger.warning('{} not exists'.format(child))

class CameraBaseSelection(): 
    def __enter__(self): 
        self.currentState = mc.selectPref(q=True, ud=True)

        if not self.currentState: 
            mc.selectPref(ud=True)

    def __exit__(self, *args): 
        mc.selectPref(ud=self.currentState)

class IsolateNothing():
    def __init__(self):
        self.panels = []
        self.selections = []

    def __enter__(self):
        if mc.about(batch=True):
            return
        self.selections = mc.ls(sl=True)
        mc.select(cl=True)
        for panel in mc.lsUI(editors=True):
            if panel.find('modelPanel') != -1:
                try:
                    mc.isolateSelect(panel, state=True)
                except:
                    pass
                self.panels.append(panel)

    def __exit__(self, *args):
        if mc.about(batch=True):
            return
        for panel in self.panels:
            try:
                mc.isolateSelect(panel, state=False)
            except:
                pass
        mc.select(self.selections)
        
def toggle_group(group, state, f=False): 
    # state = True - Create / False - Delete if empty 
    if state: 
        if not pm.objExists(group): 
            group = pm.group(em=True, n=group)
            return group
    else: 
        if pm.objExists(group): 
            if not pm.listRelatives(group, c=True): 
                return pm.delete(group)
            else: 
                if f: 
                    logger.warning('%s not empty. Force delete' % group)
                    return pm.delete(group)
                logger.warning('Group %s not empty. Cannot be removed' % group)



def merge_lists(l):
    s = map(set, l)
    i, n = 0, len(s)
    while i < n-1:
        for j in xrange(i+1, n):
            if s[i].intersection(s[j]):
                s[i].update(s[j])
                del s[j]
                n-=1
                break
        else:
            i+=1
    return [sorted(i) for i in s]

def getDependenciesList(include_namespaces=[]):
    constraints = [node for node in mc.ls(type='constraint', l=True) \
                   if not mc.referenceQuery(node, inr=True)]

    # just return nothing if there's no non-referenced constraint node
    if not constraints:
        return

    # get namespace used for reference objects
    ref_nss = set()
    for rfn in mc.ls(type='reference'):
        if rfn in ['sharedReferenceNode', '_UNKNOWN_REF_NODE_'] or rfn.endswith('sharedReferenceNode') or rfn.startswith('sharedReferenceNode'):
            continue
        try:
            ns = mc.referenceQuery(rfn, ns=True)
        except:
            continue
        if ns:
            ref_nss.add(ns[1:])

    ref_nss = ref_nss.union(set(include_namespaces))
    ref_nss = list(ref_nss)

    objectSets = []
    # looping over each constaint node
    nsPairs = []
    for eachCons in constraints:
        # list both past and future history on a constraint node
        histories = mc.listHistory(eachCons) + mc.listHistory(eachCons, f=True)
        # print histories
        # filter out only referenced node
        # refHistories = [n for n in histories if mc.referenceQuery(n, inr=True)]

        # get only the namespace out of histories
        namespaces = [n.rpartition(':')[0] for n in histories]

        # filter out empty string and namespace not used by references
        namespaces = [n for n in namespaces if n and n in ref_nss]
        if namespaces:
            nsPairs.append(namespaces)
    # print nsPairs
    return merge_lists(nsPairs)
    
def pyinterpreter(): 
    version = mc.about(v=True) or '2017'
    path = 'C:/Program Files/Autodesk/Maya%s/bin/mayapy.exe' % version
    return path


class ParentToWorld(): 
    '''
    Temporary parent objects to world
    '''
    def __init__(self, objects):
        self.objects = self.getObjects(objects)
        self.parents = {}

    def getObjects(self, objects):
        pyObjects = set()
        for obj in objects:
            lsObjs = pm.ls(obj)
            if len(lsObjs) > 1:
                obj = lsObjs[0]
            node = pm.PyNode(obj)
            if node.type() in ('transform', 'joint'):
                pyObjects.add(node)

        pyObjects = list(pyObjects)
        return pyObjects

    def __enter__(self): 
        for obj in self.objects:
            parent = obj.getParent()
            currIndex = None
            if parent:
                currIndex = parent.getChildren().index(obj) + 1
                self.parents[obj] = (parent, currIndex)

                pm.parent(obj, w=True)

    def __exit__(self, *args): 
        for child, data in self.parents.iteritems():
            parent = data[0]
            pm.parent(child, parent)

        for child, data in self.parents.iteritems():
            parent = data[0]
            index = data[1]
            currIndex = parent.getChildren().index(child) + 1
            pm.reorder(child, r=((currIndex - index) * -1))


def check_frame_rate(fps, project='') : 
    # fps = str(self.ui.fps_label.text())
    time = {'15': 'game', 
            '24': 'film',
            '25': 'pal',
            '30': 'ntsc',
            '48': 'show',
            '50': 'palf',
            '60': 'ntscf' }

    mapFps = {'game': '15', 
            'film': '24',
            'pal': '25',
            'ntsc': '30',
            'show': '48',
            'palf': '50',
            'ntscf': '60'}

    # game: 15 fps
    # film: 24 fps
    # pal: 25 fps
    # ntsc: 30 fps
    # show: 48 fps
    # palf: 50 fps
    # ntscf: 60 fps

    if not fps == 'None' : 
        currentFps = mc.currentUnit(q = True, time = True)
        currentFile = mc.file(q = True, location = True).split('/')[-1]
        if not currentFps == time[fps] : 
            result = mc.confirmDialog(  title='Frame Rate not Matched!', 
                                                    message='Frame Rate not matched! \nCurrent file \"%s\" is %s fps. \n%s project is %s fps.' % (currentFile, mapFps[currentFps], project, fps), 
                                                    button=['Change Now', 'Skip'], 
                                                    defaultButton='Change Now' 
                                                    )

            if result == 'Change Now' : 
                mc.currentUnit(time = time[fps])
                mm.eval('print "set frame rate @ %s\\n";' % fps)

            if result == 'Skip' : 
                mm.eval('warning "You choose ignore wrong frame rate setting!! Please change as soon as possible.\\n";')

        else : 
            mm.eval('print "frame rate matched @ %s\\n";' % fps)
    else : 
        mm.eval('warning "Project frame rate not found\\n";')

def lockPivots(obj):
    node = pm.PyNode(obj)
    node.rotatePivot.lock()
    node.rotatePivotTranslate.lock()
    node.scalePivot.lock()
    node.scalePivotTranslate.lock()
    node.transMinusRotatePivot.lock()

def get_dependency_anim_nodes(objects):
    '''
    Get nodes assiociated with referenced anim asset. 
    Use in conjunction with file -exportSelected -ch 0 
    to make sure each reference asset get exported
    '''

    deformer_types = mc.listNodeTypes('deformer')
    constraint_types = mc.nodeType('constraint', derived=True, isTypeName=True)
    anim_helper_types = ['pairBlend', 'groupParts', 'groupId']
    allow_types = anim_helper_types + deformer_types + constraint_types

    # get all references that are involved under selection
    refs = set()
    for obj in objects:
        mdag = om.MDagPath()
        sel = om.MSelectionList()
        sel.add(obj)
        sel.getDagPath(0, mdag)

        dagIt = om.MItDag(om.MItDag.kBreadthFirst)
        dagIt.reset(mdag)

        while not dagIt.isDone():
            objFn = om.MFnDagNode(dagIt.currentItem())
            isRef = objFn.isFromReferencedFile()
            if isRef:
                path = dagIt.partialPathName()
                ref = mc.referenceQuery(path, rfn=True)
                refs.add(ref)
                dagIt.prune()
            dagIt.next()

    anim_helper_nodes = set()
    # loop thru whole scene nodes of concerned type 
    for node in mc.ls(type=anim_helper_types):
        if node in anim_helper_nodes or mc.referenceQuery(node, inr=True):
            continue

        # list history and future of node
        connections = mc.listHistory(node) + mc.listHistory(node, f=True)

        # loop thru connection nodes, filter only nodes which has connection to references
        for con in connections:
            try:
                ref = mc.referenceQuery(con, rfn=True)
            except RuntimeError:
                continue
            if ref in refs:
                con_filtered = set([c for c in connections if mc.nodeType(c) in allow_types])
                # print con_filtered
                anim_helper_nodes = anim_helper_nodes.union(con_filtered)
                break

    return list(anim_helper_nodes)


def is_bb_intersect(obj1, obj2): 
    a = pm.PyNode(obj1)
    bb_a = a.getBoundingBox(space='world')

    b = pm.PyNode(obj2)
    bb_b = b.getBoundingBox(space='world')

    # check if any part of the bb has overlapped
    is_intersects = bb_a.intersects(bb_b)

    # check if the center of a bb is inside another bb
    is_inside = bb_a.contains(bb_b.center())

    return is_intersects


def get_assigned_sg_info(group): 
    """ separate mesh by face assignment """
    # find all meshes under group
    skipped_nodes = ['initialShadingGroup']
    material_info = dict()
    nodes = mc.listRelatives(group, ad=True)
    meshes = [a for a in nodes if mc.nodeType(a) == 'mesh']

    for mesh in meshes: 
        shading_engines = mc.listConnections(mesh, type='shadingEngine')
        se_dict = dict()

        for se in shading_engines: 
            if not se in se_dict.keys(): 
                if not se in skipped_nodes: 
                    all_members = mc.sets(se, q=True)
                    members = [a for a in all_members if mc.nodeType(a) == mc.nodeType(mesh)]
                
                se_dict[se] = members
        material_info[mesh] = se_dict

    return material_info


def get_assigned_material_info(group): 
    """ separate mesh by face assignment """
    # find all meshes under group
    skipped_nodes = ['initialShadingGroup']
    material_info = dict()
    nodes = pm.listRelatives(group, ad=True)
    meshes = [a for a in nodes if a.nodeType() == 'mesh']

    for mesh in meshes: 
        shading_engines = mesh.listConnections(type='shadingEngine')
        mtrs = list()
        for se in shading_engines: 
            if not se in skipped_nodes: 
                materials = mc.listConnections('{}.surfaceShader'.format(se.name()))
                if materials: 
                    mtrs += materials
        material_info[mesh.name()] = mtrs

    return material_info


def separate_geo_by_material2(group): 
    material_info = get_assigned_sg_info(group)

    for mesh, data in material_info.items(): 
        num = 1
        mesh = pm.PyNode(mesh)
        
        for se, face_members in data.items(): 
            # duplicate new face 
            new_mesh = pm.duplicate(mesh)[0]
            new_mesh.rename('{}_{}'.format(mesh.getParent(), num))

            # find all faces
            num_faces = pm.polyEvaluate(new_mesh, face=True)
            all_face_str = '{}.f[0:{}]'.format(new_mesh.name(), num_faces-1)
            all_faces = pm.ls(all_face_str, flatten=True)
            
            # face to keep 
            new_face_members = [a.name().replace(mesh.name(), new_mesh.name()) for a in face_members]
            keep_faces = pm.ls(new_face_members, flatten=True)

            # face to delete
            delete_faces = [a for a in all_faces if not a in keep_faces]
            pm.delete(delete_faces)
            num += 1 


def separate_geo_by_material(group): 
    def outliner_order(ply):
        ln = mc.ls(ply, l=True)[0]
        parent = mc.listRelatives(ply, p=True, f=True)
        if parent:
            childs = mc.listRelatives(parent, c=True, f=True)
        if not parent:
            childs = mc.ls(assemblies=True, l=True)
        return childs.index(ln) or 0

    nodes = mc.listRelatives(group, ad=True)
    meshes = [a for a in nodes if mc.nodeType(a) == 'mesh']

    for poly in meshes: 
        poly_transform = mc.listRelatives(poly, p=True)[0]
        material_info = get_assigned_sg_info(poly_transform)

        for mesh, data in material_info.items(): 
            # get transform  name
            transform_mesh = mc.listRelatives(mesh, p=True)[0]
            node = pm.PyNode(transform_mesh)
            parent = mc.listRelatives(transform_mesh, p=True)[0]
            transform_key = '{}.'.format(transform_mesh)
            mesh_key = '{}.'.format(mesh)
            num = 1
            newmesh_list = list()

            for se, face_members in data.items():
                # filter member only mesh 
                # print transform_mesh, mesh
                # mc.select(face_members)
                members = [a for a in face_members if transform_key in a or mesh_key in a]

                if members: 
                    # duplicate from original
                    dupmesh = mc.duplicate(mesh)[-1]
                    # retarget data to duplicate mesh
                    dupmesh_members = [a.replace(transform_mesh, dupmesh) for a in members]
                    # extract 
                    mc.polyChipOff(dupmesh_members, ch=0, kft=1, dup=1, off=0)
                    
                    # separate 
                    result = mc.polySeparate(dupmesh, ch=0)
                    
                    # delete first mesh 
                    mc.delete(result[0])

                    if len(result) > 2: 
                        # need to combine
                        newmesh = mc.polyUnite(result[1:], ch=0, mergeUVSets=1, centerPivot=1)
                    else: 
                        newmesh = result[1]

                    newmesh = mc.rename(newmesh, '{}_{}'.format(transform_mesh, num))
                    newmesh_list.append(newmesh)

                    num+=1

            if newmesh_list:
                group = '|{}'.format(transform_mesh)
                if not mc.objExists(group): 
                    group = mc.group(em=True, n=transform_mesh)

                for transform in newmesh_list: 

                    mc.parent(transform, group)
                order = outliner_order(node.name())
                pm.delete(node)
                group = mc.parent(group, parent)
                mc.reorder(group, f=True)
                mc.reorder(group, r=order)
