import os
import sys
import shutil
import re
from collections import defaultdict
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import maya.cmds as mc
import maya.mel as mm
import pymel.core as pm

plugin = 'pgYetiMaya.mll'

try: 
    if not mc.pluginInfo(plugin, q=True, l=True):
        mc.loadPlugin(plugin, qt=True)
    mm.eval('source "%s/plugins/Maya2017/yeti_2.2.1/yeti/scripts/AEpgYetiMayaTemplate.mel";' % (os.environ.get('RFSCRIPT')).replace('\\', '/'))
except Exception as e: 
    logger.error(e)

# def export_cache(node, path, startframe, endframe):
#     # set path
#     mc.setAttr('%s.outputCacheFileName' % node, path, type='string')
#     mc.setAttr('%s.outputCacheFrameRangeStart' % node, startframe)
#     mc.setAttr('%s.outputCacheFrameRangeEnd' % node, endframe)
#     mc.setAttr('%s.outputCacheNumberOfSamples' % node, 1)
#     mm.eval('AEpgYetiWriteCacheCMD %s' % node)

# def import_cache(nodeName, path):
#     node = mm.eval('pgYetiCreate();')
#     mc.setAttr('%s.fileMode' % node, 1)
#     mc.setAttr('%s.cacheFileName' % node, path, type='string')
#     mc.setAttr('%s.ignoreCachePreview' % node, 1)
#     transform = mc.listRelatives(node, p=True)
#     if transform:
#         newName = mc.rename(transform, nodeName)
#     return newName


def set_node_namespace(namespace=':'):
    yetiNodes = mc.ls(type='pgYetiMaya')

    for yetiNode in yetiNodes:
        set_namespace(namespace, yetiNode)

def set_namespace(namespace, yetiNode):
    nodes = mc.pgYetiGraph(yetiNode, listNodes=True)
    root = get_root(yetiNode)
    for node in nodes:
        newName = yeti_node_namespace(node, namespace)
        if not node == newName:
            setRoot = True if root == node else False
            mc.pgYetiGraph(yetiNode, node=node, rename=newName)
            mc.pgYetiGraph(node=yetiNode, setRootNode=newName) if setRoot else None

def add_attribute_info(yetiNodes): 
    """ add attribute node to end of graph """ 
    if isinstance(yetiNodes, str): 
        yetiNodes = [yetiNodes]
    
    for yetiNode in yetiNodes: 
        mc.select(yetiNode)
        rootNode = mc.pgYetiGraph(yetiNode, getRootNode=True)
        if rootNode: 
            nodeType = mc.pgYetiGraph(yetiNode, node=rootNode, nodeType=True)

            if not nodeType in ['attribute']: 
                attributeNode = mc.pgYetiGraph(yetiNode, create=True, type='attribute')
                print 'create Node', attributeNode
                mc.pgYetiGraph(yetiNode, node=attributeNode, param='addPref', setParamValueBoolean=True)
                mc.pgYetiGraph(yetiNode, node=rootNode, connect=(attributeNode, 0))
                mc.pgYetiGraph(node=yetiNode, setRootNode=attributeNode)
                # mc.pgYetiGraph(yetiNode, node='attribute0', listParams=True)
            else: 
                logger.warning('Root node "%s" already an attribute' % yetiNode)
        else: 
            logger.warning('YetiNode "%s" Graph does not have root node' % yetiNode)


def set_unique_name(): 
    yetiNodes = mc.ls(type='pgYetiMaya')
    duplicates = defaultdict(set)  # nodename: [yetiNode1, yetiNode2]
    for yn in yetiNodes:
        for name in mc.pgYetiGraph(yn, listNodes=True):
            duplicates[name].add(yn)

    existing_names = duplicates.keys()
    result = defaultdict(list)
    for name, yetiNodes in duplicates.iteritems():
        if len(yetiNodes) > 1:  # only rename if there's more than 1 existing name
            # need a rename, get the end digit
            match = re.match('(.*?)([0-9]+)$', name)
            digit = 1
            if match:
                # digit = int(match.group(2))
                elem = match.group(1)
            else:
                elem = name

            for yn in yetiNodes:
                exit = False
                while not exit:
                    newName = '%s%s' %(elem, digit)
                    if newName in existing_names:
                        digit += 1
                    else:
                        existing_names.append(newName)
                        exit = True

                result[yn].append((name, newName))

    # need to rename 1 YetiNode at a time, cannot jump back and forth
    for yn, pairs in result.iteritems():
        for name, newName in pairs:
            logger.debug('[%s]%s ---> %s' %(yn, name, newName))
            previewNode = mc.pgYetiGraph(yn, getRootNode=True)
            mc.pgYetiGraph(yn, node=name, rename=newName)

            # set the preview back on
            if name == previewNode:
                mc.pgYetiGraph(yn, setRootNode=newName)

    logger.info('Yeti node names are now unique.')
        

def is_node_root(yetiNode, node): 
    mc.select(yetiNode)
    root = mc.pgYetiGraph(getRootNode=True)
    return True if root == node else False 

def get_root(yetiNode): 
    mc.select(yetiNode)
    return mc.pgYetiGraph(getRootNode=True)


def list_yeti_nodes():
    allNodes = []
    yetiNodes = mc.ls(type='pgYetiMaya')
    for yetiNode in yetiNodes:
        nodes = mc.pgYetiGraph(yetiNode, listNodes=True)
        allNodes += nodes
    return allNodes

def yeti_node_namespace(node, namespace):
    allNodes = list_yeti_nodes()
    allNodes = [a for a in allNodes if not a == node]
    if ':' in node: 
        # newName = node.split(':')[-1]
        newName = node
    else:
        newName = '%s:%s' % (namespace, node.split(':')[-1]) if ':' in node else '%s:%s' % (namespace, node)

    if not newName in allNodes:
        return newName
    else:
        if newName[-1].isdigit():
            num = int(newName[-1]) + 1
            newName = '%s%s' % (newName[:-1], num)
        else:
            newName = '%s1' % newName
        return yeti_node_namespace(newName, namespace)


def get_display_dict(yetiNode): 
    """ get display value """ 
    attrs = ['subdDisplay', 'featherDisplayStyle', 'viewportDensity', 'viewportLength', 'viewportWidth', 'color']
    vectorAttrs = ['color']
    info = dict()

    if mc.objectType(yetiNode, isType='pgYetiMaya'): 
        for attr in attrs: 
            nodeAttr = '%s.%s' % (yetiNode, attr)
            value = mc.getAttr(nodeAttr)
            info[attr] = value 
    return info 

def get_display_info(): 
    info = dict()
    yetiNodes = mc.ls(type='pgYetiMaya')

    for node in yetiNodes: 
        transform = mc.listRelatives(node, p=True)
        if transform: 
            data = get_display_dict(node)
            info[transform[0]] = data 
    return info 

def set_display(data, yetiNode, namespace=''): 
    activeNode = yetiNode if not namespace else '%s:%s' % (namespace, yetiNode)
    if yetiNode in data.keys(): 
        nodeData = data[yetiNode]

        for attr in nodeData: 
            value = nodeData[attr]
            nodeAttr = '%s.%s' % (activeNode, attr)
            print 
            if type(value) == type(list()): 
                color = value[0]
                mc.setAttr(nodeAttr, color[0], color[1], color[2])
            else: 
                mc.setAttr(nodeAttr, value)

    # defaultValue = {}
    # yetiNodes = mc.ls(type='pgYetiMaya')


def find_textures(yetiNode, data='list'):
    textureDict = dict()
    textures = []
    textureNodes = mc.pgYetiGraph(yetiNode, listNodes=True, type='texture')
    for node in textureNodes:
        path = mc.pgYetiGraph(yetiNode, node=node, param='file_name', getParamValue=True)
        textureDict[node] = {'yetiNode': yetiNode, 'node': node, 'path': path}
        textures.append(path)

    return textures if data == 'list' else textureDict

def find_all_textures():
    yetiNodes = mc.ls(type='pgYetiMaya')
    allDict = dict()

    for yetiNode in yetiNodes:
        textureDict = find_textures(yetiNode, data='dict')

        for texturePath, data in textureDict.iteritems():
            allDict[texturePath] = data

    return allDict

def set_node_texture_path(yetiNode, node, path):
    mc.pgYetiGraph(yetiNode, node=node, param='file_name', setParamValueString=path)
    logger.info('repath %s - %s to %s' % (yetiNode, node, path))


def move_yeti_texture_path(dstTexturePath, override=True, relink=True):
    import maya_utils
    """ relink texture to the asset including UDIM """
    if not os.path.exists(dstTexturePath):
        os.makedirs(dstTexturePath)

    allDict = find_all_textures()
    replaceDict = dict()

    # copy
    for path, data in allDict.iteritems():
        yetiNode = data['yetiNode']
        node = data['node']
        path = data['path']

        files = maya_utils.find_udim_files(path)
        namePattern = os.path.basename(path)
        newPattern = '%s/%s' % (dstTexturePath, namePattern)
        logger.debug('Copying %s udim files ...' % len(files))

        for src in files:
            basename = os.path.basename(src)
            dst = '%s/%s' % (dstTexturePath, basename)
            copy = False

            if not src == dst:
                if not os.path.exists(dst):
                    copy = True
                else:
                    if override:
                        copy = True
                if copy:
                    shutil.copy2(src, dst)
                    logger.debug('Copied %s -> %s' % (src, dst))
            else:
                logger.debug('skip copy. src and path are the same')

        if relink:
            set_node_texture_path(yetiNode, node, newPattern)
            replaceDict[path] = newPattern

    return replaceDict

def cache(node, path, start, end, samples=10):
    if type(node) == type(str()): 
        if ':' in node:
            namespace = node.split(':')[0]
            set_namespace(namespace, node)
    if type(node) == type(list()): 
        for each in node: 
            namespace = each.split(':')[0]
            set_namespace(namespace, each)
    logger.info('This is node to be exported %s' % node)
    mc.select(node)
    mc.pgYetiCommand(writeCache=path, range=[start, end], samples=samples)


def import_cache(node, path, namespace='', override=False):
    name = '%s:%s' % (namespace, node)
    yetiNode = check_yeti_node(name)
    mc.setAttr('%s.cacheFileName' % yetiNode, path, type='string')
    mc.setAttr('%s.fileMode' % yetiNode, 1)

    if namespace:
        if not mc.namespace(exists=namespace):
            mc.namespace(add=namespace)

    return name

        # yetiShape = mc.rename(yetiNode, name)
        # transform = mc.listRelatives(yetiShape, p=True)
        # yetiShape = mc.rename(transform[0], '%s:%s' % (namespace, transform[0]))

def remove_cache(node, namespace):
    yetiNode = '%s:%s' % (namespace, node)
    print 'yetiNode', yetiNode
    mc.delete(yetiNode) if mc.objExists(yetiNode) else None
    return yetiNode


def check_yeti_node(node):
    if mc.objExists(node):
        nodeType = mc.objectType(node)
        if nodeType == 'pgYetiMaya':
            return node

        elif nodeType == 'transform':
            shape = mc.listRelatives(node, s=True)
            if shape:
                return check_yeti_node(shape[0])
            else:
                nodeName, nodeShape = create_yeti_node(node)
                return nodeShape
        else:
            nodeName, nodeShape = create_yeti_node(node)
            return nodeShape
    else:
        nodeName, nodeShape = create_yeti_node(node)
        return nodeShape


def create_yeti_node(name):
    yetiNode = mm.eval('pgYetiCreate();')
    transform = mc.listRelatives(yetiNode, p=True)
    nodeName = mc.rename(transform[0], name)
    shape = mc.listRelatives(nodeName, s=True)
    nodeShape = mc.rename(shape[0], '%sShape' % nodeName)
    return nodeName, nodeShape

def show_guide_attrs(objs=[]):
    if not objs:
        objs = pm.selected(type='transform')
        if not objs:
            return

    attrs = ('weight', 'lengthWeight', 'innerRadius', 'outerRadius', 'density', 
            'baseAttraction', 'tipAttraction', 'attractionBias', 'randomAttraction', 
            'twist', 'surfaceDirectionLimit', 'surfaceDirectionLimitFalloff', 
            'userFloat', 'userVectorX', 'userVectorY', 'userVectorZ')
    for obj in objs:
        for attr in attrs:
            if obj.hasAttr(attr):
                obj.attr(attr).showInChannelBox(True)

def export_corrective_groom(directoryPath, correctiveNodes=[]):
    if not correctiveNodes:
        correctiveNodes = [n for n in pm.selected(type='transform') if n.getShape() and n.getShape().nodeType()=='pgYetiGroom']
        if not correctiveNodes:
            logger.error('Please select corrective groom nodes.')
            return
    else:
        pyNodes = []
        for node in correctiveNodes:
            if isinstance(node, (str, unicode)) and len(mc.ls(node)) == 1:
                node = pm.PyNode(node)    
                pyNodes.append(node)
            else:
                logger.error('Skipping: %s, node does not exist or not unique.')

        correctiveNodes = pyNodes

    from rf_maya.rftool.utils import abc_utils
    reload(abc_utils)
    import maya_utils

    directoryPath = directoryPath.replace('\\', '/')
    attrName = 'crGroomCons'
    # collision geometries
    conTxts = defaultdict(list)
    tmpConnections = []
    for node in correctiveNodes:
        nodeShp = node.getShape()
        # node connection to yeti node
        nodeOuts = nodeShp.outputData.outputs(c=True, p=True)
        nodeConTxts = []
        for src, des in nodeOuts:
            nodeSn = des.node().shortName().split(':')[-1]
            attrSn = des.shortName()
            nodeTxt = '%s %s.%s' %(src.shortName(), nodeSn, attrSn)
            nodeConTxts.append(nodeTxt)
            tmpConnections.append((src, des))
        
        # add corrective groom node attr
        if not nodeShp.hasAttr(attrName):
            pm.addAttr(nodeShp, ln=attrName, dt='string')
        txt = ','.join(nodeConTxts)
        nodeShp.attr(attrName).set(txt)

        # collision geo connections
        connections = nodeShp.collisionGeometry.inputs(c=True, p=True) + nodeShp.inputGeometry.inputs(c=True, p=True)
        for des, src in connections:
            nodeSn = des.node().shortName().split(':')[-1]
            attrSn = des.shortName()
            txt = '%s %s.%s' %(src.shortName(), nodeSn, attrSn)
            conTxts[src.node()].append(txt)
            tmpConnections.append((src, des))

    # add attribute to collision geos
    objDict = {}
    for mesh, txts in conTxts.iteritems():
        geoTr = mesh.getParent()
        # add attribute
        if not mesh.hasAttr(attrName):
            pm.addAttr(mesh, ln=attrName, dt='string')
        txtStr = ','.join(txts)
        mesh.attr(attrName).set(txtStr)
        
        # collect obj:path
        path = '%s/%s.abc' %(directoryPath, geoTr.nodeName().split(':')[-1])
        objDict[geoTr.shortName()] = path

    # export collision geo abc
    if objDict:
        currentVp = maya_utils.getCurrentViewportRenderer()
        start =  pm.playbackOptions(q=True, min=True)
        end =  pm.playbackOptions(q=True, max=True)
        with maya_utils.FreezeViewport() as fw:
            abc_utils.export_multiple_abcs(objDict, start, end, extraAttrs=[attrName])

    # ---- export .ma
    # disconnectAttr
    for src, des in tmpConnections:
        pm.disconnectAttr(src, des)

    # export .ma
    for node in correctiveNodes:
        pm.select(node, r=True)
        path = '%s/%s.ma' %(directoryPath, node.nodeName())
        pm.exportSelected(path, f=True)

    # re-connect
    for src, des in tmpConnections:
        pm.connectAttr(src, des, f=True)

def getConnectionFromNodes(nodes, namespace):
    attrName = 'crGroomCons'
    results = []
    for node in nodes:
        if not node.hasAttr(attrName, checkShape=False):
            continue
        conStr = node.attr(attrName).get()
        connections = conStr.split(',')
        # print node, connections
        for c in connections:
            splits = c.split(' ')
            srcAttr = splits[0]
            desStr = splits[1]
            desSplits = desStr.split('.')
            desNodeName = '%s%s' %(namespace, desSplits[0])
            desAttrName = '.'.join(desSplits[1:])
            # print desAttrName, desNodeName, srcAttr, desStr

            if not node.hasAttr(srcAttr):
                logger.error('%s has not attr %s' %(node, srcAttr))
                continue
            src = node.attr(srcAttr)

            if not pm.objExists(desNodeName):
                logger.error('%s does not exists' %(desNodeName))
                continue
            desNode = pm.PyNode(desNodeName)
            if not desNode.hasAttr(desAttrName):
                logger.error('%s has not attr %s' %(node, c))
                continue
            des = desNode.attr(desAttrName)
            results.append((src, des))
    return results

def import_corrective_groom(directoryPath, namespace=''):
    directoryPath = directoryPath.replace('\\', '/')
    files = os.listdir(directoryPath)
    if namespace:
        namespace += ':'

    abcPaths = []
    all_connections = []
    for f in files:
        fp = '%s/%s' %(directoryPath, f)
        p, ext = os.path.splitext(fp)
        if not os.path.isfile(fp) or ext not in ('.abc', '.ma'):
            continue

        # import the ma file first
        if ext == '.ma':
            newNodes = pm.importFile(fp, returnNewNodes=True, namespace=namespace[0:-1])
            connections = getConnectionFromNodes(nodes=newNodes, namespace=namespace)  
            all_connections.extend(connections)
        elif ext == '.abc':
            abcPaths.append(fp)

    for path in abcPaths:
        newNodes = pm.importFile(path, returnNewNodes=True, namespace=namespace[0:-1])
        connections = getConnectionFromNodes(nodes=newNodes, namespace=namespace)  
        all_connections.extend(connections)

    for src, des in all_connections:
        pm.connectAttr(src, des, f=True)


# pgYetiCommand -writeCache "/base/vfx/tmp/myCache.%04d.fur" -range 1 100 -samples 5
# P:/Two_Heroes/scene/film001/q0530/s0010/hairDyn/maya/work/th_film001_q0530_s0010_hairDyn_v001_Ref.ma
# documents
# http://documentation.peregrinelabs.com/yeti/scriptingref.html



# from rftool.utils import yeti_lib
# reload(yeti_lib)
# yeti_lib.add_attribute_info('yeti:EyeBrow_YetiNode')
