import sys
import os
import maya.cmds as mc
import maya.mel as mm

from rf_utils import cask

plugins = ['AbcExport.mll', 'AbcImport.mll']

for plugin in plugins:
    if not mc.pluginInfo(plugin, q=True, l=True):
        mc.loadPlugin(plugin, qt=True)

""" Alembic Sections """
def exportABC(obj, path, list_range = []) :
    # if dir not exists, create one
    if not os.path.exists(os.path.dirname(path)) :
        os.makedirs(os.path.dirname(path))

    start = mc.playbackOptions(q = True, min = True)
    end = mc.playbackOptions(q = True, max = True)

    if list_range:
        start = list_range[0]
        end = list_range[1]
        
    options = []
    options.append('-frameRange %s %s' % (start, end))
    options.append('-attr project -attr assetID -attr assetType -attr assetSubType -attr assetName -attr assetShader ')
    options.append('-attr id -attr model -attr uv -attr rig -attr surface -attr data -attr ref -attr lod -attr hidden ')
    # this will send bad uv to look dev. do not enable this.
    options.append('-uvWrite')
    options.append('-writeUVSets')
    #options.append('-writeFaceSets')
    options.append('-eulerFilter')
    options.append('-writeVisibility')
    options.append('-worldSpace')
    options.append('-dataFormat ogawa')
    options.append('-root %s' % obj)
    options.append('-file %s' % path)
    optionCmd = (' ').join(options)

    cmd = 'AbcExport -j "%s";' % optionCmd
    result = mm.eval(cmd)

    return result

def importABC(obj, path, mode = 'new') :
    if mode == 'new' :
        cmd = 'AbcImport -mode import "%s";' % path

    if mode == 'add' :
        # cmd = 'AbcImport -mode import -connect "%s" -createIfNotFound -removeIfNoUpdate "%s";' % (obj, path)
        # not work in some cases, so use below
        cmd = 'AbcImport -mode import -connect "%s" "%s";' % (obj, path)

    if mode == 'add_remove' :
        cmd = 'AbcImport -mode import -connect "%s" -createIfNotFound -removeIfNoUpdate "%s";' % (obj, path)

    mm.eval(cmd)

    return path

def export_abc(obj, path, start, end, extraAttrs=[]) :
    # if dir not exists, create one
    if not os.path.exists(os.path.dirname(path)) :
        os.makedirs(os.path.dirname(path))

    # add support for obj argument being list/tuple 
    if isinstance(obj, (list, tuple)):
        obj = ' -root '.join(obj)

    # start = mc.playbackOptions(q = True, min = True)
    # end = mc.playbackOptions(q = True, max = True)
    options = []
    options.append('-frameRange %s %s' % (start, end))
    options.append('-attr project -attr assetID -attr assetType -attr assetSubType -attr assetName -attr assetShader -attr adPath -attr refPath -attr look -attr cacheOffset')
    options.append('-attr id -attr model -attr uv -attr rig -attr surface -attr data -attr ref -attr lod -attr hidden -attr description -attr cacheOffset')
    if extraAttrs:
        for attrName in extraAttrs:
            options.append('-attr %s' %(attrName))

    options.append('-attr shdConnect -userAttrPrefix sc__ ')
    options.append('-attr separateShader')
    # this will send bad uv to look dev. do not enable this.
    options.append('-uvWrite')
    options.append('-writeUVSets')
    #options.append('-writeFaceSets')
    options.append('-eulerFilter')
    options.append('-writeVisibility')
    options.append('-worldSpace')
    options.append('-dataFormat ogawa')
    options.append('-root %s' % obj)
    options.append('-file %s' % path)
    optionCmd = (' ').join(options)

    cmd = 'AbcExport -j "%s";' % optionCmd
    result = mm.eval(cmd)

    return result

def export_multiple_abcs(objDict, start, end, extraAttrs=[]) :
    jobStrs = []
    for obj, path in objDict.iteritems():
        # if dir not exists, create one
        if not os.path.exists(os.path.dirname(path)) :
            os.makedirs(os.path.dirname(path))

        cmd = '"-frameRange %s %s' % (start, end)
        cmd += ' -attr project -attr assetID -attr assetType -attr assetSubType -attr assetName -attr assetShader -attr adPath -attr refPath -attr look'
        cmd += ' -attr id -attr model -attr uv -attr rig -attr surface -attr data -attr ref -attr lod -attr hidden -attr cacheOffset'
        cmd += ' -attr shdConnect -userAttrPrefix sc__ '
        cmd += ' -uvWrite'
        cmd += ' -writeUVSets'
        cmd += ' -writeVisibility'
        cmd += ' -worldSpace'
        cmd += ' -eulerFilter'
        cmd += ' -dataFormat ogawa'
        if extraAttrs:
            for attrName in extraAttrs:
                cmd += ' -attr %s' %(attrName)
        cmd += ' -root %s'% obj
        cmd += ' -file %s"'% path
        jobStrs.append(cmd)

    print jobStrs

    cmd = 'AbcExport -j %s;' %' -j '.join(jobStrs)
    print 'cmd', cmd
    mm.eval(cmd)

def reference_abc(path, namespace):
    return mc.file(path, r=True, type='Alembic', ns=namespace, f=True)

def get_matrix_from_abc(nodeName, abcPath, index=[]):
    arc = cask.Archive(str(abcPath))   
    nodes = [c for c in cask.find(arc.top, name=nodeName) if c.type()=='Xform']
    node = None
    if nodes:
        node = nodes[0]
    else:
        raise Exception, 'Cannot find node of this name in ABC: %s' %nodeName

    if not index:
        numSamples = node.schema.getNumSamples()
        index = xrange(numSamples)
    matrices = []
    for i in index:
        global_mat = node.global_matrix(i)
        matrix = list([list(row) for row in global_mat])
        matrices.append(matrix)

    return matrices

def get_positions_from_abc(nodeName, abcPath, index=[]):
    matrices = get_matrix_from_abc(nodeName, abcPath, index)
    positions = []
    for mat in matrices:
        lastRow = mat[3]
        translate = [lastRow[0], lastRow[1], lastRow[2]]
        positions.append(translate)
    return positions