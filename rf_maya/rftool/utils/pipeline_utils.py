import sys, os, subprocess
import logging
from collections import OrderedDict
from datetime import datetime
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

try:
    import maya.cmds as mc
    import pymel.core as pm
    import maya.mel as mm
    isMaya=True
except ImportError:
    isMaya = False

from startup import config
from startup import template
from rftool.utils import path_info
from rf_utils import register_entity
from rftool.utils import file_utils
from rf_utils import project_info
from rf_utils.context import context_info
from rf_utils import project_info


if isMaya:
    version = mc.about(v = True)
    if '2015' in version :
        MAYA_PYTHON = 'C:/Program Files/Autodesk/Maya2015/bin/mayapy.exe'

    if '2016' in version :
        MAYA_PYTHON = 'C:/Program Files/Autodesk/Maya2016/bin/mayapy.exe'
        MAYA_RENDER = 'C:/Program Files/Autodesk/Maya2016/bin/Render.exe'

    if '2017' in version :
        MAYA_PYTHON = 'C:/Program Files/Autodesk/Maya2017/bin/mayapy.exe'

    if '2018' in version :
        MAYA_PYTHON = 'C:/Program Files/Autodesk/Maya2018/bin/mayapy.exe'

    if '2020' in version :
        MAYA_PYTHON = 'C:/Program Files/Autodesk/Maya2020/bin/mayapy.exe'

def create_asset_template(root, project, assetType, assetSubType, assetName):
    scriptServer = os.environ['RFSCRIPT']
    templatePath = ''
    rootValid = False

    if root == config.RFPROJECT:
        steps = template.workAssetSteps
        templatePath = '%s/RFPROJECT' % template.templatePath
        rootValid = True

    if root == config.RFPUBL:
        steps = template.publAssetSteps
        templatePath = '%s/RFPUBL' % template.templatePath
        rootValid = True

    if root == config.RFPROD:
        steps = template.prodAssetSteps
        templatePath = '%s/RFPROD' % template.templatePath
        rootValid = True

    if rootValid:
        asset = path_info.PathInfo(project=project, entity=config.asset, entitySub1=assetType, entitySub2=assetSubType, name=assetName)
        assetPath = asset.entityPath(root)

        if not os.path.exists(assetPath):
            os.makedirs(assetPath)

        for step in steps:
            src = '%s/%s' % (templatePath, step)
            dst = '%s/%s' % (assetPath, step)

            if not os.path.exists(dst):
                result = file_utils.copyTree(src, dst)
                logger.debug('Copy %s -> %s success' % (src, dst))

        # create_standin(asset)
        # repath_ad(asset)
        repath_work(asset)

        return True

def create_scene_template(root, project, episodeName, sequenceName, shotName):
    scriptServer = os.environ['RFSCRIPT']
    templatePath = ''
    rootValid = False

    if root == config.RFPROJECT:
        steps = template.workSceneSteps
        templatePath = '%s/RFPROJECT' % template.templatePath
        rootValid = True

    if root == config.RFPUBL:
        steps = template.publSceneSteps
        templatePath = '%s/RFPUBL' % template.templatePath
        rootValid = True

    if root == config.RFPROD:
        steps = template.prodSceneSteps
        templatePath = '%s/RFPROD' % template.templatePath
        rootValid = True

    if rootValid:
        shot = path_info.PathInfo(project=project, entity=config.scene, entitySub1=episodeName, entitySub2=sequenceName, name=shotName)
        shotPath = shot.entityPath(root)

        if not os.path.exists(shotPath):
            os.makedirs(shotPath)

        for step in steps:
            src = '%s/%s' % (templatePath, step)
            dst = '%s/%s' % (shotPath, step)

            if not os.path.exists(dst):
                result = file_utils.copyTree(src, dst)
                logger.debug('Copy %s -> %s success' % (src, dst))

        return True


def create_standin(asset):
    assetPath = asset.entityPath('RFPROJECT')
    refPath = '%s/%s' % (assetPath, config.ref)
    files = file_utils.listFile(refPath)
    count = 0

    if files:
        for ref in files:
            if config.libFileReplaceKw in ref:
                newName = ref.replace(config.libFileReplaceKw, asset.name)
                src = '%s/%s' % (refPath, ref)
                dst = '%s/%s' % (refPath, newName)
                if not os.path.exists(dst):
                    os.rename(src, dst)
                    logger.debug('Renaming %s -> %s' % (src, dst))
                    count+=1
                else:
                    logger.debug('File exists. Cannot rename %s' % dst)

    logger.info('renaming %s files standin complete (%s)' % (count, refPath))


def create_lib_template(root, project, assetType, assetSubType, assetName):
    scriptServer = os.environ['RFSCRIPT']
    templateLibPath = ''
    templateLibPath = '%s/RFPROJECT/%s' % (template.templatePath, config.ref)

    asset = path_info.PathInfo(project=project, entity=config.asset, entitySub1=assetType, entitySub2=assetSubType, name=assetName)
    assetLibPath = '%s/%s' % (asset.entityPath(root), config.ref)
    libFiles = file_utils.listFile(templateLibPath)

    if not os.path.exists(assetLibPath):
        os.makedirs(assetLibPath)

    for libFile in libFiles:
        src = '%s/%s' % (templateLibPath, libFile)
        dst = '%s/%s' % (assetLibPath, libFile)

        if not os.path.exists(dst):
            result = file_utils.copy(src, dst)
            logger.debug('Copy %s -> %s success' % (src, dst))

            if config.libFileReplaceKw in libFile:
                newName = libFile.replace(config.libFileReplaceKw, asset.name)
                renSrc = dst
                renDst = '%s/%s' % (assetLibPath, newName)

                if not os.path.exists(renDst):
                    os.rename(renSrc, renDst)
                    logger.debug('Rename %s -> %s' % (renSrc, renDst))

                else:
                    os.remove(renSrc)
                    logger.debug('Remove existing template %s' % renSrc)


    create_standin(asset)
    repath_ad(asset)

    return True


def repath_ad(asset):
    assetPath = asset.entityPath('RFPROJECT')
    refPath = '%s/%s' % (assetPath, config.ref)
    files = file_utils.listFile(refPath)
    adFile = asset.libName('', 'ad', project=False)
    replaceDict = {config.assemblyMap['assetName']: '%s_ad' % asset.name, config.assemblyMap['locatorLabel']: asset.name}

    if os.path.exists('%s/%s' % (refPath, adFile)):

        if files:
            for k, v in config.representationMap.iteritems():
                replaceFile = [a for a in files if k in a]

                if replaceFile:
                    path = '%s/%s' % (refPath, replaceFile[0])
                    replaceDict.update({v: path})

            logger.debug('replaceKey %s' % str(replaceDict))
            logger.info('repath ad complete %s' % ('%s/%s' % (refPath, adFile)))
            return file_utils.search_replace_keys('%s/%s' % (refPath, adFile), replaceDict, backupFile=False)


def repath_work(asset):
    # design work
    design = '%s/%s' % (asset.entityPath(), asset.design)

    for root, dirs, files in os.walk(design):
        if root and files:
            for file in files:
                if config.libFileReplaceKw in file:
                    newName = file.replace(config.libFileReplaceKw, asset.name)
                    src = '%s/%s' % (root, file)
                    dst = '%s/%s' % (root, newName)
                    os.rename(src, dst)


def check_export(mtime, path):
    """ check if file export success by checking time stamp """
    if not os.path.exists(path):
        return False

    else:
        newtime = os.path.getmtime(path)

        if not newtime == mtime:
            return True
        else:
            return False

def check_time(path):
    if not os.path.exists(path):
        return 0
    else:
        return os.path.getmtime(path)


# def runMayaPy(runpy, *args) :
#   """ run maya py subprocess """
#   logger.info('starting subprocess, please wait ...')
#   startupinfo = subprocess.STARTUPINFO()

#   # hide console
#   # startupinfo.dwFlags |= subprocess.STARTF_USESHOWWINDOW

#   p = subprocess.Popen([MAYA_PYTHON, runpy] + list(args), stdout=subprocess.PIPE, stderr=subprocess.PIPE, startupinfo=startupinfo)
#   stdout, stderr = p.communicate()

#   logger.info(stdout)
#   logger.info(stderr)

#   logger.info('subprocess complete')


def runMayaPy(runpy, mayaVersion, console=True, *args) :
    """ run maya py subprocess """
    logger.info('starting subprocess, please wait ...')

    if '2015' in mayaVersion :
        MAYA_PYTHON = 'C:/Program Files/Autodesk/Maya2015/bin/mayapy.exe'

    if '2016' in mayaVersion :
        MAYA_PYTHON = 'C:/Program Files/Autodesk/Maya2016/bin/mayapy.exe'
        MAYA_RENDER = 'C:/Program Files/Autodesk/Maya2016/bin/Render.exe'

    if '2017' in mayaVersion :
        MAYA_PYTHON = 'C:/Program Files/Autodesk/Maya2017/bin/mayapy.exe'

    startupinfo = subprocess.STARTUPINFO()

    # hide console
    if not console:
        startupinfo.dwFlags |= subprocess.STARTF_USESHOWWINDOW

    p = subprocess.Popen([MAYA_PYTHON, runpy] + list(args), stdout=subprocess.PIPE, stderr=subprocess.PIPE, startupinfo=startupinfo)
    p.wait()
    stdout, stderr = p.communicate()

    logger.info(stdout)
    logger.info(stderr)

    logger.info('subprocess complete')

def search_replace_file(srcFile, dstFile, replaceDict) :
    """ search and replace list of strings """
    # replaceDict = {'search': 'replace'}

    # open file
    f = open(srcFile, 'r')
    data = f.read()
    f.close()

    for search, replace in replaceDict.iteritems():
        data = data.replace(search, replace)

    # write back
    f = open(dstFile, 'w')
    f.write(data)
    f.close()

    return True


def relink_texture():
    fileNodes = mc.ls(type='file')
    asset = path_info.PathInfo()
    texturePath = asset.texturePath()
    res = asset.file_res()
    assetTexturePath = '%s/%s' % (texturePath, res)

    for node in fileNodes:
        path = mc.getAttr('%s.fileTextureName' % node)

        if not assetTexturePath in path:
            # path not valid
            filename = os.path.basename(path)
            newPath = '%s/%s' % (assetTexturePath, filename)

            if not os.path.exists(newPath):
                file_utils.copy(path, newPath)

            # link
            mc.setAttr('%s.fileTextureName' % node, newPath, type='string')
            logger.info('link success %s' % newPath)

        else:
            logger.info('texture already in pipeline %s' % path)


def relink_sound():
    """ relink sound file """
    shot = path_info.PathInfo()
    audioPath = shot.audioPath()

    audioNodes = mc.ls(type='audio')
    for node in audioNodes:
        audioFile = mc.getAttr('%s.filename' % node)
        dst = '%s/%s' % (audioPath, os.path.basename(audioFile))

        if not os.path.exists(dst):
            file_utils.copy(audioFile, dst)
            logger.info('Copy sound to %s' % dst)

        mc.setAttr('%s.filename' % node, dst, type='string')
        logger.info('Link %s success' % node)


def write_log(project, message):
    sn = mc.file(q=True, sn=True)
    user = os.environ.get('RFUser')
    if project in sn:
        date = datetime.now().strftime("%Y-%m-%d")
        stamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        logFile = 'P:/%s/_log/%s/%s/%s_log.json' % (project, date, user, user)
        data = file_utils.json_loader(logFile) if os.path.exists(logFile) else dict()
        data[stamp] = message
        file_utils.json_dumper(logFile, data)


def scale_check(asset, show=True, toggle=True):
    node = 'scaleReference'
    def create_gpu(name, path):
        gpuNode = mc.createNode('gpuCache')
        mc.setAttr('%s.cacheFileName' % gpuNode, path, type='string')
        transform = mc.listRelatives(gpuNode, p=True)[0]
        return mc.rename(transform, name)

    scaleFile = asset.projectInfo.asset.global_asset('charScale')
    if os.path.exists(scaleFile):
        if toggle:
            mc.delete(node) if mc.objExists(node) else create_gpu(node, scaleFile)
        else:
            mc.delete(node) if mc.objExists(node) else None
            create_gpu(node, scaleFile) if show else None



def scene_group():
    def parent(child, grp):
        if not mc.objExists(grp):
            grp = mc.group(em=True, n=grp)

        currentParent = mc.listRelatives(child, p=True)

        if not currentParent:
            mc.parent(child, grp)
        if currentParent:
            if not grp in currentParent:
                mc.parent(child, grp)

    scene = context_info.ContextPathInfo()
    proj = project_info.ProjectInfo(scene.project)

    geoGrp = proj.asset.geo_grp()
    camGrp = proj.asset.cam_grp()
    charGrp = proj.asset.char_grp()
    propGrp = proj.asset.prop_grp()
    groomGrp = proj.asset.groom_grp()
    tempGrp = proj.asset.temp_grp()
    mattepaintGrp = proj.asset.mattepaint_grp()
    fxGrp = proj.asset.fx_grp()

    skipMembers = [geoGrp, camGrp, charGrp, propGrp, groomGrp, tempGrp, mattepaintGrp, fxGrp, 'persp', 'top', 'front', 'side']

    # list top nodes
    topNodes = mc.ls(assemblies=True)
    refNodes = [a for a in topNodes if mc.referenceQuery(a, inr=True)]
    nonRefNodes = [a for a in topNodes if not a in skipMembers and not mc.referenceQuery(a, inr=True)]

    for refNode in refNodes:
        namespace = mc.referenceQuery(refNode, ns=True)
        tagGrp = '%s:%s.%s' % (namespace[1:], geoGrp, 'assetType')
        shape = mc.listRelatives(refNode, s=True)

        if shape:
            if mc.objectType(shape[0], isType='camera'):
                parent(refNode, camGrp)

        if mc.objExists(tagGrp):
            assetType = mc.getAttr(tagGrp)

            if assetType == 'char':
                parent(refNode, charGrp)
            if assetType == 'prop':
                parent(refNode, propGrp)
            if assetType == 'mattepaint':
                parent(refNode, mattepaintGrp)
            if assetType == 'fx':
                parent(refNode, fxGrp)

    # list yeti node
    nodes = mc.ls(type='pgYetiMaya')
    nodes = [a for a in nodes if not mc.referenceQuery(a, inr=True)]

    for node in nodes:
        if ':' in node:
            transform = mc.listRelatives(node, p=True)[0]
            namespace = transform.split(':')[0]
            subGrp = '%s_grp' % namespace
            parent(transform, subGrp)
            parent(subGrp, groomGrp)

    # non ref nodes
    # for node in nonRefNodes:
    #     parent(node, tempGrp)


def collect_assets(entity, namespaceList = []):
    refPaths = collect_asset_path(entity, namespaceList=namespaceList)
    assetDict = OrderedDict()

    for path in refPaths:
        asset = context_info.ContextPathInfo(path=path)
        if asset.valid: 
            register = register_entity.Register(asset)
            asmPath = register.hero_config_path()
            if os.path.exists(asmPath): 
                reg = register_entity.RegisterInfo(asmPath)
                assetDict[reg.asset_name] = {'name': reg.asset_name, 'project': reg.project, 'id': int(reg.asset_id)}

            # assetDict[reg.asset_name] = {'name': reg.asset_name, 'project': reg.project, 'id': reg.asset_id}
            # asset = context_info.ContextPathInfo(context=context)
            # print asset
    # for path in paths:

    # return assetDict
    return assetDict


def collect_asset_path(entity, unique=False, namespaceList=[]): 
    geo_grp = entity.projectInfo.asset.geo_grp() if entity else 'Geo_Grp'
    grps = []
    if namespaceList:
        for ns in namespaceList:
            geo = '%s:%s' % (ns, geo_grp)
            if mc.objExists(geo):
                grps.append(geo)
    else:
        grps = mc.ls('*:%s' % geo_grp)
    assetDict = OrderedDict()
    refPaths = []
    projectRoot = entity.path.project().abs_path()

    for grp in grps:
        if not unique: 
            attr = '%s.refPath' % grp
            path = mc.getAttr(attr) if mc.objExists(attr) else ''

            if not path:
                if mc.referenceQuery(grp, inr=True):
                    path = mc.referenceQuery(grp, f=True).split('{')[0]
        else: 
            path = mc.referenceQuery(grp, f=True)

        if path:
            if not path in refPaths:
                # if projectRoot in path:
                refPaths.append(path)
    return refPaths
    

def collect_asset_by_path(entity, assetType=''): 
    refPaths = collect_asset_path(entity, unique=True)
    return [a for a in refPaths if assetType in a]


def rebuild_scene():
    current = mc.file(q=True, sn=True)
    current = current if current else mc.file(q=True, location=True)
    current = current if not current == 'unknown' else ''
    if current:
        rebuildFile = '%s.rebuild%s' % (os.path.splitext(current)[0], os.path.splitext(current)[1])
        mc.file(new=True, f=True)
        mc.file(current,  i=True, type='mayaAscii', options='v=0', pr=True, loadReferenceDepth='all')
        mc.file(rename=rebuildFile)
        mc.file(save=True, f=True)



def list_assets(sels=None): 
    refs = mc.file(q=True, r=True)
    grp = 'Geo_Grp'
    refAttr = 'refPath'
    assetDict = dict()

    if sels: 
        refs = get_reference_path(sels)

    for ref in refs:
        assetPath = ref.split('{')[0]
        namespace = mc.referenceQuery(ref, ns=True)[1:]
        targetGrp = '%s:%s.%s' % (namespace, grp, refAttr)
        refPath = mc.getAttr(targetGrp) if mc.objExists(targetGrp) else None 

        if not refPath: 
            entity = context_info.ContextPathInfo(path=assetPath)
            if entity.entity_type == context_info.ContextKey.asset: 
                refPath = assetPath

        if refPath: 
            if not refPath in assetDict:
                assetDict[refPath] = [namespace]  
            else :
                assetDict[refPath].append(namespace)

            # assetDict[refPath] = [namespace] if not refPath in assetDict else assetDict[refPath].append(namespace)?

    return assetDict


def get_reference_path(plys): 
    refPaths = []
    for ply in plys: 
        if mc.referenceQuery(ply, inr=True): 
            path = mc.referenceQuery(ply, f=True)
            refPath = path.split('{')[0]
            refPaths.append(refPath) if not refPath in refPaths else None 
    return refPaths


def create_cam(): 
    scene = context_info.ContextPathInfo()
    inPipe = True if scene.project else False 
    
    # use default if not in the system 
    project = scene.project if inPipe else 'SevenChickMovie'
    projectInfo = project_info.ProjectInfo(project)

    # get global camera 
    cameraPath = projectInfo.asset.global_asset().get('cam')

    # in the pipeline 
    if inPipe: 
        if scene.entity_type == context_info.ContextKey.scene: 
            namespace = '%s_cam' % scene.name_code
    
    # if not in the pipeline 
    if not inPipe: 
        result = mc.promptDialog(title='Scene not in the pipeline',
                                    message='Enter camera namespace:',
                                    button=['OK', 'Cancel'],
                                    defaultButton='OK',
                                    cancelButton='Cancel',
                                    dismissString='Cancel'
                                    )
        if result == 'OK': 
            namespace = mc.promptDialog(query=True, text=True)
            
    if not mc.namespace(ex=namespace): 
        result = mc.file(cameraPath, r=True, ns=namespace)
        
    
    else: 
        mc.confirmDialog(title='Warning', 
                        message='Camera exists "%s" Cannot create camera' % namespace, 
                        button=['OK']
                        )


def write_hiddenGeo_data(objs, entity=None):
    if not entity:
        entity = context_info.ContextPathInfo(path=mc.file(q=True, sn=True))

    dataFile = entity.output_name(outputKey='data', hero=True)
    heroPath = entity.path.hero().abs_path()
    dataFile = '%s/%s' % (heroPath, dataFile)
    
    data = dict()
    if os.path.exists(dataFile):
        data = file_utils.ymlLoader(dataFile)

    data['hiddenGeo'] = objs
    file_utils.ymlDumper(dataFile, data)
    logger.info('Hidden Geo data updated: %s' %dataFile)

def read_hiddenGeo_data(entity=None):
    if not entity:
        entity = context_info.ContextPathInfo(path=mc.file(q=True, sn=True))

    dataFile = entity.output_name(outputKey='data', hero=True)
    heroPath = entity.path.hero().abs_path()
    dataFile = '%s/%s' % (heroPath, dataFile)

    data = []
    if os.path.exists(dataFile):
        data_from_file = file_utils.ymlLoader(dataFile)
        if 'hiddenGeo' in data_from_file:
            data = data_from_file['hiddenGeo']

    return data

def write_autoGeneratedRig_data(value, entity=None, res=None):
    from rf_utils import file_utils as fu

    if not entity:
        entity = context_info.ContextPathInfo(path=mc.file(q=True, sn=True))
    dataFile = entity.output_name(outputKey='data', hero=True)
    heroPath = entity.path.hero().abs_path()
    dataFile = '%s/%s' % (heroPath, dataFile)

    # model ABC
    entityModel = entity.copy()
    entityModel.context.update(step='model', process='main')
    publishPath = entityModel.path.output_hero().abs_path()
    abcCacheFile = entityModel.output(outputKey='cache')
    abcPath = '%s/%s' % (publishPath, abcCacheFile)
    currentHash = fu.md5(abcPath) if os.path.exists(abcPath) else ''

    data = dict()
    if os.path.exists(dataFile):
        data = file_utils.ymlLoader(dataFile)
    if not res:
        if entity.res:
            res = entity.res
        else:
            res = 'md'
    if res == 'md':
        data['autoGeneratedRig'] = value
        data['autoGeneratedRigHash'] = currentHash
    elif res == 'pr':
        data['autoGeneratedRigPr'] = value
        data['autoGeneratedRigPrHash'] = currentHash
    file_utils.ymlDumper(dataFile, data)
    logger.info('Auto generated rig data updated: %s' %dataFile)


def write_autoGeneratedRig_data(value, entity=None, res=None):
    from rf_utils import file_utils as fu

    if not entity:
        entity = context_info.ContextPathInfo(path=mc.file(q=True, sn=True))
    dataFile = entity.output_name(outputKey='data', hero=True)
    heroPath = entity.path.hero().abs_path()
    dataFile = '%s/%s' % (heroPath, dataFile)

    # model ABC
    entityModel = entity.copy()
    entityModel.context.update(step='model', process='main')
    publishPath = entityModel.path.output_hero().abs_path()
    abcCacheFile = entityModel.output(outputKey='cache')
    abcPath = '%s/%s' % (publishPath, abcCacheFile)
    currentHash = fu.md5(abcPath) if os.path.exists(abcPath) else ''

    data = dict()
    if os.path.exists(dataFile):
        data = file_utils.ymlLoader(dataFile)
    if not res:
        if entity.res:
            res = entity.res
        else:
            res = 'md'
    if res == 'md':
        data['autoGeneratedRig'] = value
        data['autoGeneratedRigHash'] = currentHash
    elif res == 'pr':
        data['autoGeneratedRigPr'] = value
        data['autoGeneratedRigPrHash'] = currentHash
    file_utils.ymlDumper(dataFile, data)
    logger.info('Auto generated rig data updated: %s' %dataFile)
    
def read_autoGeneratedRig_data_old(entity=None):
    from rf_utils import file_utils as fu
    if not entity:
        entity = context_info.ContextPathInfo(path=mc.file(q=True, sn=True))

    dataFile = entity.output_name(outputKey='data', hero=True)
    heroPath = entity.path.hero().abs_path()
    dataFile = '%s/%s' % (heroPath, dataFile)

    # model ABC 
    entity.context.update(step='model', process='main')
    publishPath = entity.path.output_hero().abs_path()
    abcCacheFile = entity.output(outputKey='cache')
    abcPath = '%s/%s' % (publishPath, abcCacheFile)
    currentHash = fu.md5(abcPath) if os.path.exists(abcPath) else ''

    data = True
    prevHash = ''
    if os.path.exists(dataFile):
        data_from_file = file_utils.ymlLoader(dataFile)
        if entity.res == 'md':
            if 'autoGeneratedRig' in data_from_file:
                data = data_from_file['autoGeneratedRig']
            if 'autoGeneratedRigHash' in data_from_file: 
                prevHash = data_from_file['autoGeneratedRigHash']
        elif entity.res == 'pr':
            if 'autoGeneratedRigPr' in data_from_file:
                data = data_from_file['autoGeneratedRigPr']
            if 'autoGeneratedRigPrHash' in data_from_file: 
                prevHash = data_from_file['autoGeneratedRigPrHash']

    # if not prevHash or not currentHash: 
    #     return True
    # else: 
    #     return False if prevHash == currentHash else True

    return False


def read_autoGeneratedRig_data(entity=None):
    generate = False
    from rf_utils import file_utils as fu
    if not entity:
        entity = context_info.ContextPathInfo(path=mc.file(q=True, sn=True))

    dataFile = entity.output_name(outputKey='data', hero=True)
    heroPath = entity.path.hero().abs_path()
    dataFile = '%s/%s' % (heroPath, dataFile)

    # model ABC 
    entity.context.update(step='model', process='main')
    publishPath = entity.path.output_hero().abs_path()
    abcCacheFile = entity.output(outputKey='cache')
    abcPath = '%s/%s' % (publishPath, abcCacheFile)
    currentHash = fu.md5(abcPath) if os.path.exists(abcPath) else ''
    prevHash = ''

    # if OS, disable 
    if os.environ.get('RFSTUDIO') == 'OS': 
        return False

    # condition
    # if no rig file - always generate auto rig 
    rigFileName = entity.output_name(outputKey='rig', hero=True, ext='.mb')
    rigFile = '%s/%s' % (heroPath, rigFileName)

    if not os.path.exists(rigFile): 
        generate = True # generate 

    else: 
        # read data 
        # check if this is human rig or autogenerate 
        
        # no data file - human rig 
        if not os.path.exists(dataFile): 
            generate = False # do nothing 
            logger.warning('Skip autogen. No data file found, possibly human rig.')
        else: 
            data_from_file = file_utils.ymlLoader(dataFile)
            if entity.res == 'md':
                if 'autoGeneratedRig' in data_from_file:
                    autoGeneratedRig = data_from_file['autoGeneratedRig']
                if 'autoGeneratedRigHash' in data_from_file: 
                    prevHash = data_from_file['autoGeneratedRigHash']
            elif entity.res == 'pr':
                if 'autoGeneratedRigPr' in data_from_file:
                    autoGeneratedRig = data_from_file['autoGeneratedRigPr']
                if 'autoGeneratedRigPrHash' in data_from_file: 
                    prevHash = data_from_file['autoGeneratedRigPrHash']
            # check if autogenerate = False - human rig do nothing 
            # True mean auto gen 
            if autoGeneratedRig: 
                # no hash found, regenerate 
                if not prevHash or not currentHash: 
                    logger.warning('Allow autogen. No hash found')
                    generate = True 
                else: 
                    # geometry not change, do not generate 
                    if prevHash == currentHash: 
                        logger.warning('Skip. Geometry is latest.')
                        generate = False
                    else: 
                        logger.warning('Allow autogen. Geometry has changed.')
                        generate = True

    return generate


def asset_preroll_postroll(entity): 
    key = 'PreRollPostRoll'
    return read_misc_data(entity, key)


def read_misc_data(entity, key): 
    dataFile = entity.output_name(outputKey='data', hero=True)
    heroPath = entity.path.hero().abs_path()
    dataFile = '%s/%s' % (heroPath, dataFile)

    data = []
    if os.path.exists(dataFile):
        rawData = file_utils.ymlLoader(dataFile)
        if key in rawData:
            data = rawData[key]

    return data


def isolate_reference(namespace, refPaths=[], entity=None, dependencyList=None): 
    """ this function isolate only given namespace and other refs will be unloaded
    - other refs are only ref that contain Geo_Grp
    - if dependencyList has included, isolate will consider this data too """ 
    dependencyPairs = [namespace]

    if dependencyList: 
        for pair in dependencyList: 
            if namespace in pair: 
                dependencyPairs = pair
                break 

    # load only given namespace 
    if not refPaths: 
        refPaths = all_geo_ref(entity=entity, returnType='path')
    namespaces = [mc.referenceQuery(a, ns=True) for a in refPaths]
    
    # unload all refs 
    if namespace in [a[1:] for a in namespaces]: 

        for path in refPaths: 
            ns = mc.referenceQuery(path, ns=True)
            rn = mc.referenceQuery(path, referenceNode=True)
            print 'path', path
            # if isolate, load if unloaded 
            if ns[1:] in dependencyPairs: 
                if not mc.referenceQuery(path, isLoaded=True): 
                    print 'load ref %s' % rn
                    mc.file(path, loadReference=rn, prompt=False)
                continue 
            
            # unload if load 
            else: 
                if mc.referenceQuery(path, isLoaded=True): 
                    print 'unload ref %s' % rn
                    mc.file(path, unloadReference=rn)

    # do nothing if isolate not in all namespaces 
    else: 
        logger.warning('namespace: %s not in the active namespace target %s. Isolate reference Abort!!' % (namespace, namespaces))


def all_geo_ref(entity=None, returnType='namespace'): 
    geoGrp = entity.projectInfo.asset.geo_grp() if entity else 'Geo_Grp'
    refs = mc.file(q=True, r=True)
    namespaces = []
    refPaths = []
    refNodes = []

    for ref in refs: 
        if mc.referenceQuery(ref, isLoaded=True):
            ns = mc.referenceQuery(ref, ns=True)
            rnNode = mc.referenceQuery(ref, referenceNode=True)
            namespaces.append(ns)
            checkGrp = '%s:%s' % (ns[1:], geoGrp)

            if mc.objExists(checkGrp): 
                namespaces.append(ns)
                refPaths.append(ref)
                refNodes.append(rnNode)

    if returnType == 'namespace': 
        return namespaces 
    if returnType == 'path': 
        return refPaths
    if returnType == 'rnNode': 
        return refNodes

    return namespaces


def list_cache_asset(entity=None): 
    """ list cache assets """ 
    geoGrp = entity.projectInfo.asset.geo_grp() if entity else 'Geo_Grp'
    refs = mc.file(q=True, r=True)
    assets = []
    if refs: 
        namespaces = [mc.referenceQuery(a, ns=True) for a in refs if mc.referenceQuery(a, isLoaded=True)]
        
        for ns in namespaces: 
            assetGeoGrp = '%s:%s' % (ns[1:], geoGrp)
            if mc.objExists(assetGeoGrp):
                assetGeoGrp = [a for a in mc.ls(assetGeoGrp) if mc.referenceQuery(a, inr=True)][0]
                path = mc.referenceQuery(assetGeoGrp, f=True)
                asset = context_info.ContextPathInfo(path=path)
                assets.append((asset, assetGeoGrp, path.split('{')[0]))

    return assets 


def add_geo_texture_tag(geoGrp): 
    """ add texture path to geo """ 
    for geo in pm.PyNode(geoGrp).getChildren(ad=True, type='transform'):
        shape = geo.getShape(type='mesh', ni=True)
        if not shape:
            continue

        # add attribute
        # print 'geo', geo
        if not geo.hasAttr('texture'):
            tex_attr = pm.addAttr(geo, ln='texture', dt='string')

        tex_attr = geo.attr('texture')
        # print tex_attr
        sgs = shape.outputs(type='shadingEngine')
        if not sgs:
            continue

        shaders = sgs[0].surfaceShader.inputs()

        if not shaders:
            continue

        hiss = shaders[0].listHistory()
        for hi in hiss:
            if hi.nodeType() == 'file':
                path = hi.fileTextureName.get()
                if os.path.exists(path):
                    tex_attr.set(path)
                    break



def switch_loop_proxy2cache(): 
    from rf_utils.context import context_info
    from rf_app.sg_manager import utils, dcc_hook
    
    targets = mc.ls(sl=True)
    if targets: 
        for target in targets: 
            # proxy offset 
            print target
            rsproxy = find_rsproxy_node(target)

            if rsproxy: 
                offset = mc.getAttr('%s.frameOffset' % rsproxy[0])
                print rsproxy, offset

                # group 
                group = mc.group(em=True, n='abc_%s' % target.split(':')[0])
                mc.delete(mc.parentConstraint(target, group))

                refPath = mc.referenceQuery(target, f=True)
                entity = context_info.ContextPathInfo(path=refPath)
                dataPath = utils.get_loop_data(entity)
                refs = utils.read_loop_data(dataPath)

                for ref in refs: 
                    asset = context_info.ContextPathInfo(path=ref)
                    node = dcc_hook.create_reference(asset, ref, material=True)
                    mc.parent(node, group, r=True)

                abcNodes = find_alembic_node(group)
                # set offset 
                for node in abcNodes: 
                    print 'node', node
                    mc.setAttr('%s.offset' % node, offset*-1)

            print '===='



def find_rsproxy_node(obj): 
    objs = mc.listRelatives(obj, ad=True)
    targets = []
    for obj in objs: 
        if mc.objectType(obj, isType='mesh'): 
            nodes = mc.listHistory(obj)
            targets += [a for a in nodes if mc.objectType(a, isType='RedshiftProxyMesh')]
    return list(set(targets ))


def find_alembic_node(obj): 
    objs = mc.listRelatives(obj, ad=True)
    targets = []
    for obj in objs: 
        if mc.objectType(obj, isType='mesh'): 
            nodes = mc.listHistory(obj)
            targets += [a for a in nodes if mc.objectType(a, isType='AlembicNode')]
    return list(set(targets ))

