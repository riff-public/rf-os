import os
import sys 
import maya.cmds as mc
import maya.mel as mm 

class Plugins: 
	gpuCache = 'gpuCache.mll'
	AbcExport = 'AbcExport.mll'
	AbcImport = 'AbcImport.mll'
	animImportExport = 'animImportExport.mll'


def load(pluginPath): 
	""" load plugins """
	if not mc.pluginInfo(pluginPath, q=True, l=True): 
		mc.loadPlugin(pluginPath, qt=True)

def unload(pluginPath): 
	""" unload plugins """
	if mc.pluginInfo(pluginPath, q=True, l=True): 
		mc.unloadPlugin(pluginPath)