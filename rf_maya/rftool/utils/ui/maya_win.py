import os 
import sys 
import maya.cmds as mc 
import maya.mel as mm
import maya.OpenMayaUI as mui
from Qt import wrapInstance
from Qt import QtWidgets


def getMayaWindow():
    """ Maya Qt pointer """
    ptr = mui.MQtUtil.mainWindow()
    return wrapInstance(long(ptr), QtWidgets.QWidget)


def deleteUI(ui):
    """ delete Maya UI """
    if mc.window(ui, exists=True):
        mc.deleteUI(ui)
        deleteUI(ui)