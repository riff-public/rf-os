import sys, os
import json

import maya.cmds as mc
import maya.mel as mel
import pymel.core as pm

# import imath
import argparse
# from collections import OrderedDict

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_maya.lib import sequencer_lib
from rf_maya.rftool.utils import yeti_lib
# from rf_maya.rftool.utils import abc_utils
# reload(abc_utils)
from rf_maya.rftool.fix import moveSceneToOrigin
from rf_maya.rftool.anim import anim_utils
reload(anim_utils)

# from rf_utils import cask
from rf_utils.context import context_info
from rf_utils import register_shot
from rf_utils import register_entity
from rf_utils.pipeline import shot_data
reload(shot_data)
# from rf_utils import file_utils
from rf_utils.pipeline import check
reload(check)
from rf_utils.pipeline import asset_tag

from rf_app.publish.scene import app
from rf_app.publish.scene.export import yeti_cache
reload(app)

lockedNs = 'N/A'
CAM_DIST_KEY = 'cameraDistanceFromOrigin'
CAM_TRANSFORMS_KEY = 'cameraTransforms'

def setup_parser(title):
    # create the parser
    parser = argparse.ArgumentParser(description=title, formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('-s', dest='scene', type=str, help='The path to the source scene to be cached', required=True)
    parser.add_argument('-l', dest='log', type=str, help='The path to temp log file for result', default='')
    parser.add_argument('-n', dest='namespaces', nargs='+', help='The namespaces of asset to be cached', default=[])
    parser.add_argument('-o', dest='cacheAtOrigin', type=bool, help='To shift ABC base on cam to origin', default=True)
    return parser

def get_non_transfrom_yeti():
    non_transforms = []

    for yetiNode in pm.ls(type='pgYetiMaya'):
        rootNode = pm.pgYetiGraph(yetiNode, getRootNode=True)
        nodeType = pm.pgYetiGraph(yetiNode, node=rootNode, nodeType=True)
        if nodeType != 'transform':
            non_transforms.append(yetiNode.shortName())
    return non_transforms

def get_non_userattr_yeti():
    non_userattrs = []
    for yetiNode in pm.ls(type='pgYetiMaya'):
        for axis in 'XYZ':
            if not yetiNode.hasAttr('yetiVariableF_tran%s' %axis):
                non_userattrs.append(yetiNode.shortName())
                break
    return non_userattrs

def main(scene, namespaces=[], log='', exportCache=True, cacheAtOrigin=True):
    # needs to load the ABC plugin
    logger.info(':::: Start export_yeti_cache :::')
    plugin = 'AbcImport.mll'

    result = {}
    try: 
        if not mc.pluginInfo(plugin, q=True, l=True):
            mc.loadPlugin(plugin, qt=True)
    except Exception as e: 
        logger.error(e)
        return

    # need to cast scene str as ContextPathInfo object (in case it was passed)
    if isinstance(scene, (str, unicode)):
        # make virtual file name 
        logger.info('input scene %s' % scene)
        mc.file(rename=scene)
        scene = context_info.ContextPathInfo(path=scene)

    # add camera varaint 
    # we agree to only use main for camera yeti calculation
    scene.context.update(look='main')

    # get duration from SG
    reg = register_shot.Register(scene)
    shotName = scene.name_code
    duration = sequencer_lib.get_duration(scene)
    pre, post = sequencer_lib.in_pre_post_roll(scene)
    sf = scene.projectInfo.scene.start_frame
    duration = duration + pre + post
    ef = (sf + duration) - 1
    qsf = sf
    qef = ef

    # logs
    logger.debug('shot duration %s' % duration)

    # get namespace from _data .yml
    all_namespaces = reg.get.asset_list()
    lock_ns = get_lock_ns(scene)
    
    if lock_ns: 
        all_namespaces = [a for a in all_namespaces if not a in lock_ns]
        logger.debug('** yeti locked namespaces %s' % lock_ns)

    logger.debug('all_namespaces %s' % all_namespaces)

    # figure out the namespace to cache
    if namespaces:  # if the namespace list is provided
        ns_to_cache = [n for n in all_namespaces if n in namespaces]
    else: # try to cache all assets
        ns_to_cache = [n for n in all_namespaces if n.endswith('_Tech')]

    logger.debug('ns_to_cache %s' % ns_to_cache)

    techGrp = scene.projectInfo.asset.tech_grp()
    groomGrp = scene.projectInfo.asset.groom_grp()
    groomGeoGrp = scene.projectInfo.asset.groom_geo_grp()
    
    offsets = []
    camBBCenter = [0.0, 0.0, 0.0]
    if cacheAtOrigin:
        camBBCenter, offsets = moveSceneToOrigin.get_cameraDistanceFromOrigin(scene)

    for ns in ns_to_cache:
        logger.info('\n\n ** Starting on %s ... **' % ns)
        # get the geo ABC path
        abc_path = reg.get.cache(ns, hero=True)
        alembicNs = reg.get.asset(key=ns).get('exportNamespace')
        # get description 
        desPath = register_shot.Output(reg.get.asset(key=ns)).get_description()
        asset = context_info.ContextPathInfo(path=desPath)

        if not asset.type == 'char': 
            logger.info('%s Not a character, Skip!' % ns)
            continue

        ns = alembicNs
        if not os.path.exists(abc_path):
            msg = 'Geo ABC is missing: %s' % abc_path
            result[ns] = msg
            writeLog(log, result)
            print msg
            logger.info(msg)
            logger.debug('** %s abort **\n\n' % ns)
            continue

        # get groom hero file
        assetDescriptionPath = reg.get.asset(ns).get('description')
        if not os.path.exists(assetDescriptionPath):
            msg = 'Asset description is missing: %s' % assetDescriptionPath
            result[ns] = msg
            writeLog(log, result)
            print msg
            logger.info(msg)
            logger.debug('** %s abort **\n\n' % ns)
            continue

        assetReg = register_entity.RegisterInfo(assetDescriptionPath)
        groomPath = assetReg.get.groom_yeti().get('heroFile')
        # groomPath = 'N:/Staff/Groom/key/hair_cara/cara_grm_old.ma'

        if not os.path.exists(str(groomPath)):
            msg = 'Groom hero file is missing: %s' % groomPath
            result[ns] = msg
            writeLog(log, result)
            print msg
            logger.info(msg)
            logger.debug('** %s abort **\n\n' % ns)
            continue

        # new scene, reference groom hero file
        mc.file(new=True, f=True)

        # set FPS
        fps = str(scene.projectInfo.render.fps())
        time = {'15': 'game', 
            '24': 'film',
            '25': 'pal',
            '30': 'ntsc',
            '48': 'show',
            '50': 'palf',
            '60': 'ntscf' }
        if fps in time:
            mc.currentUnit(time=time[fps])
            logger.info('Setting fps: %s:%s' %(fps, time[fps]))

        # import groom hero
        mc.file(groomPath, i=True, ns=ns, mnc=True)

        # check the names the ABC file
        yetiGrpName = '%s|%s' %(groomGrp, groomGeoGrp)
        yetiGrpPath = '|'.join(['%s:%s' %(ns, grp) for grp in yetiGrpName.split('|')])
        if not mc.objExists('|%s' % yetiGrpPath):  # Yeti_Grp must sits under world only
            msg = 'Cannot find %s' % yetiGrpPath
            result[ns] = msg
            # writeLog(log, result)
            print msg
            logger.info(msg)
            logger.debug('** %s abort **\n\n' % ns)
            continue

        # need to add TechGeo_Grp on top of YetiGeo_Grp
        techGeoGrp = mc.group(em=True, n='%s:TechGeo_Grp' %ns)
        mc.parent('|%s' %(yetiGrpPath.split('|')[0]), techGeoGrp)

        logger.debug('abc_path %s' % abc_path)
        checker = check.CheckHierarchy('%s|%s' %(techGeoGrp, yetiGrpPath), abc_path, addNamespaceToAbc=False)
        res = checker.check()

        if res[check.Config.mismatch] or res[check.Config.missing] or res[check.Config.alien]:
            msg = 'Object check failed'
            # msg += '\n%s' %str(res)
            result[ns] = msg
            # writeLog(log, result)
            print msg
            logger.debug('** %s abort **\n\n' % ns)
            logger.warning(msg)
            logger.warning(res)
            continue

        # before the attach, need to check yeti graph root node for transform node if cacheAtOrigin
        if cacheAtOrigin:
            non_transforms = get_non_transfrom_yeti()
            if non_transforms:
                msg = 'Not all yeti node has transform node as root node. %s' %(non_transforms)
                result[ns] = msg
                print msg
                logger.error(msg)
                continue

        techGeoGrpNode = pm.PyNode(techGeoGrp)
        gromGeoGrpNode = pm.PyNode(yetiGrpPath)
        # --- attach/blendshape groom to follow alembic
        # check for static curves
        static_curves = check.getStaticObjInABC(abc_path, ['Curve'])
        logger.debug('** Static curves: %s' %[c.name for c in static_curves])
        if static_curves:
            logger.info('Static curve found, using blendshape method.')
            # create reference of animated TechGeo (no Yeti fiber)
            ref = pm.createReference(abc_path, ns='techGeoTMP')
            ref_ns = ref.namespace

            # create blendshape
            try:
                techYetiGeoGrp = pm.PyNode('|'.join(['%s:%s' %(ref_ns, grp) for grp in yetiGrpName.split('|')]))
                bsh = pm.blendShape(techYetiGeoGrp, gromGeoGrpNode, tc=True, origin='local', automatic=True)[0]
                bsh.w[0].set(1.0)

            except Exception, e:
                msg = 'Alembic blendshape failed\n'
                msg += str(e)
                result[ns] = 'Alembic blendshape failed'
                # writeLog(log, result)
                print msg
                logger.warning(msg)
                continue

            # try to connect transforms so blendshape in local mode works
            # in case all curves has static poses, but rigged with constraints
            logger.info('Connecting transforms for blendshapes...')
            for tr in techGeoGrpNode.getChildren(ad=True, type='transform'):
                nodeName = tr.shortName()
                cacheNodeName = nodeName.replace('%s:'%ns, '%s:' %ref_ns)
                try:
                    cacheNode = pm.PyNode(cacheNodeName)
                except:
                    logger.debug('** Cannot find: %s' %(cacheNodeName))
                    continue

                for attr in 'trs':
                    for axis in 'xyz':
                        attrName = '%s%s' %(attr, axis)
                        try:
                            pm.connectAttr(cacheNode.attr(attrName), tr.attr(attrName), f=True)
                        except:
                            logger.debug('** Cannot connect: %s.%s ---> %s.%s' %(cacheNode, attrName, tr, attrName))
        else:
            logger.info('No static curve found, using ABC attach method.')
            # attach alembic
            yetiGrpAbcPath = '/'.join(['%s:%s' %(ns, grp) for grp in ('%s/%s/%s' %(techGrp, groomGrp, groomGeoGrp)).split('/')])
            try:
                pm.AbcImport(abc_path, ct=techGeoGrp, ft=yetiGrpAbcPath)
                keys = reg.get.asset_list(type='corrective')
                if keys:
                    for k in keys:
                        if ns in k:
                            corrPath = reg.get.maya(k, 'sim', hero=True)
                            namespace = '%s_corrective'%ns
                            yeti_lib.import_corrective_groom(corrPath, namespace=namespace)
                            correctives = mc.ls('*_Geo')
                            mc.parent(correctives, techGeoGrp)
                logger.info('** %s attached success **' % ns)
            except Exception, e:
                msg = 'Alembic attach failed\n'
                msg += str(e)
                result[ns] = 'Alembic attach failed'
                # writeLog(log, result)
                print msg
                logger.warning(msg)
                continue

        # apply the offset
        cacheOrigin = scene.projectInfo.scene.cache_at_origin
        # offsetAttr = mc.objExists('%s.%s' % (techGeoGrp, asset_tag.Attr.cacheOffset))
        if cacheAtOrigin and cacheOrigin:
            logger.info('Moving to origin...')
            yetiGrpNode = gromGeoGrpNode.getParent()
            for child in yetiGrpNode.getChildren(type='transform'):
                if child.nodeName() == '%s:%s' %(ns, groomGeoGrp):
                    child.inheritsTransform.set(True)
                    logger.debug('** Inherits transform ON: %s' %child.shortName())
                else:
                    child.inheritsTransform.set(False)
                    logger.debug('** Inherits transform OFF: %s' %child.shortName())

            # check if all yeti nodes have tranX, tranY, tranZ attribute
            # new method
            if not get_non_userattr_yeti():
                logger.info('Setting keyframes on TechGeo and yetiUserAttr...')

                orig_offsets = [(f[0], f[1], f[2]) for f in offsets]
                neg_offsets = [(f[0]*-1, f[1]*-1, f[2]*-1) for f in orig_offsets]
                # add update cause change to cache tech_geo at center position
                neg_offsets_w_cut_center = [(f[0]-(camBBCenter[0]*-1), f[1]-(camBBCenter[1]*-1),f[2]-(camBBCenter[2]*-1)) for f in neg_offsets]

                # generate keyframes
                acx, acy, acz = anim_utils.generate_position_anim_curve(positions=offsets, start=sf, end=ef)
                neg_acx, neg_acy, neg_acz = anim_utils.generate_position_anim_curve(positions=neg_offsets_w_cut_center, start=sf, end=ef)

                logger.info('Keyframes generated.')

                # creayre cluster fix bug Bikey groom
                techYetiGeoGrpName = pm.PyNode('|'.join(['%s:%s' %(ns, grp) for grp in yetiGrpName.split('|')]))
                cluster = pm.cluster(techYetiGeoGrpName, name = 'cc1')
                pm.delete(cluster) # for fixed bug yeti explode, don't know why but it's work
                cluster = pm.cluster(techYetiGeoGrpName, name = 'cc1')
                clusterNode = pm.PyNode(cluster[-1])

                # connect techGeoGrp keys
                pm.connectAttr(neg_acx.output, clusterNode.tx)
                pm.connectAttr(neg_acy.output, clusterNode.ty)
                pm.connectAttr(neg_acz.output, clusterNode.tz)
                # pm.connectAttr(neg_acx.output, techGeoGrpNode.tx)
                # pm.connectAttr(neg_acy.output, techGeoGrpNode.ty)
                # pm.connectAttr(neg_acz.output, techGeoGrpNode.tz)

                # connect YetiNode user attr keys
                for yetiNode in pm.ls(type='pgYetiMaya'):
                    yetiNodeName = yetiNode.shortName()
                    logger.debug('** Connecting keyframe on: %s' %yetiNodeName)
                    pm.connectAttr(acx.output, yetiNode.yetiVariableF_tranX)
                    pm.connectAttr(acy.output, yetiNode.yetiVariableF_tranY)
                    pm.connectAttr(acz.output, yetiNode.yetiVariableF_tranZ)
                    logger.info('Keyframe set on Yeti user attr: %s' %yetiNodeName)

            # old method
            else:
                # remove cause already move to center position at cache process
                # logger.info('Moving TechGeo and Yeti transform to center position...')
                
                # # move techGeoGrp with negative offset
                # neg_offset = (camBBCenter[0]*-1, camBBCenter[1]*-1, camBBCenter[2]*-1)
                # pm.xform(techGeoGrpNode, ws=True, t=neg_offset)
                # logger.info('%s moved with offset: %s, %s, %s' %(techGeoGrpNode.shortName(), neg_offset[0], neg_offset[1], neg_offset[2]))

                # apply offset value to all yeti node transform node
                for yetiNode in pm.ls(type='pgYetiMaya'):
                    yetiNodeName = yetiNode.shortName()
                    logger.debug('** Setting offset on: %s' %yetiNodeName)
                    rootNode = pm.pgYetiGraph(yetiNode, getRootNode=True)
                    pm.pgYetiGraph(yetiNode, node=rootNode, param='translate', setParamValueVector=camBBCenter)
                    logger.info('Offset set on %s/%s: %s, %s, %s' %(yetiNodeName, rootNode, camBBCenter[0], camBBCenter[1], camBBCenter[2]))

        # export the yeti cache
        logger.info('** %s preprocess success. preparing to export yeti cache **' % ns)
        mc.shot(shotName, startTime=sf, endTime=ef, sequenceStartTime=qsf, sequenceEndTime=qef)  # need to create sequencer first

        if exportCache: 
            # clear config 
            yeti_cache.clear_config(scene)
            data = app.get_export_dict(scene, shotName, ['yeti_cache'])    
            logger.debug('Export with data %s\n' % data)
            
            app.do_export(scene, itemDatas=data, uiMode=False, publish=True, sendNoti=False)
            logger.info('** %s export yeti success **' % ns)

            # save output file for debug
            localScene = scene .copy()
            localScene.context.update(process='tmp')
            workpath = localScene.path.workspace().abs_path()
            localScene.context.update(process=ns, step='yeti')
            tmpFile = '%s/%s' % (workpath, localScene.work_name())

        # logger.debug('Saving debug scene %s' % tmpFile)
        # os.makedirs(workpath) if not os.path.exists(workpath) else None
        # try: 
        #     mc.file(rename=tmpFile)
        #     mc.file(save=True, f=True, type='mayaAscii')
        #     logger.debug('Saved %s' % tmpFile)
        # except Exception as e: 
        #     logger.error(e)

        # if not data: 
        #     logger.error('** yeti data return Nothing. Could be "yeti_cache" not in the config. Failed to export!')
        #     continue

            result[ns] = True

    writeLog(log, result)
    return result
        
def writeLog(log, data):
    if log:
        with open(log, 'w') as f:
            json.dump(data, f, sort_keys=True, indent=4, separators=(',', ':'))


def get_lock_ns(entity): 
    from rf_utils import register_sg
    global lockedNs
    if lockedNs == 'N/A': 
        logger.debug('Fetching shotgun for locked namespaces ...')
        reg = register_sg.ShotElement(entity)
        lockedNs = reg.list_lock(entity.step)
    
    return lockedNs

if __name__ == "__main__":
    print ':::::: Export Yeti Cache ::::::'
    logger.info(':::::: Export Yeti Cache ::::::')

    parser = setup_parser('Export Yeti Cache')
    params = parser.parse_args()
    
    main(scene=params.scene, 
        namespaces=params.namespaces, 
        log=params.log,
        cacheAtOrigin=params.cacheAtOrigin)


'''
"C:/Program Files/Autodesk/Maya2017/bin/mayapy.exe" "D:/dev/core/rf_maya/rftool/utils/export_yeti_cache.py" -s "P:/projectName/scene/work/ep01/pr_ep01_q0050_s0110/anim/main/maya/pr_ep01_q0050_s0110_anim_main.v001.Nu.ma" -n "canis_001_Tech"
'''
