# collect information for this shot. 
import os
import sys 
from functools import partial

# maya 
import maya.cmds as mc 
import maya.mel as mm 
import pymel.core as pm 

# pipeline
from rftool.utils import path_info 
from rftool.polytag import polytag_core
from rf_utils.sg import sg_utils
from rftool.utils.ui import maya_win
sg = sg_utils.sg


def list_assets(polytag=False): 
	""" list asset in shots """
	refs = mc.file(q=True, r=True)
	refs = list(set([a.split('{')[0] for a in refs]))
	ptagAssets = []

	if polytag: 
		info = polytag_core.list_polytag()
		ptagAssets = [path_info.PathInfo(path='%s/' % info[a][0]) for a in info]

	if refs: 
		assets = [path_info.PathInfo(path=a) for a in refs]
		return assets + ptagAssets

def send_to_sg(assets): 
	""" update assets field sg """ 
	shot = path_info.PathInfo()
	sgEntities = []
	outPipeline = []

	for asset in assets: 
		sgEntity = asset.sgEntity
		if sgEntity: 
			sgEntities.append(sgEntity)
		else: 
			outPipeline.append(asset)
			print 'Not in pipeline %s' % asset

	data = {'assets': sgEntities}
	return sg.update('Shot', shot.sgEntity.get('id'), data)


def update_sg_asset_list(*args): 
	""" ui callback """ 
	assets = list_assets(polytag=True)
	send_to_sg(assets)


def show(): 
	""" ui """ 
	uiName = 'shotInfoUtils'
	maya_win.deleteUI(uiName)
	mc.window(uiName, t='Shot Info Utilities')

	mc.columnLayout(adj=1, rs=4)
	mc.text(l='Shot info utilities')
	mc.button(l='Send Asset List to SG', h=30, c=partial(update_sg_asset_list))
	mc.setParent('..')

	mc.showWindow()
	mc.window(uiName, e=True, wh=[200, 100])
