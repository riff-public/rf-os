import pymel.core as pm
import maya.cmds as mc
import maya.OpenMaya as om
import maya.mel as mel

from nuTools import misc

def toTempRsMaterial():
    # do it for selected meshes
    sels = pm.selected(type='transform')
    if sels:
        shps = [s.getShape(ni=True) for s in sels if s.getShape(ni=True, type='mesh')]
        sgs = set()
        for shp in shps:
            con_sgs = shp.outputs(type='shadingEngine')
            for sg in con_sgs:
                sgs.add(sg)
    else: # do it for all sg in the scene
        sgs = pm.ls(type='shadingEngine')
    for sg in sgs:
        # skip default sg
        if sg.nodeName() == 'initialShadingGroup':
            continue

        shaders = sg.surfaceShader.inputs()
        if shaders:
            shader = shaders[0]
            if shader.nodeType() == 'RedshiftMaterial':
                om.MGlobal.displayWarning('Skipping %s: Is already a RedshiftMaterial.' %shader.nodeName())
                continue
            # only do if the shader has "color" attribute
            if shader.hasAttr('color'):
                oldShdName = shader.nodeName().split(':')[-1]
                shdSplits = oldShdName.split('_')
                if len(shdSplits) > 1:
                    elem = ''.join(shdSplits[:-1])
                else:
                    elem = shdSplits[0]
                rsName = '%s_rsMaterial' %elem

                rsMat = pm.shadingNode('RedshiftMaterial', asShader=True, n=rsName)
                rsMat.refl_weight.set(0)
                pm.connectAttr(rsMat.outColor, sg.surfaceShader, f=True)
                # if it has texture
                textureInputs = shader.color.inputs(p=True)
                if textureInputs:
                    texSrcAttr = textureInputs[0]
                    pm.connectAttr(texSrcAttr, rsMat.diffuse_color)
                else: # no texture connected, try setting attributes
                    col_value = shader.color.get()
                    trans_value = shader.transparency.get()
                    rsMat.diffuse_color.set(col_value)
                    rsMat.opacity_color.set((1.0-trans_value[0], 1.0-trans_value[1], 1.0-trans_value[2]))

                # specular to roughness
                if shader.hasAttr('specularColor'):
                    spec_inputs = shader.specularColor.inputs(p=True)
                    if spec_inputs:
                        spec_src_attrs = spec_inputs[0]
                        spc_src_children = spec_src_attrs.getChildren()
                        if spc_src_children:
                            pm.connectAttr(spc_src_children[0], rsMat.refl_roughness)
                            rsMat.refl_weight.set(1)
                # bump
                if shader.hasAttr('normalCamera'):
                    norm_inputs = shader.normalCamera.inputs()
                    if norm_inputs and norm_inputs[0].nodeType() == 'bump2d':
                        old_bump_node = norm_inputs[0]
                        bump_tex_attrs = old_bump_node.bumpValue.inputs()
                        if bump_tex_attrs and bump_tex_attrs[0].nodeType() == 'file':
                            bump_file = bump_tex_attrs[0]
                            # create rs bump
                            oldBumpName = old_bump_node.nodeName().split(':')[-1]
                            bumpSplits = oldBumpName.split('_')[0]
                            if len(bumpSplits) > 1:
                                elem = ''.join(bumpSplits[:-1])
                            else:
                                elem = bumpSplits[0]

                            rsNmlName = '%s_rsNormalMap' %(elem)
                            rsNml = pm.shadingNode('RedshiftNormalMap', asUtility=True, n=rsNmlName)
                            
                            if bump_file.uvTilingMode.get():
                                texturePath = bump_file.computedFileTextureNamePattern.get()
                            else:
                                texturePath = bump_file.fileTextureName.get()

                            rsNml.tex0.set(texturePath)
                            pm.connectAttr(rsNml.outDisplacementVector, rsMat.bump_input)
            else:
                om.MGlobal.displayWarning('Skipping %s: No color plug found.' %shader.nodeName())
        else:
            om.MGlobal.displayWarning('Skipping %s: No shader found.' %sg.nodeName())
                    

def toTexturedAnimShade(objs=[], resolution=512):
    sels = pm.selected(type='transform')
    if sels:
        shps = [s.getShape(ni=True) for s in sels if s.getShape(ni=True, type='mesh')]
        sgs = set()
        for shp in shps:
            con_sgs = shp.outputs(type='shadingEngine')
            for sg in con_sgs:
                sgs.add(sg)
    else: # do it for all sg in the scene
        sgs = pm.ls(type='shadingEngine')
    for sg in sgs:
        # skip default sg
        if sg.nodeName() == 'initialShadingGroup':
            continue

        shaders = sg.surfaceShader.inputs()
        if shaders:
            shader = shaders[0]
            if shader.nodeType() != 'RedshiftMaterial':
                om.MGlobal.displayWarning('Skipping: %s' %shader.nodeName())
                continue

            oldShdName = shader.nodeName().split(':')[-1]
            shdSplits = oldShdName.split('_')
            if len(shdSplits) > 1:
                elem = ''.join(shdSplits[:-1])
            else:
                elem = shdSplits[0]
            lambertName = '%s_lambert' %elem

            lambert = pm.shadingNode('lambert', asShader=True, n=lambertName)

            pm.connectAttr(lambert.outColor, sg.surfaceShader, f=True)
            # if it has texture
            textureInputs = shader.diffuse_color.inputs(p=True)
            if textureInputs:
                texSrcAttr = textureInputs[0]
                pm.connectAttr(texSrcAttr, lambert.color)
            else: # no texture connected, try setting attributes
                col_value = shader.diffuse_color.get()
                op_value = shader.opacity_color.get()
                lambert.color.set(col_value)
                lambert.transparency.set((1.0-op_value[0], 1.0-op_value[1], 1.0-op_value[2]))


