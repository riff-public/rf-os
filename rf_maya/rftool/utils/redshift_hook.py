import os
import sys
import maya.cmds as mc
import maya.mel as mm


presetName = '_RS'

nodeTypes = ['RedshiftArchitectural',
            'RedshiftCarPaint',
            'RedshiftHair',
            'RedshiftIncandescent',
            'RedshiftMaterial',
            'RedshiftMaterialBlender',
            'RedshiftMatteShadowCatcher',
            'RedshiftSkin',
            'RedshiftSprite',
            'RedshiftSubSurfaceScatter']

def readDb(project) :
    """ create default db file. If not exists, generate one """
    dbResult = db.readDatabase(project, dbName='rsMatteID')

    return dbResult, db.dbPathCustom(project, dbName='rsMatteID')


def list_mtr_node() :
    """ list material """

    nodes = []

    for nodeType in nodeTypes:
        nodes = nodes + mc.ls(type=nodeType)

    mtlNodes = dict()
    matteAttr = 'rsMaterialId'

    for eachNode in nodes :
        shadingEngine = mc.listConnections(eachNode, t = 'shadingEngine')

        if shadingEngine :
            attr = '%s.%s' % (shadingEngine[0], matteAttr)
            if mc.objExists(attr) :
                id = mc.getAttr(attr)

                mtlNodes[eachNode] = id

    return mtlNodes

def set_id(material, value) :
    idAttr = '%s.rsMaterialId' % material
    if mc.objectType(material) in nodeTypes:
        if not mc.objExists(idAttr):
            mc.addAttr(material, ln='rsMaterialId', at='long')
            mc.setAttr(idAttr, e=True, keyable=True)

    attr = matte_id_attr(material)

    if attr:
         mc.setAttr('%s' % attr, value)
         mc.setAttr(idAttr, value)

def matte_id_attr(material):
    shadingEngine = mc.listConnections(material, t='shadingEngine')

    if shadingEngine:
        return '%s.rsMaterialId' % shadingEngine[0]

def set_object_id(obj, value):
    pass


def assign_multi_matte(mmName, color, mID, materialId):
    name = mmName
    attrMap = {'red': 'redId', 'green': 'greenId', 'blue': 'blueId'}
    multiMatteNode = name
    mode = 1

    if materialId:
        mode = 0

    if not mc.objExists(name) :
        multiMatteNode = create_puzzle_matte(name, mode)

    attr = '%s.%s' % (multiMatteNode, attrMap[color])

    if mc.objExists(attr) :
        mc.setAttr(attr, mID)

        # current layer
        renderLayer = mc.editRenderLayerGlobals(q=True, currentRenderLayer=True)
        if not renderLayer == 'defaultRenderLayer':
            # set renderLayer override
            mc.setAttr('%s.%s' % (multiMatteNode, 'enabled'), 0)
            mc.editRenderLayerAdjustment('%s.enabled' % multiMatteNode)
            mc.setAttr('%s.%s' % (multiMatteNode, 'enabled'), 1)


        return True


def add_object_id(target, objectID, assetName):
    node = '%s_rsObjectId' % assetName
    mc.select(cl=True)
    if not mc.objExists(node):
        result = mm.eval('redshiftCreateObjectIdNode();')
        node = mc.rename(result, node)

    mc.sets(target, add=node)
    mc.setAttr('%s.objectId' % node, int(objectID))
    return node


def create_puzzle_matte(name, mode):
    puzzleNode = mm.eval('redshiftCreateAov("Puzzle Matte")')
    mc.setAttr('%s.name' % puzzleNode, name, type='string')
    mc.setAttr('%s.mode' % puzzleNode, mode)
    mm.eval('redshiftUpdateActiveAovList')
    print 'name {0}'.format(name)
    return mc.rename(puzzleNode, name)


def connect_redshift_attr():
    materials = list_mtr_node()

    for material in materials:
        shadingEngine = mc.listConnections(material, t='shadingEngine')
        mtrAttr = '%s.rsMaterialId' % material

        if shadingEngine and mc.objExists(mtrAttr):
            shdEngineAttr = '%s.rsMaterialId' % shadingEngine[0]
            if not mc.isConnected(mtrAttr, shdEngineAttr):
                mc.connectAttr(mtrAttr, shdEngineAttr, f=True)
