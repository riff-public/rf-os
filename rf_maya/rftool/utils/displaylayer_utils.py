import os
from collections import OrderedDict
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import pymel.core as pm
import maya.cmds as mc

from rf_utils import file_utils
from rf_utils.pipeline import shot_data
from rf_utils.context import context_info
reload(context_info.project_info)

def export_displaylayer(layers=[], path=None):
    # if no layer names provided, get all user-crated layers
    if not layers:
        layers = [l for l in mc.ls(type='displayLayer') if not l.startswith('defaultLayer')]
        if not layers:
            logger.error('No layer to export')
            return

    # get path to export
    if not path:
        try:
            scene = context_info.ContextPathInfo(path=str(pm.sceneName()))
            path = shot_data.get_workspace_data_path(entity=scene)
        except Exception as e:
            logger.error('Current scene path is invalid: %s' %pm.sceneName())
            print e
            return
    if not path:
        logger.error('Invalid file path: %s' %path)
        return

    if os.path.exists(path):
        data = file_utils.ymlLoader(path)
        if not 'displayLayer' in data.keys():
            data['displayLayer'] = OrderedDict()
    else:
        data = {'displayLayer':OrderedDict()}

    for lyr in layers:
        lyr = pm.nt.DisplayLayer(lyr)
        layer_data = {}
        layer_data['members'] = [m.shortName() for m in lyr.listMembers()]
        layer_data['visibility'] = lyr.visibility.get()
        layer_data['hideOnPlayback'] = lyr.hideOnPlayback.get()
        layer_data['displayType'] = lyr.displayType.get()
        data['displayLayer'][lyr.nodeName()] = layer_data
        
    # write YML
    data_dir = os.path.dirname(path)
    if not os.path.exists(data_dir):
        os.makedirs(data_dir)
    file_utils.ymlDumper(path, data)

    logger.info('Display layer data exported: %s' %path)
    return path

def import_displaylayer(path=None):
    # if no path provided, try to get from pipeline
    if not path:
        try:
            path = get_workspace_data_path()
        except:
            logger.error('Current scene path is invalid: %s' %pm.sceneName())
            return
    if not os.path.exists(path):
        logger.error('Data file does not exist: %s' %path)
        return

    # read the data from yml
    all_data = file_utils.ymlLoader(path)
    data = all_data['displayLayer']

    # get all existing layers
    existing_layers = mc.ls(type='displayLayer')
    new_layers = []
    for lyrName, lyrData in data.iteritems():
        if lyrName in existing_layers:
            lyr = pm.nt.DisplayLayer(lyrName)
            pm.delete(lyr)

        # create new layer
        lyr = pm.createDisplayLayer(name=lyrName)
        new_layers.append(lyr)

        # add members
        new_members = []
        for memName in lyrData['members']:
            try:
                mem = pm.PyNode(memName)
                new_members.append(mem)
            except Exception:
                logger.warning('Cannot add to layer: %s' %(memName))
        if new_members:
            lyr.addMembers(new_members)

        # set renderable
        lyr.visibility.set(lyrData['visibility'])
        lyr.hideOnPlayback.set(lyrData['hideOnPlayback'])
        try:
            pm.layerButton(lyr, e=True, layerHideOnPlayback=lyrData['hideOnPlayback'])
        except RuntimeError:
            pass
        lyr.displayType.set(lyrData['displayType'])

    logger.info('Display layer data imported: %s' %path)
    return new_layers

'''
from rf_maya.rftool.utils import displaylayer_utils as dlu
reload(dlu)

dlu.export_displaylayer(path = 'D:/__playground/test.yml')
dlu.import_displaylayer(path = 'D:/__playground/test.yml')
'''