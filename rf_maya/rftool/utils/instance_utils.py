import pymel.core as pm
import maya.OpenMaya as om
import maya.cmds as mc 

from rf_utils.context import context_info
# reload(context_info)
from rf_utils import custom_exception
from rf_maya.rftool.utils import maya_utils
# reload(maya_utils)

def get_instance_grp(entity):
    ''' find Instance_Grp '''

    if not entity:
        entity = context_info.ContextPathInfo(path=str(pm.sceneName()))
    instanceGrp = entity.projectInfo.asset.instance_grp() or 'Instance_Grp'
    
    # instanceGrp = pm.PyNode(instanceGrp)
    return instanceGrp

def export_instance_shotDress(path, entity=None):
    ''' Export function for MASH shotDress elements '''

    # find Instance_Grp
    instanceGrp = get_instance_grp(entity)
    if not pm.objExists(instanceGrp):
        raise custom_exception.PipelineError('Cannot find: %s' %instanceGrp)
    instanceGrp = pm.PyNode(instanceGrp)

    insts = []
    instChildren = instanceGrp.getChildren(type='transform')
    if not instChildren:
        raise custom_exception.PipelineError('Nothing under: %s' %instanceGrp)

    insts = [node for node in instChildren if node.nodeType() == 'instancer' \
            and node.instancerMessage.inputs(type='MASH_Waiter')]
    if not insts:
        raise custom_exception.PipelineError('No Instance node found!')

    old_states = []  # for collection the state of connection before disconnect
    # print insts
    for inst in insts:
        # get input connection to inputHierarchy channel of instancer node
        inputs = inst.inputHierarchy.inputs(c=True, p=True)
        if not inputs:
            om.MGlobal.displayWarning('Skipping, Instancer node has no inputHierarchy connection: %s' %inst.shortName())
            continue

        # add the custom attr for storing connection info
        if not inst.hasAttr('inputAssets'):
            pm.addAttr(inst, ln='inputAssets', dt='string', multi=True)
        
        for i, connections in enumerate(inputs):
            input_channel = connections[0]
            obj = connections[1].node()

            objShortName = obj.shortName()
            matrix = pm.xform(obj, q=True, m=True)
            if not obj.isReferenced():
                om.MGlobal.displayWarning('Skipping, Instanced object is not a referenced object: %s' %(objShortName))
                continue

            refPath = pm.referenceQuery(obj, f=True)
            geo = maya_utils.removeDagPathNamespace(objShortName)
            data = [refPath, geo, str(matrix)]
            dataStr = ';'.join(data)
            inst.inputAssets[i].set(dataStr)
            old_states.append(connections)

        # disconnect it so we don't include the assets with it
        inst.inputHierarchy.disconnect()

    # unparent referenced objects
    for child in instChildren:
        pm.parent(child, w=True)
    
    pm.select(insts, r=True)
    pm.exportSelected(path, f=True)
    om.MGlobal.displayInfo('Instance ShotDress exported to: %s' %path)

    # reparent ref top grps
    for child in instChildren:
        pm.parent(child, instanceGrp)

    # reconnect things back to original
    for des, src in old_states:
        pm.connectAttr(src, des, f=True)

    return path

def build_instance_shotDress(path, entity=None):
    ''' Build function for MASH shotDress elements '''

    # find Instance_Grp
    instanceGrp = get_instance_grp(entity)
    # instanceGrp = 'Instance_Grp'

    # get shotDess namespace
    shotName = entity.name_code
    # shotName = 's0010'
    shotDressNs = '%s_dressInst' %shotName

    # reference/reload the shotDress path
    if not path in [str(r.path) for r in pm.listReferences()]:
        shotDressRef = pm.createReference(path, namespace=shotDressNs)
    else:
        shotDressRef = pm.FileReference(path)
        shotDressRef.load()
        shotDressNs = shotDressRef.namespace

    if not pm.objExists(instanceGrp):
        instanceGrp = pm.group(em=True, n=instanceGrp)
    else:
        instanceGrp = pm.PyNode(instanceGrp)

    top_nodes = [n for n in shotDressRef.nodes() if isinstance(n, pm.nt.Transform) and not n.getParent()]
    if top_nodes:
        pm.parent(top_nodes, instanceGrp)

    # collect existing instance root asset
    existing_refs = []
    for ref in pm.listReferences():
        ns = ref.namespace
        if ns:
            nsSplits = ns.split('_')
            if len(nsSplits) > 1 and nsSplits[1].startswith('Inst'):
                existing_refs.append(ref)

    insts = [node for node in instanceGrp.getChildren(type='transform') if node.nodeType() == 'instancer' \
            and node.instancerMessage.inputs(type='MASH_Waiter')]
    
    used_refs = set()
    loaded_refs = set()
    for inst in insts:
        instNodeName = inst.nodeName()
        if not inst.hasAttr('inputAssets'):
            om.MGlobal.displayWarning('Skipping, Instancer does not have inputAssets attribute: %s' %(instNodeName))
            continue
        # get the path
        for ai in xrange(inst.inputAssets.numElements()):
            dataStr = inst.inputAssets[ai].get()
            if not dataStr:
                om.MGlobal.displayWarning('Skipping, Instancer has no data: %s.inputAssets[%s]' %(instNodeName, ai))
                continue

            data = dataStr.split(';')
            asset_path = data[0]
            geo = data[1]
            matrix = eval(data[2])

            asset = context_info.ContextPathInfo(path=asset_path)
            ns = '%s_Inst001' %asset.name
            
            # create new ref only if needed
            if asset_path not in existing_refs and asset_path not in used_refs:
                inst_ref = pm.createReference(asset_path, namespace=ns)
                # parent asset top node to instance grp
                asset_tops = [n for n in inst_ref.nodes() if isinstance(n, pm.nt.Transform) and not n.getParent()]
                pm.parent(asset_tops, instanceGrp)
            else:
                inst_ref = pm.FileReference(asset_path)
                if inst_ref not in loaded_refs:
                    om.MGlobal.displayInfo('Reloading: %s' %inst_ref)
                    inst_ref.load()
                    loaded_refs.add(inst_ref)
            used_refs.add(inst_ref)

            ns = inst_ref.namespace
            geo = maya_utils.addDagPathNamespace(geo, '%s:' %ns)
            if pm.objExists(geo):
                geo = pm.PyNode(geo)
                pm.connectAttr(geo.matrix, inst.inputHierarchy[ai], f=True)
                pm.xform(geo, m=matrix)
            else:
                om.MGlobal.displayError('Object does not exist: %s' %geo)

        om.MGlobal.displayInfo('Build done: %s' %instNodeName)

    # clean up unsed instance refs
    unused_refs = [r for r in existing_refs if r not in used_refs]
    # print unused_refs
    for unused_ref in unused_refs:
        om.MGlobal.displayInfo('Removing unused instance reference: %s' %unused_ref.path)
        unused_ref.remove()

def remove_instance_shotDress(entity=None):
    instanceGrp = get_instance_grp(entity)
    if not pm.objExists(instanceGrp):
        raise custom_exception.PipelineError('Cannot find: %s' %instanceGrp)

    instanceGrp = pm.PyNode(instanceGrp)
    refs = set()
    for child in instanceGrp.getChildren(ad=True):
        refPath = pm.referenceQuery(child, f=True)
        refs.add(pm.FileReference(refPath))
    for ref in refs:
        ref.remove()

    pm.delete(instanceGrp)
    om.MGlobal.displayInfo('Instance ShotDress removed.')


def get_instance_dependency(entity=None): 
    instanceGrp = get_instance_grp(entity)
    childs = mc.listRelatives(str(instanceGrp), c=True, pa=True)
    refs = list(set([mc.referenceQuery(a, f=True) for a in childs if mc.referenceQuery(a, inr=True)]))
    return refs

'''  
path = 'D:/__playground/mash_node1.ma'
export_instance_shotDress(path)
# build_instance_shotDress(path)
'''
