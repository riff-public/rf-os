import os
from collections import OrderedDict
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import pymel.core as pm
import maya.cmds as mc

from rf_utils import file_utils
from rf_utils.pipeline import shot_data
from rf_utils.context import context_info
reload(context_info.project_info)


def export_renderlayer_adjustments(layers=[], path=None):
    # if no layer names provided, get all user-crated layers
    if not layers:
        layers = [l for l in mc.ls(type='renderLayer') if not l.startswith('defaultRenderLayer')]
        if not layers:
            logger.error('No layer to export')
            return

    # get path to export
    if not path:
        try:
            scene = context_info.ContextPathInfo(path=str(pm.sceneName()))
            path = shot_data.get_workspace_data_path(entity=scene)
        except Exception as e:
            logger.error('Current scene path is invalid: %s' %pm.sceneName())
            print e
            return
    if not path:
        logger.error('Invalid file path: %s' %path)
        return

    if os.path.exists(path):
        data = file_utils.ymlLoader(path)
        if not 'renderLayer' in data.keys():
            data['renderLayer'] = OrderedDict()
    else:
        data = {'renderLayer':OrderedDict()}

    for lyr in layers:
        lyr = pm.nt.RenderLayer(lyr)
        layer_data = {'members':[], 'adjustments':[]}
        layer_data['members'] = [m.shortName() for m in lyr.listMembers()]

        adjustments = lyr.listAdjustments()
        adjustmentStrs = []
        for adj in adjustments:
            index = lyr.adjustmentPlug(adj).getParent().index()
            value = lyr.adjustments[index].plug.get()
            if isinstance(value, (pm.dt.Vector)):
                value = [value.x, value.y, value.z]
            adjustmentStrs.append((adj.name(), value))

        layer_data['adjustments'] = adjustmentStrs
        layer_data['renderable'] = lyr.renderable.get()
        data['renderLayer'][lyr.nodeName()] = layer_data

    # write YML
    data_dir = os.path.dirname(path)
    if not os.path.exists(data_dir):
        os.makedirs(data_dir)
    file_utils.ymlDumper(path, data)

    logger.info('Render layer data exported: %s' %path)
    return path

def import_renderlayer_adjustments(path=None):
    # if no path provided, try to get from pipeline
    if not path:
        try:
            path = get_workspace_data_path()
        except:
            logger.error('Current scene path is invalid: %s' %pm.sceneName())
            return
    if not os.path.exists(path):
        logger.error('Data file does not exist: %s' %path)
        return

    # read the data from yml
    all_data = file_utils.ymlLoader(path)
    data = all_data['renderLayer']

    # get all existing layers
    existing_layers = mc.ls(type='renderLayer')

    # switch to masterLayer
    defaultRenderLayer = pm.PyNode('defaultRenderLayer')
    defaultRenderLayer.setCurrent()

    new_layers = []
    for lyrName, lyrData in data.iteritems():
        if lyrName in existing_layers:
            lyr = pm.nt.RenderLayer(lyrName)
            pm.delete(lyr)

        # create new layer
        lyr = pm.createRenderLayer(name=lyrName)
        new_layers.append(lyr)

        # add members
        new_members = []
        for memName in lyrData['members']:
            try:
                mem = pm.PyNode(memName)
                new_members.append(mem)
            except Exception:
                logger.warning('Cannot add to layer: %s' %(memName))
        if new_members:
            lyr.addMembers(new_members)

        # add adjustments
        i = 0
        for plug, value in lyrData['adjustments']:
            try:
                lyr.addAdjustments(plug)
                lyr.adjustments[i].value.set(value)
            except Exception:
                logger.warning('Cannot add adjustment: %s, %s' %(plug, value))
            i += 1

        # set renderable
        lyr.renderable.set(lyrData['renderable'])

    logger.info('Render layer data imported: %s' %path)
    return new_layers