import time
import logging
import operator
from pprint import pprint
from collections import OrderedDict

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import maya.cmds as mc
import maya.OpenMaya as om
import maya.OpenMayaUI as omUI
import pymel.core as pm

from rf_maya.rftool.utils import maya_utils
reload(maya_utils)

class plane(object):
    def __init__(self, normalisedVector):
        self.vector = normalisedVector
        self.distance = 0.0

    def relativeToPlane(self, point):
        # Converting the point as a vector from the origin to its position
        pointVec = om.MVector(point.x, point.y, point.z)
        val = (self.vector * pointVec) + self.distance

        if val >= 0.0:
            return True
        else:
            return False

class frustum(object):
    def __init__(self, cameraName):
        # Initialising selected transforms into its associated dagPaths
        selectionList = om.MSelectionList()
        objDagPath = om.MDagPath()
        selectionList.add(cameraName)
        selectionList.getDagPath(0, objDagPath)
        self.camera = om.MFnCamera(objDagPath)

        self.planes = []
        self.nearClip = self.camera.nearClippingPlane()
        self.farClip = self.camera.farClippingPlane()
        self.aspectRatio = self.camera.aspectRatio()

        left_util = om.MScriptUtil()
        left_util.createFromDouble(0.0)
        ptr0 = left_util.asDoublePtr()

        right_util = om.MScriptUtil()
        right_util.createFromDouble(0.0)
        ptr1 = right_util.asDoublePtr()

        bot_util = om.MScriptUtil()
        bot_util.createFromDouble(0.0)
        ptr2 = bot_util.asDoublePtr()

        top_util = om.MScriptUtil()
        top_util.createFromDouble(0.0)
        ptr3 = top_util.asDoublePtr()

        stat = self.camera.getViewingFrustum(self.aspectRatio, ptr0, ptr1, ptr2, ptr3, False, True)

        left = left_util.getDoubleArrayItem(ptr0, 0)
        right = right_util.getDoubleArrayItem(ptr1, 0)
        bottom = bot_util.getDoubleArrayItem(ptr2, 0)
        top = top_util.getDoubleArrayItem(ptr3, 0)

        #  planeA = right plane
        a = om.MVector(right, top, -self.nearClip)
        b = om.MVector(right, bottom, -self.nearClip)
        c = (a ^ b).normal()  # normal of plane = cross product of vectors a and b
        planeA = plane(c)
        self.planes.append(planeA)

        #  planeB = left plane
        a = om.MVector(left, bottom, -self.nearClip)
        b = om.MVector(left, top, -self.nearClip)
        c = (a ^ b).normal()
        planeB = plane(c)
        self.planes.append(planeB)

        # planeC = bottom plane
        a = om.MVector(right, bottom, -self.nearClip)
        b = om.MVector(left, bottom, -self.nearClip)
        c = (a ^ b).normal()
        planeC = plane(c)
        self.planes.append(planeC)

        # planeD = top plane
        a = om.MVector(left, top, -self.nearClip)
        b = om.MVector(right, top, -self.nearClip)
        c = (a ^ b).normal()
        planeD = plane(c)
        self.planes.append(planeD)

        # planeE = far plane
        c = om.MVector(0, 0, 1)
        planeE = plane(c)
        planeE.distance = self.farClip
        self.planes.append(planeE)

        # planeF = near plane
        c = om.MVector(0, 0, -1)
        planeF = plane(c)
        planeF.distance = self.nearClip
        self.planes.append(planeF)

    def relativeToFrustum(self, pointsArray):
        numPoints = len(pointsArray)
        for j in xrange(6):
            numBehindThisPlane = len(
                [i for i in xrange(numPoints) if not self.planes[j].relativeToPlane(pointsArray[i])])

            # all points were behind one plane, just return False. No need to look further
            if numBehindThisPlane == numPoints:
                return False 

        return True

def getNodeVisible(node):
    if mc.getAttr('%s.visibility' %node) == False:
        return False

    if node.startswith('|'):
        node = node[1:]
    parentPath = mc.listRelatives(node, f=True, allParents=True)[0]

    splits = parentPath.split('|')
    s = len(splits) + 1
    for p in splits:
        parent = '|'.join(splits[:s])
        if mc.getAttr('%s.visibility' %parent) == False:
            return False
        s -= 1

    return True

def anyObjectSeen(nodeMSel, cameraShape):
    points = []
    # get cam dag path
    camMSel = om.MSelectionList()
    camDagPath = om.MDagPath()
    camMSel.add(mc.listRelatives(cameraShape, parent=True)[0])
    camMSel.getDagPath(0, camDagPath)
    camInvWorldMtx = camDagPath.inclusiveMatrixInverse()
    cam_frustum = frustum(cameraShape)

    while not nodeMSel.isDone():
        objDagPath = om.MDagPath()
        nodeMSel.getDagPath(objDagPath)
        fp = objDagPath.fullPathName()
        visible = getNodeVisible(fp)
        
        if visible:
            fnDag = om.MFnDagNode(objDagPath)
            dWorldMtx = objDagPath.exclusiveMatrix()
            bbox = fnDag.boundingBox()

            minx = bbox.min().x
            miny = bbox.min().y
            minz = bbox.min().z
            maxx = bbox.max().x
            maxy = bbox.max().y
            maxz = bbox.max().z

            # Getting points relative to the cameras transmformation matrix
            points.append(bbox.min() * dWorldMtx * camInvWorldMtx)
            points.append(om.MPoint(maxx, miny, minz) * dWorldMtx * camInvWorldMtx)
            points.append(om.MPoint(maxx, miny, maxz) * dWorldMtx * camInvWorldMtx)
            points.append(om.MPoint(minx, miny, maxz) * dWorldMtx * camInvWorldMtx)
            points.append(om.MPoint(minx, maxy, minz) * dWorldMtx * camInvWorldMtx)
            points.append(om.MPoint(maxx, maxy, minz) * dWorldMtx * camInvWorldMtx)
            points.append(bbox.max() * dWorldMtx * camInvWorldMtx)
            points.append(om.MPoint(minx, maxy, maxz) * dWorldMtx * camInvWorldMtx)

            relation = cam_frustum.relativeToFrustum(points)
            # if any of the object in objects is seen inisde the camera, return True
            if relation:
                return True

        nodeMSel.next()

    return False

def objectSeen(nodeMSel, frustum, camInvWorldMtx):
    seens = set()
    while not nodeMSel.isDone():
        points = []
        objDagPath = om.MDagPath()
        nodeMSel.getDagPath(objDagPath)

        fp = objDagPath.fullPathName()
        visible = getNodeVisible(fp)
        if not visible:
            nodeMSel.next()

        fnDag = om.MFnDagNode(objDagPath)
        obj = objDagPath.node()

        dWorldMtx = objDagPath.exclusiveMatrix()
        bbox = fnDag.boundingBox()
        # bbox.transformUsing(incMatrix)

        minx = bbox.min().x
        miny = bbox.min().y
        minz = bbox.min().z
        maxx = bbox.max().x
        maxy = bbox.max().y
        maxz = bbox.max().z

        # Getting points relative to the cameras transmformation matrix
        points.append(bbox.min() * dWorldMtx * camInvWorldMtx)
        points.append(om.MPoint(maxx, miny, minz) * dWorldMtx * camInvWorldMtx)
        points.append(om.MPoint(maxx, miny, maxz) * dWorldMtx * camInvWorldMtx)
        points.append(om.MPoint(minx, miny, maxz) * dWorldMtx * camInvWorldMtx)
        points.append(om.MPoint(minx, maxy, minz) * dWorldMtx * camInvWorldMtx)
        points.append(om.MPoint(maxx, maxy, minz) * dWorldMtx * camInvWorldMtx)
        points.append(bbox.max() * dWorldMtx * camInvWorldMtx)
        points.append(om.MPoint(minx, maxy, maxz) * dWorldMtx * camInvWorldMtx)

        relation = frustum.relativeToFrustum(points)

        # if any of the object in objects is seen inisde the camera, return True
        if relation == True:
            seens.add(objDagPath.partialPathName())

        nodeMSel.next()

    return seens

def waitForAllGpuLoad(timeout=30.0):
    allGpuShapes = pm.ls(type='gpuCache')
    if not allGpuShapes:
        return

    gpuTrs = [g.getParent() for g in allGpuShapes]
    startTime = time.time()
    exit = False
    while not exit:
        if time.time() - startTime >= timeout:
            break

        loading = []
        for tr in gpuTrs:
            bb = tr.getBoundingBox()
            if bb.width() == 0.0 and bb.height() == 0.0 and bb.depth() == 0.0:
                loading.append(tr)

        if loading:
            # print 'waiting for %s' %loading
            gpuTrs = loading
        else:
            exit = True

    logger.info('All GPU loaded')

def getUnseenReferenceNodes(cameraName, grpName, refs=[], timeRange=[], step=5, viewport='vp2'):
    logger.debug('time range : {0} ( step +{1} ), getting unused list...'.format(timeRange, step))

    if not timeRange:
        startTime = int(round(float(pm.playbackOptions(q=True, min=True))))
        endTime = int(round(float(pm.playbackOptions(q=True, max=True))))
    else:
        startTime = int(round(float(timeRange[0])))
        endTime = int(round(float(timeRange[1])))
    # add step to the end time
    endTime = endTime + (endTime % step)

    references = {}
    if not refs:
        # get all refs
        references = pm.getReferences()
    else:
        for ref in refs:
            try:
                ref = pm.FileReference(ref)
                references[ref.namespace] = ref
            except:
                raise Exception, 'Error getting reference node info: %s' %ref

    assets = {}  # {refNode, [geos]}
    
    for ns, ref in references.iteritems():
        geoGrpName = '%s:%s' % (ns, grpName)
        if not pm.objExists(geoGrpName):
            continue

        # check if name is not unique, pick the first one
        geoGrpName = pm.ls(geoGrpName)[0]

        geoGrp = pm.PyNode(geoGrpName)
        if not isinstance(geoGrp, pm.nt.Transform):
            continue

        geos = [node.getShape(ni=True) for node in geoGrp.getChildren(ad=True, type='transform') if
                node.getShape(ni=True) and isinstance(node.getShape(ni=True),
                                                      (pm.nt.Mesh, pm.nt.NurbsSurface))]
        if not geos:
            continue
        nodeMSel = om.MSelectionList()
        for geo in geos:
            nodeMSel.add(geo.longName())
        assets[ref.refNode] = om.MItSelectionList(nodeMSel)

    if assets:
        # pm.refresh(suspend=True)
        with maya_utils.FreezeViewport(mode=viewport) as freeze:
            pm.currentTime(startTime)
            seens = []

            # get the camera shape
            cameraShapes = mc.listRelatives(cameraName, s=True, f=True)
            if cameraShapes and mc.nodeType(cameraShapes[0]) == 'camera':
                cameraShape = cameraShapes[0]
            else:
                raise Exception, 'Invalid camera: %s' %cameraName


            pm.waitCursor(st=True)
            frange = range(startTime, endTime+1, step)
            if frange[-1] != endTime:
                frange.append(endTime)
            for i in frange:
                # remove what has been seen
                logger.debug('Evaluating frame: %s' %i)

                if seens:
                    for s in seens:
                        del (assets[s])

                seens = []
                if assets:

                    for refNode, geoMItSel in assets.iteritems():
                        # query the camera inclusive matrix inverse
                        
                        # get the current frustum
                        # cam_frustum = frustum(cameraShape)
                        geoMItSel.reset()
                        result = anyObjectSeen(geoMItSel, cameraShape)

                        if result:
                            seens.append(refNode)
                else:
                    break

                # go to next frame
                pm.currentTime(i)
            pm.waitCursor(st=False)
            
        # pm.refresh(suspend=False)
        # pm.refresh(f=True)

    result = [refNode.nodeName() for refNode in assets]
    return result

def getSeenObjects(cameraName, timeRange=[], step=5, viewport='vp2', typ='mesh'):
    logger.debug('time range : {0} ( step +{1} ), getting unused list...'.format(timeRange, step))

    typDict = {'mesh':om.MFn.kMesh,'gpuCache':om.MFn.kPluginShape}

    if not timeRange:
        startTime = int(round(float(pm.playbackOptions(q=True, min=True))))
        endTime = int(round(float(pm.playbackOptions(q=True, max=True))))
    else:
        startTime = int(round(float(timeRange[0])))
        endTime = int(round(float(timeRange[1])))

    if not typ in typDict:
        raise Exception, 'Invalid type: %s' %typ

    dagIt = om.MItDag(om.MItDag.kDepthFirst, typDict[typ])
    mSels = om.MSelectionList()
    while not dagIt.isDone():
        mDag = om.MDagPath()
        dagIt.getPath(mDag)
        mSels.add(mDag)
        dagIt.next()
    geoMItSel = om.MItSelectionList(mSels)

    # pm.refresh(suspend=True)
    with maya_utils.FreezeViewport(mode=viewport) as freeze:
        pm.currentTime(startTime)
        seens = set()

        # get the camera shape
        cameraShapes = mc.listRelatives(cameraName, s=True, f=True)
        if cameraShapes and mc.nodeType(cameraShapes[0]) == 'camera':
            cameraShape = cameraShapes[0]
        else:
            raise Exception, 'Invalid camera: %s' %cameraName

        # get cam dag path
        camMSel = om.MSelectionList()
        camDagPath = om.MDagPath()
        camMSel.add(cameraName)
        camMSel.getDagPath(0, camDagPath)

        pm.waitCursor(st=True)

        frange = range(startTime, endTime+1, step)
        if frange[-1] != endTime:
            frange.append(endTime)
        for i in frange:
            # remove what has been seen
            logger.debug('Evaluating frame: %s' %i)

            # query the camera inclusive matrix inverse
            camInvWorldMtx = camDagPath.inclusiveMatrixInverse()
            # get the current frustum
            cam_frustum = frustum(cameraShape)
            geoMItSel.reset()
            result = objectSeen(geoMItSel, cam_frustum, camInvWorldMtx)
            # print result
            if result:
                seens = seens.union(result)

            # go to next frame
            pm.currentTime(i)
        pm.waitCursor(st=False)
            
        # pm.refresh(suspend=False)
        # pm.refresh(f=True)

    return seens

def removeUnseenReferences(shot_node, step):
    cam = pm.shot(shot_node, q=True, currentCamera=True)
    frame_start_end = [pm.shot(shot_node, q=True, st=True), pm.shot(shot_node, q=True, et=True)]
    unused_refs = getUnseenReferenceNodes(cameraName=cam, grpName='Geo_Grp', 
                timeRange=frame_start_end, step=step)

    if not unused_refs:
        return

    for refNode in unused_refs:
        try:
            ref = pm.FileReference(refNode)
            ref.remove()

        except:
            if pm.objExists(refNode):
                logger.error('Unable to remove: %s' %refNode)
            continue


def getUnseenReferenceNodes_selection(cameraName, refs=[], timeRange=[], step=5, returnRefNode=True):
    allReferences = mc.file(q=True, r=True) if not refs else refs
    allRnNodes = [mc.referenceQuery(a, referenceNode=True) for a in allReferences]
    
    # viewport objects 
    mc.lookThru(cameraName)
    # timeRange = timeRange if timeRange else [mc.playbackOptions(q=True, min=True), mc.playbackOptions(q=True, max=True)]
    if not timeRange:
        startTime = int(round(float(pm.playbackOptions(q=True, min=True))))
        endTime = int(round(float(pm.playbackOptions(q=True, max=True))))
    else:
        startTime = int(round(float(timeRange[0])))
        endTime = int(round(float(timeRange[1])))
    # add step to the end time
    endTime = endTime + (endTime % step)

    # create new win
    ds = pm.PyNode('defaultResolution')
    w = ds.width.get() * 0.5
    h = ds.height.get() * 0.5
    win = mc.window()
    pl = mc.paneLayout(cn='single', w=w+4, h=h+42)
    panel = mc.modelPanel(camera=cameraName)
    mc.showWindow(win)
    mc.modelEditor(panel, e=True, displayAppearance="wireframe")
    maya_utils.viewport_display(alo=0, polymeshes=1) 
    mc.setFocus(panel)  # set the focus to this split window

    old_overscan = mc.getAttr('%s.overscan' %cameraName)
    old_resGate = mc.getAttr('%s.displayResolution' %cameraName)
    try: 
        mc.setAttr('%s.overscan' %cameraName, 1.0)
    except RuntimeError as e: 
        logger.warning(e)
        logger.error('Cannot set overscan because it is locked')

    mc.setAttr('%s.displayResolution' %cameraName, 1.0)

    objs = set()
    pm.waitCursor(st=True)
    frange = range(startTime, endTime+1, step)
    if frange[-1] != endTime:
        frange.append(endTime)
    for i in frange: 
        mc.currentTime(i)
        maya_utils.select_viewport_objects_replace()
        sels = mc.ls(sl=True)
        objs = objs.union(set(sels))

    # cleanup to old values
    try: 
        mc.setAttr('%s.overscan' %cameraName, old_overscan)
    except RuntimeError as e: 
        logger.warning(e)
        logger.error('Cannot restore overscan because it is locked')
    mc.setAttr('%s.displayResolution' %cameraName, old_resGate)
    mc.deleteUI(win)
    pm.waitCursor(st=False)

    seenRnNodes = list(set([mc.referenceQuery(a, referenceNode=True) for a in objs if mc.referenceQuery(a, inr=True)]))
    unseenNodes = [a for a in allRnNodes if a not in seenRnNodes]

    if returnRefNode:
        # print unseenNodes
        return unseenNodes
    else:
        return list(objs)# 
        

def selectUnseenReferenceGrp(cameraName, grpName, refs=[], timeRange=[], step=5, viewport='vp2', mode='calculation'): 
    """ mode selection or calculation """
    objs = []
    if mode == 'calculation': 
        rnNodes = getUnseenReferenceNodes(cameraName, grpName, refs=refs, timeRange=timeRange, step=step, viewport=viewport)

    elif mode == 'selection': 
        rnNodes = getUnseenReferenceNodes_selection(cameraName, refs=refs, timeRange=timeRange, step=step)

    print 'Unseen Nodes', rnNodes
    for rnNode in rnNodes: 
        ns = mc.referenceQuery(rnNode, ns=True)
        objs += mc.ls('%s:%s' % (ns[1:], grpName))

    mc.select(objs)
    return rnNodes

def getCenterOfPoints(points):
    x, y, z = [], [], []
    for p in points:
        x.append(p[0])
        y.append(p[1])
        z.append(p[2])

    mix, miy, miz = min(x), min(y), min(z)
    mxx, mxy, mxz = max(x), max(y), max(z)
    position = [((mxx-mix)*0.5)+mix, ((mxy-miy)*0.5)+miy, ((mxz-miz)*0.5)+miz]
    return position

def get_asset_distance_from_cam(camera, geoGrpName, frameRange=[], step=5, onlyInCamera=True, printResult=True):
    if not frameRange:
        startTime = int(round(float(pm.playbackOptions(q=True, min=True))))
        endTime = int(round(float(pm.playbackOptions(q=True, max=True))))
    else:
        startTime = int(round(float(frameRange[0])))
        endTime = int(round(float(frameRange[1])))
    endTime = endTime + (endTime % step)

    
    camShp = camera.getShape()
    dist_dict = {}
    original_time = pm.currentTime(q=True)

    # get all meshes from refs
    ref_meshes = []
    for ref in pm.listReferences():
        namespace = ref.namespace
        if not namespace:
            om.MGlobal.displayWarning('Skipping %s: has no namespace' %(ref.refNode))
            continue

        namespace += ':'
        geoGrpNs = '%s%s' %(namespace, geoGrpName)
        if not pm.objExists(geoGrpNs) or len(pm.ls(geoGrpNs)) != 1:
            om.MGlobal.displayWarning('Skipping %s: cannot find %s' %(ref.refNode, geoGrpNs))
            continue

        geoGrp = pm.PyNode(geoGrpNs)
        allMeshes = []
        children = mc.listRelatives(geoGrp.shortName(), pa=True, ad=True, type='mesh')
        if children:
            mSels = om.MSelectionList()
            for g in children:
                if mc.polyEvaluate(g, f=True) and not mc.getAttr('%s.intermediateObject' %g):
                    allMeshes.append(pm.PyNode(g))
                    mSels.add(g)
            if allMeshes:
                geoMItSel = om.MItSelectionList(mSels)
                ref_meshes.append((ref, allMeshes, geoMItSel))

    with maya_utils.FreezeViewport(mode='vp2') as freeze:
        frange = range(startTime, endTime+1, step)
        if frange[-1] != endTime:
            frange.append(endTime)
        for frame in frange:
            pm.currentTime(frame)
            camPos = camera.getTranslation(space='world')
            for ref, allMeshes, geoMItSel in ref_meshes:
                if onlyInCamera:
                    geoMItSel.reset()
                    objectInCam = anyObjectSeen(geoMItSel, camShp.shortName())
                    if not objectInCam:
                        continue

                gbb_min, gbb_max = None, None
                for mesh in allMeshes:
                    bb = mesh.getParent().getBoundingBox(space='world')
                    bb_min = bb.min()
                    bb_max = bb.max()
                    if not gbb_min:
                        gbb_min = bb_min
                        gbb_max = bb_max
                    else:
                        if bb_min.x < gbb_min.x:
                            gbb_min.x = bb_min.x
                        if bb_min.y < gbb_min.y:
                            gbb_min.y = bb_min.y
                        if bb_min.y < gbb_min.y:
                            gbb_min.y = bb_min.y

                        if bb_max.x > gbb_max.x:
                            gbb_max.x = bb_max.x
                        if bb_max.y > gbb_max.y:
                            gbb_max.y = bb_max.y
                        if bb_max.z > gbb_max.z:
                            gbb_max.z = bb_max.z

                points = [pm.dt.Point(gbb_min.x, gbb_min.y, gbb_min.z),
                        pm.dt.Point(gbb_max.x, gbb_min.y, gbb_min.z), 
                        pm.dt.Point(gbb_max.x, gbb_min.y, gbb_max.z), 
                        pm.dt.Point(gbb_min.x, gbb_min.y, gbb_max.z), 
                        pm.dt.Point(gbb_min.x, gbb_max.y, gbb_min.z), 
                        pm.dt.Point(gbb_max.x, gbb_max.y, gbb_min.z), 
                        pm.dt.Point(gbb_max.x, gbb_max.y, gbb_max.z), 
                        pm.dt.Point(gbb_min.x, gbb_max.y, gbb_max.z)]

                point_dists = []
                for pt in points:
                    dist = pt - camPos
                    dist = dist.length()
                    point_dists.append(dist)

                min_dist = round(min(point_dists), 4)

                if ref not in dist_dict:
                    dist_dict[ref] = min_dist
                else:
                    if min_dist < dist_dict[ref]:
                        dist_dict[ref] = min_dist

    pm.currentTime(original_time)
    sorted_dists = sorted(dist_dict.items(), key=lambda x: x[1])
    result = OrderedDict()
    resTexts = []
    for ref, dist in sorted_dists:
        result[ref] = dist
        ns = ref.namespace
        resTexts.append('{:<15s}{:<15s}'.format(ns, str(dist)))

    if printResult and resTexts:
        print '======= Asset distances ======='
        print '\n'.join(resTexts)
        print '==============================='
    
    return result

def select_assets_by_distance(distance=None, withinDistance=True, camera=None, geoGrpName='Geo_Grp'):
    if not camera:
        sels = [s for s in pm.selected(type='transform') if s.getShape(type='camera')]
        if sels:
            camera = sels[0]
        else:
            sels = pm.selected(type='camera')
            if sels:
                camera = sels[0].getParent()
    elif isinstance(camera, (str, unicode)):
        camera = pm.PyNode(camera)
    if not camera:
        om.MGlobal.displayError('Select a camera!')
        return

    if not distance:
        result = pm.promptDialog(title='Select asset', message='Within distance:', text=10000, 
            button=('OK', 'Cancle'))
        if result == 'Cancle':
            return

    user_input = pm.promptDialog(q=True, text=True)
    try:
        distance = int(user_input)
    except ValueError:
        om.MGlobal.displayError('Invalid distance input: %s' %(user_input))
        return

    asset_distances = get_asset_distance_from_cam(camera, geoGrpName, onlyInCamera=True)
    
    result = []
    for ref, dist in asset_distances.iteritems():
        ns = ref.namespace
        if withinDistance:
            if dist <= distance:
                result.append(ns)
        else:
            if dist >= distance:
                result.append(ns)

    toSel = []
    for ns in result:
        obj = pm.PyNode('%s:%s' %(ns, geoGrpName))
        toSel.append(obj)
    
    pm.select(toSel, r=True)



'''
import time
from rf_maya.rftool.utils import objectsInCameraView as oic
reload(oic)

refs = [u'P:/SevenChickMovie/scene/publ/act1/scm_act1_q0080_s0100/output/warriorA_006/scm_act1_q0080_s0100_anim_warriorA_006.hero.abc',
 u'P:/SevenChickMovie/scene/publ/act1/scm_act1_q0080_s0100/output/warriorA_005/scm_act1_q0080_s0100_anim_warriorA_005.hero.abc',
 u'P:/SevenChickMovie/scene/publ/act1/scm_act1_q0080_s0100/output/canis_001/scm_act1_q0080_s0100_anim_canis_001.hero.abc',
 u'P:/SevenChickMovie/scene/publ/act1/scm_act1_q0080_s0100/output/badger_001/scm_act1_q0080_s0100_anim_badger_001.hero.abc',
 u'P:/SevenChickMovie/scene/publ/act1/scm_act1_q0080_s0100/output/warriorA_004/scm_act1_q0080_s0100_anim_warriorA_004.hero.abc',
 u'P:/SevenChickMovie/scene/publ/act1/scm_act1_q0080_s0100/output/elephantAdept_001/scm_act1_q0080_s0100_anim_elephantAdept_001.hero.abc',
 u'P:/SevenChickMovie/scene/publ/act1/scm_act1_q0080_s0100/output/warriorA_003/scm_act1_q0080_s0100_anim_warriorA_003.hero.abc',
 u'P:/SevenChickMovie/asset/publ/char/warriorA/hero/warriorA_rig_main_md.hero.mb',
 u'P:/SevenChickMovie/scene/publ/act1/scm_act1_q0080_s0100/output/warriorA_001/scm_act1_q0080_s0100_anim_warriorA_001.hero.abc',
 u'P:/SevenChickMovie/scene/publ/act1/scm_act1_q0080_s0100/output/owlAdept_001/scm_act1_q0080_s0100_anim_owlAdept_001.hero.abc',
 u'P:/SevenChickMovie/scene/publ/act1/scm_act1_q0080_s0100/output/masterToad_001/scm_act1_q0080_s0100_anim_masterToad_001.hero.abc',
 u'P:/SevenChickMovie/scene/publ/act1/scm_act1_q0080_s0100/output/hippoAdept_001/scm_act1_q0080_s0100_anim_hippoAdept_001.hero.abc',
 u'P:/SevenChickMovie/asset/publ/char/bat/hero/bat_rig_main_md.hero.ma',
 u'P:/SevenChickMovie/scene/publ/act1/scm_act1_q0080_s0100/output/sackBig_001/scm_act1_q0080_s0100_anim_sackBig_001.hero.abc'] 

st = time.time()
res = oic.unseenReferenceNodes(cameraName='s0100_cam:camshot', 
    grpName='Geo_Grp', 
    refs=refs, 
    timeRange=[], 
    step=5, 
    viewport='vp2', 
    mode='calculation')
print time.time() - st
print res
    
'''