import maya.cmds as mc
def spec_step(file_spec):
    ramp_node = mc.shadingNode('ramp',asUtility=True, n='RemapRamp')
    mc.setAttr('%s.colorEntryList[0].position'%ramp_node, 0.00)
    mc.setAttr('%s.colorEntryList[1].position'%ramp_node, 0.05)
    mc.setAttr('%s.colorEntryList[0].color'%ramp_node, 0,0,0, type='double3')
    mc.setAttr('%s.colorEntryList[1].color'%ramp_node, 1,1,1, type='double3')
    rgb_to_Hsv = mc.shadingNode('rgbToHsv',asUtility=True, n='RemapRgbToHsv')
    mc.connectAttr('%s.outColor'%file_spec, '%s.inRgb'%rgb_to_Hsv )
    mc.connectAttr('%s.outHsvH'%rgb_to_Hsv, '%s.uCoord'%ramp_node )
    mc.connectAttr('%s.outHsvV'%rgb_to_Hsv, '%s.vCoord'%ramp_node )
    return ramp_node

def bump_step(bump_node):
    bump_list = mc.listConnections(bump_node, type='RedshiftBumpMap')[0]
    file_bump = mc.listConnections(bump_node, type='file')[0]
    mc.disconnectAttr('%s.outColor'%file_bump, '%s.input'%bump_node)
    place2d = mc.shadingNode('place2dTexture',asUtility=True)
    noise_node = noise()
    mc.connectAttr('%s.outUV'%place2d, '%s.uvCoord'%noise_node)
    mc.connectAttr('%s.outUvFilterSize'%place2d,'%s.uvFilterSize'%noise_node)
    mc.connectAttr('%s.outColor'%(noise_node),'%s.input'%(bump_node))

def bump_(normal_node):
    blend_bump = mc.shadingNode('RedshiftBumpBlender',asUtility=True)
    mc.setAttr('%s.additive'%blend_bump, 1)
    mc.setAttr('%s.bumpWeight0'%blend_bump, 1)
    bump = mc.shadingNode('RedshiftBumpMap',asUtility=True)
    place2d = mc.shadingNode('place2dTexture',asUtility=True)
    noise_node = noise()
    mc.connectAttr('%s.outUV'%place2d, '%s.uvCoord'%noise_node)
    mc.connectAttr('%s.outUvFilterSize'%place2d,'%s.uvFilterSize'%noise_node)
    mc.connectAttr('%s.outColor'%(noise_node),'%s.input'%(bump))
    
    mc.connectAttr('%s.outDisplacementVector'%normal_node, '%s.baseInput'%blend_bump)
    mc.connectAttr('%s.out'%bump, '%s.bumpInput0'%blend_bump)
    
    return blend_bump
    
def noise():
    noise_node = mc.shadingNode('noise',asUtility=True)
    mc.setAttr("%s.noiseType"%noise_node,0)
    mc.setAttr("%s.ratio"%noise_node,0.95)
    mc.setAttr("%s.frequencyRatio"%noise_node,8)
    return noise_node

def bump_2_normal(normal_node):
    from rf_utils.context import context_info
    import os
    blend_bump = mc.shadingNode('RedshiftBumpBlender',asUtility=True)
    mc.setAttr('%s.additive'%blend_bump, 1)
    mc.setAttr('%s.bumpWeight0'%blend_bump, 1)
    normal_node_2 = mc.shadingNode('RedshiftNormalMap',asUtility=True)
    
    asset = context_info.ContextPathInfo()
    path_texture = 'P:/SevenChickMovie/asset/work/prop/%s/lookdev/main/image'%asset.name
    if not os.path.exists(path_texture):
        return [False, path_texture]
    print path_texture
    list_file = os.listdir(path_texture)
    for file in list_file:
        if 'AddOn_normal' in file:
            tex_path = os.path.join(path_texture, file)
    mc.setAttr('%s.tex0'%normal_node_2, tex_path,type='string')
    mc.connectAttr('%s.outDisplacementVector'%normal_node_2, '%s.baseInput'%blend_bump)
    mc.connectAttr('%s.outDisplacementVector'%normal_node, '%s.bumpInput0'%blend_bump)
    
    return [True, blend_bump]

def refl_rough(roughness_file):
    ramp_node = mc.shadingNode('ramp',asUtility=True, n='RemapRamp')
    mc.setAttr('%s.colorEntryList[0].position'%ramp_node, 0.90)
    mc.setAttr('%s.colorEntryList[1].position'%ramp_node, 1)
    mc.setAttr('%s.colorEntryList[0].color'%ramp_node, .1,.1,.1, type='double3')
    mc.setAttr('%s.colorEntryList[1].color'%ramp_node, .442,.45,.45, type='double3')
    rgb_to_Hsv = mc.shadingNode('rgbToHsv',asUtility=True, n='RemapRgbToHsv')
    mc.connectAttr('%s.outHsvH'%rgb_to_Hsv, '%s.uCoord'%ramp_node )
    mc.connectAttr('%s.outHsvV'%rgb_to_Hsv, '%s.vCoord'%ramp_node )
    mc.connectAttr('%s.outColor'%roughness_file, '%s.inRgb'%rgb_to_Hsv)
    
    noise_node = mc.shadingNode('noise',asUtility=True)
    mc.setAttr("%s.noiseType"%noise_node,0)
    mc.setAttr("%s.ratio"%noise_node,0.707)
    mc.setAttr("%s.frequencyRatio"%noise_node,2)
    
    place2d = mc.shadingNode('place2dTexture',asUtility=True)
    mc.setAttr("%s.repeatU"%place2d, 3)
    mc.setAttr("%s.repeatV"%place2d, 3)
    mc.connectAttr('%s.outUV'%place2d, '%s.uvCoord'%noise_node)
    mc.connectAttr('%s.outUvFilterSize'%place2d,'%s.uvFilterSize'%noise_node)

    
    layer = mc.shadingNode("layeredTexture",asUtility=True, n='layeredTexture')
    mc.setAttr("%s.inputs[0].alpha"%layer, 0.450)
    mc.setAttr("%s.alphaIsLuminance"%layer, 1)
    mc.connectAttr("%s.outColor"%noise_node, "%s.inputs[0].color"%layer)
    mc.connectAttr("%s.outColor"%ramp_node, "%s.inputs[1].color"%layer)

    return layer
    

def spec_2(spec_file):
    ramp_node = mc.shadingNode('ramp',asUtility=True, n='RemapRamp')
    mc.setAttr('%s.colorEntryList[0].position'%ramp_node, 0)
    mc.setAttr('%s.colorEntryList[1].position'%ramp_node, .207)
    mc.setAttr('%s.colorEntryList[0].color'%ramp_node, .323,.323,.323, type='double3')
    mc.setAttr('%s.colorEntryList[1].color'%ramp_node, 1,1,1, type='double3')
    rgb_to_Hsv = mc.shadingNode('rgbToHsv',asUtility=True, n='RemapRgbToHsv')
    mc.connectAttr('%s.outHsvH'%rgb_to_Hsv, '%s.uCoord'%ramp_node )
    mc.connectAttr('%s.outHsvV'%rgb_to_Hsv, '%s.vCoord'%ramp_node )
    mc.connectAttr('%s.outColor'%spec_file, '%s.inRgb'%rgb_to_Hsv )
    return ramp_node


def run():
    target_path = mc.ls(type='RedshiftMaterial', sl=True)
    if target_path:
        for target in target_path:
            print target 
            ############# sg enginge setAttr############
            mc.setAttr('%s.diffuse_roughness'%target, 0.450 )
            mc.setAttr('%s.refl_ior'%target, 1.330 )
            
            list_connect = mc.listConnections(target, c=True)
            for ix in list_connect:
                if 'bump_input' in ix:
                    bump = mc.listConnections(ix)
                    if mc.objectType(bump) == 'RedshiftBumpBlender':
                        print 'blend'
                        mc.setAttr('%s.additive'%bump, 1)
                        mc.setAttr('%s.bumpWeight0'%bump, 1)
                        bump_step(bump)
                    elif mc.objectType(bump) == 'RedshiftNormalMap':
                        print 'normal'
                        normal_node = bump[0]
                        mc.disconnectAttr('%s.outDisplacementVector'%normal_node, ix)
                        blend_bump = bump_(normal_node)
                        mc.connectAttr('%s.outColor'%blend_bump, '%s.bump_input'%target)
                        
                elif 'refl_roughness' in ix:
                    print 'rougn'
                    file_roughness = mc.listConnections(ix)[0]
                    mc.disconnectAttr('%s.outAlpha'%file_roughness, ix)
                elif 'refl_weight' in ix:
                    print ix
                    file_ = mc.listConnections(ix,type='file')[0]
                    if mc.objectType(file_) == 'file':
                        mc.disconnectAttr('%s.outAlpha'%file_, ix)
                        ramp_node = spec_step(file_)
                        mc.connectAttr('%s.outAlpha'%ramp_node, ix)
    else:
        mc.confirmDialog(message='Please Select material')


def run_network():
    target_path = mc.ls(type='RedshiftMaterial', sl=True)
    print target_path
    if target_path:
        for target in target_path:
            print target 
            ############# sg enginge setAttr############
            mc.setAttr('%s.refl_brdf'%target, 1 )
            mc.setAttr('%s.refl_ior'%target, 1.900 )
            
            list_connect = mc.listConnections(target, c=True)
            for ix in list_connect:
                if 'bump_input' in ix:
                    bump = mc.listConnections(ix)
                    if mc.objectType(bump) == 'RedshiftBumpBlender':
                        print 'blend'
                        mc.setAttr('%s.additive'%bump[0], 1)
                        mc.setAttr('%s.bumpWeight0'%bump[0], 1)
                        bump_step(bump)
                    elif mc.objectType(bump) == 'RedshiftNormalMap':
                        normal_node = bump[0]
                        mc.disconnectAttr('%s.outDisplacementVector'%normal_node, ix)
                        mc.setAttr('%s.scale'%normal_node, 5)
                        status, blend_bump = bump_2_normal(normal_node)

                        if status:
                            mc.connectAttr('%s.outColor'%blend_bump, '%s.bump_input'%target)
                        else:
                            mc.confirmDialog(message='Check path %s'%status)

                    elif mc.objectType(bump) == 'RedshiftBumpMap':
                        bump_node = bump[0]
                        mc.disconnectAttr('%s.out'%bump_node, ix)
                        file_node = mc.listConnections('%s.input'%bump_node)
                        file_path = mc.getAttr('%s.fileTextureName'%file_node[0])
                        normal_node = mc.shadingNode('RedshiftNormalMap',asUtility=True)
                        mc.setAttr('%s.scale'%normal_node, 5)
                        mc.setAttr('%s.tex0'%normal_node, file_path, type='string')
                        status, blend_bump = bump_2_normal(normal_node)

                        if status:
                            mc.connectAttr('%s.outColor'%blend_bump, '%s.bump_input'%target)
                        else:
                            mc.confirmDialog(message='Check path %s'%status)
                        
                elif 'refl_roughness' in ix:
                    print 'rougn'
                    file_roughness = mc.listConnections(ix)[0]
                    mc.disconnectAttr('%s.outAlpha'%file_roughness, ix)
                    layerTexture = refl_rough(file_roughness)
                    mc.connectAttr('%s.outAlpha'%layerTexture, ix)

                elif 'diffuse_roughness' in ix:
                    print 'rougn'
                    file_roughness = mc.listConnections(ix)[0]
                    mc.disconnectAttr('%s.outAlpha'%file_roughness, ix)
                    layerTexture = refl_rough(file_roughness)
                    mc.connectAttr('%s.outAlpha'%layerTexture, ix.replace('diffuse_roughness', 'refl_roughness'))

                elif 'refl_color' in ix:
                    print ix
                    file_ = mc.listConnections(ix,type='file')[0]
                    if mc.objectType(file_) == 'file':
                        mc.disconnectAttr('%s.outColor'%file_, ix)
                        ramp_node = spec_2(file_)
                        print 'testss'
                        mc.connectAttr('%s.outAlpha'%ramp_node, ix.replace('refl_color', 'refl_weight'))

                elif 'coat_bump_input' in ix:
                    print ix
                    bump_unuse = mc.listConnections(ix,type='RedshiftBumpMap')[0]
                    if mc.objectType(bump_unuse) == 'RedshiftBumpMap':
                        mc.disconnectAttr('%s.out'%bump_unuse, ix)
    else:
        mc.confirmDialog(message='Please Select material')

