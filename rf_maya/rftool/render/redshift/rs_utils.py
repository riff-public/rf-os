# redshift utils by ta.animator@gmail.com
# by TA
# 032417
# v.0.0.1 - beta


import sys
import os
import subprocess

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import maya.cmds as mc
import maya.mel as mm

def export_rsProxy(path, objs, connectivity=0, animation=0, start=0, end=0):
    """ export selected objects as redshift proxy -> rs """
    if objs:
        mc.select(objs)
        options = ['exportConnectivity=%s' % connectivity]

        if animation:
            options.append('startFrame=%s' % start)
            options.append('endFrame=%s' % end)

        if not os.path.exists(os.path.dirname(path)):
            os.makedirs(os.path.dirname(path))

        #return mc.file(path, pr=True, es=True, f=True, options=';'.join(options), typ='Redshift Proxy')
        return mm.eval('rsProxy -fp "{}" -s {} -e {} -sl;'.format(path,start,end))

    else:
        logger.warning('No objects selected. Please select atlease one object')


def create_proxy(nodeName='', proxyPath='', displayMode=0, displayPercentage=100, force=True, useSequence=0, proxySuffix='redshiftProxy', placeHolderSuffix='rsPlaceHolder', gpuPreviewPath=None):
    """ create node redshift proxy """
    mc.select(cl=True)
    proxyName = '%s_%s' % (nodeName, proxySuffix) if proxySuffix else nodeName
    transformName = '%s_%s' % (nodeName, placeHolderSuffix) if placeHolderSuffix else nodeName

    if force:
        if mc.objExists(proxyName):
            mc.delete(proxyName)

    if not mc.objExists(proxyName):
        result = mm.eval('redshiftCreateProxy;')

        if nodeName:
            proxyNode, shape, transform = result
            # print 'rename %s->%s' % (proxyNode, proxyName)
            proxyNode = mc.rename(proxyNode, proxyName)
            # print 'rename %s->%s' % (transform, transformName)
            transform = mc.rename(transform, transformName)
            shape = mc.listRelatives(transform, s=True)[0]

            if gpuPreviewPath: 
                # create gpu 
                gpuNode = mc.createNode('gpuCache')
                mc.setAttr('%s.cacheFileName' % gpuNode, gpuPreviewPath, type='string')
                gpuTransform = mc.listRelatives(gpuNode, p=True)[0]
                mc.parent(shape, gpuTransform, r=True, s=True)
                mc.delete(transform)
                transform = mc.rename(gpuTransform, transformName)
                mc.setAttr('{}.displayMode'.format(proxyNode), 3)

            if proxyPath:
                mc.setAttr('%s.fileName' % proxyNode, proxyPath, type='string')

            if not gpuPreviewPath: 
                mc.setAttr('%s.displayMode' % proxyNode, displayMode)
            # print proxyNode
            # set default
            mc.setAttr('%s.objectIdMode' % proxyNode, 1)
            mc.setAttr('%s.visibilityMode' % proxyNode, 1)
            mc.setAttr('%s.useFrameExtension' % proxyNode, useSequence)

            if displayPercentage:
                mc.setAttr('%s.displayPercent' % proxyNode, int(displayPercentage))

            return proxyNode, shape, transform

        else:
            return result
    else:
        logger.warning('Node already exists "%s"' % proxyName)


def set_preview(node, mode=1, displayPercentage=4):
    """ ex. set_preview(node='assetName', mode=1, displayPercentage=4)
        mode 0 bounding box
        mode 1 preview mesh
        mode 2 linked mesh
        mode 3 hide in viewport
    """
    mc.setAttr('%s.displayMode' % node, mode)
    mc.setAttr('%s.displayPercent' % node, displayPercentage)

def add_smooth_ply(name, plys):
    if not mc.objExists(name):
        rsMeshNode = mc.createNode('RedshiftMeshParameters')
        name = mc.rename(rsMeshNode, name)
    mc.sets(plys, add=name)


def smooth_data_file(asset, res='md'): 
    asset.context.update(step='model', process='main', res=res, entityType='asset')
    dataDir = asset.path.output_hero().abs_path()
    smoothData = asset.output_name(outputKey='smoothData', hero=True, ext='.yml')
    data = '%s/%s' % (dataDir, smoothData)
    return data

def add_object_id(name, plys): 
    if not mc.objExists(name):
        rsMeshNode = mc.createNode('RedshiftObjectId')
        name = mc.rename(rsMeshNode, name)
    mc.sets(plys, add=name)
    return name

def check_plugin():
    """ load plugin if not loaded """
    try:
        pluginPath = 'redshift4maya.mll'
        if not mc.pluginInfo(pluginPath, q=True, l=True):
            mc.loadPlugin(pluginPath, qt=True)
            logger.info('%s Plugin loaded' % pluginPath)
            return
        logger.info('%s Plugin is loaded' % pluginPath)
    except Exception as e:
        logger.error(e)


def convert_texture(textures=[]):
    # limit 30000 char 
    import rf_config as config
    reload(config)
    rsConverter = config.Software.rsTextureProcessor

    txFilter = [i for i in textures if i.split(".")[-1] in ["exr", "png"]]

    flags = ["-cs"]
    cmds = [rsConverter]
    cmds += txFilter
    cmds += flags
    #cmds += ['-s', '-wx', '-wy', '-ocolor']
    subprocess.call(cmds)

def convert_all_texture(path):
    if os.path.exists(path):
        allFiles = list_file_texture(path)
        filesList = chunk_split(allFiles)
        for files in filesList:
            convert_texture(files) if files else None

def list_file_texture(path):
    files = [os.path.join(path, d) for d in os.listdir(path) if os.path.isfile(os.path.join(path, d))]
    files = [a for a in files if os.path.splitext(a)[-1] not in ('', '.rstexbin')] if files else []
    
    return files


def get_rsproxy_node(obj): 
    nodes = mc.listHistory(obj)
    rsNode = [a for a in nodes if mc.objectType(a, isType='RedshiftProxyMesh')]
    return rsNode[0] if rsNode else None

def list_rsproxy_nodes(): 
    return mc.ls(type='RedshiftProxyMesh')

def get_mesh_from_proxy(node): 
    nodes = mc.listHistory(node, f=True)
    meshes = [a for a in nodes if mc.objectType(a, isType='mesh')]
    return meshes[0] if meshes else None

def chunk_split(textures): 
    limit = 30000 
    count = 0
    limitList = []
    allList = []
    for texture in textures: 
        count += len(texture)
        if count > limit: 
            unfinishList = [a for a in textures if not a in limitList]
            anyList = chunk_split(unfinishList)
            for list in anyList:
                allList.append(list)
            break
        limitList.append(texture)
    allList.append(limitList)
    return allList


def create_aov(type): 
    return mm.eval('redshiftCreateAov("%s")' % type)

def list_aov_type(): 
    aovs = mc.ls(type='RedshiftAOV')
    return list(set([mc.getAttr('%s.aovType' % a) for a in aovs] if aovs else []))

def update_aov_list(): 
    mm.eval('redshiftUpdateActiveAovList')


# check_plugin()

# // Result: Mel procedure found in: C:/ProgramData/Redshift/Plugins/Maya/Common/scripts/redshiftProxy.mel //
# file -force -options "startFrame=1;endFrame=2;frameStep=1;exportConnectivity=0;" -typ "Redshift Proxy" -pr -es