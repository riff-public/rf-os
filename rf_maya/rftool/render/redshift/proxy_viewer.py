# v.0.0.1 polytag switcher
_title = 'Redshift Proxy Viewer'
_version = 'v.0.0.1'
_description = 'wip'
uiName = 'RedshiftProxyViewer'

#Import python modules
import sys
import os
import getpass
import logging
from functools import partial

import maya.cmds as mc
import rs_utils
reload(rs_utils)

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]


class UI(object):
    """docstring for UI"""
    def __init__(self):
        super(UI, self).__init__()
        # window size width x height
        self.w = 200
        self.h = 220
        self.displayPercentValue = 4

    def show(self):
        self.window = mc.window(uiName, t='%s - %s - %s' % (_title, _version, _description))
        self.layout = mc.columnLayout(adj=4, rs=4)
        mc.text(l='Display Proxy Mode', align='center')
        mc.button(l='Box', h=30, bgc=(0.6, 1, 0.6), c=partial(self.set_proxy_display, 0))
        mc.button(l='Hide', h=30, bgc=(1, 0.4, 0.4), c=partial(self.set_proxy_display, 3))
        mc.separator()
        mc.button(l='Preview Mesh', h=30, bgc=(0.5, 0.8, 1), c=partial(self.set_proxy_display, 1))
        mc.text(l='Display Percentage', align='center')
        self.percentSlider = mc.intSlider(min=0, max=100, value=0, step=1, dc=partial(self.set_preview_value))
        self.valueTX = mc.textField(ec=partial(self.set_preview_value_slider))
        mc.separator()
        self.selectCB = mc.checkBox(l='Select', v=True)
        mc.showWindow()
        mc.window(uiName, e=True, wh=[self.w, self.h])

        mc.intSlider(self.percentSlider, e=True, v=self.displayPercentValue)
        mc.textField(self.valueTX, e=True, tx=self.displayPercentValue)

    def set_proxy_display(self, mode, *args): 
    	percentage = mc.intSlider(self.percentSlider, q=True, v=True)
    	nodes = self.get_selection()
    	for node in nodes: 
    		rs_utils.set_preview(node, mode=mode, displayPercentage=percentage)
    	
    	select = mc.checkBox(self.selectCB, q=True, v=True)
    	if select: 
    		self.select_mesh(nodes)

    def set_preview_value(self, *args): 
    	percentage = mc.intSlider(self.percentSlider, q=True, v=True)
    	mc.textField(self.valueTX, e=True, tx=percentage)

    def set_preview_value_slider(self, *args): 
        percentage = mc.textField(self.valueTX, q=True, tx=True)
        mc.intSlider(self.percentSlider, e=True, v=int(percentage))

    def get_selection(self): 
    	sels = mc.ls(sl=True)
    	if sels: 
    		return [rs_utils.get_rsproxy_node(a) for a in sels]
    	return rs_utils.list_rsproxy_nodes()

    def select_mesh(self, proxyNodes): 
    	meshes = [rs_utils.get_mesh_from_proxy(a) for a in proxyNodes]
    	mc.select(meshes)


def show(): 
	if mc.window(uiName, exists=True): 
		mc.deleteUI(uiName)
	app = UI()
	app.show()
	return app 