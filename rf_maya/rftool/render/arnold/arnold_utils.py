# redshift utils by ta.animator@gmail.com
# by TA
# 032417
# v.0.0.1 - beta

import os

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import maya.cmds as mc
import mtoa.core as arcore

def export_aiStandIn(path, objs, animation=0, start=0, end=0):
    """ export selected graph as arnold standin -> ass """
    if objs:
        mc.select(objs)
        '''
        args = {'f':path,
                's':objs,
                'shadowLinks':0,
                'mask':6399,
                'lightLinks':1,
                'boundingBox':True,
                'exportAllShadingGroups':True,
                'fullPath':True}
        if animation:
            args['startFrame'] = start
            args['endFrame'] = end'''

        if not os.path.exists(os.path.dirname(path)):
            os.makedirs(os.path.dirname(path))

        #export_results = mc.arnoldExportAss(**args)
        export_results = mc.file(path, force=True, options="-shadowLinks 1;-mask 6399;-lightLinks 1;-exportAllShadingGroups;-boundingBox;-fullPath",
                                typ="ASS Export", pr=True, es=True)
        return export_results

    else:
        logger.warning('No objects specified')


def create_aiStandIn(nodeName='', proxyPath='', displayMode=0, force=True, useSequence=0, proxySuffix='aiStandInShape', placeHolderSuffix='aiStandIn'):
    """ create node redshift proxy """
    mc.select(cl=True)
    proxyName = '%s_%s' % (nodeName, proxySuffix) if proxySuffix else nodeName
    transformName = '%s_%s' % (nodeName, placeHolderSuffix) if placeHolderSuffix else nodeName

    if force:
        if mc.objExists(proxyName):
            mc.delete(proxyName)

    if not mc.objExists(proxyName):
        node = arcore.createStandIn(path=proxyPath)
        transform = mc.listRelatives(node, parent=True)[0]
        mc.setAttr('%s.mode' % node, displayMode)
        mc.setAttr('%s.useFrameExtension' % node, useSequence)

        if nodeName:
            node = mc.rename(node, proxyName)
            transform = mc.rename(transform, transformName)
            
        return node, transform
    else:
        logger.warning('Node already exists "%s"' % proxyName)
        return None, None