import sys, os
import re
import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
logger.addHandler(logging.NullHandler())

import maya.cmds as mc
import maya.mel as mm
import yaml
import pymel.mayautils as mu


from rf_utils import file_utils
from rftool.utils import maya_utils
from rf_utils.context import context_info
reload(maya_utils)
from rf_utils.pipeline import asset_tag

faceSplit = '.f['


class Cache: 
    assetContext = dict()

def list_shader(targetGrp=None, objs=None, collapseFaceAssignment=False):
    ''' get all shading group in the scene '''

    seNodes = mc.ls(type='shadingEngine')
    shaderInfo = dict()  # {shaderName: {'objs':['sphereShape1', 'sphere2.f[0]'], 'shadingEngine':'lambert2SG'}}

    # if there's no objs provided, get all the children under targetGrp
    if not objs:
        objs = list_child_objects(targetGrp)

    face_counts = {}
    for obj in objs:
        fc = 0
        if mc.nodeType(obj) == 'transform' and mc.listRelatives(obj, shapes=True, type='mesh', ni=True):
            fc = mc.polyEvaluate(obj, face=True) - 1
        face_counts[obj] = fc

    for seNode in seNodes :
        # only take shading group that are not referenced
        if not mc.referenceQuery(seNode, inr=True):
            shaders = mc.listConnections('%s.surfaceShader' % seNode, s=True)
            members = mc.sets(seNode, q=True) 
        
            # only include shading group that has a shader connected and has member to assigned to set
            if shaders and members:
                strAssignments = []
                for mem in members:
                    splits = mem.split(faceSplit)
                    if faceSplit in mem:
                        mem_obj = splits[0]
                    else:
                        mem_obj = mem

                    if mem_obj in objs:
                        if mc.nodeType(mem_obj) == 'mesh':
                            mem_obj = mc.listRelatives(mem_obj, parent=True)[0]

                        # if .f[ in member
                        if len(splits) > 1:
                            resultStr = '%s%s%s' %(mem_obj, faceSplit, splits[1])

                            # convert all face assignment to object assignment
                            if collapseFaceAssignment:
                                fIndxStr = splits[1][:-1]
                                if ':' in fIndxStr:
                                    fIndexSplits = fIndxStr.split(':')
                                    minFaceIndex = int(fIndexSplits[0])
                                    maxFaceIndex = int(fIndexSplits[-1])
                                    if minFaceIndex == 0 and maxFaceIndex == face_counts[mem_obj]: # in case : xxx_Geof.[0:5555] [1stFace:lastFace]
                                        resultStr = mem_obj
                                else:
                                    faceIndex = int(fIndxStr)

                                    if faceIndex == face_counts[mem_obj] and face_counts[mem_obj] == 0: # in case of Card (object with only one face) xx_Geof.[onlyOneFace]
                                        resultStr = mem_obj
                        else:
                            resultStr = mem_obj 

                        strAssignments.append(str(resultStr))
                strShaders = str(shaders[0])
                shaderInfo[strShaders] = {'objs': strAssignments, 
                                        'shadingEngine': str(seNode)}

    return shaderInfo

def list_yeti_shader(targetGrp):
    pass

def list_child_objects(targetGrp):
    ''' Get all DAG children within a group '''
    return [targetGrp] + mc.listRelatives(targetGrp, ad=True, pa=True)


# def export_shade(exportPath, targetGrp=None, objs=None) :
#     """ export shadingEngines """
#     exportResult = False
#     mtime = 0 if not os.path.exists(exportPath) else os.path.getmtime(exportPath)

#     shaderInfo = list_shader(targetGrp=targetGrp, objs=objs)

#     if shaderInfo :
#         shadingEngines = [shaderInfo.get(shadeNode).get('shadingEngine') for shadeNode in shaderInfo if not mc.referenceQuery(shadeNode, isNodeReferenced = True)]
#         mc.select(shadingEngines, r=True, ne=True)
#         result = mc.file(exportPath, force = True, options = 'v=0', typ = 'mayaAscii', pr = True, es = True, shader=True)

#         if os.path.exists(exportPath):
#             exportResult = True if not mtime == os.path.getmtime(exportPath) else False

#     if exportResult:
#         return result

def getUvLinks(shader, sg):
    histories = mc.listHistory(shader)
    # return if no uvChooser node type found in history
    uvChoosers = [n for n in histories if mc.nodeType(n)=='uvChooser']
    if not uvChoosers:
        return [], []

    # only consider looping 2D texture node types
    textureTypes = mc.listNodeTypes("texture/2D")
    textures = [n for n in histories if mc.nodeType(n) in textureTypes]
    uvLinks = []
    for tex in textures:
        links = mc.uvLink(q=True, texture=tex)
        valid_uvLinks = []
        for link in links:
            r = re.search(r"\[([0-9]+)\]", link)
            if r:
                uvSetIndex = int(r.group(1))
                # skip map1, aka. uvSet[0]
                if uvSetIndex > 0:  
                    # get uv set name from geo.uvSet[1].uvSetName
                    try:
                        uvSetName = mc.getAttr(link)
                        geoName = link.split('.')[0]
                        valid_uvLinks.append(str('{}.{}'.format(geoName, uvSetName)))
                    except Exception, e:
                        logger.error('Failed getting UV link: {}'.format(link))
                        logger.error(e)
        if valid_uvLinks:
            valid_uvLinks = clean_namespace(valid_uvLinks) # clean namespace
            uvLinks.append((str(tex), valid_uvLinks))
    return uvLinks, uvChoosers

def export_shade_with_data(exportPath, targetGrp=None, objs=None):
    """ export shadingEngines with objs attribute for assignment """
    exportResult = False
    mtime = 0 if not os.path.exists(exportPath) else os.path.getmtime(exportPath)
    exceptions = ['initialShadingGroup']

    # EXAMPLE: {shaderName: {'objs':['sphereShape1', 'sphere2.f[0]'], 'shadingEngine':'lambert2SG'}}
    shaderInfo = list_shader(targetGrp=targetGrp, objs=objs, collapseFaceAssignment=True)  
    shadingEngines = []
    if shaderInfo :
        disconnect_attrs = []
        # iterate thru the shader info
        for shader, info in shaderInfo.iteritems():
            # SG
            shadingEngine = info['shadingEngine']
            if mc.lockNode(shadingEngine, q=True)[0]:
                mc.lockNode(shadingEngine, l=False)

            # --- assigned objects
            objs = info['objs']
            objs = clean_namespace(objs) # clean namespace
            # add attibute of assigned objects to SG
            add_attr(shadingEngine, 'objs', str(objs))
            if objs != []:
                if not shadingEngine in exceptions: 
                    shadingEngines.append(shadingEngine)

                # --- handle uvlink
                uvLinks, uvChoosers = getUvLinks(shader=shader, sg=shadingEngine)
                # print shader, uvLinks
                if uvLinks:
                    add_attr(shadingEngine, 'uvLink', str(uvLinks))
                    for uvc in uvChoosers:
                        out_cons = mc.listConnections(uvc, d=True, s=False, c=True, p=True)
                        if out_cons:
                            for i in xrange(0, len(out_cons), 2):
                                src = out_cons[i]
                                des = out_cons[i+1]
                                disconnect_attrs.append((src, des))
            
        # create directory (if not exists)
        if not os.path.exists(os.path.dirname(exportPath)):
            os.makedirs(os.path.dirname(exportPath))

        # disconnect attrs
        for src, des in disconnect_attrs:
            if mc.isConnected(src, des):
                mc.disconnectAttr(src, des)

        # export the shading engines as .ma file
        mc.select(shadingEngines, r=True, ne=True)
        print 'shadingEngines', shadingEngines
        result = mc.file(exportPath, force=True, options='v=0', typ='mayaAscii', pr=True, es=True, chn=True, ch=True,  shader=True)

        # reconnect disconnected attrs
        if disconnect_attrs:
            for src, des in disconnect_attrs:
                if not mc.isConnected(src, des):
                    mc.connectAttr(src, des, f=True)

        if os.path.exists(exportPath):
            exportResult = True if not mtime == os.path.getmtime(exportPath) else False

    else:
        logger.warning('shaderInfo missing : may be shader is disconnected.')

    if exportResult:
        return result

def clean_namespace(objs):
    ''' Clean up namespaces in all obj in a list '''

    newList = []
    for obj in objs:
        if obj.startswith('|'):
            obj = obj[1:]  # ignore the first "|"
        
        fSplits = obj.split(faceSplit)
        vtx = ''
        if len(fSplits) > 1:
            obj = fSplits[0]
            vtx = '%s%s' %(faceSplit, fSplits[1])

        new_path = '|'.join([o.split(':')[-1] for o in obj.split('|')])
        new_path += vtx
        newList.append(new_path)

    return newList

# def add_shader_data(obj, shadingEngine, shader, objs):
#     add_attr(obj, 'shadingEngine', str(shadingEngine))
#     add_attr(obj, 'shader', str(shader))
#     add_attr(obj, 'objs', str(objs))

def add_attr(obj, attr, value):
    if not mc.objExists('%s.%s' % (obj, attr)):
        mc.addAttr(obj, ln=attr, dt='string')
        mc.setAttr('%s.%s' % (obj, attr), e=True, keyable=True)
    mc.setAttr('%s.%s' % (obj, attr), value, type='string')

def get_shader_attr(obj):
    shadingEngines = mc.getAttr('%s.shadingEngine' % obj) if mc.objExists('%s.shadingEngine' % obj) else ''
    shader = mc.getAttr('%s.shader' % obj) if mc.objExists('%s.shader' % obj) else ''
    objs = eval(mc.getAttr('%s.objs' % obj)) if mc.objExists('%s.objs' % obj) else ''
    return shadingEngines, shader, objs

def get_uvlink_attr(obj):
    uvLink = eval(mc.getAttr('%s.uvLink' % obj)) if mc.objExists('%s.uvLink' % obj) else []
    return uvLink

def export_shade_data(dataPath, targetGrp=None, objs=None) :
    exportResult = False
    mtime = 0 if not os.path.exists(dataPath) else os.path.getmtime(dataPath)

    shaderInfo = list_shader(targetGrp=targetGrp, objs=objs)
    file_utils.ymlDumper(dataPath, shaderInfo)

    if os.path.exists(dataPath):
        exportResult = True if not mtime == os.path.getmtime(dataPath) else False

    if exportResult:
        return dataPath

def import_shade(shadeFile, namespace):
    return mc.file(shadeFile, r=True, ns=namespace)

def apply_shade(shadeDataFile, shadeNamespace, targetNamespace):
    shadeInfo = file_utils.ymlLoader( shadeDataFile )
    keys = shadeInfo.keys()

    for key in keys :
        objs = shadeInfo.get(key).get('objs')
        shad = shadeInfo.get(key).get('shaders')[0]

        for obj in objs :
            if '|' in obj :
                insert = '|%s:'%targetNamespace
                obj = insert.join(obj.split('|'))

            mc.select( '%s:%s' %(targetNamespace , obj))
            mc.hyperShade( assign = '%s:%s' %(shadeNamespace,shad))  

def getUvLinkAttr(geo, uvSetName):
    indices = mc.polyUVSet(geo, q=True, allUVSetsIndices=True)
    for idx in indices:
        attr_name = '{}.uvSet[{}].uvSetName'.format(geo, idx)
        currUVSetName = mc.getAttr(attr_name)
        # print currUVSetName, uvSetName, idx
        if currUVSetName == uvSetName:
            return attr_name

def apply_ref_shade(shadeFile, shadeNamespace, targetNamespace='', connectionInfo=[], reloadRef=True, debug=True, *kwargs):
    """ data on the shading Engine. use if shade is reference and target is reference """
    result = True
    # absolute path 
    shadeFileAbs = context_info.RootPath(shadeFile).abs_path()

    # clean the shadeFile path pass into the function
    shadeFile = shadeFile.replace('\\', '/')

    # get all the existing reference
    refs = list(set([a.split('{')[0] for a in mc.file(q=True, r=True)]))
    refs = [p.replace('\\', '/') for p in refs]
    refs = [context_info.RootPath(p).abs_path() for p in refs]

    # create new reference if the shade file reference or the shade namespace doesn't exists
    if not shadeFileAbs in refs or not mc.namespace(ex=shadeNamespace):
        ref = mc.file(shadeFile, r=True, f=True, ns=shadeNamespace)

    if reloadRef:
        # else try to reload the existing ref
        try: 
            maya_utils.reload_reference(shadeFile)
        except Exception as e: 
            logger.error(e)

    if targetNamespace:
        targetNamespace += ':'

    shadingEngines = mc.ls('%s:*' %shadeNamespace, type='shadingEngine')
    # looping over each shading engine
    missing = []
    for shadingEngine in shadingEngines:
        # get the added assigned objs in attibutes inside the shading engine
        se, shader, objs = get_shader_attr(shadingEngine)
        if not objs:
            continue

        # add the namespace to the obj paths
        assignObjs = []
        for obj in objs:
            objNs = '|'.join(['%s%s' %(targetNamespace, o) for o in obj.split('|')])
            assignObjs.append(objNs)

        # validate the objects, also collect the missing ones
        validObjs = []
        for o in assignObjs:
            if mc.objExists(o):
                validObjs.append(o)
            else:
                missing.append(o)

        # assign the shaders to valid objects
        if validObjs:
            try:
                mc.sets(validObjs, e=True, nw=True, fe=shadingEngine)
            except Exception, e:
                logger.error(e)

    if missing and debug:
        logger.warning('Missing %s objs' % (len(missing)))
        logger.debug(missing)
        result = False
    
    # apply uv links
    uvLink_missing = []
    for shadingEngine in shadingEngines:
        uvLinks = get_uvlink_attr(shadingEngine)
        for uvl in uvLinks:
            texture = '{}:{}'.format(shadeNamespace, uvl[0])
            link_objs = uvl[1]

            for obj in link_objs:
                objSplits = obj.split('.')
                if len(objSplits) <= 1:
                    logger.warning('Skipping uvLink: {}, invalid name'.format(obj))
                    continue
                geoName = objSplits[0]
                uvName = objSplits[1]
                print geoName
                if targetNamespace[1] != ':':
                    uvLinkNamespace = targetNamespace
                else:
                    uvLinkNamespace = targetNamespace[1:]
                geoNs = '|'.join(['%s%s' %(uvLinkNamespace, o) for o in geoName.split('|')])
                if mc.objExists(geoNs) and mc.nodeType(geoNs)=='mesh':
                    uvLinkAttr = getUvLinkAttr(geo=geoNs, uvSetName=uvName)
                    if uvLinkAttr:
                        logger.debug('Apply uvLink: {} {}'.format(uvLinkAttr, texture))
                        mc.uvLink(uvSet=uvLinkAttr, texture=texture)
                    else:
                        logger.error('Cannot find index for uvLink: {}'.format(obj))
                else:
                    uvLink_missing.append(obj)
    if uvLink_missing:
        logger.warning('Missing uvLink on %s objs' % (len(uvLink_missing)))
        logger.debug(uvLink_missing)
        result = False

    # connectionInfo = ([rig to shd], [shd to rig])
    if connectionInfo and len(connectionInfo) == 2:
        # in connection: shd --> rig
        if connectionInfo[0]:
            logger.debug('Applying output connections: %s' %connectionInfo[1])
            for src, des in connectionInfo[0]:
                srcObjName = '%s:%s' %(shadeNamespace, src)
                if not mc.objExists(srcObjName):
                    logger.error('Source node does not exist: %s' %srcObjName)
                    result = False
                    continue

                desObjName = '%s:%s' %(targetNamespace, des)
                if not mc.objExists(desObjName):
                    logger.error('Destination node does not exist: %s' %desObjName)
                    result = False
                    continue
                    
                if not mc.isConnected(srcObjName, desObjName):
                    try:
                        mc.connectAttr(srcObjName, desObjName, f=True)
                    except Exception, e:
                        logger.error(e)
                        result = False

        # out connection: rig --> shd
        if connectionInfo[1]:
            logger.debug('Applying input connections: %s' %connectionInfo[0])
            for src, des in connectionInfo[1]:
                srcObjName = '%s%s' %(targetNamespace, src)
                if not mc.objExists(srcObjName):
                    logger.error('Source node does not exist: %s' %srcObjName)
                    result = False
                    continue

                desObjName = '%s:%s' %(shadeNamespace, des)
                if not mc.objExists(desObjName):
                    logger.error('Destination node does not exist: %s' %desObjName)
                    result = False
                    continue
                if not mc.isConnected(srcObjName, desObjName):
                    try:
                        mc.connectAttr(srcObjName, desObjName, f=True)
                    except Exception, e:
                        logger.error(e)
                        result = False
        
    return result

def apply_import_shade(shadeFile, targetNamespace=''):
    """ data on the shading Engine. use if shade is import and target is reference """

    # all the existing namespace
    refs = list(set([a.split('{')[0] for a in mc.file(q=True, r=True)]))

    # import the shader under a 'tmp_shade' namespace
    shadeNamespace = 'tmp_shade'
    if not shadeFile in refs or not mc.namespace(ex=shadeNamespace):
        try: 
            ref = mc.file(shadeFile, i=True, f=True, ns=shadeNamespace)
        except Exception as e: 
            logger.error(e)
            logger.error('reference file error, possibly .ai issues')

    # add namespace to obj paths, add : if the arg is not ''
    if targetNamespace:
        targetNamespace += ':'
            
    shadingEngines = mc.ls('%s:*' % shadeNamespace, type='shadingEngine')
    missing = []
    for shadingEngine in shadingEngines:
        se, shader, objs = get_shader_attr(shadingEngine)
        if not objs:
            continue

        assignObjs = []
        
        for obj in objs:
            objNs = '|'.join(['%s%s' %(targetNamespace, o) for o in obj.split('|')])
            assignObjs.append(objNs)

        # validate the objects, also collect the missing ones
        validObjs = []
        for o in assignObjs:
            if mc.objExists(o):
                validObjs.append(o)
            else:
                missing.append(o)

        # assign the shaders to valid objects
        if validObjs:
            try:
                mc.sets(validObjs, e=True, nw=True, fe=shadingEngine)
            except Exception, e:
                logger.error(e)

    # clear namespace
    mc.namespace(f=True, mv=(':%s' % shadeNamespace, ':'))
    mc.namespace(rm=shadeNamespace)

    if missing:
        logger.warning('missing objs %s' % (len(missing)))
        logger.debug(missing)
    else:
        return True

def get_objs_all_ref(targetRefPath, objs):
    if targetRefPath:
        refs = mc.file(q=True, r=True)
        allNamespaces = [mc.referenceQuery(a, namespace=True).split(':')[-1] for a in refs if targetRefPath in a]
        allObjs = []
        for ns in allNamespaces:
            for obj in objs:
                objNs = '|'.join(['%s:%s' %(ns, o) for o in obj.split('|')])
                allObjs.append(objNs)
        return allObjs

def find_connect_ref_shade(obj):
    shape = mc.listRelatives(obj, s=True, f=True)
    path = ''
    if shape:
        nodes = mc.listConnections(shape[0], d=True)
        if nodes: 
            shadingEngine = [a for a in nodes if mc.objectType(a, isType='shadingEngine')]
            refNode = [a for a in nodes if mc.objectType(a, isType='reference')]
            targetNode = shadingEngine[0] if shadingEngine else refNode[0] if refNode else ''
            if targetNode:
                shadePath = mc.referenceQuery(targetNode, f=True) if mc.referenceQuery(targetNode, inr=True) else ''
                path = shadePath if not shadePath == mc.referenceQuery(obj, f=True) else ''

    return path

def unconnected_shader(shadeRefPath):
    shadingEngine = is_ref_shader(shadeRefPath)
    if shadingEngine:
        for engine in shadingEngine:
            meshes = mc.listConnections(engine, type='mesh')
            if meshes:
                break
        return False if meshes else True
    return False

def is_ref_shader(shadeRefPath):
    if shadeRefPath:
        namespace = mc.referenceQuery(shadeRefPath, ns=True)
        shadingEngine = [a for a in mc.ls('%s:*' % namespace) if mc.objectType(a, isType='shadingEngine')]
        return shadingEngine

def get_assigned_ref_shade(obj):
    shadeFilePath, shadeNamespace = '', ''
    geoNameSpace = mc.referenceQuery(obj, namespace=True)
    geoRefNode = mc.referenceQuery(obj, rfn=True)
    geoRefPath = mc.referenceQuery(obj, f=True)
    shape = mc.listRelatives(obj, s=True, type='mesh', ni=True)
    shadingGrp = mc.listConnections(shape[0], type='reference') # if not shading group found
    if not shadingGrp:
        shadingGrp = mc.listConnections(shape[0], type='shadingEngine')
    if shadingGrp:
        # try to get the current assigned look
        # if nothing is found (or the asset is currently connected to lambert1),
        # just return ''

        # if SG is a reference and not coming from the same refNode as the obj
        if mc.referenceQuery(shadingGrp[0], inr=True):
            filePath = mc.referenceQuery(shadingGrp[0], filename=True)
            namespace = mc.referenceQuery(shadingGrp[0], namespace=True)
            refNode = mc.referenceQuery(shadingGrp[0], rfn=True)
            if refNode != geoRefNode and '_mtr_' in os.path.basename(filePath):
                shadeFilePath = filePath
                shadeNamespace = namespace

    return shadeFilePath, shadeNamespace, geoNameSpace

def reconnect_selected_shader(obj):
    # shadeFilePath, shadeNamespace, geoNameSpace = fix_reference_shader(obj)
    geoNameSpace = mc.referenceQuery(obj, namespace=True)
    geos = get_all_geo_in_ns(geoNameSpace)
    if not geos:
        return

    shadeFilePath, shadeNamespace, geoNameSpace = get_assigned_ref_shade(obj)
    broken_geos = set()
    for geo in geos:
        cons = mc.listConnections(geo, d=True, p=True, c=True, type='reference')
        if not cons:
            continue

        for i in xrange(0, len(cons), 2):
            src = cons[i]
            srcSplits = src.split('.')
            srcAttr = '.'.join(srcSplits[1:])

            des = cons[i+1]
            desSplits = des.split('.')
            desNode = desSplits[0]
            desAttr = '.'.join(desSplits[1:])
            if srcAttr.startswith('instObjGroups') and desAttr.startswith('placeHolderList') and mc.isConnected(src, des):
                mc.disconnectAttr(src, des)
                broken_geos.add(geo)

    # reassign to lambert 1
    mc.sets(geos, e=True, nw=True, fe='initialShadingGroup')
    
    # reconnect to shader assigned before, if any
    if shadeFilePath and shadeNamespace:
        apply_ref_shade(shadeFilePath, shadeNamespace, geoNameSpace)

        # if we're in VP2, need to refresh 
        if not mc.about(batch=True):
            if maya_utils.getCurrentViewportRenderer() == 'vp2':
                # pause
                if not mc.ogs(query=True, pause=True):
                    mc.ogs(pause=True)
                # unpause
                mc.ogs(pause=True)

        return True

def clear_selected_shader(obj):
    ''' Remove all shader reference relavant to the object '''

    geoNameSpace = mc.referenceQuery(obj, namespace=True)
    geoRefPath = mc.referenceQuery(obj, f=True)

    geos = get_all_geo_in_ns(geoNameSpace)
    if not geos:
        return
    asset = context_info.ContextPathInfo(path=geoRefPath)
    asset_name = asset.name

    shadeFilePath, shadeNamespace, geoNameSpace = get_assigned_ref_shade(obj)
    shadeFilePath_wnc = shadeFilePath.split('{')[0]
    shadeFilePath_wnc = shadeFilePath_wnc.replace('\\', '/')
    for refpath in mc.file(q=True, r=True):
        refpath_wnc = refpath.split('{')[0]
        refpath_wnc = refpath_wnc.replace('\\', '/')
        reffile = os.path.basename(refpath_wnc)
        shade = context_info.ContextPathInfo(path=refpath_wnc)
        shade_name = shade.name
        isAssetShader = asset_name == shade_name and reffile.startswith('{0}_{1}_'.format(asset_name, context_info.Lib.mtr))
        if refpath_wnc == shadeFilePath_wnc or isAssetShader:
            mc.file(refpath, rr=True)

    # clear all connections to all SG
    for geo in geos:
        cons = mc.listConnections(geo, type='shadingEngine', d=True, s=False, c=True, p=True)
        if not cons:
            continue
        for i in xrange(0, len(cons), 2):
            src = cons[i]
            des = cons[i+1]
            try:
                mc.disconnectAttr(src, des)
            except Exception, e:
                logger.error('Error disconnecting: %s, %s' %(src, des))
                logger.error(e)

def get_all_geo_in_ns(ns):
    # geos = [n for n in mc.ls('%s:*' %ns[1:], type='mesh') if not mc.getAttr('%s.intermediateObject' %n)]
    geos = []
    for tr in mc.ls('%s:*' %ns[1:], type='transform'):
        shapes = mc.listRelatives(tr, s=True, pa=True, ni=True, type='mesh')
        if shapes:
            geos.append(shapes[0])
    return geos

def fix_reference_shader(obj, fixoverride=True):
    ''' OBSOLETE '''
    shadeFilePath = ''
    shadeNamespace = ''
    geoNameSpace = mc.referenceQuery(obj, namespace=True)
    geoFilepath = mc.referenceQuery(obj, filename=True)
    shape = mc.listRelatives(obj, s=True)
    shadingGrp = mc.listConnections(shape[0], type='reference') # if not shading group found
    if not shadingGrp:
        shadingGrp = mc.listConnections(shape[0], type='shadingEngine')

    if shadingGrp:
        shadeNamespace = mc.referenceQuery(shadingGrp[0], namespace=True)
        shadeFilePath = mc.referenceQuery(shadingGrp[0], filename=True)

    if fixoverride: 
        maya_utils.remove_shader_override(geoFilepath)

    return shadeFilePath, shadeNamespace, geoNameSpace

def fix_unload_reference_shader(refPath): 
    # geoNameSpace = mc.referenceQuery(refPath, namespace=True)
    geoNameSpace = mc.file(refPath, q=True, ns=True)
    geoFilepath = mc.referenceQuery(refPath, filename=True)
    rnNode = mc.referenceQuery(refPath, rfn=True)
    shadingGrp = mc.listConnections(rnNode, type='reference')

    if shadingGrp: 
        if not shadingGrp[0] in ['sharedReferenceNode', '_UNKNOWN_REF_NODE_']: 
            shadeFilePath = mc.referenceQuery(shadingGrp[0], filename=True)
            shadeNamespace = mc.file(shadeFilePath, q=True, ns=True)
            maya_utils.remove_shader_override(geoFilepath, alwaysLoad=True)
            mu.executeDeferred(apply_ref_shade, shadeFilePath, shadeNamespace, geoNameSpace)
            return shadeFilePath, shadeNamespace, geoNameSpace

def fix_shader(objs):
    """ link existing shader """
    pass

def list_mtr_files(obj, step='ldv'):
    # use only last one
    # get ref
    attr = '%s.refPath' % obj
    path = mc.getAttr(attr) or mc.referenceQuery(obj, f=True) if mc.objExists(attr) else mc.referenceQuery(obj, f=True)
    heroDir = os.path.dirname(path)
    res = get_res(path)
    mtrDict = get_mtr_dict(heroDir, step, res)
    return mtrDict

def get_res(path): 
    entity = context_info.ContextPathInfo(path = path)
    outputList = entity.guess_outputKey(os.path.basename(path))
    if outputList:
        entity.context.update(step=outputList[0][0])
        entity.extract_name(os.path.basename(path), outputKey=outputList[0][1])
    return entity.res

def get_mtr_dict(heroPath, step, res='md'): 
    mtrDict = dict()
    files = file_utils.list_file(heroPath)
    print 'files', files

    # plys = maya_utils.find_ply(obj)

    # connectRef = find_connect_ref_shade(plys[0]) if plys else ''
    # hard code only mtr
    key = '_%s_%s_' % (context_info.Lib.mtr, step)
    files = [a for a in files if key in a and '_%s'%res in a] if files else []
    # hard code look
    for mtr in files:
        shadeFileName, ext = os.path.splitext(mtr)
        if ext in ['.mb', '.ma']:
            look = mtr.split(key)[-1].split('_')[0]
            mtrDict[look] = '%s/%s' % (heroPath, mtr)

    return mtrDict

def guess_mtr_files(project, assetName, step): 
    from rf_utils.sg import sg_process 
    asset = Cache.assetContext.get(project, dict()).get(assetName, None)
    
    if not asset: 
        context = context_info.Context()
        context.use_sg(sg_process.sg, project, 'asset', entityName=assetName)
        asset = context_info.ContextPathInfo(context)
        
    heroPath = asset.path.hero().abs_path()
    mtrDict = get_mtr_dict(heroPath, step)

    return mtrDict

def apply_single_shade(shadingEngine, namespace):
    se, shader, objs = get_shader_attr(shadingEngine)
    if not objs:
        logger.warning('No objs attribute found in: %s' %shadingEngine)
        return

    # add the namespace to the obj paths
    assignObjs = []
    for obj in objs:
        objNs = '|'.join(['%s:%s' %(namespace, o) for o in obj.split('|')])
        assignObjs.append(objNs)

    # validate the objects, also collect the missing ones
    validObjs = []
    missing = []
    for o in assignObjs:
        if mc.objExists(o):
            validObjs.append(o)
        else:
            missing.append(o)

    # assign the shaders to valid objects
    if validObjs:
        try:
            mc.sets(validObjs, e=True, nw=True, fe=shadingEngine)
            # mm.eval('sets -e -fe "{}" "{}"'.format(shadingEngine, validObjs))
        except Exception, e:
            logger.error(e)

    if missing:
        logger.warning('Missing %s objs' % (len(missing)))
        logger.debug(missing)
    else:
        return True

def find_connected_mtr(node): 
    """ find material channel that connect to fileNode """ 
    mtrNodes = ['RedshiftMaterial', 'lambert', 'blinne']
    desAttrs = mc.listConnections(node, s=False, d=True, p=True)

    if desAttrs: 
        for desAttr in desAttrs: 
            if '.' in desAttr: 
                node = desAttr.split('.')[0]
                attr = desAttr.split('.')[-1]
                if mc.objectType(node) in mtrNodes: 
                    return desAttr
                else: 
                    return find_connected_mtr(node)

def get_recorded_shade_data(namespace):
    asset = context_info.ContextPathInfo(path=mc.file(q=True, sn=True))
    geoGrpName = asset.projectInfo.asset.geo_grp()
    topGrps = mc.ls('{}{}'.format(namespace, geoGrpName), type='transform')
    shadeFile, topGrpName = '', ''
    if len(topGrps) == 1:
        topGrpName = topGrps[0]
        mtrPathAttr = '{}.mtrPath'.format(topGrpName)
        if mc.objExists(mtrPathAttr):
            shadeFile = mc.getAttr(mtrPathAttr)
    return shadeFile, topGrpName

def get_connection_info(namespace, shadePath):
    # get shade key
    asset = context_info.ContextPathInfo(path=shadePath)
    shadeFileExt = os.path.basename(shadePath)
    shadeFileName, ext = os.path.splitext(shadeFileExt)
    shadeFileName = shadeFileName.split('.')[0]  # get rid of .hero
    shadeKey = shadeFileName.split('_mtr_')[-1]

    # find geo grp
    geo_grp = '{}:{}'.format(namespace, asset.projectInfo.asset.geo_grp())
    connectionInfo = []
    shd_connect_attr = '{}.shdConnect'.format(geo_grp)
    if mc.objExists(shd_connect_attr):
        connectionStr = mc.getAttr(shd_connect_attr)
        connectionData = eval(connectionStr)
        if shadeKey in connectionData:
            connectionInfo = connectionData[shadeKey]
            return connectionInfo


def ref_shade(proj, geoNamespace, step='ldv'):
    tagGrp = '%s:%s' % (geoNamespace, proj.asset.geo_grp())
    tag = asset_tag.AssetTag(tagGrp)

    if mc.objExists(tagGrp) and mc.referenceQuery(tagGrp, inr=True):
        mtrDict = list_mtr_files(tagGrp, step)
        if not mtrDict:
            step = 'mdl'
            mtrDict = list_mtr_files(tagGrp, step)
        # name = mc.getAttr('%s.assetName' % tagGrp)
        # change the way we get name to adPath instead 
        refPath = mc.getAttr('%s.refPath' % tagGrp)
        if not refPath: 
            refPath = mc.referenceQuery(tagGrp, f=True).split('{')[0]
        name = context_info.ContextPathInfo(path=refPath).name

        look = tag.get_look() or 'main'
        shadeFile = mtrDict.get(look)

        # connect shade Connect for stroke
        if shadeFile:
            asset = context_info.ContextPathInfo(path=shadeFile)
            shadeFileExt = os.path.basename(shadeFile)
            shadeFileName, ext = os.path.splitext(shadeFileExt)

            connectionData = []
            if mc.objExists('%s.shdConnect' %tagGrp):
                connectionStr = mc.getAttr('%s.shdConnect' %tagGrp)
                connectionData = eval(connectionStr)

            shadeFileName = shadeFileName.split('.')[0]  # get rid of .hero
            shadeKey = shadeFileName.split('_mtr_')[-1]
            connectionInfo = []

            if shadeKey in connectionData:
                connectionInfo = connectionData[shadeKey]

        # connect shade finish

        print 'mtrDict', mtrDict

        if shadeFile:
            shadeNamespace = '%s_%s%sMtr' % (name, step, look.capitalize())

            # separate shader 
            separateShaderAttr = '%s.separateShader' % tagGrp
            separateShader = False
            if mc.objExists(separateShaderAttr): 
                separateShader = mc.getAttr(separateShaderAttr)

                if separateShader: 
                    instance_num = re.findall(r'\d{3}', geoNamespace)[0]
                    shadeNamespace = '{}_{}'.format(shadeNamespace, instance_num)
            # separate shader End
            
            try: 
                apply_ref_shade(shadeFile, shadeNamespace, geoNamespace, connectionInfo)
                asset_tag.update_tag(tagGrp, mtrPath=shadeFile)
            except Exception as e: 
                print e
        else:
            pass


def assignRigShade(targetNamespace = ''):    
    if targetNamespace:
        targetNamespace += ':'

    shadingEngines = mc.ls('%s:*' %targetNamespace, type='shadingEngine')
    # looping over each shading engine
    missing = []
    for shadingEngine in shadingEngines:
        # get the added assigned objs in attibutes inside the shading engine
        se, shader, objs = get_shader_attr(shadingEngine)
        if not objs:
            continue

        # add the namespace to the obj paths
        assignObjs = []
        for obj in objs:
            objNs = '|'.join(['%s%s' %(targetNamespace, o) for o in obj.split('|')])
            assignObjs.append(objNs)

        # validate the objects, also collect the missing ones
        validObjs = []
        for o in assignObjs:
            if mc.objExists(o):
                validObjs.append(o)
            else:
                missing.append(o)

        # assign the shaders to valid objects
        if validObjs:
            try:
                mc.sets(validObjs, e=True, nw=True, fe=shadingEngine)
            except Exception, e:
                logger.error(e)

    if missing and debug:
        logger.warning('Missing %s objs' % (len(missing)))
        logger.debug(missing)
        result = False


def get_shader_data(filepath): 
    from rf_utils import ascii_utils
    info = dict()
    ma = ascii_utils.MayaAscii(filepath)
    ma.read()
    shading_engines = ma.ls(types=['shadingEngine'])
    for index, se in shading_engines: 
        value = ma.getAttr(se, 'objs')
        if value: 
            info[se] = value
    return info

#==================================================================================
#==================================================================================
# --- YML
# from rftool.shade import shade_utils
# reload(shade_utils)
# path = r'C:\Users\nathachon.c\Desktop\shade'

# Lokkdev
# shadeEx = shade_utils.export_shade( r'%s\shade.ma' %path , targetGrp = 'group1')
# shadeData = shade_utils.export_shade_data( r'%s\shade.yml' %path , targetGrp = 'group1')

# Combine
# data = file_utils.ymlLoader( shadeData )
# shade_utils.import_shade( shadeEx , 'shade001' )
# shade_utils.apply_shade( shadeData , 'shade001' , 'shade001' )

#==================================================================================
# --- ATTRIBUTE ON SG
# from rftool.shade import shade_utils
# reload(shade_utils)
# shade_utils.export_shade_with_data('D:/__playground/shd.ma', targetGrp='Geo_Grp')
# shade_utils.apply_ref_shade('D:/__playground/shd.ma', 'shd', 'after_shd1')