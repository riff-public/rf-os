import colorsys
import random
from collections import defaultdict
import os 

import pymel.core as pm
import maya.cmds as mc
import maya.OpenMaya as om

from nuTools.pipeline import pipeTools
# reload(pipeTools)
from rf_maya.rftool.shade import shade_utils
# reload(shade_utils)
from rf_utils.context import context_info
# reload(context_info)

SHADER_TYPE = 'lambert'
ID_ATTR = 'isIntersectShd'

def isIntersectShader(shader):
    return shader.hasAttr(ID_ATTR)

def apply_random_shader(objs=[], all_geo=True, n=6):
    selection = mc.ls(sl=True)
    # if arg objs not provided, get selection
    if not objs:
        objs = [s for s in mc.ls(sl=True, type='transform') if mc.referenceQuery(s, inr=True)]

    if not objs:
        return

    asset = context_info.ContextPathInfo(path=mc.file(q=True, sn=True))
    geoGrpName = asset.projectInfo.asset.geo_grp()

    pm.undoInfo(openChunk=True)

     # collect namespaces
    namespaces = defaultdict(list)  # {namespace: [obj1, obj2, ...]}
    for obj in objs:
        namespace = obj.split('|')[-1].rpartition(':')[0] + ':'
        # if all_geo is set, grow selection to all mesh transform within the reference
        if all_geo:
            if namespace not in namespaces:
                mc.select(obj, r=True)
                objects = pipeTools.convertSelectionToAllGeoInRef(topGrp=geoGrpName, select=False)
                objects = [pm.PyNode(obj) for obj in objects]
                namespaces[namespace].extend(objects)
        else:
            namespaces[namespace].append(pm.PyNode(obj))

    # ---- main loop
    for ns, objs in namespaces.iteritems():
        shadeFile, topGrpName = shade_utils.get_recorded_shade_data(namespace=ns) 
        
        if not shadeFile or not os.path.exists(os.path.expandvars(shadeFile)):
            om.MGlobal.displayWarning('Skipping {}, Cannot get current shader to restore to.'.format(ns))
            continue
            # answer = pm.confirmDialog(title='Warning', 
            #     icon='warning',
            #     message='Cannot find current shader on {}, restoring will not work.\nContintue?'.format(ns[:-1]), 
            #     button=('Yes', 'No'))
            # if answer == 'No':
            #     continue
        
        # collect obj sizes
        obj_sizes = []
        volume_sum = 0
        for obj in objs:
            objShp = obj.getShape()
            # get obj size
            bb = objShp.boundingBox()
            volume = bb.width() * bb.height() * bb.depth()
            volume_sum += volume
            obj_sizes.append((obj, volume))

        # sort object by size
        sorted_obj_size = sorted(obj_sizes, key=lambda x: x[1], reverse=True)
        # get volume mean value
        mean_value = volume_sum / len(objs)

        # get main colors
        main_colors_hsv = [(x*1.0/n, 1.0, 1.0) for x in xrange(n)]
        main_colors = map(lambda x: colorsys.hsv_to_rgb(*x), main_colors_hsv)
        random.shuffle(main_colors)
        main_col_num = len(main_colors)

        # get secondary colors
        colors = []
        ni = 1.0/n
        sec_color_hsv = [((x*ni)+(ni*0.5), random.randrange(25, 50)*0.01, random.randrange(50, 100)*0.01) for x in xrange(n)]
        colors = map(lambda x: colorsys.hsv_to_rgb(*x), sec_color_hsv)
        random.shuffle(colors)

        # create shaders
        shader_sgs = []
        for i, color in enumerate(main_colors + colors):
            # create shader
            shaderName = '{}intersect{}_Shd'.format(ns, i+1)
            shader = pm.shadingNode(SHADER_TYPE, asShader=True, n=shaderName)
            sg = pm.sets(renderable=True, noSurfaceShader=True, empty=True, name='%sSG' %(shaderName))
            pm.connectAttr(shader.outColor, sg.surfaceShader, f=True)

            shader_sgs.append((shader, sg))

            shader.color.set(color)
            pm.addAttr(shader, ln=ID_ATTR, at='bool', dv=True)

        # assign shaders
        # used_shader = set()
        shaders = [s[0] for s in shader_sgs]

        main_shaders = shader_sgs[:main_col_num]
        main_shader_max = len(main_shaders)
        sec_shaders = shader_sgs[main_col_num:]
        sec_shaders_max = len(sec_shaders)
        m, s = 0, 0
        for obj, size in sorted_obj_size:
            # if it's the first half of largest objects, use main colors
            if size >= mean_value:
                shader, sg = main_shaders[m%main_shader_max]
                m += 1
            else:  # else random secondary colors
                shader, sg = sec_shaders[s%sec_shaders_max]
                s += 1

            # assign the shader
            # pm.select(obj, r=True)
            # pm.hyperShade(assign=shader)
            pm.sets(sg, e=True, nw=True, fe=obj)
            # used_shader.add(shader)
            
        # cleanup unused shaders
        cleanup_unused_shaders(namespace=ns)

    # select what was select before
    mc.select(selection, r=True)
    pm.undoInfo(closeChunk=True)

def list_all_intersect_shaders(namespace):
    return [s for s in pm.ls(type=SHADER_TYPE) if isIntersectShader(s) and s.namespace()==namespace and not s.isReferenced()]

def cleanup_unused_shaders(namespace):
    all_shaders = list_all_intersect_shaders(namespace=namespace)
    to_delete = []
    for shader in all_shaders:
        sgs = shader.outputs(type='shadingEngine')
        for sg in sgs:
            members = mc.sets(sg.nodeName(), q=True)
            if members:
                break
        else:
            to_delete.append(shader)
    if to_delete:
        pm.delete(to_delete)
        print 'Deleted {} unused intersect shaders on {}'.format(len(to_delete), namespace)
   
'''
from rf_maya.rftool.shade import intersect_check_shader
reload(intersect_check_shader)

# apply intersect shader
intersect_check_shader.apply_random_shader()

# restore shader to normal
intersect_check_shader.restore()
'''