from collections import OrderedDict
import pymel.core as pm 
from rf_utils import file_utils


def node_tracing(node): 
    node = pm.PyNode(node)
    mtr_dict = OrderedDict()
    connection_dict = OrderedDict()
    mtr_dict[node.name()] = connection_dict
    mtr_dict[node.name()]['nodeType'] = node.nodeType()

    ignore_attrs = ['message']
    nodes = node.listConnections(s=False, d=True, c=True, p=True)
    
    for src_attr_node, dst_attr_node in nodes: 
        if src_attr_node.type() not in ignore_attrs: 
            dst_node = dst_attr_node.node().name()
            if not dst_node in connection_dict.keys(): 
                connection_dict[dst_node] = OrderedDict()
                connection_dict[dst_node]['nodeType'] = dst_attr_node.node().nodeType()
                connection_dict[dst_node]['connections'] = [(src_attr_node.longName(), dst_attr_node.longName())]

            else: connection_dict[dst_node]['connections'].append((src_attr_node.type(), dst_attr_node.type()))

    return mtr_dict


def connection_tracing(node): 
    return pm.PyNode(node).listHistory()


def export_mtr_json(node, dst): 
    mtr_dict = OrderedDict()
    nodes = connection_tracing(node)

    for node in nodes: 
        node_dict = node_tracing(node)
        mtr_dict.update(node_dict)

    return file_utils.json_dumper(dst, mtr_dict)
        