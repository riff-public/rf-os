import os

import maya.cmds as mc
import pymel.core as pm
import maya.mel as mel
import maya.OpenMaya as om

# UI modules
import Qt
from Qt import wrapInstance, QtGui, QtCore, QtWidgets, QtCompat
# monkey patch for Qt compatibility 
if Qt.__binding__ in ('PySide2', 'PyQt5'):
    Qt.QtWidgets.QHeaderView.setResizeMode = Qt.QtWidgets.QHeaderView.setSectionResizeMode


from rf_maya.rftool.utils import maya_utils

def export_for_AE():
    sels = pm.selected()
    cam = None
    for s in sels:
        nodeType = pm.nodeType(s)
        if nodeType == 'transform' and s.getShape(type='camera'):
            cam = s
            break
        elif nodeType == 'camera':
            cam = s.getParent()
            break
    if not cam:
        om.MGlobal.displayError('Select a camera to export!')
        return

    cam_shp = cam.getShape()

    # pop-up for export
    sn = pm.sceneName()
    path = None
    start_dir = ''
    if sn:
        start_dir = os.path.dirname(sn).replace('\\', '/')

    dialog = QtWidgets.QFileDialog()
    dialog.setWindowTitle('Export AE camera to...')
    dialog.setNameFilter('*.ma')
    dialog.setDefaultSuffix('.ma')
    if start_dir:
        dialog.setDirectory(start_dir)
    dialog.setAcceptMode(QtWidgets.QFileDialog.AcceptSave)
    if dialog.exec_() != QtWidgets.QDialog.Accepted:
        return
    path = dialog.selectedFiles()[0]

    # create new camera
    ae_cam_shp = pm.createNode('camera', name='AE_%s' %cam_shp.nodeName().split(':')[-1])
    ae_cam = ae_cam_shp.getParent()
    ae_cam.rename('AE_%s' %cam.nodeName().split(':')[-1])

    # constaint
    cons = pm.parentConstraint(cam, ae_cam)

    # set shape attributes
    for attr in cam_shp.listAttr():
        ln = attr.longName()
        try:
            value = cam_shp.attr(ln).get()
            ae_cam_shp.attr(ln).set(value)
        except:
            continue

    # bake
    start = pm.playbackOptions(q=True, min=True)
    end = pm.playbackOptions(q=True, max=True)
    cmd = '''bakeResults -simulation true 
                -t "{0}:{1}" 
                -sampleBy 1 
                -oversamplingRate 1 
                -disableImplicitControl true 
                -preserveOutsideKeys true 
                -sparseAnimCurveBake false 
                -removeBakedAttributeFromLayer false 
                -removeBakedAnimFromLayer false 
                -bakeOnOverrideLayer false 
                -minimizeRotation true 
                -controlPoints false 
                -shape true "{2}";'''.format(start, end, ae_cam.shortName())
    with maya_utils.FreezeViewport() as isolate:
        mel.eval(cmd)

    # export the camera
    pm.delete(cons)
    pm.select(ae_cam, r=True)
    pm.exportSelected(path, f=True)

    # delete the camera
    pm.delete(ae_cam)

    pm.select(cam, r=True)
    om.MGlobal.displayInfo('AE Camera exported: %s' %path)