import random
import pymel.core as pm
from maya import OpenMaya as om

SHD_NAME = 'ShockFX_Shd'
FX_COLOR = (1, 1, 1)

def set_random_value(min, max, attr_filter=''):
    sels = pm.selected(type='transform')
    if not sels:
        om.MGlobal.displayError('Please select Fx_Ctrl')
        return
    for sel in sels:
        attrs = sel.listAttr(ud=True, s=True, u=True, st=attr_filter)
        for attr in attrs:
            value = random.randint(min, max)
            attr.set(value)

def create_shader():
    if not pm.objExists(SHD_NAME):
        surfaceShader = pm.shadingNode('surfaceShader', n=SHD_NAME, asShader=True)
        surfaceShader.outColor.set(FX_COLOR)
    else:
        surfaceShader = pm.PyNode(SHD_NAME)

    return surfaceShader

def assign_shader():
    sels = pm.selected(type='transform')
    if not sels:
        om.MGlobal.displayError('Please select Fx_Ctrl')
        return
    shader = create_shader()
    for sel in sels:
        namespace = sel.namespace()
        fxGrpName = '{}FxGeo_Grp'.format(namespace)
        if pm.objExists(fxGrpName):
            fxGrp = pm.PyNode(fxGrpName)
            pm.select(fxGrp, r=True)
            pm.hyperShade(assign=shader)