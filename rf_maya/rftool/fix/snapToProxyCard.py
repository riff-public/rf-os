uiName = 'SnapToProxyCardUi'
_title = 'Snap To Proxy Card'
_version = 'v.0.0.1'
_des = 'wip'

import os
from pprint import pprint
import pymel.core as pm
import maya.OpenMaya as om
import maya.mel as mel

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

LIB_DIR = 'P:/GoAwayMrTumor/asset/work/prop/paperPopup/abc'

class Ui(QtWidgets.QWidget):
    def __init__(self, parent = None) :
        super(Ui, self).__init__(parent)
        self.allLayout = QtWidgets.QVBoxLayout()
        self.pathLayout = QtWidgets.QHBoxLayout()
        self.checkBoxLayout = QtWidgets.QHBoxLayout()
        self.allLayout.addLayout(self.pathLayout)
        self.allLayout.addLayout(self.checkBoxLayout)

        self.pathLabel = QtWidgets.QLabel('Root Path: ')
        self.pathLineEdit = QtWidgets.QLineEdit()
        self.pathLayout.addWidget(self.pathLabel)
        self.pathLayout.addWidget(self.pathLineEdit)

        self.snapButton = QtWidgets.QPushButton('Snap')
        self.snapButton.setMinimumSize(QtCore.QSize(50, 50))
        self.allLayout.addWidget(self.snapButton)

        # checkboxes
        self.snapTrCheckBox = QtWidgets.QCheckBox('transforms')
        self.checkBoxLayout.addWidget(self.snapTrCheckBox)

        self.snapBBCheckBox = QtWidgets.QCheckBox('boundingBox')
        self.checkBoxLayout.addWidget(self.snapBBCheckBox)

        self.snapScaleCheckBox = QtWidgets.QCheckBox('scale')
        self.checkBoxLayout.addWidget(self.snapScaleCheckBox)

        self.spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.checkBoxLayout.addItem(self.spacerItem)

        self.setLayout(self.allLayout)

class SnapToProxyCard(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super(SnapToProxyCard, self).__init__(parent)
        self.ui = Ui(parent)
        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)
        self.setWindowTitle('%s %s %s' % (_title, _version, _des))
        self.resize(370, 100)

        self.init_ui()

    def init_ui(self):
        self.ui.pathLineEdit.setText(LIB_DIR)
        self.ui.snapTrCheckBox.setChecked(True)
        self.ui.snapBBCheckBox.setChecked(False)
        self.ui.snapScaleCheckBox.setChecked(True)

        self.ui.snapButton.clicked.connect(self.snap)

    def snap(self):
        sels = pm.selected()
        if not sels:
            om.MGlobal.displayError('Select a proxy card!')
            return 

        doSnapTransform = self.ui.snapTrCheckBox.isChecked()
        doSnapScale = self.ui.snapScaleCheckBox.isChecked()
        doSanpBB = self.ui.snapBBCheckBox.isChecked()
        lib_dir = self.ui.pathLineEdit.text()
        toSels = []
        errors = set()
        for sel in sels:
            # get file path from selected node name
            nodeName = sel.nodeName()
            name = nodeName.split('_')[0]
            name = name.split(':')[-1]
            full_path = lib_dir + '/' + name + '.abc'

            if not os.path.exists(full_path):
                om.MGlobal.displayError('Skipping, Cannot find file: %s from %s' %(full_path, nodeName))
                errors.add(name)
                continue

            # import the file
            ref = pm.createReference(full_path, gl=True, namespace='%s_001' %name, mergeNamespacesOnClash=False)
            nodes = ref.nodes()
            # find the top node polygon and snap to selected object
            topNodeName = '%s_Geo' %(name)
            topNode = [n for n in nodes if pm.nodeType(n)=='transform' and name in n.nodeName()] 
            if topNode:
                ref.importContents(removeNamespace=False)
                newObj = topNode[0]
                toSels.append(newObj)
                old_scale = pm.xform(newObj, q=True, r=True, s=True)

                if doSnapTransform:
                    # snap to pivot using constraint
                    pm.delete(pm.parentConstraint(sel, newObj))

                bb1 = sel.getBoundingBox(space='world')
                bb1_center = bb1.center()
                bb2 = newObj.getBoundingBox(space='world')
                w1, h1, d1 = bb1.width(), bb1.height(), bb1.depth()
                w2, h2, d2 = bb2.width(), bb2.height(), bb2.depth()
                bound_diff = [w1/w2, h1/h2, d1/d2]

                # match scale from bounding box
                if doSnapScale:
                    pm.xform(newObj, scale=[bound_diff[0]*old_scale[0], bound_diff[1]*old_scale[1], old_scale[2]])

                if doSanpBB:
                    # snap center of bb
                    bb2_conter = newObj.getBoundingBox(space='world').center()
                    move_vec = bb1_center - bb2_conter
                    pm.move(newObj, move_vec, r=True)
            else:
                om.MGlobal.displayError('Skipping, Cannot find geo: %s' %topNodeName)
                ref.remove()
                
                continue

        pprint(errors)
        pm.select(toSels)


def show():
    from rftool.utils.ui import maya_win
    maya_win.deleteUI(uiName)
    myApp = SnapToProxyCard(maya_win.getMayaWindow())
    myApp.show()
    return myApp