# work around for non-reference asset 
# give path and asset infomation tag on ply geometry
import os
import sys 
import maya.cmds as mc 
import maya.mel as mm 
from rftool.utils import maya_utils
from rftool.utils import path_info
from functools import partial 
from collections import OrderedDict
import logging
logger = logging.getLogger(__name__)

class Tag: 
    attrs = OrderedDict()
    id = 'id'
    project = 'project'
    assetName = 'assetName'
    path = 'path'
    assetData = 'assetData'
    root = 'root'
    isRoot = 'isRoot'

    attrs[id] = {'data': 'long'}
    attrs[project] = {'data': 'string'}
    attrs[assetName] = {'data': 'string'}
    attrs[path] = {'data': 'string'}
    attrs[assetData] = {'data': 'string'}
    attrs[root] = {'data': 'string'}
    attrs[isRoot] = {'data': 'bool'}


def add_tag(objs): 
    """ add tag to input objects """ 
    for obj in objs: 
        for attr, dataType in Tag.attrs.iteritems(): 
            if not mc.objExists('%s.%s' % (obj, attr)): 
                if dataType.get('data') == 'string': 
                    add_string(obj, attr)

                if dataType.get('data') == 'long': 
                    add_int(obj, attr)

                if dataType.get('data') == 'bool': 
                    add_bool(obj, attr)


def set_root_tag(obj, project='', assetName='', path='', scene=False, autoId=False, id=0, reTag=False): 
    """ use this function to set root tag """ 
    root = obj.split('|')[-1]
    isRoot = True
    objln = mc.ls(obj, l=True)[0]
    if scene: 
        asset = path_info.PathInfo()
        project = asset.project 
        assetName = asset.name
        path = asset.path 

    if project and assetName and path: 
        if autoId: 
            id = find_asset_id(project, assetName)
        add_data(obj, id, project, assetName, path, root, isRoot, reTag=reTag)

        meshes = mc.listRelatives(obj, ad=True, type='mesh', f=True)
        transformMeshes = list(set([mc.listRelatives(a, p=True, f=True)[0] for a in meshes]))

        if objln in transformMeshes: 
            transformMeshes.remove(objln)

        if transformMeshes: 
            for ply in transformMeshes: 
                add_data(ply, id, project, assetName, path, root, isRoot=False, reTag=reTag)

    else: 
        logger.debug('Missing data')
        logger.debug('project : %s' % project)
        logger.debug('assetName : %s' % assetName)
        logger.debug('path : %s' % path)


def find_asset_id(project, assetName): 
    """ find asset id from shotgun """
    from rftool.utils import sg_process
    entity = sg_process.sg.find_one('Asset', [['project.Project.name', 'is', project], ['code', 'is', assetName]], ['code', 'sg_asset_type', 'sg_subtype', 'id'])
    id = 0 
    if entity: 
        id = entity.get('id')
    return id


def add_data(obj, id, project, assetName, path, root, isRoot, reTag=False): 
    add_int(obj, Tag.id, id, reTag=reTag)
    add_string(obj, Tag.project, project, reTag=reTag)
    add_string(obj, Tag.assetName, assetName, reTag=reTag)
    add_string(obj, Tag.path, path, reTag=reTag)
    add_string(obj, Tag.root, root, reTag=reTag)
    add_bool(obj, Tag.isRoot, isRoot, reTag=reTag)


def add_string(obj, attr, value=None, reTag=False): 
    objAttr = '%s.%s' % (obj, attr)
    if reTag: 
        if mc.objExists(objAttr): 
            mc.deleteAttr(obj, at=attr)
    if not mc.objExists(objAttr): 
        mc.addAttr(obj, ln=attr, dt='string')
        mc.setAttr('%s.%s' % (obj, attr), e=True, keyable=True)
    if value: 
        mc.setAttr(objAttr, value, type='string')

    return objAttr

def add_int(obj, attr, value=None, reTag=False, keyable=False): 
    objAttr = '%s.%s' % (obj, attr)
    if reTag: 
        if mc.objExists(objAttr): 
            mc.deleteAttr(obj, at=attr)
    if not mc.objExists(objAttr): 
        mc.addAttr(obj, ln=attr, at='long', min=0, dv=0)
        mc.setAttr('%s.%s' % (obj, attr), e=True, keyable=keyable)
    if value: 
        mc.setAttr(objAttr, value)

    return objAttr

def add_bool(obj, attr, value=None, reTag=False, keyable=False): 
    objAttr = '%s.%s' % (obj, attr)
    if reTag: 
        if mc.objExists(objAttr): 
            mc.deleteAttr(obj, at=attr)
    if not mc.objExists(objAttr): 
        mc.addAttr(obj, ln=attr, at='bool')
        mc.setAttr('%s.%s' % (obj, attr), e=True, keyable=keyable)
    if value: 
        mc.setAttr(objAttr, value)

    return objAttr


def list_polytag(transforms=None): 
    meshTransforms = []
    info = dict()
    
    if transforms: 
        meshTransforms = transforms
    if not transforms: 
        meshes = mc.ls(type='mesh')

    if not meshTransforms: 
        meshTransforms = [mc.listRelatives(a, p=True, f=True)[0] for a in meshes if mc.listRelatives(a, p=True)]
        
    if meshTransforms: 
        for mesh in meshTransforms: 
            if check_attr(mesh): 
                root = find_root(mesh)
                
                if root: 
                    id, project, assetName, path = get_attr(root)
                    asset = path_info.PathInfo(path=path)

                    if not assetName in info.keys(): 
                        info[assetName] = [asset, [root]]

                    else: 
                        if not root in info[assetName][1]: 
                            info[assetName][1].append(root)

    return info


def switch_selection(level): 
    sels = mc.ls(sl=True)
    info = list_polytag(transforms=sels)
    print info 

    if sels and info: 
        for assetName, values in info.iteritems(): 
            asset, plys = values
            result = switch(plys, level, mode='duplicate')

            if len(plys) == len(result): 
                print 'switch %s success %s' % (assetName, plys)

    mm.eval('hyperShadePanelMenuCommand("hyperShadePanel1", "deleteUnusedNodes");')


def switch(roots, level, mode='normal'): 
    """ switch given plys to different level """ 
    inputPly = None 
    success = []

    for i, root in enumerate(roots): 
        id, project, assetName, path = get_attr(root)
        asset = path_info.PathInfo(path=path)
        refPath = '%s/%s_%s' % (asset.libPath(), asset.name, level)

        if os.path.exists(refPath): 
            plcAsset = place_asset(refPath, inputPly=inputPly)
            transfer_attr(root, plcAsset, refPath)
            plcAsset = snap_target(plcAsset, root)
            mc.delete(root)
            rootSn = root.split('|')[-1]
            plcAsset = rename_node(plcAsset, rootSn)

            if i == 0 and mode == 'duplicate': 
                inputPly = plcAsset

            success.append(root)

    return success


def place_asset(refPath, inputPly=None): 
    """ if inputPly, use inputPly to duplicate """ 
    if not inputPly: 
        placeAssets = import_asset(refPath)

    if inputPly: 
        placeAssets = mc.duplicate(inputPly)[0]
        placeAssets = mc.parent(placeAssets, w=True)[0]

    return placeAssets


def import_asset(refPath): 
    """ import asset and findRoot """ 
    current = mc.ls(assemblies=True)
    ext = os.path.splitext(refPath)[-1]

    if ext == '.ma': 
        mc.file(refPath, i=True)
        placeAssets = [a for a in mc.ls(assemblies=True) if not a in current]

        # in the pipeline, only Rig_Grp 
        if len(placeAssets) == 1: 
            root = None 
            childs = mc.listRelatives(placeAssets[0], ad=True, f=True)
            for child in childs: 
                if mc.objExists('%s.%s' % (child, Tag.isRoot)): 
                    isRoot = mc.getAttr('%s.%s' % (child, Tag.isRoot))
                    if isRoot: 
                        root = mc.parent(child, w=True)[0]
                        break

            # polytag format
            if root: 
                mc.delete(placeAssets)
                return root

            # outside polytag
            else: 
                plys = maya_utils.find_ply(placeAssets[0], f=True)

                if len(plys) == 1: 
                    placeAsset = mc.parent(plys[0], w=True)[0]

                if len(plys) > 1: 
                    placeAsset = mc.polyUnite(plys, mergeUVSets=True, ch=False, n='tmpAsset')

                mc.delete(placeAssets)
                return placeAsset


    if ext == '.abc': 
        gpuNode = mc.createNode('gpuCache')
        mc.setAttr('%s.cacheFileName' % gpuNode, refPath, type='string')
        transform = mc.listRelatives(gpuNode, p=True)[0]
        return transform


def snap_target(srcPly, target): 
    print 'snap'
    print srcPly, target
    mc.delete(mc.parentConstraint(target, srcPly))
    mc.delete(mc.scaleConstraint(target, srcPly))
    targetOrder = maya_utils.outliner_order(target)

    targetParent = mc.listRelatives(target, p=True)
    srcParent = mc.listRelatives(srcPly, p=True)

    if not srcParent == targetParent: 
        # result = mc.parent(srcPly, targetParent)[0]
        result = maya_utils.parent_order(srcPly, targetParent, targetOrder)[0]
        # print result
        return result 

def rename_node(src, dst): 
    result = mc.rename(src, dst)
    # check if gpu in dst 
    shape = mc.listRelatives(result, s=True, f=True)

    if shape: 
        if mc.objectType(shape[0], isType='gpuCache'): 
            mc.rename(shape[0], '%s_gpu' % dst)

    return result


def is_root(ply): 
    """ check if this is a root """ 
    if mc.objExists('%s.%s' % (ply, Tag.isRoot)): 
        isRoot = mc.getAttr('%s.%s' % (ply, Tag.isRoot))
        if isRoot: 
            if mc.objExists('%s.%s' % (ply, Tag.assetName)) and mc.objExists('%s.%s' % (ply, Tag.path)): 
                return True



def find_root(ply): 
    plyln = mc.ls(ply, l=True)[0]
    plyList = plyln.split('|')

    for i, ply in enumerate(plyList): 
        if i == 0: 
            cmbList = plyList
        else: 
            cmbList = plyList[:-i]
        ln = ('|').join(cmbList) # long name
        sn = cmbList[-1] # short name 
        # root = mc.getAttr('%s.%s' % (ln, Tag.root))

        if is_root(ln): 
            return ln

def get_attr(ply): 
    id = mc.getAttr('%s.%s' % (ply, Tag.id))
    assetName = mc.getAttr('%s.%s' % (ply, Tag.assetName))
    path = mc.getAttr('%s.%s' % (ply, Tag.path))
    project = mc.getAttr('%s.%s' % (ply, Tag.project))

    return id, project, assetName, path

def transfer_attr(srcObj, dstObj, path): 
    isRoot = True
    id = mc.getAttr('%s.%s' % (srcObj, Tag.id))
    project = mc.getAttr('%s.%s' % (srcObj, Tag.project))
    assetName = mc.getAttr('%s.%s' % (srcObj, Tag.assetName)) 
    root = dstObj
    add_data(dstObj, id, project, assetName, path, root, isRoot, reTag=False)


def check_attr(ply): 
    projectAttr = '%s.%s' % (ply, Tag.project)
    assetAttr = '%s.%s' % (ply, Tag.assetName)
    pathAttr = '%s.%s' % (ply, Tag.path)
    isRootAttr = '%s.%s' % (ply, Tag.isRoot)

    if mc.objExists(projectAttr) and mc.objExists(assetAttr) and mc.objExists(pathAttr) and mc.objExists(isRootAttr): 
        return True


def select_tag(tag=False): 
    plys = mc.ls(type='mesh')

    if plys: 
        transforms = [mc.listRelatives(a, p=True)[0] for a in plys]
    if not tag: 
        noTagPlys = [a for a in transforms if not check_attr(a)]
        if noTagPlys: 
            mc.select(noTagPlys)

    if tag: 
        tagPlys = [a for a in transforms if check_attr(a)]
        if tagPlys: 
            mc.select(tagPlys)


def assign_tag(): 
    geoGrp = 'Geo_Grp'

    if mc.objExists(geoGrp): 
        childs = mc.listRelatives(geoGrp, c=True)
        childs = [a for a in childs if not 'Constraint' in mc.objectType(a)]

        if len(childs) == 1: 
            set_root_tag(childs[0], scene=True, reTag=True, autoId=True)

        else: 
            logger.warning('childs more than 1')



def polytag_to_sel(assetName=None): 
    sels = mc.ls(sl=True)
    delGrp = 'deleteGrp'

    if len(sels) > 1: 
        src = sels[0]
        targets = sels[1:]
        if not assetName: 
            id, project, assetName, path = get_attr(src)
        name = '%s_001' % assetName
        assetGrp = '%s_Grp' % assetName

        for i, target in enumerate(targets): 
            newName = maya_utils.get_asset_name(name)
            dup = mc.duplicate(src)[0]
            dup = mc.rename(dup, newName)
            mc.delete(mc.parentConstraint(target, dup))
            # mc.delete(mc.scaleConstraint(target, dup))
            
            if not mc.objExists(assetGrp): 
                assetGrp = mc.group(n=assetGrp, em=True)

            if not mc.objExists(delGrp): 
                delGrp = mc.group(n=delGrp, em=True)

            mc.parent(target, delGrp)
            mc.parent(dup, assetGrp)

