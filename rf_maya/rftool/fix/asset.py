import os
import sys
import maya.cmds as mc
from rf_utils import file_utils

def find_assets(assetRoot='', project=''):
	assetPaths = []
	assetRoot = '%s/%s/asset' % (assetRoot, project)
	types = file_utils.list_folder(assetRoot)

	for assetType in types:
		typePath = '%s/%s' % (assetRoot, assetType)
		subtypes = file_utils.list_folder(typePath)

		for subtype in subtypes:
			subtypePath = '%s/%s' % (typePath, subtype)
			assets = file_utils.list_folder(subtypePath)

			for asset in assets:
				assetPath = '%s/%s' % (subtypePath, asset)
				assetRel = assetPath.replace(assetRoot, '')
				if not assetRel in assetPaths:
					assetPaths.append(assetRel)

	return assetPaths

def get_from_server(collectRoot='', project='', keys=['rigRen_md', 'model_md']):
	assets = find_assets(collectRoot, project)
	serverRoot = 'P:/Two_Heroes/asset'
	collectFiles = []

	for asset in assets:
		heroPath = '%s/%s/lib' % (serverRoot, asset)

		files = file_utils.list_file(heroPath)

		for eachFile in files:
			if any(a in eachFile for a in keys):
				targetPath = '%s/%s' % (heroPath, eachFile)
				collectFiles.append(targetPath)

	return collectFiles

def copy(collectRoot, project, keys=['rigRen_md', 'model_md']):
	collectFiles = get_from_server(collectRoot, project, keys)

	for i, src in enumerate(collectFiles):
		drive, path = os.path.splitdrive(src)
		dst = '%s%s' % (collectRoot, path)
		if not os.path.exists(dst):
			file_utils.copy(src, dst)
			print 'copy %s/%s %s' % (i, len(collectFiles), dst)
		# dst = '%s/%s' % ()




# root = 'N:/Staff/_outsource/200718/Two_Heroes/asset'
# get_from_server(root)
