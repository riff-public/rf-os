import sys
import os
from rf_utils import file_utils
from rf_utils import log_utils
import logging 
logFile = 'E:/log/remove_plugin.txt'
logger = log_utils.init_logger(logFile, 'removePlugin')

keywordMap = {'mentalRay': 'requires -nodeType "mentalrayFramebuffer" -nodeType "mentalrayOptions" -nodeType "mentalrayGlobals"'}

# requires -nodeType "mentalrayFramebuffer" -nodeType "mentalrayOptions" -nodeType "mentalrayGlobals" ostBaby_med
# 		 -nodeType "mentalrayItemsList" -dataType "byteArray" "Mayatomr" "2015.0 - 3.12.1.12 ";

# requires -nodeType "mentalrayFramebuffer" -nodeType "mentalrayOptions" -nodeType "mentalrayGlobals"
# 		 -nodeType "mentalrayItemsList" -dataType "byteArray" "Mayatomr" "2015.0 - 3.12.1.18 ";

def remove_plugin(maFile, keyword): 
    result = backup(maFile)
    remove(maFile, keyword)

def backup(maFile): 

    root = 'G:/3D Asset/TD_Storage'
    child = maFile.replace('P:', '')
    backupDst = '%s%s' % (root,child)

    file_utils.copy(maFile, backupDst)
    logger.info('backuped %s' % backupDst)

    #shutil.copy2(maFile, backupDst)

def remove(maFile, keyword):
    f = open(maFile, 'r')
    data = f.read()
    f.close()
    removeKey = keywordMap[keyword]

    if removeKey in data:
        start = data.find(removeKey)
        end = data.find(';', start)
        search = data[start: end+1]
        
        replaceData = data.replace(search, '')
        
        f = open(maFile, 'w')
        f.write(replaceData)
        f.close()
    logger.info('removed complete %s' % maFile)   
    return maFile

