import sys
import os

from rf_utils.sg import sg_process
reload(sg_process)
# wip
# review

# create version
# upload files
# set path
# set status


def send_shot_review(projectName, episode, sequence, shot, version, task, status, mediaFile, thumbnailFile, user=None, step=None):
    print projectName, episode, sequence, shot
    shotName = get_versionName(projectName, episode, sequence, shot)
    projectEntity = sg_process.get_project(projectName)
    entity = sg_process.get_shot_entity(projectName, shotName)
    taskEntity = sg_process.get_one_task(entity, task)
    userEntity = sg_process.get_local_user(user)
    versionName = get_versionName(projectName, episode, sequence, shot, version)
    description = ''
    playlistEntity = sg_process.daily_playlist(projectName, task)
    versionEntity = sg_process.update_version(projectEntity, entity, taskEntity, userEntity, versionName, status, description, playlistEntity, step=step, versionType='Daily')

    sg_process.upload_version_media(versionEntity, mediaFile)
    # sg_process.update_entity_thumbnail('Version', versionEntity['id'], thumbnailFile)

def get_versionName(project, episode, sequence, shot, version=None):
    projectEntity = sg_process.sg.find_one('Project', [['name', 'is', project]], ['sg_project_code'])
    projectCode = projectEntity.get('sg_project_code')
    names = [projectCode, episode, sequence, shot]
    if version:
        names.append(version)

    name = ('_').join(names)
    return name
