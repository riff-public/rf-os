# This is for batch cleaning maya 27 Jun 2020 Mulware 
import maya.cmds as mc 
import os 
import sys 
from rf_utils import file_utils
from collections import OrderedDict
import subprocess
import logging 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
import MayaScannerCleaner


def create_summary(dirname): 
    ymlfile = '%s/%s' % (dirname, 'summary.yml')
    targetExt = ['.mb']
    data = OrderedDict()

    for root, dirs, files in os.walk(dirname): 
        for file in files: 
            filename = '%s/%s' % (root, file)
            if os.path.splitext(file)[-1] in targetExt: 
                data[str(filename.replace('\\', '/'))] = 'Not scan'

    file_utils.ymlDumper(ymlfile, data)


def clean_dir(dirname, backup_dir): 
    summary_file = '%s/clean_summary.yml' % backup_dir
    sum_dict = OrderedDict()
    sum_dict['success'] = list()
    sum_dict['failed'] = list()
    targetExt = ['.ma']
    totals = []
    
    for root, dirs, files in os.walk(dirname): 
        for file in files: 
            base, ext = os.path.splitext(file)

            if ext in targetExt: 
                filename = '%s/%s' % (root, file)
                result = clean_maya_ascii(filename, backup_dir)

                if result: 
                    sum_dict['success'].append(filename)
                    logger.info('Cleaned %s' % filename)
                else: 
                    sum_dict['failed'].append(filename)
                    logger.warning('Failed %s' % filename)
            totals.append(filename)

    file_utils.ymlDumper(summary_file, sum_dict)
    logger.info('Total %s files' % len(totals))
    logger.info('success %s files' % len(sum_dict['success']))
    logger.info('failed %s files' % len(sum_dict['failed']))


def clean_from_summary(ymlfile='S:/2020/08_Chronicle_of_Infinity/summary.yml', limit=0): 
    data = file_utils.ymlLoader(ymlfile)
    i = 0 
    for filename, status in data.items(): 
        if not status == 'Clean': 
            found, fixed = clean_maya_mb(filename)
            if found == fixed: 
                data[filename] = 'Clean'
            else: 
                data[filename] = 'failed'
            file_utils.ymlDumper(ymlfile, data)
            if limit > 0: 
                if i >= limit: 
                    continue 
            i+=1 


def clean_maya_mb(filename): 
    mc.file(filename, o=True, executeScriptNodes=False, f=True, loadNoReferences=True, prompt=False) 
    found, fixed = MayaScannerCleaner.clean_malware()
    return found, fixed


def clean_maya_ascii(filename, backup_dir): 
    """ clean using maya ascii reader and remove script nodes """ 
    from rf_utils import ascii_utils
    reload(ascii_utils)

    # copy file to backup dir 
    # copy target file to backup, clean from a backup and override original file 
    backup_dst = '%s/%s' % (backup_dir, os.path.splitdrive(filename)[-1])
    if not os.path.exists(os.path.dirname(backup_dst)): 
        os.makedirs(os.path.dirname(backup_dst))

    backup_path = copy_file(filename, backup_dst)

    if backup_path: 
        path = backup_path 
        output_path = filename
        search = '.*MayaMelUIConfigurationFile.*' # << edit here, search = regular expression
        types = ['script'] # << edit here, file types

        maya_scene = ascii_utils.MayaAscii(path)
        maya_scene.read()

        maya_scene.ls(types=['script'])

        maya_scene.delete_node(search=search, types=types) 
        maya_scene.write(output_path)

        return True 

    return False 


def copy_file(src, dst):
    """ xcopy using os copy """ 
    src = src.replace('/', '\\')
    dst = dst.replace('/', '\\')
    cmd = "echo f | copy \"{src}\" \"{dst}\"".format(src=src, dst=dst)
    ps = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    output = ps.communicate()[0]
    print (output)
    print src, dst
    # print "output", output
    if os.path.exists(dst):
        return dst
    else: 
        logger.debug('Failed to copy to "%s"' % dst)