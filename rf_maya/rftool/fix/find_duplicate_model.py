from collections import defaultdict

import pymel.core as pm
import maya.cmds as mc
from maya.api import OpenMaya as om

def main():
    """ Check same geos sitting exactly at the same position """
    selections = pm.selected(type='transform')
    if not selections:
        om.MGlobal.displayError('Select something!')
        return

    # create MSelectionList
    mSel = om.MSelectionList()
    for s in selections:
        shp = s.getShape(ni=True)
        mSel.add(shp.shortName())
    bbs = defaultdict(list)
    for i in xrange(mSel.length()):
        mDag = mSel.getDagPath(i)
        incMatrix = mDag.inclusiveMatrix()
        dagNode = om.MFnDagNode(mDag)
        bb = dagNode.boundingBox
        bb.transformUsing(incMatrix)
        bb_vals = (tuple(bb.min), tuple(bb.max))
        bbs[bb_vals].append(mDag.partialPathName())

    same_bb = []
    for bb, names in bbs.iteritems():
        if len(names) > 1:
            for n in names[1:]:
                node = pm.PyNode(n)
                tr = node.getParent()
                same_bb.append(tr)
    if same_bb:
        pm.select(same_bb)
    else:
        print 'No duplicate geo found at the same place.'

