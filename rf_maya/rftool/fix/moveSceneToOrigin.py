import os
import sys

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import pymel.core as pm
import maya.cmds as mc

from rf_utils import cask
from rf_utils.context import context_info
from rf_utils import register_shot
from rf_utils import register_entity
from rf_maya.rftool.utils import abc_utils
reload(abc_utils)
from rf_maya.contextMenu.dag import shade_menu
from rf_maya.rftool.utils import maya_utils
from rf_maya.rftool.groom import attach_yeti
reload(attach_yeti)
from rf_utils.pipeline import shot_data
reload(shot_data)
from rf_utils import file_utils
from rf_maya.lib import sequencer_lib

CAM_DIST_KEY = 'cameraDistanceFromOrigin'
CAM_TRANSFORMS_KEY = 'cameraTransforms'

def main(offset=None):
    '''
    from rf_maya.rftool.fix import moveSceneToOrigin as msto
    reload(msto)

    msto.main()

    '''
    obj = None
    if not offset:
        sels = pm.selected()
        if not sels or len(sels) != 1:
            logger.warning('No selection, using (0, 0, 0) as offset.')
            offset = pm.dt.Vector([0.0, 0.0, 0.0])
        else:
            obj = sels[0]
            offset = pm.dt.Vector(pm.xform(obj, q=True, ws=True, t=True))*-1

    if not isinstance(offset, pm.dt.Vector):
        offset = pm.dt.Vector(offset)

    try:
        scene = context_info.ContextPathInfo(path=mc.file(q=True, sn=True)) 
    except:
        msg = 'Invalid scene path.'
        logger.warning(msg)
        return False, msg

    # switch to masterLayer
    defaultRenderLayer = pm.PyNode('defaultRenderLayer')
    defaultRenderLayer.setCurrent()

    # list all top groups
    objToMoves = [g for g in pm.ls(assemblies=True) if not g.getShape(type='camera') and g != obj]

    # check for connections
    connected_objs = []
    for obj in objToMoves:
        if obj.t.inputs():
            connected_objs.append(obj)
            continue
        for axis in 'xyz':
            attr = obj.attr('t%s' %axis)
            if attr.inputs():
                connected_objs.append(obj)
                break
    if connected_objs:
        logger.error('Please move/remove keys or connections on these object!\n%s' %('\n'.join([c.shortName() for c in connected_objs])))
        pm.select(connected_objs, r=True)
        return

    # popup confirm dialog
    if not mc.about(batch=True):
        confirmResult = pm.confirmDialog(title='moveSceneToOrigin', message='Move scene with offset: %s, %s, %s' %(offset[0], offset[1], offset[2]), 
            button=('Yes', 'No'))
        if confirmResult == 'No':
            return

    # list geo groups
    geoGrpName = scene.projectInfo.asset.geo_grp()
    # groomGrp = scene.projectInfo.asset.groom_grp()
    groomGeoGrpName = scene.projectInfo.asset.groom_geo_grp()
    groups = pm.ls('*:%s' %geoGrpName, type='transform')
    yetiNodes = pm.ls(type='pgYetiMaya')

    # collect group of yeti furs
    geoGrps = {}  # {ns:(geoGrp, yetiNode), ...}
    for grp in groups:
        # logger.info('Attaching Yeti hero: %s' %yn.nodeName())
        ns = grp.namespace()
        if not ns:
            continue
        ns = ns[:-1]
        geoGrps[ns] = grp

    offsetVec = offset - pm.dt.Vector([0, 0, 0])
    doMove = True if offsetVec.length() > 0.0 else False

    if doMove == False:
        logger.info('doMove False, using ws_data as offset.')
        ws_data_path = None
        sceneWsData = scene.copy()
        sceneWsData.context.update(step = 'anim', process = 'main')
        try:
            ws_data_path = shot_data.get_workspace_data_path(entity=sceneWsData)
        except Exception as e:
            logger.warning('Error getting workspace path')

        # get offset from shot ws_data
        ws_data = None
        if ws_data_path and os.path.exists(ws_data_path):
            ws_data = file_utils.ymlLoader(ws_data_path)
            logger.info('Workspace data path: %s' %(ws_data_path))
            if CAM_DIST_KEY in ws_data.keys():
                offset = ws_data[CAM_DIST_KEY]
                offset = [round(offset[0], 2)*-1, round(offset[1], 2)*-1, round(offset[2], 2)*-1]
                offsetVec = offset - pm.dt.Vector([0, 0, 0])
                doMove = True if offsetVec.length() > 0.0 else False


    # bring in groom hero and attach with TechGeo ABC
    attached, nss = [], []
    for ns, geoGrp in geoGrps.iteritems():
        result, msg = attach_yeti.attach_yeti_to_cache(obj=geoGrp)
        if not result:
            # logger.warning('%s: %s' %(ns, msg))
            continue

        logger.info('%s: Attached' %(ns))
        attached.append(geoGrp)
        nss.append(ns)

        if doMove:
            # make sure to turn off inheritsTransform on YetiNode and geoRefObject
            techGeoGrp = pm.PyNode(msg)
            yetiGrp = techGeoGrp.getChildren()[0]
            yetiGrp.inheritsTransform.set(True)
            for c in yetiGrp.getChildren(type='transform'):
                cName = c.nodeName()
                if cName.endswith(groomGeoGrpName):
                    c.inheritsTransform.set(True)
                else:
                    c.inheritsTransform.set(False)

            # remove cause tech_geo already cahce at origin

            # techGeoGrpParent = techGeoGrp.getParent()
            # if not techGeoGrpParent.getParent():
            #     objToMoves.append(techGeoGrpParent)


    if objToMoves and doMove:
        # move it
        logger.info('Objects to move: %s' %[n.shortName() for n in objToMoves])
        pm.xform(objToMoves, r=True, ws=True, t=offset)

    # hide YetiMesh_Grp
    for ns in nss:
        groomGeoGrp = pm.ls('%s:%s' %(ns, groomGeoGrpName))
        if groomGeoGrp: 
            for grp in groomGeoGrp: 
                logger.info('Set hidden: %s' %grp.nodeName())
                grp.visibility.set(False)

    # delete caches
    for yn in yetiNodes:
        try:
            if yn.fileMode.get() == 1:
                pm.delete(yn.getParent())
        except:
            pass
            # logger.info('Set hidden: %s' %yn.nodeName())
            # yn.getParent().visibility.set(False)

    # fix shade
    rigGrpNames = [n.shortName() for n in attached]
    # print rigGrpNames
    logger.info('Fixing shade: %s' %rigGrpNames)
    with maya_utils.FreezeViewport() as fw:
        shade_menu.fix_shade(rigGrps=rigGrpNames)
    logger.info('Fix shade done!')
    logger.info('Scene moved with offset: %s, %s, %s' %(offset[0], offset[1], offset[2]))

def get_cameraDistanceFromOrigin(entity):
    ''' get camera boundingBox center and all transforms from the world origin to the camera '''

    # get workspace data yml
    ws_data_path = None 
    try:
        ws_data_path = shot_data.get_shot_data_path(entity=entity)
    except Exception as e:
        logger.warning('Error getting workspace path')

    # update context to main for main_cam
    entity.context.update(look='main')

    # get offsets from shot ws_data
    ws_data = None
    offsets = []
    cam_center = []
    startFrame, endFrame, seqStartFrame, seqEndFrame = sequencer_lib.shot_info(entity.name_code)
    duration = int(endFrame - 1000)

    if ws_data_path and os.path.exists(ws_data_path):
        ws_data = file_utils.ymlLoader(ws_data_path)
        logger.info('Workspace data path: %s' %(ws_data_path))
        if CAM_DIST_KEY in ws_data:
            logger.info('Getting camera boundingBox center from YML...')
            cam_center = ws_data[CAM_DIST_KEY]

        if CAM_TRANSFORMS_KEY in ws_data:
            logger.info('Getting camera offsets from YML...')
            offsets = ws_data[CAM_TRANSFORMS_KEY]

    # if not exists, get camera ABC and extract position
    if not offsets or not cam_center or duration != len(offsets):
        logger.info('Extracting camera offsets from ABC...')
        shot_hero_path = entity.path.hero().abs_path()
        cam_hero_file = entity.output_name(outputKey='camHero', hero=True, ext='.abc')
        cam_hero_path = '%s/%s' %(shot_hero_path, cam_hero_file)

        if not os.path.exists(cam_hero_path):
            logger.error('Cannot find cam ABC: %s' %cam_hero_path)
            return

        try:
            offsets = abc_utils.get_positions_from_abc(nodeName='.*camshot', abcPath=cam_hero_path)
            # round the precision to 2 digits
            offsets = [(round(f[0], 2), round(f[1], 2), round(f[2], 2)) for f in offsets]
        except Exception as e:
            logger.error('Error extracting cam positon from ABC: %s' %cam_hero_path)
            logger.error(e)
            return

        # calculate center of movement's boundingBox
        if not cam_center:
            pts_x = [f[0] for f in offsets]
            pts_y = [f[1] for f in offsets]
            pts_z = [f[2] for f in offsets]
            cam_move_min = pm.dt.Point(min(pts_x), min(pts_y), min(pts_z))
            cam_move_max = pm.dt.Point(max(pts_x), max(pts_y), max(pts_z))
            cam_move_bb = pm.dt.BoundingBox(cam_move_min, cam_move_max)
            cam_center = cam_move_bb.center()
            cam_center = [round(cam_center[0], 2), round(cam_center[1], 2), round(cam_center[2], 2)]

        # update shot data with extracted offsets
        if not ws_data:
            ws_data = {CAM_DIST_KEY: cam_center, CAM_TRANSFORMS_KEY: offsets}
        else:
            ws_data[CAM_DIST_KEY] = cam_center
            ws_data[CAM_TRANSFORMS_KEY] = offsets

        # update data missing
        if duration != len(offsets):
            lastOffset = ws_data[CAM_TRANSFORMS_KEY][-1]
            missing = (lastOffset[0], lastOffset[1], lastOffset[2])
            numLoop = int(duration - len(offsets))
            for num in range(numLoop):
                ws_data[CAM_TRANSFORMS_KEY].append(missing)

        # write YML
        data_dir = os.path.dirname(ws_data_path)
        if not os.path.exists(data_dir):
            os.makedirs(data_dir)
        file_utils.ymlDumper(ws_data_path, ws_data)
        logger.info('Update data to YML: %s' %ws_data_path)

    logger.info('Camera boundingBox center: %s' %(cam_center))
    logger.info('Camera positions: %s' %(len(offsets)))
    return cam_center, offsets

def offset_asset(obj, offset, toOrigin=True, isCache=False):
    ''' Offset all rigs in the scene '''
    
    logger.info('Offseting %s with offset %s' %(obj, offset))
    
    # make sure offset is pymel Vector
    offset = pm.dt.Vector(offset)

    # if shifting to origin, offset value needs to be inverted. 
    if toOrigin:
        offset *= -1

    # get object transform
    obj_curr_tr = pm.xform(obj, q=True, ws=True, t=True)

    # if it's a cache
    if isCache:
        # abort if shifting back near origin with Geo_Grp is already zeroed out
        if toOrigin and obj_curr_tr == [0.0, 0.0, 0.0]:
            logger.info('Cache is already near origin: %s' %obj)
            return

        # about if shifting away from origin with Geo_Grp has translation values
        elif not toOrigin and obj_curr_tr != [0.0, 0.0, 0.0]:
            logger.info('Cache has already been shifted: %s' %obj)
            return

    else:  # moving rig asset
        # abort if shifting rig back near origin with AllMover_Grp has translation values
        if toOrigin and obj_curr_tr != [0.0, 0.0, 0.0]:
            logger.info('Rig is already near origin: %s' %obj)
            return
        # abourt if shifting rig away from origin and AllMover_Grp is already zeroed out
        elif not toOrigin and obj_curr_tr == [0.0, 0.0, 0.0]:
            logger.info('Rig has already been shifted: %s' %obj)
            return
            
    # move it...
    # print pm.dt.Point(obj_curr_tr) + offset
    pNode = pm.PyNode(obj)
    pNode.attr('t').unlock()
    for ax in 'xyz':
        pNode.attr('t'+ax).unlock()
    pm.xform(obj, a=True, ws=True, t=pm.dt.Point(obj_curr_tr) + offset)
    pNode.attr('t').lock()
    for ax in 'xyz':
        pNode.attr('t'+ax).lock()

    logger.info('Asset moved with offset: %s, %s, %s' %(offset[0], offset[1], offset[2]))

