import os
import json
import shutil

from rf_utils import file_utils

def get_existing_asm(rootPath):
    result = []
    for asset in os.listdir(rootPath):
        # print asset
        heroPath = os.path.join(rootPath, asset, 'hero')
        if os.path.exists(heroPath):
            fn = '%s_set_main_md.hero.asm' %(asset)
            fp = os.path.join(heroPath, fn)
            if os.path.exists(fp):
                result.append(fp)
            else:
                print 'No asm file found in: %s' %heroPath
        else:
            print 'No hero folder for set: %s' %asset

    return result

def main(rootPath):
    asm_paths = get_existing_asm(rootPath)
    mydocs = os.environ['HOME']
    set_asm_bkup = os.path.join(mydocs, 'asm_backup')
    if not os.path.exists(set_asm_bkup):
        os.mkdir(set_asm_bkup)
    for asm_path in asm_paths:
        baseName = os.path.basename(asm_path)
        shutil.copy(asm_path, os.path.join(set_asm_bkup, baseName))

        # back up as YML
        path, ext = os.path.splitext(asm_path)
        yml_path = path + '.yml'

        shutil.copy(asm_path, yml_path)

        # # convert
        print 'Reading YML: %s' %(asm_path)
        data = file_utils.ymlLoader(asm_path)

        print 'Converting JSON: %s' %(asm_path)
        file_utils.json_dumper(path=asm_path, data=data)

if __name__ == '__main__':
    main(rootPath='P:\\SevenChickMovie\\asset\\publ\\set')
