import maya.cmds as mc 

def fix_neck(): 
	sels = mc.ls(sl=True)
	if sels: 
		target = sels[0]
		loc = mc.spaceLocator()
		mc.parentConstraint(target, loc)
		mc.scaleConstraint(target, loc)

		start = mc.playbackOptions(q=True, min=True)
		end = mc.playbackOptions(q=True, max=True)
		mc.bakeResults(loc, simulation=True, t=(start, end), sampleBy=1, oversamplingRate=1, disableImplicitControl=True, preserveOutsideKeys=True, sparseAnimCurveBake=False, removeBakedAttributeFromLayer=False, removeBakedAnimFromLayer=False, bakeOnOverrideLayer=False, minimizeRotation=True, controlPoints=False, shape=True)

		# delete Key 
		mc.select(target)
		mc.cutKey(cl=True)

		# constraint back 
		mc.parentConstraint(loc, target, mo=True)
		mc.scaleConstraint(loc, target, mo=True)
