import os 
import sys 
import maya.cmds as mc 
from rf_utils import file_utils
from rf_maya.rftool.utils import maya_utils



def batch_convert(dirname, type='FBX'): 
	ext = '.fbx' if type == 'FBX' else '.ma'
	list_fbx = [a for a in file_utils.listFile(dirname) if os.path.splitext(a)[-1] == ext]
	gpus = []
	for fbx in list_fbx: 
		fbxpath = '{}/{}'.format(dirname, fbx)
		gpu = convert_fbx_to_gpu(fbxpath, type=type)


def convert_fbx_to_gpu(fbxpath, type='FBX'): 
	dirname = os.path.dirname(fbxpath)
	gpu_dir = '{}/gpu'.format(dirname)
	filename = os.path.basename(fbxpath)
	gpuname = '{}'.format(os.path.splitext(filename)[0])

	if not os.path.exists(gpu_dir): 
		os.makedirs(gpu_dir)

	# mc.file(f=True, new=True)
	mc.file(fbxpath, o=True, f=True)

	# mc.file(fbxpath, i=True, type=type, ignoreVersion=True, ra=True, mergeNamespacesOnClash=False)
	try: 
		mc.gpuCache(startTime=1, 
		endTime=1, 
		optimize=True, 
		optimizationThreshold=40000, 
		writeMaterials=True, 
		dataFormat='ogawa', 
		directory=gpu_dir, 
		fileName=gpuname, 
		allDagObjects=True
		)
	except: 
		print gpuname
	return '{}/{}.abc'.format(gpu_dir, gpuname)


def build_gpu(path, outputfile): 
	mc.file(f=True, new=True)
	gpus = file_utils.listFile(path)
	for gpu in gpus: 
		gpufile = '{}/{}'.format(path, gpu)
		name = 'gpu_{}'.format(os.path.splitext(gpu)[0])
		maya_utils.create_gpu(name, gpufile)

	mc.file(rename=outputfile)
	mc.file(save=True, type='mayaAscii')
