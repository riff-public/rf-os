# -*- coding: utf-8 -*-
import os
import sys 
import time
from datetime import datetime
import maya.cmds as mc 
import maya.mel as mm 
import logging
logger = logging.getLogger(__name__)

from rftool.utils import path_info
from rftool.fix.polytag import polytag_core2 as polytag_core
from rftool.utils import maya_utils
from rftool.utils import file_utils
reload(maya_utils)
reload(polytag_core)

from rf_maya.nuTools.util import rotateOrder as nro
reload(nro)

def quick_export(): 
    sels = mc.ls(sl=True)
    res = 'md'
    geoGrp = 'Geo_Grp'
    rigGrp = 'Rig_Grp'

    for obj in sels: 
        assetName = str()
        libPath = str()

        assetAttr = '%s.assetName' % obj
        libAttr = '%s.path' % obj 

        if mc.objExists(assetAttr): 
            assetName = mc.getAttr(assetAttr)
        if mc.objExists(libAttr): 
            libPath = mc.getAttr(libAttr)

        if assetName and libPath: 
            asset = path_info.PathInfo(path=libPath)
            refPath = asset.libPath()
            rigName = asset.libName(step='rig', res=res, project=False)
            exportPath = '%s/%s' % (refPath, rigName)
            gpuName = os.path.splitext(asset.libName(step='gpu', res=res, project=False, ext='abc'))[0]
            adName = asset.libName('', 'ad', project=False)
            modelName = asset.libName(step='model', res=res, project=False)

            print exportPath
            print refPath, gpuName
            print refPath, modelName

            sgResult = sg_process.create_asset(project=projectEntity, assetType=entitySub1, assetSubType=entitySub2, assetName=entityName, episode=episodeEntity, taskTemplate=taskTemplate)
            if sgResult:
                dirResult = pipeline_utils.create_asset_template(root, projectEntity['name'], entitySub1, entitySub2, entityName)
            # create asset sg 
            # export geo
            # export rig 
            # export gpu 
            # exportResult = maya_utils.export_selection(exportPath, self.rigGrp)
         #    exportGpuResult = maya_utils.export_gpu(geoGrp, refPath, gpuName, time = 'still')
         #    exportGeoResult = maya_utils.export_geo(rigGrp, refPath, modelName)

def list_asset(): 
    from rftool.utils import sg_process
    from rftool.utils import path_info
    res = 'md'
    files=[]
    
    fields = ['code', 'sg_asset_type', 'sg_subtype']
    sgassets = sg_process.sg.find('Asset', [['project.Project.name', 'is', 'Two_Heroes'], ['sg_asset_type', 'is', 'setDress'], ['sg_subtype', 'is', 'garbage']], fields)

    for i, each in enumerate(sgassets): 
        asset = path_info.PathInfo(project='Two_Heroes', entity='asset', entitySub1=each.get('sg_asset_type'), entitySub2=each.get('sg_subtype'), name=each.get('code'))
        libPath = asset.libPath()
        modelName = asset.libName(step='model', res=res, project=False)
        rigName = asset.libName(step='rig', res=res, project=False)
        
        modelPath = '%s/%s' % (libPath, modelName)
        rigPath = '%s/%s' % (libPath, rigName)
        print '=================='
        print '%s %s' % (i, libPath)

        files.append(modelPath)
        files.append(rigPath)

    return files

def batch_file(files=[], func=None, save=True, loadRef='all'): 

    if not files: 
        files = list_asset()

    if files:  
        for path in files: 
            if os.path.exists(path): 
                try: 
                    mc.file(path, o=True, f=True, prompt=False, loadReferenceDepth=loadRef)

                except Exception as e: 
                    print '\nScene open error'
                    print e 
                if func: 
                    func()
                if save: 
                    mc.file(rename=path)
                    mc.file(save=True, f=True)
            else: 
                print 'missing %s' % path


def clean_file(): 
    from rftool.utils import maya_utils
    from rftool.utils import pipeline_utils

    # clean plugins 
    maya_utils.remove_plugins()
    maya_utils.cleanNamespace2()

    # link texture 
    pipeline_utils.relink_texture()

    # auto fill 
    if mc.objExists('Geo_Grp'): 
        childs = mc.listRelatives('Geo_Grp')
        plys = []

        if childs: 
            for child in childs: 
                shape = mc.listRelatives(child, s=True)
                if shape: 
                    if mc.objectType(shape[0], isType='mesh'): 
                        plys.append(child)

        if plys: 
            # mc.select(plys)
            polytag_core.assign_tag()

    else: 
        if mc.objExists('geo_grp') and mc.objExists('rig_grp'): 
            mc.rename('geo_grp', 'Geo_Grp')
            mc.rename('rig_grp', 'Rig_Grp')
            clean_file()

        else: 
            pass

    if mc.objExists('Rig_Grp'): 
        maya_utils.show_hide_transform(['Rig_Grp'], state=True)


def find_replace(): 
    sels = mc.ls(sl=True)
    grp = 'ok'
    if not mc.objExists(grp): 
        mc.group(em=True, n=grp)

    if sels: 
        for src in sels: 
            assetName = mc.getAttr('%s.assetName' % src)
            srcln = mc.ls(src, l=True)[0]
            mc.select(srcln)
            targets = maya_utils.find_same_poly()

            if targets: 
                targets.remove(src)
                targets.remove(srcln)
                
                for i, target in enumerate(targets): 
                    assetGrp = '%s_Grp' % assetName
                    parent = maya_utils.list_top_parent(target)

                    if not parent == grp: 
                        if not mc.objExists(assetGrp): 
                            mc.group(em=True, n=assetGrp)
                            mc.parent(assetGrp, grp)

                        dup = mc.duplicate(src, n='%s_%02d' % (assetName, i+1))[0]
                        mc.delete(mc.parentConstraint(target, dup))
                        mc.delete(mc.scaleConstraint(target, dup))
                        mc.parent(dup, assetGrp)
                        mc.delete(target)

def find_replace_sels(): 
    sels = mc.ls(sl=True)
    grp = 'ok'
    if not mc.objExists(grp): 
        mc.group(em=True, n=grp)

    if sels: 
        src = sels[0]
        assetName = mc.getAttr('%s.assetName' % src)
        srcln = mc.ls(src, l=True)[0]
        mc.select(srcln)
        targets = sels[1:]

        for i, target in enumerate(targets): 
            assetGrp = '%s_Grp' % assetName
            parent = maya_utils.list_top_parent(target)

            if not parent == grp: 
                if not mc.objExists(assetGrp): 
                    mc.group(em=True, n=assetGrp)
                    mc.parent(assetGrp, grp)

                dup = mc.duplicate(src, n='%s_%02d' % (assetName, i+1))[0]
                mc.delete(mc.parentConstraint(target, dup))
                # mc.delete(mc.scaleConstraint(target, dup))
                mc.parent(dup, assetGrp)
                mc.delete(target)

    
def import_assets(): 
    from rftool.utils import sg_process
    from rftool.utils import path_info
    res = 'md'
    fields = ['code', 'sg_asset_type', 'sg_subtype']
    sgassets = sg_process.sg.find('Asset', [['project.Project.name', 'is', 'Two_Heroes'], ['sg_asset_type', 'is', 'setDress'], ['sg_subtype', 'is', 'garbage']], fields)

    for i, each in enumerate(sgassets): 
        asset = path_info.PathInfo(project='Two_Heroes', entity='asset', entitySub1=each.get('sg_asset_type'), entitySub2=each.get('sg_subtype'), name=each.get('code'))
        libPath = asset.libPath()
        modelName = asset.libName(step='model', res=res, project=False)
        rigName = asset.libName(step='rig', res=res, project=False)
        rigPath = '%s/%s' % (libPath, rigName)

        if os.path.exists(rigPath): 
            mc.file(rigPath, i=True, ns=asset.name)


def clean_file3(): 
    from rftool.utils import pipeline_utils
    pipeline_utils.relink_texture()
    sn = mc.file(q=True, sn=True)
    dirname = os.path.dirname(sn)
    bkDir = '%s/_bk' % dirname
    timeSuffix = str(datetime.now()).replace(' ', '-').replace(':', '_').split('.')[0]
    bkDst = '%s/%s.%s' % (bkDir, os.path.basename(sn), timeSuffix)

    if not os.path.exists(bkDir): 
        os.makedirs(bkDir)

    # copy 
    file_utils.copy(sn, bkDst)

    if os.path.exists(bkDst): 
        result = mc.file(save=True)
        logger.info('success')


def clean_file4(): 
    from rftool.utils import pipeline_utils
    pipeline_utils.relink_sound()
    replace_cam_rig()
    write_asset_list()

    sn = mc.file(q=True, sn=True)
    filename, ext = os.path.splitext(sn)
    newName = '%s_pack%s' % (filename, ext)
    mc.file(rename=newName)
    mc.file(save=True)



def list_shot(path, step='layout'): 
    shots = file_utils.listFolder(path)
    files = []
    for shot in shots: 
        workPath = '%s/%s/%s/maya/work' % (path, shot, step)
        workFiles = sorted(file_utils.listFile(workPath))
        if workFiles: 
            latestFile = '%s/%s' % (workPath, workFiles[-1])

            print latestFile
            files.append(latestFile)

    return files


def write_asset_list(): 
    ymlPath = 'D:/shot_360_2_list.yml'
    ymlPath2 = 'D:/asset_360_2_list.yml'

    if not os.path.exists(ymlPath): 
        data = dict()
        file_utils.ymlDumper(ymlPath, data)
    data = file_utils.ymlLoader(ymlPath)

    sceneFile = mc.file(q=True, sn=True)
    sceneName = os.path.basename(sceneFile)
    files = mc.file(query=1, list=1, withoutCopyNumber=1)

    if not sceneName in data.keys(): 
        data[str(sceneName)] = [str(a) for a in files]

    file_utils.ymlDumper(ymlPath, data)

    if not os.path.exists(ymlPath2): 
        data2 = dict()
        file_utils.ymlDumper(ymlPath2, data2)
    data2 = file_utils.ymlLoader(ymlPath2)


    for eachFile in files: 
        eachFile = str(eachFile)
        ext = str(os.path.splitext(eachFile)[-1])

        if not ext in data2.keys(): 
            data2[ext] = [eachFile]

        else: 
            if not eachFile in data2[ext]: 
                data2[ext].append(eachFile)

    file_utils.ymlDumper(ymlPath2, data2)


def replace_cam_rig(): 
    from rftool.utils import maya_utils
    oldPath = 'P:/layoutLibrary/all/asset/tool/camera/layoutCamRig/hero/layoutCamRig_med.ma'
    camRigPath = 'P:/Two_Heroes/asset/_cam/layout/layoutCamRig/lib/layoutCamRig_md.ma'
    maya_utils.replace_reference(oldPath, camRigPath)


def replace_ref(dataPath): 
    refs = mc.file(q=True, r=True)
    data = file_utils.ymlLoader(dataPath)
    save = False 

    for ref in refs: 
        if ref.split('{')[0] in data.keys(): 
            print 'found %s' % ref
            replacePath = data[ref.split('{')[0]]
            maya_utils.replace_reference(ref, replacePath)
            print 'replace %s' % replacePath
            save = True 
    return save

def clean5(): 
    dataPath = 'P:/Two_Heroes/_outsource/TheMonk/_data/replace_01.yml'
    save = replace_ref(dataPath)
    write_asset_list()
    if save: 
        mc.file(save=True, f=True)


def collect(dataFile, dstPath): 
    data = file_utils.ymlLoader(dataFile)

    for filetype, files in data.iteritems(): 
        for eachFile in files: 
            basename = os.path.basename(eachFile)
            dirTree = os.path.splitdrive(os.path.dirname(eachFile))[-1]
            dst = '%s%s/%s' % (dstPath, dirTree, basename)
            
            file_utils.copy(eachFile, dst)
            logger.info('Copied %s' % dst)


def copy_backup(): 
    sn = mc.file(q=True, location=True)
    dirname = os.path.dirname(sn)
    basename = os.path.basename(sn)
    bk = '%s/_bk' % dirname
    bkName = '%s/%s.%s' % (bk, basename, datetimeStr())

    if not os.path.exists(bk): 
        os.makedirs(bk)

    return file_utils.copy(sn, bkName)


def datetimeStr(): 
    return datetime.now().strftime("%Y_%m_%d-%H_%M_%S")


def clean_plugin_assets(files=None): 
    if files: 
        batch_file(files=files, func=copy_backup, save=True)



def fix_vray_subdiv(nodes, value): 
    from rftool.light.utils import vray_utils
    reload(vray_utils)
    for node in nodes: 
        vray_utils.add_vray_subdivision(node, value)


def test_drive(idle=10, overall=1000): 
    from rftool.utils import log_utils
    logFile = log_utils.name('test', user='TA')
    logger = log_utils.init_logger(logFile)

    for i in range(overall): 
        time.sleep(idle)
        logger.info('Connected %s/%s' % (i, 1000))


def collect_pucca_scene(): 
    dst = 'Y:/NPC/_data'
    assetList = 'check_asset_ep03.yml'
    sceneList = 'check_scene_ep03.yml'

    if not os.path.exists('%s/%s' % (dst, assetList)): 
        file_utils.ymlDumper('%s/%s' % (dst, assetList), list())

    if not os.path.exists('%s/%s' % (dst, sceneList)): 
        file_utils.ymlDumper('%s/%s' % (dst, sceneList), dict())

    assetData = file_utils.ymlLoader('%s/%s' % (dst, assetList))
    sceneData = file_utils.ymlLoader('%s/%s' % (dst, sceneList))

    sn = mc.file(q=True, location=True)
    refs = mc.file(q=True, r=True)
    refs = list(set([str(a.split('{')[0]) for a in refs]))
    sceneKey = str(os.path.basename(sn))

    # scene data 
    if not sceneKey in sceneData.keys(): 
        sceneData.update({sceneKey: refs})

    # asset data 
    for ref in refs: 
        if not ref in assetData: 
            assetData.append(ref)

    # write data 
    file_utils.ymlDumper('%s/%s' % (dst, assetList), assetData)
    file_utils.ymlDumper('%s/%s' % (dst, sceneList), sceneData)


def list_pucca_anim(shotPath): 
    # shotPath = 'P:/pucca/EP03/scene/anim'
    shots = file_utils.listFolder(shotPath)
    shotPaths = []

    for shot in shots: 
        workPath = '%s/%s/version' % (shotPath, shot)
        files = sorted(file_utils.listFile(workPath))
        if files: 
            latestFile = files[-1]
            shotPaths.append('%s/%s' % (workPath, latestFile))

    return shotPaths


def switch_refs(files): 
    if files: 
        for each in files: 
            basename, ext = os.path.splitext(os.path.basename(each))
            replaceName = '%s/%s_pxy%s' % (os.path.dirname(each), basename, ext)
            file_utils.copy(each, replaceName)
            print 'copy to %s' % replaceName

            if os.path.exists(replaceName):                 
                file_utils.search_replace_file(replaceName, '_ani_v00.ma', '_vrayPxy_v00.ma', False, True)
                print 'switch success'


def list_pucca_light(path): 
    shots = file_utils.listFolder(path)
    targetFiles = []
    for shot in shots: 
        workPath = '%s/%s/ver' % (path, shot)
        files = file_utils.listFile(workPath)
        if files: 
            latestFile = '%s/%s' % (workPath, sorted(files)[-1])
            targetFiles.append(latestFile)

    return targetFiles


def find_asset(keyword, file): 
    lines = []
    with open(file) as f:
        for line in f:
            if keyword in line:
                 lines.append(line)

    return lines

def write_pucca_asset_data(keyword, files): 
    dst = 'Y:/NPC/_data'
    assetList = '%s/check_asset_ep04.yml' % dst
    sceneList = '%s/check_scene_ep04.yml' % dst
    allAssets = []
    data = dict()
    for each in files: 
        assets = find_asset(keyword, each)
        data.update({os.path.basename: each})

        for asset in assets: 
            if not asset in allAssets: 
                allAssets.append(str(asset))

        print 'finish reading %s' % each

    file_utils.ymlDumper(sceneList, data)
    file_utils.ymlDumper(assetList, allAssets)

# pucca run     
# from rftool.fix import misc
# reload(misc)
# files = misc.list_pucca_anim('P:/pucca/EP04/scene/anim')
# misc.batch_file(files=files, func=misc.collect_pucca_scene, save=False, loadRef='none')



def fix_leg_rotateOrder(oldRo='yzx', newRo='xzy'):
    
    sels = mc.ls(sl=True)
    if not sels:
        return
    
    from rf_maya.nuTools.util import rotateOrder as nro
    reload(nro)

    objName = sels[0].split(':')[-1]
    ns = sels[0].replace(objName, '')
    ctrls = ['UpLegIk_L_Ctrl', 'UpLegIk_R_Ctrl', 'AnkleIk_L_Ctrl', 
            'AnkleIk_R_Ctrl', 'UpLegFk_L_Ctrl', 'UpLegFk_R_Ctrl', 
            'LowLegFk_L_Ctrl','LowLegFk_R_Ctrl', 'AnkleFk_L_Ctrl', 
            'AnkleFk_R_Ctrl']
    if ns:
        ctrl_names = []
        for c in ctrls:
            ctrl_names.append('%s%s' %(ns, c))
    else:
        ctrl_names = ctrls

    print ctrl_names
    nro.restore(objs=ctrl_names, oldRo=oldRo, newRo=newRo) 


def fix_object_pivot_constraint(): 
    """ move object back to zero and update parentConstrinat """
    # parentConstraint -e -maintainOffset FloorDPoleMoveCGmbl_Ctrl  FloorDPoleMoveCGeo_Grp_parentConstraint1;

    sels = mc.ls(sl=True)
    zeroMatrix = [1.0,0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0]

    for sel in sels: 
        mc.xform(sel, ws=True, m=zeroMatrix)
        nodes = mc.listHistory(sel)
        conNodes = [a for a in nodes if mc.objectType(a, isType='parentConstraint')]
        if conNodes: 
            ctrl = mc.listConnections('%s.target[0].targetParentMatrix' % conNodes[0])
            mc.parentConstraint(ctrl[0], conNodes[0], e=True, maintainOffset=True)


def apply_shade_manual(): 
    from rf_utils.context import context_info
    from rftool.shade import shade_utils
    reload(shade_utils)

    sels = mc.ls(sl=True)
    for sel in sels: 
        ns = sel.split(':')[0]
        path = mc.referenceQuery(sel, f=True)
        basename = os.path.basename(path)
        assetName = basename.split('_')[4] 
        mtrDict = shade_utils.guess_mtr_files('SevenChickMovie', assetName, 'ldv')
        shadeNamespace = '%s_%s%sMtr' % (assetName, 'ldv', 'Main')
        look = 'main'
        shadeFile = mtrDict.get(look)
        
        shade_utils.apply_ref_shade(shadeFile, shadeNamespace, ns)


def remove_shot_steps(project, episode, keepStep=['layout', 'anim', 'techanim']): 
    import shutil
    from rf_utils import file_utils 
    path = 'P:/%s/scene/work/%s' % (project, episode)
    shots = file_utils.list_folder(path)

    for shot in shots: 
        shotPath = '%s/%s' % (path, shot)
        steps = file_utils.list_folder(shotPath)
        
        for step in steps: 
            stepPath = '%s/%s' % (shotPath, step)
            if not step in keepStep: 
                shutil.rmtree(stepPath)
                print 'remove %s' % stepPath


def rename_shot_step(project, episode, srcStep, dstStep): 
    import shutil
    from rf_utils import file_utils 
    path = 'P:/%s/scene/work/%s' % (project, episode)
    shots = file_utils.list_folder(path)

    for shot in shots: 
        shotPath = '%s/%s' % (path, shot)
        
        stepPath = '%s/%s' % (shotPath, srcStep)
        dstStepPath = '%s/%s' % (shotPath, dstStep)
        if os.path.exists(stepPath): 
            print 'rename %s %s' % (stepPath, dstStepPath)
            os.rename(stepPath, dstStepPath)
