import os 
import sys 
from collections import OrderedDict
import maya.cmds as mc 
from rf_utils.context import context_info
from rf_utils import file_utils
from rftool.utils import maya_utils
import pymel.core as pm 
from rf_app.save_plus import save_utils
reload(save_utils)
from functools import partial
from rf_maya.lib import sequencer_lib
from rf_app.asm import asm_maya

def replace_asset(mode='all', *args): 
    copys = []
    if mode == 'all': 
        refs = mc.file(q=True, r=True)

    if mode == 'selected': 
        selObjs = mc.ls(sl=True)
        refs = []
        for each in selObjs: 
            path = mc.referenceQuery(each, f=True)
            if not path in refs: 
                refs.append(path)

    if refs: 
        for ref in refs: 
            if not '/publ/' in ref: 
                mapRes = {'med': 'md', 'proxy': 'pr'}
                name = ref.split('/')[-3]
                name = decapitalize(name)
                assetType = ref.split('/')[-5]
                ns = mc.referenceQuery(ref, ns=True)
                curTopNode = find_top_node(ns)
                parents = mc.listRelatives(curTopNode[0], p=True)
                order = asm_maya.outliner_order(curTopNode[0])

                filename = os.path.basename(ref)
                # pipeline name 
                rigName = '%s_rig_main_md.hero.ma' % name

                res = [v for k, v in mapRes.iteritems() if k in filename]

                if res: 
                    res = res[0]

                    context = context_info.Context()
                    context.update(project='HanumanSq4', entityType='asset', entityGrp=assetType, entity=name, process='main', step='rig', app='maya', res=res)
                    asset = context_info.ContextPathInfo(context=context)
                    heroPath = asset.path.hero().abs_path()
                    rigName = asset.publish_name(hero=True)
                    maFile = '%s/%s' % (heroPath, rigName)
                    mbAsset = maFile.replace('.ma', '.mb')

                    if not os.path.exists(mbAsset): 
                        print 'file not exists %s' % mbAsset
                        if not os.path.exists(maFile): 
                            print 'ma not exists %s' % maFile 
                            # copy to asset 
                            file_utils.copy(ref.split('{')[0], maFile)
                            print 'Copy to %s' % maFile
                            copys.append(maFile)

                        # replace ref 
                        maya_utils.replace_reference(ref, maFile)
                        newTopNode = find_top_node(ns)
                        if parents: 
                            asm_maya.parent_order(newTopNode, parents[0], order)
                        print 'replace to .ma %s' % maFile
                        continue 

                    # replace ref 
                    maya_utils.replace_reference(ref, mbAsset)
                    newTopNode = find_top_node(ns)
                    if parents: 
                        asm_maya.parent_order(newTopNode, parents[0], order)
                    print 'replace', mbAsset


def decapitalize(name): 
    return (name[0].lower() + name[1:])

def missing_mb(): 
    refs = mc.file(q=True, r=True)
    missing = []

    for ref in refs: 
        path = ref.split('{')[0]
        name, ext = os.path.splitext(path)
        mb = '%s%s' % (name, '.mb')
        if not os.path.exists(mb): 
            missing.append(path)

    return missing

def switch_asset(extType='.mb', *args): 
    log = 'P:/HanumanSq4/scene/work/act1/_log/missing_asset.yml'
    missing = []
    if not os.path.exists(log): 
        data = OrderedDict()
        data['missing'] = []
        file_utils.ymlDumper(log, data)

    else: 
        data = file_utils.ymlLoader(log)


    refs = mc.file(q=True, r=True)
    for ref in refs: 
        name, ext = os.path.splitext(ref.split('{')[0])
        if not ext == extType: 
            target = '%s%s' % (name, extType)

            if os.path.exists(target): 
                maya_utils.replace_reference(ref, target)
                
            else: 
                print 'missing', target
                if not target in data['missing']: 
                    data['missing'].append(target)
                    missing.append(target) if not target in missing else None

    file_utils.ymlDumper(log, data)
    # conclusion 
    if missing: 
        print 'Missing (%s) ===========================' % len(missing)
        for key in missing: 
            print key


def create_sequencer(*args): 
    """ create sequencer """ 
    scene = context_info.ContextPathInfo()
    shotName = scene.name_code 
    camName = '%s_cam' % scene.name_code
    # camera sel 
    cam = mc.ls(sl=True)
    if cam: 
        camShape = cam[0]

        if mc.objectType(camShape, isType='camera'): 
            camTransform = mc.listRelatives(camShape, p=True, f=True)[0]
            start = mc.playbackOptions(q=True, min=True)
            end = mc.playbackOptions(q=True, max=True)
            camera = mc.rename(camTransform, camName)
            
            # clear all sequencer 
            mc.delete(mc.ls(type='shot'))
            shot = sequencer_lib.create_shot(shotName, camera, startTime=start, endTime=end, sequenceStartTime=None, sequenceEndTime=None)
        
        else: 
            mc.confirmDialog( title='Confirm', message='Select object is not a camera shape', button=['OK'])

    else: 
        mc.confirmDialog( title='Confirm', message='Select camera shape', button=['OK'])

    print '------------------'
    print 'Shot created %s' % shot


def shift_key(*args): 
    currentStart = mc.playbackOptions(q=True, min=True)
    currentEnd = mc.playbackOptions(q=True, max=True)
    duration = currentEnd - currentStart + 1
    start = 1001 
    end = start + (currentEnd - currentStart)
    offsetKey = start - currentStart

    mc.playbackOptions(min=start, max=end, ast=start, aet=end)
    sequencer_lib.shift_key(type = 'default', frameRange = [1, 10000], frame = int(offsetKey))

def save_shot(*args): 
    path = mc.file(q=True, loc=True)
    scene = context_info.ContextPathInfo(path=path)
    # version by count 
    dirname = os.path.dirname(path)
    files = file_utils.list_file(dirname)
    count = len(files) + 1
    version = 'v%03d' % count
    user = scene.local_user
    scene.context.update(version=version)
    filename = scene.work_name(user=True).replace('.%s' % user, '.Clean')
    savePath = '%s/%s' % (dirname, filename)

    mc.file(rename=savePath)
    return mc.file(save=True, f=True, type='mayaAscii')


def convert_files(fileList): 
    donePath = []

    for i, path in enumerate(fileList): 
        print i, path
        print 'leftPath', [a for a in fileList if not a in donePath] 
        mc.file(path, o=True, f=True, prompt=False)
        remove_unknown_nodes()

        asset = context_info.ContextPathInfo(path=path)
        if asset.type == 'char': 
            result = convert_char()
            save_as('.mb')

            if not result: 
                raise RuntimeError('Failed to convert char')

        if asset.type == 'prop': 
            result = convert_prop()
            save_as('.mb')

        donePath.append(path)

def convert_char(): 
    oRigGrp = 'rig_grp'
    nRigGrp = 'Rig_Grp'
    renameDict = {'rig_grp': 'Rig_Grp', 'geo_grp': 'Geo_Grp'}

    # detect top node 
    topGrps = detect_group()
    if len(topGrps) == 1: 
        topGrp = topGrps[0]

        if topGrp == oRigGrp: 
            mc.rename(oRigGrp, nRigGrp)

        else: 
            mc.rename(topGrp, nRigGrp)
    # detect top node group
    results = []
    if detect_rig_grp(): 
        for k, v in renameDict.iteritems(): 
            if mc.objExists(k): 
                mc.rename(k, v)
                results.append(True)
            else: 
                results.append(False)

    return True


def convert_prop(*args): 
    oRigGrp = 'rig_grp'
    nRigGrp = 'Rig_Grp'
    oGeoGrp = 'geo_grp'
    nGeoGrp = 'Geo_Grp'
    allMoverGrp = 'AllMover_Grp'

    topGrps = detect_group()
    if len(topGrps) == 1: 
        topGrp = topGrps[0]
        nRigGrp = pm.rename(topGrp, nRigGrp)

    # reparent geoGrp 
    geos = guess_geo_constraint()

    # check if Geo_Grp exists 
    if not pm.objExists(nGeoGrp): 
        nGeoGrp = pm.group(em=True, n=nGeoGrp)
        pm.parent(nGeoGrp, nRigGrp)

    pm.parent(geos, nGeoGrp)

    # regroupo AllMover 
    childs = pm.listRelatives(nRigGrp, c=True)
    if not pm.objExists(allMoverGrp): 
        allMoverGrp = pm.group(em=True, n=allMoverGrp)

    for child in childs: 
        if not child == nGeoGrp: 
            pm.parent(child, allMoverGrp)

    pm.parent(allMoverGrp, nRigGrp)

    return True 

def find_top_node(namespace): 
    sels = mc.ls('%s:*' % namespace, l=True, type='transform')
    roots = []
    for sel in sels: 
        root = find_ref_from_longname(sel)
        if root: 
            roots.append(root) if not root in roots else None
    return roots

def find_ref_from_longname(name): 
    count = len(name.split('|'))

    for each in range(count): 
        checkName = ('|').join(name.split('|')[:each])
        if checkName: 
            if mc.referenceQuery(checkName, inr=True): 
                return checkName

def guess_geo_constraint(): 
    constraints = ['parentConstraint', 'scaleConstraint']
    groups = [a for a in pm.ls(type='transform') if not a.getShape() and a.nodeType() == 'transform']
    geos = []
    for group in groups: 
        if group.inputs(type='parentConstraint') or group.inputs(type='scaleConstraint'): 
            geos.append(group)

    return geos


def detect_rig_grp(): 
    if 'Rig_Grp' in mc.ls(assemblies=True): 
        return True 

def detect_group(): 
    nodes = mc.ls(assemblies=True)
    groups = []
    for node in nodes: 
        shape = mc.listRelatives(node, s=True)
        if not shape: 
            groups.append(node)

    return groups

def save_as(ext='.mb', *args): 
    remove_unknown_nodes()
    path = mc.file(q=True, loc=True)
    name, currentExt = os.path.splitext(path)
    newName = '%s%s' % (name, ext)

    filetype = 'mayaAscii' if ext == '.ma' else 'mayaBinary' if ext == '.mb' else ''
    mc.file(rename=newName)
    result = mc.file(save=True, f=True, type=filetype)
    print result 
    return result


def remove_unknown_nodes(): 
    nodes = mc.ls(type='unknown')
    if nodes: 
        mc.delete(nodes)


def copy_shot(shotPath): 
    shots = file_utils.list_folder(shotPath)

    for shot in shots: 
        if not 'sh' in shot: 
            verPath = '%s/%s/version' % (shotPath, shot)
            files = file_utils.list_file(verPath)
            if files: 
                latestFile = sorted(files)[-1]
                
                # hnm4_act1_q0040_s0010
                newShotName = 'hnm4_act1_q0040_%s' % shot
                newFileName = '%s_anim_main.v001.ma' % newShotName
                dst = 'P:/HanumanSq4/scene/work/act1/%s/anim/main/maya/%s' % (newShotName, newFileName)
                src = '%s/%s' % (verPath, latestFile)

                if not os.path.exists(os.path.dirname(dst)): 
                    os.makedirs(os.path.dirname(dst))

                file_utils.copy(src, dst)
                print 'copy %s' % dst


def create_shot(shotPath): 
    from rf_utils.sg import sg_process
    project = 'HanumanSq4'
    episode = 'act1'
    sequence = 'hnm4_act1_q0040'
    projectEntity = sg_process.get_project(project)
    sequenceEntity = sg_process.get_one_sequence(project, episode, sequence, fields=[])
    episodeEntity = sg_process.get_one_episode(project, episode)
    shots = file_utils.list_folder(shotPath)

    for shot in shots: 
        if not 'sh' in shot: 
            shotName = 'hnm4_act1_q0040_%s' % shot
            shortCode = shot
            # print shotName
            result = sg_process.create_shot(projectEntity, episodeEntity, sequenceEntity, shotName, shortCode, template='default')
            print result


def set_shot_status(status='rdy', *args): 
    """ set shot status to ready """ 
    from rf_utils.sg import sg_process
    sg = sg_process.sg 
    scene = context_info.ContextPathInfo()
    scene.context.use_sg(sg, project='HanumanSq4', entityType='scene', entityName=scene.name)
    
    # find task id 
    filters = [['entity', 'is', scene.context.sgEntity], ['project.Project.name', 'is', scene.project], ['content', 'is', 'anim']]
    taskEntity = sg.find_one('Task', filters, ['id'])
    if taskEntity: 
        data = {'sg_status_list': status}
        result = sg.update('Task', taskEntity['id'], data)
        print '-------------'
        print 'set status complete %s' % result
    else: 
        print 'task not found', filters


def open_shot(*args): 
    shots = list_unready_shot()
    print 'open', shots[0]
    result = mc.confirmDialog( title='Confirm', message='Open %s?' % shots[0], button=['Yes','No'])
    if result == 'Yes': 
        mc.file(shots[0], o=True, f=True, prompt=False)
        set_shot_status('ip')


def list_unready_shot(*args): 
    """ list shot that's not finish """ 
    from rf_utils.sg import sg_process
    sg = sg_process.sg 
    shotList = []

    taskFilter = [['project.Project.name', 'is', 'HanumanSq4'], 
                    ['content', 'is', 'anim'], 
                    ['sg_status_list', 'is', 'wtg']]
    taskEntities = sg.find('Task', taskFilter, ['content', 'id', 'entity', 'entity.Shot.code'])
    sortedEntities = sorted_entities(taskEntities)

    for task in sortedEntities: 
        shotName = task['entity']['name']
        path = 'P:/HanumanSq4/scene/work/act1/%s/anim/main/maya' % shotName
        files = file_utils.list_file(path)
        latestFile = files[-1]
        filePath = '%s/%s' % (path, latestFile)
        print filePath 
        shotList.append(filePath)

    print '-------------'
    print '%s shots left' % len(taskEntities)

    return shotList 


def sorted_entities(entities, keySort='entity.Shot.code'): 
    tempDict = OrderedDict()

    for entity in entities: 
        tempDict[entity[keySort]] = entity

    return [tempDict[k] for k in sorted(tempDict.keys())]

def submit_playblast(*args): 
    from rftool.scene.playblast import pb_app
    reload(pb_app)
    myApp = pb_app.show() 



def batch_replace_mb(shotPath, start=0, end=-1): 
    logFile = 'P:/HanumanSq4/scene/work/act1/_log/batch.yml'
    files = get_workfile(shotPath)

    for file in files[start: end]: 
        mc.file(file, o=True, f=True, prompt=False)
        remove_unknown_nodes()
        switch_asset('.mb')

        # result = save_utils.save_increment()

        if not os.path.exists(logFile): 
            file_utils.ymlDumper(logFile, OrderedDict())
        data = file_utils.ymlLoader(logFile)
        data[os.path.basename(file)] = file
        file_utils.ymlDumper(logFile, data)
        print 'write log'

def batch_replace_shot(fileList): 
    """ replace ma to mb """ 

    for path in fileList: 
        mc.file(path, o=True, f=True, prompt=False)
        remove_unknown_nodes()
        switch_asset('.mb')
        save_utils.save_increment()




def get_workfile(shotPath): 
    shots = file_utils.list_folder(shotPath)
    workfiles = []
    for shot in shots: 
        workspacePath = '%s/%s/anim/main/maya' % (shotPath, shot)
        files = file_utils.list_file(workspacePath)
        if files: 
            workfile = '%s/%s' % (workspacePath, files[-1])
            workfiles.append(workfile)
    return workfiles


def copy_asset(assetPath, assetType='prop'): 
    """ copy med to md and proxy to pr """ 
    # assetPath = 'P:/FOW/all/asset/prop/main'
    assetNames = file_utils.list_folder(assetPath)
    mapRes = {'med': 'md', 'proxy': 'pr'}

    for name in assetNames: 
        heroPath = '%s/%s/hero' % (assetPath, name)
        files = file_utils.list_file(heroPath)
        
        if files: 

            for file in files: 
                res = [v for k, v in mapRes.iteritems() if k in file]
                if res: 
                    src = '%s/%s'% (heroPath, file)
                    res = res[0]
                    context = context_info.Context()
                    context.update(project='HanumanSq4', entityType='asset', entityGrp=assetType, entity=name, process='main', step='rig', app='maya', res=res)
                    asset = context_info.ContextPathInfo(context=context)
                    heroPath = asset.path.hero().abs_path()
                    rigName = asset.publish_name(hero=True)
                    rigFile = '%s/%s' % (heroPath, rigName)
                    dst = rigFile
                        
                    if os.path.exists(dst): 
                        backup(dst)
                    
                    file_utils.copy(src, dst)
                    print 'Copy %s' % dst

        # copy to dst 
        # src = ref.split('{')[0]
        # dst = rigFile
        # if not os.path.exists(dst): 
        #     file_utils.copy(src, dst)
        #     print 'copy', dst

def backup(filePath): 
    from datetime import datetime
    dateSuffix = str(datetime.now()).replace(' ', '_').replace(':', '_').split('.')[0]
    # Result: '2019-11-18_12_37_48' # 

    backup = '_bk'
    dirname = os.path.dirname(filePath)
    backupDir = '%s/%s' % (dirname, backup)
    backupFile = '%s_%s' % (os.path.basename(filePath), dateSuffix)
    dst = '%s/%s' % (backupDir, backupFile)

    if not os.path.exists(backupDir): 
        os.makedirs(backupDir)

    return file_utils.copy(filePath, dst)


def capitalized_asset_name(assetPath): 
    # assetPath = 'P:\HanumanSq4\asset\publ\prop'
    assetNames = file_utils.list_folder(assetPath)

    for name in assetNames: 
        assetName = decapitalize(name)

        src = '%s/%s' % (assetPath, name)
        dst = '%s/%s' % (assetPath, decapitalize(name))
        os.rename(src, dst)

        # heroPath = '%s/%s' % (assetPath, assetName)

        # for root, dirs, files in os.walk(heroPath): 
        #     for file in files: 
        #         filename = decapitalize(file)
        #         oldDst = '%s/%s' % (root, file)
        #         newDst = '%s/%s' % (root, filename)
        #         os.rename(oldDst, newDst)
        #         print newDst


def list_shot(path='P:/HanumanSq4/scene/work/act1'): 
    shots = file_utils.list_folder(path)
    targets = []
    for shot in shots: 
        wsPath = '%s/%s/anim/main/maya' % (path, shot)
        files = file_utils.list_file(wsPath)

        if files: 
            latestFile = files[-1]
            targets.append('%s/%s' % (wsPath, latestFile))

    return targets


def check_asset(*args): 
    """ check asset not in the pipeline """
    wrongAssets = []
    refs = mc.file(q=True, r=True)
    for ref in refs: 
        refPath = ref.split('{')[0]
        if not '/publ/' in refPath: 
            wrongAssets.append(refPath) if not refPath in wrongAssets else None

    if wrongAssets: 
        print '------------------------'
        for asset in wrongAssets: 
            print asset
        print '------------------------'
        print 'Total %s wrong assets' % len(wrongAssets)
    else: 
        print '------------------------'
        print 'All asset good'


def check_missing_mb(*args): 
    assets = []
    refs = mc.file(q=True, r=True)
    for ref in refs: 
        refPath = ref.split('{')[0]
        name, ext = os.path.splitext(refPath)
        if ext == '.ma': 
            assets.append(refPath) if not refPath in assets else None

    if assets: 
        print '------------------------'
        for asset in assets: 
            print asset
        print '------------------------'
        print 'Total %s missing .mb assets' % len(assets)
    else: 
        print '------------------------'
        print 'All asset linked to .mb'


def ui(): 
    uiName = 'fixUi'
    if mc.window(uiName, ex=True): 
        mc.deleteUI(uiName)
    win = mc.window(uiName)
    mc.columnLayout(adj=1, rs=4)
    
    mc.text(l='---- Action ----')
    mc.button(l='Open left shot', h=30, bgc=(0.6, 1, 0.6), c=open_shot)
    mc.button(l='Shift to start 1001', h=30, bgc=(0.6, 1, 0.5), c=shift_key)
    
    mc.text(l='---- Info ----')
    mc.button(l='Check nonPipe asset', h=30, bgc=(0.4, 0.7, 1), c=check_asset)
    mc.button(l='Check missing mb', h=30, bgc=(0.4, 0.8, 1), c=check_missing_mb)

    mc.text(l='---- Steps ----')
    mc.button(l='1. Replace pipeline asset', h=30, bgc=(0.7, 1, 0.7), c=partial(replace_asset, 'all'))
    mc.button(l='1. Replace selected', h=30, bgc=(0.7, 1, 0.7), c=partial(replace_asset, 'selected'))
    mc.text(l='----')
    mc.button(l='2. Replace to mb', h=30, bgc=(0.8, 1, 0.8), c=partial(switch_asset, '.mb'))
    mc.button(l='3. Add sequencer', h=30, bgc=(0.8, 1, 0.9), c=create_sequencer)
    mc.button(l='4. Save scene', h=30, bgc=(0.8, 1, 0.9), c=save_shot)


    mc.text(l='---- Fix Asset ----')
    mc.button(l='Fix prop', h=30, bgc=(1, 0.6, 0.6), c=convert_prop)
    mc.button(l='Save as .mb', h=30, bgc=(1, 0.7, 0.7), c=partial(save_as, '.mb'))

    mc.text(l='---- Tracking ----')
    mc.button(l='Set shot ready', h=30, bgc=(1, 1, 0.7), c=partial(set_shot_status, 'rdy'))
    mc.button(l='List uncomplete shot', h=30, bgc=(1, 1, 0.8), c=list_unready_shot)
    mc.button(l='Submit playblast', h=30, bgc=(1, 1, 0.9), c=submit_playblast)
    
    mc.showWindow()
    mc.window(uiName, e=True, wh=[200, 600])
