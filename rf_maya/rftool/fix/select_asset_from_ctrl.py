import pymel.core as pm


def select(root_name='Ctrl_Grp', ctrl_suffix='_Ctrl'):
    sels = pm.selected()
    if not sels:
        return

    
    
    to_sels = []
    for sel in sels:
        ns = sel.namespace()
        root = pm.PyNode('%s%s' %(ns, root_name))
        root_children = root.getChildren(ad=True, type='transform')

        to_sels.append(sel)
        parents = [p for p in sel.getAllParents() if p in root_children and p.nodeName().endswith(ctrl_suffix)]
        children = [c for c in sel.getChildren(ad=True, type='transform') if c.nodeName().endswith(ctrl_suffix)]
        
        ctrls = parents + children
        geos = []
        if ctrls:
            for c in ctrls:
                constraints = c.outputs(type='parentConstraint')
                con_children = set()
                for con in constraints:
                    con_out_children = con.constraintTranslate.children()
                    for ch in con_out_children:
                        con_outs = ch.outputs()
                        for o in con_outs:
                            con_children.add(o)
                geos += list(con_children)
        to_sels.extend(ctrls + geos)

    pm.select(to_sels)