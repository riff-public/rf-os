import pymel.core as pm
import maya.cmds as mc
from ncscript.rig import rivetTools, core
reload( rivetTools )

# edit edge number here 
edges = (5955, 49034)
name = 'monster'
side = ''

for i, sel in enumerate(pm.selected()):
    rvt = rivetTools.rivet_loft_edge(name  = name,
                                    side  = side, 
                                    mesh  = sel.getShape().shortName(), 
                                    edge1 = sel.e[edges[0]].name(),
                                    edge2 = sel.e[edges[1]].name())
    loc = pm.spaceLocator()
    locShp = loc.getShape()
    pm.parent(locShp, rvt['rvt'], r=True, s=True)
    pm.delete(loc)
    rvt['rvt'].rename('%s%sRivet_loc' %(name, (i+1)))
