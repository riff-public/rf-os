import sys 
import os,time
import xlsxwriter
from collections import OrderedDict
import pandas as pd
sys.path.append('{}/core'.format(os.environ['RFSCRIPT']))


def search_file(path):
	dic = OrderedDict()
	data = OrderedDict()
	create_files = []
	last_modified = []
	name = []
	path_file =[]
	types =[]
	for (root, dirs, files) in os.walk(path, topdown=True): 
	    for i in files:
	    	stat = os.stat('{}/{}'.format(root,i))
	    	file_type = i.split('.')[-1:][0]
	    	create_files.append(time.ctime(stat.st_ctime))
	    	name.append(i)
	    	path_file.append(root)
	    	types.append(file_type)
	    	last_modified.append(time.ctime(stat.st_mtime))


	    	dic[i] = root,time.ctime(stat.st_ctime),time.ctime(stat.st_mtime),file_type
	    	
	# data['Num'] = range(1, len(name)+1)
	data['Name'] = name	
	data['Type'] = types
	data['path'] = path_file
	data['Create_Time'] = create_files
	data['Last_Modified'] = last_modified
	return data


def create_excel(data, path, name):
	filename = '{}/{}.xlsx'.format(path, name)
	dataframe = pd.DataFrame(data=data)
	writer = pd.ExcelWriter(filename, engine='xlsxwriter')
	dataframe.to_excel(writer, sheet_name=name)
	writer.save()
	return filename


# path = 'P:/SevenChickMovie/daily/anim/2018_08_15'
# data = search_file(path)
# create_excel(data,'P:/SevenChickMovie/daily', 'test')

def list_folder(path): 
	return [a for a in os.listdir(path) if os.path.isdir(os.path.join(path, a))]

def list_asset_output(assetType): 
	assetPath = 'P:/SevenChickMovie/asset/publ/{}'.format(assetType)
	assetNames = list_folder(assetPath)
	outputDict = OrderedDict()
	for asset in assetNames: 
		outputs = []
		outputPath = '{}/{}/hero'.format(assetPath, asset)
		texturePath = '{}/{}/texture'.format(assetPath, asset)
		if os.path.exists(outputPath): 
			outputs.append(outputPath)
		if os.path.exists(texturePath): 
			outputs.append(texturePath)
		outputDict[asset] = outputs

	return outputDict


def list_shot_output(ep, sequence): 
	epPath = 'P:/SevenChickMovie/scene/publ/{}'.format(ep)
	shots = list_folder(epPath)
	outputs = []
	for shot in shots: 
		if sequence in shot: 
			outputPath = '{}/{}/output'.format(epPath, shot)
			if os.path.exists(outputPath): 
				outputs.append(outputPath)

	return sorted(outputs)[::-1]


def check_sum(path): 
	sums = []
	for root, dirs, files in os.walk(path): 
		sums.extend(files)
	return len(sums)


def combine_dict(dict1, dict2): 
	baseDict = dict1.copy()
	addDict = dict2.copy()

	for k, v in addDict.items(): 
		if k in baseDict.keys(): 
			baseDict[k].append(v)
		else: 
			baseDict[k] = v
	return baseDict

def combine_data(data1, data2):
	data = data1.copy()
	for i in data2.keys():
		for j in data2[i]:
			data[i].append(str(j))
	return data


def list_sequence(ep): 
	epPath = 'P:/SevenChickMovie/scene/publ/{}'.format(ep)
	shots = list_folder(epPath)
	seqs = []

	for shot in shots: 
		if len(shot.split('_')) == 4: 
			seq = shot.split('_')[2]
			seqs.append(seq)
	return list(set(seqs))

def main(): 
	# for act in ['act3']: 
	# 	seqs = sorted(list_sequence(act))
		
	# 	for seq in seqs: 
	# 		print(seq)
	# 		outputs = list_shot_output(act, seq)
	# 		data = OrderedDict()
	# 		for output in outputs: 
	# 			shotName = output.split('/')[-2]
	# 			result = check_sum(output)
	# 			# print('{} files: {}'.format(result, output))
	# 			print(output)
	# 			data = search_file(output)
	# 			# data = combine_data(newData, data)
	# 			xls = create_excel(data,'P:/SevenChickMovie/doc/list_files/{}'.format(act), '{}'.format(shotName))
	# 			print(xls)

	assetType = 'set'
	assetDict = list_asset_output(assetType)

	for asset, outputs in assetDict.items(): 
		data = OrderedDict()
		for output in outputs: 
			newData = search_file(output)
			data = combine_data(newData, data)
		xls = create_excel(data, 'P:/SevenChickMovie/doc/list_files/{}'.format(assetType), '{}'.format(asset))
		print(xls)
	# count = 0
	# outputs = list_shot_output('act1', 'q0010')
	# for output in outputs: 
	# 	result = check_sum(output)
	# 	# print('{} files: {}'.format(result, output))
	# 	count += result
	# print(count)

if __name__ == '__main__': 
	main()
