# v.0.0.1 polytag switcher
_title = 'RebuildAsset'
_version = 'v.0.0.1'
_des = 'wip'
uiName = 'RebuildAsset'

#Import python modules
import sys
import os 
import getpass

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]


# framework modules 
import rf_config as config
from rf_utils import log_utils
from rf_utils.ui import load
user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'

logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

from rf_utils import pipeline_path as p_path
from rftool.utils import plugin_utils
from rftool.utils import maya_utils
reload(maya_utils)
reload(p_path)

class Setting: 
    tmpPath = p_path.tmpPath(uiName)
    tmpFile = 'rebuild.anim'
    animFile = '%s/%s' % (tmpPath, tmpFile)


class RebuildAsset(QtWidgets.QMainWindow):

    def __init__(self, parent=None):
        #Setup Window
        super(RebuildAsset, self).__init__(parent)

        # ui read
        uiFile = '%s/ui.ui' % moduleDir
        self.ui = load.setup_ui_maya(uiFile, parent)
        
        self.ui.show()
        self.ui.setWindowTitle('%s %s-%s' % (_title, _version, _des))

        self.load_plugins()
        self.init_signals()

    def load_plugins(self): 
        """ turn on plugins """
        plugin_utils.load(plugin_utils.Plugins.animImportExport)


    def init_signals(self): 
        self.ui.get_pushButton.clicked.connect(self.get_ctrl)
        self.ui.rebuild_pushButton.clicked.connect(self.rebuild)

    def get_ctrl(self): 
        """ main ctrl """
        sels = maya_utils.mc.ls(sl=True)
        if sels: 
            ctrl = sels[0]
            self.ui.ctrl_lineEdit.setText(str(ctrl))

    def rebuild(self): 
        """ rebuild asset """
        node = str(self.ui.ctrl_lineEdit.text())
        refPath = maya_utils.get_reference_path(node)
        root = node.split(':')[-1]
        maya_utils.rebuild_asset(root, node)


def show():
    from rftool.utils.ui import maya_win
    logger.info('Run in Maya\n')
    maya_win.deleteUI(uiName)
    myApp = RebuildAsset(maya_win.getMayaWindow())
    return myApp
