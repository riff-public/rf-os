
try:
    from PySide import QtGui, QtCore
    import PySide.QtGui as QtWidgets
except ImportError:
    from PySide2 import QtGui, QtCore, QtWidgets

import pymel.core as pm


''' __________________________Func__________________________ '''
def vray_ex_attr(v_ex, v):
    geo_sh = pm.ls(sl = True, dag = True, s = True)
    # Add V-Ray Extra Attr
    for geo in geo_sh:
        pm.vray("addAttributesFromGroup", geo, "vray_subdivision", v_ex)
    # Enable or disable subdiv attr
    if v_ex == 1:
        for geo in geo_sh:
            geo.setAttr("vraySubdivEnable", v)
            

''' __________________________GUI__________________________ '''

logo_path = ""

class vray_ex_attr_win(QtWidgets.QWidget):
    def __init__(self):
        super(vray_ex_attr_win, self).__init__(None, QtCore.Qt.WindowStaysOnTopHint)
        
        self.setFixedSize(370, 150)
        self.setWindowTitle("RiFF | V-Ray Extra Attrtibutes")
        self.setWindowIcon(QtGui.QIcon(logo_path))

        self.__top_layout = QtWidgets.QGridLayout(self)

        self.build_ui()
        self.connect_signals()

    def build_ui(self):
        
        enb_style_sheet = "background-color:#55aa00;"
        dis_style_sheet = "background-color:#ff0000;"
        
        vr_ex_attr_label = QtWidgets.QLabel("V-Ray Extra Attributes:")
        vr_subdiv_label = QtWidgets.QLabel("V-Ray Subdivision:")
        
        self.enb_vr_ex_btn = QtWidgets.QPushButton("Enable")
        self.enb_vr_ex_btn.setStyleSheet(enb_style_sheet)
        
        self.dis_vr_ex_btn = QtWidgets.QPushButton("Disable")
        self.dis_vr_ex_btn.setStyleSheet(dis_style_sheet)
        
        self.enb_vr_subd_btn = QtWidgets.QPushButton("Enable")
        self.enb_vr_subd_btn.setStyleSheet(enb_style_sheet)
        
        self.dis_vr_subd_btn = QtWidgets.QPushButton("Disable")
        self.dis_vr_subd_btn.setStyleSheet(dis_style_sheet)
        
        self.__top_layout.addWidget(vr_ex_attr_label, 0, 0, 1, 1)
        self.__top_layout.addWidget(vr_subdiv_label, 1, 0, 1, 1)
        self.__top_layout.addWidget(self.enb_vr_ex_btn, 0, 2, 1, 1)
        self.__top_layout.addWidget(self.dis_vr_ex_btn, 0, 3, 1, 1)
        self.__top_layout.addWidget(self.enb_vr_subd_btn, 1, 2, 1, 1)
        self.__top_layout.addWidget(self.dis_vr_subd_btn, 1, 3, 1, 1)

    def connect_signals(self):
        self.enb_vr_ex_btn.clicked.connect(self.enb_ex_cb)
        self.dis_vr_ex_btn.clicked.connect(self.dis_ex_cb)
        self.enb_vr_subd_btn.clicked.connect(self.enb_subd_cb)
        self.dis_vr_subd_btn.clicked.connect(self.dis_subd_cb)
        
    def enb_ex_cb(self):
        vray_ex_attr(1, 1)
    def dis_ex_cb(self):
        vray_ex_attr(0, 0)
    def enb_subd_cb(self):
        vray_ex_attr(1, 1)
    def dis_subd_cb(self):
        vray_ex_attr(1, 0)
                

def run():
    global win
    win = vray_ex_attr_win()
    win.show()
    
