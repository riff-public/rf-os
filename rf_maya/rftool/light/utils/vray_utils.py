import maya.cmds as mc 
import maya.mel as mm 
import sys 
import os 

def add_vray_subdivision(node, value): 
	""" add render as subdivision surface """ 
	command = 'vray_subdivision'
	if mc.objectType(node) == 'mesh': 
		return mm.eval('vray addAttributesFromGroup %s %s %s;' % (node, command, value))

	if mc.objectType(node) == 'transform': 
		shape = mc.listRelatives(node, s=True, f=True)
		if shape: 
			if mc.objectType(shape[0]) == 'mesh': 
				return add_vray_subdivision(shape[0], value)
	