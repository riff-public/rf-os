from PySide2 import QtWidgets, QtCore, QtGui
from shiboken2 import wrapInstance
import pymel.core as pm
import maya.cmds as cmds

import maya.app.renderSetup.model.renderSetup as renderSetup
import maya.app.renderSetup.model.collection as collection
import maya.api.OpenMaya as OpenMaya

import os
import sys
import json

core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)

from rf_utils.ui.flatUi import main_ui
reload(main_ui)


class CameraOverscan_ui(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(CameraOverscan_ui, self).__init__(parent)


        self.__top_layout = QtWidgets.QGridLayout(self)

        self.cam, self.camShape, self.currentRatio, self.w, self.h, self.percentage = self.getCurrentCam()

        self.prj_w = 1920
        self.prj_h = 1080

        self.overscanW = int(self.w * self.percentage  / 100 + self.w)
        self.overscanH = int(self.h * self.percentage  / 100 + self.h)

        self.percentage = self.overscanW * 100 / self.prj_w - 100

        self.__build_ui()
        self.__connect_signals()


    def __build_ui(self):
        cam_label = QtWidgets.QLabel("Cam :", self)
        camName_label = QtWidgets.QLabel(self.camShape, self)
        overscan_label = QtWidgets.QLabel("Overscan :", self)
        self.slider = QtWidgets.QSlider(QtCore.Qt.Horizontal, self)
        self.slider.setRange(0, 100)
        self.slider.setValue(self.percentage)
        self.percentage_label = QtWidgets.QLabel("{}%".format(self.percentage), self)
        self.res_label = QtWidgets.QLabel("{}, {}".format(self.overscanW, self.overscanH), self)

        self.apply_btn = QtWidgets.QPushButton("Apply Overscan")
        self.apply_btn.setObjectName("blueBtn")

        self.__top_layout.addWidget(cam_label, 0, 0, 1, 1)
        self.__top_layout.addWidget(camName_label, 0, 1, 1, 1)
        self.__top_layout.addWidget(overscan_label, 1, 0, 1, 1)
        self.__top_layout.addWidget(self.slider, 1, 1, 1, 1)
        self.__top_layout.addWidget(self.percentage_label, 1, 2, 1, 1)
        self.__top_layout.addWidget(self.res_label, 1, 3, 1, 1)
        self.__top_layout.addWidget(QHLine(), 2, 0, 1, 4)
        self.__top_layout.addWidget(self.apply_btn, 3, 2, 1, 2)

    def __connect_signals(self):
        self.slider.valueChanged.connect(self.getPercentage)
        self.apply_btn.clicked.connect(self.apply)

    def getPercentage(self, v):
        self.ratioValue = v / 100.0 + 1.0
        cmds.setAttr("{}.lensSqueezeRatio".format(self.camShape), self.ratioValue)
        self.percentage_label.setText("{}%".format(v))
        self.overscanW = int(self.prj_w * v  / 100 + self.prj_w)
        self.overscanH = int(self.prj_h * v  / 100 + self.prj_h)
        self.res_label.setText("{}, {}".format(self.overscanW, self.overscanH))

    def apply(self):
        cmds.setAttr("defaultResolution.w", l=False)
        cmds.setAttr("defaultResolution.h", l=False)
        cmds.setAttr("defaultResolution.w", self.overscanW)
        cmds.setAttr("defaultResolution.h", self.overscanH)
        cmds.setAttr("{}.lensSqueezeRatio".format(self.camShape), self.currentRatio)
        cmds.setAttr("{}.cameraScale".format(self.camShape), self.ratioValue)


    def getCurrentCam(self):
        cam = cmds.ls(sl=True)
        if not cam:
            return

        cam = cam[0]
        camShape = cmds.listRelatives(cam)
        if not camShape:
            return

        camShape = camShape[0]
        if cmds.nodeType(camShape) != "camera":
            return

        currentRatio = cmds.getAttr("{}.lensSqueezeRatio".format(camShape))
        w = cmds.getAttr("defaultResolution.w")
        h = cmds.getAttr("defaultResolution.h")
        percentage = round(currentRatio % 1 * 100, 3)

        return cam, camShape, currentRatio, w, h, percentage



class QHLine(QtWidgets.QFrame):
    """docstring for QHLine"""
    def __init__(self):
        super(QHLine, self).__init__()
        self.setFrameShape(QtWidgets.QFrame.HLine)
        self.setFrameShadow(QtWidgets.QFrame.Sunken)
        

def runUI(ui_name="CameraOverscan_ui", size=(360, 110), title="Camera Overscan", parent=None):
    if pm.window(ui_name, exists=True):
        pm.deleteUI(ui_name)
        
    win = main_ui.MainWindow(ui_name=ui_name, size=size, title=title, widget=CameraOverscan_ui(), parent=parent)
    win.show()
    