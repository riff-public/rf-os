''' Convert Redshift Light to Arnold Light v0.01
Developed by Kathawut Poldet
RiFF Animation Studio '''


import pymel.core as pm
from mtoa.core import createArnoldNode

def rsLight_to_arLight():
    ar_light_ls = []
    rs_lights_ls = pm.ls(sl = True)
    if rs_lights_ls == []:
        pm.warning("Please select some Redshift Light...")
        return
    for rs in rs_lights_ls:
        # Redshift
        rs_light_tr = rs.getTranslation()
        rs_light_rot = rs.getRotation()
        rs_light_scl = rs.getScale()
        
        rs_light_shp = rs.getShape()
        rs_light_color_mode = rs_light_shp.getAttr("colorMode")
        rs_light_color_inten = rs_light_shp.getAttr("intensity")
        rs_light_color_vector = rs_light_shp.getAttr("color")
        rs_light_color_tmp = rs_light_shp.getAttr("temperature")
        
        # Arnold
        ar_light_trs = createArnoldNode("aiAreaLight", name = "aiAreaLightShape1")
        ar_light = ar_light_trs.getShape()
        
        ar_light_trs.setTranslation(rs_light_tr)
        ar_light_trs.setRotation(rs_light_rot)
        ar_light_trs.setScale(rs_light_scl)
        
        ar_light.setAttr("color", rs_light_color_vector)
        ar_light.setAttr("intensity", rs_light_color_inten)
        
        if rs_light_color_mode == 1:
            ar_light.setAttr("aiUseColorTemperature", 1)
            ar_light.setAttr("aiColorTemperature", rs_light_color_tmp)
            
        ar_light_ls.append(ar_light)
    
    if pm.objExists("ar_lights_grp"):
        pm.parent(ar_light_ls, "ar_lights_grp")
    else:
        pm.group(ar_light_ls, n = "ar_lights_grp")

def run():
    rsLight_to_arLight()

