from PySide2 import QtWidgets
import shiboken2
import sys

import pymel.core as pm
from maya import OpenMayaUI as omui

ATTRS_DATA = {"V-Ray": {"VRayMtl": {"diffuse": "diffuseColor", "reflection": "reflectionColorAmount", "bump": "bumpMap"}},
                "Redshift": {"RedshiftMaterial": {"diffuse": "diffuse_color", "reflection": "refl_weight"}},
                "Arnold": {"alSurface": {"diffuse": "diffuseColor", "reflection": "specular1Strength"}},
                "Default": {"lambert": {"diffuse": "color"},
                            "blinn": {"diffuse": "color", "reflection": "reflectivity"},
                            "phong": {"diffuse": "color", "reflection": "reflectivity"}}}

# DONE!! TO DO: switch other attr such as reflection, bump, normal .... and so on (now it's working on diffuse attribute only!)

# TO DO2:  If there are more than 2 items in each Renderer. UI will pop up button named by item in the dictionary key
# e.g. Arnold [alSurface Button] and [aiStandard Button]

class ShaderSwitcherUI(QtWidgets.QDialog):
    def __init__(self, parent=None):
        super(ShaderSwitcherUI, self).__init__(parent=parent)

        self.setFixedSize(250, 135)
        self.setWindowTitle("Shader Switcher")
        self.__top_layout = QtWidgets.QGridLayout(self)

        self.rendererList = ["V-Ray", "Redshift", "Arnold"]
        self.selectedRenderer = "V-Ray"
        self.assign = True

        self.__buildUI()
        self.__connectSignals()

    def __buildUI(self):
        convertLabel = QtWidgets.QLabel("Convert to:")
        self.__top_layout.addWidget(convertLabel, 0, 0, 1, 1)

        self.rendererComboBox = QtWidgets.QComboBox()
        self.__top_layout.addWidget(self.rendererComboBox, 0, 1, 1, 3)
        self.rendererComboBox.addItems(self.rendererList)

        self.assignChkBox = QtWidgets.QCheckBox("Assign to Object?")
        self.assignChkBox.setChecked(True)
        self.__top_layout.addWidget(self.assignChkBox, 1, 0, 1, 1)

        self.gammaChkBox = QtWidgets.QCheckBox("Gamma (soon!)")
        self.gammaChkBox.setEnabled(False)
        self.__top_layout.addWidget(self.gammaChkBox, 1, 2, 1, 1)

        self.convertBtn = QtWidgets.QPushButton("Convert it!")
        self.__top_layout.addWidget(self.convertBtn, 2, 0, 1, 4)

    def __connectSignals(self):
        self.rendererComboBox.currentIndexChanged.connect(self.__renderer_cb)
        self.assignChkBox.stateChanged.connect(self.__assign_cb)
        self.convertBtn.clicked.connect(self.__convertBtn_cb)

    def __renderer_cb(self):
        self.selectedRenderer = self.rendererComboBox.currentText()
        return self.selectedRenderer

    def __assign_cb(self):
        self.assign = self.assignChkBox.isChecked()
        return self.assign

    def __convertBtn_cb(self):
        renderer = self.selectedRenderer
        ShaderSwitcher().shaderSwitcher(renderer, assign=self.assign)

# This class will create material based on user selection. And assign the material to each selected object
class ShaderSwitcher():
    # create file node
    def createFileNode(self, node_name, path=None):
        file_node = pm.shadingNode("file", asTexture=True, isColorManaged=True, n=node_name)
        p2d_node = pm.shadingNode("place2dTexture", asUtility=True, n=node_name + "_p2d")

        p2d_node.connectAttr("outUV", "{}.uvCoord".format(file_node))
        p2d_node.connectAttr("outUvFilterSize", "{}.uvFilterSize".format(file_node))
        p2d_node.connectAttr("vertexCameraOne", "{}.vertexCameraOne".format(file_node))
        p2d_node.connectAttr("vertexUvOne", "{}.vertexUvOne".format(file_node))
        p2d_node.connectAttr("vertexUvThree", "{}.vertexUvThree".format(file_node))
        p2d_node.connectAttr("vertexUvTwo", "{}.vertexUvTwo".format(file_node))
        p2d_node.connectAttr("coverage", "{}.coverage".format(file_node))
        p2d_node.connectAttr("mirrorU", "{}.mirrorU".format(file_node))
        p2d_node.connectAttr("mirrorV", "{}.mirrorV".format(file_node))
        p2d_node.connectAttr("noiseUV", "{}.noiseUV".format(file_node))
        p2d_node.connectAttr("offset", "{}.offset".format(file_node))
        p2d_node.connectAttr("repeatUV", "{}.repeatUV".format(file_node))
        p2d_node.connectAttr("rotateUV", "{}.rotateUV".format(file_node))
        p2d_node.connectAttr("translateFrame", "{}.translateFrame".format(file_node))
        p2d_node.connectAttr("stagger", "{}.stagger".format(file_node))
        p2d_node.connectAttr("rotateFrame", "{}.rotateFrame".format(file_node))
        p2d_node.connectAttr("wrapV", "{}.wrapV".format(file_node))
        p2d_node.connectAttr("wrapU", "{}.wrapU".format(file_node))

        if path is not None:
            file_node.setAttr("fileTextureName", path)

        return file_node

    # get shading group and obj are connections together
    # e.g. {blinn1SG: [obj1, obj2, obj3],
    #        blinn2SG: [obj4, obj5]}
    def getMaterialAndObj(self, obj_ls):
        sg_ls = []
        data = {}
        
        for geo in obj_ls:
            sg = geo.shadingGroups()
            if sg in sg_ls:
                continue
               
            objChild = pm.ls(sg[0].listConnections(), tr=True)
            
            sg_ls.append(sg)
            data.update({sg[0]: objChild})
            
        yield data
    

    # get material info based on SG. and return Renderer, Material type and Material node type (pymel.core.nt)
    # e.g. (V-Ray, VRayMtl, VRayMtl1)
    def getMaterialInfo(self, sg, d=ATTRS_DATA):
        matName = pm.ls(sg.listConnections(), mat=True)[0]
        matType = matName.nodeType()
        renderer = ""
        
        for k, v in d.iteritems():
            for mt in v:
                if mt == matType:
                    renderer = k
    
        return (renderer, matType, matName)

    # get file texture path and type of the path file (diffuse, reflection, bump...) from given materials
    def getFileTxPath(self, renderer, matType, matName, d=ATTRS_DATA):
        mat = d[renderer][matType]
        
        for k, v in mat.iteritems():
            fileNode = pm.listConnections("{}.{}".format(matName, v))

            if not fileNode:
                continue
            
            if fileNode[0].nodeType() == "gammaCorrect":
                fileNode = fileNode[0].listConnections(type="file")

            
            txPath = fileNode[0].getAttr("fileTextureName")
            nodeName = fileNode[0].nodeName().split("_")[0]
            
            yield (k, nodeName, txPath)


    # switching shader
    def shaderSwitcher(self, renderer, assign=True, d=ATTRS_DATA):
        geoLs = pm.ls(sl=True)
        if geoLs == []:
            return
        for data in self.getMaterialAndObj(geoLs):
            for matSG, obj_ls in data.iteritems():
                
                renderers, matType, matName = self.getMaterialInfo(matSG)
                newMaterialType = d[renderer].keys()[0]
                newMaterial, newSg = pm.createSurfaceShader(newMaterialType)

                i = 0
                for attrType, nodeName, txPath in self.getFileTxPath(renderers, matType, matName):
                    # Rename the new material, sg only on the first loop
                    if i == 0:
                        newMaterial.rename("{}_shd".format(nodeName))
                        newSg.rename("{}_sg".format(newMaterial))
                                    
                    newTxNode = self.createFileNode(nodeName, txPath)
                    
                    newMaterialAttr = d[renderer][newMaterialType][attrType]
                    if attrType == "reflection":
                        outputAttr = "outAlpha"
                    else:
                        outputAttr = "outColor"

                    newTxNode.connectAttr(outputAttr, "{}.{}".format(newMaterial, newMaterialAttr), force=True)

                    i += 1
                
                
                # assign new material to object
                if assign is True:
                    pm.select(obj_ls)
                    pm.hyperShade(assign=newSg)

                print "Convert shader from {} to {}".format(renderers, renderer)
                pm.select(cl=True)


def getMayaMainWindow():
    ptr = omui.MQtUtil.mainWindow()
    mayaWindow = shiboken2.wrapInstance(long(ptr), QtWidgets.QMainWindow)
    return mayaWindow


def show():
    # check if our ui is exists then delete
    try:
        pm.deleteUI("ShaderSwitcher")
    except:
        pass

    parent = QtWidgets.QWidget(parent=getMayaMainWindow())
    parent.setObjectName("ShaderSwitcher")

    win = ShaderSwitcherUI(parent=parent)
    win.show()
