# Import Modules maya <= 17 #

try:
    from PySide import QtGui, QtCore
    import PySide.QtGui as QtWidgets
except ImportError:
    from PySide2 import QtGui, QtCore, QtWidgets

import pymel.core as pm


# ----------------------------------------------------------------- #
# -------------------- Override Shading Groups -------------------- #
# ----------------------------------------------------------------- #

def create_surface_shd(name):
    if not pm.objExists(name):
        shd = pm.shadingNode("surfaceShader", asShader = True, n = name)
        sg = pm.sets(n = name + "SG", renderable = True, noSurfaceShader = True, empty = True)
        shd.connectAttr("outColor", "{}.surfaceShader".format(sg))
        if name == "RED_SHD":
            shd.setAttr("outColor", (1, 0, 0))
        if name == "GREEN_SHD":
            shd.setAttr("outColor", (0, 1, 0))
        if name == "BLUE_SHD":
            shd.setAttr("outColor", (0, 0, 1))
        if name == "BLACK_SHD":
            shd.setAttr("outColor", (0, 0, 0))

def get_sg():
    obj_ls = pm.ls(sl = True)
    for obj in obj_ls:
        sh = obj.getShape()
        sg = sh.shadingGroups()
        
        yield sg
        
def override_sg(shd):
    if pm.editRenderLayerGlobals(query=True, currentRenderLayer=True) == "defaultRenderLayer":
        return pm.warning("Are you in masterLayer ?")
    for sg in get_sg():
        sg = sg[0]
        pm.editRenderLayerAdjustment("{}.surfaceShader".format(sg))
        pm.connectAttr("{}.outColor".format(shd), "{}.surfaceShader".format(sg), force = True)
        
        
def remove_override():
    if pm.editRenderLayerGlobals(query=True, currentRenderLayer=True) == "defaultRenderLayer":
        return pm.warning("Are you in masterLayer ?")
    for sg in get_sg():
        sg = sg[0]
        pm.editRenderLayerAdjustment("{}.surfaceShader".format(sg), remove = True)

        
# --------------------------------------------- #
# -------------------- GUI -------------------- #
# --------------------------------------------- #

class overide_sg_win(QtWidgets.QWidget):
    def __init__(self):
        super(overide_sg_win, self).__init__(None, QtCore.Qt.WindowStaysOnTopHint)

        self.setFixedSize(340, 200)
        self.setWindowTitle("MAYA | Override SG")

        self.__top__layout = QtWidgets.QGridLayout(self)
        self.__build_ui()
        self.__connect_signals()
        

    def __build_ui(self):
        override_sg_grp = QtWidgets.QGroupBox("Override Shading Groups :", self)
        override_sg_layout = QtWidgets.QGridLayout(override_sg_grp)
        self.__top__layout.addWidget(override_sg_grp, 0, 0, 1, 3)
        self.create_rgb_btn = QtWidgets.QPushButton("Create Surface Shader RGB", self)
        self.red_btn = QtWidgets.QPushButton("RED", self)
        self.green_btn = QtWidgets.QPushButton("GREEN", self)
        self.blue_btn = QtWidgets.QPushButton("BLUE", self)
        self.black_btn = QtWidgets.QPushButton("BLACK", self)
        self.remove_override_btn = QtWidgets.QPushButton("Remove Override", self)
        override_sg_layout.addWidget(self.create_rgb_btn, 0, 0, 1, 4)
        override_sg_layout.addWidget(self.red_btn, 1, 0, 1, 1)
        override_sg_layout.addWidget(self.green_btn, 1, 1, 1, 1)
        override_sg_layout.addWidget(self.blue_btn, 1, 2, 1, 1)
        override_sg_layout.addWidget(self.black_btn, 1, 3, 1, 1)
        override_sg_layout.addWidget(self.remove_override_btn, 2, 1, 1, 2)

        custom_shd_label = QtWidgets.QLabel("Custom Shader :", self)
        self.custom_shd_input = QtWidgets.QLineEdit(self)
        self.custom_shd_btn = QtWidgets.QPushButton("Override", self)
        self.__top__layout.addWidget(custom_shd_label, 3, 0, 1, 1)
        self.__top__layout.addWidget(self.custom_shd_input, 3, 1, 1, 1)
        self.__top__layout.addWidget(self.custom_shd_btn, 3, 2, 1, 1)

    def __connect_signals(self):
        self.create_rgb_btn.clicked.connect(self.create_rgb_cb)
        self.red_btn.clicked.connect(self.red_cb)
        self.green_btn.clicked.connect(self.green_cb)
        self.blue_btn.clicked.connect(self.blue_cb)
        self.black_btn.clicked.connect(self.black_cb)
        self.remove_override_btn.clicked.connect(self.remove_override_cb)
        self.custom_shd_btn.clicked.connect(self.custom_shd_cb)

        self.custom_shd_input.textChanged.connect(self.custom_shd_input_cb)

    def custom_shd_input_cb(self, text):
        self.custom_shd = text
        return self.custom_shd

    def create_rgb_cb(self):
         create_surface_shd("RED_SHD")
         create_surface_shd("GREEN_SHD")
         create_surface_shd("BLUE_SHD")
         create_surface_shd("BLACK_SHD")

    def red_cb(self):
        override_sg("RED_SHD")

    def green_cb(selfs):
        override_sg("GREEN_SHD")

    def blue_cb(self):
        override_sg("BLUE_SHD")

    def black_cb(self):
        override_sg("BLACK_SHD")

    def remove_override_cb(self):
        remove_override()

    def custom_shd_cb(self):
        override_sg(self.custom_shd)


def run():
    global win
    win = overide_sg_win()
    win.show()


