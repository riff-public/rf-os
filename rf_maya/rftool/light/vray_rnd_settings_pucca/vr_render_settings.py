import pymel.core as pm
import maya.mel as mel
import maya.cmds as cmds

# Locate Light preset file
light_preset_path = "P:\\pucca\\all\\research\\No'\\Template\\Light_Set\\"
light_preset_file = "Light_Set_V001-Int.ma"
#light_ext_preset = "Light_Set_V001-Ext.ma"
light_preset = light_preset_path + light_preset_file


class vr_render_settings(object):
    def __init__(self, res = (1920,1080)):
        self.res = res
        drg = pm.PyNode("defaultRenderGlobals")
        if pm.getAttr("defaultRenderGlobals.ren") != "vray":
            drg.ren.set("vray")
            pm.warning("Current Renderer has been set to V-Ray")
        self.vr = pm.PyNode("vraySettings")

    def fix_prePost(self):
        pm.setAttr("defaultRenderGlobals.postMel", "")
        pm.setAttr("defaultRenderGlobals.preMel", "")
        pm.setAttr("defaultRenderGlobals.preRenderLayerMel", "")
        pm.setAttr("defaultRenderGlobals.postRenderLayerMel", "")
        pm.setAttr("defaultRenderGlobals.preRenderMel", "")
        pm.setAttr("defaultRenderGlobals.postRenderMel", "")
        pm.setAttr("vraySettings.preKeyframeMel", "")
        pm.setAttr("vraySettings.rtImageReadyMel", "")

        pm.setAttr("defaultRenderGlobals.preMel", l = True)
        pm.setAttr("defaultRenderGlobals.postMel", l = True)
        pm.setAttr("defaultRenderGlobals.preRenderLayerMel", l = True)
        pm.setAttr("defaultRenderGlobals.postRenderLayerMel", l = True)
        pm.setAttr("defaultRenderGlobals.preRenderMel", l = True)
        pm.setAttr("defaultRenderGlobals.postRenderMel", l = True)
        pm.setAttr("vraySettings.preKeyframeMel", l = True)
        pm.setAttr("vraySettings.rtImageReadyMel", l = True)
            
        

    def obj_exists(f):
        def warper(self, aov, name):
            if not pm.objExists(name):
                result = f(self, aov, name)
                return result
        return warper

    @obj_exists
    def import_light_preset(self, aov, name):
        cmds.file(light_preset, i = True)
    
    def common_tab(self):
        # Prefix
        #file_name = mel.eval("file -q -sn -shn;")
        try:
            ep, sh = mel.eval("file -q -sn;").split("/")[5].split("_")
            file_name_prefix = "{}/{}_{}/images/{}_{}_render_v01/<Layer>/<Layer>".format(ep,ep,sh,ep,sh)
            self.vr.fileNamePrefix.set(file_name_prefix)
        except:
            pm.warning("Something wrong with file name... Please set file name prefix by your self and let me know thanks!!.")
        self.vr.imageFormatStr.set("exr (multichannel)")
        
        # Animation
        self.vr.animType.set(1)
        self.vr.animBatchOnly.set(1)
        start_frame = pm.playbackOptions(animationStartTime = True, q = True)
        end_frame = pm.playbackOptions(animationEndTime = True, q = True)
        pm.setAttr("defaultRenderGlobals.startFrame", start_frame)
        pm.setAttr("defaultRenderGlobals.endFrame", end_frame)
    
        # Renderable Camera
        default_cam = ["frontShape", "perspShape", "sideShape", "topShape"]
        cam_ls = pm.ls(cameras = True)
        renderable_cam = [cam for cam in cam_ls if cam not in default_cam][0]
        
        for cam in default_cam:
            pm.setAttr("{}.renderable".format(cam), 0)
        pm.setAttr("{}.renderable".format(renderable_cam), 1)
        
        self.vr.width.set(self.res[0])
        self.vr.height.set(self.res[1])
            
    def vray_tab(self):
        self.vr.samplerType.set(4)
        self.vr.minShadeRate.set(2)
        self.vr.aaFilterOn.set(1)
        self.vr.aaFilterType.set(1)
        self.vr.aaFilterSize.set(2)
        self.vr.dmcMinSubdivs.set(10)
        self.vr.dmcMaxSubdivs.set(20)
        self.vr.dmcThreshold.set(0.005)
        self.vr.dmcs_timeDependent.set(1)
        self.vr.dmcs_useLocalSubdivs.set(1)
        self.vr.dmcs_subdivsMult.set(1)
        self.vr.dmcs_adaptiveAmount.set(0.900)
        self.vr.dmcs_adaptiveThreshold.set(0.010)
        self.vr.dmcs_adaptiveMinSamples.set(8)
        self.vr.cmap_type.set(6)
        self.vr.cmap_darkMult.set(1)
        self.vr.cmap_brightMult.set(1)
        self.vr.cmap_gamma.set(2.2)
        self.vr.cmap_affectBackground.set(1)
        self.vr.cmap_adaptationOnly.set(1)
        self.vr.cmap_clampOutput.set(1)
        self.vr.cmap_clampLevel.set(1)
        self.vr.cmap_subpixelMapping.set(1)
        
        
    def gi_tab(self):
        self.vr.giOn.set(1)
        self.vr.reflectiveCaustics.set(0)
        self.vr.refractiveCaustics.set(1)
        self.vr.primaryEngine.set(2)
        self.vr.primaryMultiplier.set(0.9)
        self.vr.secondaryEngine.set(3)
        self.vr.secondaryMultiplier.set(0.7)
        self.vr.saturation.set(1)
        self.vr.contrast.set(1)
        self.vr.contrastBase.set(0.5)
        self.vr.dmc_subdivs.set(16)
        self.vr.subdivs.set(1200)
        self.vr.sampleSize.set(0.010)
        self.vr.showCalcPhase.set(1)
        self.vr.storeDirectLight.set(1)
        self.vr.worldScale.set(0)
        self.vr.useForGlossy.set(1)
        self.vr.useCameraPath.set(0)
        self.vr.filterType.set(1)
        self.vr.prefilter.set(1)
        self.vr.prefilterSamples.set(35)
        self.vr.depth.set(100)
        self.vr.filterSamples.set(5)
        self.vr.leakPrevention.set(0.8)
        self.vr.useRetraceThreshold.set(1)
        self.vr.retraceThreshold.set(2)

    def override_tab(self):
        self.vr.globopt_render_viewport_subdivision.set(1)

    def rnd_ele_tab(self):
        self.vr.relements_separateFolders.set(1)
    
    def settings_tab(self):
        self.vr.ddisplac_maxSubdivs.set(4)
            
    @obj_exists
    def rnd_elements_tab(self, aov, name):
        mel.eval("vrayAddRenderElement %s;" % aov)

    @obj_exists
    def vr_extra_ao(self, aov, name):
        ao_vr_ex = mel.eval("vrayAddRenderElement %s;" % aov)
        ao_vr_ex = pm.rename(ao_vr_ex, name)
        pm.setAttr("{}.vray_name_extratex".format(ao_vr_ex), "Occ", type = "string")
        vr_dirt_tx = pm.shadingNode("VRayDirt", asTexture = True)
        vr_dirt_tx.setAttr("radius", 2.5)
        p2d_tx = pm.shadingNode("place2dTexture", asUtility = True)
        pm.connectAttr("{}.outUV".format(p2d_tx), "{}.uv".format(vr_dirt_tx))
        pm.connectAttr("{}.outUvFilterSize".format(p2d_tx), "{}.uvFilterSize".format(vr_dirt_tx))
        pm.connectAttr("{}.outColor".format(vr_dirt_tx), "{}.vray_texture_extratex".format(ao_vr_ex), force = True)
    
    @obj_exists
    def vr_extra_uv(self, aov, name):
        uv_vr_ex = mel.eval("vrayAddRenderElement %s;" % aov)
        uv_vr_ex = pm.rename(uv_vr_ex, name)
        pm.setAttr("{}.vray_name_extratex".format(uv_vr_ex), "UV", type = "string")
        samplerInfo_tx = pm.shadingNode("samplerInfo", asUtility = True)
        pm.connectAttr("{}.uCoord".format(samplerInfo_tx), "{}.vray_texture_extratexR".format(uv_vr_ex))
        pm.connectAttr("{}.vCoord".format(samplerInfo_tx), "{}.vray_texture_extratexG".format(uv_vr_ex))

    @obj_exists
    def vr_extra_light_select(self, light_name, name):
        try:
            light = pm.PyNode(light_name)
        except pm.MayaNodeError:
            return pm.warning("Key_Light or ENV_Light doesn't exists. Please Make sure to rename your light to'Key_Light' or 'ENV_Light'")
        light_vr_ex = mel.eval("vrayAddRenderElement LightSelectElement;")
        light_vr_ex = pm.rename(light_vr_ex, "vrayRE_{}_Select".format(str(light)))
        pm.setAttr("{}.vray_name_lightselect".format(light_vr_ex), light, type = "string")
        light_to_set = pm.select(light)
        pm.sets(light_vr_ex.nodeName(), light_to_set, edit = True, forceElement = True)
        mel.eval("select -cl")
  
