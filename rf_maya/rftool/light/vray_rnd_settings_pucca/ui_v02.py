'''v002 update interior, exterior light
    new prefix'''

try:
    from PySide import QtGui, QtCore
    import PySide.QtGui as QtWidgets
except ImportError:
    from PySide2 import QtGui, QtCore, QtWidgets

import os
import pymel.core as pm

import vr_render_settings_v02
reload(vr_render_settings_v02)
from vr_render_settings_v02 import vr_render_settings

#from vr_render_settings import vr_render_settings
#reload(vr_render_settings)

dir = os.path.dirname(__file__)
logo_path = os.path.join(dir, "icon\\rifficon_small.png")

class vr_rnd_settings_ui(QtWidgets.QWidget):
    def __init__(self):
        super(vr_rnd_settings_ui, self).__init__(None, QtCore.Qt.WindowStaysOnTopHint)
        
        self.setFixedSize(470, 280)
        self.setWindowTitle("RiFF | V-Ray Render Settings (PUCCA)")
        self.setWindowIcon(QtGui.QIcon(logo_path))

        self.__top_layout = QtWidgets.QGridLayout(self)

        self.build_ui()
        self.connect_signals()

    def build_ui(self):

        # Create tab widget #

        self.tab_widget = QtWidgets.QTabWidget(self)
        self.__top_layout.addWidget(self.tab_widget)
        self.settings_tab = QtWidgets.QWidget(self)
        self.settings_tab_layout = QtWidgets.QGridLayout(self.settings_tab)
        self.tab_widget.addTab(self.settings_tab, "Render Settings")

        self.utl_tab = QtWidgets.QWidget(self)
        self.utl_tab_layout = QtWidgets.QGridLayout(self.utl_tab)
        self.tab_widget.addTab(self.utl_tab, "Utility")

        # render settings tab #

        btn_style_sheet = "background-color:#55aa00; \nfont: 75 10pt 'Nirmala UI';"
        btn_style_sheet_2 = "background-color:#37a2ff; \nfont: 75 10pt 'Nirmala UI';"
        btn_style_sheet_3 = "background-color:#ffa323; \nfont: 75 10pt 'Nirmala UI';"
        
        note_grp = QtWidgets.QGroupBox("Important Note !!")
        note_grp_layout = QtWidgets.QVBoxLayout(note_grp)
        st_note = QtWidgets.QLabel("*** Make sure to delete all Cameras Except Renderable camera ***")
        st_note.setAlignment(QtCore.Qt.AlignCenter)
        line = QtWidgets.QFrame()
        line.setFrameShape(QtWidgets.QFrame.HLine)
        line.setFrameShadow(QtWidgets.QFrame.Sunken)
        nd_note = QtWidgets.QLabel("Import light by clicking light preset button")
        nd_note.setAlignment(QtCore.Qt.AlignCenter)
        line_2 = QtWidgets.QFrame()
        line_2.setFrameShape(QtWidgets.QFrame.HLine)
        line_2.setFrameShadow(QtWidgets.QFrame.Sunken)
        rd_note = QtWidgets.QLabel("Prefix set to : epxx/epxx_xxxx/images/epxx_xxxx_render_v01/<Layer>/<Layer> \nImage format : *.tga")
        rd_note.setAlignment(QtCore.Qt.AlignCenter)
        line_3 = QtWidgets.QFrame()
        line_3.setFrameShape(QtWidgets.QFrame.HLine)
        line_3.setFrameShadow(QtWidgets.QFrame.Sunken)
        
        note_grp_layout.addWidget(st_note)
        note_grp_layout.addWidget(line)
        note_grp_layout.addWidget(nd_note)
        note_grp_layout.addWidget(line_2)
        note_grp_layout.addWidget(rd_note)
        note_grp_layout.addWidget(line_3)
        
        self.settings_tab_layout.addWidget(note_grp, 0, 0, 1, 3)
        
        self.vr_btn = QtWidgets.QPushButton("V-Ray Render Settings")
        self.vr_btn.setStyleSheet(btn_style_sheet)
        self.rnd_ele_btn = QtWidgets.QPushButton("Render Elements")
        self.rnd_ele_btn.setStyleSheet(btn_style_sheet_2)
        self.vr_light_sl_btn = QtWidgets.QPushButton("Light Select")
        self.vr_light_sl_btn.setStyleSheet(btn_style_sheet_3)
        self.vr_int_light_btn = QtWidgets.QPushButton("Import Light")
        self.vr_ext_light_btn = QtWidgets.QPushButton("Exterior Light")
        self.vr_ext_light_btn.setEnabled(False)

        import_light_note = QtWidgets.QLabel("Import Light Preset :")
        
        self.settings_tab_layout.addWidget(import_light_note, 1, 0, 1, 1)
        self.settings_tab_layout.addWidget(self.vr_int_light_btn, 1, 1, 1, 1)
        self.settings_tab_layout.addWidget(self.vr_ext_light_btn, 1, 2, 1, 1)
        self.settings_tab_layout.addWidget(self.vr_btn, 2, 0, 1, 1)
        self.settings_tab_layout.addWidget(self.rnd_ele_btn, 2, 1, 1, 1)
        self.settings_tab_layout.addWidget(self.vr_light_sl_btn, 2, 2, 1, 1)

        # utility tab #

        r_style_sheet = "background-color:#ff0000; \nfont: 12pt;"
        g_style_sheet = "background-color:#008400; \nfont: 12pt;"
        b_style_sheet = "background-color:#0000ff; \nfont: 12pt;"

        overide_sg_grp = QtWidgets.QGroupBox("Overide SG :", self)
        self.utl_tab_layout.addWidget(overide_sg_grp)
        overide_sg_layout = QtWidgets.QGridLayout(overide_sg_grp)
        self.id_r_btn = QtWidgets.QPushButton("ID RED")
        self.id_r_btn.setStyleSheet(r_style_sheet)
        overide_sg_layout.addWidget(self.id_r_btn, 0, 0, 1, 3)
        self.id_g_btn = QtWidgets.QPushButton("ID GREEN")
        self.id_g_btn.setStyleSheet(g_style_sheet)
        overide_sg_layout.addWidget(self.id_g_btn, 1, 0, 1, 3)
        self.id_b_btn = QtWidgets.QPushButton("ID BLUE")
        self.id_b_btn.setStyleSheet(b_style_sheet)
        overide_sg_layout.addWidget(self.id_b_btn, 2, 0, 1, 3)

        custom_sg_note = QtWidgets.QLabel("Custom Material :")
        overide_sg_layout.addWidget(custom_sg_note, 3, 0, 1, 1)

        self.custom_sg_input = QtWidgets.QLineEdit()
        overide_sg_layout.addWidget(self.custom_sg_input, 3, 1, 1, 1)

        self.custom_sg_btn = QtWidgets.QPushButton("Create Custom SG")
        overide_sg_layout.addWidget(self.custom_sg_btn, 3, 2, 1, 1)


        rnd_element_grp = QtWidgets.QGroupBox("Render Elements On/Off :", self)
        self.utl_tab_layout.addWidget(rnd_element_grp)
        rnd_element_layout = QtWidgets.QHBoxLayout(rnd_element_grp)

        self.rnd_ele_on_btn = QtWidgets.QPushButton("Enable")
        self.rnd_ele_off_btn = QtWidgets.QPushButton("Disable")
        rnd_element_layout.addWidget(self.rnd_ele_on_btn)
        rnd_element_layout.addWidget(self.rnd_ele_off_btn)


    def connect_signals(self):
        self.vr_btn.clicked.connect(self.vr_cb)
        self.rnd_ele_btn.clicked.connect(self.rnd_ele_cb)
        self.vr_light_sl_btn.clicked.connect(self.vr_light_sl_cb)
        self.vr_int_light_btn.clicked.connect(self.vr_int_light_cb)
        #self.vr_ext_light_btn.clicked.connect(self.vr_ext_light_cb)

    def vr_int_light_cb(self):
        vr_render_settings().import_light_preset("Light_Set_Int")

    def vr_cb(self):
        vr_render_settings().common_tab()
        vr_render_settings().vray_tab()
        vr_render_settings().gi_tab()
        vr_render_settings().settings_tab()
        vr_render_settings().rnd_ele_tab()
        
    def rnd_ele_cb(self):
        vr_render_settings().rnd_elements_tab("diffuseChannel", "vrayRE_Diffuse")
        vr_render_settings().rnd_elements_tab("lightingChannel", "vrayRE_Lighting")
        vr_render_settings().rnd_elements_tab("giChannel", "vrayRE_GI")
        vr_render_settings().rnd_elements_tab("reflectChannel", "vrayRE_Reflection")
        vr_render_settings().rnd_elements_tab("refractChannel", "vrayRE_Refraction")
        vr_render_settings().rnd_elements_tab("specularChannel", "vrayRE_Specular")
        vr_render_settings().rnd_elements_tab("shadowChannel", "vrayRE_Shadow")
        vr_render_settings().rnd_elements_tab("normalsChannel", "vrayRE_Normals")
        vr_render_settings().rnd_elements_tab("zdepthChannel", "vrayRE_Z_depth")
        
        vr_render_settings().vr_extra_ao("ExtraTexElement", "vrayRE_Occ")
        vr_render_settings().vr_extra_uv("ExtraTexElement", "vrayRE_UV")
        
    def vr_light_sl_cb(self):
        vr_render_settings().vr_extra_light_select("Key_Light", "vrayRE_Key_Light_Select")
        vr_render_settings().vr_extra_light_select("ENV_Light", "vrayRE_ENV_Light_Select")

    
    #def vr_ext_light_cb(self):
    #    vr_render_settings().import_light_preset("P:\\pucca\\all\\research\\No'\\Template\\Light_Set\\Light_Set_V001-Ext.ma", "Light_Set_Ext")
    
def run():
    global win
    win = vr_rnd_settings_ui()
    win.show()
