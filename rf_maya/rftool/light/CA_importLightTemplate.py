from PySide2 import QtWidgets
import maya.app.renderSetup.model.renderSetup as renderSetup
import json
import os

import maya.cmds as mc

def importPreset():
        projectName = os.environ.get("ACTIVE_PROJECT", None)
        if not projectName:
            return


        root_preset = "P:/{}/rnd/Light/Light_Template/preset".format(projectName)
        
        presetList = [i for i in os.listdir(root_preset) if i.endswith("ma")]

        if not presetList:
            return mc.warning("Preset List is emty. please export preset first")
        
        wid = QtWidgets.QWidget()
        selected_item, state = QtWidgets.QInputDialog.getItem(wid, "Preset", "select preset", presetList, editable=False)

        if selected_item and state:
            fullPath = root_preset + "/" + selected_item
            rs_presetPath = fullPath.replace(".ma", ".json")
            shaderDataPath = root_preset + "/" + selected_item.replace(".ma", "_shaderData.json")

            if not os.path.isfile(fullPath) and os.path.isfile(rs_presetPath) and os.path.isfile(shaderDataPath):
                return mc.warning("Preset not found or missing some data.")

            # if lookdev is exists #
            if not mc.objExists("LookDev_Asset"):
                mc.file(fullPath, i=True, type="mayaAscii", ignoreVersion=1, mergeNamespacesOnClash=False, rpr="", options="v=0;")

            with open(rs_presetPath, "r") as file:
                renderSetup.instance().decode(json.load(file), renderSetup.DECODE_AND_OVERWRITE, None)

            # connect shader #
            with open(shaderDataPath) as f:
                connectionInfo = json.load(f)
            for c in connectionInfo:
                if not c:
                    continue
                    
                for des, src in [c]:
                    mc.connectAttr(src, des, force=True)
            '''
            # create AO shader #
            ao_aov_node = [i for i in mc.ls(type="RedshiftAOV") if "rsAov_AO" in i]
            if ao_aov_node:
                ao_aov_node = ao_aov_node[0]
            else:
                ao_aov_node = mc.rsCreateAov(type="Custom")
                mc.setAttr("{}.name".format(ao_aov_node), "AO", type="string")

            ao_shader = mc.createNode("RedshiftAmbientOcclusion")
            mc.connectAttr("{}.outColor".format(ao_shader), "{}.defaultShader.".format(ao_aov_node))'''

            msg = QtWidgets.QMessageBox()
            msg.setWindowTitle("Done")
            msg.setText("Import Completed")
            msg.setStandardButtons(msg.Ok)
            msg.exec_()