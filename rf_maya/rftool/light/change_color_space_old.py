try:
    from PySide import QtGui, QtCore
    import PySide.QtGui as QtWidgets
except ImportError:
    from PySide2 import QtGui, QtCore, QtWidgets

import pymel.core as pm


def raw_to_srgb():
    all_files_node = pm.ls(type = "file")
    for f in all_files_node:
        f.setAttr("colorSpace", "sRGB")
        
        if f.listConnections(type = "gammaCorrect") == []:
            continue
            
        gamma = f.listConnections(type = "gammaCorrect")[0]
        gamma.setAttr("gammaX", 1)
        gamma.setAttr("gammaY", 1)
        gamma.setAttr("gammaZ", 1)


def srgb_to_raw():
    all_files_node = pm.ls(type = "file")
    for f in all_files_node:
        if f.listConnections(type = "gammaCorrect") == []:
            f.setAttr("colorSpace", "sRGB")
            
   
class change_color_space_win(QtWidgets.QWidget):
    def __init__(self):
        super(change_color_space_win, self).__init__(None, QtCore.Qt.WindowStaysOnTopHint)
        
        self.setFixedSize(220, 150)
        self.setWindowTitle("Change Color Space")
        
        self.__top_layout = QtWidgets.QVBoxLayout(self)

        self.build_ui()
        self.connect_signals()
       
        
    def build_ui(self):
        raw_srgb_label = QtWidgets.QLabel("RAW > sRGB ... (RiFF > ALT)")
        self.__top_layout.addWidget(raw_srgb_label)
        self.raw_srgb_btn = QtWidgets.QPushButton("Do It!")
        self.__top_layout.addWidget(self.raw_srgb_btn)
        srgb_raw_label = QtWidgets.QLabel("sRGB > RAW ... (ALT > RiFF)")
        self.__top_layout.addWidget(srgb_raw_label)
        self.srgb_raw_btn = QtWidgets.QPushButton("Do It!")
        self.__top_layout.addWidget(self.srgb_raw_btn)

        
    def connect_signals(self):
        self.raw_srgb_btn.clicked.connect(self.raw_srgb_cb)
        self.srgb_raw_btn.clicked.connect(self.srgb_raw_cb)

    def raw_srgb_cb(self):
        raw_to_srgb()
        
    def srgb_raw_cb(self):
        srgb_to_raw()

        
def run():
    global win
    win = change_color_space_win()
    win.show()
    
run()
