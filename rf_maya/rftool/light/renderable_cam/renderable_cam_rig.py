''' renderable_cam_rig v0.01
Developed by Kathawut Poldet
RiFF Animation Studio '''

import pymel.core as pm


def lookdev_cam_rig(distance = 10):
    
    geo_ls = pm.ls(sl = True)
    
    if geo_ls == []:
        pm.warning("Please Select object")
        return
        
    geo_ls = geo_ls[0]
    
    x_pos, y_pos, z_pos = geo_ls.boundingBox().center()
    
    focal_length = 70
    
    top_cam = pm.camera(p = [x_pos, y_pos + distance, z_pos], rot = (-90, 180, 0), fl = focal_length, n = "renderable_top_cam")
    bottom_cam = pm.camera(p = [x_pos, y_pos - distance, z_pos], rot = (90, 0, 0), fl = focal_length, n = "renderable_bottom_cam")
    front_cam = pm.camera(p = [x_pos, y_pos, z_pos + distance], fl = focal_length, n = "renderable_front_cam")
    rear_cam = pm.camera(p = [x_pos, y_pos, z_pos - distance], rot = (0, -180, 0), fl = focal_length, n = "renderable_rear_cam")
    right_cam = pm.camera(p = [x_pos + distance, y_pos, z_pos], rot = (0, 90, 0), fl = focal_length, n = "renderable_right_cam")
    left_cam = pm.camera(p = [x_pos - distance, y_pos, z_pos], rot = (0, -90, 0), fl = focal_length, n = "renderable_left_cam")
    
    renderable_cam_ls = [top_cam, bottom_cam, front_cam, rear_cam, left_cam, right_cam]


    renderable_cam_grp = pm.group(renderable_cam_ls, n = "renderable_cam_grp")

def run():
    distance = int(raw_input())
    lookdev_cam_rig(distance)

