from rf_utils.context import context_info
from rftool.light.shotBuilder import utils
reload(utils)

import json
import yaml


def exportAsmSeqData(childShot):
    
    masterRawData = getRawData(utils.getDataInScene(rebuild=False))
    asmData = utils.getDataInScene(rebuild=False)

    scene = context_info.ContextPathInfo()
    project = scene.project
    projectCode = scene.project_code
    ep = scene.episode
    step = scene.step
    process = scene.process
    name = scene.name
    root = "P:"
    
    masterDataPath = "{}/lightingData".format(scene.path.data().abs_path())
    exportPath = "{}/asmSequenceData.json".format(masterDataPath)
    
    if not os.path.isdir(masterDataPath):
        os.makedirs(masterDataPath)
    
    
    childData = {"master": {name: {"setDressing": asmData}}, "child": {}}
    
    if not childShot:
        return
        
    for child in childShot:
        dataPath = "{}/{}/scene/publ/{}/{}/_data".format(root, project, ep, child)
        lgtDataPath = dataPath + "/lightingData"
        
        if not os.path.isdir(lgtDataPath):
            os.makedirs(lgtDataPath)
        
        childWorkPath = "P:/Bikey/scene/work/{}/{}".format(ep, child)
        asmData = utils.getDataInScene(rebuild=False, path=childWorkPath)
        childRawData = getRawData(utils.getDataInScene(rebuild=False, path=childWorkPath))
            
        if child not in childData.keys():
            f = lgtDataPath + "/" + "workspaceData.json"
            childData["child"][child] = {"workspace": "",
                                "setDressing": "",
                                "build": [],
                                "remove": []}
                                
        childData["child"][child]["workspace"] = f
        childData["child"][child]["setDressing"] = asmData
        childData["child"][child]["build"] = getNewBuild(masterRawData, childRawData)
        childData["child"][child]["remove"] = getRemove(masterRawData, childRawData)
       
    
    with open(exportPath, "w") as jsFile:
        json.dump(childData, jsFile, indent=4)


    return exportPath

def getRawData(data):
    rawData = {}
    
    for _, v in data.iteritems():
        path = v["path"]
        with open(path, "r") as jsFile:
            d = json.load(jsFile)
            
        for assetName, v in d.iteritems():
            if assetName not in rawData:
                rawData[assetName] = []
            
            for child in v["child"]:
                if child not in rawData[assetName]:
                    rawData[assetName].append(child)
                    
    return rawData
    


def getNewBuild(masterRawData, childRawData):
    return list(set(childRawData.keys()) - set(masterRawData.keys()))
    
def getRemove(masterRawData, childRawData):
    return list(set(masterRawData.keys()) - set(childRawData.keys()))


##
#childShot = ["bk_ep1_q0080_s0510", "bk_ep1_q0080_s0520", "bk_ep1_q0080_s0530", "bk_ep1_q0080_s0540", "bk_ep1_q0080_s0550"]
#childShot = ["bk_ep1_q0080_s0510"]
#asmSeqPath = exportLightingData(childShot)
