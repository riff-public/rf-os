from PySide2 import QtWidgets, QtCore, QtGui
from shiboken2 import wrapInstance
import pymel.core as pm
import maya.cmds as cmds

import maya.app.renderSetup.model.renderSetup as renderSetup
import maya.app.renderSetup.model.collection as collection
from maya import OpenMayaUI as omui

from collections import OrderedDict

import os
import sys
import json
from ast import literal_eval

core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)

from rf_utils.context import context_info
from rf_utils import register_shot
from rf_app.scene.builder.model import scene_builder_model


from rf_utils.ui.flatUi import main_ui, configVar
reload(main_ui)
reload(configVar)

import utils_v03 as utils
reload(utils)

import shotSwitcher
reload(shotSwitcher)



try:
    utils.globalSettings()
except:
    cmds.warning("Some setting is not working properly")

def getMayaMainWindow():
    win = omui.MQtUtil_mainWindow()
    ptr = wrapInstance(long(win), QtWidgets.QMainWindow)
    return ptr

class ShotBuilder_ui(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(ShotBuilder_ui, self).__init__(parent)

        self.__top_layout = QtWidgets.QGridLayout(self)

        self.shotInfoNode, self.project, self.masterShot, self.currentShot, self.context, self.workspace, self.camera, self.asmSeqPath, self.childShot, self.root, self.ready, self.sceneID = utils.createShotInfoNode()
        self.ready = literal_eval(self.ready)


        with open(self.asmSeqPath, "r") as jsFile:
            self.asmSeqData = json.load(jsFile)

        self.instanceData = self.asmSeqData["sequenceData"][self.currentShot]["setDressing"]
        self.cacheData = self.asmSeqData["sequenceData"][self.currentShot]["cacheData"]["cache"]
        self.camera = self.asmSeqData["sequenceData"][self.currentShot]["cacheData"]["camera"]
        self.cacheData.update(self.camera)

        self.currentPath = self.root + "/" + self.currentShot
        self.sceneContextInfo = context_info.ContextPathInfo(path=self.currentPath)
        self.reg = register_shot.Register(self.sceneContextInfo)

        self.fullUpdate = True if self.context == self.currentShot else False
        #self.btnName = "Build Master Shot" if not self.ready else "Build/Update Env"
        self.btnName = "Build/Update Env"
        self.currentFilter = "All"
        self.gpuPreview = True
        self.proxyMode = 3
        self.childShots_list = utils.listShotFromSeq()

        self.__build_ui()
        self.__connect_signals()


    def __build_ui(self):
        l_wid = QtWidgets.QWidget(self)
        l_wid.setMinimumWidth(550)
        l_wid_lay = QtWidgets.QGridLayout(l_wid)
        l_wid_lay.setContentsMargins(0,0,0,0)

        assetList_grpBox = QtWidgets.QGroupBox("Assets List", l_wid)
        assetList_grpBox_lay = QtWidgets.QGridLayout(assetList_grpBox)

        self.drawSetdressWidget(self.instanceData)
        self.cacheBuildData = self.drawCacheWidget(self.cacheData)
        self.buildEnv_btn = QtWidgets.QPushButton(self.btnName, self)
        self.buildEnv_btn.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.buildEnv_btn.customContextMenuRequested.connect(self.show_menu_buildBtn)
        self.buildCache_btn = QtWidgets.QPushButton("Build/Update Cache", self)

        assetList_grpBox_lay.addWidget(self.setDress_twg, 0, 0, 1, 2)
        assetList_grpBox_lay.addWidget(self.cache_twg, 1, 0, 1, 2)
        assetList_grpBox_lay.addWidget(self.buildEnv_btn, 2, 0, 1, 1)
        assetList_grpBox_lay.addWidget(self.buildCache_btn, 2, 1, 1, 1)


        l_wid_lay.addWidget(assetList_grpBox)

        r_wid = QtWidgets.QWidget(self)
        r_wid_lay = QtWidgets.QGridLayout(r_wid)
        r_wid_lay.setContentsMargins(0,0,0,0)

        option_grpBox = QtWidgets.QGroupBox("Utilities", self)
        option_grpBox_lay = QtWidgets.QGridLayout(option_grpBox)
        
        filter_label = QtWidgets.QLabel("Filter", option_grpBox)
        self.filter_cbb = QtWidgets.QComboBox(option_grpBox)
        filterItems = ["All"] + self.instanceData.keys()
        self.filter_cbb.addItems(filterItems)

        self.gpuPreview_radBtn = QtWidgets.QRadioButton("Gpu Preview", option_grpBox)
        self.prxPreview_radBtn = QtWidgets.QRadioButton("Proxy Preview", option_grpBox)

        self.gpuPreview_radBtn.setEnabled(False)
        self.prxPreview_radBtn.setEnabled(False)

        self.prxWid = QtWidgets.QWidget(option_grpBox)
        self.prxWid_lay = QtWidgets.QVBoxLayout(self.prxWid)
        self.prxWid_lay.setContentsMargins(0,0,0,0)

        self.bbBox_radBtn = QtWidgets.QRadioButton("Bounding Box", option_grpBox)
        self.percentage_radBtn = QtWidgets.QRadioButton("Preview Mesh", option_grpBox)
        self.percentage_input = QtWidgets.QSpinBox(option_grpBox)
        self.percentage_input.setRange(0, 100)
        self.percentage_input.setValue(0)

        self.gpuPreview_radBtn.setChecked(True)

        self.prxWid.setDisabled(True)

        self.prxWid_lay.addWidget(self.bbBox_radBtn)
        self.prxWid_lay.addWidget(self.percentage_radBtn)
        self.prxWid_lay.addWidget(self.percentage_input)

        viewport_label = QtWidgets.QLabel("Viewport", option_grpBox)
        self.setToGpuPreview_btn = QtWidgets.QPushButton("Mesh Preview (GPU)", option_grpBox)
        self.setToPrxPreview_btn = QtWidgets.QPushButton("Box Preview (RS Proxy)", option_grpBox)
        self.setToGpuPreview_btn.setIcon(QtGui.QIcon(configVar.Icon("box").icon))
        self.setToPrxPreview_btn.setIcon(QtGui.QIcon(configVar.Icon("box_2").icon))

        self.setToGpuPreview_btn.setEnabled(False)
        self.setToPrxPreview_btn.setEnabled(False)

        # childShotList #
        child_label = QtWidgets.QLabel("Child Shots", option_grpBox)
        self.drawChildShotWidget(fullUpdate=self.fullUpdate)

        lightUtils_label = QtWidgets.QLabel("Light Utils", option_grpBox)
        lightUtils_wid = QtWidgets.QWidget(self)
        lightUtils_wid_lay = QtWidgets.QGridLayout(lightUtils_wid)
        lightUtils_wid_lay.setContentsMargins(0, 0, 0, 0)
        lightUtils_wid_lay.setSpacing(2)

        '''
        c = 0
        for row in range(4):
            for col in range(4):
                b = QtWidgets.QPushButton("{}".format(c))
                b.setFixedSize(30,30)
                lightUtils_wid_lay.addWidget(b, row, col, 1, 1)

                c += 1'''

        option_grpBox_lay.addWidget(filter_label, 0, 0, 1, 1)
        option_grpBox_lay.addWidget(self.filter_cbb, 0, 1, 1, 1)
        option_grpBox_lay.addWidget(QHLine(), 1, 1, 1, 1)
        option_grpBox_lay.addWidget(self.gpuPreview_radBtn, 2, 0, 1, 1)
        option_grpBox_lay.addWidget(self.prxPreview_radBtn, 2, 1, 1, 1)
        option_grpBox_lay.addWidget(self.prxWid, 3, 1, 1, 1)
        option_grpBox_lay.addWidget(viewport_label, 4, 0, 1, 1)
        option_grpBox_lay.addWidget(QHLine(), 4, 1, 1, 1)
        option_grpBox_lay.addWidget(self.setToGpuPreview_btn, 5, 1, 1, 1)
        option_grpBox_lay.addWidget(self.setToPrxPreview_btn, 6, 1, 1, 1)
        option_grpBox_lay.addWidget(child_label, 7, 0, 1, 1)
        option_grpBox_lay.addWidget(QHLine(), 7, 1, 1, 1)
        option_grpBox_lay.addWidget(self.childShot_lwg, 8, 1, 1, 1)
        option_grpBox_lay.addWidget(lightUtils_label, 9, 0, 1, 1)
        option_grpBox_lay.addWidget(QHLine(), 9, 1, 1, 1)
        option_grpBox_lay.addWidget(lightUtils_wid, 10, 1, 1, 1)


        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)

        r_wid_lay.addWidget(option_grpBox, 0, 1, 1, 1)
        r_wid_lay.addItem(spacerItem, 1, 1, 1, 1)

        self.__top_layout.addWidget(l_wid, 0, 0, 1, 1)
        self.__top_layout.addWidget(r_wid, 0, 1, 1, 1)
        

    def __connect_signals(self):
        self.filter_cbb.currentTextChanged.connect(self.getFilter)

        self.buildEnv_btn.clicked.connect(lambda: self.buildEnv(self.buildData, True, gpuPreview=self.gpuPreview, 
                                        proxyMode=self.proxyMode, prxPercentage=self.percentage_input.value()))

        self.buildCache_btn.clicked.connect(lambda: self.buildCache("build", self.cacheBuildData))

        self.setDress_twg.itemDoubleClicked.connect(lambda: self.selectObj(self.setDress_twg, typ="env"))
        self.cache_twg.itemDoubleClicked.connect(lambda: self.selectObj(self.cache_twg, typ="cache"))
        self.gpuPreview_radBtn.toggled.connect(lambda: self.previewOptions(self.gpuPreview_radBtn))
        self.prxPreview_radBtn.toggled.connect(lambda: self.previewOptions(self.prxPreview_radBtn))
        self.bbBox_radBtn.toggled.connect(lambda: self.proxyOptions(self.bbBox_radBtn))
        self.percentage_radBtn.toggled.connect(lambda: self.proxyOptions(self.percentage_radBtn))

        self.setToGpuPreview_btn.clicked.connect(lambda: utils.switchPreview(True))
        self.setToPrxPreview_btn.clicked.connect(lambda: utils.switchPreview(False))

    def drawSetdressWidget(self, instanceData, update=False):
        headerLabels = ["Asset", "Type", "Ldv", "Proxy", "GPU"]
        if not update:
            self.setDress_twg = QtWidgets.QTreeWidget(self)
            self.setDress_twg.setColumnCount(3)
            self.setDress_twg.setHeaderLabels(headerLabels)
            self.setDress_twg.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
            self.setDress_twg.customContextMenuRequested.connect(self.show_menu_setdress)
            self.setDress_twg.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)

        self.setDress_twg.clear()

        assetInShotCount = utils.getAssetInShot()["assetCount"]

        items = []
        self.buildData = []
        self.displayItems = []
        self.needToUpdateList = []
        for key, values in instanceData.iteritems():
            typ = values["type"]
            path = values["path"]
            asmPath = path.replace(".instance.json", ".asm")

            checkUpdate = utils.checkUpdate(path, asmPath)
            needToUpdate = checkUpdate["update"]
            
            mainItem = QtWidgets.QTreeWidgetItem([key, typ])

            mainItem.setIcon(0, QtGui.QIcon(configVar.Icon("aprv_icon").icon))

            if needToUpdate:
                self.needToUpdateList.append(key)
                mainItem.setIcon(0, QtGui.QIcon(configVar.Icon("needFix_icon").icon))
                #mainItem.setBackground(0 ,QtGui.QBrush(QtGui.QColor(255, 0, 0, 150)))

            # read json data
            with open(path, 'r') as jsFile:
                data = json.load(jsFile)
            
            self.buildData.append({key: {"data": data, "type": typ}})
            
            c = 0
            for k, v in sorted(data.iteritems()):
                ldv = v["isLdv"]
                gpuPath = v["cachePath"]
                rsproxy = v["rsproxy"]
                aiPath = v["aiPath"]
                child = v["child"]

                self.displayItems.append(k)

                nodeCount = assetInShotCount.get(k, {}).get(key, 0)
                isEqual = True if nodeCount == len(v["child"].keys()) else False
                k_name = "{} ({}/{})".format(k, nodeCount, len(v["child"].keys()))
                
                isLdv = "Yes" if ldv else   "No"
                rs = "Yes" if rsproxy else "No"
                gpu = "Yes" if gpuPath else "No"
                #operator = "Yes" if aiPath else "No"
                
                subItem = QtWidgets.QTreeWidgetItem([k_name, "", isLdv, rs, gpu])
                
                if c%2:
                    for i in range(len(headerLabels)):
                        subItem.setBackground(i ,QtGui.QBrush(QtGui.QColor(122, 122, 122, 15)))

                if k in assetInShotCount.keys() and isEqual:
                    subItem.setBackground(0 ,QtGui.QBrush(QtGui.QColor(0, 150, 0, 150)))

                conditions = [isLdv, rs, gpu]
                for column, condition in enumerate(conditions):
                    if condition == "No":
                        subItem.setForeground(column + 2 ,QtGui.QBrush(QtGui.QColor(255, 0, 0, 150)))

                        #subItem.setBackground(0 ,QtGui.QBrush(QtGui.QColor(129, 129, 54, 100)))

                for child in sorted(v["child"]):
                    childItem = QtWidgets.QTreeWidgetItem([child])
                    if cmds.objExists(child):
                        childItem.setForeground(0 ,QtGui.QBrush(QtGui.QColor(0, 100, 50, 200)))
                    
                    subItem.addChild(childItem)
                
                mainItem.addChild(subItem)
                c += 1

            items.append(mainItem)

        self.setDress_twg.insertTopLevelItems(0, items)
        for item in items:
            self.setDress_twg.expandItem(item)

        self.header = self.setDress_twg.header()
        self.header.setSectionResizeMode(QtWidgets.QHeaderView.ResizeToContents)
        self.header.setStretchLastSection(False)
        self.header.setSectionResizeMode(4, QtWidgets.QHeaderView.Stretch)

    def drawCacheWidget(self, cacheData, update=False):
        if not update:
            self.cache_twg = QtWidgets.QTreeWidget(self)
            self.cache_twg.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
            self.cache_twg.customContextMenuRequested.connect(self.show_menu_cache)
            self.cache_twg.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)

        self.cache_twg.clear()

        itemData = OrderedDict()
        itemData["camera"] = {}
        itemData["abc_cache"] = {}
        itemData["yeti_cache"] = {}

        items = []
        headerLabels = ["Asset", "Type", "Version", "Namespace", "Step"]
        self.cache_twg.setHeaderLabels(headerLabels)
        c = 0
        for assetName, values in sorted(cacheData.iteritems()):
            typ = values["type"]
            version = values["version"]
            namespace = values["namespace"]
            step = values.get("step", "")

            if step == "layout" and typ != "camera":
                continue

            item = QtWidgets.QTreeWidgetItem([assetName, typ, version, namespace, step])

            if c%2:
                for i in range(len(headerLabels)):
                    item.setBackground(i ,QtGui.QBrush(QtGui.QColor(122, 122, 122, 15)))

            if typ == "yeti_cache":
                item.setBackground(1 ,QtGui.QBrush(QtGui.QColor(61, 122, 225, 100)))

            # set status #
            status = scene_builder_model.check_status(assetName, self.reg, step)
            icon = status["icon"]
            item.setIcon(0, QtGui.QIcon(icon))

            items.append(item)

            c += 1

            itemData[typ][assetName] = {"step": step}

        self.cache_twg.insertTopLevelItems(0, items)

        return itemData

    def drawChildShotWidget(self, update=False, fullUpdate=True):

        self.childExists_list = utils.getChildShotsList()

        if not update:
            self.childShot_lwg = QtWidgets.QListWidget(self)
            self.childShot_lwg.setEnabled(False)
            self.childShot_lwg.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
            self.childShot_lwg.customContextMenuRequested.connect(self.show_menu_sh)

        self.childShot_lwg.clear()

        masterShot_Color = QtGui.QColor(227, 144, 0, 150)
        childShot_Color = QtGui.QColor(0, 150, 0, 150)

        for child in self.childShots_list:
            item = QtWidgets.QListWidgetItem(child)
            if child in self.childExists_list:
                item.setBackground(childShot_Color)
            if child == self.masterShot:
                item.setBackground(masterShot_Color)
            if child == self.currentShot:
                if child == self.masterShot:
                    item.setBackground(masterShot_Color)
                else:
                    item.setBackground(childShot_Color)

                if fullUpdate:
                    item.setIcon(QtGui.QIcon(configVar.Icon("green_check").icon))
                else:
                    item.setIcon(QtGui.QIcon(configVar.Icon("orange_check").icon))

            self.childShot_lwg.addItem(item)

    def show_menu_setdress(self, position):
        selectedItems = self.setDress_twg.selectedItems()

        index = self.setDress_twg.indexAt(position)

        if not index.isValid():
            return

        item = self.setDress_twg.itemAt(position)

        if not item.parent():
            return

        if not item.child(0):
            return

        itemName = [i.text(0).split(" (")[0] for i in selectedItems]

        menu = QtWidgets.QMenu(self.setDress_twg)

        buildAction = menu.addAction("Build/Update (Selected)")
        self.selectBuildData = utils.selectedBuild(self.buildData, itemName)

        buildAction.triggered.connect(lambda: self.buildEnv(self.selectBuildData, False, gpuPreview=self.gpuPreview, 
                                    proxyMode=self.proxyMode, prxPercentage=self.percentage_input.value()))

        menu.addSeparator()
        action = menu.addAction("Reload Material (Selected)")
        action.triggered.connect(lambda: self.reloadMaterial(itemName))
        action2 = menu.addAction("Reload Material (All)")
        action2.triggered.connect(lambda: self.reloadMaterial([]))
        menu.addSeparator()
        action3 = menu.addAction("Update All Assets")
        action3.triggered.connect(self.updateTableData)

        x,y = position.toTuple()
        offset = QtCore.QPoint(x+2, y+23)
        menu.exec_(self.setDress_twg.mapToGlobal(offset))

    def show_menu_cache(self, position):
        selectedItems = self.cache_twg.selectedItems()

        index = self.cache_twg.indexAt(position)

        if not index.isValid():
            return

        item = self.cache_twg.itemAt(position)

        self.cacheItemData = self.getItemDataFromTreeWidget(selectedItems)

        menu = QtWidgets.QMenu(self.cache_twg)

        buildAction = menu.addAction("Build/Update")
        buildAction.triggered.connect(lambda: self.buildCache("build", self.cacheItemData))
        removeAction = menu.addAction("Remove")
        removeAction.triggered.connect(lambda: self.buildCache("remove", self.cacheItemData))

        x,y = position.toTuple()
        offset = QtCore.QPoint(x+2, y+23)
        menu.exec_(self.cache_twg.mapToGlobal(offset))

    def show_menu_sh(self, position):
        selectedItem = self.childShot_lwg.selectedItems()

        index = self.childShot_lwg.indexAt(position)

        if not index.isValid():
            return

        selectedChildShot = self.childShot_lwg.itemAt(position).text()

        menu = QtWidgets.QMenu(self.childShot_lwg)
        sub1 = menu.addMenu("Switch")
        action1 = sub1.addAction("Preview")
        action2 = sub1.addAction("Make Current")
        menu.addSeparator()
        action3 = menu.addAction("Remove")

        action1.triggered.connect(lambda : self.switchShot((selectedChildShot, False), False))
        action2.triggered.connect(lambda : self.switchShot((selectedChildShot, False), True))
        action3.triggered.connect(lambda : self.removeChild(selectedChildShot))

        menu.exec_(self.childShot_lwg.mapToGlobal(position))

    def show_menu_buildBtn(self, position):

        menu = QtWidgets.QMenu(self.buildEnv_btn)
        action1 = menu.addAction("Force Update")
        action1.triggered.connect(self.forceBuild)

        menu.exec_(self.buildEnv_btn.mapToGlobal(position))

    def forceBuild(self):
        msg = QtWidgets.QMessageBox(self)
        msg.setWindowTitle("Force update ?")
        msg.setText("Update asm data and re build this shot ?")
        msg.setIcon(msg.Warning)
        msg.setStandardButtons(msg.Yes | msg.No)
        ret = msg.exec_()
        if ret == msg.No:
            return

        # update instance data #
        self.instanceData = utils.updateAsmData()
        self.drawSetdressWidget(self.instanceData, update=True)

        # clean assets #
        utils.forceUpdateEnv()
        self.buildEnv(self.buildData, False, updateMaterial=True, gpuPreview=self.gpuPreview, 
                        proxyMode=self.proxyMode, prxPercentage=self.percentage_input.value())

        # override anim pos #
        utils.overridePosition(self.currentShot)


    def removeChild(self, selectedChildShot):
        msg = QtWidgets.QMessageBox(self)

        if selectedChildShot == self.masterShot:
            msg.setWindowTitle("ERROR!")
            msg.setText("You can't remove a master shot!!")
            msg.setIcon(msg.Critical)
            msg.setStandardButtons(msg.Ok)
            msg.exec_()
            return

        if selectedChildShot not in self.childShot:
            return

        msg.setWindowTitle("Sure?")
        msg.setText("Remove selected shot ? (This process is not undoable)")
        msg.setIcon(msg.Warning)
        msg.setStandardButtons(msg.Yes | msg.No)
        ret = msg.exec_()
        if ret == msg.No:
            return

        print "Removing...", selectedChildShot
        utils.cleanupShot(cleanEnv=True, cleanCache=True, updateInfo=True, remove=selectedChildShot)
        msg.setText("Shot {} has been removed.".format(selectedChildShot))

        if selectedChildShot == self.currentShot:
            print "Switch to masterShot..."
            self.instanceData, self.cacheData, self.fullUpdate, self.currentShot, self.currentPath, self.sceneContextInfo, self.reg, self.cacheBuildData, self.childShot = shotSwitcher.switchShot((self.masterShot, False), switch_cache=False, exportWorkspace=False)
            
            msg.setText("Shot {} has been removed. The current has been changed to {}".format(selectedChildShot, self.masterShot))
            
            # update widget 
            self.drawSetdressWidget(self.instanceData, update=True)
            self.drawCacheWidget(self.cacheData, update=True)

        self.drawChildShotWidget(update=True, fullUpdate=self.fullUpdate)

        msg.setWindowTitle("Log")
        msg.setIcon(msg.Information)
        msg.setStandardButtons(msg.Ok)
        msg.exec_()


    def getItemDataFromTreeWidget(self, items):
        itemData = OrderedDict()
        itemData["camera"] = {}
        itemData["abc_cache"] = {}
        itemData["yeti_cache"] = {}

        for item in items:
            itemName = item.text(0)
            fileType = item.text(1)
            step = item.text(4)

            itemData[fileType][itemName] = {"step": step}

        return itemData

    def getFilter(self, item):
        self.currentFilter = item

        if self.currentFilter != "All":
            self.filterData = {self.currentFilter: self.instanceData[self.currentFilter]}
            self.drawSetdressWidget(self.filterData, update=True)
        else:
            self.drawSetdressWidget(self.instanceData, update=True)

    def previewOptions(self, btn):
        if btn.isChecked():
            if btn.text() == "Gpu Preview":
                self.gpuPreview = True
                self.prxWid.setDisabled(True)
            else:
                self.gpuPreview = False
                self.prxWid.setDisabled(False)
                self.bbBox_radBtn.setChecked(True)
                self.proxyMode = 0

    def proxyOptions(self, btn):
        if btn.isChecked():
            if btn.text() == "Bounding Box":
                self.proxyMode = 0
            else:
                self.proxyMode = 1

    def buildEnv(self, buildData, clickFromBtn, *args, **kwargs):
        if self.needToUpdateList:
            self.forceBuild()

            return

        for x in buildData:
            dressName = x.keys()[0]
            data = x[dressName]["data"]
            typ = x[dressName]["type"]

            if not data:
                continue

            assets = utils.buildEnv(dressName, typ, data, *args, **kwargs)

            # add created assets to set #
            utils.createObjSets(self.currentShot, items=assets)

            # unhide #
            cmds.showHidden(assets)

        if not self.ready and self.currentShot == self.masterShot and clickFromBtn:
            # override position 
            utils.overridePosition(self.currentShot)

            utils.updateShotInfoData([("ready", "True")])
            self.ready = True
            #self.btnName = "Build/Update Env"
            #self.buildEnv_btn.setText(self.btnName)


        self.drawSetdressWidget(self.instanceData, update=True)

        msg = QtWidgets.QMessageBox(self)
        msg.setWindowTitle("Done")
        msg.setText("Build / Update Done")
        msg.setStandardButtons(msg.Ok)
        ret = msg.exec_()

    def buildCache(self, *args, **kwargs):
        utils.buildCache(*args, **kwargs)
        # update cache widget #
        self.drawCacheWidget(self.cacheData, update=True)


    def switchShot(self, childShot, switchCache):
        if not self.ready:
            msg = QtWidgets.QMessageBox(self)
            msg.setWindowTitle("Error")
            msg.setText("Unable to switch. Please Build Master Shot First.")
            msg.setIcon(msg.Warning)
            msg.setStandardButtons(msg.Ok)
            msg.exec_()
            return

        # check if not on a master shot #
        if context_info.ContextPathInfo().name != self.masterShot:
            msg = QtWidgets.QMessageBox(self)
            msg.setWindowTitle("Error")
            msg.setText("Unable to switch. Try to open a master shot. ({})".format(self.masterShot))
            msg.setIcon(msg.Warning)
            msg.setStandardButtons(msg.Ok)
            msg.exec_()
            return

        self.instanceData, self.cacheData, self.fullUpdate, self.currentShot, self.currentPath, self.sceneContextInfo, self.reg, self.cacheBuildData, self.childShot = shotSwitcher.switchShot(childShot, switchCache)
        # update widget 
        self.drawSetdressWidget(self.instanceData, update=True)
        self.drawCacheWidget(self.cacheData, update=True)
        self.drawChildShotWidget(update=True, fullUpdate=self.fullUpdate)

    def selectObj(self, wid, typ):
        item = wid.currentItem()

        if typ == "env":
            obj = item.text(0)
        else:
            obj = item.text(0)
            typ = item.text(1)
            ns = item.text(3)

            if typ == "abc_cache":
                obj += ":Geo_Grp"
            elif typ == "yeti_cache":
                obj = ns + ":" + obj.split(ns)[-1].split("_")[-1] + "_YetiNode"
            else:
                obj = "{}:camshot".format(obj)

        if cmds.objExists(obj):
            cmds.select(obj)

    def reloadMaterial(self, items):
        if len(items) >= 1 and len(items) <= 5:
            mtrNodes = ["{}_mtrRN".format(i) for i in items]
        else:
            mCount = len(items)
            if mCount == 0:
                mCount = "all"
            msg = QtWidgets.QMessageBox(self)
            msg.setWindowTitle("Sure?")
            msg.setText("Are you sure to reload {} Materials?".format(mCount))
            msg.setIcon(msg.Warning)
            msg.setStandardButtons(msg.Yes | msg.No)
            ret = msg.exec_()
            if ret == msg.No:
                return

            mtrNodes = [i for i in cmds.ls(type="reference") if "_mtrRN" in i and i.split("_mtrRN")[0] in self.displayItems]


        for mtr in mtrNodes:
            if not cmds.objExists(mtr):
                continue
            
            cmds.file(loadReference=mtr)

    def updateTableData(self):
        msg = QtWidgets.QMessageBox(self)
        msg.setWindowTitle("Sure?")
        msg.setText("Do you want to update data? (Might take a bit of time to process)")
        msg.setIcon(msg.Warning)
        msg.setStandardButtons(msg.Yes | msg.No)
        ret = msg.exec_()
        if ret == msg.No:
            return

        self.instanceData = utils.updateAsmData()

        self.drawSetdressWidget(self.instanceData, update=True)

        msg = QtWidgets.QMessageBox(self)
        msg.setWindowTitle("Done!")
        msg.setText("Data is up to date")
        msg.setStandardButtons(msg.Ok)
        msg.exec_()


class QHLine(QtWidgets.QFrame):
    """docstring for QHLine"""
    def __init__(self):
        super(QHLine, self).__init__()
        self.setFrameShape(QtWidgets.QFrame.HLine)
        self.setFrameShadow(QtWidgets.QFrame.Sunken)
        

def runUI(ui_name="ShotBuilder_ui", size=(950, 750), title="Light | Sequence Builder | Beta"):
    if pm.window(ui_name, exists=True):
        pm.deleteUI(ui_name)
    win = main_ui.MainWindow(ui_name=ui_name, size=size, title=title, widget=ShotBuilder_ui(), parent=getMayaMainWindow())
    win.show()
    