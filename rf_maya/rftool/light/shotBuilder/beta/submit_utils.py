import maya.cmds as cmds
from collections import OrderedDict

import json
import os
import sys
import socket
import subprocess

core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)

from rf_utils import user_info
from rf_maya.environment import envparser

import utils
reload(utils)


def createSubmitInfo(layer_json_path, currentShot, startFrame, endFrame):
    with open(layer_json_path, "r") as jsFile:
        data = json.load(jsFile)
    
    root, camera = utils.getShotInfoData("root", "camera")
    
    renderVersion = "v001"
    currentRenderer = "redshift"
    prefix = "<RenderLayer>/<Version>/<RenderLayer>"
    output_path = "{}/{}/render/output".format(root, currentShot)
    pool = "rs_bikey"
    _camera = "{}:camshot".format(camera)
    
    layerData = OrderedDict()
    
    for rs in data["renderSetup"]["renderLayers"]:
        values = rs["renderSetupLayer"]
        renderable = values["renderable"]
        collections = values["collections"]
        layerName = values["name"]
        
        layerData[layerName] = {"frames": "", "camera": _camera, "output_dir": "", "output_path": output_path, "output_prefix": prefix,
                                "pool": pool, "renderer": currentRenderer, "renderVersion": renderVersion, "renderable": renderable}
        
        renderSettingsCollection = [cols for cols in collections if cols.get("renderSettingsCollection")]

        if renderSettingsCollection and renderSettingsCollection[0]["renderSettingsCollection"]["selfEnabled"]:
            childs = renderSettingsCollection[0]["renderSettingsCollection"]["children"]
            val = getAttrValues(childs)
        else:
            val = {}

        _startFrame = val.get("startFrame") if val.get("startFrame") else startFrame
        _endFrame = val.get("endFrame") if val.get("endFrame") else endFrame
        _currentRenderer = val.get("currentRenderer") if val.get("currentRenderer") else currentRenderer
        _prefix = val.get("imageFilePrefix") if val.get("imageFilePrefix") else prefix
        _renderVersion = val.get("renderVersion") if val.get("renderVersion") else renderVersion
        
        
        prefix_abs = prefixTranslator(layerName, _prefix, _renderVersion)
        outputDir = "{}/{}".format(output_path, prefix_abs)
        frameRange = "{}-{}".format(_startFrame, _endFrame)
        
        layerData[layerName]["frames"] = frameRange
        layerData[layerName]["renderer"] = _currentRenderer
        layerData[layerName]["renderVersion"] = _renderVersion
        layerData[layerName]["output_dir"] = outputDir

    user = user_info.User()
    userEntity = user.sg_user()
    email = userEntity["sg_ad_account"] + "@riff-studio.com"

    d = {"sgRange": [startFrame, endFrame], "renderLayers": layerData, "email": email}

    return d

def exportSubmitInfo(data, curr_workspacePath, currentShot):
    submitInfoPath = "{}/{}_submitInfo.json".format(curr_workspacePath, currentShot)
    with open(submitInfoPath, "w") as file:
        json.dump(data, file, indent=2, sort_keys=True)

    return submitInfoPath

def getAttrValues(d):
    attrs = ["startFrame", "endFrame", "renderVersion", "currentRenderer", "imageFilePrefix"]
    vals = {}
    for child in d:
        attr = child["absUniqueOverride"]["attribute"]

        if attr not in attrs:
            continue
        if not child["absUniqueOverride"]["selfEnabled"]:
            continue

        attrValue = child["absUniqueOverride"]["attrValue"]["value"]
        attrType = child["absUniqueOverride"]["attrValue"]["type"]
        if attrType == "Time":
            attrValue = int(attrValue / 250)
        
        vals[attr] = attrValue
        
    return vals
    
def prefixTranslator(layer, prefix, version, camera=""):
    return "/".join(prefix.replace("<RenderLayer>", layer).replace("<Version>", version).replace("<Camera>", camera).split("/")[0:-1])

def createDeadlineJobInfo(ms, cs, rt, si, job_info_path, job_props_path, renderserver="deadlineserver"):
    project = os.environ.get("ACTIVE_PROJECT")
    submit_cmd = "D:/TD/local/rf_maya/rftool/light/shotBuilder/submit_cmd.py"
    args = "{} -ms {} -cs {} -rt {} -si {} -env {}".format(submit_cmd, ms, cs, rt, si, job_info_path)
    dl_version = "v10"

    job_info = OrderedDict()
    job_info["Plugin"] = "CommandLine"
    job_info["Pool"] = "cmd"
    job_info["Comment"] = "submit command"
    job_info["Name"] = "{}_shotSwitcher".format(cs)
    job_info["Frames"] = 1001
    job_info["MachineName"] = socket.gethostname()
    job_info["OverrideTaskExtraInfoNames"] = False
    job_info["ExtraInfoKeyValue0"] = "project={project}".format(project=project)

    job_info = setup_render_environments(job_info, project, "Light", "2020")

    job_props = OrderedDict()

    job_props["Arguments"] = args
    job_props["Executable"] = "C:/Program Files/Autodesk/Maya2020/bin/mayapy.exe"
    job_props["Shell"] = "default"
    job_props["ShellExecute"] = False
    job_props["SingleFramesOnly"] = True
    job_props["StartupDirectory"] = ""

    config_path = "O:/Pipeline/core/rf_env/default/deadline/{}/{}.ini".format(dl_version, renderserver)
    custom_env = os.environ.copy()
    custom_env["PYTHONPATH"] = "C:/Python27/Lib;C:/Python27/Lib/DLLs"
    # set config path so deadline launches using the prefered config
    custom_env['DEADLINE_CONFIG_FILE'] = config_path

    # ----------- write job to file # ----------- #
    with open(job_info_path, "w") as f:
        for k, v in job_info.iteritems():
            t = "{}={}\n".format(k, v)
            f.write(t)

    with open(job_props_path, "w") as f:
        for k, v in job_props.iteritems():
            t = "{}={}\n".format(k, v)
            f.write(t)

    return job_info_path, job_props_path, custom_env

def send_to_deadline_cmd(job_info_path, job_props_path, custom_env):

    sended_job = False
    jobID = ""

    deadline_cmd_path = "C:/Program Files/Thinkbox/Deadline10/bin/deadlinecommand.exe"

    cmd = '{deadline_cmd_path} "{job_info_path}" "{job_props_path}"'.format(deadline_cmd_path=deadline_cmd_path,
                                                                            job_info_path=job_info_path,
                                                                            job_props_path=job_props_path)

    process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, env=custom_env)
    lines = iter(process.stdout.readline, b"")
    commandResult = ""
    for line in lines:
        commandResult += line

    if "Result=Success" in commandResult:
        sended_job = True
        jobID = commandResult.split("JobID=")[-1].split("\r")[0]

    print commandResult
    return jobID, sended_job

def setup_render_environments(job_info, project, dept, version):
    script_root = 'O:/Pipeline'

    drive_envs = OrderedDict([('RFPROJECT', 'P:'), 
                            ('RFPUBL', 'P:'), 
                            ('RFPROD', 'P:'), 
                            ('RFVERSION', 'R:'),
                            ('RFSCRIPT', script_root)])

    department = dept
    version = version
    localEnv = envparser.setupEnv(project=project, department=department, version=version, 
                                mergeWithDefault=True, setEnvironment=False, writeToFile=False)

    envs = {}

    currentScript = os.environ.get('RFSCRIPT')

    for k, v in localEnv.iteritems():
        newV = [i.replace(currentScript, script_root) for i in v]
        envs[k] = newV

    envs = OrderedDict([(i[0], ';'.join(i[1])) for i in envs.items()])
    envs.update(drive_envs)
    for i, key in enumerate(envs):
        value = envs[key]
        job_info['EnvironmentKeyValue{}'.format(i)] = '{}={}'.format(key, value)

    return job_info