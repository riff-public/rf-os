import yaml
import os
import json


# set dress #

# set dress #
def createRsProxy(prxName, prxPath, ns, displayMode, displayPercent):
    cmds.select(cl=True)
    prx, shape, tr = mm.eval("redshiftCreateProxy;")
    tr = cmds.rename(tr, prxName)
    
    cmds.setAttr("{}.fileName".format(prx), prxPath, type="string")
    cmds.setAttr("{}.displayMode".format(prx), displayMode)
    cmds.setAttr("{}.displayPercent".format(prx), displayPercent)
    cmds.setAttr("{}.materialMode".format(prx), 2)
    cmds.setAttr("{}.nameMatchPrefix".format(prx), "{}:".format(ns), type="string")
    
    cmds.setAttr("{}.objectIdMode".format(prx), 1)
    cmds.setAttr("{}.tessellationMode".format(prx), 1)
    cmds.setAttr("{}.userDataMode".format(prx), 1)
    cmds.setAttr("{}.visibilityMode".format(prx), 1)
    
    return prx, shape, tr
# set dress #  
def createInstanceData(asset, reg, rebuild):
    try:
        dataPath = reg.get.assetData[asset]["setShot"]["heroFile"]
    except:
        dataPath = reg.get.assetData[asset]["shotDress"]["heroFile"]

    name = dataPath.split("/")[-1].replace(".asm", ".instance.json")
    outPath = "/".join(dataPath.split("/")[0:-1]) + "/{}".format(name)

    if not os.path.isfile(outPath):
        rebuild = True
        
    if rebuild:
        with open(dataPath, 'r') as stream:
            shotDressData = yaml.safe_load(stream)
        
        assetData = {}
        checked = []
        
        for k, v in shotDressData.iteritems():
            shortName = v["shortName"]
            parents = v["parent"]
            assetDescription = v["assetDescription"]
            position = v["position"]
            hidden = v["hidden"]
            
            if hidden:
                continue
            
            if not os.path.isfile(assetDescription):
                continue
                
            assetName = assetDescription.split("/")[5]

            if assetDescription not in checked:
                with open(assetDescription, 'r') as y:
                    data = yaml.safe_load(y)

                if "model" not in data.keys():
                    continue

                heroPath = data["heroPath"]
            
                if "lookdev" in data.keys():
                    try:
                        prxPath = data["lookdev"]["md"][0]["rsproxy"]["heroFile"]
                        mtrRender = data["lookdev"]["md"][0]["mtrRender"]["heroFile"]
                        aiPath = prxPath.replace("rsproxy", "aiStandin").replace(".rs", ".ass")
                        if not os.path.isfile(aiPath):
                            aiPath = None
                        isLdv = True
                    except:
                        prxPath = ""
                        mtrRender = ""
                        aiPath = ""
                        isLdv = False
                else:
                    # hard coded to get a ldv data #
                    ldvPath = heroPath.replace("/hero", "/lookdev/main/hero/output/")

                    if os.path.isdir(ldvPath):
                        prxPath = [i for i in os.listdir(ldvPath) if i.endswith(".rs")]
                        mtrRender = [i for i in os.listdir(ldvPath) if "_mtr_" in i]
                        aiPath = [i for i in os.listdir(ldvPath) if i.endswith(".ass")]

                        prxPath = ldvPath + prxPath[0] if prxPath else ""
                        mtrRender = ldvPath + mtrRender[0] if mtrRender else ""
                        aiPath = ldvPath + aiPath[0] if aiPath else ""
                        isLdv = True

                    else:
                        prxPath = ""
                        mtrRender = ""
                        aiPath = ""
                        isLdv = False
                
                try:
                    cachePath = data["model"]["md"][0]["cache"]["heroFile"]
                except:
                    cachePath = ""

                checked.append(assetDescription)
            

            if assetName not in assetData.keys():
                assetData[assetName] = {"isLdv": isLdv,
                                        "assetDescription": assetDescription,
                                        "rsproxy": prxPath,
                                        "cachePath": cachePath,
                                        "mtrRender": mtrRender,
                                        "aiPath": aiPath,
                                        "child": {}}

            assetData[assetName]["child"].update({k: {"shortName": shortName, "position": position, "parents": parents}})
    
        with open(outPath, "w") as jsFile:
            json.dump(assetData, jsFile, indent=2)


    return asset, outPath
# set dress #
def createParentGrp(parents):
    if cmds.objExists(parents):
        return

    grps = parents.split("|")
    for i, g in enumerate(grps):
        grpToCheck = "|".join(grps[0:i+1])
        if not cmds.objExists(grpToCheck):
            cmds.group(n=g, empty=True)
            
            if i != 0:
                grpRoot = "|{}".format(g)
                grpToParent = "|".join(grps[0:i])
                cmds.parent(grpRoot, grpToParent)
# set dress #
def build(data, updateMaterial=True, gpuPreview=True, proxyMode=3, prxPercentage=0):
    # check if master instance grp is exists #
    masterGrp = "masterInstance_Grp"
    if not cmds.objExists(masterGrp):
        cmds.group(n=masterGrp, empty=True)

    created = []
    for assetName, v in data.iteritems():
        gpuPath = v["cachePath"]
        mtrRender = v["mtrRender"]
        rsproxy = v["rsproxy"]
        isLdv = v["isLdv"]
        aiPath = v["aiPath"]

        # create master gpu
        if not gpuPath:
            continue

        gpuName = "{}_gpuCache".format(assetName)
        if not cmds.objExists(assetName):
            cacheNode = cmds.createNode("gpuCache", name=gpuName)
            cacheTr = cmds.listRelatives(cacheNode, p=True, pa=True)
            cacheTr = cmds.rename(cacheTr, assetName)
            try:
                if cacheTr not in cmds.listRelatives(masterGrp):
                    cmds.parent(cacheTr, masterGrp)
            except:
                cmds.parent(cacheTr, masterGrp)

        cmds.setAttr("{}.cacheFileName".format(assetName), gpuPath, type="string")
        cmds.setAttr("{}.lodVisibility".format(gpuName), gpuPreview)


        # connect ai graph #
        if aiPath:
            aiIncludeGraphNode = cmds.listConnections(gpuName, type="aiIncludeGraph")
            if not aiIncludeGraphNode:
                aiIncludeGraphNode = cmds.ls(cmds.shadingNode("aiIncludeGraph", asUtility=True))

            cmds.connectAttr("{}.out".format(aiIncludeGraphNode[0]), "{}.operators[0]".format(gpuName), force=True)
            cmds.setAttr("{}.filename".format(aiIncludeGraphNode[0]), aiPath, type="string")
            cmds.setAttr("{}.target".format(aiIncludeGraphNode[0]), "aiMerge1", type="string")

        # create master proxy
        masterProxy = assetName + "_rsProxy"
        mtrNs = assetName + "_mtr"

        if isLdv:
            if not cmds.namespace(exists=mtrNs):
                cmds.file(mtrRender, r=True, gl=True, mergeNamespacesOnClash=False, namespace=mtrNs, options="v=0;p=17;f=0")

            else:
                if updateMaterial:
                    cmds.file(loadReference=mtrNs + "RN")

            if not cmds.objExists(masterProxy):
                prxNode, prxShape, prxTr = createRsProxy(masterProxy, rsproxy, mtrNs, 
                                            displayMode=proxyMode, displayPercent=prxPercentage)
            else:
                prxTr = pm.PyNode(masterProxy)
                prxShape = prxTr.getShape().name()
                prxNode = cmds.listConnections(prxShape, type="RedshiftProxyMesh")
                cmds.setAttr("{}.fileName".format(prxNode[0]), rsproxy, type="string")
                cmds.setAttr("{}.displayMode".format(prxNode[0]), proxyMode)
                cmds.setAttr("{}.displayPercent".format(prxNode[0]), prxPercentage)
                prxTr = prxTr.name()

            try:
                cmds.parent(prxTr, masterGrp)
            except:
                pass

        # add attr to master gpu #
        addAndSetAttr(assetName, ("gpuMaster", gpuName), ("rsProxyMaster", masterProxy), ("rsProxyInstance", ""), ("material", mtrNs))

        # add attr to master rx proxy #
        addAndSetAttr(masterProxy, ("gpuMaster", gpuName), ("rsProxyMaster", masterProxy), ("rsProxyInstance", ""), ("material", mtrNs))

        # create instance
        for dagName, values in v["child"].iteritems():
            shortName = values["shortName"]
            parents = values["parents"]
            pos = values["position"]
            parents = values["parents"]

            master = assetName
            instName = dagName

            if parents:
                createParentGrp(parents)

            # instance GPU and set position #
            gpuInst = createInstance(master, instName, parents, pos, shortName)
            created.append(instName)

            
            # instance PROXY and parent to gpu above ##
            if isLdv:
                master = masterProxy
                instName = "{}|{}_inst".format(dagName, shortName)
                pos = False
                parents = dagName
                shortName = shortName + "_inst"
                prxInst = createInstance(master, instName, parents, pos, shortName)


            # add and set attribute to instance nodes #
            # gpu #
            addAndSetAttr(gpuInst, ("gpuMaster", gpuName), ("rsProxyMaster", masterProxy), ("rsProxyInstance", prxInst), ("material", mtrNs))
            # proxy #
            addAndSetAttr(instName, ("gpuMaster", gpuName), ("rsProxyMaster", masterProxy), ("rsProxyInstance", prxInst), ("material", mtrNs))

    cmds.setAttr("masterInstance_Grp.visibility", 0)
    cmds.setAttr("masterInstance_Grp.hiddenInOutliner", True)

    return created
# set dress #
def createInstance(master, instName, parents, pos, shortName):
    master = "masterInstance_Grp|{}".format(master)
    if not cmds.objExists(instName):
        cmds.select(master)
        instancedNode = cmds.instance(n=shortName)
        longName = cmds.ls(instancedNode, l=True)
        if parents:
            cmds.parent(longName[0], parents, r=True)
        else:
            cmds.parent(longName[0], w=True)

    if pos:
        cmds.xform(instName, m=pos)

    return instName
# set dress #
def selectedBuild(data, selectedItems, selectedChilds=[]):
    selectedBuildData = []
    for d in data:
        c = {}
        for name in d:
            if name not in selectedItems:
                continue

            c[name] = d[name]

            if selectedChilds:
                childs = d[name]["child"]
                existingChild = [x for x in childs if x in selectedChilds]
                if not existingChild:
                    c.pop(name, None)
                    continue
                removeChilds = list(set(childs) - set(selectedChilds))
                for child in removeChilds:
                    childs.pop(child, None)
                    
                c[name]["child"] = childs

        selectedBuildData.append(c)

    return selectedBui