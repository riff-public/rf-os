import yaml
import os
import re
import json
import time

from rf_utils import register_shot
from rf_utils.context import context_info
from rf_utils.sg import sg_process
from rf_app.scene.builder.model import scene_builder_model
from rftool.utils import pipeline_utils
from rf_maya.rftool.fix import moveSceneToOrigin as msto
from rf_utils.pipeline import asset_tag

import maya.cmds as cmds
import maya.mel as mm
import pymel.core as pm

from collections import OrderedDict


def getDataInScene(rebuild=False, typ="setdress", path=""):
    scene = context_info.ContextPathInfo(path=path)
    reg = register_shot.Register(scene)
    assets = reg.get.asset_list()
    
    data = {}
    cacheData = {"camera": {},
                "cache": {}}

    if typ == "setdress":
        use = ["shotdress", "set"]
    else:
        use = ["abc_cache", "yeti_cache", "camera"]
    
    for asset in assets:
        assetType = reg.get.assetData[asset]["type"]
        if assetType in use:
            if typ == "setdress":
                asset, outPath = createInstanceData(asset, reg, rebuild)
                data[asset] = {"type": assetType, "path": outPath}
            else:
                ver = reg.get.assetData[asset]["version"]
                namespace = reg.get.assetData[asset]["namespace"]
                path = reg.get.assetData[asset]["cache"]["heroFile"]
                step = reg.get.assetData[asset]["step"]
                if assetType == "camera":
                    k = "camera"
                else:
                    k = "cache"
                cacheData[k][asset] = {"type": assetType, "path": path, "version": ver, "namespace": namespace, "step": step}
    
    if typ == "setdress":      
        return data
    else:
        return cacheData

def createInstanceData(asset, reg, rebuild):

    try:
        dataPath = reg.get.assetData[asset]["setShot"]["heroFile"]
    except:
        dataPath = reg.get.assetData[asset]["shotDress"]["heroFile"]

    name = dataPath.split("/")[-1].replace(".asm", ".instance.json")
    outPath = "/".join(dataPath.split("/")[0:-1]) + "/{}".format(name)

    if not os.path.isfile(outPath):
        rebuild = True
        
    if rebuild:
        with open(dataPath, 'r') as stream:
            shotDressData = yaml.safe_load(stream)
        
        assetData = {}
        checked = []
        
        for k, v in shotDressData.iteritems():
            shortName = v["shortName"]
            #assetName = re.sub(r"[_0-9]", "", shortName)
            parents = v["parent"]
            assetDescription = v["assetDescription"]
            position = v["position"]
            hidden = v["hidden"]
            
            if hidden:
                continue
            
            if not os.path.isfile(assetDescription):
                continue
                
            assetName = assetDescription.split("/")[5]

            if assetDescription not in checked:
                with open(assetDescription, 'r') as y:
                    data = yaml.safe_load(y)

                if "model" not in data.keys():
                    continue

                heroPath = data["heroPath"]
                
                '''
                if "lookdev" in data.keys():
                    try:
                        prxPath = data["lookdev"]["md"][0]["rsproxy"]["heroFile"]
                        mtrRender = data["lookdev"]["md"][0]["mtrRender"]["heroFile"]
                        aiPath = prxPath.replace("rsproxy", "aiStandin").replace(".rs", ".ass")
                        if not os.path.isfile(aiPath):
                            aiPath = None
                        isLdv = True
                    except:
                        prxPath = ""
                        mtrRender = ""
                        aiPath = ""
                        isLdv = False'''
                
                # hard coded to get a ldv data #
                ldvPath = heroPath.replace("/hero", "/lookdev/main/hero/output/")

                if os.path.isdir(ldvPath):
                    prxPath = [i for i in os.listdir(ldvPath) if i.endswith(".rs")]
                    mtrRender = [i for i in os.listdir(ldvPath) if "_mtr_" in i]
                    aiPath = [i for i in os.listdir(ldvPath) if i.endswith(".ass")]

                    prxPath = ldvPath + prxPath[0] if prxPath else ""
                    mtrRender = ldvPath + mtrRender[0] if mtrRender else ""
                    aiPath = ldvPath + aiPath[0] if aiPath else ""
                    isLdv = True

                else:
                    prxPath = ""
                    mtrRender = ""
                    aiPath = ""
                    isLdv = False
                
                try:
                    cachePath = data["model"]["md"][0]["cache"]["heroFile"]
                except:
                    cachePath = ""

                checked.append(assetDescription)
            

            if assetName not in assetData.keys():
                assetData[assetName] = {"isLdv": isLdv,
                                        "assetDescription": assetDescription,
                                        "rsproxy": prxPath,
                                        "cachePath": cachePath,
                                        "mtrRender": mtrRender,
                                        "aiPath": aiPath,
                                        "child": {}}

            assetData[assetName]["child"].update({k: {"shortName": shortName, "position": position, "parents": parents}})
    
        with open(outPath, "w") as jsFile:
            json.dump(assetData, jsFile, indent=2)


    return asset, outPath

def createRsProxy(prxName, prxPath, ns, displayMode, displayPercent):
    cmds.select(cl=True)
    prx, shape, tr = mm.eval("redshiftCreateProxy;")
    tr = cmds.rename(tr, prxName)
    
    cmds.setAttr("{}.fileName".format(prx), prxPath, type="string")
    cmds.setAttr("{}.displayMode".format(prx), displayMode)
    cmds.setAttr("{}.displayPercent".format(prx), displayPercent)
    cmds.setAttr("{}.materialMode".format(prx), 2)
    cmds.setAttr("{}.nameMatchPrefix".format(prx), "{}:".format(ns), type="string")
    
    cmds.setAttr("{}.objectIdMode".format(prx), 1)
    cmds.setAttr("{}.tessellationMode".format(prx), 1)
    cmds.setAttr("{}.userDataMode".format(prx), 1)
    cmds.setAttr("{}.visibilityMode".format(prx), 1)
    
    return prx, shape, tr

def createParentGrp(parents):
    if cmds.objExists(parents):
        return

    grps = parents.split("|")
    if grps[0] == "":
        grps = grps[1:]
    grps[0] = "|" + grps[0]

    for i, g in enumerate(grps):
        grpToCheck = "|".join(grps[0:i+1])
        
        if not cmds.objExists(grpToCheck):
            cmds.group(n=g, empty=True)
            if i != 0:
                grpRoot = "|{}".format(g)
                grpToParent = "|".join(grps[0:i])
                cmds.parent(grpRoot, grpToParent)
                
def createInstance(master, instName, parents, pos, shortName):
    master = "masterInstance_Grp|{}".format(master)
    if not cmds.objExists(instName):
        cmds.select(master)
        instancedNode = cmds.instance(n=shortName)
        #longName = cmds.ls(instancedNode, l=True)
        longName = "masterInstance_Grp|{}".format(instancedNode[0])
        #cmds.parent(longName, world=True)

        if parents:
            cmds.parent(longName, parents, r=True)
        else:
            cmds.parent(longName, w=True)

    if pos:
        cmds.xform(instName, m=pos)

    return instName

def setInitialPos(instName, pos):
    cmds.xform(instName, m=pos)

def selectedBuild(data, selectedItems, selectedChilds=[]):
    selectedBuildData = []
    for x in data:
        dressName = x.keys()[0]
        d = x[dressName]["data"]
        typ = x[dressName]["type"]

        c = {}

        for name in d:
            if name not in selectedItems:
                continue

            c[name] = d[name]

            if selectedChilds:
                childs = d[name]["child"]
                existingChild = [x for x in childs if x in selectedChilds]
                if not existingChild:
                    c.pop(name, None)
                    continue
                removeChilds = list(set(childs) - set(selectedChilds))
                for child in removeChilds:
                    childs.pop(child, None)
                    
                c[name]["child"] = childs

        selectedBuildData.append({dressName: {"data": c, "type": typ}})

    return selectedBuildData

def addAndSetAttr(obj, *args):
    if not cmds.objExists(obj):
        return
    for attrName, value in args:

        if not cmds.attributeQuery(attrName, node=obj, ex=True):
            cmds.addAttr(obj, ln=attrName, dt="string")
            
        cmds.setAttr("{}.{}".format(obj, attrName), l=False)
        cmds.setAttr("{}.{}".format(obj, attrName), value, type="string")
        cmds.setAttr("{}.{}".format(obj, attrName), l=True)

def globalSettings():
    # turn on gpu cache, mtoa, redhift
    cmds.loadPlugin("gpuCache", qt=True)
    cmds.loadPlugin("mtoa", qt=True)
    cmds.loadPlugin("redshift4maya", qt=True)   

    cmds.setAttr("redshiftOptions.copyToTextureCache", 0)

def switchPreview(gpuPreview=True):
    items = getSelected()
    
    if gpuPreview:
        lodVis = True
        displayMode = 3
    else:
        lodVis = False
        displayMode = 0
        
    for gpu, rs in items:
        if gpu:
            cmds.setAttr("{}.lodVisibility".format(gpu), lodVis)
        if rs:
            cmds.setAttr("{}.displayMode".format(rs), displayMode)

def getSelected():
    objs = []
        
    selected = pm.ls(sl=True)
        
    if selected:
        objLs = selected
    else:
        objLs = cmds.ls("|masterInstance_Grp", dag=True, type="transform")
                
        if "masterInstance_Grp" in objLs:
            objLs.remove("masterInstance_Grp")
    
    for obj in objLs:
        gpuMaster = cmds.getAttr("{}.gpuMaster".format(obj))
        
        rsProxyMaster = cmds.getAttr("{}.rsProxyMaster".format(obj))
        if cmds.objExists(rsProxyMaster):
            rsProxyMaster = pm.PyNode(rsProxyMaster)

            sh = rsProxyMaster.getShape()
            prxNode = sh.listConnections(type="RedshiftProxyMesh")[0].name()
            
            o = (gpuMaster, prxNode)

        else:
            o = (gpuMaster, "")

        if o not in objs:
            objs.append(o)
            
    return objs

def listShotFromSeq():
    scene = context_info.ContextPathInfo()

    s = []
    shots = sg_process.get_shot_from_sequence(scene.project, scene.episode, scene.sequence)

    for sh in shots:
        if sh["sg_status_list"] == "omt":
            continue
        if "_all" in sh["code"]:
            continue
            
        s.append(sh["code"])
    
    return s

def getChildShotsList():
    childShots = getShotInfoData("childShot")[0]
    return childShots.split(",")

def sgGetFrameRange(project, name):
    shotEntity = sg_process.get_shot_entity(project, name, extraFields=["sg_cut_in", "sg_cut_out"])
    startFrame = 1000
    sg_cut_in = shotEntity["sg_cut_in"]
    sg_cut_out = shotEntity["sg_cut_out"]

    sg_cut_in += startFrame
    sg_cut_out += startFrame

    return sg_cut_in, sg_cut_out

def setFrameRange(sg_cut_in, sg_cut_out):
    cmds.playbackOptions(ast=sg_cut_in, aet=sg_cut_out, maxTime=sg_cut_out, minTime=sg_cut_in)
    cmds.setAttr("defaultRenderGlobals.startFrame", sg_cut_in)
    cmds.setAttr("defaultRenderGlobals.endFrame", sg_cut_out)

    return sg_cut_in, sg_cut_out

def createShotInfoNode():
    loc = "seqBuilder_shotInfo"

    createObjSets("setsItems")

    if cmds.objExists(loc):
        vals = getShotInfoData("project", "masterShot", "currentShot", "context", "workspace",
                                "camera", "asmSeqPath", "childShot", "root", "ready", "sceneID")

        vals.insert(0, loc)

        return vals

    cmds.spaceLocator(n=loc)
    cmds.lockNode(loc, l=False)
    sceneID = cmds.ls(loc, uuid=True)[0].split("-")[0]

    scene = context_info.ContextPathInfo()
    name = scene.name
    project = scene.project
    masterShot, currentShot, context, childShot = name, name, name, name
    
    # create asmSeqData #
    exportPath, rootPath = exportAsmSeqData(sceneID=sceneID)

    addAndSetAttr(loc, ("project", project), ("masterShot", masterShot), ("currentShot", currentShot), ("context", context), ("workspace", ""), 
                            ("camera", ""), ("asmSeqPath", exportPath), ("childShot", childShot), ("root", rootPath), ("ready", "False"), ("sceneID", sceneID))

    # get camera #
    _, seqData, _ = readAsm()
    camera = seqData[masterShot]["cacheData"]["camera"].keys()[0]
    workspace = seqData[masterShot]["workspace"]
    updateShotInfoData([("camera", camera), ("workspace", workspace)])

    cmds.setAttr("{}.lodVisibility".format(loc), False)
    cmds.setAttr("{}.hiddenInOutliner".format(loc), True)
    cmds.lockNode(loc, l=True)

    cmds.select(cl=True)

    return loc, project, masterShot, currentShot, context, workspace, camera, exportPath, childShot, rootPath, "False", sceneID

def getShotInfoData(*args):
    val = [cmds.getAttr("seqBuilder_shotInfo.{}".format(arg)) for arg in args]
    return val

def updateShotInfoData(*args):
    cmds.lockNode("seqBuilder_shotInfo", lock=False)
    for attrs in args:
        for attr, val in attrs:
            cmds.setAttr("seqBuilder_shotInfo.{}".format(attr), lock=False)
            cmds.setAttr("seqBuilder_shotInfo.{}".format(attr), val, type="string")
            cmds.setAttr("seqBuilder_shotInfo.{}".format(attr), lock=True)
        cmds.lockNode("seqBuilder_shotInfo", lock=True)

def readAsm():
    asmSeqPath = getShotInfoData("asmSeqPath")[0]

    with open(asmSeqPath, "r") as jsFile:
        asmSeqData = json.load(jsFile)

    masterShot = asmSeqData["masterShot"]
    sequenceData = asmSeqData["sequenceData"]

    return masterShot, sequenceData, asmSeqPath

def createBuildList(v):
    buildData = []
    for key, values in v.iteritems():
        typ = values["type"]
        path = values["path"]

        with open(path, 'r') as jsFile:
            data = json.load(jsFile)
        
        buildData.append({key: {"data": data, "type": typ}})

    return buildData

def exportAsmSeqData(rebuildMaster=False, childShot=[], sceneID="test"):
    
    # asm master #
    asmData = getDataInScene(rebuild=rebuildMaster)
    masterRawData = getRawData(asmData)

    # cache data #
    cacheData = getDataInScene(typ="cache")

    scene = context_info.ContextPathInfo()
    project = scene.project
    projectCode = scene.project_code
    ep = scene.episode
    step = scene.step
    process = scene.process
    name = scene.name
    rootPath = "{}/{}".format(context_info.ContextPathInfo().path.publish().abs_path(), ep)

    masterDataPath = "{}/lightingData/{}".format(scene.path.data().abs_path(), sceneID)
    exportPath = "{}/asmSequenceData.json".format(masterDataPath)
    workspaceRoot = "{}/workspaceData".format(masterDataPath)
    workspacePath = "{}/{}".format(workspaceRoot, name)
    
    # create folder if not #
    if not os.path.isdir(masterDataPath):
        os.makedirs(masterDataPath)


    _asmData = {"sequenceData": {name: {"setDressing": asmData, "workspace": workspacePath, 
                "type": "master", "build": masterRawData, "hide": [], "cacheData": cacheData}},
                "masterShot": name}
    
    if childShot:
        childExists = {}
        for child, rebuild in childShot:
            #dataPath = "{}/{}/_data".format(rootPath, child)
            #lgtDataPath = dataPath + "/lightingData/{}".format(sceneID)
            
            #if not os.path.isdir(lgtDataPath):
            #    os.makedirs(lgtDataPath)
            
            childWorkPath = "P:/{}/scene/work/{}/{}".format(project, ep, child)
            asmData = getDataInScene(rebuild=rebuild, path=childWorkPath)

            if not asmData:
                continue

            childRawData = getRawData(asmData)
            
            cacheData = getDataInScene(rebuild=False, typ="cache", path=childWorkPath)

            # check with master #
            buildChild  = filterChild(masterRawData, childRawData)
            # check with shot before #
            _buildChild = filterChild(childExists, buildChild)

            hide = getHideObj(masterRawData, childRawData)

            # create default dict for each child shot #
            if child not in _asmData.keys():
                _asmData["sequenceData"][child] = {"workspace": "",
                                    "setDressing": "",
                                    "build": [],
                                    "type": "child",
                                    "cacheData": {}}

            workspacePath = "{}/{}".format(workspaceRoot, child)

            _asmData["sequenceData"][child]["workspace"] = workspacePath
            _asmData["sequenceData"][child]["setDressing"] = asmData
            _asmData["sequenceData"][child]["build"] = _buildChild
            _asmData["sequenceData"][child]["hide"] = hide
            _asmData["sequenceData"][child]["cacheData"] = cacheData
            
            # add child to childExists for check in next shot #
            childExists = merge(childExists, buildChild)

            print "\t Create a asmData shot {}".format(child)

    with open(exportPath, "w") as jsFile:
        json.dump(_asmData, jsFile, indent=4)

    return exportPath, rootPath

def updateAsmData():
    root, currentShot, allChilds, masterShot, sceneID = getShotInfoData("root", "currentShot", "childShot", "masterShot", "sceneID")
    
    allChilds = allChilds.split(",")
    allChilds.remove(masterShot)
    
    if currentShot == masterShot:
        rebuildMaster = True
        childs = [(c, False) for c in allChilds]
    else:
        rebuildMaster = False
        childs = [(c, True if c == currentShot else False) for c in allChilds]
        
    
    exportPath, _ = exportAsmSeqData(rebuildMaster=rebuildMaster, childShot=childs, sceneID=sceneID)
    
    with open(exportPath, "r") as jsFile:
        asmSeqData = json.load(jsFile)
    
    updateAsm = asmSeqData["sequenceData"][currentShot]["setDressing"]
        
    return updateAsm

def getRawData(asmData={}, useFromSeqPath=False, listOnly=False):
    if useFromSeqPath:
        asmSeqPath, currentShot = getShotInfoData("asmSeqPath", "currentShot")
        with open(asmSeqPath, "r") as jsFile:
            asmData = json.load(jsFile)["sequenceData"][currentShot]["setDressing"]

    rawData = {}
    
    for _, v in asmData.iteritems():
        path = v["path"]
        with open(path, "r") as jsFile:
            d = json.load(jsFile)
            
        for assetName, v in d.iteritems():
            if assetName not in rawData:
                rawData[assetName] = []
            
            for child in v["child"]:
                if child not in rawData[assetName]:
                    rawData[assetName].append(child)
    
    if listOnly:
        l = []
        for x in rawData.values():
            l += x
        return l

    return rawData

def filterChild(masterRawData, childRawData):
    d = {}
    for k, v in childRawData.iteritems():
        if k in masterRawData.keys():
            m_child = masterRawData[k]
            # check duplicates child
            c = list(set(v) - set(m_child))
        else:
            c = v
        
        if not c:
            continue
            
        d[k] = c

    return d

def merge(masterRawData, buildChild):
    for k, v in buildChild.iteritems():
        if k in masterRawData.keys():
            masterRawData[k] = list(set(masterRawData[k] + v))
        else:
            masterRawData[k] = v
        
    return masterRawData

def getHideObj(masterRawData, childRawData):
    h = []
    listMaster = convertDictToList(masterRawData)
    listChild = convertDictToList(childRawData)

    h = list(set(listMaster) - set(listChild))

    return h

def convertDictToList(d):
    y = [e for i in d.values() for e in i]
    y = list(dict.fromkeys(y))

    return y


def createObjSets(setsName, items=[], parent=None):
    cmds.select(cl=True)
    if not cmds.objExists(setsName):
        setsName = cmds.sets(n=setsName)

    if items:
        cmds.sets(items, add=setsName)
    
    if parent:
        cmds.sets(setsName, edit=True, fe=parent)
        
    cmds.select(cl=True)
    cmds.lockNode(setsName, lock=True)

    return setsName

def setPosition(setPos, asmData):
    assetInShot = []
    for dressName, val in asmData.iteritems():
        path = val["path"]
        ovrdPath = [os.path.join(os.path.dirname(path), i) for i in os.listdir(os.path.dirname(path)) if "ovrd.hero.asm" in i]

        if setPos:
            with open(path, "r") as jsFile:
                instanceData = json.load(jsFile)
                    
            for assetName in instanceData:
                for child in instanceData[assetName]["child"]:

                    if not cmds.objExists(child):
                        cmds.warning("{} is not exist".format(child))
                        
                    pos = instanceData[assetName]["child"][child]["position"]
                    setInitialPos(child, pos) 
                    assetInShot.append(child)

        if ovrdPath:
            #print "\t --: Override {} position".format(dressName)
            overridePosition(ovrdPath[0])

    return assetInShot

def overridePosition(ovrdPath):
    with open(ovrdPath, 'r') as jsFile:
        data = json.load(jsFile)
    
    for k, v in data.iteritems():
        name = k.split("|", 1)[-1]
        
        if not cmds.objExists(name):
            continue
        
        pos = v["childPos"]
        
        if pos != [1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0]:
            cmds.xform(name, m=pos)

def filterCacheData(data, *args):
    _d = {}
    for d, v in data.iteritems():
        if v["step"] not in args and v["type"] != "camera":
            continue
        _d[d] = v

    return _d

def getRemoveCacheData(d1, d2, reg):
    removeData = OrderedDict()
    removeData["camera"] = {}
    removeData["abc_cache"] = {}
    removeData["yeti_cache"] = {}

    for itemName, v in d1.iteritems():
        if itemName in d2.keys():
            continue
        
        fileType = v["type"]
        step = v["step"]

        status = scene_builder_model.check_status(itemName, reg, step)
        if status["status"] == "nomatch":
            continue
        
        removeData[fileType][itemName] = {"step": step}
       
    return removeData

def createCacheData(data):
    itemData = OrderedDict()
    itemData["camera"] = {}
    itemData["abc_cache"] = {}
    itemData["yeti_cache"] = {}
    
    for assetName, values in sorted(data.iteritems()):
        typ = values["type"]
        step = values["step"]
        
        if step == "layout" and typ != "camera":
            continue
            
        itemData[typ][assetName] = {"step": step}

    return itemData

def getLightTemplateFolder():
    scene = context_info.ContextPathInfo()
    ep = scene.episode
    root = "/".join(cmds.file(q=True, loc=True).split("/")[0:2])
    lightRigFolder = "{}/rnd/light/Light_Template/Light_Rig/{}".format(root, ep.upper())
    
    return lightRigFolder, [i for i in os.listdir(lightRigFolder) if i.endswith(".ma")]

def checkUpdate(f1, f2):
    f1Time = os.path.getmtime(f1)
    f1Abs = time.ctime(f1Time)
    f2Time = os.path.getmtime(f2)
    f2Abs = time.ctime(f2Time)
    
    update = True if f1Time < f2Time else False
    
    return {"update": update, "f1": f1Abs, "f2": f2Abs}

# ------------------------------ #
# Cleanup Stuff #
# ------------------------------ #
def getAssetsInCurrentShot(count=False, shOverride=""):

    if not shOverride:
        currentShot = getShotInfoData("currentShot")[0]
    else:
        currentShot = shOverride

    if not cmds.objExists(currentShot):
        return {}

    _, sequenceData, _ = readAsm()

    listDress = sequenceData[currentShot]["setDressing"].keys()

    asset = {"master": [], "inst": [], "mtr": []}
    assetCount = {}

    for i in pm.PyNode(currentShot).members():
        gpuMaster = i.getAttr("gpuMaster")
        tr = pm.PyNode(gpuMaster).getTransform().longName()
        prxMaster = i.getAttr("rsProxyMaster")
        mtr = i.getAttr("material")
        mtr = mtr + "RN"
        ln = i.longName()
        sn = gpuMaster.split("_gpuCache")[0]
        try:
            dressName = i.getAttr("dressName")
        except:
            dressName = "Not found"

        if sn not in assetCount.keys():
            assetCount[sn] = dict.fromkeys(listDress, 0)

        assetCount[sn][dressName] += 1

        if gpuMaster not in asset["master"]:
            asset["master"].append(gpuMaster)
            asset["master"].append(tr)
            ai = cmds.listConnections("{}.operators.".format(gpuMaster))
            if ai:
                asset["master"] += ai
            
        if prxMaster not in asset["master"]:
            if cmds.objExists(prxMaster):
                asset["master"].append(prxMaster)
                prxNode = pm.PyNode(prxMaster)
                prxMesh = prxNode.getShape().listConnections(type="RedshiftProxyMesh")
                if prxMesh:
                    asset["master"].append(prxMesh[0].name())
        
        if ln not in asset["inst"]:
            if ln[0] == "|":
                ln = ln.replace("|", "", 1)
            asset["inst"].append(ln)

            
        if mtr not in asset["mtr"]:
            if cmds.objExists(mtr):
                asset["mtr"].append(mtr)
    if count:
        return assetCount

    return asset

def getRemoveAssetsData(remove=""):
    currentShot, childsShot = getShotInfoData("currentShot", "childShot")
    childsShot = childsShot.split(",")

    assetToKeep = {"master": [], "inst": [], "mtr": []}
    assetToRemove = {"master": [], "inst": [], "mtr": []}

    if remove:
        keepList = list(childsShot)
        keepList.remove(remove)
        cleanShotList = [remove]
        
        for k in keepList:
            _assetToKeep = getAssetsInCurrentShot(shOverride=k)
            assetToKeep["master"] += _assetToKeep["master"]
            assetToKeep["inst"] += _assetToKeep["inst"]
            assetToKeep["mtr"] += _assetToKeep["mtr"]

    else:
        keepList = [currentShot]
        cleanShotList = list(childsShot)
        cleanShotList.remove(currentShot)
        assetToKeep = getAssetsInCurrentShot()

    for c in cleanShotList:
        assetInShot = getAssetsInCurrentShot(shOverride=c)
        master = assetInShot["master"]
        inst = assetInShot["inst"]
        mtr = assetInShot["mtr"]
        
        objDiff = list(set(master).difference(assetToKeep["master"]))
        instDiff = list(set(inst).difference(assetToKeep["inst"]))
        mtrDiff = list(set(mtr).difference(assetToKeep["mtr"]))
        
        assetToRemove["master"] += objDiff
        assetToRemove["inst"] += instDiff
        assetToRemove["mtr"] += mtrDiff
        
    return assetToRemove, cleanShotList, keepList

def cleanupShot(cleanEnv, cleanCache, updateInfo, *args, **kwargs):
    assetToRemove, cleanShotList, keepList = getRemoveAssetsData(*args, **kwargs)

    if cleanEnv:
        cleanupEnv(assetToRemove, cleanShotList)

    if cleanCache:
        cleanupCache(cleanShotList)

    # update shot info data #
    if updateInfo:
        childs = ",".join(keepList)
        sceneID, masterShot = getShotInfoData("sceneID", "masterShot")
        _childShot = [(c, False) for c in keepList if c and c != masterShot]
        exportPath, rootPath = exportAsmSeqData(childShot=_childShot, sceneID=sceneID)

        updateShotInfoData([("childShot", childs), ("context", masterShot), ("currentShot", masterShot)])

        return childs


def cleanupCache(cleanShotList):
    context, root, currentShot, asmSeqPath = getShotInfoData("context", "root", "currentShot", "asmSeqPath")

    with open(asmSeqPath, "r") as jsFile:
        asmSeqData = json.load(jsFile)

    for sh in cleanShotList:
        path = "{}/{}".format(root, sh)
        scene = context_info.ContextPathInfo(path=path)
        reg = register_shot.Register(scene)

        childCacheData = asmSeqData["sequenceData"][sh]["cacheData"]["cache"]
        camData = asmSeqData["sequenceData"][sh]["cacheData"]["camera"]
        childCacheData.update(camData)

        _childCacheData = filterCacheData(childCacheData, "anim", "sim")

        # remove cache #
        removeCacheData = getRemoveCacheData(_childCacheData, {}, reg)
        buildCache("remove", removeCacheData, regOverride=reg)

def cleanupEnv(assetToRemove, cleanShotList):
    inst = assetToRemove["inst"]
    master = assetToRemove["master"]
    mtr = assetToRemove["mtr"]

    # clean geos #
    try:
        cmds.delete(inst)
        cmds.delete(master)
    except:
        pass
    
    # clean object sets #
    for s in cleanShotList:
        try:
            cmds.lockNode(s, lock=False)
            cmds.delete(s)
        except:
            pass

    # clean material #
    for m in mtr:
        try:
            m = pm.PyNode(m)
            m.referenceFile().remove()
        except:
            pass

def cleanupEnvWhenBuild(delete=False):
    
    rawData = getRawData(useFromSeqPath=True, listOnly=True)
    currentShot = getShotInfoData("currentShot")[0]

    if not cmds.objExists(currentShot):
        return

    setsMembers = getAssetsInCurrentShot()["inst"]
    assets = list(set(setsMembers).difference(rawData))
    
    if assets:
        currentShot, childsShot = getShotInfoData("currentShot", "childShot")
        childsShot = childsShot.split(",")
        childsShot.remove(currentShot)
        
        assetsSeq = []
        for c in childsShot:
            assetInShot = getAssetsInCurrentShot(shOverride=c)["inst"]
            assetsSeq += assetInShot
            
        
        if set(assetsSeq).intersection(assets):
            # remove node from set #
            print "Remove assets from current shot"
            print assets
            cmds.sets(assets, remove=currentShot)
        else:
            # delete node # (EXCEPT MASTER)
            if delete:
                print "Delete assets from sequence"
                print assets
                cmds.delete(assets)
            
    return setsMembers

# ------------------------------ #
#  Build Cache  #
# ------------------------------ #
def buildCache(typ, itemData, regOverride=""):
    cmds.select(cl=True)
    root, currentShot = getShotInfoData("root", "currentShot")
    currentPath = root + "/" + currentShot

    scene = context_info.ContextPathInfo(path=currentPath)
    reg = register_shot.Register(scene)

    geoGrp = scene.projectInfo.asset.geo_grp()
    cacheOrigin = scene.projectInfo.scene.cache_at_origin

    for itemType, values in itemData.iteritems():
        for asset_item, v in values.iteritems():
            
            output_types = reg.get.output_types(asset_item)
            step = v["step"]
            
            if typ == "build":
                if not output_types:
                    continue
            
                scene_builder_model.asset_build(asset_item, reg, scene, step=step, buildType=output_types[0])

                geo_grp = "{}:{}".format(asset_item, geoGrp)
                offsetAttr = cmds.objExists("{}.{}".format(geo_grp, asset_tag.Attr.cacheOffset))

                # move to actual position #
                if cacheOrigin and offsetAttr: 
                    offsetValue, allTransforms = msto.get_cameraDistanceFromOrigin(scene)

                    pNode = pm.PyNode(geo_grp)
                    pNode.attr('t').unlock()
                    for ax in 'xyz':
                        pNode.attr('t'+ax).unlock()
                    pm.xform(geo_grp, a=True, ws=True, t=offsetValue)
                    pNode.attr('t').lock()
                    for ax in 'xyz':
                        pNode.attr('t'+ax).lock()

            else:
                if regOverride:
                    reg = regOverride
                scene_builder_model.remove(asset_item, reg, scene, step=step)
                print "\tRemove --: {}".format(asset_item)

    pipeline_utils.scene_group()


# ------------------------------ #
#  Build Env  #
# ------------------------------ #
def buildEnv(dressName, typ, data, updateMaterial=False, gpuPreview=True, proxyMode=3, prxPercentage=0):
    # check if master instance grp is exists #
    masterGrp = "masterInstance_Grp"
    if not cmds.objExists(masterGrp):
        cmds.group(n=masterGrp, empty=True)

    if typ == "shotdress":
        shotGrp = "ShotDress_Grp"
    else:
        shotGrp = "Set_Grp"

    if not cmds.objExists(shotGrp):
        cmds.group(n=shotGrp, empty=True)

    created = []
    for assetName, v in data.iteritems():
        gpuPath = v["cachePath"]
        mtrRender = v["mtrRender"]
        rsproxy = v["rsproxy"]
        isLdv = v["isLdv"]
        aiPath = v["aiPath"]

        # create master gpu
        if not gpuPath:
            continue

        gpuName = "{}_gpuCache".format(assetName)
        if not cmds.objExists(assetName):
            cacheNode = cmds.createNode("gpuCache", name=gpuName)
            cacheTr = cmds.listRelatives(cacheNode, p=True, pa=True)
            cacheTr = cmds.rename(cacheTr, assetName)
            try:
                if cacheTr not in cmds.listRelatives(masterGrp):
                    cmds.parent(cacheTr, masterGrp)
            except:
                cmds.parent(cacheTr, masterGrp)

        cmds.setAttr("{}.cacheFileName".format(assetName), gpuPath, type="string")
        cmds.setAttr("{}.lodVisibility".format(gpuName), gpuPreview)


        # connect ai graph #
        if aiPath:
            aiIncludeGraphNode = cmds.listConnections(gpuName, type="aiIncludeGraph")
            if not aiIncludeGraphNode:
                aiIncludeGraphNode = cmds.ls(cmds.shadingNode("aiIncludeGraph", asUtility=True))

                cmds.connectAttr("{}.out".format(aiIncludeGraphNode[0]), "{}.operators[0]".format(gpuName), force=True)
                cmds.setAttr("{}.filename".format(aiIncludeGraphNode[0]), aiPath, type="string")
                cmds.setAttr("{}.target".format(aiIncludeGraphNode[0]), "aiMerge1", type="string")

        # create master proxy
        masterProxy = assetName + "_rsProxy"
        mtrNs = assetName + "_mtr"

        if isLdv:
            if not cmds.namespace(exists=mtrNs):
                cmds.file(mtrRender, r=True, gl=True, mergeNamespacesOnClash=False, namespace=mtrNs, options="v=0;p=17;f=0")

            else:
                if updateMaterial:
                    cmds.file(loadReference=mtrNs + "RN")

            if not cmds.objExists(masterProxy):
                prxNode, prxShape, prxTr = createRsProxy(masterProxy, rsproxy, mtrNs, 
                                            displayMode=proxyMode, displayPercent=prxPercentage)
            else:
                prxTr = pm.PyNode(masterProxy)
                prxShape = prxTr.getShape().name()
                prxNode = cmds.listConnections(prxShape, type="RedshiftProxyMesh")
                cmds.setAttr("{}.fileName".format(prxNode[0]), rsproxy, type="string")
                cmds.setAttr("{}.displayMode".format(prxNode[0]), proxyMode)
                cmds.setAttr("{}.displayPercent".format(prxNode[0]), prxPercentage)
                prxTr = prxTr.name()

            try:
                cmds.parent(prxTr, masterGrp)
            except:
                pass

        # add attr to master gpu #
        addAndSetAttr(assetName, ("gpuMaster", gpuName), ("rsProxyMaster", masterProxy), ("rsProxyInstance", ""), ("material", mtrNs), ("dressName", dressName))

        # add attr to master rx proxy #
        addAndSetAttr(masterProxy, ("gpuMaster", gpuName), ("rsProxyMaster", masterProxy), ("rsProxyInstance", ""), ("material", mtrNs), ("dressName", dressName))

        # create instance
        for dagName, values in v["child"].iteritems():
            shortName = values["shortName"]
            parents = values["parents"]
            pos = values["position"]

            master = assetName
            instName = dagName

            if parents:
                parents = "|{}|{}".format(shotGrp, parents)
                createParentGrp(parents)

            # instance GPU and set position #
            gpuInst = createInstance(master, instName, parents, pos, shortName)
            created.append(instName)
            
            # instance PROXY and parent to gpu above ##
            if isLdv:
                master = masterProxy
                instName = "{}|{}_inst".format(dagName, shortName)
                pos = False
                parents = dagName
                shortName = shortName + "_inst"
                prxInst = createInstance(master, instName, parents, pos, shortName)


            # add and set attribute to instance nodes #
            # gpu #
            addAndSetAttr(gpuInst, ("gpuMaster", gpuName), ("rsProxyMaster", masterProxy), ("rsProxyInstance", prxInst), ("material", mtrNs), ("dressName", dressName))
            # proxy #
            addAndSetAttr(instName, ("gpuMaster", gpuName), ("rsProxyMaster", masterProxy), ("rsProxyInstance", prxInst), ("material", mtrNs), ("dressName", dressName))

    cmds.setAttr("masterInstance_Grp.visibility", 0)
    cmds.setAttr("masterInstance_Grp.hiddenInOutliner", True)


    # override anim pos #
    currentShot = getShotInfoData("currentShot")[0]
    _, sequenceData, _ = readAsm()
    asmData = sequenceData[currentShot]["setDressing"]
    positionData = {dressName: asmData[dressName]}
    setPosition(setPos=False, asmData=positionData)

    return created