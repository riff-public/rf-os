import sys
import os 
import argparse
from datetime import datetime 


import pymel.core as pm 
import maya.cmds as cmds

import json

core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)

from rf_utils.context import context_info
from rf_utils import file_utils
reload(context_info)
reload(file_utils)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

sys.path.append("D:/TD")
from local.rf_maya.rftool.light.shotBuilder import shotSwitcher
from local.rf_maya.rftool.light.shotBuilder import utils_v02 as utils
from local.rf_app.deadline_manager.submit_job.slr import submitJobToDeadline

def setup_parser(title):
    # create the parser
    parser = argparse.ArgumentParser(description=title, formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('-ms', dest='masterShot', type=str, help='Master shot to be submitted', required=True)
    parser.add_argument('-cs', dest='childShot', type=str, help='Child shot to be created', required=True)
    parser.add_argument('-rt', dest='root', type=str, help='Root path', required=True)
    parser.add_argument('-si', dest='submitInfo', type=str, help='Submit Info path', required=True)
    parser.add_argument('-env', dest='environment', type=str, help='Override environment', required=True)

    return parser


# childShot = bk_ep1_q0080_s0510

def main(masterShot, childShot, root, submitInfo, environment):
    print '======== Start the process =========='

    try: 
        result = cmds.file(masterShot, o=True, f=True, prompt=False)
        logger.debug('Open scene completed')
    except Exception as e: 
        logger.error('Scene has opened with error')
        logger.error(e)

    contextPath = "{}/{}".format(root, childShot)
    entity = setupEntity(contextPath)
    saveFileName = saveFile(entity, saveAs=True)



    # ============================================== #
    # do function (switch, cleanup, submit) #
    # swtich #
    print '============='
    print "Switching..."
    print '============='
    shotSwitcher.switchShot((childShot, False), True)

    # cleanup #
    # no need to clean up cache. because it's already cleaned up when do a shot switcher #
    print '============='
    print "Cleaning..."
    print '============='
    utils.cleanupShot(cleanEnv=True, cleanCache=False, updateInfo=False)

    # save #
    saveFile(entity)

    # submit job to deadline #
    print '============='
    print "Submitting..."
    print '============='
    submitJobs(submitInfo, environment)
    # ============================================== #

    
    print '======== Finished all the process =========='
    

def setupEntity(childShot):
    entity = context_info.ContextPathInfo(path=childShot)
    entity.context.update(process="main", step="light", app="maya")

    return entity

def saveFile(entity, saveAs=False):
    if saveAs:
        workspace_path = entity.path.workspace().abs_path()
        version = file_utils.calculate_version(file_utils.list_file(workspace_path))
        
        entity.context.update(version=version)
        
        workfile_name = entity.work_name(user=True)
        saveFileName = '%s/%s' % (workspace_path, workfile_name)
        
        cmds.file(rename=saveFileName)
        cmds.file(save=True, f=True, typ='mayaAscii')
        
        return saveFileName
    else:
        saveFileName = cmds.file(save=True, type="mayaAscii")
        return saveFileName

def submitJobs(submitInfo, environment):
    with open(submitInfo, "r") as jsFile:
        data = json.load(jsFile)

    app = "maya"
    priority = 50
    timeout = 7200
    dependency = ""
    renderserver = "deadlineserver"
    pool = "rs_bikey"

    renderLayersData = data["renderLayers"]
    email = data["email"]

    for layer, value in renderLayersData.iteritems():
        frames = value["frames"]
        camera = value["camera"]
        output_dir = value["output_dir"]
        output_path = value["output_path"]
        output_prefix = value["output_prefix"]
        pool = value["pool"]
        renderer = value["renderer"]

        submitJobToDeadline.send_to_deadline_cmd(app=app, 
                                                frames=frames, camera=camera, 
                                                output_dir=output_dir, output_path=output_path,
                                                render_layer=layer, output_prefix=output_prefix, 
                                                priority=priority, timeout=timeout, dependency=dependency,
                                                renderserver=renderserver, pool=pool,
                                                renderer=renderer, email=email,
                                                overrideRenderEnv=environment)



if __name__ == "__main__":
    parser = setup_parser('Submit params')
    params = parser.parse_args()
    main(masterShot=params.masterShot, childShot=params.childShot, root=params.root, submitInfo=params.submitInfo, environment=params.environment)