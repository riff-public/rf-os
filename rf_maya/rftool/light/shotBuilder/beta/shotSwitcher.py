import json
import os
from ast import literal_eval

from collections import OrderedDict

import maya.cmds as cmds
import maya.app.renderSetup.model.renderSetup as renderSetup

from rf_utils.context import context_info
from rf_utils import register_shot


import utils_v02 as utils
reload(utils)

import submit_utils
reload(submit_utils)

def switchEnv(childShot, *args, **kwargs):
    masterShot, currentShot, context, sceneID = utils.getShotInfoData("masterShot", "currentShot", "context", "sceneID")
  
    allChilds = utils.getShotInfoData("childShot")[0].split(",")
    
    if childShot[0] not in allChilds:
        _childShot = [(c, False) for c in allChilds if c and c != masterShot]
        _childShot.insert(-1, childShot)

        exportPath, rootPath = utils.exportAsmSeqData(childShot=_childShot, sceneID=sceneID)
        
        _, sequenceData, _ = utils.readAsm()
        childExists = sequenceData.keys()

        assetToBuild = sequenceData[childShot[0]]["build"]
        setDressing = sequenceData[childShot[0]]["setDressing"]
        buildData = utils.createBuildList(setDressing)
        selectedKeys = assetToBuild.keys()
        
        child = []
        for k, v in assetToBuild.iteritems():
            child += v
            
        selectedChilds = list(dict.fromkeys(child))
        
        buildData = utils.selectedBuild(buildData, selectedKeys, selectedChilds)
        
        # build #
        for x in buildData:
            dressName = x.keys()[0]
            data = x[dressName]["data"]
            typ = x[dressName]["type"]

            utils.buildEnv(dressName, typ, data)

        # update shot info #
        c = ",".join(allChilds)
        c = c + "," + childShot[0]

        utils.updateShotInfoData([("childShot", c)])

    
    # asmData with updated child shot #
    _, sequenceData, _ = utils.readAsm()
    childAsmData = sequenceData[childShot[0]]["setDressing"]
    childCacheData = sequenceData[childShot[0]]["cacheData"]["cache"]
    cameraCacheData = sequenceData[childShot[0]]["cacheData"]["camera"]
    childCacheData.update(cameraCacheData)
    
    # set + override position #
    assetInShot = utils.setPosition(asmData=childAsmData, sh=childShot[0])

    _assetInShot = []

    # create child's set #
    # add master and mtr too #
    for a in assetInShot:
        if not cmds.objExists(a):
            continue

        rootLocator = cmds.getAttr("{}.rootLocator".format(a))
        mtrNode = cmds.getAttr("{}.material".format(a)) + "RN"
        _assetInShot.append(rootLocator)
        _assetInShot.append(mtrNode)

    _assetInShot += assetInShot
    utils.createObjSets(childShot[0], items=_assetInShot, parent="setsItems")

    # hide everything #
    cmds.hide("setsItems")
    # show only item in child set #
    cmds.showHidden(childShot[0], a=True)
        
    # update shot info  #
    camera = cameraCacheData.keys()[0]
    workspace = sequenceData[childShot[0]]["workspace"]
    utils.updateShotInfoData([("currentShot", childShot[0]), ("camera", camera), ("workspace", workspace)])

    return childAsmData, childCacheData, childShot[0], context

def switchCache(cacheData, childShot):
    context, root, currentShot, childShots, asmSeqPath = utils.getShotInfoData("context", "root", "currentShot", "childShot", "asmSeqPath")
    childShotList = childShots.split(",")
    childShotList.remove(childShot[0])

    # filter cache #
    _cacheData = utils.filterCacheData(cacheData, "anim", "sim")

    with open(asmSeqPath, "r") as jsFile:
        asmSeqData = json.load(jsFile)

    for child in childShotList:
        path = "{}/{}".format(root, child)
        scene = context_info.ContextPathInfo(path=path)
        reg = register_shot.Register(scene)

        childCacheData = asmSeqData["sequenceData"][child]["cacheData"]["cache"]
        camData = asmSeqData["sequenceData"][child]["cacheData"]["camera"]
        childCacheData.update(camData)

        _childCacheData = utils.filterCacheData(childCacheData, "anim", "sim")

        # remove cache #
        removeCacheData = utils.getRemoveCacheData(_childCacheData, _cacheData, reg)
        utils.buildCache("remove", removeCacheData, regOverride=reg)
    

    # build cache #
    cacheBuildData = utils.createCacheData(_cacheData)
    utils.buildCache("build", cacheBuildData)
    
    utils.updateShotInfoData([("context", currentShot)])

    return context

def switchWorkspace(childShot, export=False, setup=False, exportLight=False):
    project, asmSeqPath, currentShot = utils.getShotInfoData("project", "asmSeqPath", "currentShot")
    with open(asmSeqPath, "r") as jsFile:
        asmSeqData = json.load(jsFile)

    # create current workspace folder 
    curr_workspacePath = asmSeqData["sequenceData"][currentShot]["workspace"]


    if not os.path.isdir(curr_workspacePath):
        os.makedirs(curr_workspacePath)
    
    # export necessary data of current shot into current workspacePath #
    if export:
        if exportLight:
            pass
            # export light rig #
            #lightRigPath = "{}/{}_lightRig.ma".format(curr_workspacePath, currentShot)
            #switchLightRig("export", lightRigPath)
            
        # export json #
        renderSetupPath = "{}/{}_renderSetup.json".format(curr_workspacePath, currentShot)
        
        with open(renderSetupPath, "w+") as file:
            json.dump(renderSetup.instance().encode(None), fp=file, indent=2, sort_keys=True)

        # export submit info #
        sg_cut_in, sg_cut_out = utils.sgGetFrameRange(project, currentShot)
        data = submit_utils.createSubmitInfo(renderSetupPath, currentShot, sg_cut_in, sg_cut_out)
        result = submit_utils.exportSubmitInfo(data, curr_workspacePath, currentShot)

        return result


    # setup for child shot #
    if setup:
        # check child workspace #
        child_workspacePath = asmSeqData["sequenceData"][childShot]["workspace"]
        lightRigPath = "{}/{}_lightRig.ma".format(child_workspacePath, childShot)
        renderSetupPath = "{}/{}_renderSetup.json".format(child_workspacePath, childShot)
        
        sg_cut_in, sg_cut_out = utils.sgGetFrameRange(project, childShot)
        utils.setFrameRange(sg_cut_in, sg_cut_out)

        if os.path.isfile(renderSetupPath):
            with open(renderSetupPath, "r") as file:
                renderSetup.instance().decode(json.load(file), renderSetup.DECODE_AND_OVERWRITE, None)

            renderSetup.instance().acceptImport()

        utils.updateShotInfoData([("workspace", child_workspacePath)])

def switchLightRig(step, lightRigPath, shotName=""):
    lightRigNode = "|lightRig:LightRig"
    if not cmds.objExists(lightRigNode):
        return cmds.warning("lightRigNode not found. Skip export lightRig")

    cmds.select(cl=True)

    if step == "export":
        # export light rig grp with material #
        allLightRigNodes = cmds.ls("lightRig:*")
        cmds.sets(allLightRigNodes, n="__lightRigSet")
        cmds.namespace(mergeNamespaceWithRoot=True, removeNamespace="lightRig")
        cmds.select("|LightRig")
        cmds.file(lightRigPath, force=True, options="v=0;", typ="mayaAscii", pr=True, es=True)
    
    else:
        allLightRigNodes = cmds.sets("__lightRigSet", q=True)

        if not os.path.isfile(lightRigPath):
            # which mean there is no light rig in this shot. use current shot instead #
            # restore the namespace #
            cmds.namespace(add="lightRig")
            for node in allLightRigNodes:
                try:
                    cmds.rename(node, "lightRig:{}".format(node))
                except Exception as e:
                    pass

            return cmds.warning("lightRigPath not found. Use lightRigNode from {} instead".format(shotName))

        # remove #
        if allLightRigNodes:
            cmds.delete(allLightRigNodes)
    
        # import new rig #
        cmds.file(lightRigPath, i=True, mergeNamespacesOnClash=False, namespace="lightRig", ignoreVersion=True)

def switchShot(childShot, switch_cache, exportWorkspace=True):

    # if remove shot, no need to export a current data # #
    # export workspace (lightRig.mb, renderSetup.json, submitInfo.json) #
    if exportWorkspace:
        switchWorkspace(childShot[0], export=True)

    instanceData, cacheData, currentShot, context = switchEnv(childShot)
    fullUpdate = True if context == currentShot else False

    cacheBuildData = utils.createCacheData(cacheData)

    # switch cache #
    if switch_cache:
        context = switchCache(cacheData, childShot)
        fullUpdate = True

    # import workspace #
    switchWorkspace(childShot[0], setup=True)

    # update reg #
    currentShot, root = utils.getShotInfoData("currentShot", "root")
    currentPath = root + "/" + currentShot
    sceneContext = context_info.ContextPathInfo(path=currentPath)
    reg = register_shot.Register(sceneContext)

    childShots= utils.getShotInfoData("childShot")[0]
    childShotList = childShots.split(",")

    # build cam if not switch cache #
    if not switch_cache:
        _cacheData = utils.filterCacheData(cacheData, "anim", "sim")
        camName = utils.getShotInfoData("camera")[0]
        camCacheData = _cacheData.get(camName, None)
        if camCacheData:
            _cacheBuildData = utils.createCacheData({camName: camCacheData})
            utils.buildCache("build", _cacheBuildData)

    return instanceData, cacheData, fullUpdate, currentShot, currentPath, sceneContext, reg, cacheBuildData, childShots
