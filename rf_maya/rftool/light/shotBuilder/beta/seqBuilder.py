from PySide2 import QtWidgets, QtCore, QtGui

import json
import yaml
import os
import sys

import maya.cmds as cmds
import pymel.core as pm

from rf_utils.context import context_info
from rf_utils import register_shot

import utils
reload(utils)







def sequenceBuilder(buildMaster=True, buildChild=[], forceBuild=False, *args, **kwargs):
    asm = readAsm()
    if asm:
        masterShot, sequenceData, asmSeqPath = asm
        childExists = sequenceData.keys()
        if buildChild:
            _buildChild = [b[0] for b in buildChild]
            childShot = [(i, False) for i in childExists if i not in _buildChild]
            _childShot = buildChild + childShot
            asmSeqPath, rootPath = exportAsmSeqData(childShot=_childShot)
            masterShot, sequenceData, asmSeqPath = readAsm()
        else:
            masterShot, sequenceData, asmSeqPath = asm
    else:
        if buildChild:
            childShot = buildChild
        else:
            childShot = []

        asmSeqPath, rootPath = exportAsmSeqData(childShot=childShot)
        masterShot, sequenceData, asmSeqPath = readAsm()


    childExists = sequenceData.keys()


    # build master #
    if buildMaster:
        items = []
        masterShotData = sequenceData[masterShot]
        for data in createBuildList(masterShotData):
            items += utils.build(data, *args, **kwargs)
            # updateMaterial=True, gpuPreview=gpuPreview, proxyMode=proxyMode, prxPercentage=prxPercentage
        masterSet = createObjSets(masterShot, items)

        # ref masterShot cam #
        cameraData = masterShotData["cacheData"]["camera"]
        cameraNs = cameraData[cameraData.keys()[0]]["namespace"]
        cameraPath = cameraData[cameraData.keys()[0]]["path"]

        if not cmds.namespace(exists=cameraNs):
            cmds.file(cameraPath, r=True, type="Alembic", ignoreVersion=True, gl=True, mergeNamespacesOnClash=False, namespace=cameraNs)


    # build childs #
    if not buildChild:
        return

    allChildItems = []

    check = [i[0] for i in buildChild]

    for sh in sequenceData:
        if sh not in check:
            continue

        items = []
        c = []
        typ = sequenceData[sh]["type"]
        build = sequenceData[sh]["build"]

        if typ == "master":
            continue

        # build all #
        if forceBuild:
            setDressing = sequenceData[sh]["setDressing"]
            buildData = []

            for key, values in setDressing.iteritems():
                typ = values["type"]
                path = values["path"]
                
                # read json data
                with open(path, 'r') as jsFile:
                    data = json.load(jsFile)

                buildData.append(data)

        # build only in "build" key
        else:
            # get raw build data #
            buildData = createBuildList(sequenceData[sh])

            selectedKeys = build.keys()

            for k, v in build.iteritems():
                c += v

            # remove duplicate child #
            selectedChilds = list(dict.fromkeys(c))

            # update build data with selected items and childs #
            buildData = utils.selectedBuild(buildData, selectedKeys, selectedChilds)

        for data in buildData:
            items += utils.build(data, *args, **kwargs)
            # updateMaterial=False, gpuPreview=gpuPreview, proxyMode=proxyMode, prxPercentage=prxPercentage
            allChildItems += items


    # hide set #
    childsSets = createObjSets("{}_childs".format(masterShot), allChildItems)

    cmds.hide(childsSets)

def convertDictToList(d):
    y = [e for i in d.values() for e in i]
    y = list(dict.fromkeys(y))

    return y

def readAsm():
    try:
        asmSeqPath = cmds.getAttr("seqBuilder_shotInfo.asmSeqPath")
    except:
        asmSeqPath = "{}/lightingData/asmSequenceData.json".format(context_info.ContextPathInfo().path.data().abs_path())
    
    if os.path.isfile(asmSeqPath):
        with open(asmSeqPath, "r") as jsFile:
            asmSeqData = json.load(jsFile)
    
        masterShot = asmSeqData["masterShot"]
        sequenceData = asmSeqData["sequenceData"]
    
        return masterShot, sequenceData, asmSeqPath
    else:
        return False

def createBuildList(v):
    buildData = []
    for key, values in v["setDressing"].iteritems():
        path = values["path"]

        with open(path, 'r') as jsFile:
            data = json.load(jsFile)
        
        buildData.append(data)

    return buildData

def createObjSets(setsName, items):
    cmds.select(cl=True)
    if not cmds.objExists(setsName):
        setsName = cmds.sets(n=setsName)

    cmds.sets(items, add=setsName)
    cmds.select(cl=True)

    return setsName

def switchEnv(childShot, forceBuild):
    cmds.lockNode("seqBuilder_shotInfo", l=False)
    asmSeqPath = cmds.getAttr("seqBuilder_shotInfo.asmSeqPath")
    masterShot = cmds.getAttr("seqBuilder_shotInfo.masterShot")
    currentShot = cmds.getAttr("seqBuilder_shotInfo.currentShot")

    update = False

    childShot, rebuild = childShot

    allChilds = cmds.getAttr("seqBuilder_shotInfo.childShot").split(",")

    # build childShot if not exists #
    if childShot not in allChilds:
        sequenceBuilder(buildMaster=False, buildChild=[(childShot, False)])
    else:
        if forceBuild:
            sequenceBuilder(buildMaster=False, buildChild=[(childShot, True)], forceBuild=True)

    # asmData with updated a child shot #
    with open(asmSeqPath, "r") as jsFile:
        asmSeqData = json.load(jsFile)

    masterShotBuildData = asmSeqData["sequenceData"][masterShot]["build"]
    masterShotAsmData = asmSeqData["sequenceData"][masterShot]["setDressing"]
    masterShotCacheData = asmSeqData["sequenceData"][masterShot]["cacheData"]["cache"]

    #childShotBuildData = asmSeqData["sequenceData"][childShot]["build"]
    childHideObj = asmSeqData["sequenceData"][childShot]["hide"]
    childAsmData = asmSeqData["sequenceData"][childShot]["setDressing"]
    childCacheData = asmSeqData["sequenceData"][childShot]["cacheData"]["cache"]

    if childShot != currentShot:
        childSets = masterShot + "_childs"
        cmds.hide(childSets)
        cmds.hide(childHideObj)


        update = True

        # camera setup #
        currentCam = cmds.getAttr("seqBuilder_shotInfo.camera")

        childCamData = asmSeqData["sequenceData"][childShot]["cacheData"]["camera"]
        childCameraNs = childCamData[childCamData.keys()[0]]["namespace"]
        childCameraPath = childCamData[childCamData.keys()[0]]["path"]

        refNode = pm.ls("{}RN".format(currentCam))[0]
        cmds.file(childCameraPath, loadReference=refNode.name(), type="Alembic")
        refNode.referenceFile()._setNamespace(childCameraNs)

        newRefName = "{}RN".format(childCameraNs)
        refNode.unlock()
        refNode.rename(newRefName)
        refNode.lock()

        c = ",".join(allChilds)
        if childShot not in allChilds:
            c = c + "," + childShot
        
        attrs = [("currentShot", childShot), ("camera", childCameraNs), ("childShot", c)]
        for attr, val in attrs:
            cmds.setAttr("seqBuilder_shotInfo.{}".format(attr), l=False)
            cmds.setAttr("seqBuilder_shotInfo.{}".format(attr), val, type="string")
            cmds.setAttr("seqBuilder_shotInfo.{}".format(attr), l=True)


    assetInShot = setPosition(childAsmData)
    cmds.showHidden(assetInShot, a=True)

    # set frame range #
    prj = os.environ.get("active_project")
    sg_cut_in, sg_cut_out = utils.sgGetFrameRange(prj, childShot)

    cmds.playbackOptions(min=sg_cut_in, max=sg_cut_out, ast=sg_cut_in, aet=sg_cut_out)

    if childShot[0][0] == masterShot:
        return masterShotAsmData, masterShotCacheData, update
    else:
        return childAsmData, childCacheData, update

def setPosition(*args):
    assetInShot = []
    for setDressing in args:
        for dressName in setDressing:
            path = setDressing[dressName]["path"]
        
            with open(path, "r") as jsFile:
                instanceData = json.load(jsFile)
                    
            for assetName in instanceData:
                for child in instanceData[assetName]["child"]:

                    if not cmds.objExists(child):
                        cmds.warning("{} is not exist".format(child))
                        
                    pos = instanceData[assetName]["child"][child]["position"]
                    utils.setInitialPos(child, pos) 
                    assetInShot.append(child)

    return assetInShot
    