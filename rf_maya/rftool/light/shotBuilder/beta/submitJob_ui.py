from PySide2 import QtWidgets, QtCore, QtGui
from shiboken2 import wrapInstance
import pymel.core as pm
import maya.cmds as cmds

import json
import os

from rf_utils.ui.flatUi import main_ui, configVar
reload(main_ui)
reload(configVar)

import utils
reload(utils)

import shotSwitcher
reload(shotSwitcher)


class SubmitJob_ui(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(SubmitJob_ui, self).__init__(parent)

        self.__top_layout = QtWidgets.QGridLayout(self)

        _, self.sequenceData, _ = utils.readAsm()
        self.childExists = utils.getShotInfoData("childShot")[0].split(",")
        self.shots = sorted([k for k in self.sequenceData.keys() if k in self.childExists])

        # export submit data for currentShot #
        self.currentShot = utils.getShotInfoData("currentShot")[0]
        shotSwitcher.switchWorkspace(self.currentShot, export=True)

    	self.__build_ui()


    def __build_ui(self):
        scroll = QtWidgets.QScrollArea(self)
    	top_wid = QtWidgets.QWidget(self)
    	top_wid_lay = QtWidgets.QGridLayout(top_wid)

        chunks = [self.shots[i:i+4] for i in xrange(0, len(self.shots), 4)]

        self.shotWidget_data = {}

        for row, d in enumerate(chunks):
            for col, shot in enumerate(d):
                workspace = self.sequenceData[shot]["workspace"]
                renderLayersData = self.getRenderLayer(workspace)
                self.shotWidget = ShotWidget(shot, renderLayersData, parent=self)
                top_wid_lay.addWidget(self.shotWidget, row, col, 1, 1)
                self.shotWidget_data.update({shot: {"data": renderLayersData, "widget": self.shotWidget}})

        scroll.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        scroll.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        scroll.setWidgetResizable(True)
        scroll.setWidget(top_wid)

    	bot_wid = QtWidgets.QWidget(self)
    	bot_wid_lay = QtWidgets.QGridLayout(bot_wid)
        self.submit_btn = QtWidgets.QPushButton("Submit Job To Deadline", self)
        self.submit_btn.clicked.connect(self.submit)
        self.submit_btn.setObjectName("blueBtn")
        bot_wid_lay.addWidget(self.submit_btn)

        right_wid = QtWidgets.QGroupBox("Edit Parameters", self)
        right_wid_lay = QtWidgets.QGridLayout(right_wid)
        right_wid_lay.setContentsMargins(0, 0, 0, 0)
        self.editWid = EditParmsWidget(parent=self)
        right_wid_lay.addWidget(self.editWid)

        self.__top_layout.addWidget(scroll, 0, 0, 1, 1)
        self.__top_layout.addWidget(bot_wid, 1, 0, 1, 1)
        self.__top_layout.addWidget(right_wid, 0, 1, 1, 2)

    def submit(self):
        for shot, v in self.shotWidget_data.iteritems():
            grpBox = v["widget"].grpBox
            isChecked = grpBox.isChecked()

            if isChecked:
                data_lwd = v["widget"].data_lwd
                layerData = v["data"]
                itemCount = data_lwd.count()

                checkedLayer = []
                for i in range(itemCount):
                    item = data_lwd.item(i)
                    checkState = item.checkState()
                    if checkState != QtCore.Qt.Checked:
                        continue
                    d = layerData[item.text()]
                    checkedLayer.append((item.text(), d))
                    
                if checkedLayer:
                    for layer, value in checkedLayer:
                        frames = value["frames"]
                        camera = value["camera"]
                        output_dir = value["output_dir"]
                        output_path = value["output_path"]
                        output_prefix = value["output_prefix"]
                        pool = value["pool"]
                        renderer = value["renderer"]

                        
                        print "-"*50
                        print "Submit Info : Layer : {}".format(layer)
                        print "\t app ", "maya"
                        print "\t renderer ", renderer
                        print "\t frames", frames
                        print "\t camera ", camera
                        print "\t output_dir ", output_dir
                        print "\t output_path ", output_path
                        print "\t output_prefix ", output_prefix
                        print "\t pool ", pool
                        print "-"*50

                #print shot
                #print "\t", layerData


    
    def getRenderLayer(self, workspace):
        if not os.path.isdir(workspace):
            return False

        renderSetupFile = [i for i in os.listdir(workspace) if "_submitInfo.json" in i]
        if not renderSetupFile:
            return False

        renderSetupFile = renderSetupFile[0]
        path = os.path.join(workspace,renderSetupFile)

        with open(path, "r") as jsFile:
            data = json.load(jsFile)

        return data



class ShotWidget(QtWidgets.QWidget):
    def __init__(self, shotName, data, parent=None):
        super(ShotWidget, self).__init__(parent)

        self.parent = parent
        self.shotName = shotName

        self.data = data
        self.masterShot = True
        if not self.data:
            self.data = {"Layer Not Found": {"renderable": False}}

        self.__top_layout = QtWidgets.QGridLayout(self)
        self.__top_layout.setContentsMargins(0,0,0,0)
        self.__top_layout.setSpacing(0)


        self.__build_ui()
        
    def __build_ui(self):
        self.grpBox = QtWidgets.QGroupBox(self.shotName, self)
        self.grpBox_lay = QtWidgets.QGridLayout(self.grpBox)
        self.grpBox.setCheckable(True)
        self.grpBox.setChecked(False)
        self.grpBox.setFlat(True)

        self.data_lwd = QtWidgets.QListWidget(self)
        self.data_lwd.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.data_lwd.customContextMenuRequested.connect(self.show_menu)

        for layer, values in self.data.iteritems():
            item = QtWidgets.QListWidgetItem(layer)

            renderable = values["renderable"]

            if layer == "Layer Not Found":
                item.setBackground(QtGui.QColor(255, 0, 0, 50))
                self.grpBox.setCheckable(False)
                self.grpBox.setEnabled(False)
            else:
                item.setFlags(item.flags() | QtCore.Qt.ItemIsUserCheckable)
                if renderable:
                    item.setCheckState(QtCore.Qt.Checked)
                else:
                    item.setCheckState(QtCore.Qt.Unchecked)
                    item.setBackground(QtGui.QColor(255, 0, 0, 30))

            self.data_lwd.addItem(item)
        

        self.grpBox_lay.addWidget(self.data_lwd)

        self.__top_layout.addWidget(self.grpBox)

    def show_menu(self, position):
        menu = QtWidgets.QMenu(self.data_lwd)
        editParmsAction = menu.addAction("Edit Parameters")
        editParmsAction.triggered.connect(self.editParms)
        menu.exec_(self.data_lwd.mapToGlobal(position))

    def editParms(self):
        print self.data
        #self.editParmsWin = main_ui.MainWindow(ui_name="EditParmsWidget", size=(420,550), title="Edit Parameters", widget=EditParmsWidget(), onTop=True, parent=self.parent)
        #self.editParmsWin.show()


class EditParmsWidget(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(EditParmsWidget, self).__init__(parent)
      
        self.__top_layout = QtWidgets.QGridLayout(self)

        self.layers = ["1", "2", "3"]

        self.__build_ui()
        
    def __build_ui(self):
        top_wid = QtWidgets.QWidget(self)
        top_wid_lay = QtWidgets.QHBoxLayout(top_wid)
        self.shotName_label = QtWidgets.QLabel("Test", top_wid)
        self.sgRange_label = QtWidgets.QLabel("1001,1050", top_wid)
        top_wid_lay.addWidget(self.shotName_label)
        top_wid_lay.addWidget(self.sgRange_label)

        center_wid = QtWidgets.QWidget(self)
        center_wid_lay = QtWidgets.QVBoxLayout(center_wid)

        for layer in self.layers:
            wid = QtWidgets.QWidget(self)
            wid_lay = QtWidgets.QHBoxLayout(wid)
            layer_label = QtWidgets.QLabel(layer, wid)
            self.start_input = QtWidgets.QLineEdit(wid)
            self.end_input = QtWidgets.QLineEdit(wid)
            wid_lay.addWidget(layer_label)
            wid_lay.addWidget(self.start_input)
            wid_lay.addWidget(self.end_input)

            center_wid_lay.addWidget(wid)

        center_wid_lay.addStretch()
        scroll = QtWidgets.QScrollArea(self)
        scroll.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        scroll.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        scroll.setWidgetResizable(True)
        scroll.setWidget(center_wid)

        self.apply_btn = QtWidgets.QPushButton("Apply", self)

        self.__top_layout.addWidget(top_wid)
        self.__top_layout.addWidget(scroll)
        self.__top_layout.addWidget(self.apply_btn)




def runUI(ui_name="SubmitJob_ui", size=(1000, 450), title="Multi-Shot Submitter"):
    if pm.window(ui_name, exists=True):
        pm.deleteUI(ui_name)
    win = main_ui.MainWindow(ui_name=ui_name, size=size, title=title, widget=SubmitJob_ui(), parent=None)
    win.show()
    