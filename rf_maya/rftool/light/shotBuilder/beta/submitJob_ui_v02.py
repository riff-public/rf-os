from PySide2 import QtWidgets, QtCore, QtGui
from shiboken2 import wrapInstance
import pymel.core as pm
import maya.cmds as cmds

import json
import os

from rf_utils.ui.flatUi import main_ui, configVar
reload(main_ui)
reload(configVar)

import utils
reload(utils)

import submit_utils
reload(submit_utils)

import shotSwitcher
reload(shotSwitcher)


class SubmitJob_ui(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(SubmitJob_ui, self).__init__(parent)

        self.__top_layout = QtWidgets.QGridLayout(self)

        _, self.sequenceData, _ = utils.readAsm()
        self.childExists = utils.getShotInfoData("childShot")[0].split(",")
        self.shots = sorted([k for k in self.sequenceData.keys() if k in self.childExists])

        # export submit data for currentShot #
        self.currentShot = utils.getShotInfoData("currentShot")[0]
        shotSwitcher.switchWorkspace(self.currentShot, export=True)

    	self.__build_ui()


    def __build_ui(self):
        scroll = QtWidgets.QScrollArea(self)
    	top_wid = QtWidgets.QWidget(self)
    	top_wid_lay = QtWidgets.QGridLayout(top_wid)

        chunks = [self.shots[i:i+4] for i in xrange(0, len(self.shots), 4)]

        self.shotWidget_data = {}

        for row, d in enumerate(chunks):
            for col, shot in enumerate(d):
                workspace = self.sequenceData[shot]["workspace"]
                self.renderLayersData, self.ovrPath = self.getRenderLayer(workspace)
                if not self.renderLayersData["renderLayers"]:
                    cmds.warning("Layer not found on shot {}".format(shot))
                    continue

                self.shotWidget = ShotWidget(shot, self.renderLayersData, parent=self)
                top_wid_lay.addWidget(self.shotWidget, row, col, 1, 1)
                self.shotWidget_data.update({shot: {"data": self.renderLayersData,
                                                    "widget": self.shotWidget,
                                                    "ovrPath": self.ovrPath,
                                                    "workspace": workspace}})

        scroll.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        scroll.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        scroll.setWidgetResizable(True)
        scroll.setWidget(top_wid)

    	bot_wid = QtWidgets.QWidget(self)
    	bot_wid_lay = QtWidgets.QGridLayout(bot_wid)
        self.submit_btn = QtWidgets.QPushButton("Submit Job To Deadline", self)
        self.submit_btn.clicked.connect(self.submit)
        self.submit_btn.setObjectName("blueBtn")
        bot_wid_lay.addWidget(self.submit_btn)

        self.__top_layout.addWidget(scroll, 0, 0, 1, 1)
        self.__top_layout.addWidget(bot_wid, 1, 0, 1, 1)

    def submit(self):
        for shot, v in self.shotWidget_data.iteritems():
            grpBox = v["widget"].grpBox
            isChecked = grpBox.isChecked()

            if not isChecked:
                continue

            data_table = v["widget"].data_table
            layerData = v["data"]["renderLayers"]
            email = v["data"]["email"]
            ovrPath = v["ovrPath"]
            workspace = v["workspace"]
            itemCount = data_table.rowCount()

            # create a submit info data to be used in deadline #
            ovrLayer = {"renderLayers": {}, "email": email}
            for i in range(itemCount):
                item = data_table.item(i, 0)
                startFrameItem = data_table.item(i, 1).text()
                endFrameItem = data_table.item(i, 2).text()
                checkState = item.checkState()

                if checkState != QtCore.Qt.Checked:
                    continue

                d = layerData[item.text()]
                overrideFrame = "{}-{}".format(startFrameItem, endFrameItem)
                x = d.copy()
                x["frames"] = overrideFrame
                ovrLayer["renderLayers"][item.text()] = x

            with open(ovrPath, "w") as file:
                json.dump(ovrLayer, file, indent=2, sort_keys=True)

            # submit cmd job to deadline #
            ms = cmds.file(q=True, loc=True)
            cs = shot
            rt = utils.getShotInfoData("root")[0]
            si = ovrPath
            job_info_path = "{}/cmd_job_info.job".format(workspace)
            job_props_path = "{}/cmd_job_props.job".format(workspace)

            

            job_info_path, job_props_path, custom_env = submit_utils.createDeadlineJobInfo(ms, cs, rt, si, job_info_path, job_props_path)
            jobID, sended_job = submit_utils.send_to_deadline_cmd(job_info_path, job_props_path, custom_env)

            print jobID, sended_job

    
    def getRenderLayer(self, workspace):
        if not os.path.isdir(workspace):
            return False

        renderSetupFile = [i for i in os.listdir(workspace) if "_submitInfo.json" in i]
        if not renderSetupFile:
            return False

        renderSetupFile = renderSetupFile[0]
        path = os.path.join(workspace,renderSetupFile)

        ovrPath = path.replace(".json", "_ovr_.json")

        with open(path, "r") as jsFile:
            data = json.load(jsFile)

        return data, ovrPath



class ShotWidget(QtWidgets.QWidget):
    def __init__(self, shotName, data, parent=None):
        super(ShotWidget, self).__init__(parent)

        self.parent = parent
        self.shotName = shotName

        self.renderLayersData = data["renderLayers"]
        self.sgStart, self.sgEnd = data["sgRange"]

        self.rowCount = len([k for k, v in self.renderLayersData.iteritems() if v["renderable"]])
        self.colCont = 3

        self.__top_layout = QtWidgets.QGridLayout(self)
        self.__top_layout.setContentsMargins(0,0,0,0)
        self.__top_layout.setSpacing(0)


        self.__build_ui()
        
    def __build_ui(self):
        self.grpBox = QtWidgets.QGroupBox(self.shotName, self)
        self.grpBox_lay = QtWidgets.QGridLayout(self.grpBox)
        self.grpBox.setCheckable(True)
        self.grpBox.setChecked(False)
        self.grpBox.setFlat(True)

        self.data_table = QtWidgets.QTableWidget(self)
        self.data_table.setRowCount(self.rowCount)
        self.data_table.setColumnCount(self.colCont)
        self.data_table.verticalHeader().hide()
        self.data_table.setHorizontalHeaderLabels(["Layer", "Start", "End"])

        self.data_table.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.data_table.customContextMenuRequested.connect(self.show_menu)
        self.data_table.cellChanged.connect(self.editCell)

        self.ovrColor = QtGui.QColor(200, 155, 0, 100)
        self.defaultColor = QtGui.QColor(50, 50, 50, 255)

        i = 0
        for layer, values in self.renderLayersData.iteritems():
            renderable = values["renderable"]
                
            start, end = values["frames"].split("-")
            self.layerItem = QtWidgets.QTableWidgetItem(layer)
            self.layerItem.setFlags(QtCore.Qt.ItemFlag.ItemIsUserCheckable | QtCore.Qt.ItemFlag.ItemIsEnabled)
            self.layerItem.setCheckState(QtCore.Qt.CheckState.Checked)
            self.startItem = QtWidgets.QTableWidgetItem(start)
            self.endItem = QtWidgets.QTableWidgetItem(end)

            if start != str(self.sgStart):
                self.startItem.setBackground(self.ovrColor)
            if end != str(self.sgEnd):
                self.endItem.setBackground(self.ovrColor)

            if not renderable:
                continue
       
            self.data_table.setItem(i, 0, self.layerItem)
            self.data_table.setItem(i, 1, self.startItem)
            self.data_table.setItem(i, 2, self.endItem)

            i += 1

        self.data_table.horizontalHeader().setSectionResizeMode(0, QtWidgets.QHeaderView.Stretch)
        self.data_table.horizontalHeader().setSectionResizeMode(1, QtWidgets.QHeaderView.ResizeToContents)
        self.data_table.horizontalHeader().setSectionResizeMode(2, QtWidgets.QHeaderView.ResizeToContents)

        self.grpBox_lay.addWidget(self.data_table)

        self.__top_layout.addWidget(self.grpBox)

    def editCell(self, row, col):
        val = self.data_table.item(row, col).text()
        if col == 1:
            if val != str(self.sgStart):
                self.data_table.item(row, col).setBackground(self.ovrColor)
            else:
                self.data_table.item(row, col).setBackground(self.defaultColor)
        if col == 2:
            if val != str(self.sgEnd):
                self.data_table.item(row, col).setBackground(self.ovrColor)
            else:
                self.data_table.item(row, col).setBackground(self.defaultColor)

    def show_menu(self, pos):
        item = self.data_table.itemAt(pos)

        if not item:
            return

        row = item.row()
        col = item.column()

        if col != 0:
            return

        menu = QtWidgets.QMenu(self.data_table)
        action1 = menu.addAction("SG Range")
        action1.triggered.connect(lambda: self.setRange("sg", row))
        action2 = menu.addAction("User Override")
        action2.triggered.connect(lambda: self.setRange("user", row))

        x,y = pos.toTuple()
        offset = QtCore.QPoint(x+2, y+23)
        menu.exec_(self.data_table.mapToGlobal(offset))

    def setRange(self, typ, row):
        layerName = self.data_table.item(row, 0).text()
        startCell = self.data_table.item(row, 1)
        endCell = self.data_table.item(row, 2)
        if typ == "sg":
            s = str(self.sgStart)
            e = str(self.sgEnd)
        else:
            s, e = self.renderLayersData[layerName]["frames"].split("-")

        startCell.setText(s)
        endCell.setText(e)




def runUI(ui_name="SubmitJob_ui", size=(1000, 450), title="Multi-Shot Submitter"):
    if pm.window(ui_name, exists=True):
        pm.deleteUI(ui_name)
    win = main_ui.MainWindow(ui_name=ui_name, size=size, title=title, widget=SubmitJob_ui(), parent=None)
    win.show()
    