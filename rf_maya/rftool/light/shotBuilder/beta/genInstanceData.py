import yaml
import os
import re
import json

from rf_utils import register_shot
from rf_utils.context import context_info




def getDataInScene(rebuild):
    scene = context_info.ContextPathInfo()
    reg = register_shot.Register(scene)
    assets = reg.get.asset_list()
    
    data = {}
    use = ["shotdress", "set"]
    
    
    for asset in assets:
        assetType = reg.get.assetData[asset]["type"]
        if assetType in use:
            asset, outPath = createInstanceData(asset, reg, rebuild)
            data[asset] = {"type": assetType, "path": outPath}
            
    return data


def createInstanceData(asset, reg, rebuild):

    try:
        dataPath = reg.get.assetData[asset]["setShot"]["heroFile"]
    except:
        dataPath = reg.get.assetData[asset]["shotDress"]["heroFile"]

    name = dataPath.split("/")[-1].replace(".asm", ".instance.json")
    outPath = "/".join(dataPath.split("/")[0:-1]) + "/{}".format(name)

    if not os.path.isfile(outPath):
        rebuild = True
        
    if rebuild:
        with open(dataPath, 'r') as stream:
            shotDressData = yaml.safe_load(stream)
        
        assetData = {}
        checked = []
        
        for k, v in shotDressData.iteritems():
            name = v["shortName"]
            assetName = re.sub(r"[_0-9]", "", name)
            parents = v["parent"]
            assetDescription = v["assetDescription"]
            position = {name: v["position"]}
            hidden = v["hidden"]
            
            if hidden:
                continue
            
            if not os.path.isfile(assetDescription):
                continue
                
            if assetDescription not in checked:
                with open(assetDescription, 'r') as y:
                    data = yaml.safe_load(y)
                    
                if "lookdev" not in data.keys():
                    continue

                if "model" not in data.keys():
                    continue
                    
                prxPath = data["lookdev"]["md"][0]["rsproxy"]["heroFile"]
                cachePath = data["model"]["md"][0]["cache"]["heroFile"]
                mtrRender = data["lookdev"]["md"][0]["mtrRender"]["heroFile"]

                aiPath = prxPath.replace("rsproxy", "aiStandin").replace(".rs", ".ass")
                if not os.path.isfile(aiPath):
                    aiPath = None
                
                checked.append(assetDescription)
            
            if assetName not in assetData.keys():
                assetData[assetName] = {"name": name,
                                        "parent": parents,
                                        "assetDescription": assetDescription,
                                        "rsproxy": prxPath,
                                        "cachePath": cachePath,
                                        "mtrRender": mtrRender,
                                        "aiPath": aiPath,
                                        "child": {}}

            assetData[assetName]["child"].update(position)
    
        with open(outPath, "w") as jsFile:
            json.dump(assetData, jsFile, indent=2)


    return asset, outPath