from PySide2 import QtWidgets, QtCore, QtGui
from shiboken2 import wrapInstance
import pymel.core as pm
import maya.cmds as cmds

import maya.app.renderSetup.model.renderSetup as renderSetup
import maya.app.renderSetup.model.collection as collection
from maya import OpenMayaUI as omui

import os
import sys
import json

core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)

from rf_utils.context import context_info
from rf_utils import register_shot

from rf_utils.ui.flatUi import main_ui
reload(main_ui)

import utils
reload(utils)

try:
    utils.globalSettings()
except:
    cmds.warning("Some setting is not working properly")

def getMayaMainWindow():
    win = omui.MQtUtil_mainWindow()
    ptr = wrapInstance(long(win), QtWidgets.QMainWindow)
    return ptr

class ShotBuilder_ui(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(ShotBuilder_ui, self).__init__(parent)

        self.__top_layout = QtWidgets.QGridLayout(self)

        if utils.getDataInScene(False):
            self.instanceData = utils.getDataInScene(False)
        else:
            msg = QtWidgets.QMessageBox()
            msg.setWindowTitle("Data not found")
            msg.setText("Do you want to create a new data?")
            msg.setStandardButtons(msg.Yes | msg.No)
            msg.setIcon(msg.Question)
            ret = msg.exec_()

            if ret == msg.Yes:
                self.instanceData = utils.getDataInScene(True)
            else:
                self.instanceData = {}

        #self.instanceData = {'shotdress': {'path': 'D:/TD/local/build/test_data.json',
        #       'type': 'shotdress'}}


        self.currentFilter = "All"

        self.__build_ui()
        self.__connect_signals()


    def __build_ui(self):
        
        self.drawTreeWidget(self.instanceData)

        option_grpBox = QtWidgets.QGroupBox("Options", self)
        option_grpBox.setMinimumWidth(150)
        option_grpBox_lay = QtWidgets.QGridLayout(option_grpBox)

        filter_label = QtWidgets.QLabel("Filter", option_grpBox)
        self.filter_cbb = QtWidgets.QComboBox(option_grpBox)
        filterItems = ["All"] + self.instanceData.keys()
        self.filter_cbb.addItems(filterItems)

        option_grpBox_lay.addWidget(filter_label, 0, 0, 1, 1)
        option_grpBox_lay.addWidget(self.filter_cbb, 0, 1, 1, 1)

        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)

        self.build_btn = QtWidgets.QPushButton("Build/Update", self)
        self.build_btn.setObjectName("blueBtn")


        self.__top_layout.addWidget(self.treeWidget, 0, 0, 3, 1)
        self.__top_layout.addWidget(option_grpBox, 0, 1, 1, 1)
        self.__top_layout.addItem(spacerItem, 1, 1, 1, 1)
        self.__top_layout.addWidget(self.build_btn, 2, 1, 1, 1)

    def __connect_signals(self):
        self.filter_cbb.currentTextChanged.connect(self.getFilter)
        self.build_btn.clicked.connect(lambda: self.build(self.buildData))
        self.treeWidget.itemDoubleClicked.connect(self.doubleClicked)

    def drawTreeWidget(self, instanceData, update=False):
        headerLabels = ["Asset", "Type", "Ldv", "Proxy", "GPU", "Operator"]
        if not update:
            self.treeWidget = QtWidgets.QTreeWidget(self)
            self.treeWidget.setColumnCount(4)
            self.treeWidget.setHeaderLabels(headerLabels)
            self.treeWidget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
            self.treeWidget.customContextMenuRequested.connect(self.show_menu)
            self.treeWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)

        items = []
        self.buildData = []
        self.displayItems = []
        for key, values in instanceData.iteritems():
            typ = values["type"]
            path = values["path"]
            
            mainItem = QtWidgets.QTreeWidgetItem([key, typ])
            
            # read json data
            with open(path, 'r') as jsFile:
                data = json.load(jsFile)
            
            self.buildData.append(data)
            
            c = 0
            for k, v in sorted(data.iteritems()):
                ldv = v["isLdv"]
                gpuPath = v["cachePath"]
                rsproxy = v["rsproxy"]
                aiPath = v["aiPath"]
                child = v["child"]

                # list of shortname #
                shortNameList = []
                for _, s in child.iteritems():
                    shortNameList.append(s["shortName"])

                self.displayItems.append(k)

                # list nodes that already in scene #
                gpuNodes = []
                for i in cmds.ls(type="transform"):
                    i = i.split("|")[-1]
                    if k not in i:
                        continue
                    if "_inst" in i:
                        continue
                    if "rsProxy" in i:
                        continue
                    if i not in shortNameList:
                        continue
                    
                    gpuNodes.append(i.rpartition("_")[0])

                nodeCount = len(gpuNodes) if len(gpuNodes) else 0
                k_name = "{} ({}/{})".format(k, nodeCount, len(v["child"].keys()))
                
                isLdv = "Yes" if ldv else   "No"
                rs = "Yes" if rsproxy else "No"
                gpu = "Yes" if gpuPath else "No"
                operator = "Yes" if aiPath else "No"
                
                subItem = QtWidgets.QTreeWidgetItem([k_name, "", isLdv, rs, gpu, operator])
                
                if c%2:
                    for i in range(len(headerLabels)):
                        subItem.setBackground(i ,QtGui.QBrush(QtGui.QColor(122, 122, 122, 15)))


                subItem.setBackground(0 ,QtGui.QBrush(QtGui.QColor(0, 150, 0, 150)))
                conditions = [isLdv, rs, gpu, operator]
                for column, condition in enumerate(conditions):
                    if condition == "No":
                        subItem.setForeground(column + 2 ,QtGui.QBrush(QtGui.QColor(255, 0, 0, 150)))

                        subItem.setBackground(0 ,QtGui.QBrush(QtGui.QColor(129, 129, 54, 100)))

                for child in sorted(v["child"]):
                    childItem = QtWidgets.QTreeWidgetItem([child])
                    if cmds.objExists(child):
                        childItem.setForeground(0 ,QtGui.QBrush(QtGui.QColor(0, 100, 50, 200)))
                    
                    subItem.addChild(childItem)
                
                mainItem.addChild(subItem)
                c += 1

            items.append(mainItem)

        self.treeWidget.insertTopLevelItems(0, items)
        for item in items:
            self.treeWidget.expandItem(item)

        self.header = self.treeWidget.header()
        self.header.setSectionResizeMode(QtWidgets.QHeaderView.ResizeToContents)
        self.header.setStretchLastSection(False)
        self.header.setSectionResizeMode(5, QtWidgets.QHeaderView.Stretch)

    def show_menu(self, position):
        selectedItems = self.treeWidget.selectedItems()

        index = self.treeWidget.indexAt(position)

        if not index.isValid():
            return

        item = self.treeWidget.itemAt(position)

        if not item.parent():
            return

        if not item.child(0):
            return

        itemName = [i.text(0).split(" (")[0] for i in selectedItems]

        menu = QtWidgets.QMenu(self.treeWidget)

        buildAction = menu.addAction("Build/Update (Selected)")
        self.selectBuildData = self.buidFilter(self.buildData, itemName)
        #print self.selectBuildData
        buildAction.triggered.connect(lambda: self.build(self.selectBuildData))
        menu.addSeparator()
        action = menu.addAction("Reload Material (Selected)")
        action.triggered.connect(lambda: self.reloadMaterial(itemName))
        action2 = menu.addAction("Reload Material (All)")
        action2.triggered.connect(lambda: self.reloadMaterial([]))
        menu.addSeparator()
        action3 = menu.addAction("Update Table Data")
        action3.triggered.connect(self.updateTableData)

        x,y = position.toTuple()
        offset = QtCore.QPoint(x+2, y+23)
        menu.exec_(self.treeWidget.mapToGlobal(offset))

    def getFilter(self, item):
        self.currentFilter = item
        self.treeWidget.clear()
        if self.currentFilter != "All":
            self.filterData = {self.currentFilter: self.instanceData[self.currentFilter]}
            self.drawTreeWidget(self.filterData, update=True)
        else:
            self.drawTreeWidget(self.instanceData, update=True)


    def build(self, buildData):
        for data in buildData:
            if not data:
                continue

            utils.build(data)

        msg = QtWidgets.QMessageBox(self)
        msg.setWindowTitle("Done")
        msg.setText("Build Done")
        msg.setStandardButtons(msg.Ok)
        ret = msg.exec_()

    def doubleClicked(self, item):
        obj = item.text(0)
        if cmds.objExists(obj):
            cmds.select(obj)

    def reloadMaterial(self, items):
        if len(items) >= 1 and len(items) <= 5:
            mtrNodes = ["{}_mtrRN".format(i) for i in items]
        else:
            mCount = len(items)
            if mCount == 0:
                mCount = "all"
            msg = QtWidgets.QMessageBox(self)
            msg.setWindowTitle("Sure?")
            msg.setText("Are you sure to reload {} Materials?".format(mCount))
            msg.setIcon(msg.Warning)
            msg.setStandardButtons(msg.Yes | msg.No)
            ret = msg.exec_()
            if ret == msg.No:
                return

            mtrNodes = [i for i in cmds.ls(type="reference") if "_mtrRN" in i and i.split("_mtrRN")[0] in self.displayItems]


        for mtr in mtrNodes:
            if not cmds.objExists(mtr):
                continue
            
            cmds.file(loadReference=mtr)

    def updateTableData(self):
        msg = QtWidgets.QMessageBox(self)
        msg.setWindowTitle("Sure?")
        msg.setText("Do you want to update data? (Might take a bit of time to process)")
        msg.setIcon(msg.Warning)
        msg.setStandardButtons(msg.Yes | msg.No)
        ret = msg.exec_()
        if ret == msg.No:
            return

        self.treeWidget.clear()
        self.instanceData = utils.getDataInScene(True)
        self.drawTreeWidget(self.instanceData, update=True)

        msg = QtWidgets.QMessageBox(self)
        msg.setWindowTitle("Done!")
        msg.setText("Data is up to date")
        msg.setStandardButtons(msg.Ok)
        msg.exec_()

    def buidFilter(self, data, selectedItems):
        selectedBuildData = []
        for d in data:
            c = {}
            for name in d:
                if name not in selectedItems:
                    continue
                c[name] = d[name]

            selectedBuildData.append(c)

        return selectedBuildData


def runUI(ui_name="ShotBuilder_ui", size=(800, 600), title="Light | Shot Builder"):
    if pm.window(ui_name, exists=True):
        pm.deleteUI(ui_name)
    win = main_ui.MainWindow(ui_name=ui_name, size=size, title=title, widget=ShotBuilder_ui(), parent=getMayaMainWindow())
    win.show()
    