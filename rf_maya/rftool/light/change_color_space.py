try:
    from PySide import QtGui, QtCore
    import PySide.QtGui as QtWidgets
except ImportError:
    from PySide2 import QtGui, QtCore, QtWidgets

import pymel.core as pm

def change_colorSpace(gamma_choice):
    obj_ls = pm.ls(sl = True)
    if obj_ls == []:
        return pm.warning("Please select some object or material")
    for obj in obj_ls:
        sg = obj.shadingGroups()[0]
        shd = pm.ls(sg.listConnections(), mat = True)
        if shd[0].nodeType() == "VRayMtl2Sided":
            shd = pm.ls(shd[0].listConnections(), mat = True)
        for s in shd:
            gamma_tx = s.listConnections(type = "gammaCorrect")
            if gamma_tx != []:
                file_tx = gamma_tx[0].listConnections(type = "file")[0]
                file_tx.setAttr("colorSpace", "sRGB")
                if gamma_choice == True:
                    file_tx.connectAttr("outColor", "{}.diffuseColor".format(s), force = True)
                    pm.delete(gamma_tx)
                else:
                    gamma_tx[0].setAttr("gammaX", 1)
                    gamma_tx[0].setAttr("gammaY", 1)
                    gamma_tx[0].setAttr("gammaZ", 1)
            else:
                file_tx = pm.listConnections("{}.diffuseColor".format(s))
                if file_tx != []:
                    if file_tx[0].nodeType() == "file":
                        file_tx[0].setAttr("colorSpace", "sRGB")

class re_colorSpace_win(QtWidgets.QWidget):
    def __init__(self):
        super(re_colorSpace_win, self).__init__(None, QtCore.Qt.WindowStaysOnTopHint)
        
        self.setFixedSize(250, 130)
        self.setWindowTitle("Re-ColorSpace")

        self.__top_layout = QtWidgets.QVBoxLayout(self)

        self.delete_gamma = False
        
        self.build_ui()
        self.connect_signals()

    def build_ui(self):
        radio_btn_grp = QtWidgets.QGroupBox("Select Your Choice ... ")
        radio_btn_grp_layout = QtWidgets.QHBoxLayout(radio_btn_grp)
        self.__top_layout.addWidget(radio_btn_grp)
        self.keep_gamma = QtWidgets.QRadioButton("Keep Gamma")
        self.keep_gamma.setChecked(True)
        self.del_gamma = QtWidgets.QRadioButton("Delete Gamma")
        radio_btn_grp_layout.addWidget(self.keep_gamma)
        radio_btn_grp_layout.addWidget(self.del_gamma)
        self.do_btn = QtWidgets.QPushButton("DO It!", self)
        self.__top_layout.addWidget(self.do_btn)

    def connect_signals(self):
        self.keep_gamma.toggled.connect(lambda:self.btnstate(self.keep_gamma))
        self.del_gamma.toggled.connect(lambda:self.btnstate(self.del_gamma))
        self.do_btn.clicked.connect(self.do_cb)

    def btnstate(self, btn):
        if btn.isChecked():
            if btn.text() == "Keep Gamma":    
                self.delete_gamma = False
            else:
                self.delete_gamma = True
                
        return self.delete_gamma
        
    def do_cb(self):
        change_colorSpace(self.delete_gamma)

def run():
    global win
    win = re_colorSpace_win()
    win.show()
