
import pymel.core as pmc
import maya.cmds as mc
import maya.mel as mm
from ncscript2.pymel import core
reload(core)
Node = core.Node()

def curve_shape(shape=''):
    curve = { 
                'sphere'        : [(0,1.250064,0),(0,1,0),(1.93883e-007,0.92388,-0.382683),(3.58248e-007,0.707107,-0.707107),(4.68074e-007,0.382683,-0.923879),(5.06639e-007,0,-1),(0,0,-1.250064),(5.06639e-007,0,-1),(4.68074e-007,-0.382683,-0.923879),(3.58248e-007,-0.707107,-0.707107),(1.93883e-007,-0.92388,-0.382683),(0,-1,0),(0,-1.250064,0),(0,-1,0),(-5.70243e-008,-0.92388,0.382683),(-1.05367e-007,-0.707107,0.707107),(-1.37669e-007,-0.382683,0.92388),(-1.49012e-007,0,1),(0,0,1.250064),(-1.49012e-007,0,1),(-1.37669e-007,0.382683,0.92388),(-1.05367e-007,0.707107,0.707107),(-5.70243e-008,0.92388,0.382683),(0,1,0),(0.382683,0.92388,0),(0.707107,0.707107,0),(0.92388,0.382683,0),(1,0,0),(1.250064,0,0),(1,0,0),(0.92388,-0.382683,0),(0.707107,-0.707107,0),(0.382683,-0.92388,0),(0,-1,0),(0,-1.250064,0),(0,-1,0),(-0.382683,-0.92388,-1.36858e-007),(-0.707107,-0.707107,-2.52881e-007),(-0.92388,-0.382683,-3.30405e-007),(-1,0,-3.57628e-007),(-1.250064,0,0),(-1,0,-3.57628e-007),(-0.92388,0,0.382683),(-0.707107,0,0.707107),(-0.382684,0,0.923879),(-1.49012e-007,0,1),(0.382683,0,0.92388),(0.707107,0,0.707107),(0.92388,0,0.382683),(1,0,0),(0.92388,0,-0.382683),(0.707107,0,-0.707106),(0.382684,0,-0.923879),(5.06639e-007,0,-1),(-0.382683,0,-0.92388),(-0.707106,0,-0.707107),(-0.923879,0,-0.382684),(-1,0,-3.57628e-007),(-0.92388,0.382683,-3.30405e-007),(-0.707107,0.707107,-2.52881e-007),(-0.382683,0.92388,-1.36858e-007),(0,1,0)] ,
                'cylinder'      : [(-2.98023e-008,0.5,1),(0.309017,0.5,0.951057),(0.587785,0.5,0.809017),(0.809017,0.5,0.587785),(0.951057,0.5,0.309017),(1,0.5,0),(0.951057,0.5,-0.309017),(0.809018,0.5,-0.587786),(0.587786,0.5,-0.809017),(0.309017,0.5,-0.951057),(0,0.5,-1),(-0.309017,0.5,-0.951057),(-0.587785,0.5,-0.809017),(-0.809017,0.5,-0.587785),(-0.951057,0.5,-0.309017),(-1,0.5,0),(-0.951057,0.5,0.309017),(-0.809017,0.5,0.587785),(-0.587785,0.5,0.809017),(-0.309017,0.5,0.951057),(-2.98023e-008,0.5,1),(-2.98023e-008,-0.5,1),(0.309017,-0.5,0.951057),(0.587785,-0.5,0.809017),(0.698401,-0.5,0.698401),(0.698401,0.5,0.698401),(0.698401,-0.5,0.698401),(0.809017,-0.5,0.587785),(0.951057,-0.5,0.309017),(1,-0.5,0),(1,0.5,0),(1,-0.5,0),(0.951057,-0.5,-0.309017),(0.809018,-0.5,-0.587786),(0.698402,-0.5,-0.698402),(0.698402,0.5,-0.698402),(0.698402,-0.5,-0.698402),(0.587786,-0.5,-0.809017),(0.309017,-0.5,-0.951057),(0,-0.5,-1),(0,0.5,-1),(0,-0.5,-1),(-0.309017,-0.5,-0.951057),(-0.587785,-0.5,-0.809017),(-0.698401,-0.5,-0.698401),(-0.698401,0.5,-0.698401),(-0.698401,-0.5,-0.698401),(-0.809017,-0.5,-0.587785),(-0.951057,-0.5,-0.309017),(-1,-0.5,0),(-1,0.5,0),(-1,-0.5,0),(-0.951057,-0.5,0.309017),(-0.809017,-0.5,0.587785),(-0.698401,-0.5,0.698401),(-0.698401,0.5,0.698401),(-0.698401,-0.5,0.698401),(-0.587785,-0.5,0.809017),(-0.309017,-0.5,0.951057),(-2.98023e-008,-0.5,1),(-2.98023e-008,0.5,1)] ,
                'stick'         : [(0,0,0),(0,2.403515,0),(0,2.350383,0),(0.0366249,2.348464,0),(0.0728487,2.342726,0),(0.108274,2.333234,0),(0.142513,2.32009,0),(0.175191,2.303441,0),(0.20595,2.283465,0),(0.234452,2.260385,0),(0.260385,2.234452,0),(0.283465,2.20595,0),(0.303441,2.175191,0),(0.32009,2.142513,0),(0.333234,2.108274,0),(0.342726,2.072849,0),(0.348464,2.036625,0),(0.350383,2,0),(0.348464,1.963375,0),(0.342726,1.927151,0),(0.333234,1.891726,0),(0.32009,1.857487,0),(0.303441,1.824809,0),(0.283465,1.79405,0),(0.260385,1.765548,0),(0.234452,1.739615,0),(0.20595,1.716535,0),(0.175191,1.696559,0),(0.142513,1.67991,0),(0.108274,1.666766,0),(0.0728487,1.657274,0),(0.0366249,1.651536,0),(0,1.649617,0),(-0.0366249,1.651536,0),(-0.0728487,1.657274,0),(-0.108274,1.666766,0),(-0.142513,1.67991,0),(-0.175191,1.696559,0),(-0.20595,1.716535,0),(-0.234452,1.739615,0),(-0.260385,1.765548,0),(-0.283465,1.79405,0),(-0.303441,1.824809,0),(-0.32009,1.857487,0),(-0.333234,1.891726,0),(-0.342726,1.927151,0),(-0.348464,1.963375,0),(-0.350383,2,0),(-0.403515,2,0),(0.403515,2,0),(-0.350383,2,0),(-0.348464,2.036625,0),(-0.342726,2.072849,0),(-0.333234,2.108274,0),(-0.32009,2.142513,0),(-0.303441,2.175191,0),(-0.283465,2.20595,0),(-0.260385,2.234452,0),(-0.234452,2.260385,0),(-0.20595,2.283465,0),(-0.175191,2.303441,0),(-0.142513,2.32009,0),(-0.108274,2.333234,0),(-0.0728487,2.342726,0),(-0.0366249,2.348464,0),(0,2.350383,0)] ,
                'stickSquare'   : [(0,0,0),(0,1.65,0),(-0.35,1.65,0),(-0.35,2.35,0),(0.35,2.35,0),(0.35,1.65,0),(0,1.65,0)] ,
                'stickCircle'   : [(0,0,0),(0,1.649617,0),(-0.0366249,1.651536,0),(-0.0728487,1.657274,0),(-0.108274,1.666766,0),(-0.142513,1.67991,0),(-0.175191,1.696559,0),(-0.20595,1.716535,0),(-0.234452,1.739615,0),(-0.260385,1.765548,0),(-0.283465,1.79405,0),(-0.303441,1.824809,0),(-0.32009,1.857487,0),(-0.333234,1.891726,0),(-0.342726,1.927151,0),(-0.348464,1.963375,0),(-0.350383,2,0),(-0.348464,2.036625,0),(-0.342726,2.072849,0),(-0.333234,2.108274,0),(-0.32009,2.142513,0),(-0.303441,2.175191,0),(-0.283465,2.20595,0),(-0.260385,2.234452,0),(-0.234452,2.260385,0),(-0.20595,2.283465,0),(-0.175191,2.303441,0),(-0.142513,2.32009,0),(-0.108274,2.333234,0),(-0.0728487,2.342726,0),(-0.0366249,2.348464,0),(0,2.350383,0),(0.0366249,2.348464,0),(0.0728487,2.342726,0),(0.108274,2.333234,0),(0.142513,2.32009,0),(0.175191,2.303441,0),(0.20595,2.283465,0),(0.234452,2.260385,0),(0.260385,2.234452,0),(0.283465,2.20595,0),(0.303441,2.175191,0),(0.32009,2.142513,0),(0.333234,2.108274,0),(0.342726,2.072849,0),(0.348464,2.036625,0),(0.350383,2,0),(0.348464,1.963375,0),(0.342726,1.927151,0),(0.333234,1.891726,0),(0.32009,1.857487,0),(0.303441,1.824809,0),(0.283465,1.79405,0),(0.260385,1.765548,0),(0.234452,1.739615,0),(0.20595,1.716535,0),(0.175191,1.696559,0),(0.142513,1.67991,0),(0.108274,1.666766,0),(0.0728487,1.657274,0),(0.0366249,1.651536,0),(0,1.649617,0)] ,
                'circle'        : [(-1.004121,0,0),(-0.991758,0,-0.157079),(-0.954976,0,-0.31029),(-0.894678,0,-0.455861),(-0.812351,0,-0.590207),(-0.710021,0,-0.710021),(-0.590207,0,-0.812351),(-0.455861,0,-0.894678),(-0.31029,0,-0.954976),(-0.157079,0,-0.991758),(0,0,-1.004121),(0,0,-1.277012),(0,0,-1.004121),(0.157079,0,-0.991758),(0.31029,0,-0.954976),(0.455861,0,-0.894678),(0.590207,0,-0.812351),(0.710021,0,-0.710021),(0.812351,0,-0.590207),(0.894678,0,-0.455861),(0.954976,0,-0.31029),(0.991758,0,-0.157079),(1.004121,0,0),(1.277012,0,0),(1.004121,0,0),(0.991758,0,0.157079),(0.954976,0,0.31029),(0.894678,0,0.455861),(0.812351,0,0.590207),(0.710021,0,0.710021),(0.590207,0,0.812351),(0.455861,0,0.894678),(0.31029,0,0.954976),(0.157079,0,0.991758),(0,0,1.004121),(0,0,1.277012),(0,0,1.004121),(-0.157079,0,0.991758),(-0.31029,0,0.954976),(-0.455861,0,0.894678),(-0.590207,0,0.812351),(-0.710021,0,0.710021),(-0.812351,0,0.590207),(-0.894678,0,0.455861),(-0.954976,0,0.31029),(-0.991758,0,0.157079),(-1.004121,0,0),(-1.277012,0,0)] ,
                'arrowBall'     : [(-0.101079,1.768103,0.104009),(-0.101079,1.724487,0.527368),(-0.101079,1.651794,0.791052),(-0.354509,1.651794,0.791052),(0,1.513677,1.146509),(0.354509,1.651794,0.791052),(0.101079,1.651794,0.791052),(0.101079,1.724487,0.527368),(0.101079,1.768103,0.104009),(0.527367,1.724487,0.104009),(0.791052,1.651794,0.104009),(0.791052,1.651794,0.354509),(1.146509,1.513677,0),(0.791052,1.651794,-0.354508),(0.791052,1.651794,-0.104008),(0.527367,1.724487,-0.104008),(0.101079,1.768103,-0.104008),(0.101079,1.724487,-0.527367),(0.101079,1.651794,-0.791051),(0.354509,1.651794,-0.791051),(0,1.513677,-1.146509),(-0.354509,1.651794,-0.791051),(-0.101079,1.651794,-0.791051),(-0.101079,1.724487,-0.527367),(-0.101079,1.768103,-0.104008),(-0.527367,1.724487,-0.104008),(-0.791052,1.651794,-0.104008),(-0.791052,1.651794,-0.354508),(-1.146509,1.513677,0),(-0.791052,1.651794,0.354509),(-0.791052,1.651794,0.104009),(-0.527367,1.724487,0.104009),(-0.101079,1.768103,0.104009)] ,
                'arrowCircle'   : [(-0.407582,0,2.049652),(-0.52278,0,2.613902),(-1.045561,0,2.613902),(1.37739e-008,0,3.659463),(1.045561,0,2.613902),(0.52278,0,2.613902),(0.407582,0,2.049652),(1.161124,0,1.737523),(1.737531,0,1.161114),(2.049654,0,0.407569),(2.613902,0,0.52278),(2.613902,0,1.045561),(3.659463,0,0),(2.613902,0,-1.045561),(2.613902,0,-0.52278),(2.049648,0,-0.407595),(1.737521,0,-1.16114),(1.161091,0,-1.73752),(0.407573,0,-2.049738) ,(0.52278,0,-2.613902),(1.045561,0,-2.613902),(1.37739e-008,0,-3.659463),(-1.045561,0,-2.613902),(-0.52278,0,-2.613902),(-0.407573,0,-2.049738),(-1.161091,0,-1.73752),(-1.737521,0,-1.16114),(-2.049648,0,-0.407595),(-2.613902,0,-0.52278),(-2.613902,0,-1.045561),(-3.659463,0,0),(-2.613902,0,1.045561),(-2.613902,0,0.52278),(-2.049654,0,0.407569),(-1.737531,0,1.161114),(-1.161124,0,1.737523),(-0.407582,0,2.04965)] ,
                'pyramid'       : [(-0.999999,0.0754167,-0.999999),(-0.999999,0.0754167,0.999999),(0.999999,0.0754167,0.999999),(0.999999,0.0754167,-0.999999),(-0.999999,0.0754167,-0.999999),(-0.999999,-0.0695844,-0.999999),(-0.112596,-1,-0.112596),(-0.112596,-1,0.112596),(0.112596,-1,0.112596),(0.999999,-0.0695844,0.999999),(0.999999,0.0754167,0.999999),(0.999999,-0.0695844,0.999999),(-0.999999,-0.0695844,0.999999),(-0.999999,0.0754167,0.999999),(-0.999999,-0.0695844,0.999999),(-0.112596,-1,0.112596),(-0.999999,-0.0695844,0.999999),(-0.999999,-0.0695844,-0.999999),(0.999999,-0.0695844,-0.999999),(0.999999,0.0754167,-0.999999),(0.999999,-0.0695844,-0.999999),(0.112596,-1,-0.112596),(-0.112596,-1,-0.112596),(0.112596,-1,-0.112596),(0.112596,-1,0.112596),(0.112596,-1,-0.112596),(0.999999,-0.0695844,-0.999999),(0.999999,-0.0695844,0.999999)] ,
                'capsule'       : [(-2.011489,0,0),(-1.977023,0.261792,0),(-1.875975,0.505744,0),(-1.71523,0.71523,0),(-1.505744,0.875975,0),(-1.261792,0.977023,0),(-1,1.011489,0),(1,1.011489,0),(1.261792,0.977023,0),(1.505744,0.875975,0),(1.71523,0.71523,0),(1.875975,0.505744,0),(1.977023,0.261792,0),(2.011489,0,0),(1.977023,-0.261792,0),(1.875975,-0.505744,0),(1.71523,-0.71523,0),(1.505744,-0.875975,0),(1.261792,-0.977023,0),(1,-1.011489,0),(-1,-1.011489,0),(-1.261792,-0.977023,0),(-1.505744,-0.875975,0), (-1.71523,-0.71523,0),(-1.875975,-0.505744,0),(-1.977023,-0.261792,0),(-2.011489,0,0)] ,
                'arrowCross'    : [(-3.629392,0,-2.087399),(-3.629392,0,-1.723768),(-1.723768,0,-1.723768),(-1.723768,0,-3.629392),(-2.087399,0,-3.629392),(0,0,-5.704041),(2.087399,0,-3.629392),(1.723768,0,-3.629392),(1.723768,0,-1.723768),(3.629392,0,-1.723768),(3.629392,0,-2.087399),(5.704041,0,0),(3.629392,0,2.087399),(3.629392,0,1.723768),(1.723768,0,1.723768),(1.723768,0,3.629392),(2.087399,0,3.629392),(0,0,5.704041),(-2.087399,0,3.629392),(-1.723768,0,3.629392),(-1.723768,0,1.723768),(-3.629392,0,1.723768),(-3.629392,0,2.087399),(-5.704041,0,0),(-3.629392,0,-2.087399)] ,
                'triangle'      : [(-1,0,1),(-1,0,1),(-0.9,0,0.8),(-0.8,0,0.6),(-0.7,0,0.4),(-0.6,0,0.2),(-0.5,0,0),(-0.4,0,-0.2),(-0.3,0,-0.4),(-0.2,0,-0.6),(-0.1,0,-0.8),(0,0,-1),(0.1,0,-0.8),(0.2,0,-0.6),(0.3,0,-0.4),(0.4,0,-0.2),(0.5,0,0),(0.6,0,0.2),(0.7,0,0.4),(0.8,0,0.6),(0.9,0,0.8),(1,0,1),(0.8,0,1),(0.6,0,1),(0.4,0,1),(0.2,0,1),(0,0,1),(-0.2,0,1),(-0.4,0,1),(-0.6,0,1),(-0.8,0,1),(-1,0,1)] ,
                'drop'          : [(0,0,-2.28198),(0.585544,0,-1.569395),(1.08325,0,-1.071748),(1.413628,0,-0.588119) ,(1.531946,0,0.00057226),(1.413628,0,0.585419),(1.08325,0,1.083278),(0.585544,0,1.413621) ,(0,0,1.531949),(-0.585544,0,1.413621),(-1.08325,0,1.083277),(-1.413628,0,0.585419) ,(-1.531946,0,0.000554983),(-1.413628,0,-0.588078),(-1.08325,0,-1.071626),(-0.585544,0,-1.570833),(0,0,-2.28198)] ,
                'plus'          : [(-0.353457,0,-0.354546),(-0.353457,0,-2),(0.35461,0,-2),(0.35461,0,-0.354546),(2,0,-0.354546),(2,0,0.35233),(0.354602,0,0.35233),(0.354602,0,2.000578),(-0.353457,0,2.002733),(-0.353457,0,0.35233),(-2,0,0.35233),(-2,0,-0.354546),(-0.353457,0,-0.354546)] ,
                'daimond'       : [(0,1,0),(0,0,0.625),(0,-1,0),(0.625,0,0),(0,1,0),(0,0,-0.625),(0,-1,0),(-0.625,0,0),(0,1,0),(0.625,0,0),(0,0,-0.625),(-0.625,0,0),(0,0,0.625),(0.625,0,0)] ,
                'square'        : [(0,0,-1.12558),(0,0,-1),(-1,0,-1),(-1,0,0),(-1.12558,0,0),(-1,0,0),(-1,0,1),(0,0,1),(0,0,1.12558),(0,0,1),(1,0,1),(1,0,0),(1.12558,0,0),(1,0,0),(1,0,-1),(0,0,-1)] ,
                'cube'          : [(1,-1,1),(1,1,1),(1,1,-1),(1,-1,-1),(-1,-1,-1),(-1,1,-1),(-1,1,1),(-1,-1,1),(1,-1,1),(1,-1,-1),(-1,-1,-1),(-1,-1,1),(-1,1,1),(1,1,1),(1,1,-1),(-1,1,-1)] ,
                'arrowPoint'    : [(0,0,0),(1.34615e-08,0,3.273813),(4.03845e-08,0.307963,3.503176),(0,0,4.37423),(0.307963,0,3.503176),(1.34615e-08,0,3.273813),(-0.307963,2.6923e-08,3.503176),(0,0,4.37423),(-1.34615e-08,-0.307963,3.503176),(1.34615e-08,0,3.273813),(0.307963,0,3.503176),(4.03845e-08,0.307963,3.503176),(-0.307963,2.6923e-08,3.503176),(-1.34615e-08,-0.307963,3.503176),(0.307963,0,3.503176)] ,
                'arrow'         : [(-1,0,0),(0,0,-1),(1,0,0),(0.4,0,0),(0.4,0,1),(-0.4,0,1),(-0.4,0,0),(-1,0,0)],
                'locator'       : [(0,1,0),(0,-1,0),(0,0,0),(-1,0,0),(1,0,0),(0,0,0),(0,0,-1),(0,0,1)] ,
                'direction'     : [(-4,0,-5),(4,0,-5),(4,0,3),(0,0,6),(-4,0,3),(-4,0,-5)],
                'null'          : [(0,0,0),(0,0,0),(0,0,0)] ,
                'line'          : [(0.3,0,0),(-0.3,0,0)] ,
                'F'             : [(-1.188184,0,1.681606),(-0.788184,0,1.681606),(-0.788184,0,0.0816061),(0.811816,0,0.0816061),(0.811816,0,-0.318394),(-0.788184,0,-0.318394),(-0.788184,0,-1.418394),(1.111816,0,-1.418394),(1.111816,0,-1.818394),(-1.188184,0,-1.818394),(-1.188184,0,1.681606)] ,
                'B'             : [(-1.188184,0,-1.818394),(0.138379,0,-1.818394),(0.789161,0,-1.708238),(1.172754,0,-1.368395),(1.311816,0,-0.888708),(1.203223,0,-0.450426),(0.87588,0,-0.118394),(1.272754,0,0.188637),(1.411816,0,0.700355),(1.309473,0,1.142543),(1.05713,0,1.458168),(0.680568,0,1.625356),(0.124317,0,1.681606),(-1.188184,0,1.681606),(-1.188184,0,-1.818394),(-0.788184,0,-1.418394),(-0.0225575,0,-1.418394),(0.559473,0,-1.367613),(0.82041,0,-1.178551),(0.911816,0,-0.85902),(0.81416,0,-0.541833),(0.522754,0,-0.360582),(0.0399419,0,-0.318394),(-0.788184,0,-0.318394),(-0.788184,0,-1.418394),(-1.188184,0,-1.818394),(-1.188184,0,1.681606),(-0.788184,0,1.281606),(0.135254,0,1.281606),(0.468067,0,1.264418),(0.751661,0,1.169886),(0.938379,0,0.976136),(1.011816,0,0.681606),(0.932129,0,0.342542),(0.657911,0,0.140199),(0.0977553,0,0.0816061),(-0.788184,0,0.0816061),(-0.788184,0,1.281606),(-0.788184,0,0.0816061),(-1.188184,0,-0.103208),(-0.788184,0,-0.318394)] ,
                'L'             : [(-1.188184,0,-1.818394),(-0.788184,0,-1.818394),(-0.788184,0,1.281606),(0.911816,0,1.281606),(0.911816,0,1.681606),(-1.188184,0,1.681606),(-1.188184,0,-1.818394)] ,
                'R'             : [(-1.188184,0,-1.818394),(0.283693,0,-1.818394),(0.984473,0,-1.724644),(1.368067,0,-1.392613),(1.511816,0,-0.866832),(1.274317,0,-0.241833),(0.539942,0,0.0816061),(0.766505,0,0.281606),(1.102443,0,0.723793),(1.67588,0,1.681606),(1.219629,0,1.681606),(0.76338,0,0.950356),(0.434474,0,0.465199),(0.203224,0,0.21598),(-0.00536974,0,0.101917),(-0.258496,0,0.0816061),(-0.788184,0,0.0816061),(-0.788184,0,1.681606),(-1.188184,0,1.681606),(-1.188184,0,-1.818394),(-0.788184,0,-1.418394),(0.302443,0,-1.418394),(0.917285,0,-1.266051),(1.111816,0,-0.879332),(1.007911,0,-0.580114),(0.704787,0,-0.380894),(0.193068,0,-0.318394),(-0.788184,0,-0.318394),(-1.188184,0,-0.110398),(-0.788184,0,0.0816061),(-1.188184,0,-0.110398),(-0.788184,0,-0.318394),(-0.788184,0,-1.418394)]
            }
    
    if shape in curve.keys():
        return curve[shape]

def pos(obj):
    return pmc.xform(obj, q=True, t=True, ws=True)

def add(a, b):
    return [a[0]+b[0], a[1]+b[1], a[2]+b[2]]

def diff(a, b):
    return [a[0]-b[0], a[1]-b[1], a[2]-b[2]]

def mag(vct = (0,0,0)):
    return pow(pow(vct[0],2) + pow(vct[1],2) + pow(vct[2],2), 0.5)

def distance(a='', b=''):
    # Find distance value.
    objA = pmc.PyNode(a).getRotatePivot('world')
    objB = pmc.PyNode(b).getRotatePivot('world')
    objC = objA - objB
    return objC.length()

def abs_node(name = '', side = ''):
    # Create node absolute value.
    pow_mdv = Node.multiplyDivide('{}Pow{}Mdv'.format(name,side))
    sqrt_mdv = Node.multiplyDivide('{}Sqrt{}Mdv'.format(name,side))
    pow_mdv.pynode.op.set(3)
    sqrt_mdv.pynode.op.set(3)
    pow_mdv.pynode.i2x.set(2)
    sqrt_mdv.pynode.i2x.set(0.5)
    pow_mdv.pynode.ox >> sqrt_mdv.pynode.i1x
    return pow_mdv , sqrt_mdv

def mesh_info(mesh = ''):
    meshObj = pmc.PyNode(mesh)
    return { 'bb'          :  meshObj.getBoundingBox(), 
             'faces'       :  meshObj.numFaces(), 
             'vertice'     :  meshObj.numVertices(), 
             'centerPivot' :  meshObj.c.get(), 
             'pivot'       :  meshObj.getRotatePivot() }

def compare_boundingBox(meshA ,meshB):
    meshAObj = pmc.PyNode(meshA)
    meshBObj = pmc.PyNode(meshB)

    meshAbb = meshAObj.getBoundingBox()
    meshBbb = meshBObj.getBoundingBox()

    for i in xrange(2):
        bbA = meshAbb[i]
        bbB = meshBbb[i]
        if bbA.isEquivalent(bbB ,0.001) == False:
            return False
    return True

def match_mesh(meshA = '', meshB = ''):
    meshAInfo = mesh_info(meshA)
    meshBInfo = mesh_info(meshB)

    if compare_boundingBox(meshA ,meshB) == False:
        return False
    
    keys = ['faces', 'vertice']
    for key in keys:
        if not meshAInfo[key] == meshBInfo[key]:
            return False

    for key in ['centerPivot', 'pivot']:
        infoA = meshAInfo[key]
        infoB = meshBInfo[key]
        if infoA.isEquivalent( infoB , 0.001 ) == False :
            return False
    return True

def evaluate_uv_parameter(numJnt, offset = None):
    ''' Find value UV for Ribbon '''
    
    paramList=[]
    
    if offset == None:
        offset = (.5/numJnt)    
    
    uv = (1.0 -(offset*2))
    numJnt = numJnt
    step = uv/(numJnt-1)
    param = offset 
    
    for i in range(numJnt):
        paramList.append(param)
        param += step
    return paramList

def assign_shader_selected(obj = '', color = ''):
    matName = '{}_Mat'.format(color.capitalize())
    shader = matName + 'SG'

    colorList = { 'red'    : [1.0,0.8,0.8] ,
                  'green'  : [0.8,1.0,0.8] ,
                  'blue'   : [0.8,0.8,1.0] }

    if not pmc.objExists(shader):
        shader = pmc.shadingNode('lambert', name = matName, asShader = 1)
        shader.color.set(colorList[color])

    pmc.select(obj)
    pmc.hyperShade(assign = shader)
    pmc.select(cl = True)
    return shader

def get_shape_orig(obj = ''):
    
    if obj:
        obj = pmc.PyNode(obj)
        shps = obj.getShapes()
        
        origShpsDct = {}
        origShpsDct['used'] = []
        origShpsDct['unused'] = []

        for shp in shps:
            ins = shp.inMesh.inputs()
            outs = shp.outMesh.outputs() + shp.worldMesh.outputs()
            
            if not ins and outs:
                origShpsDct['used'] += [shp]
            elif not ins and not outs:
                origShpsDct['unused'] += [shp]

        return origShpsDct
        
def node(nodeType = '', name = ''):
    if not name : name = '{}1'.format(nodeType)
    return core.Dag(pmc.createNode( '{}'.format(nodeType) , n = name ))

def char_name(name = ''):
    grp = Node.transform('Name_Grp'.format(name))
    trnf = Node.transform('{}Name_Ctrl'.format(name))
    texts = pmc.textCurves( f = 'MS Shell Dlg 2, 27.8pt', t = name )
    cuvShp = pmc.listRelatives(texts, c = True, ad = True, s = True)
    
    i = 1
    for cuv in cuvShp:
        par = pmc.parent(cuv.getParent(), w = True)
        pmc.makeIdentity(cuv.getParent(), a = True)
        pmc.parent(cuv, trnf, r = True , s = True)
        cuv.rename('{}Text_CuvShape{}'.format(name,i))
        pmc.delete(par)
        i+=1
        
    pmc.xform(trnf, cp = True)
    trnf.pynode.t.set(-trnf.pynode.c.get())
    pmc.makeIdentity(trnf, a = True)
    pmc.delete(trnf, ch = True)
    pmc.parent(trnf, grp)
    pmc.delete(texts[0])
    trnf.lock_hide()

    trnf.set_color('black')
    trnf.add_title_attr('controls')
    trnf.add_attr('blocking', 0, 1)
    trnf.add_attr('secondary', 0, 1)
    trnf.add_attr('addRig', 0, 1)
    trnf.add_title_attr('facial')
    trnf.add_attr('facialMain', 0, 1)
    trnf.add_attr('facialDetail', 0, 1)
    trnf.add_title_attr('geometry')
    trnf.add_attr('geo', 0, 1)
    return grp

def tag_data(obj = ''):
    from ncscript2.info import info_data as info
    reload(info)
    info.attach_info(obj)

def info():
    from ncscript2.info import info_data as info
    reload(info)
    info.send()

def circle(name = '', col = '', target = '', jnt = False):
    # If not target. Control create to origin 0,0,0
    circle = core.Dag(pmc.circle(name = name, nr = (0,1,0))[0])
    circle.correct_shape_name()

    if target :
        circle.snap(target)
        circle.snap_scale(target)

    circle.set_color(col)
    pmc.delete( circle , ch = 1 )
    pmc.select( cl = True )
    return core.Controls(circle)

def controls(name = '',  shape = '', col = '', target = '', jnt = False):
    # If not target. Control create to origin 0,0,0
    curve = core.Dag(pmc.curve(name = 'transformTemplate', d = 1, p = curve_shape(shape) ))

    if jnt == True :
        ctrl = Node.joint('{}'.format(name))
        curve.parent_shape(ctrl)
        ctrl.pynode.drawStyle.set(2)
        ctrl.lock_hide('radius')
    else :
        ctrl = core.Dag(curve.pynode.rename('{}'.format(name)))
        ctrl.correct_shape_name()

    if target :
        ctrl.snap(target)

    ctrl.set_color(col)
    ctrl.lock_hide('v')

    pmc.select(cl = True)
    return core.Controls(ctrl)
        
def gimbal(ctrl):
    ctrl = core.Controls(ctrl)
    shp = core.Dag(ctrl.shape)
    gmbl = core.Dag(pmc.duplicate(ctrl.name)[0])
    gmbl.pynode.rename('{}Gmbl{}'.format(ctrl.prefix, ctrl.suffix))
    gmbl.set_color('white')
    gmbl.scale_shape(0.8)

    if not hasattr(shp.pynode, 'gimbalControl'):
        shp.add_attr('gimbalControl', 0, 1)

    ctrl.shape.gimbalControl >> gmbl.shape.v
    ctrl.pynode.rotateOrder >> gmbl.pynode.rotateOrder

    gmbl.snap(ctrl)
    gmbl.pynode.setParent(ctrl)

    gmbl.lock_hide('v')
    pmc.select(cl = True)
    return core.Dag(gmbl)
        
def cons(ctrl):
    ctrl = core.Controls(ctrl)
    shp = core.Dag(ctrl.shape)
    cons = core.Dag(pmc.duplicate(ctrl.name)[0])
    pmc.delete(cons.pynode.listRelatives(c = True, typ = 'transform'))
    cons.pynode.rename('{}Cons{}'.format(ctrl.prefix, ctrl.suffix))
    cons.set_color('black')
    cons.scale_shape(1.2)

    if not hasattr(shp.pynode ,'constraintControl'):
        shp.add_attr('constraintControl', 0, 1)

    ctrl.shape.constraintControl >> cons.shape.v
    ctrl.pynode.rotateOrder >> cons.pynode.rotateOrder

    cons.snap_point(ctrl)
    ctrl.pynode.setParent(cons)

    if ctrl.par :
        cons.pynode.setParent(ctrl.par)

    cons.lock_hide('v')
    pmc.select(cl = True)
    return core.Dag(cons)

def controls_rig(obj = '', shape = 'cube', col = 'red'):
    if obj == '':
        obj = pmc.ls(sl = True)[0]

    obj = core.Dag(obj)

    if '_' in obj.pynode.nodeName():
        prefix = obj.pynode.nodeName().split('_')[0]
        prefix = '{}'.format(prefix)
    else :
        prefix = '{}'.format(obj)
    
    side = obj.get_side()

    ctrl = controls('{}{}Ctrl'.format(prefix, side), shape, col, obj)
    ctrlZro = Node.group(ctrl)
    ctrlGmbl = gimbal(ctrl)
    ctrlCons = cons(ctrl)
    
    pmc.parentConstraint(ctrlGmbl, obj, mo = False)
    pmc.scaleConstraint(ctrlGmbl, obj, mo = False)
    
    pmc.select(cl = True)

def joint_to_curve(name = '', cuv = '', side = '', skip = 0):
    # Create joint on eps curve.
    # Skip = 0 : 1 2 3
    # Skip = 1 : 1 _ 2 _ 3
    # Skip = 3 : 1 _ _ _ 2 _ _ _ 3

    cuv = pmc.PyNode(cuv)
    eps = cuv.numEPs()

    jnt_list = []

    if side == '' :
        side = '_'
    else :
        side = '_{}_'.format(side)

    ep = 0
    numLoop = 1
    while ep <= eps :
        posi = pmc.pointPosition('{}.ep[{}]'.format(cuv, ep))
        jnt = joint('{}{}{}Jnt'.format(name, numLoop, side))
        jnt.pynode.t.set(posi[0],posi[1],posi[2])
        jnt_list.append(jnt)
        
        ep += (skip+1)
        numLoop += 1

    return jnt_list

def curve_to_joint(name = '', jnt = []):
    # Draw curve on joint
    size = len(jnt)
    position = []

    for i in range(0,size) :
        null = Node.transform()
        pmc.delete(pmc.parentConstraint(jnt[i], null, mo = False))
        pos = null.pynode.t.get()
        position.append(pos)
        pmc.delete(null)
        
    cuv = pmc.curve(n = name, ep = position)
    return cuv

def add_group(obj = '', type = 'Zro'):
    obj = core.Dag(obj)
    par = obj.pynode.getParent()
    side = obj.get_side()
    prefix = obj.name.split('_')[0]
    suffix = obj.name.split('_')[-1].capitalize()

    grp = Node.transform('{}{}{}{}Grp'.format(prefix, suffix, type, side))
    
    grp.snap(obj)
    obj.pynode.setParent(grp)

    if par:
        grp.pynode.setParent(par)

    pmc.select(cl = True)
    return grp

def reset_curve_value(cuv = ''):
    pmc.rebuildCurve(cuv, rt = 0, kcp = True, kr = 0, d = pmc.getAttr('{}.degree'.format(cuv)))

def find_nearest_point_on_curve(obj = '', cuv = ''):
    cuv = core.Dag(cuv)
    cuvShp = core.Dag(cuv.shape)
    npoc = Node.nearestPointOnCurve()

    cuvShp.pynode.worldSpace[0] >> npoc.pynode.ic
    npoc.pynode.inPosition.set(pmc.xform(obj, q = True, t = True, ws = True))
    param = npoc.pynode.parameter.get()
    pmc.delete(npoc)
    return param

def expand_curve(cuv = '', offset = 0.05):
    cuvOfsts = []
    for val in ( offset , -offset ):
        cuvOfst = pmc.offsetCurve(cuv, cb = 1, ch = False, ugn = 0, d = val)[0]
        cuvOfsts.append(cuvOfst)
    return cuvOfsts

def copy_curve_shape(src = '', trgt = '', world = True):
    crv = pmc.PyNode(trgt)
    orig_crv = pmc.PyNode(src)
    crvShp = crv.getShape()
    origCrvShp = orig_crv.getShape()
    cv = crvShp.numCVs()

    for ix in range(0, cv):
        pos = pmc.xform('{}.cv[{}]'.format(origCrvShp, str(ix)), q = True, t = True, ws = True)
        pmc.xform('{}.cv[{}]'.format(crv.nodeName(), str(ix)), t = (pos[0], pos[1], pos[2]), ws = world)

def mirror_curve_shape(obj = '', search = '', replace = '', world = True):
    crv = pmc.PyNode(obj)
    crvShp = crv.getShape()
    origCrv = crv.nodeName().replace(search, replace)
    origCrvShp = origCrv.getShape()
    cv = crvShp.numCVs()
    
    for ix in range(0, cv):
        pos = pmc.xform('{}.cv[{}]'.format(origCrvShp, str(ix)), q = True, t = True, ws = True)
        pmc.xform('{}.cv[{}]'.format(crv.nodeName(), str(ix)), t = (-pos[0], pos[1], pos[2]), ws = world)
        
def scale_gimbal_all(size = 0.9):
    gmblCtrls = pmc.ls('*Gmbl*_Ctrl')
    for each in gmblCtrls :
        ctrl = each.replace('Gmbl','')
        gmbl = core.Dag(each)
        copy_curve_shape(ctrl, gmbl)
        gmbl.scale_shape(size)

def get_influence_per_vertice(obj = '', influences = 4):
    if obj == '' :
        obj = mc.ls( sl = True )[0]
    
    obj = core.Dag(obj)

    skc = pmc.mel.eval('findRelatedSkinCluster("{}")'.format(obj))
    infs = pmc.skinCluster(skc, q = True, inf = True)
    vtcs = []
    
    for i in range(mc.polyEvaluate(obj, v = True)) :
        vtx = '{}.vtx[{}]'.format(obj, i)
        infVals = pmc.skinPercent(skc, vtx, q = True, v = True)
        array = []
        
        for j in range( len ( infVals )) :
            if infVals[j-1] > 0 :
                array.append( infVals[j-1] )
        
        if len( array ) > influences :
            vtcs.append( vtx )
    
    if vtcs :
        mc.select( vtcs , r = True )

def jnt_radius_all(r = 0.1):
    jntAll = pmc.ls('*_Jnt')
    for each in jntAll :
        if not 'Pos' in each :
            if 'Rbn' in each :
                pmc.PyNode(each).radius.set(r*1.25)
            else :
                pmc.PyNode(each).radius.set(r*1)
        else :
            pmc.PyNode(each).radius.set(r*0.5)

def enable_rotateOrder_all():
    ctrlAll = pmc.ls('*_Ctrl')
    for ctrl in ctrlAll :
        listAttrs = pmc.listAttr(ctrl, sn = True , k = True)
        if 'rx' in listAttrs or 'ry' in listAttrs or 'rz' in listAttrs :
            pmc.setAttr('{}.rotateOrder'.format(ctrl), cb = True)

def hide_all_attr_selected():
    sels = pmc.ls( sl = True )

    for sel in sels:
        attrs = []
        attrs.extend(pmc.listAttr(sel, cb = True))
        attrs.extend(pmc.listAttr(sel, k = True))
        for attr in attrs:
            try :
                pmc.setAttr(sel.attr(attr), k = False, cb = False)
            except :
                pass

def hide_selected_history_node(node = '', status = 'hide'):
    if status == 'hide':
        val = 0
    else :
        val = 1

    if not node:
        node = pmc.ls(sl=True)

    if node:
        for obj in node:
            pmc.setAttr("{}.isHistoricallyInteresting".format(obj), val)

def hide_all_history_node(status = 'hide'):
    if status == 'hide':
        val = 0
    else :
        val = 1

    nodeType = [    'parentConstraint',
                    'orientConstraint',
                    'scaleConstraint',
                    'aimConstraint',
                    'poleVectorConstraint',
                    'addDoubleLinear',
                    'angleBetween',
                    'animCurveTU',
                    'blendTwoAttr',
                    'blendColors',
                    'blendShape',
                    'clamp',
                    'cluster',
                    'condition',
                    'curveInfo',
                    'curveFromMeshEdge',
                    'closestPointOnSurface',
                    'dagPose',
                    'distanceBetween',
                    'follicle',
                    'hairSystem',
                    'joint',
                    'loft',
                    'locator',
                    'multiplyDivide',
                    'multDoubleLinear',
                    'nucleus',
                    'nearestPointOnCurve',
                    'plusMinusAverage',
                    'pointOnCurveInfo',
                    'pointOnSurfaceInfo',
                    'rebuildCurve',
                    'remapValue',
                    'reverse',
                    'skinCluster',
                    'tweak'
                ]

    for node in nodeType:
        for obj in pmc.ls(typ = node):
            pmc.setAttr("{}.isHistoricallyInteresting".format(obj), val)

def renaming_all_node():
    nodeType = [    'addDoubleLinear',
                    'angleBetween',
                    'blendTwoAttr',
                    'blendColors',
                    'blendShape',
                    'clamp',
                    'cluster',
                    'condition',
                    'curveInfo',
                    'curveFromMeshEdge',
                    'closestPointOnSurface',
                    'distanceBetween',
                    'follicle',
                    'hairSystem',
                    'loft',
                    'locator',
                    'multiplyDivide',
                    'multDoubleLinear',
                    'nucleus',
                    'nearestPointOnCurve',
                    'plusMinusAverage',
                    'pointOnCurveInfo',
                    'pointOnSurfaceInfo',
                    'rebuildCurve',
                    'remapValue',
                    'reverse',
                    'skinCluster',
                    'tweak'
                ]

    for ndType in nodeType:
        ndList = pmc.ls(type = ndType)

        if ndList:
            for nd in ndList:
                pmc.rename(nd, '{}1'.format(ndType))

def auto_orientJnt(src = '', trgt = '', objUpvec = '', aimVec = (0,1,0), upVec = (1,0,0)):
    # Auto orient joint aim to target
    if src == '' :
        src = pmc.ls(sl = True)[0]
    if trgt == '' :
        trgt = pmc.ls(sl = True)[1]

    if not objUpvec: 
        objUpvecTemp = pmc.duplicate(src, rr = True)[0]
        objUpvec = objUpvecTemp
        tmpUpChldrn = pmc.listRelatives(objUpvec, f = True)
    
        if tmpUpChldrn :
            mc.delete( tmpUpChldrn )
    else :
        objUpvecTemp = ''

    pmc.delete(pmc.aimConstraint( trgt, src, aim = aimVec, u = upVec, wut ='objectrotation', wuo = objUpvec, wu = upVec))
    pmc.makeIdentity(trgt, a = True)

    if pmc.objExists(objUpvecTemp):
        pmc.delete(objUpvecTemp)

def add_line(obj1 = '', obj2 = ''):
    obj1 = core.Dag(obj1)
    side = obj1.get_side()
    prefix = obj1.name.split('_')[0]
    suffix = obj1.name.replace(prefix, '')
    
    crv = core.Dag(pmc.curve(d = 1, p = [(0,0,0),(0,0,0)]))
    crv.pynode.rename('{}Line{}Crv'.format(prefix, side))
    crv_zro = Node.group(crv)

    clstr1 = pmc.rename(pmc.cluster('{}.cv[0]'.format(crv), wn = (obj1, obj1))[0],'{}1{}Clstr'.format(prefix, side))
    clstr2 = pmc.rename(pmc.cluster('{}.cv[1]'.format(crv), wn = (obj2, obj2))[0],'{}2{}Clstr'.format(prefix, side))

    pmc.rename(pmc.listRelatives(obj1, shapes = True)[-1],'{}1{}ClstrHandleShape'.format(prefix, side))
    pmc.rename(pmc.listRelatives(obj2, shapes = True)[-1],'{}2{}ClstrHandleShape'.format(prefix, side))

    crv.pynode.overrideEnabled.set(1)
    crv.pynode.overrideDisplayType.set(2)
    crv.pynode.inheritsTransform.set(0)

    crv.lock_hide()
    pmc.select(cl = True)
    return crv_zro

def add_ikh(name = '', type = '', stJnt = '', enJnt = ''):
    # Type = ikSplineSolver , ikSCsolver , ikRPsolver
    stJnt = core.Dag(stJnt)
    side = stJnt.get_side()
    
    if not type == 'ikSplineSolver' :
        ikh = pmc.ikHandle(name = '{}{}Ikh'.format(name, side), sol = type, sj = stJnt, ee = enJnt)
        ikhZro = Node.group(ikh[0])
        ikh[-1].rename('{}{}Eff'.format(name, side))
        return { 'ikh' : core.Dag(ikh[0]), 
                 'ikhZro' : core.Dag(ikhZro) }
    else :
        ikh = pmc.ikHandle(name = '{}{}Ikh'.format(name, side), sol = type, sj = stJnt, ee = enJnt, ns = 1)
        ikhZro = Node.group(ikh[0])
        ikh[1].rename('{}{}Eff'.format(name, side))
        ikh[2].rename('{}{}Cuv'.format(name, side))
        return { 'ikh' : core.Dag(ikh[0]), 
                 'ikhZro' : core.Dag(ikhZro), 
                 'ikhCuv' : core.Dag(ikh[2]) }
    
    pmc.select(cl = True)

def local_world(ctrl = '', worldObj = '', localObj = '', consGrp = '', type = ''):
    ctrl = core.Dag(ctrl)
    side = ctrl.get_side()
    prefix = ctrl.name.split('_')[0]
    suffix = ctrl.name.replace(prefix, '')
    
    ctrl.add_attr('localWorld', 0, 1)
    
    rev = Node.reverse('{}LocWor{}Rev'.format(prefix, side))
    space = Node.transform('{}Space{}Grp'.format(prefix, side), localObj)
    world = Node.transform('{}WorldSpace{}Grp'.format(prefix, side), localObj)
    local = Node.transform('{}LocalSpace{}Grp'.format(prefix, side), localObj)
    consNd = ''
    consWor = ''

    if type == 'orient':
        consNd = core.Dag(pmc.orientConstraint(local, world, consGrp, mo = 1))

        if worldObj:
            consWor = core.Dag(pmc.orientConstraint(worldObj, world, mo = 1))

    elif type == 'parent':
        consNd = core.Dag(pmc.parentConstraint(local, world, consGrp, mo = 1))

        if worldObj:
            consWor = core.Dag(pmc.parentConstraint(worldObj, world, mo = 1))

    ctrl.pynode.localWorld >> rev.pynode.ix
    ctrl.pynode.localWorld >> consNd.pynode.attr('{}W1'.format(world))
    rev.pynode.ox >> consNd.pynode.attr('{}W0'.format(local))
    
    pmc.parent(world, local, space)
    pmc.parent(space, localObj)
    pmc.select(cl = True)

    return { 'ctrl'        : ctrl ,
             'worldSpace'  : world ,
             'localSpace'  : local ,
             'consGrp'     : consGrp ,
             'consNd'      : consNd ,
             'consWor'     : consWor ,
             'spaceGrp'    : space ,
             'rev'         : rev }

def append_space(obj = '', spaces = [('Head','Head_Jnt'),('Pelvis','Pelvis_Jnt')]):
    ctrl = obj['ctrl']
    side = ctrl.get_side()
    prefix = ctrl.name.split('_')[0]
    suffix = ctrl.name.replace(prefix, '')

    pmc.delete(obj['consNd'],obj['rev'])

    attrDict = [('World',obj['worldSpace'])]
    enumAttr = ''

    for space in spaces :
        spaceGrp = Node.transform('{}{}Space{}Grp'.format(prefix, space[0], side))
        pmc.parentConstraint(space[1], spaceGrp, mo = False)
        spaceGrp.pynode.setParent(obj['spaceGrp'])

        attrSpace = (space[0], spaceGrp)

        attrDict.append(attrSpace)

    [obj['consGrp'].pynode.attr(attr).unlock() for attr in ('tx','ty','tz','rx','ry','rz')]
    
    pmc.addAttr(ctrl, ln = 'space', at = 'enum', en = '...', k = True)

    i = 0
    for attr in attrDict:
        enumAttr += '{}:'.format(attr[0])
        pmc.addAttr('{}.space'.format(ctrl), e = True, en = enumAttr)

        consNd = core.Dag(pmc.parentConstraint(attr[1], obj['consGrp'], mo = True))

        cond = Node.condition('{}{}Space{}Cond'.format(prefix, attr[0], side))

        ctrl.pynode.space >> cond.pynode.st
        cond.pynode.outColorR >> consNd.pynode.attr('{}W{}'.format(attr[1], i))

        cond.pynode.ft.set(i)
        cond.pynode.colorIfTrueR.set(1)
        cond.pynode.colorIfFalseR.set(0)

        i += 1

    [obj['consGrp'].pynode.attr(attr).lock() for attr in ('tx','ty','tz','rx','ry','rz')]

    if hasattr(ctrl.pynode ,'localWorld'):
        ctrl.pynode.localWorld.delete()

    pmc.select(cl = True)

'''
def get_pole_vector_position(joint_1, joint_2, joint_3, multiplier=1.0):
    # http://lesterbanks.com/2013/05/calculating-the-position-of-a-pole-vector-in-maya-using-python/
    a = joint_1.getTranslation(space="world")
    b = joint_2.getTranslation(space="world")
    c = joint_3.getTranslation(space="world")

    start_to_end = c - a
    start_to_mid = b - a

    dot = start_to_mid * start_to_end

    projection = float(dot) / float(start_to_end.length())

    start_to_end_normalized = start_to_end.normal()

    projection_vector = start_to_end_normalized * projection

    arrow_vector = start_to_mid - projection_vector
    arrow_vector *= multiplier

    pole_vector_position = arrow_vector + b

    return pole_vector_position
'''

'''
def get_polevector(start, mid, end, offsetPoleVec=2):
    x = lambda vec, loc : mc.move(vec.x, vec.y, vec.z, loc)
    pos = mc.xform(start, q=1, ws=1, t=True)
    a = om.MVector(pos[0], pos[1], pos[2])
    pos2 = mc.xform(end, q=1, ws=1, t=True)
    b= om.MVector(pos2[0], pos2[1], pos2[2])
    pos3 = mc.xform(mid, q=1, ws=1, t=True)
    c = om.MVector(pos3[0], pos3[1], pos3[2])
    i = b-a
    d= i* .5
    e = a+d
    f = c-e
    g = f *2
    h = e+g
    return (h.x, h.y, h.z)
'''

def blend_fk_ik(ctrl = '', jntFk = '', jntIk = '', jntMain = ''):
    ctrl = core.Dag(ctrl)
    side = ctrl.get_side()
    prefix = ctrl.name.split('_')[0]

    if not hasattr(ctrl.pynode ,'fkIk'):
        ctrl.add_attr('fkIk', 0, 1)

    pars = pmc.parentConstraint(jntFk, jntIk, jntMain, mo = 0)
    
    if not pmc.objExists('{}FkIk{}Rev'.format(prefix, side)):
        rev = Node.reverse('{}FkIk{}Rev'.format(prefix, side))
        ctrl.pynode.fkIk >> rev.pynode.ix
    else:
        rev = core.Dag('{}FkIk{}Rev'.format(prefix, side))
        
    ctrl.pynode.fkIk >> pars.attr('{}W1'.format(jntIk))
    rev.pynode.ox >> pars.attr('{}W0'.format(jntFk))

def add_squash(ctrl = '', jnt = '', ax = ('sx','sz')):
    jnt = core.Dag(jnt)
    ctrl = core.Dag(ctrl)
    side = ctrl.get_side()
    prefix = ctrl.name.split('_')[0]
    
    mdv = Node.multiplyDivide('{}Sqsh{}Mdv'.format(prefix, side))
    pma = Node.plusMinusAverage('{}Sqsh{}Pma'.format(prefix, side))

    ctrl.add_attr('squash')
    pma.add_attr('default')

    mdv.pynode.i2x.set(0.5)
    pma.pynode.default.set(1)

    pma.pynode.default >> pma.pynode.i1[0]
    ctrl.pynode.squash >> mdv.pynode.i1x
    mdv.pynode.ox >> pma.pynode.i1[1]
    pma.pynode.o1 >> jnt.pynode.attr('{}'.format(ax[0]))
    pma.pynode.o1 >> jnt.pynode.attr('{}'.format(ax[1]))

def add_fk_stretch(ctrl = '', trgt = '', ax = 'y', val = 1):
    ctrl = core.Dag(ctrl)
    trgt = core.Dag(trgt)
    side = ctrl.get_side()
    prefix = ctrl.name.split('_')[0]
    
    mdv = Node.multiplyDivide('{}Strt{}Mdv'.format(prefix, side))
    pma = Node.plusMinusAverage('{}Strt{}Pma'.format(prefix, side))

    ctrl.add_attr('stretch')
    pma.add_attr('default')

    mdv.pynode.i2x.set(val)
    pma.pynode.default.set(trgt.pynode.attr('t{}'.format(ax)).get())

    pma.pynode.default >> pma.pynode.i1[0]
    ctrl.pynode.stretch >> mdv.pynode.i1x
    mdv.pynode.ox >> pma.pynode.i1[1]
    pma.pynode.o1 >> trgt.pynode.attr('t{}'.format(ax))

def add_non_roll(name = '', elem = '', root = '', end = '', parent = '', axis = 'y+'):
    if axis == 'x+':
        aimVec = [1,0,0] ; upVec = [0,0,1] ; rotateOrder = 1
            
    elif axis == 'x-':
        aimVec = [-1,0,0] ; upVec = [0,0,1] ; rotateOrder = 1
    
    elif axis == 'y+':
        aimVec = [0,1,0] ; upVec = [0,0,1] ; rotateOrder = 2
        
    elif axis == 'y-':
        aimVec = [0,-1,0] ; upVec = [0,0,1] ; rotateOrder = 2
    
    elif axis == 'z+':
        aimVec = [0,0,1] ; upVec = [0,1,0] ; rotateOrder = 0
    
    elif axis == 'z-':
        aimVec = [0,0,-1] ; upVec = [0,1,0] ; rotateOrder = 0

    root = core.Dag(root)
    side = root.get_side()

    jntGrp = Node.transform('{}NonRollJnt{}{}Grp'.format(name,elem,side))
    jntGrp.snap(root)

    rootNr = Node.joint('{}NonRoll{}{}Jnt'.format(name,elem,side), root)
    rootNrZro = Node.group(rootNr)

    pmc.delete( 
                    pmc.aimConstraint( end , rootNrZro , 
                                       aim = aimVec , 
                                       u = upVec , 
                                       wut ='objectrotation' , 
                                       wuo = root , 
                                       wu = upVec )
              )
    
    endNr = Node.joint('{}NonRollEnd{}{}Jnt'.format(name,elem,side), end)
    endNr.snap_orient(rootNr)
    pmc.parent(endNr, rootNr)
    
    ikh_dicts = add_ikh('{}{}NonRoll'.format(name,elem), 'ikRPsolver', rootNr, endNr)
    ikhNr = ikh_dicts['ikh']
    ikhNrZro = ikh_dicts['ikhZro']
    
    ikhNr.pynode.poleVector.set(0,0,0)

    twistGrp = Node.transform('{}NonRollTwist{}{}Grp'.format(name,elem,side))
    twistGrp.snap(rootNr)
    twistGrp.pynode.setParent(rootNr)

    twistGrp.set_rotate_order(rotateOrder)

    pmc.parentConstraint(parent, rootNrZro, mo = True)
    pmc.pointConstraint(root, rootNr, mo = True)
    pmc.parentConstraint(parent, ikhNrZro, mo = True)
    pmc.parentConstraint(end, ikhNr, mo = True)
    pmc.orientConstraint(root, twistGrp, mo = False)
    
    pmc.parent(rootNrZro, ikhNrZro, jntGrp)
    pmc.select(cl = True)

    return { 'jntGrp' : core.Dag(jntGrp),
             'rootNr' : core.Dag(rootNr),
             'rootNrZro' : core.Dag(rootNrZro),
             'endNr' : core.Dag(endNr),
             'ikhNr' : core.Dag(ikhNr),
             'ikhNrZro' : core.Dag(ikhNrZro),
             'twistGrp' : core.Dag(twistGrp)  }

def add_ik_stretch(ctrl = '', lockCtrl = '', axis = '', type = '', *args):
    ctrl = core.Dag(ctrl)
    side = ctrl.get_side()

    if 'L' in side:
        value = 1
    elif 'R' in side:
        value = -1

    prefix_list = []
    jntDistSum = 0
    
    vtc_list = ['x','y','z']
    rgb_list = ['r','g','b']

    gmbl = core.Dag(ctrl.pynode.getChildren(typ = 'transform')[0])
    shp = core.Dag(ctrl.shape)

    distAry = []
    distSum = 0
    argsLen = range(len(args))

    for i in argsLen:
        if not i == argsLen[0]:
            dist = distance(args[i-1], args[i])
            distAry.append(dist)
            distSum += dist

        if not i == argsLen[-1]:
            arg = core.Dag(args[i])
            prefix_list.append(arg.name.split('Ik')[0])

    ctrl.add_title_attr('stretch')
    ctrl.add_attr('autoStretch', 0, 1)
    shp.add_attr('autoStrtDiv')
    shp.add_attr('autoStrtCon')

    for prefix in prefix_list:
        ctrl.add_attr('{}Stretch'.format(prefix))
    
    ikDistGrp = Node.transform('{}IkDist{}Grp'.format(type, side))
    distSt = Node.transform('{}IkDistSt{}Grp'.format(type, side))
    distEn = Node.transform('{}IkDistEn{}Grp'.format(type, side))
    pmc.parent(distSt, distEn, ikDistGrp)
    
    dist = Node.distanceBetween('{}IkAutoStrt{}Dist'.format(type, side))
    pma = Node.plusMinusAverage('{}IkAutoStrt{}Pma'.format(type, side))
    cond = Node.condition('{}IkAutoStrt{}Cnd'.format(type, side))
    bcl = Node.blendColors('{}IkAutoStrt{}Bcl'.format(type, side))
    autoMdv = Node.multiplyDivide('{}IkAutoStrt{}Mdv'.format(type, side))
    ampMdv = Node.multiplyDivide('{}IkAmpStrt{}Mdv'.format(type, side))
    strtMdv = Node.multiplyDivide('{}IkStrt{}Mdv'.format(type, side))
    strtDivMdv = Node.multiplyDivide('{}IkStrtDiv{}Mdv'.format(type, side))

    pmc.pointConstraint(args[0], distSt, mo = 0)
    pmc.pointConstraint(gmbl, distEn, mo = 0)

    distSt.pynode.t >> dist.pynode.point1
    distEn.pynode.t >> dist.pynode.point2

    dist.pynode.d >> autoMdv.pynode.i1x
    dist.pynode.d >> autoMdv.pynode.i1y
    dist.pynode.d >> autoMdv.pynode.i1z

    strtDivMdv.pynode.o >> autoMdv.pynode.i2

    dist.pynode.d >> cond.pynode.ft
    shp.pynode.autoStrtCon >> cond.pynode.st
    shp.pynode.autoStrtDiv >> strtDivMdv.pynode.i2x
    shp.pynode.autoStrtDiv >> strtDivMdv.pynode.i2y
    shp.pynode.autoStrtDiv >> strtDivMdv.pynode.i2z
    autoMdv.pynode.o >> cond.pynode.ct
    cond.pynode.oc >> strtMdv.pynode.i1
    strtMdv.pynode.o >> bcl.pynode.c1
    bcl.pynode.op >> pma.pynode.i3[0]
    ampMdv.pynode.o >> pma.pynode.i3[1]
    ctrl.pynode.autoStretch >> bcl.pynode.b

    cond.pynode.op.set(2)
    strtDivMdv.pynode.op.set(2)

    shp.pynode.autoStrtDiv.set(distSum)  # jntDistSum
    shp.pynode.autoStrtCon.set(distSum)

    strtDivMdv.pynode.i1x.set(1)
    strtDivMdv.pynode.i1y.set(1)
    strtDivMdv.pynode.i1z.set(1)

    distValue = dist.pynode.distance.get()

    for arg in args:
        if not arg == args[-1]:
            i = args.index(arg)

            ctrl.pynode.attr('{}Stretch'.format(prefix_list[i])) >> ampMdv.pynode.attr('i1{}'.format(vtc_list[i]))
            ar = core.Dag(args[i+1])
            axVal = ar.pynode.attr(axis).get()
            
            ampMdv.pynode.attr('i2{}'.format(vtc_list[i])).set(value)
            strtMdv.pynode.attr('i2{}'.format(vtc_list[i])).set(axVal)
            bcl.pynode.attr('c2{}'.format(rgb_list[i])).set(axVal)
    
    if lockCtrl :
        lockCtrl = core.Dag(lockCtrl)
        lockCtrl.add_attr('lock', 0, 1)
        
        distLock = Node.transform( '{}IkDistLock{}Grp'.format(type, side))
        mc.parent( distLock , ikDistGrp )
        
        dist1Lock = Node.distanceBetween('{}IkLock1{}Dist'.format(type, side))
        dist2Lock = Node.distanceBetween('{}IkLock2{}Dist'.format(type, side))
        mdvLock = Node.multiplyDivide('{}IkLock{}Mdv'.format(type, side))
        bclLock = Node.blendColors('{}IkLock{}Bcl'.format(type, side))

        pmc.pointConstraint(lockCtrl, distLock, mo = 0)

        distSt.pynode.t >> dist1Lock.pynode.point1
        distEn.pynode.t >> dist2Lock.pynode.point1
        distLock.pynode.t >> dist1Lock.pynode.point2
        distLock.pynode.t >> dist2Lock.pynode.point2
        dist1Lock.pynode.d >> mdvLock.pynode.i1x
        dist2Lock.pynode.d >> mdvLock.pynode.i1y
        lockCtrl.pynode.lock >> bclLock.pynode.b
        mdvLock.pynode.ox >> bclLock.pynode.c1r
        mdvLock.pynode.oy >> bclLock.pynode.c1g
        pma.pynode.o3x >> bclLock.pynode.c2r
        pma.pynode.o3y >> bclLock.pynode.c2g

        args1 = core.Dag(args[1])
        args2 = core.Dag(args[2])

        bclLock.pynode.opr >> args1.pynode.attr(axis)
        bclLock.pynode.opg >> args2.pynode.attr(axis)

        mdvLock.pynode.i2x.set(value)
        mdvLock.pynode.i2y.set(value)
    
        distLock.lock_hide()
    else :
        for i in range(0,len(args)-1):
            ar = core.Dag(args[i+1])
            pma.pynode.attr('o3{}'.format(vtc_list[i])) >> ar.pynode.attr(axis)

    distSt.lock_hide()
    distEn.lock_hide()
    shp.lock_hide('autoStrtDiv', 'autoStrtCon')
    return ikDistGrp , distSt , distEn

def add_softIk_stretch(ctrl = '', lockCtrl = '', axis = '', type = '', *args):
    ctrl = core.Dag(ctrl)
    side = ctrl.get_side()

    if 'L' in side:
        value = 1
    elif 'R' in side:
        value = -1

    vtr_list = ['x','y','z']
    prefix_list = []
    jntDistSum = 0

    gmbl = core.Dag(ctrl.pynode.getChildren(typ = 'transform')[0])
    shp = core.Dag(ctrl.shape)

    distAry = []
    distSum = 0
    argsLen = range(len(args))

    for i in argsLen:
        if not i == argsLen[0]:
            dist = distance(args[i-1], args[i])
            distAry.append(dist)
            distSum += dist

        if not i == argsLen[-1]:
            arg = core.Dag(args[i])
            prefix_list.append(arg.name.split('Ik')[0])

    ctrl.add_title_attr('stretch')
    ctrl.add_attr('softness', 0, 1)
    ctrl.add_attr('autoStretch', 0, 1)

    for prefix in prefix_list:
        ctrl.add_attr('{}Stretch'.format(prefix))

    shp.add_attr('defaultLen')
    for i in range(len(prefix_list)):
        shp.add_attr('defaultLen{}'.format(prefix_list[i]))
        shp.pynode.attr('defaultLen{}'.format(prefix_list[i])).set(distAry[i]*value)
        shp.lock_hide('defaultLen{}'.format(prefix_list[i]))

    shp.add_attr('stretchLen')
    for i in range(len(prefix_list)):
        shp.add_attr('stretchLen{}'.format(prefix_list[i]))
        shp.hide('stretchLen{}'.format(prefix_list[i]))
    
    ikDistGrp = Node.transform('{}IkDist{}Grp'.format(type,side))
    distSt = Node.transform('{}IkDistSt{}Grp'.format(type,side))
    distEn = Node.transform('{}IkDistEn{}Grp'.format(type,side))
    pmc.pointConstraint(args[0], distSt, mo = 0)
    pmc.pointConstraint(gmbl, distEn, mo = 0)
    pmc.parent(distSt ,distEn , ikDistGrp)

    defSumLenPma = Node.plusMinusAverage('{}IkDefSumLen{}Pma'.format(type,side))
    strtSumLenPma = Node.plusMinusAverage('{}IkStrtSumLen{}Pma'.format(type,side))
    strtMulMdv = Node.multiplyDivide('{}IkStrtMul{}Mdv'.format(type,side))
    strtAbsPowMdv = Node.multiplyDivide('{}IkStrtAbsPow{}Mdv'.format(type,side))
    strtAbsSqrtMdv = Node.multiplyDivide('{}IkStrtAbsSqrt{}Mdv'.format(type,side))
    strtPma = Node.plusMinusAverage('{}IkStrt{}Pma'.format(type,side))

    for i in range(len(prefix_list)):
        ctrl.pynode.attr('{}Stretch'.format(prefix_list[i])) >> strtMulMdv.pynode.attr('i1{}'.format(vtr_list[i]))
        shp.pynode.attr('defaultLen{}'.format(prefix_list[i])) >> defSumLenPma.pynode.attr('i1[{}]'.format(i))
        strtMulMdv.pynode.attr('o{}'.format(vtr_list[i])) >> strtPma.pynode.attr('i3[0].i3{}'.format(vtr_list[i]))
        shp.pynode.attr('defaultLen{}'.format(prefix_list[i])) >> strtPma.pynode.attr('i3[1].i3{}'.format(vtr_list[i]))
        strtAbsSqrtMdv.pynode.attr('o{}'.format(vtr_list[i])) >> shp.pynode.attr('stretchLen{}'.format(prefix_list[i]))
        shp.pynode.attr('stretchLen{}'.format(prefix_list[i])) >> strtSumLenPma.pynode.attr('i1[{}]'.format(i))

    strtPma.pynode.o3 >> strtAbsPowMdv.pynode.i1
    strtAbsPowMdv.pynode.o >> strtAbsSqrtMdv.pynode.i1
    defSumLenPma.pynode.o1 >> shp.pynode.defaultLen
    strtSumLenPma.pynode.o1 >> shp.pynode.stretchLen

    strtAbsPowMdv.pynode.op.set(3)
    strtAbsSqrtMdv.pynode.op.set(3)
    
    strtAbsPowMdv.pynode.i2.set(2,2,2)
    strtAbsSqrtMdv.pynode.i2.set(0.5,0.5,0.5)

    strtMulMdv.pynode.i2.set(value,value,value)

    dist = Node.distanceBetween('{}IkAutoStrt{}Dist'.format(type,side))
    autoStrtCond = Node.condition('{}IkAutoStrt{}Cnd'.format(type,side))
    zeroCond = Node.condition('{}IkZero{}Cnd'.format(type,side))
    negCond = Node.condition('{}IkNeg{}Cnd'.format(type,side))
    strtMdv = Node.multiplyDivide('{}IkStrt{}Mdv'.format(type,side))
    divMdv = Node.multiplyDivide('{}IkDiv{}Mdv'.format(type,side))
    powEMdv = Node.multiplyDivide('{}IkPowE{}Mdv'.format(type,side))
    autoMulLenMdv = Node.multiplyDivide('{}IkAutoStrtMulLen{}Mdv'.format(type,side))
    autoStrtDivOrigMdv = Node.multiplyDivide('{}IkAutoStrtDivOrig{}Mdv'.format(type,side))
    subDistPma = Node.plusMinusAverage('{}IkSubDist{}Pma'.format(type,side))
    subLenPma = Node.plusMinusAverage('{}IkSubLen{}Pma'.format(type,side))
    sumLenPma = Node.plusMinusAverage('{}IkSumLen{}Pma'.format(type,side))
    invMdl = Node.multDoubleLinear('{}IkInv{}Mdl'.format(type,side))
    multMdl = Node.multDoubleLinear('{}IkMult{}Mdl'.format(type,side))
    limCmp = Node.clamp('{}IkLim{}Cmp'.format(type,side))
    bta = Node.blendTwoAttr('{}IkAutoStrt{}Bta'.format(type,side))

    distSt.pynode.t >> dist.pynode.point1
    distEn.pynode.t >> dist.pynode.point2

    dist.pynode.d >> zeroCond.pynode.cfr
    dist.pynode.d >> autoStrtCond.pynode.ft
    dist.pynode.d >> autoStrtDivOrigMdv.pynode.i1x
    dist.pynode.d >> subDistPma.pynode.i1[1]

    shp.pynode.stretchLen >> zeroCond.pynode.ctr
    shp.pynode.stretchLen >> subLenPma.pynode.i1[0]

    ctrl.pynode.softness >> zeroCond.pynode.ctg
    ctrl.pynode.softness >> zeroCond.pynode.ft
    ctrl.pynode.softness >> invMdl.pynode.i1
    ctrl.pynode.softness >> multMdl.pynode.i2
    ctrl.pynode.softness >> sumLenPma.pynode.i2[0].i2y

    zeroCond.pynode.ocr >> sumLenPma.pynode.i2[1].i2x
    zeroCond.pynode.ocg >> divMdv.pynode.i2x
    
    invMdl.pynode.o >> sumLenPma.pynode.i2[0].i2x

    sumLenPma.pynode.o2x >> subDistPma.pynode.i1[0]
    sumLenPma.pynode.o2y >> limCmp.pynode.mxr
    
    subDistPma.pynode.o1 >> divMdv.pynode.i1x

    divMdv.pynode.ox >> powEMdv.pynode.i2x
    divMdv.pynode.ox >> negCond.pynode.ft

    powEMdv.pynode.ox >> multMdl.pynode.i1

    multMdl.pynode.o >> negCond.pynode.ctr

    negCond.pynode.ocr >> subLenPma.pynode.i1[1]

    subLenPma.pynode.o1 >> autoStrtDivOrigMdv.pynode.i2x
    subLenPma.pynode.o1 >> autoStrtCond.pynode.st

    autoStrtDivOrigMdv.pynode.ox >> autoStrtCond.pynode.ctr

    autoStrtCond.pynode.ocr >> limCmp.pynode.ipr
    autoStrtCond.pynode.ocr >> bta.pynode.input[1]

    limCmp.pynode.opr >> bta.pynode.input[0]

    ctrl.pynode.autoStretch >> bta.pynode.ab

    bta.pynode.output >> autoMulLenMdv.pynode.i1x
    bta.pynode.output >> autoMulLenMdv.pynode.i1y
    bta.pynode.output >> autoMulLenMdv.pynode.i1z

    strtPma.pynode.o3 >> autoMulLenMdv.pynode.i2

    zeroCond.pynode.op.set(2)
    negCond.pynode.op.set(4)
    negCond.pynode.cfr.set(0)
    autoStrtCond.pynode.op.set(2)
    subLenPma.pynode.op.set(2)
    subDistPma.pynode.op.set(2)
    divMdv.pynode.op.set(2)
    powEMdv.pynode.op.set(3)
    powEMdv.pynode.i1x.set(3)
    autoStrtDivOrigMdv.pynode.op.set(2)
    invMdl.pynode.i2.set(-1)
    sumLenPma.pynode.i2[1].i2y.set(1)

    if lockCtrl :
        lockCtrl = core.Dag(lockCtrl)
        lockCtrl.add_attr('lock', 0, 1)
        
        distLock = Node.transform('{}IkDistLock{}Grp'.format(type, side))
        distLock.pynode.setParent(ikDistGrp)
        
        dist1Lock = Node.distanceBetween('{}IkLock1{}Dist'.format(type, side))
        dist2Lock = Node.distanceBetween('{}IkLock2{}Dist'.format(type, side))
        lockDivLenMdv = Node.multiplyDivide('{}IkLockDivLen{}Mdv'.format(type, side))
        lockMultLenMdv = Node.multiplyDivide('{}IkLockMultLen{}Mdv'.format(type, side))
        lockInvLenMdv = Node.multiplyDivide('{}IkLockInvLen{}Mdv'.format(type, side))
        bclLock = Node.blendColors('{}IkLock{}Bcl'.format(type, side))
        lockDivLenMdv.pynode.op.set(2)
        lockInvLenMdv.pynode.i2x.set(value)
        lockInvLenMdv.pynode.i2y.set(value)

        pmc.pointConstraint(lockCtrl ,distLock ,mo = 0)

        distSt.pynode.t >> dist1Lock.pynode.point1
        distEn.pynode.t >> dist2Lock.pynode.point1
        distLock.pynode.t >> dist1Lock.pynode.point2
        distLock.pynode.t >> dist2Lock.pynode.point2
        dist1Lock.pynode.d >> lockDivLenMdv.pynode.i1x
        dist2Lock.pynode.d >> lockDivLenMdv.pynode.i1y
        lockCtrl.pynode.attr('lock') >> bclLock.pynode.b
        lockDivLenMdv.pynode.ox >> lockMultLenMdv.pynode.i1x
        lockDivLenMdv.pynode.oy >> lockMultLenMdv.pynode.i1y
        lockMultLenMdv.pynode.ox >> lockInvLenMdv.pynode.i1x
        lockMultLenMdv.pynode.oy >> lockInvLenMdv.pynode.i1y
        lockInvLenMdv.pynode.ox >> bclLock.pynode.c1r
        lockInvLenMdv.pynode.oy >> bclLock.pynode.c1g
        autoMulLenMdv.pynode.ox >> bclLock.pynode.c2r
        autoMulLenMdv.pynode.oy >> bclLock.pynode.c2g

        args1 = core.Dag(args[1])
        args2 = core.Dag(args[2])

        bclLock.pynode.opr >> args1.pynode.attr(axis)
        bclLock.pynode.opg >> args2.pynode.attr(axis)

        for i in range(len(prefix_list)):
            shp.pynode.attr('defaultLen{}'.format(prefix_list[i])) >> lockDivLenMdv.pynode.attr('i2{}'.format(vtr_list[i]))
            shp.pynode.attr('defaultLen{}'.format(prefix_list[i])) >> lockMultLenMdv.pynode.attr('i2{}'.format(vtr_list[i]))

        distLock.lock_hide()
    else :
        for i in range(0,len(args)-1):
            ar = core.Dag(args[i+1])
            autoMulLenMdv.pynode.attr('o{}'.format(vtr_list[i])) >> ar.pynode.attr(axis)

    distSt.lock_hide()
    distEn.lock_hide()
    shp.hide('defaultLen', 'stretchLen')
    return ikDistGrp , distSt , distEn , dist