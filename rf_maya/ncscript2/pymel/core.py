import pymel.core as pmc

class PyNode(object):
    
    def __init__(self, pynode):
        self.obj = pmc.PyNode(pynode)
        self.pynode = pmc.PyNode(pynode)

    def __str__(self):
        return self.obj.name()

    def __repr__(self):
        return self.obj.__repr__()

    def name(self):
        return self.obj.name()

    name = property(name)

class Dag(PyNode):
    
    def __init__(self, pynode):
        super(Dag, self).__init__(pynode)

    def get_shape(self):
        return self.obj.getShape()

    shape = property(get_shape)
 
    def get_elem_name(self):
        if '_' in self.obj :
            nmsp = self.obj.namespace()
            if nmsp :
                self.obj = self.obj.split(nmsp)[-1]

            elem = self.obj.split('_')

            self.obj = elem[0]
            self.side = ''
            self.type = elem[-1]

            if not len(elem) == 2 :
                self.side = elem[1]

        else :
            self.side = ''
            self.type = ''

        return self.obj, self.side, self.type

    def get_side(self):
        currentSide = '_'
        sides = ['L','R','C']
        for side in sides :
            if '_{}_'.format(side) in self.obj.name() :
                currentSide = '_%s_' %side

        return currentSide
    
    def correct_shape_name(self):
        try :
            return self.shape.rename('{}Shape'.format(self.obj))
        except :
            return False

    def parent_shape(self, target = ''):
        if target :
            pmc.parent(self.shape, target, r = True, s = True)
            Dag(target).correct_shape_name()
            pmc.delete(self.obj)

    def duplicate(self, name = ''):
        dup = pmc.duplicate(self.obj, rr = True)
        if name:
            pmc.rename(dup, name)
        return dup

    def get_color(self):
        return self.shape.overrideColor.get()

    def set_color(self, color):

        colorId = { 'none'       : 0   ,
                    'black'      : 1   ,
                    'gray'       : 2   ,
                    'softGray'   : 3   ,
                    'darkRed'    : 4   ,
                    'darkBlue'   : 5   ,
                    'blue'       : 6   ,
                    'darkGreen'  : 7   ,
                    'brown'      : 11  ,
                    'red'        : 13  ,
                    'green'      : 14  ,
                    'white'      : 16  ,
                    'yellow'     : 17  ,
                    'cyan'       : 18  ,
                    'pink'       : 20  }
        
        if type(color) == type(str()):
            if color in colorId.keys():
                id = colorId[color]
            else :
                id = 0
        else :
            id = color

        shp = self.obj.getShape()
        shp.overrideEnabled.set(True)
        shp.overrideColor.set(id)

        shp.overrideEnabled.set(True)
        shp.overrideColor.set(id)

        self.obj.overrideEnabled.set(False)

        return True

    def get_rotate_order(self):
        rotateOrder = self.obj.getRotationOrder()
        return rotateOrder.index, rotateOrder.key

    def set_rotate_order(self, rotateOrder):
        dict = {'xyz':1,'yzx':2,'zxy':3,'xzy':4,'yxz':5,'zyx':6}

        if rotateOrder in dict.keys():
            index = dict[rotateOrder]
        else :
            index = rotateOrder
            
        self.obj.setRotationOrder(index, True)

    def freeze(self):
        pmc.makeIdentity(self.obj, a = True)
        pmc.select(cl = True)

    def snap(self, *args):
        pmc.delete(pmc.parentConstraint(args, self.obj, mo = False))
        pmc.select(cl = True)

    def snap_point(self, *args):
        pmc.delete(pmc.pointConstraint(args, self.obj, mo = False))
        pmc.select(cl = True)

    def snap_orient(self, *args):
        pmc.delete(pmc.orientConstraint(args, self.obj, mo = False))
        pmc.select(cl = True)
    
    def snap_scale(self, target):
        pmc.delete(pmc.scaleConstraint(target, self.obj, mo = False))
        pmc.select(cl = True)

    def snap_aim(self, target, aim = (0,1,0), upvec = (1,0,0), worldUpObj = '', worldUpVec = (0,1,0)):
        if worldUpObj :
            pmc.delete(pmc.aimConstraint(target, self.obj, mo = False, aim = aim, u = upvec, wut = 'objectrotation', wuo = worldUpObj, wu  = worldUpVec))
        else :
            pmc.delete(pmc.aimConstraint(target, self.obj, mo = False, aim = aim, u = upvec))

        pmc.select(cl = True)

    def snap_joint_orient(self, target):
        pmc.delete(pmc.orientConstraint ((target, self.obj), mo = False))
        ro = self.obj.rotate.get()
        self.obj.jointOrient.set(ro[0],ro[1],ro[2])
        self.obj.rotate.set(0,0,0)

    def snap_world_orient(self):
        worGrp = pmc.createNode('transform')
        pmc.delete(pmc.orientConstraint ((worGrp, self.obj), mo = False))
        pmc.delete(worGrp)

    def snap_pivot(self, target):
        # self.obj.setPivots(target.getRotatePivot(ws = True), ws = True)
        position = pmc.xform(target, q = True, worldSpace = True, rotatePivot = True)
        pmc.xform(self.obj, piv = [position[0], position[1], position[2]], ws = True)

    def snap_matrix(self, target):
        val = pmc.xform(target, q=True , worldSpace = True , matrix = True)
        pmc.xform(self.obj, worldSpace = True , matrix = val)

    @property
    def get_world_space(self):
        pars = self.obj.getParent()
        null = pmc.createNode('transform')
        null.setParent(pars)
        tr = null.t.get()
        ro = null.r.get()
        pmc.delete(null)

        return {'translate':tr, 'rotate':ro}

    def clear_cons(self):
        attrs = [ 'tx', 'ty', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz' ]
        
        for attr in attrs :
            pmc.setAttr('%s.%s' %(self.obj, attr), l = False)

        cons = self.obj.getChildren(type = 'constraint')
        lists = self.obj.listHistory()

        if cons :
            for con in cons :
                for list in lists :
                    if con == list :
                        try :
                            pmc.delete(con)
                        except :
                            pass

    def get_deformers(self, deformers):
        deformers = []

        if deformers :
            listDef = [ deformers ]
        else :
            listDef = [ 'skinCluster' , 'tweak' , 'cluster' ]
        
        for df in listDef :
            try :
                defNode = pmc.PyNode(self.shape.listConnections(type = df))
                defSet = defNode.listConnections(type = 'objectSet')
                
                deformers.append( defNode )
                deformers.append( defSet )
            except TypeError :
                continue
    
        return deformers

    def move_shape(self, target):
        cv = self.obj.numCVs()-1

        null1 = Dag(pmc.createNode('transform'))
        null2 = Dag(pmc.createNode('transform'))
        
        null1.snap(self.obj)
        null2.snap(target)
        null2.pynode.setParent(null1)

        tr = null2.pynode.t.get()

        curveCvs = pmc.select('{}.cv[0:{}]'.format(self.obj.nodeName(),cv))
        
        pmc.move(tr, r = True, os = True, wd = True)
        pmc.select(cl = True)
        pmc.delete(null1, null2)

    def rotate_shape(self, rx, ry, rz):
        piv = self.obj.getPivots(worldSpace = True)[0]
        cv = self.obj.numCVs()-1

        curveCvs = pmc.ls('{}.cv[0:{}]'.format(self.obj.nodeName(),cv))
        pmc.rotate(curveCvs, rx, ry, rz, r = True, os = True,  p = (piv[0], piv[1], piv[2]))
        pmc.select(cl = True)

    def scale_shape(self, val):
        piv = self.obj.c.get()
        cv = self.obj.numCVs()-1

        curveCvs = pmc.ls('{}.cv[0:{}]'.format(self.obj.nodeName(),cv))
        pmc.scale(curveCvs, val, val, val, r = True)
        pmc.select(cl = True)

    def lock(self, *args, **kwargs):
        if not args:
            args = pmc.listAttr(self.obj, sn = True, k = True)

            if kwargs:
                if kwargs['skip']:
                    for skip in kwargs['skip']:
                        args.pop(args.index(skip))

        [self.obj.attr(attr).lock() for attr in args]

    def unlock(self, *args, **kwargs):
        if not args:
            args = pmc.listAttr(self.obj, sn = True, k = True)

            if kwargs:
                if kwargs['skip']:
                    for skip in kwargs['skip']:
                        args.pop(args.index(skip))

        [self.obj.attr(attr).unlock() for attr in args]

    def hide(self, *args, **kwargs):
        if not args:
            args = pmc.listAttr(self.obj, sn = True, k = True)

            if kwargs:
                if kwargs['skip']:
                    for skip in kwargs['skip']:
                        args.pop(args.index(skip))

        [pmc.setAttr(self.obj.attr(attr), k = False, cb = False) for attr in args]
    
    def lock_hide(self, *args, **kwargs):
        try :
            self.lock(*args, **kwargs)
            self.hide(*args, **kwargs)
        except :
            pass

    def list_attr_from_channelbox(self):
        attrs = []
        attrs.extend(pmc.listAttr(self.obj, cb = True))
        attrs.extend(pmc.listAttr(self.obj, k = True))
        return attrs 

    def add_attr(self, attr = '', min = None, max = None, chanelBox = True):
        if not hasattr(self.obj, attr):
            pmc.addAttr(self.obj, ln = attr, at = 'float', k = chanelBox)
            
            if not min == None:
                pmc.addAttr('{}.{}'.format(self.obj, attr), e = True, min = int(min))
            
            if not max == None :
                pmc.addAttr('{}.{}'.format(self.obj, attr), e = True, max = int(max))

    def add_title_attr(self, attr = ''):
        if not pmc.objExists('{}.{}'.format(self.obj, attr.capitalize())):
            pmc.addAttr(self.obj, ln = attr.capitalize(), nn = '__{}'.format(attr.capitalize()), at = 'float', k = False)
            pmc.setAttr(self.obj.attr(attr.capitalize()), k = False, cb = True)
            pmc.addAttr('{}.{}'.format(self.obj, attr.capitalize()), e = True, min = 0)
            pmc.addAttr('{}.{}'.format(self.obj, attr.capitalize()), e = True, max = 1)

    def add_str_tag(self, attrName = '', str = ''):
        if not pmc.objExists('{}.{}'.format(self.obj, attrName)):
            pmc.addAttr(self.obj, ln = attrName, dt = 'string', k = False)
        self.obj.attr(attrName).set(str)

    def add_bool_tag(self, attrName = '', val = True):
        if not pmc.objExists('{}.{}'.format(self.obj, attrName)):
            pmc.addAttr(self.obj, ln = attrName, at = 'bool', k = False)
        self.obj.attr(attrName).set(val)

    def add_msg_tag(self, msg = ''):
        pmc.addAttr(msg, ln = 'inbox', dt = 'string', k = False)
        pmc.connectAttr('{}.message'.format(self.obj), '{}.inbox'.format(msg))

class Node(object):

    def aimConstraint(self, name = 'aimConstraint'):
        return Dag(pmc.createNode('aimConstraint', n = name))

    def addDoubleLinear(self, name = 'addDoubleLinear'):
        return Dag(pmc.createNode('addDoubleLinear', n = name))

    def angleBetween(self, name = 'angleBetween'):
        return Dag(pmc.createNode('angleBetween', n = name))

    def animCurveTU(self, name = 'animCurveTU'):
        return Dag(pmc.createNode('animCurveTU', n = name))

    def blendTwoAttr(self, name = 'blendTwoAttr'):
        return Dag(pmc.createNode('blendTwoAttr', n = name))

    def blendColors(self, name = 'blendColors'):
        return Dag(pmc.createNode('blendColors', n = name))

    def clamp(self, name = 'clamp'):
        return Dag(pmc.createNode('clamp', n = name))

    def condition(self, name = 'condition'):
        return Dag(pmc.createNode('condition', n = name))

    def curveInfo(self, name = 'curveInfo'):
        return Dag(pmc.createNode('curveInfo', n = name))

    def curveFromMeshEdge(self, name = 'curveFromMeshEdge'):
        return Dag(pmc.createNode('curveFromMeshEdge', n = name))

    def closestPointOnMesh(self, name = 'closestPointOnMesh'):
        return Dag(pmc.createNode('closestPointOnMesh', n = name))

    def closestPointOnSurface(self, name = 'closestPointOnSurface'):
        return Dag(pmc.createNode('closestPointOnSurface', n = name))

    def distanceBetween(self, name = 'distanceBetween'):
        return Dag(pmc.createNode('distanceBetween', n = name))

    def follicle(self, name = 'follicle', nurbSurface = ''):
        fol = Dag(pmc.createNode('follicle', n = name))
        folPar = fol.pynode.getParent()
        
        fol.pynode.outRotate >> folPar.rotate
        fol.pynode.outTranslate >> folPar.translate

        if nurbSurface :
            nrb = Dag(nurbSurface)
            nrb.shape.local >> fol.pynode.inputSurface
            nrb.shape.worldMatrix >> fol.pynode.inputWorldMatrix

        return fol

    def group(self, obj = '', type = 'Zro'):
        obj = Dag(obj)
        par = obj.pynode.getParent()
        side = obj.get_side()
        prefix = obj.name.split('_')[0]
        suffix = obj.name.split('_')[-1].capitalize()

        grp = self.transform('{}{}{}{}Grp'.format(prefix, suffix, type, side))
        
        grp.snap(obj)
        grp.snap_scale(obj)
        obj.pynode.setParent(grp)

        if par:
            grp.pynode.setParent(par)

        pmc.select(cl = True)
        return grp

    def hairSystem(self, name = 'hairSystem'):
        return Dag(pmc.createNode('hairSystem', n = name))

    def joint(self, name = 'joint', target = ''):
        if not pmc.objExists(name):
            jnt = Dag(pmc.createNode('joint', n = name))

            if target :
                jnt.snap(target)
                jnt.snap_scale(target)
                jnt.freeze()
        else:
            jnt = Dag(name)
            
        pmc.select(cl = True)
        return Controls(jnt)

    def loft(self, name = 'loft'):
        return Dag(pmc.createNode('loft', n = name))

    def locator(self, name = 'locator'):
        return Dag(pmc.createNode('locator', n = name))

    def multiplyDivide(self, name = 'multiplyDivide'):
        return Dag(pmc.createNode('multiplyDivide', n = name))

    def multDoubleLinear(self, name = 'multDoubleLinear'):
        return Dag(pmc.createNode('multDoubleLinear', n = name))

    def nucleus(self, name = 'nucleus'):
        return Dag(pmc.createNode('nucleus', n = name))

    def nearestPointOnCurve(self, name = 'nearestPointOnCurve'):
        return Dag(pmc.createNode('nearestPointOnCurve', n = name))

    def nurbsCurve(self, name = 'nurbsCurve'):
        return Dag(pmc.createNode('nurbsCurve', n = name))

    def plusMinusAverage(self, name = 'plusMinusAverage'):
        return Dag(pmc.createNode('plusMinusAverage', n = name))

    def pointOnCurveInfo(self, name = 'pointOnCurveInfo'):
        return Dag(pmc.createNode('pointOnCurveInfo', n = name))

    def pointOnSurfaceInfo(self, name = 'pointOnSurfaceInfo'):
        return Dag(pmc.createNode('pointOnSurfaceInfo', n = name))

    def rebuildCurve(self, name = 'rebuildCurve'):
        return Dag(pmc.createNode('rebuildCurve', n = name))

    def remapValue(self, name = 'remapValue'):
        return Dag(pmc.createNode('remapValue', n = name))

    def reverse(self, name = 'reverse'):
        return Dag(pmc.createNode('reverse', n = name))

    def transform(self, name = 'transform' , target = ''):
        if not pmc.objExists(name):
            transform = Dag(pmc.createNode('transform', n = name))

            if target :
                transform.snap(target)
                transform.snap_scale(target)
        else:
            transform = Dag(name)
            
        pmc.select(cl = True)
        return transform

class Controls(Dag):
    
    def __init__(self, pynode):
        super(Controls, self).__init__(pynode)
        self.par = self.obj.getParent()
        self.side = self.get_side()
        self.prefix = self.name.split('_')[0]
        self.suffix = self.name.replace( self.prefix , '' )
        self.type = self.name.split('_')[-1].capitalize()
        
    def gimbal(self):
        shp = Dag(self.shape)
        gmbl = Dag(pmc.duplicate(self.obj)[0])
        gmbl.pynode.rename('{}Gmbl{}'.format(self.prefix, self.suffix))
        gmbl.set_color('white')
        gmbl.scale_shape(0.8)

        if not hasattr(shp.pynode ,'gimbalControl'):
            shp.add_attr('gimbalControl', 0, 1)

        self.shape.gimbalControl >> gmbl.shape.v
        self.obj.rotateOrder >> gmbl.pynode.rotateOrder

        gmbl.snap(self.obj)
        gmbl.pynode.setParent(self.obj)

        pmc.select(cl = True)
        return gmbl
        
    def cons(self):
        shp = Dag(self.shape)
        cons = Dag(pmc.duplicate(self.obj)[0])
        pmc.delete(cons.pynode.listRelatives(c = True, typ = 'transform'))
        cons.pynode.rename('{}Cons{}'.format(self.prefix, self.suffix))
        cons.set_color('black')
        cons.scale_shape(1.2)

        if not hasattr(shp.pynode ,'constraintControl'):
            shp.add_attr('constraintControl', 0, 1)

        self.shape.constraintControl >> cons.shape.v
        self.obj.rotateOrder >> cons.pynode.rotateOrder

        cons.snap(self.obj)
        self.obj.setParent(cons)

        if self.par :
            cons.pynode.setParent(self.par)

        pmc.select(cl = True)
        return cons