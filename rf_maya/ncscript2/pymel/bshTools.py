import pymel.core as pmc
from ncscript2.pymel import core
from ncscript2.pymel import rigTools as rt
import re
reload(core)
reload(rt)
Node = core.Node()

# ----------------------------------------------------------------------------------------------------------------------------------------------------------------------
#
# Duplicate Blendshape
#
# description :  - Cut head geomety [ eye, eye brow, eyelash, in mouth ] 
#                - Select Group and run script "dupBlendshape()"
#
# ----------------------------------------------------------------------------------------------------------------------------------------------------------------------

class BlendShape(object):

    def __init__( self , grp = ''):

        if grp :
            self.fclGrp = core.Dag(grp)
            self.fclGrp.pynode.rename('BshRigGeo_Grp')

        if not pmc.objExists('Delete_Grp'):
            self.delGrp = Node.transform('Delete_Grp')
        else :
            self.delGrp = core.Dag('Delete_Grp')

        if not pmc.objExists('Bsh_Grp'):
            self.allBshGrp = Node.transform('Bsh_Grp')
        else :
            self.allBshGrp = core.Dag('Bsh_Grp')

        if not pmc.objExists('Text_Grp'):
            self.allTxtGrp = Node.transform('Text_Grp')
        else :
            self.allTxtGrp = core.Dag('Text_Grp')

        stillGrp = pmc.createNode('transform', name = 'BshRigStill_Grp')
        
        pmc.parent(self.fclGrp, stillGrp)
        pmc.parent(self.allBshGrp, self.allTxtGrp, self.delGrp)


class Generate(BlendShape):

    def __init__( self , grp     = '' ,
                         lite    = True ,
                         eyebrow = True ,
                         eye     = True ,
                         mouth   = True ,
                         nose    = True ,
                         head    = True ,
                         face    = True):
        
        super(Generate, self).__init__(grp)
        
        eyebrows_side =    [('EyebrowUp', False) ,
                            ('EyebrowDn', False) ,
                            ('EyebrowIn', False) ,
                            ('EyebrowOut', False) ,
                            ('EyebrowPull', False) ,
                            ('EyebrowInnerUp', False) ,
                            ('EyebrowInnerDn', False) ,
                            ('EyebrowInnerIn', False) ,
                            ('EyebrowInnerOut', False) ,
                            ('EyebrowInnerTurnF', False) ,
                            ('EyebrowInnerTurnC', False) ,
                            ('EyebrowMiddleUp', False) ,
                            ('EyebrowMiddleDn', False) ,
                            ('EyebrowMiddleIn', False) ,
                            ('EyebrowMiddleOut', False) ,
                            ('EyebrowMiddleTurnF', False) ,
                            ('EyebrowMiddleTurnC', False) ,
                            ('EyebrowOuterUp', False) ,
                            ('EyebrowOuterDn', False) ,
                            ('EyebrowOuterIn', False) ,
                            ('EyebrowOuterOut', False) ,
                            ('EyebrowOuterTurnF', False) ,
                            ('EyebrowOuterTurnC', False) ,
                            ('EyebrowTurnF', False) ,
                            ('EyebrowTurnC', False) ]
        
        lids_side =        [('LidsUprUp', False) ,
                            ('LidsUprDn', False) ,
                            ('LidsLwrUp', False) ,
                            ('LidsLwrDn', False) ,
                            ('LidsUprTurnF', False) ,
                            ('LidsUprTurnC', False) ,
                            ('LidsLwrTurnF', False) ,
                            ('LidsLwrTurnC', False) ,
                            ('LidsUprInUp', False) ,
                            ('LidsUprMidUp', False) ,
                            ('LidsUprOutUp', False) ,
                            ('LidsUprInDn', False) ,
                            ('LidsUprMidDn', False) ,
                            ('LidsUprOutDn', False) ,
                            ('LidsLwrInUp', False) ,
                            ('LidsLwrMidUp', False) ,
                            ('LidsLwrOutUp', False) ,
                            ('LidsLwrInDn', False) ,
                            ('LidsLwrMidDn', False) ,
                            ('LidsLwrOutDn', False) ,
                            ('EyeBallUp', False) ,
                            ('EyeBallDn', False) ,
                            ('EyeBallIn', False) ,
                            ('EyeBallOut', False) ,
                            ('EyeBallTurnF', False) ,
                            ('EyeBallTurnC', False) ,
                            ('SquintIn', True) ,
                            ('SquintOut', True) ]
        
        lips =             [('MouthUp', False) ,
                            ('MouthDn', False) ,
                            ('MouthLeft', False) ,
                            ('MouthRight', False) ,
                            ('MouthTurnF', False) ,
                            ('MouthTurnC', False) ,
                            ('LipsUprUp', True) ,
                            ('LipsUprDn', True) ,
                            ('LipsLwrUp', True) ,
                            ('LipsLwrDn', True) ,
                            ('LipsUprAllUp', False) ,
                            ('LipsUprAllDn', False) ,
                            ('LipsLwrAllUp', False) ,
                            ('LipsLwrAllDn', False) ,
                            ('LipsUprCurlIn', True) ,
                            ('LipsUprCurlOut', True) ,
                            ('LipsLwrCurlIn', True) ,
                            ('LipsLwrCurlOut', True) ,
                            ('MouthClench', True) ,
                            ('MouthPull', False) ,
                            ('MouthU', True) ]
        
        lips_side =        [('LipsUprUp', True) ,
                            ('LipsUprDn', True) ,
                            ('LipsLwrUp', True) ,
                            ('LipsLwrDn', True) ,
                            ('CornerUp', False) ,
                            ('CornerDn', False) ,
                            ('CornerIn', False) ,
                            ('CornerOut', False) ,
                            ('CornerInUp', False) ,
                            ('CornerInDn', False) ,
                            ('CornerOutUp', False) ,
                            ('CornerOutDn', False) ,
                            ('CheekUp', True) ,
                            ('CheekDn', True) ,
                            ('PuffIn', True) ,
                            ('PuffOut', True) ,
                            ('PuckerIn', False) ,
                            ('PuckerOut', False) ]
        
        noses =            [('NoseUp', False) ,
                            ('NoseDn', False) ,
                            ('NoseLeft', False) ,
                            ('NoseRight', False) ,
                            ('NoseStretch', False) ,
                            ('NoseSquash', False) ,
                            ('NoseSnarl', False) ]
        
        noses_side =       [('NoseUp', False) ,
                            ('NoseDn', False) ,
                            ('NoseTurnF', False) ,
                            ('NoseTurnC', False) ,
                            ('NoseTwist', False) ]
        
        heads =            [('HeadUprStretch', False) ,
                            ('HeadUprSquash', False) ,
                            ('HeadUprLeft', False) ,
                            ('HeadUprRight', False) ,
                            ('HeadUprFront', False) ,
                            ('HeadUprBack', False) ,
                            ('HeadLwrStretch', False) ,
                            ('HeadLwrSquash', False) ,
                            ('HeadLwrLeft', False) ,
                            ('HeadLwrRight', False) ,
                            ('HeadLwrFront', False) ,
                            ('HeadLwrBack', False) ,
                            ('HeadEyebrowPull', False) ]
        
        inBetween_side =   [('LidsUprDnInBtw50', False) ,
                            ('LidsLwrUpInBtw50', False) ]
        
        faceShape =        [('FaceOval', False) ,
                            ('FaceRound', False) ,
                            ('FaceSquare', False) ,
                            ('FaceLong', False) ,
                            ('FaceDaimond', False) ]
        
        faceBrows =        [('FaceBrowsA', False) ,
                            ('FaceBrowsB', False) ,
                            ('FaceBrowsC', False) ,
                            ('FaceBrowsD', False) ,
                            ('FaceBrowsE', False) ]
        
        faceEye =          [('FaceEyeA', False) ,
                            ('FaceEyeB', False) ,
                            ('FaceEyeC', False) ,
                            ('FaceEyeD', False) ,
                            ('FaceEyeE', False) ]
        
        faceNose =         [('FaceNoseA', False) ,
                            ('FaceNoseB', False) ,
                            ('FaceNoseC', False) ,
                            ('FaceNoseD', False) ,
                            ('FaceNoseE', False) ]
        
        faceMouth =        [('FaceMouthA', False) ,
                            ('FaceMouthB', False) ,
                            ('FaceMouthC', False) ,
                            ('FaceMouthD', False) ,
                            ('FaceMouthE', False) ]

        xValOffset = 0
        xValOffsetInv = 0
        #------------------------------------------------------
        #   CLEAR List Lite version
        #------------------------------------------------------
        all_list = ['eyebrows_side', 'lids_side', 'lips', 'lips_side', 'noses', 'noses_side', 'heads', 'inBetween_side', 'faceShape', 'faceBrows', 'faceEye', 'faceNose', 'faceMouth']
        buffer_list = []
        i = 0

        for bsh_list in (eyebrows_side, lids_side, lips, lips_side, noses, noses_side, heads, inBetween_side, faceShape, faceBrows, faceEye, faceNose, faceMouth):
            
            tmp_list = []
            for bsh_name , status in bsh_list:
                if lite == True:
                    if status == True:
                        tmp_list.append(bsh_name)
                else:
                    tmp_list.append(bsh_name)
            
            exec("{} = tmp_list".format(all_list[i]))
            i += 1

        #------------------------------------------------------
        #   EYEBROW Duplicate
        #------------------------------------------------------
        if eyebrow == True :
            if eyebrows_side:
                eyebrowDup = Duplicate(
                                            mesh  = self.fclGrp.name ,
                                            name  = 'Eyebrow' ,
                                            list  = eyebrows_side ,
                                            sides = ['L','R'] ,
                                            xVal  = xValOffset
                                      )

                pmc.parent(eyebrowDup.bshGrp, self.allBshGrp)
                pmc.parent(eyebrowDup.txtGrp, self.allTxtGrp)

                for ebBshNode in eyebrowDup.bshNode :
                    ebBshNode.blendshape.set(1)
                    ebBshNode.part.set('eyebrow')

                xValOffset = eyebrowDup.xVal

        #------------------------------------------------------
        #   EYE Duplicate
        #------------------------------------------------------
        if eye == True :
            if lids_side:
                eyeDup = Duplicate(
                                            mesh  = self.fclGrp.name ,
                                            name  = 'Lid' ,
                                            list  = lids_side ,
                                            sides = ['L','R'] ,
                                            xVal  = xValOffset
                                  )

                pmc.parent(eyeDup.bshGrp, self.allBshGrp)
                pmc.parent(eyeDup.txtGrp, self.allTxtGrp)

                for eyeBshNode in eyeDup.bshNode :
                    eyeBshNode.blendshape.set(1)
                    eyeBshNode.part.set('eye')

                xValOffset = eyeDup.xVal

        #------------------------------------------------------
        #   MOUTH Duplicate
        #------------------------------------------------------
        if mouth == True :
            if lips:
                mouthDup = Duplicate(
                                            mesh  = self.fclGrp.name ,
                                            name  = 'Lip' ,
                                            list  = lips ,
                                            sides = [''] ,
                                            xVal  = xValOffset
                                    )

                pmc.parent(mouthDup.bshGrp, self.allBshGrp)
                pmc.parent(mouthDup.txtGrp, self.allTxtGrp)

                for mouthBshNode in mouthDup.bshNode :
                    mouthBshNode.blendshape.set(1)
                    mouthBshNode.part.set('mouth')

                xValOffset = mouthDup.xVal

            if lips_side:
                mouthDupLR = Duplicate(
                                            mesh  = self.fclGrp.name ,
                                            name  = 'Lip' ,
                                            list  = lips_side ,
                                            sides = ['L','R'] ,
                                            xVal  = xValOffset
                                      )

                pmc.parent(mouthDupLR.bshGrp, self.allBshGrp)
                pmc.parent(mouthDupLR.txtGrp, self.allTxtGrp)

                for mouthBshLRNode in mouthDupLR.bshNode :
                    mouthBshLRNode.blendshape.set(1)
                    mouthBshLRNode.part.set('mouth')
        
                xValOffset = mouthDupLR.xVal

        #------------------------------------------------------
        #   NOSE Duplicate
        #------------------------------------------------------
        if nose == True :
            if noses:
                noseDup = Duplicate(
                                            mesh  = self.fclGrp.name ,
                                            name  = 'Nose' ,
                                            list  = noses ,
                                            sides = [''] ,
                                            xVal  = xValOffset
                                   )

                pmc.parent(noseDup.bshGrp, self.allBshGrp)
                pmc.parent(noseDup.txtGrp, self.allTxtGrp)

                for noseBshNode in noseDup.bshNode :
                    noseBshNode.blendshape.set(1)
                    noseBshNode.part.set('nose')

                xValOffset = noseDup.xVal

            if noses_side:
                noseDupLR = Duplicate(
                                            mesh  = self.fclGrp.name ,
                                            name  = 'Nose' ,
                                            list  = noses_side ,
                                            sides = ['L','R'] ,
                                            xVal  = xValOffset
                                    )

                pmc.parent(noseDupLR.bshGrp, self.allBshGrp)
                pmc.parent(noseDupLR.txtGrp, self.allTxtGrp)

                for noseBshLRNode in noseDupLR.bshNode :
                    noseBshLRNode.blendshape.set(1)
                    noseBshLRNode.part.set('nose')
        
                xValOffset = noseDupLR.xVal
    
        #------------------------------------------------------
        #   HEAD Duplicate
        #------------------------------------------------------
        if head == True :
            if heads:
                headDup = Duplicate(
                                            mesh  = self.fclGrp.name ,
                                            name  = 'Head' ,
                                            list  = heads ,
                                            sides = [''] ,
                                            xVal  = xValOffset
                                   )

                pmc.parent(headDup.bshGrp, self.allBshGrp)
                pmc.parent(headDup.txtGrp, self.allTxtGrp)

                for headBshNode in headDup.bshNode :
                    headBshNode.blendshape.set(1)
                    headBshNode.part.set('head')
        
                xValOffset = headDup.xVal

        #------------------------------------------------------
        #   EYE InBetween Duplicate
        #------------------------------------------------------
        if eye == True :
            if inBetween_side:
                eyeIbDup = Duplicate(
                                            mesh  = self.fclGrp.name ,
                                            name  = 'LidInBtw' ,
                                            list  = inBetween_side ,
                                            sides = ['L','R'] ,
                                            xVal  = xValOffset
                                   )

                pmc.parent(eyeIbDup.bshGrp, self.allBshGrp)
                pmc.parent(eyeIbDup.txtGrp, self.allTxtGrp)

                connectList = { 'LidsUprDnInBtw50_L' : 'LidsUprDn_L' ,
                                'LidsLwrUpInBtw50_L' : 'LidsLwrUp_L' ,
                                'LidsUprDnInBtw50_R' : 'LidsUprDn_R' ,
                                'LidsLwrUpInBtw50_R' : 'LidsLwrUp_R' }

                for eyeIbBshNode in eyeIbDup.bshNode :
                    eyeIbBshNode.part.set('eye')
                    eyeIbBshNode.inBetween.set(1)
                    eyeIbBshNode.inBetweenWeight.set(0.5)
                    eyeIbDup.bshNode[eyeIbDup.bshNode.index(eyeIbBshNode)].inBetweenTarget.set(connectList[eyeIbBshNode.nodeName()])
   
        #------------------------------------------------------
        #   FACE Duplicate
        #------------------------------------------------------
        if face == True :
            if faceShape:
                faceShpDup = Duplicate(
                                            mesh  = self.fclGrp.name ,
                                            name  = 'FaceShape' ,
                                            list  = faceShape ,
                                            sides = [''] ,
                                            xVal  = 0
                                      )

                pmc.parent(faceShpDup.bshGrp, self.allBshGrp)
                pmc.parent(faceShpDup.txtGrp, self.allTxtGrp)

                for faceShpBshNode in faceShpDup.bshNode :
                    faceShpBshNode.blendshape.set(1)

                pmc.setAttr('{}.tx'.format(faceShpDup.bshGrp[0]), faceShpDup.xVal*(-1))

            if faceBrows:
                faceBrowsDup = Duplicate(
                                            mesh  = self.fclGrp.name ,
                                            name  = 'FaceBrows' ,
                                            list  = faceBrows ,
                                            sides = [''] ,
                                            xVal  = faceShpDup.xVal
                                      )

                pmc.parent(faceBrowsDup.bshGrp, self.allBshGrp)
                pmc.parent(faceBrowsDup.txtGrp, self.allTxtGrp)

                for faceBrowsBshNode in faceBrowsDup.bshNode :
                    faceBrowsBshNode.blendshape.set(1)

                pmc.setAttr('{}.tx'.format(faceBrowsDup.bshGrp[0]), faceBrowsDup.xVal*(-1))

            if faceEye:
                faceEyeDup = Duplicate(
                                            mesh  = self.fclGrp.name ,
                                            name  = 'FaceEye' ,
                                            list  = faceEye ,
                                            sides = [''] ,
                                            xVal  = faceBrowsDup.xVal
                                      )

                pmc.parent(faceEyeDup.bshGrp, self.allBshGrp)
                pmc.parent(faceEyeDup.txtGrp, self.allTxtGrp)

                for faceEyeBshNode in faceEyeDup.bshNode :
                    faceEyeBshNode.blendshape.set(1)

                pmc.setAttr('{}.tx'.format(faceEyeDup.bshGrp[0]), faceEyeDup.xVal*(-1))

            if faceNose:
                faceNoseDup = Duplicate(
                                            mesh  = self.fclGrp.name ,
                                            name  = 'FaceNose' ,
                                            list  = faceNose ,
                                            sides = [''] ,
                                            xVal  = faceEyeDup.xVal
                                      )

                pmc.parent(faceNoseDup.bshGrp, self.allBshGrp)
                pmc.parent(faceNoseDup.txtGrp, self.allTxtGrp)

                for faceNoseBshNode in faceNoseDup.bshNode :
                    faceNoseBshNode.blendshape.set(1)

                pmc.setAttr('{}.tx'.format(faceNoseDup.bshGrp[0]), faceNoseDup.xVal*(-1))

            if faceMouth:
                faceMouthDup = Duplicate(
                                            mesh  = self.fclGrp.name ,
                                            name  = 'FaceMouth' ,
                                            list  = faceMouth ,
                                            sides = [''] ,
                                            xVal  = faceNoseDup.xVal
                                      )

                pmc.parent(faceMouthDup.bshGrp, self.allBshGrp)
                pmc.parent(faceMouthDup.txtGrp, self.allTxtGrp)

                for faceMouthBshNode in faceMouthDup.bshNode :
                    faceMouthBshNode.blendshape.set(1)

                pmc.setAttr('{}.tx'.format(faceMouthDup.bshGrp[0]), faceMouthDup.xVal*(-1))

        pmc.select(cl = True)


class Duplicate(object):

    def __init__( self ,  mesh  = '', 
                          name  = '', 
                          list  = '', 
                          sides = ['L','R'], 
                          xVal  = 0):

        self.bshGrp = []
        self.txtGrp = []
        self.bshNode = []

        bb = get_mesh_size(mesh)
        self.xVal = xVal

        if list :
            for side in sides :
                if side :
                    side = '_{}'.format(side)

                self.xVal += bb['xOffset']
                self.yVal = 0

                for each in list :
                    bshGrp = '{}Bsh{}_Grp'.format(name, side)
                    txtGrp = '{}Text{}_Grp'.format(name, side)

                    if not bshGrp in self.bshGrp :
                        self.bshGrp.append(bshGrp)

                    if not txtGrp in self.txtGrp :
                        self.txtGrp.append(txtGrp)

                    if not pmc.objExists(bshGrp):
                        bshGrp = Node.transform(bshGrp)

                    if not pmc.objExists(txtGrp):
                        txtGrp = Node.transform(txtGrp)
                        pmc.connectAttr('{}.v'.format(bshGrp), '{}.v'.format(txtGrp))

                    if not hasattr(core.Dag(bshGrp).pynode ,'blendshapeRoot'):
                        pmc.addAttr(bshGrp, ln = 'blendshapeRoot', at = 'bool', k = False)
                        bshGrp.pynode.blendshapeRoot.set(1)

                    dup = duplicate_with_text(mesh, each, side, self.yVal)

                    pmc.parent(dup['bshNode'], bshGrp)
                    pmc.parent(dup['txtCrv'], txtGrp)

                    self.bshNode.append(dup['bshNode'])
                    
                    self.yVal += bb['yOffset']

                pmc.select(bshGrp)
                pmc.move((self.xVal, 0, 0), r = True)

        pmc.select(cl = True)

def get_mesh_size(mesh = '', *args):
    bb = pmc.exactWorldBoundingBox(mesh)
    xOffset = float(abs(bb[0] - bb[3]) * 1.3)
    yOffset = float(abs(bb[1] - bb[4]) * 1.2)

    return { 'bb' : bb, 'yOffset' : yOffset, 'xOffset' : xOffset }

def duplicate_with_text(mesh = '', name = '', side = '', yVal = 0, *args):
    bb = get_mesh_size(mesh)

    duppedNode = pmc.duplicate(mesh, rr = True)
    bshNode = pmc.rename(duppedNode, '{}{}'.format(name, side))
    pmc.move(bshNode, (0, yVal, 0), r = True)

    add_tag_bsh_data(bshNode)

    txt = pmc.textCurves(t = bshNode)[0]
    txtCrv = pmc.rename(txt, '{}{}_Text'.format(name, side))
    pmc.xform(txtCrv, cp = True)

    pmc.connectAttr('{}.v'.format(bshNode), '{}.v'.format(txtCrv))

    objWidth = abs(bb['bb'][0] - bb['bb'][3])

    tmpLoc = pmc.spaceLocator()
    sclVal = objWidth*0.10
    pmc.move(tmpLoc, (0, bb['bb'][1]+yVal, bb['bb'][5]), r = True)
    
    pmc.delete(pmc.parentConstraint(tmpLoc, txtCrv))
    pmc.parentConstraint(bshNode, txtCrv, mo = True)
    pmc.scale(txtCrv, (sclVal, sclVal, sclVal), r = True)

    txtCrv.overrideEnabled.set(1)
    txtCrv.overrideDisplayType.set(2)
    
    pmc.delete(tmpLoc)
    pmc.select(cl = True)

    return { 'bshNode' : bshNode, 'txtCrv' : txtCrv }

def add_tag_bsh_data(obj, *args):
    pmc.addAttr(obj, ln = 'blendshape', at = 'bool', k = False)
    pmc.addAttr(obj, ln = 'inBetween', at = 'bool', k = False)
    pmc.addAttr(obj, ln = 'corrective', at = 'bool', k = False)
    pmc.addAttr(obj, ln = 'combineCorrective', at = 'bool', k = False)
    pmc.addAttr(obj, ln = 'part', dt = 'string', k = False)
    pmc.addAttr(obj, ln = 'inBetweenTarget', dt = 'string', k = False)
    pmc.addAttr(obj, ln = 'inBetweenWeight', at = 'float', k = False)
    pmc.select(cl = True)

def add_tag_assemble_data(obj, *args):
    if not pmc.objExists('{}.headPivot'.format(obj)):
        pmc.addAttr(obj, ln = 'headPivot', at = 'enum', en = "None:Upper:Middle:Lower:", k = False)
    pmc.select(cl = True)

def get_list_bsh_group(*args):
    bshNode = [ ]
    bshInBtw = [ ]

    objs = pmc.ls(dag = True, tr = True)

    for obj in objs :
        if hasattr(obj, 'blendshape'):
            if obj.blendshape.get() == 1 :
                bshNode.append(obj)

        if hasattr(obj, 'inBetween'):
            if obj.inBetween.get() == 1 :
                bshInBtw.append(obj)

    return { 'bshNode' : bshNode, 
             'bshInBtw' : bshInBtw }

def create_blendShape(bshBase = '', bsnName = 'BshRig_Bsn', *args):
    bshInfo = get_list_bsh_group()
    bshNode = bshInfo['bshNode']
    bshInBtw = bshInfo['bshInBtw']

    bsnName = pmc.blendShape(bshNode, bshBase, n = bsnName, foc = True)[0]
    for inbtw in bshInBtw :
        target = inbtw.inBetweenTarget.get()
        weight = inbtw.inBetweenWeight.get()

        blends = pmc.PyNode(bsnName)
        bshList = blends.listAliases()

        for bsh in bshList :
            if target in bsh[0]:
                index = bsh[1].split('[')[-1].split(']')[0]
                pmc.blendShape(bsnName, edit = True, ib = True, t = (bshBase, int(index), inbtw, weight))

    pmc.select(cl = True)
    return bsnName

def get_index_bsh_by_name(bshNode, targetName):
    attr = bshNode + '.w[{}]'
    weightCount = pmc.blendShape(bshNode, q=True, wc=True)
    for index in xrange(weightCount):
        if pmc.aliasAttr(attr.format(index), q=True) == targetName:
            return index
    return -1

def optimize_geo( list = {'eyebrow' : [ ] ,
                          'eye'     : [ ] ,
                          'mouth'   : [ ] ,
                          'nose'    : [ ] ,
                          'head'    : [ ] }):

    '''
    If you want to show mesh you can text in list
    But if you want to show all of them you can text 'All' in list
    Ex. 'eyebrow' : [ 'Head_Geo', 'Eyebrow_Geo', 'Eye_L_Geo' ]
    Ex. 'head'    : [ 'All' ] // Don't hide anything
    '''

    bshInfo = get_list_bsh_group()
    bshNode = bshInfo['bshNode']
    bshInBtw = bshInfo['bshInBtw']

    for bsh in (bshNode, bshInBtw):
        for grp in bsh :
            part = pmc.getAttr('{}.part'.format(grp))

            childs = pmc.listRelatives(grp, f = True, ad = True, type = 'transform')
            for child in childs :
                obj = child.split('|')[-1]
                if pmc.listRelatives(child, f = True, shapes = True):
                    if not 'All' in list[part] :
                        if not obj in list[part] :
                            pmc.setAttr('{}.v'.format(child), 0)
                        else :
                            pmc.setAttr('{}.v'.format(child), 1)
                    else :
                        pmc.setAttr('{}.v'.format(child), 1)

    pmc.select(cl = True)

def get_minMax_value(ctrl = '', attr = ''):
    valMin = 0
    valMax = 0
    
    if attr in ('tx', 'ty', 'tz', 'sx', 'sy', 'sz', 'rx', 'ry', 'rz'):
        valMin = pmc.getAttr('{}.m{}l'.format(ctrl,attr))
        valMax = pmc.getAttr('{}.x{}l'.format(ctrl,attr))
    else :
        if pmc.objExists('{}.{}'.format(ctrl,attr)):
            valMin, valMax = pmc.attributeQuery(attr, node = ctrl, r = True)

    return valMin, valMax

def one_way_bsh_connect(     driver = '',         # ctrl.attr
                             target = '',         # bshNode.bshName
                             inVal  = (0, 1),     # input min, max value
                             outVal = (0, 1),     # output min, max value
                             buffer = False       # connect pma before connect bshNode
                       ):

    #-- Info
    ctrl, attr = driver.split('.')
    bshNode, bshName = target.split('.')

    ctrl = core.Dag(ctrl)
    bshNode = core.Dag(bshNode)

    #-- Create & Connect node
    if pmc.objExists(target):
        rem = Node.remapValue('{}_Rem'.format(bshName))
        rem.pynode.imn.set(inVal[0])
        rem.pynode.imx.set(inVal[1])
        rem.pynode.omn.set(outVal[0])
        rem.pynode.omx.set(outVal[1])

        ctrl.pynode.attr(attr) >> rem.pynode.i

        if not buffer :
            rem.pynode.ov >> bshNode.pynode.attr(bshName)
            return { 'rem' : rem }
        else :
            pma = Node.plusMinusAverage('{}_Pma'.format(bshName))
            cmp = Node.clamp('{}_Cmp'.format(bshName))
            cmp.pynode.mxr.set(1)
            cmp.pynode.mnr.set(-1)
            rem.pynode.ov >> pma.pynode.i1[0]
            pma.pynode.o1 >> cmp.pynode.ipr
            cmp.pynode.opr >> bshNode.pynode.attr(bshName)
            return { 'rem' : rem, 'pma' : pma, 'cmp' : cmp }
    else :
        print ('{} Not Found.'.format(target))
        return False

    pmc.select(cl = True)

def auto_bsh_connect(    driver  = '',         # ctrl.attr
                         switch  = '',         # ctrl.attr < switch on off >
                         target  = '',         # pma.attr
                         inVal   = (0, 1),     # input min, max value
                         side    = ''
                    ):   

    #-- Info
    name = target.split('_')[0]
    ctrl, attr = driver.split('.')
    pma, index = target.split('.')
    swCtrl, swAttr = switch.split('.')

    if side :
        side = '_{}_'.format(side)
    else :
        side = '_'

    ctrl = core.Dag(ctrl)
    swCtrl = core.Dag(swCtrl)
    pma = core.Dag(pma)

    #-- Create & Connect node
    if pmc.objExists(pma):
        rem = Node.remapValue('{}Auto{}Rem'.format(name,side))

        limitMin, limitMax = get_minMax_value(ctrl, attr)

        rem.pynode.imn.set(inVal[0])
        rem.pynode.imx.set(inVal[1])

        ctrl.pynode.attr(attr) >> rem.pynode.i
        swCtrl.pynode.attr(swAttr) >> rem.pynode.omx
        rem.pynode.ov >> pma.pynode.attr(index)

        return { 'rem' : rem }

    pmc.select(cl = True)

def combine_two_bsh_connect(    oneDriver  = '',  # rem.attr (one driver)
                                twoDriver  = '',  # rem.attr (two driver)
                                oneDriven  = '',  # pma.attr (one driven)
                                twoDriven  = '',  # pma.attr (two driven)
                                target     = '',  # bshNode.bshName
                           ):
    
    #-- Info
    bshNode, bshName = target.split('.')
    oneDvr, oneDvrAttr = oneDriver.split('.')
    twoDvr, twoDvrAttr = twoDriver.split('.')
    oneDvn, oneDvnAttr = oneDriven.split('.')
    twoDvn, twoDvnAttr = twoDriven.split('.')

    bshNode = core.Dag(bshNode)
    oneDvr = core.Dag(oneDvr)
    twoDvr = core.Dag(twoDvr)
    oneDvn = core.Dag(oneDvn)
    twoDvn = core.Dag(twoDvn)

    #-- Create & Connect node
    if pmc.objExists(target):
        mdl = Node.multDoubleLinear('{}_Mdl'.format(bshName))

        oneDvr.pynode.attr(oneDvrAttr) >> mdl.pynode.i1
        twoDvr.pynode.attr(twoDvrAttr) >> mdl.pynode.i2

        mdl.pynode.o >> oneDvn.pynode.attr(oneDvnAttr)
        mdl.pynode.o >> twoDvn.pynode.attr(twoDvnAttr)
        mdl.pynode.o >> bshNode.pynode.attr(bshName)

        oneDvn.pynode.op.set(2)
        twoDvn.pynode.op.set(2)

        return { 'mdl' : mdl }

    pmc.select(cl = True)

def add_tag_corrective_blendshape(obj, driver, *args):
    pmc.addAttr(obj, ln = 'correctiveDriver', dt = 'string', k = False)
    pmc.setAttr('{}.correctiveDriver'.format(obj), driver)

def add_corrective_blendshape(    driver = '' ,      # ctrl.attr , bshNode.bshName
                                  driven = ''        # bshNode.bshName
                             ):

    #-- Info
    dvrName, dvrAttr = driver.split('.')
    dvnName, dvnAttr = driven.split('.')

    dvrName = core.Dag(dvrName)
    dvnName = core.Dag(dvnName)

    if '_' in dvnAttr :
        dvnAttr, side = dvnAttr.split('_')
        side = '_{}'.format(side)
    else:        
        side = ''

    #-- Create & Connect node
    if pmc.objExists(driven):
        rem = Node.remapValue('{}{}_Rem'.format(dvnAttr,side))
        rem.pynode.imx.set(1)

        dvrName.pynode.attr(dvrAttr) >> rem.pynode.i
        rem.pynode.ov >> dvnName.pynode.attr('{}{}'.format(dvnAttr,side))

        add_tag_corrective_blendshape('{}{}'.format(dvnAttr,side), driver)
        
        if not pmc.nodeType(dvrName) == 'blendShape':
            shape = core.Dag(dvrName.shape)
            shape.pynode.attr(dvnAttr) >> rem.pynode.imx

        return core.Dag(rem)

    pmc.select(cl = True)

def combine_corrective_blendshape(    driver = [],    # [bshNode.bshName, bshNode.bshName]
                                      driven = ''     # bshNode.bshName
                                 ):

    #-- Info
    dvrNodeA, dvrNameA = driver[0].split('.')
    dvrNodeB, dvrNameB = driver[1].split('.')
    dvnNodeC, dvnNameC = driven.split('.')

    dvrNodeA = core.Dag(dvrNodeA)
    dvrNodeB = core.Dag(dvrNodeB)
    dvnNodeC = core.Dag(dvnNodeC)

    #-- Create & Connect node
    if pmc.objExists(driven):
        mdl = Node.multDoubleLinear('{}_Mdl'.format(dvnNameC))

        dvrNodeA.pynode.attr(dvrNameA) >> mdl.pynode.i1
        dvrNodeB.pynode.attr(dvrNameB) >> mdl.pynode.i2
        mdl.pynode.o >> dvnNodeC.pynode.attr(dvnNameC)

        pmc.PyNode(dvnNameC).combineCorrective.set(1)
        add_tag_corrective_blendshape(dvnNameC, '{},{}'.format(driver[0],driver[1]))
        
        return core.Dag(mdl)
    pmc.select(cl = True)

def add_blendShape(src = '', tar = '', origin = 'local'):
    '''
    Add src shape to tar.
    '''
    bshs = find_related_blendshape(tar)

    if not bshs:
        # Create front of chain blend shape if no blend shape is connected to the target.
        add_frontOfChain_blendShape(src, tar, origin)
        # print 'a'
    else:
        append_blendShape(bshs[0], src, tar)
        # print 'b'
    return True

def add_blendShape_grp(srcGrp = '', tarGrp = ''):
    '''
    Find matching geo and Add src shape to tar. One to One.
    '''
    srcList = []
    tarList = []

    for grp in [(srcGrp,srcList), (tarGrp,tarList)]:
        chdns = pmc.listRelatives(grp[0], ad = True, typ = 'mesh')
        if not chdns : return False
        for chdn in chdns :
            chdnTr = chdn.getTransform()
            if not chdnTr in grp[1] :
                grp[1].append(chdnTr)

    for src in srcList :
        i = 0
        tars = tarList
        for tar in tars :
            match = rt.match_mesh(src, tar)
            if match :
                add_blendShape(src, tar) 
                del tars[i]
                i+=1
    return True

def find_related_blendshape(obj = ''):
    '''
    Find the blend shape nodes that connected to the shape node of given transform node.
    '''
    chdn = pmc.listRelatives(obj, ad=True)

    if not chdn: return False
    
    objSets = pmc.listConnections(chdn[0], type = 'objectSet', d = True, s = False)

    if not objSets: return False
    
    bshs = []
    
    for objSet in objSets:

        relBshs = pmc.listConnections(objSet, type = 'blendShape', s = True, d = False)

        if not relBshs: continue

        for relBsh in relBshs:
            if not relBsh in bshs:
                bshs.append(relBsh)

    return bshs

def add_frontOfChain_blendShape(src = '', tar = '', origin = 'local'):
    '''
    Create blend shape node with 'frontOfChain' option to 'tar' object.
    '''
    localName = tar.split(':')[-1]
    tmpNameList = localName.split('_')

    if len(tmpNameList) > 1:
        tmpNameList[-1] = 'Bsn'
    else:
        tmpNameList.append('Bsn')

    bsn = '_'.join(tmpNameList)
    pmc.blendShape(src, tar, frontOfChain = True, origin = origin, n = bsn)
    pmc.setAttr('{}.w[0]'.format(bsn), 1)

def append_blendShape(bsn = '', bshObj = '', targetObj = '', origin = 'local'):
    '''
    Add bshObj to bsn as the last blend shape attr.
    '''
    bshAttrs = pmc.aliasAttr(bsn, q = True)
    
    bshIdx = 0
    if bshAttrs:
        bshAttrDict = {}
        for ix in range(1, len(bshAttrs), 2):
            exp = r'([0-9]+)'
            m = re.search(exp, bshAttrs[ix])
            bshAttrDict[int(m.group(1))] = bshAttrs[ix-1]

        bshIdx = sorted(bshAttrDict.keys())[-1] + 1

    pmc.blendShape(bsn, e=True, t=(targetObj, bshIdx, bshObj, 1), origin = origin)

    pmc.setAttr('{}.weight[{}]'.format(bsn, bshIdx), 1)

def cleanUp_control_bsh(ctrl = ''):
    '''
    Clear controls and attribute unused
    '''
    ctrl = core.Dag(ctrl)
    attrCb = pmc.listAttr(ctrl, keyable = True)
    
    if attrCb:
        attrStatus = False
        for attr in attrCb:
            connect = pmc.listConnections('{}.{}'.format(ctrl,attr))
            
            if connect:
                attrStatus = True
            else:
                ctrl.lock_hide(attr)

        if not attrStatus:
            pmc.setAttr('{}.v'.format(ctrl.shape), 0)
    else:
        pmc.setAttr('{}.v'.format(ctrl.shape), 0)

def cleanUp_all_control_bsh():
    '''
    Clear All controls in scene and attribute unused
    '''
    all_ctrl = pmc.ls('*Bsh*_Ctrl')

    for ctrl in all_ctrl:
        cleanUp_control_bsh(ctrl)

def blendshape_transfer(bshGrp = '', bshNd = ''): 
    '''
    Transfer blendshape from model department. 
    Ref Bsh publish File without delete group or duplicate bsh in file
    '''
    bshGrp = core.Dag(bshGrp)
    bshNd = core.Dag(bshNd)
    nmsp = ''
    
    if ':' in bshGrp.pynode.name():
        nmsp = '{}:'.format(bshGrp.pynode.name().split(':')[0])
        
    #-- Source script copyShape
    pmc.mel.eval( 'source "O:/Pipeline/core/rf_maya/ncscript2/mel/copyShape.mel" ;')
    
    #-- Disconnect and set value to zero
    list_bsh = pmc.listAttr('{}.w'.format(bshNd), m = True)
    for bsh_attr in list_bsh:
        bshNd.pynode.disconnectAttr('{}'.format(bsh_attr))
        bshNd.pynode.attr('{}'.format(bsh_attr)).set(0)
    
    #-- Match mesh to transfer shape
    for bsh_attr in list_bsh:
        if pmc.objExists(bsh_attr):
            shapes = pmc.listRelatives(bsh_attr, ad = True, s = True)
            
            if shapes:
                for shp in shapes:
                    bshShapes = pmc.listRelatives(bshGrp, ad = True, s = True, ni = True)
                    if bshShapes:
                        for bShp in bshShapes:
                            match = rt.match_mesh(bShp.getParent(), shp.getParent())
                            if match:
                                bshNd.pynode.attr('{}'.format(bsh_attr)).set(1)
                                pmc.select(bShp.getParent(), shp.getParent())
                                pmc.mel.eval('copyShape ;')
                                bshNd.pynode.attr('{}'.format(bsh_attr)).set(0)

    pmc.select(cl = True)

class ControlsBsh(object):

    #------------------------------------------------------
    #   EYEBROW Controls Connect
    #------------------------------------------------------
    def eyebrowCtrlBsh( self , bshNode    = 'BshRig_Bsn' ,
                               ebCtrl     = 'EyebrowBsh_L_Ctrl' ,
                               ebInCtrl   = 'EyebrowInBsh_L_Ctrl' ,
                               ebMidCtrl  = 'EyebrowMidBsh_L_Ctrl' ,
                               ebOutCtrl  = 'EyebrowOutBsh_L_Ctrl' ,
                               side       = 'L'):

        #-- Eyebrow Control
        limitMin, limitMax = get_minMax_value(ebCtrl, 'tx')
        self.ebIn = one_way_bsh_connect(  
                                                        driver = '{}.tx'.format(ebCtrl),
                                                        target = '{}.EyebrowIn_{}'.format(bshNode,side),
                                                        inVal  = (0, limitMin),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                       )

        self.ebOut = one_way_bsh_connect(  
                                                        driver = '{}.tx'.format(ebCtrl),
                                                        target = '{}.EyebrowOut_{}'.format(bshNode,side),
                                                        inVal  = (0, limitMax),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                       )

        limitMin, limitMax = get_minMax_value(ebCtrl, 'ty')
        self.ebUp = one_way_bsh_connect(  
                                                        driver = '{}.ty'.format(ebCtrl),
                                                        target = '{}.EyebrowUp_{}'.format(bshNode,side),
                                                        inVal  = (0, limitMax),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                       )

        self.ebDn = one_way_bsh_connect(  
                                                        driver = '{}.ty'.format(ebCtrl),
                                                        target = '{}.EyebrowDn_{}'.format(bshNode,side),
                                                        inVal  = (0, limitMin),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                       )

        limitMin, limitMax = get_minMax_value(ebCtrl, 'tz')
        self.ebPull = one_way_bsh_connect(  
                                                        driver = '{}.tz'.format(ebCtrl),
                                                        target = '{}.EyebrowPull_{}'.format(bshNode,side),
                                                        inVal  = (0, limitMax),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                       )

        limitMin, limitMax = get_minMax_value(ebCtrl, 'rz')
        self.ebTurnF = one_way_bsh_connect(  
                                                        driver = '{}.rz'.format(ebCtrl),
                                                        target = '{}.EyebrowTurnF_{}'.format(bshNode,side),
                                                        inVal  = (0, limitMin),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                         )

        self.ebTurnC = one_way_bsh_connect(  
                                                        driver = '{}.rz'.format(ebCtrl),
                                                        target = '{}.EyebrowTurnC_{}'.format(bshNode,side),
                                                        inVal  = (0, limitMax),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                           )

        #-- EyebrowIn Control
        limitMin, limitMax = get_minMax_value(ebInCtrl, 'tx')
        self.ebInnerIn = one_way_bsh_connect(  
                                                        driver = '{}.tx'.format(ebInCtrl),
                                                        target = '{}.EyebrowInnerIn_{}'.format(bshNode,side),
                                                        inVal  = (0, limitMin),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                           )

        self.ebInnerOut = one_way_bsh_connect(  
                                                        driver = '{}.tx'.format(ebInCtrl),
                                                        target = '{}.EyebrowInnerOut_{}'.format(bshNode,side),
                                                        inVal  = (0, limitMax),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                           )

        limitMin, limitMax = get_minMax_value(ebInCtrl, 'ty')
        self.ebInnerUp = one_way_bsh_connect(  
                                                        driver = '{}.ty'.format(ebInCtrl),
                                                        target = '{}.EyebrowInnerUp_{}'.format(bshNode,side),
                                                        inVal  = (0, limitMax),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                           )

        self.ebInnerDn = one_way_bsh_connect(  
                                                        driver = '{}.ty'.format(ebInCtrl),
                                                        target = '{}.EyebrowInnerDn_{}'.format(bshNode,side),
                                                        inVal  = (0, limitMin),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                           )

        limitMin, limitMax = get_minMax_value(ebInCtrl, 'rz')
        self.ebInnerTurnF = one_way_bsh_connect(  
                                                        driver = '{}.rz'.format(ebInCtrl),
                                                        target = '{}.EyebrowInnerTurnF_{}'.format(bshNode,side),
                                                        inVal  = (0, limitMin),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                               )

        self.ebInnerTurnC = one_way_bsh_connect(  
                                                        driver = '{}.rz'.format(ebInCtrl),
                                                        target = '{}.EyebrowInnerTurnC_{}'.format(bshNode,side),
                                                        inVal  = (0, limitMax),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                               )

        #-- EyebrowMid Control
        limitMin, limitMax = get_minMax_value(ebMidCtrl, 'tx')
        self.ebMiddleIn = one_way_bsh_connect(  
                                                        driver = '{}.tx'.format(ebMidCtrl),
                                                        target = '{}.EyebrowMiddleIn_{}'.format(bshNode,side),
                                                        inVal  = (0, limitMin),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                               )

        self.ebMiddleOut = one_way_bsh_connect(  
                                                        driver = '{}.tx'.format(ebMidCtrl),
                                                        target = '{}.EyebrowMiddleOut_{}'.format(bshNode,side),
                                                        inVal  = (0, limitMax),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                               )

        limitMin, limitMax = get_minMax_value(ebMidCtrl, 'ty')
        self.ebMiddleUp = one_way_bsh_connect(  
                                                        driver = '{}.ty'.format(ebMidCtrl),
                                                        target = '{}.EyebrowMiddleUp_{}'.format(bshNode,side),
                                                        inVal  = (0, limitMax),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                               )

        self.ebMiddleDn = one_way_bsh_connect(  
                                                        driver = '{}.ty'.format(ebMidCtrl),
                                                        target = '{}.EyebrowMiddleDn_{}'.format(bshNode,side),
                                                        inVal  = (0, limitMin),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                            )

        limitMin, limitMax = get_minMax_value(ebMidCtrl, 'rz')
        self.ebMiddleTurnF = one_way_bsh_connect(  
                                                        driver = '{}.rz'.format(ebMidCtrl),
                                                        target = '{}.EyebrowMiddleTurnF_{}'.format(bshNode,side),
                                                        inVal  = (0, limitMin),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                               )

        self.ebMiddleTurnC = one_way_bsh_connect(  
                                                        driver = '{}.rz'.format(ebMidCtrl),
                                                        target = '{}.EyebrowMiddleTurnC_{}'.format(bshNode,side),
                                                        inVal  = (0, limitMax),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                               )

        #-- EyebrowOut Control
        limitMin, limitMax = get_minMax_value(ebOutCtrl, 'tx')
        self.ebOuterIn = one_way_bsh_connect(  
                                                        driver = '{}.tx'.format(ebOutCtrl),
                                                        target = '{}.EyebrowOuterIn_{}'.format(bshNode,side),
                                                        inVal  = (0, limitMin),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                           )

        self.ebOuterOut = one_way_bsh_connect(  
                                                        driver = '{}.tx'.format(ebOutCtrl),
                                                        target = '{}.EyebrowOuterOut_{}'.format(bshNode,side),
                                                        inVal  = (0, limitMax),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                           )

        limitMin, limitMax = get_minMax_value(ebOutCtrl, 'ty')
        self.ebOuterUp = one_way_bsh_connect(  
                                                        driver = '{}.ty'.format(ebOutCtrl),
                                                        target = '{}.EyebrowOuterUp_{}'.format(bshNode,side),
                                                        inVal  = (0, limitMax),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                           )

        self.ebOuterDn = one_way_bsh_connect(  
                                                        driver = '{}.ty'.format(ebOutCtrl),
                                                        target = '{}.EyebrowOuterDn_{}'.format(bshNode,side),
                                                        inVal  = (0, limitMin),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                           )

        limitMin, limitMax = get_minMax_value(ebOutCtrl, 'rz')
        self.ebOuterTurnF = one_way_bsh_connect(  
                                                        driver = '{}.rz'.format(ebOutCtrl),
                                                        target = '{}.EyebrowOuterTurnF_{}'.format(bshNode,side),
                                                        inVal  = (0, limitMin),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                               )

        self.ebOuterTurnC = one_way_bsh_connect(  
                                                        driver = '{}.rz'.format(ebOutCtrl),
                                                        target = '{}.EyebrowOuterTurnC_{}'.format(bshNode,side),
                                                        inVal  = (0, limitMax),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                               )


    #------------------------------------------------------
    #   EYE Controls Connect
    #------------------------------------------------------
    def eyeCtrlBsh( self , bshNode     = 'BshRig_Bsn' ,
                           eyeCtrl     = 'EyeLidsBsh_L_Ctrl' ,
                           eyeUprCtrl  = 'EyeLidsUprBsh_L_Ctrl' ,
                           eyeLwrCtrl  = 'EyeLidsLwrBsh_L_Ctrl' ,
                           side        = 'L'):

        #-- Eye Control
        limitMin, limitMax = get_minMax_value(eyeCtrl, 'tx')
        self.eyeBallIn = one_way_bsh_connect(  
                                                        driver = '{}.tx'.format(eyeCtrl),
                                                        target = '{}.EyeBallIn_{}'.format(bshNode,side),
                                                        inVal  = (0, limitMin),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                           )

        self.eyeBallOut = one_way_bsh_connect(  
                                                        driver = '{}.tx'.format(eyeCtrl),
                                                        target = '{}.EyeBallOut_{}'.format(bshNode,side),
                                                        inVal  = (0, limitMax),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                           )

        limitMin, limitMax = get_minMax_value(eyeCtrl, 'ty')
        self.eyeBallUp = one_way_bsh_connect(  
                                                        driver = '{}.ty'.format(eyeCtrl),
                                                        target = '{}.EyeBallUp_{}'.format(bshNode,side),
                                                        inVal  = (0, limitMax),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                            )

        self.eyeBallDn = one_way_bsh_connect(  
                                                        driver = '{}.ty'.format(eyeCtrl),
                                                        target = '{}.EyeBallDn_{}'.format(bshNode,side),
                                                        inVal  = (0, limitMin),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                           )

        limitMin, limitMax = get_minMax_value(eyeCtrl, 'rz')
        self.eyeBallTurnF = one_way_bsh_connect(  
                                                        driver = '{}.rz'.format(eyeCtrl),
                                                        target = '{}.EyeBallTurnF_{}'.format(bshNode,side),
                                                        inVal  = (0, limitMin),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                               )

        self.eyeBallTurnC = one_way_bsh_connect(  
                                                        driver = '{}.rz'.format(eyeCtrl),
                                                        target = '{}.EyeBallTurnC_{}'.format(bshNode,side),
                                                        inVal  = (0, limitMax),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                               )

        minVal, maxVal = get_minMax_value(eyeCtrl, 'SquintIn')
        self.eyeSquintIn = one_way_bsh_connect(  
                                                        driver = '{}.SquintIn'.format(eyeCtrl),
                                                        target = '{}.SquintIn_{}'.format(bshNode,side),
                                                        inVal  = (minVal, maxVal),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                              )    

        minVal, maxVal = get_minMax_value(eyeCtrl, 'SquintOut')
        self.eyeSquintOut = one_way_bsh_connect(  
                                                        driver = '{}.SquintOut'.format(eyeCtrl),
                                                        target = '{}.SquintOut_{}'.format(bshNode,side),
                                                        inVal  = (minVal, maxVal),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                               )

        #-- Eye Upper Control
        limitMin, limitMax = get_minMax_value(eyeUprCtrl, 'ty')
        self.lidsUprUp = one_way_bsh_connect(
                                                        driver = '{}.ty'.format(eyeUprCtrl),
                                                        target = '{}.LidsUprUp_{}'.format(bshNode,side),
                                                        inVal  = (0, limitMin),
                                                        outVal = (0, 1),
                                                        buffer = True 
                                           )

        self.lidsUprDn = one_way_bsh_connect(
                                                        driver = '{}.ty'.format(eyeUprCtrl),
                                                        target = '{}.LidsUprDn_{}'.format(bshNode,side),
                                                        inVal  = (0, limitMax),
                                                        outVal = (0, 1),
                                                        buffer = True 
                                           )

        minVal, maxVal = get_minMax_value(eyeUprCtrl, 'LidsInUD')
        self.lidsUprInUp = one_way_bsh_connect(
                                                        driver = '{}.LidsInUD'.format(eyeUprCtrl),
                                                        target = '{}.LidsUprInUp_{}'.format(bshNode,side),
                                                        inVal  = (0, maxVal),
                                                        outVal = (0, 1),
                                                        buffer = False
                                               )

        self.lidsUprInDn = one_way_bsh_connect(
                                                        driver = '{}.LidsInUD'.format(eyeUprCtrl),
                                                        target = '{}.LidsUprInDn_{}'.format(bshNode,side),
                                                        inVal  = (0, minVal),
                                                        outVal = (0, 1),
                                                        buffer = False
                                               )

        minVal, maxVal = get_minMax_value(eyeUprCtrl, 'LidsMidUD')
        self.lidsUprMidUp = one_way_bsh_connect(
                                                        driver = '{}.LidsMidUD'.format(eyeUprCtrl),
                                                        target = '{}.LidsUprMidUp_{}'.format(bshNode,side),
                                                        inVal  = (0, maxVal),
                                                        outVal = (0, 1),
                                                        buffer = False
                                               )

        self.lidsUprMidDn = one_way_bsh_connect(
                                                        driver = '{}.LidsMidUD'.format(eyeUprCtrl),
                                                        target = '{}.LidsUprMidDn_{}'.format(bshNode,side),
                                                        inVal  = (0, minVal),
                                                        outVal = (0, 1),
                                                        buffer = False
                                               )

        minVal, maxVal = get_minMax_value(eyeUprCtrl, 'LidsOutUD')
        self.lidsUprOutUp = one_way_bsh_connect(
                                                        driver = '{}.LidsOutUD'.format(eyeUprCtrl),
                                                        target = '{}.LidsUprOutUp_{}'.format(bshNode,side),
                                                        inVal  = (0, maxVal),
                                                        outVal = (0, 1),
                                                        buffer = False
                                               )

        self.lidsUprOutDn = one_way_bsh_connect(
                                                        driver = '{}.LidsOutUD'.format(eyeUprCtrl),
                                                        target = '{}.LidsUprOutDn_{}'.format(bshNode,side),
                                                        inVal  = (0, minVal),
                                                        outVal = (0, 1),
                                                        buffer = False
                                               )

        minVal, maxVal = get_minMax_value(eyeUprCtrl, 'LidsTurnFC')
        self.lidsUprTurnF = one_way_bsh_connect(
                                                        driver = '{}.LidsTurnFC'.format(eyeUprCtrl),
                                                        target = '{}.LidsUprTurnF_{}'.format(bshNode,side),
                                                        inVal  = (0, maxVal),
                                                        outVal = (0, 1),
                                                        buffer = False
                                               )

        self.lidsUprTurnC = one_way_bsh_connect(
                                                        driver = '{}.LidsTurnFC'.format(eyeUprCtrl),
                                                        target = '{}.LidsUprTurnC_{}'.format(bshNode,side),
                                                        inVal  = (0, minVal),
                                                        outVal = (0, 1),
                                                        buffer = False
                                               )

        #-- Eye Lower Control
        limitMin, limitMax = get_minMax_value(eyeLwrCtrl, 'ty')
        self.lidsLwrUp = one_way_bsh_connect(
                                                        driver = '{}.ty'.format(eyeLwrCtrl),
                                                        target = '{}.LidsLwrUp_{}'.format(bshNode,side),
                                                        inVal  = (0, limitMax),
                                                        outVal = (0, 1),
                                                        buffer = True 
                                           )

        self.lidsLwrDn = one_way_bsh_connect(
                                                        driver = '{}.ty'.format(eyeLwrCtrl),
                                                        target = '{}.LidsLwrDn_{}'.format(bshNode,side),
                                                        inVal  = (0, limitMin),
                                                        outVal = (0, 1),
                                                        buffer = True 
                                           )

        minVal, maxVal = get_minMax_value(eyeLwrCtrl, 'LidsInUD')
        self.lidsLwrInUp = one_way_bsh_connect(
                                                        driver = '{}.LidsInUD'.format(eyeLwrCtrl),
                                                        target = '{}.LidsLwrInUp_{}'.format(bshNode,side),
                                                        inVal  = (0, maxVal),
                                                        outVal = (0, 1),
                                                        buffer = False
                                               )

        self.lidsLwrInDn = one_way_bsh_connect(
                                                        driver = '{}.LidsInUD'.format(eyeLwrCtrl),
                                                        target = '{}.LidsLwrInDn_{}'.format(bshNode,side),
                                                        inVal  = (0, minVal),
                                                        outVal = (0, 1),
                                                        buffer = False
                                               )

        minVal, maxVal = get_minMax_value(eyeLwrCtrl, 'LidsMidUD')
        self.lidsLwrMidUp = one_way_bsh_connect(
                                                        driver = '{}.LidsMidUD'.format(eyeLwrCtrl),
                                                        target = '{}.LidsLwrMidUp_{}'.format(bshNode,side),
                                                        inVal  = (0, maxVal),
                                                        outVal = (0, 1),
                                                        buffer = False
                                               )

        self.lidsLwrMidDn = one_way_bsh_connect(
                                                        driver = '{}.LidsMidUD'.format(eyeLwrCtrl),
                                                        target = '{}.LidsLwrMidDn_{}'.format(bshNode,side),
                                                        inVal  = (0, minVal),
                                                        outVal = (0, 1),
                                                        buffer = False
                                               )

        minVal, maxVal = get_minMax_value(eyeLwrCtrl, 'LidsOutUD')
        self.lidsLwrOutUp = one_way_bsh_connect(
                                                        driver = '{}.LidsOutUD'.format(eyeLwrCtrl),
                                                        target = '{}.LidsLwrOutUp_{}'.format(bshNode,side),
                                                        inVal  = (0, maxVal),
                                                        outVal = (0, 1),
                                                        buffer = False
                                               )

        self.lidsLwrOutDn = one_way_bsh_connect(
                                                        driver = '{}.LidsOutUD'.format(eyeLwrCtrl),
                                                        target = '{}.LidsLwrOutDn_{}'.format(bshNode,side),
                                                        inVal  = (0, minVal),
                                                        outVal = (0, 1),
                                                        buffer = False
                                               )

        minVal, maxVal = get_minMax_value(eyeLwrCtrl, 'LidsTurnFC')
        self.lidsLwrTurnF = one_way_bsh_connect(
                                                        driver = '{}.LidsTurnFC'.format(eyeLwrCtrl),
                                                        target = '{}.LidsLwrTurnF_{}'.format(bshNode,side),
                                                        inVal  = (0, maxVal),
                                                        outVal = (0, 1),
                                                        buffer = False
                                               )

        self.lidsLwrTurnC = one_way_bsh_connect(
                                                        driver = '{}.LidsTurnFC'.format(eyeLwrCtrl),
                                                        target = '{}.LidsLwrTurnC_{}'.format(bshNode,side),
                                                        inVal  = (0, minVal),
                                                        outVal = (0, 1),
                                                        buffer = False
                                               )

        #-- EyeLids Follow Control
        if self.lidsUprUp:
            self.eyelidsUprUpLeftAuto = auto_bsh_connect(
                                                                driver  = 'EyeLidsBsh_{}_Jnt.rx'.format(side),
                                                                switch  = 'EyeLidsBsh_{}_Jnt.EyelidsFollow'.format(side),
                                                                target  = '{}.i1[1]'.format(self.lidsUprUp['pma']),
                                                                inVal   = (0, -90),
                                                                side    = '{}'.format(side)
                                                        )

        if self.lidsUprDn:
            self.eyelidsUprDnLeftAuto = auto_bsh_connect(
                                                                driver  = 'EyeLidsBsh_{}_Jnt.rx'.format(side),
                                                                switch  = 'EyeLidsBsh_{}_Jnt.EyelidsFollow'.format(side),
                                                                target  = '{}.i1[1]'.format(self.lidsUprDn['pma']),
                                                                inVal   = (0, 90),
                                                                side    = '{}'.format(side)
                                                        )

        if self.lidsLwrUp:
            self.eyelidsLwrUpLeftAuto = auto_bsh_connect(
                                                                driver  = 'EyeLidsBsh_{}_Jnt.rx'.format(side),
                                                                switch  = 'EyeLidsBsh_{}_Jnt.EyelidsFollow'.format(side),
                                                                target  = '{}.i1[1]'.format(self.lidsLwrUp['pma']),
                                                                inVal   = (0, -315),
                                                                side    = '{}'.format(side)
                                                        )

        if self.lidsLwrDn:
            self.eyelidsLwrDnLeftAuto = auto_bsh_connect(
                                                                driver  = 'EyeLidsBsh_{}_Jnt.rx'.format(side),
                                                                switch  = 'EyeLidsBsh_{}_Jnt.EyelidsFollow'.format(side),
                                                                target  = '{}.i1[1]'.format(self.lidsLwrDn['pma']),
                                                                inVal   = (0, 315),
                                                                side    = '{}'.format(side)
                                                        )

    #------------------------------------------------------
    #   MOUTH Controls Connect
    #------------------------------------------------------
    def mouthCtrlBsh( self , bshNode     = 'BshRig_Bsn' ,
                             mthCtrl     = 'MouthBsh_Ctrl' ,
                             mthUprCtrl  = 'MouthUprBsh_Ctrl' ,
                             mthLwrCtrl  = 'MouthLwrBsh_Ctrl' ,
                             mthLftCtrl  = 'MouthCnrBsh_L_Ctrl' ,
                             mthRgtCtrl  = 'MouthCnrBsh_R_Ctrl'):

        #-- Mouth Control
        limitMin, limitMax = get_minMax_value(mthCtrl, 'tx')
        self.mthLeft = one_way_bsh_connect(  
                                                        driver = '{}.tx'.format(mthCtrl),
                                                        target = '{}.MouthLeft'.format(bshNode),
                                                        inVal  = (0, limitMax),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                          )

        self.mthRight = one_way_bsh_connect(  
                                                        driver = '{}.tx'.format(mthCtrl),
                                                        target = '{}.MouthRight'.format(bshNode),
                                                        inVal  = (0, limitMin),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                            )

        limitMin, limitMax = get_minMax_value(mthCtrl, 'ty')
        self.mthUp = one_way_bsh_connect(  
                                                        driver = '{}.ty'.format(mthCtrl),
                                                        target = '{}.MouthUp'.format(bshNode),
                                                        inVal  = (0, limitMax),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                        )

        self.mthDn = one_way_bsh_connect(  
                                                        driver = '{}.ty'.format(mthCtrl),
                                                        target = '{}.MouthDn'.format(bshNode),
                                                        inVal  = (0, limitMin),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                        )

        limitMin, limitMax = get_minMax_value(mthCtrl, 'tz')
        self.mthPull = one_way_bsh_connect(  
                                                        driver = '{}.tz'.format(mthCtrl),
                                                        target = '{}.MouthPull'.format(bshNode),
                                                        inVal  = (limitMin, limitMax),
                                                        outVal = (-1, 1),
                                                        buffer = False 
                                          )

        limitMin, limitMax = get_minMax_value(mthCtrl, 'rz')
        self.mthTurnF = one_way_bsh_connect(  
                                                        driver = '{}.rz'.format(mthCtrl),
                                                        target = '{}.MouthTurnF'.format(bshNode),
                                                        inVal  = (0, limitMin),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                           )

        self.mthTurnC = one_way_bsh_connect(  
                                                        driver = '{}.rz'.format(mthCtrl),
                                                        target = '{}.MouthTurnC'.format(bshNode),
                                                        inVal  = (0, limitMax),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                           )


        minVal, maxVal = get_minMax_value(mthCtrl, 'MouthU')
        self.mouthU = one_way_bsh_connect(          
                                                        driver = '{}.MouthU'.format(mthCtrl),
                                                        target = '{}.MouthU'.format(bshNode),
                                                        inVal  = (minVal, maxVal),
                                                        outVal = (-1, 1),
                                                        buffer = False 
                                           )

        minVal, maxVal = get_minMax_value(mthCtrl, 'MouthClench')
        self.mouthClench = one_way_bsh_connect(  
                                                        driver = '{}.MouthClench'.format(mthCtrl),
                                                        target = '{}.MouthClench'.format(bshNode),
                                                        inVal  = (minVal, maxVal),
                                                        outVal = (-1, 1),
                                                        buffer = False 
                                               )

        #-- Mouth Upper Control
        limitMin, limitMax = get_minMax_value(mthUprCtrl, 'ty')
        self.lipsUprAllUp = one_way_bsh_connect(  
                                                        driver = '{}.ty'.format(mthUprCtrl),
                                                        target = '{}.LipsUprAllUp'.format(bshNode),
                                                        inVal  = (0, limitMax),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                               )

        self.lipsUprAllDn = one_way_bsh_connect(  
                                                        driver = '{}.ty'.format(mthUprCtrl),
                                                        target = '{}.LipsUprAllDn'.format(bshNode),
                                                        inVal  = (0, limitMin),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                               )

        minVal, maxVal = get_minMax_value(mthUprCtrl, 'LipsMidUD')
        self.lipsUprMidUp = one_way_bsh_connect(  
                                                        driver = '{}.LipsMidUD'.format(mthUprCtrl),
                                                        target = '{}.LipsUprUp'.format(bshNode),
                                                        inVal  = (0, maxVal),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                               )

        self.lipsUprMidDn = one_way_bsh_connect(  
                                                        driver = '{}.LipsMidUD'.format(mthUprCtrl),
                                                        target = '{}.LipsUprDn'.format(bshNode),
                                                        inVal  = (0, minVal),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                               )

        minVal, maxVal = get_minMax_value(mthUprCtrl, 'LipsLftUD')
        self.lipsUprLftUp = one_way_bsh_connect(  
                                                        driver = '{}.LipsLftUD'.format(mthUprCtrl),
                                                        target = '{}.LipsUprUp_L'.format(bshNode),
                                                        inVal  = (0, maxVal),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                               )

        self.lipsUprLftDn = one_way_bsh_connect(  
                                                        driver = '{}.LipsLftUD'.format(mthUprCtrl),
                                                        target = '{}.LipsUprDn_L'.format(bshNode),
                                                        inVal  = (0, minVal),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                               )

        minVal, maxVal = get_minMax_value(mthUprCtrl, 'LipsRgtUD')
        self.lipsUprRgtUp = one_way_bsh_connect(  
                                                        driver = '{}.LipsRgtUD'.format(mthUprCtrl),
                                                        target = '{}.LipsUprUp_R'.format(bshNode),
                                                        inVal  = (0, maxVal),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                               )

        self.lipsUprRgtDn = one_way_bsh_connect(  
                                                        driver = '{}.LipsRgtUD'.format(mthUprCtrl),
                                                        target = '{}.LipsUprDn_R'.format(bshNode),
                                                        inVal  = (0, minVal),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                               )

        minVal, maxVal = get_minMax_value(mthUprCtrl, 'LipsCurlIO')
        self.lipsUprCurlIn = one_way_bsh_connect(  
                                                        driver = '{}.LipsCurlIO'.format(mthUprCtrl),
                                                        target = '{}.LipsUprCurlIn'.format(bshNode),
                                                        inVal  = (0, minVal),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                               )

        self.lipsUprCurlOut = one_way_bsh_connect(  
                                                        driver = '{}.LipsCurlIO'.format(mthUprCtrl),
                                                        target = '{}.LipsUprCurlOut'.format(bshNode),
                                                        inVal  = (0, maxVal),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                               )

        #-- Mouth Lower Control
        limitMin, limitMax = get_minMax_value(mthLwrCtrl, 'ty')
        self.lipsLwrAllUp = one_way_bsh_connect(  
                                                        driver = '{}.ty'.format(mthLwrCtrl),
                                                        target = '{}.LipsLwrAllUp'.format(bshNode),
                                                        inVal  = (0, limitMax),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                               )

        self.lipsLwrAllDn = one_way_bsh_connect(  
                                                        driver = '{}.ty'.format(mthLwrCtrl),
                                                        target = '{}.LipsLwrAllDn'.format(bshNode),
                                                        inVal  = (0, limitMin),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                               )

        minVal, maxVal = get_minMax_value(mthLwrCtrl, 'LipsMidUD')
        self.lipsLwrMidUp = one_way_bsh_connect(  
                                                        driver = '{}.LipsMidUD'.format(mthLwrCtrl),
                                                        target = '{}.LipsLwrUp'.format(bshNode),
                                                        inVal  = (0, maxVal),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                               )

        self.lipsLwrMidDn = one_way_bsh_connect(  
                                                        driver = '{}.LipsMidUD'.format(mthLwrCtrl),
                                                        target = '{}.LipsLwrDn'.format(bshNode),
                                                        inVal  = (0, minVal),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                               )

        minVal, maxVal = get_minMax_value(mthLwrCtrl, 'LipsLftUD')
        self.lipsLwrLftUp = one_way_bsh_connect(  
                                                        driver = '{}.LipsLftUD'.format(mthLwrCtrl),
                                                        target = '{}.LipsLwrUp_L'.format(bshNode),
                                                        inVal  = (0, maxVal),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                               )

        self.lipsLwrLftDn = one_way_bsh_connect(  
                                                        driver = '{}.LipsLftUD'.format(mthLwrCtrl),
                                                        target = '{}.LipsLwrDn_L'.format(bshNode),
                                                        inVal  = (0, minVal),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                               )

        minVal, maxVal = get_minMax_value(mthLwrCtrl, 'LipsRgtUD')
        self.lipsLwrRgtUp = one_way_bsh_connect(  
                                                        driver = '{}.LipsRgtUD'.format(mthLwrCtrl),
                                                        target = '{}.LipsLwrUp_R'.format(bshNode),
                                                        inVal  = (0, maxVal),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                               )

        self.lipsLwrRgtDn = one_way_bsh_connect(  
                                                        driver = '{}.LipsRgtUD'.format(mthLwrCtrl),
                                                        target = '{}.LipsLwrDn_R'.format(bshNode),
                                                        inVal  = (0, minVal),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                               )

        minVal, maxVal = get_minMax_value(mthLwrCtrl, 'LipsCurlIO')
        self.lipsLwrCurlIn = one_way_bsh_connect(  
                                                        driver = '{}.LipsCurlIO'.format(mthLwrCtrl),
                                                        target = '{}.LipsLwrCurlIn'.format(bshNode),
                                                        inVal  = (0, minVal),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                               )

        self.lipsLwrCurlOut = one_way_bsh_connect(  
                                                        driver = '{}.LipsCurlIO'.format(mthLwrCtrl),
                                                        target = '{}.LipsLwrCurlOut'.format(bshNode),
                                                        inVal  = (0, maxVal),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                               )

        #-- Mouth Corner Left Control
        minVal, maxVal = get_minMax_value(mthLftCtrl, 'CheekUD')
        self.cheekUpLeft = one_way_bsh_connect(  
                                                        driver = '{}.CheekUD'.format(mthLftCtrl),
                                                        target = '{}.CheekUp_L'.format(bshNode),
                                                        inVal  = (0, maxVal),
                                                        outVal = (0, 1),
                                                        buffer = True 
                                               )

        self.cheekDnLeft = one_way_bsh_connect(  
                                                        driver = '{}.CheekUD'.format(mthLftCtrl),
                                                        target = '{}.CheekDn_L'.format(bshNode),
                                                        inVal  = (0, minVal),
                                                        outVal = (0, 1),
                                                        buffer = True 
                                               )

        if self.cheekUpLeft:
            self.cheekUpLeftAuto = auto_bsh_connect(
                                                            driver  = '{}.ty'.format(mthLftCtrl),
                                                            switch  = '{}Shape.AutoCheek'.format(mthLftCtrl),
                                                            target  = '{}.i1[1]'.format(self.cheekUpLeft['pma']),
                                                            inVal   = (0, 0.5),
                                                            side    = 'L'
                                                    )

        if self.cheekDnLeft:
            self.cheekDnLeftAuto = auto_bsh_connect(
                                                            driver  = '{}.ty'.format(mthLftCtrl),
                                                            switch  = '{}Shape.AutoCheek'.format(mthLftCtrl),
                                                            target  = '{}.i1[1]'.format(self.cheekDnLeft['pma']),
                                                            inVal   = (0, -0.5),
                                                            side    = 'L'
                                                    )

        minVal, maxVal = get_minMax_value(mthLftCtrl, 'PuffIO')
        self.puffInLeft = one_way_bsh_connect(  
                                                        driver = '{}.PuffIO'.format(mthLftCtrl),
                                                        target = '{}.PuffIn_L'.format(bshNode),
                                                        inVal  = (0, minVal),
                                                        outVal = (0, 1),
                                                        buffer = True 
                                               )

        self.puffOutLeft = one_way_bsh_connect(  
                                                        driver = '{}.PuffIO'.format(mthLftCtrl),
                                                        target = '{}.PuffOut_L'.format(bshNode),
                                                        inVal  = (0, maxVal),
                                                        outVal = (0, 1),
                                                        buffer = True 
                                               )

        if self.puffInLeft:
            self.puffInLeftAuto = auto_bsh_connect(
                                                            driver  = '{}.tx'.format(mthLftCtrl),
                                                            switch  = '{}Shape.AutoPuff'.format(mthLftCtrl),
                                                            target  = '{}.i1[1]'.format(self.puffInLeft['pma']),
                                                            inVal   = (0, -0.5),
                                                            side    = 'L'
                                                  )

        if self.puffOutLeft:
            self.puffOutLeftAuto = auto_bsh_connect(
                                                            driver  = '{}.tx'.format(mthLftCtrl),
                                                            switch  = '{}Shape.AutoPuff'.format(mthLftCtrl),
                                                            target  = '{}.i1[1]'.format(self.puffOutLeft['pma']),
                                                            inVal   = (0, 0.5),
                                                            side    = 'L'
                                                   )

        minVal, maxVal = get_minMax_value(mthLftCtrl, 'PuckerIO')
        self.puckerInLeft = one_way_bsh_connect(  
                                                        driver = '{}.PuckerIO'.format(mthLftCtrl),
                                                        target = '{}.PuckerIn_L'.format(bshNode),
                                                        inVal  = (0, minVal),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                               )

        self.puckerOutLeft = one_way_bsh_connect(  
                                                        driver = '{}.PuckerIO'.format(mthLftCtrl),
                                                        target = '{}.PuckerOut_L'.format(bshNode),
                                                        inVal  = (0, maxVal),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                               )

        limitMin, limitMax = get_minMax_value(mthLftCtrl, 'tx')
        self.cnrInLeft = one_way_bsh_connect(  
                                                        driver = '{}.tx'.format(mthLftCtrl),
                                                        target = '{}.CornerIn_L'.format(bshNode),
                                                        inVal  = (0, limitMin),
                                                        outVal = (0, 1),
                                                        buffer = True 
                                            )

        self.cnrOutLeft = one_way_bsh_connect(  
                                                        driver = '{}.tx'.format(mthLftCtrl),
                                                        target = '{}.CornerOut_L'.format(bshNode),
                                                        inVal  = (0, limitMax),
                                                        outVal = (0, 1),
                                                        buffer = True 
                                              )

        limitMin, limitMax = get_minMax_value(mthLftCtrl, 'ty')
        self.cnrUpLeft = one_way_bsh_connect(  
                                                        driver = '{}.ty'.format(mthLftCtrl),
                                                        target = '{}.CornerUp_L'.format(bshNode),
                                                        inVal  = (0, limitMax),
                                                        outVal = (0, 1),
                                                        buffer = True 
                                            )

        self.cnrDnLeft = one_way_bsh_connect(  
                                                        driver = '{}.ty'.format(mthLftCtrl),
                                                        target = '{}.CornerDn_L'.format(bshNode),
                                                        inVal  = (0, limitMin),
                                                        outVal = (0, 1),
                                                        buffer = True 
                                            )

        if self.cnrInLeft:
            self.cnrInUpLeft = combine_two_bsh_connect(
                                                                oneDriver  = '{}.ov'.format(self.cnrInLeft['rem']),
                                                                twoDriver  = '{}.ov'.format(self.cnrUpLeft['rem']),
                                                                oneDriven  = '{}.i1[1]'.format(self.cnrInLeft['pma']),
                                                                twoDriven  = '{}.i1[1]'.format(self.cnrUpLeft['pma']),
                                                                target  = '{}.CornerInUp_L'.format(bshNode),
                                                       )

            self.cnrInDnLeft = combine_two_bsh_connect(
                                                                oneDriver  = '{}.ov'.format(self.cnrInLeft['rem']),
                                                                twoDriver  = '{}.ov'.format(self.cnrDnLeft['rem']),
                                                                oneDriven  = '{}.i1[2]'.format(self.cnrInLeft['pma']),
                                                                twoDriven  = '{}.i1[1]'.format(self.cnrDnLeft['pma']),
                                                                target  = '{}.CornerInDn_L'.format(bshNode),
                                                       )

        if self.cnrOutLeft:
            self.cnrOutUpLeft = combine_two_bsh_connect(
                                                                oneDriver  = '{}.ov'.format(self.cnrOutLeft['rem']),
                                                                twoDriver  = '{}.ov'.format(self.cnrUpLeft['rem']),
                                                                oneDriven  = '{}.i1[1]'.format(self.cnrOutLeft['pma']),
                                                                twoDriven  = '{}.i1[2]'.format(self.cnrUpLeft['pma']),
                                                                target  = '{}.CornerOutUp_L'.format(bshNode),
                                                       )

            self.cnrOutDnLeft = combine_two_bsh_connect(
                                                                oneDriver  = '{}.ov'.format(self.cnrOutLeft['rem']),
                                                                twoDriver  = '{}.ov'.format(self.cnrDnLeft['rem']),
                                                                oneDriven  = '{}.i1[2]'.format(self.cnrOutLeft['pma']),
                                                                twoDriven  = '{}.i1[2]'.format(self.cnrDnLeft['pma']),
                                                                target  = '{}.CornerOutDn_L'.format(bshNode),
                                                       )


        #-- Mouth Corner Right Control
        minVal, maxVal = get_minMax_value(mthRgtCtrl, 'CheekUD')
        self.cheekUpRight = one_way_bsh_connect(  
                                                        driver = '{}.CheekUD'.format(mthRgtCtrl),
                                                        target = '{}.CheekUp_R'.format(bshNode),
                                                        inVal  = (0, maxVal),
                                                        outVal = (0, 1),
                                                        buffer = True 
                                               )

        self.cheekDnRight = one_way_bsh_connect(  
                                                        driver = '{}.CheekUD'.format(mthRgtCtrl),
                                                        target = '{}.CheekDn_R'.format(bshNode),
                                                        inVal  = (0, minVal),
                                                        outVal = (0, 1),
                                                        buffer = True 
                                               )

        if self.cheekUpRight:
            self.cheekUpRightAuto = auto_bsh_connect(
                                                            driver  = '{}.ty'.format(mthRgtCtrl),
                                                            switch  = '{}Shape.AutoCheek'.format(mthRgtCtrl),
                                                            target  = '{}.i1[1]'.format(self.cheekUpRight['pma']),
                                                            inVal   = (0, 0.5),
                                                            side    = 'R'
                                                    )

        if self.cheekDnRight:
            self.cheekDnRightAuto = auto_bsh_connect(
                                                            driver  = '{}.ty'.format(mthRgtCtrl),
                                                            switch  = '{}Shape.AutoCheek'.format(mthRgtCtrl),
                                                            target  = '{}.i1[1]'.format(self.cheekDnRight['pma']),
                                                            inVal   = (0, -0.5),
                                                            side    = 'R'
                                                    )

        minVal, maxVal = get_minMax_value(mthRgtCtrl, 'PuffIO')
        self.puffInRight = one_way_bsh_connect(  
                                                        driver = '{}.PuffIO'.format(mthRgtCtrl),
                                                        target = '{}.PuffIn_R'.format(bshNode),
                                                        inVal  = (0, minVal),
                                                        outVal = (0, 1),
                                                        buffer = True 
                                               )

        self.puffOutRight = one_way_bsh_connect(  
                                                        driver = '{}.PuffIO'.format(mthRgtCtrl),
                                                        target = '{}.PuffOut_R'.format(bshNode),
                                                        inVal  = (0, maxVal),
                                                        outVal = (0, 1),
                                                        buffer = True 
                                               )

        if self.puffInRight:
            self.puffInRightAuto = auto_bsh_connect(
                                                            driver  = '{}.tx'.format(mthRgtCtrl),
                                                            switch  = '{}Shape.AutoPuff'.format(mthRgtCtrl),
                                                            target  = '{}.i1[1]'.format(self.puffInRight['pma']),
                                                            inVal   = (0, -0.5),
                                                            side    = 'R'
                                                   )

        if self.puffOutRight:
            self.puffOutRightAuto = auto_bsh_connect(
                                                            driver  = '{}.tx'.format(mthRgtCtrl),
                                                            switch  = '{}Shape.AutoPuff'.format(mthRgtCtrl),
                                                            target  = '{}.i1[1]'.format(self.puffOutRight['pma']),
                                                            inVal   = (0, 0.5),
                                                            side    = 'R'
                                                    )

        minVal, maxVal = get_minMax_value(mthRgtCtrl, 'PuckerIO')
        self.puckerInRight = one_way_bsh_connect(  
                                                        driver = '{}.PuckerIO'.format(mthRgtCtrl),
                                                        target = '{}.PuckerIn_R'.format(bshNode),
                                                        inVal  = (0, minVal),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                                )

        self.puckerOutRight = one_way_bsh_connect(  
                                                        driver = '{}.PuckerIO'.format(mthRgtCtrl),
                                                        target = '{}.PuckerOut_R'.format(bshNode),
                                                        inVal  = (0, maxVal),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                                 )

        limitMin, limitMax = get_minMax_value(mthRgtCtrl, 'tx')
        self.cnrInRight = one_way_bsh_connect(  
                                                        driver = '{}.tx'.format(mthRgtCtrl),
                                                        target = '{}.CornerIn_R'.format(bshNode),
                                                        inVal  = (0, limitMin),
                                                        outVal = (0, 1),
                                                        buffer = True 
                                             )

        self.cnrOutRight = one_way_bsh_connect(  
                                                        driver = '{}.tx'.format(mthRgtCtrl),
                                                        target = '{}.CornerOut_R'.format(bshNode),
                                                        inVal  = (0, limitMax),
                                                        outVal = (0, 1),
                                                        buffer = True 
                                               )

        limitMin, limitMax = get_minMax_value(mthRgtCtrl, 'ty')
        self.cnrUpRight = one_way_bsh_connect(  
                                                        driver = '{}.ty'.format(mthRgtCtrl),
                                                        target = '{}.CornerUp_R'.format(bshNode),
                                                        inVal  = (0, limitMax),
                                                        outVal = (0, 1),
                                                        buffer = True 
                                             )

        self.cnrDnRight = one_way_bsh_connect(  
                                                        driver = '{}.ty'.format(mthRgtCtrl),
                                                        target = '{}.CornerDn_R'.format(bshNode),
                                                        inVal  = (0, limitMin),
                                                        outVal = (0, 1),
                                                        buffer = True 
                                             )

        if self.cnrInRight:
            self.cnrInUpRight = combine_two_bsh_connect(
                                                                oneDriver  = '{}.ov'.format(self.cnrInRight['rem']),
                                                                twoDriver  = '{}.ov'.format(self.cnrUpRight['rem']),
                                                                oneDriven  = '{}.i1[1]'.format(self.cnrInRight['pma']),
                                                                twoDriven  = '{}.i1[1]'.format(self.cnrUpRight['pma']),
                                                                target  = '{}.CornerInUp_R'.format(bshNode),
                                                       )

            self.cnrInDnRight = combine_two_bsh_connect(
                                                                oneDriver  = '{}.ov'.format(self.cnrInRight['rem']),
                                                                twoDriver  = '{}.ov'.format(self.cnrDnRight['rem']),
                                                                oneDriven  = '{}.i1[2]'.format(self.cnrInRight['pma']),
                                                                twoDriven  = '{}.i1[1]'.format(self.cnrDnRight['pma']),
                                                                target  = '{}.CornerInDn_R'.format(bshNode),
                                                       )

        if self.cnrOutRight:
            self.cnrOutUpRight = combine_two_bsh_connect(
                                                                oneDriver  = '{}.ov'.format(self.cnrOutRight['rem']),
                                                                twoDriver  = '{}.ov'.format(self.cnrUpRight['rem']),
                                                                oneDriven  = '{}.i1[1]'.format(self.cnrOutRight['pma']),
                                                                twoDriven  = '{}.i1[2]'.format(self.cnrUpRight['pma']),
                                                                target  = '{}.CornerOutUp_R'.format(bshNode),
                                                        )

            self.cnrOutDnRight = combine_two_bsh_connect(
                                                                oneDriver  = '{}.ov'.format(self.cnrOutRight['rem']),
                                                                twoDriver  = '{}.ov'.format(self.cnrDnRight['rem']),
                                                                oneDriven  = '{}.i1[2]'.format(self.cnrOutRight['pma']),
                                                                twoDriven  = '{}.i1[2]'.format(self.cnrDnRight['pma']),
                                                                target  = '{}.CornerOutDn_R'.format(bshNode),
                                                        )

        self.cnrUpCorLeft = add_corrective_blendshape(    
                                                                driver = '{}.ty'.format(mthLftCtrl),
                                                                driven = '{}.CornerUpCor_L'.format(bshNode),
                                                     )

        self.cnrDnCorLeft = add_corrective_blendshape(    
                                                                driver = '{}.ty'.format(mthLftCtrl),
                                                                driven = '{}.CornerDnCor_L'.format(bshNode),
                                                     )

        self.cnrInCorLeft = add_corrective_blendshape(    
                                                                driver = '{}.tx'.format(mthLftCtrl),
                                                                driven = '{}.CornerInCor_L'.format(bshNode),
                                                     )

        self.cnrOutCorLeft = add_corrective_blendshape(    
                                                                driver = '{}.tx'.format(mthLftCtrl),
                                                                driven = '{}.CornerOutCor_L'.format(bshNode),
                                                      )

        self.cnrInUpCorLeft = combine_corrective_blendshape(    
                                                                    driver = ['{}.CornerInCor_L'.format(bshNode), '{}.CornerUpCor_L'.format(bshNode)],
                                                                    driven =  '{}.CornerInUpCor_L'.format(bshNode)
                                                           )

        self.cnrInDnCorLeft = combine_corrective_blendshape(    
                                                                    driver = ['{}.CornerInCor_L'.format(bshNode), '{}.CornerDnCor_L'.format(bshNode)],
                                                                    driven =  '{}.CornerInDnCor_L'.format(bshNode)
                                                           )

        self.cnrOutUpCorLeft = combine_corrective_blendshape(    
                                                                    driver = ['{}.CornerOutCor_L'.format(bshNode), '{}.CornerUpCor_L'.format(bshNode)],
                                                                    driven =  '{}.CornerOutUpCor_L'.format(bshNode)
                                                            )

        self.cnrOutDnCorLeft = combine_corrective_blendshape(    
                                                                    driver = ['{}.CornerOutCor_L'.format(bshNode), '{}.CornerDnCor_L'.format(bshNode)],
                                                                    driven =  '{}.CornerOutDnCor_L'.format(bshNode)
                                                            )

        self.cnrUpOpenCorLeft = combine_corrective_blendshape(
                                                                    driver = ['{}.JawOpenCor'.format(bshNode), '{}.CornerUpCor_L'.format(bshNode)],
                                                                    driven =  '{}.CornerUpOpenCor_L'.format(bshNode)
                                                             )

        self.cnrDnOpenCorLeft = combine_corrective_blendshape(
                                                                    driver = ['{}.JawOpenCor'.format(bshNode), '{}.CornerDnCor_L'.format(bshNode)],
                                                                    driven =  '{}.CornerDnOpenCor_L'.format(bshNode)
                                                             )

        self.cnrInOpenCorLeft = combine_corrective_blendshape(
                                                                    driver = ['{}.JawOpenCor'.format(bshNode), '{}.CornerInCor_L'.format(bshNode)],
                                                                    driven =  '{}.CornerInOpenCor_L'.format(bshNode)
                                                             )

        self.cnrOutOpenCorLeft = combine_corrective_blendshape(
                                                                    driver = ['{}.JawOpenCor'.format(bshNode), '{}.CornerOutCor_L'.format(bshNode)],
                                                                    driven =  '{}.CornerOutOpenCor_L'.format(bshNode)
                                                              )

        self.cnrInUpOpenCorLeft = combine_corrective_blendshape(
                                                                    driver = ['{}.JawOpenCor'.format(bshNode), '{}.CornerInUpCor_L'.format(bshNode)],
                                                                    driven =  '{}.CornerInUpOpenCor_L'.format(bshNode)
                                                               )

        self.cnrInDnOpenCorLeft = combine_corrective_blendshape(
                                                                    driver = ['{}.JawOpenCor'.format(bshNode), '{}.CornerInDnCor_L'.format(bshNode)],
                                                                    driven =  '{}.CornerInDnOpenCor_L'.format(bshNode)
                                                               )

        self.cnrOutUpOpenCorLeft = combine_corrective_blendshape(
                                                                    driver = ['{}.JawOpenCor'.format(bshNode), '{}.CornerOutUpCor_L'.format(bshNode)],
                                                                    driven =  '{}.CornerOutUpOpenCor_L'.format(bshNode)
                                                                )

        self.cnrOutDnOpenCorLeft = combine_corrective_blendshape(
                                                                    driver = ['{}.JawOpenCor'.format(bshNode), '{}.CornerOutDnCor_L'.format(bshNode)],
                                                                    driven =  '{}.CornerOutDnOpenCor_L'.format(bshNode)
                                                                )

        self.cnrUpOpenCorRight = combine_corrective_blendshape(
                                                                    driver = ['{}.JawOpenCor'.format(bshNode), '{}.CornerUpCor_R'.format(bshNode)],
                                                                    driven =  '{}.CornerUpOpenCor_R'.format(bshNode)
                                                              )

        self.cnrDnOpenCorRight = combine_corrective_blendshape(
                                                                    driver = ['{}.JawOpenCor'.format(bshNode), '{}.CornerDnCor_R'.format(bshNode)],
                                                                    driven =  '{}.CornerDnOpenCor_R'.format(bshNode)
                                                              )

        self.cnrInOpenCorRight = combine_corrective_blendshape(
                                                                    driver = ['{}.JawOpenCor'.format(bshNode), '{}.CornerInCor_R'.format(bshNode)],
                                                                    driven =  '{}.CornerInOpenCor_R'.format(bshNode)
                                                              )

        self.cnrOutOpenCorRight = combine_corrective_blendshape(
                                                                    driver = ['{}.JawOpenCor'.format(bshNode), '{}.CornerOutCor_R'.format(bshNode)],
                                                                    driven =  '{}.CornerOutOpenCor_R'.format(bshNode)
                                                               )

        self.cnrInUpOpenCorRight = combine_corrective_blendshape(
                                                                    driver = ['{}.JawOpenCor'.format(bshNode), '{}.CornerInUpCor_R'.format(bshNode)],
                                                                    driven =  '{}.CornerInUpOpenCor_R'.format(bshNode)
                                                                )

        self.cnrInDnOpenCorRight = combine_corrective_blendshape(
                                                                    driver = ['{}.JawOpenCor'.format(bshNode), '{}.CornerInDnCor_R'.format(bshNode)],
                                                                    driven =  '{}.CornerInDnOpenCor_R'.format(bshNode)
                                                                )

        self.cnrOutUpOpenCorRight = combine_corrective_blendshape(
                                                                    driver = ['{}.JawOpenCor'.format(bshNode), '{}.CornerOutUpCor_R'.format(bshNode)],
                                                                    driven =  '{}.CornerOutUpOpenCor_R'.format(bshNode)
                                                                 )

        self.cnrOutDnOpenCorRight = combine_corrective_blendshape(
                                                                    driver = ['{}.JawOpenCor'.format(bshNode), '{}.CornerOutDnCor_R'.format(bshNode)],
                                                                    driven =  '{}.CornerOutDnOpenCor_R'.format(bshNode)
                                                                 )

        self.cnrUpCorRight = add_corrective_blendshape(    
                                                                driver = '{}.ty'.format(mthRgtCtrl),
                                                                driven = '{}.CornerUpCor_R'.format(bshNode),
                                                      )

        self.cnrDnCorRight = add_corrective_blendshape(    
                                                                driver = '{}.ty'.format(mthRgtCtrl),
                                                                driven = '{}.CornerDnCor_R'.format(bshNode),
                                                      )

        self.cnrInCorRight = add_corrective_blendshape(    
                                                                driver = '{}.tx'.format(mthRgtCtrl),
                                                                driven = '{}.CornerInCor_R'.format(bshNode),
                                                      )

        self.cnrOutCorRight = add_corrective_blendshape(    
                                                                driver = '{}.tx'.format(mthRgtCtrl),
                                                                driven = '{}.CornerOutCor_R'.format(bshNode),
                                                       )

        self.cnrInUpCorRight = combine_corrective_blendshape(    
                                                                    driver = ['{}.CornerInCor_R'.format(bshNode), '{}.CornerUpCor_R'.format(bshNode)],
                                                                    driven =  '{}.CornerInUpCor_R'.format(bshNode)
                                                            )

        self.cnrInDnCorRight = combine_corrective_blendshape(    
                                                                    driver = ['{}.CornerInCor_R'.format(bshNode), '{}.CornerDnCor_R'.format(bshNode)],
                                                                    driven =  '{}.CornerInDnCor_R'.format(bshNode)
                                                            )

        self.cnrOutUpCorRight = combine_corrective_blendshape(    
                                                                    driver = ['{}.CornerOutCor_R'.format(bshNode), '{}.CornerUpCor_R'.format(bshNode)],
                                                                    driven =  '{}.CornerOutUpCor_R'.format(bshNode)
                                                             )

        self.cnrOutDnCorRight = combine_corrective_blendshape(    
                                                                    driver = ['{}.CornerOutCor_R'.format(bshNode), '{}.CornerDnCor_R'.format(bshNode)],
                                                                    driven =  '{}.CornerOutDnCor_R'.format(bshNode)
                                                             )

    #------------------------------------------------------
    #   JAW Controls Connect
    #------------------------------------------------------
    def jawCtrlBsh( self , bshNode  = 'BshRig_Bsn' ,
                           jawCtrl  = 'Jaw_Ctrl'):

        #-- Jaw Control
        self.jawOpenCor = add_corrective_blendshape(    
                                                            driver = '{}.rx'.format(jawCtrl),
                                                            driven = '{}.JawOpenCor'.format(bshNode),
                                                   )

        self.jawCloseCor = add_corrective_blendshape(    
                                                            driver = '{}.rx'.format(jawCtrl),
                                                            driven = '{}.JawCloseCor'.format(bshNode),
                                                    )

        pmc.PyNode(jawCtrl).LipsCollision >> self.jawCloseCor.pynode.omx

    #------------------------------------------------------
    #   NOSE Controls Connect
    #------------------------------------------------------
    def noseCtrlBsh( self , bshNode   = 'BshRig_Bsn' ,
                            noseCtrl  = 'NoseBsh_Ctrl'):

        #-- Nose Control
        limitMin, limitMax = get_minMax_value(noseCtrl, 'tx')
        self.noseLeft = one_way_bsh_connect(  
                                                        driver = '{}.tx'.format(noseCtrl),
                                                        target = '{}.NoseLeft'.format(bshNode),
                                                        inVal  = (0, limitMax),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                            )

        self.noseRight = one_way_bsh_connect(  
                                                        driver = '{}.tx'.format(noseCtrl),
                                                        target = '{}.NoseRight'.format(bshNode),
                                                        inVal  = (0, limitMin),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                           )

        limitMin, limitMax = get_minMax_value(noseCtrl, 'ty')
        self.noseUp = one_way_bsh_connect(  
                                                        driver = '{}.ty'.format(noseCtrl),
                                                        target = '{}.NoseUp'.format(bshNode),
                                                        inVal  = (0, limitMax),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                       )

        self.noseDn = one_way_bsh_connect(  
                                                        driver = '{}.ty'.format(noseCtrl),
                                                        target = '{}.NoseDn'.format(bshNode),
                                                        inVal  = (0, limitMin),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                           )

        minVal, maxVal = get_minMax_value(noseCtrl, 'NoseStSq')
        self.noseStretch = one_way_bsh_connect(  
                                                        driver = '{}.NoseStSq'.format(noseCtrl),
                                                        target = '{}.NoseStretch'.format(bshNode),
                                                        inVal  = (0, minVal),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                               )

        self.noseSquash = one_way_bsh_connect(  
                                                        driver = '{}.NoseStSq'.format(noseCtrl),
                                                        target = '{}.NoseSquash'.format(bshNode),
                                                        inVal  = (0, maxVal),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                               )

        minVal, maxVal = get_minMax_value(noseCtrl, 'NoseSnarl')
        self.noseSnarl = one_way_bsh_connect(  
                                                        driver = '{}.NoseSnarl'.format(noseCtrl),
                                                        target = '{}.NoseSnarl'.format(bshNode),
                                                        inVal  = (minVal, maxVal),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                           )

        minVal, maxVal = get_minMax_value(noseCtrl, 'NoseLeftUD')
        self.noseLeftUp = one_way_bsh_connect(  
                                                        driver = '{}.NoseLeftUD'.format(noseCtrl),
                                                        target = '{}.NoseUp_L'.format(bshNode),
                                                        inVal  = (0, maxVal),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                               )

        self.noseLeftDn = one_way_bsh_connect(  
                                                        driver = '{}.NoseLeftUD'.format(noseCtrl),
                                                        target = '{}.NoseDn_L'.format(bshNode),
                                                        inVal  = (0, minVal),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                               )

        minVal, maxVal = get_minMax_value(noseCtrl, 'NoseRightUD')
        self.noseRightUp = one_way_bsh_connect(  
                                                        driver = '{}.NoseRightUD'.format(noseCtrl),
                                                        target = '{}.NoseUp_R'.format(bshNode),
                                                        inVal  = (0, maxVal),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                               )

        self.noseRightDn = one_way_bsh_connect(  
                                                        driver = '{}.NoseRightUD'.format(noseCtrl),
                                                        target = '{}.NoseDn_R'.format(bshNode),
                                                        inVal  = (0, minVal),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                               )

        minVal, maxVal = get_minMax_value(noseCtrl, 'NoseLeftFC')
        self.noseLeftTurnF = one_way_bsh_connect(  
                                                        driver = '{}.NoseLeftFC'.format(noseCtrl),
                                                        target = '{}.NoseTurnF_L'.format(bshNode),
                                                        inVal  = (0, maxVal),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                               )

        self.noseLeftTurnC = one_way_bsh_connect(  
                                                        driver = '{}.NoseLeftFC'.format(noseCtrl),
                                                        target = '{}.NoseTurnC_L'.format(bshNode),
                                                        inVal  = (0, minVal),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                               )

        minVal, maxVal = get_minMax_value(noseCtrl, 'NoseRightFC')
        self.noseRightTurnF = one_way_bsh_connect(  
                                                        driver = '{}.NoseRightFC'.format(noseCtrl),
                                                        target = '{}.NoseTurnF_R'.format(bshNode),
                                                        inVal  = (0, maxVal),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                               )

        self.noseRightTurnC = one_way_bsh_connect(  
                                                        driver = '{}.NoseRightFC'.format(noseCtrl),
                                                        target = '{}.NoseTurnC_R'.format(bshNode),
                                                        inVal  = (0, minVal),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                               )

        minVal, maxVal = get_minMax_value(noseCtrl, 'EyebrowPull')
        self.eyebrowPull = one_way_bsh_connect(  
                                                        driver = '{}.EyebrowPull'.format(noseCtrl),
                                                        target = '{}.HeadEyebrowPull'.format(bshNode),
                                                        inVal  = (minVal, maxVal),
                                                        outVal = (-1, 1),
                                                        buffer = False 
                                               )


    #------------------------------------------------------
    #   HEAD Controls Connect
    #------------------------------------------------------ 
    def headCtrlBsh( self , bshNode    = 'BshRig_Bsn' ,
                            hdUprCtrl  = 'FaceUprBsh_Ctrl' ,
                            hdLwrCtrl  = 'FaceLwrBsh_Ctrl'):

        #-- Head Upper Control
        limitMin, limitMax = get_minMax_value(hdUprCtrl, 'tx')
        self.headUprLeft = one_way_bsh_connect(  
                                                        driver = '{}.tx'.format(hdUprCtrl),
                                                        target = '{}.HeadUprLeft'.format(bshNode),
                                                        inVal  = (0, limitMax),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                               )

        self.headUprRight = one_way_bsh_connect(  
                                                        driver = '{}.tx'.format(hdUprCtrl),
                                                        target = '{}.HeadUprRight'.format(bshNode),
                                                        inVal  = (0, limitMin),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                               )

        limitMin, limitMax = get_minMax_value(hdUprCtrl, 'ty')
        self.headUprStretch = one_way_bsh_connect(  
                                                        driver = '{}.ty'.format(hdUprCtrl),
                                                        target = '{}.HeadUprStretch'.format(bshNode),
                                                        inVal  = (0, limitMax),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                               )

        self.headUprSquash = one_way_bsh_connect(  
                                                        driver = '{}.ty'.format(hdUprCtrl),
                                                        target = '{}.HeadUprSquash'.format(bshNode),
                                                        inVal  = (0, limitMin),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                               )
        
        limitMin, limitMax = get_minMax_value(hdUprCtrl, 'tz')
        self.headUprFront = one_way_bsh_connect(  
                                                        driver = '{}.tz'.format(hdUprCtrl),
                                                        target = '{}.HeadUprFront'.format(bshNode),
                                                        inVal  = (0, limitMax),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                               )

        self.headUprBack = one_way_bsh_connect(  
                                                        driver = '{}.tz'.format(hdUprCtrl),
                                                        target = '{}.HeadUprBack'.format(bshNode),
                                                        inVal  = (0, limitMin),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                               )

        #-- Head Lower Control
        limitMin, limitMax = get_minMax_value(hdLwrCtrl, 'tx')
        self.headLwrLeft = one_way_bsh_connect(  
                                                        driver = '{}.tx'.format(hdLwrCtrl),
                                                        target = '{}.HeadLwrLeft'.format(bshNode),
                                                        inVal  = (0, limitMax),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                               )

        self.headLwrRight = one_way_bsh_connect(  
                                                        driver = '{}.tx'.format(hdLwrCtrl),
                                                        target = '{}.HeadLwrRight'.format(bshNode),
                                                        inVal  = (0, limitMin),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                               )

        limitMin, limitMax = get_minMax_value(hdLwrCtrl, 'ty')
        self.headLwrStretch = one_way_bsh_connect(  
                                                        driver = '{}.ty'.format(hdLwrCtrl),
                                                        target = '{}.HeadLwrStretch'.format(bshNode),
                                                        inVal  = (0, limitMin),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                               )
        
        self.headLwrSquash = one_way_bsh_connect(  
                                                        driver = '{}.ty'.format(hdLwrCtrl),
                                                        target = '{}.HeadLwrSquash'.format(bshNode),
                                                        inVal  = (0, limitMax),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                               )
        
        limitMin, limitMax = get_minMax_value(hdLwrCtrl, 'tz')
        self.headLwrFront = one_way_bsh_connect(  
                                                        driver = '{}.tz'.format(hdLwrCtrl),
                                                        target = '{}.HeadLwrFront'.format(bshNode),
                                                        inVal  = (0, limitMax),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                               )

        self.headLwrBack = one_way_bsh_connect(  
                                                        driver = '{}.tz'.format(hdLwrCtrl),
                                                        target = '{}.HeadLwrBack'.format(bshNode),
                                                        inVal  = (0, limitMin),
                                                        outVal = (0, 1),
                                                        buffer = False 
                                               )

def connect_bsh( eyebrow = True ,
                 eye     = True ,
                 mouth   = True ,
                 nose    = True ,
                 head    = True ,
                 bshNode = 'BshRig_Bsn'):

    connect = ControlsBsh()

    #------------------------------------------------------
    #   EYEBROW CONNECT
    #------------------------------------------------------
    if eyebrow :
        eyebrowL = connect.eyebrowCtrlBsh( 
                                                bshNode    =  bshNode ,
                                                ebCtrl     = 'EyebrowBsh_L_Ctrl' ,
                                                ebInCtrl   = 'EyebrowInBsh_L_Ctrl' ,
                                                ebMidCtrl  = 'EyebrowMidBsh_L_Ctrl' ,
                                                ebOutCtrl  = 'EyebrowOutBsh_L_Ctrl' ,
                                                side       = 'L' 
                                         )

        eyebrowR = connect.eyebrowCtrlBsh( 
                                                bshNode    =  bshNode ,
                                                ebCtrl     = 'EyebrowBsh_R_Ctrl' ,
                                                ebInCtrl   = 'EyebrowInBsh_R_Ctrl' ,
                                                ebMidCtrl  = 'EyebrowMidBsh_R_Ctrl' ,
                                                ebOutCtrl  = 'EyebrowOutBsh_R_Ctrl' ,
                                                side       = 'R' 
                                         )

    #------------------------------------------------------
    #   EYE CONNECT
    #------------------------------------------------------
    if eye :
        eyeL = connect.eyeCtrlBsh(
                                        bshNode     =  bshNode ,
                                        eyeCtrl     = 'EyeLidsBsh_L_Ctrl' ,
                                        eyeUprCtrl  = 'EyeLidsUprBsh_L_Ctrl' ,
                                        eyeLwrCtrl  = 'EyeLidsLwrBsh_L_Ctrl' ,
                                        side        = 'L' 
                                 )

        eyeR = connect.eyeCtrlBsh(
                                        bshNode     =  bshNode ,
                                        eyeCtrl     = 'EyeLidsBsh_R_Ctrl' ,
                                        eyeUprCtrl  = 'EyeLidsUprBsh_R_Ctrl' ,
                                        eyeLwrCtrl  = 'EyeLidsLwrBsh_R_Ctrl' ,
                                        side        = 'R' 
                                 )

    #------------------------------------------------------
    #   MOUTH CONNECT
    #------------------------------------------------------
    if mouth :
        mouth = connect.mouthCtrlBsh(
                                        bshNode     =  bshNode ,
                                        mthCtrl     = 'MouthBsh_Ctrl' ,
                                        mthUprCtrl  = 'MouthUprBsh_Ctrl' ,
                                        mthLwrCtrl  = 'MouthLwrBsh_Ctrl' ,
                                        mthLftCtrl  = 'MouthCnrBsh_L_Ctrl' ,
                                        mthRgtCtrl  = 'MouthCnrBsh_R_Ctrl' 
                                    )

    #------------------------------------------------------
    #   NOSE CONNECT
    #------------------------------------------------------
    if nose :
        nose = connect.noseCtrlBsh(
                                        bshNode   =  bshNode ,
                                        noseCtrl  = 'NoseBsh_Ctrl' 
                                  )

    #------------------------------------------------------
    #   HEAD CONNECT
    #------------------------------------------------------
    if head :
        head = connect.headCtrlBsh(
                                        bshNode    =  bshNode ,
                                        hdUprCtrl  = 'FaceUprBsh_Ctrl' ,
                                        hdLwrCtrl  = 'FaceLwrBsh_Ctrl' 
                                  )

def connect_extraFace(bshNode = 'BshRig_Bsn'):
    extraTmpPath = "O:/Pipeline/core/rf_maya/ncscript2/template/ctrl_facial_extra.ma"
    pmc.importFile( extraTmpPath , preserveReferences = True )

    pmc.connectAttr('FaceBrowsA_Cnd.outColorR' , '{}.FaceBrowsA'.format(bshNode))
    pmc.connectAttr('FaceBrowsB_Cnd.outColorR' , '{}.FaceBrowsB'.format(bshNode))
    pmc.connectAttr('FaceBrowsC_Cnd.outColorR' , '{}.FaceBrowsC'.format(bshNode))
    pmc.connectAttr('FaceBrowsD_Cnd.outColorR' , '{}.FaceBrowsD'.format(bshNode))
    pmc.connectAttr('FaceBrowsE_Cnd.outColorR' , '{}.FaceBrowsE'.format(bshNode))
    pmc.connectAttr('FaceEyeA_Cnd.outColorR' , '{}.FaceEyeA'.format(bshNode))
    pmc.connectAttr('FaceEyeB_Cnd.outColorR' , '{}.FaceEyeB'.format(bshNode))
    pmc.connectAttr('FaceEyeC_Cnd.outColorR' , '{}.FaceEyeC'.format(bshNode))
    pmc.connectAttr('FaceEyeD_Cnd.outColorR' , '{}.FaceEyeD'.format(bshNode))
    pmc.connectAttr('FaceEyeE_Cnd.outColorR' , '{}.FaceEyeE'.format(bshNode))
    pmc.connectAttr('FaceMthA_Cnd.outColorR' , '{}.FaceMouthA'.format(bshNode))
    pmc.connectAttr('FaceMthB_Cnd.outColorR' , '{}.FaceMouthB'.format(bshNode))
    pmc.connectAttr('FaceMthC_Cnd.outColorR' , '{}.FaceMouthC'.format(bshNode))
    pmc.connectAttr('FaceMthD_Cnd.outColorR' , '{}.FaceMouthD'.format(bshNode))
    pmc.connectAttr('FaceMthE_Cnd.outColorR' , '{}.FaceMouthE'.format(bshNode))
    pmc.connectAttr('FaceNoseA_Cnd.outColorR' , '{}.FaceNoseA'.format(bshNode))
    pmc.connectAttr('FaceNoseB_Cnd.outColorR' , '{}.FaceNoseB'.format(bshNode))
    pmc.connectAttr('FaceNoseC_Cnd.outColorR' , '{}.FaceNoseC'.format(bshNode))
    pmc.connectAttr('FaceNoseD_Cnd.outColorR' , '{}.FaceNoseD'.format(bshNode))
    pmc.connectAttr('FaceNoseE_Cnd.outColorR' , '{}.FaceNoseE'.format(bshNode))
    pmc.connectAttr('FaceOval_Cnd.outColorR' , '{}.FaceOval'.format(bshNode))
    pmc.connectAttr('FaceRound_Cnd.outColorR' , '{}.FaceRound'.format(bshNode))
    pmc.connectAttr('FaceSquare_Cnd.outColorR' , '{}.FaceSquare'.format(bshNode))
    pmc.connectAttr('FaceLong_Cnd.outColorR' , '{}.FaceLong'.format(bshNode))
    pmc.connectAttr('FaceDaimond_Cnd.outColorR' , '{}.FaceDaimond'.format(bshNode))

    pmc.parent('FaceShapBsheCtrlZro_Grp', 'FacialBshCtrlZro_Grp')























