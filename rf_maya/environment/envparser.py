import os
import sys
from re import match
from collections import OrderedDict, defaultdict
import shutil

# class MayaHomeError(Exception):
#     pass

class OverrideEnvironment():
    def __init__(self, **kwargs):
        self.original_env = os.environ.copy()
        self.overrides = kwargs

    def __enter__(self):
        for key, value in self.overrides.items():
            os.environ[key] = value

    def __exit__(self, *args):
        os.environ = self.original_env

def get_mayaenv_path(mayaVersion):
    ''' Get Maya.env path being used in current session '''
    if 'MAYA_APP_DIR' in os.environ:
        path = os.environ['MAYA_APP_DIR']
    else:
        home_dir = os.path.expanduser('~')
        path = '%s/documents/maya'
    path = '%s/%s/Maya.env' %(path, mayaVersion)
    if os.path.exists(path):
        return path

def parse(path=None):
    ''' Read Maya.env from path. Return OrderedDict of variable:values '''
    result = OrderedDict()
    if not path or not os.path.exists(path):
        return result

    # read from Maya.env
    data = None
    with open(path, 'r') as f:
        data = f.readlines()

    # clean
    cleaned_data = []
    for d in data:
        d = d.replace('\r', '')
        d = d.replace('\n', '')
        d = d.replace('\t', '')
        if d:
            cleaned_data.append(d)
            
    # create result as OrderedDict
    for line in cleaned_data:
        line = line.replace(' ', '')
        splits = line.split('=')
        var = splits[0]
        valueTxt = ''.join(splits[1:])
        values = valueTxt.split(';')
        result[var] = [v for v in values if v]

    return result

def mergeEnvData(a_data, b_data):
    ''' merge values in Maya.env file from pathA with pathB and write result to outputPath '''
    # a_data = parse(path=pathA)
    # b_data = parse(path=pathB)

    # update dict
    merged_data = a_data.copy()

    # looping over default env
    for key, bvalues in b_data.items():
        # if project env has the same key as the looping default env
        if key in a_data:
            avalues = a_data[key]
            # if the value is ['0'] or ['1']
            if len(avalues) == 1 and match('^[0-9\.e]*$', avalues[0]):
                
                merged_data[key] = avalues
            else: # if it's a list of strings, just append to the end of list
                for bv in bvalues:
                    if bv not in avalues:
                        avalues.append(bv)
                merged_data[key] = avalues
        else: # new key
            merged_data[key] = bvalues

    return merged_data

def writeEnv(data, path):
    # create string to write
    envStr = ''
    for key, values in data.items():
        envStr += '%s = ' %key
        valueStr = '%s\n' %(';'.join(values))
        envStr += valueStr

    # write result to outputPath
    with open(path, 'w') as f:
        f.write(envStr)

    return path

def writeCurrentEnv(data, path):
    # create string to write
    envStr = ''
    for key, values in data.items():
        # print key, values
        if not values:
            continue
        envStr += '%s = %s\n\n' %(key, ';'.join(values))

        # if len(values) > 1:
        #     envStr += '%s = %s\n\n' %(key, ';\r\t\t'.join(values))
        # else:
        #     envStr += '%s = %s\n\n' %(key, values[0])

    # write result to outputPath
    with open(path, 'w') as f:
        f.write(envStr)

    return path

def setEnv(data):
    dataKeys = data.keys()
    res = defaultdict(list)
    # print '\n'.join(dataKeys)
    for var, values in data.items():
        resolved_values = []
        for v in values:
            # resolve env
            v = os.path.expandvars(v).replace('\\', '/')

            # resolve in-file %VAR%
            exit = False
            while not exit:
                solved = False
                for k in dataKeys:
                    kw = '%{0}%'.format(k)
                    if kw in v and k != var:
                        v = v.replace(kw, res[k][0])
                        if '%' in v:
                            solved = False
                        else:
                            solved = True
                        break
                else:
                    solved = True

                exit = solved

            resolved_values.append(v)
            res[var].append(v)

        old_value_lists = []
        if var in os.environ:
            old_value_str = os.environ[var]
            old_value_lists = [value.replace('\\', '/') for value in old_value_str.split(';') if value]
        
        # combine and remove duplicates while maintaining order
        new_value_lists = list(OrderedDict.fromkeys(old_value_lists + resolved_values))
        os.environ[var] = ';'.join(new_value_lists)

def overwriteEnv(pathA, pathB):
    ''' overwrite env in pathA with what's in pathB '''
    a_data = parse(path=pathA)
    b_data = parse(path=pathB)

    # looping over dept env
    for key, bvalues in b_data.items():
        # if project env has the same key as the looping default env
        # if key in a_data:
        a_data[key] = bvalues

    return a_data

def get_available_maya_versions(project):
    root = os.environ['RFSCRIPT'].replace('\\', '/')  # O:/pipeline
    proj_path = '%s/core/rf_env/%s/maya' %(root, project)
    if not os.path.exists(proj_path): 
        proj_path = '%s/core/rf_env/%s/maya' %(root, 'default')
    return [f for f in os.listdir(proj_path) if os.path.isdir('%s/%s' %(proj_path, f))]

def setupEnv(project, department, version, mergeWithDefault=True, setEnvironment=True, writeToFile=True):
    sys.stdout.write('\nSetting up environments...')
    root = os.environ['RFSCRIPT'].replace('\\', '/')  # O:/pipeline
    defaultEnv = '%s/core/rf_env/default/maya/%s/Maya.env' %(root, version)
    defaultUserPref = '%s/core/rf_env/default/maya/%s/prefs/userPrefs.mel' %(root, version)
    projEnvBase = '%s/core/rf_env/%s/maya/%s/Maya' %(root, project, version)
    projEnv = '%s.env' %(projEnvBase)
    # if by-project env exists
    projData = OrderedDict()
    defaultData = OrderedDict()

    # get project data
    if os.path.exists(projEnv):
        projDeptEnv = '%s_%s.env' %(projEnvBase, department)
        if os.path.exists(projDeptEnv):
            sys.stdout.write('\nPer-department env: %s' %projDeptEnv)
            projData = overwriteEnv(pathA=projEnv, pathB=projDeptEnv)
        else:
            sys.stdout.write('\nNo per-department env setup found.')

    else: # use studio base "baseProject" env
        sys.stdout.write('\nUsing baseProject environment...')
        projEnv = '%s/core/rf_env/baseProject/maya/%s/Maya.env' %(root, version)
    if not projData:
        projData = parse(path=projEnv)

    # make sure maya home is created
    versionDir = getMayaHome(version)
    sys.stdout.write('\nversionDir: %s' %versionDir)
    if not os.path.exists(versionDir):
        sys.stdout.write('\nCreating folder: %s' %versionDir)
        os.makedirs(versionDir)

    # copy default env, if not exists
    current_env = '%s/Maya.env' %versionDir
    if not os.path.exists(current_env):
        sys.stdout.write('\nCopying: %s ---> %s' %(defaultEnv, current_env))
        shutil.copy(defaultEnv, current_env)

    # copy default prefs, if not exists
    current_pref_dir = '%s/prefs' %(versionDir)
    current_userPref = '%s/userPrefs.mel' %current_pref_dir
    if not os.path.exists(current_userPref):
        if not os.path.exists(current_pref_dir):
            sys.stdout.write('\nCreating folder: %s' %current_pref_dir)
            os.makedirs(current_pref_dir)
        shutil.copy(defaultUserPref, current_userPref)
        sys.stdout.write('\nCopying: %s ---> %s' %(defaultUserPref, current_userPref))

    # merge the env data
    resultData = None
    sys.stdout.write('\nmergeWithDefault: %s\n' %mergeWithDefault)
    if mergeWithDefault:
        # get default data
        defaultData = parse(path=defaultEnv)
        resultData = mergeEnvData(a_data=projData, b_data=defaultData)

    else:
        resultData = projData

    # clean the merged data
    resultData = cleanData(resultData)

    # Set env via os.environ
    if setEnvironment:
        setEnv(resultData)
        sys.stdout.write('Environment set in OS.\n')

    # write out ProjectMaya.env file
    if writeToFile:
        proj_env = '%s/Maya%s.env' %(versionDir, project)
        path = writeCurrentEnv(resultData, proj_env)
        sys.stdout.write('Result Env file: %s\n\n' %proj_env)

    return resultData

def cleanData(data):
    # new_data = OrderedDict()
    all_keys = data.keys()
    for key, values in data.items():
        new_value = []
        for v in values:
            v = v.replace('\\', '/')
            
            if '%' in v:
                for k in all_keys:
                    if k == key:
                        continue
                    mstr = '.*(%{0}%).*'.format(k)
                    m = match(mstr, v)
                    if m:
                        found_str = m.group(1)
                        v = v.replace(found_str, ';'.join(data[k]))
            if '$' in v:
                v = os.path.expandvars(v)
            
            new_value.append(v)
        data[key] = new_value

    return data

def revertEnv(version):
    versionDir = getMayaHome(version)
    temp_env = '%s/_Maya.env' %versionDir
    current_env = '%s/Maya.env' %versionDir

    # if both exists remove the empty Maya.env and rename _Maya.env back to normal
    if os.path.exists(temp_env) and os.path.exists(current_env):
        sys.stdout.write('\nReverting bypassed Env...')
        sys.stdout.write('\nRemoving: %s' %current_env)
        os.remove(current_env)
        sys.stdout.write('\nRenaming: %s ---> %s' %(temp_env, current_env))
        os.rename(temp_env, current_env)

def getMayaHome(version):
    if 'MAYA_APP_DIR' in os.environ:
        mayaHome = os.environ['MAYA_APP_DIR'].replace('\\', '/')
    else:
        mayaHome = '%s/documents/maya' %(os.environ['USERPROFILE'].replace('\\', '/'))
        set_env('MAYA_APP_DIR', mayaHome)
        
    versionDir = '%s/%s' %(mayaHome, version)
    return versionDir

def set_env(var, value): 
    import subprocess
    import _subprocess
    startupinfo = subprocess.STARTUPINFO()
    startupinfo.dwFlags |= _subprocess.STARTF_USESHOWWINDOW
    p = subprocess.Popen(['SETX', var, value], startupinfo=startupinfo, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    print('%s env set to %s' %(var, value))

def forceEnv(source, version):
    import shutil
    from rf_utils import file_utils
    import datetime
    import getpass 
    from rf_utils.pipeline import user_collect
    reload(user_collect)

    user = os.environ['RFUSER']
    login = getpass.getuser()
    log_path = 'O:/Pipeline/logs/forceEnv/user_log.txt'
    jobkey = 'CopyEnv2'
    nameRegister = user_collect.check_extra_info(login, 'login')
    if not nameRegister: 
        import maya.cmds as mc 
        mc.evalDeferred('from rf_utils.pipeline import user_collect\nuser_collect.show()')

    if not user_collect.check_extra_info(login, jobkey): 
        with open(log_path, 'r') as stream:
            user_data = file_utils.ordered_load(stream)
        if not user_data:
            user_data = OrderedDict()

        if user in user_data:
            return
        else:
            mydocs = os.path.expanduser('~').replace('\\', '/')
            localEnv = '%s/maya/%s/Maya.env' %(mydocs, version)
            if os.path.exists(localEnv):
                shutil.copy(source, localEnv)
            user_collect.write_extra_info(login, jobkey, True)

            # collect time data
            now = datetime.datetime.now()
            user_data[user] = now.strftime("%m/%d/%Y, %H:%M:%S")

            # write
            file_utils.ymlDumper(log_path, user_data)


def collectUser():
    import shutil
    from rf_utils import file_utils
    import datetime
    import getpass 
    from rf_utils.pipeline import user_collect
    reload(user_collect)

    user = os.environ['RFUSER']
    login = getpass.getuser()
    nameRegister = user_collect.check_extra_info(login, 'login')
    if not nameRegister: 
        import maya.cmds as mc 
        mc.evalDeferred('from rf_utils.pipeline import user_collect\nuser_collect.show()')


def setup_render_environments(job_info, context):
    script_root = 'O:/Pipeline'
    # from maya.cmds import about
    # drive envs
    drive_envs = OrderedDict([('RFPROJECT', 'P:'), 
                            ('RFPUBL', 'P:'), 
                            ('RFPROD', 'P:'), 
                            ('RFVERSION', 'R:')])

    # project env 
    project = context.project
    department = context.department
    version = context.dcc_version
    with OverrideEnvironment(RFSCRIPT=script_root) as override_env:
        envs = setupEnv(project=project, department=department, version=version, 
                                mergeWithDefault=True, setEnvironment=False, writeToFile=False)
    envs = OrderedDict([(i[0], ';'.join(i[1])) for i in envs.items()])
    envs.update(drive_envs)
    for i, key in enumerate(envs):
        value = envs[key]
        job_info['EnvironmentKeyValue{}'.format(i)] = '{}={}'.format(key, value)

    return job_info
