import os

import maya.cmds as mc
import maya.mel as mel

from rf_utils import file_utils
import rf_config

def generate_empty_scene(path, version, project=''):
    config_dict = read_config(version, project)
    # currentUnit -l centimeter -a degree -t film;
    linear = config_dict['linear_unit']
    angle = config_dict['angle_unit']
    time = config_dict['time_unit']
    upAxis = config_dict['upAxis']
    lines = []
    lines.append('currentUnit -l {linear} -a {angle} -t {time};\n'.format(linear=linear, angle=angle, time=time))
    lines.append('upAxis -axis {upAxis} -rv;\n'.format(upAxis=upAxis))

    with open(path, 'w') as f:
        f.writelines(lines)

    return path

def read_config(version, project):
    env_dir = '%s/core/rf_env' %(os.environ['RFSCRIPT'])
    default_config_path = '%s/default/maya/%s/config/startup_config.yml' %(env_dir, version)

    # load default config
    default_config = None
    with open(default_config_path, 'r') as stream:
        default_config = file_utils.ordered_load(stream)
    # print('Default config: {}'.format(default_config))
    # load project config
    if project:
        proj_config_path = '%s/%s/maya/%s/config/startup_config.yml' %(env_dir, project, version)
        proj_config = None
        if os.path.exists(proj_config_path):
            # print('Found project config: {}'.format(%proj_config_path))
            with open(proj_config_path, 'r') as stream:
                proj_config = file_utils.ordered_load(stream)
            # print('Project config: {}'.format(proj_config))
        # override default config with project config
        if proj_config:
            print('Overriding project config...')
            default_config.update(proj_config)

    return default_config

def read_config_and_set(version, project=''):
    config_dict = read_config(version, project)
    print('Setting config: {}'.format(config_dict))

    for var, value in config_dict.iteritems():
        print('Executing: {}(value={})'.format(var, value))
        try:
            exec('%s(value=%s)' %(var, value))
        except Exception, e:
            print(e)
            print('Error applying startup config: {}'.format(var))

# ================================================
# ============= config functions =================
# ================================================

def time_unit(value):
    mc.currentUnit(time=value)
    mc.optionVar( iv=('keepCurrentWorkingUnits',1))

def linear_unit(value):
    mc.currentUnit(linear=value)
    mc.optionVar( iv=('keepCurrentWorkingUnits',1))

def angle_unit(value):
    mc.currentUnit(angle=value)
    mc.optionVar( iv=('keepCurrentWorkingUnits',1))

def time_unit(value):
    mc.currentUnit(time=value)
    mc.optionVar( iv=('keepCurrentWorkingUnits',1))

def selection_child_highlight_mode(value): 
    mc.optionVar(iv=('selectionChildHighlightMode', value))
    mc.selectPref(sch=value)

def upAxis(value):
    mc.upAxis(axis=value, rv=True)

def enable_colormanagement(value):
    if value:
        mc.evalDeferred('mc.colorManagementPrefs(e=True, cmEnabled=True)')
    else:
        mc.evalDeferred('mc.colorManagementPrefs(e=True, cmEnabled=False)')

def set_ocio_config(value):
    configFilePath = str(os.environ.get('OCIO'))
    if value and os.path.exists(configFilePath):
        # configFilePath = '{0}/{1}/config.ocio'.format(rf_config.Env.openColorIO, value)
        mc.evalDeferred('mc.colorManagementPrefs(e=True, configFilePath="{}")'.format(configFilePath))
        mc.evalDeferred('mc.colorManagementPrefs(e=True, cmConfigFileEnabled=True)')
        mc.evalDeferred('mc.colorManagementPrefs(e=True, ocioRulesEnabled=True)')
    else:
        mc.evalDeferred('mc.colorManagementPrefs(e=True, configFilePath="")')
        mc.evalDeferred('mc.colorManagementPrefs(e=True, cmConfigFileEnabled=False)')
        mc.evalDeferred('mc.colorManagementPrefs(e=True, ocioRulesEnabled=False)')
    
def set_viewtransform(value):
    configFilePath = str(os.environ.get('OCIO'))
    if value and os.path.exists(configFilePath):
        # Rec.709 (ACES)
        mc.evalDeferred('mc.colorManagementPrefs(e=True, viewTransformName="{}")'.format(value))

def set_redshift_rv_viewtransform(value):
    configFilePath = str(os.environ.get('OCIO'))
    if value and os.path.exists(configFilePath):
        try:
            # Output - Rec.709
            mc.evalDeferred('mc.setAttr("defaultRedshiftPostEffects.clrMgmtOcioViewTransform", "{}", type="string")'.format(value))  
        except Exception:
            pass

def set_input_colorspace(value):
    mc.colorManagementFileRules("Default", e=True, cs=value)

def set_transparency_algorithum(value): 
    mc.evalDeferred('mc.setAttr("hardwareRenderingGlobals.transparencyAlgorithm", {})'.format(value))
