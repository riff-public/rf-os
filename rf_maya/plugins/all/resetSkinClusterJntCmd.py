import maya.OpenMaya as om
import maya.OpenMayaAnim as oma
import maya.OpenMayaMPx as ompx

kPluginCmdName = "resetSkinClusterJnt"

# command
class ResetSkinClusterJntCmd(ompx.MPxCommand):
	def __init__(self):
		ompx.MPxCommand.__init__(self)
		self.oldValues = []
		self.bpmPlugs = []
		self.fnSkin = None
		self.selection = om.MSelectionList()

	def isUndoable(self):
		return True

	def undoIt(self):
		for plug, value in zip(self.bpmPlugs, self.oldValues):
			plug.setMObject(value)

		om.MGlobal.executeCommand('dgdirty %s;' %self.fnSkin.name())

	def doIt(self, args):
		om.MGlobal.getActiveSelectionList(self.selection)

		if self.selection.isEmpty():
			argData = om.MArgDatabase(self.syntax(), args)
			argData.getObjects(self.selection)

		self.redoIt()

	def redoIt(self):		
		# get dag path for selection
		dagPath = om.MDagPath()
		try:
			self.selection.getDagPath(0, dagPath)
			dagPath.extendToShape()
		except RuntimeError, e:
			print e
			om.MGlobal.displayError('Please select a skinned mesh.')
			return
		
		# get skincluster from shape
		itSkin = om.MItDependencyGraph(dagPath.node(), 
			om.MFn.kSkinClusterFilter, 
			om.MItDependencyGraph.kUpstream, 
			om.MItDependencyGraph.kBreadthFirst, 
			om.MItDependencyGraph.kPlugLevel)

		skcMObj = None
		while not itSkin.isDone():
			# current MObject in the iteration
			skcMObj = itSkin.currentItem()

			# here's our skinCluster node
			self.fnSkin = oma.MFnSkinCluster(skcMObj)

			# cast current MObj to MFnDependencyNode to get attribute value
			mFnDependSkinCluster = om.MFnDependencyNode(skcMObj)

			# done, break out
			break

		# check for skinCluster
		if not self.fnSkin:
			om.MGlobal.displayError('Cannot find skinCluster node.')
			return

		# get joints
		mDagPaths = om.MDagPathArray()
		self.fnSkin.influenceObjects(mDagPaths)
		bpmParentPlug = self.fnSkin.findPlug('bindPreMatrix')

		for i in xrange(mDagPaths.length()):
			index = self.fnSkin.indexForInfluenceObject(mDagPaths[i])

			# get joint world inverse matrix
			fnDag = om.MFnDagNode(mDagPaths[i])
			wimMatMObj = fnDag.findPlug('worldInverseMatrix').elementByLogicalIndex(0).asMObject()

			bpmPlug = bpmParentPlug.elementByLogicalIndex(index)
			oldValue = bpmPlug.asMObject()

			self.bpmPlugs.append(bpmPlug)
			self.oldValues.append(oldValue)

			bpmPlug.setMObject(wimMatMObj)

		# refresh the dg
		om.MGlobal.executeCommand('dgdirty %s;' %self.fnSkin.name())

# Creator
def cmdCreator():
	# Create the command
	return ompx.asMPxPtr(ResetSkinClusterJntCmd())

# Syntax creator
def syntaxCreator():
	syntax = om.MSyntax()
	syntax.setObjectType(om.MSyntax.kSelectionList, 0)
	return syntax

# Initialize the script plug-in
def initializePlugin(mobject):
	mplugin = ompx.MFnPlugin(mobject, "Nuternativ", "1.0", "Any")
	try:
		mplugin.registerCommand(kPluginCmdName, cmdCreator, syntaxCreator)
	except:
		sys.stderr.write( "Failed to register command: %s\n" % kPluginCmdName )
		raise

# Uninitialize the script plug-in
def uninitializePlugin(mobject):
	mplugin = ompx.MFnPlugin(mobject)
	try:
		mplugin.deregisterCommand(kPluginCmdName)
	except:
		sys.stderr.write( "Failed to unregister command: %s\n" % kPluginCmdName )
		raise

	
