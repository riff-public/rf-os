import pymel.core as pm
import maya.cmds as cmds
import mtoa.ui.ae.templates as templates
import mtoa.core as core

#import mtoa.utils as utils
#import mtoa.ui.ae.utils as aeUtils
#from mtoa.ui.ae.shaderTemplate import ShaderAETemplate

import subprocess
from subprocess import call

class FFXDynaTemplate(templates.ShapeTranslatorTemplate):

    def setup(self):
        self.addControl("aiPhaseFunc", label="Phase Function Anisotropy")
        self.addSeparator()
        self.addControl("aiVisibleInDiffuse", label="Visible In Diffuse")
        self.addControl("aiVisibleInGlossy",  label="Visible In Glossy")
        self.addSeparator()
        self.addControl("aiGIAtten", label="GI Attenuation")
        self.addSeparator()
        self.addControl("aiGridMoblur", label="Grid Motion Blur")
        #self.addSeparator()
        #self.addControl("aiUserOptions", label="User Options")

templates.registerAETemplate(FFXDynaTemplate, "ffxDyna")
