import os
import sys
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import maya.app.renderSetup.model.override as override
import maya.app.renderSetup.model.selector as selector
import maya.app.renderSetup.model.collection as collection
import maya.app.renderSetup.model.connectionOverride as connectionOverride
import maya.app.renderSetup.model.renderLayer as renderLayer
import maya.app.renderSetup.model.renderSetup as renderSetup
import maya.cmds as mc

def create_renderLayer(name=''): 
	""" create render layer """ 
	rs = renderSetup.instance()    
	layer = rs.createRenderLayer(name)
	return layer


def create_collection(layer, name=''): 
	""" create collection under given renderLayer object or renderLayer name """ 
	layer = get_renderLayer(layer)
	col = layer.createCollection(name)
	return col


def list_renderLayers(): 
	""" list all render layers """ 
	rs = renderSetup.instance()
	return rs.getRenderLayers()


def list_collections(layer): 
	""" list collections as collection object """ 
	layer = get_renderLayer(layer)
	return layer.getCollections()


def add_static_collection(layer, collectionName, objs=[]): 
	""" add static objects to collection """ 
	objects = format_objs(objs)
	collectionName = get_collection(layer, collectionName)
	collectionName.getSelector().setStaticSelection(objects)
	return collectionName

def add_dynamic_collection(layer, collectionName, objs=[]): 
	""" add dynamic objects to collection """ 
	objects = format_objs(objs, ln=False)
	collectionName = get_collection(layer, collectionName)
	collectionName.getSelector().setPattern(objects)
	return collectionName	

def create_material_override(layer, collectionName, name='MaterialOverride', shadingEngine=None): 
	""" create material override for collection and override with given material """ 
	collectionName = get_collection(layer, collectionName)
	ovrd = collectionName.createOverride(name, "materialOverride")
	if shadingEngine: 
		ovrd.setMaterial(shadingEngine)
	return ovrd

def set_material_override(layer, collectionName, overrideName, shadingEngine): 
	""" set material override for collection """ 
	overrideName = get_override(layer, collectionName, overrideName)
	overrideName.setMaterial(shadingEngine)
	return overrideName


def get_renderLayer(layer): 
	""" convert layer to renderLayer object """ 
	if not isinstance(layer, renderLayer.RenderLayer): 
		rs = renderSetup.instance()  
		layer = rs.getRenderLayer(layer)
	return layer 


def get_collection(layer, collectionName): 
	""" convert collection to collection object """ 
	layer = get_renderLayer(layer)
	if not isinstance(collectionName, collection.Collection): 
		collectionName = layer.getCollectionByName(collectionName)
	return collectionName

def get_override(layer, collectionName, overrideName): 
	if not isinstance(overrideName, connectionOverride.MaterialOverride): 
		collectionName = get_collection(layer, collectionName)
		ovrds = collectionName.getOverrides()
		targetNodes = [a for a in ovrds if a.name() == overrideName]
		if targetNodes: 
			overrideName = targetNodes[0]
	return overrideName


def format_objs(objs, exists=True, ln=True): 
	""" string format for collection """ 
	strFormat = ' '.join([mc.ls(a, l=ln)[0] for a in objs if mc.objExists(a)])
	return strFormat
	

