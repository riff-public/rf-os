import os
import sys
import shutil
import maya.cmds as mc
import maya.mel as mm
import pymel.core as pm 
from rf_utils.pipeline import local_hook
reload(local_hook)
from rf_utils.context import context_info
from rftool.utils import maya_utils
reload(maya_utils)

def save_as(filename, type='mayaAscii'):
    localDst = local_hook.local_name(filename)

    # maya save
    mc.file(rename=localDst)
    localfile = mc.file(save=True, f=True, type=type)

    # move back
    shutil.copy2(localfile, filename)
    return mc.file(rename=filename)

def export_selection(filename, objs, fileType='mayaAscii', exportShade=True):
    print 'objs', objs
    localDst = local_hook.local_name(filename)
    objs = [pm.PyNode(a) for a in objs if mc.objExists(a)]
    if objs:
        # this command move objs to world first then reparent back
        with maya_utils.MoveToWorld(objs): 
            pm.select(objs)
            if not os.path.exists(os.path.dirname(localDst)): 
                os.makedirs(os.path.dirname(localDst))
            
            localFile = mc.file(filename, force=True, options='v=0', typ=fileType, pr=True, es=True, sh=exportShade)
            # shutil.copy2(localFile, filename)
        return filename

def import_file(filename):
    return mc.file(filename,  i=True, type='mayaAscii', options='v=0', pr=True, loadReferenceDepth='all')


def export_alembic_geo(filename, options):
    localDst = local_hook.local_name(filename)
    if not os.path.exists(os.path.dirname(localDst)):
        os.makedirs(os.path.dirname(localDst))

    options.append('-file %s' % localDst)
    optionCmd = (' ').join(options)
    cmd = 'AbcExport -j "%s";' % optionCmd
    result = mm.eval(cmd)
    shutil.copy2(localDst, filename)

    if os.path.exists(filename):
        return filename

def export_fbx(filename, objs, fileType='FBX export'):
    print 'objs', objs
    localDst = local_hook.local_name(filename)
    objs = [pm.PyNode(a) for a in objs if mc.objExists(a)]
    if objs:
        # this command move objs to world first then reparent back
        with maya_utils.MoveToWorld(objs): 
            pm.select(objs)
            if not os.path.exists(os.path.dirname(localDst)): 
                os.makedirs(os.path.dirname(localDst))
            
            localFile = mc.file(filename ,force=True , options="v=0;" ,typ=fileType ,pr=True , es=True, con=True, chn=True, sh=False, ch=False, exp=False)
            # shutil.copy2(localFile, filename)
        return filename


def create_reference(entity, name, path, material=True):
    from rftool.shade import shade_utils
    reload(shade_utils)
    from rftool.utils import maya_utils
    print entity, name, path
    shadeNamespace = entity.mtr_namespace
    namespace = maya_utils.create_reference(name, path, force=True)
    # defalut material
    mtrFile = entity.library(context_info.Lib.rigMtr)
    mtr = '%s/%s' % (entity.path.hero().abs_path(), mtrFile)
    mtrRel = '%s/%s' % (entity.path.hero(), mtrFile)

    if material and os.path.exists(mtr):
        shade_utils.apply_ref_shade(mtrRel, shadeNamespace, namespace)

def reference_file(namespace, path):
    from rf_maya.lib import abc_lib
    reload(abc_lib)
    abc_lib.check_plugin(abc_lib.Plugin.abcImport)
    return mc.file(path, r=True, ns=namespace)

def import_abc(path):
    from rf_maya.lib import abc_lib
    reload(abc_lib)
    return abc_lib.import_abc(path)

def import_mtr(targetGrp, path):
    from rftool.shade import shade_utils
    reload(shade_utils)
    obj = mc.ls(sl=True)[0] if mc.ls(sl=True) else ''
    namespace = (':').join(obj.split(':')[:-1]) if ':' in obj else None
    targetNamespace = namespace if namespace else guess_target_namespace(targetGrp)
    return shade_utils.apply_import_shade(path, targetNamespace=targetNamespace)
