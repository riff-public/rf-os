#!/usr/bin/env python
# -- coding: utf-8 --
import os 
import sys 
import logging
import maya.cmds as mc 
import pymel.core as pm 

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

class Config: 
	MiarmyReady = 'Miarmy_Contents'
	Agent = 'Agent_loco'
	Placement = 'Placement_Set'

class Agent: 
	Setup = 'Setup_loco'
	OriginalAgent = 'OriginalAgent_loco'
	Action = 'Action_loco'
	StoryList = 'StoryList_loco'
	TransitionMap = 'TransitionMap_loco'
	Decision = 'Decision_loco'
	Geometry = 'Geometry_loco'
	LODMid = 'LODMid_loco'
	LODFar = 'LODFar_loco'
	PlacerShape = 'PlacerShape_loco'
	RandomShader = 'RandomShader_loco'
	FootMap = 'FootMap_loco'
	Misc = 'Misc_loco'


def list_mc(): 
	contents = []
	if mc.objExists(Config.MiarmyReady): 
		contents = mc.listRelatives(Config.MiarmyReady)
	return contents

def list_agent(): 
	contents = []
	if mc.objExists(Config.Agent): 
		contents = mc.listRelatives(Config.Agent)
	return contents

def ls_rsproxy(): 
	return mc.ls(type='McdRSProxy')

def export_rsproxy(dst): 
	keepContents = [Agent.Geometry, Agent.RandomShader]
	objects = ls_rsproxy() + [Config.MiarmyReady]

	with IsolateContents(keepContents): 
		mc.select(objects)
		mc.file(dst, f=True, es=True, type='mayaAscii')



class IsolateContents(object):
	"""docstring for IsolateContents"""
	def __init__(self, targets):
		super(IsolateContents, self).__init__()
		self.targets = targets 
		self.moveInfo = dict()
		self.tmpGrpSuffix = 'mtmp'
		self.checkContents = list_agent()

	def __enter__(self): 
		""" move contents out of agents """ 
		for content in self.checkContents: 
			if not content in self.targets: 
				parent = pm.PyNode(content)
				
				tmpGrp = self.tmp_grp(content)
				contents = pm.listRelatives(content, c=True)

				if contents: 
					for content in contents: 
						pm.parent(content, tmpGrp)
						self.moveInfo[content] = {'original': parent, 'tmp': tmpGrp}

		self.isolate_placement()

	def __exit__(self, *args): 
		""" move back to original location """ 
		if self.moveInfo: 
			for content, data in self.moveInfo.iteritems(): 
				pm.parent(content, data['original'])

			grps = mc.ls('*_%s' % self.tmpGrpSuffix)
			mc.delete(grps) if grps else None


	def tmp_grp(self, content): 
		tmpGrp = '%s_%s' % (content, self.tmpGrpSuffix)
		if not pm.objExists(tmpGrp): 
			pm.group(em=True, n=tmpGrp)
				
		tmpGrp = pm.PyNode(tmpGrp)
		return tmpGrp

	def isolate_placement(self): 
		tmpGrp = self.tmp_grp(Config.Placement)
		parent = pm.listRelatives(Config.Placement, p=True)
		if parent: 
			pm.parent(Config.Placement, tmpGrp)
			self.moveInfo[Config.Placement] = {'original': parent[0], 'tmp': tmpGrp}


# usage examples 
# miarmy_lib.export_rsproxy('D:/export_miarmy.ma')



		
