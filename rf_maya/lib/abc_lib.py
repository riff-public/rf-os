import os
import sys
import maya.cmds as mc
import maya.mel as mm
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

class Plugin:
    abcExport = 'AbcExport.mll'
    abcImport = 'AbcImport.mll'

def import_abc(path):
    """ import alembic file """
    check_plugin(Plugin.abcExport)
    cmd = 'AbcImport -mode import "%s";' % path
    result = mm.eval(cmd)
    return path

def check_plugin(pluginName):
    """ load plugins """
    if not mc.pluginInfo(pluginName, q=True, l=True):
        mc.loadPlugin(pluginName, qt=True)


def reference_abc(namespace, path, rebuild=True):
    if not mc.namespace(ex=namespace):
        result = mc.file(path, r=True, type='Alembic', ns=namespace, f=True)
        logger.info('Reference abc %s' % path)
    else:
        namespaceDict = get_ref_namespaces()
        info = namespaceDict.get(':%s' % namespace)
        if info:
            currentPath, rnNode = info
            if rebuild:
                mc.file(currentPath, rr=True)
                result = mc.file(path, r=True, type='Alembic', ns=namespace, f=True)
                return result
            result = mc.file(path, loadReference=rnNode, prompt=False)
            if currentPath.split('{')[0] == path:
                logger.info('Reload reference %s' % path)
            else:
                logger.info('Replace reference %s -> %s' % (currentPath, path))
            return result
        else:
            logger.warning('Namespace is not a reference node, failed to use this namespace "%s"' % namespace)


def import_rig(namespace, path):
    if mc.namespace(ex=namespace):
        namespaceDict = get_ref_namespaces()
        info = namespaceDict.get(':%s' % namespace)

        if info:
            currentPath, rnNode = info
            mc.file(currentPath, rr=True)
        else:
            objs = mc.ls('%s*:*' % namespace)
            mc.delete(objs)

    return mc.file(path, i=True, type='mayaAscii', f=True, pr=True)


def get_ref_namespaces():
    paths = mc.file(q=True, r=True)
    namespaceDict = dict()
    for path in paths:
        namespace = mc.referenceQuery(path, ns=True)
        rnNode = mc.referenceQuery(path, referenceNode=True)
        namespaceDict[namespace] = [path, rnNode]
    return namespaceDict


def build_abc(scene, step, namespace, version, rebuild=False):
    scene.context.update(step=step, process=namespace, publishVersion=version)
    cachePath = scene.path.scheme(key='cachePath').abs_path()
    files = list_file(cachePath, ['.abc'])
    return reference_abc(namespace, files[-1], rebuild) if files else None


def build_rig(scene, step, namespace, version):
    scene.context.update(step=step, process=namespace, publishVersion=version)
    cachePath = scene.path.scheme(key='cachePath').abs_path()
    files = list_file(cachePath, ['.ma'])
    return import_rig(namespace, files[-1])


def list_file(path, ext=[]):
    files = ['/'.join([path, d]) for d in os.listdir(path) if os.path.isfile('/'.join([path, d]))]
    if ext:
        return [a for a in files if os.path.splitext(a)[-1] in ext]
    return files


# def switch(namespace, cache=False, rig=False):
#     scene = context_info.
