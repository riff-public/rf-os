# import all modules in maya_lib 
import os
import sys 
import glob
from rf_maya.lib import *
from collections import OrderedDict
from inspect import getmembers, isfunction
from rf_maya.lib import maya_lib
from rf_utils import custom_exception
reload(maya_lib)


def get_module(): 
    modules = glob.glob(os.path.dirname(__file__)+"/*.py")
    return [ os.path.basename(f)[:-3] for f in modules if os.path.isfile(f) and not f.endswith('__init__.py')]

def get_module_data(): 
    mods = OrderedDict()
    allModules = get_module()
    
    for name in allModules: 
        func = __import__(name, globals(), locals(), [], -1)
        reload(func)
        mods[name] = func

    return mods


def run(name): 
	functionDict = list_function_dict()
	if name in functionDict.keys(): 
		return functionDict[name]()
	else: 
		raise custom_exception.PipelineError('No function name "%s" in "%s"' % (name, maya_lib.__file__))

def list_function_name(): 
	functionsList = [o for o in getmembers(maya_lib) if isfunction(o[1])]
	return [name for name, func in functionsList]

def list_function_dict(): 
	functionsList = [o for o in getmembers(maya_lib) if isfunction(o[1])]
	functionDict = dict()

	for name, func in functionsList: 
		functionDict[name] = func 
	
	return functionDict

        

