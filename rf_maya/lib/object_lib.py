import maya.cmds as mc 

def snap(obj1, obj2, t=True, r=True): 
    """ snap obj1 to obj2, t = translate, r = rotate """ 
    if t and r: 
        mc.delete(mc.parentConstraint(obj2, obj1))
    elif t: 
        mc.delete(mc.pointConstraint(obj2, obj1))
    elif r: 
        mc.delete(mc.orientCosntraint(obj2, obj1))


def do_snap(): 
    """ select obj then snap """ 
    sels = mc.ls(sl=True)
    if sels: 
        target = sels[-1]

        for sel in sels: 
            if not sel == target: 
                snap(sel, target)


def get_distance_from_center(obj, axis): 
    """ 
         -----  <- max
        |  .  | <- object center
         -----  <- min 
    """ 
    pos = mc.xform(obj, q=True, ws=True, t=True)
    x, y, z = pos 

    bbMin = mc.getAttr('%s.boundingBoxMin' % obj)
    bbMax = mc.getAttr('%s.boundingBoxMax' % obj)

    bbMinX = bbMin[0][0]
    bbMinY = bbMin[0][1]
    bbMinZ = bbMin[0][2]

    bbMaxX = bbMax[0][0]
    bbMaxY = bbMax[0][1]
    bbMaxZ = bbMax[0][2]

    if axis == 'x': 
        value = bbMaxX - x 
    if axis == '-x': 
        value = x - bbMinX
    if axis == 'y': 
        value = bbMaxY - y 
    if axis == '-y': 
        value = y - bbMinY
    if axis == 'z': 
        value = bbMaxZ - z
    if axis == '-z': 
        value = z - bbMinZ
    return value 

def snap_bounding(obj, target, axis=(0, 1, 0), center=False, offset=0): 
    """ snap obj to target """ 
    # move obj up by obj lower + target up 
    x, y, z = axis 
    pos = mc.xform(target, q=True, ws=True, t=True)
    posX, posY, posZ = pos 

    if center: 
        mc.delete(mc.parentConstraint(target, obj))

    if y == 1: 
        offsetValue = y*offset if offset else 0
        targetUp = get_distance_from_center(target, 'y')
        objLow = get_distance_from_center(obj, '-y')
        value = posY + targetUp + objLow + offsetValue
        mc.setAttr('%s.ty' % obj, value)

    if y == -1: 
        offsetValue = y*offset if offset else 0
        targetLo = get_distance_from_center(target, '-y')
        objUp = get_distance_from_center(obj, 'y')
        value = posY - targetLo - objUp - offsetValue
        mc.setAttr('%s.ty' % obj, value)

    if x == 1: 
        offsetValue = x*offset if offset else 0
        targetR = get_distance_from_center(target, 'x')
        objL = get_distance_from_center(obj, '-x')
        value = posX + targetR + objL + offsetValue
        mc.setAttr('%s.tx' % obj, value)

    if x == -1: 
        offsetValue = x*offset if offset else 0
        targetL = get_distance_from_center(target, '-x')
        objR = get_distance_from_center(obj, 'x')
        value = posX - targetL - objR - offsetValue
        mc.setAttr('%s.tx' % obj, value)

    if z == 1: 
        offsetValue = z*offset if offset else 0
        targetF = get_distance_from_center(target, 'z')
        objD = get_distance_from_center(obj, '-z')
        value = posZ + targetF + objD + offsetValue
        mc.setAttr('%s.tz' % obj, value)

    if z == -1: 
        offsetValue = z*offset if offset else 0
        targetD = get_distance_from_center(target, '-z')
        objF = get_distance_from_center(obj, 'z')
        value = posZ - targetD - objF - offsetValue
        mc.setAttr('%s.tz' % obj, value)


def do_snap_bounding(axis=(0, 1, 0), center=True, offset=0): 
    """ select objects to snap surface """ 
    sels = mc.ls(sl=True)
    if sels: 
        target = sels[-1]
        for sel in sels: 
            if not sel == target: 
                snap_bounding(sel, target, axis, center=center, offset=offset)


def do_stack(axis=(0, 1, 0), center=True, offset=0): 
    """ stack objects """ 
    sels = mc.ls(sl=True)
    if sels: 
        for i, sel in enumerate(sels[::-1][:-1]): 
            snap_bounding(sels[i+1], sels[i], axis, center=center, offset=offset)
