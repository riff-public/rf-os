import os
import sys
import logging
import maya.cmds as mc
import maya.mel as mm

from rf_utils.context import context_info
from rf_utils import project_info
from rf_maya.rftool.utils import maya_utils

def create_shot(name, camera, startTime=1001, endTime=1030, sequenceStartTime=None, sequenceEndTime=None):
    sequenceStartTime = startTime if not sequenceStartTime else sequenceStartTime
    sequenceEndTime = endTime if not sequenceEndTime else sequenceEndTime

    if mc.objExists(camera):
        return mc.shot(name, startTime=startTime, endTime=endTime, sequenceStartTime=sequenceStartTime, sequenceEndTime=sequenceEndTime, currentCamera=camera)

def edit_shot(name, camera=None, startTime=None, endTime=None, sequenceStartTime=None, sequenceEndTime=None):
    st, sqst, et, sqst = shot_range_info(name)


def shot_range_info(name):
    startFrame = mc.getAttr('%s.startFrame' % name)
    endFrame = mc.getAttr('%s.endFrame' % name)
    sequenceStartFrame = mc.getAttr('%s.sequenceStartFrame' % name)
    sequenceEndFrame = mc.getAttr('%s.sequenceEndFrame' % name)

    return [startFrame, endFrame, sequenceStartFrame, sequenceEndFrame]


def create_cam(shot_name=None, startFrame=None, duration=None, createSequencer=True): 
    scene = context_info.ContextPathInfo()
    if not startFrame: 
        startFrame = scene.projectInfo.scene.start_frame or 1001

    inPipe = True if scene.project else False 
    
    # use default if not in the system 
    project = scene.project if inPipe else 'SevenChickMovie'
    projectInfo = project_info.ProjectInfo(project)

    # get global camera 
    cameraPath = projectInfo.asset.global_asset().get('cam')

    # createSequencer = True 
    # in the pipeline 
    if inPipe: 
        if scene.entity_type == context_info.ContextKey.scene: 
            if not shot_name: 
                shot_name = scene.name_code
            namespace = '%s_cam' % shot_name
            seqName = shot_name

        if scene.entity_type == context_info.ContextKey.asset: 
            if not shot_name: 
                shot_name = scene.name
            namespace = '%s_cam' % shot_name
            seqName = shot_name
    
    # if not in the pipeline 
    if not inPipe: 
        namespace = ''
        result = mc.promptDialog(title='Scene not in the pipeline',
                                    message='Enter camera namespace:',
                                    button=['Create Camera', 'Custom', 'Cancel'],
                                    )
        if result == 'Create Camera':
            createSequencer = True 
            seqName = mc.promptDialog(query=True, text=True)
            namespace = '%s_cam' % seqName

        if result == 'Custom': 
            createSequencer = False 
            namespace = mc.promptDialog(query=True, text=True)

    if namespace: 
        if not mc.namespace(ex=namespace): 
            refPath = mc.file(cameraPath, r=True, ns=namespace)
            namespace = mc.referenceQuery(refPath, ns=True)
            lsCams = mc.ls('%s:*' % namespace[1:], type='camera')
            skipListKey = ['UE4'] # remove camera with these keywords

            cameras = []
            for camShape in lsCams: 
                if any(a in camShape for a in skipListKey): 
                    continue
                cameras.append(camShape)

                # lock camera pivots
                cam_tr = mc.listRelatives(camShape, parent=True, pa=True)[0]
                maya_utils.lockPivots(cam_tr)

            if createSequencer: 
                if not duration: 
                    duraionResult = mc.promptDialog(title='Enter Duration',
                                        message='Enter Duration:',
                                        button=['OK'], 
                                        tx='100'
                                        )
                    durationStr = mc.promptDialog(query=True, text=True)
                    if durationStr.isdigit(): 
                        duration = int(durationStr)
                    else: 
                        duration = 100 
                        mm.eval('warning "default duration set to 100";')
                create_shot(seqName, cameras[0], startTime=startFrame, endTime=(startFrame + duration - 1), sequenceStartTime=None, sequenceEndTime=None)
        
        else: 
            mc.confirmDialog(title='Warning', 
                            message='Camera exists "%s" Cannot create camera' % namespace, 
                            button=['OK']
                            )


def create_camera(shotName, projectName=''): 
    """ create camera only """  
    # use default if not in the system 
    skipListKey = ['UE4']
    projectInfo = project_info.ProjectInfo(projectName)

    # get global camera 
    cameraPath = projectInfo.asset.global_asset().get('cam')

    namespace = '{}_cam'.format(shotName)

    if not mc.namespace(ex=namespace): 
        refPath = mc.file(cameraPath, r=True, ns=namespace)
        namespace = mc.referenceQuery(refPath, ns=True)[1:]
    
    cams = mc.ls('%s:*' % namespace, type='camera')
    cameras = []
    if cams: 
        for cam in cams: 
            if any(a in cam for a in skipListKey): 
                continue
            cameras.append(cam)
            # lock camera pivots
            cam_tr = mc.listRelatives(cam, parent=True, pa=True)[0]
            maya_utils.lockPivots(cam_tr)
            
    return cameras[0] if cameras else None

# isolate camera 
class PrepCamera(object):
    """ Unparent camera to world and rename to match pipeline then restore """
    def __init__(self, camera, targetName='camshot'):
        super(PrepCamera, self).__init__()
        self.camera = camera

    def __enter__(self): 
        pass 

    def __exit__(self, *args): 
        pass 

