import maya.cmds as mc 
import os 
import sys 

def import_ref(): 
    exclude = ['_UNKNOWN_REF_NODE_', 'sharedReferenceNode']
    rnNodes = mc.ls(type='reference')
    rnNodes = [a for a in rnNodes if not a in exclude] if rnNodes else []

    for rnNode in rnNodes: 
        path = mc.referenceQuery(rnNode, f=True)
        mc.file(path, importReference=True)

    excludeNs = ['UI', 'shared']
    namespaces = mc.namespaceInfo(listOnlyNamespaces=True)
    namespaces = [a for a in namespaces if not a in excludeNs] if namespaces else []

    for namespace in namespaces: 
        mc.namespace(f=True, mv=(':%s' % namespace, ':'))
        mc.namespace(removeNamespace=namespace)
        
