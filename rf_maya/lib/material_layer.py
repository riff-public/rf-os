from collections import OrderedDict
import maya.cmds as mc
import os
import sys
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

class Config: 
	preview = 'preview'
	texture = 'texture'
	lookdev = 'lookdev'
	tech = 'tech'

	lookIndex = 0 
	nameIndex = 1 

	mtrKey = {	'default': {
							preview: {'step': 'model', 'outputKey': 'materialRig'},
							texture: {'step': 'texture', 'outputKey': 'material'}, 
							lookdev: {'step': 'lookdev', 'outputKey': 'material'},
							tech: {'step': 'texture', 'outputKey': 'materialTech'}
							}
			}

def list_layers(mode='default'): 
	""" list layers only in look format """ 
	layers = mc.ls(type='renderLayer')
	skip = ['defaultRenderLayer']
	layerInfo = OrderedDict()
	checkLayers = [Config.preview, Config.texture, Config.lookdev, Config.tech]
	
	if mode == 'lookdev': 
		checkLayers = [Config.texture, Config.lookdev]

	if mode == 'rig': 
		checkLayers = [Config.preview ,Config.lookdev]

	if layers: 
		for layer in layers: 
			valid = all(not a in layer for a in skip)
			nameValid = any(a in layer for a in checkLayers)
			
			if valid and nameValid: 
				look = layer.split('_')[Config.lookIndex]
				materialType = layer.split('_')[Config.nameIndex]
				status = mc.getAttr('%s.renderable' % layer)
				layerInfo[layer] = {'renderable': status, 'look': look, 'materialType': materialType}
	return layerInfo 


def list_layer_info(entity, step='default'): 
	info = list_layers() 
	layerInfo = dict()

	for layerName, data in info.iteritems(): 
		look = data['look']
		materialType = data['materialType']
		materialData = Config.mtrKey[step][materialType]
		layerStep = materialData['step']
		outputKey = materialData['outputKey']

		# setup context 
		entity.context.update(look=look, step=layerStep)
		entity.context.update(res='md') if not entity.res else None
		heroDir = entity.path.hero().abs_path()
		file = entity.output_name(outputKey=outputKey, hero=True)
		mtrFile = '%s/%s' % (heroDir, file)
		data.update({'path': mtrFile})
		layerInfo[layerName] = data

	return layerInfo


def list_layer_mtrs(): 
	data = list_layers()
	for k, v in data.iteritems(): 
		print k, v


def list_looks(): 
	""" list looks """
	layerInfo = list_layers()
	looks = list(set([v['look'] for k, v in layerInfo.iteritems()])) if layerInfo else []
	return looks 

def active_look(): 
	""" get active look """ 
	layerInfo = list_layers()
	return list(set([v['look'] for k, v in layerInfo.iteritems() if v['renderable']] if layerInfo else []))

def active_type(look): 
	""" get active type """
	layerInfo = list_layers()
	return list(set([v['materialType'] for k, v in layerInfo.iteritems() if v['look'] == look and v['renderable']] if layerInfo else []))


def list_export_type(look): 
	""" list export materialType """ 
	layerInfo = list_layers()
	return [v['materialType'] for k, v in layerInfo.iteritems() if v['look'] == look and v['renderable']]

def set_active_layer(look, materialType): 
	""" set active layers """ 
	layerInfo = list_layers()
	name = [k for k, v in layerInfo.iteritems() if v['look'] == look and v['materialType'] == materialType]
	mc.editRenderLayerGlobals(currentRenderLayer=name[0]) if name else None

def get_active_info(): 
	info = OrderedDict()
	activeLooks = active_look()
	for look in activeLooks: 
		materialType = active_type(look)
		if not look in info.keys(): 
			info[look] = [materialType]
		else: 
			info[look].append(materialType)
	return info

def get_preview_look(): 
	layerInfo = list_layers()
	activeLooks = active_look()
	return [a for a in activeLooks if Config.preview in active_type(a)]

def get_texture_look(): 
	layerInfo = list_layers()
	activeLooks = active_look()
	return [a for a in activeLooks if Config.texture in active_type(a)]

def get_lookdev_look(): 
	layerInfo = list_layers()
	activeLooks = active_look()
	return [a for a in activeLooks if Config.lookdev in active_type(a)]

def get_tech_look(): 
	layerInfo = list_layers()
	activeLooks = active_look()
	return [a for a in activeLooks if Config.tech in active_type(a)]

def create_layer(look, mode='default'): 
	target = 'Export_Grp'
	materialTypes = [Config.preview, Config.texture, Config.tech]
	if mode in ['anim', 'lookdev']: 
		target = 'Export_Grp'
		materialTypes = [Config.lookdev]
	if mode == 'groom': 
		target = 'Export_Grp'
		materialTypes = [Config.texture]
	if mode == 'rig': 
		targets = mc.ls('*:Rig_Grp', type='transform')
		if targets:
			target = targets[0]
		else:
			target = 'Rig_Grp'
		materialTypes = [Config.preview, Config.texture ,Config.lookdev]

	for materialType in materialTypes: 
		layerName = layer_name(look, materialType)
		if not mc.objExists(layerName): 
			if not mc.objExists(target): 
				mc.createRenderLayer(name=layerName, number=1, noRecurse=True)
			else: 
				mc.createRenderLayer(target, name=layerName, number=1, noRecurse=True)

		# set disable for non lookdev layer 
		if mode == 'lookdev': 
			if not materialType == Config.lookdev: 
				mc.setAttr('%s.renderable' % layerName, 0)
				print 'set non renderable', layerName

def layer_name(look, layerType): 
	if layerType == 'preview': 
		materialType = Config.preview
	if layerType == 'texture': 
		materialType = Config.texture
	if layerType == 'lookdev': 
		materialType = Config.lookdev
	if layerType == 'tech': 
		materialType = Config.tech

	layerName = '%s_%s' % (look, materialType)
	return layerName

def layer_name_info(layerName): 
	data = dict()
	elem = layerName.split('_')
	if len(elem) == 2: 
		look = elem[Config.lookIndex]
		materialType = elem[Config.nameIndex]

		if materialType in [Config.preview, Config.texture, Config.lookdev]: 
			data = {'look': look, 'type': materialType}
	return data 


def get_active_material(look): 
	activeTypes = active_type(look)
	if Config.lookdev in activeTypes: 
		return Config.lookdev 
	if Config.texture in activeTypes: 
		return Config.texture 

def get_current_layer(): 
	return mc.editRenderLayerGlobals(q=True, currentRenderLayer=True)

def show_all_geo(layer):
	exportGrp = 'Export_Grp'
	stuffInLayer = mc.editRenderLayerMembers( layer, q = True)
	if exportGrp in stuffInLayer: 
		members = mc.listRelatives(exportGrp, c=True)
		stuffInLayer += members

	ns = ''
	for stuff in stuffInLayer:
		if ':' in stuff:
			nsStuff = stuff.split(':')[0]
			if not ns and ns != nsStuff:
				ns = nsStuff
				allGeos = mc.listRelatives('{}:Geo_Grp'.format(ns), allDescendents=True)
				for geo in allGeos:
					if mc.objExists('{}.v'.format(geo)):
						conn = mc.listConnections(geo + ".v" ,s = 1 , p = 1) 
						if conn : 
							mc.disconnectAttr(conn[0] , geo + ".v")

					mc.showHidden(geo, above=True)