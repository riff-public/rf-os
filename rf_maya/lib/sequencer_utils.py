import logging 
from collections import OrderedDict 
import maya.cmds as mc 
import pymel.core as pm
from rf_utils import project_info
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

# CRUD 

# Frame space between shot (Config, args)

# timeline 

# Create 
# Create at the end of the timeline 
# Create at the start of the timeline + ripple push and shift all shot on the timeline
# Create after selected shot + ripple push and shift all shots after selected shot
# Create before selected shot + ripple push and shift all shots after selected

# Read 
# current shot duration 
# shot sg_cut_duration (xml)
# report and choice to sync 

# Update
# Extend tail + ripple push shots after selected shot and shift keyframe point after end of selected shot
# Trim tail + just trim no action needed
# Extend head = keep start frame the same, move affected shots to n amount of frame and extend shot to the same shot startframe 
# Trim head = just trim no action needed
# merge shot = keep the first one and delete the rest and extend first shot to sum duration of deleted shot
# split shot = create new shot, shink first one

# Detele
# Delete sequencer + remove camera


def create_shot(shot_name, camera, startTime=1001, endTime=1030, sequenceStartTime=None, sequenceEndTime=None):
    sequenceStartTime = startTime if not sequenceStartTime else sequenceStartTime
    sequenceEndTime = endTime if not sequenceEndTime else sequenceEndTime

    if pm.objExists(camera):
        return pm.shot(shot_name, startTime=startTime, endTime=endTime, sequenceStartTime=sequenceStartTime, sequenceEndTime=sequenceEndTime, currentCamera=camera)


def create_after_last_shot(shot_name, camera, duration=20, shot_buffer=0, round_number=False): 
    """ 
    create a new shot at the end of timeline 

    Args: 
        name (str): a name of shot 
        camera (str): camera shape name 
        shot_buffer (int): number of gap frame between shots.
        round_number (bool): automatic calculate shot_buffer to always divide by 100
    """ 
    st, et = 1, 900
    seqst, seqet = 1, 1000
    if mc.ls(type='shot'):
        st, et = get_min_max_frame()
        seqst, seqet = get_seq_min_max_frame()
    create_new_shot(shot_name, camera, et, seqet, duration, shot_buffer, round_number)


def create_new_shot(shot_name, camera, et, seqet, duration, shot_buffer, round_number): 

    if round_number: 
        shot_buffer = 100 - (et % 100) - 1
        if shot_buffer < 50: 
            shot_buffer = shot_buffer + 100

    start_time = et + 1 + shot_buffer
    end_time = start_time + duration - 1

    seq_start_time = (seqet+1)
    seq_end_time = seq_start_time + duration - 1
    result = create_shot(shot_name, camera, startTime=start_time, endTime=end_time, sequenceStartTime=seq_start_time, sequenceEndTime=seq_end_time)
    return result 


def insert_shot(shot_name, camera, anchor_shot, duration, shot_buffer, round_number): 
    """ insert shot after selected shot """ 
    # create gap 
    shot = pm.PyNode(anchor_shot)
    move_frame = shot_buffer + duration
    next_shot = get_next_shot(anchor_shot)

    if next_shot: 
        ripple_move_shot(next_shot, move_frame, tail=True)
    et = shot.getEndTime()
    seqet = shot.getSequenceEndTime()
    result = create_new_shot(shot_name, camera, et, seqet, duration, shot_buffer, round_number)
    
    if next_shot: 
        ripple_delete_gap(next_shot)
    return result


def extend_shot_ripple(shot_name, duration, tail=True, head=False): 
    shot = pm.PyNode(shot_name)
    track_data = track_info()
    if tail: 
        et = shot.getEndTime()
        seqet = shot.getSequenceEndTime()
        ripple_sequence_time(sequence_startframe=(seqet + 1), move_frame=duration, tail=True, head=False)
        ripple_maya_time(startframe=(et + 1), move_frame=duration, tail=True, head=False, shift=False)
        shot.setEndTime(et + duration)
        shift_key(start=(et + 1), frame=duration)
    if head: 
        st = shot.getStartTime()
        seqst = shot.getSequenceStartTime()
        ripple_sequence_time(sequence_startframe=(seqst - 1), move_frame=-duration, tail=False, head=True)
        ripple_maya_time(startframe=(st - 1), move_frame=-duration, tail=False, head=True, shift=False)
        shot.setStartTime(st - duration)
        shot.setSequenceStartTime(seqst - duration)
        shift_key(end=(st - 1), frame=-duration)

    set_track(track_data)


def split_shot(split_frame, shot_name, new_shot, camera): 
    shot = pm.PyNode(shot_name)
    st = shot.getStartTime()
    et = shot.getEndTime()
    seqst = shot.getSequenceStartTime()
    seqet = shot.getSequenceEndTime()

    if split_frame >= seqst and split_frame <= seqet: 
        new_duration = split_frame - seqst
        new_seqet = seqst + new_duration - 1
        new_et = st + new_duration - 1

        shot.setEndTime(new_et)
        shot.setSequenceEndTime(new_seqet)
        create_shot(new_shot, camera, startTime=(new_et+1), endTime=et, sequenceStartTime=(new_seqet+1), sequenceEndTime=seqet)

    else: 
        logger.warning('frame "{}" not in shot range {} - {}'.format(split_frame, seqst, seqet))



def ripple_move_shot(shot_name, move_frame=0, tail=True, head=False): 
    if pm.objExists(shot_name): 
        shot = pm.PyNode(shot_name)
        sequence_startframe = shot.getSequenceStartTime()
        startframe = shot.getStartTime()
        ripple_move(startframe, sequence_startframe, move_frame, tail=tail, head=head)
    else: 
        logger.warning('Shot "{}" not exists. ripple_move_shot skip'.format(shot_name))


def ripple_move(startframe, sequence_startframe, move_frame=0, tail=True, head=False): 
    """ ripple move shots anchoring from sequence_startframe """
    track_data = track_info()

    ripple_sequence_time(sequence_startframe, move_frame, tail=tail, head=head)
    ripple_maya_time(startframe, move_frame, tail=tail, head=head)

    set_track(track_data)


def ripple_sequence_time(sequence_startframe, move_frame, tail, head): 
    shots = list_shots()

    for shot in shots: 
        seq_st = shot.getSequenceStartTime()
        seq_et = shot.getSequenceEndTime()
        set_start = False 
        set_end = False 

        if tail: 
            if seq_st >= sequence_startframe: 
                set_start = True 
            if seq_et >= sequence_startframe: 
                set_end = True 
        if head: 
            if seq_st <= sequence_startframe: 
                set_start = True 
            if seq_et <= sequence_startframe: 
                set_end = True

        # always change both start and end
        if set_start: 
            shot.setSequenceStartTime(seq_st + move_frame)
        if set_end: 
            shot.setSequenceEndTime(seq_et + move_frame)

    
def ripple_maya_time(startframe, move_frame, tail, head, shift=True): 
    # detect all shots to see affected maya timeline 
    shots = list_shots()

    for shot in shots: 
        st = shot.getStartTime()
        et = shot.getEndTime()
        seqet = shot.getSequenceEndTime()

        set_start = False 
        set_end = False 

        if tail: 
            if st >= startframe: 
                set_start = True
            if et >= startframe: 
                set_end = True
        if head: 
            if st <= startframe: 
                set_start = True
            if et <= startframe: 
                set_end = True
        
        # always change both start and end 
        if set_start: 
            shot.setStartTime(st + move_frame)
        if set_end: 
            shot.setEndTime(et + move_frame)
            shot.setSequenceEndTime(seqet)

    # shift actual keyframe 
    if tail: 
        start = startframe
        end = None 
    if head: 
        start = None 
        end = startframe

    if shift: 
        shift_key(start, end, move_frame)


def ripple_delete_gap(shot_name): 
    shot = pm.PyNode(shot_name)
    previous_shot = get_previous_shot(shot)
    current_seqst = shot.getSequenceStartTime()

    if previous_shot: 
        seqet = previous_shot.getSequenceEndTime()
        move_frame = -(current_seqst - seqet - 1)
        if move_frame:  
            ripple_sequence_time(sequence_startframe=current_seqst, move_frame=move_frame, tail=True, head=False)

        else: 
            logger.warning('No space to move')


def merge_shots(start_shot, end_shot): 
    """ 
    merge 2+ selected shots 
    add childs duration to master, disregard maya time range 
    """ 
    ordered_shots = list_shots()
    start_shot = pm.PyNode(start_shot)
    end_shot = pm.PyNode(end_shot)
    start_index = ordered_shots.index(start_shot)
    end_index = ordered_shots.index(end_shot)
    extend_duration = 0

    # forward 
    if end_index > start_index: 
        childs = ordered_shots[start_index + 1: end_index + 1]
        forward = True 
    # backward 
    if start_index > end_index: 
        childs = ordered_shots[end_index: start_index]
        forward = False

    track = start_shot.getTrack()

    for shot in childs: 
        shot = pm.PyNode(shot)
        duration = shot.getSequenceEndTime() - shot.getSequenceStartTime() + 1
        extend_duration += duration

    if forward: 
        new_et = start_shot.getEndTime() + extend_duration
        start_shot.setEndTime(new_et)

    else: 
        new_st = start_shot.getStartTime() - extend_duration
        new_seqst = start_shot.getSequenceStartTime() - extend_duration
        start_shot.setStartTime(new_st)
        start_shot.setSequenceStartTime(new_seqst)

    pm.delete(childs)
    start_shot.setTrack(track)


def shift_key(start=None, end=None, frame=0):
    """ shift key frame, shift all use default mode """
    if not start: 
        start = 0
    if not end: 
        end = 99999

    mc.selectKey(mc.ls(dag=True), t=(start, end))
    mc.keyframe(e=True, iub=0, an='keys', r=True, o='over', tc=frame)


def track_info(): 
    info = dict()
    for shot in list_shots(): 
        shot = pm.PyNode(shot)
        track = shot.getTrack()
        info[shot] = track

    return info


def set_track(info_dict): 
    for shot, track in info_dict.items(): 
        print 'shot', shot.name(), track
        shot.setTrack(track)


def get_min_max_frame(): 
    shots = pm.ls(type='shot')
    min_frame = sorted([a.getStartTime() for a in shots])[0]
    max_frame = sorted([a.getEndTime() for a in shots])[-1]
    return min_frame, max_frame


def get_seq_min_max_frame(): 
    shots = pm.ls(type='shot')
    min_frame = sorted([a.getSequenceStartTime() for a in shots])[0]
    max_frame = sorted([a.getSequenceEndTime() for a in shots])[-1]
    return min_frame, max_frame


def find_shot(target_frame): 
    """ find a shot that match target frame """ 
    shots = pm.ls(type='shot')
    match_shots = []
    for shot in shots: 
        start = shot.getSequenceStartTime()
        end = shot.getSequenceEndTime()
        if target_frame >= start and target_frame <=end: 
            match_shots.append(shot)

    return match_shots


def get_next_shot(shot_name): 
    shots = list_shots()
    shot = pm.PyNode(shot_name)
    index = shots.index(shot)
    next_index = index + 1
    next_shot = shots[next_index] if next_index < len(shots) else None
    return next_shot


def get_previous_shot(shot_name): 
    shots = list_shots()
    shot = pm.PyNode(shot_name)
    index = shots.index(shot)
    prev_index = index - 1
    previous_shot = shots[prev_index] if prev_index >= 0 else None 
    return previous_shot


def list_shot_between(start_shot=None, end_shot=None): 
    shots = list_shots()
    match_shots = []

    if not start_shot: 
        start_shot = shots[0]

    if not end_shot: 
        end_shot = shots[-1]

    valid_shot = False 
    for shot in shots: 
        if shot == start_shot: 
            valid_shot = True

        if valid_shot: 
            match_shots.append(shot)

        if shot == end_shot: 
            valid_shot = False 

    return match_shots


def list_shots(): 
    """ list shot by sequence time order """ 
    shots = pm.ls(type='shot')
    seq_ordered_shots = []
    ordered_shots = []
    seq_dict = dict()
    mtime_dict = dict()

    for shot in shots:
        seq_dict[shot.getSequenceStartTime()] = shot
        mtime_dict[shot.getStartTime()] = shot

    for sst in sorted(seq_dict.keys()): 
        shot = seq_dict[sst]
        seq_ordered_shots.append(shot)

    for st in sorted(mtime_dict.keys()): 
        shot = mtime_dict[st]
        ordered_shots.append(shot)

    if not seq_ordered_shots == ordered_shots: 
        logger.warning('Shot ordered not unanimous!')

    return seq_ordered_shots


def current_shots(): 
    current = mc.sequenceManager(q=True, ct=True)
    return find_shot(current)


def current_frame(): 
    current = mc.sequenceManager(q=True, ct=True)
    return current


def is_current_frame_at(shot_name): 
    shots = current_shots()
    if shot_name in shots: 
        return True 
    return False


def fix_unlink_shot(sequencer):
    shots = list_unlink_shots()
    for shot in shots:
        slot = node_free_slot(sequencer, 'shots')
        mc.connectAttr('%s.message' % shot, '%s.shots[%s]' % (sequencer, slot), f=True)
        logger.debug('Connect %s - %s' % (shot, sequencer))


def node_free_slot(node, attr):
    results = mc.listConnections('%s.%s' % (node, attr), c=True, s=True, d=False)
    slots = []
    if not results:
        return 0

    for node in results:
        if '[' in node:
            slot = node.split('[')[-1].split(']')[0]
            slots.append(int(slot))
    maxNum = sorted(slots)[-1]
    nextSlot = maxNum + 1
    return nextSlot


def list_unlink_shots():
    unlinkedShots = []
    shots = mc.ls(type='shot')
    for shot in shots:
        seq = mc.listConnections(shot, d=True, s=False, type='sequencer')
        if not seq:
            unlinkedShots.append(shot)
    return unlinkedShots
