import os
import sys
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import maya.cmds as mc
import maya.mel as mm
import maya.OpenMaya as om
import maya.OpenMayaAnim as oma
from rf_maya.lib import camera_lib
from collections import OrderedDict
from rf_maya.rftool.anim import anim_utils
reload(anim_utils)


def list_shots():
    shots = mc.sequenceManager(lsh=True)
    return shots

def list_shot_order(bySeq=True):
    shots = list_shots()
    shotDict = OrderedDict()
    for shot in shots:
        startFrame, endFrame, seqStartFrame, seqEndFrame = shot_info(shot)
        if bySeq:
            shotDict[seqStartFrame] = shot
        else:
            shotDict[startFrame] = shot
    shotOrdered = [shotDict[a] for a in sorted(shotDict.keys())]
    return shotOrdered

def list_audios():
    audios = mc.ls(type='audio')
    return audios

def shot_info(shotName):
    startFrame = 0
    endFrame = 0
    seqStartFrame = 0
    seqEndFrame = 0
    if mc.objExists('%s.startFrame' % shotName):
        startFrame = mc.getAttr('%s.startFrame' % shotName)
    if mc.objExists('%s.endFrame' % shotName):
        endFrame = mc.getAttr('%s.endFrame' % shotName)
    if mc.objExists('%s.sequenceStartFrame' % shotName):
        seqStartFrame = mc.getAttr('%s.sequenceStartFrame' % shotName)
    if mc.objExists('%s.sequenceEndFrame' % shotName):
        seqEndFrame = mc.getAttr('%s.sequenceEndFrame' % shotName)

    return startFrame, endFrame, seqStartFrame, seqEndFrame

def shot_scale(shotName):
    startFrame, endFrame, seqStartFrame, seqEndFrame = shot_info(shotName)
    mayaDuration = (endFrame - startFrame) + 1
    seqDuration = (seqEndFrame - seqStartFrame) + 1
    return seqDuration / mayaDuration

def shift_info(shotName, defaultStartFrame=1001):
    startFrame, endFrame, seqStartFrame, seqEndFrame = shot_info(shotName)
    return defaultStartFrame - startFrame


def shift(shotName, defaultStartFrame=1001):
    from rftool.utils import maya_utils
    lock_all(state=False)
    startFrame, endFrame, seqStartFrame, seqEndFrame = shot_info(shotName)
    timeOffset = defaultStartFrame - startFrame
    seqOffset = defaultStartFrame - seqStartFrame

    shift_key(frame=timeOffset) if not timeOffset == 0 else logger.warning('Skipped: Keyframe shift')
    shift_sequencer(shotName, timeOffset=timeOffset, seqOffset=seqOffset)
    # shift_sequencer(shotName, timeOffset=timeOffset, seqOffset=seqOffset) if not seqOffset == 0 else logger.warning('Skip shift sequencer')
    shift_audio(seqOffset=seqOffset)
    logger.info('%s shifted time line %s frames, sequencer %s frames' % (shotName, timeOffset, seqOffset))

    # set range
    startFrame, endFrame, seqStartFrame, seqEndFrame = shot_info(shotName)
    set_range(startFrame, endFrame)

def lock_shot(shotName, state=True):
    return mc.shot(shotName, e=True, lock=state)

def lock_all(state=True):
    shots = mc.ls(type='shot')
    for shot in shots:
        lock_shot(shot, state=state)


def shift_key(type = 'default', frameRange = [1, 10000], frame = 0) :
    """ shift key frame, shift all use default mode """
    if type == 'default' :
        keyframe_curve_types = ['animCurveTL', 'animCurveTA', 'animCurveTT', 'animCurveTU']
        keys = [c for c in mc.ls(type=keyframe_curve_types) if not mc.referenceQuery(c, inr=True)]
        if keys:
            mc.keyframe(keys, e = True, iub = 0, an = 'objects', r = True, o = 'over', tc = frame)
    else:
        if type == 'start' :
            mm.eval('selectKey -t ("%s:") `ls -dag`;' % frameRange[0])
        elif type == 'range' :
            mm.eval('selectKey  -t ("%s:%s") `ls -dag`;' % (frameRange[0], frameRange[1]))
        elif type == 'end' :
            mm.eval('selectKey -t (":%s") `ls -dag`;' % frameRange[1])
        mc.keyframe(e = True, iub = 0, an = 'keys', r = True, o = 'over', tc = frame)

def shift_key2(frame = 0, defaultStartFrame=1001) :
    if frame < 0:  # shifting backwards <---
        backward = True
    else:  # shifting forward --->
        backward = False

    dgIt = om.MItDependencyNodes(om.MFn.kAnimCurve)
    while not dgIt.isDone():
        curveFn = oma.MFnAnimCurve(dgIt.thisNode())
        if not curveFn.isFromReferencedFile() and curveFn.animCurveType() in (0, 1, 2, 3):
            keyRange = range(curveFn.numKeyframes())
            if not backward:  # if shifting forward, shift last time index first
                keyRange.reverse()

            for k in keyRange:
                currTime = curveFn.time(k)
                old_value = currTime.value()
                currTime += float(frame)

                # needs to check if the plug to set time is locked 
                plug = curveFn.findPlug('keyTimeValue').elementByLogicalIndex(k)
                if plug.isLocked(): # unlock if it's lock
                    plug.setLocked(False)
                with anim_utils.UnlockAnimLayer() as unlock:
                    curveFn.setTime(k, currTime)
        dgIt.next()

def shift_sequencer(shotName, timeOffset=0, seqOffset=0):
    frameInfo = frame_info()
    frameList = []
    # shift forward, start with last shot
    if timeOffset >= 0:
        frameList = sorted(frameInfo.keys())[::-1]
    # shift backward, start with first shot
    elif timeOffset < 0:
        frameList = sorted(frameInfo.keys())

    for frame in frameList:
        shot, startFrame, endFrame, seqStartFrame, seqEndFrame = frameInfo[frame]
        mute = False if shot == shotName else True
        shift_shot(shot, timeOffset, seqOffset, mute)

def shift_audio(seqOffset=0):
    audios = list_audios()
    audioInfo = audio_info()
    frameList = []

    # shift forward, start with last shot
    if seqOffset > 0:
        frameList = sorted(audioInfo.keys())[::-1]
    # shift backward, start with first shot
    elif seqOffset < 0:
        frameList = sorted(audioInfo.keys())

    for frame in frameList:
        audioShot, offset = audioInfo[frame]
        offsetValue = offset + seqOffset
        mc.setAttr('%s.offset' % audioShot, offsetValue)


def frame_info(key='mayaTime'):
    shots = mc.ls(type='shot')
    # filter only non-ref
    shots = [a for a in shots if not mc.referenceQuery(a, inr=True)]
    frameInfo = dict()
    for shotName in shots:
        startFrame, endFrame, seqStartFrame, seqEndFrame = shot_info(shotName)
        if key == 'mayaTime':
            frameInfo[startFrame] = [shotName, startFrame, endFrame, seqStartFrame, seqEndFrame]
        elif key == 'seqTime':
            frameInfo[seqStartFrame] = [shotName, startFrame, endFrame, seqStartFrame, seqEndFrame]
    return frameInfo


def get_transition(shotName): 
    transitionIn = mc.getAttr('%s.transitionInLength' % shotName)
    transitionOut = mc.getAttr('%s.transitionOutLength' % shotName)
    return transitionIn, transitionOut


def has_transition(shotName): 
    transitionIn, transitionOut = get_transition(shotName)
    if transitionIn or transitionOut: 
        return True 
    return False


def audio_info():
    audios = mc.ls(type='audio')
    audioOffsetInfo = dict()
    for audioShot in audios:
        offset = mc.getAttr('%s.offset' % audioShot)
        audioOffsetInfo[offset] = [audioShot, offset]
    return audioOffsetInfo

def shift_shot(shotName, timeOffset=0, seqOffset=0, mute=True):
    currentStartFrame, currentEndFrame, currentSeqStartFrame, currentSeqEndFrame = shot_info(shotName)
    startFrame = currentStartFrame + timeOffset
    endFrame = currentEndFrame + timeOffset
    seqStartFrame = currentSeqStartFrame + seqOffset
    seqEndFrame = currentSeqEndFrame + seqOffset

    if timeOffset >= 0:
        mc.setAttr('%s.startFrame' % shotName, startFrame)
        mc.setAttr('%s.endFrame' % shotName, endFrame)
        mc.setAttr('%s.sequenceStartFrame' % shotName, seqStartFrame)
        mc.setAttr('%s.sequenceEndFrame' % shotName, seqEndFrame)
    elif timeOffset < 0:
        mc.setAttr('%s.endFrame' % shotName, endFrame)
        mc.setAttr('%s.startFrame' % shotName, startFrame)
        mc.setAttr('%s.sequenceStartFrame' % shotName, seqStartFrame)
        mc.setAttr('%s.sequenceEndFrame' % shotName, seqEndFrame)

    mc.shot(shotName, e=True, mute=mute)

def extend_shot(shotName, value, head=False, tail=False, timeslider=True):
    """ extend or trim shot from given value. It doesn't ripple other shots """ 
    startFrame, endFrame, seqStartFrame, seqEndFrame = shot_info(shotName)
    sound_node = '%s_sound'%shotName
    rangeStartFrame = startFrame
    rangeEndFrame = endFrame

    if head:
        newStartFrame = startFrame - value
        newSeqStartFrame = seqStartFrame - value
        mc.setAttr('%s.sequenceStartFrame' % shotName, newSeqStartFrame)
        mc.setAttr('%s.startFrame' % shotName, newStartFrame)
        rangeStartFrame = newStartFrame
        rangeEndFrame = endFrame

    if tail:
        newEndFrame = endFrame + value
        newSeqEndFrame = seqEndFrame + value
        mc.setAttr('%s.endFrame' % shotName, newEndFrame)
        mc.setAttr('%s.sequenceEndFrame' % shotName, newSeqEndFrame)
        mc.setAttr('%s.sourceEnd'% sound_node, (newEndFrame-startFrame)+1) if mc.objExists(sound_node) else None
        rangeStartFrame = startFrame
        rangeEndFrame = newEndFrame

    if timeslider:
        set_range(rangeStartFrame, rangeEndFrame)

def trim_shot(shotName, value, head=False, tail=False, timeslider=True):
    startFrame, endFrame, seqStartFrame, seqEndFrame = shot_info(shotName)
    sound_node = '%s_sound'%shotName
    rangeStartFrame = startFrame
    rangeEndFrame = endFrame

    if head:
        newStartFrame = startFrame + value
        newSeqStartFrame = seqStartFrame + value
        mc.setAttr('%s.startFrame' % shotName, newStartFrame)
        mc.setAttr('%s.sequenceStartFrame' % shotName, newSeqStartFrame)
        rangeStartFrame = newStartFrame
        rangeEndFrame = endFrame

    if tail:
        newEndFrame = endFrame - value
        newSeqEndFrame = seqEndFrame - value
        mc.setAttr('%s.endFrame' % shotName, newEndFrame)
        mc.setAttr('%s.sequenceEndFrame' % shotName, newSeqEndFrame)
        print endFrame-startFrame
        if mc.objExists(sound_node):
            mc.setAttr('%s.sourceEnd'% sound_node, (newEndFrame-startFrame)+1)
        rangeStartFrame = startFrame
        rangeEndFrame = newEndFrame

    if timeslider:
        set_range(rangeStartFrame, rangeEndFrame)

def mute(state):
    shots = list_shots()
    if shots:
        for shotName in shots:
            mc.shot(shotName, e=True, mute=state)


def shift_shot_seq(shotName, timeOffset=0, seqOffset=0, mute=False, trim ='right'):
    all_shot = list_shot_order()
    # all_shot = sorted(all_shot)
    order_shot = all_shot.index(shotName)
    list_shot = list(all_shot)

    frameInfo = []
    for index, ix in enumerate(list_shot):
        if order_shot > index:
            all_shot.remove(ix)

    shift_shot_list(all_shot, timeOffset, seqOffset, mute)

def shift_shot_list(shotList, timeOffset, seqOffset, mute=False):
    """ shift shot list + or - """
    if timeOffset > 0:
        shotList = shotList[::-1]
    # shift backward, start with first shot
    elif timeOffset < 0:
        shotList = shotList

    for shot in shotList:
        shift_shot(shot, timeOffset, seqOffset, mute)


def extend_shot_seq(shotName, seqTargetFrame=None, timeOffset=0, seqOffset=0, mute=False, shiftKey=True, head=False, tail=True, timeslider=True):
    """ extend or trim shot from seqStartFrame
    positive timeOffset = extend
    negative timeOffset = trim
    recommend seqStartFrame and timeOffset has the same value to preserve time scale

    """
    all_shot = list_shot_order()
    shotIndex = all_shot.index(shotName)
    list_shot = list(all_shot)
    startFrame, endFrame, seqStartFrame, seqEndFrame = shot_info(shotName)
    proceed = True

    if not proceed:
        logger.warning('SeqTargetFrame must be in between %s - %s (given value %s) - Abort!' % (seqStartFrame, seqEndFrame, seqTargetFrame))

    if proceed:
        for index, ix in enumerate(list_shot):
            if tail == True:
                if index <= shotIndex:
                    all_shot.remove(ix)
            if head == True:
                if index >= shotIndex:
                    all_shot.remove(ix)

        # positive timeOffset = extend
        if timeOffset > 0:
            if tail == True:
                shift_shot_list(all_shot, timeOffset, seqOffset, mute)
                extend_shot(shotName, value=seqOffset, head=head, tail=tail, timeslider=timeslider)
            if head == True:
                shift_shot_list(all_shot, -timeOffset, -seqOffset, mute)
                extend_shot(shotName, value=seqOffset, head=head, tail=tail, timeslider=timeslider)

        # negative timeOffset = trim
        elif timeOffset < 0:
            if tail == True:
                trim_shot(shotName, value=abs(seqOffset), head=head, tail=tail, timeslider=timeslider)
                shift_shot_list(all_shot, timeOffset, seqOffset, mute)
            if head == True:
                trim_shot(shotName, value=abs(seqOffset), head=head, tail=tail, timeslider=timeslider)
                shift_shot_list(all_shot, -timeOffset, -seqOffset, mute)

        if shiftKey:
            if tail == True:
                shift_key(type='start', frameRange=[seqTargetFrame, 10000], frame=timeOffset)
            if head == True:
                shift_key(type='end', frameRange=[-10000, seqTargetFrame], frame=-timeOffset)


def add_transition_roll(shotName, preRoll=0, postRoll=0): 
    """ add preroll postroll by adding transition attribute to shots """ 
    currentPreRoll = mc.getAttr('%s.transitionInLength' % shotName)
    currentPostRoll = mc.getAttr('%s.transitionOutLength' % shotName)

    if preRoll: 
        value = -preRoll
        if not currentPreRoll == preRoll: 
            value = -(preRoll - currentPreRoll)
        ripple_sequence_time(shotName, value, head=True, tail=False)
        mc.setAttr('%s.transitionInLength' % shotName, preRoll)

    if postRoll:
        value = postRoll
        if not currentPostRoll == postRoll: 
            value = (postRoll - currentPostRoll)
        ripple_sequence_time(shotName, value, head=False, tail=True)
        mc.setAttr('%s.transitionOutLength' % shotName, postRoll)


def remove_transition_roll(shotName): 
    """ remove transition and modify sequence / maya time for specify shotName """
    inValue = mc.getAttr('%s.transitionInLength' % shotName)
    outValue = mc.getAttr('%s.transitionOutLength' % shotName)

    ripple_sequence_time(shotName, inValue, head=True, tail=False)
    ripple_sequence_time(shotName, -outValue, head=False, tail=True)
    reset_transition(shotName)


def reset_transition(shotName): 
    mc.setAttr('%s.transitionInLength' % shotName, 0)
    mc.setAttr('%s.transitionOutLength' % shotName, 0)

def has_in_transition(shotName): 
    value = mc.getAttr('%s.transitionInLength' % shotName)
    return True if value > 0 else False 

def has_out_transition(shotName): 
    value = mc.getAttr('%s.transitionOutLength' % shotName)
    return True if value > 0 else False 



def ripple_sequence_from_frame(seqTargetFrame, value, head=False, tail=False): 
    """ move from selected sequenceTime to the right +value or left -value 
    not change mayaTime for ripple shots 
    head = True mean from target to start 
    tail = True mean from target to end 
    """ 
    all_shot = list_shot_order()
    matchShot = None
    matchIndex = None 

    for shot in all_shot: 
        st, et, sst, sse = shot_info(shot)
        if seqTargetFrame >= sst and seqTargetFrame <= sse: 
            matchShot = shot
            break 

    ripple_sequence_time(matchShot, value, head=head, tail=tail)


def ripple_sequence_time(shotName, value, head=False, tail=False): 

    all_shot = list_shot_order()

    for i, shot in enumerate(all_shot): 
        if shot == shotName: 
            matchIndex = i

    if shotName: 
        toStartShot = all_shot[:matchIndex+1]
        toEndShot = all_shot[matchIndex:]

        # handle tail or head shots 
        if tail: 
            toEndShot.remove(shotName)
            # -value to left +value to right 
            if value > 0: 
                move_shot_sequence_time(toEndShot, value)
                extend_shot(shotName, value, head=False, tail=True, timeslider=True)
            if value < 0: 
                extend_shot(shotName, value, head=False, tail=True, timeslider=True)
                move_shot_sequence_time(toEndShot, value)

            set_clip_track()

        if head: 
            toStartShot.remove(shotName)
            if value > 0: 
                extend_shot(shotName, -value, head=True, tail=False, timeslider=True)
                move_shot_sequence_time(toStartShot, value)
            if value < 0: 
                move_shot_sequence_time(toStartShot, value)
                extend_shot(shotName, -value, head=True, tail=False, timeslider=True)

            set_clip_track()

    else: 
        logger.error('frame "%s" not match to any shot' % seqTargetFrame)


def move_shot_sequence_time(shotList, value): 
    if value > 0: 
        # start from last to target 
        shots = shotList[::-1]
    if value < 0: 
        shots = shotList 

    for shotName in shots: 
        move_sequence_time(shotName, value)


def move_sequence_time(shotName, value): 
    """ move shot sequence time to right +value or left -value """ 
    st, et, sst, sse = shot_info(shotName)
    nsst = sst + value
    nsse = sse + value 

    if value > 0: 
        mc.setAttr('%s.sequenceEndFrame' % shotName, nsse)
        mc.setAttr('%s.sequenceStartFrame' % shotName, nsst)

    if value < 0:
        mc.setAttr('%s.sequenceStartFrame' % shotName, nsst)
        mc.setAttr('%s.sequenceEndFrame' % shotName, nsse)


def change_track(shot, trackIndex): 
    index = mc.shot(shot, q=True, track=True)
    if not index == trackIndex: 
        mc.shot(shot, e=True, track=trackIndex)


def set_clip_track(index=1): 
    """ move all clips to main track index 1 """
    all_shot = list_shot_order()
    for shot in all_shot: 
        change_track(shot, index)


def set_range(start, end):
    mc.playbackOptions(min=start, max=end)
    mc.playbackOptions(ast=start, aet=end)


def sync_range():
    from rf_utils.context import context_info
    from rf_utils import project_info
    scene = context_info.ContextPathInfo()
    duration = get_duration(scene) or 0
    proj = project_info.ProjectInfo(scene.project)
    startFrame = proj.render.start_frame()
    set_range(startFrame, startFrame + duration - 1)
    return duration


def in_pre_post_roll(scene=None): 
    from rf_utils.sg import sg_utils
    from rf_utils.context import context_info
    from rf_utils import project_info
    scene = context_info.ContextPathInfo() if not scene else scene
    preRoll = scene.projectInfo.scene.pre_roll
    postRoll = scene.projectInfo.scene.post_roll

    filters = [['project.Project.name', 'is', scene.project], ['code', 'is', scene.name]]
    fields = ['sg_include_prepost_roll']
    sgEntity = sg_utils.sg.find_one('Shot', filters, fields)
    durationIncludePrePostRoll = sgEntity.get('sg_include_prepost_roll')

    if not durationIncludePrePostRoll: 
        durationIncludePrePostRoll = scene.projectInfo.scene.include_prepost_sequencer

    if not durationIncludePrePostRoll: 
        preRoll = 0
        postRoll  = 0
    return preRoll, postRoll


def get_duration(scene):
    from rf_utils.sg import sg_utils
    filters = [['project.Project.name', 'is', scene.project], ['code', 'is', scene.name]]
    fields = ['sg_working_duration', 'sg_include_prepost_roll']
    sgEntity = sg_utils.sg.find_one('Shot', filters, fields)
    return sgEntity.get('sg_working_duration')


def camera_info(shotName):
    return mc.shot(shotName, q=True, cc=True)

def set_camera(shotName, scene):
    cameraShape = camera_info(shotName)
    scene.projectInfo.render.overscan()
    try:
        mc.setAttr('%s.filmFit' % cameraShape, scene.projectInfo.render.film_fit())
        mc.setAttr('%s.overscan' % cameraShape, scene.projectInfo.render.overscan())
        mc.setAttr('%s.displayGateMaskColor' % cameraShape, 0, 0, 0, type='double3')
        mc.setAttr('%s.displayGateMaskOpacity' % cameraShape, 1)
    except Exception as e:
        print e

def set_shot_camera(shotName, camera):
    camShape = None
    if mc.objectType(camera, isType='transform'):
        camShape = mc.listRelatives(camera, s=True)[0]
    if mc.objectType(camera, isType='camera'):
        camShape = camera
    if camShape:
        mc.connectAttr('%s.message' % camShape, '%s.currentCamera' % shotName, f=True)


def create_shot(name, camera, startTime=1001, endTime=1030, sequenceStartTime=None, sequenceEndTime=None):
    sequenceStartTime = startTime if not sequenceStartTime else sequenceStartTime
    sequenceEndTime = endTime if not sequenceEndTime else sequenceEndTime

    if mc.objExists(camera):
        return mc.shot(name, startTime=startTime, endTime=endTime, sequenceStartTime=sequenceStartTime, sequenceEndTime=sequenceEndTime, currentCamera=camera)


def create_audio(name, filename, offset):
    # create node
    audio = mc.createNode('audio', name=name)
    filename = filename.replace('\\', '/')
    mc.sequenceManager(attachSequencerAudio=audio)

    # set filename
    mc.setAttr('%s.filename' % audio, filename, type='string')
    mc.setAttr('%s.offset' % audio, offset)

    # mc.sequenceManager(addSequencerAudio=filename)

def rebuild(seq=True, audio=False):
    if seq:
        info = frame_info(key='seqTime')
        for k in sorted(info.keys()):
            shotName, startFrame, endFrame, seqStartFrame, seqEndFrame = info[k]
            camera = mc.listConnections(shotName, type='camera')
            camera = camera[0] if camera else ''
            mc.delete(shotName) if mc.objExists(shotName) else None
            create_shot(shotName, camera, startTime=startFrame, endTime=endFrame, sequenceStartTime=seqStartFrame, sequenceEndTime=seqEndFrame)
            logger.info('Recreated shot %s' % shotName)

    if audio:
        audioInfo = audio_info()
        for k in sorted(audioInfo.keys()):
            soundNode, offset = audioInfo[k]
            filename = mc.getAttr('%s.filename' % soundNode)
            mc.delete(soundNode) if mc.objExists(soundNode) else None
            create_audio(soundNode, filename, offset)
            logger.debug('Recreated audio node %s' % soundNode)


def create_preview_cam(cameras, duration=20): 
    start = 1001 
    st = None
    for i, camera in enumerate(cameras): 
        st = start if not st else st
        et = st + duration
        name = 'shot_{}'.format(camera)
        create_shot(name, camera, startTime=st, endTime=et)
        st = et + 1
        

def relink_shot_camera(shotNames=[]): 
    """ try to guess linked camera by namespace """ 
    if not shotNames: 
        shotNames = list_shot_order()

    for shotName in shotNames: 
        namespace = '%s_cam' % shotName
        cameraShape = mc.ls('%s:*' % namespace, type='camera')
        if cameraShape: 
            mc.shot(shotName, e=True, cc=cameraShape[0])
            logger.debug('Linked %s -> %s success' % (shotName, cameraShape[0]))
        else: 
            logger.debug('Failed to link %s camera not found' % shotName)


def fix_unlink_shot(sequencer):
    shots = list_unlink_shots()
    for shot in shots:
        slot = node_free_slot(sequencer, 'shots')
        mc.connectAttr('%s.message' % shot, '%s.shots[%s]' % (sequencer, slot), f=True)
        logger.debug('Connect %s - %s' % (shot, sequencer))


def node_free_slot(node, attr):
    results = mc.listConnections('%s.%s' % (node, attr), c=True, s=True, d=False)
    slots = []
    if not results:
        return 0

    for node in results:
        if '[' in node:
            slot = node.split('[')[-1].split(']')[0]
            slots.append(int(slot))
    maxNum = sorted(slots)[-1]
    nextSlot = maxNum + 1
    return nextSlot

def list_unlink_shots():
    unlinkedShots = []
    shots = mc.ls(type='shot')
    for shot in shots:
        seq = mc.listConnections(shot, d=True, s=False, type='sequencer')
        if not seq:
            unlinkedShots.append(shot)
    return unlinkedShots


def list_order_shots():
    """ list shot ordered by sequence time """
    pass


def has_reuse_timeline(shotName): 
    all_shot = list_shot_order()
    stm, etm, sstm, ssem = shot_info(shotName)
    reuseShots = []

    for shot in all_shot: 
        st, et, sst, sse = shot_info(shot)
        if st >= stm and st <= etm or et <= etm and et >= stm: 
            reuseShots.append(shot)

    return reuseShots


def restore_shots(allshot=True, unlinked=False):
    """ this function restore shots to unreference Sequence """
    # assume valid sequencer always name "sequencer1"
    sequenceManager = 'sequenceManager1'
    sequencer = 'sequencer1'

    # create if not exists
    if not mc.objExists(sequencer):
        sequencer = mc.createNode('sequencer', n=sequencer)

    # connect to sequencerManager1
    if not mc.objExists(sequenceManager):
        sequenceManager = mc.createNode('sequenceManager', n=sequenceManager)

    if mc.objExists(sequenceManager):
        results = mc.listConnections('%s.sequences' % sequenceManager, d=True, s=True, c=True, p=True)
        # if linked to any sequencer, disconnect
        if results:
            for index in range(0, len(results), 2):
                target = results[index]
                src = results[index + 1]
                mc.disconnectAttr(src, target)

        # reconnect to sequencer1
        mc.connectAttr('%s.message' % sequencer, '%s.sequences[0]' % sequenceManager, f=True)

    # disconnect shots from all sequences
    seqNodes = mc.ls(type='sequencer')

    for seqNode in seqNodes:
        results = mc.listConnections('%s.shots' % seqNode, d=True, s=True, c=True, p=True)
        # if linked to any sequencer, disconnect
        if results:
            for index in range(0, len(results), 2):
                target = results[index]
                src = results[index + 1]
                mc.disconnectAttr(src, target)

    # link shots
    shots = []
    if allshot:
        shots = mc.ls(type='shot')
    elif unlinked:
        shots = list_unlink_shots()

    for shot in shots:
        slot = node_free_slot(sequencer, attr='shots')
        mc.connectAttr('%s.message' % shot, '%s.shots[%s]' % (sequencer, slot), f=True)
        logger.debug('Restore %s' % shot)

    logger.info('---- Restore %s shots completed ----' % (len(shots)))

    # clean empty sequences
    seqNodes = mc.ls(type='sequencer')

    for seq in seqNodes:
        if not mc.referenceQuery(seq, inr=True):
            results = mc.listConnections('%s.shots' % seq, d=True, s=True, c=True, p=True)
            if not results:
                mc.delete(seq)
                logger.debug('Remove unused sequence %s' % seq)
        else:
            refPath = mc.referenceQuery(seq, f=True)
            logger.debug('"%s" is a referenceNode from "%s"' % (seq, refPath))

# from rf_utils.context import context_info
# from rf_utils.sg import sg_utils
# sg = sg_utils.sg
# path = mc.file(sn=True, q=True)
# scene = context_info.ContextPathInfo(path=path)
# context = context_info.Context()
# context.use_sg(sg, project='projectName', entityType='scene', entityName=scene.name)
# scene = context_info.ContextPathInfo(context=context)

# DEFAULT_PROJECT = {
#     "type": "Project",
#     "id": 134,
#     "name": scene.project
# }
# DEFAULT_EPISODE = {'project': DEFAULT_PROJECT, 'code': scene.episode, 'type': 'Scene', 'id': 607}
# DEFAULT_RANGE = 1000
# DEFAULT_DURATION_SHOT = 100

# shots = camera_sequencer.get_shots(DEFAULT_PROJECT, "q0010", "Shot", DEFAULT_EPISODE)
# start = True

# for shot in shots:
#     if start:
#         start_time = shot['sg_cut_in'] + DEFAULT_RANGE
#         start = False

#     end_time = start_time + shot['sg_cut_duration'] - 1
#     camera_lib.create_shot(shot["code"], 'camera1', start_time, end_time)
#     start_time = end_time + DEFAULT_DURATION_SHOT
