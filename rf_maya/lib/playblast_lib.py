import os
import shutil
import sys
import tempfile
import re
import maya.cmds as mc
import pymel.core as pm
import maya.mel as mm

from rf_utils import project_info
from rf_utils.pipeline import playblast_converter
# reload(playblast_converter)
from rf_utils import file_utils
from rf_utils.context import context_info
from rf_utils import audio_utils
from rf_utils.sg import sg_process
from rf_utils import admin
# reload(sg_process)
# reload(audio_utils)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())


def playblast(filepath, size=(1920, 1080), startFrame=1, endFrame=2, sequencer=1, format='qt', codec='H.264', timeRange=False, offScreen=True, quality=70):
    # format = 'image' or 'qt'
    # codec = 'H.264' or 'none'
    # compression = 'png'
    w, h = size
    audioFile = None
    useTraxSounds = 1
    if format == 'image':
        # filename no ext
        filepath, ext = os.path.splitext(filepath)
        useTraxSounds = 0

    if timeRange:
        startFrame = mc.playbackOptions(q=True, min=True)
        endFrame = mc.playbackOptions(q=True, max=True)

    result = mc.playblast( format=format,
                    # s=audioFile,
                    filename=filepath,
                    st=startFrame,
                    et=endFrame,
                    forceOverwrite=True,
                    sequenceTime=sequencer,
                    clearCache=1,
                    viewer=0,
                    showOrnaments=1,
                    fp=4,
                    widthHeight=[w,h],
                    percent=100,
                    useTraxSounds = useTraxSounds,
                    compression=codec,
                    offScreen=offScreen,
                    quality=quality
                    )

    return result

def playblast_sequencer(filepath, shotNames=[], size=(1920, 1080), format='qt', codec='H.264', combine=True, file_name='', stepKey=[], quality=70):
    # clear selection
    mc.select(cl=True)
    ext = os.path.splitext(filepath)[-1]
    movStart = int()
    movEnd = int()
    startList = []
    endList = []
    shots = []
    # get select time range
    rangeArray = get_selection_range()

    for shotName in shotNames:
        startFrame, endFrame, seqStartFrame, seqEndFrame = get_shot_frame(shotName)
        startList.append(startFrame)
        endList.append(endFrame)

        if not combine:
            # if range selection found, override default range
            if rangeArray:
                newRange = mayaTime_to_seqTime(shotName, rangeArray)
                if newRange:
                    seqStartFrame = newRange[0]
                    seqEndFrame = newRange[1]

            shotFilename = '{}{}'.format(shotName, ext)
            # separate folder if image
            dstDir = [os.path.dirname(filepath), shotFilename] if not format == 'image' else [os.path.dirname(filepath), shotName, shotFilename]
            # print('dstDir', dstDir)
            shotPath = '/'.join(dstDir)
            if file_name:
                shotPath = os.path.join(filepath, str(file_name))

            if not os.path.exists(os.path.dirname(shotPath)):
                os.makedirs(os.path.dirname(shotPath))

            result = playblast(shotPath, size=(size[0], size[1]), 
                    startFrame=seqStartFrame, 
                    endFrame=seqEndFrame, 
                    sequencer=1, 
                    format=format, 
                    codec=codec, 
                    timeRange=False,
                    quality=quality)
            print('playblast result', result)
            if stepKey: 
                make_sequence_stepkey(result, stepKey)

            shots.append(result)

    # if user select time range, use selected
    if rangeArray:
        newRange = mayaTime_to_seqTime(shotNames[0], rangeArray)
        movStart = newRange[0]
        movEnd = newRange[1]
    else: # find smallest for start
        movStart = min(startList)
        movEnd = max(endList)

    if combine:
        # make dir if not exists
        if not os.path.exists(os.path.dirname(filepath)):
            os.makedirs(os.path.dirname(filepath))
        result = playblast(filepath, size=(size[0], size[1]), startFrame=movStart, endFrame=movEnd, sequencer=1, format=format, codec=codec, timeRange=False)
        shots.append(result)
    return shots

def make_sequence_stepkey(path, keys=[]): 
    """ make image sequence stepped by keep only keyframe in the list and remove others, then extend still frame to replace those time gap """ 
    print('stepped key freeze frame', keys)
    keys = [int(a) for a in keys]
    dirname = os.path.dirname(path)
    files = file_utils.list_file(dirname)
    data = dict()

    for file in files: 
        digit = re.findall(r'[.]+\d+[.]', file)
        if digit: 
            frame = digit[0].replace('.', '')
            data[int(frame)] = '{}/{}'.format(dirname, file)

    start = sorted(data.keys())[0]
    end = sorted(data.keys())[-1]
    src = None 

    for key in sorted(data.keys()): 
        target = None
        if key in keys: 
            src = data[key]
        else: 
            target = data[key]

        if src and target: 
            file_utils.copy(src, target)
            print('Copy key still {} -- {}'.format(src, target))

def get_shot_frame(shotName):
    startFrame = mc.getAttr('{}.startFrame'.format(shotName))
    endFrame = mc.getAttr('{}.endFrame'.format(shotName))
    seqStartFrame = mc.getAttr('{}.sequenceStartFrame'.format(shotName))
    seqEndFrame = mc.getAttr('{}.sequenceEndFrame'.format(shotName))

    return startFrame, endFrame, seqStartFrame, seqEndFrame

def playblast_mov_hud(movFile, size=(1920, 1080), startFrame=1, endFrame=2, sequencer=1, hudProject='', hudUser='', hudStep='', hudFilename='', hudShot=''):
    from rf_utils.pipeline import playblast_converter
    result = playblast(movFile, size=(1920, 1080), startFrame=startFrame, endFrame=endFrame, sequencer=sequencer, format='image', codec='png', timeRange=False)
    imgPath = os.path.dirname(result)
    convertResult = playblast_converter.playblast_convert(imgPath, hudProject, hudUser, hudStep, hudFilename, hudShot)
    return convertResult

def playblast_sequencer_hud(filepath, shotName, hudProject='', hudUser='', hudStep='', hudFilename='', 
        hudShot='', showHud=True, light=False, stepKey=[], quality=70, remove_images=True):
    # filename = 'pr_ep01_q0010_s0010_ani_main.v001.mov'
    scene = context_info.ContextPathInfo()
    projectInfo = project_info.ProjectInfo(project=scene.project)
    outputW, outputH = projectInfo.render.raw_outputH()
    fps = projectInfo.render.fps()
    startFrame, endFrame, seqStartFrame, seqEndFrame = get_shot_frame(shotName)

    # get due date for hud
    result = sg_process.get_task_from_context(scene)
    not_available_str = 'N/A'
    due_date = result.get('due_date', not_available_str)
    start_date = result.get('start_date', not_available_str)

    # get finalcam status
    finalcam_entity = sg_process.get_task_status(project=scene.project, shotname=scene.name, task='finalCam')
    if finalcam_entity:
            finalcam_status = finalcam_entity.get('sg_status_list', not_available_str)
    else:
        finalcam_status = 'N/A'
    
    # create temp directory to hold images
    tmpPath = tempfile.mkdtemp().replace('\\', '/')

    lightPreviewGrp = add_preview_light(shotName) if light else None
    shots = playblast_sequencer(tmpPath, 
            shotNames=[shotName], 
            size=(outputW, outputH), 
            format='image', 
            codec='png', 
            combine=False, 
            stepKey=stepKey, 
            quality=100)  # quality to 100 cuz we will comporess them in conversion state later on.
    
    wav_path = generate_shot_audio(start=seqStartFrame, end=seqEndFrame, output_path=None, fps=fps)
    mc.refresh(su=0)

    outputWidth, outputHeight = projectInfo.render.final_outputH()
    imgPath = os.path.dirname(shots[0])
    
    remove_preview_light(lightPreviewGrp) if lightPreviewGrp else None
    cur_cam = mc.shot(shotName, cc=True,q=True)
    fcl = mc.getAttr('{}.focalLength'.format(cur_cam))

    # calcualte quality value
    if quality < 25:
        convert_quality = 'poor'
    elif quality in range(25, 50):
        convert_quality = 'low'
    elif quality in range(50, 70):
        convert_quality = 'medium'
    elif quality in range(70, 90):
        convert_quality = 'high'
    else:
        convert_quality = 'ultra'

    convertResult = playblast_converter.playblast_convert(images_path=imgPath, 
                                        wav_path=wav_path, 
                                        animation_name=hudProject, 
                                        user=hudUser, 
                                        department=hudStep, 
                                        file_name=hudFilename, 
                                        shot=shotName, 
                                        compWidth=outputWidth, 
                                        compHeight=outputHeight, 
                                        start_date=start_date,
                                        due_date=due_date,
                                        finalcam_status=finalcam_status, 
                                        showHud=showHud, 
                                        focalLength= fcl, 
                                        framerate=fps, 
                                        quality=convert_quality)
    tmpMov = '{}.mov'.format(os.path.splitext(convertResult)[0])

    # file_utils.copy(tmpMov, filepath)
    if os.path.exists(tmpMov) :
        dstDir = filepath.replace(filepath.split('/')[-1], '')
        if not os.path.exists(dstDir) :
            os.makedirs(dstDir)
    admin.copyfile(tmpMov, filepath, check_result=False)
    if remove_images:
        try:
            shutil.rmtree(os.path.dirname(tmpMov))
        except Exception as e:
            logger.warning(e)
            logger.warning('This user does not have permission to delete temp images. This may result in incorrect playblast result!')
    return filepath if os.path.exists(filepath) else False

def get_audio_path(shotName):
    # hard coded audio format
    # s0010_sound
    suffix = '_sound'
    audioNode = '{}{}'.format(shotName, suffix)
    if mc.objExists(audioNode):
        path = mc.getAttr('{}.filename'.format(audioNode))
        return path

def get_selection_range():
    aPlayBackSliderPython = mm.eval('$tmpVar=$gPlayBackSlider')
    rangeArray = mc.timeControl(aPlayBackSliderPython, q=True, rangeArray=True)
    if (rangeArray[1] - rangeArray[0]) > 1:
        return rangeArray
    return 0

def mayaTime_to_seqTime(shotName, rangeArray):
    startFrame, endFrame, seqStartFrame, seqEndFrame = get_shot_frame(shotName)
    # 1:1 map
    if (endFrame - startFrame) == (seqEndFrame - seqStartFrame):
        offset = seqStartFrame - startFrame
        newStart = 0
        newEnd = 0

        # if given range perfectly fall in maya range
        if not rangeArray[0] > endFrame:
            newStart = rangeArray[0] + offset
        if rangeArray[0] < startFrame:
            newStart = seqStartFrame
        if not rangeArray[1] < startFrame:
            newEnd = rangeArray[1] + offset
        if rangeArray[1] > endFrame:
            newEnd = seqEndFrame

        if newStart > 0 and newEnd > 0:
            return [newStart, newEnd]

    return None

def play_sequencer(shots, dst, projectHud, userHud, departmentHud, filenameHud, sound_path=None, outputWidth=1920, outputHeight=1080, combine=False):
    """ for ken """

    from rf_utils.sg import sg_process
    # reload(sg_process)
    sg = sg_process.sg

    scene = context_info.ContextPathInfo()
    context = context_info.Context()
    context.use_sg(sg, project=scene.project, entityType='scene', entityName=scene.name)
    scene = context_info.ContextPathInfo(context=context)
    name_no_shot = scene.name.split('all')[0]


    playblastWidth = 1920
    playblastHeight = 816

    shotInSeq = playblast_sequencer(dst, shots, (playblastWidth, playblastHeight),'image', 'png', combine=combine)
    result_path = []
    for index, shot in enumerate(shotInSeq):
        context.update(entityType='scene', entity='{0}{1}'.format(name_no_shot, shots[index]), step='edit', process='sound')
        scene = context_info.ContextPathInfo(context=context)
        path = scene.path.output_hero().abs_path()
        filename = scene.publish_name(True, ext='.wav')
        wav_path = None if sound_path == None else sound_path
        dest = os.path.dirname(dst)
        dst_path = "{}/{}".format(dest, shots[index])
        video_path = playblast_converter.playblast_convert(dst_path, wav_path, projectHud, userHud,
                     departmentHud, filenameHud, shots[index], outputWidth, outputHeight)

        dst_video = "{}/{}.mov".format(os.path.dirname(os.path.dirname(video_path)), os.path.basename(video_path))
        playblastAndSound_path = "{}.mov".format(video_path)
        file_utils.copy(playblastAndSound_path, dst_video)
        file_utils.remove(playblastAndSound_path)
        if not os.listdir (os.path.dirname(video_path)):
            os.rmdir(os.path.dirname(video_path))
        result_path.append(dst_video)
    return result_path

def add_preview_light(shotName):
    camera = mc.shot(shotName, q=True, cc=True)
    if mc.nodeType(camera) == 'camera':
        camera = mc.listRelatives(mc.shot(shotName, q=True, cc=True),p=True)[0]
    if mc.objExists(camera):
        key = mc.directionalLight(n='key_light2', rotation= (180, -107, 150), rgb= (0.404, 0.363, 0.299), i=1.8, rs=False)
        amb = mc.directionalLight(n='ambiet_light2', rotation= (-140, 80, -120), rgb= (0.769, 0.858, 1), i=0.8, rs=False)
        rim = mc.directionalLight(n='rim_light2', rotation= (-170, 46, -123), rgb= (0.849, 0.887, 0.637), i=1.3, rs=False)
        preview_grp = mc.group(key, amb, rim, n='preview_light')
        rotate = mc.xform(camera, rotation=True, q=True)
        mc.rotate(rotate[0], rotate[1], rotate[2],preview_grp)
        return preview_grp

def remove_preview_light(preview_grp):
    mc.delete(preview_grp) if mc.objExists(preview_grp) else None

def add_preview_light_playblast(camera):
    lightGrpName = 'preview_light'
    if mc.objExists(lightGrpName):
        remove_preview_light(lightGrpName)

    key = mc.directionalLight(n='key_light_pre', rotation= (180, -107, 150), rgb= (0.404, 0.363, 0.299), i=1.8, rs=False)
    amb = mc.directionalLight(n='ambiet_light_pre', rotation= (-140, 80, -120), rgb= (0.769, 0.858, 1), i=0.8, rs=False)
    rim = mc.directionalLight(n='rim_light_pre', rotation= (-170, 46, -123), rgb= (0.849, 0.887, 0.637), i=1.3, rs=False)
    preview_grp = mc.group(key, amb, rim, n='preview_light')
    rotate = mc.xform(camera, rotation=True, q=True)
    mc.rotate(rotate[0], rotate[1], rotate[2],preview_grp)
    logger.info('Preview light created for playblast')

    return preview_grp

def isolate_select(state = True):
    panel_cur =  mc.getPanel(wf=True)
    if not 'scriptEditorPanel' in panel_cur:
        mc.select(cl=True)
        mc.isolateSelect(panel_cur, state= state )

def get_audio_cutting(all_audios, seq_set):
    sound_data = []
    seq_start = min(seq_set)
    for sound in all_audios:
        fileName = pm.sound(sound, q=True, f=True)
        offset = int(round(pm.sound(sound, q=True, offset=True)))
        sourceStart = int(round(sound.sourceStart.get()))
        silence = int(round(sound.silence.get()))
        start = offset + silence 
        end = int(round(pm.sound(sound, q=True, endTime=True))) - 1
        duration = int(round(pm.sound(sound, q=True, sourceEnd=True)))
        sound_range = range(start, end+1)
        intersectionSet = seq_set.intersection(set(sound_range))
        if not intersectionSet:
            continue
        intersection_frames = list(intersectionSet)
        intersection_frames.sort()

        local_start = intersection_frames[0] - start
        local_end = intersection_frames[-1] - start - sourceStart
        global_start = intersection_frames[0] - seq_start
        # print(local_start, local_end)
        data = {'file': fileName, 
            'start': local_start + sourceStart,
            'global_start': global_start,
            'duration': duration,
            'end': local_end}
        sound_data.append(data)

    return sound_data

def get_current_timeSlider_sound():
    mm.eval('global string $gPlayBackSlider;')
    soundNode = mm.eval('timeControl -q -sound $gPlayBackSlider;')
    return soundNode

def get_display_sound_state():
    mm.eval('global string $gPlayBackSlider;')
    state = mm.eval('timeControl -q -ds $gPlayBackSlider;')
    return state

def get_all_audio():
    soundNode = get_current_timeSlider_sound()
    pb_state = get_display_sound_state()
    result = []
    if pb_state:
        # using only 1 sound from time slider
        if soundNode:
            soundNode = pm.nt.Audio(soundNode)
            if not pm.sound(soundNode, q=True, m=True) and os.path.exists(str(pm.sound(soundNode, q=True, f=True))):
                result = [soundNode]
        # using track sound
        else:
            # find sequencer
            sequencers = pm.ls(type='sequencer')
            if sequencers:
                sequencer = sequencers[0]
                seq_audios = pm.sequenceManager(listSequencerAudio=sequencer)
                if seq_audios:
                    result = [s for s in seq_audios if not pm.sound(s, q=True, m=True) and os.path.exists(str(pm.sound(s, q=True, f=True)))]

    return result

def generate_shot_audio(start, end, output_path, fps=24.0):
    seq_range = range(int(round(start)), (int(round(end)) +1))
    total_time = float(len(seq_range))/fps

    # get audio cutting data
    seq_set = set(seq_range)
    all_audios = get_all_audio()
    audio_data = get_audio_cutting(all_audios, seq_set)
    # print(audio_data)

    # cut audio clips
    cut_wavs = []  # [silent_path, (cut_path, clip_start_time), (...), ...]
    for data in audio_data:
        wav_input_path = data['file']
        local_start = data['start']
        local_end = data['end']
        duration = data['duration']
        global_start_frame = data['global_start']
        time_to_cut = float(local_start)/fps
        cut_duration = float((local_end+1) - local_start)/fps
        start_time = float(global_start_frame)/fps
        # print(time_to_cut, cut_duration)
        cut_audio_path = audio_utils.cut_wav(input_path=wav_input_path, 
                        time_to_cut=time_to_cut, 
                        cut_duration=cut_duration)
        cut_wavs.append((cut_audio_path, start_time))
        # print(cut_audio_path, start_time)

    # create silence clip 
    if cut_wavs:
        # overlay all clips onto silence clip
        output_path = audio_utils.mix_wavs(total_time=total_time, wavs=cut_wavs, output_path=output_path)
    else:
        output_path = audio_utils.generate_silent_wav(total_time, path=output_path)

    # print(output_path)
    return output_path

# playblast  -format qt -filename "P:/SevenChickMovie/RnD/project_setting/playblast/scm_act1_q0010_sxxx_anim.v002.mov" -forceOverwrite  -sequenceTime 0 -clearCache 1 -viewer 1 -showOrnaments 1 -offScreen  -fp 4 -percent 100 -compression "H.264" -quality 70 -widthHeight 1920 816;
# playblast  -format image -filename "playblast" -sequenceTime 0 -clearCache 1 -viewer 1 -showOrnaments 1 -offScreen  -fp 4 -percent 100 -compression "png" -quality 70 -widthHeight 960 540;
# playblast  -format image -filename "P:/SevenChickMovie/RnD/project_setting/playblast/seq2/name" -sequenceTime 0 -clearCache 1 -viewer 1 -showOrnaments 1 -offScreen  -fp 4 -percent 100 -compression "png" -quality 70 -widthHeight 960 540;

# examples
# play s0010, s0020 into one mov file
# playblast_sequencer('D:/test/shot.mov', shotNames=['s0010', 's0020'], size=(1920, 1080), format='qt', codec='H.264', combine=True)
# play s0010, s0020 into separated mov files
# playblast_sequencer('D:/test/shot.mov', shotNames=['s0010', 's0020'], size=(1920, 1080), format='qt', codec='H.264', combine=False)
# play s0010 and s0020 sequences into one dir
# playblast_sequencer('D:/test/shot.png', shotNames=['s0010', 's0020'], size=(1920, 1080), format='image', codec='png', combine=True)
# play s0010 and s0020 sequences to separated dir
# playblast_sequencer('D:/test/shot.png', shotNames=['s0010', 's0020'], size=(1920, 1080), format='image', codec='png', combine=False)