import maya.cmds as mc
import pymel.core as pm 


def unlock_animcurve_channel(): 
	""" This function check H / J hide graph and unlock it """ 
	nodes = list_anim_nodes()
	
	for node in nodes: 
		mc.setAttr('{}.ktv'.format(node), l=True)
		mc.setAttr('{}.ktv'.format(node), l=False)


def list_anim_nodes():
	""" This function list all anim nodes """  
	node_types = ['animCurveTU', 'animCurveTL', 'animCurveTA']
	nodes = list()

	for node_type in node_types: 
		curve_nodes = mc.ls(type=node_types)
		nodes.extend(curve_nodes)

	return nodes 