import maya.cmds as mc
import maya.mel as mm
import maya.OpenMaya as om
import os
import shutil

def resize_texture(sourceImage, outputImage, width, height):
    format = sourceImage.split('.')[-1]
    image = om.MImage()
    image.readFromFile('%s' %sourceImage)

    image.resize(width, height)
    image.writeToFile('%s' % outputImage , format)

def get_texture_size(sourceImage):
    image = om.MImage()
    image.readFromFile('%s' % sourceImage)

    scriptUtil = om.MScriptUtil()
    width = om.MScriptUtil()
    height = om.MScriptUtil()
    width.createFromInt(0)
    height.createFromInt(0)
    pWidth = width.asUintPtr()
    pHeight = height.asUintPtr()
    image.getSize(pWidth, pHeight)
    vWidth = width.getUint(pWidth)
    vHeight = height.getUint(pHeight)

    return vWidth, vHeight

