import maya.cmds as mc
import os
import sys
import re
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())


def import_ref():
    """ import all top level references """
    logger.debug('Run import_ref')
    excludes = ['_UNKNOWN_REF_NODE_', 'sharedReferenceNode', ':']
    rnNodes = mc.ls(type='reference')

    for rnNode in rnNodes:
        validRN = True
        for exclude in excludes:
            if exclude in rnNode:
                validRN = False
        if validRN:
            try:
                path = mc.referenceQuery(rnNode, f=True)
                mc.file(path, importReference=True)
            except RuntimeError as e:
                logger.error(e)

def rename_duplicate_namespace():
    """ rename duplicate namespace """
    refs = mc.file(q=True, r=True)
    refNamespace = [mc.referenceQuery(a, namespace=True).split(':')[-1] for a in refs]
    refDupPath = []
    refList = [] 
    for a in refs:
        refNs = mc.referenceQuery(a, namespace=True).split(':')[-1]
        if refNs in refList:
            refDupPath.append(a)
        else:
            refList.append(refNs)
    #check version
    if refDupPath:
        for path in refDupPath:
            ns = mc.referenceQuery( path, namespace=True )
            parentNs = mc.referenceQuery( path, parentNamespace=True )
            if parentNs:
                if parentNs[0]:
                    mainNs = ns.split(':')[-1]
                    suffix = re.findall('[0-9]+', mainNs)[-1]
                    #suffix = mainNs.split('_')[-1]
                    if suffix.isdigit():
                        version = int(suffix)
                        version = version + 1
                        newNs = mainNs.replace(suffix,('{:03}'.format(version)))
                        while newNs in refList:
                            version = version + 1
                            newNs = mainNs.replace(suffix,('{:03}'.format(version)))
                        refList.append(newNs)
                    else:
                        version = 1
                        newNs = '{}{:03}'.format(mainNs, version)
                        while newNs in refList:
                            version = version + 1
                            newNs = mainNs.replace(suffix,('{:03}'.format(version)))
                        refList.append(newNs)
                             
                    mc.namespace(rename = (ns , newNs), p=parentNs[0])
                    print 'Rename namespace {} to {} .'.format(ns, newNs)

def rename_duplicate_namespace_stacked():
    """ rename duplicate namespace for stacked namespace case """
    refs = mc.file(q=True, r=True)
    # refNamespace = [mc.referenceQuery(a, namespace=True).split(':')[-1] for a in refs]
    refDupPath = []
    refList = [] 
    listRefs = []
    for a in refs:
        refNs = mc.referenceQuery(a, namespace=True).split(':')[-1]
        # check for 1levels (standard) namespace
        if len(mc.referenceQuery(a, namespace=True).split(':')) <=2:
            refList.append(refNs)
        # check for objExists , namespace can't create with object exists
        elif mc.objExists(refNs):
            refList.append(refNs)
            listRefs.append(a)
        # lsit others refs
        else:
            listRefs.append(a)

    # loop check for duplicate namespace
    for a in listRefs:
        refNs = mc.referenceQuery(a, namespace=True).split(':')[-1]
        if refNs in refList:
            refDupPath.append(a)
        else:
            refList.append(refNs)
    #check version
    if refDupPath:
        for path in refDupPath:
            ns = mc.referenceQuery( path, namespace=True )
            parentNs = mc.referenceQuery( path, parentNamespace=True )
            if parentNs:
                if parentNs[0]:
                    mainNs = ns.split(':')[-1]
                    # suffix = re.findall('[0-9]+', mainNs)[-1]
                    oldNs = mainNs
                    while mainNs in refList or mc.objExists(mainNs):
                        version = ''
                        for num in mainNs[::-1]:
                            if num.isdigit():
                                version = num + version
                            else:
                                break
                        if version:
                            newVersion = int(version) + 1
                            newNs = mainNs.replace(version, '%03d'%newVersion)
                        else:
                            newNs = mainNs + '_001'
                        mainNs = newNs
                    mc.namespace(rename = ('%s:%s'%(parentNs[0],oldNs) , newNs), parent = parentNs[0])
                    print 'Rename namespace {} to {} .'.format('%s:%s'%(parentNs[0],oldNs), '%s:%s'%(parentNs[0],newNs))
                    refList.append(newNs)

def clear_stacked_namespace_in_scene():
    """ clear all stacked namespace in scene just 1 level """
    namespaces = non_ref_namespace()
    rename_duplicate_namespace_stacked()
    remove_namespaces2(namespaces)

def remove_namespaces():
    """ remove all namespaces """
    logger.debug('Run remove_namespaces')
    excludeNs = ['UI', 'shared']
    namespaces = mc.namespaceInfo(listOnlyNamespaces=True)
    namespaces = [a for a in namespaces if not a in excludeNs] if namespaces else []

    for namespace in namespaces:
        mc.namespace(f=True, mv=(':%s' % namespace, ':'))
        mc.namespace(removeNamespace=namespace)

def list_namespace():
    """ list scene namespaces """
    excludeNs = ['UI', 'shared']
    namespaces = mc.namespaceInfo(listOnlyNamespaces=True, recurse=True)
    namespaces = [a for a in namespaces if not a in excludeNs] if namespaces else []
    return namespaces

def non_ref_namespace():
    """ list non-reference namespace """
    refs = mc.file(q=True, r=True)
    refNamespace = [mc.referenceQuery(a, namespace=True)[1:] for a in refs]
    allNamespaces = list_namespace()
    nonRefNamespaces = [a for a in allNamespaces if not a in refNamespace]
    return nonRefNamespaces

def remove_namespaces2(namespaces):
    for namespace in reversed(namespaces):
        # mc.namespace(f=True, moveNamespace=(':%s' % namespace, ':'))
        mc.namespace(removeNamespace=namespace, mergeNamespaceWithRoot=True)

def list_duplicated_objects(group=''):
    """ find duplicated object name and return dict of duplicated objects """
    objs = []
    objsNoNs = dict()
    objs = mc.ls()
    objs = [a for a in objs if mc.objectType(a) in ['transform', 'mesh']]

    if mc.objExists(group) :
        objs = mc.ls(group, dag=True, l=True)
        # mc.select(group, hi = True)
        # objs = mc.ls(sl = True)

    dupObjects = dict()

    if objs :
        for eachObj in objs :
            rawName = eachObj.split(':')[-1].split('|')[-1]
            objType = mc.objectType(eachObj)
                
            # remove namespace section 
            if not rawName in objsNoNs.keys(): 
                objsNoNs[rawName] = {eachObj: objType}

            else: 
                objsNoNs[rawName].update({eachObj: objType})

                for k, v in objsNoNs[rawName].iteritems(): 
                    if not rawName in dupObjects.keys(): 
                        dupObjects[rawName] = {k: v}
                
                    else :
                        dupObjects[rawName].update({k: v})

            # if '|' in eachObj :
            #     name = eachObj.split('|')[-1]

            #     if not name in dupObjects.keys() :
            #         dupObjects[name] = {eachObj: objType}

            #     else :
            #         dupObjects[name].update({eachObj: objType})

    return dupObjects

def list_duplicated_objects_pm(group=''):
    """ find duplicated object name and return dict of duplicated objects """
    import pymel.core as pm
    if mc.objExists(group) :
        objs = pm.ls(group, dag=True, sn=True)
    dupObjects = {}
    for ix in objs:
        if '|' in ix.shortName():
            short_name = ix.split('|')[-1]
            if short_name in dupObjects:
                dupObjects[short_name].append(ix)
            else:
                dupObjects[short_name] = [ix]

    return dupObjects


def create_group():

    if not mc.objExists('Geo_Grp'):
        mc.group(em=True, n='Geo_Grp')

    if not mc.objExists('MdGeo_Grp'):
        mc.group(em=True, n='MdGeo_Grp')

    if not mc.objExists('PrGeo_Grp'):
        mc.group(em=True, n='PrGeo_Grp')

    if not mc.objExists('PvGeo_Grp'):
        mc.group(em=True, n='PvGeo_Grp')

    child = mc.listRelatives('Geo_Grp')

    if child:
        if not 'MdGeo_Grp' in child:
            mc.parent('MdGeo_Grp', 'Geo_Grp')
        if not 'PrGeo_Grp' in child:
            mc.parent('PrGeo_Grp', 'Geo_Grp')
        if not 'PvGeo_Grp' in child:
            mc.parent('PvGeo_Grp', 'Geo_Grp')
    else:
        mc.parent('MdGeo_Grp', 'Geo_Grp')
        mc.parent('PrGeo_Grp', 'Geo_Grp')
        mc.parent('PvGeo_Grp', 'Geo_Grp')


'''
import maya.OpenMaya as om

import time
import pymel.core as pm 
import maya.OpenMaya as om
def buildMatrix(mat):
    matrix = om.MMatrix()
    om.MScriptUtil.setDoubleArray(matrix[0], 0, mat[0][0])
    om.MScriptUtil.setDoubleArray(matrix[0], 1, mat[0][1])
    om.MScriptUtil.setDoubleArray(matrix[0], 2, mat[0][2])
    om.MScriptUtil.setDoubleArray(matrix[1], 0, mat[1][0])
    om.MScriptUtil.setDoubleArray(matrix[1], 1, mat[1][1])
    om.MScriptUtil.setDoubleArray(matrix[1], 2, mat[1][2])
    om.MScriptUtil.setDoubleArray(matrix[2], 0, mat[2][0])
    om.MScriptUtil.setDoubleArray(matrix[2], 1, mat[2][1])
    om.MScriptUtil.setDoubleArray(matrix[2], 2, mat[2][2])
    om.MScriptUtil.setDoubleArray(matrix[3], 0, mat[3][0])
    om.MScriptUtil.setDoubleArray(matrix[3], 1, mat[3][1])
    om.MScriptUtil.setDoubleArray(matrix[3], 2, mat[3][2])
    return matrix


# add...
# loop over all src objects
# ---- target 
# get MMatrix of target

mats = []
mtxs = []
for i in range(10000): 
    mtx = mc.xform('locator%s' % (i+1), q=True, m=True)
    mat = [list(a) for a in list(pm.PyNode('locator%s' % (i+1)).getMatrix())]
    mats.append(mat)
    mtxs.append(mtx)
    
start = time.time()
for i in range(10000): 
    mc.xform('pCube%s' % (i+1), m=mtxs[i])
    
print 'time %s' % (time.time() - start)
    
start = time.time()
for i in range(10000): 
    mat = mats[i]
    mmat = buildMatrix(mat)
    # instance MFnTransformation 
    tMat = om.MTransformationMatrix(mmat)
    
    # ---- source 
    # source of tranformation
    mSel = om.MSelectionList()
    mSel.add('pCube%s' % (i+1))
    
    mObj = om.MObject()
    mSel.getDependNode(0, mObj)  
    
    mfnTrans = om.MFnTransform(mObj)
    mfnTrans.set(tMat)

print 'finished in %s' % (time.time() - start)
'''