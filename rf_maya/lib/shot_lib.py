import os
import sys
import maya.cmds as mc
import maya.mel as mm
import pymel.core as pm 
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

class Plugin:
    abcExport = 'AbcExport.mll'
    abcImport = 'AbcImport.mll'

def import_abc(path):
    """ import alembic file """
    check_plugin(Plugin.abcExport)
    cmd = 'AbcImport -mode import "%s";' % path
    result = mm.eval(cmd)
    return path

def check_plugin(pluginName):
    """ load plugins """
    if not mc.pluginInfo(pluginName, q=True, l=True):
        mc.loadPlugin(pluginName, qt=True)

def reference_abc(namespace, path, rebuild=True):
    fileType = get_file_type(path)
    if not mc.namespace(ex=namespace):
        success = resolve_clash_namespace(namespace)
        if success: 
            result = mc.file(path, r=True, type=fileType, ns=namespace, f=True)
            logger.info('Reference abc %s' % path)
        else: 
            logger.error('Namespace inuse "%s". Please remove before build this asset' % namespace)
    else:
        namespaceDict = get_ref_namespaces()
        info = namespaceDict.get(':%s' % namespace)
        if info:
            currentPath, rnNode = info
            if rebuild:
                mc.file(currentPath, rr=True)
                result = mc.file(path, r=True, type=fileType, ns=namespace, f=True)
                return result
            result = mc.file(path, loadReference=rnNode, prompt=False)
            if currentPath.split('{')[0] == path:
                logger.info('Reload reference %s' % path)
            else:
                from rf_utils.context import context_info
                from rf_utils import project_info
                scene = context_info.ContextPathInfo(path=result)
                proj = project_info.ProjectInfo(scene.project)
                geoGrp = proj.asset.geo_grp()
                objName = namespace + ':'+geoGrp
                if mc.objExists('{}'.format(objName)): 
                    mc.setAttr(objName+'.visibility',1)
                    mc.parent(objName, w=True)
                logger.info('Replace reference %s -> %s' % (currentPath, path))
            return result
        else:
            mc.namespace(ex=namespace)
            mc.namespace(rm=namespace)
            success = resolve_clash_namespace(namespace)
            if success: 
                result = mc.file(path, r=True, type=fileType, ns=namespace, f=True)
                logger.info('Reference abc %s' % path)
            else: 
                logger.error('Namespace inuse "%s". Please remove before build this asset' % namespace)
            #logger.warning('Namespace is not a reference node, failed to use this namespace "%s"' % namespace)

def reload_reference(namespace): 
    members = mc.ls('%s:*' % namespace)
    if members: 
        path = mc.referenceQuery(members[0], f=True)
        rnNode = mc.referenceQuery(path, referenceNode=True)
        mc.file(path, unloadReference=rnNode)
        mc.file(path, loadReference=rnNode, prompt=False)

def resolve_clash_namespace(namespace): 
    """ if object name is the same as build namespace, try to rename it """ 
    suffix = '_ns'
    if not mc.objExists(namespace): 
        return True 

    if len(mc.ls(namespace)) > 1: 
        try: 
            for obj in mc.ls(namespace): 
                mc.lockNode(obj, l=False)
                mc.rename(obj, '%s%s' % (obj, suffix))
            return True
        except Exception as e: 
            return False  

    if not mc.referenceQuery(namespace, inr=True): 
        try: 
            mc.lockNode(namespace, l=False)
            mc.rename(namespace, '%s%s' % (namespace, suffix))
            return True 
        except Exception as e: 
            return False 
    return False 

def get_all_asset_references(): 
    refs = mc.file(q=True, r=True)
    assetRefs = list(set([a.split('{')[0] for a in refs]))
    return assetRefs

def get_file_type(path): 
    ext = os.path.splitext(path)[-1]
    fileTypeMap = {'Alembic': ['.abc'], 'mayaAscii': ['.ma'], 'mayaBinary': ['.mb']}

    for fileType, exts in fileTypeMap.iteritems(): 
        if ext in exts: 
            return fileType

def import_rig(namespace, path):
    if mc.namespace(ex=namespace):
        namespaceDict = get_ref_namespaces()
        info = namespaceDict.get(':%s' % namespace)

        if info:
            currentPath, rnNode = info
            mc.file(currentPath, rr=True)
        else:
            objs = mc.ls('%s*:*' % namespace)
            mc.delete(objs)

    return mc.file(path, i=True, type='mayaAscii', f=True, pr=True)

def import_reference(path, key=''):
    valid = False
    if key:
        if mc.objExists(key):
            return
        else:
            valid = True
    else:
        valid = True
    return mc.file(path, i=True, type='mayaAscii', f=True, pr=True, prompt=False) if valid else None

def remove_import_group(key):
    if mc.objExists(key):
        ls = mc.ls(key, dag=True)
        refNodes = [mc.referenceQuery(a, f=True) for a in ls if mc.referenceQuery(a, inr=True)]
        if not refNodes:
            mc.delete(key)
        else:
            removeRefs = list(set(refNodes))

            for ref in removeRefs:
                mc.file(ref, rr=True)

def get_ref_namespaces():
    paths = mc.file(q=True, r=True)
    namespaceDict = dict()
    for path in paths:
        namespace = mc.referenceQuery(path, ns=True)
        rnNode = mc.referenceQuery(path, referenceNode=True)
        namespaceDict[namespace] = [path, rnNode]
    return namespaceDict

def get_abc_file(scene, step, namespace, version):
    from rf_utils.context import context_info
    scene.context.update(step=step, process=namespace, publishVersion=version)
    cachePath = scene.path.scheme(key='cachePath').abs_path()

    # exception for camera
    if cam_pattern(namespace) and version == 'hero':
        cachePath = scene.path.scheme(key='heroPath').abs_path()

    files = list_file(cachePath, ['.abc'])
    relFiles = [context_info.RootPath(a).rel_path() for a in files]
    return files

def cam_pattern(namespace):
    # this is hard code to camera namespace pattern
    key = '_cam'
    if namespace[-len(key):] == key:
        return True

def build_abc(scene, step, namespace, version, rebuild=False):
    files = get_abc_file(scene, step, namespace, version)
    return reference_abc(namespace, files[-1], rebuild) if files else None

def remove_abc(namespace):
    path = get_ns_path(namespace)
    if path: 
        return mc.file(path, rr=True)
    logger.warning('Cannot get path from "%s"' % namespace)

def get_ns_path(namespace):
    skipNodes = ['_UNKNOWN_REF_NODE_']
    rnNodes = [a for a in mc.ls(type='reference') if not a in skipNodes]
    for node in rnNodes:
        if node == 'sharedReferenceNode':
            continue
        nodeNamespace = mc.referenceQuery(node, ns=True)
        if namespace in nodeNamespace:
            return mc.referenceQuery(node, f=True)

def build_set(root, setFile, overrideFile, namespace=''): 
    from rf_app.asm import asm_lib 
    from rf_utils.context import context_info
    from rftool.utils import maya_utils
    asset = context_info.ContextPathInfo(path=setFile)
    asset.context.update(process='main', step='set', res='md')

    # set hierarchy 
    setGrp = asset.projectInfo.asset.set_grp() or 'Set_Grp'
    if not maya_utils.mc.objExists(setGrp): 
        maya_utils.mc.group(em=True, n=setGrp)
    with maya_utils.MoveToWorld([setGrp]): 
        asm_lib.build_set(asset, namespace=namespace, buildAsset=False, mode='shot', force=False)
        asm_lib.import_override(root, overrideFile, build=True, force=False)
            # asm_lib.build(root, buildType='gpu', res='md')

def build_rig(scene, step, namespace, version):
    scene.context.update(step=step, process=namespace, publishVersion=version)
    cachePath = scene.path.scheme(key='cachePath').abs_path()
    files = list_file(cachePath, ['.ma'])
    return import_rig(namespace, files[-1])

def build_camera(namespace, path):
    reference_abc(namespace, path, False)
    cam = [a for a in mc.ls('%s:*' % namespace) if mc.objectType(a, isType='camera')]

    if cam:
        mc.lookThru(cam[0]) if not mc.about(batch=True) else None

        # try: 
        #     # set camera settings
        #     mc.setAttr('%s.filmFit' % cam[0], 1)
        #     mc.setAttr('%s.displayResolution' % cam[0], 1)
        #     mc.setAttr('%s.displayGateMaskColor' % cam[0], 0, 0, 0, type='double3')
        #     mc.setAttr('%s.displayGateMaskOpacity' % cam[0], 1)
        #     mc.setAttr('%s.overscan' % cam[0], 1)
        # except Exception as e: 
        #     logger.error('Cannot camera attributes. Maybe connections were locked')
        #     logger.debug(e)
        
        # create plate (if any)
        setup_cam_imagePlane(cam=cam[0])

        return cam[0]

def set_camera_value(camera, scene): 
    try: 
        # set camera settings
        mc.setAttr('%s.filmFit' % camera, scene.projectInfo.render.film_fit())
        mc.setAttr('%s.displayResolution' % camera, 1)
        mc.setAttr('%s.displayGateMaskColor' % camera, 0, 0, 0, type='double3')
        mc.setAttr('%s.displayGateMaskOpacity' % camera, 1)
        mc.setAttr('%s.overscan' % camera, scene.projectInfo.render.overscan())
    except Exception as e: 
        logger.error('Cannot camera attributes. Maybe connections were locked')
        logger.debug(e)
        

def setup_cam_imagePlane(cam):
    if not mc.objExists('%s.plateInfo' %cam):
        return

    cam = pm.PyNode(cam)
    plateInfoStrValue = cam.plateInfo.get()
    plateInfo = {}
    try:
        plateInfo = eval(plateInfoStrValue)
    except Exception, e:
        logger.error(e)
        logger.error('Cannot evaluate plateInfo str value on: %s' %cam.shortName())
        return

    # create the plate
    plate = pm.createNode('imagePlane')
    plateTr = plate.getParent()
    camTr = cam.getParent()
    pm.connectAttr(plate.message, cam.imagePlane[0], f=True)
    pm.connectAttr(cam.message, plate.lookThroughCamera, f=True)

    # needs to parent the imagePlane under the camera transform
    pm.matchTransform(plateTr, camTr, pos=True, rot=True, scl=True)
    plateTr.setParent(camTr)

    # set the attributes
    for attrName, value in plateInfo.iteritems():
        try:
            plate.attr(attrName).set(value)
        except:
            logger.warning('Cannot set imagePlane attr: %s, %s' %(attrName, value))

    # create frame extension expression
    if plate.frameExtension.get():
        exp = '%s.frameExtension = frame;' %(plate)
        expNode = pm.expression(s=exp, n='%s_exp' %plate.nodeName())

def build_sequencer(shotName, camera, duration, inpreRoll=0, inpostRoll=0):
    """ inpreRoll and inpostRoll will add to the duration length and add a transition to sequencer """ 
    from rf_maya.lib import sequencer_lib
    reload(sequencer_lib)
    startTime = 1001
    duration = duration + inpreRoll + inpostRoll
    endTime = startTime + duration - 1
    if not mc.objExists(shotName):
        shotName = mc.shot(shotName, startTime=startTime, endTime=endTime, sequenceStartTime=startTime, sequenceEndTime=endTime, currentCamera=camera)
    else:
        mc.setAttr('%s.startFrame' % shotName, startTime)
        mc.setAttr('%s.endFrame' % shotName, endTime)
        mc.setAttr('%s.sequenceStartFrame' % shotName, startTime)
        mc.setAttr('%s.sequenceEndFrame' % shotName, endTime)
        sequencer_lib.set_shot_camera(shotName, camera)
        
    mc.setAttr('%s.transitionInLength' % shotName, inpreRoll)
    mc.setAttr('%s.transitionOutLength' % shotName, inpostRoll)
    return shotName

def list_file(path, ext=[]):
    files = ['/'.join([path, d]) for d in os.listdir(path) if os.path.isfile('/'.join([path, d]))]
    if ext:
        return [a for a in files if os.path.splitext(a)[-1] in ext]
    return files

def switch(scene, namespace, version='hero', cache=False, rig=False):
    # new asset
    tagObj = scene.projectInfo.asset.tag_obj()
    heroAttr = scene.projectInfo.asset.hero_attr()

    if not mc.namespace(ex=namespace):
        if cache:
            build_abc(scene, scene.step, namespace, version)

    # existing
    if mc.namespace(ex=namespace):
        namespaceDict = get_ref_namespaces()
        namespaceInfo = namespaceDict.get(':%s' % namespace.replace(':', ''))

        if namespaceInfo:
            currentPath, rnNode = namespaceInfo
            path = ''
            if cache:
                logger.debug('Switching cache ...')
                files = get_abc_file(scene, scene.step, namespace, version)
                path = files[-1]

            if rig:
                logger.debug('Switching rig ...')
                # namespace:Geo_Grp.refPath
                attr = '%s:%s.%s' % (namespace, tagObj, heroAttr)
                if mc.objExists(attr):
                    path = mc.getAttr(attr)

            if path:
                result = mc.file(path, loadReference=rnNode, prompt=False)
            else:
                print 'path not found'
                logger.warning('Cannot find path. Switching abort')

def rn_has_key(rnNode):
    # guess if keyframe still connect to rnNode
    pass

def hidden_geo(namespace, geos, hidden=True): 
    if geos: 
        for geo in geos: 
            name = '%s:%s' % (namespace, geo)
            if mc.objExists(name): 
                try: 
                    node = pm.PyNode(name)
                    value = 0 if hidden else 1
                    node.visibility.unlock()
                    mc.setAttr('%s.visibility' % name, value)
                    node.visibility.lock()
                except Exception as e: 
                    logger.warning('Cannot setAttr %s.visibility. Could be locked ' % name)
                    logger.error(e)
            else: 
                logger.warning('Cannot hide "%s". It does not exists' % name)
