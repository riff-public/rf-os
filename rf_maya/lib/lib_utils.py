import os 
import sys 
from inspect import getmembers, isfunction

from rf_maya.lib import maya_lib
reload(maya_lib)


def run(name): 
	functionDict = list_function_dict()
	return functionDict[name]()

def list_function_name(): 
	functionsList = [o for o in getmembers(maya_lib) if isfunction(o[1])]
	return [name for name, func in functionsList]

def list_function_dict(): 
	functionsList = [o for o in getmembers(maya_lib) if isfunction(o[1])]
	functionDict = dict()

	for name, func in functionsList: 
		functionDict[name] = func 
	
	return functionDict

