# userSetup.py
# Author = Panupatc@gmail.com
# Based on script by Brent Tyler (tylerART)

import os, shutil, subprocess, sys
import maya.cmds as cmds
import maya.cmds as mc
import maya.mel as mel
import maya.utils as utils

###  Change Script Folder  ###
root = os.environ.get('RFSCRIPT')
scriptsFolder = '%s/legacy/lib/mayaMenu' % root

riffFolder = [
    '%s/legacy/lib/mayaMenu' % root,
    ]

for riff in riffFolder:
    sys.path.append(riff)
    for x in os.listdir(riff):
        if os.path.isdir(riff + "/" + x):
                sys.path.append(riff + "/" + x)

###  Create Menu UI  ###
def show(): 
    us_createScriptsMenu(scriptsFolder)
        
# scriptJobNum = cmds.scriptJob(event=["NewSceneOpened",menuImport])

def us_createScriptsMenu(scriptsFolder):
    
    if cmds.menu('RiFF', exists=1):
        cmds.deleteUI('RiFF')
    
    if scriptsFolder in sys.path:
        print ''
    else:
        sys.path.append(scriptsFolder)
    
    scriptsMenu = cmds.menu('RiFF', p='MayaWindow', to=1, aob=1, l='RiFF')
    cmds.menuItem(p=scriptsMenu, l="Update Menu", c='us_createScriptsMenu(scriptsFolder)')
    cmds.menuItem(p=scriptsMenu, l="Change Directory", c='us_changeDirectory()')
    cmds.menuItem(p=scriptsMenu, d=1)
        
    absoluteFiles = []
    relativeFiles = []
    folders = []
    allFiles = []
    currentFile = ''
        
    for root, dirs, files in os.walk(scriptsFolder):
        for x in files:
            correct = root.replace('\\', '/')
            currentFile = (correct + '/' + x)
            allFiles.append(currentFile)
            if currentFile.endswith('.mel'):
                relativeFiles.append(currentFile.replace((scriptsFolder + '/'), ""))
            if currentFile.endswith('.py'):
                relativeFiles.append(currentFile.replace((scriptsFolder + '/'), ""))
                        
    relativeFiles.sort()
    
    for relativeFile in relativeFiles:
        split = relativeFile.split('/')
        fileName = split[-1].split('.')
        i=0
        while i<(len(split)):
            ### Create Folders ###
            if i==0 and len(split) != 1:
                if cmds.menu(split[i] ,ex=1) == 0:
                    cmds.menuItem(split[i], p=scriptsMenu, bld=1, sm=1, to=1, l=split[i])
            if i > 0 and i < (len(split)-1):
                if cmds.menu(split[i] ,ex=1) == 0:
                    cmds.menuItem(split[i], p=split[i-1], bld=1, sm=1, to=1, l=split[i])
            
            ### Create .mel Files  ###
            if fileName[-1] == 'mel':
                if i==len(split)-1 and len(split) > 1:
                    scriptName = split[-1].split('.')
                    temp1 = 'source ' + '"' + scriptsFolder + '/' + relativeFile + '"; ' + scriptName[0]
                    command = '''mel.eval(''' + "'" + temp1 + '''')'''
                    cmds.menuItem(split[i], p=split[i-1], c=command, l=split[i])
                if i==len(split)-1 and len(split) == 1:
                    scriptName = split[-1].split('.')
                    temp1 = 'source ' + '"' + scriptsFolder + '/' + relativeFile + '"; ' + scriptName[0]
                    command = '''mel.eval(''' + "'" + temp1 + '''')'''
                    cmds.menuItem(split[i], p=scriptsMenu, c=command, l=split[i])
                
            ### Create .py Files  ###
            if fileName[-1] == 'py':
                if i==len(split)-1 and len(split) > 1:
                    command = 'import ' + fileName[0] + '\n' + fileName[0] + '.' + fileName[0]+ '()'
                    cmds.menuItem(split[i], p=split[i-1], c=command, l=split[i])
                if i==len(split)-1 and len(split) == 1:
                    command = 'import ' + fileName[0] + '\n' + fileName[0] + '.' + fileName[0]+ '()'
                    cmds.menuItem(split[i], p=scriptsMenu, c=command, l=split[i])
            i+=1
            

# utils.executeDeferred('us_createScriptsMenu(scriptsFolder)')

# evals = [
#     'source "InitDeadlineSubmitter.mel";',
#     'source "P:/sysTool/mel/igMenu.mel";',
# ]

# for e in evals:
#     try:
#         mel.eval(e)
#     except:
#         pass

