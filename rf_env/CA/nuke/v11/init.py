import os
import sys

# pipeline env 
moduleDir = os.path.dirname(sys.modules[__name__].__file__)
core = '%s/core' % os.environ.get('RFSCRIPT')
sys.path.append(core)

import nuke 
nuke.tprint('Run init from: "{}"'.format('{}/init.py'.format(moduleDir)))

import rf_config as config
