import os 
import sys 
from rf_utils import file_utils
from rf_utils.context import context_info


def move_pb(): 
	path = 'P:/CA/scene/work/ep102'
	shots = find_wrong_output(path)

	if shots: 
		for wrong_path in shots: 
			scene = context_info.ContextPathInfo(path=wrong_path)
			scene.context.update(process='main')
			pb_path = scene.path.scheme(key='workOutputPath').abs_path()
			pbfiles = file_utils.list_file(wrong_path)

			for file in pbfiles: 
				src = '{}/{}'.format(wrong_path, file)
				ver = check_filename(file)
				if not ver: 
					scene.context.update(publishVersion='v001')
					pb_name = scene.output_name(outputKey='playblast', hero=False)
					dst = '{}/{}'.format(pb_path, pb_name)
				else: 
					dst = '{}/{}'.format(pb_path, file)

				file_utils.copy(src, dst)
				print('Copy {} - {}'.format(src, dst))

			# print wrong_path, pb_files



def find_wrong_output(root, step='layout'): 
	""" output in wrong path -> shot/layout/output """ 
	wrong_shots = list()
	shots = file_utils.list_folder(root)
	for shot in shots: 
	    pb = '{}/{}/{}'.format(root, shot, step)
	    processes = file_utils.list_folder(pb)
	    
	    if 'output' in processes: 
	        wrong_path = '{}/output'.format(pb)
	        wrong_shots.append(wrong_path)

	return wrong_shots


def check_filename(filename): 
	""" check for no version on file name """ 
	import re
	result = re.findall(r'v\d{1,3}', filename)
	if result: 
		return result[0]

