import random
import maya.cmds as mc 
from rftool.utils import abc_utils
from rf_utils.context import context_info
from rf_utils import file_utils
# create
group = 'lightLoc_Grp'

def attach_balls(): 
    scene = context_info.ContextPathInfo()
    dst = 'P:/Bikey/asset/work/_global/crowd'
    dst_file = '{}/{}_{}.abc'.format(dst, scene.name, scene.process)
    group = 'lightLoc_Grp'
    if not mc.objExists(group): 
        mc.group(em=True, n=group)
    attach_ctrl = {'L_loc': 'WristFk_L_Ctrl', 'R_loc': 'WristFk_R_Ctrl'}
    # mc.file(geo_path, i=True, type='mayaAscii', mergeNamespacesOnClash=False, options='v=0')

    for loc, ctrl in attach_ctrl.items(): 
        loc = mc.spaceLocator(n=loc)[0]
        mc.parent(loc, group)
        ctrls = mc.ls('*:{}'.format(ctrl), l=True)
        target = None
        for c in ctrls: 
            if 'Export_Grp' in c: 
                target = c
                break

        mc.parentConstraint(target, loc)

    start = mc.playbackOptions(q=True, min=True)
    end = mc.playbackOptions(q=True, max=True)
    result = abc_utils.exportABC(group, dst_file, list_range = [start, end])
    print(dst_file)


def place_loc(): 
    locs = list()
    for a in mc.ls(sl=True): 
        m = mc.xform('{}.vtx[63]'.format(a), q=True, ws=True, t=True)
        loc = mc.spaceLocator()[0]
        mc.xform(loc, ws=True, t=m)
        locs.append(loc)
        mc.setAttr('{}.ry'.format(loc), -12)


def place_loop(locators, loops): 
    # path = 'P:/Bikey/asset/work/_global/crowd/baseAdultFemaleCrowd_cheerSitWave01.abc'
    for i, loc in enumerate(locators): 
        num = random.randint(0, len(loops)-1)
        ns = 'c%03d' % i
        mc.file(loops[num], i=True, ns=ns)
        target = '{}:{}'.format(ns, group)
        mc.delete(mc.parentConstraint(loc, target))


def random_alembic(start, end): 
    for node in mc.ls(type='AlembicNode'): 
        value = random.randint(start, end)
        mc.setAttr('{}.cycleType'.format(node), 1)
        mc.setAttr('{}.offset'.format(node), value)


def attach_bokeh(): 
    sources = ['src1', 'src2', 'src3'] # source geos to duplicate
    ctrls = ['L_loc', 'R_loc']
    for group in mc.ls(sl=True): 
        for ctrl in ctrls: 
            dup = mc.duplicate(random.choice(sources))[0]
            mc.parent(dup, group)
            ns = group.split(':')[0]
            dup = mc.rename(dup, '{}:{}_bokeh'.format(ns, ctrl))
            mc.pointConstraint('{}:{}'.format(ns, ctrl), dup)


def random_size(stand, end): 
    for each in mc.ls('*:*bokeh'): 
        v = random.uniform(stand, end)
        mc.setAttr('{}.scale'.format(each), v, v, v)


# def random(items, output): 
#     if output > len(items): 
#         item()
#     random_items = list(items)
#     random.shuffle(random_items)


def run(): 
    loop_path = 'P:/Bikey/asset/work/_global/crowd'
    files = ['{}/{}'.format(loop_path, a) for a in file_utils.list_file(loop_path)]
    locators = mc.ls(sl=True)
    place_loop(locators, files)
    # random_alembic(0, 40)
    # attach_bokeh()




