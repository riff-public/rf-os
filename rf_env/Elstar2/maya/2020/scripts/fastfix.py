import os
import maya.cmds as mc 
from rf_utils.context import context_info
from rf_utils import file_utils
from rf_utils import publish_info
from rf_app.export.asset_lib import export_rsproxy_anim_look
reload(export_rsproxy_anim_look)

from rf_app.export.asset_lib import export_maya_rsproxy_anim_look
reload(export_maya_rsproxy_anim_look)
from rf_maya.contextMenu.dag import shade_menu
reload(shade_menu)


def export_rsproxy(assetName, actionList=list()): 

    # assetName = 'baseAdultFemaleCrowd' # <<<<<<< AssetName, must change
    # actionList = ['idle03'] # <<<<<<, list of Action , must change
    rootPath = 'P:/Bikey/asset/work/char/{}/anim'.format(assetName)
    actionList = file_utils.list_folder(rootPath) if not actionList else actionList
    for action in actionList:
        rootPath = 'P:/Bikey/asset/work/char/{}/anim'.format(assetName)
        pathLoop = str()
        dirs = file_utils.list_folder(rootPath)
        
        for a in dirs: 
            if a.lower() == action.lower(): 
                pathLoop = '{}/{}/maya'.format(rootPath, a)

        if pathLoop: 
            animFiles = os.listdir(pathLoop)
            if animFiles: 
                lastFile = animFiles[-1]
                path = '{}/{}'.format(pathLoop,lastFile)
                print path
                print '########### start \n\n'

                try: 
                    mc.file(path, o=True,f=True)
                except: 
                    print('########### Open error')
                mc.editRenderLayerGlobals(currentRenderLayer='main_lookdev')
                
                sels = mc.ls(sl=True)
                shade_menu.multiple_switch(shadeKey='_ldv_',selection=sels)

                asset = context_info.ContextPathInfo(path=path)
                
                publishInfo = publish_info.PreRegister(asset)
                publishInfo.entity.context.update(publishVersion = asset.version, res='md')
                publishInfo.entity.path.publish_output()
                regData = publishInfo.register()
                publishInfo.hero_file(outputKey='rsproxySq')
                
                export_rsproxy_anim_look.run(publishInfo)
                
                #export_maya_rsproxy_anim_look.run(publishInfo)
                print('\n\nFinished {} {} ###################\n\n'.format(assetName, action))

            else: 
                print('\n\n Skipped No {} {} ###################'.format(assetName, action))
        else: 
            print(path)
            print('\n\nSkipped {} {} ######################\n\n'.format(assetName, action))


def run(): 
    assets = ['BaseAdultFemaleCrowd', 'BaseAdultFemaleOldCrowd', 'BaseAdultMaleCrowd', 'BaseAdultMaleOldCrowd']
    assets = ['baseAdultMaleCrowd']
    # loops = [   
    #             'Idle01', 
    #             'Idle02', 
    #             'Idle03', 
    #             'Sit01', 
    #             'Sit02', 
    #             'Sit03', 
    #             'StandTalk01',
    #             'sitTalk01', 
    #             'cheer01', 
    #             'cheer02', 
    #             'cheer03', 
    #             'cheer04', 
    #             'CheerSit01', 
    #             'CheerSit02', 
    #             'CheerSit03', 
    #             'CheerSit04', 
    #             'SitCheer01', 
    #             'SitCheer02'
    #             ]

    loops = [
        # 'cheerMale01',
        # 'cheerMale02',
        # 'cheerMale03',
        # 'cheerMale04',
        # 'cheerSitMale01',
        # 'cheerSitMale02',
        # 'cheerSitMale03',
        # 'cheerSitMale04',
        # 'main',
        # 'male talking 01',
        # 'maleIdle01',
        # 'maleIdle02',
        # 'maleIdle03',
        # 'maleSit01',
        # 'maleSit02',
        # 'maleSit03',
        # 'sitTalk01',
        'standTalk01',
    ]

    for asset in assets: 
        export_rsproxy(asset, loops)

