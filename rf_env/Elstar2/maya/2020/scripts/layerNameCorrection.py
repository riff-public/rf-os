import sys
import os
import pymel.core as pm

from PySide2 import QtWidgets, QtGui, QtCore
import os
import maya.cmds as mc

uiName = "LayerNameCorrection"
publish_root = "P:/Elstar2/scene/publ"

class MainWindow(QtWidgets.QDialog):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        self.setObjectName(uiName)
        self.setWindowTitle("Layer Name Correction")

        self.ep = self.getEp()
        self.shots = self.getShot(self.ep[0])
        
        self.__top_layout = QtWidgets.QGridLayout(self)
        self.__build_ui()
        self.__connect_signals()
                
        self.setFixedSize(600, 500)
        
    def __build_ui(self):
        top_wid = QtWidgets.QWidget(self)
        top_wid_lay = QtWidgets.QGridLayout(top_wid)
        ep_label = QtWidgets.QLabel("Ep :", self)
        sh_label = QtWidgets.QLabel("Sh :", self)
        self.ep_cbb = QtWidgets.QComboBox(self)
        self.ep_cbb.addItems(self.ep)
        self.shot_cbb = QtWidgets.QComboBox(self)
        self.shot_cbb.addItems(self.shots)


        top_wid_lay.addWidget(ep_label, 0, 0, 1, 1)
        top_wid_lay.addWidget(sh_label, 1, 0, 1, 1)
        top_wid_lay.addWidget(self.ep_cbb, 0, 1, 1, 1)
        top_wid_lay.addWidget(self.shot_cbb, 1, 1, 1, 1)

        mid_wid = QtWidgets.QWidget(self)
        mid_wid_lay = QtWidgets.QGridLayout(mid_wid)
        wrong_label = QtWidgets.QLabel("Wrong name", self)
        corr_label = QtWidgets.QLabel("Correct name :", self)
        self.wrong_listWidget = QtWidgets.QListWidget(self)
        self.wrong_listWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.corr_listWidget = QtWidgets.QListWidget(self)
        #self.corr_listWidget.setDisabled(True)
        self.rename_btn = QtWidgets.QPushButton("Fix it!", self)

        mid_wid_lay.addWidget(wrong_label, 0, 0, 1, 1)
        mid_wid_lay.addWidget(corr_label, 0, 1, 1, 1)
        mid_wid_lay.addWidget(self.wrong_listWidget, 1, 0, 1, 1)
        mid_wid_lay.addWidget(self.corr_listWidget, 1, 1, 1, 1)
        mid_wid_lay.addWidget(self.rename_btn, 2, 1, 1, 1)

        self.__top_layout.addWidget(top_wid)
        self.__top_layout.addWidget(mid_wid)

    def __connect_signals(self):
        self.ep_cbb.currentIndexChanged.connect(self.ep_change)
        self.shot_cbb.currentIndexChanged.connect(self.sh_change)
        self.rename_btn.clicked.connect(self.fix)
    
    def getEp(self):
        return os.listdir(publish_root)

    def getShot(self, ep):
        return os.listdir("{}/{}".format(publish_root, ep))

    def ep_change(self):
        self.shot_cbb.clear()
        self.ep = self.ep_cbb.currentText()
        self.shots = self.getShot(self.ep)
        self.shot_cbb.addItems(self.shots)

    def sh_change(self):
        self.wrong_listWidget.clear()
        self.corr_listWidget.clear()
        self.wrong_listWidget.setDisabled(False)
        self.corr_listWidget.setDisabled(False)

        self.ep = self.ep_cbb.currentText()
        self.shot = self.shot_cbb.currentText()
        fp = "{}/{}/{}/render/output".format(publish_root, self.ep, self.shot)
        self.layerData = self.layerNameData(fp)
        if not self.layerData:
            self.wrong_listWidget.addItem("Nothing...")
            self.corr_listWidget.addItem("Nothing...")
            self.wrong_listWidget.setDisabled(True)
            self.corr_listWidget.setDisabled(True)

        for k, v in self.layerData.iteritems():
            for ver in v:
                path = v[ver]["path"]
                wrong_name, correct_name = v[ver]["name"]
                wn = "{} | {}".format(ver, wrong_name)
                cn = "{} | {}".format(ver, correct_name)
                wrong_path = "{}/{}".format(path, wrong_name)
                correct_path = "{}/{}".format(path, correct_name)
                data = (wrong_path, correct_path)

                w_item = QtWidgets.QListWidgetItem()
                w_item.setText(wn)
                w_item.setData(QtCore.Qt.UserRole, data)

                self.wrong_listWidget.addItem(w_item)
                self.corr_listWidget.addItem(cn)

    def fix(self):
        selectedItems = self.wrong_listWidget.selectedItems()
        if not selectedItems:
            return
        for item in selectedItems:
            wrong_path, correct_path = item.data(QtCore.Qt.UserRole)
            try:
                os.rename(wrong_path, correct_path)
            except WindowsError:
                mc.warning("Files is being used somewhere else... Please ask your team to close it before rename it.")
                return
                
        msg = QtWidgets.QMessageBox(self)
        msg.setText("File(s) has been modified")
        msg.exec_()

        '''for item_index in range(self.wrong_listWidget.count()):
            data = self.wrong_listWidget.item(item_index).data(QtCore.Qt.UserRole)
            print data'''

    
    def layerNameData(self, fp):
        if not os.path.isdir(fp):
            return {}
        layer_data = {}
        for layer in os.listdir(fp):
            layer_path = "{}/{}".format(fp, layer)
            for ver in os.listdir(layer_path):
                ver_path = "{}/{}".format(layer_path, ver)
                if os.path.isfile(ver_path):
                    continue
                wrong_name = self.findWrongName(ver_path)
                if wrong_name:
                    if layer not in layer_data:
                        layer_data[layer] = {}
                    layer_data[layer].update({ver: {"name": wrong_name, "path": ver_path}})
                    
        return layer_data
            

    # find wrong name #
    def findWrongName(self, ver_path):
        exrs = [f for f in os.listdir(ver_path)]
        if len(exrs) <2:
            return
        x, y = exrs[0:2]
        if x.split(".")[0] == y.split(".")[0]:
            return
        f = ".".join(x.split(".")[1:])
        correct_name = y.split(".")[0]
        new_name = "{}.{}".format(correct_name, f)
        return x, new_name
                

def show():
    from rftool.utils.ui import maya_win
    maya_win.deleteUI(uiName)
    myApp = MainWindow(maya_win.getMayaWindow())
    myApp.show()
    
