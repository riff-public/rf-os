import os 
import sys 

root = 'O:/Pipeline/core'
sys.path.append(root)

import rf_config as config 
from rf_maya.environment import envparser

def __main__( *args ):
    # setup env 
    os.environ['RFPROJECT'] = 'P:'
    os.environ['RFPUBL'] = 'P:'
    os.environ['RFPROD'] = 'P:'
    os.environ['RFVERSION'] = 'R:'

    # project env 
    project = 'Elstar2'
    department = 'Farm'
    version = '2020'
    envs = envparser.setupEnv(project=project, department=department, version=version)