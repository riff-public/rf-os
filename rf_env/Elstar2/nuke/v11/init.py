import os
import sys


# pipeline env 
moduleDir = os.path.dirname(sys.modules[__name__].__file__)
core = '%s/core' % os.environ.get('RFSCRIPT')
sys.path.append(core)

import nuke 
nuke.tprint('Run init from: "{}"'.format('{}/init.py'.format(moduleDir)))

import rf_config as config

# ---- OCIO setup
'''
nuke.tprint('Setting up OCIO...')
# ocioPath = '{}/core/rf_lib/OpenColorIO-Configs/aces_1.2/config.ocio'.format(os.environ['RFSCRIPT'])
ocioPath = '{}/aces_1.2/config.ocio'.format(config.Env.openColorIO)
#os.environ['OCIO'] = ocioPath
nuke.knobDefault('Root.colorManagement', 'OCIO')
nuke.knobDefault('Root.OCIO_config', 'custom')
nuke.knobDefault('Root.customOCIOConfigPath', ocioPath)'''

nuke.tprint('--------------------------------')

# ---- Format setup
nuke.tprint('Setting up Format...')
nuke.knobDefault('Root.format', "1998 1080 0 0 1998 1080 1 Elstar2")
nuke.knobDefault('Root.lock_range', "true")
nuke.knobDefault('Root.fps', '30')
nuke.knobDefault("Viewer.viewerProcess", "sRGB")
nuke.tprint('--------------------------------')