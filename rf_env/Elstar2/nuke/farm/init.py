import os
import sys
import subprocess
import nuke 

# pipeline env 
moduleDir = os.path.dirname(sys.modules[__name__].__file__)
core = '%s/core' % os.environ.get('RFSCRIPT')
sys.path.append(core)
import rf_config as config
from rf_nuke.pipeline import env_setup
from rf_utils import screen_shot
from rf_utils import file_utils 
from rf_nuke import envparser

active_project = os.environ['active_project']
nuke_version = os.environ["nuke_version"]

def setupEnv():
	envparser.setupEnv(project=active_project, version=nuke_version)


def setupOcio():
	# ---- OCIO setup
	nuke.tprint('Setting up OCIO...')
	# ocioPath = '{}/core/rf_lib/OpenColorIO-Configs/aces_1.2/config.ocio'.format(os.environ['RFSCRIPT'])
	ocioPath = '{}/aces_1.2/config.ocio'.format(config.Env.openColorIO)
	#os.environ['OCIO'] = ocioPath
	nuke.knobDefault('Root.colorManagement', 'OCIO')
	nuke.knobDefault('Root.OCIO_config', 'custom')
	nuke.knobDefault('Root.customOCIOConfigPath', ocioPath)

def setupPlugins():
	# nuke default init for all projects
	# import all env

	# ----- rf_nuke
	# setup gizmos, icons, plugins 
	nuke_dir = '{}/rf_nuke'.format(core)
	pluginDirs = ['{}/{}'.format(nuke_dir, p) for p in ['_gizmos', '_icon', '_plugin']]
	env_setup.add_plugin_paths(pluginDirs)
	# add subdir of gizmos 
	gizmo_dir = '{dir}/_gizmos'.format(dir=nuke_dir)
	gizmoSubdirs = file_utils.list_folder(gizmo_dir)
	gizmoSubdirRelPath = ['{}/{}'.format(gizmo_dir, a) for a in gizmoSubdirs] if gizmoSubdirs else []
	env_setup.add_plugin_paths(gizmoSubdirRelPath) 

	# ----- O:/Pipeline/plugins/Nuke/shared_scripts
	# add global path
	global_dir = '{}/Pipeline/plugins/Nuke/shared_scripts'.format(config.Env.pipelineGlobal)
	gPluginDirs = ['{}/{}'.format(global_dir, p) for p in ['_gizmos', '_icon', '_plugin']]
	env_setup.add_plugin_paths(gPluginDirs)
	# add subdir of gizmos
	g_gizmo_dir = '{dir}/_gizmos'.format(dir=global_dir)
	g_gizmoSubdirs = file_utils.list_folder(g_gizmo_dir)
	g_gizmoSubdirRelPath = ['{}/{}'.format(g_gizmo_dir, a) for a in g_gizmoSubdirs] if g_gizmoSubdirs else []
	env_setup.add_plugin_paths(g_gizmoSubdirRelPath)

	os.environ['OPTICAL_FLARES_PRESET_PATH'] = '{}/plugins/Nuke/OpticalFlares_Nuke_11.3'.format(root)
	os.environ['OPTICAL_FLARES_LICENSE_PATH'] = '{}/plugins/Nuke/OpticalFlares_Nuke_11.3/license'.format(root)


def run():
	nuke.tprint('Run init from: "{}"'.format('{}/init.py'.format(moduleDir)))

	print "Setting up Env..."
	setupEnv()

	print "Settin gup OCIO..."
	setupOcio()

	print "Setting up Plugins..."
	setupPlugins()

run()