# nuke default init for all projects
# import all env
import subprocess
import os
import sys
import nuke 

# pipeline env 
moduleDir = os.path.dirname(sys.modules[__name__].__file__)
core = '%s/core' % os.environ.get('RFSCRIPT')
sys.path.append(core)
import rf_config as config
from rf_nuke.pipeline import env_setup
from rf_utils import screen_shot
from rf_utils import file_utils 

nuke.tprint('Running init from: "{}"'.format('{}/init.py'.format(moduleDir)))

# ----- rf_nuke
# setup gizmos, icons, plugins 
nuke.tprint('Setting up plugins...')
nuke_dir = '{}/rf_nuke'.format(core)
pluginDirs = ['{}/{}'.format(nuke_dir, p) for p in ['_gizmos', '_icon', '_plugin']]
env_setup.add_plugin_paths(pluginDirs)
# add subdir of gizmos 
gizmo_dir = '{dir}/_gizmos'.format(dir=nuke_dir)
gizmoSubdirs = file_utils.list_folder(gizmo_dir)
gizmoSubdirRelPath = ['{}/{}'.format(gizmo_dir, a) for a in gizmoSubdirs] if gizmoSubdirs else []
env_setup.add_plugin_paths(gizmoSubdirRelPath) 

# ----- O:/Pipeline/plugins/Nuke/shared_scripts
# add global path
global_dir = '{}/Pipeline/plugins/Nuke/shared_scripts'.format(config.Env.pipelineGlobal)
gPluginDirs = ['{}/{}'.format(global_dir, p) for p in ['_gizmos', '_icon', '_plugin']]
env_setup.add_plugin_paths(gPluginDirs)
# add subdir of gizmos
g_gizmo_dir = '{dir}/_gizmos'.format(dir=global_dir)
g_gizmoSubdirs = file_utils.list_folder(g_gizmo_dir)
g_gizmoSubdirRelPath = ['{}/{}'.format(g_gizmo_dir, a) for a in g_gizmoSubdirs] if g_gizmoSubdirs else []
env_setup.add_plugin_paths(g_gizmoSubdirRelPath) 

# callbacks
nuke.tprint('Setting up callbacks...')
nuke.addOnScriptLoad(env_setup.init_project_setting)
# nuke.addOnScriptSave(screen_shot.capture)

nuke.tprint('--------------------------------')