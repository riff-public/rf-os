# import all env
import subprocess
import os
import sys
core = '%s/core' % os.environ.get('RFSCRIPT')
sys.path.append(core)
import rf_config as config
from rf_nuke import envparser

active_project = os.environ['active_project']
nuke_version = os.environ["nuke_version"]

root = "O:/Pipeline"
initPrj = "{}/core/rf_env/{}/nuke/{}".format(root, active_project, nuke_version )
initDefault = "{}/core/rf_env/default/nuke/{}".format(root, nuke_version)


print "\tInit Project --> ", initPrj
print "\tInit Default --> ", initDefault

envparser.setupEnv(project=active_project, version=nuke_version)

os.environ['OPTICAL_FLARES_PRESET_PATH'] = '{}/plugins/Nuke/OpticalFlares_Nuke_11.3'.format(root)
os.environ['OPTICAL_FLARES_LICENSE_PATH'] = '{}/plugins/Nuke/OpticalFlares_Nuke_11.3/license'.format(root)

# nuke setup #
print "----------------------------------------------------"
print "             Setting up plugins path"
print "----------------------------------------------------"
nuke.pluginAddPath(initPrj)
nuke.pluginAddPath(initDefault)
