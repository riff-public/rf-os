import sys
import os

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
core = '%s/core' % os.environ.get('RFSCRIPT')
sys.path.append(core)

import nuke
nuke.tprint('Running menu from: "{}"'.format('{}/init.py'.format(moduleDir)))

from rf_nuke.commands import write_node, set_readNode_range
from utils import combineCrop, comp_menu, shared_comp, createNode
from rf_app.info_panel import nuke_dock
from app_runner import task_manager, scene_monitor
from rf_nuke._python import FromNuke2MayaExporter, backdropTools, collectFiles, openDir

nuke_dir = '{}/rf_nuke'.format(core)
nuke_menu = nuke.menu('Nuke')
node_toolbar = nuke.toolbar("Nodes")

# ----- Menus -----
# RiffPipeline menu
nuke.tprint('Adding menus...')
nuke_menu.addCommand('Riff Pipeline/Commands/Create Write Node(.exr) ', lambda: write_node.write_node_exr(), 'shift+e')
rf_menu = nuke_menu.findItem('Riff Pipeline')
nuke_menu.addCommand('Riff Pipeline/Commands/Create Write Node(.jpeg) ', lambda: write_node.write_node_jpeg(), 'ctrl+shift+e')
nuke_menu.addCommand('Riff Pipeline/Commands/Create Write Node(.dpx) ', lambda: write_node.write_node_dpx())
# nuke_menu.addCommand('Riff Pipeline/Commands/Set frame range ', lambda: set_node.set_frame_range(), 'ctrl+shift+f')
rf_menu.addSeparator()
nuke_menu.addCommand('Riff Pipeline/RF Panel', lambda: nuke_dock.show())
nuke_menu.addCommand('Riff Pipeline/Task Manager', lambda: task_manager.run())
nuke_menu.addCommand('Riff Pipeline/Scene Monitor', lambda: scene_monitor.run())
rf_menu.addSeparator()

nuke_menu.addCommand('Riff Pipeline/Shared Comp/Get Precomp', lambda: shared_comp.import_precomp())
sharecomp_menu = nuke_menu.findItem('Riff Pipeline/Shared Comp')
sharecomp_menu.addSeparator()
nuke_menu.addCommand('Riff Pipeline/Shared Comp/Import Comp', lambda: shared_comp.import_comp())
nuke_menu.addCommand('Riff Pipeline/Shared Comp/Export Comp', lambda: shared_comp.export_comp())
sharecomp_menu.addSeparator()

nuke_menu.addCommand('Riff Pipeline/Set ReadNode Range', lambda: set_readNode_range.setReadNodeRange(), 'ctrl+shift+r')

# AP definitions
AMenu = node_toolbar.addMenu('AP', icon='AP.png')
AMenu.addCommand('A_RestoreEdgePremult', 'nuke.createNode("A_RestoreEdgePremult")', icon='AP.png')

# Import and Export menu
import_export_menu = menubar.addMenu("Import and Export")
import_export_menu.addCommand("Export Camera as fm2n-File", "FromNuke2MayaExporter.FromNuke2MayaExporter()")

# Thinkbox Deadline menu 
try:
    from Client import DeadlineNukeClient
    tbmenu = nuke_menu.addMenu("&Thinkbox")
    tbmenu.addCommand("Submit Nuke To Deadline", DeadlineNukeClient.main, "")
    if nuke.env[ 'studio' ]:
        import DeadlineNukeFrameServerClient
        tbmenu.addCommand("Reserve Frame Server Slaves", DeadlineNukeFrameServerClient.main, "")
except:
    pass


backdropTools.addStandardShortcuts()

nuke.tprint('Adding toolbars...')
# ----- NODES MENU -----
custom_menu = node_toolbar.addMenu("Riff Pipeline", icon='{}/_icon/riff_logo.png'.format(nuke_dir))
cam_sub = custom_menu.addMenu("Camera", icon="{}/_icon/camera_icon.png".format(nuke_dir))
cam_sub.addCommand("Shot Camera", "createNode.shot_camera()", icon='{}/_icon/cameraRef_icon.png'.format(nuke_dir))

# ----- MASSIVE MENU -----
vMenu = node_toolbar.addMenu('MassiveNuke'  , icon='{}/_icon/Mass_Crop.png'.format(nuke_dir))
vMenu.addCommand('Combine Crop' , lambda: combineCrop.run() , '{}/_icon/Mass_Crop.png'.format(nuke_dir))

# ----- COMPOSITE MENU -----
comp_menu.add_gizmos()

# ----- Collect Files Menu Node -----
collectMenu = node_toolbar.addMenu("Collect_Files")
collectMenu.addCommand('Collect Files', 'collectFiles.collectFiles()')

# ----- Open Directory Menu -----
openDirMenu = node_toolbar.addMenu("Open_Directory", icon="{}/_icon/OpenDirectory.png".format(nuke_dir))
openDirMenu.addCommand("Open Directory", "openDir.openDir()")
openDirMenu.addCommand("Open With Pdplayer", "openDir.openSeq()", icon="{}/_icon/pdplayer.jpg".format(nuke_dir))
openDirMenu.addCommand("Open With RV", "openDir.openSeq('rvplayer')", icon="{}/_icon/rvLogo.png".format(nuke_dir))

# ----- VideoCopilot Menu -----
# optical flare
node_toolbar.addMenu("VideoCopilot", icon="{}/_icon/VideoCopilot.png".format(nuke_dir))
node_toolbar.addCommand( "VideoCopilot/OpticalFlares", "nuke.createNode('OpticalFlares')", icon="{}/_icon/OpticalFlares.png".format(nuke_dir))

nuke.tprint('--------------------------------')

