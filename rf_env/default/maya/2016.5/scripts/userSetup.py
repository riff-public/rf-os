# Riff userSetup.py for Maya 2015
import sys, os
import getpass
import logging
import maya.cmds as mc
import maya.mel as mm

from startup import setEnv
reload(setEnv)
from startup import job
reload(job)
from startup import mayaPref
reload(mayaPref)
from startup import mayaPlugin
reload(mayaPlugin)
from rftool.utils import log_utils
reload(log_utils)
from rf_maya.environment import envparser
reload(envparser)

appName = 'MayaStartup'
user = '%s-%s' % (os.environ.get('RFUSER'), getpass.getuser())
logFile = log_utils.name(appName, user=user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.INFO)
mayaVersion = mc.about(version=True)

def trace(message) :
    mm.eval('trace"%s";' % message)
    # print message

# print header
trace('# ----------------------------------------------')
trace('# Riff studio starting script from @userSetup.py')
trace('# ----------------------------------------------\\n')
logger.info('\n\nStarting Maya %s ...' % mayaVersion)

# set env
setEnv.set()

# set Qt.py
setEnv.setQt()

# set plugins
setEnv.setPlugins()

# start scriptJob
job.start()
trace('\\n# Start pipeline scriptJob ##\\n')

mayaPref.overridePref()
trace('\\n# Override maya preferences ##\\n\\n')

trace('\\n# Loading default plugins ...\\n\\n')
result = mayaPlugin.sync_plugin()

if not mc.about(batch=True):
    import rf_config
    from rf_utils import network_utils
    scriptDrive = rf_config.Env.pipelineGlobal
    # collect user
    if os.path.exists(scriptDrive) and network_utils.is_network_drive(scriptDrive):
        trace('\\n# User is at the studio ...')
        trace('\\n# Collecting user name ...\\n\\n')
        envparser.collectUser()
    else:
        trace('\\n# User is working from home ...\\n\\n')


trace('\\n# Finish ##\\n\\n')
logger.info('Finish userSet.py\n')

