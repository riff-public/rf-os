# Riff userSetup.py for Maya 2018
import sys, os
import getpass
import logging
import maya.cmds as mc
import maya.mel as mm

import rf_config
from startup import setEnv
reload(setEnv)
from startup import job
reload(job)
from startup import mayaPref
reload(mayaPref)
from startup import mayaPlugin
reload(mayaPlugin)
from rftool.utils import log_utils
reload(log_utils)
# from rf_maya.environment import envparser
# reload(envparser)

appName = 'MayaStartup'
user = '%s-%s' % (os.environ.get('RFUSER'), getpass.getuser())
logFile = log_utils.name(appName, user=user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.INFO)
mayaVersion = mc.about(version=True)

def trace(message) :
    mm.eval('trace"%s";' % message)

trace('# ----------------------------------------------')
trace('# Riff studio starting script from env/default/@userSetup.py')
trace('# ----------------------------------------------\\n')
logger.info('\n\nStarting Maya %s ...' % mayaVersion)

# set env
setEnv.set()

# set Qt.py
setEnv.setQt()

# set plugins
setEnv.setPlugins()

# start scriptJob
trace('\\n# Start pipeline scriptJob')
job.start()

trace('\\n# Override maya preferences')
mayaPref.overridePref()

trace('\\n# Loading default plugins ...')
result = mayaPlugin.sync_plugin()

trace('\\n# Report plugins')
mayaPlugin.report_plugin()

if not mc.about(batch=True):
    from rf_utils import network_utils
    scriptDrive = rf_config.Env.pipelineGlobal

    # collect user
    # trace('\\n# Checking location ...')
    # if os.path.exists(scriptDrive) and network_utils.is_network_drive(scriptDrive):
    #     trace('\\nUser is at the studio, collecting user name...')
    #     envparser.collectUser()
    # else:
    #     trace('\\nUser is working from home.')

    trace('\\n# Initialize custom UIs...')
    from rf_maya.startup import infoPanel
    mc.evalDeferred('infoPanel.show()')

    from rf_maya.startup import evaluationModeToggle
    mc.evalDeferred('evaluationModeToggle.show()')

    trace('\\n# Setup callbacks ...')
    from rf_maya.startup import reg_callback
    mc.evalDeferred('reg_callback.register()')

trace('\\n# Finish #\\n')
logger.info('Finish userSet.py\n')
