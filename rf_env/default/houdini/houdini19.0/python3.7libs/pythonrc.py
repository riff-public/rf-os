import sys
import os 
from importlib import reload

ROOT = os.environ['RFSCRIPT']
RFCORE = '{}/core'.format(ROOT)
SITEPACKAGES = '{}/core/rf_env/default/houdini/houdini19.0/scripts'.format(ROOT)
envs = [RFCORE, SITEPACKAGES]


def setProject():
    # project environment
    os.environ['RFPROJECT'] = 'P:'
    os.environ['RFPUBL'] = 'P:'
    os.environ['RFPROD'] = 'P:'
    os.environ['RFVERSION'] = 'R:'
    os.environ['DEADLINE_PATH'] = 'C:\\Program Files\\Thinkbox\\Deadline8\\bin'


def setPath(): 
	for path in envs: 
		if not path in sys.path: 
			sys.path.append(path)


def run(): 
	print('RF Pipeline initialized ...')
	setPath()
	setProject()
	print('RF Pipeline loaded.')

run()
