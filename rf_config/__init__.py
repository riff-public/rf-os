import sys
import os
import yaml

is_standalone = False
try:
    moduleDir = sys._MEIPASS.replace('\\', '/')
    is_standalone = True
except Exception:
    moduleDir = os.path.dirname(sys.modules[__name__].__file__)

# Maya detect
try:
    import maya.cmds as mc
    import maya.mel as mm
    isMaya = True
    try: 
        import ssl 
        ssl._create_default_https_context = ssl._create_unverified_context
        
    except AttributeError as e: 
        pass 


except ImportError:
    isMaya = False

# Nuke detect
try:
    import nuke
    isNuke = True
    try: 
        import ssl 
        ssl._create_default_https_context = ssl._create_unverified_context
    except AttributeError as e: 
        pass 

except ImportError:
    isNuke = False

# Deadline detect 
try: 
    from Deadline.Events import *
    isDeadline = True 

except ImportError:
    isDeadline = False

try: 
    import hou 
    isHoudini = True 
    
except: 
    isHoudini = False

# config parser
class ConfigFile:
    env = '{}/env_config.yml'.format(moduleDir)
    software = '{}/software_config.yml'.format(moduleDir)
    shotgun = 'O:/Pipeline/_config/sg_config.yml' # hardcode to this path and remove from Git
    maya = '{}/maya_config.yml'.format(moduleDir)
    outsource = '{}/outsource_config.yml'.format(moduleDir)

def readFile(file) :
    f = open(file, 'r')
    data = f.read()
    return data

def yml_loader(file):
    data = readFile(file)
    if sys.version_info.major >= 3:
        dictData = yaml.load(data, Loader=yaml.FullLoader)
    else:
        dictData = yaml.load(data)

    return dictData

def sys_append(path):
    if not path in sys.path:
        sys.path.append(path)


class Env:
    envConfig = yml_loader(ConfigFile.env)
    scriptVar = envConfig.get('SCRIPT')
    root = os.environ.get(scriptVar) if not is_standalone else moduleDir
    core = '{}/{}'.format(root, envConfig.get('environment').get('core'))
    package = '{}/{}'.format(root, envConfig.get('environment').get('package'))
    maya = '{}/{}'.format(root, envConfig.get('environment').get('maya'))
    nuke = '{}/{}'.format(root, envConfig.get('environment').get('nuke'))
    pyside = '{}/{}'.format(root, envConfig.get('environment').get('pyside'))
    qt = '{}/{}'.format(root, envConfig.get('environment').get('qt'))
    keyframePro = '{}/{}'.format(root, envConfig.get('environment').get('KeyframePro'))
    icon = '{}/{}'.format(root, envConfig.get('environment').get('icon'))
    user = os.environ.get(envConfig.get('SCRIPT'))
    standaloneEnv = envConfig.get('standalone')
    mayaEnv = envConfig.get('maya')
    nukeEnv = envConfig.get('nuke')
    deadlineEnv = envConfig.get('deadline')
    houdiniEnv = envConfig.get('houdini')
    userVar = envConfig.get('USER')
    projectVar = envConfig.get('PROJECT')
    publVar = envConfig.get('PUBL')
    versionVar = envConfig.get('VERSION')
    localuser = os.environ.get(userVar)
    serverMap = envConfig.get('serverMap')
    pipelineGlobal = envConfig.get('PIPELINEGLOBAL')
    virtualEnv = envConfig.get('virtualEnv')
    openColorIO = '{}/{}'.format(root, envConfig.get('environment').get('OpenColorIO'))

class Software:
    data = yml_loader(ConfigFile.software)
    styleSheets = '{}/{}'.format(Env.root, data.get('styleSheet'))
    defaultStyleSheet = '{}/{}'.format(Env.root, data.get('styleSheet').get('default'))
    mayaStyleSheet = '{}/{}'.format(Env.root, data.get('styleSheet').get('maya'))
    pythonPath = '{}/{}'.format(Env.root, data.get('python'))
    pythonwPath = '{}/{}'.format(Env.root, data.get('pythonw'))
    python3Path = '{}/{}'.format(Env.root, data.get('python3'))
    python3wPath = '{}/{}'.format(Env.root, data.get('python3w'))
    localPython = '{}'.format(data.get('localPython'))
    localPythonw = '{}'.format(data.get('localPythonw'))
    ffmpeg = '{}/{}'.format(Env.root, data.get('ffmpeg'))
    ffprobe = '{}/{}'.format(Env.root, data.get('ffprobe'))
    ffplay = '{}/{}'.format(Env.root, data.get('ffplay'))
    ocio = '{}/{}'.format(Env.root, data.get('ocio'))
    oiioTool = '{}/{}'.format(Env.root, data.get('oiioTool'))
    deadline = '{}'.format(data.get('deadline'))
    rsTextureProcessor = '{}/{}'.format(Env.root, data.get('rsTextureProcessor'))
    rv = '{}'.format(data.get('rv').get('path'))
    keyframePro = '{}'.format(data.get('keyframePro').get('path'))
    psexec = '{}/{}'.format(Env.root, data.get('psexec'))
    sevenZip = '{}/{}'.format(Env.root, data.get('7zip'))
    silentCmd = '{}/{}'.format(Env.root, data.get('silentCmd'))
    gservices = '{}/{}'.format(Env.root, data.get('virtualEnv').get('gservices'))

class Shotgun:
    keydata = yml_loader(ConfigFile.shotgun)
    server = keydata.get('server')
    script = keydata.get('api_key').get('default').get('script')
    id = keydata.get('api_key').get('default').get('id')
    getKey = keydata.get('api_key')
    ca_certs = keydata.get('ca_certs')

class Maya:
    data = yml_loader(ConfigFile.maya)
    env = data.get('envFile')
    env2015 = data.get('envFile2015')
    env2017 = data.get('envFile2017')


class Outsource: 
    data = yml_loader(ConfigFile.outsource)


class Studio: 
    current = os.environ.get('RFSTUDIO')


# add env
if isMaya:
    envs = Env.mayaEnv
elif isNuke: 
    envs = Env.nukeEnv
elif isDeadline: 
    envs = Env.deadlineEnv
elif isHoudini: 
    envs = Env.houdiniEnv
else:
    envs = Env.standaloneEnv

for env in envs:
    path = '{}/{}'.format(Env.root, Env.envConfig.get('environment').get(env))
    sys_append(path)


# sys_append(Env.core)
# sys_append(Env.package)
# sys_append(Env.qt)
# sys_append(Env.maya)

# if not isMaya:
#     sys_append(Env.pyside)
