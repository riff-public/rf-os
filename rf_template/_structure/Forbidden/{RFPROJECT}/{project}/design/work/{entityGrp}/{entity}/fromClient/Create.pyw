import subprocess
import os
import sys


core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)

# import config
import rf_config as config
from rf_utils import project_info
from rf_utils.context import context_info
from rf_utils.file_utils import back_slash_to_forwardslash


def main():
    # 'O:/Pipeline/core/rf_app/art/app2.py'
    # 'C:/Users/teeruk.i/projects/rf_pipleline_tools/core/rf_app/art/app2.py'
    current_path = os.getcwd()
    asset = context_info.ContextPathInfo(path=back_slash_to_forwardslash(current_path))

    if asset.project == 'CloudMaker':
        subprocess.call([
            'python',
            'O:/Pipeline/core/rf_app/art/app2.py',
            asset.project,
            asset.type,
            asset.parent,
            asset.name
        ])
    else:
        asset.context.update(process = 'main')
        subprocess.call([
            'python',
            'O:/Pipeline/core/rf_app/art/app2.py',
            asset.project,
            asset.type,
            asset.process,
            asset.name
        ])
if __name__ == '__main__':
    main()
