import logging
import os
import sys
import json

core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)
import rf_config as config
from rf_app.time_logged import summary
reload(summary)


TIMELOGGED_EVENT = 'Shotgun_CustomNonProjectEntity02_Change'
TIMETRACK_ENTITY = 'CustomNonProjectEntity01'
TIMELOGGED_ENTITY = 'CustomNonProjectEntity02'

EVENT_SYNC_CONFIG = [
        {'event': {
            'entity_type': TIMELOGGED_ENTITY, 
            'event_type': 'attribute_change', 
            'attribute_name': 'sg_duration_minute', 
            'sg_logged_type': 'User'}, 
        'trigger': {
            'entity_type': TIMETRACK_ENTITY, 
            'attribute_name': 'sg_duration_summary_event', 
            'linked_entity': 'sg_user_logged_items'
            }
            }, 
        {'event': {
            'entity_type': TIMELOGGED_ENTITY, 
            'event_type': 'attribute_change', 
            'attribute_name': 'sg_duration_minute', 
            'sg_logged_type': 'Automatic'}, 
        'trigger': {
            'entity_type': TIMETRACK_ENTITY, 
            'attribute_name': 'sg_duration_summary_event', 
            'linked_entity': 'sg_user_logged_items_1'
            }
            }
    ]


def registerCallbacks(reg):
    eventFilter = {
        TIMELOGGED_EVENT: 'sg_duration_minute'
    }
    reg.registerCallback(
        "sync_attrs.py",
        "opzgmPgcsxgwr3gzgqwinitu!",
        sync_attributes,
        eventFilter,
        None,
    )
    reg.logger.setLevel(logging.DEBUG)


def sync_attributes(sg, logger, event, args):
    src_event_type = event['meta']['type']
    src_entity_type = event['meta']['entity_type']
    src_attribute_name = event['meta']['attribute_name']
    new_value = event['meta']['new_value']
    new_value = event['meta']['new_value']
    src_entity = event['entity']

    logger.debug('sync_attributes')
    result = summary.sum_timetrack(src_entity['id'])
    logger.debug('sync completed')


def test(): 
    example = {
        'attribute_name': 'sg_duration_minute', 
        'event_type': 'Shotgun_CustomNonProjectEntity02_Change', 
        # 'created_at': datetime.datetime(2021, 1, 19, 13, 55, 32, tzinfo=<shotgun_api3.lib.sgtimezone.LocalTimezone object at 0x00000000036F9988>), 
        'entity': {'type': 'CustomNonProjectEntity02', 
            'id': 899, 
            'name': 'TA-Hanuman-storm-rig-2021:01:19'}, 
        
        'project': None, 
        'meta': {'entity_id': 899, 'attribute_name': 
            'sg_duration_minute', 
            'entity_type': 'CustomNonProjectEntity02', 
            'old_value': 20, 
            'field_data_type': 'number', 
            'new_value': 10, 
            'type': 'attribute_change'}, 
        
        'user': {'type': 'HumanUser', 
            'id': 85, 
            'name': 'Riff Admin'}, 
        
        'session_uuid': '8ce12314-5a22-11eb-b40a-0242ac110004', 
        'type': 'EventLogEntry', 
        'id': 12142027}
