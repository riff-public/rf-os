import logging
import os
import sys
import json
import requests

core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)
import rf_config as config

from rf_utils import project_info
from rf_utils.fb_workplace.settings import ACCESS_TOKEN
# from slackclient import SlackClient
# slack_token = "xoxp-450165884391-448934330772-491934688388-fd57698aa6f377dc2624c5e03de3fddc"
# slack_apis = SlackClient(slack_token)

reload(config)
reload(project_info)


ALLOW_PROJECT_DEV = ["projectName"]
ALLOW_PROJECT_PROD = ["projectName", "CloudMaker", "SevenChickMovie", "Ream", "ThreeEyes", "MI", "Hanuman", "HanumanSq4", "DarkHorse", "GoAwayMrTumor"]
GRAPH_URL_PREFIX = 'https://graph.facebook.com'


def registerCallbacks(reg):
    eventFilter = {
        'Shotgun_Note_New': None
    }
    reg.registerCallback(
        "new_note.py",
        "opzgmPgcsxgwr3gzgqwinitu!",
        new_note_event,
        eventFilter,
        None,
    )
    reg.logger.setLevel(logging.DEBUG)


def new_note_event(sg, logger, event, args):
    # project_filtered = {
    #     "type": "Project",
    #     "id": 134,
    #     "name": "projectName"
    # }
    if chekc_can_process(event):
        project_allowed = get_allow_project(event)
        logger.info("%s" % str(event))

        if project_allowed:
            note = get_note(sg, event)
            version_updated = update_status_version(sg, note)
            username = get_author_note_from_content(note)
            if username:
                human_user = get_human_user(sg, username, event)
                if human_user:
                    note_updated = update_note(sg, note, human_user)
                    logger.info("---------- note_updated : %s -----------" % str(note_updated));
                    # message_to_workplace_user(logger, note_updated, note)
                    # human_meta_data = json.loads(human_user['sg_meta_data'])
                    # print "note: ", note
                    # slack_apis.api_call(
                    #     "chat.postMessage",
                    #     channel=human_meta_data.get("slack_id"),
                    #     text="Project {project_name} Noted".format(project_name=event['project']['name']),
                    #     username="Friday"
                    # )
                # note_updated = update_note_anonymouse(sg, note)
                # note_updated = update_note(sg, note, human_user)
            # asset_version = get_version_asset(sg, note, event)


def message_to_workplace_user(logger, note_updated, current_note):
    # https://graph.facebook.com/me/messages
    endpoint = GRAPH_URL_PREFIX + "/me/messages"
    headers = buildHeader(ACCESS_TOKEN)
    payload = build_payload(logger, note_updated, current_note)
    result = requests.post(endpoint, headers=headers, data=json.dumps(payload))


def get_note(sg, event):
    project_filtered = event['project']
    note_filtered = [
        ['id', 'is', event['entity']['id']],
        ['project', 'is', project_filtered]
    ]
    fields = ['subject', 'note_links', 'tasks', 'content', 'user']

    note = sg.find_one('Note', note_filtered, fields)

    return note


# def get_version_asset(sg, note, event):
#     project_filtered = event['project']
#     version_detail = next((note_link for note_link in note['note_links'] if note_link['type'] == "Version"), None)
#     print "version_detail", version_detail
#     version_filtered = [
#         ['id', 'is', version_detail['id']],
#         ['project', 'is', project_filtered]
#     ]
#     fields = ['id', 'code', 'sg_status_list', 'entity']

#     note = sg.find_one('Version', version_filtered, fields)

#     return note


def update_status_version(sg, note):
    version_detail = next((note_link for note_link in note['note_links'] if note_link['type'] == "Version"), None)
    payload = {
        'sg_status_list': 'fix'
    }
    if version_detail:
        version_updated = sg.update('Version', version_detail['id'], payload)
        return version_updated
    return False


def get_author_note_from_content(note):
    if note['content']:
        username = note['content'][note['content'].find("[")+1:note['content'].find("]")]
        return unicode(username, errors='replace')
    else:
        return False


def update_note(sg, note, human_user):
    content_remove_user = note['content'].replace("[" + human_user['name'] + "]", '')
    payload = {
        "user": human_user,
        "content": content_remove_user
    }
    note_updated = sg.update('Note', note['id'], payload)

    return note_updated


# def update_note_anonymouse(sg, note):
#     human_user = {
#         'type': 'HumanUser',
#         'id': 293,
#         'name': 'Anonymous'
#     }
#     payload = {
#         "user": human_user
#     }
#     note_updated = sg.update('Note', note['id'], payload)

#     return note_updated


def get_human_user(sg, username, event):
    human_user_filtered = [
        ['name', 'is', username]
    ]
    fields = ['id', 'name', 'sg_meta_data']
    human_user = sg.find_one('HumanUser', human_user_filtered, fields)
    return human_user


def get_allow_project(event):
    if event['project']['name'] in ALLOW_PROJECT_PROD:
        return True
    return False


def chekc_can_process(event):
    if event['project'] is None:
        return False
    return True


def buildHeader(access_token):
    return {'Authorization': 'Bearer ' + access_token, 'Content-Type': 'application/json'}


def build_payload(logger, note_updated, current_note):
    ids_assigned = [100032090033045]
    if current_note['subject'] is None:
        current_note['subject'] = ""
    if note_updated['content'] is None:
        note_updated['content'] = ""
    data = {
      "recipient": {
        "ids": ids_assigned
      },
      "message": {
        "text": "you have note.! \n" + "Subject : " + current_note['subject'] + '\n' + "content: " + note_updated['content']
      }
    }
    return data
