import logging
import os
import sys


core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)
import rf_config as config

from rf_utils import project_info
# from rf_app.publish.asset import sg_hook
from rf_utils.sg import sg_process
# reload(sg_hook)
reload(config)
reload(project_info)


ALLOW_PROJECT_DEV = ["projectName"]
ALLOW_PROJECT_PROD = ["CloudMaker", "SevenChickMovie", "Ream", "ThreeEyes", "MI", "Hanuman", "HanumanSq4", "DarkHorse","GoAwayMrTumor"]
IGNORE_STATUS_LIST = ['wip']


def registerCallbacks(reg):
    eventFilter = {
        'Shotgun_Version_Change': "sg_status_list"
    }
    reg.registerCallback(
        "version_status_update.py",
        "gd3ndyqykdNzhjojpjinlfn*s",
        version_status_update_processor,
        eventFilter,
        None,
    )
    reg.logger.setLevel(logging.DEBUG)


def version_status_update_processor(sg, logger, event, args):
    if chekc_can_process(event):
        sg_process.sg = sg
        project_allowed = get_allow_project(event)


        # project_filtered = {
        #     "type": "Project",
        #     "id": 134,
        #     "name": "projectName"
        # }

        logger.info("%s" % str(event))

        if project_allowed and event['entity']:
            if event['meta']['old_value'] != event['meta']['new_value'] and event['meta']['new_value'] not in IGNORE_STATUS_LIST:
                version = get_version_asset(sg, event)
                if version and version['sg_task']:
                    task = get_task(sg, event, version)
                    if task:
                        if not version['sg_task']['name'] == 'anim':
                            sg_process.set_task_status(task['id'], event['meta']['new_value'])
                # if task:
                #     proj = project_info.ProjectInfo(project=project_filtered['name'])
                #     tasks_trigger_table = proj.task.dependency('asset')
                #     look_trigger_table = proj.task.look_dependency()
                #     taskEntity = sg_process.get_one_task_by_step(task['entity'], task['step']['name'], task['content'], create=False)
                #     result = update_dependency_event(sg, tasks_trigger_table, taskEntity, event)
                #     look_dependency(sg, look_trigger_table, taskEntity, event)


def get_version_asset(sg, event):
    project_filtered = event['project']
    # project_filtered = {
    #     "type": "Project",
    #     "id": 134,
    #     "name": "projectName"
    # }

    fields = ['code', 'sg_status_list', 'sg_task']
    version = sg.find_one("Version", [
        ['id', 'is', event['entity']['id']],
        ['project', 'is', project_filtered]],
        fields
    )

    return version


def get_task(sg, event, version):
    project_filtered = event['project']
    # project_filtered = {
    #     "type": "Project",
    #     "id": 134,
    #     "name": "projectName"
    # }

    fields = ['entity', 'step', 'content']
    task = sg.find_one("Task", [
        ['id', 'is', version['sg_task']['id']],
        ['project', 'is', project_filtered]],
        fields
    )

    return task


def get_allow_project(event):
    if event['project']['name'] in ALLOW_PROJECT_PROD:
        return True
    return False


def chekc_can_process(event):
    if event['project'] is None or event['meta']['old_value'] is None:
        return False
    return True
