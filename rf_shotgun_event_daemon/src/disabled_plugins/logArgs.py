import os
import logging


def registerCallbacks(reg):
    eventFilter = None
    reg.registerCallback(
        "logArgs.py",
        "9b998f019725f3ecea93c08cb8bf52880e1de3579632af762c4a219bb685ebe5",
        logArgs,
        eventFilter,
        None,
    )
    reg.logger.setLevel(logging.DEBUG)


def logArgs(sg, logger, event, args):
    logger.info("%s" % str(event))
