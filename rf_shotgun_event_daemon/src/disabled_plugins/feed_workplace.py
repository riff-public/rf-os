# -*- coding: utf-8 -*-

import requests
import logging
import os
import sys
import json

core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)
import rf_config as config

from rf_utils.fb_workplace.settings import ACCESS_TOKEN

# reload(config)
# reload(project_info)

ALLOW_PROJECT_DEV = ["projectName"]
ALLOW_PROJECT_PROD = ["projectName", "SevenChickMovie"]

GROUP_ID_WORKPLACE = {
    "SevenChickMovie": "1991432874239036",
    "projectName": "324409681749414"
}
# GROUP_ID_SEVENCHICKS_WORKPLACE = "1991432874239036"
GRAPH_URL_PREFIX = 'https://graph.facebook.com/'
GRAPH_VIDEO_URL_PREFIX = 'https://graph-video.facebook.com/v3.2/'
GROUPS_SUFFIX = '/groups'
# FIELDS_CONJ = '?fields=' 
# GROUP_FIELDS = 'id,name,members,privacy,description,updated_time'
EXT_FILES_MAP ={
    ".jpeg": 'picture',
    ".jpg": 'picture',
    ".png": 'picture',
    ".mov": 'vdo',
    ".avi": 'vdo'
}


def registerCallbacks(reg):
    eventFilter = {
        'Shotgun_Version_Change': "sg_uploaded_movie_transcoding_status"
    }
    reg.registerCallback(
        "feed_workplace.py",
        "m4gazgp~wiplwahzvpbxWiqyj",
        feed_workplace_event,
        eventFilter,
        None,
    )
    reg.logger.setLevel(logging.DEBUG)


def feed_workplace_event(sg, logger, event, args):
    if chekc_can_process(event):
        project_allowed = get_allow_project(event)
        logger.info("%s" % str(event))
        if project_allowed:
            version_obj = get_version_asset(sg, event)
            if version_obj:
                update_feed_workplace(version_obj, logger)
                # update_feed_sevenchick_workplace(version_obj, logger)


def get_version_asset(sg, event):
    project_filtered = event['project']
    fields = ['code', 'entity', 'sg_status_list', 'sg_task', 'image', 'user', 'project', 'sg_department', 'description', 'sg_uploaded_movie']
    version = sg.find_one("Version", [
        ['id', 'is', event['entity']['id']],
        ['project', 'is', project_filtered]],
        fields
    )

    return version


def get_allow_project(event):
    if event['project']['name'] in ALLOW_PROJECT_PROD:
        return True
    return False


def chekc_can_process(event):
    if event['meta']['old_value'] != 1 and event['meta']['new_value'] == 1:
        if event['project'] is not None and event['session_uuid'] is not None and event['entity'] is not None:
            return True
    return False


def update_feed_workplace(version_obj, logger):
    logger.info("version_obj: %s" % str(version_obj))
    # fri_tags_id = [100032090033045, 100032272415700,100032227480512,100023479520125]
    fri_tags_id = []
    tags_fri = ""

    headers = buildHeader(ACCESS_TOKEN)

    if len(fri_tags_id):
        for people_id in fri_tags_id:
            tags_fri = tags_fri + "@[{people_id}] ".format(people_id=people_id)

    payload = build_payload(version_obj, logger)
    logger.info("payload: %s" % str(payload))
    url_endpoint = get_endpoint(version_obj)
    result = requests.post(url_endpoint, headers=headers, data=payload)


def buildHeader(access_token):
    return {'Authorization': 'Bearer ' + access_token}


def build_payload(version_obj, logger):
    uploaded_movie = version_obj['sg_uploaded_movie']
    logger.info("uploaded_movie: %s" % str(uploaded_movie))
    data = {}
    msg = gen_msg(version_obj)
    if uploaded_movie:
        type_file = get_type_vdo(uploaded_movie)
        if type_file == "vdo":
            data.update({
                "description": msg,
                "file_url": uploaded_movie['url']
            })
        else:
            data.update({"message": msg})
            thumbnail = version_obj['image']
            if thumbnail:
                # data.update({"message": msg})
                data.update({"url": thumbnail})
    else:
        data.update({"message": msg})
        thumbnail = version_obj['image']
        if thumbnail:
            # data.update({"message": msg})
            data.update({"url": thumbnail})
    return data


def gen_msg(version_obj):
    if version_obj['sg_task'] is None:
        sg_task = "None"
    else:
        sg_task = version_obj['sg_task']['name']

    msg = "ผม published version ใหม่ '{version_name} task '{task}'' '{description}' ".format(
        version_name=version_obj.get('code') or "None",
        task=sg_task or "None",
        description=version_obj.get('description') or "None"
    )
    # msg = version_obj['code']
    msg += "\n"
    msg += "\n"
    msg += "#{department} #{task} #{user}".format(
        department=version_obj.get('sg_department') or "None",
        task=sg_task or "None",
        user=version_obj.get('user').get('name') or "None"
    )
    return msg


def get_type_vdo(uploaded_movie):
    filename, ext = os.path.splitext(uploaded_movie['name'])
    type_file = EXT_FILES_MAP.get(ext.lower())
    return type_file


def get_endpoint(version_obj):
    uploaded_movie = version_obj['sg_uploaded_movie']
    group_id = GROUP_ID_WORKPLACE[version_obj['project']['name']]
    if uploaded_movie:
        type_file = get_type_vdo(uploaded_movie)
        if type_file == "vdo":
            endpoint = GRAPH_VIDEO_URL_PREFIX + group_id + "/videos"
        else:
            endpoint = GRAPH_URL_PREFIX + group_id + "/photos"
    else:
        endpoint = GRAPH_URL_PREFIX + group_id + "/photos"
    return endpoint

# def update_feed_sevenchick_workplace(version_obj, logger):
#     if version_obj['project']['name'] == "SevenChickMovie":
#         fri_tags_id = []
#         tags_fri = ""

#         headers = buildHeader(ACCESS_TOKEN)
#         endpoint = GRAPH_URL_PREFIX + GROUP_ID_LAB_WORKPLACE + "/photos"

#         if len(fri_tags_id):
#             for people_id in fri_tags_id:
#                 tags_fri = tags_fri + "@[{people_id}] ".format(people_id=people_id)

#         payload = build_payload(version_obj, logger)
#         logger.info("version_obj: %s" % str(version_obj))
#         result = requests.post(endpoint, headers=headers, data=payload)
