# import all env 
import os 
import sys 
core = '%s/core' % os.environ.get('RFSCRIPT')
sys.path.append(core)
import rf_config as config
from rf_app.library_user import app


if __name__ == '__main__': 
	app.show(sg=False)
