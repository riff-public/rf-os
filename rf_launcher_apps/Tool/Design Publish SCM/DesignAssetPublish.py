# import all env 
import os 
import sys 
core = '%s/core' % os.environ.get('RFSCRIPT')
sys.path.append(core)
import rf_config as config

# run command
from rf_app.publish.seq import seq_app


if __name__ == '__main__': 
	seq_app.show()
