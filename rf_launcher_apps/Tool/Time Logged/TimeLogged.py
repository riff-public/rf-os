# import all env
import os
import sys
core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path: 
	sys.path.append(core)
import rf_config as config

# run command
from rf_app.time_logged.app import app

if __name__ == '__main__':
    app.show()
