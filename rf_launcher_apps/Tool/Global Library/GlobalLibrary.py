# import all env 
import os 
import sys 
core = '%s/core' % os.environ.get('RFSCRIPT')
sys.path.append(core)
import rf_config as config
from rf_app.global_library import app


if __name__ == '__main__': 
	app.show()
