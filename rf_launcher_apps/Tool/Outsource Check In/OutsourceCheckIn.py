# import all env
import os
import sys
core = '%s/core' % os.environ.get('RFSCRIPT')
sys.path.append(core)
import rf_config as config

# run command
# from rf_app.outsource_check_in import app
from rf_app.os2home import app
app.show()

if __name__ == '__main__':
    app.show()
