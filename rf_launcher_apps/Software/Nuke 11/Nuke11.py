# import all env
import subprocess
import os
import sys
core = '%s/core' % os.environ.get('RFSCRIPT')
sys.path.append(core)
import rf_config as config
from rf_nuke import envparser

if __name__ == '__main__':
    #project = os.environ['active_project']
    #version = 'v11'
    project = os.environ['active_project']
    os.environ["nuke_version"] = "v11"
    version = os.environ["nuke_version"]
    
    envparser.setupEnv(project=project, version=version)

    os.environ['OPTICAL_FLARES_PRESET_PATH'] = 'O:/Pipeline/plugins/Nuke/OpticalFlares_Nuke_11.3'
    os.environ['OPTICAL_FLARES_LICENSE_PATH'] = 'O:/Pipeline/plugins/Nuke/OpticalFlares_Nuke_11.3/license'

    subprocess.Popen([config.Software.data['Nuke11_3']['path']])
