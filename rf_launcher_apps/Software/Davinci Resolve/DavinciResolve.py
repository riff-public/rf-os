# import all env 
import subprocess
import os 
import sys
import time
core = '%s/core' % os.environ.get('RFSCRIPT')
sys.path.append(core)

import rf_config as config
from rf_maya.environment import envparser
from rf_utils import install_location

if __name__ == '__main__':
    project = os.environ['active_project']
    department = os.environ['active_department']
    exe_path = r'C:\\Program Files\\Blackmagic Design\\DaVinci Resolve\\Resolve.exe'
    if exe_path:
        subprocess.Popen([exe_path])
