# import all env 
import subprocess
import os 
import sys
import time
core = '%s/core' % os.environ.get('RFSCRIPT')
sys.path.append(core)

import rf_config as config
from rf_maya.environment import envparser
from rf_utils import install_location

if __name__ == '__main__':
    project = os.environ['active_project']
    department = os.environ['active_department']
    version = '2016.5'
    exe_path = install_location.get_maya_exe('Autodesk Maya 2016 Extension 2')
    if exe_path:
        envs = envparser.setupEnv(project=project, department=department, version=version, mergeWithDefault=False)
        subprocess.Popen([exe_path])
        # time.sleep(5)
        # envparser.revertEnv(version)
