# import all env 
import subprocess
import os 
import sys 
core = '%s/core' % os.environ.get('RFSCRIPT')
sys.path.append(core)
import rf_config as config
from rf_maya.environment import envparser

if __name__ == '__main__':
	envs = envparser.setupEnv(project=os.environ['active_project'], department=os.environ['active_department'], version='2018')
	subprocess.Popen([config.Software.data['Maya2018']['path']])
