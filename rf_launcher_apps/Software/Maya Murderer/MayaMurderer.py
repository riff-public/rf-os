# import all env 
import subprocess
import os 
import sys
import time
core = '%s/core' % os.environ.get('RFSCRIPT')
sys.path.append(core)

import rf_config as config
from rf_maya.environment import envparser
from rf_utils import install_location

if __name__ == '__main__':
    project = os.environ['active_project']
    department = os.environ['active_department']
    version = '2018'
    # exe_path = install_location.get_maya_exe('Autodesk Maya %s' %version)
    exe_path = r'C:\\Program Files\\Autodesk\\Maya2018\\bin\\maya.exe'
    if exe_path:
        envs = envparser.setupEnv(project=project, department=department, version=version)
        subprocess.Popen([exe_path])
