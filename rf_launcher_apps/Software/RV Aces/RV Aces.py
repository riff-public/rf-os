# import all env 
import subprocess
import os 
import sys
script_root = os.environ.get('RFSCRIPT') 
core = '{}/core'.format(script_root)
sys.path.append(core)
import rf_config as config

if __name__ == '__main__': 
    project = os.environ['active_project']
    version = '2021'
    ocio_version = 'aces_1.2'

    # copy python environments
    envs = os.environ.copy()

    # set OCIO var
    envs['OCIO'] = '{}/{}/config.ocio'.format(config.Env.openColorIO, ocio_version)

    # set RV.ini location
    proj_config = '{}/core/rf_env/{}/rv/{}'.format(script_root, project, version)
    default_config = '{}/core/rf_env/default/rv/{}'.format(script_root, version)
    envs['RV_PREFS_CLOBBER_PATH'] = proj_config if os.path.exists(proj_config) else default_config

    # launch RV via subprocess
    subprocess.Popen([config.Software.data['rv']['path']], env=envs)
