# import all env
import subprocess
import os
import sys
core = '%s/core' % os.environ.get('RFSCRIPT')
sys.path.append(core)
import rf_config as config
from rf_hou import envparser

if __name__ == '__main__':
    project = os.environ['active_project']
    version = '17.0'
    envparser.setupEnv(project=project, version=version)
    subprocess.Popen([config.Software.data['Houdini17']['path']])
