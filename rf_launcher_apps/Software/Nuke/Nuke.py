# import all env
import subprocess
import os
import sys
core = '%s/core' % os.environ.get('RFSCRIPT')
sys.path.append(core)
import rf_config as config

if __name__ == '__main__':
    # subprocess.Popen([config.Software.data['Nuke11Assist']['path'], "--nukeassist"])
    os.environ['Nuke_path'] = '{}/core/rf_nuke'.format(os.environ['RFSCRIPT'])
    # subprocess.Popen([config.Software.data['Nuke11_Assist']['path'], "--nukeassist"])
    subprocess.Popen([config.Software.data['Nuke11_Assist']['path']])
