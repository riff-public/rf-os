# import all env 
import os 
import sys 
core = '%s/core' % os.environ.get('RFSCRIPT')
sys.path.append(core)
import rf_config as config
import webbrowser

if __name__ == '__main__': 
	# url for design CloudMaker project
	url = 'https://riffanimation.shotgunstudio.com/page/13448'
	webbrowser.open(url)
