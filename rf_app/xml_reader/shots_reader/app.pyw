import sys
import os
import xml.dom.minidom

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)

# import config
import rf_config as config

from rf_utils.ui import load, stylesheet
from rf_utils.widget import browse_widget

from models.shots_reader_model import ShotsReaderModel
os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import QtWidgets, QtCore, QtGui

UI_PATH = "%s/view/shots_reader.ui" % moduleDir


class ShotsReaderCtrl(QtWidgets.QMainWindow):
    def __init__(self, args=None):
        super(ShotsReaderCtrl, self).__init__()
        self.ui = load.setup_ui(UI_PATH, self)
        self.init_state_app()
        self.init_signal()
        self.ui.show()

    def init_state_app(self):
        self.shots_model = ShotsReaderModel()
        self.fileFilter = 'Edit Cut(*.xml)'
        self.browser_file = browse_widget.BrowseFile(title='Sequence Xml')
        self.browser_file.set_label('Xml file : ')
        self.browser_file.filter = self.fileFilter
        self.ui.browseLayout.addWidget(self.browser_file)

    def init_signal(self):
        self.browser_file.textChanged.connect(self.onchange_browse_file)
        self.ui.export_btn.clicked.connect(self.export_file)
        self.ui.explorer_btn.clicked.connect(self.explorer_click)

    def onchange_browse_file(self):
        self.clear_shot_table()
        self.load_xml(self.browser_file.current_text())
        self.set_shots_table(self.shots_model.shots)

    def load_xml(self, file_path):
        DOMTree = xml.dom.minidom.parse(file_path)
        collection = DOMTree.documentElement
        clipitems = collection.getElementsByTagName("clipitem")
        for clip in clipitems:
            clip_name = clip.getElementsByTagName('name')[0]
            duration = clip.getElementsByTagName('duration')[0]
            cut_in = clip.getElementsByTagName('in')[0]
            cut_out = clip.getElementsByTagName('out')[0]
            start = clip.getElementsByTagName('start')[0]
            end = clip.getElementsByTagName('end')[0]

            duration = duration.childNodes[0].data
            working_duration = int(cut_out.childNodes[0].data) - int(cut_in.childNodes[0].data)
            cut_in_shot = str(int(cut_in.childNodes[0].data) + 1)
            cut_out_shot = cut_out.childNodes[0].data

            FIX_START_SHOT = 101

            data = {
                'shot_name': clip_name.childNodes[0].data,
                # 'duration': duration,
                # 'cut_in': cut_in_shot,
                # 'cut_out': cut_out_shot,
                'start': FIX_START_SHOT,
                'end': FIX_START_SHOT + working_duration - 1,
                'working_duration': working_duration
            }
            self.shots_model.append_shot_xml(data)

    def set_shots_table(self, shots):
        self.insert_row_shot_table(shots)
        for row, shot in enumerate(shots):
            self.ui.shots_table.setItem(row, 0, QtWidgets.QTableWidgetItem(shot.get('shot_name')))
            self.ui.shots_table.setItem(row, 1, QtWidgets.QTableWidgetItem(str(shot.get('start'))))
            self.ui.shots_table.setItem(row, 2, QtWidgets.QTableWidgetItem(str(shot.get('end'))))
            self.ui.shots_table.setItem(row, 3, QtWidgets.QTableWidgetItem(str(shot.get('working_duration'))))

    def clear_shot_table(self):
        self.shots_model.clear_shots()
        self.ui.shots_table.clearContents()
        self.ui.shots_table.setRowCount(0)

    def insert_row_shot_table(self, shots):
        for i in range(0, len(shots)):
            self.ui.shots_table.insertRow(i)

    def export_file(self):
        try:
            self.shots_model.export_file(self.browser_file.current_text())
            print "export !!"
        except Exception as e:
            QtWidgets.QMessageBox.critical(
                self,
                "Message",
                "Error:" + str(e) + ". Please contact admin."
            )
        finally:
            pass

    def explorer_click(self):
        self.shots_model.explorer_file()


def show():
    app = QtWidgets.QApplication.instance()
    if not app:
        app = QtWidgets.QApplication(sys.argv)
        stylesheet.set_default(app)
    myApp = ShotsReaderCtrl(sys.argv)
    # stylesheet.set_default(app)
    sys.exit(app.exec_())


if __name__ == '__main__':
    show()
