import os
import subprocess
import csv


class ShotsReaderModel:
    def __init__(self):
        self.init_state()

    def init_state(self):
        self.shots = []
        self.local_file_download_path = os.path.join(os.path.expanduser('~'), 'Downloads')

    def append_shot_xml(self, data):
        self.shots.append(data)

    def clear_shots(self):
        self.shots = []

    def export_file(self, browse_file_name):
        file_name = os.path.splitext(os.path.basename(browse_file_name))[0]
        file_path = "{local_file_download_path}\\{file_name}.csv".format(
            local_file_download_path=self.local_file_download_path,
            file_name=file_name
        )
        headers = ['shot_name', 'start', 'end', 'working_duration']
        with open(file_path, 'w') as output_file:
            dict_writer = csv.DictWriter(output_file, delimiter=',' , lineterminator='\n', fieldnames=headers)
            dict_writer.writeheader()
            dict_writer.writerows(self.shots)

    def explorer_file(self):
        subprocess.Popen(r'explorer %s' % self.local_file_download_path)
