# v.0.0.1 Ftp sync
_title = 'OS2Home'
_version = 'v.0.0.1'
_des = 'Resize'
uiName = 'OS2HomeUi'

#Import python modules
import sys
import os
import getpass
import logging
import subprocess
from functools import partial
import importlib
from . import *

# import config
import rf_config as config
from rf_utils.ui import stylesheet


module_dir = os.path.dirname(sys.modules[__name__].__file__)
appname = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui


class Module: 
    active_modules = ['_exr2home']


def import_module(name): 
    # func = __import__(name, globals(), locals(), [], -1)
    func = importlib.import_module(name)
    return func
   

class OSTool(object):
    """docstring for OSTool"""
    def __init__(self):
        super(OSTool, self).__init__()

    def read_modules(self): 
        modules = list()
        for mod in Module.active_modules: 
            func = import_module('rf_app.os2home.{}'.format(mod))
            modules.append((mod, func))
        return modules

    def run_func(self, func, parent): 
        importlib.reload(func)
        func.show(parent)


class OSToolUi(QtWidgets.QMainWindow):
    """docstring for ViewUi"""
    def __init__(self, parent=None):
        super(OSToolUi, self).__init__(parent)
        self.model = OSTool()
        self.setup_ui()
        self.setCentralWidget(self.widget)
        self.setObjectName(uiName)
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.resize(300, 200)

    def setup_ui(self): 
        self.widget = QtWidgets.QWidget()
        self.layout = QtWidgets.QVBoxLayout(self.widget)

        modules = self.model.read_modules()

        for name, func in modules: 
            name = name[1:]
            button = QtWidgets.QPushButton(name)
            button.setMinimumSize(QtCore.QSize(30, 30))
            button.clicked.connect(partial(self.model.run_func, func, self))
            self.layout.addWidget(button)

        
        
        
        
def show(parent=None): 
    if not parent: 
        main_app = QtWidgets.QApplication(sys.argv)
        app = OSToolUi()
        app.show()
        stylesheet.set_default(main_app)
        sys.exit(main_app.exec_())
    else: 
        app = OSToolUi(parent)
        app.show()


if __name__ == '__main__':
    show()

        
