# v.0.0.1 Ftp sync
_title = 'Exr2Home'
_version = 'v.0.0.1'
_des = 'Resize'
uiName = 'OS2HomeUi'

# to do 
# multi threading when copying 
# refresh item
# open explorer 
# pub status cannot send
# config separate file

#Import python modules
import sys
import os
import re
import getpass
import logging
import subprocess
from datetime import datetime
from collections import OrderedDict
from functools import partial
import importlib
from . import *
import lucidity

# import config
import rf_config as config
from rf_utils import thread_pool
from rf_utils import file_utils
from rf_utils.ui import stylesheet
from rf_utils.context import context_info
from rf_utils import admin
from rf_utils import custom_exception
from rf_utils.widget import project_widget
from rf_utils.sg import sg_process
sg = sg_process.sg
from . import app_config
Config = app_config.Config

module_dir = os.path.dirname(sys.modules[__name__].__file__)
appname = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui


# class Config: 
#     source = 'Source'
#     shot = 'Shot'
#     version = 'Version'
#     frame = 'Frame'
#     sgframe = 'SG Duration'
#     status = 'Job Status'
#     shot_status = 'Shot Status'
#     progress = 'Progress'
#     columns = [source, shot, version, frame, sgframe, shot_status, status, progress]
#     ready = 'Ready'
#     warning = 'Warning'
#     target_task = 'comp_full'
#     task_map_display = {'rev': 'Review', 'apr': 'Approved', 'pub': 'Pending Publish', 'wtg': 'Waiting to start'}
#     complete = 'Complete'
#     failed = 'Failed'
#     patterns = ['{ep}.{seq}.{shot}', '{ep}_{seq}_{shot}']


class PipelineModel(object):
    """docstring for PipelineModel"""
    def __init__(self):
        super(PipelineModel, self).__init__()
        self.regex_pattern = r'q\d{4}.+s\d{4}'
        self.step = 'comp'
        self.process = 'main'
        # self.default_pattern = '{ep}.{seq}.{shot}'
        self.target_template = lucidity.Template('shotname', 'hnm_{ep}_{seq}_{shot}')

    def analyse_input(self, project, paths, pattern): 
        data = OrderedDict()
        for path in paths: 
            statuses = list()
            folder_name = os.path.split(path)[-1]
            scene = self.detect_pattern(project, folder_name, pattern)
            output = scene.path.scheme(key='workOutputVersionPath').abs_path()
            filename = scene.output_name(outputKey='workOutput')
            dstpath = '{}/{}'.format(output, filename)

            seq_data = self.detect_image_sequence(path, dstpath)
            missing_frames = seq_data['missing_frames']
            shot_entity = self.get_shot_info(scene)
            seq_error = seq_data['error']

            # check data 
            scene_status = ''
            missing_frame_status = ''
            duration_status = ''
            task_status = ''
            status = Config.ready
            sg_duration = 0
            task_display = ''

            if shot_entity: 
                sg_duration = shot_entity['sg_working_duration']
                task_display = shot_entity['task_status']

                if shot_entity['sg_status_list'] == 'omt': 
                    statuses.append('Shot is omit')
                    scene_status = Config.warning
                    status = Config.warning

                if shot_entity['task_status'] in ['apr', 'pub', 'omt']: 
                    task_status = Config.ready
                    status = Config.warning
                    statuses.append('Already published')
                
                if task_display in Config.task_map_display.keys(): 
                    task_display = Config.task_map_display[task_display]

                if not sg_duration == seq_data['num']: 
                    duration_status = Config.warning
                    statuses.append('SG Duration not match')
                    status = Config.warning

            if not shot_entity: 
                statuses.append('Shot not found')
                scene_status = Config.warning
                status = Config.warning

            if missing_frames: 
                missing_frame_status = Config.warning
                statuses.append('Missing {}'.format(','.join(missing_frames)))


            if not seq_data['imgs']: 
                statuses.append('Images wrong format')
                status = Config.warning

            if seq_error: 
                statuses.append(seq_error)
                status = Config.warning

            if not statuses: 
                statuses = [Config.ready]

            data[scene.name] = {
                'src': (os.path.basename(path), path, None), 
                'dst': (scene.name, dstpath, scene_status), 
                'version': (scene.version, scene, None), 
                'imgs': seq_data['imgs'], 
                'missing_frames': (missing_frames, None, missing_frame_status),  
                'range': seq_data['range'],
                'num': (str(seq_data['num']), None, None),  
                'context': scene, 
                'sg_duration': (str(sg_duration), shot_entity, duration_status), 
                'task_status': (task_display, None, task_status),
                'status': ((', ').join(statuses), None, status) 
                }
            
        return data

    def detect_version(self, path): 
        new_version = 'v001'
        if os.path.exists(path): 
            dirs = file_utils.list_folder(path)
            versions = ([re.findall(r'\d{3}', a)[0] for a in dirs if re.findall(r'v\d{3}', a)])
            if versions: 
                new_version = 'v%03d' % (int(max(versions)) + 1)
        return new_version

    def detect_pattern(self, project, name, pattern): 
        # match = re.findall(self.regex_pattern, name)
        template = lucidity.Template('input', pattern)
        data = template.parse(name)

        if (all(v for k, v in data.items())): 
            entity = self.target_template.format(data)
            context = context_info.Context()
            context.update(
                project=project, 
                entityType='scene', 
                entityGrp=data['ep'], 
                entityParent=data['seq'], 
                entityCode=data['shot'], 
                entity=entity, 
                step=self.step, 
                process=self.process)
            scene = context_info.ContextPathInfo(context=context)
            dirname = scene.path.scheme(key='workOutputPath').abs_path()
            version = self.detect_version(dirname)
            scene.context.update(version=version)
            return scene

    def detect_image_sequence(self, dirname, name_pattern): 
        data = OrderedDict()
        frame_data = OrderedDict()
        missing_frames = list()
        files = file_utils.list_file(dirname)
        first_frame = 0
        last_frame = 0
        message = str()

        for file in files: 
            match = re.match(r'(.*\D)(\d+)(\..*)', file)
            if match:
                base, frame, ext = match.groups()
                if not frame in frame_data.keys(): 
                    src = '{}/{}'.format(dirname, file)
                    dst = name_pattern.replace('####', frame)
                    frame_data[frame] = {'src': src, 'dst': dst}
                else: 
                    # print('More than 1 sequence found. Stop')
                    message = 'More than 1 sequence found!! "{}"" and "{}"'.format(file, os.path.basename(frame_data[frame]['src']))
                    print(message)
                    # raise custom_exception.PipelineError(message)

        # check if any missing frame 
        if frame_data: 
            first_frame = list(frame_data.keys())[0]
            last_frame = list(frame_data.keys())[-1]

            for i in range(int(first_frame), int(last_frame)+1): 
                if not str(i) in frame_data.keys(): 
                    missing_frames.append(str(i))

            if missing_frames: 
                print('Frame are missing {}'.format(missing_frames))
            # raise custom_exception.PipelineError('Frame are missing {}'.format(missing_frames))
        data = {'imgs': frame_data, 'missing_frames': missing_frames, 'range': (first_frame, last_frame), 'num': len(frame_data.keys()), 'error': message}
        return data

    def get_shot_info(self, scene): 
        shot = sg.find_one('Shot', [
                ['project.Project.name', 'is', scene.project], 
                ['code', 'is', scene.name]], 
                ['sg_working_duration', 'sg_status_list'])
        if shot: 
            task = sg.find_one('Task', [
                ['entity', 'is', shot], 
                ['content', 'is', Config.target_task]], 
                ['sg_status_list'])
            shot['task_status'] = task['sg_status_list']
            shot['task'] = task
        return shot

    def create_workspace(self, scene): 
        scene.context.update(app='nuke', user='OS')
        dirname = os.path.split(scene.path.scheme(key='workPath').abs_path())[0]
        workfile = scene.work_name(user=False)
        # insert name 
        name, ext = os.path.splitext(workfile)
        workfile = '{}.{}{}'.format(name, 'OS', ext)
        filepath = '{}/{}'.format(dirname, workfile)

        if not os.path.exists(dirname): 
            os.makedirs(dirname)
        with open(filepath, 'w') as file:
            file.write('This file is created by OS checked in tool')

        return filepath
        # hnm_ch01_q0020_s0760_comp_main.v001.mArt.nk

    def set_task_status(self, shot_entity): 
        data = {'sg_status_list': 'pub'}
        return sg.update('Task', shot_entity['task']['id'], data)


class ToolControl(object):
    """docstring for ToolControl"""
    def __init__(self, ui):
        super(ToolControl, self).__init__()
        self.ui = ui 
        self.pipeline_model = PipelineModel()
        self.threadpool = QtCore.QThreadPool()
        self.threadpool.setMaxThreadCount(1)

    def file_drop(self, paths): 
        project = self.ui.project.current_project()
        pattern = self.ui.pattern_combo.currentText()

        # setup thread
        worker = thread_pool.Worker(self.fetch_data, project, paths, pattern)
        worker.kwargs['loop_callback'] = worker.signals.loopResult
        worker.signals.loopResult.connect(self.update_ui)
        # worker.signals.result.connect(self.fetch_sg_finished)
        self.threadpool.start(worker)

    def fetch_data(self, project, paths, pattern, loop_callback=None): 
        for path in paths: 
            item_data = self.pipeline_model.analyse_input(project, [path], pattern)
            current_shots = self.ui.exr_display_tree.list_item(Config.columns.index(Config.source))
            loop_callback.emit((item_data, current_shots))

    def update_ui(self, data): 
        self.ui.exr_display_tree.blockSignals(True)
        item_data, current_shots = data
        for name, data in item_data.items(): 
            src = data['src']
            dst = data['dst']
            frame_data = data['imgs']
            num = data['num']
            status = data['status']
            sg_duration = data['sg_duration']
            src_name = src[0]
            version = data['version']

            if not src_name in current_shots: 
                item = self.ui.exr_display_tree.add_folder(
                    source=src, 
                    shot=dst, 
                    version=version, 
                    frame=num, 
                    sg_duration=sg_duration, 
                    task_status=data['task_status'], 
                    status=status)

                if not status[0] == Config.ready: 
                    item.setFlags(item.flags() & ~ QtCore.Qt.ItemIsUserCheckable)

                for frame, img_data in frame_data.items(): 
                    src_frame = img_data['src']
                    dst_frame = img_data['dst']
                    self.ui.exr_display_tree.add_image_sequences(item, src_frame, dst_frame, frame)

        self.ui.exr_display_tree.blockSignals(False)

    def checked_in2(self, items): 
        self.threadpool.setMaxThreadCount(8)
        copy_list = list()
        for item in items: 
            for i in range(item.childCount()): 
                file_item = item.child(i)
                src = file_item.data(Config.columns.index(Config.source), QtCore.Qt.UserRole)
                dst = file_item.data(Config.columns.index(Config.shot), QtCore.Qt.UserRole)
                copy_list.append((file_item, src, dst))

        for data in copy_list: 
            file_item, src, dst = data 
            if not os.path.exists(os.path.dirname(dst)): 
                os.makedirs(os.path.dirname(dst))
            print('start worker', src)
            worker = thread_pool.Worker(self.copy_file, src, dst)
            worker.signals.result.connect(self.thread_finished)
            self.threadpool.start(worker)

    def copy_file(self, src, dst): 
        import shutil
        print('Copying ')
        print(src)
        # result = shutil.copy2(src, dst)
        # print(result)

        return dst

    def thread_finished(self, result): 
        print('thread finished', result)
        return 
        filename = os.path.basename(result)
        status = Config.complete if os.path.exists(result) else Config.failed
        status = Config.complete
        self.ui.exr_display_tree.update_file_status(filename, status)
        print(status)

    def checked_in(self, items): 
        # copy files 
        # setup thread
        # self.threadpool.setMaxThreadCount(8)
        worker = thread_pool.Worker(self.checkin_process, items)
        worker.kwargs['loop_callback'] = worker.signals.loopResult
        worker.signals.loopResult.connect(self.update_filecopy_ui)
        worker.signals.result.connect(self.thread_finished)

        self.threadpool.start(worker)
        # create nuke file 

    def checkin_process(self, items, loop_callback=None): 
        # copy file
        result = self.copy_exrs(items, loop_callback)

    def copy_exrs(self, items, loop_callback=None): 
        for item in items: 
            self.create_workfile(item)
            item_status = True

            for i in range(item.childCount()): 
                file_item = item.child(i)
                src = file_item.data(Config.columns.index(Config.source), QtCore.Qt.UserRole)
                dst = file_item.data(Config.columns.index(Config.shot), QtCore.Qt.UserRole)
                result = file_utils.copy(src, dst)
                loop_callback.emit((file_item, result, 'file'))
                if not result: 
                    item_status = False

            if item_status: 
                self.set_status(item)

            loop_callback.emit((item, item_status, 'shot'))
        return True

    def create_workfile(self, item): 
        scene = item.data(Config.columns.index(Config.version), QtCore.Qt.UserRole)
        self.pipeline_model.create_workspace(scene)

    def set_status(self, item): 
        shot_entity = item.data(Config.columns.index(Config.sgframe), QtCore.Qt.UserRole)
        self.pipeline_model.set_task_status(shot_entity)

    def update_filecopy_ui(self, data): 
        item, result, mode = data
        if mode == 'file': 
            if result: 
                item.setText(Config.columns.index(Config.status), Config.complete)
                self.ui.exr_display_tree.set_color(item, Config.columns.index(Config.status), Config.complete)
                status = Config.complete if os.path.exists(result) else Config.failed
                self.ui.exr_display_tree.update_file_status(os.path.basename(result), status)

        if mode == 'shot': 
            status = Config.complete if result else Config.failed
            item.setText(Config.columns.index(Config.status), status)
            self.ui.exr_display_tree.set_color(item, Config.columns.index(Config.status), status)
            self.ui.exr_display_tree.set_progress_bar(item, complete=True)

    def copy_finished(self, result): 
        # print('finished')
        # return 
        status = Config.complete if os.path.exists(result) else Config.failed
        self.ui.exr_display_tree.update_file_status(os.path.basename(result), status)
        print(status)

        if self.threadpool.activeThreadCount() == 0:
            print("All threads have finished")
            # self.ui.exr_display_tree.complete_status(os.path.basename(result))
        else: 
            print("{} threads running".format(self.threadpool.activeThreadCount()))


class Ui(QtWidgets.QMainWindow):
    """docstring for Ui"""
    def __init__(self, parent=None):
        super(Ui, self).__init__(parent)
        self.control = ToolControl(self)
        self.setup_ui()
        self.setCentralWidget(self.widget)
        self.setObjectName(uiName)
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.resize(1200, 800)
        self.init_signals()

    def setup_ui(self): 
        self.widget = QtWidgets.QWidget()
        self.layout = QtWidgets.QVBoxLayout(self.widget)
        self.head_layout = QtWidgets.QHBoxLayout()
        self.bottom_layout = QtWidgets.QGridLayout()
        self.button_layout = QtWidgets.QHBoxLayout()
        
        self.project = project_widget.SGProjectComboBox(sg, layout=QtWidgets.QHBoxLayout())
        self.patetrn_label = QtWidgets.QLabel('Pattern')
        self.pattern_combo = QtWidgets.QComboBox()
        self.head_spacer = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        self.ready_cb = QtWidgets.QCheckBox('Show Ready only')
        self.error_cb = QtWidgets.QCheckBox('Show Error only')

        self.exr_display_tree = EXRTree()
        self.exr_display_tree.setHeaderLabels(Config.columns)
        self.button_spacer = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.clear_button = QtWidgets.QPushButton('Clear')
        self.clear_button.setMinimumSize(QtCore.QSize(30, 30))
        self.send_button = QtWidgets.QPushButton('Send to Server')
        self.send_button.setMinimumSize(QtCore.QSize(30, 30))

        self.head_layout.addWidget(self.project)
        self.head_layout.addWidget(self.patetrn_label)
        self.head_layout.addWidget(self.pattern_combo)
        self.head_layout.addItem(self.head_spacer)
        self.bottom_layout.addWidget(self.ready_cb)
        self.bottom_layout.addWidget(self.error_cb)
        self.button_layout.addWidget(self.clear_button)
        self.button_layout.addItem(self.button_spacer)
        self.button_layout.addWidget(self.send_button)
        self.layout.addLayout(self.head_layout)
        self.layout.addWidget(self.exr_display_tree)
        self.layout.addLayout(self.bottom_layout)
        self.layout.addLayout(self.button_layout)

        self.exr_display_tree.setColumnWidth(0, 200)
        self.exr_display_tree.setColumnWidth(1, 220)
        self.exr_display_tree.setColumnWidth(2, 100)

        self.button_layout.setStretch(0, 1)
        self.button_layout.setStretch(1, 2)
        self.button_layout.setStretch(2, 1)

        self.exr_display_tree.setAlternatingRowColors(True)
        self.exr_display_tree.setStyleSheet("""
            QTreeView {
                alternate-background-color: rgb(60, 60, 60);
            }
        """)

        self.pattern_combo.addItems(Config.patterns)

    def closeEvent(self, event): 
        self.deleteLater()

    def init_signals(self): 
        self.exr_display_tree.dropped.connect(self.control.file_drop)
        self.exr_display_tree.itemExpanded.connect(self.item_expanded)
        self.ready_cb.stateChanged.connect(self.ready_checked)
        self.error_cb.stateChanged.connect(self.error_checked)
        self.exr_display_tree.itemChanged.connect(self.item_checked)
        self.send_button.clicked.connect(self.checked_in)
        self.clear_button.clicked.connect(self.clear_ui)


    def item_expanded(self): 
        self.exr_display_tree.resizeColumnToContents(Config.columns.index(Config.source))
        self.exr_display_tree.resizeColumnToContents(Config.columns.index(Config.shot))

    def ready_checked(self): 
        self.exr_display_tree.show_ready(self.ready_cb.isChecked())

    def error_checked(self): 
        self.exr_display_tree.show_error(self.error_cb.isChecked())


    def item_checked(self, item): 
        # print('item_checked')
        items = self.exr_display_tree.selectedItems()
        for sel_item in items: 
            if not item.text(0) == sel_item.text(0): 
                if sel_item.text(Config.columns.index(Config.status)) == Config.ready: 
                    sel_item.setCheckState(0, item.checkState(0))
                
    def checked_in(self): 
        items = self.exr_display_tree.get_checked_items()
        self.control.checked_in(items)

    def clear_ui(self): 
        self.exr_display_tree.clear()


class EXRTree(QtWidgets.QTreeWidget):
    """docstring for EXRTree"""
    dropped = QtCore.Signal(object)

    def __init__(self, parent=None):
        super(EXRTree, self).__init__(parent)
        self.setAcceptDrops(True)
        self.setSelectionMode(QtWidgets.QTreeWidget.SelectionMode.ExtendedSelection)

        self.root = self.invisibleRootItem()
        self.addTopLevelItem(self.root)

    def dragEnterEvent(self, event):
        if event.mimeData().hasUrls:
            event.accept()
        else:
            event.ignore()

    def dragMoveEvent(self, event):
        if event.mimeData().hasUrls:
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
        else:
            event.ignore()

    def dropEvent(self, event):
        if event.mimeData().hasUrls:
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
            links = list()
            for url in event.mimeData().urls():
                links.append(str(url.toLocalFile()))
            self.dropped.emit(links)

        else:
            event.ignore()

    def add_folder(self, source, shot, version, frame, sg_duration, task_status, status): 
        data_list = [source, shot, version, frame, sg_duration, task_status, status]
        items = self.list_children() or list()
        source_index = Config.columns.index(Config.source)
        item = QtWidgets.QTreeWidgetItem(self.root)
        item.setCheckState(0, QtCore.Qt.CheckState.Checked)

        for i, data in enumerate(data_list): 
            display, item_data, item_status = data
            item.setText(i, display)
            item.setData(i, QtCore.Qt.UserRole, item_data)
            color = self.get_color(item_status)
            if color: 
                item.setForeground(i, color)

        progress_bar = QtWidgets.QProgressBar()
        progress_bar.setMaximumSize(QtCore.QSize(120, 20))
        progress_bar.setVisible(False)
        self.setItemWidget(item, Config.columns.index(Config.progress), progress_bar)
        item.setData(Config.columns.index(Config.progress), QtCore.Qt.UserRole, 0)
        return item

    def set_progress_bar(self, item, increment_value=0, complete=False): 
        progress_bar = self.itemWidget(item, Config.columns.index(Config.progress))
        progress_bar.setVisible(True)
        current_value = item.data(Config.columns.index(Config.progress), QtCore.Qt.UserRole)
        value = current_value + increment_value if not complete else 100
        progress_bar.setValue(value)
        item.setData(Config.columns.index(Config.progress), QtCore.Qt.UserRole, value)

    def get_color(self, status): 
        if status == Config.ready: 
            return QtGui.QColor(100, 255, 100)

        if status == Config.warning: 
            return QtGui.QColor(255, 120, 120)

        if status == Config.complete: 
            return QtGui.QColor(100, 255, 100)

        if status == Config.failed: 
            return QtGui.QColor(255, 120, 120)

    def add_image_sequences(self, root, src, dst, frame): 
        item = QtWidgets.QTreeWidgetItem(root)
        display_src = os.path.basename(src)
        display_dst = os.path.basename(dst)

        # set display
        item.setText(Config.columns.index(Config.source), display_src) 
        item.setText(Config.columns.index(Config.shot), display_dst) 
        item.setText(Config.columns.index(Config.frame), str(frame)) 

        # set data 
        item.setData(Config.columns.index(Config.source), QtCore.Qt.UserRole, src)
        item.setData(Config.columns.index(Config.shot), QtCore.Qt.UserRole, dst)

        # set color 
        item.setTextColor(Config.columns.index(Config.source), QtGui.QColor(220, 220, 220))
        item.setTextColor(Config.columns.index(Config.shot), QtGui.QColor(220, 220, 220))
        item.setTextColor(Config.columns.index(Config.frame), QtGui.QColor(220, 220, 220))

    def list_children(self, root=None, visible_only=False): 
        all_items = list()
        if not root: 
            root = self.root
        count = root.childCount()
        if visible_only: 
            items = [root.child(a) for a in range(count) if not root.child(a).isHidden()]
        else: 
            items = [root.child(a) for a in range(count)]
        return items

    def list_item(self, root=None, column=0): 
        items = self.list_children(root)
        values = [a.text(column) for a in items] or list()
        return values 

    def list_item_data(self, root=None, column=0): 
        items = self.list_children(root)
        data_list = [a.data(column, QtCore.Qt.UserRole) for a in items] or list()
        return data_list 

    def set_warning_color(self, root=None, column=0): 
        root.setForeground(column, QtGui.QColor(255, 120, 120))

    def set_warning(self, root, data): 
        warning_columns = data['warning_column']
        if warning_columns: 
            for col in warning_columns: 
                column = Config.columns.index(col)
                self.set_warning_color(root, column)

    def set_color(self, root, column, status): 
        color = self.get_color(status)
        root.setForeground(column, color)

    def update_file_status(self, filename, status): 
        items = self.list_children(self.root)

        for item in items: 
            file_count = item.childCount()
            increment_value = (100.00 / file_count)
            for i in range(file_count): 
                file_item = item.child(i)
                if file_item.text(Config.columns.index(Config.shot)) == filename: 
                    file_item.setText(Config.columns.index(Config.status), Config.complete)
                    self.set_color(file_item, Config.columns.index(Config.status), status)
                    self.set_progress_bar(item, increment_value)
                    break

    def complete_status(self, filename): 
        items = self.list_children(self.root)

        for item in items: 
            if item.text(Config.columns.index(Config.shot)) in filename: 
                self.set_progress_bar(item, complete=True)

    def show_ready(self, state): 
        items = self.list_children(self.root)
        ready_items = list()

        for item in items: 
            if not item.text(Config.columns.index(Config.status)) == Config.ready: 
                self.setItemHidden(item, state)

    def show_error(self, state): 
        items = self.list_children(self.root)
        ready_items = list()

        for item in items: 
            if item.text(Config.columns.index(Config.status)) == Config.ready: 
                self.setItemHidden(item, state)


    def get_checked_items(self): 
        items = self.list_children()
        return [a for a in items if a.checkState(0) == QtCore.Qt.CheckState.Checked]

    def set_file_item(self, item, state): 
        if state: 
            text = Config.complete
            color = QtGui.QColor(160, 255, 160)
        else: 
            text = Config.failed
            color = QtGui.QColor(255, 140, 140)

        item.setText(Config.columns.index(Config.status), text)
        item.setForeground(Config.columns.index(Config.status), color)

def show(parent=None): 
    if not parent: 
        main_app = QtWidgets.QApplication(sys.argv)
        app = Ui()
        app.show()
        stylesheet.set_default(main_app)
        sys.exit(main_app.exec_())
    else: 
        app = Ui(parent)
        app.show()
