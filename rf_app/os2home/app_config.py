class Config: 
    source = 'Source'
    shot = 'Shot'
    version = 'Version'
    frame = 'Frame'
    sgframe = 'SG Duration'
    status = 'Job Status'
    shot_status = 'Shot Status'
    progress = 'Progress'
    columns = [source, shot, version, frame, sgframe, shot_status, status, progress]
    ready = 'Ready'
    warning = 'Warning'
    target_task = 'comp_full'
    task_map_display = {'rev': 'Review', 'apr': 'Approved', 'pub': 'Pending Publish', 'wtg': 'Waiting to start'}
    complete = 'Complete'
    failed = 'Failed'
    patterns = ['{ep}.{seq}.{shot}', '{ep}_{seq}_{shot}']