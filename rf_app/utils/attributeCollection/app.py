# --------- Change Log # --------- 
# - 1.0 initial version
# - 1.1 add include shape and history function
# -------------------------------- 

# Built-in modules
import os
import sys
import json
from functools import partial
from collections import OrderedDict

# Maya modules
import maya.OpenMayaUI as omui
import pymel.core as pm

# logger
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

# QT modules
import Qt
from Qt import wrapInstance, QtGui, QtCore, QtWidgets, QtCompat
# monkey patch for Qt compatibility 
if Qt.__binding__ in ('PySide2', 'PyQt5'):
    Qt.QtWidgets.QHeaderView.setResizeMode = Qt.QtWidgets.QHeaderView.setSectionResizeMode

# custom modules
from rftool.utils.ui import maya_win
from rf_utils.ui import load as loadUi

# Application variables
_uiName = 'AttributeCollectionWin'
_appTitle = 'Attr Collection'
_version = '1.1'

_moduleDir = os.path.dirname(sys.modules[__name__].__file__).replace('\\', '/')
_uiFile = '%s/main.ui' %_moduleDir
_home = os.environ['HOME'].replace('\\', '/')
_tempDir = '%s/.attributeCollection_tmp' %_home

_title_font = QtGui.QFont("Arial", 8, QtGui.QFont.Bold)
_title_font.setLetterSpacing(QtGui.QFont.AbsoluteSpacing, 1)

# -----------------------------
# --- Attribute control widgets
class BaseControl(QtWidgets.QWidget):
    def __init__(self, name='attrName', parent=None, parent_app=None, default_value=0):
        super(BaseControl, self).__init__(parent)
        # group box setup
        self.parent_app = parent_app
        self.name = name
        self.default_value = default_value

    def create(self):
        # main horizontal layout
        self.titleLayout = QtWidgets.QVBoxLayout(self)
        self.titleLayout.setContentsMargins(10, 0, 10, 0)
        self.titleLayout.setSpacing(0)
        self.layout = QtWidgets.QHBoxLayout()
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.layout.setSpacing(1)

        # name label
        self.label = QtWidgets.QLabel(self.name)
        self.label.setFont(_title_font)

        # content layout
        self.content = QtWidgets.QWidget()

        # button 
        self.set_button = QtWidgets.QPushButton()
        self.set_button.setText('Set')
        self.set_button.setMaximumSize(QtCore.QSize(30, 20))
        self.set_button.clicked.connect(self.set_value)

        # splitter
        self.line = QtWidgets.QFrame(self)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Plain)

        # add to layout
        self.titleLayout.addWidget(self.label)
        self.layout.addWidget(self.content)
        self.layout.addWidget(self.set_button)
        
        self.titleLayout.addLayout(self.layout)
        self.titleLayout.addWidget(self.line)

    def draw_drop_hilight(self, draw):
        # print draw, self
        if draw:
            self.line.setFrameShadow(QtWidgets.QFrame.Raised)
        else:
            self.line.setFrameShadow(QtWidgets.QFrame.Plain)

    def get_value(self):
        pass

    def set_value(self):
        include_shape = self.parent_app.includeShape_checkBox.isChecked()
        include_history = self.parent_app.includeHistory_checkBox.isChecked()

        sels = pm.selected()
        shapes = []
        if include_shape:
            shapes = []
            for s in sels:
                if s.hasAttr('getShape') and s.getShape():
                    shapes.append(s.getShape())
        histories = []
        if include_history:
            for s in sels:
                history = s.listHistory()
                if history:
                    histories.extend(history)
        return sels + shapes + histories

    def get_create_settings(self):
        self.name = str(self.dialog_lineEdit.text())
    
    def set_create_settings(self):
        self.label.setText(self.name)

    def create_dialog(self):
        self.dialog = QtWidgets.QDialog()
        self.dialog.setWindowTitle(self.__class__.__name__)
        # self.dialog.setMinimumSize(QtCore.QSize(150, 50))
        self.dialog_layout = QtWidgets.QVBoxLayout()

        # attribute name label
        self.dialog_name_layout = QtWidgets.QHBoxLayout()
        self.dialog_label = QtWidgets.QLabel('Attribute: ')
        self.dialog_name_layout.addWidget(self.dialog_label)

        # attribute name line edit
        self.dialog_lineEdit = QtWidgets.QLineEdit(self.name)
        self.dialog_lineEdit.setMinimumSize(QtCore.QSize(150, 20))
        self.dialog_lineEdit.selectAll()
        self.dialog_name_layout.addWidget(self.dialog_lineEdit)
        
        # buttons
        self.dialog_button_layout = QtWidgets.QHBoxLayout()
        self.dialog_buttons = QtWidgets.QDialogButtonBox()
        self.dialog_buttons.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        # connect 
        self.dialog_buttons.accepted.connect(self.dialog.accept)
        self.dialog_buttons.rejected.connect(self.dialog.reject)
        self.dialog_button_layout.addWidget(self.dialog_buttons)

        # add layout
        self.dialog_layout.addLayout(self.dialog_name_layout)
        self.dialog_layout.addLayout(self.dialog_button_layout)
        self.dialog.setLayout(self.dialog_layout)

        return self.dialog

class CheckBoxControl(BaseControl):
    def __init__(self, name='attrName', parent=None, parent_app=None, default_value=False):
        super(CheckBoxControl, self).__init__(name, parent, parent_app, default_value)

    def create(self):
        super(CheckBoxControl, self).create()
        self.contentLayout = QtWidgets.QVBoxLayout(self.content)
        self.checkbox = QtWidgets.QCheckBox(self.name)
        self.contentLayout.addWidget(self.checkbox)

        self.checkbox.setChecked(bool(self.default_value))
        self.checkbox.toggled.connect(self.set_value)

    def get_value(self):
        value = self.checkbox.isChecked()
        return value
        
    def set_create_settings(self):
        super(CheckBoxControl, self).set_create_settings()
        self.checkbox.setText(self.name)

    def set_value(self):
        nodes = super(CheckBoxControl, self).set_value()
        value = self.get_value()
        for obj in nodes:
            if obj.hasAttr(self.name):
                attr = obj.attr(self.name)
                try:
                    attr.set(value)
                except Exception, e:
                    # print e
                    pass

class LineEditControl(BaseControl):
    def __init__(self, name='attrName', parent=None, parent_app=None, default_value=0):
        super(LineEditControl, self).__init__(name, parent, parent_app, default_value)

    def create(self):
        super(LineEditControl, self).create()
        self.contentLayout = QtWidgets.QVBoxLayout(self.content)
        self.lineEdit = QtWidgets.QLineEdit()
        self.lineEdit.setMinimumSize(QtCore.QSize(30, 20))
        self.lineEdit.setAcceptDrops(False)
        self.contentLayout.addWidget(self.lineEdit)

        self.lineEdit.setText(str(self.default_value))
        self.lineEdit.returnPressed.connect(self.set_value)

    def get_value(self):
        value = self.lineEdit.text()
        return value

    def set_value(self):
        nodes = super(LineEditControl, self).set_value()
        value = self.get_value()
        for obj in nodes:
            if obj.hasAttr(self.name):
                attr = obj.attr(self.name)
                attr_type = attr.type()
                try:
                    if attr_type in ('float', 'double', 'doubleLinear', 'doubleAngle'):
                        v = float(value)
                    elif attr_type in ('short', 'long', 'bool'):
                        v = int(value)
                    elif attr_type in ('string'):
                        v = str(value)
                    elif attr_type in ('float2', 'double2', 'float3', 'double3', 'doubleArray'):
                        v = value.replace(' ', '')
                        splits = v.split(',')
                        v = [float(i) for i in splits if i]
                    elif attr_type in ('short2', 'long2', 'short3', 'long3', 'Int32Array'):
                        v = value.replace(' ', '')
                        splits = v.split(',')
                        v = [int(i) for i in splits if i]
                    else:
                        v = value

                
                    attr.set(v)
                except Exception, e:
                    # print e
                    pass

class LineEdit2Control(BaseControl):
    def __init__(self, name='attrName', parent=None, parent_app=None, default_value=(0, 0)):
        super(LineEdit2Control, self).__init__(name, parent, parent_app, default_value)

    def create(self):
        super(LineEdit2Control, self).create()
        self.contentLayout = QtWidgets.QHBoxLayout(self.content)
        self.lineEdit1 = QtWidgets.QLineEdit()
        self.lineEdit2 = QtWidgets.QLineEdit()

        self.lineEdit1.setMinimumSize(QtCore.QSize(15, 20))
        self.lineEdit2.setMinimumSize(QtCore.QSize(15, 20))

        self.lineEdit1.setAcceptDrops(False)
        self.lineEdit2.setAcceptDrops(False)

        self.contentLayout.addWidget(self.lineEdit1)
        self.contentLayout.addWidget(self.lineEdit2)

        self.lineEdit1.setText(str(self.default_value[0]))
        self.lineEdit1.returnPressed.connect(self.set_value)

        self.lineEdit2.setText(str(self.default_value[1]))
        self.lineEdit2.returnPressed.connect(self.set_value)

    def get_value(self):
        value1 = self.lineEdit1.text()
        if not value1:
            value1 = self.default_value[0]
        value2 = self.lineEdit2.text()
        if not value2:
            value2 = self.default_value[1]
        return (value1, value2)

    def set_value(self):
        nodes = super(LineEdit2Control, self).set_value()
        values = self.get_value()
        for obj in nodes:
            if obj.hasAttr(self.name):
                attr = obj.attr(self.name)
                attr_type = attr.type()
                try:
                    if attr_type in ('float2', 'double2'):
                        v = [float(i) for i in values]
                    elif attr_type in ('short2', 'long2'):
                        v = [int(i) for i in values]
                    else:
                        v = value
                
                    attr.set(v)
                except Exception, e:
                    # print e
                    pass

class LineEdit3Control(BaseControl):
    def __init__(self, name='attrName', parent=None, parent_app=None, default_value=(0, 0, 0)):
        super(LineEdit3Control, self).__init__(name, parent, parent_app, default_value)

    def create(self):
        super(LineEdit3Control, self).create()
        self.contentLayout = QtWidgets.QHBoxLayout(self.content)
        self.lineEdit1 = QtWidgets.QLineEdit()
        self.lineEdit2 = QtWidgets.QLineEdit()
        self.lineEdit3 = QtWidgets.QLineEdit()

        self.lineEdit1.setMinimumSize(QtCore.QSize(10, 20))
        self.lineEdit2.setMinimumSize(QtCore.QSize(10, 20))
        self.lineEdit3.setMinimumSize(QtCore.QSize(10, 20))

        self.lineEdit1.setAcceptDrops(False)
        self.lineEdit2.setAcceptDrops(False)
        self.lineEdit3.setAcceptDrops(False)

        self.contentLayout.addWidget(self.lineEdit1)
        self.contentLayout.addWidget(self.lineEdit2)
        self.contentLayout.addWidget(self.lineEdit3)

        self.lineEdit1.setText(str(self.default_value[0]))
        self.lineEdit1.returnPressed.connect(self.set_value)

        self.lineEdit2.setText(str(self.default_value[1]))
        self.lineEdit2.returnPressed.connect(self.set_value)

        self.lineEdit3.setText(str(self.default_value[2]))
        self.lineEdit3.returnPressed.connect(self.set_value)

    def get_value(self):
        value1 = self.lineEdit1.text()
        if not value1:
            value1 = self.default_value[0]
        value2 = self.lineEdit2.text()
        if not value2:
            value2 = self.default_value[1]

        value3 = self.lineEdit3.text()
        if not value3:
            value3 = self.default_value[2]
        return (value1, value2, value3)

    def set_value(self):
        nodes = super(LineEdit3Control, self).set_value()
        values = self.get_value()
        for obj in nodes:
            if obj.hasAttr(self.name):
                attr = obj.attr(self.name)
                attr_type = attr.type()
                try:
                    if attr_type in ('float3', 'double3'):
                        v = [float(i) for i in values]
                    elif attr_type in ('short3', 'long3'):
                        v = [int(i) for i in values]
                    else:
                        v = value
                
                    attr.set(v)
                except Exception, e:
                    # print e
                    pass

class CurveControl(BaseControl):
    def __init__(self, name='attrName', parent=None, parent_app=None, default_value='0.5, 0, 1, 0.5, 1, 1'):
        super(CurveControl, self).__init__(name, parent, parent_app, default_value)

    def create(self):
        super(CurveControl, self).create()
        values = [float(i) for i in self.default_value.replace(' ', '').split(',')]


        self.contentLayout = QtWidgets.QVBoxLayout(self.content)
        self.gradientSettingMain_layout = QtWidgets.QHBoxLayout()
        self.gradientSetting_layout = QtWidgets.QFormLayout()
        
        # selected position
        self.selPosLabel = QtWidgets.QLabel('Position')
        self.selPosLineEdit = QtWidgets.QLineEdit()
        # self.selPosLineEdit.setMinimumSize(QtCore.QSize(30, 20))
        self.selPosLineEdit.setMaximumSize(QtCore.QSize(100, 20))
        self.selPosLineEdit.setAcceptDrops(False)
        self.selPosLineEdit.setText(str(values[1]))
        # self.selPosLineEdit.setValidator(doubleValidator)

        # self.gradientSetting_layout.addRow(self.selPosLabel, self.selPosLineEdit)
        self.gradientSetting_layout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.selPosLabel)
        self.gradientSetting_layout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.selPosLineEdit)

        # selected value
        self.selValLabel = QtWidgets.QLabel('Value')
        self.selValLineEdit = QtWidgets.QLineEdit()
        # self.selValLineEdit.setMinimumSize(QtCore.QSize(30, 20))
        self.selValLineEdit.setMaximumSize(QtCore.QSize(100, 20))
        self.selValLineEdit.setAcceptDrops(False)
        self.selValLineEdit.setText(str(values[0]))
        # self.selValLineEdit.setValidator(doubleValidator)

        # self.gradientSetting_layout.addRow(self.selValLabel, self.selValLineEdit)
        self.gradientSetting_layout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.selValLabel)
        self.gradientSetting_layout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.selValLineEdit)

        # interpolation
        self.interpLabel = QtWidgets.QLabel('Interp')
        self.interpComboBox = QtWidgets.QComboBox()
        # self.interpComboBox.setMinimumSize(QtCore.QSize(30, 20))
        self.interpComboBox.setMaximumSize(QtCore.QSize(100, 20))
        self.interpComboBox.addItems(('None', 'Linear', 'Smooth', 'Spline'))
        self.interpComboBox.setCurrentIndex(int(values[2]))

        # self.gradientSetting_layout.addRow(self.interpLabel, self.interpComboBox)
        self.gradientSetting_layout.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.interpLabel)
        self.gradientSetting_layout.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.interpComboBox)

        self.get_button = QtWidgets.QPushButton('<<')
        self.get_button.setMaximumSize(QtCore.QSize(20, 20))

        # --- Create a gradient control
        temp_col = pm.columnLayout()
        self.gc = pm.gradientControlNoAttr('curveControl1', 
            parent=temp_col, h=80, 
            currentKeyChanged=lambda f: self.update_settings(), 
            dragCommand=lambda f: self.update_settings())

        self.gc.setAsString(self.default_value)

        ptr = omui.MQtUtil.findControl(str(self.gc.name()))
        self.gradient_control_widget = wrapInstance(long(ptr), QtWidgets.QWidget)
        # self.gradient_control_widget.setMinimumSize(QtCore.QSize(15, 50))

        self.gradientSettingMain_layout.addLayout(self.gradientSetting_layout)
        self.gradientSettingMain_layout.addWidget(self.get_button)
        self.contentLayout.addLayout(self.gradientSettingMain_layout)
        self.contentLayout.addWidget(self.gradient_control_widget)
        
        # self.contentLayout.setStretch(0, 1)
        # self.contentLayout.setStretch(1, 1)

        self.get_button.clicked.connect(self.get_from_selected)
        self.selValLineEdit.textChanged.connect(lambda f: self.validateInput(self.selValLineEdit))
        self.selPosLineEdit.textChanged.connect(lambda f: self.validateInput(self.selPosLineEdit))
        self.selValLineEdit.editingFinished.connect(self.set_graph_value)
        self.selPosLineEdit.editingFinished.connect(self.set_graph_position)
        self.interpComboBox.currentIndexChanged.connect(self.interp_changed)
    
    def get_from_selected(self):
        sels = pm.selected()
        if not sels:
            return
        obj = sels[0]
        if obj.hasAttr(self.name):
            exec('base_attr = obj.%s' %(self.name))
            result = []
            try:
                for i in base_attr.getArrayIndices():

                    attr_pos = base_attr[i].attr('{0}_Position'.format(self.name))
                    attr_value = base_attr[i].attr('{0}_FloatValue'.format(self.name))
                    attr_interp = base_attr[i].attr('{0}_Interp'.format(self.name))
                    result.append(str(attr_value.get()))
                    result.append(str(attr_pos.get()))
                    result.append(str(attr_interp.get()))

            except Exception, e:
                # print e
                pass

            if result:
                # print result
                resultStr = ','.join(result)
                self.gc.setAsString(resultStr)


    def get_value(self):
        return self.gc.getAsString()

    def interp_changed(self):
        curr_interp = self.interpComboBox.currentIndex()
        current_index = self.gc.getCurrentKey()
        current_str = self.gc.getAsString()
        graphs = [float(i) for i in current_str.split(',')]
        interp_index = (3 * current_index) + 2
        graphs[interp_index] = curr_interp
        new_str = ','.join([str(i) for i in graphs])

        self.gc.setAsString(new_str)

    def update_settings(self):
        current_index = self.gc.getCurrentKey()
        current_str = self.gc.getAsString()
        graphs = [float(i) for i in current_str.split(',')]
        value_index = 3 * current_index
        pos_index = value_index + 1
        interp_index = value_index + 2
        
        # update position and value
        self.selPosLineEdit.blockSignals(True)
        self.selValLineEdit.blockSignals(True)
        self.interpComboBox.blockSignals(True)

        self.selPosLineEdit.setText(str(graphs[pos_index]))
        self.selValLineEdit.setText(str(graphs[value_index]))
        self.interpComboBox.setCurrentIndex(int(graphs[interp_index]))

        self.selPosLineEdit.blockSignals(False)
        self.selValLineEdit.blockSignals(False)
        self.interpComboBox.blockSignals(False)
        return graphs

    def set_value(self):
        nodes = super(CurveControl, self).set_value()
        graphs = self.update_settings()
        pm.undoInfo(openChunk=True)
        for obj in nodes:
            if obj.hasAttr(self.name):
                # base_attr = obj.attr(self.name)
                # pymel gives differenct result if the attribute is under a node's shape
                # Needs to use exec - obj.name != obj.attr(name)
                exec('base_attr = obj.%s' %(self.name))

                i = 0
                for val, pos, interp in zip(*[iter(graphs)]*3):
                    try:
                        attr_pos = base_attr[i].attr('{0}_Position'.format(self.name))
                        attr_value = base_attr[i].attr('{0}_FloatValue'.format(self.name))
                        attr_interp = base_attr[i].attr('{0}_Interp'.format(self.name))
                        attr_pos.set(pos)
                        attr_value.set(val)
                        attr_interp.set(interp)
                    except Exception, e:
                        # print e
                        pass
                    i += 1

                try:
                    for n in obj.attr(self.name).getArrayIndices():
                        if n not in range(i):
                            attr = base_attr[n]
                            attr.remove()
                except:
                    continue

        pm.undoInfo(closeChunk=True)     

    def validateInput(self, lineEdit):
        value = str(lineEdit.text())
        new_value = ''
        found_dot = False
        for v in value:
            is_dot =  v == '.'
            if v.isdigit() or is_dot:
                if is_dot:
                    if found_dot == True:  # second dot
                        continue
                    found_dot = True
                new_value += v

        lineEdit.blockSignals(True)
        lineEdit.setText(str(new_value))
        lineEdit.blockSignals(False)

    def clampInput(self, lineEdit):
        try:
            new_value = float(lineEdit.text())
        except:
            return
        if new_value < 0.0:
            new_value = 0.0
        elif new_value > 1.0:
            new_value = 1.0

        lineEdit.blockSignals(True)
        lineEdit.setText(str(new_value))
        lineEdit.blockSignals(False)

    def set_graph_value(self):
        self.clampInput(self.selValLineEdit)
        value = float(self.selValLineEdit.text())
        current_index = self.gc.getCurrentKey()
        current_str = self.gc.getAsString()
        graphs = [float(i) for i in current_str.split(',')]

        
        value_index = 3 * current_index
        graphs[value_index] = value
        new_str = ','.join([str(i) for i in graphs])

        self.gc.setAsString(new_str)

    def set_graph_position(self):
        self.clampInput(self.selPosLineEdit)
        pos = float(self.selPosLineEdit.text())
        current_index = self.gc.getCurrentKey()
        current_str = self.gc.getAsString()
        graphs = [float(i) for i in current_str.split(',')]

        pos_index = (3 * current_index) + 1
        graphs[pos_index] = pos
        new_str = ','.join([str(i) for i in graphs])

        self.gc.setAsString(new_str)

# ------------------------
# --------- Main App
form_class, base_class = loadUi.loadUiType(_uiFile)
# --------------------------------------
_controls = {'BaseControl': BaseControl, 
    'CheckBoxControl': CheckBoxControl,
    'LineEditControl': LineEditControl,
    'LineEdit2Control': LineEdit2Control,
    'LineEdit3Control': LineEdit3Control,
    'CurveControl': CurveControl}

class AttributeListWidget(QtWidgets.QListWidget):
    buttonDropped = QtCore.Signal(tuple)

    def __init__(self, parent=None):
        super(AttributeListWidget, self).__init__(parent)
        self.current_control = None

        # additional settings
        self.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarPolicy.ScrollBarAlwaysOn)
        self.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarPolicy.ScrollBarAsNeeded)
        # self.verticalScrollBar().setSingleStep(2)
        # self.horizontalScrollBar().setSingleStep(2)
        self.setVerticalScrollMode(QtWidgets.QAbstractItemView.ScrollPerPixel)
        self.setHorizontalScrollMode(QtWidgets.QAbstractItemView.ScrollPerPixel)
        self.setDragDropMode(QtWidgets.QAbstractItemView.DragDrop)
        self.setDefaultDropAction(QtCore.Qt.MoveAction)
        self.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.setAcceptDrops(True)
        self.setStyleSheet( """QListWidget{background: rgb(10, 10, 10);}""")

    def updateGeometries(self):
        super(AttributeListWidget, self).updateGeometries()
        self.verticalScrollBar().setSingleStep(10)
        self.horizontalScrollBar().setSingleStep(10)

    def dragEnterEvent(self, event):
        if event.mimeData().hasText():
            event.acceptProposedAction()
        else:
            super(AttributeListWidget, self).dragEnterEvent(event)

    def dragMoveEvent(self, event):
        if event.mimeData().hasText():
            event.setDropAction(QtCore.Qt.MoveAction)
            event.acceptProposedAction()

            p = self.viewport().mapFromGlobal(QtGui.QCursor().pos())
            curr_item = self.itemAt(p)
            curr_control = None
            if curr_item:
                curr_control = self.itemWidget(curr_item)
                curr_control.draw_drop_hilight(True)

            self.current_control = curr_control
            for row in xrange(self.count()):
                item = self.item(row)
                control = self.itemWidget(item)
                if control and control != curr_control:
                    control.draw_drop_hilight(False)

        else:
            super(AttributeListWidget, self).dragMoveEvent(event)

    def dropEvent(self, event):
        
        mimeData = event.mimeData()
        if mimeData.hasText():
            pos = event.pos()
            event.setDropAction(QtCore.Qt.MoveAction)
            event.acceptProposedAction()
            self.buttonDropped.emit((mimeData.text(), pos))
            if self.current_control:
                self.current_control.draw_drop_hilight(False)
            
        else:
            super(AttributeListWidget, self).dropEvent(event)

class AddControlButton(QtWidgets.QPushButton):
    def __init__(self, parent=None):
        super(AddControlButton, self).__init__(parent)
        self.control_type = 'BaseControl'

    def mouseMoveEvent(self, event):
        if event.buttons() != QtCore.Qt.LeftButton:
            return

        drag = QtGui.QDrag(self)
        mimedata = QtCore.QMimeData()
        mimedata.setText(self.control_type)
        drag.setMimeData(mimedata)
        pixmap = QtGui.QPixmap(self.size())
        painter = QtGui.QPainter(pixmap)
        painter.drawPixmap(self.rect(), self.grab())
        painter.end()
        drag.setPixmap(pixmap)
        drag.setHotSpot(event.pos())
        drag.exec_(QtCore.Qt.MoveAction)

    def mousePressEvent(self, event):
        pass
        # logger.warning('Please drag and drop the button into the collection area.')
        # super(AddControlButton, self).mousePressEvent(event)

class AttributeCollection(form_class, base_class):
    ''' The application class '''
    def __init__(self, parent):
        super(AttributeCollection, self).__init__(parent)
        self.setupUi(self)

        # --- UI tweaks
        self.setWindowTitle(QtWidgets.QApplication.translate(_uiName, 
            "{0} - v.{1}".format(_appTitle, _version), None))

        # icons
        self.lineEditIcon_path = '%s/icons/lineEdit.png' %_moduleDir
        self.lineEdit2Icon_path = '%s/icons/lineEdit2.png' %_moduleDir
        self.lineEdit3Icon_path = '%s/icons/lineEdit3.png' %_moduleDir
        self.checkBoxIcon_path = '%s/icons/checkBox.png' %_moduleDir
        self.graphIcon_path = '%s/icons/graph.png' %_moduleDir

        # add buttons
        buttonSize = QtCore.QSize(30, 20)
        self.checkBox_add_button = AddControlButton(self.centralwidget)
        self.checkBox_add_button.control_type = 'CheckBoxControl'
        self.checkBox_add_button.setIcon(QtGui.QIcon(self.checkBoxIcon_path))
        self.checkBox_add_button.setMinimumSize(buttonSize)
        self.addButton_gridLayout.addWidget(self.checkBox_add_button, 0, 3, 1, 1)

        self.lineEdit3_add_button = AddControlButton(self.centralwidget)
        self.lineEdit3_add_button.control_type = 'LineEdit3Control'
        self.lineEdit3_add_button.setIcon(QtGui.QIcon(self.lineEdit3Icon_path))
        self.lineEdit3_add_button.setMinimumSize(buttonSize)
        self.addButton_gridLayout.addWidget(self.lineEdit3_add_button, 0, 2, 1, 1)

        self.lineEdit2_add_button = AddControlButton(self.centralwidget)
        self.lineEdit2_add_button.control_type = 'LineEdit2Control'
        self.lineEdit2_add_button.setIcon(QtGui.QIcon(self.lineEdit2Icon_path))
        self.lineEdit2_add_button.setMinimumSize(buttonSize)
        self.addButton_gridLayout.addWidget(self.lineEdit2_add_button, 0, 1, 1, 1)

        self.lineEdit_add_button = AddControlButton(self.centralwidget)
        self.lineEdit_add_button.control_type = 'LineEditControl'
        self.lineEdit_add_button.setIcon(QtGui.QIcon(self.lineEditIcon_path))
        self.lineEdit_add_button.setMinimumSize(buttonSize)
        self.addButton_gridLayout.addWidget(self.lineEdit_add_button, 0, 0, 1, 1)

        self.curve_add_button = AddControlButton(self.centralwidget)
        self.curve_add_button.control_type = 'CurveControl'
        self.curve_add_button.setIcon(QtGui.QIcon(self.graphIcon_path))
        self.curve_add_button.setMinimumSize(buttonSize)
        self.addButton_gridLayout.addWidget(self.curve_add_button, 0, 4, 1, 1)

        # insert attribute list widget
        self.listWidget = AttributeListWidget(self.attribute_widget)
        self.listWidget.buttonDropped.connect(self.buttonDropped)
        self.attribute_layout.addWidget(self.listWidget)

        # connections
        self.load_action.triggered.connect(self.load_preset_from_file)
        self.saveAs_action.triggered.connect(self.save_preset_to_file)
        self.clear_action.triggered.connect(self.clear)
        self.edit_action.triggered.connect(self.edit_control)
        self.remove_selected_action.triggered.connect(self.remove_selected)
        self.setAll_pushButton.clicked.connect(self.set_all)

        preset_pref_path = self.read_preset_pref()
        if preset_pref_path:
            self.load_preset_from_file(preset_pref_path)

    def clear(self):
        self.listWidget.clear()

    def edit_control(self):
        for item in self.listWidget.selectedItems():
            control = self.listWidget.itemWidget(item)
            row = self.listWidget.row(item)
            dialog = control.create_dialog()
            if dialog.exec_() == QtWidgets.QDialog.Accepted:
                control.get_create_settings()
                control.set_create_settings()

    def buttonDropped(self, data):

        controlName, pos = data
        item = self.listWidget.itemAt(pos)
        if item:
            item_row = self.listWidget.row(item) + 1
        else:
            item_row = self.listWidget.count()
        control = _controls[controlName](parent_app=self)
        dialog = control.create_dialog()
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            control.get_create_settings()
            control.create()

            listItem = QtWidgets.QListWidgetItem()
            self.listWidget.insertItem(item_row, listItem)
            self.listWidget.setItemWidget(listItem, control)
            listItem.setSizeHint(control.sizeHint())
            self.listWidget.clearSelection()
            listItem.setSelected(True)

    def remove_selected(self):
        selModel = self.listWidget.selectionModel()
        indices = []
        if selModel.hasSelection():
            indices =[QtCore.QPersistentModelIndex(index) for index in selModel.selectedRows()]
        if not indices:
            return
        for i in indices:
            index = i.row()
            item = self.listWidget.item
            self.listWidget.takeItem(index)

    def save_preset_to_file(self, path=None):
        ''' Save preset to json path '''
        if not path:
            dialog = QtWidgets.QFileDialog()
            dialog.setWindowTitle('Save preset to...')
            dialog.setNameFilter('*.json')
            dialog.setDefaultSuffix('.json')
            dialog.setAcceptMode(QtWidgets.QFileDialog.AcceptSave)
            if dialog.exec_() == QtWidgets.QDialog.Accepted:
                path = dialog.selectedFiles()[0]

        if path:
            data = self.get_preset_data()
            with open(path, 'w') as f:
                json.dump(data, f)
            self.write_preset_pref(path)
            logger.info('Preset saved to: %s' %path)

    def get_preset_data(self):
        data = []
        for row in xrange(self.listWidget.count()):
            item = self.listWidget.item(row)
            control = self.listWidget.itemWidget(item)
            attrName = control.label.text()
            controlType = control.__class__.__name__
            value = control.get_value()
            data.append([attrName, controlType, value])

        return data
                
    def load_preset_from_file(self, path=None):
        ''' Load preset from json path '''
        if not path:
            dialog = QtWidgets.QFileDialog()
            dialog.setWindowTitle('Load preset from...')
            dialog.setNameFilter('*.json')
            dialog.setDefaultSuffix('.json')
            dialog.setAcceptMode(QtWidgets.QFileDialog.AcceptOpen)
            if dialog.exec_() == QtWidgets.QDialog.Accepted:
                path = dialog.selectedFiles()[0]

        if os.path.exists(path):
            data = None
            with open(path, 'r') as f:
                data = json.load(f)
            if data:
                self.load_preset(data)
                self.write_preset_pref(path)
                logger.info('Preset loaded from: %s' %path)

    def write_preset_pref(self, path):
        if not os.path.exists(_tempDir):
            os.makedirs(_tempDir)
        pref_path = '%s/preset_pref.txt' %_tempDir
        with open(pref_path, 'w') as f:
            f.write(path)

    def read_preset_pref(self):
        pref_path = '%s/preset_pref.txt' %_tempDir
        preset_path = None
        if os.path.exists(pref_path):
            
            with open(pref_path, 'r') as f:
                preset_path = f.read()

        return preset_path

    def load_preset(self, preset):
        ''' Fill base UI with UI from template '''
        self.clear()
        for datas in preset:
            attrName = datas[0]
            controlType = datas[1]
            value = datas[2]
            control = _controls[controlType](parent_app=self)
            control.name = attrName
            control.default_value = value
            control.create()

            listItem = QtWidgets.QListWidgetItem(self.listWidget)
            self.listWidget.addItem(listItem)
            self.listWidget.setItemWidget(listItem, control)
            listItem.setSizeHint(control.sizeHint())

    def add_control(self, controlName):
        ''' Add control to current UI '''

        # get control class from name
        control = _controls[controlName](parent_app=self)
        dialog = control.create_dialog()
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            control.get_create_settings()
            control.create()

            listItem = QtWidgets.QListWidgetItem(self.listWidget)
            self.listWidget.addItem(listItem)
            self.listWidget.setItemWidget(listItem, control)
            listItem.setSizeHint(control.sizeHint())

    def set_all(self):
        pm.undoInfo(openChunk=True)
        for row in xrange(self.listWidget.count()):
            item = self.listWidget.item(row)
            selected = item.isSelected()
            if selected:
                control = self.listWidget.itemWidget(item)
                control.set_value()

        pm.undoInfo(closeChunk=True)

'''
from rf_app.utils.attributeCollection import app as attr_app
reload(attr_app)

app = attr_app.show()
'''
def show():
    ''' Launching function '''
    global AttributeCollectionApp
    try:
        AttributeCollectionApp.close()
    except Exception, e:
        pass
    
    AttributeCollectionApp = AttributeCollection(parent=maya_win.getMayaWindow())
    AttributeCollectionApp.show()
    return AttributeCollectionApp
 


# item = QtWidgets.QListWidgetItem(self.displayWidget)
# # item.setData(QtCore.Qt.UserRole, noteInfo)
# # item.setFlags(item.flags() & ~QtCore.Qt.ItemIsSelectable)
# # note_config.set_tool_tip(item, noteEntity)

# noteItem = note_item.NoteItem()
# self.set_note_item(infoDict, noteItem)

# item.setSizeHint(noteItem.sizeHint())
# self.displayWidget.addItem(item)
# self.displayWidget.setItemWidget(item, noteItem)
        
    
