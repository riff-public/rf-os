# -*- coding: utf-8 -*-
import maya.cmds as mc

from Qt import QtWidgets
from Qt import QtCore
from Qt import QtGui

# logger
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rftool.utils.ui import maya_win
from rf_maya.rftool.utils import pipeline_utils
from rf_maya.rftool.utils import maya_utils

from rf_utils.context import context_info
reload(context_info)

class HiddenGeoWriter(QtWidgets.QDialog):
    def __init__(self, *args, **kwargs):
        QtWidgets.QDialog.__init__(self, *args, **kwargs)

        self.entity = None

        # ----- UI
        self.setWindowTitle('Hidden Geometry List')
        self.layout = QtWidgets.QVBoxLayout(self)

        # asset label
        self.asset_label = QtWidgets.QLabel('')
        self.asset_label.setFont(QtGui.QFont("Times", 10, QtGui.QFont.Bold))
        self.layout.addWidget(self.asset_label)

        # instructions
        self.label = QtWidgets.QLabel('These geo will be hidden at render time...')
        self.layout.addWidget(self.label)
        self.label_th = QtWidgets.QLabel(r'โมเดลเหล่านี้จะถูกซ่อนเพื่อไม่ให้ถูก Render...')
        self.layout.addWidget(self.label_th)

        # list widget
        self.list_widget = QtWidgets.QListWidget()
        self.list_widget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.layout.addWidget(self.list_widget)

        # +, refresh, and - button
        self.hLayout = QtWidgets.QHBoxLayout()
        self.addItemButton = QtWidgets.QPushButton('+')
        self.refreshButton = QtWidgets.QPushButton('Refresh')
        self.removeItemButton = QtWidgets.QPushButton('-')
        self.refreshButton.clicked.connect(self.refresh_list)
        self.hLayout.addWidget(self.addItemButton)
        self.hLayout.addWidget(self.refreshButton)
        self.hLayout.addWidget(self.removeItemButton)
        self.layout.addLayout(self.hLayout)
        self.hLayout.setSpacing(10)
        self.hLayout.setContentsMargins(2, 2, 2, 2)
        self.addItemButton.clicked.connect(self.addToList)
        self.removeItemButton.clicked.connect(self.removeFromList)

        # OK, Cancel button
        self.okButton = QtWidgets.QPushButton('SET')
        # self.cancelButton = QtWidgets.QPushButton('Cancel')
        self.layout.addWidget(self.okButton)
        # self.layout.addWidget(self.cancelButton)
        self.okButton.clicked.connect(self.write_data)
        # self.cancelButton.clicked.connect(self.close)

        self.refresh_list()

    def refresh_list(self, objects=[]):
        self.list_widget.clear()
        self.entity = context_info.ContextPathInfo(path=mc.file(q=True, sn=True))
        asset_text = '\t%s - %s/%s' %(self.entity.project, self.entity.type, self.entity.name)
        self.asset_label.setText(str(asset_text))

        objects = pipeline_utils.read_hiddenGeo_data(self.entity)
        if objects:
            for l in objects:
                self.list_widget.addItem(QtWidgets.QListWidgetItem(l))
            self.list_widget.setCurrentRow(0)

    def addToList(self):
        objs = mc.ls(sl=True, type='transform')
        objs = [str(maya_utils.removeDagPathNamespace(obj)) for obj in objs]

        for obj in objs:
            if not self.list_widget.findItems(obj, QtCore.Qt.MatchExactly):
                self.list_widget.addItem(QtWidgets.QListWidgetItem(obj))
            else:
                logger.warning('Object already in the list: %s' %obj)

    def removeFromList(self):
        selItems = self.list_widget.selectedItems()
        if selItems:
            for item in selItems:
                self.list_widget.takeItem(self.list_widget.row(item))
        
    def write_data(self):
        objs = []
        for row in xrange(self.list_widget.count()):
            item = self.list_widget.itemAt(row)
            text = str(item.text())
            if text:
                objs.append(text)

        pipeline_utils.write_hiddenGeo_data(entity=self.entity, objs=objs)


def show():
    ''' Launching function '''
    global HiddenGeoWriterApp
    try:
        HiddenGeoWriterApp.close()
    except Exception, e:
        pass
    
    HiddenGeoWriterApp = HiddenGeoWriter(parent=maya_win.getMayaWindow())
    HiddenGeoWriterApp.show()
    return HiddenGeoWriterApp
