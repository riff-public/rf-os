import os
from collections import OrderedDict
from PIL_Maya import Image

import maya.OpenMaya as om
import maya.api.OpenMaya as om2
import pymel.core as pm

'''
TODO:
- Fix volume behavior
'''

EXTS = {'.tif': 'tiff'}

def isUvOnFace(meshFn, uv, uvSet=''):
    try:
        return meshFn.intersectFaceAtUV(uv[0], uv[1], uvSet=uvSet)
    except RuntimeError:
        return None

def generate(output_path, geo, crv_data, bg_color=(0, 0, 0), size=1024):
    '''
    crv_data = {color: (crv, inner_radius, outer_radius)}
    '''
    f, ext = os.path.splitext(output_path)
    imgExt = EXTS[ext]

    # get current UV set name of geo
    currUVSet = geo.getCurrentUVSetName()

    # create new image
    img = Image.new('RGBA', (size, size), (bg_color[0], bg_color[1], bg_color[2], 255))
    # pixels = img.load()  # create the pixel map
    sizef = float(size)

    # api meshFn
    geoDag = geo.__apimdagpath__()
    geoMobj = geo.__apimobject__()
    msel = om2.MSelectionList()
    msel.add(geo.shortName())
    meshFn2 = om2.MFnMesh(msel.getDagPath(0))

    # get geo matrix
    geoIncMatrix = geoDag.inclusiveMatrix()

    # mesh intersector
    intersector = om.MMeshIntersector()
    intersector.create(geoMobj, geoIncMatrix)
    
    # get all curve base points
    base_pts = []
    images = [img]
    for color, color_data in crv_data.iteritems():

        for data in color_data:
            crv = data[0]
            inner_radius = float(data[1])
            outer_radius = float(data[2])

            base_pos = crv.cv[0].getPosition(space='world')
            base_pt = om.MPoint(base_pos[0], base_pos[1], base_pos[2])
            ptOnMesh = om.MPointOnMesh()
            intersector.getClosestPoint(base_pt, ptOnMesh)
            resPt = om.MPoint(ptOnMesh.getPoint()) * geoIncMatrix
            resPt2 = om2.MPoint(resPt.x, resPt.y, resPt.z)
            
            # add 0 alpha to the color at first
            img_color = (color[0], color[1], color[2], 0)
            new_img = Image.new('RGBA', (size, size), img_color)
            pixels = new_img.load()

            base_pts.append((img_color, resPt2, inner_radius, outer_radius, pixels))
            images.append(new_img)

    # block size for center of pixels
    block_size = (1.0/size) * 0.5

    # print base_pts
    # loop over each pixel
    for i in xrange(img.size[0]):
        for j in xrange(img.size[1]):
            puv = (i/sizef) + block_size, 1.0 - ((j/sizef) + block_size)

            # only get pixels that are on a face
            # fid = isUvOnFace(meshFn2, puv, uvSet=currUVSet)
            fid = None
            try:
                fid = meshFn2.intersectFaceAtUV(puv[0], puv[1], uvSet=currUVSet)
            except:
                continue

            if fid != None:
                # get world position
                wPt = meshFn2.getPointAtUV(fid, puv[0], puv[1], om.MSpace.kWorld, currUVSet, 1e-5)
                
                for data in base_pts:
                    color = data[0]
                    base_pt = data[1]
                    inner_radius = data[2]
                    outer_radius = data[3]
                    pixels = data[4]

                    dist = wPt.distanceTo(base_pt)

                    # if the pixel is within inner radius, it has full alpha
                    if dist <= inner_radius:
                        pixels[i, j] = (color[0], color[1], color[2], 255)

                    # if the pixel is beyond inner radius, but within outer radius, it has percentage alpha
                    elif dist <= outer_radius:
                        dist_diff = dist - inner_radius
                        alpha = int(round((1.0 - (dist_diff / (outer_radius - inner_radius)))*255))
                        pixels[i, j] = (color[0], color[1], color[2], alpha)

    # composite all the images
    for cimg in images:
        img = Image.alpha_composite(img, cimg)

    img.save(output_path, 'tiff')
    return output_path

