# built-in modules
import os
import sys
from collections import OrderedDict

# maya modules
import maya.cmds as mc
import pymel.core as pm

# Qt modules
from Qt import QtWidgets
from Qt import QtCore
from Qt import QtGui

# logger
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

# custom modules
from rftool.utils.ui import maya_win

# core module
import core
reload(core)

__APP_NAME__ = 'Curve Map Generator'
__APP_VERSION__ = '1.2'

TEXTURE_RESOLUTIONS = ('64', '128', '256', '512', '1024', '2048', '4096', '8192')
DEFAULT_EXT = 'tif'

# color vars

RED = QtGui.QColor(255, 0, 0)
GREEN = QtGui.QColor(0, 255, 0)
BLUE = QtGui.QColor(0, 0, 255)
YELLOW = QtGui.QColor(255, 255, 0)
LIGHTBLUE = QtGui.QColor(0, 255, 255)
PURPLE = QtGui.QColor(255, 0, 255)
LIGHTGREEN = QtGui.QColor(128, 255, 0)
ORANGE = QtGui.QColor(255, 128, 0)
LIGHTPURPLE = QtGui.QColor(128, 0, 255)
GREY = QtGui.QColor(128, 128, 128)
BLACK = QtGui.QColor(0, 0, 0)
WHITE = QtGui.QColor(255, 255, 255)

DEFAULT_COLOR = WHITE
BACKGROUND_COLORS = (('Black',BLACK), ('White',WHITE), ('Red',RED), ('Green',GREEN), ('Blue',BLUE))

class QColorTableWidget(QtWidgets.QTableWidget):
    def __init__(self, parent=None):
        super(QColorTableWidget, self).__init__(parent)
        self.parent = parent

        # --- create table
        self.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.setFrameShadow(QtWidgets.QFrame.Plain)

        self.setDragDropMode(QtWidgets.QAbstractItemView.InternalMove)
        self.setDefaultDropAction(QtCore.Qt.MoveAction)
        self.setDragEnabled(True)
        self.setAcceptDrops(True)
        self.viewport().setAcceptDrops(True)
        self.setDragDropOverwriteMode(False)

        self.setAlternatingRowColors(True)
        self.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)

        self.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self.setGridStyle(QtCore.Qt.DotLine)
        self.setLineWidth(2)
        self.setColumnCount(2)
        self.setHorizontalHeaderLabels(['Color', 'Curves'])

        # headers
        header = self.horizontalHeader()
        header.setCascadingSectionResizes(False)
        header.setDefaultSectionSize(100)
        header.setHighlightSections(False)
        header.setMinimumSectionSize(30)
        header.setStretchLastSection(True)
        self.verticalHeader().setVisible(False)

        # right click menu
        self.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        
        # signals
        self.customContextMenuRequested.connect(self.generateMenu)
        self.itemSelectionChanged.connect(self.selectObj)

        # vars
        self.used_colors = []

    def selectObj(self):
        selRowIndices =[QtCore.QPersistentModelIndex(index).row() for index in self.selectionModel().selectedRows()]
        to_sels = set()
        for index in selRowIndices:
            item = self.item(index, 1)
            item_str = str(item.text())
            if item_str:
                crv_names = set(item_str.split(', '))
                to_sels = to_sels.union(crv_names)

        to_sels = list(to_sels)
        try:
            mc.select(to_sels, r=True)
        except:
            pass

    def generateMenu(self, pos):
        gPos = QtGui.QCursor.pos()
        tablePos = self.viewport().mapFromGlobal(gPos)

        item = self.itemAt(tablePos)
        rightClickMenu = QtWidgets.QMenu(self)

        if item:
            curr_row = item.row()
            
            # add curve
            add_curve_action = QtWidgets.QAction('Add Selected', self)
            add_curve_action.triggered.connect(lambda: self.parent.add_sel_curve(curr_row))
            rightClickMenu.addAction(add_curve_action)

            # remove curve
            rem_curve_action = QtWidgets.QAction('Remove Selected', self)
            rem_curve_action.triggered.connect(lambda: self.parent.remove_sel_curve(curr_row))
            rightClickMenu.addAction(rem_curve_action)

            rightClickMenu.addSeparator()

        clear_action = QtWidgets.QAction('Clear Items', self)
        clear_action.triggered.connect(lambda: self.parent.clear_color_table())
        rightClickMenu.addAction(clear_action)

        desel_action = QtWidgets.QAction('Clear Selection', self)
        desel_action.triggered.connect(lambda: self.parent.deselect_color_table())
        rightClickMenu.addAction(desel_action)

        rightClickMenu.exec_(self.mapToGlobal(pos))

    def mousePressEvent(self, event):
        if event.buttons() != QtCore.Qt.LeftButton:
            return
        super(QColorTableWidget, self).mousePressEvent(event)

    def dropEvent(self, event):
        if event.source() == self and (event.dropAction() == QtCore.Qt.MoveAction or self.dragDropMode() == QtWidgets.QAbstractItemView.InternalMove):
            success, row, col, topIndex = self.dropOn(event)
            if success:             
                selRows = self.getSelectedRowsFast()                        

                top = selRows[0]
                # print 'top is %d'%top
                dropRow = row
                if dropRow == -1:
                    dropRow = self.rowCount()
                # print 'dropRow is %d'%dropRow
                offset = dropRow - top
                # print 'offset is %d'%offset

                for i, row in enumerate(selRows):
                    r = row + offset
                    if r > self.rowCount() or r < 0:
                        r = 0
                    self.insertRow(r)
                    # print 'inserting row at %d'%r


                selRows = self.getSelectedRowsFast()
                # print 'selected rows: %s'%selRows

                top = selRows[0]
                offset = dropRow - top                
                for i, row in enumerate(selRows):
                    r = row + offset
                    if r > self.rowCount() or r < 0:
                        r = 0

                    for j in xrange(self.columnCount()):
                        source = QtWidgets.QTableWidgetItem(self.item(row, j))
                        self.setItem(r, j, source)

                event.accept()

        else:
            QTableView.dropEvent(event)                

    def getSelectedRowsFast(self):
        selRows = []
        for item in self.selectedItems():
            if item.row() not in selRows:
                selRows.append(item.row())
        return selRows

    def droppingOnItself(self, event, index):
        dropAction = event.dropAction()

        if self.dragDropMode() == QtWidgets.QAbstractItemView.InternalMove:
            dropAction = QtCore.Qt.MoveAction

        if event.source() == self and event.possibleActions() & QtCore.Qt.MoveAction and dropAction == QtCore.Qt.MoveAction:
            selectedIndexes = self.selectedIndexes()
            child = index
            while child.isValid() and child != self.rootIndex():
                if child in selectedIndexes:
                    return True
                child = child.parent()

        return False

    def dropOn(self, event):
        if event.isAccepted():
            return False, None, None, None

        index = QtCore.QModelIndex()
        row = -1
        col = -1

        if self.viewport().rect().contains(event.pos()):
            index = self.indexAt(event.pos())
            if not index.isValid() or not self.visualRect(index).contains(event.pos()):
                index = self.rootIndex()

        if self.model().supportedDropActions() & event.dropAction():
            if index != self.rootIndex():
                dropIndicatorPosition = self.position(event.pos(), self.visualRect(index), index)

                if dropIndicatorPosition == QtWidgets.QAbstractItemView.AboveItem:
                    row = index.row()
                    col = index.column()
                    # index = index.parent()
                elif dropIndicatorPosition == QtWidgets.QAbstractItemView.BelowItem:
                    row = index.row() + 1
                    col = index.column()
                else:
                    row = index.row()
                    col = index.column()

            if not self.droppingOnItself(event, index):
                return True, row, col, index

        return False, None, None, None

    def position(self, pos, rect, index):
        r = QtWidgets.QAbstractItemView.OnViewport
        margin = 2
        if pos.y() - rect.top() < margin:
            r = QtWidgets.QAbstractItemView.AboveItem
        elif rect.bottom() - pos.y() < margin:
            r = QtWidgets.QAbstractItemView.BelowItem 
        elif rect.contains(pos, True):
            r = QtWidgets.QAbstractItemView.OnItem

        if r == QtWidgets.QAbstractItemView.OnItem and not (self.model().flags(index) & QtCore.Qt.ItemIsDropEnabled):
            r = QtWidgets.QAbstractItemView.AboveItem if pos.y() < rect.center().y() else QtWidgets.QAbstractItemView.BelowItem

        return r

class CurveMapGenerator(QtWidgets.QDialog):
    def __init__(self, *args, **kwargs):
        super(CurveMapGenerator, self).__init__(*args, **kwargs)

        # ---------- UI
        self.setWindowTitle('%s - %s' %(__APP_NAME__, __APP_VERSION__))
        self.layout = QtWidgets.QVBoxLayout(self)

        # ---------- path
        self.path_layout = QtWidgets.QHBoxLayout()
        self.layout.addLayout(self.path_layout)

        self.path_label = QtWidgets.QLabel('Output')
        self.path_lineEdit = QtWidgets.QLineEdit('')
        self.path_lineEdit.setPlaceholderText('<paste path here...>')
        self.path_lineEdit.setMinimumSize(QtCore.QSize(250, 20))

        self.browse_button = QtWidgets.QPushButton('...')
        self.browse_button.setMaximumSize(QtCore.QSize(20, 20))

        self.path_layout.addWidget(self.path_label)
        self.path_layout.addWidget(self.path_lineEdit)
        self.path_layout.addWidget(self.browse_button)

        self.h_layout = QtWidgets.QHBoxLayout()
        self.layout.addLayout(self.h_layout)

        # ---------- form layouts, left and right
        self.form_layout = QtWidgets.QFormLayout()
        self.h_layout.addLayout(self.form_layout)

        self.formR_layout = QtWidgets.QFormLayout()
        self.h_layout.addLayout(self.formR_layout)

        # --- radius checkbox
        self.radius_checkbox = QtWidgets.QCheckBox('Use Curve Radius')
        self.radius_checkbox.setChecked(True)
        self.form_layout.setWidget(0, QtWidgets.QFormLayout.SpanningRole, self.radius_checkbox)

        # --- inner radius
        self.inrad_label = QtWidgets.QLabel('Inner Radius')
        self.inrad_lineEdit = QtWidgets.QLineEdit('0.2')
        self.inrad_lineEdit.setMaximumSize(QtCore.QSize(70, 20))

        self.form_layout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.inrad_label)
        self.form_layout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.inrad_lineEdit)

        # --- outer radius
        self.outrad_label = QtWidgets.QLabel('Outer Radius')
        self.outrad_lineEdit = QtWidgets.QLineEdit('0.2')
        self.outrad_lineEdit.setMaximumSize(QtCore.QSize(70, 20))
        self.inrad_label.setEnabled(False)
        self.outrad_label.setEnabled(False)
        self.inrad_lineEdit.setEnabled(False)
        self.outrad_lineEdit.setEnabled(False)

        self.form_layout.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.outrad_label)
        self.form_layout.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.outrad_lineEdit)

        # --- margin
        self.margin_label = QtWidgets.QLabel('Radius Margin')
        self.margin_spinbox = QtWidgets.QSpinBox()
        self.margin_spinbox.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))
        self.margin_spinbox.setRange(50, 200)
        self.margin_spinbox.setValue(100)
        self.margin_spinbox.setSuffix('%')

        self.form_layout.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.margin_label)
        self.form_layout.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.margin_spinbox)

        # --- curve color checkbox
        self.crvColor_checkbox = QtWidgets.QCheckBox('Change Curve Color')
        self.crvColor_checkbox.setChecked(True)
        self.formR_layout.setWidget(0, QtWidgets.QFormLayout.SpanningRole, self.crvColor_checkbox)
        
        # --- size combobox
        self.size_label = QtWidgets.QLabel('Texture Size')
        self.size_combobox = QtWidgets.QComboBox()
        self.size_combobox.addItems(TEXTURE_RESOLUTIONS)
        self.size_combobox.setCurrentIndex(4)

        self.formR_layout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.size_label)
        self.formR_layout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.size_combobox)

        # --- background 
        self.bg_label = QtWidgets.QLabel('Background')
        self.bg_combobox = QtWidgets.QComboBox()
        for name, color in BACKGROUND_COLORS:
            iconPixmap = QtGui.QPixmap(30, 30)
            iconPixmap.fill(color)
            colorIcon = QtGui.QIcon()   
            colorIcon.addPixmap(iconPixmap)
            self.bg_combobox.addItem(colorIcon, name)

        self.formR_layout.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.bg_label)
        self.formR_layout.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.bg_combobox)


        self.line = QtWidgets.QFrame(self)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.layout.addWidget(self.line)

        # -------------- color layout 
        self.color_layout = QtWidgets.QHBoxLayout()
        self.layout.addLayout(self.color_layout)
        
        spacerItem = QtWidgets.QSpacerItem(70, 25, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.color_layout.addItem(spacerItem)

        # --- color picker
        self.colorDialog = QtWidgets.QColorDialog(self)
        self.colorDialog.setWindowTitle('Select Curve Color')
        self.colorDialog.setOption(QtWidgets.QColorDialog.ShowAlphaChannel, False)
        self.colorDialog.setOption(QtWidgets.QColorDialog.DontUseNativeDialog, True)
        self.colorDialog.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))
        self.colorDialog.setCurrentColor(DEFAULT_COLOR)

        # custom colors
        self.colorDialog.setCustomColor(0, RED)
        self.colorDialog.setCustomColor(1, ORANGE)
        self.colorDialog.setCustomColor(2, GREEN)
        self.colorDialog.setCustomColor(3, LIGHTGREEN)
        self.colorDialog.setCustomColor(4, BLUE)
        self.colorDialog.setCustomColor(5, LIGHTBLUE)
        self.colorDialog.setCustomColor(6, PURPLE)
        self.colorDialog.setCustomColor(7, LIGHTPURPLE)
        self.colorDialog.setCustomColor(8, WHITE)
        self.colorDialog.setCustomColor(9, GREY)
        self.colorDialog.setCustomColor(10, BLACK)

        # color button
        self.color_button = QtWidgets.QPushButton()
        self.set_button_color(DEFAULT_COLOR)
        self.color_layout.addWidget(self.color_button)

        # ---------- add remove button
        self.add_button = QtWidgets.QPushButton('+')
        self.add_button.setFont(QtGui.QFont("Times", 10, QtGui.QFont.Bold))
        self.add_button.setMaximumSize(QtCore.QSize(25, 30))
        self.remove_button = QtWidgets.QPushButton('-')
        self.remove_button.setFont(QtGui.QFont("Times", 10, QtGui.QFont.Bold))
        self.remove_button.setMaximumSize(QtCore.QSize(25, 30))
        self.color_layout.addWidget(self.add_button)
        self.color_layout.addWidget(self.remove_button)

        # --------- color table widget
        self.color_tableWidget = QColorTableWidget(parent=self)
        self.layout.addWidget(self.color_tableWidget)

        # ---------- button
        self.button = QtWidgets.QPushButton('Generate')
        self.layout.addWidget(self.button)

        # ---------- connections
        self.path_lineEdit.editingFinished.connect(self.path_editing_finished)
        self.radius_checkbox.toggled.connect(self.radius_checked)
        self.inrad_lineEdit.textChanged.connect(lambda f: self.validateInput(self.inrad_lineEdit))
        self.outrad_lineEdit.textChanged.connect(lambda f: self.validateInput(self.outrad_lineEdit))
        self.inrad_lineEdit.editingFinished.connect(self.inner_edit_finished)
        self.outrad_lineEdit.editingFinished.connect(self.outer_edit_finished)
        self.colorDialog.colorSelected.connect(lambda f: self.color_changed())
        self.color_button.clicked.connect(self.show_color_dialog)
        self.browse_button.clicked.connect(self.browse)
        self.add_button.clicked.connect(self.add_color)
        self.remove_button.clicked.connect(self.remove_color)
        self.button.clicked.connect(self.generate)

    def browse(self):
        dialog = QtWidgets.QFileDialog(self)
        dialog.setWindowTitle('Generate to...')
        dialog.setNameFilter('*.%s' %DEFAULT_EXT)
        dialog.setDefaultSuffix(DEFAULT_EXT)
        # dialog.setDirectory(self.default_file_dir)
        dialog.setAcceptMode(QtWidgets.QFileDialog.AcceptSave)
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            path = dialog.selectedFiles()[0]
            if path:
                self.path_lineEdit.setText(str(path))

    def add_sel_curve(self, row):
        crvs = self.get_selected_crvs()
        if not crvs:
            return

        # set text
        crvNames = [str(c.shortName()) for c in crvs]
        item = self.color_tableWidget.item(row, 1)
        crv_str = str(item.text())
        crvs_str = crv_str.split(', ')
        new_crvs = [c for c in crvs_str if c]
        for crv in crvNames:
            if crv and crv not in crvs_str:
                new_crvs.append(crv)

        item.setText(str(', '.join(new_crvs)))

        # change curve color
        if self.crvColor_checkbox.isChecked():
            colorItem = self.color_tableWidget.item(row, 0)
            brush = colorItem.background()
            color = brush.color()
            # rgb = (color.red(), color.green(), color.blue())
            self.change_curve_color(crvs, color=color)

    def remove_sel_curve(self, row):
        crvs = self.get_selected_crvs()
        if not crvs:
            return

        # edit text
        crvNames = [str(c.shortName()) for c in crvs]
        item = self.color_tableWidget.item(row, 1)
        crv_str = str(item.text())
        crvs = crv_str.split(', ')
        for crv in crvNames:
            if crv in crvs:
                crvs.remove(crv)

        item.setText(str(', '.join(crvs)))

    def clear_color_table(self):
        self.color_tableWidget.clearContents()
        self.color_tableWidget.setRowCount(0)

    def deselect_color_table(self):
        self.color_tableWidget.clearSelection()

    def get_selected_crvs(self):
        sels = pm.selected(type='transform')
        return [i for i in sels if i.getShape(type='nurbsCurve')]

    def add_color(self):
        self.color_tableWidget.blockSignals(True)
        self.color_tableWidget.clearSelection()
        self.color_tableWidget.blockSignals(False)

        if self.colorDialog.exec_() == QtWidgets.QDialog.Accepted:
            color = self.colorDialog.currentColor()

        crvs = self.get_selected_crvs()
        ri = self.color_tableWidget.rowCount()

        # insert new row
        self.color_tableWidget.insertRow(ri)

         # color item
        colorItem = QtWidgets.QTableWidgetItem()
        colorItem.setBackground(color)
        self.color_tableWidget.setItem(ri, 0, colorItem)

        # curve item
        crvNames = [str(c.shortName()) for c in crvs]
        crvsItem = QtWidgets.QTableWidgetItem(str(', '.join(crvNames)))
        self.color_tableWidget.setItem(ri, 1, crvsItem)

        if self.crvColor_checkbox.isChecked():
            self.change_curve_color(crvs, color)

    def change_curve_color(self, crvs, color):
        pm.undoInfo(openChunk=True)
        for crv in crvs:
            crvShp = crv.getShape()
            crvShp.overrideEnabled.set(True)
            crvShp.overrideRGBColors.set(True)
            crvShp.overrideColorR.set(color.red()/255.0)
            crvShp.overrideColorG.set(color.green()/255.0)
            crvShp.overrideColorB.set(color.blue()/255.0)
        pm.undoInfo(closeChunk=True)

    def get_selected_table_row(self):
        selModel = self.color_tableWidget.selectionModel()
        pstMdlIndices = []
        if selModel.hasSelection():
            pstMdlIndices =[QtCore.QPersistentModelIndex(index) for index in self.color_tableWidget.selectionModel().selectedRows()]
        if not pstMdlIndices:
            return []
        indicies = [i.row() for i in pstMdlIndices]
        return indicies

    def remove_color(self):
        indicies = self.get_selected_table_row()
        for index in indicies:
            self.color_tableWidget.removeRow(index)

    def radius_checked(self):
        value = self.radius_checkbox.isChecked()
        self.inrad_lineEdit.setEnabled(not(value))
        self.outrad_lineEdit.setEnabled(not(value))
        self.inrad_label.setEnabled(not(value))
        self.outrad_label.setEnabled(not(value))

    def color_changed(self):
        color = self.colorDialog.currentColor()
        self.set_button_color(color)

        # in case a color row is selected
        indicies = self.get_selected_table_row()
        for index in indicies:
            colorItem = self.color_tableWidget.item(index, 0)
            colorItem.setBackground(color)

            if self.crvColor_checkbox.isChecked():
                crv_item = self.color_tableWidget.item(index, 1)
                crv_str = str(crv_item.text())
                crvNames = crv_str.split(', ')
                crvs = []
                for c in crvNames:
                    try:
                        c = pm.PyNode(c)
                        if c.getShape(type='nurbsCurve'):
                            crvs.append(c)
                    except:
                        continue
                self.change_curve_color(crvs, color=color)
                
    def set_button_color(self, color):
        iconPixmap = QtGui.QPixmap(30, 30)
        iconPixmap.fill(color)
        colorIcon = QtGui.QIcon()   
        colorIcon.addPixmap(iconPixmap)
        self.color_button.setIcon(colorIcon)
        
    def show_color_dialog(self):
        self.colorDialog.show()
    
    def show_bg_dialog(self):
        self.bg_colorDialog.show()

    def path_editing_finished(self):
        path = str(self.path_lineEdit.text())
        path = path.replace('"', '')
        path = path.replace(' ', '')
        path = path.replace('\\', '/')

        self.path_lineEdit.setText(path)

    def inner_edit_finished(self):
        inner_value = float(self.inrad_lineEdit.text())
        outer_value = float(self.outrad_lineEdit.text())
        if inner_value > outer_value:
            self.inrad_lineEdit.setText(str(outer_value))

    def outer_edit_finished(self):
        inner_value = float(self.inrad_lineEdit.text())
        outer_value = float(self.outrad_lineEdit.text())
        if outer_value < inner_value:
            self.outrad_lineEdit.setText(str(inner_value))

    def validateInput(self, lineEdit):
        value = str(lineEdit.text())
        new_value = ''
        found_dot = False
        for v in value:
            is_dot =  v == '.'
            if v.isdigit() or is_dot:
                if is_dot:
                    if found_dot == True:  # second dot
                        continue
                    found_dot = True
                new_value += v
        if new_value.startswith('.'):
            new_value = '0' + new_value

        lineEdit.blockSignals(True)
        lineEdit.setText(str(new_value))
        lineEdit.blockSignals(False)

    def generate(self):
        path = str(self.path_lineEdit.text())
        inner_radius = float(self.inrad_lineEdit.text())
        outer_radius = float(self.outrad_lineEdit.text())
        bg_qcolor = BACKGROUND_COLORS[self.bg_combobox.currentIndex()][1]
        bg_color = (bg_qcolor.red(), bg_qcolor.green(), bg_qcolor.blue())
        margin = float(self.margin_spinbox.value())
    
        size = int(self.size_combobox.currentText())

        dirname = os.path.dirname(path)
        if not dirname:
            logger.error('Invalid path: %s' %path)
            return
        if not os.path.exists(dirname):
            os.makedirs(dirname)

        f, ext = os.path.splitext(path)
        if ext not in core.EXTS:
            logger.error('Invalid file format: %s' %(ext))
            return

        geo = None
        crv_data = OrderedDict()
        invalid_sel_err = 'Please select a polygon geo.'

        objs = pm.selected(type='transform')
        if not objs or len(objs) != 1 or not objs[0].getShape(type='mesh'):
            logger.error(invalid_sel_err)
            return
        geo = objs[0].getShape(type='mesh')
        use_crv_radius = self.radius_checkbox.isChecked()

        for row in xrange(self.color_tableWidget.rowCount()):
            color_item = self.color_tableWidget.item(row, 0)

            # get color
            brush = color_item.background()
            color = brush.color()
            rgb = (color.red(), color.green(), color.blue())
            crv_data[rgb] = []

            # get curve
            crv_item = self.color_tableWidget.item(row, 1)
            crv_str = str(crv_item.text())
            crvNames = crv_str.split(', ')
            crvs = []
            for c in crvNames:
                try:
                    c = pm.PyNode(c)
                    crvs.append(c)
                except:
                    logger.warning('Cannot find: %s' %c)
                    continue

            for crv in crvs:
                crvShp = crv.getShape(type='nurbsCurve')
                if not crvShp:
                    logger.error('Cannot find shape for: %s' %crv)
                    continue

                if use_crv_radius and crvShp.hasAttr('innerRadius') and crvShp.hasAttr('outerRadius'):
                    inner_radius = crvShp.innerRadius.get()
                    outer_radius = crvShp.outerRadius.get()

                crv_data[rgb].append((crvShp, inner_radius*(margin/100), outer_radius*(margin/100)))

        # print crv_data
        if not crv_data:
            logger.error('Error getting curves.')
            return

        # print crv_data
        # return
        pm.waitCursor(st=True)
        try:
            res = core.generate(output_path=path,
                    geo=geo, 
                    crv_data=crv_data, 
                    bg_color=bg_color,
                    size=size)
            pm.waitCursor(st=False)
            logger.info(res)
        except Exception, e:
            logger.error(e)
            pm.waitCursor(st=False)
        

def show():
    global CurveMapGeneratorApp
    try:
        CurveMapGeneratorApp.close()
    except Exception, e:
        pass

    CurveMapGeneratorApp = CurveMapGenerator(parent=maya_win.getMayaWindow())
    CurveMapGeneratorApp.show()

    return CurveMapGeneratorApp

'''
from rf_app.utils.curveMapGenerator import app as cmg_app
reload(cmg_app)

cmg_app.show()

'''