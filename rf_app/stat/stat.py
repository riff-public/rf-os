import os
from collections import OrderedDict 
from rf_utils import file_utils
import logging 

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

assetDataDir = 'P:/SevenChickMovie/_data/stat/asset'
sceneDataDir = 'P:/SevenChickMovie/_data/stat/scene'

def write_asset_stat(assetName, abcsize, yetisize, frame, srcPath): 
	assetData = '%s/%s.json' % (assetDataDir, assetName)
	rawDict = OrderedDict()
	rawDict['name'] = assetName
	rawDict['sizePerFrame'] = 0
	rawDict['abcPerFrame'] = 0
	rawDict['shots'] = []

	shotDict = OrderedDict()
	shotDict['abcsize'] = abcsize
	shotDict['yetisize'] = yetisize
	shotDict['frame'] = frame
	shotDict['from'] = ''

	if os.path.exists(assetData): 
		rawDict = file_utils.json_loader(assetData)

	currentShots = [a['from'] for a in rawDict['shots']]
	
	if not srcPath in currentShots: 
		shotDict['from'] = srcPath
		rawDict['shots'].append(shotDict)

		abcTimes = float()
		yetiTimes = float()
		
		for data in rawDict['shots']: 
			abcsize = float(data['abcsize'])
			yetisize = float(data['yetisize'])
			frame = float(data['frame'])

			abcTime = (abcsize / frame)
			abcTimes += abcTime
			yetiTime = (yetisize / frame)
			yetiTimes += yetiTime

		abcPerFrame = abcTimes / len(rawDict['shots'])
		yetiPerFrame = yetiTimes / len(rawDict['shots'])
		sizePerFrame = abcPerFrame + yetiPerFrame

		rawDict['sizePerFrame'] = sizePerFrame
		rawDict['abcPerFrame'] = abcPerFrame

		file_utils.json_dumper(assetData, rawDict)
	else: 
		logger.warning('skip adding, src exists %s' % srcPath)

	return rawDict


def get_shot_data(shotPath): 
	# iterate through step 
	shotName = os.path.split(shotPath)[-1]
	steps = file_utils.list_folder(shotPath)
	nsDict = dict()
	abcNss = []

	for step in steps: 
		nsPath = '%s/%s/output' % (shotPath, step)
		namespaces = file_utils.list_folder(nsPath)

		assetNames = [name_from_namespace(a) for a in namespaces if valid_namespace(a)]
		abcNs = [a for a in namespaces if not is_yeti_namespace(a)]
		yetiNs = [a for a in namespaces if is_yeti_namespace(a)]
		abcNss+=abcNs


		for ns in abcNs: 
			versionPath = '%s/%s' % (nsPath, ns)
			# loop through versions 
			versions = sorted(file_utils.list_folder(versionPath))
			abcVersion = versions[-1]
			name = name_from_namespace(ns)

			# yeti 
			yetiVersions = []
			yetiSize = 0
			yetiVersion = None

			for yns in yetiNs: 
				if name in yns: 
					yetiVersionPath = '%s/%s' % (nsPath, yns)
					print ns, yns 
					versions = sorted(file_utils.list_folder(versionPath))
					if versions: 
						maxVersion = versions[-1]
						if not maxVersion in yetiVersions: 
							yetiVersions.append(maxVersion)
					yetiSize += get_size(yetiVersionPath)

			# write data 
			# abc 
			abcSize = get_size(versionPath)
			print 'abcSize', versionPath, abcSize
			# yeti 
			if yetiVersions: 
				yetiVersion = sorted(yetiVersions)[-1]

			if name in nsDict.keys(): 
				oldAbcVersion = nsDict[name]['abcVersion']
				oldYetiVersion = nsDict[name]['yetiVersion']
				abcVersion = sorted([abcVersion, oldAbcVersion])[-1]
				yetiVersion = sorted([yetiVersion, oldYetiVersion])[-1]

			nsDict[name] = {'assetName': name, 'abcsize': abcSize, 'abcVersion': abcVersion, 'yetisize': yetiSize, 'yetiVersion': yetiVersion, 'frame': 100, 'from': shotPath, 'instance': 0}

	for ns in nsDict.keys(): 
		count = 0
		for elm in list(set(abcNss)): 
			if ns in elm: 
				count+=1 

		nsDict[ns].update({'instance': count})

	return nsDict


def write_shot_data(shotPath): 
	shotName = os.path.split(shotPath)[-1]
	dataPath = '%s/%s' % (sceneDataDir, shotName)
	shotData = get_shot_data(shotPath)
	
	shotDict = OrderedDict()
	shotDict['name'] = shotName
	shotDict['assetCount'] = len(shotData.keys())
	shotDict['maxVersion'] = None
	shotDict['maxAssetCount'] = None
	shotDict['instance'] = 0
	shotDict['namespaces'] = []

	maxVersion = None
	assetMaxs = []
	instanceCount = 0

	for assetName, data in shotData.iteritems(): 
		abcSize = data.get('abcsize')
		yetiSize = data.get('yetisize')
		frame = data.get('frame')
		srcPath = data.get('from')
		abcVersion = data.get('abcVersion')
		yetiVersion = data.get('yetiVersion')
		assetMaxVersion = sorted([abcVersion, yetiVersion])[-1]
		instance = data.get('instance')
		shotDict['namespaces'].append({'name': assetName, 'maxVersion': assetMaxVersion})
		instanceCount += instance
		write_asset_stat(assetName, abcSize, yetiSize, frame, srcPath)
		
		calVersions = sorted([maxVersion, abcVersion, yetiVersion])
		newMaxVersion = calVersions[-1]
		if newMaxVersion in [abcVersion, yetiVersion]: 
			assetMaxs.append(assetName)
			maxVersion = newMaxVersion

	shotDict['maxVersion'] = maxVersion
	shotDict['maxAssetCount'] = assetMaxs
	shotDict['instance'] = instanceCount

	file_utils.json_dumper(dataPath, shotDict)


def valid_namespace(namespace): 
	elems = namespace.split('_')
	if len(elems) >= 2: 
		if elems[1].isdigit(): 
			return True 

def is_yeti_namespace(namespace): 
	if valid_namespace(namespace): 
		elems = namespace.split('_')
		if len(elems) > 2: 
			return True

def name_from_namespace(namespace): 
	if '_cam' in namespace: 
		return namespace
	return namespace.split('_')[0]


def get_size(start_path = '.'):
    total_size = 0
    for dirpath, dirnames, filenames in os.walk(start_path):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            # skip if it is symbolic link
            if not os.path.islink(fp):
                total_size += os.path.getsize(fp)

    return total_size
	
# >>> b = os.path.getsize("/path/isa_005.mp3")
# - {'abcsize': filesize, 'yetisize': yetisize, 'frame': frame, 'from': 'path'}
# - {'abcsize': filesize, 'yetisize': yetisize, 'frame': frame, 'from': 'path'}
# - {'abcsize': filesize, 'yetisize': yetisize, 'frame': frame, 'from': 'path'}
# - {'abcsize': filesize, 'yetisize': yetisize, 'frame': frame, 'from': 'path'}

# Shot json 
# name: 
# 	assetCount: 5
# 	maxVersion: 10
# 	maxAssetCount: arthur
# 	instanceCount: 10
# 	namespaces: 
# 	- {'namespace': namespace, 'revision': revision}

