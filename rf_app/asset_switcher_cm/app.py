_title = 'Riff Asset Switcher'
_version = 'v.0.0.1'
_des = 'beta'
uiName = 'RFAssetSwitcher'

#Import python modules
import sys
import os
import getpass
import json
import logging

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)

# import config
import rf_config as config
from rf_utils import log_utils
# sg

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

import ui
reload(ui)
import modules
reload(modules)

class RFAssetSwitcher(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        #SetUp Window
        super(RFAssetSwitcher, self).__init__(parent)
        #ui read
        self.asset_path = {"cloudmaker":"P:/CloudMaker/all/asset"}
        self.ui = ui.AssetSwitch(parent)
        self.setObjectName(uiName)
        self.modules = modules
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.setCentralWidget(self.ui)
        self.adjustSize()
        self.setFixedSize(600,500)
        self.display_locator()
        self.init_state()
        self.init_display_ref()
        # self.set_logo()

    def init_state(self):
        self.ui.locator_switch_push.clicked.connect(self.locator_switch)
        self.ui.locator_list_widget.itemSelectionChanged.connect(self.selected_items)
        self.ui.locator_switch_all_push.clicked.connect(self.locator_switch_all)
        self.ui.type_combo_box.currentIndexChanged.connect(self.display_ref)
        self.ui.ref_switch_push.clicked.connect(self.ref_switch)
        self.ui.ref_switch_all_push.clicked.connect(self.ref_switch_all)
        self.ui.refresh_pushbutton.clicked.connect(self.refresh_ui)

    def refresh_ui(self):
        self.ui.locator_list_widget.clear()
        self.ui.ref_list_widget.clear()
        self.display_locator()
        self.init_display_ref()

    def list_asset(self):
        self.dict_locator_data = self.modules.list_locator()
        return self.dict_locator_data

    def display_locator(self):
        self.list_asset()
        for name, value in self.dict_locator_data.iteritems():
            data = [name, value]
            self.ui.add_locator_wid_item(name ,'needFix', data)

    def locator_switch(self):
        items_all = self.ui.locator_list_widget.selectedItems()
        for item in items_all:
            name, path = item.data(QtCore.Qt.UserRole)
            dir_name = os.path.dirname(path)
            base_name = os.path.basename(path).split('_')[0]
            res = self.ui.locator_combo_box.currentText()
            file_name = '%s_%s.ma'% (base_name, res)
            ref_path = os.path.join(dir_name, file_name).replace('\\', '/')
            dst_path = self.modules.chk_exist_file_locator(ref_path)
            if dst_path:
                list_new_trans = self.modules.reference_file(dst_path, base_name)
                place_ctrl = self.modules.get_place_ctrl(list_new_trans)
                self.modules.snap_obj(name, place_ctrl)
                self.ui.set_icon(item, 'success')
                self.modules.delete(name)

    def locator_switch_all(self):
        count = self.ui.locator_list_widget.count()
        for index in xrange(self.ui.locator_list_widget.count()):
            item = self.ui.locator_list_widget.item(index)
            name, path = item.data(QtCore.Qt.UserRole)
            dir_name = os.path.dirname(path)
            base_name = os.path.basename(path).split('_')[0]
            res = self.ui.locator_combo_box.currentText()
            file_name = '%s_%s.ma'% (base_name, res)
            ref_path = os.path.join(dir_name, file_name).replace('\\', '/')
            dst_path = self.modules.chk_exist_file_locator(ref_path)
            if dst_path:
                list_new_trans = self.modules.reference_file(dst_path, base_name)
                place_ctrl = self.modules.get_place_ctrl(list_new_trans)
                self.modules.snap_obj(name, place_ctrl)
                self.ui.set_icon(item, 'success')
                self.modules.delete(name)

    def selected_items(self):
        items_all = self.ui.locator_list_widget.selectedItems()
        sel = []
        for item in items_all:
            name, path = item.data(QtCore.Qt.UserRole)           
            sel.append(name)
        self.modules.select_add(sel)  

    def init_display_ref(self):
        dict_data, list_type_asset = self.modules.get_data_ref()
        self.dict_data = dict_data
        self.ui.add_type_combo_box(list_type_asset)
        self.display_ref()

    def display_ref(self):
        self.ui.ref_list_widget.clear()
        asset_type = self.ui.type_combo_box.currentText()
        for key, value in self.dict_data.iteritems():
            
            path ,base_name, type_data, name, ref_node, status  = value
            data = [path ,base_name, type_data, name, ref_node, status]
            if type_data == asset_type:
                self.ui.add_ref_wid_item(base_name, status, data)
            elif asset_type == 'All':
                self.ui.add_ref_wid_item(base_name, status, data)
        list_res = self.get_res()
        self.ui.add_ref_combo_box(list_res)

    def get_res(self):
        count = self.ui.ref_list_widget.count()
        list_res = []
        for index in xrange(self.ui.ref_list_widget.count()):
            item = self.ui.ref_list_widget.item(index)
            path ,base_name, type_data, name, ref_node, status = item.data(QtCore.Qt.UserRole)
            hero_path = os.path.dirname(path)
            list_file = os.listdir(hero_path)
            for file in list_file:
                tmp_path = os.path.join(hero_path, file).replace('\\', '/')
                file_path,ext = os.path.splitext(tmp_path)
                if os.path.isfile(tmp_path) and ext == '.ma':
                    name = os.path.basename(file_path)
                    res = name.split('_')[1]
                    if not res in list_res:
                        list_res.append(res)
        return list_res

    def ref_switch(self):
        items_all = self.ui.ref_list_widget.selectedItems()
        res = self.ui.ref_combo_box.currentText()
        for item in items_all:
            srcpath ,base_name, type_data, name, ref_node, status = item.data(QtCore.Qt.UserRole)
            if type_data == 'Tree':
                data = [srcpath ,base_name, type_data, name, ref_node, status]
                self.reload_ref_tree(item, data, res)
            dir_path = os.path.dirname(srcpath).replace('\\', '/')
            base_name = '%s_%s.ma'%(name,res)
            dst_path = os.path.join(dir_path, base_name).replace('\\', '/')
            print dst_path
            if srcpath != dst_path and type_data != 'Tree':
                chk = self.modules.replace_ref(dst_path, ref_node, srcpath)
                print chk
                if chk:
                    self.ui.set_icon(item, 'success')
                    data = [dst_path, base_name, type_data, name, ref_node, 'success']
                    self.ui.set_item_data(item, base_name, data)
                    self.dict_data[ref_node] = data

    
    def ref_switch_all(self):
        count = self.ui.ref_list_widget.count()
        res = self.ui.ref_combo_box.currentText()
        for index in xrange(self.ui.ref_list_widget.count()):
            item = self.ui.ref_list_widget.item(index)
            srcpath ,base_name, type_data, name, ref_node, status = item.data(QtCore.Qt.UserRole)
            if type_data == 'Tree':
                data = [srcpath ,base_name, type_data, name, ref_node, status]
                self.reload_ref_tree(item, data, res)
            dir_path = os.path.dirname(srcpath).replace('\\', '/')
            base_name = '%s_%s.ma'%(name,res)
            dst_path = os.path.join(dir_path, base_name).replace('\\', '/')
            if srcpath != dst_path and type_data != 'Tree':
                chk = self.modules.replace_ref(dst_path, ref_node, srcpath)
                if chk:
                    self.ui.set_icon(item, 'success')
                    data = [dst_path, base_name, type_data, name, ref_node, 'success']
                    self.ui.set_item_data(item, base_name, data)
                    self.dict_data[ref_node] = data

    def reload_ref_tree(self, item, data, res):
        srcpath ,base_name, type_data, name, ref_node, status = data
        dir_path = os.path.dirname(srcpath).replace('\\', '/')
        base_name = os.path.basename(srcpath).replace('\\', '/')
        if res == 'Rs':
            if 'Abc' in base_name:
                base_name = base_name.replace('Abc', 'Rs')
                dst_path = os.path.join(dir_path, base_name).replace('\\', '/')
                if srcpath != dst_path:
                    chk = self.modules.replace_ref(dst_path, ref_node, srcpath)
                    if chk:
                        self.ui.set_icon(item, 'success')
                        data = [dst_path, base_name, type_data, name, ref_node, 'success']
                        self.ui.set_item_data(item, base_name, data)
                        self.dict_data[ref_node] = data

        elif res == 'shad':
            if 'med' in base_name:
                base_name = base_name.replace('med', 'shad')
                dst_path = os.path.join(dir_path, base_name).replace('\\', '/')
                if srcpath != dst_path:
                    chk = self.modules.replace_ref(dst_path, ref_node, srcpath)
                    if chk:
                        self.ui.set_icon(item, 'success')
                        data = [dst_path, base_name, type_data, name, ref_node, 'success']
                        self.ui.set_item_data(item, base_name, data)
                        self.dict_data[ref_node] = data

        elif res == 'stillRs':
            if 'gpu' in base_name:
                base_name = base_name.replace('gpu', 'stillRs')
            elif 'card' in base_name:
                base_name = base_name.replace('card', 'stillRs')
            else:
                return
            dst_path = os.path.join(dir_path, base_name).replace('\\', '/')
            if srcpath != dst_path:
                chk = self.modules.replace_ref(dst_path, ref_node, srcpath)
                if chk:
                    self.ui.set_icon(item, 'success')
                    data = [dst_path, base_name, type_data, name, ref_node, 'success']
                    self.ui.set_item_data(item, base_name, data)
                    self.dict_data[ref_node] = data


def show():
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        maya_win.deleteUI(uiName)
        myApp = RFAssetSwitcher(maya_win.getMayaWindow())
        myApp.show()
        return myApp
if __name__ == '__main__':
    show()
