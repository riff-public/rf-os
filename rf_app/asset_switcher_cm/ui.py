import os
import sys

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
from rf_utils import icon
class AssetSwitch(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(AssetSwitch, self).__init__(parent)
        self.refresh_pushbutton = QtWidgets.QPushButton('refresh')
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.logo_label = QtWidgets.QLabel()
        self.verticalLayout.addWidget(self.logo_label)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.verticalLayout_4 = QtWidgets.QVBoxLayout()
        self.locator_label = QtWidgets.QLabel('Locator')
        self.locator_label.setAlignment(QtCore.Qt.AlignCenter)
        self.verticalLayout_4.addWidget(self.locator_label)
        self.locator_list_widget = QtWidgets.QListWidget()
        self.locator_list_widget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.locator_list_widget.setResizeMode(QtWidgets.QListView.Adjust)
        self.verticalLayout_4.addWidget(self.locator_list_widget)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.locator_combo_box = QtWidgets.QComboBox()
        self.locator_combo_box.addItem("hiAbc")
        self.locator_combo_box.addItem("lowAbc")
        self.locator_combo_box.addItem("midAbc")
        self.horizontalLayout_2.addWidget(self.locator_combo_box)
        self.locator_switch_push = QtWidgets.QPushButton("Switch")
        self.locator_switch_all_push = QtWidgets.QPushButton("Switch All")
        self.horizontalLayout_2.addWidget(self.locator_switch_push)
        self.verticalLayout_4.addLayout(self.horizontalLayout_2)
        self.verticalLayout_4.addWidget(self.locator_switch_all_push)
        self.horizontalLayout.addLayout(self.verticalLayout_4)
        self.verticalLayout_5 = QtWidgets.QVBoxLayout()
        self.ref_label = QtWidgets.QLabel('Reference')
        self.ref_label.setAlignment(QtCore.Qt.AlignCenter)
        self.type_combo_box = QtWidgets.QComboBox()
        self.type_combo_box.addItem("")
        self.verticalLayout_5.addWidget(self.ref_label)
        self.horizontalLayout_4 =QtWidgets.QHBoxLayout()
        self.type_label = QtWidgets.QLabel('type')
        self.type_label.setAlignment(QtCore.Qt.AlignCenter)
        self.horizontalLayout_4.addWidget(self.type_label)
        self.horizontalLayout_4.addWidget(self.type_combo_box)
        self.verticalLayout_5.addLayout(self.horizontalLayout_4)
        self.ref_list_widget = QtWidgets.QListWidget()
        self.ref_list_widget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.ref_list_widget.setResizeMode(QtWidgets.QListView.Adjust)
        self.verticalLayout_5.addWidget(self.ref_list_widget)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.ref_combo_box = QtWidgets.QComboBox()
        self.ref_combo_box.addItem("")
        self.horizontalLayout_3.addWidget(self.ref_combo_box)
        self.ref_switch_push = QtWidgets.QPushButton('Switch')
        self.ref_switch_all_push = QtWidgets.QPushButton('Switch All')
        self.horizontalLayout_3.addWidget(self.ref_switch_push)
        self.verticalLayout_5.addLayout(self.horizontalLayout_3)
        self.verticalLayout_5.addWidget(self.ref_switch_all_push)
        self.horizontalLayout.addLayout(self.verticalLayout_5)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.verticalLayout_2.addLayout(self.verticalLayout)
        self.verticalLayout_2.addWidget( self.refresh_pushbutton)
        self.setLayout(self.verticalLayout_2)

    def add_locator_wid_item(self, text, status, data):
        wid_item = QtWidgets.QListWidgetItem(self.locator_list_widget)
        wid_item.setText(text)
        wid_item.setData(QtCore.Qt.UserRole, data)
        self.set_icon(wid_item, status)

    def set_icon(self, wid_item, status):
        if status == 'success':
            wid_item.setIcon(QtGui.QPixmap(icon.success))
        elif status == 'needFix':
            wid_item.setIcon(QtGui.QPixmap(icon.needFix))
        elif status == 'no':
            wid_item.setIcon(QtGui.QPixmap(icon.no))

    def add_ref_wid_item(self, text, status, data):
        wid_item = QtWidgets.QListWidgetItem(self.ref_list_widget)
        wid_item.setText(text)
        wid_item.setData(QtCore.Qt.UserRole, data)
        self.set_icon(wid_item, status)

    def add_type_combo_box(self, list_type):
        self.type_combo_box.clear()
        self.type_combo_box.addItem('All')
        for type_kind in list_type:
            self.type_combo_box.addItem(type_kind)
        self.type_combo_box.setCurrentIndex(0)

    def add_ref_combo_box(self, list_res):
        self.ref_combo_box.clear()
        if self.type_combo_box.currentText() == 'Tree':
            self.ref_combo_box.addItem('Rs')
            self.ref_combo_box.addItem('shade')
            self.ref_combo_box.addItem('stillRs')
        else:
            for res in list_res:
                self.ref_combo_box.addItem(res)
        self.ref_combo_box.setCurrentIndex(0)

    def set_item_data(self, wid_item, text, data):
        wid_item.setText(text)
        wid_item.setData(QtCore.Qt.UserRole, data)

