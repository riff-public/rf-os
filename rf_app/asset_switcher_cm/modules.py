import os 
import sys
import maya.cmds as mc
from rf_utils.context import context_info

def list_locator():
    dict_data = {}
    all_loc = mc.ls(type='locator')
    for loc in all_loc:
        locator = parent = mc.listRelatives( loc, fullPath=True, parent=True )
        chkc = mc.listAttr( locator[0] ,string='rootPath')
        if chkc:
            path = mc.getAttr("%s.rootPath"%locator[0])
            dict_data[locator[0]] = path.replace('\\', '/')
    return dict_data

def reference_file(file_path, namespace):
    before = set(mc.ls(type='transform'))
    mc.file(file_path, reference=True, namespace=namespace)
    after = set(mc.ls(type='transform'))
    imported_trans = after - before
    return imported_trans

def get_place_ctrl(list_trans):
    for tran in list_trans:
        if 'Place_Ctrl' in tran:
            return tran

def chk_exist_file_locator(file_path):
    list_type = ['med', 'shad']
    if not os.path.exists(file_path):
        base_name = os.path.basename(file_path).split('_')[0] # BushB_med.ma>> BushB
        dir_path = os.path.dirname(file_path)
        for type_name in list_type:
            new_base_name = '%s_%s.ma'%(base_name, type_name)
            print new_base_name
            dst_path = os.path.join(dir_path, new_base_name).replace('\\', '/')
            if os.path.exists(dst_path):
                return dst_path
        return False
    else:
        return file_path



def snap_obj(obj1, obj2):

    scale =mc.getAttr('%s.scale'%obj1)[0]
    place_ctrl = obj2.replace('Rig_Grp', 'Place_Ctrl')
    mc.setAttr('%s.scaleY'%place_ctrl, scale[1])
    mc.delete(mc.parentConstraint(obj1, obj2))

def select_add(items):
    mc.select(clear=True)
    mc.select(items)

def delete(obj):
    mc.delete(obj)

def get_data_ref():
    
    ref_node = get_nodes_from_path()
    dict_data= {}
    list_type = []
    for ref in ref_node:
        try:
            path =  mc.referenceQuery(ref, wcn=True, f=True)
            tem_path = path.split('asset/')[1]
            base_name = os.path.basename(path)
            type_data = tem_path.split('/')[0]
            name = tem_path.split('/')[2]
            if 'Tree' in path:
                type_data = 'Tree'

            if 'Rs' in base_name:
                status = 'success'
            elif 'shad' in base_name:
                status = 'success'
            else:
                status = 'needFix'

            dict_data[ref] = [path ,base_name, type_data, name, ref, status]
            if not type_data in list_type:
                list_type.append(type_data)
        except:
            print ref,'unknown'

    return dict_data, list_type

def replace_ref(dst_path, ref_node, srcpath):
    if os.path.exists(dst_path):
        mc.file( dst_path , loadReference = ref_node , prompt = False )
        return True
    else:
        return False

def get_nodes_from_path():
    ref_node = []
    ref_path = mc.file(r=True, q=True)
    for path in ref_path:
        node = mc.referenceQuery( path, referenceNode=True)
        ref_node.append(node)
    return ref_node