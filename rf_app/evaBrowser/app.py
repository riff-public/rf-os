_title = 'Eva Browser'
_version = 'v.0.0.1'
_uiName = 'EvaBrowserUI'
_user = 'riff'
_proj = 'ev04'
_ext = '.ma'

import os, sys
import getpass

import maya.cmds as mc
import pymel.core as pm

import rf_config as config
from rf_utils import log_utils
from rf_utils.ui import load
from rf_utils.widget import input_widget, ad_input_widget, file_widget, display_widget, auto_build_widget

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
userLog = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(_uiName, userLog)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide2', 'PySide'])
from Qt import wrapInstance, QtGui, QtCore, QtWidgets

class EvaBrowser(QtWidgets.QMainWindow):
    '''
    from rf_app.evaBrowser import app as evapp
    reload(evapp)
    evapp.show()
    '''
    def __init__(self, parent=None):
        super(EvaBrowser, self).__init__(parent)
        self.root_path = 'Z:/pj/ev04/shots'
        self.work_path = ''

        # ui read
        uiFile = '%s/ui.ui' % moduleDir
        self.ui = load.setup_ui_maya(uiFile, parent)

        # work file widget
        self.file_widget = file_widget.FileListWidgetExtended()
        self.ui.workfile_layout.addWidget(self.file_widget)

        # signals
        self.ui.ep_comboBox.currentIndexChanged.connect(self.ep_changed)
        self.ui.seq_comboBox.currentIndexChanged.connect(self.seq_changed)
        self.ui.shot_comboBox.currentIndexChanged.connect(self.populate_files)
        self.file_widget.listWidget.itemDoubleClicked.connect(self.openFile)
        self.ui.open_pushButton.clicked.connect(self.openFile)
        self.ui.save_pushButton.clicked.connect(self.saveFile)

        # populate folders
        self.populate_ep()
        self.populate_files()

        # show the UI
        self.ui.show()

    def populate_ep(self):
        self.ui.ep_comboBox.clear()
        for f in os.listdir(self.root_path):
            if os.path.isdir('%s/%s' %(self.root_path, f)):
                self.ui.ep_comboBox.addItem(f)

    def populate_seq(self):
        self.ui.seq_comboBox.clear()
        curr_ep = self.ui.ep_comboBox.currentText()
        if curr_ep:
            path = '%s/%s' %(self.root_path, curr_ep)
            for f in os.listdir(path):
                if os.path.isdir('%s/%s' %(path, f)):
                    self.ui.seq_comboBox.addItem(f)

    def populate_shot(self):
        self.ui.shot_comboBox.clear()
        curr_ep = self.ui.ep_comboBox.currentText()
        curr_seq = self.ui.seq_comboBox.currentText()
        if curr_ep and curr_seq:
            path = '%s/%s/%s' %(self.root_path, curr_ep, curr_seq)
            for f in os.listdir(path):
                if os.path.isdir('%s/%s' %(path, f)):
                    self.ui.shot_comboBox.addItem(f)

    def ep_changed(self):
        self.populate_seq()
        self.populate_shot()

    def seq_changed(self):
        self.populate_shot()

    def populate_files(self):
        self.file_widget.listWidget.clear()

        curr_ep = self.ui.ep_comboBox.currentText()
        curr_seq = self.ui.seq_comboBox.currentText()
        curr_shot = self.ui.shot_comboBox.currentText()
        purpose = self.ui.purpose_comboBox.currentText()
        pipeline = self.ui.pipeline_comboBox.currentText()
        task = self.ui.task_comboBox.currentText()
        self.work_path = ''
        if curr_ep and curr_seq and curr_shot:
            self.work_path = '%s/%s/%s/%s/%s/%s/%s/%s' %(self.root_path, curr_ep, curr_seq, curr_shot, purpose, _user, pipeline, task)
            for f in os.listdir(self.work_path):
                if os.path.isfile('%s/%s' %(self.work_path, f)):
                    full_path = '%s/%s' % (self.work_path, f)
                    self.file_widget.add_file(full_path.decode('utf8'))

    def openFile(self):
        sel_file = self.file_widget.listWidget.currentItem().text()
        if sel_file:
            full_path = '%s/%s' %(self.work_path, sel_file)
            if os.path.exists(full_path):
                if mc.file(q=True, mf=True):
                    confirm = mc.confirmDialog(t="Warning: Scene Not Saved", m="Save changes to %s?" %pm.sceneName(), button = ["Save", "Don't Save", "Cancel"], 
                                cancelButton="Cancel", dismissString='Cancel')
                    if confirm == "Cancel":
                        return
                    elif confirm == "Save":
                        pm.saveFile()
                pm.openFile(full_path, f=True)
            else:
                logger.error('Path does not exist: %s' %full_path) 

    def get_next_version(self, path):
        files = [f for f in os.listdir(path) if os.path.isfile('%s/%s' %(path, f)) and len(f.split('_'))==6 and f.startswith(_proj)]
        if files:
            files = sorted(files)
            last_file = files[-1]
            last_file, ext = os.path.splitext(last_file)
            splits = last_file.split('_')
            curr_ver = int(splits[-1])
            next_ver = '%s' %(curr_ver + 1)
            return next_ver.zfill(2)
        else:
            return '01'

    def saveFile(self):
        curr_ep = self.ui.ep_comboBox.currentText()
        curr_seq = self.ui.seq_comboBox.currentText()
        curr_shot = self.ui.shot_comboBox.currentText()
        purpose = self.ui.purpose_comboBox.currentText()
        pipeline = self.ui.pipeline_comboBox.currentText()
        task = self.ui.task_comboBox.currentText()

        dir_path = '%s/%s/%s/%s/%s/%s/%s/%s' %(self.root_path, curr_ep, curr_seq, curr_shot, purpose, _user, pipeline, task)
        ver = self.get_next_version(path=dir_path)
        fileName = '%s_%s_%s_%s_k01_%s%s' %(_proj, curr_seq, curr_shot, pipeline, ver, _ext)
        full_path = '%s/%s' %(dir_path, fileName)
        logger.info('Save as: %s' %(full_path))
        pm.saveAs(full_path, f=True)
        self.populate_files()

        # select the last item
        items = self.file_widget.listWidget.findItems(fileName, QtCore.Qt.MatchExactly)
        if items:
            self.file_widget.listWidget.setCurrentItem(items[0])

def show():
    from rftool.utils.ui import maya_win
    logger.info('Run in Maya\n')
    maya_win.deleteUI(_uiName)
    myApp = EvaBrowser(maya_win.getMayaWindow())
    return myApp


if __name__ == '__main__':
    show()
