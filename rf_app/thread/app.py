_title = 'Riff Sequence Design Animatic Publish'
_version = 'v.0.0.2'
_des = 'beta'
uiName = 'ThreadUI'

#Import python modules
import sys
import os
import getpass
import json
import logging
import time

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)

# import config
import rf_config as config

# framework modules
from rf_utils import log_utils
from rf_utils.ui import load
from rf_utils.ui import stylesheet

from rf_utils.sg import sg_utils
sg = sg_utils.sg



user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui


class getSGThread(QtCore.QThread):
    value = QtCore.Signal(int)
    def __init__(self):
        QtCore.QThread.__init__(self)

    def run(self):
        print 'starting thread'
        for i in range(10):
            time.sleep(0.5)
            print i
            self.value.emit(i)
        # self.emit(QtCore.SIGNAL('asset(QList)'), result)

class Worker(QtCore.QThread):

    #This is the signal that will be emitted during the processing.
    #By including int as an argument, it lets the signal know to expect
    #an integer argument when emitting.
    updateProgress = QtCore.Signal(int)

    #You can do any extra things in this init you need, but for this example
    #nothing else needs to be done expect call the super's init
    def __init__(self):
        QtCore.QThread.__init__(self)

    #A QThread is run by calling it's start() function, which calls this run()
    #function in it's own "thread".
    def run(self):
        #Notice this is the same thing you were doing in your progress() function
        for i in range(1, 100):
            #Emit the signal so it can be received on the UI side.
            self.updateProgress.emit(i)
            print i
            time.sleep(0.1)

class RFAssetDesignPublish(QtWidgets.QMainWindow):

    def __init__(self, parent=None):
        #Setup Window
        super(RFAssetDesignPublish, self).__init__(parent)

        # ui read
        uiFile = '%s/ui.ui' % moduleDir
        self.ui = load.setup_ui(uiFile, self)

        self.ui.show()
        self.ui.setWindowTitle('%s %s-%s' % (_title, _version, _des))

        self.init_functions()


    def init_functions(self):
        self.ui.pushButton.clicked.connect(self.get_asset)

    def get_asset(self):
        # self.connect(thread, QtCore.SIGNAL("asset(QList)"), self.list_asset)
        self.thread = Worker()
        self.thread.updateProgress.connect(self.list_asset)
        self.thread.finished.connect(self.finish)
        self.thread.start()

    def list_asset(self, i):
        print 'signal from thread'
        self.ui.asset_listWidget.addItem(str(i))

    def finish(self):
        print 'finish'

def show():
    logger.info('RFAssetDesignPublish Starting ...')
    logger.info('---------------------------')
    logger.info('---------------------------')
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('%s Running in Maya\n' % _title)
        maya_win.deleteUI(uiName)
        myApp = RFAssetDesignPublish(maya_win.getMayaWindow())
        return myApp
    else:
        app = QtWidgets.QApplication.instance()
        if not app:
            app = QtWidgets.QApplication(sys.argv)
        myApp = RFAssetDesignPublish()
        stylesheet.set_default(app)
        sys.exit(app.exec_())

if __name__ == '__main__':
    show()