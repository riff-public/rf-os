import os 
import sys
import time
sys.path.append('%s/core' % os.environ.get('RFSCRIPT'))

import rf_config as config 
from rf_utils.ui import load 

from Qt import QtWidgets
from Qt import QtGui
from Qt import QtCore

moduleDir = os.path.dirname(sys.modules[__name__].__file__)


class Tool(QtWidgets.QMainWindow):
	"""docstring for Tool"""
	def __init__(self, parent=None):
		super(Tool, self).__init__(parent=parent)
		uiFile = '%s/ui.ui' % moduleDir
		self.ui = load.setup_ui(uiFile, self)
		self.ui.show()

		self.ui.pushButton.clicked.connect(self.run)
		self.ui.progressBar.setValue(0)

	def run(self): 
		self.thread = Worker()
		self.thread.updateProgress.connect(self.update_bar)
		self.thread.finished.connect(self.finished)
		self.thread.start()

	def update_bar(self, var): 
		print var 
		self.ui.progressBar.setValue(var)

	def finished(self): 
		print 'finished'




class Worker(QtCore.QThread):

    #This is the signal that will be emitted during the processing.
    #By including int as an argument, it lets the signal know to expect
    #an integer argument when emitting.
    updateProgress = QtCore.Signal(int)

    #You can do any extra things in this init you need, but for this example
    #nothing else needs to be done expect call the super's init
    def __init__(self):
        QtCore.QThread.__init__(self)

    #A QThread is run by calling it's start() function, which calls this run()
    #function in it's own "thread".
    def run(self):
        #Notice this is the same thing you were doing in your progress() function
        for i in range(1, 101):
            #Emit the signal so it can be received on the UI side.
            self.updateProgress.emit(i)
            time.sleep(0.1)
            print i

def show():
    if config.isMaya: 
        from rftool.utils.ui import maya_win
        maya_win.deleteUI(uiName)
        myApp = Tool(maya_win.getMayaWindow())
        return myApp
    else: 
        app = QtWidgets.QApplication.instance()
        if not app: 
            app = QtWidgets.QApplication(sys.argv)
        myApp = Tool()
        sys.exit(app.exec_())

if __name__ == '__main__': 
    show()
