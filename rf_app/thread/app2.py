#!/usr/bin/env python2

import sys, time
import os
sys.path.append('%s/core' % os.environ['RFSCRIPT'])
moduleDir = os.path.dirname(sys.modules[__name__].__file__)

import rf_config as confing
from rf_utils.ui import load

from Qt import QtCore, QtGui, QtWidgets, QtUiTools
import time


def setup_ui(uifile, base_instance=None):
    loader = QtUiTools().QUiLoader()
    uifile = QtCore.QFile(uifile)
    uifile.open(QtCore.QFile.ReadOnly)
    ui = loader.load(uifile, base_instance)
    uifile.close()
    return ui


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(400, 133)
        self.progressBar = QtWidgets.QProgressBar(Dialog)
        self.progressBar.setGeometry(QtCore.QRect(20, 10, 361, 23))
        self.progressBar.setProperty("value", 24)
        self.progressBar.setObjectName("progressBar")
        self.pushButton = QtWidgets.QPushButton(Dialog)
        self.pushButton.setGeometry(QtCore.QRect(20, 40, 361, 61))
        self.pushButton.setObjectName("pushButton")

        self.worker = Worker()
        self.worker.updateProgress.connect(self.setProgress)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

        self.progressBar.minimum = 1
        self.progressBar.maximum = 100

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QtWidgets.QApplication.translate("Dialog", "Dialog", None, QtWidgets.QApplication.UnicodeUTF8))
        self.pushButton.setText(QtWidgets.QApplication.translate("Dialog", "PushButton", None, QtWidgets.QApplication.UnicodeUTF8))
        self.progressBar.setValue(0)
        self.pushButton.clicked.connect(self.worker.start)

    def setProgress(self, progress):
        self.progressBar.setValue(progress)


class Tool(QtWidgets.QMainWindow):
    """docstring for Tool"""
    def __init__(self):
        super(Tool, self).__init__()

        self.widget = QtWidgets.QWidget()
        self.layout = QtWidgets.QVBoxLayout(self.widget)
        self.progressBar = QtWidgets.QProgressBar()
        self.listWidget = QtWidgets.QListWidget()
        self.pushButton = QtWidgets.QPushButton()
        self.progressBar.setValue(0)

        self.layout.addWidget(self.progressBar)
        self.layout.addWidget(self.listWidget)
        self.layout.addWidget(self.pushButton)
        self.setLayout(self.layout)
        self.setCentralWidget(self.widget)

        self.pushButton.clicked.connect(self.run)

    def run(self):
        self.thread = Worker()
        self.thread.updateProgress.connect(self.add_item)
        self.thread.finished.connect(self.finish)
        self.thread.start()

    def add_item(self, val):
        self.listWidget.addItem(str(val))
        self.progressBar.setValue(val)

    def finish(self):
        print 'finished'


class Tool2(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        #Setup Window
        super(Tool2, self).__init__(parent)

        # ui read
        uiFile = '%s/ui.ui' % moduleDir
        # self.ui = setup_ui(uiFile, self)
        self.ui = load.setup_ui_maya(uiFile, self)
        self.ui.show()

        self.ui.pushButton.clicked.connect(self.run)
        self.ui.progressBar.setValue(0)

    def run(self):
        self.thread = Worker()
        self.thread.updateProgress.connect(self.add_item)
        self.thread.finished.connect(self.finish)
        self.thread.start()

    def add_item(self, val):
        self.ui.listWidget.addItem(str(val))
        self.ui.progressBar.setValue(val)

    def finish(self):
        print 'finished'

#Inherit from QThread
class Worker(QtCore.QThread):

    #This is the signal that will be emitted during the processing.
    #By including int as an argument, it lets the signal know to expect
    #an integer argument when emitting.
    updateProgress = QtCore.Signal(int)

    #You can do any extra things in this init you need, but for this example
    #nothing else needs to be done expect call the super's init
    def __init__(self):
        QtCore.QThread.__init__(self)

    #A QThread is run by calling it's start() function, which calls this run()
    #function in it's own "thread".
    def run(self):
        #Notice this is the same thing you were doing in your progress() function
        for i in range(1, 101):
            #Emit the signal so it can be received on the UI side.
            self.updateProgress.emit(i)
            time.sleep(0.1)
            print i

# if __name__ == "__main__":
#     import sys
#     app = QtWidgets.QApplication.instance()
#     if not app:
#         app = QtWidgets.QApplication(sys.argv)
#     myApp = Tool2()
#     # myApp.show()
#     sys.exit(app.exec_())

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication.instance()
    if not app:
        app = QtWidgets.QApplication(sys.argv)
    myApp = Tool2()
    # myApp.show()
    sys.exit(app.exec_())