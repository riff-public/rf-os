# -*- mode: python ; coding: utf-8 -*-


block_cipher = None
script_root = os.environ.get('RFSCRIPT').replace('\\', '/')
added_files = [
         (f'{script_root}/core/rf_app/brief_board/icons/*', 'icons'),

         (f'{script_root}/core/rf_utils/widget/icons/task_status/*', 'icons/task_status'),
         (f'{script_root}/core/rf_utils/widget/icons/note_status/*', 'icons/note_status'),
         (f'{script_root}/core/rf_utils/widget/icons/gif/*', 'icons/gif'),
         (f'{script_root}/core/rf_utils/widget/icons/etc/*', 'icons/etc'),
         (f'{script_root}/core/rf_utils/context/context_config.yml', '.'),
         (f'{script_root}/core/rf_utils/styleSheets/maya/maya.css', 'core/rf_utils/styleSheets/maya'),
         (f'{script_root}/core/rf_utils/styleSheets/maya/images/*', 'core/rf_utils/styleSheets/maya/images'),

         (f'{script_root}/core/rf_config/*.py', '.'),
         (f'{script_root}/core/rf_config/*.yml', '.'),

         (f'{script_root}/core/rf_config/project/default/*.yml', 'core/rf_config/project/default'),
         (f'{script_root}/core/rf_config/project/Hanuman/*.yml', 'core/rf_config/project/Hanuman'),
         (f'{script_root}/core/rf_config/project/Bikey/*.yml', 'core/rf_config/project/Bikey'),
         (f'{script_root}/core/rf_config/project/Atama/*.yml', 'core/rf_config/project/Atama'),
         (f'{script_root}/core/rf_config/project/Crusher/*.yml', 'core/rf_config/project/Crusher')
         
         ]

a = Analysis(
    ['app.py'],
    pathex=[],
    binaries=[],
    datas=added_files,
    hiddenimports=[],
    hookspath=[],
    hooksconfig={},
    runtime_hooks=[],
    excludes=[],
    win_no_prefer_redirects=False,
    win_private_assemblies=False,
    cipher=block_cipher,
    noarchive=False,
)
pyz = PYZ(a.pure, a.zipped_data, cipher=block_cipher)

exe = EXE(
    pyz,
    a.scripts,
    a.binaries,
    a.zipfiles,
    a.datas,
    [],
    name='BriefBoard',
    debug=False,
    bootloader_ignore_signals=False,
    strip=False,
    upx=True,
    upx_exclude=[],
    runtime_tmpdir=None,
    console=False,
    disable_windowed_traceback=False,
    argv_emulation=False,
    target_arch=None,
    codesign_identity=None,
    entitlements_file=None,
    icon=f'{script_root}/core/rf_app/brief_board/icons/app_icon.ico'
)

# exe = EXE(pyz,
#           a.scripts,
#           [],
#           exclude_binaries=True,
#           name='BriefBoard',
#           debug=False,
#           bootloader_ignore_signals=False,
#           strip=False,
#           upx=True,
#           console=True , 
#           icon=f'{script_root}/core/rf_app/brief_board/icons/app_icon.ico')
# coll = COLLECT(exe,
#                a.binaries,
#                a.zipfiles,
#                a.datas,
#                strip=False,
#                upx=True,
#                upx_exclude=[],
#                name='BriefBoard')
