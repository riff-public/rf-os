import sys, os

core = '{}/core'.format(os.environ.get('RFSCRIPT'))
if core not in sys.path:
    sys.path.append(core)
# QT
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

import rf_config as config
from rf_utils.ui import stylesheet
from rf_utils.sg import sg_note
from rf_utils.sg import sg_process
from rf_utils import user_info
from rf_app.brief_board.app import FeedTreeWidget

sg = sg_process.sg
THIS_USER = user_info.User(sg=sg)

class TestApp(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super(TestApp, self).__init__(parent=parent)
        # setup UI
        self.setWindowTitle('Test Window')
        self.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))
        self.centralwidget = QtWidgets.QWidget(self)
        self.setCentralWidget(self.centralwidget)
        self.resize(750, 750)
        self.main_layout = QtWidgets.QVBoxLayout(self.centralwidget)

        # add feed widget
        self.feed_treewidget = FeedTreeWidget()
        self.feed_treewidget.statusChanged.connect(self.note_status_changed)  # status changed signal
        self.feed_treewidget.replySubmit.connect(self.submit_reply)  # reply signal
        self.main_layout.addWidget(self.feed_treewidget)

        # SHOW
        self.show_feed(project='Hanuman', assetName='testProp')

    def show_feed(self, project, assetName):
        # fetch feed
        entity = sg_process.get_one_asset(project, assetName)
        task_filters = []  # empty filter - get all task notes from the entity
        fields = ['project', 'note_links', 'created_at', 'updated_at', 'content', 'subject', 'sg_note_type',
            'tasks', 'user', 'sg_status_list', 'sg_modified_by', 'user.HumanUser.image', 'sg_brief',
            'addressings_to', 'sg_subnotes', 'note_sg_subnotes_notes', 'ticket_sg_note_tickets', 'addressings_cc', 
            'tasks.Task.due_date']

        feeds = sg_note.fetch_entity_notes_briefs(project=project, 
                                                sg_entity=entity, 
                                                task_filters=task_filters,
                                                fields=fields)  # skip attachments fields, will fetch em along with replies

        apr_versions = sg_note.fetch_client_approved_versions(project=project, 
                                                sg_entity=entity,
                                                task_filters=task_filters)

        # combine approved versions to feeds
        for taskname, versions in apr_versions.items():
            if taskname not in feeds:
                feeds[taskname] = []
            feeds[taskname].extend(versions)

        # skip closed notes
        feed_dict = {}  # convert to ---> {feed_id: feed}
        for taskname, feed_entities in feeds.items(): 
            for feed in feed_entities: 
                if feed.get(sg_note.Attr.note_type_attr) in (sg_note.Value.brief, sg_note.Value.note):
                    
                    if feed.get('sg_status_list') not in ['opn', 'cmpt', 'ip']:  # allowed note status
                        continue

                feed_id = feed.get('id')
                if feed_id not in feed_dict:
                    feed_dict[feed_id] = feed

        # sort + convert to list
        feeds = sorted(feed_dict.values(), key=lambda i: i.get('updated_at') if i.get(sg_note.Attr.note_type_attr) \
                in (sg_note.Value.brief, sg_note.Value.note) else i.get('client_approved_at') , reverse=True)

        # show to UI
        self.feed_treewidget.show_feeds(feeds=feeds)

    def submit_reply(self, signals):
        note_entity, content, user, attachments, item = signals

        reply_results = sg_note.add_reply(note_entity=note_entity, 
                            content=content, 
                            user_entity=user, 
                            attachments=attachments)

        itemWidget = self.feed_treewidget.itemWidget(item, 0)
        note_entity = itemWidget.data
        note_id = note_entity.get('id')
        # remove replybox
        self.feed_treewidget.cancel_reply(note_entity=note_entity, force=True)

        if reply_results:
            reply_entity = reply_results[0]
            # update feed item's reply
            self.feed_treewidget.refresh_new_reply(item=item, itemWidget=itemWidget, reply_entity=reply_entity)

    def note_status_changed(self, edit_data):
        item, note_id, status = edit_data

        updated_note = sg_note.update_note(note_id, status, THIS_USER.sg_user())
        updated_note_id = updated_note.get('id')

        # update feed item UI
        self.feed_treewidget.update_entity(item=item, data=updated_note)

def show():
    app = QtWidgets.QApplication(sys.argv)
    myApp = TestApp()
    myApp.show()
    stylesheet.set_default(app)
    sys.exit(app.exec_())

if __name__ == '__main__':
    show()
