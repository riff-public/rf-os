#!/usr/bin/env python
# -*- coding: utf-8 -*-

# beta     - under development
# v.1.0.0  - initial release
# v.1.0.1  - Fix error when clicking back and forth between jobs
#          - Add toolTips
#          - Add confirm messagebox when submiting reply
# v.1.0.2  - Fix all attachments showing on top NoteItem
# v.1.0.3  - Fix bug in user photo
# v.1.1.0  - Add support for client user
# v.1.2.0  - Add right-click menu on job item to open dir
# v.1.2.1  - Fix bug when user photo does not show (link expired)
#          - Only producers, supervisors, admin can view other user profiles
#          - Reply will now use current user not selected user
# v.1.2.2  - Fix bug error displaying Note typed Brief
#          - Fix duplicate link display texts
# v.1.2.3  - Fix SSL Bug
# v.1.2.4  - Fix UI: proxy item size, content sretch
# v.1.2.5  - Fix right-click menu on job item
# v.1.3.0  - Pack app as executable using PyInstaller
# v.1.3.1  - Add no preview path to image sequence publish if path does not exist on system 
# v.1.3.2  - Fix wrong date on FeedItem. Use 'created_at' instead of 'updated_at'
# v.1.3.3  - Fix wrong job due date. Job due date will ignore approved tasks
# v.1.3.4  - Cleanup code for portability

_title = 'Brief Board'
_version = 'v.1.3.4'
_des = ''
uiName = 'briefBoard'

# import system modules
import sys
import os

core = '{}/core'.format(os.environ.get('RFSCRIPT'))
if core not in sys.path:
    sys.path.append(core)

# built-in python modules
import tempfile
from urllib.request import urlretrieve
from glob import glob
from time import sleep
from subprocess import Popen 
from functools import partial
from collections import OrderedDict
from copy import deepcopy
from datetime import date, datetime, timedelta
import ssl
ssl._create_default_https_context = ssl._create_unverified_context

# QT
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

# custom modules
import rf_config as config
from rf_utils.ui import stylesheet
from rf_utils import user_info
from rf_utils import thread_pool
from rf_utils.sg import sg_process
from rf_utils.sg.sg_wrapper import ShotgunClient
from rf_utils.sg import sg_note
from rf_utils.context import context_info
from rf_utils.widget import textEdit
from rf_utils.widget import text_label
from rf_utils.widget import image_widget
from rf_utils.widget import status_widget
from rf_utils.widget import project_widget
from rf_utils.widget import display_widget
from rf_utils.widget import user_widget
from rf_utils.widget import filmstrip_widget
from rf_utils.widget import media_widget

is_python3 = True if sys.version_info[0] > 2 else False
moduleDir = os.path.dirname(sys.modules[__name__].__file__).replace('\\', '/')
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

# global const
sg = sg_process.sg
THIS_USER = user_info.User(sg=sg)
if not THIS_USER.sg_user():
    raise RuntimeError('No SG user found.')

# CLIENT = sg.find('ClientUser', [], ['name'])
ACTIVE_PROJECTS = sg_process.get_active_projects()  # SG project entity for active projects only

# app const
DAY_RANGE = 180  # number of active days used to find task
WARNING_DURATION = 2  # number of remaining days before due date for a task to get warning
NEW_ITEM_DAYS = 1  # number of days for a feed to be flaged as new
ATTACHMENT_FORMATS = ['.'+str(f.data().decode()).lower() for f in QtGui.QImageReader.supportedImageFormats()]  # note attachment image formats
ENTITY_STATUS_FILTERS = ['omt', 'apr']  # entity with these status will not be displayed
TASK_STATUS_FILTERS = ['omt']  # task with these status will not count
NOTE_STATUS = ['opn', 'cmpt', 'ip']  # allowed note status
USER_EDITABLE_STATUS = {True: 'ip', False: 'wtg'}  # status user can edit when toggle task button
SUPPORTED_NOTE_TYPES = (sg_note.Value.brief, sg_note.Value.note)
CLIENT_APPROVED_TYPE = sg_note.Value.client_approved_version
IGNORED_TASKS = ('anim_director', 'finalcam_sup', 'setdress_sup', 'sim_sup')  # tasks for supervisors who gives notes are ignored
INCLUDE_RELATED_STEPS = ['design']

# UI const
ATTACHMENT_THUMBNAIL_HEIGHT = 175  # height of attachemnts thumbnail
REPLAYBOX_HEIGHT = 70
REPLY_ATTACHMENT_HEIGHT = 24
TASK_ICON_SIZE = (60, 60)  # size of task icon
USER_PHOTO_SIZE = (50, 50)  # size of user photo
FEED_TYPE_ICON_SIZE = (40, 40)
LOAD_REPLY_ICON_SIZE = (24, 24)
NEW_ICON_SIZE = (26, 13)
VIEWER_SIZE = (1440, 900)  # size of image viewer
DATE_FORMAT = '%B %d'
DATE_FORMAT_YEAR = '%B %d, %Y'  # date format
DATETIME_FORMAT = '%d/%m/%y %H:%M'
TIME_FORMAT = '%H:%M'
NOT_AVAILABLE = 'N/A'  # global 'not available' text
NO_CONTENT = '< No comment >'
LOAD_THREAD_TEXT = 'Loading threads...'
REPLYBOX_PLACEHODLER_TEXT = 'Write a reply...'
SEARCHBAR_PLACEHOLDER = '< Search for jobs using keywords... >'
APPROVED_TEXT = 'Approved!'

# media const
VID_SIZE_MULT = 1.0
IMAGE_PER_ROW = 4
GRIDSCALE = (4096, 4096)
BORDER_WIDTH = 32
MEDIA_OFFSET = 75
DEFAULT_VID_SIZE = (1920, 1080)
DEFAULT_VID_FPS = 24
DEFAULT_VID_QUALITY = 1.0
AUTOPLAY_VID = True
SHOW_PLAY_BUTTON = True
CLEAR_MEDIA_CACHE = False

# temp const
USER_PHOTO_CACHE = {}
GLOBAL_TEMP = set()

# icons
APP_ICON = '{}/icons/app_icon.png'.format(moduleDir)
UNKNOWN_STATUS_ICON = '{}/icons/unknown_status_icon.png'.format(moduleDir)
FIRE_ICON = '{}/icons/fire_icon.png'.format(moduleDir)
SANDCLOCK_ICON = '{}/icons/sandclock_icon.png'.format(moduleDir)
SEARCH_ICON = '{}/icons/search_icon.png'.format(moduleDir)
REFRESH_ICON = '{}/icons/refresh_icon.png'.format(moduleDir)
PENCIL_ICON = '{}/icons/pencil_icon.png'.format(moduleDir)
PAPERCLIP_ICON = '{}/icons/paperclip_icon.png'.format(moduleDir)
PLAY_ICON = '{}/icons/play_icon.png'.format(moduleDir)
FOLDER_ICON = '{}/icons/folder_icon.png'.format(moduleDir)
ROOT_ICON = '{}/icons/root_icon.png'.format(moduleDir)
IMPORTANT_ICON = '{}/icons/important_icon.png'.format(moduleDir)
NEW_ICON = '{}/icons/new_icon.gif'.format(moduleDir)
NO_FEED_BG = '{}/icons/no_notes_bg.png'.format(moduleDir)
NO_JOB_BG = '{}/icons/no_jobs_bg.png'.format(moduleDir)
NO_MEDIA_BG = '{}/icons/no_media_bg.png'.format(moduleDir)
LOADING_ICON = '{}/icons/loading.gif'.format(moduleDir)
LOADING_SMALL_ICON = '{}/icons/loading60.gif'.format(moduleDir)
UNKNOWN_PERSON_ICON = '{}/icons/unknown_person.png'.format(moduleDir)
NOPREVIEW_ICON = '{}/icons/nopreview_icon.png'.format(moduleDir)
APPROVED_ICON = '{}/icons/approved_icon.png'.format(moduleDir)
WIP_ICON = '{}/icons/wip_icon.png'.format(moduleDir)
CLIENT_PROFILE = '{}/icons/client_profile.png'.format(moduleDir)

'''
TO DOs:
    //- cache system
    //- sort feed
    //- local link
    //- show image
    //- text display bug
    //- hold shift when open link to open dir
    //- splitter handle missing
    //- human-friendly date time displays
    //- animated viewer
    //- only display brief/note from entity level and user assigned tasks
    // display notes
    //- clickable version link
    //- Fix text display bug on html text and attachment widget
    //- Brief/Note adjustable status
    //- feed status UI
    //- image viewer next image button + arrow key 
    //- refresh current job's feed (right-click menu + double clicked)
    //- show background image for empty feed
    //- ignore "_sup" tasks
    //- support task involved_with
    //- loading gif on list widgets
    //- display reply
    //- fix stylesheets on feedTreeWidget with replies
    //- user photo on feed + photo cache
    //- update app to the new API
    //- display image name from SG instead of temp downloaded path
    //- ** loading gif when loading reply
    //- add reply box
    //- confirm add reply
    //- tooltips
    //- display published media from other dept
    //- cache media
    //- play button on media item
    //- status, new icons on media
    //- media_widget stylesheets, background
    //- right-click, double-click to open media
    //- show asset with at least 1 non-approved task
    //- wrong profile picture on client review user
    //- open design work dir
    //- prevent clicking on task button with approved status
    //- show client approved event?
    - modularized widgets
    - add notification
'''

class StyleConfig:
    # colors
    black = (0, 0, 0)
    white = (255, 255, 255)

    darker_gray = (30, 30, 30)
    dark_gray = (35, 35, 35)
    mid_lower_gray = (52, 52, 52)
    mid_gray = (68, 68, 68)
    upper_mid_gray = (80, 80, 80)
    lower_light_gray = (93, 93, 93)
    light_gray = (140, 140, 140)
    bright_gray = (190, 190, 190)
    
    dark_yellow = (112, 92, 63)
    yellow = (255, 220, 64)
    pale_yellow = (210, 160, 60)
    bright_yellow = (224, 173, 50)
    hilight_yellow = (235, 185, 42)

    dark_gray_green = (52, 59, 52)
    lower_gray_green = (34, 35, 34)
    gray_green = (40, 42, 41)
    upper_gray_green = (50, 52, 51)
    light_gray_green = (70, 77, 70)
    dark_green = (29, 30, 29)
    mid_green = (100, 175, 78)
    green = (110, 185, 88)
    hilight_green = (130, 205, 108)

    red = (240, 80, 80)
    bright_red = (240, 50, 50)

    bright_brown = (176, 127, 35)
    upper_mid_brown = (175, 150, 110)
    mid_brown = (145, 120, 80)
    dark_brown = (123, 103, 73)

    light_blue = (41, 150, 214)
    bright_blue = (61, 170, 234)

    # styles
    gray_reply_button_style = ''' QPushButton {{ background-color: rgb{dark_green}; color: rgb{bright_gray}; border-radius: 6px; border: none; }}
    QToolTip {{ background-color: rgb{bright_yellow}; color: black; }}
    QPushButton:hover {{ background-color: rgb{upper_mid_gray}; color: white;}} 
    QPushButton:pressed {{ background-color: rgb{light_gray}; color: white;}}
    QPushButton:checked {{ background-color: rgb{upper_mid_gray}; color: white; border: 1px solid white; }}
    '''.format(dark_green=dark_green, bright_yellow=bright_yellow, upper_mid_gray=upper_mid_gray, mid_gray=mid_gray, light_gray=light_gray, bright_gray=bright_gray)

    reply_button_style = ''' QPushButton {{ background-color: rgb{dark_green}; color: rgb{bright_gray}; border-radius: 6px; border: none; }}
    QPushButton:enabled:hover {{ background-color: rgb{bright_blue}; color: white;}} 
    QPushButton:enabled:pressed {{ background-color: rgb{bright_blue}; color: white; border: 1px solid white;}}
    QPushButton:enabled {{ background-color: rgb{light_blue}; color: white; }}
    QPushButton:!enabled {{ background-color: rgb{dark_green}; color: rgb{mid_gray}; }}
    '''.format(bright_blue=bright_blue, light_blue=light_blue, dark_green=dark_green, mid_gray=mid_gray, bright_gray=bright_gray)

    reply_text_style = ''' background-color: rgb{dark_green}; padding: 4px; border-width: 1px solid rgb{light_gray}; border-radius: 9px; '''.format(light_gray=light_gray, dark_green=dark_green)

    menu_style = 'QMenu {{ background-color: rgb{dark_gray}; color: white; padding: 4px; }}'.format(dark_gray=dark_gray)
    listwidget_item_style = 'background-color: rgba(0, 0, 0, 0);'
    icon_label_style = '''QToolTip {{ background-color: rgb{bright_yellow}; color: black; }}
    '''.format(bright_yellow=bright_yellow)
    title_text_style = 'color: white; font-size: 15px; font: bold;'
    
    searchbar_style = ''' QLineEdit {{ background-color: rgb{dark_green}; color: white; border: 0px; border-radius: 9px; }}
    QToolTip {{ background-color: rgb{bright_yellow}; color: black; }}'''.format(bright_yellow=bright_yellow, dark_green=dark_green)
    refresh_button_style = '''QPushButton {{ border-radius: 6px; }}
    QToolTip {{ background-color: rgb{bright_yellow}; color: black; }}
    '''.format(bright_yellow=bright_yellow)

    note_status_button_style = '''QToolButton {{ background-color: rgb{gray_green}; border: 1px rgba(0, 0, 0, 0); padding-right: 8px; border-radius: 6px; }}
    QToolButton:hover {{ background-color: rgb{upper_mid_gray}; color: white; }}
    QToolTip {{ background-color: rgb{bright_yellow}; color: black; }}
    '''.format(bright_yellow=bright_yellow, gray_green=gray_green, upper_mid_gray=upper_mid_gray)
    
    viewer_widget_style = '''QWidget {{ background-color: rgb{darker_gray}; border-width: 0px; border-style: solid; }}
    QToolTip {{ background-color: rgb{bright_yellow}; color: black; }}'''.format(darker_gray=darker_gray, bright_yellow=bright_yellow)
    viewer_font_style = '''background-color: rgba(0, 0, 0, 0); 
    border: 1px rgba(255, 255, 255, 128); border-radius: 9px; 
    color: rgba(255, 255, 255, 128); font-size: 18px;'''
    creator_label_style = '''QToolTip {{ background-color: rgb{bright_yellow}; color: black; }}
    QLabel {{ color: white; font: bold; font-size: 14px;}}'''.format(bright_yellow=bright_yellow, white=white)
    date_label_style = '''QToolTip {{ background-color: rgb{bright_yellow}; color: black; }}
    QLabel {{ color: rgb{bright_gray}; font-size: 13px;}}'''.format(bright_yellow=bright_yellow, bright_gray=bright_gray)

    # content text
    content_style = '''QMenu {{ background-color: rgb{mid_gray}; }}
    QLabel {{ color: rgb{white}; font-size: 15px; }}'''.format(mid_gray=mid_gray, white=white)
    no_content_style = '''QMenu {{ background-color: rgb{mid_gray}; }}
    QLabel {{ font: italic; color: rgb{light_gray}; font-size: 14px; }}'''.format(mid_gray=mid_gray, light_gray=light_gray)
    approved_content_style = '''QMenu {{ background-color: rgb{mid_gray}; }}
    QLabel {{ color: rgb{green}; font-size: 20px; font: bold;}}'''.format(mid_gray=mid_gray, green=green)
    
    status_toolbutton_off_style = '''QToolTip {{ background-color: rgb{bright_yellow}; color: black; }}
    QToolButton {{ background-color: rgb{dark_gray}; color: rgb{light_gray}; border-radius: 9px; }}
    QToolButton:hover {{ background-color: rgb{dark_yellow}; color: white; }}
    QToolButton::menu-indicator {{ image: none; }}'''.format(bright_yellow=bright_yellow, dark_yellow=dark_yellow, light_gray=light_gray, dark_gray=dark_gray)
    
    status_toolbutton_on_style = '''QToolTip {{ background-color: rgb{bright_yellow}; color: black; }}
    QToolButton {{ background-color: rgb{pale_yellow}; color: {bright_gray}; border-radius: 9px; }}
    QToolButton:hover {{ background-color: rgb{hilight_yellow}; color: white; }}
    QToolButton::menu-indicator {{ image: none; }}'''.format(pale_yellow=pale_yellow, hilight_yellow=hilight_yellow, bright_yellow=bright_yellow, bright_gray=bright_gray)
    
    status_toolbutton_complete_style = '''QToolTip {{ background-color: rgb{bright_yellow}; color: black; }}
    QToolButton {{ background-color: rgb{green}; color: {bright_gray}; border-radius: 9px; }}
    QToolButton:hover {{ background-color: rgb{hilight_green}; color: white; }}
    QToolButton::menu-indicator {{ image: none; }}'''.format(green=green, hilight_green=hilight_green, bright_yellow=bright_yellow, bright_gray=bright_gray)

    msgbox_style = ''' background-color: rgb{mid_gray}; '''.format(mid_gray=mid_gray)

    thumbnail_style = '''QListWidget {{ background-color: rgb{dark_green}; padding: 0px 8px 0px 16px; border-width: 0px; border-radius: 9px; }}
    QListWidget::item {{ background-color: rgb{dark_green}; border-width: 0px; border-radius: 0px; }}
    '''.format(dark_green=dark_green)

    attachment_style = '''QToolTip {{ background-color: rgb{bright_yellow}; color: black; }}
    QListWidget {{ background-color: rgb{dark_green}; border-width: 0px; border-radius: 6px; }}
    QListWidget::item {{ background-color: rgb{dark_green}; border-width: 0px; border-radius: 0px; }}
    '''.format(dark_green=dark_green, bright_yellow=bright_yellow, upper_mid_gray=upper_mid_gray)

    job_listWidget_style = '''QListWidget {{ background-color: rgb{dark_green}; padding: 6px; border-radius: 9px; }}
    QListWidget::item {{ background-color: rgb{mid_gray}; border-radius: 9px;}}
    QListView {{ outline: 0; }}
    QListWidget::item:selected {{ background-color: rgb{light_gray_green}; border: 1px solid rgb{mid_green}; border-radius: 9px; }}
    QListWidget::item:selected:hover {{ background-color: rgb{light_gray_green}; border: 1px solid rgb{mid_green}; border-radius: 9px; }}
    QListWidget::item:hover {{ background-color: rgb{upper_mid_gray}; border-radius: 9px;}}
    QToolTip {{ background-color: rgb{bright_yellow}; color: black; }}
    '''.format(bright_yellow=bright_yellow, light_gray_green=light_gray_green, upper_mid_gray=upper_mid_gray, mid_gray=mid_gray, mid_green=mid_green, dark_green=dark_green)

    media_widget_style = '''QListWidget {{ background-color: rgb{dark_green}; padding: 8px; 
    border-top-left-radius: 0px;
    border-top-right-radius: 9px;
    border-bottom-left-radius: 9px;
    border-bottom-right-radius: 9px; }}

    QListWidget::item {{ background-color: rgb{gray_green}; padding: 9px; border-radius: 9px; color: rgb{light_gray}; }}
    QToolTip {{ background-color: rgb{bright_yellow}; color: black; }}
    '''.format(bright_yellow=bright_yellow,  gray_green=gray_green, dark_green=dark_green, light_gray=light_gray)

    feed_widget_style = '''QTreeWidget {{ background-color: rgb{dark_green}; padding: 8px; 
    border-top-left-radius: 0px;
    border-top-right-radius: 9px;
    border-bottom-left-radius: 9px;
    border-bottom-right-radius: 9px; }}

    QTreeWidget::item {{ background-color: rgb{gray_green}; border-radius: 9px; margin: 9px; }}
    QTreeWidget {{ outline: 0; }}

    QTreeWidget::branch:has-siblings:adjoins-item {{ border-image: url({icon_root}/branch-more.png) 31 0 0 22; 
        border-top: 31px transparent;
        border-right: 0px transparent;
        border-bottom: 0px transparent;
        border-left: 22px transparent;}}
    QTreeWidget::branch:!has-siblings:!has-children:adjoins-item {{ border-image: url({icon_root}/branch-end.png) 31 0 0 22; 
        border-top: 31px transparent;
        border-right: 0px transparent;
        border-bottom: 0px transparent;
        border-left: 22px transparent;}}
    '''.format(icon_root=moduleDir+'/icons', dark_green=dark_green, gray_green=gray_green)

    tab_widget_style = ''' 
        QTabWidget::pane {{
          border: none;
          top:-0.01px; 
          background: rgb{dark_green}; 
        }}
        QTabBar::tab {{
          background: rgb{dark_green}; 
          border: 1px solid rgb{dark_green}; 
          padding-top: 8px;
          padding-bottom: 3px;
          padding-left: 18px;
          padding-right: 18px;
          margin-right: 2px;
          border-top-left-radius: 9px;
          border-top-right-radius: 9px;
        }} 
        QTabBar::tab:!selected {{
          margin-top: 4px; 
          background-color: rgb{mid_lower_gray};
          border: 0px solid rgb{mid_lower_gray}; 
          color: rgb{light_gray};
        }}'''.format(dark_green=dark_green, mid_lower_gray=mid_lower_gray, light_gray=light_gray)

# ----- Notes tab widgets
class JobItem(QtWidgets.QWidget):
    taskChanged = QtCore.Signal(tuple)
    refreshRequested = QtCore.Signal(bool)
    def __init__(self, data, parent=None):
        super(JobItem, self).__init__(parent)
        # ui vars
        self.data = data

        self.setStyleSheet(StyleConfig.listwidget_item_style)
        self.init_ui()
        self.init_signals()
        self.set_ui()

    def init_ui(self):
        # main layout
        self.main_layout = QtWidgets.QVBoxLayout()
        self.main_layout.setContentsMargins(8, 8, 8, 8)
        self.main_layout.setSpacing(6)
        self.setLayout(self.main_layout)

        # ----- top
        self.top_layout = QtWidgets.QHBoxLayout()
        self.main_layout.addLayout(self.top_layout)

        self.top_left_label = QtWidgets.QLabel()
        self.top_left_label.setAlignment(QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        self.top_layout.addWidget(self.top_left_label)

        self.top_right_label = QtWidgets.QLabel()
        self.top_right_label.setAlignment(QtCore.Qt.AlignTop | QtCore.Qt.AlignRight)
        self.top_layout.addWidget(self.top_right_label)

        # ----- mid
        self.mid_layout = QtWidgets.QGridLayout()
        self.mid_layout.setHorizontalSpacing(4)
        self.mid_layout.setContentsMargins(4, 4, 4, 4)
        self.main_layout.addLayout(self.mid_layout)

        self.status_label = status_widget.StatusLabel()
        self.status_label.setFixedSize(QtCore.QSize(16, 16))
        self.status_label.text_label.hide()
        self.mid_layout.addWidget(self.status_label, 0, 0)

        self.title_icon_label = QtWidgets.QLabel()
        self.title_icon_label.setStyleSheet(StyleConfig.icon_label_style)
        self.title_icon_label.setToolTip('Assigned to you')
        self.title_icon_label.setSizePolicy(QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum))
        self.mid_layout.addWidget(self.title_icon_label, 0, 1)
        self.title_icon_label.hide()

        self.title_label = QtWidgets.QLabel()
        self.title_label.setSizePolicy(QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum))
        self.mid_layout.addWidget(self.title_label, 0, 2)
  
        # ----- bottom
        self.bottom_layout = QtWidgets.QHBoxLayout()
        self.bottom_layout.setContentsMargins(0, 0, 0, 0)
        self.main_layout.addLayout(self.bottom_layout)

        # task layout
        self.task_layout = QtWidgets.QVBoxLayout()
        self.task_layout.setContentsMargins(22, 0, 0, 0)
        self.task_layout.setSpacing(6)
        self.bottom_layout.addLayout(self.task_layout)

        self.bottom_item2 = QtWidgets.QSpacerItem(16, 16, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.bottom_layout.addItem(self.bottom_item2)

        # icon 
        self.icon_label = QtWidgets.QLabel()
        self.icon_label.setFixedSize(QtCore.QSize(TASK_ICON_SIZE[0], TASK_ICON_SIZE[1]))
        self.bottom_layout.addWidget(self.icon_label, alignment=(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignTop))
    
        self.main_layout.setStretch(0, 0)
        self.main_layout.setStretch(1, 1)
        self.main_layout.setStretch(2, 0)

        # context menu
        self.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)

    def init_signals(self):
        self.customContextMenuRequested.connect(self.right_clicked)

    def right_clicked(self, pos):
        # create menu
        rightClickMenu = QtWidgets.QMenu(self)
        rightClickMenu.setStyleSheet(StyleConfig.menu_style)

        # refresh
        action = QtWidgets.QAction('Refresh', self)
        action.setIcon(QtGui.QIcon(REFRESH_ICON))
        action.triggered.connect(self.refresh_clicked)
        rightClickMenu.addAction(action)
        # print(self.data)
        rightClickMenu.addSeparator()
        entity_type = None
        if self.data['entity_type'] == 'Asset':
            action = QtWidgets.QAction('Design work' , self)
            action.setIcon(QtGui.QIcon(FOLDER_ICON))
            action.triggered.connect(self.open_design_dir)
            rightClickMenu.addAction(action)
            entity_type = 'asset'
        else:
            entity_type = 'scene'

        action = QtWidgets.QAction('Root directory', self)
        action.setIcon(QtGui.QIcon(ROOT_ICON))
        action.triggered.connect(partial(self.open_entity_dir, entity_type))
        rightClickMenu.addAction(action)

        # show menu
        rightClickMenu.exec_(self.mapToGlobal(pos))

    def open_design_dir(self):
        context = context_info.Context()
        context.update(project=self.data['project']['name'],
                    entityGrp=self.data['type'],
                    entityType='asset',
                    entity=self.data['name'])
        asset = context_info.ContextPathInfo(context=context)
        asset.context.update(entityType='design')
        work_dir = os.path.dirname(asset.path.scheme(key='workPath').abs_path())
        if os.path.exists(work_dir):
            Popen(r'explorer {}'.format(work_dir.replace('/', '\\')))

    def open_entity_dir(self, entity_type):
        context = context_info.Context()
        context.use_sg(sg, project=self.data['project']['name'], entityType=entity_type, entityName=self.data['entity']['name'])
        entity = context_info.ContextPathInfo(context=context)
        entity_dir = entity.path.name().abs_path()
        if os.path.exists(entity_dir):
            Popen(r'explorer {}'.format(entity_dir.replace('/', '\\')))

    def refresh_clicked(self):
        self.refreshRequested.emit(True)

    def set_ui(self):
        # set project and type
        self.top_left_label.setStyleSheet('color: rgb{};'.format(StyleConfig.bright_gray))
        proj = self.data.get('project')['name'] if self.data.get('project') else NOT_AVAILABLE
        typ = self.data.get('type') if self.data.get('type') else NOT_AVAILABLE
        self.top_left_label.setText('{}\n{}'.format(proj, typ))

        # set due date
        due_date = self.data['due_date']
        today = datetime.today().date()
        if not due_date:
            due_date = NOT_AVAILABLE
            self.top_right_label.setStyleSheet('color: rgb{}'.format(StyleConfig.bright_gray))
        else:
            time_delta = (due_date - today).days
            if time_delta < 0:  # late
                self.top_right_label.setStyleSheet('color: rgb{};'.format(StyleConfig.bright_red))
                # set icon
                self.icon_label.setPixmap(QtGui.QIcon(FIRE_ICON).pixmap(QtCore.QSize(TASK_ICON_SIZE[0], TASK_ICON_SIZE[1])))
            elif time_delta <= WARNING_DURATION:  # due soon
                self.top_right_label.setStyleSheet('color: rgb{};'.format(StyleConfig.bright_yellow))
                self.icon_label.setPixmap(QtGui.QIcon(SANDCLOCK_ICON).pixmap(QtCore.QSize(TASK_ICON_SIZE[0], TASK_ICON_SIZE[1])))
            else:
                self.top_right_label.setStyleSheet('color: rgb{};'.format(StyleConfig.green))

            # Skip year text display if it's the current year
            date_format = DATE_FORMAT if due_date.year == today.year else DATE_FORMAT_YEAR
            due_date = '{}\n{}'.format(due_date.strftime(date_format), pretty_date(time=due_date)).lstrip("0").replace(" 0", " ")
        self.top_right_label.setText(due_date)

        # set status
        self.status_label.set_status(self.data['status'])

        # set title name
        self.title_label.setStyleSheet(StyleConfig.title_text_style)
        self.title_label.setText(self.data['name'])

        # title icon
        if self.data['is_assigned']:
            self.title_icon_label.setPixmap(QtGui.QIcon(IMPORTANT_ICON).pixmap(QtCore.QSize(20, 20)))
            self.title_icon_label.show()

        # set tasks
        for i, task in enumerate(self.data['tasks']):
            # task status
            status_button = QtWidgets.QToolButton()
            status_button.setMinimumWidth(115)
            status_button.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
            status_button.setToolButtonStyle(QtCore.Qt.ToolButtonTextBesideIcon)
            status_button.setText(task.get('content', NOT_AVAILABLE))
            status_button.setArrowType(QtCore.Qt.NoArrow)

            # set icon
            status_value = task.get('sg_status_list')
            icon_data = status_widget.Icon.statusMap.get(status_value)
            icon_path = '{}/{}'.format(status_widget.Icon.path, icon_data.get('icon')) if icon_data else UNKNOWN_STATUS_ICON
            status_button.setIcon(QtGui.QIcon(icon_path))

            # set tooltips
            status_button_tooltips = []
            task_status = status_button_tooltips.append('Status: '+icon_data.get('display')) if icon_data.get('display') else None
            task_due = status_button_tooltips.append('Due date: '+datetime.strptime(task.get('due_date'), '%Y-%m-%d').strftime(DATE_FORMAT_YEAR)) if task.get('due_date') else None
            assignees = status_button_tooltips.append('Assigned: '+', '.join([a['name'] for a in task.get('task_assignees')])) if task.get('task_assignees') else None
            involved = status_button_tooltips.append('Involved: '+', '.join([a['name'] for a in task.get('sg_involve_with')])) if task.get('sg_involve_with') else None
            if status_button_tooltips:
                status_button.setToolTip('\n'.join(status_button_tooltips))

            self.task_layout.addWidget(status_button)

            # set checked status and style color
            if status_value == USER_EDITABLE_STATUS[True]:
                check_value = True
                status_button.setStyleSheet(StyleConfig.status_toolbutton_on_style)
            elif status_value == 'apr':
                status_button.setStyleSheet(StyleConfig.status_toolbutton_complete_style)
            else:
                check_value = False
                status_button.setStyleSheet(StyleConfig.status_toolbutton_off_style)
            
            # approved task can't be toggled to in progress
            if status_value != 'apr':
                status_button.setCheckable(True)
                status_button.blockSignals(True)
                status_button.setChecked(check_value)
                status_button.blockSignals(False)
                # connect signal
                status_button.toggled.connect(partial(self.set_task_status, task, status_button))

        task_spacer = QtWidgets.QSpacerItem(16, 16, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.task_layout.addItem(task_spacer)

    def set_task_status(self, task, button, value):
        content_name = '{} - {}'.format(task.get('entity')['name'], task.get('content'))
        if value:
            text = 'Start working on {}?'.format(content_name)
            icon = QtWidgets.QMessageBox.Question
        else:
            text = 'Continue later on {}?'.format(content_name)
            icon = QtWidgets.QMessageBox.Warning
        qmsgBox = QtWidgets.QMessageBox(self)
        qmsgBox.setStyleSheet(StyleConfig.msgbox_style)
        qmsgBox.setText(text)
        qmsgBox.setWindowTitle('Confirm')
        yes_button = qmsgBox.addButton('  Yes  ', QtWidgets.QMessageBox.AcceptRole)
        yes_button.setStyleSheet('border-radius: 6px;')
        no_button = qmsgBox.addButton('  No  ', QtWidgets.QMessageBox.RejectRole)
        no_button.setStyleSheet('border-radius: 6px;')
        qmsgBox.setIcon(icon)
        result = qmsgBox.exec_()
        if result == 0:
            self.taskChanged.emit((task, USER_EDITABLE_STATUS[value]))
        else:
            button.blockSignals(True)
            button.setChecked(not(value))
            button.blockSignals(False)

class JobListWidget(QtWidgets.QListWidget):
    ''' ListWidget for jobs '''
    taskChanged = QtCore.Signal(tuple)  # task, status, item
    refreshRequested = QtCore.Signal(bool)
    def __init__(self, parent=None):
        super(JobListWidget, self).__init__(parent)
        # backgrounds
        self.bg_pixmap = QtGui.QPixmap(NO_JOB_BG)
        self.loading_movie = QtGui.QMovie(LOADING_ICON)
        self.loading_movie.frameChanged.connect(self.update)

        self.init_ui()

    def set_loading(self, value):
        if value:
            self.loading_movie.start() 
        else:
            self.loading_movie.stop() 
            self.update()

    def is_loading(self):
        return True if self.loading_movie.state() == QtGui.QMovie.Running else False

    def paintEvent(self, event):
        if self.is_loading():
            movie_frame = self.loading_movie.currentPixmap()
            self.draw_background(pixmap=movie_frame)
        else:
            if not self.count():
                self.draw_background(pixmap=self.bg_pixmap)

        super(JobListWidget, self).paintEvent(event)

    def draw_background(self, pixmap):
        # draw background
        painter = QtGui.QPainter(self.viewport())
        winSize = self.size()
        pixmapRatio = (self.bg_pixmap.width() / self.bg_pixmap.height()) 
        windowRatio = winSize.width() / winSize.height()
        if pixmapRatio > windowRatio:
            newWidth = (winSize.height() * pixmapRatio) 
            offset = (newWidth - winSize.width()) / -2
            painter.drawPixmap(offset, 0, newWidth, winSize.height(), pixmap)
        else:
            newHeight = (winSize.width() / pixmapRatio) 
            offset = (newHeight - winSize.height()) / -2
            painter.drawPixmap(0, offset, winSize.width(), newHeight, pixmap)

    def init_ui(self):
        self.setSpacing(6)
        self.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.setVerticalScrollMode(QtWidgets.QAbstractItemView.ScrollPerPixel)
        self.verticalScrollBar().setSingleStep(16)
        self.setStyleSheet(StyleConfig.job_listWidget_style)

    def create_widget(self, item, data):
        itemWidget = JobItem(data=data)
        itemWidget.taskChanged.connect(partial(self.set_task_status, item))
        itemWidget.refreshRequested.connect(self.refresh_clicked)
        # set size
        itemSize = itemWidget.sizeHint()
        widgetSize = self.sizeHint()
        item.setSizeHint(QtCore.QSize(widgetSize.width(), itemSize.height()))
        return itemWidget

    def create_item(self, data):
        # create item
        item = QtWidgets.QListWidgetItem()
        itemWidget = self.create_widget(item, data)
        return item, itemWidget

    def update_entity(self, item, data):
        itemWidget = self.create_widget(item, data)
        self.setItemWidget(item, itemWidget)
        return item, itemWidget

    def refresh_clicked(self):
        self.refreshRequested.emit(True)

    def set_task_status(self, item, task_status):
        task, status = task_status
        self.taskChanged.emit((task, status, item))

    def add_entity(self, data):
        item, itemWidget = self.create_item(data)
        # set item tooltips
        tooltips = []
        if data.get('due_date'):
            tooltips.append('Due date: {}'.format(data.get('due_date').strftime(DATE_FORMAT_YEAR)))
        if data.get('updated_at'):
            tooltips.append('Updated date: {}'.format(data.get('updated_at').strftime(DATE_FORMAT_YEAR)))
        if data.get('used_in'):
            tooltips.append('Used in:\n    {}'.format('\n    '.join([e.get('name') for e in data['used_in']])))
        if data.get('tags'):
            tags = 'Tags: {}'.format(', '.join([t.get('name') for t in data['tags']]))
            tooltips.append(tags)
        if tooltips:
            item.setToolTip('\n'.join(tooltips))

        # set item data
        item.setData(QtCore.Qt.UserRole, data.get('entity'))

        self.addItem(item)
        self.setItemWidget(item, itemWidget)
        QtWidgets.QApplication.processEvents()
        return item, itemWidget

class MediaListWidget(media_widget.MediaGridViewer):
    def __init__(self, fps=DEFAULT_VID_FPS, quality=DEFAULT_VID_QUALITY, parent=None):
        super(MediaListWidget, self).__init__(fps, quality, parent)
        # backgrounds
        self.bg_pixmap = QtGui.QPixmap(NO_MEDIA_BG)
        self.loading_movie = QtGui.QMovie(LOADING_ICON)
        self.loading_movie.frameChanged.connect(self.update)

    def set_loading(self, value):
        if value:
            self.loading_movie.start() 
        else:
            self.loading_movie.stop() 
            self.update()

    def is_loading(self):
        return True if self.loading_movie.state() == QtGui.QMovie.Running else False

    def paintEvent(self, event):
        if self.is_loading():
            movie_frame = self.loading_movie.currentPixmap()
            self.draw_background(pixmap=movie_frame)
        else:
            if not self.count():
                self.draw_background(pixmap=self.bg_pixmap)

        super(MediaListWidget, self).paintEvent(event)

    def draw_background(self, pixmap):
        # draw background
        painter = QtGui.QPainter(self.viewport())
        winSize = self.size()
        pixmapRatio = (self.bg_pixmap.width() / self.bg_pixmap.height()) 
        windowRatio = winSize.width() / winSize.height()
        if pixmapRatio > windowRatio:
            newWidth = (winSize.height() * pixmapRatio) 
            offset = (newWidth - winSize.width()) / -2
            painter.drawPixmap(offset, 0, newWidth, winSize.height(), pixmap)
        else:
            newHeight = (winSize.width() / pixmapRatio) 
            offset = (newHeight - winSize.height()) / -2
            painter.drawPixmap(0, offset, winSize.width(), newHeight, pixmap)

class FeedItem(QtWidgets.QWidget):
    loadAttachemntFinished = QtCore.Signal(tuple)  # (note_id, image)
    statusChanged = QtCore.Signal(tuple)  # (note_id, status)
    replyChecked = QtCore.Signal(dict)  # note_entity
    replyUnchecked = QtCore.Signal(dict)  # note_entity
    def __init__(self, data, sg_client=None, parent=None):
        super(FeedItem, self).__init__(parent)
        # ui vars
        self.data = data
        self.sg_client = sg_client if sg_client else ShotgunClient()
        self.threadpool = QtCore.QThreadPool()
        self.threadpool.setMaxThreadCount(8)

        self.init_ui()
        self.set_styles()
        self.init_signals()
        self.set_ui()

    def init_ui(self):
        # main layout
        self.main_layout = QtWidgets.QVBoxLayout()
        self.main_layout.setContentsMargins(12, 12, 12, 12)
        self.main_layout.setSpacing(9)
        self.setLayout(self.main_layout)

        self.top_layout = QtWidgets.QHBoxLayout()
        self.top_layout.setSpacing(4)
        self.main_layout.addLayout(self.top_layout)

        self.title_layout = QtWidgets.QHBoxLayout()
        self.title_layout.setSpacing(6)
        self.top_layout.addLayout(self.title_layout)

        # photo
        self.photo_label = display_widget.PhotoFrame()
        self.photo_label.circular = True
        self.photo_label.setFixedSize(QtCore.QSize(FEED_TYPE_ICON_SIZE[0], FEED_TYPE_ICON_SIZE[1]))
        self.photo_label.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
        self.title_layout.addWidget(self.photo_label)

        # creator date
        self.creator_date_layout = QtWidgets.QVBoxLayout()
        self.creator_date_layout.setSpacing(0)
        self.title_layout.addLayout(self.creator_date_layout)

        # creator
        self.creator_label = QtWidgets.QLabel()
        self.creator_label.setFixedHeight(20)
        self.creator_label.setAlignment(QtCore.Qt.AlignLeft)
        self.creator_date_layout.addWidget(self.creator_label)
        # date
        self.date_layout = QtWidgets.QHBoxLayout()
        self.date_layout.setContentsMargins(0, 0, 0, 0)
        self.date_layout.setSpacing(6)
        self.creator_date_layout.addLayout(self.date_layout)
        self.date_label = QtWidgets.QLabel()
        self.date_label.setFixedHeight(20)
        self.date_label.setAlignment(QtCore.Qt.AlignLeft)
        self.date_layout.addWidget(self.date_label)

        # date icon
        self.date_icon_label = QtWidgets.QLabel()
        self.date_icon_label.setFixedHeight(20)
        self.date_icon_label.setAlignment(QtCore.Qt.AlignVCenter | QtCore.Qt.AlignHCenter)
        self.date_layout.addWidget(self.date_icon_label)

        spacer1 = QtWidgets.QSpacerItem(16, 16, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.top_layout.addItem(spacer1)

        # note status
        self.status_button = QtWidgets.QToolButton()
        self.status_button.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.status_button.setCheckable(False)
        self.status_button.setMaximumHeight(18)
        self.status_button.setIconSize(QtCore.QSize(16, 16))
        self.status_button.setToolButtonStyle(QtCore.Qt.ToolButtonTextBesideIcon)
        self.status_button.setArrowType(QtCore.Qt.NoArrow)
        self.top_layout.addWidget(self.status_button, alignment=(QtCore.Qt.AlignTop | QtCore.Qt.AlignHCenter))

        # ----- body
        self.body_layout = QtWidgets.QVBoxLayout()
        self.body_layout.setContentsMargins(16, 8, 4, 4)
        self.main_layout.addLayout(self.body_layout)

        # content
        self.content_label = text_label.HtmlLabel()
        self.content_label.setWordWrap(True)
        # self.content_label.setMargin(12)
        self.content_label.setMinimumHeight(40)
        self.content_label.setSizePolicy(QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding))
        self.body_layout.addWidget(self.content_label)

        # ----- attachments
        self.attachment_layout = QtWidgets.QVBoxLayout()
        self.attachment_layout.setContentsMargins(0, 0, 0, 0)
        self.main_layout.addLayout(self.attachment_layout)

        self.attachment_widget = filmstrip_widget.DisplayReel(always_reload=True)
        self.attachment_widget.allow_delete = False
        self.attachment_widget.allow_paste = False
        self.attachment_widget.displayListWidget.setSpacing(8)
        self.attachment_widget.displayListWidget.setFlow(QtWidgets.QListView.LeftToRight)
        self.attachment_widget.displayListWidget.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)

        self.attachment_widget.setMaximumHeight(ATTACHMENT_THUMBNAIL_HEIGHT)
        self.attachment_widget.displayListWidget.setMaximumHeight(ATTACHMENT_THUMBNAIL_HEIGHT)
        self.attachment_widget.setSizePolicy(QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Maximum))
        self.attachment_widget.displayListWidget.setSizePolicy(QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Maximum))
        self.attachment_layout.addWidget(self.attachment_widget)

        self.bottom_layout = QtWidgets.QHBoxLayout()
        self.bottom_layout.setContentsMargins(0, 0, 0, 0)
        self.main_layout.addLayout(self.bottom_layout)

        # details
        self.detail_layout = QtWidgets.QFormLayout()
        self.detail_layout.setContentsMargins(0, 8, 0, 0)
        self.detail_layout.setLabelAlignment(QtCore.Qt.AlignLeft)
        self.detail_layout.setHorizontalSpacing(4)
        self.bottom_layout.addLayout(self.detail_layout, alignment=(QtCore.Qt.AlignBottom | QtCore.Qt.AlignLeft))

        # links
        self.link_label = text_label.HtmlLabel()
        self.link_label.setWordWrap(True)
        self.link_label.setMinimumHeight(20)
        self.link_label.setAlignment(QtCore.Qt.AlignVCenter | QtCore.Qt.AlignLeft)
        self.link_label.setSizePolicy(QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum))
        self.detail_layout.addRow('Links:', self.link_label)

        # reply button
        self.reply_button = QtWidgets.QPushButton()
        self.reply_button.setCheckable(True)
        self.reply_button.setIcon(QtGui.QIcon(PENCIL_ICON))
        self.reply_button.setFixedSize(QtCore.QSize(70, REPLY_ATTACHMENT_HEIGHT))
        self.reply_button.setSizePolicy(QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed))
        self.reply_button.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.bottom_layout.addWidget(self.reply_button, alignment=(QtCore.Qt.AlignBottom | QtCore.Qt.AlignRight))

        self.main_layout.setStretch(0, 0)
        self.main_layout.setStretch(1, 1)
        self.main_layout.setStretch(2, 0)
        self.main_layout.setStretch(3, 0)

        # ----- note status menu
        self.status_menu = QtWidgets.QMenu(self)
        self.status_button.setMenu(self.status_menu)
        for status in NOTE_STATUS:
            icon_data = status_widget.Icon.statusMap.get(status)
            icon_path = '{}/{}'.format(status_widget.Icon.path, icon_data.get('icon'))
            note_status = icon_data.get('display')

            action = QtWidgets.QAction(note_status, self)
            action.setData(status)
            action.setIcon(QtGui.QIcon(icon_path))
            action.setCheckable(False)

            action.triggered.connect(partial(self.status_changed, status))
            self.status_menu.addAction(action)

        # tooltips
        self.reply_button.setToolTip('Write a reply to this note')
        self.status_button.setToolTip('Change note status')

    def set_styles(self):
        self.setStyleSheet(StyleConfig.listwidget_item_style)
        self.photo_label.setStyleSheet(StyleConfig.creator_label_style)
        self.creator_label.setStyleSheet(StyleConfig.creator_label_style)
        self.date_label.setStyleSheet(StyleConfig.date_label_style)
        self.status_button.setStyleSheet(StyleConfig.note_status_button_style)
        self.attachment_widget.setStyleSheet(StyleConfig.thumbnail_style)
        self.link_label.setStyleSheet('color: rgb{}; font: italic;'.format(StyleConfig.light_gray))
        label = self.detail_layout.labelForField(self.link_label)
        label.setStyleSheet('color: rgb{}; font: italic;'.format(StyleConfig.light_gray))
        self.reply_button.setStyleSheet(StyleConfig.gray_reply_button_style)
        self.status_menu.setStyleSheet(StyleConfig.menu_style)

    def init_signals(self):
        self.attachment_widget.clicked.connect(self.image_clicked)
        self.status_button.clicked.connect(self.show_status_menu)
        self.reply_button.toggled.connect(self.reply_clicked)

    def reply_clicked(self, value):
        if value:
            self.reply_button.setToolTip('Cancel writing reply')
            self.replyChecked.emit(self.data)
        else:
            self.reply_button.setToolTip('Write a reply to this note')
            self.replyUnchecked.emit(self.data)

    def show_status_menu(self):
        self.status_button.showMenu()

    def status_changed(self, status):
        note_type = self.data['note_type']
        note_status = status_widget.Icon.statusMap[status]['display']
        text = 'Change {} status to: {}?'.format(note_type, note_status)
        icon = QtWidgets.QMessageBox.Question

        qmsgBox = QtWidgets.QMessageBox(self)
        qmsgBox.setStyleSheet(StyleConfig.msgbox_style)
        qmsgBox.setText(text)
        qmsgBox.setWindowTitle('Confirm')
        yes_button = qmsgBox.addButton('  Yes  ', QtWidgets.QMessageBox.AcceptRole)
        yes_button.setStyleSheet('border-radius: 6px;')
        no_button = qmsgBox.addButton('  No  ', QtWidgets.QMessageBox.RejectRole)
        no_button.setStyleSheet('border-radius: 6px;')
        qmsgBox.setIcon(icon)
        result = qmsgBox.exec_()
        if result == 0:
            note_id = self.data.get('id')
            self.statusChanged.emit((note_id, status))
        else:
            self.status_button.blockSignals(True)
            old_status = self.data.get('sg_status_list')
            menu = self.status_button.menu()
            for action in menu.actions():
                if action.data() == old_status:
                    menu.setActiveAction(action)
                    break
            self.status_button.blockSignals(False)

    def set_status_button(self):
        note_status = self.data.get('sg_status_list')
        icon_data = status_widget.Icon.statusMap.get(note_status)
        icon_path = '{}/{}'.format(status_widget.Icon.path, icon_data.get('icon')) if icon_data else UNKNOWN_STATUS_ICON
        note_display_status = icon_data.get('display') if icon_data else NOT_AVAILABLE
        self.status_button.setText(note_display_status)
        self.status_button.setIcon(QtGui.QIcon(icon_path))

    def set_feed_photo(self):
        creator =  self.data.get('user')
        photo_path = get_photo_path(user=creator) if creator else UNKNOWN_PERSON_ICON
        # set feed photo
        self.photo_label.update_photo(photo_path)

        # set photo tooltip
        typ = self.data.get('sg_note_type') if self.data.get('sg_note_type') else NOT_AVAILABLE
        subject = self.data.get('subject') if self.data.get('subject') else NOT_AVAILABLE
        photo_tooltip = 'Type: {}\nSubject: {}'.format(typ, subject)
        self.photo_label.setToolTip(photo_tooltip)

    def set_date(self):
        date_value = self.data.get('created_at')
        if date_value:
            date_text = 'Created: {}'.format(date_value.strftime(DATETIME_FORMAT))
            display_duration = pretty_date(time=date_value, limit_date=True)
            # new icon
            if (datetime.today().date() - date_value.date()).days <= NEW_ITEM_DAYS:
                movie = QtGui.QMovie(NEW_ICON)
                movie.setScaledSize(QtCore.QSize(NEW_ICON_SIZE[0], NEW_ICON_SIZE[1]))
                self.date_icon_label.setMovie(movie)
                movie.start()
            else:
                self.date_icon_label.hide()
        else:
            date_text = 'Created: {}'.format(NOT_AVAILABLE)
            display_duration = NOT_AVAILABLE
          
        self.date_label.setText(display_duration)
        self.date_label.setToolTip(date_text)

    def set_links(self):
        link_displays = ['entity']
        task_link = self.data.get('tasks') if self.data.get('tasks') else [] # assigned tasks
        if task_link:
            link_displays += list(set([t.get('name') for t in task_link]))

        # version links
        note_links = self.data.get('note_links') if self.data.get('note_links') else []  # version
        version_links = {}
        if note_links:
            for link in note_links:
                if link.get('type') == 'Version':
                    link_name = link.get('code')
                    link_displays.append(link_name)
                    version_links[link_name] = (self.version_clicked, link)

        link_text = ', '.join(link_displays)
        self.link_label.setText(link_text, custom_links=version_links)

    def set_creator(self):
        creator = self.data.get('user')['name'] if self.data.get('user') else NOT_AVAILABLE
        self.creator_label.setText(creator)

    def set_content(self):
        if self.data['content'] == None:
            self.content_label.setTextFormat(QtCore.Qt.PlainText)
            self.content_label.setMaximumHeight(20)
            super(text_label.HtmlLabel, self.content_label).setText(NO_CONTENT)
            content_style = StyleConfig.no_content_style
        else:
            self.content_label.setTextFormat(QtCore.Qt.RichText)
            self.content_label.setText(self.data['content'])
            content_style = StyleConfig.content_style
        self.content_label.setStyleSheet(content_style)

    def set_ui(self):
        # status
        self.set_status_button()

        # circle label
        self.set_feed_photo()

        # links
        self.set_links()

        # creator
        self.set_creator()

        # date
        self.set_date()

        # content
        self.set_content()

        # attachments
        self.show_attachments()

    def show_attachments(self):
        attachments = self.data.get('attachments')
        self.attachment_widget.clear()
        if attachments:
            self.attachment_widget.show()
            cache_data = self.data.get('downloaded')
            cached_attr_id =[]
            if cache_data:
                for att, image in cache_data:
                    att_id = att.get('id')
                    cached_attr_id.append(att_id)
                    self.attachment_widget.add_item(path=image, data={'name': att['name'], 'filepath': image})

            downloads = []
            for att in attachments:
                # check if already display using cache
                att_id = att.get('id')
                if att_id in cached_attr_id:
                    continue
                
                # create temp file
                fn, ext = os.path.splitext(att.get('name'))
                if ext.lower() in ATTACHMENT_FORMATS:
                    fh, temp = tempfile.mkstemp(suffix=ext)
                    GLOBAL_TEMP.add(temp)
                    os.close(fh)
                    downloads.append((att, temp.replace('\\', '/')))

            if downloads:
                # add proxy item
                for att, temp in downloads:
                    # add loading UI item to attachment widget
                    self.attachment_widget.add_proxy_item(path=temp)

                    # start downloading on new thread
                    worker = thread_pool.Worker(self.download_attachment, att, temp)
                    worker.signals.result.connect(self.download_attatchments_finished)
                    self.threadpool.start(worker)
        else:
            self.attachment_widget.hide()
        
    def version_clicked(self, *args):
        version = args[0]
        path = version.get('sg_path_to_movie')
        if path:
            dirname = os.path.dirname(path)
            if os.path.exists(dirname):
                Popen(r'explorer {}'.format(dirname.replace('/', '\\')))

    def download_attachment(self, attachment, destination):
        sg_intance = self.sg_client.clone().instance
        image = sg_intance.download_attachment(attachment, file_path=destination)
        return attachment, image

    def image_clicked(self, path):
        item_data = self.attachment_widget.get_all_data()
        paths = OrderedDict([(d['filepath'], d['name']) for d in item_data])  # {temp downloaded path: display name}
        img_dialog = image_widget.ImageDialog(path=path, all_paths=paths, view_size=VIEWER_SIZE, animated=True)
        img_dialog.setStyleSheet(StyleConfig.viewer_widget_style)
        img_dialog.close_button.setStyleSheet(StyleConfig.viewer_font_style)
        img_dialog.view_widget.setStyleSheet(StyleConfig.viewer_widget_style)
        img_dialog.file_label.setStyleSheet(StyleConfig.viewer_font_style)
        img_dialog.left_arrow.setStyleSheet(StyleConfig.viewer_font_style)
        img_dialog.right_arrow.setStyleSheet(StyleConfig.viewer_font_style)
        img_dialog.exec_()

    def download_attatchments_finished(self, results):
        attachment, image = results
        bid = self.data.get('id')
        try:
            self.attachment_widget.add_item(path=image, data={'name': attachment['name'], 'filepath': image})
            # emit signal
        except RuntimeError as e:
            pass
        self.loadAttachemntFinished.emit((bid, attachment, image))

class NoteItem(FeedItem):
    def __init__(self, data, sg_client=None, parent=None):
        super(NoteItem, self).__init__(data, sg_client, parent)
        self.setup_loading_layout()

    def setup_loading_layout(self):
        self.load_widget = QtWidgets.QWidget()
        self.load_layout = QtWidgets.QHBoxLayout(self.load_widget)
        self.load_layout.setSpacing(3)
        self.load_layout.setContentsMargins(0, 4, 0, 0)
        
        spacer1 = QtWidgets.QSpacerItem(16, 16, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
        self.load_layout.addItem(spacer1)

        self.text_label = text_label.BlinkLabel(text=LOAD_THREAD_TEXT, pattern=[[0,15], [0,16], [0,17], [0,18]], interval=170)
        self.text_label.setStyleSheet('color: rgb{}; font: italic;'.format(StyleConfig.lower_light_gray))
        self.load_layout.addWidget(self.text_label, alignment=(QtCore.Qt.AlignHCenter|QtCore.Qt.AlignVCenter))

        spacer2 = QtWidgets.QSpacerItem(16, 16, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.load_layout.addItem(spacer2)

        self.main_layout.addWidget(self.load_widget)
        self.main_layout.setStretch(3, 0)
        self.load_widget.hide()

    def show_loading_thread(self):
        self.text_label.start()
        self.load_widget.show()

    def hide_loading_thread(self):
        self.text_label.stop()
        self.load_widget.hide()

class ReplyItem(FeedItem):
    def __init__(self, data, sg_client=None, parent=None):
        super(ReplyItem, self).__init__(data, sg_client, parent)

    def init_ui(self):
        # main layout
        self.main_layout = QtWidgets.QVBoxLayout()
        self.main_layout.setContentsMargins(12, 12, 12, 12)
        self.main_layout.setSpacing(4)
        self.setLayout(self.main_layout)

        self.top_layout = QtWidgets.QHBoxLayout()
        self.top_layout.setSpacing(4)
        self.main_layout.addLayout(self.top_layout)

        self.title_layout = QtWidgets.QHBoxLayout()
        self.title_layout.setSpacing(6)
        self.top_layout.addLayout(self.title_layout)

        # type
        self.photo_label = display_widget.PhotoFrame()
        self.photo_label.circular = True
        self.photo_label.setFixedSize(QtCore.QSize(FEED_TYPE_ICON_SIZE[0], FEED_TYPE_ICON_SIZE[1]))
        self.photo_label.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
        self.title_layout.addWidget(self.photo_label)

        # creator date
        self.creator_date_layout = QtWidgets.QVBoxLayout()
        self.creator_date_layout.setSpacing(0)
        self.title_layout.addLayout(self.creator_date_layout)

        # creator
        self.creator_label = QtWidgets.QLabel()
        self.creator_label.setFixedHeight(20)
        self.creator_label.setAlignment(QtCore.Qt.AlignLeft)
        self.creator_date_layout.addWidget(self.creator_label)
        # date
        self.date_layout = QtWidgets.QHBoxLayout()
        self.date_layout.setContentsMargins(0, 0, 0, 0)
        self.date_layout.setSpacing(6)
        self.creator_date_layout.addLayout(self.date_layout)
        self.date_label = QtWidgets.QLabel()
        self.date_label.setFixedHeight(20)
        self.date_label.setAlignment(QtCore.Qt.AlignLeft)
        self.date_layout.addWidget(self.date_label)

        # date icon
        self.date_icon_label = QtWidgets.QLabel()
        self.date_icon_label.setFixedHeight(20)
        self.date_icon_label.setAlignment(QtCore.Qt.AlignVCenter | QtCore.Qt.AlignHCenter)
        self.date_layout.addWidget(self.date_icon_label)

        spacer1 = QtWidgets.QSpacerItem(16, 16, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.top_layout.addItem(spacer1)

        # ----- body
        self.body_layout = QtWidgets.QVBoxLayout()
        self.body_layout.setContentsMargins(16, 8, 4, 4)
        self.main_layout.addLayout(self.body_layout)

        # content
        self.content_label = text_label.HtmlLabel()
        self.content_label.setWordWrap(True)
        # self.content_label.setMargin(12)
        self.content_label.setMinimumHeight(40)
        self.content_label.setSizePolicy(QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding))
        self.body_layout.addWidget(self.content_label)

        # ----- attachments
        self.attachment_layout = QtWidgets.QVBoxLayout()
        self.attachment_layout.setContentsMargins(0, 0, 0, 0)
        self.main_layout.addLayout(self.attachment_layout)

        self.attachment_widget = filmstrip_widget.DisplayReel(always_reload=True)
        self.attachment_widget.allow_delete = False
        self.attachment_widget.allow_paste = False
        self.attachment_widget.displayListWidget.setSpacing(8)
        self.attachment_widget.displayListWidget.setFlow(QtWidgets.QListView.LeftToRight)
        self.attachment_widget.displayListWidget.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)

        self.attachment_widget.setMaximumHeight(ATTACHMENT_THUMBNAIL_HEIGHT)
        self.attachment_widget.displayListWidget.setMaximumHeight(ATTACHMENT_THUMBNAIL_HEIGHT)
        self.attachment_widget.setSizePolicy(QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Maximum))
        self.attachment_widget.displayListWidget.setSizePolicy(QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Maximum))
        self.attachment_layout.addWidget(self.attachment_widget)

        self.main_layout.setStretch(0, 0)
        self.main_layout.setStretch(1, 1)
        self.main_layout.setStretch(2, 0)

    def init_signals(self):
        self.attachment_widget.clicked.connect(self.image_clicked)

    def set_styles(self):
        self.setStyleSheet(StyleConfig.listwidget_item_style)
        self.photo_label.setStyleSheet(StyleConfig.creator_label_style)
        self.creator_label.setStyleSheet(StyleConfig.creator_label_style)
        self.date_label.setStyleSheet(StyleConfig.date_label_style)
        self.attachment_widget.setStyleSheet(StyleConfig.thumbnail_style)

    def set_ui(self):
        # circle label
        self.set_feed_photo()

        # creator
        self.set_creator()

        # date
        self.set_date()

        # content
        self.set_content()

        # attachements
        self.show_attachments()

class ReplyBox(QtWidgets.QWidget):
    cancelClicked = QtCore.Signal(dict)
    replyClicked = QtCore.Signal(tuple)  # (note_entity, content, attachments)

    def __init__(self, note_entity, user, sg_client=None, parent=None):
        super(ReplyBox, self).__init__(parent)
        self.note_entity = note_entity
        self.user = user

        self.init_ui()
        self.set_styles()
        self.init_signals()
        self.set_ui()

    def init_ui(self):
        # main layout
        self.main_layout = QtWidgets.QVBoxLayout()
        self.main_layout.setContentsMargins(12, 12, 12, 12)
        self.main_layout.setSpacing(8)
        self.setLayout(self.main_layout)

        self.mid_layout = QtWidgets.QHBoxLayout()
        self.mid_layout.setContentsMargins(0, 0, 0, 0)
        self.mid_layout.setSpacing(16)
        self.main_layout.addLayout(self.mid_layout)

        # photo
        self.photo_label = display_widget.PhotoFrame()
        self.photo_label.circular = True
        self.photo_label.setFixedSize(QtCore.QSize(FEED_TYPE_ICON_SIZE[0], FEED_TYPE_ICON_SIZE[1]))
        self.photo_label.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
        self.mid_layout.addWidget(self.photo_label)

         # reply content
        self.content_textEdit = textEdit.TextEditWithPlaceHolder()
        self.content_textEdit.setMaximumHeight(REPLAYBOX_HEIGHT)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        self.content_textEdit.setSizePolicy(sizePolicy)
        self.content_textEdit.setPlaceholderText(REPLYBOX_PLACEHODLER_TEXT)
        self.content_textEdit.setWordWrapMode(QtGui.QTextOption.WrapAtWordBoundaryOrAnywhere)
        self.mid_layout.addWidget(self.content_textEdit)

        self.button_layout = QtWidgets.QHBoxLayout()
        self.button_layout.setContentsMargins(0, 0, 0, 0)
        self.button_layout.setSpacing(6)
        self.main_layout.addLayout(self.button_layout)

        spacer1 = QtWidgets.QSpacerItem(FEED_TYPE_ICON_SIZE[0]+16, 16, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        self.button_layout.addItem(spacer1)
        
        # attachments
        self.attachment_widget = filmstrip_widget.DisplayReel()
        self.attachment_widget.allow_paste = True
        self.attachment_widget.setFixedHeight(REPLY_ATTACHMENT_HEIGHT)
        self.attachment_widget.setSizePolicy(QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed))
        self.attachment_widget.displayListWidget.setFlow(QtWidgets.QListView.LeftToRight)
        self.attachment_widget.displayListWidget.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.button_layout.addWidget(self.attachment_widget)

        # browse image button
        self.attach_image_button = QtWidgets.QPushButton()
        self.attach_image_button.setFixedSize(QtCore.QSize(22, REPLY_ATTACHMENT_HEIGHT))
        self.attach_image_button.setIcon(QtGui.QIcon(PAPERCLIP_ICON))
        self.button_layout.addWidget(self.attach_image_button)
        
        # cancel button
        self.cancel_button = QtWidgets.QPushButton('Cancel')
        self.cancel_button.setFixedSize(QtCore.QSize(55, REPLY_ATTACHMENT_HEIGHT))
        self.cancel_button.setSizePolicy(QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed))
        self.cancel_button.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.button_layout.addWidget(self.cancel_button)

        # reply button
        self.reply_button = QtWidgets.QPushButton('Reply')
        self.reply_button.setFixedSize(QtCore.QSize(70, REPLY_ATTACHMENT_HEIGHT))
        self.reply_button.setSizePolicy(QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed))
        self.reply_button.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.reply_button.setEnabled(False)
        self.button_layout.addWidget(self.reply_button)

        # tooltips
        self.attachment_widget.setToolTip('Drop image or use paste (Ctrl+V) to add attachment images')
        self.attach_image_button.setToolTip('Browse local drive for attachment images')
        self.reply_button.setToolTip('Submit your reply')
        self.cancel_button.setToolTip('Cancel writing reply')

    def init_signals(self):
        self.attach_image_button.clicked.connect(self.attach_image_clicked)
        self.attachment_widget.fileDropped.connect(self.add_images)
        self.cancel_button.clicked.connect(self.cancel_clicked)
        self.reply_button.clicked.connect(self.reply_clicked)
        self.content_textEdit.textChanged.connect(self.content_changed)

    def set_styles(self):
        self.setStyleSheet(StyleConfig.listwidget_item_style)
        self.photo_label.setStyleSheet(StyleConfig.creator_label_style)
        self.content_textEdit.setStyleSheet(StyleConfig.reply_text_style)
        self.attachment_widget.setStyleSheet(StyleConfig.attachment_style)
        self.attach_image_button.setStyleSheet(StyleConfig.gray_reply_button_style)
        self.cancel_button.setStyleSheet(StyleConfig.gray_reply_button_style)
        self.reply_button.setStyleSheet(StyleConfig.reply_button_style)

    def content_changed(self):
        text = self.content_textEdit.toPlainText()
        enabled = False
        if text:
            enabled = True
        self.reply_button.setEnabled(enabled)

    def attach_image_clicked(self):
        dialog = QtWidgets.QFileDialog()
        dialog.setWindowTitle('Add attachment')
        dialog.setNameFilters(["PNG (*.png *.PNG)", "JPEG (*.jpg *.JPG)", "TIFF (*.tiff *.TIFF)"])
        dialog.setFileMode(QtWidgets.QFileDialog.ExistingFiles)
        dialog.setAcceptMode(QtWidgets.QFileDialog.AcceptOpen) 
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            chosen_paths = dialog.selectedFiles()
            self.add_images(paths=chosen_paths)

    def add_images(self, paths):
        paths = [l for l in paths if len(os.path.splitext(l)) > 1 and os.path.splitext(l)[-1].lower() in ATTACHMENT_FORMATS]
        if paths:
            self.attachment_widget.add_items(paths=paths)

    def cancel_clicked(self):
        self.cancelClicked.emit(self.note_entity)

    def reply_clicked(self):
        qmsgBox = QtWidgets.QMessageBox(self)
        qmsgBox.setStyleSheet(StyleConfig.msgbox_style)
        qmsgBox.setText('Submit reply to note?')
        qmsgBox.setWindowTitle('Confirm')
        yes_button = qmsgBox.addButton('  Yes  ', QtWidgets.QMessageBox.AcceptRole)
        yes_button.setStyleSheet('border-radius: 6px;')
        no_button = qmsgBox.addButton('  No  ', QtWidgets.QMessageBox.RejectRole)
        no_button.setStyleSheet('border-radius: 6px;')
        qmsgBox.setIcon(QtWidgets.QMessageBox.Question)
        result = qmsgBox.exec_()
        if result == 0:
            content = self.content_textEdit.toPlainText()
            attachments = self.attachment_widget.get_all_items()
            self.replyClicked.emit((self.note_entity, content, attachments))

    def set_ui(self):
        self.set_photo()

    def set_photo(self):
        photo_path = get_photo_path(self.user)
        self.photo_label.update_photo(photo_path)

class ApprovedItem(FeedItem):
    def __init__(self, data, sg_client=None, parent=None):
        super(ApprovedItem, self).__init__(data, sg_client, parent)

    def init_ui(self):
        # main layout
        self.main_layout = QtWidgets.QVBoxLayout()
        self.main_layout.setContentsMargins(12, 12, 12, 12)
        self.main_layout.setSpacing(4)
        self.setLayout(self.main_layout)

        self.top_layout = QtWidgets.QHBoxLayout()
        self.top_layout.setSpacing(4)
        self.main_layout.addLayout(self.top_layout)

        self.title_layout = QtWidgets.QHBoxLayout()
        self.title_layout.setSpacing(6)
        self.top_layout.addLayout(self.title_layout)

        # type
        self.photo_label = display_widget.PhotoFrame()
        self.photo_label.circular = True
        self.photo_label.setFixedSize(QtCore.QSize(FEED_TYPE_ICON_SIZE[0], FEED_TYPE_ICON_SIZE[1]))
        self.photo_label.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
        self.title_layout.addWidget(self.photo_label)

        # creator date
        self.creator_date_layout = QtWidgets.QVBoxLayout()
        self.creator_date_layout.setSpacing(0)
        self.title_layout.addLayout(self.creator_date_layout)

        # creator
        self.creator_label = QtWidgets.QLabel()
        self.creator_label.setFixedHeight(20)
        self.creator_label.setAlignment(QtCore.Qt.AlignLeft)
        self.creator_date_layout.addWidget(self.creator_label)
        # date
        self.date_layout = QtWidgets.QHBoxLayout()
        self.date_layout.setContentsMargins(0, 0, 0, 0)
        self.date_layout.setSpacing(6)
        self.creator_date_layout.addLayout(self.date_layout)
        self.date_label = QtWidgets.QLabel()
        self.date_label.setFixedHeight(20)
        self.date_label.setAlignment(QtCore.Qt.AlignLeft)
        self.date_layout.addWidget(self.date_label)

        # date icon
        self.date_icon_label = QtWidgets.QLabel()
        self.date_icon_label.setFixedHeight(20)
        self.date_icon_label.setAlignment(QtCore.Qt.AlignVCenter | QtCore.Qt.AlignHCenter)
        self.date_layout.addWidget(self.date_icon_label)

        spacer1 = QtWidgets.QSpacerItem(16, 16, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.top_layout.addItem(spacer1)

        # ----- body
        self.body_layout = QtWidgets.QVBoxLayout()
        self.body_layout.setContentsMargins(16, 8, 4, 4)
        self.main_layout.addLayout(self.body_layout)

        # content
        self.content_label = text_label.HtmlLabel()
        self.content_label.setWordWrap(True)
        # self.content_label.setMargin(12)
        self.content_label.setMinimumHeight(40)
        self.content_label.setSizePolicy(QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding))
        self.body_layout.addWidget(self.content_label)

        self.bottom_layout = QtWidgets.QHBoxLayout()
        self.bottom_layout.setContentsMargins(0, 0, 0, 0)
        self.main_layout.addLayout(self.bottom_layout)

        # details
        self.detail_layout = QtWidgets.QFormLayout()
        self.detail_layout.setContentsMargins(0, 8, 0, 0)
        self.detail_layout.setLabelAlignment(QtCore.Qt.AlignLeft)
        self.detail_layout.setHorizontalSpacing(4)
        self.bottom_layout.addLayout(self.detail_layout, alignment=(QtCore.Qt.AlignBottom | QtCore.Qt.AlignLeft))

        # links
        self.link_label = text_label.HtmlLabel()
        self.link_label.setWordWrap(True)
        self.link_label.setMinimumHeight(20)
        self.link_label.setAlignment(QtCore.Qt.AlignVCenter | QtCore.Qt.AlignLeft)
        self.link_label.setSizePolicy(QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum))
        self.detail_layout.addRow('Links:', self.link_label)

        self.main_layout.setStretch(0, 0)
        self.main_layout.setStretch(1, 1)
        self.main_layout.setStretch(2, 0)

    def init_signals(self):
        pass

    def set_styles(self):
        self.setStyleSheet(StyleConfig.listwidget_item_style)
        self.photo_label.setStyleSheet(StyleConfig.creator_label_style)
        self.creator_label.setStyleSheet(StyleConfig.creator_label_style)
        self.date_label.setStyleSheet(StyleConfig.date_label_style)
        self.content_label.setStyleSheet(StyleConfig.approved_content_style)

    def set_ui(self):
        # circle label
        self.set_feed_photo()

        # creator
        self.set_creator()

        # date
        self.set_date()

        # content
        self.set_content()

        self.set_links()

    def set_feed_photo(self):
        creator =  self.data.get('client_approved_by')
        photo_path = get_photo_path(user=creator)

        # set feed photo
        self.photo_label.update_photo(photo_path)

    def set_creator(self):
        creator = self.data.get('client_approved_by')['name'] if self.data.get('client_approved_by') else NOT_AVAILABLE
        self.creator_label.setText(creator)

    def set_date(self):
        date_value = self.data.get('client_approved_at') if self.data.get('client_approved_at') else NOT_AVAILABLE
        date_text = 'Approved at: {}'.format(date_value.strftime(DATETIME_FORMAT))
        display_duration = pretty_date(time=date_value, limit_date=True)
        # new icon
        if (datetime.today().date() - date_value.date()).days <= NEW_ITEM_DAYS:
            movie = QtGui.QMovie(NEW_ICON)
            movie.setScaledSize(QtCore.QSize(NEW_ICON_SIZE[0], NEW_ICON_SIZE[1]))
            self.date_icon_label.setMovie(movie)
            movie.start()
        else:
            self.date_icon_label.hide()

        self.date_label.setText(display_duration)
        self.date_label.setToolTip(date_text)

    def set_content(self):
        self.content_label.setText(APPROVED_TEXT)

    def set_links(self):
        version_code = self.data.get('code')
        link_displays = [version_code]
        version_links = {version_code: (self.version_clicked, self.data)}
        link_text = ', '.join(list(set(link_displays)))
        self.link_label.setText(link_text, custom_links=version_links)

class FeedTreeWidget(QtWidgets.QTreeWidget):
    ''' ListWidget for note feed '''
    statusChanged = QtCore.Signal(tuple)
    dataUpdated = QtCore.Signal(tuple)  # (note_id, new_data)
    replySubmit = QtCore.Signal(tuple)
    def __init__(self, parent=None):
        super(FeedTreeWidget, self).__init__(parent)
        self.user = None
        self.attachement_cache = {}
        self.thread_cache = {}
        self.threadpool = QtCore.QThreadPool()
        self.threadpool.setMaxThreadCount(8)
        self.sg_client = ShotgunClient()

        # backgrounds
        self.bg_pixmap = QtGui.QPixmap(NO_FEED_BG)
        self.loading_movie = QtGui.QMovie(LOADING_ICON)
        self.loading_movie.frameChanged.connect(self.update)

        self.init_ui()
        self.set_styles()

    def show_feeds(self, feeds):
        self.clear()
        added_feeds = []
        for feed in feeds:
            feed_type = feed.get(sg_note.Attr.note_type_attr)
            # note
            if feed_type in SUPPORTED_NOTE_TYPES:
                self.add_entity(data=feed)

                # after the first item is added, remove loading gif
                if self.is_loading():
                    self.set_loading(False)
                added_feeds.append(feed)
            elif feed_type == CLIENT_APPROVED_TYPE:  # client approved event
                self.add_approved_feed(data=feed)
                added_feeds.append(feed)
        # expand every feed items
        self.expandAll()
        return feeds

    def set_loading(self, value):
        if value:
            self.loading_movie.start() 
        else:
            self.loading_movie.stop() 
            self.update()

    def is_loading(self):
        return True if self.loading_movie.state() == QtGui.QMovie.Running else False

    def paintEvent(self, event):
        if self.is_loading():
            movie_frame = self.loading_movie.currentPixmap()
            self.draw_background(pixmap=movie_frame)
        else:
            if not self.topLevelItemCount():
                self.draw_background(pixmap=self.bg_pixmap)

        super(FeedTreeWidget, self).paintEvent(event)

    def draw_background(self, pixmap):
        # draw background
        painter = QtGui.QPainter(self.viewport())
        winSize = self.size()
        pixmapRatio = (self.bg_pixmap.width() / self.bg_pixmap.height()) 
        windowRatio = winSize.width() / winSize.height()
        if pixmapRatio > windowRatio:
            newWidth = (winSize.height() * pixmapRatio) 
            offset = (newWidth - winSize.width()) / -2
            painter.drawPixmap(offset, 0, newWidth, winSize.height(), pixmap)
        else:
            newHeight = (winSize.width() / pixmapRatio) 
            offset = (newHeight - winSize.height()) / -2
            painter.drawPixmap(0, offset, winSize.width(), newHeight, pixmap)

    def init_ui(self):
        self.setRootIsDecorated(False)
        self.setIndentation(36)
        self.setHeaderHidden(True)
        self.setColumnCount(1)
        self.setSortingEnabled(False)
        self.setSelectionMode(QtWidgets.QAbstractItemView.NoSelection)
        self.setVerticalScrollMode(QtWidgets.QAbstractItemView.ScrollPerPixel)
        self.verticalScrollBar().setSingleStep(16)

    def set_styles(self):
        self.setStyleSheet(StyleConfig.feed_widget_style)

    def get_item_by_id(self, note_id):
        rootItem = self.invisibleRootItem()
        for c in range(rootItem.childCount()):
            item = rootItem.child(c)
            item_id = item.data(QtCore.Qt.UserRole, 0)
            if item_id == note_id:
                return item

    def get_reply_by_id(self, item, reply_id):
        for c in range(item.childCount()):
            child = item.child(c)
            child_id = child.data(QtCore.Qt.UserRole, 0)
            if child_id == reply_id:
                return child

    def add_approved_feed(self, data):
        version_id = data.get('id')

        # create itemWidget
        itemWidget = ApprovedItem(data=data, sg_client=self.sg_client, parent=self)
        # set size
        itemSize = itemWidget.sizeHint()
        widgetSize = self.sizeHint()

        # create top-level TreeWidgetItem
        item = QtWidgets.QTreeWidgetItem()
        item.setData(QtCore.Qt.UserRole, 0, version_id)
        item.setSizeHint(0, QtCore.QSize(widgetSize.width(), itemSize.height()))

        self.addTopLevelItem(item)
        self.setItemWidget(item, 0, itemWidget)
        QtWidgets.QApplication.processEvents()
        return item, itemWidget

    def add_entity(self, data):
        # if data[sg_note.Attr.note_type_attr] not in SUPPORTED_NOTE_TYPES:
        #     return

        # put cached data into FeedItem.data to skip re-download
        note_id = data.get('id')
        if note_id in self.attachement_cache:
            data['downloaded'] = self.attachement_cache[note_id]

        # create itemWidget
        itemWidget = NoteItem(data=data, sg_client=self.sg_client, parent=self)
        # set size
        itemSize = itemWidget.sizeHint()
        widgetSize = self.sizeHint()

        # create top-level TreeWidgetItem
        item = QtWidgets.QTreeWidgetItem()
        item.setData(QtCore.Qt.UserRole, 0, note_id)
        item.setSizeHint(0, QtCore.QSize(widgetSize.width(), itemSize.height()))
        itemWidget.loadAttachemntFinished.connect(self.cache_attachments)
        itemWidget.statusChanged.connect(partial(self.status_changed, item))
        itemWidget.replyChecked.connect(self.add_replybox)
        itemWidget.replyUnchecked.connect(self.cancel_reply)

        self.addTopLevelItem(item)
        self.setItemWidget(item, 0, itemWidget)
        QtWidgets.QApplication.processEvents()

        # load reply after top-level item was added
        if note_id in self.thread_cache:
            self.show_threads(item=item, thread_data=self.thread_cache[note_id])
        else:
            # start fetching reply on new thread
            itemWidget.show_loading_thread()
            worker = thread_pool.Worker(self.fetch_replies, note_id)
            worker.signals.result.connect(partial(self.fetch_replies_finished, itemWidget))
            self.threadpool.start(worker)

        return item, itemWidget

    def refresh_new_reply(self, item, itemWidget, reply_entity):
        itemWidget.show_loading_thread()
        note_id = item.data(QtCore.Qt.UserRole, 0)
        worker = thread_pool.Worker(self.fetch_replies, note_id)
        worker.signals.result.connect(partial(self.fetch_new_reply_finished, item, itemWidget, reply_entity))
        self.threadpool.start(worker)

    def fetch_new_reply_finished(self, item, itemWidget, reply_entity, fetch_results):
        self.fetch_replies_finished(itemWidget, fetch_results)

        # find the new reply and scroll to the item
        reply_id = reply_entity.get('id')
        reply_item = self.get_reply_by_id(item=item, reply_id=reply_id)
        if reply_item:
            self.scrollToItem(reply_item)

    def fetch_replies(self, note_id):
        sg_intance = self.sg_client.clone().instance
        thread_fields = {
            'Reply': ['updated_at'],
            'Attachment': ['filename', 'updated_at']
        }
        note_threads = sg_intance.note_thread_read(note_id, thread_fields)
        return note_id, note_threads

    def fetch_replies_finished(self, itemWidget, results):
        try:
            itemWidget.hide_loading_thread()
        except RuntimeError:
            pass
        if not results:
            return

        note_id, note_threads = results
        grouped_note_threads = OrderedDict()
        topic_attachments = []
        is_topic = False
        topic_thread = None
        for nt in note_threads:
            thread_typ = nt.get('type')
            thread_id = nt.get('id')
            if thread_typ in SUPPORTED_NOTE_TYPES and thread_id == note_id:
                is_topic = True
                topic_thread = nt
            elif thread_typ == 'Reply':
                is_topic = False
                nt['attachments'] = []
                grouped_note_threads[thread_id] = nt 
            elif thread_typ == 'Attachment':
                nt['name'] = nt['filename']
                if is_topic:
                    topic_attachments.append(nt)
                else:
                    existing_ids = list(grouped_note_threads.keys())
                    if existing_ids:
                        last_reply_id = existing_ids[-1]
                        grouped_note_threads[last_reply_id]['attachments'].append(nt)

        # sort replies
        sorted_reply_data = sorted(grouped_note_threads.values(), key=lambda i: i.get('updated_at') if i.get('updated_at') else i.get('created_at'), reverse=False)

        # show to UI
        note_item = self.get_item_by_id(note_id)
        if note_item:
            # add attachments to note item.data
            updated_item, updated_widget = self.update_entity(item=note_item, data={'attachments': topic_attachments})
            # update cache for new feed attachments
            self.dataUpdated.emit((note_id, updated_widget.data))

            try:
                # show replies
                self.show_threads(item=note_item, thread_data=sorted_reply_data)
            except RuntimeError as e:
                pass
        # cache reply
        self.thread_cache[note_id] = sorted_reply_data

    def show_threads(self, item, thread_data):
        # clear old reply items (if any)
        self.clear_reply_items(item=item)

        widgetSize = self.sizeHint()
        for data in thread_data:
            thread_id = data.get('id')
            if thread_id in self.attachement_cache:
                data['downloaded'] = self.attachement_cache[thread_id]

            # create reply widget
            itemWidget = ReplyItem(data, sg_client=self.sg_client)
            itemWidget.loadAttachemntFinished.connect(self.cache_attachments)
            itemSize = itemWidget.sizeHint()

            # create child item
            child_item = QtWidgets.QTreeWidgetItem()
            child_item.setData(QtCore.Qt.UserRole, 0, thread_id)
            child_item.setSizeHint(0, QtCore.QSize(widgetSize.width(), itemSize.height()))
            
            # add to parent item
            item.addChild(child_item)
            self.setItemWidget(child_item, 0, itemWidget)
            QtWidgets.QApplication.processEvents()

        # force redraw the sizes
        self.adjustSize()
        self.updateGeometry()

    def get_reply_item(self, parent_item):
        num_children = parent_item.childCount()
        for c in range(num_children):
            child = parent_item.child(c)
            childWidget = self.itemWidget(child, 0)
            if isinstance(childWidget, ReplyBox):
                return child, childWidget

    def clear_reply_items(self, item):
        item.takeChildren()

    def add_replybox(self, note_entity):
        note_id = note_entity.get('id')
        parent_item = self.get_item_by_id(note_id)
        if not parent_item:
            return
        # prevent reply box from appearing more than once
        reply_items = self.get_reply_item(parent_item)
        if reply_items:
            return

        # create reply widget
        itemWidget = ReplyBox(note_entity=note_entity, user=THIS_USER.sg_user(), sg_client=self.sg_client)
        itemWidget.cancelClicked.connect(self.cancel_reply)
        itemWidget.replyClicked.connect(self.reply_clicked)
        itemSize = itemWidget.sizeHint()
        widgetSize = self.sizeHint()

        # create child item
        child_item = QtWidgets.QTreeWidgetItem()
        child_item.setData(QtCore.Qt.UserRole, 0, note_id)
        child_item.setSizeHint(0, QtCore.QSize(widgetSize.width(), itemSize.height()))
        
        # add to parent item
        parent_item.insertChild(0, child_item)
        self.setItemWidget(child_item, 0, itemWidget)
        QtWidgets.QApplication.processEvents()

        # force redraw the sizes
        self.adjustSize()
        self.updateGeometry()
        # set focus on writing part
        itemWidget.content_textEdit.setFocus()

    def cancel_reply(self, note_entity, force=False):
        note_id = note_entity.get('id')
        parent_item = self.get_item_by_id(note_id)
        if not parent_item:
            return
        reply_items = self.get_reply_item(parent_item)
        if reply_items:
            reply_item, reply_widget = reply_items
            result = 0
            # confirm user to clear all text from reply box
            if not force and reply_widget.content_textEdit.toPlainText():
                qmsgBox = QtWidgets.QMessageBox(self)
                qmsgBox.setStyleSheet(StyleConfig.msgbox_style)
                qmsgBox.setText('Your reply will NOT be saved.\nCancel reply?')
                qmsgBox.setWindowTitle('Confirm')
                yes_button = qmsgBox.addButton('  Yes  ', QtWidgets.QMessageBox.AcceptRole)
                yes_button.setStyleSheet('border-radius: 6px;')
                no_button = qmsgBox.addButton('  No  ', QtWidgets.QMessageBox.RejectRole)
                no_button.setStyleSheet('border-radius: 6px;')
                qmsgBox.setIcon(QtWidgets.QMessageBox.Warning)
                result = qmsgBox.exec_()
            if result == 0:
                index = parent_item.indexOfChild(reply_item)
                parent_item.takeChild(index)
                checked = False
            else:
                checked = True

            # reply button check state
            parent_widget = self.itemWidget(parent_item, 0)
            parent_widget.reply_button.blockSignals(True)
            parent_widget.reply_button.setChecked(checked)
            parent_widget.reply_button.blockSignals(False)

    def reply_clicked(self, signals):
        note_entity, content, attachments = signals
        note_id = note_entity.get('id')
        note_item = self.get_item_by_id(note_id)
        if not note_item:
            return
        self.replySubmit.emit((note_entity, content, THIS_USER.sg_user(), attachments, note_item))
        
    def update_entity(self, item, data):
        itemWidget = self.itemWidget(item, 0)
        itemWidget.data.update(data)
        itemWidget.set_ui()
        # set size
        itemSize = itemWidget.sizeHint()
        widgetSize = self.sizeHint()
        item.setSizeHint(0, QtCore.QSize(widgetSize.width(), itemSize.height()))

        # set item widget
        self.setItemWidget(item, 0, itemWidget)
        return item, itemWidget

    def status_changed(self, item, edit_data):
        note_id, status = edit_data
        self.statusChanged.emit((item, note_id, status))

    def cache_attachments(self, data):
        note_id = data[0]
        att = data[1]
        image = data[2]
        att_id = att.get('id')
        to_remove = []
        if note_id not in self.attachement_cache:
            self.attachement_cache[note_id] = []
        self.attachement_cache[note_id].append((att, image))

    def clear_cache(self):
        to_remove = []
        for note_id, att_data in self.attachement_cache.items():
            for att in att_data:
                image = att[1]
                if os.path.exists(image):
                    to_remove.append(image)

        self.attachement_cache = {}
        self.thread_cache = {}
        if to_remove:
            worker = thread_pool.Worker(self.delete_temp, to_remove)
            self.threadpool.start(worker)

    def clear_note_cache(self):
        to_remove = []
        rootItem = self.invisibleRootItem()
        for i in range(rootItem.childCount()):
            item = rootItem.child(i)
            note_id = item.data(QtCore.Qt.UserRole, 0)
            if note_id in self.attachement_cache:
                for att, img in self.attachement_cache[note_id]:
                    to_remove.append(img)
                del self.attachement_cache[note_id]
            if note_id in self.thread_cache:
                del self.thread_cache[note_id]

        if to_remove:
            worker = thread_pool.Worker(self.delete_temp, to_remove)
            self.threadpool.start(worker)

    def delete_temp(self, paths):
        for path in paths:
            try:
                os.remove(path)
            except Exception as e:
                print(e)

class UserComboBox(user_widget.UserComboBox):
    """ user comboBox """
    def __init__(self, user=None, parent = None) :
        super(UserComboBox, self).__init__(parent)

    def allow_debug(self): 
        return self.user.is_admin() or self.user.is_hr() or self.user.is_supervisor() or self.user.is_producer()

# ---- Main widget
class BriefBoard(QtWidgets.QMainWindow):
    ''' Application class '''
    def __init__(self, parent=None):
        # setup Window
        super(BriefBoard, self).__init__(parent)

        # app vars
        self.is_admin = False
        self.threadpool = QtCore.QThreadPool()
        self.sg_client = ShotgunClient()
        self.jobs = OrderedDict()
        self.feed_cache = {}
        self.media_cache = {}

        # ui vars
        self.w = 1100
        self.h = 750
        
        # init functions
        self.setup_ui()
        self.set_styles()
        self.init_signals()
        self.set_default()

    def closeEvent(self, event):
        self.clear_cache()

        # clear left over temp files before closing
        sleep(1)
        for temp in GLOBAL_TEMP:
            if os.path.exists(temp):
                try:
                    os.remove(temp)
                except Exception:
                    pass

    def resizeEvent(self, event):
        super(BriefBoard, self).resizeEvent(event)
        self.media_widget.stretch_items(mode='horizontal', offset=MEDIA_OFFSET)

    def setup_ui(self):
        self.setObjectName(uiName)
        self.setWindowTitle('{} {} {}'.format(_title, _version, _des))
        self.setWindowIcon(QtGui.QIcon(APP_ICON))
        self.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))

        self.centralwidget = QtWidgets.QWidget(self)
        self.setCentralWidget(self.centralwidget)
        self.resize(self.w, self.h)

        # main layout
        self.main_layout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.main_layout.setContentsMargins(9, 9, 9, 9)
        self.main_layout.setSpacing(9)

        # splitter
        self.view_splitter = QtWidgets.QSplitter(self)
        self.view_splitter.setOrientation(QtCore.Qt.Horizontal)

        self.task_split_widget = QtWidgets.QWidget(self.view_splitter)
        self.view_split_widget = QtWidgets.QWidget(self.view_splitter)
        self.view_splitter.setSizes([350, 750])
        self.main_layout.addWidget(self.view_splitter)

        self.splitter_handle = self.view_splitter.handle(1)
        self.splitter_handle_layout = QtWidgets.QVBoxLayout(self.splitter_handle)
        self.splitter_handle_layout.setSpacing(0)
        self.splitter_handle_layout.setContentsMargins(0, 0, 0, 0)
        self.splitter_line = QtWidgets.QFrame(self.splitter_handle)
        self.splitter_line.setStyleSheet('color: rgb{};'.format(StyleConfig.lower_light_gray))
        self.splitter_line.setLineWidth(2)
        self.splitter_line.setMinimumHeight(250)
        self.splitter_line.setSizePolicy(QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Maximum))
        self.splitter_line.setFrameShadow(QtWidgets.QFrame.Plain)
        self.splitter_line.setFrameShape(QtWidgets.QFrame.VLine)
        self.splitter_handle_layout.addWidget(self.splitter_line)

        # -------- job layout
        self.task_layout = QtWidgets.QVBoxLayout(self.task_split_widget)
        self.task_layout.setContentsMargins(8, 0, 8, 0)

        # header layout
        self.header_layout = QtWidgets.QHBoxLayout()
        self.header_layout.setContentsMargins(6, 6, 0, 0)
        self.task_layout.addLayout(self.header_layout)

        # photo
        self.user_photo_label = display_widget.SGPhotoFrame()
        self.user_photo_label.circular = True
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        self.user_photo_label.setSizePolicy(sizePolicy)
        self.user_photo_label.setFixedSize(QtCore.QSize(USER_PHOTO_SIZE[0], USER_PHOTO_SIZE[1]))
        self.header_layout.addWidget(self.user_photo_label)

        self.title_option_layout = QtWidgets.QFormLayout()
        self.title_option_layout.setSpacing(6)
        self.header_layout.addLayout(self.title_option_layout)

        # user selector
        self.user_widget = UserComboBox()
        self.user_widget.setMinimumWidth(80)
        self.user_widget.label.hide()
        self.user_widget.checkBox.hide()
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.user_widget.setSizePolicy(sizePolicy)
        self.title_option_layout.addRow('User', self.user_widget)

        # project selector
        self.project_widget = project_widget.SGProjectComboBox(sg=sg, layout=QtWidgets.QHBoxLayout(), parent=self)
        self.project_widget.setMinimumWidth(80)
        self.project_widget.label.hide()
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.project_widget.setSizePolicy(sizePolicy)
        # add ALL to project
        self.project_widget.projectComboBox.insertItem(0, 'ALL', 'ALL')
        self.project_widget.projectComboBox.setCurrentIndex(0)
        self.title_option_layout.addRow('Proj', self.project_widget)

        spacer1 = QtWidgets.QSpacerItem(16, 16, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.header_layout.addItem(spacer1)

        self.header_layout.setStretch(0, 0)
        self.header_layout.setStretch(1, 0)
        self.header_layout.setStretch(2, 1)

        self.job_view_layout = QtWidgets.QVBoxLayout()
        self.job_view_layout.setSpacing(6)
        self.task_layout.addLayout(self.job_view_layout)

        # search and refresh bar
        self.search_layout = QtWidgets.QHBoxLayout()
        self.job_view_layout.addLayout(self.search_layout)

        # search icon
        self.search_icon_label = QtWidgets.QLabel()
        self.search_icon_label.setPixmap(QtGui.QIcon(SEARCH_ICON).pixmap(QtCore.QSize(24, 24)))
        self.search_layout.addWidget(self.search_icon_label)

        # search bar
        self.search_bar = QtWidgets.QLineEdit()
        self.search_bar.setMinimumSize(QtCore.QSize(24, 24))
        self.search_layout.addWidget(self.search_bar)

        # refresh button
        self.refresh_button = QtWidgets.QPushButton()
        self.refresh_button.setFixedSize(QtCore.QSize(24, 24))
        self.refresh_button.setIcon(QtGui.QIcon(REFRESH_ICON))
        self.refresh_button.setIconSize(QtCore.QSize(16, 16))
        self.search_layout.addWidget(self.refresh_button)

        # job list widget
        self.job_layout = QtWidgets.QVBoxLayout()
        self.job_view_layout.addLayout(self.job_layout)

        self.job_layout.setContentsMargins(0, 0, 0, 0)
        self.job_listwidget = JobListWidget()
        self.job_layout.addWidget(self.job_listwidget)

        self.task_layout.setStretch(0, 0)
        self.task_layout.setStretch(1, 1)

        # -------- tab layout
        self.view_layout = QtWidgets.QVBoxLayout(self.view_split_widget)
        self.view_layout.setContentsMargins(8, 8, 8, 0)

        self.tab_widget = QtWidgets.QTabWidget()
        self.view_layout.addWidget(self.tab_widget)

        # note feed
        self.feed_treewidget = FeedTreeWidget()
        self.tab_widget.addTab(self.feed_treewidget, 'Notes')
        
        # publish tab
        self.media_widget = MediaListWidget(fps=DEFAULT_VID_FPS, quality=DEFAULT_VID_QUALITY)
        self.media_widget.setSpacing(16)
        self.media_widget.setSelectionMode(QtWidgets.QAbstractItemView.NoSelection)
        self.media_widget.setVerticalScrollMode(QtWidgets.QAbstractItemView.ScrollPerPixel)
        self.media_widget.verticalScrollBar().setSingleStep(32)
        self.media_widget.auto_play = AUTOPLAY_VID
        self.media_widget.show_play_button = SHOW_PLAY_BUTTON
        self.media_widget.clear_cache_on_hide = CLEAR_MEDIA_CACHE
        self.media_widget.poster_frame_weight = 0
        self.media_widget.customContextMenuRequested.connect(self.media_right_clicked)
        self.tab_widget.addTab(self.media_widget, 'Published')

        # -------- status line
        self.statusBar = QtWidgets.QStatusBar()
        self.main_layout.addWidget(self.statusBar)

        self.main_layout.setStretch(0, 5)
        self.main_layout.setStretch(1, 0)

        # tooltips
        self.refresh_button.setToolTip('Refresh your job list')
        self.search_bar.setToolTip('Search keywords are:\n project, type, name, tags,\n episode and task name')

        # place holder texts
        self.search_bar.setPlaceholderText(SEARCHBAR_PLACEHOLDER)

    def set_styles(self):
        self.search_bar.setStyleSheet(StyleConfig.searchbar_style)
        self.refresh_button.setStyleSheet(StyleConfig.refresh_button_style)
        self.tab_widget.setStyleSheet(StyleConfig.tab_widget_style)
        self.media_widget.setStyleSheet(StyleConfig.media_widget_style)

    def init_signals(self):
        # project signals
        self.project_widget.projectChanged.connect(self.refresh_user_tasks)

        # user signals
        self.is_admin = self.user_widget.allow_debug()
        if self.is_admin:
            self.user_widget.checkBox.setChecked(True)
            self.user_widget.set_searchable(True)
            self.user_widget.searchable_by_id = True
            self.user_widget.comboBox.lineEdit().textChanged.connect(self.username_edited)
            self.user_widget.comboBox.currentIndexChanged.connect(self.user_changed)

        #tabs
        self.tab_widget.currentChanged.connect(lambda: self.update_view(force=False))
        # job signals
        self.job_listwidget.taskChanged.connect(self.task_status_changed) 
        self.job_listwidget.itemSelectionChanged.connect(self.job_item_changed)
        self.job_listwidget.refreshRequested.connect(self.refresh_views)
        self.job_listwidget.itemDoubleClicked.connect(self.refresh_views)
        # feed signals
        self.feed_treewidget.statusChanged.connect(self.note_status_changed)
        self.feed_treewidget.replySubmit.connect(self.submit_reply)
        self.feed_treewidget.dataUpdated.connect(self.update_cache)
        # media signals
        self.media_widget.itemDoubleClicked.connect(self.open_media)
        self.view_splitter.splitterMoved.connect(lambda: self.media_widget.stretch_items(mode='horizontal', offset=MEDIA_OFFSET))
        # search bar
        self.search_bar.textChanged.connect(self.filter_display)
        # buttons
        self.refresh_button.clicked.connect(self.refresh_user_tasks)

    def set_default(self):
        
        # init user
        self.user_changed()

    def refresh_views(self):
        self.feed_treewidget.clear_note_cache()
        self.clear_feed_cache()

        self.media_widget.clear_all_pixmaps()
        self.clear_media_cache()

        self.update_view(force=True)

    def update_view(self, force=False):
        current_tab_widget = self.tab_widget.currentWidget()
        if current_tab_widget == self.feed_treewidget:
            self.update_feed(force=force)
        elif current_tab_widget == self.media_widget:
            self.update_media(force=force)

    def job_item_changed(self):
        self.media_widget.clear_all_pixmaps()
        current_tab_widget = self.tab_widget.currentWidget()
        if current_tab_widget == self.feed_treewidget:
            self.update_feed(force=False)
        elif current_tab_widget == self.media_widget:
            self.update_media(force=False, redraw=True)

    # ----- media funcs
    def open_media(self, item):
        item_data = item.data(QtCore.Qt.UserRole)
        if 'filepath' in item_data:
            path = item_data['filepath']
            if os.path.exists(path):
                os.startfile(path)

    def open_media_dir(self, item):
        item_data = item.data(QtCore.Qt.UserRole)
        if 'filepath' in item_data:
            path = item_data['filepath']
            if os.path.exists(path):
                dir_path = os.path.dirname(path)
                Popen(r'explorer {}'.format(dir_path.replace('/', '\\')))

    def media_right_clicked(self, pos):
        # right click menu
        item = self.media_widget.itemAt(pos)

        rightClickMenu = QtWidgets.QMenu(self.media_widget)

        # open file
        open_action = QtWidgets.QAction('Play', self.media_widget)
        open_action.setIcon(QtGui.QIcon(PLAY_ICON))
        open_action.triggered.connect(partial(self.open_media, item))
        rightClickMenu.addAction(open_action)

        # open directory
        open_dir_action = QtWidgets.QAction('Open directory', self.media_widget)
        open_dir_action.setIcon(QtGui.QIcon(FOLDER_ICON))
        open_dir_action.triggered.connect(partial(self.open_media_dir, item))
        rightClickMenu.addAction(open_dir_action)

        rightClickMenu.exec_(self.media_widget.mapToGlobal(pos))

    def setup_media_view(self, project):
        # setup path context 
        context = context_info.Context()
        context.update(project=project)
        self.context = context_info.ContextPathInfo(context=context)

        if self.context:
            # update media widget settings everytime entity changes
            self.media_widget.fps = self.context.projectInfo.render.fps()
            w, h = self.context.projectInfo.render.final_outputH()
            self.media_widget.item_size = (w * VID_SIZE_MULT, h * VID_SIZE_MULT)  # multply the load size for performance
        else:
            self.media_widget.fps = DEFAULT_VID_FPS
            self.media_widget.item_size = DEFAULT_VID_SIZE

    def update_media(self, force=False, redraw=False):
        # get current entity
        item = self.get_current_job_item()
        if not item:
            self.show_status('Please select a job item.', level='warning')
            return
        itemWidget = self.job_listwidget.itemWidget(item)
        project = itemWidget.data.get('project')['name']
        entity = item.data(QtCore.Qt.UserRole)
        entity_id = entity.get('id')

        # setup view fps and sizes
        self.setup_media_view(project=project)

        # display from cache
        if entity_id in self.media_cache and not force:
            if redraw:
                self.clear_media_display()
                sorted_media = self.show_media(sorted_media=deepcopy(self.media_cache[entity_id]))
            self.show_status('Loaded {} published media from cache.'.format(len(self.media_cache[entity_id])), level='success')
        else:  # refetch
            self.clear_media_cache(entity_id=entity_id) 
            self.clear_media_display()
            # lock UI
            self.show_status('Fetching published media on {}...'.format(entity.get('name')), level='warning')
            self.media_widget.set_loading(True)
            self.job_listwidget.setEnabled(False)

            # fetch SG
            worker = thread_pool.Worker(self.fetch_versions, entity)
            worker.signals.result.connect(partial(self.fetch_versions_finished, entity))
            self.threadpool.start(worker)

    def version_to_tasks(self, versions, entity):
        # store in dict
        tasks = OrderedDict()  # {'task':[v1, v2, v3]}
        for version in versions:
            if 'sg_task' in version and 'name' in version['sg_task']:
                taskname = version['sg_task']['name']
                version_entity = version['entity']
                if version_entity['id'] != entity['id']:
                    entityname = version_entity['name']
                    taskname = '{} - {}'.format(taskname, entityname)
                if taskname and taskname not in tasks:
                    tasks[taskname] = []
                tasks[taskname].append(version)
        return tasks

    def fetch_versions(self, entity): 
        sg_instance = self.sg_client.clone().instance
        # fetch from SG
        fields = [
            'code', 'description', 'sg_status_list', 'sg_path_to_movie', 'sg_task.Task.step.Step.code',
            'entity', 'user', 'sg_task', 'created_at', 'sg_task.Task.sg_status_list']

        filters = [['entity', 'is', entity]]
        entity_versions = sg_instance.find('Version', filters, fields)
        entity_tasks = self.version_to_tasks(entity_versions, entity)

        # get related
        related_tasks = []
        if INCLUDE_RELATED_STEPS:
            filters = [['id','is', entity['id']]]
            fields = ['sg_link_designs', 'id']
            related_data = sg_instance.find_one('Asset', filters, fields)
            extra_entity = related_data.get('sg_link_designs') if related_data and 'sg_link_designs' in related_data else []
            if extra_entity:
                advancedFilter = {
                                "filter_operator": "any",
                                "filters": [['sg_task.Task.step.Step.code', 'is', s] for s in INCLUDE_RELATED_STEPS]
                                }
                extra_filters = [advancedFilter]
                advancedFilter = {
                                    "filter_operator": "any",
                                    "filters": [['entity', 'is', e] for e in extra_entity]
                                }
                extra_filters.append(advancedFilter)
                related_versions = sg_instance.find('Version', extra_filters, fields)
                related_tasks = self.version_to_tasks(related_versions, entity)
                entity_tasks.update(related_tasks)
        return entity_tasks

    def fetch_versions_finished(self, entity, versions):
        # sort raw sg data
        sorted_media = self.sort_media(media_versions=versions)
        # add to UI
        self.show_media(sorted_media=sorted_media)

        # store feeds in cache
        entity_id = entity.get('id')
        self.media_cache[entity_id] = sorted_media

        # release UI
        self.media_widget.set_loading(False)
        self.job_listwidget.setEnabled(True)

        media_num = len(sorted_media)
        feed_txt = 'Found {} published media.'.format(media_num) if media_num else 'No published media found.'
        self.show_status('Fetch media finished. ' + feed_txt, level='success')

    def sort_media(self, media_versions):
        media = []
        for task_name, versions in media_versions.items():
            # only take latest version
            latest_version = versions[-1]

            version_entity_name = latest_version.get('entity')['name'] if latest_version.get('entity') else NOT_AVAILABLE
            user_name = latest_version.get('user')['name'] if latest_version.get('user') else NOT_AVAILABLE
            entity_code = latest_version.get('code') if latest_version.get('code') else NOT_AVAILABLE
            step_name = latest_version.get('sg_task.Task.step.Step.code') if latest_version.get('sg_task.Task.step.Step.code') else NOT_AVAILABLE
            date_value = latest_version.get('created_at')
            updated_date = date_value.strftime(DATETIME_FORMAT) if date_value else NOT_AVAILABLE
            display_date = pretty_date(date_value, limit_date=True) if date_value else NOT_AVAILABLE
            status_value = latest_version.get('sg_task.Task.sg_status_list')
            status_data = status_widget.Icon.statusMap.get(status_value) 
            task_status_display = status_data.get('display') if status_data else NOT_AVAILABLE

            # bottom text
            bottom_text = '{}\n{} by {}'.format(entity_code, display_date, user_name)

            # data
            data = latest_version
            # tooltip
            data['__toolTip'] = 'Step: {}\nTask: {}\nUser: {}\nUpdated: {}\nStatus: {}'.format(step_name, task_name, user_name, updated_date, task_status_display)
            # task name text overlay
            text_args = {'text': task_name, 'alignment': 'topCenter', 'font': 'Calibri', 'size': 24, 'color': StyleConfig.yellow}
            data['__overlay_texts'] = [text_args]

            # status image overlay
            if status_value == 'apr':
                status_icon = APPROVED_ICON
            else:
                status_icon = WIP_ICON
            data['__overlay_images'] = [(status_icon, 'topLeft', 0.185)]

            # path
            sg_path = latest_version.get('sg_path_to_movie')
            if sg_path:
                if '#' in sg_path:
                    images = [im.replace('\\', '/') for im in glob(sg_path.replace('#', '*'))]
                    if not images:
                        images = [NOPREVIEW_ICON]
                    for im in images:
                        data['filepath'] = im
                        media.append([im, bottom_text, data])
                else: # single image/vid
                    if not os.path.exists(sg_path):
                        sg_path = NOPREVIEW_ICON
                    data['filepath'] = sg_path
                    media.append([sg_path, bottom_text, data])

        # sort by created_at
        sorted_media = sorted(media, key=lambda i: i[2].get('created_at'), reverse=True)
        return sorted_media

    def show_media(self, sorted_media):
        # add media to UI
        self.media_widget.add_items(media_info=sorted_media)
        self.media_widget.stretch_items(mode='horizontal', offset=MEDIA_OFFSET)

        return sorted_media
        
    # ----- feed funcs
    def update_feed(self, force):
        self.clear_feed_display()
        
        item = self.get_current_job_item()
        if not item:
            self.show_status('Please select a job item.', level='warning')
            return

        itemWidget = self.job_listwidget.itemWidget(item)
        project = itemWidget.data.get('project')['name']
        entity = item.data(QtCore.Qt.UserRole)
        entity_id = entity.get('id')
        
        # display from cache
        if entity_id in self.feed_cache and not force:
            sorted_feeds = self.show_feeds(feeds=deepcopy(self.feed_cache[entity_id]))
            self.show_status('Loaded {} note(s) from cache.'.format(len(sorted_feeds)), level='success')
        else:  # refetch
            # lock UI
            self.show_status('Fetching notes on {}...'.format(entity.get('name')), level='warning')
            # QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
            self.feed_treewidget.set_loading(True)
            self.job_listwidget.setEnabled(False)

            # get tasks related to the entity
            task_filters = itemWidget.data.get('tasks') if itemWidget.data.get('tasks') else []

            # spawn a thread to fetch feeds
            worker = thread_pool.Worker(self.fetch_feeds, project, entity, task_filters)
            worker.signals.result.connect(self.fetch_feeds_finished)
            self.threadpool.start(worker)
    
    def fetch_feeds(self, project, entity, task_filters):
        fields = ['project', 'note_links', 'created_at', 'updated_at', 'content', 'subject', 'sg_note_type',
            'tasks', 'user', 'sg_status_list', 'sg_modified_by', 'user.HumanUser.image', 'sg_brief',
            'addressings_to', 'sg_subnotes', 'note_sg_subnotes_notes', 'ticket_sg_note_tickets', 'addressings_cc', 
            'tasks.Task.due_date']
        sg_note.sg = self.sg_client.clone().instance
        feeds = sg_note.fetch_entity_notes_briefs(project=project, 
                                                sg_entity=entity, 
                                                task_filters=task_filters,
                                                fields=fields)  # skip attachments fields, will fetch em along with replies

        apr_versions = sg_note.fetch_client_approved_versions(project=project, 
                                                sg_entity=entity,
                                                task_filters=task_filters)
        return entity, feeds, apr_versions

    def fetch_feeds_finished(self, fetch_results):
        entity, feeds, apr_versions = fetch_results
        # combine approved versions to feeds
        for taskname, versions in apr_versions.items():
            if taskname not in feeds:
                feeds[taskname] = []
            feeds[taskname].extend(versions)

        sorted_feeds = self.show_feeds(feeds=feeds)
        
        # store feeds in cache
        entity_id = entity.get('id')
        self.feed_cache[entity_id] = feeds

        # release UI
        self.feed_treewidget.set_loading(False)
        self.job_listwidget.setEnabled(True)
        # QtWidgets.QApplication.restoreOverrideCursor()
        feed_num = len(sorted_feeds)
        feed_txt = 'Found {} note(s).'.format(feed_num) if feed_num else 'No note found.'
        self.show_status('Fetch feed finished. ' + feed_txt, level='success')
        
    def show_feeds(self, feeds):
        # filter and sort feeds by updated date
        feed_dict = {}
        for taskname, feed_entities in feeds.items(): 
            for feed in feed_entities: 
                if feed.get(sg_note.Attr.note_type_attr) in SUPPORTED_NOTE_TYPES:
                    # skip closed notes
                    if feed.get('sg_status_list') not in NOTE_STATUS:
                        continue

                feed_id = feed.get('id')
                if feed_id not in feed_dict:
                    feed_dict[feed_id] = feed

        sorted_feeds = sorted(feed_dict.values(), key=lambda i: i.get('updated_at') if i.get(sg_note.Attr.note_type_attr) in SUPPORTED_NOTE_TYPES else i.get('client_approved_at') , reverse=True)

        # add to UI
        sorted_feeds = self.feed_treewidget.show_feeds(feeds=sorted_feeds)
        return sorted_feeds

    def filter_display(self):
        keywords = []
        try:
            keywords = [str(kw.lower()) for kw in self.search_bar.text().replace(' ', '').split(',')]
        except UnicodeEncodeError:
            pass

        for i in range(self.job_listwidget.count()):
            item = self.job_listwidget.item(i)
            hidden = False
            if keywords:
                itemWidget = self.job_listwidget.itemWidget(item)
                for kw in keywords:
                    hidden = True
                    found = False
                    for search_code in itemWidget.data['keywords']:
                        if kw in search_code:
                            hidden = False
                            break
                    if found:
                        break

            if not hidden and item.isSelected():
                item.setSelected(False)
            item.setHidden(hidden)

    def update_cache(self, signals):
        note_id, new_data = signals
        job_item = self.get_current_job_item()
        entity = job_item.data(QtCore.Qt.UserRole)
        entity_id = entity.get('id')
        feed_cache_data = self.feed_cache.get(entity_id)
        if feed_cache_data:
            for taskname, feeds in feed_cache_data.items():
                for i, feed in enumerate(feeds):
                    feed_id = feed.get('id')
                    if feed_id == note_id:
                        self.feed_cache[entity_id][taskname][i] = new_data
                        return

    # ----- status funcs
    def note_status_changed(self, edit_data):
        item, note_id, status = edit_data

        # lock UI
        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
        self.show_status('Updating note status...', level='warning')
        self.feed_treewidget.setEnabled(False)

        worker = thread_pool.Worker(self.update_note_status, note_id, status, item)
        worker.signals.result.connect(self.update_note_status_finished)
        self.threadpool.start(worker)

    def update_note_status(self, note_id, status, item):
        result = sg_note.update_note(note_id, status, THIS_USER.sg_user())
        return result, item

    def update_note_status_finished(self, update_results):
        updated_note, item = update_results
        updated_note_id = updated_note.get('id')

        # update feed item UI
        self.feed_treewidget.update_entity(item=item, data=updated_note)

        # update cache
        item = self.get_current_job_item()
        entity = item.data(QtCore.Qt.UserRole)
        entity_id = entity.get('id')
        for task, feeds in self.feed_cache[entity_id].items(): 
            found = False
            for i, feed in enumerate(feeds): 
                if feed.get('id') == updated_note_id:
                    feed.update(updated_note)
                    found = True
                    break
            if found:
                break

        # release UI
        QtWidgets.QApplication.restoreOverrideCursor()
        self.show_status('Note status updated.', level='success')
        self.feed_treewidget.setEnabled(True)

    def task_status_changed(self, edit_data):
        task, status, item = edit_data
        task_id = task.get('id')

        # lock UI
        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
        self.show_status('Updating task status...', level='warning')
        self.job_listwidget.setEnabled(False)

        worker = thread_pool.Worker(self.update_task_status, task_id, status, item)
        worker.signals.result.connect(self.update_task_status_finished)
        self.threadpool.start(worker)

    def update_task_status(self, task_id, status, item):
        user = THIS_USER
        userName = user.name()
        filters = [['id', 'is', task_id]]
        old_status = sg.find('Task', filters, ['sg_status_list'])[0]['sg_status_list']
        now = datetime.now()
        current_time = now.strftime("%m-%d-%y %H:%M") 
        metaData = {'status': {'old': old_status, 'new': status, 'by': userName, 'time': current_time}}
        metaData = str(metaData)
        updated_task = sg.update('Task', task_id, {'sg_status_list': status, 'sg_meta': metaData})
        return updated_task, item

    def update_task_status_finished(self, update_results):
        updated_task, item = update_results
        entity = item.data(QtCore.Qt.UserRole)
        entity_id = entity.get('id')
        new_tasks = []
        for i, task in enumerate(self.jobs[entity_id]['tasks']):
            if task['id'] == updated_task.get('id'):
                task['sg_status_list'] = updated_task.get('sg_status_list')
            new_tasks.append(task)

        # set internal var
        self.jobs[entity_id]['tasks'] = new_tasks

        # update item
        self.job_listwidget.update_entity(item=item, data=self.jobs[entity_id])

        # release UI
        QtWidgets.QApplication.restoreOverrideCursor()
        self.show_status('Task status updated.', level='success')
        self.job_listwidget.setEnabled(True)
    
    # ----- reply funcs
    def submit_reply(self, signals):
        note_entity, content, user, attachments, item = signals
        
        # lock UI
        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
        self.show_status('Submiting your reply...', level='warning')
        self.feed_treewidget.setEnabled(False)

        worker = thread_pool.Worker(self.add_reply, note_entity, content, user, attachments, item)
        worker.signals.result.connect(self.add_reply_finished)
        self.threadpool.start(worker)

    def add_reply(self, note_entity, content, user, attachments, item):
        updated_note = sg_note.add_reply(note_entity=note_entity, 
                            content=content, 
                            user_entity=user, 
                            attachments=attachments)
        return updated_note, item

    def add_reply_finished(self, results):
        reply_results, item = results
        itemWidget = self.feed_treewidget.itemWidget(item, 0)
        note_entity = itemWidget.data
        note_id = note_entity.get('id')
        # remove replybox
        self.feed_treewidget.cancel_reply(note_entity=note_entity, force=True)

        # release UI
        text = 'Reply submited.' if reply_results else 'Reply failed.'
        msg_level = 'success' if reply_results else 'error'
        QtWidgets.QApplication.restoreOverrideCursor()
        self.show_status(text, level=msg_level)
        self.feed_treewidget.setEnabled(True)

        if reply_results:
            reply_entity = reply_results[0]
            # update feed item's reply
            self.feed_treewidget.refresh_new_reply(item=item, itemWidget=itemWidget, reply_entity=reply_entity)

    # ----- job funcs
    def fetch_jobs(self, user):
        self.jobs = OrderedDict()
        sel_proj = self.project_widget.current_item()
        project = sel_proj if sel_proj!='ALL' else None

        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
        self.show_status('Fetching jobs...', level='warning')
        self.setEnabled(False)
        self.job_listwidget.set_loading(True)

        worker = thread_pool.Worker(self.fetch_data, project, user)
        worker.signals.result.connect(self.display_jobs)
        self.threadpool.start(worker)

    def fetch_data(self, project, user):
        history_time = datetime.today() - timedelta(days=DAY_RANGE)
        tasks_filter = [
            ['updated_at', 'greater_than', history_time],  # updated withing certain time range
            ['sg_type', 'in', ['Shot', 'Asset']],  # only consider Shot or Asset typed tasks
            ['content', 'not_in', IGNORED_TASKS]  # Skip all *_sup tasks
        ]

        advanced_filter = { "filter_operator": "or",
                            "filters": [["task_assignees", "is", user],
                                        ["sg_involve_with", "is", user]]}
        tasks_filter.append(advanced_filter)

        if project:
            tasks_filter.append(['project', 'is', project])
        else:
            tasks_filter.append(['project', 'in', ACTIVE_PROJECTS])

        fields = [
            'project',
            'content',
            'sg_status_list',
            'updated_at',
            'due_date',
            'task_assignees', 
            'sg_involve_with',
            'entity',
            'step.Step.code',
            'sg_type',

            'entity.Asset.tags',
            'entity.Asset.code',
            'entity.Asset.sg_status_list',
            'entity.Asset.sg_asset_type',
            'entity.Asset.sg_scenes',
            'entity.Asset.sequences',
            'entity.Asset.sg_production_type',

            'entity.Shot.tags',
            'entity.Shot.sg_status_list',
            'entity.Shot.sg_sequence_code',
            'entity.Shot.sg_episode.Scene.code',
            'entity.Shot.sg_shortcode']
        tasks = sg.find('Task', tasks_filter, fields)
        # print(tasks)
        return tasks, user

    def display_jobs(self, results):
        tasks, user = results

        self.clear_job_display()
        self.clear_feed_display()
        self.clear_media_display()
        self.jobs = OrderedDict()
        jobs = OrderedDict()
        user_id = user.get('id')
        # filter tasks
        for task in tasks:
            entity = task.get('entity')
            entity_type = entity.get('type') if entity else None
            project = task.get('project')
            step = task.get('step.Step.code')
            is_preproduction = False
            # key may vary by entity type
            used_in = []
            if entity_type == 'Asset':
                status_key = 'entity.Asset.sg_status_list'
                tags = task.get('entity.Asset.tags') if task.get('entity.Asset.tags') else []
                name = task.get('entity.Asset.code') if task.get('entity.Asset.code') else ''
                typ = task.get('entity.Asset.sg_asset_type') if task.get('entity.Asset.sg_asset_type') else ''
                used_in = task.get('entity.Asset.sg_scenes', []) + task.get('entity.Asset.sequences', [])
                is_preproduction = True if task.get('entity.Asset.sg_production_type') == 'Preproduction' else False 
            elif entity_type == 'Shot':
                status_key = 'entity.Scene.sg_status_list'
                tags = task.get('entity.Shot.tags') if task.get('entity.Shot.tags') else []
                name = task.get('entity.Shot.sg_shortcode') if task.get('entity.Shot.sg_shortcode') else ''
                seq = task.get('entity.Shot.sg_sequence_code') if task.get('entity.Shot.sg_sequence_code') else ''
                ep = task.get('entity.Shot.sg_episode.Scene.code') if task.get('entity.Shot.sg_episode.Scene.code') else ''
                typ = ''
                if seq and ep:
                    typ = '{}_{}'.format(ep, seq)
            else:
                continue

            main_status = task.get(status_key)
            if main_status in ENTITY_STATUS_FILTERS:
                continue

            task_status = task.get('sg_status_list')
            if task_status in TASK_STATUS_FILTERS:
                continue

            entity_id = task.get('entity')['id']
            if entity_id not in jobs:
                jobs[entity_id] = {'tasks':[], 
                                'project': '', 
                                'entity_type': entity_type,
                                'name': '', 
                                'steps': [],
                                'type': '', 
                                'due_date': '', 
                                'updated_at': '',
                                'status': '', 
                                'tags': [],
                                'project': project,
                                'entity': entity,
                                'type': typ,
                                'name': name,
                                'tags': [t for t in tags if is_eng(t.get('name'))],
                                'status': main_status,
                                'used_in': used_in,
                                'is_assigned': False, 
                                'is_preproduction': is_preproduction}

            jobs[entity_id]['tasks'].append(task)
            jobs[entity_id]['steps'].append(step)

            # assigned
            assignees = [u.get('id') for u in task.get('task_assignees')] if task.get('task_assignees') else []
            if user_id in assignees and jobs[entity_id]['is_assigned'] == False:
                jobs[entity_id]['is_assigned'] = True

            # keywords for search
            project_name = project.get('name') if project.get('name') else ''
            search_keys = [project_name.lower(), typ.lower(), name.lower()]
            search_keys += [t.get('name').lower() for t in tags if is_eng(t.get('name'))]
            search_keys += [e.get('name').lower() for e in used_in]
            search_keys.append(task.get('content'))  # add taskname to search keywords
            jobs[entity_id]['keywords'] = search_keys

        # remove job with all task approved
        filtered_jobs = OrderedDict()
        for jid, data in jobs.items():
            job_tasks = data['tasks']
            for task in job_tasks:
                task_status = task.get('sg_status_list')
                if task_status != 'apr':
                    filtered_jobs[jid] = data
                    break

        # sort jobs
        self.jobs = self.sort_jobs(jobs=filtered_jobs)

        # add to UI
        loaded = False
        for entity_id, data in self.jobs.items():
            self.job_listwidget.add_entity(data=data)
            if not loaded:
                loaded = True
                self.job_listwidget.set_loading(False)

        # filter
        self.filter_display()

        QtWidgets.QApplication.restoreOverrideCursor()
        num_job = len(self.jobs)
        job_txt = 'Found {} jobs(s).'.format(num_job) if num_job else 'No job found.'
        self.show_status(job_txt, level='success')
        self.job_listwidget.set_loading(False)
        self.setEnabled(True)

    def sort_jobs(self, jobs):
        '''
        Jobs will be sorted using this order
          1. Directly assigned tasks
            1.1 Due tasks  - sort by due date, early due date comes first
            1.2 Non-due tasks  - sort by updated date, recent update comes first
          2. Involved tasks
            2.1 Due tasks  - sort by due date, early due date comes first
            2.2 Non-due tasks  - sort by updated date, recent update comes first
        '''
        now = datetime.now().replace(tzinfo=None)
        # separate directly assigned and involve jobs
        assigned_jobs = list()
        involved_jobs = list()
        for entity_id, data in jobs.items():
            job_tasks = data['tasks']
            # set overall updated date for entity
            update_dates = [t.get('updated_at') for t in job_tasks if t.get('updated_at')]
            if update_dates:
                min_updated_at = max(update_dates)
                data['updated_at'] = min_updated_at

            if data['is_assigned']:
                assigned_jobs.append((entity_id, data))
            else:
                involved_jobs.append((entity_id, data))

        results = list()
        for jobs in (assigned_jobs, involved_jobs):
            due_jobs = list()
            non_due_jobs = list()
            for entity_id, data in jobs:
                job_tasks = data['tasks']
                # set overall due date for the entity
                due_dates = [datetime.strptime(t.get('due_date'), '%Y-%m-%d').date() for t in job_tasks if t.get('due_date') and t.get('sg_status_list')!='apr']
                if due_dates:
                    min_due_date = min(due_dates)
                    data['due_date'] = min_due_date
                    due_jobs.append((entity_id, data))
                else:
                    non_due_jobs.append((entity_id, data))
            due_jobs = sorted(due_jobs, key=lambda i: (i[1].get('due_date') - now.date()).days)
            non_due_jobs = sorted(non_due_jobs, key=lambda i: (i[1].get('updated_at').replace(tzinfo=None) - now), reverse=True)  # sort non due job by updated date
            results += due_jobs + non_due_jobs
        return OrderedDict(results)

    # ----- global funcs
    def user_changed(self):
        self.clear_header()
        self.clear_job_display()
        self.clear_feed_display()
        self.clear_media_display()
        self.clear_cache()
        user = self.get_current_user()
        if not user:
            self.show_status('Invalid user selection!', level='error')
            return

        image_path = get_photo_path(user)
        self.user_photo_label.update_photo(image_path)

        self.fetch_jobs(user=user)

    def username_edited(self):
        self.clear_header()
        self.clear_job_display()
        self.clear_feed_display()
        self.clear_media_display()
        self.clear_cache()

    def get_current_user(self):
        curr_text = self.user_widget.comboBox.currentText()
        all_texts = [self.user_widget.comboBox.itemText(i) for i in range(self.user_widget.comboBox.count())]
        if curr_text not in all_texts:
            return
        return self.user_widget.comboBox.itemData(all_texts.index(curr_text), QtCore.Qt.UserRole)

    def clear_cache(self):
        self.feed_cache = {}
        self.feed_treewidget.clear_cache()
        self.media_cache = {}
        self.media_widget.clear_all_pixmaps()

    def refresh_user_tasks(self):
        self.clear_job_display()
        self.clear_feed_display()
        self.clear_media_display()
        self.clear_cache()

        # refetch
        user = self.get_current_user()
        if not user:
            self.show_status('No user found!', level='error')
            return
        self.fetch_jobs(user=user)

    def clear_header(self):
        self.user_photo_label.reset()

    def clear_job_display(self):
        self.job_listwidget.clear()

    def clear_feed_display(self):
        self.feed_treewidget.clear()

    def clear_media_display(self):
        self.media_widget.clear()

    def clear_media_cache(self, entity_id=None):
        if not entity_id:
            item = self.get_current_job_item()
            if not item:
                return
            entity = item.data(QtCore.Qt.UserRole)
            entity_id = entity.get('id')

        if entity_id in self.media_cache:
            del self.media_cache[entity_id]

    def clear_feed_cache(self, entity_id=None):
        if not entity_id:
            item = self.get_current_job_item()
            if not item:
                return
            entity = item.data(QtCore.Qt.UserRole)
            entity_id = entity.get('id')

        if entity_id in self.feed_cache:
            del self.feed_cache[entity_id]

    def get_current_job_item(self):
        selected_items = self.job_listwidget.selectedItems()
        if not selected_items:
            return
        item = selected_items[0]
        return item

    def show_status(self, message, level='normal'):
        level_dict = {'normal': StyleConfig.white, 
                    'warning': StyleConfig.yellow, 
                    'success': StyleConfig.green, 
                    'error': StyleConfig.red}
        self.statusBar.showMessage(message)
        self.statusBar.setStyleSheet('color: rgb{}; font: italic;'.format(level_dict.get(level, StyleConfig.white)))

def pretty_date(time, limit_date=False):
    """
    Get a datetime object or a int() Epoch timestamp and return a
    pretty string like 'an hour ago', 'Yesterday', '3 months ago',
    'just now', etc
    """
    if isinstance(time, date):
        now = datetime.today().date()
    if isinstance(time, datetime):
        now = datetime.now()
        time = time.replace(tzinfo=None)
    diff = now - time
    second_diff = diff.seconds
    day_diff = diff.days
    
    if day_diff < 0:  # it's future
        is_future = True
    else:  # it's in the past
        is_future = False

    abs_day_diff = abs(day_diff)
    # within a day
    if abs_day_diff == 0:
        if second_diff < 10:
            return "Just now"
        if second_diff < 60:
            result = str(second_diff) + " seconds"
        elif second_diff < 120:
            result = "1 minute"
        elif second_diff < 3600:
            result = str(second_diff // 60) + " minutes"
        elif second_diff < 7200:
            result = "1 hour"
        else:
            num_hour = second_diff // 3600
            result = str(num_hour) + " hours" if num_hour > 1 else str(num_hour) + ' hour'
    else:
        if abs_day_diff == 1:
            num_hour = second_diff // 3600
            if num_hour > 12:
                result = '2 days'
            else:
                return "Tomorrow" if is_future else "Yesterday"
        elif limit_date:
            date_format = DATE_FORMAT if time.year == now.year else DATE_FORMAT_YEAR
            return '{} at {}'.format(time.strftime(date_format), time.strftime(TIME_FORMAT)).lstrip("0").replace(" 0", " ")
        elif abs_day_diff < 7:
            result = str(abs_day_diff) + " days" 
        elif abs_day_diff < 31:
            num_week = abs_day_diff // 7
            result = str(num_week) + " weeks" if num_week > 1 else str(num_week) + ' week'
        elif abs_day_diff < 365:
            num_month = abs_day_diff // 30
            result = str(num_month) + " months" if num_month > 1 else str(num_month) + ' month'
        else: 
            num_year = abs_day_diff // 365
            result = str(num_year) + " years" if num_year > 1 else str(num_year) + ' year'

    return 'In ' + result if is_future else result[0].upper()+result[1:] + ' ago'

def get_photo_path(user):
    user_id = user.get('id')
    user_type = user.get('type')
    photo_path = UNKNOWN_PERSON_ICON
    if user_type == 'HumanUser':
        if user_id in USER_PHOTO_CACHE:
            photo_path = USER_PHOTO_CACHE[user_id]
        else:
            users = [u for u in user_info.SGUser().userEntities if u.get('id')==user_id]
            image_link = ''
            if users:
                image_link = users[0].get('image')

            # download user photo
            if image_link:
                fh, photo_path = tempfile.mkstemp(suffix='.jpg')
                photo_path = photo_path.replace('\\', '/')
                os.close(fh)
                urlretrieve(image_link, photo_path)
                USER_PHOTO_CACHE[user_id] = photo_path
                GLOBAL_TEMP.add(photo_path)
    elif user_type == 'ClientUser':
        photo_path = CLIENT_PROFILE

    return photo_path

def is_eng(text):
    try:
        text.encode(encoding='utf-8').decode('ascii')
        return True
    except UnicodeDecodeError:
        return False

def show():
    app = QtWidgets.QApplication(sys.argv)
    myApp = BriefBoard()
    myApp.show()
    stylesheet.set_default(app)
    sys.exit(app.exec_())

if __name__ == '__main__':
    show()

'''
from rf_utils.pipeline import test_create_entity
reload(test_create_entity)

test_create_entity.asset()
'''