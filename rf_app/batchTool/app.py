# v.0.0.1 polytag switcher
_title = 'Riff batch file'
_version = 'v.0.0.1'
_des = 'wip'
uiName = 'batchToolUI'

#Import python modules
import sys
import os
import getpass
import logging
from functools import partial
from collections import OrderedDict
import json
from datetime import datetime

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

# config file
config_file = '%s/local_config.json' % moduleDir
with open(config_file) as configData:
    localConfig = json.load(configData)

SCRIPT = localConfig['SCRIPT']
ROOT = os.environ.get(SCRIPT)
core = localConfig['CORE']

# import config
sys.path.append('%s/%s' % (ROOT, core))
import rf_config as config

if config.isMaya:
    import maya.cmds as mc
    import maya.mel as mm

# framework modules
from rf_utils import log_utils
from rf_utils.ui import load
from rf_utils.ui import stylesheet
from rf_utils.deadline import batch_tool_utils

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

import ui
reload(ui)
import exportbatch
reload(exportbatch)
reload(batch_tool_utils)
# thread

class BatchTool(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        #Setup Window
        super(BatchTool, self).__init__(parent)

        # ui read
        self.ui = ui.batchToolUI(parent)
        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)
        self.resize(360, 500)
        
        self.batRoot = 'P:/SevenChickMovie/_data/batch'

        # title
        self.ui.show()
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.list_command()
        self.init_signals()
        self.init_functions()

    def init_signals(self):
        self.ui.run_pushButton.clicked.connect(self.export_batch)
        self.ui.runQueue_pushButton.clicked.connect(partial(self.export_batch, True))
        self.ui.browse_pushButton.clicked.connect(self.browsefolder)
        self.ui.inputLineEdit.returnPressed.connect(self.add_input)
        self.ui.bat_pushButton.clicked.connect(self.create_bat)

    def init_functions(self): 
        self.ui.browse_pushButton.setVisible(False)
        self.list_interpreter()

    def list_interpreter(self): 
        mayaDir = 'C:/Program Files/Autodesk'
        dirs = [a for a in os.listdir(mayaDir) if os.path.isdir('%s/%s' % (mayaDir, a))]
        mayaVersions = [a for a in dirs if 'Maya' == a[:4] and a[4:].isdigit()]
        defaultIndex = 0
        self.ui.interpreterComboBox.clear()
        currentVer = ''

        if config.isMaya: 
            currentVer = mc.about(q=True, v=True)

        for index, ver in enumerate(mayaVersions): 
            mayaPyPath = '%s/%s/bin/mayapy.exe' % (mayaDir, ver)
            self.ui.interpreterComboBox.addItem(mayaPyPath)

            if currentVer in ver: 
                defaultIndex = index

        self.ui.interpreterComboBox.setCurrentIndex(defaultIndex)

    def list_command(self):
        exceptions = ['__init__.py']
        self.command_path = os.path.join(moduleDir, 'command').replace('\\', '/')
        listPython_file = os.listdir(self.command_path)
        pys = [a for a in listPython_file if not a in exceptions and os.path.splitext(a)[-1] in ['.py']]
        self.ui.comboBox_pyFile.clear()

        for i, py in enumerate(pys): 
            path = '%s/%s' % (self.command_path, py)
            self.ui.comboBox_pyFile.addItem(py)
            self.ui.comboBox_pyFile.setItemData(i, path)
            
        # self.ui.comboBox_pyFile.setCurrentIndex(1)

    def export_batch(self, queue=False):
        python_file = self.ui.comboBox_pyFile.currentText()
        pythonFile_path = os.path.join(self.command_path, python_file).replace('\\', '/')
        srcFiles = self.ui.srcFile_listWidget.selectedItems()
        interpreter = str(self.ui.interpreterComboBox.currentText())
        

        if srcFiles: 
        
            for file in srcFiles:
                fileName = file.text()
                cmd = [interpreter]
                cmd.append(pythonFile_path)
                cmd.append(fileName)
                
                if queue:
                    batch_tool_utils.process_send_to_farm(cmd)
                else:
                    exportbatch.export_batch(interpreter, pythonFile_path, fileName)


    def create_bat(self): 
        pythonFile_path = str(self.ui.comboBox_pyFile.currentText())
        data = self.generate_bat()
        if data: 
            batFile = self.write_bat(pythonFile_path, data)
            logger.info('Bat file created %s' % batFile)


    def generate_bat(self): 
        cmds = []
        interpreter = str(self.ui.interpreterComboBox.currentText())
        srcFiles = self.ui.srcFile_listWidget.selectedItems()
        pythonFile_path = self.ui.comboBox_pyFile.itemData(self.ui.comboBox_pyFile.currentIndex(), QtCore.Qt.UserRole)
        
        interpreter = interpreter.replace('/', '\\')
        pythonFile_path = pythonFile_path.replace('/', '\\')

        if srcFiles: 
            for item in srcFiles: 
                file = str(item.text())
                fileName = file.replace('/', '\\')
                cmds.append('"%s" "%s" "%s"' % (interpreter, pythonFile_path, fileName))

            cmdStr = '\n'.join(cmds)
            return cmdStr

    def write_bat(self, python_file, data): 
        timeFormat = datetime.now().strftime('%Y_%m_%d-%H_%M_%S')
        batFile = '%s/%s_%s.bat' % (self.batRoot, os.path.splitext(python_file)[0], timeFormat)

        with open(batFile, 'w') as f:
            f.write(data)

        return batFile



    # def browsefile(self):
    #     fname = QtWidgets.QFileDialog.getOpenFileName()
    #     print fname[0]
    #     self.item = QtWidgets.QListWidgetItem()
    #     self.item.setText(fname[0])
    #     self.ui.srcFile_listWidget.addItem(self.item)

    def browsefolder(self):
        seq_folder = QtWidgets.QFileDialog.getExistingDirectory()
        file_in_seq = os.listdir(seq_folder)

        if 'all' in file_in_seq:
            file_in_seq.remove('all')
        if 'playblast' in file_in_seq:
            file_in_seq.remove('playblast')
        print file_in_seq

        for file in file_in_seq:
            fPath = file + '/finalCam'+'/maya'+'/work'
            folder_maya_path = os.path.join(seq_folder, fPath).replace('\\','/')      
            list_file_maya = os.listdir(folder_maya_path)
            
            if list_file_maya:
                list_file_sorted = sorted(list_file_maya)
                file_lastest = list_file_sorted[-1]
                file_lastest_path = os.path.join(folder_maya_path, file_lastest).replace('\\', '/')
                self.item = QtWidgets.QListWidgetItem()
                self.item.setText(file_lastest_path)
                self.ui.srcFile_listWidget.addItem(self.item)

    def add_input(self): 
        path = str(self.ui.inputLineEdit.text())
        if os.path.exists(path): 
            self.ui.srcFile_listWidget.addItem(path)
            self.ui.inputLineEdit.clear()

        else: 
            logger.warning('Path "%s" not exists' % path)


def show():
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        maya_win.deleteUI(uiName)
        myApp = BatchTool(maya_win.getMayaWindow())
        myApp.show()
        return myApp
    else:
        logger.info('Run in batchTool\n')
        app = QtWidgets.QApplication.instance()
        if not app:
            app = QtWidgets.QApplication(sys.argv)
        myApp = BatchTool()
        myApp.show()
        # stylesheet.set_default(app)
        sys.exit(app.exec_())
if __name__ == '__main__':
    show()



# from rf_app.model.smooth_data import app
# reload(app)
# reload(app.ui)
# app.show()