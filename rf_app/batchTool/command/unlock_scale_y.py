import sys
import os 

import pymel.core as pm 
import maya.cmds as mc 

from rf_utils.context import context_info
from rf_utils import file_utils
from rf_app.publish.scene import app
reload(app)
from rf_utils.context import context_info

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())


def main() :
    srcFile = sys.argv[1]
    print 'open', srcFile

    mc.file(srcFile, o=True,f=True)
    mc.setAttr("AllMover_Ctrl.sy", lock=0)
    mc.file(rename=srcFile)
    mc.file(save=True, type='mayaBinary',f=True)


main()

