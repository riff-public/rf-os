# System modules
import sys
import os
import logging

core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)

import pymel.core as pm
import maya.cmds as mc

from rf_utils.context import context_info
from rf_app.publish.scene import app
from rftool.utils import pipeline_utils
reload(app)
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
moduleDir = os.path.dirname(sys.modules[__name__].__file__)


def main() :
    # args 
    srcFile = sys.argv[1]
    print "srcFile: ", srcFile
    # open file 
    mc.file(srcFile, o=True, f=True, prompt=False)

    # scene context 
    switch_yeti()
    app.batch()

def switch_yeti(): 
    switchDict = get_hair_still()
    message = check_all_asset_ready(switchDict)
    switch = True
    if message: 
        switch = True if result == 'Continue Switch' else False 
            
    if switch: 
        # switch
        for name, data in switchDict.iteritems(): 
            dstPath = data['hairStill']
            rnNode = data['rnNode']
            if os.path.exists(dstPath): 
                mc.file(dstPath, loadReference=rnNode, prompt=False)
    else: 
        print 'cannot switch %s' % message

def get_hair_still(): 
    scene = context_info.ContextPathInfo()
    refPaths = pipeline_utils.collect_asset_by_path(scene)
    mapDict = dict()

    for refPath in refPaths: 
        # rnNode 
        rnNode = mc.referenceQuery(refPath, referenceNode=True)
        asset = context_info.ContextPathInfo(path=refPath)
        asset.context.update(res='md')
        name = asset.library(key=context_info.Lib.rigGrm)
        heroPath = asset.path.hero().abs_path()
        switchPath = '%s/%s' % (heroPath, name)
        mapDict[asset.name] = {'asset': asset, 'rnNode': rnNode, 'hairStill': switchPath, 'status': os.path.exists(switchPath)}

    return mapDict

def check_all_asset_ready(switchDict): 
    # check if all hair still exists 
    result = []
    missingAssets = []
    validAssets = []
    message = ''
    for name, data in switchDict.iteritems(): 
        missingAssets.append(name) if not data['status'] else validAssets.append(name)

    print ('\n- ').join(missingAssets)
    if missingAssets: 
        message = '%s/%s Hair assets not ready Cache' % (len(missingAssets), len(switchDict.keys()))
        message += '\n- %s\n' % ('\n- ').join(missingAssets)
        message += '\nContact P.Tai or P.Nong'

    return message
   
main()

# command 
# C:\Program Files\Autodesk\Maya2017\bin>mayapy.exe "D:\Dropbox\script_server\core\rf_app\batchTool\command\export_cache.py" "P:/SevenChickMovie/scene/work/dev/scm_dev_arthur_test/anim/main/maya/scm_dev_arthur_test_anim_main.v005.TA.ma"