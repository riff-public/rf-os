import sys
import os 
import pymel.core as pm 
import maya.cmds as mc 

import rf_config as config
from rf_utils.context import context_info
from rf_utils import file_utils
from rf_app.publish.scene import app
reload(app)
from rf_utils.context import context_info


def main(): 
	path = sys.argv[1]
	result = mc.file(path, o=True, f=True, prompt=False)
	print 'Open scene complete'
	print result 
	scene = context_info.ContextPathInfo()

	print 'start caching'
	# exportData = app.get_export_dict(scene, scene.name_code, ['camera', 'alembic_cache', 'shotdress'])
	exportData = app.get_export_dict(scene, scene.name_code, [])
	print 'exporting ...'
	app.do_export(scene, itemDatas=exportData, uiMode=False, publish=True)
	print 'finish'
	print '==============='

main()
