# System modules
import sys
import os
import logging

import pymel.core as pm 
import maya.cmds as mc 

from rf_utils.context import context_info 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
moduleDir = os.path.dirname(sys.modules[__name__].__file__)

core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)

def find_dst(srcFile) :
    from rf_utils.context import context_info 
    context = context_info.Context()
    context.use_path(path=srcFile)
    scene= context_info.ContextPathInfo(context=context)
    scene.context.update(entity='all')
    scene.context.update(step='camera')
    file_name = "%s_%s_%s.ma"%(scene.sequence, scene.project_code, scene.step)
    app_flie = scene.path.step().abs_path()
    if not os.path.exists(app_flie):
        os.makedirs(app_flie)

    return os.path.join(app_flie, file_name).replace('\\', '/')

def main() :
    srcFile = sys.argv[1]
    dstFile = find_dst(srcFile)

    print 'open', srcFile
    print 'export', dstFile

    try: 
        mc.file(srcFile, o=True, f=True, loadNoReferences=True)
    except RuntimeError: 
        pass 
    print 'open file ..............................................'
    if mc.objExists('cam_grp'): 
        print 'cam found ..........................'
        mc.select('cam_grp')
        print 'obj found ..........................'
        mc.file(dstFile, es=True, type='mayaAscii', f=True)
        print 'obj export ..........................'
main()


