# System modules
import sys
import os
import logging

import pymel.core as pm 
import maya.cmds as mc 
import maya.mel as mm

import pdb
from rf_utils.context import context_info 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
moduleDir = os.path.dirname(sys.modules[__name__].__file__)

core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)

def find_dst(srcFile) :
    cam_export_path ='P:/CloudMaker/scene/film001/camera'
    from rf_utils.context import context_info 
    context = context_info.Context()
    context.use_path(path=srcFile.replace('\\', '/'))
    scene= context_info.ContextPathInfo(context=context)
    scene.context.update(entity='all')
    scene.context.update(step='camera')
    file_name = "%s_pattern_%s_%s.fbx"%(scene.sequence, scene.name_code, scene.step)
    export_path = os.path.join(cam_export_path,'{seq_folder}/{shot_folder}'.format(seq_folder =scene.sequence, shot_folder= scene.name_code)).replace('\\', '/')
    if not os.path.exists(export_path):
        os.makedirs(export_path)
    return os.path.join(export_path,file_name).replace('\\', '/')

def bake_anim_cam(dstFile):
    ##get time from timeslider
    start = pm.playbackOptions(min=True,q=True)
    end = pm.playbackOptions(max=True,q=True)
    ### list cameara non-startup
    cameras = pm.ls(type=('camera'), l=True)
    startup_cameras = [camera for camera in cameras if pm.camera(camera.parent(0), startupCamera=True, q=True)]
    non_startup_cameras_pynodes = list(set(cameras) - set(startup_cameras))
    ######### list_cam 
    export_list = []
    for cam in non_startup_cameras_pynodes:
        for a in pm.listAttr(cam.getParent()):
            cam.getParent().attr(a).unlock()
        cam_tranform = cam.getParent().name()
        mc.bakeResults(cam_tranform, simulation=True, sb=1, t=(start, end))
        # mc.bakeSimulation(cam_tranform, sb=1, t=(start, end))
        loc = bake_locator(cam_tranform)

        file_name = os.path.basename(dstFile)
        dir_path = os.path.dirname(dstFile)
        new_file_name = file_name.replace(cam.getParent().name(), 'pattern')
        new_dstFile = os.path.join(dir_path, new_file_name)
        pm.select([cam_tranform, loc])
        # mc.file(new_dstFile, es=True, f=True)
        pm.mel.FBXExport(f=new_dstFile, s=True)
        # mm.eval("FBXExport -f "%s" -s" % new_dstFile)
        export_list.append(cam_tranform)

    return export_list

def bake_locator(cam_node):
    start = pm.playbackOptions(min=True,q=True)
    end = pm.playbackOptions(max=True,q=True)

    loc = mc.spaceLocator(n='loc_{}'.format(cam_node))
    tempPointConstraint = mc.pointConstraint(cam_node, loc, maintainOffset = False)
    mc.bakeResults(loc, simulation=True, sb=1, t=(start, end))
    # mc.bakeSimulation(cam_node, sb=1, t=(start, end))
    # pm.delete(tempPointConstraint)
    return loc

def main() :
    srcFile = sys.argv[1]
    dstFile = find_dst(srcFile)

    pm.loadPlugin("fbxmaya")
    print 'open', srcFile
    print 'export', dstFile

    try: 
        mc.file(srcFile, o=True, f=True)
    except RuntimeError: 
        pass 
    print 'open file ..............................................'
    export_list = bake_anim_cam(dstFile)
    print 'path Export : {}'.format(export_list)
    print 'Finished ....................................'

main()

