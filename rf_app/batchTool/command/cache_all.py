import sys
import os 

import pymel.core as pm 
import maya.cmds as mc 

from rf_utils.context import context_info
from rf_utils import file_utils
from rf_app.publish.scene import app
reload(app)
from rf_utils.context import context_info

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())


def main(): 
	logger.debug('__name__ : %s' % __name__)
	path = sys.argv[1]
	logger.info('input file %s' % path)
	
	result = mc.file(path, o=True, f=True, prompt=False)
	logger.debug('Open scene completed')
	scene = context_info.ContextPathInfo()

	logger.info('Start caching ...')
	# exportData = app.get_export_dict(scene, scene.name_code, ['camera', 'alembic_cache', 'shotdress'])
	exportData = app.get_export_dict(scene, scene.name_code, [])
	# logger.debug('exportData')
	# logger.debug(exportData)
	app.do_export(scene, itemDatas=exportData, uiMode=False, publish=True)
	logger.info('Finished')
	logger.info('=================')

main()
