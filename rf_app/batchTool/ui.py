# -*- coding: utf-8 -*-
import os
import sys

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

class batchToolUI(QtWidgets.QWidget):
    def __init__(self, parent=None) :
        super(batchToolUI, self).__init__(parent)

        self.allLayout = QtWidgets.QVBoxLayout()
        self.mainLayout = QtWidgets.QVBoxLayout()

        self.interpreterLabel = QtWidgets.QLabel('Interpreter: ')
        self.interpreterComboBox = QtWidgets.QComboBox()
        self.mainLayout.addWidget(self.interpreterLabel)
        self.mainLayout.addWidget(self.interpreterComboBox)
        
        self.label = QtWidgets.QLabel('Operation: ')
        self.mainLayout.addWidget(self.label)
        
        self.comboBox_pyFile = QtWidgets.QComboBox()
        self.mainLayout.addWidget(self.comboBox_pyFile)

        self.argLabel = QtWidgets.QLabel('**Kwargs')
        self.argLineEdit = QtWidgets.QLineEdit()
        self.mainLayout.addWidget(self.argLabel)
        self.mainLayout.addWidget(self.argLineEdit)
        
        self.label_srcFile = QtWidgets.QLabel('SourceFile: ')
        self.mainLayout.addWidget(self.label_srcFile)
        
        self.srcFile_listWidget = QtWidgets.QListWidget()
        self.srcFile_listWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.mainLayout.addWidget(self.srcFile_listWidget)

        self.inputLineEdit = QtWidgets.QLineEdit()
        self.mainLayout.addWidget(self.inputLineEdit)


        self.buttonLayout = QtWidgets.QVBoxLayout()
        self.browse_pushButton = QtWidgets.QPushButton('Browse')
        self.run_pushButton = QtWidgets.QPushButton('Run')
        self.bat_pushButton = QtWidgets.QPushButton('Create .bat')
        self.runQueue_pushButton = QtWidgets.QPushButton('Queue')
        self.buttonLayout.addWidget(self.browse_pushButton)
        self.buttonLayout.addWidget(self.run_pushButton)
        self.buttonLayout.addWidget(self.bat_pushButton)
        self.buttonLayout.addWidget(self.runQueue_pushButton)
        self.mainLayout.addLayout(self.buttonLayout)
        self.allLayout.addLayout(self.mainLayout)
        self.setLayout(self.allLayout)


