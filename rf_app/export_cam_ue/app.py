# v.0.0.1 polytag switcher
_title = 'ExportCameraUE'
_version = 'v.0.0.1'
_des = 'wip'
uiName = 'ExportCameraUE'
# dev 
#Import python modules
import sys
import os 
import json 
import getpass

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]


import rf_config as config


from rf_utils import log_utils

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)


from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

from rf_utils.widget import browse_widget

import maya.cmds as mc
import pymel.core as pm
import maya.mel as mel
from rf_maya.rftool.utils import maya_utils

class Ui(QtWidgets.QWidget):
    def __init__(self, parent=None) :
        super(Ui, self).__init__(parent)
        self.mainVerticalLayout = QtWidgets.QVBoxLayout()
       
        self.horizontalLayout = QtWidgets.QHBoxLayout()
       
        self.path_label = QtWidgets.QLabel("Path :")

        self.path_lineEdit = QtWidgets.QLineEdit("")
        self.browse_pushButton = QtWidgets.QPushButton("Browse")
       
        self.horizontalLayout.addWidget(self.path_label)
        self.horizontalLayout.addWidget(self.path_lineEdit)
        self.horizontalLayout.addWidget(self.browse_pushButton)

        self.horizontalLayout_name = QtWidgets.QHBoxLayout()
        self.name_label = QtWidgets.QLabel("Camera export name :")
        self.name_lineEdit = QtWidgets.QLineEdit("camera_UE")

        self.horizontalLayout_name.addWidget(self.name_label)
        self.horizontalLayout_name.addWidget(self.name_lineEdit)

        line = QtWidgets.QFrame(parent)
        line.setFrameShape(QtWidgets.QFrame.HLine)
        line.setFrameShadow(QtWidgets.QFrame.Sunken)

        self.horizontalLayout_maintain = QtWidgets.QHBoxLayout()
        self.maintain_checkBox = QtWidgets.QRadioButton("Maintain shot gap")
        # self.maintain_label = QtWidgets.QLabel("Maintain shot gap")
        spacerItem_maintain = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        self.horizontalLayout_maintain.addWidget(self.maintain_checkBox)
        # self.horizontalLayout_maintain.addWidget(self.maintain_label)
        self.horizontalLayout_maintain.addItem(spacerItem_maintain)


        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.adjust_checkBox = QtWidgets.QRadioButton("Adjust shot gap to")
        # self.adjust_label = QtWidgets.QLabel("Adjust shot gap to")
        self.shotgap_lineEdit = QtWidgets.QLineEdit("1")
        self.shotgap_lineEdit.setValidator(QtGui.QIntValidator())
        self.shotgap_lineEdit.setFixedWidth(20)
        self.adjust_label_2 = QtWidgets.QLabel("frames")

        spacerItem_adjust = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        self.horizontalLayout_2.addWidget(self.adjust_checkBox)
        # self.horizontalLayout_2.addWidget(self.adjust_label)
        self.horizontalLayout_2.addWidget(self.shotgap_lineEdit)
        self.horizontalLayout_2.addWidget(self.adjust_label_2)
        self.horizontalLayout_2.addItem(spacerItem_adjust)

        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.start_label = QtWidgets.QLabel("Start at frame : ")
        self.start_lineEdit = QtWidgets.QLineEdit("1001")
        self.start_lineEdit.setValidator(QtGui.QIntValidator())
        self.start_lineEdit.setFixedWidth(50)

        spacerItem_2 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        self.horizontalLayout_3.addWidget(self.start_label)
        self.horizontalLayout_3.addWidget(self.start_lineEdit)
        self.horizontalLayout_3.addItem(spacerItem_2)

        self.export_pushButton = QtWidgets.QPushButton("Export")

        self.mainVerticalLayout.addLayout(self.horizontalLayout)
        self.mainVerticalLayout.addLayout(self.horizontalLayout_name)
        self.mainVerticalLayout.addWidget(line)
        self.mainVerticalLayout.addLayout(self.horizontalLayout_maintain)
        self.mainVerticalLayout.addLayout(self.horizontalLayout_2)
        self.mainVerticalLayout.addLayout(self.horizontalLayout_3)
        self.mainVerticalLayout.addWidget(self.export_pushButton)

        self.centralwidget = QtWidgets.QHBoxLayout()
        self.centralwidget.addLayout(self.mainVerticalLayout)
        self.setLayout(self.centralwidget)

class ExportCameraUE(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
    #Setup Window
        super(ExportCameraUE, self).__init__(parent)
        # ui read
        self.ui = Ui(parent)
        self.resize(350, 150) 

        self.init_signals()
        self.ui.maintain_checkBox.setChecked(True)

        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))

        self.show()

    def init_signals(self):
        self.ui.maintain_checkBox.toggled.connect(self.setChecked)
        self.ui.adjust_checkBox.toggled.connect(self.setChecked)
        self.ui.browse_pushButton.clicked.connect(self.browsePath)
        self.ui.export_pushButton.clicked.connect(self.export)

        self.ui.start_lineEdit.textChanged.connect(self.checkMinStartFrame)
        self.ui.shotgap_lineEdit.textChanged.connect(self.checkMinGap)


    def setChecked(self):
        if self.ui.maintain_checkBox.isChecked():

            self.ui.shotgap_lineEdit.setEnabled(False)


        elif self.ui.adjust_checkBox.isChecked():

            self.ui.shotgap_lineEdit.setEnabled(True)


    def checkMinStartFrame(self):
        startFrame =  self.ui.start_lineEdit.text()
        if startFrame:
            try:
                startFrame = int(startFrame)
            except:
                self.ui.start_lineEdit.setText('0')
            if startFrame < 0 :
                self.ui.start_lineEdit.setText('0')

    def checkMinGap(self):
        gap =  self.ui.shotgap_lineEdit.text()
        if gap:
            try:
                gap = int(gap)
            except:
                self.ui.shotgap_lineEdit.setText('1')
            if gap < 1 :
                self.ui.shotgap_lineEdit.setText('1')

    def browsePath(self):
        result = QtWidgets.QFileDialog.getExistingDirectory(self, 'Directory Browser', '', QtWidgets.QFileDialog.ShowDirsOnly)
        path = result
        if path:
            self.set_path(path)

    def set_path(self, path):
        self.ui.path_lineEdit.setText(path)

    def export(self):
        if not mc.pluginInfo('fbxmaya', query=True, loaded=True):
            mc.loadPlugin('fbxmaya')

        selShots = mc.ls(sl=True, type='shot')
        if not selShots:
            selShots = mc.sequenceManager(lsh=True) or []
        selShots = sorted(selShots)
        startFrame =  int(self.ui.start_lineEdit.text())
        shotGap = int(self.ui.shotgap_lineEdit.text())

        maintain = self.ui.maintain_checkBox.isChecked()

        path = self.ui.path_lineEdit.text()
        cameraName = self.ui.name_lineEdit.text()

        if path and selShots and cameraName:
            if os.path.exists(path):
                camName = cameraName + '.fbx'

                path = os.path.join(path, camName)
                camera_ue = self.create_camera_ue(cameraName=cameraName, startFrame=startFrame, shotGap=shotGap, shots=selShots, maintain=maintain)
                self.export_fbx(path, camera_ue)

                QtWidgets.QMessageBox.information(self, "Success", "Export camera to FBX complete")
            else:
                QtWidgets.QMessageBox.critical(self, "Error", "Path does not exists.")

        else:
            QtWidgets.QMessageBox.critical(self, "Error", "Please fill path or camera name.")

    def create_camera_ue(self, cameraName='', startFrame=1001, shotGap=1, shots=[], maintain=True):
        if mc.objExists(cameraName):
            mc.delete(cameraName)

        newCam = mc.camera()
        mc.rename(newCam[0], cameraName)

        for sel in shots:
            cam = mc.shot(sel,q=True,currentCamera=True)
            if mc.objectType( cam, isType='camera'):
                cam = mc.listRelatives(cam , p=True)[0]

            camShape = '%sShape'% cam

            start = mc.shot(sel,q=True,startTime=True)
            end = mc.shot(sel,q=True,endTime=True)

            if sel != shots[0]:
                if maintain:
                    startGap = mc.shot(sel,q=True,startTime=True)
                    shotGap = startGap - endGap

                startFrame = endFrame + shotGap

            duration = end - start

            tempCam = 'camera_temp'
            tempCamShape = 'camera_tempShape'
            temp = mc.duplicate(cam, n=tempCam)

            attrCam = mc.listAttr( tempCam,locked=True)
            if attrCam:
                for attr in attrCam:
                    mc.setAttr("%s.%s"% (tempCam,attr) , l=0)

            attrCamShape = mc.listAttr( tempCamShape ,locked=True)
            if attrCamShape:
                for attr in attrCamShape:
                    mc.setAttr("%s.%s"% (tempCamShape,attr) , l=0)
                    mc.connectAttr( "%s.%s"% (camShape,attr), "%s.%s"% (tempCamShape,attr))

            mc.parentConstraint(cam, tempCam, maintainOffset=False)

            # bake
            cmd = '''bakeResults -simulation true 
                        -t "{0}:{1}" 
                        -sampleBy 1 
                        -oversamplingRate 1 
                        -disableImplicitControl true 
                        -preserveOutsideKeys true 
                        -sparseAnimCurveBake false 
                        -removeBakedAttributeFromLayer false 
                        -removeBakedAnimFromLayer false 
                        -bakeOnOverrideLayer false 
                        -minimizeRotation true 
                        -controlPoints false 
                        -shape true "{2}";'''.format(start, end, tempCam)
            with maya_utils.FreezeViewport() as isolate:
                mel.eval(cmd)

            endFrame = startFrame + duration

            mc.copyKey(tempCam, time=(start,end))
            mc.pasteKey(cameraName, time=(startFrame,endFrame),option='replace')

            mc.delete(tempCam)

            endGap = mc.shot(sel,q=True,endTime=True)

        return cameraName


    def export_fbx(self, path, obj):
        mc.select(clear=True)
        mc.select(obj)
        mc.file(path, es=True, f=True, type='FBX export')




def show():
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        maya_win.deleteUI(uiName)
        myApp = ExportCameraUE(maya_win.getMayaWindow())
        return myApp

    else: 
        logger.info('Run in Standalone\n')
        app = QtWidgets.QApplication.instance()
        if not app: 
            app = QtWidgets.QApplication(sys.argv)
        myApp = ExportCameraUE()
        stylesheet.set_default(app)
        sys.exit(app.exec_())

if __name__ == '__main__': 
    show()