#!/usr/bin/env python
# -*- coding: utf-8 -*-

# beta - under development
# 1.0.0 - initial release
# 1.1.0 - Allow free naming in Shots when "Use Pattern" is unchecked
# 1.2.0 - Fix bug in name lineEdit when copy and pasted into with newlines and whitespaces
# 1.3.0 - Add project permission error
# 1.4.0 - Add "Preproduction" typed asset
# 1.4.1 - Fix prefix name bug in Preproduction asset form
# 1.4.2 - Add FX type to Preproduction form
# 1.5.0 - Add omit step checkboxes
# 1.5.1 - Update UI
# 1.5.2 - Fix tag color bug
# 1.6.0 - Add "Design" omit step
# 1.6.1 - Add project config for per-project naming convention setting

_title = 'Announcer'
_version = '1.6.1'
_des = ''
uiName = 'Announcer'

#Import python modules
import sys
import os
import logging
import getpass
from functools import partial
from datetime import datetime
from collections import OrderedDict

core = '%s/core' % os.environ.get('RFSCRIPT')
if core not in sys.path:
    sys.path.append(core)

# import config
import rf_config as config
from rf_utils import log_utils
from rf_utils.ui import stylesheet

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)

# QT
os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

# pipeline modules
import ui
from rf_utils.sg import sg_process
sg = sg_process.sg
from rf_utils.widget import project_widget
from rf_utils import project_info
from rf_utils import user_info
from rf_utils import file_utils

from rf_utils.pipeline.notification import noti_module_cliq
# reload(noti_module_cliq)

moduleDir = os.path.dirname(sys.modules[__name__].__file__).replace('\\', '/')
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

class CreateThread(QtCore.QThread):
    progress = QtCore.Signal(tuple)
    oneFinished = QtCore.Signal(tuple)
    allFinished = QtCore.Signal(list)

    def __init__(self, func_list, parent=None):
        super(CreateThread, self).__init__(parent=parent)
        self.func_list = func_list
        self.results = []
        self._stop = False

    def stop(self):
        self._stop = True
        return 0

    def run(self):
        num_files = len(self.func_list)
        i = 0
        for func, args in self.func_list:
            self.progress.emit((i+1, num_files, args['name']))
            result = func(**args)
            if self._stop:
                break

            self.results.append(result)
            self.oneFinished.emit((i, result))
            i += 1

        if self._stop:
            self._stop = False
        else:
            self.allFinished.emit(self.results)
        return 0

class Announcer(QtWidgets.QMainWindow):
    ''' Application class '''
    def __init__(self, parent=None):
        # setup Window
        super(Announcer, self).__init__(parent)

        # app vars
        self.thread = None
        self.creation_base_types = []

        # ui vars
        self.w = 625
        self.h = 800
        self.button_w = 53
        self.button_h = 53
        self.icon_w = 48
        self.icon_h = 48
        self.button_stroke_size = 2
        # icons
        self.app_icon = '{}/icons/app_icon.png'.format(moduleDir)
        self.logo_icon = '{}/icons/riff_logo.png'.format(moduleDir)
        self.clear_icon = '{}/icons/clear_icon.png'.format(moduleDir)
        self.delete_icon = '{}/icons/delete_icon.png'.format(moduleDir)
        self.copy_icon = '{}/icons/copy_icon.png'.format(moduleDir)

        # init functions
        self.setupUi()
        self.set_default()
        self.init_signals()

    def setupUi(self):
        self.setObjectName(uiName)
        self.setWindowTitle('{} {} {}'.format(_title, _version, _des))
        self.setWindowIcon(QtGui.QIcon(self.app_icon))
        self.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))

        self.centralwidget = QtWidgets.QWidget(self)
        self.setCentralWidget(self.centralwidget)
        self.resize(self.w, self.h)

        # main layout
        self.main_layout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.main_layout.setContentsMargins(9, 9, 9, 9)
        self.main_layout.setSpacing(9)

        # -------- header layout
        self.header_layout = QtWidgets.QHBoxLayout()
        self.header_layout.setContentsMargins(6, 6, 0, 0)
        self.main_layout.addLayout(self.header_layout)

        # logo
        self.logo = QtWidgets.QLabel()
        self.logo.setPixmap(QtGui.QPixmap(self.logo_icon).scaled(64, 64, QtCore.Qt.KeepAspectRatio))
        self.header_layout.addWidget(self.logo)

        # project selector
        self.project_widget = project_widget.SGProjectComboBox(sg=sg, layout=QtWidgets.QHBoxLayout(), parent=self)
        self.header_layout.addWidget(self.project_widget)

        # header spacer
        header_spacer = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.header_layout.addItem(header_spacer)

        # delete button
        self.header_control_layout = QtWidgets.QVBoxLayout()
        self.header_control_layout.setContentsMargins(0, 0, 0, 0)
        self.header_control_layout.setSpacing(0)
        self.header_layout.addLayout(self.header_control_layout)

        # header spacer
        header_button_spacer = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.header_control_layout.addItem(header_button_spacer)

        self.header_button_layout = QtWidgets.QHBoxLayout()
        self.header_control_layout.setContentsMargins(0, 0, 0, 0)
        self.header_control_layout.setSpacing(5)
        self.header_control_layout.addLayout(self.header_button_layout)

        # copy button
        self.copy_button = QtWidgets.QPushButton()
        self.copy_button.setToolTip('Copy selected form')
        self.copy_button.setIcon(QtGui.QIcon(self.copy_icon))
        self.copy_button.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.header_button_layout.addWidget(self.copy_button)

        # delete button
        self.delete_button = QtWidgets.QPushButton()
        self.delete_button.setToolTip('Delete selected form')
        self.delete_button.setIcon(QtGui.QIcon(self.delete_icon))
        self.delete_button.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.header_button_layout.addWidget(self.delete_button)

        # clear button
        self.clear_button = QtWidgets.QPushButton()
        self.clear_button.setToolTip('Clear all forms')
        self.clear_button.setIcon(QtGui.QIcon(self.clear_icon))
        self.clear_button.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.header_button_layout.addWidget(self.clear_button)
        
        # frame
        self.create_groupbox = QtWidgets.QGroupBox()
        self.main_layout.addWidget(self.create_groupbox)

        self.create_layout = QtWidgets.QHBoxLayout(self.create_groupbox)

        # --------- buttons
        self.button_layout = QtWidgets.QVBoxLayout()
        self.create_layout.addLayout(self.button_layout)

        # preprocution button
        self.pp_button = QtWidgets.QPushButton()
        self.pp_button.setToolTip('Create a Preproduction asset')
        self.pp_button.setMinimumSize(QtCore.QSize(self.button_w, self.button_h))
        button_styleSheet = '''QPushButton {{ border-color: rgb{}; border-width: {}px; }}
        QPushButton:hover {{ border-color: rgb{}; }}
        '''.format(ui.PreproductionForm.color, self.button_stroke_size, ui.PreproductionForm.hilight_color())
        self.pp_button.setStyleSheet(button_styleSheet)
        self.pp_button.setIcon(QtGui.QIcon(ui.PreproductionForm.icon_path()))
        self.pp_button.setIconSize(QtCore.QSize(self.icon_w, self.icon_h)) 
        self.pp_button.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.button_layout.addWidget(self.pp_button)

        # character button
        self.char_button = QtWidgets.QPushButton()
        self.char_button.setToolTip('Create a Character')
        self.char_button.setMinimumSize(QtCore.QSize(self.button_w, self.button_h))
        button_styleSheet = '''QPushButton {{ border-color: rgb{}; border-width: {}px; }}
        QPushButton:hover {{ border-color: rgb{}; }}
        '''.format(ui.CharacterForm.color, self.button_stroke_size, ui.CharacterForm.hilight_color())
        self.char_button.setStyleSheet(button_styleSheet)
        self.char_button.setIcon(QtGui.QIcon(ui.CharacterForm.icon_path()))
        self.char_button.setIconSize(QtCore.QSize(self.icon_w, self.icon_h)) 
        self.char_button.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.button_layout.addWidget(self.char_button)

        # prop button
        self.prop_button = QtWidgets.QPushButton()
        self.prop_button.setToolTip('Create a Prop')
        self.prop_button.setMinimumSize(QtCore.QSize(self.button_w, self.button_h))
        button_styleSheet = '''QPushButton {{ border-color: rgb{}; border-width: {}px; }}
        QPushButton:hover {{ border-color: rgb{}; }}
        '''.format(ui.PropForm.color, self.button_stroke_size, ui.PropForm.hilight_color())
        self.prop_button.setStyleSheet(button_styleSheet)
        self.prop_button.setIcon(QtGui.QIcon(ui.PropForm.icon_path()))
        self.prop_button.setIconSize(QtCore.QSize(self.icon_w, self.icon_h)) 
        self.prop_button.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.button_layout.addWidget(self.prop_button)

        # setdress button
        self.setdress_button = QtWidgets.QPushButton()
        self.setdress_button.setToolTip('Create a Setdress')
        self.setdress_button.setMinimumSize(QtCore.QSize(self.button_w, self.button_h))
        button_styleSheet = '''QPushButton {{ border-color: rgb{}; border-width: {}px; }}
        QPushButton:hover {{ border-color: rgb{}; }}
        '''.format(ui.SetdressForm.color, self.button_stroke_size, ui.SetdressForm.hilight_color())
        self.setdress_button.setStyleSheet(button_styleSheet)
        self.setdress_button.setIcon(QtGui.QIcon(ui.SetdressForm.icon_path()))
        self.setdress_button.setIconSize(QtCore.QSize(self.icon_w, self.icon_h)) 
        self.setdress_button.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.button_layout.addWidget(self.setdress_button)

        # set button
        self.set_button = QtWidgets.QPushButton()
        self.set_button.setToolTip('Create a Set')
        self.set_button.setMinimumSize(QtCore.QSize(self.button_w, self.button_h))
        button_styleSheet = '''QPushButton {{ border-color: rgb{}; border-width: {}px; }}
        QPushButton:hover {{ border-color: rgb{}; }}
        '''.format(ui.SetForm.color, self.button_stroke_size, ui.SetForm.hilight_color())
        self.set_button.setStyleSheet(button_styleSheet)
        self.set_button.setIcon(QtGui.QIcon(ui.SetForm.icon_path()))
        self.set_button.setIconSize(QtCore.QSize(self.icon_w, self.icon_h)) 
        self.set_button.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.button_layout.addWidget(self.set_button)

        # keyvis button
        self.keyvis_button = QtWidgets.QPushButton()
        self.keyvis_button.setToolTip('Create a Key visual')
        self.keyvis_button.setMinimumSize(QtCore.QSize(self.button_w, self.button_h))
        button_styleSheet = '''QPushButton {{ border-color: rgb{}; border-width: {}px; }}
        QPushButton:hover {{ border-color: rgb{}; }}
        '''.format(ui.KeyvisForm.color, self.button_stroke_size, ui.KeyvisForm.hilight_color())
        self.keyvis_button.setStyleSheet(button_styleSheet)
        self.keyvis_button.setIcon(QtGui.QIcon(ui.KeyvisForm.icon_path()))
        self.keyvis_button.setIconSize(QtCore.QSize(self.icon_w, self.icon_h)) 
        self.keyvis_button.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.button_layout.addWidget(self.keyvis_button)

        # mattepaint button
        self.mattepaint_button = QtWidgets.QPushButton()
        self.mattepaint_button.setToolTip('Create a Matte paint')
        self.mattepaint_button.setMinimumSize(QtCore.QSize(self.button_w, self.button_h))
        button_styleSheet = '''QPushButton {{ border-color: rgb{}; border-width: {}px; }}
        QPushButton:hover {{ border-color: rgb{}; }}
        '''.format(ui.MattepaintForm.color, self.button_stroke_size, ui.MattepaintForm.hilight_color())
        self.mattepaint_button.setStyleSheet(button_styleSheet)
        self.mattepaint_button.setIcon(QtGui.QIcon(ui.MattepaintForm.icon_path()))
        self.mattepaint_button.setIconSize(QtCore.QSize(self.icon_w, self.icon_h)) 
        self.mattepaint_button.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.button_layout.addWidget(self.mattepaint_button)

        # fx button
        self.fx_button = QtWidgets.QPushButton()
        self.fx_button.setToolTip('Create an Effect')
        self.fx_button.setMinimumSize(QtCore.QSize(self.button_w, self.button_h))
        button_styleSheet = '''QPushButton {{ border-color: rgb{}; border-width: {}px; }}
        QPushButton:hover {{ border-color: rgb{}; }}
        '''.format(ui.FxForm.color, self.button_stroke_size, ui.FxForm.hilight_color())
        self.fx_button.setStyleSheet(button_styleSheet)
        self.fx_button.setIcon(QtGui.QIcon(ui.FxForm.icon_path()))
        self.fx_button.setIconSize(QtCore.QSize(self.icon_w, self.icon_h)) 
        self.fx_button.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.button_layout.addWidget(self.fx_button)

        # ep button
        self.ep_button = QtWidgets.QPushButton()
        self.ep_button.setToolTip('Create an Episode')
        self.ep_button.setMinimumSize(QtCore.QSize(self.button_w, self.button_h))
        button_styleSheet = '''QPushButton {{ border-color: rgb{}; border-width: {}px; }}
        QPushButton:hover {{ border-color: rgb{}; }}
        '''.format(ui.EpisodeForm.color, self.button_stroke_size, ui.EpisodeForm.hilight_color())
        self.ep_button.setStyleSheet(button_styleSheet)
        self.ep_button.setIcon(QtGui.QIcon(ui.EpisodeForm.icon_path()))
        self.ep_button.setIconSize(QtCore.QSize(self.icon_w, self.icon_h)) 
        self.ep_button.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.button_layout.addWidget(self.ep_button)

        # sequence button
        self.seq_button = QtWidgets.QPushButton()
        self.seq_button.setToolTip('Create a Sequence')
        self.seq_button.setMinimumSize(QtCore.QSize(self.button_w, self.button_h))
        button_styleSheet = '''QPushButton {{ border-color: rgb{}; border-width: {}px; }}
        QPushButton:hover {{ border-color: rgb{}; }}
        '''.format(ui.SequenceForm.color, self.button_stroke_size, ui.SequenceForm.hilight_color())
        self.seq_button.setStyleSheet(button_styleSheet)
        self.seq_button.setIcon(QtGui.QIcon(ui.SequenceForm.icon_path()))
        self.seq_button.setIconSize(QtCore.QSize(self.icon_w, self.icon_h)) 
        self.seq_button.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.button_layout.addWidget(self.seq_button)

        # shot button
        self.shot_button = QtWidgets.QPushButton()
        self.shot_button.setToolTip('Create a Shot')
        self.shot_button.setMinimumSize(QtCore.QSize(self.button_w, self.button_h))
        button_styleSheet = '''QPushButton {{ border-color: rgb{}; border-width: {}px; }}
        QPushButton:hover {{ border-color: rgb{}; }}
        '''.format(ui.ShotForm.color, self.button_stroke_size, ui.ShotForm.hilight_color())
        self.shot_button.setStyleSheet(button_styleSheet)
        self.shot_button.setIcon(QtGui.QIcon(ui.ShotForm.icon_path()))
        self.shot_button.setIconSize(QtCore.QSize(self.icon_w, self.icon_h)) 
        self.shot_button.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.button_layout.addWidget(self.shot_button)

        # -------- form area
        self.create_listWidget = ui.EntityListWidget()
        self.create_layout.addWidget(self.create_listWidget)

        # -------- announce area
        self.ann_layout = QtWidgets.QHBoxLayout()
        self.main_layout.addLayout(self.ann_layout)

        # header spacer
        create_button_spacer = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.ann_layout.addItem(create_button_spacer)

        # announce button
        self.create_button = QtWidgets.QPushButton('Create')
        self.create_button.setToolTip('Start creating entities')
        self.create_button.setFixedSize(QtCore.QSize(100, 35))
        self.create_button.setIcon(QtGui.QIcon(self.app_icon))
        self.create_button.setIconSize(QtCore.QSize(28, 28)) 
        self.create_button.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.ann_layout.addWidget(self.create_button)

        # -------- status line
        self.statusBar = QtWidgets.QStatusBar()
        status_font = QtGui.QFont()
        status_font.setItalic(True)
        self.statusBar.setFont(status_font)
        self.statusBar.setStyleSheet('color: rgb{}'.format(ui.StyleConfig.inactive_color))
        self.main_layout.addWidget(self.statusBar)

        self.main_layout.setStretch(0, 0)
        self.main_layout.setStretch(1, 5)
        self.main_layout.setStretch(3, 0)

    def set_default(self):
        self.statusBar.showMessage('Ready.')
        self.project_changed()

    def init_signals(self):
        # project signals
        self.project_widget.projectChanged.connect(self.project_changed)

        # button signals
        self.pp_button.clicked.connect(lambda: self.button_clicked(formObject=ui.PreproductionForm))
        self.char_button.clicked.connect(lambda: self.button_clicked(formObject=ui.CharacterForm))
        self.prop_button.clicked.connect(lambda: self.button_clicked(formObject=ui.PropForm))
        self.setdress_button.clicked.connect(lambda: self.button_clicked(formObject=ui.SetdressForm))
        self.set_button.clicked.connect(lambda: self.button_clicked(formObject=ui.SetForm))
        self.keyvis_button.clicked.connect(lambda: self.button_clicked(formObject=ui.KeyvisForm))
        self.mattepaint_button.clicked.connect(lambda: self.button_clicked(formObject=ui.MattepaintForm))
        self.fx_button.clicked.connect(lambda: self.button_clicked(formObject=ui.FxForm))
        self.ep_button.clicked.connect(lambda: self.button_clicked(formObject=ui.EpisodeForm))
        self.seq_button.clicked.connect(lambda: self.button_clicked(formObject=ui.SequenceForm))
        self.shot_button.clicked.connect(lambda: self.button_clicked(formObject=ui.ShotForm))
        self.create_button.clicked.connect(self.create)

        # utility button signals
        self.copy_button.clicked.connect(self.copy_item)
        self.delete_button.clicked.connect(self.delete_item)
        self.clear_button.clicked.connect(self.clear_items)
        
    def project_changed(self):
        self.clear_items()

        # build cache for the project both assets and scene
        self.update_asset_cache()
        self.update_scene_cache()
        
    def update_asset_cache(self):
        project = self.project_widget.current_item()
        # assets
        assets = sg_process.get_assets(project=project['name'])
        ui.PROJ_ASSETS = assets

    def update_scene_cache(self):
        project = self.project_widget.current_item()
        # eps
        episodes = sg_process.get_episodes(project=project['name'])
        ui.PROJ_EPS = episodes
        # shots
        shots = sg_process.get_all_shots(project=project['name'])
        ui.PROJ_SHOTS = shots

    def fetch_project_data(self):
        project = self.project_widget.current_item()

    def copy_item(self):
        item = self.create_listWidget.currentItem()
        if not item:
            return
        project = self.project_widget.current_item()
        next_index = self.create_listWidget.indexFromItem(item).row() + 1
        itemWidget = self.create_listWidget.itemWidget(item)
        
        # insert new item of the same type
        newItem, newItemWidget = self.create_listWidget.insert_entity(project=project, 
                                                                    formObject=itemWidget.__class__, 
                                                                    row=next_index)
        # copy filled data over
        old_data = itemWidget.get_data()
        if old_data:
            newItemWidget.paste(data=old_data)

        return newItem, newItemWidget

    def delete_item(self):
        sels = self.create_listWidget.selectedItems()
        if sels:
            for item in sels:
                self.create_listWidget.takeItem(self.create_listWidget.row(item))

    def clear_items(self):
        self.create_listWidget.clear()

    def button_clicked(self, formObject):
        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
        project = self.project_widget.current_item()
        item, itemWidget = self.create_listWidget.add_entity(project=project, formObject=formObject)
        QtWidgets.QApplication.restoreOverrideCursor()
        # itemWidget.nameLineEdit.setText('test12333')
        # self.enable_ui(False)
        return item, itemWidget

    def check_validity(self, forms):
        valid_data = []
        # primary checks
        form_name_dict = OrderedDict()  # {name: [form, ...], ...}
        form_data_list = []  # [(form, data), ...]
        for data_form in forms:
            data_form.reset_error_hilights()
            data = data_form.get_data()
            name = data.get('name') if data.get('name') else '<empty>'
            if name not in form_name_dict:
                form_name_dict[name] = []
            form_name_dict[name].append(data_form)
            form_data_list.append((data_form, data))

        # check duplicates
        errors = []
        for name, creating_forms in form_name_dict.items():
            # add to duplicates
            if len(creating_forms) > 1:
                if len(errors) == 0:
                    errors.append('- Forms with same name')
                
                # hilight the fields
                for data_form in creating_forms:
                    errors.append('  {}: "{}"'.format(data_form.ui_type.upper(), name))
                    data_form.set_error_hilight(data_form.nameLineEdit)
                errors.append('')

        # individual checks
        for data_form, data in form_data_list:
            check_results = data_form.check()
            ui_type = data_form.ui_type.title()
            if check_results:
                name = data.get('name') if data.get('name') else '<empty>'
                form_name = '- {} "{}"'.format(ui_type, name)
                errors.append(form_name)
                for err, val in check_results.items():
                    if isinstance(val, (list, tuple)):
                        val = ', '.join(val)
                    errors.append('  {}: {}'.format(err.title(), val))
                errors.append('')
            else:
                valid_data.append((data_form, data))
        
        if errors:
            self.show_error_dialog(text='\n'.join(errors), title='Creation Error')
        else:
            detailedText = self.generate_summary_text(valid_data=valid_data)
            text = 'Total of {} new item(s) will be created. \nAre you sure?'.format(len(valid_data))
            confirm = self.show_confirm_dialog(title='Confirm', 
                                            text=text, 
                                            detailedText=detailedText, 
                                            question=True)
            if confirm == QtWidgets.QMessageBox.AcceptRole:
                return True, valid_data

        return False, None

    def generate_summary_text(self, valid_data):
        details = []
        i = 0
        for data_form, data in valid_data:
            # construct info message
            details.append('{}. {} "{}"'.format(i+1, data_form.ui_type.title(), data['name']))
            for key, values in data.items():
                # skip if no values
                if key == 'name' or not values:
                    continue
                detailTitle = key.title()
           
                # detail as list, 0 index item is the display name
                if isinstance(values, list):
                    values = [v[0] for v in values]
                    if len(values) > 1:
                        value_newline = '\n        '.join(values)
                        details.append('    {}:\n        {}'.format(detailTitle, value_newline))
                    else:
                        details.append('    {}: {}'.format(detailTitle, values[0]))
                elif isinstance(values, tuple):  # (displayName, data)
                    details.append('    {}: {}'.format(detailTitle, values[0]))
                # detail as string
                elif isinstance(values, (str, unicode)):
                    combined_text = u'    ' + detailTitle +': ' + values
                    # details.append('    {}: {}'.format(detailTitle, values))
                    details.append(combined_text)
            # separator
            details.append('')
            i += 1

        detailedText = '\n'.join(details)
        return detailedText

    def show_confirm_dialog(self, title, text, detailedText='', question=True):
        qmsgBox = ui.ConfirmMessageBox(font_color=ui.StyleConfig.active_color, parent=self)
        qmsgBox.setWindowIcon(QtGui.QIcon(self.app_icon))
        qmsgBox.setWindowTitle(title)
        qmsgBox.setText(text)
        
        if detailedText:
            qmsgBox.setDetailedText(detailedText)

        if question:
            qmsgBox.setIcon(QtWidgets.QMessageBox.Question)
            yes_button = qmsgBox.addButton('  Yes  ', QtWidgets.QMessageBox.AcceptRole)
            no_button = qmsgBox.addButton('    No    ', QtWidgets.QMessageBox.RejectRole)
        else:
            qmsgBox.setIcon(QtWidgets.QMessageBox.Information)
            ok_button = qmsgBox.addButton('    OK    ', QtWidgets.QMessageBox.AcceptRole)
        result = qmsgBox.exec_()
        return result

    def show_error_dialog(self, text, title='Error'):
        qmsgBox = QtWidgets.QMessageBox(self)
        qmsgBox.setWindowIcon(QtGui.QIcon(self.app_icon))
        qmsgBox.setWindowTitle(title)
        qmsgBox.setIcon(QtWidgets.QMessageBox.Critical)
        qmsgBox.setText(text)
        ok_button = qmsgBox.addButton('    OK    ', QtWidgets.QMessageBox.AcceptRole)
        result = qmsgBox.exec_()
        return result

    def check_project_permission(self, project):
        proj_info = project_info.ProjectInfo(project)
        proj_env = proj_info.env()
        proj_drive = proj_env.get(config.Env.projectVar)
        project_root_path = '{}/{}'.format(proj_drive, project)
        if not file_utils.is_readable(project_root_path):
            text = 'You do NOT have permission on project "{}".\nPlease contact I.T. team.'.format(project)
            self.show_error_dialog(text=text, title='Permission Error')
            return False
        return True

    def create(self):
        # get all items
        self.creation_base_types = []
        forms = self.create_listWidget.get_forms()
    
        # ----- run checks
        confirm, valid_data = self.check_validity(forms=forms)
        project = self.project_widget.current_item()['name']

        if not confirm:
            self.statusBar.showMessage('Ready.')
            return
        if not valid_data:
            self.statusBar.setStyleSheet('color: rgb{}'.format(ui.StyleConfig.error_color))
            self.statusBar.showMessage('Error, please check your inputs.')
            return
        # check for permission on the project
        if not self.check_project_permission(project=project):
            self.statusBar.setStyleSheet('color: rgb{}'.format(ui.StyleConfig.error_color))
            self.statusBar.showMessage('Error, no permission.')
            return

        # store types of crated entities for later refresh
        self.creation_base_types = list(set([f.base_type for f in forms]))

        # construct functions to be used in thread
        self.statusBar.setStyleSheet('color: rgb{}'.format(ui.StyleConfig.active_color))
        self.statusBar.showMessage('Getting creation data...')
        func_list = []  # [(func, args), ...]
        for data_form, data in valid_data:
            func, args = data_form.get_func()
            func_list.append((func, args))

        print(func_list)
        # disable UI
        self.enable_ui(enable=False)
        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
        
        # start thread
        self.thread = CreateThread(func_list, parent=self)
        self.thread.progress.connect(self.update_progress)
        self.thread.allFinished.connect(self.create_finished)
        self.thread.start()

        # notification
        project = self.project_widget.current_item()
        user = user_info.User()
        userEntity = user.sg_user()
        detailedText = self.generate_summary_text(valid_data=valid_data)
        taskEntity = {'project': project, 'content': 'create', 'step': {'type': 'Step', 'name': 'Create'}, 'entity': {'name':'announcer','type':'shot'}, 'sg_status_list': 'apr'}
        taskResult = []
        prod = {'step':{'name':'prod'}, 'task_assignees':[{'name':'-'}], 'project':taskEntity['project'], 'sg_status_list':'def' }
        taskResult.append(prod)
        noti_module_cliq.send_announcer_notification(taskResult, taskEntity, userEntity, description=detailedText)

    def enable_ui(self, enable):
        # utilities button
        self.copy_button.setEnabled(enable)
        self.clear_button.setEnabled(enable)
        self.delete_button.setEnabled(enable)
        # self.create_listWidget.setEnabled(enable)
        for widget in self.create_listWidget.get_forms():
            widget.setEnabled(enable)
        self.create_button.setEnabled(enable)

        # create buttons
        self.pp_button.setEnabled(enable)
        self.char_button.setEnabled(enable)
        self.prop_button.setEnabled(enable)
        self.setdress_button.setEnabled(enable)
        self.set_button.setEnabled(enable)
        self.keyvis_button.setEnabled(enable)
        self.mattepaint_button.setEnabled(enable)
        self.fx_button.setEnabled(enable)
        self.ep_button.setEnabled(enable)
        self.seq_button.setEnabled(enable)
        self.shot_button.setEnabled(enable)
        self.create_button.setEnabled(enable)

    def update_progress(self, progress):
        curr, total, name = progress
        self.statusBar.showMessage('Creating({}/{}): {}'.format(curr, total, name))
        # set current process item
        item = self.create_listWidget.item(curr-1)  # subtract 1 cuz progress is 1 base index
        self.create_listWidget.setCurrentItem(item)

    def create_finished(self):
        # enable UI
        self.statusBar.setStyleSheet('color: rgb{}'.format(ui.StyleConfig.inactive_color))
        self.statusBar.showMessage('Finished.')
        self.enable_ui(enable=True)
        # clear creation forms
        self.clear_items()  
        # re cache only where nescessary
        if 'scene' in self.creation_base_types:
            print('Updating scene cache')
            self.update_scene_cache()
        if 'asset' in self.creation_base_types:
            print('Updating asset cache')
            self.update_asset_cache()

        QtWidgets.QApplication.restoreOverrideCursor()

        # show confirm dialog
        self.show_confirm_dialog(title='Finished', 
                                text='New entity has been created.', 
                                question=False)

def show():
    app = QtWidgets.QApplication(sys.argv)
    myApp = Announcer()
    myApp.show()
    stylesheet.set_default(app)
    sys.exit(app.exec_())

if __name__ == '__main__':
    show()

'''
from rf_utils.pipeline import test_create_entity
reload(test_create_entity)

test_create_entity.asset()
'''