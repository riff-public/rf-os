#Import python modules
import sys
import os
from functools import partial
from datetime import datetime
from collections import OrderedDict, defaultdict

# QT
os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

from rf_utils.context import context_info
from rf_utils.sg import sg_process
from rf_utils.pipeline import create_asset
from rf_utils.pipeline import create_scene
from rf_utils import file_utils
from rf_utils.widget import sg_widget
from rf_utils.widget import hashtag_widget
from rf_utils.widget import entity_browse_widget

moduleDir = os.path.dirname(sys.modules[__name__].__file__).replace('\\', '/')

# cache public vars
PROJ_EPS = []
PROJ_ASSETS = []
PROJ_SHOTS = []

# global icon
OMIT_ICON = '{}/icons/omt_icon.png'.format(moduleDir)
PROJECT_CONFIG = file_utils.ymlLoader('{}/project_config.yml'.format(moduleDir))

class StyleConfig:
    # colors
    black = (0, 0, 0)
    darkerGray = (35, 35, 35)
    darkGray = (50, 50, 50)
    midGray = (75, 75, 75)
    lightGray = (160, 160, 160)
    brightGray = (235, 235, 235)
    white = (255, 255, 255)

    tagBackground = (248, 252, 194)
    error_color = (235, 0, 0)
    active_color = (235, 235, 52)
    inactive_color = (150, 150, 150)

    fieldBackground = 'background-color: rgb{};'.format(midGray)
    fontDisabled = 'color: rgb{};'.format(lightGray)
    fieldError = 'background-color: rgb(125, 0, 0);'
    border = 'border: 2px solid rgb{};'.format(darkerGray)
    formStyle = 'background-color: rgba(50, 50, 50, 0); border-width: 2px;'

    field_style = '''
    QLineEdit:enabled {{ {background} }}
    QLineEdit:disabled {{ {disabled} }}
    '''.format(background=fieldBackground, 
            disabled=fontDisabled)

    listWidget_style = '''QListWidget {{ background-color: rgb{darkerGray}; }}
    QListWidget::item:selected {{ border: 2px solid white; background-color: rgba(0, 0, 0, 0); }}
    QListWidget::item:hover {{ border: 2px solid white; background-color: rgb{darkGray}; }}
    '''.format(darkGray=darkGray, 
            darkerGray=darkerGray)

class TextConfig:
    required = '*Required'

class CrossCheckbox(QtWidgets.QCheckBox):
    def __init__(self, *args, **kwargs):
        super(CrossCheckbox, self).__init__(*args, **kwargs)
        self.stateChanged.connect(self.on_toggled)

    def on_toggled(self, value):
        if value:
            self.setStyleSheet('color: grey; text-decoration: line-through')
        else:
            self.setStyleSheet('color: white; text-decoration: none')

class IconLabel(QtWidgets.QWidget):
    def __init__(self, text='', icon='', parent=None):
        super(IconLabel, self).__init__(parent)

        self.layout = QtWidgets.QHBoxLayout()
        self.layout.setSpacing(2)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.layout)

        # icon
        self.icon_label = QtWidgets.QLabel()
        icon = QtGui.QIcon(icon)
        pixmap = QtGui.QPixmap(icon.pixmap(16, 16))
        self.icon_label.setPixmap(pixmap)
        self.layout.addWidget(self.icon_label)

        # text
        self.text_label = QtWidgets.QLabel(text)
        self.layout.addWidget(self.text_label)

class EntityListWidget(QtWidgets.QListWidget):
    ''' subclass QListWidget '''

    def __init__(self, parent=None):
        super(EntityListWidget, self).__init__(parent)
        self.setStyleSheet(StyleConfig.listWidget_style)

    def create_item(self, project, formObject):
        # create item
        item = QtWidgets.QListWidgetItem()
        itemWidget = formObject(project=project)
        itemWidget.clicked.connect(partial(self.select_item, item))
        item.setSizeHint(itemWidget.sizeHint())
        return item, itemWidget

    def add_entity(self, project, formObject):
        item, itemWidget = self.create_item(project, formObject)
        self.addItem(item)
        self.setItemWidget(item, itemWidget)
        return item, itemWidget

    def insert_entity(self, project, formObject, row):
        item, itemWidget = self.create_item(project, formObject)
        self.insertItem(row, item)
        self.setItemWidget(item, itemWidget)
        return item, itemWidget

    def select_item(self, item, *args, **kwargs):
        self.setCurrentItem(item)

    def get_forms(self):
        forms = []
        for i in range(self.count()):
            item = self.item(i)
            form = self.itemWidget(item)
            forms.append(form)
        return forms

class ConfirmMessageBox(QtWidgets.QMessageBox):

    def __init__(self, font_color, *args, **kwargs):            
        super(ConfirmMessageBox, self).__init__(*args, **kwargs)
        self.font_color = font_color

    def resizeEvent(self, event):
        result = super(ConfirmMessageBox, self).resizeEvent(event)
        details_box = self.findChild(QtWidgets.QTextEdit)
        if details_box is not None:
            details_box.setStyleSheet('color: rgb{};'.format(self.font_color))
            details_box.setFixedSize(details_box.sizeHint())

class PatternConfig:
    name_pattern = "^[A-Za-z0-9]*$"
    scene_pattern = "[0-9][0-9][0-9][0-9][A-Za-z]*$"
    pp_pattern = "[A-Za-z0-9]*$"

class CustomLineEdit(QtWidgets.QLineEdit):
    clicked = QtCore.Signal(bool)
    def __init__(self, parent=None):
        super(CustomLineEdit, self).__init__(parent)
        self.setStyleSheet(StyleConfig.field_style)

    def focusInEvent(self, event):
        super(CustomLineEdit, self).focusInEvent(event)
        self.clicked.emit(True)

class NameLineEdit(CustomLineEdit):
    clicked = QtCore.Signal(bool)
    def __init__(self, project=None, parent=None):
        super(NameLineEdit, self).__init__(parent)
        self.project = project
        self.invalid_texts = []
        self.text_validator = None

    def focusInEvent(self, event):
        super(NameLineEdit, self).focusInEvent(event)
        self.clicked.emit(True)

    def connect_validator(self, pattern=PatternConfig.name_pattern):
        if self.text_validator:
            self.disconnect_validator()
        if pattern:
            self.text_validator = QtGui.QRegExpValidator(QtCore.QRegExp(pattern))
            self.textChanged.connect(self.validate)

    def disconnect_validator(self):
        self.text_validator = None
        self.textChanged.disconnect()

    def validate(self, text):
        ''' Do not allow name starting with number or upper case '''
        if not text:
            return
        text = "".join(text.split())
        self.blockSignals(True)
        self.setText(text)
        self.blockSignals(False)

        status, text, pos = self.text_validator.validate(text, 0)

        # if text[0].isdigit() or text[0].isupper():
        #     status = QtGui.QValidator.State.Invalid
        project_key = self.project if self.project in PROJECT_CONFIG else 'default'
        config_value = PROJECT_CONFIG[project_key].get('name_starts_with')
        if text[0].isdigit() and config_value.get('digit') == False:
            status = QtGui.QValidator.State.Invalid
        if text[0].isupper() and config_value.get('upper') == False:
            status = QtGui.QValidator.State.Invalid

        if status == QtGui.QValidator.State.Invalid:
            text = text[0:pos-1]
            self.blockSignals(True)
            self.setText(text)
            self.blockSignals(False)

        if text in self.invalid_texts:
            self.setStyleSheet(StyleConfig.fieldError)
        else:
            self.setStyleSheet(StyleConfig.field_style)
        
class SceneLineEdit(NameLineEdit):
    clicked = QtCore.Signal(bool)
    def __init__(self, parent=None):
        super(SceneLineEdit, self).__init__(parent)
        self.prefix = ''
        self.text_validator = None
        self._validator_connected = False
        self.connect_validator()

    def connect_validator(self, prefix='', pattern=PatternConfig.scene_pattern):
        self.clear()
        if self.text_validator:
            self.disconnect_validator()
        
        self.text_validator = QtGui.QRegExpValidator(QtCore.QRegExp("{}{}".format(prefix, pattern)))
        self.textChanged.connect(self.validate)
        self._validator_connected = True
        self.prefix = prefix

    def disconnect_validator(self):
        self.clear()
        self.text_validator = None
        self.textChanged.disconnect(self.validate)
        self._validator_connected = False

    def validate(self, text):
        if not text:
            return
        text = "".join(text.split())
        self.blockSignals(True)
        self.setText(text)
        self.blockSignals(False)

        status, text, pos = self.text_validator.validate(text, 0)
        auto_correct = False
        if status != QtGui.QValidator.State.Intermediate:
            if len(text) > 0 and not text.startswith(self.prefix) and text[pos-1].isdigit():
                text = self.prefix + text
                auto_correct = True
                new_status, new_text, new_pos = self.text_validator.validate(text, 0)
                if new_status != QtGui.QValidator.State.Invalid:
                    pos += 1
        if status == QtGui.QValidator.State.Invalid:
            if not auto_correct:
                pos -= 1
            text = text[0:pos]
        self.blockSignals(True)
        self.setText(text)
        self.blockSignals(False)

class PrefixLineEdit(NameLineEdit):
    def __init__(self, prefix='', parent=None):
        super(PrefixLineEdit, self).__init__(parent)
        self.prefix = prefix
        self.editingFinished.connect(self.add_prefix)

    def add_prefix(self):
        text = self.text()
        if not text:
            return
        len_prefix = len(self.prefix)
        if text.startswith(self.prefix):
            text = text[len_prefix:]
        text = text[0].upper() + text[1:]
        text = self.prefix + text
        self.setText(text)

        if text in self.invalid_texts:
            self.setStyleSheet(StyleConfig.fieldError)
        else:
            self.setStyleSheet(StyleConfig.field_style)

class FormBase(QtWidgets.QWidget):
    ''' Abstract base class for form '''
    clicked = QtCore.Signal(bool)
    color = (0, 0, 0)
    label_width = 35
    ui_type = None
    sg_type = None
    production_type = None
    def __init__(self, parent=None):
        super(FormBase, self).__init__(parent)
        styleSheet = 'QWidget {{ {style} }}'.format(style=StyleConfig.formStyle)
        self.setStyleSheet(styleSheet)

        self.required_fields = list()
        
    @classmethod
    def hilight_color(self):
        hilight_color = QtGui.QColor(self.color[0], self.color[1], self.color[2]).lighter()
        return (hilight_color.red(), hilight_color.green(), hilight_color.blue())

    @classmethod
    def icon_path(self):
        return '{}/icons/{}_icon.png'.format(moduleDir, self.ui_type)

    def label_path(self):
        return '{}/icons/{}_label.png'.format(moduleDir, self.ui_type)

    def activated(self):
        self.clicked.emit(True)

    def init_ui(self):
        # main layout
        self.main_layout = QtWidgets.QHBoxLayout()
        self.main_layout.setSpacing(9)
        self.setLayout(self.main_layout)

        self.title_label = QtWidgets.QLabel()
        self.title_label.setMinimumWidth(self.label_width)
        self.title_label.setStyleSheet('background-color: rgb{}'.format(self.color))
        self.main_layout.addWidget(self.title_label)

        self.data_layout = QtWidgets.QVBoxLayout()
        self.data_layout.setSpacing(5)
        self.main_layout.addLayout(self.data_layout)

        self.main_layout.setStretch(0, 0)
        self.main_layout.setStretch(1, 1)

    def set_error_hilight(self, ui_object):
        ui_object.setStyleSheet(StyleConfig.fieldError)

    def reset_error_hilight(self, ui_object):
        ui_object.setStyleSheet(StyleConfig.field_style)

    def reset_error_hilights(self):
        lineEdits = self.findChildren(QtWidgets.QLineEdit)
        for lineEdit in lineEdits:
            lineEdit.setStyleSheet(StyleConfig.field_style)

    def check(self):
        # check required fields
        results = defaultdict(list)
        for ui_obj in self.required_fields:
            # if field is a line edit
            if isinstance(ui_obj, QtWidgets.QLineEdit):
                if not ui_obj.text():
                    self.set_error_hilight(ui_obj)
                    label = self.basic_layout.labelForField(ui_obj)
                    results['requires'].append(label.text())
            # if field is a TagWidget
            elif isinstance(ui_obj, hashtag_widget.TagWidget):
                if not ui_obj.get_all_item():
                    self.set_error_hilight(ui_obj.line_edit)
                    label = self.basic_layout.labelForField(ui_obj)
                    results['requires'].append(label.text())
            # if it's a layout containing lineEdit
            elif isinstance(ui_obj, QtWidgets.QLayout):
                lineEdits = self.findChildren(QtWidgets.QLineEdit)
                if lineEdits and not lineEdits[0].text():
                    self.set_error_hilight(lineEdits[0])
                    label = self.basic_layout.labelForField(ui_obj)
                    results['requires'].append(label.text())
        return results

class AssetFormBase(FormBase):
    ''' Base class for Asset form '''
    base_type = 'asset'
    def __init__(self, project, parent=None):
        self.project = project
        self.assetType = None
        self.assetSubtype = None
        self.step_checkboxes = []
        super(AssetFormBase, self).__init__(parent)

    def init_ui(self):
        super(AssetFormBase, self).init_ui()
        pixmap = QtGui.QPixmap(self.label_path()).scaled(QtCore.QSize(self.label_width, self.label_width), QtCore.Qt.KeepAspectRatio)
        self.title_label.setPixmap(pixmap)

        # basic layout
        self.basic_layout = QtWidgets.QFormLayout()
        self.data_layout.addLayout(self.basic_layout)

        self.add_name_widget()
        self.add_hashtag_widget()
        self.add_ep_widget()
        self.add_description_widget()

    def get_omit_steps(self):
        omit_steps = []
        for step_checkbox in self.step_checkboxes:
            if step_checkbox.isChecked():
                name = step_checkbox.text()
                step_name = step_checkbox.step_name
                omit_steps.append((name, step_name))
        return omit_steps

    def add_name_widget(self):
        # name
        self.nameLineEdit = NameLineEdit(project=self.project.get('name'))
        self.nameLineEdit.invalid_texts = [a.get('code') for a in PROJ_ASSETS]
        self.nameLineEdit.connect_validator(pattern=PatternConfig.name_pattern)
        self.nameLineEdit.setPlaceholderText(TextConfig.required)
        self.required_fields.append(self.nameLineEdit)
        self.basic_layout.addRow('Name', self.nameLineEdit)
        self.nameLineEdit.clicked.connect(self.activated)

    def add_hashtag_widget(self):
        # hash tag
        self.hashtagWidget = hashtag_widget.SGTagWidget(inLine=True)
        self.hashtagWidget.line_edit.setPlaceholderText(TextConfig.required)
        self.required_fields.append(self.hashtagWidget)
        self.hashtagWidget.label.hide()
        self.hashtagWidget.display.setMaximumHeight(23)
        self.basic_layout.addRow('Tags', self.hashtagWidget)
        self.hashtagWidget.clicked.connect(self.activated)

    def add_ep_widget(self):
        # EP widget
        self.epWidget = hashtag_widget.SGEpTagWidget(project=self.project, episodes=PROJ_EPS, inLine=False)
        self.epWidget.line_edit.setPlaceholderText(TextConfig.required)
        self.required_fields.append(self.epWidget)
        self.epWidget.use_hash = False
        self.epWidget.allow_creation = False
        self.epWidget.allow_rename = False
        self.epWidget.label.hide()
        self.epWidget.display.setMaximumHeight(52)
        self.basic_layout.addRow('Used in', self.epWidget)
        self.epWidget.clicked.connect(self.activated)
        
    def add_description_widget(self):
        # description
        self.desLineEdit = NameLineEdit()
        self.basic_layout.addRow('Description', self.desLineEdit)
        self.desLineEdit.clicked.connect(self.activated)
    
    def add_related_design_widget(self):
        # related design widget
        self.relatedWidget = hashtag_widget.SGAssetTagWidget(inLine=True, project=self.project, assets=PROJ_ASSETS)
        # self.relatedWidget.line_edit.setPlaceholderText('Share designs with...')
        self.relatedWidget.use_hash = False
        self.relatedWidget.allow_creation = False
        self.relatedWidget.allow_rename = False
        self.relatedWidget.label.hide()
        self.relatedWidget.display.setMaximumHeight(23)
        self.basic_layout.addRow('Related', self.relatedWidget)
        self.relatedWidget.clicked.connect(self.activated)

    def add_parent_set_widget(self):
        # parent asset widget
        set_filter = [['sg_asset_type', 'is', 'Set']]
         # filter out sets form assets
        all_sets = [a for a in PROJ_ASSETS if a.get('sg_asset_type')=='Set']
        self.parentWidget = hashtag_widget.SGAssetTagWidget(inLine=True, project=self.project, filters=set_filter, assets=all_sets)
        self.parentWidget.use_hash = False
        self.parentWidget.allow_creation = False
        self.parentWidget.allow_rename = False
        self.parentWidget.label.hide()
        self.parentWidget.display.setMaximumHeight(23)
        self.basic_layout.addRow('Set', self.parentWidget)
        self.parentWidget.clicked.connect(self.activated)

    def add_looks_widget(self):
        # looks widget
        self.looksWidget = hashtag_widget.TagWidget(inLine=True)
        self.looksWidget.use_hash = False
        self.looksWidget.allow_creation = True
        self.looksWidget.allow_rename = True
        self.looksWidget.label.hide()
        self.looksWidget.display.setMaximumHeight(23)
        self.basic_layout.addRow('Looks', self.looksWidget)
        self.looksWidget.clicked.connect(self.activated)

    def get_data(self):
        name = self.nameLineEdit.text()
        hashTags = self.hashtagWidget.get_all_item_data()
        eps = self.epWidget.get_all_item_data()
        description = self.desLineEdit.text()
        return {'name': name, 'tag': hashTags, 'used in': eps, 'description': description}

    def paste(self, data):
        self.hashtagWidget.clear()
        if 'tag' in data:
            self.hashtagWidget.add_items(data['tag'], check=False)

        self.epWidget.clear()
        if 'used in' in data:
            self.epWidget.add_items(data['used in'], check=False)

        self.desLineEdit.clear()
        if 'description' in data:
            self.desLineEdit.setText(data['description'])

    def check(self):
        result = super(AssetFormBase, self).check()
        # check if asset already exists
        name = self.nameLineEdit.text()
        if name in [a.get('code') for a in PROJ_ASSETS]:
            self.set_error_hilight(self.nameLineEdit)
            result['already exist'] = name
        return result

    def get_func(self):
        data = self.get_data()

        # create function vars
        func = create_asset.create
        project = self.project['name']
        productionType = self.production_type
        assetType = self.assetType
        assetSubtype = self.assetSubtype
        mainAsset = ''
        setEntity = ''
        template = self.template

        # required data
        name = data['name']
        tags = [t[1] for t in data['tag']]
        used_in = [u[1] for u in data['used in']]
        description = data['description']
        # need to separate each shot type into it's own arg variable
        episodes = []
        sequences = []
        shots = []
        for item in used_in:
            if 'sg_shot_type' in item:
                shottype = item['sg_shot_type']
                if shottype == 'Sequence':
                    sequences.append(item['sg_sequence'])
                elif shottype == 'Shot':
                    shots.append(item)
            else:  # it's EP
                episodes.append(item)

        # construct kwargs
        args = {'project': project, 
                'name': name, 
                'productionType': productionType,
                'assetType': assetType, 
                'assetSubtype': assetSubtype, 
                'mainAsset': mainAsset, 
                'tags': tags, 
                'template': template, 
                'setEntity': setEntity, 
                'link': True,
                'episodes': episodes,
                'sequences': sequences,
                'shots': shots, 
                'description': description}
        return func, args, data

class PreproductionForm(AssetFormBase):
    ''' Form for Character creation '''
    color = (245, 220, 66)
    ui_type = 'preproduction'
    production_type = 'Preproduction'
    name_prefix = 'pp'
    available_types = OrderedDict([('Character', {'type': 'char', 'template': 'char'}), 
                                ('Prop', {'type': 'prop', 'template': 'prop'}), 
                                ('Set', {'type': 'set', 'template': 'set'}), 
                                ('FX', {'type': 'fx', 'template': 'prop'})])

    def __init__(self, project, parent=None):
        super(PreproductionForm, self).__init__(project, parent)
        self.assetType = ''
        self.assetSubtype = 'main'
        self.template = ''
        self.init_ui()

    def init_ui(self):
        super(AssetFormBase, self).init_ui()
        pixmap = QtGui.QPixmap(self.label_path()).scaled(QtCore.QSize(self.label_width, self.label_width), QtCore.Qt.KeepAspectRatio)
        self.title_label.setPixmap(pixmap)

        # basic layout
        self.basic_layout = QtWidgets.QFormLayout()
        self.data_layout.addLayout(self.basic_layout)

        # type combobox
        self.add_type_combobox()

        # nameLineEdit that support prefixing
        self.nameLineEdit = PrefixLineEdit(prefix=self.name_prefix)
        self.nameLineEdit.invalid_texts = [a.get('code') for a in PROJ_ASSETS]
        self.nameLineEdit.connect_validator(pattern=PatternConfig.pp_pattern)
        self.nameLineEdit.setPlaceholderText(TextConfig.required)
        self.required_fields.append(self.nameLineEdit)
        self.basic_layout.addRow('Name', self.nameLineEdit)
        self.nameLineEdit.clicked.connect(self.activated)

        self.add_hashtag_widget()
        self.add_ep_widget()
        self.add_description_widget()

    def add_type_combobox(self):
        self.type_combobox = QtWidgets.QComboBox()
        self.type_combobox.setMinimumWidth(100)
        for display_name, data in self.available_types.items():
            self.type_combobox.addItem(display_name, data)
        self.basic_layout.addRow('Type', self.type_combobox)
        self.type_combobox.currentIndexChanged.connect(self.type_changed)
        self.type_changed(self.type_combobox.currentIndex())

    def type_changed(self, index):
        data = self.type_combobox.itemData(index)
        self.assetType = data['type']
        self.template = data['template']

    def get_data(self):
        data = super(PreproductionForm, self).get_data()
        index = self.type_combobox.currentIndex()
        assetType = self.type_combobox.currentText()  # get type from comboBox
        data.update({'type': (assetType, index)})
        return data

    def paste(self, data):
        super(PreproductionForm, self).paste(data)
        if 'type' in data:
            self.type_combobox.setCurrentIndex(data['type'][1])

    def get_func(self):
        func, args, data = super(PreproductionForm, self).get_func()
        return func, args

class CharacterForm(AssetFormBase):
    ''' Form for Character creation '''
    color = (26, 157, 195)
    ui_type = 'character'
    production_type = 'Production'

    def __init__(self, project, parent=None):
        super(CharacterForm, self).__init__(project, parent)
        self.assetType = 'char'
        self.assetSubtype = 'main'
        self.template = 'char'
        self.init_ui()

    def init_ui(self):
        super(CharacterForm, self).init_ui()
        self.add_related_design_widget()
        self.add_looks_widget()
        self.add_omit_step_widget()

    def add_omit_step_widget(self):
        # label
        self.omit_label = IconLabel(text='Omit', icon=OMIT_ICON)
        # self.omit_label.text_label.setStyleSheet('color: red')

        # checkboxes
        self.omit_layout = QtWidgets.QHBoxLayout()
        self.omit_layout.setContentsMargins(3, 0, 0, 0)
        self.omit_layout.setSpacing(45)

        self.design_checkBox = CrossCheckbox('Design')
        self.design_checkBox.step_name = 'design'
        self.design_checkBox.setChecked(True)
        self.omit_layout.addWidget(self.design_checkBox)

        self.rig_checkBox = CrossCheckbox('Rig')
        self.rig_checkBox.step_name = 'rig'
        self.omit_layout.addWidget(self.rig_checkBox)

        self.groom_checkBox = CrossCheckbox('Groom')
        self.groom_checkBox.step_name = 'groom'
        self.omit_layout.addWidget(self.groom_checkBox)

        self.sim_checkBox = CrossCheckbox('Sim')
        self.sim_checkBox.step_name = 'sim'
        self.omit_layout.addWidget(self.sim_checkBox)

        self.basic_layout.addRow(self.omit_label, self.omit_layout)
        self.step_checkboxes = [self.design_checkBox, self.rig_checkBox, self.groom_checkBox, self.sim_checkBox]

    def get_data(self):
        data = super(CharacterForm, self).get_data()
        # related widget
        relates = self.relatedWidget.get_all_item_data()
        data.update({'related': relates})
        # looks widget
        looks = self.looksWidget.get_all_item_data()
        data.update({'looks': looks})
        # omit steps
        omit_steps = self.get_omit_steps()
        if omit_steps:
            data.update({'omit_steps': omit_steps})
        return data

    def paste(self, data):
        super(CharacterForm, self).paste(data)
        # related widget
        self.relatedWidget.clear()
        if 'related' in data:
            self.relatedWidget.add_items(data['related'], check=False)
        # looks widget
        self.looksWidget.clear()
        if 'looks' in data:
            self.looksWidget.add_items(data['looks'], check=False)
        # omit steps
        if 'omit_steps' in data:
            for step_checkbox in self.step_checkboxes:
                if step_checkbox.step_name in [s[1] for s in data['omit_steps']]:
                    step_checkbox.setChecked(True)

    def get_func(self):
        func, args, data = super(CharacterForm, self).get_func()
        # optional data
        data_updates = {}
        if 'related' in data and data['related']:
            data_updates['design_entities'] = [r[1] for r in data['related']]
        if 'looks' in data and data['looks']:
            data_updates['looks'] = [l[1] for l in data['looks']]
        if 'omit_steps' in data and data['omit_steps']:
            data_updates['omit_steps'] = [s[1] for s in data['omit_steps']]
        # update args
        args.update(data_updates)

        return func, args
        
class PropForm(AssetFormBase):
    ''' Form for Prop creation '''
    color = (220, 140, 40)
    ui_type = 'prop'
    production_type = 'Production'

    def __init__(self, project, parent=None):
        super(PropForm, self).__init__(project, parent)
        self.assetType = 'prop'
        self.assetSubtype = 'main'
        self.template = 'prop'
        self.init_ui()

    def init_ui(self):
        super(PropForm, self).init_ui()
    
        # parent set widget
        self.add_parent_set_widget()
        # related design widget
        self.add_related_design_widget()
        # looks widget
        self.add_looks_widget()
        # omit steps
        self.add_omit_step_widget()

    def add_omit_step_widget(self):
        # label
        self.omit_label = IconLabel(text='Omit', icon=OMIT_ICON)
        # self.omit_label.text_label.setStyleSheet('color: red')

        # checkboxes
        self.omit_layout = QtWidgets.QHBoxLayout()
        self.omit_layout.setContentsMargins(3, 0, 0, 0)
        self.omit_layout.setSpacing(45)

        self.design_checkBox = CrossCheckbox('Design')
        self.design_checkBox.step_name = 'design'
        self.design_checkBox.setChecked(True)
        self.omit_layout.addWidget(self.design_checkBox)

        self.rig_checkBox = CrossCheckbox('Rig')
        self.rig_checkBox.step_name = 'rig'
        self.omit_layout.addWidget(self.rig_checkBox)

        self.basic_layout.addRow(self.omit_label, self.omit_layout)
        self.step_checkboxes = [self.design_checkBox, self.rig_checkBox]

    def get_data(self):
        data = super(PropForm, self).get_data()
        # parent widget
        parents = self.parentWidget.get_all_item_data()
        data.update({'set': parents})
        # related widget
        relates = self.relatedWidget.get_all_item_data()
        data.update({'related': relates})
        # looks widget
        looks = self.looksWidget.get_all_item_data()
        data.update({'looks': looks})
        # omit steps
        omit_steps = self.get_omit_steps()
        if omit_steps:
            data.update({'omit_steps': omit_steps})
        return data

    def paste(self, data):
        super(PropForm, self).paste(data)
        # parent widget
        self.parentWidget.clear()
        if 'set' in data:
            self.parentWidget.add_items(data['set'], check=False)
        # related widget
        self.relatedWidget.clear()
        if 'related' in data:
            self.relatedWidget.add_items(data['related'], check=False)
        # looks widget
        self.looksWidget.clear()
        if 'looks' in data:
            self.looksWidget.add_items(data['looks'], check=False)
        # omit steps
        if 'omit_steps' in data:
            for step_checkbox in self.step_checkboxes:
                if step_checkbox.step_name in [s[1] for s in data['omit_steps']]:
                    step_checkbox.setChecked(True)

    def get_func(self):
        func, args, data = super(PropForm, self).get_func()
        # optional data
        data_updates = {}
        if 'set' in data and data['set']:
            data_updates['setEntity'] = [r[1] for r in data['set']]
        if 'related' in data and data['related']:
            data_updates['design_entities'] = [r[1] for r in data['related']]
        if 'looks' in data and data['looks']:
            data_updates['looks'] = [l[1] for l in data['looks']]
        if 'omit_steps' in data and data['omit_steps']:
            data_updates['omit_steps'] = [s[1] for s in data['omit_steps']]
        # update args
        args.update(data_updates)

        return func, args
        
class SetdressForm(AssetFormBase):
    ''' Form for Setdress creation '''
    color = (120, 190, 60)
    ui_type = 'setdress'
    production_type = 'Production'

    def __init__(self, project, parent=None):
        super(SetdressForm, self).__init__(project, parent)
        self.assetType = 'prop'
        self.assetSubtype = 'setdress'
        self.template = 'prop'
        self.init_ui()

    def init_ui(self):
        super(SetdressForm, self).init_ui()

        # parent set widget
        self.add_parent_set_widget()
        self.parentWidget.line_edit.setPlaceholderText(TextConfig.required)
        self.required_fields.append(self.parentWidget)

        # related design widget
        self.add_related_design_widget()

    def get_data(self):
        data = super(SetdressForm, self).get_data()
        # parent widget
        parents = self.parentWidget.get_all_item_data()
        data.update({'set': parents})
        # related widget
        relates = self.relatedWidget.get_all_item_data()
        data.update({'related': relates})
        return data

    def paste(self, data):
        super(SetdressForm, self).paste(data)
        # parent widget
        self.parentWidget.clear()
        if 'set' in data:
            self.parentWidget.add_items(data['set'], check=False)
        # related widget 
        self.relatedWidget.clear()
        if 'related' in data:
            self.relatedWidget.add_items(data['related'], check=False)

    def get_func(self):
        func, args, data = super(SetdressForm, self).get_func()
        # reuired
        data_updates = {}
        if 'set' in data and data['set']:
            data_updates['setEntity'] = [r[1] for r in data['set']]
        # optional data
        if 'related' in data and data['related']:
            data_updates['design_entities'] = [r[1] for r in data['related']]
        # update args
        args.update(data_updates)

        return func, args

class SetForm(AssetFormBase):
    ''' Form for Set creation '''
    color = (30, 150, 80)
    ui_type = 'set'
    production_type = 'Production'

    def __init__(self, project, parent=None):
        super(SetForm, self).__init__(project, parent)
        self.assetType = 'set'
        self.assetSubtype = 'main'
        self.template = 'set'
        self.init_ui()

    def init_ui(self):
        super(SetForm, self).init_ui()
        # related design widget
        self.add_related_design_widget()

    def get_data(self):
        data = super(SetForm, self).get_data()
        # related widget
        relates = self.relatedWidget.get_all_item_data()
        data.update({'related': relates})
        return data

    def paste(self, data):
        super(SetForm, self).paste(data)
        # related widget
        self.relatedWidget.clear()
        if 'related' in data:
            self.relatedWidget.add_items(data['related'], check=False)

    def get_func(self):
        func, args, data = super(SetForm, self).get_func()
        # optional data
        data_updates = {}
        if 'related' in data and data['related']:
            data_updates['design_entities'] = [r[1] for r in data['related']]
        # update args
        args.update(data_updates)

        return func, args

class KeyvisForm(AssetFormBase):
    ''' Form for Keyvis creation '''
    color = (154, 99, 210)
    ui_type = 'keyvis'
    production_type = 'Production'

    def __init__(self, project, parent=None):
        super(KeyvisForm, self).__init__(project, parent)
        self.assetType = 'keyvis'
        self.assetSubtype = 'main'
        self.template = 'keyvis'
        self.init_ui()

    def init_ui(self):
        super(KeyvisForm, self).init_ui()

    def get_func(self):
        func, args, data = super(KeyvisForm, self).get_func()
        return func, args
        
class MattepaintForm(AssetFormBase):
    ''' Form for Mattepaint creation '''
    color = (60, 170, 170)
    ui_type = 'mattepaint'
    production_type = 'Production'

    def __init__(self, project, parent=None):
        super(MattepaintForm, self).__init__(project, parent)
        self.assetType = 'mattepaint'
        self.assetSubtype = 'main'
        self.template = 'mattepaint'
        self.init_ui()

    def init_ui(self):
        super(MattepaintForm, self).init_ui()

    def get_func(self):
        func, args, data = super(MattepaintForm, self).get_func()
        return func, args

class FxForm(AssetFormBase):
    ''' Form for FX creation '''
    color = (204, 73, 41)
    ui_type = 'fx'
    production_type = 'Production'

    def __init__(self, project, parent=None):
        super(FxForm, self).__init__(project, parent)
        self.assetType = 'fx'
        self.assetSubtype = 'main'
        self.template = 'prop'
        self.init_ui()

    def init_ui(self):
        super(FxForm, self).init_ui()

    def get_func(self):
        func, args, data = super(FxForm, self).get_func()
        return func, args

class SceneFormBase(FormBase):
    ''' Base class for Shot form '''
    base_type = 'scene'
    def __init__(self, project, parent=None):
        self.project = project
        self.nameLineEdit = None
        super(SceneFormBase, self).__init__(parent)

    def init_ui(self):
        super(SceneFormBase, self).init_ui()
        pixmap = QtGui.QPixmap(self.label_path()).scaled(QtCore.QSize(self.label_width, self.label_width), QtCore.Qt.KeepAspectRatio)
        self.title_label.setPixmap(pixmap)

        # basic layout
        self.basic_layout = QtWidgets.QFormLayout()
        self.data_layout.addLayout(self.basic_layout)

    def add_sceneLineEdit(self, label, layout=None):
        # name
        lineEdit = SceneLineEdit()
        lineEdit.clicked.connect(self.activated)

        if layout:
            layout.insertWidget(0, lineEdit)
        else:
            self.basic_layout.addRow(label, lineEdit)
        lineEdit.textChanged.connect(self.check)
        return lineEdit

    def add_nameLineEdit(self, label, layout=None):
        lineEdit = NameLineEdit()
        lineEdit.connect_validator(pattern=PatternConfig.name_pattern)
        lineEdit.clicked.connect(self.activated)

        if layout:
            layout.insertWidget(0, lineEdit)
        else:
            self.basic_layout.addRow(label, lineEdit)

        # signals
        lineEdit.textChanged.connect(self.check)
        return lineEdit

    def sg_combobox(self, displayAttr):
        combobox = sg_widget.SGComboBox(displayAttr=displayAttr)
        combobox_styleSheet = '''
        QWidget {{ {bg} }}
        QComboBox {{ {border}}}
        '''.format(bg=StyleConfig.fieldBackground, 
                border=StyleConfig.border)
        combobox.setStyleSheet(combobox_styleSheet)
        combobox.setMinimumWidth(135)
        combobox.comboBox.highlighted.connect(self.activated)
        return combobox

    def combobox(self):
        combobox = QtWidgets.QComboBox()
        combobox_styleSheet = '''
        QWidget {{ {bg} }}
        QComboBox {{ {border}}}
        '''.format(bg=StyleConfig.fieldBackground, 
                border=StyleConfig.border)
        combobox.setStyleSheet(combobox_styleSheet)
        combobox.setMinimumWidth(135)
        combobox.highlighted.connect(self.activated)
        return combobox

    def add_pattern_toggle_lineEdit(self, label, prefix):
        # line edit layout
        self.lineEdit_layout = QtWidgets.QHBoxLayout()
        self.lineEdit_layout.setSpacing(3)
        self.lineEdit_layout.setContentsMargins(0, 0, 0, 0)
        self.basic_layout.addRow(label, self.lineEdit_layout)
        # checkbox
        self.pattern_checkbox = QtWidgets.QCheckBox('Use Pattern')
        self.pattern_checkbox.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.pattern_checkbox.setChecked(True)
        self.lineEdit_layout.addWidget(self.pattern_checkbox)

        # call manually first time
        self.toggle_lineEdit(label=label, prefix=prefix)
        # connect signal
        self.pattern_checkbox.toggled.connect(lambda: self.toggle_lineEdit(label=label, prefix=prefix))

    def toggle_lineEdit(self, label, prefix):
        placeholderText = ''
        if self.nameLineEdit:
            placeholderText = self.nameLineEdit.placeholderText()
            self.nameLineEdit.deleteLater()

        # seq lineEdit
        if self.pattern_checkbox.isChecked():
            self.nameLineEdit = self.add_sceneLineEdit(label=label, layout=self.lineEdit_layout)
            self.nameLineEdit.connect_validator(prefix=prefix, pattern=PatternConfig.scene_pattern)
        else:
            self.nameLineEdit = self.add_nameLineEdit(label=label, layout=self.lineEdit_layout)
            self.nameLineEdit.connect_validator(pattern=None)
        if placeholderText:
            self.nameLineEdit.setPlaceholderText(placeholderText)

    def get_data(self):
        name = self.nameLineEdit.text()
        return {'name': name}

    def paste(self, data):
        pass

    def check(self):
        pass

class EpisodeForm(SceneFormBase):
    ''' Form for Episode creation '''
    color = (200, 110, 170)
    ui_type = 'episode'
    sg_type = 'Scene'
    production_type = 'Production'

    def __init__(self, project, parent=None):
        super(EpisodeForm, self).__init__(project, parent)
        self.init_ui()

    def init_ui(self):
        super(EpisodeForm, self).init_ui()
        # ep
        # self.nameLineEdit = self.add_nameLineEdit(label='E.P.')
        # self.nameLineEdit.setPlaceholderText(TextConfig.required)
        # self.required_fields.append(self.nameLineEdit)
        # add toggleable lineEdit
        self.add_pattern_toggle_lineEdit(label='E.P.', prefix='')
        self.pattern_checkbox.setChecked(False)
        self.nameLineEdit.setPlaceholderText(TextConfig.required)
        self.required_fields.append(self.lineEdit_layout)

    def get_data(self):
        data = super(EpisodeForm, self).get_data()
        return data

    def check(self):
        result = super(SceneFormBase, self).check()
        # check if EP already exists
        name = self.nameLineEdit.text()
        if name in [ep.get('code') for ep in PROJ_EPS]:
            self.set_error_hilight(self.nameLineEdit)
            result['already exist'] = name
        else:
            self.reset_error_hilight(self.nameLineEdit)
        return result

    def get_func(self):
        data = self.get_data()

        # create function vars
        func = create_scene.create_episode
        project = self.project
        name = data['name']

        # construct kwargs
        args = {'project': project, 
                'name': name}
        return func, args

class SequenceForm(SceneFormBase):
    ''' Form for Sequence creation '''
    color = (145, 145, 145)
    ui_type = 'sequence'
    sg_type = 'Sequence'
    production_type = 'Production'

    def __init__(self, project, parent=None):
        super(SequenceForm, self).__init__(project, parent)
        self.context = None
        self.init_ui()

    def init_ui(self):
        super(SequenceForm, self).init_ui()

        # ep
        self.ep_combobox = self.sg_combobox(displayAttr='code')
        self.ep_combobox.add_items(PROJ_EPS)
        self.basic_layout.addRow('E.P.', self.ep_combobox)

        # add toggleable lineEdit
        self.add_pattern_toggle_lineEdit(label='Seq', prefix='q')
        self.nameLineEdit.setPlaceholderText(TextConfig.required)
        self.required_fields.append(self.lineEdit_layout)

    def get_data(self):
        data = super(SequenceForm, self).get_data()  # get seq name from line edit
        ep = self.ep_combobox.current_item()  # get EP from comboBox

        data.update({'episode': (ep['code'], ep)})
        return data

    def paste(self, data):
        super(SequenceForm, self).paste(data)
        # set EP
        if 'episode' in data:
            self.ep_combobox.set_current_item(data['episode'][0])

    def check(self):
        result = super(SceneFormBase, self).check()
        # check if seq already exists
        ep = self.ep_combobox.current_text()
        seqName = self.nameLineEdit.text()
        scene = self.get_context(episode=ep, name=seqName)
        if scene.sg_seq_name in [a.get('sg_sequence')['name'] for a in PROJ_SHOTS if a.get('sg_sequence')]:
            self.set_error_hilight(self.nameLineEdit)
            result['already exist'] = scene.sg_seq_name
        else:
            self.reset_error_hilight(self.nameLineEdit)
        return result

    def get_context(self, episode, name):
        # get name args from context
        context = context_info.Context()
        context.update(project=self.project['name'], 
                    entityType='scene', 
                    projectCode=self.project['sg_project_code'], 
                    entityGrp=episode)
        scene = context_info.ContextPathInfo(context=context)
        scene.checkFormat = False
        scene.context.update(entityGrp=episode, 
                            entityParent=name)
        scene.context.update(entityParent=scene.sequence)
        scene.context.update(entityCode='all')
        scene.context.update(entityCode=scene.name_code)
        return scene

    def get_func(self):
        data = self.get_data()

        # create function vars
        func = create_scene.create

        scene = self.get_context(episode=data['episode'][0], name=data['name'])
        project = scene.project
        name = scene.sg_name
        episode = str(scene.episode)
        sequence = str(scene.sequence)
        shortcode = scene.name_code
        shotType = self.sg_type

        # construct kwargs
        args = {'project': project, 
                'name': name, 
                'episode': episode, 
                'sequence': sequence,
                'shortcode': shortcode,
                'shotType': shotType,
                'link': True}
        return func, args

class ShotForm(SceneFormBase):
    ''' Form for Shot creation '''
    color = (204, 204, 204)
    ui_type = 'shot'
    sg_type = 'Shot'
    production_type = 'Production'

    def __init__(self, project, parent=None):
        super(ShotForm, self).__init__(project, parent)
        self.init_ui()
        self.init_signals()

        self.ep_combobox.item_changed()

    def init_ui(self):
        super(ShotForm, self).init_ui()

        # ep
        self.ep_combobox = self.sg_combobox(displayAttr='code')
        self.ep_combobox.add_items(PROJ_EPS)
        self.basic_layout.addRow('E.P.', self.ep_combobox)

        # seq
        self.seq_combobox = self.combobox()
        self.basic_layout.addRow('Seq', self.seq_combobox)

        # add toggleable lineEdit
        self.add_pattern_toggle_lineEdit(label='Shot', prefix='s')
        self.nameLineEdit.setPlaceholderText(TextConfig.required)
        self.required_fields.append(self.lineEdit_layout)

    def init_signals(self):
        self.ep_combobox.currentIndexChanged.connect(self.ep_changed)

    def ep_changed(self, epEntity):
        self.seq_combobox.clear()

        filters = [['project', 'is', self.project], ['sg_episode', 'is', epEntity]]
        fields = ['code', 'id', 'project', 'sg_episode', 'sg_sequence_code', 'sg_shortcode', 'sg_shot_type']
        shots = sg_process.sg.find('Shot', filters, fields)
        sequences = sorted(list(set([s['sg_sequence_code'] for s in shots])))

        self.seq_combobox.addItems(sequences)

    def get_data(self):
        data = super(ShotForm, self).get_data()
        ep = self.ep_combobox.current_item()
        seq = self.seq_combobox.currentText()
        data.update({'episode': (ep['code'], ep), 'sequence': seq})
        return data

    def paste(self, data):
        super(ShotForm, self).paste(data)
        # set EP
        if 'episode' in data:
            self.ep_combobox.blockSignals(True)
            self.ep_combobox.set_current_item(data['episode'][0])
            epEntity = self.ep_combobox.current_item()
            self.ep_changed(epEntity=epEntity)
            self.ep_combobox.blockSignals(False)
        # set seq
        if 'sequence' in data:
            for index in range(self.seq_combobox.count()):
                text = self.seq_combobox.itemText(index)
                if text == data['sequence']:
                    self.seq_combobox.setCurrentIndex(index)
                    break

    def check(self):
        result = super(SceneFormBase, self).check()
        # check if shot already exists
        shotName = self.nameLineEdit.text()
        ep = self.ep_combobox.current_text()
        seq = self.seq_combobox.currentText()
        scene = self.get_context(episode=ep, sequence=seq, name=shotName)

        if scene.sg_name in [a.get('code') for a in PROJ_SHOTS]:
            self.set_error_hilight(self.nameLineEdit)
            result['already exist'] = scene.sg_name
        else:
            self.reset_error_hilight(self.nameLineEdit)
        return result

    def get_context(self, episode, sequence, name):
        context = context_info.Context()
        context.update(project=self.project['name'], 
                    entityType='scene', 
                    projectCode=self.project['sg_project_code'], 
                    entityGrp=episode)
        scene = context_info.ContextPathInfo(context=context)
        scene.checkFormat = False
        scene.context.update(entityGrp=episode, 
                            entityParent=sequence)
        scene.context.update(entityParent=scene.sequence)
        scene.context.update(entityCode=name)
        scene.context.update(entityCode=scene.name_code)
        return scene

    def get_func(self):
        data = self.get_data()

        # create function vars
        func = create_scene.create

        # get name args from context
        scene = self.get_context(episode=data['episode'][0], sequence=data['sequence'], name=data['name'])

        project = scene.project
        name = scene.sg_name
        episode = str(scene.episode)
        sequence = str(scene.sequence)
        shortcode = scene.name_code
        shotType = self.sg_type

        # construct kwargs
        args = {'project': project, 
                'name': name, 
                'episode': episode, 
                'sequence': sequence,
                'shortcode': shortcode,
                'shotType': shotType,
                'link': True}
        return func, args