import os
import time
import argparse
import logging

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import pymel.core as pm
from rf_utils.context import context_info
reload(context_info)
#from rf_maya.rftool.rig.utaTools.utapy.utaCore import mainProxyRig
from nuTools.misc import getRelativeBBScale
import propit_app
from rf_utils.sg import sg_utils
from rf_qc.maya_lib.asset import add_tag

from rf_maya.rftool.rig.rigScript import mainRig
reload(mainRig)
from rf_maya.rftool.rig.rigScript import fkRig
reload(fkRig)
from rf_maya.rftool.rig.rigScript import core
reload(core)
cn = core.Node()

import maya.cmds as mc
import maya.mel as mm

HOME_DIR = '%s/%s' %(os.environ['HOME'], 'propit_cmd')
LOG_LEVEL = logging.INFO
def setup_parser(title):
    parser = argparse.ArgumentParser(description=title, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-i', dest='input_path', type=str, help='The input path')
    parser.add_argument('-r', dest='res', type=str, help='The input path', default='md')
    return parser

def main(input_path, res):
    if not os.path.exists(HOME_DIR):
        logger.debug('# Script home dir created: %s' %HOME_DIR)
        os.makedirs(HOME_DIR)

    timestr = time.strftime("%Y%m%d-%H%M%S")
    log_file = 'propit_cmd_result.%s.log' %(timestr)
    log_path = '%s/%s' %(HOME_DIR, log_file)
    fh1 = logging.FileHandler(log_path)
    fh1.setLevel(LOG_LEVEL)
    logger.addHandler(fh1)

    # find model ABC
    entity = context_info.ContextPathInfo(path=input_path)

    # get group names
    geoGrpName = entity.projectInfo.asset.geo_grp()
    resGeoGrpName = entity.projectInfo.asset.md_geo_grp()
    if res == 'pr':
        resGeoGrpName = entity.projectInfo.asset.pr_geo_grp()

    # open the file
    logger.info('Openning file: %s' %input_path)
    pm.openFile(input_path, f=True)
    logger.info('File opened.')

    # # find geo grp
    # grpLn = '|%s|%s' %(geoGrpName, resGeoGrpName)
    # lsGrps = pm.ls(grpLn)
    # if not lsGrps:
    #     logger.error('Cannot find: %s' %grpLn)
    #     return

    # # ----- rig it
    # resGeoGrp = lsGrps[0]
    # geoGrp = resGeoGrp.getParent()

    # logger.info('ResGeo_Grp: %s' %resGeoGrp)
    # logger.info('Geo_Grp: %s' %geoGrp)

    # # get center
    # logger.info('Getting center of boundingBox...')
    # geoGrpBB = geoGrp.getBoundingBox()
    # center = geoGrpBB.center()
    # logger.info('Bounding Box: %s\nCentered at: %s' %(geoGrpBB, center))
    # geoGrp.setPivots(center)
    # logger.info('Pivot set for %s' %(geoGrp.nodeName()))

    # # call rig module
    # logger.info('Rigging...')
    # pm.select(geoGrp, r=True)
    # rigObj = mainProxyRig()
    # logger.info('Rig finished.')

    # # resize controls
    # logger.info('resizing controls...')
    # allMover = pm.PyNode(rigObj.allMoverCtrl)
    # offsetCtrl = pm.PyNode(rigObj.offsetCtrl)
    # allMoverBB = allMover.getBoundingBox()
    # rScale1 = getRelativeBBScale(allMoverBB, geoGrpBB)
    # avgRScale1 = ((rScale1[0] + rScale1[2])/2) * 2
    # rScale1 = (avgRScale1, 0.0, avgRScale1)
    # pm.scale(allMover.cv[0:allMover.numCVs()-1], rScale1, r=True)
    # pm.scale(offsetCtrl.cv[0:offsetCtrl.numCVs()-1], rScale1, r=True)

    # rootSubCtrl = pm.PyNode(rigObj.rootSubCtrl)
    # rootSubCtrlGimbal = pm.PyNode(rigObj.rootSubGmblCtrl)
    # rootSubCtrlBB = rootSubCtrl.getBoundingBox()
    # rScale2 = getRelativeBBScale(rootSubCtrlBB, geoGrpBB)
    # avgRScale2 = ((rScale2[0] + rScale2[2])/2) * 1.25
    # rScale2 = (avgRScale2, 0.0, avgRScale2)
    # pm.scale(rootSubCtrl.cv[0:rootSubCtrl.numCVs()-1], rScale2, r=True)
    # pm.scale(rootSubCtrlGimbal.cv[0:rootSubCtrlGimbal.numCVs()-1], rScale2, r=True)
    # allMover.sy.unlock()
    # logger.info('Controllers resized.')

    # find geo grp
    grpLn = '|%s|%s' %(geoGrpName, resGeoGrpName)
    lsGrps = pm.ls(grpLn)
    if not lsGrps:
        logger.error('Cannot find: %s' %grpLn)
        #return

    geoGrpObj = mc.rename(geoGrpName , 'Geo_GrpObj')

    main = mainRig.Run()
    root = cn.createNode('joint','Root_TmpJnt')


    root_obj = fkRig.Run(name      = 'Root' ,
                        tmpJnt     = root ,
                        ctrlGrp    = main.mainCtrlGrp , 
                        shape      = 'circle' ,
                        jnt_ctrl   = 1 ,
                        color      = 'green',
                        localWorld = 0,
                        constraint = 0,
                        jnt = 0,
                        dtl = 0)

    for obj in ( root_obj.ctrl , root_obj.gmbl ) :
        obj.scaleShape( 2.3 )

    root_obj.ctrl.setRotateOrder( 'xzy' )
    root_obj.ctrl.addAttr('geoVis' , 0 , 1)
    root_obj.ctrl.attr('geoVis').v = 1

    obj = mc.listRelatives(geoGrpObj , c=True)[0]
    mc.parent(obj , geoGrpName)
    mc.delete(geoGrpObj , root)

    #self.connectCtrlGmbl_cmd(ctrl = root_obj.ctrl.name , obj = obj)
    mc.parentConstraint(root_obj.ctrl.name, resGeoGrpName,mo=1)
    mc.scaleConstraint(root_obj.ctrl.name, resGeoGrpName,mo=1)

    mc.connectAttr('{}.geoVis'.format(root_obj.ctrl.name),'{}.v'.format(resGeoGrpName))

    for obj in ( root_obj.ctrlGrp , root_obj.zro ,root_obj.ofst ) :
        obj.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

    mc.select( cl = True )
    geoGrp = pm.PyNode(main.geoGrp.name)
    geoGrpBB = geoGrp.getBoundingBox()
    center = geoGrpBB.center()

    # resize controls
    logger.info('resizing controls...')
    allMover = pm.PyNode(main.allMoverCtrl.name)
    offsetCtrl = pm.PyNode(main.offsetCtrl.name)
    allMoverBB = allMover.getBoundingBox()
    rScale1 = getRelativeBBScale(allMoverBB, geoGrpBB)
    avgRScale1 = ((rScale1[0] + rScale1[2])/2) * 4
    rScale1 = (avgRScale1, 0.0, avgRScale1)
    pm.scale(allMover.cv[0:allMover.numCVs()-1], rScale1, r=True)
    pm.scale(offsetCtrl.cv[0:offsetCtrl.numCVs()-1], rScale1, r=True)

    rootSubCtrl = pm.PyNode(root_obj.ctrl.name)
    rootSubCtrlGimbal = pm.PyNode(root_obj.gmbl.name)
    rootSubCtrlBB = rootSubCtrl.getBoundingBox()
    rScale2 = getRelativeBBScale(rootSubCtrlBB, geoGrpBB)
    avgRScale2 = ((rScale2[0] + rScale2[2])/2) * 2
    rScale2 = (avgRScale2, 0.0, avgRScale2)
    pm.scale(rootSubCtrl.cv[0:rootSubCtrl.numCVs()-1], rScale2, r=True)
    pm.scale(rootSubCtrlGimbal.cv[0:rootSubCtrlGimbal.numCVs()-1], rScale2, r=True)
    allMover.sy.unlock()
    logger.info('Controllers resized.')

    ## check axis for project and fix axis control
    cn.createCvAxis( axisCheck = '')

    # ----- export
    logger.info('Exporting...')
    asset = context_info.ContextPathInfo()
    asset.context.update(step='rig', res=res)
    asset.context.use_sg(sg_utils.sg, project=asset.project, entityType='asset', entityName=asset.name)

    # add tag
    add_tag.run(entity=asset)

    propit_app.publish_rig(asset)
    logger.info('Export finished.')




if __name__ == "__main__":
    parser = setup_parser(':::PropIt Cmd:::')
    params = parser.parse_args()

    main(input_path=params.input_path, res=params.res)



'''
"C:/Program Files/Autodesk/Maya2018/bin/mayapy.exe" "D:/dev/core/rf_app/propit2/propit_cmd" -i "P:/SevenChickMovie/asset/publ/prop/tableLong/hero/tableLong_rig_main_md.hero.mb"

'''