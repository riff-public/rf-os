import os
import maya.cmds as mc
import argparse

# from rf_app.sg_manager import utils
from rf_app.layout.sequencer_manager import controls_program
from rftool.utils import maya_utils
from rf_maya.lib import sequencer_lib
from rf_utils.context import context_info
from rf_utils.sg import sg_process
sg = sg_process.sg 

# import logging

# logger = logging.getLogger(__name__)
# logger.addHandler(logging.NullHandler())

# HOME_DIR = '%s/%s' %(os.environ['HOME'], 'propit_cmd')
# LOG_LEVEL = logging.INFO

def setup_parser(title):
    parser = argparse.ArgumentParser(description=title, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-p', dest='project', type=str, help='The project name')
    parser.add_argument('-a', dest='assetName', type=str, help='The asset name')
    return parser


def main(project, assetName):
    # new scene
    plugin = 'AbcImport.mll'
    if not mc.pluginInfo(plugin, q=True, l=True):
        mc.loadPlugin(plugin, qt=True)

    mc.file(new=True, f=True)

    workPath = context_info.ContextPathInfo()
    workPath.context.update(entityGrp='mattePaint', project=project, entityType='design', entity=assetName)
    work_dir_path = workPath.path.scheme(key='workPath').abs_path()
    # print work_dir_path # << this is version design path

    entity = workPath.copy()
    entity.context.update(entityType='asset' ,app='png', publishVersion='hero', look='main')

    # #path =' P:/Bikey/asset/work/char/childE/texture/main/maya/childE_mtl_main.v003.GuideM.ma'
    # #entity = context_info.ContextPathInfo(path = path)
    # # texturePath1 = entity.path.texture().abs_path()
    # texturePath2 = entity.path.publish_texture().abs_path() # << this is texRen

    # try to create maya file to preview mattepaint for art department

    # reference/ import rig mattepaint
    
    # from rf_app.sg_manager import dcc_hook
    res = 'md'
    entity.context.update(app='maya')
    rigFileRel = get_rig(entity, res=res, absPath=False)
    # refObjs = dcc_hook.create_reference(entity, rigFileRel, material=True)
    mc.file(rigFileRel, i=True, prompt=False)

    # copy and change texturePath
    geoGrp = 'Geo_Grp'
    # geoGrp = publishInfo.entity.projectInfo.asset.geo_grp()
    maya_utils.move_texture_path(work_dir_path, geoGrp, resize=False, relink=True)

    # get ep and seq
    
    sg = sg_process.sg 
    # project = scene.project 
    asset_name = entity.name
    fields = ['sequences']
    asset_sg = sg.find_one('Asset', [['project.Project.name', 'is', project], ['code', 'is', asset_name]], fields)

    sequences = asset_sg['sequences']
    context = context_info.Context()
    for seq in sequences: 
        entityName = '{}_all'.format(seq['name'])
        context.use_sg(sg, project, entityType='scene', entityName=entityName)
        scene = context_info.ContextPathInfo(context=context)
        
        episode = scene.episode
        sequence = scene.sequence

        # ref cam abc
        listShot = controls_program.get_shot_from_sg(scene)
        sequenceEndTime = 0
        for shotSG in listShot:
            shotName = shotSG['code']
            shotCode = shotSG['sg_shortcode']
            shotContext = scene.copy()
            shotContext.context.update(step='default', look='main', entity=shotName)
            dirname = shotContext.path.hero().abs_path()
            basename = shotContext.output_name(outputKey='camCacheHero', hero=True)
            heroCam = '%s/%s' % (dirname, basename)
            namespace = '%s_cam'%shotCode
            if os.path.exists(heroCam):
                mc.file(heroCam, r=True, ns=namespace, prompt=False)
                name = shotCode
                mc.select('%s:*'%namespace) 
                cameraShape = mc.ls(sl=True, type='camera')[0]
                startFrame = shotContext.projectInfo.scene.start_frame or 1001
                duration = shotSG['sg_working_duration']
                endFrame = startFrame + (duration-1)
                sequenceStartTime = sequenceEndTime + 1 
                sequenceEndTime = sequenceStartTime + duration - 1
                sequencer_lib.create_shot(name, cameraShape, startFrame, endFrame, sequenceStartTime, sequenceEndTime)

    # save scene
    sceneName = '{name}_preview.ma'.format(name = assetName)
    newScene = os.path.join(work_dir_path, sceneName)
    result = save_file(newScene)
    print 'Save scene {scene}'.format(scene=newScene)
    return result

def get_rig(entity, res, absPath=False):
    step = entity.step
    prevRes = entity.res
    process = entity.process
    entity.context.update(step='rig', process='main', res=res)

    heroPath = entity.path.hero().abs_path()
    heroRelPath = entity.path.hero()
    mbFileName = entity.output_name(outputKey='rig', hero=True)
    fileName = entity.publish_name(hero=True)
    
    rigFile1 = '%s/%s' % (heroPath, mbFileName)
    rigFile2 = '%s/%s' % (heroPath, fileName)

    rigRelPath1 = '%s/%s' % (heroRelPath, mbFileName)
    rigRelPath2 = '%s/%s' % (heroRelPath, fileName)

    # restore
    entity.context.update(step=step, process=process, res=prevRes)
    
    # try mb first then ma 
    rigRelPath = ''
    if os.path.exists(rigFile1): 
        return rigFile1 if absPath else rigRelPath1

    if os.path.exists(rigFile2): 
        return rigFile2 if absPath else rigRelPath2

    return rigFile2 if absPath else rigRelPath2

def save_file(filepath):
    if not os.path.exists(os.path.dirname(filepath)): 
        os.makedirs(os.path.dirname(filepath))
    name, ext = os.path.splitext(filepath)
    filetype = 'mayaAscii' if ext == '.ma' else 'mayaBinary' if ext == '.mb' else ''
    mc.file(rename=filepath)
    result = mc.file(save=True, f=True, type=filetype)
    return result

if __name__ == "__main__":
    parser = setup_parser(':::build mattepaint scene Cmd:::')
    params = parser.parse_args()

    main(project=params.project, assetName=params.assetName)
