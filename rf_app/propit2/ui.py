# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui.ui'
#
# Created: Sat Jun 30 00:29:18 2018
#      by: PyQt4 UI code generator 4.9.5
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName(_fromUtf8("Form"))
        Form.resize(297, 447)
        self.verticalLayout_3 = QtGui.QVBoxLayout(Form)
        self.verticalLayout_3.setObjectName(_fromUtf8("verticalLayout_3"))
        self.title_layout = QtGui.QGridLayout()
        self.title_layout.setObjectName(_fromUtf8("title_layout"))
        self.label = QtGui.QLabel(Form)
        self.label.setObjectName(_fromUtf8("label"))
        self.title_layout.addWidget(self.label, 0, 0, 1, 1)
        self.label_2 = QtGui.QLabel(Form)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.title_layout.addWidget(self.label_2, 1, 0, 1, 1)
        self.label_3 = QtGui.QLabel(Form)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.title_layout.addWidget(self.label_3, 0, 1, 1, 1)
        self.label_4 = QtGui.QLabel(Form)
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.title_layout.addWidget(self.label_4, 1, 1, 1, 1)
        self.title_layout.setColumnStretch(0, 1)
        self.title_layout.setColumnStretch(1, 3)
        self.verticalLayout_3.addLayout(self.title_layout)
        self.frame = QtGui.QFrame(Form)
        self.frame.setFrameShape(QtGui.QFrame.Box)
        self.frame.setFrameShadow(QtGui.QFrame.Sunken)
        self.frame.setObjectName(_fromUtf8("frame"))
        self.frame_layout = QtGui.QVBoxLayout(self.frame)
        self.frame_layout.setObjectName(_fromUtf8("frame_layout"))
        self.label_5 = QtGui.QLabel(self.frame)
        self.label_5.setObjectName(_fromUtf8("label_5"))
        self.frame_layout.addWidget(self.label_5)
        self.verticalLayout_3.addWidget(self.frame)
        self.horizontalLayout_3 = QtGui.QHBoxLayout()
        self.horizontalLayout_3.setObjectName(_fromUtf8("horizontalLayout_3"))
        self.snap_pushButton = QtGui.QPushButton(Form)
        self.snap_pushButton.setObjectName(_fromUtf8("snap_pushButton"))
        self.horizontalLayout_3.addWidget(self.snap_pushButton)
        self.verticalLayout_3.addLayout(self.horizontalLayout_3)
        self.line = QtGui.QFrame(Form)
        self.line.setFrameShape(QtGui.QFrame.HLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName(_fromUtf8("line"))
        self.verticalLayout_3.addWidget(self.line)
        self.horizontalLayout_6 = QtGui.QHBoxLayout()
        self.horizontalLayout_6.setObjectName(_fromUtf8("horizontalLayout_6"))
        self.save_pushButton = QtGui.QPushButton(Form)
        self.save_pushButton.setObjectName(_fromUtf8("save_pushButton"))
        self.horizontalLayout_6.addWidget(self.save_pushButton)
        self.center_pushButton = QtGui.QPushButton(Form)
        self.center_pushButton.setObjectName(_fromUtf8("center_pushButton"))
        self.horizontalLayout_6.addWidget(self.center_pushButton)
        self.verticalLayout_3.addLayout(self.horizontalLayout_6)
        self.rig_pushButton = QtGui.QPushButton(Form)
        self.rig_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        self.rig_pushButton.setObjectName(_fromUtf8("rig_pushButton"))
        self.verticalLayout_3.addWidget(self.rig_pushButton)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.checkBox = QtGui.QCheckBox(Form)
        self.checkBox.setObjectName(_fromUtf8("checkBox"))
        self.horizontalLayout_2.addWidget(self.checkBox)
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem)
        self.res_comboBox = QtGui.QComboBox(Form)
        self.res_comboBox.setObjectName(_fromUtf8("res_comboBox"))
        self.horizontalLayout_2.addWidget(self.res_comboBox)
        self.verticalLayout_3.addLayout(self.horizontalLayout_2)
        self.export_pushButton = QtGui.QPushButton(Form)
        self.export_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        self.export_pushButton.setObjectName(_fromUtf8("export_pushButton"))
        self.verticalLayout_3.addWidget(self.export_pushButton)
        self.verticalLayout_3.setStretch(1, 4)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        Form.setWindowTitle(QtGui.QApplication.translate("Form", "Form", None, QtGui.QApplication.UnicodeUTF8))
        self.label.setText(QtGui.QApplication.translate("Form", "Project", None, QtGui.QApplication.UnicodeUTF8))
        self.label_2.setText(QtGui.QApplication.translate("Form", "Asset Name", None, QtGui.QApplication.UnicodeUTF8))
        self.label_3.setText(QtGui.QApplication.translate("Form", "-", None, QtGui.QApplication.UnicodeUTF8))
        self.label_4.setText(QtGui.QApplication.translate("Form", "-", None, QtGui.QApplication.UnicodeUTF8))
        self.label_5.setText(QtGui.QApplication.translate("Form", "TextLabel", None, QtGui.QApplication.UnicodeUTF8))
        self.snap_pushButton.setText(QtGui.QApplication.translate("Form", "Snap", None, QtGui.QApplication.UnicodeUTF8))
        self.save_pushButton.setText(QtGui.QApplication.translate("Form", "Save current Position", None, QtGui.QApplication.UnicodeUTF8))
        self.center_pushButton.setText(QtGui.QApplication.translate("Form", "Center", None, QtGui.QApplication.UnicodeUTF8))
        self.rig_pushButton.setText(QtGui.QApplication.translate("Form", "Rig", None, QtGui.QApplication.UnicodeUTF8))
        self.checkBox.setText(QtGui.QApplication.translate("Form", "Reference after export", None, QtGui.QApplication.UnicodeUTF8))
        self.export_pushButton.setText(QtGui.QApplication.translate("Form", "Export", None, QtGui.QApplication.UnicodeUTF8))

