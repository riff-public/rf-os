# v.0.0.1 polytag switcher
_title = 'PropIt'
_version = 'v.1.0.2'
_des = 'Support Redshift proxy'
uiName = 'PropItTool'

import os
import sys
import getpass
import logging
from collections import OrderedDict

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

# import config
# framework modules
import rf_config as config
from rf_utils import log_utils

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

from rf_utils.widget import display_widget
from rf_utils.widget import snap_widget
from rf_app.publish.asset import maya_hook
from rf_app.publish.asset import utils
from rf_app.publish.asset import sg_hook
# export modules
from rf_app.export.asset_lib import export_propit_rig
reload(export_propit_rig)
from rf_app.export.asset_lib import export_gpu
from rf_app.export.asset_lib import export_cache
from rf_app.export.asset_lib import export_workspace
from rf_app.export.asset_lib import export_material
from rf_app.export.asset_lib import export_redshift_proxy_propit
from rf_app.export.asset_lib import export_maya_rsproxy
from rf_utils.widget import dialog
from rf_utils.context import context_info
from rf_utils import publish_info
from rf_utils import register_entity
from rf_utils import user_info
from rf_utils import file_utils
from rftool.utils import maya_utils
from rf_app.asm import asm_lib
reload(asm_lib)
reload(export_workspace)
from rf_utils import icon 
from rf_app.propit2 import build_mattepaint_scene
# from rf_utils.sg import sg_process
# sg = sg_process.sg 

import maya.cmds as mc
import subprocess

class LocalConfig:
    iconSize = 60
    snapIcon = '%s/icons/snap.png' % moduleDir
    taskMapping = {'model': {'pr': 'model_pr', 'md': 'model'}, 'rig': {'pr': 'rig_pr', 'md': 'rig'},'texture': {'md': 'texture'}, 'lookdev': {'md': 'lookdev'}}


class UI(QtWidgets.QWidget):
    def __init__(self, parent=None) :
        super(UI, self).__init__(parent)

        # tab index
        self.allLayout = QtWidgets.QVBoxLayout()

        # set title
        self.title_layout = self.add_title(self.allLayout)
        self.add_snap(self.allLayout)
        self.add_line(self.allLayout)
        self.add_prop_tool(self.allLayout)
        self.add_buttom_layout(self.allLayout)
        self.allLayout.setStretch(1, 4)
        self.setLayout(self.allLayout)

    def add_title(self, parentLayout):
        title_layout = QtWidgets.QGridLayout()
        self.projectLabel = QtWidgets.QLabel('Project')
        self.projectInfo = QtWidgets.QLabel('-')
        self.assetNameLabel = QtWidgets.QLabel('Asset Name')
        self.assetNameInfo = QtWidgets.QLabel('-')

        title_layout.addWidget(self.projectLabel, 0, 0, 1, 1)
        title_layout.addWidget(self.assetNameLabel, 1, 0, 1, 1)
        title_layout.addWidget(self.projectInfo, 0, 1, 1, 1)
        title_layout.addWidget(self.assetNameInfo, 1, 1, 1, 1)

        title_layout.setColumnStretch(0, 1)
        title_layout.setColumnStretch(1, 3)

        parentLayout.addLayout(title_layout)
        return title_layout

    def add_snap(self, parentLayout):

        # frame
        self.frame = QtWidgets.QFrame()
        self.frame.setFrameShape(QtWidgets.QFrame.Box)
        self.frame.setFrameShadow(QtWidgets.QFrame.Sunken)

        self.frame_layout = QtWidgets.QVBoxLayout(self.frame)
        self.snapPreview = display_widget.Display()
        self.frame_layout.addWidget(self.snapPreview)
        parentLayout.addWidget(self.frame)

        # snap button
        self.snapButton = snap_widget.SnapButton()
        self.snapButton.set_icon(LocalConfig.snapIcon)
        self.mayaSnap_checkBox = QtWidgets.QCheckBox('Maya Viewport Snap')

        parentLayout.addWidget(self.mayaSnap_checkBox)
        parentLayout.addWidget(self.snapButton)

        return self.frame

    def add_line(self, parentLayout):
        # line
        self.line = QtWidgets.QFrame()
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        parentLayout.addWidget(self.line)

        return self.line

    def add_prop_tool(self, parentLayout):
        # self.regObject_pushButton = QtWidgets.QPushButton('Define objects')
        self.center_pushButton = QtWidgets.QPushButton('Center Asset')

        # parentLayout.addWidget(self.regObject_pushButton)
        parentLayout.addWidget(self.center_pushButton)

    def add_buttom_layout(self, parentLayout):
        self.rig_pushButton = QtWidgets.QPushButton('Rig')
        self.rig_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        parentLayout.addWidget(self.rig_pushButton)

        # add qc button
        self.qc_pushButton = QtWidgets.QPushButton('QC')
        self.qc_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        parentLayout.addWidget(self.qc_pushButton)

        self.exportOptionLayout = QtWidgets.QHBoxLayout()

        # widgets
        self.ref_checkBox = QtWidgets.QCheckBox('Reference back in')
        self.masterIcon_checkBox = QtWidgets.QCheckBox('Replace asset icon')

        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.res_comboBox = QtWidgets.QComboBox()

        self.exportOptionLayout.addWidget(self.ref_checkBox)
        self.exportOptionLayout.addWidget(self.masterIcon_checkBox)
        self.exportOptionLayout.addItem(spacerItem)
        self.exportOptionLayout.addWidget(self.res_comboBox)

        parentLayout.addLayout(self.exportOptionLayout)

        # sg layout 
        self.sgLayout = QtWidgets.QHBoxLayout()
        self.statusLabel = QtWidgets.QLabel('Complete')
        self.modelCheckBox = QtWidgets.QCheckBox('Model')
        self.textureCheckBox = QtWidgets.QCheckBox('Texture')
        self.rigCheckBox = QtWidgets.QCheckBox('Rig')
        self.lookdevCheckBox = QtWidgets.QCheckBox('Lookdev')
        self.mattepaintCheckBox = QtWidgets.QCheckBox('Mattepaint')

        self.sgLayout.addWidget(self.modelCheckBox)
        self.sgLayout.addWidget(self.textureCheckBox)
        self.sgLayout.addWidget(self.rigCheckBox)
        self.sgLayout.addWidget(self.lookdevCheckBox)
        self.sgLayout.addWidget(self.mattepaintCheckBox)
        
        self.add_line(parentLayout)
        parentLayout.addWidget(self.statusLabel)

        parentLayout.addLayout(self.sgLayout)

        self.export_pushButton = QtWidgets.QPushButton('Export')
        self.export_pushButton.setMinimumSize(QtCore.QSize(0, 30))

        parentLayout.addWidget(self.export_pushButton)

        return self.exportOptionLayout



class PropIt(QtWidgets.QMainWindow):
    """docstring for Main"""
    def __init__(self, contextPathInfo, sgEntity, mainWindow=None, parent=None):
        super(PropIt, self).__init__(parent=parent)
        self.ui = UI(self)
        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)

        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.resize(320, 530)
        self.statusBar()

        self.parent = parent
        self.asset = contextPathInfo
        self.sgEntity = sgEntity
        self.mainWindow = mainWindow
        self.defaultVersion = 'p001'
        self.capturePath = ''
        # self.taskMapping = {'model': {'pr': 'model_pr', 'md': 'model'}, 'rig': {'pr': 'rig_pr', 'md': 'rig'}, 'lookdev': {'md': 'lookdev'}}
        self.taskMapping = LocalConfig.taskMapping
        self.savePos = None

        self.set_ui()
        self.init_signals()
        self.init_functions()

    def set_ui(self): 
        self.ui.modelCheckBox.setChecked(True)
        self.ui.textureCheckBox.setChecked(True)
        self.ui.rigCheckBox.setChecked(True)
        self.ui.lookdevCheckBox.setChecked(False)
        self.ui.mattepaintCheckBox.setChecked(False)

    def init_signals(self):
        self.ui.mayaSnap_checkBox.stateChanged.connect(self.set_snap_mode)
        self.ui.snapButton.captured.connect(self.set_display)
        self.ui.snapButton.precaptured.connect(self.hide_ui)

        # save position
        self.ui.center_pushButton.clicked.connect(self.center_object)

        # rig
        self.ui.rig_pushButton.clicked.connect(self.rig_prop)
        # qc 
        self.ui.qc_pushButton.clicked.connect(self.run_qc)

        # export
        self.ui.export_pushButton.setEnabled(0)
        self.ui.export_pushButton.clicked.connect(self.export)

    def init_functions(self):
        # set information
        self.set_info()
        self.userInfo = user_info.User()

    def set_info(self):
        self.ui.projectInfo.setText(self.asset.project) if hasattr(self.asset, 'project') else None
        self.ui.assetNameInfo.setText(self.asset.name) if hasattr(self.asset, 'name') else None
        self.ui.res_comboBox.addItems(self.asset.list_res())

    def set_snap_mode(self, value):
        state = True if value else False
        override = maya_hook.capture_screen if state else None
        self.ui.snapButton.captureCommand = override

    def set_display(self, path):
        """ send path to display image after snapped """
        self.capturePath = path
        self.ui.snapPreview.set_display(path)
        if not self.ui.mayaSnap_checkBox.isChecked():
            if hasattr(self.mainWindow,'show'): 
                self.mainWindow.show()
            self.show()

    def hide_ui(self, state):
        """ hide ui before snap """
        if not self.ui.mayaSnap_checkBox.isChecked():
            self.hide()
            if hasattr(self.mainWindow,'hide'): 
                self.mainWindow.hide()


    def center_object(self):
        """ center selected objects """
        mc.undoInfo(openChunk=True)
        objGroup, self.savePos = maya_hook.group_objects()
        maya_hook.center_grid(objGroup)
        maya_hook.freeze_transform(objGroup)
        maya_hook.select(objGroup)
        mc.undoInfo(closeChunk=True)

    def rig_prop(self):

        """ create control rig """
        from rftool.rig.ncmel import mainRig
        reload(mainRig)

        currentScene = maya_hook.current_scene()
        entity = context_info.ContextPathInfo(path=currentScene)
        res_dict = {'pr': entity.projectInfo.asset.pr_geo_grp(), 
            'md': entity.projectInfo.asset.md_geo_grp(), 
            'lo':entity.projectInfo.asset.geo_grp(), 
            'hi':entity.projectInfo.asset.geo_grp()}
        res = str(self.ui.res_comboBox.currentText())

        mc.undoInfo(openChunk=True)
        # objects = maya_hook.ls_selection()
        rig = mainRig.Prop(pars=res_dict[res])
        # maya_hook.set_parent(objects, res_dict[res]) if objects else None
        mc.undoInfo(closeChunk=True)

    def run_qc(self): 
        from rf_qc import qc_widget
        reload(qc_widget)
        asset = self.asset.copy()
        asset = context_info.ContextPathInfo()
        app = qc_widget.show(configFilter=True)
        step = 'propit'
        res = str(self.ui.res_comboBox.currentText())
        
        if self.sgEntity.get('sg_asset_type') in ['mattepaint']: 
            step = 'mattepaint'
        
        app.ui.update(asset.project, asset.entity_type, step, entity=asset, res=res)
        app.ui.check.connect(self.check_qc_export)
        
    def check_qc_export(self, status):
        if status == True:
            self.ui.export_pushButton.setEnabled(1)


    def replace_back(self):
        """ replace reference back in """
        loc = self.create_gpu()
        loc.set_pos(self.savePos) if self.savePos else None
        loc.select()

    def create_gpu(self):
        """ create gpu """
        gpuPath = self.gpuOutputs[-1] # hero on last index
        name = asm_lib.asm_name(self.asset.name)
        self.asset.context.update(look='main')

        # create locator
        loc = asm_lib.MayaRepresentation(name, True)
        loc.set(gpuPath, self.asset)
        # build gpu
        res = str(self.ui.res_comboBox.currentText())
        loc.build('gpu', res)
        return loc

    def export(self):
        """ export rig and gpu """
        # export model
        publishSteps = []
        if self.ui.modelCheckBox.isChecked():
            publishSteps.append('model')
        if self.ui.rigCheckBox.isChecked():
            publishSteps.append('rig')
        if self.ui.textureCheckBox.isChecked():
            publishSteps.append('texture')
        if self.ui.lookdevCheckBox.isChecked():
            publishSteps.append('lookdev')
        if self.ui.mattepaintCheckBox.isChecked():
            publishSteps.append('mattepaint')

        description = 'Layout create proxy'
        res = str(self.ui.res_comboBox.currentText())
        self.gpuOutputs = []

        publishInfo = self.register_version(step='model', res=res)
        rigGrp = publishInfo.entity.projectInfo.asset.top_grp()
        geoGrp = publishInfo.entity.projectInfo.asset.geo_grp()
        exportGrp = publishInfo.entity.projectInfo.asset.export_grp()
        asset_type = publishInfo.entity.type
        

        # check all inputs
        if self.check_input(rigGrp, geoGrp):
            self.print_mesg('Exporting ...')
            self.set_master_icon(self.capturePath) if self.ui.masterIcon_checkBox.isChecked() else None
            for step in publishSteps:
                # register
                publishInfo = self.register_version(step=step, res=res)
                publishInfo.register()
                look = False
                publish = False

                if step == 'model':
                    # export
                    exportResult, self.gpuOutputs = export_gpu.run(publishInfo)
                    self.print_mesg('Gpu exported')
                    export_cache.run(publishInfo)
                    self.print_mesg('Cache exported')
                    publish = True
                    look = False

                if step == 'rig':
                    publishTexture = publishInfo.entity.path.publish_texture().abs_path()
                    maya_utils.move_texture_path(publishTexture, rigGrp, resize=False, relink=True)
                    export_propit_rig.run(publishInfo, runProcess=False)
                    self.print_mesg('Rig exported')
                    publish = True
                    look = False

                if step == 'texture': 
                    maya_hook.toggle_group(geoGrp, exportGrp, geo2=True)
                    export_material.run(publishInfo)
                    self.print_mesg('Material exported')
                    look = True

                if step == 'lookdev': 
                    if res == 'md': 
                        # prepare group 
                        maya_hook.toggle_group(geoGrp, exportGrp, geo2=True)
                        # copy texture to publish and re link for proxy 
                        publishTexture = publishInfo.entity.path.publish_texture().abs_path()
                        replaceDict = maya_utils.move_texture_path(publishTexture, exportGrp, resize=False, relink=True)

                        if asset_type == 'mattepaint':
                            design_path = self.design_path(publishInfo.entity)
                            for old_path, new_path in replaceDict.iteritems():
                                file_name = os.path.basename(new_path)
                                design_img_work = os.path.join(design_path, file_name)
                                file = file_utils.copy(new_path, design_img_work)
                                print file
                        # export material 
                        export_material.run(publishInfo)

                        # export proxy
                        export_redshift_proxy_propit.run(publishInfo)
                        export_maya_rsproxy.run(publishInfo)
                        # maya_hook.toggle_group(geoGrp, exportGrp, geo1=True)
                        self.print_mesg('Material exported')
                        look = True
                        publish = True
                    else: 
                        continue

                if step == 'mattepaint':

                    project = self.asset.project
                    assetName = publishInfo.entity.name

                    # get environments for the project and use them in mayapy
                    logger.info('Creating environments for mayapy...')
                    version = mc.about(v=True)
                    # project = asset.project
                    logger.info('Project: %s' %(project))
                    logger.info('Version: %s' %(version))
                    department = 'Py'

                    # merge environments with current environments
                    envStr = self.asset.projectInfo.get_project_env(dcc='maya', dccVersion=version, department=department)

                    mayapy = maya_utils.pyinterpreter()
                    mc.waitCursor(st=True)
                    genSceneProcess = subprocess.Popen([mayapy, build_mattepaint_scene.__file__, '-p', project, '-a', assetName], 
                                       stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True, env=envStr)
                    output, error = genSceneProcess.communicate()
                    # print error
                    mc.waitCursor(st=False)

                    # executable = 'C:/Program Files/Autodesk/Maya2018/bin/mayapy.exe'

                export_workspace.propit_run(publishInfo)
                self.print_mesg('workspace exported')

                # export img
                img, imgHero = self.export_img(self.asset, self.capturePath)
                self.print_mesg('snap image published')

                versionEntity = dict()
                taskName = self.taskMapping[self.asset.step][res]
                if publish: 
                    # sg
                    self.print_mesg('updating shotgun ...')
                    versionEntity = self.publish_sg(self.asset, taskName, [img], description)
                    self.print_mesg('finished')

                # register asset
                print '-->>', self.asset
                self.register_ad(self.asset, self.sgEntity, [img, imgHero], taskName, versionEntity, description, look=look)
                self.print_mesg('%s asset registered' % step)

            # remove after finish
            maya_hook.remove_object(exportGrp)
            self.replace_back() if self.ui.ref_checkBox.isChecked() else None
            # omit rig 
            if res == 'pr': 
                self.skip_rig(self.asset)
                self.print_mesg('Skip rig')
            dialog.MessageBox.success('Complete', 'Export %s complete' % self.asset.name)
            self.print_mesg('Export finished')

    def design_path(self, asset_entity): ## mattepaint
        context = context_info.Context()
        proj = context_info.ContextPathInfo().project
        context.update(entityType = 'design', project= proj, process = 'main', entity = asset_entity.name, entityGrp=asset_entity.type)
        design_entity = context_info.ContextPathInfo(context=context)
        return design_entity.path.workspace().abs_path()

    def publish_sg(self, asset, taskName, imgs, description):
        status = 'apr'

        # user
        userEntity = self.userInfo.sg_user()
        # task
        taskEntity = sg_hook.find_task(asset.project, asset.name, taskName)

        if taskEntity: 
            # version
            self.print_mesg('creating version and uploading to shotgun ...')
            versionResult = sg_hook.publish_version(asset, taskEntity, status, imgs, userEntity, description)
            # update task
            taskResult = sg_hook.update_task(asset, taskEntity, status)
            # trigger next step
            self.print_mesg('update task status to shotgun')
            triggerTaskResult = sg_hook.update_dependency(asset, taskEntity, status)
            return versionResult
        else: 
            self.print_mesg('Task not found "%s"' % taskName)

    def skip_rig(self, asset): 
        status = 'omt'
        taskName = LocalConfig.taskMapping['rig']['md']
        taskEntity = sg_hook.find_task(asset.project, asset.name, taskName)
        taskResult = sg_hook.update_task(asset, taskEntity, status)
        return taskResult

    def check_input(self, rigGrp, geoGrp):
        messages = []

        if not self.capturePath:
            messages.append('Snap asset not found')

        if not maya_hook.object_exists(rigGrp):
            messages.append('Rig Grp not found')

        if not maya_hook.object_exists(geoGrp):
            messages.append('Geo Grp not found')

        if not messages:
            return True
        else:
            messageStr = '\n'.join(messages)
            dialog.MessageBox.warning('Missing input', messageStr)

    def export_img(self, asset, srcImg):
        # collect imgs
        img, imgHero = self.collect_imgs(asset, srcImg)
        return img, imgHero

    def register_version(self, step, res):
        """ register when export """
        # self.asset.context.update(version='v001') # fix v001 from propit
        sceneVersion = self.get_scene_version()
        self.asset.context.update(step=step, process='main', look='main', app='maya', res=res, version=sceneVersion)
        preregInfo = publish_info.PreRegister(self.asset)
        return preregInfo


    def register_ad(self, asset, sgEntity, medias, taskName, versionEntity, description, look=False):
        """ register asset description """
        # register entity
        outputImgPath = os.path.dirname(medias[0])
        outputHeroImgPath = os.path.dirname(medias[1])

        output = register_entity.Output()
        output.add_process(asset.process) if not look else output.add_look(asset.look)
        output.add_version_id(versionEntity['id']) if versionEntity else None
        output.add_version(asset.publishVersion)
        output.add_media(outputImgPath, outputHeroImgPath)
        output.add_preview(medias[0]) if medias else None
        output.add_task(taskName)
        output.add_description(description)

        assetDescription = register_entity.Register(asset)
        assetDescription.set(output=output)
        assetDescription.add_asset_id(sgEntity['id'])
        assetDescription.register()

    def collect_imgs(self, asset, image):
        outputImgPath = asset.path.outputImg().abs_path()
        outputHeroImgPath = asset.path.output_hero_img().abs_path()
        imgName = asset.publish_name(ext='.png')
        imgHeroName = asset.publish_name(ext='.png', hero=True)

        status, outputs = utils.collect_imgs(outputImgPath, imgName, [image])
        statusHero, outputsHero = utils.collect_imgs(outputHeroImgPath, imgHeroName, [image])
        result1 = outputs[0] if outputs else ''
        result2 = outputsHero[0] if outputsHero else ''
        return result1, result2

    def set_master_icon(self, imagePath):
        """ capture image as master icon """
        if os.path.exists(imagePath):
            basename, ext = os.path.splitext(imagePath)
            iconDir = self.asset.path.icon().abs_path()
            iconName = self.asset.icon_name(ext=ext)
            dst = '%s/%s' % (iconDir, iconName)
            file_utils.copy(imagePath, dst)
            return dst


    def get_scene_version(self):
        currentScene = maya_hook.current_scene()
        asset = context_info.ContextPathInfo(path=currentScene)
        version = asset.version.replace('v', 'p') if hasattr(asset, 'version') else self.defaultVersion
        return version

    def print_mesg(self, text=''):
        self.statusBar().showMessage(text)



def show(contextPathInfo=None, sgEntity=None, mainWindow=None):
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('%s Running in Maya\n' % _title)
        maya_win.deleteUI(uiName)
        myApp = PropIt(contextPathInfo=contextPathInfo, sgEntity=sgEntity, mainWindow=mainWindow, parent=maya_win.getMayaWindow())
        myApp.show()
        return myApp



def publish_rig(asset): 
    description = 'auto rig'
    img = icon.nopreview
    imgHero = icon.nopreview
    publishInfo = register_version(asset, step=asset.step, res=asset.res)
    publishInfo.register()
    look = False

    export_propit_rig.run(publishInfo, runProcess=False)
    print_mesg('Rig exported')
    publish = True

    export_workspace.propit_run(publishInfo)
    print_mesg('workspace exported')

    # export img
    # img, imgHero = self.export_img(asset, self.capturePath)
    print_mesg('snap image published')

    versionEntity = dict()
    taskName = LocalConfig.taskMapping[asset.step][asset.res]

    if publish: 
        # sg
        print_mesg('updating shotgun ...')
        versionEntity = publish_sg(asset, taskName, [img], description)
        print_mesg('finished')                                                                                                                                                                                                                                        

    # register asset
    print '-->>', asset
    register_ad(asset, asset.context.sgEntity, [img, imgHero], taskName, versionEntity, description, look=look)
    print_mesg('%s asset registered' % asset.step)



def register_version(asset, step, res): 
    sceneVersion = 'v001'
    asset.context.update(step=step, process='main', look='main', app='maya', res=res, version=sceneVersion)
    preregInfo = publish_info.PreRegister(asset)
    return preregInfo


def print_mesg(message): 
    logger.info(message)


def register_ad(asset, sgEntity, medias, taskName, versionEntity, description, look=False):
    """ register asset description """
    # register entity
    outputImgPath = os.path.dirname(medias[0])
    outputHeroImgPath = os.path.dirname(medias[1])

    output = register_entity.Output()
    output.add_process(asset.process) if not look else output.add_look(asset.look)
    output.add_version_id(versionEntity['id']) if versionEntity else None
    output.add_version(asset.publishVersion)
    output.add_media(outputImgPath, outputHeroImgPath)
    output.add_preview(medias[0]) if medias else None
    output.add_task(taskName)
    output.add_description(description)

    assetDescription = register_entity.Register(asset)
    assetDescription.set(output=output)
    assetDescription.add_asset_id(sgEntity['id'])
    assetDescription.register()


def publish_sg(asset, taskName, imgs, description):
    status = 'apr'

    # user
    userEntity = None
    # task
    taskEntity = sg_hook.find_task(asset.project, asset.name, taskName)

    if taskEntity: 
        # version
        print_mesg('creating version and uploading to shotgun ...')
        versionResult = sg_hook.publish_version(asset, taskEntity, status, imgs, userEntity, description)
        # update task
        taskResult = sg_hook.update_task(asset, taskEntity, status)
        # trigger next step
        print_mesg('update task status to shotgun')
        triggerTaskResult = sg_hook.update_dependency(asset, taskEntity, status)
        return versionResult
    else: 
        print_mesg('Task not found "%s"' % taskName)

# example useage 
# path = 'P:/projectName/asset/publ/prop/treeMedH/hero/treeMedH_rig_main_md.hero.ma'
# from rf_app.propit2 import propit_app
# reload(propit_app)
# from rf_utils.sg import sg_utils
# from rf_utils.context import context_info
# asset = context_info.ContextPathInfo(path=path)
# asset.context.update(step='rig', res='md')
# asset.context.use_sg(sg_utils.sg, project=asset.project, entityType='asset', entityName=asset.name)
# propit_app.publish_rig(asset)
