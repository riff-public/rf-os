# -*- coding: utf-8 -*-
import sys
import os
import getpass
import time
from views.list_files_view import ListFilesWidget

core_path = '%s/core' % os.environ.get('RFSCRIPT')
if core_path not in sys.path:
    sys.path.append(core_path)

import rf_config as config

# from models.outsource_checkin import OutsourceCheckinModel
from models import outsource_checkin
reload(outsource_checkin)

from rf_utils.ui import load, stylesheet
from rf_utils import log_utils
os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide2', 'PySide'])
from Qt import QtWidgets, QtCore, QtGui
from rf_utils import icon

userLog = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
UI_NAME = "Outsource CheckIn"
logFile = log_utils.name(UI_NAME, userLog)
logger = log_utils.init_logger(logFile)

UI_PATH = "{core}/rf_app/outsource_check_in/views/outsource_check_in.ui".format(core=core_path)
SHOTGUN_URL = "https://riffanimation.shotgunstudio.com"


class OutsourceCheckin(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super(OutsourceCheckin, self).__init__(parent)
        self.drag_drop_list_widget = ListFilesWidget(self)
        if config.isMaya:
            self.ui = load.setup_ui_maya(UI_PATH, parent)
        else:
            self.ui = load.setup_ui(UI_PATH, self)

        self.init_ui()
        self.init_state_app()
        self.init_signal()
        self.ui.show()

    def init_ui(self):
        self.ui.drop_files_layout.addWidget(self.drag_drop_list_widget)
        self.ui.processing_txt.setVisible(False)

    def init_signal(self):
        self.drag_drop_list_widget.dropped.connect(self.dropped_files)
        self.ui.import_btn.clicked.connect(self.import_clicked)
        self.ui.project_combobox.currentIndexChanged.connect(self.project_combobox_changed)

    def init_state_app(self):
        self.outsource_check_in_model = outsource_checkin.OutsourceCheckinModel()
        self.init_project_combobox()
        # self.init_episode_combobox()
        self.init_steps_combobox()
        if self.outsource_check_in_model.user_pref_data:
            self.init_default_setting()

        app_icon = QtGui.QIcon()
        app_icon.addFile(icon.checkin, QtCore.QSize(16, 16))
        self.ui.setWindowTitle("Outsource Check in")
        self.ui.setWindowIcon(app_icon)

    def init_project_combobox(self):
        self.ui.project_combobox.blockSignals(True)
        self.ui.project_combobox.clear()
        projects = self.outsource_check_in_model.get_projects()
        if len(projects) == 0:
            self.ui.project_combobox.addItem("Not Found")
        else:
            for row, project in enumerate(projects):
                self.ui.project_combobox.addItem(project.get('name'))
                self.ui.project_combobox.setItemData(row, project, QtCore.Qt.UserRole)
        self.ui.project_combobox.blockSignals(False)
        self.project_combobox_changed()

    def init_episodes_combobox(self):
        self.ui.episode_combobox.blockSignals(True)
        self.ui.episode_combobox.clear()
        episodes = self.outsource_check_in_model.get_episodes()
        if len(episodes) == 0:
            self.ui.episode_combobox.addItem("Not Found")
        else:
            for row, episode in enumerate(episodes):
                self.ui.episode_combobox.addItem(episode.get('code'))
                self.ui.episode_combobox.setItemData(row, episode, QtCore.Qt.UserRole)
        self.ui.episode_combobox.blockSignals(False)

    def init_steps_combobox(self):
        self.ui.step_combobox.blockSignals(True)
        self.ui.step_combobox.clear()
        steps = self.outsource_check_in_model.get_steps()
        if len(steps) == 0:
            self.ui.step_combobox.addItem("Not Found")
        else:
            for row, steps in enumerate(steps):
                self.ui.step_combobox.addItem(steps)
                self.ui.step_combobox.setItemData(row, steps, QtCore.Qt.UserRole)
        self.ui.step_combobox.blockSignals(False)

    def get_project_selected(self):
        index = self.ui.project_combobox.currentIndex()
        project_selected = self.ui.project_combobox.itemData(index, QtCore.Qt.UserRole)
        return project_selected

    def get_episode_selected(self):
        index = self.ui.episode_combobox.currentIndex()
        episode_selected = self.ui.episode_combobox.itemData(index, QtCore.Qt.UserRole)
        return episode_selected

    def get_step_selected(self):
        index = self.ui.step_combobox.currentIndex()
        step_selected = self.ui.step_combobox.itemData(index, QtCore.Qt.UserRole)
        return step_selected

    def get_items_files_list_widget(self):
        files_path = []
        for i in range(self.drag_drop_list_widget.count()):
            files_path.append(self.drag_drop_list_widget.item(i).text())
        return files_path

    def project_combobox_changed(self):
        project_selected = self.get_project_selected()
        self.outsource_check_in_model.set_project_selected(project_selected)
        self.outsource_check_in_model.set_episodes()
        self.init_episodes_combobox()

    def dropped_files(self, links):
        for url in links:
            if os.path.exists(url):
                # print(url)
                # icon = QtGui.QIcon(url)
                # pixmap = icon.pixmap(72, 72)
                # icon = QtGui.QIcon(pixmap)
                widget_item = QtWidgets.QListWidgetItem(self.drag_drop_list_widget)
                widget_item.setText(url)
                widget_item.setData(QtCore.Qt.UserRole, url)
                self.drag_drop_list_widget.addItem(widget_item)
                # item.setIcon(icon)
                # item.setStatusTip(url)

    def import_clicked(self):
        start = time.time()
        self.loading_start()
        project_selected = self.get_project_selected()
        episode_selected = self.get_episode_selected()
        step_selected = self.get_step_selected()
        files_items = self.get_items_files_list_widget()

        QtCore.QCoreApplication.processEvents()
        try:
            self.outsource_check_in_model.import_outsource_work(
                project_selected,
                episode_selected,
                step_selected,
                files_items
            )
        except Exception as e:
            tb = sys.exc_info()[2]
            print(e)
            print("line number : %s", tb.tb_lineno)
            self.show_critical_box(e)
        finally:
            end = time.time()
            print("total time: ", end - start)
            self.outsource_check_in_model.save_setting(
                project_selected,
                episode_selected,
                step_selected
            )
            self.loading_stop()

    def init_default_setting(self):
        index_project = self.ui.project_combobox.findText(
            self.outsource_check_in_model.user_pref_data['default_project'],
            QtCore.Qt.MatchFixedString
        )
        if index_project >= 0:
            self.ui.project_combobox.setCurrentIndex(index_project)
        self.project_combobox_changed()

        index_episode = self.ui.episode_combobox.findText(
            self.outsource_check_in_model.user_pref_data['default_episode'],
            QtCore.Qt.MatchFixedString
        )
        if index_episode >= 0:
            self.ui.episode_combobox.setCurrentIndex(index_episode)

        index_step = self.ui.step_combobox.findText(
            self.outsource_check_in_model.user_pref_data['default_step'],
            QtCore.Qt.MatchFixedString
        )
        if index_step >= 0:
            self.ui.step_combobox.setCurrentIndex(index_step)

    def loading_start(self):
        self.ui.processing_txt.setVisible(True)

    def loading_stop(self):
        self.ui.processing_txt.setVisible(False)

    def show_critical_box(self, msg):
        QtWidgets.QMessageBox.critical(self, "Message", "Error:" + str(msg) + ". Please contact admin.")


def show():
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        maya_win.deleteUI(UI_NAME)
        myApp = OutsourceCheckin(maya_win.getMayaWindow())
        return myApp
    else:
        logger.info('Run in Outsource check in\n')
        app = QtWidgets.QApplication.instance()
        if not app:
            app = QtWidgets.QApplication(sys.argv)
        myApp = OutsourceCheckin()
        stylesheet.set_default(app)
        sys.exit(app.exec_())


if __name__ == '__main__':
    show()
