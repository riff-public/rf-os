import os
import subprocess
import shutil
from rf_utils import user_info
from rf_app.publish.asset import sg_hook
from rf_utils.sg import sg_process
import rf_config as config
from rf_utils import admin



def convert_mov(
    file_path,
    output_media_path,
    hero_file_name,
    review_file_name,
    preview_file_name,
    fps
):
    subprocess.call([
        config.Software.ffmpeg,
        '-y',
        '-apply_trc',
        'iec61966_2_1',
        '-start_number',
        '1001',
        '-framerate',
        str(fps),
        '-i',
        file_path,
        '-vcodec',
        'libx264',
        '-crf',
        '20',
        '-tune',
        'film',
        '-preset',
        'veryslow',
        '-x264opts',
        'bframes=1',
        '-vf',
        'scale=iw/2:ih/2, scale=out_color_matrix=bt709',
        '-pix_fmt',
        'yuv420p',
        '-r',
        str(fps),
        "{output_media_path}/{preview_file_name}".format(output_media_path=output_media_path, preview_file_name=preview_file_name)
    ])
    subprocess.call([
        config.Software.ffmpeg,
        '-y',
        '-apply_trc',
        'iec61966_2_1',
        '-start_number',
        '1001',
        '-framerate',
        str(fps),
        '-i',
        file_path,
        '-vcodec',
        'libx264',
        '-crf',
        '10',
        '-tune',
        'film',
        '-preset',
        'veryslow',
        '-x264opts',
        'bframes=1',
        '-vf',
        'scale=out_color_matrix=bt709',
        '-pix_fmt',
        'yuv420p',
        '-r',
        str(fps),
       "{output_media_path}/{review_file_name}".format(output_media_path=output_media_path, review_file_name=review_file_name)
    ])
    subprocess.call([
        config.Software.ffmpeg,
        '-y',
        '-apply_trc',
        'iec61966_2_1',
        '-start_number',
        '1001',
        '-framerate',
        str(fps),
        '-i',
        file_path,
        '-vcodec',
        'prores_ks',
        '-profile:v',
        '4444',
        '-quant_mat',
        'hq',
        '-vf',
        'scale=out_color_matrix=bt709',
        '-pix_fmt',
        'yuv444p10le',
        '-r',
        str(fps),
        "{output_media_path}/{hero_file_name}".format(output_media_path=output_media_path, hero_file_name=hero_file_name)
    ])


def get_file_name(scene_context, version, step_selected):
    shot_name = scene_context.name
    hero_file_name = "{shot_name}_{step}_main.{version}.mov".format(
        shot_name=shot_name,
        step=step_selected,
        version=version
    )
    review_file_name = "{shot_name}_{step}_main.{version}_review.mov".format(
        shot_name=shot_name,
        step=step_selected,
        version=version
    )
    preview_file_name = "{shot_name}_{step}_main.{version}_preview.mov".format(
        shot_name=shot_name,
        step=step_selected,
        version=version
    )

    return hero_file_name, review_file_name, preview_file_name


def backup_version(version, publ_hero_path, backup_path):
    backup_version_path = "{backup_path}/{version}".format(
        backup_path=backup_path,
        version=version
    )
    if os.path.exists(backup_version_path):
        shutil.rmtree(backup_version_path)
    shutil.copytree(publ_hero_path, backup_version_path)
    print 'BACK UP'
    print publ_hero_path
    print backup_version_path

def copy_to_publish(version, backup_path, publ_hero_path):
    backup_version_path = "{backup_path}/{version}".format(
        backup_path=backup_path,
        version=version
    )
    if os.path.exists(publ_hero_path):
        admin.rmtree(publ_hero_path)
    shutil.copytree(backup_version_path, publ_hero_path)
    print 'Copy to publish'


def get_task(sg_process, scene, taskName):
    scene.context.use_sg(
        sg_process.sg,
        project=scene.project,
        entityType='scene',
        entityName=scene.name
    )
    taskEntity = sg_process.get_one_task_by_step(
        scene.context.sgEntity,
        scene.step,
        taskName,
        create=True
    )
    return taskEntity


def publish_version(preview_file_name, scene_context, version, step_selected, review_file_path):
    scene_context.context.update(publishVersion=version)
    task_entity = get_task(sg_process, scene_context, step_selected)
    user = user_info.User()
    versionResult = sg_hook.publish_version(
        scene_context,
        task_entity,
        'rev',
        [preview_file_name],
        user.sg_user(),
        "Upload by import outsoruce check in publish",
        uploadOption="Comp publish Upload",
        localMedia=review_file_path
    )
    return versionResult


def rename_file_version_to_hero(output_media_path, hero_file_name):
    file_name_split = hero_file_name.split('.')
    file_name_split[1] = "hero"
    result_filename = ".".join(file_name_split)

    media_old_file = os.path.join(output_media_path, hero_file_name)
    media_new_file = os.path.join(output_media_path, result_filename)

    os.rename(media_old_file, media_new_file)

def rename_file_output_version_to_hero(output_dir, outputmedia_dir):
    exr_dir = "{output_dir}/exr".format(output_dir=output_dir)
    # jpg_dir = "{output_dir}/jpg".format(output_dir=output_dir)

    list_exr_files = os.listdir(exr_dir)
    # list_jpg_files = os.listdir(jpg_dir)
    for exr_file in list_exr_files:
        new_exr_filename = rename_version_to_hero(exr_file)
        old_file = os.path.join(exr_dir, exr_file)
        new_file = os.path.join(exr_dir, new_exr_filename)
        os.rename(old_file, new_file)

    # for jpg_file in list_jpg_files:
    #     new_jpg_filename = rename_version_to_hero(jpg_file)
    #     old_file = os.path.join(jpg_dir, jpg_file)
    #     new_file = os.path.join(jpg_dir, new_jpg_filename)
    #     os.rename(old_file, new_file)

    list_mov_files = os.listdir(outputmedia_dir)
    for mov_file in list_mov_files:
        output_mov_file = os.path.split(mov_file)[-1]
        output_media_filename = rename_version_to_hero(output_mov_file)
        media_old_file = os.path.join(outputmedia_dir, output_mov_file)
        media_new_file = os.path.join(outputmedia_dir, output_media_filename)
        os.rename(media_old_file, media_new_file)

def rename_version_to_hero(file_name):
    file_name_split = file_name.split('.')
    # check rename hero_preview , hero_review
    if '_' in file_name_split[1]:
        file_name_split[1] = 'hero_{}'.format(file_name_split[1].split('_')[-1])
    else:
        file_name_split[1] = "hero"
    result = ".".join(file_name_split)
    return result


def create_publish_structure_comp(output_media_path):
    if os.path.exists(output_media_path):
        admin.rmtree(output_media_path)
    if not os.path.exists(output_media_path):
        os.makedirs(output_media_path)
