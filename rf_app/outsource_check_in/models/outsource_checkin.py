import os
import re
# import shutil
import comp_import
reload(comp_import)

from rf_utils.context import context_info
from rf_utils import project_info
from rf_utils.sg.sg_utils import (
    sg,
    get_all_projects,
    get_episodes_by_project
)
from rf_utils.pipeline import user_pref
from rf_utils import network_utils
from rf_utils import admin


class OutsourceCheckinModel:
    def __init__(self):
        self.sg = sg
        # self.steps = ["anim", "animatic", "finalcam", "fx", "layout", "light", "setdress", "sim", "comp"]
        self.steps = ['comp']
        self.projects = get_all_projects()
        self.user_pref = user_pref.ToolSetting('outsource_checkin')
        self.user_pref_data = self.user_pref.read()

    def get_projects(self):
        return self.projects

    def get_episodes(self):
        return self.episodes

    def get_steps(self):
        return self.steps

    def set_project_selected(self, project_selected):
        self.project_selected = project_selected

    def set_episodes(self):
        self.episodes = get_episodes_by_project(self.project_selected)

    def save_setting(
        self,
        project_selected,
        episode_selected,
        step_selected
    ):
        self.user_pref.write({
            "default_project": project_selected['name'],
            "default_episode": episode_selected['code'],
            "default_step": step_selected
        })

    def import_outsource_work(
        self,
        project_selected,
        episode_selected,
        step_selected,
        files_items
    ):
        project_info.ProjectInfo(project_selected['name'])
        for file_path in files_items:
            context = context_info.Context()
            # filename = os.path.basename(file_path)
            # seq, shot_version, ext = filename.split(".") # q0060.s0010_v001.exr
            # shot, version = shot_version.split("_")
            version = re.findall('[vV]{1}[0-9]{3}', file_path)
            if not version:
                version = 'hero'
            episode = re.findall('(ch|act|ep){1}([0-9]{2})', file_path)
            episode = ''.join(episode[0])
            seq = re.findall('[qQ]{1}[0-9]{4}', file_path)[0]
            shot = re.findall('[sS]{1}[0-9]{4}', file_path)[0]

            context.update(
                project=project_selected['name'],
                entityType='scene',
                workspace='publ',
                projectCode=project_selected['sg_project_code'],
                # entityGrp=episode_selected['code'],
                entityGrp = episode,
                entityParent=seq,
                entityCode=shot,
                step=step_selected,
                process='main',
                publishVersion="hero"
            )
            scene_context = context_info.ContextPathInfo(context=context)
            fps = scene_context.projectInfo.render.fps()
            output_media_path = scene_context.path.scheme(key='outputImgPath').abs_path()
            output_media_path = output_media_path.replace('hero',version) # change R:/..../hero to R:/...../{version}
            output_exr_path = output_media_path.replace(os.path.basename(output_media_path),'output') + '/exr'
            # must create R:/Hanuman/scene/publ/ch07/hnm_ch07_q0040_s0550/comp/main/v001/output/exr/hnm_ch07_q0040_s0550_comp_main.v001.1001.exr
            # then 1.copy all to publ 2.rename media in Media and imgSeq 
            publ_hero_path = scene_context.path.scheme(key="publishHeroPath").abs_path()
            if step_selected == "comp":
                backup_path = os.path.dirname(scene_context.path.scheme(key="publishHeroPath").rel_version().abs_path())
                hero_file_name, review_file_name, preview_file_name = comp_import.get_file_name(
                    scene_context,
                    version,
                    step_selected
                )
                comp_import.create_publish_structure_comp(output_media_path)
                comp_import.create_publish_structure_comp(output_exr_path)
                if not os.path.exists(publ_hero_path):
                    comp_import.create_publish_structure_comp(publ_hero_path)

                files = os.listdir(file_path)
                for f in files:
                    frame = f.split('.')[-2]
                    ext = f.split('.')[-1]
                    fileName = '{projCode}_{episode}_{seq}_{shot}_{step}_main.{version}.{frame}.{ext}'.format(projCode=context.projectCode, episode=context.entityGrp, seq=seq, shot=shot, step=context.step, version=version, frame=frame, ext=ext)
                    dst = output_exr_path + '/' + fileName
                    src = file_path + '/' + f
                    admin.copyfile(src, dst)


                fn = os.listdir(file_path)[0]
                frame = fn.split('.')[-2]
                fp = file_path + '/' + fn.replace(frame, '%04d')

                # exrPath = publ_hero_path + '/output/exr'
                # outputPublMedia = publ_hero_path + '/outputMedia'

                comp_import.convert_mov(
                    fp,
                    output_media_path,
                    hero_file_name,
                    review_file_name,
                    preview_file_name,
                    fps
                )
                preview_file_path = "{output_media_path}/{preview_file_name}".format(
                    output_media_path=output_media_path,
                    preview_file_name=preview_file_name
                )
                review_file_path = "{output_media_path}/{review_file_name}".format(
                    output_media_path=output_media_path,
                    review_file_name=review_file_name
                )
                # comp_import.backup_version(version, publ_hero_path, backup_path)
                comp_import.copy_to_publish(version, backup_path, publ_hero_path)
                publ_output_dir = publ_hero_path + '/output'
                publ_outputmedia_dir = publ_hero_path + '/outputMedia'
                comp_import.rename_file_output_version_to_hero(publ_output_dir, publ_outputmedia_dir)
                args = [
                    preview_file_path,
                    scene_context,
                    version,
                    step_selected,
                    review_file_path
                ]
                network_utils.make_retries(
                    comp_import.publish_version,
                    args,
                    retries_count=5
                )

                # comp_import.rename_file_version_to_hero(output_media_path, hero_file_name)
