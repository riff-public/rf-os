import os 
import sys 
import re
from rf_utils import ascii_utils 


class StandAlone(object):
    """docstring for StandAlone"""
    def __init__(self):
        super(StandAlone, self).__init__()
        self.cmd = None
        self.dcc = str()

    def read(self, path): 
        basename, ext = os.path.splitext(path)
        if ext == '.ma': 
            self.cmd = ascii_utils.MayaAscii(path)
            self.cmd.read()
            self.dcc = 'maya'
        
    def about(self): 
        print('Stand alone mode')

    def get_scene(self): 
        return ''

    def get_start_frame(self): 
        return 1

    def get_end_frame(self): 
        return 2

    def get_dcc(self): 
        return self.dcc

    def get_dcc_version(self): 
        if self.dcc == 'maya': 
            requires = self.cmd.requires()
            # requires -> [('maya', '"2020";'), ('"redshift4maya"', '"3.5.06";'), ('"stereoCamera"', '"10.0";')]
            for key, value in requires: 
                if key == self.dcc: 
                    versions = re.findall(r'\d+', value)
                    return versions[0] if versions else None

    def get_cameras(self): 
        omit = ['front', 'side', 'top']
        scene_cameras = self.cmd.ls(types=['camera'])
        valid_cams = list()
        for cam in scene_cameras: 
            # cam -> (15, 'camera1|cameraShape1')

            camera = cam[1].split('|')[0] 
            if not camera in omit: 
                valid_cams.append(camera)
        return valid_cams

    def set_default_values(self): 
        pass 
