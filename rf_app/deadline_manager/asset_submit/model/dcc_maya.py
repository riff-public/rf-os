import maya.cmds as mc 


def about(): 
    print('Maya')


def read(path): 
    return path


def get_scene(): 
    return mc.file(q=True, loc=True)


def get_start_frame(): 
    return int(mc.getAttr('defaultRenderGlobals.startFrame'))
    # return int(mc.playbackOptions(q=True, min=True))


def get_end_frame(): 
    return int(mc.getAttr('defaultRenderGlobals.endFrame'))
    # return int(mc.playbackOptions(q=True, max=True))


def get_cameras(): 
    omit = ['front', 'side', 'top']
    scene_cameras = mc.listCameras()
    cams = [a for a in scene_cameras if not a in omit]
    return cams


def get_dcc(): 
    return 'maya'


def get_dcc_version(): 
    return mc.about(v=True)


def set_default_values(): 
    default_values = {
        "redshiftOptions.applyColorManagementToHdrFiles": 0, 
        "redshiftOptions.applyColorManagementToMaya": 0
        }
    for attr, value in default_values.items(): 
        if mc.objExists(attr): 
            mc.setAttr(attr, value)

