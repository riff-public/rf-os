import sys 
import os 
import logging
import getpass
import subprocess
import shutil
import rf_config as config
from datetime import datetime
from rf_utils import ascii_utils 
from rf_utils import admin
from rf_utils.deadline import model_queue
reload(model_queue)
from rf_utils.context import context_info

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())


class Config: 
    support_files = ['.ma']


class Model(object):
    """docstring for Model"""
    def __init__(self, config, local_config):
        super(Model, self).__init__()
        if config.isMaya: 
            from . import dcc_maya as cmd 
            reload(cmd)
        else: 
            from . import non_dcc
            cmd = non_dcc.StandAlone()
        self.cmd = cmd 
        self.local_config = local_config
        self.scene_file = str()
        self.entity = None

    def read(self, path): 
        """ run this method before calling other method """ 
        if self.check_support(path): 
            self.scene_file = path
            self.cmd.read(path)
            self.entity = context_info.ContextPathInfo(path=self.scene_file)
            return True 
        else: 
            return False 

    def check_support(self, path): 
        if os.path.exists(path): 
            basename, ext = os.path.splitext(path)
            if ext in Config.support_files: 
                return True 
            else: 
                logger.error('Unsupported file format {}'.format(ext))
                return False
        else: 
            logger.error('File not exists {}'.format(path))
            return False

    def about(self): 
        return self.cmd.about() 

    def get_scene(self): 
        return self.cmd.get_scene()

    def get_start_frame(self): 
        return self.cmd.get_start_frame()

    def get_end_frame(self): 
        return self.cmd.get_end_frame() 

    def get_cameras(self): 
        return self.cmd.get_cameras()

    def get_user(self): 
        return getpass.getuser()

    def get_output_path(self): 
        output = '{}/{}/{}'.format(
            os.environ['RFPROJECT'], 
            self.entity.project, 
            self.local_config.Path.render_output)
        return output

    def get_output_render(self): 
        root = self.get_output_path()
        date = datetime.now().strftime('%Y-%m-%d')
        output = '{}/{}/{}/{}'.format(root, self.get_user(), date, self.entity.name)
        return output

    def get_dcc(self): 
        dcc = self.cmd.get_dcc()
        version = self.cmd.get_dcc_version()
        if dcc and version: 
            return (dcc, version)
        else: 
            logger.error('DCC version not found')

    def set_default(self): 
        self.cmd.set_default_values()

    def submit(self, start, end, camera, renderer): 
        dst = self.get_output_render()
        dcc = self.get_dcc()
        output_name = '{}_{}'.format(
            os.path.splitext(os.path.basename(self.scene_file))[0], 
            camera)
        if not os.path.exists(dst): 
            admin.makedirs(dst)

        if os.path.exists(dst): 
            result = model_queue.send_to_deadline(
                    scene_file=self.scene_file, 
                    start=start, 
                    end=end, 
                    camera=camera, 
                    dst=dst, 
                    audio_path='', 
                    shot_name='', 
                    # output_filename='{}.####'.format(output_name),
                    task_name="unknown", 
                    sg_upload="unknown", 
                    focalLength="unknown", 
                    renderer=renderer, 
                    dcc=dcc
                )
            logger.info('Submit complete \n{}, {}, {}, {}, {}'.format(
                self.scene_file, start, end, camera, dst))

            return result
        else: 
            logger.warning('Path not valid {}'.format(dst))

    def open_explorer(self, path): 
        path = path.replace('/', '\\')
        if os.path.exists(path): 
            subprocess.Popen(r'explorer /select,"%s"' % path)
        else: 
            logger.warning('Path not exists {}'.format(path))

    def run_monitor(self, version):
        if version == 8:
            src = self.local_config.DeadlineConfig.deadline8_config_file
            des = self.local_config.DeadlineConfig.deadline8_config_path
            app = self.local_config.RenderFarm.monitor8_path.replace('/', '\\')
        else:
            src = self.local_config.DeadlineConfig.deadline10_config_file
            des = self.local_config.DeadlineConfig.deadline10_config_path
            app = self.local_config.RenderFarm.monitor10_path.replace('/', '\\')

        shutil.copy(src, des)

        subprocess.Popen(app)
            