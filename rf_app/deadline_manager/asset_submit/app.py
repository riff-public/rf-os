# v.0.0.1 polytag switcher
_title = 'RF Asset Farm Submitter'
_version = 'v.0.0.1'
_des = 'Beta'
uiName = 'RFAssetSubmitterUi'

#Import python modules
import sys
import os
import getpass
import logging
from datetime import datetime
from collections import OrderedDict
# import config
import rf_config as config
from rf_utils import log_utils
from rf_utils.ui import stylesheet

module_dir = os.path.dirname(sys.modules[__name__].__file__)
appname = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]
user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)
uifile = '{}/{}'.format(module_dir, 'ui.ui')
global ui

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

from rf_utils import load 
from rf_utils.widget import browse_widget
from rf_utils.widget import dialog
from . import model
reload(model)
from . import local_config
reload(local_config)


class RFAssetSubmitter(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        #Setup Window
        super(RFAssetSubmitter, self).__init__(parent)

        # ui read
        ui = load.setup_ui(uifile)
        self.ui = ui
        self.model = model.Model(config, local_config)
        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)

        # title
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.resize(400, 200)
        self.override_ui()
        self.init_signals()
        self.init_functions()

    def closeEvent(self, event): 
        self.deleteLater()

    def override_ui(self): 
        self.ui.submit_pushButton.setEnabled(False)
        self.ui.show_pushButton.setEnabled(False)
        self.ui.scene_lineEdit.deleteLater()
        self.ui.scene_lineEdit = browse_widget.BrowseFile(title='Maya files', filter='Maya(*.ma)')
        self.ui.top_gridLayout.addWidget(self.ui.scene_lineEdit, 0, 1, 1, 4)

    def init_signals(self): 
        self.ui.submit_pushButton.clicked.connect(self.submit)
        self.ui.show_pushButton.clicked.connect(self.show_output)
        self.ui.scene_lineEdit.textChanged.connect(self.path_complete)
        self.ui.monitor_pushButton.clicked.connect(self.run_monitor)

    def init_functions(self): 
        self.ui.renderer_comboBox.addItems(local_config.Renderer.support_list)
        self.ui.scene_lineEdit.set_path(self.model.get_scene())
        self.model.set_default()

    def path_complete(self, path): 
        if path: 
            result = self.model.read(path)
            if result: 
                self.ui.submit_pushButton.setEnabled(True)
                self.ui.show_pushButton.setEnabled(True)
                self.ui.sf_lineEdit.setText(str(self.model.get_start_frame()))
                self.ui.ef_lineEdit.setText(str(self.model.get_end_frame()))
                self.ui.cam_comboBox.clear()
                self.ui.cam_comboBox.addItems(self.model.get_cameras())
            else: 
                self.clear_ui()
                self.ui.submit_pushButton.setEnabled(False)
                self.ui.show_pushButton.setEnabled(False)
                dialog.MessageBox.error('Error', 'File not exists or not supported')

    def clear_ui(self): 
        self.ui.sf_lineEdit.setText('')
        self.ui.ef_lineEdit.setText('')
        self.ui.cam_comboBox.clear()

    def submit(self): 
        start_frame = int(self.ui.sf_lineEdit.text())
        end_frame = int(self.ui.ef_lineEdit.text())
        camera = str(self.ui.cam_comboBox.currentText())
        renderer = str(self.ui.renderer_comboBox.currentText())
        result = self.model.submit(start_frame, end_frame, camera, renderer)
        if result: 
            dialog.MessageBox.success('Success', 'Submit Complete')

    def show_output(self): 
        path = self.model.get_output_render()
        self.model.open_explorer(path)

    def run_monitor(self): 
        self.ui.monitor_pushButton.setEnabled(False)
        if os.environ["active_project"] == "Hanuman":
            version = 8
        else:
            version = 10
        self.model.run_monitor(version)


def show(): 
    if config.isMaya: 
        from rftool.utils.ui import maya_win
        maya_win.deleteUI(uiName)
        app = RFAssetSubmitter(maya_win.getMayaWindow())
        app.show() 
        return app
    else: 
        # app = QtWidgets.QApplication.instance()
        main_app = QtWidgets.QApplication(sys.argv)
        app = RFAssetSubmitter()
        app.show()
        stylesheet.set_default(main_app)
        sys.exit(main_app.exec_())


if __name__ == '__main__':
    show()
