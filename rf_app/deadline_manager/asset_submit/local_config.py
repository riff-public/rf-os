class Path: 
	# P:/Bikey/_tmp/asset_farm
	render_output = '_tmp/asset_farm'


class Renderer: 
	support_list = ['redshift']


class RenderFarm: 
	monitor8_path = 'C:/Program Files/Thinkbox/Deadline8/bin/deadlinemonitor.exe'
	monitor10_path = 'C:/Program Files/Thinkbox/Deadline10/bin/deadlinemonitor.exe'

class DeadlineConfig:
	deadline8_config_file = "O:/Pipeline/core/rf_app/deadline_manager/asset_submit/model/dl_config/8/deadline.ini"
	deadline8_config_path = "C:/ProgramData/Thinkbox/Deadline8/deadline.ini"

	deadline10_config_file = "O:/Pipeline/core/rf_app/deadline_manager/asset_submit/model/dl_config/10/deadline.ini"
	deadline10_config_path = "C:/ProgramData/Thinkbox/Deadline10/deadline.ini"