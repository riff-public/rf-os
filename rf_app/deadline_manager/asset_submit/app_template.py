# v.0.0.1 polytag switcher
_title = 'RF Asset Farm Submitter'
_version = 'v.0.0.1'
_des = 'Beta'
uiName = 'RFAssetSubmitterUi'

#Import python modules
import sys
import os
import getpass
import logging
from datetime import datetime
from collections import OrderedDict
# import config
import rf_config as config
from rf_utils import log_utils
from rf_utils.ui import stylesheet

module_dir = os.path.dirname(sys.modules[__name__].__file__)
appname = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]
user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)
uifile = '{}/{}'.format(module_dir, 'ui.ui')
global ui

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

from rf_utils import load 
from rf_utils.widget import browse_widget


class RFAssetSubmitter(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        #Setup Window
        super(RFAssetSubmitter, self).__init__(parent)

        # ui read
        ui = load.setup_ui(uifile)
        self.ui = ui
        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)

        # title
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.resize(400, 200)

    def closeEvent(self, event): 
        self.deleteLater()

    def override_ui(self): 
        browse_widget.BrowseFile(title='Sequence Xml')


def show(): 
    if config.isMaya: 
        from rftool.utils.ui import maya_win
        maya_win.deleteUI(uiName)
        app = RFAssetSubmitter(maya_win.getMayaWindow())
        app.show() 
        return app
    else: 
        # app = QtWidgets.QApplication.instance()
        main_app = QtWidgets.QApplication(sys.argv)
        app = RFAssetSubmitter()
        app.show()
        stylesheet.set_default(main_app)
        sys.exit(main_app.exec_())


if __name__ == '__main__':
    show()
