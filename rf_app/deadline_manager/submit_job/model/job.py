import os
import subprocess
import sys
from collections import OrderedDict
import getpass

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())


core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)

os.environ["PYTHONPATH"] = "C:/Python27/Lib"
import rf_config as config
from rf_utils.context import context_info
from Qt import wrapInstance, QtGui, QtCore, QtWidgets



log_job_info_path = "{pipeline_path}/logs/submission_job/{user}".format(pipeline_path=os.environ.get('RFSCRIPT'), user=getpass.getuser())
if not os.path.exists(log_job_info_path):
    os.makedirs(log_job_info_path)

python_subbmission_job_info_path = "{log_job_info_path}/python_subbmission_job_info.txt".format(log_job_info_path=log_job_info_path)
deadline_cmd_path = "\\\\DeadlineServer\\DeadlineRepository10\\bin\\Windows\\64bit\\deadlinecommand.exe"

python_subbmission_job_info_params = "{log_job_info_path}/python_subbmission_job_info.job".format(log_job_info_path=log_job_info_path)
python_subbmission_job_props_params = "{log_job_info_path}/python_subbmission_job.job".format(log_job_info_path=log_job_info_path)
python_subbmission_job_params = "{log_job_info_path}/python_subbmission_job.job".format(log_job_info_path=log_job_info_path)


class Job:
    def __init__(self, parent):
        self.parent = parent

    def write_job_info(self, job_info, job_props):
        with open(python_subbmission_job_info_params, "w+") as f:
            f.seek(0)
            f.truncate()

        with open(python_subbmission_job_props_params, "w+") as f:
            f.seek(0)
            f.truncate()

        with open(python_subbmission_job_info_path, 'w') as out:
            out.write(python_subbmission_job_info_params + "\n")
            out.write(python_subbmission_job_props_params)

        for key, val in job_info.iteritems():
            with open(python_subbmission_job_info_params, 'a') as out:
                out.write('{}={}\n'.format(key, val))

        for key, val in job_props.iteritems():
            with open(python_subbmission_job_params, 'a') as out:
                out.write('{}={}\n'.format(key, val))

    def python_subbmission_convert(self, job_info, job_props):
        self.parent.ui.status_text_label.setText("Sending Job...")
        QtCore.QCoreApplication.processEvents()
        i = 0
        retries = 5
        self.write_job_info(job_info, job_props)
        while i < retries:
            print "sending job round {}:".format(i)
            print "submit job by call {deadline_cmd_path} {python_subbmission_job_info_path}".format(deadline_cmd_path=deadline_cmd_path, python_subbmission_job_info_path=python_subbmission_job_info_path)
            cmd = "call \"{deadline_cmd_path}\" \"{python_subbmission_job_info_path}\"".format(deadline_cmd_path=deadline_cmd_path, python_subbmission_job_info_path=python_subbmission_job_info_path)
            proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            while proc.poll() is None:
                print proc.stdout.readline() #give output from your execution/your own message
            commandResult = proc.wait() #catch return code 
            print "commandResult", commandResult
            if commandResult:
                i = i + 1
                # retry send job to deadline if cannot send job
                continue
            else:
                self.parent.ui.status_text_label.setText("Send Job Success.")
                break

    def get_job_params(self, python_file, scene_files):
        # context = context_info.ContextPathInfo()
        data_job_info = OrderedDict()
        data_job_info["Plugin"] = "Python"
        data_job_info["Name"] = "Test hello Python"
        data_job_info["Comment"] = ""
        data_job_info["EventOptIns"] = ""
        data_job_info["Frames"] = "1"
        data_job_info["Blacklist"] = ""
        data_job_info["MachineName"] = ""
        data_job_info["OverrideTaskExtraInfoNames"] = False
        data_job_info["Pool"] = "rnd"
        data_job_info["Region"] = ""
        data_job_info["SecondaryPool"] = "none"
        data_job_info["UserName"] = "teeruk.i"

        data_job_props = OrderedDict()
        data_job_props["ScriptFile"] = python_file
        data_job_props["Arguments"] = " ".join(scene_files)
        data_job_props["SingleFramesOnly"] = "False"
        data_job_props["Version"] = "2.7"

        return data_job_info, data_job_props

    def python_subbmission(self, python_file, scene_files):
        if python_file == "" or len(scene_files) == 0 :
            self.parent.ui.status_text_label.setText("Please import python or scene file.")
            return

        data_job_info, data_job_props = self.get_job_params(python_file, scene_files)
        self.python_subbmission_convert(data_job_info, data_job_props)
