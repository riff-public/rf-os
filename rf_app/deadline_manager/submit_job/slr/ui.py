from PySide2 import QtWidgets, QtCore, QtGui
from shiboken2 import wrapInstance
from maya import OpenMayaUI as omui
import pymel.core as pm
import maya.cmds as cmds

import os
import sys
import json

core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)

from rf_utils.context import context_info
from rf_utils import user_info

from rf_utils.ui.flatUi import main_ui
reload(main_ui)

import submitJobToDeadline
reload(submitJobToDeadline)

import util as util
reload(util)

def getMayaWindow():
    win = omui.MQtUtil_mainWindow()
    ptr = wrapInstance(long(win), QtWidgets.QMainWindow)
    return ptr

class Submit_ui(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(Submit_ui, self).__init__(parent)
        
        # global settings #
        try:
            cmds.setAttr("redshiftOptions.copyToTextureCache", 0)
        except Exception as e:
            cmds.warning(e)

        self.render_server_list = ["deadlineserver"]
        self.active_server = self.render_server_list[0]

        self.start, self.end, self.renderCam, self.output_path, self.prefix, self.w, self.h = util.getShotInfo()
        self.start = int(float(self.start))
        self.end = int(float(self.end))

        self.active_project = os.environ.get("ACTIVE_PROJECT")
        self.all_render_layers, self.render_layers_data = util.genRenderLayerData()
        self.renderer = "redshift"
        self.scene = context_info.ContextPathInfo()
        self.step = self.scene.step
        
        self.__top_layout = QtWidgets.QGridLayout(self)
        self.__build_ui()
        self.__connect_signals()
        
    def __build_ui(self):
        server_grp = QtWidgets.QGroupBox("Server", self)
        server_grp_lay = QtWidgets.QGridLayout(server_grp)
        
        project_label = QtWidgets.QLabel("Project :", server_grp)
        projectName_label = QtWidgets.QLabel(self.active_project, server_grp)
        server_label = QtWidgets.QLabel("Render Server", server_grp)
        self.server_cbb = QtWidgets.QComboBox(server_grp)
        self.server_cbb.addItems(self.render_server_list)
        pool_label = QtWidgets.QLabel("Pool", server_grp)
        self.pool_cbb = QtWidgets.QComboBox(server_grp)
        self.pool_cbb.addItems(["rs_bikey", "arnold_bikey", "rs_ca", "rs_hanuman", "rs_sunflower", "asset_redshift"])
        grp_label = QtWidgets.QLabel("Group", server_grp)
        self.grp_cbb = QtWidgets.QComboBox(server_grp)
        self.grp_cbb.addItems(["None", "render", "slr"])

        if self.step != "light":
            self.pool_cbb.setCurrentText("asset_redshift")
        
        server_grp_lay.addWidget(project_label, 0, 0, 1, 1)
        server_grp_lay.addWidget(projectName_label, 0, 1, 1, 1)
        server_grp_lay.addWidget(server_label, 1, 0, 1, 1)
        server_grp_lay.addWidget(self.server_cbb, 1, 1, 1, 1)
        server_grp_lay.addWidget(pool_label, 2, 0, 1, 1)
        server_grp_lay.addWidget(self.pool_cbb, 2, 1, 1, 1)
        server_grp_lay.addWidget(grp_label, 3, 0, 1, 1)
        server_grp_lay.addWidget(self.grp_cbb, 3, 1, 1, 1)
        
        details_grpBox = QtWidgets.QGroupBox("Details", self)
        details_grpBox_lay = QtWidgets.QGridLayout(details_grpBox)
        
        prefix_label = QtWidgets.QLabel("Prefix", details_grpBox)
        self.prefix_label = QtWidgets.QLabel(self.prefix, details_grpBox)
        res_label = QtWidgets.QLabel("Width, Height", details_grpBox)
        self.res_label = QtWidgets.QLabel("{} x {}".format(self.w, self.h), details_grpBox)
        cam_label = QtWidgets.QLabel("Camera", details_grpBox)
        self.cam_cbb = QtWidgets.QComboBox(details_grpBox)
        self.cam_cbb.addItems(self.renderCam)

        start_label = QtWidgets.QLabel("Start frame", details_grpBox)
        self.startFrame_input = QtWidgets.QSpinBox(details_grpBox)
        self.startFrame_input.setRange(0, 99999)
        self.startFrame_input.setValue(self.start)

        end_label = QtWidgets.QLabel("End frame", details_grpBox)
        self.endFrame_input = QtWidgets.QSpinBox(details_grpBox)
        self.endFrame_input.setRange(0, 99999)
        self.endFrame_input.setValue(self.end)

        output_path_label = QtWidgets.QLabel("Output Path", details_grpBox)
        wid = QtWidgets.QWidget(details_grpBox)
        wid_lay = QtWidgets.QHBoxLayout(wid)
        wid_lay.setContentsMargins(0, 0, 0, 0)
        self.output_path_input = QtWidgets.QLineEdit(wid)
        self.output_path_input.setText(self.output_path)
        self.openFileDialog_btn = QtWidgets.QPushButton("...", wid)
        wid_lay.addWidget(self.output_path_input)
        wid_lay.addWidget(self.openFileDialog_btn)
        
        details_grpBox_lay.addWidget(prefix_label, 0, 0, 1, 1)
        details_grpBox_lay.addWidget(self.prefix_label, 0, 1, 1, 1)
        details_grpBox_lay.addWidget(res_label, 1, 0, 1, 1)
        details_grpBox_lay.addWidget(self.res_label, 1, 1, 1, 1)
        details_grpBox_lay.addWidget(cam_label, 2, 0, 1, 1)
        details_grpBox_lay.addWidget(self.cam_cbb, 2, 1, 1, 1)
        details_grpBox_lay.addWidget(start_label, 3, 0, 1, 1)
        details_grpBox_lay.addWidget(self.startFrame_input, 3, 1, 1, 1)
        details_grpBox_lay.addWidget(end_label, 4, 0, 1, 1)
        details_grpBox_lay.addWidget(self.endFrame_input, 4, 1, 1, 1)
        details_grpBox_lay.addWidget(output_path_label, 5, 0, 1, 1)
        details_grpBox_lay.addWidget(wid, 5, 1, 1, 1)
        
        layer_grpBox = QtWidgets.QGroupBox("Render Layers", self)
        layer_grpBox_lay = QtWidgets.QHBoxLayout(layer_grpBox)
        
        # Active #
        l_wid = QtWidgets.QWidget(layer_grpBox)
        l_wid_lay = QtWidgets.QVBoxLayout(l_wid)
        active_layer_label = QtWidgets.QLabel("Active Layers", l_wid)
        self.active_layer_lwd = QtWidgets.QListWidget(l_wid)
        self.active_layer_lwd.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.active_layer_lwd.addItems(self.all_render_layers)
        l_wid_lay.addWidget(active_layer_label)
        l_wid_lay.addWidget(self.active_layer_lwd)
        
        # mini btn #
        c_wid = QtWidgets.QWidget(layer_grpBox)
        c_wid_lay = QtWidgets.QVBoxLayout(c_wid)
        self.add_btn = QtWidgets.QPushButton(">>", c_wid)
        self.remove_btn = QtWidgets.QPushButton("<<", c_wid)
        c_wid_lay.addStretch()
        c_wid_lay.addWidget(self.add_btn)
        c_wid_lay.addWidget(self.remove_btn)
        c_wid_lay.addStretch()
        
        # Send to farm #
        r_wid = QtWidgets.QWidget(layer_grpBox)
        r_wid_lay = QtWidgets.QVBoxLayout(r_wid)
        send_label = QtWidgets.QLabel("Send to farm", r_wid)
        self.send_lwd = QtWidgets.QListWidget(r_wid)
        self.send_lwd.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        r_wid_lay.addWidget(send_label)
        r_wid_lay.addWidget(self.send_lwd)
        
        layer_grpBox_lay.addWidget(l_wid)
        layer_grpBox_lay.addWidget(c_wid)
        layer_grpBox_lay.addWidget(r_wid)
        
        
        # append #
        self.testFrame_grpBox = QtWidgets.QGroupBox("Still Frame", self)
        testFrame_grpBox_lay = QtWidgets.QVBoxLayout(self.testFrame_grpBox)
        self.testFrame_grpBox.setCheckable(True)
        self.testFrame_grpBox.setChecked(False)
        
        self.first_chk = QtWidgets.QCheckBox("First", self.testFrame_grpBox)
        self.mid_chk = QtWidgets.QCheckBox("Mid", self.testFrame_grpBox)
        self.last_chk = QtWidgets.QCheckBox("Last", self.testFrame_grpBox)
        self.appendFrame_chk = QtWidgets.QCheckBox("Append Frame", self.testFrame_grpBox)
        self.test_frame_chkList = [self.first_chk, self.mid_chk, self.last_chk]
        
        testFrame_grpBox_lay.addWidget(self.first_chk)
        testFrame_grpBox_lay.addWidget(self.mid_chk)
        testFrame_grpBox_lay.addWidget(self.last_chk)
        testFrame_grpBox_lay.addWidget(QtWidgets.QWidget())
        testFrame_grpBox_lay.addWidget(self.appendFrame_chk)
        
        # props #
        props_grpBox = QtWidgets.QGroupBox("Properties", self)
        props_grpBox_lay = QtWidgets.QVBoxLayout(props_grpBox)
        
        task_timeout_label = QtWidgets.QLabel("Task Time out (min). Maximum render time is 120 min.", props_grpBox)
        self.task_timeout_cbb = QtWidgets.QComboBox(props_grpBox)
        self.task_timeout_cbb.addItems(["15", "30", "60", "120"])
        comment_label = QtWidgets.QLabel("Comment", props_grpBox)
        self.comment_input = QtWidgets.QLineEdit(props_grpBox)
        
        props_grpBox_lay.addWidget(task_timeout_label)
        props_grpBox_lay.addWidget(self.task_timeout_cbb)
        props_grpBox_lay.addWidget(comment_label)
        props_grpBox_lay.addWidget(self.comment_input)
        
        b_wid = QtWidgets.QWidget(self)
        b_wid_lay = QtWidgets.QHBoxLayout(b_wid)
        b_wid_lay.setContentsMargins(0, 0, 0, 0)
        self.submitAsSuspended_chk = QtWidgets.QCheckBox("Submit Job As Suspended", self)
        self.cliqNoti_chk = QtWidgets.QCheckBox("Cliq Notification", self)
        b_wid_lay.addWidget(self.submitAsSuspended_chk)
        b_wid_lay.addWidget(self.cliqNoti_chk)

        self.submit_btn = QtWidgets.QPushButton("Submit To Deadline", self)
        self.submit_btn.setObjectName("blueBtn")
        
        
        self.__top_layout.addWidget(server_grp, 0, 0, 1, 2)
        self.__top_layout.addWidget(details_grpBox, 1, 0, 1, 2)
        self.__top_layout.addWidget(layer_grpBox, 2, 0, 1, 2)
        self.__top_layout.addWidget(self.testFrame_grpBox, 3, 0, 1, 1)
        self.__top_layout.addWidget(props_grpBox, 3, 1, 1, 1)
        self.__top_layout.addWidget(b_wid, 4, 1, 1, 1)
        self.__top_layout.addWidget(self.submit_btn, 5, 1, 1, 1)
        
        
    def __connect_signals(self):
        self.openFileDialog_btn.clicked.connect(self.openFileDialog)
        self.add_btn.clicked.connect(self.addToFarm)
        self.remove_btn.clicked.connect(self.removeFromFarm)
        self.submit_btn.clicked.connect(self.submit)

    def openFileDialog(self):
        flags = QtWidgets.QFileDialog.DontResolveSymlinks | QtWidgets.QFileDialog.ShowDirsOnly
        d = directory = QtWidgets.QFileDialog.getExistingDirectory(self,
                                                                    "Open Directory",
                                                                    self.output_path,
                                                                    flags)

        if d and directory:
            self.output_path_input.setText(directory)
        
    def addToFarm(self):
        sendToFarmList = [str(self.send_lwd.item(i).text()) for i in range(self.send_lwd.count())]
        
        selectedLayers = self.active_layer_lwd.selectedItems()
        
        if not selectedLayers:
            return
            
        for layer in selectedLayers:
            layerName = layer.text()
            if layerName not in sendToFarmList:
                self.send_lwd.addItem(layerName)
                
    def removeFromFarm(self):
        selectedLayers = self.send_lwd.selectedItems()
        
        if not selectedLayers:
            return
            
        for layer in selectedLayers:
            self.send_lwd.takeItem(self.send_lwd.row(layer))

    def submit(self):
        if self.send_lwd.count() == 0:
            return cmds.warning("Please add layer to submit.")

        user = user_info.User()
        userEntity = user.sg_user()
        email = userEntity["sg_ad_account"] + "@riff-studio.com"

        initialStatus = self.submitAsSuspended_chk.isChecked()
        notification = self.cliqNoti_chk.isChecked()

        submitResult = {}

        sendToFarmList = [str(self.send_lwd.item(i).text()) for i in range(self.send_lwd.count())]

        test_frame = True if self.testFrame_grpBox.isChecked() else False
        if test_frame:
            selected_frame = [i.text() for i in self.test_frame_chkList if i.isChecked()]
        
            if not selected_frame:
                return cmds.warning("Please select at least one frame")


        for data in zip(sendToFarmList, sendToFarmList):
            # every data[0] must be true #
            if self.render_layers_data[data[0]]["frame"]["startFrame"]:
                self.startFrame = self.render_layers_data[data[0]]["frame"]["startFrame"]
            else:
                self.startFrame = self.startFrame_input.value()

            if self.render_layers_data[data[0]]["frame"]["endFrame"]:
                self.endFrame = self.render_layers_data[data[0]]["frame"]["endFrame"]
            else:
                self.endFrame = self.endFrame_input.value()

            
            version = self.render_layers_data[data[0]]["version"]
            renderer = self.render_layers_data[data[0]]["currentRenderer"]

            
            # convert frame from float to int #
            self.startFrame = int(float(self.startFrame))
            self.endFrame = int(float(self.endFrame))

            app = "maya"
            frames = "{}-{}".format(self.startFrame, self.endFrame)
            camera = self.cam_cbb.currentText().replace("Shape", "")
            output_prefix = self.prefix


            if self.output_path_input.text() == "":
                output_path = self.output_path
            else:
                output_path = self.output_path_input.text()

            prefix_absName = self.prefix.replace("<RenderLayer>", data[0]).replace("<Version>", version).replace("<Camera>", camera)

            output_dir = output_path + "/" + "/".join(prefix_absName.split("/")[0:-1])

            comment = self.comment_input.text()
            renderserver = self.server_cbb.currentText()
            priority = 50
            timeout = str(int(self.task_timeout_cbb.currentText()) * 60) # convert min to seconds
            pool = self.pool_cbb.currentText()
            grp = self.grp_cbb.currentText()

            if renderer == "arnold":
                pool = "arnold_bikey"

            #secondaryPool = "rs_bikey" if pool == "rs_bikey_ldv" else ""
            secondaryPool = ""

            # check if test frame is checked #
            if test_frame and selected_frame:
                #self.midFrame = str(int(((float(self.endFrame) - float(self.startFrame)) / 2) + float(self.startFrame)))
                testFrameList = {"first": False,
                                "mid": False,
                                "last": False}

                for index, layer in enumerate(data):

                    if index == 0:
                        frames = ""
                        if self.first_chk.isChecked():
                            frames = str(self.startFrame)
                            testFrameList["first"] = int(self.startFrame)
                        if self.mid_chk.isChecked():
                            self.midFrame = str(((self.endFrame - self.startFrame) / 2) + self.startFrame)
                            frames += ",{}".format(self.midFrame)
                            testFrameList["mid"] = int(self.midFrame)
                        if self.last_chk.isChecked():
                            frames += ",{}".format(self.endFrame)
                            testFrameList["last"] = int(self.endFrame)

                        if frames[0] == ",":
                            frames = frames[1:]

                        dependency = ""
                        priority = 55
                    else:
                        if self.appendFrame_chk.isChecked():
                            frames = self.framePattern(self.startFrame, self.endFrame, testFrameList)
                            dependency = jobId
                            priority = 50
                        else:
                            break

                    print "-"*50
                    print "Submit Info : Layer : {}".format(layer)
                    print "\t app ", app
                    print "\t renderer ", renderer
                    print "\t frames", frames
                    print "\t camera ", camera
                    print "\t output_dir ", output_dir
                    print "\t output_path ", output_path
                    print "\t output_prefix ", output_prefix
                    print "\t comment ", comment
                    print "\t priority ", priority
                    print "\t timeout ", timeout
                    print "\t renderserver ", renderserver
                    print "\t pool ", pool
                    print "\t group ", grp
                    print "\t seconddary_pool", secondaryPool
                    print "-"*50


                    jobId, result = submitJobToDeadline.send_to_deadline_cmd(app=app, 
                                                                    frames=frames, camera=camera, 
                                                                    output_dir=output_dir, output_path=output_path,
                                                                    render_layer=layer, output_prefix=output_prefix, 
                                                                    priority=priority, timeout=timeout, dependency=dependency,
                                                                    renderserver=renderserver, pool=pool, group=grp, secondaryPool=secondaryPool,
                                                                    comment=comment,
                                                                    renderer=renderer, initialStatus=initialStatus, email=email,
                                                                    notification=notification, useStillFrame=True)
                    
            else:
                layer = data[0]
                print "-"*50
                print "Submit Info : Layer : {}".format(layer)
                print "\t app ", app
                print "\t renderer ", renderer
                print "\t frames", frames
                print "\t camera ", camera
                print "\t output_dir ", output_dir
                print "\t output_path ", output_path
                print "\t output_prefix ", output_prefix
                print "\t comment ", comment
                print "\t priority ", priority
                print "\t timeout ", timeout
                print "\t renderserver ", renderserver
                print "\t pool ", pool
                print "\t group ", grp
                print "\t seconddary_pool", secondaryPool
                print "-"*50

                jobId, result = submitJobToDeadline.send_to_deadline_cmd(app=app, 
                                                                frames=frames, camera=camera, 
                                                                output_dir=output_dir, output_path=output_path,
                                                                render_layer=layer, output_prefix=output_prefix, 
                                                                priority=priority, timeout=timeout,
                                                                renderserver=renderserver, pool=pool, group=grp, secondaryPool=secondaryPool,
                                                                comment=comment,
                                                                renderer=renderer, initialStatus=initialStatus, email=email,
                                                                notification=notification)



            submitResult[layer] = result

        layers = {}
        txt = ""
        layerCount = 0
        for k, v in submitResult.iteritems():
            if v == True:
                r = "Successful"
                layers[k] = 0
                layerCount += 1
            else:
                r = "Failed"
            txt += "{}:\n - {}\n".format(k,r)
            txt += "-----------------------------------------------\n"

        msg = QtWidgets.QMessageBox(self)
        msg.setText(txt)
        msg.setStandardButtons(msg.Ok)
        msg.setWindowTitle("Reports")
        msg.exec_()

    def framePattern(self, startFrame, endFrame, testFrameList):
        startFrame = testFrameList["first"] +1 if testFrameList["first"] else startFrame
        frameBeforeMid =  testFrameList["mid"] -1 if testFrameList["mid"] else 0
        frameAfterMid =  testFrameList["mid"] +1 if testFrameList["mid"] else 0
        endFrame = testFrameList["last"] -1 if testFrameList["last"] else endFrame


        if frameBeforeMid and frameAfterMid:
            frame = "{}-{},{}-{}".format(startFrame, frameBeforeMid, frameAfterMid, endFrame)
        else:
            frame = "{}-{}".format(startFrame, endFrame)

        return frame


def runUI(ui_name="Submit_ui", size=(470, 800), title="Submit Job To Deadline"):
    if pm.window(ui_name, exists=True):
        pm.deleteUI(ui_name)
        
    win = main_ui.MainWindow(ui_name=ui_name, size=size, title=title, widget=Submit_ui(), parent=getMayaWindow())
    win.show()
    