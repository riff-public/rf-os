import os
import subprocess
import sys
from collections import OrderedDict
import logging
import maya.cmds as cmds

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)

import rf_config as config
from rf_utils.context import context_info
from rf_maya.environment import envparser
reload(envparser)

# global vars
#deadline_cmd_path = config.Software.deadline
deadline_cmd_path = "C:/Program Files/Thinkbox/Deadline10/bin/deadlinecommand.exe"
dl_version = "v10"

script_root = 'O:/Pipeline'

def createDeadlineJobInfo(app, frames, camera , output_dir="", output_path="", output_fileName="", render_layer="", 
                            output_prefix="<RenderLayer>/<Version>/<RenderLayer>", priority=50,
                            comment="", pool="None", secondaryPool="", dept="SLR", group="None", dependency=None,
                            task_perframe=1, renderserver="deadlineserver", renderer="redshift", UseLegacyRenderLayers="0",
                            timeout=900, initialStatus="", email="", notification=False, useStillFrame=False,
                            overrideRenderEnv=False):

    scene_file = cmds.file(q=True, sn=True)
    try:
        scene = context_info.ContextPathInfo(path=scene_file)
        data_path = scene.path.scheme(key="dataPath").abs_path().replace("publ", "work")
        ep = scene.episode.upper()
        filename = scene.filename
        user = scene.local_user
        step = scene.step
        shot_code = scene.name
        project = os.environ.get("ACTIVE_PROJECT")

        if step == "light":
            batch_name = "{}_{}".format(project, ep)
        elif step == "lookdev":
            batch_name = "{}_LDV".format(project)

    except KeyError as e:
        data_path = "/".join(scene_file.split("/")[0:-1]) + "/_data"
        batch_name = scene_file.split("/")[-1].split(".")[0].split("_")[0]
        filename = scene_file.split("/")[-1]
        user = "Not Found"
        step = "Not Found"
        shot_code = "Not Found"

    deadline_folder_path = "{}/deadline/{}".format(data_path, app)

    if not os.path.isdir(deadline_folder_path):
        os.makedirs(deadline_folder_path)
        
    logFile(deadline_folder_path)

    sg_trigger_file = "{}/slr_sg_trigger.json".format(deadline_folder_path)
    job_info_path = "{}/slr_job_info.job".format(deadline_folder_path)
    job_props_path = "{}/slr_job_props.job".format(deadline_folder_path)

    try:
        sg_task = os.environ["TASK"]
    except KeyError as e:
        sg_task = "None"

    plugin_info = {"maya": "MayaBatch",
                    "nuke": False}

    if UseLegacyRenderLayers == "0":
        name_subfix = " - rs_"
    else:
        name_subfix = ""

    name = filename + name_subfix + render_layer

    if useStillFrame and not dependency:
        name += "_STILL_FRAME"

    if dependency:
        name += "_APPEND_FRAME"

    project = os.environ.get("ACTIVE_PROJECT")


    # ----------- job info ----------- #
    job_info = OrderedDict()
    job_info["BatchName"] = batch_name
    job_info["Plugin"] = plugin_info[app]
    job_info["Name"] = name
    job_info["Comment"] = comment
    job_info["EventOptIns"] = "StatusManager"
    job_info["Pool"] = pool
    job_info["SecondaryPool"] = secondaryPool
    job_info["Priority"] = priority
    job_info["OnJobComplete"] = "Nothing"
    job_info["TaskTimeoutSeconds"] = timeout
    job_info["EnableAutoTimeout"] = "0"
    job_info["OnTaskTimeout"] = "Requeue"
    job_info["ConcurrentTasks"] = "1"
    job_info["Department"] = dept
    job_info["Group"] = group
    job_info["LimitGroups"] = ""
    job_info["Whitelist"] = ""
    job_info["OutputDirectory0"] = output_dir
    job_info["Frames"] = frames
    job_info["ChunkSize"] = task_perframe
    job_info["FailureDetectionTaskErrors"] = 10
    job_info["OverrideTaskFailureDetection"] = True
    job_info["MachineLimit"] = 5
    job_info["ExtraInfoKeyValue0"] = "project={project}".format(project=project)
    job_info["ExtraInfoKeyValue1"] = "user={user}".format(user=user)
    job_info["ExtraInfoKeyValue2"] = "step={step}".format(step=step)
    job_info["ExtraInfoKeyValue3"] = "file_name={filename}".format(filename=filename)
    job_info["ExtraInfoKeyValue4"] = "scene_file={scene_file}".format(scene_file=scene_file)
    job_info["ExtraInfoKeyValue5"] = "shot_code={shot_code}".format(shot_code=shot_code)
    job_info["ExtraInfoKeyValue6"] = "sg_task={sg_task}".format(sg_task=sg_task)
    job_info["ExtraInfoKeyValue7"] = "render_layer={render_layer}".format(render_layer=render_layer)
    job_info["ExtraInfoKeyValue8"] = "email={email}".format(email=email)
    job_info["ExtraInfoKeyValue9"] = "output_dir={output_dir}".format(output_dir=output_dir)
    job_info["ExtraInfoKeyValue10"] = "notification={notification}".format(notification=notification)


    if initialStatus and not dependency:
        job_info["InitialStatus"] = "Suspended"

    if dependency:
        job_info["JobDependency0"] = dependency

    config_path = "O:/Pipeline/core/rf_env/default/deadline/{}/{}.ini".format(dl_version, renderserver)
    custom_env = os.environ.copy()
    custom_env["PYTHONPATH"] = "C:/Python27/Lib;C:/Python27/Lib/DLLs"
    # set config path so deadline launches using the prefered config
    custom_env['DEADLINE_CONFIG_FILE'] = config_path
            
    # config env #
    if overrideRenderEnv:
        job_info = override_render_environments(job_info, overrideRenderEnv)
    else:
        job_info = setup_render_environments(job_info, project, "Light", "2020")

    # ----------- job props ----------- #
    job_props = OrderedDict()
    job_props["Animation"] = "1"
    job_props["Renderer"] = renderer
    job_props["UsingRenderLayers"] = "1"
    job_props["RenderLayer"] = render_layer
    job_props["RenderHalfFrames"] = "0"
    job_props["FrameNumberOffset"] = "0"
    job_props["LocalRendering"] = "0"
    job_props["StrictErrorChecking"] = "0"
    job_props["Version"] = "2020"
    job_props["UseLegacyRenderLayers"] = UseLegacyRenderLayers
    job_props["Build"] = "64bit"
    job_props["StartupScript"] = ""
    job_props["SkipExistingFrames"] = "0"
    job_props["OutputFilePath"] = output_path
    job_props["OutputPrefix"] = output_prefix
    job_props["Camera"] = camera
    job_props["CountRenderableCameras"] = "1"
    job_props["SceneFile"] = scene_file
    job_props["IgnoreError211"] = "0"
    job_props["UseLocalAssetCaching"] = "0"

    # ----------- write job to file # ----------- #
    with open(job_info_path, "w") as f:
        for k, v in job_info.iteritems():
            t = "{}={}\n".format(k, v)
            f.write(t)

    with open(job_props_path, "w") as f:
        for k, v in job_props.iteritems():
            t = "{}={}\n".format(k, v)
            f.write(t)

    return job_info_path, job_props_path, custom_env


def setup_render_environments(job_info, project, dept, version):
    # from maya.cmds import about
    # drive envs
    drive_envs = OrderedDict([('RFPROJECT', 'P:'), 
                            ('RFPUBL', 'P:'), 
                            ('RFPROD', 'P:'), 
                            ('RFVERSION', 'R:'),
                            ('RFSCRIPT', script_root)])

    # project env 
    #project = context.project
    department = dept
    version = version
    '''with envparser.OverrideEnvironment(RFSCRIPT=script_root) as override_env:
        envs = envparser.setupEnv(project=project, department=department, version=version, 
                                mergeWithDefault=True, setEnvironment=False, writeToFile=False)'''

    localEnv = envparser.setupEnv(project=project, department=department, version=version, 
                                mergeWithDefault=True, setEnvironment=False, writeToFile=False)

    envs = {}

    currentScript = os.environ.get('RFSCRIPT')

    for k, v in localEnv.iteritems():
        newV = [i.replace(currentScript, script_root) for i in v]
        envs[k] = newV

    envs = OrderedDict([(i[0], ';'.join(i[1])) for i in envs.items()])
    envs.update(drive_envs)
    for i, key in enumerate(envs):
        value = envs[key]
        # yeti lic server #
        if key == "peregrinel_LICENSE":
            value = "5053@172.16.100.33"
        job_info['EnvironmentKeyValue{}'.format(i)] = '{}={}'.format(key, value)

        #print '\t --: EnvironmentKeyValue{} = {}={}'.format(i, key, value)

    return job_info


def override_render_environments(job_info, env_file):
    with open(env_file, "r") as f:
        env_data = [line.strip() for line in f]
    
    for e in env_data:
        if "ExtraInfoKeyValue" in e or "EnvironmentKeyValue" in e:
            key, val = e.split("=", 1)
            job_info[key] = val
            
    return job_info

# submit #
def send_to_deadline_cmd(*args, **kwargs):

    job_info_path, job_props_path, custom_env = createDeadlineJobInfo(*args, **kwargs)

    sended_job = False
    jobID = ""

    cmd = '{deadline_cmd_path} "{job_info_path}" "{job_props_path}"'.format(deadline_cmd_path=deadline_cmd_path,
                                                                            job_info_path=job_info_path,
                                                                            job_props_path=job_props_path)

    process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, env=custom_env)
    lines = iter(process.stdout.readline, b"")
    commandResult = ""
    for line in lines:
        commandResult += line

    if "Result=Success" in commandResult:
        sended_job = True
        jobID = commandResult.split("JobID=")[-1].split("\r")[0]

    print commandResult
    return jobID, sended_job

def logFile(deadline_folder_path):
    from datetime import datetime
    import getpass

    scriptName = os.path.basename(__file__)
    user = getpass.getuser()
    now = datetime.now()
    dt = now.strftime("%d/%m/%Y %H:%M:%S")
    d = "Created from {} by {} at {}".format(scriptName, user, dt)
    
    path = deadline_folder_path.split("/maya")[0] + "/log.log"
    
    
    with open(path, "w") as f:
        f.writelines(d)