import maya.cmds as cmds
import re
import maya.app.renderSetup.model.renderSetup as renderSetup

from collections import OrderedDict

# global vars #
rs = renderSetup.instance()
allRenderLayers = rs.getRenderLayers()

def getShotInfo():
    start = str(cmds.getAttr("defaultRenderGlobals.startFrame"))
    end = str(cmds.getAttr("defaultRenderGlobals.endFrame"))

    defaultCam = ["frontShape", "perspShape", "sideShape", "topShape", "backShape"]
    #renderCam = [i for i in cmds.ls(type="camera") if cmds.getAttr("{}.renderable".format(i)) == 1]
    renderCam = [i for i in cmds.ls(type="camera")]
    if renderCam:
        renderCam = list(set(renderCam).difference(defaultCam))
   
    prefix = cmds.getAttr("defaultRenderGlobals.imageFilePrefix")
    if not prefix:
        prefix = ""
    v = cmds.workspace(q=True, fileRule=True)
    v_index = v.index("images") +1
    output_path = v[v_index]
    
    w = cmds.getAttr("defaultResolution.width")
    h = cmds.getAttr("defaultResolution.height")
    
    return start, end, renderCam, output_path, prefix, w, h
    


def genRenderLayerData(defaultRenderer="redshift"):
    
    data = OrderedDict()
    rawVersion = getRawVersion()
                    
    isRenderable = [i.name() for i in allRenderLayers if i.isRenderable()]          
    for layer in allRenderLayers:
        data[layer.name()] = {"frame": {"startFrame": None, "endFrame": None},
                "version": rawVersion,
                "currentRenderer": defaultRenderer,
                "labelColor": layer.getLabelColor()}
        
        renderSettingsCollection = [i for i in layer.getContainers() if i.typeName() == "renderSettingsCollection"]
        
        if renderSettingsCollection:
            renderSettingsCollection = renderSettingsCollection[0]
            use = ["startFrame", "endFrame", "renderVersion", "currentRenderer"]
        
            for c in renderSettingsCollection.getChildren():
                node = c.name()
                k = re.sub(r'[0-9]', "", node)
                
                if k not in use:
                    continue
                
                value = cmds.getAttr("{}.attrValue".format(node))
                
                if "startFrame" in c.name() or "endFrame" in c.name():
                    data[layer.name()]["frame"][k] = value
                
                if "renderVersion" in c.name():
                    data[layer.name()]["version"] = value
    
                if "currentRenderer" in c.name():
                    data[layer.name()]["currentRenderer"] = value
                   
                
    return isRenderable, data



def getRawVersion():
    currLayer = [i for i in allRenderLayers if i.isVisible()]   
    if not currLayer:
        # assume it's a materLayer
        version = cmds.getAttr("defaultRenderGlobals.renderVersion")
    else:
        # check if render settings is override #
        currLayer = currLayer[0]
        renderSettingsCollection = [i for i in currLayer.getContainers() if i.typeName() == "renderSettingsCollection"]
        if not renderSettingsCollection:
            # assume there's no override on the render version #
            # get raw value #
            version = cmds.getAttr("defaultRenderGlobals.renderVersion")
        else:
            renderSettingsCollection = renderSettingsCollection[0]
            # if self enabled #
            # turn it off > get raw value > turn back on #
            if renderSettingsCollection.isSelfEnabled():
                renderSettingsCollection.setSelfEnabled(False)
                version = cmds.getAttr("defaultRenderGlobals.renderVersion")
                renderSettingsCollection.setSelfEnabled(True)
            else:
                version = cmds.getAttr("defaultRenderGlobals.renderVersion")
                
    return version
                