import sys
import os

from model.job import Job

core_path = '%s/core' % os.environ.get('RFSCRIPT_DEV')
if not core_path in sys.path:
    sys.path.append(core_path)

# import config
import rf_config as config

from rf_utils.ui import load, stylesheet
from rf_utils.widget import browse_widget, display_widget

# from models.shot import Shot
# from models.animatic import Animatic
os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import QtWidgets, QtCore, QtGui

UI_PATH = "{core_path}/rf_app/deadline_manager/submit_job/view/ui.ui".format(core_path=core_path)


class SubmitJobCtrl(QtWidgets.QMainWindow):
    def __init__(self, args=None):
        super(SubmitJobCtrl, self).__init__()
        self.ui = load.setup_ui(UI_PATH, self)
        self.init_ui_app()
        self.init_signal()
        self.job_model = Job(self)
        self.scene_files = []
        self.ui.show()

    def init_signal(self):
        self.list_files_display.multipleDropped.connect(self.input_files)
        self.ui.submiit_job_btn.clicked.connect(self.submit_job)

    def init_ui_app(self):
        self.fileFilter = 'Python(*.py)'
        self.browser_file = browse_widget.BrowseFile(title='Python File')
        self.browser_file.set_label('Python File : ')
        self.browser_file.filter = self.fileFilter
        self.ui.submit_job_layout.addWidget(self.browser_file)

        self.list_files_display = display_widget.DropUrlList()
        self.ui.submit_job_layout.addWidget(self.list_files_display)

        item = QtWidgets.QListWidgetItem(self.list_files_display)
        item.setText("Drag Drop scene files.")

    def input_files(self, result):
        self.list_files_display.clear()
        self.scene_files = []
        for file_name in result:
            item = QtWidgets.QListWidgetItem(self.list_files_display)
            item.setText(file_name)
            self.scene_files.append(file_name)

    def submit_job(self):
        python_file = self.browser_file.current_text()
        self.job_model.python_subbmission(python_file, self.scene_files)


if __name__ == '__main__':
    app = QtWidgets.QApplication.instance()
    if not app:
        app = QtWidgets.QApplication(sys.argv)
        stylesheet.set_default(app)
    myApp = SubmitJobCtrl(sys.argv)
    # stylesheet.set_default(app)
    sys.exit(app.exec_())
