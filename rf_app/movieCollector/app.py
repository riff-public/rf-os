#!/usr/bin/env python
# -*- coding: utf-8 -*-

# v.beta   - under development
# v.1.0.0  - initial release
# v.1.1.0  - add remove version name in copied files
#          - add create date directory for copied files
# v.1.1.1  - Fix error when found unknown status icon

_title = 'Movie Collector'
_version = 'v.1.1.1'
_des = ''
uiName = 'movieCollector'

# python modules
import sys
import os
import time
import subprocess
from datetime import datetime
from functools import partial
from collections import OrderedDict

core = '%s/core' % os.environ.get('RFSCRIPT')
sys.path.append(core)

# import config
import rf_config as config
from rf_utils.ui import stylesheet

# QT
os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

# custom modules
from rf_utils.sg import sg_process
from rf_utils import thread_pool
from rf_utils.pipeline import movie_collect
from rf_utils.widget.status_widget import Icon
from rf_utils.widget import project_widget
from rf_utils.widget import collapse_widget
from rf_utils.widget import sg_widget

moduleDir = os.path.dirname(sys.modules[__name__].__file__).replace('\\', '/')
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

# ---------- TODOs ----------
# //- pop up to open file when collect finishes
# //- double-click open movie
# //- right-click open directory
# - drag movie to viewer playlist
# * Callback


class CollectThread(QtCore.QThread):
    def __init__(self, parent=None):
        super(CollectThread, self).__init__(parent=parent)
        
    def run(self):
        pass

class MovieCollector(QtWidgets.QMainWindow):
    progress_message = QtCore.Signal(tuple)

    def __init__(self, parent=None):
        # setup Window
        super(MovieCollector, self).__init__(parent)
        # app vars
        self.movie_data = None
        self.tasks = ('layout', 'anim', 'finalcam', 'techanim', 'light_full', 'comp_full')
        self.default_task = 'anim'
        self.thread = None
        self.threadpool = QtCore.QThreadPool()

        # ui vars
        self.w = 525
        self.h = 700
        self.grey_col = (128, 128, 128)
        self.white_col = (255, 255, 255)
        self.yellow_col = (255, 255, 32)
        self.green_col = (32, 255, 32)
        self.red_col = (255, 32, 32)
        self.regular_font = QtGui.QFont()
        self.regular_font.setItalic(False)
        self.italic_font = QtGui.QFont()
        self.italic_font.setItalic(True)
        
        self.app_icon = '{}/icons/app_icon.png'.format(moduleDir)
        self.logo_icon = '{}/icons/riff_logo.png'.format(moduleDir)
        self.search_icon = '{}/icons/search_icon.png'.format(moduleDir)
        self.collect_icon = '{}/icons/collect_icon.png'.format(moduleDir)
        self.movie_file_icon = '{}/icons/movie_file_icon.png'.format(moduleDir)
        self.movie_online_icon = '{}/icons/movie_online_icon.png'.format(moduleDir)
        self.movie_nofile_icon = '{}/icons/movie_nofile_icon.png'.format(moduleDir)
        self.blank_status_icon = '{}/icons/blank_status_icon.png'.format(moduleDir)
        self.unknown_status_icon = '{}/icons/unknown_status_icon.png'.format(moduleDir)
        self.play_icon = '{}/icons/play_icon.png'.format(moduleDir)
        self.folder_icon = '{}/icons/folder_icon.png'.format(moduleDir)
        self.sequence_icon = '{}/icons/sequence_icon.png'.format(moduleDir)

        # init functions
        self.setupUi()
        self.init_signals()
        self.set_default()
        
    def setupUi(self):
        self.setObjectName(uiName)
        self.setWindowTitle('{} {} {}'.format(_title, _version, _des))
        self.setWindowIcon(QtGui.QIcon(self.app_icon))
        self.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))

        self.centralwidget = QtWidgets.QWidget(self)
        self.setCentralWidget(self.centralwidget)
        self.resize(self.w, self.h)

        # main layout
        self.main_layout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.main_layout.setSpacing(9)

        # header layout
        self.header_layout = QtWidgets.QHBoxLayout()
        self.main_layout.addLayout(self.header_layout)

        # logo
        self.logo = QtWidgets.QLabel()
        self.logo.setPixmap(QtGui.QPixmap(self.logo_icon).scaled(32, 32, QtCore.Qt.KeepAspectRatio))
        self.header_layout.addWidget(self.logo)

        # input layout
        self.input_layout = QtWidgets.QHBoxLayout()
        self.input_layout.setSpacing(6)
        self.header_layout.addLayout(self.input_layout)

        self.filter_layout = QtWidgets.QHBoxLayout()
        self.header_layout.addLayout(self.filter_layout)

        # project 
        self.project_widget = project_widget.SGProjectComboBox(sg=sg_process.sg, layout=QtWidgets.QHBoxLayout())
        self.project_widget.projectComboBox.setMinimumWidth(125)
        # episode
        self.ep_label = QtWidgets.QLabel('E.P.')
        self.ep_comboBox = sg_widget.SGComboBox('code')
        self.ep_comboBox.comboBox.setMinimumWidth(85)
        # task
        self.task_label = QtWidgets.QLabel('Task')
        self.task_comboBox = QtWidgets.QComboBox()
        self.task_comboBox.setMinimumWidth(85)
        self.input_layout.addWidget(self.project_widget)
        self.input_layout.addWidget(self.ep_label)
        self.input_layout.addWidget(self.ep_comboBox)
        self.input_layout.addWidget(self.task_label)
        self.input_layout.addWidget(self.task_comboBox)

        self.input_line = QtWidgets.QFrame()
        self.input_line.setFrameShape(QtWidgets.QFrame.VLine)
        self.input_line.setFrameShadow(QtWidgets.QFrame.Raised)
        self.input_layout.addWidget(self.input_line)

        # fetch button
        self.fetch_button = QtWidgets.QPushButton(' Search ')
        self.fetch_button.setIcon(QtGui.QIcon(self.search_icon))
        self.fetch_button.setMinimumWidth(80)
        self.input_layout.addWidget(self.fetch_button)

        spacerItem = QtWidgets.QSpacerItem(0, 0, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.header_layout.addItem(spacerItem)

        # view layout
        self.view_layout = QtWidgets.QVBoxLayout()
        self.main_layout.addLayout(self.view_layout)

        # table widget
        self.tree_widget = QtWidgets.QTreeWidget()
        self.tree_widget.setMinimumHeight(200)
        self.tree_widget.setHeaderHidden(True)
        self.tree_widget.setColumnCount(2)
        self.tree_widget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.tree_widget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.tree_widget.setColumnWidth(0, 125)
        self.view_layout.addWidget(self.tree_widget)

        # bottom layout
        self.bottom_layout = QtWidgets.QVBoxLayout()
        self.bottom_layout.setSpacing(3)
        self.main_layout.addLayout(self.bottom_layout)

        self.browse_frame = QtWidgets.QFrame()
        self.browse_frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.browse_frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.bottom_layout.addWidget(self.browse_frame)

        # browse layout
        self.browse_layout = QtWidgets.QHBoxLayout(self.browse_frame)
        self.browse_layout.setSpacing(5)
        self.browse_layout.setContentsMargins(5, 5, 5, 5)

        self.dest_label = QtWidgets.QLabel('Directory: ')
        self.browse_layout.addWidget(self.dest_label)

        self.dest_lineEdit = QtWidgets.QLineEdit()
        self.dest_lineEdit.setPlaceholderText('< Please specify output directory >')
        self.browse_layout.addWidget(self.dest_lineEdit)

        self.browse_button = QtWidgets.QPushButton('...')
        self.browse_button.setMinimumSize(QtCore.QSize(25, 25))
        self.browse_button.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.browse_layout.addWidget(self.browse_button)

        # collect button
        self.collect_button = QtWidgets.QPushButton('Collect')
        self.collect_button.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.collect_button.setMinimumSize(QtCore.QSize(75, 25))
        self.collect_button.setIcon(QtGui.QIcon(self.collect_icon))
        self.browse_layout.addWidget(self.collect_button)

        # setting layout
        self.setting_groupbox = collapse_widget.CollapsibleBox(title='Settings')
        self.bottom_layout.addWidget(self.setting_groupbox)
        self.setting_layout = QtWidgets.QGridLayout(self.setting_groupbox)

        # mode radio buttons
        self.mode_layout = QtWidgets.QHBoxLayout()

        spacerItem = QtWidgets.QSpacerItem(0, 0, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.mode_layout.addItem(spacerItem)

        self.mode_label = QtWidgets.QLabel('Mode: ')
        self.mode_layout.addWidget(self.mode_label)

        self.copy_radiobutton = QtWidgets.QRadioButton('Copy')
        self.mode_layout.addWidget(self.copy_radiobutton)

        spacerItem = QtWidgets.QSpacerItem(90, 0, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        self.mode_layout.addItem(spacerItem)

        self.combine_radio_button = QtWidgets.QRadioButton('Merge')
        self.mode_layout.addWidget(self.combine_radio_button)

        spacerItem = QtWidgets.QSpacerItem(0, 0, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.mode_layout.addItem(spacerItem)

        self.setting_layout.addLayout(self.mode_layout, 0, 0, 2, 7)

        # line between mode and checkboxes
        self.setting_line = QtWidgets.QFrame()
        self.setting_line.setFrameShape(QtWidgets.QFrame.HLine)
        self.setting_line.setFrameShadow(QtWidgets.QFrame.Raised)
        self.setting_layout.addWidget(self.setting_line, 2, 0, 1, 7, QtCore.Qt.AlignVCenter)

        # general settings
        # download checkbox
        self.download_checkbox = QtWidgets.QCheckBox('Download from SG')
        self.setting_layout.addWidget(self.download_checkbox, 3, 1, 1, 1, QtCore.Qt.AlignLeft)

        # show only available
        self.show_available_checkbox = QtWidgets.QCheckBox('Show only available')
        self.setting_layout.addWidget(self.show_available_checkbox, 4, 1, 1, 1, QtCore.Qt.AlignLeft)

        # ------ copy settings
        # remove version name from file
        self.remove_versionname_checkbox = QtWidgets.QCheckBox('Remove version name')
        self.setting_layout.addWidget(self.remove_versionname_checkbox, 3, 3, 1, 1, QtCore.Qt.AlignLeft)

        # create dirctory by date
        self.create_datedir_checkbox = QtWidgets.QCheckBox('Create folder by date')
        self.setting_layout.addWidget(self.create_datedir_checkbox, 4, 3, 1, 1, QtCore.Qt.AlignLeft)

        # ----- merge settings
        # use cut order
        self.use_cutorder_checkbox = QtWidgets.QCheckBox('Use SG cut order')
        self.setting_layout.addWidget(self.use_cutorder_checkbox, 3, 5, 1, 1, QtCore.Qt.AlignLeft)


        # set content of CollapsibleBox 
        self.setting_groupbox.setContentLayout(self.setting_layout)

        # status line
        self.statusBar = QtWidgets.QStatusBar()
        status_font = QtGui.QFont()
        status_font.setItalic(True)
        self.statusBar.setFont(status_font)
        self.main_layout.addWidget(self.statusBar)

        self.fetch_button.setToolTip('Search for latest movies from selected project, episode and task.')
        self.dest_lineEdit.setToolTip('Directory path for collected movies.')
        self.browse_button.setToolTip('Browse for directory path.')
        self.collect_button.setToolTip('Start collecting movies.')
        self.tree_widget.setToolTip('Selected sequences/shots will be collected. \nDouble-click on a shot to open file.')
        self.copy_radiobutton.setToolTip('Copy mode. \nWill collect movies by copying them to your desired location.')
        self.combine_radio_button.setToolTip('Merge mode. \nWill collect and merge movies into 1 long movie.')
        self.download_checkbox.setToolTip('If checked, will download movie from SG \nif the movie is not available locally.')
        self.show_available_checkbox.setToolTip('If checked, will hide empty shots without a movie.')
        self.remove_versionname_checkbox.setToolTip('Copy mode only. \nCopied file names will not contain version name.')
        self.create_datedir_checkbox.setToolTip('Copy mode only. \nCopy files to a new folder named by date and time.')
        self.use_cutorder_checkbox.setToolTip('Merge mode only. \nMovie will be ordered by cut order from SG, \nelse will be ordered by shot name.')
        
    def set_default(self):
        # mode
        self.copy_radiobutton.setChecked(True)

        # checkboxes
        self.download_checkbox.setChecked(True)
        self.use_cutorder_checkbox.setChecked(False)
        self.show_available_checkbox.setChecked(True)
        self.remove_versionname_checkbox.setChecked(True)
        self.create_datedir_checkbox.setChecked(True)

        # settings
        # self.setting_groupbox.toggle_button.setChecked(False)
        self.show_status('Please select project, episode, task, and click "Search".', level='normal')
        self.project_widget.emit_project_changed()

    def init_signals(self):
        # project widget
        self.project_widget.projectChanged.connect(self.project_changed)
        # buttons
        self.fetch_button.clicked.connect(self.thread_fetch_data)
        self.browse_button.clicked.connect(self.browse_directory)
        self.collect_button.clicked.connect(self.thread_collect)
        # modes 
        self.copy_radiobutton.toggled.connect(self.mode_toggled)
        self.combine_radio_button.toggled.connect(self.mode_toggled)
        # checkboxes
        self.show_available_checkbox.toggled.connect(self.apply_filter)
        # tree widget
        self.tree_widget.itemDoubleClicked.connect(self.movie_double_clicked)
        self.tree_widget.customContextMenuRequested.connect(self.movie_right_clicked)

        self.progress_message.connect(self.show_progress)

    def mode_toggled(self):
        if self.copy_radiobutton.isChecked():
            self.remove_versionname_checkbox.setEnabled(True)
            self.create_datedir_checkbox.setEnabled(True)
            self.use_cutorder_checkbox.setEnabled(False)
        else:
            self.remove_versionname_checkbox.setEnabled(False)
            self.create_datedir_checkbox.setEnabled(False)
            self.use_cutorder_checkbox.setEnabled(True)

    def movie_right_clicked(self, pos):
        item = self.tree_widget.itemAt(pos)
        # right clicked on an item
        if not item or not item.parent(): # return if it's not a shot item
            return
        # get shot data in item
        data = item.data(QtCore.Qt.UserRole, 0)
        if not data:
            return
        # get path
        path = data.get('path')
        if not path or not os.path.exists(path):
            return
        path_dir = os.path.dirname(path)

        # create menu
        rightClickMenu = QtWidgets.QMenu(self)

        # play movie
        play_action = QtWidgets.QAction('Play movie',  self)
        play_action.setIcon(QtGui.QIcon(self.play_icon))
        play_action.triggered.connect(partial(self.open_file, path))
        rightClickMenu.addAction(play_action)

        # open dir action
        opendir_action = QtWidgets.QAction('Open file location',  self)
        opendir_action.setIcon(QtGui.QIcon(self.folder_icon))
        opendir_action.triggered.connect(partial(self.open_dir, path_dir))
        rightClickMenu.addAction(opendir_action)

        # show the menu
        rightClickMenu.exec_(self.tree_widget.mapToGlobal(pos))

    def browse_directory(self):
        dirpath = QtWidgets.QFileDialog.getExistingDirectory(parent=self, 
                                                            caption='Output Directory',
                                                            dir=os.path.expanduser('~').replace('\\' ,'/'))
        if dirpath:
            self.dest_lineEdit.setText(dirpath.replace('\\', '/'))

    def show_progress(self, callback):
        msg, level = callback
        self.show_status(msg, level=level)

    def show_status(self, message, level='normal'):
        level_dict = {'normal': self.white_col, 'working': self.yellow_col, 'success': self.green_col, 'error': self.red_col}
        self.statusBar.showMessage(message)
        self.statusBar.setStyleSheet('color: rgb{}'.format(level_dict.get(level, self.white_col)))

    def thread_fetch_data(self):
        self.tree_widget.clear()
        self.movie_data = None

        project = self.project_widget.current_project()
        task = self.task_comboBox.currentText()
        episode = self.ep_comboBox.current_text()

        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
        self.show_status('Searching for movies: {} {} {}...'.format(project, episode, task), level='working')
        self.setEnabled(False)

        worker = thread_pool.Worker(self.fetch_data, project, task, episode)
        worker.signals.result.connect(self.list_movies)
        self.threadpool.start(worker)

    def fetch_data(self, project, task, episode):
        # movie_data = {seq_code: [{'shot':shot, 'path':path, 'url':url}]}
        self.movie_data = movie_collect.get_movie_data(project=project, 
                                                task=task, 
                                                episode=episode)
        return project, episode, task

    def list_movies(self, results, *args, **kwargs):
        self.tree_widget.clear()
        project, episode, task = results
        if not self.movie_data:
            QtWidgets.QApplication.restoreOverrideCursor()
            self.show_status('No movie found for: {} {} {}'.format(project, episode, task), level='error')
            self.setEnabled(True)
            return

        global_shot_count = 0
        for seq_code, shot_data in self.movie_data.iteritems():
            seq_item = QtWidgets.QTreeWidgetItem(self.tree_widget)
            seq_item.setText(0, seq_code)
            seq_item.setIcon(0, QtGui.QIcon(self.sequence_icon))

            num_shots = len(shot_data)
            local_count = 0
            online_count = 0
            for sd in shot_data:
                # print '...', sd
                updated = sd.get('updated')
                mov_path = sd.get('path')

                shot_item = QtWidgets.QTreeWidgetItem(seq_item)
                shot_item.setData(QtCore.Qt.UserRole, 0, sd)

                # set shot name
                shot_item.setText(0, sd['shot']['sg_shortcode'])
                # set sg status icon
                status = sd['status']
                
                if status:
                    if Icon.statusMap.get(status):
                        iconPath = '{0}/{1}'.format(Icon.path, Icon.statusMap[status].get('icon'))
                        statusName = Icon.statusMap[status]['display']
                    else:
                        iconPath = self.unknown_status_icon
                        statusName = status
                else:
                    iconPath = self.blank_status_icon
                    statusName = '< None >'
                icon = QtGui.QIcon(iconPath)
                shot_item.setIcon(0, icon)

                shot_item.setToolTip(0, 'Status: {}'.format(statusName))

                # set movie path
                if mov_path:
                    # set item data
                    shot_item.setData(QtCore.Qt.UserRole, 0, sd)
                    # set tooltip as full mov path
                    update_str = datetime.strftime(updated, '%y/%m/%d - %H:%M:%S') if updated else 'N/A'
                    shot_item.setToolTip(1, 'Updated: {}\nDirectory: {}\nFilename: {}'.format(update_str, 
                                                                                os.path.dirname(mov_path) , 
                                                                                os.path.basename(mov_path)))
                    # set icons
                    if os.path.exists(mov_path):
                        shot_item.setIcon(1, QtGui.QIcon(self.movie_file_icon))
                        shot_item.setText(1, os.path.basename(mov_path))
                        shot_item.setFont(1, self.regular_font)
                        shot_item.setForeground(1, QtGui.QBrush(QtGui.QColor(self.white_col[0], self.white_col[1], self.white_col[2])))
                        local_count += 1
                    else:
                        shot_item.setIcon(1, QtGui.QIcon(self.movie_online_icon))
                        shot_item.setText(1, os.path.basename(mov_path))
                        shot_item.setFont(1, self.italic_font)
                        shot_item.setForeground(1, QtGui.QBrush(QtGui.QColor(self.grey_col[0], self.grey_col[1], self.grey_col[2])))
                        online_count += 1
                else:
                    shot_item.setIcon(1, QtGui.QIcon(self.movie_nofile_icon))
                    shot_item.setText(1, '< No movie found >')
                    shot_item.setFont(1, self.italic_font)
                    shot_item.setForeground(1, QtGui.QBrush(QtGui.QColor(self.grey_col[0], self.grey_col[1], self.grey_col[2])))

            # set movie count
            seq_item.setText(1, 'Local: {}, Online: {}, Total: {}'.format(local_count, online_count, num_shots))
            seq_item.setFont(1, self.regular_font)
            seq_item.setForeground(1, QtGui.QBrush(QtGui.QColor(self.grey_col[0], self.grey_col[1], self.grey_col[2])))
            global_shot_count += num_shots

        self.apply_filter()

        QtWidgets.QApplication.restoreOverrideCursor()
        self.show_status('Found {} sequences, {} shots'.format(len(self.movie_data), global_shot_count), level='success')
        self.setEnabled(True)

    def apply_filter(self, *args, **kwargs):
        available_only = self.show_available_checkbox.isChecked()
        rootItem = self.tree_widget.invisibleRootItem()
        for i in range(rootItem.childCount()):
            seq_item = rootItem.child(i)
            for c in range(seq_item.childCount()):
                shot_item = seq_item.child(c)
                data = shot_item.data(QtCore.Qt.UserRole, 0)

                if available_only and not data['path']:
                    shot_item.setHidden(True)
                    shot_item.setSelected(False)
                else:
                    shot_item.setHidden(False)

    def thread_collect(self):
        sels = self.tree_widget.selectedItems()
        if not sels:
            self.show_status('Please select a Shot item!', level='error')
            return
        directory = self.dest_lineEdit.text()
        if not directory:
            self.show_status('Please specify an output directory!', level='error')
            return
        if not os.path.exists(directory):
            self.show_status('Directory doesn\'t exists: {}'.format(directory), level='error')
            return

        project = self.project_widget.current_project()
        task = self.task_comboBox.currentText()
        episode = self.ep_comboBox.current_text()
        download = self.download_checkbox.isChecked()
        use_cutorder = self.use_cutorder_checkbox.isChecked()
        combine = self.combine_radio_button.isChecked()
        remove_version = self.remove_versionname_checkbox.isChecked()
        create_date_dir = self.create_datedir_checkbox.isChecked()

        # filter out seq item
        seq_shots = OrderedDict()
        found = False
        for item in sels:
            shots = []
            if not item.parent():  # it's sequence item
                if item.childCount() < 1:
                    continue
                seq_code = item.text(0)
                if seq_code not in seq_shots:
                    seq_shots[seq_code] = []
                for i in range(item.childCount()):
                    shot_item = item.child(i)
                    shot_data = shot_item.data(QtCore.Qt.UserRole, 0)
                    shots.append(shot_data)
            else:  # it's shot item
                seq_item = item.parent()
                if not [i for i in sels if i is seq_item]:  # if shot item is selected individually
                    seq_code = seq_item.text(0)
                    if seq_code not in seq_shots:
                        seq_shots[seq_code] = []
                    shot_data = item.data(QtCore.Qt.UserRole, 0)
                    shots.append(shot_data)

            if shots:
                for shot_data in shots:
                    if download:
                        if shot_data['url']:
                            seq_shots[seq_code].append(shot_data)
                            found = True  
                    else:
                        if os.path.exists(shot_data['path']):
                             seq_shots[seq_code].append(shot_data)
                             found = True  
              
        if not found:
            self.show_status('No shot with valid movie found!', level='error')
            return

        # flatten seq_shots to [(shot, path, url), ...]
        movie_data = []
        for seq_code, shot_data in seq_shots.iteritems():
            for shot in shot_data:
                if shot['status'] != 'omt':
                    data = (shot['shot'], shot['path'], shot['url'])
                    movie_data.append(data)

        mode = 'Copy' if not combine else 'Merge'
        # confirm user
        qmsgBox = QtWidgets.QMessageBox(self)
        text = '{} movies from {} Sequence(s), {} Shot(s)?\nSee the list, click "Show Details"'. format(mode, len(seq_shots), len(movie_data))
        detailedText = ''
        for seq, shots in seq_shots.iteritems():
            # print seq, [s['shot']['code'] for s in shots]
            detailedText += '{}:\n  - {}'.format(seq, '\n  - '.join([s['shot']['code'] for s in shots]))
            detailedText += '\n'
        qmsgBox.setText(text)
        qmsgBox.setWindowTitle('Confirm')
        qmsgBox.setDetailedText(detailedText)
        qmsgBox.setIcon(QtWidgets.QMessageBox.Question)
        qmsgBox.addButton('  Yes  ', QtWidgets.QMessageBox.AcceptRole)
        qmsgBox.addButton('  No  ', QtWidgets.QMessageBox.RejectRole)
        answer = qmsgBox.exec_()
        if answer == 1: 
            return

        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
        self.show_status('Collecting movies...', level='working')
        self.setEnabled(False)

        worker = thread_pool.Worker(self.collect, project, task, episode, directory, download, combine, use_cutorder, 
                            remove_version, create_date_dir, movie_data)
        worker.signals.result.connect(self.collect_finished)
        self.threadpool.start(worker)

    def collect(self, project, task, episode, directory, download, combine, use_cutorder, remove_version, create_date_dir, movie_data):
        result = movie_collect.main(project=project, 
                                    task=task, 
                                    episode=episode, 
                                    directory=directory, 
                                    download=download, 
                                    combine=combine, 
                                    use_cutorder=use_cutorder,
                                    remove_version=remove_version,
                                    create_date_dir=create_date_dir,
                                    movie_data=movie_data,
                                    callback=self.emit_progress_message)
        return result

    def emit_progress_message(self, msg, level, *args, **kwargs):
        self.progress_message.emit((msg, level))

    def collect_finished(self, result):
        QtWidgets.QApplication.restoreOverrideCursor()
        self.setEnabled(True)
        qmsgBox = QtWidgets.QMessageBox(self)
        if result:
            result_path = os.path.dirname(result[0]) if isinstance(result, (list, tuple)) else result
            open_func = self.open_file if os.path.isfile(result_path) else self.open_dir
            self.show_status('Finished.', level='success')
            qmsgBox.setWindowTitle('Finished')
            qmsgBox.setText('Please check your result.\n{}'.format(result_path))
            qmsgBox.setIcon(QtWidgets.QMessageBox.Information)
            qmsgBox.addButton('  Done  ', QtWidgets.QMessageBox.AcceptRole)
            see_result_button = qmsgBox.addButton(' See results ', QtWidgets.QMessageBox.YesRole)
            see_result_button.clicked.connect(partial(open_func, result_path))
        else:
            # self.show_status(result, level='error')
            qmsgBox.setWindowTitle('Error')
            qmsgBox.setText('Something went wrong, please contact admin.\n{}'.format(result))
            qmsgBox.setIcon(QtWidgets.QMessageBox.Critical)
            qmsgBox.addButton('  Done  ', QtWidgets.QMessageBox.AcceptRole)
        qmsgBox.exec_()

    def movie_double_clicked(self, item):
        if not item.parent():  # return if it's seq item
            return
        data = item.data(QtCore.Qt.UserRole, 0)
        path = data['path']
        if path and os.path.exists(path):
            self.open_file(path=path)

    def open_file(self, path):
        # os.startfile(path)
        subprocess.Popen([path], shell=True, creationflags=subprocess.SW_HIDE)

    def open_dir(self, path):
        subprocess.Popen(r'explorer {}'.format(path.replace('/', '\\')))

    def project_changed(self, project):
        self.ep_comboBox.clear()
        self.task_comboBox.clear()
        self.tree_widget.clear()

        # get ep
        episodes = sg_process.get_episodes(project=project['name'])
        self.ep_comboBox.add_items(episodes)
        for task in self.tasks:
            self.task_comboBox.addItem(task)
        self.task_comboBox.setCurrentIndex(self.tasks.index(self.default_task))


def show():
    app = QtWidgets.QApplication(sys.argv)
    myApp = MovieCollector()
    myApp.show()
    stylesheet.set_default(app)
    sys.exit(app.exec_())

if __name__ == '__main__':
    show()
