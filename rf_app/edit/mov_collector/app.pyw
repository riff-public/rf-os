_title = 'Riff Mov Collector'
_version = 'v.0.0.1'
_des = 'beta'
uiName = 'RFMovCollector'

#Import python modules
import sys
import os
import getpass
import json
import logging

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)

# import config
import rf_config as config
from rf_utils import log_utils
# sg

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

import ui
reload(ui)
from rf_utils.context import context_info
from rf_utils import file_utils
from rf_utils import icon 
from rf_utils.ui import stylesheet


class RFMovCollector(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        #SetUp Window
        super(RFMovCollector, self).__init__(parent)
        #ui read
        self.ui = ui.MovCollectorUI(parent)
        self.resize(800, 700)
        self.show()
        self.setObjectName(uiName)
        # self.modules = modules
        self.episode_path = None
        self.step_path = None
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.setCentralWidget(self.ui)
        self.init_state()
        self.fetch_episode_folder()


    def init_state(self):
        self.init_path()
        self.ui.project_comboBox.highlighted.connect(self.fetch_episode_folder)
        # self.ui.pushButton_test.clicked.connect(self.test)
        self.ui.episode_list_widget.itemSelectionChanged.connect(self.fetch_sequence)
        self.ui.sequence_list_widget.itemSelectionChanged.connect(self.get_shot)
        self.ui.step_list_widget.itemSelectionChanged.connect(self.get_step_path)
        self.ui.local_lineEdit.editingFinished.connect(self.chk_local_lineEdit)
        self.ui.mov_radioButton.clicked.connect(self.get_shot)
        self.ui.exr_radioButton.clicked.connect(self.get_shot)
        self.ui.pushButton_test.clicked.connect(self.sync_select)

    def init_path(self):
        context = context_info.Context()
        context.update(project=self.ui.project_comboBox.currentText())
        context.update(entityType ='scene')
        context.update(process ='main')
        self.scene_ctx = context_info.ContextPathInfo(context=context)
        self.publish_path = self.scene_ctx.path.publish().abs_path()

    def fetch_episode_folder(self):
        self.ui.episode_list_widget.clear()
        for folder in  os.listdir(self.publish_path):
            item = QtWidgets.QListWidgetItem(self.ui.episode_list_widget)
            item.setText(folder)

    def get_step_path(self):
        self.ui.step_list_widget.setStyleSheet( """QListWidget{background:  none;}""")
        step = self.ui.step_list_widget.currentItem().text()
        if self.episode_path:
            self.scene_ctx.context.update(step = step)
        else:
            self.ui.episode_list_widget.setStyleSheet( """QListWidget{background: red;}""")

            ############//////////
    def chk_local_lineEdit(self):
        if self.ui.local_lineEdit.text() != '':
            self.ui.local_lineEdit.setStyleSheet( """QListWidget{background:  none;}""")

    def fetch_sequence(self):
        self.ui.episode_list_widget.setStyleSheet( """QListWidget{background:  none;}""")
        self.ui.sequence_list_widget.clear()
        episode = self.ui.episode_list_widget.currentItem().text()
        self.scene_ctx.context.update(entityGrp= episode)
        self.episode_path = os.path.join(self.publish_path, episode).replace('\\', '/')
        list_seq_folder = os.listdir(self.episode_path)
        seq_list = self.get_data_seq(list_seq_folder)
        for seq in  seq_list:
            item = QtWidgets.QListWidgetItem(self.ui.sequence_list_widget)
            item.setText(seq)
            
    def get_data_seq(self, list_seq_folder):
        seq_list = []
        self.seq_data_dict= {}
        for folder_name in  list_seq_folder:
            seq = folder_name.split('_')[2]
            ############## seq_data_dict >>> backend
            if seq in self.seq_data_dict:
                list_data = self.seq_data_dict[seq]
                list_data.append(folder_name)
                self.seq_data_dict[seq] = list_data
            else:
                self.seq_data_dict[seq] = [folder_name]
            #################  seq_list >>> display 
            if not seq in seq_list:
                seq_list.append(seq)
        return seq_list 

    def get_shot(self):
        seq_wid_items = self.ui.sequence_list_widget.selectedItems()
        self.ui.shot_list_widget.clear()
        for seq_item in seq_wid_items:
            self.shot_data_dict = {}
            if self.scene_ctx.step != 'none':
                cur_seq = seq_item.text()
                print seq_item, cur_seq
                self.fetch_shot(self.seq_data_dict[cur_seq])
                self.chk_sync_video()
            else:
                self.ui.step_list_widget.setStyleSheet( """QListWidget{background: red;}""")

    def fetch_shot(self, list_shot):
        for entity_shot in list_shot:
            self.scene_ctx.context.update(entity = entity_shot)
            output_path = self.scene_ctx.path.output_hero_img().abs_path()
            if os.path.exists(output_path):
                list_file = os.listdir(output_path)

                for file in list_file:
                    file_path = os.path.join(output_path, file).replace('\\', '/')
                    ext = os.path.splitext(file_path)[1]
                    if self.ui.mov_radioButton.isChecked():
                        if ext == '.mov':
                            shot_item = QtWidgets.QListWidgetItem(self.ui.shot_list_widget)
                            shot_item.setText(file_path)
                            print file_path
                            shot_item.setData(QtCore.Qt.UserRole,file_path)
                            self.ui.shot_list_widget.addItem(shot_item)
                    elif self.ui.exr_radioButton.isChecked():
                        if ext == '.exr':
                            shot_item = QtWidgets.QListWidgetItem(self.ui.shot_list_widget)
                            shot_item.setText(file_path)
                            shot_item.setData(QtCore.Qt.UserRole,file_path)
                            self.ui.shot_list_widget.addItem(shot_item)
        


    def chk_sync_video(self):
        local_root = self.ui.local_lineEdit.text()
        self.copy_path = {}
        if local_root == '':
            self.ui.local_lineEdit.setStyleSheet( """QLineEdit{background: red;}""")
        else:
            project_cur = self.ui.project_comboBox.currentText()
            step_cur = self.ui.step_list_widget.currentItem().text()
            if len(local_root) == 1:
                local_root = '%s:'%local_root 
            new_path = '%s/%s/sources/%s'%(local_root, project_cur, step_cur)
            list_item =  self.ui.shot_list_widget.count()
            if self.ui.mov_radioButton.isChecked():
                ext = 'mov'
            elif self.ui.exr_radioButton.isChecked():
                ext = 'exr'    

            for ix in range(0, list_item):
                src_path = self.ui.shot_list_widget.item(ix).text()
                scene = context_info.ContextPathInfo(path= src_path)
                publ_path = scene.path.output_hero_img().abs_path()
                base_name = scene.publish_name(hero=True, ext ='.mov')
                dst_path = os.path.join(new_path, base_name).replace('\\', '/')
                if not os.path.exists(dst_path):
                    self._mkdir_recursive(dst_path)
                    self.ui.shot_list_widget.item(ix).setIcon(QtGui.QPixmap(icon.no))
                    self.copy_path[ix] = [src_path, dst_path]
                else:
                    mtime_src = os.path.getmtime(src_path)
                    mtime_dst = os.path.getmtime(dst_path)
                    if mtime_src != mtime_dst:
                        self.ui.shot_list_widget.item(ix).setIcon(QtGui.QPixmap(icon.needFix))
                        print self.ui.shot_list_widget.item(ix)
                        self.copy_path[ix] = [src_path, dst_path]
                    else:
                        self.ui.shot_list_widget.item(ix).setIcon(QtGui.QPixmap(icon.success))
    def print_text(self):
        print self.copy_path
        for key, value in self.copy_path.iteritems():
            print key, 'testst'
            print value[0], value[1]

    def sync_select(self):
        print self.copy_path,
        items = self.ui.shot_list_widget.selectedItems()
        for item in items:
            row = self.ui.shot_list_widget.row(item)
            if row in self.copy_path: 
                src_path, dst_path = self.copy_path[row]
                status_copy = self.copy_file(src_path, dst_path)
                if status_copy:
                    self.ui.shot_list_widget.item(row).setIcon(QtGui.QPixmap(icon.success))
                        
    def copy_file(self, src_path, dst_path):
        file_utils.xcopy_file(src_path, dst_path)
        if os.path.exists(dst_path):
            return True          
        else:
            return False     

    def _mkdir_recursive(self, path):
        sub_path = os.path.dirname(path)
        if not os.path.exists(sub_path):
            self._mkdir_recursive(sub_path)
        if not os.path.exists(path) and not '.' in os.path.basename(path):
            os.mkdir(path)

def show():
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        maya_win.deleteUI(uiName)
        myApp = RFMovCollector(maya_win.getMayaWindow())
        myApp.show()
        return myApp
    else:
        logger.info('Run in Standalone\n')
        app = QtWidgets.QApplication.instance()
        if not app: 
            app = QtWidgets.QApplication(sys.argv)
        myApp = RFMovCollector()
        stylesheet.set_default(app)
        sys.exit(app.exec_())
if __name__ == '__main__':
    show()
