import os 
import sys
sys.path.append('O:/Pipeline/core')
import rf_config
from rf_utils.sg import sg_process
sg = sg_process.sg 
from rf_utils.context import context_info
from rf_utils import file_utils
dst = 'P:/Hanuman/edit/work/_data'

def collect(project='Hanuman', episode='ch01', step='anim'):
    movs = list()
    filename = '{}/{}_{}_{}.yml'.format(dst, project, episode, step)

    shots = sg.find('Shot', [
        ['project.Project.name', 'is', project], 
        ['sg_shot_type', 'is', 'Shot'],
        ['sg_episode.Scene.code', 'is', episode]], ['code', 'sg_episode'])

    for shot in shots: 
        drive = 'R:'
        context = context_info.Context()
        context.update(project=project, entityType='scene', entity=shot['code'], step=step, process='main', app='maya')
        scene = context_info.ContextPathInfo(context=context)
        media_path = scene.path.scheme(key='outputImgPath').abs_path()
        versions = list_version(media_path)
        if not versions: 
            drive = 'P:'
            media_path = media_path.replace('R:', drive)
            versions = list_version(media_path)

        if versions: 
            latest_version = sorted(versions)[-1]
            scene.context.update(version=latest_version, publishVersion=latest_version)
            media_path = scene.path.scheme(key='outputImgPath').abs_path()
            media_path = media_path.replace('R:', drive)
            name = scene.publish_name(ext='.mov')
            if step in ['light', 'comp']: 
                name = scene.output_name(outputKey='reviewOutput')
            mov = '{}/{}'.format(media_path, name)

            if os.path.exists(mov): 
                movs.append(mov)
                print mov
        else: 
            print 'missing {}'.format(shot['code'])

    file_utils.ymlDumper(filename, movs)


def list_version(path): 
    version_path = os.path.split(os.path.split(path)[0])[0]
    versions = [a for a in file_utils.list_folder(version_path) if a.startswith('v')]
    return versions 
