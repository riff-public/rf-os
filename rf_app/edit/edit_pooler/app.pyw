import sys
import os
from view.custom_ui import ShotlistCustomWidget

core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)

# import config
import rf_config as config

from rf_utils.context import context_info
from rf_utils.ui import load, stylesheet
from rf_utils.widget import browse_widget

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import QtWidgets, QtCore, QtGui

UI_PATH = "{core}/rf_app/edit/edit_pooler/view/edit_pooler.ui".format(core=core)


class EditPoolerCtrl(QtWidgets.QMainWindow):
    def __init__(self, args=None):
        super(EditPoolerCtrl, self).__init__()
        self.ui = load.setup_ui(UI_PATH, self)
        # self.xml_type = xml_type
        # self.project = project
        # self.episode = episode
        # self.init_state_app()
        # self.init_signal()
        self.init_ui()
        self.init_shot_list()
        self.ui.show()

    def init_ui(self):
        self.shotlist_widget_custom = ShotlistCustomWidget(self)
        self.ui.horizontalLayout_2.addWidget(self.shotlist_widget_custom)

    def init_state_app(self):
        self.ui.loading_text.setVisible(False)
        self.ui.msg_sync.setVisible(False)
        self.ui.header_text.setText(self.header_txt)
        self.fileFilter = 'Edit Cut(*.xml)'
        self.browser_file = browse_widget.BrowseFile(title='Sequence Xml')
        self.browser_file.set_label('Xml file : ')
        self.browser_file.filter = self.fileFilter
        self.ui.verticalLayout.addWidget(self.browser_file)

        self.fileFilter = 'Cut Shot(*.mov *.mp4)'
        self.browser_vdo_file = browse_widget.BrowseFile(title='Sequence Vdo')
        self.browser_vdo_file.set_label('Vdo file : ')
        self.browser_vdo_file.filter = self.fileFilter
        self.ui.verticalLayout.addWidget(self.browser_vdo_file)

        self.wavFileFilter = 'Cut Shot(*.wav)'
        self.browser_wav_file = browse_widget.BrowseFile(title='Wav File')
        self.browser_wav_file.set_label('Wav file : ')
        self.browser_wav_file.filter = self.wavFileFilter
        self.ui.verticalLayout.addWidget(self.browser_wav_file)

    def init_signal(self):
        self.browser_file.textChanged.connect(self.onchange_browse_file)
        self.browser_vdo_file.textChanged.connect(self.onchange_browse_vdo_file)
        self.browser_wav_file.textChanged.connect(self.onchange_browse_wav_file)
        self.ui.sync_btn.clicked.connect(self.sync_update)
        self.ui.uploads_checkbox.stateChanged.connect(self.onchange_uploads_checkbox)

    def init_shot_list(self):
        self.shotlist_widget_custom.clear()
        # self.work_scene.process_work_scene()
        # work_versions = self.work_scene.get_work_versions()
        # if len(work_versions) == 0:
            # self.shotlist_widget_custom.addItem("Not Found")

        shot_list = [
            "P:/projectName/scene/publ/ep01/pr_ep01_q0040a_s0010/layout/main/hero/outputMedia/pr_ep01_q0040a_s0010_layout_main.hero.mov",
            "P:/projectName/scene/publ/ep01/pr_ep01_q0040a_s0020/layout/main/hero/outputMedia/pr_ep01_q0040a_s0020_layout_main.hero.mov",
            "P:/projectName/scene/publ/ep01/pr_ep01_q0040a_s0030/layout/main/hero/outputMedia/pr_ep01_q0040a_s0030_layout_main.hero.mov"
        ]

        for shot_path in shot_list:
            widget_item = QtWidgets.QListWidgetItem(self.shotlist_widget_custom)
            widget_item.setText(os.path.basename(shot_path))
            data = {
                "shot_path": shot_path,
                "shotname": os.path.basename(shot_path)
            }
            widget_item.setData(QtCore.Qt.UserRole, data)
            self.shotlist_widget_custom.addItem(widget_item)


if __name__ == '__main__':
    app = QtWidgets.QApplication.instance()
    if not app:
        app = QtWidgets.QApplication(sys.argv)
        stylesheet.set_default(app)
    myApp = EditPoolerCtrl(sys.argv)
    # stylesheet.set_default(app)
    sys.exit(app.exec_())
