from rf_app.edit.xml_view.models.sg_services import ShotgunServices


class Shot:
    def __init__(self, project, episode):
        self.sg = ShotgunServices()
        self.shots = []
        # self.shots_mapped = {}
        self.seq_filtered = []
        self.seq_file_title = ""
        self.status = {
            "waiting": "Waiting",
            "matched": "Matched",
            "unmatched": "Unmatched",
            "omit": "Omit",
            "not_found": "Not Found"
        }
        self.project = project
        self.episode = episode
        self.sg_shots_data = []

    def append_shot_xml(self, shot_data):
        shot_data['type'] = self.get_type_file(shot_data['shot_name'])
        self.set_file_title(shot_data)
        sequence = self.get_sequence_shot(shot_data)
        shot_code = self.get_shot_code(shot_data)
        sg_shot_data = self.find_and_compare(shot_code)
        updated_shot_data = self.set_update_data(shot_data, sg_shot_data, sequence, shot_code)
        self.append_shot(updated_shot_data)

    def process_shot_sync_update(self):
        if len(self.shots) == 0:
            error_data = {
                'can_sync': False,
                'msg': "Empty Shots"
            }
            return error_data

        for shot in self.shots:
            if shot['can_sync'] and shot['status'] != "Matched":
                shot_updated = self.sg.update_shot(shot)
                shot['sg_status'] = str(shot_updated['sg_status_list'])
                shot['sg_cut_order'] = str(shot_updated['sg_cut_order'])
                shot['sg_cut_in'] = str(shot_updated['sg_cut_in'])
                shot['sg_cut_out'] = str(shot_updated['sg_cut_out'])
                shot['sg_cut_duration'] = str(shot_updated['sg_cut_duration'])
                shot['sg_working_duration'] = str(shot_updated['sg_working_duration'])
        data = {
            'can_sync': True,
            'data': self.shots
        }
        self.set_sg_shots(self.project, self.episode)
        self.set_shots_in_sequence()
        # self.process_omit_shot_status()
        return data

    def process_omit_shot_status(self):
        for shot_in_seq in self.shots_in_sequence:
            omit_shot = True
            for shot in self.shots:
                if shot.get('shot_code') is not None:
                    if shot['type'] == 'shot' and shot['shot_code'] == shot_in_seq['code']:
                        omit_shot = False
            if omit_shot:
                status_update = "omt"
                shot_updated = self.sg.update_shot_status(shot_in_seq, status_update)

    def clear_shots(self):
        self.shots = []
        self.seq_filtered = []

    def get_sequence_shot(self, shot_data):
        shot_name_split = shot_data['shot_name'].split("_")
        if shot_data['type'] == "shot":
            return shot_name_split[2]
        elif shot_data['type'] == "sequence":
            return "{}_{}".format(self.seq_file_title, shot_name_split[0])

    def get_sequence_name(self, shot_data):
        if shot_data['type'] == "shot":
            sequence_name = shot_data['shot_name'].split("_")[2]
        elif shot_data['type'] == "sequence":
            sequence_name = shot_data['shot_name'].split(".")[0]
        return sequence_name

    def get_shot_code(self, shot_data):
        shot_name_split = shot_data['shot_name'].split("_")
        if shot_data['type'] == "shot":
            if shot_name_split[3].split('.') > 1:
                shot_name_split[3] = shot_name_split[3].split('.')[0]
            shot_code = "{}_{}_{}_{}".format(
                shot_name_split[0],
                shot_name_split[1],
                shot_name_split[2],
                shot_name_split[3]
            )
            return shot_code
        elif shot_data['type'] == "sequence":
            sequence_code = self.get_sequence_code(shot_data['shot_name'])
            return sequence_code

    def get_sequence_code(self, shot_name):
        sequence = shot_name.split(".")[0]
        return "{file_title}_{sequence}_all".format(file_title=self.seq_file_title, sequence=sequence)

    def get_type_file(self, shot_name):
        if len(shot_name.split("_")) == 1:
            return "sequence"
        else:
            return "shot"

    def append_shot(self, shot_data):
        self.shots.append(shot_data)

    def set_shots_in_sequence(self):
        self.shots_in_sequence = []
        for seq in self.seq_filtered:
            sequence_name = self.get_sequence_name(seq)
            for sg_shot in self.sg_shots_data:
                if sg_shot['sg_shot_type'] == "Shot" and sg_shot['sg_sequence_code'] == sequence_name:
                    self.shots_in_sequence.append(sg_shot)

    def set_update_data(self, xml_shot_data, sg_shot_data, sequence, shot_code):
        if sg_shot_data:
            sg_cut_order = "Empty" if str(sg_shot_data['sg_cut_order']) == "None" else str(sg_shot_data['sg_cut_order'])
            sg_cut_in = "Empty" if str(sg_shot_data['sg_cut_in']) == "None" else str(sg_shot_data['sg_cut_in'])
            sg_cut_out ="Empty" if str(sg_shot_data['sg_cut_out']) == "None" else str(sg_shot_data['sg_cut_out'])
            sg_cut_duration = "Empty" if str(sg_shot_data['sg_cut_duration']) == "None" else str(sg_shot_data['sg_cut_duration'])
            sg_working_duration = "Empty" if str(sg_shot_data['sg_working_duration']) == "None" else str(sg_shot_data['sg_working_duration'])

            xml_shot_data.update({
                'id': sg_shot_data['id'],
                'shot_code': shot_code,
                'sequence': sequence,
                'sg_cut_order': sg_cut_order,
                'sg_cut_in': sg_cut_in,
                'sg_cut_out': sg_cut_out,
                'sg_cut_duration': sg_cut_duration,
                'sg_working_duration': sg_working_duration,
                'can_sync': True,
                'status': self.status['waiting'],
                'sg_status': sg_shot_data['sg_status']
            })
        else:
            xml_shot_data.update({
                'sequence': sequence,
                'sg_cut_order': '-',
                'sg_cut_in': '-',
                'sg_cut_out': '-',
                'sg_cut_duration': '-',
                'sg_working_duration': '-',
                'can_sync': False,
                'status': self.status['not_found'],
                'sg_status': self.status['not_found']
            })
        return xml_shot_data

    def cal_duration_sequence(self, cut_in, cut_out):
        return str(cut_out - cut_in)

    def set_file_title(self, shot_data):
        # pr_ep01_q0040_all
        if shot_data['type'] == "shot":
            shot_name_split = shot_data['shot_name'].split("_")
            self.seq_file_title = "{}_{}".format(shot_name_split[0], shot_name_split[1])

    def set_sg_shots(self, project, episode):
        self.sg_shots_data = self.sg.get_shots(project, episode)

    def set_cut_order_shots(self):
        self.shots = sorted(self.shots, key=lambda shot: (int(shot['start']), shot['type']))
        self.seq_filtered = list(filter(lambda shot: shot['type'] == "sequence", self.shots))
        seq_order = 1
        for seq in self.seq_filtered:
            working_duration = 0
            seq_name = self.get_sequence_name(seq)
            shot_order = 1
            for idx, shot in enumerate(self.shots):
                if shot['type'] == "shot" and shot['sequence'] == seq_name:
                    shot['cut_order'] = str(shot_order)
                    shot_order = shot_order + 1
                    working_duration += int(shot['working_duration'])
                if shot['type'] == "sequence" and self.get_sequence_name(shot) == seq_name:
                    shot['cut_order'] = str(seq_order)
                    seq_order = seq_order + 1
                    seq_idx = idx
            self.shots[seq_idx]['working_duration'] = str(working_duration)

    def check_is_sequence_file(self, shot_name):
        if len(shot_name.split("_")) == 1:
            return True
        else:
            return False

    def updated_matched_status(self, shots):
        for shot in shots:
            if shot['sg_status'] == "omt":
                shot['status'] = self.status['omit']
            elif shot['sg_cut_in'] == "Empty" or shot['sg_cut_out'] == "Empty":
                shot['status'] = self.status['waiting']
            elif shot['cut_in'] != shot['sg_cut_in'] or shot['cut_out'] != shot['sg_cut_out'] or shot['duration'] != shot['sg_cut_duration'] or shot['working_duration'] != shot['sg_working_duration']:
                shot['status'] = self.status['unmatched']
            else:
                shot['status'] = self.status['matched']
        return shots

    def find_and_compare(self, shot_code):
        for shot in self.sg_shots_data:
            if shot_code == shot['code']:
                shot['sg_status'] = shot.get('sg_status_list')
                return shot
        return False

    # def find_sequence_shot_mapped(self, sequence_name):
    #     if sequence_name in self.shots_mapped:
    #         return True
    #     return False
