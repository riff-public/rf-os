from rf_app.edit.xml_view.settings import (
    SHOTGUN_URL,
    SCRIPTS_NAME,
    API_KEY
)
from shotgun_api3 import Shotgun


class ShotgunServices:
    def __init__(self):
        self.sg = self.sg_connect()

    def sg_connect(self):
        try:
            sg = Shotgun(
                SHOTGUN_URL,
                SCRIPTS_NAME,
                API_KEY
            )
            print("connected shotgun api: {}".format(sg))
        except Exception as e:
            print("Cannot connect shotgun api.{}".format(e))

        return sg

    def sg_connection(self):
        return self.sg

    def get_shots(self, project, episode, all_sequence):
        project_filtered = {
            "type": project["type"],
            "id": project["id"],
            "name": project["name"]
        }
        episode_filtered = {
            "type": episode["type"],
            "id": episode["id"],
            "code": episode["code"]
        }

        shots_filter = [
            ['project', 'is', project_filtered],
            ['sg_episode', 'is', episode_filtered],
            ['sg_sequence_code', 'in', all_sequence]
        ]

        fields = [
            'id',
            'code',
            'sg_status_list',
            'sg_sequence_code',
            'sg_shot_type',
            'sg_cut_order',
            'sg_cut_in',
            'sg_cut_out',
            'sg_working_duration',
            'sg_cut_duration',
            'sg_episode'
        ]

        shots = self.sg.find('Shot', shots_filter, fields)
        return shots

    def get_project_title(self, project_name):
        project_filter = [
            ['name', 'is', project_name],
        ]
        fields = ['sg_project_code']
        project_code = self.sg.find_one('Project', project_filter, fields)

        return project_code['sg_project_code']

    # def find_shot_data(self, shot_code, project):
    #     project_filtered = {
    #         "type": project["type"],
    #         "id": project["id"],
    #         "name": project["name"]
    #     }

    #     shots_filter = [
    #         ['project', 'is', project_filtered],
    #         ['code', 'is', shot_code]
    #     ]

    #     fields = ['id', 'code', 'sg_cut_in', 'sg_cut_out', 'sg_working_duration', 'sg_episode']
    #     shot = self.sg.find_one('Shot', shots_filter, fields)

    #     return shot

    def update_shot(self, shot_data):
        payload = {
            'sg_status_list': "wtg",
            'sg_cut_order': int(shot_data['cut_order']),
            'sg_cut_in': int(shot_data['cut_in']),
            'sg_cut_out': int(shot_data['cut_out']),
            'sg_working_duration': int(shot_data['working_duration']),
            'sg_cut_duration': int(shot_data['duration'])
        }

        shot_updated = self.sg.update('Shot', shot_data['id'], payload)

        return shot_updated

    def update_duration_animatic(self, shot_id, animatic_data):
        payload = {
            'sg_status_list': "wtg",
            'sg_cut_order': int(animatic_data['cut_order']),
            'sg_cut_in': int(animatic_data['cut_in']),
            'sg_cut_out': int(animatic_data['cut_out']),
            'sg_working_duration': int(animatic_data['working_duration'])
        }

        shot_updated = self.sg.update('Shot', shot_id, payload)

        return shot_updated

    def update_shot_status(self, shot, status_update):
        payload = {
            'sg_status_list': status_update
        }
        shot_updated = self.sg.update('Shot', shot['id'], payload)
        return shot_updated
