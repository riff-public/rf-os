import os
import subprocess
import math
import tempfile
import sys
from decimal import Decimal, ROUND_UP, ROUND_DOWN
from shutil import copy2, rmtree

from rf_app.edit.xml_view.models.sg_services import ShotgunServices

from rf_utils.context import context_info
from rf_utils.pipeline import create_scene

from rf_utils import user_info
from rf_app.publish.asset import sg_hook
from rf_utils.sg import sg_process
from rf_utils import network_utils


class Animatic:
    def __init__(self, project, episode, sequence, department):
        self.sg = ShotgunServices()
        self.animatics = []
        self.animatics_mapped = []
        self.sg_animatic_data = []
        # self.mov_paths = []
        self.all_sequence = []
        # self.shots_in_sequence = []
        self.project_title = self.sg.get_project_title(project["name"])
        self.episode = episode
        self.project = project
        self.sequence = sequence
        self.project_name = project["name"]
        self.department = department
        self.process_map = {
            'edit': 'animatic',
            'layout': 'layout',
            'anim': 'anim'
        }
        self.tasks_map = {
            'edit': 'animatic',
            'layout': 'edit_layout',
            'anim': 'edit_anim'
        }
        self.temp_path = "{tempfile}/split_vdo".format(tempfile=tempfile.gettempdir())

    def create_tempshot_path(self):
        if not os.path.exists(self.temp_path):
            os.makedirs(self.temp_path)

    def process_xml_animatic(self):
        for animatic in self.animatics_mapped:
            if animatic['status'] != "Matched":
                shot = self.make_shot(animatic)
                updated_shot = self.sg.update_duration_animatic(shot[1]['id'], animatic)
                if updated_shot:
                    animatic['sg_working_duration'] = str(updated_shot['sg_working_duration'])

        self.set_sg_animatic(self.project, self.episode)
        self.process_omit_shot_status()

    def process_omit_shot_status(self):
        for sg_animatic in self.sg_animatic_data:
            found = False
            for animatic in self.animatics_mapped:
                if sg_animatic['code'] == animatic['shot_code']:
                    found = True
                    break
            if not found:
                status_update = "omt"
                shot_updated = self.sg.update_shot_status(sg_animatic, status_update)

    def clear_animatic(self):
        self.animatics = []
        self.all_sequence = []

    def append_animatic(self, animatic_data):
        self.animatics.append(animatic_data)

    def map_sequence_shots(self):
        shots_filtered = []
        sequence_filtered = []
        # sequence_order = 1
        for animatic in self.animatics:
            animatic['is_upload'] = True
            if 'type' in animatic:
                if animatic['type'] == "shot":
                    #animatic['type'] = "shot"
                    shots_filtered.append(animatic)
                elif animatic['type'] == "sequence":
                    #animatic['type'] = "sequence"
                    sequence_filtered.append(animatic)
                    self.all_sequence.append(animatic['shot_name'].split('.')[0])
            else:
                if animatic['shot_name'][0] == "s":
                    animatic['type'] = "shot"
                    shots_filtered.append(animatic)
                elif animatic['shot_name'][0] == "q":
                    animatic['type'] = "sequence"
                    sequence_filtered.append(animatic)
                    self.all_sequence.append(animatic['shot_name'].split('.')[0])

        # check Type in trackName
        if not shots_filtered or not sequence_filtered:
            return False

        for seq_idx, sequence in enumerate(sequence_filtered):
            for shot_idx, shot in enumerate(filter(lambda shot: shot.get('sequence') is None, shots_filtered)):
                if int(shot['start']) < int(sequence['end']):
                    shot['sequence'] = sequence['shot_name'].split('.')[0]

        shots_filtered = self.set_animatic_shot_cut_order_all(shots_filtered, sequence_filtered) 
        # change cut order by shots instead of by sequences
        sequence_filtered = self.get_animatic_seq_cut_order(sequence_filtered)
        self.animatics_mapped = shots_filtered + sequence_filtered

    def make_shot(self, animatic):
        is_shot = animatic.get('sequence')
        sequence_name = self.get_sequence_name(animatic)
        if is_shot:
            shot_type = "Shot"
        else:
            shot_type = "Sequence"

        result = create_scene.create(
            project=str(self.project_name),
            name=str(animatic['shot_code']),
            episode=str(self.episode['code']),
            sequence=str(sequence_name),
            shortcode=str(animatic['shot_name'].split('.')[0]),
            shotType=str(shot_type),
            link=True
        )

        return result

    def set_shot_code(self):
        for animatic in self.animatics_mapped:
            shot_code, path_data = self.get_shot_context(animatic)
            animatic['shot_code'] = shot_code
            animatic['file_manager'] = path_data
            # mov_file_path, sound_path = self.get_mov_and_sound_path(animatic)

    def set_sg_animatic(self, project, episode):
        self.sg_animatic_data = self.sg.get_shots(project, episode, self.all_sequence)

    # def set_shots_in_sequence(self):
    #     self.shots_in_sequence = []
    #     for animatic in self.animatics_mapped:
    #         if animatic['type'] == "sequence":
    #             sequence_name = self.get_sequence_name(animatic)
    #             shots = self.get_shots_by_sequence(sequence_name)
    #             self.shots_in_sequence = self.shots_in_sequence + shots

    def get_animatic_seq_cut_order(self, sequences):
        sequence_sorted = sorted(sequences, key=lambda seq: int(seq['start']))
        for idx, seq in enumerate(sequence_sorted):
            seq['cut_order'] = str(idx + 1)

        return sequence_sorted

    def set_animatic_shot_cut_order(self, shots, sequences):
        """ this set the cut order by sequence """ 
        shots_sorted = sorted(shots, key=lambda shot: int(shot['start']))
        for seq in sequences:
            shot_order = 1
            seq_name = seq['shot_name'].split('.')[0]
            for shot in shots_sorted:
                if shot['sequence'] == seq_name:
                    shot['cut_order'] = str(shot_order)
                    shot_order = int(shot_order) + 1
        return shots_sorted

    def set_animatic_shot_cut_order_all(self, shots, sequences):
        """ this set the cut order by shots regardless sequence """ 
        shots_sorted = sorted(shots, key=lambda shot: int(shot['start']))
        shot_order = 1
        for shot in shots_sorted:
            shot['cut_order'] = str(shot_order)
            shot_order = int(shot_order) + 1
        return shots_sorted

    def get_sequence_name(self, animatic):
        is_shot = animatic.get('sequence')
        if is_shot:
            sequence_name = animatic['sequence'].split('.')[0]
        else:
            sequence_name = animatic['shot_name'].split('.')[0]
        return sequence_name

    def get_shot_context(self, animatic):
        is_shot = animatic.get('sequence')
        sequence_name = self.get_sequence_name(animatic)
        if is_shot:
            context = context_info.Context()
            context.update(
                project=self.project_name,
                projectCode=self.project_title,
                entityGrp=self.episode['code'],
                entityParent=sequence_name,
                entityCode=animatic['shot_name'].split('.')[0],
                entityType='scene',
                step='edit',
                process=self.process_map[self.department]
            )
            scene = context_info.ContextPathInfo(context=context)
            shot_code = scene.sg_name
            scene.set_project_code(self.sg.sg)
            scene.set_name()
            mov_path = scene.path.output_hero_img().abs_path()
            mov_filename = scene.publish_name(True, ext='.mov')
            # mov_file_path = '%s/%s' % (path, filename)

            context.update(process='sound')
            sound_path = scene.path.output_hero().abs_path()
            sound_filename = scene.publish_name(True, ext='.wav')
            # sound_path = '%s/%s' % (path, filename)
        else:
            context = context_info.Context()
            context.update(
                project=self.project_name,
                projectCode=self.project_title,
                entityGrp=self.episode['code'],
                entityParent=sequence_name,
                entityCode="all",
                entityType='scene',
                step='edit',
                process=self.process_map[self.department]
            )
            scene = context_info.ContextPathInfo(context=context)
            shot_code = scene.sg_name
            scene.set_project_code(self.sg.sg)
            scene.set_name()
            mov_path = scene.path.output_hero().abs_path()
            mov_filename = scene.publish_name(True, ext='.mov')
            # mov_file_path = '%s/%s' % (path, filename)

            context.update(process='sound')
            sound_path = scene.path.output_hero().abs_path()
            sound_filename = scene.publish_name(True, ext='.wav')
            # sound_path = '%s/%s' % (path, filename)

        path_data = {
            'mov_path': mov_path,
            'mov_filename': mov_filename,
            'sound_hero_path': mov_path,
            'sound_path': sound_path,
            'sound_filename': sound_filename
        }

        return shot_code, path_data

    def get_shots_by_sequence(self, seq):
        shots_in_sequnece = []
        for animatic in self.sg_animatic_data:
            if animatic['sg_shot_type'] == "Shot" and animatic['sg_sequence_code'] == seq:
                shots_in_sequnece.append(animatic)
        return shots_in_sequnece

    def updated_matched_status(self, shots):
        for shot in shots:
            find_shot = self.find_shot_data(shot['shot_code'])
            if find_shot:
                if find_shot['sg_status_list'] == 'omt':
                    shot['status'] = "Omit"
                elif str(shot['working_duration']) == str(find_shot['sg_working_duration']) and \
                    str(shot['cut_order']) == str(find_shot['sg_cut_order']) and \
                    str(shot['cut_in']) == str(find_shot['sg_cut_in']) and \
                    str(shot['cut_out']) == str(find_shot['sg_cut_out']):
                    shot['status'] = "Matched"
                    shot['sg_working_duration'] = str(find_shot['sg_working_duration'])
                    shot['sg_cut_order'] = str(find_shot['sg_cut_order'])
                    shot['sg_cut_in'] = str(find_shot['sg_cut_in'])
                    shot['sg_cut_out'] = str(find_shot['sg_cut_out'])
                elif str(shot['working_duration']) != str(find_shot['sg_working_duration']) or \
                    str(shot['cut_order']) != str(find_shot['sg_cut_order']) or \
                    str(shot['cut_in']) != str(find_shot['sg_cut_in']) or \
                    str(shot['cut_out']) != str(find_shot['sg_cut_out']):
                    shot['status'] = "Unmatched"
                    if find_shot['sg_working_duration'] is None:
                        shot['sg_working_duration'] = "Empty"
                    else:
                        shot['sg_working_duration'] = str(find_shot['sg_working_duration'])

                    if find_shot['sg_cut_order'] is None:
                        shot['sg_cut_order'] = "Empty"
                    else:
                        shot['sg_cut_order'] = str(find_shot['sg_cut_order'])

                    if find_shot['sg_cut_in'] is None:
                        shot['sg_cut_in'] = "Empty"
                    else:
                        shot['sg_cut_in'] = str(find_shot['sg_cut_in'])

                    if find_shot['sg_cut_out'] is None:
                        shot['sg_cut_out'] = "Empty"
                    else:
                        shot['sg_cut_out'] = str(find_shot['sg_cut_out'])
            else:
                shot['status'] = "Not Found"
                shot['sg_working_duration'] = "Empty"
                shot['sg_cut_order'] = "Empty"
                shot['sg_cut_in'] = "Empty"
                shot['sg_cut_out'] = "Empty"

        return shots

    def find_shot_data(self, shot_code):
        for animatic in self.sg_animatic_data:
            if animatic['code'] == shot_code:
                return animatic

    def process_split_shot_vdo(self, vdo_path, wav_path, fps=24, loading_text=None):
        self.split_shots_vdo(vdo_path, wav_path, fps, loading_text=loading_text)

    def split_shots_vdo(self, vdo_path, wav_path, fps=24, loading_text=None):
        for animatic in self.animatics_mapped:
            if animatic['type'] == "shot":
                mov_path = animatic['file_manager']['mov_path']
                mov_filename = animatic['file_manager']['mov_filename']
                sound_path = animatic['file_manager']['sound_path']
                sound_filename = animatic['file_manager']['sound_filename']

                if not os.path.exists(mov_path):
                    os.makedirs(mov_path)
                if not os.path.exists(sound_path):
                    os.makedirs(sound_path)

                output_no_wav_vdo_path = "{mov_path}/empty_sound_{mov_filename}".format(mov_path=self.temp_path, mov_filename=mov_filename)
                output_vdo_path = "{mov_path}/{mov_filename}".format(mov_path=mov_path, mov_filename=mov_filename)

                output_wav_path = "{sound_path}/{sound_filename}".format(sound_path=sound_path, sound_filename=sound_filename)
                output_hero_wav_path = "{mov_path}/{sound_filename}".format(mov_path=mov_path, sound_filename=sound_filename)

                start_frame_vdo = str(Decimal(str(int(animatic['start']) / float(fps))).quantize(Decimal('1e-4'), rounding=ROUND_DOWN))
                end_frame_vdo = str(Decimal(str((int(animatic['end']) - int(animatic['start'])) / float(fps))).quantize(Decimal('1e-4'), rounding=ROUND_DOWN))
                start_frame_wav = str(int(animatic['start']) / float(fps))
                end_frame_wav = str((int(animatic['end']) - int(animatic['start'])) / float(fps))
                # start_frame = str(math.floor(int(animatic['start']) / float(fps) * 100) / 100)
                # end_frame = str(math.floor((int(animatic['end']) - int(animatic['start'])) / float(fps) * 100) / 100)
                subprocess.call([
                    'O:/Pipeline/lib/ffmpeg-20161227-0ff8c6b-win64-static/bin/ffmpeg.exe',
                    '-y',
                    '-ss',
                    start_frame_vdo,
                    '-t',
                    end_frame_vdo,
                    '-i',
                    vdo_path,
                    '-c:v',
                    'libx264',
                    '-crf',
                    '24',
                    '-profile:v',
                    'baseline',
                    '-level',
                    '3.0',
                    '-pix_fmt',
                    'yuv420p',
                    '-r',
                    str(fps),
                    '-an',
                    output_no_wav_vdo_path
                ])
                subprocess.call([
                    'O:/Pipeline/lib/ffmpeg-20161227-0ff8c6b-win64-static/bin/ffmpeg.exe',
                    '-i',
                    wav_path,
                    '-y',
                    '-ss',
                    start_frame_wav,
                    '-t',
                    end_frame_wav,
                    '-acodec',
                    'pcm_s16le',
                    '-ac',
                    '2',
                    '-r',
                    str(fps),
                    output_wav_path
                ])
                # merge vdo and wav
                copy2(output_wav_path, output_hero_wav_path)
                subprocess.call([
                    'O:/Pipeline/lib/ffmpeg-20161227-0ff8c6b-win64-static/bin/ffmpeg.exe',
                    '-y',
                    '-i',
                    output_no_wav_vdo_path,
                    '-i',
                    output_wav_path,
                    '-c',
                    'copy',
                    output_vdo_path
                ])
                # subprocess.call([
                #     'O:/Pipeline/lib/ffmpeg-20161227-0ff8c6b-win64-static/bin/ffmpeg.exe',
                #     '-y',
                #     '-i',
                #     output_vdo_path,
                #     output_hero_wav_path
                # ])
                # self.mov_paths.append(output_vdo_path)

    def upload_vdo_animatic(self, vdo_path):
        for animatic in self.animatics_mapped:
            if animatic.get('is_upload'):
                if animatic['type'] == "shot":
                    mov_path = animatic['file_manager']['mov_path']
                    mov_filename = animatic['file_manager']['mov_filename']
                    output_vdo_path = "{mov_path}/{mov_filename}".format(mov_path=mov_path, mov_filename=mov_filename)
                    user = user_info.User()
                    context = context_info.Context()
                    context.use_sg(
                        self.sg.sg,
                        project=self.project_name,
                        entityType='scene',
                        entityName=animatic['shot_code']
                    )
                    context.update(step='edit', process=self.process_map[self.department], publishVersion='v001')
                    scene = context_info.ContextPathInfo(context=context)

                    # projectName:ep01:scene:ep01:q0040a:pr_ep01_q0040a_s0130:edit:animatic:none:none::none:none:none:v001:s0130:pr:3915:none
                    taskEntity = sg_process.get_one_task_by_step(
                        scene.context.sgEntity,
                        step='edit',
                        task=self.tasks_map[self.department],
                        create=True
                    )
                    status = 'apr'
                    userEntity = user.sg_user()
                    description = 'Animatic from Edit'

                    args = [
                        scene,
                        taskEntity,
                        status,
                        [output_vdo_path],
                        userEntity,
                        description
                    ]
                    version_result = network_utils.make_retries(
                        sg_hook.publish_version,
                        args,
                        retries_count=5
                    )
                elif animatic['type'] == "sequence":
                    user = user_info.User()
                    context = context_info.Context()
                    context.use_sg(
                        self.sg.sg,
                        project=self.project_name,
                        entityType='scene',
                        entityName=animatic['shot_code']
                    )
                    context.update(
                        step='edit',
                        process=self.process_map[self.department],
                        publishVersion='v001'
                    )
                    scene = context_info.ContextPathInfo(context=context)

                    taskEntity = sg_process.get_one_task_by_step(
                        scene.context.sgEntity,
                        step='edit',
                        task=self.tasks_map[self.department],
                        create=True
                    )
                    status = 'apr'
                    userEntity = user.sg_user()
                    description = 'Animatic from Edit'

                    args = [
                        scene,
                        taskEntity,
                        status,
                        [output_vdo_path],
                        userEntity,
                        description
                    ]
                    version_result = network_utils.make_retries(
                        sg_hook.publish_version,
                        args,
                        retries_count=5
                    )
            else:
                print "this video not upload"

    def get_reference_edit_path(self):
        return "P:/{project_name}/edit/output/{department}".format(
            project_name=self.project_name,
            department=self.process_map[self.department]
        )

    def process_copy_reference_file(self, src_xml_path, src_vdo_path, src_wav_path):
        dst_path = self.get_reference_edit_path()
        ext_xml = os.path.splitext(src_xml_path)[1]
        ext_vdo = os.path.splitext(src_vdo_path)[1]
        ext_wav = os.path.splitext(src_wav_path)[1]
        if not os.path.exists(dst_path):
            os.makedirs(dst_path)
        list_seq_files = os.listdir(dst_path)
        version_file_count = 1
        for seq_file in list_seq_files:
            if self.sequence['code'] in seq_file:
                version_file_count += 1

        version_file_count = int(math.ceil(float(version_file_count) / float(2)))
        dst_xml_file_name = "{seq_code}_{department}.v{version_file_count}{ext_xml}".format(
            seq_code=self.sequence['code'],
            department=self.process_map[self.department],
            version_file_count=str(version_file_count).zfill(3),
            ext_xml=ext_xml
        )
        dst_vdo_file_name = "{seq_code}_{department}.v{version_file_count}{ext_vdo}".format(
            seq_code=self.sequence['code'],
            department=self.process_map[self.department],
            version_file_count=str(version_file_count).zfill(3),
            ext_vdo=ext_vdo
        )
        dst_wav_file_name = "{seq_code}_{department}.v{version_file_count}{ext_wav}".format(
            seq_code=self.sequence['code'],
            department=self.process_map[self.department],
            version_file_count=str(version_file_count).zfill(3),
            ext_wav=ext_wav
        )

        dst_xml_file_path = "{dst_path}/{dst_xml_file_name}".format(
            dst_path=dst_path,
            dst_xml_file_name=dst_xml_file_name
        )
        dst_vdo_file_path = "{dst_path}/{dst_vdo_file_name}".format(
            dst_path=dst_path,
            dst_vdo_file_name=dst_vdo_file_name
        )
        dst_wav_file_path = "{dst_path}/{dst_wav_file_name}".format(
            dst_path=dst_path,
            dst_wav_file_name=dst_wav_file_name
        )
        copy2(src_xml_path, dst_xml_file_path)
        copy2(src_vdo_path, dst_vdo_file_path)
        copy2(src_vdo_path, dst_wav_file_path)

    def toggle_upload_vdo(self, toggle_upload_vdo, row_selected):
        self.animatics_mapped[row_selected].update({"is_upload": toggle_upload_vdo.isChecked()})

    def remove_tempshots(self):
        if os.path.exists(self.temp_path):
            rmtree(self.temp_path)
