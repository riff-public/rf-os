SHOT_DATA = {
    'duration': '30',
    'cut_out': '30',
    'shot_name': 'pr_ep01_q0040_s0010_anim.hero.mov',
    'cut_in': '0'
}

SHOT_DATA_2 = {
    'duration': '20',
    'cut_out': '20',
    'shot_name': 'pr_ep01_q0040_s0020.hero.mov',
    'cut_in': '0'
}

ANIMATIC = [
    {
        'shot_name': 's0010.png',
        'end': '31',
        'start': '0',
        'cut_in': '1',
        'duration': '31',
        'cut_out': '1'
   },
   {
        'shot_name': 's0020.png',
        'end': '82',
        'start': '31',
        'cut_in': '1',
        'duration': '51',
        'cut_out': '1'
   },
   {
        'shot_name': 's0010.png',
        'end': '111',
        'start': '82',
        'cut_in': '1',
        'duration': '29',
        'cut_out': '1'
   },
   {
        'shot_name': 's0020.png',
        'end': '166',
        'start': '111',
        'cut_in': '1',
        'duration': '55',
        'cut_out': '1'
   },
   {
        'shot_name': 's0030.png',
        'end': '202',
        'start': '166',
        'cut_in': '1',
        'duration': '36',
        'cut_out': '1'
   },
   {
        'shot_name': 's0040.png',
        'end': '228',
        'start': '202',
        'cut_in': '1',
        'duration': '26',
        'cut_out': '1'
   },
   {
        'shot_name': 'q0010.png',
        'end': '82',
        'start': '0',
        'cut_in': '1',
        'duration': '82',
        'cut_out': '1'
   },
   {
        'shot_name': 'q0020.png',
        'end': '228',
        'start': '82',
        'cut_in': '1',
        'duration': '146',
        'cut_out': '1'
   }
]

EXPECTED_MAP_ANIMATIC = [
    {
        'shot_name': 's0010.png',
        'cut_order': '1',
        'type': 'shot',
        'end': '31',
        'start': '0',
        'cut_in': '1',
        'duration': '31',
        'cut_out': '1',
        'sequence': 'q0010'
   },
   {
        'shot_name': 's0020.png',
        'cut_order': '2',
        'type': 'shot',
        'end': '82',
        'start': '31',
        'cut_in': '1',
        'duration': '51',
        'cut_out': '1',
        'sequence': 'q0010'
   },
   {
        'shot_name': 's0010.png',
        'cut_order': '1',
        'type': 'shot',
        'end': '111',
        'start': '82',
        'cut_in': '1',
        'duration': '29',
        'cut_out': '1',
        'sequence': 'q0020'
   },
   {
        'shot_name': 's0020.png',
        'cut_order': '2',
        'type': 'shot',
        'end': '166',
        'start': '111',
        'cut_in': '1',
        'duration': '55',
        'cut_out': '1',
        'sequence': 'q0020'
   },
   {
        'shot_name': 's0030.png',
        'cut_order': '3',
        'type': 'shot',
        'end': '202',
        'start': '166',
        'cut_in': '1',
        'duration': '36',
        'cut_out': '1',
        'sequence': 'q0020'
   },
   {
        'shot_name': 's0040.png',
        'cut_order': '4',
        'type': 'shot',
        'end': '228',
        'start': '202',
        'cut_in': '1',
        'duration': '26',
        'cut_out': '1',
        'sequence': 'q0020'
   },
   {
        'shot_name': 'q0010.png',
        'cut_order': '1',
        'type': 'sequence',
        'end': '82',
        'start': '0',
        'cut_in': '1',
        'duration': '82',
        'cut_out': '1'
   },
   {
        'shot_name': 'q0020.png',
        'cut_order': '2',
        'type': 'sequence',
        'end': '228',
        'start': '82',
        'cut_in': '1',
        'duration': '146',
        'cut_out': '1'
   }
]

SEQ_FILTERED = [
  {'shot_name': 'q0110.png', 'end': '140', 'start': '0', 'cut_in': '1', 'duration': '140', 'cut_out': '1', 'type': 'sequence'},
  {'shot_name': 'q0020.png', 'end': '657', 'start': '0', 'cut_in': '1', 'duration': '657', 'cut_out': '1', 'type': 'sequence'},
  {'shot_name': 'q0010.png', 'end': '400', 'start': '0', 'cut_in': '1', 'duration': '400', 'cut_out': '1', 'type': 'sequence'},
  {'shot_name': 'q0030.png', 'end': '250', 'start': '0', 'cut_in': '1', 'duration': '250', 'cut_out': '1', 'type': 'sequence'},
]

SHOT_FILTERED = [
    {'shot_name': 's0110.png', 'end': '657', 'start': '553', 'cut_in': '1', 'duration': '104', 'cut_out': '1'},
    {'shot_name': 's0010.png', 'end': '61', 'start': '0', 'cut_in': '1', 'duration': '61', 'cut_out': '1'},
    {'shot_name': 's0020.png', 'end': '89', 'start': '61', 'cut_in': '1', 'duration': '28', 'cut_out': '1'},
    {'shot_name': 's0030.png', 'end': '137', 'start': '89', 'cut_in': '1', 'duration': '48', 'cut_out': '1'},
    {'shot_name': 's0040.png', 'end': '169', 'start': '137', 'cut_in': '1', 'duration': '32', 'cut_out': '1'},
    {'shot_name': 's0050.png', 'end': '243', 'start': '169', 'cut_in': '1', 'duration': '74', 'cut_out': '1'},
    {'shot_name': 's0060.png', 'end': '267', 'start': '243', 'cut_in': '1', 'duration': '24', 'cut_out': '1'},
    {'shot_name': 's0080.png', 'end': '444', 'start': '394', 'cut_in': '1', 'duration': '50', 'cut_out': '1'},
    {'shot_name': 's0100.png', 'end': '601', 'start': '481', 'cut_in': '1', 'duration': '120', 'cut_out': '1'},
    {'shot_name': 's0070.png', 'end': '394', 'start': '267', 'cut_in': '1', 'duration': '127', 'cut_out': '1'},
    {'shot_name': 's0090.png', 'end': '481', 'start': '421', 'cut_in': '1', 'duration': '60', 'cut_out': '1'}
]

SEQ_DATA = [
    {'shot_name': 'q0010.png', 'end': '350', 'start': '250', 'cut_in': '1', 'duration': '100', 'cut_out': '1', 'type': 'sequence'},
    {'shot_name': 'q0020.png', 'end': '450', 'start': '350', 'cut_in': '1', 'duration': '100', 'cut_out': '1', 'type': 'sequence'},
    {'shot_name': 'q0030.png', 'end': '82', 'start': '0', 'cut_in': '1', 'duration': '82', 'cut_out': '1', 'type': 'sequence'},
    {'shot_name': 'q0040.png', 'end': '228', 'start': '82', 'cut_in': '1', 'duration': '146', 'cut_out': '1', 'type': 'sequence'}
]


SHOT_DATA = [
    {'shot_name': 's0110.png', 'end': '300', 'sequence': 'q0030', 'start': '250', 'cut_in': '1', 'duration': '50', 'cut_out': '1', 'type': 'shot'},
    {'shot_name': 's0010.png', 'end': '111', 'sequence': 'q0020', 'start': '82', 'cut_in': '1', 'duration': '29', 'cut_out': '1', 'type': 'shot'},
    {'shot_name': 's0010.png', 'end': '31', 'sequence': 'q0010', 'start': '0', 'cut_in': '1', 'duration': '31', 'cut_out': '1', 'type': 'shot'},
    {'shot_name': 's0030.png', 'end': '500', 'sequence': 'q0030', 'start': '380', 'cut_in': '1', 'duration': '120', 'cut_out': '1', 'type': 'shot'},
    {'shot_name': 's0040.png', 'end': '228', 'sequence': 'q0020', 'start': '202', 'cut_in': '1', 'duration': '26', 'cut_out': '1', 'type': 'shot'},
    {'shot_name': 's0020.png', 'end': '82', 'sequence': 'q0010', 'start': '31', 'cut_in': '1', 'duration': '51', 'cut_out': '1', 'type': 'shot'},
    {'shot_name': 's0020.png', 'end': '166', 'sequence': 'q0020', 'start': '111', 'cut_in': '1', 'duration': '55', 'cut_out': '1', 'type': 'shot'},
    {'shot_name': 's0030.png', 'end': '202', 'sequence': 'q0020', 'start': '166', 'cut_in': '1', 'duration': '36', 'cut_out': '1', 'type': 'shot'},
    {'shot_name': 's0130.png', 'end': '400', 'sequence': 'q0030', 'start': '320', 'cut_in': '1', 'duration': '80', 'cut_out': '1', 'type': 'shot'}
]
