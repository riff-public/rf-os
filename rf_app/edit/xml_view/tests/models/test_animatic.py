import unittest
from tests.models.data.test_shot_data import (
    ANIMATIC,
    EXPECTED_MAP_ANIMATIC,
    SEQ_FILTERED,
    SHOT_FILTERED,
    SEQ_DATA,
    SHOT_DATA
)
from models.animatic import Animatic

DEFAULT_PROJECT = {
    "type": "Project",
    "id": 134,
    "name": "projectName"
}
DEFAULT_EPISODE = {
    "code": "ep01"
}


class TestAnimatic(unittest.TestCase):
    def setUp(self):
        self.animatic_model = Animatic(DEFAULT_PROJECT, DEFAULT_EPISODE)
        self.animatic_model.animatics = ANIMATIC

    def test_map_sequence_shots(self):
        self.animatic_model.map_sequence_shots()
        self.assertEqual(self.animatic_model.animatics_mapped, EXPECTED_MAP_ANIMATIC)

    # def test_human_sort_sequence(self):
    #     expected_seq_filtered = [
    #       {'shot_name': 'q0010.png', 'end': '400', 'start': '0', 'cut_in': '1', 'duration': '400', 'cut_out': '1', 'type': 'sequence'},
    #       {'shot_name': 'q0020.png', 'end': '657', 'start': '0', 'cut_in': '1', 'duration': '657', 'cut_out': '1', 'type': 'sequence'},
    #       {'shot_name': 'q0030.png', 'end': '250', 'start': '0', 'cut_in': '1', 'duration': '250', 'cut_out': '1', 'type': 'sequence'},
    #       {'shot_name': 'q0110.png', 'end': '140', 'start': '0', 'cut_in': '1', 'duration': '140', 'cut_out': '1', 'type': 'sequence'}
    #     ]
    #     actual = self.animatic_model.human_sort_sequence(SEQ_FILTERED)
    #     self.assertEqual(actual, expected_seq_filtered)

    # def test_human_sort_shot(self):
    #     expected_shot_filtered = [
    #         {'shot_name': 's0010.png', 'end': '61', 'start': '0', 'cut_in': '1', 'duration': '61', 'cut_out': '1'},
    #         {'shot_name': 's0020.png', 'end': '89', 'start': '61', 'cut_in': '1', 'duration': '28', 'cut_out': '1'},
    #         {'shot_name': 's0030.png', 'end': '137', 'start': '89', 'cut_in': '1', 'duration': '48', 'cut_out': '1'},
    #         {'shot_name': 's0040.png', 'end': '169', 'start': '137', 'cut_in': '1', 'duration': '32', 'cut_out': '1'},
    #         {'shot_name': 's0050.png', 'end': '243', 'start': '169', 'cut_in': '1', 'duration': '74', 'cut_out': '1'},
    #         {'shot_name': 's0060.png', 'end': '267', 'start': '243', 'cut_in': '1', 'duration': '24', 'cut_out': '1'},
    #         {'shot_name': 's0070.png', 'end': '394', 'start': '267', 'cut_in': '1', 'duration': '127', 'cut_out': '1'},
    #         {'shot_name': 's0080.png', 'end': '444', 'start': '394', 'cut_in': '1', 'duration': '50', 'cut_out': '1'},
    #         {'shot_name': 's0090.png', 'end': '481', 'start': '421', 'cut_in': '1', 'duration': '60', 'cut_out': '1'},
    #         {'shot_name': 's0100.png', 'end': '601', 'start': '481', 'cut_in': '1', 'duration': '120', 'cut_out': '1'},
    #         {'shot_name': 's0110.png', 'end': '657', 'start': '553', 'cut_in': '1', 'duration': '104', 'cut_out': '1'}
    #     ]
    #     actual = self.animatic_model.human_sort_shot(expected_shot_filtered)
    #     self.assertEqual(actual, expected_shot_filtered)

    def test_set_animatic_seq_cut_order(self):
        expected_seq_filtered = [
            {'shot_name': 'q0030.png', 'end': '82', 'start': '0', 'cut_in': '1', 'duration': '82', 'cut_out': '1', 'type': 'sequence', 'cut_order': '1'},
            {'shot_name': 'q0040.png', 'end': '228', 'start': '82', 'cut_in': '1', 'duration': '146', 'cut_out': '1', 'type': 'sequence', 'cut_order': '2'},
            {'shot_name': 'q0010.png', 'end': '350', 'start': '250', 'cut_in': '1', 'duration': '100', 'cut_out': '1', 'type': 'sequence', 'cut_order': '3'},
            {'shot_name': 'q0020.png', 'end': '450', 'start': '350', 'cut_in': '1', 'duration': '100', 'cut_out': '1', 'type': 'sequence', 'cut_order': '4'}
        ]
        actual = self.animatic_model.set_animatic_seq_cut_order(SEQ_DATA)
        self.assertEqual(actual, expected_seq_filtered)

    def test_set_animatic_shot_cut_order(self):
        seq = [
            {'shot_name': 'q0030.png', 'end': '500', 'start': '250', 'cut_in': '1', 'duration': '82', 'cut_out': '1', 'type': 'sequence', 'cut_order': '3'},
            {'shot_name': 'q0010.png', 'end': '31', 'start': '0', 'cut_in': '1', 'duration': '100', 'cut_out': '1', 'type': 'sequence', 'cut_order': '1'},
            {'shot_name': 'q0020.png', 'end': '228', 'start': '82', 'cut_in': '1', 'duration': '100', 'cut_out': '1', 'type': 'sequence', 'cut_order': '2'}
        ]
        expected_shots_filtered = [
            {'shot_name': 's0010.png', 'end': '31', 'sequence': 'q0010', 'start': '0', 'cut_in': '1', 'duration': '31', 'cut_out': '1', 'type': 'shot', 'cut_order': '1'},
            {'shot_name': 's0020.png', 'end': '82', 'sequence': 'q0010', 'start': '31', 'cut_in': '1', 'duration': '51', 'cut_out': '1', 'type': 'shot', 'cut_order': '2'},
            {'shot_name': 's0010.png', 'end': '111', 'sequence': 'q0020', 'start': '82', 'cut_in': '1', 'duration': '29', 'cut_out': '1', 'type': 'shot', 'cut_order': '1'},
            {'shot_name': 's0020.png', 'end': '166', 'sequence': 'q0020', 'start': '111', 'cut_in': '1', 'duration': '55', 'cut_out': '1', 'type': 'shot', 'cut_order': '2'},
            {'shot_name': 's0030.png', 'end': '202', 'sequence': 'q0020', 'start': '166', 'cut_in': '1', 'duration': '36', 'cut_out': '1', 'type': 'shot', 'cut_order': '3'},
            {'shot_name': 's0040.png', 'end': '228', 'sequence': 'q0020', 'start': '202', 'cut_in': '1', 'duration': '26', 'cut_out': '1', 'type': 'shot', 'cut_order': '4'},
            {'shot_name': 's0110.png', 'end': '300', 'sequence': 'q0030', 'start': '250', 'cut_in': '1', 'duration': '50', 'cut_out': '1', 'type': 'shot', 'cut_order': '1'},
            {'shot_name': 's0130.png', 'end': '400', 'sequence': 'q0030', 'start': '320', 'cut_in': '1', 'duration': '80', 'cut_out': '1', 'type': 'shot', 'cut_order': '2'},
            {'shot_name': 's0030.png', 'end': '500', 'sequence': 'q0030', 'start': '380', 'cut_in': '1', 'duration': '120', 'cut_out': '1', 'type': 'shot', 'cut_order': '3'}
        ]
        actual = self.animatic_model.set_animatic_shot_cut_order(SHOT_DATA, seq)
        self.assertEqual(actual, expected_shots_filtered)

    def tearDown(self):
        pass


if __name__ == '__main__':
    unittest.main()
