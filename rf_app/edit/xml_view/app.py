import sys
import os
import xml.dom.minidom
from rf_utils.context import context_info

core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)

# import config
import rf_config as config

from rf_utils import project_info
from rf_utils.ui import load, stylesheet
from rf_utils.widget import browse_widget

from models.shot import Shot
from models.animatic import Animatic
os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import QtWidgets, QtCore, QtGui
from functools import partial

UI_PATH = "{core}/rf_app/edit/xml_view/view/ui.ui".format(core=core)
DEFAULT_PROJECT = {
    "type": "Project",
    "id": 134,
    "name": "projectName"
}
DEFAULT_EPISODE = {
    "code": "ep01"
}


class ShotXmlViewer(QtWidgets.QMainWindow):
    def __init__(self, args=None, xml_type="edit", project=DEFAULT_PROJECT, episode=DEFAULT_EPISODE, seq=""):
        super(ShotXmlViewer, self).__init__()
        self.ui = load.setup_ui(UI_PATH, self)
        self.xml_type = xml_type
        self.project = project
        self.episode = episode
        self.vdo_path = ""
        self.sequence = seq
        if xml_type == "layout":
            # self.shots_model = Shot(project=self.project, episode=self.episode)
            self.header_txt = "Edit Layout"
            self.department = "layout"
        elif xml_type == "edit":
            self.header_txt = "Edit Animatic"
            self.department = "edit"
        elif xml_type == "anim":
            self.header_txt = "Edit Anim"
            self.department = "anim"
            # self.animatic_model = Animatic(project=self.project, episode=self.episode, sequence=self.sequence)
        self.animatic_model = Animatic(
            project=self.project,
            episode=self.episode,
            sequence=self.sequence,
            department=self.department
        )
        projectInfo = project_info.ProjectInfo(project=self.project['name'])
        self.fps = projectInfo.render.fps()
        # self.fps = 24
        self.init_state_app()
        self.init_signal()
        self.ui.show()

    def init_state_app(self):
        self.ui.loading_text.setVisible(False)
        self.ui.msg_sync.setVisible(False)
        self.ui.header_text.setText(self.header_txt)
        self.fileFilter = 'Edit Cut(*.xml)'
        self.browser_file = browse_widget.BrowseFile(title='Sequence Xml')
        self.browser_file.set_label('Xml file : ')
        self.browser_file.filter = self.fileFilter
        self.ui.verticalLayout.addWidget(self.browser_file)

        self.fileFilter = 'Cut Shot(*.mov *.mp4)'
        self.browser_vdo_file = browse_widget.BrowseFile(title='Sequence Vdo')
        self.browser_vdo_file.set_label('Vdo file : ')
        self.browser_vdo_file.filter = self.fileFilter
        self.ui.verticalLayout.addWidget(self.browser_vdo_file)

        self.wavFileFilter = 'Cut Shot(*.wav)'
        self.browser_wav_file = browse_widget.BrowseFile(title='Wav File')
        self.browser_wav_file.set_label('Wav file : ')
        self.browser_wav_file.filter = self.wavFileFilter
        self.ui.verticalLayout.addWidget(self.browser_wav_file)

    def init_signal(self):
        self.browser_file.textChanged.connect(self.onchange_browse_file)
        self.browser_vdo_file.textChanged.connect(self.onchange_browse_vdo_file)
        self.browser_wav_file.textChanged.connect(self.onchange_browse_wav_file)
        self.ui.sync_btn.clicked.connect(self.sync_update)
        self.ui.uploads_checkbox.stateChanged.connect(self.onchange_uploads_checkbox)
        # self.ui.edit_checkbox.stateChanged.connect(self.onchange_edit_checkbox)

    def onchange_browse_file(self):
        self.loading_state()
        QtCore.QCoreApplication.processEvents()
        self.clear_animatic_table()
        self.load_animatic_xml(self.browser_file.current_text())
        self.animatic_model.set_sg_animatic(self.project, self.episode)
        self.set_animatic_table(self.animatic_model.animatics_mapped)
        self.enable_checkbox()
        self.loaded_state()

    def onchange_uploads_checkbox(self):
        if self.ui.uploads_checkbox.isChecked():
            for row in range(0, self.ui.shots_table.rowCount()):
                row_table = self.ui.shots_table.cellWidget(row, 13)
                row_table.setChecked(True)
                self.animatic_model.toggle_upload_vdo(row_table, row)
        else:
            for row in range(0, self.ui.shots_table.rowCount()):
                row_table = self.ui.shots_table.cellWidget(row, 13)
                row_table.setChecked(False)
                self.animatic_model.toggle_upload_vdo(row_table, row)

    # def onchange_edit_checkbox(self):
    #     self.edit_files_action = True

        # if self.xml_type == "layout":
        #     self.loading_state()
        #     QtCore.QCoreApplication.processEvents()
        #     self.shots_model.set_sg_shots(self.project, self.episode)
        #     self.clear_shot_table()
        #     self.load_xml(self.browser_file.current_text())
        #     self.set_shots_table(self.shots_model.shots)
        #     self.loaded_state()
        # elif self.xml_type == "edit":
        #     self.loading_state()
        #     QtCore.QCoreApplication.processEvents()
        #     self.clear_animatic_table()
        #     self.load_animatic_xml(self.browser_file.current_text())
        #     self.animatic_model.set_sg_animatic(self.project, self.episode)
        #     self.set_animatic_table(self.animatic_model.animatics_mapped)
        #     self.loaded_state()

    def onchange_browse_vdo_file(self):
        self.vdo_path = self.browser_vdo_file.current_text()

    def onchange_browse_wav_file(self):
        self.wav_path = self.browser_wav_file.current_text()

    def enable_checkbox(self):
        self.ui.edit_checkbox.setEnabled(True)
        self.ui.uploads_checkbox.setEnabled(True)
        self.ui.edit_checkbox.setChecked(True)
        self.ui.uploads_checkbox.setChecked(True)

    # def load_xml(self, file_path):
    #     DOMTree = xml.dom.minidom.parse(file_path)
    #     collection = DOMTree.documentElement
    #     clipitems = collection.getElementsByTagName("clipitem")
    #     for clip in clipitems:
    #         clip_name = clip.getElementsByTagName('name')[0]
    #         duration = clip.getElementsByTagName('duration')[0]
    #         cut_in = clip.getElementsByTagName('in')[0]
    #         cut_out = clip.getElementsByTagName('out')[0]
    #         start = clip.getElementsByTagName('start')[0]
    #         end = clip.getElementsByTagName('end')[0]

    #         is_sequence_file = self.shots_model.check_is_sequence_file(clip_name.childNodes[0].data)

    #         if is_sequence_file:
    #             duration = self.shots_model.cal_duration_sequence(
    #                 int(cut_in.childNodes[0].data),
    #                 int(cut_out.childNodes[0].data)
    #             )
    #             cut_in_shot = "1"
    #             cut_out_shot = str(int(cut_out.childNodes[0].data) - int(cut_in.childNodes[0].data))
    #         else:
    #             duration = duration.childNodes[0].data
    #             working_duration =  self.shots_model.cal_duration_sequence(
    #                 int(cut_in.childNodes[0].data),
    #                 int(cut_out.childNodes[0].data)
    #             )
    #             cut_in_shot = str(int(cut_in.childNodes[0].data) + 1)
    #             cut_out_shot = cut_out.childNodes[0].data

    #         data = {
    #             'shot_name': clip_name.childNodes[0].data,
    #             'duration': duration,
    #             'working_duration': working_duration,
    #             'cut_in': cut_in_shot,
    #             'cut_out': cut_out_shot,
    #             'start': start.childNodes[0].data,
    #             'end': end.childNodes[0].data
    #         }
    #         self.shots_model.append_shot_xml(data)
    #     self.shots_model.set_cut_order_shots()

    def load_animatic_xml(self, file_path):
        DOMTree = xml.dom.minidom.parse(file_path)
        collection = DOMTree.documentElement
        tracks = collection.getElementsByTagName("track")
        for track in tracks:
            type = track.getAttribute('MZ.TrackName').lower()
            clipitems = track.getElementsByTagName("clipitem")
            for clip in clipitems:
                clip_name = clip.getElementsByTagName('name')[0]
                cut_in = clip.getElementsByTagName('in')[0]
                cut_out = clip.getElementsByTagName('out')[0]
                start = clip.getElementsByTagName('start')[0]
                end = clip.getElementsByTagName('end')[0]

                # working_duration = str(int(cut_out.childNodes[0].data) - int(cut_in.childNodes[0].data))
                working_duration = str(int(end.childNodes[0].data) - int(start.childNodes[0].data))

                data = {
                    'shot_name': clip_name.childNodes[0].data,
                    'working_duration': working_duration,
                    'cut_in': "1",
                    'cut_out': str(working_duration),
                    'start': start.childNodes[0].data,
                    'end': int(end.childNodes[0].data)
                }
                if type:
                    data['type'] = type
                print(data)
                self.animatic_model.append_animatic(data)
        check_type = self.animatic_model.map_sequence_shots()

        # check Type in trackName
        if check_type == False:
            msg = QtWidgets.QMessageBox()
            msg.setText("Data does not have Type 'Shot' or 'Sequence' in track name.")
            retval = msg.exec_()

        self.animatic_model.set_shot_code()

    # def set_shots_table(self, shots):
    #     self.insert_row_shot_table(shots)
    #     shots = self.shots_model.updated_matched_status(shots)
    #     for row, shot in enumerate(shots):
    #         self.ui.shots_table.setItem(row, 0, QtWidgets.QTableWidgetItem(shot.get('sequence')))
    #         self.ui.shots_table.setItem(row, 1, QtWidgets.QTableWidgetItem(shot.get('shot_name')))
    #         self.ui.shots_table.setItem(row, 2, QtWidgets.QTableWidgetItem(shot.get('cut_order')))
    #         self.ui.shots_table.setItem(row, 3, QtWidgets.QTableWidgetItem(shot.get('cut_in')))
    #         self.ui.shots_table.setItem(row, 4, QtWidgets.QTableWidgetItem(shot.get('cut_out')))
    #         self.ui.shots_table.setItem(row, 5, QtWidgets.QTableWidgetItem(shot.get('working_duration')))
    #         self.ui.shots_table.setItem(row, 6, QtWidgets.QTableWidgetItem(shot.get('duration')))
    #         self.ui.shots_table.setItem(row, 7, QtWidgets.QTableWidgetItem(shot.get('sg_cut_order')))
    #         self.ui.shots_table.setItem(row, 8, QtWidgets.QTableWidgetItem(shot.get('sg_cut_in')))
    #         self.ui.shots_table.setItem(row, 9, QtWidgets.QTableWidgetItem(shot.get('sg_cut_out')))
    #         self.ui.shots_table.setItem(row, 10, QtWidgets.QTableWidgetItem(shot.get('sg_working_duration')))
    #         self.ui.shots_table.setItem(row, 11, QtWidgets.QTableWidgetItem(shot.get('sg_cut_duration')))
    #         self.ui.shots_table.setItem(row, 12, QtWidgets.QTableWidgetItem(shot.get('status')))

    def set_animatic_table(self, shots):
        self.insert_row_shot_table(shots)
        animatics = self.animatic_model.updated_matched_status(shots)
        for row, shot in enumerate(animatics):
            self.ui.shots_table.setItem(row, 0, QtWidgets.QTableWidgetItem(shot.get('sequence')))
            self.ui.shots_table.setItem(row, 1, QtWidgets.QTableWidgetItem(shot.get('shot_name')))
            self.ui.shots_table.setItem(row, 2, QtWidgets.QTableWidgetItem(shot.get('cut_order')))
            self.ui.shots_table.setItem(row, 3, QtWidgets.QTableWidgetItem(shot.get('cut_in')))
            self.ui.shots_table.setItem(row, 4, QtWidgets.QTableWidgetItem(shot.get('cut_out')))
            self.ui.shots_table.setItem(row, 5, QtWidgets.QTableWidgetItem(shot.get('working_duration')))
            self.ui.shots_table.setItem(row, 6, QtWidgets.QTableWidgetItem(shot.get('duration')))
            self.ui.shots_table.setItem(row, 7, QtWidgets.QTableWidgetItem(shot.get('sg_cut_order')))
            self.ui.shots_table.setItem(row, 8, QtWidgets.QTableWidgetItem(shot.get('sg_cut_in')))
            self.ui.shots_table.setItem(row, 9, QtWidgets.QTableWidgetItem(shot.get('sg_cut_out')))
            self.ui.shots_table.setItem(row, 10, QtWidgets.QTableWidgetItem(shot.get('sg_working_duration')))
            self.ui.shots_table.setItem(row, 11, QtWidgets.QTableWidgetItem(shot.get('sg_cut_duration')))
            self.ui.shots_table.setItem(row, 12, QtWidgets.QTableWidgetItem(shot.get('status')))
            check_upload_action = QtWidgets.QCheckBox("upload")
            check_upload_action.setChecked(shot['is_upload'])
            check_upload_action.clicked.connect(partial(
                self.animatic_model.toggle_upload_vdo,
                check_upload_action,
                row
            ))
            self.ui.shots_table.setCellWidget(row, 13, check_upload_action)

    def clear_shot_table(self):
        self.shots_model.clear_shots()
        self.ui.shots_table.clearContents()
        self.ui.shots_table.setRowCount(0)

    def clear_animatic_table(self):
        self.animatic_model.clear_animatic()
        self.ui.shots_table.clearContents()
        self.ui.shots_table.setRowCount(0)

    def insert_row_shot_table(self, shots):
        for i in range(0, len(shots)):
            self.ui.shots_table.insertRow(i)

    def sync_update(self):
        if len(self.animatic_model.animatics) == 0:
            QtWidgets.QMessageBox.critical(self, "Message", "Please upload XML file.")
            return
        try:
            self.loading_state()
            QtCore.QCoreApplication.processEvents()
            src_xml_path = self.browser_file.current_text()
            src_vdo_path = self.browser_vdo_file.current_text()
            src_wav_path = self.browser_wav_file.current_text()
            self.animatic_model.process_xml_animatic()
            self.refresh_animatic_table()
            self.animatic_model.create_tempshot_path()
            if self.vdo_path and self.wav_path:
                if self.ui.edit_checkbox.isChecked():
                    self.animatic_model.process_split_shot_vdo(
                        self.vdo_path,
                        self.wav_path,
                        self.fps,
                        loading_text=self.ui.loading_text
                    )
                self.animatic_model.process_copy_reference_file(
                    src_xml_path,
                    src_vdo_path,
                    src_wav_path
                )
                self.animatic_model.upload_vdo_animatic(self.vdo_path)
                self.animatic_model.remove_tempshots()
            else:
                print("No vdo and wav upload.")
            print("Finish !!")
        except Exception as e:
            QtWidgets.QMessageBox.critical(self, "Message", "Error:" + str(e) + ". Please contact admin.")
            tb = sys.exc_info()[2]
            print(e)
            print("line number : %s", tb.tb_lineno)
        finally:
            self.loaded_state()

    def refresh_animatic_table(self):
        self.ui.shots_table.clearContents()
        self.ui.shots_table.setRowCount(0)
        self.set_animatic_table(self.animatic_model.animatics_mapped)

    def updating_cut_shot(self):
        for row in xrange(self.ui.shots_table.rowCount()):
            self.ui.shots_table.setItem(row, 5, QtWidgets.QTableWidgetItem("Updating..."))
            self.ui.shots_table.setItem(row, 6, QtWidgets.QTableWidgetItem("Updating..."))
            self.ui.shots_table.setItem(row, 7, QtWidgets.QTableWidgetItem("Updating..."))
            self.ui.shots_table.setItem(row, 8, QtWidgets.QTableWidgetItem("Updating..."))
            self.ui.shots_table.setItem(row, 10, QtWidgets.QTableWidgetItem("Updating..."))
            self.ui.shots_table.setItem(row, 11, QtWidgets.QTableWidgetItem("Updating..."))

    def error_sync_shot(self, msg):
        self.ui.msg_sync.setVisible(True)
        self.ui.msg_sync.setStyleSheet('color: Red')
        self.ui.msg_sync.setText(msg)

    def loading_state(self):
        self.ui.loading_text.setVisible(True)

    def loaded_state(self):
        self.ui.loading_text.setVisible(False)


if __name__ == '__main__':
    app = QtWidgets.QApplication.instance()
    if not app:
        app = QtWidgets.QApplication(sys.argv)
        stylesheet.set_default(app)
    myApp = ShotXmlViewer(sys.argv)
    # stylesheet.set_default(app)
    sys.exit(app.exec_())
