import os
import sys

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
from rf_utils import icon

import pymel.core as pm

class ReplaceObjectUI(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(ReplaceObjectUI, self).__init__(parent)
        self.mainLayout = QtWidgets.QVBoxLayout()
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.add_asset_pushButton = QtWidgets.QPushButton("Add Asset Base")
        self.verticalLayout.addWidget(self.add_asset_pushButton)
        self.asset_base_listWidget = QtWidgets.QListWidget()
        ####################
        self.asset_base_listWidget.currentItemChanged.connect(self.select_base_item)
        ####################
        self.verticalLayout.addWidget(self.asset_base_listWidget)
        self.horizontalLayout.addLayout(self.verticalLayout)
        self.direct_pushButton = QtWidgets.QPushButton(">>")
        self.horizontalLayout.addWidget(self.direct_pushButton)
        self.verticalLayout_3 = QtWidgets.QVBoxLayout()
        self.add_group_pushButton = QtWidgets.QPushButton("Add group target")
        self.verticalLayout_3.addWidget(self.add_group_pushButton)
        self.group_target_listWidget = QtWidgets.QListWidget()
        self.verticalLayout_3.addWidget(self.group_target_listWidget)
        self.replace_pushButton = QtWidgets.QPushButton("Replace Object")
        self.verticalLayout_3.addWidget(self.replace_pushButton)
        self.horizontalLayout.addLayout(self.verticalLayout_3)
        self.mainLayout.addLayout(self.horizontalLayout)
        self.setLayout(self.mainLayout)


    def select_base_item(self):
        widget_item = self.asset_base_listWidget.currentItem()
        pm.select(widget_item.text())