_title = 'Riff ReplaceObject'
_version = 'v.0.0.1'
_des = 'beta'
uiName = 'RFReplaceObject'

#Import python modules
import sys
import os
import getpass
import json
import logging

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)

# import config
import rf_config as config
from rf_utils import log_utils
# sg

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

import ui

import maya.cmds as mc
import pymel.core as pm

class RFReplaceObject(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        #SetUp Window
        super(RFReplaceObject, self).__init__(parent)
        #ui read
        self.ui = ui.ReplaceObjectUI(parent)
        self.setObjectName(uiName)
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.setCentralWidget(self.ui)
        self.init_state()

        # self.setFixedSize(50,195)
        # self.set_logo()

    def init_state(self):
        self.ui.add_asset_pushButton.clicked.connect(self.add_item_asset)
        self.ui.add_group_pushButton.clicked.connect(self.add_target_group)
        self.ui.replace_pushButton.clicked.connect(self.replace_object)

    def add_item_asset(self):
        list_select = pm.ls(sl=True)
        for select in list_select:
            widget = QtWidgets.QListWidgetItem(select.name())
            self.ui.asset_base_listWidget.addItem(widget)

    def add_target_group(self):
        list_select = pm.ls(sl=True)
        for select in list_select:
            widget = QtWidgets.QListWidgetItem(select.name())
            print select.name()
            self.ui.group_target_listWidget.addItem(widget)

    def replace_object(self):
        base_widget_item = self.ui.asset_base_listWidget.currentItem()
        target_widget_item = self.ui.group_target_listWidget.currentItem()
        target_grp = pm.PyNode(target_widget_item.text())
        list_target = target_grp.getChildren()

        asset_base = pm.PyNode(base_widget_item.text())
        if list_target:
            new_grp = pm.group(n='%s_grp'%asset_base.name(), em=True)
            for target in list_target:
                
                duplicate_obj = pm.duplicate(asset_base)
                pm.parent(duplicate_obj, new_grp)
                self.snapToObj(target, duplicate_obj[0])
                old_rotateX = duplicate_obj[0].rx.get()
                duplicate_obj[0].rx.set(old_rotateX+90)

    def snapToObj(self, target, objToSnap):

        self.unlock_attr(objToSnap)
        tempPointConstraint = pm.parentConstraint(target, objToSnap, maintainOffset = False)
        pm.delete(tempPointConstraint)
 

    def unlock_attr(self, obj):
        axis = ['X', 'Y', 'Z']
        attrs = ['translate', 'rotate', 'scale']
        for ax in axis:
            for attr in attrs:
                mc.setAttr(obj+'.'+attr+ax, lock=0)


def show():
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        maya_win.deleteUI(uiName)
        myApp = RFReplaceObject(maya_win.getMayaWindow())
        myApp.show()
        return myApp
if __name__ == '__main__':
    show()