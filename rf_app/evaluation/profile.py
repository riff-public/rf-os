# -*- coding: utf-8 -*-
import sys
import os
import getpass
import time


core_path = '%s/core' % os.environ.get('RFSCRIPT')
if core_path not in sys.path:
    sys.path.append(core_path)

import rf_config as config

from models.profile_model import ProfileModel

from rf_utils.ui import load, stylesheet
from rf_utils import log_utils
os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide2', 'PySide'])
from Qt import QtWidgets, QtCore, QtGui
from rf_utils import icon

userLog = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
UI_NAME = "Evaluation"
logFile = log_utils.name(UI_NAME, userLog)
logger = log_utils.init_logger(logFile)

UI_PATH = "{core}/rf_app/evaluation/views/profile.ui".format(core=core_path)
SHOTGUN_URL = "https://riffanimation.shotgunstudio.com"


class ProfileCtrl(QtWidgets.QMainWindow):
    def __init__(self, parent=None, data=None):
        super(ProfileCtrl, self).__init__(parent)
        if config.isMaya:
            self.ui = load.setup_ui_maya(UI_PATH, parent)
        else:
            self.ui = load.setup_ui(UI_PATH, self)

        self.init_ui()
        self.init_state_app(data)
        self.init_signal()
        self.set_ui()
        self.ui.show()

    def init_ui(self):
        app_icon = QtGui.QIcon()
        app_icon.addFile(icon.evaluation_icon, QtCore.QSize(16, 16))
        self.ui.setWindowTitle("Profile")
        self.ui.setWindowIcon(app_icon)
        self.ui.picture_label.setPixmap(QtGui.QPixmap(icon.evaluation_icon))

    def init_signal(self):
        pass

    def init_state_app(self, data):
        self.data = data
        self.profile_model = ProfileModel()
        self.user_tasks = self.profile_model.get_tasks_user(data)

    def set_ui(self):
        self.ui.name_label.setText(self.data['name'])
        self.set_tasks_list()

    def set_tasks_list(self):
        if len(self.user_tasks) == 0:
            item = QtWidgets.QListWidgetItem(self.ui.tasks_list)
            item.setText("Not Found")
        else:
            for task in self.user_tasks:
                if task['entity'] is not None:
                    item = QtWidgets.QListWidgetItem(self.ui.tasks_list)
                    iconWidget = QtGui.QIcon()
                    icon_path = self.find_icon_path(task['sg_status_list'])
                    iconWidget.addPixmap(QtGui.QPixmap(icon_path), QtGui.QIcon.Normal,QtGui.QIcon.Off)
                    item.setIcon(iconWidget)
                    item.setText(task['entity'].get('name'))

    def find_icon_path(self, status_code):
        for item in icon.ICONS_LIST:
            if item['code'] == status_code:
                return item['icon_path']
