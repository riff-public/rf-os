from rf_utils.sg.sg_utils import (
    sg
)
from rf_utils.pipeline import user_pref


class ProfileModel:
    def __init__(self):
        self.sg = sg

    def get_tasks_user(self, data):
        task_assignees = []
        user_filtered = {
            "type": data['type'],
            "id": data['id'],
            "name": data['name']
        }
        task_assignees.append(user_filtered)

        tasks_filter = [
            ['task_assignees', 'in', task_assignees]
        ]
        fields = [
            'content',
            'sg_status_list',
            'entity',
            'entity.Asset.sg_asset_type',
            'entity.Shot.sg_sequence_code',
            'entity.Asset.sg_mainasset',
            'entity.Shot.sg_episode.Scene.code',
            'entity.Asset.code',
            'due_date'
        ]
        order_name = [{'field_name': 'created_at', 'direction': 'desc'}]
        tasks = self.sg.find('Task', tasks_filter, fields, order_name)
        print("tasks length: %s", len(tasks))
        return tasks
