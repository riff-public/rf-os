import os

from rf_utils.context import context_info
from rf_utils import project_info
from rf_utils.sg.sg_utils import (
    sg,
    get_all_users,
    get_all_departments
)
from rf_utils.pipeline import user_pref


class EvaluationModel:
    def __init__(self):
        self.sg = sg

    def get_people(self):
        users = get_all_users()
        return users

    def get_departments(self):
        departments = get_all_departments()
        return departments 
