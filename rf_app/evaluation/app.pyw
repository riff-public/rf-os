# -*- coding: utf-8 -*-
import sys
import os
import getpass
import time
from profile import ProfileCtrl
from functools import partial

core_path = '%s/core' % os.environ.get('RFSCRIPT')
if core_path not in sys.path:
    sys.path.append(core_path)

import rf_config as config

from models.evaluation_model import EvaluationModel

from rf_utils.ui import load, stylesheet
from rf_utils import log_utils
os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide2', 'PySide'])
from Qt import QtWidgets, QtCore, QtGui
from rf_utils import icon

userLog = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
UI_NAME = "Evaluation"
logFile = log_utils.name(UI_NAME, userLog)
logger = log_utils.init_logger(logFile)

UI_PATH = "{core}/rf_app/evaluation/views/evaluation.ui".format(core=core_path)
SHOTGUN_URL = "https://riffanimation.shotgunstudio.com"


class EvaluationCtrl(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super(EvaluationCtrl, self).__init__(parent)
        if config.isMaya:
            self.ui = load.setup_ui_maya(UI_PATH, parent)
        else:
            self.ui = load.setup_ui(UI_PATH, self)

        self.init_ui()
        self.init_state_app()
        # self.init_signal()
        self.ui.show()

    def init_ui(self):
        app_icon = QtGui.QIcon()
        app_icon.addFile(icon.evaluation_icon, QtCore.QSize(16, 16))
        self.ui.setWindowTitle("Evaluation App")
        self.ui.setWindowIcon(app_icon)

    # def init_signal(self):
    #     users_widget.itemDoubleClicked.connect(self.users_double_clicked)

    def init_state_app(self):
        self.evaluation_model = EvaluationModel()
        self.evaluation_model.get_people()
        self.init_list_users()

    def init_list_users(self):
        users = self.evaluation_model.get_people()
        departments = self.evaluation_model.get_departments()
        # print(departments)

        for department in departments:
            department_label = QtWidgets.QLabel()
            self.ui.verticalLayout.addWidget(department_label)
            department_label.setText(department['name'])
            users_widget = QtWidgets.QListWidget()
            self.ui.verticalLayout.addWidget(users_widget)
            users_widget.setFlow(QtWidgets.QListView.LeftToRight)
            users_widget.setResizeMode(QtWidgets.QListView.Adjust)
            users_widget.setGridSize(QtCore.QSize(64, 64))
            users_widget.setSpacing(32)
            users_widget.setViewMode(QtWidgets.QListView.IconMode)
            users_widget.setMinimumSize(QtCore.QSize(0, 300))

            for user in users:
                if user['department'] is not None:
                    if user['department']['name'] == department['name']:
                        item = QtWidgets.QListWidgetItem(users_widget)
                        iconWidget = QtGui.QIcon()
                        iconWidget.addPixmap(QtGui.QPixmap(icon.user_eval), QtGui.QIcon.Normal,QtGui.QIcon.Off)
                        item.setIcon(iconWidget)

                        item.setText(user['name'])
                        item.setData(QtCore.Qt.UserRole, user)
                        users_widget.setIconSize(QtCore.QSize(32, 32))

            users_widget.itemDoubleClicked.connect(lambda user=user: self.users_double_clicked(user))

    def users_double_clicked(self, user):
        data = user.data(QtCore.Qt.UserRole)
        self.profile_window = ProfileCtrl(self, data)


def show():
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        maya_win.deleteUI(UI_NAME)
        myApp = EvaluationCtrl(maya_win.getMayaWindow())
        return myApp
    else:
        logger.info('Run in Evaluation\n')
        app = QtWidgets.QApplication.instance()
        if not app:
            app = QtWidgets.QApplication(sys.argv)
        myApp = EvaluationCtrl()
        stylesheet.set_default(app)
        sys.exit(app.exec_())


if __name__ == '__main__':
    show()
