import os
import sys
import subprocess
import _subprocess
import logging
import re
# logger = logging.getLogger(__name__)
# logger.addHandler(logging.NullHandler())
moduleDir = os.path.dirname(sys.modules[__name__].__file__)

from rf_utils import log_utils
logFile = log_utils.name('testsubprocess', user='TA')
logger = log_utils.init_logger(logFile)
from rf_utils import file_utils

from rf_maya.rftool.utils import yeti_lib

class LocalConfig:
    getAsset = '%s/get_asset.py' % moduleDir
    placeholder = '%s/scene.ma' % moduleDir

# def get_asset(version, filePath):
#     run_maya_batch(version, LocalConfig.getAsset, filePath)
#     tmpFile = 'get_asset_tmp.yml'
#     tmpdir = os.environ.get('TMPDIR')
#     tmpPath = '%s/%s' % (tmpdir, tmpFile)
#     data = file_utils.ymlLoader(tmpPath)
#     return data


def run_maya_batch(version, pyCommand, mayaFile) :
    MAYA_PYTHON = 'C:\\Program Files\\Autodesk\\Maya%s\\bin\\mayapy.exe' % version
    # mayaCmdFlPath = 'O:/studioTools/maya/python/tool/myStandAlone/mayapyCmd.py'
    # filePath = 'O:/studioTools/maya/python/tool/myStandAlone/maya.ma'
    # startupinfo = subprocess.STARTUPINFO()
    # startupinfo.dwFlags |= _subprocess.STARTF_USESHOWWINDOW
    p = subprocess.Popen([MAYA_PYTHON, pyCommand, mayaFile])
    # p = subprocess.call([MAYA_PYTHON, pyCommand, mayaFile], stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    stdout, stderr = p.communicate()
    # returnCode = p.returncode

    logger.info(stdout)
    logger.info(stderr)

    return p


def list_file(rootPath, nameFilter=str(), pathFilter=str(), extFilter=str(), fileIndex=str(), pathInvFilter=str()):
    pathMatch = True
    pathMatchRmKey = True
    nameMatch = True
    extMatch = True
    filterFiles = []

    for root, dirs, files in os.walk(rootPath):
        if files:
            normRoot = root.replace('\\', '/')
            if pathFilter:
                pathMatch = True if pathFilter in normRoot else False
            if pathInvFilter:
                pathMatchRmKey = True if pathInvFilter not in normRoot else False

            if all([pathMatch, pathMatchRmKey]):
                matchExt = [a for a in files if os.path.splitext(a)[-1] in extFilter] if extFilter else files
                matchName = [a for a in matchExt if nameFilter in a] if nameFilter else matchExt
                matchFiles = ['%s/%s' % (normRoot, a) for a in matchName]
                matchIndex = [sorted(matchFiles)[int(fileIndex)]] if fileIndex and len(matchFiles) else matchFiles
                filterFiles+=matchIndex

    return filterFiles


def get_asset(mayaFile, batch=True, mode='all', version='2017') :
    if batch :
        print 'this is batch mode'
        # pyCommand = 'D:/TA/Dropbox/script_server/core/rf_app/sceneCollector/get_asset.py'
        pyCommand = LocalConfig.getAsset
        runMayaBatch(pyCommand, mayaFile, version, mode)

        tmpFile = 'mayaPyDictTmp.txt'
        tmpdir = os.environ.get('TEMP')
        tmpPath = '%s/%s' % (tmpdir, tmpFile)

        if os.path.exists(tmpPath) :
            f = open(tmpPath, 'r')
            data = f.read()
            f.close()

            result = eval(data)
            os.remove(tmpPath)
            return result
        else:
            print 'file does not exists'
            return dict()

    else :
        try:
            import maya.cmds as mc
            mc.file(mayaFile, force=True , open=True, prompt=False, loadReferenceDepth=mode)
        except Exception as e:
            logger.error(e)

        files = mc.file(query=1, list=1, withoutCopyNumber=1)
        files = get_additional_files(files)
        # files += get_abc_from_gpu()
        
        tmpFile = 'get_asset_tmp.yml'
        tmpdir = os.environ.get('TEMP')
        tmpPath = '%s/%s' % (tmpdir, tmpFile)

        # data = {'files': files, 'assemblyFiles': assemblyFiles}
        data = {'files': files}
        file_utils.ymlDumper(tmpPath, data)

        return data

# def get_abc_from_gpu():
#     from rf_utils.pipeline.misc import etc
#     abc = etc.get_gpu_asset()
#     return abc

def get_additional_files(inputFiles):
    # red shifts
    import maya.cmds as mc
    import maya.app.general.fileTexturePathResolver as ftpr

    pluginNodeConfig = {'RedshiftNormalMap': 'tex0'}
    additionalFiles = []
    for nodeType, attr in pluginNodeConfig.iteritems():
        pluginNodes = mc.ls(type=nodeType)

        if pluginNodes:
            for pluginNode in pluginNodes:
                filePath = mc.getAttr('%s.%s' % (pluginNode, attr))
                if filePath:
                    pattern = ftpr.getFilePatternString( filePath , False , 2 )
                    allFiles = ftpr.findAllFilesForPattern( pattern , None )

                    for file in allFiles:
                        additionalFiles.append(file) if not file in additionalFiles else None
    # get yetiGroom texture
    texGroom = yeti_lib.find_all_textures()
    if texGroom:
        for key , value in texGroom.iteritems():
            patternGroom = ftpr.getFilePatternString( key , False , 2 )
            allFilesGroom = ftpr.findAllFilesForPattern( patternGroom , None )

            for fileGroom in allFilesGroom:
                additionalFiles.append(fileGroom) if not fileGroom in additionalFiles else None
    # get texture .ftn
    allFileNodes = mc.ls(et="file")
    if allFileNodes:
        for eachFile in allFileNodes:
            currentFile = mc.getAttr("%s.fileTextureName" % eachFile)
            if currentFile:
                pattern = ftpr.getFilePatternString( currentFile , False , 2 )
                allFiles = ftpr.findAllFilesForPattern( pattern , None )
                for file in allFiles:
                    if not file in inputFiles:
                        additionalFiles.append(file) if not file in additionalFiles else None

    # get gpu 
    gpuFileNodes = mc.ls(type='gpuCache')
    if gpuFileNodes: 
        for node in gpuFileNodes: 
            file = mc.getAttr('%s.cacheFileName' % node)
            if not file in inputFiles:
                additionalFiles.append(file) if not file in additionalFiles else None


    return inputFiles + additionalFiles

def runMayaBatch(pyCommand, mayaFile, version = '', mode='all') :
    if not version :
        version = mc.about(v = True)

    if '2012' in version :
        MAYA_PYTHON = 'C:\\Program Files\\Autodesk\\Maya2012\\bin\\mayapy.exe'

    if '2015' in version :
        MAYA_PYTHON = 'C:\\Program Files\\Autodesk\\Maya2015\\bin\\mayapy.exe'

    if '2016' in version :
        MAYA_PYTHON = 'C:\\Program Files\\Autodesk\\Maya2016\\bin\\mayapy.exe'

    if '2017' in version :
        MAYA_PYTHON = 'C:\\Program Files\\Autodesk\\Maya2017\\bin\\mayapy.exe'

    # mayaCmdFlPath = 'O:/studioTools/maya/python/tool/myStandAlone/mayapyCmd.py'
    # filePath = 'O:/studioTools/maya/python/tool/myStandAlone/maya.ma'
    subprocess.call([MAYA_PYTHON, pyCommand, mayaFile, mode])


def get_drive_regx(drives):
    strDrive = "("
    for drive in drives:
        strDrive = strDrive + drive + ":|"
    strDrive = strDrive[:-1] + ")"
    return strDrive


def check_duplicate_name(data_path, path):
    if path in data_path:
        return True
    return False


def get_data_path(input_file):
    drive = ["P", "O", "Y", "N", "C", "D"]
    dirve_search = get_drive_regx(drive)
    regx_find_path = "\\b{dirve_search}.*\.[\w:]+".format(dirve_search=dirve_search)
    data_path = []

    with open(input_file, "r") as file_:
        for line in file_:
            if line.startswith("applyMetadata"):
                continue
            result = re.search(regx_find_path, line)
            if result is not None:
                path = result.group()
                isDuplicate = check_duplicate_name(data_path, path)
                if isDuplicate:
                    continue
                data_path.append(path)
    return data_path
