# System modules
import sys
import os
import re
import shutil
import logging

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
moduleDir = os.path.dirname(sys.modules[__name__].__file__)

core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)

import rf_config as config
from rf_utils import file_utils

def main() :
    # Maya modules
    import pymel.core as pm
    import maya.cmds as mc
    import maya.mel as mm

    mayaFile = sys.argv[1]
    mode = sys.argv[2]
    # maya.standalone.initialize( name='python' )
    try:
        mc.file(mayaFile, force=True , open=True, prompt=False, loadReferenceDepth = mode)
    except Exception as e:
        logger.error(e)
    # mc.file(mayaFile,  i = True, options = 'v=0', pr = True, loadReferenceDepth = 'all')
    files = mc.file(query=1, list=1, withoutCopyNumber=1)
    files = get_additional_files(files)
    # assemblyFiles = getAssemblyFiles()
    assemblyFiles = []
    # textureFiles = mayaTools.getTextureFile()
    tmpFile = 'mayaPyDictTmp.txt'
    tmpdir = os.environ.get('TEMP')
    tmpPath = '%s/%s' % (tmpdir, tmpFile)


    data = {'files': files, 'assemblyFiles': assemblyFiles}

    f = open(tmpPath, 'w')
    f.write(str(data))
    f.close()


def get_additional_files(inputFiles):
    # red shifts
    import maya.cmds as mc
    import maya.app.general.fileTexturePathResolver as ftpr

    pluginNodeConfig = {'RedshiftNormalMap': 'tex0'}
    additionalFiles = []
    for nodeType, attr in pluginNodeConfig.iteritems():
        pluginNodes = mc.ls(type=nodeType)

        if pluginNodes:
            for pluginNode in pluginNodes:
                filePath = mc.getAttr('%s.%s' % (pluginNode, attr))
                pattern = ftpr.getFilePatternString( filePath , False , 2 )
                allFiles = ftpr.findAllFilesForPattern( pattern , None )

                for file in allFiles:
                    additionalFiles.append(file) if not file in additionalFiles else None

    return inputFiles + additionalFiles


def get_asset(mayaFile, mode='all'):
    try:
        mc.file(mayaFile, force=True , open=True, prompt=False, loadReferenceDepth=mode)
    except Exception as e:
        logger.error(e)

    files = mc.file(query=1, list=1, withoutCopyNumber=1)
    tmpFile = 'get_asset_tmp.yml'
    tmpdir = mm.eval("getenv TMPDIR")
    tmpPath = '%s/%s' % (tmpdir, tmpFile)

    # data = {'files': files, 'assemblyFiles': assemblyFiles}
    data = {'files': files}
    file_utils.ymlDumper(tmpPath, data)

    return data

def getAssemblyFiles():
    return []
    sels = mc.ls(type = 'assemblyReference')
    copyFiles = []

    if sels :
        for each in sels :
            ad = mc.getAttr('%s.definition' % each)

            if ad :
                fileStatus = False

                if os.path.exists(ad) :
                    fileStatus = True

                copyFiles.append((ad, fileStatus))

            datas = listRepIndex(each, 'data')

            if datas :
                for each in datas :
                    fileStatus = False
                    if os.path.exists(each) :
                        fileStatus = True

                    copyFiles.append((each, fileStatus))

    return copyFiles


def listRepIndex(assemblyNode, listType = 'name') :
    lists = mc.assembly(assemblyNode, q = True, listRepresentations = True)

    if listType == 'name' :
        return lists

    labels = []
    datas = []

    if lists :
        for i in range(len(lists)) :
            label = mc.getAttr('%s.representations[%s].repLabel' % (assemblyNode, i))
            data = mc.getAttr('%s.representations[%s].repData' % (assemblyNode, i))
            labels.append(label)
            datas.append(data)

    if listType == 'label' :
        return labels

    if listType == 'data' :
        return datas


if __name__ == '__main__':
    main()