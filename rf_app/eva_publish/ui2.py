# -*- coding: utf-8 -*-
import os
import sys

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
from rf_utils import icon
from functools import partial

class SendToFTPUI(QtWidgets.QWidget):
    # sendData = QtCore.Signal(list)
    def __init__(self, parent=None) :
        super(SendToFTPUI, self).__init__(parent)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.addItem(spacerItem)
        self.logo_layout = QtWidgets.QVBoxLayout()
        self.horizontalLayout_3.addLayout(self.logo_layout)
        self.main_layout = QtWidgets.QVBoxLayout()
        self.main_layout.addLayout(self.horizontalLayout_3)
        self.head_label = QtWidgets.QLabel("EvaOutSource SendToFTP")
        font = QtGui.QFont()
        font.setFamily("MingLiU_HKSCS-ExtB")
        font.setPointSize(12)
        self.head_label.setFont(font)
        self.head_label.setObjectName("head_label")
        self.main_layout.addWidget(self.head_label)
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setSpacing(6)
        self.lineEdit = QtWidgets.QLineEdit()
        self.lineEdit.setReadOnly(True)
        self.horizontalLayout_2.addWidget(self.lineEdit)
        self.plus_pushButton = QtWidgets.QPushButton('+')
        self.plus_pushButton.setMinimumSize(QtCore.QSize(0, 0))
        self.plus_pushButton.setMaximumSize(QtCore.QSize(30, 50))
        self.horizontalLayout_2.addWidget(self.plus_pushButton)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem)
        self.horizontalLayout_2.setStretch(0, 4)
        self.horizontalLayout_2.setStretch(1, 2)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.verticalLayout_3 = QtWidgets.QVBoxLayout()
        self.file_listWidget = QtWidgets.QListWidget()
        self.verticalLayout_3.addWidget(self.file_listWidget)
        self.STF_pushButton = QtWidgets.QPushButton('Send to FTP')
        self.verticalLayout_3.addWidget(self.STF_pushButton)
        self.verticalLayout_2.addLayout(self.main_layout)
        self.verticalLayout.addLayout(self.verticalLayout_3)
        self.verticalLayout_2.addLayout(self.verticalLayout)

        self.setLayout(self.verticalLayout_2)