# v.0.0.1 polytag switcher
_title = 'Riff Eva Publish'
_version = 'v.0.0.1'
_des = 'wip'
uiName = 'RFEvaPublishUI'
uiName_2 = 'SendToFTPUI'

#Import python modules
import sys
import os , fnmatch
import getpass
import json 
from shutil import copy2

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
root = os.path.split(moduleDir)[0]

core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path: 
    sys.path.append(core)

# import config
import rf_config as config
from rf_utils.ui import load
from rf_utils import log_utils
from rf_utils import project_info
from rf_utils.context import context_info
from rf_utils.context import scene_description
from rf_utils.pipeline import user_pref, commands
reload(scene_description)

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

import ui
import ui2
from rf_utils import file_utils

project_path = "P:/Eva/scene/work"

class RFEvaPublish(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super(RFEvaPublish, self).__init__(parent)
        self.ui = ui.RFEvaPublishUI(parent)
        self.setFixedSize(580, 750)
        self.ui.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)
        self.list_all_shot()
        self.init_state()
        # self.ui.shot_tableWidget.setColumnWidth(3,20)
        # self.ui.show()

    def init_state(self):
        self.ui.achive_pushButton.clicked.connect(self.achive_file)
        self.ui.rv_playlist_pushButton.clicked.connect(self.process_play_rv)
        

    def list_all_shot(self):
        for episode in os.listdir(project_path):
            self.ui.add_episode_row(episode)
            episode_path = os.path.join(project_path, episode)
            for shot in os.listdir(episode_path):
                shot_path = os.path.join(episode_path, shot).replace('\\', '/')
                shot_entity = scene_description.SceneDescription(path = shot_path)
                shot_data = ''
                shot_name = shot_entity._scene.name
                if shot_entity.data:
                    shot_data = shot_entity.data['k_version']
                self.ui.add_shot_row(shot_name, shot_entity)

    def list_mp4_file(self):
        count_select_row = self.get_selceted_row()
        list_path = []
        for row in count_select_row:
            mp4_file = self.get_mp4_file(row_input=row)
            if mp4_file:
                list_path.append(mp4_file)
        return list_path
        # show_edit_ui(list_path)

    def get_mp4_file(self, row_input = ''):
        row = self.ui.shot_tableWidget.currentRow()
        if row_input:
            row = row_input
        if self.ui.shot_tableWidget.cellWidget(row, 2) and self.ui.shot_tableWidget.cellWidget(row, 4):
            step = self.ui.shot_tableWidget.cellWidget(row, 2).currentText()
            path_shot = self.make_path_output(row, step)
            mp4_file = self.ui.shot_tableWidget.cellWidget(row, 4).currentText()
            # mp4_file = self.get_lastest_file(path_shot)
            if mp4_file:
                return os.path.join(path_shot, mp4_file)
                

    def process_play_rv(self):
        # RVPlayer().play(files_selected) # play in local
        files_selected = self.list_mp4_file()
        if files_selected:
            for file in os.listdir("C:/Program Files/Shotgun"):
                if "RV-" in file:
                    rv_version = file
            command = ["C:/Program Files/Shotgun/{rv_version}/bin/rv.exe".format(rv_version=rv_version)]

            for file_item in files_selected:
                print file_item
                if os.path.exists(file_item):
                    command.append(file_item)
            thread_subprocess = commands.ThreadSubprocess(command)
            thread_subprocess.start()

    def achive_file(self):
        list_path = self.list_mp4_file()
        show_edit_ui(list_path)

    def get_selceted_row(self):
        all_row = range(0, self.ui.shot_tableWidget.rowCount())
        count_list = []
        for select_row in all_row:
            item  = self.ui.shot_tableWidget.item(select_row, 0)
            currentState = item.checkState()
            if currentState == QtCore.Qt.Checked:
                count_list.append(select_row)
        return count_list
                

    def get_lastest_file(self, folder_path, ext='mp4'):

        return file_utils.get_lastest_file(folder_path, ext)


    def make_path_output(self, select_row, step=''):
        shot_entity = self.ui.shot_tableWidget.item(select_row, 1).data(QtCore.Qt.UserRole)
        episode = shot_entity._scene.episode
        shot_entity._scene.context.update(process='main', step =step)
        print shot_entity._scene.path.output()
        shot_name = self.ui.shot_tableWidget.item(select_row, 1).text()
        if self.ui.shot_tableWidget.cellWidget(select_row, 2):
            depart = self.ui.shot_tableWidget.cellWidget(select_row, 2).currentText()
            k_version = self.ui.shot_tableWidget.item(select_row, 3).text()
        else:
            return

        return os.path.join(project_path, '%s/%s/%s/main/output'%(episode, shot_name, depart)).replace('\\', '/')

FTP_path = 'P:/Eva/send_weekly/FTP'
# FTP_path = 'D:/eva_test'
class SendToFTP(QtWidgets.QMainWindow):
    def __init__(self, parent=None, *args):
        super(SendToFTP, self).__init__(parent)
        self.list_path_FTP = args[0]
        self.ui = ui2.SendToFTPUI(parent)
        self.setFixedSize(495, 600)
        self.ui.setWindowTitle('SendToFTPUI')
        self.setCentralWidget(self.ui)
        self.setObjectName(uiName_2)
        self.display_path()
        self.ftp_path()
        self.init_state()

    def init_state(self):
        self.ui.STF_pushButton.clicked.connect(self.send_to_ftp)
        self.ui.plus_pushButton.clicked.connect(self.update_ftp_path)
        

    def ftp_path(self):
        dayTime = get_day_time()
        dayTime_latest_version = get_version_ftp(FTP_path, dayTime)
        day_ftp_path = os.path.join(FTP_path, dayTime_latest_version)
        self.check_ftp_version(day_ftp_path)
        self.ui.lineEdit.setText(day_ftp_path.replace('\\','/'))
        self.ui.lineEdit.setPlaceholderText(day_ftp_path)


    def display_path (self):
        import re
        for path in self.list_path_FTP:
            if path:
                shot_entity = scene_description.SceneDescription(path = path)
                file = os.path.basename(path)
                version = re.findall ( 'v' + '[0-9]{3}' , file )
                print version
                if version:
                    shot_entity._scene.context.update(version = version[0])
                k_version = shot_entity.k_version(shot_entity._scene.step)
                filename = shot_entity.est_file_name(k_version = int(k_version)+1)
                item = QtWidgets.QListWidgetItem(filename)
                item.setData(QtCore.Qt.UserRole,[path, filename])
                self.ui.file_listWidget.addItem(item)

    def update_ftp_path(self):
        current_path = self.ui.lineEdit.text()
        if current_path:
            folder = os.path.basename(current_path)
            folder_version = folder.split('_')[1]
            new_version = int(folder_version) + 1
            date = folder.split('_')[0] 
            ftp_path = os.path.dirname(current_path)
            day_ftp_path = os.path.join(ftp_path, "{date}_{ver}".format(date= date, ver ="%02d"%new_version)).replace('\\','/')
            self.ui.lineEdit.setText(day_ftp_path)
            self.ui.lineEdit.setPlaceholderText(day_ftp_path)
            # os.mkdir(day_ftp_path)
            return day_ftp_path
            

    def send_to_ftp(self):
        list_file = []
        
        for index in xrange(self.ui.file_listWidget.count()):
            shot_widget = self.ui.file_listWidget.item(index)
            mp4_path, filename = shot_widget.data(QtCore.Qt.UserRole)
            weekly_path = self.ui.lineEdit.text()
            self.copy_to_local(mp4_path, filename)
            list_file.append(filename)
        try:
            self.copy_to_client(weekly_path, list_file)
            complete_dialog()
        except:
            error_dialog()



            
            # mp4_weekly_path = os.path.join(weekly_path, filename)
            #P:/Eva/scene/work/ev04/ev_ev04_q0010_s0010/layout/main/output\ev_ev04_q0010_s0010_layout_main.v001.mp4 
            #ev04_ps001_p001_3dLo_k01_v001.mp4
            #file_utils.copy(mp4_path, )

            # self.copy_to_local()

    def copy_to_local(self, source='', filename=''):
        if source and filename:
            weekly_path = self.ui.lineEdit.text().replace('\\', '/')
            if not os.path.exists(source):
                os.mkdir(weekly_path)
            send_weekly_path = os.path.join(weekly_path, filename).replace('\\', '/')
            file_utils.copy(source, send_weekly_path)

    def copy_to_client(self, weekly_path='', list_file=[]):
        client_path = 'Z:/Eva/toKhara/3dCheck/shots/3dAnim'
        _date = os.path.basename(weekly_path)
        print weekly_path.replace('\\','/'), os.path.join(client_path, _date).replace('\\','/')
        file_utils.xcopy_directory(weekly_path.replace('\\','/'), os.path.join(client_path, _date).replace('\\','/'))

    def check_ftp_version(self, path):
        path = path.replace('\\', '/')
        return path

def get_version_ftp(path_folder, folder_name):
    list_file = os.listdir(path_folder)
    list_file_match_name = [f for f in list_file if folder_name in f and '_'in f]
    if list_file_match_name:
        return max(list_file_match_name)
    else:
        return "{}_01".format(folder_name)

def complete_dialog():
    from rf_utils.widget import dialog
    dialog.MessageBox.success(title ='Send FTP', message = 'Publish Complete')

def error_dialog():
    from rf_utils.widget import dialog
    dialog.MessageBox.error(title ='Send FTP', message = 'Error Copy to Z:')



def get_day_time():
    from datetime import datetime
    now = datetime.now() # current date and time
    year = now.strftime("%Y")[-2:]
    month = now.strftime("%m")
    day = now.strftime("%d")
    dayTime = "{year}{month}{day}".format(year=year, month=month, day=day)
    return dayTime


def show_edit_ui(list_path_FTP): 
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        maya_win.deleteUI(uiName_2)
        myApp = SendToFTP(maya_win.getMayaWindow(), list_path_FTP)
        myApp.show()
        return myApp
    else:
        logger.info('Run in SendToFTP\n')
        app = QtWidgets.QApplication.instance()
        if not app:
            app = QtWidgets.QApplication(sys.argv)
        myApp = SendToFTP(list_path_FTP)
        myApp.show()
        # stylesheet.set_default(app)
        sys.exit(app.exec_()) 


def show():
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        maya_win.deleteUI(uiName)
        myApp = RFEvaPublish(maya_win.getMayaWindow())
        myApp.show()
        return myApp
    else:
        logger.info('Run in RFEvaPublish\n')
        app = QtWidgets.QApplication.instance()
        if not app:
            app = QtWidgets.QApplication(sys.argv)
        myApp = RFEvaPublish()
        myApp.show()
        # stylesheet.set_default(app)
        sys.exit(app.exec_())
if __name__ == '__main__':
    show()
