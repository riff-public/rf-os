# -*- coding: utf-8 -*-
import os
import sys

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
from rf_utils import icon
from functools import partial

project_path = "P:/Eva/scene/work"

class RFEvaPublishUI(QtWidgets.QWidget):
    # sendData = QtCore.Signal(list)
    def __init__(self, parent=None) :
        super(RFEvaPublishUI, self).__init__(parent)
        self.setMouseTracking(True)
        
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem)
        self.logo_layout = QtWidgets.QVBoxLayout()
        self.horizontalLayout_2.addLayout(self.logo_layout)
        self.main_layout = QtWidgets.QVBoxLayout()
        self.main_layout.addLayout(self.horizontalLayout_2)
        self.head_label = QtWidgets.QLabel("EvaOutSource Publish")
        font = QtGui.QFont()
        font.setFamily("MingLiU_HKSCS-ExtB")
        font.setPointSize(12)
        self.head_label.setFont(font)
        self.head_label.setObjectName("head_label")
        self.main_layout.addWidget(self.head_label)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.shot_tableWidget = QtWidgets.QTableWidget()
        self.shot_tableWidget.setObjectName("shot_tableWidget")
        self.shot_tableWidget.setColumnCount(5)
        self.shot_tableWidget.setRowCount(0)
        # item = QtWidgets.QTableWidgetItem()
        # self.shot_tableWidget.setVerticalHeaderItem(0, item)
        # item = QtWidgets.QTableWidgetItem()
        # self.shot_tableWidget.setVerticalHeaderItem(1, item)

        item = QtWidgets.QTableWidgetItem()
        self.shot_tableWidget.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem('Shot')
        self.shot_tableWidget.setHorizontalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem("Department")
        self.shot_tableWidget.setHorizontalHeaderItem(2, item)
        item = QtWidgets.QTableWidgetItem("Version")
        self.shot_tableWidget.setHorizontalHeaderItem(3, item)
        item = QtWidgets.QTableWidgetItem("File Mp4")
        self.shot_tableWidget.setHorizontalHeaderItem(4, item)
        #pyqt5
        # self.shot_tableWidget.horizontalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Stretch)
        # self.shot_tableWidget.horizontalHeader().setSectionResizeMode(0, QtWidgets.QHeaderView.ResizeToContents)
        # self.shot_tableWidget.horizontalHeader().setSectionResizeMode(1, QtWidgets.QHeaderView.ResizeToContents)
        # self.shot_tableWidget.horizontalHeader().setSectionResizeMode(3, QtWidgets.QHeaderView.ResizeToContents)
        #pyqt4
        self.shot_tableWidget.horizontalHeader().setResizeMode(QtWidgets.QHeaderView.Stretch)
        self.shot_tableWidget.horizontalHeader().setResizeMode(0, QtWidgets.QHeaderView.ResizeToContents)
        self.shot_tableWidget.horizontalHeader().setResizeMode(1, QtWidgets.QHeaderView.ResizeToContents)
        self.shot_tableWidget.horizontalHeader().setResizeMode(2, QtWidgets.QHeaderView.ResizeToContents)
        self.shot_tableWidget.horizontalHeader().setResizeMode(3, QtWidgets.QHeaderView.ResizeToContents)
        # self.shot_tableWidget.horizontalHeader().setResizeMode(4, QtWidgets.QHeaderView.ResizeToContents)

        item = QtWidgets.QTableWidgetItem()
        item.setCheckState(QtCore.Qt.Checked)
        self.shot_tableWidget.setItem(1, 0, item)
        self.shot_tableWidget.horizontalHeader().setCascadingSectionResizes(False)
        self.shot_tableWidget.horizontalHeader().setMinimumSectionSize(40)
        self.shot_tableWidget.verticalHeader().setDefaultSectionSize(40)
        self.shot_tableWidget.verticalHeader().setMinimumSectionSize(30)
        self.horizontalLayout.addWidget(self.shot_tableWidget)
        self.main_layout.addLayout(self.horizontalLayout)
        self.achive_pushButton = QtWidgets.QPushButton('Achive')
        self.rv_playlist_pushButton = QtWidgets.QPushButton('Play RV Playlist')
        self.main_layout.addWidget(self.rv_playlist_pushButton)
        self.main_layout.addWidget(self.achive_pushButton)
        self.verticalLayout_2.addLayout(self.main_layout)
        self.shot_tableWidget.itemActivated.connect(self.chageCurrentRow)
        self.shot_tableWidget.resizeColumnsToContents()
        self.setLayout(self.verticalLayout_2)


# ev_ev04_q2010_s1210:
#   anim:
#     workspace: P:/Eva/scene/work/ev04/ev_ev04_q2010_s1210/layout/main/maya/ev_ev04_q2010_s1210_layout_main.v001.TA.ma
#     k_version: 1
#     assets:
#     - namespace: :eva_mk07_0_001
#       path: Z:/asset/ev04/assets/eva/eva/mk07/0/publish/rig/provMid_Maya_all/rev003/mk07_0_pubRIG_provMid.ma
#     - namespace: :s1210_cam
#       path: P:/Eva/asset/_global/cam/hero/cam_rig_main.hero.ma
#   genz:
#     workspace: P:/Eva/scene/work/ev04/ev_ev04_q2010_s1210/layout/main/maya/ev_ev04_q2010_s1210_layout_main.v001.TA.ma
#     k_version: 2
#     assets:
#     - namespace: :eva_mk07_0_001
#       path: Z:/asset/ev04/assets/eva/eva/mk07/0/publish/rig/provMid_Maya_all/rev003/mk07_0_pubRIG_provMid.ma
#     - namespace: :s1210_cam
#       path: P:/Eva/asset/_global/cam/hero/cam_rig_main.hero.ma

    def mousePressEvent(self, QMouseEvent):
        print 'test'
        self.mouse_move = QMouseEvent.pos()
        return self.mouse_move

    def chageCurrentRow(self):
        print 'test'
        row = self.shot_tableWidget.indexAt(self.pos().row())
        print row
        self.shot_tableWidget.setCurrentRow(row)
        # int row = ui->tableWidget->indexAt(w->pos()).row();
        # ui->tableWidget->removeRow(row);
        # ui->tableWidget->setCurrentCell(0, 0);

    def update_file_mp4_comboBox(self, rowPosition, shot_entity, *args):
        step = self.shot_tableWidget.cellWidget(rowPosition, 2).currentText()
        mp4_file_comboBox = self.shot_tableWidget.cellWidget(rowPosition, 4)
        if mp4_file_comboBox:
            mp4_file_comboBox.clear()
            output_path = make_path_output(shot_entity, step=step)
            list_mp4_file = [f for f in os.listdir(output_path) if f.endswith(".mp4")]
            if list_mp4_file:
                mp4_file_comboBox.addItems(list_mp4_file)
    

    def add_shot_row(self, shot_name = '', shot_entity= dict()):
        rowPosition = self.shot_tableWidget.rowCount()
        self.shot_tableWidget.insertRow(rowPosition)
        check_item = QtWidgets.QTableWidgetItem() 
        check_item.setBackground(QtGui.QColor(100, 100, 100))
        self.shot_tableWidget.setItem(rowPosition, 0, check_item)

        shot_table_widget = QtWidgets.QTableWidgetItem(shot_name)  
        shot_table_widget.setData(QtCore.Qt.UserRole,shot_entity)
        self.shot_tableWidget.setItem(rowPosition, 1, shot_table_widget)
        if not 'all' in shot_name:
            check_item.setCheckState(QtCore.Qt.Unchecked )
            shot_data = None
            if shot_name in shot_entity.data:  
                shot_data = shot_entity.data[shot_name]
            if shot_data:
                print shot_data
                self.shot_tableWidget.setItem(rowPosition, 2, QtWidgets.QTableWidgetItem())
                step_comboBox = QtWidgets.QComboBox()
                self.shot_tableWidget.setCellWidget(rowPosition, 2, step_comboBox)
                for step in shot_data:
                    step_comboBox.addItem(step)
                # combobox = self.shot_tableWidget.currentItem()
                # combobox = self.shot_tableWidget.cellWidget(rowPosition, 2)
                # # if isinstance(widget, QComboBox):
                # combo_currentIndex = combobox.currentIndex ()
                #       
            else:
                self.shot_tableWidget.setItem(rowPosition, 2, QtWidgets.QTableWidgetItem())
                step_comboBox = QtWidgets.QComboBox()
                self.shot_tableWidget.setCellWidget(rowPosition, 2, step_comboBox)

            k_version = shot_entity.data['k_version']
            version_table_widget = QtWidgets.QTableWidgetItem('k%03d'%k_version)  
            self.shot_tableWidget.setItem(rowPosition, 3, version_table_widget)

            # rv_play_pushButton = QtWidgets.QPushButton('Play RV')
            # rv_play_pushButton.setMinimumSize(10, 30)
            # rv_table_widget = QtWidgets.QTableWidgetItem(rowPosition) 
            if shot_data:
                mp4_file_comboBox = QtWidgets.QComboBox() 
                output_path = make_path_output(shot_entity, step=shot_data.keys()[0])
                print output_path
                list_mp4_file = [f for f in os.listdir(output_path) if f.endswith(".mp4")]
                if list_mp4_file:
                    self.shot_tableWidget.setCellWidget(rowPosition, 4, mp4_file_comboBox)
                    if list_mp4_file:
                        mp4_file_comboBox.addItems(list_mp4_file)

                step_comboBox.currentIndexChanged.connect(partial(self.update_file_mp4_comboBox, rowPosition, shot_entity))
            # self.shot_tableWidget.setCellWidget(rowPosition, 4, rv_play_pushButton)
            # rv_play_pushButton.clicked.connect(partial(self.play_rv, rv_play_pushButton))


            # if 'k_version' in shot_data[step]:
            #     self.shot_tableWidget.setItem(rowPosition, 3, version_table_widget)
            # shot_data[step]['k_version']

        #             for step in shot_data[shot]:
        #                 print step
        #                 comboBox.addItem(step)

                    # step_comboBox = QtWidgets.QtCore
                    # shot_table_widget = QtWidgets.QTableWidgetItem()  
                    # self.shot_tableWidget.setItem(rowPosition, 1, shot_table_widget)
                    # print 'step', step
    
    


    def play_rv(self, rv_play_pushButton):
        print self.mouse_move


    def mousePressEvent(self, QMouseEvent):
        return QMouseEvent.pos()

    def switch_k_veresion(self, comboBox, k_widget_item, args):
        combo_currentIndex = comboBox.currentIndex ()
        k_version = comboBox.itemData( combo_currentIndex, QtCore.Qt.UserRole)
        k_widget_item.setText('k%03d'%k_version)


    def add_episode_row(self, episode_name=''):
        rowPosition = self.shot_tableWidget.rowCount()
        self.shot_tableWidget.insertRow(rowPosition)

        newItem = QtWidgets.QTableWidgetItem(episode_name)  
        self.shot_tableWidget.setItem(rowPosition, 0, newItem)
        self.shot_tableWidget.setSpan(rowPosition, 0, 1, 4) 

def make_path_output(shot_entity, step=''):
    episode = shot_entity._scene.episode
    shot_entity._scene.context.update(process='main', step =step)
    print shot_entity._scene.path.output()
    shot_name = shot_entity._scene.name
    depart = shot_entity._scene.step
    k_version = shot_entity.data['k_version']
    
    return os.path.join(project_path, '%s/%s/%s/main/output'%(episode, shot_name, depart)).replace('\\', '/')
