#! /usr/bin/env python
# -*- coding: utf-8 -*-
# fileName : commandPort.py
import os 
import sys 
import socket
#HOST = '192.168.1.122'	# The remote host
HOST = '127.0.0.1'		# the local host
PORT = 9001			 # The same port as used by the server
PORT2 = 9002			 # The same port as used by the server
ADDR=(HOST, PORT)
ADDR2=(HOST, PORT2)
 

def send_command(command):
	client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	client.connect(ADDR)
	client.send(command)
	data = client.recv(1024)
	client.close()

	return data 

def send_mel_command(command):
	client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	client.connect(ADDR2)
	client.send(command)
	data = client.recv(1024)
	client.close()

	return data 

class MayaCommandPort(object):
		"""docstring for MayaCommandPort"""
		def __init__(self):
			super(MayaCommandPort, self).__init__()
			self.client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			self.client.connect(ADDR)
			self.cmds = []

		def add(self, command): 
			self.cmds.append(command)
			# return self.client.recv(1024)

		def send(self): 
			command = ';'.join(self.cmds)
			self.client.send(command)
			self.cmds = []
			return self.client.recv(1024)

		def close(self): 
			self.client.close()
				
	 
def open_file(path): 
	print 'open file %s' % path
	cmds = 'import maya.cmds as mc;'
	cmds += 'mc.file("%s", o=True, f=True)' % path
	return send_command(cmds)

def save_file(path): 
	print 'save file %s' % path
	filename, ext = os.path.splitext(path)
	filetype = 'mayaAscii' if ext == '.ma' else 'mayaBinary' if ext == '.mb' else ''
	cmds = 'import maya.cmds as mc;'
	cmds += 'mc.file(rename="%s");' % path
	cmds += 'mc.file(save=True, f=True, type="%s");' % filetype
	return send_command(cmds)



def get_file(): 
	cmds = 'file -q -loc'
	return send_mel_command(cmds)


# def test(): 
# 	client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# 	client.connect(ADDR)
# 	client.send('import maya.cmds as mc;')
# 	client.send('mc.polyCube()')
# 	# data = client.recv(1024)
# 	# print data 
# 	client.close() 
