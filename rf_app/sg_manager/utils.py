import sys
import os
import subprocess
from collections import OrderedDict
import time

import logging
logger = logging.getLogger(__name__)

from Qt import QtWidgets
from Qt import QtGui
from Qt import QtCore

from rf_utils.context import context_info
from rf_utils import file_utils

def get_type_episode(sgEntity):
    if sgEntity['type'] == 'Asset':
        return 'sg_asset_type'

    if sgEntity['type'] == 'Shot':
        return 'sg_episode.Scene.code'

def update_entity(sgEntity):
    context = context_info.Context()
    context.update(project=sgEntity['project']['name'],
                    entityType=Config.typeMap[sgEntity['type']],
                    entity=sgEntity['code'],
                    entityGrp=sgEntity[get_type_episode(sgEntity)]
                    )
    entity = context_info.ContextPathInfo(context=context)
    return entity

def list_dir(path):
    return ['%s/%s' % (path, a) for a in file_utils.listFolder(path)]

def list_file(path):
    return ['%s/%s' % (path, a) for a in file_utils.listFile(path)]

def calculate_version(workspacePath):
    return file_utils.calculate_version(file_utils.list_file(workspacePath))


def get_rig(entity, res, absPath=False):
    step = entity.step
    prevRes = entity.res
    process = entity.process
    entity.context.update(step='rig', process='main', res=res)

    heroPath = entity.path.hero().abs_path()
    heroRelPath = entity.path.hero()
    mbFileName = entity.output_name(outputKey='rig', hero=True)
    fileName = entity.publish_name(hero=True)
    
    rigFile1 = '%s/%s' % (heroPath, mbFileName)
    rigFile2 = '%s/%s' % (heroPath, fileName)

    rigRelPath1 = '%s/%s' % (heroRelPath, mbFileName)
    rigRelPath2 = '%s/%s' % (heroRelPath, fileName)

    # restore
    entity.context.update(step=step, process=process, res=prevRes)
    
    # try mb first then ma 
    rigRelPath = ''
    if os.path.exists(rigFile1): 
        return rigFile1 if absPath else rigRelPath1

    if os.path.exists(rigFile2): 
        return rigFile2 if absPath else rigRelPath2

    return rigFile2 if absPath else rigRelPath2

def get_miarmy_oa(entity, res, absPath=False): 
    step = entity.step
    prevRes = entity.res
    process = entity.process
    entity.context.update(step='rig', process='oa', res=res)

    heroPath = entity.path.hero().abs_path()
    heroRelPath = entity.path.hero()
    oaFile = entity.output_name(outputKey='miarmyOa', hero=True)

    oaRigFile = '%s/%s' % (heroPath, oaFile)
    oaRigRelFile = '%s/%s' % (heroRelPath, oaFile)

    if absPath: 
        return oaRigFile

    else: 
        return oaRigRelFile

def get_anim_loop(entity, res, process, look, absPath=False): 
    step = entity.step
    # prevRes = entity.res
    # process = entity.process
    entity.context.update(step='anim', process=process, res=res, look=look)

    heroPath = entity.path.hero().abs_path()
    heroRelPath = entity.path.hero()
    maLoopFile = entity.output_name(outputKey='mayarsproxy', hero=True)

    animLoopFile = '%s/%s' % (heroPath, maLoopFile)
    animLoopRelFile = '%s/%s' % (heroRelPath, maLoopFile)

    if absPath: 
        return animLoopFile

    else: 
        return animLoopRelFile

def get_crowd_card(entity, res, process, look, absPath=False): 
    step = entity.step
    # prevRes = entity.res
    # process = entity.process
    entity.context.update(step='anim', process=process, res=res, look=look)

    heroPath = entity.path.hero().abs_path()
    heroRelPath = entity.path.hero()
    maCardFile = entity.output_name(outputKey='card', hero=True)

    crowdCardList = []
    # crowdCardFile = '%s/%s' % (heroPath, maCardFile)
    # crowdCardRelFile = '%s/%s' % (heroRelPath, maCardFile)

    if absPath: 
        if res == '*':
            if heroPath:
                allFiles = os.listdir(heroPath)
                nameCheck = maCardFile.split('*')[0]
                for f in allFiles:
                    if nameCheck in f:
                        crowdCardFile = '%s/%s' % (heroPath, f)
                        crowdCardList.append(crowdCardFile)
        return crowdCardList

    else: 
        if res == '*':
            if heroRelPath:
                allFiles = os.listdir(heroPath)
                nameCheck = maCardFile.split('*')[0]
                for f in allFiles:
                    if nameCheck in f:
                        crowdCardRelFile = '%s/%s' % (heroRelPath, f)
                        crowdCardList.append(crowdCardRelFile)
        return crowdCardList



def get_gpu(entity, res, absPath=False):
    step = entity.step
    prevRes = entity.res
    process = entity.process
    entity.context.update(step='model', process='main', res=res)

    heroPath = entity.path.hero().abs_path() if absPath else entity.path.hero()
    fileName = entity.output_name(outputKey='gpu', hero=True)
    gpuFile = '%s/%s' % (heroPath, fileName)

    # restore
    entity.context.update(step=step, process=process, res=prevRes)
    return gpuFile

def get_set(entity, res, absPath=True):
    step = entity.step
    prevRes = entity.res
    process = entity.process
    entity.context.update(step='set', process='main', res=res)

    heroPath = entity.path.hero().abs_path() if absPath else entity.path.hero()
    fileName = entity.output_name(outputKey='set', hero=True)
    setFile = '%s/%s' % (heroPath, fileName)

    # restore
    entity.context.update(step=step, process=process, res=prevRes)
    return setFile


def get_rig_set(entity, res, absPath=True): 
    step = entity.step
    prevRes = entity.res
    process = entity.process
    entity.context.update(step='set', process='main', res=res)

    heroPath = entity.path.hero().abs_path() if absPath else entity.path.hero()
    fileName = entity.output_name(outputKey='rig', hero=True, ext='.ma')
    setFile = '%s/%s' % (heroPath, fileName)

    # restore
    entity.context.update(step=step, process=process, res=prevRes)
    return setFile

def get_loop_data(entity): 
    refData = None
    try: 
        heroPath = entity.path.hero().abs_path()
        fileName = entity.output_name(outputKey='refData', hero=True)
        refData = '%s/%s' % (heroPath, fileName)
    except Exception as e: 
        logger.error(e)
        logger.debug('Failed to find refData. Nothing serious. It still OK to continue')
    return refData

def read_loop_data(path): 
    data = file_utils.ymlLoader(path)
    refs = [v for k, v in data.iteritems()]
    return refs

def open_explorer(entityPath):
    path = entityPath.replace('/', '\\')
    subprocess.Popen(r'explorer /select,"%s"' % path)


def convert_format(format1, format2):
    try:
        from PIL import Image
    except ImportError:
        from PIL_Maya import Image
    im = Image.open(format1)
    im.save(format2)


def copy_clipboard(data):
    """
    Add data provided to clipboard.
        args:
            data = Data to add to clipboard. (string)
        return: None
    """
    from subprocess import call
    command = 'echo | set /p=%s|clip' %data.strip()
    call(command, shell=True)
