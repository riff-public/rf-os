import os
import sys
import logging 
import shutil
import re

try:
    import maya.cmds as mc
    import maya.mel as mm
    import pymel.mayautils as mu
    from rftool.utils import maya_utils
    from rftool.shade import shade_utils
    from rf_utils.context import context_info
    from rftool import preload
    from rf_utils import ascii_utils
    from rf_utils import file_utils
except ImportError as e:
    print("except", e)


logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())


def save_file(filepath):
    name, ext = os.path.splitext(filepath)
    filetype = 'mayaAscii' if ext == '.ma' else 'mayaBinary' if ext == '.mb' else ''
    mc.file(rename=filepath)
    return mc.file(save=True, f=True, type=filetype)


def open_file(filepath, fixshade=False, loadNoReference=False):
    ext = os.path.splitext(filepath)[-1]
    if os.path.exists(filepath):
        entity = context_info.ContextPathInfo(path=filepath)
        fileType = 'mayaAscii' if ext == '.ma' else 'mayaBinary' if ext == '.mb' else ''
        loadRef = 'all' if not loadNoReference else 'none'
        result = 0
        try: 
            result = mc.file(filepath, o=True, f=True, ignoreVersion=True, loadReferenceDepth=loadRef)
        except Exception as e: 
            logger.error(e)
        finally: 
            preload.check(entity)

        if fixshade: 
            fix_shade()
        return result 


def open_as(srcPath, dstPath):
    if not srcPath == dstPath:
        if not os.path.exists(os.path.dirname(dstPath)):
            os.makedirs(os.path.dirname(dstPath))
        shutil.copy2(srcPath, dstPath)
        return mc.file(dstPath, o=True, f=True)

def create_reference(entity, path, material=True, namespace=None, materialType='preview', defaultPose=None, separateShader=False):
    from rftool.shade import shade_utils
    from rf_utils.pipeline import asset_tag
    from rf_utils.pipeline import namespace_control as nc 
    from rf_qc.maya_lib.scene import add_asset_tag
    reload(nc)
    from rf_utils import user_info 
    scene = context_info.ContextPathInfo()
    ncontrol = nc.NamespaceControl(scene)
    currentDags = mc.ls(assemblies=True)
    asset = entity.copy()
    assetName = asset.name
    asset.context.update(step='rig') # use rig mtr for anim rig arthur_rigMainMtr
    shadeNamespace = asset.mtr_namespace
    namespace = maya_utils.get_namespace2('%s_001' % assetName, scene)
    instance_num = re.findall(r'\d{3}', namespace)[0]
    # namespace = maya_utils.create_reference(assetName, path, force=True) if not namespace else namespace
    
    # use this instead because we want to check shot namespaces first 
    maya_utils.create_reference_asset(namespace, path)
    
    # add record 
    if scene.valid: 
        workspace = mc.file(q=True, loc=True)
        user = user_info.User()
        ncontrol.add_record(namespace, workspace, path, user.login_user(), override=False)

    # defalut material
    # rig material 
    mtrFile = asset.library(context_info.Lib.rigMtr)
    if materialType == 'render': 
        mtrFile = asset.library(context_info.Lib.mtr)
    mtr = '%s/%s' % (asset.path.hero().abs_path(), mtrFile)
    
    # try find model tmp shade if rig shade does not exists 
    if not os.path.exists(mtr): 
        mtrFile = asset.library(context_info.Lib.modelTmp)
        mtr = '%s/%s' % (asset.path.hero().abs_path(), mtrFile)

    mtrRel = '%s/%s' % (asset.path.hero(), mtrFile)

    # connect shade Connect for stroke
    geoGrp = '%s:%s' % (namespace, 'Geo_Grp')

    # get shader attribute 
    separateShaderAttr = '%s.separateShader' % geoGrp
    if not separateShader:
        separateShader = False
    if mc.objExists(separateShaderAttr): 
        separateShader = mc.getAttr(separateShaderAttr)

    asset = context_info.ContextPathInfo(path=mtrRel)
    shadeFileExt = os.path.basename(mtrRel)
    shadeFileName, ext = os.path.splitext(shadeFileExt)

    connectionData = []
    dataFile = entity.output_name(outputKey='shdConnectData', hero=True)
    heroPath = entity.path.hero().abs_path()
    dataFile = '%s/%s' % (heroPath, dataFile)
    if os.path.isfile(dataFile):
        print 'found Connection Data assign it!'
        connectionData = file_utils.ymlLoader(dataFile)
    else:
        print 'ConnectionData not found!'
        if mc.objExists('%s.shdConnect' %geoGrp):
            connectionStr = mc.getAttr('%s.shdConnect' %geoGrp)
            connectionData = eval(connectionStr)

    shadeFileName = shadeFileName.split('.')[0]  # get rid of .hero
    shadeKey = shadeFileName.split('_mtr_')[-1]
    connectionInfo = []

    if shadeKey in connectionData:
        connectionInfo = connectionData[shadeKey]

    # connect shade finish


    if material and os.path.exists(mtr):
        if separateShader: 
            shadeNamespace = '{}_{}'.format(shadeNamespace, instance_num)
        shade_utils.apply_ref_shade(mtrRel, shadeNamespace, namespace, connectionInfo)
        # add tag
        geoGrp = '%s:%s' % (namespace, 'Geo_Grp')
        if mc.objExists(geoGrp):
            asset_mtr = context_info.ContextPathInfo(path=mtr)
            asset_mtr.update_context_from_file()
            print('look', asset_mtr.look)
            asset_tag.add_tag(geoGrp, look=asset_mtr.look, mtrPath=mtrRel)
        else:
            logger.error('No %s found!! Tag cannot be added. Asset might be outside pipeline' % geoGrp)
    refObjects = [a for a in mc.ls(assemblies=True) if not a in currentDags]

    if defaultPose: 
        if os.path.exists(defaultPose): 
            from rf_app.anim.animIO import core as animIO
            animIO.import_pose(namespace=namespace, path=defaultPose)
            logger.info('Apply "%s" pose' % defaultPose)

    
    return refObjects


def create_miarmyOa_reference(entity, path): 
    from rf_utils.miarmy import miarmy_utils
    currentDags = mc.ls(assemblies=True)
    namespace = miarmy_utils.miarmy_oa_namespace(entity)
    version =  1
    namespace = namespace + '%03d'%(version)
    if mc.namespace(ex=namespace):
        version = version + 1
        currVersion = namespace.split('_')[-1]
        namespace.replace(currVersion, '%03d' % version)
    maya_utils.create_reference_asset(namespace, path)
    refObjects = [a for a in mc.ls(assemblies=True) if not a in currentDags]
    return refObjects
    # else: 
    #     logger.warning('namespace %s exists. Skip create reference' % namespace)


def miarmy_oa_namespace(entity): 
    from rf_utils.miarmy import miarmy_utils
    return miarmy_utils.miarmy_oa_namespace(entity)


def group_oa(agents): 
    from rf_utils.miarmy import miarmy_utils
    reload(miarmy_utils)
    if agents: 
        miarmy_utils.miarmy_ready(agentLoco=False)
        miarmy_utils.parent_agent_logo(agents)


def import_file(path):
    return mc.file(path,  i=True, type='mayaAscii', options = 'v=0', pr = True, loadReferenceDepth = 'all')

def import_crowd_loop_file(path, ns):
    allObj = mc.file(path,  i=True, type='mayaAscii', options = 'v=0', pr = True, loadReferenceDepth = 'all', rnn=True)
    for obj in allObj:
        if mc.ls(obj, type='transform'):
            obj = mc.ls(obj)[0]
            mc.rename(obj , '{}{}'.format(ns,obj))
            mc.rename('redshiftProxyPlaceholder1', ns[:-1])
    return allObj

def import_crowd_card_file(path, ns):
    allObj = mc.file(path,  i=True, type='mayaAscii', options = 'v=0', pr = True, loadReferenceDepth = 'all', rnn=True)
    for obj in allObj:
        if mc.ls(obj, type='transform'):
            obj = mc.ls(obj)[0]
            mc.rename(obj , '{}{}'.format(ns,obj))
    mc.rename('%s%s'% (ns,'card_Geo'), ns[:-1])
    return allObj
    
def get_available_namespace(name):
    i = 1
    all_dags = mc.ls(dag=True)
    while True:
        new_name = '{}{}_'.format(name, i)
        if [dag for dag in all_dags if dag.startswith(new_name)]:
            i += 1
        else:
            return new_name

def reference_file(namespace, path):
    from rf_maya.lib import abc_lib
    reload(abc_lib)
    abc_lib.check_plugin(abc_lib.Plugin.abcImport)
    if not mc.namespace(ex=namespace): 
        return mc.file(path, r=True, ns=namespace)
    return path

def import_abc(path):
    from rf_maya.lib import abc_lib
    reload(abc_lib)
    return abc_lib.import_abc(path)

def import_mtr(targetGrp, path):
    from rftool.shade import shade_utils
    reload(shade_utils)
    obj = mc.ls(sl=True)[0] if mc.ls(sl=True) else ''
    namespace = (':').join(obj.split(':')[:-1]) if ':' in obj else None
    targetNamespace = namespace if namespace else guess_target_namespace(targetGrp)
    return shade_utils.apply_import_shade(path, targetNamespace=targetNamespace)
    # return mu.executeDeferred(shade_utils.apply_import_shade, path, targetNamespace)

def import_mtr_se(targetGrp, path): 
    ascii_utils.MayaAscii(path=path)
    result = mc.file(path, i=True, rnn=True)

    shadingEngines = [a for a in result if mc.objectType(a, isType='shadingEngine')]
    namespace = guess_target_namespace(targetGrp)

    for shadingEngine in shadingEngines: 
        shade_utils.apply_single_shade(shadingEngine, namespace) 

def apply_mtr_se(targetGrp, path): 
    shadingEngines = check_existing_se(path)
    namespace = guess_target_namespace(targetGrp)

    for shadingEngine in shadingEngines: 
        shade_utils.apply_single_shade(shadingEngine, namespace) 

def guess_target_namespace(targetGrp):
    namespaces = maya_utils.list_all_namespaces()
    namespace = [a for a in namespaces if mc.objExists('%s:%s' % (a, targetGrp))]
    return namespace[0] if namespace else ''

def check_existing_se(path): 
    """ check for existing se, if yes return existing nodes """ 
    ma = ascii_utils.MayaAscii(path=path)
    ma.read()
    shadingEngines = ma.ls(['shadingEngine'])
    return [a for a in shadingEngines if mc.objExists(a)] if shadingEngines else []


def group(groupName, objects):
    if not mc.objExists(groupName):
        groupName = mc.group(em=True, n=groupName)
    mc.parent(objects, groupName)

def create_group(name): 
    if not mc.objExists(name): 
        return mc.group(em=True, n=name)
    return name


def duplicate_reference_warning(entity, path): 
    confirm = 'Yes I am sure' 
    cancel = 'Cancel'
    message = '%s is main asset and already exists in your scene. There should be only 1 %s. \nDo you still want to create another reference?' % (entity.name, entity.name)
    path = context_info.RootPath(path).abs_path()
    info = get_refs()

    if entity.type == 'char' and entity.parent == 'main': 
        if path in info.keys():

            alternative = 'Remove %s %s and create a new reference' % (len(info[path]), entity.name)
            # warning 
            result = mc.confirmDialog(title='Warning', message=message, button=[confirm, alternative, cancel])
            
            if result == alternative: 
                # remove refs 
                for ref in info[path]: 
                    mc.file(ref, rr=True)

            return True  if result in [confirm, alternative] else False 

    return True 

def default_pose_choice(entity): 
    from rf_app.export.pose import pose_utils
    reload(pose_utils)
    message = '"%s" has default pose. Do you want to set default pose?' % entity.name
    confirm = 'Set Default Pose'
    no = 'Use T-Pose'

    path = pose_utils.default_pose(entity)
    if os.path.exists(path): 
        # ask 
        result = mc.confirmDialog(title='Set Default Pose', message=message, button=[confirm, no])
        if result == confirm: 
            return path 
    return None
                

def get_refs(): 
    info = dict()
    refs = mc.file(q=True, r=True)
    for ref in refs: 
        key = context_info.RootPath(ref.split('{')[0]).abs_path()
        if not key in info: 
            info.update({key: [ref]})
        else: 
            info[key].append(ref)
    return info


def fix_shade(): 
    sn = mc.file(q=True, sn=True) or mc.file(q=True, location=True)
    # only fix if char or prop in sevenchieck 
    refPaths = mc.file(q=True, r=True)
    projects = ['SevenChickMovie', 'projectName']
    validProject = any(a in sn for a in projects)
    activeKeys = ['char', 'prop']
    
    # get reference path dict 
    refDict = maya_utils.get_file_reference_info(sn)
    
    # convert to abs_path 
    absDict = dict()
    for k, v in refDict.iteritems(): 
        absDict[context_info.RootPath(k).abs_path()] = v

    for path in refPaths: 
        info = absDict.get(path, dict())
        unload = '0'

        if '-dr' in info.keys(): 
            unload = info['-dr']
        # fix and load 
        if not unload == '1': 
            if any(a in path for a in activeKeys): 
                result = shade_utils.fix_unload_reference_shader(path)
                logger.debug('shade fixed %s' % path)
                
                # not result mean not fix shade, just do manual load 
                if not result: 
                    try: 
                        mc.file(path, loadReference=True, prompt=False)
                    except Exception as e: 
                        logger.error(e)
            # load only
            else: 
                try: 
                    mc.file(path, loadReference=True, prompt=False)
                except Exception as e: 
                    logger.error(e)

def add_obj_to_layer(namespace): 
    """ add content within namespace to current layer """
    currentLayer = mc.editRenderLayerGlobals(q=True, currentRenderLayer=True)
    topNodes = maya_utils.list_top_namespace(namespace)
    if not currentLayer == 'defaultRenderLayer': 
        mc.editRenderLayerMembers(currentLayer, topNodes, noRecurse=True)    
