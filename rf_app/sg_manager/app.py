# v.0.0.1 polytag switcher
_title = 'Riff SG Manager'
_version = 'v.0.1.0'
_des = 'Mov preview'
uiName = 'SGManagerUI'

#Import python modules
import sys
import os
import getpass
from datetime import datetime
import logging
from functools import partial
from collections import OrderedDict

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

# import config
import rf_config as config

# framework modules
from rf_utils import log_utils
from rf_utils.ui import load
from rf_utils.ui import stylesheet
from rf_app.notification.file_unlock import main as noti_unlock

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

import ui
reload(ui)
# reload(ui.auto_build_widget)
# thread
from rf_utils.sg import sg_thread
import dcc_hook
reload(dcc_hook)
import cp_hook 
# reload(cp_hook)
import utils
reload(utils)
from rf_utils.widget import dialog
from rf_utils import icon
from rf_utils.sg import sg_utils
from rf_utils.sg import sg_process
from rf_utils.pipeline import user_pref
from rf_utils.context import context_info
from rf_utils.widget.app import create_entity_widget
from rf_utils.pipeline import lockdown
from rf_utils.pipeline import create_asset
from rf_utils.pipeline import create_scene
context_info.sg = sg_utils.sg
# reload(create_entity_widget)
from rf_utils import register_entity
from rf_maya.rftool.subasset import subasset_utils


if not sg_utils.connection: 
    dialog.MessageBox.error('Error', 'No internet connection to Shotgun server')
    reload(sg_utils)

class Config:
    asset = 'asset'
    scene = 'scene'
    mode = {asset: 'Asset', scene: 'Shot'}
    allTag = 'All tag'
    expiredOption = {'1. default': 300, '2. 1 Hour': 3600}


class RFSGManager(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        #Setup Window
        super(RFSGManager, self).__init__(parent)

        # ui read
        self.ui = ui.SGManagerUI(parent)
        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)

        # title
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.resize(1200, 750)

        # declare context
        self.entity = None
        self.uiEntity = None

        # caches
        self.cachesData = {Config.asset: dict(), Config.scene: dict()}

        # app
        self.app = ''
        if config.isMaya:
            self.app = 'maya'

        # pass modules
        # self.ui.lookWidget.asm = asm_lib

        # menu cache
        self.pendingShowMenu = False
        self.pendingUpdateIcon = False

        # menu command
        self.showPanel = 'Show RF Panel'
        self.referenceAnimRig = 'Reference Anim Rig'
        self.referenceAnimLoRig = 'Reference Lo Rig'
        self.referenceAnimProxyRig = 'Reference Proxy Rig'
        self.referenceAnimPrevisRig = 'Reference Previs Rig'
        self.referenceMiarmyOA = 'Reference Miarmy Oa'
        self.importAnimLoop = 'Import Crowd Anim Loop'
        self.referenceGpuMd = 'Reference Gpu-md'
        self.referenceGpuPr = 'Reference Gpu-pr'
        self.propItLabel = 'Prop It'
        self.autoRigLabel = 'Auto Generate Rig'
        self.referenceSet = 'Reference Set'
        self.referenceRigSet = 'Reference Set Rig'
        self.referenceGroupAsset = 'Reference Loop'

        # view
        self.iconMode = 'icon'
        self.listMode = 'list'
        self.textMode = 'No Icon'
        self.view = self.iconMode

        # thread list
        self.threads = []

        # prefs
        self.pref = user_pref.ToolSetting(uiName)
        self.prefData = self.pref.read()

        self.init_menu()
        self.init_ui()
        self.init_signals()
        self.init_functions()
        self.set_default()

    def init_menu(self): 
        # main menu bar
        mainMenu = self.menuBar()
        fileMenu = mainMenu.addMenu('Options')
        refreshCache = fileMenu.addAction('Refresh Cache')
        refreshCache.triggered.connect(partial(self.refresh_cache))

        self.set_cache_menu(fileMenu)

    def set_cache_menu(self, menu): 
        expiredCacheMenu = menu.addMenu('Set Expired')

        for label in sorted(Config.expiredOption.keys()): 
            time = Config.expiredOption[label]
            subMenu = expiredCacheMenu.addAction(label, checkable=True)
            subMenu.triggered.connect(partial(self.set_cache_expired, label, time))

    def set_default(self):
        pass

    def init_ui(self):
        """ initial ui values """
        self.add_status_bar()
        self.ui.modeComboBox.addItems(sorted(Config.mode.keys()))
        # set prev selection
        index = sorted(Config.mode.keys()).index(self.prefData['entityType']) if self.prefData.get('entityType') else 0
        self.ui.modeComboBox.setCurrentIndex(index)
        self.entityType = str(self.ui.modeComboBox.currentText())

    def add_status_bar(self):
        self.statusBar().showMessage('Ready')

    def init_signals(self):
        """ init ui signals """
        self.ui.projectWidget.projectChanged.connect(self.project_changed)
        self.ui.modeComboBox.currentIndexChanged.connect(self.mode_changed)
        self.ui.sliderWidget.valueChanged.connect(self.set_icon_size)

        # filters
        self.ui.typeEpisodeComboBox.currentIndexChanged.connect(self.filter1_changed)
        self.ui.subtypeSeqComboBox.currentIndexChanged.connect(self.filter2_changed)
        self.ui.tagWidget.selected.connect(self.tag_selected)

        self.ui.typeEpisodeCheckBox.stateChanged.connect(partial(self.show_filtered_entity, 'type'))
        self.ui.subtypeSeqCheckBox.stateChanged.connect(partial(self.show_filtered_entity, 'subtype'))

        # search
        self.ui.searchLineEdit.textChanged.connect(partial(self.show_filtered_entity, 'search'))

        # entity signals
        self.ui.entityWidget.selected.connect(self.entity_selected)
        self.ui.entityWidget.listWidget.customContextMenuRequested.connect(self.show_entity_menu)
        self.ui.stepWidget.currentIndexChanged.connect(self.step_changed)
        self.ui.processWidget.currentIndexChanged.connect(self.process_changed)
        self.ui.appWidget.currentIndexChanged.connect(self.app_changed)

        # button
        self.ui.openButton.clicked.connect(self.open_file)
        self.ui.saveButton.clicked.connect(self.save_file)
        self.ui.openAsButton.clicked.connect(self.open_as)
        self.ui.infoWidget.custom_pushButton.clicked.connect(self.reference_file)

        # tabs
        self.ui.tabWidget.currentChanged.connect(self.tab_changed)

        # view mode
        self.ui.iconRadioButton.clicked.connect(self.view_changed)
        self.ui.listRadioButton.clicked.connect(self.view_changed)
        self.ui.textRadioButton.clicked.connect(self.view_changed)

        # workspace mode
        self.ui.workspaceCheckBox.stateChanged.connect(self.workspace_checked)

        # unlock file 
        self.ui.fileWidget.unlocked.connect(self.file_unlock)

        self.ui.refBucketWidget.listWidget.itemEntered.connect(self.bucket_selected)

    def init_functions(self):
        """ start functions """
        self.ui_customized()
        self.set_prev_selection()
        self.set_ui_context()
        self.fetch_data()
        self.ui.fileWidget.hook = dcc_hook.reference_file

    def mode_changed(self):
        """ asset or scene changed """
        self.entityType = str(self.ui.modeComboBox.currentText())
        self.rename_label(self.entityType)
        self.fetch_data()

    def view_changed(self):
        if self.ui.iconRadioButton.isChecked():
            self.view = self.iconMode
            self.ui.entityWidget.set_icon_mode()
            self.fetch_icon()

        elif self.ui.listRadioButton.isChecked():
            self.view = self.listMode
            self.ui.entityWidget.set_list_mode()
            self.fetch_icon()

        elif self.ui.textRadioButton.isChecked(): 
            self.view = self.textMode
            self.ui.entityWidget.set_text_mode()
            self.fetch_icon()


    def workspace_checked(self, val):
        state = True if val else False
        # workspace default
        index = self.ui.tabIndex.index(self.ui.workspaceTab) if state else self.ui.tabIndex.index(self.ui.infoTab)
        self.ui.tabWidget.setCurrentIndex(index)

    def file_unlock(self, status): 
        filename = str(self.ui.fileWidget.selected_file())
        noti_unlock.run(filename)

    def refresh_cache(self): 
        reload(sg_thread)
        self.cachesData = {Config.asset: dict(), Config.scene: dict()}
        sg_thread.sgCache = dict()
        self.project_changed()

    def set_cache_expired(self, label, time): 
        reload(sg_thread)
        sg_thread.Config.cacheTimeOut = time
        logger.info('Set expired cache to %s (%s secs)' % (label, time))

    def project_changed(self):
        """ project changed """
        self.fetch_data()
        self.set_ui_context()
        self.save_pref()

    def rename_label(self, mode):
        """ set label to match mode """
        if mode == Config.asset:
            typeEpisodeText = 'Type : '
            subtypeSeqText = 'Subtype : '

        if mode == Config.scene:
            typeEpisodeText = 'Episode : '
            subtypeSeqText = 'Sequence : '

        self.ui.typeEpisodeCheckBox.setText(typeEpisodeText)
        self.ui.subtypeSeqCheckBox.setText(subtypeSeqText)


    def ui_customized(self):
        self.ui.stepWidget.sortedGuide = ['model', 'rig', 'gloom', 'texture', 'lookdev', 'set', 'layout', 'anim', 'finalcam', 'setdress', 'sim', 'fx', 'techanim', 'light', 'comp']
        self.ui.stepWidget.skipList = ['textures', 'colorscript', 'keyvis', 'storyboard']

    def set_prev_selection(self):
        # set from env 
        activeProject = os.environ.get('active_project')
        prevSaveProject = self.prefData.get('project')
        setDefaultProject = activeProject if activeProject else prevSaveProject
        if setDefaultProject: 
            self.ui.projectWidget.set_current_item(setDefaultProject, signal=False)

    def fetch_data(self):
        """ get data from shotgun """
        # get ui values
        projectEntity = self.ui.projectWidget.current_item()
        sgEntityType = Config.mode[self.entityType]
        project = self.ui.projectWidget.current_project()

        # loading bar
        self.loading_display()

        # if no cache
        if not self.ui.projectWidget.current_project() in self.cachesData[self.entityType].keys():
            # fetch sg data thread
            extraFilters = [['sg_status_list', 'is_not', 'omt']]
            self.sgThread = sg_thread.GetEntity(projectEntity, sgEntityType, extraFilters)
            self.sgThread.value.connect(self.populate_entity)
            self.sgThread.finished.connect(self.fetch_finished)
            self.sgThread.start()
            self.lock_ui(True)

        # use cache
        else:
            sortedData = self.cachesData[self.entityType][self.ui.projectWidget.current_project()]
            self.populate_entity([sortedData[a] for a in sortedData])

    def set_ui_context(self):
        # set global entity for ui use
        project = self.ui.projectWidget.current_project()
        self.uiEntityThread = sg_thread.GetContext(project)
        self.uiEntityThread.value.connect(self.init_ui_entity)
        self.uiEntityThread.start()

    def init_ui_entity(self, entity):
        """ init ui entity """
        self.uiEntity = entity
        self.uiEntity.context.update(entityType=self.entityType)
        # if pending update, do it here
        if self.pendingUpdateIcon:
            self.fetch_icon()
            self.pendingUpdateIcon = False

    def populate_entity(self, sgEntities):
        """ poplulate filters and entities """
        # add cache
        self.sortedEntities = sg_utils.ordered_entity_dict(sgEntities)
        # set caches
        self.cachesData[self.entityType][self.ui.projectWidget.current_project()] = self.sortedEntities
        self.relationData = self.find_relation_data(sgEntities)

        # set filter1
        if self.relationData.keys(): 
            self.populate_filter1(self.relationData.keys())
        else: 
            self.no_item_display()

    def populate_filter1(self, data):
        self.ui.typeEpisodeComboBox.blockSignals(True)
        self.ui.typeEpisodeCheckBox.blockSignals(True)

        self.ui.typeEpisodeComboBox.clear()
        self.ui.typeEpisodeComboBox.addItems(data)
        # prev selection
        lastSel = self.prefData.get('typeEpisode')
        status = self.prefData.get('typeEpisodeStatus')

        self.ui.typeEpisodeComboBox.setCurrentIndex(data.index(lastSel)) if lastSel in data else None
        self.ui.typeEpisodeCheckBox.setChecked(status) if not status == None else None

        self.ui.typeEpisodeComboBox.blockSignals(False)
        self.ui.typeEpisodeCheckBox.blockSignals(False)
        self.filter1_changed()

    def populate_filter2(self, data):
        self.ui.subtypeSeqComboBox.blockSignals(True)
        self.ui.subtypeSeqCheckBox.blockSignals(True)

        self.ui.subtypeSeqComboBox.clear()
        self.ui.subtypeSeqComboBox.addItems(data)
        # prev selection
        lastSel = self.prefData.get('subtypeSeq')
        status = self.prefData.get('subtypeSeqStatus')

        self.ui.subtypeSeqComboBox.setCurrentIndex(data.index(lastSel)) if lastSel in data else None
        self.ui.subtypeSeqCheckBox.setChecked(status) if not status == None else None

        self.ui.subtypeSeqComboBox.blockSignals(False)
        self.ui.subtypeSeqCheckBox.blockSignals(False)
        self.filter2_changed()

    def populate_tags(self):
        self.ui.tagWidget.blockSignals(True)
        sgEntities = self.get_filter_entity()
        tagDict = sg_utils.get_tags(sgEntities) or dict()
        self.ui.tagWidget.clear()
        lastSel = self.prefData.get('tags')
        allTag = {'name': Config.allTag}
        self.ui.tagWidget.add_item(allTag, iconPath=icon.sgTag, showIcon=True)

        for tag, tagEntity in tagDict.iteritems():
            self.ui.tagWidget.add_item(tagEntity, iconPath=icon.sgTag, showIcon=True)

        self.ui.tagWidget.set_current(lastSel[0]) if lastSel else Config.allTag
        self.ui.tagWidget.blockSignals(False)

    def list_entities(self, sgEntities):
        self.ui.entityWidget.clear()
        self.ui.entityWidget.add_items(sgEntities, widget=False)
        self.ui.stepWidget.clear()
        self.ui.processWidget.clear()
        self.ui.appWidget.clear()
        self.view_changed()

        # sel prev selection
        lastSel = self.prefData.get('entity')
        self.ui.entityWidget.set_current(lastSel, signal=True) if not lastSel == None else None

    def fetch_icon(self):
        """ update icon later """
        # update if self.uiEntity from thread finished
        if not self.ui.textRadioButton.isChecked(): 
            if self.uiEntity:
                self.uiEntity.context.update(entityType=self.entityType)
                sgDownloadList = []
                items = self.ui.entityWidget.all_items()

                for item in items:
                    sgEntity = item.data(QtCore.Qt.UserRole)
                    entityGrp = sgEntity['sg_asset_type'] if self.entityType == Config.asset else sgEntity['sg_episode.Scene.code'] if self.entityType == Config.scene else ''
                    entityParent = sgEntity['sg_subtype'] if self.entityType == Config.asset else sgEntity['sg_sequence_code'] if self.entityType == Config.scene else ''
                    entityParent = entityParent if entityParent else ''
                    self.uiEntity.context.update(entity=sgEntity['code'], entityGrp=entityGrp, entityParent=entityParent)
                    self.uiEntity.update_scene_context()
                    iconDir = self.uiEntity.path.icon().abs_path()
                    iconName = self.uiEntity.icon_name(ext='.png')
                    iconPath = '%s/%s' % (iconDir, iconName)

                    if os.path.exists(iconPath):
                        widget = True if self.view == self.listMode else False

                        self.ui.entityWidget.update_icon(item, iconPath, widget)
                    else:
                        sgDownloadList.append([item, sgEntity.get('image'), iconPath]) if sgEntity.get('image') else None

                self.rearange_list()
                self.download_icon(sgDownloadList)
            # if not set pending
            else:
                self.pendingUpdateIcon = True

    def fetch_finished(self):
        self.lock_ui(False)

    def download_icon(self, downloadList):
        self.downloadThread = sg_thread.DownloadImage(downloadList)
        self.downloadThread.status.connect(partial(self.statusBar().showMessage))
        self.downloadThread.update.connect(self.update_icon)
        self.downloadThread.start()
        self.threads.append(self.downloadThread)

    def update_icon(self, data):
        item, iconPath = data
        widget = True if self.view == self.listMode else False
        self.ui.entityWidget.update_icon(item, iconPath, widget)
        self.rearange_list()


    def fetch_publish_workspace(self):
        self.ui.publishSpaceWidget.update_input(self.entity)

    def fetch_input(self):
        if self.entity:
            sgEntity = self.ui.entityWidget.current_item()
            parentSgEntities = sgEntity.get('parents') if hasattr(sgEntity, 'get') else None
            setNames = [a['name'] for a in parentSgEntities] if parentSgEntities else []
            self.ui.inputWidget.update_input(self.entity.copy(), setNames)

    def refresh_status_tab(self):
        if self.entity:
            # self.ui.status_widget.firstTimeActivation = True
            self.ui.status_widget.refresh(entity=self.entity)

    def filter1_changed(self):
        filter1 = str(self.ui.typeEpisodeComboBox.currentText())
        if filter1:
            filter2List = self.relationData[filter1]
            self.populate_filter2(filter2List)

    def filter2_changed(self):
        self.show_filtered_entity()
        self.save_pref()

    def tag_selected(self):
        self.calculate_list()
        self.save_pref()

    def get_filter_entity(self):
        filter1Value = str(self.ui.typeEpisodeComboBox.currentText()) if self.ui.typeEpisodeCheckBox.isChecked() else ''
        filter2Value = str(self.ui.subtypeSeqComboBox.currentText()) if self.ui.subtypeSeqCheckBox.isChecked() else ''

        filter1 = (filter1Value, self.get_filter1_attr())
        filter2 = (filter2Value, self.get_filter2_attr())
        # filters
        sgEntities = sg_utils.filter_entities([self.sortedEntities[a] for a in self.sortedEntities], filter1, filter2)
        return sgEntities

    def show_filtered_entity(self, *args):
        self.populate_tags()
        self.calculate_list()


    def calculate_list(self, *args):
        """ filtering entities. refresh ui here """
        sgEntities = self.get_filter_entity()
        # tags
        sgEntities = self.filter_tags(sgEntities)
        # search key
        sgEntities = self.filter_search(sgEntities)
        self.list_entities(sgEntities)

    def filter_tags(self, sgEntities):
        selectedTags = self.ui.tagWidget.selected_items()
        filterEntities = []
        if selectedTags:
            filterTags = [a['name'] for a in selectedTags]
            allTags = True if Config.allTag in filterTags else False

            if allTags:
                return sgEntities

            for sgEntity in sgEntities:
                tags = sg_utils.get_entity_tags(sgEntity)
                names = [a for a in tags.keys()] if tags else []
                if any(tagKey in names for tagKey in filterTags):
                    filterEntities.append(sgEntity)

        return filterEntities if filterEntities else sgEntities

    def filter_search(self, sgEntities):
        key = str(self.ui.searchLineEdit.text())
        return [a for a in sgEntities if key.lower() in a['code'].lower()] if key else sgEntities

    def find_relation_data(self, sgEntities):
        mode = self.ui.modeComboBox.currentText()
        if mode == Config.asset:
            data = sg_utils.find_attr_relation(sgEntities, 'sg_asset_type', 'sg_subtype')
        if mode == Config.scene:
            data = sg_utils.find_attr_relation(sgEntities, 'sg_episode.Scene.code', 'sg_sequence_code')

        return data

    def set_icon_size(self):
        value = self.ui.sliderWidget.value()
        self.ui.entityWidget.set_icon_size([value, value])

    def rearange_list(self):
        value = self.ui.sliderWidget.value()
        self.ui.entityWidget.set_icon_size([value+1, value+1])
        self.ui.entityWidget.set_icon_size([value, value])

    def get_filter1_attr(self):
        mode = self.ui.modeComboBox.currentText()
        if mode == Config.asset:
            return 'sg_asset_type'
        if mode == Config.scene:
            return 'sg_episode.Scene.code'

    def get_filter2_attr(self):
        mode = self.ui.modeComboBox.currentText()
        if mode == Config.asset:
            return 'sg_subtype'
        if mode == Config.scene:
            return 'sg_sequence_code'

    def loading_display(self):
        self.ui.entityWidget.listWidget.clear()
        self.ui.entityWidget.listWidget.addItem('Loading ...')

    def no_item_display(self): 
        self.ui.entityWidget.display_empty_item()

    def lock_ui(self, state):
        self.ui.projectWidget.projectComboBox.setEnabled(not state)
        self.ui.modeComboBox.setEnabled(not state)

    def block_nav_signals(self, state):
        self.ui.stepWidget.blockSignals(state)
        self.ui.processWidget.blockSignals(state)
        self.ui.appWidget.blockSignals(state)

    def bucket_selected(self):
        self.ui.entityWidget.listWidget.clearSelection()

    # calculate workspace context
    def entity_selected(self, sgEntity):
        """ entity selected signal """
        # clear step, process, app
        self.ui.refBucketWidget.listWidget.clearSelection()
        if not sgEntity == self.ui.entityWidget.noItem: 
            self.block_nav_signals(True)
            self.ui.stepWidget.clear()
            self.ui.processWidget.clear()
            self.ui.appWidget.clear()
            self.ui.fileWidget.clear()
            self.ui.publishSpaceWidget.clear()
            self.ui.inputWidget.clear_widget()
            self.block_nav_signals(False)

            # browse step
            # no thread
            user = self.ui.userWidget.data()
            entity = sg_thread.update_entity(sgEntity)
            if user: 
                entity.context.update(user=user['sg_name'])
            self.set_step(entity)
            # send signal to look widget
            # self.ui.lookWidget.update_input(entity)
            self.ui.infoWidget.update_input(entity, sgEntity)
            # self.ui.buildWidget.update_input(entity)

            # refresh status tab
            self.ui.status_widget.entity = entity
            self.ui.status_widget.refresh_clicked()

            self.ui.refBucketWidget.entity = entity

            return
            # entityType = Config.mode[self.ui.modeComboBox.currentText()]
            # self.stepThread = sg_thread.UpdateEntity(sgEntity)
            # self.stepThread.value.connect(self.set_step)
            # self.stepThread.start()

    def step_changed(self, path):
        step = self.ui.stepWidget.current_text()
        self.entity.context.update(step=step)
        self.set_process(self.entity)
        self.save_pref()

    def process_changed(self, path):
        process = self.ui.processWidget.current_text()
        self.entity.context.update(process=process)
        self.set_app(self.entity)
        self.save_pref()

    def app_changed(self, path):
        app = self.ui.appWidget.current_text()
        self.entity.context.update(app=app)

        # refresh only active tab
        index = self.ui.tabWidget.currentIndex()
        self.tab_changed(index)

        # self.set_workspace(self.entity)
        # self.fetch_publish_workspace()
        # self.fetch_input()
        self.save_pref()

    def tab_changed(self, index):
        if self.ui.tabIndex[index] == self.ui.workspaceTab:
            self.set_workspace(self.entity)

        elif self.ui.tabIndex[index] == self.ui.publishTab:
            self.fetch_publish_workspace()

        elif self.ui.tabIndex[index] == self.ui.inputTab:
            self.fetch_input()

        # elif self.ui.tabIndex[index] == self.ui.statusTab:
        #     self.refresh_status_tab()

    def show_entity_menu(self, pos):
        sgEntity = self.ui.entityWidget.current_item() or dict()
        self.pos = pos
        menu = QtWidgets.QMenu(self)

        # show menu if item and self.entity value from thread are matched
        if self.entity and not sgEntity == self.ui.entityWidget.noItem:
            if sgEntity.get('code') == self.entity.name:

                if self.entity.entity_type == Config.asset:
                    if not sgEntity.get('sg_asset_type') in ['set']:
                        # add root menu
                        menu.addSeparator()
                        self.show_panel_menu(menu)
                        menu.addSeparator()
                        self.anim_rig_menu(menu)
                        self.lo_rig_menu(menu)
                        self.proxy_rig_menu(menu)
                        self.previs_rig_menu(menu)
                        self.miary_rig_menu(menu)

                        menu.addSeparator()
                        self.anim_loop_menu(menu)

                        menu.addSeparator()
                        self.gpu_pr_menu(menu)
                        self.gpu_md_menu(menu)

                        menu.addSeparator()
                        self.loop_menu(menu)

                    else:
                        # set menu
                        menu.addSeparator()
                        self.set_menu(menu)

                    menu.addSeparator()
                    self.add_admin_asset_menu(menu, sgEntity)

                if self.entity.entity_type == Config.scene:
                    menu.addSeparator()
                    self.show_panel_menu(menu)
                    menu.addSeparator()
                    self.add_admin_scene_menu(menu, sgEntity)

                menu.addSeparator()
                explorerMenu = menu.addAction('Open in Explorer')
                explorerMenu.triggered.connect(partial(self.open_explorer, self.entity))
                menu.popup(self.ui.entityWidget.listWidget.mapToGlobal(pos))
                self.pendingShowMenu = False
            # if not match, set pendingShowMenu = True and menu will be called again after thread entity finished
            else:
                self.pendingShowMenu = True

        else: 
            mode = self.ui.modeComboBox.currentText()
            if mode == Config.asset: 
                self.add_create_asset_menu(menu)

            if mode == Config.scene: 
                self.add_create_scene_menu(menu)

            menu.popup(self.ui.entityWidget.listWidget.mapToGlobal(pos))

    def show_panel_menu(self, menu): 
        panelMenu = menu.addAction(self.showPanel)
        panelMenu.triggered.connect(partial(self.show_panel, self.entity.copy()))

    def anim_rig_menu(self, menu):
        res = self.entity.projectInfo.asset.medium
        animMenu = menu.addAction(self.referenceAnimRig)
        rigFile = utils.get_rig(self.entity, res=res, absPath=True)
        rigFileRel = utils.get_rig(self.entity, res=res, absPath=False)
        animMenu.setEnabled(os.path.exists(rigFile))
        animMenu.triggered.connect(partial(self.create_reference, rigFileRel, res))

    def proxy_rig_menu(self, menu):
        res = self.entity.projectInfo.asset.proxy
        proxyMenu = menu.addAction(self.referenceAnimProxyRig)
        rigFile = utils.get_rig(self.entity, res=res, absPath=True)
        rigFileRel = utils.get_rig(self.entity, res=res, absPath=False)
        proxyMenu.setEnabled(os.path.exists(rigFile))
        proxyMenu.triggered.connect(partial(self.create_reference, rigFileRel, res))

    def previs_rig_menu(self, menu):
        res = self.entity.projectInfo.asset.previs
        if res:
            previsMenu = menu.addAction(self.referenceAnimPrevisRig)
            rigFile = utils.get_rig(self.entity, res=res, absPath=True)
            rigFileRel = utils.get_rig(self.entity, res=res, absPath=False)
            previsMenu.setEnabled(os.path.exists(rigFile))
            previsMenu.triggered.connect(partial(self.create_reference, rigFileRel, res))

    def lo_rig_menu(self, menu):
        res = self.entity.projectInfo.asset.lo
        if res:
            loMenu = menu.addAction(self.referenceAnimLoRig)
            rigFile = utils.get_rig(self.entity, res=res, absPath=True)
            rigFileRel = utils.get_rig(self.entity, res=res, absPath=False)
            loMenu.setEnabled(os.path.exists(rigFile))
            loMenu.triggered.connect(partial(self.create_reference, rigFileRel, res))

    def anim_loop_menu(self, menu): 
        res = self.entity.projectInfo.asset.medium
        # rigOaFile = utils.get_miarmy_oa(self.entity, res=res, absPath=True)
        # rigOaFileRel = utils.get_miarmy_oa(self.entity, res=res, absPath=False)

        reg = register_entity.Register(self.entity)
        actions = reg.get_processes('anim', res)
        if 'sgVersion' in actions:
            actions.remove('sgVersion')
        if None in actions:
            actions.remove(None)

        looks = reg.get_looks('lookdev', 'md')
        if None in looks:
            looks.remove(None)

        if not looks:
            looks = ['main']

        # reg.get_looks('anim', 'md')
        # reg.get_looks('lookdev', 'md')

        if actions:
            menu.addSeparator()
            animLoopMenu = QtWidgets.QMenu(self.importAnimLoop, self)
            menu.addMenu(animLoopMenu)
            for process in actions:
                processMenu = QtWidgets.QMenu(process, self)
                animLoopMenu.addMenu(processMenu)
                for look in looks:
                    animLoopFile = utils.get_anim_loop(self.entity, res=res, process=process, look=look, absPath=True)
                    crowdCardList = utils.get_crowd_card(self.entity, res='*', process=process, look=look, absPath=True)
                    if os.path.exists(animLoopFile):
                        fn = os.path.basename(animLoopFile)
                        animMenu = processMenu.addAction(fn)
                        # animMenu.setEnabled(os.path.exists(rigOaFile))
                        animMenu.triggered.connect(partial(self.create_crowd_loop, animLoopFile, res, look, process))
                    if crowdCardList:
                        for crowdCardFile in crowdCardList:
                            if os.path.exists(crowdCardFile):
                                fn = os.path.basename(crowdCardFile)
                                cardMenu = processMenu.addAction(fn)
                                # cardMenu.setEnabled(os.path.exists(rigOaFile))
                                cardMenu.triggered.connect(partial(self.create_crowd_card, crowdCardFile, res, look, process))

        # HAVE LOOK LEVELS
        # if actions:
        #     menu.addSeparator()
        #     animLoopMenu = QtWidgets.QMenu(self.importAnimLoop, self)
        #     menu.addMenu(animLoopMenu)
        #     for process in actions:
        #         processMenu = QtWidgets.QMenu(process, self)
        #         animLoopMenu.addMenu(processMenu)
        #         for look in looks:
        #             lookMenu = QtWidgets.QMenu(look, self)
        #             processMenu.addMenu(lookMenu)
        #             animLoopFile = utils.get_anim_loop(self.entity, res=res, process=process, look=look, absPath=True)
        #             if os.path.exists(animLoopFile):
        #                 fn = os.path.basename(animLoopFile)
        #                 animMenu = lookMenu.addAction(fn)
        #                 # animMenu.setEnabled(os.path.exists(rigOaFile))
        #                 animMenu.triggered.connect(partial(self.create_crowd_loop, animLoopFile, res, look, process))

        # if os.path.exists(rigOaFile): 
        #     menu.addSeparator()
        #     animMenu = menu.addAction(self.referenceMiarmyOA)
        #     animMenu.setEnabled(os.path.exists(rigOaFile))
        #     animMenu.triggered.connect(partial(self.create_miarmy_reference, rigOaFileRel, res))

    def miary_rig_menu(self, menu): 
        res = self.entity.projectInfo.asset.medium
        rigOaFile = utils.get_miarmy_oa(self.entity, res=res, absPath=True)
        rigOaFileRel = utils.get_miarmy_oa(self.entity, res=res, absPath=False)

        if os.path.exists(rigOaFile): 
            menu.addSeparator()
            animMenu = menu.addAction(self.referenceMiarmyOA)
            animMenu.setEnabled(os.path.exists(rigOaFile))
            animMenu.triggered.connect(partial(self.create_miarmy_reference, rigOaFileRel, res))

    def gpu_pr_menu(self, menu):
        gpuMenu = menu.addAction(self.referenceGpuPr)
        gpuFile = utils.get_gpu(self.entity, res='pr', absPath=True)
        gpuMenu.setEnabled(os.path.exists(gpuFile))
        gpuMenu.triggered.connect(partial(self.create_gpu, gpuFile, 'pr'))

    def gpu_md_menu(self, menu):
        gpuMenu = menu.addAction(self.referenceGpuMd)
        gpuFile = utils.get_gpu(self.entity, res='md', absPath=True)
        gpuMenu.setEnabled(os.path.exists(gpuFile))
        gpuMenu.triggered.connect(partial(self.create_gpu, gpuFile, 'md'))

    def set_menu(self, menu):
        res = self.entity.projectInfo.asset.medium
        setMenu = menu.addAction(self.referenceSet)
        setFile = utils.get_set(self.entity, res=res, absPath=True)
        setMenu.setEnabled(os.path.exists(setFile))
        setRigMenu = menu.addAction(self.referenceRigSet)
        setRigFile = utils.get_rig_set(self.entity, res=res, absPath=True)
        setRigMenu.setEnabled(os.path.exists(setRigFile))
        setMenu.triggered.connect(partial(self.create_set, setFile, res))
        setRigMenu.triggered.connect(partial(self.create_rig_set, setRigFile, res))

    def propit_menu(self, menu):
        propitMenu = menu.addAction(self.propItLabel)
        propitMenu.triggered.connect(self.propit)

    def autorig_menu(self, menu):
        autorigMenu = menu.addAction(self.autoRigLabel)
        autorigMenu.triggered.connect(self.autorig)

    def loop_menu(self, menu): 
        refData = utils.get_loop_data(self.entity)
        if os.path.exists(refData): 
            loopMenu = menu.addAction(self.referenceGroupAsset)
            loopMenu.triggered.connect(partial(self.create_reference_loop, refData))

    def add_admin_asset_menu(self, menu, sgEntity):
        adminMenu = QtWidgets.QMenu('Admin', self)
        menu.addMenu(adminMenu)

        createAssetMenu = adminMenu.addAction('Create Asset')
        createStructureMenu = adminMenu.addAction('Create Structure')
        addLookMenu = adminMenu.addAction('Add Look')

        createAssetMenu.triggered.connect(self.create_asset)
        createStructureMenu.triggered.connect(self.create_structure)
        addLookMenu.triggered.connect(partial(self.add_look, sgEntity))
        adminMenu.addSeparator()
        self.propit_menu(adminMenu)
        adminMenu.addSeparator()
        self.autorig_menu(adminMenu)

    def add_create_asset_menu(self, menu): 
        adminMenu = QtWidgets.QMenu('Admin', self)
        menu.addMenu(adminMenu)
        createAssetMenu = adminMenu.addAction('Create Asset')
        createAssetMenu.triggered.connect(self.create_asset)

    def add_admin_scene_menu(self, menu, sgEntity):
        adminMenu = QtWidgets.QMenu('Admin', self)
        menu.addMenu(adminMenu)

        createShotMenu = adminMenu.addAction('Create Shot')
        createStructureMenu = adminMenu.addAction('Create Structure')

        createShotMenu.triggered.connect(self.create_shot)
        createStructureMenu.triggered.connect(self.create_shot_structure)

    def add_create_scene_menu(self, menu): 
        adminMenu = QtWidgets.QMenu('Admin', self)
        menu.addMenu(adminMenu)
        createShotMenu = adminMenu.addAction('Create Shot')
        createShotMenu.triggered.connect(self.create_shot)

    def show_panel(self, entity): 
        # add tab to main window 
        if config.isMaya: 
            from rf_app.info_panel import maya_dock
            app = maya_dock.show(entity, True) 

        # else: 
        #     from rf_app.info_panel import main_ui
        #     reload(main_ui)
        #     tabs = [self.ui.tabWidget.tabText(a) for a in range(self.ui.tabWidget.count())]
        #     if not self.ui.panelTab in tabs: 
        #         self.panel = main_ui.MainUi(self.entity.copy())
        #         self.ui.tabWidget.addTab(self.panel, self.ui.panelTab)
        #         self.ui.tabIndex.append(self.ui.panelTab)
        #     else: 
        #         self.panel.initialize_context(self.entity.copy())


    def create_reference_loop(self, path): 
        refs = utils.read_loop_data(path)

        for ref in refs: 
            entity = context_info.ContextPathInfo(path=ref)
            self.create_reference(ref, 'md', entity)


    def create_reference(self, path, res, entity=None):
        entity = self.entity if not entity else entity
        look = self.ui.infoWidget.look()
        look = look if look else 'main'
        entity.context.update(res=res, look=look)
        # check for duplicated reference 
        allowReference = dcc_hook.duplicate_reference_warning(entity, path)
        defaultPose = dcc_hook.default_pose_choice(entity)
        
        if allowReference: 
            refs = dcc_hook.create_reference(entity, path, material=True, defaultPose=defaultPose)
            self.group_refs(entity, refs)
            subassets = subasset_utils.load_default_subasset(refs)
            for subasset in subassets:
                subassetGeo = ['{}:Rig_Grp'.format(subasset)]
                entity.context.update(entityGrp='prop')
                self.group_refs(entity, subassetGeo)
        else: 
            logger.info('Create reference %s cancelled' % path)

    def create_miarmy_reference(self, path, res, entity=None): 
        entity = self.entity if not entity else entity
        look = self.ui.infoWidget.look()
        look = look if look else 'main'
        entity.context.update(res=res, look=look)
        agents = dcc_hook.create_miarmyOa_reference(entity, path)

        if not agents: 
            namespace = dcc_hook.miarmy_oa_namespace(entity)
            dialog.MessageBox.warning('maya', 'Namespace %s exists' % namespace)
        
        if agents: 
            dcc_hook.group_oa(agents)

    def create_crowd_loop(self, path, res , look, process, entity=None): 
        entity = self.entity if not entity else entity
        entity.context.update(res=res, look=look)
        name = '%s_%s_%s_rsProxy' % (entity.name, process, look)
        namespace = dcc_hook.get_available_namespace(name)
        obj = dcc_hook.import_crowd_loop_file(path, namespace)
        geoGrp = '%s%s' % (namespace, 'Geo_Grp')
        entity.context.update(entityGrp='rsProxy')
        self.group_refs(entity, geoGrp)

    def create_crowd_card(self, path, res , look, process, entity=None): 
        entity = self.entity if not entity else entity
        entity.context.update(res=res, look=look)
        name = '%s_%s_%s_card' % (entity.name, process, look)
        namespace = dcc_hook.get_available_namespace(name)
        obj = dcc_hook.import_crowd_card_file(path, namespace)
        objCard = '%s' % (namespace[:-1])
        entity.context.update(entityGrp='rsProxy')
        self.group_refs(entity, objCard)

    def create_gpu(self, filename, res, group=False):
        from rf_app.asm import asm_lib

        name = asm_lib.asm_name(self.entity.name)
        self.entity.context.update(look='main')

        # create locator
        loc = asm_lib.MayaRepresentation(name, True)
        loc.set(filename, self.entity)
        # build gpu
        loc.build('gpu', res, 'setdress')
        loc.select()
        
        if group: 
            grp = self.entity.projectInfo.asset.shotdress_grp()
        
            # create group 
            dcc_hook.create_group(grp)
            loc.set_parent(grp)

    def create_set(self, filename, res):
        from rf_app.asm import asm_lib

        asset = context_info.ContextPathInfo(path=filename)
        asset.context.update(process='main', step='set', res='md')
        asm_lib.build_set(asset, buildAsset=True, mode='shot')

    def create_rig_set(self, filename, res): 
        from rf_maya.rftool.utils import maya_utils
        reload(maya_utils)
        maya_utils.import_reference_without_namespace(filename)

    def create_asset(self):
        context = context_info.Context()
        context.update(project=self.ui.projectWidget.current_project())
        asset = context_info.ContextPathInfo(context=context)
        assetDialog = create_entity_widget.CreateAsset.show(context=asset)

        sgEntity = assetDialog[1] if assetDialog else ''
        if sgEntity:
            self.cachesData[self.entityType][self.ui.projectWidget.current_project()][sgEntity['code']] = sgEntity
            self.show_filtered_entity()

    def create_structure(self):
        selection = self.ui.entityWidget.current_text()
        # asset
        project = str(self.entity.project)
        name = str(self.entity.name)
        assetType = str(self.entity.type)
        mainAsset = str(self.entity.parent)
        look = True
        create_asset.create_dir(project, name, assetType, mainAsset, look=look)

        # refresh
        self.show_filtered_entity()
        self.ui.entityWidget.set_current(selection)

    def add_look(self, sgEntity):
        """ add look """
        lookDialog = create_entity_widget.CreateLook.show(context=self.entity, sgHook=sg_process, entity=sgEntity)
        # update
        self.entity_selected(sgEntity)

    def group_refs(self, entity, objs):
        """ grouping reference into group """
        assetType = entity.type
        group = ''
        if assetType == 'char':
            group = entity.projectInfo.asset.char_grp()
        if assetType == 'prop':
            group = entity.projectInfo.asset.prop_grp()
        if assetType == 'set':
            group = entity.projectInfo.asset.set_grp()
        if assetType == 'rsProxy':
            group = entity.projectInfo.asset.crowd_rs_grp()
        if group:
            dcc_hook.group(group, objs)

    def propit(self):
        """ launch propit """
        from rf_app.propit2 import propit_app

        sgEntity = self.ui.entityWidget.current_item()
        propit_app.show(self.entity, sgEntity, self)

    def autorig(self): 
        import subprocess
        from rf_maya.rftool.utils import maya_utils
        from rf_maya.rftool.utils import pipeline_utils
        reload(pipeline_utils)
        from rf_app.propit2 import propit_cmd
        reload(propit_cmd)
        asset = self.entity.copy()
        import maya.cmds as mc
        # get environments for the project and use them in mayapy
        logger.info('Creating environments for mayapy...')
        version = mc.about(v=True)
        project = asset.project
        logger.info('Project: %s' %(project))
        logger.info('Version: %s' %(version))
        department = 'Py'
        res = 'md'

        if pipeline_utils.read_autoGeneratedRig_data(entity=asset): 
            logger.info('Creating rig ...')
            # # find model ABC
            asset.context.update(step='model', process='main', res=res)
            publishPath = asset.path.output_hero().abs_path()
            dataFile = asset.output(outputKey='cache')
            abcPath = '%s/%s' % (publishPath, dataFile)

            # merge environments with current environments
            envStr = asset.projectInfo.get_project_env(dcc='maya', dccVersion=version, department=department)

            # auto generate rig
            mayapy = maya_utils.pyinterpreter()
            genRigProcess = subprocess.Popen([mayapy, propit_cmd.__file__, '-i', abcPath, '-r', res], 
                               stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True, env=envStr)
            out, err = genRigProcess.communicate()

            # set asset data autoGenerateRig to True
            pipeline_utils.write_autoGeneratedRig_data(value=True, entity=asset) 
            logger.info('Finished')

        else: 
            logger.warning('Rig already created and Geometry is the latest.')

    def create_shot(self):
        context = context_info.Context()
        projectEntity = self.ui.projectWidget.current_item()
        episode = str(self.ui.typeEpisodeComboBox.currentText()) or ''
        
        if not projectEntity['sg_project_code']: 
            QtWidgets.QMessageBox.warning(self, 'Error', 'Please set project code in Shotgun first')
            return 

        context.update(project=projectEntity['name'], entityType=Config.scene, projectCode=projectEntity['sg_project_code'], entityGrp=episode)
        scene = context_info.ContextPathInfo(context=context)
        shotDialog = create_entity_widget.CreateShot.show(context=scene)

        sgEntity = shotDialog[1] if shotDialog else ''
        if sgEntity:
            self.cachesData[self.entityType][self.ui.projectWidget.current_project()][sgEntity['code']] = sgEntity
            # self.show_filtered_entity()
            self.mode_changed() # switch to mode changed to list episode if empty

    def create_shot_structure(self):
        selection = self.ui.entityWidget.current_text()
        # asset
        project = str(self.entity.project)
        name = str(self.entity.name)
        episode = str(self.entity.episode)
        sequence = str(self.entity.parent)

        create_scene.create_dir(project, name, episode, sequence)

        # refresh
        self.show_filtered_entity()
        self.ui.entityWidget.set_current(selection)


    def set_step(self, entity):
        # make entity global
        self.entity = entity
        self.entity.context.update(app=self.app)

        browseStepPath = os.path.split(entity.path.step().abs_path())[0]
        print 'browseStepPath', browseStepPath

        if os.path.exists(browseStepPath):
            # add ui
            self.ui.stepWidget.clear()
            self.ui.stepWidget.list_items(browseStepPath)

            lastSel = self.prefData.get('step')
            self.ui.stepWidget.set_current(lastSel) if not lastSel == None else None

        if self.pendingShowMenu:
            self.show_entity_menu(self.pos)

    def set_process(self, entity):
        browseProcessPath = os.path.split(entity.path.process().abs_path())[0]

        self.ui.processWidget.clear()
        self.ui.processWidget.list_items(browseProcessPath)

        lastSel = self.prefData.get('process')
        self.ui.processWidget.set_current(lastSel) if not lastSel == None else None

    def set_app(self, entity):
        browseAppPath = os.path.split(entity.path.app().abs_path())[0]

        self.ui.appWidget.clear()
        self.ui.appWidget.list_items(browseAppPath)

        lastSel = self.prefData.get('app') or 'maya'
        self.ui.appWidget.set_current(lastSel) if not lastSel == None else None

    def set_workspace(self, entity):
        browseWorkspace = entity.path.workspace().abs_path()
        self.ui.fileWidget.clear()
        self.ui.fileWidget.list_files(browseWorkspace, sortedVersion='top')
        self.preview_filename()


    def preview_filename(self):
        workspacePath = self.entity.path.workspace().abs_path()
        version = utils.calculate_version(workspacePath)
        self.entity.context.update(version=version)
        filename = self.entity.work_name(user=True)

        # default save name
        saveFileName = '%s/%s' % (workspacePath, filename)

        # preview name
        overrideNamePart = filename.split('.%s.' % version)[0] # hard coded
        fixName = filename.replace(overrideNamePart, '')

        # set preview
        self.ui.previewNameWidget.setText(overrideNamePart)
        self.ui.previewLabel.setText(fixName)


    def save_pref(self):
        project = self.ui.projectWidget.current_project()
        entityType = self.entityType
        typeEpisode = str(self.ui.typeEpisodeComboBox.currentText())
        subtypeSeq = str(self.ui.subtypeSeqComboBox.currentText())
        step = self.ui.stepWidget.current_text()

        prefDict = OrderedDict()
        prefDict['project'] = project
        prefDict['entityType'] = entityType or str()
        prefDict['typeEpisode'] = typeEpisode or str()
        prefDict['subtypeSeq'] = subtypeSeq or str()
        prefDict['step'] = step or str()
        prefDict['typeEpisodeStatus'] = self.ui.typeEpisodeCheckBox.isChecked()
        prefDict['subtypeSeqStatus'] = self.ui.subtypeSeqCheckBox.isChecked()
        prefDict['entity'] = self.ui.entityWidget.current_text()
        prefDict['tags'] = self.ui.tagWidget.selected_texts()

        self.pref.write(prefDict)


    def open_file(self):
        path = self.ui.fileWidget.current_data()
        loadNoReference = self.ui.unloadRefCheckBox.isChecked()
        startTime = datetime.now()
        if config.isMaya: 
            fixshade = self.ui.fixshadeCheckBox.isChecked()
            self.entity.write_env()
            sceneValid = True if self.entityType == Config.scene else False 
            result = dcc_hook.open_file(path, fixshade=fixshade, loadNoReference=loadNoReference)

        else: 
            # send to command port
            try: 
                cp_hook.open_file(path)
            except Exception as e: 
                dialog.MessageBox.warning('Error', 'No connection to Maya found. Please link maya first') 
                
        logger.info('Open file in %s' % (datetime.now() - startTime))


    def save_file(self):
        workspacePath = self.entity.path.workspace().abs_path()
        name = str(self.ui.previewNameWidget.text())
        fixName = str(self.ui.previewLabel.text())
        filename = '%s%s' % (name, fixName)
        saveFileName = '%s/%s' % (workspacePath, filename)

        if saveFileName:
            if config.isMaya: 
                # save as
                result = dcc_hook.save_file(saveFileName)
            else: 
                # send to command port 
                try: 
                    result = cp_hook.save_file(saveFileName)
                    # from rf_app.save_plus import save_utils
                    # save_utils.publish_data_scene_eva()
                except Exception as e: 
                    dialog.MessageBox.warning('Error', 'No connection to Maya found. Please link maya first')

        self.set_workspace(self.entity)
        
        # set initial parameters for light and lookdev dept #
        if self.entity.step in ["light", "lookdev"]:
            from rf_maya.rftool.preload import set_output_path
            reload(set_output_path)
            set_output_path.run()


    def open_as(self):
        """ open as workfile """
        # selected path
        publishFile = self.ui.publishSpaceWidget.listWidget.selected_file()

        if config.isMaya and publishFile:
            title = 'Open Publish File'
            message = 'Checkout as your work file?'
            result = QtWidgets.QMessageBox.question(self, title, message, QtWidgets.QMessageBox.Yes, QtWidgets.QMessageBox.No)

            if result == QtWidgets.QMessageBox.Yes:
                # get work file name
                workspacePath = self.entity.path.workspace().abs_path()
                version = utils.calculate_version(workspacePath)
                self.entity.context.update(version=version)
                filename = self.entity.work_name(user=True)
                workspaceFile = '%s/%s' % (workspacePath, filename)
                return dcc_hook.open_as(publishFile, workspaceFile)

    def reference_file(self): 
        """ this is being called by libraryButton """ 
        acceptExt = ['.abc', '.ma', '.mb']
        path = self.ui.infoWidget.hero_listWidget.selected_file()
        print path
        if os.path.splitext(path)[-1] in acceptExt: 
            res = self.guess_res(self.entity, path) or 'md'
            self.create_reference(path, res)

    def guess_res(self, entity, path): 
        allResDict = entity.projectInfo.asset.list_res()
        allRes = [v for k, v in allResDict.iteritems()]

        basename = os.path.basename(path)
        elems = basename.split('.')[0].split(context_info.ContextKey.nameSeparator)

        for res in allRes: 
            if any([res == a for a in elems]): 
                return res

    def lock_down_check(self): 
        return lockdown.check_status(self.entity, self.entity.step)

    def open_explorer(self, entity):
        entityPath = entity.path.step().abs_path()
        utils.open_explorer(entityPath)



class AssetMode(RFSGManager):
    """docstring for AssetMode"""
    def __init__(self, parent=None):
        super(AssetMode, self).__init__(parent=parent)
        self.setObjectName('%sAsset' % uiName)

    def set_default(self):
        self.ui.modeComboBox.setCurrentIndex(0)

class ShotMode(RFSGManager):
    """docstring for ShotMode"""
    def __init__(self, parent=None):
        super(ShotMode, self).__init__(parent=parent)
        self.setObjectName('%sShot' % uiName)

    def set_default(self):
        self.ui.modeComboBox.setCurrentIndex(1)


def show(mode='default'):
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        if mode == 'default':
            maya_win.deleteUI(uiName)
            myApp = RFSGManager(maya_win.getMayaWindow())
        if mode == 'asset':
            maya_win.deleteUI('%sAsset' % uiName)
            myApp = AssetMode(maya_win.getMayaWindow())
        if mode == 'shot':
            maya_win.deleteUI('%sShot' % uiName)
            myApp = ShotMode(maya_win.getMayaWindow())
        myApp.show()
        return myApp
    else:
        logger.info('Run in standalone\n')
        app = QtWidgets.QApplication.instance()
        if not app:
            app = QtWidgets.QApplication(sys.argv)
        if mode == 'default':
            myApp = RFSGManager()
        if mode == 'asset':
            myApp = AssetMode()
        if mode == 'shot':
            myApp = ShotMode()
        myApp.show()
        stylesheet.set_default(app)
        sys.exit(app.exec_())


if __name__ == '__main__':
    show()
