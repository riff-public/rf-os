import os
import sys
import traceback
from collections import OrderedDict

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_utils import icon
from rf_utils.widget import project_widget
from rf_utils.widget import file_comboBox
from rf_utils.widget import sg_entity_widget
from rf_utils.widget import ad_input_widget
from rf_utils.widget import auto_build_widget
from rf_utils.widget import look_widget
from rf_utils.widget import info_widget
from rf_utils.widget import sg_input_widget
from rf_utils.widget import file_widget
from rf_utils.widget import user_widget
from rf_utils.widget import publish_space_widget
from rf_utils.widget import process_widget
from rf_utils.sg import sg_utils
import dcc_hook
# reload(info_widget)
from rf_app.info_panel.tabs import tasks as info_panel_tasks
reload(info_panel_tasks)
from rf_utils.widget import reference_bucket_widget
reload(reference_bucket_widget)

class SGManagerCoreUI(QtWidgets.QWidget):
    status = QtCore.Signal(str)
    def __init__(self, parent=None) :
        super(SGManagerCoreUI, self).__init__(parent)

        # tab index
        self.infoTab = 'Info'
        self.workspaceTab = 'Workspace'
        self.recentTab = 'Recent'
        self.publishTab = 'Publish'
        self.inputTab = 'Input'
        self.lookTab = 'Look'
        self.autoTab = 'Auto Build'
        self.refTab = 'Ref Bucket'
        self.statusTab = 'Status'
        self.panelTab = 'RF Panel'
        self.tabIndex = [self.infoTab, self.workspaceTab, self.publishTab, self.inputTab, self.statusTab, self.autoTab]
        # self.tabIndex = [self.infoTab, self.workspaceTab, self.recentTab, self.publishTab, self.inputTab, self.statusTab, self.autoTab]

        # main layout
        self.allLayout = QtWidgets.QVBoxLayout()
        # header
        self.add_title_layout()
        self.add_project_widget()
        self.add_user_widget()
        self.add_logo()
        self.add_line()
        self.set_title_layout()

        self.add_category_widget()

        self.add_library_widget()
        self.add_workspace_widget()
        self.set_category_layout()
        self.set_all_layout()
        self.setLayout(self.allLayout)
        self.set_default()

    def add_title_layout(self):
        self.titleLayout = QtWidgets.QHBoxLayout()
        self.allLayout.addLayout(self.titleLayout)

    def add_logo(self):
        # set logo widget
        spacerItemH = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.titleLayout.addItem(spacerItemH)

        self.logo = QtWidgets.QLabel()
        self.logo.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.titleLayout.addWidget(self.logo)
        self.logo.setPixmap(QtGui.QPixmap(icon.logo).scaled(64, 64, QtCore.Qt.KeepAspectRatio))

    def add_line(self):
        """ set line to layout """
        self.lineHeader = self.line(self.allLayout)

    def set_title_layout(self):
        self.titleLayout.setStretch(0, 0) # project
        self.titleLayout.setStretch(1, 0) # user
        self.titleLayout.setStretch(2, 2) # spacer
        self.titleLayout.setStretch(3, 0) # logo

        # 2 cols layout - library, workspace
    def add_category_widget(self):
        self.category_container = QtWidgets.QWidget()
        self.categoryLayout = QtWidgets.QHBoxLayout(self.category_container)
        self.categoryLayout.setContentsMargins(0, 0, 0, 0)

        self.splitter2 = QtWidgets.QSplitter(self.category_container)
        # self.allLayout.addLayout(self.categoryLayout)
        self.categoryLayout.addWidget(self.splitter2)
        self.allLayout.addWidget(self.category_container)

    def add_library_widget(self):
        """ entity listwidget """
        # layout
        self.library_container = QtWidgets.QWidget(self.splitter2)
        self.libraryLayout = QtWidgets.QVBoxLayout(self.library_container)
        self.libraryLayout.setContentsMargins(0, 0, 0, 0)
        # self.categoryLayout.addLayout(self.libraryLayout)
        # self.categoryLayout.addWidget(self.library_container)
        self.entityList_container = QtWidgets.QWidget()
        self.entityListLayout = QtWidgets.QHBoxLayout(self.entityList_container)

        self.splitter = QtWidgets.QSplitter(self.entityList_container)


        # widgets
        self.add_navigation_widget()
        self.line(self.libraryLayout)
        self.add_search_widget()
        self.add_view_mode()
        self.add_tag_widget()
        self.add_entity_widget()
        self.libraryLayout.addWidget(self.splitter)
        self.add_library_button()
        self.add_slider_widget()
        self.set_navigation_layout()

        # set splitter ratio 
        self.splitter.setStretchFactor(0, 1)
        self.splitter.setStretchFactor(1, 4)

    def add_workspace_widget(self):
        # file
        self.workspace_container = QtWidgets.QWidget(self.splitter2)
        self.workspaceLayout = QtWidgets.QVBoxLayout(self.workspace_container)
        self.workspaceLayout.setContentsMargins(0, 0, 0, 0)

        self.splitter2.setStretchFactor(0, 25)
        self.splitter2.setStretchFactor(1, 30)

        # add sublayout
        self.workspaceSubLayout = QtWidgets.QHBoxLayout()
        self.workspaceLayout.addLayout(self.workspaceSubLayout)

        self.add_process_widget(self.workspaceSubLayout)
        self.add_app_widget(self.workspaceSubLayout)

        # tab
        self.add_tab()
        # self.categoryLayout.addLayout(self.workspaceLayout)
        # self.categoryLayout.addWidget(self.workspace_container)

    def add_tab(self):
        self.tabWidget = QtWidgets.QTabWidget()
        self.workspaceLayout.addWidget(self.tabWidget)

        self.add_tab5()
        # self.add_tab4()
        self.add_tab1()
        self.add_tab2()
        self.add_tab3()
        # self.add_tab7()
        self.add_tab8()
        self.add_tab6()

    def add_tab1(self):
        # tab 1
        self.fileWidgetWindow = QtWidgets.QWidget()
        self.fileTabLayout = QtWidgets.QVBoxLayout(self.fileWidgetWindow)

        self.tabWidget.addTab(self.fileWidgetWindow, self.workspaceTab)

        # add widgets
        self.add_file_widget(self.fileTabLayout)
        self.add_name_widget(self.fileTabLayout)
        self.add_file_buttons(self.fileTabLayout)

    def add_tab2(self):
        # tab 2
        self.publishFileWindow = QtWidgets.QWidget()
        self.publishFileTabLayout = QtWidgets.QVBoxLayout(self.publishFileWindow)

        self.tabWidget.addTab(self.publishFileWindow, self.publishTab)

        self.publishSpaceWidget = publish_space_widget.ViewPublishSpace2()
        self.openAsButton = QtWidgets.QPushButton('Open as workfile')

        self.publishFileTabLayout.addWidget(self.publishSpaceWidget)
        self.publishFileTabLayout.addWidget(self.openAsButton)

    def add_tab3(self):
        # tab 3
        self.inputWindow = QtWidgets.QWidget()
        self.inputTabLayout = QtWidgets.QVBoxLayout(self.inputWindow)

        self.tabWidget.addTab(self.inputWindow, self.inputTab)

        # self.sgInputWidget = sg_input_widget.SGInputWidget(sg=sg_utils.sg, dccHook=dcc_hook)
        self.inputWidget = ad_input_widget.InputWidget(dccHook=dcc_hook)

        self.inputTabLayout.addWidget(self.inputWidget)

    def add_tab4(self):
        """ look widget """
        # tab 3
        self.lookWindow = QtWidgets.QWidget()
        self.lookLayout = QtWidgets.QVBoxLayout(self.lookWindow)

        self.tabWidget.addTab(self.lookWindow, self.lookTab)

        self.lookWidget = look_widget.LookWidgetLibrary(dccHook=dcc_hook)
        self.lookLayout.addWidget(self.lookWidget)

    def add_tab5(self):
        """ info widget """
        # tab 3
        self.infoWindow = QtWidgets.QWidget()
        self.infoLayout = QtWidgets.QVBoxLayout(self.infoWindow)

        self.tabWidget.addTab(self.infoWindow, self.infoTab)

        self.infoWidget = info_widget.AssetInfoWidget(dccHook=dcc_hook)
        self.infoLayout.addWidget(self.infoWidget)

    def add_tab7(self):
        """ info widget """
        # tab 3
        self.autoBuildWindow = QtWidgets.QWidget()
        self.autoBuildLayout = QtWidgets.QVBoxLayout(self.autoBuildWindow)

        self.tabWidget.addTab(self.autoBuildWindow, self.autoTab)

        self.buildWidget = auto_build_widget.AutoBuildWidget(dccHook=dcc_hook)
        self.autoBuildLayout.addWidget(self.buildWidget)

    def add_tab8(self):
        """ Ref Bucket widget """
        # tab 8
        self.refBucketWindow = QtWidgets.QWidget()
        self.refBucketLayout = QtWidgets.QVBoxLayout(self.refBucketWindow)

        self.tabWidget.addTab(self.refBucketWindow, self.refTab)

        self.refBucketWidget = reference_bucket_widget.ReferenceBucketUi()
        self.refBucketLayout.addWidget(self.refBucketWidget)

    def add_tab6(self):
        self.status_widget = info_panel_tasks.Ui(parent=self)
        self.tabWidget.addTab(self.status_widget, self.statusTab)

    def add_navigation_widget(self):
        # nav
        self.navigationLayout = QtWidgets.QHBoxLayout()
        self.libraryLayout.addLayout(self.navigationLayout)

        self.add_mode()
        self.add_type_episode_widget()
        self.add_subtype_seq_widget()
        self.line(self.navigationLayout, 'v')
        self.add_step_widget(self.navigationLayout)
        self.add_workspace_checkbox()

        # add spacer
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.navigationLayout.addItem(spacerItem)

    def add_search_widget(self):
        self.searchLayout = QtWidgets.QHBoxLayout()
        self.libraryLayout.addLayout(self.searchLayout)

        self.searchLineEdit = QtWidgets.QLineEdit()
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        self.searchLayout.addWidget(self.searchLineEdit)
        self.searchLayout.addItem(spacerItem)

        self.searchLayout.setStretch(0, 2)
        self.searchLayout.setStretch(1, 6)

    def add_view_mode(self):
        self.listRadioButton = QtWidgets.QRadioButton('List')
        self.iconRadioButton = QtWidgets.QRadioButton('Icon')
        self.textRadioButton = QtWidgets.QRadioButton('Text')
        self.searchLayout.addWidget(self.textRadioButton)
        self.searchLayout.addWidget(self.listRadioButton)
        self.searchLayout.addWidget(self.iconRadioButton)

        self.searchLayout.setStretch(2, 1)
        self.searchLayout.setStretch(3, 1)

    def add_tag_widget(self):
        self.tagWidget = sg_entity_widget.TagListWidget(self.splitter)
        self.tagWidget.listWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        # self.entityListLayout.addWidget(self.tagWidget)
        # self.libraryLayout.addLayout(self.entityListLayout)

    def add_entity_widget(self):
        self.entityWidget = sg_entity_widget.EntityListWidget(self.splitter)
        self.entityWidget.listWidget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.entityWidget.set_icon_mode()
        # self.entityListLayout.addWidget(self.entityWidget)

        self.entityListLayout.setStretch(0, 1)
        self.entityListLayout.setStretch(1, 4)

    def add_library_button(self):
        # layout
        self.buttonLayout = QtWidgets.QHBoxLayout()
        self.libraryButton = QtWidgets.QPushButton('Reference')
        self.libraryButton.setMinimumSize(QtCore.QSize(0, 30))

        # spacer
        # spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        # self.buttonLayout.addItem(spacerItem)
        self.libraryLayout.addLayout(self.buttonLayout)
        self.buttonLayout.addWidget(self.libraryButton)

        self.buttonLayout.setStretch(0, 0)
        self.buttonLayout.setStretch(1, 2)
        self.buttonLayout.setStretch(2, 1)

    def add_slider_widget(self):
        self.sliderLabel = QtWidgets.QLabel('Size: ')
        self.sliderWidget = QtWidgets.QSlider()
        self.sliderWidget.setOrientation(QtCore.Qt.Horizontal)

        self.buttonLayout.addWidget(self.sliderLabel)
        self.buttonLayout.addWidget(self.sliderWidget)

    def add_project_widget(self):
        # add project
        self.projectLayout = QtWidgets.QVBoxLayout()
        self.titleLayout.addLayout(self.projectLayout)

        self.projectWidget = project_widget.SGProjectComboBox(sg=sg_utils.sg, layout=QtWidgets.QHBoxLayout())
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        self.projectLayout.addItem(spacerItem)
        self.projectLayout.addWidget(self.projectWidget)

        # layout
        # self.projectWidget.projectComboBox.setMinimumSize(QtCore.QSize(140, 0))
        self.projectWidget.allLayout.setSpacing(8)
        self.projectWidget.allLayout.setStretch(0, 1)
        self.projectWidget.allLayout.setStretch(1, 2)

    def add_user_widget(self):
        self.userLayout = QtWidgets.QVBoxLayout()
        self.titleLayout.addLayout(self.userLayout)

        self.userWidget = user_widget.UserComboBox()
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        # self.userWidget.comboBox.setMinimumSize(QtCore.QSize(100, 0))

        self.userLayout.addItem(spacerItem)
        self.userLayout.addWidget(self.userWidget)

    def add_mode(self):
        self.modeLabel = QtWidgets.QLabel('Mode :')
        self.modeComboBox = QtWidgets.QComboBox()
        self.modeLabel.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)

        self.navigationLayout.addWidget(self.modeLabel)
        self.navigationLayout.addWidget(self.modeComboBox)

    def add_type_episode_widget(self):
        self.typeEpisodeCheckBox = QtWidgets.QCheckBox('Type :')
        self.typeEpisodeComboBox = QtWidgets.QComboBox()
        # self.typeEpisodeCheckBox.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)

        self.navigationLayout.addWidget(self.typeEpisodeCheckBox)
        self.navigationLayout.addWidget(self.typeEpisodeComboBox)

    def add_subtype_seq_widget(self):
        self.subtypeSeqCheckBox = QtWidgets.QCheckBox('Subtype :')
        self.subtypeSeqComboBox = QtWidgets.QComboBox()
        # self.subtypeSeqCheckBox.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)

        self.navigationLayout.addWidget(self.subtypeSeqCheckBox)
        self.navigationLayout.addWidget(self.subtypeSeqComboBox)

    def add_workspace_checkbox(self):
        # add checkBox
        self.workspaceCheckBox = QtWidgets.QCheckBox('Workspace')
        self.navigationLayout.addWidget(self.workspaceCheckBox)

    def add_step_widget(self, layout):
        self.stepWidget = file_comboBox.FileComboBox(layout=QtWidgets.QHBoxLayout())
        self.stepWidget.set_label('Department : ')
        # self.stepWidget.label.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        layout.addWidget(self.stepWidget)
        self.stepWidget.comboBox.setMinimumSize(QtCore.QSize(100, 0))

    def add_process_widget(self, layout):
        self.processWidget = process_widget.ProcessWidget()
        # self.processWidget = file_comboBox.FileComboBox(layout=QtWidgets.QHBoxLayout())
        # self.processWidget.set_label('Process : ')
        # self.addProcessButton = QtWidgets.QPushButton('+')
        # self.addProcessButton.setMaximumSize(QtCore.QSize(20, 18))
        # self.processWidget.label.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        layout.addWidget(self.processWidget)
        # layout.addWidget(self.addProcessButton)

    def add_app_widget(self, layout):
        self.appWidget = file_comboBox.FileComboBox(layout=QtWidgets.QHBoxLayout())
        self.appWidget.set_label('App : ')
        self.appWidget.label.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        layout.addWidget(self.appWidget)

    def add_file_widget(self, layout):
        self.fileWidget = file_widget.FileListWidgetExtended()
        layout.addWidget(self.fileWidget)

    def add_name_widget(self, layout):
        self.optionLayout = QtWidgets.QHBoxLayout()
        self.nameLayout = QtWidgets.QHBoxLayout()

        self.overrideCheckBox = QtWidgets.QCheckBox('Override Filename')
        self.fixshadeCheckBox = QtWidgets.QCheckBox('Auto fix shade')
        self.unloadRefCheckBox = QtWidgets.QCheckBox('Unload Reference')
        self.previewNameWidget = QtWidgets.QLineEdit()
        self.previewLabel = QtWidgets.QLabel()

        self.optionLayout.addWidget(self.overrideCheckBox)
        self.optionLayout.addWidget(self.fixshadeCheckBox)
        layout.addWidget(self.unloadRefCheckBox)
        self.nameLayout.addWidget(self.previewNameWidget)
        self.nameLayout.addWidget(self.previewLabel)
        layout.addLayout(self.optionLayout)
        layout.addLayout(self.nameLayout)

        self.nameLayout.setStretch(0, 3)
        self.nameLayout.setStretch(1, 1)

    def add_file_buttons(self, layout):
        self.openButton = QtWidgets.QPushButton('Open')
        self.saveButton = QtWidgets.QPushButton('Save ++')

        layout.addWidget(self.openButton)
        layout.addWidget(self.saveButton)

    def set_navigation_layout(self):
        self.navigationLayout.setSpacing(8)

        self.navigationLayout.setStretch(0, 1) # mode label
        self.navigationLayout.setStretch(1, 2) # mode
        self.navigationLayout.setStretch(2, 1) # type label
        self.navigationLayout.setStretch(3, 2) # type
        self.navigationLayout.setStretch(4, 1) # subtype label
        self.navigationLayout.setStretch(5, 2) # subtype
        self.navigationLayout.setStretch(6, 0) # line
        self.navigationLayout.setStretch(7, 1) # department
        self.navigationLayout.setStretch(8, 4) # spacer

    def set_category_layout(self):
        self.categoryLayout.setStretch(0, 8) # widget
        self.categoryLayout.setStretch(1, 3) # tab

    def set_all_layout(self): 
        self.allLayout.setStretch(0, 0)
        self.allLayout.setStretch(1, 0)
        self.allLayout.setStretch(2, 1)


    def line(self, layout, direction='h', parent=None):
        """ line """
        line = QtWidgets.QFrame(parent)
        line.setFrameShape(QtWidgets.QFrame.HLine) if direction == 'h' else line.setFrameShape(QtWidgets.QFrame.VLine)
        line.setFrameShadow(QtWidgets.QFrame.Sunken)
        layout.addWidget(line)
        return line

    def set_default(self):
        self.libraryButton.setVisible(False)
        self.workspaceCheckBox.setChecked(False)
        self.entityWidget.set_icon_size([128, 128])

        self.typeEpisodeCheckBox.setChecked(True)
        self.typeEpisodeComboBox.setEnabled(True)
        self.subtypeSeqComboBox.setEnabled(False)

        self.fixshadeCheckBox.setChecked(False)

        # default mode
        self.textRadioButton.setChecked(True)

        # set slider range
        self.sliderWidget.setMinimum(60)
        self.sliderWidget.setMaximum(200)
        self.sliderWidget.setSingleStep(10)
        self.sliderWidget.setValue(128)

        self.previewNameWidget.setEnabled(False)


class SGManagerUI(SGManagerCoreUI):
    """docstring for SGManagerUI"""
    def __init__(self, parent=None):
        super(SGManagerUI, self).__init__(parent=parent)
        self.init_signals()

    def init_signals(self):
        # self.workspaceCheckBox.stateChanged.connect(self.show_workspace)
        self.typeEpisodeCheckBox.stateChanged.connect(self.filter1_changed)
        self.subtypeSeqCheckBox.stateChanged.connect(self.filter2_changed)

        self.overrideCheckBox.stateChanged.connect(self.name_enable)

    def show_workspace(self, var):
        state = True if var else False

        # tab visibility
        self.tabWidget.setVisible(state)
        # library button visibility
        self.libraryButton.setVisible(not state)
        # process
        self.processWidget.setVisible(state)
        # app
        self.appWidget.setVisible(state)

        # show workspace
        if state:
            self.categoryLayout.setStretch(1, 1)
            self.navigationLayout.setStretch(8, 4)
        else:
            self.categoryLayout.setStretch(1, 0)
            self.navigationLayout.setStretch(8, 12)

    def filter1_changed(self, value):
        state = True if value else False
        self.typeEpisodeComboBox.setEnabled(state)

    def filter2_changed(self, value):
        state = True if value else False
        self.subtypeSeqComboBox.setEnabled(state)

    def name_enable(self, value):
        state = True if value else False
        self.previewNameWidget.setEnabled(state)



