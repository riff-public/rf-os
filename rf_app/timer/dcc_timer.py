import os
import sys
import traceback
import logging
from datetime import datetime
import time
is_python3 = True if sys.version_info[0] > 2 else False
if is_python3:
    from importlib import reload

from Qt import QtWidgets
from Qt import QtCore
from Qt import QtGui
from rf_utils import thread_pool
from rf_utils import time_utils
from rf_utils import file_utils
from . import modules
reload(modules)
reload(time_utils)

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())


class Color:
    label = 'color: rgb(140, 180, 200);'
    active = 'color: rgb(100, 200, 100);'
    idle = 'color: rgb(220, 200, 100);'
    grey = 'color: rgb(100, 100, 100);'


class TimerWidget(QtWidgets.QWidget):
    """docstring for TimerWidget"""
    def __init__(self, dcc='', parent=None):
        super(TimerWidget, self).__init__(parent=parent)
        self.threadpool = QtCore.QThreadPool()
        self.threadpool.setMaxThreadCount(1)
        self.dcc = dcc
        self.framework_running = True
        self.count_duration = True
        self.is_active = True
        self.interval = 1  # update timer every 1 second
        self.idle_limit = 300  # 10 minutes
        self.refresh_interval = 10  # execute jobs every 10 seconds
        self.global_duration = int(datetime.now().strftime('%S'))
        self.current_project = os.environ.get('ACTIVE_PROJECT', 'default')
        self.startTime = datetime.now()
        self.config_data = self.read_config()
        self.module_dict = self.load_modules()

        self.setup_ui()
        self.init_signals()
        self.set_default()
        self.start_timer()

    def setup_ui(self):
        self.layout = QtWidgets.QGridLayout()
        self.time_label = QtWidgets.QLabel('Time : ')
        self.elapse_label = QtWidgets.QLabel('Elapsed : ')
        self.current_time_label = QtWidgets.QLabel('')
        self.elapse_time_label = QtWidgets.QLabel('')
        self.status_label = QtWidgets.QLabel('Active')
        self.status_time_label = QtWidgets.QLabel('')

        self.time_label.setStyleSheet(Color.label)
        self.elapse_label.setStyleSheet(Color.label)
        self.status_label.setStyleSheet(Color.active)
        self.status_time_label.setStyleSheet(Color.grey)
        font = QtGui.QFont()
        font.setPointSize(12)
        # font.setBold(True)
        self.time_label.setFont(font)
        self.elapse_label.setFont(font)

        self.start_stop_button = QtWidgets.QPushButton('Stop')
        self.setting_button = QtWidgets.QPushButton('Setting')

        self.layout.addWidget(self.time_label, 0, 0)
        self.layout.addWidget(self.elapse_label, 1, 0)
        self.layout.addWidget(self.status_label, 2, 0)
        self.layout.addWidget(self.current_time_label, 0, 1)
        self.layout.addWidget(self.elapse_time_label, 1, 1)
        self.layout.addWidget(self.status_time_label, 2, 1)
        self.layout.addWidget(self.start_stop_button, 2, 0)
        self.layout.addWidget(self.setting_button, 2, 1)
        self.layout.setColumnStretch(0, 0)
        self.layout.setColumnStretch(1, 1)

        self.setLayout(self.layout)

    def init_signals(self):
        self.start_stop_button.clicked.connect(self.start_stop_timer)

    def set_default(self):
        self.start_stop_button.setVisible(False)
        self.setting_button.setVisible(False)

    def read_config(self):
        config_path = '{}/config.yml'.format(os.path.dirname(__file__).replace('\\', '/'))
        self.config_data = file_utils.ymlLoader(config_path)
        return self.config_data

    def get_project_config(self):
        """ get config data by project """
        if self.current_project not in self.config_data.keys():
            self.current_project = 'default'
        data = self.config_data[self.dcc][self.current_project]
        return data

    def load_modules(self):
        """ read all modules name """
        module_dict = modules.get_module_data()
        return module_dict

    def run(self, current_time, duration):
        self.run_interval_func(duration)
        self.run_specific_time_func(current_time)

    def run_interval_func(self, duration):
        """ run interval functions """
        data = self.get_project_config()
        interval = data.get('interval')

        if interval: 
            # round last digit for time error
            if not (duration % self.refresh_interval) == 0:
                # print 'round', duration
                duration = duration - (duration % self.refresh_interval)

            for intv, mods in interval.items():
                # run every x second
                # print duration, int(intv), (duration % int(intv))
                if (duration % int(intv)) == 0:
                    for mod in mods:
                        if mod in self.module_dict.keys():
                            # logger.debug('Execute {} every {}'.format(mod, intv))
                            func = self.module_dict[mod]
                            self.run_function(func)
                else:
                    # logger.debug('duration not matched {}, {}'.format(duration, intv))
                    pass

    def run_specific_time_func(self, current_time):
        """ run specific time function """
        data = self.get_project_config()
        specific_time = data.get('time')

        if specific_time: 
            current_time_string = current_time.strftime('%H:%M:%S')

            for exectime, mods in specific_time.items():
                # run at specific time
                if current_time_string == exectime:
                    for mod in mods:
                        if mod in self.module_dict.keys():
                            # logger.debug('Execute {} at {}'.format(mod, current_time_string))
                            func = self.module_dict[mod]
                            self.run_function(func)
                else:
                    # logger.debug('time not matched {} {}'.format(current_time_string, exectime))
                    pass

    def run_function(self, func):
        """ execute parse function """
        try:
            logger.debug('Execute {} '.format(func.__name__))
            func.run()
            # logger.debug('Finished')
        except Exception as e:
            error = traceback.format_exc()
            traceback.print_exc()
            logger.error(error)

    def start_timer(self):
        """ start timer threading """
        self.worker = thread_pool.Worker(self.run_timer)
        self.worker.kwargs['loop_callback'] = self.worker.signals.loopResult
        self.worker.signals.loopResult.connect(self.time_trigger)
        self.threadpool.start(self.worker)

    def run_timer(self, **kwargs):
        """ main looping framework """
        loopResult = kwargs['loop_callback']
        seconds = 0
        while self.framework_running:
            self.current_time = datetime.now()
            if self.count_duration:
                seconds += self.interval
            loopResult.emit((self.current_time, self.global_duration, seconds))
            self.global_duration += self.interval
            time.sleep(self.interval) # sleep for 1 second

    def time_trigger(self, result):
        """ run every x second determined by self.interval """
        current_time, global_duration, seconds = result
        self.set_display(current_time, seconds)
        current_sec = int(current_time.strftime('%S'))
        self.check_idle()

        # run every x seconds from self.refresh_interval
        # to avoid too many loads, do not set refresh_interval less than 10
        if (current_sec % self.refresh_interval) == 0:
            # logger.debug('Refreshing ...')
            # print datetime.now(), 'refresh ...'
            self.run(current_time, global_duration)

    def set_display(self, current_time, seconds):
        """ display main clock """
        diff_string = time_utils.seconds_to_hms(0, seconds)
        self.current_time_label.setText(time_utils.time_to_str(current_time))
        self.elapse_time_label.setText(diff_string)

    def start_stop_timer(self):
        """ start / stop counting duration """
        self.count_duration = not self.count_duration
        if self.count_duration:
            self.start_stop_button.setText('Stop')
        else:
            self.start_stop_button.setText('Start')

    def check_idle(self):
        from rf_app.time_logged import interval_record
        seconds = interval_record.last_idle()

        if seconds <= self.idle_limit:
            self.set_active(seconds)
            interval_record.set_idle_status(True)
        else:
            self.set_idle(seconds - self.idle_limit + 1)
            interval_record.set_idle_status(False)

    def set_active(self, seconds):
        remain_seconds = self.idle_limit - seconds
        self.status_label.setText('Active')
        self.status_label.setStyleSheet(Color.active)
        time_string = time_utils.seconds_to_hms(0, remain_seconds)
        self.status_time_label.setText('{}   Time to idle'.format(time_string))
        self.is_active = True

    def set_idle(self, seconds):
        self.status_label.setText('Idle')
        self.status_label.setStyleSheet(Color.idle)
        time_string = time_utils.seconds_to_hms(0, seconds)
        self.status_time_label.setText('{}   Idle time'.format(time_string))
        self.is_active = False

    def stop_timer(self): 
        self.worker.signals.blockSignals(True)
        self.threadpool.deleteLater()
        self.framework_running = False
