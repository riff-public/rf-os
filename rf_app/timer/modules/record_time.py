import os
import sys
from rf_app.time_logged import auto_interval_record


def run():
    auto_interval_record.run_thread(duration=15)
