# import all modules in maya_lib 
import os
import sys 
import glob
from rf_app.export.asset_lib import *
from collections import OrderedDict

is_python3 = True if sys.version_info[0] > 2 else False
if is_python3:
    from importlib import reload, import_module
    
def get_module(): 
    modules = glob.glob(os.path.dirname(__file__)+"/*.py")
    return [ os.path.basename(f)[:-3] for f in modules if os.path.isfile(f) and not f.endswith('__init__.py')]

def get_module_data(): 
    mods = OrderedDict()
    allModules = get_module()
    
    for name in allModules: 
        print('name', name)
        if is_python3:
            func = import_module('.'+name, 'rf_app.timer.modules')
        else:
            func = __import__(name, globals(), locals(), [], -1)
        reload(func)
        mods[name] = func

    return mods

def get_func(name): 
    func = __import__(name, globals(), locals(), [], -1)
    return func
