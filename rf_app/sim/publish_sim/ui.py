import os
import sys

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
from rf_utils import icon
from rf_qc import qc_widget
reload(qc_widget)

class SimPublishUI(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(SimPublishUI, self).__init__(parent)

        self.verticalLayout_5 = QtWidgets.QVBoxLayout()
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.label_6 = QtWidgets.QLabel("project")
        self.horizontalLayout_4.addWidget(self.label_6)
        self.project_lineEdit = QtWidgets.QLineEdit()
        self.project_lineEdit.setEnabled(False)
        self.horizontalLayout_4.addWidget(self.project_lineEdit)
        self.label_7 = QtWidgets.QLabel("episode")
        self.horizontalLayout_4.addWidget(self.label_7)
        self.ep_lineEdit = QtWidgets.QLineEdit()
        self.ep_lineEdit.setEnabled(False)
        self.horizontalLayout_4.addWidget(self.ep_lineEdit)
        self.label_8 = QtWidgets.QLabel("seq_shot")
        self.horizontalLayout_4.addWidget(self.label_8)
        self.seq_lineEdit = QtWidgets.QLineEdit()
        self.seq_lineEdit.setEnabled(False)
        self.horizontalLayout_4.addWidget(self.seq_lineEdit)
        self.label_9 = QtWidgets.QLabel("namspace")
        self.horizontalLayout_4.addWidget(self.label_9)
        self.namespace_lineEdit = QtWidgets.QLineEdit()
        self.namespace_lineEdit.setEnabled(False)
        self.horizontalLayout_4.addWidget(self.namespace_lineEdit)
        self.label_10 = QtWidgets.QLabel("Type")
        self.horizontalLayout_4.addWidget(self.label_10)
        self.type_comboBox = QtWidgets.QComboBox()
        self.type_comboBox.addItem("Geo")
        self.type_comboBox.addItem("Yeti")
        self.type_comboBox.addItem("Corrective")
        self.horizontalLayout_4.addWidget(self.type_comboBox)
        self.verticalLayout.addLayout(self.horizontalLayout_4)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.verticalLayout_4 = QtWidgets.QVBoxLayout()
        # self.qc_listWidget = QtWidgets.QListWidget()
        self.qc_listWidget = qc_widget.QcWidget()
        # self.verticalLayout_4.addWidget(self.qc_listWidget)
        self.qc_push = QtWidgets.QPushButton("Check QC")
        # self.verticalLayout_4.addWidget(self.qc_push)
        self.horizontalLayout.addLayout(self.verticalLayout_4)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.Export_cache_listWidget = QtWidgets.QListWidget()
        self.Export_cache_listWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.verticalLayout_2.addWidget(self.Export_cache_listWidget)
        self.export_publish_checkBox = QtWidgets.QCheckBox()
        self.export_publish_checkBox.setText("Publish")
        self.verticalLayout_2.addWidget(self.export_publish_checkBox)
        self.Export_cache_push = QtWidgets.QPushButton("Export Cache")
        self.verticalLayout_2.addWidget(self.Export_cache_push)
        self.horizontalLayout.addLayout(self.verticalLayout_2)
        self.verticalLayout_3 = QtWidgets.QVBoxLayout()
        self.publish_listWidget = QtWidgets.QListWidget()
        self.verticalLayout_3.addWidget(self.publish_listWidget)
        self.publish_push = QtWidgets.QPushButton("Generate Publish File")
        self.verticalLayout_3.addWidget(self.publish_push)
        self.horizontalLayout.addLayout(self.verticalLayout_3)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.verticalLayout_5.addLayout(self.verticalLayout)

        self.centralwidget = QtWidgets.QHBoxLayout()
        self.centralwidget.addLayout(self.verticalLayout_5)
        self.setLayout(self.centralwidget)


class SelectNamespaceUI(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(SimPublishUI, self).__init__(parent)

        self.main_layout = QtWidgets.QVBoxLayout()
        self.namespace_label = QtWidgets.QLabel('Namspace')
        self.namespace_combobox = QtWidgets.QComboBox()
        self.ok_pushButton = QtWidgets.QPushButton('OK')
        self.main_layout.addItem(self.namespace_label)
        self.main_layout.addItem(self.namespace_combobox)
        self.main_layout.addItem(self.ok_pushButton)
        self.central_layout= QtWidgets.QHBoxLayout()
        self.central_layout.addLayout(main_layout)
        self.setLayout(self.central_layout)
        self.show()

    def add_item_combobox(self, item_list):
        for item in item_list:
            self.namespace_combobox.addItem(item)

    def return_output(self):
        return self.namespace_combobox.currentText()

