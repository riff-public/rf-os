_title = 'Input Sim'
_version = 'v.0.0.1'
_des = 'beta'
uiName = 'InputSim'

import os
import sys

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
from rf_qc import qc_widget
reload(qc_widget)

import rf_config as config

from rf_utils.context import context_info
reload(context_info)
from rf_utils import register_shot
reload(register_shot)
from rf_app.sim.publish_sim import output_utils
reload(output_utils)
from rf_maya.rftool.utils import yeti_lib
reload(yeti_lib)
from rf_utils import register_entity

import re
import maya.cmds as mc
import pymel.core as pm

class InputUI(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super(InputUI, self).__init__(parent)
        self.setObjectName(uiName)
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.widget = QtWidgets.QWidget()
        self.horizontalLayout_1 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.verticalLayout_1 = QtWidgets.QVBoxLayout()
        self.title = QtWidgets.QLabel("Input Shot")
        self.input_listWidget = QtWidgets.QListWidget()
        self.input_listWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.input_widget = QtWidgets.QListWidgetItem()

        self.updateAnimButton = QtWidgets.QPushButton("Update Anim")
        self.buildButton = QtWidgets.QPushButton("Build")
        #self.input_listWidget = qc_widget.QcWidget()
        self.horizontalLayout_1.addLayout(self.verticalLayout_1)
        self.verticalLayout_1.addWidget(self.title)
        self.verticalLayout_1.addWidget(self.input_listWidget)
        self.verticalLayout_1.addLayout(self.horizontalLayout_2)
        self.horizontalLayout_2.addWidget(self.updateAnimButton)
        self.horizontalLayout_2.addWidget(self.buildButton)
        
        self.widget.setLayout(self.horizontalLayout_1)
        self.setCentralWidget(self.widget)
    
        self.updateAnimButton.clicked.connect(self.updateAnim)
        self.buildButton.clicked.connect(self.build)

        self.list_input()

    def list_input(self):
        self.input_listWidget.clear()
        sceneContext = context_info.ContextPathInfo()
        reg = register_shot.Register(sceneContext)
        namespace_shot = sceneContext.process
        sim_publish_path = output_utils.sim_data_path(sceneContext, step='sim', namespace = namespace_shot)
        anim_data_path = os.path.join(sim_publish_path, 'anim')
        sim_data_path = os.path.join(sim_publish_path, 'sim')
        anim_input = os.listdir(anim_data_path)
        sim_input = os.listdir(sim_data_path)
        for input in anim_input:
            inputPath = os.path.join(anim_data_path, input)
            input_name = 'Anim -- ' + re.findall('%s[_a-zA-Z]*'%namespace_shot,os.path.basename(input).split('.')[0])[0]
            item = QtWidgets.QListWidgetItem(input_name)
            item.setData(QtCore.Qt.UserRole, inputPath)
            self.input_listWidget.addItem(item)
        for input in sim_input:
            inputPath = os.path.join(sim_data_path, input)
            input_name = 'Sim -- ' + re.findall('%s[_a-zA-Z]*'%namespace_shot,os.path.basename(input).split('.')[0])[0]
            item = QtWidgets.QListWidgetItem(input_name)
            item.setData(QtCore.Qt.UserRole, inputPath)
            self.input_listWidget.addItem(item)

        outputHero = sceneContext.path.hero().abs_path()
        allOutput = os.listdir(outputHero)
        for output in allOutput:
            if output == '%s_corrective' % namespace_shot:
                corrPath = os.path.join(outputHero, output)
                input_name = 'Corr -- ' + output
                item = QtWidgets.QListWidgetItem(input_name)
                item.setData(QtCore.Qt.UserRole, corrPath)
                self.input_listWidget.addItem(item)

    def build(self):
        sceneContext = context_info.ContextPathInfo()
        reg = register_shot.Register(sceneContext)
        namespace_shot = sceneContext.process
        sel = self.input_listWidget.selectedItems()
        for s in sel:
            path = s.data( QtCore.Qt.UserRole)
            type = s.text().split(' -- ')[0]
            if type == 'Anim':
                print 'Create %s' % path
                mc.file(path, r=True, mnc=True, f=True, namespace=namespace_shot)
                asset_anim = pm.PyNode('%s:Geo_Grp'% namespace_shot)
                animGrp , simGrp = output_utils.create_group()
                pm.parent(asset_anim, animGrp)
                if pm.objExists('%s:TechGeo_Grp'% namespace_shot):
                    asset_techAnim = pm.PyNode('%s:TechGeo_Grp'% namespace_shot)
                    pm.parent(asset_techAnim, animGrp)
            elif type == 'Sim':
                print 'Create %s' % path
                simFile = mc.file(path, r=True, mnc=True, f=True, namespace='%s_sim'%namespace_shot)
                asset_sim = pm.PyNode('%s_sim:Geo_Grp'% namespace_shot)
                animGrp , simGrp = output_utils.create_group()
                pm.parent(asset_sim, simGrp)
                if pm.objExists('%s_sim:TechGeo_Grp'% namespace_shot):
                    asset_techSim = pm.PyNode('%s_sim:TechGeo_Grp'% namespace_shot)
                    pm.parent(asset_techSim, simGrp)
            elif type == 'Corr':
                print 'Create %s' % path
                if not pm.objExists('%s_techGeoTMP:Yeti_Grp'%namespace_shot):
                    assetDescriptionPath = reg.get.asset(namespace_shot).get('description')
                    assetReg = register_entity.RegisterInfo(assetDescriptionPath)
                    groomPath = assetReg.get.groom_yeti().get('heroFile')
                    mc.file(groomPath, i=True, ns='%s_techGeoTMP'%namespace_shot, mnc=True)
                yeti_lib.import_corrective_groom(path, namespace='%s_corrective'%namespace_shot)

    def updateAnim(self):
        sceneContext = context_info.ContextPathInfo()
        namespace_shot = sceneContext.process
        reg = register_shot.Register(sceneContext)
        anim_path = reg.version.caches(namespace_shot, step='anim', hero=True)[-1]
        anim_base_name = os.path.basename(anim_path)
        sim_publish_path = output_utils.sim_data_path(sceneContext, step='sim', namespace = namespace_shot)
        anim_data_path = os.path.join(sim_publish_path, 'anim', anim_base_name)
        print 'Update Anim hero'
        output_utils.copy(anim_path, anim_data_path)

        tech_anim_path = reg.version.caches('%s_Tech'%namespace_shot, step='anim', hero=True)
        if tech_anim_path:
            tech_anim_path = tech_anim_path[-1]
            tech_anim_base_name = os.path.basename(tech_anim_path)
            tech_anim_data_path = os.path.join(sim_publish_path, 'anim', tech_anim_base_name)
            print 'Update Tech hero'
            output_utils.copy(tech_anim_path, tech_anim_data_path)
        allRef = mc.ls(rf=True)
        for ref in allRef:
            fp = mc.referenceQuery( ref, filename=True, shortName=True )
            if fp == os.path.split(anim_data_path)[-1]:
                print 'RELOAD %s' % anim_data_path
                mc.file(loadReference = ref)
            if tech_anim_path:
                if fp == os.path.split(tech_anim_data_path)[-1]:
                    print 'RELOAD %s' % tech_anim_data_path
                    mc.file(loadReference = ref)

        self.list_input()


def show():
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        maya_win.deleteUI(uiName)
        myApp = InputUI(maya_win.getMayaWindow())
        myApp.show()
        return myApp
if __name__ == '__main__':
    show()