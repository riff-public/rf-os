import os
import sys
import re
from collections import OrderedDict

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_utils.context import context_info
from rf_utils import publish_info
from rf_utils import file_utils
from rf_utils import register_shot
from rf_utils import register_entity
from rf_utils.pipeline import snap_utils
reload(register_shot)
from rf_utils import register_sg
from rf_maya.rftool.utils import maya_utils
from rf_maya.rftool.utils import yeti_lib
import maya.cmds as mc
import pymel.core as pm


def generate_publish_sim(self, sceneContext, key, outputKey, adPath, cache_local_path, namespace_shot ):

    workspace = workspace_path(sceneContext, sceneContext.name)

    outputPaths = export_path(sceneContext, sceneContext.name, namespace=key, outputKey=outputKey, schemaKey=[u'cachePath', u'cacheGlobalHero'], outputHeroKey='')
    publish_sim_file_path = sim_maya_filePath(sceneContext, step='sim', namespace = namespace_shot)
    sim_publish_path = sim_data_path(sceneContext, step='sim', namespace = namespace_shot)
    reg = register_shot.Register(sceneContext)
    camera_namespace = reg.get.camera()[0]
    cam_dict = reg.get.asset(camera_namespace)
    cam_path = cam_dict['cache']['heroFile']
    anim_path = reg.version.caches(namespace_shot, step='anim', hero=True)[-1]
    anim_base_name = os.path.basename(anim_path)
    anim_data_path = os.path.join(sim_publish_path, 'anim', anim_base_name)
    copy(anim_path, anim_data_path)

    tech_anim_path = reg.version.caches('%s_Tech'%namespace_shot, step='anim', hero=True)
    if tech_anim_path:
        tech_anim_path = tech_anim_path[-1]
        tech_anim_base_name = os.path.basename(tech_anim_path)
        tech_anim_data_path = os.path.join(sim_publish_path, 'anim', tech_anim_base_name)
        copy(tech_anim_path, tech_anim_data_path)

    print 'CACHE_LOCAL_PATH %s'%cache_local_path
    for cache_path in cache_local_path:
        for output in outputPaths:
            base_name = os.path.basename(output)
            if 'Geo' in cache_path:
                sim_output_geo = os.path.join(sim_publish_path, 'sim', base_name) ## copy to this path
                copy(cache_path, sim_output_geo)
                print 'copy sim to this path', sim_output_geo ## copy to this path
            elif 'Corrective' in cache_path:
                reg = register_shot.Register(sceneContext)
                funcType = 'corrective'
                key = '%s_corrective' % namespace_shot
                namespace = namespace_shot
                node = namespace_shot
                outputHero = sceneContext.path.hero().abs_path()
                corrPath = os.path.join(outputHero, key)
                outputCorrPaths = [corrPath,corrPath]
                sceneContext.context.update(publishVersion='v001')
                corrFiles = os.listdir(cache_path)
                for corrFile in corrFiles:
                    filePath = os.path.join(cache_path, corrFile)
                    dstPath = os.path.join(outputCorrPaths[0], corrFile)
                    print 'copy sim to this path', dstPath ## copy to this path 
                    copy(filePath, dstPath)
                reg.set_data(sceneContext, 'asset', funcType, sceneContext.step, key, namespace, node, maya=[outputCorrPaths[0],outputCorrPaths[1]])
                reg.register()
            elif 'Yeti' in cache_path:
                fn = base_name.split('.')[0]
                base_name_tech = base_name.replace(fn, fn + '_tech')
                sim_output_tech = os.path.join(sim_publish_path, 'sim', base_name_tech) ## copy to this path
                copy(cache_path, sim_output_tech)
                print 'copy sim to this path', sim_output_tech ## copy to this path

    
    #####################build File######################
    mc.file(new=True, f=True)
    mc.file(rename= publish_sim_file_path)
    currentFps = sceneContext.projectInfo.render.fps_unit()
    pm.currentUnit( time = currentFps )
    mc.file( force=True, type='mayaAscii', save=True )
    anim_grp, sim_grp  = create_group()
    create_cam(sceneContext.name_code,cam_path, camera_namespace)
    for cache_path in cache_local_path:
        if 'Geo' in cache_path:
            if not pm.objExists('%s:Geo_Grp'% namespace_shot):
                mc.file(anim_data_path, r=True, mnc=True, f=True, namespace=namespace_shot)
                #maya_utils.create_reference_asset(namespace_shot, anim_data_path)
                asset_anim = pm.PyNode('%s:Geo_Grp'% namespace_shot)
                pm.parent(asset_anim, anim_grp)

            mc.file(sim_output_geo, r=True, mnc=True, f=True, namespace='%s_sim'%namespace_shot)
            #maya_utils.create_reference_asset('%s_sim'%namespace_shot, sim_output_geo)
            if pm.objExists('%s_sim:Geo_Grp'% namespace_shot):
                asset_sim = pm.PyNode('%s_sim:Geo_Grp'% namespace_shot)
                pm.parent(asset_sim, sim_grp)

        elif 'Corrective' in cache_path:
            assetDescriptionPath = reg.get.asset(namespace_shot).get('description')
            assetReg = register_entity.RegisterInfo(assetDescriptionPath)
            groomPath = assetReg.get.groom_yeti().get('heroFile')
            mc.file(groomPath, i=True, ns='%s_techGeoTMP'%namespace_shot, mnc=True)
            keys = reg.get.asset_list(type='corrective')
            if keys:
                for k in keys:
                    if namespace_shot in k:
                        corrPath = reg.get.maya(k, 'sim', hero=True)
                        yeti_lib.import_corrective_groom(corrPath, namespace='%s_corrective'%namespace_shot)


        elif 'Yeti' in cache_path:
            mc.file(tech_anim_data_path, r=True, mnc=True, f=True, namespace=namespace_shot)
            #maya_utils.create_reference_asset(namespace_shot, tech_anim_data_path)
            mc.file(sim_output_tech, r=True, mnc=True, f=True, namespace='%s_sim'%namespace_shot)
            #maya_utils.create_reference_asset('%s_sim'%namespace_shot, sim_output_tech)
            if not pm.objExists('%s:Geo_Grp'% namespace_shot):
                mc.file(anim_data_path, r=True, mnc=True, f=True, namespace=namespace_shot)
                #maya_utils.create_reference_asset(namespace_shot, anim_data_path)
                asset_anim = pm.PyNode('%s:Geo_Grp'% namespace_shot)
                pm.parent(asset_anim, anim_grp)
            if pm.objExists('%s:TechGeo_Grp'% namespace_shot):
                asset_tech_anim = pm.PyNode('%s:TechGeo_Grp'% namespace_shot)
                pm.parent(asset_tech_anim, anim_grp)
            if pm.objExists('%s_sim:TechGeo_Grp'% namespace_shot):
                asset_sim = pm.PyNode('%s_sim:TechGeo_Grp'% namespace_shot)
                pm.parent(asset_sim, sim_grp)


    #####################################################


    # if dataType == 'Yeti':
    #     outputPaths = export_path(sceneContext, sceneContext.name, namespace=key, outputKey=outputKey, schemaKey=[u'cachePath', u'cacheGlobalHero'], outputHeroKey='')
    #     publish_sim_file_path = sim_maya_filePath(sceneContext, step='sim', namespace = namespace_shot)
    #     sim_publish_path = sim_data_path(sceneContext, step='sim', namespace = namespace_shot)
    #     reg = register_shot.Register(sceneContext)
    #     camera_namespace = reg.get.camera()[0]
    #     cam_dict = reg.get.asset(camera_namespace)
    #     cam_path = cam_dict['cache']['heroFile']
    #     anim_path = reg.version.caches(namespace_shot, step='anim', hero=False)[-1]
    #     anim_base_name = os.path.basename(anim_path)
    #     anim_data_path = os.path.join(sim_publish_path, 'anim', anim_base_name)

    #     tech_anim_path = reg.version.caches('%s_Tech'%namespace_shot, step='anim', hero=False)[-1]
    #     tech_anim_base_name = os.path.basename(tech_anim_path)
    #     tech_anim_data_path = os.path.join(sim_publish_path, 'anim', tech_anim_base_name)
    #     copy(anim_path, anim_data_path)
    #     copy(tech_anim_path, tech_anim_data_path)
    #     for output in outputPaths:
    #         base_name = os.path.basename(output)
    #         sim_output = os.path.join(sim_publish_path, 'sim', base_name) ## copy to this path
    #         print 'cache', cache_local_path, sim_output
    #         copy(cache_local_path, sim_output)
    #         print 'copy sim to this path', sim_output ## copy to this path
        
    #     #####################build File######################
    #     mc.file(new=True, f=True)
    #     mc.file(rename= publish_sim_file_path)
    #     mc.file( force=True, type='mayaAscii', save=True )
    #     anim_grp, sim_grp  = create_group()
    #     create_cam(sceneContext.name_code, cam_path, camera_namespace)
    #     maya_utils.create_reference_asset(namespace_shot, tech_anim_data_path)
    #     maya_utils.create_reference_asset('%s_Geo'%namespace_shot, anim_data_path)
    #     maya_utils.create_reference_asset('%s_sim'%namespace_shot, sim_output)
    #     if pm.objExists('%s_Geo:Geo_Grp'% namespace_shot):
    #         asset_anim = pm.PyNode('%s_Geo:Geo_Grp'% namespace_shot)
    #         pm.parent(asset_anim, anim_grp)
    #     if pm.objExists('%s:TechGeo_Grp'% namespace_shot):
    #         asset_tech_anim = pm.PyNode('%s:TechGeo_Grp'% namespace_shot)
    #         pm.parent(asset_tech_anim, anim_grp)
    #     if pm.objExists('%s_sim:TechGeo_Grp'% namespace_shot):
    #         asset_sim = pm.PyNode('%s_sim:TechGeo_Grp'% namespace_shot)
    #         pm.parent(asset_sim, sim_grp)
    #     #####################################################

def create_cam(shot, cam_path, cam_namspace):
    # if not pm.objExists('Cam_Grp'):
    #     cam_grp = pm.group(em=True, n='Cam_Grp')
    # else:
    #     cam_grp = pm.PyNode('Cam_Grp')
    from rf_maya.lib import sequencer_lib
    from rf_maya.lib import shot_lib
    reload(sequencer_lib)
    reload(shot_lib)
    duration = sequencer_lib.sync_range()
    camera = shot_lib.build_camera(cam_namspace, cam_path)
    shot_lib.build_sequencer(shot, camera, duration)
    # return cam_grp

def create_group():
    if not pm.objExists('ABC_Anim'):
        anim_grp = pm.group(em=True, n='ABC_Anim')
    else:
        anim_grp = pm.PyNode('ABC_Anim')
        
    if not pm.objExists('ABC_Sim'):
        sim_grp = pm.group(em=True, n='ABC_Sim')
    else:
        sim_grp = pm.PyNode('ABC_Sim')

    return [anim_grp, sim_grp]

def sim_maya_filePath(sceneContext, step='', namespace = ''):
    from rf_app.save_plus import save_utils
    scene = sceneContext.copy()
    scene.context.update(step=step,publishVersion='v001', app='maya', process = namespace)
    maya_path =scene.path.workspace().abs_path()
    file_name = scene.publish_name()
    path = os.path.join(maya_path, file_name)
    lastest_file = save_utils.increment_file(path.replace('\\', '/'))
    return lastest_file

def sim_data_path(sceneContext, step='', namespace = ''):
    scene = sceneContext.copy()
    scene.context.update(step=step,publishVersion='hero',app='cache', process=namespace)
    data_path = scene.path.workspace().abs_path()
    if not os.path.exists(data_path):
        os.makedirs(data_path)
    return data_path

def publish_version(sceneContext, shotName):
    # override context to shot
    sceneContext.context.update(entityCode=shotName)
    sceneContext.context.update(entity=sceneContext.sg_name)


def reg_version(scene, shotName, namespace, reg=True):
    scene.context.update(entity=scene.sg_name)
    reg = publish_info.PreRegisterShot(scene)
    regVersion = reg.add_output(namespace)
    reg.register() if reg else None
    return regVersion


def export_path(sceneContext, shotName, namespace='', outputKey='', schemaKey=[], outputHeroKey=''):
    scene = sceneContext.copy()
    # scene.context.update(entityCode=shotName)
    scene.context.update(entity=scene.sg_name)

    # published file
    scene.context.update(process=namespace) if namespace else None
    scene.context.update(publishVersion= 'v001')
    outputDir = scene.path.scheme(schemaKey[0]).abs_path()
    
    
    published_process_path = outputDir.split('/v001')[0] if '/v001' in outputDir else None
    if not os.path.exists(published_process_path):
        os.makedirs(published_process_path)
    list_publish_file = os.listdir(published_process_path)
    if list_publish_file:
        list_version = os.listdir(published_process_path)
        lastest_version = max([int(a.split('v')[-1]) for a in os.listdir(published_process_path)])
        if lastest_version:
            scene.context.update(publishVersion= 'v%03d'%((int(lastest_version))+1))
            publish_dir = scene.path.scheme(schemaKey[0]).abs_path()
            filename = scene.output_name(outputKey=outputKey)
            publishedFile = '%s/%s' % (publish_dir, filename)
    else:
        publish_dir = scene.path.scheme(schemaKey[0]).abs_path()
        filename = scene.output_name(outputKey=outputKey)
        publishedFile = '%s/%s' % (publish_dir, filename)

    # hero file
    heroOutputDir = scene.path.scheme(schemaKey[1]).abs_path()
    heroFileName = scene.output_name(outputKey=outputKey, hero=True)
    heroFile = '%s/%s' % (heroOutputDir, heroFileName)

    if outputHeroKey:
        shotHero = scene.path.hero().abs_path()
        heroName = scene.output_name(outputKey=outputHeroKey, hero=True)
        heroFile = '%s/%s' % (shotHero, heroName)

    return [publishedFile, heroFile]

def copy(src, dst):
    dst_path = os.path.dirname(dst)
    if os.path.exists(dst_path):
        file_utils.copy(src, dst)
        return True if os.path.exists(dst) else False
    else:
        os.makedirs(dst_path)
        if os.path.exists(src):
            file_utils.copy(src, dst)
            return True if os.path.exists(dst) else False
        # # detect pattern
        # pattern = re.findall(r"%\d+d",src)
        # if pattern:
        #     rawFiles = file_utils.list_file(os.path.dirname(src))
        #     replacePatterns = os.path.basename(src).split(pattern[0])
        #     validFiles = OrderedDict()

        #     for srcFile in rawFiles:
        #         seqNumber = srcFile
        #         for replaceKey in replacePatterns:
        #             seqNumber = seqNumber.replace(replaceKey, '')

        #         if seqNumber.isdigit():
        #             validFiles['%s/%s' % (os.path.dirname(src), srcFile)] = seqNumber

        #     results = []
        #     for srcFile, seqNumber in validFiles.iteritems():
        #         try:
        #             dstFile = sequence_name(dst, seqNumber)
        #             # file_utils.copy(srcFile, dstFile)
        #             file_utils.xcopy_file(srcFile, dstFile)
        #             logger.debug('Copy to publish %s' % dstFile)
        #             status = True if os.path.exists(dstFile) else False
        #         except Exception as e:
        #             logger.error(e)
        #             status = False
        #         results.append(status)
        #     return all(results)

def workspace_path(sceneContext, shotName):
    scene = sceneContext.copy()
    scene.context.update(app='maya')
    # workspace version
    wsVersion = reg_version(scene, shotName, 'workspace', reg=True)

    # wsVersion = 'v001'
    scene.context.update(publishVersion=wsVersion)

    publishedWsDir = scene.path.publish_workspace().abs_path()

    publishedWsFile = scene.publish_name()
    publishedWs = '%s/%s' % (publishedWsDir, publishedWsFile)

    # workspace hero
    heroWsDir = scene.path.publish_workspace_hero().abs_path()
    heroWsFile = scene.publish_name(hero=True)
    heroWs = '%s/%s' % (heroWsDir, heroWsFile)

    return [publishedWs, heroWs]

def increment_version(file_path):
    file_path = check_exists(file_path)
    all_file = os.listdir(file_path)
    increment_count=[]
    for file in all_file:
        increment = re.findall ( 'v' + '[0-9]{3}', file ) ;
        if increment:
            increment_count.append(increment[0])
    if increment_count:
        version_lastest = int(max(increment_count).split('v')[1])
        new_version = 'v%03d'%(version_lastest+1)
    else:
        new_version = 'v001'
    return new_version

def check_exists(path):
    exists = os.path.exists(path)
    if exists:
        return path.replace('\\','/')
    else:
        os.makedirs(path.replace('\\','/'))
        return path.replace('\\','/')
# abc_cache funcType
# abc_cache dataType
# badger_001 outputNs
# badger_001 namespace
# cache outputKey
# [u'cachePath', u'cacheGlobalHero'] schemaKey
# Geo_Grp node
# badger_001 key
# P:/SevenChickMovie/asset/publ/char/badger/hero/badger_rig_main_md.hero.ma ref
#  outputHeroKey
# False publish


# if dataType == 'abc_cache':
#     reg.set_data(scene, 'asset', funcType, scene.step, key, namespace, node, adPath=adPath, workspace=[workspace[0], workspace[1]], cache=[outputPaths[0], outputPaths[1]], exportNamespace=exportNamespace)



# if dataType == 'abc_cache':
#     reg.set_data(scene, 'asset', 'abc_cache', scene.step, 'badger_001', 'badger_001', 'Geo_Grp', adPath=adPath, workspace=[workspace[0], workspace[1]], cache=[outputPaths[0], outputPaths[1]], exportNamespace=exportNamespace)