_title = 'Riff PublishCache'
_version = 'v.0.0.1'
_des = 'beta'
uiName = 'RFPublishCache'

#Import python modules
import sys
import os
import getpass
import json
import logging

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)

# import config
import rf_config as config
from rf_utils import log_utils
# sg

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

import ui
import re 
import maya.cmds as mc
from rftool.utils import maya_utils
from rf_utils.context import context_info
import pymel.core as pm 
from rf_utils import register_shot
from functools import partial 
from rf_maya.rftool.utils import abc_utils
from rf_utils import file_utils
import pymel.core as pm
import output_utils
reload(output_utils)

from rf_maya.rftool.utils import sg_process
project = 'DarkHorse'
abc_pre_roll = 30
abc_post_roll =10
class RFPublishCache(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        #SetUp Window
        super(RFPublishCache, self).__init__(parent)
        self.ui = ui.SimPublishUI(parent)
        self.setObjectName(uiName)
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.setCentralWidget(self.ui)
        self.resize(800, 500)
        file_path = mc.file(q=True, loc=True)
        print 'rest'
        print file_path
        if file_path:
            self.init_ui()
            self.init_signals()
            self.list_item()
            self.list_output()

    def init_ui(self):
        episode, shot_entity, char_namespace = self.context_from_local()
        self.ui.project_lineEdit.setText(project)
        self.ui.ep_lineEdit.setText(episode)
        self.ui.seq_lineEdit.setText(shot_entity)
        self.ui.namespace_lineEdit.setText(char_namespace)
        self.ui.qc_listWidget.update( project= project, entityType='scene', step='sim', res='default', entity= self.context())


    def init_signals(self):
        self.ui.qc_push.clicked.connect(self.list_item)
        self.ui.Export_cache_push.clicked.connect(self.export_cache)
        self.ui.publish_push.clicked.connect(self.publish)
        self.ui.type_comboBox.currentIndexChanged.connect(self.list_output)

    def context_from_local(self, drive=False):
        file_path = mc.file(q=True, loc=True)
        if file_path:
            file_path_split = file_path.split(project)[1]
            if drive:
                return '%s/'%os.path.splitdrive(file_path)[0]
            if file_path_split:
                episode = file_path_split.split('/')[1]
                shot_entity = file_path_split.split('/')[2]
                char_namespace = file_path_split.split('/')[3]
                print episode, shot_entity, char_namespace
                return episode, shot_entity, char_namespace

    def context(self):
        context = context_info.Context()
        episode, shot_entity, char_namespace = self.context_from_local() 
        shot = shot_entity.split('_')[-1]
        context.update(entityType='scene', project= project, entityGrp=episode, entity=shot_entity, step='sim', process='main',name=shot)
        self.scene_entity = context_info.ContextPathInfo(context=context)
        return self.scene_entity

    def get_ad_path(self, entity, type):
        context =context_info.Context()
        context.update(project = 'DarkHorse', entityGrp = type, entityType = 'asset', entity = entity)
        asset = context_info.ContextPathInfo(context=context)
        asset.path.scheme(key='asmDataPath')
        adPath = '%s/%s' % (asset.path.asm_data().abs_path(), asset.ad_name)
        return adPath



    def list_item(self):
        print self.ui.type_comboBox.currentText() 
        type_export = self.ui.type_comboBox.currentText() 

        if type_export == 'Geo':
            self.list_geo_in_scene()
        elif type_export == 'Yeti':
            pass
 

    def list_yeti(self):
        pass

    def list_asmData(self):
        type_export = self.ui.type_comboBox.currentText()
        scene = self.context()
        reg = register_shot.Register(scene)
        asset_list = reg.get.asset_list(type='abc_cache')
        for asset in asset_list:
            path = reg.get.asset(asset)['description']
            if type_export in path:
                print path, asset

    def list_geo_in_scene(self):
        
        all_geo_grp = pm.ls('*:Geo_Grp')

        all_item = []
        for item in all_geo_grp:
            item_root = item.root()
            all_item.append(item_root)
            if not '_CIN' in item_root.name():
                self.add_item_Export_cache(item_root)

    def add_item_Export_cache (self, pm_node):
        name = pm_node.name()
        name_space = pm_node.namespace().split(':')[0]
        widget_item = QtWidgets.QListWidgetItem()
        widget_item.setText(name)
        widget_item.setData(QtCore.Qt.UserRole, name_space)
        self.ui.Export_cache_listWidget.addItem(widget_item)

    ############ export_cache
    def export_cache(self):
        version_path, hero_path = self.get_sim_local_path()
        cur_item = self.ui.Export_cache_listWidget.currentItem()
        char_grp = cur_item.text()
        path = version_path
        char_node = pm.PyNode(char_grp)

        #### time_range### 
        pre_roll = 30
        post_roll = 10
        start = mc.playbackOptions(q = True, min = True)- pre_roll
        end = mc.playbackOptions(q = True, max = True)+ post_roll

        if 'Geo_Grp' in char_node.name():
            print char_node.name()
            self.add_attr(pyNode =char_node)
            print path
            result = abc_utils.export_abc(char_node.name(), path, start, end)
            file_utils.copy(version_path, hero_path)

        self.list_output()
    ################
    ############# publish#########
    def publish(self):
        result = []
        char_namespace = self.ui.namespace_lineEdit.text()
        assetName = char_namespace.split('_')[0]
        export_namespace = self.check_asset_in_scene(namespace=char_namespace)
        assetType = sg_process.get_one_asset(project, assetName)['sg_asset_type']
        if export_namespace:
            if self.ui.type_comboBox.currentText() == 'Geo':
                cache_file_path = self.ui.publish_listWidget.currentIndex().data( QtCore.Qt.UserRole)
                context = context_info.Context()
                context.update(project= project, entityType ='asset', step='rig', entity=export_namespace.split('_')[0])
                context.update(entityGrp = assetType)
                asset = context_info.ContextPathInfo(context=context)
                adPath = '%s/%s' % (asset.path.asm_data().abs_path(), asset.ad_name)
                dataType = self.ui.type_comboBox.currentText()
                print cache_file_path
                result = output_utils.publish_sim(self, self.scene_entity, export_namespace, 'cache', adPath, dataType, cache_file_path, export_namespace)
                
            else:
                pass
        
        if result:
            from rf_utils.widget import dialog
            dialog.MessageBox.success('Publish!', "Publish Complete", parent=None)

    def check_asset_in_scene(self, namespace = ''):
        from rf_utils import register_shot
        from rf_utils import register_sg
        reload(register_sg)
        reg = register_shot.Register(self.scene_entity)
        asset_list = reg.get.asset_list(type='abc_cache')
        #currentItem = self.ui.Export_cache_listWidget.currentItem.data(QtCore.Qt.UserRole) ####(namespace)
        if namespace in asset_list :
            shotElm = register_sg.ShotElement(self.scene_entity)
            ex_namespace= shotElm.read(namespace).exportNamespace
            return ex_namespace


    ################## add_attribute
    #def add_attribute(self, ):


    #######################path#################

    def get_output_path_publish(self):
        scene = self.context()
        output_path = scene.path.scheme(key='heroPath').abs_path()
        return  output_path
        
    def get_sim_publ_path(self):
        episode, shot_entity, char_namespace =  self.context_from_local()
        scene_entity = self.context()
        path_ver = scene_entity.path.scheme(key='publishPath').abs_path()
        output_path, version =self.increment_version_folder(path = path_ver)

        context.update(publishVersion = version, app='maya')
        scene_entity = context_info.ContextPathInfo(context=context)
        name_entity = scene_entity.copy()
        name_entity.context.update(app='maya', process = char_namespace, publishVersion = version)
        output_path_name = name_entity.publish_name()
        return  os.path.join(output_path, output_path_name).replace('\\', '/')
###################################local_path#################
    def get_sim_local_path(self, get_output_path = False):
        cur_drive = self.context_from_local(drive=True)
        type_cache = self.ui.type_comboBox.currentText()
        episode, shot_entity, char_namespace =  self.context_from_local()
        #output_path
        output_path = os.path.join(cur_drive, project, episode, shot_entity, char_namespace, 'output')
        if get_output_path:
            return output_path
        currentItem = self.ui.Export_cache_listWidget.currentItem().data(QtCore.Qt.UserRole)
        # namespace = self.ui.Export_cache_listWidget.currentItem.data(QtCore.)
        process_path = os.path.join(output_path, type_cache, currentItem)
        
        ###version file
        new_version = self.increment_version(process_path)
        version_file_name = self.get_cache_local_name(version = new_version)
        ###hero_file
        hero_file_name = self.get_cache_local_name()
        ### cache_path
        cache_version_path = os.path.join(process_path, new_version, version_file_name)
        cache_hero_path = os.path.join(process_path, 'hero', hero_file_name)

        # print self.check_exists(cache_version_path)
        # print self.check_exists(cache_hero_path)
        if not os.path.exists(os.path.dirname(cache_hero_path)):
            os.makedirs(os.path.dirname(cache_hero_path))
        return cache_version_path.replace('\\', '/') , cache_hero_path.replace('\\','/')

    #######################path#################


    def get_file_name(self):
        scene_entity = self.context()
        scene_entity.context.update(publishVersion = 'hero', app='maya')
        output_path_name = name_entity.publish_name()
        return output_path_name

    ###############################################
    def get_context(self):
        pass
    #######################################

    def check_exists(self, path):
        exists = os.path.exists(path)
        if exists:
            return path.replace('\\','/')
        else:
            os.makedirs(path.replace('\\','/'))
            return path.replace('\\','/')


    def get_cache_local_name(self, version ='hero'):
        type_export = self.ui.type_comboBox.currentText()
        if type_export == 'Geo':
            episode, shot_entity, char_namespace =  self.context_from_local()
            scene_entity = self.context()
            scene_entity.context.update(publishVersion = version, app='maya', process = char_namespace)
            output_path_name = scene_entity.publish_name(ext='.abc')
            return output_path_name

        elif type_export == 'Yeti':
            pass

    def get_yeti_name(self):
        episode, shot_entity, char_namespace = self.context_from_local()
        context = context_info.Context()
        context.update(project = project, episode =episode, entity=shot_entity, process = char_namespace, step='sim',entityType='scene')
        scene = context_info.ContextPathInfo(context = context)
        scene.context.publishVersion
        scene2 = scene.copy()
        yeti_name_cache = scene2.output_name(outputKey='cache_yeti',hero=True)
        return yeti_name_cache

    def increment_version(self, file_path):
        return output_utils.increment_version(file_path)

    def add_attr(self, pyNode=None):######pynode = namespace:Geo_Grp
        if pyNode:
            add_attr = ['project', 'assetType', 'assetName', 'adPath', 'refPath']
            assetName = pyNode.namespace()[0:-1]
            if '_' in assetName:
                assetName = assetName.split('_')[0]
            assetType = sg_process.get_one_asset(project, assetName)['sg_asset_type']
            scene_entity = self.context()
            for attr in add_attr:
                if not pyNode.hasAttr(attr):
                    pyNode.addAttr(attr, dt='string')
                if attr == 'project':
                    pyNode.attr('project').set(project)
                elif attr == 'assetType':
                    pyNode.attr('assetType').set(assetType)
                elif attr == 'assetName':
                    pyNode.attr('assetName').set(assetName)
                elif attr == 'adPath':
                    adPath = self.get_ad_path(assetName, assetType)
                    pyNode.attr('adPath').set(adPath)
                elif attr == 'refPath':
                    refPath =self.getRefPath(pyNode)
                    pyNode.attr('refPath').set(refPath)
        else:
            return None
        
    def getRefPath(self, pynode):
        asset = context_info.ContextPathInfo(path= pm.FileReference(namespace=pynode.namespace()).path)
        asset.context.update(step='rig')
        output_path = asset.path.hero().abs_path()
        output_name = asset.output_name(outputKey='rig',hero=True,ext='.ma')
        publish_path = os.path.join(output_path, output_name)
        return publish_path

    def list_output(self):
        self.ui.publish_listWidget.clear()
        type_output = self.ui.type_comboBox.currentText()
        path_output = self.get_sim_local_path(get_output_path=True)
        type_path = os.path.join(path_output, type_output).replace('\\', '/')
        if os.path.exists(type_path):
            all_item = os.listdir(type_path)
            for ix in all_item:
                item = QtWidgets.QListWidgetItem(ix)
                hero_path = os.path.join(type_path, '%s/hero'%ix).replace('\\', '/')
                file = os.listdir(hero_path)
                hero_output_path = os.path.join(hero_path, file[0])
                item.setData(QtCore.Qt.UserRole, hero_output_path)
                self.ui.publish_listWidget.addItem(item)

def show():
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        maya_win.deleteUI(uiName)
        myApp = RFPublishCache(maya_win.getMayaWindow())
        myApp.show()
        return myApp
if __name__ == '__main__':
    show()