import calendar
from rf_app.time_logged import sg_time_log_utils as stlg
from datetime import datetime
sg = stlg.sg


def summary_project_logs(user_name, year, month):
    weekday, numdays = calendar.monthrange(year, month)
    start_date = (year, month, 1)
    end_date = (year, month, numdays)
    start = datetime(*start_date).date()
    end = datetime(*end_date).date()
        
    date_range = [start, end]
    filters = [['date', 'between', date_range], ['user.HumanUser.sg_name', 'is', user_name]]
    fields = ['project', 'user', 'duration', 'date', 'entity', 'user.HumanUser.department', 'user.HumanUser.sg_name']
    logs = sg.find('TimeLog', filters, fields)

    project_dict = dict()
    for log in logs:
        step = log['user.HumanUser.department']['name'].lower()
        user = log['user.HumanUser.sg_name']
        duration = log['duration']
        project = log['project']['name']
        dt = datetime.strptime(log['date'], '%Y-%m-%d')
        day = dt.day
        month = dt.month

        if log['entity']:
            
            if project not in project_dict.keys():
                project_dict[project] = {day:duration}
            else:
                if day not in project_dict[project].keys():
                    project_dict[project][day] = duration
                else:
                    project_dict[project][day] += duration

    return project_dict

