from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

import sys
import os
sys.path.append('{}/core'.format(os.environ['RFSCRIPT']))

import calendar
from datetime import datetime
from rf_utils.ui import stylesheet
from rf_app.time_logged.user_sum import utils
from rf_utils.widget import display_widget
from rf_utils.widget import user_widget
from rf_utils import user_info

class config:
    img_app = '%s/core/rf_launcher_apps/Tool/User Summary/UserSummary.png' % os.environ.get('RFSCRIPT')

class App(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super(App, self).__init__(parent=parent)
        self.setup_ui()
        self.check_permission()
        self.setup_month()
        self.init_signal()

    def setup_ui(self):
        self.widget = QtWidgets.QWidget()
        self.main = QtWidgets.QHBoxLayout(self.widget)
        self.user_layout = QtWidgets.QVBoxLayout()
        self.table_layout = QtWidgets.QVBoxLayout()

        self.photo_label = display_widget.SGPhotoFrame()
        self.photo_label.setAlignment(QtCore.Qt.AlignCenter)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        self.photo_label.setSizePolicy(sizePolicy)
        self.photo_label.setMinimumSize(QtCore.QSize(200, 200))
        self.photo_label.setMaximumHeight(200)

        self.userWidget = user_widget.UserComboBox()
        self.userWidget.checkBox.setVisible(False)

        self.department_label = QtWidgets.QLabel('Department :')
        self.month_layout = QtWidgets.QHBoxLayout()
        self.month_label = QtWidgets.QLabel('Month :')
        self.month_combo = QtWidgets.QComboBox()
        self.year_combo = QtWidgets.QComboBox()
        self.search_button = QtWidgets.QPushButton()
        self.search_button.setText('Search')
        self.search_button.setFixedHeight(30)
        self.spacer_item = QtWidgets.QSpacerItem(20,30)

        self.tableWidget = QtWidgets.QTableWidget()
        self.tableWidget.horizontalHeader().setDefaultSectionSize(120)
        self.tableWidget.verticalHeader().hide()
        self.tableWidget.horizontalHeader().hide()
        self.tableWidget.setStyleSheet("background-color: rgb(45, 45, 45);")
        #self.tableWidget.setSelectionMode(QtWidgets.QAbstractItemView.NoSelection)
        self.tableWidget.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)

        self.user_layout.addWidget(self.photo_label)
        self.user_layout.addWidget(self.userWidget)
        self.user_layout.addWidget(self.department_label)
        self.month_layout.addWidget(self.month_label)
        self.month_layout.addWidget(self.month_combo)
        self.month_layout.addWidget(self.year_combo)

        self.user_layout.addLayout(self.month_layout)
        self.user_layout.addWidget(self.search_button)
        self.user_layout.addItem(self.spacer_item)

        self.month_layout.setStretch(0, 0)
        self.month_layout.setStretch(1, 2)
        self.month_layout.setStretch(2, 2)

        self.user_layout.setStretch(0, 0)
        self.user_layout.setStretch(1, 0)
        self.user_layout.setStretch(2, 0)
        self.user_layout.setStretch(3, 0)
        self.user_layout.setStretch(4, 0)
        self.user_layout.setStretch(5, 2)

        self.table_layout.addWidget(self.tableWidget)
        self.main.addLayout(self.user_layout)
        self.main.addLayout(self.table_layout)

        self.main.setStretch(0, 0)
        self.main.setStretch(1, 2)

        self.setCentralWidget(self.widget)
        self.setMinimumSize(700, 350)
        self.setWindowTitle('User Summary')
        self.setWindowIcon(QtGui.QIcon(config.img_app))


    def init_signal(self):
        self.search_button.clicked.connect(self.fetch_user)


    def setup_month(self):
        fisrtYear = 2021
        date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        today = datetime.strptime(date.split(" ")[0], '%Y-%m-%d')
        numYear = today.year - fisrtYear + 1

        for num in range(numYear):
            self.year_combo.addItem(str(fisrtYear))
            fisrtYear += 1

        for num in range(1, 13):
            month = calendar.month_abbr[num]
            self.month_combo.addItem(month)

        self.month_combo.setCurrentText(calendar.month_abbr[today.month])
        self.year_combo.setCurrentText(str(today.year))


    def fetch_user(self):
        self.tableWidget.clear()
        self.photo_label.reset()
        self.tableWidget.setRowCount(0)
        self.tableWidget.setColumnCount(0)

        self.set_user()
        user_name = self.userWidget.data()['sg_name']
        year = self.year_combo.currentText()
        month = self.month_combo.currentText()
        month_number = list(calendar.month_abbr).index(month)
        self.set_table_project(user_name, int(year), month_number)


    def check_permission(self):
        user = user_info.User()
        if user.is_hr() or user.is_admin():
            self.userWidget.checkBox.setChecked(True)
            self.userWidget.set_searchable(True)
            self.userWidget.searchable_by_id = True
        else:
            self.userWidget.checkBox.setChecked(False)
            self.userWidget.set_searchable(False)


    def set_user(self):
        if 'image' in self.userWidget.data().keys():
            image_link = self.userWidget.data()['image']
            
            if image_link:
                self.photo_label.show(image_link)

        department = self.userWidget.data()['department']['name']
        self.department_label.setText('%s %s'%('Department :', department))


    def set_table_project(self, user_name, year, month):
        self.project_dict = utils.summary_project_logs(user_name, year, month)
        weekday, numdays = calendar.monthrange(year, month)
        project_num = len(self.project_dict)

        #column project
        if self.project_dict:
            self.tableWidget.setRowCount(numdays+1)
            self.tableWidget.setColumnCount(project_num+1)

            for num in range(numdays):
                month_name = calendar.month_abbr[month]
                text = '%s %s %s'%(num+1, month_name, year) 

                item = QtWidgets.QTableWidgetItem(text)
                item.setTextAlignment(QtCore.Qt.AlignCenter)
                item.setForeground(QtCore.Qt.white)
                item.setBackground(QtGui.QColor(70, 70, 70))
                self.tableWidget.setItem(num+1, 0, item)

        #row days
        for num, project in enumerate(sorted(self.project_dict)):
            item = QtWidgets.QTableWidgetItem(project)
            item.setTextAlignment(QtCore.Qt.AlignCenter)
            item.setForeground(QtCore.Qt.white)
            item.setBackground(QtGui.QColor(90, 90, 90))
            self.tableWidget.setItem(0, num+1, item)

            #value
            for day in self.project_dict[project]:
                duration = self.project_dict[project][day]
                value = round(duration/60, 2)

                item = QtWidgets.QTableWidgetItem(str(value))
                item.setTextAlignment(QtCore.Qt.AlignCenter)
                item.setForeground(QtCore.Qt.white)
                item.setBackground(QtGui.QColor(90, 90, 90))
                self.tableWidget.setItem(day, num+1, item)

            #not value
            for day in range(numdays):
                if day+1 not in self.project_dict[project].keys():
                    item = QtWidgets.QTableWidgetItem()
                    item.setBackground(QtGui.QColor(70, 70, 70))
                    self.tableWidget.setItem(day+1, num+1, item)


def show():
    app = QtWidgets.QApplication.instance()
    if not app:
        app = QtWidgets.QApplication(sys.argv)
    myApp = App()
    stylesheet.set_default(app)
    myApp.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    show()