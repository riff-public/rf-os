# uncompyle6 version 3.8.0
# Python bytecode 2.7 (62211)
# Decompiled from: Python 2.7.11 (v2.7.11:6d1b6a68f775, Dec  5 2015, 20:40:30) [MSC v.1500 64 bit (AMD64)]
# Warning: this version of Python has problems handling the Python 3 byte type in constants properly.

# Embedded file name: D:/dev/core\rf_app\time_logged\time_logged_utils.py
# Compiled at: 2021-03-12 18:12:36
import os, sys, argparse, logging
from collections import OrderedDict
from datetime import datetime
from rf_utils.context import context_info
from rf_utils import user_info
from rf_utils.sg import sg_time_track as sgtt
from rf_utils import custom_exception
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
sg = sgtt.sg
TIMETRACK_ENTITY = 'CustomNonProjectEntity01'
TIMELOGGED_ENTITY = 'CustomNonProjectEntity02'
DATEFORMAT = '%Y:%m:%d'
USER_CACHES = dict()
PROJECT_CACHES = dict()
ENTRY_CACHES = dict()
ENTITY_CACHES = dict()
STEP_CACHES = dict()
NON_PROJECT = 'None-Project'
NON_ENTITY = 'No_Entity'
RND_ENTITY = 'RnD_Entity'
NON_TASK = 'No_Task'

class ReturnField:
    timetrack = [
     'code', 'sg_project', 'sg_user', 'sg_department',
     'sg_assets', 'sg_shots', 'sg_task', 'sg_entity', 'sg_start_record',
     'sg_end_record', 'sg_duration_minute', 'sg_duration_summarry', 'sg_user_logged_items', 'sg_status_list', 'updated_at']
    logged_item = [
     'code', 'sg_project', 'sg_user', 'sg_duration_minute',
     'sg_record_time', 'sg_time_track_parent',
     'sg_time_track_parent_automatic', 'description']
    task = [
     'content', 'entity', 'step', 'project', 'sg_status_list',
     'task_assignees', 'sg_app', 'sg_resolution', 'sg_name',
     'sg_lockdown', 'sg_lockdown_message', 'open_notes', 'step.Step.short_name']


def find_project(name):
    if isinstance(name, str) or isinstance(name, unicode):
        if name not in PROJECT_CACHES.keys():
            project = sg.find_one('Project', [['name', 'is', name]], ['id', 'name', 'sg_project_code'])
            if not project:
                project = find_project(NON_PROJECT)
            PROJECT_CACHES[name] = project
        return PROJECT_CACHES[name]
    if isinstance(name, dict):
        return name


def find_entity_task(project, sg_entity, task_name, create=False):
    project_entity = find_project(project)
    filters = [['project', 'is', project_entity], ['entity', 'is', sg_entity], ['content', 'is', task_name]]
    task_entity = sg.find_one('Task', filters, ReturnField.task)
    if not task_entity:
        if create:
            return create_none_task(project_entity, sg_entity['type'], sg_entity['code'])
        return dict()
    return task_entity


def find_user(name):
    """
    get user entity from given name or login name
    """
    if isinstance(name, str) or isinstance(name, unicode):
        if name not in USER_CACHES.keys():
            nickname = name if '.' not in name else ''
            adname = name if '.' in name else ''
            user = user_info.User(sg, nickname=nickname, adname=adname)
            sg_user = user.sg_user()
            if sg_user:
                USER_CACHES[name] = user.sg_user()
            else:
                logger.warning(('No user found {}').format(name))
                return sg_user
        return USER_CACHES[name]
    if isinstance(name, dict):
        return name


def find_entity(project, entity_type, entity, create=False):
    if entity_type and entity:
        project_entity = find_project(project)
        key = ('{}-{}').format(project_entity['name'], entity)
        if key not in ENTITY_CACHES.keys():
            filters = [
             [
              'project', 'is', project_entity], ['code', 'is', entity]]
            fields = ['id', 'code']
            entity = sg.find_one(entity_type, filters, fields)
            if entity:
                ENTITY_CACHES[key] = entity
            else:
                if create:
                    result = create_none_entity(project_entity, entity_type, RND_ENTITY)
                    return sg.find_one(entity_type, filters, fields)
                else:
                    return dict()

        return ENTITY_CACHES[key]
    return dict()


def find_entities(project, entity_type):
    project_entity = find_project(project)
    filters = [['project', 'is', project_entity]]
    fields = ['id', 'code']
    return sg.find(entity_type, filters, fields)


def list_projects():
    project_dict = OrderedDict()
    projects = sg.find('Project', [['sg_status', 'is', 'Active']], ['name', 'id'])
    for project in projects:
        name = project['name']
        project_dict[name] = project

    sorted_projects = [ project_dict[k] for k in sorted(project_dict.keys()) ]
    return sorted_projects


def fetch_timetrack_data(user, project=None):
    """
    fetch data frrom time tracking entity for given user

    Args:
        user (str) : user name

    Returns (dict): Shotgun entities dictionary
    """
    user_entity = find_user(user)
    filters = [['sg_user', 'is', user_entity]]
    filters.append(['sg_project.Project.name', 'is', project])
    return sg.find(TIMETRACK_ENTITY, filters, ReturnField.timetrack)


def find_tasks(project, user):
    user_entity = find_user(user)
    project_entity = find_project(project)
    if not user_entity:
        raise custom_exception.PipelineError(('No user "{}" found').format(user))
    if not project_entity:
        raise custom_exception.PipelineError(('No project "{}" found').format(project))
    filters = [
     [
      'project', 'is', project_entity], ['task_assignees', 'is', user_entity]]
    return sg.find('Task', filters, ReturnField.task)


def create_entity(project, entity_name, task_name):
    entity_type = 'Asset'
    project_entity = find_project(project)
    entity = find_entity(project, entity_type, entity_name)
    if not entity:
        data = {'project': project_entity, 
           'sg_asset_type': 'rnd', 
           'code': entity_name, 
           'sg_status_list': 'omt'}
        entity = sg.create(entity_type, data)
    task_entity = find_entity_task(project_entity, entity, task_name, create=False)
    step = find_etc_step(entity_type)
    if not task_entity:
        data = {'project': project_entity, 'entity': entity, 
           'step': step, 
           'content': task_name, 
           'sg_status_list': 'omt', 
           'sg_description': 'Task for timetracking'}
        task_entity = sg.create('Task', data)
    return {'project': project_entity, 'entity': entity, 'task': task_entity}


def create_user_task_track(user, project, entity_type, entity_name, task_name, step='', logged_type='', create=False):
    """
    This will create an entry on Time Tracking Entity
    """
    user_entity = find_user(user)
    project_entity = find_project(project)
    sg_entity = find_entity(project, entity_type, entity_name, create=create)
    if sg_entity:
        task_entity = find_entity_task(project_entity, sg_entity, task_name, create=create)
        if task_entity:
            if not step:
                step = task_entity.get('step.Step.short_name') if 1 else step
                task_name = task_entity['content']
                entity_name = sg_entity['code']
                name = user_task_track_name(user, project, entity_name, task_name)
                data = {'code': name, 
                   'sg_project': project_entity, 
                   'sg_user': user_entity, 
                   'sg_entity': sg_entity}
                data.update({'sg_task': task_entity}) if task_entity else None
                data.update({'sg_department': step}) if step else None
                entry = find_user_task_track(name, project, user)
                entry = entry or sg.create(TIMETRACK_ENTITY, data)
                logger.debug(('Time Task created: {}').format(entry))
            else:
                entry = sg.update(TIMETRACK_ENTITY, entry['id'], data)
                logger.debug(('Time Task exists: {}').format(entry))
            return entry
        raise custom_exception.PipelineError(('Task "{}" not found in project: "{}" entity: "{}"').format(task_name, project, entity_name))
    else:
        raise custom_exception.PipelineError(('"{}" "{}" "{}" not found').format(project, entity_type, entity_name))
    return


def create_user_logged_item(parent_entity, project, user, duration=0, duration_append=False, description='', logged_type='User'):
    item_name = user_logged_name(parent_entity['code'], logged_type=logged_type)
    project_entity = find_project(project)
    user_entity = find_user(user)
    entry = find_user_logged_item(item_name, project_entity, user_entity, parent_entity, logged_type=logged_type)
    current_duration = 0
    record_time = datetime.now()
    data = dict()
    if description:
        data.update({'description': description})
    if logged_type == 'User':
        data.update({'sg_time_track_parent': parent_entity})
    if logged_type == 'Automatic':
        data.update({'sg_time_track_parent_automatic': parent_entity})
    if entry:
        current_duration = int(entry['sg_duration_minute'])
        new_duration = current_duration + duration if duration_append else duration
        data.update({'sg_duration_minute': new_duration})
        return sg.update(TIMELOGGED_ENTITY, entry['id'], data)
    if not entry:
        new_duration = current_duration + duration if duration_append else duration
        data.update({'code': item_name, 
           'sg_project': project_entity, 
           'sg_user': user_entity, 
           'sg_duration_minute': new_duration, 
           'sg_record_time': record_time, 
           'sg_logged_type': logged_type})
        return sg.create(TIMELOGGED_ENTITY, data)


def user_task_track_name(user='None', project='None', entity_name='None', task_name='None'):
    user_str = user['name'] if isinstance(user, dict) else user
    project_str = project['name'] if isinstance(project, dict) else project
    name = ('-').join([user_str, project_str, entity_name, task_name])
    return name


def user_logged_name(track_name, date_string='', logged_type='User'):
    if not date_string:
        date_string = datetime.now().date().strftime(DATEFORMAT)
    item_name = ('{}-{}').format(track_name, date_string)
    if logged_type == 'Automatic':
        item_name = ('{}-Auto').format(item_name)
    return item_name


def day_str(datetime_object):
    if not datetime_object:
        return datetime.now().date().strftime(DATEFORMAT)
    return datetime_object.date().strftime(DATEFORMAT)


def find_active_user_from_tasks(project):
    project_entity = find_project(project)
    filters = [['project', 'is', project_entity]]
    fields = ['task_assignees']
    tasks = sg.find('Task', filters, ReturnField.task)
    active_users = list()
    for task in tasks:
        users = task['task_assignees']
        for user in users:
            if user['type'] == 'HumanUser':
                if user not in active_users:
                    active_users.append(user)

    return active_users


def find_user_task_track(name, project, user):
    project_entity = find_project(project)
    user_entity = find_user(user)
    filters = [
     [
      'code', 'is', name],
     [
      'sg_project', 'is', project_entity],
     [
      'sg_user', 'is', user_entity]]
    return sg.find_one(TIMETRACK_ENTITY, filters, ReturnField.timetrack)


def find_user_task_tracks(project, user):
    project_entity = find_project(project)
    user_entity = find_user(user)
    filters = [
     [
      'sg_project', 'is', project_entity],
     [
      'sg_user', 'is', user_entity]]
    return sg.find(TIMETRACK_ENTITY, filters, ReturnField.timetrack)


def find_user_time_tracks(user, project=None):
    user_entity = find_user(user)
    project_entity = find_project(project) if project else None
    filters = [['sg_user', 'is', user_entity], ['sg_status_list', 'is_not', 'omt']]
    filters.append(['sg_project', 'is', project_entity]) if project_entity else None
    return sg.find(TIMETRACK_ENTITY, filters, ReturnField.timetrack)


def find_user_logged_item(name, project, user, parent_entity=None, logged_type='User'):
    project_entity = find_project(project)
    user_entity = find_user(user)
    filters = [
     [
      'code', 'is', name],
     [
      'sg_project', 'is', project_entity],
     [
      'sg_user', 'is', user_entity],
     [
      'sg_logged_type', 'is', logged_type]]
    return sg.find_one(TIMELOGGED_ENTITY, filters, ReturnField.logged_item)


def find_user_logged_items(user, project=None):
    project_entity = find_project(project)
    user_entity = find_user(user)
    filters = [['sg_user', 'is', user_entity]]
    filters.append(['sg_project', 'is', project_entity]) if project_entity else None
    return sg.find(TIMELOGGED_ENTITY, filters, ReturnField.logged_item)


def find_user_logged_by_ids(ids):
    filter_ids = [ ['id', 'is', id] for id in ids ]
    filters = [
     {'filter_operator': 'any', 
        'filters': filter_ids}]
    return sg.find(TIMELOGGED_ENTITY, filters, ReturnField.logged_item)


def create_none_entity(project, entity_type, name):
    project_entity = find_project(project)
    entity = sg.find_one(entity_type, [['project', 'is', project_entity],
     [
      'code', 'is', name]])
    if not entity:
        data = {'project': project_entity, 'code': name, 
           'sg_asset_type': 'rnd', 
           'sg_status_list': 'omt', 
           'description': 'Automatic create pipeline Asset. Do not delete'}
        return sg.create(entity_type, data)
    else:
        return entity


def create_none_task(project, entity_type, name):
    project_entity = find_project(project)
    none_entity = sg.find_one(entity_type, [['project', 'is', project_entity], ['code', 'is', name]], ['code', 'id'])
    if not none_entity:
        none_entity = create_none_entity(project_entity, entity_type, name)
    step = find_etc_step(entity_type)
    task_entity = sg.find_one('Task', [['project', 'is', project_entity],
     [
      'entity', 'is', none_entity],
     [
      'content', 'is', NON_TASK]], ReturnField.task)
    if not task_entity:
        data = {'project': project_entity, 'entity': none_entity, 
           'sg_status_list': 'omt', 
           'content': NON_TASK, 
           'step': step, 
           'sg_description': 'Automatic create pipeline Task. Do not delete'}
        return sg.create('Task', data)
    else:
        return task_entity


def find_etc_step(entity_type):
    key = ('{}-{}').format(entity_type, 'Etc')
    if key not in STEP_CACHES.keys():
        result = sg.find_one('Step', [['code', 'is', 'Etc'], ['entity_type', 'is', entity_type]], ['code'])
        STEP_CACHES[key] = result
    return STEP_CACHES[key]


def create_time_track_from_task(user, project, create=False):
    tasks = find_tasks(project, user)
    timetracks = find_user_task_tracks(project, user)
    timetrack_names = [ a['code'] for a in timetracks ]
    to_create = list()
    created = list()
    entity_types = list()
    logger.debug('Fetching tasks ...')
    for task in tasks:
        entity_name = task['entity']['name']
        task_name = task['content']
        entity_type = task['entity']['type']
        entity_types.append(entity_type) if entity_type not in entity_types else None
        name = user_task_track_name(user, project, entity_name, task_name)
        if name not in timetrack_names:
            to_create.append(task)

    logger.debug(('To create {} tasks').format(len(to_create)))
    if create:
        project_entity = find_project(project)
        user_entity = find_user(user)
        sg_entities = list()
        batch_data = []
        for entity_type in entity_types:
            sg_entities += find_entities(project, entity_type)

        for task in to_create:
            entity_type = task['entity']['type']
            entity_name = task['entity']['name']
            task_name = task['content']
            step = task.get('step.Step.short_name')
            name = user_task_track_name(user, project, entity_name, task_name)
            sg_entity = [ a for a in sg_entities if a['code'] == entity_name ]
            if not sg_entity:
                sg_entity = find_entity(project, entity_type, entity_name, create=create)
            else:
                sg_entity = sg_entity[0]
            data = {'code': name, 
               'sg_project': project_entity, 
               'sg_user': user_entity, 
               'sg_entity': sg_entity}
            data.update({'sg_task': task})
            data.update({'sg_department': step}) if step else None
            batch_data.append({'request_type': 'create', 
               'entity_type': TIMETRACK_ENTITY, 
               'data': data})

        result = sg.batch(batch_data)
        logger.debug(('{} tasks created').format(len(batch_data)))
    return result


class UserTimeTrack(object):
    """docstring for UserTimeTrack"""

    def __init__(self, user, project=None, logged_type='User'):
        super(UserTimeTrack, self).__init__()
        self.user = user
        self.project = project
        self.user_entity = find_user(user)
        self.project_entity = find_project(project) if project else None
        self.logged_type = logged_type
        return

    def list_tasks(self):
        self.check_data()
        id_dict = dict()
        task_list = list()
        tasks = find_user_time_tracks(self.user_entity, self.project_entity)
        logs = find_user_logged_items(self.user_entity, self.project_entity)
        for log in logs:
            id_dict[log['id']] = log

        for task in tasks:
            new_list = []
            for log in task['sg_user_logged_items']:
                log_id = log['id']
                log_entity = id_dict[log_id]
                new_list.append(log_entity)

            task['sg_user_logged_items'] = new_list

        return tasks

    def find_task(self, entity_type, entity_name, task_name):
        self.check_data()
        task = TimeTask(user=self.user_entity, project=self.project_entity, entity_name=entity_name, entity_type=entity_type, task_name=task_name, logged_type=self.logged_type)
        return task

    def check_data(self):
        warning = []
        if not self.user_entity:
            warning.append(('No user "{}" found').format(self.user))
        if not self.project_entity:
            warning.append('No project found')
        if warning:
            raise custom_exception.PipelineError(('{}').format((', ').join(warning)))
        return True


class TimeTask(object):
    """docstring for TimeTask"""

    def __init__(self, user, project, entity_name, entity_type, task_name, logged_type='User'):
        super(TimeTask, self).__init__()
        self.user_entity = find_user(user)
        self.project_entity = find_project(project) if project else None
        self.entity_name = entity_name
        self.entity_type = entity_type
        self.task_name = task_name
        self.logged_type = logged_type
        self.name = user_task_track_name(user, project, self.entity_name, self.task_name)
        self.sg_entity = find_entity(project, entity_type, self.entity_name)
        self.entity = find_user_task_track(name=self.name, project=self.project_entity, user=self.user_entity)
        return

    def __str__(self):
        return self.entity['code']

    def find_logged(self):
        track_entity = self.entity
        if track_entity:
            entity_logged = TimeLoggedItem(self.user_entity, self.project_entity, parent_entity=track_entity, logged_type=self.logged_type)
            return entity_logged
        logger.warning('Time Task not created yet. see method "create"')

    def create_task(self):
        self.sg_entity = find_entity(self.project_entity, self.entity_type, self.entity_name)
        track_entity = create_user_task_track(user=self.user_entity, project=self.project_entity, entity_type=self.entity_type, entity_name=self.entity_name, task_name=self.task_name, logged_type=self.logged_type)
        entity_logged = TimeLoggedItem(self.user_entity, self.project_entity, parent_entity=track_entity, logged_type=self.logged_type)
        logger.info(('Time Task: {}').format(track_entity))
        return entity_logged

    def create_logged(self, duration):
        if not self.entity:
            entity_logged = self.create_task()
        else:
            entity_logged = TimeLoggedItem(self.user_entity, self.project_entity, parent_entity=self.entity, logged_type=self.logged_type)
        result = entity_logged.add_time(duration)
        return result


class TimeLoggedItem(object):
    """docstring for TimeLoggedItem"""

    def __init__(self, user, project, entity_name='', task_name='', parent_entity=None, logged_type='User'):
        super(TimeLoggedItem, self).__init__()
        self.logged_entity = None
        self.user_entity = find_user(user)
        self.project_entity = find_project(project)
        self.logged_type = logged_type
        self.name = user_task_track_name(user, project, entity_name, task_name)
        self.parent_entity = (parent_entity or find_user_task_track)(name=self.name, project=self.project_entity, user=self.user_entity) if 1 else parent_entity
        return

    def list_all_logged(self):
        entities = self.parent_entity['sg_user_logged_items']
        ids = [ a['id'] for a in entities ]
        return find_user_logged_by_ids(ids)

    def add_time(self, duration=0):
        result = create_user_logged_item(self.parent_entity, self.project_entity, self.user_entity, duration=duration, duration_append=True, logged_type=self.logged_type)
        return result

    def time(self, item_date='today'):
        track_name = self.parent_entity['code']
        date_string = day_str(datetime.now())
        name = user_logged_name(track_name, date_string=date_string, logged_type=self.logged_type)
        self.logged_entity = find_user_logged_item(name, self.project_entity, self.user_entity, parent_entity=self.track_entity, logged_type=self.logged_type)
        return self.logged_entity['sg_duration_minute']


def sync_tasks_timetrack(project, user):
    """
    Convert assigned task to timetrack entry
    Args:
        project (str) : project name
        user (str) : user name

    Returns (list) : a list of shotgun entry entity dict
    """
    task_entities = sgtt.find_tasks(project, user)
    timetrack_entries = sgtt.fetch_timetrack_data(user, project)
    create = False
    for task in task_entities:
        project = task.get('project').get('name')
        task_name = task.get('content')
        entity_name = task.get('entity').get('name')
        entity_type = task.get('entity').get('type')
        step_name = task.get('step.Step.short_name')
        match_entry = None
        for entry in timetrack_entries:
            project_entry = (entry.get('sg_project') or dict()).get('name')
            tasks = [ a.get('name') for a in entry.get('sg_tasks') ] if entry.get('sg_tasks') else []
            entity_entry = (entry.get('sg_entity') or dict()).get('name')
            if project == project_entry and task_name in tasks and entity_name == entity_entry:
                match_entry = entry

        if not match_entry:
            result = update_entry(user, entity_name, step_name, entity_type, project, tasks=[task_name], duration=0, duration_append=True, logged_type='User')
            logger.debug(('Created {}').format(result))
            create = True
        else:
            logger.debug(('Matched {}').format(entry))

    if create:
        return sgtt.fetch_timetrack_data(user, project)
    else:
        return timetrack_entries


def fetch_entry(user):
    return sgtt.fetch_timetrack_data(user)