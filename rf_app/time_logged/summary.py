import os
import sys
import logging
from datetime import datetime
from rf_utils.context import context_info
from rf_utils import user_info
from rf_utils import custom_exception
from . import time_logged_utils as tlu
sg = tlu.sg

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

DATEFORMAT = '%Y:%m:%d'
USER_CACHES = dict()
PROJECT_CACHES = dict()
ENTRY_CACHES = dict()
ENTITY_CACHES = dict()
STEP_CACHES = dict()

TIMETRACK_ENTITY = 'CustomNonProjectEntity01'
TIMELOGGED_ENTITY = 'CustomNonProjectEntity02'

sum_attr = 'sg_summary_man_hour' 
sum_auto_attr = 'sg_summary_man_hour_automatic'

sum_task_attr = 'sg_duration_summary_event'
sum_auto_task_attr = 'sg_duration_summary_event_automatic'


def sum_timetrack(timelog_id): 
    filters = [['id', 'is', timelog_id]]
    fields = ['sg_duration_minute', 'sg_logged_type', 
        'sg_time_track_parent', 'sg_time_track_parent_automatic']
    log_entity = sg.find_one(TIMELOGGED_ENTITY, filters, fields)

    attr = 'sg_time_track_parent'
    target_attr = 'sg_duration_summary_event'

    if log_entity.get('sg_logged_type') == 'User': 
        attr = 'sg_time_track_parent'
        target_attr = 'sg_duration_summary_event'

    elif log_entity.get('sg_logged_type') == 'Automatic': 
        attr = 'sg_time_track_parent_automatic'
        target_attr = 'sg_duration_summary_event_automatic'

    parent_entity = log_entity.get(attr)
    parent_fields = ['code', 'id', 'sg_entity']
    parent_entity = sg.find_one(TIMETRACK_ENTITY, [['id', 'is', parent_entity['id']]], parent_fields)

    if parent_entity: 
        filters = [[attr, 'is', parent_entity]]
        all_logs = sg.find(TIMELOGGED_ENTITY, filters, fields)
        summary = 0

        for log in all_logs: 
            minute = log['sg_duration_minute']
            summary += minute

        data = {target_attr: summary}
        result = sg.update(TIMETRACK_ENTITY, parent_entity['id'], data)
        logger.info('Update summary {} on track: "{}" entity: "{}"'.format(summary, parent_entity['code'], parent_entity['sg_entity']['name']))
        return result


def sum_entity(timetrack_id): 
    """ summary all timetrack linked to entity """
    fields = ['sg_duration_summary_event', 'sg_duration_summary_event_automatic', 'sg_entity']
    timetrack_entity = sg.find_one(TIMETRACK_ENTITY, [['id', 'is', timetrack_id]], fields)
    linked_entity = timetrack_entity['sg_entity']

    if linked_entity: 
        # find all timetrack entities to summarize duration 
        all_track_entities = sg.find(TIMETRACK_ENTITY, [['sg_entity', 'is', linked_entity]], fields)
        user_sum = 0 
        auto_sum = 0 

        for entity in all_track_entities: 
            if entity['sg_duration_summary_event']: 
                user_sum += entity['sg_duration_summary_event']
            if entity['sg_duration_summary_event_automatic']:                 
                auto_sum += entity['sg_duration_summary_event_automatic']

        # update linked entity 
        data = {'sg_summary_man_hour': user_sum, 'sg_summary_man_hour_automatic': auto_sum}
        sg.update(linked_entity['type'], linked_entity['id'], data)
        logger.info('Update summary {}, {} on entity "{}"'.format(user_sum, auto_sum, linked_entity))


def sum_project(timelog_id): 
    """ this will sum the whole project including 
    - time task 
    - task 
    - entity 
    - project 
    """ 
    timetrack_entity = summary_timetrack(timelog_id)
    summary_entity(timetrack_entity)

def summary_timetrack(timelog_id): 
    # from given log id, find logged type and parent entity 
    log_fields = [
        'sg_time_track_parent_automatic', 
        'sg_time_track_parent', 'sg_duration_minute', 
        'sg_logged_type', 'sg_user']

    log_entity = sg.find_one(TIMELOGGED_ENTITY, [['id', 'is', timelog_id]], log_fields)
    logged_type = log_entity['sg_logged_type']
    user_entity = log_entity['sg_user']
    # find other logs that linked to the same parent timetrack entity 
    # if logged type is User, find parent timetrack entity from 'sg_time_track_parent'
    log_filters = [['sg_user', 'is', user_entity]]
    if logged_type == 'User': 
        parent_timetrack_entity = log_entity['sg_time_track_parent']
        log_filters.append(['sg_time_track_parent', 'is', log_entity['sg_time_track_parent']])
        target_attr = sum_task_attr

    if logged_type == 'Automatic': 
        parent_timetrack_entity = log_entity['sg_time_track_parent_automatic']
        log_filters.append(['sg_time_track_parent_automatic', 'is', log_entity['sg_time_track_parent_automatic']])
        target_attr = sum_auto_task_attr

    all_logs = sg.find(TIMELOGGED_ENTITY, log_filters, log_fields)
    sum_duration = 0 

    for log in all_logs:
        if log['sg_duration_minute']:  
            sum_duration += int(log['sg_duration_minute'])

    # we get all duration from each logged 
    # now assign to parent entity (time track)
    data = {target_attr: sum_duration}
    parent_timetrack_entity = sg.update(TIMETRACK_ENTITY, parent_timetrack_entity['id'], data)
    return parent_timetrack_entity


def summary_entity_and_task(timetrack_entity): 
    all_tracks = list_neighbour_timetracks(timetrack_entity)
    entity = update_summary_entity(all_tracks)
    tasks = update_summary_task(all_tracks)
    return entity, tasks


def list_neighbour_timetracks(timetrack_entity): 
    # find parent entity
    fields = ['sg_entity', 'sg_task', sum_task_attr, sum_auto_task_attr, 'sg_user']
    filters = [['id', 'is', timetrack_entity['id']]]
    timetrack_entity = sg.find_one(TIMETRACK_ENTITY, filters, fields)

    user_entity = timetrack_entity['sg_user']
    asset_shot_entity = timetrack_entity['sg_entity']
    task_entity = timetrack_entity['sg_task']
    # user_sum = timetrack_entity[sum_task_attr]
    # auto_sum = timetrack_entity[sum_auto_task_attr]

    # find other task tracks to get minute summary 
    filters = [['sg_entity', 'is', asset_shot_entity], ['sg_user', 'is', user_entity]]
    fields = ['sg_entity', 'sg_task', sum_task_attr, sum_auto_task_attr, 'sg_user']
    all_tracks = sg.find(TIMETRACK_ENTITY, filters, fields)

    return all_tracks


def update_summary_entity(all_tracks): 
    user_sum = 0 
    auto_sum = 0 
    asset_shot_entity = None

    for timetrack in all_tracks: 
        user_duration = timetrack[sum_task_attr]
        auto_duration = timetrack[sum_auto_task_attr]
        asset_shot_entity = timetrack['sg_entity']

        if user_duration: 
            user_sum += int(user_duration)
        if auto_duration: 
            auto_sum += int(auto_duration)

    # update entity 
    data = {sum_attr: user_sum, sum_auto_attr: auto_sum}
    return sg.update(asset_shot_entity['type'], asset_shot_entity['id'], data)


def update_summary_task(all_tracks): 
    # tasks 
    update_tasks = []
    if all_tracks: 
        task_ids = list(set([a['sg_task']['id'] for a in all_tracks if a['sg_task']]))

        for task_id in task_ids: 
            task_user_sum = 0 
            task_auto_sum = 0 
            for timetrack  in all_tracks: 
                if (timetrack['sg_task'] or dict()).get('id') == task_id: 
                    user_duration = timetrack[sum_task_attr]
                    auto_duration = timetrack[sum_auto_task_attr]

                    if user_duration: 
                        task_user_sum += int(user_duration)
                    if auto_duration: 
                        task_auto_sum += int(auto_duration)

            data = {sum_attr: task_user_sum, sum_auto_attr: task_auto_sum}
            task = sg.update('Task', task_id, data)
            update_tasks.append(task)

    return update_tasks


def update_summary_project(project): 
    # list entities 
    project_entity = sg.find_one('Project', [['name', 'is', project]])
    if project_entity: 
        assets = sg.find('Asset', [['project', 'is', project_entity]], [sum_attr, sum_auto_attr, 'project'])
        shots = sg.find('Shot', [['project', 'is', project_entity]], [sum_attr, sum_auto_attr, 'project'])
        entities = assets + shots
        user_sum = 0 
        auto_sum = 0 

        for entity in entities: 
            user_duration = entity[sum_attr]
            auto_duration = entity[sum_auto_attr]

            if user_duration: 
                user_sum += int(user_duration)
            if auto_duration: 
                auto_sum += int(auto_duration)

        user_day = (user_sum / 60) / 8
        auto_day = (auto_sum / 60) / 8
        meta_dict = {'user': '{} days'.format(user_day), 'auto': '{} days'.format(auto_day)}

        if user_sum or auto_sum: 
            data = {sum_attr: user_sum, sum_auto_attr: auto_sum, 'sg_meta': str(meta_dict)}
            return sg.update('Project', project_entity['id'], data)
    else: 
        logger.warning('project not found "{}"'.format(project))


def collect(): 
    pass
    # project 
    # user 
    # asset / shot 
    # sum 
