import os
import sys
from openpyxl import Workbook
from openpyxl.styles import Alignment, colors, fills
from openpyxl.utils import get_column_letter

import string
from itertools import count, product, islice
from datetime import datetime
import calendar

core = '{}/core'.format(os.environ['RFSCRIPT'])
sys.path.append(core) if not core in sys.path else None
import rf_config
from rf_app.time_logged.sum import config
from rf_app.time_logged import sg_time_log_utils as stlg

departments = ['art', 'anim', 'light',
    'prod', 'layout', 'model', 'rig', 'groom', 'comp', 'sim', 'fx', 'pipeline', 'realtime', 'edit']


def main():
    projects = stlg.find_active_projects()
    projects = stlg.entity_sorted(projects, 'name')


    logs = stlg.fetch_logs(start_date=(2021, 6, 1), department='')
    print(logs)
    userdict = stlg.list_all_users()


def test2():
    wb = Workbook()

    # create calendar work sheet
    ws = wb.create_sheet("calendar", 0)
    ws.title = 'Raw Data'
    ws['A1'] = 'test'
    col_index = 4
    col_letter = get_column_letter(col_index)
    print(col_letter)
    color_fill = fills.PatternFill(patternType='solid', fgColor=colors.Color(rgb='8c8c8c'))
    ws['A2'].fill = color_fill
    print(ws['A1'].value)

    wb.save('D:/test2.xlsx')



if __name__ == '__main__':
    test2()


# data
# {'date': '2021-06-01',
#  'description': 'Fixing Force-Shield_Fx',
#  'duration': 480,
#  'entity': {'id': 321192, 'name': 'RnD', 'type': 'Task'},
#  'entity.Task.content': 'RnD',
#  'entity.Task.entity': {'id': 15012, 'name': 'Miscellaneous', 'type': 'Asset'},
#  'entity.Task.project': {'id': 281, 'name': 'Hanuman', 'type': 'Project'},
#  'entity.Task.task_assignees': [{'id': 409,
#                                  'name': 'Aum',
#                                  'type': 'HumanUser'},
#                                 {'id': 912,
#                                  'name': 'Janjao',
#                                  'type': 'HumanUser'},
#                                 {'id': 418,
#                                  'name': 'John',
#                                  'type': 'HumanUser'},
#                                 {'id': 210,
#                                  'name': 'KenR',
#                                  'type': 'HumanUser'},
#                                 {'id': 913,
#                                  'name': 'Man',
#                                  'type': 'HumanUser'},
#                                 {'id': 19,
#                                  'name': 'Nuke',
#                                  'type': 'HumanUser'},
#                                 {'id': 18,
#                                  'name': 'Pang',
#                                  'type': 'HumanUser'},
#                                 {'id': 358,
#                                  'name': 'Pom',
#                                  'type': 'HumanUser'},
#                                 {'id': 356,
#                                  'name': 'Pop',
#                                  'type': 'HumanUser'},
#                                 {'id': 441,
#                                  'name': 'Por',
#                                  'type': 'HumanUser'},
#                                 {'id': 350,
#                                  'name': 'Preaw',
#                                  'type': 'HumanUser'},
#                                 {'id': 469,
#                                  'name': 'Sheroz',
#                                  'type': 'HumanUser'},
#                                 {'id': 447,
#                                  'name': 'Tan',
#                                  'type': 'HumanUser'},
#                                 {'id': 344,
#                                  'name': 'Tawan',
#                                  'type': 'HumanUser'},
#                                 {'id': 336,
#                                  'name': 'Toey',
#                                  'type': 'HumanUser'},
#                                 {'id': 465,
#                                  'name': 'Yousuf',
#                                  'type': 'HumanUser'}],
#  'entity.Task.updated_at': datetime.datetime(2021, 6, 17, 20, 9, 7, tzinfo=<shotgun_api3.lib.sgtimezone.LocalTimezone object at 0x000001B04485DDD8>),
#  'id': 13612,
#  'project': {'id': 281, 'name': 'Hanuman', 'type': 'Project'},
#  'sg_type': 'User',
#  'type': 'TimeLog',
#  'user': {'id': 358, 'name': 'Pom', 'type': 'HumanUser'},
#  'user.HumanUser.department': {'id': 49, 'name': 'FX', 'type': 'Department'}}
