ECHO OFF

SCHTASKS /CREATE /F /TN "Riff TimeLogged" /TR "C:\Python27\pythonw.exe %RFSCRIPT%\core\rf_launcher\launcher_cmd.py -p Hanuman -a TimeLogged -t Tool" /SC WEEKLY /D MON,TUE,WED,THU,FRI /ST 19:00
SCHTASKS /CREATE /F /TN "Riff TimeLogged Reminder" /TR "C:\Python27\pythonw.exe %RFSCRIPT%\core\rf_app\time_logged\reminder.py" /SC WEEKLY /D MON,TUE,WED,THU,FRI /ST 11:30
