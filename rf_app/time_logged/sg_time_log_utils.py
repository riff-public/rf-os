#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import argparse
import logging
from collections import OrderedDict
from datetime import datetime, date
from rf_utils.context import context_info
from rf_utils import user_info
from rf_utils.sg import sg_time_track as sgtt
from rf_utils import custom_exception

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
sg = sgtt.sg
is_python3 = True if sys.version_info[0] > 2 else False
#
CACHE_PROJECT = dict()
CACHE_ENTITY = dict()
CACHES_USER = dict()
CACHE_STEP = dict()
CACHE_DEPARTMENT = dict()
DATEFORMAT = '%Y-%m-%d'
NON_PROJECT = 'None-Project'
NON_ENTITY = 'No_Entity'
RND_ENTITY = 'RnD_Entity'
NON_TASK = 'No_Task'


class ReturnField:
    project = ['name', 'id']
    entity = ['code', 'id']
    timelog = [
        'project', 'entity', 'user', 'duration', 'date', 'sg_type', 'description',
        'entity.Task.content', 'entity.Task.entity', 'entity.Task.updated_at',
        'entity.Task.task_assignees', 'entity.Task.project', 'user.HumanUser.department', 
        'entity.Ticket.sg_ticket_type', 'entity.Ticket.title', 'entity.Ticket.addressings_to', 
        'entity.Ticket.updated_at', 'entity.Ticket.sg_link']
    task = [
        'content', 'entity', 'step', 'project', 'sg_status_list', 'created_at',
        'task_assignees', 'sg_app', 'sg_resolution', 'sg_name', 'time_logs_sum', 'est_in_mins',
        'sg_lockdown', 'sg_lockdown_message', 'open_notes', 'step.Step.short_name', 'updated_at', 'sg_involve_with']
    ticket = ['project', 'title', 'sg_ticket_type', 'addressings_to', 'sg_link', 'created_at', 'updated_at']

    department = ['name', 'code']
    user = [
            'sg_employee_id', 'sg_ad_account', 
            'sg_employee_department', 'name', 'sg_hr_local_name', 
            'department.Department.name']


class SG: 
    departments = [
    'art', 'anim', 'design', 'light', 
    'prod', 'layout', 'model', 'rig', 'lookdev', 
    'groom', 'set', 'comp', 'sim', 'techanim', 'fx', 'pipeline', 'edit', 'tracking']


def create_time_logged(project, entity_type, entity_name, task_name, user, duration=0, logtype='User', description='', create_date=None):
    """
    Args:
        project (str): name of a project
        entity_type (str): 'Asset' or 'Shot'
        entity_name (str): name of an asset or shot
        task_name (str): name of a task
        duration (int): minute
    """
    user_entity = find_user(user)
    today = date.today() if not create_date else create_date

    project = find_project(project)
    task_entity = find_task(project, entity_type, entity_name, task_name)
    if task_entity:
        data = {
            'entity': task_entity,
            'project': project,
            'user': user_entity,
            'duration': duration,
            'date': today,
            'sg_type': logtype,
            'description': description
            }
        result = sg.create('TimeLog', data)
        result['entity'] = task_entity
        # update_involve_people(task_entity, user_entity)
        return result
    else:
        custom_exception.PipelineError('Task not found: {}:{}:{}'.format(project, entity_name, task_name))


def update_time_logged(logid, duration=0, description=''):
    data = {'duration': duration}
    if description:
        data.update({'description': description})
    result = sg.update('TimeLog', logid, data)
    return sg.find_one('TimeLog', [['id', 'is', result['id']]], ReturnField.timelog)


def add_time_logged(project, entity_type, entity_name, task_name, user, duration=0, logtype='', description='', add=True, date_filter='today', create_date=None):
    logs = list_logged_by_date(project, entity_type, entity_name, task_name, user, logtype=logtype, date_filter=date_filter)
    if not logs:
        return create_time_logged(project, entity_type, entity_name, task_name, user, duration, logtype=logtype, description=description, create_date=create_date)

    else:
        logid = logs[0]['id']
        minute = logs[0]['duration']
        new_duration = (minute + duration) if add else duration
        return update_time_logged(logid, new_duration, description)


def list_time_logged(project, entity_type, entity_name, task_name, user, logtype=''):
    logs = list()
    project = find_project(project)
    user_entity = find_user(user)
    task_entity = find_task(project, entity_type, entity_name, task_name)
    if project and user_entity and task_entity:
        filters = [['project', 'is', project], ['entity', 'is', task_entity], ['user', 'is', user_entity]]
        filters.append(['sg_type', 'is', logtype]) if logtype else None
        logs = sg.find('TimeLog', filters, ReturnField.timelog)

    return logs


def list_logged_by_date(project, entity_type, entity_name, task_name, user, logtype='', date_filter='today'):
    logs = list_time_logged(project, entity_type, entity_name, task_name, user, logtype=logtype)
    match_date = datetime.now().strftime(DATEFORMAT) if date_filter == 'today' else date_filter
    match_logs = [a for a in logs if a['date'] == match_date]
    return match_logs


def find_tasks(user, project=None):
    user_entity = find_user(user)
    project_entity = find_project(project)
    if not user_entity:
        raise custom_exception.PipelineError('No user "{}" found'.format(user))
    filters = [
        {
        "filter_operator": "or",
        "filters": [
            ["task_assignees", "is", user_entity],
            ["sg_involve_with", "is", user_entity]
        ]}]

    filters.append(['project', 'is', project_entity]) if project else None
    tasks = sg.find('Task', filters, ReturnField.task)

    # combind ticket to dict 
    statuses = ['wtg', 'opn', 'ip', 'rev', 'cmpt']
    ticket_filter = [['addressings_to', 'is', user_entity]]
    status_filter = {
                        "filter_operator": "any",
                        "filters": [['sg_status_list', 'is', a] for a in statuses]
                    }
    ticket_filter.append(status_filter)
    tickets = sg.find('Ticket', ticket_filter, ReturnField.ticket)
    task_dict = dict()

    for task in tasks:
        try:
            code = user_task_track_name(
                user=user, project=task['project']['name'],
                entity_name=task['entity']['name'],
                task_name=task['content']
                )
            task['sg_user_logged_items'] = []
            task['code'] = code
            task['name'] = task['content']
            task['user'] = user
            id_key = 'tsk_{}'.format(task['id'])
            task_dict[id_key] = task
        except Exception as e:
            logger.error(e)

    for ticket in tickets: 
        try: 
            entity_name = 'Ticket-{}-{}'.format(ticket['sg_ticket_type'], ticket['id'])
            code = user_task_track_name(
                user=user, 
                project=ticket['project']['name'],
                entity_name=entity_name,
                task_name=ticket['title']
                )

            ticket['sg_user_logged_items'] = []
            ticket['code'] = code
            # ticket['name'] = ticket['title'].decode(encoding='UTF-8',errors='strict') # Title can contain non-westen char that needs decoding
            ticket['name'] = ticket['title']
            ticket['user'] = user
            ticket['entity'] = {'name': entity_name, 'id': ticket['id'], 'type': 'Ticket'}
            ticket['content'] = ticket['title']
            id_key = 'tkt_{}'.format(ticket['id'])
            task_dict[id_key] = ticket
        except Exception as e: 
            logger.error(e)
            print(e)

    return task_dict


def find_task_from_logged(user, project=None):
    project_entity = find_project(project) if project else None
    user_entity = find_user(user)
    filters = [['user', 'is', user_entity]]
    filters.append(['project', 'is', project_entity]) if project else None
    logs = sg.find('TimeLog', filters, ReturnField.timelog)
    task_dict = dict()

    for log in logs:
        log['sg_record_time'] = log['date']
        log['sg_duration_minute'] = log['duration']
        if not log['entity']:
            continue
        if log['entity']['type'] == 'Task': 
            task = log['entity']
            task['content'] = log['entity.Task.content']
            task['name'] = log['entity.Task.content']
            task['entity'] = log['entity.Task.entity']
            task['project'] = log['project']
            task['sg_user_logged_items'] = [log]
            task['updated_at'] = log['entity.Task.updated_at']
            task['user'] = user
            taskid = task['id']
            id_key = 'tsk_{}'.format(taskid)

        elif log['entity']['type'] == 'Ticket': 
            ticket = log['entity']
            ticket['content'] = log['entity.Ticket.title']
            entity_name = 'Ticket-{}-{}'.format( log['entity.Ticket.sg_ticket_type'], ticket['id'])
            ticket['name'] = entity_name
            ticket['entity'] = {'name': entity_name, 'id': ticket['id'], 'type': 'Ticket'}
            ticket['project'] = log['project']
            ticket['sg_user_logged_items'] = [log]
            ticket['updated_at'] = log['entity.Ticket.updated_at']
            ticket['user'] = user
            taskid = ticket['id']
            id_key = 'tkt_{}'.format(taskid)
            task = ticket

        if task['entity']: 
            code = user_task_track_name(
                user=user, project=task['project']['name'],
                entity_name=task['entity']['name'],
                task_name=task['content']
                )
            task['code'] = code

            if id_key in task_dict.keys():
                task_dict[id_key]['sg_user_logged_items'].append(log)
            else:
                task_dict[id_key] = task
                task_dict[id_key]['sg_user_logged_items'] = [log]

    return task_dict


def list_user_tasks(project, user):
    task_dict = find_tasks(user, project=None)
    task_from_log_dict = find_task_from_logged(user, project)

    for id, task_entity in task_from_log_dict.items():
        if not id in task_dict.keys():
            task_dict[id] = task_entity
            # raise custom_exception.PipelineError('task name "{}"" missing "{}" in "sg_involve_with" fields'.format(task_entity['content'], user))
        task_dict[id]['sg_user_logged_items'] = task_entity['sg_user_logged_items']

    return [v for k, v in task_dict.items()]


def list_user_tasks2(project, user):
    """ list task from assigned user and logged user """
    task_dict = find_task_from_logged(user, project)
    assign_task_dict = find_tasks(user, project)
    combind_task = dict()
    # conform data
    for id, task_entity in task_dict.items():

        task_entity['name'] = task_entity['content']
        entity_name = task_entity['entity']['name']
        code = user_task_track_name(user=user, project=task_entity['project']['name'], entity_name=entity_name, task_name=task_entity['content'])
        temp = {
            'sg_task': task_entity,
            'code': code,
            'user': user
            }

        for key, value in task_entity.items():
            if not key in temp.keys():
                temp[key] = value

        combind_task[id] = temp

    for id, task_entity in assign_task_dict.items():
        if not id in combind_task.keys():
            task_entity['name'] = task_entity['content']
            entity_name = task_entity['entity']['name']
            code = user_task_track_name(user=user, project=task_entity['project']['name'], entity_name=entity_name, task_name=task_entity['content'])
            task_entity['code'] = code
            task_entity['user'] = user
            combind_task[id] = task_entity

    # for k, v in combind_task.items():
    #     print v['project']['name'], v['entity']['name'], v['content']
    #     for a in v.get('sg_user_logged_items', list()):
    #         print a.get('duration')

    return [v for k, v in combind_task.items()]


def find_user(name):
    """
    get user entity from given name or login name
    """
    if isinstance(name, str) or isinstance(name, unicode):
        if name not in CACHES_USER.keys():
            nickname = name
            user = user_info.User(sg, nickname=nickname)
            sg_user = user.sg_user()
            if sg_user:
                CACHES_USER[name] = user.sg_user()
            else:
                logger.warning('No user found {}'.format(name))
                return sg_user
        return CACHES_USER[name]

    elif isinstance(name, dict):
        return name


def find_project(project):
    if isinstance(project, str):
        if not project in CACHE_PROJECT.keys():
            project_entity = sg.find_one('Project', [['name', 'is', project]], ReturnField.project)
            if project_entity:
                CACHE_PROJECT[project] = project_entity
        return CACHE_PROJECT.get(project, None)
    else:
        return project


def find_entity(project, entity_type, entity_name):
    cache_key = '{}-{}'.format(project, entity_name)

    if not cache_key in CACHE_ENTITY.keys():
        project = find_project(project)
        filters = [['project', 'is', project], ['code', 'is', entity_name]]
        entity = sg.find_one(entity_type, filters, ReturnField.entity)
        if entity:
            CACHE_ENTITY[cache_key] = entity

    return CACHE_ENTITY.get(cache_key, None)


def find_task(project, entity_type, entity_name, task_name):
    """ find task or ticket """ 
    project_entity = find_project(project)
    if entity_type == 'Ticket': 
        id = entity_name.split('-')[2]
        filters = [['project', 'is', project_entity], ['id', 'is', int(id)]]
        ticket_entity = sg.find_one('Ticket', filters, ReturnField.task) 
        return ticket_entity
    entity = find_entity(project, entity_type, entity_name)
    if project_entity and entity:
        filters = [['project', 'is', project_entity], ['entity', 'is', entity], ['content', 'is', task_name]]
        task_entity = sg.find_one('Task', filters, ReturnField.task)
        return task_entity


def find_department(name): 
    keys = [a.lower() for a in CACHE_DEPARTMENT.keys()]
    if not name in keys: 
        filters = [['code', 'is', name]]
        department_entity = sg.find_one('Department', filters, ReturnField.department)
        if department_entity: 
            CACHE_DEPARTMENT[name] = department_entity
        else: 
            logger.warning('{} not found'.format(name))
            logger.debug(CACHE_DEPARTMENT.keys())

    return CACHE_DEPARTMENT.get(name, {})


def add_involve_people(project, entity_type, entity_name, task_name, user):
    user_entity = find_user(user)
    task_entity = find_task(project, entity_type, entity_name, task_name)
    update_involve_people(task_entity, user_entity)


def update_involve_people(task_entity, user_entity):
    currents = task_entity['sg_involve_with']
    if not user_entity['id'] in [a['id'] for a in currents]:
        involve_people = currents + [user_entity]
        data = {'sg_involve_with': involve_people}
        return sg.update('Task', task_entity['id'], data)
    else:
        logger.debug('Already added')


def user_task_track_name(user='None', project='None', entity_name='None', task_name='None'):
    user_str = user['name'] if isinstance(user, dict) else user
    project_str = project['name'] if isinstance(project, dict) else project

    if not is_python3:    
        user_str = user_str.decode(encoding='UTF-8',errors='strict')
        project_str = project_str.decode(encoding='UTF-8',errors='strict')
        entity_name = entity_name.decode(encoding='UTF-8',errors='strict')
        task_name = task_name.decode(encoding='UTF-8',errors='strict')

        name = user_str +u'-'+ project_str +u'-'+ entity_name +u'-'+ task_name
    else:
        name = user_str +'-'+ project_str +'-'+ entity_name +'-'+ task_name
    return name


def create_entity(project, entity_name, task_name, user):
    entity_type = 'Asset'
    project_entity = find_project(project)
    user_entity = find_user(user)
    entity = find_entity(project, entity_type, entity_name)
    if not entity:
        # Create type rnd
        data = {
            'project': project_entity,
            'sg_asset_type': 'rnd',
            'code': entity_name,
            'sg_status_list': 'omt'}

        entity = sg.create(entity_type, data)

    task_entity = find_task_entity(project_entity, entity, task_name, create=False)
    step = find_etc_step(entity_type)
    if not task_entity:
        data = {
            'project': project_entity,
            'entity': entity,
            'step': step,
            'content': task_name,
            'sg_status_list': 'omt',
            'sg_description': 'Task for timetracking',
            'task_assignees': [user_entity]}

        task_entity = sg.create('Task', data)

    else:
        assignees = task_entity['task_assignees']
        if not user_entity['id'] in [a['id'] for a in assignees]:
            new_assignees = assignees + [user_entity]
            data = {'task_assignees': new_assignees}
            sg.update('Task', task_entity['id'], data)

    return {'project': project_entity, 'entity': entity, 'task': task_entity}


def find_task_entity(project, sg_entity, task_name, create=False):
    project_entity = find_project(project)
    filters = [['project', 'is', project_entity], ['entity', 'is', sg_entity], ['content', 'is', task_name]]
    task_entity = sg.find_one('Task', filters, ReturnField.task)
    if not task_entity:
        if create:
            return create_none_task(project_entity, sg_entity['type'], sg_entity['code'])
        return dict()
    return task_entity


def create_none_entity(project, entity_type, name):
    project_entity = find_project(project)
    entity = sg.find_one(entity_type, [['project', 'is', project_entity],
        ['code', 'is', name]])

    if not entity:
        data = {
            'project': project_entity,
            'code': name,
            'sg_asset_type': 'rnd',
            'sg_status_list': 'omt',
            'description': 'Automatic create pipeline Asset. Do not delete'}
        return sg.create(entity_type, data)
    else:
        return entity


def create_none_task(project, entity_type, name):
    project_entity = find_project(project)
    none_entity = sg.find_one(entity_type, [['project', 'is', project_entity], ['code', 'is', name]], ['code', 'id'])
    if not none_entity:
        none_entity = create_none_entity(project_entity, entity_type, name)
    step = find_etc_step(entity_type)

    task_entity = sg.find_one('Task', [['project', 'is', project_entity],
        ['entity', 'is', none_entity],
        ['content', 'is', NON_TASK]], ReturnField.task)

    if not task_entity:
        data = {
            'project': project_entity,
            'entity': none_entity,
            'sg_status_list': 'omt',
            'content': NON_TASK,
            'step': step,
            'sg_description': 'Automatic create pipeline Task. Do not delete'}
        return sg.create('Task', data)
    else:
        return task_entity


def find_etc_step(entity_type):
    key = '{}-{}'.format(entity_type, 'Etc')
    if not key in CACHE_STEP.keys():
        result = sg.find_one('Step', [['code', 'is', 'Etc'], ['entity_type', 'is', entity_type]], ['code'])
        CACHE_STEP[key] = result
    return CACHE_STEP[key]


def find_active_projects(): 
    skip = ['projectName']
    result = sg.find('Project', [['sg_status', 'is', 'Active']], ['name', 'id', 'sg_project_code'])
    projects = list()

    for project in result: 
        if not project['name'] in skip: 
            projects.append(project)

    return projects


def list_all_users(): 
    userdict = dict()
    result = sg.find('HumanUser', [], ReturnField.user)

    for user in result: 
        userdict[user.get('name')] = user 
    return userdict


def remove_log(logid):
    return sg.delete('TimeLog', logid)


def fetch_logs(start_date=(2021, 3, 1), end_date=None, user=None, project=None, department=None): 
    """ fetch log from start to end date 
    Args: 
        start_date (tuple): date in format of Year/Month/Date e.g. (2021, 2, 28)
        end_date (tuple): date in format of Year/Month/Date e.g. (2021, 2, 28)
        leave blank for current day

    Returns (list): list of TimeLog entity
    """ 
    start = datetime(*start_date).date()
    end = datetime(*end_date).date() if end_date else datetime.now().date()
    date_range = [start, end]
    user_entity = find_user(user) if user else None 
    project_entity = find_project(project) if project else None 
    # department_entity = find_department(department) if department else None 
    
    if start == end: 
        filters = [['date', 'is', start]]
    else: 
        filters = [['date', 'between', date_range]]
    filters.append(['user', 'is', user_entity]) if user_entity else None 
    filters.append(['entity.Task.project', 'is', project_entity]) if project_entity else None 
    filters.append(['user.HumanUser.department.Department.name', 'is', department]) if department else None 
    logs = sg.find('TimeLog', filters, ReturnField.timelog)

    return logs


def entity_sorted(entities, sort_attr, ascending=True): 
    sorted_list = list() 
    temp = dict()

    for entity in entities: 
        value = entity[sort_attr]
        if not value in temp.keys():  
            temp[value] = [entity]
        else: 
            temp[value].append(entity)

    keysort = sorted(temp.keys()) 
    keysort = keysort[::-1] if not ascending else keysort

    for k in keysort: 
        for entity in temp[k]: 
            sorted_list.append(entity)

    return sorted_list


def entity_task_time(project): 
    filters = [['project.Project.name', 'is', project]]
    logs = sg.find('TimeLog', filters, ReturnField.timelog)
    entity_dict = dict()

    timelog = [
        'project', 'entity', 'user', 'duration', 'date', 'sg_type', 'description',
        'entity.Task.content', 'entity.Task.entity', 'entity.Task.updated_at',
        'entity.Task.task_assignees', 'entity.Task.project', 'user.HumanUser.department']

    for log in logs: 
        entity = log['entity.Task.entity']
        if entity: 
            entity_name = entity['name']
            entity_type = entity['type']
            duration = log['duration']
            user = (log.get('user') or dict()).get('name')

            if entity_name and duration and user: 
                if not entity_type in entity_dict.keys(): 
                    entity_dict[entity_type] = {entity_name: {user: duration}}
                else: 
                    if not entity_name in entity_dict[entity_type].keys(): 
                        entity_dict[entity_type][entity_name] = {user: duration}
                    else: 
                        if not user in entity_dict[entity_type][entity_name].keys(): 
                            entity_dict[entity_type][entity_name][user] = duration
                        else: 
                            entity_dict[entity_type][entity_name][user] += duration

    return entity_dict
        # {'type': 'Shot', 'id': 11158, 'name': 'hnm_act1_q0010a_s0110'} 300 {'type': 'HumanUser', 'id': 356, 'name': 'Pop'}


