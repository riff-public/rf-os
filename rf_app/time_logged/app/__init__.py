import _strptime
from datetime import datetime
# Python2 Bug, need a dummy call of strptime so datetime play nicely with threads
datetime.strptime('2012-01-01', '%Y-%m-%d')