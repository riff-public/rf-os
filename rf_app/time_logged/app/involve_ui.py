# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'E:\script\core\rf_app\time_logged\app\submit.ui'
#
# Created: Fri Jan 22 12:04:53 2021
#      by: pyside2-uic  running on PySide2 2.0.0~alpha0
#
# WARNING! All changes made in this file will be lost!

from Qt import QtCore, QtGui, QtWidgets
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

from rf_utils.widget import entity_browser
from rf_utils.widget import display_widget
from rf_utils import icon

class Ui_Involve(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super(Ui_Involve, self).__init__(parent)
        self.centralwidget = QtWidgets.QWidget(self)
        self.setCentralWidget(self.centralwidget)

        # main layout
        self.main_layout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.main_layout.setContentsMargins(10, 10, 10, 10)

        # split layout
        self.split_layout = QtWidgets.QHBoxLayout()
        self.main_layout.addLayout(self.split_layout)

        # photo widget
        self.photo_frame = QtWidgets.QFrame()
        self.photo_frame.setFrameShape(QtWidgets.QFrame.StyledPanel);
        self.photo_frame.setFrameShadow(QtWidgets.QFrame.Sunken);
        self.photo_frame.setStyleSheet('background-color: rgb(40, 40, 40)')
        self.split_layout.addWidget(self.photo_frame)

        self.photo_layout = QtWidgets.QVBoxLayout(self.photo_frame)
        # self.preview_label = QtWidgets.QLabel('Preview')
        # self.preview_label.setAlignment(QtCore.Qt.AlignHCenter|QtCore.Qt.AlignVCenter)
        # self.photo_layout.addWidget(self.preview_label)

        self.photo_label = display_widget.SGPhotoFrame(unknown_photo=icon.nopreview)
        self.photo_label.setAlignment(QtCore.Qt.AlignHCenter|QtCore.Qt.AlignVCenter)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        self.photo_label.setSizePolicy(sizePolicy)
        self.photo_label.setMinimumSize(QtCore.QSize(256, 256))
        self.photo_layout.addWidget(self.photo_label)

        # entity widget
        self.asset_shot_widget = entity_browser.EntityTaskBrowser()
        self.asset_shot_widget.layout.setContentsMargins(0, 0, 0, 0)
        self.asset_shot_widget.entitytype_comboBox.setMinimumWidth(70)
        self.asset_shot_widget.filtertype_comboBox.setMinimumWidth(70)
        self.asset_shot_widget.step_comboBox.setMinimumWidth(90)
        self.asset_shot_widget.task_comboBox.setMinimumWidth(125)
        self.asset_shot_widget.search_lineEdit.setPlaceholderText('Search by name, tag or chapter...')
        self.split_layout.addWidget(self.asset_shot_widget)

        # buttons
        self.button_layout = QtWidgets.QHBoxLayout()
        self.main_layout.addLayout(self.button_layout)

        spacerItem1 = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.button_layout.addItem(spacerItem1)

        self.select_button = QtWidgets.QPushButton(' Select ')
        self.select_button.setMinimumSize(QtCore.QSize(80, 22))
        self.button_layout.addWidget(self.select_button)

        self.close_button = QtWidgets.QPushButton(' Close ')
        self.close_button.setMinimumSize(QtCore.QSize(60, 22))
        self.button_layout.addWidget(self.close_button)

        # set stretches
        self.split_layout.setStretch(0, 3)
        self.split_layout.setStretch(1, 1)


