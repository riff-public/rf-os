# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'E:\script\core\rf_app\time_logged\app\ui.ui'
#
# Created: Mon Jan 11 14:22:14 2021
#      by: pyside2-uic  running on PySide2 2.0.0~alpha0
#
# WARNING! All changes made in this file will be lost!

from Qt import QtCore, QtGui, QtWidgets

from rf_utils.widget import project_widget
# reload(project_widget)
from rf_utils.widget import user_widget
# reload(user_widget)
# from rf_utils import user_info
from rf_utils import project_info
from rf_utils.sg import sg_utils
from rf_utils.widget import display_widget
# reload(download_photo_widget)

# from rf_app.task_manager.sg_services import ShotgunServices

class Ui(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super(Ui, self).__init__(parent)
        self.setObjectName("MainWindow")
        self.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))
        self.resize(1039, 772)
        self.centralwidget = QtWidgets.QWidget(self)
        self.centralwidget.setObjectName("centralwidget")

        self.main_layout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.main_layout.setObjectName("verticalLayout")
        self.main_layout.setSpacing(8)
        self.main_layout.setSizeConstraint(QtWidgets.QLayout.SetDefaultConstraint)

        # splitter
        self.mainSplitter = QtWidgets.QSplitter()
        self.mainSplitter.setOrientation(QtCore.Qt.Vertical)

        self.titleSplitWidget = QtWidgets.QWidget(self.mainSplitter)
        self.viewSplitWidget = QtWidgets.QWidget(self.mainSplitter)
        self.mainSplitter.setSizes([110, 540])
        self.main_layout.addWidget(self.mainSplitter)

        # ------- title layout
        self.titleLayout = QtWidgets.QHBoxLayout(self.titleSplitWidget)

        self.photoLayout = QtWidgets.QVBoxLayout()
        self.photoLayout.setContentsMargins(5, 5, 10, 5)
        self.infoLayout = QtWidgets.QVBoxLayout()
        spacerItem24 = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.infoLayout.addItem(spacerItem24)

        # spacerItem31 = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        # spacerItem32 = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        # self.titleLayout.addItem(spacerItem31)
        self.titleLayout.addLayout(self.photoLayout)
        self.titleLayout.addLayout(self.infoLayout)
        # self.titleLayout.addItem(spacerItem32)
        self.titleLayout.setStretch(0, 20)
        self.titleLayout.setStretch(1, 19)
        # self.titleLayout.setStretch(2, 1)
        # self.titleLayout.setStretch(3, 19)

        # photo
        self.photo_label = display_widget.SGPhotoFrame()
        self.photo_label.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        self.photo_label.setSizePolicy(sizePolicy)
        self.photo_label.setMinimumSize(QtCore.QSize(125, 125))
        self.photo_label.setMaximumHeight(285)
        # self.photo_label.setMaximumSize(QtCore.QSize(250, 250))
        self.photoLayout.addWidget(self.photo_label)

        # sp = self.photo_label.sizePolicy()
        # sp.setHorizontalPolicy(QtWidgets.QSizePolicy.Maximum)
        # self.setSizePolicy(sp)
        # self.photoLayout.setAlignment(self.photo_label, QtCore.Qt.AlignCenter)

        # user
        self.userLayout = QtWidgets.QHBoxLayout()
        self.userWidget = UserComboBox()
        
        # self.userWidget = user_widget.UserComboBox(user=user_info.User(nickname='Fah.r'))
        self.userWidget.setFixedWidth(130)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
        self.userWidget.setSizePolicy(sizePolicy)
        spacerItem4 = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.userLayout.addWidget(self.userWidget)
        self.userLayout.addItem(spacerItem4)
        self.infoLayout.addLayout(self.userLayout)

        # department
        self.deptLayout = QtWidgets.QHBoxLayout()
        spacerItem223 = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.department_text_label = QtWidgets.QLabel()
        self.department_label = QtWidgets.QLabel()
        self.deptLayout.addWidget(self.department_text_label)
        self.deptLayout.addWidget(self.department_label)
        self.deptLayout.addItem(spacerItem223)
        self.infoLayout.addLayout(self.deptLayout)

        # # email
        # self.emailLayout = QtWidgets.QHBoxLayout()
        # spacerItem224 = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        # self.email_text_label = QtWidgets.QLabel()
        # self.email_label = QtWidgets.QLabel()
        # self.emailLayout.addWidget(self.email_text_label)
        # self.emailLayout.addWidget(self.email_label)
        # self.emailLayout.addItem(spacerItem224)
        # self.infoLayout.addLayout(self.emailLayout)

        # date
        self.dateLayout = QtWidgets.QHBoxLayout()
        self.date_label = QtWidgets.QLabel()
        self.calendar = QtWidgets.QCalendarWidget()
        self.calendar.setVerticalHeaderFormat(QtWidgets.QCalendarWidget.NoVerticalHeader)
        self.dateEdit = QtWidgets.QDateEdit()
        self.dateEdit.setDisplayFormat('dd MMM yyyy')
        self.dateEdit.setCalendarPopup(True)
        self.dateEdit.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.dateEdit.setCalendarWidget(self.calendar)
        calSpacerItem2 = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.dateLayout.addWidget(self.date_label)
        self.dateLayout.addWidget(self.dateEdit)
        self.dateLayout.addItem(calSpacerItem2)
        self.infoLayout.addLayout(self.dateLayout)

        # credit
        self.creditLayout = QtWidgets.QHBoxLayout()
        self.credit_text_label = QtWidgets.QLabel()
        self.credit_label = QtWidgets.QLabel()
        spacerItem2 = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.creditLayout.addWidget(self.credit_text_label)
        self.creditLayout.addWidget(self.credit_label)
        self.creditLayout.addItem(spacerItem2)
        self.infoLayout.addLayout(self.creditLayout)

        spacerItem14 = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.infoLayout.addItem(spacerItem14)
        # self.infoLayout.setStretch(0, 2)
        # self.infoLayout.setStretch(1, 1)
        # self.infoLayout.setStretch(2, 1)
        # self.infoLayout.setStretch(3, 1)
        # self.infoLayout.setStretch(4, 1)
        # self.infoLayout.setStretch(5, 2)

        # ------- view layout
        self.viewLayout = QtWidgets.QVBoxLayout(self.viewSplitWidget)
        self.viewLayout.setSpacing(6)

        # project combobox
        self.projectLayout = QtWidgets.QHBoxLayout()
        self.projectLayout.setSpacing(6)
        spacerItemProject = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.project_ui = project_widget.SGProjectComboBox(sg=sg_utils.sg, layout=QtWidgets.QHBoxLayout())
        self.project_ui.label.setText('Project ')
        self.project_ui.projectComboBox.setFixedSize(QtCore.QSize(160, 22))

        # help button
        self.help_button = QtWidgets.QPushButton("Help")
        self.help_button.setFixedSize(QtCore.QSize(65, 22))
        # time button
        self.local_setting_button = QtWidgets.QPushButton()
        self.local_setting_button.setFixedSize(QtCore.QSize(25, 22))

        # setting button
        self.global_setting_button = QtWidgets.QPushButton()
        self.global_setting_button.setFixedSize(QtCore.QSize(25, 22))
        
        self.projectLayout.addWidget(self.project_ui)
        self.projectLayout.addItem(spacerItemProject)
        self.projectLayout.addWidget(self.help_button)
        self.projectLayout.addWidget(self.local_setting_button)
        self.projectLayout.addWidget(self.global_setting_button)
        self.viewLayout.addLayout(self.projectLayout)

        # since combobox
        self.sinceLayout = QtWidgets.QHBoxLayout()
        self.since_label = QtWidgets.QLabel('Active  ')
        self.timeRange_comboBox = QtWidgets.QComboBox(self.centralwidget)
        self.timeRange_comboBox.setFixedSize(QtCore.QSize(160, 22))

        # find task button
        self.list_button_layout = QtWidgets.QHBoxLayout()
        self.list_button_layout.setSpacing(5)

        self.list_button = QtWidgets.QPushButton("Show Task...")
        self.list_button.setFixedSize(QtCore.QSize(120, 22))

        # tool button
        self.find_toolButton = QtWidgets.QToolButton()
        self.find_toolButton.setAutoRaise(True)
        self.find_menu = QtWidgets.QMenu(self)
        self.find_toolButton.setMenu(self.find_menu)

        # setup menu
        self.curr_year_action = QtWidgets.QAction('Show only this year log', self)
        self.curr_year_action.setCheckable(True)
        self.curr_year_action.setChecked(True)
        self.find_menu.addAction(self.curr_year_action)

        self.empty_action = QtWidgets.QAction('Show empty tasks', self)
        self.empty_action.setCheckable(True)
        self.empty_action.setChecked(True)
        self.find_menu.addAction(self.empty_action)

        self.future_task_action = QtWidgets.QAction('Show future tasks', self)
        self.future_task_action.setCheckable(True)
        self.future_task_action.setChecked(True)
        self.find_menu.addAction(self.future_task_action)

        self.future_log_action = QtWidgets.QAction('Show future logs', self)
        self.future_log_action.setCheckable(True)
        self.future_log_action.setChecked(True)
        self.find_menu.addAction(self.future_log_action)

        self.list_button_layout.addWidget(self.list_button)
        self.list_button_layout.addWidget(self.find_toolButton)

        spacerItemSince = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.sinceLayout.addWidget(self.since_label)
        self.sinceLayout.addWidget(self.timeRange_comboBox)
        self.sinceLayout.addLayout(self.list_button_layout)
        self.sinceLayout.addItem(spacerItemSince)
        self.viewLayout.addLayout(self.sinceLayout)

        # create task button
        self.filterLayout = QtWidgets.QHBoxLayout()
        self.filterLayout.setSpacing(5)
        spacerItemTask = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        self.create_button = QtWidgets.QPushButton("Create Task")
        self.create_button.setFixedSize(QtCore.QSize(140, 22))

        # filter bar
        self.search_label = QtWidgets.QLabel('Search ')
        self.filter_line = QtWidgets.QLineEdit()
        self.filter_line.setFixedSize(QtCore.QSize(285, 22))

        # search line
        self.search_line = QtWidgets.QLineEdit()
        self.search_line.setFixedSize(QtCore.QSize(285, 22))

        # search button
        self.search_button = QtWidgets.QPushButton('')
        self.search_button.setFixedSize(QtCore.QSize(25, 22))
        
        # # current year only
        # self.curr_year_checkbox = QtWidgets.QCheckBox('Current year only')
        # self.future_checkbox = QtWidgets.QCheckBox('Future task')
        # self.empty_task_checkbox = QtWidgets.QCheckBox('Empty task')

        self.filterLayout.addWidget(self.search_label)
        self.filterLayout.addWidget(self.filter_line)
        self.filterLayout.addWidget(self.search_line)
        self.filterLayout.addWidget(self.search_button)
        # self.filterLayout.addWidget(self.curr_year_checkbox)
        # self.filterLayout.addWidget(self.future_checkbox)
        # self.filterLayout.addWidget(self.empty_task_checkbox)
        self.filterLayout.addItem(spacerItemTask)
        self.filterLayout.addWidget(self.create_button)
        self.viewLayout.addLayout(self.filterLayout)

        # tree widget
        self.treeWidget = QtWidgets.QTreeWidget(self.centralwidget)
        self.treeWidget.setMinimumHeight(200)
        self.treeWidget.setSortingEnabled(False)
        self.viewLayout.addWidget(self.treeWidget)

        # status bar
        self.statusLabel = QtWidgets.QLabel('Status: ')
        self.viewLayout.addWidget(self.statusLabel)
        self.statusbar = QtWidgets.QStatusBar(self)
        self.statusbar.setObjectName("statusbar")
        self.setStatusBar(self.statusbar)

        self.setCentralWidget(self.centralwidget)
        self.retranslateUi(self)
        QtCore.QMetaObject.connectSlotsByName(self)

    def retranslateUi(self, MainWindow):
        header = self.treeWidget.headerItem()
        # header.setTextAlignment(0, QtCore.Qt.AlignCenter)
        # header.setTextAlignment(1, QtCore.Qt.AlignCenter)
        # header.setTextAlignment(2, QtCore.Qt.AlignCenter)
        # header.setTextAlignment(3, QtCore.Qt.AlignCenter)
        header.setTextAlignment(4, QtCore.Qt.AlignCenter)
        header.setTextAlignment(5, QtCore.Qt.AlignCenter)
        header.setTextAlignment(6, QtCore.Qt.AlignCenter)
        header.setTextAlignment(7, QtCore.Qt.AlignCenter)
        header.setTextAlignment(8, QtCore.Qt.AlignCenter)
        header.setTextAlignment(9, QtCore.Qt.AlignCenter)

        header.setText(0, "Project")
        header.setText(1, "Entity")
        header.setText(2, "Name")
        header.setText(3, "Task")
        header.setText(4, "Total hours")
        header.setText(5, "Created on")
        header.setText(6, "Due date")
        header.setText(7, "Last active")
        header.setText(8, "Add")
        header.setText(9, "Description")

        self.treeWidget.setColumnWidth(0, 145)
        self.treeWidget.setColumnWidth(1, 270)
        self.treeWidget.setColumnWidth(2, 165)
        self.treeWidget.setColumnWidth(3, 110)
        self.treeWidget.setColumnWidth(4, 90)
        self.treeWidget.setColumnWidth(5, 95)
        self.treeWidget.setColumnWidth(6, 95)
        self.treeWidget.setColumnWidth(7, 95)
        self.treeWidget.setColumnWidth(8, 45)
        self.treeWidget.setColumnWidth(9, 200)

class UserComboBox(user_widget.UserComboBox):
    """ user comboBox """
    def __init__(self, user=None, parent = None) :
        super(UserComboBox, self).__init__(parent)

    def allow_debug(self): 
        return self.user.is_admin() or self.user.is_hr()



