#!/usr/bin/env python
# -- coding: utf-8 --

# v.0.0.1 polytag switcher
_title = 'Involve Time Logged'
_version = 'v.0.0.1'
_des = ''
uiName = 'Involve Time Logged'

#Import python modules
import sys
import os
import logging
import getpass

moduleDir = os.path.dirname(sys.modules[__name__].__file__).replace('\\', '/')
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

core = '{}/core'.format(os.environ.get('RFSCRIPT'))
sys.path.append(core)

# import config
import rf_config as config

from rf_utils import log_utils
from rf_utils.ui import load
# reload(load)
from rf_utils.ui import stylesheet

user = '{}-{}'.format(config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)

from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

# maya & module function 
from . import involve_ui

from rf_utils import file_utils

class InvolveTimeLogged(QtWidgets.QMainWindow):
    submit = QtCore.Signal(object)
    windowHidden = QtCore.Signal(bool)

    def __init__(self, parent=None):
        #Setup Window
        super(InvolveTimeLogged, self).__init__(parent)
        self._task_count = 0

        # ui read
        uiFile = '{}/involve_ui.py'.format(moduleDir)
        self.ui = involve_ui.Ui_Involve()
       
        # setup UI
        self.w = 770
        self.h = 315 
        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)
        self.resize(self.w, self.h)
        self.setWindowTitle('{} {} {}'.format(_title, _version, _des))
        self.setWindowModality(QtCore.Qt.ApplicationModal)
        self.setWindowIcon(QtGui.QIcon('{}/icons/main_icon_black.png'.format(moduleDir)))

        self.init_signals()

    def init_signals(self):
        self.ui.asset_shot_widget.entitySelected.connect(self.update_preview)
        self.ui.asset_shot_widget.displayChanged.connect(self.ui.photo_label.reset)
        self.ui.close_button.clicked.connect(self.hide)
        self.ui.select_button.clicked.connect(self.select)

    def hideEvent(self, event):
        super(InvolveTimeLogged, self).hideEvent(event)
        if self._task_count > 0:  # only emit window hidden when at least 1 task is submitted
            self.windowHidden.emit(True)

    def update_preview(self, entity):
        self.ui.photo_label.reset()
        if 'image' in entity:
            image_link = entity['image']
             # get image
            if image_link:
                self.ui.photo_label.show(image_link)

    def select(self):
        entity = self.ui.asset_shot_widget.current_entity()
        task = self.ui.asset_shot_widget.current_task()
        if entity and task:
            entity_text = 'Name: {}\nStep: {}\nTask: {}'.format(entity['code'], task['step.Step.code'], task['content'])

            # pop up to confirm
            qmsgBox = QtWidgets.QMessageBox(self)
            qmsgBox.setText('Did you take part in this {}?\n\n{}\n'.format(entity['type'], entity_text))
            qmsgBox.setWindowTitle('Confirm')
            qmsgBox.addButton('   Yes   ', QtWidgets.QMessageBox.AcceptRole)
            qmsgBox.addButton('   No   ', QtWidgets.QMessageBox.RejectRole)
            qmsgBox.setIcon(QtWidgets.QMessageBox.Question)
            results = qmsgBox.exec_()

            # user confirm to add involvement
            if results == 0:
                self.submit.emit(task)
                self._task_count += 1
                qmsgBox = QtWidgets.QMessageBox(self)
                qmsgBox.setText('Task involvement added!\n\n{}\n'.format(entity_text))
                qmsgBox.setWindowTitle('Success')
                qmsgBox.addButton('   OK   ', QtWidgets.QMessageBox.AcceptRole)
                qmsgBox.setIcon(QtWidgets.QMessageBox.Information)
                qmsgBox.exec_()
        else:
            entity_type = self.ui.asset_shot_widget.current_type()
            qmsgBox = QtWidgets.QMessageBox(self)
            qmsgBox.setText('Please select {}, Step and Task'.format(entity_type))
            qmsgBox.setWindowTitle('Error')
            qmsgBox.addButton('   OK   ', QtWidgets.QMessageBox.AcceptRole)
            qmsgBox.setIcon(QtWidgets.QMessageBox.Critical)
            qmsgBox.exec_()

def show(mode='default'):
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        if mode == 'default':
            maya_win.deleteUI(uiName)
            myApp = InvolveTimeLogged(maya_win.getMayaWindow())
        myApp.show()
    else:
        logger.info('Run in standalone\n')
        app = QtWidgets.QApplication.instance()
        if not app:
            app = QtWidgets.QApplication(sys.argv)
        if mode == 'default':
            myApp = InvolveTimeLogged()
        myApp.show()
        # stylesheet.set_default(app)
    return myApp


if __name__ == '__main__':
    show()