#!/usr/bin/env python
# -- coding: utf-8 --

# v.0.0.1 polytag switcher
_title = 'Time Add'
_version = 'v.0.0.1'
_des = ''
uiName = 'Time Add'

#Import python modules
import sys
import os
import logging
import getpass

moduleDir = os.path.dirname(sys.modules[__name__].__file__).replace('\\', '/')
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

core = '{}/core'.format(os.environ.get('RFSCRIPT'))
sys.path.append(core)

# import config
import rf_config as config
from rf_utils import log_utils
from rf_utils.ui import load
from rf_utils.ui import stylesheet

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)

from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

# maya & module function 
from . import time_add_ui


class AddTimeLogged(QtWidgets.QMainWindow):
    time_changed = QtCore.Signal(int)
    window_closed = QtCore.Signal(bool)

    def __init__(self, parent=None):
        #Setup Window
        super(AddTimeLogged, self).__init__(parent)

        # ui read
        uiFile = '{}/time_add_ui.py'.format(moduleDir)
        self.ui = time_add_ui.Ui_Time_Add()
        self.w = 275
        self.h = 175 

        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)
        self.resize(self.w, self.h)
        self.setWindowModality(QtCore.Qt.ApplicationModal)
        self.setWindowTitle('{} {} {}'.format(_title, _version, _des))
        self.setWindowIcon(QtGui.QIcon('{}/icons/main_icon_black.png'.format(moduleDir)))
        
        self.set_default()
        self.set_focus()
        self.init_signal()

    def closeEvent(self, event):
        self.window_closed.emit(True)

    def init_signal(self):
        self.ui.min_spinBox.valueChanged.connect(self.check_add_enabled)
        self.ui.hr_spinBox.valueChanged.connect(self.check_add_enabled)
        self.ui.min_spinBox.valueChanged.connect(self.emit_time_changed)
        self.ui.hr_spinBox.valueChanged.connect(self.emit_time_changed)

    def emit_time_changed(self):
        hr = self.ui.hr_spinBox.value()
        minute = self.ui.min_spinBox.value()
        total_minute = (hr*60) + minute
        self.time_changed.emit(total_minute)

    def check_add_enabled(self):
        value = False
        if self.ui.hr_spinBox.value() > 0 or self.ui.min_spinBox.value() > 0:
            value = True
        self.ui.add_time_button.setEnabled(value)

    def set_default(self):
        self.ui.hr_spinBox.setValue(0)
        self.ui.min_spinBox.setValue(0)
        self.ui.add_time_button.setEnabled(False)

    def set_focus(self):
        self.ui.min_spinBox.setFocus()

    def convert_hr_to_min(self):
        hour = self.ui.hr_spinBox.value()
        minute = self.ui.min_spinBox.value()
        allMin = minute
        if hour:
            allMin = minute+(hour*60)
        return allMin

    def get_description(self):
        des = self.ui.des_box.toPlainText()
        return des

    # def get_selected_date(self): 
    #     return self.ui.calendar.selectedDate()

def show(mode='default'):
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        if mode == 'default':
            maya_win.deleteUI(uiName)
            myApp = AddTimeLogged(maya_win.getMayaWindow())
        myApp.show()
    else:
        logger.info('Run in standalone\n')
        app = QtWidgets.QApplication.instance()
        if not app:
            app = QtWidgets.QApplication(sys.argv)
        if mode == 'default':
            myApp = AddTimeLogged()
        myApp.show()
        # stylesheet.set_default(app)
        # sys.exit(app.exec_())
    return myApp


if __name__ == '__main__':
    show()