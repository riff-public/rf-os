#!/usr/bin/env python
# -*- coding: utf-8 -*-

# v.1.2.1 - official release
# v.2.0.0 - upgraded to limit admin hour allowed
# v.2.1.0 - Make add time win show previous entry, sort recorded date column descending order automatically
# v.2.1.2 - Make work hour update realtime, fix bugs on hours.
# v.2.2.0 - Add task involvement
# v.2.3.0 - Only allow 1 instance to run
# v.2.3.1 - fix bug for singleton when the app launches with RFLauncher
# v.2.3.2 - fix bug for singleton when switching to production env
# v.2.4.0 - Disable add time button and edit log menu on entity with non-active project
#         - Make create task comboboxs longer 
#         - Add create task config
# v.2.4.1 - Add meeting on Production Technology and I.T. create task config
# v.2.4.2 - Inactive entity change to grey color
#         - UI sizes - main window and create task
#         - Remove category name sorting on create task UI
#         - Force refresh when date changes
#         - When fetch tasks, force table widget to sort by project name and recorded date respectively
# v.2.5.0 - Added show this year record checkbox
# v.2.6.0 - Added move log in right click menu
# v.2.7.0 - Add artist_editable_days config, artist can edit/remove logs within the given day range
#         - Upgrade right-click menu
# v.2.8.0 - Add Admin setting panel
#         - Work hours calcuated from leave days, holidays and weekend
# v.2.8.1 - Add Splash Screen, change title logo to app's logo
#         - Fix bug: reminder keep reminding even when last working day has a log
#         - Add close button(x) to Create Task QMessageBox
# v.2.8.2 - Rename SG project "Management_Work" to "Production_Mangement_Work"
# v.2.8.3 - Fix app not launching when calling datetime.strptime due to Python2.7 bug. (https://bugs.launchpad.net/openobject-server/+bug/947231)
# v.2.8.4 - Fix create task "others" still popping up when user hit 'X' button on create task window
# v.2.9.0 - Fix bug in active task filtering combobox when selected date is not the present day
#         - Move find task filter checkboxes to a toolButton
#         - Add task filter checkbox "Show empty tasks" to filter out tasks with no record
#         - Add task filter checkbox "Show future tasks" to allow showing of task created after selected date
#         - Task that has record on the selected date will always show despite the filters
#         - Optimize refresh code, UI creation code
#         - Disable all widget when loading
#         - Add search button, search line for adding task outside of time range
# v.3.0.0 - Move loading sg to another thread when app starts
#         - Optimize loading of sg data
# v.3.1.0 - Add color to log item of the selected day
#         - Add task filter checkbox "Show future logs" to allow showing of logs created after selected date
# v.3.2.0 - Fetch tasks when user changed
# v.3.2.1 - Fix bug: invalid user name make app freeze 
# v.3.2.2 - Fix bug: sg data not update when changing user while pre-fetch
#         - Fix bug: wrong credit calculation when filter task by project/keyword
# v.3.2.3 - Fix bug: internal data(self.tasks) not updated when add, remove, update or move task is performed
# v.3.2.4 - Fix bug: newly created task does not show up after creation
# v.3.3.0 - Producer & artist share the same rights
# v.3.3.1 - Fix bug: Task involvement does not trigger refetch data
# v.4.0.0 - Port to Python3
# v.4.0.1 - Fix bug in userWidget completer not setting the right item when searching with employee ID
# v.4.0.2 - HR personal will have full hour credit on everyday including holidays
# v.4.1.0 - Add app setting to allow admin/HR to set maximum work hours
# v.4.1.1 - Fix bug in search

_title = 'Time Logged'
_version = 'v.4.1.1'
_des = ''
uiName = 'Time Logged'

#Import python modules
import sys
import os
import logging
import getpass
from functools import partial
from datetime import datetime, timedelta
from collections import OrderedDict
import tempfile
import subprocess

moduleDir = os.path.dirname(sys.modules[__name__].__file__).replace('\\', '/')
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

core = '{}/core'.format(os.environ.get('RFSCRIPT'))
sys.path.append(core)

# import config
import rf_config as config
from rf_utils import log_utils
from rf_utils.ui import load
from rf_utils.ui import stylesheet

user = '{}-{}'.format(config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)

from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

# maya & module function
from rf_app.time_logged.app import ui
from rf_app.time_logged.app import global_settings, app_settings

from rf_app.time_logged import sg_time_log_utils as stlg
from rf_app.time_logged import utils as utils
from rf_utils import thread_pool
from rf_utils import file_utils
from rf_utils import admin
from rf_utils import user_info
from rf_utils.widget.completer import CustomQCompleter

from rf_app.time_logged import reminder
from rf_app.time_logged.app import submit_app
from rf_app.time_logged.app import involve_app
from rf_app.time_logged.app import time_add_app
# reload(time_add_app)

# ========= TO DO =========
# //- Fix text, UI, widget sizes
# //- Add warning when available hour exceed
# //- Make add time and create task child of main window
# //- Remove Pixmap thread safe warning in add time, create task
# //- Add Logo to windows
# //- display duration in hours
# //- Add right-click "remove today entry"
# //- Enable table sorting
# //- Show warning when clicking add time and available credit is 0
# //- Display available credit in red when credit is 0
# //- Add greetings
# //- sort by update date, make sure the latest tasks are on top - just use sorting
# //- add UE tasks
# //- Do not allow adding 0 hr 0 min
# //- History logs should be colored in grey
# //- Task with today's entry displayed in green
# //- Place holder text for add time app description
# //- Add ToolTip
# //- Add help button
# //- Admin mode: make user widget searchable
# //- Set calendar range to 1 year ago to today
# //- Admin mode: edit log history
# //- Fix bug when today credit update when setting date other than today
# //- Fix bug when selecting date other than today in add time cause the tool to create new log 
# //- Fix bug when Admin added time to a log exceeds 8 hours and failed to display on AddTimeApp hour spinBox
# //- Admin mode: add remove log
# //- Fix bug when admin add, then edit today hours, then remove today entry cause total credit to exceeds limit
# //- Fix bug updating today credit when admin edit today entry 
# =========================

TODAY = datetime.today().date()
global PRE_FETCH_TASKS
PRE_FETCH_TASKS = None
global PRE_FETCH_USER
PRE_FETCH_USER = None
ACTIVE_PROJECTS = None

# read global settings
def read_global_settings():
    setting_path = '{}/Pipeline/config/timelogged_settings.yml'.format(config.Env.pipelineGlobal, moduleDir)
    if os.path.exists(setting_path):
        settings = file_utils.ymlLoader(setting_path)
        return settings

# write global settings
def write_global_settings(setting_values):
    setting_path = '{}/Pipeline/config/timelogged_settings.yml'.format(config.Env.pipelineGlobal, moduleDir)
    fh, temp = tempfile.mkstemp(suffix='.yml')
    os.close(fh)
    file_utils.ymlDumper(temp.replace('\\', '/'), setting_values)
    result = admin.copyfile(temp, setting_path)
    if result:
        os.remove(temp)
    return result

class TimeLogged(QtWidgets.QMainWindow):

    def __init__(self, parent=None, thread=None):
        # setup Window
        super(TimeLogged, self).__init__(parent)

        # app vars
        self._need_refetch = True
        self.user = None
        self.maximum_work_time = 480
        self.tasks = []
        self.task_dict = {}
        self.all_user = []

        self.grey_brush = QtGui.QBrush(QtGui.QColor(130, 130, 130))
        self.green_brush = QtGui.QBrush(QtGui.QColor(144, 238, 120))
        self.white_brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))

        dark_green_col = self.green_brush.color().darker()
        self.dark_green_brush = QtGui.QBrush(self.green_brush)
        self.dark_green_brush.setColor(dark_green_col)

        self.task_from_map = OrderedDict([('Today', 1),
                            ('3 days', 3),
                            ('5 days', 5),
                            ('7 days', 7),
                            ('1 month', 30),
                            ('3 months', 90),
                            ('6 months', 180),
                            ('1 year', 365),
                            ('Forever', 9999999)])
        self.exception_days = []
        self.success_sound_path = "{}/sound/level_up.m4a".format(moduleDir)

        # ui read
        self.ui = ui.Ui()
        self.w = 1100
        self.h = 650

        # thread
        self.threadpool = QtCore.QThreadPool() if not thread else thread

        # ----- read settings
        global_settings_value = read_global_settings()
        # override admin mode
        override_admin = global_settings_value.get('admin_override') if isinstance(global_settings_value, dict) else 0
        if override_admin == 1:
            self.allow_debug = override_admin
        else:
            self.allow_debug = self.ui.userWidget.allow_debug()
        # self.allow_debug = False

        # user editable days
        self.artist_editable_days = global_settings_value.get('artist_editable_days') if isinstance(global_settings_value, dict) else 1

        # ---------------------------------------------

        # init functions
        self.setup_ui()
        self.set_default()
        self.init_signals()

        self.today_credit = 0  # self.get_work_hour()
        self.previous_duration = 0
        self.previous_credit = 0  # self.today_credit


    def setup_ui(self):
        # windows
        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)
        self.resize(self.w, self.h)
        self.setWindowTitle('{} {} {}'.format(_title, _version, _des))
        self.setWindowIcon(QtGui.QIcon('{}/icons/main_icon_black.png'.format(moduleDir)))

        # icons
        self.ui.list_button.setIcon(QtGui.QIcon('{}/icons/search_icon.png'.format(moduleDir)))
        self.ui.create_button.setIcon(QtGui.QIcon('{}/icons/new_icon.png'.format(moduleDir)))
        self.ui.help_button.setIcon(QtGui.QIcon('{}/icons/help_icon.png'.format(moduleDir)))
        self.ui.local_setting_button.setIcon(QtGui.QIcon('{}/icons/gear_icon.png'.format(moduleDir)))
        self.ui.global_setting_button.setIcon(QtGui.QIcon('{}/icons/globe_icon.png'.format(moduleDir)))
        self.ui.find_toolButton.setIcon(QtGui.QIcon('{}/icons/filter_icon.png'.format(moduleDir)))
        self.ui.search_button.setIcon(QtGui.QIcon('{}/icons/binocular_icon.png'.format(moduleDir)))

        # add range combobox
        self.ui.timeRange_comboBox.addItems(list(self.task_from_map.keys()))

    def set_default(self):
        # misc
        self.ui.filter_line.setPlaceholderText('Keywords: Project, Name or Task')
        self.ui.filter_line.setVisible(True)
        self.ui.search_line.setVisible(False)
        self.ui.search_line.setStyleSheet("border-width: 1px; border-color: yellow")

        # status
        self.tree_widget_dirty()

        # set time range from 1 year ago till today
        current_day = datetime.today().date()

        # hide code in table
        self.ui.treeWidget.setColumnHidden(1, True)
        # # set alternating row colors
        # self.ui.treeWidget.setAlternatingRowColors(True)

        # from since
        self.ui.timeRange_comboBox.setCurrentIndex(2)

        # curr year
        self.ui.curr_year_action.setChecked(True)
        # show future
        self.ui.future_task_action.setChecked(False)
        # show empty
        self.ui.empty_action.setChecked(True)
        # show future log
        self.ui.future_log_action.setChecked(False)

        # user widget and dateEdit
        self.ui.userWidget.label.setText('User: ')
        self.ui.dateEdit.setVisible(True)
        self.ui.date_label.setText('Date: ')
        self.ui.dateEdit.setDate(QtCore.QDate(TODAY.year, TODAY.month, TODAY.day))  # select TODAY global var to start app in different day other than now
        
        # user widget, only allow search on admin & hr
        if self.allow_debug:
            self.ui.userWidget.checkBox.setChecked(True)
            self.ui.userWidget.set_searchable(True)
            self.ui.userWidget.searchable_by_id = True
            # allow admin to go back up to 1 year
            year_ago = current_day - timedelta(days=548)
            start_date = QtCore.QDate(year_ago.year, year_ago.month, year_ago.day)
            end_date = QtCore.QDate(current_day.year, current_day.month, current_day.day)
            self.ui.dateEdit.setDateRange(start_date, end_date)
            self.ui.calendar.setDateRange(start_date, end_date)

            self.ui.local_setting_button.setHidden(False)
            self.ui.global_setting_button.setHidden(False)
        else:
            # normal users are allowed to go back only till last working day
            last_working_day = reminder.get_last_working_days(username=self.get_selected_user()['sg_name'], num_days=self.artist_editable_days)
            start_date  = QtCore.QDate(last_working_day.year, last_working_day.month, last_working_day.day)
            end_date = QtCore.QDate(current_day.year, current_day.month, current_day.day)
            self.ui.dateEdit.setDateRange(start_date, end_date)
            self.ui.calendar.setDateRange(start_date, end_date)

            self.ui.local_setting_button.setHidden(True)
            self.ui.global_setting_button.setHidden(True)

        self.ui.userWidget.checkBox.setVisible(False)

        # department
        self.ui.department_text_label.setText('Department: ')

        # credit
        self.ui.credit_text_label.setText('Work Hours: ')

        # add ALL to project
        self.ui.project_ui.projectComboBox.insertItem(0, 'ALL')
        self.ui.project_ui.projectComboBox.setCurrentIndex(0)

        # toolTips
        self.ui.project_ui.projectComboBox.setToolTip('Filter tasks by project, select ALL to include every project.')
        self.ui.timeRange_comboBox.setToolTip('Display task that has been activated within the time range before and after the selected date.')
        self.ui.list_button.setToolTip('Press this button to refresh.')
        self.ui.create_button.setToolTip('Can\'t find what you\'ve done? Create your own task!')
        self.ui.filter_line.setToolTip('Filter huge list of tasks for what you\'d like to see using your own words.')
        self.ui.help_button.setToolTip('See app documentation (.pdf).')
        self.ui.local_setting_button.setToolTip('Make change to local configuration.')
        self.ui.global_setting_button.setToolTip('Make changes to global configuration.')
        self.ui.userWidget.setToolTip('Show users (All users for Management and Admin only).')
        self.ui.search_button.setToolTip('Search for tasks outside of the time range.')

        # reset UI
        self.tree_widget_dirty()

        # update photo
        self.setup_user_info(user=self.get_selected_user())

    def ord(self, n):
        return str(n)+("th" if 4<=n%100<=20 else {1:"st",2:"nd",3:"rd"}.get(n%10, "th"))

    def dtStylish(self, dt, f):
        return dt.strftime(f).replace("{th}", self.ord(dt.day))

    def set_title_date(self, date):
        # set date
        self.ui.date_label.setText('Date: {}'.format(self.dtStylish(date, '%A, {th} %B %Y')))

    def init_signals(self):
        self.ui.project_ui.projectComboBox.currentIndexChanged.connect(self.tree_widget_dirty)
        self.ui.list_button.clicked.connect(self.fetch_tasks)
        self.ui.timeRange_comboBox.currentIndexChanged.connect(self.tree_widget_dirty)

        self.ui.find_toolButton.clicked.connect(self.show_find_menu)
        self.ui.search_button.clicked.connect(self.search_item)
        self.ui.curr_year_action.triggered.connect(self.tree_widget_dirty)
        self.ui.future_task_action.triggered.connect(self.tree_widget_dirty)
        self.ui.future_log_action.triggered.connect(self.tree_widget_dirty)
        self.ui.empty_action.triggered.connect(self.tree_widget_dirty)

        if self.allow_debug:
            self.ui.userWidget.comboBox.lineEdit().editingFinished.connect(self.user_searched)
            self.ui.userWidget.comboBox.lineEdit().returnPressed.connect(self.user_searched)
            self.ui.userWidget.comboBox.currentIndexChanged.connect(self.user_changed)
            
        self.ui.create_button.clicked.connect(self.create_button_clicked)
        self.ui.filter_line.textChanged.connect(self.filter_edited)
        self.ui.help_button.clicked.connect(self.display_help)
        self.ui.local_setting_button.clicked.connect(self.show_app_setting_ui)
        self.ui.global_setting_button.clicked.connect(self.show_global_setting_ui)
        self.ui.dateEdit.dateChanged.connect(self.date_changed)
        # right clicked
        self.ui.treeWidget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.ui.treeWidget.customContextMenuRequested.connect(self.task_item_right_clicked)

    def show_app_setting_ui(self):
        self.settingApp = app_settings.show()
        current_settings = {'maximum_work_time': self.maximum_work_time/60}
        self.settingApp.set_initial_values(current_settings)
        self.settingApp.submit.connect(partial(self.edit_app_setting))

    def edit_app_setting(self, value):
        work_time = value.get('maximum_work_time')
        self.maximum_work_time = work_time * 60
        self.settingApp.close()
        if self.tasks:  # refetch
            self.fetch_tasks()

    def show_find_menu(self):
        self.ui.find_toolButton.showMenu()

    def create_button_clicked(self):
        qmsgBox = QtWidgets.QMessageBox(self)
        qmsgBox.setText('What did you work on?')
        qmsgBox.setWindowTitle('Create Task')
        qmsgBox.addButton('  Asset  ', QtWidgets.QMessageBox.AcceptRole)
        qmsgBox.addButton('  Shot  ', QtWidgets.QMessageBox.AcceptRole)
        qmsgBox.addButton(' Others ', QtWidgets.QMessageBox.AcceptRole)
        cancle_button = qmsgBox.addButton(' Cancel ', QtWidgets.QMessageBox.RejectRole)
        cancle_button.setHidden(True)
        qmsgBox.setIcon(QtWidgets.QMessageBox.Question)
        results = qmsgBox.exec_()
        if results == 0:  # Asset
            self.show_involve_ui('asset')
        elif results == 1:  # Shot
            self.show_involve_ui('scene')
        elif results == 2:  # Others
            self.show_submit_ui()

    def show_involve_ui(self, mode):
        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
        self.set_enable_UI(False)
        self.ui.statusLabel.setText('Status: Opening Involve Task UI...')
        self.ui.statusLabel.setStyleSheet('color: yellow')

        # show involve app
        self.involveApp = involve_app.show()
        self.involveApp.setWindowTitle('Task Involvement')

        # auto set type
        self.involveApp.ui.asset_shot_widget.start(project=None, entity_type=mode, filter_type='All', enabled=True)  # set mode, Asset or Scene

        # connect signal
        self.involveApp.submit.connect(self.involve_task)
        self.involveApp.windowHidden.connect(self.fetch_tasks)  # refresh when the windows is closed to reveal new task added

        self.ui.statusLabel.setText('Status: Involve Task UI opened.')
        self.ui.statusLabel.setStyleSheet('color: lightGreen')
        self.set_enable_UI(True)
        QtWidgets.QApplication.restoreOverrideCursor()

    def involve_task(self, task_entity):
        user_entity = self.get_selected_user()
        stlg.update_involve_people(task_entity, user_entity)
        self._need_refetch = True
        self.reset_global_vars()

    def setup_user_info(self, user):
        self.reset_user_info()

        # set department
        if 'department.Department.code' in user:
            department = user['department.Department.code']
            if department:
                self.ui.department_label.setText(department.title())
                self.ui.department_label.setStyleSheet('color: white')

        # set image
        if 'image' in user:
            image_link = user['image']
             # get image
            if image_link:
                self.ui.photo_label.show(image_link)

        # update exception days for the user
        selected_date = self.ui.dateEdit.date().toPython()
        self.exception_days = reminder.get_exception_days(date=selected_date, username=user['sg_name'])

    def reset_global_vars(self):
        global PRE_FETCH_TASKS
        global PRE_FETCH_USER
        PRE_FETCH_TASKS = None
        PRE_FETCH_USER = None

    def user_searched(self):
        name = self.ui.userWidget.comboBox.lineEdit().text()
        index = self.ui.userWidget.comboBox.findText(name, QtCore.Qt.MatchFixedString)
        if index >= 0:
            self.ui.userWidget.comboBox.setCurrentIndex(index)
        else:  # set it back to original
            self.ui.userWidget.blockSignals(True)
            name = self.user['sg_name']
            self.ui.userWidget.comboBox.setCurrentText(name)
            self.ui.userWidget.blockSignals(False)

    def user_changed(self):
        # prepare UI
        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
        self.ui.statusLabel.setText('Status: Fetching tasks...')
        self.ui.statusLabel.setStyleSheet('color: yellow')
        self.ui.list_button.setStyleSheet("")
        self.set_enable_UI(False)

        self.reset_global_vars()
        user = self.get_selected_user()
        # reset vars
        self.tasks = []
        self.task_dict = {}
        
        # start the fetch worker
        self._need_refetch = True
        worker = thread_pool.Worker(self.fetch_tree)
        worker.signals.result.connect(self.user_changed_finished)
        self.threadpool.start(worker)

        # display user info
        self.setup_user_info(user=user)

        # clear focus on user widget 
        self.ui.userWidget.comboBox.clearFocus()
        self.tree_widget_dirty()

    def user_changed_finished(self):
        self._need_refetch = False

        # bring back UI
        self.tree_widget_dirty()
        QtWidgets.QApplication.restoreOverrideCursor()
        self.set_enable_UI(True)

    def reset_user_info(self):
        self.ui.photo_label.reset()
        self.ui.department_label.setText('N/A')
        self.ui.department_label.setStyleSheet('color: grey')

    def get_work_hour(self):
        full_work_hour = self.maximum_work_time
        if self.ui.userWidget.user.is_hr():  # HR Manager always have full hour credit on everyday
            return full_work_hour

        selected_date = self.ui.dateEdit.date().toPython()
        exception_days = [d[0] for d in self.exception_days]

        # Saturday or Sunday
        if selected_date.weekday() in (5, 6):
            return 0

        # Leaves and Studio Holidays
        elif selected_date in exception_days:
            duration = 0
            for day in exception_days:
                if selected_date == day:
                    date, dur = self.exception_days[exception_days.index(day)]
                    duration += dur
                    
            #date, duration = self.exception_days[exception_days.index(selected_date)]
            # half day leave
            if duration < full_work_hour:
                remaining_credit = full_work_hour - duration if full_work_hour - duration > 0 else 0
            else:  # full day leave
                remaining_credit = 0
            return remaining_credit
        else:
            return full_work_hour

    def date_changed(self, date):
        date = date.toPython()
        self.tree_widget_dirty()
        if self.tasks:
            self.fetch_tree_finished()
        else:
            self.fetch_tasks()

    def update_credit(self, time=0, add=False):
        if add:
            credit_limit = self.get_work_hour()
            result = self.today_credit + time
            self.today_credit = result if result <= credit_limit else credit_limit
        else:
            result = self.today_credit - time
            self.today_credit = result if result >= 0 else 0

        self.ui.credit_label.setText(utils.minutes_to_display_hours(minutes=self.today_credit))
        if self.today_credit == 0:
            self.ui.credit_label.setStyleSheet('color: yellow')
        else:
            self.ui.credit_label.setStyleSheet('color: lightGreen')

    def fetch_tasks(self):
        self.set_enable_UI(False)
        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
        self.ui.statusLabel.setText('Status: Fetching tasks...')
        self.ui.statusLabel.setStyleSheet('color: yellow')
        self.ui.list_button.setStyleSheet("")

        # start the fetch worker
        if self._need_refetch:
            worker = thread_pool.Worker(self.fetch_tree)
            worker.signals.result.connect(self.fetch_tree_finished)
            self.threadpool.start(worker)
        else:
            self.fetch_tree_finished()

    def get_selected_user(self):
        curr_text = self.ui.userWidget.comboBox.currentText()
        all_texts = [self.ui.userWidget.comboBox.itemText(i) for i in range(self.ui.userWidget.comboBox.count())]
        if curr_text not in all_texts:
            err = 'Error: Invalid user selection!'
            self.ui.statusLabel.setText(err)
            self.ui.statusLabel.setStyleSheet('color: red')
            self.user = None
            raise RuntimeError(err)
        self.user = self.ui.userWidget.comboBox.itemData(all_texts.index(curr_text), QtCore.Qt.UserRole)
        return self.user

    def search_item(self):
        # show lineEdit
        self.ui.filter_line.setVisible(False)
        self.ui.search_line.setVisible(True)

        completer = CustomQCompleter()
        completer.setCaseSensitivity(QtCore.Qt.CaseInsensitive)
        completer.setModel(QtCore.QStringListModel(list(self.task_dict.keys())))
        self.ui.search_line.setCompleter(completer)

        # # connect rename signals
        # lineEdit.textChanged.connect(partial(self.rename_text_changed, lineEdit, item))
        self.ui.search_line.returnPressed.connect(self.search)
        self.ui.search_line.editingFinished.connect(self.search_finished)
        # self.display.itemDoubleClicked.disconnect(self.add_rename)
        self.ui.search_line.setFocus()

    def search(self):
        keyword = self.ui.search_line.text()
        task_index = self.task_dict[keyword]
        # check if task is already exists in UI
        item = self.get_item_from_task_index(task_index)

        # add to UI if not exists, despite the filters (except keyword filter)
        if not item:
            # filter from search text
            search_value = self.ui.filter_line.text()
            curr_year = self.ui.curr_year_action.isChecked()
            selected_date = self.ui.dateEdit.date().toPython()
            task = self.tasks[task_index]
            if search_value:
                if search_value.lower() not in task.get('code', '').lower():
                    return
            
            logs = task.get('sg_user_logged_items', list())
            record_dates, minutes, descriptions = [], [], []
            # get created date
            created_date = task.get('created_at', '')
            if created_date:
                created_date = created_date.replace(tzinfo=None).date()
            # get last active date
            updated_date = task.get('updated_at', '')
            if updated_date:
                updated_date = updated_date.replace(tzinfo=None).date()
            # loop each log
            for log in logs:
                # get log record date
                record_date = datetime.strptime(log['sg_record_time'], stlg.DATEFORMAT).date()

                # show only records created in current year if show this year record is checked
                if curr_year and record_date.year != selected_date.year:
                    continue       

                # get description
                description = log.get('description')

                # get duration
                minute = log['sg_duration_minute']

                record_dates.append(record_date)
                minutes.append(minute)
                descriptions.append(description)

            # ----- add to UI
            item = self.add_task_item(task, task_index, created_date, updated_date, record_dates, minutes, descriptions)
            self.ui.treeWidget.setCurrentItem(item)

    def search_finished(self):
        self.ui.search_line.clear()
        self.ui.search_line.setVisible(False)
        self.ui.filter_line.setVisible(True)

    def get_selected_project(self):
        project_selected = self.ui.project_ui.projectComboBox.currentText()
        return project_selected

    def fetch_tree(self):
        user = self.get_selected_user()['sg_name']
        global PRE_FETCH_TASKS
        global PRE_FETCH_USER
        if PRE_FETCH_TASKS:
            if user == PRE_FETCH_USER:
                self.tasks = PRE_FETCH_TASKS
                self._need_refetch = False

        if self._need_refetch:
            self.tasks = stlg.list_user_tasks(None, user)
            self._need_refetch = False

        # build task dict {task_code: task_index}
        self.task_dict = {}
        for i, task in enumerate(self.tasks):
            projname = task['project']['name']
            entityname = task['entity']['name']
            taskname = task['name']
            search_key = entityname + ' - ' + taskname + ' ({})'.format(projname)
            self.task_dict[search_key] = i
        return self.tasks

    def filter_edited(self):
        if not self.tasks:
            return
        self.fetch_tree_finished()
    
    def fetch_tree_finished(self):
        # ready the UI
        self.ui.statusLabel.setText('Status: Updating UI...')
        self.ui.statusLabel.setStyleSheet('color: yellow')
        self.ui.treeWidget.clear()
        self.ui.treeWidget.setSortingEnabled(False)

        # ----- Filter for data needed to be displayed
        # get selected project
        sel_proj = self.get_selected_project()
        selected_project = sel_proj if sel_proj!='ALL' else None
        # get active time range
        activeday_filter = self.task_from_map[self.ui.timeRange_comboBox.currentText()]
        # get search text
        search_value = self.ui.filter_line.text()
        # get current today credit
        self.today_credit = self.get_work_hour()

        # present day
        today_date = datetime.today().date()
        # selected day
        selected_date = self.ui.dateEdit.date().toPython()

        # get filter checkboxes
        curr_year = self.ui.curr_year_action.isChecked()
        show_empty = self.ui.empty_action.isChecked()
        show_future = self.ui.future_task_action.isChecked()
        future_log = self.ui.future_log_action.isChecked()

        # loop each task
        task_data = {}
        for i, task in enumerate(self.tasks):
            
            # pre filter by log
            logs = task.get('sg_user_logged_items', list())
            record_dates, minutes, descriptions = [], [], []
            # get created date
            created_date = task.get('created_at', '')
            if created_date:
                created_date = created_date.replace(tzinfo=None).date()
            # get last active date
            updated_date = task.get('updated_at', '')
            if updated_date:
                updated_date = updated_date.replace(tzinfo=None).date()

            has_selectd_day_log = any([datetime.strptime(l['sg_record_time'], stlg.DATEFORMAT).date()==selected_date for l in logs])
            # task of selected day will skip these filter
            if not has_selectd_day_log:
                # if project matched condition
                if selected_project and selected_project != task['project']['name']:
                    continue

                # filter from search text
                if search_value:
                    if search_value.lower() not in task.get('code', '').lower():
                        continue

                # filter by 'Active' combobox
                if updated_date:
                    active_delta = abs((selected_date - updated_date).days)
                    if active_delta > activeday_filter:
                        continue
   
                # if show future is NOT checked, skip task later than selected date
                if created_date:
                    if not show_future and created_date > selected_date:
                        continue

            # loop each log
            for log in logs:
                # get log record date
                record_date = datetime.strptime(log['sg_record_time'], stlg.DATEFORMAT).date()

                # do NOT show log created after selected date
                if not future_log and record_date > selected_date:
                    continue
                # show only records created in current year if show this year record is checked
                if curr_year and record_date.year != selected_date.year:
                    continue       

                # get description
                description = log.get('description')

                # get duration
                minute = log['sg_duration_minute']
                if selected_date == record_date:  # update today's credit if log is created on selected day
                    self.today_credit -= minute  

                record_dates.append(record_date)
                minutes.append(minute)
                descriptions.append(description)

            # if show empty task in NOT checked, skip task without any record
            if not record_dates and not show_empty:
                continue

            # ----- add to UI
            item = self.add_task_item(task, i, created_date, updated_date, record_dates, minutes, descriptions)

        # sort
        self.ui.treeWidget.setSortingEnabled(True)
        self.ui.treeWidget.sortItems(0, QtCore.Qt.DescendingOrder)  # sort by project name
        self.ui.treeWidget.sortItems(5, QtCore.Qt.DescendingOrder)  # sort by recorded date

        # update creadit
        self.update_credit()
        self._need_refetch = False

        # bring back UI
        self.set_enable_UI(True)
        self.ui.statusLabel.setText('Status: Fetch tasks finished.')
        self.ui.statusLabel.setStyleSheet('color: lightGreen')
        QtWidgets.QApplication.restoreOverrideCursor()

    def add_task_item_widget(self):
        # new task item
        item = QtWidgets.QTreeWidgetItem(self.ui.treeWidget)
        # set alignment
        item.setTextAlignment(4, QtCore.Qt.AlignCenter)
        item.setTextAlignment(5, QtCore.Qt.AlignCenter)
        item.setTextAlignment(6, QtCore.Qt.AlignCenter)
        item.setTextAlignment(7, QtCore.Qt.AlignCenter)
        item.setTextAlignment(8, QtCore.Qt.AlignCenter)
        # add button
        add_button = QtWidgets.QPushButton(" + ", self.ui.treeWidget)
        add_button.clicked.connect(partial(self.show_add_time_ui, item))
        add_button.clicked.connect(partial(self.activate_tree_item, item))
        add_button.setMaximumSize(QtCore.QSize(100, 30))
        self.ui.treeWidget.setItemWidget(item, 8, add_button)

        return item, add_button

    def add_log_item_widget(self, parent, brush=None):
        item = QtWidgets.QTreeWidgetItem(parent)
        item.setTextAlignment(4, QtCore.Qt.AlignCenter)  # total hours
        item.setTextAlignment(5, QtCore.Qt.AlignCenter)  # record date
        item.setTextAlignment(6, QtCore.Qt.AlignCenter)  # due date
        item.setTextAlignment(7, QtCore.Qt.AlignCenter)  # last active
        # set color
        if not brush:
            brush = self.green_brush
        item.setForeground(4, brush)
        item.setForeground(5, brush)
        item.setForeground(9, brush)
        return item

    def add_task_item(self, task, task_index, created_date, updated_date, record_dates, minutes, descriptions):
        selected_date = self.ui.dateEdit.date().toPython()
        has_selectd_day_log = selected_date in record_dates
        duration = 0
        projname = task['project']['name']
        entityname = task['entity']['name']
        taskname = task['name']
        taskcode = task['code']

        item_0, add_button = self.add_task_item_widget()
        item_0.setData(QtCore.Qt.UserRole, 0, task_index)

        # add texts
        item_0.setText(0, projname)
        item_0.setText(1, taskcode)
        item_0.setText(2, entityname)
        item_0.setText(3, taskname)
        item_0.setText(5, str(created_date))
        item_0.setText(6, '-')
        item_0.setText(7, str(updated_date))
        
        if task['project']['name'] not in ACTIVE_PROJECTS:
            add_button.setEnabled(False)
        
        # add history log under task item
        for record_date, minute, description in zip(record_dates, minutes, descriptions):
            log_brush = self.dark_green_brush if record_date == selected_date else self.grey_brush
            item_1 = self.add_log_item_widget(parent=item_0, brush=log_brush)
            item_1.setData(QtCore.Qt.UserRole, 0, record_date)
            
            item_1.setText(4, utils.minutes_to_display_hours(minutes=minute))
            item_1.setText(5, str(record_date))
            item_1.setText(9, description)

            duration += minute

        # set color
        # if task has today's entry, set it to green
        brush = self.white_brush if not has_selectd_day_log else self.green_brush
        # proj active status. inactive project will be disabled and use grey or dark green color
        inactive = True if projname not in ACTIVE_PROJECTS else False
        if inactive:
            dark_col = brush.color().darker()
            brush = QtGui.QBrush(brush)
            brush.setColor(dark_col)
            add_button.setEnabled(False)
        item_0.setForeground(0, brush)
        item_0.setForeground(1, brush)
        item_0.setForeground(2, brush)
        item_0.setForeground(3, brush)
        item_0.setForeground(4, brush)
        item_0.setForeground(5, brush)
        item_0.setForeground(6, brush)
        item_0.setForeground(7, brush)

        # set total hours of the whole task
        item_0.setText(4, utils.minutes_to_display_hours(minutes=duration))
        return item_0

    def task_item_right_clicked(self, pos):
        item = self.ui.treeWidget.itemAt(pos)
        if not isinstance(item, QtWidgets.QTreeWidgetItem):
            return
        
        menu_items = []
        # --- check if item is topLevel item
        # child item (logged history)
        if item.parent():
            # get task
            task_item = item.parent()
            task_index = task_item.data(QtCore.Qt.UserRole, 0)
            task = self.tasks[task_index]
            allow_edit = True if self.allow_debug else False

            if 'sg_user_logged_items' in task and task['sg_user_logged_items']:
                # get recorded date from widget item
                item_record_date = item.data(QtCore.Qt.UserRole, 0)

                # allow artist to edit if the date is not prior to the calendar settings
                if item_record_date >= self.ui.dateEdit.minimumDate().toPython():
                    allow_edit = True

                # get duration, description
                logs = task['sg_user_logged_items']
                logId = None
                minute = 0
                description = ''
                for log in logs:
                    record_date = log['sg_record_time']
                    record_date = datetime.strptime(record_date, '%Y-%m-%d').date()
                    if record_date == item_record_date:
                        minute = log['sg_duration_minute']
                        description = log.get('description')
                        logId = log['id']
                        break

            # only allow admin to edit log
            if allow_edit:
                # edit log
                edit_log_action = QtWidgets.QAction('Edit log...', self)
                edit_log_action.triggered.connect(lambda: self.show_edit_time_ui(task, task_index, item_record_date, minute, description, logId))
                menu_items.append(edit_log_action)

                if task['project']['name'] not in ACTIVE_PROJECTS:
                    edit_log_action.setEnabled(False)

                    # move log
                    move_log_menu = QtWidgets.QMenu('Move log', self)
                    menu_items.append(move_log_menu)
                    # find tasks to move to
                    rootItem = self.ui.treeWidget.invisibleRootItem()
                    for c in range(rootItem.childCount()):
                        dest_task_item = rootItem.child(c)
                        dest_task_index = dest_task_item.data(QtCore.Qt.UserRole, 0)
                        dest_task = self.tasks[dest_task_index]

                        if dest_task['project']['name'] in ACTIVE_PROJECTS:

                            # only allow moving log to task without log on the same day
                            for l in dest_task['sg_user_logged_items']:
                                dest_record_date = datetime.strptime(l['sg_record_time'], '%Y-%m-%d').date()
                                if dest_record_date == item_record_date:
                                    break
                            else:
                                # add menu item for destination task to move to
                                dest_action = QtWidgets.QAction(' - '.join(dest_task['code'].split('-')[1:]), self)
                                dest_action.triggered.connect(partial(self.move_log, dest_task_item, dest_task, item_record_date, minute, description, task, task_item, logId))
                                move_log_menu.addAction(dest_action)

                # remove log
                remove_log_action = QtWidgets.QAction('Remove log', self)
                remove_log_action.triggered.connect(lambda: self.remove_entry_clicked(task, task_item, date=item_record_date))
                menu_items.append(remove_log_action)

        # topLevel item (tasks)
        else:
            task_index = item.data(QtCore.Qt.UserRole, 0)
            task = self.tasks[task_index]
            # make item active
            self.activate_tree_item(item)
            if 'sg_user_logged_items' in task and task['sg_user_logged_items']:
                date_selected = self.ui.dateEdit.date().toPython()
                logs = task['sg_user_logged_items']
                for log in logs:
                    record_date = log['sg_record_time']
                    record_date = datetime.strptime(record_date, '%Y-%m-%d').date()
                    if record_date == date_selected:
                        # remove today's entry
                        remove_today_action = QtWidgets.QAction('Remove record on this day', self)
                        remove_today_action.triggered.connect(lambda: self.remove_entry_clicked(task, item, date=date_selected))
                        menu_items.append(remove_today_action)

        if menu_items:
             # create menu
            rightClickMenu = QtWidgets.QMenu(self)
            for mi in menu_items:
                if isinstance(mi, QtWidgets.QAction):
                    rightClickMenu.addAction(mi)
                elif isinstance(mi, QtWidgets.QMenu):
                    rightClickMenu.addMenu(mi)
            rightClickMenu.exec_(self.ui.treeWidget.mapToGlobal(pos))

    def move_log(self, dest_item, dest_task, record_date, minute, description, old_task, old_item, old_logId):
        qmsgBox = QtWidgets.QMessageBox(self)
        old_task_name = '-'.join(old_task.get('code').split('-')[1:])
        log_name = '{}[{}]'.format(old_task_name, utils.minutes_to_display_hours(minutes=minute))
        task_name = '-'.join(dest_task.get('code').split('-')[1:])
        qmsgBox.setText('This will move your record on {}\n   {}\n\nTo task\n   {}\n\nAre you sure?'.format(self.dtStylish(record_date, '%A, {th} %B %Y'), log_name, task_name))
        qmsgBox.setWindowTitle('Move Log')
        qmsgBox.addButton('  Yes  ', QtWidgets.QMessageBox.AcceptRole)
        qmsgBox.addButton('  No  ', QtWidgets.QMessageBox.RejectRole)
        qmsgBox.setIcon(QtWidgets.QMessageBox.Warning)
        results = qmsgBox.exec_()

        if results == 0:
            # lock UI
            QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
            self.set_enable_UI(False)

            # set status
            self.ui.statusLabel.setText('Status: Moving Time Logged...')
            self.ui.statusLabel.setStyleSheet('color: yellow')

            # start worker
            worker = thread_pool.Worker(self.add_time_logged, dest_task, minute, description, dest_item, record_date)
            worker.signals.result.connect(partial(self.move_time_logged_finished, old_task, old_item, old_logId))
            self.threadpool.start(worker)

    def move_time_logged_finished(self, old_task, old_item, old_logId, results, *args, **kwargs):
        if results:
            # remove old log
            stlg.remove_log(logid=old_logId)

            self._need_refetch = True
            self.reset_global_vars()
            self.fetch_tree()
            self.update_tree(update_task=results['timelog']['entity'], item=results['item'])
            self.update_tree(update_task=old_task, item=old_item)

            # update credit only if log_date is current day
            if results['log_date'] == self.ui.dateEdit.date().toPython():
                self.update_credit(time=self.addTimeApp.convert_hr_to_min())

        self.set_enable_UI(True)
        QtWidgets.QApplication.restoreOverrideCursor()

    def get_item_from_task_index(self, task_index):
        rootItem = self.ui.treeWidget.invisibleRootItem()
        for c in range(rootItem.childCount()):
            task_item = rootItem.child(c)
            data_index = task_item.data(QtCore.Qt.UserRole, 0)
            if data_index == task_index:
                return task_item

    def show_edit_time_ui(self, task, task_index, date, old_duration, description, logId):
        self.ui.statusLabel.setText('Status: Opening Edit Log UI...')
        self.ui.statusLabel.setStyleSheet('color: yellow')

        # set date to record date, this will change item objects if the selected date is not the same as edited log 
        self.ui.dateEdit.setDate(QtCore.QDate(date.year, date.month, date.day))

        # update previous credit and previous duration to match the date selected
        self.previous_credit = self.today_credit
        self.previous_duration = old_duration

         # show time add app
        self.addTimeApp = time_add_app.show()

        # set duration on record date
        hour, minute = utils.minute_to_hour(minutes=old_duration)
        # set maximum in hour spinbox to 24
        self.addTimeApp.ui.hr_spinBox.setMaximum(24)
        self.addTimeApp.ui.hr_spinBox.setValue(hour)
        self.addTimeApp.ui.min_spinBox.setValue(minute)

        # set description
        self.addTimeApp.ui.des_box.setPlainText(description)
        
        # set up dialog
        self.addTimeApp.setWindowTitle('Edit Log')
        self.addTimeApp.ui.date_label.setText(self.dtStylish(date, '%A, {th} %B %Y'),)
        self.addTimeApp.ui.text_label.setText('{}  {}  {}'.format(task['project']['name'],
                                                                    task['entity']['name'],
                                                                    task['content']))
        self.addTimeApp.ui.date_label.setStyleSheet('color: yellow')
        self.addTimeApp.ui.text_label.setStyleSheet('color: yellow')

        # signal
        self.addTimeApp.time_changed.connect(self.time_editted)
        self.addTimeApp.window_closed.connect(self.restore_actual_time_display)
        self.addTimeApp.ui.add_time_button.clicked.connect(partial(self.edit_log_clicked, task, task_index, logId, old_duration))

        self.ui.statusLabel.setText('Status: Edit Log opened.')
        self.ui.statusLabel.setStyleSheet('color: lightGreen')

    def edit_log_clicked(self, task, task_index, logId, old_duration):
        # get duration
        duration = self.addTimeApp.convert_hr_to_min()

        if (duration - old_duration) > self.today_credit:
            hour, minute = utils.minute_to_hour(minutes=old_duration)
            self.addTimeApp.ui.hr_spinBox.setValue(hour)
            self.addTimeApp.ui.min_spinBox.setValue(minute)
            self.display_error(text='No hours left!')
            return

        self.ui.statusLabel.setText('Status: Editing log...')
        self.ui.statusLabel.setStyleSheet('color: yellow')

        # lock UI
        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
        self.set_enable_UI(False)

        # get description
        des = self.addTimeApp.get_description()
        
        # start worker
        worker = thread_pool.Worker(self.update_time_logged, task_index, logId, duration, des, old_duration)
        worker.signals.result.connect(self.update_time_logged_finished)
        self.threadpool.start(worker)
        self.addTimeApp.ui.setEnabled(False)

    def update_time_logged(self, task_index, logId, duration, description, old_duration):
        timelog = stlg.update_time_logged(logId, duration=duration, description=description)
        return {'timelog': timelog, 'task_index': task_index, 'new_duration': duration, 'old_duration': old_duration}

    def update_time_logged_finished(self, results):
        timelog = results['timelog']
        updated_log_record_date = timelog['date']
        task_index = results['task_index']
        task_item = self.get_item_from_task_index(task_index=task_index)
        
        self._need_refetch = True
        self.reset_global_vars()
        self.fetch_tree()

        if task_item:
            self.update_tree(update_task=timelog['entity'], item=task_item)
        if updated_log_record_date == str(self.ui.dateEdit.date().toPython()):
            add = True
            if results['new_duration'] >= results['old_duration']:
                add = False
            self.update_credit(time=abs(results['new_duration'] - results['old_duration']), add=add)
        self.addTimeApp.hide()
        
        self.set_enable_UI(True)
        self.select_log_item(task_item=task_item, record_date=updated_log_record_date)
        QtWidgets.QApplication.restoreOverrideCursor()

    def select_log_item(self, task_item, record_date):
        # select the same log widget
        for i in range(task_item.childCount()):
            log_item = task_item.child(i)
            log_record_date = log_item.data(QtCore.Qt.UserRole, 0)
            if record_date == str(log_record_date):
                self.ui.treeWidget.setCurrentItem(log_item)
                return

    def set_enable_UI(self, value):
        self.ui.setEnabled(value)
        # self.ui.dateEdit.setEnabled(value)
        # self.ui.userWidget.setEnabled(value)
        # self.ui.project_ui.projectComboBox.setEnabled(value)
        # self.ui.timeRange_comboBox.setEnabled(value)
        # self.ui.list_button.setEnabled(value)
        # self.ui.create_button.setEnabled(value)
        # self.ui.filter_line.setEnabled(value)
        # self.ui.treeWidget.setEnabled(value)
        # self.ui.help_button.setEnabled(value)
        # self.ui.global_setting_button.setEnabled(value)
        # self.ui.search_button.setEnabled(value)
        # self.ui.find_toolButton.setEnabled(value)

    def remove_entry_clicked(self, task, item, date):
        self.ui.statusLabel.setText('Status: Removing entry...')
        self.ui.statusLabel.setStyleSheet('color: yellow')

        # lock UI
        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
        self.set_enable_UI(False)

        # start worker
        worker = thread_pool.Worker(self.remove_entry, task, item, date)
        worker.signals.result.connect(self.remove_entry_finished)
        self.threadpool.start(worker)

    def remove_entry(self, task, item, date):
        logs = task['sg_user_logged_items']
        minute = 0
        if date == self.ui.dateEdit.date().toPython():
            is_today = True
        else:
            is_today = False
        for log in logs:
            record_date = log['sg_record_time']
            record_date = datetime.strptime(record_date, stlg.DATEFORMAT).date()
            if record_date == date:
                stlg.remove_log(logid=log['id'])
                minute = log['sg_duration_minute']
                break
        results = {'task': task, 'minute': minute, 'item': item, 'today': is_today}
        return results

    def remove_entry_finished(self, results):
        if results['minute']:
            self._need_refetch = True
            self.reset_global_vars()
            self.fetch_tree()

            self.update_tree(update_task=results['task'], item=results['item'])
            if results['today']:
                self.update_credit(time=results['minute'], add=True)
        else:
            self.ui.statusLabel.setText('Status: Update tasks finished.')
            self.ui.statusLabel.setStyleSheet('color: lightGreen')

        # restore UI
        self.set_enable_UI(True)
        self.ui.treeWidget.setCurrentItem(results['item'])
        QtWidgets.QApplication.restoreOverrideCursor()

    def update_tree(self, update_task, item):
        self.ui.statusLabel.setText('Status: Updating UI...')
        self.ui.statusLabel.setStyleSheet('color: yellow')

        duration = 0
        selected_date = self.ui.dateEdit.date().toPython()
        curr_year = self.ui.curr_year_action.isChecked()
        future_log = self.ui.future_log_action.isChecked()
        for task in self.tasks:
            if task['id'] == update_task['id']:
                # remove all existing history log from widget
                has_today = False
                item.takeChildren()
                if 'sg_user_logged_items' in task and task['sg_user_logged_items']:
                    logs = task['sg_user_logged_items']
                    for i, log in enumerate(logs):
                        record_date_str = log['sg_record_time']
                        record_date = datetime.strptime(record_date_str, stlg.DATEFORMAT).date()

                        # do NOT show log created after selected date
                        if not future_log and record_date > selected_date:
                            continue
                        # show only records created in current year if show this year record is checked
                        if curr_year and record_date.year != selected_date.year:
                            continue       

                        minute = log['sg_duration_minute']
                        description = log.get('description')
                        log_brush = self.dark_green_brush if record_date == selected_date else self.grey_brush
                        child_item = self.add_log_item_widget(parent=item, brush=log_brush)
                        child_item.setData(QtCore.Qt.UserRole, 0, record_date)
                        child_item.setText(4, utils.minutes_to_display_hours(minutes=minute))
                        child_item.setText(5, record_date_str)
                        child_item.setText(9, description)

                        if selected_date == record_date:
                            has_today = True
                        duration += minute

                item.setText(4, utils.minutes_to_display_hours(minutes=duration))

                # proj active status
                projname = task['project']['name']
                inactive = True if projname not in ACTIVE_PROJECTS else False
                # if task has today's entry, set it to green
                brush = self.white_brush if not has_today else self.green_brush
                if inactive:
                    dark_col = brush.color().darker()
                    brush = QtGui.QBrush(brush)
                    brush.setColor(dark_col)

                item.setForeground(0, brush)
                item.setForeground(1, brush)
                item.setForeground(2, brush)
                item.setForeground(3, brush)
                item.setForeground(4, brush)
                item.setForeground(5, brush)
                item.setForeground(6, brush)
                item.setForeground(7, brush)
                break

        self.ui.statusLabel.setText('Status: Update tasks finished.')
        self.ui.statusLabel.setStyleSheet('color: lightGreen')

    def tree_widget_dirty(self):
        self.ui.treeWidget.clear()
        self.ui.credit_label.setText('N/A')
        self.ui.credit_label.setStyleSheet('color: grey')
        self.ui.statusLabel.setText('Status: Please press "Find Task".')
        self.ui.statusLabel.setStyleSheet('color: yellow')
        self.ui.list_button.setStyleSheet("QPushButton {border-width: 1px; border-color: yellow;}")

    def display_help(self):
        # import webbrowser
        # url = 'https://connect.riff-studio.com/manual/timelog'
        # webbrowser.open(url)
        pipelineDrive = config.Env.pipelineGlobal
        path = '{}/Pipeline/documents/manuals/time_logged/timelog.pdf'.format(pipelineDrive)
        subprocess.Popen([path], shell=True)

    def activate_tree_item(self, item):
        # make sure tree widget item is the current item when add time button clicked
        self.ui.treeWidget.setCurrentItem(item)

    def show_global_setting_ui(self):
        self.globalSettingApp = global_settings.show()
        global_settings_value = read_global_settings()
        self.globalSettingApp.set_initial_values(global_settings_value)
        self.globalSettingApp.submit.connect(partial(self.edit_global_settings))

    def edit_global_settings(self, settings):
        # write to yml
        result = write_global_settings(settings)
        self.globalSettingApp.close()

    def show_add_time_ui(self, item):
        if self.today_credit == 0:
            self.display_error(text='No hours left!')
            return

        selected_date = self.ui.dateEdit.date().toPython()
        task_index = item.data(QtCore.Qt.UserRole, 0)
        task = self.tasks[task_index]
        logId = None
        old_duration = 0
        description = ''
        if 'sg_user_logged_items' in task and task['sg_user_logged_items']:
            # get duration, description
            logs = task['sg_user_logged_items']
            for log in logs:
                record_date = log['sg_record_time']
                record_date = datetime.strptime(record_date, '%Y-%m-%d').date()
                if record_date == selected_date:
                    old_duration = log['sg_duration_minute']
                    description = log.get('description')
                    logId = log['id']
                    # print minute, description, record_date
                    break

        # store old values before launching the UI
        self.previous_credit = self.today_credit
        self.previous_duration = old_duration

        self.ui.statusLabel.setText('Status: Opening Add Time Logged UI...')
        self.ui.statusLabel.setStyleSheet('color: yellow')

        # init add time UI
        self.addTimeApp = time_add_app.show()
        self.addTimeApp.setWindowTitle('Add hours')
        self.addTimeApp.ui.date_label.setStyleSheet('color: yellow')
        self.addTimeApp.ui.text_label.setStyleSheet('color: yellow')
        self.addTimeApp.ui.des_box.setPlaceholderText('Some more details about your work?')

        # set ui details
        self.addTimeApp.ui.date_label.setText(self.dtStylish(selected_date, '%A, {th} %B %Y'),)
        self.addTimeApp.ui.text_label.setText('{}  {}  {}'.format(task['project']['name'],
                                                                task['entity']['name'],
                                                                task['content']))
        # set duration on record date
        hour, minute = utils.minute_to_hour(minutes=old_duration)
        # set maximum in hour spinbox to 24
        self.addTimeApp.ui.hr_spinBox.setMaximum(24)
        self.addTimeApp.ui.hr_spinBox.setValue(hour)
        self.addTimeApp.ui.min_spinBox.setValue(minute)

        # set description
        self.addTimeApp.ui.des_box.setPlainText(description)

        # signal
        self.addTimeApp.time_changed.connect(self.time_editted)
        self.addTimeApp.window_closed.connect(self.restore_actual_time_display)
        if logId:
            self.addTimeApp.ui.add_time_button.clicked.connect(partial(self.edit_log_clicked, task, task_index, logId, old_duration))
        else:
            self.addTimeApp.ui.add_time_button.clicked.connect(partial(self.add_time_clicked, task, item))

        self.ui.statusLabel.setText('Status: Add Time Logged opened.')
        self.ui.statusLabel.setStyleSheet('color: lightGreen')
    
    def time_editted(self, value):
        display_credit = self.previous_credit + (self.previous_duration - value)
        total_time_left = self.previous_credit + self.previous_duration
        # if user exceed the maximum time allowed at the moment
        if display_credit < 0:
            self.addTimeApp.blockSignals(True)
            # set spinbox back to max time allowed
            hour, minutes = utils.minute_to_hour(total_time_left)
            self.addTimeApp.ui.hr_spinBox.setValue(hour)
            self.addTimeApp.ui.min_spinBox.setValue(minutes)
            # set credit to 0
            display_text = utils.minutes_to_display_hours(minutes=0)
            self.ui.credit_label.setText(display_text)
            self.addTimeApp.blockSignals(False)
            # popup warning
            self.display_error(text='You only have {} hours left!'.format(utils.minutes_to_display_hours(minutes=total_time_left)))
        else:
            display_text = utils.minutes_to_display_hours(minutes=display_credit)
            self.ui.credit_label.setText(display_text)
            if display_credit == 0:
                self.ui.credit_label.setStyleSheet('color: yellow')
            else:
                self.ui.credit_label.setStyleSheet('color: lightGreen')

    def restore_actual_time_display(self):
        display_text = utils.minutes_to_display_hours(minutes=self.today_credit)
        self.ui.credit_label.setText(display_text)
        if self.today_credit == 0:
            self.ui.credit_label.setStyleSheet('color: yellow')
        else:
            self.ui.credit_label.setStyleSheet('color: lightGreen')

    def display_error(self, text):
        qmsgBox = QtWidgets.QMessageBox(self)
        qmsgBox.setText(text)
        qmsgBox.setWindowTitle('Error')
        qmsgBox.addButton('  OK  ', QtWidgets.QMessageBox.AcceptRole)
        qmsgBox.setIcon(QtWidgets.QMessageBox.Critical)
        results = qmsgBox.exec_()

    def add_time_clicked(self, task, item):
        # get duration
        duration = self.addTimeApp.convert_hr_to_min()
        log_date = self.ui.dateEdit.date()
        log_date = log_date.toPython()

        # do not allow adding time to today - if duration exceed today credit
        if self.today_credit - duration < 0:
            self.display_error(text='You only have {} hours left!'.format(utils.minutes_to_display_hours(minutes=self.today_credit)))
            return

        # get description
        des = self.addTimeApp.get_description()

        # lock UI
        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
        self.set_enable_UI(False)

        # set status
        self.ui.statusLabel.setText('Status: Adding Time Logged...')
        self.ui.statusLabel.setStyleSheet('color: yellow')

        # start worker
        worker = thread_pool.Worker(self.add_time_logged, task, duration, des, item, log_date)
        worker.signals.result.connect(self.add_time_logged_finished)
        self.threadpool.start(worker)
        self.addTimeApp.ui.setEnabled(False)

    def add_time_logged(self, task, duration, description, item, log_date):
        user = task['user']
        project = task['project']['name']
        log_date_str = str(log_date)
        timelog = stlg.add_time_logged(
            project= project,
            entity_type=task['entity']['type'],
            entity_name=task['entity']['name'],
            task_name=task['content'],
            user=user,
            duration=duration,
            logtype='User',
            description=description,
            add=True,
            date_filter=log_date_str, 
            create_date=log_date_str
            )
        return {'timelog': timelog, 'item': item, 'log_date': log_date}

    def add_time_logged_finished(self, results):
        self._need_refetch = True
        self.reset_global_vars()
        self.fetch_tree()
        self.update_tree(update_task=results['timelog']['entity'], item=results['item'])

        # update credit only if log_date is current day
        if results['log_date'] == self.ui.dateEdit.date().toPython():
            self.update_credit(time=self.addTimeApp.convert_hr_to_min())
        self.addTimeApp.hide()

        self.set_enable_UI(True)
        QtWidgets.QApplication.restoreOverrideCursor()
        
        # play sound if it's today and work hours is 0
        if self.ui.dateEdit.date().toPython() == datetime.today().date() and self.today_credit == 0:
            self.play_sound(path=self.success_sound_path)

    def play_sound(self, path):
        ffplay_path = config.Software.ffplay
        cmds = [ffplay_path, '-nodisp', '-autoexit', self.success_sound_path]
        subprocess.Popen(cmds, shell=True)

    def show_submit_ui(self):
        self.ui.statusLabel.setText('Status: Opening Create Task UI...')
        self.ui.statusLabel.setStyleSheet('color: yellow')

        self.submitApp = submit_app.show()
        self.submitApp.setFixedSize(QtCore.QSize(385, 165))
        self.submitApp.setWindowTitle('Create task')

        self.submitApp.ui.submit_create_button.clicked.connect(self.create_task)

        # auto set project in submit dialog
        selected_project = self.get_selected_project()
        existing_index = self.submitApp.ui.submit_project_box.findText(selected_project, QtCore.Qt.MatchFixedString)
        index = existing_index if existing_index > 0 else 0
        self.submitApp.ui.submit_project_box.setCurrentIndex(index)

        self.ui.statusLabel.setText('Status: Create Task UI opened.')
        self.ui.statusLabel.setStyleSheet('color: lightGreen')

    def create_task(self):
        self.submitApp.ui.setEnabled(False)
        self.ui.statusLabel.setText('Status: Creating task...')
        self.ui.statusLabel.setStyleSheet('color: yellow')

        project = self.submitApp.ui.submit_project_box.currentText()
        entity_name = self.submitApp.ui.submit_category_box.currentText()
        task_name = self.submitApp.ui.submit_task_box.currentText()
        entity_type = 'Asset'
        user = self.get_selected_user()['sg_name']
        result = stlg.create_entity(project, entity_name, task_name, user)
        self.reset_global_vars()
        self._need_refetch = True
        self.fetch_tasks()
        self.submitApp.close()

def show(mode='default'):
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        maya_win.deleteUI(uiName)
        parent = maya_win.getMayaWindow()
        # launch app
        window = TimeLogged(parent)
        window.show()
    else:
        logger.info('Run in standalone\n')
        kill_other_instance()
        
        # pre fetch tasks
        threadpool = QtCore.QThreadPool()
        threadpool.setMaxThreadCount(1)
        user = user_info.User().sg_user()['sg_name']
        worker1 = thread_pool.Worker(prefetch, user)
        threadpool.start(worker1)

        app = QtWidgets.QApplication.instance()
        if not app:
            app = QtWidgets.QApplication(sys.argv)
        
        # init splash screen
        pixmap = QtGui.QPixmap('{}/icons/splash_image.png'.format(moduleDir).replace('\\', '/'))
        splash = QtWidgets.QSplashScreen(pixmap)
        splash.setWindowFlags(QtCore.Qt.FramelessWindowHint|QtCore.Qt.WindowStaysOnTopHint)
        splash.show()
        splash.showMessage('{} {} is loading, please wait..'.format(_title, _version), 
                        QtCore.Qt.AlignBottom, 
                        QtCore.Qt.white)
       
        app.processEvents()
        
        # launch app
        window = TimeLogged(thread=threadpool)
        window.show()
        # kill splash screen
        splash.finish(window)

        # stylesheets
        window.ui.treeWidget.setStyleSheet('background-image:url("{}/icons/riff_bg.png");'.format(moduleDir))
        stylesheet.set_default(app)
        sys.exit(app.exec_())

def prefetch(user):
    global PRE_FETCH_USER
    PRE_FETCH_USER = user

    global PRE_FETCH_TASKS
    PRE_FETCH_TASKS = stlg.list_user_tasks(None, user)
    print('Fetch tasks finished')

    global ACTIVE_PROJECTS
    ACTIVE_PROJECTS = [p.get('name') for p in stlg.find_active_projects()]
    print('Fetch active project finished')

def kill_other_instance():
    import psutil
    cmd_to_kill = ('/rf_app/time_logged/app/app.py',  # app itself
                '/rf_app/time_logged/reminder.py',  # app launches from reminder
                '/rf_launcher_apps/Tool/Time Logged/TimeLogged.py')  # app launch from RFlauncher
    my_pid = os.getpid()
    # iterate through all running processes
    for p in psutil.process_iter():
        # if it's process we're looking for...
        if p.name() in ('python.exe', 'pythonw.exe') and len(p.cmdline()) >= 2:
            app_name = p.cmdline()[-1].replace('\\', '/')
            # if app is timelogged, 
            # and if the process has a different PID than the current process, kill it
            if any([app_name.endswith(k) for k in cmd_to_kill]) and p.pid != my_pid: 
                p.terminate()

if __name__ == '__main__':
    show()

    