#!/usr/bin/env python
# -- coding: utf-8 --

# v.0.0.1 polytag switcher
_title = 'Submit Time Logged'
_version = 'v.0.0.1'
_des = ''
uiName = 'Submit Time Logged'

#Import python modules
import sys
import os
import logging
import getpass

moduleDir = os.path.dirname(sys.modules[__name__].__file__).replace('\\', '/')
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

core = '{}/core'.format(os.environ.get('RFSCRIPT'))
sys.path.append(core)

# import config
import rf_config as config

from rf_utils import log_utils
from rf_utils.ui import load
# reload(load)
from rf_utils.ui import stylesheet

user = '{}-{}'.format(config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)

from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

# maya & module function 
from . import submit_ui
from rf_app.time_logged import time_logged_utils as tlg
from rf_utils import file_utils


class SubmitTimeLogged(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        #Setup Window
        super(SubmitTimeLogged, self).__init__(parent)

        # ui read
        uiFile = '{}/submit_ui.py'.format(moduleDir)
        configFile = '{}/config.yml'.format(moduleDir)
        self.ui = submit_ui.Ui_Submit()
        self.data = file_utils.ymlLoader(configFile)

        # set size 
        self.w = 500
        self.h = 100 

        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)
        self.resize(self.w, self.h)
        self.setWindowTitle('{} {} {}'.format(_title, _version, _des))
        self.setWindowModality(QtCore.Qt.ApplicationModal)
        self.setWindowIcon(QtGui.QIcon('{}/icons/main_icon_black.png'.format(moduleDir)))

        self.ui.icon_label.setPixmap(self.style().standardPixmap(QtWidgets.QStyle.SP_MessageBoxQuestion))

        
        self.list_project()
        self.list_category()
        self.list_tasks()
        self.init_signals()

    def init_signals(self):
        self.ui.submit_project_box.currentIndexChanged.connect(self.list_category)
        self.ui.submit_category_box.currentIndexChanged.connect(self.list_tasks)

    def list_project(self):
        projects = tlg.list_projects()
        self.ui.submit_project_box.blockSignals(True)
        self.ui.submit_project_box.clear()
        for project in projects:
            projName = project['name']
            self.ui.submit_project_box.addItem(projName)
        self.ui.submit_project_box.blockSignals(False)
        self.list_category()

    def get_category_data(self):
        main_category = self.data.keys()
        curr_proj = self.ui.submit_project_box.currentText()
        data = self.data['default']
        if curr_proj in main_category:
            data = self.data[curr_proj]
        return data

    def list_category(self):
        self.ui.submit_category_box.blockSignals(True)
        self.ui.submit_category_box.clear()

        data = self.get_category_data()
        category = data.keys()
        for cat in category:
            self.ui.submit_category_box.addItem(cat)
        self.ui.submit_category_box.blockSignals(False)
        self.list_tasks()

    def list_tasks(self):
        self.ui.submit_task_box.clear()

        category_selected = self.ui.submit_category_box.currentText()
        data = self.get_category_data()
        tasks = data[category_selected]
        for task in tasks:
            self.ui.submit_task_box.addItem(task)

def show(mode='default'):
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        if mode == 'default':
            maya_win.deleteUI(uiName)
            myApp = SubmitTimeLogged(maya_win.getMayaWindow())
        myApp.show()
    else:
        logger.info('Run in standalone\n')
        app = QtWidgets.QApplication.instance()
        if not app:
            app = QtWidgets.QApplication(sys.argv)
        if mode == 'default':
            myApp = SubmitTimeLogged()
        myApp.show()
        # stylesheet.set_default(app)
    return myApp


if __name__ == '__main__':
    show()