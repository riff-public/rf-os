# -*- coding: utf-8 -*-

from Qt import QtCore, QtGui, QtWidgets
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

from rf_utils.widget import textEdit

class Ui_Time_Add(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super(Ui_Time_Add, self).__init__(parent)
        self.setObjectName("MainWindow")
        # self.resize(300, 150)
        self.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))

        # main layout
        self.centralwidget = QtWidgets.QWidget(self)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName("verticalLayout")

        # text layout
        self.textLayout = QtWidgets.QVBoxLayout()
        self.textLayout.setObjectName("textLayout")
        self.verticalLayout.addLayout(self.textLayout)

        self.textLayout21 = QtWidgets.QHBoxLayout()
        self.date_label = QtWidgets.QLabel()
        spacerItemProject111 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        spacerItemProject121 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.textLayout21.addItem(spacerItemProject111)
        self.textLayout21.addWidget(self.date_label)
        self.textLayout21.addItem(spacerItemProject121)

        self.textLayout2 = QtWidgets.QHBoxLayout()
        self.text_label = QtWidgets.QLabel()
        spacerItemProject11 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        spacerItemProject12 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.textLayout2.addItem(spacerItemProject11)
        self.textLayout2.addWidget(self.text_label)
        self.textLayout2.addItem(spacerItemProject12)

        self.textLayout.addLayout(self.textLayout21)
        self.textLayout.addLayout(self.textLayout2)

        self.line = QtWidgets.QFrame(self.centralwidget)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.verticalLayout.addWidget(self.line)

        # upper layout
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setSpacing(8)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.duration_label = QtWidgets.QLabel('Duration: ')
        self.duration_label.setObjectName("duration_label")
        self.horizontalLayout.addWidget(self.duration_label)
        self.verticalLayout.addLayout(self.horizontalLayout)

        # hour spinbox
        self.hr_spinBox = QtWidgets.QSpinBox(self.centralwidget)
        # self.hr_spinBox.setButtonSymbols(QtWidgets.QAbstractSpinBox.PlusMinus)
        self.hr_spinBox.setMinimum(0)
        self.hr_spinBox.setMaximum(8)
        self.hr_spinBox.setObjectName("hr_spinBox")
        self.hr_spinBox.setMinimumSize(QtCore.QSize(60, 22))
        self.hr_spinBox.setAlignment(QtCore.Qt.AlignRight) 
        self.horizontalLayout.addWidget(self.hr_spinBox)

        # --- hour labels
        self.hr_label = QtWidgets.QLabel('hr')
        self.hr_label.setObjectName("hr_label")
        self.horizontalLayout.addWidget(self.hr_label)

        # ---minute spinbox
        self.min_spinBox = QtWidgets.QSpinBox(self.centralwidget)
        # self.min_spinBox.setButtonSymbols(QtWidgets.QAbstractSpinBox.PlusMinus)
        self.min_spinBox.setMinimum(0)
        self.min_spinBox.setMaximum(59)
        self.min_spinBox.setSingleStep(15)
        self.min_spinBox.setObjectName("min_spinBox")
        self.min_spinBox.setMinimumSize(QtCore.QSize(60, 22))
        self.min_spinBox.setAlignment(QtCore.Qt.AlignRight) 
        self.horizontalLayout.addWidget(self.min_spinBox)

        # --- min labels
        self.min_label = QtWidgets.QLabel('min')
        self.min_label.setObjectName("min_label")
        self.horizontalLayout.addWidget(self.min_label)
        spacerItemProjectTime = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItemProjectTime)

        # --- Description
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.des_label = QtWidgets.QLabel('Description: ')
        self.des_label.setObjectName("des_label")
        spacerItemProject = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.des_box = textEdit.TextEditWithPlaceHolder(self.centralwidget)
        self.des_box.setObjectName("des_box")
        self.horizontalLayout_2.addWidget(self.des_label)
        self.horizontalLayout_2.addItem(spacerItemProject)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.verticalLayout.addWidget(self.des_box)

        self.add_time_button = QtWidgets.QPushButton('Submit Time')
        self.add_time_button.setObjectName("add_time_button")
        self.add_time_button.setMinimumSize(QtCore.QSize(240, 40))
        self.verticalLayout.addWidget(self.add_time_button)

        # set central widget
        self.setCentralWidget(self.centralwidget)
