# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'E:\script\core\rf_app\time_logged\app\submit.ui'
#
# Created: Fri Jan 22 12:04:53 2021
#      by: pyside2-uic  running on PySide2 2.0.0~alpha0
#
# WARNING! All changes made in this file will be lost!

from Qt import QtCore, QtGui, QtWidgets
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

class Ui_Submit(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super(Ui_Submit, self).__init__(parent)
        self.setObjectName("MainWindow")
        self.centralwidget = QtWidgets.QWidget(self)
        self.centralwidget.setObjectName("centralwidget")

        self.main_layout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.main_layout.setObjectName("main_layout")
        self.main_layout.setContentsMargins(15, 15, 15, 20)

        self.horizontalLayout22 = QtWidgets.QHBoxLayout()
        self.icon_label = QtWidgets.QLabel()
        self.help_label = QtWidgets.QLabel(' In which area have you worked on?')
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout22.addWidget(self.icon_label)
        self.horizontalLayout22.addWidget(self.help_label)
        self.horizontalLayout22.addItem(spacerItem)
        self.main_layout.addLayout(self.horizontalLayout22)

        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setContentsMargins(0, 5, 0, 0)
        self.horizontalLayout.setSpacing(10)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.main_layout.addLayout(self.horizontalLayout)

        self.submit_form_layout = QtWidgets.QFormLayout()
        self.horizontalLayout.addLayout(self.submit_form_layout)

        # project
        self.submit_project_box = QtWidgets.QComboBox(self.centralwidget)
        self.submit_project_box.setObjectName("submit_project_box")
        self.submit_project_box.setFixedSize(QtCore.QSize(170, 20))
        self.submit_form_layout.addRow('Project:', self.submit_project_box)

        # category
        self.submit_category_box = QtWidgets.QComboBox(self.centralwidget)
        self.submit_category_box.setObjectName("submit_category_box")
        self.submit_category_box.setFixedSize(QtCore.QSize(170, 20))
        self.submit_form_layout.addRow('Category:', self.submit_category_box)

        # task
        self.submit_task_box = QtWidgets.QComboBox(self.centralwidget)
        self.submit_task_box.setObjectName("submit_task_box")
        self.submit_task_box.setFixedSize(QtCore.QSize(170, 20))
        self.submit_form_layout.addRow('Task:', self.submit_task_box)

        # create button
        self.button_layout = QtWidgets.QVBoxLayout()
        self.horizontalLayout.addLayout(self.button_layout)

        # spacerItem1 = QtWidgets.QSpacerItem(25, 25, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        # self.button_layout.addItem(spacerItem1)

        self.submit_create_button = QtWidgets.QPushButton(self.centralwidget)
        self.submit_create_button.setText('Create')
        self.submit_create_button.setObjectName("submit_create_button")
        self.submit_create_button.setFixedSize(QtCore.QSize(100, 30))
        self.button_layout.addWidget(self.submit_create_button)

        spacerItem2 = QtWidgets.QSpacerItem(25, 25, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.button_layout.addItem(spacerItem2)

        self.setCentralWidget(self.centralwidget)

        # self.retranslateUi(self)
        QtCore.QMetaObject.connectSlotsByName(self)

    # def retranslateUi(self, MainWindow):
        # MainWindow.setWindowTitle(QtWidgets.QApplication.translate("MainWindow", "MainWindow", None, -1))
        # self.submit_project_label.setText("Project: ")
        # self.submit_category_label.setText("Category: ")
        # self.submit_task_label.setText("Task: ")
        # self.submit_create_button.setText("Create")

