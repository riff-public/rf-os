#!/usr/bin/env python
# -- coding: utf-8 --

_title = 'Settings'
_version = 'v.0.0.1'
_des = ''
uiName = 'TimeLoggedSettings'

#Import python modules
import sys
import os
import logging
import getpass
import traceback
from collections import OrderedDict

moduleDir = os.path.dirname(sys.modules[__name__].__file__).replace('\\', '/')
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

core = '{}/core'.format(os.environ.get('RFSCRIPT'))
sys.path.append(core)

# import config
import rf_config as config

from rf_utils import log_utils
from rf_utils.ui import load
from rf_utils.ui import stylesheet

user = '{}-{}'.format(config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)

from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

class TimeSettings(QtWidgets.QMainWindow):
    submit = QtCore.Signal(dict)

    def __init__(self, parent=None):
        #Setup Window
        super(TimeSettings, self).__init__(parent)
        self.w = 250
        self.h = 200 
        self.initial_values = None

        self.init_ui()
        self.init_signals()

    def init_ui(self):
        # setup UI
        self.setObjectName(uiName)
        self.setWindowTitle(_title)
        self.setWindowIcon(QtGui.QIcon('{}/icons/riff_logo.png'.format(moduleDir)))
        self.setFixedSize(QtCore.QSize(self.w, self.h))
        self.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))
        self.setWindowModality(QtCore.Qt.ApplicationModal)
        
        self.centralwidget = QtWidgets.QWidget(self)
        self.setCentralWidget(self.centralwidget)

        # main layout
        self.main_layout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.main_layout.setContentsMargins(10, 10, 10, 10)

        self.frame = QtWidgets.QFrame(self)
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Plain)
        self.main_layout.addWidget(self.frame)

        self.form_layout = QtWidgets.QFormLayout(self.frame)

        # artist editable days
        self.time_spinbox = QtWidgets.QSpinBox()
        self.time_spinbox.setToolTip('Total hour credit')
        self.form_layout.addRow('Maximum work hours: ', self.time_spinbox)

        # buttons
        self.button_layout = QtWidgets.QHBoxLayout()
        self.main_layout.addLayout(self.button_layout)

        spacerItem1 = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.button_layout.addItem(spacerItem1)

        self.accept_button = QtWidgets.QPushButton(' Accept ')
        self.accept_button.setMinimumSize(QtCore.QSize(80, 22))
        self.button_layout.addWidget(self.accept_button)

        self.cancel_button = QtWidgets.QPushButton(' Cancel ')
        self.cancel_button.setMinimumSize(QtCore.QSize(60, 22))
        self.button_layout.addWidget(self.cancel_button)

    def init_signals(self):
        self.cancel_button.clicked.connect(self.hide)
        self.accept_button.clicked.connect(self.accept)

    def set_initial_values(self, setting_values):
        maximum_work_time = int(setting_values.get('maximum_work_time'))
        self.time_spinbox.setValue(maximum_work_time)

        self.initial_values = setting_values

    def get_setting_values(self):
        maximum_work_time = int(self.time_spinbox.value())
        results = {'maximum_work_time': maximum_work_time}
        return results

    def accept(self):
        setting_values = self.get_setting_values()
        if setting_values != self.initial_values:
            qmsgBox = QtWidgets.QMessageBox(self)
            qmsgBox.setText('Make change to local settings?')
            qmsgBox.setWindowTitle('Confirm')
            qmsgBox.addButton('   Yes   ', QtWidgets.QMessageBox.AcceptRole)
            qmsgBox.addButton('   No   ', QtWidgets.QMessageBox.RejectRole)
            qmsgBox.setIcon(QtWidgets.QMessageBox.Question)
            results = qmsgBox.exec_()
            if results == 0:
                self.submit.emit(setting_values)
        else:
            self.close()


def show(mode='default'):
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        if mode == 'default':
            maya_win.deleteUI(uiName)
            myApp = TimeSettings(maya_win.getMayaWindow())
        myApp.show()
    else:
        logger.info('Run in standalone\n')
        app = QtWidgets.QApplication.instance()
        if not app:
            app = QtWidgets.QApplication(sys.argv)
        if mode == 'default':
            myApp = TimeSettings()
        myApp.show()
        # stylesheet.set_default(app)
    return myApp


if __name__ == '__main__':
    show()