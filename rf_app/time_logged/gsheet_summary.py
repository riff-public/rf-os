import os 
import sys 
from datetime import datetime

core = '{}/core'.format(os.environ['RFSCRIPT'])
sys.path.append(core) if not core in sys.path else None
import rf_config 
from rf_utils.gsheet import googlesheet 
from rf_app.time_logged import time_logged_utils as tlg
sg = tlg.sg
TIMELOGGED_ENTITY = tlg.TIMELOGGED_ENTITY


def export(project, start='2021-W1', end='', logged_type='User'): 
    data = fetch_data(project, start, end, logged_type)
    gsheet(project, data)


def fetch_data(project, start, end, logged_type): 
    project_entity = sg.find_one('Project', [['name', 'is', project]])
    filters = [['sg_project', 'is', project_entity], ['sg_logged_type', 'is', logged_type]]
    start_date = datetime.strptime(start + '-1', "%Y-W%W-%w")
    if not end: 
        filters.append(['sg_record_time', 'greater_than', start_date])
    if end: 
        end_date = datetime.strptime(end + '-1', "%Y-W%W-%w")
        filters.append(['sg_record_time', 'between', (start_date, end_date)])

    fields = tlg.ReturnField.logged_item + [
        'sg_time_track_parent.CustomNonProjectEntity01.sg_entity', 
        'sg_time_track_parent_automatic.CustomNonProjectEntity01.sg_entity', 
        'sg_record_time']

    items = sg.find(TIMELOGGED_ENTITY, filters, fields)
    data = list()

    for log in items: 
        user = log['sg_user']['name']
        minute = log['sg_duration_minute']
        hours = minute / 60
        if logged_type == 'User': 
            parent_attr = 'sg_time_track_parent.CustomNonProjectEntity01.sg_entity'
        else: 
            parent_attr = 'sg_time_track_parent_automatic.CustomNonProjectEntity01.sg_entity'
        entity_type = log[parent_attr]['type']
        record_time = log['sg_record_time'].strftime('%Y-%M-%d')
        data.append([user, entity_type, hours, record_time])

    return data


def gsheet(project, data): 
    """ test function """
    from rf_utils.gsheet import googlesheet as gs
    from rf_utils import time_utils
    ws = project

    format_col = ['user', 'entity_type', 'hours', 'date']

    scope = gs.Scope.sheet_full
    client = gs.createAccount(gs.Setting.keyfile, scope)
    service = gs.buildService(gs.Setting.keyfile, scope)
    sheet = gs.openByURL(client, 'https://docs.google.com/spreadsheets/d/1Ld_I-NYoiPx-_UgJ4n42r2VjQMTEGnvRyd3UECkkeAs/edit#gid=0')

    # list all tabs
    # ws_list = gs.list_worksheet(sheet)
    # ws = gs.select_worksheetByTitle(sheet, project)

    service.spreadsheets().values().append(
            spreadsheetId = sheet.id,
            range = "{}!A:Z".format(ws),
            body = {
                    "majorDimension": "ROWS",
                    "values": data
                    },
            valueInputOption="USER_ENTERED").execute()


def gsheet2(): 
    """ test function """
    from rf_utils.gsheet import googlesheet as gs
    scope = gs.Scope.sheet_full
    client = gs.createAccount(gs.Setting.keyfile, scope)
    sheet = gs.openByURL(client, 'https://docs.google.com/spreadsheets/d/1Ld_I-NYoiPx-_UgJ4n42r2VjQMTEGnvRyd3UECkkeAs/edit#gid=0')
    spreadsheetId = sheet.id
    sheetName = "Summary"

    ss = client.open_by_key(spreadsheetId)
    sheetId = ss.worksheet(sheetName)._properties['sheetId']
    # list all tabs
    # ws_list = gs.list_worksheet(sheet)

    body = {
        "requests": [
            {
                "updateDimensionProperties": {
                    "range": {
                        "sheetId": sheet.id,
                        "dimension": "COLUMNS",
                        "startIndex": 0,
                        "endIndex": 1
                    },
                    "properties": {
                        "pixelSize": 100
                    },
                    "fields": "pixelSize"
                }
            }
        ]
        }
    res = sheet.batch_update(body)


if __name__ == '__main__': 
    project = 'Hanuman'
    gsheet(project, [['a', 'b', 'c', 'd'], ['1', '2', '3', '4']])
    # export(project, start='2021-W1', end='', logged_type='Automatic')
