#!/usr/bin/env python
# -*- coding: utf-8 -*-

# beta - under development
# v.1.0.0 - initial release
# v.1.0.1 - Fix bug unable to double click when go back in years
# v.1.0.2 - Fix bug with month page changes when scroll wheel is rolled
# v.1.0.3 - Fix bug in get_user()

_title = 'Leave Manager'
_version = 'v.1.0.3'
_des = ''
uiName = 'Leave Manager'

#Import python modules
import sys
import os
import logging
import getpass
import tempfile
from random import randint
from functools import partial
from datetime import datetime, timedelta
from collections import OrderedDict, defaultdict
import subprocess

core = '%s/core' % os.environ.get('RFSCRIPT')
sys.path.append(core)

# import config
import rf_config as config
from rf_utils import log_utils
from rf_utils.ui import stylesheet

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)

# QT
os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

from rf_utils.sg import sg_schedule
from rf_utils import thread_pool
from rf_utils import user_info
from rf_utils.widget import user_widget
from rf_utils.widget import display_widget
from rf_utils.sg import sg_thread
from rf_app.time_logged import utils as utils

moduleDir = os.path.dirname(sys.modules[__name__].__file__).replace('\\', '/')
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

def get_working_days(start_date, end_date):
    days = end_date - start_date
    working_date_list = {(start_date + timedelta(days=x)) for x in range(days.days+1) if (start_date + timedelta(days=x)).isoweekday() <= 5}
    working_date_list = sorted(list(working_date_list))
    return working_date_list

class YearCalendarWidget(QtWidgets.QWidget):
    def __init__(self, year, parent=None):
        super(YearCalendarWidget, self).__init__(parent)
        # internal vars
        self.year = year
        self.calendars = []

        # UI vars
        self.month_name_color = [255, 255, 255]
        self.month_number_color = [255, 20, 20]
        self.header_color = [10, 10, 80]
        self.month_font_size = 12
        self.setupUi()

    def set_year(self, year):
        self.year = year
        self.calendar_layout.deleteLater()
        self.calendar_layout.setParent(None)
        self.setup_calendars()
        if self.year != datetime.today().year:
            self.set_current_date(QtCore.QDate(self.year, 1, 1))

    def set_current_date(self, date):
        for cal in self.calendars:
            cal.setSelectedDate(date)

    def reset(self):
        self.reset_holidays()
        self.reset_cross()

    def reset_holidays(self):
        for cal in self.calendars:
            cal.reset_holidays()

    def set_holiday(self, date, description):
        widget = self.calendars[date.month()-1]
        widget.set_holiday(date, description)

    def reset_cross(self):
        for cal in self.calendars:
            cal.reset_cross()

    def set_cross(self, date, color):
        widget = self.calendars[int(date.month())-1]
        widget.set_cross(date, color)

    def setupUi(self):
        self.layout = QtWidgets.QVBoxLayout()
        self.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.layout)
        self.setup_calendars()

    def setup_calendars(self):
        self.calendars = []
        # calendars
        self.calendar_layout = QtWidgets.QGridLayout()
        self.calendar_layout.setContentsMargins(0, 0, 0, 0)
        self.layout.addLayout(self.calendar_layout)
        today = datetime.today()
        monthName_qss = '''QLabel {{background-color: rgb({}, {}, {}); 
                        font-size: {}pt;
                        color: rgb({}, {}, {})}}'''.format(self.header_color[0], self.header_color[1], self.header_color[2],
                                                        self.month_font_size,
                                                        self.month_name_color[0], self.month_name_color[1], self.month_name_color[2])
        monthNumber_qss = '''QLabel {{background-color: rgb({}, {}, {}); 
                        font-size: {}pt;
                        color: rgb({}, {}, {})}}'''.format(self.header_color[0], self.header_color[1], self.header_color[2],
                                                        self.month_font_size,
                                                        self.month_number_color[0], self.month_number_color[1], self.month_number_color[2])
        ri, ci = 0, 0
        for m in xrange(1, 13):
            lay = QtWidgets.QVBoxLayout()
            lay.setSpacing(0)

            label_lay = QtWidgets.QHBoxLayout()
            label_lay.setSpacing(0)
            lay.addLayout(label_lay)

            # local month name
            month_name_local = QtCore.QDate.longMonthName(m)
            label_local = QtWidgets.QLabel('  '+month_name_local+'  ')
            label_local.setStyleSheet(monthName_qss)
            label_local.setMinimumHeight(28)
            label_local.setAlignment(QtCore.Qt.AlignCenter)
            label_lay.addWidget(label_local)

            # local number
            month_number = str(m)
            label_number = QtWidgets.QLabel('  '+month_number+'  ')
            label_number.setStyleSheet(monthNumber_qss)
            label_number.setMinimumHeight(28)
            label_number.setAlignment(QtCore.Qt.AlignCenter)
            label_lay.addWidget(label_number)

            # local month global
            month_name = datetime(self.year, m, 1).strftime('%B')
            label_month_name = QtWidgets.QLabel('  '+month_name+'  ')
            label_month_name.setStyleSheet(monthName_qss)
            label_month_name.setMinimumHeight(28)
            label_month_name.setAlignment(QtCore.Qt.AlignCenter)
            label_lay.addWidget(label_month_name)

            label_lay.setStretch(0, 1)
            label_lay.setStretch(1, 5)
            label_lay.setStretch(2, 1)

            cal = CalendarWidget(month=m, year=self.year)
            cal.active_date.connect(self.cell_clicked)
            lay.addWidget(cal)

            self.calendar_layout.addLayout(lay, ri, ci, 1, 1)
            self.calendars.append(cal)
            ci += 1
            if ci == 3:
                ci = 0
                ri += 1

            if m == today.month:
                self.active_widget = cal

    def cell_clicked(self, active_list):
        widget = active_list[0]
        date = active_list[1]
        # print date
        self.set_current_date(date)
        self.active_widget = widget

class CalendarWidget(QtWidgets.QCalendarWidget):
    active_date = QtCore.Signal(tuple)
    def __init__(self, month, year, parent=None):
        super(CalendarWidget, self).__init__(parent)
        
        # internal vars
        self.month = month
        self.year = year
        self.holidays = set()
        self.cross_days = {}

        # UI vars
        self.border_thickness = 2
        self.mark_thickness = 1.5
        self.background_color = [225, 228, 220]
        self.font_size = 11
        self.today_color = QtGui.QColor(QtCore.Qt.yellow)
        self.holiday_color = QtGui.QColor(255, 20, 20)
        self.workday_color = QtGui.QColor(20, 20, 200)
        self.out_of_range_color = QtGui.QColor(170, 170, 190)
        self.blank_color = QtGui.QColor(0, 0, 0)
        self.header_holiday_color = QtGui.QColor(200, 0, 0)
        self.header_weekday_color = QtGui.QColor(100, 100, 190)
        self.table = None  # calendar internal table widget

        self.setupUi()
        self.init_connection()

    def init_connection(self):
        # signals
        self.clicked.connect(self.on_clicked)
        self.currentPageChanged.connect(self.page_changed)

    def page_changed(self):
        ''' this is to prevent wheel scrolling '''
        self.setCurrentPage(self.year, self.month)

    def setupUi(self):
        calendar_css = '''
        QCalendarWidget QAbstractItemView {{
                        selection-background-color: transparent; 
                        background-color: rgb({}, {}, {});
                        font-size: {font_size}pt;
        }}

        QCalendarWidget QMenu {{ font-size: {font_size}pt; }}
        QCalendarWidget QToolButton {{ font-size: {font_size}pt; }}
        QCalendarWidget QSpinBox {{ font-size: {font_size}pt; }}
        QCalendarWidget QTableWidget {{outline: 0;}}
        '''.format(self.background_color[0], self.background_color[1], self.background_color[2], font_size=self.font_size)

        self.setStyleSheet(calendar_css)
        self.setFocusPolicy(QtCore.Qt.NoFocus)  # so there's no ugly dotted frame around selection
        # self.setFirstDayOfWeek(QtCore.Qt.DayOfWeek.Monday)
        self.setNavigationBarVisible(False)
        self.setGridVisible(True)
        self.setVerticalHeaderFormat(QtWidgets.QCalendarWidget.NoVerticalHeader)

        # set current page to self.year, self.month
        first_day = QtCore.QDate(self.year, 1, 1)
        last_day = QtCore.QDate(self.year, 12, 31)
        self.setDateRange(first_day, last_day)
        self.setCurrentPage(self.year, self.month)

        # color
        header_weekday_brush = QtGui.QBrush(self.header_weekday_color)
        header_weekday_format = QtGui.QTextCharFormat()
        header_weekday_format.setBackground(header_weekday_brush)
        self.setWeekdayTextFormat(QtCore.Qt.DayOfWeek.Monday, header_weekday_format)
        self.setWeekdayTextFormat(QtCore.Qt.DayOfWeek.Tuesday, header_weekday_format)
        self.setWeekdayTextFormat(QtCore.Qt.DayOfWeek.Wednesday, header_weekday_format)
        self.setWeekdayTextFormat(QtCore.Qt.DayOfWeek.Thursday, header_weekday_format)
        self.setWeekdayTextFormat(QtCore.Qt.DayOfWeek.Friday, header_weekday_format)
        self.setWeekdayTextFormat(QtCore.Qt.DayOfWeek.Saturday, header_weekday_format)

        header_holiday_brush = QtGui.QBrush(self.header_holiday_color)
        header_holiday_format = QtGui.QTextCharFormat()
        header_holiday_format.setBackground(header_holiday_brush)
        self.setWeekdayTextFormat(QtCore.Qt.DayOfWeek.Sunday, header_holiday_format)

        self.table = self.findChild(QtWidgets.QTableView)

    @QtCore.Slot(QtCore.QDate)
    def on_clicked(self, date):
        self.active_date.emit((self, date))

    def set_background_color(self, r, g, b):
        self.setStyleSheet('QCalendarWidget QAbstractItemView {{background-color: rgb({}, {}, {});}}'.format(r, g, b))

    def setSelectedDate(self, *args):
        date = args[0]
        super(CalendarWidget, self).setSelectedDate(*args)
        if date.month() != self.month:
            self.setCurrentPage(self.year, self.month)

    def set_holiday(self, date, description):
        char_format = QtGui.QTextCharFormat() 
        char_format.setForeground(self.holiday_color)
        # support Thai description
        if description:
            description = description.decode(encoding='UTF-8',errors='strict')
            char_format.setToolTip(description)

        self.holidays.add(date)
        self.setDateTextFormat(date, char_format)
        self.updateCell(date)

    def reset_holidays(self):
        self.holidays = set()
        self.updateCells()

    def reset_cross(self):
        self.cross_days = {}
        self.updateCells()

    def set_cross(self, date, color):
        self.cross_days[date] = color
        self.updateCell(date)
        # print date, color

    def get_date_color(self, date):
        # old_color = self.dateTextFormat(date).foreground().color()
        if date.month() != self.monthShown():
            color = self.out_of_range_color
        elif date.dayOfWeek() in (6, 7) or date in self.holidays:
            color = self.holiday_color 
        else:
            color = self.workday_color
        return color

    def draw_ellipse(self, painter, rect):
        painter.save()
        selection_pen = QtGui.QPen(self.today_color, self.border_thickness)
        painter.setPen(selection_pen)  # no ellipse border
        painter.setBrush(self.today_color)
        radius = QtCore.QRect(QtCore.QPoint(), min(rect.width()-(self.border_thickness*3), rect.height()-(self.border_thickness*3))*QtCore.QSize(1, 1))
        radius.moveCenter(rect.center())
        painter.drawEllipse(radius)

        painter.restore()

    def draw_frame(self, painter, rect):
        painter.save()
        selection_pen = QtGui.QPen(self.today_color, self.border_thickness)
        painter.setPen(selection_pen)
        inner_rect = QtCore.QRect(0, 0, rect.width()-(self.border_thickness*2), rect.height()-(self.border_thickness*2))
        inner_rect.moveCenter(rect.center())
        painter.drawRect(inner_rect)
        painter.restore()

    def draw_cross(self, painter, rect, date, color):
        painter.save()
        selection_pen = QtGui.QPen(color, self.mark_thickness)
        painter.setPen(selection_pen)
        painter.drawLine(rect.bottomLeft(), rect.topRight())
        painter.drawLine(rect.topLeft(), rect.bottomRight())
        painter.restore()

    def paintCell(self, painter, rect, date):
        today = datetime.today().date()
        is_today = date.toPython() == today
        painter.setRenderHint(QtGui.QPainter.Antialiasing)
        is_current_month = date.month() == self.monthShown()
        if is_current_month:
            # if it's today
            if is_today and self.monthShown() == today.month:
                # draw ellipse  
                self.draw_ellipse(painter, rect)

            
        # draw text
        color = self.get_date_color(date)  # get color
        today_pen = QtGui.QPen(color)
        painter.setPen(today_pen)
        painter.drawText(rect, QtCore.Qt.AlignCenter, str(date.day()))

        # drawover
        days_to_mark = self.cross_days
        if date in days_to_mark:
            self.draw_cross(painter, rect, date, self.cross_days[date])

        # if it's the selected date, draw frame
        if is_current_month:
            if date == self.selectedDate():
                # draw frame
                self.draw_frame(painter, rect)

class AddLeaveWindow(QtWidgets.QMainWindow):
    user_confirmed = QtCore.Signal(list)

    def __init__(self, holidays=[], parent=None):
        #Setup Window
        super(AddLeaveWindow, self).__init__(parent)

        self.holidays = holidays
        self.w = 280
        self.h = 200 

        self.setupUi()
        self.init_signal()

    def setupUi(self):
        self.setWindowModality(QtCore.Qt.ApplicationModal)
        self.setWindowTitle('Add Leave')
        self.setWindowIcon(QtGui.QIcon('{}/icons/riff_logo.png'.format(moduleDir)))
        self.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))

        self.centralwidget = QtWidgets.QWidget(self)

        # main layout
        self.main_layout = QtWidgets.QVBoxLayout(self.centralwidget)
        # self.main_layout.setAlignment(QtCore.Qt.AlignCenter)
        self.main_layout.setSpacing(5)

        # labels
        self.user_label = QtWidgets.QLabel()
        self.user_label.setAlignment(QtCore.Qt.AlignCenter)
        self.main_layout.addWidget(self.user_label)

        self.leave_label = QtWidgets.QLabel()
        self.leave_label.setAlignment(QtCore.Qt.AlignCenter)
        self.main_layout.addWidget(self.leave_label)

        self.line = QtWidgets.QFrame(self.centralwidget)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.main_layout.addWidget(self.line)

        # date 
        self.date_layout = QtWidgets.QHBoxLayout()
        self.main_layout.addLayout(self.date_layout)

        # date range layout
        spacerItem1 = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.date_layout.addItem(spacerItem1)

        self.date_form_layout = QtWidgets.QFormLayout()
        self.date_form_layout.setLabelAlignment(QtCore.Qt.AlignRight)
        self.date_form_layout.setFormAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignTop)
        self.date_form_layout.setSpacing(3)
        self.date_layout.addLayout(self.date_form_layout)

        spacerItem2 = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.date_layout.addItem(spacerItem2)

        # start date
        self.start_dateEdit = QtWidgets.QDateEdit()
        self.start_dateEdit.setDisplayFormat('dd/MM/yy')
        self.date_form_layout.addRow('from: ', self.start_dateEdit)

        # end date
        self.end_dateEdit = QtWidgets.QDateEdit()
        self.end_dateEdit.setDisplayFormat('dd/MM/yy')
        self.date_form_layout.addRow('to: ', self.end_dateEdit)

        spacerItem3 = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.main_layout.addItem(spacerItem3)

        # total label
        self.total_label = QtWidgets.QLabel()
        self.total_label.setAlignment(QtCore.Qt.AlignCenter)
        self.total_label.setStyleSheet('color: yellow')
        self.main_layout.addWidget(self.total_label)

        spacerItem4 = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.main_layout.addItem(spacerItem4)

        # buttons
        self.buttonBox = QtWidgets.QDialogButtonBox(QtCore.Qt.Horizontal)
        ok_button = QtWidgets.QPushButton('  OK  ')
        cancel_button = QtWidgets.QPushButton('Cancel')
        self.buttonBox.addButton(ok_button, QtWidgets.QDialogButtonBox.AcceptRole)
        self.buttonBox.addButton(cancel_button, QtWidgets.QDialogButtonBox.RejectRole)
        self.main_layout.addWidget(self.buttonBox)

        self.main_layout.setStretch(0, 1)
        self.main_layout.setStretch(1, 1)
        self.main_layout.setStretch(2, 1)
        self.main_layout.setStretch(3, 1)
        self.main_layout.setStretch(4, 1)
        self.main_layout.setStretch(5, 1)
        self.main_layout.setStretch(6, 5)
        self.main_layout.setStretch(7, 1)

        self.setCentralWidget(self.centralwidget)
        self.resize(self.w, self.h)

    def init_signal(self):
        self.start_dateEdit.dateChanged.connect(self.start_date_changed)
        self.end_dateEdit.dateChanged.connect(self.update_total_leave_days)

        # button signals
        self.buttonBox.rejected.connect(self.close)

    def start_date_changed(self):
        # end date must not be earlier than start date
        self.end_dateEdit.setMinimumDate(self.start_dateEdit.date())
        self.update_total_leave_days()

    def update_total_leave_days(self):
        # update total day count
        start_date = self.start_dateEdit.date().toPython()
        end_date = self.end_dateEdit.date().toPython()
        # print 'ho', self.holidays
        working_days = [d for d in get_working_days(start_date, end_date) if d not in self.holidays]
        # print 'wo',  working_days
        self.total_label.setText('Total {} working day(s)'.format(len(working_days)))

class LeaveManager(QtWidgets.QMainWindow):

    def __init__(self, parent=None):
        # setup Window
        super(LeaveManager, self).__init__(parent)

        # app vars
        self.unknown_photo = '{}/icons/unknown_person.png'.format(moduleDir)
        self.holidays = []  # list of public holidays (datetime)
        self.leave_info = OrderedDict()  # dict leave_type: list( {start, end, leave_type, duration}, ... )
        self.leave_dates = []  # list of leave date (date time)

        # internal vars
        self.user = None
        self._max_year_shown = 5  # allow dating back in years

        # ui vars
        self.w = 1275
        self.h = 800
        self.leave_types = OrderedDict([('sick', QtGui.QColor(QtCore.Qt.magenta)), 
                                    ('personal', QtGui.QColor(255, 130, 0)), 
                                    ('vacation', QtGui.QColor(QtCore.Qt.green)), 
                                    ('special', QtGui.QColor(QtCore.Qt.cyan)),
                                    ('absent', QtGui.QColor(QtCore.Qt.lightGray))])

        # thread
        self.threadpool = QtCore.QThreadPool()
        self.threadpool.setMaxThreadCount(1)
        self.threads = []
        self.downloadThread = None

        # init functions
        self.setupUi()
        self.set_default()
        self.init_signals()

        # first fetch
        self.init_display()

    def setupUi(self):
        this_year = datetime.today().year

        self.setObjectName(uiName)
        self.setWindowTitle('{} {} {}'.format(_title, _version, _des))
        self.setWindowIcon(QtGui.QIcon('{}/icons/riff_logo.png'.format(moduleDir)))
        self.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))

        self.centralwidget = QtWidgets.QWidget(self)

        # main layout
        self.main_layout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.main_layout.setAlignment(QtCore.Qt.AlignCenter)
        self.main_layout.setSpacing(8)

        # ---------------------------
        # view splitter
        self.viewSplitter = QtWidgets.QSplitter()
        self.viewSplitter.setOrientation(QtCore.Qt.Horizontal)
        self.main_layout.addWidget(self.viewSplitter)

        self.viewSplitWidget = QtWidgets.QWidget(self.viewSplitter)
        self.infoSplitWidget = QtWidgets.QWidget(self.viewSplitter)
        # self.viewSplitter.setSizes([800, 200])

        # view layout
        self.view_layout = QtWidgets.QHBoxLayout(self.viewSplitWidget)
        self.view_layout.setContentsMargins(0, 0, 0, 0)
        # calendar 
        self.calendar = YearCalendarWidget(year=this_year)
        # self.calendar = QtWidgets.QCalendarWidget()
        self.view_layout.addWidget(self.calendar)
        
        # info layout
        self.info_layout = QtWidgets.QVBoxLayout(self.infoSplitWidget)
        self.info_layout.setSpacing(15)

        # user layout
        self.user_layout = QtWidgets.QHBoxLayout()
        self.user_layout.setSpacing(3)
        self.info_layout.addLayout(self.user_layout)
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.user_layout.addItem(spacerItem1)

        # user widget
        self.userWidget = user_widget.UserComboBox()
        # self.userWidget = user_widget.UserComboBox(user=user_info.User(nickname='Pia'))
        self.userWidget.comboBox.setFixedSize(QtCore.QSize(100, 20))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
        self.userWidget.setSizePolicy(sizePolicy)
        self.user_layout.addWidget(self.userWidget)

        # year combobox
        self.year_layout = QtWidgets.QFormLayout()
        self.user_layout.addLayout(self.year_layout)
        self.year_comboBox = QtWidgets.QComboBox()
        self.year_comboBox.setMinimumWidth(60)
        year = int(this_year)
        years = [str(year)]
        for i in xrange(self._max_year_shown):
            year -= 1
            years.append(str(year))
        self.year_comboBox.addItems(years)
        self.year_layout.addRow('Year', self.year_comboBox)

        spacerItem2 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.user_layout.addItem(spacerItem2)

        # photo
        self.photo_layout = QtWidgets.QHBoxLayout()
        self.photo_layout.setAlignment(QtCore.Qt.AlignCenter)
        self.photo_layout.setContentsMargins(5, 5, 5, 5)
        self.info_layout.addLayout(self.photo_layout)
        
        self.photo_label = display_widget.PhotoFrame(self)
        self.photo_label.setMinimumSize(QtCore.QSize(200, 200))
        self.photo_layout.addWidget(self.photo_label)

         # user info
        self.user_info_layout = QtWidgets.QHBoxLayout()
        self.info_layout.addLayout(self.user_info_layout)

        spacerItem3 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.user_info_layout.addItem(spacerItem3)

        # user detail
        self.user_detail_layout = QtWidgets.QFormLayout()
        self.user_detail_layout.setLabelAlignment(QtCore.Qt.AlignRight)
        self.user_detail_layout.setFormAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignTop)
        self.user_detail_layout.setSpacing(3)

        self.email_label = QtWidgets.QLabel()
        self.id_label = QtWidgets.QLabel()
        self.department_label = QtWidgets.QLabel()

        self.user_detail_layout.addRow('E-mail: ', self.email_label)
        self.user_detail_layout.addRow('Employee ID: ', self.id_label)
        self.user_detail_layout.addRow('Department: ', self.department_label)

        self.user_info_layout.addLayout(self.user_detail_layout)

        spacerItem4 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.user_info_layout.addItem(spacerItem4)
        self.user_info_layout.setStretch(0, 6)
        self.user_info_layout.setStretch(1, 1)
        self.user_info_layout.setStretch(2, 9)

        # summery 
        self.sum_widget = QtWidgets.QTreeWidget()
        self.sum_widget.setStyleSheet('QTreeWidget::item{padding:1px 0}')
        self.sum_widget.setSortingEnabled(False)
        self.sum_widget.setColumnHidden(1, True)
        self.info_layout.addWidget(self.sum_widget)

        # summery table
        header = self.sum_widget.headerItem()
        header.setTextAlignment(0, QtCore.Qt.AlignLeft)
        header.setTextAlignment(1, QtCore.Qt.AlignLeft)
        header.setText(0, "Date")
        header.setText(1, "Duration")
        self.sum_widget.setColumnWidth(0, 170)
        self.sum_widget.setColumnWidth(1, 70)

        # # leave items
        # self.sick_leave_item = QtWidgets.QTreeWidgetItem(self.sum_widget)
        # self.sick_leave_item.setText(0, 'Sick')
        # self.sick_leave_item.setText(1, '2 days, 0 hours')

        # self.personal_leave_item = QtWidgets.QTreeWidgetItem(self.sum_widget)
        # self.personal_leave_item.setText(0, 'Personal')
        # self.personal_leave_item.setText(1, '0 days')

        # self.vacation_leave_item = QtWidgets.QTreeWidgetItem(self.sum_widget)
        # self.vacation_leave_item.setText(0, 'Vacation')
        # self.vacation_leave_item.setText(1, '3 days')

        # self.absent_leave_item = QtWidgets.QTreeWidgetItem(self.sum_widget)
        # self.absent_leave_item.setText(0, 'Absent')
        # self.absent_leave_item.setText(1, '0 days')

        self.main_layout.setStretch(0, 1)
        self.main_layout.setStretch(1, 15)
        self.setCentralWidget(self.centralwidget)
        self.resize(self.w, self.h)

    def set_default(self):
        self.userWidget.label.setText('ID / User: ')
        self.userWidget.checkBox.setChecked(True)
        self.userWidget.checkBox.setVisible(False)
        self.userWidget.set_searchable(True)
        self.userWidget.searchable_by_id = True

        # # *****
        # # TEST
        # sick1 = QtWidgets.QTreeWidgetItem(self.sick_leave_item)
        # sick1.setText(1, '21/3/15, 08:00 hours')
        # sick2 = QtWidgets.QTreeWidgetItem(self.sick_leave_item)
        # sick2.setText(1, '21/5/3, 08:00 hours')

    def init_signals(self):
        self.userWidget.comboBox.currentIndexChanged.connect(self.fetch_user_info)
        self.userWidget.comboBox.lineEdit().returnPressed.connect(self.fetch_user_info)
        self.userWidget.comboBox.lineEdit().textChanged.connect(self.fetch_user_info)
        self.year_comboBox.currentIndexChanged.connect(self.year_changed)
        self.init_calendar_signals()

    def init_calendar_signals(self):
        # right clicked
        for cal in self.calendar.calendars:
            cal.activated.connect(self.date_activated)

    def date_activated(self, date):
        # only allow double clicking within the day range of that month
        if date.month() == self.calendar.active_widget.month:
            rightClickMenu = QtWidgets.QMenu(self)
            date_py = date.toPython()

            duration = 0
            date_check = {date_py:duration}

            if date_py in self.leave_dates:
                for leave_type in self.leave_info:
                    for leave in self.leave_info[leave_type]:
                        date = leave['start_date'] if 'start_date' in leave.keys() else None
                        duration = leave['duration'] if 'duration' in leave.keys() else None
                        if duration and date:
                            if date == date_py and duration != 480 and date_check[date_py] != 480:
                                date_check[date_py] += duration

                            elif date == date_py and duration == 480:
                                date_check[date_py] = duration

            if date_py not in self.leave_dates:
                # Add
                add_menu = QtWidgets.QMenu('Add leave', self)
                rightClickMenu.addMenu(add_menu)
                duration_map = OrderedDict([(u'½ day', 240), ('Full day', 480)])
                for leave_type in self.leave_types:
                    # leave menu
                    leave_menu = QtWidgets.QMenu(leave_type.title(), self)
                    add_menu.addMenu(leave_menu)
                    for duration, value in duration_map.iteritems():
                        leave_action = QtWidgets.QAction(duration, self)

                        leave_action.triggered.connect(partial(self.add_leave, date_py, date_py, leave_type, value))
                        leave_menu.addAction(leave_action)
                    leave_menu.addSeparator()
                    more_action = QtWidgets.QAction('More...', self)
                    more_action.triggered.connect(partial(self.add_more_leave, date_py, leave_type))
                    leave_menu.addAction(more_action)

            elif date_check[date_py] != 480:
                add_menu = QtWidgets.QMenu('Add leave', self)
                rightClickMenu.addMenu(add_menu)
                duration_map = OrderedDict([(u'½ day', 240)])
                for leave_type in self.leave_types:
                    # leave menu
                    leave_menu = QtWidgets.QMenu(leave_type.title(), self)
                    add_menu.addMenu(leave_menu)
                    for duration, value in duration_map.iteritems():
                        leave_action = QtWidgets.QAction(duration, self)

                        leave_action.triggered.connect(partial(self.add_leave, date_py, date_py, leave_type, value))
                        leave_menu.addAction(leave_action)
                    leave_menu.addSeparator()
                    more_action = QtWidgets.QAction('More...', self)
                    more_action.triggered.connect(partial(self.add_more_leave, date_py, leave_type))
                    leave_menu.addAction(more_action)

                # remove leave
                leave_info = sg_schedule.get_leave_info(self.user['sg_name'], start=(date_py.year, 1, 1), end=(date_py.year, 12, 31))
                remove_menu = QtWidgets.QMenu('Remove', self)
                rightClickMenu.addMenu(remove_menu)
                for entry in leave_info:
                    if sg_schedule.is_inbetween(str(date_py), entry['start_date'], entry['end_date']):
                        duration = '1 day' if entry['sg_duration'] == 480 else '4 hours'
                        remove_leave_action = QtWidgets.QAction('%s %s'%(entry['sg_leave_type'], duration), self)
                        remove_leave_action.triggered.connect(partial(self.remove_leave, date_py, entry))
                        remove_menu.addAction(remove_leave_action)

            else:
                # remove leave
                leave_info = sg_schedule.get_leave_info(self.user['sg_name'], start=(date_py.year, 1, 1), end=(date_py.year, 12, 31))
                remove_menu = QtWidgets.QMenu('Remove', self)
                rightClickMenu.addMenu(remove_menu)
                for entry in leave_info:
                    if sg_schedule.is_inbetween(str(date_py), entry['start_date'], entry['end_date']):
                        duration = '1 day' if entry['sg_duration'] == 480 else '4 hours'
                        remove_leave_action = QtWidgets.QAction('%s %s'%(entry['sg_leave_type'], duration), self)
                        remove_leave_action.triggered.connect(partial(self.remove_leave, date_py, entry))
                        remove_menu.addAction(remove_leave_action)

                #remove_leave_action = QtWidgets.QAction('Remove', self)
                #remove_leave_action.triggered.connect(partial(self.remove_leave, date_py))
                #rightClickMenu.addAction(remove_leave_action)

            if date_py not in self.holidays:
                if date_py.isoweekday() <= 5:
                    # mark as holiday
                    mark_as_holiday_action = QtWidgets.QAction('Mark as holiday', self)
                    mark_as_holiday_action.triggered.connect(partial(self.mark_holiday, date_py))
                    rightClickMenu.addAction(mark_as_holiday_action)
            # else:
            #     # mark as work day
            #     mark_as_workday_action = QtWidgets.QAction('Mark as workday', self)
            #     mark_as_workday_action.triggered.connect(partial(self.mark_workday, date_py))
            #     rightClickMenu.addAction(mark_as_workday_action)

            pos = self.calendar.active_widget.cursor().pos()
            rightClickMenu.exec_(pos)

    def mark_holiday(self, date):
        # set date
        def ord(n):
            return str(n)+("th" if 4<=n%100<=20 else {1:"st",2:"nd",3:"rd"}.get(n%10, "th"))

        def dtStylish(dt, f):
            return dt.strftime(f).replace("{th}", ord(dt.day))

        des_input_dialog = QtWidgets.QInputDialog()
        description, ok = des_input_dialog.getText(self,
                                                'Mark as holiday',
                                                'What day is {}?'.format(dtStylish(date, '{th} %B'))
                                                )
        if ok:
            sg_schedule.add_studio_holiday((date.year, date.month, date.day), description)
            self.refresh_calendar()

    # def mark_workday(self, date):
    #     sg_schedule.remove_studio_holiday((date.year, date.month, date.day))
    #     self.refresh_calendar()

    def add_more_leave(self, date, leave_type):
        self.leave_win = AddLeaveWindow(holidays=self.holidays, parent=self)

        # setup win ui
        self.leave_win.user_label.setText('{} (#{})'.format(self.user['sg_name'], self.user['sg_employee_id']))
        self.leave_win.leave_label.setText('{} Leave'.format(leave_type.title()))
        leave_color = self.leave_types[leave_type]
        self.leave_win.leave_label.setStyleSheet('color: rgb({}, {}, {})'.format(leave_color.red(), leave_color.green(), leave_color.blue()))

        # set date
        qdate = QtCore.QDate(date.year, date.month, date.day)
        self.leave_win.start_dateEdit.setDate(qdate)
        self.leave_win.start_dateEdit.setMinimumDate(QtCore.QDate(self.calendar.year, 1, 1))
        self.leave_win.start_dateEdit.setMaximumDate(QtCore.QDate(self.calendar.year, 12, 31))
        self.leave_win.end_dateEdit.setDate(qdate)
        self.leave_win.end_dateEdit.setMaximumDate(QtCore.QDate(self.calendar.year, 12, 31))

        # signal
        self.leave_win.buttonBox.accepted.connect(lambda: self.add_range_leave(leave_type))
        self.leave_win.show()

    def add_range_leave(self, leave_type):
        start_date = self.leave_win.start_dateEdit.date()
        end_date = self.leave_win.end_dateEdit.date()

        st = start_date.toPython()
        et = end_date.toPython()
        working_days = [d for d in get_working_days(st, et) if d not in self.holidays]
        et = working_days[-1]  # the last day of leave should always be working day

        duration = len(working_days) * 480  # convert leave days to minutes
        # print leave_type, start_date, end_date, duration

        intersect_entry = sg_schedule.check_intersect_date(self.user['sg_name'], (st.year, st.month, st.day), (et.year, et.month, et.day))
        # print intersect_entry
        if intersect_entry:
            display_st = st.strftime('%d/%m/%Y')
            display_et = et.strftime('%d/%m/%Y')
            err_text = 'Please remove existing leave day from {} to {} first.'.format(display_st, display_et)
            self.show_dialog(title='Leave day overlap',
                            text=err_text, 
                            icon=QtWidgets.QMessageBox.Critical)
        else:
            self.add_leave(st, et, leave_type, duration)
        self.leave_win.close()

    def add_leave(self, start_date, end_date, leave_type, duration):
        # print date, leave_type, duration
        start_date = (start_date.year, start_date.month, start_date.day)
        end_date = (end_date.year, end_date.month, end_date.day)
        assign_result = sg_schedule.assign_user_leave(user=self.user['sg_name'], 
                        start=start_date, 
                        end=end_date, 
                        duration=duration, 
                        leave_type=leave_type)
        logger.info('Add leave: start_date={}, end_date={}, leave_type={}, duration={}'.format(start_date, end_date, leave_type, duration))
        logger.debug(assign_result)
        
        self.refresh_user_calendar(user=self.user)

    def show_dialog(self, title, text, icon):
        qmsgBox = QtWidgets.QMessageBox(self)
        # set tile, icon
        qmsgBox.setWindowTitle(title)
        qmsgBox.setIcon(icon)

        # set text
        qmsgBox.setText(text)
        ok_button = qmsgBox.addButton('  OK  ', QtWidgets.QMessageBox.AcceptRole)
        qmsgBox.exec_()

    def remove_leave(self, date, entry=None):
        if entry:
            date_str = str(date)
            if sg_schedule.is_inbetween(date_str, entry['start_date'], entry['end_date']):
                entity_id = entry['id']
                remove_result = sg_schedule.remove_user_leave(entity_id)
                logger.info('Remove leave: date={}'.format(date))
                logger.debug(remove_result)

            self.refresh_user_calendar(user=self.user)

        '''
        leave_info = sg_schedule.get_leave_info(self.user['sg_name'], start=(date.year, 1, 1), end=(date.year, 12, 31))
        if leave_info:
            date_str = str(date)
            for entry in leave_info:
                if sg_schedule.is_inbetween(date_str, entry['start_date'], entry['end_date']):
                    entity_id = entry['id']
                    remove_result = sg_schedule.remove_user_leave(entity_id)
                    logger.info('Remove leave: date={}'.format(date))
                    logger.debug(remove_result)

            self.refresh_user_calendar(user=self.user)
        '''

    def refresh_calendar(self):
        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
        self.calendar.reset()

        # public holiday
        worker1 = thread_pool.Worker(self.fetch_holiday)
        worker1.signals.result.connect(self.fetch_holiday_finished)
        self.threadpool.start(worker1)

    def init_display(self):
        self.refresh_calendar()
        self.fetch_user_info()

    def year_changed(self):
        year = int(self.year_comboBox.currentText())
        self.calendar.set_year(year)
        self.init_display()
        self.init_calendar_signals()

    def fetch_holiday(self):
        year = int(self.year_comboBox.currentText())
        first_day = (year, 1, 1)
        last_day = (year, 12, 31)
        holidays = sg_schedule.get_studio_schedule(first_day, last_day)
        return holidays

    def fetch_holiday_finished(self, holidays):
        # print holidays
        self.holidays = []
        for date, data in holidays.iteritems():
            year, month, day = date.split('-')
            self.holidays.append(sg_schedule.to_datetime(date))
            self.calendar.set_holiday(QtCore.QDate(int(year), int(month), int(day)), data['description'])

        QtWidgets.QApplication.restoreOverrideCursor()

    def fetch_user_info(self):
        self.refresh_calendar()
        user = self.get_selected_user()
        # print 'user', user
        if user != self.user:
            self.reset_userinfo_display()

        if user:
            # update UI
            if 'email' in user:
                email = user['email']
                if email:
                    self.email_label.setText(email)

            if 'sg_employee_id' in user:
                employee_id = user['sg_employee_id']
                if employee_id != None:
                    self.id_label.setText(employee_id)

            if 'department.Department.code' in user:
                department = user['department.Department.code']
                if department:
                    self.department_label.setText(department.title())

            if 'image' in user:
                image_link = user['image']
                 # get image
                if image_link:
                    fh, des = tempfile.mkstemp(suffix='.png')
                    os.close(fh)
                    downloadList = [(self.photo_label, image_link, des)]
                    self.download_image(downloadList=downloadList)

            self.refresh_user_calendar(user=user)
            self.user = user

    def refresh_user_calendar(self, user):
        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
        # user leave
        worker2 = thread_pool.Worker(self.fetch_user_leave, user['sg_name'])
        worker2.signals.result.connect(self.fetch_user_leave_finished)
        self.threadpool.start(worker2)
        return True

    def fetch_user_leave(self, user):
        year = int(self.year_comboBox.currentText())
        first_day = (year, 1, 1)
        last_day = (year, 12, 31)
        leave_informations = sg_schedule.get_leave_info(user, start=first_day, end=last_day)
        return leave_informations

    def fetch_user_leave_finished(self, leave_informations):
        # print leave_informations
        self.calendar.reset_cross()
        delta = timedelta(days=1)
        self.leave_info = OrderedDict()
        for leave_type in self.leave_types:
            self.leave_info[leave_type] = []

        self.leave_dates = []
        for info in leave_informations:
            leave_type = info['sg_leave_type']
            if leave_type in self.leave_types:
                s = info['start_date'].split('-')
                start_date = datetime(int(s[0]), int(s[1]), int(s[2])).date()
                e = info['end_date'].split('-')
                end_date = datetime(int(e[0]), int(e[1]), int(e[2])).date()

                while start_date <= end_date:
                    qdate = QtCore.QDate(start_date.year, start_date.month, start_date.day)
                    # only set cross on working days
                    if start_date not in self.holidays and qdate.dayOfWeek() not in (6, 7):
                        self.calendar.set_cross(qdate, color=self.leave_types[leave_type])

                    self.leave_dates.append(start_date)
                    start_date += delta
                duration = info['sg_duration']
                self.leave_info[leave_type].append({'start_date': datetime(int(s[0]), int(s[1]), int(s[2])).date(), 'end_date': end_date, 'duration': duration})

        # update summay tree widget
        self.update_summary()
        QtWidgets.QApplication.restoreOverrideCursor()

    def update_summary(self):
        self.sum_widget.clear()
        for leave_type, informations in self.leave_info.iteritems():
            # add item
            item_0 = QtWidgets.QTreeWidgetItem(self.sum_widget)
            item_0.setText(0, leave_type.title())
            # item_0.setText(1, '0 Days 0 Hours')
            item_0.setForeground(0, QtGui.QBrush(self.leave_types[leave_type]))

            duration = 0
            for info in informations:
                start_date = info['start_date']
                end_date = info['end_date']
                leave_duration = info['duration']  # duration in minutes
                if leave_duration:
                    duration += leave_duration

                    # add item
                    item_1 = QtWidgets.QTreeWidgetItem(item_0)
                    item_1.setText(0, '{} - {}'.format(start_date.strftime('%d/%m/%y'), end_date.strftime('%d/%m/%y')))

                    day_duration = utils.minutes_to_display_days_hours(minutes=leave_duration)
                    item_1.setText(1, day_duration)

            # total
            total_duration = utils.minutes_to_display_days_hours(minutes=duration)
            item_0.setText(1, total_duration)


        
    def reset_userinfo_display(self):
        self.photo_label.update_photo(self.unknown_photo)
        self.email_label.setText('N/A')
        self.id_label.setText('N/A')
        self.department_label.setText('N/A')

    def download_image(self, downloadList):
        self.downloadThread = sg_thread.DownloadImage(downloadList)
        self.downloadThread.update.connect(self.download_image_finished)
        self.downloadThread.start()
        self.threads.append(self.downloadThread)

    def download_image_finished(self, data):
        item, imagePath = data
        self.photo_label.update_photo(imagePath)
        os.remove(imagePath)

    def get_selected_user(self):
        curr_text = self.userWidget.comboBox.currentText()
        all_texts = [self.userWidget.comboBox.itemText(i) for i in xrange(self.userWidget.comboBox.count())]
        if curr_text not in all_texts:
            return None

        curr_index = self.userWidget.comboBox.currentIndex()
        return self.userWidget.comboBox.itemData(curr_index, QtCore.Qt.UserRole)

def show():
    from rf_utils import user_info
    user = user_info.User()
    app = QtWidgets.QApplication(sys.argv)
    if user.is_hr() or user.is_admin():
        myApp = LeaveManager()
        myApp.show()
        stylesheet.set_default(app)
        sys.exit(app.exec_())
    else:
        qmsgBox = QtWidgets.QMessageBox()
        qmsgBox.setWindowIcon(QtGui.QIcon('{}/icons/riff_logo.png'.format(moduleDir)))
        qmsgBox.setWindowTitle("Permission denied")
        qmsgBox.setIcon(QtWidgets.QMessageBox.Critical)

        qmsgBox.setText('Only H.R. and Admin is autorized to run this application!')
        ok_button = qmsgBox.addButton('  OK  ', QtWidgets.QMessageBox.AcceptRole)
        qmsgBox.exec_()

if __name__ == '__main__':
    show()
