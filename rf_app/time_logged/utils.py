import sys
import os
import time

def minute_to_hour(minutes):
    hours = int(minutes) / 60
    rem_mins = int(minutes) % 60
    return hours, rem_mins

def hours_to_days(hours):
    days = hours / 8
    rem_hours = hours % 8
    return days, rem_hours

def minutes_to_display_hours(minutes):
    hours, rem_mins = minute_to_hour(minutes=minutes)
    return '{}:{}:00'.format(str(int(hours)).zfill(2), str(rem_mins).zfill(2))

def minutes_to_display_days_hours(minutes):
    hours, mins = minute_to_hour(minutes)
    days, rem_hours = hours_to_days(hours)
    result = '{} Days - {} Hours'.format(days, rem_hours)
    return result

def ord(n):
        return str(n)+("th" if 4<=n%100<=20 else {1:"st",2:"nd",3:"rd"}.get(n%10, "th"))

def dtStylish(dt, f):
    return dt.strftime(f).replace("{th}", ord(dt.day))