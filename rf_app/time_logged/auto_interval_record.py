import os
import sys
import argparse
import logging
import subprocess
from datetime import datetime
import getpass
import rf_config as config
from Qt import QtCore
from rf_utils.context import context_info
from rf_utils import user_info
from . import time_logged_utils as logged
from . import sg_time_log_utils as stlg
from . import summary
from rf_utils import thread_pool

logger = logging.getLogger(__name__)
MODULEDIR = os.path.dirname(__file__).replace('\\', '/')
LASTACTIVE = 'LASTACTIVE'
USER_IDLE = 'USER_IDLE'
STRFFORMAT = '%Y-%m-%d %H:%M:%S'
NONE_PROJECT = 'None-Project'
TYPE_MAP = {'asset': 'Asset', 'scene': 'Shot'}


class RecordTime(object):
    """docstring for RecordTime"""
    def __init__(self, duration=0):
        super(RecordTime, self).__init__()
        self.threadpool = QtCore.QThreadPool()
        self.threadpool.setMaxThreadCount(1)
        self.duration = duration

        # for better performance. if no scene, do not call context
        # self.entity = context_info.ContextPathInfo()
        import maya.cmds as mc 
        self.entity = context_info.ContextPathInfo() if mc.file(q=True, sn=True) else None

    def start(self):
        worker = thread_pool.Worker(self.record)
        worker.signals.finished.connect(self.finished)
        self.threadpool.start(worker)

    def record(self):
        record(self.entity, self.duration)

    def finished(self):
        logger.info('TIME RECORDED')


def run_thread(duration=0):
    record = RecordTime(duration)
    record.start()


def run(duration=0):
    record(context_info.ContextPathInfo(), duration)


def record(entity, duration=0):
    if is_active():
        user = user_info.User()

        if not entity: 
            project = NONE_PROJECT
            entity_type = 'Asset'
            entity_name = logged.RND_ENTITY
            task_name = logged.NON_TASK

        else: 
            if entity.valid:
                project = entity.project
                entity_type = TYPE_MAP[entity.entity_type]
                entity_name = entity.name
                task_name = entity.task if entity.task else entity.step

            else:
                project = NONE_PROJECT
                entity_type = 'Asset'
                entity_name = logged.RND_ENTITY
                task_name = logged.NON_TASK

        

        stlg.add_involve_people(project, entity_type, entity_name, task_name, user.name())
        user_track = logged.UserTimeTrack(user=user.name(), project=project, logged_type='Automatic')
        user_task = user_track.find_task(entity_type=entity_type, entity_name=entity_name, task_name=task_name)
        log = user_task.create_logged(duration)
        if log: 
            track = summary.summary_timetrack(log['id'])
            entity = summary.summary_entity_and_task(track)
            summary.update_summary_project(project)

        # logger.debug('TIME RECORDED {}'.format(result))
    else:
        logger.debug('NOT RECORDED')


def is_active():
    status = int(os.environ.get(USER_IDLE, 'None'))
    if status == 'None':
        return True
    return status
