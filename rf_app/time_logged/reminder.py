#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os
from datetime import datetime, timedelta


core = '{}/core'.format(os.environ.get('RFSCRIPT'))
sys.path.append(core)

# import config
from rf_utils.ui import stylesheet

# QT
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui
from rf_utils import user_info
from rf_app.time_logged import sg_time_log_utils as stlg
from rf_utils.sg import sg_schedule

from . import utils

def get_exception_days(date, username):
    first_day = (date.year, 1, 1)
    last_day = (date.year, 12, 31)

    # get holiday
    holidays_data = sg_schedule.get_studio_schedule(first_day, last_day)
    holidays = []
    for date, data in holidays_data.items():
        holidays.append((sg_schedule.to_datetime(date), 480))

    # user leave
    leave_info = sg_schedule.get_leave_info(username, start=first_day, end=last_day)
    leave_dates = []
    leave_types = ('sick', 'personal', 'vacation', 'special', 'absent')
    for info in leave_info:
        leave_type = info['sg_leave_type']
        if leave_type in leave_types:
            s = info['start_date'].split('-')
            start_date = datetime(int(s[0]), int(s[1]), int(s[2])).date()
            e = info['end_date'].split('-')
            end_date = datetime(int(e[0]), int(e[1]), int(e[2])).date()
            leave_dates.append((start_date, info['sg_duration']))
            if start_date != end_date:
                step = timedelta(days=1)
                while start_date < end_date:
                    start_date += step
                    leave_dates.append((start_date, info['sg_duration']))

    exception_days = holidays + leave_dates
    return exception_days

def get_last_working_days(username, num_days):
    today = datetime.today().date()
    if num_days <= 0:
        return today
    exception_days = [d[0] for d in get_exception_days(date=today, username=username)]
    last_working_day = today - timedelta(days=1)
    i = 0
    while True:
        # if it's holiday, keep going bak in time 1 day at a time
        if last_working_day in exception_days or last_working_day.weekday() in (5, 6):
            last_working_day -= timedelta(days=1)
        else:  # it's a working day
            i += 1
            # looking back N number of days, exit
            if i == num_days:
                break
            else:
                last_working_day -= timedelta(days=1)
                
    return last_working_day

def check_last_submit(username):
    result = False

    # --- find last working day
    last_working_day = get_last_working_days(username=username, num_days=1)
    print('Last working day: {}'.format(last_working_day))

    # get submitted tasks
    user_tasks = stlg.list_user_tasks(None, username)
    for task in user_tasks:
        if 'sg_user_logged_items' in task and task['sg_user_logged_items']:
            record_dates = [l['sg_record_time'] for l in task['sg_user_logged_items']]
            latest_date = datetime.strptime(max(record_dates), stlg.DATEFORMAT)
            if latest_date.date() >= last_working_day:
                result = True
                break
    return result, last_working_day

def show_warning(last_working_day):
    app_module_dir = os.path.dirname('{}/app'.format(__file__))

    # show warning
    style_date = utils.dtStylish(last_working_day, '%A, {th} %B %Y')
    day_map = ['จันทร์', 'อังคาร', 'พุธ', 'พฤหัสบดี', 'ศุกร์', 'เสาร์', 'อาทิตย์']
    month_map = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม']
    thai_date = day_map[last_working_day.weekday()] + ' ที่ ' + str(last_working_day.day) + ' ' + month_map[last_working_day.month -1] + ' ' + str(last_working_day.year+543)
    qmsgBox = QtWidgets.QMessageBox()
    qmsgBox.setWindowIcon(QtGui.QIcon('{}/icons/riff_logo.png'.format(app_module_dir)))
    qmsgBox.setWindowTitle("Friendly Reminder")
    qmsgBox.setIcon(QtWidgets.QMessageBox.Warning)
    text = 'กรุณาใส่ชั่วโมงทำงานให้ครบถ้วนในวัน' + thai_date + '\nข้อมูลนี้สำคัญกับสตูดิโอของเรามาก ขอความร่วมมือช่วยกันกรอกทุกวัน\nขอบคุณครับ'
    text += '\n\nPlease submit your work hours on ' + style_date + '\nThese data are very important to our studio, please fill them everyday.\nThank you.\n'
    # text = text.decode(encoding='UTF-8',errors='strict')
    qmsgBox.setText(text)
    ok_button = qmsgBox.addButton('  OK  ', QtWidgets.QMessageBox.AcceptRole)
    qmsgBox.exec_()

def main():
    user = user_info.User()
    result, last_working_day = check_last_submit(username=user.name())
    # if no work submitted on last working day
    if not result:
        from app import app
        qapp = QtWidgets.QApplication(sys.argv)
        show_warning(last_working_day=last_working_day)

        # show time logged
        app.TODAY = last_working_day  # set time to last working day
        timelogged = app.show()

if __name__ == "__main__":
    main()
