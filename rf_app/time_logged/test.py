#!/usr/bin/env python
# -- coding: utf-8 --

import sys 
import os 
from . import interval_record
reload(interval_record)
from rf_app.time_logged import time_logged_utils as tlg
reload(tlg)


def create_time_logged(): 
    user = 'TA'
    project = 'Hanuman'
    user_log = tlg.UserTimeTrack(user, project)
    user_task = user_log.find_task(entity_type='Asset', entity_name='cara', task_name='rig')
    user_task.create_logged(30)


def list_user_tasks(): 
    user = 'TA'
    project = 'Hanuman'
    user_log = tlg.UserTimeTrack(user, project)
    tasks = user_log.list_tasks()

    for task in tasks: 
        print 'user: {}, project: {}, entity: {}, task: {}'.format(task['sg_user']['name'], task['sg_project']['name'], task['sg_entity']['name'], task['sg_task']['name'])
        for a in task['sg_user_logged_items']: 
            print '\tdate: {}, work minute: {}'.format(a['sg_record_time'], a['sg_duration_minute'])




def pia(): 
    # เอา entity, step, task comboBox ออก แล้วเพิ่มช่อง search lineEdit
    # เพิ่มปุ่ม "Fetch Data" ที่จะเป็นตัวเริ่มเรียกข้อมูลทั้งหมด 
    # แล้วใช้คำสั่งหา tasks ทั้งหมด 
    tasks = tlg.find_user_time_tracks('Pia', project=None)
    # หา logged ทั้งหมด 
    logs = tlg.find_user_logged_items('Pia', project=None)
    # เอา logged มาใส่ dict จะได้หาจาก id 
    id_dict = dict()
    for log in logs:
        id_dict[log['id']] = log

    # ใช้บรรทัดนี้ แสดงในช่อง project 
    projects = [a['sg_project']['name'] for a in tasks]
    # ผลลัพท์ ['None-Project', 'Hanuman', 'DarkHorse', 'DarkHorse']
    # ถ้า project แสดง item ไหน ให้แสดงเฉพาะของโปรเจ็คนั้น 
    
    selected_project = 'DarkHorse'
    for task in tasks: 
        # if project matched condition
        if selected_project == task['sg_project']['name']: 
            # item ย่อย 
            logs = task['sg_user_logged_items'] 
            print task['code']

            for log in logs: 
                log_entity = id_dict[log['id']]
                record_date = log_entity['sg_record_time']
                minute = log_entity['sg_duration_minute']
                print '\t', record_date, minute

    # จะมี ได้ผลลัพท์อันนี้ เอามาแสดง
    # Pia-DarkHorse-jaxTwo-rig | + | <----- ปุ่ม + เพิ่มเวลาอยู่ตรงนี้ พอกดแล้วให้รัน add_logged() ด้านล่าง
    #     2021-01-15 16:59:35+07:00 15 
    # Pia-DarkHorse-jaxTwo-model | + |
    #     2021-01-15 17:20:16+07:00 30  

def pia2(): 
    # พี่ขอแก้วิธีหาโปรเจ็คหน่อย พี่ลืมไปว่าบางทีเราอาจจะต้องเลือกโปรเจ็คที่ไม่มีใน task 
    # list projects ให้ใช้ widget เดิม project widget ของ task manager ไปเลย 

    # แล้วพอ user เลือกโปรเจ็คให้รันอันนี้ก่อน 
    # คำสั่งนี้จะสร้าง task time track ขึ้นมาโดยอิงจาก task ใน Shotgun 
    # รอบแรกจะช้า รอบต่อไปจะเร็วขึ้นเพราะสร้างหมดแล้ว
    tlg.create_time_track_from_task(user, project, create=True)
    # ระหว่างรันคำสั่งนี้ให้ set status line -> "building tasks ..."

    # พอ user เลือก project เข้าคำสั่งเดิม 
    # แต่ตอนนี้ จะใส่ project แทน 
    tasks = tlg.find_user_time_tracks('Pia', project=selected_project)
    # status -> "collecting tasks"
    # หา logged ทั้งหมด 
    logs = tlg.find_user_logged_items('Pia', project=selected_project)
    # status -> "collecting logged"
    # ที่เหลือ น่าจะเข้า code เดิมได้ 

    # พี่ขอเปลี่ยน code create logged หน่อย 
    # class พี่เขียนไม่ค่อยดีใช้ยาก เอาเป็น func base
    user = 'TA'
    project = 'Hanuman'
    entity_type = 'Asset'
    entity_name = 'storm'
    task_name = 'rig'
    parent_entity = tlg.create_user_task_track(user, project, entity_type, entity_name, task_name, step='', create=False)
    tlg.create_user_logged_item(parent_entity, project, user, duration=10, duration_append=False, logged_type='User')







def add_logged(): 
    from rf_app.time_logged import time_logged_utils as tlg
    reload(tlg)
    from rf_app.time_logged import summary
    reload(summary)
    # create logged with out object
    user = 'TA'
    project = 'Hanuman'
    entity_type = 'Asset'
    entity_name = 'storm'
    task_name = 'rig'
    parent_entity = tlg.create_user_task_track(user, project, entity_type, entity_name, task_name, step='', create=False)
    tlg.create_user_logged_item(parent_entity, project, user, duration=10, duration_append=False, logged_type='User')

    # create logged 
    track = tlg.UserTimeTrack(user='Pia', project='DarkHorse')
    logged = track.find_task('Asset', 'jaxTwo', 'rig')
    if logged.sg_entity: 
        logged.create_logged(15)

    # create logged and update 
    parent_entity = tlg.create_user_task_track(user, project, entity_type, entity_name, task_name, step='', create=False)
    log = tlg.create_user_logged_item(parent_entity, project, user, duration=20, duration_append=False, logged_type='Automatic')
    track = summary.summary_timetrack(log['id'])
    result = summary.summary_entity_and_task(track)


def create_task(): 
    """
    สำหรับสร้าง task ที่ยังไม่มี 
    ui จะแบ่งเป็นสาม combo 

    |project |v| |entity |v| |task |v| 

    โดยโปรเจ็ค ให้ list มาจาก 
    """
    tlu.list_projects()
    # แล้วเอา entity และ task มาจาก config.yml 
    # พอ user เลือกเสร็จ กดสร้าง ให้ใช้คำสั่งนี้
    project = 'None-Project' # เอามาจาก comboBox ที่เลือก 
    entity_name = 'general' # เอามาจาก comboBox ที่เลือก 
    task_name = 'rnd' # เอามาจาก comboBox ที่เลือก 
    entity_type = 'Asset' # สำหรับหมวดที่สร้างเองอื่น ๆ ให้อยู่ใน Asset เลย
    result = tlu.create_entity(project, entity_name, task_name)
    # พอมี entity ครบ ให้สร้าง task track ต่อ 
    parent_entity = tlu.create_user_task_track('TA', project, entity_type, entity_name, task_name)

    # หลังจากสร้างเสร็จก็ให้เอาไปแสดงบรรทัดแรกของ tree เลย เค้าจะได้เห็นว่าสร้างอะไรไป 


"""

    tlu.create_time_track_from_task('TA', 'Hanuman', True)
tlu.create_entity('None-Project', 'genral', 'rnd')
parent_entity = tlu.create_user_task_track('TA', 'None-Project', 'Asset', 'genral', 'rnd')
tlu.create_user_logged_item(parent_entity, 'None-Project', 'TA', duration=30)
    
    """

    """
    result: 
    user: TA, project: Hanuman, entity: cara, task: rig
        date: None, work minute: 30
        date: None, work minute: 60
        date: 2021-01-10 19:27:33+07:00, work minute: 120
        date: 2021-01-11 11:42:10+07:00, work minute: 60
    user: TA, project: Hanuman, entity: cara, task: model
        date: 2021-01-10 19:31:15+07:00, work minute: 30
    user: TA, project: Hanuman, entity: cara, task: modelUv
        date: 2021-01-11 11:32:32+07:00, work minute: 60
    """

# def test_fetch(): 
#   user = 'TA'
#   result = interval_record.fetch_entry(user)
#   print result


# def test_add(): 
#   """ example use """ 

#   # add entry and append every 15 mins to fields
#   user = 'TA'
#   entity_name = 'cara'
#   step = 'rig'
#   entity_type = 'Asset'
#   project_name = 'Hanuman'
#   logged_type = 'User'
#   duration = 15 
#   duration_append = True
#   result = interval_record.update_entry(
#       user, 
#       entity_name, 
#       step, 
#       entity_type, 
#       project_name, 
#       duration=duration, 
#       duration_append=duration_append, 
#       logged_type=logged_type)

#   print result


# def test_get_entry(): 
#   project = 'Hanuman'
#   user = 'Pia'
#   result = interval_record.sync_tasks_timetrack(project, user)
#   print result


# from rf_app.time_logged import sg_time_log_utils as sgt
# reload(sgt)
# logs = sgt.add_time_logged('Hanuman', 'Asset', 'testProp', 'model', duration=240, logtype='User', description='test', add=True)