import sys
import os
import time
import subprocess
from Qt import QtWidgets
from Qt import QtGui
from Qt import QtCore
from rf_utils.ui import stylesheet
from functools import partial
import modules
import config
from rf_utils import thread_pool
from rf_utils import user_info
DIRNAME = os.path.dirname(__file__).replace('/', '\\')


class Config:
    run_modules = ['hr_sum', 'user_daily', 'project_summary2']
    name_map = {'hr_sum': 'Summary for HR', 'user_daily': 'User daily check', 'project_summary2': 'Project Summary'}
    url_map = {'hr_sum': config.sheet_url, 'user_daily': config.user_url, 'project_summary2': config.project_url}
    wait = {'hr_sum': 5, 'user_daily': 14, 'project_summary2': 10}
    root = os.environ['RFSCRIPT'].replace('/', '\\')
    compiler = '{}\\core\\rf_lib\\python\\gservices\\python.exe'.format(root)


class App(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super(App, self).__init__(parent=parent)
        self.threadpool = QtCore.QThreadPool()
        self.setup_ui()
        self.check_permission()

    def setup_ui(self):
        self.widget = QtWidgets.QWidget()
        self.main_layout = QtWidgets.QVBoxLayout(self.widget)
        self.grid_layout = QtWidgets.QGridLayout()

        self.label = QtWidgets.QLabel('Time Log Summary')
        self.main_layout.addWidget(self.label)
        self.line = self.line(self.main_layout)


        self.status = QtWidgets.QLabel()
        self.main_layout.addLayout(self.grid_layout)
        self.list_buttons(self.grid_layout)
        self.browser_checkBox = QtWidgets.QCheckBox('Open Browser')
        self.browser_checkBox.setChecked(True)
        self.spacer = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.main_layout.addItem(self.spacer)
        self.main_layout.addWidget(self.browser_checkBox)
        self.main_layout.addWidget(self.status)
        self.setCentralWidget(self.widget)
        self.resize(300, 200)

        self.main_layout.setStretch(0, 0)
        self.main_layout.setStretch(1, 0)
        self.main_layout.setStretch(2, 0)
        self.main_layout.setStretch(3, 2)
        self.main_layout.setStretch(4, 1)
        self.setWindowTitle('Time Log Summary')

    def line(self, layout=None):
        line = QtWidgets.QFrame()
        line.setFrameShape(QtWidgets.QFrame.HLine)
        line.setFrameShadow(QtWidgets.QFrame.Sunken)
        layout.addWidget(line) if layout else None

    def list_buttons(self, layout):
        mods = modules.get_module()
        i = 0
        self.buttons = list()
        for mod in Config.run_modules:
            if mod in mods:
                name = Config.name_map[mod]
                label = QtWidgets.QLabel(name)
                button = QtWidgets.QPushButton('Run')
                button.clicked.connect(partial(self.run_thread, mod, button))
                button.setEnabled(False)
                self.buttons.append(button)
                layout.addWidget(label, i, 0)
                layout.addWidget(button, i, 1)
                i+=1

    def check_permission(self):
        user = user_info.User()
        if user.is_producer() or user.is_admin() or user.is_hr():
            for button in self.buttons:
                button.setEnabled(True)
        else:
            self.status.setText('Only producer or admin can view this')
            self.status.setStyleSheet('color: rgb(220, 20, 20);')

    def run_thread(self, func, widget):
        worker = thread_pool.Worker(self.run, func, widget)
        self.threadpool.start(worker)

    def run(self, func, widget):
        sleep = Config.wait[func] * 10
        widget.setEnabled(False)
        name = Config.name_map[func]

        startupinfo = None
        console = True
        if not console:
            startupinfo = subprocess.STARTUPINFO()
            startupinfo.dwFlags |= subprocess.STARTF_USESHOWWINDOW
        runfile = '{}\\modules\\{}.py'.format(DIRNAME, func)
        subprocess.Popen([Config.compiler, runfile], startupinfo=startupinfo)

        for i in range(sleep):
            percent = round((float(i+1)/sleep) * 100.00)
            self.status.setText('Running {} ... {}%'.format(name, percent))
            time.sleep(0.1)

        self.status.setText('Complete')
        if self.browser_checkBox.isChecked():
            open_browser(Config.url_map[func])
        widget.setEnabled(True)


def open_browser(url):
    import webbrowser
    chrome_paths = [
        'C:/Program Files/Google/Chrome/Application/chrome.exe',
        'C:/Program Files (x86)/Google/Chrome/Application/chrome.exe'
        ]

    for path in chrome_paths:
        if os.path.exists(path):
            chrome_path = '{} %s'.format(path)
            webbrowser.get(chrome_path).open(url)
            break
    # chrome_path = 'C:/Program Files (x86)/Google/Chrome/Application/chrome.exe'
    # chrome_path = 'C:/Program Files (x86)/Google/Chrome/Application/chrome.exe %s'


def show():
    app = QtWidgets.QApplication.instance()
    if not app:
        app = QtWidgets.QApplication(sys.argv)
    myApp = App()
    myApp.show()
    stylesheet.set_default(app)
    sys.exit(app.exec_())
