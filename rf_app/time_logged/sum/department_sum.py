import os
import sys
from datetime import datetime

core = '{}/core'.format(os.environ['RFSCRIPT'])
sys.path.append(core) if not core in sys.path else None
import rf_config
from rf_utils.gsheet import googlesheet as gs
from .. import config
from .. import sum_utils


departments = ['model', 'rig', 'groom', 'sim', 'anim', 'pipeline', 'layout', 'edit']


def sum():
    client, service, sheet = config.init_department_summary()
    worksheets = gs.list_worksheet(sheet)

    year = int(datetime.today().strftime('%Y'))
    month = int(datetime.today().strftime('%m'))
    data = sum_utils.write_calendar(year, month)
    wsname = datetime.today().strftime('%b')

    # data = sum_utils.generate_date(year, month)
    for step in departments: 
        data.extend([step])
        loglist = sum_utils.fetch_logs(department, year, month)
        data.extend(loglist)

    if not wsname in worksheets:
        worksheet = gs.new_worksheet(sheet, wsname, 100, 100)
    else:
        worksheet = gs.select_worksheetByTitle(sheet, wsname)

    sum_utils.clear_cell(service, sheet, worksheet)
    sum_utils.write_data(service, sheet, department, data)
    sum_utils.generate_calendar_color(service, sheet, worksheet, data)
    sum_utils.resize_cell(service, sheet, worksheet, 1, 40, 30)


def set_color(): 
    client = gs.createAccount(gs.Setting.keyfile, gs.Scope.sheet_full)
    sheet = gs.openByURL(client, config.sheet_url)
    worksheets = gs.list_worksheet(sheet)
    service = gs.buildService(gs.Setting.keyfile, gs.Scope.sheet_full)
    worksheet = gs.select_worksheetByTitle(sheet, 'model')

    sum_utils.set_cell_color(service, sheet, worksheet, 0, 50, 0, 1, 100, 100, 100)


if __name__ == '__main__':
    # set_color()
    # project = 'Hanuman'
    sum()
    # for step in departments: 
    #     sum(step)
    # export(project, start='2021-W1', end='', logged_type='Automatic')
