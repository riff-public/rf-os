import os
import sys
from calendar import monthrange
from datetime import datetime
from rf_app.time_logged import sg_time_log_utils as stlg


class Day:
    weekdays = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
    weekends = ['Sat', 'Sun']
    workhour = 8.0

class Header: 
    fix_headers = ['Employee_id', 'ad_account', 'Name', 'HR_department']
    field_map = {
        'Employee_id': 'sg_employee_id', 
        'Name': 'sg_hr_local_name', 
        'ad_account': 'sg_ad_account', 
        'HR_department': 'sg_employee_department'}

    project_headers = ['Name', 'department']
    project_field_map = {
        'department': 'department.Department.name', 
        'Name': 'name'}


def write_calendar(year, month): 
    header_data = generate_date(year, month)
    return header_data
    

def generate_date(year, month):
    daylist = ['']
    datelist = ['']
    data = list()
    year = int(year)
    month = int(month)

    for date in range(0, monthrange(year, month)[1]):
        day = Day.weekdays[datetime(year, month, date+1).weekday()]
        daylist.append(day)
        datelist.append(date+1)

    data.append(daylist)
    data.append(datelist)

    return data


def generate_calendar_color(service, sheet, worksheet, data): 
    # grey title 
    col_start = 0 
    col_end = len(data[1])
    row_start = 1
    row_end = 2
    red = 50 
    green = 50 
    blue = 50
    set_cell_color(service, sheet, worksheet, col_start, col_end, row_start, row_end, red, green, blue)
    # set_cell_color(service, sheet, worksheet, 0, 50, 0, 1, 100, 100, 100)

    daylist = data[1]

    for i, day in enumerate(daylist): 
        if day in Day.weekends: 
            set_cell_color(
                service, sheet, worksheet, 
                col_start=i, col_end=i+1, row_start=1, row_end=len(data), 
                red=100, green=100, blue=100
                )


def hr_summary(year, month): 
    celldata = list()
    # data pattern for datadict {'TA': {'Hanuman': 1, 'None-Project': 55}}
    datadict = dict() 
    headers = list()
    projects = stlg.find_active_projects()
    projectlist = sorted([a['name'] for a in projects])

    headers = Header.fix_headers + projectlist
    celldata = [headers]

    logs = stlg.fetch_logs(start_date=(year, month, 1), department='')
    userdict = stlg.list_all_users()

    for log in logs:
        if log['user']:
            user = log['user']['name']
            duration = log['duration']
            duration = duration / 60
            project = log['project']['name']

            if not user in datadict.keys(): 
                datadict[user] = {project: duration}

            else: 
                if not project in datadict[user].keys(): 
                    datadict[user][project] = duration
                else: 
                    datadict[user][project]+=duration

    for user, data in datadict.items(): 
        userlist = list()

        for header in Header.fix_headers:  
            attr = Header.field_map[header]
            userlist.append(userdict[user][attr])

        for project in projectlist: 
            duration = data.get(project, 0)
            userlist.append(duration)

        celldata.append(userlist)
    return celldata


def project_summary(year, month): 
    celldata = list()
    # data pattern for datadict {'TA': {'Hanuman': 1, 'None-Project': 55}}
    datadict = dict() 
    headers = list()
    projects = stlg.find_active_projects()
    projectlist = sorted([a['name'] for a in projects])

    headers = Header.project_headers + projectlist
    celldata = [headers]

    logs = stlg.fetch_logs(start_date=(year, month, 1), department='')
    userdict = stlg.list_all_users()

    for log in logs:
        if log['user']:
            user = log['user']['name']
            duration = log['duration']
            duration = duration / 60
            project = log['project']['name']

            if not user in datadict.keys(): 
                datadict[user] = {project: duration}

            else: 
                if not project in datadict[user].keys(): 
                    datadict[user][project] = duration
                else: 
                    datadict[user][project]+=duration

    for user, data in datadict.items(): 
        userlist = list()

        for header in Header.project_headers:  
            attr = Header.project_field_map[header]
            userlist.append(userdict[user][attr])

        for project in projectlist: 
            duration = data.get(project, 0)
            userlist.append(duration)

        celldata.append(userlist)
    return celldata
            

def fetch_logs(department, year, month):
    end_date = monthrange(year, month)[1]
    max_hour = 8

    logs = stlg.fetch_logs(start_date=(year, month, 1), end_date=(year, month, end_date), department=department)
    summary = dict()

    for log in logs:
        user = log['user']['name']
        duration = log['duration']
        duration = duration / 60.0
        date = log['date']
        dt = datetime.strptime(date, '%Y-%m-%d')
        day = int(dt.strftime('%d'))

        if not user in summary.keys():
            summary[user] = {day: duration}
        else:
            if not day in summary[user].keys():
                summary[user][day] = duration
            else:
                summary[user][day] += duration

    data = list()
    for user, date in summary.items():
        rowlist = [user]

        for i in range(0, monthrange(year, month)[1]):
            date = i + 1
            duration = ''
            if date in summary[user].keys():
                duration = summary[user][date]
                if duration > max_hour: 
                    duration = max_hour
            rowlist.append(duration)

        data.append(rowlist)

    return data


def write_data(service, sheet, worksheet, data):

    service.spreadsheets().values().append(
            spreadsheetId = sheet.id,
            range = "{}!A:Z".format(worksheet),
            body = {
                    "majorDimension": "ROWS",
                    "values": data, 
                    },
            valueInputOption="USER_ENTERED").execute()


def resize_cell(service, sheet, worksheet, col_start, col_end, col_size): 
    request_body = {
                      "requests": [
                        {
                          "updateDimensionProperties": {
                            "range": {
                              "sheetId": worksheet.id,
                              "dimension": "COLUMNS",
                              "startIndex": col_start,
                              "endIndex": col_end
                            },
                            "properties": {
                              "pixelSize": col_size
                            },
                            "fields": "pixelSize"
                          }
                        },
                      ]
                    }
    response = service.spreadsheets().batchUpdate(spreadsheetId=sheet.id, body=request_body).execute()


def set_cell_color(service, sheet, worksheet, col_start, col_end, row_start, row_end, red, green, blue):
    #credentials_file = credentials path(str)
    #sheets = google sheet name (str)
    #worksheet = google tab name (str)
    #col_start, col_end = range of columns (int)
    #row_start, row_end = range of row (int)
    #red, green, blue = color (int)

    request_body = {
                      "requests": [
                        {
                          "repeatCell": {
                            "range": {
                              "sheetId": worksheet.id,
                              "startRowIndex": row_start,
                              "endRowIndex": row_end,
                              'startColumnIndex': col_start,
                              'endColumnIndex': col_end
                            },
                            "cell": {
                              "userEnteredFormat": {
                                "backgroundColor": {
                                  "red": red,
                                  "green": green,
                                  "blue": blue
                                },
                              }
                            },
                            "fields": "userEnteredFormat(backgroundColor,textFormat,horizontalAlignment)"
                          }
                        },
                      ]
                    }
    response = service.spreadsheets().batchUpdate(spreadsheetId=sheet.id, body=request_body).execute()


def clear_cell(service, sheet, worksheet, fields='*'): 
    request_body = {
                      "requests": [
                        {
                          "updateCells": {
                            "range": {
                              "sheetId": worksheet.id,
                            },
                            "fields": "{}".format(fields)
                          }
                        },
                      ]
                    }
    response = service.spreadsheets().batchUpdate(spreadsheetId=sheet.id, body=request_body).execute()


def dict_to_cell(headers, data): 
    """ 
    args: 
        headers (list): ['Hanuman', 'Darkhorse']
        data (dict): {'TA': {'Hanuman': {'value': 10}, 'DarkHorse': {'value': 10}}}

    Returns (list): [['', 'Hanuman', 'DarkHorse'], ['TA', 10, 10]]
    """ 
    cell_list = [['']]
    cell_list[0].extend(headers)

    for k, v in data.items(): 
        item_list = [k]
        for header in headers: 
            if header in v.keys(): 
                value = v[header].get('value')
                item_list.append(value) if value else None
            else: 
                item_list.append('')

        cell_list.append(item_list)

    return cell_list


def write_cell_from_dict(service, sheet, worksheet, headers, data): 
    cell_list = dict_to_cell(headers, data)
    write_data(service, sheet, worksheet, cell_list)


def set_color_from_dict(service, sheet, worksheet, headers, data): 
    """ 
    args: 
        headers (list): ['Hanuman', 'Darkhorse']
        data (dict): {'TA': {'Hanuman': {'color': (50, 50, 50)}, 'DarkHorse': {'color': (50, 50, 50)}}}

    Returns (list): [['', 'Hanuman', 'DarkHorse'], ['TA', 10, 10]]
    """ 
    set_cell_color(service, sheet, worksheet, col_start, col_end, row_start, row_end, red, green, blue)
