
sheet_url = 'https://docs.google.com/spreadsheets/d/1Ld_I-NYoiPx-_UgJ4n42r2VjQMTEGnvRyd3UECkkeAs/edit#gid=0'
user_url = 'https://docs.google.com/spreadsheets/d/1ydUGzH3nJEkoWoc0jzXsC9DyWGMQEWaDVKlHkdCi8og/edit#gid=8891619'
project_url = 'https://docs.google.com/spreadsheets/d/1tiIHnjQXmqbhnJpvjzHa7moWrcw1lmCr7tzHgEONbSk/edit#gid=2043799348'

def init_gsheet(): 
	from rf_utils.gsheet import googlesheet as gs
	client = gs.createAccount(gs.Setting.keyfile, gs.Scope.sheet_full)
	sheet = gs.openByURL(client, sheet_url)
	service = gs.buildService(gs.Setting.keyfile, gs.Scope.sheet_full)

	return client, service, sheet


def init_department_summary(): 
	from rf_utils.gsheet import googlesheet as gs
	client = gs.createAccount(gs.Setting.keyfile, gs.Scope.sheet_full)
	sheet = gs.openByURL(client, user_url)
	service = gs.buildService(gs.Setting.keyfile, gs.Scope.sheet_full)
	return client, service, sheet


def init_service(url): 
	from rf_utils.gsheet import googlesheet as gs
	client = gs.createAccount(gs.Setting.keyfile, gs.Scope.sheet_full)
	sheet = gs.openByURL(client, url)
	service = gs.buildService(gs.Setting.keyfile, gs.Scope.sheet_full)
	return client, service, sheet



	# sheetapi@iron-fountain-298610.iam.gserviceaccount.com
	