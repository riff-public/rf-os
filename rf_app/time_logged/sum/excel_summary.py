from openpyxl import Workbook
from openpyxl.styles import Alignment, colors, fills, Font, Border, Side
from openpyxl.utils import get_column_letter
from openpyxl.utils.cell import column_index_from_string
from openpyxl.chart import BarChart, Series, Reference
from openpyxl.chart.layout import Layout, ManualLayout
from openpyxl.comments import Comment

import string
from itertools import count, product, islice
from datetime import datetime
import calendar
import os
import sys

sys.path.append('{}/core'.format(os.environ['RFSCRIPT']))

class Config_Step:
    departments = ['Art','Model','Groom','Rig','Layout','Anim','Sim','Lighting','FX','Comp','Edit','Pipeline','Realtime','Production','None']
    sg = ['art', 'anim', 'light', 'prod', 'layout', 'model', 'rig', 'groom', 'comp', 'sim', 'fx', 'pipeline', 'edit', 'realtime']

class Config:
    title_RawData = ['ID', 'NAME']
    total_RawData = ['Total']
    title_RawLeave = ['ID', 'NAME', 'Sick', 'Personal', 'Vacation', 'Absent']
    title_Summary = ['ID', 'NAME', 'Work', 'Absent', 'Leave', 'Holiday', 'Total']
    total_RawProject = ['Leave', 'Absent', 'Total', 'Working Hrs']
    chartMonth = {'Jan':'ws_Jan','Feb':'ws_Feb','Mar':'ws_Mar','Apr':'ws_Apr','May':'ws_May','Jun':'ws_Jun','Jul':'ws_Jul'}

class Config_COLOR:
    color = {'blue':'4f81bd', 'green':'9bbb59', 'yellow':'f7dc6f', 'orange':'f79646', 'red':'c0504d', 'violet':'8064a2',
             'honey':'F2C280', 'milk':'FFFDE0', 'snow':'FFFCD2', 'bluemonday':'6B8DC0', 'lavender':'C4C3EE', 'moon':'FFF9AF',
             'amber':'836764'}

    color_project = {'Argrada':'green', 'Bikey-WorldBuild':'blue', 'DarkHorse':'amber', 'DarkHorse3':'violet', 'GoAwayMrTumor':'yellow',
                         'Forbidden':'honey', 'Destiny2':'milk', 'HLH':'snow', 'Hanuman':'red', 'HanumanSq4':'bluemonday',
                         'Holiday3':'orange', 'None-Project':'lavender', 'ScotchBN2021':'moon'}

    check_color_workday = ['00000000','00c0504d','00c0504d','00c0504d','00c0504d']
    check_color_holiday = ['00bfbfbf', '00505050']
    check_color_leave = ['00ff96ff', '00ffa143', '0000C459', '00c0c0c0']

class Config_STYLES:
    Fill = {'month': [fills.PatternFill(patternType='solid', fgColor=colors.Color(rgb='fce5cd')),
                      fills.PatternFill(patternType='solid', fgColor=colors.Color(rgb='ead1dc')), 
                      fills.PatternFill(patternType='solid', fgColor=colors.Color(rgb='fff2cc'))],

            'week_workday': fills.PatternFill(patternType='solid', fgColor=colors.Color(rgb='333f4f')),
            'week_holiday': fills.PatternFill(patternType='solid', fgColor=colors.Color(rgb='bfbfbf')),
            'year_holiday': fills.PatternFill(patternType='solid', fgColor=colors.Color(rgb='505050')),
            'white': fills.PatternFill(patternType='solid', fgColor=colors.Color(rgb='ffffff')),
            'graywhite': fills.PatternFill(patternType='solid', fgColor=colors.Color(rgb='f2f2f2')),
            'department': fills.PatternFill(patternType='solid', fgColor=colors.Color(rgb='ebdef0')),

            'Work': fills.PatternFill(patternType='solid', fgColor=colors.Color(rgb='333f4f')),
            'Absent': fills.PatternFill(patternType='solid', fgColor=colors.Color(rgb='e93c6a')),
            'Leave': fills.PatternFill(patternType='solid', fgColor=colors.Color(rgb='4f81bd')),
            'Holiday': fills.PatternFill(patternType='solid', fgColor=colors.Color(rgb='505050')),
            'Total': fills.PatternFill(patternType='solid', fgColor=colors.Color(rgb='333f4f')),

            'leave':{'sick': fills.PatternFill(patternType='solid', fgColor=colors.Color(rgb='ff96ff')),
                     'personal': fills.PatternFill(patternType='solid', fgColor=colors.Color(rgb='ffa143')),
                     'vacation': fills.PatternFill(patternType='solid', fgColor=colors.Color(rgb='00C459')),
                     'absent': fills.PatternFill(patternType='solid', fgColor=colors.Color(rgb='c0c0c0')),
                     'None': fills.PatternFill(patternType='solid', fgColor=colors.Color(rgb='E93C6A'))},

            'leave_half':{'sick': fills.PatternFill(patternType='solid', fgColor=colors.Color(rgb='c0504d')),
                     'personal': fills.PatternFill(patternType='solid', fgColor=colors.Color(rgb='c0504d')),
                     'vacation': fills.PatternFill(patternType='solid', fgColor=colors.Color(rgb='c0504d')),
                     'absent': fills.PatternFill(patternType='solid', fgColor=colors.Color(rgb='c0504d')),
                     'None': fills.PatternFill(patternType='solid', fgColor=colors.Color(rgb='c0504d'))},

            'none': fills.PatternFill(fill_type=None)}

    Font = {'white': Font(color=colors.Color(rgb='ffffff')),
            'whiteBold': Font(color=colors.Color(rgb='ffffff'),bold=True),
            'black': Font(color=colors.Color(rgb='000000')),
            'blackBold': Font(color=colors.Color(rgb='000000'),bold=True),
            'department': Font(color=colors.Color(rgb='763e8d'), size=20),
            'projectsummary': Font(name='Century Gothic', size=22, bold=True, color=colors.Color(rgb='333f4f')),
            'size': Font(size=9.5),
            'none' :Font(color=None)}

    Border = {'white': Border(left=Side(border_style='thin',color=colors.Color(rgb='ffffff')),
                            right=Side(border_style='thin',color=colors.Color(rgb='ffffff')),
                            top=Side(border_style='thin',color=colors.Color(rgb='ffffff')),
                            bottom=Side(border_style='thin',color=colors.Color(rgb='ffffff'))),
              'red': Border(left=Side(border_style='thick',color=colors.Color(rgb='E93C6A')),
                            right=Side(border_style='thick',color=colors.Color(rgb='E93C6A')),
                            top=Side(border_style='thick',color=colors.Color(rgb='E93C6A')),
                            bottom=Side(border_style='thick',color=colors.Color(rgb='E93C6A'))),
              'blue': Border(left=Side(border_style='thin',color=colors.Color(rgb='333f4f')),
                            right=Side(border_style='thin',color=colors.Color(rgb='333f4f')),
                            top=Side(border_style='thin',color=colors.Color(rgb='333f4f')),
                            bottom=Side(border_style='thin',color=colors.Color(rgb='333f4f'))),

              'none': Border(left=Side(border_style=None),
                            right=Side(border_style=None),
                            top=Side(border_style=None),
                            bottom=Side(border_style=None))}

    Alignment = {'center': Alignment(horizontal='center', vertical='center')}
    Format = {'0': '0', '0.00': '0.00'}

    def Fill_BG(color):
        return fills.PatternFill(patternType='solid', fgColor=colors.Color(rgb=color))

    #!/usr/bin/python3
    # -*- coding: utf-8 -*-
    def Comment_TEXT(*args):
        if len(args) == 1:
            t1 = 'Time Work = %s \nใส่เวลาทำงานเกิน'%(args)
            t2 = 'Time Work = %s \nใส่เวลาทำงานไม่ครบ'%(args)
            t3 = 'Time Work = %s \nไม่ได้ใส่เวลา'%('None')
            if args[0]:
                text = t1 if args[0] > 8 else t2
            else:
                text = t3
        else:
            x1 = 0 if args[0] == 'None' else float(args[0])
            x2 = 0 if args[1] == 'None' else float(args[1])

            tsum_1 = 'Time Work = %s ,\nTime Holiday = %s ,\nใส่เวลาทำงานเกิน'%(args[0], args[1])
            tsum_2 = 'Time Work = %s ,\nTime Holiday = %s ,\nใส่เวลาทำงานไม่ครบ'%(args[0], args[1])
            text = tsum_1 if x1+x2 > 8 else tsum_2
        return Comment(text, "Author")

    def Comment_Holiday(value1, value2):
        text = 'Weekend = %s ,\nHoliday = %s'%(value1, value2)
        return Comment(text, "Author")

class Excel:
    def __init__(self):
        print ('create Excel')

    def create_workbook(self):
        wb = Workbook()
        return wb

    def create_sheet(self, workbook, name, num):
        ws = workbook.create_sheet(name, num)
        ws.title = name
        return ws

    def remove_sheet(self, workbook, name):
        sheet = workbook.get_sheet_by_name(name)
        workbook.remove_sheet(sheet)

    def set_freezePanes(self, worksheet, cell):
        worksheet.freeze_panes = worksheet[cell]

    def merge_cell(self, worksheet, start_cell, end_cell):
        worksheet.merge_cells(f'{start_cell}:{end_cell}')

    def save(self, workbook, path):
        workbook.save(path)

    def sum(self, workbook, cell_sum, start_cell, end_cell):
        workbook[cell_sum] = "=SUM({}:{})".format(str(start_cell),str(end_cell))

class Calendar(Excel):
    def __init__(self, year):
        print ('create Calendar')
        self.title_pos = dict()
        self.month_start = dict()
        self.user_pos = dict()
        self.ws_sum_pos = dict()
        self.summary_month = dict()
        self.summary_project = dict()
        self.userInfo = dict()

    def set_title(self, worksheet='', title=[], start_col_num=1, start_row_num=1, freezePanes='C4'):
        self.set_freezePanes(worksheet, freezePanes)

        for text in title:
            self.title_pos[text.lower()] = start_col_num
            cell = worksheet.cell(column=start_col_num, row=start_row_num)
            cell.value = text
            cell.alignment = Config_STYLES.Alignment['center']
            cell.fill = Config_STYLES.Fill['week_workday']
            cell.font = Config_STYLES.Font['whiteBold']
            cell.border = Config_STYLES.Border['white']

            col = get_column_letter(start_col_num)

            if text == 'ID':
                worksheet.column_dimensions[col].width = 12
            elif text == 'NAME':
                worksheet.column_dimensions[col].width = 26
            else:
                worksheet.column_dimensions[col].width = 4
                cell.alignment = Alignment(textRotation=180, horizontal='center', vertical='center')
                cell.font = Config_STYLES.Font['white']

            start_col_num += 1

            self.merge_cell(worksheet,'%s%s'%(col, 1),'%s%s'%(col, 3))

    def set_user(self, worksheet=None, step=None, users=None, start_col_num=1, start_row_num=4, status=False):
        start_col = get_column_letter(start_col_num)
        start_row = start_row_num
        end_col = get_column_letter(start_col_num+1)

        if step in Config_Step.departments:
            start_cell = '%s%s'%(start_col, start_row)
            end_cell = '%s%s'%(end_col, start_row)
            self.merge_cell(worksheet, start_cell, end_cell)

            worksheet[f'{start_col}{start_row}'] = step
            worksheet[f'{start_col}{start_row}'].alignment = Config_STYLES.Alignment['center']
            worksheet[f'{start_col}{start_row}'].font = Config_STYLES.Font['blackBold']
            worksheet[f'{start_col}{start_row}'].font = Config_STYLES.Font['department']
            worksheet[f'{start_col}{start_row}'].fill = Config_STYLES.Fill['department']

            start_merge_col = get_column_letter(start_col_num+2)
            end_merge_col = get_column_letter(worksheet.max_column)
            start_merge_cell = '%s%s'%(start_merge_col, start_row)
            end_merge_cell = '%s%s'%(end_merge_col, start_row)
            self.merge_cell(worksheet, start_merge_cell, end_merge_cell)
            worksheet[f'{start_merge_col}{start_row}'].fill = Config_STYLES.Fill['department']

            start_row += 1

        step_list = []
        for user in users:
            if users[user]['department.Department.name'] == step:
                if users[user]['sg_employee_id'] == None:
                    users[user]['sg_employee_id'] = " "
                step_list.append((users[user]['sg_employee_id'],users[user]['sg_ad_account'],users[user]['name']))

        for id_num, user, nickname in sorted(step_list):
            worksheet[f'{start_col}{start_row}'] = id_num
            worksheet[f'{start_col}{start_row}'].alignment = Config_STYLES.Alignment['center']

            worksheet[f'{end_col}{start_row}'] = '%s  (%s)'%(user, nickname)
            nickname = user if status else nickname
            self.user_pos[nickname] = start_row

            self.userInfo[nickname] = {'row': start_row}
            for i, title in enumerate(Config.title_Summary):
                if title.lower() == 'holiday':
                    self.userInfo[nickname].update({title.lower(): {'weekend':0, 'holiday':0}})

                elif title.lower() == 'leave':
                    self.userInfo[nickname].update({title.lower(): {'sick':0, 'personal':0, 'vacation':0, 'absent':0}})
                else:
                    if i > 1:
                        self.userInfo[nickname].update({title.lower(): 0})

            start_row += 1

        #start_row += 1
        return start_row

    def header_info(self, worksheet=None,start_col_num=1, start_row_num=1):
        pass



    def set_month(self, worksheet=None, year=2021, start_month=1, end_month=12, start_col_num=3, start_row_num=1):
        start_row = start_row_num
        for month in range(start_month, end_month+1):
            weekday, numdays = calendar.monthrange(year, month)
            end_col_num = start_col_num + (numdays - 1)
            start_col = get_column_letter(start_col_num)
            end_col = get_column_letter(end_col_num)
            start_row = start_row_num

            month_cell = worksheet.cell(row=start_row, column=start_col_num)
            month_cell.value = calendar.month_name[month]
            month_cell.alignment = Config_STYLES.Alignment['center']
            month_cell.fill = Config_STYLES.Fill['month'][month%3]

            self.merge_cell(worksheet, f'{start_col}{start_row}', f'{end_col}{start_row}')
            self.month_start[month] = start_col_num
            start_col_num = end_col_num +1

    def set_month_total(self, worksheet=None, title_total=[], start_col_num=3, start_row_num=1):
        start_row = start_row_num
        for i, text in enumerate(title_total):
            total = worksheet.cell(row=start_row, column=start_col_num+i)
            total.value = text
            total.alignment = Config_STYLES.Alignment['center']
            total.fill = Config_STYLES.Fill['week_workday']
            total.font = Config_STYLES.Font['whiteBold']
            total.border = Config_STYLES.Border['white']

            total_col_start = get_column_letter(start_col_num+i)
            total_start = f'{total_col_start}1'
            total_end = f'{total_col_start}3'

            self.month_start[text] = start_col_num+i
            self.merge_cell(worksheet, total_start, total_end)

    def set_weekday(self, worksheet=None, year=2021, start_month=1, end_month=12, start_col_num=3, start_row_num=1):
        weekDaysMapping = ("Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday")
        week_row = 2
        day_row = 3
        start_col = start_col_num
        for month in range(start_month, end_month+1):
            weekday, numdays = calendar.monthrange(year, month)
            for day in range(1, numdays+1):
                dt = datetime(year, month, day)
                weekday = weekDaysMapping[dt.weekday()]
                weekday = weekday[0] if dt.weekday() not in (5, 6) else ''

                day_col = get_column_letter(start_col)
                week_cell = f'{day_col}{week_row}'
                day_cell = f'{day_col}{day_row}'
                worksheet[day_cell] = day
                worksheet[week_cell] = weekday
                worksheet.column_dimensions[day_col].width = 3

                if dt.weekday() in (5, 6):
                    week_fill = Config_STYLES.Fill['week_holiday']
                    week_font = Config_STYLES.Font['black']
                    week_border = Config_STYLES.Border['white']
                    week_alignment = Config_STYLES.Alignment['center']

                else:
                    week_fill = Config_STYLES.Fill['week_workday']
                    week_font = Config_STYLES.Font['white']
                    week_border = Config_STYLES.Border['white']
                    week_alignment = Config_STYLES.Alignment['center']

                worksheet[week_cell].fill = week_fill
                worksheet[week_cell].font = week_font
                worksheet[week_cell].border = week_border
                worksheet[week_cell].alignment = week_alignment
                worksheet[day_cell].fill = week_fill
                worksheet[day_cell].font = week_font
                worksheet[day_cell].border = week_border
                worksheet[day_cell].alignment = week_alignment

                start_col += 1

    def set_official_holidays(self, worksheet=None, year=2021, month=1):
        weekday, numdays = calendar.monthrange(year, month)
        first_day = (year, month, 1)
        last_day = (year, month, numdays)
        holidays = sg_schedule.get_studio_schedule(first_day, last_day)
        #holidays = holiday_dict.result

        for holiday in holidays:
            holiday_day = int(holiday.split('-')[-1])
            holiday_month = int(holiday.split('-')[1])
            start_col_num = self.month_start[holiday_month]

            holiday_col_num = start_col_num + (holiday_day - 1 )
            holiday_col = get_column_letter(holiday_col_num)
            for i in range(5, worksheet.max_row+1):
                if worksheet[f'A{i}'].value not in Config_Step.departments:
                    worksheet[f'{holiday_col}{i}'].fill = Config_STYLES.Fill['year_holiday']
                    worksheet[f'{holiday_col}{i}'].border = Config_STYLES.Border['white']
                    worksheet[f'{holiday_col}{i}'].font = Config_STYLES.Font['white']
                    worksheet[f'{holiday_col}{i}'].alignment = Config_STYLES.Alignment['center']

    def set_holiday_SSday(self, worksheet=None, year=2021, month=1):
        weekday, numdays = calendar.monthrange(year, month)
        month_num = self.month_start[month]
        for day in range(1, numdays+1):
            start_col = month_num + (day - 1)
            day_col = get_column_letter(start_col)
            dt = datetime(year, month, day)
            if dt.weekday() in (5, 6):#Saturday, Sunday
                for i in range(5, worksheet.max_row+1):
                    if worksheet[f'A{i}'].value not in Config_Step.departments:
                        worksheet[f'{day_col}{i}'].fill = Config_STYLES.Fill['week_holiday']
                        worksheet[f'{day_col}{i}'].border = Config_STYLES.Border['white']
                        worksheet[f'{day_col}{i}'].font = Config_STYLES.Font['black']
                        worksheet[f'{day_col}{i}'].alignment = Config_STYLES.Alignment['center']

            start_col += 1

    def set_leave_day(self, worksheet=None, users_data=[], year=2021, month=1, status=False):
        for leave in users_data:
            for data in leave:
                y, m, d  = data['start_date'].split('-')
                year_leave, month_leave, day_leave = int(y), int(m), int(d)
                month_col_num = self.month_start[month_leave]
                col_num = month_col_num + (int(day_leave) -1)

                num_days = 1
                if not data['start_date'] == data['end_date']:
                    y_end, m_end, d_end  = data['end_date'].split('-')
                    num_days += int(d_end) - (day_leave)

                if data['user']['name'] in self.user_pos.keys() and year_leave == year and month_leave == month:
                    row = self.user_pos[data['user']['name']]
                    data['sg_leave_type'] = 'None' if data['sg_leave_type'] == None else data['sg_leave_type']

                    divisor = self.number_days_leave(worksheet=worksheet, data=data, num_days=num_days, col_num=col_num, row=row)
                    if status:
                        self.set_time_leave(worksheet=worksheet, data=data, num_days=num_days, divisor=divisor, col_num=col_num, row=row)
                        #self.sum_time_leave(worksheet=worksheet, data=data, num_days=num_days, col_num=col_num, row=row)

    def number_days_leave(self, worksheet=None, data='', num_days='', col_num='', row=''):
        divisor = num_days
        for num in range(num_days):
            col = get_column_letter(col_num)
            if worksheet[f'{col}{row}'].fill.start_color.index in Config_COLOR.check_color_holiday:
                divisor -= 1
            else:
                worksheet[f'{col}{row}'].fill = Config_STYLES.Fill['leave'][data['sg_leave_type']]
                worksheet[f'{col}{row}'].border = Config_STYLES.Border['white']
            col_num += 1
        return divisor

    def set_time_leave(self, worksheet=None, data='', num_days='', divisor='', mins=60, col_num='', row=''):
        for num in range(num_days):
            col = get_column_letter(col_num)
            if worksheet[f'{col}{row}'].fill.start_color.index not in Config_COLOR.check_color_holiday:
                worksheet[f'{col}{row}'].value = ((data['sg_duration']/divisor)/mins)
                worksheet[f'{col}{row}'].alignment = Config_STYLES.Alignment['center']
            col_num += 1

    def sum_time_leave(self, worksheet=None, data='', num_days='', col_num='', row=''):
        col_leave = get_column_letter(self.title_pos[data['sg_leave_type']])
        for num in range(num_days):
            col = get_column_letter(col_num)
            if worksheet[f'{col}{row}'].fill.start_color.index not in Config_COLOR.check_color_holiday:
                leave_value = worksheet[f'{col_leave}{row}'].value
                days_value = worksheet[f'{col}{row}'].value
                leave_value = (leave_value + days_value) if leave_value else days_value

                #worksheet[f'{col_leave}{row}'].value = leave_value
                #worksheet[f'{col_leave}{row}'].alignment = Config_STYLES.Alignment['center']
            col_num += 1

    def set_time_workday(self, worksheet=None, list_data=[], month=1):
        for data in list_data:
            if data[0] in self.user_pos.keys():
                user_row = self.user_pos[data[0]]
                month_num = self.month_start[month]
                for num, dt in enumerate(data):
                    if not num == 0:
                        start_month_col = get_column_letter(month_num)
                        if dt and worksheet[f'{start_month_col}{user_row}'].value:
                            dt = '%s,%s'%(worksheet[f'{start_month_col}{user_row}'].value, dt)
                        elif worksheet[f'{start_month_col}{user_row}'].value:
                            dt = worksheet[f'{start_month_col}{user_row}'].value

                        worksheet[f'{start_month_col}{user_row}'] = dt
                        worksheet[f'{start_month_col}{user_row}'].alignment = Config_STYLES.Alignment['center']
                        month_num += 1

    def check_time_holiday(self, worksheet=None, col='', row='', value=8, set_value=False):
        holiday_dict = dict()

        if worksheet[f'{col}{row}'].fill.start_color.index == Config_COLOR.check_color_holiday[0]:
            holiday_dict = {'weekend':8, 'holiday': None}
        else:
            holiday_dict = {'weekend':None, 'holiday': 8}

        if worksheet[f'{col}{row}'].value and set_value:
            worksheet[f'{col}{row}'].comment = Config_STYLES.Comment_TEXT(worksheet[f'{col}{row}'].value, value)
            worksheet[f'{col}{row}'].value = value
            worksheet[f'{col}{row}'].border = Config_STYLES.Border['red']
            worksheet[f'{col}{row}'].fill = Config_STYLES.Fill['none']
            worksheet[f'{col}{row}'].font = Config_STYLES.Font['none']
        else:
            worksheet[f'{col}{row}'].value = value if set_value else ''

        return holiday_dict

    def check_time_workday(self, worksheet=None, col='', row='', value=8, set_value=False):
        if worksheet[f'{col}{row}'].value:
            if worksheet[f'{col}{row}'].value != value:
                if set_value:
                    worksheet[f'{col}{row}'].comment = Config_STYLES.Comment_TEXT(worksheet[f'{col}{row}'].value)
                    worksheet[f'{col}{row}'].value = value
                worksheet[f'{col}{row}'].border = Config_STYLES.Border['red']
            return (value, None)
        else:
            worksheet[f'{col}{row}'].comment = Config_STYLES.Comment_TEXT(None)
            worksheet[f'{col}{row}'].fill = Config_STYLES.Fill['leave']['None']
            worksheet[f'{col}{row}'].font = Config_STYLES.Font['white']
            worksheet[f'{col}{row}'].border = Config_STYLES.Border['white']
            return (None, value)

    def check_time_leave(self, worksheet=None, col='', row='', value=8.0, set_value=False):
        if worksheet[f'{col}{row}'].fill.start_color.index == Config_COLOR.check_color_leave[0]:
            text_color = 'sick'
        elif worksheet[f'{col}{row}'].fill.start_color.index == Config_COLOR.check_color_leave[1]:
            text_color = 'personal'
        elif worksheet[f'{col}{row}'].fill.start_color.index == Config_COLOR.check_color_leave[2]:
            text_color = 'vacation'
        else:
            text_color = 'absent'

        leave_retrun = (None,None)
        if worksheet[f'{col}{row}'].value and set_value:
            value_num, leave_num, work_num = 0, 0, 0

            if type(worksheet[f'{col}{row}'].value) == str:
                leave_num, work_num = worksheet[f'{col}{row}'].value.split(',')
                value_num = float(leave_num) + float(work_num)
                leave_retrun = (8, None) if float(leave_num) == value else (4, 4)

            else:
                value_num = worksheet[f'{col}{row}'].value
                leave_num = worksheet[f'{col}{row}'].value
                work_num = "None"

                leave_retrun = (8, None) if float(leave_num) == value else (4, 4)

            if value_num != value:
                worksheet[f'{col}{row}'].comment = Config_STYLES.Comment_TEXT(work_num, leave_num)
                worksheet[f'{col}{row}'].value = value
                worksheet[f'{col}{row}'].fill = Config_STYLES.Fill['none']
                worksheet[f'{col}{row}'].border = Config_STYLES.Border['red']
            else:
                worksheet[f'{col}{row}'].value = value
                worksheet[f'{col}{row}'].fill = Config_STYLES.Fill['none']
                worksheet[f'{col}{row}'].border = Config_STYLES.Border['none']

        leave, work = leave_retrun
        return (text_color, leave, work)

    def check_workday(self, worksheet=None, year=2021, month=1, set_value=False):
        weekday, numdays = calendar.monthrange(year, month)
        today = datetime.today().strftime('%Y-%m-%d')
        y, m, d  = today.split('-')
        year_today, month_today, day_today = int(y), int(m), int(d)

        last_day = self.month_start[month] + (numdays)
        if month >= month_today:
            last_day = self.month_start[month_today] + (day_today - 1)

        for user in self.user_pos:
            row_user = self.user_pos[user]
            for col in range(self.month_start[month], last_day):
                col_user = get_column_letter(col)
                worksheet[f'{col_user}{row_user}'].alignment = Config_STYLES.Alignment['center']
                if worksheet[f'{col_user}{row_user}'].fill.start_color.index in Config_COLOR.check_color_holiday:
                    value = self.check_time_holiday(worksheet=worksheet, col=col_user, row=row_user, set_value=set_value)
                    for i in value:
                        value_num = self.userInfo[user]['holiday'][i] + value[i] if value[i] else self.userInfo[user]['holiday'][i]
                        self.userInfo[user]['holiday'][i] = value_num

                elif worksheet[f'{col_user}{row_user}'].fill.start_color.index in Config_COLOR.check_color_workday:
                    value_work, value_absent  = self.check_time_workday(worksheet=worksheet, col=col_user, row=row_user, set_value=set_value)
                    value_work = self.userInfo[user]['work'] + value_work if value_work else self.userInfo[user]['work']
                    value_absent = self.userInfo[user]['absent'] + value_absent if value_absent else self.userInfo[user]['absent']
                    self.userInfo[user]['work'] = value_work
                    self.userInfo[user]['absent'] = value_absent

                elif worksheet[f'{col_user}{row_user}'].fill.start_color.index in Config_COLOR.check_color_leave:
                    text_color, value_leave, value_work = self.check_time_leave(worksheet=worksheet, col=col_user, row=row_user, set_value=set_value)
                    value_leave = self.userInfo[user]['leave'][text_color] + value_leave if value_leave else self.userInfo[user]['leave'][text_color]
                    value_work = self.userInfo[user]['work'] + value_work if value_work else self.userInfo[user]['work']
                    self.userInfo[user]['leave'][text_color] = value_leave
                    self.userInfo[user]['work'] = value_work

    def sum_time_total(self, worksheet=None, start_col=3, end_col=6, sum_col=7):
        hrs = 8
        for user in self.userInfo:
            row = self.userInfo[user]['row']
            for title in range(start_col, sum_col+1):
                col = get_column_letter(title)
                if worksheet[f'{col}1'].value == 'Holiday':
                    value_1 = self.userInfo[user][(worksheet[f'{col}1'].value).lower()]['weekend']
                    value_2 = self.userInfo[user][(worksheet[f'{col}1'].value).lower()]['holiday']
                    value = value_1+value_2

                    worksheet[f'{col}{row}'].value = value/hrs
                    #username_col = get_column_letter(2)
                    worksheet[f'{col}{row}'].comment = Config_STYLES.Comment_Holiday(value_1/hrs, value_2/hrs)
                    worksheet[f'{col}{row}'].comment.width = 120

                elif worksheet[f'{col}1'].value == 'Leave':
                    value = 0
                    leave_dict = self.userInfo[user]['leave']
                    for leave in leave_dict:
                        value += leave_dict[leave]
                    worksheet[f'{col}{row}'].value = value/hrs
                    worksheet[f'{col}{row}'].comment = Comment(leave_dict, 'Author')
                    worksheet[f'{col}{row}'].comment.width = 120

                else:
                    worksheet[f'{col}{row}'].value = self.userInfo[user][(worksheet[f'{col}1'].value).lower()]/hrs

                worksheet[f'{col}{row}'].fill = Config_STYLES.Fill[worksheet[f'{col}1'].value]
                worksheet[f'{col}{row}'].border = Config_STYLES.Border['white']
                worksheet[f'{col}{row}'].font = Config_STYLES.Font['white']
                worksheet[f'{col}{row}'].alignment = Config_STYLES.Alignment['center']

            start_cell = f'{get_column_letter(start_col)}{row}'
            end_cell = f'{get_column_letter(end_col)}{row}'
            sum_cell = f'{get_column_letter(sum_col)}{row}'

            self.sum(worksheet,  sum_cell, start_cell, end_cell)

    def sum_time_workday(self, worksheet=None, year=2021, start_month=1, end_month=12, start_col_num=3, start_row_num=5):
        weekday, numdays = calendar.monthrange(year, end_month)
        for row in range(start_row_num, worksheet.max_row+1):
            sum_col = get_column_letter(self.month_start['Total'])
            start_col = get_column_letter(self.month_start[start_month])
            end_col = get_column_letter(self.month_start[end_month] + (numdays -1 ))

            if worksheet[f'A{row}'].value not in Config_Step.departments:
                sum_cell = f'{sum_col}{row}'
                start_cell = f'{start_col}{row}'
                end_cell = f'{end_col}{row}'

                worksheet[sum_cell].alignment = Config_STYLES.Alignment['center']
                self.sum(worksheet,  sum_cell, start_cell, end_cell)

    def set_head_project(self, worksheet=None, data=[], start_row_num=1, start_col_num=3, size_cell=12.5):
        for num, projectName in enumerate(data[0]):
            if num > 1:
                project_col = get_column_letter(start_col_num)
                worksheet[f'{project_col}{start_row_num}'] = projectName
                worksheet[f'{project_col}{start_row_num}'].fill = Config_STYLES.Fill_BG(Config_COLOR.color[Config_COLOR.color_project[projectName]])
                worksheet[f'{project_col}{start_row_num}'].alignment = Config_STYLES.Alignment['center']
                worksheet[f'{project_col}{start_row_num}'].font = Config_STYLES.Font['blackBold']
                worksheet[f'{project_col}{start_row_num}'].border = Config_STYLES.Border['blue']
                worksheet.column_dimensions[project_col].width = size_cell
                self.merge_cell(worksheet, f'{project_col}{start_row_num}', f'{project_col}{start_row_num+2}')
                start_col_num += 1

    def set_total_project(self, worksheet=None, title_total=[], start_col_num=3, start_row_num=1):
        start_row = start_row_num
        for i, text in enumerate(title_total):
            total = worksheet.cell(row=start_row, column=start_col_num+i)
            total.value = text
            total.alignment = Config_STYLES.Alignment['center']
            total.fill = Config_STYLES.Fill['week_workday']
            total.font = Config_STYLES.Font['whiteBold']
            total.border = Config_STYLES.Border['white']

            total_col_start = get_column_letter(start_col_num+i)
            total_start = f'{total_col_start}1'
            total_end = f'{total_col_start}3'

            self.month_start[text] = start_col_num+i
            self.merge_cell(worksheet, total_start, total_end)

    def set_data_project(self, worksheet=None, data=[], start_col_num=3, start_row_num=5):
        for num, user in enumerate(data):
            row = 0
            start_col = start_col_num
            if user[0] in self.user_pos.keys():
                row = self.user_pos[user[0]]
            if num >= 1 and row != 0:
                for i in range(2, len(user)):
                    col = get_column_letter(start_col)
                    worksheet[f'{col}{row}'] = user[i]
                    worksheet[f'{col}{row}'].number_format = Config_STYLES.Format['0.00'] if worksheet[f'{col}{row}'].value > 0 else Config_STYLES.Format['0']
                    worksheet[f'{col}{row}'].alignment = Config_STYLES.Alignment['center']
                    start_col += 1

    def sum_project(self, worksheet=None, start_col_num=3, start_row_num=5):
        total_col = get_column_letter(start_col_num-2)
        total_end_col = get_column_letter(start_col_num-1)
        total = f'{total_col}{worksheet.max_row+1}'
        worksheet[total] = "TOTAL"
        worksheet[total].alignment = Config_STYLES.Alignment['center']
        worksheet[total].font = Config_STYLES.Font['whiteBold']
        worksheet[total].fill = Config_STYLES.Fill['week_workday']

        worksheet.row_dimensions[worksheet.max_row].height = 30
        self.merge_cell(worksheet, total, f'{total_end_col}{worksheet.max_row}')

        for col in range(start_col_num, worksheet.max_column+1):
            start_col = get_column_letter(col)
            start_cell = f'{start_col}{start_row_num}'
            end_cell = f'{start_col}{worksheet.max_row-1}'
            sum_cell = f'{start_col}{worksheet.max_row}'
            
            self.sum(worksheet, sum_cell, start_cell, end_cell)
            worksheet[sum_cell].alignment = Config_STYLES.Alignment['center']
            worksheet[sum_cell].number_format = Config_STYLES.Format['0.00']
            worksheet[sum_cell].fill = Config_STYLES.Fill['week_workday']
            worksheet[sum_cell].border = Config_STYLES.Border['white']
            worksheet[sum_cell].font = Config_STYLES.Font['white']

            project_col = get_column_letter(col)
            self.ws_sum_pos[worksheet[f'{project_col}1'].value] = f'{start_col}{worksheet.max_row}'


    def chart_month(self, worksheet=None, months=13, start_col_num=3, start_row_num=5):
        worksheet.column_dimensions['A'].width = 3
        frist_col = get_column_letter(start_col_num-1)
        worksheet.column_dimensions[frist_col].width = 20
        for month in range(1, months):
            start_col = get_column_letter(start_col_num)
            worksheet[f'{start_col}{start_row_num}'] = calendar.month_abbr[month]
            worksheet[f'{start_col}{start_row_num}'].alignment = Config_STYLES.Alignment['center']
            worksheet[f'{start_col}{start_row_num}'].font = Config_STYLES.Font['white']
            worksheet[f'{start_col}{start_row_num}'].fill = Config_STYLES.Fill['week_workday']
            worksheet[f'{start_col}{start_row_num}'].border = Config_STYLES.Border['white']

            for row in range(start_row_num+1, worksheet.max_row+1):
                if month%2 == 0:
                    worksheet[f'{start_col}{row}'].fill = Config_STYLES.Fill['graywhite']
                else:
                    worksheet[f'{start_col}{row}'].fill = Config_STYLES.Fill['white']

            worksheet.column_dimensions[start_col].width = 13
            self.summary_month[calendar.month_abbr[month]] = start_col_num
            start_col_num += 1

    def chart_project(self, worksheet=None, sheetList=[], start_col_num=2, start_row_num=6):
        self.project = []
        for sheet in sheetList:
            for num in range(3, sheet.max_column+1):
                project_col = get_column_letter(num)
                if sheet[f'{project_col}1'].value not in self.project:
                    self.project.append(sheet[f'{project_col}1'].value)

        for proj in self.project:
            start_col = get_column_letter(start_col_num)
            worksheet[f'{start_col}{start_row_num}'] = proj
            worksheet[f'{start_col}{start_row_num}'].fill = Config_STYLES.Fill['graywhite']
            worksheet.row_dimensions[start_row_num].height = 20
            self.summary_project[proj] = start_row_num
            start_row_num += 1

    def chart_data_project(self, worksheet=None, sheetList=[]):
        for sheet in sheetList:
            month_col =  get_column_letter(self.summary_month[sheet.title])
            sheet_start_col = get_column_letter(3)
            sheet_end_col = get_column_letter(sheet.max_column)
            start_cell = '%s!%s'%(sheet.title, f'{sheet_start_col}{sheet.max_row}')
            end_cell = '%s!%s'%(sheet.title, f'{sheet_end_col}{sheet.max_row}')
            for project in self.summary_project:
                worksheet[f'{month_col}{self.summary_project[project]}'].number_format = Config_STYLES.Format['0.00']
                worksheet[f'{month_col}{self.summary_project[project]}'].alignment = Config_STYLES.Alignment['center']
                worksheet[f'{month_col}{self.summary_project[project]}'].font = Font(name='Calibri')
                worksheet[f'{month_col}{self.summary_project[project]}'] = ('=%s!%s/sum(%s:%s)*100'%(sheet.title,
                                                                                                    self.ws_sum_pos[project],
                                                                                                    start_cell,
                                                                                                    end_cell))

    def chart_allYaer(self, worksheet=None, start_col_num=1,start_row_num=3):
        worksheet['B2'] = 'Project Summary'
        worksheet['B2'].font = Config_STYLES.Font['projectsummary']
        worksheet.row_dimensions[2].height = 30
        worksheet.row_dimensions[start_row_num].height = 270

        chart = BarChart()
        chart.type = "col"
        chart.style = 10
        chart.height = 9 # default is 7.5
        chart.width = 35 # default is 15

        chart.layout = Layout(manualLayout=ManualLayout(x=0, y=0, h=0.9, w=0.85))
        data = Reference(worksheet, min_col=2, min_row=6, max_col=14, max_row=18)
        cats = Reference(worksheet, min_col=3, min_row=5, max_col=14)
        chart.add_data(data, titles_from_data=True, from_rows=True)
        chart.set_categories(cats)

        for i, s in enumerate(chart.series):
            #s.graphicalProperties.line.solidFill = Config_COLOR.color[Config_COLOR.color_project[self.project[i]]]
            s.graphicalProperties.solidFill = Config_COLOR.color[Config_COLOR.color_project[self.project[i]]]

        worksheet.add_chart(chart, "B3")

from rf_app.time_logged import sg_time_log_utils as stlg
from rf_app.time_logged.sum import sum_utils
from rf_utils.sg import sg_schedule

userdict = stlg.list_all_users()
user_data = sg_schedule.get_all_leave_info()
start_month = 1
end_month = 13
year = 2021

file = Calendar(2021)
wb_test = file.create_workbook()

ws_Header = file.create_sheet(wb_test, 'Header', 0)
file.header_info(worksheet=ws_Header)

#file.remove_sheet(wb_test, 'Sheet')
ws_RawData = file.create_sheet(wb_test, 'RawData', 1)
file.set_title(worksheet=ws_RawData, title=Config.title_RawData)
file.set_month(worksheet=ws_RawData, year=year)
file.set_weekday(worksheet=ws_RawData, year=year)
file.set_month_total(worksheet=ws_RawData, title_total=Config.total_RawData, start_col_num=ws_RawData.max_column+1, start_row_num=1)

start_row = 4
for step in Config_Step.departments:
    start_row = file.set_user(worksheet=ws_RawData, step=step, users=userdict, start_row_num=start_row)
for month in range(start_month, end_month):
    file.set_leave_day(worksheet=ws_RawData, users_data=user_data, year=year, month=month)
    file.set_official_holidays(worksheet=ws_RawData, year=year, month=month)
    file.set_holiday_SSday(worksheet=ws_RawData, year=year, month=month)
    for step in Config_Step.sg:
        data = sum_utils.fetch_logs(step, year, month)
        file.set_time_workday(worksheet=ws_RawData, list_data=data, month=month)
    if month >= 4:
        file.check_workday(worksheet=ws_RawData, year=year, month=month)

file.sum_time_workday(worksheet=ws_RawData, start_month=1)

ws_RawLeave = file.create_sheet(wb_test, 'RawLeave', 2)
file.set_title(worksheet=ws_RawLeave, title=Config.title_RawLeave, freezePanes='G4')
file.set_month(worksheet=ws_RawLeave, year=year, start_col_num=7)
file.set_weekday(worksheet=ws_RawLeave, year=year, start_col_num=7)
start_row = 4
for step in Config_Step.departments:
    start_row = file.set_user(worksheet=ws_RawLeave, step=step, users=userdict, start_row_num=start_row)
for month in range(start_month, end_month):
    file.set_official_holidays(worksheet=ws_RawLeave, year=year, month=month)
    file.set_holiday_SSday(worksheet=ws_RawLeave, year=year, month=month)
    file.set_leave_day(worksheet=ws_RawLeave, users_data=user_data, year=year, month=month, status=True)

ws_Summary = file.create_sheet(wb_test, 'Summary', 3)
file.set_title(worksheet=ws_Summary, title=Config.title_Summary, freezePanes='H4')
file.set_month(worksheet=ws_Summary, year=year, start_col_num=8)
file.set_weekday(worksheet=ws_Summary, year=year, start_col_num=8)

start_row = 4
for step in Config_Step.departments:
    start_row = file.set_user(worksheet=ws_Summary, step=step, users=userdict, start_row_num=start_row)
for month in range(start_month, end_month):
    file.set_official_holidays(worksheet=ws_Summary, year=year, month=month)
    file.set_holiday_SSday(worksheet=ws_Summary, year=year, month=month)
    file.set_leave_day(worksheet=ws_Summary, users_data=user_data, year=year, month=month, status=True)
    for step in Config_Step.sg:
        data = sum_utils.fetch_logs(step, year, month)
        file.set_time_workday(worksheet=ws_Summary, list_data=data, month=month)

    file.check_workday(worksheet=ws_Summary, year=year, month=month, set_value=True)
file.sum_time_total(worksheet=ws_Summary, start_col=3, end_col=6, sum_col=7)

sheetList = []
sheet_num = 3
for i, month in enumerate(Config.chartMonth):
    Config.chartMonth[month] = file.create_sheet(wb_test, month, sheet_num)
    file.set_title(worksheet=Config.chartMonth[month], title=Config.title_RawData)
    dataMay = sum_utils.project_summary(year, i+1)
    file.set_head_project(worksheet=Config.chartMonth[month], data=dataMay)
    #file.set_total_project(worksheet=Config.chartMonth[month], title_total=Config.total_RawProject, start_col_num=Config.chartMonth[month].max_column+1, start_row_num=1)
    start_row = 4
    for step in Config_Step.departments:
        start_row = file.set_user(worksheet=Config.chartMonth[month], step=step, users=userdict, start_row_num=start_row)
    file.set_data_project(worksheet=Config.chartMonth[month], data=dataMay)
    file.sum_project(worksheet=Config.chartMonth[month])
    sheetList.append(Config.chartMonth[month])
    sheet_num += 1

ws_summary = file.create_sheet(wb_test, "Project summary", 0)
file.chart_project(worksheet=ws_summary, sheetList=sheetList)
file.chart_month(worksheet=ws_summary)
file.chart_data_project(worksheet=ws_summary, sheetList=sheetList)
file.chart_allYaer(worksheet=ws_summary)


desktop_dir = os.path.join(os.environ['HOMEDRIVE'], os.environ["HOMEPATH"], "Desktop")
namefile = '%s%s'%('test','.xlsx')
path = os.path.join(desktop_dir, namefile)
file.save(wb_test,path)