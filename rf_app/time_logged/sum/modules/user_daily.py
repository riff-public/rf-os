import os
import sys
from datetime import datetime

core = '{}/core'.format(os.environ['RFSCRIPT'])
sys.path.append(core) if not core in sys.path else None
import rf_config
from rf_utils.gsheet import googlesheet as gs
from rf_app.time_logged.sum import config
from rf_app.time_logged.sum import sum_utils

departments = ['art', 'anim', 'light',
    'prod', 'layout', 'model', 'rig', 'groom', 'comp', 'sim', 'fx', 'pipeline', 'realtime', 'edit']


def sum():
    client, service, sheet = config.init_department_summary()
    worksheets = gs.list_worksheet(sheet)

    year = int(datetime.today().strftime('%Y'))
    month = int(datetime.today().strftime('%m'))
    wsname = datetime.today().strftime('%b')
    # month = 5
    # wsname = 'May'
    data = [[wsname]]
    calendar_data = sum_utils.write_calendar(year, month)
    data.extend(calendar_data)

    # data = sum_utils.generate_date(year, month)
    for step in departments:
        data.append(['--{}--'.format(step)])
        loglist = sum_utils.fetch_logs(step, year, month)
        data.extend(loglist)
        data.append([''])

    if not wsname in worksheets:
        worksheet = gs.new_worksheet(sheet, wsname, 100, 100)
    else:
        worksheet = gs.select_worksheetByTitle(sheet, wsname)

    sum_utils.clear_cell(service, sheet, worksheet)
    set_header_color(service, sheet, worksheet)
    sum_utils.write_data(service, sheet, wsname, data)
    sum_utils.generate_calendar_color(service, sheet, worksheet, data)
    set_step_color(service, sheet, worksheet, data)
    sum_utils.resize_cell(service, sheet, worksheet, 1, 40, 30)


def set_step_color(service, sheet, worksheet, data):
    # grey title

    for i, item in enumerate(data):
        if '--' in item[0]:
            sum_utils.set_cell_color(
                service, sheet, worksheet,
                col_start=0, col_end=32, row_start=i, row_end=i+1,
                red=50, green=50, blue=50
                )

def set_header_color(service, sheet, worksheet):
    sum_utils.set_cell_color(
                service, sheet, worksheet,
                col_start=0, col_end=32, row_start=0, row_end=1,
                red=100, green=70, blue=50
                )


if __name__ == '__main__':
    # set_color()
    # project = 'Hanuman'
    sum()
    # for step in departments:
    #     sum(step)
    # export(project, start='2021-W1', end='', logged_type='Automatic')
