import os
import sys
from datetime import datetime

core = '{}/core'.format(os.environ['RFSCRIPT'])
sys.path.append(core) if not core in sys.path else None
import rf_config
from rf_utils.gsheet import googlesheet as gs
from rf_app.time_logged.sum import sum_utils


class Config: 
    url = 'https://docs.google.com/spreadsheets/d/191p1JwuKRfCeQmRfaZAlzgFa7LQotS_bHvxeTwG-PVw/edit#gid=0'


def init_gservice(): 
    client = gs.createAccount(gs.Setting.keyfile, gs.Scope.sheet_full)
    service = gs.buildService(gs.Setting.keyfile, gs.Scope.sheet_full)
    sheet = gs.openByURL(client, Config.url)
    return client, service, sheet

def main_daily():
    wsname = 'Raw' # worksheet name 
    client, service, sheet = init_gservice()
    worksheets = gs.list_worksheet(sheet)
    
    if wsname in worksheets: 
        worksheet = gs.select_worksheetByTitle(sheet, wsname)
    else: 
        worksheet = gs.new_worksheet(sheet, wsname, 100, 100)

    print(worksheet)

    data = [['Month', 'Day', 'Year'], ['Jan', '01', '2021'], ['Feb', '02', '2021']]
    sum_utils.write_data(service, sheet, wsname, data)
    col_start = 0 
    col_end = 20 
    row_start = 0 
    row_end = 2 
    red = 100 
    green = 100 
    blue = 100
    sum_utils.set_cell_color(service, sheet, worksheet, col_start, col_end, row_start, row_end, red, green, blue)


if __name__ == '__main__':
    main_daily()

