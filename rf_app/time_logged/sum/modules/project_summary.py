import os
import sys
from datetime import datetime

core = '{}/core'.format(os.environ['RFSCRIPT'])
sys.path.append(core) if not core in sys.path else None
import rf_config
from rf_utils.gsheet import googlesheet as gs
from rf_app.time_logged.sum import config
from rf_app.time_logged.sum import sum_utils
from rf_app.time_logged import sg_time_log_utils as stlg


url = 'https://docs.google.com/spreadsheets/d/1tiIHnjQXmqbhnJpvjzHa7moWrcw1lmCr7tzHgEONbSk/edit#gid=2043799348'


def run():
    """ headers -> [asset, shot]
    column -> name 
    """ 

    client, service, sheet = init_service()
    worksheets = gs.list_worksheet(sheet)
    year = int(datetime.today().strftime('%Y'))
    month = int(datetime.today().strftime('%m'))
    wsname = '{}-Asset'.format(datetime.today().strftime('%b'))

    if not wsname in worksheets:
        worksheet = gs.new_worksheet(sheet, wsname, 100, 100)
    else:
        worksheet = gs.select_worksheetByTitle(sheet, wsname)

    # data = sum_utils.generate_date(year, month)
    logs = stlg.fetch_logs(start_date=(year, month, 1), department='')
    userdict = stlg.list_all_users()
    userdict = dict()
    headers = list()

    for log in logs: 
        user = log['user']['name']
        entity = log['entity.Task.entity']['name']
        duration = log['duration']
        entity_type = log['entity.Task.entity']['type']

        if entity_type == 'Asset': 

            if not entity in headers: 
                headers.append(entity)

            if not user in userdict.keys(): 
                userdict[user] = {entity: {'value': duration}}
            else: 
                if not entity in userdict[user].keys(): 
                    userdict[user][entity] = {'value': duration}
                else: 
                    userdict[user][entity]['value']+=duration

    cell_data = sum_utils.dict_to_cell(headers, data=userdict)
    sum_utils.write_data(service, sheet, wsname, cell_data)


def init_service(): 
    from rf_utils.gsheet import googlesheet as gs
    client = gs.createAccount(gs.Setting.keyfile, gs.Scope.sheet_full)
    sheet = gs.openByURL(client, url)
    service = gs.buildService(gs.Setting.keyfile, gs.Scope.sheet_full)
    return client, service, sheet

if __name__ == '__main__':
    run()
    # export(project, start='2021-W1', end='', logged_type='Automatic')
