import os
import sys
from datetime import datetime

core = '{}/core'.format(os.environ['RFSCRIPT'])
sys.path.append(core) if not core in sys.path else None
import rf_config
from rf_utils.gsheet import googlesheet as gs
from rf_app.time_logged.sum import config
from rf_app.time_logged.sum import sum_utils


def run(): 
    main()


def main():
    client, service, sheet = config.init_gsheet()
    worksheets = gs.list_worksheet(sheet)
    year = int(datetime.today().strftime('%Y'))
    month = int(datetime.today().strftime('%m'))
    wsname = 'Project-{}'.format(datetime.today().strftime('%b'))

    # data = sum_utils.generate_date(year, month)
    data = sum_utils.hr_summary(year, month)

    sumlist = get_sum(data[0], data[1:])
    sumdata = [data[0], sumlist]
    sumdata.extend(data[1:])

    if not wsname in worksheets:
        worksheet = gs.new_worksheet(sheet, wsname, 100, 100)
    else:
        worksheet = gs.select_worksheetByTitle(sheet, wsname)

    sum_utils.clear_cell(service, sheet, worksheet)
    sum_utils.write_data(service, sheet, wsname, sumdata)
    sum_utils.resize_cell(service, sheet, worksheet, 1, 20, 100)


def get_sum(projects, userlist): 
    start = len(sum_utils.Header.fix_headers)
    sumlist = list()
    sumlist.extend(['' for a in range(start)])

    for i in range(start, len(projects)): 
        project_sum = 0 
        for user in userlist: 
            project_sum += user[i]
        sumlist.append(str(project_sum))
    return sumlist




if __name__ == '__main__':
    main()
    # export(project, start='2021-W1', end='', logged_type='Automatic')
