from openpyxl import Workbook
from openpyxl.styles import Alignment, colors, fills, Font, Border, Side
from openpyxl.utils import get_column_letter
from openpyxl.chart import BarChart, Series, Reference
from openpyxl.chart.layout import Layout, ManualLayout
from openpyxl.comments import Comment

from datetime import datetime
import calendar

class Config_STYLES:
    Fill = {'month': [fills.PatternFill(patternType='solid', fgColor=colors.Color(rgb='fce5cd')),
                      fills.PatternFill(patternType='solid', fgColor=colors.Color(rgb='ead1dc')), 
                      fills.PatternFill(patternType='solid', fgColor=colors.Color(rgb='fff2cc'))],

            'week_workday': fills.PatternFill(patternType='solid', fgColor=colors.Color(rgb='333f4f')),
            'week_holiday': fills.PatternFill(patternType='solid', fgColor=colors.Color(rgb='bfbfbf')),
            'year_holiday': fills.PatternFill(patternType='solid', fgColor=colors.Color(rgb='505050')),
            'white': fills.PatternFill(patternType='solid', fgColor=colors.Color(rgb='ffffff')),
            'graywhite': fills.PatternFill(patternType='solid', fgColor=colors.Color(rgb='f2f2f2')),
            'grayInActive': fills.PatternFill(patternType='solid', fgColor=colors.Color(rgb='C2CBCE')),
            'gray': fills.PatternFill(patternType='solid', fgColor=colors.Color(rgb='415063')),
            'green': fills.PatternFill(patternType='solid', fgColor=colors.Color(rgb='3B8686')),
            'yellow': fills.PatternFill(patternType='solid', fgColor=colors.Color(rgb='FBEEC2')),
            'department': fills.PatternFill(patternType='solid', fgColor=colors.Color(rgb='ebdef0')),

            'leave':{'sick': fills.PatternFill(patternType='solid', fgColor=colors.Color(rgb='ff96ff')),
                     'personal': fills.PatternFill(patternType='solid', fgColor=colors.Color(rgb='ffa143')),
                     'vacation': fills.PatternFill(patternType='solid', fgColor=colors.Color(rgb='00C459')),
                     'absent': fills.PatternFill(patternType='solid', fgColor=colors.Color(rgb='e93c6a')),
                     'special': fills.PatternFill(patternType='solid', fgColor=colors.Color(rgb='b1f4f8')),
                     'None': fills.PatternFill(patternType='solid', fgColor=colors.Color(rgb='E93C6A'))},

            'sum':{'work': fills.PatternFill(patternType='solid', fgColor=colors.Color(rgb='333f4f')),
                   'absents': fills.PatternFill(patternType='solid', fgColor=colors.Color(rgb='e93c6a')),
                   'leave': fills.PatternFill(patternType='solid', fgColor=colors.Color(rgb='4f81bd')),
                   'holiday': fills.PatternFill(patternType='solid', fgColor=colors.Color(rgb='505050')),
                   'total': fills.PatternFill(patternType='solid', fgColor=colors.Color(rgb='333f4f'))},

            'none': fills.PatternFill(fill_type=None)}

    Font = {'white': Font(color=colors.Color(rgb='ffffff')),
            'whiteBold': Font(color=colors.Color(rgb='ffffff'),bold=True),
            'black': Font(color=colors.Color(rgb='000000')),
            'blackBold': Font(color=colors.Color(rgb='000000'),bold=True),
            'department': Font(color=colors.Color(rgb='763e8d'), size=20),
            'projectsummary': Font(name='Century Gothic', size=22, bold=True, color=colors.Color(rgb='333f4f')),
            'size': Font(size=9),
            'none' :Font(color=None)}

    Border = {'white': Border(left=Side(border_style='thin',color=colors.Color(rgb='ffffff')),
                            right=Side(border_style='thin',color=colors.Color(rgb='ffffff')),
                            top=Side(border_style='thin',color=colors.Color(rgb='ffffff')),
                            bottom=Side(border_style='thin',color=colors.Color(rgb='ffffff'))),
              'red': Border(left=Side(border_style='thick',color=colors.Color(rgb='E93C6A')),
                            right=Side(border_style='thick',color=colors.Color(rgb='E93C6A')),
                            top=Side(border_style='thick',color=colors.Color(rgb='E93C6A')),
                            bottom=Side(border_style='thick',color=colors.Color(rgb='E93C6A'))),
              'gray': Border(left=Side(border_style='thin',color=colors.Color(rgb='808080')),
                            right=Side(border_style='thin',color=colors.Color(rgb='808080')),
                            top=Side(border_style='thin',color=colors.Color(rgb='808080')),
                            bottom=Side(border_style='thin',color=colors.Color(rgb='808080'))),
              'blue': Border(left=Side(border_style='thin',color=colors.Color(rgb='333f4f')),
                            right=Side(border_style='thin',color=colors.Color(rgb='333f4f')),
                            top=Side(border_style='thin',color=colors.Color(rgb='333f4f')),
                            bottom=Side(border_style='thin',color=colors.Color(rgb='333f4f'))),

              'none': Border(left=Side(border_style=None),
                            right=Side(border_style=None),
                            top=Side(border_style=None),
                            bottom=Side(border_style=None))}

    Alignment = {'center': Alignment(horizontal='center', vertical='center'),
                 'center-wrap': Alignment(horizontal='center', vertical='center',wrapText=True)}

    Format = {'0':'0', '0.0':'0.0', '0.00':'0.00'}

    #!/usr/bin/python3
    # -*- coding: utf-8 -*-
    def Comment(text1=None, value1=None, text2=None, value2=None):
        t1 = 'ใส่เวลาทำงานเกิน'
        t2 = 'ใส่เวลาทำงานไม่ครบ'
        if value1 and value2:
            textsum = t1 if value1 + value2 > 8 else t2
            text = '%s = %s \n%s = %s\n%s'%(text1, value1, text2, value2, textsum)
        else:
            textsum = t1 if value1 > 8 else t2
            text = '%s = %s\n%s'%(text1, value1, textsum)
        return Comment(text, "Author")

Hour = 8
Hrs = 60
HrsDAY = Hour * Hrs
FIRSTDAY = 1
FIXED = 1
size_comment = 200

class Excel:
    def __init__(self):
        print ('create Excel')

    def create_workbook(self):
        wb = Workbook()
        return wb

    def create_sheet(self, workbook, name, num):
        ws = workbook.create_sheet(name, num)
        ws.title = name
        return ws

    def save(self, workbook, path):
        workbook.save(path)

class Calendar(Excel):
    def __init__(self):
        print ('Time Log Summary')

        self.title_POS = dict()
        self.month_POS = dict()
        self.user_POS = dict()
        self.end_POS = dict()
        self.project_COLOR = dict()
        self.project_POS = dict()
        self.chart_POS = dict()

    def generate_header(self, worksheet='', user='', date=''):
        worksheet['A1'].value = '%s%s'%('create: ', user)
        worksheet['A2'].value = '%s%s'%('date: ', date)

    def set_title(self, worksheet='', title=[], start_col_num=1, start_row_num=1, start_merge_row=1, end_merge_row=3, freezePanes='C4'):
        worksheet.freeze_panes = worksheet[freezePanes]

        for text in title:
            cell = worksheet.cell(column=start_col_num, row=start_row_num)
            cell.value = text
            cell.alignment = Config_STYLES.Alignment['center']
            cell.fill = Config_STYLES.Fill['week_workday']
            cell.font = Config_STYLES.Font['whiteBold']
            cell.border = Config_STYLES.Border['white']

            col = get_column_letter(start_col_num)
            if text == 'ID':
                worksheet.column_dimensions[col].width = 12
            elif text == 'NAME':
                worksheet.column_dimensions[col].width = 34
            else:
                worksheet.column_dimensions[col].width = 5
                cell.alignment = Alignment(textRotation=180, horizontal='center', vertical='center')
                cell.font = Config_STYLES.Font['white']

            self.title_POS[text.lower()] = start_col_num
            worksheet.merge_cells(start_row=start_merge_row, start_column=start_col_num, end_row=end_merge_row, end_column=start_col_num)
            start_col_num += 1

    def generate_user(self, worksheet='', departments='', users='', create='', leave=None, data_month=None, start_col_num=1, start_row_num=4, end_col_num=2, end_row_num=4):
        self.user_POS = dict()
        for step in departments:
            worksheet.merge_cells(start_row=start_row_num, start_column=start_col_num, end_row=start_row_num, end_column=end_col_num)
            cell = worksheet.cell(column=start_col_num, row=start_row_num)

            cell.value = step
            cell.alignment = Config_STYLES.Alignment['center']
            cell.font = Config_STYLES.Font['blackBold']
            cell.font = Config_STYLES.Font['department']
            cell.fill = Config_STYLES.Fill['department']

            for col_num in range(end_col_num, worksheet.max_column + FIXED):
                cell = worksheet.cell(column=col_num, row=start_row_num)
                cell.fill = Config_STYLES.Fill['department']

            start_row_num += 1

            step_list = []
            for user in users[step]:
                if step != 'None':
                    user_id = users[step][user]['sg_employee_id']
                    user_name = users[step][user]['sg_ad_account']
                    user_nickname = users[step][user]['sg_name']
                    user_active = users[step][user]['sg_active']
                    user_id = user_id if user_id else ''

                    if user_active and not data_month:
                        step_list.append((user_id, user_name, user_nickname))

                    elif data_month:
                        if user in data_month.keys():
                            step_list.append((user_id, user_name, user_nickname))

                        elif leave:
                            if user in leave.keys():
                                step_list.append((user_id, user_name, user_nickname))

            for id_uer, user, nickname in sorted(step_list):
                start_cell = worksheet.cell(column=start_col_num, row=start_row_num)
                end_cell = worksheet.cell(column=end_col_num, row=start_row_num)

                start_cell.value = id_uer
                start_cell.alignment = Config_STYLES.Alignment['center']
                end_cell.value = '%s  (%s)'%(user, nickname)

                create_at = create[nickname]['created_at'] if nickname in create.keys() else None

                self.user_POS[nickname] = {'pos':start_row_num, 'create_at':create_at}
                start_row_num += 1


    def generate_month(self, worksheet='', year=2021, start_month=1, end_month=12, start_col_num=3, start_row_num=1):
        weekDaysMapping = ("Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday")
        week_row, day_row = 2, 3
        start_col = start_col_num

        for month in range(start_month, end_month + FIXED):
            weekday, numdays = calendar.monthrange(year, month)
            end_col_num = start_col + (numdays - FIXED)

            month_cell = worksheet.cell(row=start_row_num, column=start_col)
            month_cell.value = calendar.month_name[month]
            month_cell.alignment = Config_STYLES.Alignment['center']
            month_cell.fill = Config_STYLES.Fill['month'][month%3]
            worksheet.merge_cells(start_row=start_row_num, start_column=start_col, end_row=start_row_num, end_column=end_col_num)

            self.month_POS[month] = start_col
            start_col = end_col_num +1

            for day in range(FIRSTDAY, numdays + FIXED):
                dt = datetime(year, month, day)
                weekday = weekDaysMapping[dt.weekday()]
                weekday = weekday[0] if dt.weekday() not in (5, 6) else ''

                week_cell = worksheet.cell(row=week_row, column=start_col_num)
                week_cell.value = weekday
                week_cell.fill = Config_STYLES.Fill['week_holiday']
                week_cell.font = Config_STYLES.Font['black']
                week_cell.border = Config_STYLES.Border['white']
                week_cell.alignment = Config_STYLES.Alignment['center']

                day_cell = worksheet.cell(row=day_row, column=start_col_num)
                day_cell.value = day
                day_cell.fill = Config_STYLES.Fill['week_workday']
                day_cell.font = Config_STYLES.Font['white']
                day_cell.border = Config_STYLES.Border['white']
                day_cell.alignment = Config_STYLES.Alignment['center']

                worksheet.column_dimensions[get_column_letter(start_col_num)].width = 3
                start_col_num += 1

    def generate_hoilday(self, worksheet='', timeLogs='', first_month='', last_month=''):
        for user in self.user_POS:
            row = self.user_POS[user]['pos']
            for month in range(first_month, last_month + FIXED):
                for day in timeLogs[user][month]:

                    col = self.month_POS[month] + (day - FIXED)
                    cell = worksheet.cell(row=row, column=col)

                    if 'weekend' in timeLogs[user][month][day].keys():
                        cell.fill = Config_STYLES.Fill['week_holiday']
                        cell.border = Config_STYLES.Border['white']

                    elif 'holiday' in timeLogs[user][month][day].keys():
                        cell.fill = Config_STYLES.Fill['year_holiday']
                        cell.border = Config_STYLES.Border['white']
                        cell.font = Config_STYLES.Font['white']


    def generate_workday(self, worksheet='', timeLogs='', first_month='', last_month='', typeLog=''):
        for user in self.user_POS:
            row = self.user_POS[user]['pos']
            leave_dict = {'sick':0, 'personal':0, 'vacation':0, 'special':0, 'absent':0, 'total':0}

            for month in range(first_month, last_month + FIXED):
                for day in timeLogs[user][month]:

                    col = self.month_POS[month] + (day - FIXED)
                    cell = worksheet.cell(row=row, column=col)
                    cell.alignment = Config_STYLES.Alignment['center']
                    duration = 0

                    if typeLog == 'work' and typeLog in timeLogs[user][month][day].keys():
                        duration = timeLogs[user][month][day][typeLog]
                        duration = duration/Hrs

                    elif typeLog == 'leave' and typeLog in timeLogs[user][month][day].keys():
                        leaveLogs = timeLogs[user][month][day][typeLog]
                        if len(leaveLogs) > 1:
                            textList = []
                            for idName in leaveLogs:
                                leave_dict[idName] += leaveLogs[idName]/Hrs/Hour
                                duration += leaveLogs[idName]/Hrs
                                cell.fill = Config_STYLES.Fill[typeLog]['None']
                                text = '%s = %s'%(idName, leaveLogs[idName]/Hrs)
                                textList.append(text)

                            text = '%s\n%s'%(textList[0], textList[1])
                            cell.comment = Comment(text, "Author")

                        else:
                            for idName in leaveLogs:
                                leave_dict[idName] += leaveLogs[idName]/Hrs/Hour
                                duration += leaveLogs[idName]/Hrs
                                cell.fill = Config_STYLES.Fill[typeLog][idName]

                    cell.value = duration if duration != 0 else None

            data = {'total':leave_dict}
            self.user_POS[user].update(data)

    def generate_summary(self, worksheet='', timeLogs='', first_month='', last_month='', year=''):
        for user in self.user_POS:
            row = self.user_POS[user]['pos']
            dt = self.user_POS[user]['create_at']
            usercreate = datetime(*dt).date()
            summary_dict = {'work':0, 'absents':0, 'leave':0, 'holiday':0}

            for month in range(first_month, last_month + FIXED):
                weekday, numdays = calendar.monthrange(year, month)
                today = datetime.today()
                status_today = False
                status_create = False
                first_day = FIRSTDAY

                if year == today.year:
                    status_today = True if month <= today.month else False
                    numdays = today.day - FIXED if month == today.month else numdays
                elif year <= today.year:
                    status_today = True

                status_create = True if year > usercreate.year else False

                if year == usercreate.year:
                    if month == usercreate.month:
                        first_day = usercreate.day
                        status_create = True
                    elif month > usercreate.month:
                        status_create = True
                    else:
                        status_create = False

                if status_today and status_create:
                    for day in range(first_day, numdays + FIXED):
                        col = self.month_POS[month] + (day - FIXED)
                        cell = worksheet.cell(row=row, column=col)
                        cell.alignment = Config_STYLES.Alignment['center']

                        logs = timeLogs[user][month][day]
                        work_dur = logs['work']/Hrs if 'work' in logs.keys() else None
                        weekend_dur = logs['weekend']/Hrs if 'weekend' in logs.keys() else None
                        holiday_dur = logs['holiday']/Hrs if 'holiday' in logs.keys() else None
                        leave_dur = sum(logs['leave'][idName] for idName in logs['leave'])/Hrs if 'leave' in logs.keys() else None

                        if leave_dur:
                            leave_type = [idName for idName in timeLogs[user][month][day]['leave']][0]
                            text1, text2, num1, num2, num3 = self.check_time_leave(leave_duration=leave_dur, work_duration=work_dur, leave_type=leave_type)

                            if text1 and text2:
                                cell.fill = Config_STYLES.Fill['none']
                                cell.border = Config_STYLES.Border['red']
                                cell.comment = Config_STYLES.Comment(text1=text1, value1=leave_dur, text2=text2, value2=work_dur)
                                cell.comment.width = size_comment

                            cell.value = Hour
                            summary_dict['leave'] += num1 if num1 else 0
                            summary_dict['work'] += num2 if num2 else 0
                            summary_dict['absents'] += num3 if num3 else 0

                        elif weekend_dur or holiday_dur:
                            text_work = 'Work'
                            text_type = 'WeekEnd' if weekend_dur else 'Holiday'
                            duration = weekend_dur if weekend_dur else holiday_dur

                            if work_dur:
                                cell.font = Config_STYLES.Font['black']
                                cell.fill = Config_STYLES.Fill['none']
                                cell.border = Config_STYLES.Border['red']
                                cell.comment = Config_STYLES.Comment(text1=text_type, value1=duration, text2=text_work, value2=work_dur)
                                cell.comment.width = size_comment

                            cell.value = Hour
                            summary_dict['holiday'] += FIXED

                        elif work_dur:
                            if work_dur != Hour:
                                text = 'Work'
                                cell.fill = Config_STYLES.Fill['none']
                                cell.border = Config_STYLES.Border['red']
                                cell.comment = Config_STYLES.Comment(text1=text, value1=work_dur)
                                cell.comment.width = size_comment

                            cell.value = Hour
                            summary_dict['work'] += FIXED

                        else:
                            cell.fill = Config_STYLES.Fill['leave']['None']
                            cell.border = Config_STYLES.Border['white']
                            summary_dict['absents'] += FIXED

            data = {'total':summary_dict}
            self.user_POS[user].update(data)

    def check_time_leave(self, leave_duration='', work_duration='', leave_type=''):
        leave_text, work_text = None, None
        leave_num, work_num ,absents_num = 0, 0, 0
        HALF_DAY = 0.5
        DAY = 1

        if leave_duration == Hour:
            if work_duration == 0:
                leave_num = DAY

            elif work_duration != 0:
                leave_text ='Leave(%s)'%(leave_type)
                work_text = 'Work'
                leave_num = DAY

        elif leave_duration == 4:
            if leave_duration + work_duration == Hour:
                leave_num = HALF_DAY
                work_num = HALF_DAY

            elif leave_duration + work_duration != Hour:
                leave_text ='Leave(%s)'%(leave_type)
                work_text = 'Work'
                leave_num = HALF_DAY
                work_num = HALF_DAY

            elif not work_duration :
                leave_text ='Leave(%s)'%(leave_type)
                work_text = 'Work'
                leave_num = HALF_DAY
                absents_num = HALF_DAY

        return (leave_text, work_text, leave_num, work_num, absents_num)

    def generate_title_summary(self, worksheet=''):
        for user in self.user_POS:
            summary = 0
            row = self.user_POS[user]['pos']

            for title in self.title_POS:
                if title.lower() in self.user_POS[user]['total'].keys():
                    total_type = self.user_POS[user]['total'][title.lower()]

                    col = self.title_POS[title.lower()]
                    cell = worksheet.cell(row=row, column=col)
                    cell.value = total_type if total_type != 0 else ''
                    cell.alignment = Config_STYLES.Alignment['center']

                    if title in Config_STYLES.Fill['sum'].keys():
                        cell.value = total_type
                        cell.fill = Config_STYLES.Fill['sum'][title.lower()]
                        cell.border = Config_STYLES.Border['white']
                        cell.font = Config_STYLES.Font['white']

                    summary += total_type

            if 'total' in self.title_POS.keys():
                col = self.title_POS['total']
                cell = worksheet.cell(row=row, column=col)
                cell.value = summary
                cell.alignment = Config_STYLES.Alignment['center']
                cell.fill = Config_STYLES.Fill['sum']['total']
                cell.border = Config_STYLES.Border['white']
                cell.font = Config_STYLES.Font['white']

    def generate_head_project(self, worksheet='', projectLogs='', color_project='', start_row_num=1, start_col_num=3, start_merge_row=1, end_merge_row=3):
        for project in sorted(projectLogs):
            cell = worksheet.cell(row=start_row_num, column=start_col_num)

            R, G, B = color_project[project]['color'].split(',')
            color = '%02x%02x%02x' % (int(R),int(G),int(B))

            cell.value = project
            cell.fill = fills.PatternFill(patternType='solid', fgColor=colors.Color(rgb=color))
            cell.alignment = Config_STYLES.Alignment['center-wrap']
            cell.font = Config_STYLES.Font['whiteBold']
            cell.border = Config_STYLES.Border['blue']
            worksheet.column_dimensions[get_column_letter(start_col_num)].width = 12.5
            worksheet.merge_cells(start_row=start_merge_row, start_column=start_col_num, end_row=end_merge_row, end_column=start_col_num)
            
            month = worksheet.title
            self.project_COLOR[project] = color

            if not month in self.project_POS.keys():
                self.project_POS[month] = {project:{'pos':start_col_num}}
            else:
                if not project in self.project_POS[month].keys():
                    self.project_POS[month][project] = {'pos':start_col_num}
                else:
                    if not 'pos' in self.project_POS[month][project].keys():
                        self.project_POS[month][project]['pos'] = start_col_num
                    else:
                        self.project_POS[month][project]['pos'] = start_col_num

            start_col_num += 1

    def set_end_title_project(self, worksheet='', month='', end_title='', start_row_num=1, start_merge_row=1, end_merge_row=3):
        start_col_num = worksheet.max_column + 1
        for title in end_title:
            cell = worksheet.cell(row=start_row_num, column=start_col_num)
            cell.value = title
            cell.fill = Config_STYLES.Fill['week_workday']
            cell.alignment = Config_STYLES.Alignment['center']
            cell.font = Config_STYLES.Font['whiteBold']
            cell.border = Config_STYLES.Border['white']
            worksheet.column_dimensions[get_column_letter(start_col_num)].width = 12.5
            worksheet.merge_cells(start_row=start_merge_row, start_column=start_col_num, end_row=end_merge_row, end_column=start_col_num)

            if not month in self.end_POS.keys():
                self.end_POS[month] = {title:start_col_num}
            else:
                if not title in self.end_POS[month].keys():
                    self.end_POS[month][title] = start_col_num
                else:
                    self.end_POS[month][title] = start_col_num

            start_col_num += 1

    def generate_data_project(self, worksheet='', data=[], start_col_num=3, start_row_num=5):
        for user in sorted(data):
            for name in sorted(data[user]):
                if user in self.user_POS.keys() and name in self.project_POS[worksheet.title].keys():
                    row = self.user_POS[user]['pos']
                    col = self.project_POS[worksheet.title][name]['pos']
                    cell = worksheet.cell(row=row, column=col)
                    cell.value = round(data[user][name]/Hrs, 2)
                    cell.alignment = Config_STYLES.Alignment['center']

    def set_color_project_inactive(self, worksheet='', project_status='', year=''):
        row_max = worksheet.max_row
        month_name = worksheet.title
        datetime_object = datetime.strptime(month_name, "%b")
        month_number = datetime_object.month

        for project in self.project_POS[worksheet.title]:
            start_col_num = self.project_POS[worksheet.title][project]['pos']
            status = project_status[project]['status']
            start_date = project_status[project]['start_date']
            end_date = project_status[project]['end_date']
            status_color = False

            if status != 'Active':
                if start_date != None and start_date != None:
                    status_color = True if year > end_date.year else False

                    if year == end_date.year:
                        if month_number > end_date.month:
                            status_color = True

                else:
                    status_color = True

            if status_color:
                for row in range(row_max):
                    cell_color = worksheet.cell(row=row + FIXED, column=start_col_num)
                    if cell_color.fill != Config_STYLES.Fill['department']:
                        cell_color.fill = Config_STYLES.Fill['grayInActive']
                        cell_color.font = Config_STYLES.Font['black']
                        cell_color.border = Config_STYLES.Border['white']


    def generate_sum_project(self, worksheet='', start_col_num=3, start_row_num=5):
        row_max = worksheet.max_row + FIXED
        col_max = worksheet.max_column
        cell = worksheet.cell(row=row_max, column=1)
        cell.value = "TOTAL"
        cell.alignment = Config_STYLES.Alignment['center']
        cell.font = Config_STYLES.Font['whiteBold']
        cell.fill = Config_STYLES.Fill['week_workday']
        cell.border = Config_STYLES.Border['white']

        worksheet.row_dimensions[row_max].height = 30
        worksheet.merge_cells(start_row=row_max, start_column=1, end_row=row_max, end_column=2)

        for col in range(start_col_num, col_max + FIXED):
            start_col = get_column_letter(col)
            cellSum = worksheet.cell(row=row_max, column=col)
            cellSum.value = "=SUM({}:{})".format(str(f'{start_col}{start_row_num}'),str(f'{start_col}{row_max-FIXED}'))

            cellSum.alignment = Config_STYLES.Alignment['center']
            cellSum.number_format = Config_STYLES.Format['0.00']
            cellSum.fill = Config_STYLES.Fill['week_workday']
            cellSum.border = Config_STYLES.Border['white']
            cellSum.font = Config_STYLES.Font['white']

            project = worksheet[f'{start_col}1'].value
            month = worksheet.title
            if project in self.project_POS[month].keys():
                self.project_POS[month][project] = {'cell':f'{start_col}{row_max}'}

    def generate_sum_title_project(self, worksheet='', timeLogs='', month='', year=2021):
        for user in self.user_POS:
            summary = 0
            for title in self.end_POS[month]:
                col = self.end_POS[month][title]
                row = self.user_POS[user]['pos']
            
                cell = worksheet.cell(row=row, column=col)
                cell.alignment = Config_STYLES.Alignment['center']
                cell.border = Config_STYLES.Border['gray']
                cell.fill = Config_STYLES.Fill['gray'] if title.lower() != 'working hrs' else Config_STYLES.Fill['week_workday']
                cell.font = Config_STYLES.Font['white']

                dt = self.user_POS[user]['create_at']
                usercreate = datetime(*dt).date()
                weekday, numdays = calendar.monthrange(year, month)
                today = datetime.today()
                duration = 0
                status_today = False
                status_create = False
                first_day = FIRSTDAY

                if year == today.year:
                    status_today = True if month <= today.month else False
                    numdays = today.day - FIXED if month == today.month else numdays
                elif year <= today.year:
                    status_today = True

                status_create = True if year > usercreate.year else False

                if year == usercreate.year:
                    if month == usercreate.month:
                        first_day = usercreate.day
                        status_create = True
                    elif month > usercreate.month:
                        status_create = True
                    else:
                        status_create = False

                if status_today and status_create:
                    for day in range(first_day, numdays + FIXED):
                        log = timeLogs[user][month][day]

                        if title.lower() == 'absents':
                            if 'leave' in log:
                                durLeave = 0
                                for idName in log['leave']:
                                    durLeave += log['leave'][idName]

                                if durLeave == HrsDAY/2 and log['work'] < HrsDAY/2:
                                    duration += HrsDAY - (log['work'] + HrsDAY/2)

                            elif not 'holiday' in log and not 'weekend' in log:
                                duration += HrsDAY - log['work'] if log['work'] < HrsDAY else 0

                        elif title.lower() == 'leave':
                            if 'leave' in log:
                                duration += sum(log['leave'][idName] for idName in log['leave'])

                        elif title.lower() == 'work':
                            duration += timeLogs[user][month][day]['work']

                        elif title.lower() == 'working hrs':
                            if not 'holiday' in log and not 'weekend' in log:
                                duration += HrsDAY

                    summary += duration if title.lower() != 'working hrs' else 0
                    cell.value = round(duration/Hrs, 2) if duration else ''

            if 'Total' in self.end_POS[month].keys():
                col = self.end_POS[month]['Total']
                row = self.user_POS[user]['pos']
                cell = worksheet.cell(row=row, column=col)
                cell.value = int(summary/Hrs) if summary else ''
                cell.alignment = Config_STYLES.Alignment['center']

    def chart_project(self, worksheet='', sheetList=[], start_col_num=2, start_row_num=6):
        self.project_list = []
        for sheet in sheetList:
            for project in self.project_POS[sheet.title]:
                if not project in self.project_list:
                    self.project_list.append(project)

        for project in sorted(self.project_list):
            project_cell = worksheet.cell(row=start_row_num, column=start_col_num)
            project_cell.value = project
            project_cell.fill = Config_STYLES.Fill['graywhite']
            worksheet.row_dimensions[start_row_num].height = 20
            self.chart_POS[project] = start_row_num
            self.project_list.append(project)
            start_row_num += 1

    def chart_month(self, worksheet='', months=13, start_col_num=3, start_row_num=5):
        worksheet.column_dimensions['A'].width = 3
        worksheet.column_dimensions['B'].width = 20

        for month in range(1, months):
            cell = worksheet.cell(row=start_row_num, column=start_col_num)
            cell.value = calendar.month_abbr[month]
            cell.alignment = Config_STYLES.Alignment['center']
            cell.font = Config_STYLES.Font['white']
            cell.fill = Config_STYLES.Fill['week_workday']
            cell.border = Config_STYLES.Border['white']

            for row in range(start_row_num+1, worksheet.max_row+1):
                row_cell = worksheet.cell(row=row, column=start_col_num)
                if month%2 == 0:
                    row_cell.fill = Config_STYLES.Fill['graywhite']
                else:
                    row_cell.fill = Config_STYLES.Fill['white']

            worksheet.column_dimensions[get_column_letter(start_col_num)].width = 13
            self.chart_POS[calendar.month_abbr[month]] = start_col_num
            start_col_num += 1

    def chart_data_project(self, worksheet='', sheetList=[], projectdict=''):
        for sheet in sheetList:

            month_number = datetime.strptime(sheet.title, "%b")
            max_col = len(projectdict[month_number.month]) + 2

            sheet_start_col = get_column_letter(3)
            sheet_end_col = get_column_letter(max_col)
            start_cell = '%s!%s'%(sheet.title, f'{sheet_start_col}{sheet.max_row}')
            end_cell = '%s!%s'%(sheet.title, f'{sheet_end_col}{sheet.max_row}')

            for project in self.project_list:
                cell = worksheet.cell(row=self.chart_POS[project], column=self.chart_POS[sheet.title])
                cell.number_format = Config_STYLES.Format['0.00']
                cell.alignment = Config_STYLES.Alignment['center']

                month = sheet.title
                if project in self.project_POS[month].keys():
                    cell.value = ('=%s!%s/sum(%s:%s)*100'%(sheet.title, self.project_POS[month][project]['cell'], start_cell, end_cell))
                else:
                    cell.value = 0

    def chart_allYaer(self, worksheet='', start_col_num=1, start_row_num=3):
        worksheet['B2'] = worksheet.title
        worksheet['B2'].font = Config_STYLES.Font['projectsummary']
        worksheet.row_dimensions[2].height = 30
        worksheet.row_dimensions[start_row_num].height = 270

        chart = BarChart()
        chart.type = "col"
        chart.style = 10
        chart.height = 9 # default is 7.5
        chart.width = 35 # default is 15

        chart.layout = Layout(manualLayout=ManualLayout(x=0, y=0, h=0.9, w=0.85))
        data = Reference(worksheet, min_col=2, min_row=6, max_col=14, max_row=worksheet.max_row)
        cats = Reference(worksheet, min_col=3, min_row=5, max_col=14)
        chart.add_data(data, titles_from_data=True, from_rows=True)
        chart.set_categories(cats)

        for i, s in enumerate(chart.series):
            projectName = self.project_list[i]
            if projectName in self.project_COLOR.keys():
                #s.graphicalProperties.line.solidFill = self.color_proj[projectName]
                s.graphicalProperties.solidFill = self.project_COLOR[projectName]

        worksheet.add_chart(chart, "B3")

    def chart_status_project(self, worksheet='', project_status='', start_col_num='', start_row_num=5):
        start_col_num = worksheet.max_column + 1
        title = ['status', 'start_date', 'end_date']
        proj_status = dict()

        for t in title:
            cell_tilte = worksheet.cell(row=start_row_num, column=start_col_num)
            cell_tilte.value = t
            cell_tilte.font = Config_STYLES.Font['white']
            cell_tilte.fill = Config_STYLES.Fill['week_workday']
            cell_tilte.alignment = Config_STYLES.Alignment['center']

            if t not in proj_status.keys():
                proj_status[t] = start_col_num

            worksheet.column_dimensions[get_column_letter(start_col_num)].width = 13
            start_col_num += 1

        for project in self.project_list:

            for proj in proj_status:
                col_num = proj_status[proj]
                value = ''
                
                if proj == 'status':
                    value = project_status[project][proj]
                else:
                    value = str(project_status[project][proj]).split(" ")[0] if project_status[project][proj] else ''

                cell = worksheet.cell(row=self.chart_POS[project], column=col_num)
                cell.value = value
                cell.alignment = Config_STYLES.Alignment['center']

                if proj == 'status':
                    if project_status[project]['status'] == 'Finish':
                        cell.fill = Config_STYLES.Fill['green']

                    elif project_status[project]['status'] == 'Active':
                        cell.fill = Config_STYLES.Fill['yellow']

                    else:
                        cell.fill = Config_STYLES.Fill['white']