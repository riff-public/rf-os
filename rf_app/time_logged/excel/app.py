from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import os
import time
import subprocess
sys.path.append('{}/core'.format(os.environ['RFSCRIPT']))

import calendar
from datetime import datetime

from rf_app.time_logged.excel import excel_summary
from rf_app.time_logged.excel import utils
from rf_app.time_logged.excel import config
from rf_utils import user_info

class config_style:
    img_app = '%s/core/rf_launcher_apps/Tool/Time Log Summary/TimelogSum.png' % os.environ.get('RFSCRIPT')
    img_icon = '%s/core/icons/asmRefresh.png' % os.environ.get('RFSCRIPT')
    colorRGB = 'color: rgb(0 ,0 ,0);'
    bg_green = 'background-color: rgb(16, 90, 57);'
    bg_greenWhite = 'background-color: rgba(16, 90, 57, 0.8);'
    bg_white = 'background-color: rgb(240, 240, 240);'
    bg_gray = 'background-color: rgb(200, 200, 200);'
    color_white = 'color: rgb(255 ,255 ,255);'
    color_black = 'color: rgb(0 ,0 ,0);'

class App(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super(App, self).__init__(parent=parent)

        self.ui()
        self.setup_ui()
        self.signal()
        self.status_fetch = False

    def check_permission(self):
        name = None
        dep = None
        user = user_info.User()
        if user.is_producer() or user.is_hr() or user.is_admin():
            self.switch_style(status=True)

        elif user.is_artist() or user.is_supervisor():
            self.switch_style(status=True)
            
            department = user.department()
            for num, room in enumerate(config.room):
                if department['name'] in config.room[room] and room != 'All':
                    self.room_departments.setItemData(num, 33, QtCore.Qt.UserRole - 1)
                    self.room_departments.setCurrentIndex(num)
                    #name = user.name()
                    dep = [department['name']]
                else:
                    self.room_departments.setItemData(num, 0, QtCore.Qt.UserRole - 1)

        else:
            self.switch_style(status=False)

        self.departments = config.room[self.room_departments.currentText()] if not dep else dep
        self.user_dict = utils.list_all_users(name=name)

    def fetch_data(self):
        year = int(self.year.currentText())
        self.timeLogs = utils.summary_logs(departments=config.room['All'], year=year)
        self.projectLogs, self.projectdict, self.project_status = utils.summary_project_logs(year=year)
        self.leaveLogs = utils.summary_leave(year=year)
        #self.user_dict = utils.list_all_users()
        self.check_permission()
        self.usercreate = utils.find_date_create_account()
        self.color_dict = utils.find_color_project()

        date = 'ข้อมูลเมื่อเวลา %s'%(datetime.now().strftime('%H:%M:%S'))
        self.date_lable.setText(date)

    def setup_ui(self):
        self.add_item_room(self.room_departments)
        self.add_item_month()
        self.add_item_year()
        
        today = datetime.today()
        self.first_month.setCurrentText('Jan')
        self.end_month.setCurrentText('Dec')
        self.year.setCurrentText(str(today.year))
        self.year_old =  self.year.currentText()

        self.check_permission()

    def ui(self):
        self.widget = QtWidgets.QWidget()
        self.main = QtWidgets.QVBoxLayout(self.widget)
        self.pageWidget = QtWidgets.QWidget()
        self.main.addWidget(self.pageWidget)

        self.main_layout = QtWidgets.QVBoxLayout(self.pageWidget)
        self.refresh_layout = QtWidgets.QHBoxLayout()
        self.date_lable = QtWidgets.QLabel('ข้อมูลเมื่อเวลา')
        self.icon_button = QtWidgets.QPushButton()
        self.icon_button.setFixedSize(20,20)
        self.icon_button.setIcon(QtGui.QPixmap(config_style.img_icon))

        self.month_layout = QtWidgets.QHBoxLayout()
        self.room_lable = QtWidgets.QLabel('Departments')
        self.room_layout = QtWidgets.QHBoxLayout()
        self.room_departments = QtWidgets.QComboBox()

        self.first_month = QtWidgets.QComboBox()
        self.firstend_lable = QtWidgets.QLabel('-')
        self.end_month = QtWidgets.QComboBox()
        self.year = QtWidgets.QComboBox()

        self.refresh_spacer = QtWidgets.QSpacerItem(80, 5)
        self.refresh_layout.addItem(self.refresh_spacer)
        self.refresh_layout.addWidget(self.date_lable)
        self.refresh_layout.addWidget(self.icon_button)

        self.refresh_layout.setStretch(0, 1)
        self.refresh_layout.setStretch(1, 0)
        self.refresh_layout.setStretch(2, 0)

        self.room_layout.addWidget(self.room_lable)
        self.room_layout.addWidget(self.room_departments)

        self.month_layout.addWidget(self.first_month)
        self.month_layout.addWidget(self.firstend_lable)
        self.month_layout.addWidget(self.end_month)
        self.month_layout.addWidget(self.year)

        self.room_layout.setStretch(0, 0)
        self.room_layout.setStretch(1, 1)

        self.month_layout.setStretch(0, 1)
        self.month_layout.setStretch(1, 0)
        self.month_layout.setStretch(2, 1)
        self.month_layout.setStretch(3, 0)

        self.detal_layout = QtWidgets.QHBoxLayout()
        self.summary_lable = QtWidgets.QLabel('Summary')
        self.summary = QtWidgets.QCheckBox()
        self.rawData_lable = QtWidgets.QLabel('RawData')
        self.rawData = QtWidgets.QCheckBox()
        self.rawLeave_lable = QtWidgets.QLabel('RawLeave')
        self.rawLeave = QtWidgets.QCheckBox()

        self.project_layout = QtWidgets.QHBoxLayout()
        self.chart_lable = QtWidgets.QLabel('Project Summary')
        self.chart = QtWidgets.QCheckBox()
        self.monthly_lable = QtWidgets.QLabel('Monthly')
        self.monthly = QtWidgets.QCheckBox()

        self.spacer = QtWidgets.QSpacerItem(60, 5)

        self.summary.setChecked(True)
        self.rawData.setChecked(True)
        self.rawLeave.setChecked(True)
        self.monthly.setChecked(True)
        self.chart.setChecked(True)
        self.submit_button = QtWidgets.QPushButton('Submit')
        self.submit_button.setEnabled(False)
        self.submit_button.setStyleSheet("QPushButton{%s%s}"%(config_style.bg_gray, config_style.color_black))

        self.widget.setStyleSheet(config_style.bg_green)
        self.pageWidget.setStyleSheet(config_style.bg_white)

        self.summary_lable.setStyleSheet(config_style.colorRGB)
        self.rawData_lable.setStyleSheet(config_style.colorRGB)
        self.rawLeave_lable.setStyleSheet(config_style.colorRGB)
        self.monthly.setStyleSheet(config_style.colorRGB)

        self.detal_layout.addWidget(self.summary_lable)
        self.detal_layout.addWidget(self.summary)
        self.detal_layout.addWidget(self.rawData_lable)
        self.detal_layout.addWidget(self.rawData)
        self.detal_layout.addWidget(self.rawLeave_lable)
        self.detal_layout.addWidget(self.rawLeave)

        self.project_layout.addWidget(self.chart_lable)
        self.project_layout.addWidget(self.chart)
        self.project_layout.addWidget(self.monthly_lable)
        self.project_layout.addWidget(self.monthly)
        self.project_layout.addItem(self.spacer)

        self.main_layout.addLayout(self.refresh_layout)
        self.main_layout.addLayout(self.room_layout)
        self.main_layout.addLayout(self.month_layout)
        self.main_layout.addLayout(self.detal_layout)
        self.main_layout.addLayout(self.project_layout)
        self.main_layout.addWidget(self.submit_button)

        self.setCentralWidget(self.widget)
        self.setMinimumSize(320, 230)
        self.setWindowTitle('Time Log Summary')
        self.setWindowIcon(QtGui.QIcon(config_style.img_app))

    def switch_style(self, status=''):
        if status == True:
            self.submit_button.setEnabled(True)
            self.submit_button.setStyleSheet("QPushButton{%s%s} QPushButton:hover{%s%s} QPushButton:pressed{%s%s}"
                                             %(config_style.bg_green, config_style.color_white,
                                               config_style.bg_greenWhite, config_style.color_black,
                                               config_style.bg_gray, config_style.color_black))

            self.icon_button.setEnabled(True)
            self.icon_button.setStyleSheet("QPushButton{%s} QPushButton:hover{%s} QPushButton:pressed{%s}"
                                             %(config_style.bg_white,
                                               config_style.bg_greenWhite,
                                               config_style.bg_gray))
        else:
            self.icon_button.setEnabled(False)
            self.submit_button.setEnabled(False)
            self.submit_button.setStyleSheet("QPushButton{%s%s}"%(config_style.bg_gray, config_style.color_black))


    def add_item_month(self):
        for num in range(1, 13):
            month = calendar.month_abbr[num]
            self.first_month.addItem(month)
            self.end_month.addItem(month)

    def add_item_end_month(self):
        index_end = self.end_month.currentIndex()
        index_first = self.first_month.currentIndex()

        for index in range(12):
            if index < index_first:
                self.end_month.setItemData(index, 0, QtCore.Qt.UserRole - 1)
            else:
                self.end_month.setItemData(index, 33, QtCore.Qt.UserRole - 1)

        self.end_month.setCurrentIndex(index_first) if index_first >= index_end else self.end_month.setCurrentIndex(index_end)

    def add_item_year(self):
        fisrtYear = 2021
        today = datetime.today()
        numYear = today.year - fisrtYear + 1

        for i in range(numYear):
            self.year.addItem(str(fisrtYear))
            fisrtYear += 1

    def add_item_room(self, widget):
        for room in config.room:
            widget.addItem(room)

    def submit(self):
        self.check_permission()
        self.switch_style(status=False)

        if not self.status_fetch or self.year.currentText() != self.year_old:
            self.fetch_data()
            self.year_old = self.year.currentText()
            self.status_fetch = True

        self.run()
        self.switch_style(status=True)

    def run(self):
        excel = excel_summary.Calendar()
        wb = excel.create_workbook()

        status_summary = True if self.summary.isChecked() == True else False
        status_rawData = True if self.rawData.isChecked() == True else False
        status_rawLeave = True if self.rawLeave.isChecked() == True else False
        status_monthly = True if self.monthly.isChecked() == True else False
        status_chart = True if self.chart.isChecked() == True else False

        year = int(self.year.currentText())
        first_dt = datetime.strptime(self.first_month.currentText(), "%b")
        last_dt = datetime.strptime(self.end_month.currentText(), "%b")
        first_month = first_dt.month
        last_month = last_dt.month
        zoom = 90
        #departments = config.room[self.room_departments.currentText()]
        department_list = []
        for step in self.departments:
            if config.department_HR[step] not in department_list:
                department_list.append(config.department_HR[step])

        hearder = wb['Sheet']
        hearder.title = 'Header'
        user = user_info.User()
        date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        excel.generate_header(worksheet=hearder, user=user.adname, date=date)

        if status_rawData:
            ws_RawData = excel.create_sheet(wb, 'RawData', 2)
            ws_RawData.sheet_view.zoomScale = zoom
            excel.set_title(worksheet=ws_RawData, title=config.title_RawData, freezePanes='C4')
            excel.generate_month(worksheet=ws_RawData, year=year, start_month=first_month, end_month=last_month, start_col_num=3)
            excel.generate_user(worksheet=ws_RawData, departments=department_list, users=self.user_dict, create=self.usercreate)
            excel.generate_hoilday(worksheet=ws_RawData, timeLogs=self.timeLogs, first_month=first_month, last_month=last_month)
            excel.generate_workday(worksheet=ws_RawData, timeLogs=self.timeLogs, typeLog='work', first_month=first_month, last_month=last_month)

        if status_rawLeave:
            ws_Rawleave = excel.create_sheet(wb, 'RawLeave', 3)
            ws_Rawleave.sheet_view.zoomScale = zoom
            excel.set_title(worksheet=ws_Rawleave, title=config.title_RawLeave, freezePanes='H4')
            excel.generate_month(worksheet=ws_Rawleave, year=year, start_month=first_month, end_month=last_month, start_col_num=8)
            excel.generate_user(worksheet=ws_Rawleave, departments=department_list, users=self.user_dict, create=self.usercreate)
            excel.generate_hoilday(worksheet=ws_Rawleave, timeLogs=self.timeLogs, first_month=first_month, last_month=last_month)
            excel.generate_workday(worksheet=ws_Rawleave, timeLogs=self.timeLogs, typeLog='leave', first_month=first_month, last_month=last_month)
            excel.generate_title_summary(worksheet=ws_Rawleave)

        if status_summary:
            ws_summary = excel.create_sheet(wb, 'Summary', 4)
            ws_summary.sheet_view.zoomScale = zoom
            excel.set_title(worksheet=ws_summary, title=config.title_Summary, freezePanes='H4')
            excel.generate_month(worksheet=ws_summary, year=year, start_month=first_month, end_month=last_month, start_col_num=8)
            excel.generate_user(worksheet=ws_summary, departments=department_list, users=self.user_dict, create=self.usercreate)
            excel.generate_hoilday(worksheet=ws_summary, timeLogs=self.timeLogs, first_month=first_month, last_month=last_month)
            excel.generate_summary(worksheet=ws_summary, timeLogs=self.timeLogs, first_month=first_month, last_month=last_month, year=year)
            excel.generate_title_summary(worksheet=ws_summary)

        if status_monthly:
            for month in range(first_month, last_month + 1):
                if month in self.projectLogs.keys():

                    dt = datetime.strptime(str(month), "%m")
                    month_name = dt.strftime("%b")
                    data_month = self.projectLogs[month] if month in self.projectLogs.keys() else {}
                    leave_month = self.leaveLogs[month] if month in self.leaveLogs.keys() else {}

                    ws_month = wb.create_sheet(month_name)
                    ws_month.sheet_view.zoomScale = zoom
                    excel.set_title(worksheet=ws_month, title=config.title_RawData)
                    excel.generate_head_project(worksheet=ws_month, projectLogs=self.projectdict[month], color_project=self.color_dict)
                    excel.set_end_title_project(worksheet=ws_month, month=month, end_title=config.title_Project)
                    excel.generate_user(worksheet=ws_month, departments=department_list, users=self.user_dict, leave=leave_month, create=self.usercreate, data_month=data_month)
                    excel.generate_data_project(worksheet=ws_month, data=self.projectLogs[month])
                    excel.set_color_project_inactive(worksheet=ws_month, project_status=self.project_status, year=year)
                    excel.generate_sum_project(worksheet=ws_month)
                    excel.generate_sum_title_project(worksheet=ws_month, timeLogs=self.timeLogs, month=month, year=year)

        if status_chart:
            sheetList = [ sheet for sheet in wb if sheet.title in config.months]
            project_summary = excel.create_sheet(wb, 'Project Summary', 1)
            excel.chart_project(worksheet=project_summary, sheetList=sheetList)
            excel.chart_month(worksheet=project_summary)
            excel.chart_data_project(worksheet=project_summary, sheetList=sheetList, projectdict=self.projectdict)
            excel.chart_status_project(worksheet=project_summary, project_status=self.project_status)
            excel.chart_allYaer(worksheet=project_summary)

        try:
            desktop = os.path.join(os.environ['HOMEDRIVE'], os.environ["HOMEPATH"], "Desktop")
            fileName = '%s%s'%('Time_Log_Summary','.xlsx')
            path = os.path.join(desktop, fileName)
            excel.save(wb, path)
            os.startfile(path)
        except Exception as e:
            print("Error", e)

    def signal(self):
        self.submit_button.clicked.connect(self.submit)
        self.first_month.currentIndexChanged.connect(self.add_item_end_month)
        self.icon_button.clicked.connect(self.fetch_data)


def show():
    app = QtWidgets.QApplication.instance()
    if not app:
        app = QtWidgets.QApplication(sys.argv)
    myApp = App()
    myApp.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    show()