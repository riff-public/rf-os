from datetime import datetime
from datetime import timedelta
import calendar
import os
import sys

from rf_utils.sg import sg_schedule
from rf_utils import project_info
from rf_app.time_logged.excel import config
from rf_utils import file_utils
from rf_app.time_logged import sg_time_log_utils as stlg
sg = stlg.sg
moduleDir = os.path.dirname(sys.modules[__name__].__file__).replace('\\', '/')

def list_project_active(year):
    fields = ['name', 'sg_status','start_date','end_date']
    projects = sg.find('Project', [], fields)
    projectsActive = [a for a in projects if a['sg_status'] == 'Active' and a['name'] != 'projectName']
    projectsTimeline = [a for a in projects if a['start_date'] and a['end_date']]

    project_status = dict()
    project_active = list()
    for project in projects:
        name = project['name']
        status = project['sg_status']
        start_date = datetime.strptime(project['start_date'], '%Y-%m-%d') if project['start_date'] else None
        end_date = datetime.strptime(project['end_date'], '%Y-%m-%d') if project['end_date'] else None
        
        if not name in project_status.keys():
            project_status[name] = {'status':status, 'start_date':start_date, 'end_date':end_date}

    for i in projectsTimeline:
        projectName = i['name']
        start_date = datetime.strptime(i['start_date'], '%Y-%m-%d')
        end_date = datetime.strptime(i['end_date'], '%Y-%m-%d')
        
        if year >= start_date.year and year <= end_date.year:
            if projectName not in project_active:
                project_dict = {projectName:{'start_date':start_date, 'end_date':end_date}}
                project_active.append(project_dict)

    for i in projectsActive:
        projectName = i['name']
        start_date = datetime.strptime(i['start_date'], '%Y-%m-%d') if i['start_date'] else None
        end_date = datetime.strptime(i['end_date'], '%Y-%m-%d') if i['end_date'] else None

        if projectName not in project_active:
            project_dict = {projectName:{'start_date':start_date, 'end_date':end_date}}
            project_active.append(project_dict)

    return project_status, project_active

def find_all_timeLog(year=2021):
    start_date = (year, 1, 1)
    end_date = (year, 12, 31)
    start = datetime(*start_date).date()
    end = datetime(*end_date).date()
    
    date_range = [start, end]
    filters = [['date', 'between', date_range]]
    fields = ['project', 'user', 'duration', 'date', 'entity', 'user.HumanUser.department', 'user.HumanUser.sg_name']
    logs = sg.find('TimeLog', filters, fields)
    logs = [log for log in logs if log['user.HumanUser.sg_name']]

    return logs

def find_date_create_account():
    createdict = dict()
    data = ''
    logs = sg.find('HumanUser', [], ["name", "department","created_at","sg_date_employment", "sg_name"])
    for user in logs:
        name = user.get('sg_name')
        date_time_str = user['sg_date_employment']
        
        if date_time_str:
            start_work = datetime.strptime(date_time_str, '%Y-%m-%d')
            data = (start_work.year, start_work.month, start_work.day)
        else:
            data = (user['created_at'].year, user['created_at'].month, user['created_at'].day)
        
        if not name in createdict.keys():
            createdict[name] = {'created_at': data}
            
    return createdict

def find_color_project():
    colorLogs = dict()
    fields = ['code', 'id', 'color', 'name']
    projects = sg.find('Project', [], fields)

    for pro in projects:
        name = pro['name']
        color = pro['color']

        if not name in colorLogs.keys():
            colorLogs[name] = {'color':color}
        else:
            if not 'color' in colorLogs[name].keys():
                colorLogs[name]['color'] = color
            else:
                colorLogs[name]['color'] = color

    return colorLogs

def list_all_users(name=None): 
    userdict = dict()
    filters = [['name', 'is', name]] if name else []
    field = ['sg_employee_id', 'sg_ad_account', 'sg_employee_department', 'sg_name', 'sg_hr_local_name', 'department.Department.name', 'sg_studio', 'sg_active']
    result = sg.find('HumanUser', filters, field)

    blacklist = read_user_blackList()

    for data in result:
        step = data['department.Department.name']
        step = config.department_HR[step] if step in config.department_HR.keys() else 'None'
        user = data.get('sg_name')
        studio = data['sg_studio']
        active =  data['sg_active']

        if user not in blacklist['User'] and studio == 'Riff':
        
            if not step in userdict.keys():
                userdict[step] = {user:data}
            else:
                 if not user in userdict[step].keys():
                     userdict[step][user] = data
                 else:
                     userdict[step][user] = data
        
    return userdict

def read_user_blackList():
    blacklistfile = 'blacklist.yml'
    blacklist_path = '{}/{}'.format(moduleDir, blacklistfile)
    if os.path.exists(blacklist_path):
        blacklist = file_utils.ymlLoader(blacklist_path)
        return blacklist

def summary_holiday_logs(year=2021):
    FIRST_DAY = (year, 1, 1)
    LAST_DAY = (year, 12, 31)
    logs = sg_schedule.get_studio_schedule(FIRST_DAY, LAST_DAY)
    
    info = dict()
    minute = 480

    for log in logs:
        date = log
        dt = datetime.strptime(date, '%Y-%m-%d')
        day = dt.day
        month = dt.month
        duration = minute
        
        if not month in info.keys():
            info[month] = {day:duration}
        else:
            if not day in info[month].keys():
                info[month][day] = duration
            else:
                info[month][day] = duration
                
    return info

def summary_weekend_logs(year=''):
    info = dict()
    minute = 480

    FIXED = 1
    SATURDAY, SUNDAY = 5, 6
    START_MONTH = 1
    LAST_MONTH = 12 + FIXED

    for month in range(START_MONTH, LAST_MONTH):
        weekday, numdays = calendar.monthrange(year, month)
        for day in range(FIXED, numdays + FIXED):
            dt = datetime(year, month, day)
            duration = minute

            if dt.weekday() in (SATURDAY, SUNDAY):

                if not month in info.keys():
                    info[month] = {day:duration}
                else:
                    if not day in info[month].keys():
                        info[month][day] = duration
                    else:
                        info[month][day] = duration
    return info

def summary_leave(year=2021):
    filters = [['vacation', 'is', True]]
    fields = ['sg_leave_type', 'start_date', 'end_date', 'user', 'note', 'id', 'sg_duration', 'user.HumanUser.department', 'user.HumanUser.sg_name']
    leave_logs = sg.find('Booking', filters, fields)
    FIXED = 1
    holiday_logs = summary_holiday_logs(year=year)
    SATURDAY, SUNDAY = 5, 6
    num_day = 8

    leave_data = dict()
    for log in leave_logs:
         if log['user'] and log['sg_duration']:
            start_date = log['start_date']
            end_date = log['end_date']
            user = log['user.HumanUser.sg_name']
            step = log['user.HumanUser.department']['name'].lower()
            typeName = log['sg_leave_type']
            duration = log['sg_duration']
            data = {typeName:duration}

            start_dt = datetime.strptime(start_date, '%Y-%m-%d')
            end_dt = datetime.strptime(end_date, '%Y-%m-%d')
            numdays = end_dt - start_dt if start_dt != end_dt else None

            if start_dt.year == year:
                if numdays:
                    for dt in range(FIXED, numdays.days + FIXED):
                        date = start_dt + timedelta(days=dt)

                        if date.month in holiday_logs.keys():
                            holiday = True if date.day in holiday_logs[date.month].keys() else False

                        num_day = num_day + FIXED if date.weekday() not in (SATURDAY, SUNDAY) and not holiday else num_day

                    data = {typeName:int(duration/num_day)}
                    for dt in range(0, numdays.days + FIXED):
                        date = start_dt + timedelta(days=dt)

                        if date.month not in leave_data.keys():
                            leave_data[date.month] = {user:[date]}
                        else:
                            if user not in leave_data[date.month].keys():
                                leave_data[date.month][user] = [date]
                            else:
                                leave_data[date.month][user].append(date)

                else:
                    if start_dt.month not in leave_data.keys():
                            leave_data[start_dt.month] = {user:[start_dt]}
                    else:
                        if user not in leave_data[start_dt.month].keys():
                            leave_data[start_dt.month][user] = [start_dt]
                        else:
                            leave_data[start_dt.month][user].append(start_dt)

    return leave_data


def summary_leave_logs(holiday_logs='', timeLogs= '', year=''):
    FIXED = 1
    ONEDAY = 1
    SATURDAY, SUNDAY = 5, 6

    filters = [['vacation', 'is', True]]
    fields = ['sg_leave_type', 'start_date', 'end_date', 'user', 'note', 'id', 'sg_duration', 'user.HumanUser.department', 'user.HumanUser.sg_name']
    leave_logs = sg.find('Booking', filters, fields)

    for log in leave_logs:
        if log['user'] and log['sg_duration']:
            start_date = log['start_date']
            end_date = log['end_date']
            user = log['user.HumanUser.sg_name']
            step = log['user.HumanUser.department']['name'].lower()
            leave_type = log['sg_leave_type']
            duration = log['sg_duration']
            data = {leave_type:duration}
            num_day = ONEDAY
            holiday = False

            start_dt = datetime.strptime(start_date, '%Y-%m-%d')
            end_dt = datetime.strptime(end_date, '%Y-%m-%d')
            numdays = end_dt - start_dt if start_dt != end_dt else None

            if start_dt.year == year:
                if numdays:
                    for dt in range(FIXED, numdays.days + FIXED):
                        date = start_dt + timedelta(days=dt)

                        if date.month in holiday_logs.keys():
                            holiday = True if date.day in holiday_logs[date.month].keys() else False

                        num_day = num_day + FIXED if date.weekday() not in (SATURDAY, SUNDAY) and not holiday else num_day

                    data = {leave_type:int(duration/num_day)}
                    for dt in range(0, numdays.days + FIXED):
                        date = start_dt + timedelta(days=dt)

                        if date.month in holiday_logs.keys():
                            holiday = True if date.day in holiday_logs[date.month].keys() else False

                        if date.weekday() not in (SATURDAY, SUNDAY) and not holiday:
                            logs_dict(logs=timeLogs, user=user, month=date.month, day=date.day, key='leave', data=data)

                else:
                    logs_dict(logs=timeLogs, user=user, month=start_dt.month, day=start_dt.day, key='leave', data=data)

    return timeLogs

def summary_time_logs(year=2021):
    timeLog = dict()
    logs = find_all_timeLog(year=year)

    for log in logs:
        dt = datetime.strptime(log['date'], '%Y-%m-%d')
        day = dt.day
        month = dt.month
        step = log['user.HumanUser.department']['name'].lower()
        user = log['user.HumanUser.sg_name']
        duration = log['duration']
        project = log['project']['name']
        _id = log['id']
        data = duration
        
        if log['entity']:
            logs_dict(logs=timeLog, user=user, month=month, day=day, key='work', data=data)
        
    return timeLog

def summary_project_logs(year=2021):
    projectLog = dict()
    logs = find_all_timeLog(year=year)
    projectdict = dict()
    date_today = datetime.strptime(datetime.now().strftime('%Y-%m-%d'), '%Y-%m-%d')

    for log in logs:
        dt = datetime.strptime(log['date'], '%Y-%m-%d')
        day = dt.day
        month = dt.month
        step = log['user.HumanUser.department']['name'].lower()
        user = log['user.HumanUser.sg_name']
        duration = log['duration']
        project = log['project']['name']
        _id = log['id']
        data = {'duration':duration, 'project':project}
        
        if dt < date_today and log['entity']:

            if not month in projectLog.keys():
                 projectLog[month] = {user:{project:duration}}
            else:
                if not user in projectLog[month].keys():
                    projectLog[month][user] = {project:duration}
                else:
                    if not project in projectLog[month][user].keys():
                        projectLog[month][user][project] = duration
                    else:
                        projectLog[month][user][project] += duration

            if not month in projectdict.keys():
                projectdict[month] = {project:user}
            else:
                if not project in projectdict[month].keys():
                    projectdict[month][project] = user
                else:
                    projectdict[month][project] = user

    #make summary decimal
    projectLog = summary_make_project(projectLog)

    #make project active
    project_status, project_active = list_project_active(year)
    for project in project_active:
        for month in range(date_today.month):
            month = month + 1
            projectName = project

            start_month, end_month = month, month
            if isinstance(project, dict):
                keys = [k for k, v in project.items()]

                projectName = keys[0]
                start_month = project[keys[0]]['start_date'].month if project[keys[0]]['start_date'] else month
                end_month = project[keys[0]]['end_date'].month if project[keys[0]]['end_date'] else month

                if project_status[projectName]['status'] == 'Active' and start_month < month:
                    end_month = month

            if month >= start_month and month <= end_month:
                if not month in projectdict.keys():
                    projectdict[month] = {projectName:None}
                else:
                    if not projectName in projectdict[month].keys():
                        projectdict[month][projectName] = None

    return projectLog, projectdict, project_status


def summary_make_project(projectLog):
    for month in projectLog:
        for user in projectLog[month]:
            value = 0
            old_project = None
            old_duration = None
            status = False
            Hrs = 60

            for project in projectLog[month][user]:
                num = projectLog[month][user][project]

                duration = (num/Hrs)

                if not old_project:
                    old_project = project
                    old_duration = duration

                elif duration > old_duration:
                    old_project = project
                    old_duration = duration

                if int(duration) > duration:
                    value += int(duration)* Hrs - num
                    projectLog[month][user][project] = (int(duration)) * Hrs
                    status = True

                elif int(duration) < duration:
                    value += (int(duration) + 1)* Hrs - num
                    projectLog[month][user][project] = (int(duration) + 1) * Hrs
                    status = True

            if status:
                dur = projectLog[month][user][old_project]
                projectLog[month][user][old_project] = dur - value

    return projectLog


def logs_dict(logs='', user='', month='', day='', key='', data=''):
    if key == 'leave':
        if not user in logs.keys():
            logs[user] = {month:{day:{key:data}}}
        else:
            if not month in logs[user].keys():
                logs[user][month] = {day:{key:data}}
            else:
                if not day in logs[user][month].keys():
                    logs[user][month][day] = {key:data}
                else:
                    if not key in logs[user][month][day].keys():
                        logs[user][month][day][key] = data
                    else:
                        logs[user][month][day][key].update(data)

    else:
        if not user in logs.keys():
            logs[user] = {month:{day:{key:data}}}
        else:
            if not month in logs[user].keys():
                logs[user][month] = {day:{key:data}}
            else:
                if not day in logs[user][month].keys():
                    logs[user][month][day] = {key:data}
                else:
                    if not key in logs[user][month][day].keys():
                        logs[user][month][day][key] = data
                    else:
                        logs[user][month][day][key] += data


def summary_logs(departments='', year=2021):
    FIXED = 1
    FIRST_DAY = 1
    FIRST_MONTH = 1
    LAST_MONTH = 12
    TYPE_WORK = 'work'
    TYPE_WEEKEND = 'weekend'
    TYPE_HOLIDAY = 'holiday'

    timeLogs = summary_time_logs(year=year)
    holiLogs = summary_holiday_logs(year=year)
    weekLogs = summary_weekend_logs(year=year)
    user_dict = list_all_users()

    for step in departments:
        step = config.department_HR[step]
        for user in user_dict[step]:

            for month in range(FIRST_MONTH, LAST_MONTH + FIXED):
                weekday, numdays = calendar.monthrange(year, month)
                data = 0

                for day in range(FIRST_DAY, numdays + FIXED):
                    if user not in timeLogs.keys():
                        logs_dict(logs=timeLogs, user=user, month=month, day=day, key=TYPE_WORK, data=data)

                    if user in timeLogs.keys():
                        if month not in timeLogs[user].keys():
                            logs_dict(logs=timeLogs, user=user, month=month, day=day, key=TYPE_WORK, data=data)

                        elif day not in timeLogs[user][month].keys():
                            logs_dict(logs=timeLogs, user=user, month=month, day=day, key=TYPE_WORK, data=data)

            for month in weekLogs:
                for day in weekLogs[month]:

                    minute = weekLogs[month][day]
                    logs_dict(logs=timeLogs, user=user, month=month, day=day, key=TYPE_WEEKEND, data=minute)

            for month in holiLogs:
                for day in holiLogs[month]:

                    minute = holiLogs[month][day]
                    logs_dict(logs=timeLogs, user=user, month=month, day=day, key=TYPE_HOLIDAY, data=minute)

    timeLogs = summary_leave_logs(holiday_logs=holiLogs, timeLogs=timeLogs, year=year)
    return timeLogs