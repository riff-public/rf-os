from rf_app.time_logged.excel import config
fixed_point = 1

def hearderinfo(excel='', workbook='', sheetName='Header', sheet_num='', user=None, date=None):
    headerinfo = workbook.get_sheet_by_name('Sheet')
    headerinfo.title = sheetName
    headerinfo.index = sheet_num
    excel.generate_headerInfo(worksheet=headerinfo, user=user, date=date)

def raw_data(excel='', workbook='',sheetName='RawData', sheet_num='', users=[], user_create='',leave_data=[], work_data=[], year=2021, start_month=1, end_month=12, room_step=''):
    ws_RawData = excel.create_sheet(workbook, sheetName, sheet_num)
    excel.set_title(worksheet=ws_RawData, title=config.title_RawData)
    excel.generate_month(worksheet=ws_RawData, year=year, start_month=start_month, end_month=end_month)
    excel.generate_user(worksheet=ws_RawData, departments=config.room[room_step], users=users, user_create=user_create)

    for month in range(start_month, end_month + fixed_point):
        excel.generate_holidays(worksheet=ws_RawData, year=year, month=month)
        excel.generate_weekend(worksheet=ws_RawData, year=year, month=month)

        for step in config.room[room_step]:
            if step in config.sg.keys() and month in work_data.keys() and config.sg[step] in work_data[month].keys():
                excel.generate_workday(worksheet=ws_RawData, list_data=work_data[month][config.sg[step]], month=month)

def raw_leave(excel='', workbook='',sheetName='RawLeave', sheet_num='', users=[], user_create='', leave_data=[], year=2021, start_month=1, end_month=12, room_step=''):
    ws_RawLeave = excel.create_sheet(workbook, sheetName, sheet_num)
    excel.set_title(worksheet=ws_RawLeave, title=config.title_RawLeave, freezePanes='H4')
    excel.generate_month(worksheet=ws_RawLeave, year=year, start_month=start_month, end_month=end_month, start_col_num=8)
    excel.generate_user(worksheet=ws_RawLeave, departments=config.room[room_step], users=users, user_create=user_create)

    for month in range(start_month, end_month + fixed_point):
        excel.generate_holidays(worksheet=ws_RawLeave, year=year, month=month)
        excel.generate_weekend(worksheet=ws_RawLeave, year=year, month=month)
        excel.generate_leaveday(worksheet=ws_RawLeave, leaveday=leave_data, year=year, month=month, status=True)

    excel.generate_sum_time_total(worksheet=ws_RawLeave)

def summary(excel='', workbook='',sheetName='Summary', sheet_num='', users=[], user_create='', leave_data=[], work_data=[], year=2021, start_month=1, end_month=12, room_step=''):
    ws_Summary = excel.create_sheet(workbook, sheetName, sheet_num)
    excel.set_title(worksheet=ws_Summary, title=config.title_Summary, freezePanes='H4')
    excel.generate_month(worksheet=ws_Summary, year=year, start_month=start_month, end_month=end_month ,start_col_num=8)
    excel.generate_user(worksheet=ws_Summary, departments=config.room[room_step], users=users, user_create=user_create)

    for month in range(start_month, end_month + fixed_point):
        excel.generate_holidays(worksheet=ws_Summary, year=year, month=month)
        excel.generate_weekend(worksheet=ws_Summary, year=year, month=month)
        excel.generate_leaveday(worksheet=ws_Summary, leaveday=leave_data, year=year, month=month, status=False)

        for step in config.room[room_step]:
            if step in config.sg.keys() and month in work_data.keys() and config.sg[step] in work_data[month].keys():
                excel.generate_workday(worksheet=ws_Summary, list_data=work_data[month][config.sg[step]], month=month)

        excel.check_time(worksheet=ws_Summary, year=2021, month=month, set_value=True)
    excel.generate_sum_time_total(worksheet=ws_Summary)

def project(excel='', workbook='',sheet_num='', users=[], user_create='', project_data=[], color_dict=[], year=2021, start_month=1, end_month=12, room_step=''):
    for month in range(start_month, end_month + fixed_point):
        if month in project_data.keys():
            sheetName = config.month_dict[month]
            ws_sheetName = excel.create_sheet(workbook, sheetName, sheet_num)
            excel.set_title(worksheet=ws_sheetName, title=config.title_RawData)
            excel.generate_head_project(worksheet=ws_sheetName, data=project_data[month], color_dict=color_dict)
            excel.generate_user(worksheet=ws_sheetName, departments=config.room[room_step], users=users, user_create=user_create)
            excel.generate_data_project(worksheet=ws_sheetName, data=project_data[month])
            excel.generate_sum_project(worksheet=ws_sheetName)
            sheet_num += 1

def project_summary(excel='', workbook='',sheetName='Project Summary', sheet_num=''):
    sheetList = [ wb for wb in workbook for month in config.month_dict if wb.title == config.month_dict[month]]
    project_summary = excel.create_sheet(workbook, sheetName, sheet_num)
    excel.chart_project(worksheet=project_summary, sheetList=sheetList)
    excel.chart_month(worksheet=project_summary)
    excel.chart_data_project(worksheet=project_summary, sheetList=sheetList)
    excel.chart_allYaer(worksheet=project_summary)
