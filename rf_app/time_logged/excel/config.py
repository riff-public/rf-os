department_HR = {'Art':'Art',
				 'Model':'Model',
				 'Groom':'Groom & Look Development',
				 'Lookdev':'Groom & Look Development',
				 'Rig':'Rigging',
 				 'Layout':'Previsualization',
				 'Anim':'Animation',
				 'Sim':'CFX',
				 'Lighting':'Lighting & Rendering',
				 'FX':'FX Dynamics ',
				 'Comp':'Compositing',
				 'Edit':'Editorial',
				 'Realtime':'Production Technology',
				 'Pipeline':'Production Technology',
				 'IP Management':'IP Management',
				 'Production':'Production',
				 'None':'None'
                }

room = {'All':['Art','Model','Groom','Lookdev','Rig','Layout','Anim','Sim','Lighting','FX','Comp','Edit','Realtime','Pipeline','IP Management','Production','None'],
        'Art, Edit':['Art','Edit','None'],
        'Model, Groom & LDV':['Model','Groom','Lookdev','None'],
        'Layout, Anim':['Layout','Anim','None'],
        'Rig, Sim, Realtime, Pipeline':['Rig','Sim','Realtime','Pipeline','None'],
        'Lighting, FX, Comp':['Lighting','FX','Comp','None'],
        'IP Management':['IP Management', 'None'],
        'Producer':['Production','None']
       }

months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']

title_RawData = ['ID', 'NAME']
title_RawLeave = ['ID', 'NAME', 'Sick', 'Personal', 'Vacation', 'Special', 'Absent']
title_Summary = ['ID', 'NAME','Work', 'Absents', 'Leave', 'Holiday', 'Total']
title_Project = ['Work', 'Absents', 'Leave', 'Total', 'Working Hrs']