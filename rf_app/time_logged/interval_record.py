import os
import sys
import argparse
import logging
import subprocess
from datetime import datetime
core = '{}/core'.format(os.environ['RFSCRIPT'])
if not core in sys.path: 
    sys.path.append(core)
import rf_config as config
from rf_utils.context import context_info
from rf_utils import user_info
from rf_utils.sg import sg_time_track as sgtt


logger = logging.getLogger(__name__)
MODULEDIR = os.path.dirname(__file__).replace('\\', '/')
LASTACTIVE = 'LASTACTIVE'
USER_IDLE = 'USER_IDLE'
STRFFORMAT = '%Y-%m-%d %H:%M:%S'


class Setting:
    keyfile = '{}/key/credentials.json'.format(MODULEDIR)
    sheet_url = 'https://docs.google.com/spreadsheets/d/1LX8YnLHtImFx4idHSfGaFpSQQF3kXmiDCYjzJ-84njs/edit#gid=0'


class DataSetting:
    date = 'Date'
    time = 'Start time'
    user = 'User'
    login = 'Login'
    duration = 'Duration'
    path = 'Entity'
    columns = [date, time, user, login, duration, path]


def record_name(user, entity='', project='', task=''):
    date = datetime.now().date().strftime('%Y:%m:%d')
    name_list = [user, date]
    name_list.append(project) if project else None
    name_list.append(entity) if entity else None
    name_list.append(task) if task else None
    return '-'.join(name_list)


def add_dcc_record(dcc='maya', duration=10, logged_type='Automatic'):
    """ 
    Add / Update entry from dcc app 
    """ 
    assets = []
    shots = []
    start = datetime.now()
    if dcc == 'maya':
        if is_active():
            if config.isMaya:
                entity = context_info.ContextPathInfo()
                project = entity.project
                user = user_info.User(sg=sgtt.sg)
                userstr = user.name() or ''
                name = record_name(userstr, project, entity.name, task='')
                date_obj = datetime.now().date()
                entity_type = ''
                if entity.entity_type == 'asset':
                    entity_type = 'Asset'
                    assets = [entity.name]
                if entity.entity_type == 'scene':
                    shots = [entity.name]
                    entity_type = 'Shot'
                # sgtt.update_entry(name, userstr, date_obj, project, department=entity.step, duration=duration, assets=assets, shots=shots, tasks=[], duration_append=True)
                sgtt.update_entry(
                    name, userstr, date_obj, project,
                    entity_type=entity_type,
                    entity=entity.name,
                    department=entity.step,
                    duration=duration,
                    duration_append=True)
                logger.debug('TIME RECORDED')
    else:
        pass 
        # logger.debug('NOT RECORD, user idle')


def update_entry(user, entity_name, step, entity_type, project_name, task='', duration=15, duration_append=True, logged_type='Automatic'): 
    status = 'ip' if duration > 0 else ''
    sg_user = sgtt.find_user(user)
    userstr = sg_user.get('name', '')
    entry_name = record_name(userstr, project_name, entity_name, task)
    date_obj = datetime.now().date()
    result = sgtt.update_entry(
                    entry_name, 
                    userstr, 
                    date_obj, 
                    project_name,
                    tasks=[task], 
                    entity_type=entity_type,
                    entity=entity_name,
                    department=step,
                    duration=duration,
                    duration_append=duration_append, 
                    status=status,
                    logged_type=logged_type)

    return result


def sync_tasks_timetrack(project, user, **kwargs): 
    """ 
    Convert assigned task to timetrack entry 
    Args: 
        project (str) : project name
        user (str) : user name 

    Returns (list) : a list of shotgun entry entity dict
    """ 
    loop_progress = kwargs.get('loop_callback')
    task_entities = sgtt.find_tasks(project, user)
    timetrack_entries = sgtt.fetch_timetrack_data(user, project)
    create = False

    for task in task_entities: 
        project = task.get('project').get('name')
        task_name = task.get('content')
        entity_name = task.get('entity').get('name')
        entity_type = task.get('entity').get('type')
        step_name = task.get('step.Step.short_name')

        match_entry = None
        
        for entry in timetrack_entries: 
            project_entry = (entry.get('sg_project') or dict()).get('name')
            tasks = [a.get('name') for a in entry.get('sg_tasks')] if entry.get('sg_tasks') else []
            entity_entry = (entry.get('sg_entity') or dict()).get('name')

            # print '{}-{} {}-{} {}-{}'.format(project, project_entry, task_name, tasks, entity_name, entity_entry)
            if project == project_entry and task_name in tasks and entity_name == entity_entry: 
                # print 'Matched'
                match_entry = entry
        
        if not match_entry: 
            # create entry 
            # print 'Nothing match'
            result = update_entry(user, entity_name, step_name, entity_type, project, task=task_name, duration=0, duration_append=True, logged_type='User')
            logger.debug('Created {}'.format(result))
            create = True

        else: 
            logger.debug('Matched {}'.format(entry))

    if create: 
        return sgtt.fetch_timetrack_data(user, project)
    return timetrack_entries


def fetch_entry(user): 
    return sgtt.fetch_timetrack_data(user)


def value_data(date='', time='', user='', duration='', path='', login=''):
    return [date, time, user, login, duration, path]


def set_last_active_time(*args):
    os.environ[LASTACTIVE] = datetime.now().strftime(STRFFORMAT)


def set_idle_status(status):
    os.environ[USER_IDLE] = '1' if status else '0'


def is_active():
    status = int(os.environ.get(USER_IDLE, 'None'))
    if status == 'None':
        return True
    return status


def last_idle():
    date_str = os.environ.get(LASTACTIVE)
    if date_str:
        last_active = datetime.strptime(date_str, STRFFORMAT)
        timedelta = datetime.now() - last_active
        return timedelta.total_seconds()
    return 0


# GOOGLE SHEET FUNCTION __ NOT USE

# def run(project, user, login, path, duration=15, console=False):
#     """
#     Add record to google sheet
#     """
#     startupinfo = None
#     if not console:
#         startupinfo = subprocess.STARTUPINFO()
#         startupinfo.dwFlags |= subprocess.STARTF_USESHOWWINDOW
#     subprocess.Popen([config.Software.gservices, __file__, '-p', project, '-u', user, '-login', login, '-path', path, '-duration', str(duration)], startupinfo=startupinfo)


# def main(project, user, login, path, duration=15):
#     """
#     This function is meant to be run within "gservices" virtualenv at rf_lib/python/gservices/python.exe
#     This cannot be run within maya due to environment packages.
#     """
#     add_record(project, user, login, path, int(duration))


# def add_dcc_record(dcc='maya'):
#     """ add record """
#     if dcc == 'maya':
#         if config.isMaya:
#             import maya.cmds as mc
#             from rf_utils.context import context_info
#             from rf_utils import user_info
#             user = user_info.User()
#             entity = context_info.ContextPathInfo()
#             login = user.login_user()
#             userstr = user.name()
#             path = entity.name
#             if entity.valid:
#                 project = entity.project
#                 path = entity.name
#             else:
#                 project = 'Unknown'
#                 path = mc.file(q=True, loc=True)

#             run(project, userstr, login, path)


# def add_record(project, user, login, path, duration=15):
#     from rf_utils.gsheet import googlesheet as gs
#     from rf_utils import time_utils

#     scope = gs.Scope.sheet_full
#     client = gs.createAccount(Setting.keyfile, scope)
#     sheet = gs.openByURL(client, Setting.sheet_url)

#     now = datetime.now()
#     date = time_utils.time_to_str(now, showdate=True, showtime=False)
#     start_time = time_utils.time_to_str(now, showdate=False, showtime=True)

#     # list all tabs
#     ws_list = gs.list_worksheet(sheet)

#     if project not in ws_list:
#         # create new tab
#         gs.new_worksheet(sheet, project, 10, 10)
#         ws = gs.select_worksheetByTitle(sheet, project)
#         gs.append_row(ws, DataSetting.columns)

#     else:
#         ws = gs.select_worksheetByTitle(sheet, project)

#     users = gs.get_col_val(ws, DataSetting.columns.index(DataSetting.user) + 1)

#     if user not in users:
#         # new
#         input_list = value_data(date=date, time=start_time, user=user, duration=duration, path=path, login=login)
#         gs.append_row(ws, input_list)

#     else:
#         # existing
#         user_cell = gs.find_item(ws, user)
#         old_date_dict = dict()

#         for cell in user_cell:
#             old_date = gs.get_val_acell(ws, 'A{}'.format(cell.row))
#             old_date_dict[old_date[0][0]] = cell

#         if date not in old_date_dict.keys():
#             input_list = value_data(date=date, time=start_time, user=user, duration=duration, path=path, login=login)
#             gs.append_row(ws, input_list)

#         else:
#             for date_key in old_date_dict.keys():
#                 if date == date_key:
#                     # this is the latest cell
#                     current_cell = old_date_dict[date_key]
#                     # get current duration
#                     old_duration = gs.get_val_cell(ws, current_cell.row, DataSetting.columns.index(DataSetting.duration) + 1)
#                     new_duration = int(old_duration) + duration
#                     gs.update_val_cell(ws, current_cell.row, DataSetting.columns.index(DataSetting.duration) + 1, new_duration)

#                     # get current entity
#                     old_path = gs.get_val_cell(ws, current_cell.row, DataSetting.columns.index(DataSetting.path) + 1)
#                     old_path_list = old_path.split(',')

#                     if path not in old_path_list:
#                         new_path = old_path + ',{}'.format(path)
#                         gs.update_val_cell(ws, current_cell.row, DataSetting.columns.index(DataSetting.path) + 1, new_path)


# def setup_parser(title):
#     """ setup parser """
#     parser = argparse.ArgumentParser(description=title, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
#     parser.add_argument('-p', dest='project', type=str, help='Project name', default='')
#     parser.add_argument('-u', dest='user', type=str, help='User name', default='')
#     parser.add_argument('-login', dest='login', type=str, help='Login', default='')
#     parser.add_argument('-path', dest='path', type=str, help='Scene path', default='')
#     parser.add_argument('-duration', dest='duration', type=str, help='Scene path', default=15)
#     return parser


# if __name__ == '__main__':
#     print 'Adding record ...'
#     parser = setup_parser('GSheet interval record')
#     params = parser.parse_args()
#     main(project=params.project, user=params.user, login=params.login, path=params.path, duration=params.duration)
