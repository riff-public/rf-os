import os
import sys
import traceback
from collections import OrderedDict, defaultdict

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui


module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.StreamHandler())
logger.setLevel(logging.DEBUG)

from rf_utils.sg import sg_process
from rf_utils import file_utils
from rf_utils import daily
sg = sg_process.sg

from rf_utils.pipeline.notification import noti_module_cliq
reload(noti_module_cliq)

def find_next_version(projectEntity, taskEntity, shotEntity):
    filters = [['project', 'is', projectEntity],
                ['entity', 'is', shotEntity],
                ['sg_task', 'is', taskEntity]]
    fields = ['code', 'id']
    versions = sg.find('Version', filters, fields)

    versionCodes = [a['code'] for a in versions]
    version = next_version(versionCodes)
    return version

def next_version(versions):
    allVersions = sorted([extract_version(a) for a in versions])
    maxVersion = allVersions[-1] if allVersions else 'v000'
    nextVersion = int(maxVersion.replace('v', '')) + 1
    return 'v%03d' % nextVersion


# def extract_version(filename):
#     elems = filename.split('.')
#     version = [a for a in elems if a[0] == 'v' and a[1:].isdigit()]
#     return version[0] if version else ''

def extract_version(filename):
    for i, st in enumerate(filename):
        if st == 'v' and filename[i+1:i+4].isdigit():
            return filename[i:i+4]


def publish_images(ContextPathInfo, outputPath, imageFiles, hero=False, clearOutput=False, clearOutputFilters=[], forceImageSequence=False):
    scene = ContextPathInfo

    outputs = []
    if clearOutput: 
        if os.path.exists(outputPath): 
            try: 
                file_utils.emptyDir(outputPath, filters=clearOutputFilters)
            except Exception as e: 
                logger.error('Remove error {}'.format(outputPath))

    if imageFiles:
        singlefile = len(imageFiles) == 1
        if forceImageSequence or not singlefile:
            for i, imageFile in enumerate(imageFiles):
                fn, ext = os.path.splitext(imageFile)
                filename = scene.publish_name(hero=hero, ext=ext)
                name, ext = os.path.splitext(filename)
                imgSeqName = '%s.%04d%s' % (name, i + 1, ext)
                dst = '%s/%s' % (outputPath, imgSeqName)
                # copy
                file_utils.copy(imageFile, dst)
                outputs.append(dst)
        else:
            fn, ext = os.path.splitext(imageFiles[0])
            filename = scene.publish_name(hero=hero, ext=ext)
            dst = '%s/%s' % (outputPath, filename)
            # copy
            result = file_utils.copy(imageFiles[0], dst)
            outputs.append(dst)

    return outputs

def publish_files(ContextPathInfo, inputImgs, subfolder=''):
    scene = ContextPathInfo
    # publishOutput
    outputImgPath = scene.path.outputImg().abs_path()
    publishedImages = publish_images(scene, outputImgPath, inputImgs, hero=False)
    logger.info('published imgs' % publishedImages)

    # publishHero
    outputHeroImgPath = scene.path.output_hero_img().abs_path()
    publishedHeroImages = publish_images(scene, outputHeroImgPath, inputImgs, hero=True, clearOutput=True)
    logger.info('published hero' % publishedHeroImages)

    # publishDaily
    daily.submit(scene, publishedHeroImages, subfolder=subfolder)
    logger.info('submit daily')

    return (publishedImages, publishedHeroImages)

def publish_output(ContextPathInfo, namekey, outputFile):
    scene = ContextPathInfo
    # publishOutput
    outputPath = scene.path.publish_output().abs_path()
    publishedName = scene.output_name(outputKey=namekey, hero=False)
    publishedFile = '%s/%s' % (outputPath, publishedName)

    file_utils.copy(outputFile, publishedFile)
    logger.info('published imgs %s' % publishedFile)

    # publishHero
    outputHeroPath = scene.path.output_hero().abs_path()
    publishedHeroName = scene.output_name(outputKey=namekey, hero=True)
    publishedHeroFile = '%s/%s' % (outputHeroPath, publishedHeroName)

    file_utils.copy(outputFile, publishedHeroFile)
    logger.info('published hero %s' % publishedHeroFile)

    return (publishedHeroFile, publishedHeroFile)


def art_publish_files(ContextPathInfo, workspaceFiles, inputImgs):
    asset = ContextPathInfo
    asset_filters =  ['{}_{}'.format(asset.name, asset.process)]

    # ===== WORKSPACE
    for ext, files in workspaceFiles.iteritems():
        asset.context.update(app=ext.replace('.', ''))

        # --- publish Workspace up version
        # design/publ/type/assetName/task/v00x/ext
        # workspacePath P:/Hanuman/design/publ/prop/testProp/color/v001/psd/testProp_color.v001.png
        workspacePath = asset.path.publish_workspace().abs_path()
        workspaceVersions = publish_images(ContextPathInfo=asset, 
                                        outputPath=workspacePath, 
                                        imageFiles=files, 
                                        hero=False, 
                                        clearOutput=False,
                                        forceImageSequence=True)
        logger.info('Workspace version: {}'.format(workspaceVersions))

        # --- publish Workspace hero
        # design/publ/type/assetName/task/hero/ext
        # workspaceHero P:/Hanuman/design/publ/prop/testProp/color/hero/psd/testProp_color.hero.png
        workspaceHeroPath = asset.path.publish_workspace_hero().abs_path()
        # copy the files, if files is [] then the script will clear the folder
        workspaceHeroes = publish_images(ContextPathInfo=asset, 
                                        outputPath=workspaceHeroPath, 
                                        imageFiles=files, 
                                        hero=True, 
                                        clearOutput=True,
                                        clearOutputFilters=asset_filters, 
                                        forceImageSequence=True)
        logger.info('Workspace hero: {}'.format(workspaceHeroes))

    # ===== OUTPUTS
    # --- publish task version
    # design/publ/type/assetName/task/v00x/output
    # publishedImages P:/Hanuman/design/publ/prop/testProp/color/v001/output/testProp_color.v001.jpg
    outputImgPath = asset.path.outputImg().abs_path()
    publishedImages = publish_images(asset, outputImgPath, inputImgs, hero=False, clearOutput=True, 
                                    clearOutputFilters=asset_filters, forceImageSequence=True)
    logger.info('Output version: {}'.format(publishedImages))

    # --- publsih task output
    # design/publ/type/assetName/task/hero/output
    # publishedHeroImages P:/Hanuman/design/publ/prop/testProp/color/hero/output/testProp_color.hero.jpg
    outputHeroImgPath = asset.path.output_hero_img().abs_path()
    publishedHeroImages = publish_images(asset, outputHeroImgPath, inputImgs, hero=True, clearOutput=True, 
                                    clearOutputFilters=asset_filters, forceImageSequence=True)
    logger.info('Output hero: {}'.format(publishedHeroImages))

    # --- publish asset hero
    # design/publ/type/assetName/hero
    # outputAssetHero P:/Hanuman/design/publ/prop/testProp/hero
    outputAssetHero = asset.path.scheme(key='publishHeroPath').abs_path()
    # needs to add clearOutputFilters to only clear out files with _process name in it
    heroImages = publish_images(asset, 
                outputAssetHero, 
                inputImgs, 
                hero=True, 
                clearOutput=True, 
                clearOutputFilters=asset_filters, 
                forceImageSequence=True)
    logger.info('Asset hero: {}'.format(heroImages))


    # --- publishDaily
    # daily/design/date
    daily_path = daily.get_path(asset)
    # needs to clear old stuff within that daily date folder first
    if os.path.exists(daily_path):
        file_utils.emptyDir(daily_path, filters=asset_filters)
    daily.submit(asset, publishedHeroImages)
    logger.info('Submit daily: {}'.format(daily_path))

    # --- publish _artbook
    # design/publ/_artbook/type
    # outputArtbook P:/Hanuman/design/publ/_artbook/prop
    outputArtbook = asset.path.hero().abs_path()
    artbook_images = publish_images(asset, 
                    outputArtbook, 
                    inputImgs, 
                    hero=True, 
                    clearOutput=True, 
                    clearOutputFilters=asset_filters, 
                    forceImageSequence=True)
    logger.info('Artbook images: {}'.format(artbook_images))

    return (publishedImages, publishedHeroImages)

def design_notification(asset, taskEntity, userEntity, versionResult, description):
    triggerStep = 'Model'
    triggerTaskName = 'Model'
    status = versionResult.get('sg_status_list')
    taskFromId = sg_process.get_task_from_id(taskEntity['id'])
    if status == 'apr':
        taskResult = [sg_process.get_one_task_by_step(taskEntity['entity'], triggerStep, triggerTaskName)]
        taskResult.append(taskFromId)
    elif status == 'rev':
        taskResult = [taskFromId]
    emails = noti_module_cliq.get_notification_data(taskResult, taskEntity, description, userEntity)

    # get image
    imgurl = sg_process.get_media(versionResult)
    if '.mov' in imgurl:
        imgurl = sg_process.get_media_thumbnail(versionResult)
    if 'thumbnail_pending' in imgurl:
        imgurl = 'https://previewengine-accl.zoho.com/image/WD/54mxc9836fa8a51d545eab56f79f735e2029c?width=2046&height=1536'
    # print imgurl
    urlImage = [imgurl]

    noti_module_cliq.send_notification(emails, taskEntity, urlImage=urlImage)

def design_mattepaint_notification(asset, taskEntity, userEntity, versionResult, description):
    taskEntity['entity']['type'] = 'shot'
    status = versionResult.get('sg_status_list')
    taskFromId = sg_process.get_task_from_id(taskEntity['id'])
    taskResult = []
    if status == 'apr':
        lightTrigger = {'step':{'name':'light'}, 'task_assignees':[{'name':'-'}], 'project':taskEntity['project'], 'sg_status_list':'def' }
        compTrigger = {'step':{'name':'comp'}, 'task_assignees':[{'name':'-'}], 'project':taskEntity['project'], 'sg_status_list':'def' }
        taskResult.append(lightTrigger)
        taskResult.append(compTrigger)
    elif status == 'rev':
        taskResult = [taskFromId]
    emails = noti_module_cliq.get_notification_data(taskResult, taskEntity, description, userEntity)

    # get image
    imgurl = sg_process.get_media(versionResult)
    if '.mov' in imgurl:
        imgurl = sg_process.get_media_thumbnail(versionResult)
    if 'thumbnail_pending' in imgurl:
        imgurl = 'https://previewengine-accl.zoho.com/image/WD/54mxc9836fa8a51d545eab56f79f735e2029c?width=2046&height=1536'
    # print imgurl
    urlImage = [imgurl]

    noti_module_cliq.send_notification(emails, taskEntity, urlImage=urlImage)



def published(src, dst):
    file_utils.copy(src, dst)
    if os.path.exists(dst):
        return dst
