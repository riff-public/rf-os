# -*- coding: utf-8 -*-
import os
import sys
import traceback
from collections import OrderedDict

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_utils import icon
from rf_utils.widget import project_widget
from rf_utils.widget import user_widget
from rf_utils.widget import entity_browse_widget
from rf_utils.widget import console_widget
from rf_utils.sg import sg_utils


class EditPublishUI(QtWidgets.QWidget):
    status = QtCore.Signal(str)
    def __init__(self, parent=None) :
        super(EditPublishUI, self).__init__(parent)
        self.setupUi()

    def setupUi(self):
        self.mainLayout = QtWidgets.QVBoxLayout()
        # title 
        self.titleLayout = QtWidgets.QHBoxLayout()
        self.toolTitle = QtWidgets.QLabel('Edit Publish Movie')
        # logo 
        self.logo = QtWidgets.QLabel()
        self.logo.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.logo.setPixmap(QtGui.QPixmap(icon.logo).scaled(64, 64, QtCore.Qt.KeepAspectRatio))

        self.titleLayout.addWidget(self.toolTitle)
        self.titleLayout.addWidget(self.logo)

        # user 
        self.userLayout = QtWidgets.QHBoxLayout()
        self.userSpacer = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum) 
        self.userWidget = user_widget.UserComboBox()
        self.userLayout.addItem(self.userSpacer)
        self.userLayout.addWidget(self.userWidget)
        self.userLayout.setStretch(0, 4)
        self.userLayout.setStretch(1, 1)

        # body layout 
        self.bodyLayout = QtWidgets.QHBoxLayout()
        self.sideLayout = QtWidgets.QVBoxLayout()

        # project 
        self.projectLayout = QtWidgets.QGridLayout()
        self.projectWidget = project_widget.SGProjectComboBox(sg=sg_utils.sg)
        self.projectWidget.allLayout.setSpacing(4)
        self.projectLayout.addWidget(self.projectWidget, 0, 0, 1, 1)

        # episode 
        self.sgNavWidget = entity_browse_widget.SGSceneNavigation(sg=sg_utils.sg, shotFilter=True)
        self.sgNavWidget.shotExtraFilter = ['sg_shot_type', 'is', 'Sequence']
        self.processLabel = QtWidgets.QLabel('Process : ')
        self.processWidget = QtWidgets.QComboBox()
        self.otherWidget = QtWidgets.QLineEdit()

        # this will be linked from sgNavWidget sequence filter 
        self.sequenceLabel = QtWidgets.QLabel('Sequence')
        self.sequenceWidget = CheckableComboBox()
        # hide shot widgets 
        self.sgNavWidget.shotLabel.setVisible(False)
        self.sgNavWidget.shotWidget.setVisible(False)
        self.sgNavWidget.sequenceFilter.setVisible(False)
        self.sgNavWidget.sequenceLabel.setVisible(False)
        self.sgNavWidget.sequenceFilter.currentIndexChanged.connect(self.sequence_changed)

        self.projectLayout.addWidget(self.sgNavWidget, 1, 0, 1, 1)
        self.projectLayout.addWidget(self.processLabel, 2, 0, 1, 1)
        self.projectLayout.addWidget(self.processWidget, 3, 0, 1, 1)
        self.projectLayout.addWidget(self.otherWidget, 4, 0, 1, 1)
        self.projectLayout.addWidget(self.sequenceLabel, 5, 0, 1, 1)
        self.projectLayout.addWidget(self.sequenceWidget, 6, 0, 1, 1)

        self.projectLayout.setColumnStretch(0, 1)
        self.projectLayout.setColumnStretch(1, 3)

        # spacer 
        self.spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum) 

        self.sideLayout.addLayout(self.projectLayout)
        self.sideLayout.addItem(self.spacerItem)

        self.sideLayout.setStretch(0, 1)
        self.sideLayout.setStretch(1, 4)

        # publish file 
        self.tabWidget = QtWidgets.QTabWidget()

        self.publishFrame = QtWidgets.QFrame()
        self.publishFrame.setFrameShape(QtWidgets.QFrame.Box)
        self.publishFrame.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.tabWidget.addTab(self.publishFrame, 'Publish')

        self.publishLayout = QtWidgets.QVBoxLayout(self.publishFrame)
        
        self.movFileWidget = DroppedFile('- Drop Movie -')
        self.movFileWidget.button.setFlat(True)
        self.movFileWidget.button.setMinimumSize(QtCore.QSize(0, 160))
        
        self.projectFileWidget = DroppedFile('- Drop Project -')
        self.projectFileWidget.button.setFlat(True)
        self.projectFileWidget.button.setMinimumSize(QtCore.QSize(0, 160))

        self.descpiptionLabel = QtWidgets.QLabel('Description : ')
        self.descriptionWidget = QtWidgets.QPlainTextEdit()

        self.publishLayout.addWidget(self.movFileWidget)
        self.publishLayout.addWidget(self.projectFileWidget)
        self.publishLayout.addWidget(self.descpiptionLabel)
        self.publishLayout.addWidget(self.descriptionWidget)


        self.bodyLayout.addLayout(self.sideLayout)
        self.bodyLayout.addWidget(self.tabWidget)
        self.bodyLayout.setStretch(0, 0)
        self.bodyLayout.setStretch(1, 2)

        # console 
        self.summaryWidget = QtWidgets.QWidget()
        self.tabWidget.addTab(self.summaryWidget, 'Summary')
        self.consoleLayout = QtWidgets.QVBoxLayout(self.summaryWidget)
        self.console = console_widget.Console()
        self.consoleLayout.addWidget(self.console)

        # button 
        self.buttonLayout = QtWidgets.QHBoxLayout()
        self.confirmButton = QtWidgets.QPushButton('Confirm')
        self.confirmButton.setMinimumSize(QtCore.QSize(0, 40))
        self.publishButton = QtWidgets.QPushButton('Publish')
        self.publishButton.setMinimumSize(QtCore.QSize(0, 40))
        self.buttonSpacer = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum) 
        
        self.buttonLayout.addItem(self.buttonSpacer)
        self.buttonLayout.addWidget(self.confirmButton)
        self.buttonLayout.addWidget(self.publishButton)

        self.buttonLayout.setStretch(0, 1)
        self.buttonLayout.setStretch(1, 1)
        self.buttonLayout.setStretch(2, 1)
        
        # spacer 
        self.mainSpacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum) 
        

        # mainLayout
        self.mainLayout.addLayout(self.titleLayout)
        self.mainLayout.addLayout(self.userLayout)
        self.line(self.mainLayout)
        self.mainLayout.addLayout(self.bodyLayout)
        self.mainLayout.addItem(self.mainSpacerItem)
        self.mainLayout.addLayout(self.buttonLayout)

        self.mainLayout.setStretch(0, 0)
        self.mainLayout.setStretch(1, 0)
        self.mainLayout.setStretch(2, 0)
        self.mainLayout.setStretch(3, 0)
        self.mainLayout.setStretch(4, 1)
        self.mainLayout.setStretch(5, 0)

        self.setLayout(self.mainLayout)

    def line(self, layout, direction='h', parent=None):
        """ line """
        line = QtWidgets.QFrame(parent)
        line.setFrameShape(QtWidgets.QFrame.HLine) if direction == 'h' else line.setFrameShape(QtWidgets.QFrame.VLine)
        line.setFrameShadow(QtWidgets.QFrame.Sunken)
        layout.addWidget(line)
        return line

    def sequence_changed(self, row): 
        allItems = [self.sgNavWidget.sequenceFilter.itemText(a) for a in range(self.sgNavWidget.sequenceFilter.count())]
        self.sequenceWidget.clear()

        for index, item in enumerate(allItems): 
            self.sequenceWidget.addItem(item)
            item = self.sequenceWidget.model().item(index, 0)
            item.setCheckState(QtCore.Qt.Unchecked)


class DroppedButton(QtWidgets.QPushButton):
    """docstring for DroppedButton"""
    dropped = QtCore.Signal(object)
    def __init__(self, *args, **kwargs):
        super(DroppedButton, self).__init__(*args, **kwargs)
        self.setAcceptDrops(True)
        
    def dragEnterEvent(self, event):
            if event.mimeData().hasUrls:
                event.accept()
            else:
                event.ignore()

    def dragMoveEvent(self, event):
        if event.mimeData().hasUrls:
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
        else:
        event.ignore()

    def dropEvent(self, event):
        if event.mimeData().hasUrls:
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
            links = []
            for url in event.mimeData().urls():
                links.append(str(url.toLocalFile()))
            self.dropped.emit(links[-1])
        else:
            event.ignore()


class DroppedFile(QtWidgets.QWidget):
    """docstring for DroppedFile"""
    dropped = QtCore.Signal(object)

    def __init__(self, label='', parent=None):
        super(DroppedFile, self).__init__(parent=parent)
        self.layout = QtWidgets.QVBoxLayout()
        self.button = DroppedButton(label, parent=parent)
        self.layout.addWidget(self.button)
        self.layout.setSpacing(0)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.layout)
        self.button.dropped.connect(self.preview)
        self.allowExtension = []
        self.path = ''
        self.moduleDir = os.path.dirname(__file__).replace('\\', '/')
        self.okIcon = '{}/icons/ok_icon.png'.format(self.moduleDir)
        self.failIcon = '{}/icons/fail_icon.png'.format(self.moduleDir)

    def preview(self, url): 
        basename, ext = os.path.splitext(url)
        allow = True
        if self.allowExtension: 
            allow = True if ext in self.allowExtension else False 

        if allow: 
            self.path = url 
            self.button.setIcon(QtGui.QIcon(self.okIcon))
            self.button.setText('OK')
            self.button.setToolTip(url)
            # QtGui.QPixmap(displayPath).scaled(60, 60, QtCore.Qt.KeepAspectRatio))
            print self.okIcon, os.path.exists(self.okIcon)
            self.dropped.emit(url)
        else: 
            self.button.setIcon(QtGui.QIcon(self.failIcon))
            self.button.setText('Accept only {}'.format(', '.join(self.allowExtension)))
        
        self.button.setIconSize(QtCore.QSize(64, 64))


class CheckableComboBox(QtWidgets.QComboBox):
    itemChanged = QtCore.Signal(object)

    def __init__(self, parent=None):
        super(CheckableComboBox, self).__init__(parent)
        self.view().pressed.connect(self.handleItemPressed)
        self._changed = False

    def handleItemPressed(self, index):
        item = self.model().itemFromIndex(index)
        if item.checkState() == QtCore.Qt.Checked:
            item.setCheckState(QtCore.Qt.Unchecked)
        else:
            item.setCheckState(QtCore.Qt.Checked)
        self._changed = True
        self.itemChanged.emit(index)

    def hidePopup(self):
        if not self._changed:
            super(CheckableComboBox, self).hidePopup()
        self._changed = False

    def itemChecked(self, index):
        item = self.model().item(index, self.modelColumn())
        return item.checkState() == QtCore.Qt.Checked

    def setItemChecked(self, index, checked=True):
        item = self.model().item(index, self.modelColumn())
        if checked:
            item.setCheckState(QtCore.Qt.Checked)
        else:
            item.setCheckState(QtCore.Qt.Unchecked)

    def checkedItems(self):
        checkedItems = []
        for index in range(self.count()):
            item = self.model().item(index, 0)
            if item.checkState() == QtCore.Qt.Checked:
                checkedItems.append(item)
        return checkedItems

# display a frame
# from PySide2 import QtWidgets, QtGui, QtCore
# import cv2
# import numpy
# label = QtWidgets.QLabel()
# video_file = 'P:/Hanuman/scene/work/mv/hnm_mv_q0010_s0010/anim/main/output/hnm_mv_q0010_s0010_anim_main.v002.mov'
# vid = cv2.VideoCapture(video_file)
# read_flag, frame = vid.read()
# rgbImage = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
# h, w, ch = rgbImage.shape
# bytesPerLine = ch * w
# convertToQtFormat = QtGui.QImage(rgbImage.data, w, h, bytesPerLine, QtGui.QImage.Format_RGB888)
# p = convertToQtFormat.scaled(640, 480, QtCore.Qt.KeepAspectRatio)

# label.setPixmap(QtGui.QPixmap.fromImage(p))           
# label.show()
        