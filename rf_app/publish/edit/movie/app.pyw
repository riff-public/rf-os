# -*- coding: utf-8 -*-
# v.0.0.1 polytag switcher
_title = 'Riff Edit Publish'
_version = 'v.0.0.1'
_des = 'Wip'
uiName = 'EditPublishUI'

import os
import sys
import traceback
from collections import OrderedDict
from datetime import datetime
import getpass
import rf_config as config
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui
import logging
from rf_utils import log_utils
from rf_utils import icon
from rf_utils.widget import project_widget
from rf_utils.ui import stylesheet
from rf_utils import thread_pool
from rf_utils.context import context_info
from . import ui
from . import edit_publish
reload(ui)
reload(edit_publish)

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)
user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)


class Setting: 
    processes = ['-', 'all_edit', 'animatic_edit', 'layout_edit', 'anim_edit', 'comp_edit', 'finalcam_edit', 'setdress_edit', 'others']
    processKey = 'others'
    processAll = 'all_edit'


class EditPublish(QtWidgets.QMainWindow):
    status = QtCore.Signal(str)
    def __init__(self, parent=None) :
        super(EditPublish, self).__init__(parent)
        self.ui = ui.EditPublishUI()
        self.setCentralWidget(self.ui)
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.setObjectName(uiName)
        self.resize(700, 400)

        # initialized
        self.threadpool = QtCore.QThreadPool()
        self.threadpool.setMaxThreadCount(1)
        self.cmdDict = dict()
        self.init_signals()
        self.init_functions()
        self.set_default()

    def set_default(self): 
        self.ui.movFileWidget.allowExtension = ['.mov', '.mp4']
        self.ui.projectFileWidget.allowExtension = ['.prproj']
        self.ui.otherWidget.setVisible(False)

    def init_signals(self): 
        self.ui.projectWidget.projectChanged.connect(self.project_changed)
        self.ui.processWidget.currentIndexChanged.connect(self.process_changed)
        self.ui.sequenceWidget.itemChanged.connect(self.item_checked)
        self.ui.confirmButton.clicked.connect(self.pre_publish)
        self.ui.movFileWidget.dropped.connect(self.check_allow_publish)
        self.ui.projectFileWidget.dropped.connect(self.check_allow_publish)
        self.ui.descriptionWidget.textChanged.connect(self.check_allow_publish)
        self.ui.otherWidget.textChanged.connect(self.check_allow_publish)
        self.ui.publishButton.clicked.connect(self.publish)

    def project_changed(self, projectEntity): 
        self.ui.sgNavWidget.list_episodes(projectEntity)

    def process_changed(self, index): 
        current = self.ui.processWidget.itemText(index)
        showOption = True if current == Setting.processKey else False 
        self.ui.otherWidget.setVisible(showOption)

        showSequence = True if not current == Setting.processAll else False 
        self.ui.sequenceWidget.setVisible(showSequence)
        self.ui.sequenceLabel.setVisible(showSequence)
        self.check_allow_publish()

    def item_checked(self, index): 
        items = self.ui.sequenceWidget.checkedItems()
        checkedSeq = [a.text() for a in items]
        self.check_allow_publish()

    def init_functions(self): 
        self.ui.projectWidget.emit_project_changed()
        self.ui.processWidget.addItems(Setting.processes)

    def get_process(self): 
        process = self.ui.processWidget.currentText()
        if not process == '-': 
            if process == Setting.processKey: 
                process = self.ui.otherWidget.text()
            return process
        return ''

    def check_allow_publish(self): 
        self.ui.confirmButton.setEnabled(False)
        self.ui.publishButton.setEnabled(False)
        items = self.ui.sequenceWidget.checkedItems()
        process = self.get_process()
        description = self.ui.descriptionWidget.toPlainText()
        if process == Setting.processAll: 
            items = True

        if items and self.ui.movFileWidget.path and self.ui.projectFileWidget.path and description and process: 
            self.ui.confirmButton.setEnabled(True)
        else: 
            print 'process or sequencee or paths or description are not set'

    def pre_publish(self): 
        # collect 
        projectEntity = self.ui.projectWidget.current_item()
        projectCode = projectEntity.get('sg_project_code')
        project = projectEntity.get('name')
        process = self.get_process()

        userEntity = self.ui.userWidget.data()
        user = userEntity.get('name')

        episode = self.ui.sgNavWidget.current_episode()
        items = self.ui.sequenceWidget.checkedItems()
        sequences = [a.text() for a in items] if not process == Setting.processAll else ['sequence']
        movFile = self.ui.movFileWidget.path 
        workspaceFile = self.ui.projectFileWidget.path 
        description = self.ui.descriptionWidget.toPlainText()
        richDescription = edit_publish.report_string(project, episode, sequences, process, movFile, description, user)

        # init commands string        
        self.cmdDict = edit_publish.summary(project, projectCode, episode, process, sequences, movFile, workspaceFile, description, richDescription, userEntity)
        reportString = edit_publish.report_string(project, episode, sequences, process, movFile, description, user, self.cmdDict)
        
        # set summary 
        # self.ui.console.setPlainText(reportString)
        self.update_console((reportString, 'info'))

        # ui handling
        self.ui.tabWidget.setCurrentIndex(1)
        self.ui.publishButton.setEnabled(True)
        self.ui.confirmButton.setEnabled(False)

    def publish(self): 
        """ publish from command dict "self.cmdDict" """ 
        self.start = datetime.now()
        worker = thread_pool.Worker(edit_publish.publish_from_command, self.cmdDict)
        worker.kwargs['loop_callback'] = worker.signals.loopResult
        worker.signals.loopResult.connect(self.update_console)
        worker.signals.result.connect(self.publish_finished)
        self.threadpool.start(worker)
        self.ui.publishButton.setEnabled(False)
        self.update_console(('Start publishing ...', 'complete'))
        self.update_console(('----------------------------------------------', 'complete'))

    def publish_finished(self, result): 
        finishedTime = datetime.now() - self.start
        fileResults, errorResults = result 
        texts = ['----------------------------------------']

        if not errorResults: 
            displayOutput = ['{} - {}'.format(title, path) for title, path in fileResults]
            texts.append('Publish completed!!')
            texts.append('See output')
            texts.append('<br>'.join(displayOutput))
            textStr = '<br>'.join(texts)
            self.update_console((textStr, 'complete'))
        
        else: 
            texts.append('Complete with error')
            texts.append(errorResults)
            textStr = '<br>'.join(texts)
            self.update_console((textStr, 'error'))

        texts = ['=================================']
        texts.append('Finished in %s' % finishedTime)
        textStr = '<br>'.join(texts)
        self.update_console((textStr, 'complete'))


    def update_console(self, result): 
        text, resultType = result
        if resultType == 'info': 
            logger.info(text)
            self.ui.console.append(text)
        elif resultType == 'error': 
            logger.error(text)
            self.ui.console.error(text)
        elif resultType == 'warning': 
            logger.warning(text)
            self.ui.console.warning(text)
        elif resultType == 'debug': 
            logger.debug(text)
            self.ui.console.debug(text)
        elif resultType == 'complete': 
            logger.info(text)
            self.ui.console.complete(text)
        elif resultType == 'title': 
            logger.info(text)
            self.ui.console.title(text)

        # self.ui.console.appendPlainText(str(result))



def show():
    logger.info('Run in standalone\n')
    app = QtWidgets.QApplication.instance()
    if not app:
        app = QtWidgets.QApplication(sys.argv)
    myApp = EditPublish()
    myApp.show()
    stylesheet.set_default(app)
    sys.exit(app.exec_())
