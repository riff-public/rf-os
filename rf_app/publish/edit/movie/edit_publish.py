# -*- coding: utf-8 -*-
import os 
import sys 
import re
import traceback
import logging
from rf_utils.context import context_info
from rf_utils import file_utils
from rf_utils import daily
from datetime import datetime
from collections import OrderedDict

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())


def summary(project, projectCode, episode, process, sequences, movFile, workspaceFile, description, textDescription, userEntity): 
    cmdDict = OrderedDict()
    sequenceStr = '-'.join(sequences)
    # setup context 
    context = context_info.Context()
    context.update(project=project, 
        entityType='scene', 
        entityGrp=episode, 
        process=process, 
        entityParent=sequenceStr, 
        projectCode=projectCode,
        step='edit')
    scene = context_info.ContextPathInfo(context)

    # publish path 
    editPath, version = os.path.split(scene.path.scheme(key='editPublishPath').abs_path())
    publishVersion = find_version(editPath)
    scene.context.update(publishVersion=publishVersion)
    publishPath = scene.path.scheme(key='editPublishPath').abs_path()

    # mov files
    publishMovHeroFile = scene.output_name(outputKey='editMov', hero=True)
    publishMovVersionFile = scene.output_name(outputKey='editMov', hero=False)
    publishMovVersionFilePath = '{}/{}'.format(publishPath, publishMovVersionFile)

    basename, ext = os.path.splitext(publishMovHeroFile)

    # workspace files 
    publishWsHeroFile = publishMovHeroFile.replace(ext, '.prproj')
    publishWsVersionFile = publishMovVersionFile.replace(ext, '.prproj')
    publishWsVersionFilePath = '{}/{}'.format(publishPath, publishWsVersionFile)

    # description files 
    publishDescriptionHeroFile = publishMovHeroFile.replace(ext, '.txt')
    publishDescriptionVersionFile = publishMovVersionFile.replace(ext, '.txt')
    publishDescriptionVersionFilePath = '{}/{}'.format(publishPath, publishDescriptionVersionFile)

    # daily path 
    dailyPath = daily.get_path(scene)
    dailyFilePath = '{}/{}'.format(dailyPath, publishMovVersionFile)

    cmdDict['copy'] = [
                            {'title': 'movFile', 'src': movFile, 'dst': publishMovVersionFilePath}, 
                            {'title': 'projectFile', 'src': workspaceFile, 'dst': publishWsVersionFilePath}, 
                            {'title': 'daily', 'src': movFile, 'dst': dailyFilePath}
                        ]
    cmdDict['write'] = {
                            'title': 'description', 
                            'src': textDescription, 
                            'dst': publishDescriptionVersionFilePath
                        }
    cmdDict['upload'] = {
                            'title': 'Shotgun upload', 
                            'context': scene, 
                            'src': publishMovVersionFilePath, 
                            'user': userEntity, 
                            'description': description
                        }

    return cmdDict


def report_string(project, episode, sequences, process, movFile, description, user, cmdDict=None):
    text = []
    text.append('Publish Summary')
    text.append('Date : {}'.format(datetime.now().strftime('%d %b %Y %H.%M.%S')))
    text.append('-----------------------------')
    text.append('Project : {}'.format(project))
    text.append('Episode : {}'.format(episode))
    text.append('Sequences : {}'.format(' - '.join(sequences)))
    text.append('Description : ')
    text.append(description)
    text.append('Submit by : {}'.format(user))

    text.append('-----------------------------')
    if cmdDict: 
        text.append('Tech details')
        text.append('-----------------------------')

        for key, value in cmdDict.items(): 
            text.append('{}'.format(key))

            if isinstance(value, list): 
                for item in value: 
                    title = item.get('title')
                    dst = item.get('dst')
                    if not dst: 
                        dst = item.get('src')
                    text.append('- {}'.format(title))
                    text.append('- - {}'.format(dst))
            
            if isinstance(value, dict): 
                title = value.get('title')
                dst = value.get('dst')
                if not dst: 
                    dst = value.get('src')
                text.append('- {}'.format(title))
                text.append('- - {}'.format(dst))

    textStr = '\n'.join(text)
    return textStr


def publish_from_command(cmdDict, loop_callback): 
    fileResults = []
    errorResults = []
    for key, values in cmdDict.items(): 
        if key == 'copy': 
            for i, item in enumerate(values): 
                try: 
                    title = item.get('title')
                    loop_callback.emit(('Copying {}...'.format(title), 'title'))
                    src = item.get('src')
                    dst = item.get('dst')
                    file_utils.copy(src, dst)
                    fileResults.append((title, dst))
                    loop_callback.emit(('Result: {}'.format(dst), 'info'))
                except Exception as e: 
                    errorMessage = manage_error(e, loop_callback)
                    errorResults.append(errorMessage)


        if key == 'write': 
            try: 
                item = values
                title = item.get('title')
                loop_callback.emit(('Write {}...'.format(title), 'title'))
                src = item.get('src')
                dst = item.get('dst')
                if not os.path.exists(os.path.dirname(dst)): 
                    os.makedirs(os.path.dirname(dst))
                
                f = open(dst, 'w')
                f.write(src.encode('utf-8'))
                f.close()
                loop_callback.emit(('Result: {}'.format(dst), 'info'))
            except Exception as e: 
                errorMessage = manage_error(e, loop_callback)
                errorResults.append(errorMessage)

        if key == 'upload': 
            try: 
                item = values
                title = item.get('title')
                loop_callback.emit(('Uploading {}...'.format(title), 'title'))
                entity = item.get('context')
                user = item.get('user')
                filepath = item.get('src')
                description = item['description']
                upload_to_shotgun(entity, filepath, description, user, loop_callback)
                loop_callback.emit(('Finish update Shotgun', 'info'))
            except Exception as e: 
                errorMessage = manage_error(e, loop_callback)
                errorResults.append(errorMessage)

    return (fileResults, errorResults)


def upload_to_shotgun(entity, filepath, description, user, callback): 
    from rf_utils.sg import sg_process 
    reload(sg_process)
    # find sequence 
    sequenceName = entity.sg_seq_name
    task = entity.process
    versionName = entity.output_name(outputKey='editMov', hero=False)

    callback.emit(('Create sequence name {} ...'.format(sequenceName), 'title'))

    # sg engity 
    projectEntity = sg_process.get_project_entity(entity.project)
    episodeEntity = sg_process.get_one_episode(entity.project, entity.episode)
    
    if not task == 'all': 
        seqeunceEntity = sg_process.create_sequence(projectEntity, episode=episodeEntity, sequenceName=sequenceName, shortCode=entity.sequence)
        linkEntity = seqeunceEntity
        entityType = 'Sequence'
    else: 
        linkEntity = episodeEntity
        entityType = 'Scene'

    callback.emit(('Create version name {} ...'.format(versionName), 'title'))
    callback.emit((str(linkEntity), 'debug'))
    versionEntity = sg_process.create_version(
                                name=versionName, 
                                project=projectEntity, 
                                entityType=entityType, 
                                entity=linkEntity, 
                                task=task, 
                                step=entity.step, 
                                user='', 
                                sg_status_list='rev', 
                                description=description
                                )
    callback.emit((str(versionEntity), 'debug'))

    callback.emit(('Uploading movie ...', 'title'))
    callback.emit((filepath, 'info'))
    callback.emit(('(This will take several minutes)', 'warning'))
    sg_process.upload_version_media(versionEntity, filepath)
    callback.emit(('Finish upload movie', 'info'))
    return True

def manage_error(e, callback): 
    error = traceback.format_exc()
    traceback.print_exc()
    logger.error(error)
    callback.emit((error, 'error'))
    return error


def find_version(path): 
    versions = file_utils.list_folder(path)
    newVersion = file_utils.find_next_version(versions)
    return newVersion


