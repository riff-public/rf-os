# v.0.0.1 Beta 

# app config 
_UINAME = 'Edit Shot Publish'
_VERSION = 'v.0.0.1'
_DES = ''
_TITLE = 'RF Edit Publish'

import sys
import os
from datetime import datetime
import logging
import traceback
import getpass
os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])


# import config
import rf_config as config
from rf_utils import project_info
from rf_utils.ui import load, stylesheet
from Qt import QtWidgets, QtCore, QtGui
from rf_utils.sg import sg_process
from rf_utils import thread_pool
from rf_utils import custom_exception
from rf_utils import log_utils


from . import ui
from . import app_config
from . import xml_model
from . import publish_model
from . import cmd

USER = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(_UINAME, USER)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)
_MODULEDIR = os.path.dirname(sys.modules[__name__].__file__)



class EditApp(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super(EditApp, self).__init__(parent=parent)
        self.ui = ui.Ui(app_config, sg_process.sg, parent)
        self.setCentralWidget(self.ui)
        self.setObjectName(_UINAME)

        # set variables
        self.xml = xml_model.XMLModel(self, app_config, sg=sg_process.sg)
        self.publish_model = publish_model.Publish()
        self.entity = None
        self.threadpool = QtCore.QThreadPool()
        self.threadpool.setMaxThreadCount(1)
        self.complete_tasks = 0
        self.total_tasks = 0
        self.failed_tasks = 0
        self.message = ''
        self.run = True

        # setup ui 
        self.setup_ui()
        self.init_signals()
        self.init_functions()

        # title
        self.setWindowTitle('%s %s-%s' % (_TITLE, _VERSION, _DES))
        self.resize(1200, 750)

    def setup_ui(self): 
        # setup table 
        # self.ui.xml_file.set_path("P:/Hanuman/edit/OUTPUT/XML/Pr2022/Ch09/Chapter09_Xml_pr2022.xml")
        # self.ui.mov_file.set_path("P:/Hanuman/edit/OUTPUT/XML/Pr2022/Ch09/Chapter09_Xml_pr2022.mp4")
        # self.ui.wav_file.set_path("P:/Hanuman/edit/OUTPUT/XML/Pr2022/Ch09/Chapter09_Xml_pr2022.wav")
        self.ui.xml.setColumnHidden(app_config.TableConfig.columns.index('Action'), True) 

    def init_signals(self): 
        self.ui.project.projectChanged.connect(self.ui.episode.set_episodes)
        self.ui.episode.episodeChanged.connect(self.episode_changed)
        self.ui.preview.clicked.connect(self.preview_xml)
        self.ui.publish.clicked.connect(self.publish)
        self.ui.cancel.clicked.connect(self.cancel)

        # message signals 
        self.ui.xml.actionChanged.connect(self.status_update)
        self.ui.xml.statusUpdated.connect(self.set_message)
        self.xml.statusUpdated.connect(self.set_message)

        # browse file 
        self.ui.xml_file.textChanged.connect(self.file_set)
        self.ui.mov_file.textChanged.connect(self.file_set)
        self.ui.wav_file.textChanged.connect(self.file_set)

    def init_functions(self): 
        self.ui.project.emit_project_changed()
        self.ui.episode.episode_changed()
        self.status_update()

    def episode_changed(self, episode): 
        project = self.ui.project.project_entity()
        process = self.ui.edit_type.current_text()
        step = app_config.Config.step
        if project and episode: 
            self.entity = self.xml.set_context(project, episode, step, process)
            self.publish_model.set_context(self.entity)
        self.ui.xml.clear()
        self.reset_counter()
        self.set_auto_browse()

    def file_set(self, path): 
        search_ext = {
            '.xml': self.ui.xml_file, 
            '.mp4': self.ui.mov_file, 
            '.wav': self.ui.wav_file}

        dirname, basename = os.path.split(path)
        name, extension = os.path.splitext(basename)

        for ext, widget in search_ext.items(): 
            if not ext == extension: 
                guess_file = '{}/{}{}'.format(dirname, name, ext)
                if os.path.exists(guess_file): 
                    if not widget.current_text(): 
                        widget.blockSignals(True)
                        widget.set_path(guess_file)
                        widget.blockSignals(False)

    def set_auto_browse(self): 
        if self.entity: 
            path = self.entity.path.scheme(key='editWorkspace').abs_path()
            self.ui.xml_file.path = path
            self.ui.mov_file.path = path
            self.ui.wav_file.path = path

    def set_message(self, text): 
        self.message = text
        self.status_update()

    def status_update(self): 
        self.ui.set_status('  Total {} / {} Task(s)   {}'.format(self.complete_tasks, self.ui.xml.task_count, self.message))
        # self.ui.set_status_color(app_config.Color.normal)

    def reset_counter(self): 
        self.complete_tasks = 0
        self.ui.xml.task_count = 0

    def reset_ui(self): 
        self.ui.publish.setEnabled(True)
        self.ui.cancel.setEnabled(True)
        self.ui.set_inprogress(False)
        self.ui.set_status_color(app_config.Color.normal)
        self.run = True

    def preview_xml(self): 
        project = self.ui.project.project_entity()
        episode = self.ui.episode.episode_entity()
        path = self.ui.xml_file.current_text()
        # set input to model object
        self.xml.mov = self.ui.mov_file.current_text()
        self.xml.wav = self.ui.wav_file.current_text()
        self.xml.user = self.ui.user.data()
        self.xml.project = project 
        if path: 
            self.xml.read(path)
            sequences = self.xml.get_sequences()
            worker = thread_pool.Worker(self.fetch_sg, project, episode, sequences)
            worker.signals.result.connect(self.fetch_sg_finished)
            self.threadpool.start(worker)

    def fetch_sg(self, project, episode, sequences): 
        shot_entities = self.xml.read_sg(project, episode, sequences)
        return shot_entities

    def fetch_sg_finished(self, shot_entities): 
        # self.xml.data_list is valid after self.xml.read() was called
        self.reset_counter()
        display_summary = self.xml.compare(self.xml.xml_list, shot_entities)
        self.ui.xml.set_data(display_summary)
        self.set_message('Ready')
        self.reset_ui()

    def cancel(self): 
        self.run = False
        self.ui.cancel.setEnabled(False)
        self.set_message('Canceling ...')

    def publish(self): 
        # collect selected row and actions 
        self.run = True
        data_list = self.ui.xml.get_checked_item_data()
        project = self.ui.project.current_project()
        episode = self.ui.episode.current_episode()
        mov = self.ui.mov_file.current_text()
        wav = self.ui.wav_file.current_text()
        xml = self.ui.xml_file.current_text()
        user = self.ui.user.data()
        ignore_error = self.ui.error_checkbox.isChecked()

        self.publish_model.set_output(mov, wav, xml)

        if mov and wav: 
            self.ui.publish.setEnabled(False)
            self.ui.set_inprogress(True)
            worker = thread_pool.Worker(self.publish_thread, project, episode, data_list, user, ignore_error)
            worker.kwargs['loop_callback'] = worker.signals.loopResult
            worker.signals.loopResult.connect(self.publish_progress)
            worker.signals.result.connect(self.publish_finished)
            self.threadpool.start(worker)
        else: 
            self.ui.set_status('Please browse mov and wav files')
            # self.ui.set_status_color(app_config.Color.error)

    def publish_thread(self, project, episode, data_list, user, ignore_error, loop_callback=None): 
        self.start_time = datetime.now()
        
        # publishing 
        loop_callback.emit((None, None, 'Publishing source files ...'))
        self.publish_model.callback = loop_callback
        self.publish_model.check_version()
        
        self.log = publish_model.Log(self.publish_model.entity)
        self.log.write('# User: {}'.format(user['name']))
        self.log.write('# Start publishing at {}'.format(self.start_time))

        # self.publish_model.demo = True
        self.publish_model.publish()

        # read modules
        funcs = list()
        errors = list()
        try: 
            modules = cmd.get_module()
        except Exception as e: 
            loop_callback.emit((None, None, 'Error: Please see log for detail'))
            error = traceback.format_exc()
            traceback.print_exc()
            logger.error(error)
            errors.append(error)
            self.log.write(str(error))
            return errors


        if self.run: 
            for data in data_list: 
            # data = {
            # 'sg_name': 'bk_ep1_q0010_s0010'
            # 'sg_working_duration': 894
            # 'color': [255, 255, 100]
            # 'type': 'shot'
            # 'cut_order': 1
            # 'sg_cut_order': 1
            # 'working_duration': 375}
                if self.run: 
                    entity = data['sg_name']
                    actions = data['action']
                    shot_item = data['data']

                    for action in actions: 
                        func_name = action.data['func']
                        kwargs = action.data['kwargs']
                        if func_name in modules: 
                            func = cmd.get_func(func_name)

                            if self.run: 
                                try: 
                                    loop_callback.emit((action, 'in-progress', 'Processing ...'))
                                    result = func.run(project, episode, shot_item, self.log, loop_callback, **kwargs)
                                    status = 'complete' if result else 'failed'
                                    message = 'Complete'
                                    self.complete_tasks += 1
                                except Exception as e: 
                                    error = traceback.format_exc()
                                    traceback.print_exc()
                                    logger.error(error)
                                    errors.append(error)
                                    self.failed_tasks += 1 
                                    status = 'failed'
                                    message = 'Eror'
                                    logger.error(e)
                                    logger.error('{} error, {}'.format(func_name, shot_item.sg_name))
                                    if not ignore_error: 
                                        return errors
                            else: 
                                break

                            loop_callback.emit((action, status, message))
                            # print('action', action.data)
                            self.status_update()
                        else: 
                            raise custom_exception.PipelineError('Module name "cmd/{}.py"" not found'.format(func_name))
                else: 
                    break 

        self.end_time = datetime.now() - self.start_time
        message = 'Finished in {}'.format(self.end_time)
        loop_callback.emit((None, None, message))
        return errors

    def publish_progress(self, data): 
        if data: 
            action, status, message = data
            if action and status: 
                action.set_status(status)
            if message: 
                self.set_message(message)

    def publish_finished(self, errors): 
        if self.run: 
            header = 'Publish complete' 
            color = app_config.Color.success
        else: 
            header = 'User cancelled' 
            color = app_config.Color.error

        self.log.write('# {} {} / {} Tasks in {}'.format(header, self.complete_tasks, self.ui.xml.task_count, self.end_time))
        if not errors: 
            self.set_message('{} in {}'.format(header, self.end_time))
            self.ui.set_status_color(color)
        else: 
            error_message = '# Finished with "{}"" tasks error'.format(len(errors))
            self.set_message(error_message)
            self.ui.set_status_color(app_config.Color.error)

            self.log.write(error_message)
            for error in errors: 
                self.log.write('# {}'.format(str(error)))

        self.ui.set_inprogress(False)


def show(): 
    app = QtWidgets.QApplication.instance()
    if not app:
        app = QtWidgets.QApplication(sys.argv)
        stylesheet.set_default(app)

    splash = show_splash()
    app.processEvents()

    edit_app = EditApp()
    edit_app.show()
    splash.finish(edit_app)

    sys.exit(app.exec_())


def show_splash(): 
    # init splash screen
    pixmap = QtGui.QPixmap('{}/icons/splash_image.png'.format(_MODULEDIR).replace('\\', '/'))
    splash = QtWidgets.QSplashScreen(pixmap)
    splash.setWindowFlags(QtCore.Qt.FramelessWindowHint | QtCore.Qt.WindowStaysOnTopHint)
    splash.show()
    splash.showMessage('{} {} is Loading, Please wait..'.format(_TITLE, _VERSION), 
                    QtCore.Qt.AlignBottom, 
                    QtCore.Qt.white)

    return splash




if __name__ == '__main__':
    show()
