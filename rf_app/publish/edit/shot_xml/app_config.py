# config for apps
from collections import OrderedDict

class TableConfig: 
    columns = [
                # 'Sequence', 
                # 'Shot', 
                'Name', 
                'Type', 
                'Duration', 
                'SG Duration', 
                'Cut Order', 
                'SG Cut Order', 
                'Action'
            ]
    actions = ['Action1', 'Action2', 'Action3']
    data_map = {
                'sequence': 'Sequence', 
                'shot': 'Shot', 
                'sg_name': 'Name', 
                'type': 'Type', 
                'working_duration': 'Duration', 
                'cut_order': 'Cut Order', 
                'sg_working_duration': 'SG Duration', 
                'sg_cut_order': 'SG Cut Order', 
                'action': 'Action'}

    column_size = {
                    'Name': 160, 
                    'Type': 60, 
                    'Duration': 100, 
                    'SG Duration': 100, 
                    'Action': 70, 
                    'Action1': 140, 
                    'Action2': 140, 
                    'Action3': 140}


class Color: 
    match = (100, 140, 255)
    new = (100, 255, 100)
    remove = (255, 100, 100)
    update = (255, 255, 100)

    match_light = (100, 140, 200)
    new_light = (140, 210, 140)
    remove_light = (255, 140, 140)
    update_light = (200, 200, 140)

    action_new = (100, 140, 100)
    action_update = (140, 140, 100)
    action_remove = (140, 100, 100)
    action_media = (100, 120, 140)

    cell_enable = (200, 200, 200)
    cell_disable = (120, 120, 120)

    error = (240, 60, 60)
    normal = (200, 200, 200)
    success = (60, 200, 60)


class SGConfig: 
    sg_fields = [
        'code', 'sg_duration', 'sg_shot_type', 
        'sg_cut_order', 'sg_cut_in', 'sg_cut_out', 
        'sg_working_duration', 'sg_sequence', 'sg_sequence_code', 
        'sg_shortcode', 'project', 'sg_episode', 'sg_status_list']


class Config: 
    step = 'edit'
    departments = ['animatic', 'edit_layout', 'edit_anim', 'edit_comp']


class XmlConfig: 
    sequence_track_name = 'sequence'
    shot_track_name = 'shot'
    all_suffix = 'all'
    track_relation = [sequence_track_name, shot_track_name]


class ActionLabel: 
    create = 'New'
    remove = 'Omit'
    update = 'Update'
    match = 'Match'


class Action: 
    create_shot = {'display': 'Create Shot', 'func': 'create_shot', 'kwargs': {}, 'color': Color.action_new}
    update_cut_order = {'display': 'Update Cut Order', 'func': 'update_cut_order', 'kwargs': {'action': 'update_cut_order'}, 'color': Color.action_update}
    update_duration = {'display': 'Update Duration', 'func': 'update_duration', 'kwargs': {'action': 'update_duration'}, 'color': Color.action_update}
    publish_media = {'display': 'Publish Media', 'func': 'publish_media', 'kwargs': {}, 'color': Color.action_media}
    remove_shot = {'display': 'Omit', 'func': 'remove_shot', 'kwargs': {'action': 'remove_shot'}, 'color': Color.action_remove}
    
    action_map = OrderedDict() 
    action_map['create_shot'] = create_shot
    action_map['update_cut_order'] = update_cut_order
    action_map['update_duration'] = update_duration
    action_map['publish_media'] = publish_media
    action_map['remove_shot'] = remove_shot

    condition = {
        ActionLabel.create: [create_shot, publish_media], 
        ActionLabel.remove: [remove_shot], 
        ActionLabel.update: [update_cut_order, update_duration, publish_media],
        ActionLabel.match: [publish_media]}


def reverse_key(dict_data): 
    new_dict = dict()
    for k, v in dict_data.items(): 
        new_dict[v] = k 
    return new_dict
