from collections import OrderedDict
from Qt import QtWidgets
from Qt import QtCore
import xml.dom.minidom
from rf_utils.context import context_info
from rf_utils import custom_exception
import logging 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())




    # for item in xml_data['shot']: 
    #     print item.get('combine_name')
    #     print item['start']
    #     print item['end']
    #     print item['working_duration']
    #     print '-----------------------------'
    # return xml_data

class ShotItem(object):
    """docstring for ShotItem"""
    def __init__(self):
        super(ShotItem, self).__init__()
        self.data = {'name': '',
                            'type': '',
                            'sequence': '', 
                            'sg_name': '', 
                            'project': '', 
                            'episode': '', 
                            'project_entity': None, 
                            'shot_entity': None, 
                            'id': int(), 
                            'working_duration': int(),
                            'cut_order': int(), 
                            'cut_in': int(),
                            'cut_out': int(),
                            'start': int(),
                            'end': int(), 
                            'sg_cut_order': '', 
                            'sg_working_duration': '', 
                            'action': '',
                            'context': None, 
                            'main_mov': str(), 
                            'main_wav': str(), 
                            'mov': str(), 
                            'wav': str(), 
                            'user': None, 
                            'main_xml': str(), 
                            'main': None, 
                            'error': 'No error', 
                            }

        self.data_color = {'name': None,
                            'type': None,
                            'sequence': None, 
                            'sg_name': None, 
                            'working_duration': None,
                            'cut_order': None, 
                            'cut_in': None,
                            'cut_out': None,
                            'start': None,
                            'end': None, 
                            'sg_cut_order': None, 
                            'sg_working_duration': None, 
                            'action': None
                            }

        self.actions = list()

    def update(self, **kwargs): 
        self.data.update(**kwargs)

    def update_color(self, **kwargs): 
        self.data_color.update(**kwargs)

    def set_row_color(self, color): 
        for k, v in self.data_color.items(): 
            self.data_color[k] = color

    def update_action(self, actions): 
        self.actions = actions

    @property 
    def name(self): 
        return self.data['name']

    @property 
    def id(self): 
        return self.data['id']

    @property 
    def sequence(self): 
        return self.data['sequence']

    @property 
    def sg_name(self): 
        return self.data['sg_name']

    @property 
    def type(self): 
        return self.data['type']

    @property 
    def sg_type(self): 
        return self.type.capitalize() if self.type else ''

    @property 
    def working_duration(self): 
        return self.data['working_duration']

    @property 
    def cut_order(self): 
        return self.data['cut_order']

    @property 
    def context(self): 
        return self.data['context']

    @property 
    def cut_in(self): 
        return self.data['cut_in']

    @property 
    def cut_out(self): 
        return self.data['cut_out']

    @property 
    def cut_in_display(self): 
        return 1

    @property 
    def cut_out_display(self): 
        return int(self.cut_in_display) + int(self.working_duration) - 1

    @property 
    def start(self): 
        return self.data['start']

    @property 
    def end(self): 
        return self.data['end']

    @property 
    def sg_cut_order(self): 
        return self.data['sg_cut_order']

    @property 
    def sg_working_duration(self): 
        return self.data['sg_working_duration']

    @property 
    def action(self): 
        return self.data['action']

    @property 
    def main_mov(self): 
        return self.data['main_mov']

    @property
    def main_wav(self): 
        return self.data['main_wav']

    @property 
    def mov(self): 
        return self.data['mov']

    @property
    def wav(self): 
        return self.data['wav']

    @property 
    def user(self): 
        return self.data['user']

    @property 
    def project_entity(self): 
        return self.data['project_entity']

    @property 
    def shot_entity(self): 
        return self.data['shot_entity']

    @property
    def main_xml(self): 
        return self.data['main_xml']

    @property
    def error(self): 
        return self.data['error']


class XMLModel(QtWidgets.QWidget):
    """docstring for XMLModel"""
    statusUpdated = QtCore.Signal(str)

    def __init__(self, main, config, sg=None):
        super(XMLModel, self).__init__()
        self.main = main
        self.config = config
        self.sg = sg 
        self.xml_list = list() 
        self.data_list = list()
        self.combine_shot_list = list()
        self.project = None
        self.path = None
        self.mov = None 
        self.wav = None
        self.xml_path = None
        self.user = None

    def read_xml_file(self, filepath): 
        # filepath = "P:/Hanuman/edit/OUTPUT/XML/Pr2022/Ch09/Chapter09_Xml_pr2022.xml"
        domtree = xml.dom.minidom.parse(filepath)
        collection = domtree.documentElement
        tracks = collection.getElementsByTagName("track")
        all_shot = 'all'

        # expect 2 tracks "sequence" and "shot"
        xml_list = list()

        for track in tracks:
            track_name = track.getAttribute('MZ.TrackName').lower()
            clipitems = track.getElementsByTagName("clipitem")
            
            for cut_order, clip in enumerate(clipitems):
                clip_name = clip.getElementsByTagName('name')[0]
                cut_in = clip.getElementsByTagName('in')[0]
                cut_out = clip.getElementsByTagName('out')[0]
                start = clip.getElementsByTagName('start')[0]
                end = clip.getElementsByTagName('end')[0]

                working_duration = int(end.childNodes[0].data) - int(start.childNodes[0].data)
                name = clip_name.childNodes[0].data.split('.')[0]

                item = ShotItem()
                item.update(
                    name=name, 
                    type=track_name, 
                    working_duration=working_duration, 
                    cut_order=cut_order + 1, 
                    cut_in=int(cut_in.childNodes[0].data), 
                    cut_out=int(cut_out.childNodes[0].data), 
                    start=int(start.childNodes[0].data), 
                    end=int(end.childNodes[0].data), 
                    xml=filepath, 
                    user=self.user), 


                xml_list.append(item)

        # set relation between shot and sequence 
        orphants = list()
        # sequence 
        sequences = [a for a in xml_list if a.type == self.config.XmlConfig.sequence_track_name]
        shots = [a for a in xml_list if a.type == self.config.XmlConfig.shot_track_name]
        for sequence in sequences: 
            # shot
            for shot in shots: 
                if shot.start >= sequence.start and shot.end <=sequence.end: 
                    shot.data.update({self.config.XmlConfig.sequence_track_name: sequence.name, self.config.XmlConfig.shot_track_name: shot.name})
                else: 
                    orphants.append(shot)

            sequence.data.update({self.config.XmlConfig.sequence_track_name: sequence.name, self.config.XmlConfig.shot_track_name: all_shot})
        
        return xml_list

    def read(self, path): 
        self.xml_path = path
        self.statusUpdated.emit('Reading xml ...')
        self.path = path
        self.xml_list = self.read_xml_file(path)
        self.xml_list = self.add_sg_info(self.xml_list)
        # self.scene.path = path
        # self.data_list = self.map_to_table(self.xml_list)
        return self.xml_list

    def add_sg_info(self, xml_list): 
        self.statusUpdated.emit('Adding context ...')
        for item in xml_list: 
            if item.type == self.config.XmlConfig.sequence_track_name: 
                sequence = item.name
                shot = self.config.XmlConfig.all_suffix
            elif item.type == self.config.XmlConfig.shot_track_name: 
                sequence = item.sequence
                shot = item.name
            else: 
                raise custom_exception.PipelineError(
                    'No layer name {} or {}'.format(self.config.XmlConfig.sequence_track_name, self.config.XmlConfig.shot_track_name))

            self.scene.context.update(entityParent=sequence, entityCode=shot, entity='none')
            self.scene.update_scene_context()
            item.update(sg_name=self.scene.name, context=self.scene.copy())
            item.update(main_mov=self.mov, main_wav=self.wav, project_entity=self.project)
        return xml_list

    def set_context(self, project, episode, step, process): 
        context = context_info.Context()
        context.update(project=project['name'], 
            projectCode=project['sg_project_code'], 
            entityType='scene', 
            entityGrp=episode['code'],
            entityParent='q',  
            entityCode='s', 
            step=step, 
            process=process)
        self.scene = context_info.ContextPathInfo(context=context)
        return self.scene

    def map_to_table(self, xml_list): 
        data_list = list()
        for row, item in enumerate(xml_list): 
            column_dict = dict()
            column_dict[0] = ('', item)
            # key from xml does not match column name, match it from config
            for key, value in item.data.items(): 
                column_display = self.config.TableConfig.data_map.get(key)
                if column_display: 
                    if column_display in self.config.TableConfig.columns: 
                        index = self.config.TableConfig.columns.index(column_display)
                        column_dict[index] = (value, {key: value})
            data_list.append(column_dict)

        return data_list

    def get_sequences(self): 
        sequences = list()
        if self.xml_list: 
            for item in self.xml_list: 
                if item.type == self.config.XmlConfig.sequence_track_name: 
                    sequences.append(item.name)
        else: 
            logger.warning('No xml data')

        return sequences

    def read_sg(self, project, episode, sequences): 
        self.statusUpdated.emit('Fetching shots and sequences ...')
        sequence_entities = self.get_sg_sequences(project, episode, sequences)
        shot_entities = self.get_sg_shots(project, episode, sequence_entities)
        return shot_entities

    def get_sg_sequences(self, project, episode, sequences):
        filters = [['project', 'is', project], ['sg_episodes', 'is', episode]]
        advancedFilter1 = {
                            "filter_operator": "any",
                            "filters": [['sg_shortcode', 'is', a] for a in sequences]
                        }
        filters.append(advancedFilter1)
        fields = ['code']
        sequence_entities = self.sg.find('Sequence', filters, fields)  
        return sequence_entities

    def get_sg_shots(self, project, episode, sequences): 
        filters = [['project', 'is', project], ['sg_episode', 'is', episode], ['sg_status_list', 'is_not', 'omt']]
        advancedFilter1 = {
                            "filter_operator": "any",
                            "filters": [['sg_sequence', 'is', a] for a in sequences]
                        }
        filters.append(advancedFilter1)
        # sg_fields = ['code', 'sg_duration', 'sg_cut_order', 'sg_cut_in', 'sg_cut_out', 'sg_working_duration', 'sg_sequence', 'sg_sequence_code', 'sg_shortcode']

        shot_entities = self.sg.find('Shot', filters, self.config.SGConfig.sg_fields)
        return shot_entities

    def compare(self, xml_list, shot_entities): 
        # compare shot match 
        sg_shot_dict = dict()
        xml_shot_dict = dict()
        combine_shot_list = list()
        shot_dict = dict()

        self.statusUpdated.emit('Calculating ...')

        for shot in shot_entities: 
            sg_shot_dict[shot['code']] = shot 

        for item in xml_list: 
            if item.type in [self.config.XmlConfig.sequence_track_name, self.config.XmlConfig.shot_track_name]: 
                xml_shot_dict[item.sg_name] = item 

        # combine shots 

        for item in xml_list: 
            sg_name = item.sg_name
            # New shot
            if not sg_name in sg_shot_dict.keys(): 
                item.update(action=self.config.ActionLabel.create)
                item.set_row_color(self.config.Color.new_light)
                item.update_color(action=self.config.Color.new)
            # yes update sg information
            else: 
                # Shot exists 
                shot = sg_shot_dict[sg_name]
                sg_cut_order = shot.get('sg_cut_order')
                sg_working_duration = shot.get('sg_working_duration')

                # check if this value exists 
                sg_cut_order = sg_cut_order if sg_cut_order else 0
                sg_working_duration = sg_working_duration if sg_working_duration else 0

                # Shot not match duration
                if not item.working_duration == int(sg_working_duration or 0): 
                    item.update(action=self.config.ActionLabel.update)
                    item.update_color(
                        action=self.config.Color.update, 
                        working_duration=self.config.Color.update_light, 
                        sg_working_duration=self.config.Color.update_light)
                
                # Shot not match cut order 
                elif not item.cut_order == int(sg_cut_order): 
                    item.update(action=self.config.ActionLabel.update)
                    item.update_color(
                        action=self.config.Color.update, 
                        cut_order=self.config.Color.update_light, 
                        sg_cut_order=self.config.Color.update_light)
                
                # Shot match duration and cut order 
                else: 
                    item.update(action=self.config.ActionLabel.match)
                    item.update_color(
                        action=self.config.Color.match)

                item.update(
                    sg_working_duration=sg_working_duration, 
                    sg_cut_order=sg_cut_order)
            combine_shot_list.append(sg_name)
            item.update_action(self.config.Action.condition[item.action])
            shot_dict[sg_name] = item


        for shot in shot_entities: 
            sg_name = shot['code']
            if not sg_name in xml_shot_dict.keys(): 
                item = ShotItem()
                item.update(
                    sg_name=sg_name, 
                    name=shot['sg_shortcode'], 
                    id=shot['id'], 
                    shot_entity=shot,
                    sequence=shot['sg_sequence_code'], 
                    shot=shot['sg_shortcode'], 
                    type=shot['sg_shot_type'].lower(), 
                    sg_working_duration=shot['sg_working_duration'], 
                    project=shot['project'], 
                    episode=shot['sg_episode'], 
                    sg_cut_order=shot['sg_cut_order'])
                item.update(action=self.config.ActionLabel.remove)
                item.set_row_color(self.config.Color.remove_light)
                item.update_color(action=self.config.Color.remove)
                item.update_action(self.config.Action.condition[item.action])
                combine_shot_list.append(sg_name)
                shot_dict[sg_name] = item
            else: 
                item = xml_shot_dict[sg_name]
                item.update(
                    id=shot['id'], 
                    project=shot['project'], 
                    episode=shot['sg_episode'], 
                    shot_entity=shot,
                    )

        combine_items = [shot_dict[a] for a in combine_shot_list]

        self.statusUpdated.emit('Finished')
        return combine_items

