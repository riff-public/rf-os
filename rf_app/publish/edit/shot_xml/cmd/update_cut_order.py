import os 
import sys 
from rf_utils.sg import sg_process
sg = sg_process.sg 


def run(project, episode, shot_item, log, loop_callback, **kwargs): 
    shot_id = shot_item.data['id']
    shot_item.cut_order
    # print 'update_cut_order', shot_id
    loop_callback.emit((None, None, 'Updating cut order {}'.format(shot_item.cut_order)))
    result = sg.update('Shot', shot_id, {'sg_cut_order': shot_item.cut_order})
    log.append_changes(
        name=shot_item.sg_name, 
        module=os.path.basename(__file__), 
        old_value=shot_item.sg_cut_order, 
        new_value=shot_item.cut_order)

    loop_callback.emit((None, None, 'Complete'))
    return result
