import os 
import sys 
from rf_utils.sg import sg_process
sg = sg_process.sg 


def run(project, episode, shot_item, log, loop_callback, **kwargs): 
    # print 'update_work_duration'
    # print shot_item.data.keys()
    loop_callback.emit((None, None, 'Updating duration {}'.format(shot_item.working_duration)))
    shot_id = shot_item.data['id']
    result = sg.update('Shot', shot_id, {'sg_working_duration': shot_item.working_duration, 'sg_cut_in': shot_item.cut_in_display, 'sg_cut_out': shot_item.cut_out_display})
    log.append_changes(
        name=shot_item.sg_name, 
        module=os.path.basename(__file__), 
        old_value=shot_item.sg_working_duration, 
        new_value=shot_item.working_duration)

    loop_callback.emit((None, None, 'Update duration complete'))
    return result
