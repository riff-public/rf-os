import os 
import sys 
from rf_utils.pipeline import create_scene
from rf_utils.sg import sg_process
sg = sg_process.sg 


def run(project, episode, shot_item, log, loop_callback, **kwargs): 

    sequence = shot_item.sequence
    shortcode = shot_item.name
    name = shot_item.sg_name

    loop_callback.emit((None, None, 'Creating Shot ...'))
    result = create_scene.create(
        project, name, episode, 
        sequence=sequence, 
        shortcode=shortcode,
        shotType=shot_item.sg_type, 
        link=True)

    shot = sg.find_one('Shot', [['project.Project.name', 'is', project], ['code', 'is', name]], ['code', 'id'])
    data = {'sg_working_duration': shot_item.working_duration, 'sg_cut_order': shot_item.cut_order, 'sg_cut_in': shot_item.cut_in_display, 'sg_cut_out': shot_item.cut_out_display}
    result = sg.update('Shot', shot['id'], data)
    log.append_changes(
        name=shot_item.sg_name, 
        module=os.path.basename(__file__), 
        old_value='', 
        new_value=shot['id'])
    loop_callback.emit((None, None, 'Create shot complete'))
    return result
