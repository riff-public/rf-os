import os 
import sys 
from rf_utils.sg import sg_process
sg = sg_process.sg 


def run(project, episode, shot_item, log, loop_callback, **kwargs): 
    shot_id = shot_item.data['id']
    shot_item.cut_order
    remove_status = 'omt'
    # print 'remove_shot', shot_id
    loop_callback.emit((None, None, 'Omit shot ...'))
    result = sg.update('Shot', shot_id, {'sg_status_list': remove_status})
    log.append_changes(
        name=shot_item.sg_name, 
        module=os.path.basename(__file__), 
        old_value=shot_item.shot_entity['sg_status_list'], 
        new_value=remove_status)

    loop_callback.emit((None, None, 'Omit shot compllete'))
    return result

