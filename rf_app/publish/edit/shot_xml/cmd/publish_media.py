import os 
import sys 
import tempfile
import shutil
from rf_app.publish.edit.shot_xml import ffmpeg_model
from rf_utils import admin
from rf_app.publish.asset import sg_hook
from rf_utils import register_sg
from rf_utils.sg import sg_process
sg = sg_process.sg 


def run(project, episode, shot_item, log, loop_callback, **kwargs): 
    # mov 
    # print 'publish media', shot_item.start, shot_item.end, shot_item.context
    mov_dir = shot_item.context.path.scheme(key='outputHeroImgPath').abs_path()
    mov_file = shot_item.context.publish_name(hero=True, ext='.mov')
    publish_mov = '{}/{}'.format(mov_dir, mov_file) 
    # publish mov P:/projectName/scene/publ/rnd2/pr_rnd2_q0010_s0010/edit/animatic/hero/outputMedia/pr_rnd2_q0010_s0010_edit_animatic.hero.mov

    # wav 
    # modify context to set process to sound
    modify_context = shot_item.context.copy()
    modify_context.context.update(process='sound')
    wav_dir = modify_context.path.scheme(key='publishHeroOutputPath').abs_path()
    wav_file = modify_context.publish_name(hero=True, ext='.wav')
    publish_wav = '{}/{}'.format(wav_dir, wav_file) 
    # publish wav P:/projectName/scene/publ/rnd2/pr_rnd2_q0010_s0010/edit/sound/hero/output/pr_rnd2_q0010_s0010_edit_sound.hero.wav
    publish_mov_wav = '{}/{}'.format(mov_dir, wav_file)

    shot_item.data.update(mov=publish_mov, wav=publish_mov_wav)

    fps = shot_item.context.projectInfo.render.fps()
    # 24

    loop_callback.emit((None, None, 'Processing {} movie from {} to {} ...'.format(shot_item.name, shot_item.start, shot_item.end)))

    mov = ffmpeg_model.FFMpeg(shot_item.main_mov , fps, shot_item.main_wav)
    mov.cut(shot_item.start , shot_item.end, publish_mov)
    mov.cut_audio(shot_item.start, shot_item.end, publish_wav)

    # copy to publish 
    loop_callback.emit((None, None, 'Copying to publish ...'))
    admin.copyfile(publish_wav, publish_mov_wav)

    # copy to sequence publish 
    if shot_item.type == 'sequence': 
        publish_to_sequence(shot_item.context.copy(), publish_mov)

    version = upload_to_sg(shot_item, publish_mov, loop_callback)
    loop_callback.emit((None, None, 'Publish media complete'))

    log.append_changes(
        name=shot_item.sg_name, 
        module=os.path.basename(__file__), 
        old_value='', 
        new_value=version['id'])


    return True


def upload_to_sg(shot_item, mov_file, loop_callback): 
    # upload to shotgun 
    entity = shot_item.context
    version = register_sg.get_task_versions(entity.project, entity.name, 'Shot', entity.process)
    entity.context.update(publishVersion=version)
    version_name, ext = os.path.splitext(entity.publish_name())
    filters = [
        ['project.Project.name', 'is', entity.project], 
        ['entity.Shot.code', 'is', entity.name], 
        ['content', 'is', entity.process]]
    fields = ['content']
    
    # find task 
    task_entity = sg.find_one('Task', filters, fields)
    status = 'apr'
    imgs = [mov_file]
    user_entity = shot_item.user
    description = ''
    loop_callback.emit((None, None, 'Create version ...'))
    version = sg_process.update_version(shot_item.project_entity,
                                            shot_item.shot_entity,
                                            task_entity,
                                            user_entity,
                                            version_name,
                                            status,
                                            description,
                                            [],
                                            versionType='Published',
                                            step=entity.step,
                                            workspaceFile=shot_item.main_xml,
                                            publishFile='',
                                            heroFile='',
                                            recommendedFile='',
                                            data='')

    loop_callback.emit((None, None, 'Uploading to ShotGrid (This might take a few minutes) ...'))
    result = sg_process.upload_version_media(version, shot_item.mov)
    return version

     # sg_hook.publish_version(shot_item.context, task_entity, status, imgs, user_entity, description)


def publish_to_sequence(entity, mov_file): 
    # publish to 
    # P:\projectName\edit\publ\rnd2\output\pr_rnd_q0010_all_edit_main.hero.mov
    entity.context.update(process='output', publishVersion='hero')
    path = entity.path.scheme(key='editPublishPath').abs_path()
    filename = entity.publish_name(hero=True, ext='.mov')
    dst = '{}/{}'.format(path, filename)
    if not os.path.exists(path): 
        os.makedirs(path)
    shutil.copy2(mov_file, dst)
    return True if os.path.exists(dst) else False


