from functools import partial 
from Qt import QtWidgets 
from Qt import QtGui 
from Qt import QtCore 

from rf_utils import icon


class XmlTable(QtWidgets.QTableWidget):
    """docstring for XmlTable"""
    actionChanged = QtCore.Signal(object)
    statusUpdated = QtCore.Signal(str)

    def __init__(self, config=None, parent=None):
        super(XmlTable, self).__init__(parent=parent)
        self.config = config 
        self.task_count = 0
        self.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)

        # setup table 
        self.set_column()
        # self.itemChanged.connect(self.item_selected)

    def clear(self): 
        while self.rowCount() > 0: 
            self.removeRow(0)

    def set_column(self): 
        columns = self.config.TableConfig.columns
        data_map = self.config.reverse_key(self.config.TableConfig.data_map)
        # insert select column 
        columns.insert(0, '')
        self.setColumnCount(len(columns) + len(self.config.TableConfig.actions))
        # self.setHorizontalHeaderLabels(columns)

        # myHeader = QCheckableHeader(QtCore.Qt.Horizontal, self)
        # self.setHorizontalHeader(myHeader)
        # myHeader.lisCheckboxes[0].clicked.connect(self.head_clicked)
        for i, display in enumerate(columns): 
            key = data_map.get(display) or display
            item = QtWidgets.QTableWidgetItem()
            item.setText(display)  
            item.setData(QtCore.Qt.UserRole, key)  
            self.setHorizontalHeaderItem(i, item)
            col_size = self.config.TableConfig.column_size.get(display)
            if col_size: 
                self.setColumnWidth(i, col_size)

        # funcs 
        for i, display in enumerate(self.config.TableConfig.actions): 
            col_index = i + len(columns)
            item = QtWidgets.QTableWidgetItem()
            item.setText(display)  
            self.setHorizontalHeaderItem(col_index, item)
            col_size = self.config.TableConfig.column_size.get(display)
            if col_size: 
                self.setColumnWidth(col_index, col_size)

    def head_clicked(self): 
        print('clicked')

    def add_data(self, xml_item): 
        # key from xml does not match column name, match it from config
        self.blockSignals(True)
        current_row = self.rowCount()
        self.insertRow(current_row)
        col_count = 0 

        # add checkbox selection 
        row_checkbox = QtWidgets.QCheckBox()
        row_checkbox.setChecked(True)
        row_checkbox.stateChanged.connect(partial(self.row_checked, current_row))
        self.setCellWidget(current_row, 0, row_checkbox)

        # add xml and sg data 
        for key, value in xml_item.data.items(): 
            column_display = self.config.TableConfig.data_map.get(key)
            if column_display: 
                if column_display in self.config.TableConfig.columns: 
                    column_index = self.config.TableConfig.columns.index(column_display)
                    color = xml_item.data_color.get(key) or self.config.Color.cell_enable
                    data = {key: value, 'color': color, 'data': xml_item}

                    item = QtWidgets.QTableWidgetItem()
                    item.setTextAlignment(QtCore.Qt.AlignHCenter)
                    item.setText(str(value))
                    item.setData(QtCore.Qt.UserRole, data)
                    self.setItem(current_row, column_index, item)
                    
                    if color: 
                        item.setBackground(QtGui.QColor(*color))

                    col_count += 1 

        # add actions  
        for i, action in enumerate(xml_item.actions): 
            col_index = i + col_count + 1
            color = action['color']
            # action_checkbox = QtWidgets.QCheckBox(action['display'])
            action_checkbox = ActionCheckBox()
            action_checkbox.setText(action['display'])
            action_checkbox.set_name(action['func'])
            action_checkbox.set_data(action)
            action_checkbox.setChecked(True)
            action_checkbox.set_color(color)
            action_checkbox.ready()
            action_checkbox.stateChanged.connect(partial(self.action_checked, current_row, col_index, color))
            self.setCellWidget(current_row, col_index, action_checkbox)
            self.task_count += 1

        self.blockSignals(False)

    def set_data(self, data_list): 
        self.clear() 
        for item in data_list: 
            self.add_data(item)
        self.resizeColumnToContents(0)

    def select_row(self, row, state): 
        checkbox = self.cellWidget(row, 0)
        checkbox.setChecked(state)

    def item_selected(self, item): 
        checked_row = self.get_checked_items()
        for row in range(self.rowCount()): 
            state = False 
            if row in checked_row: 
                state = True 
            self.enable_row(row, state)

    def row_checked(self, row, *args): 
        item = self.item(row, 0)
        checkbox = self.cellWidget(row, 0)
        color = self.config.Color.cell_disable
        
        for index in range(self.columnCount()): 
            item = self.item(row, index)
            widget = self.cellWidget(row, index)
            if item: 
                data = item.data(QtCore.Qt.UserRole)
                if checkbox.isChecked(): 
                    color = data['color'] or self.config.Color.cell_enable
                self.set_color(row, index, color)
            if widget: 
                if isinstance(widget, QtWidgets.QCheckBox): 
                    widget.setChecked(checkbox.isChecked())


    def action_checked(self, row, column, color, *args): 
        checkbox = self.cellWidget(row, column)
        bgcolor = color if checkbox.isChecked() else self.config.Color.cell_disable
        checkbox.set_color(bgcolor)
        if checkbox.isChecked(): 
            self.task_count += 1
        else: 
            self.task_count -= 1
        self.actionChanged.emit(checkbox)
        # checkbox.setStyleSheet('background-color: rgb(%s, %s, %s);' % (bgcolor[0], bgcolor[1], bgcolor[2]))

    def enable_row(self, row, state): 
        color = self.config.Color.cell_enable if state else self.config.Color.cell_disable
        for index in range(self.columnCount()): 
            self.set_color(row, index, color)

    def get_checked_items(self): 
        item_col = 0 
        checked_row = list() 

        for row in range(self.rowCount()): 
            if self.cellWidget(row, 0).isChecked(): 
                checked_row.append(row)

        return checked_row

    def set_color(self, row, column, color): 
        item = self.item(row, column)
        if item: 
            item.setBackground(QtGui.QColor(*color))

    def get_checked_item_data(self): 
        checked_row = self.get_checked_items()
        data_list = list()

        for row in checked_row: 
            data = self.get_data(row)
            data_list.append(data)
        return data_list

    def get_data(self, row): 
        col_count = self.columnCount()
        data = dict()

        for col in range(col_count): 
            item = self.item(row, col)
            if item: 
                item_data = item.data(QtCore.Qt.UserRole)
                item_data['action'] = self.get_checked_action_widget(row)
                data.update(item_data) if item_data else None
        return data

    def get_row_actions(self, row): 
        action_columns = list()
        checkboxs = list()
        columns = self.columnCount()
        for i in range(columns): 
            item = self.horizontalHeaderItem(i)
            if item.text() in self.config.TableConfig.actions: 
                action_columns.append(i)

        for column in action_columns: 
            checkbox = self.cellWidget(row, column)
            if isinstance(checkbox, ActionCheckBox): 
                checkboxs.append(checkbox)
        return checkboxs

    def get_checked_action_widget(self, row): 
        checkboxs = self.get_row_actions(row)
        checked_actions = list() 

        for checkbox in checkboxs: 
            if checkbox.isChecked(): 
                checked_actions.append(checkbox)

        return checked_actions


class ActionCheckBox(QtWidgets.QCheckBox):
    """docstring for ActionCheckBox"""
    def __init__(self, **kwargs):
        super(ActionCheckBox, self).__init__(**kwargs)
        self.data = None
        self.name = None
        
    def set_data(self, data): 
        self.data = data

    def set_color(self, color): 
        self.setStyleSheet('background-color: rgb(%s, %s, %s);' % (color[0], color[1], color[2]))

    def set_status(self, status): 
        if status == 'ready': 
            icon_path = icon.sgRdy 
        if status == 'in-progress': 
            icon_path = icon.sgIp
        if status == 'complete': 
            icon_path = icon.ok 
        if status == 'failed': 
            icon_path = icon.no 

        icon_widget = QtGui.QIcon()
        icon_widget.addPixmap(QtGui.QPixmap(icon_path), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.setIcon(icon_widget)

    def ready(self): 
        self.set_status('ready')

    def in_progress(self): 
        self.set_status('in-progress')

    def complete(self): 
        self.set_status('complete')

    def failed(self): 
        self.set_status('failed')

    def set_name(self, name): 
        self.name = name

    def name(self): 
        return self.name

        
class QCheckableHeader(QtWidgets.QHeaderView):

    def __init__(self, orientation, parent=None):
        QtWidgets.QHeaderView.__init__(self, orientation, parent)
        self.lisCheckboxes = []
        self.sectionCountChanged.connect(self.onSectionCountChanged)

    def paintSection(self, painter, rect, logicalIndex):
        painter.save()
        QtWidgets.QHeaderView.paintSection(self, painter, rect, logicalIndex)
        painter.restore()
        painter.save()
        painter.translate(rect.topLeft())

        option = QtWidgets.QStyleOptionButton()
        option.rect = QtCore.QRect(10, 10, 10, 10)
        if (len(self.lisCheckboxes) != self.count()):
            self.onSectionCountChanged(len(self.lisCheckboxes), self.count())

        if self.lisCheckboxes[logicalIndex]:
            option.state = QtWidgets.QStyle.State_On
        else:
            option.state = QtWidgets.QStyle.State_Off
        self.style().drawControl(QtWidgets.QStyle.CE_CheckBox, option, painter)
        painter.restore()

    def mousePressEvent(self, event):

        iIdx = self.logicalIndexAt(event.pos())
        self.lisCheckboxes[iIdx] = not self.lisCheckboxes[iIdx]
        self.updateSection(iIdx)
        QtWidgets.QHeaderView.mousePressEvent(self, event)

    @QtCore.Slot()
    def onSectionCountChanged(self, oldCount,  newCount):
        if newCount > oldCount:
            for i in range(newCount - oldCount):
                self.lisCheckboxes.append(False)
        else:
            self.lisCheckboxes = self.lisCheckboxes[0:newCount]
