import os
import sys 
import re
from datetime import datetime
from rf_utils.context import context_info 
from rf_utils import file_utils, admin


class Publish(object):
    """docstring for Publish"""
    def __init__(self, entity_context=None, callback=None):
        super(Publish, self).__init__()
        self.entity = entity_context
        self.callback = callback
        self.initial_version = 'v001'
        self.precheck = False
        self.mov = str()
        self.wav = str() 
        self.xml = str()
        self.demo = False
        self.publish_files = {'.mov': self.mov, '.wav': self.wav, '.xml': self.xml}

    def set_output(self, mov, wav, xml): 
        self.mov = mov 
        self.wav = wav 
        self.xml = xml
        self.publish_files = {'.mov': self.mov, '.wav': self.wav, '.xml': self.xml}

    def set_context(self, entity): 
        self.entity = entity.copy()
        self.entity.context.update(entityParent='all')

    def get_publish_workspace_path(self): 
        dirname = self.entity.path.scheme(key='editPublishPath').abs_path() 
        # dirname = P:/projectName/edit/publ/rnd2/animatic/none

        # publish_dir, version = os.path.split(dirname)
        return dirname 

    def get_latest_version(self): 
        publish_dir = self.get_publish_workspace_path()
        dirname, version = os.path.split(publish_dir)
        vers = file_utils.list_folder(dirname)
        print('latest version', dirname, vers)
        return vers[-1] if vers else None 

    def get_file(self, version, ext): 
        self.entity.context.update(publishVersion=version)
        # file_name = self.entity.publish_name(hero=False, ext=ext)
        file_name = self.entity.output_name(outputKey='editMov', hero=False, ext=ext)
        publish_file = '{}/{}'.format(self.get_publish_workspace_path(), file_name)
        return publish_file

    def check_version(self): 
        self.set_publish_version(self.entity)
        self.precheck = True

    def publish(self): 
        # set version 
        if not self.precheck: 
            self.check_version()

        for ext, src in self.publish_files.items(): 
            dst_dir = self.get_publish_workspace_path()
            file_name = self.entity.output_name(outputKey='editMov', hero=False, ext=ext)
            dst = '{}/{}'.format(dst_dir, file_name)
            if not os.path.exists(dst_dir): 
                admin.makedirs(dst_dir)
            
            print('copying ...{} to {}'.format(src, dst))
            self.print_out('Publishing ... to {}'.format(dst))
            self.copy(src, dst)
            print('Copy complete')

    def copy(self, src, dst): 
        if not self.demo: 
            admin.copyfile(src, dst)

    def set_publish_version(self, entity): 
        publish_version = self.get_latest_version()
        if not publish_version: 
            publish_version = self.initial_version
        else: 
            file_new = self.check_file_changes()
            if file_new: 
                publish_version = self.inicrement_version(publish_version)

        entity.context.update(publishVersion=publish_version)

    def inicrement_version(self, version): 
        result = re.findall(r'\d{3}', version)
        if result: 
            new_version = int(result[0]) + 1 
            return 'v%03d' % new_version

    def check_file_changes(self): 
        # check if publish is new 
        file_is_new = False
        version = self.get_latest_version()
        if version: 
            self.print_out('Checking source files ...')
            for ext, src in self.publish_files.items(): 
                file = self.get_file(version, ext)
                print('Checking ...')
                print(src, file)
                file_is_same = file_utils.is_file_same(src, file)
                print('file same {}'.format(file_is_same))

                if not file_is_same: 
                    file_is_new = True
                    return file_is_new
        return file_is_new  

    def print_out(self, message): 
        self.callback.emit((None, None, message))


class Log(object):
    """docstring for Log"""
    def __init__(self, entity):
        super(Log, self).__init__()
        self.entity = entity 
        self.log_file = self.get_log_file()
        
    def get_log_file(self): 
        date_string = datetime.now().strftime('%Y-%m-%d_%H%M')
        publish_dir = self.entity.path.scheme(key='editPublishPath').abs_path()
        logfile = '{}/log_changes_{}.txt'.format(publish_dir, date_string) 
        return logfile    

    def append_changes(self, name, module, old_value, new_value): 
        message = self.format(name, module, old_value, new_value)
        self.write(message)

    def write(self, message): 
        if not os.path.exists(os.path.dirname(self.log_file)): 
            os.makedirs(os.path.dirname(self.log_file))
        with open(self.log_file, 'a') as f:
            f.write("{}\n".format(message))

    def format(self, name, module, old_value, new_value): 
        log = {'name': name, 'module': module, 'old_value': old_value, 'new_value': new_value}
        return str(log)

        