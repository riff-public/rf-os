import os 
import sys 
import subprocess
from decimal import Decimal, ROUND_UP, ROUND_DOWN
import tempfile
import logging 
import shutil
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

core = '{}/core'.format(os.environ['RFSCRIPT'])
if not core in sys.path: 
    sys.path.append(core) 

import rf_config as config


# class Config: 
#     ffmpeg_exe = 'O:/Pipeline/lib/ffmpeg-20161227-0ff8c6b-win64-static/bin/ffmpeg.exe'


# start_frame_vdo = str(Decimal(str(int(animatic['start']) / float(fps))).quantize(Decimal('1e-4'), rounding=ROUND_DOWN))
# end_frame_vdo = str(Decimal(str((int(animatic['end']) - int(animatic['start'])) / float(fps))).quantize(Decimal('1e-4'), rounding=ROUND_DOWN))
# start_frame_wav = str(int(animatic['start']) / float(fps))
# end_frame_wav = str((int(animatic['end']) - int(animatic['start'])) / float(fps))


class FFMpeg(object):
    """docstring for FFMpeg"""
    def __init__(self, input_path, fps=None, audio_path=None):
        super(FFMpeg, self).__init__()
        self.input_path = input_path
        self.audio_path = audio_path
        self.output_path = str()
        self.start_frame = 0
        self.end_frame = 0
        self.fps = str(fps)
        self.tempdir = '{}/shot_xml'.format(tempfile.gettempdir())
        self.temp_video = '{}/video{}'.format(self.tempdir, os.path.splitext(input_path)[-1]).replace('\\', '/')
        self.temp_audio = '{}/audio{}'.format(self.tempdir, '.wav').replace('\\', '/')
        self.temp_combind = '{}/combine{}'.format(self.tempdir, os.path.splitext(input_path)[-1]).replace('\\', '/')

    def cut(self, start_frame, end_frame, output_path=None): 
        # cut video without audio 
        if self.clear_temp(): 
            self.cut_video(start_frame, end_frame, self.temp_video)
            self.cut_audio(start_frame, end_frame, self.temp_audio)
            self.merge_video_audio(self.temp_video, self.temp_audio, self.temp_combind)
            self.copy_file(self.temp_combind, output_path)

            return True if os.path.exists(output_path) else False

        else: 
            logger.error('{} cannot be deleted'.format(self.tempdir))

    def copy_file(self, src, dst): 
        if not os.path.exists(os.path.dirname(dst)): 
            os.makedirs(os.path.dirname(dst))
        shutil.copy2(src, dst)

    def clear_temp(self): 
        os.remove(self.temp_video) if os.path.exists(self.temp_video) else None
        os.remove(self.temp_audio) if os.path.exists(self.temp_audio) else None
        return all([not os.path.exists(self.temp_video), not os.path.exists(self.temp_audio)])

    def cut_video(self, start_frame, end_frame, output_path=None): 
        start_timecode = self.to_timecode(start_frame)
        end_timecode = self.to_timecode(end_frame - start_frame)
        self.output_path = output_path
        params = [
            config.Software.ffmpeg, 
            '-y', 
            '-ss', 
            start_timecode, 
            '-t', 
            end_timecode, 
            '-i', 
            self.input_path, 
            '-c:v',
            'libx264',
            '-crf',
            '24',
            '-profile:v',
            'baseline',
            '-level',
            '3.0',
            '-pix_fmt',
            'yuv420p',
            '-r',
            self.fps,
            '-an',
            self.output_path
            ]
        if not os.path.exists(os.path.dirname(output_path)): 
            os.makedirs(os.path.dirname(output_path))
        return subprocess.call(params)

    def cut_audio(self, start_frame, end_frame, output_path=None): 
        start_timecode = self.to_timecode(start_frame)
        end_timecode = self.to_timecode(end_frame - start_frame)
        params = [
            config.Software.ffmpeg,
            '-i',
            self.audio_path,
            '-y',
            '-ss',
            start_timecode,
            '-t',
            end_timecode,
            '-acodec',
            'pcm_s16le',
            # 'aac',
            '-ac',
            '2',
            '-r',
            self.fps,
            output_path
                ]
        if not os.path.exists(os.path.dirname(output_path)): 
            os.makedirs(os.path.dirname(output_path))
        return subprocess.call(params)

    def merge_video_audio(self, video_path, audio_path, output_path): 
        params = [
            config.Software.ffmpeg,
            '-y',
            '-i',
            video_path,
            '-i',
            audio_path,
            '-map', '0:v:0', '-map', '1:a:0',
            '-c:v', 'copy', '-c:a', 'aac',
            # 'copy',
            output_path
                ]
        if not os.path.exists(os.path.dirname(output_path)): 
            os.makedirs(os.path.dirname(output_path))
        return subprocess.call(params)

    def to_timecode(self, frame): 
        timecode = str(Decimal(str(int(frame) / float(self.fps))).quantize(Decimal('1e-4'), rounding=ROUND_DOWN))
        return timecode


def test(): 
    mov = FFMpeg('P:/Hanuman/edit/OUTPUT/XML/Pr2022/Ch09/Chapter09_Xml_pr2022.mp4', 24, 'P:/Hanuman/edit/OUTPUT/XML/Pr2022/Ch09/Chapter09_Xml_pr2022.wav')
    mov.cut(0, 375, 'P:/Bikey/_tmp/asset_farm/chanon.v/s0010.mp4')
    # mov.merge_video_audio(mov.temp_video, mov.temp_audio, 'P:/Bikey/_tmp/asset_farm/chanon.v/s0010.mp4')

# test()

"""        
subprocess.call([
                    Config.ffmpeg_exe,
                    '-y',
                    '-ss',
                    start_frame_vdo,
                    '-t',
                    end_frame_vdo,
                    '-i',
                    vdo_path,
                    '-c:v',
                    'libx264',
                    '-crf',
                    '24',
                    '-profile:v',
                    'baseline',
                    '-level',
                    '3.0',
                    '-pix_fmt',
                    'yuv420p',
                    '-r',
                    str(fps),
                    '-an',
                    output_no_wav_vdo_path
                ])
                subprocess.call([
                    Config.ffmpeg_exe,
                    '-i',
                    wav_path,
                    '-y',
                    '-ss',
                    start_frame_wav,
                    '-t',
                    end_frame_wav,
                    '-acodec',
                    'pcm_s16le',
                    '-ac',
                    '2',
                    '-r',
                    str(fps),
                    output_wav_path
                ])
                # merge vdo and wav
                copy2(output_wav_path, output_hero_wav_path)
                subprocess.call([
                    Config.ffmpeg_exe,
                    '-y',
                    '-i',
                    output_no_wav_vdo_path,
                    '-i',
                    output_wav_path,
                    '-c',
                    'copy',
                    output_vdo_path
                ])
"""