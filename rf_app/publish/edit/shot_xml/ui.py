from Qt import QtWidgets
from Qt import QtGui
from Qt import QtCore 
from functools import partial

from . import xml_widget
from rf_utils.widget import project_widget 
from rf_utils.widget import episode_widget 
from rf_utils.widget import user_widget
from rf_utils.widget import generic_widget as gen
from rf_utils.widget import browse_widget


class Ui(QtWidgets.QWidget):
    """docstring for Ui"""
    def __init__(self, config=None, sg=None, parent=None):
        super(Ui, self).__init__(parent=parent)
        self.config = config
        self.layout = QtWidgets.QVBoxLayout()
        self.header = QtWidgets.QHBoxLayout()
        self.lf_layout = QtWidgets.QVBoxLayout()
        self.rt_layout = QtWidgets.QVBoxLayout()
        self.file_layout = QtWidgets.QGridLayout()
        self.filter_layout = QtWidgets.QGridLayout()
        self.bottom_layout = QtWidgets.QGridLayout()
        self.button_layout = QtWidgets.QHBoxLayout()

        # widgets 
        self.user = user_widget.UserComboBox()
        self.project = project_widget.SGProjectComboBox(sg, layout=QtWidgets.QHBoxLayout())
        self.episode = episode_widget.SGEpisodeComboBox(sg, layout=QtWidgets.QHBoxLayout())
        self.edit_type = ComboBoxLabel('Edit type')
        self.edit_type.add_items(self.config.Config.departments)
        self.xml = xml_widget.XmlTable(self.config)

        self.file_filter = 'Edit Cut(*.xml)'
        self.xml_file = browse_widget.BrowseFile(title='Sequence Xml')
        self.xml_file.set_label('Xml file : ')
        self.xml_file.filter = self.file_filter

        self.mov_filter = 'Cut Shot(*.mov *.mp4)'
        self.mov_file = browse_widget.BrowseFile(title='Sequence Vdo')
        self.mov_file.set_label('Vdo file : ')
        self.mov_file.filter = self.mov_filter

        self.wav_filter = 'Cut Shot(*.wav)'
        self.wav_file = browse_widget.BrowseFile(title='Wav File')
        self.wav_file.set_label('Wav file : ')
        self.wav_file.filter = self.wav_filter

        self.preview = QtWidgets.QPushButton('Preview XML')
        self.preview.setMinimumSize(QtCore.QSize(160, 70))

        self.select_all_checkbox = QtWidgets.QCheckBox('Select All')
        self.select_all_checkbox.stateChanged.connect(self.select_all)
        self.select_all_checkbox.setChecked(True)
        self.filter_label = QtWidgets.QLabel('Filters')

        self.error_checkbox = QtWidgets.QCheckBox('Continue on Error')
        self.publish = QtWidgets.QPushButton('Start Publish')
        self.publish.setMinimumSize(QtCore.QSize(200, 40))
        self.cancel = QtWidgets.QPushButton('Cancel')
        self.cancel.setMinimumSize(QtCore.QSize(200, 40))

        self.status_label = QtWidgets.QLabel('')

        # action filters 
        filter_checkboxs = list()
        for action in self.config.Action.action_map.keys(): 
            display = self.config.Action.action_map[action]['display']
            name = self.config.Action.action_map[action]['func']
            checkbox = QtWidgets.QCheckBox(display)
            checkbox.setChecked(True)
            checkbox.setObjectName(name)
            checkbox.stateChanged.connect(partial(self.filter_checked, action))
            filter_checkboxs.append(checkbox)

        # layout 
        # self.lf_layout.addItem(gen.spacer('h'))
        self.lf_layout.addWidget(QtWidgets.QLabel())
        self.lf_layout.addWidget(self.project)
        self.lf_layout.addWidget(self.episode)
        self.lf_layout.addWidget(self.edit_type)

        self.rt_layout.addWidget(gen.Logo())
        self.rt_layout.addWidget(self.user)

        self.header.addItem(self.lf_layout)
        self.header.addItem(gen.spacer('w'))
        self.header.addLayout(self.rt_layout)

        self.file_layout.addWidget(self.xml_file, 0, 0, 1, 1)
        self.file_layout.addWidget(self.mov_file, 1, 0, 1, 1)
        self.file_layout.addWidget(self.wav_file, 2, 0, 1, 1)
        self.file_layout.addWidget(self.preview, 0, 1, 3, 1)

        self.filter_layout.addWidget(self.select_all_checkbox, 0, 0, 1, 1)
        self.filter_layout.addWidget(gen.line('v'), 0, 1, 1, 1)
        self.filter_layout.addItem(gen.spacer('w'), 0, 2, 1, 1)
        self.filter_layout.addWidget(self.filter_label, 0, 3, 1, 1)
        self.filter_layout.addWidget(gen.line('v'), 0, 4, 1, 1)

        for i, checkbox in enumerate(filter_checkboxs): 
            col = i + 5
            self.filter_layout.addWidget(checkbox, 0, col, 1, 1)
            self.filter_layout.setColumnStretch(col, 1)

        self.button_layout.addWidget(self.publish)
        self.button_layout.addWidget(self.cancel)

        self.bottom_layout.addWidget(self.status_label, 0, 1, 1, 1)
        self.bottom_layout.addItem(gen.spacer('w'), 0, 1, 1, 1)
        self.bottom_layout.addWidget(self.error_checkbox, 0, 2, 1, 1)
        self.bottom_layout.addLayout(self.button_layout, 0, 3, 1, 1)

        self.layout.addLayout(self.header)
        self.layout.addWidget(gen.line('h'))
        self.layout.addLayout(self.file_layout)
        self.layout.addWidget(gen.line('h'))
        self.layout.addLayout(self.filter_layout)
        self.layout.addWidget(self.xml)
        self.layout.addLayout(self.bottom_layout)

        # cosmetic tweak 
        self.header.setStretch(0, 1)
        self.header.setStretch(1, 3)
        self.header.setStretch(2, 1)
        self.lf_layout.setStretch(0, 0)
        self.lf_layout.setStretch(1, 0)
        self.lf_layout.setStretch(2, 0)
        self.lf_layout.setStretch(3, 0)
        self.project.allLayout.setStretch(0, 1)
        self.project.allLayout.setStretch(1, 4)
        self.episode.layout.setStretch(0, 1)
        self.episode.layout.setStretch(1, 4)
        self.xml_file.allLayout.setSpacing(12)
        self.mov_file.allLayout.setSpacing(12)
        self.wav_file.allLayout.setSpacing(12)

        self.filter_layout.setColumnStretch(0, 0)
        self.filter_layout.setColumnStretch(1, 0)
        self.filter_layout.setColumnStretch(2, 4)
        self.filter_layout.setColumnStretch(3, 1)
        self.filter_layout.setColumnStretch(4, 0)

        self.bottom_layout.setColumnStretch(0, 0)
        self.bottom_layout.setColumnStretch(1, 4)
        self.bottom_layout.setColumnStretch(2, 0)
        self.bottom_layout.setColumnStretch(3, 1)

        self.button_layout.setSpacing(0)
        self.button_layout.setContentsMargins(0, 0, 0, 0)

        # set layout
        self.setLayout(self.layout)
        self.set_inprogress(False)

    def filter_checked(self, filter_name, value): 
        state = True if value else False 
        rows = self.xml.get_checked_items()
        action = self.config.Action.action_map[filter_name]
        
        for row in rows: 
            checkboxs = self.xml.get_row_actions(row)
            data = self.xml.get_data(row)
            for checkbox in checkboxs: 
                if action['display'] == checkbox.text(): 
                    checkbox.setChecked(state)

    def select_all(self, value): 
        state = True if value else False 
        count = self.xml.rowCount()
        for row in range(count): 
            self.xml.select_row(row, state)

    def set_status(self, text): 
        self.status_label.setText(text)

    def set_status_color(self, color): 
        if not color: 
            self.status_label.setStyleSheet('')
            return
        self.status_label.setStyleSheet('color: rgb(%s, %s, %s);' % (color[0], color[1], color[2]))

        # print filter_name, state

    def set_inprogress(self, state): 
        self.publish.setVisible(not state)
        self.cancel.setVisible(state)


class ComboBoxLabel(QtWidgets.QWidget):
    """docstring for ComboBoxLabel"""
    def __init__(self, label, parent=None):
        super(ComboBoxLabel, self).__init__(parent=parent)
        self.layout = QtWidgets.QHBoxLayout()
        self.label = QtWidgets.QLabel(label)
        self.combobox = QtWidgets.QComboBox()
        self.layout.addWidget(self.label)
        self.layout.addWidget(self.combobox)
        self.setLayout(self.layout)
        self.layout.setStretch(0, 0)
        self.layout.setStretch(1, 1)
        self.layout.setContentsMargins(0, 0, 0, 0)      

    def add_items(self, items): 
        self.combobox.addItems(items) 

    def current_text(self): 
        return self.combobox.currentText()

