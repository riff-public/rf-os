# v.0.0.1 polytag switcher
_title = 'Riff Shot Export'
_version = 'v.0.0.2'
_des = 'add select all export items'
uiName = 'ShotExportUI'

#Import python modules
import sys
import os
import getpass
import logging
from functools import partial
from collections import OrderedDict
import traceback
import json
from datetime import datetime

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

# import config
import rf_config as config
from rf_utils import log_utils
from rf_utils import file_utils
from rf_utils.sg import sg_utils
from rf_utils.context import context_info
import rf_app.publish.scene.export as export
from rf_app.publish.scene.utils import output_utils
from rf_utils import icon
from rf_utils import register_shot
from rf_maya.lib import sequencer_lib
from rf_app.publish.scene.utils import maya_hook as hook
from rf_utils.pipeline import snap_utils
from rf_utils.widget import dialog
from . import export_cmd
from ui import ui
reload(ui)
reload(export_cmd)
reload(hook)
reload(export)
reload(sequencer_lib)
reload(output_utils)

from rf_utils.pipeline.notification import noti_module_cliq
reload(noti_module_cliq)
from rf_utils import user_info
from rf_utils.sg import sg_process

sg = sg_utils.sg
ui.sg = sg

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui



# set logging level
context_info.logger.setLevel(logging.INFO)
ui.qc_widget.logger.setLevel(logging.INFO)
ui.qc_widget.config.logger.setLevel(logging.INFO)

class Color:
    red = [200, 60, 60]
    green = [60, 200, 60]
    yellow = [200, 120, 60]


class TaskConfig:
    taskMap = {'layout': 'split', 'anim': 'anim', 'setdress': 'setdress', 'fx': 'fx', 'light': 'light', 'finalcam': 'finalcam', 'techanim': 'techAnim', 'sim': 'sim', 'crowd': 'crowd', 'tracking': 'tracking'}

class RFShotExport(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        #Setup Window
        super(RFShotExport, self).__init__(parent)
        self.parent = parent

        # ui read
        self.ui = ui.ShotExportUI(parent)
        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)

        # title
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.resize(800, 460)

        self.set_default()
        self.init_signals()
        self.init_functions()

    def set_default(self): 
        self.ui.publishButton.setVisible(False)
        self.ui.exportSelectCheckBox.setChecked(True)
        self.ui.exportButton.setEnabled(False)

    def init_signals(self):
        self.ui.exportButton.clicked.connect(self.export)
        self.ui.shotWidget.selected.connect(self.shot_selected)
        self.ui.exportWidget.selected.connect(self.export_selected)

        # widget signals
        self.ui.exportWidget.checkBoxChecked.connect(self.export_checked)
        self.ui.shotWidget.checkBoxChecked.connect(self.shot_checked)

        # qc signals 
        self.ui.qcWidget.itemSelectionChanged.connect(self.qc_selected)
        self.ui.qcWidget.check.connect(self.qc_pass)

        # shot signals
        self.ui.shotSelectCheckBox.stateChanged.connect(self.shot_checked_all)

        # publish
        self.ui.publishButton.clicked.connect(self.publish_snap)

        # options
        self.ui.settingButton.clicked.connect(self.export_option)

        # export context
        self.ui.exportWidget.listWidget.customContextMenuRequested.connect(self.show_export_menu)

        # export 
        self.ui.exportSelectCheckBox.stateChanged.connect(self.export_checked_all)

    def init_functions(self):
        self.entity = context_info.ContextPathInfo()
        self.entity.context.update(task = TaskConfig.taskMap[self.entity.step])
        # always merge other processes to main
        self.entity.context.update(process='main')

        self.startFrame = self.entity.projectInfo.scene.start_frame
        sequenceKey = self.entity.projectInfo.scene.sequence_key
        self.isShot = True if not self.entity.name_code == sequenceKey else False

        self.init_qc()
        self.init_filters()
        self.refresh_shot_list()
        self.snap_check()
        # self.fetch_task()

    def init_qc(self): 
        # allow qc by config 
        self.ui.qcWidget.configFilter = True
        # update qc list by given project, step, entityType data
        self.entity.context.update(res='default')
        self.ui.qcWidget.update(self.entity.project, context_info.ContextKey.scene, self.entity.step, res=self.entity.res, entity=self.entity, process='default')

    def init_filters(self):
        config = export.read_config(self.entity.project)
        self.modData = config.get(self.entity.step, config.get('default'))
        self.filterMods = config.get(self.entity.step, config.get('default'))

    def refresh_shot_list(self):
        self.ui.shotWidget.clear()
        shots = hook.list_shot()

        if self.isShot:
            if shots:
                shots = [self.entity.name_code] if self.entity.name_code in shots else []
            else:
                shots = [self.entity.name_code]
        status = None
        shift = None
        statusMessage = ''
        
        if shots:
            for shotName in shots:
                startTime, endTime, shotScale = hook.list_shot_status(shotName)
                startFrame, endFrame, seqStartFrame, seqEndFrame = hook.shot_info(shotName)

                if shotScale == 1:
                    if startTime == self.startFrame:
                        shift = False
                        status = True
                        statusMessage = 'Ready'
                    if startTime != 1001 or seqStartFrame != 1001:
                        shift = True
                        status = True
                        statusMessage = 'Ready - Shift %s' % hook.shift_info(shotName)

                else:
                    status = False
                    statusMessage = 'Error Time Scale'

                # data = self.get_export_list(shotName)
                data = export.export_list(self.entity, shotName, self.filterMods)
                widgetItem = self.ui.shotWidget.add_item_info(shotName, startTime, endTime, data, statusMessage, status)

                # set status
                statusColor = Color.green

                if not status:
                    statusColor = Color.red
                else:
                    statusColor = Color.yellow if shift else Color.green

                widgetItem.set_status_color(statusColor)


    def export_option(self):
        self.optionUi = ui.Options()
        self.optionUi.set_data(self.modData, self.filterMods)
        # self.optionUi.setObjectName('uiOptions')
        self.optionUi.exec_()
        filterMods = self.optionUi.get_checked_mod()
        newFilter = OrderedDict()

        for mod, data in self.modData.iteritems():
            if mod in filterMods:
                newFilter[mod] = data
        self.filterMods = newFilter
        self.refresh_shot_list()
        # result = dialog.exec_()

    def export_option_menu(self, func, itemData, *args): 
        func(itemData, args) 
        self.refresh_shot_list()

    def show_export_menu(self, pos): 
        # get selected item
        selections = self.ui.exportWidget.data(select=True) 
        if selections: 
            menuFunc = selections[0].get('menu')
            menu = QtWidgets.QMenu(self)
            actionName = menuFunc.__name__ if menuFunc else 'No option'
            exportMenu = menu.addAction(actionName)
            exportMenu.setEnabled(False) if not menuFunc else None

            # connect menu signals
            if menuFunc: 
                exportMenu.triggered.connect(partial(self.export_option_menu, menuFunc, selections[0]))

            # show menu
            menu.popup(self.ui.exportWidget.listWidget.mapToGlobal(pos))

    def fetch_task(self):
        entityType = 'Shot'
        self.ui.taskComboBox.fetch_task(self.entity.project, entityType, self.entity.name, self.entity.step)

    def snap_check(self):
        snapFile = snap_utils.snap_file(self.entity)
        self.ui.publishButton.setEnabled(os.path.exists(snapFile))

    def shot_selected(self):
        shotData = self.ui.shotWidget.data(select=True)
        exportData = export_cmd.sorted_data_display(shotData[0]) if shotData else None
        self.ui.exportButton.setEnabled(True)

        if exportData:
            items = self.ui.shotWidget.selected_items()
            shotWidgetItem = self.ui.shotWidget.widget_item(items[0])
            self.set_export_list(preview=True)
            self.ui.exportButton.setEnabled(False)

    def shot_checked(self, inputData):
        item, state = inputData
        # refresh data
        data = item.data(QtCore.Qt.UserRole)
        # disable all items
        allItems = self.ui.shotWidget.items()


        for item in allItems:
            if state and item.flags() & QtCore.Qt.ItemIsEnabled:
                item.setFlags(item.flags() ^ QtCore.Qt.ItemIsEnabled)
            elif not state and not item.flags() & QtCore.Qt.ItemIsEnabled:
                item.setFlags(item.flags() ^ QtCore.Qt.ItemIsEnabled)
            self.ui.shotWidget.listWidget.setItemSelected(item, not state)

        # list export list
        self.set_export_list(export=True)

    def shot_checked_all(self, value):
        state = True if value else False
        self.ui.shotWidget.blockSignals(True)
        widgetItems = self.ui.shotWidget.widget_items()
        for widget in widgetItems:
            widget.set_checked(state)

        self.set_export_list(export=True)
        self.ui.shotWidget.blockSignals(False)
        self.ui.exportButton.setEnabled(state)

    def export_checked_all(self, value): 
        state = True if value else False
        widgetItems = self.ui.exportWidget.widget_items()
        if widgetItems: 
            # self.ui.exportWidget.blockSignals(True)
        
            for widget in widgetItems:
                widget.set_checked(state)

            # self.ui.exportWidget.blockSignals(False)


    def set_export_list(self, preview=False, export=False):
        # find widget
        self.ui.exportWidget.blockSignals(True)
        self.ui.exportWidget.clear()
        items = []

        if preview:
            items = self.ui.shotWidget.selected_items()
        if export:
            items = self.ui.shotWidget.checked_items()

        if items:
            for item in items:
                shotWidgetItem = self.ui.shotWidget.widget_item(item)
                exportData = item.data(QtCore.Qt.UserRole)
                exportData = export_cmd.sorted_data_display(exportData)

                for exportName, data in exportData.iteritems():
                    shotName = shotWidgetItem.shot()
                    exportType = data['type']
                    status = data['status']

                    if not status and export:
                        pass
                        # statusMessage = 'Skip'
                        # itemWidget.set_status_color(ui.export_list_widget.Color.red)
                    else:
                        item, itemWidget = self.ui.exportWidget.add_export_Item(shotName, exportName, exportType, data)
                        itemWidget.set_icon(icon.gear)
                        itemWidget.set_checked(status)
                        itemWidget.set_enabled(not export) if export else None
                        statusMessage = 'Ready' if export else '-'
                        itemWidget.set_status_color(ui.export_list_widget.Color.orange)
                        itemWidget.set_status(statusMessage)

        self.ui.exportWidget.blockSignals(False)

    def export_checked(self, inputData):
        """ if export item is checked, save state back to shot item data """
        shotItem = self.ui.shotWidget.selected_items()[0]
        shotData = self.ui.shotWidget.data(select=True)[0]

        item, widgetItem, data, state = inputData
        exportName = widgetItem.get_text2()
        data['status'] = state

        shotData[exportName] = data
        shotItem.setData(QtCore.Qt.UserRole, shotData)

    def refresh_export_list(self):
        shotName = self.ui.shotWidget.data(select=True)
        if self.entity.path and shotName:
            exportData = export.export_list(self.entity, shotName[0])
            self.ui.exportWidget.clear()

            for display, data in exportData.iteritems():
                self.ui.exportWidget.add_item(display, data)

    def export_selected(self):
        selections = self.ui.exportWidget.data(select=True)
        if selections:
            print selections[0].get('details')

    def qc_selected(self, data): 
        logger.info(data.get('info'))

    def qc_pass(self, status): 
        if status: 
            self.ui.exportButton.setEnabled(True)
            # self.init_filters()
            self.refresh_shot_list()
            # self.snap_check()


    def export(self):
        logger.info('Start export process ...')
        publish = self.ui.publishCheckBox.isChecked()
        startTime = datetime.now()
        items = self.ui.exportWidget.items(all=True)
        shotNames = []

        # start export function
        sucessItems, failedItems = do_export(self.entity, itemDatas=items, uiMode=True, widget=self.ui.exportWidget.listWidget, publish=False)
        self.duration = round((float((datetime.now() - startTime).seconds)), 2)

        if publish:
            self.publish_snap()

        # summary 
        else: 

            if failedItems: 
                title = 'Export not complete'
                failedItemList = '\n -'.join(failedItems)
                message = 'Finished in %s seconds\n%s failed items \n- %s' % (self.duration, len(failedItems), failedItemList)
                dialog.MessageBox.warning(title, message) if not publish else None
                logger.info('Export finished - %s with failed items %s' % ((datetime.now() - startTime), failedItemList))
            
            if not failedItems: 
                title = 'Export complete'
                message = 'Finished in %s seconds' % self.duration
                dialog.MessageBox.success(title, message) if not publish else None
                logger.info('Export finished - %s' % (datetime.now() - startTime))

    def publish_snap(self):
        logger.info('Publishing snap ...')
        self.thread = snap_utils.PublishSnap(context_info.ContextPathInfo())
        self.thread.complete.connect(self.complete_dialog)
        self.thread.status.connect(self.set_message)
        self.thread.start()

        items = self.ui.exportWidget.items(all=True)
        shotNames = [a.data(QtCore.Qt.UserRole)['shotName'] for a in items if a.data(QtCore.Qt.UserRole).get('status')]
        output_utils.post_register(self.entity, list(set(shotNames)))

    def complete_dialog(self, state):
        logger.info('Publish complete')
        dialog.MessageBox.success('Publish complete', 'Publish Complete in %s seconds' % self.duration)

    def set_message(self, message):
        logger.info(message)




def do_export(scene, itemDatas=None, uiMode=True, widget=None, publish=False, user=None, sendNoti=True):
    shotNames = []
    defaultStartFrame = scene.projectInfo.scene.start_frame or 1001
    shift = scene.projectInfo.scene.shift_to_start
    successItems = []
    failedItems = []
    cacheItems = []

    # write log before start exporting 
    write_log(itemDatas, uiMode)
    logger.info('Exporting ...')
    hook.turnoff_undo()
    for item in itemDatas:
        data = item.data(QtCore.Qt.UserRole) if uiMode else item
        status = data['status'] if uiMode else True
        itemType = data.get('type')
        func = data['func']
        args = data['args']
        shotName = data['shotName']

        if not shotName in shotNames:
            shotNames.append(shotName)
            if not scene.step == 'crowd':
                if shift: 
                    logger.info('%s Shifting keys' % shotName)
                    sequencer_lib.shift(shotName, defaultStartFrame=defaultStartFrame)

        if status:
            startProcess = datetime.now()
            # run
            logger.info('')
            
            try:
                startTimeMessage = 'Start time %s' % startProcess.strftime('%H:%M:%S')
                set_status('Exporting ...', message2=startTimeMessage, item=item, widget=widget, color=ui.export_list_widget.Color.yellow, color2=ui.export_list_widget.Color.green, uiMode=uiMode)
                logger.info('Exporting %s ...' % itemType)
                result, details = func(*args)

                message = 'Success' if result else 'Failed'
                color = ui.export_list_widget.Color.green if result else ui.export_list_widget.Color.red
                data['details'] = details
                timeMessage = 'Finished'
                timeColor = ui.export_list_widget.Color.yellow
                failedItems.append(itemType) if not result else successItems.append(itemType)
                if result:
                    cacheItems.append(data['display'])

            except Exception as e:
                # store traceback to var
                error = traceback.format_exc()
                traceback.print_exc()
                logger.error(error)
                message = 'Error'
                color = ui.export_list_widget.Color.red
                data['details'] = error
                hook.viewport_update(True)
                timeMessage = 'Failed'
                timeColor = ui.export_list_widget.Color.red
                failedItems.append(itemType)


            endProcess = datetime.now()
            duration = endProcess - startProcess
            timeFinishMessage = '%s in %s secs' % (timeMessage, round((float(duration.seconds)), 2))

            set_status(message, message2=timeFinishMessage, item=item, widget=widget, color=color, color2=timeColor, uiMode=uiMode)
            item.setData(QtCore.Qt.UserRole, data) if uiMode else None
                # itemWidget.set_status(message)
                # itemWidget.set_status_color(color)

    sequencer_lib.mute(False)

    if publish:
        output_utils.post_register(scene, shotNames)
        data = snap_utils.read(scene)
        if data:
            path = snap_utils.snap_file(scene)
            data = file_utils.ymlLoader(path) if os.path.exists(path) else dict()
            snap_utils.publish(data)
            timeName = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
            newName = '%s/%s.%s' % (os.path.dirname(path), os.path.basename(path), timeName)
            os.rename(path, newName)
        else:
            logger.info('No pending post publish data')

    hook.turnon_undo()

    # Send notification to anim, sim, light, comp
    if sendNoti:
        if cacheItems:
            projectEntity = sg_process.get_project(scene.project)
            projectEntity['name'] = scene.project
            entity = sg_process.get_entity(projectEntity, 'Shot', scene.name)
            entity['name'] = scene.name
            entity['type'] = 'Shot'
            taskEntity = {'project': projectEntity, 'content': scene.step, 'step': {'type': 'Step', 'name': 'Cache'}, 'entity': entity, 'sg_status_list': 'apr'}
            result = []
            if not user:
                user = user_info.User()
            else:
                user = user_info.User(adname=user)
            userEntity = user.sg_user()
            desc = 'Cache Asset List : '
            imgurl = 'https://previewengine-accl.zoho.com/image/WD/3gjs967154bd173b74ae3895650804860bfef?width=2046&height=1536'
            for item in cacheItems:
                desc = desc + '\n\t' + item

            noti_module_cliq.send_scene_notification(result, taskEntity, scene, userEntity, description=desc, imgurl=imgurl)
    return [successItems, failedItems]

def write_log(itemDatas, uiMode): 
    if itemDatas: 
        logger.info('Processing %s items' % len(itemDatas))
        for item in itemDatas:
            data = item.data(QtCore.Qt.UserRole) if uiMode else item
            status = data['status'] if uiMode else True
            itemType = data.get('type')
            func = data['func']
            args = data['args']
            shotName = data['shotName']

            logger.info('%s - %s - %s' % (shotName, itemType, args))


def batch(filters=[]):
    itemDatas = []
    scene = context_info.ContextPathInfo()
    isShot = True if not scene.name_code == 'all' else False
    shots = hook.list_shot()

    if isShot:
        shots = [scene.name_code] if scene.name_code in shots else []

    for shotName in shots:
        # datas = export.export_list(scene, shotName, modName=filters)
        # if datas:
        #     sortedData = export_cmd.sorted_data_display(datas)
        #     for key, data in sortedData.iteritems():
        #         itemDatas.append(data)
        exportData = get_export_dict(scene, shotName, modName=filters)
        itemDatas += exportData

    do_export(scene, itemDatas=itemDatas, uiMode=False, publish=True)


def get_export_dict(entity, shotName, modName): 
    exportData = []
    datas = export.export_list(entity, shotName, modName=modName)
    if datas:
        sortedData = export_cmd.sorted_data_display(datas)
        for key, data in sortedData.iteritems():
            exportData.append(data)
    return exportData


def get_export_list(scene):
    data = export.read_config(project=scene.project)
    defaultFiles = data.get('default').keys()
    exportFiles = data.get(scene.step, defaultFiles).keys()
    return exportFiles

def set_status(message, message2='', item=None, widget=None, color=None, color2=None, uiMode=True):
    if uiMode:

        itemWidget = widget.itemWidget(item)
        itemWidget.set_status(message) if itemWidget else None
        itemWidget.set_status_color(color) if color else None
        itemWidget.set_text4(message2)
        itemWidget.set_text4_color(color2) if color2 else None
        QtWidgets.QApplication.processEvents() if itemWidget else None


def show():
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Stand by shot export\n')
        maya_win.deleteUI(uiName)
        myApp = RFShotExport(maya_win.getMayaWindow())
        myApp.show()
        return myApp



# example 
# export batch
# from rf_app.publish.scene import app
# reload(app)
# shotName = 's0040'
# data = app.get_export_dict(entity, shotName, ['yeti_groom'])
# app.do_export(entity, itemDatas=data, uiMode=False, publish=True)