#Import python modules
import sys
import os
import getpass
import logging
from collections import OrderedDict
import traceback
from rf_utils.context import context_info
from rf_utils.sg import sg_process
from rf_utils import ascii_utils
from rf_utils.widget import dialog

def export_all(mayaScene):
    from rf_app.publish.scene import app
    reload(app)
    try:
        import maya.cmds as mc
        mc.file(mayaScene, o=True, f=True, prompt=False)
    except Exception as e:
        pass
    app.batch()
    # data = app.get_export_dict(entity, shotName, ['yeti_groom'])
    # app.do_export(entity, itemDatas=data, uiMode=False, publish=True)

def submit_queue(scene=None, namespaces=[], modules=[], assets=[], prefix='cache', build=True):
    from rf_utils.deadline import cache_all
    reload(cache_all)
    from rf_utils.context import context_info
    reload(context_info)
    from rf_utils import user_info

    # version = mc.about(v=True)
    cachePy = 'O:/Pipeline/core/rf_app/bot/cache_cmd.py'
    # mayaPy = 'C:/Program Files/Autodesk/Maya2018/bin/mayapy.exe'
    camera = None
    fcl = None
    if not scene: 
        import maya.cmds as mc
        scene = mc.file(q=True, loc=True)
        entity = context_info.ContextPathInfo(path = scene)
        shotName = entity.name_code
        camera = mc.shot(shotName, q=True, cc=True)
        fcl = mc.getAttr('%s.focalLength' %camera)

    entity = context_info.ContextPathInfo(path = scene)
    mayaPy = entity.projectInfo.software.mayapy() # 'C:/Program Files/Autodesk/Maya2020/bin/mayapy.exe'

    if entity.step == 'crowd':
        prefix = entity.step
    user = user_info.User()
    username = user.adname
    jobname = '%s_%s' % (prefix, os.path.basename(scene))

    cmds = [cachePy, '-s', scene, '-build', str(build)]
    if namespaces:
        cmds.append('-n')
        for ns in namespaces:
            cmds.append(ns)
    if modules:
        cmds.append('-m')
        for mod in modules:
            cmds.append(mod)
    if assets: 
        cmds.append('-a')
        for asset in assets: 
            cmds.append(asset)
    if camera: 
        cmds.append('-c')
        cmds.append(camera)
    if fcl: 
        cmds.append('-fcl')
        cmds.append(str(fcl))
    if user: 
        cmds.append('-user')
        cmds.append(str(username))


    sended_job = cache_all.process_send_to_farm(cmds, jobname=jobname, executable=mayaPy)
    if sended_job:
        if entity.step == 'anim' and build:
            set_sg_status(scene, 'cache', 'queue')
        # mc.confirmDialog(title='Confirm', message='Submit success', button=['OK'])
        dialog.MessageBox.success('Confirm', 'Submit success')

def submit_yeti_shot(scene=None, namespaces=[]):
    from rf_utils.deadline import cache_all
    reload(cache_all)
    cachePy = 'O:/Pipeline/core/rf_app/bot/yeti_cmd.py'
    # mayaPy = 'C:/Program Files/Autodesk/Maya2018/bin/mayapy.exe'
    entity = context_info.ContextPathInfo(path=scene)
    mayaPy = entity.projectInfo.software.mayapy()

    jobname = 'yeti_%s' % os.path.basename(scene)
    cmds = [cachePy, '-s', scene]
    if namespaces:
        cmds.append('-n')
        for ns in namespaces:
            cmds.append(ns)
            
    sended_job = cache_all.process_send_to_farm(cmds, jobname=jobname, executable=mayaPy)

def submit_miarmy_shot(scene=None):
    from rf_utils.deadline import cache_all
    reload(cache_all)
    cachePy = 'O:/Pipeline/core/rf_app/bot/cache_miarmy_cmd.py'
    # mayaPy = 'C:/Program Files/Autodesk/Maya2018/bin/mayapy.exe'
    entity = context_info.ContextPathInfo(path = scene)
    mayaPy = entity.projectInfo.software.mayapy()

    jobname = 'miarmy_%s' % os.path.basename(scene)
    cmds = [cachePy, '-s', scene]
            
    sended_job = cache_all.process_send_to_farm(cmds, jobname=jobname, executable=mayaPy, Pool='miarmy', SecondaryPool='miarmy')

def submit_miarmy_meshdrive(scene, destinationPath, agents=[]): 
    from rf_utils.deadline import cache_all
    reload(cache_all)
    cachePy = 'O:/Pipeline/core/rf_app/bot/cache_miarmy_meshDrive_cmd.py'
    # mayaPy = 'C:/Program Files/Autodesk/Maya2018/bin/mayapy.exe'
    entity = context_info.ContextPathInfo(path = scene)
    mayaPy = entity.projectInfo.software.mayapy()
    jobname = 'miarmyMD_%s' % os.path.basename(scene)
    agents = ' '.join(agents)
    cmds = [cachePy, '-s', scene, '-d', destinationPath, '-a', agents] # input arguments 
    # print cmds    
    sended_job = cache_all.process_send_to_farm(cmds, jobname=jobname, executable=mayaPy, Pool='miarmy', SecondaryPool='miarmy')

def get_cacheable_namespaces(path):
    cacheable_types = ('char', 'prop')
    reader = ascii_utils.MayaAscii(path=path)
    data = reader.read()
    refs = reader.listReferences()
    result = []
    for ref in refs:
        ns = ref['namespace']
        if ns:
            refPath = ref['path']
            asset = context_info.ContextPathInfo(path=refPath)
            if asset.type in cacheable_types and not ns.endswith('Mtr'):
                result.append(ns)
    return result

def set_sg_status(path, task, status):
    """ set status for anim and tech anim to check if auto cache and autobuild works """
    # find cache and tech anim task
    scene = context_info.ContextPathInfo(path=path)
    scene.context.use_sg(sg_process.sg, scene.project, entityType='scene', entityName=scene.name)

    # find task
    filters = [['project.Project.name', 'is', scene.project], ['entity', 'is', scene.context.sgEntity], ['content', 'is', task]]
    fields = ['content', 'id']
    taskEntity = sg_process.sg.find_one('Task', filters, fields)

    # update task
    data = {'sg_status_list': status}
    sg_process.sg.update('Task', taskEntity['id'], data)


def submit_check_near_camera(file_list, jobname):
    from rf_utils.deadline import cache_all
    reload(cache_all)

    cachePy = 'O:/Pipeline/core/rf_app/bot/check_near_assets/run.py'
    mayaPy = 'C:/Program Files/Autodesk/Maya2018/bin/mayapy.exe'

    files = []
    with open(file_list, 'r') as f:
        files = [l.rstrip() for l in f.readlines()]

    for path in files:
        shot_jobname = 'check_near_assets_%s' % (os.path.basename(path))
        cmds = [cachePy, '-s', path, '-j', jobname, shot_jobname]
        sended_job = cache_all.process_send_to_farm(cmds, jobname=shot_jobname, executable=mayaPy)
