# v.0.0.1 polytag switcher
_title = 'Riff Cache Farm'
_version = 'v.0.1.0'
_des = 'Cache Selective Feature'
uiName = 'SubmitQueueUi'

#Import python modules
import sys
import os
import getpass
import logging
from functools import partial
from collections import OrderedDict
import traceback
from datetime import datetime

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

# import config
import rf_config as config
from rf_utils import log_utils


user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

from rf_qc import qc_widget
from rf_utils.widget import export_list_widget
from rf_utils.context import context_info
from rf_app.publish.scene import batch
from rf_utils.pipeline import lockdown
from rf_utils.pipeline import shot_asset
from rf_app.farm.cache_submit import app
from rf_utils.pipeline import password
from rf_utils.pipeline.notification import noti
from rf_utils.widget import dialog

class Color:
    red = 'color: rgb(200, 0, 0);'
    green = 'color: rgb(0, 240, 0);'
    bgGreen = 'background-color: rgb(0, 140, 0);'
    bgYellow = 'background-color: rgb(200, 120, 0);'
    bgRed = 'background-color: rgb(140, 0, 0);'
    bgLightRed = 'background-color: rgb(140, 60, 60);'

class Ui(object):
    """docstring for Ui"""
    def __init__(self, mainWidget):
        super(Ui, self).__init__()
        self.mainWidget = mainWidget

        self.layout = QtWidgets.QVBoxLayout()
        self.selectiveCacheWidget = export_list_widget.ExportListWidget()
        self.filterWidget = export_list_widget.ExportListWidget()

        self.cacheLabel = QtWidgets.QLabel('Cache Selected Asset')
        self.layout.addWidget(self.cacheLabel)
        self.layout.addWidget(self.filterWidget)
        self.layout.addWidget(self.selectiveCacheWidget)

        self.debugCheckBox = QtWidgets.QCheckBox('Details')
        self.button = QtWidgets.QPushButton('Submit Selected to Queue')
        self.layout.addWidget(self.debugCheckBox)
        self.layout.addWidget(self.button)

        self.mainWidget.setLayout(self.layout)

        self.layout.setStretch(0, 0)
        self.layout.setStretch(1, 3)
        self.layout.setStretch(2, 1)
        self.layout.setStretch(3, 0)
        self.layout.setStretch(4, 0)

        self.layout.setContentsMargins(0, 0, 0, 0)


class Modules: 
    default = [
         # 'alembic_cache',
         'cache_single',
         # 'export_grp',
         # 'export_set',
         # 'instance_dress',
         # 'set_asset_list',
         # 'shotdress',
         # 'shot_workspace',
         'yeti_bg']

    cache = ['cache_single']

        
class QueueWidget(QtWidgets.QWidget):
    def __init__(self, parent=None):
        #Setup Window
        super(QueueWidget, self).__init__(parent)
        
        # ui read
        self.ui = Ui(self)
        self.set_default()
        self.init_functions()
        self.init_signals()

    def set_default(self): 
        self.ui.button.setMinimumSize(QtCore.QSize(0, 30))
        self.ui.selectiveCacheWidget.setVisible(False)
        self.ui.filterWidget.listWidget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)

    def activate_button(self, status): 
        self.ui.button.setEnabled(status)

    def init_signals(self): 
        self.ui.button.clicked.connect(self.submit_selective_queue)
        self.ui.filterWidget.listWidget.itemChanged.connect(self.filter_selected)
        self.ui.debugCheckBox.stateChanged.connect(self.debug_checked)
        self.ui.filterWidget.listWidget.customContextMenuRequested.connect(self.show_menu)

    def init_functions(self): 
        pass 

    def init_widget(self, entity): 
        self.entity = entity
        self.init_list()
        self.ui.button.setEnabled(False)

    def init_list(self): 
        # main list 
        self.ui.filterWidget.listWidget.blockSignals(True)
        self.ui.selectiveCacheWidget.clear()

        shotAssetData = shot_asset.read_shot_assets(self.entity)
        app.SubmitterCache.add_cache_list(self.ui.selectiveCacheWidget, shotAssetData, path=self.entity.path.current().abs_path())
        self.ui.selectiveCacheWidget.set_checked_items(self.ui.selectiveCacheWidget.items(all=True), False)

        # filter list 
        data = self.get_common_name(shotAssetData)
        commonNames = data['filter']
        lockKey = data['lockKey']
        self.ui.filterWidget.clear()

        for name in commonNames: 
            display = name
            lock = lockKey.get(name, False)
            item = self.ui.filterWidget.add_item(display, [name, lock])

            if lock: 
                color = [255, 100, 100]
                item.setForeground(QtGui.QColor(color[0], color[1], color[2]))
                item.setText('%s - locked: Already render' % display)
                # item.setFlags(QtCore.Qt.NoItemFlags)

            self.ui.filterWidget.set_checked_items([item], False)
        self.ui.filterWidget.listWidget.blockSignals(False)

    def show_menu(self, pos): 
        item = self.ui.filterWidget.item_selected()
        data = item.data(QtCore.Qt.UserRole)
        name = data[0]
        lock = data[1]
        
        if lock: 
            menu = QtWidgets.QMenu(self)
            unlockMenu = menu.addAction('Request Unlocked %s' % name)
            unlockMenu.triggered.connect(partial(self.unlock))
            menu.popup(self.ui.filterWidget.listWidget.mapToGlobal(pos))

    def unlock(self): 
        result = password.anim_dialog()

        if result: 
            logger.info('Sending emails ...')
            noti.run(self.entity)

            logger.info('Unlocking ...')
            allItems = self.ui.selectiveCacheWidget.items(all=True)
            item = self.ui.filterWidget.item_selected()
            data = item.data(QtCore.Qt.UserRole)
            key = data[0]
            unlockInfo = dict()

            for item in allItems: 
                itemData = item.data(QtCore.Qt.UserRole)
                ns, path, lock, cacheType = itemData
                if key in ns and lock: 
                    unlockInfo[ns] = {'namespace': ns, 'path': path}

            for ns, lockData in unlockInfo.iteritems(): 
                path = lockData['path']
                entity = context_info.ContextPathInfo(path=path)
                self.set_lock(entity, ns, False)

            self.init_widget(self.entity)

    def set_lock(self, entity, namespace, state): 
        from rf_utils import register_sg
        shotReg = register_sg.ShotElement(entity)
        allNs = shotReg.list_keys()

        if namespace in allNs: 
            elem = shotReg.read(namespace)
            elem.set_task('anim', sg_lockdown=state)

    def get_common_name(self, data): 
        # use first element as filters
        returnData = []
        lockKey = dict()
        if data: 
            for k, v in data.iteritems(): 
                name =  v.get('namespace').split('_')[0]
                lock = v.get('lock')
                returnData.append(name) if not name in returnData else None
                
                if lock: 
                    lockKey[name] = lock 

        return {'filter': returnData, 'lockKey': lockKey}

    def get_checked_ns(self): 
        items = self.ui.selectiveCacheWidget.checked_items()
        namespaces = [a.data(QtCore.Qt.UserRole)[0] for a in items]
        return namespaces

    def filter_selected(self): 
        """ select by filter name """ 
        checkedItems = self.ui.filterWidget.checked_items()
        allItems = self.ui.selectiveCacheWidget.items(all=True)
        targetItems = []

        # un checked all selective widget 
        self.ui.selectiveCacheWidget.set_checked_items(allItems, False)

        for checkedItem in checkedItems: 
            data = checkedItem.data(QtCore.Qt.UserRole)
            filterName, lock = data

            if lock: 
                logger.warning('Checked locked item not allowed')
                checkedItem.setCheckState(QtCore.Qt.Unchecked)
                continue

            for item in allItems: 
                ns = item.data(QtCore.Qt.UserRole)[0]
                if filterName in ns: 
                    targetItems.append(item)
        
        # rechecked only match filters 
        self.ui.selectiveCacheWidget.set_checked_items(targetItems, True)

    def debug_checked(self, value): 
        """ show / hide details widget """ 
        state = True if value else False 
        self.ui.selectiveCacheWidget.setVisible(state)

    def submit_selective_queue(self): 
        path = self.entity.path.current().abs_path()
        modules = Modules.default
        namespaces = self.get_checked_ns()

        if namespaces: 
            assets = []
            # find yeti namespace "_Tech" if not remove "yeti_bg" from modules
            yetiNs = [a for a in namespaces if '_Tech' in a]
            if not yetiNs: 
                modules = Modules.cache

            from rf_app.save_plus import save_utils
            path = save_utils.save_file_farm()

            batch.submit_queue(path, namespaces=namespaces, modules=modules, assets=assets, prefix='cacheSelective')
            logger.info('Submit %s' % namespaces)

        else: 
            dialog.MessageBox.warning('Warning', 'No asset selected')

    def hide_option_layout(self, state): 
        widgets = [self.ui.cacheLabel, 
                    self.ui.filterWidget, 
                    self.ui.debugCheckBox, 
                    self.ui.button
                    ]

        for widget in widgets: 
            widget.setVisible(state)

        if self.ui.debugCheckBox.isChecked(): 
            self.ui.selectiveCacheWidget.setVisible(state)


class RFQueue(QtWidgets.QMainWindow):
    """docstring for RFQueue"""
    def __init__(self, parent=None):
        super(RFQueue, self).__init__(parent)
        # ui read
        self.ui = QueueWidget(parent)
        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)

        # title
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.w = 500 
        self.h = 500
        self.resize(self.w, self.h)

        self.entity = context_info.ContextPathInfo()
        self.ui.init_widget(self.entity)
        self.ui.activate_button()
        
def show():
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run shot export\n')
        maya_win.deleteUI(uiName)
        myApp = RFQueue(maya_win.getMayaWindow())
        myApp.show()
        return myApp



# example 
# export batch
# from rf_app.publish.scene import app
# reload(app)
# shotName = 's0040'
# data = app.get_export_dict(entity, shotName, ['yeti_groom'])
# app.do_export(entity, itemDatas=data, uiMode=False, publish=True)