import sys
import os
from collections import OrderedDict
from rftool.utils import pipeline_utils
from rf_app.asm import asm_lib
reload(asm_lib)

from rf_app.publish.scene.utils import maya_hook as hook
reload(hook)

def run(scene, shotName, exportFilter=[]):
	""" scene = contextPathInfo, shotName = sequencerName """
	listData = OrderedDict()
	startTime, endTime, shotScale = hook.list_shot_status(shotName)
	listData['set_asset_list'] = {'type': 'shotgun', 'status': True, 'func': export2, 'shotName': shotName, 'args': [scene, shotName], 'details': 'Update asset list to Shotgun'}
	return listData

def export(scene, shotName, **kwargs):
	""" module for export playblast """
	from rf_utils.sg import sg_process
	scene = scene.copy()
	scene.context.update(entityCode=shotName, entity='none')
	scene.update_scene_context()
	shotEntity = sg_process.get_shot_entity(scene.project, scene.name)
	assetDict = pipeline_utils.collect_assets(scene)
	setDict = asm_lib.collect_set_description(scene)
	setdressDict = asm_lib.collect_asset_description(scene)
	shotdressDict = asm_lib.collect_shotdress_description(scene)
	entities = combine_dict(assetDict, setDict)
	setDressEntities = combine_dict(shotdressDict, setdressDict)
	sg_process.update_asset_list(shotEntity.get('id'), entities, sg_field='assets')
	sg_process.update_asset_list(shotEntity.get('id'), setDressEntities, sg_field='sg_setdresses')
	return True, 'Complete'

def export2(scene, shotName=None, **kwargs): 
	from rf_utils.sg import sg_process
	from rf_maya.rftool.utils import objectsInCameraView
	from rf_app.asm import asm_lib
	import maya.cmds as mc
	from rf_utils.pipeline import shot_data
	# from rf_utils import register_shot
	# reg = register_shot.Register(scene)
	# assets = reg.get.asset_list()
	if shotName:
		scene = scene.copy()
		scene.context.update(entityCode=shotName, entity='none')
		scene.update_scene_context()
	shotEntity = sg_process.get_shot_entity(scene.project, scene.name)
 
	keyData = shot_data.Config.buildData
	currentData = shot_data.read_data(scene, keyData)
	newData = OrderedDict(currentData)
	nsList = newData.keys()

	assetDict = pipeline_utils.collect_assets(scene, namespaceList = nsList)
	setDict = asm_lib.collect_set_description(scene)

	if shotName:
		startTime, endTime, shotScale = hook.list_shot_status(shotName)
		camShape = mc.shot(shotName, q=True, cc=True)
		cam = mc.listRelatives(camShape, parent=True)[0]
		asms = objectsInCameraView.getSeenObjects(cam ,timeRange=[startTime,endTime],viewport='legacy', typ='gpuCache')
		mc.select(list(asms))
		roots = asm_lib.find_selection_root()
		gpuShow = []
		for root in roots:
		    hid = mc.getAttr('%s.hidden'%root)
		    if not hid:
		        gpuShow.append(root)
		setdressDict = asm_lib.sg_asset_dict(gpuShow)
	else:
		setdressDict = asm_lib.collect_asset_description(scene, includeHidden=False)

	#setdressDict = asm_lib.collect_asset_description(scene, includeHidden=False)
	shotdressDict = asm_lib.collect_shotdress_description(scene, includeHidden=False)
	entities = combine_dict(assetDict, setDict)
	setDressEntities = combine_dict(shotdressDict, setdressDict)
	sg_process.update_asset_list(shotEntity.get('id'), entities, sg_field='assets')
	sg_process.update_asset_list(shotEntity.get('id'), setDressEntities, sg_field='sg_setdresses')
	# update sequenece assets 
	sequenceEntity = shotEntity['sg_sequence']
	sg_process.update_asset_list(sequenceEntity.get('id'), entities, sg_field='assets', entity='Sequence', mode='merge')
	sg_process.update_asset_list(sequenceEntity.get('id'), setDressEntities, sg_field='assets', entity='Sequence', mode='merge')
	# print 'Update sequence assets'
	return True, 'Complete'

def remove_duplicate_dict(listData): 
	data = [dict(t) for t in {tuple(a.items()) for a in listData}]
	return data 

def combine_dict(dict1, dict2):
	sgEntities = []
	combDict = OrderedDict()

	for k, v in dict1.iteritems():
		if not k in combDict.keys():
			combDict[k] = v

	for k, v in dict2.iteritems():
		if not k in combDict.keys():
			combDict[k] = v

	for k, v in combDict.iteritems():
		if v.get('id'):
			entity = {'type': 'Asset', 'id': v.get('id'), 'code': v.get('name'), 'project': v.get('project')}
			if not entity in sgEntities:
				sgEntities.append(entity)
	return sgEntities
# publish_version(ContextPathInfo, taskEntity, status, imgs, userEntity, description, uploadOption='default')
