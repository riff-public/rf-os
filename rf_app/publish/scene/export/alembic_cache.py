import sys
import os
import shutil
from datetime import datetime
from rf_utils import publish_info
from collections import OrderedDict
import maya.cmds as mc
import pymel.core as pm
from rf_app.publish.scene.utils import output_utils
from rf_utils.context import context_info
reload(output_utils)
from rf_app.publish.scene.export import cache
from rf_utils.pipeline import cache_list
reload(cache_list)
from rf_utils import file_utils 
from rftool.utils import maya_utils
import logging
logger = logging.getLogger(__name__)

try: 
    from Qt import QtCore
    from Qt import QtWidgets
    from Qt import QtGui
except ImportError: 
    pass 

namespaceFilter = []
techSuffix = '_Tech'
newConfig = True
lockedNs = 'N/A'

def run(scene, shotName, exportFilter=[]):
    """ scene = contextPathInfo, shotName = sequencerName """
    listData = OrderedDict()
    cacheGrp = scene.projectInfo.asset.geo_grp() or 'Geo_Grp'
    techGrp = scene.projectInfo.asset.tech_grp() or 'TechGeo_Grp'
    # set default config 
    set_config(scene)
    # get alembic export objDict 
    objGeoDict = geo_export_dict(scene, shotName, cacheGrp, 'abc_cache')
    objTechDict = geo_export_dict(scene, shotName, techGrp, 'abc_tech_cache', techSuffix)
    objDict = dict(objGeoDict)
    objDict.update(objTechDict)

    preRoll = scene.projectInfo.scene.pre_roll or 30
    postRoll = scene.projectInfo.scene.post_roll or 10


    # create function data for framework
    listData['cache_all_assets'] = {
            'type': 'cache',
            'status': True,
            'func': multiple_cache,
            'shotName': shotName,
            'args': [objDict, shotName, scene, preRoll, postRoll],
            'details': 'Export cache all',
            'order': 0,
            'menu': cache_options, 
            'configFile': precache_config(scene)

        }

    # register function 
    orderCount = 1
    for key, objData in objDict.iteritems(): 
        namespace = objData['namespace']
        cacheType = objData['type']
        ref = cache.get_ref_path(objData['originalNs'])
        funcDict = {
            'registerFile': {
            'func': register_cache,
            'funcType': cacheType,
            'dataType': 'abc_cache',
            'outputNs': namespace,
            'namespace': namespace,
            'node': cacheGrp,
            'outputKey': 'cache',
            'schemaKey': ['cachePath', 'cacheGlobalHero'],
            'ref': ref, 
            'exportNs': objData['originalNs']
            }
        }

        listData[namespace] = {
            'type': 'register_version',
            'status': True,
            'func': output_utils.run,
            'shotName': shotName,
            'args': [funcDict, scene, exportFilter, shotName, scene, namespace],
            'details': 'Export cache',
            'order': orderCount
        }
        orderCount += 1 

    return listData
        

def geo_export_dict(scene, shotName, cacheGrp, cacheType, namespaceSuffix=''): 
    """ generate objDict for further use in jobString """ 
    objDict = OrderedDict()
    namespaces = read_active_namespace(scene, cacheType)
    namespaces = [a for a in namespaces if a in namespaceFilter] if namespaceFilter else namespaces

    # filter out in view data 
    entity = scene.copy()
    entity.context.update(entityCode=shotName, entity='none')
    entity.update_scene_context()

    cacheList = cache_list.CacheList(entity)
    inviewNs = cacheList.list_keys_status(status=True)
    namespaces = [a for a in namespaces if a in inviewNs] if inviewNs else namespaces

    # get locked information from shotgun 
    lockNs = get_lock_ns(entity)
    if lockNs: 
        namespaces = [a for a in namespaces if not a in lockNs]
        logger.debug('Namespace locked ** %s' % lockNs)

    # creating objectDict for alembic export 
    for namespace in namespaces:
        # cache 
        buildNamespace = namespace if not namespaceSuffix else '%s%s' % (namespace, namespaceSuffix)
        entity = scene.copy()
        entity.context.update(entityCode=shotName, entity=entity.sg_name, process=buildNamespace)
        # entity.context.update(entity=entity.sg_name)

        ref = cache.get_ref_path(namespace)
        obj = '%s:%s' % (namespace, cacheGrp)
        dstDir = entity.path.scheme(key='cacheHeroPath').local_path()
        cacheName = entity.output_name(outputKey='cache', hero=True)
        objDict[obj] = {'path': '%s/%s' % (dstDir, cacheName), 'namespace': buildNamespace, 'type': cacheType, 'originalNs': namespace}

    return objDict


def get_lock_ns(entity): 
    from rf_utils import register_sg
    global lockedNs
    if lockedNs == 'N/A': 
        logger.debug('Fetching shotgun for locked namespaces ...')
        reg = register_sg.ShotElement(entity)
        lockedNs = reg.list_lock(entity.step)
    
    return lockedNs

def register_cache(dstPath, shotName, scene, namespace): 
    """ copy file from export config path to dstPath """ 
    configFile = precache_config(scene)
    data = read_config(scene)
    if namespace in data.keys(): 
        path = data[namespace]['path']

    if not dstPath == path: 
        file_utils.xcopy_file(path, dstPath)
        if os.path.isfile(path): 
            file_utils.remove_file(path)
        else: 
            shutil.rmtree(path)

    return dstPath if os.path.exists(dstPath) else None

def update_config(scene, objDict, key): 
    data = read_config(scene)

    for display, objData in objDict.iteritems(): 
        path = objData[key]
        namespace = objData['namespace']
        if namespace in data.keys(): 
            data[namespace][key] = path

    write_config(scene, data)


def multiple_cache(objDict, shotName, scene, preRoll, postRoll): 
    from rftool.utils import abc_utils
    reload(abc_utils)
    objDict = reformat_order(objDict)
    startTime, endTime = cache.get_shot_time(shotName)
    startTime = startTime - preRoll
    endTime = endTime + postRoll
    exportDict = OrderedDict()

    # pre cache steps 
    for obj, data in objDict.iteritems(): 
        cache.add_description(obj)
        cache.add_ref(obj)
        exportDict[obj] = data['path']

    # freeze viewport 
    # export cache section 
    # mc.refresh(su=True)
    # abc_utils.export_multiple_abcs(exportDict, startTime, endTime)
    # mc.refresh(su=False)

    with maya_utils.FreezeViewport(): 
        abc_utils.export_multiple_abcs(exportDict, startTime, endTime)

    # register path section 
    update_config(scene, objDict, 'path')
    # return all(os.path.exists(path) for k, path in objDict.iteritems()

    return [path for k, path in objDict.iteritems()], 'All cache exported'


def reformat_order(dictData): 
    orderDict = OrderedDict()
    for k in sorted(dictData.keys()): 
        orderDict[k] = dictData[k]
    return orderDict


def get_cache_list(scene, cacheGrp): 
    configFile = precache_config(scene)

    cache.get_asset_namespace(cacheGrp)


def precache_config(scene): 
    path = scene.path.scheme(key='outputStepPath').local_path()
    configFile = '%s/cache_asset_list.yml' % path
    return configFile


def get_asset_data(scene): 
    """ set config for exporting both anim geo and tech geo """ 
    geoCacheGrp = scene.projectInfo.asset.geo_grp() or 'Geo_Grp'
    techCacheGrp = scene.projectInfo.asset.tech_grp() or 'TechGeo_Grp'

    cacheList = cache.get_asset_namespace(geoCacheGrp)
    techCacheList = cache.get_asset_namespace(techCacheGrp)

    # default data 
    data = OrderedDict()
    for namespace in cacheList: 
        data[namespace] = {'status': True, 'path': '', 'type': 'abc_cache', 'namespace': namespace}

    for namespace in techCacheList: 
        data['%s%s' % (namespace, techSuffix)] = {'status': True, 'path': '', 'type': 'abc_tech_cache', 'namespace': namespace}

    return data



def set_config(scene): 
    """ set config for exporting both anim geo and tech geo """ 
    data = get_asset_data(scene)
    # write config 
    # get path 
    # write new config only when reopen ui
    global newConfig
    configFile = precache_config(scene)
    createConfig = False

    if newConfig or not os.path.exists(configFile): 
        createConfig = True
        newConfig = False

    if createConfig: 
        # create folder 
        os.makedirs(os.path.dirname(configFile)) if not os.path.exists(os.path.dirname(configFile)) else None 
        file_utils.ymlDumper(configFile, data)
    

    return True if os.path.exists(configFile) else False 

def read_config(scene): 
    configFile = precache_config(scene)
    data = file_utils.ymlLoader(configFile)
    return data 

def read_active_namespace(scene, cacheType): 
    data = read_config(scene)
    return [v['namespace'] for k, v in data.iteritems() if v['status'] and v['type'] == cacheType]

def set_cache_path(scene, namespace, path): 
    configFile = precache_config(scene)
    data = file_utils.ymlLoader(configFile)
    
    if namespace in data.keys(): 
        data[namespaces]['path'] = path

    write_config(scene, data)

def write_config(scene, data): 
    configFile = precache_config(scene)
    file_utils.ymlDumper(configFile, data)
    return True if os.path.exists(configFile) else False


def cache_options(itemData, *args): 
    """ show asset cache options """ 
    configFile = itemData.get('configFile')
    configData = file_utils.ymlLoader(configFile)

    cacheOption = MenuOptions()
    cacheOption.set_data(configData)
    cacheOption.exec_()

    cacheSelection = cacheOption.get_checked_mod()

    for namespace, data in configData.iteritems(): 
        data['status'] = True if namespace in cacheSelection else False

    file_utils.ymlDumper(configFile, configData)


class MenuOptions(QtWidgets.QDialog):
    """docstring for Option"""
    def __init__(self, parent=None):
        super(MenuOptions, self).__init__(parent=parent)
        self.layout = QtWidgets.QVBoxLayout()
        self.listWidget = QtWidgets.QListWidget()
        self.checkBox = QtWidgets.QCheckBox('Select All')
        self.checkBox.setChecked(True)
        self.layout.addWidget(self.listWidget)
        self.layout.addWidget(self.checkBox)
        self.setLayout(self.layout)
        self.setWindowTitle('Cache asset selection')
        self.init_signals()

    def init_signals(self):
        self.checkBox.stateChanged.connect(self.set_check)
        self.listWidget.itemSelectionChanged.connect(self.set_check_item)

    def set_check(self, value):
        state = True if value else False
        items = [self.listWidget.item(a) for a in range(self.listWidget.count())]

        for item in items:
            item.setCheckState(QtCore.Qt.Unchecked) if not state else item.setCheckState(QtCore.Qt.Checked)

    def set_check_item(self):
        item = self.listWidget.currentItem()
        # if check, deselect
        if item.checkState() == QtCore.Qt.Checked:
            state = QtCore.Qt.Unchecked
        # if not check, select
        else:
            state = QtCore.Qt.Checked
        item.setCheckState(state)


    def set_data(self, configDict):
        self.listWidget.clear()

        for namespace, data in configDict.iteritems():
            check = True if data['status'] else False
            self.add_item(namespace, check=check)

    def add_item(self, text, data=None, check=True):
        item = QtWidgets.QListWidgetItem(self.listWidget)
        item.setFlags(item.flags() | QtCore.Qt.ItemIsUserCheckable)

        item.setText(text)
        item.setData(QtCore.Qt.UserRole, data)
        item.setCheckState(QtCore.Qt.Unchecked) if not check else item.setCheckState(QtCore.Qt.Checked)

    def get_checked_mod(self):
        items = [self.listWidget.item(a) for a in range(self.listWidget.count())]
        checkedMods = [str(a.text()) for a in items if a.checkState() == QtCore.Qt.Checked]
        return checkedMods
# from rf_utils.context import context_info
# scene = context_info.ContextPathInfo()
# from rf_app.publish.scene.export import all_cache
# reload(all_cache)

# control cache selection by giving a filter namespace
# all_cache.namespaceFilter = ['arthur_001']
# data = all_cache.run(scene, 'dialogue')

# objDict {'arthur_001:Geo_Grp': {'path': u'C:/Users/TA/Documents/SevenChickMovie/scene/publ/dev/scm_dev_arthur_dialogue/anim/output/arthur_001/hero/scm_dev_arthur_dialogue_anim_arthur_001.hero.abc', 'namespace': 'arthur_001', 'type': 'abc_cache'}}
