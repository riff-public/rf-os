import sys
import os
from datetime import datetime
from rf_utils import publish_info
from collections import OrderedDict
import maya.cmds as mc
import pymel.core as pm
from rf_app.publish.scene.utils import output_utils
from rf_app.publish.scene.export import cache
from rf_app.publish.scene.export import alembic_cache
from rftool.utils import pipeline_utils
from rftool.utils import maya_utils
from rf_utils.context import context_info
from rf_app.anim.animIO import core as animIOCore
reload(animIOCore)
from rf_utils.pipeline import asset_tag
reload(asset_tag)
from rf_maya.rftool.fix import moveSceneToOrigin as msto
reload(msto)
reload(pipeline_utils)
reload(output_utils)
from rftool.on2 import on2_utils as on2
reload(on2)
import logging
logger = logging.getLogger(__name__)


def run(scene, shotName, exportFilter=[]):
    """ cache tech will be raw cache for sim (remove on2 node) """ 
    listData = OrderedDict()
    cacheGrp = scene.projectInfo.asset.geo_grp() or 'Geo_Grp'
    techGrp = scene.projectInfo.asset.tech_grp() or 'TechGeo_Grp'

    # get alembic export objDict 
    alembic_cache.set_config(scene)
    objGeoDict = alembic_cache.geo_export_dict(scene, shotName, cacheGrp, 'abc_cache')
    objTechDict = alembic_cache.geo_export_dict(scene, shotName, techGrp, 'abc_tech_cache', alembic_cache.techSuffix)
    objDict = dict(objGeoDict)
    objDict.update(objTechDict)

    
    refPaths = pipeline_utils.all_geo_ref(entity=scene, returnType='path')
    dependencyList = maya_utils.getDependenciesList() if not scene.step in ['layout', 'setdress'] else None
    shiftScene = scene.projectInfo.scene.cache_at_origin
    offsetValue, allTransforms = [], []
    if shiftScene:
        entity = scene.copy()
        entity.context.update(entityCode=shotName, entity='none')
        entity.update_scene_context()
        result = msto.get_cameraDistanceFromOrigin(entity)
        if result:
            offsetValue, allTransforms = result

    allmover = scene.projectInfo.asset.allmover_grp()
    shiftSceneData = {'shift': shiftScene, 'offset': offsetValue, 'obj': allmover}

    customPrePostDict = cache.get_custom_preroll_postroll(cacheGrp)

    for key, objData in objDict.iteritems(): 
        namespace = objData['namespace']
        cacheType = objData['type']
        ref = cache.get_ref_path(objData['originalNs'])
        cacheType = objData['type']

        preRoll = scene.projectInfo.scene.pre_roll or 30
        postRoll = scene.projectInfo.scene.post_roll or 10

        if namespace in customPrePostDict.keys(): 
            prepostData = customPrePostDict[namespace]
            preRoll, postRoll = prepostData

        funcDict = {
                'exportCache': {
                'func': exportCache,
                'funcType': cacheType,
                'dataType': 'abc_cache',
                'outputNs': namespace,
                'namespace': namespace,
                'node': cacheGrp,
                'outputKey': 'cacheTech',
                'schemaKey': ['cachePath', 'cacheGlobalHero'],
                'ref': ref, 
                'exportNs': objData['originalNs']
            }
        }

        listData[namespace] = {
            'type': 'cacheTech',
            'status': True,
            'func': output_utils.run,
            'shotName': shotName,
            'args': [funcDict, scene, exportFilter, shotName, namespace, key, preRoll, postRoll, refPaths, dependencyList, shiftSceneData, scene],
            'details': 'Export cache tech'
        }
    # listData['charB'] = {'type': 'cache', 'status': True, 'func': output_utils.run, 'shotName': shotName, 'args': [funcDict, scene, shotName, 'charA'], 'details': 'Export cache'}
    return listData


def exportCache(dstPath, shotName, namespace, exportGrp, preRoll=0, postRoll=0, refPaths=[], dependencyList=None, shiftSceneData=dict(), entity=None):
    from rftool.utils import abc_utils
    reload(abc_utils)
    from rftool.on2 import on2_utils as on2 
    reload(on2)
    
    # isolate part 
    if dependencyList: 
        pipeline_utils.isolate_reference(namespace, refPaths=refPaths, dependencyList=dependencyList)
    cache.add_description(exportGrp)
    cache.add_ref(exportGrp)
    startTime, endTime = cache.get_shot_time(shotName)
    startTime = startTime - preRoll
    endTime = endTime + postRoll

    startProcess = cache.timestamp()

    # shift scene
    shift = shiftSceneData.get('shift') or False 
    offset = shiftSceneData.get('offset')
    allmover = shiftSceneData.get('obj')


    # export cache
    with maya_utils.FreezeViewport(mode='legacy'): 
        if shift: 
            if '_Tech' in namespace:
                namespace = namespace.split('_Tech')[0]
            ctrlName = '%s:%s' %(namespace, allmover)
            ctrls = mc.ls(ctrlName, type='transform')

            if ctrls: 
                asset_tag.add_cache_tag(exportGrp, offset)
                objWithInputs, poses, pivots = animIOCore.getAllInputs(root=ctrls[0])
                # bake all attributes from "objWithInputs"
                with animIOCore.BakeConstraintsInHierarchy(objWithInputs, startTime, endTime) as bakeCons:
                    # shift forward
                    msto.offset_asset(obj=ctrls[0], offset=offset, toOrigin=True, isCache=False)

                    with on2.DisableTimeStep() as disableTimeStep: 
                        # cache 
                        abc_utils.export_abc(exportGrp, dstPath, startTime, endTime)
                    # shift back
                    msto.offset_asset(obj=ctrls[0], offset=offset, toOrigin=False, isCache=False)
            else: 
                abc_utils.export_abc(exportGrp, dstPath, startTime, endTime)


        else: 
            abc_utils.export_abc(exportGrp, dstPath, startTime, endTime)

    # export on2 data 
    path = entity.path.scheme(key='on2DataPath').abs_path()
    filename = entity.output_name(outputKey='on2_data', hero=True)
    on2_file = '{}/{}'.format(path, filename)
    print '----', on2_file, namespace
    on2.export_data(namespace, on2_file)


    # switch to cache 
    cache.duration(startProcess)
    print 'dstPath', dstPath

    return dstPath if os.path.exists(dstPath) else ''

