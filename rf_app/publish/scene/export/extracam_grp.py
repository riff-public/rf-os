import sys
import os

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
from rf_app.publish.scene.utils import maya_hook as hook
from rf_maya.lib import maya_lib
from rf_utils import publish_info
from collections import OrderedDict
import maya.cmds as mc
import pymel.core as pm
from rf_app.publish.scene.utils import output_utils
reload(output_utils)
reload(maya_lib)


def run(scene, shotName, exportFilter=[]):
    """ scene = contextPathInfo, shotName = sequencerName """
    extracam_grp = scene.projectInfo.asset.extracam_grp() or 'ExtraCam_Grp'
    listData = OrderedDict()
    if mc.objExists(extracam_grp):
        funcDict = {'extracam_grp': {
        	'func': export, 
        	'funcType': 'import_data', 
        	'dataType': 'import_data', 
        	'outputNs': 'extraCamGrp', 
        	'namespace': 'ExtraCam_Grp', 
        	'node': extracam_grp, 
        	'outputKey': 'maya', 
        	'schemaKey': ['cachePath', 'cacheGlobalHero']}}
        listData['ExtraCam_Grp'] = {
        	'type': 'import_data', 
        	'status': True, 
        	'func': output_utils.run, 
        	'shotName': shotName, 
        	'args': [funcDict, scene, exportFilter, shotName, extracam_grp], 
        	'details': 'Export grp'}
    return listData


def export(dstPath, shotName, extracam_grp):
    result = hook.export_selection(extracam_grp, dstPath)
    return dstPath if result else False
