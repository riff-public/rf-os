import sys
import os
from collections import OrderedDict
from rf_app.publish.scene.utils import maya_hook as hook
from rf_maya.lib import playblast_lib
from rf_app.publish.scene.utils import output_utils
from rf_utils.pipeline import snap_utils


def run(scene, shotName, exportFilter=[]):
	""" scene = contextPathInfo, shotName = sequencerName """
	taskName = 'cache'
	listData = OrderedDict()
	listData['set_cache_status'] = {'type': 'media', 'status': True, 'func': set_sg_status, 'shotName': shotName, 'args': [scene, taskName], 'details': 'Export shot camera'}
	return listData

def set_sg_status(scene, taskName, **kwargs):
	""" module for export playblast """
	from rf_utils.sg import sg_utils
	sg = sg_utils.sg
	status = 'apr'
	scene.context.use_sg(sg=sg, project=scene.project, entityType=scene.entity_type, entityName=scene.name)
	filters = [['project.Project.name', 'is', scene.project], 
				['entity', 'is', {'type': 'Shot', 'id': int(scene.id)}], 
				['content', 'is', taskName]]
	fields = ['content', 'id']
	taskEntity = sg.find_one('Task', filters, fields)
	data = {'sg_status_list': status}
	result = sg.update('Task', taskEntity['id'], data)
	
	message = 'Update status to %s' % status if result else 'Failed to update status'
	resultStatus = True if result else False 
	return resultStatus, message

# publish_version(ContextPathInfo, taskEntity, status, imgs, userEntity, description, uploadOption='default')
