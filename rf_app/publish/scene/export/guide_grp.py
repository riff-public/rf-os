import sys
import os

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
from rf_app.publish.scene.utils import maya_hook as hook
from rf_maya.lib import maya_lib
from rf_utils import publish_info
from collections import OrderedDict
import maya.cmds as mc
import pymel.core as pm
from rf_app.publish.scene.utils import output_utils
reload(output_utils)
reload(maya_lib)


def run(scene, shotName, exportFilter=[]):
    """ scene = contextPathInfo, shotName = sequencerName """
    guideGrp = scene.projectInfo.asset.guide_grp() or 'Guide_Grp'
    listData = OrderedDict()
    if mc.objExists(guideGrp):
        funcDict = {'guide_grp': {'func': export, 'funcType': 'ref_data', 'dataType': 'ref_data', 'outputNs': 'guideGrp', 'namespace': 'Guide_Grp', 'node': guideGrp, 'outputKey': 'maya', 'schemaKey': ['cachePath', 'cacheGlobalHero']}}
        listData['guide_grp'] = {'type': 'ref_data', 'status': True, 'func': output_utils.run, 'shotName': shotName, 'args': [funcDict, scene, exportFilter, shotName, guideGrp], 'details': 'Guide Grp'}
    return listData


def export(dstPath, shotName, exportGrp):
    result = hook.export_selection(exportGrp, dstPath)
    return dstPath if result else False
