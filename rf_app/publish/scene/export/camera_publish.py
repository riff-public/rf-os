import sys
import os

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
from rf_utils import publish_info
from collections import OrderedDict
from rf_app.publish.scene.utils import output_utils
reload(output_utils)


def run(scene, shotNode, exportFilter=[]):
    """ scene = contextPathInfo, shotName = sequencerName """
    listData = OrderedDict()
    camList = find_cam_variant(shotNode)

    for data in camList: 
        nodeName = data['master']
        shotName = data['shotName']
        variant = data['variant']

        entity = scene.copy()
        displayName = 'camera_%s' % variant
        namespace = '%s_%s_cam' % (shotName, variant)
        if variant == 'main': 
            namespace = '%s_cam' % shotName
            displayName = 'camera'
        entity.context.update(look=variant)

        funcDict = {'maya': {'func': publish_ma_cam, 'funcType': 'camera', 'dataType': 'maya', 'outputNs': namespace, 'namespace': namespace, 'outputKey': 'cam', 'outputHeroKey': 'camHero', 'schemaKey': ['cachePath', 'cacheGlobalHero']},
                    'cache': {'func': publish_abc_cam, 'funcType': 'camera', 'dataType': 'abc_cache', 'outputNs': namespace, 'namespace': namespace, 'outputKey': 'camCache', 'outputHeroKey': 'camCacheHero', 'schemaKey': ['cachePath', 'cacheGlobalHero']}, 
                    'fbx': {'func': publish_fbx_cam, 'funcType': 'camera', 'dataType': 'fbx', 'outputNs': namespace, 'namespace': namespace, 'outputKey': 'camFbx', 'outputHeroKey': 'camFbxHero', 'schemaKey': ['cachePath', 'cacheGlobalHero']}}
        listData[displayName] = {'type': 'camera', 'status': True, 'func': output_utils.run, 'shotName': shotName, 'args': [funcDict, entity, exportFilter, shotName, nodeName, entity], 'details': 'Export shot camera'}
    return listData


def publish_ma_cam(dstPath, *args):
    """ shotName use to interpret path. shotNode use to find camera within node """ 
    # last arg is entity 
    entity = args[-1]
    path = entity.path.scheme(key='localOutput').abs_path()
    name = entity.output_name(outputKey='cam', hero=True)
    _outout = '{}/{}'.format(path, name)

    # path = "P:/Hanuman/scene/work/act1/hnm_act1_q9990_s0010/finalcam/main/_output/v001/hnm_act1_q9990_s0010_finalcam_main_cam.hero.ma"
    return _outout 

def publish_abc_cam(dstPath, *args): 
    entity = args[-1]
    path = entity.path.scheme(key='localOutput').abs_path()
    name = entity.output_name(outputKey='camCacheHero', hero=True)
    _outout = '{}/{}'.format(path, name)
    
    # path = "P:/Hanuman/scene/work/act1/hnm_act1_q9990_s0010/finalcam/main/_output/v001/hnm_act1_q9990_s0010_main_cam.hero.abc"
    return _outout 

def publish_fbx_cam(dstPath, *args): 
    entity = args[-1]
    path = entity.path.scheme(key='camFbxHero').abs_path()
    name = entity.output_name(outputKey='cam', hero=True)
    _outout = '{}/{}'.format(path, name)
    # path = "P:/Hanuman/scene/publ/act1/hnm_act1_q9990_s0010/hnm_act1_q9990_s0010_main_cam.hero.fbx"
    return _outout 


def find_cam_variant(shotName): 
    return [{'master': shotName, 'shotName': shotName, 'variant': 'main'}]

    