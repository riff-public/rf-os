import sys
import os
from datetime import datetime
from rf_utils import publish_info
from collections import OrderedDict
import maya.cmds as mc
import pymel.core as pm
from rf_app.publish.scene.utils import output_utils
from rf_app.publish.scene.export import cache
from rf_app.publish.scene.export import alembic_cache
from rftool.utils import pipeline_utils
from rftool.utils import maya_utils
from rf_utils.context import context_info
reload(pipeline_utils)
reload(output_utils)
import logging
logger = logging.getLogger(__name__)


def run(scene, shotName, exportFilter=[]):
    """ scene = contextPathInfo, shotName = sequencerName """
    listData = OrderedDict()
    cacheGrp = scene.projectInfo.asset.geo_grp() or 'Geo_Grp'

    # get alembic export objDict 
    alembic_cache.set_config(scene)
    objGeoDict = geo_export_dict(scene, shotName, cacheGrp, 'rsproxy')

    refPaths = pipeline_utils.all_geo_ref(entity=scene, returnType='path')

    customPrePostDict = cache.get_custom_preroll_postroll(cacheGrp)

    for exportGrp, objData in objGeoDict.iteritems(): 
        namespace = objData['namespace']
        cacheType = objData['type']
        ref = cache.get_ref_path(objData['originalNs'])

        preRoll = scene.projectInfo.scene.pre_roll or 30
        postRoll = scene.projectInfo.scene.post_roll or 10

        if namespace in customPrePostDict.keys(): 
            prepostData = customPrePostDict[namespace]
            preRoll, postRoll = prepostData

        funcDict = {
                'exportRsproxy': {
                'func': export_rsproxy,
                'funcType': 'ma_rsproxy',
                'dataType': 'ma_rsproxy',
                'outputNs': namespace,
                'namespace': namespace,
                'node': cacheGrp,
                'outputKey': 'mayarsproxy',
                'schemaKey': ['cachePath', 'cacheGlobalHero'],
                'ref': ref, 
                'exportNs': objData['originalNs']
            }
        }

        listData[namespace] = {
            'type': 'ma_rsproxy',
            'status': True,
            'func': output_utils.run,
            'shotName': shotName,
            'args': [funcDict, scene, exportFilter, shotName, namespace, exportGrp, preRoll, postRoll],
            'details': 'Export rsproxy'
        }
    # listData['charB'] = {'type': 'cache', 'status': True, 'func': output_utils.run, 'shotName': shotName, 'args': [funcDict, scene, shotName, 'charA'], 'details': 'Export cache'}

    return listData


def export_rsproxy(dstPath, shotName, namespace, exportGrp, preRoll=0, postRoll=0):
    from rftool.render.redshift import rs_utils
    reload(rs_utils)
    from rf_utils import file_utils 

    # init context 
    entity = context_info.ContextPathInfo(path=dstPath)
    entity.context.update(process=namespace)
    rsproxyDir = entity.path.scheme(key='cacheGlobalHero').abs_path()
    rsproxy = entity.output_name(outputKey='rsproxy', hero=True)
    rsproxyPath = '%s/%s' % (rsproxyDir, rsproxy)

    
    startTime, endTime = cache.get_shot_time(shotName)
    startTime = startTime - preRoll
    endTime = endTime + postRoll
    startProcess = cache.timestamp()

    # export cache
    with maya_utils.FreezeViewport(mode='legacy'): 
        rs_utils.export_rsProxy(rsproxyPath, [exportGrp], animation=1, start=startTime, end=endTime)

    # maya template 
    templateFile = entity.projectInfo.asset.global_asset(key='rsTemplate')
    # copy template to dst 
    dstPath = file_utils.copy(templateFile, dstPath)
    dstPath = create_maya_rsproxy(entity, dstPath, rsproxyPath)

    # switch to cache 
    cache.duration(startProcess)

    return dstPath if os.path.exists(dstPath) else ''


def create_maya_rsproxy(entity, mayaFile, rsproxyFile): 
    """ search and replace rsproxy path within maya ascii """ 
    searchStr = entity.projectInfo.asset.path_dummy()
    with open(mayaFile, 'r') as hf:
        data = hf.read()
            
    data = data.replace(searchStr, rsproxyFile)
    with open(mayaFile, 'w') as hf:
        hf.write(data)

    return mayaFile


def geo_export_dict(scene, shotName, cacheGrp, cacheType, namespaceSuffix=''): 
    """ generate objDict for further use in jobString """ 
    objDict = OrderedDict()

    # filter out in view data 
    entity = scene.copy()
    entity.context.update(entityCode=shotName, entity='none')
    entity.update_scene_context()

    cacheList = alembic_cache.cache_list.CacheList(entity)
    inviewNs = cacheList.list_keys()
    print 'inviewNs', inviewNs
    namespaces = cache.get_asset_namespace(cacheGrp)
    namespaces = [a for a in namespaces if a in inviewNs] if inviewNs else namespaces
    print 'namespaces', namespaces

    # get locked information from shotgun 
    lockNs = alembic_cache.get_lock_ns(entity)
    if lockNs: 
        namespaces = [a for a in namespaces if not a in lockNs]
        logger.debug('Namespace locked ** %s' % lockNs)

    # creating objectDict for alembic export 
    for namespace in namespaces:
        # cache 
        buildNamespace = namespace if not namespaceSuffix else '%s%s' % (namespace, namespaceSuffix)
        entity = scene.copy()
        entity.context.update(entityCode=shotName, entity=entity.sg_name, process=buildNamespace)
        # entity.context.update(entity=entity.sg_name)

        ref = cache.get_ref_path(namespace)
        obj = '%s:%s' % (namespace, cacheGrp)
        dstDir = entity.path.scheme(key='cacheHeroPath').local_path()
        cacheName = entity.output_name(outputKey='cache', hero=True)
        objDict[obj] = {'path': '%s/%s' % (dstDir, cacheName), 'namespace': buildNamespace, 'type': cacheType, 'originalNs': namespace}

    return objDict
