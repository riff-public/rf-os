import sys
import os
import re
from rf_utils import publish_info
from collections import OrderedDict
import maya.cmds as mc
import pymel.core as pm
from rf_app.publish.scene.utils import output_utils
from rf_utils.context import context_info
from rf_utils import file_utils
reload(output_utils)
from rftool.utils import yeti_lib
reload(yeti_lib)
from datetime import datetime

try: 
    from Qt import QtCore
    from Qt import QtWidgets
    from Qt import QtGui
except ImportError: 
    pass 

import logging
logger = logging.getLogger(__name__)

yetiNodeFilter = []
newConfig = True
lockedNs = 'N/A'

def run(scene, shotName, exportFilter=[]):
    """ Cache Yeti nodes in one pass """
    # To export yeti node, folder name will be as following 
    # path = 'P:/SevenChickMovie/scene/publ/dev/scm_dev_arthur_dialogue/anim/output/<NAME>/<NAME>.%04d.fur'

    listData = OrderedDict()

    yetiNodes = mc.ls(type='pgYetiMaya')
    entity = scene.copy()
    entity.context.update(entityCode=shotName, entity=entity.sg_name)

    preRoll = 5 # scene.projectInfo.scene.pre_roll or 30
    postRoll = 5 # scene.projectInfo.scene.post_roll or 10
    
    # create default export list 
    set_config(scene, yetiNodes)

    # read yeti nodes selection
    selectNodes = read_active_namespace(scene)
    exportNodes = [a for a in yetiNodes if a in selectNodes]

    # filter nodes otherwise export all nodes
    exportNodes = [a for a in exportNodes if a in yetiNodeFilter] if yetiNodeFilter else exportNodes

    # check locked node
    lockNs = get_lock_ns(entity)
    
    if lockNs: 
        logger.debug('** yeti nodes locked name %s' % lockNs)
        exportNodes = filter_lock_node(exportNodes, lockNs)
    
    # output string 
    entity.context.update(process='<NAME>')
    dstDir = entity.path.scheme(key='cacheHeroPath').local_path()
    filename = entity.output_name(outputKey='cache_yeti', hero=True)
    pathString = '%s/%s' % (dstDir, filename)

    logger.info('** This is a yeti nodes to be exported %s' % exportNodes)

    listData['yeti_cache_all_nodes'] = {
            'type': 'yeti_cache',
            'status': True,
            'func': export_yeti_cache,
            'shotName': shotName,
            'args': [entity, shotName, exportNodes, pathString, preRoll, postRoll],
            'details': 'Export cache', 
            'order': 0, 
            'menu': cache_options, 
            'configFile': precache_config(scene)
        }



    # register function 
    orderCount = 1
    
    for node in exportNodes:
        transform = mc.listRelatives(node, p=True)[0]
        namespace = node.split(':')[0]
        nodeName = transform.split(':')[-1] # transform node
        ref = get_groom_path(scene, namespace)
        if not ref:
            logger.warning('Skipping namespace: %s of %s' %(namespace, node))
            continue
        name = node.split('_YetiNodeShape')[0]
        logger.info('** This is refPath %s' % ref)
        # print ref,'testRef'
        funcDict = {
            'exportCache': {
                'func': register_cache,
                'funcType': 'yeti_cache',
                'dataType': 'yeti_cache',
                'outputNs': name.replace(':', '_'),
                'node': nodeName,
                'namespace': namespace,
                'key': ('_').join(node.replace(':', '_').split('_')[0: -1]),
                'outputKey': 'cache_yeti',
                'schemaKey': ['cachePath', 'cacheGlobalHero'],
                'ref': ref, 
                'order': orderCount
            }
        }

        listData[name] = {
            'type': 'register_version',
            'status': True,
            'func': output_utils.run,
            'shotName': shotName,
            'args': [funcDict, scene, exportFilter, shotName, scene, name],
            'details': 'Export cache'
        }

        orderCount += 1

    
    # for node in yeti_nodes:
    #     transform = mc.listRelatives(node, p=True)[0]
    #     namespace = node.split(':')[0]
    #     nodeName = transform.split(':')[-1] # transform node
    #     ref = get_ref_path(namespace)
    #     name = node.split('_YetiNodeShape')[0]

    #     funcDict = {
    #         'exportCache': {
    #             'func': exportCache,
    #             'funcType': 'yeti_cache',
    #             'dataType': 'yeti_cache',
    #             'outputNs': name.replace(':', '_'),
    #             'node': nodeName,
    #             'namespace': namespace,
    #             'key': ('_').join(node.replace(':', '_').split('_')[0: -1]),
    #             'outputKey': 'cache_yeti',
    #             'schemaKey': ['cachePath', 'cacheHeroPath'],
    #             'ref': ref
    #         }
    #     }

    #     listData[name] = {
    #         'type': 'yeti_cache',
    #         'status': True,
    #         'func': output_utils.run,
    #         'shotName': shotName,
    #         'args': [funcDict, scene, exportFilter, shotName, namespace, node],
    #         'details': 'Export cache'
    #     }
    # listData['charB'] = {'type': 'cache', 'status': True, 'func': output_utils.run, 'shotName': shotName, 'args': [funcDict, scene, shotName, 'charA'], 'details': 'Export cache'}

    return listData


def export_yeti_cache(entity, shotName, nodes, pathString, preRoll=0, postRoll=0): 
    # create folder for cache output to store 
    create_output_dir(entity, nodes) 
    start, end = get_shot_time(shotName)
    start = start - preRoll
    end = end + postRoll
    sample = entity.projectInfo.render.yeti_sample()
    if not sample:
        sample = 10
    # # add attributes for chaya
    # yeti_lib.add_attribute_info(nodes)

    # cache yeti 
    yeti_lib.set_unique_name()
    yeti_lib.cache(nodes, pathString, start, end, samples=sample)

    # update config 
    update_config(entity, nodes, pathString)

    return pathString, '%s nodes exported' % len(nodes)


def register_cache(dstPath, shotName, scene, namespace): 
    """ copy file from export config path to dstPath """ 
    configFile = precache_config(scene)
    data = file_utils.ymlLoader(configFile)

    if namespace in data.keys(): 
        path = data[namespace]['path']
        
        if not dstPath == path: 
            output_utils.copy(path, dstPath)
            # remove files 
            remove_files(path)

    return dstPath

    # # if not dstPath == path: 
    # #     file_utils.xcopy_file(path, dstPath)

    # return dstPath if os.path.exists(dstPath) else None

def remove_files(path): 
    if not os.path.exists(path): 
        pattern = re.findall(r"%\d+d", path)
        if pattern: 
            basename = os.path.basename(path)
            dirname = os.path.dirname(path)
            key = basename.split(pattern[0])[0]
            files = file_utils.list_file(dirname)
            files = ['%s/%s' % (dirname, a) for a in files if key in a]

            for each in files: 
                file_utils.remove(each)

            file_utils.remove_empty_dir(dirname)


def read_config(scene): 
    path = precache_config(scene)
    data = file_utils.ymlLoader(path)
    return data 

def read_active_namespace(scene): 
    configData = read_config(scene)
    return [v['node'] for k, v in configData.iteritems() if v['status']]


def filter_lock_node(yetiNodes, lockNs): 
    nodeDict = dict()
    for node in yetiNodes: 
        nodeDict[yeti_node_name(node)] = node

    nodes = [a for a in nodeDict.keys() if not a in lockNs]
    validNodes = [nodeDict[a] for a in nodes]
    return validNodes 

def yeti_node_name(yetiNode): 
    return yetiNode.split('_YetiNodeShape')[0].replace(':', '_') 

def update_config(scene, nodes, pathString): 
    configFile = precache_config(scene)
    configData = file_utils.ymlLoader(configFile)

    for node in nodes: 
        name = node.replace(':', '_')
        checkPath = pathString.replace('<NAME>', name)
        namespace = node.split('_YetiNodeShape')[0]

        if namespace in configData.keys(): 
            configData[namespace]['path'] = checkPath

    file_utils.ymlDumper(configFile, configData)


def create_output_dir(entity, nodes): 
    for node in nodes: 
        nodeName = node.replace(':', '_')
        entity.context.update(process=nodeName)
        dstDir = '%s/%s' % (entity.path.scheme(key='cacheHeroPath').local_path(), nodeName)

        if not os.path.exists(dstDir): 
            os.makedirs(dstDir)


def get_shot_time(shotName):
    startTime = mc.shot(shotName, q=True, startTime=True)
    endTime = mc.shot(shotName, q=True, endTime=True)
    return startTime, endTime

def exportCache(dstPath, shotName, namespace, node):
    mc.refresh(su=True)
    startProcess = timestamp()
    start, end = get_shot_time(shotName)
    os.makedirs(os.path.dirname(dstPath)) if not os.path.exists(os.path.dirname(dstPath)) else None
    yeti_lib.cache(nodes, dstPath, start, end)
    mc.refresh(su=False)
    duration(startProcess)

    return dstPath

def get_shot_time(shotName):
    startTime = mc.shot(shotName, q=True, startTime=True)
    endTime = mc.shot(shotName, q=True, endTime=True)
    return startTime, endTime


def precache_config(scene): 
    path = scene.path.scheme(key='outputStepPath').local_path()
    configFile = '%s/yeti_cache_list.yml' % path
    return configFile

def clear_config(scene): 
    path = precache_config(scene)
    if os.path.exists(path): 
        os.remove(path)

def set_config(scene, yeti_nodes): 
    """ create default yeti nodes export list to use with ui """ 
    data = OrderedDict()
    for node in yeti_nodes: 
        name = node.split('_YetiNodeShape')[0]
        data[name] = {'status': True, 'path': '', 'type': 'yeti_cache', 'namespace': name, 'node': node}

    # config path 
    configFile = precache_config(scene)

    global newConfig
    createConfig = False 

    if newConfig or not os.path.exists(configFile): 
        createConfig = True
        newConfig = False

    if createConfig: 
        if not os.path.exists(os.path.dirname(configFile)): 
            os.makedirs(os.path.dirname(configFile))
        file_utils.ymlDumper(configFile, data)


def get_groom_path(scene, namespace):
    from rf_utils import register_shot
    from rf_utils import register_entity

    reg = register_shot.Register(scene)
    ns = reg.get.asset(namespace)
    if not ns:
        return
    assetDescriptionPath = ns.get('description')
    assetReg = register_entity.RegisterInfo(assetDescriptionPath)
    groomPath = assetReg.get.groom_yeti().get('heroFile')

    return groomPath
    # obj = '%s:Geo_Grp' % namespace
    # if mc.objExists(obj):
    #     obj = mc.ls(obj)[0]
    #     return mc.referenceQuery(obj, f=True) if mc.referenceQuery(obj, inr=True) else ''
    # # try searching for reference path 
    # else: 
    #     objs = mc.ls('%s:*' % namespace)
    #     for obj in objs: 
    #         if mc.referenceQuery(obj, inr=True): 
    #             path = mc.referenceQuery(obj, f=True)
    #             return path 
    # return ''

def timestamp():
    now = datetime.now()
    logger.debug('Start %s' % now)
    return now

def duration(start):
    dur = datetime.now() - start
    logger.debug('Finished in %s' % dur)
    return dur


def cache_options(itemData, *args): 
    """ show asset cache options """ 
    configFile = itemData.get('configFile')
    configData = file_utils.ymlLoader(configFile)

    cacheOption = MenuOptions()
    cacheOption.set_data(configData)
    cacheOption.exec_()

    cacheSelection = cacheOption.get_checked_mod()

    for namespace, data in configData.iteritems(): 
        data['status'] = True if namespace in cacheSelection else False

    file_utils.ymlDumper(configFile, configData)


def get_lock_ns(entity): 
    from rf_utils import register_sg
    global lockedNs
    if lockedNs == 'N/A': 
        logger.debug('Fetching shotgun for locked namespaces ...')
        reg = register_sg.ShotElement(entity)
        lockedNs = reg.list_lock(entity.step)
    
    return lockedNs


class MenuOptions(QtWidgets.QDialog):
    """docstring for Option"""
    def __init__(self, parent=None):
        super(MenuOptions, self).__init__(parent=parent)
        self.layout = QtWidgets.QVBoxLayout()
        self.listWidget = QtWidgets.QListWidget()
        self.checkBox = QtWidgets.QCheckBox('Select All')
        self.checkBox.setChecked(True)
        self.layout.addWidget(self.listWidget)
        self.layout.addWidget(self.checkBox)
        self.setLayout(self.layout)
        self.setWindowTitle('Cache asset selection')
        self.init_signals()

    def init_signals(self):
        self.checkBox.stateChanged.connect(self.set_check)
        self.listWidget.itemSelectionChanged.connect(self.set_check_item)

    def set_check(self, value):
        state = True if value else False
        items = [self.listWidget.item(a) for a in range(self.listWidget.count())]

        for item in items:
            item.setCheckState(QtCore.Qt.Unchecked) if not state else item.setCheckState(QtCore.Qt.Checked)

    def set_check_item(self):
        item = self.listWidget.currentItem()
        # if check, deselect
        if item.checkState() == QtCore.Qt.Checked:
            state = QtCore.Qt.Unchecked
        # if not check, select
        else:
            state = QtCore.Qt.Checked
        item.setCheckState(state)


    def set_data(self, configDict):
        self.listWidget.clear()

        for namespace, data in configDict.iteritems():
            check = True if data['status'] else False
            self.add_item(namespace, check=check)

    def add_item(self, text, data=None, check=True):
        item = QtWidgets.QListWidgetItem(self.listWidget)
        item.setFlags(item.flags() | QtCore.Qt.ItemIsUserCheckable)

        item.setText(text)
        item.setData(QtCore.Qt.UserRole, data)
        item.setCheckState(QtCore.Qt.Unchecked) if not check else item.setCheckState(QtCore.Qt.Checked)

    def get_checked_mod(self):
        items = [self.listWidget.item(a) for a in range(self.listWidget.count())]
        checkedMods = [str(a.text()) for a in items if a.checkState() == QtCore.Qt.Checked]
        return checkedMods
