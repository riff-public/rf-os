import sys
import os
from collections import OrderedDict
from rf_app.publish.scene.utils import maya_hook as hook
from rf_maya.lib import playblast_lib
from rf_maya.lib import sequencer_lib
from rf_app.publish.scene.utils import output_utils
from rf_utils.pipeline import snap_utils
from rf_utils.context import context_info
from rf_utils import custom_exception
from rf_utils import file_utils



def run(scene, shotName, exportFilter=[]):
	""" For split mov """
	listData = OrderedDict()
	funcDict = {'mov_split': {'func': export, 'funcType': 'mov', 'dataType': 'media', 'outputNs': '', 'namespace': 'workspace', 'outputKey': 'playblast', 'schemaKey': ['outputImgPath', 'outputHeroImgPath'], 'publish': True}}
	listData['mov_split'] = {'type': 'media', 'status': True, 'func': output_utils.run, 'shotName': shotName, 'args': [funcDict, scene, exportFilter, shotName, scene], 'details': 'Export shot camera'}
	return listData


def export(dstPath, shotName, scene, **kwargs):
	""" module for export playblast """
	if not hook.is_batch():
		from rf_utils.sg import sg_process
		scene = context_info.ContextPathInfo(path=dstPath)
		scene.sg_name
		sg = sg_process.sg
		versions = sg.find('Version', [['project.Project.name', 'is', scene.project], ['entity.Shot.code', 'is', scene.sg_name], ['sg_task.Task.content', 'is', 'layout']], ['sg_path_to_movie', 'id'])
		if versions: 
			movPath = versions[-1]['sg_path_to_movie']
			if os.path.exists(movPath): 
				file_utils.copy(movPath, dstPath)
			else: 
				raise custom_exception.PipelineError('Path not exists on latest published version "{}"'.format(movPath))
		else: 
			raise custom_exception.PipelineError('No published layout found')

	return dstPath


def export_pre_publish_data(scene, dstPath, shotName):
	snap_utils.write_media_sg(media=dstPath, shotName=shotName, step=scene.step, description='')


# publish_version(ContextPathInfo, taskEntity, status, imgs, userEntity, description, uploadOption='default')
