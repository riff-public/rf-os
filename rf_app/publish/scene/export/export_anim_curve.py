import sys
import os
from datetime import datetime
from rf_utils import publish_info
from collections import OrderedDict
import maya.cmds as mc
import pymel.core as pm
from rf_app.publish.scene.utils import output_utils
from rf_app.publish.scene.export import cache
from rf_app.publish.scene.export import alembic_cache
from rftool.utils import pipeline_utils
from rftool.utils import maya_utils
from rf_utils.context import context_info
from rf_app.anim.animIO import core as animIOCore
reload(animIOCore)
from rf_utils.pipeline import asset_tag
reload(asset_tag)
from rf_maya.rftool.fix import moveSceneToOrigin as msto
reload(msto)
reload(pipeline_utils)
reload(output_utils)
import logging
logger = logging.getLogger(__name__)


def run(scene, shotName, exportFilter=[]):
    """ scene = contextPathInfo, shotName = sequencerName """
    listData = OrderedDict()
    cacheGrp = scene.projectInfo.asset.get('allMoverCtrl') or 'AllMover_Ctrl'

    # get alembic export objDict 
    alembic_cache.set_config(scene)
    objDict = alembic_cache.geo_export_dict(scene, shotName, cacheGrp, 'abc_cache')
    refPaths = pipeline_utils.all_geo_ref(entity=scene, returnType='path')

    customPrePostDict = cache.get_custom_preroll_postroll(cacheGrp)

    for exportGrp, objData in objDict.iteritems(): 
        namespace = objData['namespace']
        cacheType = objData['type']
        ref = cache.get_ref_path(objData['originalNs'])
        cacheType = objData['type']

        preRoll = scene.projectInfo.scene.pre_roll or 30
        postRoll = scene.projectInfo.scene.post_roll or 10

        if namespace in customPrePostDict.keys(): 
            prepostData = customPrePostDict[namespace]
            preRoll, postRoll = prepostData

        funcDict = {
                'exportAnimData': {
                'func': exportAnimData,
                'funcType': cacheType,
                'dataType': 'animCurve',
                'outputNs': namespace,
                'namespace': namespace,
                'node': cacheGrp,
                'outputKey': 'animData',
                'schemaKey': ['cachePath', 'cacheGlobalHero'],
                'ref': ref, 
                'exportNs': objData['originalNs']
            }
        }

        listData['%s Curve' % namespace] = {
            'type': 'animData', # this will be a keyword in output_utils to defind data type keyword in shot_yml
            'status': True,
            'func': output_utils.run,
            'shotName': shotName,
            'args': [funcDict, scene, exportFilter, shotName, namespace, exportGrp, preRoll, postRoll],
            'details': 'Export cache'
        }
    # listData['charB'] = {'type': 'cache', 'status': True, 'func': output_utils.run, 'shotName': shotName, 'args': [funcDict, scene, shotName, 'charA'], 'details': 'Export cache'}

    return listData


def exportAnimData(dstPath, shotName, namespace, exportGrp, preRoll=0, postRoll=0):
    startTime, endTime = cache.get_shot_time(shotName)
    if not os.path.exists(os.path.dirname(dstPath)): 
        os.makedirs(os.path.dirname(dstPath))
    io = animIOCore.AnimAsset(root=exportGrp)
    io.export_anim(name=namespace, path=dstPath, start=startTime, end=endTime)

    return dstPath if os.path.exists(dstPath) else ''

