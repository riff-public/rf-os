import sys
import os
from rf_utils import publish_info
from collections import OrderedDict
import maya.cmds as mc
import pymel.core as pm
from rf_app.publish.scene.utils import output_utils
from rf_utils.context import context_info
reload(output_utils)
from rftool.utils import yeti_lib
reload(yeti_lib)
from datetime import datetime
import logging
logger = logging.getLogger(__name__)


def run(scene, shotName, exportFilter=[]):
    """ scene = contextPathInfo, shotName = sequencerName """
    listData = OrderedDict()

    list_node = mc.ls(type = 'pgYetiMaya')
    yeti_nodes = mc.ls(type='pgYetiMaya')
    for node in yeti_nodes:
        transform = mc.listRelatives(node, p=True)[0]
        namespace = node.split(':')[0]
        nodeName = transform.split(':')[-1] # transform node
        ref = get_ref_path(namespace)
        name = node.split('_YetiNodeShape')[0]

        funcDict = {
            'exportCache': {
                'func': exportCache,
                'funcType': 'yeti_cache',
                'dataType': 'yeti_cache',
                'outputNs': name.replace(':', '_'),
                'node': nodeName,
                'namespace': namespace,
                'key': ('_').join(node.replace(':', '_').split('_')[0: -1]),
                'outputKey': 'cache_yeti',
                'schemaKey': ['cachePath', 'cacheHeroPath'],
                'ref': ref
            }
        }

        listData[name] = {
            'type': 'yeti_cache',
            'status': True,
            'func': output_utils.run,
            'shotName': shotName,
            'args': [funcDict, scene, exportFilter, shotName, namespace, node],
            'details': 'Export cache'
        }
    # listData['charB'] = {'type': 'cache', 'status': True, 'func': output_utils.run, 'shotName': shotName, 'args': [funcDict, scene, shotName, 'charA'], 'details': 'Export cache'}

    return listData

def exportCache(dstPath, shotName, namespace, node):
    mc.refresh(su=True)
    startProcess = timestamp()
    start, end = get_shot_time(shotName)
    os.makedirs(os.path.dirname(dstPath)) if not os.path.exists(os.path.dirname(dstPath)) else None
    yeti_lib.set_unique_name()
    yeti_lib.cache(node, dstPath, start, end)
    mc.refresh(su=False)
    duration(startProcess)

    return dstPath

def get_shot_time(shotName):
    startTime = mc.shot(shotName, q=True, startTime=True)
    endTime = mc.shot(shotName, q=True, endTime=True)
    return startTime, endTime

def get_ref_path(namespace):
    obj = '%s:Geo_Grp' % namespace
    if mc.objExists(obj):
        obj = mc.ls(obj)[0]
        return mc.referenceQuery(obj, f=True) if mc.referenceQuery(obj, inr=True) else ''
    return ''

def timestamp():
    now = datetime.now()
    logger.debug('Start %s' % now)
    return now

def duration(start):
    dur = datetime.now() - start
    logger.debug('Finished in %s' % dur)
    return dur
