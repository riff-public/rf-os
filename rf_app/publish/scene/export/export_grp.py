import sys
import os

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
from rf_app.publish.scene.utils import maya_hook as hook
from rf_maya.lib import maya_lib
from rf_utils import publish_info
from collections import OrderedDict
import maya.cmds as mc
import pymel.core as pm
from rf_app.publish.scene.utils import output_utils
reload(output_utils)
reload(maya_lib)


def run2(scene, shotName, exportFilter=[]):
    """ scene = contextPathInfo, shotName = sequencerName """
    exportGrp = scene.projectInfo.asset.export_grp() or 'Export_Grp'
    listData = OrderedDict()
    if mc.objExists(exportGrp):
        funcDict = {'export_grp': {
            'func': export, 
            'funcType': 'import_data', 
            'dataType': 'import_data', 
            'outputNs': 'exportGrp', 
            'namespace': 'Export_Grp', 
            'node': exportGrp, 
            'outputKey': 'maya', 
            'schemaKey': ['cachePath', 'cacheGlobalHero']}}
        listData['export_grp'] = {
            'type': 'import_data', 
            'status': True, 
            'func': output_utils.run, 
            'shotName': shotName, 
            'args': [funcDict, scene, exportFilter, shotName, exportGrp], 
            'details': 'Export grp'}
    return listData


def run(scene, shotName, exportFilter=[]):
    """ scene = contextPathInfo, shotName = sequencerName """
    exportGrp = scene.projectInfo.asset.export_grp() or 'Export_Grp'
    listData = OrderedDict()
    # list grp 
    exportGrps = [a for a in mc.ls(assemblies=True) if exportGrp in a]

    for exportGrp in exportGrps: 
        if mc.objExists(exportGrp):
            funcDict = {'export_grp': {
                'func': export, 
                'funcType': 'import_data', 
                'dataType': 'import_data', 
                'outputNs': exportGrp, 
                'namespace': exportGrp, 
                'node': exportGrp, 
                'outputKey': 'maya', 
                'schemaKey': ['cachePath', 'cacheGlobalHero']}}
            listData[exportGrp] = {
                'type': 'import_data', 
                'status': True, 
                'func': output_utils.run, 
                'shotName': shotName, 
                'args': [funcDict, scene, exportFilter, shotName, exportGrp], 
                'details': 'Export grp'}
    return listData


def export(dstPath, shotName, exportGrp):
    hook.disconstraint_selection(exportGrp, shotName)
    result = hook.export_selection(exportGrp, dstPath)
    return dstPath if result else False
