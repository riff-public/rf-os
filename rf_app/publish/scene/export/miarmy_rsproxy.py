import sys
import os

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
from rf_app.publish.scene.utils import maya_hook as hook
from rf_maya.lib import maya_lib
from rf_utils import publish_info
from collections import OrderedDict
import maya.cmds as mc
import pymel.core as pm
from rf_app.publish.scene.utils import output_utils
reload(output_utils)
from rf_utils.miarmy import miarmy_utils
reload(miarmy_utils)
from rf_utils import file_utils


class Config: 
    heroSchema = 'cacheGlobalHero'
    namespace = 'miarmy'

def run(scene, shotName, exportFilter=[]):
    """ scene = contextPathInfo, shotName = sequencerName """
    listData = OrderedDict()
    if check_node(): 
        exportGrp = 'Miarmy_Contents'
        if mc.objExists(exportGrp):
            funcDict = {'maya': 
                            {
                            'func': export, 
                            'funcType': 'ref_data', 
                            'dataType': 'maya', 
                            'outputNs': Config.namespace, 
                            'namespace': Config.namespace, 
                            'node': exportGrp, 
                            'outputKey': 'miarmyRsMaya', 
                            # 'outputHeroKey': 'miarmyRsMaya', 
                            'schemaKey': ['cachePath', Config.heroSchema]
                            }
                        }
            listData['miarmy_rsproxy'] = {
                                        'type': 'miarmy_rscache', 
                                        'status': True, 
                                        'func': output_utils.run, 
                                        'shotName': shotName, 
                                        'args': [funcDict, scene, exportFilter, shotName, exportGrp, scene.copy()], 'details': 'Miarmy RsProxy'
                                    }
    return listData


def export(dstPath, shotName, exportGrp, entity=None):
    name = 'data'
    path, filename = os.path.split(dstPath)
    basename, ext = os.path.splitext(filename)
    startTime, endTime = get_shot_time(shotName)
    
    if not os.path.exists(path): 
        os.makedirs(path)
    
    rsNodes = miarmy_utils.cache_redshift(path, name, startTime, endTime)
    if rsNodes: 
        logger.debug('Finished export cache data "%s"' % path)
        dataPath = '%s/%s' % (path, name)
        # copy_data(entity, dataPath)

        glob = miarmy_utils.list_global()
        brain = miarmy_utils.list_brain()
        bp = miarmy_utils.list_brain_post()

        exportNodes = miarmy_utils.list_contents() + rsNodes + glob + brain + bp
        # exportNodes = miarmy_utils.list_agents() + rsNodes
        result = hook.export_selection(exportNodes, dstPath)
        logger.debug('Finished export node to maya file "%s"' % dstPath)
    else: 
        logger.error('Failed to cache redshift proxy')
    return dstPath


def check_node(): 
    return True

def copy_data(entity, srcPath): 
    entity.context.update(process=Config.namespace)
    dstPath = entity.path.scheme(Config.heroSchema).abs_path()
    print '---------------- copy', srcPath, dstPath
    shutil.copytree(srcPath, dstPath)
    # file_utils.xcopy_directory(srcPath, dstPath)
    logger.debug('Copy finished %s' % dstPath)
    return os.path.exists(dstPath)


def get_require_nodes(): 
    miarmy_utils.find_agent_loco()
    miarmy_utils.list_rsproxy()


def get_shot_time(shotName):
    # startTime = mc.shot(shotName, q=True, startTime=True)
    # endTime = mc.shot(shotName, q=True, endTime=True)
    startTime = mc.playbackOptions(q=True, minTime=True)
    endTime = mc.playbackOptions(q=True, maxTime=True)
    return startTime, endTime
