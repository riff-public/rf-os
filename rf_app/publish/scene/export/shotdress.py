import sys
import os

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
from rf_app.publish.scene.utils import maya_hook as hook
from rf_maya.lib import maya_lib
from rf_utils import publish_info
from collections import OrderedDict
import maya.cmds as mc
import pymel.core as pm
from rf_app.publish.scene.utils import maya_hook as hook
from rf_app.publish.scene.utils import output_utils
from rf_app.asm import asm_lib
reload(output_utils)
reload(maya_lib)


def run(scene, shotName, exportFilter=[]):
    """ scene = contextPathInfo, shotName = sequencerName """
    shotDressGrp = scene.projectInfo.asset.shotdress_grp() or 'ShotDress_Grp'
    if mc.objExists(shotDressGrp):
        listData = OrderedDict()
        funcDict = {'shotdress': {'func': export, 'funcType': 'shotdress', 'dataType': 'shotdress', 'outputNs': 'shotdress', 'namespace': 'shotdress', 'outputKey': 'shotDress', 'schemaKey': ['cachePath', 'cacheGlobalHero']}}
        listData['shotdress'] = {'type': 'asm', 'status': True, 'func': output_utils.run, 'shotName': shotName, 'args': [funcDict, scene, exportFilter, shotName, shotDressGrp], 'details': 'Export grp'}
        return listData


def export(dstPath, shotName, exportGrp):
    shotdress_set = '{}_ShotDressSet'.format(shotName)

    if not mc.objExists(shotdress_set): 
        logger.info('Export all ShotDress_Grp')
        result = asm_lib.export(exportGrp, dstPath, 'copy')
    else: 
        logger.info('Export from {}'.format(shotdress_set))
        mc.select(shotdress_set)
        result = asm_lib.export(exportGrp, dstPath, 'copy', selection=True)
    # result = hook.export_selection(exportGrp, dstPath)
    return dstPath if result else False
