import sys
import os

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
from rf_app.publish.scene.utils import maya_hook as hook
from rf_maya.lib import maya_lib
from rf_utils import publish_info
from collections import OrderedDict
import maya.cmds as mc
import pymel.core as pm
from rf_app.publish.scene.utils import output_utils
reload(output_utils)
reload(maya_lib)


def run(scene, shotName, exportFilter=[]):
    """ scene = contextPathInfo, shotName = sequencerName """
    crowdRsGrp = scene.projectInfo.asset.crowd_rs_grp() or 'CrowdRs_Grp'
    listData = OrderedDict()
    if mc.objExists(crowdRsGrp):
        funcDict = {'export_crowd_rs_grp': {'func': export, 'funcType': 'import_data', 'dataType': 'import_data', 'outputNs': 'crowdRsGrp', 'namespace': 'CrowdRs_Grp', 'node': crowdRsGrp, 'outputKey': 'maya', 'schemaKey': ['cachePath', 'cacheGlobalHero']}}
        listData['export_crowd_rs_grp'] = {'type': 'import_data', 'status': True, 'func': output_utils.run, 'shotName': shotName, 'args': [funcDict, scene, exportFilter, shotName, crowdRsGrp], 'details': 'Export grp'}
    return listData


def export(dstPath, shotName, crowdRsGrp):
    result = hook.export_selection(crowdRsGrp, dstPath)
    return dstPath if result else False
