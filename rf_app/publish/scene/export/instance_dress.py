import sys
import os

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
from rf_app.publish.scene.utils import maya_hook as hook
from rf_maya.lib import maya_lib
from rf_utils import publish_info
from collections import OrderedDict
import maya.cmds as mc
import pymel.core as pm
from rf_app.publish.scene.utils import maya_hook as hook
from rf_app.publish.scene.utils import output_utils
from rf_app.asm import asm_lib
from rftool.utils import instance_utils
reload(instance_utils)
reload(output_utils)
reload(maya_lib)


def run(scene, shotName, exportFilter=[]):
    """ scene = contextPathInfo, shotName = sequencerName """
    instanceGrp = scene.projectInfo.asset.instance_grp() or 'Instance_Grp'
    if mc.objExists(instanceGrp):
        listData = OrderedDict()
        funcDict = {'instdress': {'func': export, 'funcType': 'instdress', 'dataType': 'instdress', 'outputNs': 'instdress', 'namespace': 'instdress', 'outputKey': 'instDress', 'schemaKey': ['cachePath', 'cacheHeroPath']}}
        listData['instance_dress'] = {'type': 'maya', 'status': True, 'func': output_utils.run, 'shotName': shotName, 'args': [funcDict, scene, exportFilter, shotName, instanceGrp], 'details': 'Export grp'}
        return listData


def export(dstPath, shotName, exportGrp):
    dirname = os.path.dirname(dstPath)
    if not os.path.exists(dirname): 
        os.makedirs(dirname)
    result = instance_utils.export_instance_shotDress(dstPath)
    return dstPath if result else False
