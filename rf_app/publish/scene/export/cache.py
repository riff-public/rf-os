import sys
import os
from datetime import datetime
from rf_utils import publish_info
from collections import OrderedDict
import maya.cmds as mc
import pymel.core as pm
from rf_app.publish.scene.utils import output_utils
from rf_qc.maya_lib.scene import add_asset_tag
from rf_utils.context import context_info
from rftool.utils import pipeline_utils
reload(output_utils)
from rf_utils.pipeline import cache_utils
from rf_utils import custom_exception
reload(cache_utils)
import logging
logger = logging.getLogger(__name__)


def run(scene, shotName, exportFilter=[]):
    """ scene = contextPathInfo, shotName = sequencerName """
    listData = OrderedDict()
    cacheGrp = scene.projectInfo.asset.geo_grp() or 'Geo_Grp'
    namespaces = get_asset_namespace(cacheGrp)
    preRoll = scene.projectInfo.scene.pre_roll or 30
    postRoll = scene.projectInfo.scene.post_roll or 10

    customPrePostDict = get_custom_preroll_postroll(cacheGrp)

    for namespace in namespaces:
        ref = get_ref_path(namespace)
        funcDict = {
            'exportMaya': {
                'func': export,
                'funcType': 'maya',
                'dataType': 'maya',
                'outputNs': namespace,
                'namespace': namespace,
                'node': cacheGrp,
                'outputKey': 'maya',
                'schemaKey': ['cachePath', 'cacheHeroPath'],
                'ref': ref
            },
            'exportCache': {
                'func': exportCache,
                'funcType': 'abc_cache',
                'dataType': 'abc_cache',
                'outputNs': namespace,
                'namespace': namespace,
                'node': cacheGrp,
                'outputKey': 'cache',
                'schemaKey': ['cachePath', 'cacheHeroPath'],
                'ref': ref
            }
        }

        listData[namespace] = {
            'type': 'cache',
            'status': True,
            'func': output_utils.run,
            'shotName': shotName,
            'args': [funcDict, scene, exportFilter, shotName, namespace, preRoll, postRoll],
            'details': 'Export cache'
        }
    # listData['charB'] = {'type': 'cache', 'status': True, 'func': output_utils.run, 'shotName': shotName, 'args': [funcDict, scene, shotName, 'charA'], 'details': 'Export cache'}

    return listData


def export(dstPath, shotName, namespace, *args):
    """ module for export cache """
    os.makedirs(os.path.dirname(dstPath)) if not os.path.exists(os.path.dirname(dstPath)) else None
    mc.select('%s:Geo_Grp' % namespace)
    result = pm.exportSelected(dstPath, preserveReferences=True, f=True)
    mc.select(cl=True)
    return result


def exportCache(dstPath, shotName, namespace, preRoll=0, postRoll=0):
    from rftool.utils import abc_utils
    reload(abc_utils)
    
    sel_obj = '%s:Geo_Grp' % namespace
    add_description(sel_obj)
    add_ref(sel_obj)
    startTime, endTime = get_shot_time(shotName)
    startTime = startTime - preRoll
    endTime = endTime + postRoll

    # freeze viewport 
    mc.refresh(su=True)
    # start time
    startProcess = timestamp()

    # export cache
    abc_utils.export_abc(sel_obj, dstPath, startTime, endTime)

    # unfreeze viewport 
    mc.refresh(su=False)
    duration(startProcess)

    return dstPath if os.path.exists(dstPath) else ''


def get_asset_namespace(cacheGrp):
    namespace = []
    wrongAssets = []
    char_geo_grp = mc.ls('*:%s' % cacheGrp)
    
    for char in char_geo_grp:
        # check if not empty group 
        if mc.listRelatives(char, c=True): 
            is_ref = mc.referenceQuery(char, inr=True)
            
            if is_ref:
                # check if rig or abc 
                path = mc.referenceQuery(char, f=True).split('{')[0]
                basename, ext = os.path.splitext(path)
                ref_node_namspace = mc.referenceQuery(char, namespace=True)
                
                if not ext == '.abc': 
                    namespace.append(ref_node_namspace[1:])
                # if abc 
                else: 
                    entity = context_info.ContextPathInfo(path=path)
                    if entity.valid: 
                        # allow if asset 
                        if entity.entity_type == context_info.ContextKey.asset: 
                            namespace.append(ref_node_namspace[1:])

                        # if scene 
                        else: 
                            # allow if workspace 
                            cacheAsset = cache_utils.Cache(ref_node_namspace)
                            if cacheAsset.is_workspace(): 
                                namespace.append(ref_node_namspace[1:])
                    else: 
                        logger.warning('Invalid asset path "%s"' % path)
                        wrongAssets.append(path)


    if wrongAssets: 
        logger.warning('---------- Error Asset ----------')
        for asset in wrongAssets: 
            logger.warning(asset)
        logger.warning('%s wrong assets' % len(wrongAssets))
        logger.warning('----------------------------')

    return namespace

def get_all_asset_namespace(cacheGrp): 
    namespaces = []
    char_geo_grp = mc.ls('*:%s' % cacheGrp)
    
    for char in char_geo_grp:
        # check if not empty group 
        if mc.listRelatives(char, c=True): 
            is_ref = mc.referenceQuery(char, inr=True)
            
            if is_ref:
                # check if rig or abc 
                path = mc.referenceQuery(char, f=True).split('{')[0]
                basename, ext = os.path.splitext(path)
                ref_node_namspace = mc.referenceQuery(char, namespace=True)
                namespaces.append(ref_node_namspace[1:])

    return namespaces


def get_custom_preroll_postroll(cacheGrp): 
    """ get custom PreRoll PostRoll data """ 
    char_geo_grp = mc.ls('*:%s' % cacheGrp)
    infoDict = dict()

    for char in char_geo_grp:
        # check if not empty group 
        if mc.listRelatives(char, c=True): 
            is_ref = mc.referenceQuery(char, inr=True)
            
            if is_ref:
                # check if rig or abc 
                path = mc.referenceQuery(char, f=True)
                ref_node_namspace = mc.referenceQuery(char, namespace=True)
                asset = context_info.ContextPathInfo(path=path)
                if asset.valid: 
                    data = pipeline_utils.asset_preroll_postroll(asset)

                    if data: 
                        infoDict[ref_node_namspace[1:]] = data 
                else:
                    logger.warning('Asset "%s" is not in the pipeline, Skip!!' % path)

    return infoDict


def get_ref_path(namespace):
    obj = '%s:Geo_Grp' % namespace
    if mc.objExists(obj):
        attr_name = '%s.refPath'%(obj)
        if mc.objExists(attr_name):
            return mc.getAttr('%s.refPath'% obj)
        else:
            obj = mc.ls(obj)[0]
            return mc.referenceQuery(obj, f=True) if mc.referenceQuery(obj, inr=True) else ''
    return ''


def get_shot_time(shotName):
    startTime = mc.shot(shotName, q=True, startTime=True)
    endTime = mc.shot(shotName, q=True, endTime=True)
    return startTime, endTime


def add_description(geoGrp):
    attrName = 'adPath'
    attr = '%s.%s' % (geoGrp, attrName)
    if mc.objExists(attr):
        ad_path = mc.getAttr(attr)
        #####################  dont do if already have ad_path
        if ad_path:
            return
        ###################
    path = mc.referenceQuery(geoGrp, f=True)
    asset = context_info.ContextPathInfo(path=path)
    description = ''
    if asset.entity_type == 'asset':
        description = '%s/%s' % (asset.path.asm_data(), asset.ad_name)
    if not mc.objExists(attr):
        mc.addAttr(geoGrp, ln=attrName, dt='string')
        mc.setAttr(attr, e=True, keyable=True)
    mc.setAttr(attr, description, type='string')

def add_ref(geoGrp):
    attrName = 'refPath'
    attr = '%s.%s' % (geoGrp, attrName)
    #####################  dont do if already have ref_path
    if mc.objExists(attr):
        ref_path = mc.getAttr(attr)
        if ref_path:
            return
    #####################
    path = mc.referenceQuery(geoGrp, f=True).split('{')[0]
    newPath = add_asset_tag.filter_ref_path(path)
    path = newPath if newPath else path
    if not mc.objExists(attr):
        mc.addAttr(geoGrp, ln=attrName, dt='string')
        mc.setAttr(attr, e=True, keyable=True)
    mc.setAttr(attr, path, type='string')


def timestamp():
    now = datetime.now()
    logger.debug('Start %s' % now)
    return now

def duration(start):
    dur = datetime.now() - start
    logger.debug('Finished in %s' % dur)
    return dur
