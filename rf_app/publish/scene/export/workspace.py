import sys
import os

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from collections import OrderedDict
from rf_app.publish.scene.utils import maya_hook as hook
reload(hook)
from rf_maya.rftool.layout.split_shot import split_shot_cmd
from rf_app.publish.scene.utils import output_utils
# reload(output_utils)
from rf_maya.rftool.utils import objectsInCameraView as oic


def run(scene, shotName, exportFilter=[]):
    """ scene = contextPathInfo, shotName = sequencerName """
    listData = OrderedDict()
    funcDict = {'workspace': {'func': export, 'funcType': 'workspace', 'dataType': 'workspace', 'outputNs': 'workspace', 'namespace': 'workspace', 'outputKey': '', 'schemaKey': ['cachePath', 'cacheHeroPath']}}
    listData['workspace'] = {'type': 'workspace', 'status': True, 'func': output_utils.run, 'shotName': shotName, 'args': [funcDict, scene, exportFilter, shotName], 'details': 'Export workspace'}
    return listData


def export(dstPath, shotName):
    """ module for export workspace """
    logger.info('Exporting workspace ...')
    # go to current shot, set start & end frame each shot

    # select only polygon 
    hook.set_selection_filter()
    # find cam
    camRig = hook.find_cam_rig(shotName)
    hook.set_seq_current_time(1001)
    start, end = hook.shot_time(shotName)
    shots = hook.list_shot()
    setGrp = hook.add_asm_set()
    shotdressGrp = hook.add_shotdress()
    # print shots
    hook.look_thru(camRig)
    #remove Current Cam
    list_del_cam = remove_curCam_in_camseq(list(shots), shotName)

    hook.clearSelection()


    # select Object
    list_frame_selection = listFrame(int(start), int(end), 5)
    for index, frame in enumerate(list_frame_selection):
        hook.changeFrame(frame)
        if index == 0:
            panel = hook.get_panel()
            hook.set_panel_show_only_polygon(panel)
        hook.select_viewport_objects()
        selection = hook.listSelection()
        hook.addSelection(selection)
    # res = oic.getSeenObjects(cameraName=camRig, 
    #         timeRange=[int(start), int(end)], 
    #         step=5, 
    #         viewport='vp2')

    # hook.addSelection(list(res))

    # GET A LIST OF REFERENCES
    hook.addSelection(shotName)
    hook.addSelection(camRig)
    hook.addSelection(setGrp) if setGrp else None
    hook.addSelection(shotdressGrp) if shotdressGrp else None
    hook.addSelection(list_sound_append(shotName))
    # import audio
    #
    ##
    #
    hook.add_rig_grp_from_selection()
    
    print 'export Item:', hook.listSelection() 
    #####################
    os.makedirs(os.path.dirname(dstPath)) if not os.path.exists(os.path.dirname(dstPath)) else None
    hook.exportSelected(dstPath)
    ###################

    # record namespaces
    hook.record_namespace_from_selection(shotName)
    #writemayafile (setstart,end time slider)
    writeFileMasceneConfig(dstPath, start, end)
    #writemayafile (connect node sequncer)
    writeFileMaShot(dstPath, shotName, shots)
    #add Current Cam
    assign_curCam(list_del_cam)

    return dstPath

def listFrame(start, end, skipFrameCount):
    frameRange = list(range(start, end+1))
    list_frame_selection = []
    for index, frame in enumerate(frameRange):
        if (index % skipFrameCount == 0):
            list_frame_selection.append(frame)
        else:
            pass
    return list_frame_selection

def writeFileMasceneConfig(pathFile, start, end):
    file = open(pathFile,"a")
    sceneConfig = 'createNode script -name "sceneConfigurationScriptNode";setAttr ".before" -type "string" "playbackOptions -min %s -max %s -ast %s -aet %s ";setAttr ".scriptType" 6;' % (start, end, start, end)
    file.write(sceneConfig)
    file.close()

def writeFileMaShot(pathFile, shotName, list_shot):
    file = open(pathFile,"a")
    createNode = '\ncreateNode sequencer -n "sequencer1";'
    file.write(createNode)

    for index, shot in enumerate(list_shot):
        if index == 0:
            sceneConfig = '\nconnectAttr "%s.se" "sequencer1.shts" -na;'%(shot)
            file.write(sceneConfig)
        else:
            sceneConfig = '\nconnectAttr "%s.msg" "sequencer1.shts" -na;'%(shot)
            file.write(sceneConfig)
    mayaNode = '\nnconnectAttr "sequencer1.msg" ":sequenceManager1.seqts" -na;'
    file.write(mayaNode)
    file.close()


def remove_curCam_in_camseq(shots_all, shotName):
    list_shot_camera = {}
    if shotName in shots_all:
        shots_all.remove(shotName)
    for shotName in shots_all:
        cam_shot = hook.find_cam(shotName)
        cam_shape = hook.find_cam_shape(cam_shot)
        if cam_shot:
            possible_nodes = cam_shape + [cam_shot]
            for cam in possible_nodes:
                hook.disconnect_attr_cam(cam, shotName)
            list_shot_camera[shotName] = cam_shape[0]
    return list_shot_camera

def assign_curCam(list_shot_camera):
    for (key,value) in list_shot_camera.iteritems():
        hook.connect_attr_cam(value, key)

def list_sound_append(shotName):
    start,end,seq_start,seq_end = hook.shot_info_data(shotName)
    lst_sound = hook.lst_audio()
    lst_audio = []
    sound_node = '%s_sound'%shotName
    for sound_node in lst_sound:
        sound_start = hook.get_attr(sound_node,'offset')
        sound_end = int(hook.get_attr(sound_node, 'endFrame'))
        if sound_start == seq_start:
            lst_audio.append(sound_node)
        elif sound_end > seq_start and sound_end < seq_end:
            lst_audio.append(sound_node)
        elif sound_start > seq_start and sound_start < seq_end:
            lst_audio.append(sound_node)

    return lst_audio