import sys
import os
import subprocess
import re
import tempfile
import json
from rf_utils import publish_info
from collections import OrderedDict
import maya.cmds as mc
import pymel.core as pm
from rf_app.publish.scene.utils import output_utils
from rf_utils.context import context_info
from rf_utils import file_utils
import rf_config as config 

reload(output_utils)
from rftool.utils import export_yeti_cache
reload(export_yeti_cache)
from datetime import datetime

try: 
    from Qt import QtCore
    from Qt import QtWidgets
    from Qt import QtGui
except ImportError: 
    pass 

import logging
logger = logging.getLogger(__name__)

yetiNodeFilter = []
newConfig = True

def run(scene, shotName, exportFilter=[]):
    """ Cache Yeti nodes in one pass """
    # To export yeti node, folder name will be as following 
    # path = 'P:/SevenChickMovie/scene/publ/dev/scm_dev_arthur_dialogue/anim/output/<NAME>/<NAME>.%04d.fur'

    listData = OrderedDict()
    sceneName = mc.file(q=True, sn=True)

    listData['yeti_cache_bg'] = {
            'type': 'bg_process',
            'status': True,
            'func': export_yeti,
            'shotName': shotName,
            'args': [sceneName],
            'details': 'Export cache', 
            'order': 9
        }

    return listData


def export_yeti(sceneName): 
    mayapy = config.Software.data.get('Maya2017').get('mayapy')
    hndl, logPath = tempfile.mkstemp('.temp')
    # logPath = logPath.replace('\\', '/')
    # print logPath

    py =  '%s/core/rf_maya/rftool/utils/export_yeti_cache.py' % os.environ['RFSCRIPT']
    p = subprocess.Popen(['%s' % mayapy, '%s' % py, '-s', '%s' % sceneName, '-l', '%s' %logPath], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    stdout, stderr = p.communicate()
    logger.debug(stdout)
    logger.error(stderr)
    # print stdout

    result = dict()
    # splitter = '::::: RESULT :::::'
    # print stdout
    # splits = stdout.split(splitter)
    # if len(splits) > 0:
    #     result = splits[1]
    # p.wait()
    try:
        with open(logPath, 'r') as f:
            result = json.load(f)
        os.remove(logPath)
    except: 
        pass

    print 'THIS IS RESULT :::' 
    if hasattr(result, 'iteritems'): 
        for k, v in result.iteritems(): 
            logger.debug('namespace %s - %s' % (k, v))
    
    if result: 
        allResult = all(v for k, v in result.iteritems())
    else: 
        logger.error('** No result returned')
        allResult = False 
    message = 'Success' if allResult else 'Failed. See log path for details %s' % logPath

    return allResult, message
    

# import subprocess 
# subprocess.call([
#     'C:/Program Files/Autodesk/Maya2017/bin/mayapy.exe',
#     'D:/Dropbox/script_server/core/rf_maya/rftool/utils/export_yeti_cache.py',
#     '-s', 
#     'P:/projectName/scene/work/ep01/pr_ep01_q0070_s0010/anim/main/maya/pr_ep01_q0070_s0010_anim_main.v008.TA.ma', 
    
#     # '{dstFile}'.format(dstFile=dstFile),
#     ])