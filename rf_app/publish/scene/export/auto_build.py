import sys
import os
from collections import OrderedDict
from rftool.utils import pipeline_utils
from rf_utils import file_utils
from rf_app.asm import asm_lib


def run(scene, shotName, exportFilter=[]):
    """ scene = contextPathInfo, shotName = sequencerName """
    listData = OrderedDict()
    if scene.step == 'layout':
        listData['auto_build'] = {'type': 'file', 'status': True, 'func': build_anim, 'shotName': shotName, 'args': [scene, shotName], 'details': 'Update asset list to Shotgun'}
    return listData

def build_anim(scene, shotName, **kwargs):
    """ module for copy split file to anim workspace """
    scene.context.update(entityCode=shotName)
    scene.context.update(entity=scene.sg_name)

    splitPath = scene.path.publish_workspace_hero().abs_path()
    splitFile = scene.publish_name(hero=True)
    src = '%s/%s' % (splitPath, splitFile)
    scene2 = scene.copy()
    steps = ['layout', 'anim']
    increments = []
    mesg = ''
    for step in steps:
        scene2.context.update(step=step, version='v001')

        animWorkspace = scene2.path.workspace().abs_path()
        scene2.context.update(process='split')
        filename = scene2.work_name()
        dst = '%s/%s' % (animWorkspace, filename)
        increment = file_utils.increment_file(dst)
        scene2.context.update(process='main')

        if os.path.exists(src):
            file_utils.copy(src, increment)
            mesg = 'Success'
            increments.append(increment) if os.path.exists(increment) else None
            print 'copy %s -> %s', (src, increment)
            print os.path.exists(increment)
        else:
            mesg = 'published file not found %s' % src
        if not os.path.exists(increment):
            mesg += ' file not copy %s increment' % increment
    return '\n'.join(increments), mesg


