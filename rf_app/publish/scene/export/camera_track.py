import sys
import os

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
from rf_app.publish.scene.utils import maya_hook as hook
from rf_maya.lib import maya_lib
from rf_maya.rftool.utils import maya_utils
from rf_utils import publish_info
from collections import OrderedDict
import maya.cmds as mc
import pymel.core as pm
from rf_app.publish.scene.utils import output_utils
reload(output_utils)
reload(maya_lib)


def run(scene, shotName, exportFilter=[]):
    """ scene = contextPathInfo, shotName = sequencerName """
    listData = OrderedDict()

    namespace = '%s_cam' % shotName
    # assume tracking camera always main
    scene.context.update(look='main')
    funcDict = {'maya': {'func': export, 'funcType': 'camera', 'dataType': 'maya', 'outputNs': namespace, 'namespace': namespace, 'outputKey': 'cam', 'outputHeroKey': 'camHero', 'schemaKey': ['cachePath', 'cacheGlobalHero']},
                'cache': {'func': exportCache, 'funcType': 'camera', 'dataType': 'abc_cache', 'outputNs': namespace, 'namespace': namespace, 'outputKey': 'camCache', 'outputHeroKey': 'camCacheHero', 'schemaKey': ['cachePath', 'cacheGlobalHero']}}
    listData['camera'] = {'type': 'camera', 'status': True, 'func': output_utils.run, 'shotName': shotName, 'args': [funcDict, scene, exportFilter, shotName], 'details': 'Export shot camera'}
    return listData


def export(dstPath, shotName):
    from rf_maya.rftool.movie_image import app as img_plane
    list_shot = mc.sequenceManager(lsh=True)
    img_plane.close_all_movie_img_plane(list_shot)
    return hook.export_camera(dstPath, shotName)


def exportCache(dstPath, shotName):
    from rftool.utils import abc_utils
    reload(abc_utils)

    if not pm.objExists(shotName) or mc.objectType(shotName) != 'shot':
        logger.error('Cannot find shot node: %s' %shotName)
        return

    # get range from shotName, looking up the sequencer
    start, end, time = hook.list_shot_status(shotName)

    # find cam linked to seqencer
    cam = hook.current_camera(shotName=shotName)
    if not cam:
        logger.error('Cannot find shot camera for: %s' %shotName)
        return ''

    cam = pm.PyNode(cam)
    # make sure we have cam(transform) and camShape
    if cam.getShape():
        camShp = cam.getShape()
    else:
        camShp = cam
        cam = camShp.getParent()

    if cam:
        # unparent
        camParent = cam.getParent()
        order = camParent.getChildren(type='transform').index(cam)
        oldName = cam.nodeName()
        pm.parent(cam, w=True)

        # find and get plate attr
        plateTr = camShp.imagePlane[0].inputs()

        if plateTr:
            plateTr = plateTr[0]
            plate = plateTr.getShape()
            # get plate attr values as string
            attrNames = ('displayMode', 'type', 'textureFilter', 'imageName', 'useFrameExtension',
                'frameOffset', 'frameCache', 'frameIn', 'frameOut', 'fit', 'size',
                'squeezeCorrection', 'offset', 'depth', 'rotate', 'coverageX', 'coverageY', 
                'coverageOriginX', 'coverageOriginY', 'imageCenter', 'width', 'height', 'maintainRatio')
            attrDict = {}
            for attrName in attrNames:
                attr = plate.attr(attrName)
                attrSn = str(attr.shortName())
                attrValue = attr.get()
                if isinstance(attrValue, (unicode, str)):
                    attrValue = str(attrValue)
                elif isinstance(attrValue, (pm.dt.Vector)):
                    attrValue = list(attrValue)
                attrDict[attrSn] = attrValue
            attrStr = str(attrDict)

            # add and set plate attr on camShape
            if not camShp.hasAttr('plateInfo'):
                pm.addAttr(camShp, ln='plateInfo', dt='string')
            camShp.plateInfo.set(attrStr)

        # rename
        cam.rename('camshot')

        # export ABC
        currentRenderer = maya_utils.getCurrentViewportRenderer()
        with maya_utils.FreezeViewport(mode=currentRenderer) as freezeVp:
            # abc_utils.exportABC(cam, dstPath, shot_info)
            abc_utils.export_abc(obj=cam.shortName(), 
                    path=dstPath, 
                    start=start, 
                    end=end,
                    extraAttrs=['plateInfo'])

        # reparent
        pm.parent(cam, camParent)
        # reorder
        currentIndex = camParent.getChildren(type='transform').index(cam)
        pm.reorder(cam, r=(order - currentIndex))

        # rename back to old name
        cam.rename(oldName)
 
        if os.path.exists(dstPath):
            logger.info('Export cache camera success %s' % dstPath)
            return dstPath
        else:
            return ''

