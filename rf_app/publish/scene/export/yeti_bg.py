import sys
import os
import subprocess
import re
import tempfile
import json
from rf_utils import publish_info
from collections import OrderedDict
import maya.cmds as mc
import pymel.core as pm
from rf_app.publish.scene.utils import output_utils
from rf_utils.context import context_info
from rf_utils import file_utils
import rf_config as config 

reload(output_utils)
from rftool.utils import export_yeti_cache
reload(export_yeti_cache)
from datetime import datetime
from rf_utils import register_shot
from rf_app.publish.scene.export import cache
from rf_app.publish.scene.export import alembic_cache


try: 
    from Qt import QtCore
    from Qt import QtWidgets
    from Qt import QtGui
except ImportError: 
    pass 

import logging
logger = logging.getLogger(__name__)

yetiNodeFilter = []
newConfig = True
lockedNs = 'N/A'

def run(scene, shotName, exportFilter=[]):
    """ Cache Yeti nodes in one pass """
    # To export yeti node, folder name will be as following 
    # path = 'P:/SevenChickMovie/scene/publ/dev/scm_dev_arthur_dialogue/anim/output/<NAME>/<NAME>.%04d.fur'
    listData = OrderedDict()
    namespaces = get_export_namespaces(scene)
    sceneName = mc.file(q=True, sn=True)

    for i, namespace in enumerate(namespaces): 

        listData['%s_yeti' % namespace] = {
                'type': 'yeti_bg_process',
                'status': True,
                'func': export_yeti,
                'shotName': shotName,
                'args': [sceneName, namespace],
                'details': 'Export cache', 
                'order': 9+i
            }

    return listData


def export_yeti(sceneName, namespace): 
    version = mc.about(v=True)
    mayapy = config.Software.data.get('Maya%s' % version).get('mayapy')
    hndl, logPath = tempfile.mkstemp('.temp')
    entity = context_info.ContextPathInfo(path=sceneName)
    # logPath = logPath.replace('\\', '/')
    # print logPath
    envStr = entity.projectInfo.get_project_env(dcc='maya', dccVersion=version, department='Py')
    py =  '%s/core/rf_maya/rftool/utils/export_yeti_cache.py' % os.environ['RFSCRIPT']
    p = subprocess.Popen(['%s' % mayapy, '%s' % py, '-s', '%s' % sceneName, '-l', '%s' % logPath, '-n', '%s' % namespace], stdout=subprocess.PIPE, stderr=subprocess.STDOUT, env=envStr)
    stdout, stderr = p.communicate()
    logger.debug(stdout)
    logger.error(stderr)
    # print stdout

    result = dict()
    # splitter = '::::: RESULT :::::'
    # print stdout
    # splits = stdout.split(splitter)
    # if len(splits) > 0:
    #     result = splits[1]
    # p.wait()
    try:
        with open(logPath, 'r') as f:
            result = json.load(f)
        os.remove(logPath)
    except: 
        pass

    print 'THIS IS RESULT :::' 
    print result
    if hasattr(result, 'iteritems'): 
        for k, v in result.iteritems(): 
            logger.debug('namespace %s - %s' % (k, v))
    
    if result: 
        allResult = True
        for k, v in result.iteritems(): 
            if not v == True: 
                allResult = False 
        # allResult = all(v for k, v in result.iteritems())
    else: 
        logger.error('** No result returned')
        allResult = False 
    message = 'Success' if allResult else 'Failed. See log path for details %s' % logPath

    return allResult, message
    

def get_export_namespaces(scene): 
    techCacheGrp = scene.projectInfo.asset.tech_grp()
    techCacheList = cache.get_asset_namespace(techCacheGrp)
    lockNs = get_lock_ns(scene)
    ns = ['%s%s' % (a, alembic_cache.techSuffix) for a in techCacheList]

    if lockNs: 
        ns = [a for a in ns if not a in lockNs]
        logger.debug('locked namespaces %s' % lockNs)
    return ns 


def get_lock_ns(entity): 
    from rf_utils import register_sg
    global lockedNs
    if lockedNs == 'N/A': 
        logger.debug('Fetching shotgun for locked namespaces ...')
        reg = register_sg.ShotElement(entity)
        lockedNs = reg.list_lock(entity.step)
    
    return lockedNs