import sys
import os

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from collections import OrderedDict
from rf_app.publish.scene.utils import maya_hook as hook
from rf_maya.lib import sequencer_lib
from rf_maya.lib import playblast_lib
from rf_app.publish.scene.utils import output_utils
from rf_utils.pipeline import snap_utils


def run(scene, shotName, exportFilter=[]):
	""" This module playblast and upload mov to step anim and task anim """
	listData = OrderedDict()
	funcDict = {'mov': {'func': export, 'funcType': 'mov', 'dataType': 'media', 'outputNs': '', 'namespace': 'workspace', 'outputKey': 'playblast', 'schemaKey': ['outputImgPath', 'outputHeroImgPath'], 'publish': False}}
	listData['anim_mov'] = {'type': 'media', 'status': True, 'func': output_utils.run, 'shotName': shotName, 'args': [funcDict, scene, exportFilter, shotName, scene], 'details': 'Export shot camera', 'order': 0}
	return listData

def export(dstPath, shotName, scene, **kwargs):
	""" module for export playblast """
	print 'dstPath -------------------', dstPath
	if not hook.is_batch():
		panel = hook.get_panel()
		hook.set_panel_show_only_polygon(panel)
		hook.viewport_update(True)
		sequencer_lib.set_camera(shotName, scene)
		playblast_lib.playblast_sequencer_hud(dstPath, shotName, hudProject=scene.project, hudUser=scene.local_user, hudStep=scene.step, hudFilename=scene.publish_name(), hudShot=shotName, showHud=True)
		upload_playblast(scene, dstPath, shotName)
	return dstPath

def upload_playblast(scene, dstPath, shotName):
	entity = scene.copy()
	entity.context.update(step='anim', task='anim')
	path = snap_utils.write_media_sg(entity, media=dstPath, shotName=shotName, step=entity.step, description='')
	# data = snap_utils.read_data(entity)
	# snap_utils.publish(data)


# publish_version(ContextPathInfo, taskEntity, status, imgs, userEntity, description, uploadOption='default')
