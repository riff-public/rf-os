import sys
import os
from collections import OrderedDict
from rf_app.publish.scene.utils import maya_hook as hook
from rf_maya.lib import playblast_lib
from rf_maya.lib import sequencer_lib
from rf_app.publish.scene.utils import output_utils
from rf_utils.pipeline import snap_utils


def run(scene, shotName, exportFilter=[]):
	""" scene = contextPathInfo, shotName = sequencerName """
	listData = OrderedDict()
	funcDict = {'mov': {'func': export, 'funcType': 'mov', 'dataType': 'media', 'outputNs': '', 'namespace': 'workspace', 'outputKey': 'playblast', 'schemaKey': ['outputImgPath', 'outputHeroImgPath'], 'publish': True}}
	listData['mov'] = {'type': 'media', 'status': True, 'func': output_utils.run, 'shotName': shotName, 'args': [funcDict, scene, exportFilter, shotName, scene], 'details': 'Export shot camera'}
	return listData

def export(dstPath, shotName, scene, **kwargs):
	""" module for export playblast """
	if not hook.is_batch():
		panel = hook.get_panel()
		sequencer_lib.set_camera(shotName, scene)
		hook.set_panel_show_only_polygon(panel)
		hook.viewport_update(True)
		playblast_lib.playblast_sequencer_hud(dstPath, shotName, hudProject=scene.project, hudUser=scene.local_user, hudStep=scene.step, hudFilename=scene.filename, hudShot=shotName, showHud=True)
		# export_pre_publish_data(scene, dstPath, shotName)
	return dstPath

def export_pre_publish_data(scene, dstPath, shotName):
	snap_utils.write_media_sg(media=dstPath, shotName=shotName, step=scene.step, description='')


# publish_version(ContextPathInfo, taskEntity, status, imgs, userEntity, description, uploadOption='default')
