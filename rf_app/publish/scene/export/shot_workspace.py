import sys
import os

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from collections import OrderedDict
from rf_app.publish.scene.utils import maya_hook as hook
from rf_maya.rftool.layout.split_shot import split_shot_cmd
from rf_app.publish.scene.utils import output_utils
reload(output_utils)
from rf_utils import file_utils

def run(scene, shotName, exportFilter=[]):
    """ scene = contextPathInfo, shotName = sequencerName """
    listData = OrderedDict()
    funcDict = {'workspace': {'func': export, 'funcType': 'workspace', 'dataType': 'workspace', 'outputNs': 'workspace', 'namespace': 'workspace', 'outputKey': '', 'schemaKey': ['cachePath', 'cacheHeroPath']}}
    listData['shot_workspace'] = {'type': 'workspace', 'status': True, 'func': output_utils.run, 'shotName': shotName, 'args': [funcDict, scene, exportFilter, shotName], 'details': 'Export workspace'}
    return listData


def export(dstPath, shotName):
    """ module for export workspace """
    logger.info('Save workspace ...')
    # result = hook.save()
    result = hook.current_scene()
    file_utils.xcopy_file(result, dstPath)

    if os.path.exists(dstPath): 
        return result
    return False

