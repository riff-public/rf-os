import sys
import os

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
from rf_app.publish.scene.utils import maya_hook as hook
from rf_maya.lib import maya_lib
from rf_utils import publish_info
from collections import OrderedDict
import maya.cmds as mc
import pymel.core as pm
from rf_app.publish.scene.utils import output_utils
from rf_app.asm import asm_lib
reload(output_utils)
reload(maya_lib)

lockedNs = 'N/A'

def run(scene, shotName, exportFilter=[]):
    """ scene = contextPathInfo, shotName = sequencerName """
    listData = OrderedDict()
    asms = asm_lib.list_set(scene)
    # check locked node
    lockNs = get_lock_ns(scene)
    
    if lockNs: 
        asms = [a for a in asms if not a in lockNs]
        logger.debug('Namespace locked ** %s' % lockNs)

    for namespace in asms:
        root = namespace
        ref = asm_lib.MayaRepresentation(namespace).asm_data
        funcDict = {
            'setshot': {
                'func': export_set,
                'funcType': 'set',
                'dataType': 'setshot',
                'outputNs': namespace,
                'namespace': namespace,
                'outputKey': 'setShot',
                'schemaKey': ['cachePath', 'cacheGlobalHero'],
                'ref': ref
            },
            'setShotMaya': {
                'func': export_maya,
                'funcType': 'set',
                'dataType': 'maya',
                'outputNs': namespace,
                'namespace': namespace,
                'outputKey': 'setShotMaya',
                'schemaKey': ['cachePath', 'cacheGlobalHero'],
                'ref': ref
            },
            'override': {
                'func': export_override,
                'funcType': 'set',
                'dataType': 'setoverride',
                'outputNs': namespace,
                'namespace': namespace,
                'outputKey': 'setOverride',
                'schemaKey': ['cachePath', 'cacheGlobalHero'],
                'ref': ref
            }
        }

        listData[namespace] = {
            'type': 'set',
            'status': True,
            'func': output_utils.run,
            'shotName': shotName,
            'args': [funcDict, scene, exportFilter, shotName, namespace],
            'details': 'Export cache'
        }
    # listData['charB'] = {'type': 'cache', 'status': True, 'func': output_utils.run, 'shotName': shotName, 'args': [funcDict, scene, shotName, 'charA'], 'details': 'Export cache'}

    return listData


def export_set(dstPath, shotName, root):
    result = asm_lib.export(root, dstPath, 'copy')
    return dstPath if result else False

def export_override(dstPath, shotName, root):
    result = asm_lib.export_override(root, dstPath)
    return dstPath if result else False

def export_maya(dstPath, shotName, root):
    result = hook.export_selection(root, dstPath)
    return dstPath if result else False

def get_set(scene):
    setGrp = scene.projectInfo.asset.set_grp() or 'Set_Grp'
    asms = []
    if mc.objExists(setGrp):
        sets = mc.listRelatives(setGrp, c=True)
        if sets: 
            for asset in sets:
                asm = asm_lib.MayaRepresentation(asset)
                if asm.is_root():
                    asms.append(asset)
    return asms


def get_lock_ns(entity): 
    from rf_utils import register_sg
    global lockedNs
    if lockedNs == 'N/A': 
        logger.debug('Fetching shotgun for locked namespaces ...')
        reg = register_sg.ShotElement(entity)
        lockedNs = reg.list_lock(entity.step)
    
    return lockedNs
