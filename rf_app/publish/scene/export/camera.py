import sys
import os

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
from rf_app.publish.scene.utils import maya_hook as hook
reload(hook)
from rf_maya.lib import maya_lib
from rf_maya.lib import sequencer_lib
from rf_utils import publish_info
from collections import OrderedDict
import maya.cmds as mc
import maya.mel as mm 
import pymel.core as pm
from rf_app.publish.scene.utils import output_utils
from rf_maya.lib.sequencer_lib import *
reload(output_utils)
reload(maya_lib)
from rf_utils import custom_exception


def run(scene, shotNode, exportFilter=[]):
    """ scene = contextPathInfo, shotName = sequencerName """
    listData = OrderedDict()
    camList = find_cam_variant(shotNode)

    for data in camList: 
        nodeName = data['master']
        shotName = data['shotName']
        variant = data['variant']
        active = check_active_cam(nodeName)

        if active: 
            entity = scene.copy()
            displayName = 'camera_%s' % variant
            namespace = '%s_%s_cam' % (shotName, variant)
            if variant == 'main': 
                namespace = '%s_cam' % shotName
                displayName = 'camera'
            entity.context.update(look=variant)

            funcDict = {'maya': {'func': export, 'funcType': 'camera', 'dataType': 'maya', 'outputNs': namespace, 'namespace': namespace, 'outputKey': 'cam', 'outputHeroKey': 'camHero', 'schemaKey': ['cachePath', 'cacheGlobalHero']},
                        'cache': {'func': exportCache, 'funcType': 'camera', 'dataType': 'abc_cache', 'outputNs': namespace, 'namespace': namespace, 'outputKey': 'camCache', 'outputHeroKey': 'camCacheHero', 'schemaKey': ['cachePath', 'cacheGlobalHero']}, 
                        'fbx': {'func': exportFbx, 'funcType': 'camera', 'dataType': 'fbx', 'outputNs': namespace, 'namespace': namespace, 'outputKey': 'camFbx', 'outputHeroKey': 'camFbxHero', 'schemaKey': ['cachePath', 'cacheGlobalHero']}}
            listData[displayName] = {'type': 'camera', 'status': True, 'func': output_utils.run, 'shotName': shotName, 'args': [funcDict, entity, exportFilter, shotName, nodeName], 'details': 'Export shot camera'}
    return listData


def export(dstPath, shotName, shotNode=None):
    """ shotName use to interpret path. shotNode use to find camera within node """ 
    from rf_maya.rftool.movie_image import app as img_plane
    shotName = shotNode if shotNode else shotName


    # try to bake the camera to free it from any constraints
    camshot = camera_info(shotName)
    if not camshot: 
        raise custom_exception.PipelineError('Camera not linked to Shot "{}".'.format(shotName))

    hook.set_overscan(camshot, 1, preserveRig=True)

    obj_toBake, constraints = hook.get_cam_dependencies(shotName)
    if obj_toBake:
        hook.bake_transform(shotName, obj_toBake)
        hook.delete_nodes([c.shortName() for c in constraints])

    list_shot = mc.sequenceManager(lsh=True)
    img_plane.close_all_movie_img_plane(list_shot)
    result = hook.export_camera(dstPath, shotName)
    sequencer_lib.relink_shot_camera([shotName])
    return result


def exportCache(dstPath, shotName, shotNode=None):
    """ shotName use to interpret path. shotNode use to find camera within node """ 
    from rftool.utils import abc_utils
    reload(abc_utils)
    shotName = shotNode if shotNode else shotName
    hook.viewport_update(False)
    cam_rig = hook.find_cam_rig(shotName)
    start, end, time = hook.list_shot_status(shotName)
    shot_info = [start, end]
    if cam_rig:
        # export .abc
        abc_utils.exportABC(cam_rig, dstPath, shot_info)
        hook.viewport_update(True)
        if os.path.exists(dstPath):
            logger.info('Export cache camera success %s' % dstPath)
            return dstPath
        return ''


def exportFbx(dstPath, shotName, shotNode=None): 
    ''' export FBX Maya to UE'''
    plugin = 'fbxmaya.mll'
    if not mc.pluginInfo(plugin, q=True, l=True):
        mc.loadPlugin(plugin, qt=True)
    cam_rig = hook.find_cam_rig(shotName)
    camName = 'UE4_Standard'
    ns = cam_rig.split(':')[0]
    ueCam = '%s:%s' % (ns, camName)

    if mc.objExists(ueCam): 
        start = mc.playbackOptions(q=True, min=True)
        end = mc.playbackOptions(q=True, max=True)
        
        dstDir = os.path.dirname(dstPath)
        if not os.path.exists(dstDir):
            os.makedirs(dstDir)

        cmd = 'FBXExportUpAxis "Z";FBXExportBakeComplexAnimation -q;FBXExportBakeComplexStart -v {0};FBXExportBakeComplexEnd -v {1};FBXExport -f "{2}" -s "{3}";'.format(start, end, dstPath, ueCam)
        mm.eval(cmd)
    if os.path.exists(dstPath):
        logger.info('Export cache camera success %s' % dstPath)
        return dstPath
    else: 
        logger.warning('"{}" not found. Not output'.format(ueCam))
    return ''


def check_active_cam(shotName): 
    passiveExt = '.abc'
    if mc.objExists(shotName): 
        cam = hook.find_cam(shotName)
        if cam: 
            if mc.referenceQuery(cam, inr=True): 
                path = mc.referenceQuery(cam, f=True)
                ext = os.path.splitext(path.split('{')[0])[-1]
                if ext == passiveExt: 
                    return False 
        else: 
            return False
        return True
    else: 
        return False 


def find_cam_variant(shotName): 
    master = hook.is_shot_master(shotName)
    if master: 
        # find variant 
        shots = hook.list_shot_variant(shotName)
        # name, variant = hook.shot_variant_info(shotName)
        # return [hook.shot_dict(shotName, name, variant)]
        return shots 
    else: 
        if shotName in hook.list_shot(): 
            name, variant = hook.shot_variant_info(shotName)
            return [hook.shot_dict(shotName, name, variant)]
    return []

# ============================================
# use by new export 
def export_maya(entity, dst): 
    shotName = entity.name_code
    return export(dst, shotName)


def export_abc(entity, dst): 
    shotName = entity.name_code
    return exportCache(dst, shotName)


def export_fbx(entity, dst): 
    shotName = entity.name_code
    return exportFbx(dst, shotName)
