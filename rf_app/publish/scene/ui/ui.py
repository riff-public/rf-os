import os
import sys

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

# widgets
from rf_utils.widget import shot_list_widget
from rf_utils.widget import export_list_widget
from rf_utils.widget import snap_version_widget
from rf_utils.widget import task_widget
from rf_utils.widget import status_widget
from rf_qc import qc_widget
sg = None


class ShotExportUI(QtWidgets.QWidget):
    status = QtCore.Signal(str)
    def __init__(self, parent=None) :
        super(ShotExportUI, self).__init__(parent)

        # tab index
        self.ui()

    def ui(self):
        self.layout = QtWidgets.QVBoxLayout()
        self.primaryLayout = QtWidgets.QHBoxLayout()

        # qc widget
        self.qcWidget = qc_widget.QcWidget()
        self.primaryLayout.addWidget(self.qcWidget)

        # shot widget
        self.listShotLayout = QtWidgets.QVBoxLayout()
        self.shotLabel = QtWidgets.QLabel('Shot List')
        self.shotWidget = shot_list_widget.ShotListWidget()
        self.listShotLayout.addWidget(self.shotLabel)
        self.listShotLayout.addWidget(self.shotWidget)

        self.shotSelectCheckBox = QtWidgets.QCheckBox('Select All')
        self.listShotLayout.addWidget(self.shotSelectCheckBox)

        self.settingButton = QtWidgets.QPushButton('Export Options')
        self.listShotLayout.addWidget(self.settingButton)
        self.primaryLayout.addLayout(self.listShotLayout)

        # export list widget
        self.exportLayout = QtWidgets.QVBoxLayout()
        self.exportLabel = QtWidgets.QLabel('Export List')
        self.exportWidget = export_list_widget.ExportListWidget()
        self.exportWidget.listWidget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.exportLayout.addWidget(self.exportLabel)
        self.exportLayout.addWidget(self.exportWidget)
        self.primaryLayout.addLayout(self.exportLayout)

        # publish checkBox
        self.exportSelectCheckBox = QtWidgets.QCheckBox('Select All')
        self.exportLayout.addWidget(self.exportSelectCheckBox)
        self.publishCheckBox = QtWidgets.QCheckBox('Export and Publish')
        self.exportLayout.addWidget(self.publishCheckBox)
        self.continueCheckBox = QtWidgets.QCheckBox('Continue on error')
        self.exportLayout.addWidget(self.continueCheckBox)

        # export button
        self.exportButton = QtWidgets.QPushButton('Export')
        self.exportButton.setMinimumSize(QtCore.QSize(0, 30))
        self.exportLayout.addWidget(self.exportButton)

        # publish list
        # self.publishLayout = QtWidgets.QVBoxLayout()
        # self.publishLabel = QtWidgets.QLabel('Publish List')
        # self.publishWidget = snap_version_widget.SnapVersionWidget()
        # self.publishLayout.addWidget(self.publishLabel)
        # self.publishLayout.addWidget(self.publishWidget)
        # self.primaryLayout.addLayout(self.publishLayout)

        # # task
        # self.taskComboBox = task_widget.TaskThreadComboBox()
        # self.taskComboBox.allLayout.setStretch(0, 1)
        # self.taskComboBox.allLayout.setStretch(1, 2)
        # self.taskComboBox.label.setText('Task')

        # self.publishLayout.addWidget(self.taskComboBox)

        # publish button
        self.publishButton = QtWidgets.QPushButton('Publish')
        self.publishButton.setMinimumSize(QtCore.QSize(0, 30))
        self.exportLayout.addWidget(self.publishButton)

        self.layout.addLayout(self.primaryLayout)
        self.setLayout(self.layout)


class Options(QtWidgets.QDialog):
    """docstring for Option"""
    def __init__(self, parent=None):
        super(Options, self).__init__(parent=parent)
        self.layout = QtWidgets.QVBoxLayout()
        self.listWidget = QtWidgets.QListWidget()
        self.checkBox = QtWidgets.QCheckBox('Select All')
        self.checkBox.setChecked(True)
        self.layout.addWidget(self.listWidget)
        self.layout.addWidget(self.checkBox)
        self.setLayout(self.layout)
        self.setWindowTitle('Export options')
        self.init_signals()

    def init_signals(self):
        self.checkBox.stateChanged.connect(self.set_check)
        self.listWidget.itemSelectionChanged.connect(self.set_check_item)

    def set_check(self, value):
        state = True if value else False
        items = [self.listWidget.item(a) for a in range(self.listWidget.count())]

        for item in items:
            item.setCheckState(QtCore.Qt.Unchecked) if not state else item.setCheckState(QtCore.Qt.Checked)

    def set_check_item(self):
        item = self.listWidget.currentItem()
        # if check, deselect
        if item.checkState() == QtCore.Qt.Checked:
            state = QtCore.Qt.Unchecked
        # if not check, select
        else:
            state = QtCore.Qt.Checked
        item.setCheckState(state)


    def set_data(self, modData, filterData):
        self.listWidget.clear()

        for mod in modData.keys():
            check = True if mod in filterData else False
            self.add_item(mod, check=check)

    def add_item(self, text, data=None, check=True):
        item = QtWidgets.QListWidgetItem(self.listWidget)
        item.setFlags(item.flags() | QtCore.Qt.ItemIsUserCheckable)

        item.setText(text)
        item.setData(QtCore.Qt.UserRole, data)
        item.setCheckState(QtCore.Qt.Unchecked) if not check else item.setCheckState(QtCore.Qt.Checked)

    def get_checked_mod(self):
        items = [self.listWidget.item(a) for a in range(self.listWidget.count())]
        checkedMods = [str(a.text()) for a in items if a.checkState() == QtCore.Qt.Checked]
        return checkedMods

