import os
import sys
import re
from collections import OrderedDict

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_utils.context import context_info
from rf_utils import publish_info
from rf_utils import file_utils
from rf_utils import admin
from rf_utils import register_shot
from rf_utils.pipeline import snap_utils
reload(register_shot)
from rf_utils import register_sg
from rf_utils import project_info
from . import output_trigger
# reload(output_trigger)


def run(funcDicts, scene, exportFilter=[], shotName='', *args):
    """ module for export camera """
    # extracted result
    messages = []
    results = []
    workspace = workspace_path(scene, shotName)
    project = project_info.ProjectInfo(project=scene.project)

    for exportType, funcDict in funcDicts.iteritems():

        # filtering export config
        runExport = True if '*' in exportFilter else True if exportType in exportFilter else False

        # outputs type
        if runExport:
            func = funcDict['func']
            funcType = funcDict['funcType']
            dataType = funcDict['dataType']
            outputNs = funcDict['outputNs']
            namespace = funcDict['namespace']
            outputKey = funcDict['outputKey']
            schemaKey = funcDict['schemaKey']
            node = funcDict.get('node', '')
            key = funcDict.get('key', namespace)
            ref = funcDict.get('ref', '')
            exportNamespace = funcDict.get('exportNs', '')
            outputHeroKey = funcDict.get('outputHeroKey', '')
            publish = funcDict.get('publish', False)
            adPath = ''

            regVersion = reg_version(scene, shotName, namespace, reg=True)
            scene.context.update(publishVersion=regVersion)

            if ref:
                asset = context_info.ContextPathInfo(path=ref)
                adPath = '%s/%s' % (asset.path.asm_data().abs_path(), asset.ad_name)

            # get output / workspace

            # register shot
            # reg workspace
            reg = register_shot.Register(scene)

            if dataType == 'workspace':
                outputPaths = workspace
            else:
                outputPaths = export_path(scene, shotName, namespace=outputNs, outputKey=outputKey, schemaKey=schemaKey, outputHeroKey=outputHeroKey)

            # check prefer step
            prefer = True
            if dataType in project.scene.prefer_built_step.keys(): 
                prefer_step = project.scene.prefer_built_step[dataType]
                if reg.data: 
                    current_step = reg.get.step(key)
                    if current_step: 
                        if current_step == prefer_step: 
                            if not scene.step == prefer_step: 
                                prefer = False
                                # print '**** start noti ****', scene.step, prefer_step
                                output_trigger.run(key, dataType, scene, prefer_step)
                    #         else: 
                    #             print '** current step is prefer step'
                    #     else: 
                    #         print '** last publish is not prefer step', current_step, prefer_step
                    # else: 
                    #     print '** first publish'


            # register dataType
            if dataType == 'maya':
                reg.set_data(scene, 'asset', funcType, scene.step, key, namespace, node, adPath=adPath, workspace=[workspace[0], workspace[1]], maya=[outputPaths[0], outputPaths[1]])
            if dataType == 'setshot':
                reg.set_data(scene, 'asset', funcType, scene.step, key, namespace, node, adPath=adPath, workspace=[workspace[0], workspace[1]], setShot=[outputPaths[0], outputPaths[1]], asmPath=ref)
            if dataType == 'setoverride':
                reg.set_data(scene, 'asset', funcType, scene.step, key, namespace, node, adPath=adPath, workspace=[workspace[0], workspace[1]], setOverride=[outputPaths[0], outputPaths[1]], asmPath=ref)
            if dataType == 'shotdress':
                reg.set_data(scene, 'asset', funcType, scene.step, key, namespace, node, adPath=adPath, workspace=[workspace[0], workspace[1]], shotDress=[outputPaths[0], outputPaths[1]])
            if dataType == 'instdress':
                reg.set_data(scene, 'asset', funcType, scene.step, key, namespace, node, adPath=adPath, workspace=[workspace[0], workspace[1]], maya=[outputPaths[0], outputPaths[1]])
            if dataType == 'abc_cache':
                reg.set_data(scene, 'asset', funcType, scene.step, key, namespace, node, adPath=adPath, workspace=[workspace[0], workspace[1]], cache=[outputPaths[0], outputPaths[1]], exportNamespace=exportNamespace)
            if dataType == 'abc_cache_tech':
                reg.set_data(scene, 'asset', funcType, scene.step, key, namespace, node, adPath=adPath, workspace=[workspace[0], workspace[1]], cacheTech=[outputPaths[0], outputPaths[1]], exportNamespace=exportNamespace)
            if dataType == 'ma_rsproxy':
                reg.set_data(scene, 'asset', funcType, scene.step, key, namespace, node, adPath=adPath, workspace=[workspace[0], workspace[1]], maRsproxy=[outputPaths[0], outputPaths[1]], exportNamespace=exportNamespace)
            if dataType == 'fbx':
                reg.set_data(scene, 'asset', funcType, scene.step, key, namespace, node, adPath=adPath, workspace=[workspace[0], workspace[1]], fbx=[outputPaths[0], outputPaths[1]], exportNamespace=exportNamespace)
            if dataType == 'yeti_cache':
                reg.set_data(scene, 'asset', funcType, scene.step, key, namespace, node, adPath=adPath, workspace=[workspace[0], workspace[1]], cache=[outputPaths[0], outputPaths[1]], exportNamespace=exportNamespace)
            if dataType == 'animCurve':
                reg.set_data(scene, 'asset', funcType, scene.step, key, namespace, node, adPath=adPath, workspace=[workspace[0], workspace[1]], animCurve=[outputPaths[0], outputPaths[1]])
            if dataType == 'import_data':
                reg.set_data(scene, 'asset', funcType, scene.step, key, namespace, node, maya=[outputPaths[0], outputPaths[1]], workspace=[workspace[0], workspace[1]])
            if dataType == 'ref_data':
                reg.set_data(scene, 'asset', funcType, scene.step, key, namespace, node, maya=[outputPaths[0], outputPaths[1]], workspace=[workspace[0], workspace[1]])

            if dataType == 'media':
                reg.set_media(scene, workspace=[workspace[0], workspace[1]], media=[outputPaths[0], outputPaths[1]])


            # register shotgun 
            asset = context_info.ContextPathInfo(path=adPath)
            assetName = asset.name if adPath else ''
            elemReg = register_sg.update_sg_reg(scene, nsKey=key, assetName=assetName, step=scene.step, namespace=namespace, exportNamespace=exportNamespace, filetype=dataType, heroFile=outputPaths[1], publishedFile=outputPaths[0], version=scene.publishVersion, status='ip')

            #export selection
            dstPath = outputPaths[0]
            exportResult = func(dstPath, shotName, *args)

            # status set 
            elemReg.add(step=scene.step, status='apr')

            if publish:
                snap_utils.write_media_sg(scene, media=dstPath, shotName=shotName, step=scene.step, description='')

            for output in outputPaths:
                if not output == exportResult:
                    result = copy(exportResult, output)
                else:
                    result = exportResult

                if result:
                    results.append(True)
                    messages.append('** %s File exported %s' % (exportType, result))
                else:
                    results.append(False)
                    messages.append('** %s Failed to export %s' % (exportType, result))

            message = '\n'.join(messages)
            regVersion = reg_version(scene, shotName, namespace, reg=True)

    return all(results), message


def publish_version(sceneContext, shotName):
    # override context to shot
    sceneContext.context.update(entityCode=shotName)
    sceneContext.context.update(entity=sceneContext.sg_name)


def reg_version(scene, shotName, namespace, reg=True):
    scene.context.update(entityCode=shotName)
    scene.context.update(entity=scene.sg_name)
    reg = publish_info.PreRegisterShot(scene)
    regVersion = reg.add_output(namespace)
    reg.register() if reg else None
    return regVersion


def export_path(sceneContext, shotName, namespace='', outputKey='', schemaKey=[], outputHeroKey=''):
    scene = sceneContext.copy()
    scene.context.update(entityCode=shotName)
    scene.context.update(entity=scene.sg_name)

    # published file
    scene.context.update(process=namespace) if namespace else None
    outputDir = scene.path.scheme(schemaKey[0]).abs_path()
    filename = scene.output_name(outputKey=outputKey)
    publishedFile = '%s/%s' % (outputDir, filename)

    # hero file
    heroOutputDir = scene.path.scheme(schemaKey[1]).abs_path()
    heroFileName = scene.output_name(outputKey=outputKey, hero=True)
    heroFile = '%s/%s' % (heroOutputDir, heroFileName)

    if outputHeroKey:
        shotHero = scene.path.hero().abs_path()
        heroName = scene.output_name(outputKey=outputHeroKey, hero=True)
        heroFile = '%s/%s' % (shotHero, heroName)

    return [publishedFile, heroFile]

def workspace_path(sceneContext, shotName):
    scene = sceneContext.copy()
    # workspace version
    wsVersion = reg_version(scene, shotName, 'workspace', reg=True)
    # wsVersion = 'v001'
    scene.context.update(publishVersion=wsVersion)
    publishedWsDir = scene.path.publish_workspace().abs_path()
    publishedWsFile = scene.publish_name()
    publishedWs = '%s/%s' % (publishedWsDir, publishedWsFile)

    # workspace hero
    heroWsDir = scene.path.publish_workspace_hero().abs_path()
    heroWsFile = scene.publish_name(hero=True)
    heroWs = '%s/%s' % (heroWsDir, heroWsFile)

    return [publishedWs, heroWs]


def post_register(sceneContext, shotNames):
    scene = sceneContext.copy()
    for shotName in shotNames:
        scene.context.update(entityCode=shotName)
        scene.context.update(entity=scene.sg_name)
        reg = register_shot.Register(scene)
        regFile = reg.tmp_config_path()
        if os.path.exists(regFile):
            reg.register()
            logger.info('Post register %s complete' % shotName)
        else:
            logger.info('No post register file %s' % shotName)

    logger.info('Post registered %s shots' % (len(shotNames)))



def copy(src, dst):
    if os.path.exists(src):
        file_utils.copy(src, dst)
        return True if os.path.exists(dst) else False

    else:
        # detect pattern
        pattern = re.findall(r"%\d+d",src)
        if pattern:
            rawFiles = file_utils.list_file(os.path.dirname(src))
            replacePatterns = os.path.basename(src).split(pattern[0])
            validFiles = OrderedDict()

            for srcFile in rawFiles:
                seqNumber = srcFile
                for replaceKey in replacePatterns:
                    seqNumber = seqNumber.replace(replaceKey, '')

                if seqNumber.isdigit():
                    validFiles['%s/%s' % (os.path.dirname(src), srcFile)] = seqNumber

            results = []
            for srcFile, seqNumber in validFiles.iteritems():
                try:
                    dstFile = sequence_name(dst, seqNumber)
                    # file_utils.copy(srcFile, dstFile)
                    file_utils.xcopy_file(srcFile, dstFile)
                    logger.debug('Copy to publish %s' % dstFile)
                    status = True if os.path.exists(dstFile) else False
                except Exception as e:
                    logger.error(e)
                    status = False
                results.append(status)
            return all(results)


        # basename = os.path.basename(src)
        # if '%' in basename:
        #     basename

def is_seq_pattern(name):
    pattern = re.findall(r"%\d+d",name)
    return pattern[0] if pattern else ''

def sequence_name(filePattern, seqNumber):
    key = is_seq_pattern(filePattern)
    if key:
        return filePattern.replace(key, seqNumber)





"""
example of register files 
from rf_utils.context import context_info 
scene = context_info.ContextPathInfo(path='P:/projectName/scene/work/ep01/pr_ep01_q0040_s0010/sim/main/maya/pr_ep01_q0040_s0010_desc.hero.yml')
scene.context.update(task='sim', publishVersion='v006')
# abc cache 
funcType = 'abc_cache'
key = 'arthur_001'
namespace = 'arthur_001'
node = 'Geo_Grp'
adPath = 'P:/SevenChickMovie/asset/publ/prop/FluteGold/_data/asmData/FluteGold_ad.hero.yml'
workspace = [u'P:/SevenChickMovie/scene/publ/act1/scm_act1_q0070_s0020/anim/main/v006/maya/scm_act1_q0070_s0020_anim_main.v006.ma', u'P:/SevenChickMovie/scene/publ/act1/scm_act1_q0070_s0020/anim/main/hero/maya/scm_act1_q0070_s0020_anim_main.hero.ma']
outputPaths = [u'R:/SevenChickMovie/scene/publ/act1/scm_act1_q0070_s0020/anim/output/fluteGold_001/v001/scm_act1_q0070_s0020_anim_fluteGold_001.v001.abc', u'P:/SevenChickMovie/scene/publ/act1/scm_act1_q0070_s0020/output/fluteGold_001/scm_act1_q0070_s0020_anim_fluteGold_001.hero.abc']
exportNamespace = 'arthur_001'

from rf_utils import register_shot
reg = register_shot.Register(scene)
reg.set_data(scene, 'asset', funcType, scene.step, key, namespace, node, adPath=adPath, workspace=[workspace[0], workspace[1]], cache=[outputPaths[0], outputPaths[1]], exportNamespace=exportNamespace)
reg.register()

funcType = 'yeti_cache'
key = 'Jacobus_001_ArmFeat'
namespace = 'Jacobus_001'
node = 'ArmFeat_YetiNode'
adPath = 'P:/SevenChickMovie/asset/publ/prop/FluteGold/_data/asmData/FluteGold_ad.hero.yml'
workspace = [u'P:/SevenChickMovie/scene/publ/act1/scm_act1_q0070_s0020/anim/main/v006/maya/scm_act1_q0070_s0020_anim_main.v006.ma', u'P:/SevenChickMovie/scene/publ/act1/scm_act1_q0070_s0020/anim/main/hero/maya/scm_act1_q0070_s0020_anim_main.hero.ma']
outputPaths = [u'R:/projectName/scene/publ/ep01/pr_ep01_q0040_s0010/anim/output/Jacobus_001_BeardWhite/v001/pr_ep01_q0040_s0010_anim_Jacobus_001_BeardWhite.v001.%04d.fur', u'P:/projectName/scene/publ/ep01/pr_ep01_q0040_s0010/output/Jacobus_001_BeardWhite/pr_ep01_q0040_s0010_anim_Jacobus_001_BeardWhite.hero.%04d.fur']
exportNamespace = 'Jacobus_001'
reg.set_data(scene, 'asset', funcType, scene.step, key, namespace, node, adPath=adPath, workspace=[workspace[0], workspace[1]], cache=[outputPaths[0], outputPaths[1]], exportNamespace=exportNamespace)
reg.register()
"""