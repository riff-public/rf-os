import maya.cmds as mc
import pymel.core as pm
import maya.OpenMaya as om
import maya.OpenMayaUI as omUI
import maya.mel as mm
import os
from collections import OrderedDict

from rf_maya.lib.sequencer_lib import *
from rftool.utils import maya_utils
from rf_utils.context import context_info

def makedirs(path):
    os.makedirs(path) if not os.path.exists(path) else None
    return path

def exportSelected(pathFile, type='mayaAscii'):
    return mc.file(pathFile, es=True, pr=True, f=True, sh=True, type=type)
    # pm.exportSelected(pathFile, preserveReferences = True)

def save(): 
    mc.file(rename=mc.file(q=True, loc=True))
    return mc.file(save=True, f=True)

def listSelection():
    return mc.ls(sl=True)

def add_rig_grp_from_selection():
    exportList = mc.ls(sl=True)
    for ex in exportList:
        if ':' in ex:
            ns = ex.split(':')[0]
            addition = '%s:%s' % (ns, 'Rig_Grp')
            if not addition in exportList:
                if mc.objExists(addition):
                    addSelection(addition)
                    exportList.append(addition)

def current_scene(): 
    cr = mc.file(q=True, loc=True)
    return cr if not cr == 'unknown' else ''

def set_overscan(camera, value, preserveRig=False): 
    camera = pm.PyNode(camera)
    if camera.nodeType() == 'camera': 
        camShape = camera

    if camera.nodeType() == 'transform': 
        camShape = camera.getShape()

    src_attrs = camShape.overscan.inputs(p=True)
    if src_attrs:
        for attr in src_attrs: 
            attr.set(value)
            
        if not preserveRig: 
            pm.disconnectAttr(src_attrs[0], camShape.overscan)
    try:
        camShape.overscan.set(value)
    except:
        print "not setAttr overscan"

def select_viewport_objects():
    view = omUI.M3dView.active3dView()
    om.MGlobal.selectFromScreen(0,0,view.portWidth(),view.portHeight(),om.MGlobal.kAddToList)
    sel = mc.ls(sl=True)
    return sel

def set_seq_current_time(value):
    return mc.sequenceManager(currentTime=value)

def shot_time(shot):
    startTime = mc.shot(shot, q=True, startTime=True)
    endTime = mc.shot(shot, q=True, endTime=True)
    return startTime, endTime

def look_thru(cam):
    """ look through this camera """
    mc.lookThru(cam) if not mc.about(batch=True) else None

def changeFrame(frame):
    mc.currentTime(frame)

def clearSelection():
    mc.select(clear = True)

def addSelection(obj):
    return mc.select(obj, add = True)

def parentNode(obj):
    return mc.listRelatives(obj, p = True)

def get_file_format(shot):
    return '%s.ma' % shot

def list_shot():
    shots = mc.sequenceManager(lsh=True) or []
    return shots

def list_pipeline_shot(): 
    shots = list_shot()
    shotDict = OrderedDict()
    for shot in shots: 
        name, variant = shot_variant_info(shot)
        shotDict[shot] = shot_dict(shot, name, variant)

    return shotDict

def shot_dict(shot, name, variant): 
    return {'master': shot, 'shotName': name, 'variant': variant}

def shot_variant_info(shotNode): 
    # second element will be process 
    # first element will be shot name 
    elms = shotNode.split('_')
    shotName = elms[0]
    variant = elms[1] if len(elms) > 1 else 'main'
    return shotName, variant

def list_shot_variant(shotName): 
    variant = []
    shots = list_shot()
    shotDict = list_pipeline_shot()
    if shotName in shots: 
        for shot, data in shotDict.iteritems(): 
            if shotName == data['shotName']: 
                variant.append(data)

    return variant


def set_selection_filter(): 
    mm.eval('setObjectPickMask "Surface" true;')
    mm.eval('setObjectPickMask "Marker" false;')
    mm.eval('setObjectPickMask "Joint" false;')
    mm.eval('setObjectPickMask "Curve" false;')


def is_shot_master(shotName): 
    if mc.objExists(shotName): 
        camDict = list_pipeline_shot()
        for shotKey, v in camDict.iteritems(): 
            if shotName == shotKey: 
                variant = v['variant']

                if variant == 'main': 
                    return True 
    return False 


def get_attr(objs, attr_name):
    attr = mc.getAttr('%s.%s' % (objs, attr_name))
    return attr

def list_shot_status(shotName):
    startFrame, endFrame, seqStartFrame, seqEndFrame = shot_info(shotName)
    time = shot_scale(shotName)
    return startFrame, endFrame, time

def find_cam(shotName):
    currentConnections = mc.listConnections('{}.currentCamera'.format(shotName), source=True)
    if not currentConnections:
        return None
    con = currentConnections[0]
    objType = mc.objectType(con)
    if objType == 'transform':
        shps = mc.listRelatives(con, shapes=True, type='camera')
        if shps:
            return  [con]
    elif objType == 'camera':
        return mc.listRelatives(con, parent=True)

def find_cam_shape(cam):
    return mc.listRelatives(cam, shapes=True)

def disconnect_attr_cam(attr1, attr2):
    if mc.isConnected(attr1, attr2):
        mc.disconnectAttr('%s.message' % (attr1), '%s.currentCamera' % attr2)

def connect_attr_cam(attr1, attr2):
    mc.connectAttr('%s.message'%(attr1), '%s.currentCamera'%attr2, f=True)

def get_panel():
    vis_panel = mc.getPanel(vis =1)
    for view in vis_panel:
        if mc.getPanel(typeOf=view) == 'modelPanel':
            current_view = view
    return current_view

def set_panel_show_only_polygon(panel):
    mm.eval('modelEditor -e -allObjects 0 %s;modelEditor -e -polymeshes true %s; modelEditor -e -pluginObjects gpuCacheDisplayFilter true %s; modelEditor -e -imagePlane true %s;'%(panel, panel, panel, panel))

def export_selection(obj, dstPath):
    mc.select(obj)
    dirname = os.path.dirname(dstPath)
    os.makedirs(dirname) if not os.path.exists(dirname) else None
    return mc.file(dstPath, es=True, f=True, type='mayaAscii')

def disconstraint_selection(obj, shotName):
    constraintList = []
    targetList = []
    startFrame, endFrame, seqStartFrame, seqEndFrame = shot_info(shotName)
    constraintObj = mc.listRelatives(obj, ad=True, typ='constraint', f=True)
    if constraintObj:
        for con in constraintObj:
            if not mc.referenceQuery(con, isNodeReferenced=True):
                target = mc.listRelatives(con, p=True, f=True)[0]
                targetList.append(target)
                constraintList.append(con)

        if targetList:
            isolate_select(obj)
            mc.bakeSimulation(targetList, sb=1, hi='below', t=(startFrame, endFrame))
            mc.delete(constraintList)
            isolate_not_select()

def isolate_select(roots):
    panel_act = mc.paneLayout('viewPanes', q=True, pane1=True)
    mc.select(d=True)

    if roots:
        mc.select(roots)
        mc.isolateSelect( panel_act, state=True )
        mc.isolateSelect( panel_act, addSelected=True)

def isolate_not_select():
    panel_act = mc.paneLayout('viewPanes', q=True, pane1=True)
    mc.isolateSelect( panel_act, state=False )
    mc.select(d=True)

def export_camera(dstPath, shotName):
    # find cam
    cam_rig = find_cam_grp(shotName)
    if cam_rig:
        # get camera out of Cam_Grp before export
        unparent_camgrp()
        # get namespace
        if mc.referenceQuery(cam_rig, inr=True):
            namespace = mc.referenceQuery(cam_rig, ns=True)
        else:
            namespace = cam_rig.split(':')[0] if ':' in cam_rig else '%s_cam' % shotName

        # disconnect camera from shot to persp cam
        persps = mc.ls('perspShape', type='camera')
        if persps:
            set_current_cam(shotName, persps[0])

        # make directory
        dirname = os.path.dirname(dstPath)
        os.makedirs(dirname) if not os.path.exists(dirname) else None

        # export selection, preserve ref, no shade info 
        mc.select(cam_rig, r=True)
        result = mc.file(dstPath, 
                        f=True, # force overwrite
                        chn=True, # channels
                        con=True, # constraints
                        ch=True, # construction history
                        sh=False, # NO shader should be exported
                        es=True, # export selected
                        pr=True, # preserve ref
                        type='mayaAscii', 
                        options="v=0;")

        cam_tr, cam_shp = find_cam_shape(cam_rig)
        set_current_cam(shotName, cam_shp)

        # group the cam back in Cam_Grp
        group_cam(shotName)

        logger.info('Export maya camera success %s' % dstPath)
        return result

def set_current_cam(shotName, camera): 
    shotNode = pm.PyNode(shotName)
    if camera:
        cam = pm.PyNode(camera)
        if isinstance(cam, pm.nt.Transform):
            camShp = cam.getShape()
        if isinstance(cam, pm.nt.Camera): 
            camShp = cam
        pm.connectAttr(camShp.message, shotNode.currentCamera, f=True)
    # else:
    #     shotNode.currentCamera.disconnect()

def current_camera(shotName): 
    shot = pm.PyNode(shotName)
    cam = shot.currentCamera.get()
    if cam:
        return cam.shortName()

def find_cam_grp(shotName):
    scene = context_info.ContextPathInfo()
    key = scene.projectInfo.asset.get('camGroup')
    camera = mc.shot(shotName, q=True, currentCamera=True)
    if camera:
        camNs = ''
        if ':' in camera:
            camNs = camera.split(':')[0]
            camNs += ':'
        camGrp = '%s%s' % (camNs, key)
        return camGrp if mc.objExists(camGrp) else camera

def find_cam_rig(shot):
    camera = mc.shot(shot, q=True, currentCamera=True)
    cam_type = mc.nodeType(camera)
    if cam_type == 'camera':
        camTr = mc.listRelatives(camera, parent=True)[0]
        return camTr
    elif cam_type == 'transform':
        camShape = mc.listRelatives(camera, shapes=True, type='camera')
        if camShape:
            return camera
    return camera

def is_active_cam(cam):
    passiveExt = ['.abc']
    activeExt = ['.ma']
    if mc.referenceQuery(cam, inr=True):
        fp = mc.referenceQuery(cam, f=True, withoutCopyNumber=True)
        f, ext = os.path.splitext(fp)
        if ext in activeExt:
            return True
        else:
            return False
    else:
        return True

def flat_reference(obj):
    if mc.referenceQuery(obj, inr=True):
        path = mc.referenceQuery(obj, f=True)
        namespace = mc.referenceQuery(obj, ns=True)
        objectName = obj.replace('%s:' % namespace[1:], '')
        mc.file(path, importReference=True)
        remove_namespace(namespace)
    else:
        namespace = obj.split(':')[0] if ':' in obj else ''
        objectName = obj.replace('%s:' % namespace, '') if namespace else obj
        remove_namespace(namespace) if namespace else None
    return objectName

def edit_camera(shotName):
    passiveExt = ['.abc']
    activeExt = '.ma'
    # unparent the camera to world
    unparent_camgrp()
    # find the cam
    camGrp = find_cam_grp(shotName)
    cam = current_camera(shotName)
    cam_name = cam.split(':')[-1]

    # only work if the camGrp is referenced
    if mc.referenceQuery(camGrp, inr=True):
        # find out the file extension
        path = mc.referenceQuery(camGrp, f=True).replace('\\', '/')
        pathNoCopy = path.split('{')[0]
        # rnNode = mc.referenceQuery(camGrp, referenceNode=True)
        namespace = mc.file(path, q=True, namespace=True)
        ext = os.path.splitext(pathNoCopy)[-1]

        # if the cam is .abc
        if ext in passiveExt: 
            # remove abc ref
            mc.file(path, rr=True)

            # import shot cam.ma while preserving references
            activePath = '%s%s' % (os.path.splitext(pathNoCopy)[0], activeExt)
            nodes =  mc.file(activePath, i=True, type='mayaAscii', ignoreVersion=True, 
                    ra=True, mergeNamespacesOnClash=True,
                    namespace=':', options="v=0", pr=True, rnn=True)
            print 'Edit cam with: %s' % path

            # find camera in imported objects
            imported_cams = [n for n in nodes if mc.nodeType(n)=='transform' \
                and mc.listRelatives(n, shapes=True, type='camera') \
                and n.endswith(cam_name)]
            if imported_cams:
                cam = imported_cams[0]

                # reconnect new imported cam with shot node
                camera = find_cam_shape(cam)
                set_current_cam(shotName, camera[1])

                # if the import contents has reference in it, it's the new method
                if mc.referenceQuery(cam, inr=True):
                    # lock cam transform
                    rnNode = mc.referenceQuery(cam, referenceNode=True)
                    maya_utils.lock_ref_cam(rnNode=rnNode, state=1)
                # old method with imported camera
                else:
                    # lock cam transform
                    maya_utils.lock_ref_cam(camera=cam, state=1)

                    # add namespace (if there's none)
                    if not mc.namespace(ex=namespace):
                        mc.namespace(add=namespace)
                    # add namespace to node if none (for backward compatability)
                    pynodes = [pm.PyNode(n) for n in nodes]
                    for node in pynodes:
                        nodeName = node.nodeName()
                        if not nodeName.startswith(namespace):
                            node.rename('%s:%s' %(namespace, nodeName))
                if not path: 
                    return False
            else:
                raise RuntimeError('Cannot find camera: %s in the imported contents.' %(cam_name))
    else:
        return False


def find_cam_namespace(shotName): 
    camGrp = find_cam_grp(shotName)
    cam = current_camera(shotName)
    if cam: 
        namespace = cam.split(':')[0]
        return namespace


def unparent_camgrp(): 
    camGrp = 'Cam_Grp'
    if mc.objExists(camGrp): 
        members = mc.listRelatives(camGrp, c=True)
        if members:
            for member in members: 
                mc.parent(member, w=True)

def switch_camera(shotName, rig=True): 
    cam = current_camera(shotName)
    # switchExt = '.ma' if rig else '.abc'
    if cam and mc.referenceQuery(cam, inr=True): 
        path = mc.referenceQuery(cam, f=True)
        rnNode = mc.referenceQuery(cam, referenceNode=True)
        newPath = '%s.abc' % (os.path.splitext(path.split('{')[0])[0])

        if newPath != path.split('{')[0]:
            # unload the reference first
            mc.file(path, unloadReference=True)
            # cleanup the reference
            mc.file(cleanReference=rnNode)
            try: 
                mc.file(newPath, loadReference=rnNode, prompt=False)
            except Exception as e: 
                logger.error(e)
                logger.warning('File read error. Could be arnold .ai issues')
            path = mc.referenceQuery(rnNode, f=True)
            maya_utils.lock_ref_cam(rnNode=rnNode, state=1)
            return path
        else: 
            return path

def switch_reference(obj, new_path):
    path = mc.referenceQuery(obj, f=True)
    rnNode = mc.referenceQuery(obj, referenceNode=True)

    # unload ref
    mc.file(path, unloadReference=True)

    # cleanup animcurves
    cleanupRefAnimCurves(rnNode)

    # cleanup the reference
    mc.file(cleanReference=rnNode)

    # replace ref path to abc
    try: 
        mc.file(new_path, loadReference=rnNode, prompt=False)
    except Exception as e: 
        logger.error('Error switching to passive cam: %s' %new_path)
        logger.error(e)

def cleanupRefAnimCurves(refNode):
    allConnectedCurves = mc.listConnections('%s.placeHolderList' %refNode, s=True, d=False, type='animCurve')
    animCurves = []
    if allConnectedCurves:
        animCurves = [c for c in allConnectedCurves if not mc.referenceQuery(c, inr=True)]
    try:
        mc.delete(animCurves)
        print '%s unused animCurves deleted.' %(len(animCurves))
    except:
        pass

def switch_hero_cam(entity, shotName): 
    scene = entity
    scene.context.update(step='default', look='main')
    dirname = scene.path.hero().abs_path()
    basename = scene.output_name(outputKey='camCacheHero', hero=True)
    heroCam = '%s/%s' % (dirname, basename)
    cam = current_camera(shotName)

    if os.path.exists(heroCam) and cam: 
        # new method
        if mc.referenceQuery(cam, inr=True):
            rnNode = mc.referenceQuery(cam, referenceNode=True)
            path = mc.referenceQuery(cam, f=True)
             # unload the reference first
            mc.file(path, unloadReference=True)
            # cleanup all anim curves connected to refNode
            cleanupRefAnimCurves(refNode=rnNode)
            # cleanup the reference
            mc.file(cleanReference=rnNode)
            # switch ref path to .abc
            mc.file(heroCam, loadReference=rnNode, prompt=False)

        else:
            camgrp_name = scene.projectInfo.asset.get('camGroup')
            camgroups = mc.ls(camgrp_name) + mc.ls('%s_cam:%s' %(shotName, camgrp_name))
            if camgroups:
                mc.delete(camgroups)

            # create new reference to the cam .abc
            namespace = '%s_cam' % shotName
            cam = create_reference(namespace, heroCam, mergeNamespacesOnClash=True)
        
        # get the cam shp
        cam, camShp = find_cam_shape(cam)

        # set current cam to sequencer
        set_current_cam(shotName, camShp)
        set_default_display(shotName)

        mc.select(cl=True)
    else: 
        logger.warning('{} not exists'.format(heroCam))

def remove_namespace(namespace):
    mc.namespace(f=True, mv=(':%s' % namespace, ':'))
    mc.namespace(removeNamespace=namespace)

def clear_namespace_content(namespace):
    contents = mc.namespaceInfo(namespace, ls=True)
    if contents:
        for obj in contents:
            if mc.objExists(obj):
                mc.delete(obj)

def create_reference(namespace, path, **kwargs):
    currents = mc.ls(assemblies=True)
    path = path.split('{')[0]
    result = mc.file(path, r=True, ns=namespace, **kwargs)
    refObject = [a for a in mc.ls(assemblies=True) if not a in currents]
    return refObject[0] if refObject else None

def find_cam_shape(group):
    cam = None
    cams = [a for a in mc.ls(group, dag=True) if mc.objectType(a, isType='camera')]
    camShape = cams[0] if cams else None
    if camShape:
        cam = mc.listRelatives(camShape, p=True)[0]
    return [cam, camShape]

def is_batch():
    return mc.about(batch=True)

def viewport_update(state):
    mc.refresh(su=not state)

def lst_audio():
    return mc.ls(type = 'audio')

def shot_info_data(shotName):
    return shot_info(shotName)

def add_asm_set(entity=None): 
    if not entity == None: 
        setGrp = entity.projectInfo.set_grp()
        return setGrp if mc.objExists(setGrp) else None
    return 'Set_Grp' if mc.objExists('Set_Grp') else None

def add_shotdress(entity=None): 
    if not entity == None: 
        shotdressGrp = entity.projectInfo.shotdress_grp()
        return shotdressGrp if mc.objExists(shotdressGrp) else None
    return 'ShotDress_Grp' if mc.objExists('ShotDress_Grp') else None

def set_default_display(shotName): 
    cam = current_camera(shotName)
    if cam: 

        if mc.objExists('%s.displayResolution' % cam): 
            mc.setAttr('%s.displayResolution' % cam, 1)
        if mc.objExists('%s.displayGateMask' % cam): 
            mc.setAttr('%s.displayGateMask' % cam, 1)
        if mc.objExists('%s.displayGateMaskColor' % cam): 
            mc.setAttr('%s.displayGateMaskColor' % cam, 0, 0, 0, type='double3')
        if mc.objExists('%s.displayGateMaskOpacity' % cam): 
            mc.setAttr('%s.displayGateMaskOpacity' % cam, 1)

def group_cam(shotName): 
    cam = current_camera(shotName)
    camGrp = 'Cam_Grp'
    if not mc.objExists(camGrp): 
        camGrp = mc.group(em=True, n=camGrp)
    if cam: 
        if mc.referenceQuery(cam, inr=True): 
            topGrp = maya_utils.find_top_reference(cam)
            currentParent = mc.listRelatives(topGrp, p=True)

            if currentParent: 
                if not currentParent[0] == camGrp: 
                    mc.parent(topGrp, camGrp)
            else: 
                mc.parent(topGrp, camGrp)

def get_cam_dependencies(shotName):
    cam_ns = '%s_cam' %shotName
    if not cam_ns:
        return
    camshot = camera_info(shotName)

    cons_types = ('pointConstraint', 'orientConstraint', 'parentConstraint', 'aimConstraint', 'scaleConstraint')
    bake_objs = set()
    constraints = set()
    for con in [node for node in pm.ls(type='constraint') if not node.isReferenced() and node.type() in cons_types]:
        targets = con.getTargetList()
        camTargets = [t for t in targets if t.nodeName().startswith(cam_ns)]

        children = list(set([c for c in con.outputs(type='transform') if c != con]))
        child = None
        if children:
            child = children[0]
        else:
            continue
        camChild = child.nodeName().startswith(cam_ns)
        toBake = None
        # camera is constrainted
        if camChild and not camTargets:
            # do bake
            print 'Cam is constrainted: %s' %child
            toBake = child

        elif camTargets and not camChild:

            print 'Cam is target: %s' %child
            toBake = child

        if toBake:
            bake_objs.add(toBake)
            constraints.add(con)

    # if bake_objs:
    bake_objs = list(bake_objs)
    return bake_objs, constraints

def bake_transform(shotName, objs):
    startFrame, endFrame, seqStartFrame, seqEndFrame = shot_info(shotName)
    renderer = maya_utils.getCurrentViewportRenderer()
    with maya_utils.FreezeViewport(mode=renderer) as fvp:
        pm.bakeResults(objs,
                    simulation=True,
                    t=(startFrame, endFrame), 
                    sampleBy=1, 
                    disableImplicitControl=True,
                    preserveOutsideKeys=True,
                    sparseAnimCurveBake=False, 
                    removeBakedAttributeFromLayer=False, 
                    removeBakedAnimFromLayer=False, 
                    bakeOnOverrideLayer=False, 
                    minimizeRotation=True,
                    controlPoints=False, 
                    shape=False)


def record_namespace_from_selection(shotName): 
    from rf_utils import user_info
    from rf_utils.pipeline import namespace_control as nc
    refPaths = []
    objects = mc.ls(sl=True, l=True)

    entity = context_info.ContextPathInfo()
    entity.context.update(entityCode=shotName, entity='none')
    entity.update_scene_context()

    ncontrol = nc.NamespaceControl(entity)
    workspace = mc.file(q=True, loc=True)
    user = user_info.User()
    
    for obj in objects: 
        if mc.referenceQuery(obj, inr=True): 
            refPath = mc.referenceQuery(obj, f=True)

            if not refPath in refPaths: 
                refPaths.append(refPath)

    for path in refPaths: 
        namespace = mc.referenceQuery(path, ns=True)[1:]
        ncontrol.add_record(namespace, workspace, path, user.login_user(), override=False)

def delete_nodes(nodes):
    try:
        mc.delete(nodes)
    except Exception, e:
        print e

def turnoff_undo():
    mc.undoInfo(st=False)

def turnon_undo():
    mc.undoInfo(st=True)
