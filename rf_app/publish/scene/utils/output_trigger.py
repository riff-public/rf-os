import os 
import sys 
import logging
from rf_utils.sg import sg_process 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
sg = sg_process.sg

def run(key, dataType, scene, prefer_step): 
    logger.debug('Preferred step trigger start ...')
    if prefer_step == 'sim': 
        assetName = key.split('_')[0] # assume first element is asset name
        suffix_map = {'abc_cache': 'cloth', 'yeti_cache': 'hair'}
        target_task = '{}_{}'.format(assetName, suffix_map[dataType])
        filters = [
            ['project.Project.name', 'is', scene.project], 
            ['entity.Shot.code', 'is', scene.name], 
            ['step.Step.short_name', 'is', prefer_step], 
            ['content', 'is', target_task]]
        fields = ['id', 'content']
        task = sg.find_one('Task', filters, fields)
        if not task: 
            shot = sg_process.get_shot_entity(project=scene.project, code=scene.name)
            step = sg.find_one('Step', [['short_name', 'is', prefer_step], ['entity_type', 'is', 'Shot']])
            data = {
                'project': shot['project'], 'step': step, 'entity': shot, 
                'content': target_task, 'sg_type': 'Shot'}
            task = sg.create('Task', data)
        status = 'fix'
        description = '{} overwriten cache'.format(scene.step)
        return sg_process.set_task_status(task['id'], status, description)
