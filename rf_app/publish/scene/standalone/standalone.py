import subprocess
import sys


def main():
    srcFile = sys.argv[1]
    shot = sys.argv[2]

    command_file_path = "./commands/export.py"

    subprocess.call([
        'C:/Program Files/Autodesk/Maya2017/bin/mayapy.exe',
        '{command_file_path}'.format(command_file_path=command_file_path),
        '{srcFile}'.format(srcFile=srcFile),
        srcFile,
        shot
    ])


main()


# USAGE
# command
# python .\standalone.py maya_file.ma all
