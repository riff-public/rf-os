import os
import sys

core = '%s/core' % os.environ.get('RFSCRIPT_DEV')
if not core in sys.path:
    sys.path.append(core)

from rf_utils.context import context_info
from rf_app.publish.scene.utils import maya_hook as hook
from rf_app.publish.scene.export import cache
from rf_app.publish.scene import export
from rf_app.publish.scene import app

import pymel.core as pm
import maya.cmds as mc
import maya.mel as mm
reload(app)
reload(hook)


def get_context(maya_file):
    context = context_info.ContextPathInfo(path=maya_file)
    return context


def get_all_shots():
    shots = hook.list_shot()
    print "shots", shots
    return shots


def run_cache(scene, shotNames):
    print "run export"
    datas = []
    for shotName in shotNames:
        shotData = export.export_list(scene, shotName)
        for key, data in shotData.iteritems():
            datas.append(data)

    # cache.run(scene, shotname)
    print app
    app.do_export(scene, itemDatas=datas, uiMode=False, widget=None)
    print "finish run export"


def main():
    print "sys.argv", sys.argv
    mayaFile = sys.argv[1]
    src_file = sys.argv[2]
    shot_type = sys.argv[3]
    context = get_context(src_file)

    mc.file(mayaFile, o=True, f=True)

    if shot_type == 'all':
        shots = get_all_shots()
        run_cache(context, shots)


main()
