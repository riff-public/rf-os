#!/usr/bin/env python
# -- coding: utf-8 --

import os
import sys
import traceback
from collections import OrderedDict

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

module_path = sys.modules[__name__].__file__
module_dir = os.path.dirname(module_path)
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_utils.widget import project_widget
from rf_utils.widget import entity_browse_widget
from rf_utils.widget import sg_entity_widget
from rf_utils.widget import entity_step_widget
from rf_utils.widget import filter_widget
from rf_utils.widget import register_file_widget
from rf_utils.widget import status_widget
from rf_utils import icon
sg = None


class Setting:
    refreshIcon = '%s/icons/refresh.png' % module_dir


class ScenePublisherUi(QtWidgets.QWidget):
    status = QtCore.Signal(str)

    def __init__(self, parent=None):
        super(ScenePublisherUi, self).__init__(parent)
        self.layout = QtWidgets.QVBoxLayout()
        self.top_layout = QtWidgets.QGridLayout()
        self.splitter = QtWidgets.QSplitter(QtCore.Qt.Horizontal)
        self.center_layout = QtWidgets.QHBoxLayout(self.splitter)
        self.bottom_layout = QtWidgets.QHBoxLayout()
        self.layout.addLayout(self.top_layout)
        self.layout.addWidget(self.splitter)
        self.layout.addLayout(self.bottom_layout)
        self.setLayout(self.layout)

        self.setup_widget()
        self.set_default()
        self.init_signals()
        self.init_functions()

    def setup_widget(self):
        # logo
        self.logo = QtWidgets.QLabel()
        self.logo.setPixmap(QtGui.QPixmap(icon.logo).scaled(64, 64, QtCore.Qt.KeepAspectRatio))

        spacer = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        self.project_widget = project_widget.SGProjectComboBox(sg=sg, layout=QtWidgets.QHBoxLayout())
        self.entity_step_widget = entity_step_widget.EntityStepWidget(sg=sg)
        self.submit_button = QtWidgets.QPushButton('Submit')
        self.submit_button.setMinimumSize(QtCore.QSize(120, 40))

        self.top_layout.addWidget(self.project_widget, 0, 0, 1, 1)
        self.top_layout.addWidget(self.entity_step_widget, 0, 1, 1, 1)
        self.top_layout.addWidget(self.submit_button, 0, 2, 1, 1)
        self.top_layout.addItem(spacer, 0, 3, 1, 1)
        self.top_layout.addWidget(self.logo, 0, 5, 1, 1)

        self.top_layout.setColumnStretch(0, 1)
        self.top_layout.setColumnStretch(1, 2)
        self.top_layout.setColumnStretch(2, 1)
        self.top_layout.setColumnStretch(3, 4)
        self.top_layout.setColumnStretch(4, 1)

        # filters 
        self.type_ep_filter = filter_widget.FilterWidget('Sequence')
        self.task_filter = filter_widget.FilterWidget('Task')
        self.status_filter = status_widget.StatusListWidget(layout=QtWidgets.QVBoxLayout())
        self.status_filter.label.setText('Status')
        
        self.type_ep_filter.set_multiple_selection()

        self.entity_widget = EntityWidget(self.splitter)
        self.entity_widget.label.setText('Shot')

        self.version_widget = VersionWidget(self.splitter)
        self.version_widget.label.setText('Version')

        self.reg_file_widget = RegFileWidget(self.splitter)
        self.reg_file_widget.label.setText('Files')

        self.filter_splitter = QtWidgets.QSplitter(QtCore.Qt.Vertical)
        self.filter_layout = QtWidgets.QVBoxLayout(self.filter_splitter)

        self.filter_splitter.addWidget(self.task_filter)
        self.filter_splitter.addWidget(self.status_filter)
        self.filter_splitter.addWidget(self.type_ep_filter)

        # self.file_splitter.addWidget(self.version_widget)
        # self.file_splitter.addWidget(self.reg_file_widget)

        self.splitter.addWidget(self.filter_splitter)
        self.splitter.addWidget(self.entity_widget)
        self.splitter.addWidget(self.version_widget)
        self.splitter.addWidget(self.reg_file_widget)

        self.splitter.setStretchFactor(0, 40)
        self.splitter.setStretchFactor(1, 40)
        self.splitter.setStretchFactor(2, 40)
        self.splitter.setStretchFactor(3, 40)
        self.splitter.setStretchFactor(4, 40)
        # self.splitter.setStretchFactor(3, 20)

        # bottom layout 
        self.spacer_bottom = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.publish_button = QtWidgets.QPushButton('Publish')
        self.publish_button.setMinimumSize(QtCore.QSize(260, 40))
        self.bottom_layout.addItem(self.spacer_bottom)
        self.bottom_layout.addWidget(self.publish_button)

        self.bottom_layout.setStretch(0, 4)
        self.bottom_layout.setStretch(1, 1)

    def init_signals(self):
        self.project_widget.projectChanged.connect(self.project_changed)

    def init_functions(self):
        # this will trigger current project to list episode, sequence and shots
        self.project_widget.emit_project_changed()

    def project_changed(self, project): 
        self.entity_step_widget.set_project(project['name']) 
        self.entity_step_widget.entity_selected()

    def set_default(self): 
        self.entity_step_widget.scene_radioButton.setChecked(True)


class EntityWidget(QtWidgets.QWidget):
    """docstring for EntityWidget"""
    def __init__(self, parent=None):
        super(EntityWidget, self).__init__(parent=parent)
        self.layout = QtWidgets.QVBoxLayout()
        self.widget = sg_entity_widget.EntityListWidget()
        self.widget.listWidget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.widget.set_text_mode()
        self.header_layout = QtWidgets.QHBoxLayout()
        self.label = QtWidgets.QLabel()
        self.search_lineEdit = QtWidgets.QLineEdit()
        self.search_lineEdit.setMaximumSize(QtCore.QSize(2000, 20))
        self.header_layout.addWidget(self.label)
        self.header_layout.addWidget(self.search_lineEdit)
        self.layout.addLayout(self.header_layout)
        self.layout.addWidget(self.widget)
        self.header_layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.layout)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.init_signals()

    def init_signals(self): 
        self.search_lineEdit.textChanged.connect(self.search)

    def search(self): 
        key = self.search_lineEdit.text()
        items = self.widget.all_items()

        for item in items: 
            if key: 
                hidden = True 
                if key in item.text(): 
                    hidden = False
            else: 
                hidden = False
            item.setHidden(hidden)

    def clear(self): 
        self.widget.clear()


        

class VersionWidget(QtWidgets.QWidget):
    """docstring for VersionWidget"""
    item_selected = QtCore.Signal(object)

    def __init__(self, parent=None):
        super(VersionWidget, self).__init__(parent=parent)
        self.layout = QtWidgets.QVBoxLayout()
        self.label = QtWidgets.QLabel()
        self.widget = QtWidgets.QListWidget()
        self.layout.addWidget(self.label)
        self.layout.addWidget(self.widget)
        self.setLayout(self.layout)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.widget.currentItemChanged.connect(self.version_selected)

    def add_version(self, version, path): 
        item = QtWidgets.QListWidgetItem()
        item.setText(version)
        item.setData(QtCore.Qt.UserRole, (version, path))
        self.widget.addItem(item)

    def version_selected(self): 
        path = self.widget.currentItem().data(QtCore.Qt.UserRole)
        self.item_selected.emit(path)
    

class RegFileWidget(QtWidgets.QWidget):
    """docstring for RegFileWidget"""
    def __init__(self, parent=None):
        super(RegFileWidget, self).__init__(parent=parent)
        self.layout = QtWidgets.QVBoxLayout()
        self.label = QtWidgets.QLabel()
        self.widget = register_file_widget.RegisterFileWidget()
        self.layout.addWidget(self.label)
        self.layout.addWidget(self.widget)
        self.setLayout(self.layout)
        self.layout.setContentsMargins(0, 0, 0, 0)

        self.layout.insertWidget(0, self.label)

    def add_file(self, display, data): 
        item = QtWidgets.QListWidgetItem()
        item.setText(display)
        item.setData(QtCore.Qt.UserRole, data)
        self.widget.addItem(item)
        
        
        


# splitter example
# from PySide2 import QtWidgets
# from PySide2 import QtCore
# widget = QtWidgets.QWidget()
# layout = QtWidgets.QHBoxLayout()
# splitter = QtWidgets.QSplitter(QtCore.Qt.Horizontal)
# layout.addWidget(splitter)
# splitter.addWidget(QtWidgets.QPushButton())
# splitter.addWidget(QtWidgets.QPushButton())
# widget.setLayout(layout)
# widget.show()


