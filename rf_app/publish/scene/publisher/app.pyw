#!/usr/bin/env python
# -- coding: utf-8 --

_title = 'RF Scene Publisher'
_version = 'v.0.0.2'
_des = 'Wip'
uiName = 'ScenePublisherUi'

#Import python modules
import sys
import os
import logging
import getpass
import traceback
from collections import OrderedDict
import os
import json
import pickle

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

core = '%s/core' % os.environ.get('RFSCRIPT')
sys.path.append(core)

# import config
import rf_config as config

from rf_utils import log_utils
from rf_utils.ui import load
reload(load)
from rf_utils.ui import stylesheet
# maya & module function
from . import ui
from . import tabs
from rf_utils.sg import sg_process
from rf_utils.widget import dialog
from rf_utils.context import context_info
from rf_utils import thread_pool
from rf_utils import file_utils
ui.sg = sg_process.sg

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui


isNuke = True
try:
    import nuke
    import nukescripts
except ImportError:
    isNuke = False

modulePath = sys.modules[__name__].__file__.replace('\\', '/')
moduleDir  = os.path.dirname(modulePath)
appModuleDir = os.path.dirname(moduleDir)

class FullConfig:
    """ name of tabs module """
    activeTab = 'builder'
    allowedTabs = ['builder']
    tabNameMap = {'builder': 'Render Builder'}

class ViewerConfig:
    activeTab = 'colorscript'
    allowedTabs = ['mastershot', 'colorscript', 'mattepaint', 'media']
    tabNameMap = {}

class ScenePublisher(QtWidgets.QMainWindow):
    def __init__(self, tab_config=FullConfig, parent=None):
        #Setup Window
        super(ScenePublisher, self).__init__(parent)

        # ui read
        uiFile = '%s/ui.py' % moduleDir
        self.ui = ui.ScenePublisherUi()
        self.w = 1080
        self.h = 680
        self.tab_config = tab_config
        self.threadpool = QtCore.QThreadPool()
        self.threadpool.setMaxThreadCount(1)

        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)
        self.resize(self.w, self.h)
        self.setWindowTitle('%s %s %s' % (_title, _version, _des))
        self.setWindowIcon(QtGui.QIcon('{}/icons/app_icon.png'.format(moduleDir)))
        self.setup_ui()
        self.init_signals()
        self.init_functions()


    def closeEvent(self, event):
        '''
        Closes all tab widgets before actually close itself triggering each tab closeEvents
        '''
        pass 

    def setup_ui(self):
        # set tabs
        pass

    def init_signals(self):
        self.ui.submit_button.clicked.connect(self.load_data) 
        self.ui.type_ep_filter.item_selected.connect(self.filter_selected)
        self.ui.task_filter.item_selected.connect(self.filter_selected)
        self.ui.status_filter.item_selected.connect(self.filter_selected)
        self.ui.entity_widget.widget.selected.connect(self.entity_selected)
        self.ui.version_widget.item_selected.connect(self.version_selected)
        self.ui.publish_button.clicked.connect(self.publish)

    def init_functions(self):
        pass 

    def load_data(self): 
        project_entity = self.ui.project_widget.current_item()
        entity_type = self.ui.entity_step_widget.get_entity_type()
        step = self.ui.entity_step_widget.get_step()

        # clear ui 
        self.ui.type_ep_filter.clear()
        self.ui.task_filter.clear()
        self.ui.status_filter.clear()
        self.ui.entity_widget.clear()

        self.ui.submit_button.setEnabled(False)
        self.ui.submit_button.setText('Loading ...')
        worker = thread_pool.Worker(self.fetch_data, project_entity, step)
        worker.signals.result.connect(self.fetch_finished)
        self.threadpool.start(worker)

    def fetch_data(self, project_entity, step): 
        group_dict = sg_process.get_task_by_step(project_entity, step)
        return group_dict

    def fetch_finished(self, result): 
        self.ui.submit_button.setEnabled(True)
        self.ui.submit_button.setText('Submit')
        self.data = result 
        self.set_display()

    def set_display(self): 
        """ display from self.data """ 
        self.set_type_ep(self.data)
        self.set_task(self.data)
        self.set_status(self.data)
        self.ui.publish_button.setEnabled(False)

    def filter_selected(self): 
        self.set_entities()

    def entity_selected(self, sg_entity): 
        """ this will list veresions 
        1. from a data file (later)
        2. form folder (for now)
        """ 
        self.entity = self.create_context_from_entity(sg_entity)
        # find version from _output key 
        self.list_versions(self.entity)

    def list_versions(self, entity): 
        """ this will be hardcoded for now and will be replaced by json data """ 
        version_path = os.path.split(self.entity.path.scheme(key='localOutput').abs_path())[0]
        versions = file_utils.list_folder(version_path)
        self.ui.version_widget.widget.clear()

        for version in versions: 
            path = '{}/{}'.format(version_path, version)
            self.ui.version_widget.add_version(version, path)

    def version_selected(self, data): 
        version, path = data
        self.entity.context.update(version=version)
        self.list_files(path)
        self.ui.publish_button.setEnabled(True)

    def list_files(self, path): 
        files = file_utils.list_file(path)
        self.ui.reg_file_widget.widget.clear()

        for file in files: 
            filepath = '{}/{}'.format(path, file)
            self.ui.reg_file_widget.add_file(file, filepath)

    def create_context_from_entity(self, sg_entity): 
        context = context_info.Context()
        context.update(project=sg_entity['project']['name'],
                    entityType=ui.entity_step_widget.Config.sg_entity_map[sg_entity['type']],
                    entity=sg_entity['code'],
                    entityGrp=sg_entity['type_ep_group'],
                    entityParent=sg_entity['entity_parent_key'],
                    entityCode=sg_entity['sg_shortcode'] or '', 
                    step=sg_entity['step'],
                    process='main'
                    )
        entity = context_info.ContextPathInfo(context=context)
        return entity


    def set_type_ep(self, data): 
        type_ep_list = [ui.filter_widget.Config.all_filter]

        for name, values in data.items(): 
            tasks = values['task']
            for task in tasks: 
                group_key = task.get('group_key')
                type_ep_list.append(group_key) if not group_key in type_ep_list else None
        
        self.ui.type_ep_filter.clear()
        for fltr in sorted(type_ep_list): 
            self.ui.type_ep_filter.add_filter(display=fltr, data=fltr, iconpath='')
        item = self.ui.type_ep_filter.filter.item(0)
        self.ui.type_ep_filter.filter.setCurrentItem(item)

    def set_task(self, data): 
        task_list = [ui.filter_widget.Config.all_filter]

        for name, values in data.items(): 
            tasks = values['task']
            for task in tasks: 
                taskname = task.get('content')
                task_list.append(taskname) if not taskname in task_list else None 

        self.ui.task_filter.clear()
        for task in sorted(task_list): 
            self.ui.task_filter.add_filter(display=task, data=task, iconpath='')
        item = self.ui.task_filter.filter.item(0)
        self.ui.task_filter.filter.setCurrentItem(item)

    def set_status(self, data): 
        status_list = [ui.filter_widget.Config.all_filter]

        for name, values in data.items(): 
            tasks = values['task']
            for task in tasks: 
                status = task.get('sg_status_list')
                status_list.append(status) if not status in status_list else None 

        self.ui.status_filter.clear()
        for status in status_list: 
            self.ui.status_filter.add_filter(status) 
        item = self.ui.status_filter.listWidget.item(0)
        self.ui.status_filter.listWidget.setCurrentItem(item)

    def set_entities(self): 
        type_ep_key = self.ui.type_ep_filter.selected_filters()
        task_key = self.ui.task_filter.selected_filters()
        status_key = self.ui.status_filter.selected_filters()
        entities = list()
        self.ui.entity_widget.clear()

        for name in sorted(self.data.keys()): 
            values = self.data[name]
            keywords = values['keywords']
            entity = values['entity']
            type_seq_result = True
            task_key_result = True 
            status_key_result = True

            if not ui.filter_widget.Config.all_filter in type_ep_key: 
                type_seq_result = any(a in keywords for a in type_ep_key)
            if not ui.filter_widget.Config.all_filter in task_key: 
                task_key_result = any(a in keywords for a in task_key)
            if not ui.filter_widget.Config.all_filter in status_key: 
                status_key_result = any(a in keywords for a in status_key)

            if all([type_seq_result, task_key_result, status_key_result]): 
                entities.append(entity)

        self.ui.entity_widget.widget.add_texts(entities, widget=False)

    def publish(self): 
        """ this onlywork and hard code for finalcam for now """ 
        logger.info('Publishing ...')
        self.ui.publish_button.setEnabled(False)
        worker = thread_pool.Worker(self.start_publish, self.entity)
        worker.signals.result.connect(self.publish_finished)
        self.threadpool.start(worker)

    def start_publish(self, entity): 
        from rf_app.publish.scene import export_cmd
        shotname = entity.name_code
        try: 
            data = export_cmd.get_export_dict(self.entity, shotname, ['camera_publish'])
            export_cmd.export_from_items(self.entity, data)
        except Exception as e:
            error = traceback.format_exc()
            traceback.print_exc()
            logger.error(error)
            return False

        taskname = 'finalCam'
        status = 'apr'
        sg_process.set_task(self.entity.project, self.entity.sg_entity_type, self.entity.name, taskname, status)
        return True

    def publish_finished(self, result): 
        if result: 
            self.ui.publish_button.setEnabled(True)
            logger.info('Set shotgun status complete')
            dialog.MessageBox.success('Success', 'Publish Complete')
        else: 
            dialog.MessageBox.error('Error', 'Publish error')


def show(mode='default'):
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        if mode == 'default':
            maya_win.deleteUI(uiName)
            myApp = ScenePublisher(tab_config=FullConfig, parent=maya_win.getMayaWindow())
        myApp.show()
    elif config.isNuke:
        from rf_nuke import nuke_win 
        # reload(nuke_win)
        logger.info('Run in Nuke\n')
        if mode == 'default':
            nuke_win.deleteUI(uiName)
            myApp = ScenePublisher(tab_config=FullConfig, parent=nuke_win._nuke_main_window())
        myApp.show()
    else:
        logger.info('Run in standalone\n')
        app = QtWidgets.QApplication.instance()
        if not app:
            app = QtWidgets.QApplication(sys.argv)
        if mode == 'default':
            myApp = ScenePublisher(tab_config=ViewerConfig, parent=None)
        myApp.show()
        stylesheet.set_default(app)
        sys.exit(app.exec_())
    return myApp


if __name__ == '__main__':
    show()
