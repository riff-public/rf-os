# entity = init_entity('project', 'act1', 'q0010', 's0010', 'finalcam', 'main', 'v001')
# export(entity, 'camera', publish=False)
import os 
import sys
import logging 
from collections import OrderedDict
import traceback
from rf_utils import custom_exception
import export as utils
from rf_utils import file_utils
from rf_app.publish.scene.utils import output_utils
from rf_utils.pipeline import snap_utils


reload(utils)
from rf_utils import file_utils
DIR = os.path.dirname(__file__).replace('\\', '/')
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())


class Config:
    dataSorted = ['camera', 'workspace', 'media', 'shotgun', 'cache', 'alembic_cache', 'yeti_cache']


def run(entity, module=None, processes=[], dst_type='local'): 
    entity = check_entity(entity) # check basic missing data 
    module_data = get_output_config(entity.project, module_name=module)
    module = utils.import_module(module)
    reload(module)
    all_result = True

    if module_data: 
        processes = module_data.keys() if not processes else processes

        for process in processes: 
            process_result = True
            if process in module_data.keys(): 
                process_data = module_data[process]
                attr_name = process_data['func']
                outputs = process_data[dst_type]
                dst_paths = list()

                for output in outputs: 
                    dir_key = output['outputDir']
                    name_key = output['outputName']
                    hero = output['hero']

                    path = entity.path.scheme(dir_key).abs_path()
                    name = entity.output_name(outputKey=name_key, hero=hero)
                    path = '{}/{}'.format(path, name)
                    dst_paths.append(path)

                func = getattr(module, attr_name)

                # pick first output to export
                dst = dst_paths[0]
                
                # run 
                try: 
                    success = run_func(func, entity, dst)
                
                    if not success: 
                        logger.error('Error: no output "{}" from "{}.{}"'.format(dst, module, attr_name))
                        process_result = False
                        break 

                    # copy to other sources 
                    backup_success = backup(dst, dst_paths)
                    if not backup_success: 
                        logger.warning('Backup not complete')
                        process_result = False

                except Exception as e:
                    # store traceback to var
                    error = traceback.format_exc()
                    traceback.print_exc()
                    logger.error(error)
                    process_result = False

                if process_result: 
                    logger.debug('Process "{}" export success'.format(process))
                    logger.debug(dst_paths)
                else: 
                    logger.warning('Some error during exporting "{}"'.format(process))


def run_func(func, entity, dst): 
    print 'dst', dst
    exists_hash = file_utils.md5(dst) if os.path.exists(dst) else ''    
    output_file = func(entity, dst)

    # check output 
    if not os.path.exists(output_file): 
        raise custom_exception.PipelineError('No output from "{}"'.format(func.__name__))
    output_hash = file_utils.md5(output_file)
    success = True if not exists_hash == output_hash else False
    return success


def backup(src, dsts): 
    result = list()
    for dst in dsts: 
        if not dst == src: 
            exists_hash = file_utils.md5(dst) if os.path.exists(dst) else ''    
            file_utils.copy(src, dst)
            # check output 
            output_hash = file_utils.md5(dst)
            success = True if not exists_hash == output_hash else False
            if not success: 
                logger.warning('Backup failed "{}"'.format(dst))
            result.append(success)
    return all(result)


def list_process(project, module): 
    module_data = get_output_config(project, module_name=module)
    return module_data.keys()
    

def check_entity(entity): 
    # add look if missing 
    look = entity.process if entity.look == entity.context.noContext else entity.look 
    entity.context.update(look=look)
    return entity

def read_config(project): 
    config_project = '{}/config/{}_output.yml'.format(DIR, project)
    config_default = '{}/config/default_output.yml'.format(DIR, project)
    config_file = config_default if not os.path.exists(config_project) else config_project
    # read 
    config = file_utils.ymlLoader(config_file)
    return config 


def get_output_config(project, module_name): 
    config = read_config(project)
    module_data = config.get(module_name, dict())
    return module_data


def export_from_items(entity, item_data): 
    results = list()
    shotnames = list()
    for data in item_data:
        func = data['func']
        args = data['args']
        shotName = data['shotName']

        if not shotName in shotnames:
            shotnames.append(shotName)
            
        try:
            result, details = func(*args)
            results.append(True)

        except Exception as e:
            error = traceback.format_exc()
            traceback.print_exc()
            logger.error(error)
            results.append(False)

    output_utils.post_register(entity, shotnames)
    data = snap_utils.read(entity)
    if data:
        path = snap_utils.snap_file(entity)
        data = file_utils.ymlLoader(path) if os.path.exists(path) else dict()
        snap_utils.publish(data)
        timeName = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        newName = '%s/%s.%s' % (os.path.dirname(path), os.path.basename(path), timeName)
        os.rename(path, newName)
            
    return all(results)


def get_export_dict(entity, shotName, modName): 
    exportData = []
    datas = utils.export_list(entity, shotName, modName=modName)
    if datas:
        sortedData = sorted_data_display(datas)
        for key, data in sortedData.iteritems():
            exportData.append(data)
    return exportData


def get_export_list(scene):
    data = utils.read_config(project=scene.project)
    defaultFiles = data.get('default').keys()
    exportFiles = data.get(scene.step, defaultFiles).keys()
    return exportFiles


def sorted_data_display(inputData):
    """ sorted by order first if data available """ 
    sortedData = OrderedDict()
    # sort by order first 
    tempDict = OrderedDict()
    orderDict = OrderedDict()

    for index, display in enumerate(inputData): 
        data = inputData[display]
        order = data.get('order')
        dataType = data.get('type')
        data['display'] = display
        key = '%02d-%s' % (order, dataType) if order else display
        tempDict[key] = data 


    for order in sorted(tempDict.keys()):
        data = tempDict[order]
        orderDict[data['display']] = data

    inputData = orderDict

    if type(inputData) in [type(dict()), type(OrderedDict())]:
        for sortedType in Config.dataSorted:
            for display, data in inputData.iteritems():
                if sortedType == data['type']:
                    sortedData[display] = data

        for display, data in sorted(inputData.iteritems()):
            if not display in sortedData.keys():
                sortedData[display] = data

    return sortedData
