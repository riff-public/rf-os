# import all modules in maya_lib
import os
import sys
import glob
import importlib
from rf_app.publish.scene.export import *
from collections import OrderedDict
from rf_utils import file_utils
moduleDir = os.path.dirname(sys.modules[__name__].__file__)


sortedOrder = ['camera', 'workspace', 'cache']
moduleDataCache = OrderedDict()

def get_module():
    modules = glob.glob(os.path.dirname(__file__)+"/*.py")
    return [ os.path.basename(f)[:-3] for f in modules if os.path.isfile(f) and not f.endswith('__init__.py')]

def get_module_data():
    mods = OrderedDict()
    sortedMods = OrderedDict()
    allModules = get_module()

    for name in allModules:
        # func = __import__(name, globals(), locals(), [], -1)
        # reload(func)
        # mods[name] = func
        mods[name] = name

    for name in sortedOrder:
    	if name in mods.keys():
    		sortedMods[name] = mods[name]

    for name, data in mods.iteritems():
    	if not name in sortedMods.keys():
    		sortedMods[name] = data

    return sortedMods


def import_module(name): 
	func = __import__(name, globals(), locals(), [], -1)
	return func


def export_list(entity, shotName, modName=[], modData=None):
	modules = cache_module_data()
	exportData = OrderedDict()

	# config
	config = read_config(entity.project)
	modData = config.get(entity.step, config.get('default')) if not modData else modData
	modFilter = modData.keys()
	export = True 
	for mod in modules:
		if modName: 
			export = True if mod in modName else False 

		if export: 
			if mod in modFilter:
				exportFilter = modData[mod]
				mod_name = modules[mod]
				func = import_module(mod_name)
				reload(func)
				
				if hasattr(func, 'run'):
					exportDict = func.run(entity, shotName, exportFilter)

					if exportDict:
						for display, data in exportDict.iteritems():
							exportData[display] = data

	return exportData


def cache_module_data(): 
	global moduleDataCache
	if not moduleDataCache: 
		moduleDataCache = get_module_data() 
	return moduleDataCache


def read_config(project=''):
	default = 'default.yml'
	configPath = '%s/config' % moduleDir
	configName = '%s_export_config.yml' % project
	configFile = '%s/%s' % (configPath, configName)
	defaultConfigFile = '%s/%s' % (configPath, default)
	data = file_utils.ymlLoader(configFile) if os.path.exists(configFile) else file_utils.ymlLoader(defaultConfigFile)
	return data

def export_filters(project=''):
	data = read_config(project)
