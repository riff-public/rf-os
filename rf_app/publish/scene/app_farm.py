# v.0.0.1 polytag switcher
_title = 'Riff Cache Farm'
_version = 'v.0.2.0'
_des = 'Unlock with password and notification'
uiName = 'SubmitQueueUi'

#Import python modules
import sys
import os
import getpass
import logging
from functools import partial
from collections import OrderedDict
import traceback
from datetime import datetime

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

# import config
import rf_config as config
from rf_utils import log_utils


user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

from rf_qc import qc_widget
from rf_utils.widget import export_list_widget
from rf_utils.context import context_info
from rf_app.publish.scene import batch
from rf_utils.pipeline import lockdown
from rf_utils.pipeline import shot_asset
from rf_app.farm.cache_submit import app
from rf_app.publish.scene.farm import farm_widget

class Color:
    red = 'color: rgb(200, 0, 0);'
    green = 'color: rgb(0, 240, 0);'
    bgGreen = 'background-color: rgb(0, 140, 0);'
    bgYellow = 'background-color: rgb(200, 120, 0);'
    bgRed = 'background-color: rgb(140, 0, 0);'
    bgLightRed = 'background-color: rgb(140, 60, 60);'

class Ui(QtWidgets.QWidget):
    """docstring for Ui"""
    def __init__(self, parent=None):
        super(Ui, self).__init__(parent)
        
        self.layout = QtWidgets.QHBoxLayout()
        self.cacheLayout = QtWidgets.QVBoxLayout()
        self.qcLayout = QtWidgets.QVBoxLayout()
        self.optionLayout = QtWidgets.QVBoxLayout()
        self.qcWidget = qc_widget.QcWidget()
        self.selectiveCacheWidget = export_list_widget.ExportListWidget()
        self.filterWidget = export_list_widget.ExportListWidget()
        self.button = QtWidgets.QPushButton('Submit Cache to Queue')
        self.qcLayout.addWidget(self.qcWidget)
        self.add_console()
        self.add_choice()
        self.qcLayout.addWidget(self.button)
        self.layout.addLayout(self.qcLayout)
        self.layout.addLayout(self.optionLayout)
        self.layout.addLayout(self.cacheLayout)

        self.cacheWidget = farm_widget.QueueWidget()
        self.cacheLayout.addWidget(self.cacheWidget)


        self.showOptionButton = QtWidgets.QPushButton('>>')
        self.showOptionButton.setCheckable(True)
        self.optionLayout.addWidget(self.showOptionButton)

        self.setLayout(self.layout)

        self.qcLayout.setStretch(0, 3)
        self.qcLayout.setStretch(1, 1)
        self.qcLayout.setStretch(2, 0)

    def add_choice(self): 
        self.choiceLayout = QtWidgets.QHBoxLayout()
        self.publishShot_radioButton = QtWidgets.QRadioButton('Publish Cache')
        self.masterShot_radioButton = QtWidgets.QRadioButton('Master shot')
        self.choiceLayout.addWidget(self.publishShot_radioButton)
        self.choiceLayout.addWidget(self.masterShot_radioButton)
        self.qcLayout.addLayout(self.choiceLayout)
        self.publishShot_radioButton.setChecked(True)

    def add_console(self): 
        # console
        self.consoleLayout = QtWidgets.QVBoxLayout()
        self.consoleLabel = QtWidgets.QLabel('Information')
        self.consoleWidget = QtWidgets.QPlainTextEdit()
        self.consoleLayout.addWidget(self.consoleLabel)
        self.consoleLayout.addWidget(self.consoleWidget)
        self.qcLayout.addLayout(self.consoleLayout)


class Modules: 
    default = [
         # 'alembic_cache',
         'cache_single',
         # 'export_grp',
         # 'export_set',
         # 'instance_dress',
         # 'set_asset_list',
         # 'shotdress',
         # 'shot_workspace',
         'yeti_bg']

        
class RFShotQueue(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        #Setup Window
        super(RFShotQueue, self).__init__(parent)
        
        # ui read
        self.ui = Ui(parent)
        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)

        # title
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.w = 360 
        self.h = 500
        self.resize(self.w, self.h)

        self.set_default()
        self.init_functions()
        self.init_signals()

    def set_default(self): 
        self.ui.button.setMinimumSize(QtCore.QSize(0, 30))
        self.ui.selectiveCacheWidget.setVisible(False)
        self.hide_option_layout(False)

    def init_signals(self): 
        self.ui.qcWidget.check.connect(self.qc_pass) 
        self.ui.qcWidget.itemSelectionChanged.connect(self.console)
        self.ui.button.clicked.connect(self.submit_queue)
        self.ui.showOptionButton.clicked.connect(self.show_option)

    def init_functions(self): 
        self.entity = context_info.ContextPathInfo()
        self.init_qc()
        self.init_cache()
        self.ui.button.setEnabled(False)

    def init_qc(self): 
        # allow qc by config 
        self.ui.qcWidget.configFilter = True
        # update qc list by given project, step, entityType data
        self.entity.context.update(res='default')
        self.ui.qcWidget.update(self.entity.project, context_info.ContextKey.scene, self.entity.step, res=self.entity.res, entity=self.entity, process='default')

    def init_cache(self): 
        self.ui.cacheWidget.init_widget(self.entity)

    def qc_pass(self, status): 
        self.ui.button.setEnabled(status)
        self.ui.cacheWidget.activate_button(status)

    def console(self, data):
        """ display console """
        display = []
        func = data.get('func')
        doc = func.run.__doc__ if func else ''
        traceback = data.get('traceback')
        message = data.get('message')
        consoleColor = ''

        display.append(doc)

        if traceback:
            display = []
            display.append(traceback)
            consoleColor = Color.red

        if message:
            display = []
            display.append(message)
            consoleColor = ''

        displayMessage = ('\n').join(display)

        self.ui.consoleWidget.setPlainText(displayMessage)
        self.ui.consoleWidget.setStyleSheet(consoleColor)


    def show_option(self): 
        checked = self.ui.showOptionButton.isChecked()
        self.hide_option_layout(checked)
        self.ui.button.setVisible(not checked)
        # resize
        width = self.w + 100 if checked else self.w - 100
        self.resize(width, self.h)

    def hide_option_layout(self, state): 
        self.ui.cacheWidget.hide_option_layout(state)

    def is_publish(self): 
        return self.ui.publishShot_radioButton.isChecked()

    def submit_queue(self): 
        if self.is_publish():
            from rf_app.save_plus import save_utils
            save_utils.save_file_farm()

            # logger.info('Lock down shot ...')
            # lockdown.lock_shot(self.entity, 'anim')
            logger.info('Lock down asset ...')
            lockdown.lock_asset(self.entity, 'model')

        from rf_maya.rftool.clean.clean_scene import app
        record = app.CleaningFile()
        record.record_namespace()

        batch.submit_queue()


def show():
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run shot export\n')
        maya_win.deleteUI(uiName)
        myApp = RFShotQueue(maya_win.getMayaWindow())
        myApp.show()
        return myApp



# example 
# export batch
# from rf_app.publish.scene import app
# reload(app)
# shotName = 's0040'
# data = app.get_export_dict(entity, shotName, ['yeti_groom'])
# app.do_export(entity, itemDatas=data, uiMode=False, publish=True)