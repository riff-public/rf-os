#!/usr/bin/env python
# -- coding: utf-8 --

_title = 'RF Scene Publisher'
_version = 'v.0.0.2'
_des = 'Wip'
uiName = 'ScenePublisherUi'

#Import python modules
import sys
import os
import logging
import getpass
import traceback
from collections import OrderedDict
import os
import json
import pickle

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

core = '%s/core' % os.environ.get('RFSCRIPT')
sys.path.append(core)

# import config
import rf_config as config

from rf_utils import log_utils
from rf_utils.ui import load
reload(load)
from rf_utils.ui import stylesheet

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

# maya & module function
from . import ui
from . import tabs
from rf_utils.sg import sg_process
from rf_utils.context import context_info
ui.sg = sg_process.sg

isNuke = True
try:
    import nuke
    import nukescripts
except ImportError:
    isNuke = False

modulePath = sys.modules[__name__].__file__.replace('\\', '/')
moduleDir  = os.path.dirname(modulePath)
appModuleDir = os.path.dirname(moduleDir)

class FullConfig:
    """ name of tabs module """
    activeTab = 'builder'
    allowedTabs = ['builder']
    tabNameMap = {'builder': 'Render Builder'}

class ViewerConfig:
    activeTab = 'colorscript'
    allowedTabs = ['mastershot', 'colorscript', 'mattepaint', 'media']
    tabNameMap = {}

class ScenePublisher(QtWidgets.QMainWindow):
    def __init__(self, tab_config=FullConfig, parent=None):
        #Setup Window
        super(ScenePublisher, self).__init__(parent)

        # ui read
        uiFile = '%s/ui.py' % moduleDir
        self.ui = ui.ScenePublisherUi()
        self.w = 1150
        self.h = 680
        self.tab_config = tab_config

        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)
        self.resize(self.w, self.h)
        self.setWindowTitle('%s %s %s' % (_title, _version, _des))
        self.setWindowIcon(QtGui.QIcon('{}/icons/app_icon.png'.format(moduleDir)))
        self.setup_ui()
        self.init_signals()
        self.init_functions()

        self.set_default()

    def closeEvent(self, event):
        '''
        Closes all tab widgets before actually close itself triggering each tab closeEvents
        '''
        for i in xrange(self.ui.tab_widget.count()):
            widget = self.ui.tab_widget.widget(i)
            widget.close()

    def setup_ui(self):
        # set tabs
        pass

    def init_signals(self):
        self.ui.sg_nav_widget.completed.connect(self.shot_selected)

    def init_functions(self):
        self.ui.sg_nav_widget.emit_entity()

    def set_default(self):
        # try to find context from current Nuke path
        if isNuke:
            try:
                curr_scene_path = nuke.scriptName()
            except:
                return
            entity = context_info.ContextPathInfo(path=curr_scene_path)

            try:
                # project
                self.ui.project_widget.set_current_item(project=entity.project)
                # ep
                self.ui.sg_nav_widget.episodeWidget.set_current_item(text=entity.episode)
                # seq
                items = [self.ui.sg_nav_widget.sequenceFilter.itemText(a) for a in range(self.ui.sg_nav_widget.sequenceFilter.count())]
                index = items.index(entity.sequence) if entity.sequence in items else 0
                self.ui.sg_nav_widget.sequenceFilter.setCurrentIndex(index)
                # shot
                self.ui.sg_nav_widget.shotWidget.set_current_item(text=entity.name_code)
            except Exception, e:
                pass

    def add_tab_widget(self):
        """ looping for each tabs modules and compare to allowedTab setting,
            add to tab widgets """
        self.ui.tab_widget = QtWidgets.QTabWidget()
        modules = tabs.get_module()
        activeIndex = 0
        self.tabModules = dict()
        self.threadpool = QtCore.QThreadPool()
        self.threadpool.setMaxThreadCount(1)
        tabNames = []

        for tabName in self.tab_config.allowedTabs:
            for index, name in enumerate(modules):
                title = self.tab_config.tabNameMap.get(name) or name.capitalize()
                if name == tabName:
                    func = tabs.get_func(name)
                    reload(func)
                    widget = func.Ui(thread=self.threadpool, parent=self)
                    self.ui.tab_widget.addTab(widget, title)
                    if hasattr(widget, 'refresh'):
                        widget.refresh
                    if name == self.tab_config.activeTab:
                        activeIndex = len(self.tabModules.keys())

                    self.tabModules[name] = widget
        # active tab
        self.ui.tab_widget.setCurrentIndex(activeIndex)
        self.ui.sub_layout.addWidget(self.ui.tab_widget)

        return self.ui.tab_widget

    def shot_selected(self, sg_entity):
        """ shot selected, context changed """
        self.project = sg_entity.get('project').get('name')
        self.episode = sg_entity.get('sg_episode').get('name')
        self.sequence = sg_entity.get('sg_sequence_code')
        self.shot = sg_entity.get('sg_shortcode')
        
        # setup entity 
        context = context_info.Context()
        context.update(
            project=self.project,
            entityType='scene',
            entity=sg_entity.get('code'))
            # entityGrp=self.episode,
            # entityParent=self.sequence,
            # entityCode=self.shot)
        self.entity = context_info.ContextPathInfo(context=context)
        

def show(mode='default'):
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        if mode == 'default':
            maya_win.deleteUI(uiName)
            myApp = ScenePublisher(tab_config=FullConfig, parent=maya_win.getMayaWindow())
        myApp.show()
    elif config.isNuke:
        from rf_nuke import nuke_win 
        # reload(nuke_win)
        logger.info('Run in Nuke\n')
        if mode == 'default':
            nuke_win.deleteUI(uiName)
            myApp = ScenePublisher(tab_config=FullConfig, parent=nuke_win._nuke_main_window())
        myApp.show()
    else:
        logger.info('Run in standalone\n')
        app = QtWidgets.QApplication.instance()
        if not app:
            app = QtWidgets.QApplication(sys.argv)
        if mode == 'default':
            myApp = ScenePublisher(tab_config=ViewerConfig, parent=None)
        myApp.show()
        stylesheet.set_default(app)
        sys.exit(app.exec_())
    return myApp


if __name__ == '__main__':
    show()
