#!/usr/bin/env python
# -- coding: utf-8 --

import os
import sys
import traceback
from collections import OrderedDict

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

module_path = sys.modules[__name__].__file__
module_dir = os.path.dirname(module_path)
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_utils.widget import project_widget
from rf_utils.widget import entity_browse_widget
from rf_utils.widget import sg_entity_widget
from rf_utils import icon
sg = None


class Setting:
    refreshIcon = '%s/icons/refresh.png' % module_dir


class ScenePublisherUi(QtWidgets.QWidget):
    status = QtCore.Signal(str)

    def __init__(self, parent=None):
        super(ScenePublisherUi, self).__init__(parent)
        self.layout = QtWidgets.QVBoxLayout()
        self.sub_layout = QtWidgets.QHBoxLayout()
        self.layout.addLayout(self.sub_layout)
        self.setLayout(self.layout)

        self.setup_widget()
        self.init_signals()
        self.init_functions()

    def setup_widget(self):
        # logo
        self.logo = QtWidgets.QLabel()
        self.logo.setPixmap(QtGui.QPixmap(icon.logo).scaled(64, 64, QtCore.Qt.KeepAspectRatio))
        self.layout.insertWidget(0, self.logo)

        # refresh
        self.refresh_layout = QtWidgets.QHBoxLayout()
        self.refresh_button = QtWidgets.QPushButton()
        self.refresh_button.setMaximumSize(32, 32)
        self.refresh_button.setIcon(QtGui.QIcon(Setting.refreshIcon))
        refresh_spacer = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.refresh_layout.addItem(refresh_spacer)
        self.refresh_layout.addWidget(self.refresh_button)
        self.layout.insertLayout(1, self.refresh_layout)

        # navigation bar
        # self.entityList_container = QtWidgets.QWidget()
        # self.entityListLayout = QtWidgets.QHBoxLayout(self.entityList_container)

        # self.splitter = QtWidgets.QSplitter(self.entityList_container)


        self.nav_layout = QtWidgets.QVBoxLayout()
        self.project_widget = project_widget.SGProjectComboBox(sg=sg)
        self.nav_layout.addWidget(self.project_widget)
        self.sub_layout.addLayout(self.nav_layout)


        """ add sg navigation widget """
        self.sg_nav_widget = entity_browse_widget.SGSceneNavigation(sg=sg, shotFilter=True)
        self.sg_nav_widget.shotExtraFilter = ['sg_shot_type', 'is', 'Shot']

        self.sg_nav_widget.shotWidget.displayAttr = 'sg_shortcode'
        self.sg_nav_widget.shotWidget.set_searchable(True)

        self.nav_layout.addWidget(self.sg_nav_widget)

        # main frame 
        self.splitter = QtWidgets.QSplitter(QtCore.Qt.Horizontal)
        self.entity_list = sg_entity_widget.EntityListWidget()
        self.entity_list.listWidget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.entity_list.listWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.splitter.addWidget(self.entity_list)
        self.splitter.addWidget(QtWidgets.QPushButton())
        self.sub_layout.addWidget(self.splitter)

        # spacer
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.nav_layout.addItem(spacerItem)

        self.nav_layout.setStretch(0, 0)
        self.nav_layout.setStretch(1, 0)
        self.nav_layout.setStretch(2, 2)

        self.sub_layout.setStretch(0, 1)
        self.sub_layout.setStretch(1, 6)

    def init_signals(self):
        self.project_widget.projectChanged.connect(self.project_changed)
        self.sg_nav_widget.shotSignal.connect(self.list_shot)

    def init_functions(self):
        # this will trigger current project to list episode, sequence and shots
        self.project_widget.emit_project_changed()

    def project_changed(self, project):
        self.sg_nav_widget.list_episodes(project)

    def list_shot(self, entities): 
        self.entity_list.clear()
        self.entity_list.add_items(entities)


# splitter example
# from PySide2 import QtWidgets
# from PySide2 import QtCore
# widget = QtWidgets.QWidget()
# layout = QtWidgets.QHBoxLayout()
# splitter = QtWidgets.QSplitter(QtCore.Qt.Horizontal)
# layout.addWidget(splitter)
# splitter.addWidget(QtWidgets.QPushButton())
# splitter.addWidget(QtWidgets.QPushButton())
# widget.setLayout(layout)
# widget.show()


