#!/usr/bin/env python
# -- coding: utf-8 --

# v.0.0.1 polytag switcher
_title = 'Render Browser App'
_version = 'v.0.0.1'
_des = 'Wip'
uiName = 'Render Browser'

#Import python modules
import sys
import os
import logging
import getpass
import traceback
from collections import OrderedDict
import os
import json
import pickle
import subprocess
from functools import partial
import re

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

core = '%s/core' % os.environ.get('RFSCRIPT')
sys.path.append(core)

# import config
import rf_config as config
from rf_utils import log_utils
from rf_utils.ui import load
reload(load)
from rf_utils.ui import stylesheet

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

# maya & module function 
import ui
reload(ui)
from rf_utils.context import context_info
from rf_utils import register_render
reload(register_render)
from rf_utils.sg import sg_process
ui.sg = sg_process.sg
from rf_utils import file_utils
from rf_utils import user_info
from rf_utils import thread_pool

from rf_utils.sg import sg_utils

from rf_app.publish.asset import sg_hook

from rf_utils.pipeline.notification import noti_module_cliq
reload(noti_module_cliq)

class RenderBrowser(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        #Setup Window
        super(RenderBrowser, self).__init__(parent)

        # ui read
        uiFile = '%s/ui.py' % moduleDir
        self.ui = ui.Ui()
        # set size 
        self.w = 1600
        self.h = 635

        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)
        self.resize(self.w, self.h)
        self.setWindowTitle('%s %s %s' % (_title, _version, _des))


        # thread
        thread = None
        self.threadpool = QtCore.QThreadPool() if not thread else thread
        self.threadpool.setMaxThreadCount(1)

        self.color_code_dict = {'Shadow': QtGui.QColor(48, 46, 115),
                        'Beauty': QtGui.QColor(84, 43, 66),
                        'Light': QtGui.QColor(115, 95, 46),
                        'Outline': QtGui.QColor(40, 40, 40),
                        'ID': QtGui.QColor(57, 115, 46)}
        # self.dark_grey = QtGui.QColor(40, 40, 40)
        self.cryptomatte = 'Cryptomatte'

        self.state = ''
        self.threadCount = 0

        self.sg = sg_utils.sg

        self.init_signals()
        self.init_fetch()
        self.init_check()

    def init_signals(self):
        self.ui.sg_nav_widget.completed.connect(self.shot_selected)
        self.ui.layerWidget.itemSelectionChanged.connect(self.fetch_version)
        self.ui.publishButton.clicked.connect(self.publish_queue)
        self.ui.layerWidget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.ui.layerWidget.customContextMenuRequested.connect(partial(self.set_menu, self.ui.layerWidget))
        self.ui.versionWidget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.ui.versionWidget.customContextMenuRequested.connect(partial(self.set_menu, self.ui.versionWidget))
        self.ui.versionWidget.itemDoubleClicked.connect(self.add_queue)
        self.ui.queueWidget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.ui.queueWidget.customContextMenuRequested.connect(partial(self.set_menu, self.ui.queueWidget))
        self.ui.queueWidget.customContextMenuRequested.connect(partial(self.set_menu, self.ui.queueWidget))

        self.ui.lastestCheck.stateChanged.connect(self.show_lastest)
        self.ui.moveRightButton.clicked.connect(self.add_queue)
        self.ui.searchLayer.textChanged.connect(self.fetch_layer)

        self.ui.tabs.currentChanged.connect(self.fetch_tags)
        # self.ui.artSetDirWidget.createTagButton.clicked.connect(self.create_tags)
        self.ui.artSetDirWidget.tagComboBox.currentIndexChanged.connect(self.fetch_tasks)
        # self.ui.artSetDirWidget.taskComboBox.currentIndexChanged.connect(self.fetch_sub_tasks)
        self.ui.artSetDirWidget.createTaskButton.clicked.connect(self.add_task)
        # self.ui.artSetDirWidget.createSubTaskButton.clicked.connect(self.add_subtask)

    def init_fetch(self):
        self.ui.sg_nav_widget.list_episodes(self.ui.project_widget.current_item())

    def init_check(self):
        self.ui.lastestCheck.setChecked(True)

    def shot_selected(self, sg_entity):
        """ shot selected, context changed """
        self.sgEntity = sg_entity
        self.project = sg_entity.get('project').get('name')
        self.episode = sg_entity.get('sg_episode').get('name')
        self.sequence = sg_entity.get('sg_sequence_code')
        self.shot = sg_entity.get('sg_shortcode')
        
        # setup entity 
        context = context_info.Context()
        context.update(
            project=self.project,
            entityType='scene',
            entity=sg_entity.get('code'))

        self.entity = context_info.ContextPathInfo(context=context)

        self.fetch_layer()
        self.fetch_tags()
        self.ui.queueWidget.clear()

    def set_color(self, item, layer):
        # if layer.endswith(self.cryptomatte):
        if self.cryptomatte in layer:
            gradient = QtGui.QLinearGradient()
            gradient.setStart(0, 0)
            gradient.setFinalStop(190, 0)
            gradient.setColorAt(0, QtGui.QColor(77, 38, 38))
            gradient.setColorAt(0.1667, QtGui.QColor(77, 77, 38))
            gradient.setColorAt(0.3334, QtGui.QColor(37, 77, 37))
            gradient.setColorAt(0.6667, QtGui.QColor(37, 77, 77))
            gradient.setColorAt(0.8335, QtGui.QColor(37, 37, 77))
            gradient.setColorAt(1, QtGui.QColor(77, 37, 77))
            # gradient.setColorAt(1, self.dark_grey)

            brush = QtGui.QBrush(gradient)
            item.setBackground(brush)
        else:
            for key, color in self.color_code_dict.iteritems():
                if key in layer:
                    item.setBackground(color)
                    # item.setBackground(brush)
                    break

    def add_subtask(self, parentItem, item): 
        text, okPressed = QtWidgets.QInputDialog.getText(self, 'Add Subtask', 'Subtask Name:', QtWidgets.QLineEdit.Normal, '')
        
        if okPressed: 
            stepSG = 'art'
            # taskName = self.ui.artSetDirWidget.taskComboBox.currentText()
            taskName = parentItem.text(0)
            subtaskName = text
            subtaskList = []
            # tags = self.ui.artSetDirWidget.tagComboBox.currentText()
            task = self.find_taskName(self.sg, self.entity.project, self.entity.name, stepSG, taskName)
            if task:
                meta = task[0]['sg_data']
                # meta = meta.replace("'", '"')
                if meta:
                    meta = eval(meta)
                    if 'output' in meta:
                        subtaskList = meta['output']
                else:
                    meta = OrderedDict()

                subtaskList.append(subtaskName)
                meta['output'] = subtaskList

                subtask = [self.update_subtask_art(self.sg ,task[0], meta)]

            # self.fetch_sub_tasks()
            # parentItem = self.ui.artSetDirWidget.taskWidget.findItems(taskName, QtCore.Qt.MatchExactly, 0)
            # self.add_sub_tasks(parentItem[0])


            # add subtask
            item.setText(0, subtaskName)
            item.setData(QtCore.Qt.UserRole,0 ,task)
            self.ui.artSetDirWidget.taskWidget.removeItemWidget(item, 0)

            item.setText(1, 'Path : ')

            browseWidget = ui.ButtonWidget(self.ui.artSetDirWidget.taskWidget)
            browseWidget.button.setText('Browse')
            browseWidget.setFixedSize(QtCore.QSize(80, 30))
            # browseWidget.button.clicked.connect(partial(self.build, childItem, optionWidget))
            browseWidget.button.clicked.connect(partial(self.browse_file, item))
            self.ui.artSetDirWidget.taskWidget.setItemWidget(item, 2, browseWidget)

            deleteWidget = ui.ButtonWidget(self.ui.artSetDirWidget.taskWidget)
            deleteWidget.button.setText('Delete')
            deleteWidget.setFixedSize(QtCore.QSize(80, 30))
            # deleteWidget.button.clicked.connect(partial(self.build, childItem, optionWidget))
            deleteWidget.button.clicked.connect(partial(self.delete_subtask, parentItem, item))
            self.ui.artSetDirWidget.taskWidget.setItemWidget(item, 3, deleteWidget)


            # add subtask button
            item = QtWidgets.QTreeWidgetItem(parentItem)

            addSubtaskWidget = ui.ButtonWidget(self.ui.artSetDirWidget.taskWidget)
            addSubtaskWidget.button.setText('add subtask')
            addSubtaskWidget.setFixedSize(QtCore.QSize(80, 30))
            # addSubtaskWidget.button.clicked.connect(partial(self.build, childItem, optionWidget))
            addSubtaskWidget.button.clicked.connect(partial(self.add_subtask, parentItem, item))
            self.ui.artSetDirWidget.taskWidget.setItemWidget(item, 0, addSubtaskWidget)

            self.ui.artSetDirWidget.taskWidget.expandItem(parentItem)
            self.ui.artSetDirWidget.taskWidget.collapseItem(parentItem)


            # self.fetch_tasks()
            parentItem = self.ui.artSetDirWidget.taskWidget.findItems(taskName, QtCore.Qt.MatchExactly, 0)
            if parentItem:
                self.ui.artSetDirWidget.taskWidget.expandItem(parentItem[0])


    def delete_subtask(self, parentItem, item):
        dial = QtWidgets.QMessageBox()
        dial.setText('Are you sure you want to delete this subtask?')
        dial.setWindowTitle("Delete subtask")
        dial.setIcon(QtWidgets.QMessageBox.Question)
        yes_button = dial.addButton('Yes', QtWidgets.QMessageBox.ButtonRole.YesRole)
        dial.addButton('No', QtWidgets.QMessageBox.ButtonRole.RejectRole)
        dial.exec_()

        if dial.clickedButton() == yes_button:
            stepSG = 'art'
            # taskName = self.ui.artSetDirWidget.taskComboBox.currentText()
            taskName = parentItem.text(0)
            subtaskName = item.text(0)
            subtaskList = []
            # tags = self.ui.artSetDirWidget.tagComboBox.currentText()
            task = self.find_taskName(self.sg, self.entity.project, self.entity.name, stepSG, taskName)
            if task:
                meta = task[0]['sg_data']
                # meta = meta.replace("'", '"')
                if meta:
                    meta = eval(meta)
                    if 'output' in meta:
                        subtaskList = meta['output']
                else:
                    meta = OrderedDict()

                subtaskList.remove(subtaskName)
                meta['output'] = subtaskList

                subtask = [self.update_subtask_art(self.sg ,task[0], meta)]

            parentItem.removeChild(item)

            # self.fetch_sub_tasks()
            # self.fetch_tasks()
            # parentItem = self.ui.artSetDirWidget.taskWidget.findItems(taskName, QtCore.Qt.MatchExactly, 0)
            # if parentItem:
            #     self.ui.artSetDirWidget.taskWidget.expandItem(parentItem[0])

    def add_task(self): 
        text, okPressed = QtWidgets.QInputDialog.getText(self, 'Add Task', 'Task Name:', QtWidgets.QLineEdit.Normal, '')
        
        if okPressed: 
            stepSG = 'art'
            taskName = text
            tags = self.ui.artSetDirWidget.tagComboBox.currentText()
            task = self.find_taskName(self.sg, self.entity.project, self.entity.name, stepSG, taskName)
            if not task:
                task = [self.create_task_art(self.sg ,self.sgEntity, stepSG, taskName, tags)]
            else:
                if tags != 'none' and tags != 'All':
                    tagNameList = []
                    task = task[0]
                    if task['tags']:
                        for tag in task['tags']:
                            tagName = tag['name']
                            tagNameList.append(tagName)
                    else:
                         tagNameList = ['none']

                    currTag = self.ui.artSetDirWidget.tagComboBox.currentText()
                    tagEntity = self.find_tags(self.sg, currTag)
                    tagData = {'tags': tagEntity}
                    if currTag not in tagNameList:
                        self.sg.update('Task',task['id'],tagData)

            self.fetch_tasks()

    def create_task_art(self,sg , entity, step, taskName, tags):
        stepEntity = sg.find_one('Step', [['code', 'is', step],['entity_type','is','Shot'],['id','is',627]], ['code', 'id'])
        projectEntity = entity.get('project')
        if tags != 'none' and tags != 'All':
            tagEntity = self.find_tags(sg, tags)
            data = {'project': projectEntity,
                            'entity': entity,
                            'step': stepEntity,
                            'content': taskName,
                            'tags': tagEntity}
            tagData = {'tags': tagEntity}
            sg.update('Shot',entity['id'],tagData)
        else:
             data = {'project': projectEntity,
                        'entity': entity,
                        'step': stepEntity,
                        'content': taskName}
        taskEntity = sg.create('Task', data)
        return taskEntity

    def update_subtask_art(self,sg , taskEntity, meta):
        # stepEntity = sg.find_one('Step', [['code', 'is', step],['entity_type','is','Shot'],['id','is',627]], ['code', 'id'])
        # projectEntity = entity.get('project')
        metaData = {'sg_data': str(meta)}
        sg.update('Task',taskEntity['id'], metaData)

        return metaData

    def create_tag_sg(self, sg, tags):
        tagEntity = self.find_tags(sg, tags)
        if not tagEntity:
            tagData = {'name': tags}
            tagEntity = sg.create('Tag', tagData)
            tagEntity = self.find_tags(sg, tags)
        return tagEntity

    def fetch_layer(self):
        searchKey = self.ui.searchLayer.text()
        # self.ui.queueWidget.clear()
        self.ui.layerWidget.clear()
        self.entity.context.update(step='render')
        self.outputPath = self.entity.path.scheme(key='outputStepPath').abs_path()
        if os.path.exists(self.outputPath):
            layers = os.listdir(self.outputPath)
            for layer in layers:
                if searchKey:
                    if not searchKey.lower() in layer.lower():
                        continue
                layerPath = os.path.join(self.outputPath, layer)
                if os.path.isdir(layerPath):
                    item = QtWidgets.QListWidgetItem( layer, self.ui.layerWidget)
                    item.setData(QtCore.Qt.UserRole,layerPath)

                    self.set_color(item, layer)
        else:
            self.ui.layerWidget.addItem('Not Found')

    def fetch_version(self):
        lastSel = self.ui.versionWidget.currentRow()
        self.ui.versionWidget.clear()
        sels = self.ui.layerWidget.selectedItems()
        if sels:
            for sel in sels:
                layer = sel.text()
                versionPath = os.path.join(self.outputPath, layer)
                if os.path.exists(versionPath):
                    if os.path.isdir(versionPath):
                        versions = os.listdir(versionPath)
                        if self.ui.lastestCheck.isChecked():
                            versions = [versions[-1]]
                        for version in versions:
                            if re.search('[vV]{1}[0-9]{3}', version):
                                reg = register_render.Register(ContextPathInfo=self.entity)
                                render_data = reg.get_data()

                                comment = 'This version is fresh.'

                                renderVer = render_data.get_render_element_version(layer, version)
                                if renderVer:
                                    comment = renderVer.histories[-1]['comment']

                                versionName = layer + ' - ' + version
                                versionFilePath = os.path.join(versionPath, version)
                                item = QtWidgets.QListWidgetItem( versionName, self.ui.versionWidget)
                                toolTipText = comment
                                item.setData(QtCore.Qt.UserRole,versionFilePath)
                                item.setToolTip(toolTipText)

                                self.set_color(item, layer)
                                # if comment:
                                #     item.setBackground( QtGui.QColor('#ffb140') )
                                # else:
                                #     item.setBackground( QtGui.QColor('#95ff79') )
                                # item.setForeground (QtGui.QColor("black"))

        self.ui.versionWidget.setCurrentRow(lastSel)

    # def create_tags(self):
    #     text, okPressed = QtWidgets.QInputDialog.getText(self, 'Create Tags', 'Tags Name:', QtWidgets.QLineEdit.Normal, '')
        
    #     if okPressed: 
    #         self.ui.artSetDirWidget.tagComboBox.addItem(text)
    #         self.ui.artSetDirWidget.tagComboBox.setCurrentIndex( self.ui.artSetDirWidget.tagComboBox.count() - 1 )
    #         self.create_tag_sg(self.sg, text)
    #         # stepSG = 'art'
    #         # taskName = text
    #         # task = self.find_taskName(self.sg, self.entity.project, self.entity.name, stepSG, taskName)
    #         # if not task:
    #         #     task = [self.create_task_art(self.sg ,self.sgEntity, stepSG, taskName)]

    #     # self.fetch_tasks()

    def fetch_tags(self):
        self.currTab = self.ui.tabs.currentIndex()
        if self.currTab == 2:

            # self.ui.artSetDirWidget.taskWidget.clear()
            # self.ui.artSetDirWidget.taskComboBox.clear()
            self.ui.artSetDirWidget.tagComboBox.clear()

            project = self.entity.project
            # entityType = 'Shot'
            shotname = self.entity.name
            # step = self.get_step()
            step = 'art'
            tagNameList = []
            taskEntity = self.find_task(self.sg, project, shotname, step)
            tagName = 'All'
            if self.ui.artSetDirWidget.tagComboBox.findText(tagName) == -1:
                self.ui.artSetDirWidget.tagComboBox.addItem(tagName)
            for task in taskEntity:
                if task['tags']:
                    for tag in task['tags']:
                        tagName = tag['name']
                        if self.ui.artSetDirWidget.tagComboBox.findText(tagName) == -1:
                            self.ui.artSetDirWidget.tagComboBox.addItem(tagName)

            shotTags = self.find_shot_tags(self.sg, self.entity.project, self.entity.episode, self.entity.sequence, self.entity.name)
            tags = shotTags[0]['tags']
            for tg in tags:
                tg = tg['name']
                if self.ui.artSetDirWidget.tagComboBox.findText(tg) == -1:
                    self.ui.artSetDirWidget.tagComboBox.addItem(tg)

            tagName = 'none'
            if self.ui.artSetDirWidget.tagComboBox.findText(tagName) == -1:
                self.ui.artSetDirWidget.tagComboBox.addItem(tagName)

            self.ui.step_widget.setCurrentIndex(3)

        self.fetch_tasks()

    def fetch_tasks(self):
        self.currTab = self.ui.tabs.currentIndex()
        if self.currTab == 2:

            # self.ui.artSetDirWidget.taskWidget.clear()
            # self.ui.artSetDirWidget.taskComboBox.clear()
            self.ui.artSetDirWidget.taskWidget.clear()

            project = self.entity.project
            # entityType = 'Shot'
            shotname = self.entity.name
            # step = self.get_step()
            step = 'art'
            taskEntity = self.find_task(self.sg, project, shotname, step)

            for task in taskEntity:
                tagNameList = ['All']
                taskName = task['content']
                if task['tags']:
                    for tag in task['tags']:
                        tagName = tag['name']
                        tagNameList.append(tagName)
                else:
                     tagNameList.append('none')
                currTag = self.ui.artSetDirWidget.tagComboBox.currentText()
                if currTag in tagNameList:
                    # self.ui.artSetDirWidget.taskComboBox.addItem(taskName)
                    # self.ui.artSetDirWidget.taskComboBox.setCurrentIndex( self.ui.artSetDirWidget.taskComboBox.count() - 1 )

                    # item = QtWidgets.QListWidgetItem( taskName, self.ui.artSetDirWidget.taskWidget)
                    # item.setData(QtCore.Qt.UserRole,task)


                    item = QtWidgets.QTreeWidgetItem(self.ui.artSetDirWidget.taskWidget)
                    item.setText(0, taskName)
                    item.setData(QtCore.Qt.UserRole,0 ,task)

                    item.setText(1, 'Path : ')

                    browseWidget = ui.ButtonWidget(self.ui.artSetDirWidget.taskWidget)
                    browseWidget.button.setText('Browse')
                    browseWidget.setFixedSize(QtCore.QSize(80, 30))
                    browseWidget.button.clicked.connect(partial(self.browse_file, item))
                    self.ui.artSetDirWidget.taskWidget.setItemWidget(item, 2, browseWidget)

                    item.setText(3, 'Preview Path : ')

                    browsePreviewWidget = ui.ButtonWidget(self.ui.artSetDirWidget.taskWidget)
                    browsePreviewWidget.button.setText('Browse Preview')
                    browsePreviewWidget.setFixedSize(QtCore.QSize(100, 30))
                    browsePreviewWidget.button.clicked.connect(partial(self.browse_file_preview, item))
                    self.ui.artSetDirWidget.taskWidget.setItemWidget(item, 4, browsePreviewWidget)

                    tgLabel = 'Tags : '
                    count = 0
                    for tg in tagNameList:
                        if tg != 'All' and tg != 'none':
                            if count != 0:
                                tgLabel += ', {}'.format(tg)
                            else:
                                tgLabel += '{}'.format(tg)
                            count += 1
                    item.setText(5, tgLabel)

                    addTagWidget = ui.ButtonWidget(self.ui.artSetDirWidget.taskWidget)
                    addTagWidget.button.setText('Add Tag')
                    addTagWidget.setFixedSize(QtCore.QSize(100, 30))
                    addTagWidget.button.clicked.connect(partial(self.add_tag, item))
                    self.ui.artSetDirWidget.taskWidget.setItemWidget(item, 6, addTagWidget)

                    self.add_sub_tasks(item)

            # self.fetch_sub_tasks()

    # def fetch_sub_tasks(self):
    #     self.currTab = self.ui.tabs.currentIndex()
    #     if self.currTab == 2:

    #         self.ui.artSetDirWidget.taskWidget.clear()
    #         stepSG = 'art'
    #         taskName = self.ui.artSetDirWidget.taskComboBox.currentText()
    #         # subtaskName = text
    #         subtaskList = []
    #         # tags = self.ui.artSetDirWidget.tagComboBox.currentText()
    #         task = self.find_taskName(self.sg, self.entity.project, self.entity.name, stepSG, taskName)
    #         if task:
    #             meta = task[0]['sg_data']
    #             # meta = meta.replace("'", '"')
    #             if meta:
    #                 meta = eval(meta)
    #                 if 'output' in meta:
    #                     subtaskList = meta['output']

    #                 for subTaskName in subtaskList:
    #                     # self.ui.artSetDirWidget.taskComboBox.addItem(taskName)
    #                     # self.ui.artSetDirWidget.taskComboBox.setCurrentIndex( self.ui.artSetDirWidget.taskComboBox.count() - 1 )

    #                     item = QtWidgets.QTreeWidgetItem(self.ui.artSetDirWidget.taskWidget)
    #                     item.setText(0, subTaskName)
    #                     item.setData(QtCore.Qt.UserRole,0 ,task)

    def add_tag(self, item):
        text, okPressed = QtWidgets.QInputDialog.getText(self, 'Create Tags', 'Tags Name:', QtWidgets.QLineEdit.Normal, '')
        
        if okPressed: 
            task = item.data(QtCore.Qt.UserRole,0)
            self.ui.artSetDirWidget.tagComboBox.addItem(text)
            # self.ui.artSetDirWidget.tagComboBox.setCurrentIndex( self.ui.artSetDirWidget.tagComboBox.count() - 1 )
            tagEntity = self.create_tag_sg(self.sg, text)
            tagList = task['tags']
            tagList += (tagEntity)

            task['tags'] = tagList
            item.setData(QtCore.Qt.UserRole,0 ,task)
            tagLabel = item.text(5)
            tagLabel += ', {}'.format(text)
            item.setText(5, tagLabel)

            self.update_tag(task, tagList)


            # currTag = self.ui.artSetDirWidget.tagComboBox.currentText()
            # tagEntity = self.find_tags(self.sg, text)

            # stepSG = 'art'
            # taskName = text
            # task = self.find_taskName(self.sg, self.entity.project, self.entity.name, stepSG, taskName)
            # if not task:
            #     task = [self.create_task_art(self.sg ,self.sgEntity, stepSG, taskName)]

    def update_tag(self, task, tagList):
        tagData = {'tags': tagList}
        # tagShotData = {'tags': tagEntity}
        self.sg.update('Task',task['id'],tagData)

        self.sg.update('Shot',self.sgEntity['id'],tagData)

    def browse_file(self, item):
        file_path = str(QtWidgets.QFileDialog.getOpenFileName(self, "Select File",self.state)[0])
        self.state = file_path
        item.setText(1, "Path : {}".format(file_path))
        item.setData(QtCore.Qt.UserRole,1 ,file_path)
        # root = self.ui.artSetDirWidget.taskWidget.invisibleRootItem()
        # child_count = root.childCount()
        # for i in range(child_count):
        #     item = root.child(i)
        #     print item.text(0)
        #     item_child_count = item.childCount()
        #     for i in range(item_child_count):
        #         childItem = item.child(i)
        #         print childItem.text(0)

    def browse_file_preview(self, item):
        file_path = str(QtWidgets.QFileDialog.getOpenFileName(self, "Select File",self.state)[0])
        self.state = file_path
        item.setText(3, "Preview Path : {}".format(file_path))
        item.setData(QtCore.Qt.UserRole,3 ,file_path)


    def add_sub_tasks(self, parentItem):
        self.currTab = self.ui.tabs.currentIndex()
        if self.currTab == 2:

            # self.ui.artSetDirWidget.taskWidget.clear()
            stepSG = 'art'
            # taskName = self.ui.artSetDirWidget.taskComboBox.currentText()
            taskName = parentItem.text(0)
            # subtaskName = text
            subtaskList = []
            # tags = self.ui.artSetDirWidget.tagComboBox.currentText()
            task = self.find_taskName(self.sg, self.entity.project, self.entity.name, stepSG, taskName)
            if task:
                meta = task[0]['sg_data']
                # meta = meta.replace("'", '"')
                if meta:
                    meta = eval(meta)
                    if 'output' in meta:
                        subtaskList = meta['output']

                    for subTaskName in subtaskList:
                        # self.ui.artSetDirWidget.taskComboBox.addItem(taskName)
                        # self.ui.artSetDirWidget.taskComboBox.setCurrentIndex( self.ui.artSetDirWidget.taskComboBox.count() - 1 )

                        item = QtWidgets.QTreeWidgetItem(parentItem)
                        item.setText(0, subTaskName)
                        item.setData(QtCore.Qt.UserRole,0 ,task)

                        item.setText(1, 'Path : ')

                        browseWidget = ui.ButtonWidget(self.ui.artSetDirWidget.taskWidget)
                        browseWidget.button.setText('Browse')
                        browseWidget.setFixedSize(QtCore.QSize(80, 30))
                        # browseWidget.button.clicked.connect(partial(self.build, childItem, optionWidget))
                        browseWidget.button.clicked.connect(partial(self.browse_file, item))
                        self.ui.artSetDirWidget.taskWidget.setItemWidget(item, 2, browseWidget)

                        deleteWidget = ui.ButtonWidget(self.ui.artSetDirWidget.taskWidget)
                        deleteWidget.button.setText('Delete')
                        deleteWidget.setFixedSize(QtCore.QSize(80, 30))
                        # deleteWidget.button.clicked.connect(partial(self.build, childItem, optionWidget))
                        deleteWidget.button.clicked.connect(partial(self.delete_subtask, parentItem, item))
                        self.ui.artSetDirWidget.taskWidget.setItemWidget(item, 3, deleteWidget)



                item = QtWidgets.QTreeWidgetItem(parentItem)
                # item.setText(0, ' + add subtask')
                # item.setData(QtCore.Qt.UserRole,0 ,task)
                # item.setBackground(0, QtGui.QColor(96, 96, 96))
                # item.setForeground(0, QtGui.QColor(128, 128, 128))

                addSubtaskWidget = ui.ButtonWidget(self.ui.artSetDirWidget.taskWidget)
                addSubtaskWidget.button.setText('add subtask')
                addSubtaskWidget.setFixedSize(QtCore.QSize(80, 30))
                # addSubtaskWidget.button.clicked.connect(partial(self.build, childItem, optionWidget))
                addSubtaskWidget.button.clicked.connect(partial(self.add_subtask, parentItem, item))
                self.ui.artSetDirWidget.taskWidget.setItemWidget(item, 0, addSubtaskWidget)

                self.ui.artSetDirWidget.taskWidget.expandItem(parentItem)
                self.ui.artSetDirWidget.taskWidget.collapseItem(parentItem)

                # test = QtWidgets.QWidget()
                # createTagButtonnn = QtWidgets.QPushButton(test)
                # createTagButtonnn.setFixedSize(QtCore.QSize(15, 15))
                # createTagButtonnn.setObjectName("createTagButtonnn")
                # createTagButtonnn.setText("+")

                # self.ui.artSetDirWidget.taskWidget.setColumnCount(1)
                # self.ui.artSetDirWidget.taskWidget.setItemWidget(item, 1,QtWidgets.QLabel('sss'))


    def show_lastest(self):
        self.fetch_version()

    def add_queue(self):
        items = self.ui.versionWidget.selectedItems()
        if items:
            for item in items:
                name = item.text()
                data = item.data(QtCore.Qt.UserRole)
                toolTip = item.toolTip()
                if not self.ui.queueWidget.findItems(name, QtCore.Qt.MatchExactly):
                    item = QtWidgets.QListWidgetItem(name, self.ui.queueWidget)
                    item.setData(QtCore.Qt.UserRole,data)

                    self.set_color(item, name)

                    # if toolTip:
                    #     item.setBackground( QtGui.QColor('#ffb140') )
                    # else:
                    #     item.setBackground( QtGui.QColor('#95ff79') )
                    # item.setForeground (QtGui.QColor("black"))

    def set_menu(self, widget, position):
        menu = QtWidgets.QMenu()
        open_action = menu.addAction("Open In Explorer")
        action = menu.exec_(widget.mapToGlobal(position))
        if action:
            items = widget.selectedItems()
            for item in items:
                path = item.data(QtCore.Qt.UserRole)
                self.open_file(path)

    def open_file(self, path):
        subprocess.Popen(r'explorer %s' % path.replace('/', '\\'))

    def get_description(self):
        if self.currTab == 0:
            return self.ui.descriptionText.toPlainText()
        elif self.currTab == 1 or self.currTab == 2:
            return self.ui.setDirWidget.descriptionText.toPlainText()

    # def get_render_data_path(self):
    #     self.dataPath = self.entity.path.scheme(key='renderDataPath').abs_path()

    def get_step(self):
        return self.ui.step_widget.currentText()

    def register_from_path(self, reg, path, userName, desc, step):
        elemPath , version = os.path.split(path)
        element = os.path.split(elemPath)[-1]
        fileName = os.listdir(path)[0]
        frame = fileName.split('.')[-2]
        if frame.isdigit():
            fileName = fileName.replace(frame, r'%04d')
        path = os.path.join(path, fileName)
        path = path.replace('\\', '/')

        reg.update_render_version(element=element, 
                version=version, 
                user=userName, 
                step=step, 
                path=path, 
                comment=desc)
        # reg.register()

    def check_error_directory(self, path):
        if os.path.isdir(path):
            files = os.listdir(path)
            oldFrame = 0
            for f in files:
                pattern = re.findall('[.]{1}[0-9]{4}', f)
                if pattern:
                    frame = pattern[0][1:]
                    if oldFrame == 0:
                        oldFrame = int(frame) - 1
                    if int(frame) == int(oldFrame) + 1:
                        oldFrame = frame
                    else:
                        return QtWidgets.QMessageBox.warning(self, "Message", str('Some skip frame "%s" in this folder!\n Do you still running?'%(int(oldFrame)+1)), QtWidgets.QMessageBox.Ok, QtWidgets.QMessageBox.Cancel)
                else:
                    return QtWidgets.QMessageBox.warning(self, "Message", str('Invalid name "%s" in this folder!\n Do you still running?'%f), QtWidgets.QMessageBox.Ok, QtWidgets.QMessageBox.Cancel)
                fn = os.path.join(path, f)
                size = os.path.getsize(fn)
                if size == long('0L'):
                    return QtWidgets.QMessageBox.warning(self, "Message", str('Empty file "%s" in this folder!\n Do you still running?'% f), QtWidgets.QMessageBox.Ok, QtWidgets.QMessageBox.Cancel)

    def publish_queue(self):
        try:
            self.ui.publishButton.setEnabled(False)
            self.ui.status_label.setText('Status : registering . . .')
            self.ui.status_label.setStyleSheet('color: yellow')

            QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)

            self.currTab = self.ui.tabs.currentIndex()
            reg = register_render.Register(ContextPathInfo=self.entity)
            render_data = reg.get_data()

            user = user_info.User()
            userName = user.name()
            desc = self.get_description()
            step = self.get_step()

            if self.currTab == 0:
                for index in xrange(self.ui.queueWidget.count()):
                    item = self.ui.queueWidget.item(index)
                    # queueName = item.text()
                    path = item.data(QtCore.Qt.UserRole)
                    if not self.ui.skip_chkBox.isChecked():
                        result = self.check_error_directory(path)
                        if result == QtWidgets.QMessageBox.Cancel:
                            QtWidgets.QApplication.restoreOverrideCursor()
                            self.ui.status_label.setText('Status : stopped.')
                            self.ui.status_label.setStyleSheet('color: red')
                            return
                    self.register_from_path(reg, path, userName, desc, step)

                    layer = item.text().split(' - ')[0]

                    # create task and update status
                    taskName = layer
                    stepSG = 'render'
                    status = 'apr'
                    task = self.find_taskName(self.sg, self.entity.project, self.entity.name, stepSG, taskName)
                    if not task:
                        task = [sg_process.create_task(self.sgEntity, stepSG, taskName)]
                    sg_process.set_task_status(task[0].get('id'), status)

                reg.register()
                self.ui.descriptionText.clear()

                QtWidgets.QApplication.restoreOverrideCursor()
                self.show_message_box("Published Successfully!.")
                self.fetch_layer()
                self.ui.queueWidget.clear()

                self.ui.status_label.setText('Status : register Complete.')
                self.ui.status_label.setStyleSheet('color: lightGreen')
                self.ui.publishButton.setEnabled(True)

            elif self.currTab == 1:
                allFiles = self.ui.setDirWidget.dropFile_widget.get_all_items()
                self.regItems = []
                for item in allFiles:
                    if not self.ui.skip_chkBox.isChecked():
                        result = self.check_error_directory(item)
                        if result == QtWidgets.QMessageBox.Cancel:
                            QtWidgets.QApplication.restoreOverrideCursor()
                            self.ui.status_label.setText('Status : stopped.')
                            self.ui.status_label.setStyleSheet('color: red')
                            return

                # start the fetch worker
                worker = thread_pool.Worker(self.thread_register, allFiles, reg, userName, desc, step)
                worker.kwargs['loop_callback'] = worker.signals.loopResult
                worker.signals.finished.connect(self.thread_register_finished)
                worker.signals.loopResult.connect(self.thread_register_progress)
                self.threadpool.start(worker)

                self.ui.setDirWidget.descriptionText.clear()

                # QtWidgets.QApplication.restoreOverrideCursor()
                # self.show_message_box("Published Successfully!.")
                # self.fetch_layer()

                # self.ui.status_label.setText('Status : register Complete.')
                # self.ui.status_label.setStyleSheet('color: lightGreen')

            elif self.currTab == 2:
                # taskSel = self.ui.artSetDirWidget.taskWidget.currentItem()
                taskSel = self.ui.artSetDirWidget.taskComboBox.currentText()

                root = self.ui.artSetDirWidget.taskWidget.invisibleRootItem()
                child_count = root.childCount()

                
                for i in range(child_count):
                    item = root.child(i)
                    taskName = item.text(0)
                    taskPath = item.data(QtCore.Qt.UserRole,1)
                    taskPathPreview = item.data(QtCore.Qt.UserRole,3)
                    if taskPathPreview:
                        self.threadCount +=1

                for i in range(child_count):
                    allFiles = []
                    foldName = []
                    self.regItems = []
                    item = root.child(i)
                    taskName = item.text(0)
                    taskPath = item.data(QtCore.Qt.UserRole,1)
                    taskPathPreview = item.data(QtCore.Qt.UserRole,3)
                    item_child_count = item.childCount()
                    if taskPath:
                        allFiles.append(taskPath)
                        foldName.append(taskName)
                    if taskPathPreview:
                        for i in range(item_child_count):
                            childItem = item.child(i)
                            subtaskName = childItem.text(0)
                            subtaskPath = childItem.data(QtCore.Qt.UserRole,1)
                            if subtaskPath:
                                allFiles.append(subtaskPath)
                                foldName.append(subtaskName)

                        # start the fetch worker
                        if allFiles:
                            worker = thread_pool.Worker(self.thread_register, allFiles, reg, userName, desc, step, foldName=foldName, taskNameArt=taskName, displayArt=taskPathPreview)
                            worker.kwargs['loop_callback'] = worker.signals.loopResult
                            worker.signals.finished.connect(self.thread_register_finished)
                            worker.signals.loopResult.connect(self.thread_register_progress)
                            self.threadpool.start(worker)

                # QtWidgets.QApplication.restoreOverrideCursor()
                # self.show_message_box("Published Successfully!.")
                # self.fetch_layer()

                # self.ui.status_label.setText('Status : register Complete.')
                # self.ui.status_label.setStyleSheet('color: lightGreen')


            # Send notification part
            project = self.entity.project
            # entityType = 'Shot'
            shotname = self.entity.name
            step = self.get_step()
            # if step == 'Mattepaint':
            #     step = 'Art'
            taskEntity = self.find_task(self.sg, project, shotname, step)
            if taskEntity:
                taskEntity = taskEntity[0]
                taskFromId = sg_process.get_task_from_id(taskEntity['id'])
                result = [taskFromId]
                taskEntity['sg_status_list'] = 'apr'

                taskEntity['content'] = 'render'
                taskEntity['step']['name'] = 'Render'
                if step == 'Art':
                    # taskEntity['content'] = self.ui.artSetDirWidget.taskWidget.currentItem().text()
                    taskEntity['content'] = taskName
                    taskEntity['step']['name'] = step
                # taskEntity = {'project': {'type': 'Project', 'id': 134, 'name': 'projectName'}, 'sg_name': None, 'sg_resolution': None, 'entity': {'type': 'Shot', 'id': 8896, 'name': 'pr_ep01_C_GameEndCortex_Shot001'}, 'content': 'render', 'step': {'type': 'Step', 'id': None, 'name': 'Render'}, 'sg_status_list': 'apr', 'type': 'Task', 'id': None}
                userEntity = user.sg_user()
                noti_module_cliq.send_scene_notification(result, taskEntity, self.entity, userEntity, description=desc)
                # Send notification End

        except Exception as e:
            print e
            QtWidgets.QMessageBox.warning(self, "Message", str('%s\nERROR! Please contact admin'%e))
            QtWidgets.QApplication.restoreOverrideCursor()
            self.ui.status_label.setText('Status : Error.')
            self.ui.status_label.setStyleSheet('color: red')
            self.ui.publishButton.setEnabled(True)


    def show_message_box(self, msg):
        QtWidgets.QMessageBox.information(self, "Message", str(msg))

    def thread_register_progress(self, message): 
        self.ui.status_label.setText(str(message))

    def thread_register_finished(self):
        self.threadCount -= 1
        if self.threadCount <= 0:
            QtWidgets.QApplication.restoreOverrideCursor()
            self.show_message_box("Published Successfully!.")
            self.fetch_layer()
            self.ui.queueWidget.clear()
            self.ui.setDirWidget.dropFile_widget.set_style(ui.Config.filePreview)
            self.ui.setDirWidget.dropFile_widget.display.clear()
            self.ui.artSetDirWidget.dropFile_widget.set_style(ui.Config.filePreview)
            self.ui.artSetDirWidget.dropFile_widget.display.clear()

            self.ui.status_label.setText('Status : register Complete.')
            self.ui.status_label.setStyleSheet('color: lightGreen')
            self.ui.publishButton.setEnabled(True)

            self.ui.artSetDirWidget.descriptionText.clear()
            self.fetch_tasks()
            self.threadCount = 0

    def thread_register(self, allFiles, reg, userName, desc, step, foldName=None, taskNameArt=None,displayArt=None ,**kwargs):
        callback = kwargs['loop_callback']
        displayThumbnail = None
        for i,item in enumerate(allFiles):
            if os.path.isdir(item):
                files = os.listdir(item)
                layerList = []
                newVerPath = ''
                lastLayerPath = ''
                # lenFile = len(files)
                # count = 0
                for f in files:
                    pattern = re.findall('[_.]{1}[0-9]{4}', f)
                    if pattern:
                        pattern = pattern[0]
                        layerName = f.split(pattern)[0]
                        if step == 'Art':
                            stepSG = 'Art'
                            # taskName = self.ui.artSetDirWidget.taskWidget.currentItem().text()
                            # taskName = self.ui.artSetDirWidget.taskComboBox.currentText()
                            layerName = 'Art_' + taskNameArt
                            if foldName:
                                layerName = 'Art_' + foldName[i]
                        srcPath = os.path.join(item, f)
                        layerPath = "{}/{}".format(self.outputPath, layerName)

                        # run version
                        if layerPath != lastLayerPath:
                            newVersion = 'v001'
                            if os.path.exists(layerPath):
                                versions = os.listdir(layerPath)
                                verList = []
                                for version in versions:
                                    versionPath =  "{}/{}".format(layerPath, version)
                                    if os.path.isdir(versionPath):
                                        ver = re.findall('[vV]{1}[0-9]{3}', version)
                                        if ver:
                                            version = ver[0].lower().split('v')[-1]
                                            verList.append(version)
                                if verList:
                                    newVersion = 'v{:03d}'.format(int(verList[-1]) + 1)
                        # run version END

                        newVerPath = "{}/{}".format(layerPath, newVersion)
                        if '_' in pattern:
                            # newPattern = pattern
                            newPattern = pattern.replace('_','.')
                            f = f.replace(pattern, newPattern)
                        dstPath = "{}/{}".format(newVerPath, f)
                        if not newVerPath in self.regItems:
                            self.regItems.append(newVerPath)
                        lastLayerPath = layerPath

                        # print 'Copy {} to {}'.format(srcPath, dstPath)
                        callback.emit('Copy {}'.format(f))
                        file_utils.copy(srcPath, dstPath)

                        displayThumbnail = [dstPath]

                        # count +=1
                        # percent = ((count/lenFile)*100.00)
                        # self.ui.status_label.setText('Status : registering {} ( {}% ) . . .'.format(layerName, percent))

                    else:
                        return QtWidgets.QMessageBox.warning(self, "Message", str('%s\nERROR! The naming is illegal must have . or _ before frame number. Ex: xxx.1001.exr'))

            # elif item.split('.')[-1] == 'mov':
            else:
                srcPath = item
                fn = srcPath.split('/')[-1]
                # layerName = fn.split('.mov')[0]
                layerName = os.path.splitext(fn)[0]
                if step == 'Art':
                    stepSG = 'Art'
                    lastLayerPath = ''
                    # taskName = self.ui.artSetDirWidget.taskWidget.currentItem().text()
                    pattern = re.findall('[_.]{1}[0-9]{4}', fn)
                    if pattern:
                        dirPath = os.path.dirname(srcPath)
                        files = os.listdir(dirPath)
                        for f in files:
                            pattern = re.findall('[_.]{1}[0-9]{4}', f)
                            if pattern:

                                pattern = pattern[0]
                                stepSG = 'Art'
                                # taskName = self.ui.artSetDirWidget.taskWidget.currentItem().text()
                                # taskName = self.ui.artSetDirWidget.taskComboBox.currentText()
                                layerName = 'Art_' + taskNameArt
                                if foldName:
                                    layerName = 'Art_' + foldName[i]
                                srcPath = os.path.join(dirPath, f)
                                layerPath = "{}/{}".format(self.outputPath, layerName)

                                # run version
                                if layerPath != lastLayerPath:
                                    newVersion = 'v001'
                                    if os.path.exists(layerPath):
                                        versions = os.listdir(layerPath)
                                        verList = []
                                        for version in versions:
                                            versionPath =  "{}/{}".format(layerPath, version)
                                            if os.path.isdir(versionPath):
                                                ver = re.findall('[vV]{1}[0-9]{3}', version)
                                                if ver:
                                                    version = ver[0].lower().split('v')[-1]
                                                    verList.append(version)
                                        if verList:
                                            newVersion = 'v{:03d}'.format(int(verList[-1]) + 1)
                                # run version END

                                newVerPath = "{}/{}".format(layerPath, newVersion)
                                if '_' in pattern:
                                    # newPattern = pattern
                                    newPattern = pattern.replace('_','.')
                                    f = f.replace(pattern, newPattern)
                                dstPath = "{}/{}".format(newVerPath, f)
                                if not newVerPath in self.regItems:
                                    self.regItems.append(newVerPath)
                                lastLayerPath = layerPath

                                # print 'Copy {} to {}'.format(srcPath, dstPath)
                                callback.emit('Copy {}'.format(f))
                                file_utils.copy(srcPath, dstPath)

                                displayThumbnail = [dstPath]

                    else:
                        layerPath = "{}/{}".format(self.outputPath, layerName)

                        # run version
                        newVersion = 'v001'
                        if os.path.exists(layerPath):
                            versions = os.listdir(layerPath)
                            verList = []
                            for version in versions:
                                versionPath =  "{}/{}".format(layerPath, version)
                                if os.path.isdir(versionPath):
                                    ver = re.findall('[vV]{1}[0-9]{3}', version)
                                    if ver:
                                        version = ver[0].lower().split('v')[-1]
                                        verList.append(version)
                            if verList:
                                newVersion = 'v{:03d}'.format(int(verList[-1]) + 1)
                        # run version END

                        newVerPath = "{}/{}".format(layerPath, newVersion)
                        dstPath = "{}/{}".format(newVerPath, fn)
                        if not newVerPath in self.regItems:
                            self.regItems.append(newVerPath)
                        # print 'Copy {} to {}'.format(srcPath, dstPath)
                        file_utils.copy(srcPath, dstPath)

                        displayThumbnail = [dstPath]


                else:
                    layerPath = "{}/{}".format(self.outputPath, layerName)

                    # run version
                    newVersion = 'v001'
                    if os.path.exists(layerPath):
                        versions = os.listdir(layerPath)
                        verList = []
                        for version in versions:
                            versionPath =  "{}/{}".format(layerPath, version)
                            if os.path.isdir(versionPath):
                                ver = re.findall('[vV]{1}[0-9]{3}', version)
                                if ver:
                                    version = ver[0].lower().split('v')[-1]
                                    verList.append(version)
                        if verList:
                            newVersion = 'v{:03d}'.format(int(verList[-1]) + 1)
                    # run version END

                    newVerPath = "{}/{}".format(layerPath, newVersion)
                    dstPath = "{}/{}".format(newVerPath, fn)
                    if not newVerPath in self.regItems:
                        self.regItems.append(newVerPath)
                    # print 'Copy {} to {}'.format(srcPath, dstPath)
                    file_utils.copy(srcPath, dstPath)

                    displayThumbnail = [dstPath]


            # create task and update status
            taskName = layerName
            stepSG = 'render'
            status = 'apr'
            if step == 'Art':
                stepSG = 'Art'
                # taskName = self.ui.artSetDirWidget.taskWidget.currentItem().text()
                # taskName = self.ui.artSetDirWidget.taskComboBox.currentText()
                taskName = taskNameArt

            # print 'TASK -----------------'
            # print taskName

            task = self.find_taskName(self.sg, self.entity.project, self.entity.name, stepSG, taskName)
            if not task:
                task = [sg_process.create_task(self.sgEntity, stepSG, taskName)]

            if step == 'Art':
                user = user_info.User()
                userEntity = user.sg_user()
                desc = self.get_description()
                displayThumbnail = [displayArt]

                self.entity.context.update(process=taskName)
                self.entity.context.update(publishVersion=newVersion)
                if displayThumbnail:
                    sg_hook.publish_version(self.entity, task[0], status, displayThumbnail, userEntity, desc)
                else:
                    return QtWidgets.QMessageBox.warning(self, "Message", str('%s\nERROR! Do not have Preview to upload.'))

            sg_process.set_task_status(task[0].get('id'), status)

        if self.regItems:
            for item in self.regItems:
                self.register_from_path(reg, item, userName, desc, step)
            reg.register()

    def find_task(self, sg, project, shotname, step):
        filters = [['project.Project.name', 'is', project],
                        ['entity.Shot.code', 'is', shotname],
                        ['step.Step.short_name', 'is', step],
                        ['step.Step.entity_type', 'is', 'Shot']]
        fields = ['content', 'id', 'entity', 'project', 'step', 'sg_name', 'sg_resolution', 'sg_status_list', 'tags']
        result = sg.find('Task', filters, fields)
        return result

    def find_taskName(self, sg, project, shotname, step, taskName):
        filters = [['project.Project.name', 'is', project],
                        ['entity.Shot.code', 'is', shotname],
                        ['step.Step.short_name', 'is', step],
                        ['content', 'is', taskName]]
        fields = ['content', 'id', 'entity', 'project', 'step', 'sg_name', 'sg_resolution', 'sg_status_list', 'sg_data','tags']
        result = sg.find('Task', filters, fields)
        return result

    def find_tags(self, sg, tagName):
        filters = [['name','is',tagName]]
        fields = ['type', 'id', 'name']
        result = sg.find('Tag', filters, fields)
        return result

    def find_shot_tags(self, sg, project, episode, sequence, shot):
        filters = [['project.Project.name', 'is', project],
                        ['sg_episode.Scene.code', 'is', episode],
                        ['sg_sequence_code', 'is', sequence],
                        ['code', 'is', shot]]
        fields = ['tags', 'code']
        result = sg.find('Shot', filters, fields)
        return result


def show(mode='default',activeTab = 0):
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        if mode == 'default':
            maya_win.deleteUI(uiName)
            myApp = RenderBrowser(maya_win.getMayaWindow())
        myApp.show()
        myApp.ui.tabs.setCurrentIndex(activeTab)
    else:
        logger.info('Run in standalone\n')
        app = QtWidgets.QApplication.instance()
        if not app:
            app = QtWidgets.QApplication(sys.argv)
        if mode == 'default':
            myApp = RenderBrowser()
        myApp.show()
        myApp.ui.tabs.setCurrentIndex(activeTab)
        stylesheet.set_default(app)
        sys.exit(app.exec_())
    return myApp


def show2(parent=None): 
    myApp = RenderBrowser(parent)
    myApp.show()
    myApp.ui.tabs.setCurrentIndex(2)


if __name__ == '__main__':
    show()