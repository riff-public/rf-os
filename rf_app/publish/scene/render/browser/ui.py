import os
import sys
import traceback
from collections import OrderedDict

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

module_path = sys.modules[__name__].__file__
module_dir = os.path.dirname(module_path)
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_utils.widget import project_widget
from rf_utils.widget import entity_browse_widget
from rf_utils import icon
sg = None

import custom_widget
import utils

moduleDir = os.path.dirname(sys.modules[__name__].__file__)

allStep = ['Light', 'FX', 'Comp', 'Art']

class Config:
    # imgPreview = '%s/icons/dropImage.png' % moduleDir
    filePreview = utils.back_slash_to_forwardslash('%s/icons/dropfile.png' % moduleDir)
    # filePreview = '%s/icons/dropfile.png' % moduleDir

class ArtPublishSetDir(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(ArtPublishSetDir, self).__init__(parent)
        # self.taskWidget = QtWidgets.QListWidget()
        # self.taskWidget.setObjectName("taskWidget")
        # self.taskWidget.setMinimumSize(QtCore.QSize(300, 30))
        self.taskWidget = QtWidgets.QTreeWidget()
        self.taskWidget.setSortingEnabled(False)
        self.taskWidget.setHeaderHidden(False)
        self.taskWidget.setObjectName("taskWidget")
        # self.taskWidget.setMinimumSize(QtCore.QSize(1400, 250))
        self.taskWidget.setFixedSize(QtCore.QSize(1320, 250))
        # self.taskWidget.adjustSize()
        self.tagLayout = QtWidgets.QHBoxLayout()
        self.tagLabel = QtWidgets.QLabel()
        self.tagLabel.setObjectName("Tags")
        self.tagLabel.setText("Tags")
        self.tagComboBox = QtWidgets.QComboBox()
        self.tagComboBox.setMinimumSize(QtCore.QSize(100, 10))
        # self.createTagButton = QtWidgets.QPushButton()
        # self.createTagButton.setMinimumSize(QtCore.QSize(10, 10))
        # self.createTagButton.setObjectName("createTagButton")
        # self.createTagButton.setText("+")
        spacerItem = QtWidgets.QSpacerItem(0, 0, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        # self.taskLayout = QtWidgets.QHBoxLayout()
        # self.taskLabel = QtWidgets.QLabel(self)
        # self.taskLabel.setObjectName("Tasks")
        # self.taskLabel.setText("Tasks")
        self.taskComboBox = QtWidgets.QComboBox()
        self.taskComboBox.setMinimumSize(QtCore.QSize(100, 10))
        # self.createTaskButton = QtWidgets.QPushButton(self)
        # self.createTaskButton.setMinimumSize(QtCore.QSize(10, 10))
        # self.createTaskButton.setObjectName("createTaskButton")
        # self.createTaskButton.setText("Add Task")
        taskSpacerItem = QtWidgets.QSpacerItem(0, 0, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.taskLabel = QtWidgets.QLabel(self)
        self.taskLabel.setObjectName("Tasks")
        self.taskLabel.setText("Tasks")

        self.createTaskButton = QtWidgets.QPushButton()
        self.createTaskButton.setMinimumSize(QtCore.QSize(10, 30))
        self.createTaskButton.setObjectName("createTaskButton")
        self.createTaskButton.setText("Add Task")

        self.pageH_layout = QtWidgets.QHBoxLayout(self)
        self.page1_layout = QtWidgets.QVBoxLayout(self)
        # self.page1_layout.addWidget(self.taskLabel)

        self.tagLayout.addWidget(self.tagLabel)
        self.tagLayout.addWidget(self.tagComboBox)
        # self.tagLayout.addWidget(self.createTagButton)
        self.tagLayout.addItem(spacerItem)
        self.page1_layout.addLayout(self.tagLayout)

        # self.taskLayout.addWidget(self.taskLabel)
        # self.taskLayout.addWidget(self.taskComboBox)
        # # self.taskLayout.addWidget(self.createTaskButton)
        # self.taskLayout.addItem(taskSpacerItem)
        # self.page1_layout.addLayout(self.taskLayout)

        self.page1_layout.addWidget(self.taskLabel)
        # self.page1_layout.addWidget(self.taskWidget)
        self.page1_layout.addWidget(self.taskWidget)
        # self.page1_layout.addWidget(self.createTaskButton)
        self.page1_layout.addWidget(self.createTaskButton)

        self.dropFile_widget = custom_widget.Display()
        self.dropFile_widget.set_style(Config.filePreview)
        self.page2_layout = QtWidgets.QVBoxLayout(self)
        # self.page2_layout.addWidget(self.dropFile_widget)

        # spacerItem = QtWidgets.QSpacerItem(0, 0, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        # self.page2_layout.addItem(spacerItem)

        self.descLabel = QtWidgets.QLabel(self)
        self.descLabel.setObjectName("Description")
        self.descLabel.setText("Description")
        self.descriptionText = QtWidgets.QPlainTextEdit(self)
        self.descriptionText.setObjectName("descriptionText")


        self.page1_layout.addWidget(self.descLabel)
        self.page1_layout.addWidget(self.descriptionText)


        self.pageH_layout.addLayout(self.page1_layout)
        # self.pageH_layout.addLayout(self.page2_layout)

        # self.page2_layout.addWidget(self.descLabel)
        # self.page2_layout.addWidget(self.descriptionText)
        # self.page2_layout.setStretch(0, 3)
        # self.page2_layout.setStretch(1, 0)
        # self.page2_layout.setStretch(2, 0)
        # self.page2_layout.setStretch(3, 1)

        self.pageH_layout.setStretch(0, 0)
        self.pageH_layout.setStretch(1, 1)


        self.init_signals()

    def init_signals(self):
        self.dropFile_widget.dropped.connect(self.dropped_file)
        self.dropFile_widget.deleted.connect(self.set_display_dropFile)

    def dropped_file(self,path):
        # if os.path.isdir(path) or '.mov' in path:
        self.dropFile_widget.add_item(path)
        self.dropFile_widget.display.setStyleSheet("background-color: rgb(50, 50, 50);")

    def set_display_dropFile(self):
        if self.dropFile_widget.display.count() == 0:
            self.dropFile_widget.set_style(Config.filePreview)

class PublishSetDir(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(PublishSetDir, self).__init__(parent)
        self.dropFile_widget = custom_widget.Display()
        self.dropFile_widget.set_style(Config.filePreview)
        self.page1_layout = QtWidgets.QVBoxLayout(self)
        self.page1_layout.addWidget(self.dropFile_widget)

        spacerItem = QtWidgets.QSpacerItem(0, 0, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.page1_layout.addItem(spacerItem)

        self.descLabel = QtWidgets.QLabel(self)
        self.descLabel.setObjectName("Description")
        self.descLabel.setText("Description")
        self.descriptionText = QtWidgets.QPlainTextEdit(self)
        self.descriptionText.setObjectName("descriptionText")

        self.page1_layout.addWidget(self.descLabel)
        self.page1_layout.addWidget(self.descriptionText)
        self.page1_layout.setStretch(0, 3)
        self.page1_layout.setStretch(1, 0)
        self.page1_layout.setStretch(2, 0)
        self.page1_layout.setStretch(3, 1)


        self.init_signals()

    def init_signals(self):
        self.dropFile_widget.dropped.connect(self.dropped_file)
        self.dropFile_widget.deleted.connect(self.set_display_dropFile)

    def dropped_file(self,path):
        # if os.path.isdir(path) or '.mov' in path:
        self.dropFile_widget.add_item(path)
        self.dropFile_widget.display.setStyleSheet("background-color: rgb(50, 50, 50);")

    def set_display_dropFile(self):
        if self.dropFile_widget.display.count() == 0:
            self.dropFile_widget.set_style(Config.filePreview)

class Ui(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super(Ui, self).__init__(parent)

        self.setObjectName("MainWindow")
        self.resize(1600, 570)

        self.centralwidget = QtWidgets.QWidget(self)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")


        self.tabLayoutWidget = QtWidgets.QWidget(self.centralwidget)
        self.publishWidget = QtWidgets.QWidget(self.tabLayoutWidget)
        self.gridLayout_3 = QtWidgets.QGridLayout(self.publishWidget)
        self.gridLayout_3.setObjectName("gridLayout_3")

        
        # self.verticalLayout_tab = QtWidgets.QVBoxLayout(self.tabLayoutWidget)
        # self.verticalLayout_tab.addLayout(self.gridLayout_3)

        self.setDirWidget = PublishSetDir()
        self.artSetDirWidget = ArtPublishSetDir()

        self.nameLabel = 'Task Name'
        self.pathLabel = 'Path'
        self.actionLabel = 'Action'
        self.previewLabel = 'Preview Path'
        self.actionPreviewLabel = 'Action'
        self.tagLabel = 'Tag'
        self.addTagLabel = 'AddTag'
        self.headers = [self.nameLabel, self.pathLabel, self.actionLabel, self.previewLabel, self.actionPreviewLabel, self.tagLabel, self.addTagLabel]
        header = QtWidgets.QTreeWidgetItem(self.headers)
        self.artSetDirWidget.taskWidget.setHeaderItem(header)
        self.artSetDirWidget.taskWidget.setColumnWidth(0, 150)
        self.artSetDirWidget.taskWidget.setColumnWidth(1, 300)
        self.artSetDirWidget.taskWidget.setColumnWidth(2, 100)
        self.artSetDirWidget.taskWidget.setColumnWidth(3, 350)
        self.artSetDirWidget.taskWidget.setColumnWidth(4, 100)
        self.artSetDirWidget.taskWidget.setColumnWidth(5, 200)
        self.artSetDirWidget.taskWidget.setColumnWidth(6, 100)

        self.tabs = QtWidgets.QTabWidget(self.tabLayoutWidget)
        self.tabs.addTab(self.publishWidget,"Publish")
        self.tabs.addTab(self.setDirWidget,"PublishSetDir")
        self.tabs.addTab(self.artSetDirWidget,"ArtPublishSetDir")
        self.gridLayout.addWidget(self.tabLayoutWidget, 0, 1, 1, 1)

        self.layerLayout = QtWidgets.QHBoxLayout()

        self.layerLabel = QtWidgets.QLabel(self.centralwidget)
        self.layerLabel.setObjectName("Layer")
        self.layerLabel.setText("Layer")
        # self.gridLayout_3.addWidget(self.layerLabel, 0, 1, 1, 1)
        self.layerLayout.addWidget(self.layerLabel)

        # spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        # self.layerLayout.addItem(spacerItem)
        self.searchLayer = QtWidgets.QLineEdit(self.centralwidget)
        self.searchLayer.setObjectName("lasetestVersion")
        # self.searchLayer.setText("search : ")
        self.layerLayout.addWidget(self.searchLayer)

        self.gridLayout_3.addLayout(self.layerLayout, 0, 1, 1, 1)

        self.layerWidget = QtWidgets.QListWidget(self.centralwidget)
        self.layerWidget.setObjectName("layerWidget")
        self.gridLayout_3.addWidget(self.layerWidget, 1, 1, 1, 1)


        self.versionLayout = QtWidgets.QHBoxLayout()

        self.versionLabel = QtWidgets.QLabel(self.centralwidget)
        self.versionLabel.setObjectName("Version")
        self.versionLabel.setText("Version")
        self.versionLayout.addWidget(self.versionLabel)

        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.versionLayout.addItem(spacerItem)

        self.lastestCheck = QtWidgets.QCheckBox(self.centralwidget)
        self.lastestCheck.setObjectName("lasetestVersion")
        self.lastestCheck.setText("show lastest version")
        self.versionLayout.addWidget(self.lastestCheck)

        self.gridLayout_3.addLayout(self.versionLayout, 0, 2, 1, 1)


        self.versionWidget = QtWidgets.QListWidget(self.centralwidget)
        self.versionWidget.setObjectName("versionWidget")
        self.gridLayout_3.addWidget(self.versionWidget, 1, 2, 1, 1)

        self.verticalLayout_3 = QtWidgets.QVBoxLayout()
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.gridLayout_3.addLayout(self.verticalLayout_3, 1, 4, 1, 1)

        self.moveRightButton = QtWidgets.QPushButton(self.centralwidget)
        self.moveRightButton.setMinimumSize(QtCore.QSize(0, 10))
        self.moveRightButton.setObjectName("moveRightButton")
        self.moveRightButton.setText(">>")
        self.verticalLayout_3.addWidget(self.moveRightButton)
        self.moveLeftButton = QtWidgets.QPushButton(self.centralwidget)
        self.moveLeftButton.setMinimumSize(QtCore.QSize(0, 10))
        self.moveLeftButton.setObjectName("moveLeftButton")
        self.moveLeftButton.setText("<<")
        self.verticalLayout_3.addWidget(self.moveLeftButton)



        self.queueLabel = QtWidgets.QLabel(self.centralwidget)
        self.queueLabel.setObjectName("Queue")
        self.queueLabel.setText("Queue")
        self.gridLayout_3.addWidget(self.queueLabel, 0, 5, 1, 1)

        self.verticalLayout_4 = QtWidgets.QVBoxLayout()
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.queueWidget = QtWidgets.QListWidget(self.centralwidget)
        self.queueWidget.setObjectName("queueWidget")

        self.verticalLayout_4.addWidget(self.queueWidget)
        self.gridLayout_3.addLayout(self.verticalLayout_4, 1, 5, 1, 1)
        self.line = QtWidgets.QFrame(self.centralwidget)
        self.line.setFrameShape(QtWidgets.QFrame.VLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")

        self.descLabel = QtWidgets.QLabel(self.centralwidget)
        self.descLabel.setObjectName("Description")
        self.descLabel.setText("Description")
        self.gridLayout_3.addWidget(self.descLabel, 2, 1, 1, 1)

        self.descriptionText = QtWidgets.QPlainTextEdit(self.centralwidget)
        self.descriptionText.setObjectName("descriptionText")
        self.gridLayout_3.addWidget(self.descriptionText, 3, 1, 1, 5)
        self.gridLayout_3.addWidget(self.line, 1, 3, 1, 1)
        self.gridLayout_3.setRowStretch(1, 3)
        self.gridLayout_3.setRowStretch(3, 1)
        # self.gridLayout.addLayout(self.gridLayout_3, 0, 1, 1, 1)

        self.setup_widget()

        self.gridLayout.setColumnStretch(0, 0)
        self.gridLayout.setColumnStretch(1, 1)
        self.verticalLayout_2.addLayout(self.gridLayout)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem)

        self.skip_chkBox = QtWidgets.QCheckBox("Skip Checking Frame", self.centralwidget)
        self.publishButton = QtWidgets.QPushButton(self.centralwidget)
        self.publishButton.setMinimumSize(QtCore.QSize(300, 30))
        self.publishButton.setObjectName("publishButton")
        self.publishButton.setText("Publish")
        self.horizontalLayout_2.addWidget(self.skip_chkBox)
        self.horizontalLayout_2.addWidget(self.publishButton)

        #self.horizontalLayout_2.setStretch(0, 2)
        #self.horizontalLayout_2.setStretch(0, 1)
        self.verticalLayout_2.addLayout(self.horizontalLayout_2)
        self.setCentralWidget(self.centralwidget)

        self.init_signals()


    def setup_widget(self): 
        # navigation bar 
        self.nav_layout = QtWidgets.QVBoxLayout()
        self.project_widget = project_widget.SGProjectComboBox(sg=sg)
        self.nav_layout.addWidget(self.project_widget)


        """ add sg navigation widget """
        self.sg_nav_widget = entity_browse_widget.SGSceneNavigation(sg=sg, shotFilter=True)
        self.sg_nav_widget.shotExtraFilter = ['sg_shot_type', 'is', 'Shot']
        self.sg_nav_widget.shotWidget.set_searchable(True)

        self.nav_layout.addWidget(self.sg_nav_widget)
        
        self.step_label = QtWidgets.QLabel('Step :')
        self.step_widget = QtWidgets.QComboBox()
        for step in allStep:
            self.step_widget.addItem(step)

        self.nav_layout.addWidget(self.step_label)
        self.nav_layout.addWidget(self.step_widget)

        # spacer 
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.nav_layout.addItem(spacerItem)

        self.status_label = QtWidgets.QLabel('Status :')
        self.nav_layout.addWidget(self.status_label)

        self.nav_layout.setStretch(0, 0)
        self.nav_layout.setStretch(1, 0)
        self.nav_layout.setStretch(2, 0)
        self.nav_layout.setStretch(3, 0)
        self.nav_layout.setStretch(4, 2)
        self.nav_layout.setStretch(5, 0)

        self.gridLayout.addLayout(self.nav_layout, 0, 0, 1, 1)

        project = self.project_widget.current_item()
        self.sg_nav_widget.list_episodes(project)

        self.layerWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.versionWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.queueWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)

    def init_signals(self): 
        self.project_widget.projectChanged.connect(self.project_changed)
        # self.moveRightButton.clicked.connect(self.add_queue)
        self.moveLeftButton.clicked.connect(self.remove_queue)

    def project_changed(self, project):
        self.sg_nav_widget.list_episodes(project)

    # def add_queue(self):
    #     items = self.versionWidget.selectedItems()
    #     if items:
    #         for item in items:
    #             name = item.text()
    #             data = item.data(QtCore.Qt.UserRole)
    #             toolTip = item.toolTip()
    #             if not self.queueWidget.findItems(name, QtCore.Qt.MatchExactly):
    #                 item = QtWidgets.QListWidgetItem( name, self.queueWidget)
    #                 item.setData(QtCore.Qt.UserRole,data)
    #                 if toolTip:
    #                     item.setBackground( QtGui.QColor('#ffb140') )
    #                 else:
    #                     item.setBackground( QtGui.QColor('#95ff79') )
    #                 item.setForeground (QtGui.QColor("black"))

    def keyPressEvent(self, event):
        if event.key() == QtCore.Qt.Key_Delete:
            self.remove_queue()

    def remove_queue(self):
        items = self.queueWidget.selectedItems()
        if items:
            for item in items:
                row = self.queueWidget.row(item)
                self.queueWidget.takeItem(row)

class ButtonWidget(QtWidgets.QWidget):
    status = QtCore.Signal(str)
    def __init__(self, parent=None) :
        super(ButtonWidget, self).__init__(parent)
        self.ui()
        
    def ui(self): 
        self.layout = QtWidgets.QHBoxLayout()
        self.button = QtWidgets.QPushButton()
        self.layout.addWidget(self.button)
        self.setLayout(self.layout)
        self.layout.setContentsMargins(1, 1, 1, 1)

