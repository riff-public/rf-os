#!/usr/bin/env python
# -- coding: utf-8 --
# v.0.0.1 polytag switcher
_title = 'Tool'
_version = 'v.0.0.2'
_des = 'Change workspace to V:'
uiName = 'FxPublish'

#Import python modules
import sys
import os 
import json 
import getpass

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

import rf_config as config

# framework modules 
from rf_utils import log_utils
from rf_utils.ui import load
from rf_utils.ui import stylesheet

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

import ui 
reload(ui)
from rf_utils.pipeline import user_pref
from rf_utils.context import context_info
from rf_utils import file_utils
from rf_utils import register_shot
from rf_utils import register_sg
from rf_app.publish.scene.utils import output_utils 
from rf_app.publish.asset import sg_hook

from rf_utils.sg import sg_process
sg = sg_process.sg 


class Config: 
    fileTypes = ['abc', 'rs']
    fx = 'fx'
    processMap = {'main': 'fx'}

class FxPublishMain(QtWidgets.QMainWindow):

    def __init__(self, parent=None):
        #Setup Window
        super(FxPublishMain, self).__init__(parent)

        # ui read
        self.ui = ui.FxPublishUi()
        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)
        self.statusBar()

        # title
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.resize(750, 650)

        # data 
        self.userPref = user_pref.ToolSetting(uiName)
        
        # signals 
        self.init_signals()
        self.init_functions()

    def init_signals(self): 
        self.ui.projectWidget.projectChanged.connect(self.project_changed)
        self.ui.sgWidget.completed.connect(self.shot_selected)
        self.ui.processComboBox.currentIndexChanged.connect(self.process_selected)
        self.ui.fetchButton.clicked.connect(self.list_data)

        self.ui.cacheButton.clicked.connect(self.get_preview)
        self.ui.renderCacheButton.clicked.connect(self.get_cache)
        self.ui.movButton.clicked.connect(self.get_mov)

        self.ui.publishButton.clicked.connect(self.publish)
        self.ui.taskComboBox.fetchFinished.connect(self.sync_task)

    def init_functions(self): 
        ep = self.ui.sgWidget.episodeWidget.current_item()
        if not ep: 
            projectEntity = self.ui.projectWidget.current_item()
            self.ui.sgWidget.list_episodes(projectEntity)

    def project_changed(self, project): 
        """ what happen when project changed """
        self.ui.sgWidget.list_episodes(project)
        # save ui selection
        self.save_last_selection()

    def save_last_selection(self):
        """ save selection to json file """
        data = dict()
        project = str(self.ui.projectWidget.projectComboBox.currentText())
        episode = str(self.ui.sgWidget.episodeWidget.comboBox.currentText())
        data['project'] = project
        data['episode'] = episode

        result = self.userPref.write(data)

    def shot_selected(self, shotDict): 
        """ shot changed """ 
        # store these global data to carry over when list data 
        self.shotDict = shotDict
        self.project = shotDict['project']['name']
        self.shotId = shotDict['id']
        self.clear_data()

    def list_data(self): 
        """ this function list all data after shot selected """
        project = self.project
        context = context_info.Context()
        context.use_sg(sg, project, context_info.ContextKey.scene, entityId=self.shotId)
        self.scene = context_info.ContextPathInfo(context=context)
        self.scene.context.update(step=Config.fx)
        stepPath = self.scene.path.step().abs_path()
        # hard code change workspace to V:
        vPath = stepPath.replace('P:', 'V:')
        combineProcesses = []

        if os.path.exists(stepPath): 
            processes = ['%s/%s' % (stepPath, a) for a in file_utils.list_folder(stepPath)]
            combineProcesses += processes
            # self.update_process(processes)

        if os.path.exists(vPath): 
            processes = ['%s/%s' % (vPath, a) for a in file_utils.list_folder(vPath)]
            combineProcesses += processes

        self.update_process(combineProcesses)
        self.list_tasks()
        self.ui.fetchButton.setEnabled(False)


    def clear_data(self): 
        """ clear data if shot selection is not finished """ 
        self.ui.fetchButton.setEnabled(True)
        self.ui.processComboBox.clear()

    def update_process(self, processes): 
        with BlockSignals(self.ui.processComboBox): 
            self.ui.processComboBox.clear()

            for i, path in enumerate(processes): 
                drive, relPath = os.path.splitdrive(path)
                process = os.path.split(path)[-1]
                self.ui.processComboBox.addItem('%s (%s)' % (process, drive))
                self.ui.processComboBox.setItemData(i, [process, path])

        # manual trigger functions 
        self.process_selected(self.ui.processComboBox.currentIndex())

    def process_selected(self, index): 
        index = self.ui.processComboBox.currentIndex()
        data = self.ui.processComboBox.itemData(index, QtCore.Qt.UserRole)
        process, path = data
        # process = str(self.ui.processComboBox.currentText())
        self.scene.context.update(process=process)

        self.list_output()
        self.sync_task()

    def list_output(self): 
        outputPath = self.scene.path.work_output().abs_path()
        self.ui.fileWidget.listWidget.clear()

        index = self.ui.processComboBox.currentIndex()
        data = self.ui.processComboBox.itemData(index, QtCore.Qt.UserRole)
        if data: 
            process, path = data
            drive, relPath = os.path.splitdrive(path)

            # hard code to V: 
            outputPath = outputPath.replace('P:', drive)

            if os.path.exists(outputPath): 
                files = ['%s/%s' % (outputPath, a) for a in file_utils.list_file(outputPath)]
                seqs = ['%s/%s' % (outputPath, a) for a in file_utils.list_folder(outputPath)]

                for file in files: 
                    self.ui.fileWidget.add_file(file)

                for seq in seqs: 
                    self.ui.fileWidget.add_sequences(seq)

    def sync_task(self, *args): 
        """ sync process to task """ 
        # process = str(self.ui.processComboBox.currentText())
        data = self.ui.processComboBox.itemData(self.ui.processComboBox.currentIndex(), QtCore.Qt.UserRole)
        process, path = data
        task = process 

        if process in Config.processMap.keys(): 
            task = Config.processMap[process]

        tasks = self.ui.taskComboBox.list_tasks()
        index = tasks.index(task) if task in tasks else 0
        
        if not task in tasks: 
            self.ui.taskComboBox.comboBox.addItem(task)
            index = len(self.ui.taskComboBox.list_tasks()) - 1
            item = self.ui.taskComboBox.comboBox.model().item(index)
            item.setForeground(QtGui.QColor(100, 160, 100))
        
        self.ui.taskComboBox.comboBox.setCurrentIndex(index)

    def list_tasks(self): 
        """ list fx tasks """ 
        self.ui.taskComboBox.fetch_task(self.scene.project, 'Shot', self.scene.name, Config.fx)

    def get_preview(self): 
        """ this is preview file which is alembic -> abc / gpu """ 
        data = self.ui.fileWidget.current_data()
        self.ui.cacheLineEdit.setText(data)

    def get_cache(self): 
        """ this is render file -> rs """ 
        data = self.ui.fileWidget.current_data()
        self.ui.renderCacheLineEdit.setText(data)

    def get_mov(self): 
        """ this is mov upload shotgun """ 
        data = self.ui.fileWidget.current_data()
        self.ui.movLineEdit.setText(data) 

    def publish(self): 
        """ publish input files """ 
        # reg file 
        stdout('\nPublishing ...', self.statusBar)
        reg = register_shot.Register(self.scene)
        elemReg = register_sg.ShotElement(self.scene)

        # task / status 
        status = self.ui.statusComboBox.current_item()
        task = self.ui.taskComboBox.current_item()
        taskName = self.scene.process 
        description = str(self.ui.descriptionLineEdit.text())
        userEntity = self.ui.userWidget.data()

        # if no task name, create task 
        if not task: 
            task = sg_process.create_task(self.shotDict, Config.fx, taskName)
            stdout('Create task %s' % taskName, self.statusBar)

        # publish
        previewCacheResult = self.publish_preview(reg, elemReg, task=task, status=status)
        renderCache = str(self.ui.renderCacheLineEdit.text())
        if os.path.exists(renderCache):
            if os.path.isfile(renderCache):
                renderCacheResult = self.publish_cache(reg, elemReg, task=task, status=status)
            elif os.path.isdir(renderCache):
                renderCacheResult = self.publish_caches(reg, elemReg, task=task, status=status)
        mediaResult = self.publish_media(reg, task, status)

        # register 
        reg.register()

        # set  fx task 
        sg_process.set_task_status(task.get('id'), status)

        # assign task 
        currentTask = sg_process.sg.find_one('Task', [['id', 'is', task['id']]], ['task_assignees', 'id'])
        users = currentTask['task_assignees']
        if not users: 
            sg_process.sg.update('Task', currentTask.get('id'), {'task_assignees': [userEntity]})

        message = self.get_output_message(previewCacheResult, mediaResult, renderCacheResult)
        stdout(message, self.statusBar)


    def publish_preview(self, reg, elemReg, task=None, status=''): 
        previewCache = str(self.ui.cacheLineEdit.text())

        if os.path.exists(previewCache): 
            stdout('Publishing preview cache ...', self.statusBar)
            ext = os.path.splitext(previewCache)[-1]
            schemaKey = 'cacheGlobalHero'
            outputKey = 'cache'
            taskName = task.get('content') if task else ''

            # output name 
            globalHero = self.scene.path.scheme(key=schemaKey).abs_path()
            outputName = self.scene.output_name(outputKey=outputKey, hero=True, ext=ext)
            heroFile = '%s/%s' % (globalHero, outputName)
            filetype = 'fx_cache'

            # use process as namespace key 
            nsKey = self.scene.process
            namespace = self.scene.process 
            node = ''
            version = 'v001'
            self.scene.context.update(publishVersion=version, task=taskName)


            logger.info('Copying hero file %s ...' % heroFile)
            # copy file 
            exportCheck = file_utils.ExportCheck(heroFile)
            
            with exportCheck: 
                stdout('Copying cache %s...' % heroFile, self.statusBar)
                file_utils.xcopy_file(previewCache, heroFile)
                stdout('Finished copy cache', self.statusBar)

            # register file 
            stdout('Register data ...', self.statusBar)
            reg.set_data(self.scene, 'asset', filetype, self.scene.step, nsKey, namespace, node, cache=[heroFile, heroFile])
            elem = elemReg.add(nsKey, 
                            step=self.scene.step, 
                            hero=heroFile, 
                            filetype=filetype, 
                            version=version, 
                            namespace=namespace, 
                            status=status
                            )

            message = 'Publish preview cache complete' if exportCheck.result else 'Publish preview cache failed'
            stdout(message, self.statusBar)

            return exportCheck.result


    def publish_caches(self, reg, elemReg, task=None, status=''): 
        renderCache = str(self.ui.renderCacheLineEdit.text())

        # if os.path.exists(renderCache) and os.path.isdir(renderCache): 
        stdout('Publishing sequential render caches ...', self.statusBar)
        ext = '.ma'
        schemaKey = 'cacheGlobalHero'
        outputKey = 'cache'
        
        filetype = 'fx_cache'
        taskName = task.get('content') if task else ''

        # output name 
        globalHero = self.scene.path.scheme(key=schemaKey).abs_path()
        outputName = self.scene.output_name(outputKey=outputKey, hero=True, ext=ext)
        heroFile = '%s/%s' % (globalHero, outputName)

        cache_files = [f for f in os.listdir(renderCache) if os.path.isfile('%s/%s' %(renderCache, f)) and os.path.splitext(f)[-1] in ('.rs', '.vdb')]
        cf, cfext = os.path.splitext(cache_files[0])
        output_key_mapping = {'.rs':'rsproxy', '.vdb':'vdb'}
        cacheOutputKey = output_key_mapping[cfext]
        globalHeroRs = '%s/%s' %(globalHero, cacheOutputKey)
        outputRs = self.scene.output_name(outputKey=cacheOutputKey, hero=True)
        heroRs = '%s/%s' % (globalHeroRs, outputRs)

        # use process as namespace key 
        nsKey = self.scene.process
        namespace = self.scene.process 
        node = ''
        version = 'v001'
        self.scene.context.update(publishVersion=version, task=taskName)

        # print 'renderCache: %s' %renderCache  # work/folder
        # print 'heroFile: %s '%heroFile  # publ/.ma
        # print 'heroRs: %s' %heroRs  # publ/.hero.####.rs
        # return 
        stdout('Copying caches from: %s' %(renderCache))
        # check folder contents, get destination paths
        
        overAll_result = True
        dest_paths = []
        for cf in cache_files:
            f, ext = os.path.splitext(cf )
            fp = '%s/%s' %(renderCache, cf)
            frame_str = file_utils.get_frame_digits(cf, padding=4)
            if frame_str:
                dest_path = heroRs.replace('####', frame_str)
                dest_dir = os.path.dirname(dest_path)
                if not os.path.exists(dest_dir):
                    os.makedirs(dest_dir)

                dest_paths.append(dest_path)
                res = file_utils.xcopy_file(fp, dest_path)
                
                if overAll_result and not res:
                    overAll_result = False

        stdout('Finish copying caches:\n  %s\n  %s' %(renderCache, heroRs), self.statusBar)

        # copy .ma template from global to publ
        dp, dpext = os.path.splitext(dest_paths[0])
        template_name_mapping = {'.rs':'rsTemplate', '.vdb':'vdbTemplate'}
        template_key = template_name_mapping[dpext]

        template_path = self.scene.projectInfo.asset.global_asset(key=template_key)
        searchStr = self.scene.projectInfo.asset.path_dummy()

        stdout('Copying cache hero:\n  %s\n  %s' %(template_path, heroFile), self.statusBar)
        res = file_utils.xcopy_file(template_path, heroFile)
        if not res:
            overAll_result = False
        
        stdout('Over all result: %s' %overAll_result, self.statusBar)
        if overAll_result:
            # edit .ma to point to cache path
            stdout('Editting Ascii: %s' %heroFile, self.statusBar)
            data = None
            with open(heroFile, 'r') as hf:
                data = hf.read()
            
            stdout('Search String is: %s' %searchStr, self.statusBar)
            stdout('Cache path is: %s' %dest_paths[0], self.statusBar)
            data = data.replace(searchStr, dest_paths[0])
            with open(heroFile, 'w') as hf:
                hf.write(data)

            stdout('Finished editting Ascii', self.statusBar)
            # ------------------------------------------------------------
            # register file 
            stdout('Register data ...', self.statusBar)
            reg.set_data(self.scene, 'asset', filetype, self.scene.step, nsKey, namespace, node, maya=[heroFile, heroFile])
            elem = elemReg.add(nsKey, 
                            step=self.scene.step, 
                            hero=heroFile, 
                            filetype=filetype, 
                            version=version, 
                            namespace=namespace, 
                            status=status
                            )

            message = 'Publish render cache complete' if overAll_result else 'Publish render cache failed'
            stdout(message, self.statusBar)

        return overAll_result

    def publish_cache(self, reg, elemReg, task=None, status=''): 
        renderCache = str(self.ui.renderCacheLineEdit.text())

        # if os.path.exists(renderCache) and os.path.isfile(renderCache):
        stdout('Publishing single render cache ...', self.statusBar)
        fn, ext =  os.path.splitext(renderCache)
        schemaKey = 'cacheGlobalHero'
        outputKey = 'cache'
        filetype = 'fx_cache'
        taskName = task.get('content') if task else ''

        # output name 
        globalHero = self.scene.path.scheme(key=schemaKey).abs_path()
        outputName = self.scene.output_name(outputKey=outputKey, hero=True, ext=ext)
        heroFile = '%s/%s' % (globalHero, outputName)           

        # use process as namespace key 
        nsKey = self.scene.process
        namespace = self.scene.process 
        node = ''
        version = 'v001'
        self.scene.context.update(publishVersion=version, task=taskName)


        # print 'renderCache: %s' %renderCache  # work/folder
        # print 'heroFile: %s '%heroFile  # publ/.ma
        # print 'heroRs: %s' %heroRs  # publ/.hero.####.rs
        
        # check folder contents, get destination paths
        stdout('Copying cache hero:\n  %s\n  %s' %(renderCache, heroFile), self.statusBar)
        copy_result = file_utils.xcopy_file(renderCache, heroFile)
        stdout('Copy result: %s' %copy_result, self.statusBar)
        result = False
        if copy_result:
            result = True
            # ------------------------------------------------------------
            # register file 
            stdout('Register data ...', self.statusBar)
            reg.set_data(self.scene, 'asset', filetype, self.scene.step, nsKey, namespace, node, maya=[heroFile, heroFile])
            elem = elemReg.add(nsKey, 
                            step=self.scene.step, 
                            hero=heroFile, 
                            filetype=filetype, 
                            version=version, 
                            namespace=namespace, 
                            status=status
                            )

            message = 'Publish render cache complete' if result else 'Publish render cache failed'
            stdout(message, self.statusBar)

        return result

    def publish_media(self, reg, taskEntity, status): 
        mov = str(self.ui.movLineEdit.text())
        schemaKey = ['outputImgPath', 'outputHeroImgPath']
        outputKey = 'playblast'
        version = 'v001'
        description = str(self.ui.descriptionLineEdit.text())
        userEntity = self.ui.userWidget.data()
        taskName = taskEntity.get('content')

        if os.path.exists(mov): 
            stdout('Publishing playblast ...', self.statusBar)
            nsKey = self.scene.process
            namespace = self.scene.process 
            self.scene.context.update(publishVersion=version, task=taskName)
            outputPaths = output_utils.export_path(self.scene, self.scene.name_code, namespace=namespace, outputKey=outputKey, schemaKey=schemaKey, outputHeroKey='')

            # copy files 
            for heroFile in outputPaths: 
                exportCheck = file_utils.ExportCheck(heroFile)
                with exportCheck: 
                    stdout('Copying playblast %s ...' % heroFile, self.statusBar)
                    file_utils.xcopy_file(mov, heroFile)
                    stdout('Finish copy playblast', self.statusBar)

            stdout('Uploading to Shotgun ...', self.statusBar)
            reg.set_media(self.scene, media=[outputPaths[0], outputPaths[1]])
            result = sg_hook.publish_version(self.scene, taskEntity, status, [outputPaths[0]], userEntity, description, uploadOption='default')
            stdout('Register Shotgun complete', self.statusBar)
            return True if result else False


    def get_output_message(self, previewCacheResult, mediaResult, cacheResult): 
        message = '\n:::::::::: FX PUBLISH RESULT ::::::::::'
        sd = {True: 'Success', False: 'Failed', None:'Skipped'}

        message += '\nPreview publish %s.' %(sd[previewCacheResult]) 
        message += '\nCache publish %s.' %(sd[cacheResult]) 
        message += '\nMedia publish %s.' %(sd[mediaResult]) 
        return message

def stdout(message, statusBar=None): 
    """ output stdout """
    logger.info(message)
    print message 
    if statusBar: 
        statusBar().showMessage(message)
        QtWidgets.QApplication.processEvents()


class BlockSignals(): 
    """ temporary block signals """ 
    def __init__(self, widget): 
        self.widget = widget 

    def __enter__(self): 
        self.widget.blockSignals(True)

    def __exit__(self, *args): 
        self.widget.blockSignals(False)


def show():
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        uiName = 'FxPublish'
        maya_win.deleteUI(uiName)
        myApp = FxPublishMain(maya_win.getMayaWindow())
        return myApp

    else: 
        logger.info('Run in standalone\n')
        app = QtWidgets.QApplication.instance()
        if not app: 
            app = QtWidgets.QApplication(sys.argv)
        myApp = FxPublishMain()
        myApp.show()
        stylesheet.set_default(app)
        sys.exit(app.exec_())

if __name__ == '__main__': 
    show()
