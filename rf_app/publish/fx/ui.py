# v.0.0.1 polytag switcher
_title = 'FxPublishUi'
_version = 'v.0.0.1'
_des = 'wip'
uiName = 'FxPublish'

import os
import sys
import traceback
from collections import OrderedDict

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_utils import icon
from rf_utils.widget import user_widget
from rf_utils.widget import project_widget
from rf_utils.widget import entity_browse_widget
from rf_utils.widget import file_widget
from rf_utils.widget import task_widget
from rf_utils.widget import status_widget
from rf_utils.pipeline import user_pref
from rf_utils import project_info
from rf_utils.sg import sg_utils



class FxPublishUi(QtWidgets.QWidget):
    status = QtCore.Signal(str)
    def __init__(self, parent=None) :
        super(FxPublishUi, self).__init__(parent)

        # init data 
        self.init_data()
        # tab index

        # main layout
        self.allLayout = QtWidgets.QVBoxLayout()

        # horizontal layout 
        self.hLayout = QtWidgets.QHBoxLayout()
        self.allLayout.addLayout(self.hLayout)

        # layout 
        self.navigation_layout = QtWidgets.QVBoxLayout()
        self.hLayout.addLayout(self.navigation_layout)

        # main layout 
        self.main_layout = QtWidgets.QVBoxLayout()
        self.hLayout.addLayout(self.main_layout)

        # file layout 
        self.file_layout = QtWidgets.QVBoxLayout()
        self.main_layout.addLayout(self.file_layout)

        # publish layout
        self.publish_layout = QtWidgets.QVBoxLayout()
        self.main_layout.addLayout(self.publish_layout)

        self.setup_widget()
        # header
        self.setLayout(self.allLayout)

        # set default sizes 
        self.hLayout.setStretch(0, 3)
        self.hLayout.setStretch(1, 10)

        self.init_signals()


    def setup_widget(self): 
        self.add_logo()
        self.add_project_widget()
        self.add_sg_navigation()
        self.add_file_list()
        self.add_publish_widgets()

        self.set_navigation_size()

    def init_data(self): 
        # data
        self.projectInfo = project_info.ProjectInfo()
        self.userPref = user_pref.ToolSetting(uiName)
        self.userData = self.userPref.read()

    def add_logo(self): 
        self.logoLabel = QtWidgets.QLabel()
        self.logoLabel.setPixmap(QtGui.QPixmap(icon.logo).scaled(64, 64, QtCore.Qt.KeepAspectRatio))
        self.navigation_layout.addWidget(self.logoLabel)
    
    def add_project_widget(self):
        """ add project widget """
        project = self.userData.get('project')
        self.projectWidget = project_widget.SGProjectComboBox(sg=sg_utils.sg, defaultProject=project)
        self.navigation_layout.addWidget(self.projectWidget)

    def add_sg_navigation(self):
        """ add sg navigation widget """
        self.fetchButton = QtWidgets.QPushButton('Next >>')
        self.sgWidget = entity_browse_widget.SGSceneNavigation2(sg=sg_utils.sg, shotFilter=True)
        self.sgWidget.shotExtraFilter = ['sg_shot_type', 'is', 'Shot']
        self.line = hline_widget()
        self.navigation_layout.addWidget(self.sgWidget)
        self.navigation_layout.addWidget(self.line)

        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.navigation_layout.addWidget(self.fetchButton)
        self.navigation_layout.addItem(spacerItem)

    def add_file_list(self): 
        # sublayout 
        self.sub_layout = QtWidgets.QHBoxLayout()
        self.file_layout.addLayout(self.sub_layout)

        # widgets 
        self.processLabel = QtWidgets.QLabel('Process')
        self.processComboBox = QtWidgets.QComboBox()
        self.userWidget = user_widget.UserComboBox()
        self.fileWidget = file_widget.FileListWidgetExtended()
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        # add layouts 
        self.sub_layout.addWidget(self.processLabel)
        self.sub_layout.addWidget(self.processComboBox)
        self.sub_layout.addWidget(self.userWidget)
        self.sub_layout.addItem(spacerItem)
        self.file_layout.addWidget(self.fileWidget)

        # set size 
        self.sub_layout.setStretch(0, 1)
        self.sub_layout.setStretch(1, 2)
        self.sub_layout.setStretch(2, 4)
        self.sub_layout.setStretch(3, 0)

    def add_publish_widgets(self): 
        # add preview lineEdit 
        self.input_layout = QtWidgets.QGridLayout()
        self.task_layout = QtWidgets.QHBoxLayout()
        line = hline_widget()

        self.publish_layout.addLayout(self.input_layout)
        self.publish_layout.addWidget(line)
        self.publish_layout.addLayout(self.task_layout)

        self.cacheLineEdit = QtWidgets.QLineEdit()
        self.renderCacheLineEdit = QtWidgets.QLineEdit()
        self.movLineEdit = QtWidgets.QLineEdit()

        self.cacheCheckBox = QtWidgets.QCheckBox('Fx Preview Cache')
        self.renderCacheCheckBox = QtWidgets.QCheckBox('Fx Render Cache')
        self.movCheckBox = QtWidgets.QCheckBox('Fx Playblast')

        self.cacheButton = QtWidgets.QPushButton('Get')
        self.renderCacheButton = QtWidgets.QPushButton('Get')
        self.movButton = QtWidgets.QPushButton('Get')

        self.descriptionLabel = QtWidgets.QLabel('Description')
        self.descriptionLineEdit = QtWidgets.QLineEdit()

        # button 
        self.publishButton = QtWidgets.QPushButton('Publish')

        # task 
        self.taskComboBox = task_widget.TaskThreadComboBox()
        self.taskComboBox.label.setText('Task')
        self.statusComboBox = status_widget.TaskStatusWidget()
        self.statusComboBox.limit = 'Fx'
        self.statusComboBox.set_icon()
        self.statusComboBox.label.setText('Status')
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        self.input_layout.addWidget(self.cacheCheckBox, 0, 0)
        self.input_layout.addWidget(self.cacheLineEdit, 0, 1)
        self.input_layout.addWidget(self.cacheButton, 0, 2)

        self.input_layout.addWidget(self.renderCacheCheckBox, 1, 0)
        self.input_layout.addWidget(self.renderCacheLineEdit, 1, 1)
        self.input_layout.addWidget(self.renderCacheButton, 1, 2)

        self.input_layout.addWidget(self.movCheckBox, 2, 0)
        self.input_layout.addWidget(self.movLineEdit, 2, 1)
        self.input_layout.addWidget(self.movButton, 2, 2)
        
        self.input_layout.addWidget(self.descriptionLabel, 3, 0)
        self.input_layout.addWidget(self.descriptionLineEdit, 3, 1)

        self.task_layout.addWidget(self.taskComboBox)
        self.task_layout.addWidget(self.statusComboBox)
        self.task_layout.addItem(spacerItem)
        
        self.publish_layout.addWidget(self.publishButton)

    def set_navigation_size(self): 
        self.navigation_layout.setStretch(0, 0)
        self.navigation_layout.setStretch(1, 0)
        self.navigation_layout.setStretch(2, 0)
        self.navigation_layout.setStretch(3, 0)
        self.navigation_layout.setStretch(4, 0)
        self.navigation_layout.setStretch(5, 1)
        self.publishButton.setMinimumSize(QtCore.QSize(0, 30))

        self.cacheLineEdit.setEnabled(True)
        self.cacheButton.setEnabled(True)
        self.renderCacheLineEdit.setEnabled(True)
        self.renderCacheButton.setEnabled(True)
        self.movLineEdit.setEnabled(True)
        self.movButton.setEnabled(True)

        self.cacheCheckBox.setChecked(True)
        self.renderCacheCheckBox.setChecked(True)
        self.movCheckBox.setChecked(True)

        self.task_layout.setStretch(0, 2)
        self.task_layout.setStretch(1, 2)
        self.task_layout.setStretch(2, 1)

        self.taskComboBox.allLayout.setStretch(0, 1)
        self.taskComboBox.allLayout.setStretch(1, 2)

        self.cacheButton.setMinimumSize(QtCore.QSize(60, 0))
        self.renderCacheButton.setMinimumSize(QtCore.QSize(60, 0))
        self.movButton.setMinimumSize(QtCore.QSize(60, 0))

    def init_signals(self): 
        self.cacheCheckBox.stateChanged.connect(self.cacheCheckBox_checked)
        self.renderCacheCheckBox.stateChanged.connect(self.renderCacheCheckBox_checked)
        self.movCheckBox.stateChanged.connect(self.movCheckBox_checked)

    def cacheCheckBox_checked(self, value): 
        state = True if value else False 
        self.cacheLineEdit.setEnabled(state)
        self.cacheButton.setEnabled(state)

    def renderCacheCheckBox_checked(self, value): 
        state = True if value else False 
        self.renderCacheLineEdit.setEnabled(state)
        self.renderCacheButton.setEnabled(state)

    def movCheckBox_checked(self, value): 
        state = True if value else False 
        self.movLineEdit.setEnabled(state)
        self.movButton.setEnabled(state)


def hline_widget():
    line = QtWidgets.QFrame()
    line.setFrameShape(QtWidgets.QFrame.HLine)
    line.setFrameShadow(QtWidgets.QFrame.Sunken)
    return line





        