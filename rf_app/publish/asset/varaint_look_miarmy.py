from rf_utils.context import context_info
from rf_utils import register_entity
from rftool.asset.setup import setup_app
from rf_utils.sg import sg_process
from rf_maya.rftool.utils import maya_utils
from rf_maya.rftool.shade import shade_utils
import maya.cmds as mc 

from collections import OrderedDict
from rf_maya.rftool.clean.texture_checker import maya_hook as hook
import pymel.core as pm 
import maya.cmds as mc
import re
import os
reload(setup_app)

def clear_material_miarmy():
    #### remove all mtr refernce in scene
    list_reference = pm.ls(type='reference')
    for ref_node in list_reference:
        file_name = ref_node.fileName(resolvedName=True, includePath=True, includeCopyNumber=True)
        if '_mtr_ldv_' in file_name: 
            pm.FileReference(ref_node.name()).remove(f=True)
    ## remove_render_layer
    default_layer = pm.ls('defaultRenderLayer')[0]
    layer = pm.ls(type='renderLayer')
    default_layer.setCurrent()
    for lay in layer:
        if lay != default_layer:
            pm.delete(lay)

def generate_render_layer():##### main
    clear_material_miarmy()
    asset = context_info.ContextPathInfo()
    asset.context.use_sg(sg_process.sg, project= asset.project, entityType=asset.entity_type, entityName=asset.name)
    reg = register_entity.Register(asset)
    asset_name = asset.name
    list_look = reg.get_looks('lookdev', 'md')
    mdl_path = reg.get.cache()['heroFile'] ##### reference
    shade_data_path = list_shade_path(asset, list_look)

    asset_namespace = '%s_mdl'%asset_name
    geo_grp = '{namespace}:Geo_Grp'.format(namespace=asset_namespace)
    ###### check already_geo_grp
    if not mc.objExists(geo_grp):
        maya_utils.create_reference_asset(asset_namespace, mdl_path)
    ###### check already_delete_grp
    if not  pm.objExists('Delete_Grp'):
        pm.group(n='Delete_Grp', em=True)
    delete_grp_node = pm.PyNode('Delete_Grp')
    geo_grp_node = pm.PyNode(geo_grp)
    check_geo_parented = delete_grp_node.getChildren()
    if not geo_grp_node in check_geo_parented:
        pm.parent(geo_grp_node, delete_grp_node)
    ####################################################
    pm.select(geo_grp_node)#### select geo_grp
    setup_app.setup_layer(asset, 'lookdev') ####### setup_layer
    if geo_grp_node and shade_data_path:
        assign_shader_layer(asset, shade_data_path, asset_name, asset_namespace)



def list_shade_path(entity, list_look):
    entity_cop = entity.copy()
    reg = register_entity.Register(entity_cop)
    dict_shade = {}
    for look in list_look:    
        mtr_data = reg.get.mtr_render(look=look)
        #{look: ['heroFile']}
        print mtr_data
        if 'heroFile' in mtr_data:
            dict_shade[look] = mtr_data['heroFile']
    return dict_shade  ####### ex: 'BlightgreyCblackHorange': 'P:/SevenChickMovie/asset/publ/char/elephantMaleDefaultHairA/lookdev/BlightgreyCblackHorange/hero/output/elephantMaleDefaultHairA_mtr_ldv_BlightgreyCblackHorange_md.hero.ma',
    
####### assign shade to layer#########
def assign_shader_layer(entity, shade_data, asset_name, target_namespace): #'BlightgreyCblackHorange': 'P:/SevenChickMovie/asset/publ/char/elephantMaleDefaultHairA/lookdev/BlightgreyCblackHorange/hero/output/elephantMaleDefaultHairA_mtr_ldv_BlightgreyCblackHorange_md.hero.ma',
    render_layer = mc.ls(type='renderLayer')
    for layer in render_layer: ##### layer ex:BlightgreyCblackHorange_lookdev
        if not 'defaultRenderLayer' in layer:
            mc.editRenderLayerGlobals(crl = layer)
            look_name = layer.split('_')[0]
            if look_name:
                if look_name in shade_data:
                    shadeFile = shade_data[look_name]
                    if os.path.exists(shadeFile):
                        shade_utils.apply_ref_shade(shadeFile, '{asset_name}_{look}_mtr'.format(asset_name=asset_name, look=look_name), targetNamespace= target_namespace)
    default_render = pm.PyNode('defaultRenderLayer')
    default_render.setCurrent()


def shade_chanel_data(shade_type):
    shade_chanel = {'RedshiftMaterial': ['diffuse_color', 'refl_roughness'] }
    if shade_type in shade_chanel:
        return shade_chanel[shade_type]

def get_look_data():

    # {geo:{diffuse: file_path
    #       refl: file_path}
    #      
    data_geo = OrderedDict()
    render_adjustment = pm.editRenderLayerAdjustment( query=True )
    if render_adjustment:
    # oa_texture_dir_path = 
        for instObjGroups in render_adjustment:
            inst_obj = pm.PyNode(instObjGroups)
            geo = inst_obj.node().getParent()
            geo_shade = inst_obj.node().getParent().shadingGroups()[0]
            #### shade_type### to run in def shade_chanel_data  #####
            shade_type = 'RedshiftMaterial'
            ################ change shadeType 
            shading_engine = geo_shade.connections(type= shade_type)
            if shading_engine:
                rs_mat = shading_engine[0].name()
                chanel_list = shade_chanel_data(shade_type)
                chanel_dict = dict()
                for chanel in chanel_list:
                    attr_ = '{material}.{chanel}'.format(material = rs_mat, chanel = chanel)
                    file_node = find_untill_file(attr_)
                    
                    if file_node:
                        file_path = file_node.getAttr('fileTextureName')
                        list_files = check_udim_pattern(file_path, file_node.name())
                        dict_data = dict()
                        dict_data[chanel] = list_files
                        file_dict = dict()
                        file_dict[file_node.name()] = file_path
                        dict_data['fileTextureName'] = file_dict
                        chanel_dict[chanel] = dict_data
                data_geo[geo.name()] = chanel_dict
            
        return data_geo

def get_dict_look_data():
    render_layers = pm.ls(type='renderLayer')
    look_dict_data = {}
    for layer in render_layers:
        layer.setCurrent()
        geo_dict = get_look_data()
        if geo_dict:
            look_dict_data[layer.name()] = geo_dict
        else:
            return
    return look_dict_data

def generate_oa_file(entity=None):
    if not entity: 
        entity = context_info.ContextPathInfo()
    data_layer = get_dict_look_data()
    oa_texture_dir_path = entity.path.scheme(key='crowdTexturePath').abs_path()
    if data_layer:
        for index, layer in enumerate(data_layer):
            index_ = int(index) + 1
            if index_ == 1:
                layer_first = layer
            layer_dict = data_layer[layer]
            for geo in layer_dict:
                # elephantMaleDefaultHairA_mdl:Cloth_Geo >>> geo_name = Cloth_Geo
                geo_name = geo.split(':')[-1]
                # Cloth_Geo>>>> geoName = Cloth
                geoName = geo_name.split('_')[0]
                namespace = geo.split(':')[0]
                geo_name = namespace.split('_')[0]
                chanel_dict = layer_dict[geo]
                for attr, data_dict in chanel_dict.iteritems():
                    chanel = attr.split('_')[0]
                    # ######### oa_texture_dir_path????????????
                    print data_dict
                    for element, data in data_dict.iteritems():

                        if element == 'fileTextureName':
                            #### data = attr from filetextureName
                            for file_node, data in data.iteritems():
                                file_path_tex = generate_new_file_texture_path([data], geo_name, chanel, geoName, index_)
                                pm.setAttr('{node}.fileTextureName'.format(node = file_node), file_path_tex, type='string')
                                print 'file_path_tex', file_path_tex
                        else:
                            #### data = list_file from filetextureName
                            generate_new_file(data, geo_name, chanel, geoName, index_)
                            # file_utils.copy(path_file, new_path)
        return layer_first
    else:
        return

def get_oa_texture_dir_path():
    from rf_utils.context import context_info
    reload(context_info)
    asset = context_info.ContextPathInfo()
    
    return asset.path.scheme(key='crowdTexturePath').abs_path()

def copy_shade(namespace= ''):# from asset to geo_loco
    namespace_ = namespace
    #geo_loco = pm.ls(type='SetGeometry') ####### ver7
    agent_grp = pm.ls(type='McdAgentGroup')[0]
    pre_fix_loco = agent_grp.split('Agent_')[1]
    if agent_grp:
        for agent in agent_grp.getChildren():
            if 'Geometry_' in agent.name():
                geo_loco = agent
    if geo_loco:
        list_geo_loco = geo_loco.getChildren()
        if list_geo_loco:
            for geo in list_geo_loco:
                print geo.name()
                name_loco = geo.name()
                # prefix = name_loco.split('_')[-1]
                name = name_loco.split('_%s'%pre_fix_loco)[0]
                asset_geo = '%s:%s'%(namespace_, name)
                if pm.objExists(asset_geo):
                    asset_node = pm.PyNode(asset_geo)
                    shading_engine = asset_node.getShape().connections(type='shadingEngine')[0]
                    pm.sets(shading_engine,forceElement=geo)


def generate_new_file( list_file, assetName, chanel, geoName, numbering):
    from rf_utils import file_utils
    oa_texture_dir_path = get_oa_texture_dir_path()
    if not os.path.exists(oa_texture_dir_path):
        os.mkdir(oa_texture_dir_path)
    for path_file in list_file:
        padding = re.findall('[0-9][0-9][0-9][0-9]', path_file)
        ext = os.path.splitext(path_file)[-1]
        if padding:
            new_basename_tex = '{assetName}_{chanel}_{geoName}_{padding}_{number}{ext}'.format(assetName=assetName, chanel=chanel, geoName=geoName, padding=padding[0], number=numbering, ext=ext )
        else:
            new_basename_tex = '{assetName}_{chanel}_{geoName}_{number}{ext}'.format(assetName=assetName, chanel=chanel, geoName=geoName, number= numbering, ext=ext,  )
        new_path = os.path.join(oa_texture_dir_path, new_basename_tex)
        print 'test',new_path
        file_utils.copy(path_file, new_path)
        return new_path

def generate_new_file_texture_path(list_file, assetName, chanel, geoName, numbering):
    from rf_utils import file_utils
    oa_texture_dir_path = get_oa_texture_dir_path()
    for path_file in list_file:
        if not '<UDIM>' in path_file:
            padding = re.findall('[0-9][0-9][0-9][0-9]', path_file)
            ext = os.path.splitext(path_file)[-1]
            if padding:
                new_basename_tex = '{assetName}_{chanel}_{geoName}_{padding}_{number}{ext}'.format(assetName=assetName, chanel=chanel, geoName=geoName, padding=padding[0], number=numbering, ext=ext )
            else:
                new_basename_tex = '{assetName}_{chanel}_{geoName}_{number}{ext}'.format(assetName=assetName, chanel=chanel, geoName=geoName, number= numbering, ext=ext,  )
            new_path = os.path.join(oa_texture_dir_path, new_basename_tex)
            return new_path
        else:
            padding = '<UDIM>'
            ext = os.path.splitext(path_file)[-1]
            if padding:
                new_basename_tex = '{assetName}_{chanel}_{geoName}_{padding}_{number}{ext}'.format(assetName=assetName, chanel=chanel, geoName=geoName, padding=padding, number=numbering, ext=ext )
            else:
                new_basename_tex = '{assetName}_{chanel}_{geoName}_{number}{ext}'.format(assetName=assetName, chanel=chanel, geoName=geoName, number= numbering, ext=ext,  )
            new_path = os.path.join(oa_texture_dir_path, new_basename_tex)
            return new_path
        


def check_udim_pattern(file_tex, node):
    currentFile = file_tex
    uvMode = hook.get_uv_mode(node)
    pattern = hook.pattern( currentFile, uvMode )
    try:
        print '12'
        allFiles = hook.allFiles( pattern )
        print allFiles
        print '34'
    except :
        allFiles = []
        currentFile  = '%s - deiend permission'% node
    return allFiles

def find_untill_file(node):
    connection = pm.listConnections(node,d=False, s=True)
    if not connection:
        return []
    for connect in connection:
        if connect.type() =='file':
            return connect
    return find_untill_file(connection[0])


# {'RedshiftMaterial': 'diffuse_color':  'file_path'
#                      'refl_roughness': 'file_path'}