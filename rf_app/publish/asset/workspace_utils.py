import os 
import sys 
import yaml
from collections import OrderedDict

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
moduleDir = os.path.dirname(sys.modules[__name__].__file__)

from rf_utils import file_utils
from rf_utils.widget import file_widget
from rf_utils import publish_info


def lock_workspace(entity): 
    """ lock workspace files """
    # reg info 
    regInfo = publish_info.RegisterInfo(entity)
    content = regInfo.content.get(entity.version)
    if content: 
        regVersion = content.get(publish_info.Data.regVersion)
        workfile = content.get(publish_info.Data.workfile)

        # file tag 
        fileWidget = file_widget.FileTag(workfile)
        tagWorkFiles = fileWidget.get_tag_files()
        tagFile = file_widget.TagMode.tagname


        workspaceFiles = [a for a in file_utils.listFile(os.path.dirname(workfile)) if not a == tagFile]
        unTaggedFiles = ['%s/%s' % (os.path.dirname(workfile), a) for a in workspaceFiles if not a in tagWorkFiles]
        
        for unTaggedFile in unTaggedFiles: 
            tagFile = file_widget.FileTag(unTaggedFile)
            data = OrderedDict()
            data['version'] = regVersion
            tagFile.set_tag(file_widget.TagMode.disable, tagData=data)
