import sys
import os
from Qt import QtWidgets
from Qt import QtCore
from Qt import QtGui

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler)

moduleDir = os.path.dirname(sys.modules[__name__].__file__)

from rf_utils.widget import note_widget
from rf_utils.widget import file_comboBox


class NoteDialog(QtWidgets.QDialog):

    def __init__(self, sg=None, taskEntity=None, prevSelection=None, parent=None):
        super(NoteDialog, self).__init__(parent=parent)

        self.layout = QtWidgets.QVBoxLayout(self)
        
        # title 
        self.setWindowTitle('Notes')
        
        self.noteWidget = note_widget.NoteWidget(sg, taskEntity, prevSelection)
        self.noteWidget.show_notes()
        self.button = QtWidgets.QDialogButtonBox(QtWidgets.QDialogButtonBox.Ok, QtCore.Qt.Horizontal, self)

        self.layout.addWidget(self.noteWidget)
        self.layout.addWidget(self.button)

        # set layout
        self.layout.setSpacing(0)
        self.layout.setContentsMargins(2, 2, 2, 2)

        # signals 
        self.button.accepted.connect(self.accept)


# usage
# custom_dialog.Dialog.complete('Complete', 'Complete')
