import os
import sys
import traceback
from collections import OrderedDict
from functools import partial 
import subprocess

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_utils import publish_info
from rf_utils.widget import status_widget
from rf_utils.widget import task_widget
from rf_utils.widget import sg_widget
reload(status_widget)
reload(sg_widget)

class Color: 
    bgred = 'background-color: rgb(200, 0, 0);'


class SgWidget(QtWidgets.QWidget):
    """docstring for SgWidget"""
    def __init__(self, sg=None, parent=None, spacer=True):
        super(SgWidget, self).__init__(parent=None)
        self.sg = sg 
        self.spacer = spacer
        self.allLayout = QtWidgets.QHBoxLayout()
        self.subLayout1 = self.set_main_widget()
        # self.subLayout2 = self.set_nextstep_widget()

        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.allLayout.addLayout(self.subLayout1)
        # self.allLayout.addLayout(self.subLayout2)
        # self.allLayout.addItem(spacerItem)


        self.allLayout.setStretch(0, 1)
        self.allLayout.setStretch(1, 3)

        self.allLayout.setSpacing(2)
        self.allLayout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.allLayout)

        self.init_signals()

    def set_main_widget(self): 
        subLayout = QtWidgets.QVBoxLayout()
        
        self.taskWidget = sg_widget.TaskComboBox(self.sg, layout=QtWidgets.QHBoxLayout())
        subLayout.addWidget(self.taskWidget)

        self.statusWidget = status_widget.TaskStatusWidget()
        self.statusWidget.label.setText('Status')
        subLayout.addWidget(self.statusWidget)
        subLayout.setSpacing(10)

        return subLayout

    def set_nextstep_widget(self): 
        """ layout for next step widget """ 
        subLayout = QtWidgets.QVBoxLayout()
        
        self.taskWidget = sg_widget.TaskComboBox(self.sg, layout=QtWidgets.QHBoxLayout())
        subLayout.addWidget(self.taskWidget)

        self.statusWidget = status_widget.TaskStatusWidget()
        self.statusWidget.label.setText('Status')
        subLayout.addWidget(self.statusWidget)

        return subLayout

    def init_signals(self): 
        self.taskWidget.currentIndexChanged.connect(self.task_emit)
        self.statusWidget.currentIndexChanged.connect(self.status_emit)

    def set_task(self, project, entityType, entityName, step): 
        self.taskWidget.activate(project, entityType, entityName, step)

    def nextstep(self, state): 
        """ show / hide next step """ 
        pass 

    def get_task(self): 
        index = self.taskWidget.comboBox.currentIndex()
        return self.taskWidget.comboBox.itemData(index, QtCore.Qt.UserRole)

    def set_current_task(self, taskName): 
        items = self.get_all_items()
        if taskName in items: 
            index = items.index(taskName)
            self.taskWidget.comboBox.setCurrentIndex(index)


    def get_all_items(self): 
        return [self.taskWidget.comboBox.itemText(a) for a in range(self.taskWidget.comboBox.count())]

    def get_status(self): 
        index = self.statusWidget.comboBox.currentIndex()
        return self.statusWidget.comboBox.itemData(index, QtCore.Qt.UserRole)

    def task_emit(self): 
        self.taskWidget.setStyleSheet('')

    def status_emit(self): 
        self.statusWidget.setStyleSheet('')

    def check_info(self): 
        """ check if all info are selected """
        taskColor = Color.bgred if not self.get_task() else ''
        statusColor = Color.bgred if not self.get_status() else ''
        self.taskWidget.setStyleSheet(taskColor)
        self.statusWidget.setStyleSheet(statusColor)

        return not all([taskColor, statusColor])

    def set_alert(self, value): 
        taskColor = Color.bgred if value else ''
        self.taskWidget.setStyleSheet(taskColor)

        
class SgWidgetH(SgWidget):
    """docstring for SgWidget"""
    def __init__(self, sg=None, parent=None, spacer=True):
        super(SgWidgetH, self).__init__(sg=sg, parent=parent, spacer=spacer)
        
    def set_main_widget(self): 
        subLayout = QtWidgets.QHBoxLayout()
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        
        self.taskWidget = sg_widget.TaskComboBox(self.sg, layout=QtWidgets.QHBoxLayout())
        subLayout.addWidget(self.taskWidget)

        self.statusWidget = status_widget.TaskStatusWidget()
        self.statusWidget.label.setText('Status')
        subLayout.addWidget(self.statusWidget)
        subLayout.addItem(spacerItem) if self.spacer else None
        subLayout.setSpacing(10)

        return subLayout



