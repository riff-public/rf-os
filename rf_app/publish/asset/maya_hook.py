import os 
import sys 
from datetime import datetime 
import maya.cmds as mc
import maya.mel as mm


import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

def open_maya_file(filename): 
    return mc.file(filename, o=True, f=True, prompt=False)


def check_file(filename): 
    ext = os.path.splitext(filename)[-1]
    mc.file(new=True, f=True)

    if ext in ['.ma', '.mb']: 
        mc.file(filename, i=True, f=True, prompt=False)

    # gpu 
    if ext in ['.abc']: 
        load_plugin('gpuCache.mll')
        load_plugin('AbcImport.mll')
        if 'gpu' in os.path.basename(filename): 
            import_gpu(filename)

        else: 
            import_abc('tmp', filename)


def load_plugin(pluginName): 
    if not mc.pluginInfo(pluginName, q=True, l=True): 
        mc.loadPlugin(pluginName, qt=True)


def import_abc(obj, path, mode='new'): 
    if mode == 'new' : 
        cmd = 'AbcImport -mode import "%s";' % path

    if mode == 'add' : 
        cmd = 'AbcImport -mode import -connect "%s" "%s";' % (obj, path)

    if mode == 'add_remove' : 
        cmd = 'AbcImport -mode import -connect "%s" -createIfNotFound -removeIfNoUpdate "%s";' % (obj, path)

    if mode == 'merge': 
        cmd = 'AbcImport -mode import -connect "%s" "%s";' % (obj, path)

    mm.eval(cmd)
    return path


def import_gpu(path): 
    gpuNode = mc.createNode('gpuCache')
    mc.setAttr('%s.cacheFileName' % gpuNode, path, type='string')
    return gpuNode


def capture_screen(w, h): 
    sequencer = 0
    filename = datetime.now().strftime("%y-%m-%d_%H-%M-%S")
    outputFile = '%s/tmpCapture/capture_%s' % (os.environ.get('TEMP'), filename)
    currentTime = mc.currentTime(q=True)

    # has to round frame to int
    currentFrame = int(round(currentTime))
    start = currentFrame
    end = currentFrame

    result = mc.playblast( format= 'image' ,
                            filename= outputFile,
                            st=start ,
                            et=end ,
                            forceOverwrite=True ,
                            sequenceTime=sequencer ,
                            clearCache=1 ,
                            viewer=0 ,
                            showOrnaments=1 ,
                            fp=4 ,
                            widthHeight= (w,h) ,
                            percent=100 ,
                            compression= 'png' ,
                            offScreen=True ,
                            quality=70
                            )


    if result : 
        padding = '%04d' % start
        output = result.replace('####', padding)
        if os.path.exists(output): 
            return output

def current_scene(): 
    scene = mc.file(q=True, sn=True) 
    location = mc.file(q=True, loc=True)
    scene = scene if scene else location if not location == 'unknown' else ''
    return scene 


def object_exists(obj): 
    return mc.objExists(obj)


def toggle_group(geo1Name, geo2Name, geo1=False, geo2=False): 
    """ set Geo_Grp as root or Export_Grp as root """
    parent = mc.listRelatives(geo1Name, p=True)
    if geo1: 
        if parent: 
            mc.parent(geo1Name, w=True)
        if mc.objExists(geo2Name): 
            childs = mc.listRelatives(geo2Name)
            if not childs: 
                mc.delete(geo2Name)

    if geo2: 
        if not mc.objExists(geo2Name): 
            geo2Name = mc.group(n=geo2Name, em=True)
        if parent: 
            if not parent[0] == geo2Name: 
                mc.parent(geo1Name, geo2Name)
        else: 
            mc.parent(geo1Name, geo2Name)


def group_objects(): 
    groupName = 'combine_Grp'
    objGroup = ''
    pos = None
    sels = mc.ls(sl=True)
    if sels: 
        tmpLoc = mc.spaceLocator()[0]
        objGroup = sels[0]
        if len(sels) > 1: 
            objGroup = mc.group(sels, n=groupName)

        mc.delete(mc.parentConstraint(objGroup, tmpLoc))
        pos = mc.xform(tmpLoc, q=True, ws=True, m=True)
        mc.delete(tmpLoc)

    return objGroup, pos

def center_grid(obj): 
    zeroLoc = 'zeroPos_loc'
    # del if exists 
    mc.delete(zeroLoc) if mc.objExists(zeroLoc) else None
    
    centerLocator = mc.spaceLocator(n=zeroLoc)[0]
    mc.delete(mc.parentConstraint(centerLocator, obj))
    mc.delete(centerLocator)

def freeze_transform(obj): 
    return mc.makeIdentity(obj, apply=True, t=1, r=1, s=1, n=0, pn=1)
    
def select(obj): 
    mc.select(obj)

def remove_object(obj): 
    mc.delete(obj) if mc.objExists(obj) else None

def ls_selection(l=False): 
    return mc.ls(sl=True, l=l)

def set_parent(child, parent): 
    currentParent = mc.listRelatives(child, p=True)
    if currentParent: 
        if str(parent) == str(currentParent[0]): 
            return 
    if len(child) > 1: 
        return mc.parent(child, parent)
    if len(child) == 1: 
        return mc.parent(child[0], parent)

