import os
import sys

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_utils.sg import sg_process
sg = sg_process.sg
# these statuses mean task not start yet
notStartTasks = ['wtg', 'rdy', 'hld']

# check dependency of each task 
# ex. when model finish, rig start and modelUv start 
# we will check rf_config/project/asset_task_config.yml to check dependency 
# use step, task name and status = 'apr' to check events 
# ex. step=model task=model status=apr
# looking for any step and task that match status='apr'

def get_info(contextPathInfo, step, task): 
	return []
	# """ get dependency for given step and task """ 
	# # use step and task name to check 
	# triggerList = []
	# entity = contextPathInfo
	# # get config by project 
	# taskDict = entity.projectInfo.task_data()
	# depInfo = taskDict['asset']['directTrigger']

	# for info in depInfo:
	# 	events = info['events']
	# 	triggers = info['trigger']

	# 	# if step and task match any events list
	# 	matchCondition = any(a['step'].lower() == step.lower() and a['task'] == task and a['new_status'] == 'apr' for a in info['events'])
	# 	if matchCondition: 
	# 		for trigger in triggers: 
	# 			status = trigger['new_status']

	# 			if status == 'rdy': 
	# 				triggerSep = trigger['step']
	# 				triggerTask = trigger['task']
	# 				triggerList.append([triggerSep, triggerTask])

	# return triggerList

# check affected task mean checking for task that already started or finished. 
# any task that's not 'wtg' or 'rdy' or 'hld' mean already started or finished 

def check_affected(contextPathInfo, step, task): 
	""" get affected tasks """ 
	taskEntities = get_dependency(contextPathInfo, step, task)
	# any status that's not in notStartTasks will be affected 
	affectedTasks = [a for a in taskEntities if a['sg_status_list'] not in notStartTasks]
	return affectedTasks

def get_dependency(contextPathInfo, step, task): 
	""" get tasks dependency """ 
	dependencyList = get_info(contextPathInfo, step, task)

	# get all tasks 
	entity = get_entity(contextPathInfo)
	if entity: 
		taskEntities = get_tasks(entity.get('project'), entity)

		# narrow down tasks that match criteria 
		forcusTasks = [task for step, task in dependencyList]
		return [a for a in taskEntities if a['content'] in forcusTasks]
	return []


def get_entity(contextPathInfo): 
	""" get shotgun asset entity """ 
	filters = [['project.Project.name', 'is', contextPathInfo.project], ['code', 'is', contextPathInfo.name]]
	fields = ['code', 'id', 'project']
	entity = sg.find_one('Asset', filters, fields)
	return entity


def get_tasks(projectEntity, entity): 
	""" get shotgun tasks entities """ 
	filters = [['entity', 'is', entity], ['project', 'is', projectEntity]]
	fields = ['sg_status_list', 'content', 'step', 'task_assignees']
	taskEntities = sg.find('Task', filters, fields)
	return taskEntities



