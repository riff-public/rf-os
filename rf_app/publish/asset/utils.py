import os
import sys
import shutil
from rf_utils.widget import file_widget
from rf_utils import admin

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)
icon_dir = '%s/icons' % module_dir

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

class IconExt:
	value = {'.ma': '%s/Maya.png' % icon_dir,
			'.mb': '%s/Maya.png' % icon_dir,
			'.abc': '%s/Maya.png' % icon_dir
			}

def get_icon(filename):
	ext = os.path.splitext(filename)[-1]
	iconPath = IconExt.value.get(ext)
	return iconPath


def collect_imgs(dstDir, filename, imgs):
	filename, ext = os.path.splitext(filename)
	outputs = []
	remove_previous_hero(dstDir, filename)

	for i, img in enumerate(imgs):
		seqName = '%s.%04d%s' % (filename, i+1, ext)
		dst = '%s/%s' % (dstDir, seqName)
		if not os.path.exists(dstDir):
			os.makedirs(dstDir)
		shutil.copy2(img, dst)

		if os.path.exists(dst):
			outputs.append(dst)

	return (True, outputs) if len(imgs) == len(outputs) else (False, outputs)


def remove_previous_hero(dirname, name): 
	""" remove any name in the directory that match this name 
	e.g. testProp2_model_md.hero -> 
	remove [testProp2_model_md.hero.0001.png, testProp2_model_md.hero.0002.png]
	""" 

	logger.debug('Removing old heroes ...')
	if os.path.exists(dirname): 
		files = [d for d in os.listdir(dirname) if os.path.isfile(os.path.join(dirname, d))]
		files_remove = ['{}/{}'.format(dirname, a) for a in files if name in a]
		results = list()

		for file in files_remove: 
			result = admin.remove(file)
			results.append(result)
			if not result: 
				logger.warning('Failed to remove {}'.format(file))

		if all(results) and len(results) == len(files_remove): 
			logger.debug('All previous files had been removed')
			return True 
		else: 
			logger.warning('Not all file removed')
			return False


def copy(src, dst):
	if not os.path.exists(os.path.dirname(dst)):
		os.makedirs(os.path.dirname(dst))
	return shutil.copy2(src, dst)
