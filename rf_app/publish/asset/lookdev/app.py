_UI = 'LookdevSetManager'

import sys
import os 
from Qt import QtWidgets
from Qt import QtCore
from rf_utils.ui import load
from rftool.utils.ui import maya_win
from rf_app.asm import asm_lib
from rf_utils.context import context_info


_DIRNAME = os.path.dirname(__file__)
ui_class, base_class = load.loadUiType("{}/ui.ui".format(_DIRNAME))


class App(base_class):
    """docstring for App"""
    def __init__(self, parent=None):
        super(App, self).__init__(parent)
        self.ui = ui_class()
        self.ui.setupUi(self)
        self.setup_ui()
        self.init_signals()
        self.init_functions()

    def setup_ui(self): 
        self.ui.asset_treewidget.setColumnWidth(0, 200)

    def init_functions(self): 
        self.load_set()
        # self.load_asset()

    def init_signals(self): 
        self.ui.load_set_bn.clicked.connect(self.load_set)      
        self.ui.set_combo.currentIndexChanged.connect(self.load_asset)

    def load_set(self): 
        sets = asm_lib.list_set()
        self.ui.set_combo.clear()
        self.ui.set_combo.addItems(sets)

    def load_asset(self): 
        current_set = self.ui.set_combo.currentText()
        root = asm_lib.Asm(current_set)
        assets = root.list_assets()
        members = root.list_asm()
        asset_dict = dict()
        
        self.ui.asset_treewidget.clear()
        self.asset_root = self.ui.asset_treewidget.invisibleRootItem()

        for member in members: 
            asm = asm_lib.MayaRepresentation(member)
            data_path = asm.asset_description()
            asset = context_info.ContextPathInfo(path=data_path)
            if not asset.name in asset_dict.keys(): 
                asset_dict[asset.name] = {'context': asset, 'data': data_path, 'members': [asm.shortname]}
            else: 
                asset_dict[asset.name]['members'].append(asm.shortname)


        for asset in sorted(asset_dict.keys()): 
            item = QtWidgets.QTreeWidgetItem(self.asset_root)
            item.setText(0, asset)
            item.setData(0, QtCore.Qt.UserRole, asset_dict[asset]['context'])
            build_bn = QtWidgets.QPushButton('Build')
            self.ui.asset_treewidget.setItemWidget(item, 1, build_bn)

            members = asset_dict[asset]['members']
            for member in members: 
                subitem = QtWidgets.QTreeWidgetItem(item)
                subitem.setText(0, member)


    def read_set(self): 
        sets = asm_lib.list_set()
        return sets



def show(): 
    maya_win.deleteUI(_UI)
    app = App(maya_win.getMayaWindow())
    app.show()
