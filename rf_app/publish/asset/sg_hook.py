import os
import sys

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_utils.sg import sg_process
# reload(sg_process)
from rf_utils.pipeline import watermark
# reload(watermark)
from rf_utils import custom_exception

try: 
    from rf_utils.pipeline.notification import noti_module_cliq
except: 
    pass

def publish_version(ContextPathInfo, taskEntity, status, imgs, userEntity, description, uploadOption='default', localMedia=''):
    versionName, ext = os.path.splitext(ContextPathInfo.publish_name())
    projectEntity = taskEntity.get('project')
    entity = taskEntity.get('entity')
    playlistEntity = sg_process.daily_playlist(ContextPathInfo.project, ContextPathInfo.step)
    wPlaylistEntity = sg_process.weekly_playlist(ContextPathInfo.project, ContextPathInfo.step)
    logger.info('Create playlist')

    # recommended file
    recommendedFileName = ContextPathInfo.recommended_file(step=ContextPathInfo.step, hero=True)
    publishOutput = ContextPathInfo.path.output_hero()
    recommendedFile = '%s/%s' % (publishOutput, recommendedFileName)

    # data
    data = {'res': str(ContextPathInfo.res)}

    # sg
    versionEntity = sg_process.update_version(projectEntity,
                                            entity,
                                            taskEntity,
                                            userEntity,
                                            versionName,
                                            status,
                                            description,
                                            [playlistEntity, wPlaylistEntity],
                                            versionType='Published',
                                            step=ContextPathInfo.step,
                                            workspaceFile=ContextPathInfo.path.path(),
                                            publishFile=ContextPathInfo.path.publish_version(),
                                            heroFile=ContextPathInfo.path.publish_hero(),
                                            recommendedFile=recommendedFile,
                                            data=str(data))
    logger.info('Update version')

    media = imgs[0] if imgs else ''
    if media:

        proxyMov = None
        if is_mov(imgs):
            vid = imgs[0]
            rvformat = vid
            if uploadOption == 'default':
                media = proxy_mov(vid)

            if uploadOption == 'downsize':
                media = proxy_mov(vid)

            if uploadOption == 'frame':
                media = mov_frame(vid, 1)

        else:
            rvformat = rv_format(imgs)
            if uploadOption == 'default':
                media = combine_img_mov(imgs)

        print('Uploading ... {0}'.format(imgs))
        rvformat = rvformat if not localMedia else localMedia

        # add watermark to media
        watermarked_media, watermark_status = watermark.add_watermark_to_media(entity=ContextPathInfo, input_path=media)
        # if watermark added successfully, use it to upload instead
        if watermark_status:
            proxyMov = watermarked_media

        # upload the media to SG
        try:
            result = sg_process.upload_version_media(versionEntity, media, sequences=rvformat, proxy=proxyMov)
            print('Upload finish')
        except sg_process.error as e:
            # *** call bot to notify user of the error
            versionEntity['task_assignees'] = [{'name':str(userEntity['name'])}]
            result= [versionEntity]
            emails = noti_module_cliq.get_notification_data(result, versionEntity, description, userEntity)
            noti_module_cliq.send_failed_upload_notification(emails)
            # raise pipelineError
            raise custom_exception.PipelineError('Upload media failed.')

        # delete temp watermark if succeed
        if watermark_status:
            try:
                os.remove(watermarked_media)
            except Exception as e:
                logger.error('Failed to delete temp file: %s' %watermarked_media)
                logger.error(e)

        logger.info('Upload media')
        # update task status 
        print('Sync task status -------------------')
        taskEntity = versionEntity.get('sg_task')
        if not status == 'wip':
            # if not taskEntity['name'] == 'anim': 
            taskEntity = sg_process.set_task_status(taskEntity['id'], status)
        print('Finished')

    allTasks = sg_process.get_tasks(entity)
    aprCheck = True
    for t in allTasks:
        status = t['sg_status_list']
        if status != 'apr' and status != 'omt':
            aprCheck = False
    if aprCheck:
        setStatus = 'apr'
        mainStatus = sg_process.set_entity_status(entity['type'], entity['id'], setStatus)
        #entity['type'] = 'Asset'
    else:
        setStatus = 'ip'
        mainStatus = sg_process.set_entity_status(entity['type'], entity['id'], setStatus)

    return versionEntity


def publish_daily(ContextPathInfo, taskEntity, status, imgs, userEntity, description, playlistEntities=[]):
    versionName, ext = os.path.splitext(ContextPathInfo.daily_name(version='subversion'))
    projectEntity = taskEntity.get('project')
    entity = taskEntity.get('entity')
    playlistEntity = sg_process.daily_playlist(ContextPathInfo.project, ContextPathInfo.step)
    wPlaylistEntity = sg_process.weekly_playlist(ContextPathInfo.project, ContextPathInfo.step)

    versionEntity = sg_process.update_version(projectEntity, entity, taskEntity, userEntity, versionName, status, description, [playlistEntity, wPlaylistEntity], versionType='Daily', step=ContextPathInfo.step, workspaceFile=ContextPathInfo.path.path())

    if playlistEntities:
        playlists = versionEntity['playlists']
        playlists += playlistEntities
        sg_process.update_playlist(versionEntity['id'], playlists)

    if is_mov(imgs):
        rvformat = None
        proxyMov = proxy_mov(imgs[0]) # updload this
        media = imgs[0] # embeded link
    else:
        # sequences
        rvformat = rv_format(imgs) # embeded link
        media = combine_img_mov(imgs) # updload this
        proxyMov = media

    # add watermark to media
    watermarked_media, watermark_status = watermark.add_watermark_to_media(entity=ContextPathInfo, input_path=proxyMov)
    # if watermark added successfully, use it to upload instead
    if watermark_status:
        proxyMov = watermarked_media

    result = sg_process.upload_version_media(versionEntity, media, sequences=rvformat, proxy=proxyMov)
    
    # delete temp watermark if succeed
    if watermark_status:
        try:
            os.remove(watermarked_media)
        except Exception as e:
            logger.error('Failed to delete temp file: %s' %watermarked_media)
            logger.error(e)
            
    return result


def combine_img_mov(images):
    from rf_utils.pipeline import convert_lib
    from datetime import datetime
    date = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")

    if len(images) == 1:
        return images[0]

    srcFile = images[0]
    basename = os.path.basename(srcFile)
    name, ext = os.path.splitext(basename)
    dstDir = '%s/rf_tmp' % os.environ['TEMP']
    dst = '%s/%s_combne_%s.mov' % (dstDir, name, date)

    os.makedirs(dstDir) if not os.path.exists(dstDir) else None

    # ** try use new function to convert image with difference size to mov
    convert_lib.img_varysize_to_mov(images, dst)
    # convert_lib.img_to_mov2(images, dst)
    return dst

def is_mov(images):
    filename, ext = os.path.splitext(images[0])
    return True if ext in ['.mov', '.mp4'] else False


def proxy_mov(srcFile):
    from rf_utils.pipeline import convert_lib
    filesize = os.path.getsize(srcFile)

    # if filesize is more than 50MB, resize mov
    if filesize > 50000000:
        name, ext = os.path.splitext(srcFile)
        resizeDst = '%s_resize%s' % (name, ext)

        if ext in ['.mp4', '.mov']:
            if os.path.exists(resizeDst):
                os.remove(resizeDst)
            return convert_lib.compress_mov(srcFile, resizeDst)
    else:
        return srcFile

def mov_frame(srcFile, frame=1):
    from rf_utils.pipeline import convert_lib
    basename = os.path.basename(srcFile)
    name, ext = os.path.splitext(basename)
    dst = '%s/rf_tmp/%s_still.png' % (os.environ['TEMP'], name)

    if ext in ['.mp4', '.mov']:
        if os.path.exists(dst):
            os.remove(dst)
        return convert_lib.mov_frame(srcFile, dst)

def update_task(ContextPathInfo, taskEntity, status):
    display = 'hero'
    recheckStatus = ['apr', 'rev']
    # if status already be 'apr', change to 'fix' first
    currentStatus = taskEntity.get('sg_status_list')
    # for checkStatus in recheckStatus:
    #     if status == checkStatus and currentStatus == checkStatus:
    #         sg_process.set_task_status(taskEntity.get('id'), 'fix')
    #         logger.debug('set task to "fix" because old status was "apr"')

    heroPath = ContextPathInfo.path.output_hero().abs_path().replace('/', '\\')
    result = sg_process.update_task(taskEntity.get('id'), status, display, heroPath)
    return result

# def update_dependency_event(eventList, taskEntity, status):
#     step = taskEntity.get('step').get('name')
#     taskname = taskEntity.get('content')
#     triggers = [a['trigger'] for a in eventList if a['event']['step'] == step and a['event']['task'] == taskname and a['event']['status'] == status]
#     results = []
#     # update
#     if triggers:
#         for trigger in triggers[0]:
#             triggerStep = trigger['step']
#             triggerTaskName = trigger['task']
#             triggerStatus = trigger['status']

#             if triggerTaskName == '*':
#                 triggerTasks = sg_process.get_tasks_by_step(taskEntity['entity'], triggerStep)
#             else:
#                 triggerTasks = [sg_process.get_one_task_by_step(taskEntity['entity'], triggerStep, triggerTaskName)]

#             for triggerTask in triggerTasks:
#                 if triggerTask:
#                     result = sg_process.set_task_status(triggerTask['id'], triggerStatus)
#                     logger.debug('trigger %s %s %s' % (triggerStep, triggerTaskName, triggerStatus))
#                     results.append(result)

#     return results

def update_dependency(ContextPathInfo, taskEntity, status):
    entity = ContextPathInfo
    eventList = entity.projectInfo.task.dependency(entity.entity_type)
    step = taskEntity.get('step').get('name')
    taskname = taskEntity.get('content')
    # triggers = [a['trigger'] for a in eventList if a['event']['step'] == step and a['event']['task'] == taskname and a['event']['status'] == status]
    triggers = []
    for i, a  in enumerate(eventList):
        for event in a['events']:
                if event['step'] == step and event['task'] == taskname and event['new_status'] == status:
                    triggers.append(a['trigger'])

    #triggers = [a['trigger'] for a in eventList if a['events']['step'] == step and a['events']['task'] == taskname and a['events']['new_status'] == status]
    results = []
    # update
    if triggers:
        for trigger in triggers[0]:
            triggerStep = trigger['step']
            triggerTaskName = trigger['task']
            # triggerStatus = trigger['status']
            triggerStatus = trigger['new_status']
            if triggerTaskName == '*':
                triggerTasks = sg_process.get_tasks_by_step(taskEntity['entity'], triggerStep)
            else:
                triggerTasks = [sg_process.get_one_task_by_step(taskEntity['entity'], triggerStep, triggerTaskName)]
            for triggerTask in triggerTasks:
                if triggerTask:
                    if triggerTask['sg_status_list'] in trigger['old_status']:
                        result = sg_process.set_task_status(triggerTask['id'], triggerStatus)
                        logger.debug('trigger %s %s %s' % (triggerStep, triggerTaskName, triggerStatus))
                        results.append(result)

    return results

# def look_dependency(ContextPathInfo, taskEntity, status):
#     """ texture triggered lookdev with the same sg_name """
#     entity = ContextPathInfo
#     eventList = entity.projectInfo.task.look_dependency()
#     step = taskEntity.get('step').get('name')
#     triggers = [a['trigger'] for a in eventList if a['event']['step'] == step and a['event']['status'] == status]
#     eventAttr = [a['event']['attr'] for a in eventList if a['event']['step'] == step and a['event']['status'] == status]
#     lookname = taskEntity[eventAttr[0]] if eventAttr else ''

#     if triggers and lookname:
#         assetTasks = sg_process.get_tasks(taskEntity['entity'])
#         for trigger in triggers:
#             triggerStep = trigger['step']
#             matchAttr = trigger['attr']
#             triggerStatus = trigger['status']

#             triggeredTasks = [a for a in assetTasks if a[matchAttr] == lookname and a['step']['name'] == triggerStep]
#             for triggeredTask in triggeredTasks:
#                 if triggeredTask:
#                     result = sg_process.set_task_status(triggeredTask['id'], triggerStatus)
#                     logger.debug('trigger %s %s %s' % (triggerStep, triggeredTask['content'], triggerStatus))


def complete_notes(noteEntities, versionEntity):
    results = []
    if noteEntities:
        for noteEntity in noteEntities:
            data = {'sg_resolve_version': versionEntity,
                    'sg_status_list': 'cmpt'}
            result = sg_process.sg.update('Note', noteEntity['id'], data)
            results.append(result)

        return results

def rv_format(imgs):
    basename = os.path.basename(imgs[0])
    dirname = os.path.dirname(imgs[0])
    elems = basename.split('.')
    padding = [a for a in elems if a.isdigit()]
    rvname = basename.replace(padding[0], '#') if padding else basename
    return '%s/%s' % (dirname, rvname)


def find_task(project, assetName, taskName):
    filters = [['project.Project.name', 'is', project], ['entity.Asset.code', 'is', assetName], ['content', 'is', taskName]]
    fields = ['content', 'entity', 'project', 'step']
    return sg_process.sg.find_one('Task', filters, fields)
