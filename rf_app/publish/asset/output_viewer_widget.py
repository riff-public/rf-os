import os
import sys
import traceback
from collections import OrderedDict
from functools import partial 
import subprocess

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_utils import publish_info
import utils 
import maya_hook

class LocalConfig: 
    iconSize = 60 

class OutputViewBaseUI(QtWidgets.QWidget) :

    def __init__(self, ContextPathInfo=None, parent=None) :
        super(OutputViewBaseUI, self).__init__(parent)
        self.allLayout = QtWidgets.QVBoxLayout()
        
        # display listWidget 
        # spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.resLayout = QtWidgets.QHBoxLayout()
        self.resLabel = QtWidgets.QLabel('Res')
        self.resComboBox = QtWidgets.QComboBox()
        # self.resLayout.addItem(spacerItem)
        self.resLayout.addWidget(self.resLabel)
        self.resLayout.addWidget(self.resComboBox)

        # lable 
        self.versionLayout = QtWidgets.QHBoxLayout()
        self.versionLabel = QtWidgets.QLabel('Work Version')
        self.versionComboBox = QtWidgets.QComboBox()
        self.versionLayout.addWidget(self.versionLabel)
        self.versionLayout.addWidget(self.versionComboBox)
        
        self.regVersionLayout = QtWidgets.QHBoxLayout()
        self.regVersionLabel = QtWidgets.QLabel('Register version')
        self.regVersionDisplay = QtWidgets.QComboBox()
        self.regVersionDisplay.setEnabled(False)
        self.regVersionLayout.addWidget(self.regVersionLabel)
        self.regVersionLayout.addWidget(self.regVersionDisplay)

        self.showAllCheckBox = QtWidgets.QCheckBox('Show All')

        self.outputListWidget = QtWidgets.QListWidget()
        self.outputListWidget.setViewMode(QtWidgets.QListView.IconMode)

        # export pushButton
        self.checkButton = QtWidgets.QPushButton('Check Files')
        self.reopenButton = QtWidgets.QPushButton('Re-open workspace file')
        
        self.outputListWidget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)


        # add to layout
        self.allLayout.addLayout(self.resLayout)
        self.allLayout.addLayout(self.versionLayout)
        self.allLayout.addLayout(self.regVersionLayout)

        self.allLayout.addWidget(self.outputListWidget)
        self.allLayout.addWidget(self.showAllCheckBox)
        self.allLayout.addWidget(self.checkButton)
        self.allLayout.addWidget(self.reopenButton)
        self.allLayout.setSpacing(8)
        self.allLayout.setContentsMargins(2, 2, 2, 2)
        self.setLayout(self.allLayout)


        # set display
        # self.exportButton.setText('Export')
        # self.exportButton.setMinimumSize(QtCore.QSize(0, 30))

class OutputViewWidget(OutputViewBaseUI):
    """docstring for OutputViewWidget"""
    def __init__(self, ContextPathInfo=None, parent=None):
        super(OutputViewWidget, self).__init__(ContextPathInfo, parent)
        self.entity = ContextPathInfo

        self.init_signals()

    def init_signals(self): 
        self.resComboBox.currentIndexChanged.connect(self.res_signal)
        self.versionComboBox.currentIndexChanged.connect(self.version_signal)
        self.showAllCheckBox.stateChanged.connect(self.checkbox_signal)
        
        # button 
        self.reopenButton.clicked.connect(self.open_workfile)
        self.checkButton.clicked.connect(self.checkfile)


    def res_signal(self, index): 
        res = str(self.resComboBox.currentText())
        self.publishInfo.entity.context.update(res=res)
        self.list_register_version()

    def version_signal(self, index): 
        self.add_output_list(self.version)
        self.add_reg_version(self.publish_version)
        self.publishInfo.entity.context.update(publishVersion=self.publish_version)

    def add_reg_version(self, version): 
        self.regVersionDisplay.clear()
        self.regVersionDisplay.addItem(version)

    @property
    def version(self): 
        return str(self.versionComboBox.currentText())

    @property
    def publish_version(self): 
        return self.publish_data.get(publish_info.Data.regVersion)


    def checkbox_signal(self, value): 
        state = True if value else False 
        version = str(self.versionComboBox.currentText())
        self.add_output_list(version, not state)

    def open_workfile(self): 
        data = self.publish_data
        workfile = data.get(publish_info.Data.workfile)
        return maya_hook.open_maya_file(workfile)

    def checkfile(self): 
        currentItem = self.outputListWidget.currentItem()
        if currentItem: 
            path = currentItem.data(QtCore.Qt.UserRole)
            maya_hook.check_file(path)
        else: 
            logger.warning('Select a file to check')

    @property
    def publish_data(self): 
        version = str(self.versionComboBox.currentText())
        data = self.publishInfo.content.get(version)
        return data 

    def publish_path(self): 
        return self.publish_data.get(publish_info.Data.publishVersion)

    def output_img(self): 
        return self.publish_data.get(publish_info.Data.outputImg)

    def update(self): 
        self.list_res()

    def list_register_version(self): 
        versions = self.publishInfo.check_publish_version(publish=False)
        self.versionComboBox.clear()
        self.versionComboBox.addItems(versions[::-1])

    def list_output(self, version): 
        data = self.publishInfo.content.get(version)
        outputList = data.get(publish_info.Data.outputList)
        return outputList

    def add_output_list(self, version, filter=True): 
        outputList = self.list_output(version)
        self.outputListWidget.clear()
        # list only hero 
        heroPath = self.entity.path.hero().abs_path()
        
        outputList = [a for a in outputList if heroPath in a] if filter else outputList
        self.add_output_items(outputList)


    def list_res(self): 
        if self.entity: 
            self.publishInfo = publish_info.RegisterInfo(self.entity)
            res = self.publishInfo.list_res_files()
            self.resComboBox.clear()
            self.resComboBox.addItems(res)

    def add_output_items(self, paths): 
        for path in paths: 
            item = QtWidgets.QListWidgetItem(self.outputListWidget)
            item.setText(os.path.basename(path))
            item.setData(QtCore.Qt.UserRole, path)

            iconPath = utils.get_icon(path)
            iconWidget = QtGui.QIcon()
            iconWidget.addPixmap(QtGui.QPixmap(iconPath),QtGui.QIcon.Normal,QtGui.QIcon.Off)
            item.setIcon(iconWidget)

            self.outputListWidget.setIconSize(QtCore.QSize(LocalConfig.iconSize, LocalConfig.iconSize))

            # self.publishInfo.unpublish_version_content()

        


