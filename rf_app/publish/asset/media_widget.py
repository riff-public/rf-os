import os
import sys
import traceback
from collections import OrderedDict
from functools import partial
import subprocess

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_utils import publish_info
from rf_utils.widget import display_widget
from rf_utils.widget import filmstrip_widget
from rf_utils.widget import snap_widget
# reload(display_widget)
# reload(filmstrip_widget)
# reload(snap_widget)
import utils

class Color:
    bgred = 'background-color: rgb(200, 0, 0);'

class LocalConfig:
    iconSize = 60
    snapIcon = '%s/icons/snap.png' % module_dir

class MediaWidgetUI(QtWidgets.QWidget) :
    def __init__(self, isMaya=None, parent=None) :
        super(MediaWidgetUI, self).__init__(parent)
        self.allLayout = QtWidgets.QHBoxLayout()
        self.isMaya = isMaya
        self.create_layout()
        self.set_layout()

    def set_layout(self):
        self.preview_widget()
        self.reel_widget()
        self.button_widget()
        self.add_widgets()
        self.set_stretch()

    def create_layout(self):
        self.reelLayout = QtWidgets.QVBoxLayout()
        self.previewLayout = QtWidgets.QVBoxLayout()

    def preview_widget(self):
        # preview widget
        self.previewWidget = display_widget.Display()
        self.previewWidget.set_label('Media Here')
        self.previewWidget.display.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.set_bgcolor([60, 60, 60])

    def reel_widget(self):
        # reel preview widget
        self.reelWidget = filmstrip_widget.DisplayReel()

    def button_widget(self):
        self.snapButton = snap_widget.SnapButton()
        self.snapButton.button.setMinimumSize(QtCore.QSize(0, 40))
        self.snapButton.set_icon(LocalConfig.snapIcon)

        self.captureSize = {'w': 1280, 'h': 1024}

        if self.isMaya:
            self.mayaCheckbox = QtWidgets.QCheckBox('Maya Snap')
            self.mayaCheckbox.stateChanged.connect(self.override_capture)
            self.mayaCheckbox.setChecked(True)

    def add_widgets(self):
        self.previewLayout.addWidget(self.previewWidget)
        self.reelLayout.addWidget(self.reelWidget)

        self.allLayout.addLayout(self.previewLayout)
        self.allLayout.addLayout(self.reelLayout)
        self.reelLayout.addWidget(self.snapButton)

        if self.isMaya:
            self.reelLayout.addWidget(self.mayaCheckbox)


    def set_stretch(self):
        self.reelLayout.setStretch(0, 6)
        self.reelLayout.setStretch(1, 1)
        self.reelLayout.setSpacing(0)

        self.allLayout.setStretch(0, 6)
        self.allLayout.setStretch(1, 1)
        self.allLayout.setSpacing(8)
        self.allLayout.setContentsMargins(2, 2, 2, 2)
        self.setLayout(self.allLayout)


class MediaWidget(MediaWidgetUI):
    """docstring for MediaWidget"""
    def __init__(self, isMaya=None, parent=None):
        super(MediaWidget, self).__init__(isMaya=isMaya, parent=parent)
        self.init_signals()
        self.parentUI = None

    def override_capture(self, value):
        import maya_hook
        state = True if value else False
        override = maya_hook.capture_screen if state else None
        self.snapButton.captureCommand = override
        self.snapButton.w = self.captureSize['w']
        self.snapButton.h = self.captureSize['h']

    def init_signals(self):
        self.previewWidget.multipleDropped.connect(self.dropped_signal)
        self.reelWidget.clicked.connect(self.set_preview)
        self.snapButton.precaptured.connect(self.pre_capture)
        self.snapButton.captured.connect(self.capture)

    def dropped_signal(self, paths):
        """ when user drop images """
        self.reelWidget.add_items(paths)

    def set_preview(self, path):
        """ set preview """
        if path:
            self.previewWidget.set_display(path)
        else:
            self.previewWidget.clear()

    def pre_capture(self, state):
        """ precapture signal """
        if state and self.parentUI:
            self.parentUI.hide()

    def capture(self, path):
        """ capture signal """
        self.previewWidget.call_back([path])
        self.snapButton.setStyleSheet('')

        if self.parentUI:
            self.parentUI.show()

    def set_bgcolor(self, color):
        """ set bg color """
        self.previewWidget.display.setStyleSheet('background-color: rgb(%s, %s, %s);' % (color[0], color[1], color[2]))

    def get_all_items(self):
        """ get all item in film strip """
        return self.reelWidget.get_all_items()

    def check_complete(self):
        imgs = self.get_all_items()
        color = Color.bgred if not imgs else ''
        self.snapButton.setStyleSheet(color)
        return True if imgs else False


class MediaWidget2(MediaWidget):
    """docstring for MediaWidget2"""
    def __init__(self, isMaya=None, parent=None):
        super(MediaWidget2, self).__init__(isMaya=isMaya, parent=parent)
        self.override_size()

    def override_size(self):
        self.snapButton.button.setMinimumSize(QtCore.QSize(0, 30))

    def add_widgets(self):
        # self.previewLabel = QtWidgets.QLabel('Screen Shot')
        # self.previewLayout.addWidget(self.previewLabel)
        self.previewLayout.addWidget(self.previewWidget)
        self.previewLayout.addWidget(self.snapButton)

        self.reelLayout.addWidget(self.reelWidget)

        self.allLayout.addLayout(self.previewLayout)
        self.allLayout.addLayout(self.reelLayout)
        reelSpacer = QtWidgets.QSpacerItem(10, 20, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.reelLayout.addItem(reelSpacer)

        if self.isMaya:
            self.reelLayout.addWidget(self.mayaCheckbox)

    def set_stretch(self):
        self.reelLayout.setStretch(0, 8)
        self.reelLayout.setStretch(1, 0)
        self.reelLayout.setStretch(2, 0)
        self.reelLayout.setSpacing(0)

        # self.previewLayout.setStretch(0, 0)
        self.previewLayout.setStretch(0, 1)
        self.previewLayout.setStretch(1, 0)

        self.allLayout.setStretch(0, 6)
        self.allLayout.setStretch(1, 1)

        self.allLayout.setSpacing(8)
        self.allLayout.setContentsMargins(2, 2, 2, 2)
        self.setLayout(self.allLayout)




