#!/usr/bin/env python
# -- coding: utf-8 --
# v.0.0.1 polytag switcher
# v.1.0.0 add start/due date to capture image burn-in
# v.1.1.0 - border HUD  for video
#         - support multiple drop
#         - add play icon to indicate a video file (not playable)
# v.1.1.1 - Fix bug with screen snap

_title = 'Asset Publish Tool'
_version = 'v.1.1.1'
_des = 'wip'
uiName = 'ExportTool'

#Import python modules
import sys
import os
import getpass
import logging
from datetime import datetime
from functools import partial
import time

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

# import config
import rf_config as config

# framework modules
from rf_utils import log_utils
from rf_utils.ui import load
from rf_utils.ui import stylesheet

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

# import qc widget
from rf_qc import qc_widget
from rf_app.export import output_widget
from rf_utils.widget import entity_browse_widget
# reload(entity_browse_widget)
from rf_utils.widget import project_widget
from rf_utils.widget import file_comboBox
from rf_utils.widget import user_widget
from rf_utils.widget import file_widget
from rf_utils.widget import task_dependency_widget
from rf_utils.widget import note_widget
from rf_utils import icon
from rf_utils.context import context_info
from rf_utils import project_info
from rf_utils import publish_info
from rf_utils import register_entity
from rf_utils import file_utils
from rf_app.publish.asset import media_widget
# reload(media_widget)
from rf_app.publish.asset import sginfo_widget
from rf_utils.widget import dialog
from rf_utils.sg import sg_utils
from rf_utils import daily
from rf_utils.pipeline.notification import noti
from rf_utils.pipeline.notification import noti_module_cliq
from rf_utils.pipeline import watermark
# reload(watermark)
from rf_utils import user_info
import utils
import sg_hook
import workspace_utils
import note_dialog
from rf_shotgun_event_daemon.src.plugins import status_update
# reload(status_update)

# reload(task_dependency_widget)
# reload(sginfo_widget)
# reload(sg_hook)
# reload(qc_widget)
# reload(output_widget)
# reload(noti)
# reload(status_update)

# reload(task_dependency_widget)
# reload(sginfo_widget)
# reload(sg_hook)
# reload(qc_widget)
# reload(output_widget)
# reload(noti)
# reload(noti_module_cliq)
# reload(user_info)

from rf_utils.sg import sg_process
# reload(sg_process)

import lucidity

class Color:
    red = 'color: rgb(200, 0, 0);'
    green = 'color: rgb(0, 240, 0);'
    bgGreen = 'background-color: rgb(0, 140, 0);'
    bgYellow = 'background-color: rgb(200, 120, 0);'
    bgRed = 'background-color: rgb(140, 0, 0);'
    bgLightRed = 'background-color: rgb(140, 60, 60);'

class Config: 
    taskSyncStep = ['anim']

class RFAssetPublish(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        #Setup Window
        super(RFAssetPublish, self).__init__(parent)

        # ui read
        uiFile = '%s/ui_core.ui' % moduleDir
        if config.isMaya:
            self.ui = load.setup_ui_maya(uiFile, parent)
        else: 
            self.ui = load.setup_ui(uiFile, parent)
            # hide export area
            self.ui.exportArea_frame.setVisible(False)
            self.ui.setMinimumSize(QtCore.QSize(700, 600))

        # self.ui.show()
        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))

        self.projectSelection = '-- Choose Project --'
        self.exceptionFilter = '_'

        # read config 
        self.appConfig = '{}/config.yml'.format(moduleDir)

        # setup
        self.setup_context()
        self.setup_layout()
        self.init_signals()
        self.init_functions()


    def setup_layout(self):
        self.add_logo()
        self.add_user_widget()
        self.set_top_layout()

        self.add_auto_checkBox()
        self.add_project_widget()
        self.add_navigation_widget()
        self.add_step_widget()
        self.add_process_widget()
        self.add_version_widget()

        self.add_display_widget()

        self.add_confirm_button()
        self.set_navigation_layout()

        self.add_qc_widget()
        self.add_output_widget()
        self.add_console_widget()
        self.add_task_widget()
        self.add_task_dependency_widget()
        self.add_note_widget()
        self.add_snap_widget()
        self.set_snap_layout()
        self.add_publish_button()
        self.set_publish_layout()
        self.add_description_widget()
        

    def add_logo(self):
        # logo
        self.logoLabel = QtWidgets.QLabel()
        self.ui.top_layout.addWidget(self.logoLabel)
        self.logoLabel.setPixmap(QtGui.QPixmap(icon.logo).scaled(64, 64, QtCore.Qt.KeepAspectRatio))

    def add_user_widget(self):
        topSpacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)

        # user
        self.userWidget = user_widget.UserComboBox()
        self.ui.top_layout.addItem(topSpacerItem)
        self.ui.top_layout.addWidget(self.userWidget)

    def set_top_layout(self):
        # set layout
        self.ui.top_layout.setStretch(0, 0)
        self.ui.top_layout.setStretch(1, 2)
        self.ui.top_layout.setStretch(2, 0)

    def add_auto_checkBox(self):
        # auto
        self.autoCheckBox = QtWidgets.QCheckBox('From scene name')
        self.autoCheckBox.setChecked(True)
        self.ui.navigation_verticalLayout.addWidget(self.autoCheckBox)

    def add_project_widget(self):
        # project
        self.projectWidget = project_widget.SGProjectComboBox(sg=sg_utils.sg, projectActive=False)
        self.ui.navigation_verticalLayout.addWidget(self.projectWidget)

    def add_navigation_widget(self):
        # type subtype asset
        self.navigationWidget = entity_browse_widget.SGAssetNavigation()
        self.navigationWidget.assetWidget.set_searchable(True)  # make asset combobox searchable
        self.ui.navigation_verticalLayout.addWidget(self.navigationWidget)

    def add_step_widget(self):
        # step
        self.stepWidget = file_comboBox.FileComboBox()
        self.stepWidget.set_label('Department')
        self.stepWidget.colorCheck = True
        self.ui.navigation_verticalLayout.addWidget(self.stepWidget)

    def add_process_widget(self):
        # process
        self.processWidget = file_comboBox.FileComboBox()
        self.processWidget.set_label('Process')
        self.processWidget.colorCheck = True
        self.ui.navigation_verticalLayout.addWidget(self.processWidget)
        self.processWidget.notStartwith = self.exceptionFilter

    def add_version_widget(self):
        self.versionWidget = QtWidgets.QLineEdit()
        self.ui.navigation_verticalLayout.addWidget(self.versionWidget)

    def add_display_widget(self):
        """ for auto get context from scene """
        self.displayWidget = entity_browse_widget.AssetDisplayInfoWidget()
        self.ui.navigation_verticalLayout.addWidget(self.displayWidget)

    def add_confirm_button(self):
        self.confirmButton = QtWidgets.QPushButton('Confirm Asset')
        self.confirmButton.setMinimumSize(QtCore.QSize(140, 26))
        self.ui.navigation_verticalLayout.addWidget(self.confirmButton)

        self.registerStatusLabel = QtWidgets.QLabel()
        self.ui.navigation_verticalLayout.addWidget(self.registerStatusLabel)

    def set_navigation_layout(self):
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.ui.navigation_verticalLayout.addItem(spacerItem)

    def add_qc_widget(self):
        # qc widget
        self.qcWidget = qc_widget.QcWidget()
        self.ui.qc_verticalLayout.addWidget(self.qcWidget)

    def add_output_widget(self):
        # output widget
        self.outputWidget = output_widget.OutputWidget()
        self.ui.output_verticalLayout.addWidget(self.outputWidget)
        self.outputWidget.exportButton.setEnabled(False)

    def add_console_widget(self):
        # console
        self.consoleLayout = QtWidgets.QVBoxLayout()
        self.consoleLabel = QtWidgets.QLabel('Information')
        self.consoleWidget = QtWidgets.QPlainTextEdit()
        self.consoleLayout.addWidget(self.consoleLabel)
        self.consoleLayout.addWidget(self.consoleWidget)
        self.ui.console_horizontalLayout.addLayout(self.consoleLayout)

    def add_task_widget(self):
        # task / status info
        self.taskStatusWidget = sginfo_widget.SgWidgetH(sg=sg_utils.sg, spacer=False)
        self.ui.snap_verticalLayout.addWidget(self.taskStatusWidget)

    def add_task_dependency_widget(self): 
        self.taskDependencyWidget = task_dependency_widget.TaskDependencyWidget()
        self.taskStatusWidget.subLayout1.addWidget(self.taskDependencyWidget)
        # set layout
        self.taskStatusWidget.subLayout1.setStretch(0, 2)
        self.taskStatusWidget.subLayout1.setStretch(1, 2)
        self.taskStatusWidget.subLayout1.setStretch(2, 4)
        # self.taskStatusWidget.subLayout1.setStretch(3, 6)

    def add_note_widget(self):
        buttonSpacer = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.ui.publish_layout.addItem(buttonSpacer)
        
        # note
        self.noteButton = QtWidgets.QPushButton('Complete Notes')
        # self.taskStatusWidget.subLayout1.addWidget(self.noteButton)
        self.completeNotes = None
        self.ui.publish_layout.addWidget(self.noteButton)


    def add_snap_widget(self):
        # snap area
        self.snapWidget = media_widget.MediaWidget2(isMaya=config.isMaya)
        self.ui.snap_verticalLayout.addWidget(self.snapWidget)

    def set_snap_layout(self):
        self.ui.snap_verticalLayout.setStretch(0, 0)
        self.ui.snap_verticalLayout.setStretch(1, 1)

    def add_publish_button(self):
        # publish button
        self.publishButton = QtWidgets.QPushButton('Publish')
        self.publishButton.setMinimumSize(QtCore.QSize(0, 40))
        self.ui.publish_layout.addWidget(self.publishButton)

    def set_publish_layout(self):
        self.ui.publish_layout.setStretch(0, 6)
        self.ui.publish_layout.setStretch(1, 1)
        self.ui.publish_layout.setStretch(2, 2)

    def add_description_widget(self):
        # description
        self.descriptionWidget = QtWidgets.QTextEdit()
        desLabel = QtWidgets.QLabel('Description')
        self.ui.publish_verticalLayout.addWidget(desLabel)
        self.ui.publish_verticalLayout.addWidget(self.descriptionWidget)

    def show_export_widget(self, state):
        self.ui.exportArea_frame.setVisible(state)

    def show_publish_widget(self, state):
        self.ui.publishArea_frame.setVisible(state)

    def custom_navigation_vis(self, state):
        self.projectWidget.set_visible(state)
        self.navigationWidget.set_visible(state)
        self.stepWidget.set_visible(state)
        self.processWidget.set_visible(state)
        self.versionWidget.setVisible(state)


    def setup_context(self):
        self.entity = context_info.ContextPathInfo()
        self.projectInfoTmp = project_info.ProjectInfo()
        self.asset = None


    def init_signals(self):
        # auto checkBox
        self.autoCheckBox.stateChanged.connect(self.auto_signal)
        # nav signals
        self.projectWidget.projectChanged.connect(self.project_signal)
        self.projectWidget.cleared.connect(self.project_clear_signal)

        self.navigationWidget.completed.connect(self.navigation_signal)
        self.stepWidget.currentIndexChanged.connect(self.step_signal)
        self.processWidget.currentIndexChanged.connect(self.process_signal)
        self.confirmButton.clicked.connect(self.confirm_data)

        # display output to console
        self.qcWidget.itemSelectionChanged.connect(self.console)
        self.outputWidget.itemSelectionChanged.connect(self.console)
        self.outputWidget.exportFeedback.connect(self.console)
        self.outputWidget.widgetStatus.connect(self.console)
        self.outputWidget.resChanged.connect(self.res_signal)

        # task signal
        self.taskStatusWidget.taskWidget.currentIndexChanged.connect(self.task_changed)

        # check qc if success
        self.qcWidget.check.connect(self.qc_signal)

        # check description widget
        self.descriptionWidget.textChanged.connect(self.description_check)

        # publish button
        self.outputWidget.exportStatus.connect(self.export_signal)
        self.publishButton.clicked.connect(self.publish)

        # note
        self.noteButton.clicked.connect(self.show_notes)

        # dependency check message signal
        self.taskDependencyWidget.message.connect(self.set_text_information)

        # icons
        self.snapWidget.previewWidget.display.customContextMenuRequested.connect(self.show_snap_menu)

    def init_functions(self):
        """ start functions """
        self.qcWidget.configFilter = True
        self.notificationMessages = []

        # read app config 
        self.configData = file_utils.ymlLoader(self.appConfig)

        # set auto run if checkBox is checked
        self.auto_signal(self.autoCheckBox.isChecked())

        # run if project selection not empty
        projectSelection = self.projectWidget.current_item()

        # set override export files if task not selected 
        self.export_check_task()

        if projectSelection and self.entity.valid:
            self.navigationWidget.populate(projectSelection)
            self.file_version()

            # run color guide
            self.color_guide()

        # run automate on ui 
        self.automate_selection()


    def console(self, data):
        """ display console """
        display = []
        func = data.get('func')
        doc = func.run.__doc__ if func else ''
        status = data.get('status')
        traceback = data.get('traceback')
        message = data.get('message')
        consoleColor = ''

        display.append(doc)

        if not status and traceback:
            display = []
            display.append(traceback)
            consoleColor = Color.red

        if message:
            display = []
            display.append(message)
            consoleColor = ''

        displayMessage = ('\n').join(display)

        self.consoleWidget.setPlainText(displayMessage)
        self.consoleWidget.setStyleSheet(consoleColor)

    def set_text_information(self, messages): 
        self.consoleWidget.setPlainText(messages)
        self.consoleWidget.setStyleSheet(Color.bgLightRed)

    def export_check_task(self): 
        task = self.taskStatusWidget.get_task()
        self.outputWidget.allowExport = True if task else False 
        self.outputWidget.allowMessage = '' if task else 'Please select task before export'

    def qc_signal(self, status):
        self.outputWidget.exportButton.setEnabled(status)
        self.set_widget_guide(self.qcWidget.checkButton, not status)
        self.set_widget_guide(self.outputWidget.exportButton, status, bg=True)


    def auto_signal(self, state):
        """ if auto, by pass navigation """
        value = True if state else False
        self.autoAsset = value

        # show hide widgets
        if self.autoAsset:
            self.custom_navigation_vis(False)
            self.displayWidget.set_visible(False)
            self.displayWidget.set_visible(True)

        else:
            self.displayWidget.set_visible(False)
            self.custom_navigation_vis(False)
            self.custom_navigation_vis(True)

        self.confirmButton.setEnabled(not self.autoAsset)

        self.confirm_data()
        logger.info('Set navigation from scene complete')

        # self.navigation_state(not value)
        # self.set_widget_guide(self.autoCheckBox, False)

    def task_changed(self, taskEntity):
        """ task comboBox changed """
        if taskEntity:
            """ try to guess res by look at task name """
            res = self.asset.list_res()
            taskRes = taskEntity.get('sg_resolution') or ''
            result = [a for a in res if a in taskRes]
            guess = result[0] if result else ''
            index = res.index(guess) + 1 if guess else 0
            self.outputWidget.resComboBox.setCurrentIndex(index)

            # allow output export 
            self.outputWidget.allowExport = True 
            self.outputWidget.allowMessage = ''

            # update task dependency 
            warningMessage = self.taskDependencyWidget.update(self.entity, taskEntity.get('step').get('name'), taskEntity.get('content'))
            # add to noti messages 
            self.notificationMessages.append(warningMessage)


    def description_check(self):
        self.descriptionWidget.setStyleSheet('')

    def navigation_state(self, state):
        """ set navigation widgets state """
        self.projectWidget.comboBox.setEnabled(state)
        self.navigationWidget.set_enable(state)
        self.stepWidget.set_enable(state)
        self.processWidget.set_enable(state)
        self.versionWidget.setEnabled(state)

    def project_signal(self, projectEntity):
        # this should be replaced by database
        if projectEntity:
            self.navigationWidget.populate(projectEntity)

    def project_clear_signal(self, state):
        if state:
            self.navigationWidget.clear()
            self.stepWidget.comboBox_activate()

    def navigation_signal(self, assetEntity):
        """ when asset show, send assetEntity """
        # list step by use assetEntity to compose path
        info = sg_utils.asset_info(assetEntity)
        context = context_info.Context()
        context.update(project=info['project'],
                        entityType='asset',
                        entityGrp=info['assetType'],
                        entity=info['name']
                        )

        asset = context_info.ContextPathInfo(context=context)
        stepPath = os.path.split(asset.path.step().abs_path())[0]
        self.stepWidget.list_items(stepPath)


    def step_signal(self, path):
        """ step events """
        if path: 
            asset = context_info.ContextPathInfo(path=path)
            processPath = os.path.split(asset.path.process().abs_path())[0]
            self.processWidget.list_items(processPath)

    def process_signal(self, path):
        """ process events """
        process = str(self.processWidget.comboBox.currentText())
        self.asset.context.update(permanent=True, process=process, app='maya')
        # self.confirm_data()

    def export_signal(self, state):
        # self.publishButton.setEnabled(state)

        # set widget color guide
        self.set_widget_guide(self.outputWidget.exportButton, not state, bg=True)
        self.check_file_register(self.asset)

    def res_signal(self, res):
        """ res changed signal """
        if res and not res == self.outputWidget.blankRes:
            self.asset.context.update(permanent=True, res=res)
            self.check_file_register(self.asset)
            self.preregInfo = publish_info.PreRegister(self.asset)
            # update qc 
            self.qcWidget.update(project=self.asset.project, entityType=self.asset.entity_type, step=self.asset.step, entity=self.asset, res=self.asset.res, process=self.asset.process)


    def file_version(self):
        """ find file version """
        ptc = context_info.PathToContext(path=self.entity.path.path())
        self.versionWidget.setText(ptc.version)


    def check_file_register(self, entity):
        self.regInfo = publish_info.RegisterInfo(entity)
        content = self.regInfo.content.get(entity.version)
        regVersion = content.get(publish_info.Data.regVersion) if content else ''
        status = 'Not Register Daily only' if not content else 'Register %s' % regVersion
        color = Color.bgGreen if content else Color.bgYellow

        self.registerStatusLabel.setText(status)
        self.registerStatusLabel.setStyleSheet(color)
        self.registerStatus = True if regVersion else False
        self.set_publish_mode()
        # complete note options
        self.noteButton.setEnabled(self.registerStatus)

    def set_publish_mode(self):
        text = 'Publish' if self.registerStatus else 'Submit Daily'
        self.publishButton.setText(text)


    def set_task_list(self, asset):
        entityType = 'Asset'
        self.taskStatusWidget.set_task(asset.project, entityType, asset.name, asset.step)

    def set_status_list(self, asset): 
        # read default base 
        baseConfig = self.configData['default']

        # override with project config 
        if asset.project in self.configData.keys(): 
            projectConfig = self.configData[asset.project]
            baseConfig.update(projectConfig)

        # override config 
        sginfo_widget.status_widget.Icon.limit = baseConfig['TaskLimit']
        if asset.step in baseConfig['TaskLimit']: 
            self.taskStatusWidget.statusWidget.limit = asset.step
        else: 
            self.taskStatusWidget.statusWidget.limit = 'default'
        self.taskStatusWidget.statusWidget.set_icon()
        print(baseConfig)

    def guess_task(self, asset):
        # guess task
        context = asset.read_env()
        if context:
            asset2 = context_info.ContextPathInfo()
            asset2.context.context = context
            task = asset2.task
            tasks = self.taskStatusWidget.set_current_task(task)


    def confirm_data(self):
        """ transfer complete entity to qc and output """
        # if auto, get from scene
        if self.autoAsset:
            self.asset = context_info.ContextPathInfo()
            self.displayWidget.set_display(self.asset)

        # if not, get from ui
        else:
            version = str(self.versionWidget.text())
            path = self.processWidget.current_item()
            if path: 
                # make self.asset global after confirm
                self.asset = context_info.ContextPathInfo(path=path)
                self.asset.context.update(app='maya', version=version)

        # update look
        self.asset.context.update(look=self.asset.process)

        self.set_task_list(self.asset)
        self.set_status_list(self.asset)
        self.connect_qc(self.asset)
        self.guess_task(self.asset)



    def connect_qc(self, entity):
        """ run qc directly """
        self.qcWidget.update(project=entity.project, entityType=entity.entity_type, step=entity.step, entity=entity, res=entity.res)
        self.outputWidget.update_entity(entity)
        self.outputWidget.add_res(entity.list_res())

        # set widget color guide
        self.set_widget_guide(self.qcWidget.checkButton, True, True)

    def color_guide(self):
        self.set_widget_guide(self.autoCheckBox, True, bg=False)

    def set_widget_guide(self, widget, state, bg=False):
        colorType = Color.bgGreen if bg else Color.green
        color = colorType if state else ''
        widget.setStyleSheet(color)


    def show_notes(self):
        """ shot notes widgets """
        taskEntity = self.taskStatusWidget.get_task()
        if taskEntity:
            self.noteDialog = note_dialog.NoteDialog(sg_utils.sg, taskEntity, self.completeNotes)
            result = self.noteDialog.exec_()
            self.completeNotes = self.noteDialog.noteWidget.checked_notes()

    def show_snap_menu(self, pos):
        imgPath = self.snapWidget.previewWidget.currentPath
        menu = QtWidgets.QMenu(self)
        iconMenu = menu.addAction('Make master icon')
        iconMenu.triggered.connect(partial(self.make_master_icon, imgPath))
        menu.popup(self.snapWidget.previewWidget.display.mapToGlobal(pos))

    def make_master_icon(self, path):
        if os.path.exists(path):
            name, ext = os.path.splitext(path)
            iconDir = self.entity.path.icon().abs_path()
            iconName = self.entity.icon_name(ext=ext)
            dst = '%s/%s' % (iconDir, iconName)
            utils.copy(path, dst)
            logger.info('Set master %s' % dst)

        else:
            logger.warning('No image found')


    def automate_selection(self): 
        """ automate ui selection """ 
        self.auto_task_selection()

    def auto_task_selection(self): 
        """ auto select task based on step """ 
        if self.asset.step in Config.taskSyncStep: 
            self.sync_process_task(self.asset)

    def sync_process_task(self, asset): 
        """ sync process to task selection """ 
        taskName = asset.process 
        allTasks = self.taskStatusWidget.get_all_items()
        if taskName in allTasks: 
            self.taskStatusWidget.set_current_task(taskName)

        else: 
            self.taskStatusWidget.set_alert(True)
            self.set_text_information('No task "%s" exist on Shotgun! Contact coordinator!' % taskName)
            logger.warning('No task "%s" exist on Shotgun' % taskName)

    # publish section
    def publish(self):
        """ publish method """
        QtWidgets.QApplication.processEvents()

        # user
        userEntity = self.userWidget.data()

        # set bg-red if not select
        status = self.taskStatusWidget.check_info()
        captureStatus = self.snapWidget.check_complete()

        # description
        description = self.descriptionWidget.toPlainText()
        desColor = Color.bgRed if not description else ''
        self.descriptionWidget.setStyleSheet(desColor)

        if all([status, captureStatus, description]):

            taskEntity = self.taskStatusWidget.get_task()
            status = self.taskStatusWidget.get_status()
            taskEntity['sg_status_list'] = status
            asset = self.asset.copy()
            if self.asset.step in ['texture', 'lookdev', 'groom']: 
                if not taskEntity['sg_name']: 
                    logger.error('No look name in this task "%s"' % taskEntity.get('content'))
                asset.context.update(process=taskEntity['sg_name'], look=taskEntity['sg_name'])

            # if files already exported, register asset data.
            if self.registerStatus:
                # publish and hero imgs path
                outputImgPath = asset.path.outputImg().abs_path()
                outputHeroImgPath = asset.path.output_hero_img().abs_path()

                # collect imgs
                imgs = self.collect_snap(asset, userEntity, outputImgPath)
                # heroImgs = self.collect_snap(asset, userEntity, outputHeroImgPath, hero=False) # can't remember why it false
                heroImgs = self.collect_snap(asset, userEntity, outputHeroImgPath, hero=True) # change to hero name
                dailyResult = daily.submit(asset, heroImgs)

                # create version
                versionResult = sg_hook.publish_version(asset, taskEntity, status, imgs, userEntity, description)
                taskResult = sg_hook.update_task(asset, taskEntity, status)

                project_entity = taskEntity['project']
                proj = project_info.ProjectInfo(project=project_entity['name'])
                tasks_trigger_table = proj.task.dependency('asset')
                result = status_update.update_dependency_event_api(tasks_trigger_table, taskEntity)

                # triggerTaskResult = sg_hook.update_dependency(asset, taskEntity, status)
                # lookResult = sg_hook.look_dependency(asset, taskEntity, status)
                noteResult = sg_hook.complete_notes(self.completeNotes, versionResult)

                # publish info
                asset.context.update(task=taskEntity.get('content'))
                publishInfo = publish_info.RegisterAsset(asset)
                regData = publishInfo.register()

                # register entity
                output = register_entity.Output()
                output.add_process(asset.process) if not asset.step in [context_info.Step.texture, context_info.Step.lookdev, context_info.Step.groom] else output.add_look(asset.look)
                output.add_version_id(versionResult['id'])
                output.add_version(asset.publishVersion)
                output.add_media(outputImgPath, outputHeroImgPath)
                output.add_preview(imgs[0]) if imgs else None
                output.add_task(taskEntity['content'])
                output.add_description(description)

                assetDescription = register_entity.Register(asset)
                assetDescription.set(output=output)
                assetDescription.add_asset_id(taskEntity['entity']['id'])
                assetDescription.register()

                # lock workfiles
                workspace_utils.lock_workspace(asset)

                #send noti to cliq bot
                #noti_module_cliq.main()
                taskFromId = sg_process.get_task_from_id(taskEntity['id'])
                if status == 'apr':
                    cache = {'step':{'name':'cache'}, 'task_assignees':[{'name':'-'}], 'project':taskEntity['project'], 'sg_status_list':'def' }
                    result.append(cache)
                    result.append(taskFromId)
                    taskname = taskEntity.get('content')
                    if taskname == 'rigUv' or taskname == 'rig':
                        anim = {'step':{'name':'anim'}, 'task_assignees':[{'name':'-'}], 'project':taskEntity['project'], 'sg_status_list':'def' }
                        layout = {'step':{'name':'layout'}, 'task_assignees':[{'name':'-'}], 'project':taskEntity['project'], 'sg_status_list':'def' }
                        result.append(anim)
                        result.append(layout)
                    elif taskname == 'rigBsh':
                        rigBsh = {'step':{'name':'model'}, 'task_assignees':[{'name':'Fuga'}], 'project':taskEntity['project'], 'sg_status_list':'def' }
                        result.append(rigBsh)


                # if status is review (Daily)
                elif status == 'rev':
                    result = [taskFromId]

                emails = noti_module_cliq.get_notification_data(result, taskEntity, description, userEntity)

            # if files not exported, data will not register. Submit to shotgun daily only.
            if not self.registerStatus:
                # publish and hero imgs path
                # override subversion
                publishVersion = asset.publishVersion
                asset.context.update(publishVersion=asset.subversion)

                outputImgPath = asset.path.outputImg().abs_path()
                outputHeroImgPath = asset.path.output_hero_img().abs_path()

                # collect imgs
                imgs = self.collect_snap(asset, userEntity, outputImgPath)
                # heroImgs = self.collect_snap(asset, userEntity, outputHeroImgPath, hero=False) # can't remember why it false
                heroImgs = self.collect_snap(asset, userEntity, outputHeroImgPath, hero=True) # change to hero name
                dailyResult = daily.submit(asset, heroImgs)

                # restore
                asset.context.update(publishVersion=publishVersion)

                # create version
                versionResult = sg_hook.publish_daily(asset, taskEntity, status, imgs, userEntity, description)
                taskResult = sg_hook.update_task(asset, taskEntity, status)

            self.publishButton.setEnabled(False)
            self.set_publish_status(all([versionResult, taskResult]))
            if self.registerStatus:

                imgurl = sg_process.get_media(versionResult)
                if '.mov' in imgurl:
                    imgurl = sg_process.get_media_thumbnail(versionResult)
                if 'thumbnail_pending' in imgurl:
                    imgurl = 'https://previewengine-accl.zoho.com/image/WD/54mxc9836fa8a51d545eab56f79f735e2029c?width=2046&height=1536'
                # print imgurl
                urlImage = [imgurl]

                noti_module_cliq.send_notification(emails, taskEntity, urlImage=urlImage)


    def set_publish_status(self, state):
        iconPath = icon.buttonCheck if state else icon.buttonFail
        dialog.MessageBox.success('Complete', 'Publish Complete')

    def get_start_due_date(self):
        # get due date for hud
        taskEntity = self.taskStatusWidget.get_task()
        not_available_str = 'N/A'
        due_date = taskEntity.get('due_date', not_available_str)
        start_date = taskEntity.get('start_date', not_available_str)
        # start date
        start_date_text = 'Start: N/A'
        if start_date not in ('', None, 'N/A'):
            try:
                dt = datetime.strptime(start_date, '%Y-%m-%d').date()
                start_date = dt.strftime('%d %b %Y')
            except Exception:
                start_date = 'N/A'
            start_date_text = 'Start: {}'.format(start_date)

        # due date
        due_date_text = 'Due: N/A'
        if due_date not in ('', None, 'N/A'):  
            try:
                dt = datetime.strptime(due_date, '%Y-%m-%d').date()
                due_date = dt.strftime('%d %b %Y')
            except Exception:
                due_date = 'N/A'
            due_date_text = 'Due: {}'.format(due_date)
        start_due_date_text = '{} {}'.format(start_date_text, due_date_text)
        return start_due_date_text

    def collect_snap(self, asset, userEntity, outputImgPath, hero=False):
        imgs = self.snapWidget.get_all_items()
        date_text = 'Date: {}'.format(time.strftime('%d %b %Y - %H:%M:%S'))
        start_due_date_text = self.get_start_due_date()

        IMG_EXT = ('.jpg', '.png', '.tif', '.exr')
        VID_EXT = ('.mov', '.mp4', '.mpg')
        # add border hud to each media
        process_images = list(imgs)
        for i, img in enumerate(imgs):
            logger.info('Adding border HUD: {}'.format(img))
            fn, ext = os.path.splitext(img)
            result = None
            kwargs = {'input_path': img, 
                    'output_path': '', 
                    'topLeft': [asset.project], 
                    'topMid': [], 
                    'topRight': [date_text, start_due_date_text], 
                    'bottomLeft': ['user: {}'.format(userEntity['name']), 
                                'department: {}'.format(asset.step), 
                                'file: {}'.format(asset.filename)],
                    'bottomMid': [asset.name],
                    'bottomRight': ['type: {}'.format(asset.type),
                                'LOD: {}'.format(asset.res), 
                                'process: {}'.format(asset.process)]}

            # add hud
            QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
            if ext in IMG_EXT:
                result = watermark.add_border_hud_to_image(**kwargs)
            elif ext in VID_EXT:
                result = watermark.add_border_hud_to_mov(frameHud='bottomMid', **kwargs)
            QtWidgets.QApplication.restoreOverrideCursor()

            if not result:
                logger.warning('Failed to add border HUD: {}'.format(img))
                continue
            process_images[i] = result

        # *** WARNING! 
        # image path in "process_images" variable might relocate to TEMP dir if border HUD was succeeded
        # example: 'C:\\Users\\NATTAP~1.N\\AppData\\Local\\Temp\\kai_CU_Turn.mpg'
        # if border HUD has failed, process_images[i] will be the same as imgs[i]

        # ------ Copy and rename to publish destination
        name, ext = os.path.splitext(process_images[0])
        imgName = asset.publish_name(ext=ext, hero=hero)
        status, outputs = utils.collect_imgs(outputImgPath, imgName, process_images)

        return outputs

def show():
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('%s Running in Maya\n' % _title)
        maya_win.deleteUI(uiName)
        myApp = RFAssetPublish(maya_win.getMayaWindow())
        myApp.show()
        return myApp

    else: 
        logger.info('Run in standalone\n')
        app = QtWidgets.QApplication.instance()
        if not app:
            app = QtWidgets.QApplication(sys.argv)
        myApp = RFAssetPublish()
        myApp.show()
        stylesheet.set_default(app)
        sys.exit(app.exec_())
