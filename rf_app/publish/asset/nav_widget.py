import os
import sys
import traceback
from collections import OrderedDict
from functools import partial 
import subprocess

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_utils import publish_info
from rf_utils.widget import display_widget
from rf_utils.widget import filmstrip_widget
from rf_utils.widget import snap_widget

reload(snap_widget)
import utils 

class LocalConfig: 
    iconSize = 60 
    snapIcon = '%s/icons/snap.png' % module_dir


class DisplayComboBox(QtWidgets.QWidget):
    """docstring for DisplayComboBox"""
    def __init__(self, parent=None):
        super(DisplayComboBox, self).__init__(parent=None)
        self.allLayout = QtWidgets.QHBoxLayout()
        self.label = QtWidgets.QLabel()
        self.comboBox = QtWidgets.QComboBox()
        self.allLayout.addWidget(self.label)
        self.allLayout.addWidget(self.comboBox)
        self.label.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)

        self.allLayout.setStretch(0, 1)
        self.allLayout.setStretch(1, 3)

        self.allLayout.setSpacing(8)
        self.allLayout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.allLayout)

    def enable(self, state): 
        self.comboBox.setEnabled(state)

    def set_text(self, text): 
        self.comboBox.clear()
        self.comboBox.addItem(text)

        

class NavigationUI(QtWidgets.QWidget) :
    def __init__(self, parent=None) :
        super(NavigationUI, self).__init__(parent)
        self.allLayout = QtWidgets.QHBoxLayout()
        self.projectDisplay = DisplayComboBox()
        self.assetDisplay = DisplayComboBox()
        self.stepDisplay = DisplayComboBox()
        self.processDisplay = DisplayComboBox()
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        self.projectDisplay.label.setText('Project')
        self.assetDisplay.label.setText('Asset')
        self.stepDisplay.label.setText('Step')
        self.processDisplay.label.setText('Process')

        self.allLayout.addWidget(self.projectDisplay)
        self.allLayout.addWidget(self.assetDisplay)
        self.allLayout.addWidget(self.stepDisplay)
        self.allLayout.addWidget(self.processDisplay)
        self.allLayout.addItem(spacerItem)

        self.allLayout.setStretch(0, 1)
        self.allLayout.setStretch(1, 1)
        self.allLayout.setStretch(2, 1)
        self.allLayout.setStretch(3, 1)
        self.allLayout.setStretch(4, 1)

        self.allLayout.setSpacing(20)
        self.allLayout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.allLayout)
        

class Navigation(NavigationUI):
    """docstring for Navigation"""
    def __init__(self, parent=None):
        super(Navigation, self).__init__(parent=parent)

    def enable(self, state): 
        self.projectDisplay.comboBox.setEnabled(state)
        self.assetDisplay.comboBox.setEnabled(state)
        self.stepDisplay.comboBox.setEnabled(state)
        self.processDisplay.comboBox.setEnabled(state)
        
