# v.0.0.1 polytag switcher
_title = 'Asset Publish'
_version = 'v.0.0.1'
_des = 'wip'
uiName = 'AssetPublishUI'

#Import python modules
import sys
import os 
import json 
import getpass

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

# import config
core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path: 
    sys.path.append(core)

import rf_config as config

# framework modules 
from rf_utils import log_utils
from rf_utils.ui import load
from rf_utils.ui import stylesheet

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

# import pipeline modules
from rf_app.publish.asset import output_viewer_widget
from rf_app.publish.asset import media_widget
from rf_app.publish.asset import nav_widget
from rf_app.publish.asset import sginfo_widget
from rf_utils.widget import user_widget
from rf_utils import icon 
from rf_utils import publish_info 
from rf_utils.sg import sg_utils
from rf_utils.context import context_info
from rf_utils import daily 
import utils 
import sg_hook

class LocalConfig: 
    captureSize = {'w': 1280, 'h': 1024}

class AssetPublish(QtWidgets.QMainWindow):

    def __init__(self, ContextPathInfo=None, parent=None):
        #Setup Window
        super(AssetPublish, self).__init__(parent)

        # ui read
        uiFile = '%s/ui.ui' % moduleDir
        if config.isMaya: 
            self.ui = load.setup_ui_maya(uiFile, parent)
        else: 
            self.ui = load.setup_ui(uiFile, self)
        
        self.ui.show()
        self.ui.setWindowTitle('%s %s-%s' % (_title, _version, _des))

        self.entity = ContextPathInfo
        self.set_widgets()
        self.init_functions()

    def set_widgets(self): 
        """ setup widgets for publish app """ 

        ### output check widget on version_verticalLayout
        self.outputViewWidget = output_viewer_widget.OutputViewWidget(ContextPathInfo=self.entity)
        self.ui.version_verticalLayout.addWidget(self.outputViewWidget)
        
        ### snap widget on preview_verticalLayout 
        self.snapWidget = media_widget.MediaWidget(isMaya=config.isMaya)
        self.ui.preview_verticalLayout.addWidget(self.snapWidget)

        ### display navigation widget on nav_horizontalLayout
        # logo 
        self.logoWidget = QtWidgets.QLabel()
        self.ui.nav_horizontalLayout.addWidget(self.logoWidget)
        self.set_logo()

        # navigation widget 
        self.displayWidget = nav_widget.Navigation()
        self.ui.nav_horizontalLayout.addWidget(self.displayWidget)

        # user 
        self.userWidget = user_widget.UserComboBox()
        self.ui.nav_horizontalLayout.addWidget(self.userWidget)
        

        ### sg widget on publish_horizontalLayout
        # task widget 
        self.sgWidget = sginfo_widget.SgWidget(sg=sg_utils.sg)
        self.ui.publish_horizontalLayout.addWidget(self.sgWidget)

        # publish button 
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.publishButton = self.publish_button()
        self.ui.publish_horizontalLayout.addWidget(self.publishButton)

        # status label 
        self.publishStatusLabel = QtWidgets.QLabel()
        self.ui.publish_horizontalLayout.addWidget(self.publishStatusLabel)
        self.ui.publish_horizontalLayout.addItem(spacerItem)

        self.ui.publish_horizontalLayout.setStretch(0, 1)
        self.ui.publish_horizontalLayout.setStretch(1, 6)
        self.ui.publish_horizontalLayout.setStretch(2, 4)
        self.ui.publish_horizontalLayout.setStretch(3, 1)
        self.ui.publish_horizontalLayout.setStretch(3, 1)


    def publish_button(self): 
        publishButton = QtWidgets.QPushButton('Publish')
        publishButton.setMinimumSize(QtCore.QSize(200, 40))
        publishButton.clicked.connect(self.publish)
        return publishButton

    def set_logo(self): 
        self.logoWidget.setPixmap(QtGui.QPixmap(icon.logo).scaled(64, 64, QtCore.Qt.KeepAspectRatio))


    def init_functions(self): 
        self.snapWidget.captureSize = LocalConfig.captureSize
        self.outputViewWidget.update()
        self.display_widget()
        self.set_task_list()

    def display_widget(self): 
        if self.entity: 
            self.displayWidget.projectDisplay.set_text(self.entity.project)
            self.displayWidget.assetDisplay.set_text(self.entity.name)
            self.displayWidget.stepDisplay.set_text(self.entity.step)
            self.displayWidget.processDisplay.set_text(self.entity.process)
            self.displayWidget.enable(False)

    def set_task_list(self): 
        entityType = 'Asset'
        if self.entity: 
            self.sgWidget.set_task(self.entity.project, entityType, self.entity.name, self.entity.step)

    def publish(self): 
        """ publish method """ 
        QtWidgets.QApplication.processEvents()

        # publishPath = context_info.RootPath(self.outputViewWidget.publish_path()).abs_path()
        # publishVersion = self.outputViewWidget.version

        # publish and hero imgs path 
        outputImgPath = self.entity.path.outputImg().abs_path()
        outputHeroImgPath = self.entity.path.output_hero_img().abs_path()

        # user 
        user = self.userWidget.data()
        userEntity = user.sg_user()

        # set bg-red if not select 
        status = self.sgWidget.check_info()
        captureStatus = self.snapWidget.check_complete()

        if all([status, captureStatus]): 
            self.publishButton.setEnabled(False)

            taskEntity = self.sgWidget.get_task()
            status = self.sgWidget.get_status()
            imgs = self.collect_snap(outputImgPath)
            heroImgs = self.collect_snap(outputHeroImgPath, hero=True)
            dailyResult = daily.submit(self.entity, heroImgs)

            # create version 
            versionResult = sg_hook.publish_version(self.entity, taskEntity, status, imgs, userEntity)
            taskResult = sg_hook.update_task(self.entity, taskEntity, status)

            # reg version 
            self.entity.context.update(task=taskEntity.get('content'))
            publishInfo = publish_info.RegisterAsset(self.entity)
            regData = publishInfo.register()

            self.set_publish_status(all([versionResult, taskResult]))

    
    def set_publish_status(self, state): 
        iconPath = icon.buttonCheck if state else icon.buttonFail
        self.publishStatusLabel.setPixmap(QtGui.QPixmap(iconPath).scaled(32, 32, QtCore.Qt.KeepAspectRatio))

    
    def collect_snap(self, outputImgPath, hero=False): 
        imgs = self.snapWidget.get_all_items()
        imgName = self.outputViewWidget.entity.publish_name(ext='.png', hero=hero)
        status, outputs = utils.collect_imgs(outputImgPath, imgName, imgs)
        return outputs



def show(contextPathInfo=None):
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        maya_win.deleteUI(uiName)
        myApp = AssetPublish(ContextPathInfo=contextPathInfo, parent=maya_win.getMayaWindow())
        return myApp

    else: 
        logger.info('Run in standalone\n')
        app = QtWidgets.QApplication.instance()
        if not app: 
            app = QtWidgets.QApplication(sys.argv)
        myApp = AssetPublish()
        stylesheet.set_default(app)
        sys.exit(app.exec_())

if __name__ == '__main__': 
    show()
