# v.0.0.1 polytag switcher
_title = 'Asset Publish Tool'
_version = 'v.0.0.1'
_des = 'wip'
uiName = 'ExportTool'

#Import python modules
import sys
import os
import getpass
import logging
from datetime import datetime

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

# import config
import rf_config as config

# framework modules
from rf_utils import log_utils
from rf_utils.ui import load
from rf_utils.ui import stylesheet
from rf_app.publish.asset import app

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

# import qc widget
from rf_qc import qc_widget
from rf_app.export import output_widget
from rf_utils.widget import entity_browse_widget
from rf_utils.widget import project_widget
from rf_utils.widget import file_comboBox
from rf_utils.widget import user_widget
from rf_utils.widget import file_widget
from rf_utils import icon
from rf_utils.context import context_info
from rf_utils import project_info
from rf_utils import publish_info
from rf_utils import file_utils
from rf_app.publish.asset import media_widget
from rf_app.publish.asset import sginfo_widget
from rf_utils.widget import dialog
from rf_utils.sg import sg_utils
from rf_utils import daily
import utils
import sg_hook
import workspace_utils

class Color:
    red = 'color: rgb(200, 0, 0);'
    green = 'color: rgb(0, 240, 0);'
    bgGreen = 'background-color: rgb(0, 140, 0);'
    bgYellow = 'background-color: rgb(200, 120, 0);'
    bgRed = 'background-color: rgb(140, 0, 0);'

class RFAssetPublish(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        #Setup Window
        super(RFAssetPublish, self).__init__(parent)

        # ui read
        uiFile = '%s/ui_core.ui' % moduleDir
        if config.isMaya:
            self.ui = load.setup_ui_maya(uiFile, parent)

        self.ui.show()
        self.now = datetime.now()
        self.ui.setWindowTitle('%s %s-%s' % (_title, _version, _des))

        self.projectSelection = '-- Choose Project --'
        self.exceptionFilter = '_'

        # setup
        # self.setup_context()
        self.setup_layout()
        self.init_signals()
        self.init_functions()


    def setup_layout(self):
        self.add_logo()
        self.add_user_widget()
        self.set_top_layout()

        self.add_auto_checkBox()
        self.add_project_widget()
        self.add_navigation_widget()
        self.add_step_widget()
        self.add_process_widget()
        self.add_version_widget()

        self.add_display_widget()

        self.add_confirm_button()
        self.set_navigation_layout()

        self.add_qc_widget()
        self.add_output_widget()
        self.add_console_widget()
        self.add_task_widget()
        self.add_snap_widget()
        self.set_snap_layout()
        self.add_publish_button()
        self.set_publish_layout()
        self.add_description_widget()

    def add_logo(self):
        # logo
        self.logoLabel = QtWidgets.QLabel()
        self.ui.top_layout.addWidget(self.logoLabel)
        self.logoLabel.setPixmap(QtGui.QPixmap(icon.logo).scaled(64, 64, QtCore.Qt.KeepAspectRatio))

    def add_user_widget(self):
        topSpacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)

        # user
        self.userWidget = user_widget.UserComboBox()
        self.ui.top_layout.addItem(topSpacerItem)
        self.ui.top_layout.addWidget(self.userWidget)

    def set_top_layout(self):
        # set layout
        self.ui.top_layout.setStretch(0, 0)
        self.ui.top_layout.setStretch(1, 2)
        self.ui.top_layout.setStretch(2, 0)

    def add_auto_checkBox(self):
        # auto
        self.autoCheckBox = QtWidgets.QCheckBox('From scene name')
        self.autoCheckBox.setChecked(True)
        self.ui.navigation_verticalLayout.addWidget(self.autoCheckBox)

    def add_project_widget(self):
        # project
        self.projectWidget = project_widget.SGProjectComboBox(sg=sg_utils.sg, projectActive=False)
        self.ui.navigation_verticalLayout.addWidget(self.projectWidget)

    def add_navigation_widget(self):
        # type subtype asset
        self.navigationWidget = entity_browse_widget.SGAssetNavigation()
        self.ui.navigation_verticalLayout.addWidget(self.navigationWidget)

    def add_step_widget(self):
        # step
        self.stepWidget = file_comboBox.FileComboBox()
        self.stepWidget.set_label('Department')
        self.stepWidget.colorCheck = True
        self.ui.navigation_verticalLayout.addWidget(self.stepWidget)

    def add_process_widget(self):
        # process
        self.processWidget = file_comboBox.FileComboBox()
        self.processWidget.set_label('Process')
        self.processWidget.colorCheck = True
        self.ui.navigation_verticalLayout.addWidget(self.processWidget)
        self.processWidget.notStartwith = self.exceptionFilter

    def add_version_widget(self):
        self.versionWidget = QtWidgets.QLineEdit()
        self.ui.navigation_verticalLayout.addWidget(self.versionWidget)

    def add_display_widget(self):
        """ for auto get context from scene """
        self.displayWidget = entity_browse_widget.AssetDisplayInfoWidget()
        self.ui.navigation_verticalLayout.addWidget(self.displayWidget)

    def add_confirm_button(self):
        self.confirmButton = QtWidgets.QPushButton('Confirm Asset')
        self.confirmButton.setMinimumSize(QtCore.QSize(140, 26))
        self.ui.navigation_verticalLayout.addWidget(self.confirmButton)

        self.registerStatusLabel = QtWidgets.QLabel()
        self.ui.navigation_verticalLayout.addWidget(self.registerStatusLabel)

    def set_navigation_layout(self):
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.ui.navigation_verticalLayout.addItem(spacerItem)

    def add_qc_widget(self):
        # qc widget
        self.qcWidget = qc_widget.QcWidget()
        self.ui.qc_verticalLayout.addWidget(self.qcWidget)

    def add_output_widget(self):
        # output widget
        self.outputWidget = output_widget.OutputWidget()
        self.ui.output_verticalLayout.addWidget(self.outputWidget)
        self.outputWidget.exportButton.setEnabled(False)

    def add_console_widget(self):
        # console
        self.consoleLayout = QtWidgets.QVBoxLayout()
        self.consoleLabel = QtWidgets.QLabel('Information')
        self.consoleWidget = QtWidgets.QPlainTextEdit()
        self.consoleLayout.addWidget(self.consoleLabel)
        self.consoleLayout.addWidget(self.consoleWidget)
        self.ui.console_horizontalLayout.addLayout(self.consoleLayout)

    def add_task_widget(self):

        # task info
        self.sgWidget = sginfo_widget.SgWidgetH(sg=sg_utils.sg)
        self.ui.snap_verticalLayout.addWidget(self.sgWidget)

    def add_snap_widget(self):
        # snap area
        self.snapWidget = media_widget.MediaWidget2(isMaya=config.isMaya)
        self.ui.snap_verticalLayout.addWidget(self.snapWidget)

    def set_snap_layout(self):
        self.ui.snap_verticalLayout.setStretch(0, 0)
        self.ui.snap_verticalLayout.setStretch(1, 1)

    def add_publish_button(self):
        # publish button
        buttonSpacer = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.publishButton = QtWidgets.QPushButton('Publish')
        self.publishButton.setMinimumSize(QtCore.QSize(0, 40))
        self.ui.publish_layout.addItem(buttonSpacer)
        self.ui.publish_layout.addWidget(self.publishButton)

    def set_publish_layout(self):
        self.ui.publish_layout.setStretch(0, 3)
        self.ui.publish_layout.setStretch(1, 1)

    def add_description_widget(self):
        # description
        self.descriptionWidget = QtWidgets.QTextEdit()
        desLabel = QtWidgets.QLabel('Description')
        self.ui.publish_verticalLayout.addWidget(desLabel)
        self.ui.publish_verticalLayout.addWidget(self.descriptionWidget)

    def show_export_widget(self, state):
        self.ui.exportArea_frame.setVisible(state)

    def show_publish_widget(self, state):
        self.ui.publishArea_frame.setVisible(state)

    def custom_navigation_vis(self, state):
        self.projectWidget.set_visible(state)
        self.navigationWidget.set_visible(state)
        self.stepWidget.set_visible(state)
        self.processWidget.set_visible(state)
        self.versionWidget.setVisible(state)


    def setup_context(self):
        self.entity = context_info.ContextPathInfo()
        self.projectInfoTmp = project_info.ProjectInfo()
        self.asset = None


    def init_signals(self):
        # auto checkBox
        self.autoCheckBox.stateChanged.connect(self.auto_signal)
        # nav signals
        self.projectWidget.projectChanged.connect(self.project_signal)
        self.projectWidget.cleared.connect(self.project_clear_signal)

        self.navigationWidget.completed.connect(self.navigation_signal)
        self.stepWidget.currentIndexChanged.connect(self.step_signal)
        self.processWidget.currentIndexChanged.connect(self.process_signal)
        self.confirmButton.clicked.connect(self.confirm_data)

        # display output to console
        self.qcWidget.itemSelectionChanged.connect(self.console)
        self.outputWidget.itemSelectionChanged.connect(self.console)
        self.outputWidget.exportFeedback.connect(self.console)
        self.outputWidget.widgetStatus.connect(self.console)
        self.outputWidget.resChanged.connect(self.res_signal)

        # task signal
        self.sgWidget.taskWidget.currentIndexChanged.connect(self.guess_res)

        # check qc if success
        self.qcWidget.check.connect(self.qc_signal)

        # check description widget
        self.descriptionWidget.textChanged.connect(self.description_check)

        # publish button
        self.outputWidget.exportStatus.connect(self.export_signal)
        self.publishButton.clicked.connect(self.publish)


    def init_functions(self):
        """ start functions """
        # set auto run if checkBox is checked
        self.auto_signal(self.autoCheckBox.isChecked())

        # run if project selection not empty
        projectSelection = self.projectWidget.current_item()

        if projectSelection:
            self.navigationWidget.populate(projectSelection)

            self.qcWidget.configFilter = True

            # run color guide
            self.color_guide()


    def console(self, data):
        """ display console """
        display = []
        func = data.get('func')
        doc = func.run.__doc__ if func else ''
        traceback = data.get('traceback')
        message = data.get('message')
        consoleColor = ''

        display.append(doc)

        if traceback:
            display = []
            display.append(traceback)
            consoleColor = Color.red

        if message:
            display = []
            display.append(message)
            consoleColor = ''

        displayMessage = ('\n').join(display)

        self.consoleWidget.setPlainText(displayMessage)
        self.consoleWidget.setStyleSheet(consoleColor)

    def qc_signal(self, status):
        self.outputWidget.exportButton.setEnabled(status)
        self.set_widget_guide(self.qcWidget.checkButton, not status)
        self.set_widget_guide(self.outputWidget.exportButton, status, bg=True)


    def auto_signal(self, state):
        """ if auto, by pass navigation """
        value = True if state else False
        self.autoAsset = value

        # show hide widgets
        if self.autoAsset:
            self.custom_navigation_vis(False)
            self.displayWidget.set_visible(False)
            self.displayWidget.set_visible(True)

        else:
            self.displayWidget.set_visible(False)
            self.custom_navigation_vis(False)
            self.custom_navigation_vis(True)

        self.confirmButton.setEnabled(not self.autoAsset)

        self.confirm_data()
        logger.info('Set navigation from scene complete')

        # self.navigation_state(not value)
        # self.set_widget_guide(self.autoCheckBox, False)

    def guess_res(self, taskEntity):
        """ try to guess res by look at task name """
        if taskEntity:
            res = self.asset.list_res()
            result = [a for a in res if a in taskEntity.get('content')]
            guess = result[0] if result else ''
            index = res.index(guess) + 1 if guess else 0
            self.outputWidget.resComboBox.setCurrentIndex(index)


    def description_check(self):
        self.descriptionWidget.setStyleSheet('')

    def navigation_state(self, state):
        """ set navigation widgets state """
        self.projectWidget.comboBox.setEnabled(state)
        self.navigationWidget.set_enable(state)
        self.stepWidget.set_enable(state)
        self.processWidget.set_enable(state)
        self.versionWidget.setEnabled(state)

    def project_signal(self, projectEntity):
        # this should be replaced by database
        if projectEntity:
            self.navigationWidget.populate(projectEntity)

    def project_clear_signal(self, state):
        if state:
            self.navigationWidget.clear()
            self.stepWidget.comboBox_activate()

    def navigation_signal(self, assetEntity):
        """ when asset show, send assetEntity """
        # list step by use assetEntity to compose path
        info = sg_utils.asset_info(assetEntity)
        context = context_info.Context()
        context.update(project=info['project'],
                        entityType='asset',
                        entityGrp=info['assetType'],
                        entity=info['name']
                        )

        asset = context_info.ContextPathInfo(context=context)
        stepPath = os.path.split(asset.path.step().abs_path())[0]
        self.stepWidget.list_items(stepPath)


    def step_signal(self, path):
        """ step events """
        asset = context_info.ContextPathInfo(path=path)
        processPath = os.path.split(asset.path.process().abs_path())[0]
        self.processWidget.list_items(processPath)

    def process_signal(self, path):
        """ process events """
        return
        process = str(self.processWidget.comboBox.currentText())
        self.asset.context.update(permanent=True, process=process, app='maya')
        # self.confirm_data()

    def export_signal(self, state):
        # self.publishButton.setEnabled(state)

        # set widget color guide
        self.set_widget_guide(self.outputWidget.exportButton, not state, bg=True)
        self.check_file_register(self.asset)

    def res_signal(self, res):
        """ res changed signal """
        if not res == self.outputWidget.blankRes:
            self.asset.context.update(permanent=True, res=res)
            self.check_file_register(self.asset)
            self.preregInfo = publish_info.PreRegister(self.asset)


    def file_version(self, asset):
        """ find file version """
        ptc = context_info.PathToContext(path=asset.path.path())
        self.versionWidget.setText(ptc.version)


    def check_file_register(self, entity):
        self.regInfo = publish_info.RegisterInfo(entity)
        content = self.regInfo.content.get(entity.version)
        regVersion = content.get(publish_info.Data.regVersion) if content else ''
        status = 'Not Register Daily only' if not content else 'Register %s' % regVersion
        color = Color.bgGreen if content else Color.bgYellow

        self.registerStatusLabel.setText(status)
        self.registerStatusLabel.setStyleSheet(color)
        self.registerStatus = True if regVersion else False
        self.set_publish_mode()

    def set_publish_mode(self):
        text = 'Publish' if self.registerStatus else 'Submit Daily'
        self.publishButton.setText(text)


    def set_task_list(self, asset):
        entityType = 'Asset'
        self.sgWidget.set_task(asset.project, entityType, asset.name, asset.step)


    def guess_task(self, asset):
        # guess task
        context = asset.read_env()
        asset2 = context_info.ContextPathInfo()
        asset2.context.context = context
        task = asset2.task
        tasks = self.sgWidget.set_current_task(task)


    def confirm_data(self):
        """ transfer complete entity to qc and output """
        # if auto, get from scene
        if self.autoAsset:
            self.asset = context_info.ContextPathInfo()
            self.displayWidget.set_display(self.asset)

        # if not, get from ui
        else:
            version = str(self.versionWidget.text())
            path = self.processWidget.current_item()

            # make self.asset global after confirm
            self.asset = context_info.ContextPathInfo(path=path)
            self.asset.context.update(app='maya', version=version)

        self.file_version(self.asset)
        self.set_task_list(self.asset)
        self.connect_qc(self.asset)
        self.guess_task(self.asset)
        return 



    def connect_qc(self, entity):
        """ run qc directly """
        self.qcWidget.update(project=entity.project, entityType=entity.entity_type, step=entity.step, entity=entity)
        self.outputWidget.update_entity(entity)
        self.outputWidget.add_res(entity.list_res())

        # set widget color guide
        self.set_widget_guide(self.qcWidget.checkButton, True, True)

    def color_guide(self):
        self.set_widget_guide(self.autoCheckBox, True, bg=False)

    def set_widget_guide(self, widget, state, bg=False):
        colorType = Color.bgGreen if bg else Color.green
        color = colorType if state else ''
        widget.setStyleSheet(color)


    # publish section
    def publish(self):
        """ publish method """
        QtWidgets.QApplication.processEvents()

        # user
        userEntity = self.userWidget.data()

        # set bg-red if not select
        status = self.sgWidget.check_info()
        captureStatus = self.snapWidget.check_complete()

        # description
        description = self.descriptionWidget.toPlainText()
        desColor = Color.bgRed if not description else ''
        self.descriptionWidget.setStyleSheet(desColor)

        if all([status, captureStatus, description]):

            taskEntity = self.sgWidget.get_task()
            status = self.sgWidget.get_status()

            # if files already exported, register asset data.
            if self.registerStatus:
                # publish and hero imgs path
                outputImgPath = self.asset.path.outputImg().abs_path()
                outputHeroImgPath = self.asset.path.output_hero_img().abs_path()

                # collect imgs
                imgs = self.collect_snap(outputImgPath)
                heroImgs = self.collect_snap(outputHeroImgPath, hero=True)
                dailyResult = daily.submit(self.asset, heroImgs)

                # create version
                versionResult = sg_hook.publish_version(self.asset, taskEntity, status, imgs, userEntity, description)
                taskResult = sg_hook.update_task(self.asset, taskEntity, status)

                # publish info
                self.asset.context.update(task=taskEntity.get('content'))
                publishInfo = publish_info.RegisterAsset(self.asset)
                regData = publishInfo.register()

                # lock workfiles
                workspace_utils.lock_workspace(self.asset)


            # if files not exported, data will not register. Submit to shotgun daily only.
            if not self.registerStatus:
                # publish and hero imgs path
                # override subversion
                publishVersion = self.asset.publishVersion
                self.asset.context.update(publishVersion=self.asset.subversion)

                outputImgPath = self.asset.path.outputImg().abs_path()
                outputHeroImgPath = self.asset.path.output_hero_img().abs_path()

                # collect imgs
                imgs = self.collect_snap(outputImgPath)
                heroImgs = self.collect_snap(outputHeroImgPath, hero=True)
                dailyResult = daily.submit(self.asset, heroImgs)

                # restore
                self.asset.context.update(publishVersion=publishVersion)

                # create version
                versionResult = sg_hook.publish_daily(self.asset, taskEntity, status, imgs, userEntity, description)
                taskResult = sg_hook.update_task(self.asset, taskEntity, status)

            self.publishButton.setEnabled(False)
            self.set_publish_status(all([versionResult, taskResult]))


    def set_publish_status(self, state):
        iconPath = icon.buttonCheck if state else icon.buttonFail
        dialog.MessageBox.success('Complete', 'Publish Complete')

    def collect_snap(self, outputImgPath, hero=False):
        imgs = self.snapWidget.get_all_items()
        imgName = self.asset.publish_name(ext='.png', hero=hero)
        status, outputs = utils.collect_imgs(outputImgPath, imgName, imgs)
        return outputs


def show():
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('%s Running in Maya\n' % _title)
        maya_win.deleteUI(uiName)
        myApp = RFAssetPublish(maya_win.getMayaWindow())
        return myApp
