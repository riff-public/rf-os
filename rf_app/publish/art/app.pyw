# v.0.0.1 beta
# v.0.0.3 add daily
# v.0.0.4 hide excessive elements
_title = 'Riff Art Publish'
_version = 'v.0.0.4'
_des = 'beta'
uiName = 'RFArtPublishUI'

#Import python modules
import sys
import os
import getpass
import json
import logging

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)

# import config
import rf_config as config

# framework modules
from rf_utils import log_utils
from rf_utils.ui import load
from rf_utils.ui import stylesheet

# import widgets
from rf_utils.widget import file_comboBox
from rf_utils.widget import browse_widget
from rf_utils.widget import display_widget
from rf_utils.widget import project_widget
from rf_utils.widget import entity_browse_widget
from rf_utils.widget import sg_widget
from rf_utils.widget import message_widget
from rf_utils import publish_info

# sg
from rf_utils.sg import sg_utils
from rf_utils.sg import sg_process
from rf_utils import project_info
from rf_utils.context import context_info
import utils
from rf_utils import daily
from rf_utils import user_info

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

class RFArtPublish(QtWidgets.QMainWindow):

    def __init__(self, project=None, auto=True, parent=None):
        #Setup Window
        super(RFArtPublish, self).__init__(parent)
        self.parent = parent
        self.auto = auto

        # info
        self.project = project
        self.projectInfo = project_info.ProjectInfo()
        self.app = 'psd'
        self.skipDesignShot = ['storyboard', 'colorscript']

        # ui read
        uiFile = '%s/ui.ui' % moduleDir
        self.ui = load.setup_ui(uiFile, self)

        self.ui.show()
        self.ui.setWindowTitle('%s %s-%s' % (_title, _version, _des))

        # ui
        self.setup_widgets()

        # navigation -> replace with database
        # config
        # sg cache
        self.sgEntity = dict()

        # hard coded
        self.navigationConfig = {'Two_Heroes': 'asset', 'projectName': 'asset/work', 'CloudMaker': 'all/asset', 'default': 'asset'}
        self.navigationDesignConfig = {'Two_Heroes': 'design', 'projectName': 'design/work', 'CloudMaker': 'design', 'SevenChickMovie': 'design/work', 'ThreeEyes': 'design/work', 'default': 'design'}

        # signals
        self.init_signals()

        # start logic
        self.init_functions()


    def setup_widgets(self):
        self.set_project_widget()
        self.design_navigation_widget()
        self.browsing_widget()
        self.preview_widget()
        self.button_widget()
        self.widget_vis(not self.auto)

    def set_project_widget(self):
        self.projectWidget = project_widget.ProjectComboBox(projectInfo=self.projectInfo, firstItem=self.project)
        self.ui.art_verticalLayout.addWidget(self.projectWidget)
        self.ui.art_verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.projectWidget.comboBox.setEnabled(not self.auto)


    def design_navigation_widget(self):
        # design
        self.designWidget = entity_browse_widget.AssetNavigation()
        self.designWidget.typeWidget.skipList = self.skipDesignShot
        self.designMessageWidget = message_widget.MessageLabel()

        # process part
        self.processLabel = QtWidgets.QLabel('Process')
        self.processWidget = QtWidgets.QLineEdit()
        # self.processWidget.setEditable(True)

        # sg area
        self.taskWidget = sg_widget.TaskComboBox(sg=sg_utils.sg)

        # layout
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.ui.art_verticalLayout.addWidget(self.designWidget)
        self.ui.art_verticalLayout.addWidget(self.processLabel)
        self.ui.art_verticalLayout.addWidget(self.processWidget)
        self.ui.art_verticalLayout.addWidget(self.taskWidget)
        self.ui.art_verticalLayout.addWidget(self.designMessageWidget)
        self.ui.art_verticalLayout.addItem(spacerItem)

        # set stretch task layout
        self.taskWidget.allLayout.setStretch(0, 0)
        self.taskWidget.allLayout.setStretch(1, 0)


    def browsing_widget(self):

        # browsing
        self.srcWidget = browse_widget.BrowseFile()
        self.srcWidget.set_label('Workspace File : ')
        self.srcWidget.filter = 'Media(*.psd *.png *.tif *.exr *.jpg *.jpeg)'
        self.publishWidget = browse_widget.BrowseFile()
        self.publishWidget.set_label('Output File : ')
        self.ui.path_verticalLayout.addWidget(self.srcWidget)
        self.ui.path_verticalLayout.addWidget(self.publishWidget)

    def preview_widget(self):
        # display
        self.displayWidget = display_widget.Display()
        self.displayWidget.set_label('JPG Here')
        self.ui.display_verticalLayout.addWidget(self.displayWidget)


    def button_widget(self):
        # button
        self.publishButton = QtWidgets.QPushButton('Publish')
        self.publishButton.setMinimumSize(QtCore.QSize(0, 30))
        self.ui.art_verticalLayout.addWidget(self.publishButton)

    def widget_vis(self, state):
        """ set visibility control """
        self.projectWidget.comboBox.setEnabled(state)
        self.designWidget.set_visible(state)
        self.srcWidget.set_visible(state)
        self.publishWidget.set_visible(state)
        self.processLabel.setVisible(state)
        self.processWidget.setVisible(state)
        self.projectWidget.projectComboBox.label.setVisible(state)
        self.designMessageWidget.label.setVisible(state)


    def init_signals(self):
        """ display signals """
        # display
        self.displayWidget.dropped.connect(self.display_trigger)
        self.publishWidget.textChanged.connect(self.publish_trigger)

        # navigation
        self.projectWidget.currentIndexChanged.connect(self.project_trigger)
        self.designWidget.completed.connect(self.design_trigger)


        # publish
        self.publishButton.clicked.connect(self.publish)


    def init_functions(self):
        self.projectWidget.list_project()


    def project_trigger(self, path):
        """ project comboBox trigger """
        project = str(self.projectWidget.comboBox.currentText())
        designSuffix = self.navigationDesignConfig.get(project, self.navigationDesignConfig.get('default'))
        designPath = ('/').join([path, designSuffix])
        assetSuffix = self.navigationConfig.get(project, self.navigationDesignConfig.get('default'))
        assetPath = ('/').join([path, assetSuffix])
        self.projectInfo = project_info.ProjectInfo(project=project)

        self.designWidget.list_items(designPath)

    def publish_trigger(self, path):
        """ publish lineEdit trigger """
        self.preview_thumbnail(path)
        self.auto_navigation(path)
        self.guess_psd(path)
        self.guess_process(path)

    def display_trigger(self, path):
        """ display signal """
        self.set_publish_path(path)
        self.auto_navigation(path)
        self.guess_psd(path)
        self.guess_process(path)

    def design_trigger(self, path):
        """ event after design widget complete the form """
        self.design = context_info.ContextPathInfo(path=path)
        self.asset = context_info.ContextPathInfo(path=path)
        project = self.design.project
        designType = self.design.type
        entityName = self.design.name
        step = self.projectInfo.context_key().get('design').get('name')
        entityType = self.projectInfo.context_key().get('asset').get('entity')

        if project and designType and entityName:
            self.activate_task(project, entityType, entityName, step)
            # self.sg_valid(project, entityType, entityName)


    def sg_valid(self, project, entityType, entityName):
        """ check if asset valid in sg """
        context = context_info.Context()
        context.use_sg(sg_utils.sg, project=project, entityType=entityType, entityName=entityName)
        self.sgEntity = context.sgEntity

        if not self.sgEntity:
            self.designMessageWidget.set_message('No asset in Shotgun')
            self.designMessageWidget.set_color([200, 0, 0])


    def activate_task(self, project, entityType, entityName, step):
        """ list task """
        result = self.taskWidget.activate(project, entityType, entityName, step)

        if not result:
            QtWidgets.QMessageBox.critical(self, 'Error', 'No "%s" found in Shotgun. Cannot continue.' % entityName)


    def guess_asset(self, project, entityType, entityName, step):
        """ guess asset """
        context = context_info.Context()
        context.use_sg(sg_utils.sg, project, entityType, entityName=entityName)
        asset = context_info.ContextPathInfo(context=context)
        result = False

        if asset.name == context.noContext:
            QtWidgets.QMessageBox.warning(self, 'Failed to get asset', 'No assset name "%s". Please select asset manually' % entityName)

        else:
            result1 = self.assetWidget.typeWidget.set_current(asset.type)
            result2 = self.assetWidget.subtypeWidget.set_current(asset.parent)
            result3 = self.assetWidget.entityWidget.set_current(asset.name)

            if not all([result1, result2, result3]):
                QtWidgets.QMessageBox.warning(self, 'Error', 'Cannot find asset "%s" type "%s" subtype "%s". Please select manually.' % (asset.name, asset.type, asset.parent))

            else:
                result = True
                self.messageWidget.set_message('Automatic link success \nAsset: %s\nType: %s\nSubtype: %s' % (asset.name, asset.type, asset.parent))

        if not result:
            self.messageWidget.set_message('Failed to find asset \nAsset: %s\nType: %s\nSubtype: %s\n Please select manually' % (asset.name, asset.type, asset.parent))
            self.messageWidget.set_color([200, 0, 0])
            # self.assetWidget.set_visible(True)



    def set_publish_path(self, path):
        """ when display change, update publish path """
        self.publishWidget.lineEdit.setText(path)

    def preview_thumbnail(self, path):
        """ when publish text changed, preview thumbnail """
        self.displayWidget.set_display(path)


    def pre_publish_design(self):
        designHero = self.design.path.scheme('publishHeroPath')


    def publish(self):
        logger.info('Publish starting ...\n')
        process = str(self.processWidget.text())
        workspaceFile = self.srcWidget.current_text()
        outputFile = self.publishWidget.current_text()
        taskEntity = self.taskWidget.current_item()

        if all([process, workspaceFile, outputFile, taskEntity]):
            files = self.publish_files(process, workspaceFile, outputFile)
            result = self.publish_sg(taskEntity, files)

            if result:
                msgBox = QtWidgets.QMessageBox()
                msgBox.setIconPixmap(QtGui.QPixmap(utils.IconPath.success))
                msgBox.setText('Publish complete')
                # msgBox.information(self, 'Complete', 'Publish complete')
                msgBox.exec_()

                # reset
                self.init_functions()
                self.taskWidget.clear()
                self.displayWidget.clear()

        else:
            QtWidgets.QMessageBox.warning(self, 'Error', 'Please select all information')


    def pre_register(self, design):
        """ preregister publish version """
        design.context.update(app='psd', step='design')
        self.publishInfo = publish_info.PreRegister(design)
        publishVersion = self.publishInfo.content.get(design.version).get(publish_info.Data.regVersion)
        logger.info('work version "%s" PreRegister to publish version "%s"' % (design.version, publishVersion))

        return self.publishInfo


    def publish_files(self, process, workspaceFile, outputFile):
        # files
        outputExt = os.path.splitext(outputFile)[-1]
        workspaceExt = os.path.splitext(workspaceFile)[-1]

        # set project env
        self.projectInfo.set_project_env()

        # registered version
        # self.design = context_info.ContextPathInfo(path=workspaceFile)
        self.design.context.update(process=process, res='md')
        publishInfo = self.pre_register(self.design)

        # publish work files
        savework = self.save_workspace(workspaceFile, outputFile)


        # publish files
        publishFile = context_info.RootPath(publishInfo.publish_file()).abs_path()
        publishWorkspace = context_info.RootPath(publishInfo.publish_workspace()).abs_path()
        publishWorkspace = '%s%s' % (os.path.splitext(publishWorkspace)[0], workspaceExt)

        # publish hero files
        publishHeroFile = context_info.RootPath(publishInfo.publish_file(hero=True)).abs_path()
        publishHeroWorkspace = context_info.RootPath(publishInfo.publish_hero_workspace()).abs_path()
        publishHeroWorkspace = '%s%s' % (os.path.splitext(publishHeroWorkspace)[0], workspaceExt)

        # add data
        self.design.context.update(process=process, app=self.app)
        self.asset.context.update(step='design', entityType='asset', process=process, app=self.app, episode='all')

        # design hero
        logger.debug('commit hero to design hero')
        srcDesign = utils.commit_hero_file(self.design, workspaceFile, 'publishHeroPath')
        outputDesign = utils.commit_hero_file(self.design, outputFile, 'publishHeroPath')
        logger.debug('%s\n%s' % (srcDesign, outputDesign))

        # all hero
        logger.debug('commit hero to all dir')
        outputAll = utils.commit_hero_file(self.design, outputFile, 'heroPath')
        logger.debug(outputAll)

        # asset hero
        logger.debug('commit hero to asset hero')
        outputName = self.design.publish_name(hero=True, ext=outputExt)
        outputAsset = utils.commit_hero_file(self.asset, outputFile, 'publishHeroPath', outputName)
        logger.debug(outputAsset)

        # publish version
        logger.debug('Publish workspace / output version')
        publishSrcDesign = utils.copy(srcDesign, publishWorkspace)
        publishDesign = utils.copy(outputFile, publishFile)
        logger.debug(publishSrcDesign)
        logger.debug(publishDesign)

        # hero publish
        logger.debug('Publish Hero workspace / output version')
        publishHeroSrcDesign = utils.copy(srcDesign, publishHeroWorkspace)
        publishHeroDesign = utils.copy(outputFile, publishHeroFile)
        logger.debug(publishHeroSrcDesign)
        logger.debug(publishHeroDesign)


        # submit to daily
        # dailyResult = utils.submit_daily(self.design, self.design.step, publishFile)
        dailyResult = daily.submit(self.design, [publishFile])
        logger.info('Copy to daily complete %s' % dailyResult)


        if all([outputDesign, outputAll, outputAsset, publishDesign, publishSrcDesign]):
            # QtWidgets.QMessageBox.information(self, 'Complete', 'Publish complete')
            logger.info('Publish %s files complete' % (len([outputDesign, outputAll, outputAsset, publishSrcDesign, publishDesign])))
            logger.info('%s\n%s\n%s' % (outputDesign, outputAll, outputAsset))
            logger.info('===========================')

            # registered this version
            publishInfo.register()
            publishAsset = publish_info.RegisterAsset(self.design)
            regData = publishAsset.register()

            return [outputDesign, outputAll, outputAsset, publishDesign, publishSrcDesign]

        else:
            QtWidgets.QMessageBox.warning(self, 'Not complete', 'Failed to publish')


    def publish_sg(self, taskEntity, outputFiles):
        # all inputs
        outputDesign, outputAll, outputAsset, publishDesign, publishSrcDesign = outputFiles
         # get data
        workspaceFile = self.srcWidget.current_text()
        versionName = os.path.splitext(os.path.basename(publishDesign))[0]
        status = 'rev'
        description = ''

        # get user
        user = user_info.User(sg_utils.sg)
        userEntity = user.sg_user()

        # set task to review
        dailyPath = daily.get_path(self.design)
        playlistEntity = sg_process.daily_playlist(self.design.project, self.design.step, dailyPath)
        projectEntity = taskEntity.get('project')
        entity = taskEntity.get('entity')

        result1 = sg_process.set_task_status(taskEntity.get('id'), status)
        result1 = sg_process.update_task(taskEntity.get('id'), status, 'hero', outputAsset)
        logger.info('Set task status complete')

        # versionEntity = sg_process.publish_version(projectEntity, entity, taskEntity, userEntity, versionName, status, description, playlistEntity)
        versionEntity = sg_process.update_version(projectEntity, entity, taskEntity, userEntity, versionName, status, description, playlistEntity, self.design.step, 'Published', workspaceFile)
        logger.info('Create version complete')
        result2 = sg_process.upload_version_media(versionEntity, publishDesign)
        logger.info('Upload media complete')
        # taskEntity = utils.set_task(sg_utils.sg, task=taskEntity, data)
        # utils.create_version(sg_utils.sg, taskEntity, publishDesign)
        return all([result1, versionEntity, result2])

    def save_workspace(self, workFile, outputFile):
        if self.design.project in ['SevenChickMovie']:
            workspacePath = self.design.path.workspace().abs_path()
            workExt = workFile.split('.')[-1]
            self.design.context.update(app=workExt, version='v001_001')
            workName = '%s/%s' % (workspacePath, self.design.work_name())

            outputExt = outputFile.split('.')[-1]
            self.design.context.update(app=outputExt, version='v001_001')
            outputName = '%s/%s' % (workspacePath, self.design.work_name())

            if not workFile == workName:
                result1 = utils.copy(workFile, workName)
                logger.debug('Copy workspace %s' % workName)

            if not workName == outputName and not outputFile == outputName:
                result2 = utils.copy(outputFile, outputName)
                logger.debug('Copy output %s' % outputName)

            self.design.context.update(app='psd')

            return True


    def auto_navigation(self, path):
        """ auto complete nagivation """
        design = context_info.ContextPathInfo(path=path)
        # check if enough infomation
        project = design.project
        assetType = design.type
        assetParent = design.parent
        name = design.name
        showWidgetPanel = False

        completeData = all([project, assetType, name])

        if completeData:
            result1 = self.projectWidget.projectComboBox.set_current(project)
            result2 = self.designWidget.typeWidget.set_current(assetType)
            result2 = self.designWidget.subtypeWidget.set_current(assetParent)
            result3 = self.designWidget.entityWidget.set_current(name)
            result = all([result1, result2, result3])

            if not result:
                message = 'Cannot find project and design name %s. Please select Manually' % name
                QtWidgets.QMessageBox.critical(self, 'Error', 'Auto find design failed. No asset match name "%s", "%s", "%s"' % (project, assetType, name))
                self.designMessageWidget.set_message(message)
                showWidgetPanel = True

            else:
                self.designMessageWidget.set_message('Complete')
                self.designMessageWidget.reset_color()

        else:
            message = 'Cannot find project and design name %s. Please select Manually' % name
            # QtWidgets.QMessageBox.critical(self, 'Error', message)
            logger.warning('Not detected any design, please select from the list')
            showWidgetPanel = True

            # message
            self.designMessageWidget.set_message('Please select manually')
            self.designMessageWidget.set_color([200, 0, 0])

        if showWidgetPanel:
            self.widget_vis(showWidgetPanel)


    def guess_psd(self, path):
        """ guess psd file """
        psdPath = utils.guess_psd(path)
        workspaceFile = psdPath if os.path.exists(psdPath) else path
        self.srcWidget.lineEdit.setText(workspaceFile)


    def guess_process(self, path):
        process = utils.guess_process(path)
        self.processWidget.setText(process)


def show(project=None, auto=True):
    logger.info('RFArtPublish Starting ...')
    logger.info('---------------------------')
    logger.info('---------------------------')
    app = QtWidgets.QApplication.instance()
    if not app:
        app = QtWidgets.QApplication(sys.argv)
    myApp = RFArtPublish(project=project, auto=auto)
    stylesheet.set_default(app)
    sys.exit(app.exec_())

if __name__ == '__main__':
    show()
