import os
import sys
import traceback
import time
from collections import OrderedDict

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_utils.widget.app import display_reel_widget
from rf_utils.context import context_info
from rf_utils import publish_info
from rf_utils import register_entity
from rf_utils import daily
from rf_utils.pipeline import watermark
# reload(watermark)
from rf_app.publish.utils import design_publish_utils
from rf_app.publish.asset import sg_hook

class Color:
    red = 'color: rgb(200, 0, 0);'
    green = 'color: rgb(0, 240, 0);'
    bgGreen = 'background-color: rgb(0, 140, 0);'
    bgYellow = 'background-color: rgb(200, 120, 0);'
    bgRed = 'background-color: rgb(140, 0, 0);'
    bgGrey = 'background-color: rgb(40, 40, 40);'

class LookdevWidget(QtWidgets.QWidget):
    status = QtCore.Signal(str)
    def __init__(self, sg=None) :
        super(LookdevWidget, self).__init__()
        self.sg = sg

        self.allLayout = QtWidgets.QVBoxLayout()
        self.set_ui()
        self.init_signals()


    def set_ui(self) :
        # radiobutton
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        # layout
        self.headLayout = QtWidgets.QHBoxLayout()
        self.headLayout.addItem(spacerItem)

        self.headLayout.setStretch(0, 1)
        self.headLayout.setStretch(1, 1)
        self.headLayout.setStretch(2, 2)

        # snap
        self.displayWidget = display_reel_widget.DisplayImages()
        self.displayWidget.displayWidget.set_style(Color.bgGrey)
        self.allLayout.addLayout(self.headLayout)
        self.allLayout.addWidget(self.displayWidget)
        self.displayWidget.multiple_file(False)

        # button
        self.publishButton = QtWidgets.QPushButton('Publish')
        self.publishButton.setMinimumSize(QtCore.QSize(0, 30))

        # info label
        self.infoLabel = QtWidgets.QLabel()

        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.bottomLayout = QtWidgets.QHBoxLayout()
        self.bottomLayout.addWidget(self.infoLabel)
        self.bottomLayout.addItem(spacerItem)
        self.bottomLayout.addWidget(self.publishButton)
        self.bottomLayout.setStretch(0, 1)
        self.bottomLayout.setStretch(1, 1)
        self.bottomLayout.setStretch(2, 1)
        self.allLayout.addLayout(self.bottomLayout)

        self.setLayout(self.allLayout)

    def init_signals(self):
        # storyboard
        self.displayWidget.dropped.connect(self.drop_signal)

    def drop_signal(self, paths):
        """ event when drop imgs to stroyboard panel """
        self.infoLabel.setText('%s files' % len(paths))

    def publish(self, entity, taskEntity, userEntity, status, process):
        # input ContextPathInfo
        projectEntity = entity['project']
        assetName = entity['code']
        inputImgs = self.displayWidget.get_all_items()
        outputExt = inputImgs[0].split('.')[-1] if inputImgs else 'png'
        workspaceFile = self.guess_workspace_file(inputImgs)
        # print outputExt
        # calculate version
        version = design_publish_utils.find_next_version(projectEntity, taskEntity, entity)
        # context
        self.asset = self.setup_context(projectEntity['name'], assetName, version, taskEntity['content'], outputExt, process)
        if self.asset.project == 'CloudMaker':
            self.asset.context.update(episode ='all')

        process_images = list(inputImgs)
        for i, img in enumerate(inputImgs):
            # add info watermark using cv2
            result = watermark.add_border_hud_to_image(input_path=img, 
                                                    output_path='',  # write to temp
                                                    topLeft=[self.asset.project],
                                                    topMid=[],
                                                    topRight=['Date: {}'.format(time.strftime('%d %b %Y - %H:%M:%S'))],
                                                    bottomLeft=['user: {}'.format(userEntity['name']), 
                                                            'department: {}'.format(self.asset.step), 
                                                            'file: {}'.format(os.path.basename(img))],
                                                    bottomMid=[self.asset.name],
                                                    bottomRight=['type: {}'.format(self.asset.type),
                                                            'LOD: {}'.format(self.asset.res), 
                                                            'process: {}'.format(self.asset.process)])
            if not result:
                logger.warning('Failed to add border HUD: {}'.format(img))
                continue
            process_images[i] = result

        # register process to get publish version
        reg = publish_info.PreRegister(self.asset)
        reg.register()
        self.print_out('Registered process')
        # publish File
        self.print_out('Publishing files ...')
        publishedImgs, publishedHeroImgs = design_publish_utils.publish_files(self.asset, process_images)


        self.print_out('Published files')
        # register files
        regInfo = register_entity.Register(self.asset)
        output = register_entity.Output()
        output.add_process(self.asset.process)
        # output.add_design(publishedImgs[0], publishedHeroImgs[0]) if publishedImgs and publishedHeroImgs else None
        output.add_media(os.path.dirname(publishedImgs[0]), os.path.dirname(publishedHeroImgs[0])) if publishedImgs and publishedHeroImgs else None
        regInfo.set(output=output)
        regInfo.register()

        # publishShotgun
        description = ''
        self.print_out('Publishing / Uploading media to Shotgun ...')
        versionResult = sg_hook.publish_version(self.asset, taskEntity, status, publishedImgs, userEntity, description, uploadOption='downsize')
        self.print_out('Published version')

        taskResult = sg_hook.update_task(self.asset, taskEntity, status)
        self.print_out('Update task')

        # register shot
        self.print_out('Registering Shot data ...')
        publishAsset = publish_info.RegisterAsset(self.asset)
        regData = publishAsset.register()
        self.print_out('Complete')

    def guess_workspace_file(self, inputImgs):
        """ looking for psd """
        ext = '.psd'
        psd = ['%s%s' % (a.split('.')[0], ext) for a in inputImgs if os.path.exists('%s%s' % (a.split('.')[0], ext))]
        psd = psd[0] if psd else inputImgs[0]
        return psd


    def setup_context(self, project, assetName, version, taskName, ext, process):
        """ setup contextPathInfo """
        context = context_info.Context()
        context.use_sg(sg=self.sg, project=project, entityType='asset', entityName=assetName)

        step = 'lookdev'
        app = ext
        context.update(step=step, process=process, app=app, version=version, res='md', task=taskName, entityType='asset')
        scene = context_info.ContextPathInfo(context=context)
        return scene


    def print_out(self, message):
        logger.info(message)
        print message
        self.status.emit(message)








