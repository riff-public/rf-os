import os
import sys
import traceback
import itertools
import time
from datetime import datetime
from collections import OrderedDict

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.StreamHandler())
logger.setLevel(logging.DEBUG)

from rf_utils.widget.app import display_reel_widget
# reload(display_reel_widget)
from rf_utils.widget import textEdit
from rf_utils.context import context_info
from rf_utils import publish_info
from rf_utils import register_entity
from rf_utils import daily
from rf_utils.sg import sg_process
from rf_utils.pipeline import watermark
# reload(watermark)
from rf_app.publish.utils import design_publish_utils
from rf_app.publish.asset import sg_hook

class Color:
    red = 'color: rgb(200, 0, 0);'
    green = 'color: rgb(0, 240, 0);'
    bgGreen = 'background-color: rgb(0, 140, 0);'
    bgYellow = 'background-color: rgb(200, 120, 0);'
    bgRed = 'background-color: rgb(140, 0, 0);'
    bgGrey = 'background-color: rgb(40, 40, 40);'

class Extensions:
    workspace_ext = ['.psd']
    output_ext = ['.jpg', '.png']

class DesignWidget(QtWidgets.QWidget):
    title_font_size = 20
    status = QtCore.Signal(str)
    def __init__(self, sg=None) :
        super(DesignWidget, self).__init__()
        self.sg = sg

        self.allLayout = QtWidgets.QVBoxLayout()
        self.set_ui()
        self.init_signals()

    def set_ui(self) :
        self.allLayout.setSpacing(8)
        self.allLayout.setContentsMargins(10, 10, 10, 0)

        # snap
        self.display_layout = QtWidgets.QVBoxLayout()
        self.allLayout.addLayout(self.display_layout)

        self.displayWidget = display_reel_widget.DisplayImages()
        self.displayWidget.multiple_file(True)
        self.displayWidget.allLayout.setContentsMargins(0, 0, 0, 0)
        self.displayWidget.displayWidget.set_style(Color.bgGrey)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.displayWidget.setSizePolicy(sizePolicy)
        self.display_layout.addWidget(self.displayWidget)

        self.title_label = QtWidgets.QLabel('N/A')
        self.title_label.setStyleSheet('color: grey; font-size: {}px'.format(self.title_font_size))
        self.title_label.setAlignment(QtCore.Qt.AlignVCenter|QtCore.Qt.AlignHCenter)
        self.display_layout.addWidget(self.title_label)
        
        # description 
        self.desc_groupBox = QtWidgets.QGroupBox('Description')
        self.allLayout.addWidget(self.desc_groupBox)

        self.description_layout = QtWidgets.QVBoxLayout(self.desc_groupBox)
        self.description_textEdit = textEdit.TextEditWithPlaceHolder()
        self.description_textEdit.setPlaceholderText('Some information about your publish...')
        self.description_textEdit.setWordWrapMode(QtGui.QTextOption.WrapAtWordBoundaryOrAnywhere)
        self.description_textEdit.setTextInteractionFlags(QtCore.Qt.TextEditorInteraction)
        self.description_layout.addWidget(self.description_textEdit)

        # bottom layout 
        self.bottomLayout = QtWidgets.QHBoxLayout()
        self.bottomLayout.setContentsMargins(0, 0, 0, 0)
        self.allLayout.addLayout(self.bottomLayout)

        # info label
        self.infoLabel = QtWidgets.QLabel()
        self.bottomLayout.addWidget(self.infoLabel)

        spacerItem = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.bottomLayout.addItem(spacerItem)

        # button
        self.publishButton = QtWidgets.QPushButton('Publish')
        self.publishButton.setMinimumSize(QtCore.QSize(160, 35))
        self.bottomLayout.addWidget(self.publishButton)

        self.allLayout.setStretch(0, 5)
        self.allLayout.setStretch(1, 1)
        self.allLayout.setStretch(2, 1)
        self.setLayout(self.allLayout)

    def init_signals(self):
        self.displayWidget.dropped.connect(self.input_changed)
        self.displayWidget.deleted.connect(self.input_changed)

    def input_changed(self, *args, **kwargs):
        """ event when drop imgs to stroyboard panel """
        paths = self.displayWidget.get_all_items()
        workspaceFiles, outputFiles = self.guess_workspace_file(paths)
        all_workspaceFiles = list(itertools.chain.from_iterable(workspaceFiles.values()))
        self.infoLabel.setText('%s workspace file(s), %s output file(s)' %(len(all_workspaceFiles), len(outputFiles)))

    def check_inputs(self):
        allInputImgs = self.displayWidget.get_all_items()
        if not allInputImgs:
            return False
        workspaceFiles, outputFiles = self.guess_workspace_file(allInputImgs)
        if not outputFiles:
            return False
        return True

    def update_label(self, entity):
        if entity:
            self.title_label.setText(entity['code'])
            self.title_label.setStyleSheet('color: yellow; font-size: {}px;'.format(self.title_font_size))
        else:
            self.title_label.setText('N/A')
            self.title_label.setStyleSheet('color: grey; font-size: {}px'.format(self.title_font_size))

    def get_start_due_date(self):
        # get due date for hud
        result = sg_process.get_task_from_context(self.asset)
        not_available_str = 'N/A'
        due_date = result.get('due_date', not_available_str)
        start_date = result.get('start_date', not_available_str)
        # start date
        start_date_text = 'Start: N/A'
        if start_date not in ('', None, 'N/A'):
            try:
                dt = datetime.strptime(start_date, '%Y-%m-%d').date()
                start_date = dt.strftime('%d %b %Y')
            except Exception:
                start_date = 'N/A'
            start_date_text = 'Start: {}'.format(start_date)

        # due date
        due_date_text = 'Due: N/A'
        if due_date not in ('', None, 'N/A'):  
            try:
                dt = datetime.strptime(due_date, '%Y-%m-%d').date()
                due_date = dt.strftime('%d %b %Y')
            except Exception:
                due_date = 'N/A'
            due_date_text = 'Due: {}'.format(due_date)
        start_due_date_text = '{} {}'.format(start_date_text, due_date_text)
        return start_due_date_text

    def publish(self, entity, taskEntity, userEntity, status, process, register=True):
        # input ContextPathInfo
        projectEntity = entity['project']
        assetName = entity['code']
        allInputImgs = self.displayWidget.get_all_items()
        if not allInputImgs:
            return
        first_img = allInputImgs[0]

        # get description
        description = self.description_textEdit.toPlainText()

        self.print_out('\n===========================================')
        self.print_out('===== Publishing: {} - {}'.format(assetName, process))

        # find ext from first file
        fn, outputExt = os.path.splitext(first_img)
        if not outputExt:
            outputExt = 'png'
        else:
            outputExt = outputExt[1:]  # leave out the . in the beginning

        # go thru each image dropped, return separately workspace files and output files
        # workspaceFiles = {ext1: [path1, path2, ...], ext2: [...]}
        # outputFiles = [path1, path2, ...]
        workspaceFiles, outputFiles = self.guess_workspace_file(allInputImgs)

        # calculate version
        version = design_publish_utils.find_next_version(projectEntity, taskEntity, entity)

        # context
        self.asset = self.setup_context(projectEntity['name'], assetName, version, taskEntity['content'], outputExt, process)

        date_text = 'Date: {}'.format(time.strftime('%d %b %Y - %H:%M:%S'))
        start_due_date_text = self.get_start_due_date()
        # --- add border HUD
        self.print_out('Adding border HUD to images ...')
        hud_imgs = []
        for img in outputFiles:
            # add info watermark using cv2
            result = watermark.add_border_hud_to_image(input_path=img, 
                                                    output_path='',  # write to temp
                                                    topLeft=[self.asset.project],
                                                    topMid=[],
                                                    topRight=[date_text, start_due_date_text],
                                                    bottomLeft=['user: {}'.format(userEntity['name']), 
                                                            'department: {}'.format(self.asset.step), 
                                                            'file: {}'.format(os.path.basename(img))],
                                                    bottomMid=[self.asset.name],
                                                    bottomRight=['type: {}'.format(self.asset.type),
                                                            'LOD: {}'.format(self.asset.res), 
                                                            'process: {}'.format(self.asset.process)])
            if not result:
                # print 'Failed to add border HUD: {}'.format(img)
                logger.warning('Failed to add border HUD: {}'.format(img))
            else:
                img = result
            hud_imgs.append(img)
        self.print_out('HUD add complete')

        # --- register process to get publish version
        self.print_out('Registering process ...')
        if register:
            reg = publish_info.PreRegister(self.asset)
            reg.register()
            self.print_out('Registered process')
        else:
            self.asset.context.update(publishVersion=version)

        # ----- publish File
        self.print_out('Publishing files ...')
        publishedImgs, publishedHeroImgs = design_publish_utils.art_publish_files(self.asset, workspaceFiles, hud_imgs)
        self.print_out('File publish complete')

        # ----- register files
        self.print_out('Registering entity data ...')
        regInfo = register_entity.Register(self.asset)
        output = register_entity.Output()
        output.add_process(self.asset.process)
        output.add_design(publishedImgs[0], publishedHeroImgs[0]) if publishedImgs and publishedHeroImgs else None
        output.add_media(os.path.dirname(publishedImgs[0]), os.path.dirname(publishedHeroImgs[0])) if publishedImgs and publishedHeroImgs else None
        regInfo.set(output=output)
        regInfo.register()
        self.print_out('Entry registered')

        # ----- publishShotgun
        self.print_out('Uploading media to Shotgun ...')
        versionResult = sg_hook.publish_version(self.asset, taskEntity, status, publishedImgs, userEntity, description)
        self.print_out('Shotgun uploaded')

        taskResult = sg_hook.update_task(self.asset, taskEntity, status)
        self.print_out('Shotgun task updated')

        # ----- register publish info
        self.print_out('Registering publish data ...')
        publishAsset = publish_info.RegisterAsset(self.asset)
        regData = publishAsset.register()
        self.print_out('Publish data registered')
        
        # ----- send bot notification
        # if user select status daily --> send only within department
        # if user select status approve --> send within deparment and downsteam
        design_publish_utils.design_notification(self.asset, taskEntity, userEntity, versionResult, description)

        # reset info label
        self.infoLabel.setText('')
        self.print_out('Publish Complete: {}'.format(versionResult['code']))

    def guess_workspace_file(self, inputImgs):
        """ looking for psd """
        workspace_files = {}
        # add all possible extensions to workspace files dict
        for ext in Extensions.workspace_ext:
            workspace_files[ext] = set()

        output_files = set()
        for img in inputImgs:
            fn, ext = os.path.splitext(img)
            if ext in Extensions.workspace_ext:
                workspace_files[ext].add(img)
            else:
                output_files.add(img)

        # sort output files
        output_files = sorted(list(output_files))
        # convert workspace files to list
        for ext, fileset in workspace_files.iteritems():
            workspace_files[ext] = list(fileset)

        return workspace_files, output_files

    def setup_context(self, project, assetName, version, taskName, ext, process):
        """ setup contextPathInfo """
        context = context_info.Context()
        context.use_sg(sg=self.sg, project=project, entityType='asset', entityName=assetName)

        step = 'design'
        app = ext
        context.update(step=step, process=process, app=app, version=version, res='md', task=taskName, entityType='design')
        scene = context_info.ContextPathInfo(context=context)
        return scene

    def print_out(self, message):
        logger.info(message)
        self.status.emit(message)








