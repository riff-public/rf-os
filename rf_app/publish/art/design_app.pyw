# v.0.1.0 beta
# v.0.2.0 minor color change
# v.0.4.0 add animatic and dialog check
# v.0.5.0 multiple image publish for design
# v.0.6.0 add description in design tab
# v.0.7.0 UI upgrade
# v.0.8.0 Add start, due date to burn-in

_title = 'Riff Asset Design Publish'
_version = 'v.0.8.0'
_des = ''
uiName = 'RFAssetDesignPublish'

#Import python modules
import sys
import os
import getpass
import json
import logging

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)

# import config
import rf_config as config

# framework modules
from rf_utils import log_utils
from rf_utils.ui import load
from rf_utils.ui import stylesheet
from rf_utils import icon

# import widgets
from rf_utils import publish_info
from rf_utils.widget import user_widget
from rf_utils.widget import entity_browser
from rf_utils.widget import entity_browse_widget
from rf_utils.widget import sg_widget
from rf_utils.widget import status_widget
from rf_utils.widget import display_widget
from rf_utils.widget import dialog
import design_widget
import lookdev_widget
import mattePaint_widget
import keyvis_widget
import mattePaintComp_widget
# sg
from rf_utils.sg import sg_utils
from rf_utils.context import context_info
from rf_utils import daily
from rf_utils import user_info
from rf_utils.pipeline import user_pref

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui


class RBGColor: 
    red = [200, 0, 0]
    green = [0, 240, 0]
    grey = [120, 120, 120]
    lightGrey = [200, 200, 200]
    bgGreen = [0, 140, 0]
    bgYellow = [200, 120, 0]
    bgRed = [140, 0, 0]
    bgGrey = [40, 40, 40]

class Color:
    red = 'color: rgb(200, 0, 0);'
    green = 'color: rgb(0, 240, 0);'
    yellow = 'color: rgb(200, 120, 60);'
    grey = 'color: rgb(120, 120, 120);'
    lightGrey = 'color: rgb(200, 200, 200);'
    bgGreen = 'background-color: rgb(0, 140, 0);'
    bgYellow = 'background-color: rgb(200, 120, 0);'
    bgRed = 'background-color: rgb(140, 0, 0);'
    bgGrey = 'background-color: rgb(40, 40, 40);'

class Process:
    sketch = 'sketch'
    color = 'color'
    detail = 'detail'
    names = [sketch, color, detail]

class RFAssetDesignPublish(QtWidgets.QMainWindow):

    def __init__(self, parent=None):
        #Setup Window
        super(RFAssetDesignPublish, self).__init__(parent)
        self.window_icon = QtGui.QIcon('{}/icons/main_icon.png'.format(moduleDir))

        # ui read
        uiFile = '%s/design_ui.ui' % moduleDir
        self.ui = load.setup_ui(uiFile, self)

        # data
        self.userPref = user_pref.ToolSetting(uiName)
        self.userData = self.userPref.read()

        # ui
        # tabs
        self.designTab = 'Design'
        self.lookdevTab = 'lookdev'
        self.mattePaintTab = 'MattePaint Layout'
        self.mattePaintCompTab = 'MattePaint Comp'
        self.keyvisTab = 'Keyvis'
        self.stepMap = {self.designTab: 'design',  self.keyvisTab: 'design', self.lookdevTab: 'lookdev', self.mattePaintTab: 'design', self.mattePaintCompTab: 'design'}
        self.stepTypeMap = {'keyvis': 'keyvis', 'default': 'design'}
        self.tabIndex = [self.designTab, self.lookdevTab, self.mattePaintTab, self.keyvisTab]

        self.setup_ui()
        self.add_tabs()
        self.init_signals()
        self.set_default()

    def setup_ui(self):
        self.ui.show()
        self.ui.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.ui.setWindowIcon(self.window_icon)

        # ----- logo widget
        self.logo = QtWidgets.QLabel()
        self.ui.head_layout.addWidget(self.logo)
        self.logo.setPixmap(QtGui.QPixmap(icon.logo).scaled(64, 64, QtCore.Qt.KeepAspectRatio))

        spacerItem = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.ui.head_layout.addItem(spacerItem)

        # ----- user widget
        self.userWidget = user_widget.UserComboBox()
        self.userWidget.setMinimumWidth(180)
        self.ui.head_layout.addWidget(self.userWidget)

        # # reviewer widget
        # self.reviewerWidget = user_widget.SGReviewerComboBox()
        # self.reviewerWidget.setMinimumWidth(148)
        # self.ui.head_layout.addWidget(self.reviewerWidget)

        self.ui.head_layout.setStretch(0, 0)
        self.ui.head_layout.setStretch(1, 1)
        self.ui.head_layout.setStretch(2, 0)
        # self.ui.head_layout.setStretch(3, 0)

        # ----- splitter
        self.mainSplitter = QtWidgets.QSplitter()

        self.mainSplitter.setOrientation(QtCore.Qt.Horizontal)

        self.navSplitWidget = QtWidgets.QWidget(self.mainSplitter)
        self.tabSplitWidget = QtWidgets.QWidget(self.mainSplitter)
        self.ui.view_layout.addWidget(self.mainSplitter)

        self.navigation_layout = QtWidgets.QVBoxLayout(self.navSplitWidget)
        self.navigation_layout.setSpacing(5)
        self.navigation_layout.setContentsMargins(5, 5, 5, 5)

        self.tab_layout = QtWidgets.QVBoxLayout(self.tabSplitWidget)
        self.mainSplitter.setSizes([100, 400])

       # ----- navigation
        # asset widget
        self.sgWidget = entity_browser.EntityTreeBrowser()
        self.sgWidget.search_lineEdit.setPlaceholderText('Search by name, tag or chapter...')
        # self.sgWidget.filtertype_comboBox.setMinimumWidth(75)
        self.sgWidget.filtertype_comboBox.setMaximumWidth(80)
        self.sgWidget.project_widget.setMinimumWidth(125)
        self.sgWidget.project_widget.setMaximumWidth(250)
        self.sgWidget.entity_widget.setMinimumHeight(250)
        self.sgWidget.layout.setContentsMargins(0, 0, 0, 0)
        
        # hide entity type, only asset
        self.sgWidget.entitytype_label.hide()
        self.sgWidget.entitytype_comboBox.hide()

        self.navigation_layout.addWidget(self.sgWidget)

        # ----- add task widget
        self.options_groupBox = QtWidgets.QGroupBox()
        self.navigation_layout.addWidget(self.options_groupBox)

        self.options_layout = QtWidgets.QVBoxLayout(self.options_groupBox)
        self.options_layout.setSpacing(8)
        self.options_layout.setContentsMargins(25, 15, 25, 15)

        self.taskWidget = sg_widget.TaskComboBox(sg=sg_utils.sg)
        self.taskWidget.allLayout.setSpacing(5)
        self.taskWidget.setMinimumWidth(100)
        self.taskWidget.setMaximumWidth(250)
        self.options_layout.addWidget(self.taskWidget)

        # show process checkBox 
        self.processVisCheckBox = QtWidgets.QCheckBox('Custom process')
        # self.processVisCheckBox.setMaximumWidth(150)
        self.options_layout.addWidget(self.processVisCheckBox)

        # ----- process widget
        self.processLabel = QtWidgets.QLabel('Process')
        self.options_layout.addWidget(self.processLabel)

        self.processWidget = QtWidgets.QComboBox()
        self.processWidget.setEditable(True)
        self.processWidget.lineEdit().setFocusPolicy(QtCore.Qt.ClickFocus)
        self.processWidget.addItems(Process.names)
        self.processWidget.setMinimumWidth(100)
        self.processWidget.setMaximumWidth(250)
        self.options_layout.addWidget(self.processWidget)

        # ----- status widget
        self.statusWidget = status_widget.TaskStatusWidget(layout=QtWidgets.QVBoxLayout())
        self.statusWidget.setMinimumWidth(100)
        self.statusWidget.setMaximumWidth(250)
        self.statusWidget.allLayout.setSpacing(5)
        self.statusWidget.label.setText('Status')
        self.options_layout.addWidget(self.statusWidget)
        
        # add spacer
        spacerItem = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.options_layout.addItem(spacerItem)

        self.options_layout.setStretch(0, 0)
        self.options_layout.setStretch(1, 0)
        self.options_layout.setStretch(2, 0)
        self.options_layout.setStretch(3, 0)
        self.options_layout.setStretch(4, 0)
        self.options_layout.setStretch(5, 5)

        # set stertches
        self.navigation_layout.setStretch(0, 0) # sg widget
        self.navigation_layout.setStretch(1, 0) # task widget
        # self.navigation_layout.setStretch(3, 0) # custom process checkbox
        self.navigation_layout.setStretch(2, 0) # options layout

        # ----- status label
        self.statusLabel = QtWidgets.QLabel()
        self.statusLabel.setMaximumHeight(24)
        self.ui.bottom_layout.addWidget(self.statusLabel)
        self.statusLabel.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)

        # # set style
        # self.processLabel.setStyleSheet(Color.grey)
        # self.taskWidget.label.setStyleSheet(Color.grey)
        # self.statusWidget.label.setStyleSheet(Color.grey)

    def add_tabs(self):
        self.tabWidget = QtWidgets.QTabWidget()
        
        # ----- design
        self.designTabWidget = QtWidgets.QWidget()
        self.designLayout = QtWidgets.QVBoxLayout(self.designTabWidget)
        self.designWidget = design_widget.DesignWidget(sg=sg_utils.sg)
        self.designLayout.addWidget(self.designWidget)
        self.tabWidget.addTab(self.designTabWidget, self.designTab)
        # setup
        self.designLayout.setContentsMargins(0, 0, 0, 0)
        self.designLayout.setSpacing(8)

        # ----- matte paint
        self.mattePaintTabWidget = QtWidgets.QWidget()
        self.mattePaintLayout = QtWidgets.QVBoxLayout(self.mattePaintTabWidget)
        self.mattePaintWidget = mattePaint_widget.MattePaintWidget(sg=sg_utils.sg)
        self.mattePaintLayout.addWidget(self.mattePaintWidget)
        self.tabWidget.addTab(self.mattePaintTabWidget, self.mattePaintTab)

        # ----- matte paint comp
        # self.mattePaintCompTabWidget = QtWidgets.QWidget()
        # self.mattePaintCompLayout = QtWidgets.QVBoxLayout(self.mattePaintCompTabWidget)
        # self.mattePaintWidget = mattePaintComp_widget.MattePaintWidget(sg=sg_utils.sg)
        # self.mattePaintCompLayout.addWidget(self.mattePaintWidget)
        # self.tabWidget.addTab(self.mattePaintCompTabWidget, self.mattePaintCompTab)

        # setup
        self.mattePaintLayout.setContentsMargins(0, 0, 0, 0)
        self.mattePaintLayout.setSpacing(8)

        # needs to insert tabs on top of status label
        self.tab_layout.insertWidget(0, self.tabWidget)

    def init_signals(self):
        # --- main ui
        self.sgWidget.entitySelected.connect(self.asset_selected)
        self.tabWidget.currentChanged.connect(self.refresh_asset)
        # input control
        self.taskWidget.comboBox.currentIndexChanged.connect(self.task_changed)
        self.processWidget.lineEdit().textChanged.connect(self.process_changed)
        # custom process 
        self.processVisCheckBox.stateChanged.connect(self.show_process)

        # --- tabs
        # design publish
        self.designWidget.publishButton.clicked.connect(self.design_publish)
        self.designWidget.status.connect(self.print_status)
        # matte paint publish
        self.mattePaintWidget.publishButton.clicked.connect(self.mattePaint_publish)
        self.mattePaintWidget.status.connect(self.print_status)
        
    def set_default(self):
        self.sgWidget.refresh()

        # hide process 
        self.show_process(value=False)

        # restore last selection
        self.restore_selection()

    def asset_selected(self, entityDict):
        if entityDict:
            project = entityDict['project']['name']
            entityType = 'Asset'
            entityName = entityDict['code']
            entityAssetType = entityDict['sg_asset_type']
            step = self.get_step_mode(entityAssetType)
            self.taskWidget.activate(project, entityType, entityName, step, entityAssetType=entityAssetType)
            self.sgWidget.entity_widget.treeWidget.setStyleSheet('')
            self.save_last_selection()
        # labels
        self.designWidget.update_label(entity=entityDict)
        self.mattePaintWidget.update_label(entity=entityDict)

    def refresh_asset(self, assetName):
        """ refresh and set current assetName """
        entityDict = self.sgWidget.current_entity()
        self.asset_selected(entityDict=entityDict)

    def design_publish(self):
        """ publish storyboard call """
        entity, taskEntity, status, userEntity, process = self.collect_ui_data()
        if self.check_input_ui(entity, taskEntity, userEntity, process):
            # throw error if there's no file
            if not self.designWidget.check_inputs():
                dialog.MessageBox.error('Error', 'No valid image to publish!', self.ui)
                return

            if not self.designWidget.description_textEdit.toPlainText():
                dialog.MessageBox.error('Error', 'Please fill some description for this publish!', self.ui)
                self.designWidget.description_textEdit.setStyleSheet('background-color: rgb(75, 15, 15)')
                return

            QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
            self.designWidget.publish(entity, taskEntity, userEntity, status, process)
            QtWidgets.QApplication.restoreOverrideCursor()

            message = 'Design publish complete'
            self.complete_dialog(message)
            self.designWidget.displayWidget.clear()
            self.designWidget.description_textEdit.clear()
            self.designWidget.description_textEdit.setStyleSheet('')

    def lookdev_publish(self):
        """ publish storyboard call """
        entity, taskEntity, status, userEntity, process = self.collect_ui_data()
        if self.check_input_ui(entity, taskEntity, userEntity, process):
            QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
            self.lookdevWidget.publish(entity, taskEntity, userEntity, status, process)
            QtWidgets.QApplication.restoreOverrideCursor()

            message = 'lookdev publish complete'
            self.complete_dialog(message)
            self.lookdevWidget.displayWidget.clear()

    def mattePaint_publish(self):
        """ publish storyboard call """
        entity, taskEntity, status, userEntity, process = self.collect_ui_data()

        if self.check_input_ui(entity, taskEntity, userEntity, process):
            QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
            self.mattePaintWidget.publish(entity, taskEntity, userEntity, status, process)
            QtWidgets.QApplication.restoreOverrideCursor()

            message = 'mattePaint publish complete'
            self.complete_dialog(message)
            self.mattePaintWidget.displayWidget.clear()
    
    def keyvis_publish(self):
        """ publish storyboard call """
        entity, taskEntity, status, userEntity, process = self.collect_ui_data()
        
        if self.check_input_ui(entity, taskEntity, userEntity, process):
            QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
            self.keyvisWidget.publish(entity, taskEntity, userEntity, status, process)
            QtWidgets.QApplication.restoreOverrideCursor()

            message = 'keyvis publish complete'
            self.complete_dialog(message)
            self.keyvisWidget.displayWidget.clear()

    def task_changed(self):
        self.taskWidget.comboBox.setStyleSheet('')
        task = self.taskWidget.comboBox.currentText()
        index = self.taskWidget.comboBox.currentIndex()
        self.processWidget.lineEdit().setText(task)
        if index != 0:
            self.processWidget.setEnabled(True)
            # unlock process 
            self.processVisCheckBox.setVisible(True)
            self.processWidget.setEnabled(True)
        else: 
            self.processWidget.setEnabled(False)
            self.processVisCheckBox.setVisible(False)

    def show_process(self, value): 
        self.processWidget.setVisible(value)
        self.processLabel.setVisible(value)

        # reset if state is false 
        if not value: 
            task = self.taskWidget.comboBox.currentText()
            index = self.taskWidget.comboBox.currentIndex()
            if not index == 0:
                self.processWidget.lineEdit().setText(task)

    def process_changed(self):
        self.processWidget.setStyleSheet('')

    def collect_ui_data(self):
        """ collect asset, task, status, user entity """
        entity = self.sgWidget.current_entity()
        taskEntity = self.taskWidget.current_item()
        status = self.statusWidget.current_item()
        userEntity = self.userWidget.data()
        process = str(self.processWidget.lineEdit().text())

        return entity, taskEntity, status, userEntity, process

    def check_input_ui(self, entity, taskEntity, userEntity, process):
        status = True
        messages = []
        if not entity:
            self.sgWidget.entity_widget.treeWidget.setStyleSheet(Color.bgRed)
            messages.append('No shot selected')
            status = False
        if not taskEntity:
            self.taskWidget.comboBox.setStyleSheet(Color.bgRed)
            messages.append('No task selected')
            status = False
        if not userEntity:
            messages.append('No task selected')
            status = False

        if not process:
            self.processWidget.setStyleSheet((Color.bgRed))
            messages.append('No process')
            status = False

        if not status:
            dialog.MessageBox.error('Error', ('\n').join(messages), self.ui)

        return status

    def print_status(self, message):
        self.statusLabel.setText(message)
        QtWidgets.QApplication.processEvents()

    def complete_dialog(self, message):
        title = 'Complete'
        result = dialog.MessageBox.success(title, message, self.ui)
        return result

    def get_step_mode(self, assetType):
        # we will change the logic here 
        # previously we change the tab to change step and map those tap with self.stepMap 
        # now everything will be publish through "Design" tab
        # we will separate "design", "keyvis" by type only 

        # index = self.tabWidget.currentIndex()
        # currentTab = self.tabWidget.tabText(index)
        # return self.stepMap[currentTab]

        # new implement 
        step = self.stepTypeMap['default']
        if assetType in self.stepTypeMap.keys(): 
            step = self.stepTypeMap[assetType]
        return step

    def save_last_selection(self):
        """ save selection to json file """
        data = dict()
        # project = str(self.projectWidget.projectComboBox.currentText())
        current_entity = self.sgWidget.current_entity()
        if current_entity:
            data['project'] = current_entity['project']['name']
            data['type'] = current_entity['sg_asset_type']
            data['asset'] = current_entity['code']
            result = self.userPref.write(data)

    def restore_selection(self):
        data = self.userData
        if not data:
            return

        # asset contexts
        project = data.get('project', None)
        filter_type = data.get('type', None)
        asset = data.get('asset', None)

        if project and filter_type and asset:
            # setup app
            self.sgWidget.blockSignals(True)
            # set project
            self.sgWidget.project_widget.set_current_item(project)
            # set entity type
            index = self.sgWidget.entitytype_comboBox.findText('Asset', QtCore.Qt.MatchFixedString)
            self.sgWidget.entitytype_comboBox.setCurrentIndex(index)
            # set type
            index = self.sgWidget.filtertype_comboBox.findText(filter_type, QtCore.Qt.MatchFixedString)
            self.sgWidget.filtertype_comboBox.setCurrentIndex(index)
            self.sgWidget.blockSignals(False)
            # set tag to all tags
            self.sgWidget.tag_widget.set_current_index(0)
            # set asset
            self.sgWidget.entity_widget.blockSignals(True)
            self.sgWidget.entity_widget.set_current(displayText=asset, signal=False)
            self.sgWidget.entity_widget.blockSignals(False)
            # explicit call to asset selected
            entity = self.sgWidget.entity_widget.current_item()
            if entity:
                self.asset_selected(entityDict=entity)

def show():
    logger.info('RFAssetDesignPublish Starting ...')
    logger.info('---------------------------')
    logger.info('---------------------------')
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('%s Running in Maya\n' % _title)
        maya_win.deleteUI(uiName)
        myApp = RFAssetDesignPublish(maya_win.getMayaWindow())
        return myApp
    else:
        app = QtWidgets.QApplication.instance()
        if not app:
            app = QtWidgets.QApplication(sys.argv)
        myApp = RFAssetDesignPublish()
        stylesheet.set_default(app)
        sys.exit(app.exec_())


def show2(index=0, parent=None): 
    myApp = RFAssetDesignPublish(parent)
    myApp.tabWidget.setCurrentIndex(index)


if __name__ == '__main__':
    show()
