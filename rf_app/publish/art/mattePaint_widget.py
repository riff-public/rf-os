import os
import sys
import traceback
import time
from collections import OrderedDict

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_utils.widget.app import display_reel_widget
from rf_utils.context import context_info
from rf_utils import publish_info
from rf_utils import register_entity
from rf_utils import daily
from rf_utils.pipeline import watermark
# reload(watermark)
from rf_app.publish.utils import design_publish_utils
from rf_app.publish.asset import sg_hook
from rf_utils import file_utils
from rf_utils.widget import dialog
from rf_utils import admin

class Color:
    red = 'color: rgb(200, 0, 0);'
    green = 'color: rgb(0, 240, 0);'
    bgGreen = 'background-color: rgb(0, 140, 0);'
    bgYellow = 'background-color: rgb(200, 120, 0);'
    bgRed = 'background-color: rgb(140, 0, 0);'
    bgGrey = 'background-color: rgb(40, 40, 40);'

class MattePaintWidget(QtWidgets.QWidget):
    title_font_size = 20
    status = QtCore.Signal(str)
    def __init__(self, sg=None) :
        super(MattePaintWidget, self).__init__()
        self.sg = sg

        self.allLayout = QtWidgets.QVBoxLayout()
        self.entity = None
        self.set_ui()
        self.init_signals()

    def set_ui(self):
        self.allLayout.setSpacing(8)
        self.allLayout.setContentsMargins(10, 10, 10, 0)

        self.display_layout = QtWidgets.QVBoxLayout()
        self.allLayout.addLayout(self.display_layout)

        # snap
        self.displayWidget = display_reel_widget.DisplayImages()
        self.displayWidget.allLayout.setContentsMargins(0, 0, 0, 0)
        self.displayWidget.displayWidget.set_style(Color.bgGrey)
        self.displayWidget.multiple_file(True)
        self.display_layout.addWidget(self.displayWidget)

        self.title_label = QtWidgets.QLabel('N/A')
        self.title_label.setStyleSheet('color: grey; font-size: {}px'.format(self.title_font_size))
        self.title_label.setAlignment(QtCore.Qt.AlignVCenter|QtCore.Qt.AlignHCenter)
        self.display_layout.addWidget(self.title_label)
        
        # file_browe_widget
        self.file_groupBox = QtWidgets.QGroupBox('File PSD')
        self.allLayout.addWidget(self.file_groupBox)

        self.file_layout = QtWidgets.QHBoxLayout(self.file_groupBox)
        self.file_layout.setContentsMargins(5, 20, 5, 25)
        self.file_layout.setSpacing(8)
        self.file_lineEdit = QtWidgets.QLineEdit()
        self.file_browe_pushButton = QtWidgets.QPushButton('Browse')
        self.file_layout.addWidget(self.file_lineEdit)
        self.file_layout.addWidget(self.file_browe_pushButton)

        self.file_png_groupBox = QtWidgets.QGroupBox('File PNG')
        self.allLayout.addWidget(self.file_png_groupBox)
        self.file_png_Vlayout = QtWidgets.QVBoxLayout(self.file_png_groupBox)

        self.file_png1_layout = QtWidgets.QHBoxLayout()
        self.file_png1_layout.setContentsMargins(9, 3, 9, 3)
        self.file_png1_layout.setSpacing(8)
        self.text_png1 = QtWidgets.QLabel('Layer 1 :')
        self.file_png1_lineEdit = QtWidgets.QLineEdit()
        self.file_png1_browe_pushButton = QtWidgets.QPushButton('Browse')
        self.file_png1_layout.addWidget(self.text_png1)
        self.file_png1_layout.addWidget(self.file_png1_lineEdit)
        self.file_png1_layout.addWidget(self.file_png1_browe_pushButton)

        self.file_png2_layout = QtWidgets.QHBoxLayout()
        self.file_png2_layout.setContentsMargins(9, 3, 9, 3)
        self.file_png2_layout.setSpacing(8)
        self.text_png2 = QtWidgets.QLabel('Layer 2 :')
        self.file_png2_lineEdit = QtWidgets.QLineEdit()
        self.file_png2_browe_pushButton = QtWidgets.QPushButton('Browse')
        self.file_png2_layout.addWidget(self.text_png2)
        self.file_png2_layout.addWidget(self.file_png2_lineEdit)
        self.file_png2_layout.addWidget(self.file_png2_browe_pushButton)

        self.file_png3_layout = QtWidgets.QHBoxLayout()
        self.file_png3_layout.setContentsMargins(9, 3, 9, 3)
        self.file_png3_layout.setSpacing(8)
        self.text_png3 = QtWidgets.QLabel('Layer 3 :')
        self.file_png3_lineEdit = QtWidgets.QLineEdit()
        self.file_png3_browe_pushButton = QtWidgets.QPushButton('Browse')
        self.file_png3_layout.addWidget(self.text_png3)
        self.file_png3_layout.addWidget(self.file_png3_lineEdit)
        self.file_png3_layout.addWidget(self.file_png3_browe_pushButton)

        self.file_png4_layout = QtWidgets.QHBoxLayout()
        self.file_png4_layout.setContentsMargins(9, 3, 9, 3)
        self.file_png4_layout.setSpacing(8)
        self.text_png4 = QtWidgets.QLabel('Layer 4 :')
        self.file_png4_lineEdit = QtWidgets.QLineEdit()
        self.file_png4_browe_pushButton = QtWidgets.QPushButton('Browse')
        self.file_png4_layout.addWidget(self.text_png4)
        self.file_png4_layout.addWidget(self.file_png4_lineEdit)
        self.file_png4_layout.addWidget(self.file_png4_browe_pushButton)

        self.file_png5_layout = QtWidgets.QHBoxLayout()
        self.file_png5_layout.setContentsMargins(9, 3, 9, 3)
        self.file_png5_layout.setSpacing(8)
        self.text_png5 = QtWidgets.QLabel('Layer 5 :')
        self.file_png5_lineEdit = QtWidgets.QLineEdit()
        self.file_png5_browe_pushButton = QtWidgets.QPushButton('Browse')
        self.file_png5_layout.addWidget(self.text_png5)
        self.file_png5_layout.addWidget(self.file_png5_lineEdit)
        self.file_png5_layout.addWidget(self.file_png5_browe_pushButton)

        self.file_png_Vlayout.addLayout(self.file_png1_layout)
        self.file_png_Vlayout.addLayout(self.file_png2_layout)
        self.file_png_Vlayout.addLayout(self.file_png3_layout)
        self.file_png_Vlayout.addLayout(self.file_png4_layout)
        self.file_png_Vlayout.addLayout(self.file_png5_layout)

        # bottom layout
        self.bottomLayout = QtWidgets.QHBoxLayout()
        self.bottomLayout.setContentsMargins(0, 0, 0, 0)
        self.allLayout.addLayout(self.bottomLayout)

        # info label
        self.infoLabel = QtWidgets.QLabel()
        self.bottomLayout.addWidget(self.infoLabel)

        # spacer
        spacerItem = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.bottomLayout.addItem(spacerItem)

        # button
        self.publishButton = QtWidgets.QPushButton('Publish')
        self.publishButton.setMinimumSize(QtCore.QSize(160, 35))
        self.bottomLayout.addWidget(self.publishButton)

        self.allLayout.setStretch(0, 5)
        self.allLayout.setStretch(1, 1)
        self.allLayout.setStretch(2, 1)
        self.allLayout.setStretch(3, 1)
        self.setLayout(self.allLayout)

    def browse_file(self):
        workPath = self.get_work_path()
        file_path = str(QtWidgets.QFileDialog.getOpenFileName(self, "Select Directory", workPath, filter=('PhotoShop(*.psd),(*psd;*.psb)'))[0])
        self.file_lineEdit.clear()
        self.file_lineEdit.setText(file_path)

    def browse_file_png1(self):
        workPath = self.get_work_path()
        file_path = str(QtWidgets.QFileDialog.getOpenFileName(self, "Select Directory", workPath, filter=('(*png;*.jpg)'))[0])
        self.file_png1_lineEdit.clear()
        self.file_png1_lineEdit.setText(file_path)

    def browse_file_png2(self):
        workPath = self.get_work_path()
        file_path = str(QtWidgets.QFileDialog.getOpenFileName(self, "Select Directory", workPath, filter=('(*png;*.jpg)'))[0])
        self.file_png2_lineEdit.clear()
        self.file_png2_lineEdit.setText(file_path)

    def browse_file_png3(self):
        workPath = self.get_work_path()
        file_path = str(QtWidgets.QFileDialog.getOpenFileName(self, "Select Directory", workPath, filter=('(*png;*.jpg)'))[0])
        self.file_png3_lineEdit.clear()
        self.file_png3_lineEdit.setText(file_path)

    def browse_file_png4(self):
        workPath = self.get_work_path()
        file_path = str(QtWidgets.QFileDialog.getOpenFileName(self, "Select Directory", workPath, filter=('(*png;*.jpg)'))[0])
        self.file_png4_lineEdit.clear()
        self.file_png4_lineEdit.setText(file_path)

    def browse_file_png5(self):
        workPath = self.get_work_path()
        file_path = str(QtWidgets.QFileDialog.getOpenFileName(self, "Select Directory", workPath, filter=('(*png;*.jpg)'))[0])
        self.file_png5_lineEdit.clear()
        self.file_png5_lineEdit.setText(file_path)

    def auto_fill_png(self):
        workPath = self.get_work_path()
        if os.path.exists(workPath):
            files = os.listdir(workPath)
            for f in files:
                if 'layer01' in f:
                    pic_path = os.path.join(workPath, f)
                    self.file_png1_lineEdit.clear()
                    self.file_png1_lineEdit.setText(pic_path)
                elif 'layer02' in f:
                    pic_path = os.path.join(workPath, f)
                    self.file_png2_lineEdit.clear()
                    self.file_png2_lineEdit.setText(pic_path)
                elif 'layer03' in f:
                    pic_path = os.path.join(workPath, f)
                    self.file_png3_lineEdit.clear()
                    self.file_png3_lineEdit.setText(pic_path)
                elif 'layer04' in f:
                    pic_path = os.path.join(workPath, f)
                    self.file_png4_lineEdit.clear()
                    self.file_png4_lineEdit.setText(pic_path)
                elif 'layer05' in f:
                    pic_path = os.path.join(workPath, f)
                    self.file_png5_lineEdit.clear()
                    self.file_png5_lineEdit.setText(pic_path)

    def publish_psd(self, version):
        file_path = self.file_lineEdit.text()
        if file_path:
            app = file_path.split('.')[-1]
            design = context_info.ContextPathInfo(path = file_path)

            ####hero
            design.context.update(process='mattePaint', app=app, publishVersion='hero')
            hero_dir_path = design.path.scheme(key='publishHeroWorkspacePath').abs_path()
            hero_file_path = design.name_format(key='publish')
            if 'psb' in hero_dir_path:
                hero_dir_path = hero_dir_path.replace('psb', 'psd')
            hero_path = os.path.join(hero_dir_path, hero_file_path)
            print 'hero_path', hero_path
            file_utils.copy(file_path, hero_path)
            ############version
            design.context.update(process='mattePaint', app=app, publishVersion= version)
            ver_dir_path = design.path.scheme(key='publishWorkspacePath').abs_path()
            ver_file_path = design.name_format(key='publish')
            if 'psb' in ver_dir_path:
                ver_dir_path = ver_dir_path.replace('psb', 'psd')
            ver_path = os.path.join(ver_dir_path, ver_file_path)
            if not os.path.exists(os.path.dirname(ver_path)):
                os.makedirs(os.path.dirname(ver_path))
            print 'ver_path', ver_path
            file_utils.copy(file_path, ver_path)

            # design.context.update(process='mattePaint', entityType='asset' ,app=app , publishVersion='hero', look='main')
            # texrenPath = design.path.publish_texture().abs_path() #P:/<proj>/asset/publ/mattepaint/<matteName>/texture/main/hero/texRen
            # hero_file_path = os.path.basename(file_path)
            # texrenHeroPath = os.path.join(texrenPath, hero_file_path)
            # print 'texren_path', texrenHeroPath
            # file_utils.copy(file_path, texrenHeroPath)
            
        else:
            self.allowMessage = 'No PSD found'
            dialog.MessageBox.error('Input PSD', self.allowMessage)

        self.file_lineEdit.clear()

    def publish_png_layer(self, version):
        files_path = []
        if self.file_png1_lineEdit.text():
            files_path.append(self.file_png1_lineEdit.text())
        if self.file_png2_lineEdit.text():
            files_path.append(self.file_png2_lineEdit.text())
        if self.file_png3_lineEdit.text():
            files_path.append(self.file_png3_lineEdit.text())
        if self.file_png4_lineEdit.text():
            files_path.append(self.file_png4_lineEdit.text())
        if self.file_png5_lineEdit.text():
            files_path.append(self.file_png5_lineEdit.text())

        if files_path:
            for file_path in files_path:
                if file_path:
                    design = context_info.ContextPathInfo(path = file_path)

                    # ####hero
                    # design.context.update(process='mattePaint', app='png', publishVersion='hero')
                    # hero_dir_path = design.path.scheme(key='publishHeroWorkspacePath').abs_path()
                    # hero_file_path = design.name_format(key='publish')
                    # hero_path = os.path.join(hero_dir_path, hero_file_path)
                    # print 'hero_path', hero_path
                    # file_utils.copy(file_path, hero_path)
                    # ############version
                    # design.context.update(process='mattePaint', app='png', publishVersion= version)
                    # ver_dir_path = design.path.scheme(key='publishWorkspacePath').abs_path()
                    # ver_file_path = design.name_format(key='publish')
                    # ver_path = os.path.join(ver_dir_path, ver_file_path)
                    # if not os.path.exists(os.path.dirname(ver_path)):
                    #     os.makedirs(os.path.dirname(ver_path))
                    # print 'ver_path', ver_path
                    # file_utils.copy(file_path, ver_path)

                    ###### publish texren
                    entity = design.copy()
                    entity.context.update(process='mattePaint', entityType='asset' ,app='png', publishVersion='hero', look='main')
                    #entity = context_info.ContextPathInfo(path = path)
                    #texturePath1 = entity.path.texture().abs_path() #P:/<proj>/asset/work/mattepaint/<matteName>/texture/main/images
                    texrenPath = entity.path.publish_texture().abs_path() #P:/<proj>/asset/publ/mattepaint/<matteName>/texture/main/hero/texRen
                    hero_file_path = os.path.basename(file_path)
                    texrenHeroPath = os.path.join(texrenPath, hero_file_path)
                    print 'texren_path', texrenHeroPath
                    # file_utils.copy(file_path, texrenHeroPath)
                    admin.copyfile(file_path, texrenHeroPath)
            
        else:
            self.allowMessage = 'No PNG found'
            dialog.MessageBox.error('Input PNG', self.allowMessage)

        self.file_png1_lineEdit.clear()
        self.file_png2_lineEdit.clear()
        self.file_png3_lineEdit.clear()
        self.file_png4_lineEdit.clear()
        self.file_png5_lineEdit.clear()


    def init_signals(self):
        # storyboard
        self.file_browe_pushButton.clicked.connect(self.browse_file)
        self.file_png1_browe_pushButton.clicked.connect(self.browse_file_png1)
        self.file_png2_browe_pushButton.clicked.connect(self.browse_file_png2)
        self.file_png3_browe_pushButton.clicked.connect(self.browse_file_png3)
        self.file_png4_browe_pushButton.clicked.connect(self.browse_file_png4)
        self.file_png5_browe_pushButton.clicked.connect(self.browse_file_png5)
        self.displayWidget.dropped.connect(self.drop_signal)

    def drop_signal(self, paths):
        """ event when drop imgs to stroyboard panel """
        self.infoLabel.setText('%s files' % len(paths))

    def update_label(self, entity):
        self.entity = entity
        if entity:
            # auto fill update png files
            self.auto_fill_png()
            
            self.title_label.setText(entity['code'])
            self.title_label.setStyleSheet('color: yellow; font-size: {}px;'.format(self.title_font_size))
        else:
            self.title_label.setText('N/A')
            self.title_label.setStyleSheet('color: grey; font-size: {}px'.format(self.title_font_size))

    def get_work_path(self):
        workEntity = context_info.ContextPathInfo()
        workEntity.context.update(entityGrp='mattePaint', project=self.entity['project']['name'], entityType='design', entity=self.entity['code'])
        work_dir_path = workEntity.path.scheme(key='workPath').abs_path()
        return work_dir_path

    def publish(self, entity, taskEntity, userEntity, status, process, register=True):
        # input ContextPathInfo
        projectEntity = entity['project']
        assetName = entity['code']
        allInputImgs = self.displayWidget.get_all_items()

        for img in allInputImgs:
            fn, outputExt = os.path.splitext(img)
            if not outputExt:
                outputExt = 'png'
            else:
                outputExt = outputExt[1:]  # leave out the . in the beginning
            workspaceFile = self.guess_workspace_file([img])

            # calculate version
            version = design_publish_utils.find_next_version(projectEntity, taskEntity, entity)
            # context
            self.asset = self.setup_context(projectEntity['name'], assetName, version, taskEntity['content'], outputExt, process)

            # add info watermark using cv2
            result = watermark.add_border_hud_to_image(input_path=img, 
                                                    output_path='',  # write to temp
                                                    topLeft=[self.asset.project],
                                                    topMid=[],
                                                    topRight=['Date: {}'.format(time.strftime('%d %b %Y - %H:%M:%S'))],
                                                    bottomLeft=['user: {}'.format(userEntity['name']), 
                                                            'department: {}'.format(self.asset.step), 
                                                            'file: {}'.format(os.path.basename(img))],
                                                    bottomMid=[self.asset.name],
                                                    bottomRight=['type: {}'.format(self.asset.type),
                                                            'LOD: {}'.format(self.asset.res), 
                                                            'process: {}'.format(self.asset.process)])
            if not result:
                logger.warning('Failed to add border HUD: {}'.format(img))
            else:
                img = result

            # register process to get publish version
            if register:
                reg = publish_info.PreRegister(self.asset)
                reg.register()
                self.print_out('Registered process')
            else:
                self.asset.context.update(publishVersion=version)

            # publish File
            self.print_out('Publishing files ...')
            publishedImgs, publishedHeroImgs = design_publish_utils.art_publish_files(self.asset, workspaceFile, [img])

            # register files
            regInfo = register_entity.Register(self.asset)
            output = register_entity.Output()
            output.add_process(self.asset.process)
            output.add_design(publishedImgs[0], publishedHeroImgs[0]) if publishedImgs and publishedHeroImgs else None
            output.add_media(os.path.dirname(publishedImgs[0]), os.path.dirname(publishedHeroImgs[0])) if publishedImgs and publishedHeroImgs else None
            regInfo.set(output=output)
            regInfo.register()

            # publishShotgun
            description = ''
            self.print_out('Publishing / Uploading media to Shotgun ...')
            versionResult = sg_hook.publish_version(self.asset, taskEntity, status, publishedImgs, userEntity, description)
            self.print_out('Published version')

            self.print_out('Published files PSD')
            taskResult = sg_hook.update_task(self.asset, taskEntity, status)
            self.print_out('Update task')

            # register shot
            self.print_out('Registering Shot data ...')
            publishAsset = publish_info.RegisterAsset(self.asset)
            regData = publishAsset.register()
            self.print_out('Complete - Version %s' % versionResult['code'])

        # send noti
        design_publish_utils.design_mattepaint_notification(self.asset, taskEntity, userEntity, versionResult, description)
        self.publish_psd(self.asset.publishVersion)
        self.publish_png_layer(self.asset.publishVersion)

        # reset info label
        self.infoLabel.setText('')

        self.print_out('Publish %s versions complete' % len(allInputImgs))

    def guess_workspace_file(self, inputImgs):
        """ looking for psd """
        workspaceFile = {}
        ext = '.psd'
        psd = ['%s%s' % (a.split('.')[0], ext) for a in inputImgs if os.path.exists('%s%s' % (a.split('.')[0], ext))]
        psd = psd if psd else inputImgs
        workspaceFile[ext] = psd
        return workspaceFile

    def setup_context(self, project, assetName, version, taskName, ext, process):
        """ setup contextPathInfo """
        context = context_info.Context()
        context.use_sg(sg=self.sg, project=project, entityType='asset', entityName=assetName)

        step = 'design'
        app = ext
        context.update(step=step, process=process, app=app, version=version, res='md', task=taskName, entityType='design')
        scene = context_info.ContextPathInfo(context=context)
        return scene

    def print_out(self, message):
        logger.info(message)
        print message
        self.status.emit(message)








