import os 
import sys 
import logging
import shutil
from datetime import datetime

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

class IconPath:
    success = '%s/core/rf_app/art/check-icon.png' % os.environ.get('RFSCRIPT')
        
def guess_psd(path): 
    """ guess psd file """
    basename = os.path.splitext(path)[0]
    ext = os.path.splitext(path)[-1]
    psdExt = '.psd'
    psdPath = path.replace(ext, psdExt)

    return psdPath


def guess_process(path): 
	""" guess process """
	# hard coded proces to 2nd element
	elems = os.path.basename(path).split('_')
	return elems[1].split('.')[0] if len(elems) > 1 else ''


def commit_hero_file(entity, publishFile, scheme, outputName=str()): 
    dstDir = entity.path.scheme(scheme).abs_path()
    ext = os.path.splitext(publishFile)[-1]
    outputName = entity.publish_name(hero=True, ext=ext) if not outputName else outputName
    outputDst = ('/').join([dstDir, outputName])

    # check dir 
    if not os.path.exists(dstDir): 
        os.makedirs(dstDir)
        logger.info('dir created %s' % dstDir)
    
    # copy 
    shutil.copy2(publishFile, outputDst)
    logger.info('Copy %s' % (outputDst))

    if os.path.exists(outputDst): 
        return outputDst

def copy(src, dst): 
    if not os.path.exists(os.path.dirname(dst)): 
        os.makedirs(os.path.dirname(dst))
    shutil.copy2(src, dst)
    if os.path.exists(dst): 
        return dst

def create_version(sg, taskEntity, path): 
    data = {'code': versionName}
    sg.create('Version', data)

def set_task(sg, taskEntity, data): 
    return sg.update('Task', taskEntity.get('id'), data)

def submit_daily(entity, step, path): 
    dailyPath = entity.path.daily().abs_path()
    name = '%s' % datetime.now().strftime("%y_%m_%d")
    filename = os.path.basename(path)
    src = path
    dst = '%s/%s/%s/%s' % (dailyPath, step, name, filename)

    if not os.path.exists(os.path.dirname(dst)): 
        os.makedirs(os.path.dirname(dst))
    shutil.copy2(src, dst)

    return dst if os.path.exists(dst) else False 
    

