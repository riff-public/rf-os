# v.0.0.1 beta
# v.0.0.2 minor color change
# v.0.0.4 add animatic and dialog check
# v.0.0.5 upgrade UI

_title = 'Riff Storyboard Publish'
_version = 'v.0.0.5'
_des = 'beta'
uiName = 'RFStoryboardPublish'

#Import python modules
import sys
import os
import getpass
import json
import logging

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)

# import config
import rf_config as config

# framework modules
from rf_utils import log_utils
from rf_utils.ui import load
from rf_utils.ui import stylesheet
from rf_utils import icon

# import widgets
from rf_utils import publish_info
from rf_utils import thread_pool
from rf_utils.widget import user_widget
from rf_utils.widget import project_widget
from rf_utils.widget import entity_browse_widget
from rf_utils.widget import sg_widget
from rf_utils.widget import status_widget
from rf_utils.widget import display_widget
from rf_utils.widget import dialog
from rf_utils.widget.app import create_entity_widget
import storyboard_widget
import keyvis_widget
import colorscript_widget
import animatic_widget

# sg
from rf_utils.sg import sg_utils
from rf_utils import project_info
from rf_utils.context import context_info
from rf_utils import daily
from rf_utils import user_info
from rf_utils.pipeline import user_pref

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

class Color:
    red = 'color: rgb(200, 0, 0);'
    green = 'color: rgb(0, 240, 0);'
    grey = 'color: rgb(120, 120, 120);'
    bgGreen = 'background-color: rgb(0, 140, 0);'
    bgYellow = 'background-color: rgb(200, 120, 0);'
    bgRed = 'background-color: rgb(140, 0, 0);'
    bgGrey = 'background-color: rgb(40, 40, 40);'

class RFStoryboardPublish(QtWidgets.QMainWindow):

    def __init__(self, parent=None):
        #Setup Window
        super(RFStoryboardPublish, self).__init__(parent)

        # ui read
        uiFile = '%s/ui.ui' % moduleDir
        self.ui = load.setup_ui(uiFile, self)
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.setCentralWidget(self.ui)
        self.resize(800, 600)

        # data
        self.threadpool = QtCore.QThreadPool()
        self.threadpool.setMaxThreadCount(1)

        self.projectInfo = project_info.ProjectInfo()
        self.userPref = user_pref.ToolSetting(uiName)
        self.userData = self.userPref.read()

        # ui
        # tabs
        self.storyboardTab = 'Storyboard'
        self.keyvisTab = 'Keyvis'
        self.colorscriptTab = 'Colorscript'
        self.animaticTab = 'Animatic'
        self.stepMap = {self.storyboardTab: 'storyboard', self.keyvisTab: 'keyvis', self.colorscriptTab: 'colorscript', self.animaticTab: 'edit'}
        self.tabIndex = [self.storyboardTab, self.animaticTab, self.keyvisTab, self.colorscriptTab]

        self.setup_widgets()
        self.init_signals()
        self.init_functions()

    def setup_widgets(self):
        self.tabWidget = QtWidgets.QTabWidget()
        # widgets
        self.storyboardTabWidget = QtWidgets.QWidget()
        self.storyboardLayout = QtWidgets.QVBoxLayout(self.storyboardTabWidget)

        # add tabs
        self.tabWidget.addTab(self.storyboardTabWidget, self.storyboardTab)

        # set spacing
        self.storyboardLayout.setContentsMargins(0, 0, 0, 0)
        self.storyboardLayout.setSpacing(8)
        self.ui.tab_layout.addWidget(self.tabWidget)

        # set logo widget
        self.logo = QtWidgets.QLabel()
        self.ui.head_layout.addWidget(self.logo)
        self.logo.setPixmap(QtGui.QPixmap(icon.logo).scaled(64, 64, QtCore.Qt.KeepAspectRatio))

        """ add user widget """
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.userWidget = user_widget.UserComboBox()

        self.ui.head_layout.addItem(spacerItem)
        self.ui.head_layout.addWidget(self.userWidget)

        self.ui.head_layout.setStretch(0, 0)
        self.ui.head_layout.setStretch(1, 3)
        self.ui.head_layout.setStretch(2, 1)

        """ add project widget """
        project = self.userData.get('project')
        self.projectWidget = project_widget.SGProjectComboBox(sg=sg_utils.sg, defaultProject=project)
        self.ui.navigation_layout.addWidget(self.projectWidget)

        """ add sg navigation widget """
        self.sgWidget = entity_browse_widget.SGSceneNavigation(sg=sg_utils.sg, shotFilter=False)
        self.sgWidget.shotExtraFilter = ['sg_shot_type', 'is', 'Preproduction']
        self.sgWidget.shotWidget.displayAttr = 'sg_sequence_code'
        self.sgWidget.shotLabel.setText('Part')
        self.ui.navigation_layout.addWidget(self.sgWidget)

        self.line = QtWidgets.QFrame()
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.ui.navigation_layout.addWidget(self.line)

        """ add create shot button """
        self.createShotButton = QtWidgets.QPushButton('Create Part')
        self.ui.navigation_layout.addWidget(self.createShotButton)

        """ task widget """
        self.taskWidget = sg_widget.TaskComboBox(sg=sg_utils.sg)
        self.ui.navigation_layout.addWidget(self.taskWidget)

        # set layout
        self.taskWidget.allLayout.setStretch(0, 0)
        self.taskWidget.allLayout.setStretch(1, 0)

        """ status widget """
        # self.statusLabel = QtWidgets.QLabel('Status')
        self.statusWidget = status_widget.TaskStatusWidget(layout=QtWidgets.QVBoxLayout())
        self.statusWidget.label.setText('Status')
        self.ui.navigation_layout.addWidget(self.statusWidget)

        """ naviagation layout setup """
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.ui.navigation_layout.addItem(spacerItem)

        self.ui.navigation_layout.setStretch(0, 0) # project
        self.ui.navigation_layout.setStretch(1, 0) # sg
        self.ui.navigation_layout.setStretch(2, 0) # create shot button
        self.ui.navigation_layout.setStretch(3, 0) # line
        self.ui.navigation_layout.setStretch(4, 0) # task
        self.ui.navigation_layout.setStretch(5, 0) # status
        self.ui.navigation_layout.setStretch(6, 4) # spacer

        """ storyborad widget """
        self.storyboardWidget = storyboard_widget.StoryboardWidget(sg=sg_utils.sg)
        self.storyboardLayout.addWidget(self.storyboardWidget)

        """ status label """
        self.statusLabel = QtWidgets.QLabel()
        self.ui.tab_layout.addWidget(self.statusLabel)
        self.statusLabel.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)

        self.ui.tab_layout.setStretch(0, 1)
        self.ui.tab_layout.setStretch(1, 0)

        """ override stylesheet """
        self.projectWidget.label.setStyleSheet(Color.grey)
        self.sgWidget.episodeLabel.setStyleSheet(Color.grey)
        self.sgWidget.shotLabel.setStyleSheet(Color.grey)
        self.taskWidget.label.setStyleSheet(Color.grey)
        self.statusWidget.label.setStyleSheet(Color.grey)

    # end layout

    def init_signals(self):
        self.projectWidget.projectChanged.connect(self.project_signal)
        self.sgWidget.completed.connect(self.navigation_trigger)
        self.sgWidget.episodeTriggered.connect(self.episode_trigger)
        self.tabWidget.currentChanged.connect(self.tab_changed)

        # stroyboard publish
        self.storyboardWidget.publishButton.clicked.connect(self.publish)
        self.storyboardWidget.status.connect(self.print_status)

        # create sequence (shot)
        self.createShotButton.clicked.connect(self.create_shot)

        # input control
        self.taskWidget.comboBox.currentIndexChanged.connect(self.task_changed)
        self.sgWidget.shotWidget.currentIndexChanged.connect(self.shot_changed)

    def init_functions(self):
        self.sgWidget.list_episodes(self.projectWidget.current_item())

         # restore last selection
        episode = self.userData.get('episode')
        self.sgWidget.episodeWidget.set_current_item(episode)

    def restore_last_selection(self):
        data = self.userPref.read()
        project = data['project']
        episode = data['episode']
        self.projectWidget.set_current_item(project)

    def project_signal(self, project):
        """ what happen when project changed """
        self.sgWidget.list_episodes(project)

        # save ui selection
        self.save_last_selection()

    def navigation_trigger(self, shotDict):
        if shotDict:
            project = shotDict['project']['name']
            entityType = 'Shot'
            entityName = shotDict['code']
            step = self.get_step_mode()
            self.taskWidget.activate(project, entityType, entityName, step)
        else:
            logger.warning('No shot on the list')

    def episode_trigger(self, epDict):
         # save ui selection
        self.save_last_selection()

    def refresh_shot(self, shotName):
        """ refresh and set current shotName """
        epDict = self.sgWidget.episodeWidget.current_item()
        self.sgWidget.episode_trigger(epDict)
        self.sgWidget.shotWidget.set_current_item(shotName)


    def tab_changed(self, index):
        shotDict = self.sgWidget.shotWidget.current_item()
        self.navigation_trigger(shotDict)

    def publish(self):
        worker = thread_pool.Worker(self.storyboard_publish)
        worker.kwargs['loopResult'] = worker.signals.loopResult
        worker.signals.loopResult.connect(self.print_status)
        worker.signals.result.connect(self.complete)
        self.threadpool.start(worker)

    def storyboard_publish(self, **kwargs): 
        """ publish storyboard call """
        callback = kwargs['loopResult']
        shotEntity, taskEntity, status, userEntity = self.collect_ui_data()
        self.storyboardWidget.publishButton.setEnabled(False)
        
        if self.check_input_ui(shotEntity, taskEntity, userEntity):
            callback.emit('Publishing ...')
            return self.storyboardWidget.publish(shotEntity, taskEntity, userEntity, status, callback)

        
    def complete(self, result): 
        message = 'Storyboard publish complete'
        self.complete_dialog(message)
        self.storyboardWidget.publishButton.setEnabled(True)
        self.storyboardWidget.displayWidget.clear()
        self.storyboardWidget.description_textEdit.clear()
        self.storyboardWidget.description_textEdit.setStyleSheet('')
        dirname = os.path.dirname(result[0])
        open_explorer(dirname)

    def task_changed(self):
        self.taskWidget.comboBox.setStyleSheet('')

    def shot_changed(self):
        self.sgWidget.shotWidget.setStyleSheet('')


    def collect_ui_data(self):
        """ collect shot, task, status, user entity """
        shotEntity = self.sgWidget.shotWidget.current_item()
        taskEntity = self.taskWidget.current_item()
        status = self.statusWidget.current_item()
        userEntity = self.userWidget.data()

        return shotEntity, taskEntity, status, userEntity

    def check_input_ui(self, shotEntity, taskEntity, userEntity):
        status = True
        messages = []
        if not shotEntity:
            self.sgWidget.shotWidget.setStyleSheet(Color.bgRed)
            messages.append('No shot selected')
            status = False
        if not taskEntity:
            self.taskWidget.comboBox.setStyleSheet(Color.bgRed)
            messages.append('No task selected')
            status = False
        if not userEntity:
            messages.append('No task selected')
            status = False

        if not status:
            dialog.MessageBox.error('Error', ('\n').join(messages))

        return status

    def create_shot(self):
        """ create shot dialog """
        project = self.projectWidget.current_item()
        episode = self.sgWidget.episodeWidget.current_item()
        context = context_info.Context()
        context.update(project=project['name'], projectCode=project['sg_project_code'], entityType='scene', entityGrp=episode['code'])
        scene = context_info.ContextPathInfo(context=context)
        shotName = create_entity_widget.CreateShot.create_part(context=scene)

        # refresh by list episode
        self.refresh_shot(shotName)


    def print_status(self, message):
        self.statusLabel.setText(message)
        # QtWidgets.QApplication.processEvents()


    def complete_dialog(self, message):
        title = 'Complete'
        result = dialog.MessageBox.success(title, message)
        return result


    def get_step_mode(self):
        index = self.tabWidget.currentIndex()
        currentTab = self.tabWidget.tabText(index)
        return self.stepMap[currentTab]


    def save_last_selection(self):
        """ save selection to json file """
        data = dict()
        project = str(self.projectWidget.projectComboBox.currentText())
        episode = str(self.sgWidget.episodeWidget.comboBox.currentText())
        data['project'] = project
        data['episode'] = episode

        result = self.userPref.write(data)


def open_explorer(path): 
    import subprocess
    path = path.replace('/', '\\')
    subprocess.Popen(r'explorer /select,"%s"' % path)


def show():
    logger.info('RFStoryboardPublish Starting ...')
    logger.info('---------------------------')
    logger.info('---------------------------')
    app = QtWidgets.QApplication.instance()
    if not app:
        app = QtWidgets.QApplication(sys.argv)
    myApp = RFStoryboardPublish()
    myApp.show()
    stylesheet.set_default(app)
    sys.exit(app.exec_())

if __name__ == '__main__':
    show()
