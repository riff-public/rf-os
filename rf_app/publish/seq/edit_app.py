# v.0.0.1 beta
# v.0.0.2 minor color change
# v.0.0.4 add animatic and dialog check

_title = 'Riff Edit Publish'
_version = 'v.0.0.2'
_des = 'beta'
uiName = 'RFSeqDesignPublish'

#Import python modules
import sys
import os
import getpass
import json
import logging

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)

# import config
import rf_config as config

# framework modules
from rf_utils import log_utils
from rf_utils.ui import load
from rf_utils.ui import stylesheet
from rf_utils import icon

# import widgets
from rf_utils import publish_info
from rf_utils.widget import user_widget
from rf_utils.widget import project_widget
from rf_utils.widget import entity_browse_widget
from rf_utils.widget import sg_widget
from rf_utils.widget import status_widget
from rf_utils.widget import display_widget
from rf_utils.widget import dialog
from rf_utils.widget.app import create_entity_widget
from rf_app.publish.utils import design_publish_utils

import edit_layout_widget
import edit_anim_widget
import colorscript_widget
import edit_animatic_widget as animatic_widget

# sg
from rf_utils.sg import sg_utils
from rf_utils import project_info
from rf_utils.context import context_info
from rf_utils import daily
from rf_utils import user_info
from rf_utils.pipeline import user_pref


from rf_app.edit.xml_view.app import ShotXmlViewer

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

class Color:
    red = 'color: rgb(200, 0, 0);'
    green = 'color: rgb(0, 240, 0);'
    grey = 'color: rgb(120, 120, 120);'
    bgGreen = 'background-color: rgb(0, 140, 0);'
    bgYellow = 'background-color: rgb(200, 120, 0);'
    bgRed = 'background-color: rgb(140, 0, 0);'
    bgGrey = 'background-color: rgb(40, 40, 40);'

class RFDesignPublish(QtWidgets.QMainWindow):

    def __init__(self, parent=None):
        #Setup Window
        super(RFDesignPublish, self).__init__(parent)

        # ui read
        uiFile = '%s/ui.ui' % moduleDir
        self.ui = load.setup_ui(uiFile, self)

        self.ui.show()
        self.ui.setWindowTitle('%s %s-%s' % (_title, _version, _des))

        # data
        self.projectInfo = project_info.ProjectInfo()
        self.userPref = user_pref.ToolSetting(uiName)
        self.userData = self.userPref.read()

        # ui
        # tabs
        self.editLayoutTab = 'Edit Layout'
        self.editAnimTab = 'Edit Anim'
        self.animaticTab = 'Animatic'
        self.stepMap = {self.editLayoutTab: 'layout', self.editAnimTab: 'anim', self.animaticTab: 'edit'}
        self.tabIndex = [self.animaticTab, self.editLayoutTab, self.editAnimTab]

        self.setup_widgets()
        self.init_signals()
        self.init_functions()


    def setup_widgets(self):
        self.add_tab_layout()

        self.add_logo_widget()
        self.add_user_widget()
        self.set_header_layout()

        self.add_project_widget()
        self.add_sg_navigation()
        self.add_create_shot()
        self.add_task_widget()
        self.add_status_widget()
        self.add_version_widget()
        self.set_navigation_layout()

        self.add_edit_layout_widget()
        self.add_edit_anim_widget()
        self.add_animatic_widget()

        self.add_status_label()
        self.set_style()
        self.disable_widget()

    def add_tab_layout(self):
        self.tabWidget = QtWidgets.QTabWidget()

        # widgets
        self.editLayoutTabWidget = QtWidgets.QWidget()
        self.editLayoutLayout = QtWidgets.QVBoxLayout(self.editLayoutTabWidget)

        self.editAnimTabWidget = QtWidgets.QWidget()
        self.editAnimLayout = QtWidgets.QVBoxLayout(self.editAnimTabWidget)

        self.animaticTabWidget = QtWidgets.QWidget()
        self.animaticLayout = QtWidgets.QVBoxLayout(self.animaticTabWidget)

        # add tabs
        self.tabWidget.addTab(self.animaticTabWidget, self.animaticTab)
        self.tabWidget.addTab(self.editLayoutTabWidget, self.editLayoutTab)
        self.tabWidget.addTab(self.editAnimTabWidget, self.editAnimTab)

        # set spacing
        self.editLayoutLayout.setContentsMargins(0, 0, 0, 0)
        self.editAnimLayout.setContentsMargins(0, 0, 0, 0)
        self.animaticLayout.setContentsMargins(0, 0, 0, 0)

        self.editLayoutLayout.setSpacing(8)
        self.editAnimLayout.setSpacing(8)
        self.animaticLayout.setSpacing(8)

        self.ui.tab_layout.addWidget(self.tabWidget)


    def add_logo_widget(self):
        # set logo widget
        self.logo = QtWidgets.QLabel()
        self.ui.head_layout.addWidget(self.logo)
        self.logo.setPixmap(QtGui.QPixmap(icon.logo).scaled(64, 64, QtCore.Qt.KeepAspectRatio))

    def add_user_widget(self):
        """ add user widget """
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.userWidget = user_widget.UserComboBox()

        self.ui.head_layout.addItem(spacerItem)
        self.ui.head_layout.addWidget(self.userWidget)

    def set_header_layout(self):
        self.ui.head_layout.setStretch(0, 0)
        self.ui.head_layout.setStretch(1, 3)
        self.ui.head_layout.setStretch(2, 1)

    def add_project_widget(self):
        """ add project widget """
        project = self.userData.get('project')
        self.projectWidget = project_widget.SGProjectComboBox(sg=sg_utils.sg, defaultProject=project)
        self.ui.navigation_layout.addWidget(self.projectWidget)

    def add_sg_navigation(self):
        """ add sg navigation widget """
        self.sgWidget = entity_browse_widget.SGSceneNavigation(sg=sg_utils.sg, shotFilter=False)
        self.sgWidget.shotExtraFilter = ['sg_shot_type', 'is', 'Sequence']
        self.sgWidget.shotLabel.setText('Sequence')

        self.line = self.hline_widget()
        self.ui.navigation_layout.addWidget(self.sgWidget)
        self.ui.navigation_layout.addWidget(self.line)

    def add_create_shot(self):
        """ add create shot button """
        self.createShotButton = QtWidgets.QPushButton('Create Sequence')
        self.ui.navigation_layout.addWidget(self.createShotButton)


    def add_task_widget(self):
        self.taskWidget = sg_widget.TaskComboBox(sg=sg_utils.sg)
        self.ui.navigation_layout.addWidget(self.taskWidget)

        # set layout
        self.taskWidget.allLayout.setStretch(0, 0)
        self.taskWidget.allLayout.setStretch(1, 0)

    def add_status_widget(self):
        # self.statusLabel = QtWidgets.QLabel('Status')
        self.statusWidget = status_widget.TaskStatusWidget(layout=QtWidgets.QVBoxLayout())
        self.statusWidget.label.setText('Status')
        self.ui.navigation_layout.addWidget(self.statusWidget)

    def add_version_widget(self):
        self.versionLabel = QtWidgets.QLabel('Version')
        self.versionWidget = QtWidgets.QLineEdit()
        self.ui.navigation_layout.addWidget(self.versionLabel)
        self.ui.navigation_layout.addWidget(self.versionWidget)


    def set_navigation_layout(self):
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.ui.navigation_layout.addItem(spacerItem)

        self.ui.navigation_layout.setStretch(0, 0) # project
        self.ui.navigation_layout.setStretch(1, 0) # sg
        self.ui.navigation_layout.setStretch(2, 0) # create shot button
        self.ui.navigation_layout.setStretch(3, 0) # line
        self.ui.navigation_layout.setStretch(4, 0) # task
        self.ui.navigation_layout.setStretch(5, 0) # status
        self.ui.navigation_layout.setStretch(6, 0) # version label
        self.ui.navigation_layout.setStretch(7, 0) # version
        self.ui.navigation_layout.setStretch(8, 4) # spacer

    def add_edit_layout_widget(self):
        self.editLayoutWidget = edit_layout_widget.LayoutEditWidget(sg=sg_utils.sg)
        self.editLayoutLayout.addWidget(self.editLayoutWidget)

    def add_edit_anim_widget(self):
        self.editAnimWidget = edit_anim_widget.AnimEditWidget(sg=sg_utils.sg)
        self.editAnimLayout.addWidget(self.editAnimWidget)

    def add_animatic_widget(self):
        self.animaticWidget = animatic_widget.AnimaticWidget(sg=sg_utils.sg)
        self.animaticLayout.addWidget(self.animaticWidget)


    def add_status_label(self):
        self.statusLabel = QtWidgets.QLabel()
        self.ui.tab_layout.addWidget(self.statusLabel)
        self.statusLabel.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)

        self.ui.tab_layout.setStretch(0, 1)
        self.ui.tab_layout.setStretch(1, 0)

    def set_style(self):
        """ override stylesheet """
        self.projectWidget.label.setStyleSheet(Color.grey)
        self.sgWidget.episodeLabel.setStyleSheet(Color.grey)
        self.sgWidget.shotLabel.setStyleSheet(Color.grey)
        self.taskWidget.label.setStyleSheet(Color.grey)
        self.statusWidget.label.setStyleSheet(Color.grey)
        self.versionLabel.setStyleSheet(Color.grey)

    def disable_widget(self):
        """ override and disable some functions """
        pass


    def hline_widget(self):
        line = QtWidgets.QFrame()
        line.setFrameShape(QtWidgets.QFrame.HLine)
        line.setFrameShadow(QtWidgets.QFrame.Sunken)
        return line

    # end layout

    def init_signals(self):
        self.projectWidget.projectChanged.connect(self.project_signal)
        self.sgWidget.completed.connect(self.navigation_trigger)
        self.sgWidget.episodeTriggered.connect(self.episode_trigger)
        self.tabWidget.currentChanged.connect(self.tab_changed)

        # edit layout publish
        self.editLayoutWidget.publishButton.clicked.connect(self.edit_layout_publish)
        self.editLayoutWidget.status.connect(self.print_status)
        self.editLayoutWidget.viewXmlButton.clicked.connect(self.run_layout_xml)

        # create sequence (shot)
        self.createShotButton.clicked.connect(self.create_shot)

        # edit anim publish
        self.editAnimWidget.publishButton.clicked.connect(self.edit_anim_publish)
        self.editAnimWidget.status.connect(self.print_status)
        self.editAnimWidget.viewXmlButton.clicked.connect(self.run_anim_xml)

        # animatic publish
        self.animaticWidget.publishButton.clicked.connect(self.animatic_publish)
        self.animaticWidget.status.connect(self.print_status)
        self.animaticWidget.viewXmlButton.clicked.connect(self.run_animatic_xml)

        # input control
        self.taskWidget.comboBox.currentIndexChanged.connect(self.task_changed)
        self.sgWidget.shotWidget.currentIndexChanged.connect(self.shot_changed)

    def init_functions(self):
        self.sgWidget.list_episodes(self.projectWidget.current_item())

         # restore last selection
        episode = self.userData.get('episode')
        self.sgWidget.episodeWidget.set_current_item(episode)

    def restore_last_selection(self):
        data = self.userPref.read()
        project = data['project']
        episode = data['episode']
        self.projectWidget.set_current_item(project)


    def project_signal(self, project):
        """ what happen when project changed """
        self.sgWidget.list_episodes(project)

        # save ui selection
        self.save_last_selection()

    def navigation_trigger(self, shotDict):
        if shotDict:
            project = shotDict['project']['name']
            entityType = 'Shot'
            entityName = shotDict['code']
            step = self.get_step_mode()
            self.taskWidget.activate(project, entityType, entityName, step)
        else:
            logger.warning('No shot on the list')

    def episode_trigger(self, epDict):
         # save ui selection
        self.save_last_selection()

    def refresh_shot(self, shotName):
        """ refresh and set current shotName """
        epDict = self.sgWidget.episodeWidget.current_item()
        self.sgWidget.episode_trigger(epDict)
        self.sgWidget.shotWidget.set_current_item(shotName)


    def tab_changed(self, index):
        shotDict = self.sgWidget.shotWidget.current_item()
        self.navigation_trigger(shotDict)

    def edit_layout_publish(self):
        """ publish storyboard call """
        shotEntity, taskEntity, status, userEntity = self.collect_ui_data()
        if self.check_input_ui(shotEntity, taskEntity, userEntity):
            self.editLayoutWidget.publish(shotEntity, taskEntity, userEntity, status)

            message = 'Edit Layout publish complete'
            self.complete_dialog(message)

    def edit_anim_publish(self):
        """ publish keyvis call """
        shotEntity, taskEntity, status, userEntity = self.collect_ui_data()
        if self.check_input_ui(shotEntity, taskEntity, userEntity):
            self.editAnimWidget.publish(shotEntity, taskEntity, userEntity, status)

            message = 'Keyvis publish complete'
            self.complete_dialog(message)

    def animatic_publish(self):
        """ publish animatic call """
        shotEntity, taskEntity, status, userEntity = self.collect_ui_data()
        version = str(self.versionWidget.text())
        if self.check_input_ui(shotEntity, taskEntity, userEntity):
            self.animaticWidget.publish(shotEntity, taskEntity, userEntity, status, version)

            message = 'Animatic publish complete'
            self.complete_dialog(message)

    def task_changed(self):
        self.taskWidget.comboBox.setStyleSheet('')
        # self.calculate_version()

    def shot_changed(self):
        self.sgWidget.shotWidget.setStyleSheet('')


    def collect_ui_data(self):
        """ collect shot, task, status, user entity """
        shotEntity = self.sgWidget.shotWidget.current_item()
        taskEntity = self.taskWidget.current_item()
        status = self.statusWidget.current_item()
        userEntity = self.userWidget.data()

        return shotEntity, taskEntity, status, userEntity

    def check_input_ui(self, shotEntity, taskEntity, userEntity):
        status = True
        messages = []
        if not shotEntity:
            self.sgWidget.shotWidget.setStyleSheet(Color.bgRed)
            messages.append('No shot selected')
            status = False
        if not taskEntity:
            self.taskWidget.comboBox.setStyleSheet(Color.bgRed)
            messages.append('No task selected')
            status = False
        if not userEntity:
            messages.append('No task selected')
            status = False

        if not status:
            dialog.MessageBox.error('Error', ('\n').join(messages))

        return status

    def create_shot(self):
        """ create shot dialog """
        project = self.projectWidget.current_item()
        episode = self.sgWidget.episodeWidget.current_item()
        context = context_info.Context()
        context.update(project=project['name'], projectCode=project['sg_project_code'], entityType='scene', entityGrp=episode['code'])
        scene = context_info.ContextPathInfo(context=context)
        shotName = create_entity_widget.CreateShotSequence.show(context=scene)

        # refresh by list episode
        self.refresh_shot(shotName)

    def calculate_version(self):
        # calculate version
        print 'calculate version'
        return
        shotEntity, taskEntity, status, userEntity = self.collect_ui_data()
        version = design_publish_utils.find_next_version(shotEntity['project'], taskEntity, shotEntity)
        self.versionWidget.setText(version)

    def print_status(self, message):
        self.statusLabel.setText(message)
        QtWidgets.QApplication.processEvents()


    def complete_dialog(self, message):
        title = 'Complete'
        result = dialog.MessageBox.success(title, message)
        return result

    def run_animatic_xml(self):
        project = self.projectWidget.current_item()
        episode = self.sgWidget.episodeWidget.current_item()
        shot_entity = self.sgWidget.shotWidget.current_item()
        self.xml_viewer = ShotXmlViewer(self, project=project, episode=episode, xml_type="edit", seq=shot_entity)

    def run_layout_xml(self):
        project = self.projectWidget.current_item()
        episode = self.sgWidget.episodeWidget.current_item()
        shot_entity = self.sgWidget.shotWidget.current_item()
        self.xml_viewer = ShotXmlViewer(self, project=project, episode=episode, xml_type="layout", seq=shot_entity)

    def run_anim_xml(self):
        project = self.projectWidget.current_item()
        episode = self.sgWidget.episodeWidget.current_item()
        shot_entity = self.sgWidget.shotWidget.current_item()
        self.xml_viewer = ShotXmlViewer(self, project=project, episode=episode, xml_type="anim", seq=shot_entity)

    def get_step_mode(self):
        index = self.tabWidget.currentIndex()
        currentTab = self.tabWidget.tabText(index)
        return self.stepMap[currentTab]


    def save_last_selection(self):
        """ save selection to json file """
        data = dict()
        project = str(self.projectWidget.projectComboBox.currentText())
        episode = str(self.sgWidget.episodeWidget.comboBox.currentText())
        data['project'] = project
        data['episode'] = episode

        result = self.userPref.write(data)


def show():
    logger.info('RFDesignPublish Starting ...')
    logger.info('---------------------------')
    logger.info('---------------------------')
    app = QtWidgets.QApplication.instance()
    if not app:
        app = QtWidgets.QApplication(sys.argv)
    myApp = RFDesignPublish()
    stylesheet.set_default(app)
    sys.exit(app.exec_())

if __name__ == '__main__':
    show()
