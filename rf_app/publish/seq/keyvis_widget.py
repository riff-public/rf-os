import os
import sys
import traceback
import time
from collections import OrderedDict

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_utils.widget.app import display_reel_widget
from rf_utils.context import context_info
from rf_utils import publish_info
from rf_utils import daily
from rf_utils.pipeline import watermark
from rf_app.publish.utils import design_publish_utils
from rf_app.publish.asset import sg_hook

from rf_utils.pipeline.notification import noti_module_cliq
reload(noti_module_cliq)

from rf_utils.sg import sg_process

class Color: 
    red = 'color: rgb(200, 0, 0);'
    green = 'color: rgb(0, 240, 0);'
    bgGreen = 'background-color: rgb(0, 140, 0);'
    bgYellow = 'background-color: rgb(200, 120, 0);'
    bgRed = 'background-color: rgb(140, 0, 0);'
    bgGrey = 'background-color: rgb(40, 40, 40);'

class KeyvisWidget(QtWidgets.QWidget) :
    status = QtCore.Signal(str)
    def __init__(self, sg=None) :
        super(KeyvisWidget, self).__init__()
        self.contactSheet = 'contactSheet'
        self.panel = 'panel'
        self.sg = sg 
        
        self.allLayout = QtWidgets.QVBoxLayout()
        self.set_ui()
        self.init_signals()

    
    def set_ui(self) :
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        
        # layout
        self.headLayout = QtWidgets.QHBoxLayout()
        self.headLayout.addItem(spacerItem)

        self.headLayout.setStretch(0, 1)
        self.headLayout.setStretch(1, 1)
        self.headLayout.setStretch(2, 2)

        # snap 
        self.displayWidget = display_reel_widget.DisplayImages()
        self.displayWidget.displayWidget.set_style(Color.bgGrey)
        self.allLayout.addLayout(self.headLayout)
        self.allLayout.addWidget(self.displayWidget)
        self.displayWidget.multiple_file(True)

        # button 
        self.publishButton = QtWidgets.QPushButton('Publish')
        self.publishButton.setMinimumSize(QtCore.QSize(0, 30))

        # info label 
        self.infoLabel = QtWidgets.QLabel()

        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.bottomLayout = QtWidgets.QHBoxLayout()
        self.bottomLayout.addWidget(self.infoLabel)
        self.bottomLayout.addItem(spacerItem)
        self.bottomLayout.addWidget(self.publishButton)
        self.bottomLayout.setStretch(0, 1)
        self.bottomLayout.setStretch(1, 1)
        self.bottomLayout.setStretch(2, 1)
        self.allLayout.addLayout(self.bottomLayout)

        self.setLayout(self.allLayout)

    def init_signals(self): 
        # storyboard 
        self.displayWidget.dropped.connect(self.drop_signal)


    def drop_signal(self, paths): 
        """ event when drop imgs to stroyboard panel """
        self.infoLabel.setText('%s files' % len(paths))


    def publish(self, shotEntity, taskEntity, userEntity, status): 
        # input ContextPathInfo 
        projectEntity = shotEntity['project']
        shotName = shotEntity['code']
        inputImgs = self.displayWidget.get_all_items()
        outputExt = inputImgs[0].split('.')[-1] if inputImgs else 'png'
        
        # calculate version 
        version = design_publish_utils.find_next_version(projectEntity, taskEntity, shotEntity)
        # context 
        self.scene = self.setup_context(projectEntity['name'], shotName, version, taskEntity['content'], outputExt)

        process_images = list(inputImgs)
        for i, img in enumerate(inputImgs):
            # add info watermark using cv2
            result = watermark.add_border_hud_to_image(input_path=img, 
                                                    output_path='',  # write to temp
                                                    topLeft=[self.scene.project],
                                                    topMid=[],
                                                    topRight=['Date: {}'.format(time.strftime('%d %b %Y - %H:%M:%S'))],
                                                    bottomLeft=['user: {}'.format(userEntity['name']), 
                                                            'department: {}'.format(self.scene.step), 
                                                            'file: {}'.format(os.path.basename(img))],
                                                    bottomMid=[self.scene.name],
                                                    bottomRight=['type: {}'.format(self.scene.type),
                                                            'LOD: {}'.format(self.scene.res), 
                                                            'process: {}'.format(self.scene.process)])
            if not result:
                logger.warning('Failed to add border HUD: {}'.format(img))
                continue
            process_images[i] = result
            
        # register process to get publish version
        reg = publish_info.PreRegister(self.scene)
        reg.register()
        self.print_out('Registered process')

        # publish File 
        self.print_out('Publishing files ...')
        publishedImgs, publishedHeroImgs = design_publish_utils.publish_files(self.scene, process_images)
        self.print_out('Published files')
        
        # publishShotgun
        description = ''
        self.print_out('Publishing / Uploading media to Shotgun ...')
        versionResult = sg_hook.publish_version(self.scene, taskEntity, status, publishedImgs, userEntity, description)
        self.print_out('Published version')
        
        taskResult = sg_hook.update_task(self.scene, taskEntity, status)
        self.print_out('Update task')

        # register shot 
        self.print_out('Registering Shot data ...')
        publishAsset = publish_info.RegisterAsset(self.scene)
        regData = publishAsset.register()
        self.print_out('Complete')

        taskFromId = sg_process.get_task_from_id(taskEntity['id'])
        result = [taskFromId]

        taskEntity['sg_status_list'] = status

        noti_module_cliq.send_scene_notification(result, taskEntity, self.scene, userEntity, versionResult)


    def setup_context(self, project, shotName, version, taskName, ext): 
        """ setup contextPathInfo """ 
        context = context_info.Context()
        context.use_sg(sg=self.sg, project=project, entityType='scene', entityName=shotName)

        step = 'keyvis'
        process = 'main'
        app = ext
        context.update(step=step, process=process, app=app, version=version, res='scene', task=taskName)
        scene = context_info.ContextPathInfo(context=context)
        return scene 


    def print_out(self, message): 
        logger.info(message)
        print message
        self.status.emit(message)








