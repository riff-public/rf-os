import os
import sys
import traceback
import time
from collections import OrderedDict

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_utils.widget.app import display_reel_widget
from rf_utils.widget import textEdit
from rf_utils.context import context_info
from rf_utils import publish_info
from rf_utils import daily
from rf_utils.pipeline import watermark
from rf_app.publish.utils import design_publish_utils
from rf_app.publish.asset import sg_hook

from rf_utils.pipeline.notification import noti_module_cliq
reload(noti_module_cliq)

from rf_utils.sg import sg_process


Message = None

class Color:
    red = 'color: rgb(200, 0, 0);'
    green = 'color: rgb(0, 240, 0);'
    bgGreen = 'background-color: rgb(0, 140, 0);'
    bgYellow = 'background-color: rgb(200, 120, 0);'
    bgRed = 'background-color: rgb(140, 0, 0);'
    bgGrey = 'background-color: rgb(40, 40, 40);'

class StoryboardWidget(QtWidgets.QWidget):
    status = QtCore.Signal(str)
    def __init__(self, sg=None) :
        super(StoryboardWidget, self).__init__()
        self.contactSheet = 'thumbnail'
        self.panel = 'panel'
        self.sg = sg

        self.allLayout = QtWidgets.QVBoxLayout()
        self.set_ui()
        self.init_signals()


    def set_ui(self) :
        self.allLayout.setSpacing(8)
        self.allLayout.setContentsMargins(10, 10, 10, 0)

        # snap
        self.display_layout = QtWidgets.QVBoxLayout()
        self.allLayout.addLayout(self.display_layout)

        self.displayWidget = display_reel_widget.DisplayImages()
        self.displayWidget.multiple_file(True)
        self.displayWidget.allLayout.setContentsMargins(0, 0, 0, 0)
        self.displayWidget.displayWidget.set_style(Color.bgGrey)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.displayWidget.setSizePolicy(sizePolicy)
        self.display_layout.addWidget(self.displayWidget)

        # description 
        self.desc_groupBox = QtWidgets.QGroupBox('Description')
        self.allLayout.addWidget(self.desc_groupBox)

        self.description_layout = QtWidgets.QVBoxLayout(self.desc_groupBox)
        self.description_textEdit = textEdit.TextEditWithPlaceHolder()
        self.description_textEdit.setMaximumHeight(45)
        self.description_textEdit.setPlaceholderText('Some information about your publish...')
        self.description_textEdit.setWordWrapMode(QtGui.QTextOption.WrapAtWordBoundaryOrAnywhere)
        self.description_textEdit.setTextInteractionFlags(QtCore.Qt.TextEditorInteraction)
        self.description_layout.addWidget(self.description_textEdit)

        # button
        self.publishButton = QtWidgets.QPushButton('Publish')
        self.publishButton.setMinimumSize(QtCore.QSize(160, 35))

        # info label
        self.infoLabel = QtWidgets.QLabel()

        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.bottomLayout = QtWidgets.QHBoxLayout()
        self.bottomLayout.addWidget(self.infoLabel)
        self.bottomLayout.addItem(spacerItem)
        self.bottomLayout.addWidget(self.publishButton)
        self.bottomLayout.setStretch(0, 1)
        self.bottomLayout.setStretch(1, 1)
        self.bottomLayout.setStretch(2, 1)

        self.allLayout.setStretch(0, 1)
        self.allLayout.setStretch(1, 0)
        self.allLayout.setStretch(2, 0)
        self.allLayout.addLayout(self.bottomLayout)

        self.setLayout(self.allLayout)

    def init_signals(self):
        # storyboard
        self.displayWidget.dropped.connect(self.input_changed)
        self.displayWidget.deleted.connect(self.input_changed)
        # self.contactSheetRB.clicked.connect(self.mode_changed)
        # self.panelRB.clicked.connect(self.mode_changed)

    def input_changed(self, *args, **kwargs):
        """ event when drop imgs to stroyboard panel """
        paths = self.displayWidget.get_all_items()
        self.infoLabel.setText('Dropped %s files' % len(paths))

    def publish(self, shotEntity, taskEntity, userEntity, status, callback=None):
        # input ContextPathInfo
        projectEntity = shotEntity['project']
        shotName = shotEntity['code']
        inputImgs = self.displayWidget.get_all_items()
        outputExt = inputImgs[0].split('.')[-1] if inputImgs else 'png'

        # calculate version
        version = design_publish_utils.find_next_version(projectEntity, taskEntity, shotEntity)
        # context
        self.scene = self.setup_context(projectEntity['name'], shotName, version, taskEntity['content'], outputExt)

        process_images = list(inputImgs)
        for i, img in enumerate(inputImgs):
            # add info watermark using cv2
            result = watermark.add_border_hud_to_image(input_path=img, 
                                                    output_path='',  # write to temp
                                                    topLeft=[self.scene.project],
                                                    topMid=[],
                                                    topRight=['Date: {}'.format(time.strftime('%d %b %Y - %H:%M:%S'))],
                                                    bottomLeft=['user: {}'.format(userEntity['name']), 
                                                            'department: {}'.format(self.scene.step), 
                                                            'file: {}'.format(os.path.basename(img))],
                                                    bottomMid=[self.scene.name],
                                                    bottomRight=['type: {}'.format(self.scene.type),
                                                            'LOD: {}'.format(self.scene.res), 
                                                            'process: {}'.format(self.scene.process)])
            if not result:
                logger.warning('Failed to add border HUD: {}'.format(img))
                continue
            process_images[i] = result
            callback.emit('{}/{} processing watermark'.format(i, len(inputImgs)))

        # register process to get publish version
        reg = publish_info.PreRegister(self.scene)
        reg.register()
        callback.emit('Registered process')

        # publish File
        callback.emit('Publishing files ...')
        subfolder = '{}_{}_{}'.format(shotName, taskEntity['content'], userEntity['name'])
        publishedImgs, publishedHeroImgs = design_publish_utils.publish_files(self.scene, process_images, subfolder=subfolder)
        callback.emit('Published files')

        # publishShotgun
        description = self.description_textEdit.toPlainText()  # get description
        callback.emit('Uploading media to Shotgun (several minutes) ...')
        versionResult = sg_hook.publish_version(self.scene, taskEntity, status, publishedImgs, userEntity, description)
        callback.emit('Published version')

        taskResult = sg_hook.update_task(self.scene, taskEntity, status)
        callback.emit('Update task')

        # register shot
        callback.emit('Registering Shot data ...')
        publishAsset = publish_info.RegisterAsset(self.scene)
        regData = publishAsset.register()

        taskFromId = sg_process.get_task_from_id(taskEntity['id'])
        result = [taskFromId]

        taskEntity['sg_status_list'] = status

        callback.emit('Sending Notification...')
        noti_module_cliq.send_scene_notification(result, taskEntity, self.scene, userEntity, versionResult)
        
        # reset info label
        self.infoLabel.setText('')
        callback.emit('Publish Complete: {}'.format(versionResult.get('code')))
        return publishedHeroImgs


    def setup_context(self, project, shotName, version, taskName, ext):
        """ setup contextPathInfo """
        context = context_info.Context()
        context.use_sg(sg=self.sg, project=project, entityType='scene', entityName=shotName)

        step = 'storyboard'
        # process = self.get_mode()
        process = taskName
        app = ext
        context.update(step=step, process=process, app=app, version=version, res='scene', task=taskName)
        scene = context_info.ContextPathInfo(context=context)
        return scene


    def print_out(self, message):
        logger.info(message)
        print message
        self.status.emit(message)
