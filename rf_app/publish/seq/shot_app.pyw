# v.0.0.1 beta
# v.0.0.2 minor color change
# v.0.0.4 add animatic and dialog check

_title = 'Riff Shot Publish'
_version = 'v.0.0.1'
_des = 'beta'
uiName = 'RFShotPublish'

#Import python modules
import sys
import os
import getpass
import json
import logging

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)

# import config
import rf_config as config

# framework modules
from rf_utils import log_utils
from rf_utils.ui import load
from rf_utils.ui import stylesheet
from rf_utils import icon

# import widgets
from rf_utils import publish_info
from rf_utils.widget import user_widget
from rf_utils.widget import project_widget
from rf_utils.widget import entity_browse_widget
from rf_utils.widget import sg_widget
from rf_utils.widget import status_widget
from rf_utils.widget import display_widget
from rf_utils.widget import dialog
from rf_utils.widget.app import create_entity_widget
import animation_widget
import keyvis_widget
import colorscript_widget
import animatic_widget
import light_widget
import fx_widget
import comp_widget

# sg
from rf_utils.sg import sg_utils
from rf_utils import project_info
from rf_utils.context import context_info
from rf_utils import daily
from rf_utils import user_info
from rf_utils.pipeline import user_pref

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

class Color:
    red = 'color: rgb(200, 0, 0);'
    green = 'color: rgb(0, 240, 0);'
    grey = 'color: rgb(120, 120, 120);'
    bgGreen = 'background-color: rgb(0, 140, 0);'
    bgYellow = 'background-color: rgb(200, 120, 0);'
    bgRed = 'background-color: rgb(140, 0, 0);'
    bgGrey = 'background-color: rgb(40, 40, 40);'

class RFShotPublish(QtWidgets.QMainWindow):

    def __init__(self, parent=None):
        #Setup Window
        super(RFShotPublish, self).__init__(parent)

        # ui read
        uiFile = '%s/ui.ui' % moduleDir
        self.ui = load.setup_ui(uiFile, self)

        self.ui.show()
        self.ui.setWindowTitle('%s %s-%s' % (_title, _version, _des))

        # data
        self.projectInfo = project_info.ProjectInfo()
        self.userPref = user_pref.ToolSetting(uiName)
        self.userData = self.userPref.read()

        # ui
        # tabs
        self.animationTab = 'Animation'
        self.colorscriptTab = 'Colorscript'
        self.lightTab = 'Light'
        self.effectTab = 'fx'
        self.compTab = 'comp'
        self.stepMap = {self.animationTab: 'animation', self.colorscriptTab: 'colorscript', self.lightTab: 'light', self.effectTab: 'fx', self.compTab: 'comp'}
        self.tabIndex = [self.colorscriptTab, self.animationTab, self.lightTab, self.effectTab, self.compTab]

        self.setup_widgets()
        self.init_signals()
        self.init_functions()


    def setup_widgets(self):
        self.add_tab_layout()

        self.add_logo_widget()
        self.add_user_widget()
        self.set_header_layout()

        self.add_project_widget()
        self.add_sg_navigation()
        self.add_task_widget()
        self.add_status_widget()
        self.set_navigation_layout()

        self.add_colorscript_widget()
        self.add_animation_widget()
        self.add_light_widget()
        self.add_fx_widget()
        self.add_comp_widget()

        self.add_status_label()
        self.set_style()
        self.disable_widget()

    def add_tab_layout(self):
        self.tabWidget = QtWidgets.QTabWidget()

        # widgets
        self.animationTabWidget = QtWidgets.QWidget()
        self.animationLayout = QtWidgets.QVBoxLayout(self.animationTabWidget)

        self.colorscriptTabWidget = QtWidgets.QWidget()
        self.colorscriptLayout = QtWidgets.QVBoxLayout(self.colorscriptTabWidget)

        self.lightTabWidget = QtWidgets.QWidget()
        self.lightLayout = QtWidgets.QVBoxLayout(self.lightTabWidget)

        self.fxTabWidget = QtWidgets.QWidget()
        self.fxLayout = QtWidgets.QVBoxLayout(self.fxTabWidget)

        self.compTabWidget = QtWidgets.QWidget()
        self.compLayout = QtWidgets.QVBoxLayout(self.compTabWidget)

        # add tabs
        self.tabWidget.addTab(self.colorscriptTabWidget, self.colorscriptTab)
        # self.tabWidget.addTab(self.animationTabWidget, self.animationTab)
        self.tabWidget.addTab(self.lightTabWidget, self.lightTab)
        self.tabWidget.addTab(self.fxTabWidget, self.effectTab)
        self.tabWidget.addTab(self.compTabWidget, self.compTab)

        # set spacing
        self.animationLayout.setContentsMargins(0, 0, 0, 0)

        self.animationLayout.setSpacing(8)

        self.ui.tab_layout.addWidget(self.tabWidget)


    def add_logo_widget(self):
        # set logo widget
        self.logo = QtWidgets.QLabel()
        self.ui.head_layout.addWidget(self.logo)
        self.logo.setPixmap(QtGui.QPixmap(icon.logo).scaled(64, 64, QtCore.Qt.KeepAspectRatio))

    def add_user_widget(self):
        """ add user widget """
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.userWidget = user_widget.UserComboBox()

        self.ui.head_layout.addItem(spacerItem)
        self.ui.head_layout.addWidget(self.userWidget)

    def set_header_layout(self):
        self.ui.head_layout.setStretch(0, 0)
        self.ui.head_layout.setStretch(1, 3)
        self.ui.head_layout.setStretch(2, 1)

    def add_project_widget(self):
        """ add project widget """
        project = self.userData.get('project')
        self.projectWidget = project_widget.SGProjectComboBox(sg=sg_utils.sg, defaultProject=project)
        self.ui.navigation_layout.addWidget(self.projectWidget)

    def add_sg_navigation(self):
        """ add sg navigation widget """
        self.sgWidget = entity_browse_widget.SGSceneNavigation(sg=sg_utils.sg, shotFilter=True)
        self.sgWidget.shotExtraFilter = ['sg_shot_type', 'is', 'Shot']

        self.line = self.hline_widget()
        self.ui.navigation_layout.addWidget(self.sgWidget)
        self.ui.navigation_layout.addWidget(self.line)

    def add_create_shot(self):
        """ add create shot button """
        self.createShotButton = QtWidgets.QPushButton('Create Sequence')
        self.ui.navigation_layout.addWidget(self.createShotButton)


    def add_task_widget(self):
        self.taskWidget = sg_widget.TaskComboBox(sg=sg_utils.sg)
        self.ui.navigation_layout.addWidget(self.taskWidget)

        # set layout
        self.taskWidget.allLayout.setStretch(0, 0)
        self.taskWidget.allLayout.setStretch(1, 0)

    def add_status_widget(self):
        # self.statusLabel = QtWidgets.QLabel('Status')
        self.statusWidget = status_widget.TaskStatusWidget(layout=QtWidgets.QVBoxLayout())
        self.statusWidget.label.setText('Status')
        self.ui.navigation_layout.addWidget(self.statusWidget)


    def set_navigation_layout(self):
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.ui.navigation_layout.addItem(spacerItem)

        self.ui.navigation_layout.setStretch(0, 0) # project
        self.ui.navigation_layout.setStretch(1, 0) # sg
        self.ui.navigation_layout.setStretch(2, 0) # line
        self.ui.navigation_layout.setStretch(3, 0) # task
        self.ui.navigation_layout.setStretch(4, 0) # status
        self.ui.navigation_layout.setStretch(5, 4) # spacer

    def add_animation_widget(self):
        self.animationWidget = animation_widget.AnimationWidget(sg=sg_utils.sg)
        self.animationLayout.addWidget(self.animationWidget)

    def add_keyvis_widget(self):
        self.keyvisWidget = keyvis_widget.KeyvisWidget(sg=sg_utils.sg)
        self.keyvisLayout.addWidget(self.keyvisWidget)

    def add_colorscript_widget(self):
        self.colorscriptWidget = colorscript_widget.ColorscriptWidget(sg=sg_utils.sg)
        self.colorscriptLayout.addWidget(self.colorscriptWidget)

    def add_animatic_widget(self):
        self.animaticWidget = animatic_widget.AnimaticWidget(sg=sg_utils.sg)
        self.animaticLayout.addWidget(self.animaticWidget)

    def add_light_widget(self):
        self.lightWidget = light_widget.LightingWidget(sg=sg_utils.sg)
        self.lightLayout.addWidget(self.lightWidget)

    def add_fx_widget(self):
        self.fxWidget = fx_widget.FxWidget(sg=sg_utils.sg)
        self.fxLayout.addWidget(self.fxWidget)

    def add_comp_widget(self):
        self.compWidget = comp_widget.CompositeWidget(sg=sg_utils.sg)
        self.compLayout.addWidget(self.compWidget)


    def add_status_label(self):
        self.statusLabel = QtWidgets.QLabel()
        self.ui.tab_layout.addWidget(self.statusLabel)
        self.statusLabel.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)

        self.ui.tab_layout.setStretch(0, 1)
        self.ui.tab_layout.setStretch(1, 0)

    def set_style(self):
        """ override stylesheet """
        self.projectWidget.label.setStyleSheet(Color.grey)
        self.sgWidget.episodeLabel.setStyleSheet(Color.grey)
        self.sgWidget.shotLabel.setStyleSheet(Color.grey)
        self.taskWidget.label.setStyleSheet(Color.grey)
        self.statusWidget.label.setStyleSheet(Color.grey)
        self.sgWidget.shotWidget.displayAttr = 'sg_shortcode'

    def disable_widget(self):
        """ override and disable some functions """
        pass



    def hline_widget(self):
        line = QtWidgets.QFrame()
        line.setFrameShape(QtWidgets.QFrame.HLine)
        line.setFrameShadow(QtWidgets.QFrame.Sunken)
        return line

    # end layout

    def init_signals(self):
        self.projectWidget.projectChanged.connect(self.project_signal)
        self.sgWidget.completed.connect(self.navigation_trigger)
        self.sgWidget.episodeTriggered.connect(self.episode_trigger)
        self.tabWidget.currentChanged.connect(self.tab_changed)

        # stroyboard publish
        self.animationWidget.publishButton.clicked.connect(self.animation_publish)
        self.animationWidget.status.connect(self.print_status)

        # keyvis publish
        # self.keyvisWidget.publishButton.clicked.connect(self.keyvis_publish)
        # self.keyvisWidget.status.connect(self.print_status)

        # colorscript publish
        self.colorscriptWidget.publishButton.clicked.connect(self.colorscript_publish)
        self.colorscriptWidget.status.connect(self.print_status)

        self.lightWidget.publishButton.clicked.connect(self.light_publish)
        self.lightWidget.status.connect(self.print_status)

        self.fxWidget.publishButton.clicked.connect(self.fx_publish)
        self.fxWidget.status.connect(self.print_status)

        self.compWidget.publishButton.clicked.connect(self.comp_publish)
        self.compWidget.status.connect(self.print_status)
        # animatic publish
        # self.animaticWidget.publishButton.clicked.connect(self.animatic_publish)
        # self.animaticWidget.status.connect(self.print_status)

        # input control
        self.taskWidget.comboBox.currentIndexChanged.connect(self.task_changed)
        self.sgWidget.shotWidget.currentIndexChanged.connect(self.shot_changed)

    def init_functions(self):
        self.sgWidget.list_episodes(self.projectWidget.current_item())

         # restore last selection
        episode = self.userData.get('episode')
        self.sgWidget.episodeWidget.set_current_item(episode)

    def restore_last_selection(self):
        data = self.userPref.read()
        project = data['project']
        episode = data['episode']
        self.projectWidget.set_current_item(project)


    def project_signal(self, project):
        """ what happen when project changed """
        self.sgWidget.list_episodes(project)

        # save ui selection
        self.save_last_selection()

    def navigation_trigger(self, shotDict):
        if shotDict:
            project = shotDict['project']['name']
            entityType = 'Shot'
            entityName = shotDict['code']
            step = self.get_step_mode()
            self.taskWidget.activate(project, entityType, entityName, step)
        else:
            logger.warning('No shot on the list')

    def episode_trigger(self, epDict):
         # save ui selection
        self.save_last_selection()

    def refresh_shot(self, shotName):
        """ refresh and set current shotName """
        epDict = self.sgWidget.episodeWidget.current_item()
        self.sgWidget.episode_trigger(epDict)
        self.sgWidget.shotWidget.set_current_item(shotName)


    def tab_changed(self, index):
        shotDict = self.sgWidget.shotWidget.current_item()
        self.navigation_trigger(shotDict)

    def animation_publish(self):
        """ publish storyboard call """
        shotEntity, taskEntity, status, userEntity = self.collect_ui_data()
        if self.check_input_ui(shotEntity, taskEntity, userEntity):
            self.animationWidget.publish(shotEntity, taskEntity, userEntity, status)

            self.animationWidget.displayWidget.clear()

            message = 'Storyboard publish complete'
            self.complete_dialog(message)

    def keyvis_publish(self):
        """ publish keyvis call """
        shotEntity, taskEntity, status, userEntity = self.collect_ui_data()
        if self.check_input_ui(shotEntity, taskEntity, userEntity):
            self.keyvisWidget.publish(shotEntity, taskEntity, userEntity, status)

            message = 'Keyvis publish complete'
            self.complete_dialog(message)

    def light_publish(self):
        """ publish colorscript call """
        shotEntity, taskEntity, status, userEntity = self.collect_ui_data()
        if self.check_input_ui(shotEntity, taskEntity, userEntity):
            self.lightWidget.publish(shotEntity, taskEntity, userEntity, status)

            self.lightWidget.displayWidget.clear()

            message = 'light publish complete'
            self.complete_dialog(message)

    def fx_publish(self):
        """ publish colorscript call """
        shotEntity, taskEntity, status, userEntity = self.collect_ui_data()
        if self.check_input_ui(shotEntity, taskEntity, userEntity):
            self.fxWidget.publish(shotEntity, taskEntity, userEntity, status)

            self.fxWidget.displayWidget.clear()

            message = 'fx publish complete'
            self.complete_dialog(message)

    def comp_publish(self):
        """ publish colorscript call """
        shotEntity, taskEntity, status, userEntity = self.collect_ui_data()
        if self.check_input_ui(shotEntity, taskEntity, userEntity):
            self.compWidget.publish(shotEntity, taskEntity, userEntity, status)

            self.compWidget.displayWidget.clear()
            message = 'comp publish complete'
            self.complete_dialog(message)

    def colorscript_publish(self):
        """ publish colorscript call """
        shotEntity, taskEntity, status, userEntity = self.collect_ui_data()
        if self.check_input_ui(shotEntity, taskEntity, userEntity):
            self.colorscriptWidget.publish(shotEntity, taskEntity, userEntity, status)

            self.colorscriptWidget.displayWidget.clear()
            message = 'Colorscript publish complete'
            self.complete_dialog(message)

    def animatic_publish(self):
        """ publish animatic call """
        shotEntity, taskEntity, status, userEntity = self.collect_ui_data()
        if self.check_input_ui(shotEntity, taskEntity, userEntity):
            self.animaticWidget.publish(shotEntity, taskEntity, userEntity, status)

            message = 'Animatic publish complete'
            self.complete_dialog(message)

    def task_changed(self):
        self.taskWidget.comboBox.setStyleSheet('')

    def shot_changed(self):
        self.sgWidget.shotWidget.setStyleSheet('')


    def collect_ui_data(self):
        """ collect shot, task, status, user entity """
        shotEntity = self.sgWidget.shotWidget.current_item()
        taskEntity = self.taskWidget.current_item()
        status = self.statusWidget.current_item()
        userEntity = self.userWidget.data()

        return shotEntity, taskEntity, status, userEntity

    def check_input_ui(self, shotEntity, taskEntity, userEntity):
        status = True
        messages = []
        if not shotEntity:
            self.sgWidget.shotWidget.setStyleSheet(Color.bgRed)
            messages.append('No shot selected')
            status = False
        if not taskEntity:
            self.taskWidget.comboBox.setStyleSheet(Color.bgRed)
            messages.append('No task selected')
            status = False
        if not userEntity:
            messages.append('No task selected')
            status = False

        if not status:
            dialog.MessageBox.error('Error', ('\n').join(messages))

        return status


    def print_status(self, message):
        self.statusLabel.setText(message)
        QtWidgets.QApplication.processEvents()

    def complete_dialog(self, message):
        title = 'Complete'
        result = dialog.MessageBox.success(title, message)
        return result


    def get_step_mode(self):
        index = self.tabWidget.currentIndex()
        currentTab = self.tabWidget.tabText(index)
        return self.stepMap[currentTab]


    def save_last_selection(self):
        """ save selection to json file """
        data = dict()
        project = str(self.projectWidget.projectComboBox.currentText())
        episode = str(self.sgWidget.episodeWidget.comboBox.currentText())
        data['project'] = project
        data['episode'] = episode

        result = self.userPref.write(data)


def show():
    logger.info('RFShotPublish Starting ...')
    logger.info('---------------------------')
    logger.info('---------------------------')
    app = QtWidgets.QApplication.instance()
    if not app:
        app = QtWidgets.QApplication(sys.argv)
    myApp = RFShotPublish()
    stylesheet.set_default(app)
    sys.exit(app.exec_())


def show2(parent=None): 
    myApp = RFShotPublish(parent)


if __name__ == '__main__':
    show()
