# v.0.0.1 beta
# v.0.0.3 add daily 
# v.0.0.4 hide excessive elements
_title = 'Riff Sequence Design Publish'
_version = 'v.0.0.1'
_des = 'beta'
uiName = 'RFSeqDesignPublish'

#Import python modules
import sys
import os 
import getpass
import json 
import logging

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path: 
    sys.path.append(core)

# import config
import rf_config as config

# framework modules 
from rf_utils import log_utils
from rf_utils.ui import load
from rf_utils.ui import stylesheet
from rf_utils import icon

# import widgets 
from rf_utils import publish_info
from rf_utils.widget import user_widget
from rf_utils.widget import project_widget
from rf_utils.widget import entity_browse_widget
from rf_utils.widget import sg_widget
from rf_utils.widget import status_widget
from rf_utils.widget import display_widget
from rf_utils.widget import dialog
from rf_utils.widget.app import create_entity_widget
import storyboard_widget
import keyvis_widget
import colorscript_widget

# sg 
from rf_utils.sg import sg_utils
from rf_utils import project_info
from rf_utils.context import context_info
from rf_utils import daily
from rf_utils import user_info
from rf_utils.pipeline import user_pref

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

class Color: 
    red = 'color: rgb(200, 0, 0);'
    green = 'color: rgb(0, 240, 0);'
    grey = 'color: rgb(120, 120, 120);'
    bgGreen = 'background-color: rgb(0, 140, 0);'
    bgYellow = 'background-color: rgb(200, 120, 0);'
    bgRed = 'background-color: rgb(140, 0, 0);'
    bgGrey = 'background-color: rgb(40, 40, 40);'

class RFDesignPublish(QtWidgets.QMainWindow):

    def __init__(self, project=None, auto=True, parent=None):
        #Setup Window
        super(RFDesignPublish, self).__init__(parent)

        # ui read
        uiFile = '%s/ui.ui' % moduleDir
        self.ui = load.setup_ui(uiFile, self)
        
        self.ui.show()
        self.ui.setWindowTitle('%s %s-%s' % (_title, _version, _des))

        # data 
        self.projectInfo = project_info.ProjectInfo()
        self.userPref = user_pref.ToolSetting(uiName)
        self.userData = self.userPref.read()

        # ui 
        # tabs
        self.storyboardTab = 'Storyboard'
        self.keyvisTab = 'Keyvis'
        self.colorscriptTab = 'Colorscript'
        self.tabIndex = [self.storyboardTab, self.keyvisTab, self.colorscriptTab]

        self.setup_widgets()
        self.init_signals()
        self.init_functions()


    def setup_widgets(self): 
        self.add_logo_widget() 
        self.add_user_widget()
        self.set_header_layout()

        self.add_project_widget()
        self.add_sg_navigation()
        self.add_create_shot()
        self.add_task_widget()
        self.add_status_widget()
        self.set_navigation_layout()

        self.add_storyboard_widget()
        self.add_keyvis_widget()
        self.add_colorscript_widget()

        self.add_status_label()
        self.set_style()

    
    def add_logo_widget(self): 
        # set logo widget 
        self.logo = QtWidgets.QLabel() 
        self.ui.head_layout.addWidget(self.logo)
        self.logo.setPixmap(QtGui.QPixmap(icon.logo).scaled(64, 64, QtCore.Qt.KeepAspectRatio)) 

    def add_user_widget(self): 
        """ add user widget """
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.userWidget = user_widget.UserComboBox()

        self.ui.head_layout.addItem(spacerItem)
        self.ui.head_layout.addWidget(self.userWidget)

    def set_header_layout(self): 
        self.ui.head_layout.setStretch(0, 0)
        self.ui.head_layout.setStretch(1, 3)
        self.ui.head_layout.setStretch(2, 1)

    def add_project_widget(self): 
        """ add project widget """ 
        project = self.userData.get('project')
        self.projectWidget = project_widget.SGProjectComboBox(sg=sg_utils.sg, defaultProject=project)
        self.ui.navigation_layout.addWidget(self.projectWidget)

    def add_sg_navigation(self): 
        """ add sg navigation widget """ 
        self.sgWidget = entity_browse_widget.SGSceneNavigation(sg=sg_utils.sg, shotFilter=False)
        self.sgWidget.shotExtraFilter = ['sg_shot_type', 'is', 'Sequence']
        self.sgWidget.shotLabel.setText('Sequence')

        self.line = self.hline_widget()
        self.ui.navigation_layout.addWidget(self.sgWidget)
        self.ui.navigation_layout.addWidget(self.line)

    def add_create_shot(self): 
        """ add create shot button """
        self.createShotButton = QtWidgets.QPushButton('Create Sequence')
        self.ui.navigation_layout.addWidget(self.createShotButton)


    def add_task_widget(self): 
        self.taskWidget = sg_widget.TaskComboBox(sg=sg_utils.sg)
        self.ui.navigation_layout.addWidget(self.taskWidget)

        # set layout
        self.taskWidget.allLayout.setStretch(0, 0)
        self.taskWidget.allLayout.setStretch(1, 0)

    def add_status_widget(self): 
        # self.statusLabel = QtWidgets.QLabel('Status')
        self.statusWidget = status_widget.TaskStatusWidget(layout=QtWidgets.QVBoxLayout())
        self.statusWidget.label.setText('Status')
        self.ui.navigation_layout.addWidget(self.statusWidget)


    def set_navigation_layout(self): 
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.ui.navigation_layout.addItem(spacerItem)

        self.ui.navigation_layout.setStretch(0, 0) # project
        self.ui.navigation_layout.setStretch(1, 0) # sg 
        self.ui.navigation_layout.setStretch(2, 0) # create shot button 
        self.ui.navigation_layout.setStretch(3, 0) # line 
        self.ui.navigation_layout.setStretch(4, 0) # task
        self.ui.navigation_layout.setStretch(5, 0) # status
        self.ui.navigation_layout.setStretch(6, 4) # spacer

    def add_storyboard_widget(self): 
        self.storyboardWidget = storyboard_widget.StoryboardWidget(sg=sg_utils.sg)
        self.ui.storyboard_layout.addWidget(self.storyboardWidget)

    def add_keyvis_widget(self): 
        self.keyvisWidget = keyvis_widget.KeyvisWidget(sg=sg_utils.sg)
        self.ui.keyvis_layout.addWidget(self.keyvisWidget)

    def add_colorscript_widget(self): 
        self.colorscriptWidget = colorscript_widget.ColorscriptWidget(sg=sg_utils.sg)
        self.ui.colorscript_layout.addWidget(self.colorscriptWidget)

    def add_status_label(self): 
        self.statusLabel = QtWidgets.QLabel()
        self.ui.tab_layout.addWidget(self.statusLabel)
        self.statusLabel.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)

        self.ui.tab_layout.setStretch(0, 1)
        self.ui.tab_layout.setStretch(1, 0)

    def set_style(self): 
        """ override stylesheet """
        self.projectWidget.label.setStyleSheet(Color.grey)
        self.sgWidget.episodeLabel.setStyleSheet(Color.grey)
        self.sgWidget.shotLabel.setStyleSheet(Color.grey)
        self.taskWidget.label.setStyleSheet(Color.grey)
        self.statusWidget.label.setStyleSheet(Color.grey)


    def hline_widget(self): 
        line = QtWidgets.QFrame()
        line.setFrameShape(QtWidgets.QFrame.HLine)
        line.setFrameShadow(QtWidgets.QFrame.Sunken)
        return line

    # end layout

    def init_signals(self): 
        self.projectWidget.projectChanged.connect(self.project_signal)
        self.sgWidget.completed.connect(self.navigation_trigger)
        self.sgWidget.episodeTriggered.connect(self.episode_trigger)
        self.ui.tabWidget.currentChanged.connect(self.tab_changed)

        # stroyboard publish
        self.storyboardWidget.publishButton.clicked.connect(self.storyboard_publish)
        self.storyboardWidget.status.connect(self.print_status)

        # create sequence (shot)
        self.createShotButton.clicked.connect(self.create_shot)

        # keyvis publish
        self.keyvisWidget.publishButton.clicked.connect(self.keyvis_publish)
        self.keyvisWidget.status.connect(self.print_status)

        # colorscript publish 
        self.colorscriptWidget.publishButton.clicked.connect(self.colorscript_publish)
        self.colorscriptWidget.status.connect(self.print_status)

    def init_functions(self): 
        self.sgWidget.list_episodes(self.projectWidget.current_item())

         # restore last selection 
        episode = self.userData.get('episode')
        self.sgWidget.episodeWidget.set_current_item(episode)

    def restore_last_selection(self): 
        data = self.userPref.read()
        project = data['project']
        episode = data['episode']
        self.projectWidget.set_current_item(project)


    def project_signal(self, project): 
        """ what happen when project changed """
        self.sgWidget.list_episodes(project)

        # save ui selection
        self.save_last_selection()

    def navigation_trigger(self, shotDict): 
        if shotDict: 
            project = shotDict['project']['name']
            entityType = 'Shot'
            entityName = shotDict['code']
            step = self.get_step_mode()
            self.taskWidget.activate(project, entityType, entityName, step)    
        else: 
            logger.warning('No shot on the list')

    def episode_trigger(self, epDict): 
         # save ui selection
        self.save_last_selection()

    def refresh_shot(self, shotName): 
        """ refresh and set current shotName """
        epDict = self.sgWidget.episodeWidget.current_item()
        self.sgWidget.episode_trigger(epDict)
        self.sgWidget.shotWidget.set_current_item(shotName)


    def tab_changed(self, index): 
        shotDict = self.sgWidget.shotWidget.current_item()
        self.navigation_trigger(shotDict)

    def storyboard_publish(self): 
        """ publish storyboard call """
        shotEntity, taskEntity, status, userEntity = self.collect_ui_data()
        self.storyboardWidget.publish(shotEntity, taskEntity, userEntity, status)

        message = 'Storyboard publish complete'
        self.complete_dialog(message)

    def keyvis_publish(self): 
        """ publish keyvis call """
        shotEntity, taskEntity, status, userEntity = self.collect_ui_data()
        self.keyvisWidget.publish(shotEntity, taskEntity, userEntity, status)
        
        message = 'Keyvis publish complete'
        self.complete_dialog(message)

    def colorscript_publish(self): 
        """ publish colorscript call """
        shotEntity, taskEntity, status, userEntity = self.collect_ui_data()
        self.colorscriptWidget.publish(shotEntity, taskEntity, userEntity, status)
        
        message = 'Colorscript publish complete'
        self.complete_dialog(message)

    def collect_ui_data(self): 
        """ collect shot, task, status, user entity """
        shotEntity = self.sgWidget.shotWidget.current_item()
        taskEntity = self.taskWidget.current_item()
        status = self.statusWidget.current_item()
        userEntity = self.userWidget.data()

        return shotEntity, taskEntity, status, userEntity




    def create_shot(self): 
        """ create shot dialog """ 
        project = self.projectWidget.current_item()
        episode = self.sgWidget.episodeWidget.current_item()
        context = context_info.Context()
        context.update(project=project['name'], projectCode=project['sg_project_code'], entityType='scene', entityGrp=episode['code'])
        scene = context_info.ContextPathInfo(context=context)
        shotName = create_entity_widget.CreateShotSequence.show(context=scene)

        # refresh by list episode 
        self.refresh_shot(shotName)


    def print_status(self, message): 
        self.statusLabel.setText(message)
        QtWidgets.QApplication.processEvents()

    
    def complete_dialog(self, message): 
        title = 'Complete'
        result = dialog.CompleteDialog.show(title, message)
        return result 


    def get_step_mode(self): 
        index = self.ui.tabWidget.currentIndex()
        currentTab = self.ui.tabWidget.tabText(index)
        return currentTab.lower()


    def save_last_selection(self): 
        """ save selection to json file """
        data = dict()
        project = str(self.projectWidget.projectComboBox.currentText())
        episode = str(self.sgWidget.episodeWidget.comboBox.currentText())
        data['project'] = project 
        data['episode'] = episode 

        result = self.userPref.write(data)


def show(project=None, auto=True):
    logger.info('RFDesignPublish Starting ...')
    logger.info('---------------------------')
    logger.info('---------------------------')
    app = QtWidgets.QApplication.instance()
    if not app: 
        app = QtWidgets.QApplication(sys.argv)
    myApp = RFDesignPublish(project=project, auto=auto)
    stylesheet.set_default(app)
    sys.exit(app.exec_())

if __name__ == '__main__': 
    show()
