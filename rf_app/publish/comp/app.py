#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import os
import getpass
import subprocess
from operator import itemgetter
from itertools import groupby
from collections import defaultdict
import glob

from settings import (
    UI_NAME
)
core_path = '%s/core' % os.environ.get('RFSCRIPT')
if core_path not in sys.path:
    sys.path.append(core_path)

import rf_config as config
# from rf_utils.context import context_info
from rf_utils.ui import load, stylesheet
from rf_utils.widget import browse_widget
from rf_utils import log_utils, project_info, icon
from rf_utils.widget import dialog
from rf_utils import icon

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide2', 'PySide'])
from Qt import QtWidgets, QtCore, QtGui

from models.project import Project
from models.work_scene import WorkScene

from rf_utils.context import context_info


userLog = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(UI_NAME, userLog)
logger = log_utils.init_logger(logFile)

UI_PATH = "{core}/rf_app/publish/comp/view/comp_publish.ui".format(core=core_path)
tasks_comp = ['comp_still', 'comp_full']
tasks_light = ['light_still', 'light_full']

class PublishAppCtrl(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super(PublishAppCtrl, self).__init__(parent)
        # ui read
        # if config.isMaya:
        #     self.ui = load.setup_ui_maya(UI_PATH, parent)
        # else:
        #     self.ui = load.setup_ui(UI_PATH, self)

        if config.isMaya:
            self.ui = load.setup_ui_maya(UI_PATH, parent)
        elif parent:
            if parent.metaObject().className() == 'Foundry::UI::DockMainWindow': # is Nuke
                self.ui = load.setup_ui_maya(UI_PATH, parent)
        else:
            self.ui = load.setup_ui(UI_PATH, self)

        self.ui.show()
        self.init_ui_app()
        self.init_state_app()
        # self.init_options()
        self.init_signal()

    def init_ui_app(self):
        app_icon = QtGui.QIcon()
        app_icon.addFile(icon.publish_sm, QtCore.QSize(16,16))
        self.ui.setWindowTitle("Comp publish")
        self.ui.setWindowIcon(app_icon)

    def init_state_app(self):
        self.project_model = Project()
        self.init_projects_combobox()
        self.init_entity_combobox()
        # self.init_task_combobox()
        if self.project_model.user_pref_data:
            self.init_default_setting()
        self.set_context_menu()
        # print "ssss", self.project_model.get_projects()

    def init_signal(self):
        self.ui.project_combobox.currentIndexChanged.connect(self.projects_combobox_changed)
        # self.ui.episode_combobox.currentIndexChanged.connect(self.episodes_combobox_changed)
        self.ui.entity_combobox.currentIndexChanged.connect(self.entity_combobox_changed)
        self.ui.department_combobox.currentIndexChanged.connect(self.department_combobox_change)
        self.ui.seq_list_widget.itemClicked.connect(self.seq_list_clicked)
        self.ui.shot_list_widget.itemClicked.connect(self.shot_list_clicked)
        self.ui.work_list_widget.itemClicked.connect(self.work_list_clicked)
        self.ui.submit_btn.clicked.connect(self.on_submit)
        self.ui.play_rv_btn.clicked.connect(self.on_playrv_clicked)
        self.ui.publish_btn.clicked.connect(self.on_publish_clicked)
        self.ui.add_btn.clicked.connect(self.on_add_clicked)
        self.ui.remove_btn.clicked.connect(self.on_remove_clicked)
        self.ui.publish_queue_btn.clicked.connect(self.on_publish_queue_clicked)

    def set_context_menu(self):
        self.ui.work_list_widget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.ui.work_list_widget.customContextMenuRequested.connect(self.work_list_widget_right_clicked)

    def work_list_widget_right_clicked(self, position):
        try:
            menu = QtWidgets.QMenu()
            menu.addAction("Open Explorer Work")
            menu.addAction("Open Explorer Publish")
            action = menu.exec_(self.ui.work_list_widget.mapToGlobal(position))
            if action:
                if action.text() == "Open Explorer Work":
                    self.open_explorer()
                if action.text() == "Open Explorer Publish":
                    self.open_explorer_publish()
        except Exception as e:
            tb = sys.exc_info()[2]
            print e
            print "line number : %s", tb.tb_lineno
            self.show_critical_box(e)

    def open_explorer(self):
        dir_selected = self.get_work_selected()
        subprocess.Popen(r'explorer %s' % os.path.dirname(dir_selected[0]['dir_path']).replace('/', '\\'))

    def open_explorer_publish(self):
        dir_selected = self.get_work_selected()
        scene = context_info.ContextPathInfo(path=dir_selected[0]['dir_path'])
        dst_path = scene.path.output().abs_path()
        subprocess.Popen(r'explorer %s' % os.path.dirname(dst_path).replace('/', '\\'))

    def init_projects_combobox(self):
        self.ui.project_combobox.blockSignals(True)
        self.ui.project_combobox.clear()
        projects = self.project_model.get_projects()
        if len(projects) == 0:
            self.ui.project_combobox.addItem("Not Found")
        else:
            for row, project in enumerate(projects):
                self.ui.project_combobox.addItem(project.get('name'))
                self.ui.project_combobox.setItemData(row, project, QtCore.Qt.UserRole)
        self.ui.project_combobox.blockSignals(False)
        self.projects_combobox_changed()

    def init_episodes_combobox(self):
        self.ui.episode_combobox.blockSignals(True)
        self.ui.episode_combobox.clear()
        project_selected = self.get_project_selected()
        episodes = self.project_model.get_episodes(project_selected)
        if len(episodes) == 0:
            self.ui.episode_combobox.addItem("Not Found")
        else:
            for row, episode in enumerate(episodes):
                self.ui.episode_combobox.addItem(episode.get('code'))
                self.ui.episode_combobox.setItemData(row, episode, QtCore.Qt.UserRole)
        self.ui.episode_combobox.blockSignals(False)

    def init_entity_combobox(self):
        self.ui.entity_combobox.blockSignals(True)
        self.ui.entity_combobox.clear()
        entity = self.project_model.get_entity()
        if len(entity) == 0:
            self.ui.entity_combobox.addItem("Not Found")
        else:
            for row, entity in enumerate(entity):
                self.ui.entity_combobox.addItem(entity)
                self.ui.entity_combobox.setItemData(row, entity, QtCore.Qt.UserRole)
        self.ui.entity_combobox.blockSignals(False)
        self.init_departments_combobox()

    def init_task_combobox(self): 
        self.ui.task_comboBox.clear()
        department = self.get_department_selected()
        if department == 'comp':
            self.ui.task_comboBox.addItems(tasks_comp)
        elif department == 'light':
            self.ui.task_comboBox.addItems(tasks_light)

    def init_departments_combobox(self):
        self.ui.department_combobox.blockSignals(True)
        self.ui.department_combobox.clear()
        project_selected = self.get_project_selected()
        entity_selected = self.get_entity_selected()
        departments = self.project_model.get_departments(project_selected, entity_selected)
        if len(departments) == 0:
            self.ui.department_combobox.addItem("Not Found")
        else:
            for row, department in enumerate(departments):
                    self.ui.department_combobox.addItem(department)
                    self.ui.department_combobox.setItemData(row, department, QtCore.Qt.UserRole)
        self.ui.department_combobox.blockSignals(False)

        self.init_task_combobox()

    def init_default_setting(self):
        index_project = self.ui.project_combobox.findText(
            self.project_model.user_pref_data['default_project'],
            QtCore.Qt.MatchFixedString
        )
        if index_project >= 0:
            self.ui.project_combobox.setCurrentIndex(index_project)
        self.projects_combobox_changed()

        index_episode = self.ui.episode_combobox.findText(
            self.project_model.user_pref_data['default_episode'],
            QtCore.Qt.MatchFixedString
        )
        if index_episode >= 0:
            self.ui.episode_combobox.setCurrentIndex(index_episode)

        index_step = self.ui.entity_combobox.findText(
            self.project_model.user_pref_data['default_entity'],
            QtCore.Qt.MatchFixedString
        )
        if index_step >= 0:
            self.ui.entity_combobox.setCurrentIndex(index_step)
        self.init_departments_combobox()

        index_order = self.ui.department_combobox.findText(
            self.project_model.user_pref_data['default_department'],
            QtCore.Qt.MatchFixedString
        )
        if index_order >= 0:
            self.ui.department_combobox.setCurrentIndex(index_order)

        self.init_task_combobox()

    def projects_combobox_changed(self):
        project_selected = self.get_project_selected()
        self.project_model.set_project_selected(project_selected)
        self.projectInfo = project_info.ProjectInfo(project=project_selected)
        self.projectInfo.set_project_env()
        self.init_episodes_combobox()

    # def episodes_combobox_changed(self):
        # episode_selected = self.get_episode_selected()
        # self.project_model.set_episode_selected(episode_selected)
        # self.init_episodes_combobox()

    def entity_combobox_changed(self):
        # self.project_model.set_episode_selected(episode_selected)
        self.init_departments_combobox()

    def department_combobox_change(self):
        self.init_task_combobox()

    def get_project_selected(self):
        index = self.ui.project_combobox.currentIndex()
        project_selected = self.ui.project_combobox.itemData(index, QtCore.Qt.UserRole)
        return project_selected

    def get_episode_selected(self):
        index = self.ui.episode_combobox.currentIndex()
        episode_selected = self.ui.episode_combobox.itemData(index, QtCore.Qt.UserRole)
        return episode_selected

    def get_entity_selected(self):
        entity_text = self.ui.entity_combobox.currentText()
        return entity_text

    def get_department_selected(self):
        index = self.ui.department_combobox.currentIndex()
        department_selected = self.ui.department_combobox.itemData(index, QtCore.Qt.UserRole)
        return department_selected

    def get_seqs_selected(self):
        seqs_selected = []
        items = self.ui.seq_list_widget.selectedItems()
        for i in xrange(0, len(items)):
            seq_item = self.ui.seq_list_widget.selectedItems()[i].data(QtCore.Qt.UserRole)
            seqs_selected.append(seq_item)
        return seqs_selected

    def get_shot_selected(self):
        shot_selected = []
        items = self.ui.shot_list_widget.selectedItems()
        for i in xrange(0, len(items)):
            shot_item = self.ui.shot_list_widget.selectedItems()[i].data(QtCore.Qt.UserRole)
            shot_selected.append(shot_item)
        return shot_selected

    def get_work_selected(self):
        work_selected = []
        items = self.ui.work_list_widget.selectedItems()
        for i in xrange(0, len(items)):
            work_item = self.ui.work_list_widget.selectedItems()[i].data(QtCore.Qt.UserRole)
            work_selected.append(work_item)
        return work_selected

    def get_task_selected(self): 
        return str(self.ui.task_comboBox.currentText())

    def set_seq_list(self):
        self.ui.seq_list_widget.clear()
        seq_list = self.work_scene.get_seqs_list()
        for seq_name in sorted(seq_list):
            widget_item = QtWidgets.QListWidgetItem(self.ui.seq_list_widget)
            widget_item.setText(seq_name)
            data = {
                "seq_name": seq_name,
                "dir_path": self.work_scene.get_base_path()
            }
            widget_item.setData(QtCore.Qt.UserRole, data)
            self.ui.seq_list_widget.addItem(widget_item)

    def set_shot_list(self):
        self.ui.shot_list_widget.clear()
        seqs_selected = self.get_seqs_selected()
        for seq in seqs_selected:
            shots = self.work_scene.get_shots_by_seq(seq["seq_name"])
            for shot_name in sorted(shots):
                widget_item = QtWidgets.QListWidgetItem(self.ui.shot_list_widget)
                widget_item.setText(shot_name)
                data = {
                    "shot_name": shot_name,
                    "dir_path": self.work_scene.get_shot_path(shot_name)
                }
                widget_item.setData(QtCore.Qt.UserRole, data)
                self.ui.shot_list_widget.addItem(widget_item)

    def set_work_list(self):
        self.ui.work_list_widget.clear()
        self.work_scene.process_work_scene()
        work_versions = self.work_scene.get_work_versions()
        if len(work_versions) == 0:
            self.ui.work_list_widget.addItem("Not Found")
        for work_file in work_versions:
            widget_item = QtWidgets.QListWidgetItem(self.ui.work_list_widget)
            widget_item.setText(work_file)
            data = {
                "work_file": work_file,
                "dir_path": self.work_scene.get_work_path()
            }
            widget_item.setData(QtCore.Qt.UserRole, data)

            # --- set tooltip of files inside grouped by sequence
            sequence_dir = '{}/{}'.format(self.work_scene.get_work_path(), work_file)
            files = os.listdir(sequence_dir)
            valid_files = defaultdict(list)
            for f in files:
                fp = '{}/{}'.format(sequence_dir, f)
                if not os.path.isfile(fp):
                    continue
                fsplits = f.split('.')
                if len(fsplits) != 4:
                    continue
                frame_part = fsplits[2]
                if not all([i for i in frame_part if i.isdigit()]):
                    continue
                
                ext = fsplits[3]
                element_name = '.'.join(fsplits[:2])
                element_name += '.*.{}'.format(ext)
                frame_num = int(frame_part)
                valid_files[element_name].append(frame_num)

            tooltips = []
            for name, frames in valid_files.iteritems():
                frames = sorted(frames)
                groups = group_range_in_list(frames)
                for g in groups:
                    if isinstance(g, tuple):
                        range_txt = '[{}-{}]'.format(str(g[0]).zfill(4), str(g[1]).zfill(4))
                    elif isinstance(g, int):
                        range_txt = str(g).zfill(4)
                    tooltips.append(name.replace('*', range_txt))

            widget_item.setToolTip('\n'.join(tooltips))

            self.ui.work_list_widget.addItem(widget_item)

    def seq_list_clicked(self):
        seqs_selected = self.get_seqs_selected()
        self.work_scene.clear_shots()
        self.work_scene.set_seq_selected(seqs_selected[0])
        self.set_shot_list()

    def shot_list_clicked(self):
        shot_selected = self.get_shot_selected()
        self.work_scene.clear_work()
        self.work_scene.set_shot_selected(shot_selected[0])
        self.set_work_list()

    def work_list_clicked(self):
        work_selected = self.get_work_selected()
        # print("work_selected", work_selected)
        self.ui.play_rv_btn.setEnabled(True)
        self.ui.publish_btn.setEnabled(True)
        # self.work_scene.clear_work()
        self.work_scene.set_work_selected(work_selected)
        # self.set_work_list()

    def on_submit(self):
        self.loading_start()
        QtCore.QCoreApplication.processEvents()
        try:
            project_selected = self.get_project_selected()
            episode_selected = self.get_episode_selected()
            entity_selected = self.get_entity_selected()
            department_selected = self.get_department_selected()
            self.project_model.save_setting_project(
                project_selected,
                episode_selected,
                entity_selected,
                department_selected
            )
            if entity_selected == "Scene":
                print("Searching work scene")
                self.work_scene = WorkScene(
                    project_selected,
                    episode_selected,
                    entity_selected,
                    department_selected
                )
                self.work_scene.set_base_path()
                self.set_seq_list()
            elif entity_selected == "Asset":
                print("Searching work asset")
        except Exception as e:
            tb = sys.exc_info()[2]
            print e
            print "line number : %s", tb.tb_lineno
            self.show_critical_box(e)
        finally:
            self.loading_stop()

    def on_playrv_clicked(self):
        self.loading_start()
        QtCore.QCoreApplication.processEvents()
        try:
            work_selected = self.get_work_selected()
            if len(work_selected) == 0:
                self.show_message_box("Please select some file.")
                return
            self.work_scene.process_play_rv(work_selected)
        except Exception as e:
            tb = sys.exc_info()[2]
            print e
            print "line number : %s", tb.tb_lineno
            self.show_critical_box(e)
        finally:
            self.loading_stop()

    def on_publish_clicked(self, work=None, task=None, queue=False, shot_selected=None):
        self.loading_start()
        QtCore.QCoreApplication.processEvents()
        try:
            work_selected = self.get_work_selected() if not work else work 
            task_selected = self.get_task_selected() if not task else task
            if len(work_selected) == 0:
                self.show_message_box("Please select some file.")
                return
            
            if shot_selected: 
                self.work_scene.clear_work()
                self.work_scene.set_shot_selected(shot_selected[0])
            self.work_scene.publish_work_file(work_selected, task_selected)
            if not queue: 
                self.show_message_box("Published Success")
        except WindowsError as e:
            if not queue: 
                self.show_critical_box("File is in use by somebody")
                print "*"*50
                print e
                print "*"*50
            return False
        except Exception as e:
            tb = sys.exc_info()[2]
            print e
            print "line number : %s", tb.tb_lineno
            if not queue: 
                self.show_critical_box(e)
            return False
        finally:
            self.loading_stop()

        return True

    def on_add_clicked(self): 
        work_selected = self.get_work_selected()
        task_selected = self.get_task_selected()
        shot_selected = self.get_shot_selected()

        all_items = [self.ui.queue_list_widget.item(a).text() for a in range(self.ui.queue_list_widget.count())]

        dir_path = work_selected[0]['dir_path']
        version = work_selected[0]['work_file']

        scene = context_info.ContextPathInfo(path=dir_path)
        display = '{} - {} - {}'.format(scene.name, task_selected, version)

        if not display in all_items: 
            item = QtWidgets.QListWidgetItem(self.ui.queue_list_widget)
            item.setText(display)
            item.setData(QtCore.Qt.UserRole, [work_selected, task_selected, shot_selected])
            iconWidget = QtGui.QIcon()
            iconWidget.addPixmap(QtGui.QPixmap(icon.gear),QtGui.QIcon.Normal,QtGui.QIcon.Off)
            item.setIcon(iconWidget)
        else: 
            dialog.MessageBox.warning('Warning', '{} already added'.format(display))

    def on_remove_clicked(self): 
        self.ui.queue_list_widget.takeItem(self.ui.queue_list_widget.currentRow())

    def on_publish_queue_clicked(self): 
        def set_status(icon_path): 
            iconWidget = QtGui.QIcon()
            iconWidget.addPixmap(QtGui.QPixmap(icon_path),QtGui.QIcon.Normal,QtGui.QIcon.Off)
            item.setIcon(iconWidget)
            QtWidgets.QApplication.processEvents()

        all_items = [self.ui.queue_list_widget.item(a) for a in range(self.ui.queue_list_widget.count())]

        for item in all_items: 
            work_selected, task_selected, shot_selected = item.data(QtCore.Qt.UserRole)
            set_status(icon.sgIp)
            result = self.on_publish_clicked(work_selected, task_selected, queue=True, shot_selected=shot_selected)
            icon_path = icon.ok if result else icon.no 
            set_status(icon_path)

    def loading_start(self):
        self.ui.loading_txt.setText("Loading ...")

    def loading_stop(self):
        self.ui.loading_txt.setText("")

    def show_critical_box(self, msg):
        QtWidgets.QMessageBox.critical(self, "Message", "Error: " + str(msg) + ". Please contact admin.")

    def show_message_box(self, msg):
        QtWidgets.QMessageBox.information(self, "Message", str(msg))

def group_range_in_list(num_list):
    new_list = []
    for k, g in groupby(enumerate(num_list), lambda (i,x): i-x):
        group = map(itemgetter(1), g)
        if len(group) > 1:
            elem = (group[0], group[-1])
        else:
            elem = group[0]
        new_list.append(elem)
    return new_list

def show():
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        maya_win.deleteUI(UI_NAME)
        myApp = PublishAppCtrl(maya_win.getMayaWindow())
        return myApp
    else:
        logger.info('Run in launcher\n')
        app = QtWidgets.QApplication.instance()
        if not app:
            app = QtWidgets.QApplication(sys.argv)
        myApp = PublishAppCtrl()
        # myApp.ui.show()
        stylesheet.set_default(app)
        sys.exit(app.exec_())


if __name__ == '__main__':
    show()
