import os
import subprocess
import shutil
import tempfile

import comp_pub
from services import ShotgunServices
from rf_utils.context import context_info
from rf_utils.oiio import run_oiio
# import shutil

class TempConvertAcesSrgb(): 
    """
    Convert images to srgb in temp and delete temp when exit
    """
    """
    Update split a list into chunks of size 100
    """
    def __init__(self, use_ocio, paths):
        self.use_ocio = use_ocio
        self.paths = paths
        self.tmp_dir = None
        self.output_paths = []
        self.chunk_size = 100

    def __enter__(self): 
        if self.use_ocio:

            # convert aces to png - write to temp
            self.tmp_dir = tempfile.mkdtemp().replace("\\", "/")
            temp_paths = []
            for path in self.paths:
                basename = os.path.basename(path)
                out_fh, out_temp = tempfile.mkstemp(dir=self.tmp_dir)
                os.close(out_fh)
                temp_path = "{}/{}.png".format(self.tmp_dir, os.path.splitext(basename)[0])
                os.rename(out_temp, temp_path)
                temp_paths.append(temp_path)


            self.paths_chunks = [self.paths[i:i + self.chunk_size] for i in range(0, len(self.paths), self.chunk_size)]
            self.temp_paths_chunks = [temp_paths[i:i + self.chunk_size] for i in range(0, len(temp_paths), self.chunk_size)]

            for p, tmp in zip(self.paths_chunks, self.temp_paths_chunks):
                output_paths = run_oiio.convert_colorspace(p, tmp, "ACES - ACES2065-1", "Output - sRGB")
                self.output_paths += output_paths

        else:
            self.output_paths = self.paths
        return self

    def __exit__(self, *args): 
        if self.tmp_dir and os.path.exists(self.tmp_dir):
            shutil.rmtree(self.tmp_dir)

class WorkScene:
    def __init__(self, project, episode, entity, department):
        self.project = project
        self.episode = episode
        self.entity = entity
        self.department = department
        self.seq_dirs = []
        self.seq_selected = ""
        self.shot_dirs = []
        self.shot_selected = ""
        self.work_versions = []
        self.work_selected = ""
        self.base_path = ""
        self.sg = ShotgunServices().sg_connection()

    def work_scene(self):
        pass

    def get_seqs_list(self):
        shot_dirs = list(filter(lambda shot_dir: shot_dir.split("_")[-1] != "all" and len(shot_dir.split("_")) == 4, os.listdir(self.base_path)))
        for shot in shot_dirs:
            seq_name = shot.split("_")[2]
            if seq_name not in self.seq_dirs and seq_name[0] == "q":
                self.seq_dirs.append(shot.split("_")[2])
        return self.seq_dirs

    def get_shots_by_seq(self, seq_selected):
        shot_dirs = list(filter(lambda shot_dir: shot_dir.split("_")[-1] != "all" and len(shot_dir.split("_")) == 4, os.listdir(self.base_path)))
        for shot in shot_dirs:
            seq_name = shot.split("_")[2]
            if seq_name == seq_selected:
                self.shot_dirs.append(shot)
        return self.shot_dirs

    def get_base_path(self):
        return self.base_path

    def get_shot_path(self, shot_name=None):
        if shot_name:
            return "{base_path}/{shot_name}".format(
                base_path=self.base_path,
                shot_name=shot_name
            )
        return "{base_path}/{shot_name}".format(
            base_path=self.base_path,
            shot_name=self.shot_selected['shot_name']
        )

    def get_work_path(self):
        work_path = "{base_path}/{shot_name}/{department}/main/output".format(
            base_path=self.base_path,
            shot_name=self.shot_selected['shot_name'],
            department=self.department
        )
        return work_path

    # def get_files_filter(self, images_path, ext):
    #     return sorted(filter(lambda x: os.path.splitext(x)[1] == ext, os.listdir(images_path)))

    def process_work_scene(self):
        if self.department == "comp" or self.department == "light":
            self.process_get_work_scene_comp()

    def get_work_versions(self):
        return self.work_versions

    def clear_seq(self):
        self.seq_dirs = []

    def clear_shots(self):
        self.shot_dirs = []

    def clear_work(self):
        self.work_versions = []

    def set_seq_selected(self, seq_selected):
        self.seq_selected = seq_selected

    def set_shot_selected(self, shot_selected):
        self.shot_selected = shot_selected

    def set_work_selected(self, work_selected):
        self.work_selected = work_selected

    def set_base_path(self):
        context = context_info.Context()
        context.update(
            project=self.project['name'],
            entityType='scene',
            entityGrp=self.episode['code']
        )
        self.asset_context = context_info.ContextPathInfo(context=context)
        self.base_path = '/'.join(self.asset_context.path.name().abs_path().split("/")[:-1])

    def process_get_work_scene_comp(self):
        work_path = self.get_work_path()
        if os.path.exists(work_path):
            self.work_versions = list(filter(lambda shot_dir: os.path.isdir(work_path + "/" + shot_dir), os.listdir(work_path)))

    def process_play_rv(self, files_selected):
        for file in os.listdir("C:/Program Files/Shotgun"):
            if "RV-" in file:
                rv_version = file
        command = ["C:/Program Files/Shotgun/{rv_version}/bin/rv.exe".format(rv_version=rv_version)]
        for file_item in files_selected:
            version_dir_path = self.combine_version_comp_dir(file_item)
            # version_dir_path = "{dir_path}/{version}".format(dir_path=file_item['dir_path'], version=file_item['work_file'])
            if os.path.exists(version_dir_path):
                command.append(version_dir_path)
        subprocess.Popen(command)

    def publish_work_file(self, work_selected, task_selected='comp'):
        for work_scene in work_selected:
            if self.department == "comp" or self.department == "light":
                has_exr = comp_pub.check_have_ext_file(work_scene)
                valid_size, img_sizes = comp_pub.check_valid_size(work_scene) if has_exr else False
                if has_exr and valid_size:
                    shot_path = self.get_shot_path()
                    nuke_dir, output_dir, outputmedia_dir = comp_pub.create_publish_structure_comp(
                        self.sg,
                        work_scene,
                        self.project['name'],
                        self.shot_selected['shot_name'],
                        self.department
                    )
                    comp_pub.gen_publish_file_structure_comp(
                        work_scene,
                        nuke_dir,
                        output_dir,
                        outputmedia_dir,
                        shot_path,
                        self.shot_selected['shot_name'],
                        self.department,
                        self.work_selected[0]['work_file']
                    )
                    nuke_file_path = comp_pub.get_nuke_file_path(nuke_dir)
                    if nuke_file_path:
                        # if not comp_pub.output_have_jpg(output_dir):
                        #     comp_pub.convert_exr_to_jpg_ffmpeg(output_dir)
                        exr_dir = "{output_dir}/exr".format(output_dir=output_dir)
                        files_images = sorted(['{}/{}'.format(exr_dir, f) for f in os.listdir(exr_dir) if f.endswith('.exr')])
                        if files_images and len(files_images) > 0:
                            # scene.context.use_sg(sg_process.sg, project=scene.project, entityType='scene', entityName=scene.name)
                            fps = self.asset_context.projectInfo.render.fps()

                            # if OCIO is used, convert all images to sRGB colorspace in temp folder
                            use_ocio = self.asset_context.projectInfo.render.use_ocio()
                            # Convert ACES images to srgb in temp and delete temp when exit
                            with TempConvertAcesSrgb(use_ocio=use_ocio, paths=files_images) as temp_convert:
                                # use different method creating mov for comp still
                                if task_selected == 'comp_still' or task_selected == 'light_still':
                                    output_mov_file, output_mov_file_upload_sg, output_mov_file_review = comp_pub.slideshow_mov(
                                        outputmedia_dir,
                                        temp_convert.output_paths,
                                        fps
                                    )

                                else: # it's comp_full

                                    output_mov_file, output_mov_file_upload_sg, output_mov_file_review = comp_pub.build_mov(
                                        outputmedia_dir,
                                        temp_convert.output_paths,
                                        fps
                                    )
                            nuke_mock_dir = "{shot_path}/{department}/main/nuke".format(shot_path=shot_path, department=self.department)
                            nuke_file = "{nuke_mock_dir}/{nuke_file_path}".format(
                                nuke_mock_dir=nuke_mock_dir,
                                nuke_file_path=nuke_file_path[0]
                            )
                            backup_nuke_path, backup_output_path, backup_outputMedia_path = comp_pub.backup_version(
                                nuke_dir,
                                output_dir,
                                outputmedia_dir,
                                work_scene,
                                nuke_file
                            )
                            comp_pub.rename_file_output_version_to_hero(
                                output_dir,
                                outputmedia_dir
                            )
                            publ_nuke_path, publ_output_path, publ_outputMedia_path = comp_pub.copy_to_publish(
                                self.sg,
                                nuke_dir,
                                output_dir,
                                outputmedia_dir,
                                self.project['name'],
                                self.shot_selected['shot_name'],
                                self.department
                            )
                            output_mov_file_upload_sg = output_mov_file_upload_sg.replace(output_dir, backup_output_path)
                            comp_pub.publish_version(
                                output_mov_file_upload_sg,
                                nuke_file,
                                self.work_selected[0]['work_file'], 
                                task_selected
                            )
                else:
                    if not has_exr:
                        raise Exception('EXR file not found')
                    if not valid_size:
                        raise Exception('Width and height need to be divisable by 2. Current: {} x {}'.format(img_sizes[0], img_sizes[1]))

    def combine_version_comp_dir(self, file_item):
        version_dir_path = "{dir_path}/{version}".format(
            dir_path=file_item['dir_path'],
            version=file_item['work_file']
        )
        return version_dir_path
