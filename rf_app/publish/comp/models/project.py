from services import ShotgunServices


from rf_utils.sg.sg_utils import (
    sg,
    get_all_projects,
    get_episodes_by_project
)
from rf_utils import project_info
from rf_utils.pipeline import user_pref


class Project:
    def __init__(self):
        self.init_projects()
        self.sg = ShotgunServices()
        self.project_selected = ""
        # self.departments = ["anim", "colorscript", "comp", "finalcam", "fx", "keyvis", "layout", "light", "setdress", "sim", "storyboard", "techanim"]
        self.entity = ["Asset", "Scene"]
        self.user_pref = user_pref.ToolSetting('comp_publish')
        self.user_pref_data = self.user_pref.read()

    def init_projects(self):
        print("init_projects")

    def get_projects(self):
        return get_all_projects()

    def get_episodes(self, project_selected):
        return get_episodes_by_project(project_selected)

    def get_entity(self):
        return self.entity

    def get_departments(self, project_selected, entity_selected):
        # print "project_selected: ", project_selected
        # print "entity_selected: ", entity_selected
        proj = project_info.ProjectInfo(project_selected['name'])
        if entity_selected == "Asset":
            return proj.step.list_asset()
        elif entity_selected == "Scene":
            return proj.step.list_scene()

    def set_project_selected(self, project_selected):
        self.project_selected = project_selected

    def set_episodes(self):
        self.episodes = get_episodes_by_project(self.project_selected)

    def save_setting_project(
        self,
        project,
        episode,
        entity,
        department
    ):
        self.user_pref.write({
            "default_project": project['name'],
            "default_episode": episode['code'],
            "default_entity": entity,
            "default_department": department
        })
