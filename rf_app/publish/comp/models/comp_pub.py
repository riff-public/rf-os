import os
import subprocess
import shutil
import glob
import tempfile

# import config
import rf_config as config
from rf_utils.context import context_info
from rf_utils.file_utils import xcopy_directory, xcopy_file, back_slash_to_forwardslash
from rf_app.publish.asset import sg_hook
from rf_utils.sg import sg_process
from rf_utils import user_info
from rf_utils.pipeline import watermark
from rf_utils.pipeline.convert_lib import get_media_dimension
from rf_utils import admin

def get_base_backup_path(nuke_file_path, work_scene):
    scene = context_info.ContextPathInfo(path=nuke_file_path)
    scene.context.update(publishVersion=work_scene['work_file'])
    result_path = scene.path.scheme(key='publishOutputPath').rel_version().abs_path()
    path_split = result_path.split("/")
    dst_path = '/'.join(path_split[:-2])

    return dst_path


def create_publish_structure_comp(sg, work_scene, project_name, shot_name, department):
    context = context_info.Context()
    context.use_sg(
        sg,
        project=project_name,
        entityType='scene',
        entityName=shot_name
    )
    context.update(step=department, process='main', publishVersion="hero")
    scene = context_info.ContextPathInfo(context=context)
    dst_path = scene.path.output().abs_path()
    dst_path = os.path.join(tempfile.gettempdir(),'hero_comp').replace('\\', '/')
    if os.path.exists(dst_path):
        admin.rmtree(dst_path)
    if not os.path.exists(dst_path):
        os.makedirs(dst_path)
        nuke_dir = "{dst_path}/nuke".format(dst_path=dst_path)
        os.makedirs(nuke_dir)
        output_dir = "{dst_path}/output".format(dst_path=dst_path)
        os.makedirs(output_dir)
        outputmedia_dir = "{dst_path}/outputMedia".format(dst_path=dst_path)
        os.makedirs(outputmedia_dir)

        return nuke_dir, output_dir, outputmedia_dir


def gen_publish_file_structure_comp(
    work_scene,
    nuke_dir,
    output_dir,
    outputmedia_dir,
    shot_path,
    shot_name,
    department,
    version_dir
):
    version_dir_path = combine_version_comp_dir(work_scene)
    exr_dir = "{output_dir}/exr".format(output_dir=output_dir)
    build_output_exr(version_dir_path, exr_dir)
    file_name = "{shot_name}_{department}_main.{version}".format(
        shot_name=shot_name,
        department=department,
        version=version_dir
    )
    nuke_file_path = "{shot_path}/{department}/main/nuke".format(shot_path=shot_path, department=department)
    for nuke_file_name in os.listdir(nuke_file_path):
        if file_name in nuke_file_name:
            src_nuke_file = "{nuke_file_path}/{nuke_file_name}".format(
                nuke_file_path=nuke_file_path,
                nuke_file_name=nuke_file_name
            )
            xcopy_file(src_nuke_file, nuke_dir)
            break
    else:  # the loop ended without break - .nk file with specified name not found
        raise Exception('Cannot find: {}/{}'.format(nuke_file_path, file_name))

    src_mov_file = "{work_dir}/{file_name}.mov".format(
        work_dir=work_scene['dir_path'],
        file_name=file_name
    )
    if os.path.isfile(src_mov_file):
        xcopy_file(src_mov_file, outputmedia_dir)


def combine_version_comp_dir(file_item):
    version_dir_path = "{dir_path}/{version}".format(
        dir_path=file_item['dir_path'],
        version=file_item['work_file']
    )
    return version_dir_path


def build_output_exr(version_dir_path, exr_dir):
    xcopy_directory(version_dir_path, exr_dir)


# def convert_exr_to_jpg_ffmpeg(output_dir):
#     if os.path.exists(output_dir):
#         jpg_dir = "{output_dir}/jpg".format(output_dir=output_dir)
#         exr_dir = "{output_dir}/exr".format(output_dir=output_dir)
#         if not os.path.exists(jpg_dir):
#             os.makedirs(jpg_dir)
#         for exr_file in os.listdir(exr_dir):
#             jpg_file_name = exr_file.replace(".exr", ".jpg")
#             subprocess.call([
#                 config.Software.ffmpeg,
#                 '-y',
#                 '-gamma',
#                 '2.2',
#                 '-i',
#                 '{exr_dir}/{exr_file}'.format(exr_dir=exr_dir, exr_file=exr_file),
#                 '{jpg_dir}/{jpg_file_name}'.format(jpg_dir=jpg_dir, jpg_file_name=jpg_file_name)
#             ])


# def convert_jpg_to_mov(output_dir):
#     if os.path.exists(output_dir):
#         for exr_file in os.listdir(output_dir):
#             jpg_file_name = exr_file.replace(".exr", ".jpg")
#             subprocess.call([
#                 config.Software.ffmpeg,
#                 '-y',
#                 '-gamma',
#                 '2.2',
#                 '-i',
#                 '{output_dir}/{exr_file}'.format(output_dir=output_dir, exr_file=exr_file),
#                 '{output_dir}/{jpg_file_name}'.format(output_dir=output_dir, jpg_file_name=jpg_file_name)
#             ])


# def output_have_jpg(output_dir):
#     for file in os.listdir(output_dir):
#         if ".jpg" in file:
#             return True
#     return False

def build_mov(outputmedia_dir, input_paths, framerate):
    filename = os.path.basename(input_paths[0])
    input_dir = os.path.dirname(input_paths[0])
    first_file_name, version, frame, first_ext = filename.split('.')
    output_mov_file = '{outputmedia_dir}/{first_file_name}.{version}.mov'.format(outputmedia_dir=outputmedia_dir, first_file_name=first_file_name, version=version)
    output_mov_file_upload_sg = '{outputmedia_dir}/{first_file_name}.{version}_preview.mov'.format(outputmedia_dir=outputmedia_dir, first_file_name=first_file_name, version=version)
    output_mov_file_review = '{outputmedia_dir}/{first_file_name}.{version}_review.mov'.format(outputmedia_dir=outputmedia_dir, first_file_name=first_file_name, version=version)

    # Lossless file
    subprocess.call([
        config.Software.ffmpeg,
        '-y',
        '-apply_trc',
        'iec61966_2_1',
        '-start_number',
        '1001',
        '-framerate',
        str(framerate),
        '-i',
        '{input_dir}/{first_file_name}.{version}.%04d.{first_ext}'.format(input_dir=input_dir, first_file_name=first_file_name, version=version, first_ext=first_ext),
        '-vcodec',
        'prores_ks',
        '-profile:v',
        '4444',
        '-quant_mat',
        'hq',
        '-vf',
        'scale=out_color_matrix=bt709',
        '-pix_fmt',
        'yuv444p10le',
        '-r',
        str(framerate),
        output_mov_file
    ])

    # small file for upload to SG
    subprocess.call([
        config.Software.ffmpeg,
        '-y',
        '-apply_trc',
        'iec61966_2_1',
        '-start_number',
        '1001',
        '-framerate',
        str(framerate),
        '-i',
        '{input_dir}/{first_file_name}.{version}.%04d.{first_ext}'.format(input_dir=input_dir, first_file_name=first_file_name, version=version, first_ext=first_ext),
        '-vcodec',
        'libx264',
        '-crf',
        '20',
        '-tune',
        'film',
        '-preset',
        'veryslow',
        '-x264opts',
        'bframes=1',
        '-vf',
        # 'scale=iw/2:ih/2, scale=out_color_matrix=bt709',
        'scale=out_color_matrix=bt709',
        '-pix_fmt',
        'yuv420p',
        '-r',
        str(framerate),
        output_mov_file_upload_sg
    ])

    # Hi res file for review
    subprocess.call([
        config.Software.ffmpeg,
        '-y',
        '-apply_trc',
        'iec61966_2_1',
        '-start_number',
        '1001',
        '-framerate',
        str(framerate),
        '-i',
        '{input_dir}/{first_file_name}.{version}.%04d.{first_ext}'.format(input_dir=input_dir, first_file_name=first_file_name, version=version, first_ext=first_ext),
        '-vcodec',
        'libx264',
        '-crf',
        '10',
        '-tune',
        'film',
        '-preset',
        'veryslow',
        '-x264opts',
        'bframes=1',
        '-vf',
        'scale=out_color_matrix=bt709',
        '-pix_fmt',
        'yuv420p',
        '-r',
        str(framerate),
        output_mov_file_review
    ])

    return output_mov_file, output_mov_file_upload_sg, output_mov_file_review

def slideshow_mov(outputmedia_dir, input_paths, framerate):
    # create output file name base one the first file name
    filename = os.path.basename(input_paths[0])
    input_dir = os.path.dirname(input_paths[0])
    first_file_name, version, frame, first_ext = filename.split('.')
    output_mov_file = '{outputmedia_path}/{first_file_name}.{version}.mov'.format(outputmedia_path=outputmedia_dir, first_file_name=first_file_name, version=version)
    output_mov_file_upload_sg = '{outputmedia_path}/{first_file_name}.{version}_preview.mov'.format(outputmedia_path=outputmedia_dir, first_file_name=first_file_name, version=version)
    output_mov_file_review = '{outputmedia_path}/{first_file_name}.{version}_review.mov'.format(outputmedia_path=outputmedia_dir, first_file_name=first_file_name, version=version)

    # add hud
    frames = []
    temp_images = []  # needs to collect temp to delete later
    for fp in input_paths:
        basename = os.path.basename(fp)
        fp_wm = watermark.add_border_hud_to_image(input_path=fp, 
                                            output_path='',
                                            topLeft=[],
                                            topMid=[],
                                            topRight=[],
                                            bottomLeft=[],
                                            bottomMid=[],
                                            bottomRight=[basename],
                                            canvas='original', 
                                            border_size=10)  # make text lower so it doesn't block much of the frame
        if fp_wm:
            fp = fp_wm
            temp_images.append(fp)
        frames.append(fp)

    # generate file list text file
    temp_hndl, temp_path = tempfile.mkstemp(suffix='.txt')
    temp_path = temp_path.replace('\\', '/')
    with open(temp_path, 'w')as tf:
        lines = []
        for f in frames: 
            l = "file 'file:{}'\n".format(f)
            lines.append(l)
        tf.writelines(lines)
    os.close(temp_hndl)  # close the handle so we can delete it later

    # Lossless file
    subprocess.call([
        config.Software.ffmpeg,
        '-y',
        '-apply_trc',
        'iec61966_2_1',
        '-f',
        'concat',
        '-safe',
        '0',
        '-r',
         str(framerate),
        '-i',
        temp_path,
        '-vcodec',
        'prores_ks',
        '-profile:v',
        '4444',
        '-quant_mat',
        'hq',
        '-vf',
        'scale=out_color_matrix=bt709',
        '-pix_fmt',
        'yuv444p10le',
        '-r',
        str(framerate),
        output_mov_file
    ])

    # small file for upload to SG
    subprocess.call([
        config.Software.ffmpeg,
        '-y',
        '-apply_trc',
        'iec61966_2_1',
        '-f',
        'concat',
        '-safe',
        '0',
        '-r',
         str(framerate),
        '-i',
        temp_path,
        '-vcodec',
        'libx264',
        '-crf',
        '20',
        '-tune',
        'film',
        '-preset',
        'veryslow',
        '-x264opts',
        'bframes=1',
        '-vf',
        'scale=iw:ih, scale=out_color_matrix=bt709',
        '-pix_fmt',
        'yuv420p',
        '-r',
        str(framerate),
        output_mov_file_upload_sg
    ])

    # Hi res file for review
    subprocess.call([
        config.Software.ffmpeg,
        '-y',
        '-apply_trc',
        'iec61966_2_1',
        '-f',
        'concat',
        '-safe',
        '0',
        '-r',
         str(framerate),
        '-i',
        temp_path,
        '-vcodec',
        'libx264',
        '-crf',
        '10',
        '-tune',
        'film',
        '-preset',
        'veryslow',
        '-x264opts',
        'bframes=1',
        '-vf',
        'scale=out_color_matrix=bt709',
        '-pix_fmt',
        'yuv420p',
        '-r',
        str(framerate),
        output_mov_file_review
    ])

    # remove temp file list text file
    admin.remove(temp_path)
    for temp_img in temp_images:
        admin.remove(temp_img)

    return output_mov_file, output_mov_file_upload_sg, output_mov_file_review

def publish_version(output_mov_file, nuke_file, version, task='comp'):
    scene_ctx = context_info.ContextPathInfo(path=nuke_file)
    scene_ctx.context.update(publishVersion=version)
    # import pdb; pdb.set_trace();
    task_entity = get_task(sg_process, scene_ctx, task)
    user = user_info.User()
    versionResult = sg_hook.publish_version(
        scene_ctx,
        task_entity,
        'rev',
        [output_mov_file],
        user.sg_user(),
        "Upload by comp publish",
        uploadOption="Comp publish Upload"
    )
    print("versionResult: ", versionResult)


def get_task(sg_process, scene, taskName):
    scene.context.use_sg(sg_process.sg, project=scene.project, entityType='scene', entityName=scene.name)
    taskEntity = sg_process.get_one_task_by_step(scene.context.sgEntity, scene.step, taskName, create=True)
    return taskEntity


def get_nuke_file_path(nuke_dir):
    return os.listdir(nuke_dir)


def backup_version(nuke_dir, output_dir, outputmedia_dir, work_scene, nuke_file):
    version_name = work_scene['work_file']
    base_path = get_base_backup_path(nuke_file, work_scene)
    backup_path = "{base_path}/{version_name}".format(
        base_path=base_path,
        version_name=version_name
    )
    backup_nuke_path = "{base_path}/{version_name}/nuke".format(
        base_path=base_path,
        version_name=version_name
    )
    backup_output_path = "{base_path}/{version_name}/output".format(
        base_path=base_path,
        version_name=version_name
    )
    backup_outputMedia_path = "{base_path}/{version_name}/outputMedia".format(
        base_path=base_path,
        version_name=version_name
    )
    if os.path.exists(backup_path):
        admin.rmtree(backup_path)
    if not os.path.exists(backup_path):
        os.makedirs(backup_path)
    xcopy_directory(nuke_dir, backup_nuke_path)

    if os.path.exists(backup_output_path):
        output_files = os.listdir(backup_output_path)
        for output_file in output_files:
            exr_output_path = os.path.join(backup_output_path, output_file)
            exr_files = os.listdir(exr_output_path)
            for exr_file in exr_files:
                exr_path = os.path.join(exr_output_path, exr_file)
                admin.remove(exr_path)
            exr_output_dir = os.path.join(output_dir, output_file)
            xcopy_directory(exr_output_dir, exr_output_path)
    else:
        shutil.copytree(output_dir, backup_output_path)
        
    xcopy_directory(outputmedia_dir, backup_outputMedia_path)

    return backup_nuke_path,backup_output_path,backup_outputMedia_path


def rename_file_output_version_to_hero(output_dir, outputmedia_dir):
    exr_dir = "{output_dir}/exr".format(output_dir=output_dir)
    # jpg_dir = "{output_dir}/jpg".format(output_dir=output_dir)

    list_exr_files = os.listdir(exr_dir)
    # list_jpg_files = os.listdir(jpg_dir)
    for exr_file in list_exr_files:
        new_exr_filename = rename_version_to_hero(exr_file)
        old_file = os.path.join(exr_dir, exr_file)
        new_file = os.path.join(exr_dir, new_exr_filename)
        os.rename(old_file, new_file)

    # for jpg_file in list_jpg_files:
    #     new_jpg_filename = rename_version_to_hero(jpg_file)
    #     old_file = os.path.join(jpg_dir, jpg_file)
    #     new_file = os.path.join(jpg_dir, new_jpg_filename)
    #     os.rename(old_file, new_file)

    list_mov_files = os.listdir(outputmedia_dir)
    for mov_file in list_mov_files:
        output_mov_file = os.path.split(mov_file)[-1]
        output_media_filename = rename_version_to_hero(output_mov_file)
        media_old_file = os.path.join(outputmedia_dir, output_mov_file)
        media_new_file = os.path.join(outputmedia_dir, output_media_filename)
        os.rename(media_old_file, media_new_file)

def copy_to_publish(sg, nuke_dir, output_dir, outputmedia_dir, project_name, shot_name, department):
    context = context_info.Context()
    context.use_sg(
        sg,
        project=project_name,
        entityType='scene',
        entityName=shot_name
    )
    context.update(step=department, process='main', publishVersion="hero")
    scene = context_info.ContextPathInfo(context=context)
    dst_path = scene.path.output().abs_path()
    publ_nuke_path = "{dst_path}/nuke".format(dst_path=dst_path)
    publ_output_path = "{dst_path}/output".format(dst_path=dst_path)
    publ_outputMedia_path = "{dst_path}/outputMedia".format(dst_path=dst_path)

    # admin.rmtree(publ_output_path)
    # remove exr file and replace
    if os.path.exists(publ_output_path):
        output_files = os.listdir(publ_output_path)
        for output_file in output_files:
            exr_output_path = os.path.join(publ_output_path, output_file)
            exr_files = os.listdir(exr_output_path)
            for exr_file in exr_files:
                exr_path = os.path.join(exr_output_path, exr_file)
                admin.remove(exr_path)
            exr_output_dir = os.path.join(output_dir, output_file)
            xcopy_directory(exr_output_dir, exr_output_path)
    else:
        shutil.copytree(output_dir, publ_output_path)
        
    xcopy_directory(nuke_dir, publ_nuke_path)
    # if not os.path.exists(publ_output_path):
    #     shutil.copytree(output_dir, publ_output_path)
    # else:
    #     xcopy_directory(output_dir, publ_output_path)
    xcopy_directory(outputmedia_dir, publ_outputMedia_path)
    admin.rmtree(nuke_dir)
    admin.rmtree(output_dir)
    admin.rmtree(outputmedia_dir)

    return publ_nuke_path, publ_output_path, publ_outputMedia_path

def rename_version_to_hero(file_name):
    file_name_split = file_name.split('.')
    # check rename hero_preview , hero_review
    if '_' in file_name_split[1]:
        file_name_split[1] = 'hero_{}'.format(file_name_split[1].split('_')[-1])
    else:
        file_name_split[1] = "hero"
    result = ".".join(file_name_split)
    return result


def check_have_ext_file(work_scene):
    version_dir_path = combine_version_comp_dir(work_scene)
    for file in os.listdir(version_dir_path):
        if ".exr" in file or ".jpeg" in file or ".jpg" in file or ".dpx" in file:
            return True
    return False

def check_valid_size(work_scene):
    version_dir_path = combine_version_comp_dir(work_scene)
    exrs = sorted([f for f in os.listdir(version_dir_path) if f.endswith('.exr')])
    first_file = '{}/{}'.format(version_dir_path, exrs[0])
    imgHeight, imgWidth, imgChannels = get_media_dimension(first_file)
    img_sizes = (imgWidth, imgHeight)
    if imgHeight % 2 == 0 and imgWidth % 2 == 0:
        return True, img_sizes
    return False, img_sizes
