# v.0.0.1 polytag switcher
_title = 'Riff Model Smooth Data'
_version = 'v.0.0.1'
_des = 'wip'
uiName = 'RFModelSmoothUI'

#Import python modules
import sys
import os
import getpass
import logging
from collections import OrderedDict
import maya.app.renderSetup.model.renderSetup as renderSetup
import json
import pymel.core as pm
import traceback

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

# import config
import rf_config as config

if config.isMaya:
    import maya.cmds as mc
    import maya.mel as mm

# framework modules
from rf_utils import log_utils
from rf_utils import file_utils
from rf_utils.context import context_info
from rftool.render.redshift import rs_utils
from rf_utils.tech_script import copy_mat
from rftool.utils import maya_utils
from rf_maya.rftool.shade import shade_utils
reload(copy_mat)
reload(context_info)
reload(rs_utils)
reload(shade_utils)

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

# import ui
# thread

class Ui(QtWidgets.QWidget):
    def __init__(self, parent=None) :
        super(Ui, self).__init__(parent)

        self.allLayout = QtWidgets.QVBoxLayout()
        self.label = QtWidgets.QLabel('Lookdev Tools')
        self.allLayout.addWidget(self.label)

        self.subLayout = QtWidgets.QHBoxLayout()
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.resLabel = QtWidgets.QLabel('Res')
        self.res_comboBox = QtWidgets.QComboBox()

        self.subLayout.addItem(spacerItem1)
        self.subLayout.addWidget(self.resLabel)
        self.subLayout.addWidget(self.res_comboBox)
        self.allLayout.addLayout(self.subLayout)

        self.input_pushButton = QtWidgets.QPushButton('Show Inputs')
        self.input_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        self.allLayout.addWidget(self.input_pushButton)

        self.layer_pushButton = QtWidgets.QPushButton('Setup layers')
        self.layer_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        self.allLayout.addWidget(self.layer_pushButton)

        self.smooth_pushButton = QtWidgets.QPushButton('Set smooth objects')
        self.smooth_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        self.allLayout.addWidget(self.smooth_pushButton)

        self.matte_pushButton = QtWidgets.QPushButton('MatteID Tool')
        self.matte_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        self.allLayout.addWidget(self.matte_pushButton)

        self.proxy_pushButton = QtWidgets.QPushButton('Create Proxy')
        self.proxy_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        self.allLayout.addWidget(self.proxy_pushButton)

        self.setPrefix_pushButton = QtWidgets.QPushButton("Set Prefix")
        self.setPrefix_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        self.allLayout.addWidget(self.setPrefix_pushButton)

        self.exportPreset_pushButton = QtWidgets.QPushButton("Export Preset")
        self.exportPreset_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        self.allLayout.addWidget(self.exportPreset_pushButton)

        self.importPreset_pushButton = QtWidgets.QPushButton("Import Preset")
        self.importPreset_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        self.allLayout.addWidget(self.importPreset_pushButton)

        self.createVtxNormal_pushButton = QtWidgets.QPushButton("Create Vertex Normal")
        self.createVtxNormal_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        self.allLayout.addWidget(self.createVtxNormal_pushButton)

        self.createGlassRampShd_pushButton = QtWidgets.QPushButton("Create Glass Ramp Shd")
        self.createGlassRampShd_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        self.allLayout.addWidget(self.createGlassRampShd_pushButton)

        self.createGlassEmissionShd_pushButton = QtWidgets.QPushButton("Create Glass Emission")
        self.createGlassEmissionShd_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        self.allLayout.addWidget(self.createGlassEmissionShd_pushButton)

        self.createRsSwitch_pushButton = QtWidgets.QPushButton("Create Rs Switch")
        self.createRsSwitch_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        self.allLayout.addWidget(self.createRsSwitch_pushButton)

        self.replaceRig_pushButton = QtWidgets.QPushButton('Replace Cache with Rig')
        self.replaceRig_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        self.allLayout.addWidget(self.replaceRig_pushButton)



        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.allLayout.addItem(spacerItem)

        self.allLayout.setSpacing(8)
        self.allLayout.setContentsMargins(2, 2, 2, 2)

        self.allLayout.setStretch(0, 0)
        self.allLayout.setStretch(1, 0)
        self.allLayout.setStretch(2, 0)
        self.allLayout.setStretch(3, 0)
        self.allLayout.setStretch(4, 2)

        self.setLayout(self.allLayout)


class LookdevTool(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        #Setup Window
        super(LookdevTool, self).__init__(parent)

        # ui read
        self.ui = Ui(parent)
        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)

        # title
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.resize(200, 200)   

        self.init_functions()
        self.init_signals()

    def init_functions(self):
        self.set_res()
        self.asset = self.set_entity()
        # copy_mat.copy_texture_ldv_to_mdl(self.asset.name)

    def set_res(self):
        self.ui.res_comboBox.addItems(['md'])

    def set_entity(self):
        scene = mc.file(q=True, sn=True)
        loc = mc.file(q=True, loc=True)
        curScene = scene if scene else loc if not loc == 'unknown' else None
        message = 'Ready' if curScene else 'Scene is not in the pipeline'
        self.statusBar().showMessage(message)
        self.asset = context_info.ContextPathInfo() if curScene else None
        return self.asset

    def init_signals(self):
        self.ui.smooth_pushButton.clicked.connect(self.set_smooth)
        self.ui.layer_pushButton.clicked.connect(self.set_layers)
        self.ui.proxy_pushButton.clicked.connect(self.create_proxy)
        self.ui.matte_pushButton.clicked.connect(self.matte_tool)
        self.ui.input_pushButton.clicked.connect(self.run_auto_build)
        self.ui.setPrefix_pushButton.clicked.connect(self.set_prefix)
        self.ui.importPreset_pushButton.clicked.connect(self.importPreset)
        self.ui.exportPreset_pushButton.clicked.connect(self.exportPreset)
        self.ui.createVtxNormal_pushButton.clicked.connect(self.vtxNormal)
        self.ui.createGlassRampShd_pushButton.clicked.connect(self.rampShd)
        self.ui.createGlassEmissionShd_pushButton.clicked.connect(self.createGlassShader)
        self.ui.createRsSwitch_pushButton.clicked.connect(self.rsSwitch)
        self.ui.replaceRig_pushButton.clicked.connect(self.replaceRig)


    def undo(function):
        def funcCall(*args, **kwargs):
            result = None

            try:
                mc.undoInfo(openChunk=True, chunkName = function.__name__)
                print "This"
                result = function(*args, **kwargs)
            except Exception as e:
                print traceback.format_exc()
                pm.displayError("## Error, see script editor: %s" %e)
            finally:
                mc.undoInfo(closeChunk=True)
                
            return result
        return funcCall


    def set_smooth(self):
        # hard coded
        if self.asset:
            data = self.smooth_data_file(self.asset)
            polyList = file_utils.ymlLoader(data)
            ns = self.guess_namespace()
            polyNs = ['%s:%s' % (ns, a) for a in polyList]
            polyExists = [a for a in polyNs if mc.objExists(a)]
            missingPoly = [a for a in polyNs if not mc.objExists(a)]
            self.add_set(polyExists)

            if missingPoly:
                logger.info('%s polys missing' % len(missingPoly))
                logger.info(missingPoly)

    def set_layers(self): 
        from rf_maya.lib import material_layer 
        reload(material_layer)
        from rf_utils.sg import sg_process
        
        self.asset.context.use_sg(sg_process.sg, project=self.asset.project, entityType=self.asset.entity_type, entityName=self.asset.name)
        tasks = sg_process.list_looks(self.asset.context.sgEntity)
        looks = [a.get('sg_name') for a in tasks]
        
        for look in looks: 
            material_layer.create_layer(look, mode=self.asset.step)

    def add_set(self, plys):
        """ sync by delete existing node """
        nodeName = '%s_meshParameter' % self.asset.name
        mc.delete(nodeName) if mc.objExists(nodeName) else None
        rs_utils.add_smooth_ply(nodeName, plys)


    def guess_namespace(self):
        geoGrp = self.asset.projectInfo.asset.geo_grp()
        guessExport = mc.ls('*:%s' % geoGrp)
        exportGrp = guessExport[0] if guessExport else ''
        if ':' in exportGrp:
            namespace = exportGrp.split(':')[0]
            return namespace

    def smooth_data_file(self, entity):
        asset = entity.copy()
        asset.context.update(step='model', process='main', res=str(self.ui.res_comboBox.currentText()))
        dataDir = asset.path.output_hero().abs_path()
        smoothData = asset.output_name(outputKey='smoothData', hero=True, ext='.yml')
        data = '%s/%s' % (dataDir, smoothData)
        return data

    def rs_file(self, entity):
        # output
        outputDir = entity.path.output_hero().abs_path()
        outputRs = entity.output_name(outputKey='rsproxy', hero=True, ext='.rs')
        rsfile = '%s/%s' % (outputDir, outputRs)
        return rsfile

    def create_proxy(self):
        if self.asset:
            self.asset.context.update(look=self.asset.process, res=str(self.ui.res_comboBox.currentText()))
            geoGrp = self.asset.projectInfo.asset.geo_grp()
            guessExport = mc.ls('*:%s' % geoGrp)
            exportGrp = guessExport[0] if guessExport else None

            rsfile = self.rs_file(self.asset)

            rs_utils.export_rsProxy(rsfile, exportGrp, connectivity=0, animation=0, start=0, end=0)
            self.statusBar().showMessage('Export rs file success')

            if self.check_rig_grp() and os.path.exists(rsfile):
                note, shape, transform = rs_utils.create_proxy(nodeName=self.asset.name, proxyPath=rsfile, displayMode=0, displayPercentage=100, force=True)
                rigGrp = self.rig_grp(transform)
                self.statusBar().showMessage('Create placeholder success')

            else:
                rsGrp = self.asset.projectInfo.asset.rs_grp()
                logger.warning('%s exists' % rsGrp)
                self.statusBar().showMessage('%s exists' % rsGrp)


    def run_auto_build(self): 
        from rf_utils.widget import auto_build_widget
        reload(auto_build_widget)
        auto_build_widget.show()


    def matte_tool(self): 
        from rf_app.lookdev.matte_tool import app
        reload(app)
        app.show()

    def rig_grp(self, obj):
        rsGrp = self.asset.projectInfo.asset.rs_grp()
        geoGrp = mc.group(obj, n='Geo_Grp')
        return mc.group(geoGrp, n=rsGrp)

    def check_rig_grp(self):
        rsGrp = self.asset.projectInfo.asset.rs_grp()
        checks = [rsGrp, 'Geo_Grp']
        return all(not mc.objExists(a) for a in checks)

    def set_prefix(self):
        scene = context_info.ContextPathInfo(path=mc.file(q=True, sn=True))

        project = scene.project
        assetType = scene.type
        assetName = scene.name
        ocio = ["Bikey"]
        colorSpace = "_acescg" if project in ocio else ""

        prefix = "{}/{}/pass/<Camera>/v001/<RenderLayer>/<RenderLayer>{}".format(assetType, assetName, colorSpace)
        mc.setAttr("defaultRenderGlobals.imageFilePrefix", prefix, type="string")

        workspaceImagePath = "P:/{}/rnd/lookdev/image".format(project)
        mc.workspace(fileRule=['images', workspaceImagePath])

    def exportPreset(self):
        self.projectName = os.environ.get("ACTIVE_PROJECT", None)
        if not self.projectName:
            return

        fp = mc.file(q=True, loc=True)

        d = {"lookdev": "LookDev_Template", "Light": "Light_Template"}
        self.ws = fp.split("/")[3]
        self.root_preset = "P:/{}/rnd/{}/{}/preset".format(self.projectName, self.ws, d[self.ws])

        # create preset folder if not exists #
        if not self.root_preset in fp:
            mc.warning("The current path is not in {}. Skipped".format(self.root_preset))
            return

        if not os.path.isdir(self.root_preset):
            os.makedirs(self.root_preset)

        msg = QtWidgets.QMessageBox()
        msg.setWindowTitle("Export?")
        msg.setText("Do you want to export to preset?")
        msg.setStandardButtons(msg.Yes | msg.No)
        ret = msg.exec_()

        if ret == msg.Yes:
            result = False

            
            p = "/".join(fp.split("/")[0:-1])
            name = fp.split("/")[-1].split(".")[0]
            
            rs_jsonPath = fp.replace(".ma", ".json")
            shaderDataPath = p + "/" + name + "_shaderData.json"
            shaderOvrPath = p + "/" + name + "_shaderOvr.json"
            
            allAovs = mc.ls(type="RedshiftAOV")
            
            # get aov connections 
            connectionsData = []
            for aov in allAovs:
                try:
                    connectionInfo = mc.listConnections("{}.defaultShader".format(aov), c=True, p=True)
                    connectionsData.append(connectionInfo)
                except:
                    pass

            with open(rs_jsonPath, "w+") as file:
                json.dump(renderSetup.instance().encode(None), fp=file, indent=2, sort_keys=True)
                
            with open(shaderDataPath, "w+") as file:
                json.dump(connectionsData, file, indent=2)

            # get shaderOverride
            d = []
            for i in mc.ls(type="shaderOverride"):
                shd = mc.listConnections("{}.attrValue".format(i), c=True, p=True)
                if shd:
                    d.append(shd)

            with open(shaderOvrPath, "w+") as file:
                json.dump(d, file, indent=2)
                result = True


            if result:
                # export done #
                msg.setWindowTitle("Done")
                msg.setText("Export Completed")
                msg.setStandardButtons(msg.Ok)
                msg.exec_()
                


    def importPreset(self):
        self.projectName = os.environ.get("ACTIVE_PROJECT", None)
        if not self.projectName:
            return


        self.root_preset = "P:/{}/rnd/lookdev/LookDev_Template/preset".format(self.projectName)
        
        presetList = [i for i in os.listdir(self.root_preset) if i.endswith("ma")]

        if not presetList:
            return mc.warning("Preset List is emty. please export preset first")

        selected_item, state = QtWidgets.QInputDialog.getItem(self, "Preset", "select preset", presetList, editable=False)

        if selected_item and state:
            fullPath = self.root_preset + "/" + selected_item
            rs_presetPath = fullPath.replace(".ma", ".json")
            shaderDataPath = self.root_preset + "/" + selected_item.replace(".ma", "_shaderData.json")

            if not os.path.isfile(fullPath) and os.path.isfile(rs_presetPath) and os.path.isfile(shaderDataPath):
                return mc.warning("Preset not found or missing some data.")

            # if lookdev is exists #
            if not mc.objExists("LookDev_Asset"):
                mc.file(fullPath, i=True, type="mayaAscii", ignoreVersion=1, mergeNamespacesOnClash=False, rpr="", options="v=0;")

            with open(rs_presetPath, "r") as file:
                renderSetup.instance().decode(json.load(file), renderSetup.DECODE_AND_OVERWRITE, None)

            # connect shader #
            with open(shaderDataPath) as f:
                connectionInfo = json.load(f)

            for des, src in connectionInfo:
                mc.connectAttr(src, des, force=True)
            '''
            # create AO shader #
            ao_aov_node = [i for i in mc.ls(type="RedshiftAOV") if "rsAov_AO" in i]
            if ao_aov_node:
                ao_aov_node = ao_aov_node[0]
            else:
                ao_aov_node = mc.rsCreateAov(type="Custom")
                mc.setAttr("{}.name".format(ao_aov_node), "AO", type="string")

            ao_shader = mc.createNode("RedshiftAmbientOcclusion")
            mc.connectAttr("{}.outColor".format(ao_shader), "{}.defaultShader.".format(ao_aov_node))'''

            msg = QtWidgets.QMessageBox()
            msg.setWindowTitle("Done")
            msg.setText("Import Completed")
            msg.setStandardButtons(msg.Ok)
            msg.exec_()

    def vtxNormal(self):
        objs = mc.ls(sl=True)
        offset = 10
        
        for obj in objs:
            bbox = mc.xform(obj, bb=True, q=True)
            
            xmin, ymin, zmin, xmax, ymax, zmax = bbox
            
            centerx = (xmin + xmax) / 2
            centery = (ymin + ymax) / 2
            centerz = (zmin + zmax) / 2
            
            if not mc.objExists("vertex_guide_geo"):
                sphereGeo = mc.polySphere(n="vertex_guide_geo")[0]
            else:
                sphereGeo = "vertex_guide_geo"
                
            mc.setAttr("{}.tx".format(sphereGeo), centerx)
            mc.setAttr("{}.ty".format(sphereGeo), centery)
            mc.setAttr("{}.tz".format(sphereGeo), centerz)
            
            sx = (abs(xmin - xmax) / 2) + offset
            sy = (abs(ymin - ymax) / 2) + offset
            sz = (abs(zmin - zmax) / 2) + offset
            
            mc.setAttr("{}.sx".format(sphereGeo), sx)
            mc.setAttr("{}.sy".format(sphereGeo), sy)
            mc.setAttr("{}.sz".format(sphereGeo), sx)
            
            mc.select(sphereGeo, obj)
            mc.transferAttributes(transferPositions=0, transferNormals=1, 
                                    transferUVs=0, transferColors=0, sampleSpace=0, 
                                    sourceUvSpace="map1", targetUvSpace="map1", 
                                    searchMethod=3, flipUVs=0, colorBorders=1)
                                    
        
            mc.select(obj)
            mm.eval("DeleteHistory;")
            
        mc.delete("vertex_guide_geo")
        
        msg = QtWidgets.QMessageBox()
        msg.setText("Done")
        msg.exec_()
    
    @undo
    def rampShd(self):
        selectedShd = mc.ls(sl=True)
        if not selectedShd:
            return mc.warning("Please select shading node!")

        for shd in selectedShd:
            if mc.nodeType(shd) != "RedshiftMaterial":
                mc.warning("The selected node ({}) is not a RedshiftMaterial. Skipped.".format(shd))
                continue
            
            self.setUpGlassShd(shd)

        print mc.file(q=True, loc=True)
        msg = QtWidgets.QMessageBox()
        msg.setText("Done")
        msg.exec_()

    @undo
    def createRampNode(self):
        rampNode = mc.shadingNode("ramp", asTexture=True)
        p2dNode = mc.shadingNode("place2dTexture", asUtility=True)
        mc.connectAttr("{}.outUV".format(p2dNode), "{}.uvCoord".format(rampNode), force=True)
        mc.connectAttr("{}.outUvFilterSize".format(p2dNode), "{}.uvFilterSize".format(rampNode), force=True)
        
        mc.setAttr("{}.rotateUV".format(p2dNode), 240)
        
        mc.setAttr("{}.interpolation".format(rampNode), 0)
        mc.setAttr("{}.vWave".format(rampNode), 1)
        mc.setAttr("{}.noise".format(rampNode), 0.1)
        
        
        mc.setAttr("{}.colorEntryList[1].position".format(rampNode), 0.269)
        mc.setAttr("{}.colorEntryList[2].position".format(rampNode), 0.560)
        
        mc.setAttr("{}.colorEntryList[0].color".format(rampNode), 0.561068, 0.559782, 0.559364, type="double3")
        mc.setAttr("{}.colorEntryList[1].color".format(rampNode), 0.998863, 0.998046, 0.997597, type="double3")
        mc.setAttr("{}.colorEntryList[2].color".format(rampNode), 0.342374, 0.341752, 0.341633, type="double3")
        
        return rampNode

    @undo
    def setUpGlassShd(self, shd):
        attrs = {"opacity_color": (1,1,1), "refl_roughness": 0, "refr_weight": 0, "refl_weight": "ramp",
                "refr_color": (0,0,0), "bump_input": None}

        for attr in attrs:
            value = attrs[attr]
            des = "{}.{}".format(shd, attr)
            p = mc.listConnections(des, p=True)
            if p:
                mc.disconnectAttr(p[0], des)
            
            if value:
                if value == "ramp":
                    ramp = self.createRampNode()
                    mc.connectAttr("{}.outAlpha".format(ramp), des, force=True)
                else:
                    pm.setAttr(des, value)

    def createGlassShader(self):
        selectedShd = pm.ls(sl=True)
        
        if not selectedShd:
            return mc.warning("Please select shading node!")
        
        selectedShd = selectedShd[0]
        
        
        if selectedShd.nodeType() != "RedshiftMaterial":
            return mc.warning("Please select a RedshiftMaterial")
                    
        
        attrs = {"refl_color": (1, 1, 1), "refl_weight": 1, "refl_roughness": 0, "refr_weight": 0, 
                "emission_weight": 2, "bump_input": None, "opacity_color": (1, 1, 1), "emission_color": None}
        
        for attr, values in attrs.iteritems():
            attrNode = pm.PyNode("{}.{}".format(selectedShd, attr))
            
            if attr == "emission_color":
                fileNode = attrNode.listConnections()
                if fileNode:
                    fileNode = fileNode[0] 
            attrNode.disconnect()
            
            if values is not None:
                attrNode.set(values)

        # import glass preset #
        presetPath = "P:/Bikey/rnd/lookdev/LookDev_Template/shade/Glass_Shd.ma"
        if not mc.objExists("__Glass_Emission_Color__"):
            mc.file(presetPath, i=True, type="mayaAscii", ignoreVersion=True, mergeNamespacesOnClash=False, rpr="Glass_Shd", options="v=0;", pr=True)
            
        emissionColorNode = pm.PyNode("__Glass_Emission_Color__")
            
        if fileNode:
            fileNode.outAlpha >> emissionColorNode.base_alpha
            fileNode.outAlpha >> emissionColorNode.layer1_alpha
            fileNode.outAlpha >> emissionColorNode.layer1_mask
            
        emissionColorNode.outColor >> selectedShd.emission_color
        
        # rename shader node to Glass_Shd
        selectedShd.rename("Glass_Shd")


    ## create rs switch ##
    @undo
    def rsSwitch(self):
        objs = pm.ls("Export_Grp", dag=True, s=True)
        sgs = []

        if not objs:
            return mc.warning("Export_Grp is empty!")

        assetType, state = QtWidgets.QInputDialog.getItem(self, "Asset Type", "select type", ["prop", "char"], editable=False)
        if not state:
            return

        rs = renderSetup.instance()
        currentLayer = rs.getVisibleRenderLayer().name()
        if currentLayer != "defaultRenderLayer":
            return mc.warning("Please Switch to MasterLayer")

        for obj in objs:
            sg = obj.shadingGroups()
            if not sg:
                continue
            sg = sg[0].name()
            if sg in sgs:
                continue    
            shdConnected = mc.listConnections("{}.surfaceShader.".format(sg))
            if not shdConnected:
                continue
            
            shdConnected = shdConnected[0]
            
            shdType = mc.nodeType(shdConnected)

            if shdType == "RedshiftShaderSwitch":
                continue
            
            if not mc.objExists("__Stylize_Shd__"):
                shader1 = mc.shadingNode("RedshiftMaterial", asShader=True, n="__Stylize_Shd__")
            else:
                shader1 = pm.PyNode("__Stylize_Shd__")
            pm.setAttr("{}.diffuse_color".format(shader1), 1.33567, 1.334577, 1.333802)
            pm.setAttr("{}.refl_weight".format(shader1), 0)
            
            if assetType == "prop":
                if not mc.objExists("texture_add_shd"):
                    mc.file("P:/Bikey/rnd/light/Light_Template/shade/texture_add.ma", i=True)
                shader2 = "texture_add_shd"

            if assetType == "char":
                if not mc.objExists("Hair_WhiteMat_SHD"):
                    mc.file("P:/Bikey/rnd/light/Light_Template/shade/Hair_WhiteMat_SHD.ma", i=True)

                shader2 = None

            yetiShaderWhiteMat = "Hair_WhiteMat_SHD"
            
            if shdType == "RedshiftSprite":
                shader0 = mc.listConnections("{}.input.".format(shdConnected))[0]
                name = shader0 + "_Switcher"
                rsSwitch = self.createRsSwitch(name, shader0, shader1, shader2)
                #mc.connectAttr("{}.outColor".format(shader0), "{}.shader0.".format(rsSwitch), force=True)
                mc.connectAttr("{}.outColor".format(rsSwitch), "{}.input.".format(shdConnected), force=True)

                # create aiUtility
                txPath = mc.getAttr("{}.tex0".format(shdConnected))
                if txPath:
                    self.createToonShd(txPath, sg)
            else:
                shader0 = shdConnected
                name = shader0 + "_Switcher"
                # check if shader0 is connected with yeti, use yetiShaderWhiteMat instead of shader1. remove shader2
                if mc.nodeType(shader0) == "RedshiftHair":
                    shader1 = yetiShaderWhiteMat
                    shader2 = None
                if assetType == "char":
                    shader2 == None
                rsSwitch = self.createRsSwitch(name, shader0, shader1, shader2)
                mc.connectAttr("{}.outColor".format(rsSwitch), "{}.surfaceShader.".format(sg), force=True)

                # arnold outline #
                if assetType == "prop":
                    if not mc.objExists("outline_shd"):
                        mc.file("P:/Bikey/rnd/light/Light_Template/shade/outline.ma", i=True)
                    outlineShd = "outline_shd"
                    mc.connectAttr("{}.outColor".format(outlineShd), "{}.aiSurfaceShader.".format(sg), force=True)

            sgs.append(sg)

        mc.select(cl=True)
        # create ai operators #
        self.createOperators()

        msg = QtWidgets.QMessageBox()
        msg.setWindowTitle("Done")
        msg.setText("Convert Completed")
        msg.setStandardButtons(msg.Ok)
        msg.exec_()

    def replaceRig(self): 
        # get rig file 
        sel = mc.ls(sl=True)
        if sel: 
            selectedObject = sel[0]
            asset = self.asset.copy()
            res = self.ui.res_comboBox.currentText()
            asset.context.update(res=res, step='model')
            heroPath = asset.path.hero().abs_path()
            heroRig = asset.output_name(outputKey='rig', hero=True, ext='.mb')
            rigFile = '{}/{}'.format(heroPath, heroRig)

            ns = selectedObject.split(':')[0]
            rnNode = mc.referenceQuery(selectedObject, referenceNode=True)
            mc.file(rigFile, loadReference=rnNode, prompt=False)
            
            # get connectionData of shdConnect
            connectionData = []
            dataFile = asset.output_name(outputKey='shdConnectData', hero=True)
            heroPath = asset.path.hero().abs_path()
            dataFile = '%s/%s' % (heroPath, dataFile)

            # get mtrMdl Path
            heroPath = asset.path.hero().abs_path()
            asset = self.asset.copy()
            asset.context.update(look ='main',res=res,step='texture')
            mtrFile = asset.output_name(outputKey='material', hero=True)
            mtrPath = '%s/%s' % (heroPath, mtrFile)

            # get Connection Info for shade_utils.apply_ref_shade()
            if os.path.isfile(dataFile):
                print 'found Connection Data assign it!'
                connectionData = file_utils.ymlLoader(dataFile)
            else:
                print 'ConnectionData not found!'
                if mc.objExists('%s.shdConnect' %geoGrp):
                    connectionStr = mc.getAttr('%s.shdConnect' %geoGrp)
                    connectionData = eval(connectionStr)

            shadeFileName = mtrPath.split('.')[0]  # get rid of .hero
            shadeKey = shadeFileName.split('_mtr_')[-1]
            connectionInfo = []

            if shadeKey in connectionData:
                connectionInfo = connectionData[shadeKey]

            # check there is connectionInfo for asset
            if connectionInfo == []:
                mc.confirmDialog(m = 'There is no ShdConnectData with texture file please conntact rig Team')

            # apply Ref shade ,
            shade_utils.apply_ref_shade(mtrPath, 'tmpMtr', targetNamespace=ns, connectionInfo=connectionInfo, reloadRef=True, debug=True)
            
            #disale UDIM
            allFileNodes = mc.ls(type="file")

            for f in allFileNodes:
                if not mc.getAttr("{}.useFrameExtension".format(f)):
                    continue
                    
                mc.setAttr("{}.uvTilingMode".format(f), 0)
            
            #import mtr shader , remove tmp namespace            
            mc.file(mtrPath,ir=1)
            mc.namespace(mergeNamespaceWithParent =1,removeNamespace = 'tmpMtr')
            


    def createToonShd(self, txPath, sg):
        fileNode = self.createFileTexture("file", "filep2d")
        
        pm.setAttr("{}.fileTextureName".format(fileNode), txPath, type="string")
        pm.setAttr("{}.alphaIsLuminance".format(fileNode), 1)

        aiSurface = mc.shadingNode("aiStandardSurface", asShader=True)
        pm.setAttr("{}.base".format(aiSurface), 1)
        pm.setAttr("{}.thinWalled".format(aiSurface), 1)

        aiToon = mc.shadingNode("aiToon", asShader=True)
        pm.setAttr("{}.edgeColor".format(aiToon), (1, 1, 1))

        mc.connectAttr("{}.outColor".format(aiToon), "{}.baseColor.".format(aiSurface), force=True)
        mc.connectAttr("{}.outColor".format(fileNode), "{}.opacity.".format(aiSurface), force=True)
        mc.connectAttr("{}.outColor".format(aiSurface), "{}.aiSurfaceShader.".format(sg), force=True)

                
    # create switch shader #
    def createRsSwitch(self, name, shader0, shader1, shader2=""):
        rsSwitch = mc.shadingNode("RedshiftShaderSwitch", asShader=True, n=name)
        mc.connectAttr("{}.outColor".format(shader0), "{}.shader0".format(rsSwitch), force=True)
        mc.connectAttr("{}.outColor".format(shader1), "{}.shader1".format(rsSwitch), force=True)
        if shader2:
            mc.connectAttr("{}.outColor".format(shader2), "{}.shader2".format(rsSwitch), force=True)
        
        return rsSwitch

    def createFileTexture(self, fileTextureName, p2dName):
        tex = pm.shadingNode('file', name=fileTextureName, asTexture=True, isColorManaged=True)
        if not pm.objExists(p2dName):
            pm.shadingNode('place2dTexture', name=p2dName, asUtility=True)
        p2d = pm.PyNode(p2dName)
        tex.filterType.set(0)
        pm.connectAttr(p2d.outUV, tex.uvCoord)
        pm.connectAttr(p2d.outUvFilterSize, tex.uvFilterSize)
        pm.connectAttr(p2d.vertexCameraOne, tex.vertexCameraOne)
        pm.connectAttr(p2d.vertexUvOne, tex.vertexUvOne)
        pm.connectAttr(p2d.vertexUvThree, tex.vertexUvThree)
        pm.connectAttr(p2d.vertexUvTwo, tex.vertexUvTwo)
        pm.connectAttr(p2d.coverage, tex.coverage)
        pm.connectAttr(p2d.mirrorU, tex.mirrorU)
        pm.connectAttr(p2d.mirrorV, tex.mirrorV)
        pm.connectAttr(p2d.noiseUV, tex.noiseUV)
        pm.connectAttr(p2d.offset, tex.offset)
        pm.connectAttr(p2d.repeatUV, tex.repeatUV)
        pm.connectAttr(p2d.rotateFrame, tex.rotateFrame)
        pm.connectAttr(p2d.rotateUV, tex.rotateUV)
        pm.connectAttr(p2d.stagger, tex.stagger)
        pm.connectAttr(p2d.translateFrame, tex.translateFrame)
        pm.connectAttr(p2d.wrapU, tex.wrapU)
        pm.connectAttr(p2d.wrapV, tex.wrapV)
        return tex

    # ai operator #
    def findSg(self):
        root = "|Export_Grp"
        exportGrp = mc.ls(root,dag=True, s=True)
        
        sgList = {}
        for obj in exportGrp:
            sgs = mc.listConnections(obj, type="shadingEngine")
            if sgs:

                for sg in sgs:
                    if sg == "intitialShadingGroup":
                        continue
                    if sg not in sgList:
                        obj = mc.sets(sg, q=True)
                        sgList[sg] = obj
                        
        return sgList, root

    def createOperators(self):
        if mc.objExists("operatorSet"):
            n = mc.listConnections("operatorSet")
            mc.delete("operatorSet")
            if n:
                mc.delete(n)
            
        aiSets = mc.sets(n="operatorSet")
        
        sgList = self.findSg()[0]
        aiSetNode = []
        addToSets = []
        for sg in sgList:
            aiSurface = mc.listConnections("{}.aiSurfaceShader".format(sg))
            if not aiSurface:
                continue
                
            aiSurface = aiSurface[0]
            
            items = sgList[sg]
            s = ""
            c = len(items)
            for i, j in enumerate(items):
                pyNode = pm.PyNode(j)
                if pyNode.nodeType() == "mesh":
                    nn = pm.PyNode(j).name().split(".f[")[0]
                    name = pm.PyNode(nn).getTransform().name().split(":")[-1]
                else:
                    name = pm.PyNode(j).getTransform().name().split(":")[-1]
                
                s += "*/{}/*".format(name)
                if i < c-1:
                    s += " or "
            
            prefixCol = mc.file(q=True, loc=True).split("/")[-1].split("_")[0]
            aiCollectionNode = mc.shadingNode("aiCollection", asUtility=True)
            selectionColName = prefixCol + "_" + sg
            mc.setAttr("{}.selection".format(aiCollectionNode), s, type="string")
            mc.setAttr("{}.collection".format(aiCollectionNode), selectionColName, type="string")
            mc.sets(aiCollectionNode, add=aiSets)
            
            col = "#{}".format(selectionColName)
            aiSetparameterNode = mc.shadingNode("aiSetParameter", asUtility=True)
            mc.setAttr("{}.selection".format(aiSetparameterNode), col, type="string")
            
            mc.connectAttr("{}.out".format(aiCollectionNode), "{}.inputs[0]".format(aiSetparameterNode), force=True)
            
            pm.PyNode(aiSetparameterNode).setAttr("assignment[0]", ["shader='{}'".format(aiSurface)])
            aiSetNode.append(aiSetparameterNode)
            mc.sets(aiSetparameterNode, add=aiSets)


        aiMergeNode = mc.shadingNode("aiMerge", asUtility=True)
        
        for i, aiSet in enumerate(aiSetNode):
            pm.connectAttr("{}.out".format(aiSet), "{}.inputs[{}]".format(aiMergeNode, i), force=True)
            
        
        mc.sets(aiMergeNode, add=aiSets)


def show():
    if config.isMaya:
        from rftool.utils.ui import maya_win
        maya_win.deleteUI(uiName)
        myApp = LookdevTool(maya_win.getMayaWindow())
        myApp.show()
        return myApp