_title = 'Riff matte tool'
_version = 'v.0.0.1'
_des = 'wip'
uiName = 'MatteToolUI'

import sys
import os
import getpass
import logging
import json
from collections import OrderedDict

core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)

# import config
import rf_config as config

from rf_utils.context import context_info
from rf_utils import log_utils, icon
from rf_utils.ui import stylesheet

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import QtWidgets, QtCore, QtGui

from view import matte_tool_view
from model.material_tool import MaterialTool
from rf_utils.widget import list_material_widget

import maya.cmds as mc
import maya.mel as mm


class MatterialToolCtrl(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super(MatterialToolCtrl, self).__init__(parent)
        self.ui = matte_tool_view.MatteToolUI(parent)
        self.ui.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.ui.setObjectName(uiName)
        self.setCentralWidget(self.ui)
        self.resize(1400, 800)
        self.sourceMel()
        self.init_state()
        self.init_signal()

    def sourceMel(self):
        pluginsPath = os.environ.get('REDSHIFT_SCRIPT_PATH')
        melFile = 'redshiftCreateAovTab.mel'
        sourcePath = os.path.join(pluginsPath, melFile).replace('\\', '/')
        mm.eval('source "%s"'%sourcePath)

    def init_state(self):
        if os.path.isfile(mc.file(q=True, sn=True)):
            context = context_info.ContextPathInfo()
            self.matteDataPath = ""
            self.material_model = MaterialTool(context)
            self.object_id = ''
            self.list_material = []
            self.list_puzzleMatte = {}
            self.list_rgb_id = []
            self.mat_clear = []
            self.set_lookdev_matte_data()
            self.set_asset_layout()
            self.set_list_material()
            if self.material_model.dont_have_object_id():
                self.material_model.create_node("%s_ObjectID"% self.object_id['code'], mode=1)
                self.material_model.set_rgb_id("%s_ObjectID"% self.object_id['code'], int(self.object_id['sg_object_id']), 0, 0)
            self.set_node()
            self.set_mat_layout()
            self.mat_clear = self.material_model.filter_material_id(self.list_material, self.list_rgb_id)
            self.chk_list_material(self.list_material)
            self.max_puzzle_id = self.object_id['sg_object_id'] + 24

    def init_signal(self):
        self.ui.puzzle_matte_listWidget.currentItemChanged.connect(self.select_puzzle_matte)
        self.ui.list_material_widget.currentItemChanged.connect(self.select_material)
        self.ui.puzzle_matte_listWidget.dropped.connect(self.dropped_node)
        self.ui.puzzle_matte_listWidget.right_click.connect(self.select_clear_matte_item)
        self.ui.add_node_pushButton.clicked.connect(self.add_node)
        self.ui.export_mat_pushButton.clicked.connect(self.export_file)

    def select_material(self):
        if self.ui.list_material_widget:
            material_data = self.ui.list_material_widget.currentItem().data(QtCore.Qt.UserRole)
            material_name = material_data['material_name']
            shad_engine = self.material_model.get_shad_engine_from_mat(material_name)
            mesh = self.material_model.get_geo_shader_assign(shad_engine)
            self.material_model.select_obj(mesh)

    def select_puzzle_matte(self):
        if self.ui.puzzle_matte_listWidget.currentItem():
            currentItem_data = self.ui.puzzle_matte_listWidget.currentItem().data(QtCore.Qt.UserRole)
            mat_in_puz_red = currentItem_data['puzzle_red_mat']
            mat_in_puz_green = currentItem_data['puzzle_green_mat']
            mat_in_puz_blue = currentItem_data['puzzle_blue_mat']
            list_sel = []
            for materials in [mat_in_puz_red, mat_in_puz_green, mat_in_puz_blue]:
                if materials != '':
                    if type(materials) == list :
                        for mat_obj in materials:
                            shad_engine = self.material_model.get_shad_engine_from_mat(mat_obj)
                            mesh = self.material_model.get_geo_shader_assign(shad_engine)
                            for obj in mesh:
                                list_sel.append(obj)

                    else:
                        shad_engine = self.material_model.get_shad_engine_from_mat(mat)
                        mesh = self.material_model.get_geo_shader_assign(shad_engine)
                        for obj in mesh:
                            list_sel.append(obj)
            self.material_model.select_obj(list_sel)

    def chk_list_material(self, list_mat):
        for matterial in self.list_material:
            if matterial['material_name'] in self.mat_clear:
                matterial['material_id'] = 0

    def select_clear_matte_item(self, puzzle_item, key_puzz_matte):
        node_data = puzzle_item.data(QtCore.Qt.UserRole)
        if key_puzz_matte == 'remove_node':
            rgb_puzzle_keys = ['puzzle_red_mat', 'puzzle_green_mat', 'puzzle_blue_mat']
            self.list_rgb_id.remove(node_data['puzzle_red_id'])
            self.list_rgb_id.remove(node_data['puzzle_green_id'])
            self.list_rgb_id.remove(node_data['puzzle_blue_id'])
            for puzzle_key in rgb_puzzle_keys:
                self.clear_matte_item(puzzle_item, puzzle_key)
            self.remove_matte_item(puzzle_item)
            self.set_node()
        else:
            self.clear_matte_item(puzzle_item, key_puzz_matte)

    def remove_matte_item(self, puzzle_item):
        node_data = puzzle_item.data(QtCore.Qt.UserRole)
        aov_name = node_data.get('puzzle_name')
        self.material_model.remove_node(aov_name)

    def clear_matte_item(self, puzzle_item, key_puzz_matte):
        node_data = puzzle_item.data(QtCore.Qt.UserRole)
        materials_name = node_data.get(key_puzz_matte)
        for material_name in materials_name:
            if material_name:
                shading_engine = self.material_model.get_shad_engine_from_mat(material_name)
                del self.list_puzzleMatte[material_name]
                self.material_model.set_material_id_by_name(shading_engine, 0)
                self.clear_node_matte(puzzle_item, node_data, key_puzz_matte)
                self.ui.list_material_widget.clear()
                self.set_mat_layout()
                self.update_matterial_list(shading_engine, 0, None)

    def clear_node_matte(self, puzzle_item, node_data, key_puzz_matte):
        customWidget = matte_tool_view.puzzleMatte_CustomWidget()
        node_data.update({
            key_puzz_matte: ''
        })
        customWidget.set_text_puzzle_name(node_data['puzzle_name'])
        customWidget.set_text_puzR_id(node_data['puzzle_red_id'])
        customWidget.set_text_puzG_id(node_data['puzzle_green_id'])
        customWidget.set_text_puzB_id(node_data['puzzle_blue_id'])
        customWidget.set_text_puzR_mat(node_data['puzzle_red_mat'])
        customWidget.set_text_puzG_mat(node_data['puzzle_green_mat'])
        customWidget.set_text_puzB_mat(node_data['puzzle_blue_mat'])

        puzzle_item.setData(QtCore.Qt.UserRole, node_data)

        self.ui.puzzle_matte_listWidget.editItem(puzzle_item)
        self.ui.puzzle_matte_listWidget.setItemWidget(puzzle_item, customWidget)

    def dropped_node(self, curr_pos, node, material):
        puzz_input, rgb_id_key = self.chk_assign_box()
        node_data = node.data(QtCore.Qt.UserRole)
        material_data = material.data(QtCore.Qt.UserRole)
        puzz_mat = self.material_model.droped_update_node(
            node_data,
            material_data,
            self.list_puzzleMatte
        )

        customWidget = matte_tool_view.puzzleMatte_CustomWidget()
        mat_list_widget = list_material_widget.ListMaterialWidget()

        if puzz_mat == 'valid':
            node_data[puzz_input].append(material_data['material_name'])
            customWidget.set_text_puzzle_name(node_data['puzzle_name'])
            customWidget.set_text_puzR_id(node_data['puzzle_red_id'])
            customWidget.set_text_puzG_id(node_data['puzzle_green_id'])
            customWidget.set_text_puzB_id(node_data['puzzle_blue_id'])
            customWidget.set_text_puzR_mat(node_data['puzzle_red_mat'])
            customWidget.set_text_puzG_mat(node_data['puzzle_green_mat'])
            customWidget.set_text_puzB_mat(node_data['puzzle_blue_mat'])
            self.update_rgb_node(node_data, rgb_id_key, customWidget, node, material)
            self.update_matterial_list(material_data['shader_engine'], node_data[rgb_id_key], node_data)
            self.update_object_assign(mat_list_widget, node, material)

        elif puzz_mat == 'duplicate':
            self.show_warning_box('Duplicate material in puzzle')
            return
        elif puzz_mat == 'object_id':
            self.show_warning_box('Cannot drop in ObjectID')
            return

    def chk_assign_box(self):
        self.dialog_message = matte_tool_view.RGBSelectDialog()
        if self.dialog_message.exec_():
            return self.dialog_message.get_value()

    def add_node(self):
        last_rgb_id = self.material_model.get_last_rgb_id(self.list_rgb_id)
        text, ok = QtWidgets.QInputDialog.getText(self.ui, 'Text Input Dialog', 'Puzzle Matte:')
        if ok and text != "":
            if last_rgb_id <= self.max_puzzle_id:
                chk_node_create = self.material_model.create_node(text)
                if chk_node_create:
                    self.ui.puzzle_matte_listWidget.clear()
                    self.material_model.set_rgb_id(
                        text,
                        last_rgb_id,
                        last_rgb_id + 1,
                        last_rgb_id + 2
                    )
                    self.set_node()
                else:
                    self.show_warning_box('Name Error')
            else:
                self.show_warning_box('PuzzleMatte Over')

    def show_warning_box(self, message):
        msg_box = QtWidgets.QMessageBox()
        msg_box.setIcon(QtWidgets.QMessageBox.Warning)
        msg_box.setText(message)
        msg_box.setWindowTitle("MessageBox")
        msg_box.exec_()

    def set_asset_layout(self):
        self.material_model.get_object_id()
        asset_name = self.material_model.get_asset_name()
        self.object_id = self.material_model.get_object_id()
        self.ui.asset_label.setText(asset_name)
        self.ui.objectID_label.setText(str(self.object_id['sg_object_id']))

    def set_list_material(self):
        list_shader_engine = self.material_model.fetch_material()
        print list_shader_engine
        for shader_engine in list_shader_engine:
            #self.ui.add_item_material(material_name, "object_assign")
            print shader_engine
            material_name = self.material_model.get_material_name(shader_engine)
            if material_name:
                material_name = material_name[0]
                material_id = self.material_model.get_material_id(shader_engine)
                mat_dat_list = {
                    'material_id': material_id,
                    'material_name': material_name,
                    'shader_engine': shader_engine
                }
                self.list_material.append(mat_dat_list)

    def set_puzzleMat_layout(self):
        #1. create puzzleMatte
        #2. get data from puzzlematte
        #3. create listwidgetitem and add puzzlematte data(name, objRed, objGreen, objBlue)
        pass

    def set_lookdev_matte_data(self):
        asset = context_info.ContextPathInfo()
        asset.context.update(look=asset.process, res='md')
        path = asset.path.hero().abs_path()
        name = asset.output_name(outputKey='matteData', hero=True)
        self.matteDataPath = '%s/%s' % (path, name)

    def set_node(self):
        nodes = self.material_model.fetch_aov_type()
        self.ui.puzzle_matte_listWidget.clear()
        self.list_rgb_id = []
        for node in nodes:
            red_id, green_id, blue_id = self.material_model.get_rgb_id(node)
            red_mat, green_mat, blue_mat = self.material_model.get_puzz_mat(
                self.list_material,
                red_id,
                green_id,
                blue_id
            )
            self.ui.add_item_puzzle(node, red_id, green_id, blue_id, red_mat, green_mat, blue_mat)
            self.list_rgb_id.append(red_id)
            self.list_rgb_id.append(green_id)
            self.list_rgb_id.append(blue_id)
            if len(red_mat) != 0:
                self.assign_list_puzzlematte(red_mat, node)
            if len(green_mat) != 0:
                self.assign_list_puzzlematte(green_mat, node)
            if len(blue_mat) != 0:
                self.assign_list_puzzlematte(blue_mat, node)

    def assign_list_puzzlematte(self, rgb_matt, node):
        for matt in rgb_matt:
            self.list_puzzleMatte[matt] = node

    def set_mat_layout(self):
        for mat_data in self.list_material:
            material_display = self.material_model.get_material_name(mat_data['shader_engine'])
            if mat_data['material_name'] in self.list_puzzleMatte:
                self.ui.add_item_material(mat_data['shader_engine'], material_display[0], self.list_puzzleMatte[mat_data['material_name']])
            else:
                self.ui.add_item_material(mat_data['shader_engine'], material_display[0], "object_assign")

    def update_rgb_node(
            self,
            updated_node_data,
            rgb_id_key,
            customWidget,
            current_node,
            dragged_material
    ):
        customWidget.set_icon(icon.maya)
        node_data = current_node.data(QtCore.Qt.UserRole)
        dragged_material_data = dragged_material.data(QtCore.Qt.UserRole)
        node_data.update(updated_node_data)
        current_node.setData(QtCore.Qt.UserRole, node_data)

        self.ui.puzzle_matte_listWidget.editItem(current_node)
        self.ui.puzzle_matte_listWidget.setItemWidget(current_node, customWidget)

        self.material_model.set_material_id(dragged_material_data, node_data[rgb_id_key])

    def update_object_assign(self, mat_list_widget, current_node, dragged_material):
        node_data = current_node.data(QtCore.Qt.UserRole)
        dragged_material_data = dragged_material.data(QtCore.Qt.UserRole)

        mat_list_widget.set_text_material_name(dragged_material_data['material_name'])
        mat_list_widget.set_text_obj_assign(node_data['puzzle_name'])
        mat_list_widget.set_icon(icon.maya)

        self.ui.list_material_widget.editItem(dragged_material)
        self.ui.list_material_widget.setItemWidget(dragged_material, mat_list_widget)

    def update_matterial_list(self, shader_engine, matterial_id, node_data):
        for index, matterial in enumerate(self.list_material):
            if matterial['shader_engine'] == shader_engine:
                matterial['material_id'] = matterial_id
                if node_data:
                    mat_name = self.material_model.get_material_name(shader_engine)
                    self.list_puzzleMatte[mat_name[0]] = node_data['puzzle_name']

    def export_file(self):
        export_data = []
        nodes = self.material_model.fetch_aov_type()
        asset = context_info.ContextPathInfo()
        for node in nodes:
            red_id, green_id, blue_id = self.material_model.get_rgb_id(node)
            red_mat, green_mat, blue_mat = self.material_model.get_puzz_mat(
                self.list_material,
                red_id,
                green_id,
                blue_id
            )
            data = OrderedDict()
            data['node_name'] = '%s_%s'%(asset.name ,node)
            print '%s_%s'%(asset.name ,node)
            data['puzz_red_mat'] = red_mat
            data['red_id'] = red_id
            data['puzz_green_mat'] = green_mat
            data['green_id'] = green_id
            data['puzz_blue_mat'] = blue_mat
            data['blue_id'] = blue_id
            export_data.append(data)

        with open(self.matteDataPath, 'w') as outfile:
            json.dump(export_data, outfile)
        self.publish_asset()

    def publish_asset(self):
        from rf_app.publish.asset import asset_app
        reload(asset_app)
        app = asset_app.show()

    def publish_warning_box(self):
        pass


   

def show():
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        maya_win.deleteUI(uiName)
        myApp = MatterialToolCtrl(maya_win.getMayaWindow())
        myApp.show()
        return myApp
    else:
        logger.info('Run in MatterialToolCtrl\n')
        app = QtWidgets.QApplication.instance()
        if not app:
            app = QtWidgets.QApplication(sys.argv)
        myApp = MatterialToolCtrl()
        myApp.show()
        stylesheet.set_default(app)
        sys.exit(app.exec_())


if __name__ == '__main__':
    show()
