# -*- coding: utf-8 -*-
import os
import sys

from Qt import QtCore, QtWidgets, QtGui

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_utils import icon
from rf_utils.widget import list_material_widget


class MatteToolUI(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(MatteToolUI, self).__init__(parent=parent)

        self.allLayout = QtWidgets.QVBoxLayout()
        self.mainLayout = QtWidgets.QGridLayout()
        self.asset_layout = QtWidgets.QVBoxLayout()
        self.asset_widget_layout = QtWidgets.QVBoxLayout()
        self.asset_label = QtWidgets.QLabel('lily')
        self.objectID_label = QtWidgets.QLabel('1000')

        self.list_material_widget = list_material_widget.MaterialListWidget(self)

        self.material_layout = QtWidgets.QVBoxLayout()

        self.puzzle_matte_layout = QtWidgets.QVBoxLayout()
        self.puzzle_matte_listWidget = NodeListWidget(self)

        self.pushButton_layout = QtWidgets.QVBoxLayout()
        self.add_node_pushButton = QtWidgets.QPushButton('Add +')
        self.export_mat_pushButton = QtWidgets.QPushButton('Export')
        self.init_ui()

    def init_ui(self):
        self.asset_layout.addWidget(self.asset_label)
        self.asset_layout.addWidget(self.objectID_label)
        self.material_layout.addWidget(self.list_material_widget)
        self.asset_widget_layout.addLayout(self.asset_layout)


        self.puzzle_matte_layout.addWidget(self.puzzle_matte_listWidget)
        # self.add_item_puzzle('body_matte','red', 'green', 'blue', 'data')
        # self.add_item_puzzle('head_matte', 'red', 'green', 'blue', 'data')
        # self.add_item_material('lambert555', 'body')

        self.pushButton_layout.addWidget(self.add_node_pushButton)
        self.pushButton_layout.addWidget(self.export_mat_pushButton)

        self.mainLayout.addLayout(self.asset_widget_layout, 0, 0)
        self.mainLayout.addLayout(self.material_layout, 0, 1)
        self.mainLayout.addLayout(self.puzzle_matte_layout, 0, 2)
        self.mainLayout.addLayout(self.pushButton_layout, 1, 2)

        # self.mainLayout.addStretch(1)
        self.allLayout.addLayout(self.mainLayout)
        self.setLayout(self.allLayout)
        verticalSpacer = QtWidgets.QSpacerItem(20, 80, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.asset_layout.addItem(verticalSpacer)
        # self.mainLayout.setStretch(0, 1)
        # self.mainLayout.setStretch(1, 2)
        # self.mainLayout.setStretch(2, 2)
        self.resize(800, 600)

    def add_item_puzzle(
        self,
        puzzle_name,
        puzzle_red_id,
        puzzle_green_id,
        puzzle_blue_id,
        puzzle_red_mat=[],
        puzzle_green_mat=[],
        puzzle_blue_mat=[]
    ):
        # data = data if data else display
        # frameRange = '%s - %s'%(startFrame, endFrame)

        customWidget = puzzleMatte_CustomWidget()
        customWidget.set_text_puzzle_name(puzzle_name)
        customWidget.set_text_puzR_id(puzzle_red_id)
        customWidget.set_text_puzG_id(puzzle_green_id)
        customWidget.set_text_puzB_id(puzzle_blue_id)
        customWidget.set_text_puzR_mat(puzzle_red_mat)
        customWidget.set_text_puzG_mat(puzzle_green_mat)
        customWidget.set_text_puzB_mat(puzzle_blue_mat)
        customWidget.set_icon(icon.maya)

        item = QtWidgets.QListWidgetItem(self.puzzle_matte_listWidget)
        item.setSizeHint(customWidget.sizeHint())
        data = {
            'puzzle_name': puzzle_name,
            'puzzle_red_id': puzzle_red_id,
            'puzzle_green_id': puzzle_green_id,
            'puzzle_blue_id': puzzle_blue_id,
            'puzzle_red_mat': puzzle_red_mat,
            'puzzle_green_mat': puzzle_green_mat,
            'puzzle_blue_mat': puzzle_blue_mat
        }
        item.setData(QtCore.Qt.UserRole, data)

        self.puzzle_matte_listWidget.addItem(item)
        self.puzzle_matte_listWidget.setItemWidget(item, customWidget)

    def add_item_material(self, shader_engine, material_name, object_assign):
        material_item = list_material_widget.ListMaterialWidget()
        material_item.set_text_material_name(material_name)
        material_item.set_text_obj_assign(object_assign)
        material_item.set_icon(icon.maya)

        item = QtWidgets.QListWidgetItem(self.list_material_widget)

        item.setSizeHint(material_item.sizeHint())
        data = {
            'shader_engine': shader_engine,
            'material_name': material_name,
            'object_assign': object_assign
        }
        item.setData(QtCore.Qt.UserRole, data)

        self.list_material_widget.addItem(item)
        self.list_material_widget.setItemWidget(item, material_item)


class puzzleMatte_CustomWidget(QtWidgets.QWidget):
    """docstring for puzzleMatte_CustomWidget"""
    def __init__(self, parent=None):
        super(puzzleMatte_CustomWidget, self).__init__(parent)
        self.layout = QtWidgets.QVBoxLayout()

        self.puzzle_matte_name = QtWidgets.QLabel()
        self.layout.addWidget(self.puzzle_matte_name)

        self.puzzle_red_layout = QtWidgets.QHBoxLayout()
        self.puzzle_red_label = QtWidgets.QLabel('red')
        self.puzzle_red_id_label = QtWidgets.QLabel()
        self.puzzle_red_mat_label = QtWidgets.QLabel()
        self.puzzle_red_layout.addWidget(self.puzzle_red_label)
        self.puzzle_red_layout.addWidget(self.puzzle_red_id_label)
        self.puzzle_red_layout.addWidget(self.puzzle_red_mat_label)
        self.layout.addLayout(self.puzzle_red_layout)

        self.puzzle_green_layout = QtWidgets.QHBoxLayout()
        self.puzzle_green_label = QtWidgets.QLabel('green')
        self.puzzle_green_id_label = QtWidgets.QLabel()
        self.puzzle_green_mat_label = QtWidgets.QLabel()
        self.puzzle_green_layout.addWidget(self.puzzle_green_label)
        self.puzzle_green_layout.addWidget(self.puzzle_green_id_label)
        self.puzzle_green_layout.addWidget(self.puzzle_green_mat_label)
        self.layout.addLayout(self.puzzle_green_layout)

        self.puzzle_blue_layout = QtWidgets.QHBoxLayout()
        self.puzzle_blue_label = QtWidgets.QLabel('blue')
        self.puzzle_blue_id_label = QtWidgets.QLabel()
        self.puzzle_blue_mat_label = QtWidgets.QLabel()
        self.puzzle_blue_layout.addWidget(self.puzzle_blue_label)
        self.puzzle_blue_layout.addWidget(self.puzzle_blue_id_label)
        self.puzzle_blue_layout.addWidget(self.puzzle_blue_mat_label)
        self.layout.addLayout(self.puzzle_blue_layout)

        self.allQHBoxLayout = QtWidgets.QHBoxLayout()
        self.iconQLabel = QtWidgets.QLabel()
        self.allQHBoxLayout.addWidget(self.iconQLabel, 0)

        self.allQHBoxLayout.addLayout(self.layout, 1)
        self.setLayout(self.allQHBoxLayout)

        self.layout.setContentsMargins(0, 0, 0, 0)
        self.layout.setSpacing(0)

        # setStyleSheet
        self.puzzle_matte_name.setFont(QtGui.QFont("Arial", 12, QtGui.QFont.Bold))
        self.puzzle_red_label.setFont(QtGui.QFont("Arial", 8, QtGui.QFont.Bold))
        self.puzzle_green_label.setFont(QtGui.QFont("Arial", 8, QtGui.QFont.Bold))
        self.puzzle_blue_label.setFont(QtGui.QFont("Arial", 8, QtGui.QFont.Bold))

        self.puzzle_red_label.setStyleSheet('color: #FF3333')
        self.puzzle_green_label.setStyleSheet('color: #55F365')
        self.puzzle_blue_label.setStyleSheet('color: #5555F3')

    def set_text_puzzle_name(self, text):
        self.puzzle_matte_name.setText(text)

    def set_text_puzR_id(self, text):
        self.puzzle_red_id_label.setText(str(text))

    def set_text_puzG_id(self, text):
        self.puzzle_green_id_label.setText(str(text))

    def set_text_puzB_id(self, text):
        self.puzzle_blue_id_label.setText(str(text))

    def set_text_puzR_mat(self, list_text):
        text = " , ".join(list_text)
        self.puzzle_red_mat_label.setText(text)

    def set_text_puzG_mat(self, list_text):
        text = " , ".join(list_text)
        self.puzzle_green_mat_label.setText(text)

    def set_text_puzB_mat(self, list_text):
        text = " , ".join(list_text)
        self.puzzle_blue_mat_label.setText(text)

    def set_icon(self, imagePath):
        self.iconQLabel.setPixmap(QtGui.QPixmap(imagePath))


class NodeListWidget(QtWidgets.QListWidget):
    dropped = QtCore.Signal(tuple, dict, dict)
    right_click = QtCore.Signal(dict, str)

    def __init__(self, parent=None):
        super(NodeListWidget, self).__init__(parent)
        self.parent = parent
        self.setAcceptDrops(True)
        self.setDragDropMode(QtWidgets.QAbstractItemView.DropOnly)
        self.setIconSize(QtCore.QSize(72, 72))

    def dragEnterEvent(self, event):
        if event.mimeData().hasUrls():
            event.accept()
        else:
            super(NodeListWidget, self).dragEnterEvent(event)

    def dragMoveEvent(self, event):
        if event.mimeData().hasUrls():
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
        else:
            super(NodeListWidget, self).dragMoveEvent(event)

    def dropEvent(self, event):
        cursorPos = event.pos()
        node_selected = self.parent.puzzle_matte_listWidget.itemAt(cursorPos)
        material_selected = self.parent.list_material_widget.currentItem()
        if node_selected and material_selected:
            self.dropped.emit(cursorPos, node_selected, material_selected)
            # self.parent.add_item_puzzle(material_name, 'red', 'green', 'blue', 'data')

    def contextMenuEvent(self, event):
        puzz_item = self.parent.puzzle_matte_listWidget.currentItem()
        # index = self.parent.puzzle_matte_listWidget.currentIndex()
        # data = index.data(QtCore.Qt.UserRole)
        contextMenu = QtWidgets.QMenu(self.parent.puzzle_matte_listWidget)

        remove_node_act = contextMenu.addAction("Remove Node")
        clear_red_act = contextMenu.addAction("Clear Red")
        clear_green_act = contextMenu.addAction("Clear Green")
        clear_blue_act = contextMenu.addAction("Clear Blue")

        action = contextMenu.exec_(self.mapToGlobal(event.pos()))

        if action == clear_red_act:
            self.right_click.emit(puzz_item, 'puzzle_red_mat')
        elif action == clear_green_act:
            self.right_click.emit(puzz_item, 'puzzle_green_mat')
        elif action == clear_blue_act:
            self.right_click.emit(puzz_item, 'puzzle_blue_mat')
        elif action == remove_node_act:
            self.right_click.emit(puzz_item, 'remove_node')


class RGBSelectDialog(QtWidgets.QDialog):
    def __init__(self, parent=None):
        super(RGBSelectDialog, self).__init__(parent)
        self.push_btn_layout = QtWidgets.QHBoxLayout()
        self.red_btn = QtWidgets.QPushButton('Red', self)
        self.green_btn = QtWidgets.QPushButton('Green', self)
        self.blue_btn = QtWidgets.QPushButton('Blue', self)

        self.push_btn_layout.addWidget(self.red_btn)
        self.push_btn_layout.addWidget(self.green_btn)
        self.push_btn_layout.addWidget(self.blue_btn)

        self.setLayout(self.push_btn_layout)

        self.init_signal()
        self.show()

    def init_signal(self):
        self.red_btn.clicked.connect(self.red_btn_click)
        self.green_btn.clicked.connect(self.green_btn_click)
        self.blue_btn.clicked.connect(self.blue_btn_click)

    def red_btn_click(self):
        self.value = 'puzzle_red_mat', 'puzzle_red_id'
        self.accept()

    def green_btn_click(self):
        self.value = 'puzzle_green_mat', 'puzzle_green_id'
        self.accept()

    def blue_btn_click(self):
        self.value = 'puzzle_blue_mat', 'puzzle_blue_id'
        self.accept()

    def get_value(self):
        return self.value
