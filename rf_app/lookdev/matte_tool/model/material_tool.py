from sg_services import ShotgunServices

import maya.cmds as mc
import maya.mel as mm
from rf_maya.rftool.utils import redshift_hook

DISTANCE_OBJECT_ID = 25


class MaterialTool:
    def __init__(self, context=None):
        # super(MaterialTool, self).__init__(parent, asset_name)
        self.sg = ShotgunServices()
        self.context = context
        self.asset_name = ""
        self.object_id = ""
        self.project = ""

        self.last_material_id = ""

        self.init_state(context)

    def init_state(self, context):
        self.set_project()
        self.set_asset_name(context.name)
        self.set_object_id()

    def remove_rgb_id(self, rgb_id):
        pass

    def get_object_id(self):
        return self.object_id

    def get_asset_name(self):
        return self.asset_name

    def get_project(self):
        return self.project

    def get_rgb_id(self, node):
        red_id = mc.getAttr('%s.redId' % node)
        green_id = mc.getAttr('%s.greenId' % node)
        blue_id = mc.getAttr('%s.blueId' % node)

        return red_id, green_id, blue_id

    def get_material_id(self, shader_engine):
        material_id = mc.getAttr('%s.rsMaterialId' %shader_engine)
        return material_id

    def get_last_rgb_id(self, list_rgb_id):
        for obj_id in range(self.object_id['sg_object_id'] + 1, self.object_id['sg_object_id'] + DISTANCE_OBJECT_ID):
            if obj_id not in list_rgb_id:
                return obj_id

    def get_puzz_mat(self, list_material, red_id, green_id, blue_id):
        r_mat = []
        g_mat = []
        b_mat = []
        for material in list_material:
            if material['material_id'] == 0:
                continue
            if red_id == material['material_id']:
                r_mat.append(material['material_name'])
            elif green_id == material['material_id']:
                g_mat.append(material['material_name'])
            elif blue_id == material['material_id']:
                b_mat.append(material['material_name'])
        return r_mat, g_mat, b_mat

    def set_material_id_by_name(self, shader_engine, value):
        material_id = mc.setAttr('%s.rsMaterialId' %shader_engine, value)

    def set_project(self):
        self.project = self.sg.get_project_info(self.context.project)

    def set_asset_name(self, asset_name):
        self.asset_name = asset_name

    def set_object_id(self):
        self.object_id = self.sg.get_object_id(self.project, self.asset_name)

    def set_rgb_id(self, node_name, red_id, green_id, blue_id):
        red_id = mc.setAttr('{node_name}.redId'.format(node_name=node_name), red_id)
        green_id = mc.setAttr('{node_name}.greenId'.format(node_name=node_name), green_id)
        blue_id = mc.setAttr('{node_name}.blueId'.format(node_name=node_name), blue_id)

    def set_material_id(self, dragged_material_data, last_rgb_id):
        material_name = dragged_material_data['shader_engine']
        self.set_material_id_by_name(dragged_material_data['shader_engine'], last_rgb_id)
        # blue_id = mc.setAttr('{node_naget_puzz_matme}.blueId'.format(
        #     material_name=dragged_material_data['material_name']
        # ), blue_id)

    def set_object_assign(self, dragged_material_data, node_data):
        # mc.setAttr
        pass

    def fetch_material(self):
        list_materials = mc.ls(type='shadingEngine', l=True)
        return list_materials

    def fetch_node(self):
        nodes = mc.ls(type='RedshiftAOV')
        return nodes

    def fetch_aov_type(self):
        nodes = self.fetch_node()
        puzzle_name_nodes = []
        for node in nodes:
            aov_type = mc.getAttr('%s.aovType'%node)
            if aov_type == 'Puzzle Matte':
                puzzle_name_nodes.append(node)
        return puzzle_name_nodes

    def create_node(self, node_name, mode=0):
        if mc.objExists(node_name) or node_name.isdigit():
            return False
        else:
            redshift_hook.create_puzzle_matte(node_name, mode)
            return True

    def dont_have_object_id(self):
        nodes = self.fetch_node()
        for node in nodes:
            if node == "ObjectID":
                return False
        return True

    def droped_update_node(self, node, material, list_mat):
        # object_id = self.get_object_id()
        if node['puzzle_name'] == "ObjectID":
            return 'object_id'
        if material['material_name'] in list_mat.keys():
            return 'duplicate'
        else:
            return "valid"
            # last_zero_rgb = self.get_last_zero_rgb(node)
            # return last_zero_rgb

    def filter_material_id(self, list_materials, list_rgb):
        mat_clear = []
        for material in list_materials:
            if material['material_id'] in list_rgb:
                pass
            else:
                self.set_material_id_by_name(material['shader_engine'], 0)
                mat_clear.append(material['shader_engine'])

        return mat_clear

    def get_material_name(self, shading_group):
        return mc.ls(mc.listConnections(shading_group), materials=True)

    def get_last_zero_rgb(self, node_data):

        if node_data['puzzle_red_mat'] == "":
            return 'puzzle_red_id'
        elif node_data['puzzle_green_mat'] == "":
            return 'puzzle_green_id'
        elif node_data['puzzle_blue_mat'] == "":
            return 'puzzle_blue_id'
        else:
            return None

    def get_geo_shader_assign(self, shader_engine):
        return mc.listConnections(shader_engine, type="mesh")

    def get_shad_engine_from_mat(self, material):
        return mc.listConnections(material, type='shadingEngine')[0]

    def clear_puzzle_matte_item(self, matte_name):
        pass

    def remove_node(self, aov_name):
        mc.delete(aov_name)

    def select_obj(self, obj):
        mc.select(obj)
