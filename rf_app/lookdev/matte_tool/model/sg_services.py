import os
import sys


core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)

# import config
import rf_config as config

from rf_utils.sg.sg_utils import sg


class ShotgunServices:
    def __init__(self):
        self.sg = sg

    def get_object_id(self, project, asset_name):
        project_filtered = {
            "type": project['type'],
            "id": project['id'],
            "name": project['name']
        }
        asset_filter = [
            ['code', 'is', asset_name],
            ['project', 'is', project_filtered],
        ]
        fields = ['sg_object_id', 'code']
        object_id = self.sg.find_one('Asset', asset_filter, fields)
        return object_id

    def get_project_info(self, project_name):
        project_filter = [
            ['name', 'is', project_name]
        ]
        fields = ['name']
        project = self.sg.find_one('Project', project_filter, fields)
        return project
