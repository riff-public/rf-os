import os
import sys

core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)

# import config
import rf_config as config
from rf_utils.sg.sg_utils import sg
from rf_utils.context import context_info
from rf_utils import file_utils
import maya.cmds as mc
import maya.mel as mm
#node_data ex: {"node_name": "Test8", "puzz_red_mat": ["canis1:rsShortHair"], "red_id": 7247, "puzz_green_mat": [], "green_id": 7248, "puzz_blue_mat": [], "blue_id": 7249}
def sourceMel():
    pluginsPath = os.environ.get('REDSHIFT_SCRIPT_PATH')
    melFile = 'redshiftCreateAovTab.mel'
    sourcePath = os.path.join(pluginsPath, melFile).replace('\\', '/')
    mm.eval('source "%s"'%sourcePath)


def set_rgb_id(asset_context, node_data, red_id, green_id, blue_id):
    shadeNamespace = asset_context.mtr_namespace
    if red_id:
        mc.setAttr('{node_name}.redId'.format(node_name=node_data['node_name']), red_id)
        set_rgb_id_in_shadEng(node_data['puzz_red_mat'], red_id, shadeNamespace)
    if green_id:
        mc.setAttr('{node_name}.greenId'.format(node_name=node_data['node_name']), green_id)
        set_rgb_id_in_shadEng(node_data['puzz_green_mat'], green_id, shadeNamespace)
    if blue_id:
        mc.setAttr('{node_name}.blueId'.format(node_name=node_data['node_name']), blue_id)
        set_rgb_id_in_shadEng(node_data['puzz_blue_mat'], blue_id, shadeNamespace)

def create_puzzle_matte(name, mode):
    puzzleNode = mm.eval('redshiftCreateAov("Puzzle Matte")')
    mc.setAttr('%s.name' % puzzleNode, name, type='string')
    mc.setAttr('%s.mode' % puzzleNode, mode)
    mm.eval('redshiftUpdateActiveAovList')
    print 'name {0}'.format(name)
    return mc.rename(puzzleNode, name)

def set_rgb_id_in_shadEng(node_name, mat_id, shadeNamespace): #node_name == 'Redshiftmat'
    #find shadingEngine
    if node_name != 'ObjectID' and node_name:
        if not mc.objExists(node_name[0]):
            return

        node_name = node_name[0].split(":")[-1]
        print "*"*50
        print shadeNamespace, node_name
        print "*"*50

        shadingEngine = mc.listConnections('%s:%s'%(shadeNamespace, node_name), c=True, type='shadingEngine')

        if shadingEngine:
            shadingEngine = shadingEngine[1]
            mc.setAttr('%s.rsMaterialId'%shadingEngine, mat_id)
  

def import_puzz_matte(asset_context, namespaces):
    sourceMel()
    asset = asset_context
    asset.context.update(look='main', res='md', step='lookdev')
    path = asset.path.hero().abs_path()
    name = asset.output_name(outputKey='matteData', hero=True)
    matte_data_path = os.path.join(path, name).replace('\\', '/')

    json_data = file_utils.json_loader(matte_data_path)
    for node_data in json_data:
        node_name = node_data['node_name']
        if not 'ObjectID' in node_name:
            exists = chk_aov_exists(node_name)
            if not exists:
                create_puzzle_matte(node_name, 0)
            print node_data['red_id']
            set_rgb_id(asset_context, node_data, node_data['red_id'], node_data['green_id'], node_data['blue_id'])

        ######### build objectID
        # else:
        #     create_rs_object_parameter(asset.name, namespaces, node_data['red_id'])
        #     exists = chk_aov_exists(node_name)
        #     if not exists:
        #         create_puzzle_matte(node_name, 1)
        #     set_rgb_id(asset_context, node_data, node_data['red_id'], node_data['green_id'], node_data['blue_id'])


def chk_aov_exists(Aov):
    list_aov = mc.ls(type = 'RedshiftAOV')
    puzzle_name_nodes = []
    for aov in list_aov:
        aov_type = mc.getAttr('%s.aovType'%aov)
        if aov_type == 'Puzzle Matte':
            puzzle_name_nodes.append(aov)
    if not Aov in puzzle_name_nodes:
        return False
    else:
        return True

def create_rs_object_parameter(asset_name, namespaces, objectId):
    from rftool.render.redshift import rs_utils
    reload(rs_utils)
    if namespaces:
        for namespace in namespaces:
            print '%s:Geo_Grp' % namespace
            if mc.objExists('%s:Geo_Grp' % namespace):
                rs_utils.add_object_id('%s_rsObjectId'%asset_name, '%s:Geo_Grp' % namespace)
                mc.setAttr("%s_rsObjectId.objectId"% asset_name, objectId)
