# # -*- coding: utf-8 -*-
# import sys
# import os
# import getpass

# core_path = '%s/core' % os.environ.get('RFSCRIPT_DEV')
# if core_path not in sys.path:
#     sys.path.append(core_path)

# import rf_config as config

# from controller.submitter_ctrl import SubmitterCtrl

# from models.auto_edit import AutoEditModel
# # from models.submitter import Submitter

# from rf_utils.context import context_info
# from rf_utils.ui import load, stylesheet
# from rf_utils.widget import browse_widget
# from rf_utils import log_utils, project_info
# os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide2', 'PySide'])
# from Qt import QtWidgets, QtCore, QtGui


# UI_PATH = "{core}/rf_app/auto_edit/view/auto_edit.ui".format(core=core_path)
# # SHOTGUN_URL = "https://riffanimation.shotgunstudio.com"


# class AutoEditCtrl(QtWidgets.QMainWindow):
#     def __init__(self, parent=None):
#         super(AutoEditCtrl, self).__init__(parent)
#         self.ui = load.setup_ui(UI_PATH, self)

#         self.auto_edit_model = AutoEditModel()
#         self.init_state_app()
#         self.init_signal()
#         self.ui.show()

#     def init_signal(self):
#         self.ui.projects_combobox.currentIndexChanged.connect(self.project_combobox_changed)
#         self.ui.submit_btn.clicked.connect(self.onclick_submitter)

#     def init_state_app(self):
#         self.init_projects_combobox()

#     def init_projects_combobox(self):
#         self.ui.projects_combobox.blockSignals(True)
#         self.ui.projects_combobox.clear()
#         projects = self.auto_edit_model.get_projects()
#         if len(projects) == 0:
#             self.ui.projects_combobox.addItem("Not Found")
#         else:
#             for row, project in enumerate(projects):
#                 self.ui.projects_combobox.addItem(project.get('name'))
#                 self.ui.projects_combobox.setItemData(row, project, QtCore.Qt.UserRole)
#         self.ui.projects_combobox.blockSignals(False)
#         self.project_combobox_changed()

#     def project_combobox_changed(self):
#         self.refresh_monitor()

#     def onclick_submitter(self):
#         self.submitter_ctrl = SubmitterCtrl()
#         self.submitter_ctrl.ui.show()

#     def get_project_selected(self):
#         index = self.ui.projects_combobox.currentIndex()
#         project_selected = self.ui.projects_combobox.itemData(index, QtCore.Qt.UserRole)
#         return project_selected

#     def refresh_monitor(self):
#         project_selected = self.get_project_selected()
#         print("refresh_monitor: ", project_selected)
