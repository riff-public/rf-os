# # -*- coding: utf-8 -*-
# import sys
# import os

# core_path = '%s/core' % os.environ.get('RFSCRIPT_DEV')
# if core_path not in sys.path:
#     sys.path.append(core_path)

# import rf_config as config

# from models.submitter import SubmitterModel

# from rf_utils.context import context_info
# from rf_utils.ui import load, stylesheet
# from rf_utils.widget import browse_widget
# from rf_utils import log_utils, project_info
# os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide2', 'PySide'])
# from Qt import QtWidgets, QtCore, QtGui

# UI_SUBMITTER_PATH = "{core}/rf_app/auto_edit/view/submitter.ui".format(core=core_path)
# SHOTGUN_URL = "https://riffanimation.shotgunstudio.com"


# class SubmitterCtrl(QtWidgets.QMainWindow):
#     def __init__(self, parent=None):
#         super(SubmitterCtrl, self).__init__(parent)
#         self.ui = load.setup_ui(UI_SUBMITTER_PATH, self)

#         self.submitter_model = SubmitterModel()
#         self.init_state_app()
#         self.init_signal()

#     def init_signal(self):
#         self.ui.projects_combobox.currentIndexChanged.connect(self.project_combobox_changed)
#         self.ui.step_combobox.currentIndexChanged.connect(self.step_combobox_changed)
#         self.ui.fetch_btn.clicked.connect(self.onfetch)
#         self.ui.seq_listwidget.itemClicked.connect(self.seq_listwidget_clicked)
#         self.ui.submit_btn.clicked.connect(self.onsubmit)
#         # self.ui.run_now_checkbox.stateChanged.connect(self.onchange_run_now_checkbox)
#     #     self.ui.submit_btn.clicked.connect(self.onclick_submitter)

#     def init_state_app(self):
#         self.init_projects_combobox()
#         self.init_steps_combobox()

#     def init_projects_combobox(self):
#         self.ui.projects_combobox.blockSignals(True)
#         self.ui.projects_combobox.clear()
#         projects = self.submitter_model.get_projects()
#         if len(projects) == 0:
#             self.ui.projects_combobox.addItem("Not Found")
#         else:
#             for row, project in enumerate(projects):
#                 self.ui.projects_combobox.addItem(project.get('name'))
#                 self.ui.projects_combobox.setItemData(row, project, QtCore.Qt.UserRole)
#         self.ui.projects_combobox.blockSignals(False)
#         self.project_combobox_changed()

#     def init_steps_combobox(self):
#         self.ui.step_combobox.blockSignals(True)
#         self.ui.step_combobox.clear()
#         steps = self.submitter_model.get_steps()
#         if len(steps) == 0:
#             self.ui.step_combobox.addItem("Not Found")
#         else:
#             for row, step in enumerate(steps):
#                 self.ui.step_combobox.addItem(step)
#                 self.ui.step_combobox.setItemData(row, step, QtCore.Qt.UserRole)
#         self.ui.step_combobox.blockSignals(False)

#     def init_seq_list_widget(self):
#         self.ui.seq_listwidget.clear()
#         seq_list = self.submitter_model.get_seq_list()
#         for seq in seq_list:
#             widget_item = QtWidgets.QListWidgetItem(self.ui.seq_listwidget)
#             widget_item.setText(seq)
#             widget_item.setData(QtCore.Qt.UserRole, seq)
#             self.ui.seq_listwidget.addItem(widget_item)

#     def init_shots_list_widget(self):
#         self.ui.shot_listwidget.clear()
#         shots_list = self.submitter_model.get_shots_list()
#         for shot in shots_list:
#             widget_item = QtWidgets.QListWidgetItem(self.ui.shot_listwidget)
#             widget_item.setText(shot.get('code'))
#             widget_item.setData(QtCore.Qt.UserRole, shot)
#             self.ui.shot_listwidget.addItem(widget_item)

#     def project_combobox_changed(self):
#         project_selected = self.get_project_selected()
#         self.submitter_model.set_project_selected(project_selected)

#     def step_combobox_changed(self):
#         step_selected = self.get_step_selected()
#         self.submitter_model.set_step_selected(step_selected)

#     def seq_listwidget_clicked(self):
#         seq_selected = self.get_seq_selected()
#         self.submitter_model.set_seq_selected(seq_selected)
#         self.submitter_model.set_shots_listwidget()
#         self.init_shots_list_widget()

#     def get_project_selected(self):
#         index = self.ui.projects_combobox.currentIndex()
#         project_selected = self.ui.projects_combobox.itemData(index, QtCore.Qt.UserRole)
#         return project_selected

#     def get_step_selected(self):
#         index = self.ui.step_combobox.currentIndex()
#         step_selected = self.ui.step_combobox.itemData(index, QtCore.Qt.UserRole)
#         return step_selected

#     def get_seq_selected(self):
#         seq_selected = []
#         items = self.ui.seq_listwidget.selectedItems()
#         for i in xrange(0, len(items)):
#             shot_item = self.ui.seq_listwidget.selectedItems()[i].data(QtCore.Qt.UserRole)
#             seq_selected.append(shot_item)
#         return seq_selected

#     def onfetch(self):
#         self.submitter_model.refresh_list_seqs_submitter()
#         self.init_seq_list_widget()

#     def onsubmit(self):
#         run_now = self.ui.run_now_checkbox.isChecked()
#         freq = self.ui.freq_time.text()

#         if not freq.isdigit():
#             diag = QtWidgets.QMessageBox(
#                 QtWidgets.QMessageBox.Critical,
#                 "Error Message",
#                 "Freq should be Number."
#             )
#             diag.exec_()
#             return
#         self.submitter_model.start_job(run_now=run_now, freq=freq)
