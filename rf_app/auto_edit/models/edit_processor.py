# import os
# import sys
# import json
# import subprocess

# core = '%s/core' % os.environ.get('RFSCRIPT')
# if not core in sys.path:
#     sys.path.append(core)

# # import config
# import rf_config as config

# from rf_utils.context import context_info
# from rf_utils.file_utils import xcopy_file

# STEP_PRIORITY = ["comp", "anim", "layout", "animatic"]


# def get_priority_shot(shot, submit_job_data, episode):
#     step_filtered = STEP_PRIORITY[STEP_PRIORITY.index(submit_job_data['step']):]
#     matched_priority = False
#     mov_files = []
#     match_step = ""

#     for step in step_filtered:
#         if matched_priority:
#             break
#         if step == "animatic":
#             publ_path = submit_job_data['publ_path']
#             animatic_path = "{publ_path}/{episode}/{shot_code}/edit/animatic/hero/outputMedia".format(
#                 publ_path=publ_path,
#                 episode=episode,
#                 shot_code=shot['code']
#             )
#             if os.path.exists(animatic_path):
#                 edit_animatics = sorted(os.listdir(animatic_path))
#                 for edit_animatic_file in edit_animatics:
#                     if len(edit_animatic_file.split("_")) == 6:
#                         filename, file_extension = os.path.splitext(edit_animatic_file)
#                         if file_extension == ".mov":
#                             file_data = {
#                                 "filename": edit_animatic_file,
#                                 "file_path": animatic_path
#                             }
#                             mov_files.append(file_data)
#         else:
#             shot_path = "{episode_path}/{episode}/{shot_code}".format(
#                 episode_path=submit_job_data['episode_path'],
#                 episode=episode,
#                 shot_code=shot['code']
#             )
#             if os.path.exists(shot_path):
#                 processes_path = "{shot_path}/{step}".format(shot_path=shot_path, step=step)
#                 processes = os.listdir(processes_path)
#                 for process in processes:
#                     if process != "qc":
#                         output_work_files = "{processes_path}/{process}/output".format(processes_path=processes_path, process=process)
#                         if os.path.exists(output_work_files):
#                             list_file = sorted(os.listdir(output_work_files))
#                             for file in list_file:
#                                 if len(file.split("_")) == 6:
#                                     file_data = {
#                                         "filename": file,
#                                         "file_path": output_work_files
#                                     }
#                                     mov_files.append(file_data)

#         if len(mov_files) != 0:
#             matched_priority = True
#         match_step = step

#     result = {
#         "mov_files": mov_files,
#         "match_step": match_step
#     }

#     return result


# def read_data(file_path):
#     data = []
#     with open(file_path) as json_file:
#         data = json.load(json_file)

#     return data


# def settlement_edit_shot(shots_mapped, step_selected, file_path):
#     for shot in shots_mapped:
#         if shot['match_step'] == "animatic":
#             if shot['mov_files']:
#                 mov_file = shot['mov_files'][-1]
#                 src_mov_file = "{file_path}/{filename}".format(
#                     file_path=mov_file['file_path'],
#                     filename=mov_file['filename']
#                 )
#                 xcopy_file(src_mov_file, os.path.dirname(file_path))
#             else:
#                 create_missing_shot(mov_file, file_path, shot['shot_code'])
#             continue

#         if shot['match_step'] != step_selected:
#             mov_file = shot['mov_files'][-1]
#             convert_vdo_not_matched_step(mov_file, file_path, shot['match_step'])
#         else:
#             # for mov_file in shot['mov_files'][-1]:
#             mov_file = shot['mov_files'][-1]
#             src_mov_file = "{file_path}/{filename}".format(
#                 file_path=mov_file['file_path'],
#                 filename=mov_file['filename']
#             )
#             # print("src_mov_file:", src_mov_file)
#             # print("file_path:",  os.path.dirname(file_path))
#             xcopy_file(src_mov_file, os.path.dirname(file_path))


# def write_list_vdo_file(data, file_path):
#     list_episode = os.listdir(data['episode_path'])
#     shots_mapped = []
#     for episode in list_episode:
#         for shot in data['shots']:
#             shot_path = "{episode_path}/{episode}/{shot_code}".format(
#                 episode_path=data['episode_path'],
#                 episode=episode,
#                 shot_code=shot['code']
#             )
#             if os.path.exists(shot_path):
#                 priority_step_shot = get_priority_shot(shot, data, episode)
#                 priority_step_shot.update({'shot_code': shot['code']})
#                 priority_step_shot.update({'seq': shot['sg_sequence_code']})
#                 priority_step_shot.update({'episode': episode})
#                 shots_mapped.append(priority_step_shot)
#             # print("shot_path:", shot_path)

#     # with open("D:\\tutorial\\edit_processor\\result.json", 'w') as outfile:
#     #     json.dump(shots_mapped, outfile)
#     settlement_edit_shot(shots_mapped, data['step'], file_path)


# def create_missing_shot(mov_file, output_filepath, shot_code):
#     raw_s = "drawtext=fontfile=C\\\\:/Windows/Fonts/arial.ttf:text='Missing {shot_code}':fontcolor=red:fontsize=48:x=(w-tw)/2:y=(h/2)".format(
#         shot_code=shot_code
#     )
#     output_path = "{output_path}/{filename}".format(
#         output_path=os.path.dirname(output_filepath),
#         filename=shot_code + ".mov"
#     )
#     subprocess.Popen([
#         config.Software.ffmpeg,
#         '-y',
#         '-loop',
#         '1',
#         '-i',
#         '{core_path}/rf_app/auto_edit/utils/black_screen.png'.format(core_path=core),
#         '-t',
#         "2",
#         '-vf',
#         raw_s,
#         output_path
#     ])


# def convert_vdo_not_matched_step(mov_file, output_filepath, step):
#     src_mov_file = "{file_path}/{filename}".format(
#         file_path=mov_file['file_path'],
#         filename=mov_file['filename']
#     )
#     font_file = "C\\\\:/Windows/Fonts/arial.ttf"
#     output_path = "{output_path}/{filename}".format(
#         output_path=os.path.dirname(output_filepath),
#         filename=mov_file['filename']
#     )
#     font_color = "red"
#     font_size = 36
#     draw_text_cmd = "drawtext=fontfile={font_file}:text='{step}':fontcolor={font_color}:fontsize={font_size}:x=(w-tw)/2:y=(h*0.92)".format(
#         step=step,
#         font_file=font_file,
#         font_color=font_color,
#         font_size=font_size
#     )

#     subprocess.call([
#         config.Software.ffmpeg,
#         '-y',
#         '-i',
#         src_mov_file,
#         '-vf',
#         draw_text_cmd,
#         output_path
#     ])


# def run(payload_job={}, file_path=""):
#     data = read_data(file_path)
#     if data:
#         if data['run_now']:
#             list_file = write_list_vdo_file(data, file_path)


# # ('draw_text_cmd:', 'drawtext="fontfile=C:/Windows/Fonts/arial.ttf:text=\'layout\':fontcolor=red:fontsize=28:x=(w-tw)/2:y=(h*0.8)"')
# # drawtext="fontfile=C:/Windows/Fonts/arial.ttf:text=\'layout\':fontcolor=red:fontsize=28:x=(w-tw)/2:y=(h*0.8)"

# # drawtext="fontfile=C\\:/Windows/Fonts/arial.ttf:text='Stack Overflow':fontcolor=red:fontsize=28:x=(w-tw)/2:y=(h*0.8)"