# import os
# import uuid
# import json
# import requests
# import urllib2
# from threading import Thread

# from datetime import datetime

# from rf_utils.context import context_info
# from rf_utils import project_info
# from rf_utils.sg.sg_utils import (
#     sg,
#     get_all_projects
# )
# import rf_config as config

# from rf_utils.ui import load
# from Qt import QtWidgets
# import edit_processor


# class SubmitterModel:
#     def __init__(self):
#         self.sg = sg
#         self.projects = []
#         self.project_selected = None
#         self.step_selected = "comp"
#         self.seq_selected = None
#         self.steps = ["comp", "anim", "layout", "animatic"]
#         self.seqs_shots = []
#         self.seqs_list = []
#         self.shots_list = []
#         self.database_path = "\\\\riff-data/Data/Data/Pipeline/_database/auto_edit"
#         self.init_state()
#         self.context = context_info.Context()

#     def init_state(self):
#         self.set_projects()

#     def get_projects(self):
#         return self.projects

#     def get_steps(self):
#         return self.steps

#     def get_seq_list(self):
#         return self.seqs_list

#     def get_shots_list(self):
#         return self.shots_list

#     def set_projects(self):
#         self.projects = get_all_projects()

#     def set_shots_listwidget(self):
#         self.shots_list = []
#         for seq in self.seq_selected:
#             for shot in self.seqs_shots:
#                 if shot['sg_sequence_code'] == seq:
#                     self.shots_list.append(shot)

#     def set_project_selected(self, project_selected):
#         self.project_selected = project_selected
#         self.projectInfo = project_info.ProjectInfo(project=project_selected['name'])
#         self.projectInfo.set_project_env()

#     def set_step_selected(self, step_selected):
#         self.step_selected = step_selected

#     def set_seq_selected(self, seq_selected):
#         self.seq_selected = seq_selected

#     def refresh_list_seqs_submitter(self):
#         self.process_seqs_shots()

#     def group_seqs(self, seqs_shots):
#         self.seqs_list = []
#         for shot in seqs_shots:
#             self.seqs_list.append(shot['sg_sequence_code'])
#         self.seqs_list = sorted(list(set(self.seqs_list)))

#     def process_seqs_shots(self):
#         filtered = [
#             ["project", "is", self.project_selected],
#             ["sg_shot_type", "is", ["Shot"]]
#         ]
#         fields = ["code", "sg_sequence_code"]
#         order_filtered = [{'field_name': 'code', 'direction': 'asc'}]
#         self.seqs_shots = self.sg.find("Shot", filtered, fields, order_filtered)
#         self.group_seqs(self.seqs_shots)

#     def start_job(self, run_now=True, freq=7):
#         payload_job = {
#             'project': self.project_selected,
#             'step': self.step_selected,
#             'seq': self.seq_selected,
#             'shots': self.shots_list,
#             'freq': freq,
#             'run_now': run_now
#         }

#         self.context.use_sg(self.sg, self.project_selected['name'], 'scene')
#         scene = context_info.ContextPathInfo(context=self.context)
#         payload_job.update({'episode_path': scene.path.work().unc_path()})
#         payload_job.update({'publ_path': scene.path.publish().unc_path()})
#         file_path, job_id = self.save_job(payload_job)

#         data = {
#             'payload_job': payload_job,
#             'file_path': file_path
#         }

#         # SUBMIT_API_ENDPOINT = "http://172.16.100.26/riff_flask/auto_edit/submit_job"
#         SUBMIT_API_ENDPOINT = "http://127.0.0.1:5000/auto_edit/submit_job"

#         # response = requests.async.post(url=SUBMIT_API_ENDPOINT, json=data)

#         # edit_processor.run(payload_job=payload_job, file_path=file_path)
#         try:
#             response = Thread(target=self.submit_job_url, args=[SUBMIT_API_ENDPOINT, data]).start()
#             print("Submitteddd jobbbbbbbbbbbbbbbbbbbbbbbbbbb")
#             msgBox = QtWidgets.QMessageBox()
#             msgBox.setText("Submitted Job {job_id}".format(job_id=job_id))
#             msgBox.exec_()
#         except Exception as e:
#             diag = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Critical, "Error Message", "Error Submit job")
#             diag.exec_()

#     def submit_job_url(self, url, data):
#         payload = json.dumps(data)
#         req = urllib2.Request(url, payload, {'Content-Type': 'application/json'})
#         f = urllib2.urlopen(req)
#         response = json.loads(f.read())
#         f.close()
#         return response

#     def save_job(self, payload_job):
#         date_now = datetime.today().strftime('%Y-%m-%d')
#         date_path = "{database_path}/{date_now}".format(
#             database_path=self.database_path,
#             date_now=date_now
#         )
#         if not os.path.exists(date_path):
#             os.makedirs(date_path)

#         job_id = uuid.uuid4()
#         job_path = "{date_path}/{job_id}".format(
#             date_path=date_path,
#             job_id=job_id
#         )
#         if not os.path.exists(job_path):
#             os.makedirs(job_path)

#         filename = self.generate_filename()
#         file_path = "{job_path}/{filename}.json".format(
#             job_path=job_path,
#             filename=filename
#         )
#         with open(file_path, 'w') as outfile:
#             json.dump(payload_job, outfile)

#         return file_path, str(job_id)

#     def generate_filename(self):
#         project_code = self.project_selected['sg_project_code']
#         seq = self.seq_selected[0]
#         filename = "{project_code}_{seq}".format(
#             project_code=project_code,
#             seq=seq
#         )
#         return filename
