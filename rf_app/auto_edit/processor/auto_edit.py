import os
import sys
import json
import shutil
import subprocess
from datetime import datetime

core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)

# import config
import rf_config as config
import cv2
from rf_utils.context import context_info
from rf_utils import project_info
from rf_utils.file_utils import xcopy_file
from rf_utils.sg import sg_utils, sg_process
sg = sg_utils.sg


STEP_PRIORITY = ["comp", "anim", "layout", "animatic"]


def get_priority_shot(shot, submit_job_data, episode, episode_path, step_selected, publ_path):
    step_filtered = STEP_PRIORITY[STEP_PRIORITY.index(step_selected):]
    matched_priority = False
    mov_files = []
    match_step = ""

    for step in step_filtered:
        if matched_priority:
            break
        if step == "animatic":
            animatic_path = "{publ_path}/{episode}/{shot_code}/edit/animatic/hero/outputMedia".format(
                publ_path=publ_path,
                episode=episode,
                shot_code=shot['code']
            )
            if os.path.exists(animatic_path):
                edit_animatics = sorted(os.listdir(animatic_path))
                for edit_animatic_file in edit_animatics:
                    if len(edit_animatic_file.split("_")) == 6:
                        filename, file_extension = os.path.splitext(edit_animatic_file)
                        if file_extension == ".mov":
                            file_data = {
                                "filename": edit_animatic_file,
                                "file_path": animatic_path
                            }
                            mov_files.append(file_data)
        else:
            shot_path = "{episode_path}/{episode}/{shot_code}".format(
                episode_path=episode_path,
                episode=episode,
                shot_code=shot['code']
            )
            if os.path.exists(shot_path):
                processes_path = "{shot_path}/{step}".format(shot_path=shot_path, step=step)
                processes = os.listdir(processes_path)
                for process in processes:
                    if process != "qc":
                        output_work_files = "{processes_path}/{process}/output".format(processes_path=processes_path, process=process)
                        if os.path.exists(output_work_files):
                            list_file = sorted(os.listdir(output_work_files))
                            for file in list_file:
                                if len(file.split("_")) == 6:
                                    file_data = {
                                        "filename": file,
                                        "file_path": output_work_files
                                    }
                                    mov_files.append(file_data)

        if len(mov_files) != 0:
            matched_priority = True
        match_step = step

    result = {
        "mov_files": mov_files,
        "match_step": match_step
    }

    return result


def get_shots(project_entity, data, episode_path):
    filtered = [
        ["project", "is", project_entity],
        ["sg_shot_type", "is", ["Shot"]],
        ["sg_sequence_code", "is", data['seq']],
        ["sg_status_list", "is_not", "omt"]
    ]
    fields = ["code", "sg_sequence_code", "sg_working_duration"]
    order_filtered = [{'field_name': 'code', 'direction': 'asc'}]
    shots = sg.find("Shot", filtered, fields, order_filtered)
    print("shots length:", len(shots))

    return shots


def get_episodes(project_name):
    context = context_info.Context()
    context.use_sg(sg, project_name, 'scene')
    scene = context_info.ContextPathInfo(context=context)
    episode_path = scene.path.work().unc_path()
    publ_path = scene.path.publish().unc_path()

    return episode_path, publ_path


def get_last_mov_frame_count(shot):
    if shot['mov_files']:
        mov_file = shot['mov_files'][-1]
        src_mov_file = "{file_path}/{filename}".format(
            file_path=mov_file['file_path'],
            filename=mov_file['filename']
        )
        vidcap = cv2.VideoCapture(src_mov_file)
        success, image = vidcap.read()
        frame_count = 0
        while success:
            success, image = vidcap.read()
            frame_count += 1
        return frame_count
    return 0


def process_auto_edit(shots, episode_path, data, publ_path):
    list_episode = os.listdir(episode_path)
    shots_mapped = []
    for episode in list_episode:
        for shot in shots:
            shot_path = "{episode_path}/{episode}/{shot_code}".format(
                episode_path=episode_path,
                episode=episode,
                shot_code=shot['code']
            )
            if os.path.exists(shot_path):
                priority_step_shot = get_priority_shot(shot, data, episode, episode_path, data['step'], publ_path)
                priority_step_shot.update({'shot_code': shot['code']})
                priority_step_shot.update({'seq': shot['sg_sequence_code']})
                priority_step_shot.update({'sg_working_duration': shot['sg_working_duration'] or 0})

                frame_count_last_mov = get_last_mov_frame_count(priority_step_shot)
                priority_step_shot.update({'last_mov_frame': frame_count_last_mov})
                priority_step_shot.update({'episode': episode})
                shots_mapped.append(priority_step_shot)

    return shots_mapped


def settlement_auto_edit_shot(shots_mapped, data, dst_path):
    for shot in shots_mapped:
        frame_not_match = False
        if shot['mov_files']:
            mov_file = shot['mov_files'][-1]
            if int(shot['sg_working_duration']) != int(shot['last_mov_frame']):
                frame_not_match = True
            if shot['match_step'] == data['step']:
                src_mov_file = "{file_path}/{filename}".format(
                    file_path=mov_file['file_path'],
                    filename=mov_file['filename']
                )
                if frame_not_match:
                    draw_not_match_duration(mov_file, dst_path)
                else:
                    xcopy_file(src_mov_file, dst_path)
            else:
                convert_vdo_not_matched_step(mov_file, dst_path, shot['match_step'], frame_not_match)
        else:
            create_missing_shot(dst_path, shot['shot_code'])
    combine_mov_files(dst_path, data)


def check_sound_vdo(file_path):
    cmd_ff = "\"{ffmpeg}\" -hide_banner -i {file_path} -af volumedetect -vn -f null - 2>&1 | findstr mean_volume".format(
        ffmpeg=config.Software.ffmpeg,
        file_path=file_path
    )
    # print("cmd_ff:", cmd_ff)
    try:
        output = subprocess.check_output(cmd_ff, shell=True)
        return True
    except Exception as e:
        return False


def combine_mov_files(dst_path, data):
    result_filename = "{project_code}_{seq}.mov".format(
        project_code=data['project'],
        seq=data['seq']
    )
    mov_files = list(filter(lambda file: os.path.splitext(file)[1] == ".mov" , os.listdir(dst_path)))
    sound_index = len(mov_files)
    cmd = "{ffmpeg}".format(ffmpeg=config.Software.ffmpeg)
    input_file_cmd = ""
    filter_complex_cmd = "-filter_complex"
    filter_complex_args = ""
    for index, file in enumerate(mov_files):
        file_path = "{dst}/{file}".format(dst=dst_path, file=file)
        is_file_have_sound = check_sound_vdo(file_path)
        input_file_cmd += " -i {file_path}".format(file_path=file_path)

        if is_file_have_sound:
            filter_complex_args += " [{index}:v] [{index}:a]".format(index=index)
        else:
            filter_complex_args += " [{index}:v] [{sound_index}:a]".format(index=index, sound_index=sound_index)
    input_file_cmd += " -f lavfi -t 0.1 -i anullsrc "
    tail_cmd = 'concat=n={count_file}:v=1:a=1 [v] [a]" -map "[v]" -map "[a]" {dst_path}/{result_filename}'.format(
        count_file=len(mov_files),
        dst_path=dst_path,
        result_filename=result_filename
    )

    cmd += input_file_cmd + filter_complex_cmd + ' "' + filter_complex_args + tail_cmd
    subprocess.call(cmd)
    remove_clear_shots(mov_files, dst_path)


def remove_clear_shots(mov_files, dst_path):
    for index, file in enumerate(mov_files):
        file_path = "{dst}/{file}".format(dst=dst_path, file=file)
        os.remove(file_path)


def create_missing_shot(output_filepath, shot_code):
    raw_s = "drawtext=fontfile=C\\\\:/Windows/Fonts/arial.ttf:text='Missing {shot_code}':fontcolor=red:fontsize=48:x=(w-tw)/2:y=(h/2)".format(
        shot_code=shot_code
    )
    output_path = "{output_path}/{filename}".format(
        output_path=output_filepath,
        filename=shot_code + ".mov"
    )
    subprocess.Popen([
        config.Software.ffmpeg,
        '-y',
        '-loop',
        '1',
        '-i',
        '{core_path}/rf_app/auto_edit/utils/black_screen.png'.format(core_path=core),
        '-t',
        "2",
        '-vf',
        raw_s,
        '-vcodec',
        'libx264',
        '-vprofile',
        'baseline',
        '-crf',
        '22',
        '-bf',
        '0',
        '-pix_fmt',
        'yuv420p',
        '-f',
        'mov',
        output_path
    ])


def convert_vdo_not_matched_step(mov_file, output_filepath, step, frame_not_match):
    src_mov_file = "{file_path}/{filename}".format(
        file_path=mov_file['file_path'],
        filename=mov_file['filename']
    )
    font_file = "C\\\\:/Windows/Fonts/arial.ttf"
    output_path = "{output_path}/{filename}".format(
        output_path=output_filepath,
        filename=mov_file['filename']
    )
    font_color = "red"
    font_size = 36
    if frame_not_match:
        not_match_step_msg = "drawtext=fontfile={font_file}:text='{step}':fontcolor={font_color}:fontsize={font_size}:x=(w-tw)/2:y=(h*0.92)".format(
            step=step,
            font_file=font_file,
            font_color=font_color,
            font_size=font_size
        )
        not_match_frame_msg = "drawtext=fontfile={font_file}:text='{text}':fontcolor={font_color}:fontsize={font_size}:x=(w-tw)/2:y=(h*0.08)".format(
            text="Duration not match",
            font_file=font_file,
            font_color=font_color,
            font_size=font_size
        )
        draw_text_cmd = "[in]{not_match_step_msg},{not_match_frame_msg}[out]".format(
            not_match_step_msg=not_match_step_msg,
            not_match_frame_msg=not_match_frame_msg
        )
    else:
        draw_text_cmd = "drawtext=fontfile={font_file}:text='{step}':fontcolor={font_color}:fontsize={font_size}:x=(w-tw)/2:y=(h*0.92)".format(
            step=step,
            font_file=font_file,
            font_color=font_color,
            font_size=font_size
        )

    subprocess.call([
        config.Software.ffmpeg,
        '-y',
        '-i',
        src_mov_file,
        '-vf',
        draw_text_cmd,
        '-vcodec',
        'libx264',
        '-vprofile',
        'baseline',
        '-crf',
        '22',
        '-bf',
        '0',
        '-pix_fmt',
        'yuv420p',
        '-f',
        'mov',
        output_path
    ])


def draw_not_match_duration(mov_file, dst_path):
    src_mov_file = "{file_path}/{filename}".format(
        file_path=mov_file['file_path'],
        filename=mov_file['filename']
    )
    output_path = "{output_path}/{filename}".format(
        output_path=dst_path,
        filename=mov_file['filename']
    )
    font_color = "red"
    font_size = 36
    font_file = "C\\\\:/Windows/Fonts/arial.ttf"
    draw_text_cmd = "drawtext=fontfile={font_file}:text='{text}':fontcolor={font_color}:fontsize={font_size}:x=(w-tw)/2:y=(h*0.08)".format(
        text="Duration not match",
        font_file=font_file,
        font_color=font_color,
        font_size=font_size
    )
    subprocess.call([
        config.Software.ffmpeg,
        '-y',
        '-i',
        src_mov_file,
        '-vf',
        draw_text_cmd,
        '-vcodec',
        'libx264',
        '-vprofile',
        'baseline',
        '-crf',
        '22',
        '-bf',
        '0',
        '-pix_fmt',
        'yuv420p',
        '-f',
        'mov',
        output_path
    ])


def get_dst_path(data):
    dst = "//riff-data/Production/Riff_production/{project_name}/edit/auto_output/{step}/{date}/{seq}".format(
        project_name=data['project'],
        step=data['step'],
        date=datetime.today().strftime('%Y-%m-%d'),
        seq=data['seq']
    )

    return dst


def main():
    data = {}
    data['project'] = sys.argv[1]
    data['seq'] = sys.argv[2]
    data['step'] = sys.argv[3]

    dst_path = get_dst_path(data)
    if os.path.exists(dst_path):
        shutil.rmtree(dst_path)
    if not os.path.exists(dst_path):
        os.makedirs(dst_path)

    projectInfo = project_info.ProjectInfo(project=data['project'])
    projectInfo.set_project_env()

    project_entity = sg_process.get_project(data['project'])
    episode_path, publ_path = get_episodes(data['project'])
    shots = get_shots(project_entity, data, episode_path)
    shots_mapped = process_auto_edit(shots, episode_path, data, publ_path)
    settlement_auto_edit_shot(shots_mapped, data, dst_path)


if __name__ == '__main__':
    main()
