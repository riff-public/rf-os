# # -*- coding: utf-8 -*-
# import sys
# import os
# import getpass
# from settings import (
#     TITLE_APP,
#     VERSION_APP,
#     DES,
#     UI_NAME
# )

# core_path = '%s/core' % os.environ.get('RFSCRIPT_DEV')
# if core_path not in sys.path:
#     sys.path.append(core_path)

# import rf_config as config

# from controller.auto_edit_ctrl import AutoEditCtrl
# # from controller.submitter_ctrl import SubmitterCtrl

# from models.auto_edit import AutoEditModel
# # from models.submitter import Submiiter

# from rf_utils.context import context_info
# from rf_utils.ui import load, stylesheet
# from rf_utils.widget import browse_widget
# from rf_utils import log_utils, project_info
# os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide2', 'PySide'])
# from Qt import QtWidgets, QtCore, QtGui

# userLog = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
# logFile = log_utils.name(UI_NAME, userLog)
# logger = log_utils.init_logger(logFile)


# class AppCtrl:
#     def __init__(self):
#         self.autoedit_ctrl = AutoEditCtrl()
#         # self.submitter_model = SubmiiterCtrl()
#         self.autoedit_ctrl.ui.show()


# def show():
#     logger.info('Run in Audo Edit\n')
#     app = QtWidgets.QApplication.instance()
#     if not app:
#         app = QtWidgets.QApplication(sys.argv)
#     myApp = AppCtrl()
#     stylesheet.set_default(app)
#     sys.exit(app.exec_())


# if __name__ == '__main__':
#     show()
