# -*- coding: utf-8 -*-

# beta - under development
# 1.0.0 - Check existing user using 'sg_name' instead 'name' key in SG
# 1.0.1 - Disable the use of "-" in english names
# 1.0.2 - Add non case-sensitive existing sg_name check before creation

_title = 'SG User Creator'
_version = '1.0.2'
_des = ''
uiName = 'SGUserCreator'

#Import python modules
import sys
import os
import logging
import getpass
import tempfile
import time
from functools import partial
from collections import OrderedDict, defaultdict

core = '%s/core' % os.environ.get('RFSCRIPT')
sys.path.append(core)

# import config
import rf_config as config
from rf_utils import log_utils
from rf_utils.ui import stylesheet

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)

# QT
os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

from rf_utils.sg import sg_user
from rf_utils.sg import sg_process
from rf_utils import thread_pool
from rf_utils import user_info
from rf_utils.widget import display_widget

moduleDir = os.path.dirname(sys.modules[__name__].__file__).replace('\\', '/')
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]


class SGUserCreator(QtWidgets.QMainWindow):

    def __init__(self, parent=None):
        # setup Window
        super(SGUserCreator, self).__init__(parent)

        # app vars
        self.threadpool = QtCore.QThreadPool()
        self.error_color = (235, 0, 0)

        # validators
        self.eng_validator = QtGui.QRegExpValidator(QtCore.QRegExp("[a-zA-Z]+"))
        self.eng_nickname_validator = QtGui.QRegExpValidator(QtCore.QRegExp("[a-zA-Z0-9 ]+"))
        self.thai_validator = QtGui.QRegExpValidator(QtCore.QRegExp(u"[\u0e00-\u0E7F]+"))
        self.int_validator = QtGui.QIntValidator()

        # ui vars
        self.w = 675
        self.h = 425
        self.app_icon = '{}/icons/app_icon.png'.format(moduleDir)
        self.logo_icon = '{}/icons/riff_logo.png'.format(moduleDir)
        self.browse_icon = '{}/icons/browse_icon.png'.format(moduleDir)
        self.paste_icon = '{}/icons/paste_icon.png'.format(moduleDir)
        self.clear_icon = '{}/icons/clear_icon.png'.format(moduleDir)
        self.add_icon = '{}/icons/add_icon.png'.format(moduleDir)
        self.unknown_photo = '{}/icons/unknown_person.png'.format(moduleDir)

        # init functions
        self.setupUi()
        self.set_default()
        self.init_signals()

    def setupUi(self):
        self.setObjectName(uiName)
        self.setWindowTitle('{} {} {}'.format(_title, _version, _des))
        self.setWindowIcon(QtGui.QIcon(self.app_icon))
        self.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))

        self.centralwidget = QtWidgets.QWidget(self)
        self.setCentralWidget(self.centralwidget)
        self.resize(self.w, self.h)

        # main layout
        self.main_layout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.main_layout.setSpacing(9)

        # ----- header layout
        self.header_layout = QtWidgets.QHBoxLayout()
        self.main_layout.addLayout(self.header_layout)

        # logo
        self.logo = QtWidgets.QLabel()
        self.logo.setPixmap(QtGui.QPixmap(self.logo_icon).scaled(64, 64, QtCore.Qt.KeepAspectRatio))
        self.header_layout.addWidget(self.logo)

        # header label
        self.header_label = QtWidgets.QLabel('New user creation for ShotGrid')
        self.header_layout.addWidget(self.header_label)

        # spacer
        spacerItem1 = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.header_layout.addItem(spacerItem1)

        # ----- line
        self.line = QtWidgets.QFrame(self.centralwidget)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.main_layout.addWidget(self.line)

        # ----- tool button
        self.toolButton_layout = QtWidgets.QHBoxLayout()
        self.main_layout.addLayout(self.toolButton_layout)

        # spacer
        spacerItem2 = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.toolButton_layout.addItem(spacerItem2)

        # clear button
        self.clear_button = QtWidgets.QPushButton()
        self.clear_button.setIcon(QtGui.QIcon(self.clear_icon))
        self.clear_button.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.toolButton_layout.addWidget(self.clear_button)

        # ----- new user form layout
        self.data_layout = QtWidgets.QHBoxLayout()
        self.main_layout.addLayout(self.data_layout)

        # --- display
        self.display_layout = QtWidgets.QVBoxLayout()
        self.data_layout.addLayout(self.display_layout)

        self.photo_frame = display_widget.EditablePhotoFrame(self)
        self.display_layout.addWidget(self.photo_frame)

        # --- form
        self.form_layout = QtWidgets.QVBoxLayout()
        self.data_layout.addLayout(self.form_layout)

        # personal
        self.personal_groupBox = QtWidgets.QGroupBox('Personal')
        self.form_layout.addWidget(self.personal_groupBox)
        self.name_layout = QtWidgets.QFormLayout(self.personal_groupBox)

        self.engName_lineEdit = QtWidgets.QLineEdit()
        self.engName_lineEdit.setValidator(self.eng_validator)
        self.engName_lineEdit.setPlaceholderText(u'< Full name in English >')
        self.name_layout.addRow('Full name', self.engName_lineEdit)

        self.engLastName_lineEdit = QtWidgets.QLineEdit()
        self.engLastName_lineEdit.setValidator(self.eng_validator)
        self.engLastName_lineEdit.setPlaceholderText(u'< Last name in English >')
        self.name_layout.addRow('Last name', self.engLastName_lineEdit)

        self.engNickName_lineEdit = QtWidgets.QLineEdit()
        self.engNickName_lineEdit.setValidator(self.eng_nickname_validator)
        self.engNickName_lineEdit.setPlaceholderText(u'< Nickname in English >')
        self.name_layout.addRow('Nickname', self.engNickName_lineEdit)

        self.thaiName_lineEdit = QtWidgets.QLineEdit()
        self.thaiName_lineEdit.setValidator(self.thai_validator)
        self.thaiName_lineEdit.setPlaceholderText(u'< ชื่อ ภาษาไทย >')
        self.name_layout.addRow(u'ชื่อ', self.thaiName_lineEdit)

        self.thaiLastName_lineEdit = QtWidgets.QLineEdit()
        self.thaiLastName_lineEdit.setValidator(self.thai_validator)
        self.thaiLastName_lineEdit.setPlaceholderText(u'< นามสกุล ภาษาไทย >')
        self.name_layout.addRow(u'นามสกุล', self.thaiLastName_lineEdit)

        self.thaiNickName_lineEdit = QtWidgets.QLineEdit()
        self.thaiNickName_lineEdit.setValidator(self.thai_validator)
        self.thaiNickName_lineEdit.setPlaceholderText(u'< ชื่อเล่น ภาษาไทย >')
        self.name_layout.addRow(u'ชื่อเล่น', self.thaiNickName_lineEdit)

        # Account info
        self.acc_groupBox = QtWidgets.QGroupBox('Account Info')
        self.form_layout.addWidget(self.acc_groupBox)
        self.acc_layout = QtWidgets.QFormLayout(self.acc_groupBox)

        self.id_lineEdit = QtWidgets.QLineEdit()
        self.id_lineEdit.setValidator(self.int_validator)
        self.id_lineEdit.setPlaceholderText(u'< รหัสพนักงาน >')
        self.acc_layout.addRow('Employee ID', self.id_lineEdit)

        self.dept_combobox = QtWidgets.QComboBox()
        self.acc_layout.addRow('Department', self.dept_combobox)

        self.lang_combobox = QtWidgets.QComboBox()
        self.acc_layout.addRow('Language', self.lang_combobox)

        # ----- bottom
        self.bottom_layout = QtWidgets.QHBoxLayout()
        self.main_layout.addLayout(self.bottom_layout)

        spacerItem3 = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.bottom_layout.addItem(spacerItem3)

        self.submit_button = QtWidgets.QPushButton('Submit')
        self.submit_button.setMinimumSize(QtCore.QSize(65, 30))
        self.submit_button.setIcon(QtGui.QIcon(self.add_icon))
        self.submit_button.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.bottom_layout.addWidget(self.submit_button)

        # ----- status line
        self.statusBar = QtWidgets.QStatusBar()
        status_font = QtGui.QFont()
        status_font.setItalic(True)
        self.statusBar.setFont(status_font)
        self.statusBar.setStyleSheet('color: rgb(150, 150, 150)')
        self.main_layout.addWidget(self.statusBar)

        # stretch setup
        self.data_layout.setStretch(0, 0)
        self.data_layout.setStretch(1, 1)
        self.main_layout.setStretch(0, 0)
        self.main_layout.setStretch(1, 0)
        self.main_layout.setStretch(2, 0)
        self.main_layout.setStretch(3, 1)

    def set_default(self):
        self.reset_ui()
        self.reset_input_color()
        self.statusBar.showMessage('Please add a photo and fill the forms.')

        # department combobox
        departments = sorted(sg_user.Config.department_mapping.keys())
        self.dept_combobox.addItems(departments)
        # languages
        self.lang_combobox.addItems(['Thai', 'English'])

    def init_signals(self):
        self.clear_button.clicked.connect(self.reset_ui)
        self.submit_button.clicked.connect(self.submit)

    def reset_ui(self):
        self.photo_frame.update_photo(self.unknown_photo)
        self.engName_lineEdit.clear()
        self.engLastName_lineEdit.clear()
        self.engNickName_lineEdit.clear()
        self.thaiName_lineEdit.clear()
        self.thaiLastName_lineEdit.clear()
        self.thaiNickName_lineEdit.clear()
        self.id_lineEdit.clear()

    def reset_input_color(self):
        for ui in (self.engName_lineEdit, self.engLastName_lineEdit, self.engNickName_lineEdit, self.thaiName_lineEdit, self.thaiLastName_lineEdit, self.thaiNickName_lineEdit, self.id_lineEdit):
            ui.setStyleSheet('')

    def check_inputs(self):
        self.reset_input_color()
        err = []
        # find empty fields
        for ui in (self.engName_lineEdit, self.engLastName_lineEdit, self.engNickName_lineEdit, self.thaiName_lineEdit, self.thaiLastName_lineEdit, self.thaiNickName_lineEdit, self.id_lineEdit):
            if not ui.text():
                ui.setStyleSheet('background-color: rgb{}'.format(self.error_color))
                label = ui.parent().layout().labelForField(ui)
                err.append(label.text() + ' field is empty')

        # check employee ID digit count
        employee_id = self.id_lineEdit.text()
        if len(employee_id) != 8:
            self.id_lineEdit.setStyleSheet('background-color: rgb{}'.format(self.error_color))
            err.append('Employee ID needs 8 digits')

        # check for existing name
        nickname = self.engNickName_lineEdit.text()
        existing_names = [u.get('sg_name').lower() for u in user_info.SGUser().userEntities]
        print(nickname.lower(), existing_names)
        if nickname.lower() in existing_names:
            self.engNickName_lineEdit.setStyleSheet('background-color: rgb{}'.format(self.error_color))
            err.append('The nickname "{}" has already been used'.format(nickname))

        return err

    def submit(self):
        # check inputs
        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
        self.setEnabled(False)
        self.statusBar.showMessage('Checking user inputs...')
        err = self.check_inputs()
        QtWidgets.QApplication.restoreOverrideCursor()
        self.setEnabled(True)
        if err:
            self.statusBar.showMessage('Please check your inputs.')
            err_text = '\n'.join(err)
            self.show_msg_dialog(err_text, title='Input Error', icon='critical')
            return

        # get all inputs
        eng_name = self.engName_lineEdit.text()
        eng_lastname = self.engLastName_lineEdit.text()
        eng_nickname = self.engNickName_lineEdit.text()
        thai_name = self.thaiName_lineEdit.text()
        thai_lastname = self.thaiLastName_lineEdit.text()
        thai_nickname = self.thaiNickName_lineEdit.text()
        employee_id = int(self.id_lineEdit.text())
        department = self.dept_combobox.currentText()
        language = self.lang_combobox.currentText()
        func_args = {'nickname': eng_nickname, 
                    'local_nickname': thai_nickname, 
                    'english_name': eng_name, 
                    'english_lastname': eng_lastname, 
                    'local_name': thai_name, 
                    'local_lastname': thai_lastname, 
                    'department_name': department, 
                    'employee_id': employee_id, 
                    'language': language}

        self.statusBar.showMessage('Please make sure the informations are correct.')
        confirm = self.show_confirm_dialog(func_args)
        if confirm == QtWidgets.QMessageBox.AcceptRole:
            self.statusBar.showMessage('Creating new user...')
            QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
            self.setEnabled(False)
            print 'Args', func_args
            
            # save photo to file
            if self.photo_frame.has_photo():
                photo_path = self.photo_frame.to_file()

            worker = thread_pool.Worker(partial(self.thread_create_user, func_args, photo_path))
            worker.signals.result.connect(self.create_user_finished)
            self.threadpool.start(worker)

    def thread_create_user(self, func_args, photo_path=''):
        # create user
        user_result = None
        try:
            user_result = sg_user.create(**func_args)
            print 'User Creation', user_result

            # upload photo
            if user_result and photo_path:
                photo_result = sg_process.update_entity_thumbnail('HumanUser', user_result.get('id'), photo_path)
                print 'Photo Upload', photo_result
        except Exception, e:
            print e
        return user_result
    
    def create_user_finished(self, result):
        if result:
            # confirm user
            finish_txt = 'New SG user has been created\n'
            finish_txt += '\n  SG name: {}'.format(result.get('name'))
            finish_txt += '\n  entity ID: {}\n'.format(result.get('id'))
            self.show_msg_dialog(finish_txt, title='Finished', icon='information')
            self.statusBar.showMessage('Creation success.')
            self.reset_ui()
        else:
            err_txt = 'Error creating SG user. \nPlease contact admin.'
            self.show_msg_dialog(err_txt, title='Error', icon='critical')
            self.statusBar.showMessage('Creation failed.')

        self.setEnabled(True)
        QtWidgets.QApplication.restoreOverrideCursor()

    def show_confirm_dialog(self, func_args):
        text = u'ชื่อ ' + func_args.get('local_name') + ' ' + func_args.get('local_lastname') + ' (' + func_args.get('local_nickname') + ')'
        text += '\nUser: {}'.format(func_args.get('nickname'))
        text += '\nFull name: {} {}'.format(func_args.get('english_name'), func_args.get('english_lastname'))
        text += '\nE-mail: {}.{}{}'.format(func_args.get('english_name').lower(), func_args.get('english_lastname').lower()[0], sg_user.Config.email_domain)
        text += '\nEmployee ID: {}'.format(func_args.get('employee_id'))
        text += '\nDepartment: {} ({})'.format(func_args.get('department_name'), sg_user.Config.department_mapping[func_args.get('department_name')].get('groups'))
        text += '\nLanguage: {}'.format(func_args.get('language'))
        text += '\n\nCreate SG user?'

        qmsgBox = QtWidgets.QMessageBox(self)
        qmsgBox.setWindowIcon(QtGui.QIcon(self.app_icon))
        qmsgBox.setWindowTitle('Confirm')
        qmsgBox.setIcon(QtWidgets.QMessageBox.Information)
        qmsgBox.setText(text)
        yes_button = qmsgBox.addButton('  Yes  ', QtWidgets.QMessageBox.AcceptRole)
        no_button = qmsgBox.addButton('    No    ', QtWidgets.QMessageBox.RejectRole)
        result = qmsgBox.exec_()
        return result

    def show_msg_dialog(self, text, title='Error', icon='information'):
        icons = {'information': QtWidgets.QMessageBox.Information, 
                'warning': QtWidgets.QMessageBox.Warning,
                'critical': QtWidgets.QMessageBox.Critical}

        qmsgBox = QtWidgets.QMessageBox(self)
        qmsgBox.setWindowIcon(QtGui.QIcon(self.app_icon))
        qmsgBox.setWindowTitle(title)
        qmsgBox.setIcon(icons[icon])
        qmsgBox.setText(text)
        ok_button = qmsgBox.addButton('    OK    ', QtWidgets.QMessageBox.AcceptRole)
        result = qmsgBox.exec_()
        return result

def show():
    app = QtWidgets.QApplication(sys.argv)
    myApp = SGUserCreator()
    myApp.show()
    stylesheet.set_default(app)
    sys.exit(app.exec_())

if __name__ == '__main__':
    show()
