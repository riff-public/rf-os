#!/usr/bin/env python
# -- coding: utf-8 --

import os
import sys
import traceback
from collections import OrderedDict

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

module_path = sys.modules[__name__].__file__
module_dir = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_utils.widget import project_widget
from rf_utils.widget import entity_browse_widget
from rf_utils.widget import file_widget
from rf_utils.widget import completer
from rf_utils import icon
sg = None

class Setting:
    refreshIcon = '{}/icons/refresh.png'.format(module_dir)
    appIcon = '{}/icons/app_icon.png'.format(module_dir)
    addIcon = '{}/icons/add_icon.png'.format(module_dir)

class AddShotWindow(QtWidgets.QMainWindow):
    user_confirmed = QtCore.Signal(list)

    def __init__(self, parent=None):
        #Setup Window
        super(AddShotWindow, self).__init__(parent)
        self.w = 285
        self.h = 180

        self.setupUi()
        self.init_signal()

    def setupUi(self):
        self.setWindowModality(QtCore.Qt.ApplicationModal)
        self.setWindowTitle('Add Shot')
        self.setWindowIcon(QtGui.QIcon(Setting.appIcon))
        self.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))

        self.centralwidget = QtWidgets.QWidget(self)

        # main layout
        self.main_layout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.main_layout.setSpacing(5)

        # template
        self.template_layout = QtWidgets.QVBoxLayout()
        self.main_layout.addLayout(self.template_layout)
        self.template_label = QtWidgets.QLabel('Select template to use for new shot')
        # self.template_label.setAlignment(QtCore.Qt.AlignCenter)
        self.template_layout.addWidget(self.template_label)

        self.template_tab_widget = QtWidgets.QTabWidget()
        self.template_layout.addWidget(self.template_tab_widget)

        # ep
        self.ep_layout = QtWidgets.QHBoxLayout()
        self.main_layout.addLayout(self.ep_layout)

        self.ep_label = QtWidgets.QLabel('Episode: ')
        self.ep_layout.addWidget(self.ep_label)

        self.ep_combobox = QtWidgets.QComboBox()
        self.ep_combobox.setEditable(True)
        self.ep_combobox.setSizePolicy(QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum))
        self.searchCompleter = completer.CustomQCompleter()
        self.searchCompleter.setCompletionMode(QtWidgets.QCompleter.UnfilteredPopupCompletion)
        self.searchCompleter.setCaseSensitivity(QtCore.Qt.CaseSensitive)
        self.searchCompleter.setModel(self.ep_combobox.model())
        self.ep_combobox.setCompleter(self.searchCompleter)
        self.ep_combobox.setInsertPolicy(QtWidgets.QComboBox.NoInsert) 
        self.ep_layout.addWidget(self.ep_combobox)

        # shot
        self.shotname_layout = QtWidgets.QHBoxLayout()
        self.main_layout.addLayout(self.shotname_layout)

        self.shotname_label = QtWidgets.QLabel('Shot: ')
        self.shotname_layout.addWidget(self.shotname_label)
        self.shotname_lineedit = QtWidgets.QLineEdit()
        self.shotname_lineedit.setPlaceholderText('Example: 10, 20, 50-60')
        self.shotname_layout.addWidget(self.shotname_lineedit)

        # increment
        self.inc_layout = QtWidgets.QHBoxLayout()
        self.main_layout.addLayout(self.inc_layout)

        self.inc_label = QtWidgets.QLabel('Increment by: ')
        self.inc_layout.addWidget(self.inc_label)

        self.inc_spinBox = QtWidgets.QSpinBox()
        self.inc_spinBox.setButtonSymbols(QtWidgets.QAbstractSpinBox.PlusMinus)
        self.inc_spinBox.setMinimum(1)
        self.inc_spinBox.setSingleStep(10)
        self.inc_spinBox.setValue(10)
        self.inc_layout.addWidget(self.inc_spinBox)

        spacerItem = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.inc_layout.addItem(spacerItem)
        
        # buttons
        self.buttonBox = QtWidgets.QDialogButtonBox(QtCore.Qt.Horizontal)
        ok_button = QtWidgets.QPushButton('  OK  ')
        cancel_button = QtWidgets.QPushButton('Cancel')
        self.buttonBox.addButton(ok_button, QtWidgets.QDialogButtonBox.AcceptRole)
        self.buttonBox.addButton(cancel_button, QtWidgets.QDialogButtonBox.RejectRole)
        self.main_layout.addWidget(self.buttonBox)

        self.setCentralWidget(self.centralwidget)
        self.resize(self.w, self.h)

    def init_signal(self):
        # button signals
        self.buttonBox.rejected.connect(self.close)

    def populate_templates(self, templates):
        for app, files in templates.iteritems():
            template_listwidget = file_widget.FileListWidget()
            template_listwidget.add_files(files)
            template_listwidget.listWidget.setCurrentRow(0)
            self.template_tab_widget.addTab(template_listwidget, app)

    def get_selected_templates(self):
        results = OrderedDict()
        for i in xrange(self.template_tab_widget.count()):
            template_listwidget = self.template_tab_widget.widget(i)
            app = self.template_tab_widget.tabText(i)
            results[app] = template_listwidget.selected_file()
        return results

    def select_template(self, templates):
        for i in xrange(self.template_tab_widget.count()):
            app = self.template_tab_widget.tabText(i)
            template_listwidget = self.template_tab_widget.widget(i)
            all_files = template_listwidget.get_all_files()
            if app in templates and templates[app] in all_files:
                # select the file
                index = all_files.index(templates[app])
                template_listwidget.listWidget.setCurrentRow(index)
                
class VfxManagerUi(QtWidgets.QWidget):

    def __init__(self, parent=None):
        super(VfxManagerUi, self).__init__(parent)
        self.layout = QtWidgets.QVBoxLayout()
        self.layout.setSpacing(1)
        self.layout.setContentsMargins(5, 5, 5, 5)
        self.setLayout(self.layout)

        self.setup_widget()
        # self.init_signals()

    def setup_widget(self):
        # logo
        self.logo_layout = QtWidgets.QHBoxLayout()
        self.logo_layout.setContentsMargins(5, 5, 5, 5)
        self.layout.addLayout(self.logo_layout)
        self.logo = QtWidgets.QLabel()
        self.logo.setPixmap(QtGui.QPixmap(icon.logo).scaled(64, 64, QtCore.Qt.KeepAspectRatio))
        self.logo_layout.addWidget(self.logo)

        # navigation bar
        self.nav_layout = QtWidgets.QHBoxLayout()
        self.nav_layout.setContentsMargins(10, 0, 10, 0)
        self.project_widget = project_widget.SGProjectComboBox(sg=sg, layout=QtWidgets.QHBoxLayout())
        self.nav_layout.addWidget(self.project_widget)
        self.layout.addLayout(self.nav_layout)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.nav_layout.addItem(spacerItem)

        # refresh
        self.refresh_button = QtWidgets.QPushButton()
        self.refresh_button.setMaximumSize(32, 32)
        self.refresh_button.setIcon(QtGui.QIcon(Setting.refreshIcon))
        self.nav_layout.addWidget(self.refresh_button)

        # sub layout
        self.sub_layout = QtWidgets.QHBoxLayout()
        self.sub_layout.setContentsMargins(0, 0, 0, 0)
        self.layout.addLayout(self.sub_layout)

        # setup splitter
        self.mainSplitter = QtWidgets.QSplitter()
        self.mainSplitter.setOrientation(QtCore.Qt.Horizontal)

        self.shotSplitWidget = QtWidgets.QWidget(self.mainSplitter)
        self.shotSplitWidget.setMinimumWidth(150)
        self.shotSplitWidget.setMaximumWidth(400)
        self.tabSplitWidget = QtWidgets.QWidget(self.mainSplitter)
        self.mainSplitter.setSizes([155, 645])
        self.sub_layout.addWidget(self.mainSplitter)

        # ------------ SHOT
        self.shotnav_layout = QtWidgets.QVBoxLayout(self.shotSplitWidget)


        # EP
        self.ep_layout = QtWidgets.QVBoxLayout()
        self.ep_layout.setSpacing(0)
        self.ep_layout.setContentsMargins(0, 0, 0, 0)
        self.shotnav_layout.addLayout(self.ep_layout)

        # button
        self.entity_button_layout = QtWidgets.QHBoxLayout()
        self.entity_button_layout.setContentsMargins(0, 0, 0, 0)
        self.ep_layout.addLayout(self.entity_button_layout)

        self.ep_label = QtWidgets.QLabel('Episode')
        self.entity_button_layout.addWidget(self.ep_label)

        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.entity_button_layout.addItem(spacerItem)

        self.add_shot_button = QtWidgets.QPushButton()
        self.add_shot_button.setMaximumSize(24, 24)
        self.add_shot_button.setIcon(QtGui.QIcon(Setting.addIcon))
        self.entity_button_layout.addWidget(self.add_shot_button)
        
        self.ep_listwidget = QtWidgets.QListWidget()
        self.ep_layout.addWidget(self.ep_listwidget)

        # Shot
        self.shot_layout = QtWidgets.QVBoxLayout()
        self.shotnav_layout.addLayout(self.shot_layout)
        self.shot_label = QtWidgets.QLabel('Shot')
        self.shot_layout.addWidget(self.shot_label)
        self.shot_listwidget = QtWidgets.QListWidget()
        self.shot_layout.addWidget(self.shot_listwidget)

        self.shotnav_layout.setStretch(0, 1)
        self.shotnav_layout.setStretch(1, 2)

        # ------------ TAB
        self.tab_layout = QtWidgets.QHBoxLayout(self.tabSplitWidget)

        # status bar
        self.statusbar = QtWidgets.QStatusBar()
        self.layout.addWidget(self.statusbar)

        self.layout.setStretch(0, 1)
        self.layout.setStretch(1, 1)
        self.layout.setStretch(2, 8)
        # self.sub_layout.setStretch(2, 4)

        # set tool tip
        self.project_widget.setToolTip('Select a project here')
        self.refresh_button.setToolTip('Refresh current tab')
        self.add_shot_button.setToolTip('Add a new shot to this project')
        



