#!/usr/bin/env python
# -- coding: utf-8 --

import os
import sys
import subprocess
from collections import OrderedDict
from datetime import datetime
from functools import partial
from copy import deepcopy
import time
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from Qt import QtCore
from Qt import QtGui
from Qt import QtWidgets

import rf_config as config
from rf_utils.widget import media_widget
from rf_utils.widget import file_widget
from rf_utils.widget import textEdit
from rf_utils.widget import status_widget
from rf_utils.pipeline import watermark
from rf_utils.sg import sg_process
from rf_utils import user_info
from rf_utils import file_utils
from rf_utils import thread_pool
from . import master


modulePath = sys.modules[__name__].__file__.replace('\\', '/')
moduleDir  = os.path.dirname(modulePath)
appModuleDir = os.path.dirname(moduleDir)

# ** TODO
# // -make popup window with status and description to fill/select
# // -move SG process to new thread
# // -file copy
# //-add HUD, watermark


class PublishWindow(QtWidgets.QMainWindow):
    user_confirmed = QtCore.Signal(list)

    def __init__(self, parent=None):
        #Setup Window
        super(PublishWindow, self).__init__(parent)
        self.w = 280
        self.h = 200
        self.text_color = QtGui.QColor(QtCore.Qt.green)

        self.setupUi()
        self.init_signal()

    def setupUi(self):
        self.setWindowModality(QtCore.Qt.ApplicationModal)
        self.setWindowTitle('Publish Render')
        self.setWindowIcon(QtGui.QIcon('{}/icons/app_icon.png'.format(appModuleDir)))
        self.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))

        self.centralwidget = QtWidgets.QWidget(self)

        # main layout
        self.main_layout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.main_layout.setSpacing(5)

        # text
        self.text_label = QtWidgets.QLabel()
        self.text_label.setAlignment(QtCore.Qt.AlignCenter)
        self.text_label.setStyleSheet('color: rgb({}, {}, {})'.format(self.text_color.red(), self.text_color.green(), self.text_color.blue()))

        self.main_layout.addWidget(self.text_label)

        # status
        self.status_widget = status_widget.TaskStatusWidget()
        self.status_widget.label.setText('Status')
        self.status_widget.allLayout.addItem(QtWidgets.QSpacerItem(25, 25, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum))
        self.status_widget.allLayout.setStretch(2, 5)
        self.main_layout.addWidget(self.status_widget)

        # description
        self.description_textEdit = textEdit.TextEditWithPlaceHolder()
        self.description_textEdit.setPlaceholderText('< Say something about this publish >')
        self.description_textEdit.setWordWrapMode(QtGui.QTextOption.WrapAtWordBoundaryOrAnywhere)
        self.description_textEdit.setTextInteractionFlags(QtCore.Qt.TextEditorInteraction)
        self.main_layout.addWidget(self.description_textEdit)

        # buttons
        self.buttonBox = QtWidgets.QDialogButtonBox(QtCore.Qt.Horizontal)
        ok_button = QtWidgets.QPushButton('  OK  ')
        cancel_button = QtWidgets.QPushButton('Cancel')
        self.buttonBox.addButton(ok_button, QtWidgets.QDialogButtonBox.AcceptRole)
        self.buttonBox.addButton(cancel_button, QtWidgets.QDialogButtonBox.RejectRole)
        self.main_layout.addWidget(self.buttonBox)

        self.setCentralWidget(self.centralwidget)
        self.resize(self.w, self.h)

    def init_signal(self):
        # button signals
        self.buttonBox.rejected.connect(self.close)

    def get_status(self):
        return self.status_widget.current_item()

    def get_description(self):
        return self.description_textEdit.toPlainText()

class Ui(master.Ui):
    """docstring for Ui"""
    def __init__(self, thread=None, parent=None):
        super(Ui, self).__init__(thread=thread, parent=parent)
        self.media_type = 'mov'
        self.entityType = 'Shot'
        self.task = 'comp'
        self.step = 'comp'
        self.thread_pool = None
        self._poster_weight = 0.2
        self._queue_num = 0

        self.nopreivew_path = '{}/icons/nopreview_icon.png'.format(appModuleDir)
        self.uploadicon_path = '{}/icons/upload.gif'.format(appModuleDir)

        self.setupUi()
        self.init_signal()
        self.set_default()

    def setupUi(self):
        # --- media layout
        self.media_layout = QtWidgets.QVBoxLayout()
        self.main_layout.addLayout(self.media_layout)
        # media viewver
        self.preview_label = media_widget.PreviewMedia()
        self.media_layout.addWidget(self.preview_label)

        # split
        self.split_layout = QtWidgets.QHBoxLayout()
        self.media_layout.addLayout(self.split_layout)
    
        # file selection
        self.file_listWidget = file_widget.FileListWidget()
        self.split_layout.addWidget(self.file_listWidget)
        
        # --- buttons layout
        self.button_layout = QtWidgets.QVBoxLayout()
        self.button_layout.setSpacing(5)
        self.button_layout.setContentsMargins(3, 0, 3, 0)
        self.split_layout.addLayout(self.button_layout)
        # view button
        self.play_button = QtWidgets.QPushButton('')
        self.play_button.setFixedSize(QtCore.QSize(55, 22))
        self.play_button.setIcon(QtGui.QIcon('{}/icons/play_icon.png'.format(appModuleDir)))
        self.button_layout.addWidget(self.play_button)

        # publish button
        self.publish_button = QtWidgets.QPushButton('')
        self.publish_button.setFixedSize(QtCore.QSize(55, 22))
        self.publish_button.setIcon(QtGui.QIcon('{}/icons/upload_icon.png'.format(appModuleDir)))
        self.button_layout.addWidget(self.publish_button)

        # spacer
        spacerItem1 = QtWidgets.QSpacerItem(25, 25, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.button_layout.addItem(spacerItem1)

        # load icon
        self.load_layout = QtWidgets.QVBoxLayout()
        self.load_layout.setSpacing(2)
        self.button_layout.addLayout(self.load_layout)

        self.load_label = QtWidgets.QLabel('')
        self.load_label.setFixedSize(QtCore.QSize(55, 30))
        self.load_label.setAlignment(QtCore.Qt.AlignCenter)
        self.load_layout.addWidget(self.load_label)

        self.progress_label = QtWidgets.QLabel('')
        self.progress_label.setStyleSheet("QLabel {color: yellow;}")
        self.progress_label.setAlignment(QtCore.Qt.AlignCenter)
        self.load_layout.addWidget(self.progress_label)

        # set stretch
        self.media_layout.setStretch(0, 3)
        self.media_layout.setStretch(1, 1)

        # set toolTip
        self.play_button.setToolTip('Play in media player')
        self.file_listWidget.setToolTip('Media files for this shot, double-click to play')
        self.publish_button.setToolTip('Publish this media')

    def refresh(self, entity): 
        super(Ui, self).refresh(entity)
        self.file_listWidget.clear()
        self.clear_view()
        if not entity:
            return

        # set FPS
        fps = self.entity.projectInfo.render.fps()
        self.preview_label.fps = fps
        self.preview_label.updateInfo()

        # find mov
        sequence_path = entity.path.sequence()
        if sequence_path:
            mov_dir_path = '{}/{}'.format(sequence_path.combine_path, self.media_type)
            if os.path.exists(mov_dir_path):
                files = []
                for f in sorted(os.listdir(mov_dir_path)):
                    fp = '{}/{}'.format(mov_dir_path, f)
                    if f.startswith(entity.shotname):
                        files.append(fp)
                if files:
                    self.file_listWidget.add_files(files)
                    self.file_listWidget.listWidget.setCurrentRow(len(files)-1)  # select the last file in the list

    def set_default(self):
        self.preview_label.loadMedia(self.nopreivew_path)
        # set to float between 0-1 to load single frame on first load, to load full, user must hit Play
        self.preview_label.poster_frame_weight = self._poster_weight 

    def show_uploading(self):
        # set text
        self.progress_label.setText('{} file(s)'.format(self._queue_num))

        if not self.load_label.movie():
            # setup qmovie
            self.load_movie = QtGui.QMovie(self.uploadicon_path)
            self.load_label.setMovie(self.load_movie)
            self.load_movie.start()
            # resize
            rect = self.load_label.frameRect()
            size = QtCore.QSize(rect.width()*0.5, rect.width()*0.5)
            self.load_movie.setScaledSize(size)

    def stop_uploading(self):
        self.progress_label.setText('')
        self.load_label.setMovie(None)

    def init_signal(self):
        self.file_listWidget.itemSelected.connect(self.update_view)
        self.play_button.clicked.connect(self.play_media)
        self.file_listWidget.listWidget.itemDoubleClicked.connect(self.play_media)
        self.publish_button.clicked.connect(self.publish_pressed)
        
        self.preview_label.mediaLoading.connect(partial(self.parent.showStatusMessage, 'Loading media...'))
        self.preview_label.mediaLoadFinished.connect(partial(self.parent.showStatusMessage, 'Media loaded.'))

    def play_media(self):
        selected_file = self.file_listWidget.selected_file()
        if not selected_file:
            return
        if os.path.exists(selected_file):
            os.startfile(selected_file)

    def publish_pressed(self):
        filepath = self.file_listWidget.selected_file()
        if not filepath:
            return

        self.publish_win = PublishWindow(parent=self)
        self.publish_win.text_label.setText(os.path.basename(filepath))

        # signal
        entity = deepcopy(self.entity)  # need a copy of entity in case self.entity changes when thread has not yet to start
        self.publish_win.buttonBox.accepted.connect(lambda: self.start_publish(filepath, entity, self.entityType, self.task, self.step))
        self.publish_win.show()

    def start_publish(self, filepath, entity, entityType, task, step):
        # get data from popup win
        status = self.publish_win.get_status()
        description = self.publish_win.get_description()
        self.publish_win.close()

        # start new thrad to publish SG
        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
        worker = thread_pool.Worker(self.sg_publish, filepath, status, description, entity, entityType, task, step)
        worker.signals.result.connect(self.upload_finished)
        self.threadpool.start(worker)

        # show loading
        self._queue_num += 1
        self.show_uploading()

        # copy file
        self.publish_file(filepath)

    def upload_finished(self, entity_name, *args, **kwargs):
        if self._queue_num - 1 == 0:
            self.stop_uploading()
        else:
            self._queue_num -= 1
            self.show_uploading()

    def sg_publish(self, filepath, status, description, entity, entityType, task, step):
        basename = os.path.basename(filepath)
        filename, ext = os.path.splitext(basename)
        project = entity.project.name()
        entityName = entity.shotname
        user = user_info.User().sg_user()

        # create version
        versionEntity = sg_process.create_version(filename, project, entityType, entityName, task, step, user, sg_status_list=status, sg_path_to_movie=filepath)
            
        # add HUD, watermark to media to be uploaded
        # date
        pub_time = datetime.now().strftime('%d %b %Y %H.%M.%S')
        date_text = "Date: {}".format(pub_time)
        # user
        user_text = "User: {}".format(user['name'])
        # shot name
        shot_text = "Shot: {}".format(entityName)
        # size
        outputWidth, outputHeight = entity.project.render.final_outputH()
        # add HUD
        filepath_hud = watermark.add_border_hud_to_mov(input_path=filepath, 
                                                output_path='',  # write to temp
                                                topLeft=[project],
                                                topRight=[date_text],
                                                bottomLeft=[user_text],
                                                bottomMid=[shot_text],
                                                frameHud='bottomRight',
                                                canvas=[outputWidth, outputHeight])
        filepath_tmp, watermark_result = watermark.add_watermark_to_media(entity=entity, input_path=filepath_hud)

        # upload movie
        mediaResult = sg_process.sg.upload('Version', versionEntity['id'], filepath_tmp, 'sg_uploaded_movie')
        data = {'sg_path_to_movie': filepath.replace('/', '\\'), 'description': description}
        sg_process.sg.update('Version', versionEntity.get('id'), data)

        # update task status
        projectEntity = sg_process.get_project_entity(project)
        sg_entity = sg_process.get_entity(projectEntity, entityType, entityName)
        stepEntity = sg_process.get_step_entity(step, entityType)
        taskEntity = sg_process.get_task_entity(projectEntity, sg_entity, stepEntity, task)
        sg_process.set_task_status(taskEntity['id'], status)

        # remove temp
        os.remove(filepath_hud)
        os.remove(filepath_tmp)

        return entityName

    def publish_file(self, filepath):
        hero_dir = self.entity.path.heropath().combine_path
        hero_filename = self.entity.hero_filename(ext=self.media_type)
        dest_path = '{}/{}'.format(hero_dir, hero_filename)
        if not os.path.exists(hero_dir):
            os.makedirs(hero_dir)

        # copy to destination
        file_utils.xcopy_file(filepath, dest_path)

        QtWidgets.QApplication.restoreOverrideCursor()

        # popup to confirm success
        qmsgBox = QtWidgets.QMessageBox(self)
        qmsgBox.setText('Please wait for SG to upload in a moment.\n{}'.format(dest_path))
        qmsgBox.setWindowTitle('Publish success')
        qmsgBox.setIcon(QtWidgets.QMessageBox.Information)
        qmsgBox.addButton('  OK  ', QtWidgets.QMessageBox.AcceptRole)
        qmsgBox.exec_()

    def update_view(self, media_path):
        # load video
        if os.path.exists(media_path):
            self.preview_label.loadMedia([media_path])
        else:
            self.clear_view()

    def clear_view(self):
        self.preview_label.mediaplayer.clear()
        self.preview_label.clear_view()
