#!/usr/bin/env python
# -- coding: utf-8 --

import os
import sys
import subprocess
import re
from collections import OrderedDict
from datetime import datetime
from functools import partial
from glob import glob
import time
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from Qt import QtCore
from Qt import QtGui
from Qt import QtWidgets

import rf_config as config
from rf_utils.widget import file_widget
from rf_utils.sg import sg_process
from . import master


modulePath = sys.modules[__name__].__file__.replace('\\', '/')
moduleDir  = os.path.dirname(modulePath)
appModuleDir = os.path.dirname(moduleDir)

# ** TODO
# //-Add support for AE version detection
# //-find a way to launch AE and open .aep from subprocess

class Ui(master.Ui):
    """docstring for Ui"""
    def __init__(self, thread=None, parent=None):
        super(Ui, self).__init__(thread=thread, parent=parent)
        self.ae_versions = OrderedDict()
        self.proxy_model = None

        self.setupUi()
        self.init_signal()
        self.set_default()

    def setupUi(self):
        self.split_layout = QtWidgets.QHBoxLayout()
        self.main_layout.addLayout(self.split_layout)
    
        # file list view
        # self.file_listWidget = file_widget.FileListWidget()
        self.file_treeview = QtWidgets.QTreeView(self)
        self.file_treeview.setSortingEnabled(True)
        
        self.split_layout.addWidget(self.file_treeview)

        # buttons
        self.button_layout = QtWidgets.QVBoxLayout()
        self.button_layout.setSpacing(6)
        self.split_layout.addLayout(self.button_layout)

        self.open_layout = QtWidgets.QVBoxLayout()
        self.open_layout.setSpacing(3)
        self.button_layout.addLayout(self.open_layout)

        # version
        self.version_layout = QtWidgets.QHBoxLayout()
        self.open_layout.addLayout(self.version_layout)
        self.version_label = QtWidgets.QLabel('Version:')
        self.version_layout.addWidget(self.version_label)
        self.version_combobox = QtWidgets.QComboBox()
        self.version_combobox.setMinimumWidth(75)
        self.version_layout.addWidget(self.version_combobox)

        # open button
        self.open_button = QtWidgets.QPushButton('Open')
        self.open_button.setIcon(QtGui.QIcon('{}/icons/open_icon.png'.format(appModuleDir)))
        self.open_layout.addWidget(self.open_button)

        # dir button
        self.dir_button = QtWidgets.QPushButton('Location')
        self.dir_button.setIcon(QtGui.QIcon('{}/icons/folder_icon.png'.format(appModuleDir)))
        self.button_layout.addWidget(self.dir_button)

        spacerItem1 = QtWidgets.QSpacerItem(25, 25, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.button_layout.addItem(spacerItem1)

        self.split_layout.setStretch(0, 9)
        self.split_layout.setStretch(1, 2)

        # set toolTip
        self.open_button.setToolTip('Open in After Effects')

        # right clicked
        self.file_treeview.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)

        # set file view width
        self.set_file_view_width()

    def set_file_view_width(self):
        # set tree column width
        self.file_treeview.setColumnWidth(0, 300)
        self.file_treeview.setColumnWidth(1, 60)
        self.file_treeview.setColumnWidth(2, 60)

    def refresh(self, entity): 
        super(Ui, self).refresh(entity)
        app_path = self.entity.path.sequence().combine_path
        # set filter to only see file from this shot
        self.init_file_model(path=app_path)
        self.fileModel.setNameFilters(['{}*.aep'.format(entity.shotname)])

    def init_file_model(self, path):
        if not os.path.isdir(path):
            return 
        if self.proxy_model is None:
            self.proxy_model = QtCore.QSortFilterProxyModel(self)
        else:
            # remove the current source model
            self.proxy_model.setSourceModel(None)
        self.fileModel = QtWidgets.QFileSystemModel()
        self.fileModel.setNameFilterDisables(False)
        self.fileModel.setFilter(QtCore.QDir.NoDotAndDotDot | QtCore.QDir.Files)
        self.proxy_model.setSourceModel(self.fileModel)
        self.file_treeview.setModel(self.proxy_model)
        self.fileModel.directoryLoaded.connect(self.load_directory)
        self.fileModel.setRootPath(path)

    def load_directory(self):
        path = self.fileModel.rootPath()
        source_index = self.fileModel.index(path)
        index = self.proxy_model.mapFromSource(source_index)
        self.file_treeview.setRootIndex(index)
        self.set_file_view_width()

    def set_default(self):
        self.find_AE_version()
        self.init_file_model(path=os.environ['USERPROFILE'])

    def find_AE_version(self):
        config_path = config.Software.data['AfterEffects']['path']
        search_str = config_path.format(version='[0-9][0-9][0-9][0-9]')
        ae_paths = glob(search_str)
        # ae_paths = ['C:/Program Files/Adobe/Adobe After Effects 2021/Support Files/AfterFX.exe', 'C:/Program Files/Adobe/Adobe After Effects 2019/Support Files/AfterFX.exe']
        regex = re.compile(config_path.format(version='([0-9][0-9][0-9][0-9])'))
        self.ae_versions = OrderedDict()
        for path in ae_paths:
            path = path.replace('\\', '/')
            match = re.match(regex, path)
            if match:
                v_str = match.group(1)
                self.ae_versions[v_str] = path
        self.version_combobox.addItems(self.ae_versions.keys())

    def init_signal(self):
        self.open_button.clicked.connect(self.open_clicked)
        self.dir_button.clicked.connect(self.open_directory)
        self.file_treeview.doubleClicked.connect(self.file_double_clicked)
        self.file_treeview.customContextMenuRequested.connect(self.file_treeview_right_clicked)

    def file_treeview_right_clicked(self, pos):
        rightClickMenu = QtWidgets.QMenu(self)
        opendir_action = QtWidgets.QAction('Open in Explorer', self)
        opendir_action.triggered.connect(self.open_directory)
        rightClickMenu.addAction(opendir_action)
        rightClickMenu.exec_(self.mapToGlobal(pos))

    def open_directory(self):
        directory = self.entity.path.sequence().combine_path
        if os.path.isdir(directory):
            subprocess.Popen('explorer "{}"'.format(directory.replace('/', '\\')))

    def open_clicked(self):
        source_index = self.file_treeview.currentIndex()
        index = self.proxy_model.mapToSource(source_index)
        path = self.fileModel.filePath(index)
        self.open_file(path)

    def file_double_clicked(self, source_index):
        index = self.proxy_model.mapToSource(source_index)
        path = self.fileModel.filePath(index)
        self.open_file(path)

    def open_file(self, path):
        if not path or not os.path.exists(path):
            return

        if self.ae_versions:
            selected_version = self.version_combobox.currentText()
            subprocess.Popen('"{}" "{}"'.format(self.ae_versions[selected_version], path))
        else:
            # popup to confirm success
            qmsgBox = QtWidgets.QMessageBox(self)
            qmsgBox.setText('No Adobe After Effects found, open using Windows default?')
            qmsgBox.setWindowTitle('Warning')
            qmsgBox.setIcon(QtWidgets.QMessageBox.Warning)
            qmsgBox.addButton('  Yes  ', QtWidgets.QMessageBox.AcceptRole)
            qmsgBox.addButton('  No  ', QtWidgets.QMessageBox.RejectRole)
            result = qmsgBox.exec_()
            if result == 0:
                os.startfile(path)

