#!/usr/bin/env python
# -- coding: utf-8 --

# v.0.0.1 polytag switcher
_title = 'VFX Manager'
_version = 'v.beta'
_des = ''
uiName = 'VfxManagerUi'

#Import python modules
import sys
import os
import getpass
import logging
import re
from collections import OrderedDict
from copy import deepcopy

core = '%s/core' % os.environ.get('RFSCRIPT')
sys.path.append(core)

# import config
import rf_config as config
from rf_utils import log_utils
from rf_utils import file_utils
from rf_utils.ui import load
reload(load)
from rf_utils.ui import stylesheet

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)

# QT
os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

# custom function
from . import ui
reload(ui)
from . import tabs
from rf_utils import project_info
from rf_utils import thread_pool
from rf_utils import user_info
from rf_utils.pipeline import create_scene
from rf_utils.sg import sg_process
ui.sg = sg_process.sg
from rf_utils.context import fx_entity
reload(fx_entity)

isNuke = True
try:
    import nuke
    import nukescripts
except ImportError:
    isNuke = False

modulePath = sys.modules[__name__].__file__.replace('\\', '/')
moduleDir  = os.path.dirname(modulePath)

# ** TODO
# //-create new EQ, Shot
# //-Status bar
# //-Add shot now accepts special shot name using double qoute; i.e. "180to220"

class TabConfig:
    """ name of tabs module """
    activeTab = 'render'
    allowedTabs = ['render', 'afterFX']
    tabNameMap = {'afterFX': 'After Effects'}

class AppConfig:
    exception_apps = ['cutting', 'output', 'footage']
    app_templates = {'afterFX': 'template.aep', 'nuke': 'template.nk'}

class VFxManager(QtWidgets.QMainWindow):
    def __init__(self, tab_config=TabConfig, parent=None):
        #Setup Window
        super(VFxManager, self).__init__(parent)

        # vars
        self.shotData = OrderedDict()
        self.entity_type = 'vfx'
        self.step = 'comp'
        self.threadpool = None
        self.previous_shot_selected = {}
        self.previous_template_selection = OrderedDict()

        # ui read
        uiFile = '%s/ui.py' % moduleDir
        self.ui = ui.VfxManagerUi()
        self.w = 925
        self.h = 720
        self.tab_config = tab_config

        self.setup_ui()
        self.init_signals()
        self.set_default()

    def closeEvent(self, event):
        '''
        Closes all tab widgets before actually close itself triggering each tab closeEvents
        '''
        for i in xrange(self.ui.tab_widget.count()):
            widget = self.ui.tab_widget.widget(i)
            widget.close()

    def setup_ui(self):
        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)
        self.resize(self.w, self.h)
        self.setWindowTitle('%s %s %s' % (_title, _version, _des))
        self.setWindowIcon(QtGui.QIcon('{}/icons/app_icon.png'.format(moduleDir)))

        # set tabs
        self.ui.tab_widget = self.add_tab_widget()

    def reset_vars(self):
        # reset vars
        self.previous_shot_selected = {}
        self.previous_template_selection = {}
        self.entity = None

        # reset UI
        self.ui.ep_listwidget.blockSignals(True)
        self.ui.shot_listwidget.blockSignals(True)
        self.ui.ep_listwidget.clear()
        self.ui.shot_listwidget.clear()
        self.ui.ep_listwidget.blockSignals(False)
        self.ui.shot_listwidget.blockSignals(False)

        # refresh current tab
        self.refresh_tab()

    def thread_fetch_data(self, entity): 
        self.reset_vars()

        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
        self.ui.statusbar.showMessage('Fetching new data...')
        worker = thread_pool.Worker(self.get_shot_data, entity)
        worker.signals.result.connect(self.fetch_finished)
        self.threadpool.start(worker)

    def init_signals(self):
        self.ui.project_widget.projectChanged.connect(self.thread_fetch_data)
        self.ui.ep_listwidget.itemSelectionChanged.connect(self.ep_selected)
        self.ui.shot_listwidget.itemSelectionChanged.connect(self.shot_selected)
        self.ui.refresh_button.clicked.connect(self.refresh_tab)
        self.ui.tab_widget.currentChanged.connect(self.tab_changed)
        self.ui.add_shot_button.clicked.connect(self.add_shot_clicked)

    def set_default(self):
        self.ui.project_widget.emit_project_changed()

    def add_shot_clicked(self):
        self.ui.statusbar.showMessage('Add shot requested.')
        # find templates
        step_path = self.entity.path.step().combine_path
        template_paths = OrderedDict()
        for s in os.listdir(step_path):
            app_dir = '{}/{}'.format(step_path, s)
            if os.path.isdir(app_dir) and s in AppConfig.app_templates:
                for f in os.listdir(app_dir):
                    fp = '{}/{}'.format(app_dir, f)
                    if os.path.isfile(fp) and f.endswith(AppConfig.app_templates[s]):
                        if s not in template_paths:
                            template_paths[s] = []
                        template_paths[s].append(fp)

        if template_paths:
            print('----', template_paths)
            self.addshot_win = ui.AddShotWindow(parent=self)

            # add EP to UI
            all_eps = [self.ui.ep_listwidget.item(i).text() for i in range(self.ui.ep_listwidget.count())]
            self.addshot_win.ep_combobox.addItems(all_eps)
            current_ep_item = self.ui.ep_listwidget.currentItem()
            if current_ep_item:
                current_ep = current_ep_item.text()
                self.addshot_win.ep_combobox.setCurrentIndex(all_eps.index(current_ep))

            # add template tab to UI
            self.addshot_win.populate_templates(templates=template_paths)

            # store selection
            if self.previous_template_selection:
                self.addshot_win.select_template(templates=self.previous_template_selection)

            # increment
            self.addshot_win.inc_spinBox.setValue(fx_entity.Config.shot_inc)

            # signal
            self.addshot_win.buttonBox.accepted.connect(lambda: self.do_add_shot())
            # self.addshot_win.ep_combobox.lineEdit().textChanged.connect(lambda: self.add_ui_shotlist())
            self.addshot_win.show()
        else:
            self.ui.statusbar.showMessage('No template found.')

    def do_add_shot(self):
        # collect info from popup UI
        template_files = self.addshot_win.get_selected_templates()
        ep_name = self.addshot_win.ep_combobox.lineEdit().text()
        shotname_input = self.addshot_win.shotname_lineedit.text()
        inc = self.addshot_win.inc_spinBox.value()
        self.addshot_win.close()

        # split shot name input from user
        shotname_str = shotname_input.replace(' ', '')
        shotnums = shotname_str.split(',')
        shotnumbers = []

        for sn in shotnums:
            if sn.startswith('"') and sn.endswith('"'):
                name_input = sn[1:-1]
                valid = re.match("(?:[A-Za-z0-9]+" + r")\Z", name_input, flags=0)
                if valid:
                    shotnumbers.append(name_input)
            elif '-' in sn:
                range_splits = sn.split('-')
                if len(range_splits) == 2 and all([i.isdigit() for i in range_splits[0]+range_splits[1]]):
                    num_range = range(int(range_splits[0]), int(range_splits[1]) + inc, inc)
                    shotnumbers += num_range
            else:
                if all([i.isdigit() for i in sn]):
                    shotnumbers.append(sn)

        # get user
        user = user_info.User().name()
        # new entity
        entity = deepcopy(self.entity)
        entity.context.update(user=user, version='v01')
        entity.sequence = ep_name

        shotnames = []
        for sn in shotnumbers:
            entity.sequence = ep_name
            entity.context.update(shot_code=str(sn).zfill(fx_entity.Config.shot_padding))
            shotnames.append(entity.shotname)

        qmsgBox = QtWidgets.QMessageBox(self)
        if shotnames:
            # confirm with user
            qmsgBox.setText('Create new shots?\n{}'.format('\n'.join(shotnames)))
            qmsgBox.setWindowTitle('Confirm')
            qmsgBox.setIcon(QtWidgets.QMessageBox.Question)
            qmsgBox.addButton('  OK  ', QtWidgets.QMessageBox.AcceptRole)
            qmsgBox.addButton('  Cancel  ', QtWidgets.QMessageBox.RejectRole)
            result = qmsgBox.exec_()

            # user cancelled
            if result == 1:
                return

            # continue the operation
            num_shots = len(shotnames)
            QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
            # print 'SHOT', ep_name, shotnames

            # store template selection
            self.previous_template_selection = template_files
            
            # loop over each shot to create
            for si, shotname in enumerate(shotnames):
                progress = '({}/{})'.format(si+1, num_shots)
                entity.shotname = shotname

                # add shot in SG
                project = entity.project.name()
                episode = entity.episode_code
                episode = entity.episode_code
                sequencecode = entity.sequencecode
                shortcode = entity.shotcode
                shotType = 'Shot'
                # print 'SG', project, shotname, episode, sequencecode, shortcode, shotType
                self.ui.statusbar.showMessage('Adding new shots:{}, Creating SG...'.format(progress))
                create_scene.create_sg(project, shotname, episode, sequencecode, shortcode, shotType=shotType)

                # create EP folder, if not exists
                self.ui.statusbar.showMessage('Adding new shots:{}, Making directories...'.format(progress))
                step_path = entity.path.step().combine_path
                for f in os.listdir(step_path):
                    fp = '{}/{}'.format(step_path, f)
                    if os.path.isdir(fp) and f not in AppConfig.exception_apps:
                        ep_dir = '{}/{}'.format(fp, ep_name)
                        # print 'CREATE FOLDER', ep_dir
                        if not os.path.exists(ep_dir):
                            os.makedirs(ep_dir)
                        
                # copy and rename from template
                self.ui.statusbar.showMessage('Adding new shots:{}, Copying templates...'.format(progress))
                for app, template in template_files.iteritems():
                    fn, ext = os.path.splitext(template)
                    entity.context.update(app=app, ext=ext[1:])
                    dest_dir = entity.path.sequence().combine_path
                    dest_filename = entity.filename(user=True)
                    dest_path = '{}/{}'.format(dest_dir, dest_filename)
                    # print 'COPY', template, dest_path
                    if not os.path.exists(dest_path):
                        file_utils.xcopy_file(template, dest_path)
            
            # refresh
            self.thread_update_data(self.project, ep_name, shotnames[-1])
        else:
            qmsgBox.setText('Invalid shot name input: {}'.format(shotname_input))
            qmsgBox.setWindowTitle('Error')
            qmsgBox.setIcon(QtWidgets.QMessageBox.Critical)
            qmsgBox.addButton('  OK  ', QtWidgets.QMessageBox.AcceptRole)
            result = qmsgBox.exec_()

    def thread_update_data(self, entity, ep_name, shotname): 
        self.ui.statusbar.showMessage('Fetching new data...')
        self.reset_vars()
        worker = thread_pool.Worker(self.get_shot_data, entity)
        worker.signals.result.connect(lambda: self.update_finished(ep_name, shotname))
        self.threadpool.start(worker)

    def update_finished(self, ep_name, shotname):
        eps = self.shotData.keys()
        if eps:
            self.ui.ep_listwidget.addItems(eps)
            self.ui.ep_listwidget.blockSignals(True)
            self.ui.shot_listwidget.blockSignals(True)
            # select new EP
            all_eps = [self.ui.ep_listwidget.item(i).text() for i in range(self.ui.ep_listwidget.count())]
            if ep_name in all_eps:
                index = all_eps.index(ep_name)
                self.ui.ep_listwidget.setCurrentRow(index)
                self.ep_selected()
                # select new shot
                all_shots = [self.ui.shot_listwidget.item(i).text() for i in range(self.ui.shot_listwidget.count())]
                if shotname in all_shots:
                    index = all_shots.index(shotname)
                    self.ui.shot_listwidget.setCurrentRow(index)
                    self.shot_selected()
            else:
                self.ui.ep_listwidget.setCurrentRow(0)
                self.ep_selected()
                self.ui.shot_listwidget.setCurrentRow(0)
                self.shot_selected()
            self.ui.ep_listwidget.blockSignals(False)
            self.ui.shot_listwidget.blockSignals(False)
        else:
            self.entity = None
        self.ui.statusbar.showMessage('New shots added.')
        QtWidgets.QApplication.restoreOverrideCursor()
        
    def add_tab_widget(self):
        """ looping for each tabs modules and compare to allowedTab setting,
            add to tab widgets """
        self.ui.tab_widget = QtWidgets.QTabWidget()
        modules = tabs.get_module()
        activeIndex = 0
        self.tabModules = dict()
        self.threadpool = QtCore.QThreadPool()
        self.threadpool.setMaxThreadCount(1)
        tabNames = []

        for tabName in self.tab_config.allowedTabs:
            for index, name in enumerate(modules):
                title = self.tab_config.tabNameMap.get(name) or name.capitalize()
                if name == tabName:
                    func = tabs.get_func(name)
                    reload(func)
                    widget = func.Ui(thread=self.threadpool, parent=self)
                    widget.tabName = name
                    self.ui.tab_widget.addTab(widget, title)
                    if hasattr(widget, 'refresh'):
                        widget.refresh
                    if name == self.tab_config.activeTab:
                        activeIndex = len(self.tabModules.keys())

                    self.tabModules[name] = widget
        # active tab
        self.ui.tab_widget.setCurrentIndex(activeIndex)
        self.ui.tab_layout.addWidget(self.ui.tab_widget)

        return self.ui.tab_widget

    def get_shot_data(self, entity):
        self.project = entity
        set_project_env(project=entity['name'])
        shots = ui.sg.find('Shot', [['project', 'is', entity]], ['code', 'sg_sequence'])
        # {'sg_sequence': {'type': 'Sequence', 'id': 2973, 'name': 'FBD_VFX_EP101'}, 'code': 'FBD_VFX_EP101-010', 'type': 'Shot', 'id': 13624}

        self.shotData = OrderedDict()
        for shot in shots:
            if shot and 'sg_sequence' in shot and 'code' in shot and shot['code'] and 'id' in shot and isinstance(shot['id'], int):
                seq = shot['sg_sequence']
                if seq and 'name' in seq:
                    seq_name = seq['name']
                    if seq_name not in self.shotData:
                        self.shotData[seq_name] = []
                    self.shotData[seq_name].append(shot)

        # construct entity
        self.entity = fx_entity.FXEntity()
        self.entity.context.update(root=os.environ['RFPROJECT'], 
                        project=self.project['name'], 
                        entity_type=self.entity_type, 
                        step=self.step)
        return self.shotData

    def fetch_finished(self, *args):
        eps = self.shotData.keys()
        if eps:
            self.ui.ep_listwidget.addItems(eps)
            self.ui.ep_listwidget.setCurrentRow(0)
        # else:
        #     self.entity = None

        self.ui.statusbar.showMessage('Data fetched.')
        QtWidgets.QApplication.restoreOverrideCursor()
        
    def ep_selected(self):
        self.ui.statusbar.showMessage('Updating shots...')
        self.ui.shot_listwidget.blockSignals(True)
        self.ui.shot_listwidget.clear()
        
        ep = self.ui.ep_listwidget.currentItem().text()
        shots = self.shotData[ep]
        if shots:
            shotCodes = sorted([shot['code'] for shot in shots])
            self.ui.shot_listwidget.addItems(shotCodes)
            self.ui.statusbar.showMessage('Shot updated.')

            # restore last selection
            if ep in self.previous_shot_selected and self.previous_shot_selected[ep] in shotCodes:
                index = shotCodes.index(self.previous_shot_selected[ep])
                self.ui.shot_listwidget.setCurrentRow(index)
            else:
                self.ui.shot_listwidget.setCurrentRow(0)

            self.shot_selected()
            self.ui.shot_listwidget.blockSignals(False)
            
        else:
            self.ui.shot_listwidget.blockSignals(False)
            self.entity.shotname = None
        

    def shot_selected(self):
        """ shot selected, context changed """
        # setup entity 
        widget = self.ui.tab_widget.currentWidget()
        self.entity.context.update(app=widget.tabName)
        shot = self.ui.shot_listwidget.currentItem().text()
        self.entity.shotname = shot

        # store selection
        ep = self.ui.ep_listwidget.currentItem().text()
        self.previous_shot_selected[ep] = shot

        # set unsync for all tabs
        self.unsync_tabs()
        # refresh current tab 
        self.refresh_tab()
        
    def showStatusMessage(self, msg):
        self.ui.statusbar.showMessage(msg)

    def refresh_tab(self):
        """ refresh current tab """
        self.ui.statusbar.showMessage('Updating tab...')
        widget = self.ui.tab_widget.currentWidget()
        if self.entity: 
            print('----', self.entity.path.sequence())
        widget.refresh(self.entity)
        widget.context_sync = True
        self.ui.statusbar.showMessage('Tab updated.')

    def tab_changed(self): 
        """ tab only refresh if context has changed """
        if not self.entity:
            return
        widget = self.ui.tab_widget.currentWidget()
        self.entity.context.update(app=widget.tabName)
        if not widget.context_sync: 
            self.refresh_tab()

    def unsync_tabs(self): 
        """ unsync all tabs so it will refresh when tab changed """ 
        tab_count = self.ui.tab_widget.count()
        for i in range(tab_count): 
            widget = self.ui.tab_widget.widget(i)
            widget.context_sync = False 

def set_project_env(project):
    # --- set env 
    envData = project_info.ProjectInfo(project).env()
    for key, path in envData.items():
        os.environ[key] = path

def show(mode='default'):
    logger.info('Run in standalone\n')
    app = QtWidgets.QApplication.instance()
    if not app:
        app = QtWidgets.QApplication(sys.argv)
    if mode == 'default':
        myApp = VFxManager(tab_config=TabConfig, parent=None)
    myApp.show()
    stylesheet.set_default(app)
    sys.exit(app.exec_())
    return myApp


if __name__ == '__main__':
    show()

'''
from rf_app.vfxManager import app
reload(app)
app.show()
'''