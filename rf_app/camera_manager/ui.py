import os 
os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

from rf_utils.widget import project_widget
from rf_utils.widget import media_widget
reload(media_widget)
sg = None


class CameraManagerUI(QtWidgets.QWidget):
    """docstring for CameraManagerUI"""
    def __init__(self, parent=None):
        super(CameraManagerUI, self).__init__(parent=parent)
        # main layout 
        self.layout = QtWidgets.QGridLayout()

        # widgets 
        self.projectWidget = project_widget.SGProjectComboBox(sg=sg, layout=QtWidgets.QHBoxLayout())
        self.projectWidget.label.setText('Project : ')
        self.line = self.add_line()
        self.set_logo()
        self.set_filter_widgets()

        self.shotLabel = QtWidgets.QLabel('Shot List : ')
        self.shotListWidget = QtWidgets.QListWidget()
        self.shotListWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)

        self.listCameraButton = QtWidgets.QPushButton('List Camera')
        self.listCameraButton.setMinimumSize(QtCore.QSize(0, 30))

        self.buildButton = QtWidgets.QPushButton('Build')
        self.buildButton.setMinimumSize(QtCore.QSize(0, 30))

        self.assetLineEdit = QtWidgets.QLineEdit()
        self.shotLineEdit = QtWidgets.QLineEdit()

        # preview 
        self.previewFrame = QtWidgets.QFrame()
        self.previewFrame.setFrameShape(QtWidgets.QFrame.Box)
        self.previewFrame.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.previewLayout = QtWidgets.QHBoxLayout(self.previewFrame)
        self.previewWidget = media_widget.PipelinePreviewMedia()
        self.previewLayout.addWidget(self.previewWidget)

        # progress bar 
        self.progressBar = QtWidgets.QProgressBar()
        self.progressBar.setRange(0, 100)
        self.progressBar.setVisible(False)

        # add layout 
        self.layout.addWidget(self.projectWidget, 0, 0, 1, 1)
        self.layout.addWidget(self.line, 1, 0, 1, 1)
        self.layout.addLayout(self.assetLayout, 2, 0, 1, 1)
        self.layout.addWidget(self.assetLineEdit, 3, 0, 1, 1)

        self.layout.addWidget(self.assetListWidget, 4, 0, 1, 1)
        self.layout.addWidget(self.listCameraButton, 5, 0, 1, 1)
        self.layout.addWidget(self.progressBar, 6, 0, 1, 1)

        # logo
        self.layout.addWidget(self.logo, 0, 2, 3, 1)

        self.layout.addWidget(self.shotLabel, 2, 1, 1, 1)
        self.layout.addWidget(self.shotLineEdit, 3, 1, 1, 1)
        self.layout.addWidget(self.shotListWidget, 4, 1, 1, 1)
        self.layout.addWidget(self.buildButton, 5, 1, 1, 1)

        self.layout.addWidget(self.previewFrame, 3, 2, 4, 1)

        self.layout.setColumnStretch(0, 1)
        self.layout.setColumnStretch(1, 1)
        self.layout.setColumnStretch(2, 2)

        self.setLayout(self.layout)


    def set_filter_widgets(self): 
        self.assetLayout = QtWidgets.QGridLayout()
        self.assetComboBox = QtWidgets.QComboBox()
        self.assetLabel = QtWidgets.QLabel('Assets : ')
        self.assetLayout.addWidget(self.assetLabel, 0, 0, 1, 1)
        self.assetLayout.addWidget(self.assetComboBox, 0, 1, 1, 1)

        self.assetLayout.setColumnStretch(0, 1)
        self.assetLayout.setColumnStretch(1, 3)

        self.assetListWidget = QtWidgets.QListWidget()

    def set_logo(self):
        # set logo widget
        from rf_utils import icon

        spacerItemH = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        self.logo = QtWidgets.QLabel()
        self.logo.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.logo.setPixmap(QtGui.QPixmap(icon.logo).scaled(64, 64, QtCore.Qt.KeepAspectRatio))


    def add_line(self): 
        line = QtWidgets.QFrame()
        line.setFrameShape(QtWidgets.QFrame.HLine)
        line.setFrameShadow(QtWidgets.QFrame.Sunken)
        return line
