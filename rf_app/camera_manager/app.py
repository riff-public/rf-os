# v.0.0.1 polytag switcher
_title = 'Riff Camera Manager'
_version = 'v.0.0.1'
_des = 'WIP'
uiName = 'CameraManagerUI'

#Import python modules
import sys
import os
import getpass
import logging
from functools import partial
from collections import OrderedDict

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

# import config
import rf_config as config

# framework modules
from rf_utils import log_utils
user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

from rf_utils.sg import sg_process 
from . import ui
from . import general_cmd
from . import maya_cmd
reload(general_cmd)
reload(maya_cmd)
reload(ui)
from rf_utils import thread_pool
ui.sg = sg_process.sg


class RFCameraManager(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        #Setup Window
        super(RFCameraManager, self).__init__(parent)

        # ui read
        self.ui = ui.CameraManagerUI(parent)
        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)
        self.setWindowTitle('Camera Manager {}'.format(_version))
        self.resize(740, 400)
        self.init_functions()
        self.init_signals()

    def init_functions(self): 
        self.threadpool = QtCore.QThreadPool()
        self.threadpool.setMaxThreadCount(1)

    def init_signals(self): 
        self.ui.listCameraButton.clicked.connect(self.list_project_data)
        self.ui.projectWidget.projectChanged.connect(self.list_project_data)
        self.ui.assetComboBox.currentIndexChanged.connect(self.type_changed)
        self.ui.assetLineEdit.textChanged.connect(self.list_assets)
        self.ui.assetListWidget.itemSelectionChanged.connect(self.asset_selected)
        self.ui.shotLineEdit.textChanged.connect(self.list_shots)
        self.ui.buildButton.clicked.connect(self.build)
        self.ui.shotListWidget.itemSelectionChanged.connect(self.shot_selected)

    def list_project_data(self):
        projectEntity = self.ui.projectWidget.current_item()
        worker = thread_pool.Worker(self.fetch_data, projectEntity)
        worker.kwargs['callback'] = worker.signals.loopResult
        worker.signals.result.connect(self.fetch_data_finished)
        worker.signals.loopResult.connect(self.set_progress)
        self.threadpool.start(worker)
        self.set_loading(self.ui.assetListWidget)

    def fetch_data(self, project, **kwargs): 
        relationDict = general_cmd.get_shot_asset_relation(project, **kwargs)
        return relationDict

    def fetch_data_finished(self, relationDict): 
        self.setup_display(relationDict)

    def setup_display(self, relationDict): 
        # set comboBox 
        self.ui.assetComboBox.blockSignals(True)
        self.ui.assetComboBox.clear()
        self.ui.assetListWidget.clear()
        self.ui.shotListWidget.clear()

        index = 0 
        for assetType, data in relationDict.items(): 
            self.ui.assetComboBox.addItem(assetType)
            self.ui.assetComboBox.setItemData(index, data, QtCore.Qt.UserRole)
            index+=1

        self.ui.assetComboBox.blockSignals(False)
        self.ui.assetComboBox.currentIndexChanged.emit(0)

    def type_changed(self): 
        self.list_assets()

    def list_assets(self): 
        index = self.ui.assetComboBox.currentIndex()
        currentData = self.ui.assetComboBox.itemData(index, QtCore.Qt.UserRole)
        self.ui.assetListWidget.clear()
        self.ui.shotListWidget.clear()
        key = self.ui.assetLineEdit.text()
        self.ui.assetListWidget.blockSignals(True)

        if currentData: 
            for asset, data in currentData.items(): 
                if key in asset: 
                    item = QtWidgets.QListWidgetItem()
                    item.setText(asset)
                    item.setData(QtCore.Qt.UserRole, data)
                    self.ui.assetListWidget.addItem(item)

        else: 
            self.set_no_item(self.ui.assetListWidget)
        self.ui.assetListWidget.blockSignals(False)

    def asset_selected(self): 
        self.list_shots()

    def list_shots(self): 
        item = self.ui.assetListWidget.currentItem()
        listData = item.data(QtCore.Qt.UserRole)
        key = self.ui.shotLineEdit.text()

        self.ui.shotListWidget.blockSignals(True)
        self.ui.shotListWidget.clear()

        if listData: 
            for shotEntity in listData: 
                shotName = shotEntity['code']
                cameraPath = shotEntity['camera_path']
                if key in shotName: 
                    item = QtWidgets.QListWidgetItem()
                    item.setText(shotName)
                    item.setData(QtCore.Qt.UserRole, shotEntity)
                    icon = QtGui.QIcon()
                    iconPath = '{}/camera_icon.png'.format(moduleDir)
                    icon.addPixmap(QtGui.QPixmap(iconPath), QtGui.QIcon.Normal, QtGui.QIcon.Off)
                    item.setIcon(icon)
                    self.ui.shotListWidget.addItem(item)

                    if not cameraPath: 
                        item.setFlags(QtCore.Qt.NoItemFlags)
        else: 
            self.set_no_item(self.ui.shotListWidget)

        self.ui.shotListWidget.blockSignals(False)

    def build(self): 
        items = self.ui.shotListWidget.selectedItems()

        for item in items: 
            shotEntity = item.data(QtCore.Qt.UserRole)
            general_cmd.build(shotEntity)

        general_cmd.maya_cmd.open_seuquencer()

    def shot_selected(self): 
        item = self.ui.shotListWidget.currentItem()
        shotEntity = item.data(QtCore.Qt.UserRole)
        project = shotEntity.get('project').get('name')
        entityName = shotEntity.get('code')
        step = 'anim'
        process = 'main'
        self.ui.previewWidget.view(project=project, entityName=entityName, step=step, process=process)

    def set_no_item(self, widget): 
        widget.clear()
        widget.addItem('No Data')

    def set_loading(self, widget): 
        widget.clear()
        widget.addItem('Loading ...')

    def set_progress(self, progress): 
        self.ui.assetListWidget.clear()
        self.ui.assetListWidget.addItem(progress)


def show(mode='default'):
    if config.isMaya:
        from rftool.utils.ui import maya_win
        maya_win.deleteUI(uiName)
        myApp = RFCameraManager(maya_win.getMayaWindow())
        myApp.show()
