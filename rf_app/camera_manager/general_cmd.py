import os 
import sys 
from collections import OrderedDict 
from rf_utils.sg import sg_process
from rf_utils.context import context_info
from rf_utils import register_shot
from datetime import datetime

from . import maya_cmd
sg = sg_process.sg 


def fetch_shots(project): 
    filters = [['project', 'is', project]]
    fields = ['project', 'assets', 'sg_setdresses', 'code', 'id', 'sg_episode', 'sg_sequence_code', 'sg_shortcode', 'project.Project.sg_project_code', 'sg_working_duration']
    return sg.find('Shot', filters, fields)


def fetch_assets(project): 
    filters = [['project', 'is', project]]
    fields = ['shots', 'sg_asset_type', 'code', 'id']
    return sg.find('Asset', filters, fields)


def get_shot_asset_relation(project, callback=None): 
    shots = fetch_shots(project)
    assets = fetch_assets(project)

    progress = 0.0
    shotDict = OrderedDict()
    for shotEntity in shots:
        shotDict[shotEntity['code']]  = shotEntity
        cameraPath = get_camera_path_speed(shotEntity)
        shotEntity['camera_path'] = cameraPath
        percent = round((progress/len(shots))*100.0, 2)
        if hasattr(callback, 'emit'): 
            callback.emit('Processing shot {} % ...'.format(percent))
        progress+=1


    assetDict = OrderedDict()
    for assetEntity in assets: 
        assetDict[assetEntity['code']] = assetEntity

    relationDict = OrderedDict()
    relationDict['all'] = {'all': shots}

    progress = 0.0
    for assetName, entity in assetDict.items(): 
        assetType = entity.get('sg_asset_type')
        shotEntities = [shotDict.get(a['name']) for a in entity.get('shots')]

        if not assetType: 
            assetType = 'No Type'

        if not assetType in relationDict.keys(): 
            entityDict = OrderedDict()
            entityDict[assetName] = shotEntities
            relationDict[assetType] = entityDict

        else: 
            entityDict = relationDict[assetType]
            entityDict[assetName] = shotEntities

    return relationDict


def get_camera_path(shotEntity): 
    now = datetime.now()
    project = shotEntity.get('project').get('name')
    projectCode = shotEntity.get('project.Project.sg_project_code')
    episode = shotEntity.get('sg_episode').get('name')
    entityName = shotEntity.get('code')

    context = context_info.Context()
    context.update(project=project, projectCode=projectCode, entityType='scene', entityGrp=episode, entity=entityName, look='main')
    scene = context_info.ContextPathInfo(context=context)

    reg = register_shot.Register(scene)
    cameraKeys = reg.get.camera()
    if cameraKeys: 
        camPath = reg.get.cache(cameraKeys[0], hero=True)
        return camPath


def get_camera_path_speed(shotEntity): 
    """ get by hardcoding camera name config not from register data """ 
    project = shotEntity.get('project').get('name')
    projectCode = shotEntity.get('project.Project.sg_project_code')
    if shotEntity.get('sg_episode'): 
        episode = shotEntity.get('sg_episode').get('name')
        entityName = shotEntity.get('code')

        context = context_info.Context()
        context.update(project=project, projectCode=projectCode, entityType='scene', entityGrp=episode, entity=entityName, look='main')
        scene = context_info.ContextPathInfo(context=context)

        outputDir = scene.path.scheme(key='heroPath').abs_path()
        cameraName = scene.output_name(outputKey='camCacheHero', hero=True)
        camPath = '%s/%s' % (outputDir, cameraName)
        if os.path.exists(camPath): 
            return camPath


def build(shotEntity):
    shotName = shotEntity.get('sg_shortcode')
    cameraPath = shotEntity.get('camera_path')
    duration = shotEntity.get('sg_working_duration')
    namespace = '%s_cam' % shotName
    shotNode = maya_cmd.build_camera(shotName, namespace, cameraPath, duration)
    return shotNode

# char: 
#   assetA: 
#       ['s0010', 's0020']

# all: 
#   ['all']


