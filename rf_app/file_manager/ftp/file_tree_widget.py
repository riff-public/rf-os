from Qt import QtWidgets
from Qt import QtCore
from Qt import QtGui


class FileTree(QtWidgets.QTreeWidget):
    """docstring for FileTree"""
    def __init__(self, root_path=None, parent=None):
        super(FileTree, self).__init__(parent=parent)
        self.root_path = root_path
        self.root = self.invisibleRootItem()
        self.root_path = str()
        self.header().setStretchLastSection(False)
        self.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)

    def set_headers(self, headers): 
        header = QtWidgets.QTreeWidgetItem(headers)
        self.setHeaderItem(header)

    def resize_headers(self, sizes): 
        for i, size in enumerate(sizes): 
            self.setColumnWidth(i, size)

    def add_item(self, root, value, index=0, icon_path=''): 
        item = QtWidgets.QTreeWidgetItem(root)
        item.setText(index, value)
        if icon_path: 
            icon_widget = QtGui.QIcon()
            icon_widget.addPixmap(QtGui.QPixmap(icon_path), QtGui.QIcon.Normal, QtGui.QIcon.Off)
            item.setIcon(index, icon_widget)

        return item

    def add_icon(self, root, index, icon_path): 
        icon_widget = QtGui.QIcon()
        icon_widget.addPixmap(QtGui.QPixmap(icon_path), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        root.setIcon(index, icon_widget)
        return icon_widget

    def set_background_color(self, root, index, color): 
        root.setBackground(index, QtGui.QColor(*color))

    def set_root_path(self, root_path): 
        self.root_path = root_path

    def set_item_grey(self, index, item): 
        item.setForeground(index, QtGui.QColor(100, 100, 100))

    def find_item_from_path(self, path): 
        if self.root_path in path: 
            if path == self.root_path: 
                return self.root
            else: 
                search_path = path.split(self.root_path)[-1]

    def add_file_info(self, root_item, text, col_index, icon_path=''): 
        """ add data type """ 
        label = LabelIcon(text, icon_path, parent=self)
        self.setItemWidget(root_item, col_index, label)
        return label

    def add_action_button(self, root, col_index, label): 
        button = QtWidgets.QPushButton(label, parent=self)
        self.setItemWidget(root, col_index, button)
        return button

    def list_all_children(self, root=None): 
        all_items = list()
        if not root: 
            root = self.root
        count = root.childCount()
        if not count == 0: 
            items = [root.child(a) for a in range(count)]
            all_items += items
            for item in items: 
                all_items += self.list_all_children(item) 
        else: 
            return all_items
        return all_items

    def list_children(self, root=None, visible_only=False): 
        all_items = list()
        if not root: 
            root = self.root
        count = root.childCount()
        if visible_only: 
            items = [root.child(a) for a in range(count) if not root.child(a).isHidden()]
        else: 
            items = [root.child(a) for a in range(count)]
        return items

    def get_item_by_name(self, root, name, index=0): 
        items = self.list_children(root)
        for item in items: 
            if item.text(index) == name: 
                return item

    def hide_all(self): 
        items = self.list_children(self.root)
        for item in items: 
            item.setHidden(True)

    def clear_item(self, indexs=[]): 
        if indexs: 
            items = self.list_children(self.root)
            for item in items: 
                for i in indexs: 
                    item.setText(i, '')
                    item.setData(i, QtCore.Qt.BackgroundRole, None)

    def clear_widget(self, index): 
        items = self.list_children(self.root)
        for item in items: 
            widget = self.itemWidget(item, index)
            if widget: 
                widget.deleteLater()

    def get_all_widgets(self, index): 
        widgets = list()
        items = self.list_children(self.root)
        for item in items: 
            widget = self.itemWidget(item, index)
            if widget: 
                widgets.append(widget)
        return widgets

    def clear_children(self, item): 
        count = item.childCount()
        children = self.list_children(item)

        for child in children: 
            item.removeChild(child)

    def fit_column(self):   
        header = self.header()
        header.setSectionResizeMode(QtWidgets.QHeaderView.ResizeToContents)
        # header.setSectionResizeMode(5, QtWidgets.QHeaderView.Stretch)

    def current_item(self): 
        item = self.currentItem()
        return item


class LabelIcon(QtWidgets.QWidget):
    status = QtCore.Signal(str)
    def __init__(self, text, iconPath=None, parent=None) :
        super(LabelIcon, self).__init__(parent)
        self.text = text
        self.iconPath = iconPath
        self.ui()
        
    def ui(self): 
        self.layout = QtWidgets.QHBoxLayout()
        self.label = QtWidgets.QLabel(self.text)
        self.label.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.layout.addWidget(self.label)

        if self.iconPath: 
            self.logo = QtWidgets.QLabel()
            # self.logo.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
            self.logo.setPixmap(QtGui.QPixmap(self.iconPath).scaled(16, 16, QtCore.Qt.KeepAspectRatio))
            self.layout.addWidget(self.logo)

        self.setLayout(self.layout)
        self.layout.setContentsMargins(1, 1, 1, 1)

    def get_text(self): 
        return self.label.text()
