# v.0.0.1 Ftp sync
_title = 'RF Ftp Sync'
_version = 'v.0.0.1'
_des = 'Beta'
uiName = 'RFFtp Manager'

#Import python modules
import sys
import os
import getpass
import logging
from datetime import datetime
from collections import OrderedDict
from functools import partial
# import config
import rf_config as config
from rf_utils import log_utils
from rf_utils.ui import stylesheet

module_dir = os.path.dirname(sys.modules[__name__].__file__)
appname = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]
user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)
uifile = '{}/{}'.format(module_dir, 'ui.ui')
global ui

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

from rf_utils import load 
from rf_utils.widget import browse_widget
from rf_utils.widget import dialog
from . import file_tree_widget
from rf_utils import file_utils
from rf_utils import ftp_utils
from rf_utils.widget import sg_entity_widget
from rf_utils.sg import sg_process 
from rf_utils import thread_pool
from rf_utils.context import context_info
from rf_utils import icon
from . import model
from . import app_config
Config = app_config.Config
Color = app_config.Color
sg = sg_process.sg


# class Config: 
#     owner_col = 'Owner'
#     asset_col = 'Local Assets'
#     status_col = 'Cloud'
#     action_col = 'Action'

#     tree_columns = [owner_col, asset_col, status_col, action_col]
#     column_size = [50, 400, 100, 100]
#     user = ''
#     root_config = {
#         'Riff': {'upload': '/Riff', 'local': 'P:', 'download': '/Zings'}, 
#         'Zings': {'upload': '/Zings', 'local': 'P:', 'download': '/Riff'}}

#     owners = {
#         'Riff': {'name': 'RFF', 'icon': 'icons/riff.png', 'color': (0, 0, 0)}, 
#         'Zings': {'name': 'ZGS', 'icon': 'icons/zings.png', 'color': (100, 100, 160)}, 
#         'Shared': {'name': 'SHR', 'icon': 'icons/shares.png', 'color': (60, 60, 60)}, 
#         'Blank': {'name': 'BLNK', 'icon': 'icons/blank.png', 'color': (40, 40, 40)}}


# class Color: 
#     green = (100, 160, 100)
#     red = (160, 100, 100)
#     grey = (60, 60, 60)
#     blue = (100, 120, 160)
#     yellow = (160, 140, 100)
class Cache: 
    upload_status = dict() 
    download_status = dict()


class FTPManager(QtWidgets.QMainWindow):
    def __init__(self, user, parent=None):
        #Setup Window
        super(FTPManager, self).__init__(parent)

        # ui read
        Config.user = user 
        ui = load.setup_ui(uifile)
        self.ui = ui
        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)
        self.setup_ui()
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.resize(1120, 800)
        self.threadpool = QtCore.QThreadPool()
        self.threadpool.setMaxThreadCount(1)
        self.init_functions()
        self.init_signals()

    def setup_ui(self): 
        self.ui.file_tree = file_tree_widget.FileTree()
        self.ui.tag = sg_entity_widget.TagListWidget()
        self.ui.tag.listWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)

        self.ui.file_layout.insertWidget(0, self.ui.tag)
        self.ui.file_layout.insertWidget(1, self.ui.file_tree)
        self.ui.file_tree.set_headers(Config.tree_columns)
        self.ui.file_tree.resize_headers(Config.column_size)

        self.ui.file_layout.setStretch(0, 1)
        self.ui.file_layout.setStretch(1, 4)
        self.ui.file_layout.setStretch(2, 0)
        self.ui.status.setText('Ready')

        self.ui.user.setText(Config.user)
        self.ui.rb_upload.setChecked(True)
        # set path 
        # self.ui.combo_upload.addItem(Config.root_config[Config.user]['upload'])
        # self.ui.combo_download.addItem(Config.root_config[Config.user]['download'])

    def init_functions(self): 
        self.entities = list()
        self.ftp = ftp_utils.RFFtp()
        self.model = model.CloudModel(project='CA', config=Config)
        self.set_mode()
        self.collect_data()

    def init_signals(self): 
        self.ui.refresh_button.clicked.connect(self.collect_data)
        self.ui.check_button.clicked.connect(self.check_status)
        self.ui.tag.selected.connect(self.filter_selected)
        self.ui.search.textChanged.connect(self.filter_selected)
        self.ui.rb_upload.toggled.connect(self.set_mode)
        self.ui.rb_download.toggled.connect(self.set_mode)
        self.ui.file_tree.customContextMenuRequested.connect(self.show_menu)

    def show_menu(self, pos): 
        menu = QtWidgets.QMenu(self)
        update_menu = menu.addAction('Update fileinfo')
        update_menu.triggered.connect(partial(self.update_asset_info))
        menu.popup(self.ui.file_tree.mapToGlobal(pos))

    def set_mode(self): 
        self.upload = self.ui.rb_upload.isChecked()
        self.download = self.ui.rb_download.isChecked()
        self.ui.combo_cloud.clear()
        self.filter_selected()

        self.ui.file_tree.clear_item([Config.tree_columns.index(Config.localdate_col), 
            Config.tree_columns.index(Config.remotedate_col), 
            Config.tree_columns.index(Config.status_col)])

        buttons = self.ui.file_tree.get_all_widgets(Config.tree_columns.index(Config.action_col))
        for button in buttons: 
            button.setEnabled(False)

        # self.ui.file_tree.clear_widget(Config.tree_columns.index(Config.action_col))
        if self.upload: 
            # self.ui.cloud_label.setText('Cloud')
            self.ui.combo_cloud.addItem(Config.root_config[Config.user]['upload'])
        if self.download: 
            # self.ui.cloud_label.setText('Cloud')
            self.ui.combo_cloud.addItem(Config.root_config[Config.user]['download'])

    def collect_data(self): 
        self.ui.file_tree.clear()
        self.ui.refresh_button.setEnabled(False)
        worker = thread_pool.Worker(self.fetch_data)
        worker.kwargs['loop_callback'] = worker.signals.loopResult
        worker.signals.result.connect(self.fetch_data_finished)
        worker.signals.loopResult.connect(self.set_progress)
        self.threadpool.start(worker)
        self.ui.status.setText('Fetching data ...')

    def fetch_data(self, loop_callback): 
        loop_callback.emit('fetching ShotGrid ...')
        assets = self.model.fetch_sg_assets()
        entities = self.model.create_context_from_entities(assets)
        types = self.model.sg_asset_types()

        # extract all types 
        loop_callback.emit('fetching Ftp ...')
        cloud_assets = self.model.fetch_ftp_asset_from_types(types)

        return entities

    def fetch_data_finished(self, entities): 
        """ list assets """ 
        self.entities = entities
        self.ui.status.setText('Finished')
        tag_dict = self.model.get_tags([a.sg_entity for a in self.entities]) or dict()

        self.ui.tag.clear()
        all_tags = {'name': 'all'}
        self.ui.tag.add_item(all_tags, iconPath=icon.sgTag, showIcon=True)

        for tag, tag_entity in tag_dict.items():
            self.ui.tag.add_item(tag_entity, iconPath=icon.sgTag, showIcon=True)

        self.ui.tag.set_current('all')

    def filter_selected(self): 
        """ tag and text activated """ 
        filters = list()
        selected_tags = self.ui.tag.selected_items()
        keyword = self.ui.search.text()
        selected_tag_names = [a['name'] for a in selected_tags]

        for entity in self.entities: 
            sg_entity = entity.sg_entity # assign from model
            tags = sg_entity.get('tags')

            if 'all' in selected_tag_names: 
                valid = True 
            else: 
                valid = True if any([a['name'] in selected_tag_names for a in tags]) else False
                    
            if keyword: 
                valid = True if keyword in entity.keyword else False 

            if valid: 
                filters.append(entity)

        self.list_assets(filters)

    def list_assets(self, entities): 
        self.ui.file_tree.hide_all()
        for entity in entities: 
            hero = entity.path.hero().abs_path()
            local_dir_icon = icon.folder_full_icon if os.path.exists(hero) else icon.folder_grey_icon
            owner_icon, color = self.model.get_display_owner(entity)
            item = self.ui.file_tree.get_item_by_name(self.ui.file_tree.root, entity.name, Config.tree_columns.index(Config.asset_col))

            # if item: 
            #     print('item found', entity.name)
            if not item: 
                # print('create new item', entity.name)
                item = self.ui.file_tree.add_item(
                        self.ui.file_tree.root, 
                        entity.name, 
                        Config.tree_columns.index(Config.asset_col), 
                        icon_path=local_dir_icon)

            # set owner 
            self.ui.file_tree.add_icon(item, Config.tree_columns.index(Config.owner_col), owner_icon)
            # self.ui.file_tree.set_background_color(item, Config.tree_columns.index(Config.owner_col), color)
            # item.setText(Config.tree_columns.index(Config.owner_col), owner)
            
            # set status 
            item.setText(Config.tree_columns.index(Config.status_col), '-')
            item.setData(Config.tree_columns.index(Config.asset_col), QtCore.Qt.UserRole, entity)
            
            # add button 
            # button = self.ui.file_tree.add_action_button(item, Config.tree_columns.index(Config.action_col), '-')
            # button.setEnabled(False)

            if entity.name in Cache.upload_status.keys(): 
                entity = Cache.upload_status[entity.name]
                self.update_upload_item(entity, item)      
            item.setHidden(False)

        self.ui.refresh_button.setEnabled(True)
        
    def check_status(self, items=list()): 
        self.ui.check_button.setEnabled(False)
        items = self.ui.file_tree.list_children(root=None, visible_only=True) if not items else items
        self.set_progress('Checking status ...')

        for item in items: 
            item.setText(Config.tree_columns.index(Config.status_col), '')
            item.setText(Config.tree_columns.index(Config.localdate_col), '')
            item.setText(Config.tree_columns.index(Config.remotedate_col), '')

        if self.upload: 
            worker = thread_pool.Worker(self.check_upload_status_thread, items)
            worker.kwargs['loop_callback'] = worker.signals.loopResult
            worker.signals.result.connect(self.check_upload_finished)
            worker.signals.loopResult.connect(self.update_upload_status)
            self.threadpool.start(worker)
            self.ui.status.setText('Fetching data ...')

        if self.download: 
            worker = thread_pool.Worker(self.check_download_status_thread, items)
            worker.kwargs['loop_callback'] = worker.signals.loopResult
            worker.signals.result.connect(self.check_download_finished)
            worker.signals.loopResult.connect(self.update_download_status)
            self.threadpool.start(worker)
            self.ui.status.setText('Fetching data ...')

    def check_upload_status_thread(self, items, loop_callback=None): 
        for item in items: 
            entity = item.data(Config.tree_columns.index(Config.asset_col), QtCore.Qt.UserRole)
            entity = self.model.add_upload_status_entity(entity)
            loop_callback.emit((entity, item))
            Cache.upload_status[entity.name] = entity
            # loop_callback.emit('Checking {} ...'.format(entity.name))
        return True

    def check_download_status_thread(self, items, loop_callback=None): 
        for item in items: 
            entity = item.data(Config.tree_columns.index(Config.asset_col), QtCore.Qt.UserRole)
            entity = self.model.add_download_status_entity(entity)
            loop_callback.emit((entity, item))
            Cache.download_status[entity.name] = entity
        return True

    def update_upload_status(self, result): 
        entity, item = result
        self.update_upload_item(entity, item)

    def update_download_status(self, result): 
        entity, item = result
        self.update_download_item(entity, item)

    def check_upload_finished(self, result): 
        self.ui.check_button.setEnabled(True) 
        self.set_progress('Finished')

    def check_download_finished(self, result): 
        self.ui.check_button.setEnabled(True) 
        self.set_progress('Finished')

    def update_asset_info(self): 
        item = self.ui.file_tree.current_item()
        entity = item.data(Config.tree_columns.index(Config.asset_col), QtCore.Qt.UserRole)
        self.model.update_asset_info(entity)
        self.check_status([item])

    def update_upload_item(self, entity, item): 
        # item = self.ui.file_tree.get_item_by_name(entity.name)
        owner = True
        status = entity.context.status
        date_color = Color.light_grey
        if status['sync']: 
            sync = 'Sync' 
            color = Color.green 
        else: 
            if status['local_info']: 
                sync = 'Off sync'
                color = Color.red
                date_color = Color.green
            else: 
                sync = 'No Files'
                color = Color.grey 
        
        if entity.studio not in entity.owners: 
            sync = 'Not Owner'
            owner = False


        local_info = status['local_info']
        remote_info = status['remote_info']
        queue_files = status['queue_files']
        local_date = status['local_date']
        remote_date = status['remote_date']
        error_message = status['error_message']
        hero_path = entity.path.hero().abs_path()

        if not status['sync'] and not local_date == '-': 
            date_color = Color.green

        if error_message: 
            sync = error_message
            color = Color.red

        # set sync 
        item.setText(Config.tree_columns.index(Config.status_col), sync)
        item.setText(Config.tree_columns.index(Config.localdate_col), local_date)
        item.setText(Config.tree_columns.index(Config.remotedate_col), remote_date)

        # set color
        item.setBackground(Config.tree_columns.index(Config.status_col), QtGui.QColor(*color))
        item.setForeground(Config.tree_columns.index(Config.localdate_col), QtGui.QColor(*date_color))
        item.setForeground(Config.tree_columns.index(Config.remotedate_col), QtGui.QColor(*Color.light_grey))
        
        # set update files 
        self.ui.file_tree.clear_children(item)

        for file in queue_files: 
            file_item = self.ui.file_tree.add_item(item, os.path.basename(file), Config.tree_columns.index(Config.asset_col), icon_path=icon.file_icon)
            file_item.setData(Config.tree_columns.index(Config.asset_col), QtCore.Qt.UserRole, file)

        button = self.ui.file_tree.itemWidget(item, Config.tree_columns.index(Config.action_col))
        action = 'Upload'
        if button: 
            button.deleteLater()
        button = self.ui.file_tree.add_action_button(item, Config.tree_columns.index(Config.action_col), action)
        button.setEnabled(False)
        
        if not status['sync'] and not error_message: 
            button.clicked.connect(partial(self.action_upload_clicked, local_info, entity, item))
            button.setText(action)
            if os.path.exists(hero_path) and owner: 
                button.setEnabled(True)
        # else: 
        #     button.setEnabled(False)

    def update_download_item(self, entity, item): 
        status = entity.context.status
        date_color = Color.light_grey
        if status['sync']: 
            sync = 'Sync' 
            color = Color.green 
        else: 
            if status['remote_info']: 
                sync = 'Off sync'
                color = Color.red
                date_color = Color.green
            else: 
                sync = 'No Files'
                color = Color.grey 

        local_info = status['local_info']
        remote_info = status['remote_info']
        queue_files = status['queue_files']
        local_date = status['local_date']
        remote_date = status['remote_date']
        error_message = status['error_message']

        # set item 
        item.setText(Config.tree_columns.index(Config.status_col), sync)
        item.setText(Config.tree_columns.index(Config.localdate_col), local_date)
        item.setText(Config.tree_columns.index(Config.remotedate_col), remote_date)
        
        # set color
        item.setBackground(Config.tree_columns.index(Config.status_col), QtGui.QColor(*color))
        item.setForeground(Config.tree_columns.index(Config.localdate_col), QtGui.QColor(*Color.light_grey))
        item.setForeground(Config.tree_columns.index(Config.remotedate_col), QtGui.QColor(*date_color))

        # set update files 
        self.ui.file_tree.clear_children(item)

        for file in queue_files: 
            file_item = self.ui.file_tree.add_item(item, os.path.basename(file), Config.tree_columns.index(Config.asset_col), icon_path=icon.file_icon)
            file_item.setData(Config.tree_columns.index(Config.asset_col), QtCore.Qt.UserRole, file)
        
        # set date 

        # create button
        button = self.ui.file_tree.itemWidget(item, Config.tree_columns.index(Config.action_col))
        action = 'Download'
        if button: 
            button.deleteLater()
        button = self.ui.file_tree.add_action_button(item, Config.tree_columns.index(Config.action_col), action)
        button.setEnabled(False)
        if not status['sync']: 
            button.clicked.connect(partial(self.action_download_clicked, local_info, entity, item))
            button.setText(action)
            if status['remote_info']: 
                button.setEnabled(True)
            else: 
                button.setEnabled(False)

    def set_progress(self, message): 
        self.ui.status.setText(message)

    def action_upload_clicked(self, local_info, entity, item, *args): 
        # compare json from local and server 
        worker = thread_pool.Worker(self.run_upload, local_info, entity, item)
        worker.kwargs['loop_callback'] = worker.signals.loopResult
        worker.signals.result.connect(self.action_upload_finished)
        worker.signals.loopResult.connect(self.set_progress)
        self.threadpool.start(worker)
        self.ui.status.setText('Fetching data ...')

        # ui 
        button = self.ui.file_tree.itemWidget(item, Config.tree_columns.index(Config.action_col))
        button.setEnabled(False)
        item.setText(Config.tree_columns.index(Config.status_col), 'Working ...')
        item.setBackground(Config.tree_columns.index(Config.status_col), QtGui.QColor(*Color.yellow))


    def run_upload(self, local_info, entity, item, loop_callback=None): 
        if local_info: 
            loop_callback.emit('Checking local info {}'.format(entity.name))
            self.model.update_local_info(entity)
        # sync, local_info, remote_info = self.model.compare_local_cloud(entity)
        # print(sync, local_info, remote_info)
        loop_callback.emit('{}: Uploading files ...'.format(entity.name))
        self.model.sync_remote(entity)

        # read status again 
        loop_callback.emit('{}: Checking status ...'.format(entity.name))
        entity = self.model.add_upload_status_entity(entity)
        return (entity, item)

    def action_upload_finished(self, result): 
        entity, item = result
        self.update_upload_item(entity, item)
        self.set_progress('Finished')

    def action_download_clicked(self, local_info, entity, item, *args): 
        # compare json from local and server 
        print('download_clicked')
        worker = thread_pool.Worker(self.run_download, local_info, entity, item)
        worker.kwargs['loop_callback'] = worker.signals.loopResult
        worker.signals.result.connect(self.action_download_finished)
        worker.signals.loopResult.connect(self.set_progress)
        self.threadpool.start(worker)
        self.ui.status.setText('Fetching data ...')

        # ui 
        button = self.ui.file_tree.itemWidget(item, Config.tree_columns.index(Config.action_col))
        button.setEnabled(False)
        item.setText(Config.tree_columns.index(Config.status_col), 'Working ...')
        item.setBackground(Config.tree_columns.index(Config.status_col), QtGui.QColor(*Color.yellow))

    def run_download(self, local_info, entity, item, loop_callback=None): 
        loop_callback.emit('{}: Downloading files ...'.format(entity.name))
        results = self.model.sync_local(entity)

        if results: 
            for file in results: 
                loop_callback.emit('{} Missing from FTP ...'.format(file))

        # read status again 
        loop_callback.emit('{}: Checking status ...'.format(entity.name))
        entity = self.model.add_download_status_entity(entity)
        return (entity, item)

    def action_download_finished(self, result): 
        entity, item = result
        self.update_download_item(entity, item)
        self.set_progress('Finished')


def show(): 
    user = config.Studio.current
    print('Studio: {}'.format(user))

    if user: 
        if config.isMaya: 
            from rftool.utils.ui import maya_win
            maya_win.deleteUI(uiName)
            app = FTPManager(user, maya_win.getMayaWindow())
            app.show() 
            return app
        else: 
            # app = QtWidgets.QApplication.instance()
            main_app = QtWidgets.QApplication(sys.argv)
            app = FTPManager(user)
            app.show()
            stylesheet.set_default(main_app)
            sys.exit(main_app.exec_())
    else: 
        print('No studio found! Please set "RFSTUDIO" environment before continue')


if __name__ == '__main__':
    show()


""" 
Cloud status condition 
1. no sync 
    1.1 cloud not exists 
    1.2 md5 not match 
2. sync 

md5 matching strategy 
- file count follow local
- cloud less count -> not match 
- read file md5 from json compare json from cloud 
"""

""" 
Download condition 
Zings asset - publ/hero 
Riff asset - publ/hero_zings 
    1. Ftp No - Nothing 
    2. Ftp yes Local Yes No json - Download 
    3. Ftp yes Local Yes Json Yes - Compare - Download 
    4. Ftp yes Local Yes Json Yes - Compare - Yes - Sync 
"""
