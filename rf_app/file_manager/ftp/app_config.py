import os 
import sys 


class Config: 
    owner_col = 'Owner'
    asset_col = 'Local Assets'
    status_col = 'Cloud'
    action_col = 'Action'
    localdate_col = 'Server Date'
    remotedate_col = 'FTP Date'

    tree_columns = [owner_col, asset_col, localdate_col, remotedate_col, status_col, action_col]
    column_size = [50, 280, 160, 160, 100, 100]
    user = ''
    root_config = {
        'Riff': {'upload': '/Riff', 'local': 'P:', 'download': '/Zings'}, 
        'Zings': {'upload': '/Zings', 'local': 'P:', 'download': '/Riff'}}

    owners = {
        'Riff': {'name': 'RFF', 'icon': 'icons/riff.png', 'color': (0, 0, 0)}, 
        'Zings': {'name': 'ZGS', 'icon': 'icons/zings.png', 'color': (100, 100, 160)}, 
        'Shared': {'name': 'SHR', 'icon': 'icons/shares.png', 'color': (60, 60, 60)}, 
        'Blank': {'name': 'BLNK', 'icon': 'icons/blank.png', 'color': (40, 40, 40)}}


class Color: 
    green = (100, 160, 100)
    red = (160, 100, 100)
    grey = (60, 60, 60)
    blue = (100, 120, 160)
    yellow = (160, 140, 100)
    light_grey = (100, 100, 100)
