import os 
import sys 
import tempfile
from collections import OrderedDict
from rf_utils.sg import sg_process
from rf_utils import ftp_utils
from rf_utils.context import context_info
from rf_utils.pipeline import fileinfo
from rf_utils import file_utils
sg = sg_process.sg 

MODULEDIR = os.path.dirname(__file__)


class SG: 
    asset_fields = [
        'code', 'sg_asset_type', 'sg_episode', 'tags', 'project', 
        'project.Project.sg_project_code', 'sg_scenes']
    riff_tag = 'Riff'
    zings_tag = 'Zings'


class CloudModel(object):
    """docstring for CloudModel"""
    def __init__(self, project, config):
        super(CloudModel, self).__init__()
        self.project = project 
        self.config = config
        self.project_entity = None
        self.project_code = str()
        self.ftp = ftp_utils.RFFtp()
        self.ftp.upload_root = self.root_upload
        self.ftp.download_root = self.root_download
        self.ftp.local_root = self.root_local
        self.sg_assets = list()
        self.ftp_entities_cache = list()

    @property 
    def root_upload(self): 
        return self.config.root_config[self.config.user]['upload']

    @property 
    def root_download(self): 
        return self.config.root_config[self.config.user]['download']

    @property 
    def root_local(self): 
        return self.config.root_config[self.config.user]['local']

    def fetch_project(self): 
        self.project_entity = sg.find_one('Project', [['name', 'is', self.project]], ['sg_project_code'])
        return self.project_entity

    def fetch_sg_assets(self): 
        if not self.project_entity: 
            self.fetch_project()
        filters = [['project', 'is', self.project_entity], ['sg_status_list', 'is_not', 'omt']]
        self.sg_assets = sg.find('Asset', filters, SG.asset_fields)
        return self.sg_assets

    def fetch_sg_asset(self, entity): 
        filters = [
            ['project.Project.name', 'is', entity.project], 
            ['sg_status_list', 'is_not', 'omt'], 
            ['code', 'is', entity.name]]
        return sg.find_one('Asset', filters, SG.asset_fields)

    def create_context_from_entity(self, sg_entity): 
        """ create rf context from shotgun entity """ 
        project_code = sg_entity['project.Project.sg_project_code']
        context = context_info.Context()
        context.update(
            project=self.project, 
            projectCode=project_code, 
            entityType='asset', 
            entityGrp=sg_entity['sg_asset_type'],
            entity=sg_entity['code'] 
            )
        entity = context_info.ContextPathInfo(context=context)
        entity.sg_entity = sg_entity

        # add remote user 
        entity.studio = self.config.user

        # find studio tag 
        owners = self.get_owners(sg_entity)
        entity.owners = owners

        # build keywords
        episodes = sg_entity['sg_scenes']
        asset_type = sg_entity['sg_asset_type']
        epkw = ':'.join([a['name'] for a in episodes]) if episodes else ''
        entity.keyword = '{}:{}:{}'.format(sg_entity['code'], asset_type, epkw)
        return entity

    def create_context_from_entities(self, entities): 
        """ create context from list of entities """ 
        contexts = list()
        for entity in entities: 
            contexts.append(self.create_context_from_entity(entity))
        return contexts

    def sg_asset_types(self): 
        types = list()
        if not self.sg_assets: 
            self.fetch_sg_assets()
        for asset in self.sg_assets: 
            if not asset['sg_asset_type'] in types: 
                types.append(asset['sg_asset_type'])
        return types

    def fetch_ftp_asset_from_types(self, types): 
        assets = list()
        if not self.project_entity: 
            self.fetch_project()
        for asset_type in types: 
            context = context_info.Context()
            context.update(
                project=self.project, 
                projectCode=self.project_entity['sg_project_code'], 
                entityType='asset', 
                entityGrp=asset_type,
                )
            entity = context_info.ContextPathInfo(context=context)
            type_path = '{}/{}'.format(self.root_upload, entity.path.scheme(key='publishAssetTypePath').striproot())
            assets += self.fetch_ftp_assets(type_path)
        return assets

    def get_display_owner(self, entity): 
        owners = self.get_owners(entity.sg_entity)

        if len(owners) > 1: 
            icon_path = self.config.owners.get('Shared').get('icon')
            color = self.config.owners.get('Shared').get('color')
        elif len(owners) == 1: 
            icon_path = self.config.owners.get(owners[0]).get('icon')
            color = self.config.owners.get(owners[0]).get('color')
        else: 
            icon_path = self.config.owners.get('Blank').get('icon')
            color = self.config.owners.get('Blank').get('color')
        icon_path = '{}/{}'.format(MODULEDIR, icon_path)
        return icon_path, color

    def get_owners(self, sg_entity): 
        owners = list()
        tags = sg_entity['tags']
        for tag in tags: 
            if tag['name'] in self.config.owners.keys(): 
                owners.append(tag['name'])
        return owners

    def fetch_ftp_assets(self, path): 
        """ fetch from list asset path """
        assets = list() 
        if self.ftp.exists(path): 
            assets = self.ftp.list_dirs(path)
        return assets

    def check_status_cloud(self, entity, cloud_assets): 
        """ check if ftp path exists from entity """ 
        asset_path = entity.path.scheme(key='publishAssetNamePath').striproot()
        upload_path = '{}/{}'.format(self.root_upload, asset_path)
        ftp_status = 'No'
        if upload_path in cloud_assets: 
            ftp_status = 'Yes'

        return ftp_status

    def compare_local_upload_cloud(self, entity): 
        """ compare json between local and upload cloud """ 
        asset_path = entity.path.hero().abs_path()
        info = fileinfo.HeroInfo(asset_path)
        ftpinfo = fileinfo.HeroInfo(asset_path)
        datafile = info.datafile 
        cloud_data = Path(datafile, self.config).upload
        temp = info.tmpfile
        hero_path = entity.path.hero().abs_path()
        result_dict = dict()

        sync = False 
        local_info = False
        remote_info = False 
        queue_files = list()
        na = '-'
        local_date = na
        remote_date = na
        error_message = str()

        # if hero exists 
        if os.path.exists(hero_path): 
            # local not exists 
            if entity.studio in entity.owners: 
                if not os.path.exists(datafile): 
                    print('Local not ready, create local info then sync') 
                    local_info = False
                    sync = False 
                    try: 
                        info.update()
                        queue_files = info.get_files()
                        print('generate fileinfo and get files')
                        local_date = info.data['date']
                    except Exception as e: 
                        print('Cannot write to disk {}'.format(hero_path))
                        print(e)
                        error_message = 'Disk Error'
                    # files = info.get_files()
                    # self.ftp.upload_files(files)
                else: 
                    local_info = True
                    local_date = info.data['date']
                    # begin compare
                    if self.ftp.exists(cloud_data): 
                        remote_info = True
                        print('Checking remote files {} ...'.format(cloud_data))
                        self.ftp.download(cloud_data, temp)
                        ftpinfo = fileinfo.HeroInfo(asset_path)
                        ftpinfo.read_from_file(temp)
                        remote_date = ftpinfo.data['date']
                        queue_files = info.compare(temp)
                        if queue_files: 
                            sync = False
                            print('{} files need sync'.format(len(queue_files)))
                            # self.ftp.upload_files(queue_files)
                        else: 
                            sync = True
                            print('All files Sync')
                        print('delete temp')
                        os.remove(temp)
                    # remote not found
                    else: 
                        remote_info = False
                        sync = False
                        print('Remote not found, need sync')
                        # files = info.get_files()
                        # self.ftp.upload_files(files)
                        queue_files = info.get_files()
            else: 
                print('You are not the owner of this asset')
                sync = False

        # add datafile back to queue 
        if not sync and queue_files: 
            if not datafile in queue_files: 
                queue_files.append(datafile)
        print('Finished sync')
        result_dict = {
            'sync': sync, 
            'local_info': local_info, 
            'remote_info': remote_info, 
            'queue_files': queue_files, 
            'local_date': local_date, 
            'remote_date': remote_date, 
            'error_message': error_message}
        return result_dict

    def compare_local_download_cloud(self, entity): 
        # Zings asset - publ/hero 
        # Riff asset - publ/hero_zings 
        #     1. Ftp No - Nothing 
        #     2. Ftp yes Local Yes No json - Download 
        #     3. Ftp yes Local Yes Json Yes - Compare - Download 
        #     4. Ftp yes Local Yes Json Yes - Compare - Yes - Sync  
        sync = False 
        local_info = False
        remote_info = False 
        queue_files = list()
        result_dict = dict()
        na = '-'
        local_date = na 
        remote_date = na 
        error_message = str()

        asset_path = entity.path.hero().abs_path()
        info = fileinfo.HeroInfo(asset_path)
        ftpinfo = fileinfo.HeroInfo(asset_path)
        temp = info.tmpfile
        datafile = info.datafile 
        cloud_data = Path(datafile, self.config).download
        hero_path = entity.path.hero().abs_path()
        direct_download = True

        print('-----------------')
        print(entity.name)

        if self.ftp.exists(cloud_data): 
            # find local to compare 
            # check owner 
            # if studio and owner match, use 'work'
            # if studio and owner not match user 'publ'
            remote_info = True
            print('Checking remote files {} ...'.format(cloud_data))
            self.ftp.download(cloud_data, temp)
            src_file = temp
            ftpinfo.read_from_file(src_file)
            remote_date = ftpinfo.data['date']

            if entity.studio in entity.owners: 
                direct_download = False
                print('{} work on this asset, do not replace hero. download to work'.format(entity.studio))
                dst_file = Path(datafile, self.config).work() # this is hardcoding 
                local_hero = Path(asset_path, self.config).work() # this is hardcoding 
            else: 
                direct_download = True
                dst_file = datafile
                local_hero = asset_path
                print("{} not work on this asset, download to hero directly".format(entity.studio))

            if not os.path.exists(local_hero) or not os.path.exists(dst_file): 
                print('Action - No local. Download all')
                info.read_from_file(src_file)
                queue_files = info.get_files()
                print('reading from {}'.format(src_file))
                print(queue_files)
            else: 
                local_info = True
                queue_files = info.compare_files(src_file, dst_file)
                print('Found local path {}'.format(dst_file))
                print('Action - Comparing changes ...')
                info.read_from_file(dst_file)
                local_date = info.data['date']
                if not queue_files: 
                    print('All files sync')
                    sync = True 

            if not direct_download: 
                queue_files = [Path(a, self.config).work() for a in queue_files]

            if queue_files: 
                pass
                # print('Download unsync files {}'.format(queue_files))

            os.remove(temp)
        else: 
            remote_info = False
            print('Nothing to download')

        result_dict = {
            'sync': sync, 
            'local_info': local_info, 
            'remote_info': remote_info, 
            'queue_files': queue_files, 
            'local_date': local_date, 
            'remote_date': remote_date, 
            'error_message': error_message}
        return result_dict

    def add_check_cloud_entities(self, entity): 
        asset_path = entity.path.scheme(key='publishAssetNamePath').striproot()
        upload_path = '{}/{}'.format(self.root_upload, asset_path)
        if not self.ftp_entities_cache: 
            self.ftp_entities_cache = self.ftp.list_dirs(upload_path)

        return True if entity.name in self.ftp_entities_cache else False

    def add_upload_status_entity(self, entity): 
        result_dict = self.compare_local_upload_cloud(entity)
        sync = result_dict['sync']
        local_info = result_dict['local_info']
        remote_info = result_dict['remote_info']
        queue_files = result_dict['queue_files']
        local_date = result_dict['local_date']
        remote_date = result_dict['remote_date']
        error_message = result_dict['error_message']

        status = {
            'sync': sync, 
            'local_info': local_info, 
            'remote_info': remote_info, 
            'queue_files': queue_files, 
            'local_date': local_date, 
            'remote_date': remote_date, 
            'error_message': error_message}
        entity.context.status = status
        return entity

    def add_download_status_entity(self, entity): 
        print('check_download_status_entity entity', entity)
        result_dict = self.compare_local_download_cloud(entity)
        sync = result_dict['sync']
        local_info = result_dict['local_info']
        remote_info = result_dict['remote_info']
        queue_files = result_dict['queue_files']
        local_date = result_dict['local_date']
        remote_date = result_dict['remote_date']
        error_message = result_dict['error_message']

        status = {
            'sync': sync, 
            'local_info': local_info, 
            'remote_info': remote_info, 
            'queue_files': queue_files, 
            'local_date': local_date, 
            'remote_date': remote_date, 
            'error_message': error_message}
        entity.context.status = status
        return entity


    def sync_remote(self, entity): 
        files = entity.context.status['queue_files']
        asset_path = entity.path.hero().abs_path()
        info = fileinfo.HeroInfo(asset_path)

        if not info.datafile in files: 
            files.append(info.datafile)
        # asset_path = entity.path.hero().abs_path()
        # info = fileinfo.HeroInfo(asset_path)
        # files = info.get_files()
        self.ftp.upload_files(files)
        return True

    def update_local_info(self, entity): 
        asset_path = entity.path.hero().abs_path()
        info = fileinfo.HeroInfo(asset_path)
        info.update()
        return True

    def sync_local(self, entity): 
        """ download from remote to local """ 
        info = fileinfo.HeroInfo(entity.path.hero().abs_path())
        localinfo = info.datafile
        cloud_data = Path(info.datafile, self.config).download
        print('cloud_data', cloud_data)
        self.ftp.download(cloud_data, info.tmpfile)
        info.read_from_file(info.tmpfile)
        files = entity.context.status['queue_files']
        not_exists = list()

        for file in files: 
            publish_path = Path(file, self.config).publish()
            ftp_path = Path(publish_path, self.config).download
            # print('download {}'.format(ftp_path))
            # print('to {}'.format(file))
            if self.ftp.exists(ftp_path): 
                print('Downloading {} ...'.format(file))
                self.ftp.download(ftp_path, file)
                origin_md5 = info.get_md5(publish_path)
                print('Checking with hash {}'.format(origin_md5))
                download_md5 = file_utils.md5(file)
                if origin_md5 == download_md5: 
                    print('Download complete!')
                else: 
                    print('Hash not match. Download might be incomplete')
            else: 
                not_exists.append(ftp_path)
                print('{} not exists! skip download.'.format(ftp_path))

        if files: 
            print('Copy heroinfo')
            file_utils.copy(info.tmpfile, localinfo)

        if not_exists: 
            return not_exists

    def update_asset_info(self, entity): 
        info = fileinfo.HeroInfo(entity.path.hero().abs_path())
        info.update()

    def get_tags(self, entities): 
        tag_dict = OrderedDict()

        for entity in entities:
            entity_tag = self.get_entity_tags(entity)

            for k, v in entity_tag.items():
                if not k in tag_dict.keys():
                    tag_dict[k] = v

        return tag_dict

    def get_entity_tags(self, entity):
        tag_dict = OrderedDict()
        tags = entity.get('tags') or dict()

        for tag in tags:
            if not tag['name'] in tag_dict.keys():
                tag_dict[tag['name']] = tag

        return tag_dict


class Path(object):
    """docstring for Path"""
    def __init__(self, path, config):
        super(Path, self).__init__()
        self.config = config
        self.path = path
        self.root_local = config.root_config[config.user]['local'] 
        self.root_upload = config.root_config[config.user]['upload']
        self.root_download = config.root_config[config.user]['download']

    def __str__(self): 
        return self.path 

    def __repr__(self): 
        return self.path 

    @property 
    def local(self): 
        if self.root_local in self.path: 
            return self.path
        elif self.root_upload in self.path: 
            self.path = self.path.replace(self.root_upload, self.root_local)
        elif self.root_download in self.path: 
            self.path = self.path.replace(self.root_download, self.root_local)
            return self.path

    @property
    def upload(self): 
        if self.root_local in self.path: 
            self.path = self.path.replace(self.root_local, self.root_upload)
            return self.path
        elif self.root_upload in self.path: 
            return self.path

    @property
    def download(self): 
        if self.root_local in self.path: 
            self.path = self.path.replace(self.root_local, self.root_download)
            return self.path
        elif self.root_download in self.path: 
            return self.path

    def work(self): 
        return self.path.replace('/publ', '/work')

    def publish(self): 
        return self.path.replace('/work', '/publ')
        


def upload_asset(entity, user='Riff'): 
    from . import app_config
    app_config.Config.user = user
    model = CloudModel(entity.project, app_config.Config)
    print('Updating local hero info ...')
    model.update_local_info(entity)
    sg_entity = model.fetch_sg_asset(entity)
    entity = model.create_context_from_entity(sg_entity)
    model.add_upload_status_entity(entity)
    print('Uploading files ...')
    model.sync_remote(entity)
    print('Upload complete')
    return True
