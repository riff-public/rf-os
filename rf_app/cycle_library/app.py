# v.0.0.1 wip
# v.0.0.2 add multiple actions 
_title = 'Riff Cycle Library'
_version = 'v.0.0.2'
_des = 'support multiple actions'
uiName = 'SGCycleLibrary'

#Import python modules
import sys
import os
import logging
import getpass
from collections import OrderedDict
from functools import partial

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

# import config
import rf_config as config

# framework modules
from rf_utils import log_utils
from rf_utils.ui import load
from rf_utils.ui import stylesheet

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

import ui
reload(ui)
import override_ui
reload(override_ui)
import utils
reload(utils)
from rf_utils.context import context_info
from rf_utils.pipeline import user_pref
from rf_utils.sg import sg_thread
reload(sg_thread)

class Tab: 
    names = ['Create', 'Work', 'Qc', 'Export', 'Publish']

class Color:
    bggreen = 'background-color: rgb(0, 160, 0);'
    bgred = 'background-color: rgb(160, 0, 0);'


class RFCycleManager(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        #Setup Window
        super(RFCycleManager, self).__init__(parent)

        # ui read
        self.ui = ui.Ui_CycleLibraryUi()
        self.ui.setupUi(self)
        
        # override ui from designer 
        self.overrideUi = override_ui.PipelineWidget(self.ui)
        self.overrideUi.setupWidget()

        # self.setCentralWidget(self.ui)
        self.setObjectName(uiName)
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        # self.resize(1200, 750)

        self.init_functions()
        self.init_signals()

    def init_functions(self): 
        # prefs
        self.pref = user_pref.ToolSetting(uiName)

        # check scene context 
        self.restoreData = self.check_restore_data()
        self.restore_project_pref()

        # app 
        self.app = 'maya'
        self.step = 'anim'
        self.res = 'md'

        # start 
        self.set_worktab_mode()
        self.set_type() 
        self.fetch_asset()

        # tab one time load
        self.userLoaded = False
        self.qcLoaded = False
        self.exportWidgetLoaded = False

        # vars 
        self.qcPassed = False

        # create tab 
        # self.fetch_user()

    def init_signals(self): 
        self.ui.projectWidget.projectChanged.connect(self.project_changed)
        self.ui.assetType_comboBox.currentIndexChanged.connect(self.type_changed)
        self.ui.searchAsset_lineEdit.textChanged.connect(self.searchAsset_changed)

        self.ui.asset_listWidget.selected.connect(self.asset_selected)
        self.ui.cycle_listWidget.selected.connect(self.cycle_selected)
        self.ui.cycle_listWidget.listWidget.customContextMenuRequested.connect(self.show_cycle_menu)

        # add cycle 
        self.ui.add_pushButton.clicked.connect(self.add_cycle)

        # workspace tab 
        self.ui.create_pushButton.clicked.connect(self.create_file)
        self.ui.save_pushButton.clicked.connect(self.save_file)
        self.ui.open_pushButton.clicked.connect(self.open_file)
        self.ui.rig_pushButton.clicked.connect(self.reference_rig)
        self.ui.camera_pushButton.clicked.connect(self.create_camera)

        # create tab 
        self.ui.createCycle_pushButton.clicked.connect(self.create_cycle)

        # tab changed 
        self.ui.tabWidget.currentChanged.connect(self.tab_changed)

        # qc label 
        self.ui.qcWidget.check.connect(self.qc_signal)

    def show_cycle_menu(self, pos): 
        menu = QtWidgets.QMenu(self)
        self.action_menu(menu)
        self.create_preview_menu(menu)
        self.create_oa_menu(menu)
        self.open_explorer_menu(menu)
        menu.popup(self.ui.cycle_listWidget.listWidget.mapToGlobal(pos))

    def action_menu(self, menu):
        taskEntity = self.ui.cycle_listWidget.current_item()
        menuData = taskEntity.get('menu')

        for data in menuData: 
            menuType = data.get('type')

            if menuType == 'menu': 
                actionMenu = menu.addAction(data.get('display'))
                filename = data.get('file')
                action = data.get('action')
                status = data.get('status')

                actionMenu.setEnabled(status)
                actionMenu.triggered.connect(partial(self.menu_selected, filename, action))
                menu.addSeparator()

            if menuType == 'submenu': 
                displays = data.get('displays')

                if displays: 
                    actionMenus = QtWidgets.QMenu(data.get('display'), self)
                    menu.addMenu(actionMenus)

                    for i, display in enumerate(displays): 
                        submenu = actionMenus.addAction(display)
                        filename = data.get('file')[i]
                        action = data.get('action')
                        status = data.get('status')[i]

                        submenu.setEnabled(status)
                        submenu.triggered.connect(partial(self.menu_selected, filename, action))


    def create_preview_menu(self, menu): 
        previewMenu = menu.addAction('create preview')
        previewMenu.triggered.connect(self.create_preview)
        menu.addSeparator()

    def create_oa_menu(self, menu): 
        res = self.asset.projectInfo.asset.medium
        rigOaFile = utils.get_miarmy_oa(self.asset, res=res, absPath=True)
        rigOaFileRel = utils.get_miarmy_oa(self.asset, res=res, absPath=False)

        if os.path.exists(rigOaFile): 
            menu.addSeparator()
            oaMenu = menu.addAction('Reference Oa Rig')
            oaMenu.setEnabled(os.path.exists(rigOaFile))
            oaMenu.triggered.connect(partial(self.reference_oa_rig, rigOaFileRel, res))

    def open_explorer_menu(self, menu): 
        openExpMenu = menu.addAction('Open in Explorer')
        openExpMenu.triggered.connect(self.open_explorer)

    def open_explorer(self):
        utils.open_workspace_explorer(self.asset)

    def create_preview(self): 
        utils.create_playblast(self.asset)

    def menu_selected(self, path, action): 
        """ menu callback """ 
        utils.action(path, action)

    def check_restore_data(self): 
        self.prefData = self.pref.read()
        print self.prefData, 'load'
        asset = utils.scene_context()
        if not asset: 
            return self.prefData
        else: 
            prefDict = OrderedDict()
            prefDict['project'] = asset.project
            prefDict['assetType'] = asset.type
            prefDict['assetName'] = asset.name
            prefDict['taskName'] = asset.process
            return prefDict

    def project_changed(self, projectEntity):
        """ project changed """
        self.set_type(projectEntity)
        self.save_pref()

    def type_changed(self): 
        self.save_pref()
        self.fetch_asset()

    def set_type(self, projectEntity=None): 
        if not projectEntity: 
            projectEntity = self.ui.projectWidget.current_item()
        types = utils.list_type(projectEntity)
        self.ui.assetType_comboBox.clear()
        self.ui.assetType_comboBox.addItems(types)

        # restore selections 
        self.restore_type_pref()

    def fetch_asset(self): 
        projectEntity = self.ui.projectWidget.current_item()
        assetType = str(self.ui.assetType_comboBox.currentText())
        sgEntityType = 'Asset'
        extraFilters = [['sg_asset_type', 'is', assetType]]

        # thread 
        sg_thread.Config.memoryCache = False 
        self.sgThread = sg_thread.GetEntity(projectEntity, sgEntityType, extraFilters=extraFilters)
        self.sgThread.value.connect(self.fetch_finished)
        self.sgThread.start()
        self.set_loading()

    def fetch_finished(self, sgEntities): 
        """ shotgun send fetching data """ 
        sortedDict = sg_thread.sg_utils.ordered_entity_dict(sgEntities)
        self.sortedEntities = [v for k, v in sortedDict.iteritems()]
        self.populate_asset(self.sortedEntities)

        # unlock comboBox 
        self.ui.assetType_comboBox.setEnabled(True)
        self.ui.searchAsset_lineEdit.setEnabled(True)

    def fetch_user(self): 
        """ fetching user from shotgun """ 
        self.sgUser = sg_thread.GetUsers()
        self.sgUser.value.connect(self.fetch_user_finished)
        self.sgUser.start()

    def fetch_user_finished(self, userEntities): 
        # print 'finished'
        # print userEntities
        # userEntities = utils.filter_users(userEntities, groups=['Animator'])
        self.ui.user_comboBox.clear()
        self.ui.user_comboBox.add_items(userEntities)
        self.userLoaded = True

    def searchAsset_changed(self): 
        self.populate_asset()

    def populate_asset(self, entities=None): 
        """ populate asset list """ 
        if not entities: 
            entities = self.sortedEntities

        # filter search 
        searchKey = str(self.ui.searchAsset_lineEdit.text())
        if searchKey: 
            entities = [a for a in entities if searchKey in a['code']]

        self.ui.asset_listWidget.clear()
        self.ui.asset_listWidget.add_items(entities)
        self.ui.asset_listWidget.set_text_mode()

        # restore pref 
        self.restore_asset_pref()

    def asset_selected(self, sgEntity): 
        self.asset = self.setup_context()
        self.fetch_tasks(sgEntity)
        self.save_pref()

    def fetch_tasks(self, sgEntity=None, refresh=False): 
        """ fetching task (loop) from shotgun """ 
        # refresh mean do not use cache 
        projectEntity = self.ui.projectWidget.current_item()
        if not sgEntity: 
            sgEntity = self.ui.asset_listWidget.current_item()

        # thread fetching tasks 
        self.sgThreadTask = sg_thread.GetTasks(projectEntity, sgEntity, step=self.step, refresh=refresh)
        self.sgThreadTask.value.connect(self.fetch_task_finished)
        self.sgThreadTask.start()
        self.set_task_loading()

    def set_task_loading(self): 
        self.ui.cycle_listWidget.listWidget.clear()
        self.ui.cycle_listWidget.listWidget.addItem('Loading ...')

    def fetch_task_finished(self, taskEntities): 
        # print 'taskEntities', taskEntities
        # asset entity now contain only asset 
        # self.taskEntities = taskEntities
        self.taskEntities = utils.combine_additional_data(self.asset, taskEntities)
        self.populate_tasks(self.taskEntities)

    def populate_tasks(self, taskEntities=None): 
        if not taskEntities: 
            taskEntities = self.taskEntities
        self.ui.cycle_listWidget.clear() 
        
        if taskEntities: 
            self.ui.cycle_listWidget.add_items(taskEntities, widget=True)

            # restore selection 
            taskName = self.restoreData.get('taskName')
            self.ui.cycle_listWidget.set_current_item(taskName)
            taskEntity = self.ui.cycle_listWidget.current_item()
            self.cycle_selected(taskEntity)

        else: 
            self.ui.cycle_listWidget.set_text('No Item')

    def set_loading(self): 
        # loading page 
        self.ui.asset_listWidget.clear()
        self.ui.asset_listWidget.listWidget.addItem('Loading ...')

        # lock comboBox 
        self.ui.assetType_comboBox.setEnabled(False)
        self.ui.searchAsset_lineEdit.setEnabled(False)

    def cycle_selected(self, taskEntity): 
        self.asset = self.setup_context()
        self.init_workspace(self.asset)
        self.save_pref()
        self.check_rig_status()

        self.qcLoaded = False
        # self.exportWidgetLoaded = False
        self.qcPassed = False

        taskEntity = self.ui.cycle_listWidget.current_item()
        self.ui.tabWidget.setCurrentIndex(1)

    def add_cycle(self): 
        # set tab 
        index = override_ui.Tab.names.index('Create')
        self.ui.tabWidget.setCurrentIndex(index)

    def create_file(self): 
        """ create workspace and file to start with """ 
        if self.asset: 
            utils.create_workspace(self.asset.copy())
            utils.create_file(self.asset.copy())
            self.init_workspace(self.asset)

    def save_file(self): 
        """ save increment files """ 
        utils.save_file(self.asset)
        self.init_workspace(self.asset)

    def open_file(self): 
        """ open file from selection """ 
        filePath = self.ui.fileWidget.current_data()
        utils.open_file(filePath)

    def reference_rig(self): 
        """ reference rig that match the asset entity """ 
        # get from selected asset 
        taskEntity = self.ui.cycle_listWidget.current_item()
        if not taskEntity: 
            rigPath = taskEntity.get('rig', dict()).get('file')
            status = taskEntity.get('rig', dict()).get('status')

            if status: 
                utils.create_reference(self.asset, rigPath)
            else: 
                QtWidgets.QMessageBox.warning(self, 'Maya', 'Rig not exists')
        else: 
            QtWidgets.QMessageBox.warning(self, 'Maya', 'No cycle selected')

    def reference_oa_rig(self, path, res, entity=None): 
        from rf_app.sg_manager import dcc_hook
        entity = self.asset if not entity else entity
        look = 'main'
        entity.context.update(res=res, look=look)
        agents = dcc_hook.create_miarmyOa_reference(entity, path)

        if not agents: 
            namespace = dcc_hook.miarmy_oa_namespace(entity)
            dialog.MessageBox.warning('maya', 'Namespace %s exists' % namespace)
        
        if agents: 
            dcc_hook.group_oa(agents) 

    def create_camera(self): 
        utils.create_camera()

    def check_rig_status(self): 
        taskEntity = self.ui.cycle_listWidget.current_item()
        rigPath = taskEntity.get('rig').get('file')
        status = taskEntity.get('rig').get('status')
        # ---- TEMPORARY COMMENTED OUT BY NU
        # rigPath = taskEntity.get('rig', dict()).get('file')
        # status = taskEntity.get('rig', dict()).get('status')


        if status: 
            self.ui.rig_pushButton.setEnabled(True)
        else: 
            self.ui.rig_pushButton.setEnabled(False)

    def init_workspace(self, asset): 
        if asset: 
            files = utils.list_workspace(asset)
            self.ui.fileWidget.clear()
            if files == None: 
                self.ui.fileWidget.set_empty('No workspace')
                self.set_worktab_mode('create')
            elif files: 
                self.ui.fileWidget.add_files(files)
                self.set_worktab_mode()
            else: 
                self.ui.fileWidget.set_empty()
                self.set_worktab_mode('create')

    def create_cycle(self): 
        """ create cycle task and folder """ 
        taskName = str(self.ui.cycleName_lineEdit.text())
        
        projectEntity = self.ui.projectWidget.current_item()
        assetEntity = self.ui.asset_listWidget.current_item()

        taskEntities = self.ui.cycle_listWidget.all_items()
        tasks = [a.get('content') for a in taskEntities if type(a) == type(dict())]
        
        if not taskName in tasks: 
            # create shotgun task 
            self.sgCreateTask = sg_thread.CreateTask(projectEntity, assetEntity, self.step, taskName, self.res)
            self.sgCreateTask.value.connect(self.create_task_finished)
            self.sgCreateTask.start()

            # create folder 
            utils.create_cycle_folder(self.asset, taskName)
        else: 
            QtWidgets.QMessageBox.warning(self, 'Maya', '"%s" already exists' % taskName)

    def create_task_finished(self, taskEntity): 
        # re fetch task 
        sgEntity = self.ui.asset_listWidget.current_item()
        self.fetch_tasks(sgEntity, refresh=True)
        QtWidgets.QMessageBox.information(self, 'Maya', 'Create task finished "%s"' % taskEntity.get('content'))

    def tab_changed(self, index): 
        tabName = Tab.names[index]
        if tabName == 'Create': 
            if not self.userLoaded: 
                self.fetch_user()

        if tabName == 'Qc': 
            if not self.qcLoaded: 
                self.ui.qcWidget.configFilter = True
                self.ui.qcWidget.enable_console(True)
                self.ui.qcWidget.update(project=self.asset.project, entityType=self.asset.entity_type, step=self.asset.step, entity=self.asset, res=self.asset.res)
                self.qcLoaded = True


        if tabName == 'Export': 
            if not self.exportWidgetLoaded: 
                self.overrideUi.setup_exporttab()
                self.ui.outputWidget.add_res(self.asset.list_res())
                self.exportWidgetLoaded = True
                self.ui.outputWidget.enable_console(True)

            self.ui.outputWidget.add_res(self.asset.list_res())

            self.ui.outputWidget.exportButton.setEnabled(self.qcPassed)

            sceneEntity = utils.scene_context()
            matchContext = utils.check_matching_context(self.asset, sceneEntity)
            if not matchContext: 
                QtWidgets.QMessageBox.warning(self, 'Maya', 'Open file not match asset and cycle selection. Cannot Export')

            else: 
                self.ui.outputWidget.update_entity(self.asset)
                return 

    def set_worktab_mode(self, mode=''): 
        if mode == 'create': 
            self.ui.open_pushButton.setVisible(False)
            self.ui.save_pushButton.setVisible(False)
            self.ui.create_pushButton.setVisible(True)

        else: 
            self.ui.open_pushButton.setVisible(True)
            self.ui.save_pushButton.setVisible(True)
            self.ui.create_pushButton.setVisible(False)

    def qc_signal(self, value): 
        """ signal if qc complete all """
        if value: 
            label = 'Qc Passed'
            color = Color.bggreen
            self.qcPassed = True
        else: 
            label = 'Qc not completed. Cannot export.'
            color = Color.bgred
            self.qcPassed = False

        self.ui.qc_label.setText(label)
        self.ui.qc_label.setStyleSheet(color)

    def setup_context(self): 
        """ setup context based on ui selection """ 
        projectEntity = self.ui.projectWidget.current_item()
        projectName = projectEntity.get('name')
        assetType = str(self.ui.assetType_comboBox.currentText())
        assetName = self.ui.asset_listWidget.current_text()
        taskEntity = self.ui.cycle_listWidget.current_item()
        taskName = taskEntity.get('content') if taskEntity else ''

        if projectName and assetType and assetName: 
            context = context_info.Context()
            context.update(project=projectName, entityType='asset', step=self.step, res='md')
            context.update(entity=assetName, entityGrp=assetType, app=self.app)
            context.update(process=taskName)
            asset = context_info.ContextPathInfo(context=context)
            return asset
        else: 
            logger.debug('Context input %s %s %s %s' % (projectName, assetType, assetName, taskName))
            logger.warning('Cannot generate path from current context')

    def restore_project_pref(self): 
        """ restore project pref """ 
        self.ui.projectWidget.set_current_item(self.restoreData['project'], signal=False) if self.restoreData.get('project') else None

    def restore_type_pref(self): 
        """ restore type pref """ 
        assetType = self.restoreData.get('assetType')
        types = [self.ui.assetType_comboBox.itemText(a) for a in range(self.ui.assetType_comboBox.count())]
        index = 0 
        if assetType in types: 
            index = types.index(assetType)
        self.ui.assetType_comboBox.setCurrentIndex(index)

    def restore_asset_pref(self): 
        """ restore type pref """ 
        assetName = self.restoreData.get('assetName')
        self.ui.asset_listWidget.set_current(assetName, signal=False)


    def save_pref(self):
        """ save selection prefs """ 
        project = self.ui.projectWidget.current_project()
        assetType = str(self.ui.assetType_comboBox.currentText())
        assetName = self.ui.asset_listWidget.current_text()
        taskItem = self.ui.cycle_listWidget.current_item()
        taskName = ''
        if taskItem: 
            taskName = taskItem.get('content')

        prefDict = OrderedDict()
        prefDict['project'] = project
        prefDict['assetType'] = assetType or str()
        prefDict['assetName'] = assetName or str()
        if taskName: 
            logger.debug('write task %s' % taskName)
            prefDict['taskName'] = taskName 

        self.pref.write(prefDict)
        print prefDict
        logger.debug('Saved preference')
        

def show(): 
    if config.isMaya:
        from rftool.utils.ui import maya_win
        maya_win.deleteUI(uiName)
        myApp = RFCycleManager(maya_win.getMayaWindow())
        myApp.show()
        return myApp
    else:
        logger.info('Run in standalone\n')
        app = QtWidgets.QApplication.instance()
        if not app:
            app = QtWidgets.QApplication(sys.argv)
        myApp = RFCycleManager()
        myApp.show()
        stylesheet.set_default(app)
        sys.exit(app.exec_())


def refresh(): 
    from rf_utils.ui import pyuic
    ui = '%s/ui.ui' % (moduleDir)
    pyuic.convert(ui)
