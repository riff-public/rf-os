import os
import sys
import traceback

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_utils import icon
from rf_utils.widget import project_widget
from rf_utils.widget import file_comboBox
from rf_utils.widget import sg_entity_widget
from rf_utils.widget import ad_input_widget
from rf_utils.widget import auto_build_widget
from rf_utils.widget import look_widget
from rf_utils.widget import info_widget
from rf_utils.widget import sg_input_widget
from rf_utils.widget import file_widget
from rf_utils.widget import user_widget
from rf_utils.widget import publish_space_widget
from rf_utils.widget import process_widget
from rf_utils.widget import cycle_widget
from rf_app.export import output_widget
from rf_utils.sg import sg_utils
from rf_qc import qc_widget
reload(sg_entity_widget)
reload(cycle_widget)
reload(user_widget)
reload(output_widget)

class Tab: 
    names = ['Create', 'Work', 'Qc', 'Export', 'Publish']


class PipelineWidget(object):
    """ Override custom widgets Ui converted from Qtdesigner """
    def __init__(self, ui):
        super(PipelineWidget, self).__init__()
        self.ui = ui
        
    def setupWidget(self): 
        # override each elements 
        self.add_logo()
        self.add_project_widget()
        self.add_asset_type_widget()
        self.add_asset_widget()
        self.add_cycle_widget()

        # tabs 
        self.setup_worktab()
        self.setup_qctab()
        self.setup_createtab()
        # self.setup_exporttab()


    def add_project_widget(self): 
        self.ui.projectWidget = project_widget.SGProjectComboBox(sg=sg_utils.sg, layout=QtWidgets.QVBoxLayout())
        self.ui.projectWidget.allLayout.setSpacing(8)
        self.ui.navigation_layout.addWidget(self.ui.projectWidget, 1, 0, 1, 1)


    def add_asset_type_widget(self): 
        # asset type 
        self.ui.assetTypeLayout = QtWidgets.QVBoxLayout()
        self.ui.assetType_label = QtWidgets.QLabel('Type')
        self.ui.assetType_comboBox = QtWidgets.QComboBox()
        self.ui.assetTypeLayout.addWidget(self.ui.assetType_label)
        self.ui.assetTypeLayout.addWidget(self.ui.assetType_comboBox)
        self.ui.navigation_layout.addLayout(self.ui.assetTypeLayout, 2, 0, 1, 1)


    def add_asset_widget(self): 
        # asset layout 
        self.ui.asset_layout = QtWidgets.QVBoxLayout()

        # widgets 
        self.ui.searchAsset_lineEdit = QtWidgets.QLineEdit()
        self.ui.asset_listWidget = sg_entity_widget.EntityListWidget()

        self.ui.asset_layout.addWidget(self.ui.searchAsset_lineEdit)
        self.ui.asset_layout.addWidget(self.ui.asset_listWidget)
        self.ui.all_layout.addLayout(self.ui.asset_layout, 2, 0, 1, 1)


    def add_cycle_widget(self): 
        # cycle layout 
        self.ui.cycle_layout = QtWidgets.QVBoxLayout()
        self.ui.header_layout = QtWidgets.QHBoxLayout()

        # widgets 
        self.ui.add_label = QtWidgets.QLabel('Add cycle')
        self.ui.add_pushButton = QtWidgets.QPushButton('+')
        self.ui.cycle_listWidget = cycle_widget.CycleListWidget()
        self.ui.cycle_listWidget.listWidget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        self.ui.header_layout.addItem(self.spacerItem)
        self.ui.header_layout.addWidget(self.ui.add_label)
        self.ui.header_layout.addWidget(self.ui.add_pushButton)

        self.ui.cycle_layout.addLayout(self.ui.header_layout)
        self.ui.cycle_layout.addWidget(self.ui.cycle_listWidget)

        self.ui.cycle_layout.setSpacing(0)
        self.ui.header_layout.setSpacing(0)

        self.ui.header_layout.setStretch(0, 8)
        self.ui.header_layout.setStretch(1, 1)
        self.ui.header_layout.setStretch(2, 1)

        self.ui.all_layout.addLayout(self.ui.cycle_layout, 2, 1, 1, 1)


    def add_logo(self): 
        # set logo 
        self.ui.logo_label.setPixmap(QtGui.QPixmap(icon.logo).scaled(64, 64, QtCore.Qt.KeepAspectRatio))
        self.ui.logo_label.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)


    def setup_worktab(self): 
        # label 
        self.ui.work_label = QtWidgets.QLabel('File: ')
        # file widget 
        self.ui.fileWidget = file_widget.FileListWidgetExtended()

        # save button 
        self.ui.open_pushButton = QtWidgets.QPushButton('Open')
        self.ui.save_pushButton = QtWidgets.QPushButton('Save ++')
        self.ui.create_pushButton = QtWidgets.QPushButton('Create file')

        self.ui.line = QtWidgets.QFrame()
        self.ui.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.ui.line.setFrameShadow(QtWidgets.QFrame.Sunken)

        self.ui.ref_layout = QtWidgets.QHBoxLayout()
        self.ui.sceneSetup_label = QtWidgets.QLabel('Scene setup')
        self.ui.rig_pushButton = QtWidgets.QPushButton('Reference Rig')
        self.ui.camera_pushButton = QtWidgets.QPushButton('Setup Camera')

        # add layout
        self.ui.work_layout.addWidget(self.ui.work_label)
        self.ui.work_layout.addWidget(self.ui.fileWidget)
        self.ui.work_layout.addWidget(self.ui.open_pushButton)
        self.ui.work_layout.addWidget(self.ui.save_pushButton)
        self.ui.work_layout.addWidget(self.ui.create_pushButton)

        self.ui.work_layout.addWidget(self.ui.line)
        self.ui.work_layout.addWidget(self.ui.sceneSetup_label)
        self.ui.ref_layout.addWidget(self.ui.rig_pushButton)
        self.ui.ref_layout.addWidget(self.ui.camera_pushButton)
        self.ui.work_layout.addLayout(self.ui.ref_layout)

        self.ui.rig_pushButton.setEnabled(False)


    def setup_qctab(self): 
        self.ui.qcWidget = qc_widget.QcWidget()
        self.ui.qc_layout.addWidget(self.ui.qcWidget)
        
        bgred = 'background-color: rgb(160, 0, 0);'
        self.ui.qc_label.setText('Qc not completed. Cannot export.')
        self.ui.qc_label.setStyleSheet(bgred)


    def setup_createtab(self): 
        self.ui.user_comboBox.deleteLater()
        self.ui.user_comboBox = user_widget.SGUserComboBox()
        self.ui.user_comboBox.label.setText('Assigned to: ')
        self.ui.create_layout.insertWidget(3, self.ui.user_comboBox, 0)

    def setup_exporttab(self): 
        self.ui.outputWidget = output_widget.OutputWidget()
        self.ui.export_layout.insertWidget(2, self.ui.outputWidget, 1)
        self.ui.outputWidget.exportButton.setEnabled(False)
