import os
import sys 
from rf_utils import file_utils
import logging 
import subprocess

from rf_utils.context import context_info
from rf_app.save_plus import save_utils 
reload(save_utils)
from rf_app.sg_manager import dcc_hook

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())


def list_type(projectEntity): 
    """ find asset type path """
    context = context_info.Context()
    context.update(project=projectEntity.get('name'), entityType='asset')
    asset = context_info.ContextPathInfo(context=context)
    typePath = asset.path.type().abs_path()
    path = os.path.split(typePath)[0]
    types = []
    if os.path.exists(path): 
        try: 
            types = file_utils.list_folder(path)
        except Exception as e: 
            types = []
    return types


def list_workspace(entity): 
    workspacePath = entity.path.workspace().abs_path()
    if not os.path.exists(workspacePath): 
        logger.warning('Path not exists : %s' % workspacePath)
        return None
    files = file_utils.list_file(workspacePath)
    if not files: 
        logger.debug('No files')
    return ['%s/%s' % (workspacePath, a) for a in files]


def open_workspace_explorer(entity): 
    path = entity.path.workspace().abs_path()
    subprocess.Popen(r'explorer /select,"%s"' % path.replace('/', '\\'))


def create_workspace(entity): 
    workspacePath = entity.path.workspace().abs_path()
    if not os.path.exists(workspacePath): 
        os.makedirs(workspacePath)
        logger.debug('workspace created %s' % workspacePath)
        return True

def combine_additional_data(asset, taskEntities): 
    rigMbFile = get_rig(asset)
    rigMaFile = rigMbFile.replace('.mb', '.ma')


    rigFile = rigMbFile
    if not os.path.exists(rigMbFile): 
        rigFile = rigMaFile

    for taskEntity in taskEntities: 
        process = taskEntity.get('content')
        # action 
        actionPath = get_miarmy_action(asset, process)
        actionPaths = get_miarmy_group_action(asset, process) # group actions 
        cachePath = get_cache(asset, process)
        animPath = get_anim_data(asset, process)
        previewPath = get_img_path(asset)
        displays = [os.path.basename(a) for a in actionPaths]

        rigFileStatus = os.path.exists(rigFile)
        cacheFileStatus = os.path.exists(cachePath)
        animFileStatus = os.path.exists(animPath)
        actionFileStatus = os.path.exists(actionPath)
        actionFileStatuses = [os.path.exists(a) for a in actionPaths]

        rigStatus = 'Ready' if rigFileStatus else 'No File'
        cacheStatus = 'Ready' if cacheFileStatus else 'No File'
        animStatus = 'Ready' if animFileStatus else 'No File'
        actionStatus = 'Ready' if actionFileStatus else 'No File'

        taskEntity['rig'] = {'file': rigFile, 'status': rigFileStatus, 'display': rigStatus}
        taskEntity['cache'] = {'file': cachePath, 'status': cacheFileStatus, 'display': cacheStatus}
        taskEntity['animData'] = {'file': animPath, 'status': animFileStatus, 'display': animStatus}
        taskEntity['mcdAction'] = {'file': actionPath, 'status': actionFileStatus, 'display': actionStatus}
        # taskEntity['mcdActions'] = group_action_data(actionPath)
        taskEntity['preview'] = {'file': previewPath}

        menuData = []
        menuData.append({'display': 'Import Anim Data', 'type': 'menu', 'file': animPath, 'action': 'animImport', 'status': cacheFileStatus})
        menuData.append({'display': 'Import Miarmy action', 'type': 'menu', 'file': actionPath, 'action': 'mayaImport', 'status': actionFileStatus})
        menuData.append({'display': 'Import Miarmy actions', 'type': 'submenu', 'displays': displays, 'file': actionPaths, 'action': 'mayaImport', 'status': actionFileStatuses})
        menuData.append({'display': 'Reference Abc Cache', 'type': 'menu', 'file': cachePath, 'action': 'mayaReference', 'status': actionFileStatus})
        taskEntity['menu'] = menuData

    return taskEntities


def group_action_data(paths): 
    actionList = []
    for actionPath in paths: 
        actionFileStatus = os.path.exists(actionPath)
        actionStatus = 'Ready' if actionFileStatus else 'No File'
        actionList.append({'file': actionPath, 'status': actionFileStatus, 'display': actionStatus})
    return actionList

def action(path, action): 
    import maya.cmds as mc
    if action == 'animImport': 
        if mc.ls(sl=True): 
            import_anim(path)
        else: 
            mc.confirmDialog(title='Error', message='Nothing selected', button=['OK'])

    if action == 'mayaImport': 
        from rf_maya.lib import maya_file
        return maya_file.import_file(path)

    if action == 'mayaReference': 
        from rf_app.sg_manager import dcc_hook
        entity = context_info.ContextPathInfo(path=path)
        dcc_hook.create_reference(entity, path, material=True, materialType='preview')


def import_anim(filename):
    import maya.cmds as mc
    import maya.mel as mm
    mm.eval('file -import -type "animImport" -ra true -options "targetTime=4;copies=1;option=replace;pictures=0;connect=0;"  -pr -loadReferenceDepth "all" "%s";' % filename)


def create_cycle_folder(entity, taskName): 
    asset = entity.copy()
    asset.context.update(step='anim', process=taskName)
    workspacePath = asset.path.workspace().abs_path()
    if not os.path.exists(workspacePath): 
        os.makedirs(workspacePath)
    return workspacePath


def create_playblast(asset): 
    from rf_maya.lib import playblast_lib
    import maya.cmds as mc
    reload(playblast_lib)
    outputImgPath = asset.path.output_hero_img_seq().abs_path()
    imgName = asset.publish_name(hero=True, ext='.png')
    filepath = '%s/%s' % (outputImgPath, imgName)

    startFrame = mc.playbackOptions(q=True, min=True)
    endFrame = mc.playbackOptions(q=True, max=True)

    result = playblast_lib.playblast(filepath, size=(128, 64), sequencer=0, format='image', codec='png', timeRange=True, offScreen=False)
    print 'result', result
    return result


def get_img_path(asset): 
    outputImgPath = asset.path.output_hero_img_seq().abs_path()
    imgName = asset.publish_name(hero=True, ext='.png')
    files = file_utils.list_file(outputImgPath)
    return files

def filter_users(userEntities, groupFilter=['Animator']): 
    filterUserEntities = []
    for user in userEntities: 
        groups = user.get('groups')
        
        for group in groups: 
            if group.name in groupFilters: 
                filterUserEntities.append(user)
    return filterUserEntities


def create_file(entity): 
    """ create start maya file with rig reference in """ 
    moduleDir = os.path.dirname(sys.modules[__name__].__file__)
    templateFile = '%s/template/maya.ma' % moduleDir

    # target file 
    dst = work_file(entity)

    # copy 
    file_utils.copy(templateFile, dst)
    if os.path.exists(dst): 
        return dst 

def work_file(entity): 
    # workfile
    entity.context.update(version='v001')
    workspacePath = entity.path.workspace().abs_path()
    workfile = entity.work_name(user=True)
    dst = '%s/%s' % (workspacePath, workfile)
    return dst


def save_file(entity): 
    workfile = work_file(entity)
    saveFile = save_utils.pipeline_increment2(entity, workfile)
    name, ext = os.path.splitext(saveFile)
    fileFormat = 'mayaAscii' if ext == '.ma' else 'mayaBinary' if ext == '.mb' else ''
    result = save_utils.save_file(saveFile, fileFormat)
    logger.debug(result)
    return result


def open_file(path, loadNoReference=False): 
    return dcc_hook.open_file(path, loadNoReference=loadNoReference)

def create_reference(entity, path): 
    from rf_app.sg_manager import dcc_hook
    dcc_hook.create_reference(entity, path, material=True, materialType='preview')

def create_camera(): 
    from rf_maya.lib import camera_lib
    reload(camera_lib)
    camera_lib.create_cam()


def get_rig(entity): 
    asset = entity.copy()
    asset.context.update(step='rig', process='main', res='md')
    heroPath = asset.path.hero().abs_path()
    heroFile = asset.output_name(outputKey='rig', hero=True)
    path = '%s/%s' % (heroPath, heroFile)
    return path 

def get_anim_data(entity, process): 
    asset = entity.copy()
    asset.context.update(step='rig', process=process, res='md')
    heroPath = asset.path.hero().abs_path()
    heroFile = asset.output_name(outputKey='animData', hero=True)
    path = '%s/%s' % (heroPath, heroFile)
    return path 

def get_miarmy_action(entity, process): 
    asset = entity.copy()
    asset.context.update(step='anim', process=process, look=process, res='md')
    heroPath = asset.path.hero().abs_path()
    heroFile = asset.output_name(outputKey='miarmyAction', hero=True)
    path = '%s/%s' % (heroPath, heroFile)
    return path 

def get_miarmy_group_action(entity, process): 
    key = 'mcdAction'
    asset = entity.copy()
    asset.context.update(step='anim', process=process, look=process, res='md')
    heroPath = asset.path.hero().abs_path()
    files = file_utils.list_file(heroPath)
    actionFiles = ['%s/%s' % (heroPath, a) for a in files if key in a and process in a]
    return actionFiles 


def get_miarmy_oa(entity, res, absPath=False): 
    step = entity.step
    prevRes = entity.res
    process = entity.process
    entity.context.update(step='rig', process='oa', res=res)

    heroPath = entity.path.hero().abs_path()
    heroRelPath = entity.path.hero()
    oaFile = entity.output_name(outputKey='miarmyOa', hero=True)

    oaRigFile = '%s/%s' % (heroPath, oaFile)
    oaRigRelFile = '%s/%s' % (heroRelPath, oaFile)

    if absPath: 
        return oaRigFile

    else: 
        return oaRigRelFile
        

def get_cache(entity, process): 
    asset = entity.copy()
    asset.context.update(step='anim', process=process, res='md')
    heroPath = asset.path.hero().abs_path()
    heroFile = asset.output_name(outputKey='cache', hero=True)
    path = '%s/%s' % (heroPath, heroFile)
    return path 

def scene_context(): 
    try: 
        import maya.cmds as mc 
        if mc.file(q=True, sn=True): 
            return context_info.ContextPathInfo()

    except: 
        return 

def check_matching_context(entity1, entity2): 
    list1 = list()
    if entity1: 
        list1.append(entity1.project)
        list1.append(entity1.type)
        list1.append(entity1.name)
        list1.append(entity1.process)

    list2 = list()
    if entity2: 
        list2.append(entity2.project)
        list2.append(entity2.type)
        list2.append(entity2.name)
        list2.append(entity2.process)
    
    if list1 and list2: 
        if list1 == list2: 
            return True
        else: 
            logger.warning(list1)
            logger.warning(list2)
