# v.0.0.1 polytag switcher
_title = 'Riff Model Smooth Data'
_version = 'v.0.0.1'
_des = 'wip'
uiName = 'RFModelSmoothUI'

#Import python modules
import sys
import os
import getpass
import logging
from functools import partial
from collections import OrderedDict

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

# import config
import rf_config as config

uiMode = True
if config.isMaya:
    import maya.cmds as mc
    import maya.mel as mm

# framework modules
from rf_utils import log_utils
from rf_utils.ui import load
from rf_utils.ui import stylesheet

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

import ui
reload(ui)
# thread

class Config:
    asset = 'asset'
    scene = 'scene'
    mode = {asset: 'Asset', scene: 'Shot'}

class ModelSmoothData(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        #Setup Window
        super(ModelSmoothData, self).__init__(parent)

        # ui read
        self.ui = ui.ModelSmoothUI(parent)
        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)

        # smooth
        self.smoothSet = 'smoothPly_set'

        # title
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.resize(300, 400)
        self.display_smooth()
        self.init_signals()

    def init_signals(self):
        self.ui.addSmooth_pushButton.clicked.connect(self.add_smooth)
        self.ui.removeSmooth_pushButton.clicked.connect(self.remove_smooth)
        self.ui.createSet_pushButton.clicked.connect(self.create_set)
        self.ui.selNonSmooth_pushButton.clicked.connect(self.select_non_smooth)
        self.ui.selSmooth_pushButton.clicked.connect(self.select_smooth)
        self.ui.listWidget.itemSelectionChanged.connect(self.select_tranform)

    def get_objects_in_group(self):
        sels = mc.ls(sl=True)
        list_obj = []
        for sel in sels:
            chk_group = self.is_group(sel)
            if chk_group == True:
                list_sel = self.select_under_grp(sel)
                for item in list_sel:
                    list_obj.append(item)
            else:
                list_obj.append(sel)
        return list_obj

    def add_smooth(self):
        list_obj = self.get_objects_in_group()
        smoothSet = self.create_set()
        mc.sets(list_obj, add=smoothSet)
        self.display_smooth()

    def remove_smooth(self):
        sels = mc.ls(sl=True)
        smoothSet = self.create_set()
        mc.sets(sels, rm=smoothSet)
        self.display_smooth()

    def create_set(self):
        if not mc.objExists(self.smoothSet):
            return mc.sets(n=self.smoothSet)
        else:
            list_obj = self.get_objects_in_group()
            if list_obj:
                mc.sets(list_obj, add=self.smoothSet)
        return self.smoothSet

    def select_smooth(self):
        if mc.objExists(self.smoothSet):
            smoothItem = mc.sets(self.smoothSet, q=True)
            mc.select(smoothItem)

    def select_non_smooth(self):
        if mc.objExists(self.smoothSet):
            smoothItem = mc.sets(self.smoothSet, q=True)
            geo = mc.ls(geometry=True)
            geo_list = mc.listRelatives(geo, parent=True)
            non_smooth = [x for x in geo_list if x not in smoothItem]
            mc.select(non_smooth)

    def display_smooth(self):
        self.ui.listWidget.clear()
        if mc.objExists(self.smoothSet):
            smoothItem = mc.sets(self.smoothSet, q=True)
            if smoothItem:
                for item in smoothItem:
                    self.myQListWidgetItem = QtWidgets.QListWidgetItem(self.ui.listWidget)
                    self.myQListWidgetItem.setText(item)
                    self.ui.listWidget.addItem(self.myQListWidgetItem)

    def select_tranform(self):
        all_item = self.ui.listWidget.selectedItems()
        list_select = []
        for item in all_item:
            transform = item.text()
            list_select.append(transform)
        mc.select(list_select)

    def select_under_grp(self,grp):
        list_sel = []
        all_shape = mc.ls(grp, dag=1, type='geometryShape')
        for geo_shape in all_shape:
            geo_tran = mc.listRelatives(geo_shape, parent=True)
            list_sel.append(geo_tran[0])
        return list_sel

    def is_group(self, node=None):
        if node == None:
            node = mc.ls(selection=True)[0]
        if mc.nodeType(node) != "transform":
            return False

        children = mc.listRelatives(node, c=True)

        if children == None:
            return True

        for c in children:
            if mc.nodeType(c) != 'transform':
                return False
        else:
            return True


def show():
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        maya_win.deleteUI(uiName)
        myApp = ModelSmoothData(maya_win.getMayaWindow())
        if uiMode:
            myApp.show()
        return myApp

# from rf_app.model.smooth_data import app
# reload(app)
# reload(app.ui)
# app.show()