# -*- coding: utf-8 -*-
import os
import sys

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())


class ModelSmoothUI(QtWidgets.QWidget):
    def __init__(self, parent=None) :
        super(ModelSmoothUI, self).__init__(parent)

        self.allLayout = QtWidgets.QVBoxLayout()
        self.mainLayout = QtWidgets.QVBoxLayout()
        self.label = QtWidgets.QLabel('Smooth List')
        self.mainLayout.addWidget(self.label)
        self.listWidget = QtWidgets.QListWidget()
        self.listWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.mainLayout.addWidget(self.listWidget)
        self.allLayout.addLayout(self.mainLayout)
        self.gridLayout = QtWidgets.QGridLayout()

        self.createSet_pushButton = QtWidgets.QPushButton('Create Set')
        self.allLayout.addWidget(self.createSet_pushButton)
        
        self.addSmooth_pushButton = QtWidgets.QPushButton('Add')
        self.removeSmooth_pushButton = QtWidgets.QPushButton('Remove')
        self.selSmooth_pushButton = QtWidgets.QPushButton('Select Smooth')
        self.selNonSmooth_pushButton = QtWidgets.QPushButton('Select Non Smooth')

        self.gridLayout.addWidget(self.addSmooth_pushButton, 0, 0, 1, 1)
        self.gridLayout.addWidget(self.removeSmooth_pushButton, 0, 1, 1, 1)
        self.gridLayout.addWidget(self.selSmooth_pushButton, 1, 0, 1, 1)
        self.gridLayout.addWidget(self.selNonSmooth_pushButton, 1, 1, 1, 1)

        

        self.allLayout.addLayout(self.gridLayout)
        self.setLayout(self.allLayout)


