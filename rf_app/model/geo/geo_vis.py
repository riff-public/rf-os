#!/usr/bin/env python
# -- coding: utf-8 --

# v.0.0.1 polytag switcher
_title = 'Hidden Geo'
_version = 'v.0.0.1'
_des = 'wip'
uiName = 'SetHiddenUI'

#Import python modules
import sys
import os 
import getpass
from collections import OrderedDict
from functools import partial

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]


# framework modules 
import rf_config as config
from rf_utils import log_utils
from rf_utils.ui import load
user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'

logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

import maya.cmds as mc 
from rf_utils.context import context_info
from rftool.utils import pipeline_utils
from rf_utils.deadline import playblast_queue
from rf_maya.lib import sequencer_lib
reload(pipeline_utils)
reload(context_info)
reload(playblast_queue)
attr = 'hidden'

class GeoVisControlUI(QtWidgets.QMainWindow):

    def __init__(self, parent=None):
        #Setup Window
        super(GeoVisControlUI, self).__init__(parent)

        # ui read
        uiFile = '%s/ui.ui' % moduleDir
        self.ui = load.setup_ui_maya(uiFile, parent)
        self.setCentralWidget(self.ui)
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.setObjectName(uiName)

        # init stuffs
        self.init_signals()
        # self.ui.runYetiCache_pushButton.setVisible(False)


    def init_signals(self): 
        self.ui.setHidden_pushButton.clicked.connect(partial(self.set_hidden, True))
        self.ui.setVisible_pushButton.clicked.connect(partial(self.set_hidden, False))
        self.ui.selectHidden_pushButton.clicked.connect(partial(self.select, 'hidden'))
        self.ui.selectVisible_pushButton.clicked.connect(partial(self.select, 'visible'))

    def set_hidden(self, state): 
        sels = mc.ls(sl=True)

        for sel in sels: 
            childs = mc.listRelatives(sel, ad=True, type='mesh')
            transforms = [mc.listRelatives(a, p=True)[0] for a in childs]

            for obj in transforms: 
                pipeline_utils.set_hidden_geo(obj=obj, state=state)



    def select(self, selType=''): 
        objs = mc.ls(type='mesh')
        transforms = [mc.listRelatives(a, p=True)[0] for a in objs]
        visibles = []
        hiddens = []

        for obj in transforms: 
            if mc.objExists('%s.%s' % (obj, attr)): 
                state = mc.getAttr('%s.%s' % (obj, attr))
                if not state: 
                    visibles.append(obj)
                else: 
                    hiddens.append(obj)
            else: 
                visibles.append(obj)

        if selType == 'hidden': 
            mc.select(hiddens)

        if selType == 'visible': 
            mc.select(visibles)


def show():
    from rftool.utils.ui import maya_win
    logger.info('Run in Maya\n')
    maya_win.deleteUI(uiName)
    myApp = GeoVisControlUI(maya_win.getMayaWindow())
    myApp.show()
    return myApp
