import os 
import sys 
import maya.cmds as mc 

def import_file(path): 
	return mc.file(path,  i=True, type='mayaAscii', options = 'v=0', pr = True, loadReferenceDepth = 'all')

def reference_file(namespace, path): 
	return mc.file(path, r=True, ns=namespace)