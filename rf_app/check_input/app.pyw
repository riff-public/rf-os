# v.0.0.1 polytag switcher
_title = 'Asset Publish'
_version = 'v.0.0.1'
_des = 'wip'
uiName = 'AssetPublishUI'

#Import python modules
import sys
import os 
import json 
import getpass

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

# import config
core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path: 
    sys.path.append(core)

import rf_config as config

# framework modules 
from rf_utils import log_utils
from rf_utils.ui import load
from rf_utils.ui import stylesheet

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

from rf_utils.widget import input_widget
from rf_utils.context import context_info 
import maya_hook

class LocalConfig: 
    captureSize = {'w': 1280, 'h': 1024}

class AssetPublish(QtWidgets.QMainWindow):

    def __init__(self, ContextPathInfo=None, parent=None):
        #Setup Window
        super(AssetPublish, self).__init__(parent)

        # ui read
        uiFile = '%s/ui.ui' % moduleDir
        if config.isMaya: 
            self.ui = load.setup_ui_maya(uiFile, parent)
        else: 
            self.ui = load.setup_ui(uiFile, self)

        self.entity = ContextPathInfo if ContextPathInfo else None
        
        self.ui.show()
        self.ui.setWindowTitle('%s %s-%s' % (_title, _version, _des))

        self.set_widgets()
        self.activate()

    def set_widgets(self): 
        self.inputWidget = input_widget.InputWidget(dccHook=maya_hook)
        self.ui.all_layout.addWidget(self.inputWidget)

    def activate(self): 
        self.entity = self.entity if self.entity else context_info.ContextPathInfo()
        self.inputWidget.update_input(self.entity)



def show(contextPathInfo=None):
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        maya_win.deleteUI(uiName)
        myApp = AssetPublish(ContextPathInfo=contextPathInfo, parent=maya_win.getMayaWindow())
        return myApp

    else: 
        logger.info('Run in standalone\n')
        app = QtWidgets.QApplication.instance()
        if not app: 
            app = QtWidgets.QApplication(sys.argv)
        myApp = AssetPublish()
        stylesheet.set_default(app)
        sys.exit(app.exec_())

if __name__ == '__main__': 
    show()
