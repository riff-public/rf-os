import os 
import sys 
import yaml

from Qt import QtWidgets
from Qt import QtGui
from Qt import QtCore

import subprocess
import shutil
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
moduleDir = os.path.dirname(sys.modules[__name__].__file__)

from rf_utils import file_utils 
from rftool.utils import maya_utils

class BrowseWidget(QtWidgets.QWidget):
    """docstring for BrowseWidget"""
    sendData = QtCore.Signal(str)
    def __init__(self, parent=None):
        super(BrowseWidget, self).__init__(parent=parent)
        
        # layout
        self.layout = QtWidgets.QVBoxLayout()
        
        # widget 
        self.label = QtWidgets.QLabel('Items')
        self.listWidget = QtWidgets.QListWidget()
        self.listWidget.setViewMode(QtWidgets.QListView.IconMode)
        self.listWidget.setIconSize(QtCore.QSize(100, 100))

        self.layout.addWidget(self.label)
        self.layout.addWidget(self.listWidget)
        self.setLayout(self.layout)
        self.init_signals()

    def init_signals(self): 

        self.listWidget.clicked[QtCore.QModelIndex].connect(self.send_data)

    def list_item(self, path,inFile): 
        dirs = file_utils.listFolder(path)
        self.listWidget.addItems(dirs)


    def new_pic(self,path,inFile):
        
        name = os.path.split(path)
        if ('_library'in inFile):
            for file in os.listdir(path):
                if file.endswith(".png"):
                    localPath = (os.path.join(path, file))
                    itm = QtWidgets.QListWidgetItem( self.listWidget )
                    itm.setIcon(QtGui.QIcon(localPath))
                    itm.setText(name[1:][0])
                    itm.setData(QtCore.Qt.UserRole,path)
                    self.listWidget.addItem (itm)

    def listOfFiles(self,dirs):
        folder = os.path.split(dirs)
        lists = file_utils.listFolder(folder[0])
        path = folder[0]

        self.clearWidget()
        for ix in range (len(lists)):
            path = os.path.join(folder[0], lists[ix])
            infile = os.listdir(path)
            self.new_pic(path,infile)



    def clearWidget(self):
        self.listWidget.clear()

    def send_data(self): 
        index = self.listWidget.currentIndex()
        data = index.data( QtCore.Qt.UserRole)
        self.sendData.emit(data)

    def refreshWidget(self):
        self.listWidget.takeItem(1)

    def contextMenuEvent(self,event):

        index = self.listWidget.currentIndex()
        data = index.data( QtCore.Qt.UserRole)

        contextMenu = QtWidgets.QMenu(self.listWidget)
        
        
        openFileAct = contextMenu.addAction("open FileExplorer")
        reTakeAct = contextMenu.addAction("RetakeThumbnail")
        deleteAct = contextMenu.addAction("delete")

        action = contextMenu.exec_(self.mapToGlobal(event.pos()))

        if action == deleteAct:
            self.deletePath(data)
        elif action == openFileAct:
            self.openInFileEx(data)
        elif action == reTakeAct:
            self.retakeThumbnail(data)

    def deletePath (self,data):
        quit_msg = "Are you sure deletfile?"
        reply = QtWidgets.QMessageBox.question(self, 'Delete', 
                         quit_msg, QtWidgets.QMessageBox.Yes, QtWidgets.QMessageBox.No)

        if reply == QtWidgets.QMessageBox.Yes:
            shutil.rmtree(data)
            self.listOfFiles(data)

    def openInFileEx (self,data):
        os.startfile(data)
        self.listOfFiles(data)

    def retakeThumbnail(self,data):

        index = self.listWidget.currentIndex()
        data = index.data( QtCore.Qt.UserRole)
        
        fileName = os.path.split(data)[1]
        filePng = data+'/'+fileName+'.png'
        sequencer = False
        formt = 'png'
        st = 1
        w = 900
        h = 900
        maya_utils.capture_screen(filePng, formt, st, sequencer, w, h)


        itm = self.listWidget.itemFromIndex(index)
        itm.setIcon(QtGui.QIcon(filePng))
        itm.setText(fileName)
        itm.setData(QtCore.Qt.UserRole,data)

        
        #for index in range(self.listWidget.count()):
         #   print self.listWidget.item(index).checkState()
            #if self.listWidget.item(index).checkState() == QtCore.Qt.Checked:
             #   checked_items.append(self.listWidget.item(index))
        #print checked_items



        
        